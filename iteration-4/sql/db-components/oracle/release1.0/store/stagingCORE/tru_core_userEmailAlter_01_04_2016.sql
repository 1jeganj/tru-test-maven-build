alter table dps_user add email2 varchar(60);

Update dps_user SET email2 = email;

ALTER TABLE dps_user DROP COLUMN email;

ALTER TABLE dps_user RENAME COLUMN email2 to email;


alter table dps_user modify login varchar(60);