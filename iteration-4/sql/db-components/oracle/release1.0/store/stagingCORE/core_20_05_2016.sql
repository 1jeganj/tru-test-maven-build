--adding additional column to identify this address is selected for Billing Address
ALTER TABLE TRU_DPSX_CONTACT_INFO ADD (BILLING_ADDRESS_IS_SAME NUMBER(1,0));