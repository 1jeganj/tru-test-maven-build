create or replace procedure TRU_NMWAREPORT_DATA_PRC
/*
***************************************************************TRU_NMWAREPORT_DATA_PRC**********************************************************************************************************
**    i.  Purpose
**
**            This procedure use to generate csv file for NMWA report
**

**    ii. Creation History
**
**           Date         Created by                   Comments
**          ---------     -----------                ---------------
**          20/Jan/2016   Malleshwara.C              Initial version


*******************************************************************************************************************************************************************************************
*/

(P_FILE_NAME varchar2,
 P_DIR_NAME varchar2,
 P_START_MTH varchar2,
 P_END_MTH  varchar2
)
is
	L_START_DATE  DATE;
	L_END_DATE  DATE;
	CSV_FILE UTL_FILE.FILE_TYPE;
	CURSOR NMWA_CUR IS SELECT A.SKU_ID,A.PRODUCT_ID,NO_REQ,NO_SENT,NO_OPEN,NO_NEW
					   FROM
						  (SELECT  SKU_ID,PRODUCT_ID,COUNT(*) NO_REQ FROM TRU_NMWA_ALERTS  WHERE NMWA_REQ_DATE > = TRUNC(L_START_DATE) AND NMWA_REQ_DATE < TRUNC(L_END_DATE+1) GROUP BY SKU_ID,PRODUCT_ID  ) A
						   LEFT OUTER JOIN
						  (SELECT SKU_ID,PRODUCT_ID,COUNT(*) NO_SENT FROM TRU_NMWA_ALERTS WHERE NMWA_SENT_FLAG=1  AND  NMWA_EMAIL_SENT_DATE >= TRUNC(SYSDATE) AND NMWA_EMAIL_SENT_DATE < TRUNC(SYSDATE+1)  GROUP BY SKU_ID,PRODUCT_ID )B
						   ON(A.SKU_ID =B.SKU_ID AND A.PRODUCT_ID=B.PRODUCT_ID)
						   LEFT OUTER JOIN
						  (SELECT SKU_ID,PRODUCT_ID,COUNT(*) NO_OPEN FROM TRU_NMWA_ALERTS WHERE NMWA_SENT_FLAG=0  GROUP BY SKU_ID,PRODUCT_ID )C
						   ON(A.SKU_ID =C.SKU_ID AND A.PRODUCT_ID=C.PRODUCT_ID)
						   LEFT OUTER JOIN
						  (SELECT SKU_ID,PRODUCT_ID,COUNT(*) NO_NEW FROM TRU_NMWA_ALERTS WHERE NMWA_REQ_DATE >= TRUNC(SYSDATE) AND NMWA_REQ_DATE < TRUNC(SYSDATE+1)   GROUP BY SKU_ID,PRODUCT_ID )D
						   ON(A.SKU_ID =D.SKU_ID AND A.PRODUCT_ID=D.PRODUCT_ID);

	L_NMWA NMWA_CUR%ROWTYPE;
	L_FILE_NAME VARCHAR2(255);
	L_HEADER VARCHAR2(4000);
	L_TRIALER VARCHAR2(4000);
	L_TOT_COUNT NUMBER;
	L_SENT_SUM NUMBER;

    non_num_exc EXCEPTION;
    PRAGMA EXCEPTION_INIT (non_num_exc, -01858);


begin

	if P_FILE_NAME is null or P_DIR_NAME is null
	then
	 RAISE_APPLICATION_ERROR(-20101, 'please pass file_name and dir_name');
	end if;

	begin

    select TO_DATE('01-'||P_START_MTH,'dd-mm-yyyy') into  L_START_DATE from DUAL;
    select LAST_DAY(TO_DATE(P_END_MTH,'mm-yyyy')) into  L_END_DATE from DUAL;
		--select to_date('01-'||P_START_MTH||'-'||extract(year from sysdate),'dd-mm-yyyy') into L_START_DATE from dual;
		--select last_day(ADD_MONTHS(to_date('01-'||P_END_MTH||'-'||extract(year from sysdate),'dd-mm-yyyy'),12)) into L_END_DATE from dual;
	 exception when non_num_exc
	 then RAISE_APPLICATION_ERROR(-20101, 'please pass valid start and end month');
	end;


	dbms_output.put_line('L_START_DATE:'||L_START_DATE);
	dbms_output.put_line('L_END_DATE:'||L_END_DATE);

	select P_FILE_NAME||'.dat' into L_FILE_NAME from DUAL;
	dbms_output.put_line(L_FILE_NAME);

		csv_file                    := UTL_FILE.FOPEN(P_DIR_NAME,L_file_NAME,'w',32767);
		select 'HEADER|'||TO_CHAR(sysdate,'YYYY-MM-DD')||'|'||TO_CHAR(LPAD(NMWA_RPT_SEQ.NEXTVAL,6,0)) ||'||||||||' into L_HEADER from DUAL;
		UTL_FILE.PUT(CSV_FILE,L_HEADER);
		UTL_FILE.NEW_LINE(CSV_FILE);

		OPEN   NMWA_CUR ;

		LOOP
		  FETCH NMWA_CUR INTO L_NMWA;
		  EXIT when NMWA_CUR%NOTFOUND;
		  UTL_FILE.PUT(CSV_FILE,to_char(sysdate,'YYYY-MM-DD'));
		  UTL_FILE.PUT(CSV_FILE,'|'||L_NMWA.product_id);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_NMWA.product_id);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_NMWA.SKU_ID);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_NMWA.no_new);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_NMWA.no_sent);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_NMWA.NO_OPEN);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_NMWA.no_req);
		  UTL_FILE.PUT(CSV_FILE,'|'||'049');
		  UTL_FILE.NEW_LINE(CSV_FILE);
		END LOOP;

		SELECT COUNT(1),SUM(NO_SENT) INTO L_TOT_COUNT,L_SENT_SUM
		FROM
			(SELECT A.SKU_ID,A.PRODUCT_ID,NO_REQ,NO_SENT,NO_OPEN,NO_NEW
						   FROM
							  (SELECT  SKU_ID,PRODUCT_ID,COUNT(*) NO_REQ FROM TRU_NMWA_ALERTS  WHERE NMWA_REQ_DATE > = TRUNC(L_START_DATE) AND NMWA_REQ_DATE < TRUNC(L_END_DATE+1) GROUP BY SKU_ID,PRODUCT_ID  ) A
							   LEFT OUTER JOIN
							  (SELECT SKU_ID,PRODUCT_ID,COUNT(*) NO_SENT FROM TRU_NMWA_ALERTS WHERE NMWA_SENT_FLAG=1  AND  NMWA_EMAIL_SENT_DATE >= TRUNC(SYSDATE) AND NMWA_EMAIL_SENT_DATE < TRUNC(SYSDATE+1)  GROUP BY SKU_ID,PRODUCT_ID )B
							   ON(A.SKU_ID =B.SKU_ID AND A.PRODUCT_ID=B.PRODUCT_ID)
							   LEFT OUTER JOIN
							  (SELECT SKU_ID,PRODUCT_ID,COUNT(*) NO_OPEN FROM TRU_NMWA_ALERTS WHERE NMWA_SENT_FLAG=0  GROUP BY SKU_ID,PRODUCT_ID )C
							   ON(A.SKU_ID =C.SKU_ID AND A.PRODUCT_ID=C.PRODUCT_ID)
							   LEFT OUTER JOIN
							  (SELECT SKU_ID,PRODUCT_ID,COUNT(*) NO_NEW FROM TRU_NMWA_ALERTS WHERE NMWA_REQ_DATE >= TRUNC(SYSDATE) AND NMWA_REQ_DATE < TRUNC(SYSDATE+1)   GROUP BY SKU_ID,PRODUCT_ID )D
							   ON(A.SKU_ID =D.SKU_ID AND A.PRODUCT_ID=D.PRODUCT_ID)
			);

		select 'TRAILER|'||to_char(lpad(L_TOT_COUNT+2,10,0))||'|'||NVL(to_char(lpad(L_SENT_SUM,15,0)),'0')||'||||||||' into L_TRIALER from DUAL;

		UTL_FILE.PUT(CSV_FILE,L_TRIALER);
		UTL_FILE.FCLOSE(CSV_FILE);
	EXCEPTiON when OTHERS then
	RAISE;
end;
/