ALTER TABLE tru_product DROP (channel_availability, item_in_store_pick_up, product_htgit_msg);

ALTER TABLE tru_sku ADD (
CHANNEL_AVAILABILITY NUMBER(38),
ITEM_IN_STORE_PICK_UP VARCHAR2(254),
PRODUCT_HTGIT_MSG NUMBER(38),
BRAND_NAME_PRIMARY VARCHAR2(254) );

ALTER TABLE tru_sku
  RENAME COLUMN inventory_first_available_date TO inventory_received_date;