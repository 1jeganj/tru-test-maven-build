drop table tru_clf_prd_att_name;

CREATE TABLE tru_clf_prd_att_name (
	category_id 		varchar2(254)	NOT NULL REFERENCES dcs_category(category_id),
	attribute_name 		varchar2(254)	NOT NULL REFERENCES tru_attribute_name(attribute_id),
	CONSTRAINT tru_clf_prd_att_name_P PRIMARY KEY(category_id, attribute_name)
);