-- Start - Turn off the Leave Warning functionality in checkout - 27th Apr 2016

ALTER TABLE TRU_SITE_CONFIGURATION ADD TURN_OFF_LEAVE_WARNING NUMBER(1) DEFAULT 0;

-- End - Turn off the Leave Warning functionality in checkout - 27th Apr 2016