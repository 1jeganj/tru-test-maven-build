create or replace procedure TRU_SEO_RPT_PRC
/*
***************************************************************TRU_SEO_RPT_PRC**********************************************************************************************************
**    i.  Purpose
**
**            This procedure use to generate csv file for SEO report
**

**    ii. Creation History
**
**           Date         Created by                   Comments
**          ---------     -----------                ---------------
**          28/Jan/2016   Malleshwara.C              Initial version


*******************************************************************************************************************************************************************************************
*/

(P_FILE_NAME varchar2,
 P_DIR_NAME varchar2
)
is

	CSV_FILE UTL_FILE.FILE_TYPE;
	CURSOR SEO_CUR IS select tru_classification.category_id
							  ,case when dcs_category_sites.SITE_ID = 'ToysRUs' then 'Toys Category Pages'
                     when dcs_category_sites.SITE_ID = 'BabyRUs' then 'Babies Category Pages'
                     else null end PARAMETRIC_PROFILE_NAV_TXT
                ,nvl(tru_seo.SEO_TITLE, dcs_category.display_name||' - '||site_configuration.name) SEO_TITLE
							  ,case when tru_seo.SEO_TITLE is null then 'N' else 'Y'  end seo_title_override
							  ,case when tru_seo.SEO_DESCRIPTION is null and dcs_category_sites.SITE_ID = 'ToysRUs' 
								then  'Buy '||dcs_category.display_name||' products at '||site_configuration.name||'.com. The leading toy store for toys, games, educational toys and more.'
								 when tru_seo.SEO_DESCRIPTION is null and dcs_category_sites.SITE_ID = 'BabyRUs'
								then 'Buy '||dcs_category.display_name||' products at '||site_configuration.name||'.com. The leading baby store for nursery furniture, baby products and more.'
								when tru_seo.SEO_DESCRIPTION is not null then tru_seo.SEO_DESCRIPTION
							   else null end SEO_DESCRIPTION
							  , case when tru_seo.SEO_DESCRIPTION  is null then 'N' else 'Y'  end seo_desc_override
							   ,dcs_cat_keywrds.KEYWORD
                 , 'N' seo_kwd_override
							   
						from
						tru_classification
						inner join dcs_category on(tru_classification.category_id = dcs_category.category_id)
						left outer join dcs_category_sites on(tru_classification.category_id = dcs_category_sites.category_id)
						left outer join  site_configuration  on(dcs_category_sites.SITE_ID = site_configuration.id)
						left outer join tru_seo  on(tru_classification.SEO_TAG_ID = tru_seo.SEO_TAG_ID)
						left outer join  (select category_id,LISTAGG (KEYWORD,',')  WITHIN GROUP (order by category_id) KEYWORD from dcs_cat_keywrds group by category_id) dcs_cat_keywrds   
						on(tru_classification.category_id = dcs_cat_keywrds.category_id);
						   
	L_SEO SEO_CUR%ROWTYPE;
	L_FILE_NAME VARCHAR2(255);
	L_HEADER VARCHAR2(4000);
	L_TRIALER VARCHAR2(4000);
	L_COUNT NUMBER;
	L_SUM NUMBER;

	

begin

	if P_FILE_NAME is null or P_DIR_NAME is null
	then
	 RAISE_APPLICATION_ERROR(-20101, 'please pass file_name and dir_name');
	end if;

	
	select P_FILE_NAME into L_FILE_NAME from DUAL;
	--dbms_output.put_line(L_FILE_NAME);

		csv_file                    := UTL_FILE.FOPEN(P_DIR_NAME,L_file_NAME,'w',32767);
		
		select 'HEADER|'||TO_CHAR(sysdate,'YYYY-MM-DD')||'|'||TO_CHAR(LPAD(SEO_RPT_SEQ.NEXTVAL,6,0)) into L_HEADER from DUAL;
		UTL_FILE.PUT(CSV_FILE,L_HEADER);
		UTL_FILE.NEW_LINE(CSV_FILE);
		
		OPEN   SEO_CUR ;
		
		LOOP
		  FETCH SEO_CUR INTO L_SEO;
		  EXIT when SEO_CUR%NOTFOUND;
		  UTL_FILE.PUT(CSV_FILE,L_SEO.category_id);
		  UTL_FILE.PUT(CSV_FILE,'|');
          UTL_FILE.PUT(CSV_FILE,L_SEO.PARAMETRIC_PROFILE_NAV_TXT);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_SEO.SEO_TITLE);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_SEO.seo_title_override);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_SEO.SEO_DESCRIPTION);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_SEO.seo_desc_override);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_SEO.KEYWORD);
          UTL_FILE.PUT(CSV_FILE,'|'||L_SEO.seo_kwd_override);
          UTL_FILE.PUT(CSV_FILE,'|'||TO_CHAR(sysdate,'YYYY-MM-DD'));
		  UTL_FILE.NEW_LINE(CSV_FILE);
		END LOOP;
		
		select COUNT(tru_classification.category_id),SUM(tru_classification.category_id)
				INTO L_COUNT,L_SUM	   
		from
		tru_classification
		inner join dcs_category on(tru_classification.category_id = dcs_category.category_id)
		left outer join dcs_category_sites on(tru_classification.category_id = dcs_category_sites.category_id)
		left outer join  site_configuration  on(dcs_category_sites.SITE_ID = site_configuration.id)
		left outer join tru_seo  on(tru_classification.SEO_TAG_ID = tru_seo.SEO_TAG_ID)
		left outer join  (select category_id,LISTAGG (KEYWORD,',')  WITHIN GROUP (order by category_id) KEYWORD from dcs_cat_keywrds group by category_id) dcs_cat_keywrds   
		on(tru_classification.category_id = dcs_cat_keywrds.category_id);
		
		select 'TRAILER'||'|'||to_char(lpad(L_COUNT+2,10,0))||'|'||to_char(lpad(L_SUM,13,0)) into L_TRIALER from DUAL;
		
		UTL_FILE.PUT(CSV_FILE,L_TRIALER);
		UTL_FILE.FCLOSE(CSV_FILE);
	EXCEPTiON when OTHERS then
	RAISE;
end;
/