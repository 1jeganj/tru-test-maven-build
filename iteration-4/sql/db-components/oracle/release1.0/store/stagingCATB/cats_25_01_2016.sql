drop table tru_wcm_article;
CREATE TABLE tru_wcm_article (
	id 			varchar2(254)	NOT NULL REFERENCES wcm_article(id),	
	seo_title_txt 		varchar2(254)	NOT NULL,
	seo_meta_desc 		varchar2(254)	NOT NULL,
	seo_meta_keyword_txt 	varchar2(254)	NOT NULL,
	seo_alt_meta_desc 	varchar2(254)	NULL,
	PRIMARY KEY(id)
);