drop table tru_sku_battery;

CREATE TABLE tru_prd_battery (
	product_id 		VARCHAR(254)	NOT NULL REFERENCES dcs_product(product_id),
	sequence_num 		INTEGER	NOT NULL,
	battery 		VARCHAR(254)	NULL REFERENCES tru_battery(battery_id),
	PRIMARY KEY(product_id, sequence_num)
);