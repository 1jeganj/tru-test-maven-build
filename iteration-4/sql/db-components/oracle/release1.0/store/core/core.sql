CREATE TABLE TRU_OMS_AUDIT (
	ID VARCHAR2(40) NOT NULL,
	TRANS_ID VARCHAR2(40) NULL,
	REQUEST CLOB NULL,
	RESPONSE CLOB NULL,
	TYPE VARCHAR2(40) NULL,
	STATUS VARCHAR2(40) NULL,
	TRAN_TIME TIMESTAMP NULL,
	PRIMARY KEY(ID)
);
CREATE TABLE tru_order_coupon_code (
	order_id 		varchar2(254)	NOT NULL,
	coupon_id 		varchar2(254)	NOT NULL,
	single_use_coupon 		varchar2(254)	NULL,
	CONSTRAINT   tru_order_coupon_code_p PRIMARY KEY(order_id, coupon_id),
  CONSTRAINT   tru_order_coupon_code_f FOREIGN KEY (order_id) REFERENCES dcspp_order(order_id)
);
commit;

CREATE TABLE TRU_OMS_ERROR_RECORD (
	ID 			varchar2(254)	NOT NULL,
	OMS_SERVICE_ID 		varchar2(254)	NOT NULL,
	OMS_SERVICE_TYPE 	varchar2(254)	NOT NULL,
	OMS_REQUEST 		CLOB	NOT NULL,
	OMS_ORDER_ID 		varchar2(254)	NOT NULL,
	REQ_SEND_COUNT 		INTEGER	NULL,
	tran_time 		DATE	NULL,
	PRIMARY KEY(ID)
);

commit;
