--This script need to run under BATCH schema

--Ignore the error(ORA-01430: column being added already exists in table) if TOTAL_REC column already exist in TRU_TMP_XML_SPLIT table
alter table TRU_TMP_XML_SPLIT add( TOTAL_REC  NUMBER);


create or replace
PACKAGE TRU_NMWA_MIG_PKG
/*
***************************************************************TRU_NMWA_MIG_PKG**********************************************************************************************************
**    i.  Purpose
**
**            This package use to migrate data from XML file to TRU_NMWA_ALERTS table
**
**
**    ii. Procedures

***         i.NMWA_MIG_STAGE_MAIN
            -------------------------
               This procedure use to split XML file  into 5 parts and it calls NMWA_XML_STAGE_LOAD procedure to load NMWA_ALERTS records into TRU_TEMP_NMWA_ALERT tables.
			   
			   IN parameter --    P_DIR_NAME,P_FILE_NAME and P_TRUNCATE (It holds XML file directory name , it's file name  and truncate flag(y or n))


***         ii.    NMWA_XML_STAGE_LOAD
			-----------------------------
                This procedure parse the XML file and load data into staging tables
				   TRU_TEMP_NMWA_ALERT  	: table holds NMWA_ALERT  details

				   
				IN parameter --    P_START_NO,P_END_NO (it holds satrt and end number of XML file profiles,  it  picks the profile from the XML file and load staging table based on start and end number)  
				   

***         iii.NMWA_MIG_TARGT
			-----------------------
				This procedure migrate the NMWA_ALERT data from TRU_TEMP_NMWA_ALERT tables into TRU_NMWA_ALERTS tables
				   
				IN parameter --    P_LAOD_TYPE(value should be 'initial' or 'delta' to decide the profile migration type)


***         iV.NMWA_MIG_TARGT_BATCH
			-----------------------
				This procedure migrate the NMWA_ALERT data from TRU_TEMP_NMWA_ALERT tables into TRU_NMWA_ALERTS tables by batchwise commit. ( commit interval is 10000 records)
				   
				IN parameter --    P_LAOD_TYPE,P_COMMIT_INTRVL(P_LAOD_TYPE value should be 'initial' or 'delta' to decide the profile migration type and P_COMMIT_INTRVL value should be >= 10000)

**   iii. Functions

            i.XML_FILEEXISTS
            ----------------------
			IN parameter --    P_DIRNAME,P_FILENAME (It holds XML file directory name and it's file name respectively)
            This function use to identify the XML file exists or not in the directory. 


**    iv. Creation History
**
**           Date         Created by                   Comments
**          ---------     -----------                ---------------
**          28/Jan/2016   Malleshwara.C              Initial version


*******************************************************************************************************************************************************************************************
*/


AS
	G_START                            number;
    G_END                              NUMBER;
	G_BFILE  bfile;                             -- this holds the binary file
	G_CLOB CLOB;                                -- this is to read the data from bfile into clob type
	G_PARSER DBMS_XMLPARSER.PARSER;             -- this variable for parsing clob value to XMLparser
	G_MY_DOC DBMS_XMLDOM.DOMDOCUMENT;             -- this vairable shall hold XML parsed data
	--  represents the XML group of userinfo nodes 
	G_CURRENT_ITEM_LIST DBMS_XMLDOM.DOMNODELIST;  
	G_CURRENT_ITEM DBMS_XMLDOM.DOMNODE;           
	G_XMLELEM XMLDOM.DOMELEMENT;                  --This will read the root element information. In our file rooot element is :<profile ..>
	G_NSPACE VARCHAR2(50);                        --This will read name sapce of root element information. In our file rooot element is :<profile ..> and no namespaces are defined in file
	--- below variables are to be passed as parameters for DBMS_LOB.LOADCLOBFROMFILE
	G_DEST_OFFSET  NUMBER :=1;
	G_SRC_OFFSET   NUMBER :=1;
	G_BFILE_CSID   NUMBER := 0;
	G_LANG_CONTEXT NUMBER := DBMS_LOB.DEFAULT_LANG_CTX;
	G_WARNING      NUMBER;
	G_NMWA_COUNT NUMBER;
	G_FILE_NAME  VARCHAR2(255);
  
	FUNCTION XML_FILEEXISTS
	(
		P_DIRNAME IN VARCHAR2,   
		P_FILENAME IN VARCHAR2
	) RETURN VARCHAR2;
	PROCEDURE NMWA_MIG_STAGE_MAIN(P_DIR_NAME VARCHAR2,P_FILE_NAME VARCHAR2,P_TRUNCATE VARCHAR2);
    PROCEDURE NMWA_XML_STAGE_LOAD(P_START_NO NUMBER, P_END_NO NUMBER);
    PROCEDURE NMWA_MIG_TARGT(P_LAOD_TYPE  varchar2);
	PROCEDURE NMWA_MIG_TARGT_BATCH(P_LAOD_TYPE  varchar2,P_COMMIT_INTRVL PLS_INTEGER);
end TRU_NMWA_MIG_PKG;
/


create or replace
package body TRU_NMWA_MIG_PKG
AS
    
  FUNCTION XML_FILEEXISTS
	(
		P_DIRNAME IN VARCHAR2,   
		P_FILENAME IN VARCHAR2
		) RETURN VARCHAR2
	is
	L_FILENAME varchar2(255);
	L_EXISTS VARCHAR2(1000) := NULL ;
	begin
	
  if P_DIRNAME is null or P_DIRNAME = ' '
  then
     return 'ENTER VALID DIRECTORY NAME';
  end if;
  
  if P_FILENAME is null or P_FILENAME= ' '
  then
      return 'ENTER VALID FILE NAME';
  END IF;
  
	 --This loop convert comma seprated values(P_FILENAME - list of XML file name) into  records and check the file exists or not in the directory
	 FOR I IN (SELECT EXTRACT (VALUE (d), '//row/text()').getstringval() FILE_NAME 
							   FROM
								   (select xmltype (   '<rows><row>'
									|| REPLACE (P_FILENAME, ',', '</row><row>')
									|| '</row></rows>'
								   ) AS xmlval
									from DUAL) X,
								table (XMLSEQUENCE (extract (X.XMLVAL, '/rows/row')))D
                )
	  LOOP
								
								
		  G_BFILE := BFILENAME(UPPER(P_DIRNAME),I.FILE_NAME);
		  IF DBMS_LOB.FILEEXISTS(G_BFILE) = 0
		  THEN
			  L_EXISTS :=  L_EXISTS||','||I.FILE_NAME||':'||'Not Exist';
		  end if;
	  end LOOP;
    
    if L_EXISTS is null
    then
      return 'EXISTS';
    ELSE 
      return L_EXISTS;
    END IF;
  
	end XML_FILEEXISTS;


	PROCEDURE NMWA_MIG_STAGE_MAIN (P_DIR_NAME VARCHAR2,P_FILE_NAME VARCHAR2,P_TRUNCATE VARCHAR2)
    IS
	  L_FILE_EXISTS VARCHAR2(1000);
	  L_SPLIT_THREAD number := 5;
	  L_SPLIT_num NUMBER;
	  L_START_NUM number := 0;
	  L_TOTAL_NUM number :=23;
	  L_JOB_STMNT VARCHAR2(4000);
	  L_FILE_NAME VARCHAR2(4000);
    BEGIN

		G_START :=DBMS_UTILITY.GET_TIME;
		
		--Checking XML file name exists or not in the directory
		L_FILE_EXISTS := TRU_PROFILE_MIG_PKG.XML_FILEEXISTS(P_DIRNAME => P_DIR_NAME,P_FILENAME => P_FILE_NAME);
		
		IF L_FILE_EXISTS <> 'EXISTS'
		THEN
			RAISE_APPLICATION_ERROR(-20101, L_FILE_EXISTS );
		END IF;
		
		L_FILE_NAME := P_FILE_NAME;
		
		IF lower(P_TRUNCATE) not in('y','n') or P_TRUNCATE is null
		THEN
			RAISE_APPLICATION_ERROR(-20104, 'P_TRUNCATE value should be either y or n' );
		END IF;
		
		IF lower(P_TRUNCATE) = 'y'
		THEN
			--Clearing all records in NMWA migration staging tables
			DELETE FROM TRU_TMP_XML_SPLIT WHERE  JOB_NAME LIKE '%NMWA_MIG%';
			EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TEMP_NMWA_ALERT';
		ELSIF lower(P_TRUNCATE) = 'n'
		THEN
			FOR V_FNAM IN (SELECT FILE_NAME||',' FILE_NAME FROM TRU_TMP_XML_SPLIT WHERE JOB_NAME = 'NMWA_MIG')
			LOOP
				SELECT REPLACE(L_FILE_NAME,V_FNAM.FILE_NAME,NULL) INTO L_FILE_NAME FROM DUAL;
				DBMS_OUTPUT.PUT_LINE('FILE_NAME TRIM : '||L_FILE_NAME); 
			END LOOP;
		END IF;

		FOR I IN (SELECT EXTRACT (VALUE (d), '//row/text()').getstringval() FILE_NAME 
					   FROM
						   (select xmltype (   '<rows><row>'
							|| REPLACE (L_FILE_NAME, ',', '</row><row>')
							|| '</row></rows>'
						   ) AS xmlval
							from DUAL) X,
						table (XMLSEQUENCE (extract (X.XMLVAL, '/rows/row')))D
				 )
		LOOP

			  G_DEST_OFFSET  :=1;
			  G_SRC_OFFSET   :=1;
			  G_BFILE := BFILENAME(P_DIR_NAME, I.FILE_NAME);
			  G_FILE_NAME := I.FILE_NAME;
			  DBMS_LOB.CREATETEMPORARY(G_CLOB , cache=>false);
			  dbms_lob.open(G_BFILE, dbms_lob.lob_readonly);
			  dbms_lob.loadclobfromfile(dest_lob => G_CLOB, src_bfile => G_BFILE, AMOUNT => DBMS_LOB.GETLENGTH(G_BFILE), DEST_OFFSET => G_DEST_OFFSET, SRC_OFFSET => G_SRC_OFFSET, BFILE_CSID => DBMS_LOB.DEFAULT_CSID, LANG_CONTEXT => G_LANG_CONTEXT, warning => G_warning );
			  dbms_lob.close(G_BFILE);
			  dbms_session.set_nls('NLS_DATE_FORMAT','''DD-MON-YYYY''');
			  -- Create a parser.
			  G_PARSER := DBMS_XMLPARSER.NEWPARSER;
			  -- Parse the document and create a new DOM document.
			  DBMS_XMLPARSER.PARSECLOB(G_PARSER,G_CLOB);
			  G_MY_DOC  := dbms_xmlparser.getdocument(G_PARSER);
			  G_XMLELEM := xmldom.getdocumentelement(G_MY_DOC);
			  G_NSPACE  := xmldom.getnamespace(G_XMLELEM);
			  DBMS_OUTPUT.PUT_LINE('XML Root element information');
			  DBMS_OUTPUT.PUT_LINE('Qualified Name: ' || DBMS_XMLDOM.GETQUALIFIEDNAME(G_XMLELEM));
			  DBMS_OUTPUT.PUT_LINE('Local Name: ' || DBMS_XMLDOM.GETLOCALNAME(G_XMLELEM));
			  DBMS_OUTPUT.PUT_LINE('Namespace: ' || G_NSPACE);
			  DBMS_OUTPUT.PUT_LINE('Expanded Name: ' || DBMS_XMLDOM.GETEXPANDEDNAME(G_XMLELEM));
			  -- Free resources associated with the CLOB and Parser now they are no longer needed.
			  dbms_lob.freetemporary(G_CLOB);
			  DBMS_XMLPARSER.FREEPARSER(G_PARSER);
			  -- The following statement shall search for a node starting with 'data/profile'
			  G_CURRENT_ITEM_LIST := DBMS_XSLPROCESSOR.SELECTNODES(DBMS_XMLDOM.MAKENODE(G_MY_DOC),'/gsa-template/import-items/add-item');
			  
			  G_NMWA_COUNT := DBMS_XMLDOM.GETLENGTH(G_CURRENT_ITEM_LIST);
			  
			  DBMS_OUTPUT.PUT_LINE('Count of /gsa-template/import-items/add-item : '||G_NMWA_COUNT); 
			  
			  INSERT INTO TRU_TMP_XML_SPLIT(FILE_NAME,TOTAL_REC,JOB_NAME)
			  VALUES (I.FILE_NAME,G_NMWA_COUNT,'NMWA_MIG');
			  commit;
			  
			  G_NMWA_COUNT := G_NMWA_COUNT-1;
			  
			  --The following IF block decides to split XML file and load the data into staging tables
			  IF G_NMWA_COUNT < 0 THEN
				RAISE_APPLICATION_ERROR(-20102, 'There is no record in XML file :'||I.FILE_NAME);
			  ELSIF
				 G_NMWA_COUNT <= L_SPLIT_THREAD THEN 
 
				 NMWA_XML_STAGE_LOAD(P_START_NO => 0, P_END_NO => G_NMWA_COUNT);
			  
			  ELSIF 
			    G_NMWA_COUNT > L_SPLIT_THREAD 
			  THEN
			    L_SPLIT_NUM := G_NMWA_COUNT/L_SPLIT_THREAD;
				L_START_NUM := 0;

				  FOR J IN 1..L_SPLIT_THREAD
				  LOOP
					INSERT INTO TRU_TMP_XML_SPLIT(FILE_NAME,SI_NO,START_NO,END_NO,JOB_NAME)
					VALUES (I.FILE_NAME,J,L_START_NUM,L_SPLIT_NUM*J,'NMWA_MIG_'||J);
					L_START_NUM := L_SPLIT_NUM*J+1;
				  END LOOP;
				  
				  COMMIT;
				  FOR K IN (SELECT SI_NO,JOB_NAME,START_NO,END_NO FROM TRU_TMP_XML_SPLIT WHERE FILE_NAME=I.FILE_NAME  AND JOB_NAME LIKE '%NMWA_MIG_%' order by SI_NO)
				  LOOP        

					   DBMS_OUTPUT.PUT_LINE('-----------------xm split execution---------'||K.START_NO||','||K.END_NO);
						
						NMWA_XML_STAGE_LOAD(P_START_NO => K.START_NO, P_END_NO => K.END_NO);   
					
					
							
				  END LOOP;
				  DBMS_XMLDOM.FREEDOCUMENT(G_MY_DOC);
			  END IF;
		END LOOP;	

     DBMS_XMLDOM.FREEDOCUMENT(G_MY_DOC);
           
           

	G_END :=DBMS_UTILITY.GET_TIME;
    DBMS_OUTPUT.PUT_LINE('NMWA_MIG_STAGE_MAIN: Time Taken to execute ' || (G_END-G_START)/100||' '||'seconds');
		
	  EXCEPTION WHEN OTHERS THEN
		RAISE;
		
	END NMWA_MIG_STAGE_MAIN;
	
	
	PROCEDURE NMWA_XML_STAGE_LOAD
	( 
	  P_START_NO NUMBER, 
	  P_END_NO NUMBER
	)
    IS

		L_CURRENT_ITEM_LIST1 DBMS_XMLDOM.DOMNODELIST; 
		L_CURRENT_ITEM DBMS_XMLDOM.DOMNODE;           
		L_CURRENT_ITEM1 DBMS_XMLDOM.DOMNODE;   
		L_CURRENT_ITEM_LIST2 DBMS_XMLDOM.DOMNODELIST; 
		L_CURRENT_ITEM2 DBMS_XMLDOM.DOMNODE;  
		L_START NUMBER;
		L_END NUMBER;
	 
	 

    ---Record type for NMWA_ALERT information

	  type L_NMWA_RCRD is RECORD
		(  
		  ID      			TRU_TEMP_NMWA_ALERT.ID%TYPE,
		  EMAIL_ID       	TRU_TEMP_NMWA_ALERT.EMAIL_ID%TYPE,
		  PRODUCT_ID      	TRU_TEMP_NMWA_ALERT.PRODUCT_ID%TYPE,
		  SKU_ID       		TRU_TEMP_NMWA_ALERT.SKU_ID%TYPE,
		  NMWA_REQ_DATE     TRU_TEMP_NMWA_ALERT.NMWA_REQ_DATE%TYPE
	  );

	 type NMWA_COL  is table of L_NMWA_RCRD index by pls_integer; 
	 
	 L_NMWA_INFO NMWA_COL;
 

   
    BEGIN
		
		L_START :=DBMS_UTILITY.GET_TIME;
		
				 --DBMS_OUTPUT.PUT_LINE('#############     user info loop begin #################');
		  FOR CUR_ENT IN P_START_NO .. P_END_NO
		  LOOP 
      
			L_CURRENT_ITEM := DBMS_XMLDOM.ITEM(G_CURRENT_ITEM_LIST, CUR_ENT); 
			
			--DBMS_OUTPUT.PUT_LINE('L_CURRENT_ITEM:'|| DBMS_XMLDOM.GETNODENAME(L_CURRENT_ITEM));
			
			--#############-------------/gsa-template/import-items/add-item----------------##############
			
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'//@id',L_NMWA_INFO(CUR_ENT).ID);
			--DBMS_OUTPUT.PUT_LINE('//@id: '||L_NMWA_INFO(CUR_ENT).ID);
			 
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'//set-property[@name="emailAddress"]',L_NMWA_INFO(CUR_ENT).EMAIL_ID);
			--DBMS_OUTPUT.PUT_LINE('set-property[@name="emailAddress"]: '||L_NMWA_INFO(CUR_ENT).EMAIL_ID);
			
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'//set-property[@name="productId"]',L_NMWA_INFO(CUR_ENT).PRODUCT_ID);
			--DBMS_OUTPUT.PUT_LINE('set-property[@name="productId"]: '||L_NMWA_INFO(CUR_ENT).PRODUCT_ID);
			
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'//set-property[@name="skuId"]',L_NMWA_INFO(CUR_ENT).SKU_ID);
			--DBMS_OUTPUT.PUT_LINE('set-property[@name="skuId"]: '||L_NMWA_INFO(CUR_ENT).SKU_ID);
			
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'//set-property[@name="NMWAReqDate"]',L_NMWA_INFO(CUR_ENT).NMWA_REQ_DATE);
			--DBMS_OUTPUT.PUT_LINE('set-property[@name="NMWAReqDate"]: '||L_NMWA_INFO(CUR_ENT).NMWA_REQ_DATE);

		END LOOP;
 
		 FORALL i in P_START_NO..P_END_NO
		  INSERT INTO TRU_TEMP_NMWA_ALERT
		  (		
				ID,
				EMAIL_ID,
				PRODUCT_ID,
				SKU_ID,
				NMWA_REQ_DATE
		  )
		  VALUES
		  (
				L_NMWA_INFO(I).ID,
				L_NMWA_INFO(I).EMAIL_ID,
				L_NMWA_INFO(I).PRODUCT_ID,
				L_NMWA_INFO(I).SKU_ID,
        L_NMWA_INFO(I).NMWA_REQ_DATE
				--TO_TIMESTAMP(L_NMWA_INFO(I).NMWA_REQ_DATE,'dd-mm-yy hh24:mi:ss')
		  )
		 LOG ERRORS INTO DML_ERROR_LOG ('TRU_TEMP_NMWA_ALERT:'||G_FILE_NAME) REJECT LIMIT UNLIMITED;
		 
		
		L_END :=DBMS_UTILITY.GET_TIME;
		DBMS_OUTPUT.PUT_LINE('NMWA_XML_STAGE_LOAD: Time Taken to execute ' || (L_END-L_START)/100||' '||'seconds');	
		
		EXCEPTION
		WHEN OTHERS THEN
		  DBMS_OUTPUT.PUT_LINE('sqlerrm :'||SQLERRM);
		  dbms_lob.close(G_bfile);
		  DBMS_LOB.FREETEMPORARY(G_CLOB);
		  DBMS_XMLPARSER.FREEPARSER(G_PARSER);
		  DBMS_XMLDOM.FREEDOCUMENT(G_MY_DOC);
		  RAISE;
	END NMWA_XML_STAGE_LOAD;
	
	PROCEDURE NMWA_MIG_TARGT(P_LAOD_TYPE  VARCHAR2)
    IS
    BEGIN
		
		G_START :=DBMS_UTILITY.GET_TIME;
		
		IF LOWER(P_LAOD_TYPE) not in('initial' ,'delta') or P_LAOD_TYPE is null
		THEN
			
			RAISE_APPLICATION_ERROR(-20103, 'load type should be either initial or delta ' );
			
		END IF;
		
		IF lower(P_LAOD_TYPE) = 'initial'
		THEN
			
			INSERT INTO TRU_NMWA_ALERTS 
			(
				ID,
				EMAIL_ID,
				PRODUCT_ID,
				SKU_ID,
				NMWA_REQ_DATE,
				--NMWA_EMAIL_SENT_DATE,
				NMWA_SENT_FLAG
			)
		    SELECT 
				ID,
				EMAIL_ID,
				PRODUCT_ID,
				SKU_ID,
        TO_TIMESTAMP(NMWA_REQ_DATE,'mm/dd/yyyy') NMWA_REQ_DATE,
				--NMWA_EMAIL_SENT_DATE,
				0
			FROM TRU_TEMP_NMWA_ALERT
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_NMWA_ALERTS') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('TRU_NMWA_ALERTS load completed');
			
		END IF;
		
	    IF lower(P_LAOD_TYPE) = 'delta'
		THEN
			
			MERGE INTO TRU_NMWA_ALERTS A
			USING
				(
				     SELECT 
						ID,
						EMAIL_ID,
						PRODUCT_ID,
						SKU_ID,
						TO_TIMESTAMP(NMWA_REQ_DATE,'mm/dd/yyyy') NMWA_REQ_DATE
					FROM TRU_TEMP_NMWA_ALERT
				) B
			ON (A.ID=B.ID)
			WHEN MATCHED THEN
				UPDATE SET A.EMAIL_ID = B.EMAIL_ID ,A.PRODUCT_ID = B.PRODUCT_ID ,A.SKU_ID = B.SKU_ID ,A.NMWA_REQ_DATE = B.NMWA_REQ_DATE 
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.EMAIL_ID,A.PRODUCT_ID,A.SKU_ID,A.NMWA_REQ_DATE,A.NMWA_SENT_FLAG)
				VALUES (B.ID,B.EMAIL_ID,B.PRODUCT_ID,B.SKU_ID,B.NMWA_REQ_DATE,0)
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_NMWA_ALERTS') REJECT LIMIT UNLIMITED;
			
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('TRU_NMWA_ALERTS merge completed');
			
			

		END IF;
		
		
		G_END :=DBMS_UTILITY.GET_TIME;
        DBMS_OUTPUT.PUT_LINE('NMWA_MIG_TARGT: Time Taken to execute ' || (G_END-G_START)/100||' '||'seconds');
		
	  EXCEPTION WHEN OTHERS THEN
		RAISE;
		
	END NMWA_MIG_TARGT;


	PROCEDURE NMWA_MIG_TARGT_BATCH(P_LAOD_TYPE  VARCHAR2,P_COMMIT_INTRVL PLS_INTEGER)
    IS
		
		 ---Record type for NMWA_ALERT information

	  type L_NMWA_RCRD is RECORD
		(  
		  ID      			TRU_TEMP_NMWA_ALERT.ID%TYPE,
		  EMAIL_ID       	TRU_TEMP_NMWA_ALERT.EMAIL_ID%TYPE,
		  PRODUCT_ID      	TRU_TEMP_NMWA_ALERT.PRODUCT_ID%TYPE,
		  SKU_ID       		TRU_TEMP_NMWA_ALERT.SKU_ID%TYPE,
		  NMWA_REQ_DATE     TRU_TEMP_NMWA_ALERT.NMWA_REQ_DATE%TYPE
	  );

	 type NMWA_COL  is table of L_NMWA_RCRD index by pls_integer; 
	 
	 L_NMWA_INFO NMWA_COL;
		
		ROWS PLS_INTEGER;
		
		L_DYN_CUR SYS_REFCURSOR;
		
		
    BEGIN
		
		G_START :=DBMS_UTILITY.GET_TIME;
		
		IF P_COMMIT_INTRVL IS NULL OR P_COMMIT_INTRVL < 10000
		THEN
			
			RAISE_APPLICATION_ERROR(-20102, 'commit inerval value should be greater than or equal to 10000' );
			
		END IF;
		
		ROWS := P_COMMIT_INTRVL;
		
		IF LOWER(P_LAOD_TYPE) not in('initial','delta') or  P_LAOD_TYPE is null
		THEN
			
			RAISE_APPLICATION_ERROR(-20103, 'load type should be either initial or delta ' );
			
		END IF;
		
		IF lower(P_LAOD_TYPE) = 'initial'
		THEN
			
			 OPEN L_DYN_CUR FOR  SELECT 
									ID,
									EMAIL_ID,
									PRODUCT_ID,
									SKU_ID,
									TO_TIMESTAMP(NMWA_REQ_DATE,'mm/dd/yyyy') NMWA_REQ_DATE
								FROM TRU_TEMP_NMWA_ALERT;

             LOOP
                  FETCH L_DYN_CUR BULK COLLECT INTO L_NMWA_INFO LIMIT ROWS;
                  EXIT WHEN L_NMWA_INFO.count=0;
                  FORALL I IN L_NMWA_INFO.FIRST .. L_NMWA_INFO.LAST
					  INSERT INTO TRU_NMWA_ALERTS 
						(ID,EMAIL_ID,PRODUCT_ID,SKU_ID,NMWA_REQ_DATE,NMWA_SENT_FLAG)
					  values
						(L_NMWA_INFO(i).ID,L_NMWA_INFO(i).EMAIL_ID,L_NMWA_INFO(i).PRODUCT_ID,L_NMWA_INFO(i).SKU_ID,L_NMWA_INFO(i).NMWA_REQ_DATE,0)
					  LOG ERRORS INTO DML_ERROR_LOG ('TRU_NMWA_ALERTS') REJECT LIMIT UNLIMITED;
					
				  
                 COMMIT;
            END LOOP;
			
			
			
			
			DBMS_OUTPUT.PUT_LINE ('TRU_NMWA_ALERTS load completed');
			
			
			 
			
		END IF;
		
	    IF lower(P_LAOD_TYPE) = 'delta'
		THEN
		
		
			OPEN L_DYN_CUR  FOR 		SELECT 
											ID,
											EMAIL_ID,
											PRODUCT_ID,
											SKU_ID,
											TO_TIMESTAMP(NMWA_REQ_DATE,'mm/dd/yyyy') NMWA_REQ_DATE
										FROM TRU_TEMP_NMWA_ALERT;

             LOOP
                  FETCH L_DYN_CUR BULK COLLECT INTO L_NMWA_INFO LIMIT ROWS;
                  EXIT WHEN L_NMWA_INFO.count=0;
			
				  FORALL I IN L_NMWA_INFO.FIRST .. L_NMWA_INFO.LAST
					MERGE INTO TRU_NMWA_ALERTS A
					USING
						(
							SELECT 
								L_NMWA_INFO(I).ID ID,
								L_NMWA_INFO(I).EMAIL_ID EMAIL_ID,
								L_NMWA_INFO(I).PRODUCT_ID PRODUCT_ID,
								L_NMWA_INFO(I).SKU_ID SKU_ID,
								L_NMWA_INFO(I).NMWA_REQ_DATE NMWA_REQ_DATE
							FROM DUAL
						) B
					ON (A.ID=B.ID)
					WHEN MATCHED THEN
						UPDATE SET A.EMAIL_ID = B.EMAIL_ID ,A.PRODUCT_ID = B.PRODUCT_ID ,A.SKU_ID = B.SKU_ID ,A.NMWA_REQ_DATE = B.NMWA_REQ_DATE 
					WHEN NOT MATCHED THEN
						INSERT (A.ID,A.EMAIL_ID,A.PRODUCT_ID,A.SKU_ID,A.NMWA_REQ_DATE,A.NMWA_SENT_FLAG)
						VALUES (B.ID,B.EMAIL_ID,B.PRODUCT_ID,B.SKU_ID,B.NMWA_REQ_DATE,0)
					LOG ERRORS INTO DML_ERROR_LOG ('TRU_NMWA_ALERTS') REJECT LIMIT UNLIMITED;
					
				 
				 
					
				COMMIT;
			
			END LOOP;


			DBMS_OUTPUT.PUT_LINE ('TRU_NMWA_ALERTS merge completed');
				

			

		END IF;
		
		
		G_END :=DBMS_UTILITY.GET_TIME;
        DBMS_OUTPUT.PUT_LINE('NMWA_MIG_TARGT_BATCH: Time Taken to execute ' || (G_END-G_START)/100||' '||'seconds');
		
	  EXCEPTION WHEN OTHERS THEN
		RAISE;
		
	END NMWA_MIG_TARGT_BATCH;
END TRU_NMWA_MIG_PKG;