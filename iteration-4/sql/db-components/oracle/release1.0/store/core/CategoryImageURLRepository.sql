create table category_image_url
(
category_id varchar2(40) not null,
image_url varchar2(250) not null,
constraint category_image_url_p primary key (category_id)
);

commit;