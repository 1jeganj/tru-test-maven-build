--This script need to run under BATCH schema

alter table TRU_TEMP_PROFILE_ADDRESS add( LAST_ACTIVITY  TIMESTAMP(6));
alter table TRU_TMP_XML_SPLIT add( TOTAL_REC  NUMBER);
alter table TRU_TEMP_PROFILE_USERINFO MODIFY (USER_ID  VARCHAR2(40) NOT NULL);
alter table TRU_TEMP_PROFILE_ADDRESS MODIFY (ADDRESS_ID VARCHAR2(40) NOT NULL, USER_ID  VARCHAR2(40) NOT NULL);
alter table TRU_TEMP_PROFILE_CREDITCARD MODIFY (CREDIT_CARD_ID VARCHAR2(40) NOT NULL,USER_ID  VARCHAR2(40) NOT NULL);




create or replace
PACKAGE TRU_PROFILE_MIG_PKG
/*
***************************************************************TRU_PROFILE_MIG_PKG**********************************************************************************************************
**    i.  Purpose
**
**            This package use to migrate data from XML file to all profile related target table
**
**
**    ii. Procedures

***         i.PROFILE_MIG_STAGE_MAIN
            -------------------------
               This procedure use to split XML file  into 5 parts and it calls PROFILE_XML_STAGE_LOAD procedure to load profiles records into staging tables.
			   
			   IN parameter --    P_DIR_NAME,P_FILE_NAME and P_TRUNCATE (It holds XML file directory name , it's file name  and truncate flag(y or n))


***         ii.    PROFILE_XML_STAGE_LOAD
			-----------------------------
                This procedure parse the XML file and load data into staging tables
				   TRU_TEMP_PROFILE_USERINFO  	: table holds user info details
				   TRU_TEMP_PROFILE_ADDRESS		: table holds user address  details
				   TRU_TEMP_PROFILE_CREDITCARD	: table holds user credit cards  details
				   
				IN parameter --    P_START_NO,P_END_NO (it holds satrt and end number of XML file profiles,  it  picks the profile from the XML file and load staging table based on start and end number)  
				   

***         iii.PROFILE_MIG_TARGT
			-----------------------
				This procedure migrate the profiles data from stagin tables into ATG target tables
				List of ATG target table is going to migrate

					DPS_USER 
					TRU_DPSX_USER 
					DPS_USER_ADDRESS 
					DPS_CONTACT_INFO 
					TRU_DPSX_CONTACT_INFO
					DPS_OTHER_ADDR 
					DPS_CREDIT_CARD 				
					TRU_DPSX_CREDIT_CARD 
					DPS_USR_CREDITCARD 
				   
				IN parameter --    P_LAOD_TYPE(value should be 'initial' or 'delta' to decide the profile migration type)


***         iV.PROFILE_MIG_TARGT_BATCH
			-----------------------
				This procedure migrate the profiles data from staging tables into ATG target tables by batch wise commit. ( commit interval is 10000 records)
				   
				IN parameter --    P_LAOD_TYPE,P_COMMIT_INTRVL(P_LAOD_TYPE value should be 'initial' or 'delta' to decide the profile migration type and P_COMMIT_INTRVL value should be greater than 10000)

**   iii. Functions

            i.XML_FILEEXISTS
            ----------------------
			IN parameter --    P_DIRNAME,P_FILENAME (It holds XML file directory name and it's file name respectively)
            This function use to identify the XML file exists or not in the directory. 


**    iv. Creation History
**
**           Date         Created by                   Comments
**          ---------     -----------                ---------------
**          19/Jan/2016   Malleshwara.C              Initial version


*******************************************************************************************************************************************************************************************
*/


AS
	G_START                            number;
    G_END                              NUMBER;
	G_BFILE  bfile;                             -- this holds the binary file
	G_CLOB CLOB;                                -- this is to read the data from bfile into clob type
	G_PARSER DBMS_XMLPARSER.PARSER;             -- this variable for parsing clob value to XMLparser
	G_MY_DOC DBMS_XMLDOM.DOMDOCUMENT;             -- this vairable shall hold XML parsed data
	--  represents the XML group of userinfo nodes 
	G_CURRENT_ITEM_LIST DBMS_XMLDOM.DOMNODELIST;  
	G_CURRENT_ITEM DBMS_XMLDOM.DOMNODE;           
	G_XMLELEM XMLDOM.DOMELEMENT;                  --This will read the root element information. In our file rooot element is :<profile ..>
	G_NSPACE VARCHAR2(50);                        --This will read name sapce of root element information. In our file rooot element is :<profile ..> and no namespaces are defined in file
	--- below variables are to be passed as parameters for DBMS_LOB.LOADCLOBFROMFILE
	G_DEST_OFFSET  NUMBER :=1;
	G_SRC_OFFSET   NUMBER :=1;
	G_BFILE_CSID   NUMBER := 0;
	G_LANG_CONTEXT NUMBER := DBMS_LOB.DEFAULT_LANG_CTX;
	G_WARNING      NUMBER;
	G_PROFILE_COUNT NUMBER;
	G_PASWD VARCHAR2(255) := '748c97e6fad37513800acf4b81e2ba8af4249e7e';
	G_SALT_PASWD VARCHAR2(255) := 'ed3fd1cd845d2aba2dcbfaffeeafdc6458030a69';
	G_FILE_NAME  VARCHAR2(255);
  
	FUNCTION XML_FILEEXISTS
	(
		P_DIRNAME IN VARCHAR2,   
		P_FILENAME IN VARCHAR2
	) RETURN VARCHAR2;
	PROCEDURE PROFILE_MIG_STAGE_MAIN(P_DIR_NAME VARCHAR2,P_FILE_NAME VARCHAR2,P_TRUNCATE VARCHAR2);
    PROCEDURE PROFILE_XML_STAGE_LOAD(P_START_NO NUMBER, P_END_NO NUMBER);
    PROCEDURE PROFILE_MIG_TARGT(P_LAOD_TYPE  varchar2);
	PROCEDURE PROFILE_MIG_TARGT_BATCH(P_LAOD_TYPE  varchar2,P_COMMIT_INTRVL PLS_INTEGER);
end TRU_PROFILE_MIG_PKG;
/


create or replace
package body TRU_PROFILE_MIG_PKG
AS
    
  FUNCTION XML_FILEEXISTS
	(
		P_DIRNAME IN VARCHAR2,   
		P_FILENAME IN VARCHAR2
		) RETURN VARCHAR2
	is
	L_FILENAME varchar2(255);
	L_EXISTS VARCHAR2(1000) := NULL ;
	begin
	
  if P_DIRNAME is null or P_DIRNAME = ' '
  then
     return 'ENTER VALID DIRECTORY NAME';
  end if;
  
  if P_FILENAME is null or P_FILENAME= ' '
  then
      return 'ENTER VALID FILE NAME';
  END IF;
  
	 --This loop convert comma seprated values(P_FILENAME - list of XML file name) into  records and check the file exists or not in the directory
	 FOR I IN (SELECT EXTRACT (VALUE (d), '//row/text()').getstringval() FILE_NAME 
							   FROM
								   (select xmltype (   '<rows><row>'
									|| REPLACE (P_FILENAME, ',', '</row><row>')
									|| '</row></rows>'
								   ) AS xmlval
									from DUAL) X,
								table (XMLSEQUENCE (extract (X.XMLVAL, '/rows/row')))D
                )
	  LOOP
								
								
		  G_BFILE := BFILENAME(UPPER(P_DIRNAME),I.FILE_NAME);
		  IF DBMS_LOB.FILEEXISTS(G_BFILE) = 0
		  THEN
			  L_EXISTS :=  L_EXISTS||','||I.FILE_NAME||':'||'Not Exist';
		  end if;
	  end LOOP;
    
    if L_EXISTS is null
    then
      return 'EXISTS';
    ELSE 
      return L_EXISTS;
    END IF;
  
	end XML_FILEEXISTS;


	PROCEDURE PROFILE_MIG_STAGE_MAIN (P_DIR_NAME VARCHAR2,P_FILE_NAME VARCHAR2,P_TRUNCATE VARCHAR2)
    IS
	  L_FILE_EXISTS VARCHAR2(1000);
	  L_SPLIT_THREAD number := 5;
	  L_SPLIT_num NUMBER;
	  L_START_NUM number := 0;
	  L_TOTAL_NUM number :=23;
	  L_JOB_STMNT VARCHAR2(4000);
	  L_FILE_NAME VARCHAR2(4000);
    BEGIN

		G_START :=DBMS_UTILITY.GET_TIME;
		
		--Checking XML file name exists or not in the directory
		L_FILE_EXISTS := TRU_PROFILE_MIG_PKG.XML_FILEEXISTS(P_DIRNAME => P_DIR_NAME,P_FILENAME => P_FILE_NAME);
		
		IF L_FILE_EXISTS <> 'EXISTS'
		THEN
			RAISE_APPLICATION_ERROR(-20101, L_FILE_EXISTS );
		END IF;
		
		L_FILE_NAME := P_FILE_NAME;
		
		IF lower(P_TRUNCATE) not in('y','n') or P_TRUNCATE is null
		THEN
			RAISE_APPLICATION_ERROR(-20104, 'P_TRUNCATE value should be either y or n' );
		END IF;
		
		IF lower(P_TRUNCATE) = 'y'
		THEN
			--Clearing all records in profile migration staging tables
			DELETE FROM TRU_TMP_XML_SPLIT WHERE  JOB_NAME LIKE '%PROF_MIG%';
			EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TEMP_PROFILE_USERINFO';
			EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TEMP_PROFILE_ADDRESS';
			EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TEMP_PROFILE_CREDITCARD';
			--EXECUTE IMMEDIATE 'TRUNCATE TABLE DML_ERROR_LOG';
			--EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TMP_XML_SPLIT';
		ELSIF lower(P_TRUNCATE) = 'n'
		THEN
			FOR V_FNAM IN (SELECT FILE_NAME||',' FILE_NAME FROM TRU_TMP_XML_SPLIT WHERE JOB_NAME = 'PROF_MIG')
			LOOP
				SELECT REPLACE(L_FILE_NAME,V_FNAM.FILE_NAME,NULL) INTO L_FILE_NAME FROM DUAL;
				DBMS_OUTPUT.PUT_LINE('FILE_NAME TRIM : '||L_FILE_NAME); 
			END LOOP;
		END IF;
		

		FOR I IN (SELECT EXTRACT (VALUE (d), '//row/text()').getstringval() FILE_NAME 
					   FROM
						   (select xmltype (   '<rows><row>'
							|| REPLACE (L_FILE_NAME, ',', '</row><row>')
							|| '</row></rows>'
						   ) AS xmlval
							from DUAL) X,
						table (XMLSEQUENCE (extract (X.XMLVAL, '/rows/row')))D
				 )
		LOOP
			  G_DEST_OFFSET   :=1;
			  G_SRC_OFFSET    :=1;
			  G_BFILE := BFILENAME(P_DIR_NAME, I.FILE_NAME);
			  G_FILE_NAME := I.FILE_NAME;
			  DBMS_LOB.CREATETEMPORARY(G_CLOB , cache=>false);
			  dbms_lob.open(G_BFILE, dbms_lob.lob_readonly);
			  dbms_lob.loadclobfromfile(dest_lob => G_CLOB, src_bfile => G_BFILE, AMOUNT => DBMS_LOB.GETLENGTH(G_BFILE), DEST_OFFSET => G_DEST_OFFSET, SRC_OFFSET => G_SRC_OFFSET, BFILE_CSID => DBMS_LOB.DEFAULT_CSID, LANG_CONTEXT => G_LANG_CONTEXT, warning => G_warning );
			  dbms_lob.close(G_BFILE);
			  dbms_session.set_nls('NLS_DATE_FORMAT','''DD-MON-YYYY''');
			  -- Create a parser.
			  G_PARSER := DBMS_XMLPARSER.NEWPARSER;
			  -- Parse the document and create a new DOM document.
			  DBMS_XMLPARSER.PARSECLOB(G_PARSER,G_CLOB);
			  G_MY_DOC  := dbms_xmlparser.getdocument(G_PARSER);
			  G_XMLELEM := xmldom.getdocumentelement(G_MY_DOC);
			  G_NSPACE  := xmldom.getnamespace(G_XMLELEM);
			  DBMS_OUTPUT.PUT_LINE('XML Root element information');
			  DBMS_OUTPUT.PUT_LINE('Qualified Name: ' || DBMS_XMLDOM.GETQUALIFIEDNAME(G_XMLELEM));
			  DBMS_OUTPUT.PUT_LINE('Local Name: ' || DBMS_XMLDOM.GETLOCALNAME(G_XMLELEM));
			  DBMS_OUTPUT.PUT_LINE('Namespace: ' || G_NSPACE);
			  DBMS_OUTPUT.PUT_LINE('Expanded Name: ' || DBMS_XMLDOM.GETEXPANDEDNAME(G_XMLELEM));
			  -- Free resources associated with the CLOB and Parser now they are no longer needed.
			  dbms_lob.freetemporary(G_CLOB);
			  DBMS_XMLPARSER.FREEPARSER(G_PARSER);
			  -- The following statement shall search for a node starting with 'data/profile'
			  G_CURRENT_ITEM_LIST := DBMS_XSLPROCESSOR.SELECTNODES(DBMS_XMLDOM.MAKENODE(G_MY_DOC),'data/profile');
			  
			  G_PROFILE_COUNT := DBMS_XMLDOM.GETLENGTH(G_CURRENT_ITEM_LIST);
			  
			  DBMS_OUTPUT.PUT_LINE('Count of data/profile : '||G_PROFILE_COUNT); 
			  
			  INSERT INTO TRU_TMP_XML_SPLIT(FILE_NAME,TOTAL_REC,JOB_NAME)
					VALUES (I.FILE_NAME,G_PROFILE_COUNT,'PROF_MIG');
			  commit;
			  
			  G_PROFILE_COUNT := G_PROFILE_COUNT-1;
			  
			  --The following IF block decides to split XML file and load the data into staging tables
			  IF G_PROFILE_COUNT < 0 THEN
				RAISE_APPLICATION_ERROR(-20102, 'There is no record in XML file :'||I.FILE_NAME);
			  ELSIF
				 G_PROFILE_COUNT <= L_SPLIT_THREAD THEN 
 
				 PROFILE_XML_STAGE_LOAD(P_START_NO => 0, P_END_NO => G_PROFILE_COUNT);
			  
			  ELSIF 
			    G_PROFILE_COUNT > L_SPLIT_THREAD 
			  THEN
			    L_SPLIT_NUM := G_PROFILE_COUNT/L_SPLIT_THREAD;
				L_START_NUM := 0;

				  FOR J IN 1..L_SPLIT_THREAD
				  LOOP
					INSERT INTO TRU_TMP_XML_SPLIT(FILE_NAME,SI_NO,START_NO,END_NO,JOB_NAME)
					VALUES (I.FILE_NAME,J,L_START_NUM,L_SPLIT_NUM*J,'PROF_MIG_'||J);
					L_START_NUM := L_SPLIT_NUM*J+1;
				  END LOOP;
				  
				  COMMIT;
				  FOR K IN (SELECT SI_NO,JOB_NAME,START_NO,END_NO FROM TRU_TMP_XML_SPLIT WHERE FILE_NAME=I.FILE_NAME  AND JOB_NAME LIKE '%PROF_MIG_%' order by SI_NO)
				  LOOP        
							 
						   --Schedule the job to run in multiple seesion.
							-- DBMS_OUTPUT.PUT_LINE('scheduler start'); 
							--DBMS_SCHEDULER.CREATE_JOB(JOB_NAME => K.JOB_NAME,
							--                           JOB_TYPE => 'PLSQL_BLOCK',
							--                           JOB_ACTION => 'begin TRU_PROFILE_MIG_PKG.PROFILE_XML_STAGE_LOAD('||K.START_NO||','||K.END_NO||'); end;',
							--                           COMMENTS => 'Prof_Mig Thread'
							--                          );

							-- DBMS_OUTPUT.PUT_LINE('scheduler end');
							 
							-- DBMS_SCHEDULER.RUN_JOB(K.JOB_NAME,TRUE);
							-- DBMS_SCHEDULER.DROP_JOB('PROF_MIG');
					   DBMS_OUTPUT.PUT_LINE('-----------------xm split execution---------'||K.START_NO||','||K.END_NO);
						
					   PROFILE_XML_STAGE_LOAD(P_START_NO => K.START_NO, P_END_NO => K.END_NO);   
					   
							
				  END LOOP;
				  DBMS_XMLDOM.FREEDOCUMENT(G_MY_DOC);
	  
			  END IF;
            
		END LOOP;	

     DBMS_XMLDOM.FREEDOCUMENT(G_MY_DOC);
           
           

		 G_END :=DBMS_UTILITY.GET_TIME;
    DBMS_OUTPUT.PUT_LINE('PROFILE_MIG_STAGE_MAIN: Time Taken to execute ' || (G_END-G_START)/100||' '||'seconds');
		
	  EXCEPTION WHEN OTHERS THEN
		RAISE;
		
	END PROFILE_MIG_STAGE_MAIN;
	
	
	PROCEDURE PROFILE_XML_STAGE_LOAD
	( 
	  P_START_NO NUMBER, 
	  P_END_NO NUMBER
	)
    IS

		L_CURRENT_ITEM_LIST1 DBMS_XMLDOM.DOMNODELIST; 
		L_CURRENT_ITEM DBMS_XMLDOM.DOMNODE;           
		L_CURRENT_ITEM1 DBMS_XMLDOM.DOMNODE;   
		L_CURRENT_ITEM_LIST2 DBMS_XMLDOM.DOMNODELIST; 
		L_CURRENT_ITEM2 DBMS_XMLDOM.DOMNODE;  
		l_START number;
		l_end number;
	 
	 

  ---Record type for User address information

	  type L_USR_ADDRS_RCRD is RECORD
		(  
		  ADDRESS_ID      TRU_TEMP_PROFILE_ADDRESS.ADDRESS_ID%TYPE,
		  NICK_NAME       TRU_TEMP_PROFILE_ADDRESS.NICK_NAME%TYPE,
		  FIRST_NAME      TRU_TEMP_PROFILE_ADDRESS.FIRST_NAME%TYPE,
		  LAST_NAME       TRU_TEMP_PROFILE_ADDRESS.LAST_NAME%TYPE,
		  ADDRESS1        TRU_TEMP_PROFILE_ADDRESS.ADDRESS1%TYPE,
		  ADDRESS2        TRU_TEMP_PROFILE_ADDRESS.ADDRESS2%TYPE,
		  ADDRESS3        TRU_TEMP_PROFILE_ADDRESS.ADDRESS3%TYPE,
		  COUNTY      	  TRU_TEMP_PROFILE_ADDRESS.COUNTY%TYPE, 
		  CITY       	  TRU_TEMP_PROFILE_ADDRESS.CITY%TYPE,
		  STATE       	  TRU_TEMP_PROFILE_ADDRESS.STATE%TYPE,
		  POSTAL_CODE 	  TRU_TEMP_PROFILE_ADDRESS.POSTAL_CODE%TYPE, 
		  PHONE_NUMBER 	  TRU_TEMP_PROFILE_ADDRESS.PHONE_NUMBER%TYPE, 
		  COUNTRY         TRU_TEMP_PROFILE_ADDRESS.COUNTRY%TYPE, 
		  EMAIL       	  TRU_TEMP_PROFILE_ADDRESS.EMAIL%TYPE
	  );

	 type USER_ADDRS_COL  is table of L_USR_ADDRS_RCRD index by pls_integer; 
	 
	 L_ADDRS_INFO USER_ADDRS_COL;

	---Record type for User credit card information
	 
   TYPE L_USR_CC_RCRD IS RECORD
		(  
		  CREDIT_CARD_ID      	TRU_TEMP_PROFILE_CREDITCARD.CREDIT_CARD_ID%TYPE, 
		  NAME_ON_CARD      	TRU_TEMP_PROFILE_CREDITCARD.NAME_ON_CARD%TYPE,
		  EXPIRATION_MONTH      TRU_TEMP_PROFILE_CREDITCARD.EXPIRATION_MONTH%TYPE,
		  CREDIT_CARD_NUMBER    TRU_TEMP_PROFILE_CREDITCARD.CREDIT_CARD_NUMBER%TYPE,
		  BILLING_ADDRESS      	TRU_TEMP_PROFILE_CREDITCARD.BILLING_ADDRESS%TYPE,
		  EXPIRATION_YEAR      	TRU_TEMP_PROFILE_CREDITCARD.EXPIRATION_YEAR%TYPE,
		  CREDIT_CARD_TYPE      TRU_TEMP_PROFILE_CREDITCARD.CREDIT_CARD_TYPE%TYPE
		);
   TYPE USER_CC_COL  IS TABLE OF L_USR_CC_RCRD INDEX BY PLS_INTEGER;  
   L_CC_INFO USER_CC_COL;
   
   	---Record type for User information

	  type L_USR_RCRD is RECORD  
	  (
		USER_ID                 	TRU_TEMP_PROFILE_USERINFO.USER_ID%TYPE,
		FIRST_NAME              	TRU_TEMP_PROFILE_USERINFO.FIRST_NAME%TYPE,
		LAST_NAME               	TRU_TEMP_PROFILE_USERINFO.LAST_NAME%TYPE,
		LASTPWDUPDATE           	VARCHAR2(255),
		REGISTRATION_DATE       	VARCHAR2(255),
		EMAIL                   	TRU_TEMP_PROFILE_USERINFO.EMAIL%TYPE,
		DEFAULT_BILLING_ADDRESS 	TRU_TEMP_PROFILE_USERINFO.DEFAULT_BILLING_ADDRESS%TYPE,
		DEFAULT_SHIPPING_ADDRESS 	TRU_TEMP_PROFILE_USERINFO.DEFAULT_SHIPPING_ADDRESS%TYPE,
		DEFAULT_CREDITCARD      	TRU_TEMP_PROFILE_USERINFO.DEFAULT_CREDITCARD%TYPE,
		SECONDARY_ADDRESSES     	TRU_TEMP_PROFILE_USERINFO.SECONDARY_ADDRESSES%TYPE,
		REWARD_NUMBER 			    TRU_TEMP_PROFILE_USERINFO.REWARD_NUMBER%TYPE,
		STATUS		  				VARCHAR2(255),
		LASTACTIVITY_DATE			VARCHAR2(255),
		LOCALE						VARCHAR2(255),
		ADDRESS                 	USER_ADDRS_COL,
		CREDITCARD              	USER_CC_COL
	 );
	 
	 type USER_COL  is table of L_USR_RCRD index by pls_integer; 
	 
	 L_USER_INFO USER_COL;
 

   
    BEGIN
		
		l_START :=DBMS_UTILITY.GET_TIME;
		
				 --DBMS_OUTPUT.PUT_LINE('#############     user info loop begin #################');
		  FOR CUR_ENT IN P_START_NO .. P_END_NO
		  LOOP 
      
			L_CURRENT_ITEM := DBMS_XMLDOM.ITEM(G_CURRENT_ITEM_LIST, CUR_ENT); 
			
			--DBMS_OUTPUT.PUT_LINE('L_CURRENT_ITEM:'|| DBMS_XMLDOM.GETNODENAME(L_CURRENT_ITEM));
			
			--#############-------------data/profile/userInfo----------------##############
      --DBMS_OUTPUT.PUT_LINE('---------------userInfo----------------');
			
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'userInfo/property[@name="id"]',L_USER_INFO(CUR_ENT).USER_ID);
			--DBMS_OUTPUT.PUT_LINE('property[@name="id"]: '||L_USER_INFO(CUR_ENT).USER_ID);
			 
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'userInfo/property[@name="firstName"]',L_USER_INFO(CUR_ENT).FIRST_NAME);
			--DBMS_OUTPUT.PUT_LINE('property[@name="firstName"]: '||L_USER_INFO(CUR_ENT).FIRST_NAME);
			
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'userInfo/property[@name="lastName"]',L_USER_INFO(CUR_ENT).LAST_NAME);
			--DBMS_OUTPUT.PUT_LINE('property[@name="lastName"]: '||L_USER_INFO(CUR_ENT).LAST_NAME);
			
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'userInfo/property[@name="lastProfileUpdate"]',L_USER_INFO(CUR_ENT).LASTPWDUPDATE);
			--DBMS_OUTPUT.PUT_LINE('property[@name="lastProfileUpdate"]: '||L_USER_INFO(CUR_ENT).LASTPWDUPDATE);
			
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'userInfo/property[@name="registrationDate"]',L_USER_INFO(CUR_ENT).REGISTRATION_DATE);
			--DBMS_OUTPUT.PUT_LINE('property[@name="registrationDate"]: '||L_USER_INFO(CUR_ENT).REGISTRATION_DATE);
			
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'userInfo/property[@name="email"]',L_USER_INFO(CUR_ENT).EMAIL);
			--DBMS_OUTPUT.PUT_LINE('property[@name="email"]: '||L_USER_INFO(CUR_ENT).EMAIL);
			
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'userInfo/property[@name="defaultBillingAddress"]',L_USER_INFO(CUR_ENT).DEFAULT_BILLING_ADDRESS);
			--dbms_output.put_line('property[@name="defaultBillingAddress"]: '||l_user_info(CUR_ENT).DEFAULT_BILLING_ADDRESS);
			
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'userInfo/property[@name="defaultShippingAddress"]',L_USER_INFO(CUR_ENT).DEFAULT_SHIPPING_ADDRESS);
			--dbms_output.put_line('property[@name="defaultShippingAddress"]: '||l_user_info(CUR_ENT).DEFAULT_SHIPPING_ADDRESS);
			
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'userInfo/property[@name="defaultCreditCard"]',L_USER_INFO(CUR_ENT).DEFAULT_CREDITCARD);
			--DBMS_OUTPUT.PUT_LINE('property[@name="defaultCreditCard"]: '||L_USER_INFO(CUR_ENT).DEFAULT_CREDITCARD);
			
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'userInfo/property[@name="secondaryAddresses"]',L_USER_INFO(CUR_ENT).SECONDARY_ADDRESSES);
			--DBMS_OUTPUT.PUT_LINE('property[@name="secondaryAddresses"]: '||L_USER_INFO(CUR_ENT).SECONDARY_ADDRESSES); 
			
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'userInfo/property[@name="rewardNumber"]',L_USER_INFO(CUR_ENT).REWARD_NUMBER);
			--DBMS_OUTPUT.PUT_LINE('property[@name="rewardNumber"]: '||L_USER_INFO(CUR_ENT).REWARD_NUMBER); 
			
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'userInfo/property[@name="lastLoginDate"]',L_USER_INFO(CUR_ENT).LASTACTIVITY_DATE);
			
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'userInfo/property[@name="profileStatus"]',L_USER_INFO(CUR_ENT).STATUS);
			
			DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM,'userInfo/property[@name="locale"]',L_USER_INFO(CUR_ENT).LOCALE);
		
     
		   L_CURRENT_ITEM_LIST1 := DBMS_XSLPROCESSOR.SELECTNODES(L_CURRENT_ITEM,'addresses/address');
		   -- DBMS_OUTPUT.PUT_LINE('#############     user ADDRESS info loop begin #################');
		--	DBMS_OUTPUT.PUT_LINE('Count of data/profile/addresses : '||DBMS_XMLDOM.GETLENGTH(L_CURRENT_ITEM_LIST1)); -- find the no of entries in this node
        FOR CUR_ADD IN 0 .. DBMS_XMLDOM.GETLENGTH(L_CURRENT_ITEM_LIST1)-1
        LOOP 
          L_CURRENT_ITEM1 := DBMS_XMLDOM.ITEM(L_CURRENT_ITEM_LIST1, CUR_ADD); --READ the current item from current entry of <addresses>
          --DBMS_OUTPUT.PUT_LINE('L_current_item1:'|| DBMS_XMLDOM.GETNODENAME(L_CURRENT_ITEM1));
          --#############-------------data/profile/addresses/address----------------##############
         --DBMS_OUTPUT.PUT_LINE('---------------address----------------');
          
          DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM1,'property[@name="id"]',L_USER_INFO(CUR_ENT).ADDRESS(CUR_ADD).ADDRESS_ID);
         -- DBMS_OUTPUT.PUT_LINE('adrres_id : '||L_USER_INFO(CUR_ENT).ADDRESS(CUR_ADD).ADDRESS_ID);
          DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM1,'property[@name="nickName"]',L_USER_INFO(CUR_ENT).ADDRESS(CUR_ADD).NICK_NAME);
          DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM1,'property[@name="firstName"]',L_USER_INFO(CUR_ENT).ADDRESS(CUR_ADD).FIRST_NAME);
          DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM1,'property[@name="lastName"]',L_USER_INFO(CUR_ENT).ADDRESS(CUR_ADD).LAST_NAME);
          DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM1,'property[@name="address1"]',L_USER_INFO(CUR_ENT).ADDRESS(CUR_ADD).address1);
          DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM1,'property[@name="address2"]',L_USER_INFO(CUR_ENT).ADDRESS(CUR_ADD).address2);
          DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM1,'property[@name="address3"]',L_USER_INFO(CUR_ENT).ADDRESS(CUR_ADD).address3);
          DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM1,'property[@name="county"]',L_USER_INFO(CUR_ENT).ADDRESS(CUR_ADD).county);
          DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM1,'property[@name="city"]',L_USER_INFO(CUR_ENT).ADDRESS(CUR_ADD).city);
          DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM1,'property[@name="state"]',L_USER_INFO(CUR_ENT).ADDRESS(CUR_ADD).STATE);
          DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM1,'property[@name="postalCode"]',L_USER_INFO(CUR_ENT).ADDRESS(CUR_ADD).POSTAL_CODE);
          DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM1,'property[@name="phoneNumber"]',L_USER_INFO(CUR_ENT).ADDRESS(CUR_ADD).phone_Number);
          DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM1,'property[@name="country"]',L_USER_INFO(CUR_ENT).ADDRESS(CUR_ADD).COUNTRY);
          DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM1,'property[@name="email"]',L_USER_INFO(CUR_ENT).ADDRESS(CUR_ADD).email);
          
        END LOOP;
        
        L_CURRENT_ITEM_LIST2 := DBMS_XSLPROCESSOR.SELECTNODES(L_CURRENT_ITEM,'creditCards/creditCard');
         -- DBMS_OUTPUT.PUT_LINE('#############     user creditcard info loop begin #################');
      --	DBMS_OUTPUT.PUT_LINE('Count of data/profile/creditCards : '||DBMS_XMLDOM.GETLENGTH(L_CURRENT_ITEM_LIST2)); -- find the no of entries in this node
        FOR CUR_CC IN 0 .. DBMS_XMLDOM.GETLENGTH(L_CURRENT_ITEM_LIST2)-1
        LOOP 
          L_CURRENT_ITEM2 := DBMS_XMLDOM.ITEM(L_CURRENT_ITEM_LIST2, CUR_CC); --READ the current item from current entry of <creditCards>
          --DBMS_OUTPUT.PUT_LINE('L_current_item1:'|| DBMS_XMLDOM.GETNODENAME(L_CURRENT_ITEM1));
          --#############-------------data/profile/addresses/address----------------##############
         --DBMS_OUTPUT.PUT_LINE('---------------address----------------');
          
          DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM2,'property[@name="id"]',L_USER_INFO(CUR_ENT).CREDITCARD(CUR_CC).CREDIT_CARD_ID);
          --DBMS_OUTPUT.PUT_LINE('USER_ID : '||L_CC_INFO(L_CC_COUNT).USER_ID);
          --DBMS_OUTPUT.PUT_LINE('CREDIT_CARD_ID : '||L_CC_INFO(L_CC_COUNT).CREDIT_CARD_ID);
         DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM2,'property[@name="nameOnCard"]',L_USER_INFO(CUR_ENT).CREDITCARD(CUR_CC).NAME_ON_CARD);
         DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM2,'property[@name="id"]',L_USER_INFO(CUR_ENT).CREDITCARD(CUR_CC).CREDIT_CARD_ID);
         DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM2,'property[@name="expirationMonth"]',L_USER_INFO(CUR_ENT).CREDITCARD(CUR_CC).EXPIRATION_MONTH);
         DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM2,'property[@name="creditCardNumber"]',L_USER_INFO(CUR_ENT).CREDITCARD(CUR_CC).CREDIT_CARD_NUMBER);
         DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM2,'property[@name="billingAddress"]',L_USER_INFO(CUR_ENT).CREDITCARD(CUR_CC).BILLING_ADDRESS);
         DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM2,'property[@name="expirationYear"]',L_USER_INFO(CUR_ENT).CREDITCARD(CUR_CC).EXPIRATION_YEAR);
         DBMS_XSLPROCESSOR.VALUEOF(L_CURRENT_ITEM2,'property[@name="creditCardType"]',L_USER_INFO(CUR_ENT).CREDITCARD(CUR_CC).CREDIT_CARD_TYPE);
         
        END LOOP;
      
      end loop;
 
     FORALL i in P_START_NO..P_END_NO
      INSERT INTO TRU_TEMP_PROFILE_USERINFO
      (		
			USER_ID,
			FIRST_NAME,
			LAST_NAME,
			LASTPWDUPDATE,
			REGISTRATION_DATE,
			EMAIL,
			DEFAULT_BILLING_ADDRESS,
			DEFAULT_SHIPPING_ADDRESS,
			DEFAULT_CREDITCARD,
			SECONDARY_ADDRESSES,
			REWARD_NUMBER,
			LASTACTIVITY_DATE,
			STATUS,
			LOCALE
      )
      VALUES
      (
			L_USER_INFO(I).USER_ID,
			L_USER_INFO(I).FIRST_NAME,
			L_USER_INFO(I).LAST_NAME,
			TO_TIMESTAMP(L_USER_INFO(I).LASTPWDUPDATE,'dd-mm-yy hh24:mi:ss'),
			TO_TIMESTAMP(L_USER_INFO(I).REGISTRATION_DATE,'dd-mm-yy hh24:mi:ss'),
			L_USER_INFO(I).EMAIL,
			L_USER_INFO(I).DEFAULT_BILLING_ADDRESS,
			L_USER_INFO(I).DEFAULT_SHIPPING_ADDRESS,
			L_USER_INFO(I).DEFAULT_CREDITCARD,
			L_USER_INFO(I).SECONDARY_ADDRESSES,
			L_USER_INFO(I).REWARD_NUMBER,
			TO_TIMESTAMP(L_USER_INFO(I).LASTACTIVITY_DATE,'dd-mm-yy hh24:mi:ss'),
			DECODE(L_USER_INFO(I).STATUS,'true',1,'false',0),
			DECODE(L_USER_INFO(I).LOCALE ,'unset',0,
					'en_US',1,
					'fr_FR',2,
					'ja_JP',3,
					'de_DE',4,
					'zh_CN',2000,
					'nl',2001,
					'en',2002,
					'en_AU',2003,
					'en_CA',2004,
					'en_GB',2005,
					'fr',2006,
					'de',2007,
					'it',2008,
					'ja',2009,
					'pt_BR',2010,
					'es',2011,
					'sv',2012,
					'el_GR',2013,
					'tr_TR',2014,
					'ko_KR',2015,
					'zh_TW',2016,
					'ru_RU',2017,
					'hu_HU',2018,
					'cs_CZ',2019,
					'pl_PL',2020,
					'da_DK',2021,
					'fi_FI',2022,
					'pt_PT',2023,
					'th',2024,0
			)
      )
     LOG ERRORS INTO DML_ERROR_LOG ('TRU_TEMP_PROFILE_USERINFO :'||G_FILE_NAME) REJECT LIMIT UNLIMITED;
     
     COMMIT;


    FOR I IN P_START_NO..P_END_NO
    LOOP
     FORALL j IN l_USER_INFO(i).ADDRESS.FIRST..L_USER_INFO(i).ADDRESS.LAST
      INSERT INTO TRU_TEMP_PROFILE_ADDRESS
		(
		 	USER_ID,
			ADDRESS_ID,
			NICK_NAME,
			FIRST_NAME,
			LAST_NAME,
			address1,
			ADDRESS2,
			ADDRESS3,
			COUNTY,
			city,
			STATE,
			POSTAL_CODE,
			PHONE_NUMBER,
			COUNTRY,
			EMAIL,
			STATUS,
			LAST_ACTIVITY
		)
      VALUES
      (
			 L_USER_INFO(I).USER_ID,
			 L_USER_INFO(I).ADDRESS(j).ADDRESS_ID,
			 L_USER_INFO(I).ADDRESS(j).NICK_NAME,
			 L_USER_INFO(I).ADDRESS(j).FIRST_NAME,
			 L_USER_INFO(I).ADDRESS(j).LAST_NAME,
			 L_USER_INFO(I).ADDRESS(j).ADDRESS1,
			 L_USER_INFO(I).ADDRESS(j).ADDRESS2,
			 L_USER_INFO(I).ADDRESS(j).ADDRESS3,
			 L_USER_INFO(I).ADDRESS(j).COUNTY,
			 L_USER_INFO(I).ADDRESS(j).CITY,
			 L_USER_INFO(I).ADDRESS(j).STATE,
			 L_USER_INFO(I).ADDRESS(j).POSTAL_CODE,
			 L_USER_INFO(I).ADDRESS(j).PHONE_NUMBER,
			 L_USER_INFO(I).ADDRESS(j).COUNTRY,
			 L_USER_INFO(I).ADDRESS(j).EMAIL,
			 DECODE(L_USER_INFO(I).STATUS,'true',1,'false',0),
			 TO_TIMESTAMP(L_USER_INFO(I).LASTACTIVITY_DATE,'dd-mm-yy hh24:mi:ss')
      ) 
	  LOG ERRORS INTO DML_ERROR_LOG ('TRU_TEMP_PROFILE_ADDRESS:'||G_FILE_NAME) REJECT LIMIT UNLIMITED;
    
     FORALL j IN l_USER_INFO(i).CREDITCARD.FIRST..L_USER_INFO(i).CREDITCARD.LAST
      INSERT INTO TRU_TEMP_PROFILE_CREDITCARD
		(
			USER_ID, 
			CREDIT_CARD_ID,
			NAME_ON_CARD,
			EXPIRATION_MONTH,
			CREDIT_CARD_NUMBER,
			BILLING_ADDRESS,
			EXPIRATION_YEAR,
			CREDIT_CARD_TYPE,
			STATUS
		)
      VALUES
      (
        L_USER_INFO(I).USER_ID, 
        L_USER_INFO(I).CREDITCARD(J).CREDIT_CARD_ID,
        L_USER_INFO(I).CREDITCARD(J).NAME_ON_CARD,
        L_USER_INFO(I).CREDITCARD(J).EXPIRATION_MONTH,
        L_USER_INFO(I).CREDITCARD(J).CREDIT_CARD_NUMBER,
        L_USER_INFO(I).CREDITCARD(J).BILLING_ADDRESS,
        L_USER_INFO(I).CREDITCARD(J).EXPIRATION_YEAR,
        L_USER_INFO(I).CREDITCARD(J).CREDIT_CARD_TYPE,
		DECODE(L_USER_INFO(I).STATUS,'true',1,'false',0)
      ) 
	  LOG ERRORS INTO DML_ERROR_LOG ('TRU_TEMP_PROFILE_CREDITCARD:'||G_FILE_NAME) REJECT LIMIT UNLIMITED;
    
    END LOOP;
    
    COMMIT;

		L_END :=DBMS_UTILITY.GET_TIME;
        DBMS_OUTPUT.PUT_LINE('PROFILE_XML_STAGE_LOAD: Time Taken to execute ' || (L_END-l_START)/100||' '||'seconds');
		
		EXCEPTION
		WHEN OTHERS THEN
		  DBMS_OUTPUT.PUT_LINE('sqlerrm :'||SQLERRM);
		  dbms_lob.close(G_bfile);
		  DBMS_LOB.FREETEMPORARY(G_CLOB);
		  DBMS_XMLPARSER.FREEPARSER(G_PARSER);
		  DBMS_XMLDOM.FREEDOCUMENT(G_MY_DOC);
		  RAISE;
	END PROFILE_XML_STAGE_LOAD;
	
	PROCEDURE PROFILE_MIG_TARGT(P_LAOD_TYPE  VARCHAR2)
    IS
    BEGIN
		
		G_START :=DBMS_UTILITY.GET_TIME;
		
		IF LOWER(P_LAOD_TYPE) not in('initial' ,'delta') or P_LAOD_TYPE is null
		THEN
			
			RAISE_APPLICATION_ERROR(-20103, 'load type should be either initial or delta ' );
			
		END IF;
		
		IF lower(P_LAOD_TYPE) = 'initial'
		THEN
			
			INSERT INTO DPS_USER 
			(
				ID,
				LOGIN,
				AUTO_LOGIN,
				PASSWORD,
				PASSWORD_SALT,
				PASSWORD_KDF,
				--REALM_ID,
				MEMBER,
				FIRST_NAME,
				--MIDDLE_NAME,
				LAST_NAME,
				--USER_TYPE,
				LOCALE,
				LASTACTIVITY_DATE,
				LASTPWDUPDATE,
				GENERATEDPWD,
				REGISTRATION_DATE,
				EMAIL,
				EMAIL_STATUS,
				RECEIVE_EMAIL
				--LAST_EMAILED,
				--GENDER,
				--DATE_OF_BIRTH,
				--SECURITYSTATUS,
				--DESCRIPTION
			)
		    SELECT 
				USER_ID,
				EMAIL,
				0,
				G_PASWD,
				G_SALT_PASWD,
				5,
				--NULL
				0,
				FIRST_NAME,
				--NULL
				LAST_NAME,
				--NULL
				LOCALE,
				LASTACTIVITY_DATE,
				LASTPWDUPDATE,
				0,
				REGISTRATION_DATE,
				EMAIL,
				0,
				1
				--NULL
				--NULL
				--NULL
				--NULL
				--NULL
			FROM TRU_TEMP_PROFILE_USERINFO WHERE STATUS =1
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER load completed');
			
			INSERT INTO TRU_DPSX_USER
			(
				ID,
				REWARD_NUMBER
				--LOYAL_CUST,
			)
			SELECT 
				 USER_ID,
				 REWARD_NUMBER 
				 --NULL
			FROM TRU_TEMP_PROFILE_USERINFO WHERE STATUS =1
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_USER') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_USER load completed');
			
			INSERT INTO DPS_CONTACT_INFO 
			(
				ID,                   
				USER_ID,
				--PREFIX,
				FIRST_NAME,
				--MIDDLE_NAME,
				LAST_NAME,
				--SUFFIX,
				--JOB_TITLE,
				--COMPANY_NAME,
				ADDRESS1,
				ADDRESS2,
				ADDRESS3,
				CITY,
				STATE,
				POSTAL_CODE,
				COUNTY,
				COUNTRY,
				PHONE_NUMBER
				--FAX_NUMBER
			)
		    SELECT
				ADDRESS_ID,
				USER_ID,
				--NULL
				FIRST_NAME,
				--NULL
				LAST_NAME,
				--NULL
				--NULL
				--NULL
				address1,
				ADDRESS2,
				ADDRESS3,
				city,
				STATE,
				POSTAL_CODE,
				COUNTY,
				country,
				PHONE_NUMBER
				--NULL
			FROM TRU_TEMP_PROFILE_ADDRESS WHERE STATUS =1
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_CONTACT_INFO') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_CONTACT_INFO load completed');
			
			INSERT INTO TRU_DPSX_CONTACT_INFO
			(
				ID,
				LAST_ACTIVITY,
				STATUS_CD
			)
			SELECT ADDRESS_ID,
				   LAST_ACTIVITY,
				   10
			FROM TRU_TEMP_PROFILE_ADDRESS WHERE STATUS =1
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CONTACT_INFO') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CONTACT_INFO load completed');
	
			INSERT INTO DPS_CREDIT_CARD
			(
				ID,
				CREDIT_CARD_NUMBER,
				CREDIT_CARD_TYPE,
				EXPIRATION_MONTH,
				--EXP_DAY_OF_MONTH,
				EXPIRATION_YEAR,
				BILLING_ADDR
			)
			SELECT 
				CREDIT_CARD_ID,
				CREDIT_CARD_NUMBER,
				CREDIT_CARD_TYPE,
				EXPIRATION_MONTH,
				--NONE
				EXPIRATION_YEAR,
				BILLING_ADDRESS	 
			FROM TRU_TEMP_PROFILE_CREDITCARD WHERE STATUS =1
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_CREDIT_CARD') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			

			
			DBMS_OUTPUT.PUT_LINE ('DPS_CREDIT_CARD load completed');
			
			
			INSERT INTO TRU_DPSX_CREDIT_CARD
			(
				ID,
				NAME_ON_CARD
				--CREDIT_CARD_TOKEN,
				--LAST_ACTIVITY
			)
			SELECT 
				   CREDIT_CARD_ID,
				   NAME_ON_CARD
				   --NULL,
				   --NULL
			FROM  TRU_TEMP_PROFILE_CREDITCARD WHERE STATUS =1
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CREDIT_CARD') REJECT LIMIT UNLIMITED;
				   
			COMMIT;
										
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CREDIT_CARD load completed');
			
			INSERT INTO DPS_USER_ADDRESS
			(
				ID,
				--HOME_ADDR_ID,
				BILLING_ADDR_ID,
				SHIPPING_ADDR_ID
			)
			SELECT 
				USER_ID,
				--NULL
				DEFAULT_BILLING_ADDRESS,
				DEFAULT_SHIPPING_ADDRESS
			FROM TRU_TEMP_PROFILE_USERINFO WHERE STATUS =1
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER_ADDRESS') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER_ADDRESS load completed');
		
			INSERT INTO DPS_OTHER_ADDR
			(
				USER_ID,
				TAG,
				ADDRESS_ID
			)
			SELECT
				USER_ID,
				NICK_NAME,
				ADDRESS_ID	
			FROM TRU_TEMP_PROFILE_ADDRESS WHERE STATUS =1
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_OTHER_ADDR') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_OTHER_ADDR load completed');
			
			INSERT INTO DPS_USR_CREDITCARD
			(
				USER_ID,
				TAG,
				CREDIT_CARD_ID
			)
			SELECT 
				USER_ID,
				CREDIT_CARD_TYPE,
				CREDIT_CARD_ID

			FROM TRU_TEMP_PROFILE_CREDITCARD WHERE STATUS =1
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USR_CREDITCARD') REJECT LIMIT UNLIMITED;
			
			COMMIT;

			DBMS_OUTPUT.PUT_LINE ('DPS_USR_CREDITCARD load completed');
		END IF;
		
	    IF lower(P_LAOD_TYPE) = 'delta'
		THEN
			
			MERGE INTO DPS_USER A
			USING
				(
				    SELECT 
						USER_ID,
						EMAIL,
						--0 AUTO_LOGIN,
						--'ENCRIPT PSWD' PASSWORD
						--'ENCRIPT PSWD' PASSWORD_SALT
						--5 PASSWORD_KDF,
						--NULL REALM_ID
						--0 MEMBER,
						FIRST_NAME,
						--NULL MIDDLE_NAME
						LAST_NAME,
						--NULL USER_TYPE
						LOCALE,
						LASTACTIVITY_DATE,
						LASTPWDUPDATE,
						--0 GENERATEDPWD,
						REGISTRATION_DATE
						--EMAIL,
						--0 EMAIL_STATUS,
						--1 RECEIVE_EMAIL
						--NULL LAST_EMAILED
						--NULL GENDER
						--NULL DATE_OF_BIRTH
						--NULL SECURITYSTATUS
						--NULL DESCRIPTION
					FROM TRU_TEMP_PROFILE_USERINFO WHERE STATUS =1
				) B
			ON (A.ID=B.USER_ID)
			WHEN MATCHED THEN
				UPDATE SET A.LOGIN = B.EMAIL ,A.FIRST_NAME = B.FIRST_NAME ,A.LAST_NAME = B.LAST_NAME ,A.LASTPWDUPDATE = B.LASTPWDUPDATE ,A.REGISTRATION_DATE = B.REGISTRATION_DATE ,A.EMAIL = B.EMAIL,A.LOCALE = B.LOCALE,A.LASTACTIVITY_DATE = B.LASTACTIVITY_DATE
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.LOGIN,A.AUTO_LOGIN,A.PASSWORD,A.PASSWORD_SALT,A.PASSWORD_KDF,A.MEMBER,A.FIRST_NAME,A.LAST_NAME,A.LASTPWDUPDATE,A.GENERATEDPWD,A.REGISTRATION_DATE,A.EMAIL,A.EMAIL_STATUS,A.RECEIVE_EMAIL,A.LOCALE,A.LASTACTIVITY_DATE)
				VALUES (B.USER_ID,B.EMAIL,0,G_PASWD,G_SALT_PASWD,5,0,B.FIRST_NAME,B.LAST_NAME,B.LASTPWDUPDATE,0,B.REGISTRATION_DATE,B.EMAIL,0,1,B.LOCALE,B.LASTACTIVITY_DATE)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER') REJECT LIMIT UNLIMITED;
			
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER merge completed');
			
			MERGE INTO TRU_DPSX_USER A
			USING
				(
				    SELECT 
						 USER_ID,
						 REWARD_NUMBER 
						 --NULL LOYAL_CUST
					FROM TRU_TEMP_PROFILE_USERINFO WHERE STATUS =1
				) B
			ON (A.ID=B.USER_ID)
			WHEN MATCHED THEN
				UPDATE SET A.REWARD_NUMBER = B.REWARD_NUMBER 
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.REWARD_NUMBER)
				VALUES (B.USER_ID,B.REWARD_NUMBER)
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_USER') REJECT LIMIT UNLIMITED;

			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_USER merge completed');
			
			
						
			MERGE INTO DPS_CONTACT_INFO A
			USING
				(
				     SELECT
						ADDRESS_ID,
						USER_ID,
						--NULL PREFIX
						FIRST_NAME,
						--NULL MIDDLE_NAME
						LAST_NAME,
						--NULL SUFFIX
						--NULL JOB_TITLE
						--NULL COMPANY_NAME
						address1,
						ADDRESS2,
						ADDRESS3,
						city,
						STATE,
						POSTAL_CODE,
						COUNTY,
						country,
						PHONE_NUMBER
						--NULL FAX_NUMBER
					FROM TRU_TEMP_PROFILE_ADDRESS WHERE STATUS =1
				) B
			ON (A.ID=B.ADDRESS_ID)
			WHEN MATCHED THEN
				UPDATE SET A.USER_ID = B.USER_ID,A.FIRST_NAME = B.FIRST_NAME,A.LAST_NAME = B.LAST_NAME,A.ADDRESS1 = B.ADDRESS1,A.ADDRESS2 = B.ADDRESS2,A.ADDRESS3 = B.ADDRESS3,
						   A.CITY = B.CITY,A.STATE = B.STATE,A.POSTAL_CODE = B.POSTAL_CODE,A.COUNTY = B.COUNTY,A.country = B.country,A.PHONE_NUMBER = B.PHONE_NUMBER
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.USER_ID,A.FIRST_NAME,A.LAST_NAME,A.ADDRESS1,A.ADDRESS2,A.ADDRESS3,A.CITY,A.STATE,A.POSTAL_CODE,A.COUNTY,A.COUNTRY,A.PHONE_NUMBER)
				VALUES (B.ADDRESS_ID,B.USER_ID,B.FIRST_NAME,B.LAST_NAME,B.ADDRESS1,B.ADDRESS2,B.ADDRESS3,B.CITY,B.STATE,B.POSTAL_CODE,B.COUNTY,B.COUNTRY,B.PHONE_NUMBER)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_CONTACT_INFO') REJECT LIMIT UNLIMITED;
			
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_CONTACT_INFO merge completed');
			
			MERGE INTO TRU_DPSX_CONTACT_INFO A
			USING
				(
				     SELECT ADDRESS_ID,
							LAST_ACTIVITY
							--NULL STATUS_CD
					 FROM TRU_TEMP_PROFILE_ADDRESS WHERE STATUS =1
				) B
			ON (A.ID=B.ADDRESS_ID)
			WHEN MATCHED THEN
				UPDATE SET A.LAST_ACTIVITY = B.LAST_ACTIVITY
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.LAST_ACTIVITY,A.STATUS_CD)
				VALUES (B.ADDRESS_ID,B.LAST_ACTIVITY,10)
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CONTACT_INFO') REJECT LIMIT UNLIMITED;

			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CONTACT_INFO merge completed');
	
			MERGE INTO DPS_CREDIT_CARD A
			USING
				(
				     SELECT 
							CREDIT_CARD_ID,
							CREDIT_CARD_NUMBER,
							CREDIT_CARD_TYPE,
							EXPIRATION_MONTH,
							--NONE EXP_DAY_OF_MONTH
							EXPIRATION_YEAR,
							BILLING_ADDRESS	
				    FROM TRU_TEMP_PROFILE_CREDITCARD WHERE STATUS =1
				) B
			ON (A.ID=B.CREDIT_CARD_ID)
			WHEN MATCHED THEN
				UPDATE SET A.CREDIT_CARD_NUMBER = B.CREDIT_CARD_NUMBER,A.CREDIT_CARD_TYPE = B.CREDIT_CARD_TYPE,A.EXPIRATION_MONTH = B.EXPIRATION_MONTH,A.EXPIRATION_YEAR = B.EXPIRATION_YEAR,A.BILLING_ADDR = B.BILLING_ADDRESS
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.CREDIT_CARD_NUMBER,A.CREDIT_CARD_TYPE,A.EXPIRATION_MONTH,A.EXPIRATION_YEAR,A.BILLING_ADDR)
				VALUES (B.CREDIT_CARD_ID,B.CREDIT_CARD_NUMBER,B.CREDIT_CARD_TYPE,B.EXPIRATION_MONTH,B.EXPIRATION_YEAR,B.BILLING_ADDRESS)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_CREDIT_CARD') REJECT LIMIT UNLIMITED;	
			
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_CREDIT_CARD merge completed');
			
			MERGE INTO TRU_DPSX_CREDIT_CARD A
			USING
				(
				    SELECT 
						   CREDIT_CARD_ID,
						   NAME_ON_CARD
						   --NULL CREDIT_CARD_TOKEN,
						   --NULL LAST_ACTIVITY
					FROM  TRU_TEMP_PROFILE_CREDITCARD WHERE STATUS =1
				) B
			ON (A.ID=B.CREDIT_CARD_ID)
			WHEN MATCHED THEN
				UPDATE SET A.NAME_ON_CARD = B.NAME_ON_CARD
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.NAME_ON_CARD)
				VALUES (B.CREDIT_CARD_ID,B.NAME_ON_CARD)
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CREDIT_CARD') REJECT LIMIT UNLIMITED;	

				   
			COMMIT;
										
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CREDIT_CARD merge completed');
			
			MERGE INTO DPS_USER_ADDRESS A
			USING
				(
				    SELECT 
						USER_ID,
						--NULL HOME_ADDR_ID
						DEFAULT_BILLING_ADDRESS,
						DEFAULT_SHIPPING_ADDRESS
					FROM TRU_TEMP_PROFILE_USERINFO WHERE STATUS =1
				) B
			ON (A.ID=B.USER_ID)
			WHEN MATCHED THEN
				UPDATE SET A.BILLING_ADDR_ID = B.DEFAULT_BILLING_ADDRESS,A.SHIPPING_ADDR_ID = B.DEFAULT_SHIPPING_ADDRESS
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.BILLING_ADDR_ID,A.SHIPPING_ADDR_ID)
				VALUES (B.USER_ID,B.DEFAULT_BILLING_ADDRESS,B.DEFAULT_SHIPPING_ADDRESS)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER_ADDRESS') REJECT LIMIT UNLIMITED;	
			
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER_ADDRESS merge completed');
			
						
			MERGE INTO DPS_OTHER_ADDR A
			USING
				(
				    SELECT
						USER_ID,
						NICK_NAME,
						ADDRESS_ID	
					FROM TRU_TEMP_PROFILE_ADDRESS WHERE STATUS =1
				) B
			ON (A.USER_ID=B.USER_ID AND A.TAG = B.NICK_NAME)
			WHEN MATCHED THEN
				UPDATE SET A.ADDRESS_ID = B.ADDRESS_ID
			WHEN NOT MATCHED THEN
				INSERT (A.USER_ID,A.TAG,A.ADDRESS_ID)
				VALUES (B.USER_ID,B.NICK_NAME,B.ADDRESS_ID)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_OTHER_ADDR') REJECT LIMIT UNLIMITED;	

			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_OTHER_ADDR merge completed');
			
									
			MERGE INTO DPS_USR_CREDITCARD A
			USING
				(
				    SELECT 
						USER_ID,
						CREDIT_CARD_TYPE,
						CREDIT_CARD_ID
					FROM TRU_TEMP_PROFILE_CREDITCARD WHERE STATUS =1
				) B
			ON (A.USER_ID=B.USER_ID AND A.TAG = B.CREDIT_CARD_TYPE)
			WHEN MATCHED THEN
				UPDATE SET A.CREDIT_CARD_ID = B.CREDIT_CARD_ID
			WHEN NOT MATCHED THEN
				INSERT (A.USER_ID,A.TAG,A.CREDIT_CARD_ID)
				VALUES (B.USER_ID,B.CREDIT_CARD_TYPE,B.CREDIT_CARD_ID)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USR_CREDITCARD') REJECT LIMIT UNLIMITED;	
			
			
			COMMIT;

			DBMS_OUTPUT.PUT_LINE ('DPS_USR_CREDITCARD merge completed');
			

		END IF;
		
		DBMS_OUTPUT.PUT_LINE ('All Target table load completed');
		
		G_END :=DBMS_UTILITY.GET_TIME;
        DBMS_OUTPUT.PUT_LINE('PROFILE_MIG_TARGT: Time Taken to execute ' || (G_END-G_START)/100||' '||'seconds');
		
	  EXCEPTION WHEN OTHERS THEN
		RAISE;
		
	END PROFILE_MIG_TARGT;


	PROCEDURE PROFILE_MIG_TARGT_BATCH(P_LAOD_TYPE  VARCHAR2,P_COMMIT_INTRVL PLS_INTEGER)
    IS
		
		type l_rcrd is record 
			(
				USER_ID                 TRU_TEMP_PROFILE_USERINFO.USER_ID%type, 
				EMAIL                   TRU_TEMP_PROFILE_USERINFO.EMAIL%type,
				FIRST_NAME              TRU_TEMP_PROFILE_USERINFO.FIRST_NAME%type,
				LAST_NAME               TRU_TEMP_PROFILE_USERINFO.LAST_NAME%type,
				LOCALE					        TRU_TEMP_PROFILE_USERINFO.LOCALE%type,
				LASTACTIVITY_DATE		TRU_TEMP_PROFILE_USERINFO.LASTACTIVITY_DATE%type,
				LASTPWDUPDATE           TRU_TEMP_PROFILE_USERINFO.LASTPWDUPDATE%type,
				REGISTRATION_DATE       TRU_TEMP_PROFILE_USERINFO.REGISTRATION_DATE%type,
				REWARD_NUMBER 			TRU_TEMP_PROFILE_USERINFO.REWARD_NUMBER%type,
				DEFAULT_BILLING_ADDRESS	TRU_TEMP_PROFILE_USERINFO.DEFAULT_BILLING_ADDRESS%type,
				DEFAULT_SHIPPING_ADDRESS TRU_TEMP_PROFILE_USERINFO.DEFAULT_SHIPPING_ADDRESS%type
				
			);
		
		type l_rcrd1 is record 
			(
				ADDRESS_ID              TRU_TEMP_PROFILE_ADDRESS.ADDRESS_ID%type, 
				USER_ID                 TRU_TEMP_PROFILE_ADDRESS.USER_ID%type,
				FIRST_NAME              TRU_TEMP_PROFILE_ADDRESS.FIRST_NAME%type,
				LAST_NAME               TRU_TEMP_PROFILE_ADDRESS.LAST_NAME%type,
				address1           		TRU_TEMP_PROFILE_ADDRESS.address1%type,
				address2       			TRU_TEMP_PROFILE_ADDRESS.address2%type,
				address3 				TRU_TEMP_PROFILE_ADDRESS.address3%type,
				city					TRU_TEMP_PROFILE_ADDRESS.city%type,
				STATE					TRU_TEMP_PROFILE_ADDRESS.STATE%type,
				POSTAL_CODE				TRU_TEMP_PROFILE_ADDRESS.POSTAL_CODE%type,
				COUNTY					TRU_TEMP_PROFILE_ADDRESS.COUNTY%type,
				country					TRU_TEMP_PROFILE_ADDRESS.country%type,
				PHONE_NUMBER			TRU_TEMP_PROFILE_ADDRESS.PHONE_NUMBER%type,
				NICK_NAME				TRU_TEMP_PROFILE_ADDRESS.NICK_NAME%type,
				LAST_ACTIVITY		    TRU_TEMP_PROFILE_ADDRESS.LAST_ACTIVITY%type
			);
			
		type l_rcrd2 is record 
			(
				CREDIT_CARD_ID			TRU_TEMP_PROFILE_CREDITCARD.CREDIT_CARD_ID%TYPE,
				USER_ID             	TRU_TEMP_PROFILE_CREDITCARD.USER_ID%TYPE,
				CREDIT_CARD_NUMBER		TRU_TEMP_PROFILE_CREDITCARD.CREDIT_CARD_NUMBER%type,
				CREDIT_CARD_TYPE		TRU_TEMP_PROFILE_CREDITCARD.CREDIT_CARD_TYPE%type,
				EXPIRATION_MONTH		TRU_TEMP_PROFILE_CREDITCARD.EXPIRATION_MONTH%type,
				EXPIRATION_YEAR			TRU_TEMP_PROFILE_CREDITCARD.EXPIRATION_YEAR%type,
				BILLING_ADDRESS			TRU_TEMP_PROFILE_CREDITCARD.BILLING_ADDRESS%TYPE,
        NAME_ON_CARD        TRU_TEMP_PROFILE_CREDITCARD.NAME_ON_CARD%TYPE
			);
				
		type USER_COL  is table of l_rcrd index by pls_integer; 
		L_USER_INFO USER_COL;
		
		type ADDRS_COL  is table of l_rcrd1 index by pls_integer; 
		L_USER_ADDRS ADDRS_COL;
		
		type CC_COL  is table of l_rcrd2 index by pls_integer; 
		L_USER_CC CC_COL;
		
		ROWS PLS_INTEGER;
		
		L_DYN_CUR SYS_REFCURSOR;
		
		
    BEGIN
		
		G_START :=DBMS_UTILITY.GET_TIME;
		
		IF P_COMMIT_INTRVL IS NULL OR P_COMMIT_INTRVL < 10000
		THEN
			
			RAISE_APPLICATION_ERROR(-20102, 'commit inerval value should be greater than or equal to 10000' );
			
		END IF;
		
		ROWS := P_COMMIT_INTRVL;
		
		IF LOWER(P_LAOD_TYPE) not in('initial','delta') or  P_LAOD_TYPE is null
		THEN
			
			RAISE_APPLICATION_ERROR(-20103, 'load type should be either initial or delta ' );
			
		END IF;
		
		IF lower(P_LAOD_TYPE) = 'initial'
		THEN
			
			 OPEN L_DYN_CUR FOR  SELECT 
												USER_ID,
												EMAIL,
												--0 AUTO_LOGIN,
												--ENCRIPT PSWD PASSWORD
												--ENCRIPT PSWD PASSWORD_SALT
												--5 PASSWORD_KDF,
												--NULL REALM_ID
												--0 MEMBER,
												FIRST_NAME,
												--NULL MIDDLE_NAME
												LAST_NAME,
												--NULL USER_TYPE
												LOCALE,
												LASTACTIVITY_DATE,
												LASTPWDUPDATE,
												--0 GENERATEDPWD,
												REGISTRATION_DATE,
												--EMAIL,
												--0 EMAIL_STATUS,
												--1 RECEIVE_EMAIL
												--NULL LAST_EMAILED
												--NULL GENDER
												--NULL DATE_OF_BIRTH
												--NULL SECURITYSTATUS
												--NULL DESCRIPTION
												REWARD_NUMBER,
												DEFAULT_BILLING_ADDRESS,
												DEFAULT_SHIPPING_ADDRESS
											FROM TRU_TEMP_PROFILE_USERINFO WHERE STATUS=1
                    ;

             LOOP
                  FETCH L_DYN_CUR BULK COLLECT INTO L_USER_INFO LIMIT ROWS;
                  EXIT WHEN L_USER_INFO.count=0;
                  FORALL I IN L_USER_INFO.FIRST .. L_USER_INFO.LAST
					  INSERT INTO DPS_USER 
						(ID,LOGIN,AUTO_LOGIN,PASSWORD,PASSWORD_SALT,PASSWORD_KDF,MEMBER,FIRST_NAME,LAST_NAME,LASTPWDUPDATE,GENERATEDPWD,REGISTRATION_DATE,EMAIL,EMAIL_STATUS,RECEIVE_EMAIL,
							LOCALE,LASTACTIVITY_DATE
						)
					  values
						(L_USER_INFO(i).USER_ID,L_USER_INFO(i).EMAIL,0,G_PASWD,G_SALT_PASWD,5,0,L_USER_INFO(i).FIRST_NAME,L_USER_INFO(i).LAST_NAME,L_USER_INFO(i).LASTPWDUPDATE,0,L_USER_INFO(i).REGISTRATION_DATE,L_USER_INFO(i).EMAIL,0,1,
						 L_USER_INFO(i).LOCALE,L_USER_INFO(i).LASTACTIVITY_DATE
						)
					  LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER') REJECT LIMIT UNLIMITED;
					
				  
                  FORALL I IN L_USER_INFO.FIRST .. L_USER_INFO.LAST
					  INSERT INTO TRU_DPSX_USER 
						(ID,REWARD_NUMBER)
					  values
						(L_USER_INFO(i).USER_ID,L_USER_INFO(i).REWARD_NUMBER)
					  LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_USER') REJECT LIMIT UNLIMITED;
					  
				  FORALL I IN L_USER_INFO.FIRST .. L_USER_INFO.LAST 			
					INSERT INTO DPS_USER_ADDRESS
					(
						ID,
						--HOME_ADDR_ID,
						BILLING_ADDR_ID,
						SHIPPING_ADDR_ID
					)
					VALUES
					(
						L_USER_INFO(i).USER_ID,
						--NULL
						L_USER_INFO(I).DEFAULT_BILLING_ADDRESS,
						L_USER_INFO(i).DEFAULT_SHIPPING_ADDRESS
					)
					LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER_ADDRESS') REJECT LIMIT UNLIMITED;
         
                COMMIT;
            END LOOP;
			
			
			
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER load completed');
			
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_USER load completed');
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER_ADDRESS load completed');
			
			
			 OPEN L_DYN_CUR FOR 	SELECT
										ADDRESS_ID,
										USER_ID,
										--NULL PREFIX
										FIRST_NAME,
										--NULL MIDDLE_NAME
										LAST_NAME,
										--NULL SUFFIX
										--NULL JOB_TITLE
										--NULL COMPANY_NAME
										address1,
										ADDRESS2,
										ADDRESS3,
										city,
										STATE,
										POSTAL_CODE,
										COUNTY,
										country,
										PHONE_NUMBER,
										NICK_NAME,
										LAST_ACTIVITY
										--NULL FAX_NUMBER
									FROM TRU_TEMP_PROFILE_ADDRESS WHERE STATUS=1;

             LOOP
                  FETCH L_DYN_CUR BULK COLLECT INTO L_USER_ADDRS LIMIT ROWS;
                  EXIT WHEN L_USER_ADDRS.count=0;
                  FORALL I IN L_USER_ADDRS.FIRST .. L_USER_ADDRS.LAST
					INSERT INTO  DPS_CONTACT_INFO
						(ID,USER_ID,FIRST_NAME,LAST_NAME,ADDRESS1,ADDRESS2,ADDRESS3,CITY,STATE,POSTAL_CODE,COUNTY,COUNTRY,PHONE_NUMBER)
					VALUES 
						(L_USER_ADDRS(I).ADDRESS_ID,L_USER_ADDRS(I).USER_ID,L_USER_ADDRS(I).FIRST_NAME,L_USER_ADDRS(I).LAST_NAME,L_USER_ADDRS(I).ADDRESS1,L_USER_ADDRS(I).ADDRESS2,
						 L_USER_ADDRS(I).ADDRESS3,L_USER_ADDRS(I).CITY,L_USER_ADDRS(I).STATE,L_USER_ADDRS(I).POSTAL_CODE,L_USER_ADDRS(I).COUNTY,L_USER_ADDRS(I).COUNTRY,L_USER_ADDRS(I).PHONE_NUMBER)
					LOG ERRORS INTO DML_ERROR_LOG ('DPS_CONTACT_INFO') REJECT LIMIT UNLIMITED;
					
				  
                  FORALL I IN L_USER_ADDRS.FIRST .. L_USER_ADDRS.LAST
					INSERT INTO TRU_DPSX_CONTACT_INFO
						(
							ID,
							LAST_ACTIVITY,
							STATUS_CD
						)
					VALUES
						(L_USER_ADDRS(I).ADDRESS_ID,L_USER_ADDRS(I).LAST_ACTIVITY,10)
					LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CONTACT_INFO') REJECT LIMIT UNLIMITED;
				  
				  FORALL I IN L_USER_ADDRS.FIRST .. L_USER_ADDRS.LAST
							
					INSERT INTO DPS_OTHER_ADDR
						(
							USER_ID,
							TAG,
							ADDRESS_ID
						)
					VALUES
						(
							L_USER_ADDRS(I).USER_ID,
							L_USER_ADDRS(I).NICK_NAME,
							L_USER_ADDRS(I).ADDRESS_ID
						)
					LOG ERRORS INTO DML_ERROR_LOG ('DPS_OTHER_ADDR') REJECT LIMIT UNLIMITED;
         
                COMMIT;
            END LOOP;
			
			
			
			DBMS_OUTPUT.PUT_LINE ('DPS_CONTACT_INFO load completed');
			
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CONTACT_INFO load completed');
			
			DBMS_OUTPUT.PUT_LINE ('DPS_OTHER_ADDR load completed');
			
			
			 OPEN L_DYN_CUR FOR SELECT 
									CREDIT_CARD_ID,
                  USER_ID,
									CREDIT_CARD_NUMBER,
									CREDIT_CARD_TYPE,
									EXPIRATION_MONTH,
									--NONE EXP_DAY_OF_MONTH
									EXPIRATION_YEAR,
									BILLING_ADDRESS,
									NAME_ON_CARD
								FROM TRU_TEMP_PROFILE_CREDITCARD WHERE STATUS=1;

             LOOP
                  FETCH L_DYN_CUR BULK COLLECT INTO L_USER_CC LIMIT ROWS;
                  EXIT WHEN L_USER_CC.count=0;
                  FORALL I IN L_USER_CC.FIRST .. L_USER_CC.LAST
						INSERT INTO DPS_CREDIT_CARD
							(
								ID,
								CREDIT_CARD_NUMBER,
								CREDIT_CARD_TYPE,
								EXPIRATION_MONTH,
								EXPIRATION_YEAR,
								BILLING_ADDR
							)
						VALUES
							( 
								L_USER_CC(I).CREDIT_CARD_ID,
								L_USER_CC(I).CREDIT_CARD_NUMBER,
								L_USER_CC(I).CREDIT_CARD_TYPE,
								L_USER_CC(I).EXPIRATION_MONTH,
								L_USER_CC(I).EXPIRATION_YEAR,
								L_USER_CC(I).BILLING_ADDRESS	
							)
						LOG ERRORS INTO DML_ERROR_LOG ('DPS_CREDIT_CARD') REJECT LIMIT UNLIMITED;
					
				  
                  FORALL I IN L_USER_CC.FIRST .. L_USER_CC.LAST
								
						INSERT INTO TRU_DPSX_CREDIT_CARD
						   (
								ID,
								NAME_ON_CARD
								--CREDIT_CARD_TOKEN,
								--LAST_ACTIVITY
							)
						VALUES
							(
							   L_USER_CC(I).CREDIT_CARD_ID,
							   L_USER_CC(I).NAME_ON_CARD
							   --NULL,
							   --NULL
							)
						LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CREDIT_CARD') REJECT LIMIT UNLIMITED;
				 
				  FORALL I IN L_USER_CC.FIRST .. L_USER_CC.LAST

						INSERT INTO DPS_USR_CREDITCARD
						(
							USER_ID,
							TAG,
							CREDIT_CARD_ID
						)
						VALUES
						(
							L_USER_CC(I).USER_ID,
							L_USER_CC(I).CREDIT_CARD_TYPE,
							L_USER_CC(I).CREDIT_CARD_ID
						)
						LOG ERRORS INTO DML_ERROR_LOG ('DPS_USR_CREDITCARD') REJECT LIMIT UNLIMITED;

         
                COMMIT;
            END LOOP;
	

			
			DBMS_OUTPUT.PUT_LINE ('DPS_CREDIT_CARD load completed');
										
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CREDIT_CARD load completed');

			DBMS_OUTPUT.PUT_LINE ('DPS_USR_CREDITCARD load completed');
			
		END IF;
		
	    IF lower(P_LAOD_TYPE) = 'delta'
		THEN
		
		
			OPEN L_DYN_CUR FOR 		    SELECT 
												USER_ID,
												EMAIL,
												--0 AUTO_LOGIN,
												--ENCRIPT PSWD PASSWORD
												--ENCRIPT PSWD PASSWORD_SALT
												--5 PASSWORD_KDF,
												--NULL REALM_ID
												--0 MEMBER,
												FIRST_NAME,
												--NULL MIDDLE_NAME
												LAST_NAME,
												--NULL USER_TYPE
												LOCALE,
												LASTACTIVITY_DATE,
												LASTPWDUPDATE,
												--0 GENERATEDPWD,
												REGISTRATION_DATE,
												--EMAIL,
												--0 EMAIL_STATUS,
												--1 RECEIVE_EMAIL
												--NULL LAST_EMAILED
												--NULL GENDER
												--NULL DATE_OF_BIRTH
												--NULL SECURITYSTATUS
												--NULL DESCRIPTION
												REWARD_NUMBER,
												DEFAULT_BILLING_ADDRESS,
												DEFAULT_SHIPPING_ADDRESS
											FROM TRU_TEMP_PROFILE_USERINFO WHERE STATUS=1;

             LOOP
                  FETCH L_DYN_CUR BULK COLLECT INTO L_USER_INFO LIMIT ROWS;
                  EXIT WHEN L_USER_INFO.count=0;
			
				  FORALL I IN L_USER_INFO.FIRST .. L_USER_INFO.LAST
					MERGE INTO DPS_USER A
					USING
						(
							SELECT 
								L_USER_INFO(I).USER_ID USER_ID,
								L_USER_INFO(I).EMAIL EMAIL,
								L_USER_INFO(I).FIRST_NAME FIRST_NAME,
								L_USER_INFO(I).LAST_NAME LAST_NAME,
								L_USER_INFO(I).LASTPWDUPDATE LASTPWDUPDATE,
								L_USER_INFO(I).REGISTRATION_DATE REGISTRATION_DATE,
                L_USER_INFO(I).LOCALE LOCALE,
                L_USER_INFO(I).LASTACTIVITY_DATE LASTACTIVITY_DATE
							FROM DUAL
						) B
					ON (A.ID=B.USER_ID)
					WHEN MATCHED THEN
						UPDATE SET A.LOGIN = B.EMAIL ,A.FIRST_NAME = B.FIRST_NAME ,A.LAST_NAME = B.LAST_NAME ,A.LASTPWDUPDATE = B.LASTPWDUPDATE ,A.REGISTRATION_DATE = B.REGISTRATION_DATE ,A.EMAIL = B.EMAIL,A.LOCALE = B.LOCALE,A.LASTACTIVITY_DATE = B.LASTACTIVITY_DATE
					WHEN NOT MATCHED THEN
						INSERT (A.ID,A.LOGIN,A.AUTO_LOGIN,A.PASSWORD,A.PASSWORD_SALT,A.PASSWORD_KDF,A.MEMBER,A.FIRST_NAME,A.LAST_NAME,A.LASTPWDUPDATE,A.GENERATEDPWD,A.REGISTRATION_DATE,A.EMAIL,A.EMAIL_STATUS,A.RECEIVE_EMAIL,A.LOCALE,A.LASTACTIVITY_DATE)
						VALUES (B.USER_ID,B.EMAIL,0,G_PASWD,G_SALT_PASWD,5,0,B.FIRST_NAME,B.LAST_NAME,B.LASTPWDUPDATE,0,B.REGISTRATION_DATE,B.EMAIL,0,1,B.LOCALE,B.LASTACTIVITY_DATE)
					LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER') REJECT LIMIT UNLIMITED;
					
				 
				 FORALL I IN L_USER_INFO.FIRST .. L_USER_INFO.LAST
					MERGE INTO TRU_DPSX_USER A
					USING
						(
							SELECT 
								 L_USER_INFO(I).USER_ID USER_ID,
								 L_USER_INFO(I).REWARD_NUMBER REWARD_NUMBER 
								 --NULL LOYAL_CUST
							FROM DUAL
						) B
					ON (A.ID=B.USER_ID)
					WHEN MATCHED THEN
						UPDATE SET A.REWARD_NUMBER = B.REWARD_NUMBER 
					WHEN NOT MATCHED THEN
						INSERT (A.ID,A.REWARD_NUMBER)
						VALUES (B.USER_ID,B.REWARD_NUMBER)
					LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_USER') REJECT LIMIT UNLIMITED;
					
				  FORALL I IN L_USER_INFO.FIRST .. L_USER_INFO.LAST
					MERGE INTO DPS_USER_ADDRESS A
					USING
						(
							SELECT 
								L_USER_INFO(I).USER_ID USER_ID,
								--NULL HOME_ADDR_ID
								L_USER_INFO(I).DEFAULT_BILLING_ADDRESS DEFAULT_BILLING_ADDRESS,
								L_USER_INFO(I).DEFAULT_SHIPPING_ADDRESS DEFAULT_SHIPPING_ADDRESS
							FROM TRU_TEMP_PROFILE_USERINFO
						) B
					ON (A.ID=B.USER_ID)
					WHEN MATCHED THEN
						UPDATE SET A.BILLING_ADDR_ID = B.DEFAULT_BILLING_ADDRESS,A.SHIPPING_ADDR_ID = B.DEFAULT_SHIPPING_ADDRESS
					WHEN NOT MATCHED THEN
						INSERT (A.ID,A.BILLING_ADDR_ID,A.SHIPPING_ADDR_ID)
						VALUES (B.USER_ID,B.DEFAULT_BILLING_ADDRESS,B.DEFAULT_SHIPPING_ADDRESS)
					LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER_ADDRESS') REJECT LIMIT UNLIMITED;	
					
				COMMIT;
			
			END LOOP;


			DBMS_OUTPUT.PUT_LINE ('DPS_USER merge completed');
			
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_USER merge completed');
						
			DBMS_OUTPUT.PUT_LINE ('DPS_USER_ADDRESS merge completed');
			
			
			
			
			
			 OPEN L_DYN_CUR FOR 	SELECT
										ADDRESS_ID,
										USER_ID,
										--NULL PREFIX
										FIRST_NAME,
										--NULL MIDDLE_NAME
										LAST_NAME,
										--NULL SUFFIX
										--NULL JOB_TITLE
										--NULL COMPANY_NAME
										address1,
										ADDRESS2,
										ADDRESS3,
										city,
										STATE,
										POSTAL_CODE,
										COUNTY,
										country,
										PHONE_NUMBER,
										NICK_NAME,
										LAST_ACTIVITY
										--NULL FAX_NUMBER
									FROM TRU_TEMP_PROFILE_ADDRESS WHERE STATUS=1;

             LOOP
                  FETCH L_DYN_CUR BULK COLLECT INTO L_USER_ADDRS LIMIT ROWS;
                  EXIT WHEN L_USER_ADDRS.count=0;
				  
                  FORALL I IN L_USER_ADDRS.FIRST .. L_USER_ADDRS.LAST
					MERGE INTO DPS_CONTACT_INFO A
					USING
						(
							 SELECT
								L_USER_ADDRS(I).ADDRESS_ID ADDRESS_ID,
								L_USER_ADDRS(I).USER_ID USER_ID,
								L_USER_ADDRS(I).FIRST_NAME FIRST_NAME,
								L_USER_ADDRS(I).LAST_NAME LAST_NAME,
								L_USER_ADDRS(I).ADDRESS1 ADDRESS1,
								L_USER_ADDRS(I).ADDRESS2 ADDRESS2,
								L_USER_ADDRS(I).ADDRESS3 ADDRESS3,
								L_USER_ADDRS(I).CITY CITY,
								L_USER_ADDRS(I).STATE STATE,
								L_USER_ADDRS(I).POSTAL_CODE POSTAL_CODE,
								L_USER_ADDRS(I).COUNTY COUNTY,
								L_USER_ADDRS(I).COUNTRY COUNTRY,
								L_USER_ADDRS(I).PHONE_NUMBER PHONE_NUMBER
							FROM DUAL
						) B
					ON (A.ID=B.ADDRESS_ID)
					WHEN MATCHED THEN
						UPDATE SET A.USER_ID = B.USER_ID,A.FIRST_NAME = B.FIRST_NAME,A.LAST_NAME = B.LAST_NAME,A.ADDRESS1 = B.ADDRESS1,A.ADDRESS2 = B.ADDRESS2,A.ADDRESS3 = B.ADDRESS3,
								   A.CITY = B.CITY,A.STATE = B.STATE,A.POSTAL_CODE = B.POSTAL_CODE,A.COUNTY = B.COUNTY,A.country = B.country,A.PHONE_NUMBER = B.PHONE_NUMBER
					WHEN NOT MATCHED THEN
						INSERT (A.ID,A.USER_ID,A.FIRST_NAME,A.LAST_NAME,A.ADDRESS1,A.ADDRESS2,A.ADDRESS3,A.CITY,A.STATE,A.POSTAL_CODE,A.COUNTY,A.COUNTRY,A.PHONE_NUMBER)
						VALUES (B.ADDRESS_ID,B.USER_ID,B.FIRST_NAME,B.LAST_NAME,B.ADDRESS1,B.ADDRESS2,B.ADDRESS3,B.CITY,B.STATE,B.POSTAL_CODE,B.COUNTY,B.COUNTRY,B.PHONE_NUMBER)
					LOG ERRORS INTO DML_ERROR_LOG ('DPS_CONTACT_INFO') REJECT LIMIT UNLIMITED;
							
					
				  
				  FORALL I IN L_USER_ADDRS.FIRST .. L_USER_ADDRS.LAST
					MERGE INTO TRU_DPSX_CONTACT_INFO A
					USING
						(
							 SELECT L_USER_ADDRS(I).ADDRESS_ID ADDRESS_ID,
									L_USER_ADDRS(I).LAST_ACTIVITY LAST_ACTIVITY
									--NULL STATUS_CD
							 FROM DUAL
						) B
					ON (A.ID=B.ADDRESS_ID)
					WHEN MATCHED THEN
						UPDATE SET A.LAST_ACTIVITY = B.LAST_ACTIVITY
					WHEN NOT MATCHED THEN
						INSERT (A.ID,A.LAST_ACTIVITY,A.STATUS_CD)
						VALUES (B.ADDRESS_ID,B.LAST_ACTIVITY,10)
					LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CONTACT_INFO') REJECT LIMIT UNLIMITED;
					
				  
				  FORALL I IN L_USER_ADDRS.FIRST .. L_USER_ADDRS.LAST	
					MERGE INTO DPS_OTHER_ADDR A
					USING
						(
							SELECT
								L_USER_ADDRS(I).USER_ID USER_ID,
								L_USER_ADDRS(I).NICK_NAME NICK_NAME,
								L_USER_ADDRS(I).ADDRESS_ID ADDRESS_ID	
							FROM DUAL
						) B
					ON (A.USER_ID=B.USER_ID AND A.TAG = B.NICK_NAME)
					WHEN MATCHED THEN
						UPDATE SET A.ADDRESS_ID = B.ADDRESS_ID
					WHEN NOT MATCHED THEN
						INSERT (A.USER_ID,A.TAG,A.ADDRESS_ID)
						VALUES (B.USER_ID,B.NICK_NAME,B.ADDRESS_ID)
					LOG ERRORS INTO DML_ERROR_LOG ('DPS_OTHER_ADDR') REJECT LIMIT UNLIMITED;	
         
                COMMIT;
            END LOOP;
			
			
			
			DBMS_OUTPUT.PUT_LINE ('DPS_CONTACT_INFO merge completed');
			
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CONTACT_INFO merge completed');
			
			DBMS_OUTPUT.PUT_LINE ('DPS_OTHER_ADDR merge completed');
						

			OPEN L_DYN_CUR FOR SELECT 
									CREDIT_CARD_ID,
                  USER_ID,
									CREDIT_CARD_NUMBER,
									CREDIT_CARD_TYPE,
									EXPIRATION_MONTH,
									--NONE EXP_DAY_OF_MONTH
									EXPIRATION_YEAR,
									BILLING_ADDRESS,
                  NAME_ON_CARD
								FROM TRU_TEMP_PROFILE_CREDITCARD WHERE STATUS=1;

             LOOP
                  FETCH L_DYN_CUR BULK COLLECT INTO L_USER_CC LIMIT ROWS;
                  EXIT WHEN L_USER_CC.count=0;
                  FORALL I IN L_USER_CC.FIRST .. L_USER_CC.LAST
					MERGE INTO DPS_CREDIT_CARD A
					USING
						(
							 SELECT 
									L_USER_CC(I).CREDIT_CARD_ID CREDIT_CARD_ID,
									L_USER_CC(I).CREDIT_CARD_NUMBER CREDIT_CARD_NUMBER,
									L_USER_CC(I).CREDIT_CARD_TYPE CREDIT_CARD_TYPE,
									L_USER_CC(I).EXPIRATION_MONTH EXPIRATION_MONTH,
									--NONE EXP_DAY_OF_MONTH
									L_USER_CC(I).EXPIRATION_YEAR EXPIRATION_YEAR,
									L_USER_CC(I).BILLING_ADDRESS	 BILLING_ADDRESS
							FROM DUAL
						) B
					ON (A.ID=B.CREDIT_CARD_ID)
					WHEN MATCHED THEN
						UPDATE SET A.CREDIT_CARD_NUMBER = B.CREDIT_CARD_NUMBER,A.CREDIT_CARD_TYPE = B.CREDIT_CARD_TYPE,A.EXPIRATION_MONTH = B.EXPIRATION_MONTH,A.EXPIRATION_YEAR = B.EXPIRATION_YEAR,A.BILLING_ADDR = B.BILLING_ADDRESS
					WHEN NOT MATCHED THEN
						INSERT (A.ID,A.CREDIT_CARD_NUMBER,A.CREDIT_CARD_TYPE,A.EXPIRATION_MONTH,A.EXPIRATION_YEAR,A.BILLING_ADDR)
						VALUES (B.CREDIT_CARD_ID,B.CREDIT_CARD_NUMBER,B.CREDIT_CARD_TYPE,B.EXPIRATION_MONTH,B.EXPIRATION_YEAR,B.BILLING_ADDRESS)
					LOG ERRORS INTO DML_ERROR_LOG ('DPS_CREDIT_CARD') REJECT LIMIT UNLIMITED;		
					
				  
                  FORALL I IN L_USER_CC.FIRST .. L_USER_CC.LAST
					MERGE INTO TRU_DPSX_CREDIT_CARD A
					USING
						(
							SELECT 
								   L_USER_CC(I).CREDIT_CARD_ID CREDIT_CARD_ID,
								   L_USER_CC(I).NAME_ON_CARD NAME_ON_CARD
								   --NULL CREDIT_CARD_TOKEN,
								   --NULL LAST_ACTIVITY
							FROM  DUAL
						) B
					ON (A.ID=B.CREDIT_CARD_ID)
					WHEN MATCHED THEN
						UPDATE SET A.NAME_ON_CARD = B.NAME_ON_CARD
					WHEN NOT MATCHED THEN
						INSERT (A.ID,A.NAME_ON_CARD)
						VALUES (B.CREDIT_CARD_ID,B.NAME_ON_CARD)
					LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CREDIT_CARD') REJECT LIMIT UNLIMITED;
								
						
				 
				  FORALL I IN L_USER_CC.FIRST .. L_USER_CC.LAST
					MERGE INTO DPS_USR_CREDITCARD A
					USING
						(
							SELECT 
								L_USER_CC(I).USER_ID USER_ID,
								L_USER_CC(I).CREDIT_CARD_TYPE CREDIT_CARD_TYPE,
								L_USER_CC(I).CREDIT_CARD_ID CREDIT_CARD_ID
							FROM DUAL
						) B
					ON (A.USER_ID=B.USER_ID AND A.TAG = B.CREDIT_CARD_TYPE)
					WHEN MATCHED THEN
						UPDATE SET A.CREDIT_CARD_ID = B.CREDIT_CARD_ID
					WHEN NOT MATCHED THEN
						INSERT (A.USER_ID,A.TAG,A.CREDIT_CARD_ID)
						VALUES (B.USER_ID,B.CREDIT_CARD_TYPE,B.CREDIT_CARD_ID)
					LOG ERRORS INTO DML_ERROR_LOG ('DPS_USR_CREDITCARD') REJECT LIMIT UNLIMITED;	
					
         
                COMMIT;
            END LOOP;
	

			
			DBMS_OUTPUT.PUT_LINE ('DPS_CREDIT_CARD merge completed');
										
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CREDIT_CARD merge completed');

			DBMS_OUTPUT.PUT_LINE ('DPS_USR_CREDITCARD merge completed');			

			

		END IF;
		
		DBMS_OUTPUT.PUT_LINE ('All Target table load completed');
		
		G_END :=DBMS_UTILITY.GET_TIME;
        DBMS_OUTPUT.PUT_LINE('PROFILE_MIG_TARGT_BATCH: Time Taken to execute ' || (G_END-G_START)/100||' '||'seconds');
		
	  EXCEPTION WHEN OTHERS THEN
		RAISE;
		
	end PROFILE_MIG_TARGT_BATCH;
END TRU_PROFILE_MIG_PKG;
/