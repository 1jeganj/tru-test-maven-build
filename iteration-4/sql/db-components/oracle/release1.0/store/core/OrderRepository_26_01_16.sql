CREATE TABLE TRU_FAILED_REVERSE_AUTH (
	ORDER_ID 		VARCHAR2(254)	NOT NULL,
	STATUS 			INTEGER	NULL,
	LAST_MODIFIED_DATE 	DATE	NULL,
	PRIMARY KEY(ORDER_ID)
);

CREATE TABLE TRU_FAILED_AUTH_PG (
	ORDER_ID 		VARCHAR2(254)	NOT NULL,
	SEQUENCE_NUM 		INTEGER	NOT NULL,
	PAYMENT_GROUPS 		VARCHAR2(254)	NULL,
	CONSTRAINT TRU_FAILED_AUTH_PG_P PRIMARY KEY(ORDER_ID,SEQUENCE_NUM),
	CONSTRAINT TRU_FAILED_AUTH_PG_F FOREIGN KEY(ORDER_ID) REFERENCES TRU_FAILED_REVERSE_AUTH(ORDER_ID)
);