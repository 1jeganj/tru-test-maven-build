create table tru_coupon(
	coupon_id varchar2(254) not null,
	sucn_enabled NUMBER(1) DEFAULT 0,
	CONSTRAINT tru_coupon_p PRIMARY KEY(coupon_id)
);