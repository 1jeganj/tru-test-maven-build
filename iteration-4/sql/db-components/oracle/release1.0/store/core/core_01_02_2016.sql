drop table tru_recentlyViewed_list;
drop table tru_recently_date;

CREATE TABLE tru_recentlyViewed_list (
                id varchar2(254) NOT NULL REFERENCES dps_user(id),
                recentlyViewed_sequenceNumber INTEGER NOT NULL,
                recently_viewed_items varchar2(254) NULL,
                PRIMARY KEY(id, recentlyViewed_sequenceNumber)
);

CREATE INDEX tru_recentlyViewed_list_idx ON tru_recentlyViewed_list(id, recentlyViewed_sequenceNumber);

CREATE TABLE tru_recently_date (
                id varchar2(254) NOT NULL REFERENCES dps_user(id),
                recently_viewed_date TIMESTAMP NULL,
                PRIMARY KEY(id)
);

CREATE INDEX tru_recently_date_idx ON tru_recently_date(id);
