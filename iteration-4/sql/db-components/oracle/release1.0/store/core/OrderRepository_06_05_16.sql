ALTER TABLE TRU_ORDER ADD ENTERED_BY VARCHAR2(254)	NULL;
ALTER TABLE TRU_ORDER ADD ENTERED_LOCATION VARCHAR2(254)	NULL;
ALTER TABLE TRU_LAYAWAY_ORDER ADD ENTERED_BY VARCHAR2(254)	NULL;
ALTER TABLE TRU_LAYAWAY_ORDER ADD ENTERED_LOCATION VARCHAR2(254)	NULL;
commit;