CREATE TABLE tru_productanalytics (
	product_Id 		varchar2(40)	NOT NULL,
	quantity 		number(19, 0)	NULL,
	PRIMARY KEY(product_Id)
);
commit;