
CREATE TABLE TRU_ZIPCODE_RESTRICTIONS (
	ID 			VARCHAR2(254)		NOT NULL,
	SKU_ID 			VARCHAR2(254)	NOT NULL,
	ZIP_CODE 		VARCHAR2(254)	NOT NULL,
	ERROR_MESSAGE 		CLOB		NOT NULL,
	CONSTRAINT  TRU_ZIPCODE_RESTRI_INFO_P PRIMARY KEY(ID)
);
