CREATE TABLE tru_shipitemrelation_tax (
	amount_info_id 		varchar2(254)	NOT NULL REFERENCES dcspp_amount_info(amount_info_id),
	relationship_id 	varchar2(254)	NOT NULL,
	ship_relation_item_tax 	varchar2(254)	NULL REFERENCES dcspp_amount_info(amount_info_id),
	PRIMARY KEY(amount_info_id, relationship_id)
);


CREATE TABLE tru_tax_price (
	amount_info_id 		varchar2(254)	NOT NULL REFERENCES dcspp_amount_info(amount_info_id),
	secondary_state_tax_amount number(28, 20)	NULL,
	secondary_county_tax_amount number(28, 20)	NULL,
	secondary_city_tax_amount number(28, 20)	NULL,
	tax_city 		varchar2(254)	NULL,
	tax_state 		varchar2(254)	NULL,
	tax_county 		varchar2(254)	NULL,
	tax_secondory_state 	varchar2(254)	NULL,
	tax_secondory_city 	varchar2(254)	NULL,
	tax_secondory_county 	varchar2(254)	NULL,
	PRIMARY KEY(amount_info_id)
);
commit;