
drop table tru_clf_prd_att_name;
drop table tru_product_attributes;
drop table tru_attribute;
drop table tru_attribute_name;
drop table tru_attribute_group;
--------------PUB-----------------

-- drop table tru_comparable_attributes;

CREATE TABLE tru_comparable_attributes (
	id 			varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	attribute_name 		varchar2(254)	NULL,
	attribute_type 		varchar2(254)	NULL,
	attribute_property 	varchar2(254)	NULL,
	version_deleted 	number(1)	NULL,
	version_editable 	number(1)	NULL,
	pred_version 		INTEGER	NULL,
	workspace_id 		varchar2(254)	NULL,
	branch_id 		varchar2(254)	NULL,
	is_head 		number(1)	NULL,
	checkin_date 		DATE	NULL,
	CHECK (version_deleted IN (0, 1)),
	CHECK (version_editable IN (0, 1)),
	CHECK (is_head IN (0, 1)),
	PRIMARY KEY(id, asset_version)
);


-- drop table tru_clf_comp_Attr;


CREATE TABLE tru_clf_comp_Attr (
	category_id 		varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	clf_compAttr INTEGER	NOT NULL,
	comparable_attributes 	varchar2(254)	NULL,
	PRIMARY KEY(category_id, asset_version, clf_compAttr)
);

CREATE INDEX tru_clf_comp_Attr_category_idx ON tru_clf_comp_Attr(category_id, asset_version, clf_compAttr);


----------------------------