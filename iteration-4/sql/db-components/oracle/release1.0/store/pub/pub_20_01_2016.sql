DROP TABLE TRU_CLF_SEO;
DROP TABLE TRU_PRODUCT_SEO;

ALTER TABLE TRU_CLASSIFICATION add SEO_TAG_ID varchar2(40);
ALTER TABLE TRU_PRODUCT add SEO_TAG_ID varchar2(40);

CREATE TABLE TRU_SEO (
	SEO_TAG_ID VARCHAR2(40) NOT NULL,
	ASSET_VERSION INTEGER NOT NULL,
	SEO_TITLE varchar2(254),
	SEO_H1 varchar2(254),
	SEO_DESCRIPTION varchar2(254),
	VERSION_DELETED 	number(1)	NULL,
	VERSION_EDITABLE 	number(1)	NULL,
	PRED_VERSION 		INTEGER	NULL,
	WORKSPACE_ID 		varchar2(254)	NULL,
	BRANCH_ID 		varchar2(254)	NULL,
	IS_HEAD 		number(1)	NULL,
	CHECKIN_DATE 		DATE	NULL,
	CHECK (VERSION_DELETED IN (0, 1)),
	CHECK (VERSION_EDITABLE IN (0, 1)),
	CHECK (IS_HEAD IN (0, 1)),
	PRIMARY KEY(SEO_TAG_ID, ASSET_VERSION)
);

CREATE TABLE TRU_CATEGORY (
	CATEGORY_ID VARCHAR2(40) NOT NULL,
	SEO_TAG_ID VARCHAR2(40),
	ASSET_VERSION INTEGER NOT NULL,
	VERSION_DELETED 	number(1)	NULL,
	VERSION_EDITABLE 	number(1)	NULL,
	PRED_VERSION 		INTEGER	NULL,
	WORKSPACE_ID 		varchar2(254)	NULL,
	BRANCH_ID 		varchar2(254)	NULL,
	IS_HEAD 		number(1)	NULL,
	CHECKIN_DATE 		DATE	NULL,
	CHECK (VERSION_DELETED IN (0, 1)),
	CHECK (VERSION_EDITABLE IN (0, 1)),
	CHECK (IS_HEAD IN (0, 1))
	,constraint TRU_CATEGORY_P PRIMARY KEY (CATEGORY_ID,ASSET_VERSION));

--Start - Added for payeezy enable/ disable flag - 19th Jan 2016
ALTER TABLE TRU_SITE_CONFIGURATION ADD ENABLE_PAYEEZY NUMBER(1)	DEFAULT 1	CHECK (ENABLE_PAYEEZY IN (0, 1));