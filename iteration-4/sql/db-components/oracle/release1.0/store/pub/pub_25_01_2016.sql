drop table tru_wcm_article;
CREATE TABLE tru_wcm_article (
    id                    	varchar2(254)   NOT NULL,
    asset_version         	INTEGER         NOT NULL,	
	seo_title_txt 		  	varchar2(254)	NOT NULL,
	seo_meta_desc 		  	varchar2(254)	NOT NULL,
	seo_meta_keyword_txt 	varchar2(254)	NOT NULL,
	seo_alt_meta_desc 		varchar2(254)	NULL,
    version_deleted     	number(1)   	NULL,
    version_editable        number(1)   	NULL,
    pred_version            INTEGER     	NULL,
    workspace_id            varchar2(254)  	NULL,
    branch_id               varchar2(254)   NULL,
    is_head                 number(1)       NULL,
    checkin_date            DATE            NULL,
    CHECK (version_deleted IN (0, 1)),
    CHECK (version_editable IN (0, 1)),
    CHECK (is_head IN (0, 1)),
    PRIMARY KEY(id, asset_version)
);