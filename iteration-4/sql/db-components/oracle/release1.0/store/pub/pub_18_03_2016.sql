drop table tru_clf_prd_att_name;

CREATE TABLE tru_clf_prd_att_name (
                category_id                        varchar2(254)    NOT NULL,
                asset_version                    INTEGER              NOT NULL,
                attribute_name                                varchar2(254)    NOT NULL,
              CONSTRAINT tru_clf_prd_att_name_P  PRIMARY KEY (category_id, asset_version, attribute_name)
);