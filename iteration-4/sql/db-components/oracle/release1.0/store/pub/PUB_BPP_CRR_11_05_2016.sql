 drop table tru_bpp;
 CREATE TABLE tru_bpp (
                bpp_id                                  varchar2(254)    NOT NULL,
                asset_version                    INTEGER              NOT NULL,
                group_number                                 varchar2(254)    NULL,
                bpp_name                          varchar2(254)    NULL,
                low                                         number(28, 20) NULL,
                high                                       number(28, 20) NULL,
                term                                      varchar2(254)    NULL,
                retail                                      number(28, 20) NULL,
                SKU                                        varchar2(254)    NULL,
                cart_description               varchar2(254)    NULL,
                version_deleted              number(1)          NULL,
                version_editable              number(1)          NULL,
                pred_version                     INTEGER              NULL,
                workspace_id                    varchar2(254)    NULL,
                branch_id                            varchar2(254)    NULL,
                is_head                                number(1)          NULL,
                checkin_date                     DATE     NULL,
                CHECK (version_deleted IN (0, 1)),
                CHECK (version_editable IN (0, 1)),
                CHECK (is_head IN (0, 1)),
                PRIMARY KEY(bpp_id, asset_version)
);
