CREATE TABLE TRU_SEO_HOME (
	SEO_TAG_ID VARCHAR2(40) NOT NULL,
	ASSET_VERSION INTEGER NOT NULL,
	SEO_DESCRIPTION varchar2(254),
 	SEO_KEYWORDS varchar2(254),
	VERSION_DELETED 	number(1)	NULL,
	VERSION_EDITABLE 	number(1)	NULL,
	PRED_VERSION 		INTEGER	NULL,
	WORKSPACE_ID 		varchar2(254)	NULL,
	BRANCH_ID 		varchar2(254)	NULL,
	IS_HEAD 		number(1)	NULL,
	CHECKIN_DATE 		DATE	NULL,
	CHECK (VERSION_DELETED IN (0, 1)),
	CHECK (VERSION_EDITABLE IN (0, 1)),
	CHECK (IS_HEAD IN (0, 1)),
	PRIMARY KEY(SEO_TAG_ID, ASSET_VERSION)
);
  
ALTER TABLE TRU_CATALOG add SEO_TAG_ID varchar2(40);

ALTER TABLE TRU_SEO add SEO_KEYWORDS VARCHAR2(254); 

ALTER TABLE TRU_SKU DROP COLUMN SEO_KEYWORD; 



