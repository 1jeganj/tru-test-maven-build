CREATE TABLE tru_static_page_folder (
	id 			varchar2(254) NOT NULL,
	 asset_version INTEGER NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	static_page_folder 	varchar2(254)	NULL,
	PRIMARY KEY(id, sequence_num, asset_version)
);