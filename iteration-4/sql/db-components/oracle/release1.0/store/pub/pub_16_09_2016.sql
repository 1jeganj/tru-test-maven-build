drop table tru_sku_battery;

CREATE TABLE tru_prd_battery (
	product_id 			varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	battery 		varchar2(254)	NULL,
	PRIMARY KEY(product_id, asset_version, sequence_num)
);