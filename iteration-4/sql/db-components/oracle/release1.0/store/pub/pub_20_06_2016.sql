CREATE TABLE tru_sku_alt_canonical_image (
	sku_id 			varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	alternate_canonical_image varchar2(254)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

alter table tru_sku add (
	swatch_canonical_image 	varchar2(254)	NULL,
	primary_canonical_image varchar2(254)	NULL,
	secondary_canonical_image varchar2(254)	NULL);