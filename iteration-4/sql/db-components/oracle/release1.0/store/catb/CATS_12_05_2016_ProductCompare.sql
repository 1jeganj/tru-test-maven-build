
drop table tru_clf_prd_att_name;
drop table tru_product_attributes;
drop table tru_attribute;
drop table tru_attribute_name;
drop table tru_attribute_group;

--------------STORE-----------------

-- drop table tru_comparable_attributes;

CREATE TABLE tru_comparable_attributes (
	id 			varchar2(254)	NOT NULL,
	attribute_name 		varchar2(254)	NULL,
	attribute_type 		varchar2(254)	NULL,
	attribute_property 	varchar2(254)	NULL,
	PRIMARY KEY(id)
);

-- drop table tru_clf_comp_Attr;

CREATE TABLE tru_clf_comp_Attr (
	category_id 		varchar2(254)	NOT NULL REFERENCES dcs_category(category_id),
	clf_compAttr INTEGER	NOT NULL,
	comparable_attributes 	varchar2(254)	NULL REFERENCES tru_comparable_attributes(id),
	PRIMARY KEY(category_id, clf_compAttr)
);

CREATE INDEX tru_clf_comp_Attr_category_idx ON tru_clf_comp_Attr(category_id, clf_compAttr);
