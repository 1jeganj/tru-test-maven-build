
CREATE TABLE tru_product_seo (
	product_id 		varchar2(254)	NOT NULL REFERENCES dcs_product(product_id),
	seo_title_product 	varchar2(254)	NULL,
	seo_h1_product 		varchar2(254)	NULL,
	seo_description_product varchar2(254)	NULL,
	PRIMARY KEY(product_id)
);

CREATE TABLE tru_clf_seo (
	category_id 		varchar2(254)	NOT NULL REFERENCES dcs_category(category_id),
	seo_title_cat 		varchar2(254)	NULL,
	seo_h1_cat 		varchar2(254)	NULL,
	seo_description_cat 	varchar2(254)	NULL,
	PRIMARY KEY(category_id)
);