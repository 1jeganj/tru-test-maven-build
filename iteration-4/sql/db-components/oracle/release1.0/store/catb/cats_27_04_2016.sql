create or replace
procedure PR_QUALIF_PROD_PRDS

/*
***************************************************************PR_QUALIF_PROD_PRDS**********************************************************************************************************
**    i.  Purpose
**
**		  This procedure populates TRU_PRODUCT_QUALIFIED_PROMOS and TRU_PRODUCT_TARGETED_PROMOS table

**    ii. Procedures
**
**			i.PR_QUALIF_PROD_PRDS
**			---------------------------------------
**				It populates TRU_PRODUCT_QUALIFIED_PROMOS and TRU_PRODUCT_TARGETED_PROMOS table by using the tables (tru_qualifying_category,tru_qualifying_prod) and
**				 (tru_targeted_category,tru_targeted_prod) respectively.

**			Sample example how product_id and promotion_id mapping using category_id

**			Assume One  category contains 2 level of child category and the data like below
**			Parent category  :

**											 par_cat         prom1   prd1

**			child category    :

**							   level1 : par_cat is parent of child_cata1 and child_cata2

											  child_cata1     prom2  prd2
											  child_cata2     prom3  prd3

**							  level2: child_cata1 is parent of child_cata3

											  child_cata3     prom4  prd4

**			Final date in TRU_PRODUCT_QUALIFIED_PROMOS or TRU_PRODUCT_TARGETED_PROMOS table like below

			prd1  0  prom1
			prd2  0  prom1
			prd2  1  prom2
			prd3  0  prom1
			prd3  1  prom3
			prd4  0  prom1
			prd4  1  prom2
			prd4  2  prom4



**    iii. Creation History
**
**           Date         Created by           		Comments
**          ---------     -----------        		---------------
**	   		22/JAN/2016   Malleshwara.C				Initial Version
*******************************************************************************************************************************************************************************************

*/

AS
	L_CATEGORY_ID DCS_CATEGORY.CATEGORY_ID%TYPE;
    L_PROMOTION_ID TRU_QUALIFYING_CATEGORY.PROMOTION_ID%TYPE;

    ---Variable to hold cursor operation
	CURSOR C1 IS SELECT CATEGORY_ID,PROMOTION_ID FROM TRU_QUALIFYING_CATEGORY;
	L_COUNT number;
	CURSOR C2 IS SELECT CATEGORY_ID,PROMOTION_ID FROM TRU_TARGETED_CATEGORY ;

    ---Variable to hold dynamic sql statements
	L_STATEMENT_STR VARCHAR2(10000);
	L_STATEMENT_STR1 VARCHAR2(10000);
	L_STATEMENT_STR2 VARCHAR2(10000);
	L_STATEMENT_STR3 VARCHAR2(10000);
	L_STR varchar2(100);
	L_G_STR varchar2(1);
	L_STATEMENT_1 VARCHAR2(10000);
	L_COUNT1 number;

BEGIN


------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            ------------------------------------------TRU_PRODUCT_QUALIFIED_PROMOS table population start------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	BEGIN
			execute immediate 'truncate table TRU_PRODUCT_QUALIFIED_PROMOS';
			--Cursor ci fetch the TRU_QUALIFYING_CATEGORY records
			OPEN  C1;
			LOOP
				  <<LOOP1>>
				  FETCH C1 into L_CATEGORY_ID,L_PROMOTION_ID;
				  EXIT when C1%NOTFOUND;

				  select COUNT(1) into L_COUNT1 from GT_PAR_CAT_PROM 
				  where GT_PAR_CAT_PROM.CATEGORY_ID = L_CATEGORY_ID  AND GT_PAR_CAT_PROM.PROMOTION_ID =L_PROMOTION_ID;
				  
				  --This  if condition use to skip the ctaegory_id which is already inserted in target table
				  DBMS_OUTPUT.PUT_LINE(L_CATEGORY_ID ||'::'||L_PROMOTION_ID||'::'||L_COUNT1);
				  IF L_COUNT1 > 0
				  then
						DBMS_OUTPUT.PUT_LINE(L_CATEGORY_ID ||'::'||L_PROMOTION_ID||': already inserted');
						GOTO LOOP1;
				  END IF;
				  
				  		
				  

             
          
				--Populating product list for parent category list					 
							INSERT INTO GT_PROD_PROM (PRODUCT_ID,PROMOTION_ID)
							SELECT distinct CHILD_PRD_ID PRODUCT_ID,PROMOTION_ID
							from
							(SELECT DISTINCT CATEGORY_ID,L_PROMOTION_ID PROMOTION_ID
							from
							(SELECT  CATEGORY_ID,level lvl
											   FROM   DCS_CAT_CHLDCAT 
													 START WITH DCS_CAT_CHLDCAT.CATEGORY_ID = L_CATEGORY_ID
													 CONNECT BY DCS_CAT_CHLDCAT.CATEGORY_ID = PRIOR CHILD_CAT_ID
													 ORDER SIBLINGS BY CHILD_CAT_ID
							)
							) CAT_PROM
							inner join DCS_CAT_CHLDPRD on(CAT_PROM.CATEGORY_ID = DCS_CAT_CHLDPRD.CATEGORY_ID)
              union

              select CHILD_PRD_ID PRODUCT_ID,L_PROMOTION_ID from  DCS_CAT_CHLDPRD where CATEGORY_ID = L_CATEGORY_ID;
							--DBMS_OUTPUT.PUT_LINE ('GT_PROD_PROM 1 :'||sql%ROWCOUNT);
			  
						
								 -- L_STATEMENT_STR := 'SELECT  CHILD_CAT_ID FROM DCS_CAT_CHLDCAT WHERE CATEGORY_ID  = '||''''||K.CATEGORY_ID||'''';
								 -- L_STATEMENT_STR1 := 'select child_cat_id,'||''''||K.PAR_PROMOTION_ID||''''||' from dcs_cat_chldcat where category_id in(';
								 -- L_STATEMENT_STR2 := 'select child_cat_id from dcs_cat_chldcat where category_id in(';
                 L_STATEMENT_STR := 'SELECT  CHILD_CAT_ID FROM DCS_CAT_CHLDCAT WHERE CATEGORY_ID  = '||''''||L_CATEGORY_ID||'''';
								 L_STATEMENT_STR1 := 'select child_cat_id,'||''''||L_PROMOTION_ID||''''||' from dcs_cat_chldcat where category_id in(';
								  L_STATEMENT_STR2 := 'select child_cat_id from dcs_cat_chldcat where category_id in(';
								  L_STR := ')';
								  l_g_str :=')';
								  L_STATEMENT_STR3 := null;
								  FOR L IN 1..50
								  LOOP

									  L_STATEMENT_1 :=  L_STATEMENT_STR1||L_STATEMENT_STR3||L_STATEMENT_STR||L_STR;
                   --dbms_output.put_line ('qlfy :'||L_STATEMENT_1);
									  EXECUTE IMMEDIATE 'insert into GT_CAT_PROM(CATEGORY_ID,PROMOTION_ID)'||L_STATEMENT_1;

									  IF SQL%ROWCOUNT = 0
									  THEN
										EXIT;
									  END IF;
									  L_STR := l_g_str||L_STR;
                   -- dbms_output.put_line ('L_STR :' ||l||'-'|| L_STR);
									 L_STATEMENT_STR3 := L_STATEMENT_STR2||L_STATEMENT_STR3;

								  END LOOP;
							--end LOOP;
					  --end LOOP;
            
					   --Populating category_id and promotion_id in global temporary table (GT_PAR_CAT_PROM) to validate category_id already inserted or not
						INSERT INTO GT_PAR_CAT_PROM(CATEGORY_ID,PROMOTION_ID)
						SELECT distinct par_cat_id,L_PROMOTION_ID
						FROM
						  (SELECT CHILD_CAT_ID,
						   DCS_CAT_CHLDCAT.CATEGORY_ID par_cat_id
						   from   DCS_CAT_CHLDCAT     
							start with DCS_CAT_CHLDCAT.CATEGORY_ID =L_CATEGORY_ID
							connect by DCS_CAT_CHLDCAT.CATEGORY_ID = prior CHILD_CAT_ID
							order siblings by CHILD_CAT_ID
						  );	
			END LOOP;
			
			--mapping product_id with promotion_id using child category list  end
			
			CLOSE C1;

			INSERT INTO GT_PROD_PROM (PRODUCT_ID,PROMOTION_ID)
			SELECT CHILD_PRD_ID PRODUCT_ID,PROMOTION_ID
			FROM GT_CAT_PROM INNER JOIN DCS_CAT_CHLDPRD ON(GT_CAT_PROM.CATEGORY_ID = DCS_CAT_CHLDPRD.CATEGORY_ID);

			INSERT INTO TRU_PRODUCT_QUALIFIED_PROMOS(PRODUCT_ID,SEQUENCE_NUM,PROMOTION_ID)
			SELECT PRODUCT_ID,
				   ROW_NUMBER() OVER(PARTITION BY PRODUCT_ID ORDER BY PROMOTION_ID) -1 SEQUENCE_NUM,
				   PROMOTION_ID
			FROM(
						SELECT PRODUCT_ID,PROMOTION_ID FROM GT_PROD_PROM
						UNION
						SELECT PRODUCT_ID,PROMOTION_ID FROM TRU_QUALIFYING_PROD
				);
			COMMIT;

	END;
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            ------------------------------------------TRU_PRODUCT_QUALIFIED_PROMOS table population end------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            ------------------------------------------TRU_PRODUCT_TARGETED_PROMOS table population start------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------

BEGIN
			execute immediate 'truncate table TRU_PRODUCT_TARGETED_PROMOS';

			--Cursor ci fetch the TRU_TARGETED_CATEGORY records
			OPEN  C2;
			LOOP
				  <<LOOP1>>
				  FETCH C2 into L_CATEGORY_ID,L_PROMOTION_ID;
				  EXIT WHEN C2%notfound;

				  SELECT COUNT(1) INTO L_COUNT1 FROM GT_PAR_CAT_PROM WHERE L_CATEGORY_ID = GT_PAR_CAT_PROM.CATEGORY_ID AND L_PROMOTION_ID = GT_PAR_CAT_PROM.PROMOTION_ID;

				  --This  if condition use to skip the ctaegory_id which is already inserted in target table
				  IF L_COUNT1 > 0
				  THEN
						GOTO LOOP1;
				  END IF;

				  --Populating category_id and promotion_id in global temporary table (GT_PAR_CAT_PROM) to validate category_id already inserted or not
				  INSERT INTO GT_PAR_CAT_PROM(CATEGORY_ID,PROMOTION_ID)
					SELECT par_cat_id,PAR_PROMOTION_ID
					FROM
						(SELECT CHILD_CAT_ID,
						 DCS_CAT_CHLDCAT.CATEGORY_ID par_cat_id,
						 TRU_TARGETED_CATEGORY.PROMOTION_ID PAR_PROMOTION_ID,
						 LEVEL LVL
						 from   DCS_CAT_CHLDCAT left outer join TRU_TARGETED_CATEGORY on(DCS_CAT_CHLDCAT.CATEGORY_ID = TRU_TARGETED_CATEGORY.CATEGORY_ID)
						  start with DCS_CAT_CHLDCAT.CATEGORY_ID =L_CATEGORY_ID
						  connect by DCS_CAT_CHLDCAT.CATEGORY_ID = prior CHILD_CAT_ID
						  ORDER SIBLINGS BY CHILD_CAT_ID
						)
				  WHERE LVL <>1;


				  --Populating product_id and promotion_id in global temporary table (GT_PROD_PROM) from category_id
				  insert into GT_PROD_PROM (product_id,PROMOTION_ID)
				  SELECT child_prd_id product_id,PAR_PROMOTION_ID
				  from
				  (SELECT CHILD_CAT_ID,
						 DCS_CAT_CHLDCAT.CATEGORY_ID par_cat_id,
						 TRU_TARGETED_CATEGORY.PROMOTION_ID PAR_PROMOTION_ID
				  from   DCS_CAT_CHLDCAT left outer join TRU_TARGETED_CATEGORY on(DCS_CAT_CHLDCAT.CATEGORY_ID = TRU_TARGETED_CATEGORY.CATEGORY_ID)
				  start with DCS_CAT_CHLDCAT.CATEGORY_ID =L_CATEGORY_ID
				  connect by DCS_CAT_CHLDCAT.CATEGORY_ID = prior CHILD_CAT_ID
				  ORDER SIBLINGS BY CHILD_CAT_ID
				  ) CAT_PROM
				  INNER JOIN DCS_CAT_CHLDPRD ON(CAT_PROM.PAR_CAT_ID = DCS_CAT_CHLDPRD.CATEGORY_ID)
				  union
				  SELECT child_prd_id product_id,PAR_PROMOTION_ID
				  from
				  (SELECT CHILD_CAT_ID,
						 DCS_CAT_CHLDCAT.CATEGORY_ID par_cat_id,
						 TRU_TARGETED_CATEGORY.PROMOTION_ID PAR_PROMOTION_ID,
						 child_quli.PROMOTION_ID chld_PROMOTION_ID
				  from   DCS_CAT_CHLDCAT left outer join TRU_TARGETED_CATEGORY on(DCS_CAT_CHLDCAT.CATEGORY_ID = TRU_TARGETED_CATEGORY.CATEGORY_ID)
						 LEFT OUTER JOIN TRU_TARGETED_CATEGORY CHILD_QULI ON(DCS_CAT_CHLDCAT.CHILD_CAT_ID = CHILD_QULI.CATEGORY_ID)
				  start with DCS_CAT_CHLDCAT.CATEGORY_ID =L_CATEGORY_ID
				  connect by DCS_CAT_CHLDCAT.CATEGORY_ID = prior CHILD_CAT_ID
				  ORDER SIBLINGS BY CHILD_CAT_ID
				  ) CAT_PROM
				  INNER JOIN DCS_CAT_CHLDPRD ON(CAT_PROM.CHILD_CAT_ID = DCS_CAT_CHLDPRD.CATEGORY_ID)
				  union
				  SELECT child_prd_id product_id,chld_PROMOTION_ID
				  from
				  (SELECT CHILD_CAT_ID,
						 child_quli.PROMOTION_ID chld_PROMOTION_ID
				  from   DCS_CAT_CHLDCAT
						 left outer join TRU_TARGETED_CATEGORY child_quli on(DCS_CAT_CHLDCAT.CHILD_CAT_ID = child_quli.CATEGORY_ID)

				  start with DCS_CAT_CHLDCAT.CATEGORY_ID =L_CATEGORY_ID
				  connect by DCS_CAT_CHLDCAT.CATEGORY_ID = prior CHILD_CAT_ID
				  ORDER SIBLINGS BY CHILD_CAT_ID
				  ) CAT_PROM
				  inner join DCS_CAT_CHLDPRD on(cat_prom.CHILD_CAT_ID = DCS_CAT_CHLDPRD.category_id);


				 --Finding the  child category level from qualifid category list
				 SELECT COUNT(DISTINCT LVL) INTO L_COUNT
					  FROM
					   (SELECT
							 LEVEL LVL
					   FROM   DCS_CAT_CHLDCAT
							 START WITH DCS_CAT_CHLDCAT.CATEGORY_ID =L_CATEGORY_ID
							 CONNECT BY DCS_CAT_CHLDCAT.CATEGORY_ID = PRIOR CHILD_CAT_ID
							 ORDER SIBLINGS BY CHILD_CAT_ID
					   );

				--mapping product_id with promotion_id using child category list  start
				 IF L_COUNT > 1
				  THEN
					  FOR J IN 1..L_COUNT
					  LOOP

						 IF J =1 THEN

							INSERT INTO GT_PROD_PROM (PRODUCT_ID,PROMOTION_ID)
							SELECT CHILD_PRD_ID PRODUCT_ID,PAR_PROMOTION_ID
							from
							(select   CHILD_CAT_ID,PAR_PROMOTION_ID
							from
							(SELECT DISTINCT CHILD_CAT_ID
							from
							(SELECT  CHILD_CAT_ID,level lvl
											   FROM   DCS_CAT_CHLDCAT
													 START WITH DCS_CAT_CHLDCAT.CATEGORY_ID = L_CATEGORY_ID
													 CONNECT BY DCS_CAT_CHLDCAT.CATEGORY_ID = PRIOR CHILD_CAT_ID
													 ORDER SIBLINGS BY CHILD_CAT_ID
							) WHERE LVL <>1
							) a,
							(SELECT DISTINCT PAR_PROMOTION_ID
							from
							(SELECT  TRU_TARGETED_CATEGORY.PROMOTION_ID PAR_PROMOTION_ID,LEVEL LVL
								FROM   DCS_CAT_CHLDCAT LEFT OUTER JOIN TRU_TARGETED_CATEGORY ON(DCS_CAT_CHLDCAT.CATEGORY_ID = TRU_TARGETED_CATEGORY.CATEGORY_ID)
													 START WITH DCS_CAT_CHLDCAT.CATEGORY_ID = L_CATEGORY_ID
													 CONNECT BY DCS_CAT_CHLDCAT.CATEGORY_ID = PRIOR CHILD_CAT_ID
													 ORDER SIBLINGS BY CHILD_CAT_ID
											)WHERE LVL =1
							) B
							)
							INNER JOIN DCS_CAT_CHLDPRD ON(CHILD_CAT_ID = DCS_CAT_CHLDPRD.CATEGORY_ID);

						 ELSIF  J > 1 THEN

							FOR K IN (
										  SELECT DISTINCT CATEGORY_ID,nvl(PAR_PROMOTION_ID,'d_prom') PAR_PROMOTION_ID
										  from
										  (SELECT  DCS_CAT_CHLDCAT.CATEGORY_ID,TRU_TARGETED_CATEGORY.PROMOTION_ID PAR_PROMOTION_ID,level lvl
															 FROM   DCS_CAT_CHLDCAT   LEFT OUTER JOIN TRU_TARGETED_CATEGORY ON(DCS_CAT_CHLDCAT.CATEGORY_ID = TRU_TARGETED_CATEGORY.CATEGORY_ID)
																   START WITH DCS_CAT_CHLDCAT.CATEGORY_ID = L_CATEGORY_ID
																   CONNECT BY DCS_CAT_CHLDCAT.CATEGORY_ID = PRIOR CHILD_CAT_ID
																   ORDER SIBLINGS BY CHILD_CAT_ID
										  ) WHERE LVL = J
										)

							LOOP

								  L_STATEMENT_STR := 'SELECT  CHILD_CAT_ID FROM DCS_CAT_CHLDCAT WHERE CATEGORY_ID  = '||''''||K.CATEGORY_ID||'''';
								  L_STATEMENT_STR1 := 'select child_cat_id,'||''''||K.PAR_PROMOTION_ID||''''||' from dcs_cat_chldcat where category_id in(';
								  L_STATEMENT_STR2 := 'select child_cat_id from dcs_cat_chldcat where category_id in(';
								  L_STR := ')';
								  l_g_str :=')';
                  L_STATEMENT_STR3 := null;

								  FOR L IN 1..50
								  LOOP

									  L_STATEMENT_1 :=  L_STATEMENT_STR1||L_STATEMENT_STR3||L_STATEMENT_STR||L_STR;
                   -- dbms_output.put_line ('target :'||L_STATEMENT_1);
									  EXECUTE IMMEDIATE 'insert into GT_CAT_PROM(CATEGORY_ID,PROMOTION_ID)'||L_STATEMENT_1;

									  IF SQL%ROWCOUNT = 0
									  THEN
										EXIT;
									  END IF;
									  L_STR := l_g_str||L_STR;
                    --dbms_output.put_line ('L_STR :' ||l||'-'|| L_STR);
									  L_STATEMENT_STR3 := L_STATEMENT_STR2||L_STATEMENT_STR3;

								  END LOOP;
							END LOOP;
						 END IF;
					  END LOOP;
				 END IF;
			END LOOP;

			--mapping product_id with promotion_id using child category list  end

			CLOSE C2;

			INSERT INTO GT_PROD_PROM (PRODUCT_ID,PROMOTION_ID)
			SELECT CHILD_PRD_ID PRODUCT_ID,PROMOTION_ID
			FROM GT_CAT_PROM INNER JOIN DCS_CAT_CHLDPRD ON(GT_CAT_PROM.CATEGORY_ID = DCS_CAT_CHLDPRD.CATEGORY_ID);

			INSERT INTO TRU_PRODUCT_TARGETED_PROMOS(PRODUCT_ID,SEQUENCE_NUM,PROMOTION_ID)
			SELECT PRODUCT_ID,
				   ROW_NUMBER() OVER(PARTITION BY PRODUCT_ID ORDER BY PROMOTION_ID) -1 SEQUENCE_NUM,
				   PROMOTION_ID
			FROM(
						SELECT PRODUCT_ID,PROMOTION_ID FROM GT_PROD_PROM
						UNION
						SELECT PRODUCT_ID,PROMOTION_ID FROM TRU_TARGETED_PROD
				) WHERE PROMOTION_ID IS NOT NULL and PROMOTION_ID <>'d_prom';
			COMMIT;

	END;


------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            ------------------------------------------TRU_PRODUCT_TARGETED_PROMOS table population start------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------


EXCEPTION  WHEN OTHERS THEN
RAISE;

END PR_QUALIF_PROD_PRDS;

