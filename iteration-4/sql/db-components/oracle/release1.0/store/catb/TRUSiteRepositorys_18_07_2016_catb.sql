CREATE TABLE tru_static_page_folder (
	id 			varchar2(254)	NOT NULL REFERENCES site_configuration(id),
	sequence_num 		INTEGER	NOT NULL,
	static_page_folder 	varchar2(254)	NULL,
	PRIMARY KEY(id, sequence_num)
);