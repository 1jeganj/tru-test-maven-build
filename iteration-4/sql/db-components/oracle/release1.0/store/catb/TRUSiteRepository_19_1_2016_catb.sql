--Start - Added for payeezy enable/ disable flag - 19th Jan 2016
ALTER TABLE TRU_SITE_CONFIGURATION ADD ENABLE_PAYEEZY NUMBER(1)	DEFAULT 1	CHECK (ENABLE_PAYEEZY IN (0, 1));
--End - Added for payeezy enable/ disable flag - 19th Jan 2016