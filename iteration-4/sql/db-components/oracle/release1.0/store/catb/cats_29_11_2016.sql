CREATE TABLE tru_collection_display_status (
	product_id 		varchar2(40)	NOT NULL REFERENCES dcs_product(product_id),
	display_status 		varchar2(40)	NULL,
	PRIMARY KEY(product_id)
);