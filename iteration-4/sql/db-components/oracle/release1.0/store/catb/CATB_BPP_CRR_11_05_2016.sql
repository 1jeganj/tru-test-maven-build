 drop table tru_bpp;
 CREATE TABLE tru_bpp (
	bpp_id 			varchar2(254)	NOT NULL,
	group_number 		varchar2(254)	NULL,
	bpp_name 		varchar2(254)	NULL,
	low 			number(28, 20)	NULL,
	high 			number(28, 20)	NULL,
	term 			varchar2(254)	NULL,
	retail 			number(28, 20)	NULL,
	SKU 			varchar2(254)	NULL,
	cart_description 	varchar2(254)	NULL,
	PRIMARY KEY(bpp_id)
);