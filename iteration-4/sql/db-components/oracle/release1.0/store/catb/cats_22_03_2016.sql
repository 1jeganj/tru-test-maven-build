alter table tru_sku drop column interest

alter table tru_sku_prod_featuers modify prod_feature varchar2(255)

create table tru_interests (
	sku_id 			varchar2(40)	not null references dcs_sku(sku_id),
	sequence_num 		integer	not null,
	interests 		varchar2(255)	null,
	primary key(sku_id, sequence_num)
);

create table tru_finder_eligible (
	sku_id 			varchar2(40)	not null references dcs_sku(sku_id),
	sequence_num 		integer	not null,
	finder_eligible 	varchar2(255)	null,
	primary key(sku_id, sequence_num)
);

create table tru_special_features (
	sku_id 			varchar2(40)	not null references dcs_sku(sku_id),
	sequence_num 		integer	not null,
	special_features 	varchar2(255)	null,
	primary key(sku_id, sequence_num)
);