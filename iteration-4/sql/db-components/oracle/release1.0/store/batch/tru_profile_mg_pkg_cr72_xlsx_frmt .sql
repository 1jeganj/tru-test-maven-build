--This script should run under TRUBATCH schema.
CREATE TABLE TRU_TEMP_PROFILE_COUNTRY
(
  COUNTRY_CODE        VARCHAR2(255),
  VALID_COUNTRY_CODE  VARCHAR2(255)
)INITRANS 32;

CREATE TABLE TRU_TEMP_PROFILE_STATE
(
  STATE_CODE        VARCHAR2(255),
  VALID_STATE_CODE  VARCHAR2(255)
)INITRANS 32;

TRUNCATE TABLE TRU_TEMP_PROFILE_ADDRESS;
ALTER TABLE TRU_TEMP_PROFILE_ADDRESS MODIFY (STATE NOT NULL,COUNTRY NOT NULL);

create or replace
package as_read_xlsx
is
/**********************************************
**
** Author: Anton Scheffer
** Date: 19-01-2013
** Website: http://technology.amis.nl/blog
**
** Changelog:
** 18-02-2013 - Ralph Bieber
                Handle cell type "str" to prevent ORA-06502
                if cell content is a string calculated by formula, 
                then cell type is "str" instead of "s" and value is inside <v> tag
** 19-02-2013 - Ralph Bieber
                Add column formula in tp_one_cell record, to show, if value is calculated by formula 
** 20-02-2013 - Anton Scheffer
                Handle cell types 'inlineStr' and 'e' to prevent ORA-06502
** 19-03-2013 - Anton Scheffer
                Support for formatted and empty strings
                Handle columns per row to prevent ORA-31186: Document contains too many nodes 
** 12-06-2013 - Anton Scheffer
                Handle sharedStrings.xml on older Oracle database versions
** 18-09-2013 - Anton Scheffer
                Fix for LPX-00200 could not convert from encoding UTF-8 to ...
                (Note, this is an error I can't reproduce myself, maybe depending on database version and characterset)
                Thank you Stanislav Safonov for this solution
                Handle numbers with scientific notation
** 20-01-2014 - Anton Scheffer
                Fix for a large number (60000+) of strings
** 16-05-2014 - Anton Scheffer
                round to 15 digits

******************************************************************************
******************************************************************************
Copyright (C) 2013 by Anton Scheffer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

******************************************************************************
*************************************************************************** */
/*
**
** Some examples
**
--
-- every sheet and every cell
    select *
    from table( as_read_xlsx.read( as_read_xlsx.file2blob( 'DOC', 'Book1.xlsx' ) ) )
--
-- cell A3 from the first and the second sheet
    select *
    from table( as_read_xlsx.read( as_read_xlsx.file2blob( 'DOC', 'Book1.xlsx' ), '1:2', 'A3' ) )
--
-- every cell from the sheet with the name "Sheet3"
    select *
    from table( as_read_xlsx.read( as_read_xlsx.file2blob( 'DOC', 'Book1.xlsx' ), 'Sheet3' ) )
--
*/
  type tp_one_cell is record
    ( sheet_nr number(2)
    , sheet_name varchar(4000)
    , row_nr number(10)
    , col_nr number(10)
    , cell varchar2(100)
    , cell_type varchar2(1)
    , string_val varchar2(4000)
    , number_val number
    , date_val date
    , formula varchar2(4000)
  );
  type tp_all_cells is table of tp_one_cell;
--
  function read( p_xlsx blob, p_sheets varchar2 := null, p_cell varchar2 := null )
  return tp_all_cells pipelined;
--
  function file2blob
    ( p_dir varchar2
    , p_file_name varchar2
    )
  return blob;
--
end as_read_xlsx;
/

create or replace
package body as_read_xlsx
is
--
  function read( p_xlsx blob, p_sheets varchar2 := null, p_cell varchar2 := null )
  return tp_all_cells pipelined
  is
    t_date1904 boolean;
    type tp_date is table of boolean index by pls_integer;
    t_xf_date tp_date;
    t_numfmt_date tp_date;
    type tp_strings is table of varchar2(32767) index by pls_integer;
    t_strings tp_strings;
    t_sheet_ids tp_strings;
    t_sheet_names tp_strings;
    t_r varchar2(32767);
    t_s varchar2(32767);
    t_val varchar2(32767);
    t_t varchar2(400);
    t_nr number;
    t_c pls_integer;
    t_x pls_integer;
    t_xx pls_integer;
    t_ns varchar2(200) := 'xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main"';
    t_nd dbms_xmldom.domnode;
    t_nd2 dbms_xmldom.domnode;
    t_nl dbms_xmldom.domnodelist;
    t_nl2 dbms_xmldom.domnodelist;
    t_nl3 dbms_xmldom.domnodelist;
    t_one_cell tp_one_cell;
--
    function blob2node( p_blob blob )
    return dbms_xmldom.domnode
    is
    begin
      if p_blob is null or dbms_lob.getlength( p_blob ) = 0
      then
        return null;
      end if;
      return dbms_xmldom.makenode( dbms_xmldom.getdocumentelement( dbms_xmldom.newdomdocument( xmltype( p_blob, nls_charset_id( 'AL32UTF8' ) ) ) ) );
    exception
      when others
      then
        declare
          t_nd dbms_xmldom.domnode;
          t_clob         clob;
          t_dest_offset  integer;
          t_src_offset   integer;
          t_lang_context number := dbms_lob.default_lang_ctx;
          t_warning      integer;
        begin
          dbms_lob.createtemporary( t_clob, true );
          t_dest_offset := 1;
          t_src_offset  := 1;
          dbms_lob.converttoclob( t_clob
                                , p_blob
                                , dbms_lob.lobmaxsize
                                , t_dest_offset
                                , t_src_offset
                                , nls_charset_id('AL32UTF8')
                                , t_lang_context
                                , t_warning
                                );
          t_nd := dbms_xmldom.makenode( dbms_xmldom.getdocumentelement( dbms_xmldom.newdomdocument( t_clob ) ) );
          dbms_lob.freetemporary( t_clob );
          return t_nd;
      end;
    end;
--  
    function blob2num( p_blob blob, p_len integer, p_pos integer )
    return number
    is
    begin
      return utl_raw.cast_to_binary_integer( dbms_lob.substr( p_blob, p_len, p_pos ), utl_raw.little_endian );
    end;
--  
    function little_endian( p_big number, p_bytes pls_integer := 4 )
    return raw
    is
    begin
      return utl_raw.substr( utl_raw.cast_from_binary_integer( p_big, utl_raw.little_endian ), 1, p_bytes );
    end;
--  
    function col_alfan( p_col varchar2 )
    return pls_integer
    is
    begin
      return ascii( substr( p_col, -1 ) ) - 64
           + nvl( ( ascii( substr( p_col, -2, 1 ) ) - 64 ) * 26, 0 )
           + nvl( ( ascii( substr( p_col, -3, 1 ) ) - 64 ) * 676, 0 );
    end;
--  
    function get_file
      ( p_zipped_blob blob
      , p_file_name varchar2
      )
    return blob
    is
      t_tmp blob;
      t_ind integer;
      t_hd_ind integer;
      t_fl_ind integer;
      t_encoding varchar2(10);
      t_len integer;
    begin
      t_ind := dbms_lob.getlength( p_zipped_blob ) - 21;
      loop
        exit when t_ind < 1 or dbms_lob.substr( p_zipped_blob, 4, t_ind ) = hextoraw( '504B0506' ); -- End of central directory signature
        t_ind := t_ind - 1;
      end loop;
--  
      if t_ind <= 0
      then
        return null;
      end if;
--  
      t_hd_ind := blob2num( p_zipped_blob, 4, t_ind + 16 ) + 1;
      for i in 1 .. blob2num( p_zipped_blob, 2, t_ind + 8 )
      loop
        if utl_raw.bit_and( dbms_lob.substr( p_zipped_blob, 1, t_hd_ind + 9 ), hextoraw( '08' ) ) = hextoraw( '08' )
        then
          t_encoding := 'AL32UTF8'; -- utf8
        else
          t_encoding := 'US8PC437'; -- IBM codepage 437
        end if;
        if p_file_name = utl_i18n.raw_to_char
                           ( dbms_lob.substr( p_zipped_blob
                                            , blob2num( p_zipped_blob, 2, t_hd_ind + 28 )
                                            , t_hd_ind + 46
                                            )
                           , t_encoding
                           )
        then
          t_len := blob2num( p_zipped_blob, 4, t_hd_ind + 24 ); -- uncompressed length
          if t_len = 0
          then
            if substr( p_file_name, -1 ) in ( '/', '\' )
            then  -- directory/folder
              return null;
            else -- empty file
              return empty_blob();
            end if;
          end if;
--  
          if dbms_lob.substr( p_zipped_blob, 2, t_hd_ind + 10 ) = hextoraw( '0800' ) -- deflate
          then
            t_fl_ind := blob2num( p_zipped_blob, 4, t_hd_ind + 42 );
            t_tmp := hextoraw( '1F8B0800000000000003' ); -- gzip header
            dbms_lob.copy( t_tmp
                         , p_zipped_blob
                         ,  blob2num( p_zipped_blob, 4, t_hd_ind + 20 )
                         , 11
                         , t_fl_ind + 31
                         + blob2num( p_zipped_blob, 2, t_fl_ind + 27 ) -- File name length
                         + blob2num( p_zipped_blob, 2, t_fl_ind + 29 ) -- Extra field length
                         );
            dbms_lob.append( t_tmp, utl_raw.concat( dbms_lob.substr( p_zipped_blob, 4, t_hd_ind + 16 ) -- CRC32
                                                  , little_endian( t_len ) -- uncompressed length
                                                  )
                           );
            return utl_compress.lz_uncompress( t_tmp );
          end if;
--  
          if dbms_lob.substr( p_zipped_blob, 2, t_hd_ind + 10 ) = hextoraw( '0000' ) -- The file is stored (no compression)
          then
            t_fl_ind := blob2num( p_zipped_blob, 4, t_hd_ind + 42 );
            dbms_lob.createtemporary( t_tmp, true );
            dbms_lob.copy( t_tmp
                         , p_zipped_blob
                         , t_len
                         , 1
                         , t_fl_ind + 31
                         + blob2num( p_zipped_blob, 2, t_fl_ind + 27 ) -- File name length
                         + blob2num( p_zipped_blob, 2, t_fl_ind + 29 ) -- Extra field length
                         );
            return t_tmp;
          end if;
        end if;
        t_hd_ind := t_hd_ind + 46
                  + blob2num( p_zipped_blob, 2, t_hd_ind + 28 )  -- File name length
                  + blob2num( p_zipped_blob, 2, t_hd_ind + 30 )  -- Extra field length
                  + blob2num( p_zipped_blob, 2, t_hd_ind + 32 ); -- File comment length
      end loop;
--  
      return null;
    end;
--
  begin
    t_one_cell.cell_type := 'S';
    t_one_cell.sheet_name := 'This doesn''t look like an Excel (xlsx) file to me!';
    t_one_cell.string_val := t_one_cell.sheet_name;
    if dbms_lob.substr( p_xlsx, 4, 1 ) != hextoraw( '504B0304' )
    then
      pipe row( t_one_cell );
      return;
    end if;
    t_nd := blob2node( get_file( p_xlsx, 'xl/workbook.xml' ) );
    if dbms_xmldom.isnull( t_nd )
    then
      pipe row( t_one_cell );
      return;
    end if;
    t_date1904 := lower( dbms_xslprocessor.valueof( t_nd, '/workbook/workbookPr/@date1904', t_ns ) ) in ( 'true', '1' );
    t_nl := dbms_xslprocessor.selectnodes( t_nd, '/workbook/sheets/sheet', t_ns );
    for i in 0 .. dbms_xmldom.getlength( t_nl ) - 1
    loop
      t_sheet_ids( i + 1 ) := dbms_xslprocessor.valueof( dbms_xmldom.item( t_nl, i ), '@r:id', 'xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"' );
      t_sheet_names( i + 1 ) := dbms_xslprocessor.valueof( dbms_xmldom.item( t_nl, i ), '@name' );
    end loop;
    t_nd := blob2node( get_file( p_xlsx, 'xl/styles.xml' ) );
    t_nl := dbms_xslprocessor.selectnodes( t_nd, '/styleSheet/numFmts/numFmt', t_ns );
    for i in 0 .. dbms_xmldom.getlength( t_nl ) - 1
    loop
      t_val := dbms_xslprocessor.valueof( dbms_xmldom.item( t_nl, i ), '@formatCode' );
      if (  instr( t_val, 'dd' ) > 0
         or instr( t_val, 'mm' ) > 0
         or instr( t_val, 'yy' ) > 0
         )
      then
        t_numfmt_date( dbms_xslprocessor.valueof( dbms_xmldom.item( t_nl, i ), '@numFmtId' ) ) := true;
      end if;
    end loop;
    t_numfmt_date( 14 ) := true;
    t_numfmt_date( 15 ) := true;
    t_numfmt_date( 16 ) := true;
    t_numfmt_date( 17 ) := true;
    t_numfmt_date( 22 ) := true;
    t_nl := dbms_xslprocessor.selectnodes( t_nd, '/styleSheet/cellXfs/xf/@numFmtId', t_ns );
    for i in 0 .. dbms_xmldom.getlength( t_nl ) - 1
    loop
      t_xf_date( i ) := t_numfmt_date.exists( dbms_xmldom.getnodevalue( dbms_xmldom.item( t_nl, i ) ) );
    end loop;
    t_nd := blob2node( get_file( p_xlsx, 'xl/sharedStrings.xml' ) );
    if not dbms_xmldom.isnull( t_nd )
    then
      t_x := 0;
      t_xx := 5000;
      loop
        t_nl := dbms_xslprocessor.selectnodes( t_nd, '/sst/si[position()>="' || to_char( t_x * t_xx + 1 ) || '" and position()<=" ' || to_char( ( t_x + 1 ) * t_xx ) || '"]', t_ns );
        exit when dbms_xmldom.getlength( t_nl ) = 0;
        t_x := t_x + 1;
        for i in 0 .. dbms_xmldom.getlength( t_nl ) - 1
        loop
          t_c := t_strings.count;
          t_strings( t_c ) := dbms_xslprocessor.valueof( dbms_xmldom.item( t_nl, i ), '.' );
          if t_strings( t_c ) is null
          then 
            t_strings( t_c ) := dbms_xslprocessor.valueof( dbms_xmldom.item( t_nl, i ), '*/text()' );
            if t_strings( t_c ) is null
            then 
              t_nl2 := dbms_xslprocessor.selectnodes( dbms_xmldom.item( t_nl, i ), 'r/t/text()' );
              for j in 0 .. dbms_xmldom.getlength( t_nl2 ) - 1
              loop
                t_strings( t_c ) := t_strings( t_c ) || dbms_xmldom.getnodevalue( dbms_xmldom.item( t_nl2, j ) );
              end loop;
            end if;
          end if;
        end loop;
      end loop;
    end if;
    t_nd2 := blob2node( get_file( p_xlsx, 'xl/_rels/workbook.xml.rels' ) );
    for i in 1 .. t_sheet_ids.count
    loop
      if ( p_sheets is null
         or instr( ':' || p_sheets || ':', ':' || to_char( i ) || ':' ) > 0
         or instr( ':' || p_sheets || ':', ':' || t_sheet_names( i ) || ':' ) > 0
         )
      then
        t_val := dbms_xslprocessor.valueof( t_nd2, '/Relationships/Relationship[@Id="' || t_sheet_ids( i ) || '"]/@Target', 'xmlns="http://schemas.openxmlformats.org/package/2006/relationships"' );
        t_one_cell.sheet_nr := i;
        t_one_cell.sheet_name := t_sheet_names( i );
        t_nd := blob2node( get_file( p_xlsx, 'xl/' || t_val ) );
        t_nl3 := dbms_xslprocessor.selectnodes( t_nd, '/worksheet/sheetData/row' );
        for r in 0 .. dbms_xmldom.getlength( t_nl3 ) - 1
        loop
          t_nl2 := dbms_xslprocessor.selectnodes( dbms_xmldom.item( t_nl3, r ), 'c' );
          for j in 0 .. dbms_xmldom.getlength( t_nl2 ) - 1
          loop
            t_one_cell.date_val := null;
            t_one_cell.number_val := null;
            t_one_cell.string_val := null;
            t_r := dbms_xslprocessor.valueof( dbms_xmldom.item( t_nl2, j ), '@r', t_ns );
            t_val := dbms_xslprocessor.valueof( dbms_xmldom.item( t_nl2, j ), 'v' );
            -- see Changelog 2013-02-19 formula column 
            t_one_cell.formula := dbms_xslprocessor.valueof( dbms_xmldom.item( t_nl2, j ), 'f' );
            -- see Changelog 2013-02-18 type='str' 
            t_t := dbms_xslprocessor.valueof( dbms_xmldom.item( t_nl2, j ), '@t' );
            if t_t in ( 'str', 'inlineStr', 'e' )
            then
              t_one_cell.cell_type := 'S';
              t_one_cell.string_val := t_val;
            elsif t_t = 's'
            then
              t_one_cell.cell_type := 'S';
              if t_val is not null
              then
                t_one_cell.string_val := t_strings( to_number( t_val ) );
              end if;
            else
              t_s := dbms_xslprocessor.valueof( dbms_xmldom.item( t_nl2, j ), '@s' );
              t_nr := to_number( t_val
                               , case when instr( t_val, 'E' ) = 0
                                   then translate( t_val, '.012345678,-+', 'D999999999' )
                                   else translate( substr( t_val, 1, instr( t_val, 'E' ) - 1 ), '.012345678,-+', 'D999999999' ) || 'EEEE'
                                 end
                               , 'NLS_NUMERIC_CHARACTERS=.,'
                               );
              if t_s is not null and t_xf_date( to_number( t_s ) )
              then
                t_one_cell.cell_type := 'D';
                if t_date1904
                then
                  t_one_cell.date_val := to_date('01-01-1904','DD-MM-YYYY') + to_number( t_nr );
                else
                  t_one_cell.date_val := to_date('01-03-1900','DD-MM-YYYY') + ( to_number( t_nr ) - 61 );
                end if;
              else
                t_one_cell.cell_type := 'N';
                t_nr := round( t_nr, 14 - substr( to_char( t_nr, 'TME' ), -3 ) );
                t_one_cell.number_val := t_nr;
              end if;
            end if;
            t_one_cell.row_nr := ltrim( t_r, rtrim( t_r, '0123456789' ) );
            t_one_cell.col_nr := col_alfan( rtrim( t_r, '0123456789' ) );
            t_one_cell.cell := t_r;
            if p_cell is null or t_r = upper( p_cell )
            then
              pipe row( t_one_cell );
            end if;
          end loop;
        end loop;
      end if;
    end loop;
    return;
  end;
--
  function file2blob
    ( p_dir varchar2
    , p_file_name varchar2
    )
  return blob
  is
    file_lob bfile;
    file_blob blob;
  begin
    file_lob := bfilename( p_dir, p_file_name );
    dbms_lob.open( file_lob, dbms_lob.file_readonly );
    dbms_lob.createtemporary( file_blob, true );
    dbms_lob.loadfromfile( file_blob, file_lob, dbms_lob.lobmaxsize );
    dbms_lob.close( file_lob );
    return file_blob;
  exception
    when others then
      if dbms_lob.isopen( file_lob ) = 1
      then
        dbms_lob.close( file_lob );
      end if;
      if dbms_lob.istemporary( file_blob ) = 1
      then
        dbms_lob.freetemporary( file_blob );
      end if;
      raise;
  end;
--
END AS_READ_XLSX;
/

create or replace
PACKAGE TRU_PROFILE_MIG_PKG
/*
***************************************************************TRU_PROFILE_MIG_PKG**********************************************************************************************************
**    i.  Purpose
**
**            This package use to migrate data from XML file to all profile related target table
**
**
**    ii. Procedures

***         i.PROFILE_MIG_STAGE_MAIN
            -------------------------
               This procedure insert the XML file in temp table(TRU_TEMP_PROFILE_XML) and call PROFILE_XML_STAGE_LOAD procedure to load profiles records into staging tables.
			   
			   IN parameter --    P_DIR_NAME,P_FILE_NAME and P_TRUNCATE (It holds XML file directory name , it's file name  and truncate flag(y or n))


***         ii.    PROFILE_XML_STAGE_LOAD
			-----------------------------
                This procedure parse the XML file and load data into staging tables
				   TRU_TEMP_PROFILE_USERINFO  	: table holds user info details
				   TRU_TEMP_PROFILE_ADDRESS		: table holds user address  details
				   TRU_TEMP_PROFILE_CREDITCARD	: table holds user credit cards  details
				   
				IN parameter --    P_FILE_NAME (it holds XML file name,  it  picks the profile from the XML file and load staging table)  
				   

***         iii.PROFILE_MIG_TARGT
			-----------------------
				This procedure migrate the profiles data from stagin tables into ATG target tables
				List of ATG target table is going to migrate

					DPS_USER 
					TRU_DPSX_USER 
					DPS_USER_ADDRESS 
					DPS_CONTACT_INFO 
					TRU_DPSX_CONTACT_INFO
					DPS_OTHER_ADDR 
					DPS_CREDIT_CARD 				
					TRU_DPSX_CREDIT_CARD 
					DPS_USR_CREDITCARD 
				   
				IN parameter --    P_LAOD_TYPE(value should be 'initial' or 'delta' to decide the profile migration type)


***         iV.PROFILE_MIG_TARGT_BATCH
			-----------------------
				This procedure migrate the profiles data from staging tables into ATG target tables by batch wise commit. ( commit interval is 10000 records)
				   
				IN parameter --    P_LAOD_TYPE,P_COMMIT_INTRVL(P_LAOD_TYPE value should be 'initial' or 'delta' to decide the profile migration type and P_COMMIT_INTRVL value should be greater than 10000)


***         v. PROFILE_STAGE_STATE_LOAD
			-----------------------------
                This procedure resd the external file and load into TRU_TEMP_PROFILE_STATE  table
				   
				IN parameter --    P_DIR_NAME,P_FILE_NAME and P_TRUNCATE (It holds XML file directory name , it's file name  and truncate flag(y or n))
				   
***         vi. PROFILE_STAGE_COUNTRY_LOAD
			-----------------------------
                This procedure resd the external file and load into TRU_TEMP_PROFILE_COUNTRY  table
				
				IN parameter --    P_DIR_NAME,P_FILE_NAME and P_TRUNCATE (It holds XML file directory name , it's file name  and truncate flag(y or n)) 
				   

**   iii. Functions

            i.XML_FILEEXISTS
            ----------------------
			IN parameter --    P_DIRNAME,P_FILENAME (It holds XML file directory name and it's file name respectively)
            This function use to identify the XML file exists or not in the directory. 


**    iv. Creation History
**
**           Date         Created by                   Comments
**          ---------     -----------                ---------------
**          19/Jan/2016   Malleshwara.C              Initial version


*******************************************************************************************************************************************************************************************
*/


AS
	G_START                            number;
    G_END                              NUMBER;
	G_BFILE  BFILE;   
	G_PASWD VARCHAR2(255) := '748c97e6fad37513800acf4b81e2ba8af4249e7e';
	G_SALT_PASWD VARCHAR2(255) := 'ed3fd1cd845d2aba2dcbfaffeeafdc6458030a69';
	G_FILE_NAME  VARCHAR2(255);
	G_FILE_SIZE NUMBER(15,2);
	G_MIG_LOAD_ID  NUMBER;
  
	FUNCTION XML_FILEEXISTS
	(
		P_DIRNAME IN VARCHAR2,   
		P_FILENAME IN VARCHAR2
	) RETURN VARCHAR2;
	PROCEDURE PROFILE_MIG_STAGE_MAIN(P_DIR_NAME VARCHAR2,P_FILE_NAME VARCHAR2,P_TRUNCATE VARCHAR2);
    PROCEDURE PROFILE_XML_STAGE_LOAD(P_FILE_NAME VARCHAR2);
    PROCEDURE PROFILE_MIG_TARGT(P_LAOD_TYPE  varchar2);
	PROCEDURE PROFILE_MIG_TARGT_BATCH(P_LAOD_TYPE  varchar2,P_COMMIT_INTRVL PLS_INTEGER);
	PROCEDURE PROFILE_STAGE_STATE_LOAD(P_DIR_NAME VARCHAR2,P_FILE_NAME VARCHAR2,P_TRUNCATE VARCHAR2);
	PROCEDURE PROFILE_STAGE_COUNTRY_LOAD(P_DIR_NAME VARCHAR2,P_FILE_NAME VARCHAR2,P_TRUNCATE VARCHAR2);
	FUNCTION  GET_MAPPED_STATE_CODE(P_STATE_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION  GET_MAPPED_COUNTRY_CODE(P_COUNTRY_CODE IN VARCHAR2) RETURN VARCHAR2;
end TRU_PROFILE_MIG_PKG;
/
create or replace
package body TRU_PROFILE_MIG_PKG
AS
    
  FUNCTION XML_FILEEXISTS
	(
		P_DIRNAME IN VARCHAR2,   
		P_FILENAME IN VARCHAR2
		) RETURN VARCHAR2
	is
	L_FILENAME varchar2(255);
	L_EXISTS VARCHAR2(1000) := NULL ;
	begin
	
  if P_DIRNAME is null or P_DIRNAME = ' '
  then
     return 'ENTER VALID DIRECTORY NAME';
  end if;
  
  if P_FILENAME is null or P_FILENAME= ' '
  then
      return 'ENTER VALID FILE NAME';
  END IF;
  
	 --This loop convert comma seprated values(P_FILENAME - list of XML file name) into  records and check the file exists or not in the directory
	 FOR I IN (SELECT EXTRACT (VALUE (d), '//row/text()').getstringval() FILE_NAME 
							   FROM
								   (select xmltype (   '<rows><row>'
									|| REPLACE (P_FILENAME, ',', '</row><row>')
									|| '</row></rows>'
								   ) AS xmlval
									from DUAL) X,
								table (XMLSEQUENCE (extract (X.XMLVAL, '/rows/row')))D
                )
	  LOOP
								
								
	 G_BFILE := BFILENAME(UPPER(P_DIRNAME),I.FILE_NAME);
		  IF DBMS_LOB.FILEEXISTS(G_BFILE) = 0
		  THEN
			  L_EXISTS :=  L_EXISTS||','||I.FILE_NAME||':'||'Not Exist';
		  end if;
	  end LOOP;
    
    if L_EXISTS is null
    then
      return 'EXISTS';
    ELSE 
      return L_EXISTS;
    END IF;
  
	end XML_FILEEXISTS;


	PROCEDURE PROFILE_MIG_STAGE_MAIN (P_DIR_NAME VARCHAR2,P_FILE_NAME VARCHAR2,P_TRUNCATE VARCHAR2)
    IS
	  L_FILE_EXISTS VARCHAR2(1000);
	  L_SPLIT_THREAD number := 5;
	  L_SPLIT_num NUMBER;
	  L_START_NUM number := 0;
	  L_TOTAL_NUM number :=23;
	  L_JOB_STMNT VARCHAR2(4000);
	  L_FILE_NAME VARCHAR2(4000);
	  L_RPT_FILE VARCHAR2(40);
	  l_ex BOOLEAN;
	  l_flen NUMBER;
	  l_bsize NUMBER;
	  V_ERR VARCHAR2(255);
	  V_ERR_CD VARCHAR2(255);
	  L_USER         varchar2(40);
    BEGIN

		G_START :=DBMS_UTILITY.GET_TIME;
		
		--Checking XML file name exists or not in the directory
		L_FILE_EXISTS := TRU_PROFILE_MIG_PKG.XML_FILEEXISTS(P_DIRNAME => P_DIR_NAME,P_FILENAME => P_FILE_NAME);
		
		IF L_FILE_EXISTS <> 'EXISTS'
		THEN
			RAISE_APPLICATION_ERROR(-20101, L_FILE_EXISTS );
		END IF;
		
		L_FILE_NAME := P_FILE_NAME;
		
		IF lower(P_TRUNCATE) not in('y','n') or P_TRUNCATE is null
		THEN
			RAISE_APPLICATION_ERROR(-20104, 'P_TRUNCATE value should be either y or n' );
		END IF;
		
		IF lower(P_TRUNCATE) = 'y'
		THEN
			--Clearing all records in profile migration staging tables
			DELETE FROM TRU_TEMP_MIG_FILE_STATUS WHERE  JOB_NAME = 'PROF_MIG';
      
		  select SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA')  into L_USER FROM DUAL;
		  for tname in(SELECT EXTRACT (VALUE (d), '//row/text()').getstringval() TAB_NAME 
								   FROM 
									   (select xmltype (   '<rows><row>'
										|| REPLACE ('TRU_TEMP_PROFILE_ADDRESS,TRU_TEMP_PROFILE_CREDITCARD,TRU_TEMP_PROFILE_USERINFO', ',', '</row><row>')
										|| '</row></rows>'
									   ) AS xmlval
										from DUAL) X,
									table (XMLSEQUENCE (extract (X.XMLVAL, '/rows/row')))D)
			  LOOP
				 for cname in(select TABLE_NAME, CONSTRAINT_NAME, STATUS, OWNER
								from ALL_CONSTRAINTS
								where r_owner =l_user
								and CONSTRAINT_TYPE = 'R' 
								and r_constraint_name in
								 (
								   select constraint_name from all_constraints
								   where CONSTRAINT_TYPE in ('P', 'U')
								   and table_name = tname.TAB_NAME
								   and OWNER = L_USER
								 ))
				  LOOP
					  execute immediate ('alter table'||' '||CNAME.TABLE_NAME||' '||'disable constraint'||' '||CNAME.CONSTRAINT_NAME);
				  end LOOP;
				 execute immediate 'truncate table'||' '|| TNAME.TAB_NAME; 
				 for cname in(select TABLE_NAME, CONSTRAINT_NAME, STATUS, OWNER
								from ALL_CONSTRAINTS
								where r_owner =l_user
								and CONSTRAINT_TYPE = 'R' 
								and r_constraint_name in
								 (
								   select constraint_name from all_constraints
								   where CONSTRAINT_TYPE in ('P', 'U')
								   and table_name = tname.TAB_NAME
								   and OWNER = L_USER
								 ))
				  LOOP
					execute immediate ('alter table'||' '||CNAME.TABLE_NAME||' '||'enable constraint'||' '||CNAME.CONSTRAINT_NAME);
				  END LOOP;
			  END LOOP;
      
			--EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TEMP_PROFILE_USERINFO';
			--EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TEMP_PROFILE_ADDRESS';
			--EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TEMP_PROFILE_CREDITCARD';
			--EXECUTE IMMEDIATE 'TRUNCATE TABLE DML_ERROR_LOG';
			--EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TMP_XML_SPLIT';
		ELSIF lower(P_TRUNCATE) = 'n'
		THEN
			FOR V_FNAM IN (SELECT FILE_NAME||',' FILE_NAME FROM TRU_TEMP_MIG_FILE_STATUS WHERE JOB_NAME = 'PROF_MIG')
			LOOP
				SELECT REPLACE(L_FILE_NAME,V_FNAM.FILE_NAME,NULL) INTO L_FILE_NAME FROM DUAL;
				DBMS_OUTPUT.PUT_LINE('FILE_NAME TRIM : '||L_FILE_NAME); 
			END LOOP;
		END IF;
		

		FOR I IN (SELECT EXTRACT (VALUE (d), '//row/text()').getstringval() FILE_NAME 
					   FROM
						   (select xmltype (   '<rows><row>'
							|| REPLACE (L_FILE_NAME, ',', '</row><row>')
							|| '</row></rows>'
						   ) AS xmlval
							from DUAL) X,
						table (XMLSEQUENCE (extract (X.XMLVAL, '/rows/row')))D
				 )
		LOOP
			
			 --Gettin file zize in MB
			 UTL_FILE.FGETATTR(UPPER(P_DIR_NAME),I.FILE_NAME, L_EX, L_FLEN, L_BSIZE);
			 G_FILE_SIZE := round(L_FLEN/1000000,2);
			 
			BEGIN
				
				EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TEMP_PROFILE_XML';
			
				INSERT INTO TRU_TEMP_PROFILE_XML(XML_DATA) VALUES( xmltype(bfilename(UPPER(P_DIR_NAME),I.FILE_NAME),nls_charset_id('AL32UTF8'))); 
				
				DBMS_OUTPUT.PUT_LINE('Inserted XML file Name : '||I.FILE_NAME); 
				
				INSERT INTO TRU_TEMP_MIG_FILE_STATUS(FILE_NAME,JOB_NAME,START_DATE) VALUES(I.FILE_NAME,'PROF_MIG',SYSTIMESTAMP);
				
				COMMIT;
				
			EXCEPTION WHEN OTHERS THEN
				V_ERR := SUBSTR(SQLERRM, 1, 200);
				V_ERR_CD := SQLCODE;
				INSERT INTO TRU_TEMP_MIG_FILE_STATUS(FILE_NAME,JOB_NAME,START_DATE,STATUS,ERROR_CODE,ERROR_MSG) VALUES(I.FILE_NAME,'PROF_MIG',SYSTIMESTAMP,'temp_xml_insert_failed',V_ERR_CD,V_ERR);
				commit;
				GOTO LOOP1;
			END;
			  				
			BEGIN
			
				PROFILE_XML_STAGE_LOAD(P_FILE_NAME => I.FILE_NAME);   

			EXCEPTION WHEN OTHERS THEN
				GOTO LOOP1;
			END;
			
            <<LOOP1>>
			null;
			
		END LOOP;	
        
		G_END :=DBMS_UTILITY.GET_TIME;
		DBMS_OUTPUT.PUT_LINE('PROFILE_MIG_STAGE_MAIN: Time Taken to execute ' || (G_END-G_START)/100||' '||'seconds');
		
		EXCEPTION WHEN OTHERS THEN
		RAISE;
		
	END PROFILE_MIG_STAGE_MAIN;
	
	
	PROCEDURE PROFILE_XML_STAGE_LOAD
	( 
	  P_FILE_NAME VARCHAR2
	)
    IS

		L_START NUMBER;
		L_END NUMBER;
		L_COUNT NUMBER;
		L_FILE_NAME VARCHAR2(255);
		V_ERR VARCHAR2(255);
		V_ERR_CD VARCHAR2(255);
   
    BEGIN
		
		l_START := DBMS_UTILITY.GET_TIME;
		
		
		L_FILE_NAME := P_FILE_NAME;
    

			

				INSERT INTO TRU_TEMP_PROFILE_USERINFO
				  (		
						USER_ID,
						FIRST_NAME,
						LAST_NAME,
						LASTPWDUPDATE,
						REGISTRATION_DATE,
						EMAIL,
						DEFAULT_BILLING_ADDRESS,
						DEFAULT_SHIPPING_ADDRESS,
						DEFAULT_CREDITCARD,
						--SECONDARY_ADDRESSES,
						REWARD_NUMBER,
						LASTACTIVITY_DATE,
						--STATUS,
						LOCALE
				  )  
				  SELECT   
						USER_ID,
						FIRST_NAME,
						LAST_NAME,
						TO_TIMESTAMP(LASTPWDUPDATE,'mm/dd/yyyy hh24:mi:ss'),
						TO_TIMESTAMP(REGISTRATION_DATE,'mm/dd/yyyy hh24:mi:ss'),
						lower(EMAIL),
						DEFAULT_BILLING_ADDRESS,
						DEFAULT_SHIPPING_ADDRESS,
						DEFAULT_CREDITCARD,
						--SECONDARY_ADDRESSES,
						REWARD_NUMBER,
						TO_TIMESTAMP(LASTACTIVITY_DATE,'mm/dd/yyyy hh24:mi:ss'),
						--DECODE(STATUS,'true',1,'false',0),
						DECODE(LOCALE ,'unset',0,
								'en_US',1,
								'fr_FR',2,
								'ja_JP',3,
								'de_DE',4,
								'zh_CN',2000,
								'nl',2001,
								'en',2002,
								'en_AU',2003,
								'en_CA',2004,
								'en_GB',2005,
								'fr',2006,
								'de',2007,
								'it',2008,
								'ja',2009,
								'pt_BR',2010,
								'es',2011,
								'sv',2012,
								'el_GR',2013,
								'tr_TR',2014,
								'ko_KR',2015,
								'zh_TW',2016,
								'ru_RU',2017,
								'hu_HU',2018,
								'cs_CZ',2019,
								'pl_PL',2020,
								'da_DK',2021,
								'fi_FI',2022,
								'pt_PT',2023,
								'th',2024,0
						)

					  FROM (SELECT XML_DATA 
							FROM TRU_TEMP_PROFILE_XML
								   )TMP,
							XMLTABLE('/data/profile'
										  PASSING TMP.XML_DATA
										  columns 	USER_ID                 	VARCHAR2(255) PATH 'userInfo/property[@name="id"]',
													FIRST_NAME              	VARCHAR2(255) PATH 'userInfo/property[@name="firstName"]',
													LAST_NAME               	VARCHAR2(255) PATH 'userInfo/property[@name="lastName"]',
													LASTPWDUPDATE           	VARCHAR2(255) PATH 'userInfo/property[@name="lastProfileUpdate"]',
													REGISTRATION_DATE       	VARCHAR2(255) PATH 'userInfo/property[@name="registrationDate"]',
													EMAIL                   	VARCHAR2(255) PATH 'userInfo/property[@name="email"]',
													DEFAULT_BILLING_ADDRESS 	VARCHAR2(255) PATH 'userInfo/property[@name="defaultBillingAddress"]',
													DEFAULT_SHIPPING_ADDRESS 	VARCHAR2(255) PATH 'userInfo/property[@name="defaultShippingAddress"]',
													DEFAULT_CREDITCARD      	VARCHAR2(255) PATH 'userInfo/property[@name="defaultCreditCard"]',
													--SECONDARY_ADDRESSES     	VARCHAR2(255) PATH 'userInfo/property[@name="secondaryAddresses"]',
													REWARD_NUMBER 			    VARCHAR2(255) PATH 'userInfo/property[@name="rewardNumber"]',
													--STATUS		  				VARCHAR2(255) PATH 'userInfo/property[@name="profileStatus"]',
													LASTACTIVITY_DATE			VARCHAR2(255) PATH 'userInfo/property[@name="lastLoginDate"]',
													LOCALE						VARCHAR2(255) PATH 'userInfo/property[@name="locale"]'
									)
				LOG ERRORS INTO DML_ERROR_LOG ('TRU_TEMP_PROFILE_USERINFO :'||L_FILE_NAME) REJECT LIMIT UNLIMITED;
				
			     L_COUNT := SQL%ROWCOUNT;
				
				COMMIT;
				
				  
				  INSERT INTO TRU_TEMP_PROFILE_ADDRESS
					(
						USER_ID,
						ADDRESS_ID,
						NICK_NAME,
						FIRST_NAME,
						LAST_NAME,
						address1,
						ADDRESS2,
						ADDRESS3,
						COUNTY,
						city,
						STATE,
						POSTAL_CODE,
						PHONE_NUMBER,
						COUNTRY,
						EMAIL,
						--STATUS,
						LAST_ACTIVITY
					)
					
				 SELECT USR.USER_ID,
						ADDRESS_ID,
						NICK_NAME,
						FIRST_NAME,
						LAST_NAME,
						ADDRESS1,
						ADDRESS2,
						ADDRESS3,
						COUNTY,
						CITY,
						DECODE(TRU_PROFILE_MIG_PKG.GET_MAPPED_STATE_CODE(STATE),'N/A',STATE,TRU_PROFILE_MIG_PKG.GET_MAPPED_STATE_CODE(STATE)),
						POSTAL_CODE,
						PHONE_NUMBER,
						DECODE(TRU_PROFILE_MIG_PKG.GET_MAPPED_COUNTRY_CODE(COUNTRY),'N/A',COUNTRY,TRU_PROFILE_MIG_PKG.GET_MAPPED_COUNTRY_CODE(COUNTRY)),
						LOWER(EMAIL),
						--DECODE(USR.STATUS,'true',1,'false',0) STATUS,
						TO_TIMESTAMP(USR.LASTACTIVITY_DATE,'mm/dd/yyyy hh24:mi:ss') LASTACTIVITY_DATE
				 FROM
					 (SELECT XML_DATA 
										FROM TRU_TEMP_PROFILE_XML
											   )TMP,
										XMLTABLE('/data/profile/addresses/address'
													  PASSING TMP.XML_DATA
													  COLUMNS 	USER_ID     		VARCHAR2(255) PATH './../../userInfo/property[@name="id"]',
																--STATUS		  		VARCHAR2(255) PATH './../../userInfo/property[@name="profileStatus"]',
																LASTACTIVITY_DATE	VARCHAR2(255) PATH './../../userInfo/property[@name="lastLoginDate"]',
																ADDRESS_ID      	VARCHAR2(255) PATH 'property[@name="id"]',
																NICK_NAME       	VARCHAR2(255) PATH 'property[@name="nickName"]',
																FIRST_NAME      	VARCHAR2(255) PATH 'property[@name="firstName"]',
																LAST_NAME       	VARCHAR2(255) PATH 'property[@name="lastName"]',
																ADDRESS1        	VARCHAR2(255) PATH 'property[@name="address1"]',
																ADDRESS2        	VARCHAR2(255) PATH 'property[@name="address2"]',
																ADDRESS3        	VARCHAR2(255) PATH 'property[@name="address3"]',
																COUNTY      	  	VARCHAR2(255) PATH 'property[@name="county"]', 
																CITY       	  		VARCHAR2(255) PATH 'property[@name="city"]',
																STATE       	  	VARCHAR2(255) PATH 'property[@name="state"]',
																POSTAL_CODE 	  	VARCHAR2(255) PATH 'property[@name="postalCode"]', 
																PHONE_NUMBER 	  	VARCHAR2(255) PATH 'property[@name="phoneNumber"]', 
																COUNTRY         	VARCHAR2(255) PATH 'property[@name="country"]', 
																EMAIL       	  	VARCHAR2(255) PATH 'property[@name="email"]'
												)USR
								  
				  LOG ERRORS INTO DML_ERROR_LOG ('TRU_TEMP_PROFILE_ADDRESS:'||L_FILE_NAME) REJECT LIMIT UNLIMITED;
				  
				COMMIT;
				
				  INSERT INTO TRU_TEMP_PROFILE_CREDITCARD
					(
						USER_ID, 
						CREDIT_CARD_ID,
						NAME_ON_CARD,
						EXPIRATION_MONTH,
						CREDIT_CARD_NUMBER,
						BILLING_ADDRESS,
						EXPIRATION_YEAR,
						CREDIT_CARD_TYPE
						--STATUS
					)
				  SELECT USR.USER_ID, 
						 CREDIT_CARD_ID,
						 NAME_ON_CARD,
						 EXPIRATION_MONTH,
						 CREDIT_CARD_NUMBER,
						 BILLING_ADDRESS,
						 EXPIRATION_YEAR,
						 CREDIT_CARD_TYPE
						 --DECODE(USR.STATUS,'true',1,'false',0)

				  FROM
					(SELECT XML_DATA 
									FROM TRU_TEMP_PROFILE_XML
										   )TMP,
									XMLTABLE('/data/profile/creditCards/creditCard'
												  PASSING TMP.XML_DATA
												  COLUMNS 	USER_ID           	VARCHAR2(255) PATH './../../userInfo/property[@name="id"]',
															--STATUS		  	  	VARCHAR2(255) PATH './../../userInfo/property[@name="profileStatus"]',
															CREDIT_CARD_ID    	VARCHAR2(255) PATH 'property[@name="id"]', 
															NAME_ON_CARD      	VARCHAR2(255) PATH 'property[@name="nameOnCard"]',
															EXPIRATION_MONTH   	VARCHAR2(255) PATH 'property[@name="expirationMonth"]',
															CREDIT_CARD_NUMBER  VARCHAR2(255) PATH 'property[@name="creditCardNumber"]',
															BILLING_ADDRESS     VARCHAR2(255) PATH 'property[@name="billingAddress"]',
															EXPIRATION_YEAR     VARCHAR2(255) PATH 'property[@name="expirationYear"]',
															CREDIT_CARD_TYPE    VARCHAR2(255) PATH 'property[@name="creditCardType"]'
							  )USR
								
				  LOG ERRORS INTO DML_ERROR_LOG ('TRU_TEMP_PROFILE_CREDITCARD:'||L_FILE_NAME) REJECT LIMIT UNLIMITED;
				  
				 COMMIT;
	
    
		UPDATE TRU_TEMP_MIG_FILE_STATUS SET END_DATE = SYSTIMESTAMP, EXEC_TIME = SYSTIMESTAMP-START_DATE , STATUS = 'temp_load_completed', TOTAL_REC = L_COUNT,FILE_SIZE = G_FILE_SIZE
		WHERE FILE_NAME = P_FILE_NAME AND JOB_NAME = 'PROF_MIG';
		
		COMMIT;
   
		L_END :=DBMS_UTILITY.GET_TIME;
        DBMS_OUTPUT.PUT_LINE('PROFILE_XML_STAGE_LOAD: Time Taken to execute ' || (L_END-l_START)/100||' '||'seconds');
		
		EXCEPTION
		WHEN OTHERS THEN
			V_ERR := SUBSTR(SQLERRM, 1, 200);
			V_ERR_CD := SQLCODE;
			UPDATE TRU_TEMP_MIG_FILE_STATUS SET END_DATE = SYSTIMESTAMP, EXEC_TIME = SYSTIMESTAMP-START_DATE , STATUS = 'temp_load_failed',TOTAL_REC = L_COUNT,
												ERROR_CODE = V_ERR_CD, ERROR_MSG = V_ERR,FILE_SIZE = G_FILE_SIZE
			WHERE FILE_NAME = P_FILE_NAME AND JOB_NAME = 'PROF_MIG';
			commit;
		  RAISE;
	END PROFILE_XML_STAGE_LOAD;
	
	PROCEDURE PROFILE_MIG_TARGT(P_LAOD_TYPE  VARCHAR2)
    IS
    BEGIN
		
		G_START :=DBMS_UTILITY.GET_TIME;
		
		IF LOWER(P_LAOD_TYPE) not in('initial' ,'delta') or P_LAOD_TYPE is null
		THEN
			
			RAISE_APPLICATION_ERROR(-20103, 'load type should be either initial or delta ' );
			
		END IF;
		
		IF lower(P_LAOD_TYPE) = 'initial'
		THEN
			
			INSERT INTO DPS_USER 
			(
				ID,
				LOGIN,
				AUTO_LOGIN,
				PASSWORD,
				PASSWORD_SALT,
				PASSWORD_KDF,
				--REALM_ID,
				MEMBER,
				FIRST_NAME,
				--MIDDLE_NAME,
				LAST_NAME,
				--USER_TYPE,
				LOCALE,
				LASTACTIVITY_DATE,
				LASTPWDUPDATE,
				GENERATEDPWD,
				REGISTRATION_DATE,
				EMAIL,
				EMAIL_STATUS,
				RECEIVE_EMAIL
				--LAST_EMAILED,
				--GENDER,
				--DATE_OF_BIRTH,
				--SECURITYSTATUS,
				--DESCRIPTION
			)
		    SELECT 
				USER_ID,
				EMAIL,
				0,
				G_PASWD,
				G_SALT_PASWD,
				5,
				--NULL
				0,
				FIRST_NAME,
				--NULL
				LAST_NAME,
				--NULL
				LOCALE,
				LASTACTIVITY_DATE,
				LASTPWDUPDATE,
				0,
				REGISTRATION_DATE,
				EMAIL,
				0,
				1
				--NULL
				--NULL
				--NULL
				--NULL
				--NULL
			FROM TRU_TEMP_PROFILE_USERINFO 
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER load completed');
			--MODIFIED FOR CR36 
			INSERT INTO TRU_DPSX_USER
			(
				ID,
				REWARD_NUMBER,
				--LOYAL_CUST,
				--MIG_PWD_RESET_FLAG,    
				MIG_PROFILE_FLAG 
			)
			SELECT 
				 USER_ID,
				 REWARD_NUMBER, 
				 --NULL
				 --NULL
				 1
			FROM TRU_TEMP_PROFILE_USERINFO 
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_USER') REJECT LIMIT UNLIMITED;
			
			COMMIT;
				--MODIFIED FOR CR36 
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_USER load completed');
			
			INSERT INTO DPS_CONTACT_INFO 
			(
				ID,                   
				USER_ID,
				--PREFIX,
				FIRST_NAME,
				--MIDDLE_NAME,
				LAST_NAME,
				--SUFFIX,
				--JOB_TITLE,
				--COMPANY_NAME,
				ADDRESS1,
				ADDRESS2,
				ADDRESS3,
				CITY,
				STATE,
				POSTAL_CODE,
				COUNTY,
				COUNTRY,
				PHONE_NUMBER
				--FAX_NUMBER
			)
		    SELECT
				ADDRESS_ID,
				USER_ID,
				--NULL
				FIRST_NAME,
				--NULL
				LAST_NAME,
				--NULL
				--NULL
				--NULL
				address1,
				ADDRESS2,
				ADDRESS3,
				city,
				STATE,
				POSTAL_CODE,
				COUNTY,
				country,
				PHONE_NUMBER
				--NULL
			FROM TRU_TEMP_PROFILE_ADDRESS 
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_CONTACT_INFO') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_CONTACT_INFO load completed');
			--MODIFIED FOR CR36
			INSERT INTO TRU_DPSX_CONTACT_INFO
			(
				ID,
				LAST_ACTIVITY,
				STATUS_CD,
				MIG_PROFILE_FLAG 
			)
			SELECT ADDRESS_ID,
				   LAST_ACTIVITY,
				   10,
				   1
			FROM TRU_TEMP_PROFILE_ADDRESS 
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CONTACT_INFO') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			--MODIFIED FOR CR36
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CONTACT_INFO load completed');
	
			INSERT INTO DPS_CREDIT_CARD
			(
				ID,
				CREDIT_CARD_NUMBER,
				CREDIT_CARD_TYPE,
				EXPIRATION_MONTH,
				--EXP_DAY_OF_MONTH,
				EXPIRATION_YEAR,
				BILLING_ADDR
			)
			SELECT 
				CREDIT_CARD_ID,
				CREDIT_CARD_NUMBER,
				CREDIT_CARD_TYPE,
				EXPIRATION_MONTH,
				--NONE
				EXPIRATION_YEAR,
				BILLING_ADDRESS	 
			FROM TRU_TEMP_PROFILE_CREDITCARD 
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_CREDIT_CARD') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			

			
			DBMS_OUTPUT.PUT_LINE ('DPS_CREDIT_CARD load completed');
			
			--MODIFIED FOR CR36
			INSERT INTO TRU_DPSX_CREDIT_CARD
			(
				ID,
				NAME_ON_CARD,
				--CREDIT_CARD_TOKEN,
				--LAST_ACTIVITY
				MIG_PROFILE_FLAG
			)
			SELECT 
				   CREDIT_CARD_ID,
				   NAME_ON_CARD,
				   --NULL,
				   --NULL,
				   1
			FROM  TRU_TEMP_PROFILE_CREDITCARD 
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CREDIT_CARD') REJECT LIMIT UNLIMITED;
				   
			COMMIT;
			--MODIFIED FOR CR36							
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CREDIT_CARD load completed');
			
			INSERT INTO DPS_USER_ADDRESS
			(
				ID,
				--HOME_ADDR_ID,
				BILLING_ADDR_ID,
				SHIPPING_ADDR_ID
			)
			SELECT 
				USER_ID,
				--NULL
				DEFAULT_BILLING_ADDRESS,
				DEFAULT_SHIPPING_ADDRESS
			FROM TRU_TEMP_PROFILE_USERINFO 
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER_ADDRESS') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER_ADDRESS load completed');
		
			INSERT INTO DPS_OTHER_ADDR
			(
				USER_ID,
				TAG,
				ADDRESS_ID
			)
			SELECT
				USER_ID,
				NICK_NAME,
				ADDRESS_ID	
			FROM TRU_TEMP_PROFILE_ADDRESS 
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_OTHER_ADDR') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_OTHER_ADDR load completed');
			
			INSERT INTO DPS_USR_CREDITCARD
			(
				USER_ID,
				TAG,
				CREDIT_CARD_ID
			)
			SELECT 
				USER_ID,
				CREDIT_CARD_TYPE,
				CREDIT_CARD_ID

			FROM TRU_TEMP_PROFILE_CREDITCARD 
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USR_CREDITCARD') REJECT LIMIT UNLIMITED;
			
			COMMIT;

			DBMS_OUTPUT.PUT_LINE ('DPS_USR_CREDITCARD load completed');
		END IF;
		
	    IF lower(P_LAOD_TYPE) = 'delta'
		THEN
			
			MERGE INTO DPS_USER A
			USING
				(
				    SELECT 
						USER_ID,
						EMAIL,
						--0 AUTO_LOGIN,
						--'ENCRIPT PSWD' PASSWORD
						--'ENCRIPT PSWD' PASSWORD_SALT
						--5 PASSWORD_KDF,
						--NULL REALM_ID
						--0 MEMBER,
						FIRST_NAME,
						--NULL MIDDLE_NAME
						LAST_NAME,
						--NULL USER_TYPE
						LOCALE,
						LASTACTIVITY_DATE,
						LASTPWDUPDATE,
						--0 GENERATEDPWD,
						REGISTRATION_DATE
						--EMAIL,
						--0 EMAIL_STATUS,
						--1 RECEIVE_EMAIL
						--NULL LAST_EMAILED
						--NULL GENDER
						--NULL DATE_OF_BIRTH
						--NULL SECURITYSTATUS
						--NULL DESCRIPTION
					FROM TRU_TEMP_PROFILE_USERINFO 
				) B
			ON (A.ID=B.USER_ID)
			WHEN MATCHED THEN
				UPDATE SET A.LOGIN = B.EMAIL ,A.FIRST_NAME = B.FIRST_NAME ,A.LAST_NAME = B.LAST_NAME ,A.LASTPWDUPDATE = B.LASTPWDUPDATE ,A.REGISTRATION_DATE = B.REGISTRATION_DATE ,A.EMAIL = B.EMAIL,A.LOCALE = B.LOCALE,A.LASTACTIVITY_DATE = B.LASTACTIVITY_DATE
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.LOGIN,A.AUTO_LOGIN,A.PASSWORD,A.PASSWORD_SALT,A.PASSWORD_KDF,A.MEMBER,A.FIRST_NAME,A.LAST_NAME,A.LASTPWDUPDATE,A.GENERATEDPWD,A.REGISTRATION_DATE,A.EMAIL,A.EMAIL_STATUS,A.RECEIVE_EMAIL,A.LOCALE,A.LASTACTIVITY_DATE)
				VALUES (B.USER_ID,B.EMAIL,0,G_PASWD,G_SALT_PASWD,5,0,B.FIRST_NAME,B.LAST_NAME,B.LASTPWDUPDATE,0,B.REGISTRATION_DATE,B.EMAIL,0,1,B.LOCALE,B.LASTACTIVITY_DATE)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER') REJECT LIMIT UNLIMITED;
			
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER merge completed');
			
			MERGE INTO TRU_DPSX_USER A
			USING
				(
				    SELECT 
						 USER_ID,
						 REWARD_NUMBER 
						 --NULL LOYAL_CUST
					FROM TRU_TEMP_PROFILE_USERINFO 
				) B
			ON (A.ID=B.USER_ID)
			WHEN MATCHED THEN
				UPDATE SET A.REWARD_NUMBER = B.REWARD_NUMBER 
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.REWARD_NUMBER)
				VALUES (B.USER_ID,B.REWARD_NUMBER)
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_USER') REJECT LIMIT UNLIMITED;

			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_USER merge completed');
			
			
						
			MERGE INTO DPS_CONTACT_INFO A
			USING
				(
				     SELECT
						ADDRESS_ID,
						USER_ID,
						--NULL PREFIX
						FIRST_NAME,
						--NULL MIDDLE_NAME
						LAST_NAME,
						--NULL SUFFIX
						--NULL JOB_TITLE
						--NULL COMPANY_NAME
						address1,
						ADDRESS2,
						ADDRESS3,
						city,
						STATE,
						POSTAL_CODE,
						COUNTY,
						country,
						PHONE_NUMBER
						--NULL FAX_NUMBER
					FROM TRU_TEMP_PROFILE_ADDRESS 
				) B
			ON (A.ID=B.ADDRESS_ID)
			WHEN MATCHED THEN
				UPDATE SET A.USER_ID = B.USER_ID,A.FIRST_NAME = B.FIRST_NAME,A.LAST_NAME = B.LAST_NAME,A.ADDRESS1 = B.ADDRESS1,A.ADDRESS2 = B.ADDRESS2,A.ADDRESS3 = B.ADDRESS3,
						   A.CITY = B.CITY,A.STATE = B.STATE,A.POSTAL_CODE = B.POSTAL_CODE,A.COUNTY = B.COUNTY,A.country = B.country,A.PHONE_NUMBER = B.PHONE_NUMBER
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.USER_ID,A.FIRST_NAME,A.LAST_NAME,A.ADDRESS1,A.ADDRESS2,A.ADDRESS3,A.CITY,A.STATE,A.POSTAL_CODE,A.COUNTY,A.COUNTRY,A.PHONE_NUMBER)
				VALUES (B.ADDRESS_ID,B.USER_ID,B.FIRST_NAME,B.LAST_NAME,B.ADDRESS1,B.ADDRESS2,B.ADDRESS3,B.CITY,B.STATE,B.POSTAL_CODE,B.COUNTY,B.COUNTRY,B.PHONE_NUMBER)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_CONTACT_INFO') REJECT LIMIT UNLIMITED;
			
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_CONTACT_INFO merge completed');
			
			MERGE INTO TRU_DPSX_CONTACT_INFO A
			USING
				(
				     SELECT ADDRESS_ID,
							LAST_ACTIVITY
							--NULL STATUS_CD
					 FROM TRU_TEMP_PROFILE_ADDRESS 
				) B
			ON (A.ID=B.ADDRESS_ID)
			WHEN MATCHED THEN
				UPDATE SET A.LAST_ACTIVITY = B.LAST_ACTIVITY
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.LAST_ACTIVITY,A.STATUS_CD)
				VALUES (B.ADDRESS_ID,B.LAST_ACTIVITY,10)
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CONTACT_INFO') REJECT LIMIT UNLIMITED;

			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CONTACT_INFO merge completed');
	
			MERGE INTO DPS_CREDIT_CARD A
			USING
				(
				     SELECT 
							CREDIT_CARD_ID,
							CREDIT_CARD_NUMBER,
							CREDIT_CARD_TYPE,
							EXPIRATION_MONTH,
							--NONE EXP_DAY_OF_MONTH
							EXPIRATION_YEAR,
							BILLING_ADDRESS	
				    FROM TRU_TEMP_PROFILE_CREDITCARD 
				) B
			ON (A.ID=B.CREDIT_CARD_ID)
			WHEN MATCHED THEN
				UPDATE SET A.CREDIT_CARD_NUMBER = B.CREDIT_CARD_NUMBER,A.CREDIT_CARD_TYPE = B.CREDIT_CARD_TYPE,A.EXPIRATION_MONTH = B.EXPIRATION_MONTH,A.EXPIRATION_YEAR = B.EXPIRATION_YEAR,A.BILLING_ADDR = B.BILLING_ADDRESS
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.CREDIT_CARD_NUMBER,A.CREDIT_CARD_TYPE,A.EXPIRATION_MONTH,A.EXPIRATION_YEAR,A.BILLING_ADDR)
				VALUES (B.CREDIT_CARD_ID,B.CREDIT_CARD_NUMBER,B.CREDIT_CARD_TYPE,B.EXPIRATION_MONTH,B.EXPIRATION_YEAR,B.BILLING_ADDRESS)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_CREDIT_CARD') REJECT LIMIT UNLIMITED;	
			
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_CREDIT_CARD merge completed');
			
			MERGE INTO TRU_DPSX_CREDIT_CARD A
			USING
				(
				    SELECT 
						   CREDIT_CARD_ID,
						   NAME_ON_CARD
						   --NULL CREDIT_CARD_TOKEN,
						   --NULL LAST_ACTIVITY
					FROM  TRU_TEMP_PROFILE_CREDITCARD 
				) B
			ON (A.ID=B.CREDIT_CARD_ID)
			WHEN MATCHED THEN
				UPDATE SET A.NAME_ON_CARD = B.NAME_ON_CARD
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.NAME_ON_CARD)
				VALUES (B.CREDIT_CARD_ID,B.NAME_ON_CARD)
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CREDIT_CARD') REJECT LIMIT UNLIMITED;	

				   
			COMMIT;
										
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CREDIT_CARD merge completed');
			
			MERGE INTO DPS_USER_ADDRESS A
			USING
				(
				    SELECT 
						USER_ID,
						--NULL HOME_ADDR_ID
						DEFAULT_BILLING_ADDRESS,
						DEFAULT_SHIPPING_ADDRESS
					FROM TRU_TEMP_PROFILE_USERINFO 
				) B
			ON (A.ID=B.USER_ID)
			WHEN MATCHED THEN
				UPDATE SET A.BILLING_ADDR_ID = B.DEFAULT_BILLING_ADDRESS,A.SHIPPING_ADDR_ID = B.DEFAULT_SHIPPING_ADDRESS
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.BILLING_ADDR_ID,A.SHIPPING_ADDR_ID)
				VALUES (B.USER_ID,B.DEFAULT_BILLING_ADDRESS,B.DEFAULT_SHIPPING_ADDRESS)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER_ADDRESS') REJECT LIMIT UNLIMITED;	
			
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER_ADDRESS merge completed');
			
						
			MERGE INTO DPS_OTHER_ADDR A
			USING
				(
				    SELECT
						USER_ID,
						NICK_NAME,
						ADDRESS_ID	
					FROM TRU_TEMP_PROFILE_ADDRESS 
				) B
			ON (A.USER_ID=B.USER_ID AND A.TAG = B.NICK_NAME)
			WHEN MATCHED THEN
				UPDATE SET A.ADDRESS_ID = B.ADDRESS_ID
			WHEN NOT MATCHED THEN
				INSERT (A.USER_ID,A.TAG,A.ADDRESS_ID)
				VALUES (B.USER_ID,B.NICK_NAME,B.ADDRESS_ID)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_OTHER_ADDR') REJECT LIMIT UNLIMITED;	

			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_OTHER_ADDR merge completed');
			
									
			MERGE INTO DPS_USR_CREDITCARD A
			USING
				(
				    SELECT 
						USER_ID,
						CREDIT_CARD_TYPE,
						CREDIT_CARD_ID
					FROM TRU_TEMP_PROFILE_CREDITCARD 
				) B
			ON (A.USER_ID=B.USER_ID AND A.TAG = B.CREDIT_CARD_TYPE)
			WHEN MATCHED THEN
				UPDATE SET A.CREDIT_CARD_ID = B.CREDIT_CARD_ID
			WHEN NOT MATCHED THEN
				INSERT (A.USER_ID,A.TAG,A.CREDIT_CARD_ID)
				VALUES (B.USER_ID,B.CREDIT_CARD_TYPE,B.CREDIT_CARD_ID)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USR_CREDITCARD') REJECT LIMIT UNLIMITED;	
			
			
			COMMIT;

			DBMS_OUTPUT.PUT_LINE ('DPS_USR_CREDITCARD merge completed');
			

		END IF;
		
		DBMS_OUTPUT.PUT_LINE ('All Target table load completed');
		
		G_END :=DBMS_UTILITY.GET_TIME;
        DBMS_OUTPUT.PUT_LINE('PROFILE_MIG_TARGT: Time Taken to execute ' || (G_END-G_START)/100||' '||'seconds');
		
	  EXCEPTION WHEN OTHERS THEN
		RAISE;
		
	END PROFILE_MIG_TARGT;


	PROCEDURE PROFILE_MIG_TARGT_BATCH(P_LAOD_TYPE  VARCHAR2,P_COMMIT_INTRVL PLS_INTEGER)
    IS
		
		type l_rcrd is record 
			(
				USER_ID                 TRU_TEMP_PROFILE_USERINFO.USER_ID%type, 
				EMAIL                   TRU_TEMP_PROFILE_USERINFO.EMAIL%type,
				FIRST_NAME              TRU_TEMP_PROFILE_USERINFO.FIRST_NAME%type,
				LAST_NAME               TRU_TEMP_PROFILE_USERINFO.LAST_NAME%type,
				LOCALE					        TRU_TEMP_PROFILE_USERINFO.LOCALE%type,
				LASTACTIVITY_DATE		TRU_TEMP_PROFILE_USERINFO.LASTACTIVITY_DATE%type,
				LASTPWDUPDATE           TRU_TEMP_PROFILE_USERINFO.LASTPWDUPDATE%type,
				REGISTRATION_DATE       TRU_TEMP_PROFILE_USERINFO.REGISTRATION_DATE%type,
				REWARD_NUMBER 			TRU_TEMP_PROFILE_USERINFO.REWARD_NUMBER%type,
				DEFAULT_BILLING_ADDRESS	TRU_TEMP_PROFILE_USERINFO.DEFAULT_BILLING_ADDRESS%type,
				DEFAULT_SHIPPING_ADDRESS TRU_TEMP_PROFILE_USERINFO.DEFAULT_SHIPPING_ADDRESS%type
				
			);
		
		type l_rcrd1 is record 
			(
				ADDRESS_ID              TRU_TEMP_PROFILE_ADDRESS.ADDRESS_ID%type, 
				USER_ID                 TRU_TEMP_PROFILE_ADDRESS.USER_ID%type,
				FIRST_NAME              TRU_TEMP_PROFILE_ADDRESS.FIRST_NAME%type,
				LAST_NAME               TRU_TEMP_PROFILE_ADDRESS.LAST_NAME%type,
				address1           		TRU_TEMP_PROFILE_ADDRESS.address1%type,
				address2       			TRU_TEMP_PROFILE_ADDRESS.address2%type,
				address3 				TRU_TEMP_PROFILE_ADDRESS.address3%type,
				city					TRU_TEMP_PROFILE_ADDRESS.city%type,
				STATE					TRU_TEMP_PROFILE_ADDRESS.STATE%type,
				POSTAL_CODE				TRU_TEMP_PROFILE_ADDRESS.POSTAL_CODE%type,
				COUNTY					TRU_TEMP_PROFILE_ADDRESS.COUNTY%type,
				country					TRU_TEMP_PROFILE_ADDRESS.country%type,
				PHONE_NUMBER			TRU_TEMP_PROFILE_ADDRESS.PHONE_NUMBER%type,
				NICK_NAME				TRU_TEMP_PROFILE_ADDRESS.NICK_NAME%type,
				LAST_ACTIVITY		    TRU_TEMP_PROFILE_ADDRESS.LAST_ACTIVITY%type
			);
			
		type l_rcrd2 is record 
			(
				CREDIT_CARD_ID			TRU_TEMP_PROFILE_CREDITCARD.CREDIT_CARD_ID%TYPE,
				USER_ID             	TRU_TEMP_PROFILE_CREDITCARD.USER_ID%TYPE,
				CREDIT_CARD_NUMBER		TRU_TEMP_PROFILE_CREDITCARD.CREDIT_CARD_NUMBER%type,
				CREDIT_CARD_TYPE		TRU_TEMP_PROFILE_CREDITCARD.CREDIT_CARD_TYPE%type,
				EXPIRATION_MONTH		TRU_TEMP_PROFILE_CREDITCARD.EXPIRATION_MONTH%type,
				EXPIRATION_YEAR			TRU_TEMP_PROFILE_CREDITCARD.EXPIRATION_YEAR%type,
				BILLING_ADDRESS			TRU_TEMP_PROFILE_CREDITCARD.BILLING_ADDRESS%TYPE,
        NAME_ON_CARD        TRU_TEMP_PROFILE_CREDITCARD.NAME_ON_CARD%TYPE
			);
				
		type USER_COL  is table of l_rcrd index by pls_integer; 
		L_USER_INFO USER_COL;
		
		type ADDRS_COL  is table of l_rcrd1 index by pls_integer; 
		L_USER_ADDRS ADDRS_COL;
		
		type CC_COL  is table of l_rcrd2 index by pls_integer; 
		L_USER_CC CC_COL;
		
		ROWS PLS_INTEGER;
		
		L_DYN_CUR SYS_REFCURSOR;
		
		
    BEGIN
		
		G_START :=DBMS_UTILITY.GET_TIME;
		
		IF P_COMMIT_INTRVL IS NULL OR P_COMMIT_INTRVL < 10000
		THEN
			
			RAISE_APPLICATION_ERROR(-20102, 'commit inerval value should be greater than or equal to 10000' );
			
		END IF;
		
		ROWS := P_COMMIT_INTRVL;
		
		IF LOWER(P_LAOD_TYPE) not in('initial','delta') or  P_LAOD_TYPE is null
		THEN
			
			RAISE_APPLICATION_ERROR(-20103, 'load type should be either initial or delta ' );
			
		END IF;
		
		IF lower(P_LAOD_TYPE) = 'initial'
		THEN
			
			 OPEN L_DYN_CUR FOR  SELECT 
												USER_ID,
												EMAIL,
												--0 AUTO_LOGIN,
												--ENCRIPT PSWD PASSWORD
												--ENCRIPT PSWD PASSWORD_SALT
												--5 PASSWORD_KDF,
												--NULL REALM_ID
												--0 MEMBER,
												FIRST_NAME,
												--NULL MIDDLE_NAME
												LAST_NAME,
												--NULL USER_TYPE
												LOCALE,
												LASTACTIVITY_DATE,
												LASTPWDUPDATE,
												--0 GENERATEDPWD,
												REGISTRATION_DATE,
												--EMAIL,
												--0 EMAIL_STATUS,
												--1 RECEIVE_EMAIL
												--NULL LAST_EMAILED
												--NULL GENDER
												--NULL DATE_OF_BIRTH
												--NULL SECURITYSTATUS
												--NULL DESCRIPTION
												REWARD_NUMBER,
												DEFAULT_BILLING_ADDRESS,
												DEFAULT_SHIPPING_ADDRESS
											FROM TRU_TEMP_PROFILE_USERINFO 
                    ;

             LOOP
                  FETCH L_DYN_CUR BULK COLLECT INTO L_USER_INFO LIMIT ROWS;
                  EXIT WHEN L_USER_INFO.count=0;
                  FORALL I IN L_USER_INFO.FIRST .. L_USER_INFO.LAST
					  INSERT INTO DPS_USER 
						(ID,LOGIN,AUTO_LOGIN,PASSWORD,PASSWORD_SALT,PASSWORD_KDF,MEMBER,FIRST_NAME,LAST_NAME,LASTPWDUPDATE,GENERATEDPWD,REGISTRATION_DATE,EMAIL,EMAIL_STATUS,RECEIVE_EMAIL,
							LOCALE,LASTACTIVITY_DATE
						)
					  values
						(L_USER_INFO(i).USER_ID,L_USER_INFO(i).EMAIL,0,G_PASWD,G_SALT_PASWD,5,0,L_USER_INFO(i).FIRST_NAME,L_USER_INFO(i).LAST_NAME,L_USER_INFO(i).LASTPWDUPDATE,0,L_USER_INFO(i).REGISTRATION_DATE,L_USER_INFO(i).EMAIL,0,1,
						 L_USER_INFO(i).LOCALE,L_USER_INFO(i).LASTACTIVITY_DATE
						)
					  LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER') REJECT LIMIT UNLIMITED;
					
				  
                  FORALL I IN L_USER_INFO.FIRST .. L_USER_INFO.LAST
					  INSERT INTO TRU_DPSX_USER 
						(ID,REWARD_NUMBER)
					  values
						(L_USER_INFO(i).USER_ID,L_USER_INFO(i).REWARD_NUMBER)
					  LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_USER') REJECT LIMIT UNLIMITED;
					  
				  FORALL I IN L_USER_INFO.FIRST .. L_USER_INFO.LAST 			
					INSERT INTO DPS_USER_ADDRESS
					(
						ID,
						--HOME_ADDR_ID,
						BILLING_ADDR_ID,
						SHIPPING_ADDR_ID
					)
					VALUES
					(
						L_USER_INFO(i).USER_ID,
						--NULL
						L_USER_INFO(I).DEFAULT_BILLING_ADDRESS,
						L_USER_INFO(i).DEFAULT_SHIPPING_ADDRESS
					)
					LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER_ADDRESS') REJECT LIMIT UNLIMITED;
         
                COMMIT;
            END LOOP;
			
			
			
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER load completed');
			
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_USER load completed');
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER_ADDRESS load completed');
			
			
			 OPEN L_DYN_CUR FOR 	SELECT
										ADDRESS_ID,
										USER_ID,
										--NULL PREFIX
										FIRST_NAME,
										--NULL MIDDLE_NAME
										LAST_NAME,
										--NULL SUFFIX
										--NULL JOB_TITLE
										--NULL COMPANY_NAME
										address1,
										ADDRESS2,
										ADDRESS3,
										city,
										STATE,
										POSTAL_CODE,
										COUNTY,
										country,
										PHONE_NUMBER,
										NICK_NAME,
										LAST_ACTIVITY
										--NULL FAX_NUMBER
									FROM TRU_TEMP_PROFILE_ADDRESS;

             LOOP
                  FETCH L_DYN_CUR BULK COLLECT INTO L_USER_ADDRS LIMIT ROWS;
                  EXIT WHEN L_USER_ADDRS.count=0;
                  FORALL I IN L_USER_ADDRS.FIRST .. L_USER_ADDRS.LAST
					INSERT INTO  DPS_CONTACT_INFO
						(ID,USER_ID,FIRST_NAME,LAST_NAME,ADDRESS1,ADDRESS2,ADDRESS3,CITY,STATE,POSTAL_CODE,COUNTY,COUNTRY,PHONE_NUMBER)
					VALUES 
						(L_USER_ADDRS(I).ADDRESS_ID,L_USER_ADDRS(I).USER_ID,L_USER_ADDRS(I).FIRST_NAME,L_USER_ADDRS(I).LAST_NAME,L_USER_ADDRS(I).ADDRESS1,L_USER_ADDRS(I).ADDRESS2,
						 L_USER_ADDRS(I).ADDRESS3,L_USER_ADDRS(I).CITY,L_USER_ADDRS(I).STATE,L_USER_ADDRS(I).POSTAL_CODE,L_USER_ADDRS(I).COUNTY,L_USER_ADDRS(I).COUNTRY,L_USER_ADDRS(I).PHONE_NUMBER)
					LOG ERRORS INTO DML_ERROR_LOG ('DPS_CONTACT_INFO') REJECT LIMIT UNLIMITED;
					
				  
                  FORALL I IN L_USER_ADDRS.FIRST .. L_USER_ADDRS.LAST
					INSERT INTO TRU_DPSX_CONTACT_INFO
						(
							ID,
							LAST_ACTIVITY,
							STATUS_CD
						)
					VALUES
						(L_USER_ADDRS(I).ADDRESS_ID,L_USER_ADDRS(I).LAST_ACTIVITY,10)
					LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CONTACT_INFO') REJECT LIMIT UNLIMITED;
				  
				  FORALL I IN L_USER_ADDRS.FIRST .. L_USER_ADDRS.LAST
							
					INSERT INTO DPS_OTHER_ADDR
						(
							USER_ID,
							TAG,
							ADDRESS_ID
						)
					VALUES
						(
							L_USER_ADDRS(I).USER_ID,
							L_USER_ADDRS(I).NICK_NAME,
							L_USER_ADDRS(I).ADDRESS_ID
						)
					LOG ERRORS INTO DML_ERROR_LOG ('DPS_OTHER_ADDR') REJECT LIMIT UNLIMITED;
         
                COMMIT;
            END LOOP;
			
			
			
			DBMS_OUTPUT.PUT_LINE ('DPS_CONTACT_INFO load completed');
			
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CONTACT_INFO load completed');
			
			DBMS_OUTPUT.PUT_LINE ('DPS_OTHER_ADDR load completed');
			
			
			 OPEN L_DYN_CUR FOR SELECT 
									CREDIT_CARD_ID,
                  USER_ID,
									CREDIT_CARD_NUMBER,
									CREDIT_CARD_TYPE,
									EXPIRATION_MONTH,
									--NONE EXP_DAY_OF_MONTH
									EXPIRATION_YEAR,
									BILLING_ADDRESS,
									NAME_ON_CARD
								FROM TRU_TEMP_PROFILE_CREDITCARD ;

             LOOP
                  FETCH L_DYN_CUR BULK COLLECT INTO L_USER_CC LIMIT ROWS;
                  EXIT WHEN L_USER_CC.count=0;
                  FORALL I IN L_USER_CC.FIRST .. L_USER_CC.LAST
						INSERT INTO DPS_CREDIT_CARD
							(
								ID,
								CREDIT_CARD_NUMBER,
								CREDIT_CARD_TYPE,
								EXPIRATION_MONTH,
								EXPIRATION_YEAR,
								BILLING_ADDR
							)
						VALUES
							( 
								L_USER_CC(I).CREDIT_CARD_ID,
								L_USER_CC(I).CREDIT_CARD_NUMBER,
								L_USER_CC(I).CREDIT_CARD_TYPE,
								L_USER_CC(I).EXPIRATION_MONTH,
								L_USER_CC(I).EXPIRATION_YEAR,
								L_USER_CC(I).BILLING_ADDRESS	
							)
						LOG ERRORS INTO DML_ERROR_LOG ('DPS_CREDIT_CARD') REJECT LIMIT UNLIMITED;
					
				  
                  FORALL I IN L_USER_CC.FIRST .. L_USER_CC.LAST
								
						INSERT INTO TRU_DPSX_CREDIT_CARD
						   (
								ID,
								NAME_ON_CARD
								--CREDIT_CARD_TOKEN,
								--LAST_ACTIVITY
							)
						VALUES
							(
							   L_USER_CC(I).CREDIT_CARD_ID,
							   L_USER_CC(I).NAME_ON_CARD
							   --NULL,
							   --NULL
							)
						LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CREDIT_CARD') REJECT LIMIT UNLIMITED;
				 
				  FORALL I IN L_USER_CC.FIRST .. L_USER_CC.LAST

						INSERT INTO DPS_USR_CREDITCARD
						(
							USER_ID,
							TAG,
							CREDIT_CARD_ID
						)
						VALUES
						(
							L_USER_CC(I).USER_ID,
							L_USER_CC(I).CREDIT_CARD_TYPE,
							L_USER_CC(I).CREDIT_CARD_ID
						)
						LOG ERRORS INTO DML_ERROR_LOG ('DPS_USR_CREDITCARD') REJECT LIMIT UNLIMITED;

         
                COMMIT;
            END LOOP;
	

			
			DBMS_OUTPUT.PUT_LINE ('DPS_CREDIT_CARD load completed');
										
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CREDIT_CARD load completed');

			DBMS_OUTPUT.PUT_LINE ('DPS_USR_CREDITCARD load completed');
			
		END IF;
		
	    IF lower(P_LAOD_TYPE) = 'delta'
		THEN
					
			RAISE_APPLICATION_ERROR(-20104, 'Run delta load using TRU_PROFILE_MIG_PKG.PROFILE_MIG_TARGT procedure ' );

		END IF;
		
		DBMS_OUTPUT.PUT_LINE ('All Target table load completed');
		
		G_END :=DBMS_UTILITY.GET_TIME;
        DBMS_OUTPUT.PUT_LINE('PROFILE_MIG_TARGT_BATCH: Time Taken to execute ' || (G_END-G_START)/100||' '||'seconds');
		
	  EXCEPTION WHEN OTHERS THEN
		RAISE;
		
	end PROFILE_MIG_TARGT_BATCH;
	
	FUNCTION  GET_MAPPED_STATE_CODE(P_STATE_CODE IN VARCHAR2) RETURN VARCHAR2
	IS
		V_MAPPED_VALUE VARCHAR2(255) := 'N/A';
		BEGIN
		FOR I IN ( select * from TRU_TEMP_PROFILE_STATE )
		LOOP
			IF   P_STATE_CODE = I.STATE_CODE
			THEN
			  V_MAPPED_VALUE :=  I.VALID_STATE_CODE;
			EXIT;
			end if;
		END LOOP;
		RETURN V_MAPPED_VALUE;
	EXCEPTION WHEN OTHERS THEN
	RETURN NULL;
	END GET_MAPPED_STATE_CODE;
	
	FUNCTION  GET_MAPPED_COUNTRY_CODE(P_COUNTRY_CODE IN VARCHAR2) RETURN VARCHAR2
	IS
		V_MAPPED_VALUE VARCHAR2(255) := 'N/A';
		BEGIN
		FOR I IN ( select * from TRU_TEMP_PROFILE_COUNTRY )
		LOOP
			IF   P_COUNTRY_CODE = I.COUNTRY_CODE
			THEN
			  V_MAPPED_VALUE := I.VALID_COUNTRY_CODE;
			EXIT;
			
			end if;
		END LOOP;
		RETURN V_MAPPED_VALUE;
	EXCEPTION WHEN OTHERS THEN
	RETURN NULL;
	END GET_MAPPED_COUNTRY_CODE; 
	
	PROCEDURE PROFILE_STAGE_COUNTRY_LOAD (P_DIR_NAME VARCHAR2,P_FILE_NAME VARCHAR2,P_TRUNCATE VARCHAR2)
    IS
	  L_FILE_EXISTS VARCHAR2(1000);
	  L_FILE_NAME VARCHAR2(4000);
	  l_ex BOOLEAN;
	  l_flen NUMBER;
	  l_bsize NUMBER;
	  V_ERR VARCHAR2(255);
	  V_ERR_CD VARCHAR2(255);
	  FILE_HANDLE           UTL_FILE.FILE_TYPE;
	  V_COUNTRY_CODE TRU_TEMP_PROFILE_COUNTRY.COUNTRY_CODE%TYPE;
	  V_VALID_COUNTRY_CODE TRU_TEMP_PROFILE_COUNTRY.VALID_COUNTRY_CODE%TYPE;
	  V_STRING VARCHAR2(1000);
	  V_CNT NUMBER;
    BEGIN

		G_START :=DBMS_UTILITY.GET_TIME;
		
		--Checking XML file name exists or not in the directory
		L_FILE_EXISTS := TRU_PROFILE_MIG_PKG.XML_FILEEXISTS(P_DIRNAME => P_DIR_NAME,P_FILENAME => P_FILE_NAME);
		
		IF L_FILE_EXISTS <> 'EXISTS'
		THEN
			RAISE_APPLICATION_ERROR(-20101, L_FILE_EXISTS );
		END IF;
		
		L_FILE_NAME := P_FILE_NAME;
		
		IF lower(P_TRUNCATE) not in('y','n') or P_TRUNCATE is null
		THEN
			RAISE_APPLICATION_ERROR(-20104, 'P_TRUNCATE value should be either y or n' );
		END IF;
		
		IF lower(P_TRUNCATE) = 'y'
		THEN
			--Clearing all records in profile migration staging tables
			DELETE FROM TRU_TEMP_MIG_FILE_STATUS WHERE  JOB_NAME = 'PROF_COUNTRY';
			EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TEMP_PROFILE_COUNTRY';

		ELSIF lower(P_TRUNCATE) = 'n'
		THEN
			FOR V_FNAM IN (SELECT FILE_NAME||',' FILE_NAME FROM TRU_TEMP_MIG_FILE_STATUS WHERE JOB_NAME = 'PROF_COUNTRY')
			LOOP
				SELECT REPLACE(L_FILE_NAME,V_FNAM.FILE_NAME,NULL) INTO L_FILE_NAME FROM DUAL;
				DBMS_OUTPUT.PUT_LINE('FILE_NAME TRIM : '||L_FILE_NAME); 
			END LOOP;
		END IF;
		

		FOR I IN (SELECT EXTRACT (VALUE (d), '//row/text()').getstringval() FILE_NAME 
					   FROM
						   (select xmltype (   '<rows><row>'
							|| REPLACE (L_FILE_NAME, ',', '</row><row>')
							|| '</row></rows>'
						   ) AS xmlval
							from DUAL) X,
						table (XMLSEQUENCE (extract (X.XMLVAL, '/rows/row')))D
				 )
		LOOP
			
			 --Gettin file zize in MB
			 UTL_FILE.FGETATTR(UPPER(P_DIR_NAME),I.FILE_NAME, L_EX, L_FLEN, L_BSIZE);
			 G_FILE_SIZE := round(L_FLEN/1000000,2);
			 
			BEGIN
				 INSERT INTO TRU_TEMP_MIG_FILE_STATUS(FILE_NAME,FILE_SIZE,JOB_NAME,START_DATE) VALUES(I.FILE_NAME,G_FILE_SIZE,'PROF_COUNTRY',SYSTIMESTAMP);
				 V_CNT := 0;
				 
				 insert into TRU_TEMP_PROFILE_COUNTRY (COUNTRY_CODE,VALID_COUNTRY_CODE) 
						 SELECT COUNTRY_CODE,VALID_COUNTRY_CODE
						 FROM
							(SELECT nvl(STRING_VAL,number_val) COUNTRY_CODE,TO_NUMBER(REGEXP_SUBSTR(CELL, '[[:digit:]]+$')) CELL_VALUE FROM TABLE( AS_READ_XLSX.READ( AS_READ_XLSX.FILE2BLOB( P_DIR_NAME, I.FILE_NAME ) ) )
							WHERE CELL LIKE '%A%') A_CELL
							FULL OUTER JOIN
							(SELECT nvl(STRING_VAL,number_val) VALID_COUNTRY_CODE,TO_NUMBER(REGEXP_SUBSTR(CELL, '[[:digit:]]+$')) CELL_VALUE FROM TABLE( AS_READ_XLSX.READ( AS_READ_XLSX.FILE2BLOB( P_DIR_NAME, I.FILE_NAME ) ) )
							WHERE CELL LIKE '%B%') B_CELL
							ON(A_CELL.CELL_VALUE = B_CELL.CELL_VALUE)
							WHERE A_CELL.CELL_VALUE <>1 ORDER BY A_CELL.CELL_VALUE;
						 V_CNT := sql%rowcount;
				 
				 UPDATE TRU_TEMP_MIG_FILE_STATUS SET STATUS='temp_country_load_completed',end_date = systimestamp,exec_time = start_date -systimestamp, TOTAL_REC = V_CNT WHERE FILE_NAME = I.FILE_NAME AND JOB_NAME='PROF_COUNTRY';
				 COMMIT;
			EXCEPTION WHEN OTHERS THEN
				V_ERR := SUBSTR(SQLERRM, 1, 200);
				V_ERR_CD := SQLCODE;
				UPDATE TRU_TEMP_MIG_FILE_STATUS SET STATUS='temp_country_load_failed',ERROR_CODE=V_ERR_CD,ERROR_MSG=V_ERR  WHERE FILE_NAME = I.FILE_NAME AND JOB_NAME='PROF_COUNTRY';
				commit;
				GOTO LOOP1;
			END;
			
            <<LOOP1>>
			null;
			
		END LOOP;	
        
		G_END :=DBMS_UTILITY.GET_TIME;
		DBMS_OUTPUT.PUT_LINE('PROFILE_STAGE_COUNTRY_LOAD: Time Taken to execute ' || (G_END-G_START)/100||' '||'seconds');
		
		EXCEPTION WHEN OTHERS THEN
		RAISE;
		
	END PROFILE_STAGE_COUNTRY_LOAD;
	
	PROCEDURE PROFILE_STAGE_STATE_LOAD (P_DIR_NAME VARCHAR2,P_FILE_NAME VARCHAR2,P_TRUNCATE VARCHAR2)
    IS
	  L_FILE_EXISTS VARCHAR2(1000);
	  L_FILE_NAME VARCHAR2(4000);
	  l_ex BOOLEAN;
	  l_flen NUMBER;
	  l_bsize NUMBER;
	  V_ERR VARCHAR2(255);
	  V_ERR_CD VARCHAR2(255);
	  FILE_HANDLE           UTL_FILE.FILE_TYPE;
	  V_STATE_CODE TRU_TEMP_PROFILE_STATE.STATE_CODE%TYPE;
	  V_VALID_STATE_CODE TRU_TEMP_PROFILE_STATE.VALID_STATE_CODE%TYPE;
	  V_STRING VARCHAR2(1000);
	  V_CNT NUMBER;
    BEGIN

		G_START :=DBMS_UTILITY.GET_TIME;
		
		--Checking XML file name exists or not in the directory
		L_FILE_EXISTS := TRU_PROFILE_MIG_PKG.XML_FILEEXISTS(P_DIRNAME => P_DIR_NAME,P_FILENAME => P_FILE_NAME);
		
		IF L_FILE_EXISTS <> 'EXISTS'
		THEN
			RAISE_APPLICATION_ERROR(-20101, L_FILE_EXISTS );
		END IF;
		
		L_FILE_NAME := P_FILE_NAME;
		
		IF lower(P_TRUNCATE) not in('y','n') or P_TRUNCATE is null
		THEN
			RAISE_APPLICATION_ERROR(-20104, 'P_TRUNCATE value should be either y or n' );
		END IF;
		
		IF lower(P_TRUNCATE) = 'y'
		THEN
			--Clearing all records in profile migration staging tables
			DELETE FROM TRU_TEMP_MIG_FILE_STATUS WHERE  JOB_NAME = 'PROF_STATE';
			EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TEMP_PROFILE_STATE';

		ELSIF lower(P_TRUNCATE) = 'n'
		THEN
			FOR V_FNAM IN (SELECT FILE_NAME||',' FILE_NAME FROM TRU_TEMP_MIG_FILE_STATUS WHERE JOB_NAME = 'PROF_STATE')
			LOOP
				SELECT REPLACE(L_FILE_NAME,V_FNAM.FILE_NAME,NULL) INTO L_FILE_NAME FROM DUAL;
				DBMS_OUTPUT.PUT_LINE('FILE_NAME TRIM : '||L_FILE_NAME); 
			END LOOP;
		END IF;
		

		FOR I IN (SELECT EXTRACT (VALUE (d), '//row/text()').getstringval() FILE_NAME 
					   FROM
						   (select xmltype (   '<rows><row>'
							|| REPLACE (L_FILE_NAME, ',', '</row><row>')
							|| '</row></rows>'
						   ) AS xmlval
							from DUAL) X,
						table (XMLSEQUENCE (extract (X.XMLVAL, '/rows/row')))D
				 )
		LOOP
			
			 --Gettin file zize in MB
			 UTL_FILE.FGETATTR(UPPER(P_DIR_NAME),I.FILE_NAME, L_EX, L_FLEN, L_BSIZE);
			 G_FILE_SIZE := round(L_FLEN/1000000,2);
			 
			BEGIN
				 INSERT INTO TRU_TEMP_MIG_FILE_STATUS(FILE_NAME,FILE_SIZE,JOB_NAME,START_DATE) VALUES(I.FILE_NAME,G_FILE_SIZE,'PROF_STATE',SYSTIMESTAMP);
				 V_CNT := 0;
					insert into TRU_TEMP_PROFILE_STATE (STATE_CODE,VALID_STATE_CODE) 
						 SELECT STATE_CODE,VALID_STATE_CODE
						 FROM
							(SELECT nvl(STRING_VAL,number_val) STATE_CODE,TO_NUMBER(REGEXP_SUBSTR(CELL, '[[:digit:]]+$')) CELL_VALUE FROM TABLE( AS_READ_XLSX.READ( AS_READ_XLSX.FILE2BLOB( P_DIR_NAME, I.FILE_NAME ) ) )
							WHERE CELL LIKE '%A%') A_CELL
							FULL OUTER JOIN
							(SELECT nvl(STRING_VAL,number_val) VALID_STATE_CODE,TO_NUMBER(REGEXP_SUBSTR(CELL, '[[:digit:]]+$')) CELL_VALUE FROM TABLE( AS_READ_XLSX.READ( AS_READ_XLSX.FILE2BLOB( P_DIR_NAME, I.FILE_NAME ) ) )
							WHERE CELL LIKE '%B%') B_CELL
							ON(A_CELL.CELL_VALUE = B_CELL.CELL_VALUE)
							WHERE A_CELL.CELL_VALUE <>1 ORDER BY A_CELL.CELL_VALUE;
						 V_CNT := sql%rowcount;
				 UPDATE TRU_TEMP_MIG_FILE_STATUS SET STATUS='temp_state_load_completed',end_date = systimestamp,exec_time = start_date -systimestamp, TOTAL_REC = V_CNT WHERE FILE_NAME = I.FILE_NAME AND JOB_NAME='PROF_STATE';
				 COMMIT;
			EXCEPTION WHEN OTHERS THEN
				V_ERR := SUBSTR(SQLERRM, 1, 200);
				V_ERR_CD := SQLCODE;
				UPDATE TRU_TEMP_MIG_FILE_STATUS SET STATUS='temp_state_load_failed',ERROR_CODE=V_ERR_CD,ERROR_MSG=V_ERR  WHERE FILE_NAME = I.FILE_NAME AND JOB_NAME='PROF_STATE';
				commit;
				GOTO LOOP1;
			END;
			
            <<LOOP1>>
			null;
			
		END LOOP;	
        
		G_END :=DBMS_UTILITY.GET_TIME;
		DBMS_OUTPUT.PUT_LINE('PROFILE_STAGE_STATE_LOAD: Time Taken to execute ' || (G_END-G_START)/100||' '||'seconds');
		
		EXCEPTION WHEN OTHERS THEN
		RAISE;
		
	END PROFILE_STAGE_STATE_LOAD;
	
	
END TRU_PROFILE_MIG_PKG;
/