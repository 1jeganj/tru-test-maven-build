-- Start - Updated column size for abandon cart threshhold 

ALTER TABLE TRU_SITE_CONFIGURATION DROP COLUMN ABANDON_CART_THRESHOLD;
ALTER TABLE TRU_SITE_CONFIGURATION ADD ABANDON_CART_THRESHOLD NUMBER(8) DEFAULT 3600;

-- End - Updated column size for abandon cart threshhold