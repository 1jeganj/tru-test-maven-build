----------CATS_------------
--DROP PROCEDURE
DROP PROCEDURE PR_QUALIF_PROD_PRDS;


create table tru_qualifying_sku (
	promotion_id	varchar2(40)	not null,
	sequence_num	integer	not null,
	sku_id	varchar2(40)	not null
,constraint tru_qualifying_sku_p primary key (promotion_id,sequence_num)
,constraint tru_qualifyingsku_d_f foreign key (promotion_id) references dcs_promotion (promotion_id)
,constraint tru_qualifying_sku_d_f foreign key (sku_id) references dcs_sku (sku_id));

create table tru_excluded_sku (
	promotion_id	varchar2(40)	not null,
	sequence_num	integer	not null,
	sku_id	varchar2(40)	not null
,constraint tru_excluded_sku_p primary key(promotion_id,sequence_num)
,constraint tru_excludedsku_d_f foreign key (promotion_id) references dcs_promotion (promotion_id)
,constraint tru_excluded_sku_d_f foreign key (sku_id) references dcs_sku (sku_id));

create table tru_excluded_prod (
	promotion_id	varchar2(40)	not null,
	sequence_num	integer	not null,
	product_id	varchar2(40)	not null
,constraint tru_excluded_prod_p primary key (promotion_id,sequence_num)
,constraint tru_excludedprod_d_f foreign key (promotion_id) references dcs_promotion (promotion_id)
,constraint tru_excluded_prod_d_f foreign key (product_id) references dcs_product (product_id));

create table tru_excluded_category (
	promotion_id	varchar2(40)	not null,
	sequence_num	integer	not null,
	category_id	varchar2(40)	not null
,constraint tru_excluded_category_p primary key (promotion_id,sequence_num)
,constraint tru_excludedcategory_d_f foreign key (promotion_id) references dcs_promotion (promotion_id)
,constraint tru_excluded_category_d_f foreign key (category_id) references dcs_category (category_id));


create table tru_sku_qualified_promos (
	sku_id	varchar2(40)	not null,
	sequence_num	integer	not null,
	promotion_data	varchar2(1000)	not null
,constraint tru_sku_qualified_promos_p primary key (sku_id,sequence_num)
,constraint tru_sku_qualified_promos_d_f foreign key (sku_id) references dcs_sku (sku_id));

--Creating indexes for foreign key
CREATE INDEX tru_excluded_category_idx ON tru_excluded_category(category_id);
CREATE INDEX tru_qualifying_category_idx ON tru_qualifying_category(category_id);
CREATE INDEX tru_excluded_prod_idx ON tru_excluded_prod(product_id);
CREATE INDEX tru_qualifying_prod_idx ON tru_qualifying_prod(product_id);
CREATE INDEX tru_excluded_sku_idx ON tru_excluded_sku(sku_id);
CREATE INDEX tru_qualifying_sku_idx ON tru_qualifying_sku(sku_id);

--CREATE NEW PROCEDURE
create or replace
procedure TRU_QUALIF_SKU_PROMOS_PRC

/*
***************************************************************TRU_QUALIF_SKU_PROMOS_PRC**********************************************************************************************************
**    i.  Purpose 	
**																																								
**		  This procedure populates TRU_SKU_QUALIFIED_PROMOS table

**    ii. Procedures 
**
**			i.TRU_QUALIF_SKU_PROMOS_PRC
**			---------------------------------------
**				It populates TRU_SKU_QUALIFIED_PROMOS  by using the tables (tru_qualifying_category,tru_qualifying_prod,tru_qualifying_sku) and
				exclude the skus which are  present in excluded table (tru_excluded_category,tru_excluded_prod,tru_excluded_sku).
				 
**			Sample example how sku_id  mapping using category_id
			
**			Assume One  category contains 2 level of child category and the data like below
**			Parent category  :
					 
**											 par_cat         prom1   prd1
											
**			child category    :

**							   level1 : par_cat is parent of child_cata1 and child_cata2
											
											  child_cata1     prom2  prd2 
											  child_cata2     prom3  prd3 
											  
**							   level2: child_cata1 is parent of child_cata3
											
											  child_cata3     prom4  prd4
											  
			product-sku relationship
			prd1	0	sku1
			prd2	0	sku2
			prd2	1	sku3
			prd3	0	sku4
			prd4	0	sku5
					
			TRU_EXCLUDED_PROD :
			prom4	0	prd4
			
			TRU_QUALIFYING_SKU :
			prom5	0	sku1
			

**			Final date in TRU_SKU_QUALIFIED_PROMO table like below

			PROMOTION_DATA is the concat of promotion_id|display_name|startdate|enddate|priority columns in DCS_PROMOTION table
			
			sku1  0  prom1				
			sku1  1  prom5
			sku2  0  prom1 
			sku2  1	 prom2
			sku3  0  prom1
			sku3  1	 prom2
			sku4  0  prom1
			sku4  1  prom3 
			sku5  0	 prom2
			


**    iii. Creation History
**
**           Date         Created by           		Comments
**          ---------     -----------        		---------------
**	   		13/APR/2016   Malleshwara.C				Initial Version
*******************************************************************************************************************************************************************************************

*/

AS
	L_CATEGORY_ID DCS_CATEGORY.CATEGORY_ID%TYPE;
    L_PROMOTION_ID TRU_QUALIFYING_CATEGORY.PROMOTION_ID%TYPE;
   
    ---Variable to hold cursor operation
	CURSOR C1 IS SELECT CATEGORY_ID,PROMOTION_ID FROM TRU_QUALIFYING_CATEGORY
				 MINUS
				 SELECT CATEGORY_ID,PROMOTION_ID FROM TRU_EXCLUDED_CATEGORY;
	L_COUNT number;
	  
    ---Variable to hold dynamic sql statements 
	L_STATEMENT_STR VARCHAR2(10000);
	L_STATEMENT_STR1 VARCHAR2(10000);
	L_STATEMENT_STR2 VARCHAR2(10000);
	L_STATEMENT_STR3 VARCHAR2(10000);
	L_STR varchar2(100);
	L_G_STR varchar2(1);
	L_STATEMENT_1 VARCHAR2(10000);
	L_COUNT1 number;
	
BEGIN
			execute immediate 'truncate table TRU_SKU_QUALIFIED_PROMOS';
			--Cursor ci fetch the TRU_QUALIFYING_CATEGORY records
			OPEN  C1;
			LOOP
				  <<LOOP1>>
				  FETCH C1 into L_CATEGORY_ID,L_PROMOTION_ID;
				  EXIT when C1%NOTFOUND;

				  select COUNT(1) into L_COUNT1 from GT_PAR_CAT_PROM 
				  where GT_PAR_CAT_PROM.CATEGORY_ID = L_CATEGORY_ID  AND GT_PAR_CAT_PROM.PROMOTION_ID =L_PROMOTION_ID;
				  
				  --This  if condition use to skip the ctaegory_id which is already inserted in target table
				  DBMS_OUTPUT.PUT_LINE(L_CATEGORY_ID ||'::'||L_PROMOTION_ID||'::'||L_COUNT1);
				  IF L_COUNT1 > 0
				  then
						DBMS_OUTPUT.PUT_LINE(L_CATEGORY_ID ||'::'||L_PROMOTION_ID||': already inserted');
						GOTO LOOP1;
				  END IF;
				  
				  		
				  
				  
				 --Finding the  child category level from qualifid category list
				 SELECT COUNT(DISTINCT LVL) INTO L_COUNT
					  FROM
					   (SELECT 
							 LEVEL LVL
					   FROM   DCS_CAT_CHLDCAT        
							 START WITH DCS_CAT_CHLDCAT.CATEGORY_ID =L_CATEGORY_ID
							 CONNECT BY DCS_CAT_CHLDCAT.CATEGORY_ID = PRIOR CHILD_CAT_ID
							 ORDER SIBLINGS BY CHILD_CAT_ID
					   );
             
          
				--Populating product list for parent category list					 
							INSERT INTO GT_PROD_PROM (PRODUCT_ID,PROMOTION_ID)
							SELECT distinct CHILD_PRD_ID PRODUCT_ID,PROMOTION_ID
							from
							(SELECT DISTINCT CATEGORY_ID,L_PROMOTION_ID PROMOTION_ID
							from
							(SELECT  CATEGORY_ID,level lvl
											   FROM   DCS_CAT_CHLDCAT 
													 START WITH DCS_CAT_CHLDCAT.CATEGORY_ID = L_CATEGORY_ID
													 CONNECT BY DCS_CAT_CHLDCAT.CATEGORY_ID = PRIOR CHILD_CAT_ID
													 ORDER SIBLINGS BY CHILD_CAT_ID
							)
							MINUS
							select CATEGORY_ID,PROMOTION_ID from TRU_EXCLUDED_CATEGORY
							MINUS select CATEGORY_ID,PROMOTION_ID from GT_PAR_CAT_PROM
							) CAT_PROM
							inner join DCS_CAT_CHLDPRD on(CAT_PROM.CATEGORY_ID = DCS_CAT_CHLDPRD.CATEGORY_ID)
              				union
							select CHILD_PRD_ID PRODUCT_ID,L_PROMOTION_ID from  DCS_CAT_CHLDPRD where CATEGORY_ID = L_CATEGORY_ID;
							--DBMS_OUTPUT.PUT_LINE ('GT_PROD_PROM 1 :'||sql%ROWCOUNT);
			  
						/*--mapping product_id with promotion_id using child category list  start
							  FOR J IN 1..L_COUNT
							  LOOP
									FOR K IN (
												  SELECT DISTINCT CATEGORY_ID,L_PROMOTION_ID PAR_PROMOTION_ID
												  from
												  (SELECT  DCS_CAT_CHLDCAT.CATEGORY_ID,level lvl
																	 FROM   DCS_CAT_CHLDCAT  
																		   START WITH DCS_CAT_CHLDCAT.CATEGORY_ID = L_CATEGORY_ID
																		   CONNECT BY DCS_CAT_CHLDCAT.CATEGORY_ID = PRIOR CHILD_CAT_ID
																		   ORDER SIBLINGS BY CHILD_CAT_ID
												  ) WHERE LVL = J
												  minus select CATEGORY_ID,PROMOTION_ID from TRU_EXCLUDED_CATEGORY
							  minus select CATEGORY_ID,PROMOTION_ID from GT_PAR_CAT_PROM
												)
								 
					  LOOP
						*/			
							   --DBMS_OUTPUT.PUT_LINE ('k loop '||J||':'||L_CATEGORY_ID);
								  L_STATEMENT_STR := 'SELECT CHILD_CAT_ID FROM (SELECT  CHILD_CAT_ID,'||''''||L_PROMOTION_ID||''''||' FROM DCS_CAT_CHLDCAT WHERE CATEGORY_ID  = '||''''||L_CATEGORY_ID||''''||
                                      'minus  select CATEGORY_ID,PROMOTION_ID from TRU_EXCLUDED_CATEGORY
                                       minus select CATEGORY_ID,PROMOTION_ID from GT_PAR_CAT_PROM)';
								  L_STATEMENT_STR1 := 'select child_cat_id,'||''''||L_PROMOTION_ID||''''||' from dcs_cat_chldcat where category_id in(';
								  L_STATEMENT_STR2 := 'select child_cat_id from dcs_cat_chldcat where category_id in(';
								  L_STR := ')';
								  l_g_str :=')';
								  L_STATEMENT_STR3 := null;
								  
								  FOR L IN 1..50
								  LOOP
								  
									  L_STATEMENT_1 :=  L_STATEMENT_STR1||L_STATEMENT_STR3||L_STATEMENT_STR||L_STR;
									  
									  execute immediate 'insert into GT_CAT_PROM(CATEGORY_ID,PROMOTION_ID)'||L_STATEMENT_1|| ' MINUS SELECT CATEGORY_ID,PROMOTION_ID FROM TRU_EXCLUDED_CATEGORY minus select CATEGORY_ID,PROMOTION_ID from GT_PAR_CAT_PROM';
									  --DBMS_OUTPUT.PUT_LINE('insert into GT_CAT_PROM(CATEGORY_ID,PROMOTION_ID)'||L_STATEMENT_1);
									  DBMS_OUTPUT.PUT_LINE ('GT_PROD_PROM '||L_CATEGORY_ID||' :'||SQL%ROWCOUNT);
									  IF SQL%ROWCOUNT = 0
									  THEN
										EXIT;
									  END IF;
									  L_STR := l_g_str||L_STR;
									  L_STATEMENT_STR3 := L_STATEMENT_STR2||L_STATEMENT_STR3;
									  
								  end LOOP;
							--end LOOP;
					  --end LOOP;
            
					   --Populating category_id and promotion_id in global temporary table (GT_PAR_CAT_PROM) to validate category_id already inserted or not
						INSERT INTO GT_PAR_CAT_PROM(CATEGORY_ID,PROMOTION_ID)
						SELECT distinct par_cat_id,L_PROMOTION_ID
						FROM
						  (SELECT CHILD_CAT_ID,
						   DCS_CAT_CHLDCAT.CATEGORY_ID par_cat_id
						   from   DCS_CAT_CHLDCAT     
							start with DCS_CAT_CHLDCAT.CATEGORY_ID =L_CATEGORY_ID
							connect by DCS_CAT_CHLDCAT.CATEGORY_ID = prior CHILD_CAT_ID
							order siblings by CHILD_CAT_ID
						  );	
			END LOOP;
			
			--mapping product_id with promotion_id using child category list  end
			
			CLOSE C1;
									 
			INSERT INTO GT_PROD_PROM (PRODUCT_ID,PROMOTION_ID)
			SELECT CHILD_PRD_ID PRODUCT_ID,PROMOTION_ID
			FROM GT_CAT_PROM INNER JOIN DCS_CAT_CHLDPRD ON(GT_CAT_PROM.CATEGORY_ID = DCS_CAT_CHLDPRD.CATEGORY_ID);
			
			INSERT INTO TRU_SKU_QUALIFIED_PROMOS(SKU_ID,SEQUENCE_NUM,PROMOTION_DATA)
			select SKU_ID,
				   ROW_NUMBER() over(partition by SKU_ID order by DCS_PROMOTION.PROMOTION_ID) -1 SEQUENCE_NUM,
				   DCS_PROMOTION.PROMOTION_ID||'|'||DISPLAY_NAME||'|'||NVL(BEGIN_USABLE,TO_TIMESTAMP('01-JAN-2106 12.00.00.000000 AM','DD-MON-YY HH.MI.SS.FF AM'))||'|'||NVL(END_USABLE,TO_TIMESTAMP('01-JAN-9999 12.00.00.000000 AM','DD-MON-YY HH.MI.SS.FF AM'))||'|'||PRIORITY PROMOTION_DATA
			from(DCS_PROMOTION
				inner join
					 (
					 (select DCS_PRD_CHLDSKU.SKU_ID,PROMOTION_ID
					  FROM
					  DCS_PRD_CHLDSKU inner join
					  (
						(SELECT PRODUCT_ID,PROMOTION_ID FROM GT_PROD_PROM
						  UNION
						  select PRODUCT_ID,PROMOTION_ID from TRU_QUALIFYING_PROD
						)
						minus
						select PRODUCT_ID,PROMOTION_ID from TRU_EXCLUDED_PROD
					  )PROM_PRD on(DCS_PRD_CHLDSKU.PRODUCT_ID = PROM_PRD.PRODUCT_ID)
					  union
					  select SKU_ID,PROMOTION_ID from TRU_QUALIFYING_SKU
					 ) 					  
					 minus
					(select SKU_ID,PROMOTION_ID from TRU_EXCLUDED_SKU )
				  )SKU_PROM 
					on (DCS_PROMOTION.PROMOTION_ID = SKU_PROM.PROMOTION_ID)
				  )where DCS_PROMOTION.enabled =1 and (DCS_PROMOTION.END_USABLE > systimestamp or DCS_PROMOTION.END_USABLE is null);
        DBMS_OUTPUT.PUT_LINE ('TRU_SKU_QUALIFIED_PROMOS :'||sql%ROWCOUNT);
	  COMMIT;

EXCEPTION  WHEN OTHERS THEN
RAISE;

END TRU_QUALIF_SKU_PROMOS_PRC;