ALTER TABLE tru_site_configuration ADD lightbox_configurable_time INTEGER 	NULL;

ALTER TABLE tru_site_configuration ADD lightbox_submit_Conf_time INTEGER 	NULL;

ALTER TABLE tru_site_configuration ADD lightbox_donotshow_Conf_time INTEGER 	NULL;


UPDATE tru_site_configuration
SET lightbox_configurable_time=1
WHERE ID='ToysRUs';

UPDATE tru_site_configuration
SET lightbox_submit_Conf_time=1
WHERE ID='ToysRUs';

UPDATE tru_site_configuration
SET lightbox_donotshow_Conf_time=1
WHERE ID='ToysRUs';

UPDATE tru_site_configuration
SET lightbox_configurable_time=1
WHERE ID='BabyRUs';

UPDATE tru_site_configuration
SET lightbox_submit_Conf_time=1
WHERE ID='BabyRUs';

UPDATE tru_site_configuration
SET lightbox_donotshow_Conf_time=1
WHERE ID='BabyRUs';