drop table tru_integration;
CREATE TABLE tru_integration (
          id varchar2(254)  NOT NULL,
		  integration_name varchar2(254),
		  enable_integration number(1),          
          CONSTRAINT tru_integration_P PRIMARY KEY(id)
);

drop table tru_sos_int_configurations;
CREATE TABLE tru_sos_int_configurations (
          id                 varchar2(254)   NOT NULL REFERENCES site_configuration(id),
          sequence_num              INTEGER   NOT NULL,
          sos_integrations      varchar2(254)  NULL,
          CONSTRAINT tru_sos_int_configurations_P PRIMARY KEY(id, sequence_num)
);


drop table tru_int_configurations;
CREATE TABLE tru_int_configurations (
          id                 varchar2(254)   NOT NULL REFERENCES site_configuration(id),
          sequence_num              INTEGER   NOT NULL,
          tru_integrations      varchar2(254)  NULL,
          CONSTRAINT tru_int_configurations_P PRIMARY KEY(id, sequence_num)
);
