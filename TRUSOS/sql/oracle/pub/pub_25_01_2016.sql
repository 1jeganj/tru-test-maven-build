drop table tru_integration;
CREATE TABLE tru_integration (
          id varchar2(254)  NOT NULL,
		  asset_version 	INTEGER	NOT NULL,
		  integration_name varchar2(254),
		  enable_integration number(1), 
		  version_deleted  NUMBER(1),    
          version_editable  NUMBER(1),     
		  pred_version  NUMBER(38),    
		  workspace_id  VARCHAR2(254),
          branch_id  VARCHAR2(254), 
		  is_head  NUMBER(1),     
          checkin_date  DATE,     		  
          CONSTRAINT tru_integration_P PRIMARY KEY(id, asset_version)
);

drop table tru_sos_int_configurations;
CREATE TABLE tru_sos_int_configurations (
	id 			varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	sos_integrations 	varchar2(254)	NULL,
	CONSTRAINT tru_sos_int_configurations_P PRIMARY KEY(id, asset_version, sequence_num)
);

drop table tru_int_configurations;
CREATE TABLE tru_int_configurations (
	id 			varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	tru_integrations 	varchar2(254)	NULL,
	CONSTRAINT tru_int_configurations_P PRIMARY KEY(id, asset_version, sequence_num)
);

