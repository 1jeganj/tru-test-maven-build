package com.tru.dynamo.servlet.dafpipeline;

import java.util.Map;

import atg.core.util.StringUtils;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.AccessController;
import atg.userprofiling.Profile;

import com.tru.common.constants.TRUSOSConstants;
import com.tru.common.vo.TRUSiteVO;
import com.tru.userprofiling.TRUSOSCookieManager;
import com.tru.utils.TRUGetSiteTypeUtil;

/** 
 * TRUSOSAccessController class for restricting the user to some pages based on certain conditions.
 * @author Professional Access
 * @version 1.0
 */
public class TRUSOSAccessController extends GenericService implements AccessController {
		/** Property to hold the denied access mDeniedUrl. */
	private String mDeniedUrl;
	/** Property to hold mCookieManager. */
	private TRUSOSCookieManager mCookieManager;
	/** The m tru get site type mTruGetSiteTypeUtil. */
	private TRUGetSiteTypeUtil mTruGetSiteTypeUtil;

	
	
	/**
	 * @return the truGetSiteTypeUtil
	 */
	public TRUGetSiteTypeUtil getTruGetSiteTypeUtil() {
		return mTruGetSiteTypeUtil;
	}

	/**
	 * @param pTruGetSiteTypeUtil the truGetSiteTypeUtil to set
	 */
	public void setTruGetSiteTypeUtil(TRUGetSiteTypeUtil pTruGetSiteTypeUtil) {
		mTruGetSiteTypeUtil = pTruGetSiteTypeUtil;
	}

	/**
     * Checks if the user is allowed to access the requested URL.
     *
     * @param pProfile - current user
     * @param pRequest - DynamoHttpServletRequest
     * @return true if is explicitly logged in
     *         false otherwise
     */
	public boolean allowAccess(Profile pProfile, DynamoHttpServletRequest pRequest) {
		if (isLoggingDebug()) {
			logDebug("TRUSOSAccessController (allowAccess) : START");
		}
		boolean checkaccess = false;
		checkaccess = !isRestricted(pRequest);
		if (isLoggingDebug()) {
			logDebug("TRUSOSAccessController (allowAccess) : END");
		}
		return checkaccess;
	}
	
	/**
	 * Checks if the user logged in as SOS user.
	 * @param pRequest - DynamoHttpServletRequest
	 * @return true if user is a SOS user
	 */
	private boolean isRestricted(DynamoHttpServletRequest pRequest){
		boolean restrictAccess = false;
		String currentSite = null;
		//getting the TRUSiteVO which contain site related details
		TRUSiteVO truSiteVO = getTruGetSiteTypeUtil().getSiteInfo(pRequest,SiteContextManager.getCurrentSite());
		if(truSiteVO != null){
			currentSite = truSiteVO.getSiteStatus();
		}
		
		
		if (isLoggingDebug()) {
			logDebug("TRUSOSAccessController (isRestricted) : START");
		}
		Map<String,String> sosUserMap= getCookieManager().getTRUSosCookie(pRequest);
		if(null != sosUserMap && StringUtils.isNotBlank(currentSite) && currentSite.equals(TRUSOSConstants.SOS)){
				String isSos = sosUserMap.get(TRUSOSConstants.IS_SOS_COOKIE);
				if(StringUtils.isNotBlank(isSos)){
					restrictAccess = Boolean.parseBoolean(isSos);
				}
			
		}
		
		if (isLoggingDebug()) {
			logDebug("TRUSOSAccessController (isRestricted) : END");
		}
		return restrictAccess;
	}
	
	/**
	 * Gets the denied access Url for the specific profile.
	 * @param pProfile - current user
	 * @return deniedAccessUrl String
	 */
	public String getDeniedAccessURL(Profile pProfile) {
		String deniedAccessUrl = getDeniedUrl();
		return deniedAccessUrl;
	}
	
	/** getter method for denied url.
     *  @return the mDeniedUrl
     */
	public String getDeniedUrl() {
		return mDeniedUrl;
	}
	
	/**
     * @param pDeniedUrl the mDeniedUrl to set.
     */
	public void setDeniedUrl(String pDeniedUrl) {
		this.mDeniedUrl = pDeniedUrl;
	}

	/**
	 * @return the cookieManager
	 */
	public TRUSOSCookieManager getCookieManager() {
		return mCookieManager;
	}

	/**
	 * @param pCookieManager the cookieManager to set
	 */
	public void setCookieManager(TRUSOSCookieManager pCookieManager) {
		mCookieManager = pCookieManager;
	}
}
