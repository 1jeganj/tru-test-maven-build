package com.tru.userprofiling;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.common.constants.TRUSOSConstants;
import com.tru.integrations.TRUSOSIntegrationPropertiesConfig;


/**
 * Extending TRUCookieManager class.This class is used to set some sos related cookie when user login as SOS user.
 * @author Professional Access
 * @version 1.0
 */

public class TRUSOSCookieManager extends TRUCookieManager{
	/**  property to hold mIntegrationPropertiesConfig. */
	private TRUSOSIntegrationPropertiesConfig mIntegrationPropertiesConfig;	
		
	/**
	 * @return the integrationPropertiesConfig
	 */
	public TRUSOSIntegrationPropertiesConfig getIntegrationPropertiesConfig() {
		return mIntegrationPropertiesConfig;
	}

	/**
	 * @param pIntegrationPropertiesConfig the integrationPropertiesConfig to set
	 */
	public void setIntegrationPropertiesConfig(
			TRUSOSIntegrationPropertiesConfig pIntegrationPropertiesConfig) {
		mIntegrationPropertiesConfig = pIntegrationPropertiesConfig;
	}

	/**
	 * This  method will set sos cookies  when user login as SOS user.
	 * @param pRequest -DynamoHttpServletRequest
	 * @param pResponse - DynamoHttpServletResponse
	 * @param pEmployeeName - String
	 * @param pEmployeeNumber -String
	 * @param pStoreNumber -String
	 */
	public void setTRUSOSCookie(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse, String pEmployeeName,
			String pEmployeeNumber, String pStoreNumber) {
		if(isLoggingDebug()){
			logDebug("TRUCookieManager (setTRUSOSCookie) : START");
		}
		String domainName = (String)pRequest.getServerName();
		String serverName=null;
		if (StringUtils.isNotBlank(domainName))
		{
			serverName = domainName.substring(domainName.indexOf(TRUSOSConstants.DOT), domainName.length());
		}
		boolean isSOS = Boolean.FALSE;
		if(!StringUtils.isBlank(pRequest.getServerName()) && getIntegrationPropertiesConfig().getSosConfiguredURLs() != null && getIntegrationPropertiesConfig().getSosConfiguredURLs().size() > 0 && getIntegrationPropertiesConfig().getSosConfiguredURLs().contains(pRequest.getServerName())) 
		{
			if(isLoggingDebug()){
				logDebug("If request url match with the configured with url we are setting isSos cookie as true");
			}
			removeAllSOSCookies(pRequest, pResponse, TRUSOSConstants.IS_SOS_COOKIE);
			isSOS = Boolean.TRUE;
			Cookie iSSOS = new Cookie(TRUSOSConstants.IS_SOS_COOKIE,TRUSOSConstants.IS_SOS_VALUE);
			iSSOS.setMaxAge(Integer.MAX_VALUE);
			iSSOS.setPath(TRUSOSConstants.PATH);
			iSSOS.setSecure(Boolean.FALSE);
			iSSOS.setDomain(serverName);
			pResponse.addCookie(iSSOS);
			addCookieToSession(pRequest, iSSOS);
		}
		if(isLoggingDebug()){
			logDebug("TRUCookieManager (setTRUSOSCookie) :value of IsSOS" + isSOS);
		}
		// If SOS is true then below block will execute and set SOS user details cookie
		if(isSOS){
			Cookie sosUserNameCookie = new Cookie(TRUSOSConstants.SOS_EMPLOYEE_NAME_COOKIE,pEmployeeName);
			sosUserNameCookie.setMaxAge(Integer.MAX_VALUE);
			sosUserNameCookie.setPath(TRUSOSConstants.PATH);
			sosUserNameCookie.setSecure(Boolean.FALSE);

			sosUserNameCookie.setDomain(serverName);
			
			if(null != sosUserNameCookie){
				pResponse.addCookie(sosUserNameCookie);
				addCookieToSession(pRequest, sosUserNameCookie);
			}
			
			Cookie sosUserNumberCookie = new Cookie(TRUSOSConstants.SOS_EMPLOYEE_NUMBER_COOKIE,pEmployeeNumber);
			sosUserNumberCookie.setMaxAge(Integer.MAX_VALUE);
			sosUserNumberCookie.setPath(TRUSOSConstants.PATH);
			sosUserNumberCookie.setSecure(Boolean.FALSE);
			sosUserNumberCookie.setDomain(serverName);
			if(null != sosUserNumberCookie){
				pResponse.addCookie(sosUserNumberCookie);
				addCookieToSession(pRequest, sosUserNumberCookie);
			}
			
			Cookie sosStoreNumberCookie = new Cookie(TRUSOSConstants.SOS_STORE_NUMBER_COOKIE,pStoreNumber);
			sosStoreNumberCookie.setMaxAge(Integer.MAX_VALUE);
			sosStoreNumberCookie.setPath(TRUSOSConstants.PATH);
			sosStoreNumberCookie.setSecure(Boolean.FALSE);
			sosStoreNumberCookie.setDomain(serverName);
			if(null != sosStoreNumberCookie){
				pResponse.addCookie(sosStoreNumberCookie);
				addCookieToSession(pRequest, sosStoreNumberCookie);
			}
		}
		if(isLoggingDebug()){
			logDebug("TRUCookieManager (setTRUSOSCookie) : END");
		}
	}
	/**
	 * This  method will return sos cookies.
	 * @param pRequest -DynamoHttpServletRequest
	 * @return a Map object contain SOS related cookie
	 */
	
	public Map<String, String> getTRUSosCookie(DynamoHttpServletRequest pRequest) {
		if(isLoggingDebug()){
			logDebug("TRUSOSCookieManager (getTRUSosCookie) : START");
		}
		Map<String, String> sosUserDetailsMap=new HashMap<String, String>();
		Cookie[] cookies = null;
		boolean isSos = false;
		if (pRequest != null) {
			cookies = pRequest.getCookies();
		}
		if (null != cookies) {
			for (Cookie cookie : cookies) {
				if (cookie.getName() != null && cookie.getName().equals(TRUSOSConstants.IS_SOS_COOKIE) && StringUtils.isNotBlank(cookie.getValue())) {
						isSos = Boolean.parseBoolean(cookie.getValue());
					break;
				}
			}
		}
		
		// if isSos is true we are setting SOS user details to map
		if (null != cookies && isSos) {
			if(isLoggingDebug()){
				logDebug("cookies is not null and isSos is true hence processing");
			}
			for (Cookie cookie : cookies) {
				if (cookie.getName() != null && cookie.getName().equals(TRUSOSConstants.SOS_EMPLOYEE_NAME_COOKIE)) {
					sosUserDetailsMap.put(cookie.getName(), cookie.getValue());
				}

				if (cookie.getName() != null && cookie.getName().equals(TRUSOSConstants.IS_SOS_COOKIE)) {
					sosUserDetailsMap.put(cookie.getName(), cookie.getValue());
				}

				if (cookie.getName() != null && cookie.getName().equals(TRUSOSConstants.SOS_EMPLOYEE_NUMBER_COOKIE)) {
					sosUserDetailsMap.put(cookie.getName(), cookie.getValue());
				}

				if (cookie.getName() != null && cookie.getName().equals(TRUSOSConstants.SOS_STORE_NUMBER_COOKIE)) {
					sosUserDetailsMap.put(cookie.getName(), cookie.getValue());
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("TRUSOSCookieManager (getTRUSosCookie) : END");
		}

		return sosUserDetailsMap;
	}
	
	/**
	 * This method will return true if user is sos user otherwise return false.
	 * @param pRequest -DynamoHttpServletRequest
	 * @return true if it's a SOS user
	 */
	public boolean verifySOSCookie(DynamoHttpServletRequest pRequest) {
		if(isLoggingDebug()){
			logDebug("TRUSOSCookieManager (verifySOSCookie) : START");
		}
		Cookie[] cookies = null;
		boolean isSOSCookieVal = false;
		if (pRequest != null) {
			cookies = pRequest.getCookies();
		}
		if (null != cookies) {
			for (Cookie cookie : cookies) {
				if (StringUtils.isNotBlank(cookie.getName()) && cookie.getName().equals(TRUSOSConstants.IS_SOS_COOKIE) && StringUtils.isNotBlank(cookie.getValue())) {
					isSOSCookieVal = Boolean.parseBoolean(cookie.getValue());
					break;										
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("TRUSOSCookieManager (verifySOSCookie) : START");
		}
		return isSOSCookieVal;
	}
	
	/**
	 * This  method will get sos cookies.
	 * @param pRequest -DynamoHttpServletRequest
	 * @return  a Map object contain SOS related cookie
	 */
	public Map<String, String> getSOSUserCookieDetails(DynamoHttpServletRequest pRequest) {
		if(isLoggingDebug()){
			logDebug("TRUSOSCookieManager (getTRUSosCookie) : START");
		}
		Map<String, String> sosUserDetailsMap = new HashMap<String, String>();
		Cookie[] cookies = pRequest.getCookies();
		// below block will set SOS user details to map
		if (null != cookies) {
			for (Cookie cookie : cookies) {
				if (cookie.getName() != null && cookie.getName().equals(TRUSOSConstants.SOS_EMPLOYEE_NAME_COOKIE)) {
					sosUserDetailsMap.put(cookie.getName(), cookie.getValue());
				}
				if (cookie.getName() != null && cookie.getName().equals(TRUSOSConstants.IS_SOS_COOKIE)) {
					sosUserDetailsMap.put(cookie.getName(), cookie.getValue());
				}
				if (cookie.getName() != null && cookie.getName().equals(TRUSOSConstants.SOS_EMPLOYEE_NUMBER_COOKIE)) {
					sosUserDetailsMap.put(cookie.getName(), cookie.getValue());
				}
				if (cookie.getName() != null && cookie.getName().equals(TRUSOSConstants.SOS_STORE_NUMBER_COOKIE)) {
					sosUserDetailsMap.put(cookie.getName(), cookie.getValue());
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("TRUSOSCookieManager (getTRUSosCookie) : END");
		}
		return sosUserDetailsMap;
	}
	
	/**
	 * This  method will set isSos cookie as once user logout from SOS site.
	 * @param pRequest -DynamoHttpServletRequest
	 * @param pResponse - DynamoHttpServletResponse
	 */
	public void removeIsSOSCookie(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		
		if(isLoggingDebug()){
			logDebug("TRUSOSCookieManager (removeIsSOSCookie) : START");
		}
		String domainName = (String)pRequest.getServerName();
		String domainNamePrefix=null;
		if (StringUtils.isNotBlank(domainName))
		{
         domainNamePrefix = domainName.substring(domainName.indexOf(TRUSOSConstants.DOT), domainName.length());
		}
		Cookie[] cookies = null;
		if (pRequest != null) {
			cookies = pRequest.getCookies();
		}
		if (null != cookies) {
			for (Cookie cookie : cookies) {
				if (cookie.getName() != null && cookie.getName().equals(TRUSOSConstants.IS_SOS_COOKIE)) {
					//setting isSOS cookie as false
					cookie.setValue(TRUSOSConstants.IS_SOS_FALSE_VALUE);
					cookie.setDomain(domainNamePrefix);
					cookie.setMaxAge(Integer.MAX_VALUE);
					cookie.setPath(TRUSOSConstants.PATH);
					cookie.setSecure(Boolean.FALSE);
					pResponse.addCookie(cookie);
					addCookieToSession(pRequest, cookie);
				}	

			}
		}
		removeAllSOSCookies(pRequest, pResponse, TRUSOSConstants.SOS_EMPLOYEE_NAME_COOKIE);
		removeAllSOSCookies(pRequest, pResponse, TRUSOSConstants.SOS_EMPLOYEE_NUMBER_COOKIE);
		removeAllSOSCookies(pRequest, pResponse, TRUSOSConstants.SOS_STORE_NUMBER_COOKIE);
		if(isLoggingDebug()){
			logDebug("TRUSOSCookieManager (removeIsSOSCookie) : END");
		}
	}
	
	
	/**
	 * Removes the all SOS cookies.
	 *
	 * @param pRequest the request.
	 * @param pResponse the response.
	 * @param pCookieName the cookie name.
	 */
	private void removeAllSOSCookies(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse,
			String pCookieName) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUSOSCookieManager::@method::removeAllSOSCookies() : BEGIN");
			vlogDebug("@Class::TRUSOSCookieManager::@method::removeAllSOSCookies() : CookieName {0}:", pCookieName);
		}
		String domainName = (String)pRequest.getServerName();
		String domainNamePrefix=null;
		if (StringUtils.isNotBlank(domainName))
		{
         domainNamePrefix = domainName.substring(domainName.indexOf(TRUSOSConstants.DOT), domainName.length());
		}
		String cookieName = pCookieName;
		Cookie sosCookie = getCookieFromRequest(cookieName, pRequest);
		if (sosCookie != null) {
			sosCookie.setDomain(domainNamePrefix);
			sosCookie.setMaxAge(0);
			sosCookie.setPath(TRUSOSConstants.PATH);
			sosCookie.setValue(TRUSOSConstants.EMPTY_STRING);
			pResponse.addCookie(sosCookie);
			removeCookieFromSession(pRequest, sosCookie.getName());
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUSOSCookieManager::@method::removeSOSCookies() : END");
		}
	}	
}
