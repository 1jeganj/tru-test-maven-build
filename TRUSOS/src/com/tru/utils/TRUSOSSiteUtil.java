package com.tru.utils;

import java.util.List;

import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryItem;

import com.tru.integrations.TRUSOSIntegrationPropertiesConfig;

public class TRUSOSSiteUtil extends TRUGetSiteTypeUtil {

	/**
	 * Checks if is SOS integration site config enabled.
	 *
	 * @param pSite the site
	 * @param pString the string
	 * @return true, if is SOS integration site config enabled
	 */
	@SuppressWarnings("unchecked")
	public boolean isSOSIntegrationSiteConfigEnabled(String pString){
		if (isLoggingDebug()) {
			vlogDebug("START:: TRUGetSiteTypeUtil.isSOSIntegrationSiteConfigEnabled" );
		}
		Site pSite = SiteContextManager.getCurrentSite();
		if(pSite != null && !StringUtils.isBlank(pString)){
			
			List<RepositoryItem> itemList = (List<RepositoryItem>)pSite.getPropertyValue((
					(TRUSOSIntegrationPropertiesConfig)getTRUIntegrationPropertiesConfig()).getSosIntegrationsPropertyName());
			for(RepositoryItem item : itemList){
				String integName = (String)item.getPropertyValue(getTRUIntegrationPropertiesConfig().getIntegrationNamePropertyName());
				if(pString.equals(integName)){
					return (Boolean)item.getPropertyValue(getTRUIntegrationPropertiesConfig().getEnabledPropertyName());
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUGetSiteTypeUtil.isSOSIntegrationSiteConfigEnabled" );
		}
		return false;
	}
}
