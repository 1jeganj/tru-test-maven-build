package com.tru.integrations;

import java.util.List;

/**
 * Configuration for holding property names specific to SOS.
 * extends TRUIntegrationPropertiesConfig properties
 * @author PA
 * @version 1.0
 */
public class TRUSOSIntegrationPropertiesConfig extends TRUIntegrationPropertiesConfig {
	
	/** property to hold mSosIntegrationsPropertyName. */
	private String mSosIntegrationsPropertyName;

	/** Holds mSosConfiguredURLs value. */
	private List<String> mSosConfiguredURLs;

	/**
	 * @return the sosIntegrationsPropertyName
	 */
	public String getSosIntegrationsPropertyName() {
		return mSosIntegrationsPropertyName;
	}
	/**
	 * @param pSosIntegrationsPropertyName the sosIntegrationsPropertyName to set
	 */
	public void setSosIntegrationsPropertyName(String pSosIntegrationsPropertyName) {
		mSosIntegrationsPropertyName = pSosIntegrationsPropertyName;
	}	
	
	
	/**
	 * @return the mSosConfiguredURLs - sosConfiguredURLs
	 */
	public List<String> getSosConfiguredURLs() {
		return mSosConfiguredURLs;
	}

	/**
	 * Sets the sos configured URLS.
	 *
	 * @param pSosConfiguredURLs the sos configured URLS
	 */
	public void setSosConfiguredURLs(List<String> pSosConfiguredURLs) {
		mSosConfiguredURLs = pSosConfiguredURLs;
	}	
}
