package com.tru.common;

import com.tru.integrations.TRUSOSIntegrationPropertiesConfig;
import com.tru.utils.TRUSOSSiteUtil;

/**
 * This class contains tru sos integration specific configurations.
 * @author PA
 * @version 1.0
 */
public class TRUSOSIntegrationConfiguration extends TRUSOSIntegrationConfig {

	/** property to hold mEnableCouponService. */
    private boolean mEnableCouponService;
    
    /**  property to hold mEnableAddressDoctor. */
	private boolean mEnableAddressDoctor;

	/**  property to hold mEnableEpslon. */
	private boolean mEnableEpslon;

	/**  property to hold mEnableHookLogic. */
	private boolean mEnableHookLogic;
	
	/**  property to hold mEnablePowerReviews. */
	private boolean mEnablePowerReviews;
	
	/**  property to hold mEnableTaxware. */
	private boolean mEnableTaxware;
	
	/**  property to hold mEnableCertona. */
	private boolean mEnableCertona;
	
	/**  property to hold mEnableCardinal. */
	private boolean mEnableCardinal;
	
	/** property to hold mRadialPaymentIntegrationName. */
	private String mRadialPaymentIntegrationName;
	/** property to hold mRadialSoftAllocationIntegrationName. */
	private String mRadialSoftAllocationIntegrationName;
	
	/** The Integration properties config. */
	private TRUSOSIntegrationPropertiesConfig mIntegrationPropertiesConfig;
	/**
	 * Gets the integration properties config.
	 *
	 * @return the integration properties config
	 */
	public TRUSOSIntegrationPropertiesConfig getIntegrationPropertiesConfig() {
		return mIntegrationPropertiesConfig;
	}

	/**
	 * Sets the integration properties config.
	 *
	 * @param pIntegrationPropertiesConfig the new integration properties config
	 */
	public void setIntegrationPropertiesConfig(TRUSOSIntegrationPropertiesConfig pIntegrationPropertiesConfig) {
		mIntegrationPropertiesConfig = pIntegrationPropertiesConfig;
	}
	/**
	 * @return the radialPaymentIntegrationName
	 */
	public String getRadialPaymentIntegrationName() {
		return mRadialPaymentIntegrationName;
	}
	/**
	 * @param pRadialPaymentIntegrationName the radialPaymentIntegrationName to set
	 */
	public void setRadialPaymentIntegrationName(String pRadialPaymentIntegrationName) {
		mRadialPaymentIntegrationName = pRadialPaymentIntegrationName;
	}
	/**
	 * @return the radialSoftAllocationIntegrationName
	 */
	public String getRadialSoftAllocationIntegrationName() {
		return mRadialSoftAllocationIntegrationName;
	}
	/**
	 * @param pRadialSoftAllocationIntegrationName the radialSoftAllocationIntegrationName to set
	 */
	public void setRadialSoftAllocationIntegrationName(
			String pRadialSoftAllocationIntegrationName) {
		mRadialSoftAllocationIntegrationName = pRadialSoftAllocationIntegrationName;
	}

	/**
	 * Checks if is enable PowerReviews.
	 * @return the enablePowerReviews
	 */
	public boolean isEnablePowerReviews() {
		return mEnablePowerReviews;
	}

	/**
	 * Sets the enable PowerReviews.
	 * @param pEnablePowerReviews the enablePowerReviews to set
	 */
	public void setEnablePowerReviews(boolean pEnablePowerReviews) {
		mEnablePowerReviews = pEnablePowerReviews;
	}
	
	/**
	 * Checks if is enable hook logic.
	 * @return the enableHookLogic
	 */
	public boolean isEnableHookLogic() {
		return mEnableHookLogic;
	}

	/**
	 * Sets the enable hooklogic.
	 * @param pEnableHookLogic the enableHookLogic to set
	 */
	public void setEnableHookLogic(boolean pEnableHookLogic) {
		mEnableHookLogic = pEnableHookLogic;
	}
	
	/**
	 * Checks if is enable address doctor.
	 * @return the enableAddressDoctor
	 */
	public boolean isEnableAddressDoctor() {
		return mEnableAddressDoctor;
	}

	/**
	 * Sets the enable address doctor.
	 * @param pEnableAddressDoctor the enableAddressDoctor to set
	 */
	public void setEnableAddressDoctor(boolean pEnableAddressDoctor) {
		mEnableAddressDoctor = pEnableAddressDoctor;
	}

	/**
	 * Checks if is enable epslon.
	 * @return the enableEpslon
	 */
	public boolean isEnableEpslon() {
		if(getSiteUtil() != null && getIntegrationPropertiesConfig() != null){
			return ((TRUSOSSiteUtil)getSiteUtil()).isSOSIntegrationSiteConfigEnabled(getIntegrationPropertiesConfig().getEpslonIntegrationName());
		}else{
		return mEnableEpslon;
		}
	}

	/**
	 * Sets the enable epslon.
	 * @param pEnableEpslon the enableEpslon to set
	 */
	public void setEnableEpslon(boolean pEnableEpslon) {
		mEnableEpslon = pEnableEpslon;
	}
	
	/**
	 * @return the enableCouponService
	 */
	public boolean isEnableCouponService() {
		return mEnableCouponService;
	}

	/**
	 * @param pEnableCouponService the enableCouponService to set
	 */
	public void setEnableCouponService(boolean pEnableCouponService) {
		mEnableCouponService = pEnableCouponService;
	}

	/**
	 * @return the enableTaxware
	 */
	public boolean isEnableTaxware() {
		return mEnableTaxware;
	}

	/**
	 * @param pEnableTaxware the enableTaxware to set
	 */
	public void setEnableTaxware(boolean pEnableTaxware) {
		mEnableTaxware = pEnableTaxware;
	}

	/**
	 * @return the enableCertona
	 */
	public boolean isEnableCertona() {
		return mEnableCertona;
	}

	/**
	 * @param pEnableCertona the enableCertona to set
	 */
	public void setEnableCertona(boolean pEnableCertona) {
		mEnableCertona = pEnableCertona;
	}

	/**
	 * @return the enableCardinal
	 */
	public boolean isEnableCardinal() {
		return mEnableCardinal;
	}

	/**
	 * @param pEnableCardinal the enableCardinal to set
	 */
	public void setEnableCardinal(boolean pEnableCardinal) {
		mEnableCardinal = pEnableCardinal;
	}
	
}
