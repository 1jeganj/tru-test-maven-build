package com.tru.common;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import atg.commerce.CommerceException;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.purchase.PaymentGroupMapContainer;
import atg.commerce.order.purchase.ShippingGroupMapContainer;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUCommerceItemManager;
import com.tru.commerce.order.TRUOrderHolder;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.constants.TRUSOSConstants;
import com.tru.userprofiling.TRUCookieManager;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.userprofiling.TRUSOSCookieManager;
/**
 * This formHnadler will validate SOS login and logout page page.
 * @author Pragadeeshwaran AS
 * @version 1.0
 */
public class TRUSOSValidationFormHandler extends GenericFormHandler {
	/** Property to hold mLogouSuccessURL. */
	private String  mLogoutSuccessURL;
	/** Property to hold mEmployeeName. */
	private String  mEmployeeName;

	/** Property to hold mEmployeeNumber. */
	private String mEmployeeNumber;

	/** Property to hold mStoreNumber. */
	private String mStoreNumber;

	/** Property to hold mSuccessURL. */
	private String  mSuccessURL;

	/** Property to hold mErrorURL. */
	private String   mErrorURL;

	/** Property to hold mCookieManager. */
	private TRUSOSCookieManager mCookieManager;
	
	/** Property to hold mCookieManager. */
	private PaymentGroupMapContainer mPaymentGroupGroupMapContainer;
	
	/** Property to hold mCookieManager. */
	private ShippingGroupMapContainer mShippingGroupMapContainer;
	
	/** Property to hold mProfileTools. */
	private TRUProfileTools mProfileTools;
	/**
	 * @return the profileTools
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * @param pProfileTools the profileTools to set
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	/**
	 * Holds reference for mTransactionManager.
	 */
	private TransactionManager mTransactionManager;

	/**
	 * @return the transactionManager
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}


	/**
	 * @param pTransactionManager the transactionManager to set
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}


	/**
	 * @return the paymentGroupGroupMapContainer
	 */
	public PaymentGroupMapContainer getPaymentGroupGroupMapContainer() {
		return mPaymentGroupGroupMapContainer;
	}


	/**
	 * @param pPaymentGroupGroupMapContainer the paymentGroupGroupMapContainer to set
	 */
	public void setPaymentGroupGroupMapContainer(
			PaymentGroupMapContainer pPaymentGroupGroupMapContainer) {
		mPaymentGroupGroupMapContainer = pPaymentGroupGroupMapContainer;
	}


	/**
	 * @return the shippingGroupMapContainer
	 */
	public ShippingGroupMapContainer getShippingGroupMapContainer() {
		return mShippingGroupMapContainer;
	}


	/**
	 * @param pShippingGroupMapContainer the shippingGroupMapContainer to set
	 */
	public void setShippingGroupMapContainer(
			ShippingGroupMapContainer pShippingGroupMapContainer) {
		mShippingGroupMapContainer = pShippingGroupMapContainer;
	}


	/**
	 * @return the cookieManager
	 */
	public TRUSOSCookieManager getCookieManager() {
		return mCookieManager;
	}


	/**
	 * @param pCookieManager the cookieManager to set
	 */
	public void setCookieManager(TRUSOSCookieManager pCookieManager) {
		mCookieManager = pCookieManager;
	}


	/**
	 * @return the employeeName
	 */
	public String getEmployeeName() {
		return mEmployeeName;
	}


	/**
	 * @param pEmployeeName the employeeName to set
	 */
	public void setEmployeeName(String pEmployeeName) {
		mEmployeeName = pEmployeeName;
	}


	/**
	 * @return the employeeNumber
	 */
	public String getEmployeeNumber() {
		return mEmployeeNumber;
	}


	/**
	 * @param pEmployeeNumber the employeeNumber to set
	 */
	public void setEmployeeNumber(String pEmployeeNumber) {
		mEmployeeNumber = pEmployeeNumber;
	}


	/**
	 * @return the storeNumber
	 */
	public String getStoreNumber() {
		return mStoreNumber;
	}


	/**
	 * @param pStoreNumber the storeNumber to set
	 */
	public void setStoreNumber(String pStoreNumber) {
		mStoreNumber = pStoreNumber;
	}


	/**
	 * @return the successURL
	 */
	public String getSuccessURL() {
		return mSuccessURL;
	}


	/**
	 * @param pSuccessURL the successURL to set
	 */
	public void setSuccessURL(String pSuccessURL) {
		mSuccessURL = pSuccessURL;
	}


	/**
	 * @return the errorURL
	 */
	public String getErrorURL() {
		return mErrorURL;
	}


	/**
	 * @param pErrorURL the errorURL to set
	 */
	public void setErrorURL(String pErrorURL) {
		mErrorURL = pErrorURL;
	}


	/**
	 * @return the logoutSuccessURL
	 */
	public String getLogoutSuccessURL() {
		return mLogoutSuccessURL;
	}


	/**
	 * @param pLogoutSuccessURL the logoutSuccessURL to set
	 */
	public void setLogoutSuccessURL(String pLogoutSuccessURL) {
		mLogoutSuccessURL = pLogoutSuccessURL;
	}


	/**
	 * This method is used to handle ValidateSosLogin  when user login in sos.
	 * 
	 * @param pRequest
	 *            - the request
	 * @param pResponse
	 *            - the response
	 * @throws ServletException
	 *             - ServletException
	 * @throws IOException
	 *             - IOException
	 * @return true, if successful
	 */
	public boolean handleValidateSosLogin(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUSOSValidationFormHnadler::@method::handleValidateSosLogin() : BEGIN");
		}
		// checking all kind of form possible exception can occur
		if(StringUtils.isBlank(getEmployeeName()) || (getEmployeeName().length() >TRUSOSConstants.HUNDRED)){
			addFormException(new DropletException(TRUSOSConstants.MESSAGE));
		}
		if(StringUtils.isBlank(getEmployeeNumber())){
			addFormException(new DropletException(TRUSOSConstants.MESSAGE));

		}else{
			if(!isNumeric(getEmployeeNumber()) || !(getEmployeeNumber().length() ==TRUSOSConstants.SEVEN)){
				addFormException(new DropletException(TRUSOSConstants.MESSAGE));
			}
		}
		if(StringUtils.isBlank(getStoreNumber()) ){
			addFormException(new DropletException(TRUSOSConstants.MESSAGE));
		}else {
			if(!isNumeric(getStoreNumber())|| !(getStoreNumber().length()==TRUSOSConstants.FOUR)){
				addFormException(new DropletException(TRUSOSConstants.MESSAGE));
			}
		}
		// if any exception occur redirecting to error page
		if(getFormError()){
			return  checkFormRedirect(null, getErrorURL(), pRequest, pResponse);
		}else{
			getCookieManager().setTRUSOSCookie(pRequest, pResponse,getEmployeeName(),getEmployeeNumber(),getStoreNumber());
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUSOSValidationFormHnadler::@method::handleValidateSosLogin() : END");
		}
		return checkFormRedirect(getSuccessURL(), getErrorURL(), pRequest, pResponse);
	}
	
	/**
	 * This method is used to handle Logout  when user Logout sos.
	 * 
	 * @param pRequest
	 *            - the request
	 * @param pResponse
	 *            - the response
	 * @throws ServletException
	 *             - ServletException
	 * @throws IOException
	 *             - IOException
	 * @return true, if successful
	 */
	public boolean handleLogout(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUSOSValidationFormHnadler::@method::handleSOSLogout() : BEGIN");
		}
		TransactionManager tm = getTransactionManager();
		TransactionDemarcation td = null;
		td = new TransactionDemarcation();
		try {
			td.begin(tm);
		//  at the time of SOS logout removing the cookie
			getCookieManager().removeIsSOSCookie(pRequest, pResponse);
			synchronized ((TRUOrderImpl) getShoppingCart().getCurrent()){
				getShippingGroupMapContainer().removeAllShippingGroups();
				getCommerceItemManager().removeAllCommerceItemsFromOrder(getShoppingCart().getCurrent());
				//Start :: Changes for TUW-57172
				getPaymentGroupGroupMapContainer().removeAllPaymentGroups();
				((TRUOrderImpl)getShoppingCart().getCurrent()).setEmail(TRUCheckoutConstants.EMPTY_STRING);
				((TRUOrderImpl)getShoppingCart().getCurrent()).setBillingAddress(null);
				((TRUOrderImpl)getShoppingCart().getCurrent()).setBillingAddressNickName(null);
				((TRUOrderImpl)getShoppingCart().getCurrent()).setBillingAddressSameAsShippingAddress(false);
				((TRUOrderImpl)getShoppingCart().getCurrent()).setRewardNumber(TRUCheckoutConstants.EMPTY_STRING);
				getProfileTools().updateProperty(TRUConstants.REWARD_NO,TRUCheckoutConstants.EMPTY_STRING,getProfileTools().getProfileItem(getShoppingCart().getCurrent().getProfileId()));
				getProfileTools().getOrderManager().updateOrder((TRUOrderImpl) getShoppingCart().getCurrent());
			}
		}
		catch (TransactionDemarcationException | CommerceException | RepositoryException exe) {
			if(isLoggingError()){
				vlogError("Exception occurs @Class::TRUSOSValidationFormHnadler::@method::handleSOSLogout() {0}", exe);
			}
		}finally {
			try {
				td.end();
			}
			catch (TransactionDemarcationException tde) {
				if(isLoggingError()){
					vlogError("TransactionDemarcationException occurs @Class::TRUSOSValidationFormHnadler::@method::handleSOSLogout() {0}", tde);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUSOSValidationFormHnadler::@method::handleSOSLogout() : END");
		}
		return checkFormRedirect(getLogoutSuccessURL(), null, pRequest, pResponse);
	}
	
	
	/**
	 * This method is used to check input value isNumeric or not.
	 * @param pCheckStringNumeric - String
	 * @return - true if input value isNumeric 
	 */
	private static boolean isNumeric(String pCheckStringNumeric)  
	{  
		String regex =TRUSOSConstants.REGEX;
		String checkNumeric = pCheckStringNumeric;
		return checkNumeric.matches(regex);
	}
	
	/** Property to hold mCommerceItemManager. */
	private TRUCommerceItemManager mCommerceItemManager;
	
	/**
	 * @param pCommerceItemManager the CommerceItemManager to set
	 */
	public void setCommerceItemManager(TRUCommerceItemManager pCommerceItemManager) {
	    this.mCommerceItemManager = pCommerceItemManager;
	}

	/**
	 * @return the commerceItemManager
	 */
	public TRUCommerceItemManager getCommerceItemManager() {
	    return this.mCommerceItemManager;
	}

	/** Property to hold ShoppingCart */
	private TRUOrderHolder mShoppingCart;
	
	/**
	 * @param pShoppingCart the ShoppingCart to set
	 */
	public void setShoppingCart(TRUOrderHolder pShoppingCart) {
	    this.mShoppingCart = pShoppingCart;
	}

	/**
	 * @return the shoppingCart
	 */
	public TRUOrderHolder getShoppingCart() {
	    return this.mShoppingCart;
	}
}
