/**
 * 
 */
package com.tru.common.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.TRUConfiguration;
import com.tru.common.TRUIntegrationConfiguration;
import com.tru.common.TRUSOSIntegrationConfiguration;
import com.tru.common.constants.TRUSOSConstants;
import com.tru.userprofiling.TRUSOSCookieManager;


/** This droplet is used to read all the service integration values which has been set from BCC and store in TRUSOSIntegrationConfiguration,
 *  here we are reading this value, using output keyword as  output parameter.
 * @author Professional Access
 * @version 1.0
 */
public class TRUSOSGetIntegrationConfigDroplet extends DynamoServlet {
	/**
	 * Holds the property value of mTruSOSIntegrationConfiguration
	 */	
	private TRUSOSIntegrationConfiguration mTruSOSIntegrationConfiguration;
	/**
	 * Holds the property value of mTruIntegrationConfiguration
	 */	
	private TRUIntegrationConfiguration mTruIntegrationConfiguration;

	
	/** Property to hold mCookieManager */
	private TRUSOSCookieManager mCookieManager;
	/**
	 * This property hold reference for mConfiguration.
	 */
	private TRUConfiguration mConfiguration;

	/**
	 * @param pCookieManager the cookieManager to set
	 */
	public void setCookieManager(TRUSOSCookieManager pCookieManager) {
		mCookieManager = pCookieManager;
	}
	
	/**
	 * @return the cookieManager
	 */
	public TRUSOSCookieManager getCookieManager() {
		return mCookieManager;
	}		
	
	/**
	 * @return the truSOSIntegrationConfiguration
	 */
	public TRUSOSIntegrationConfiguration getTruSOSIntegrationConfiguration() {
		return mTruSOSIntegrationConfiguration;
	}

	/**
	 * @param pTruSOSIntegrationConfiguration the truSOSIntegrationConfiguration to set
	 */
	public void setTruSOSIntegrationConfiguration(
			TRUSOSIntegrationConfiguration pTruSOSIntegrationConfiguration) {
		mTruSOSIntegrationConfiguration = pTruSOSIntegrationConfiguration;
	}

	/**
	 * @return the truIntegrationConfiguration
	 */
	public TRUIntegrationConfiguration getTruIntegrationConfiguration() {
		return mTruIntegrationConfiguration;
	}

	/**
	 * @param pTruIntegrationConfiguration the truIntegrationConfiguration to set
	 */
	public void setTruIntegrationConfiguration(
			TRUIntegrationConfiguration pTruIntegrationConfiguration) {
		mTruIntegrationConfiguration = pTruIntegrationConfiguration;
	}

	/**
	 * Returns the this property hold reference for TRUConfiguration.
	 * 
	 * @return the mConfiguration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * sets commerce TRUConfiguration.
	 * @param pConfiguration the TRUConfiguration to set
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		this.mConfiguration = pConfiguration;
	}
	
	/**
	 * This method is used to get the sos user details and populate in the header sections.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */

	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUSOSGetIntegrationConfigDroplet.service :: START");
		}
		boolean isEnableAddressDoctor = Boolean.FALSE;
		boolean isEnableCouponService = Boolean.FALSE;
		boolean isEnableEpslon = Boolean.FALSE;
		boolean isEnableHookLogic = Boolean.FALSE;
		boolean isEnablePowerReviews = Boolean.FALSE;
		boolean isSOS = getCookieManager().verifySOSCookie(pRequest);
		if(isSOS && getConfiguration().isEnableSOS()) {
			if (isLoggingDebug()) {
				logDebug("Inside SOS flow, and we are setting SOS specific service integration value");
			}
			isEnableAddressDoctor = getTruSOSIntegrationConfiguration().isEnableAddressDoctor();
			isEnableCouponService = getTruSOSIntegrationConfiguration().isEnableCouponService();
			isEnableEpslon = getTruSOSIntegrationConfiguration().isEnableEpslon();
			isEnableHookLogic = getTruSOSIntegrationConfiguration().isEnableHookLogic();
			isEnablePowerReviews = getTruSOSIntegrationConfiguration().isEnablePowerReviews();
		} else {
			if (isLoggingDebug()) {
				logDebug("Inside TRU flow, and we are setting TRU specific service integration value");
			}
			isEnableAddressDoctor = getTruIntegrationConfiguration().isEnableAddressDoctor();
			isEnableCouponService = getTruIntegrationConfiguration().isEnableCouponService();
			isEnableEpslon = getTruIntegrationConfiguration().isEnableEpslon();
			isEnableHookLogic = getTruIntegrationConfiguration().isEnableHookLogic();
			isEnablePowerReviews = getTruIntegrationConfiguration().isEnablePowerReviews();
		}
		// setting the request parameter
		pRequest.setParameter(TRUSOSConstants.IS_ENABLE_ADDRESS_DOCTOR, isEnableAddressDoctor);			
		pRequest.setParameter(TRUSOSConstants.IS_ENABLE_COUPON_SERVICE, isEnableCouponService);		
		pRequest.setParameter(TRUSOSConstants.IS_ENABLE_EPSLON, isEnableEpslon);		
		pRequest.setParameter(TRUSOSConstants.IS_ENABLE_HOOKLOGIC, isEnableHookLogic);
		pRequest.setParameter(TRUSOSConstants.IS_ENABLE_POWER_REVIEWS, isEnablePowerReviews);
		
		pRequest.serviceLocalParameter(TRUSOSConstants.OUTPUT_OPARAM, pRequest, pResponse);		
		if (isLoggingDebug()) {
			logDebug("Exiting  method TRUSOSGetIntegrationConfigDroplet.service :: END");
		}
	}
}
