/**
 * 
 */
package com.tru.common.droplet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.constants.TRUSOSConstants;
import com.tru.userprofiling.TRUSOSCookieManager;


/** This droplet is used to get the SOS User name and store number and display in the sosHeader. 
 * And check the user is SOS user or Store user if user is store user then we are setting output parameter as tru else sos.
 *  
 * @author Professional Access
 * @version 1.0
 */
public class TRUSOSHeaderInfoDroplet extends DynamoServlet {

	/** Property to hold mCookieManager */
	private TRUSOSCookieManager mCookieManager;
	
	/**
	 * This method is used to get the sos user details and populate in the header sections.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */

	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUSosUserDetailsDroplet.service :: START");
		}
		Map<String,String> sosUserInfoMap = null;
		boolean sosCookieVal = getCookieManager().verifySOSCookie(pRequest);
		if(!sosCookieVal) {
			pRequest.serviceLocalParameter(TRUSOSConstants.TRU_OPARAM, pRequest, pResponse);
			return;
		}		
		sosUserInfoMap = getCookieManager().getTRUSosCookie(pRequest);
		if(sosUserInfoMap == null  || sosUserInfoMap.isEmpty()) {
			pRequest.serviceLocalParameter(TRUSOSConstants.TRU_OPARAM, pRequest, pResponse);
			return;
		}
		// setting the SOS user details to request parameter
		pRequest.setParameter(TRUSOSConstants.SOS_EMP_NAME, sosUserInfoMap.get(TRUSOSConstants.SOS_EMPLOYEE_NAME_COOKIE));			
		pRequest.setParameter(TRUSOSConstants.IS_SOS, sosUserInfoMap.get(TRUSOSConstants.IS_SOS_COOKIE));		
		pRequest.setParameter(TRUSOSConstants.SOS_EMP_NUM, sosUserInfoMap.get(TRUSOSConstants.SOS_EMPLOYEE_NUMBER_COOKIE));		
		pRequest.setParameter(TRUSOSConstants.SOS_STORE_NUM, sosUserInfoMap.get(TRUSOSConstants.SOS_STORE_NUMBER_COOKIE));
		pRequest.serviceLocalParameter(TRUSOSConstants.SOS_OPARAM, pRequest, pResponse);		
		if (isLoggingDebug()) {
			logDebug("Exiting  method TRUSosUserDetailsDroplet.service :: END");
		}
	}

	/**
	 * @return the cookieManager
	 */
	public TRUSOSCookieManager getCookieManager() {
		return mCookieManager;
	}

	/**
	 * @param pCookieManager the cookieManager to set
	 */
	public void setCookieManager(TRUSOSCookieManager pCookieManager) {
		mCookieManager = pCookieManager;
	}

}
