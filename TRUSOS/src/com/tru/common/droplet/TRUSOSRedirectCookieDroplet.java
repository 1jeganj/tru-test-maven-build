package com.tru.common.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import atg.commerce.order.Order;
import atg.core.util.StringUtils;
import atg.multisite.SiteContextManager;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.cart.vo.TRUCartItemDetailVO;
import com.tru.common.TRUConfiguration;
import com.tru.common.constants.TRUSOSConstants;

/**
 * This class redirect registry user to shopping cart page during checkout.
 * @author PA
 * @version 1.0
 *
 */
public class TRUSOSRedirectCookieDroplet extends DynamoServlet{
	
	/** The store configuration. */
	private TRUConfiguration mStoreConfiguration;
	
	/** The Shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;
	
	/**
	 * This method set the value to redirect registry user during checkout to SOS checkout page with 
	 * required value.
	 * @param pRequest the  request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start: TRUSOSRedirectCookieDroplet :: service method ", pRequest);
		}
	String serverName=null;
	String sosEmpName = (String)pRequest.getQueryParameter(TRUSOSConstants.USER);
	String sosStoreNumber = (String)pRequest.getQueryParameter(TRUSOSConstants.STORE_ID);
	String domainName = (String)pRequest.getServerName();

	
	String siteId = SiteContextManager.getCurrentSiteId();
	String sosRedrictionUrl = (String)getStoreConfiguration().getSosCookieredirectionURL().get(siteId);
	if (StringUtils.isNotBlank(domainName)){
		serverName = domainName.substring(domainName.indexOf(TRUSOSConstants.DOT), domainName.length());
	}

	if (sosStoreNumber == null || StringUtils.isEmpty(sosStoreNumber)){
		sosStoreNumber = getStoreConfiguration().getDefaultSOSStoreNumber();
	}
	
	if(StringUtils.isEmpty(sosRedrictionUrl) || StringUtils.isEmpty(serverName) ||  StringUtils.isEmpty(sosEmpName) ||(sosStoreNumber !=null &&sosStoreNumber.length()!=TRUSOSConstants.FOUR )|| ! isNumeric(sosStoreNumber)){
		pResponse.sendRedirect(getStoreConfiguration().getSosErrorUrl());
		if (isLoggingDebug()) {
			vlogDebug("End: TRUSOSRedirectCookieDroplet :: service method and set Status 404");
		}
		return;
	}
	
	Cookie ck1 = new Cookie(TRUSOSConstants.SOS_EMPLOYEE_NAME_COOKIE,sosEmpName);
	Cookie ck2 = new Cookie(TRUSOSConstants.IS_SOS_COOKIE,TRUSOSConstants.BOOLEAN_TRUE);
	Cookie ck3 = new Cookie(TRUSOSConstants.SOS_STORE_NUMBER_COOKIE, sosStoreNumber);
	Cookie ck4 = new Cookie(TRUSOSConstants.SOS_EMPLOYEE_NUMBER_COOKIE,getStoreConfiguration().getDefaultsosEmpNumber());
	Cookie ck5 = new Cookie(TRUSOSConstants.IS_REDIRECT,TRUSOSConstants.BOOLEAN_TRUE);
	
	ck1.setPath(TRUCommerceConstants.PATH_SEPERATOR);
	ck1.setDomain(serverName);
		
	ck2.setPath(TRUCommerceConstants.PATH_SEPERATOR);
	ck2.setDomain(serverName);

	ck3.setPath(TRUCommerceConstants.PATH_SEPERATOR);
	ck3.setDomain(serverName);
	
	ck4.setPath(TRUCommerceConstants.PATH_SEPERATOR);
	ck4.setDomain(serverName);
	ck5.setPath(TRUCommerceConstants.PATH_SEPERATOR);
	ck5.setDomain(serverName);
	
	
	pResponse.addCookie(ck2);
	pResponse.addCookie(ck1);
	pResponse.addCookie(ck3);
	pResponse.addCookie(ck4);
	pResponse.addCookie(ck5);
	
	if (isLoggingDebug()) {
		vlogDebug("Employee Name {0} ::Store Number {1} :: Checkout Url {2} :: Server Name {3}", sosEmpName,sosStoreNumber,sosRedrictionUrl,serverName);
	}
	
		if(StringUtils.isNotBlank(sosEmpName)){
			Order order = (Order) pRequest.getObjectParameter(TRUCommerceConstants.PARAM_ORDER);
			List<TRUCartItemDetailVO> cartISPUItems = new ArrayList<TRUCartItemDetailVO>();
			if (order != null && !order.getCommerceItems().isEmpty()) {
				List<TRUCartItemDetailVO> cartItems = getShoppingCartUtils().getItemDetailVOsByRelationShips(order, null,
						true, getShoppingCartUtils().getLocationIdFromCookie(pRequest),false);
				if(cartItems != null && !cartItems.isEmpty()){
					for (TRUCartItemDetailVO cartItemDetailVO : cartItems) {
						if(cartItemDetailVO.getInventoryInfoMessage() != null){
							cartISPUItems.add(cartItemDetailVO);
						}
					}
				}
			}
			if(cartISPUItems != null && !cartISPUItems.isEmpty()){
				pRequest.setParameter(TRUCommerceConstants.ITEMS, cartISPUItems);
				pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
			}
			else {
				pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
				pResponse.sendRedirect(sosRedrictionUrl,true);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End: TRUSOSRedirectCookieDroplet :: service method ");
		}
	}
	
	/**
	 * This method will domain url for current site.
	 *
	 * @param pCheckStringNumeric the check string numeric
	 * @return String siteProductUrl
	 */
	private static boolean isNumeric(String pCheckStringNumeric)  
	{  
		String regex =TRUSOSConstants.REGEX;
		String checkNumeric = pCheckStringNumeric;
		return checkNumeric.matches(regex);
	}
	
	
	/**
	 * Gets the store configuration.
	 *
	 * @return the store configuration
	 */
	public TRUConfiguration getStoreConfiguration() {
		return mStoreConfiguration;
	}

	/**
	 * Sets the store configuration.
	 * 
	 * @param pStoreConfiguration
	 *            the new store configuration.
	 */
	public void setStoreConfiguration(TRUConfiguration pStoreConfiguration) {
		this.mStoreConfiguration = pStoreConfiguration;
	}
	/**
	 * Gets the shopping cart utils.
	 *
	 * @return the shopping cart utils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}


	/**
	 * Sets the shopping cart utils.
	 *
	 * @param pShoppingCartUtils the new shopping cart utils
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		mShoppingCartUtils = pShoppingCartUtils;
	}
}
