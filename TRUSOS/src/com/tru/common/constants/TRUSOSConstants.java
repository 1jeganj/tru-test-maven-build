package com.tru.common.constants;

import atg.nucleus.naming.ParameterName;

/**
 * This class holds all the constants.
 * @author Professional Access
 * @version 1.0
 */
public class TRUSOSConstants {
	
	/** The Constant USER. */

	public static final String USER = "user";

	/** The Constant Is_SOS. */

	public static final String IS_SOS = "isSOS";

	/** The Constant Is_SOS_VALUE. */

	public static final String IS_SOS_VALUE = "true";
	/** The Constant IS_SOS_FALSE_VALUE. */

	public static final String IS_SOS_FALSE_VALUE = "false";

	/** The Constant SOS_TOYSRUS_URL. */
	public static final String SOS_TOYSRUS_URL = "aos.toysrus.com";

	/** The Constant SOS_BABYSRUS_URL. */
	public static final String SOS_BABYSRUS_URL = "aos.babysrus.com";

	/** The Constant LOCAL_URL. */
	public static final String LOCAL_URL = "tru/sos/";

	/** The Constant HUNDRED. */
	public static final int HUNDRED = 100;

	/** The Constant SEVEN. */
	public static final int SEVEN = 7;

	/** The Constant FOUR. */
	public static final int FOUR = 4;

	/** The Constant REGEX. */
	public static final String REGEX = "\\d+";

	/** The Constant MESSAGE. */
	public static final String MESSAGE = "Please check the values entered";
	/** The Constant SOS_EMP_NAME. */
	public static final String SOS_EMP_NAME = "SosEmpName";
	/** The Constant SOS_EMP_NUM. */
	public static final String SOS_EMP_NUM = "SosEmpNum";
	/** The Constant SOS_STORE_NUM. */
	public static final String SOS_STORE_NUM = "SosStoreNum";
	/** The Constant SOS_STORE_NUM. */
	public static final String STORE_ID = "store";
	/** Constant to hold TRU_OPARAM. */
	public static final ParameterName TRU_OPARAM = ParameterName.getParameterName("tru");
	
	/** Constant to hold SOS_OPARAM. */
	public static final ParameterName SOS_OPARAM = ParameterName.getParameterName("sos");
	
	/** Constant to hold OUTPUT_OPARAM. */
	public static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
	
	/** Constant to hold PATH. */
	public static final String PATH = "/";
	/** Constant to hold IS_ENABLE_ADDRESS_DOCTOR. */
	public static final String IS_ENABLE_ADDRESS_DOCTOR = "isEnableAddressDoctor";
	/** Constant to hold IS_ENABLE_COUPON_SERVICE. */
	public static final String IS_ENABLE_COUPON_SERVICE = "isEnableCouponService";
	/** Constant to hold IS_ENABLE_EPSLON. */
	public static final String IS_ENABLE_EPSLON = "isEnableEpslon";
	/** Constant to hold IS_ENABLE_HOOKLOGIC. */
	public static final String IS_ENABLE_HOOKLOGIC = "isEnableHookLogic";
	/** Constant to hold IS_ENABLE_POWER_REVIEWS. */
	public static final String IS_ENABLE_POWER_REVIEWS = "isEnablePowerReviews";
	/** Constant to hold BOOLEAN_TRUE. */
	public static final String BOOLEAN_TRUE = "true";
	/** Constant to hold SOS_EMPLOYEE_NAME_COOKIE. */
	public static final String SOS_EMPLOYEE_NAME_COOKIE = "sosEmpName";
	/** Constant to hold SOS_EMPLOYEE_NUMBER_COOKIE. */
	public static final String SOS_EMPLOYEE_NUMBER_COOKIE = "sosEmpNumber";
	/** Constant to hold SOS_STORE_NUMBER_COOKIE. */
	public static final String SOS_STORE_NUMBER_COOKIE = "sosStoreNumber";
	/** Constant to hold IS_SOS_COOKIE. */
	public static final String IS_SOS_COOKIE = "isSOS";
	/** Constant to hold SOS. */
	public static final String SOS = "sos";
	/** Constant to hold DOT. */
	public static final String DOT = ".";
	/** The Constant EMPTY_STRING. */
	public static final String EMPTY_STRING = "";
	/** Constant for INT_FOUR_ZERO_FOUR. */
	public static final int INT_FOUR_ZERO_FOUR = 404;
	/** Constant for serverNametoysrus. */
	public static final String serverNametoysrus=".toysrus.com";
	/** Constant for serverNamebabiessrus. */
	public static final String serverNamebabiessrus=".babiesrus.com";
	
	/** Constant for IsRedirect. */
	public static final String IS_REDIRECT="isRedirect";
	

}
