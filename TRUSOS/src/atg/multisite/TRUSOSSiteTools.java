package atg.multisite;

import java.util.List;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;

import com.tru.common.TRUSOSIntegrationConfiguration;
import com.tru.integrations.TRUSOSIntegrationPropertiesConfig;

/**
 * This will get CouponService, AddressDoctor, Epslon, HookLogic and PowerReviews BOOLEAN true false vale.
 * and set it for dynamic integration enabling or disabling. Give the same names in BCC Site Repository 
 * which are CouponService, AddressDoctor, Epslon, HookLogic and PowerReviews.
 * Update above comment whenever new integrations are added
 * @author PA
 * @version 1.0
 */

public class TRUSOSSiteTools extends TRUSiteTools {
	/** Holds the Constant of mTruSOSIntegrationConfiguration. */
	private TRUSOSIntegrationConfiguration mTruSOSIntegrationConfiguration;
	
	/**
	 * @return the truSOSIntegrationConfiguration
	 */
	public TRUSOSIntegrationConfiguration getTruSOSIntegrationConfiguration() {
		return mTruSOSIntegrationConfiguration;
	}
	
	/**
	 * @param pTruSOSIntegrationConfiguration the truSOSIntegrationConfiguration to set
	 */
	public void setTruSOSIntegrationConfiguration(TRUSOSIntegrationConfiguration pTruSOSIntegrationConfiguration) {
		mTruSOSIntegrationConfiguration = pTruSOSIntegrationConfiguration;
	}

	/**
	 * This method will read the the service name and value from repository and update those value to TRUSOSIntegrationConfiguration.
	 *  @param pSiteItem -RepositoryItem
	 */
	
	@SuppressWarnings("unchecked")
	public void updateIntegrations(RepositoryItem pSiteItem) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUSOSSiteTools.updateIntegrations()");
		}
		// this super call to update the integration value for TRUSiteTools
		super.updateIntegrations(pSiteItem);		
		if(pSiteItem == null) {
			if (isLoggingDebug()) {
				logDebug("Site item is empty -- >"  + pSiteItem);
			}
			return;
		} 
		Boolean enabled = null;
		String integrationName = null;
		List<RepositoryItem> sosIntegrations =  (List<RepositoryItem>) pSiteItem.getPropertyValue(((TRUSOSIntegrationPropertiesConfig) getIntegrationPropertiesConfig()).getSosIntegrationsPropertyName());
		if(sosIntegrations != null && !sosIntegrations.isEmpty()) {			
			for(RepositoryItem sosIntegration : sosIntegrations) {
				integrationName = (String) sosIntegration.getPropertyValue(getIntegrationPropertiesConfig().getIntegrationNamePropertyName());
				if(StringUtils.isBlank(integrationName)) {
					continue;
				}
				//if enabled value is not null then only continuing the flow and read the integration value from site Repository
				enabled = (Boolean) sosIntegration.getPropertyValue(getIntegrationPropertiesConfig().getEnabledPropertyName());
				if(enabled == null) {
					continue;
				}				
				if (isLoggingDebug()) {
					logDebug(">>>> SOS IntegrationName:: "+integrationName+"  enabled:: " + enabled);
				}
				if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getCouponServiceIntegrationName())) {
					getTruSOSIntegrationConfiguration().setEnableCouponService(enabled.booleanValue());					
				}else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getAddressDoctorIntegrationName())) {
					getTruSOSIntegrationConfiguration().setEnableAddressDoctor(enabled.booleanValue());
				}else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getEpslonIntegrationName())) {
					getTruSOSIntegrationConfiguration().setEnableEpslon(enabled.booleanValue());
				}else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getHookLogicIntegrationName())) {
					getTruSOSIntegrationConfiguration().setEnableHookLogic(enabled.booleanValue());	
				}else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getPowerReviewsIntegrationName())) {
					getTruSOSIntegrationConfiguration().setEnablePowerReviews(enabled.booleanValue());
				}else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getTaxwareIntegrationName())) {
					getTruSOSIntegrationConfiguration().setEnableTaxware(enabled.booleanValue());
				}else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getCertonaIntegrationName())) {
					getTruSOSIntegrationConfiguration().setEnableCertona(enabled.booleanValue());
				}else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getCardinalIntegrationName())) {
					getTruSOSIntegrationConfiguration().setEnableCardinal(enabled.booleanValue());
				} else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getRadialCreditCardIntegrationName())) {
					getTruSOSIntegrationConfiguration().setEnableRadialCreditCard(enabled.booleanValue());
				} else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getRadialGiftCardIntegrationName())) {
					getTruSOSIntegrationConfiguration().setEnableRadialGiftCard(enabled.booleanValue());
				} else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getRadialPayPalIntegrationName())) {
					getTruSOSIntegrationConfiguration().setEnableRadialPayPal(enabled.booleanValue());
				} else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getRadialApplePayIntegrationName())) {
					getTruSOSIntegrationConfiguration().setEnableRadialApplePay(enabled.booleanValue());
				} else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getRadialSoftAllocationIntegrationName())) {
					getTruSOSIntegrationConfiguration().setEnableRadialSoftAllocation(enabled.booleanValue());
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUSOSSiteTools.updateIntegrations()");
		}
	}
}