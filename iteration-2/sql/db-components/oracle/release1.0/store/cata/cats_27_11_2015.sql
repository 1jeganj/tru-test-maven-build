DROP TABLE tru_qualifying_prod;
DROP TABLE tru_targeted_prod;
DROP TABLE tru_qualifying_category;
DROP TABLE tru_targeted_category;

create table tru_qualifying_prod (
	promotion_id	varchar2(40)	not null,
	sequence_num	integer	not null,
	product_id	varchar2(40)	not null
,constraint tru_qualifying_prod_p primary key (promotion_id,sequence_num)
,constraint tru_qualifyingprod_d_f foreign key (promotion_id) references dcs_promotion (promotion_id)
,constraint tru_qualifying_prod_d_f foreign key (product_id) references dcs_product (product_id));

create table tru_targeted_prod (
	promotion_id	varchar2(40)	not null,
	sequence_num	integer	not null,
	product_id	varchar2(40)	not null
,constraint tru_targeted_prod_p primary key (promotion_id,sequence_num)
,constraint tru_targetedprod_d_f foreign key (promotion_id) references dcs_promotion (promotion_id)
,constraint tru_targeted_prod_d_f foreign key (product_id) references dcs_product (product_id));





create table tru_qualifying_category (
	promotion_id	varchar2(40)	not null,
	sequence_num	integer	not null,
	category_id	varchar2(40)	not null
,constraint tru_qualifying_category_p primary key (promotion_id,sequence_num)
,constraint tru_qualifyingcategory_d_f foreign key (promotion_id) references dcs_promotion (promotion_id)
,constraint tru_qualifying_category_d_f foreign key (category_id) references dcs_category (category_id));



create table tru_targeted_category (
	promotion_id	varchar2(40)	not null,
	sequence_num	integer	not null,
	category_id	varchar2(40)	not null
,constraint tru_targeted_category_p primary key (promotion_id,sequence_num)
,constraint tru_targetedcategory_d_f foreign key (promotion_id) references dcs_promotion (promotion_id)
,constraint tru_targeted_category_d_f foreign key (category_id) references dcs_category (category_id));
