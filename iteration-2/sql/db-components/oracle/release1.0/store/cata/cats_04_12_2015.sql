drop table tru_shipping_discount_promo;

create table tru_shipping_discount_promo(
	promotion_id varchar2(40) not null,
	order_limit number(19,2) null,
	CONSTRAINT tru_shipping_discount_promo_P PRIMARY KEY(promotion_id)
);

alter table tru_site_configuration add (free_ship_promo varchar2(40));