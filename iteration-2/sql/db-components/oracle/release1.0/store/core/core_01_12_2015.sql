-- START added changes for Out Of Stock items.
drop table tru_order_out_of_stock_item;
drop table tru_out_of_stock_item;

CREATE TABLE tru_out_of_stock_item (
	id 			varchar2(254)	NOT NULL,
	catalog_ref_id 		varchar2(254)	NULL,
	product_id 		varchar2(254)	NULL,
	quantity 		number(19, 0)	NULL,
	location_id 		varchar2(254)	NULL,
	list_price 		number(28, 20)	NULL,
	sale_price 		number(28, 20)	NULL,
	total_saved_amount 	number(28, 20)	NULL,
	total_saved_percentage 	number(28, 20)	NULL,
	channel_name 		varchar2(254)	NULL,
	channel_id 		varchar2(254)	NULL,
	channel_user_name 	varchar2(254)	NULL,
	channel_co_user_name 	varchar2(254)	NULL,
	PRIMARY KEY(id)
);
CREATE TABLE tru_order_out_of_stock_item (
	order_id 		varchar2(254)	NOT NULL REFERENCES dcspp_order(order_id),
	sequence_num 		INTEGER	NOT NULL,
	out_of_stock_items 	varchar2(254)	NULL REFERENCES tru_out_of_stock_item(id),
	PRIMARY KEY(order_id, sequence_num)
);
-- END added changes for Out Of Stock items.

drop table tru_commerce_item;