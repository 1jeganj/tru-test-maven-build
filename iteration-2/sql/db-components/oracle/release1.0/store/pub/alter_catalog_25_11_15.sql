alter table tru_classification drop column COLLECTION_IMAGE;
drop table tru_collection_image;
CREATE TABLE tru_collection_image (
	product_id 		varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	collection_image 	varchar2(255)	NULL,
	PRIMARY KEY(product_id, asset_version)
);

DROP TABLE TRU_LOCATION_SERVICES;
DROP TABLE TRU_STORE_SERVICES;

CREATE TABLE tru_store_services 
(
asset_version    number(19)        not null,
workspace_id    varchar2(40)       not null,
branch_id            varchar2(40)       not null,
is_head                number(1,0)      not null,
version_deleted              number(1,0)      not null,
version_editable number(1,0)   not null,
pred_version     number(19,0),
checkin_date timestamp (6),
id  varchar2(254) not null,
service_name  varchar2(254) NULL,
service_description CLOB NULL, 
LEARN_MORE  CLOB NULL, 
PRIMARY KEY  (id,asset_version)
);


CREATE TABLE tru_location_services (
                location_id           varchar2(254)      NOT NULL ,
                asset_version          INTEGER              NOT NULL,
                SEQUENCE_NUMBER        INTEGER              NOT NULL,
                service_id             varchar2(254)      ,
                PRIMARY KEY(location_id, asset_version,SEQUENCE_NUMBER)
);