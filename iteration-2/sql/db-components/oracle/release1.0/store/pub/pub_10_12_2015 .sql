drop table tru_gift_finder_stack_list;
CREATE TABLE tru_gift_finder_stack_list (
	id 			varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	gift_finder_stack_list 	varchar2(254)	NULL,
	PRIMARY KEY(id, asset_version, sequence_num)
);

CREATE INDEX tru_gift_finder_stack_list_idx ON tru_gift_finder_stack_list(id, asset_version, sequence_num);