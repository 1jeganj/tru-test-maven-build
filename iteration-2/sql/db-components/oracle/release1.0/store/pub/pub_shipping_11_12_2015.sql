--drop table TRU_ZIPCODE_RESTRICTIONS;
--drop table TRU_SHIPMETHOD_REGION_RATES;
--drop table TRU_SHIP_RATES;
--drop table TRU_REGIONS;
--drop table TRU_SHIP_METH_NOT_ALLOWED;
--drop table TRU_SHIP_METH_ALLOWED;
--drop table TRU_SHIP_METHODS;
--drop table TRU_FREIGHT_SHIP_METHODS;
--drop table TRU_FREIGHT_CLASS;

CREATE TABLE TRU_ZIPCODE_RESTRICTIONS(
	ASSET_VERSION	NUMBER(19)		NOT NULL,
	WORKSPACE_ID	VARCHAR2(40)	NOT NULL,
	BRANCH_ID	VARCHAR2(40)		NOT NULL,
	IS_HEAD	NUMBER(1,0)				NOT NULL,
	VERSION_DELETED	NUMBER(1,0)		NOT NULL,
	VERSION_EDITABLE NUMBER(1,0) 	NOT NULL,
	PRED_VERSION	NUMBER(19,0)			,
	CHECKIN_DATE TIMESTAMP (6)				, 
	ID 			VARCHAR2(254)		NOT NULL,
 	SKU_ID VARCHAR2(20 BYTE) 		NOT NULL,
 	ZIP_CODE VARCHAR2(20 BYTE) 		NOT NULL, 
 	ERROR_MESSAGE 		CLOB		NOT NULL,
 	CONSTRAINT TRU_ZIPCODE_RESTRI_INFO_P PRIMARY KEY (ID,ASSET_VERSION)
);


CREATE TABLE TRU_FREIGHT_CLASS (
				ASSET_VERSION	number(19)	not null,
				WORKSPACE_ID	varchar2(40)	not null,
				BRANCH_ID	varchar2(40)	not null,
				IS_HEAD	number(1,0)	not null,
				VERSION_DELETED	number(1,0)	not null,
				VERSION_EDITABLE number(1,0)	not null,
				PRED_VERSION	number(19,0)	,
				CHECKIN_DATE timestamp (6), 
                FREIGHT_CLASS_ID           varchar2(254)      NOT NULL,
                FREIGHT_CLASS_NAME    INTEGER               NULL,
                PRIMARY KEY(FREIGHT_CLASS_ID,ASSET_VERSION)
);
CREATE TABLE TRU_SHIP_METHODS (

				ASSET_VERSION	number(19)	not null,
				WORKSPACE_ID	varchar2(40)	not null,
				BRANCH_ID	varchar2(40)	not null,
				IS_HEAD	number(1,0)	not null,
				VERSION_DELETED	number(1,0)	not null,
				VERSION_EDITABLE number(1,0)	not null,
				PRED_VERSION	number(19,0)	,
				CHECKIN_DATE timestamp (6), 
                SHIP_METH_ID                   varchar2(254)      NOT NULL,
                SHIP_METH_CODE                            varchar2(254)      NOT NULL,
                SHIP_METH_NAME                           varchar2(254)      NOT NULL,
                SHIP_METH_DETAILS       varchar2(2500)      NULL,
                SHIP_FULFILLMENT_MSG                varchar2(254)      NULL,
                IS_ACTIVE                             INTEGER               NULL,
                PRIMARY KEY(SHIP_METH_ID,asset_version)
);
CREATE TABLE TRU_FREIGHT_SHIP_METHODS (
                FREIGHT_CLASS_ID           varchar2(254)      NOT NULL ,
                ASSET_VERSION                    INTEGER              NOT NULL,
                SEQUENCE_NUM                                INTEGER              NOT NULL,
                SHIPPING_METHOD                varchar2(254)      ,
                PRIMARY KEY(FREIGHT_CLASS_ID, SHIPPING_METHOD,ASSET_VERSION,SEQUENCE_NUM)
);

CREATE TABLE TRU_SHIP_METH_ALLOWED (
                SHIP_METH_ID                   varchar2(254)      NOT NULL ,
                ASSET_VERSION                    INTEGER              NOT NULL,
                SEQUENCE_NUM                                INTEGER              NOT NULL,
                STATE_CODE                       varchar2(254)      NOT NULL,
                PRIMARY KEY(SHIP_METH_ID, STATE_CODE,ASSET_VERSION,SEQUENCE_NUM)
);

CREATE TABLE TRU_SHIP_METH_NOT_ALLOWED (
                SHIP_METH_ID                   varchar2(254)      NOT NULL ,
                asset_version                    INTEGER              NOT NULL,
                sequence_num                                INTEGER              NOT NULL,
                STATE_CODE                       varchar2(254)      NOT NULL,
                PRIMARY KEY(SHIP_METH_ID, STATE_CODE,ASSET_VERSION,SEQUENCE_NUM)
);

CREATE TABLE TRU_REGIONS (
				ASSET_VERSION	number(19)	not null,
				WORKSPACE_ID	varchar2(40)	not null,
				BRANCH_ID	varchar2(40)	not null,
				IS_HEAD	number(1,0)	not null,
				VERSION_DELETED	number(1,0)	not null,
				VERSION_EDITABLE number(1,0)	not null,
				PRED_VERSION	number(19,0)	,
				CHECKIN_DATE timestamp (6), 
                SHIP_REGION_ID                               varchar2(254)      NOT NULL,
                REGION_CODE                    varchar2(254)      NOT NULL,
                REGION_NAME                  varchar2(254)      NOT NULL,
                PRIMARY KEY(SHIP_REGION_ID,ASSET_VERSION)
);

CREATE TABLE TRU_SHIP_RATES (
				ASSET_VERSION	number(19)	not null,
				WORKSPACE_ID	varchar2(40)	not null,
				BRANCH_ID	varchar2(40)	not null,
				IS_HEAD	number(1,0)	not null,
				VERSION_DELETED	number(1,0)	not null,
				VERSION_EDITABLE number(1,0)	not null,
				PRED_VERSION	number(19,0)	,
				CHECKIN_DATE timestamp (6), 
                SHIP_RATE_ID                    varchar2(254)      NOT NULL,
                SHIP_RATE_NAME                             varchar2(254)      NULL,
                LOWER_ORDER_VALUE   number(28, 20)   NULL,
                UPPER_ORDER_VALUE    number(28, 20)   NULL,
                DESCRIPTION                      varchar2(254)      NULL,
                SHIP_PRICE                          number(28, 20)   NULL,
                PRIMARY KEY(SHIP_RATE_ID,ASSET_VERSION)
);

CREATE TABLE TRU_SHIPMETHOD_REGION_RATES (
				ASSET_VERSION	number(19)	not null,
				WORKSPACE_ID	varchar2(40)	not null,
				BRANCH_ID	varchar2(40)	not null,
				IS_HEAD	number(1,0)	not null,
				VERSION_DELETED	number(1,0)	not null,
				VERSION_EDITABLE number(1,0)	not null,
				PRED_VERSION	number(19,0)	,
				CHECKIN_DATE timestamp (6), 
                SHIP_REGION_ID                               varchar2(254)      NOT NULL,
                SHIP_METH_ID                   varchar2(254)      NOT NULL,
                SHIP_RATE_ID                    varchar2(254)      NOT NULL,
                PRIMARY KEY(SHIP_REGION_ID, SHIP_METH_ID, SHIP_RATE_ID,ASSET_VERSION)
);
