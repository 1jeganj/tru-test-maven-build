drop table tru_size;

drop table tru_color;

CREATE TABLE tru_color (
	color_id 		varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	color_name 		varchar2(254)	NULL,
	color_Display_Sequence 	varchar2(254)	NULL,
	version_deleted 	number(1)	NULL,
	version_editable 	number(1)	NULL,
	pred_version 		INTEGER	NULL,
	workspace_id 		varchar2(254)	NULL,
	branch_id 		varchar2(254)	NULL,
	is_head 		number(1)	NULL,
	checkin_date 		DATE	NULL,
	CHECK (version_deleted IN (0, 1)),
	CHECK (version_editable IN (0, 1)),
	CHECK (is_head IN (0, 1)),
	PRIMARY KEY(color_id, asset_version)
);

CREATE TABLE tru_size (
	size_id 		varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	size_Description 	varchar2(254)	NULL,
	size_Display_Sequence 	varchar2(254)	NULL,
	version_deleted 	number(1)	NULL,
	version_editable 	number(1)	NULL,
	pred_version 		INTEGER	NULL,
	workspace_id 		varchar2(254)	NULL,
	branch_id 		varchar2(254)	NULL,
	is_head 		number(1)	NULL,
	checkin_date 		DATE	NULL,
	CHECK (version_deleted IN (0, 1)),
	CHECK (version_editable IN (0, 1)),
	CHECK (is_head IN (0, 1)),
	PRIMARY KEY(size_id, asset_version)
);