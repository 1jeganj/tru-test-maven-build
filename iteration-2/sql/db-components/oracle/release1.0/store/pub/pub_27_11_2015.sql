DROP TABLE tru_qualifying_prod;
DROP TABLE tru_targeted_prod;
DROP TABLE tru_qualifying_category;
DROP TABLE tru_targeted_category;

create table tru_qualifying_prod (
	promotion_id	varchar2(40)	not null,
	sequence_num	integer	not null,
	product_id	varchar2(40)	not null,
	asset_version number(19,0) not null enable
,constraint tru_qualifying_prod_p primary key (promotion_id,sequence_num,asset_version));

create table tru_targeted_prod (
	asset_version number(19,0) not null enable,
	promotion_id	varchar2(40)	not null,
	sequence_num	integer	not null,
	product_id	varchar2(40)	not null
,constraint tru_targeted_prod_p primary key (promotion_id,sequence_num,asset_version));



create table tru_qualifying_category (
	asset_version number(19,0) not null enable,
	promotion_id	varchar2(40)	not null,
	sequence_num	integer	not null,
	category_id	varchar2(40)	not null
,constraint tru_qualifying_category_p primary key (promotion_id,sequence_num,asset_version));



create table tru_targeted_category (
	asset_version number(19,0) not null enable,
	promotion_id	varchar2(40)	not null,
	sequence_num	integer	not null,
	category_id	varchar2(40)	not null
,constraint tru_targeted_category_p primary key (promotion_id,sequence_num,asset_version));





