drop table tru_gift_finder_stack_list;
CREATE TABLE tru_gift_finder_stack_list (
          id                          varchar2(254)      NOT NULL REFERENCES site_configuration(id),
          sequence_num              INTEGER   NOT NULL,
          gift_finder_stack_list      varchar2(254)      NULL REFERENCES TRUCORE_QC.tru_giftFinderStackItemList(id),
          PRIMARY KEY(id, sequence_num)
);

CREATE INDEX tru_gift_finder_stack_list_idx ON tru_gift_finder_stack_list(id, sequence_num);