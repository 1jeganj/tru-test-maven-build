-- drop table ral_errorhandler_error_xlate;
-- drop table ral_errorhandler_xlate;
-- drop table ral_errorhandler;

CREATE TABLE ral_errorhandler ( 
	error_id varchar(40) NOT NULL,
	error_key varchar(254) NOT NULL,
	error_message varchar(4000) NULL,
	constraint ral_errorhandler_p PRIMARY KEY(error_id,error_key) );
  
  CREATE TABLE ral_errorhandler_xlate ( 
	translation_id varchar(254) NOT NULL, 
	error_message varchar(4000) NULL, 
	constraint ral_errorhandler_xlate_p PRIMARY KEY(translation_id) );

 CREATE TABLE ral_errorhandler_error_xlate ( 
	error_id varchar(40) NOT NULL, 
	locale varchar(254) NOT NULL, 
	translation_id varchar(254) NOT NULL,
	constraint ral_errorhandler_error_xlate_p primary key (error_id,locale),
	constraint ral_errorhandler_error_xlate_f foreign key (translation_id) references ral_errorhandler_xlate (translation_id));

CREATE INDEX ral_errorhandler_error_xlate_error_idx ON ral_errorhandler_error_xlate(error_id, locale);