package com.tru.errorhandler.fhl;

import java.text.MessageFormat;
import java.util.List;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.droplet.DropletFormException;
import atg.droplet.GenericFormHandler;
import atg.nucleus.servlet.ServletService;

import com.tru.common.cml.AbstractComponent;
import com.tru.common.cml.ApplicationException;
import com.tru.common.cml.Constants;
import com.tru.common.cml.IErrorId;
import com.tru.common.cml.SystemException;
import com.tru.common.cml.ValidationError;
import com.tru.common.cml.ValidationExceptions;
import com.tru.errorhandler.mgl.IErrorHandlerManager;

/**
 * This Class implements IErrorHandler interface and implements all the process methods.
 * This class is used to handle ValidationExceptions, SystemExceptions, and ApplicationExceptions.
 * 
 * @author Professional Access
 * @version 1.0
 */

public class DefaultErrorHandlerImpl extends AbstractComponent implements IErrorHandler {
	
	/** Holds the Constant of ErrorHandlerManager. */
	private IErrorHandlerManager mErrorHandlerManager;

	/**
	 * Gets the error handler manager.
	 *
	 * @return mErrorHandlerManager
	 */
	public IErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}
	
	/**
	 * Sets the error handler manager.
	 *
	 * @param pErrorHandlerManager ErrorHandlerManager
	 */
	public void setErrorHandlerManager(IErrorHandlerManager pErrorHandlerManager) {
		this.mErrorHandlerManager = pErrorHandlerManager;
	}
	
	/**
	 * No Initialization is required for this class. So providing Empty initComponent() method.
	 * @throws SystemException SystemException
	 */
	protected void initComponent() throws SystemException {
		if(isLoggingDebug()){
			logDebug("DefaultErrorHandlerImpl - empty initComponent()");
		}
	}
	
	/**
	 * This method is used to process Validation Exceptions. Iterate through the ValidationExceptions and get the ValidationError.
	 * Get the ErrorId from the VaidationError and pas it to ErrorHandlerManager class to get ErrorMessage from repository.
	 * This errorMessage is added to the FormExcetions using Formhandler's addFormException method.
	 * This method uses MessageFomat.format method to format the ErrorMessage.
	 * Ex: If the ErrorMessage from repository is 'invalid data for {0}'. MessageFormat.format method replaces the '{0}' with proper field name like 
	 * 'Invalid data for First Name'
	 * 
	 * @param pException ValidationExceptions
	 * @param pHandler GenericFormHandler
	 * @throws ServletException Exception
	 */
	@Override
	public void processException(ValidationExceptions pException,GenericFormHandler pHandler)throws ServletException {
		if(isLoggingDebug()){
			logDebug("DefaultErrorHandlerImpl - (processValidationException-[pException,pHandler]):Start");
		}
		//If pException or 	pHandler is null throw System Exception
		if(pException == null || pHandler == null){
			throw new ServletException(IErrorId.NO_EXCEPTION_OR_HANDLER);
		}
		//Get ValidationError from pException
		List<ValidationError> validationErrors = pException.getValidationErrors();
		String errorMessage = Constants.CONSTANT_EMPTY;
		//Iterate through all validation errors and get Error id.
		if(validationErrors != null && !validationErrors.isEmpty()){
			for(ValidationError error: validationErrors){
				String errorId = error.getErrorId();
				if(isLoggingDebug()){
					logDebug("ErrorId from ValidationError is: " + errorId);
				}
				try{
					//call the ErrorHandlerManager's getErrorMessage() method.
					errorMessage = getErrorHandlerManager().getErrorMessage(errorId);
				} catch(SystemException se){
					throw new ServletException(se.getErrorId(), se);
				}
				if(isLoggingDebug()){
					logDebug("ErrorMessage is: " + errorMessage);
				}
				String propertyPath  = generatePropetyPath(error.getMessageContext());
				//If ErrorMessage is not null add it to FormExceptions.
				if(!StringUtils.isBlank(errorMessage)){
					//ValidationError.getMessageContext() will give field Name like First Name
					errorMessage = errorMessage.replaceAll(Constants.SOURCE_STRING, Constants.DESTINATION_STRING);
					pHandler.addFormException(new DropletFormException(MessageFormat.format(errorMessage, error.getMessageContext()),propertyPath,errorId));
				}

			}
		}
		if(isLoggingDebug()){
			logDebug("DefaultErrorHandlerImpl - (processValidationException-[pException,pHandler]):End");
		}
	}
	
	/**
	 * This method is used to process SystemExceptions for FormHandlers.Get the errorId from SystemException and pass it to 
	 * ErrorHandlerManager's getErrorMessage method. add the formException and log the error message.
	 * @param pException SystemException
	 * @param pHandler GenericFormHandler
	 * @throws ServletException Exception
	 */
	@Override
	public void processException(SystemException pException,GenericFormHandler pHandler)throws ServletException {
		if(isLoggingDebug()){
			logDebug("DefaultErrorHandlerImpl - (processSystemExceptions-[pException,pHandler]):Start");
		}
		//If pException or 	pHandler is null throw System Exception
		if(pException == null || pHandler == null){
			throw new ServletException(IErrorId.NO_EXCEPTION_OR_HANDLER);
		}
		String errorId = pException.getErrorId();
		String errorMessage = Constants.CONSTANT_EMPTY;
		if(isLoggingDebug()){
			logDebug("ErrorId from ValidationError is: " + errorId);
		}
		try{
			errorMessage = getErrorHandlerManager().getErrorMessage(errorId);
		} catch(SystemException se){
			throw new ServletException(se.getErrorId(), se);
		}
		if(isLoggingDebug()){
			logDebug("ErrorMessage is: " + errorMessage);
		}
		//Adding the form exception.
		pHandler.addFormException(new DropletFormException(errorMessage,Constants.CONSTANT_EMPTY, errorId));
		//log the error message.
		if(isLoggingError()){
			logError(errorMessage,pException);
		}
		if(isLoggingDebug()){
			logDebug("DefaultErrorHandlerImpl - (processSystemExceptions-[pException,pHandler]):End");
		}
	}

	/**
	 * This method is used to handle System exceptions in Droplets and other GenericService classes. 
	 * @param pException SystemException
	 * @param pService ServletService 
	 * @throws ServletException Exception
	 */
	@Override
	public void processException(SystemException pException, ServletService pService)throws ServletException {
		if(isLoggingDebug()){
			logDebug("DefaultErrorHandlerImpl - (processSystemExceptions-[pException]):Start");
		}
		if(pException == null){
			throw new ServletException(IErrorId.NO_EXCEPTION_OR_HANDLER);
		}
		String errorId = pException.getErrorId();
		String errorMessage = Constants.CONSTANT_EMPTY;
		if(isLoggingDebug()){
			logDebug("ErrorId from ValidationError is: " + errorId);
		}
		try{
			errorMessage = getErrorHandlerManager().getErrorMessage(errorId);
		} catch(SystemException se){
			throw new ServletException(se.getErrorId(), se);
		}
		if(isLoggingDebug()){
			logDebug("ErrorMessage is: " + errorMessage);
		}
		//log the error message.
		if(isLoggingError()){
			logError(errorMessage,pException);
		}
		if(isLoggingDebug()){
			logDebug("DefaultErrorHandlerImpl - (processSystemExceptions-[pException]):End");
		}
	}
	
	/**
	 * This method is used to process ApplicationExceptions for FormHandlers.Get the errorId from ApplicationException and pass it to 
	 * ErrorHandlerManager's getErrorMessage method. add the formException and log the error message.
	 * @param pException ApplicationException
	 * @param pHandler GenericFormHandler
	 * @throws ServletException Exception
	 */
	@Override
	public void processException(ApplicationException pException,
			GenericFormHandler pHandler) throws ServletException {
		if(isLoggingDebug()){
			logDebug("DefaultErrorHandlerImpl - (processApplicationExceptions-[pException,pHandler]):Start");
		}
		if(pException == null){
			throw new ServletException(IErrorId.NO_EXCEPTION_OR_HANDLER);
		}
		String errorId = pException.getErrorId();
		String errorMessage = Constants.CONSTANT_EMPTY;
		if(isLoggingDebug()){
			logDebug("ErrorId from ValidationError is: " + errorId);
		}
		try{
			errorMessage = getErrorHandlerManager().getErrorMessage(errorId);
		} catch(SystemException se){
			throw new ServletException(se.getErrorId(), se);
		}
		if(isLoggingDebug()){
			logDebug("ErrorMessage is: " + errorMessage);
		}
		pHandler.addFormException(new DropletFormException(errorMessage,Constants.CONSTANT_EMPTY,errorId));
		if(isLoggingDebug()){
			logDebug("DefaultErrorHandlerImpl - (processApplicationExceptions-[pException,pHandler]):End");
		}
	}
	/**
	 * This method is used to get propertyPath from Error messageContext Object.
	 * @param pValue Object
	 * @return String
	 */
	private String generatePropetyPath(Object[] pValue){
		if(isLoggingDebug()){
			logDebug("DefaultErrorHandlerImpl - (generatePropetyPath-[pValue]):Start");
		}
		String propertyPath = null;
		if(pValue != null && pValue.length > Constants.ZERO){
			propertyPath = getAbsoluteName() + Constants.DOT + (String)pValue[Constants.ZERO];
		}	
		if(isLoggingDebug()){
			logDebug("PropertyPath: "+ propertyPath);
			logDebug("DefaultErrorHandlerImpl - (generatePropetyPath-[pValuer]):End");
		}
		return propertyPath;
	}
}
