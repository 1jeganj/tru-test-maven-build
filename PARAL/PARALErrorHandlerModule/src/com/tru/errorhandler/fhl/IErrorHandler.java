package com.tru.errorhandler.fhl;

import javax.servlet.ServletException;

import atg.droplet.GenericFormHandler;
import atg.nucleus.servlet.ServletService;

import com.tru.common.cml.ApplicationException;
import com.tru.common.cml.SystemException;
import com.tru.common.cml.ValidationExceptions;

/**
 * This is interface  has methods to handle Validation Exceptions,
 * ApplicationExcpetions, and SystemExceptions.
 * 
 * @author Professional Access
 * @version 1.0
 */

public interface IErrorHandler {

	/**
	 * This method is used for handling ValidationExceptions of FormHandler class.
	 *
	 * @param pException ValidationExceptions
	 * @param pHandler GenericFormHandler
	 * @throws ServletException the servlet exception
	 */
	void processException(ValidationExceptions pException, GenericFormHandler pHandler)throws ServletException;
	
	/**
	 * This method is used for handling SystemExceptions of FormHandler class.
	 *
	 * @param pException SystemException
	 * @param pHandler GenericFormHandler
	 * @throws ServletException the servlet exception
	 */
	void processException(SystemException pException, GenericFormHandler pHandler)throws ServletException;
	
	/**
	 * This method is used for handling SystemExceptions of Droplet class.
	 *
	 * @param pException SystemException
	 * @param pService ServletService
	 * @throws ServletException the servlet exception
	 */
	void processException(SystemException pException, ServletService pService)throws ServletException;
	
	/**
	 * This method is used for handling ApplicationExceptions of FormHandler class.
	 *
	 * @param pException ApplicationException
	 * @param pHandler GenericFormHandler
	 * @throws ServletException the servlet exception
	 */
	void processException(ApplicationException pException, GenericFormHandler pHandler)throws ServletException;

}
