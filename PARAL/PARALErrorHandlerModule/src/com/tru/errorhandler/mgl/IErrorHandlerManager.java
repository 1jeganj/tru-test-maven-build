package com.tru.errorhandler.mgl;

import com.tru.common.cml.SystemException;

/**
 * This is interface has methods that interact with ErrorHandlerTools class to get ErrorMessage from 
 * Repository.
 * @author Professional Access
 * @version 1.0
 * 
 */

public interface IErrorHandlerManager {

	/**
	 * This method is used to call tools class getErrorMessage method to get the errorMessage from repository.
	 *
	 * @param pErrorId errorId
	 * @return String
	 * @throws SystemException the system exception
	 */
	String getErrorMessage(String pErrorId) throws SystemException;
}
