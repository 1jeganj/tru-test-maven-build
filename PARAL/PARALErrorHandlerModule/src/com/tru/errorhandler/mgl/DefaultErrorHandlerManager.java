package com.tru.errorhandler.mgl;

import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;

import com.tru.common.cml.Constants;
import com.tru.common.cml.IErrorId;
import com.tru.common.cml.SystemException;
import com.tru.common.tol.IResourceBundleTools;

/**
 * The class holds the methods that interacts with ErrorHandlerTools to get the 
 * errormessage from repository.
 * 
 * @author Professional Access
 * @version 1.0
 */

public class DefaultErrorHandlerManager extends GenericService implements IErrorHandlerManager {
	
	/** Holds the constant of Error Handler Tools. */
	private IResourceBundleTools mErrorHandlerTools;
	
	/**
	 * Gets the error handler tools.
	 *
	 * @return mErrorHandlerTools
	 */
	public IResourceBundleTools getErrorHandlerTools() {
		return mErrorHandlerTools;
	}
	
	/**
	 * Sets the error handler tools.
	 *
	 * @param pErrorHandlerTools ErrorHandlerTools
	 */
	public void setErrorHandlerTools(IResourceBundleTools pErrorHandlerTools) {
		this.mErrorHandlerTools = pErrorHandlerTools;
	}
		
	/**
	 * This method is used to append the site name with the ErrorId.
	 * @return String pErrorKey
	 */
	public String getSiteId(){		
		if(isLoggingDebug()){
			logDebug("ErrorHandlerManager - (getSiteKey-[pErrorId]):Start");
		}
		String currentSiteId = null;
		
		currentSiteId = SiteContextManager.getCurrentSiteId();
		if(isLoggingDebug()){
			logDebug("currentSite :"+ currentSiteId);
		}
				
		if(isLoggingDebug()){
			logDebug("ErrorHandlerManager - (getSiteKey-[pErrorId]):End");
		}
		return currentSiteId;
		
	}
	
	/**
	 * This method checks the UseSiteKey property and if it is true, calls the getSiteErrorKey() method
	 * to append siteId with ErrorId. This method Invokes tools class getErrorMessage by passing ErrorId
	 * and SiteErrorId.
	 *
	 * @param pErrorId - String
	 * @return String errorMessage
	 * @throws SystemException the system exception
	 */
	public String getErrorMessage(String pErrorId) throws SystemException{
		if(isLoggingDebug()){
			logDebug("ErrorHandlerManager - (getErrorMessage-[pErrorId]):Start");
		}
		String siteId = null;	
		String errorMessage = Constants.CONSTANT_EMPTY;
		siteId = getSiteId();
		
		try{
			errorMessage = getErrorHandlerTools().getKeyValue(pErrorId,siteId);
		} catch(RepositoryException re){
			throw new SystemException(IErrorId.UNABLE_TO_GET_THE_ITEM, re);
		}
		if(isLoggingDebug()){
			logDebug("ErrorHandlerManager - (getErrorMessage-[pErrorId]):End");
		}
		return errorMessage;		
	}
}
