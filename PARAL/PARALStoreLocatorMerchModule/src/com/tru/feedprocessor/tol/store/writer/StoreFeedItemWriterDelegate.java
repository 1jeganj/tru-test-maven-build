package com.tru.feedprocessor.tol.store.writer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.base.mapper.IFeedItemMapper;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.writer.AbstractFeedItemWriterDelegate;

/**
 * The Class StoreFeedItemWriterDelegate.
 *
 * @version 1.1
 * @author Professional Access
 */
public class StoreFeedItemWriterDelegate extends AbstractFeedItemWriterDelegate {
	/**
	 * The entity id map.
	 */
	private Map<String,Map<String,String>> mEntityIdMap = null;
	/**The IdentifierIdStoreKey 	 */
	private String mIdentifierIdStoreKey;

	/** The Hours Item key */
	private String mHoursItemKey;
	
	/** The Hours property name */
	private String mHoursPropertyName;
	
	/** The Hours itemDescriptor name */
	private String mHoursItemDescriptorName;
	
	/**
	 * @return the hoursItemDescriptorName
	 */
	public String getHoursItemDescriptorName() {
		return mHoursItemDescriptorName;
	}

	/**
	 * @param pHoursItemDescriptorName the hoursItemDescriptorName to set
	 */
	public void setHoursItemDescriptorName(String pHoursItemDescriptorName) {
		mHoursItemDescriptorName = pHoursItemDescriptorName;
	}

	/**
	 * @return the hoursPropertyName
	 */
	public String getHoursPropertyName() {
		return mHoursPropertyName;
	}

	/**
	 * @param pHoursPropertyName the hoursPropertyName to set
	 */
	public void setHoursPropertyName(String pHoursPropertyName) {
		mHoursPropertyName = pHoursPropertyName;
	}

	/**
	 * @return the hoursItemKey
	 */
	public String getHoursItemKey() {
		return mHoursItemKey;
	}

	/**
	 * @param pHoursItemKey the hoursItemKey to set
	 */
	public void setHoursItemKey(String pHoursItemKey) {
		mHoursItemKey = pHoursItemKey;
	}

	/** @return the IdentifierIdStoreKey	 */
	public String getIdentifierIdStoreKey() {
		return mIdentifierIdStoreKey;
	}

	/**
	 * @param pIdentifierIdStoreKey the IdentifierIdStoreKey to set
	 */
	public void setIdentifierIdStoreKey(String pIdentifierIdStoreKey) {
		mIdentifierIdStoreKey = pIdentifierIdStoreKey;
	}

	/**
	 * (non-Javadoc).
	 *
	 * @param pBaseFeedProcessVO the BaseFeedProcessVO
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 * @see com.tru.feedprocessor.tol.AbstractFeedItemWriter.
	 * AbstractBvFeedItemWriter#write(java.util.List)
	 */
	@Override
	public void doWrite(BaseFeedProcessVO pBaseFeedProcessVO) throws RepositoryException,FeedSkippableException{
		
			 String lItemDiscriptorName = getItemDescriptorName(pBaseFeedProcessVO.getEntityName());
				if(mEntityIdMap!=null && mEntityIdMap.get(lItemDiscriptorName).get(pBaseFeedProcessVO.getRepositoryId())!=null){
					updateItem(pBaseFeedProcessVO);
				}else{
					addItem(pBaseFeedProcessVO);
				}
	}
	/**
	 * Open.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void doOpen(){
		super.doOpen();
		
		AbstractFeedItemMapper mapper=(AbstractFeedItemMapper) getMapper();
		mapper.setFeedHelper(getFeedHelper());
		
		mEntityIdMap = (Map<String, Map<String, String>>)retriveData(getIdentifierIdStoreKey()); 
	}

	/**
	 * Update item.
	 *
	 * @param pFeedVO the feed vo
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	public void updateItem(BaseFeedProcessVO pFeedVO) throws RepositoryException, FeedSkippableException{
		MutableRepositoryItem lCurrentItem=null;
		
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
		
		if(lItemDiscriptorName!=null){
			IFeedItemMapper	lFeedItemMapper =(IFeedItemMapper) getMapper();
			lCurrentItem=(MutableRepositoryItem) getRepository(lRepositoryName).getItem(mEntityIdMap.get(lItemDiscriptorName).get(pFeedVO.getRepositoryId()),lItemDiscriptorName);
			beforeUpdateItem(pFeedVO,lCurrentItem);
			lCurrentItem=lFeedItemMapper.map(pFeedVO, lCurrentItem);
			List<RepositoryItem> lHoursItems=(List<RepositoryItem>) retriveData(getHoursItemKey());
			lCurrentItem.setPropertyValue(getHoursPropertyName(), lHoursItems);
			getRepository(lRepositoryName).updateItem(lCurrentItem);
			
			addUpdateItemToList(pFeedVO);
			
		}

	}

	/**
	 * Before update item.
	 * @param pFeedVO - pFeedVO
	 * @param pCurrentItem - pCurrentItem
	 * @throws RepositoryException - RepositoryException
	 * @throws FeedSkippableException - FeedSkippableException
	 */
	protected void beforeUpdateItem(BaseFeedProcessVO pFeedVO, MutableRepositoryItem pCurrentItem) throws RepositoryException,FeedSkippableException{
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
		RepositoryItem lRepoItem=null;
		List<RepositoryItem> lRepoItems=null;
		lRepoItem=getRepository(lRepositoryName).getItem(pFeedVO.getRepositoryId(),lItemDiscriptorName);
		lRepoItems =  (List<RepositoryItem>) lRepoItem.getPropertyValue(getHoursPropertyName());
		if(lRepoItems!=null &&!lRepoItems.isEmpty() ){
			((MutableRepositoryItem) lRepoItem).setPropertyValue(getHoursPropertyName(), new ArrayList());
			for(RepositoryItem repoItem:lRepoItems){
					getRepository(lRepositoryName).removeItem(repoItem.getRepositoryId(), getHoursItemDescriptorName());
			}
		}
	}

	/**
	 * Adds the item.
	 *
	 * @param pFeedVO the feed vo
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	public void addItem(BaseFeedProcessVO pFeedVO)throws RepositoryException, FeedSkippableException{
		MutableRepositoryItem lCurrentItem=null;
		
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
		
		if(lItemDiscriptorName!=null){
			IFeedItemMapper lFeedItemMapper =(IFeedItemMapper) getMapper();
			lCurrentItem=getRepository(lRepositoryName).createItem(pFeedVO.getRepositoryId(),lItemDiscriptorName);
			beforeAddItem(pFeedVO,lCurrentItem);
			lCurrentItem=lFeedItemMapper.map(pFeedVO, lCurrentItem);
			getRepository(lRepositoryName).addItem(lCurrentItem);
			
			addInsertItemToList(pFeedVO);
			
		}

	}
	/** Before add item.
	 * 
	 * @param pFeedVO - pFeedVO
	 * @param pCurrentItem - pCurrentItem
	 * @throws RepositoryException - RepositoryException
	 * @throws FeedSkippableException - FeedSkippableException
	 */
	protected void beforeAddItem(BaseFeedProcessVO pFeedVO, MutableRepositoryItem pCurrentItem) throws RepositoryException,FeedSkippableException{
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
		RepositoryItem lRepoItem=null;
		List<RepositoryItem> lRepoItems=null;
		lRepoItem=getRepository(lRepositoryName).getItem(pFeedVO.getRepositoryId(),lItemDiscriptorName);
		lRepoItems =  (List<RepositoryItem>) lRepoItem.getPropertyValue(getHoursPropertyName());
		if(lRepoItems!=null &&!lRepoItems.isEmpty() ){
			for(RepositoryItem repoItem:lRepoItems){
				getRepository(lRepositoryName).removeItem(repoItem.getRepositoryId(),getHoursItemDescriptorName());
			}
		}
	}
}
