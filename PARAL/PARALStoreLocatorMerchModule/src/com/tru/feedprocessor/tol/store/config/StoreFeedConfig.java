package com.tru.feedprocessor.tol.store.config;

import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.config.AbstractFeedConfig;

/**
 * The Class InvFeedConfiguration. This class is specific to bazaarvoice feed,
 * which will hold the configuration details of bazaarvoice
 * 
 * @version 1.1
 * @author Professional Access
 */
public class StoreFeedConfig extends AbstractFeedConfig { 
	/** The mXmlRootElement. */
	private String mXmlRootElement;

	/**
	 * Gets the xml root element.
	 * 
	 * @return the xmlRootElement
	 */
	public String getXmlRootElement() {
		return mXmlRootElement;
	}
	/**
	 * Sets the entity list.
	 * 
	 * @param pRootElement
	 *            the new entity list
	 */
	public void setXmlRootElement(String pRootElement) {
		mXmlRootElement=pRootElement;
		setConfigValue(FeedConstants.ROOT_ELEMENT, mXmlRootElement);
	}
	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.AbstractFeedConfig#afterPropertiesSet()
	 */
	@SuppressWarnings("unchecked")
	@Override
	 public void afterPropertiesSet() throws Exception {
		try{
			/*if(StringUtils.isBlank( getConfigValue(FeedConstants.ANNOTATEDCLASSES).toString())){
				getLogger().logError("ANNOTATEDCLASSES can not be null in  feed configuration");
			}*/
			if(StringUtils.isBlank( getConfigValue(FeedConstants.FILENAME).toString())){
				getLogger().logError("File name can not be null in  feed configuration");
			}
			if(StringUtils.isBlank( getConfigValue(FeedConstants.FILEPATH).toString())){
				getLogger().logError("File path can not be null in  feed configuration");
			}
			if(StringUtils.isBlank((String) getConfigValue(FeedConstants.FILETYPE))){
				getLogger().logError("File Type can not be null in feed configuration");
			}
			/*if(StringUtils.isBlank((String) getConfigValue(FeedConstants.ROOT_TAG))){
				getLogger().logError("Xml root tag can not be null in feed configuration");
			}*/
			if(((List<String>) getConfigValue(FeedConstants.ENTITY_LIST)).isEmpty()){
				getLogger().logError("Entity list can not be null in feed configuration");
			}
			if(((Map<String, String>)getConfigValue(FeedConstants.ENTITY_SQL)).isEmpty()){
				getLogger().logError("Entity sql can not be null in feed configuration");
			}
			String voName = (String) getConfigValue(FeedConstants.ENTITY_PROVIDER_FQCN);
			if(StringUtils.isBlank(voName)){
				getLogger().logError("Entity Provider FQCN can not be null in bazaarvoice feed configuration");
			}
			try {
				Class.forName(voName);
			} catch (ClassNotFoundException e) {
				if (isLoggingError()) {
					logError("Entity Provider FQCN must be a valid entry",e);
				}
			}
			
			for(String key :((List<String>)getConfigValue(FeedConstants.ENTITY_LIST))){
				if(!((Map<String, String>)getConfigValue(FeedConstants.ENTITY_SQL)).containsKey(key)){
					getLogger().logError("SQL is mandatory for each entity provided in entity list in feed configuration");
				}
			}
				if((((String)getConfigValue(FeedConstants.FILETYPE)).toLowerCase().equals(FeedConstants.XML)) && (StringUtils.isBlank((String)getConfigValue(FeedConstants.FILENAME))) || (getConfigValue(FeedConstants.FILENAME).toString().endsWith(FeedConstants.XML)))
						{
					getLogger().logError("Please provide proper values in feed configuration");
			}
			if(!StringUtils.isBlank(getConfigValue(FeedConstants.FILE_DOWNLOADER_FLAG).toString()) 
					&& getConfigValue(FeedConstants.FILE_DOWNLOADER_FLAG).equals(Boolean.toString(FeedConstants.TRUE))){
				
				if(StringUtils.isBlank(getConfigValue(FeedConstants.FTP_SFTP_HOST).toString())){
					getLogger().logError("Host Name for FTP and SFTP is mandatory for file downloading in store feed configuration");
				}
				if(StringUtils.isBlank(getConfigValue(FeedConstants.FTP_SFTP_PORT).toString())){
					getLogger().logError("Port NO for FTP and SFTP is mandatory for file downloading in store feed configuration");
				}
				if(StringUtils.isBlank(getConfigValue(FeedConstants.FPT_SFTP_USER).toString())){
					getLogger().logError("Server IP for FTP and SFTP is mandatory for file downloading in store feed configuration");
				}
				if(StringUtils.isBlank(getConfigValue(FeedConstants.FPT_SFTP_PASSWORD).toString())){
					getLogger().logError("Server IP for FTP and SFTP is mandatory for file downloading in store feed configuration");
				}
				if(StringUtils.isBlank(getConfigValue(FeedConstants.FPT_SFTP_SOURCE_DIR).toString())){
					getLogger().logError("Server IP for FTP and SFTP is mandatory for file downloading in store feed configuration ");
				}
				if(StringUtils.isBlank( getConfigValue(FeedConstants.FILEPATH).toString())){
					getLogger().logError("File path can not be null in store feed configuration");
				}	
				if(StringUtils.isBlank( getConfigValue(FeedConstants.PROTOCOL_TYPE).toString())){
					getLogger().logError("Protocol type for FTP or SFTP can not be null in store feed configuration");
				}
				if(StringUtils.isBlank( getConfigValue(FeedConstants.DOWNLOAD_FILETYPE).toString())){
					getLogger().logError("Download file type can not be null in store feed configuration");
				}	
			}
		}catch(Exception e){
			if (isLoggingError()) {
				logError("Please provide proper values in feed configuration",e);
			}
		}
		}
		

}
