package com.tru.feedprocessor.tol.store.config;

import java.io.File;
import java.util.Map;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.FeedHelper;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;


/**
 * The Class StoreFeedConfigMapper.
 * 
 * @version 1.1
 * @author Professional Access
 */
public class StoreFeedConfigMapper {

	/** The m feed helper. */
	private FeedHelper mFeedHelper;

	/**
	 * @return the feed helper
	 */
	public FeedHelper getFeedHelper() {
		return mFeedHelper;
	}

	/**
	 * @param pFeedHelper the feed helper to set
	 */
	public void setFeedHelper(FeedHelper pFeedHelper) {
		mFeedHelper = pFeedHelper;
	}

	/**
	 * Gets the resource name.
	 * 
	 * @return the resource name
	 */
	public String getResourceName() {
		StringBuffer lresourceName = new StringBuffer();
		
		FeedExecutionContextVO curExecCntx = getCurrentFeedExecutionContext();
		if(curExecCntx == null ){
			lresourceName.append(FeedConstants.FILE);
			lresourceName.append(getConfigValue(FeedConstants.PROCESSING_DIRECTORY));
			lresourceName.append(getConfigValue(FeedConstants.FILENAME)+FeedConstants.DOT+getConfigValue(FeedConstants.FILETYPE));
		}
		else{
			lresourceName.append(FeedConstants.FILE);
			lresourceName.append(getConfigValue(FeedConstants.PROCESSING_DIRECTORY));
			File lFile = new File(curExecCntx.getFilePath()+curExecCntx.getFileName());
			lresourceName.append(lFile.getName());
		}
 		return lresourceName.toString();
	}

	/**
	 * Gets the root element.
	 * 
	 * @return the root element
	 */
	public String getRootElement() {
		return (String) getConfigValue(FeedConstants.ROOT_ELEMENT);
	}

	/**
	 * Gets the root tag.
	 * 
	 * @return the root tag
	 */
	public String getRootTag(){
		return (String) getConfigValue(FeedConstants.ROOT_TAG);
	}
	/**
	 * Gets the root element attributes
	 * 
	 * @return the  rootElementAttributes
	 */
	public Map<String,String> getRootElementAttributes(){
		return (Map<String, String>) getConfigValue(FeedConstants.ROOT_ELEMENT_ATTRIBUTES);
	}
	/**
	 * Gets the aliases
	 * 
	 * @return the aliases
	 */
	public Map<String, String> getAliases(){
		return (Map<String, String>) getConfigValue(FeedConstants.ALIASES);
	}
	/**
	 * Gets the omitted fields
	 * 
	 * @return the omittedFields
	 */
	public Map<String, String> getOmittedFields(){
		return (Map<String, String>) getConfigValue(FeedConstants.OMITTEDFIELDS);
	}
	/**
	 * Gets the annotated classes
	 * 
	 * @return the annotatedClasses
	 */
	public Class<?>[] getAnnotatedClasses(){
		return  (Class<?>[]) getConfigValue(FeedConstants.ANNOTATEDCLASSES);
		
	}
	/**
	 * Gets the mapeer vo class name.
	 * 
	 * @return the mapeer vo class name
	 * 
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 */
	public Class<?> getFeedEntityProviderFQCN() throws ClassNotFoundException {
		return Class.forName((String) getConfigValue(FeedConstants.ENTITY_PROVIDER_FQCN));

	}
	/**
	 * 
	 * @return - CurrentFeedExecutionContext
	 */
	public FeedExecutionContextVO getCurrentFeedExecutionContext() {
		return (FeedExecutionContextVO) retriveData(FeedConstants.CURRENT_FEEDCONTEXT);
	}
	/**
	 * Retrive data.
	 * 
	 * @param pDataMapKey
	 *            the data map key
	 * @return the object
	 */
	public Object retriveData(String pDataMapKey) {
		return getFeedHelper().retriveData(pDataMapKey);
	}
	/**
	 * 
	 * @param pConfigKey the pConfigKey
	 * @return value
	 */
	public Object getConfigValue(String pConfigKey) {
		return getFeedHelper().getConfigValue(pConfigKey);
	}
}
