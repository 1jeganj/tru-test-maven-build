package com.tru.feedprocessor.tol.store.writer;

import java.util.ArrayList;
import java.util.List;

import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.base.mapper.IFeedItemMapper;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.writer.AbstractFeedItemWriterDelegate;
import com.tru.feedprocessor.tol.store.vo.Store;
import com.tru.feedprocessor.tol.store.vo.WeekDay;


/**
 * The class StoreFeedItemWriterTradingHoursDelegate.
 * 
 * @version 1.1
 * @author Professional Access
 */
public class StoreFeedItemWriterTradingHoursDelegate extends AbstractFeedItemWriterDelegate{
	
	/**
	 * The IdentifierIdStoreKey.
	 */
	private String mIdentifierIdStoreKey;
	
	/**
	 * The Hours Item key.
	 */
	private String mHoursItemKey;
	
	/**
	 * Gets the hours item key.
	 * 
	 * @return the hoursItemKey
	 */
	public String getHoursItemKey() {
		return mHoursItemKey;
	}

	/**
	 * Sets the hours item key.
	 * 
	 * @param pHoursItemKey
	 *            the hoursItemKey to set
	 */
	public void setHoursItemKey(String pHoursItemKey) {
		mHoursItemKey = pHoursItemKey;
	}
	
	/**
	 * Gets the identifier id store key.
	 * 
	 * @return the IdentifierIdStoreKey
	 */
	public String getIdentifierIdStoreKey() {
		return mIdentifierIdStoreKey;
	}

	/**
	 * Sets the identifier id store key.
	 * 
	 * @param pIdentifierIdStoreKey
	 *            the IdentifierIdStoreKey to set
	 */
	public void setIdentifierIdStoreKey(String pIdentifierIdStoreKey) {
		mIdentifierIdStoreKey = pIdentifierIdStoreKey;
	}
	/**
	 * Open.
	 */
	@Override
	public void doOpen(){
		AbstractFeedItemMapper mapper=(AbstractFeedItemMapper) getMapper();
		mapper.setFeedHelper(getFeedHelper());
	}
	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.base.writer.AbstractFeedItemWriter#doWrite(java.util.List)
	 */
	@Override
	public void doWrite(BaseFeedProcessVO pFeedV0)
			throws RepositoryException, FeedSkippableException {

		addItem(pFeedV0);
	}
	/**
	 * Adds the item.
	 *
	 * @param pFeedVO the feed vo
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	public void addItem(BaseFeedProcessVO pFeedVO)throws RepositoryException, FeedSkippableException{
		
		MutableRepositoryItem lCurrentItem=null;
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		Store store=(Store) pFeedVO;
		List<MutableRepositoryItem> lRepoItems=new ArrayList<MutableRepositoryItem>();
		IFeedItemMapper lFeedItemMapper =(IFeedItemMapper) getMapper();
		for(WeekDay weekDay: store.getWeekDays().getWeekDay()){
			if(weekDay.getEntityName()!=null && lRepositoryName!=null){
				String lItemDescriptorName=getItemDescriptorName(weekDay.getEntityName());
				lCurrentItem=getRepository(lRepositoryName).createItem(lItemDescriptorName);
				lCurrentItem=lFeedItemMapper.map(weekDay, lCurrentItem);
				beforeAddItem(weekDay, lCurrentItem);
				getRepository(lRepositoryName).addItem(lCurrentItem);
				lRepoItems.add(lCurrentItem);
			}
		}
		saveData(getHoursItemKey(),lRepoItems);
	}
	/**
	 * Before add item.
	 * 
	 * @param pFeedVO - pFeedVO
	 * @param pCurrentItem - pCurrentItem
	 * @throws RepositoryException - RepositoryException
	 * @throws FeedSkippableException - FeedSkippableException
	 */
	protected void beforeAddItem(BaseFeedProcessVO pFeedVO, MutableRepositoryItem pCurrentItem) throws RepositoryException,FeedSkippableException{
		//TO-DO
	}
}
