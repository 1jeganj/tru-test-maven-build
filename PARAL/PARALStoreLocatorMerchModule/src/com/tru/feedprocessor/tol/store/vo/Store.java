package com.tru.feedprocessor.tol.store.vo;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * @version 1.0
 * @author Professional Access
 * 
 */
  @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mTitle",
        "mContact",
        "mManager",
        "mStoreId",
        "mActive",
        "mDisplayStoreHours",
        "mAdditionalNotes",
        "mSites",
        "mWeekDays",
        "mLocation"
    })
    @XmlRootElement(name="Store")
    public class Store extends BaseFeedProcessVO{

      /**
		 * Gets the entity name.
		 * 
		 * @return the entity name EntityName
		 */
	  public String getEntityName(){
			return FeedConstants.STORE;
		}

        /**
		 * The Title.
		 */
        @XmlElement(name = "Title", required = true)
        protected String mTitle;
        
        /**
		 * The Contact.
		 */
        @XmlElement(name = "Contact", required = true)
        protected Contact mContact;
        
        /**
		 * The Manager.
		 */
        @XmlElement(name = "Manager", required = true)
        protected String mManager;
        
        /**
		 * The Store id.
		 */
        @XmlElement(name = "StoreId")
        protected String mStoreId;
        
        /**
		 * The Active.
		 */
        @XmlElement(name = "Active", required = true)
        protected String mActive;
        
        /**
		 * The Display store hours.
		 */
        @XmlElement(name = "DisplayStoreHours", required = true)
        protected String mDisplayStoreHours;
        
        /**
		 * The Additional notes.
		 */
        @XmlElement(name = "AdditionalNotes", required = true)
        protected String mAdditionalNotes;
        
        /**
		 * The Sites.
		 */
        @XmlElement(name = "Sites", required = true)
        protected Sites mSites;
        
        /**
		 * The Week days.
		 */
        @XmlElement(name = "WeekDays", required = true)
        protected WeekDays mWeekDays;
        
        /**
		 * The Location.
		 */
        @XmlElement(name = "Location", required = true)
        protected Location mLocation;
        
        /**
		 * Gets the repository id.
		 * 
		 * @return mStoreId
		 */
        public String getRepositoryId(){
    		return mStoreId;
    	}
        
        /**
		 * Gets the title.
		 * 
		 * @return mTitle
		 */
        public String getTitle() {
            return mTitle;
        }

        /**
         * Sets the value of the title property.
         * 
         * @param pValue
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTitle(String pValue) {
            this.mTitle = pValue;
        }

       /**
		 * Gets the contact.
		 * 
		 * @return mContact
		 */
        public Contact getContact() {
            return mContact;
        }

        /**
         * Sets the value of the contact property.
         * 
         * @param pValue
         *     allowed object is
         *     {@link Contact }
         *     
         */
        public void setContact(Contact pValue) {
            this.mContact = pValue;
        }

        /**
		 * Gets the value of the manager property.
		 * 
		 * @return the manager possible object is {@link String }
		 */
        public String getManager() {
            return mManager;
        }

        /**
         * Sets the value of the manager property.
         * 
         * @param pValue
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setManager(String pValue) {
            this.mManager = pValue;
        }

        /**
		 * Gets the store id.
		 * 
		 * @return mStoreId
		 */
        public String getStoreId() {
            return mStoreId;
        }

        /**
		 * Sets the store id.
		 * 
		 * @param pValue
		 *            - value
		 */
        public void setStoreId(String pValue) {
            this.mStoreId = pValue;
        }

        /**
		 * Gets the value of the active property.
		 * 
		 * @return the active possible object is {@link String }
		 */
        public String getActive() {
            return mActive;
        }

        /**
         * Sets the value of the active property.
         * 
         * @param pValue
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActive(String pValue) {
            this.mActive = pValue;
        }

        /**
		 * Gets the value of the displayStoreHours property.
		 * 
		 * @return the display store hours possible object is {@link String }
		 */
        public String getDisplayStoreHours() {
            return mDisplayStoreHours;
        }

        /**
         * Sets the value of the displayStoreHours property.
         * 
         * @param pValue
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDisplayStoreHours(String pValue) {
            this.mDisplayStoreHours = pValue;
        }

        /**
		 * Gets the value of the additionalNotes property.
		 * 
		 * @return the additional notes possible object is {@link String }
		 */
        public String getAdditionalNotes() {
            return mAdditionalNotes;
        }

        /**
         * Sets the value of the additionalNotes property.
         * 
         * @param pValue
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAdditionalNotes(String pValue) {
            this.mAdditionalNotes = pValue;
        }

        /**
		 * Gets the value of the sites property.
		 * 
		 * @return the sites possible object is {@link Sites }
		 */
        public Sites getSites() {
            return mSites;
        }

        /**
         * Sets the value of the sites property.
         * 
         * @param pValue
         *     allowed object is
         *     {@link Sites }
         *     
         */
        public void setSites(Sites pValue) {
            this.mSites = pValue;
        }

        /**
		 * Gets the value of the weekDays property.
		 * 
		 * @return the week days possible object is {@link WeekDays }
		 */
        public WeekDays getWeekDays() {
            return mWeekDays;
        }

        /**
         * Sets the value of the weekDays property.
         * 
         * @param pValue
         *     allowed object is
         *     {@link WeekDays }
         *     
         */
        public void setWeekDays(WeekDays pValue) {
            this.mWeekDays = pValue;
        }

        /**
		 * Gets the value of the location property.
		 * 
		 * @return the location possible object is {@link Location }
		 */
        public Location getLocation() {
            return mLocation;
        }

        /**
         * Sets the value of the location property.
         * 
         * @param pValue
         *     allowed object is
         *     {@link Location }
         *     
         */
        public void setLocation(Location pValue) {
            this.mLocation = pValue;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Address">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="StreetAddressLine1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="StreetAddressLine2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="StreetAddressLine3" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Suburb" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="StateORProvince" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}short"/>
         *                   &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="Fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "mAddress",
            "mPhone",
            "mFax",
            "mEmail"
        })
        public static class Contact {

            /**
			 * The Address.
			 */
            @XmlElement(name = "Address", required = true)
            protected Address mAddress;
            
            /**
			 * The Phone.
			 */
            @XmlElement(name = "Phone")
            protected String mPhone;
            
            /**
			 * The Fax.
			 */
            @XmlElement(name = "Fax", required = true)
            protected String mFax;
            
            /**
			 * The Email.
			 */
            @XmlElement(name = "Email", required = true)
            protected String mEmail;

            /**
			 * Gets the value of the address property.
			 * 
			 * @return the address possible object is {@link Address }
			 */
            public Address getAddress() {
                return mAddress;
            }

            /**
             * Sets the value of the address property.
             * 
             * @param pValue
             *     allowed object is
             *     {@link Address }
             *     
             */
            public void setAddress(Address pValue) {
                this.mAddress = pValue;
            }

           /**
			 * Gets the phone.
			 * 
			 * @return phone
			 */
            public String getPhone() {
                return mPhone;
            }

            /**
			 * Sets the phone.
			 * 
			 * @param pValue
			 *            - value
			 */
            public void setPhone(String pValue) {
                this.mPhone = pValue;
            }

            /**
			 * Gets the value of the fax property.
			 * 
			 * @return the fax possible object is {@link String }
			 */
            public String getFax() {
                return mFax;
            }

            /**
             * Sets the value of the fax property.
             * 
             * @param pValue
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFax(String pValue) {
                this.mFax = pValue;
            }

            /**
			 * Gets the value of the email property.
			 * 
			 * @return the email possible object is {@link String }
			 */
            public String getEmail() {
                return mEmail;
            }

            /**
             * Sets the value of the email property.
             * 
             * @param pValue
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEmail(String pValue) {
                this.mEmail = pValue;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="StreetAddressLine1" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="StreetAddressLine2" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="StreetAddressLine3" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Suburb" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="StateORProvince" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}short"/>
             *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "mStreetAddressLine1",
                "mStreetAddressLine2",
                "mStreetAddressLine3",
                "mSuburb",
                "mCity",
                "mStateORProvince",
                "mPostalCode",
                "mCountry"
            })
            public static class Address {

                /**
				 * The Street address line1.
				 */
                @XmlElement(name = "StreetAddressLine1", required = true)
                protected String mStreetAddressLine1;
                
                /**
				 * The Street address line2.
				 */
                @XmlElement(name = "StreetAddressLine2", required = true)
                protected String mStreetAddressLine2;
                
                /**
				 * The Street address line3.
				 */
                @XmlElement(name = "StreetAddressLine3", required = true)
                protected String mStreetAddressLine3;
                
                /**
				 * The Suburb.
				 */
                @XmlElement(name = "Suburb", required = true)
                protected String mSuburb;
                
                /**
				 * The City.
				 */
                @XmlElement(name = "City", required = true)
                protected String mCity;
                
                /**
				 * The State or province.
				 */
                @XmlElement(name = "StateORProvince", required = true)
                protected String mStateORProvince;
                
                /**
				 * The Postal code.
				 */
                @XmlElement(name = "PostalCode")
                protected short mPostalCode;
                
                /**
				 * The Country.
				 */
                @XmlElement(name = "Country", required = true)
                protected String mCountry;

                /**
				 * Gets the value of the streetAddressLine1 property.
				 * 
				 * @return the street address line1 possible object is
				 *         {@link String }
				 */
                public String getStreetAddressLine1() {
                    return mStreetAddressLine1;
                }

                /**
                 * Sets the value of the streetAddressLine1 property.
                 * 
                 * @param pValue
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStreetAddressLine1(String pValue) {
                    this.mStreetAddressLine1 = pValue;
                }

                /**
				 * Gets the value of the streetAddressLine2 property.
				 * 
				 * @return the street address line2 possible object is
				 *         {@link String }
				 */
                public String getStreetAddressLine2() {
                    return mStreetAddressLine2;
                }

                /**
                 * Sets the value of the streetAddressLine2 property.
                 * 
                 * @param pValue
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStreetAddressLine2(String pValue) {
                    this.mStreetAddressLine2 = pValue;
                }

                /**
				 * Gets the value of the streetAddressLine3 property.
				 * 
				 * @return the street address line3 possible object is
				 *         {@link String }
				 */
                public String getStreetAddressLine3() {
                    return mStreetAddressLine3;
                }

                /**
                 * Sets the value of the streetAddressLine3 property.
                 * 
                 * @param pValue
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStreetAddressLine3(String pValue) {
                    this.mStreetAddressLine3 = pValue;
                }

                /**
				 * Gets the value of the suburb property.
				 * 
				 * @return the suburb possible object is {@link String }
				 */
                public String getSuburb() {
                    return mSuburb;
                }

                /**
                 * Sets the value of the suburb property.
                 * 
                 * @param pValue
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSuburb(String pValue) {
                    this.mSuburb = pValue;
                }

                /**
				 * Gets the value of the city property.
				 * 
				 * @return the city possible object is {@link String }
				 */
                public String getCity() {
                    return mCity;
                }

                /**
                 * Sets the value of the city property.
                 * 
                 * @param pValue
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCity(String pValue) {
                    this.mCity = pValue;
                }

                /**
				 * Gets the value of the stateORProvince property.
				 * 
				 * @return the state or province possible object is
				 *         {@link String }
				 */
                public String getStateORProvince() {
                    return mStateORProvince;
                }

                /**
                 * Sets the value of the stateORProvince property.
                 * 
                 * @param pValue
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStateORProvince(String pValue) {
                    this.mStateORProvince = pValue;
                }

               /**
				 * Gets the postal code.
				 * 
				 * @return postalCode
				 */ 
                public short getPostalCode() {
                    return mPostalCode;
                }

                /**
				 * Sets the postal code.
				 * 
				 * @param pValue
				 *            - pValue
				 */
                public void setPostalCode(short pValue) {
                    this.mPostalCode = pValue;
                }

                /**
				 * Gets the value of the country property.
				 * 
				 * @return the country possible object is {@link String }
				 */
                public String getCountry() {
                    return mCountry;
                }

                /**
                 * Sets the value of the country property.
                 * 
                 * @param pValue
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCountry(String pValue) {
                    this.mCountry = pValue;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Latitude" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *         &lt;element name="Longitude" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "mLatitude",
            "mLongitude"
        })
        public static class Location {

            /**
			 * The Latitude.
			 */
            @XmlElement(name = "Latitude")
            protected float mLatitude;
            
            /**
			 * The Longitude.
			 */
            @XmlElement(name = "Longitude")
            protected float mLongitude;

            /**
			 * Gets the latitude.
			 * 
			 * @return latitude
			 */
            public float getLatitude() {
                return mLatitude;
            }

            /**
			 * Sets the latitude.
			 * 
			 * @param pValue
			 *            - value
			 */
            public void setLatitude(float pValue) {
                this.mLatitude = pValue;
            }

            /**
			 * Gets the longitude.
			 * 
			 * @return longitude
			 */
            public float getLongitude() {
                return mLongitude;
            }

            /**
			 * Sets the longitude.
			 * 
			 * @param pValue
			 *            - value
			 */
            public void setLongitude(float pValue) {
                this.mLongitude = pValue;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="SiteId" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "mSiteId"
        })
        public static class Sites {

            /**
			 * The Site id.
			 */
            @XmlElement(name = "SiteId")
            protected byte mSiteId;

            /**
			 * Gets the site id.
			 * 
			 * @return siteId
			 */
            public byte getSiteId() {
                return mSiteId;
            }

            /**
			 * Sets the site id.
			 * 
			 * @param pValue
			 *            - value
			 */
            public void setSiteId(byte pValue) {
                this.mSiteId = pValue;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="WeekDay" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="DayName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="WorkingTimes">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="WorkingTime">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="FromTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="ToTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "mWeekDay"
        })
        public static class WeekDays {

            /**
			 * The Week day.
			 */
            @XmlElement(name = "WeekDay", required = true)
            protected List<WeekDay> mWeekDay;

            /**
			 * Gets the value of the weekDay property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the weekDay property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getWeekDay().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * 
			 * @return mWeekDay {@link WeekDay }
			 */
            public List<WeekDay> getWeekDay() {
                if (mWeekDay == null) {
                	mWeekDay = new ArrayList<WeekDay>();
                }
                return this.mWeekDay;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="DayName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="WorkingTimes">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="WorkingTime">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="FromTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="ToTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            

        }

    }

