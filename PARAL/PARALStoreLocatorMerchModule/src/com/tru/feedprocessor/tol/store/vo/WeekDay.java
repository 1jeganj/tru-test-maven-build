package com.tru.feedprocessor.tol.store.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

	/**
	 * The Class WeekDay.
	 * @author Professional Access
	 * @version 1.0
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mDayName",
        "mWorkingTimes"
    })
    public class WeekDay extends BaseFeedProcessVO {
		
		/**
		 * Gets the entity name.
		 * 
		 * @return entity name
		 */
		 public String getEntityName(){
			 	return FeedConstants.TRADINGHOURS;
			}

        /**
		 * The Day name.
		 */
        @XmlElement(name = "DayName", required = true)
        protected String mDayName;
        
        /**
		 * The Working times.
		 */
        @XmlElement(name = "WorkingTimes", required = true)
        protected WorkingTimes mWorkingTimes;
        /* (non-Javadoc)
         * @see com.tru.feedprocessor.cml.vo.BaseFeedProcessVO#getRepositoryId()
         */
        @Override
        public String getRepositoryId() {
         
        	return mDayName;
        }
        
        /**
		 * Gets the value of the dayName property.
		 * 
		 * @return the day name possible object is {@link String }
		 */
        public String getDayName() {
            return mDayName;
        }

        /**
         * Sets the value of the dayName property.
         * 
         * @param pValue
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDayName(String pValue) {
            this.mDayName = pValue;
        }

        /**
		 * Gets the value of the workingTimes property.
		 * 
		 * @return the working times possible object is {@link WorkingTimes }
		 */
        public WorkingTimes getWorkingTimes() {
            return mWorkingTimes;
        }

        /**
         * Sets the value of the workingTimes property.
         * 
         * @param pValue
         *     allowed object is
         *     {@link WorkingTimes }
         *     
         */
        public void setWorkingTimes(WorkingTimes pValue) {
            this.mWorkingTimes = pValue;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="WorkingTime">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="FromTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ToTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "mWorkingTime"
        })
        public static class WorkingTimes {

            /**
			 * The Working time.
			 */
            @XmlElement(name = "WorkingTime", required = true)
            protected WorkingTime mWorkingTime;

            /**
			 * Gets the value of the workingTime property.
			 * 
			 * @return the working time possible object is {@link WorkingTime }
			 */
            public WorkingTime getWorkingTime() {
                return mWorkingTime;
            }

            /**
             * Sets the value of the workingTime property.
             * 
             * @param pValue
             *     allowed object is
             *     {@link WorkingTime }
             *     
             */
            public void setWorkingTime(WorkingTime pValue) {
                this.mWorkingTime = pValue;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="FromTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ToTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "fromTime",
                "toTime"
            })
            public static class WorkingTime {

                /**
				 * The from time.
				 */
                @XmlElement(name = "FromTime", required = true)
                protected String fromTime;
                
                /**
				 * The to time.
				 */
                @XmlElement(name = "ToTime", required = true)
                protected String toTime;

                /**
				 * Gets the value of the fromTime property.
				 * 
				 * @return the from time possible object is {@link String }
				 */
                public String getFromTime() {
                    return fromTime;
                }

                /**
                 * Sets the value of the fromTime property.
                 * 
                 * @param pValue
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFromTime(String pValue) {
                    this.fromTime = pValue;
                }

                /**
				 * Gets the value of the toTime property.
				 * 
				 * @return the to time possible object is {@link String }
				 */
                public String getToTime() {
                    return toTime;
                }

                /**
                 * Sets the value of the toTime property.
                 * 
                 * @param pValue
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setToTime(String pValue) {
                    this.toTime = pValue;
                }

            }

        }
}
