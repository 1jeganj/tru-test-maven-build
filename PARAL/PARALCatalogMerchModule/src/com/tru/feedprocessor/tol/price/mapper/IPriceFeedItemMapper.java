package com.tru.feedprocessor.tol.price.mapper;

import java.util.List;

import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Interface IPriceFeedItemMapper.
 * 
 *  @version 1.1
 *  @author Professional Access
 * @param <T> - class which extends the BaseFeedProcessVO
 *            
 */
public interface IPriceFeedItemMapper<T extends BaseFeedProcessVO> {
	
	/**
	 * Map.
	 *
	 * @param pVOItems the vO list
	 * @return the list
	 */
	List<? extends BaseFeedProcessVO>	map(List<? extends BaseFeedProcessVO> pVOItems);
}
