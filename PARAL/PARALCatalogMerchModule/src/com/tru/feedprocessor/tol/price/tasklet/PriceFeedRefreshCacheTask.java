package com.tru.feedprocessor.tol.price.tasklet;

import atg.nucleus.ServiceMap;
import atg.repository.RepositoryImpl;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.tasklet.AbstractTasklet;

/**
 * The  class PriceFeedRefreshCacheTask 
 *  
 * This class is used to the implement the task cache refreshment after process and 
 * involved in post processing phase of feed job. Project team can override the
 * doExecute() method to implement their project specific functionality. In case
 * of exception they need to throw the FeedProcess Exception.
 * 
 *  @version 1.1
 *  @author Professional Access
 */
public class PriceFeedRefreshCacheTask extends AbstractTasklet {

	/** The m mapper map. */
	private ServiceMap mRepositoryMap = null;
	/**
	 * Gets the repository map.
	 * 
	 * @return the mRepositoryMap
	 */
	protected ServiceMap getRepositoryMap() {
		return mRepositoryMap;
	}

	/**
	 * Sets the repository map.
	 * 
	 * @param pRepositoryMap
	 *            the new repository map
	 */
	public void setRepositoryMap(ServiceMap pRepositoryMap) {
		mRepositoryMap = pRepositoryMap;
	}
	
	/**
	 *  
	 *
	 * @throws FeedSkippableException the feed skippable exception
	 * @see com.tru.feedprocessor.tol.AbstractTasklet#doExecute()
	 */
	@Override
	protected void doExecute() throws FeedSkippableException {
		if(getRepositoryMap()!=null){
			for(Object key:getRepositoryMap().keySet()){
				RepositoryImpl  repository = (RepositoryImpl )getRepositoryMap().get(key);
				repository.invalidateCaches(true);
			}
		}
	}

}
