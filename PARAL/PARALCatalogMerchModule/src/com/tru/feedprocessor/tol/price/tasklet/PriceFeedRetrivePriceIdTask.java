package com.tru.feedprocessor.tol.price.tasklet;

 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.ApplicationContextHolder;
import com.tru.feedprocessor.tol.base.EntityDataProvider;
import com.tru.feedprocessor.tol.base.tasklet.AbstractRetrieveIdForIdentifierTask;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.price.vo.PriceVO;


/**
 *  The class PriceFeedRetrivePriceIdTask
 *    
 * This is the common class which is used
 * to retreve the unique id and repository id from the corresponding data
 * source. Using teh EntiryResultExtractor it will store the data into feed
 * context.
 * 
 *  @version 1.1
 *  @author Professional Access
 */
public class PriceFeedRetrivePriceIdTask extends AbstractRetrieveIdForIdentifierTask {
	
	/**
	 * Gets the identifiers.
	 *
	 * @return the identifiers
	 * @see com.tru.feedprocessor.tol.base.tasklet.AbstractRetrieveIdForIdentifierTask#getIdentifiers()
	 */
	@Override
	protected List<String> getIdentifiers() {
		Map<String,List<? extends BaseFeedProcessVO>> lPriceDataMap = getEntityMap();
		return new ArrayList(lPriceDataMap.keySet());
	}
	
	
	/**
	 * Gets the entity map.
	 * 
	 * @return mEntityDataProvider
	 */
	protected Map<String,List<? extends BaseFeedProcessVO>>  getEntityMap(){
		EntityDataProvider lEntityDataProvider = (EntityDataProvider) retriveData(FeedConstants.ENTITY_PROVIDER);
		return lEntityDataProvider.getAllEntityList();
	}

	/**
	 * Gets the query.
	 *
	 * @param pCurrentIdentifier the current identifier
	 * @return the query
	 * @see com.tru.feedprocessor.tol.base.tasklet.AbstractRetrieveIdForIdentifierTask#getQuery(java.lang.String)
	 */
	@Override
	protected String getQuery(String pCurrentIdentifier) {
		
		StringBuffer skus = new StringBuffer();
		StringBuffer lQuery = null;
		lQuery = new StringBuffer();	
		
		Map<String,List<? extends BaseFeedProcessVO>> lPriceDataMap =  getEntityMap();
		Map<String, String> lEntitySQLMap = (HashMap<String, String>) getConfigValue(FeedConstants.ENTITY_SQL);
		
		List<? extends BaseFeedProcessVO> tempList = lPriceDataMap.get(pCurrentIdentifier );
		
		if(tempList!=null && !tempList.isEmpty()){
			int counter =0;
			for(BaseFeedProcessVO vo:tempList){
				if(counter==FeedConstants.NUMBER_THOUSAND){
					int size = skus.length();
					skus.replace(size-FeedConstants.NUMBER_ONE, size, FeedConstants.CLOSING_BRACKET);
					skus.append(FeedConstants.PRICE_OR_QUERY);
					counter=0;
				}
				PriceVO price = (PriceVO)vo;
				skus.append(FeedConstants.QUOTE+price.getSkuId()+FeedConstants.QUOTE+FeedConstants.COMA_SEPERATOR);
				counter++;
			}
			if(skus != null && skus.length() > FeedConstants.NUMBER_ONE){
				skus.replace(skus.length()-FeedConstants.NUMBER_ONE, skus.length(),FeedConstants.CLOSING_BRACKET);
			}
			lQuery.append(lEntitySQLMap.get(FeedConstants.PRICE_ENTITY_NAME).toString());
			lQuery.replace(lQuery.indexOf(FeedConstants.QUESTIONMARK),lQuery.indexOf(FeedConstants.QUESTIONMARK)+FeedConstants.NUMBER_TWO,skus.toString());

		}
		
		skus=null;
		return lQuery.toString();
	}
	
	/**
	 * Price list ids.
	 * 
	 * @return priceListId
	 */
	protected Set<String> priceListIds(){
		Set<String> priceListId = new HashSet<String>();
		List<? extends BaseFeedProcessVO> priceEntityList =  getEntityMap().get(FeedConstants.PRICE_ENTITY_NAME);
		for(BaseFeedProcessVO vo: priceEntityList){
			PriceVO price = (PriceVO)vo;
			priceListId.add(price.getPriceListId());
		}
		return priceListId;
	}
	
	/**
	 * Gets the quer map.
	 * 
	 * @param pCurrentIdentifier
	 *            - pCurrentIdentifier
	 * @return lPriceQueryMap
	 */
	protected Map<String,StringBuffer> getQuerMap(String pCurrentIdentifier) {
		String lQuery = getQuery(pCurrentIdentifier);
		Map<String,StringBuffer> lPriceQueryMap = new HashMap<String, StringBuffer>();
		
		for(String ids:priceListIds()){
			StringBuffer lMapQuery = new StringBuffer(lQuery);
			lMapQuery.replace(lMapQuery.indexOf(FeedConstants.HASH),lMapQuery.indexOf(FeedConstants.HASH)+FeedConstants.NUMBER_ONE,FeedConstants.QUOTE + ids + FeedConstants.QUOTE);
			lPriceQueryMap.put(ids, lMapQuery);
		}	
		return lPriceQueryMap;
	}
	/**
	 * Retrive entity ids. This method will get the list of entities and their
	 * corresponding sql's from feed configuration. Then it will execute all
	 * those queries using spring batch's jdbc template. Result will be store in
	 * feed data holder
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@SuppressWarnings("unchecked")
	protected void retriveIdsForIdentifiers() throws Exception {

		JdbcTemplate lJdbcTemplate = null;
		DataSource lDataSource = (DataSource) ApplicationContextHolder.getBean(getDataSourceName());
		
		if (lDataSource != null) {
			Map<String, Map<String, String>> lIdHolderMap = new HashMap<String, Map<String, String>>();
			List<String> lIdentifierList = getIdentifiers();
			Map<String, String> lIdResultMap = null;
			if (lIdentifierList != null) {
				lJdbcTemplate = new JdbcTemplate(lDataSource);
				for (String lIdentifier : lIdentifierList) {
					Map<String,StringBuffer> priceQueryMap = getQuerMap(lIdentifier);
					if (priceQueryMap != null) {
						for(Entry<String, StringBuffer> lQueryEntry: priceQueryMap.entrySet() ){
							lIdResultMap = (HashMap<String, String>) lJdbcTemplate.query(lQueryEntry.getValue().toString(), getQueryParams(lIdentifier),getIdResultSetExtractor());
							lIdHolderMap.put(lQueryEntry.getKey(), lIdResultMap);
						}
					}
				}
				storeIds(lIdHolderMap);
			}
		}
	}
}
