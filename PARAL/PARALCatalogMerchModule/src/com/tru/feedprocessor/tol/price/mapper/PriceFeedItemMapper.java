package com.tru.feedprocessor.tol.price.mapper;

import java.util.List;

import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Class PriceFeedItemMapper.
 * 
 *  @version 1.1
 *  @author Professional Access
 */
public class PriceFeedItemMapper implements IPriceFeedItemMapper<BaseFeedProcessVO> {

	/**
	 * Map.
	 *
	 * @param pVOItems the VO items
	 * @return the list<? extends base feed process v o>
	 * @see com.tru.feedprocessor.tol.price.IPriceFeedItemMapper#map(java.util.List)
	 */
	@Override
	public List<? extends BaseFeedProcessVO> map(
			List<? extends BaseFeedProcessVO> pVOItems) {
		
		return pVOItems;
	}
}
