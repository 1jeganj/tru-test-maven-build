package com.tru.feedprocessor.tol.price;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.tasklet.support.IProjectNameGenerator;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class PriceProjectNameGenerator. This class is used to generate the name which
 * will be used during project creation in BCC Project team can over ride the
 * getProjectName() method to give the project specific BCC's project name
 * 
 *  @version 1.1
 *  @author Professional Access
 */
public class PriceProjectNameGenerator implements IProjectNameGenerator {

	/**
	 *  
	 *
	 * @param pFeedExecCntx the feed exec cntx
	 * @return the project name
	 * @see com.tru.feedprocessor.tol.IProjectNameGenerator#getProjectName()
	 */
	@Override
	public String getProjectName(FeedExecutionContextVO pFeedExecCntx) {

		Calendar lCal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat(FeedConstants.TIMESTAMP_FORMAT_FOR_IMPORT, Locale.getDefault());

		return dateFormat.format(lCal.getTime()).toString();
	}

}
