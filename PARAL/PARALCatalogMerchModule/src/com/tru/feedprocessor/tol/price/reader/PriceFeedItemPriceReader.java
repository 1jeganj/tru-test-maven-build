package com.tru.feedprocessor.tol.price.reader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import atg.epub.project.Process;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.EntityDataProvider;
import com.tru.feedprocessor.tol.base.reader.AbstractFeedItemReader;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.merchutil.tol.workflow.WorkFlowTools;


/**
 * The Class PriceFeedItemPriceReader.
 * 
 *<p>
 * doOpen read the data for the entityMap, divided it into the the different
 * range and store it to UniqueSubList
 * </p>
 * 
 * <P> doRead will read next set of UniqueSubList for the process. </p>
 * 
 *  @version 1.0
 *  @author Professional Access
 */
public class PriceFeedItemPriceReader extends AbstractFeedItemReader {
	
	/** The WorkFlow Tools. */
	private WorkFlowTools mWorkFlowTools;
	
	/** The Unique sub list. */
	private ThreadLocal <List<BaseFeedProcessVO> > mUniqueSubList =  new ThreadLocal <List<BaseFeedProcessVO> > ();
	
	/** The Thread index. */
	private ThreadLocal <Integer> mThreadIndex =  new ThreadLocal <Integer> ();
	
	/**
	 * Gets the work flow tools.
	 * 
	 * @return the workFlowTools
	 */
	public WorkFlowTools getWorkFlowTools() {
		return mWorkFlowTools;
	}

	/**
	 * Sets the work flow tools.
	 * 
	 * @param pWorkFlowTools
	 *            the workFlowTools to set
	 */
	public void setWorkFlowTools(WorkFlowTools pWorkFlowTools) {
		mWorkFlowTools = pWorkFlowTools;
	}
	
	/**
	 * Next.
	 * 
	 * @return the base feed process vo
	 */
	protected   BaseFeedProcessVO next()
	{
		int lIndex = mThreadIndex.get().intValue();
		List<BaseFeedProcessVO> lBaseFeedProcessVOList = mUniqueSubList.get();
		if(FeedConstants.NUMBER_ZERO <= lIndex && lIndex < lBaseFeedProcessVOList.size())
		{
			mThreadIndex.set(Integer.valueOf(lIndex+FeedConstants.NUMBER_ONE));
			return lBaseFeedProcessVOList.get(lIndex);
		}
		return null;

	}

	/**
	 * (non-Javadoc).
	 * 
	 * @see com.tru.feedprocessor.tol.base.reader.AbstractFeedItemReader#doOpen()
	 */
	@Override
	protected void doOpen(){

		if(getConfigValue(FeedConstants.VERSIONED) != null && ((Boolean) getConfigValue(FeedConstants.VERSIONED))){
			getWorkFlowTools().pushDevelopmentLine((Process) retriveData(FeedConstants.PROCESS));
		}
		
		EntityDataProvider lEntityDataProvider = (EntityDataProvider) retriveData(FeedConstants.ENTITY_PROVIDER);
		Map<String,List<? extends BaseFeedProcessVO>> lProviderEntityList = lEntityDataProvider.getAllEntityList();
		
		List<BaseFeedProcessVO> lBaseFeedProcessVOList = new ArrayList<BaseFeedProcessVO>();
		List<String> lEntityList = getEntityList();
		
		if(lEntityList !=null){
			for(String key:lEntityList){
				lBaseFeedProcessVOList.addAll(lProviderEntityList.get(key)); 
			}
		
			lBaseFeedProcessVOList = lBaseFeedProcessVOList.subList(getStartIndex(), getLastIndex(lBaseFeedProcessVOList.size()));
		    mUniqueSubList.set(lBaseFeedProcessVOList);
		    mThreadIndex.set(Integer.valueOf(FeedConstants.NUMBER_ZERO));
		}
	}

	/**
	 * (non-Javadoc).
	 * 
	 * @return the base feed process vo
	 * @see com.tru.feedprocessor.tol.base.reader.AbstractFeedItemReader#doRead()
	 */
	@Override
	protected BaseFeedProcessVO doRead() {
		BaseFeedProcessVO nextVo = next();
		while((nextVo!=null) && (nextVo.isSkipped())){
			nextVo = next();
		}
		return nextVo;
	}
}