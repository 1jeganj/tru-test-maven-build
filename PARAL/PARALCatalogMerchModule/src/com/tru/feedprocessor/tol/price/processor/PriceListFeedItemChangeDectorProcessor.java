package com.tru.feedprocessor.tol.price.processor;

import java.util.Map;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.processor.FeedItemChangeDetectorProcessor;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Class PriceListFeedItemChangeDectorProcessor.
 * 
 * The changed detector processor is identify for isDataUpdated that override by the team by implementing lItemValueChangeInspector
 * based on the the overridden it will process the vo. and also this class need to override in the team ayer based on the VO structure.  
 *
 *  @version 1.1
 *  @author Professional Access
 */
public class PriceListFeedItemChangeDectorProcessor extends FeedItemChangeDetectorProcessor {
	
	/**
	 * The entity id map.
	 */
	private Map<String, Map<String, String>> mEntityIdMap = null;
	
	/**
	 * Check entity exists.
	 *
	 * @param pVo the vo
	 * @return true, if successful
	 */
	private boolean checkEntityExists(BaseFeedProcessVO pVo) {
			String lItemDiscriptorName = getItemDescriptorName(pVo.getEntityName());
			String lRepositoryId = mEntityIdMap.get(lItemDiscriptorName).get(pVo.getRepositoryId());
			if(StringUtils.isNotEmpty(lRepositoryId)){
				return true;
			}
		return false;
	}
	
	/**
	 * Do process.
	 *
	 * @param pFeedVO the feed vo
	 * @return the base feed process vo
	 * @throws FeedSkippableException the feed skippable exception
	 * @see com.tru.feedprocessor.tol.AbstractFeedItemProcessor#process(com.pa
	 * .ral.feedprocessor.cml.vo.BaseFeedProcessVO)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public BaseFeedProcessVO doProcess(BaseFeedProcessVO pFeedVO) throws FeedSkippableException {
		
		mEntityIdMap = (Map<String, Map<String, String>>)retriveData(getIdentifierIdStoreKey());
		if(null != mEntityIdMap && null!=pFeedVO && !checkEntityExists(pFeedVO)){
			 return pFeedVO;
		 }
		return null;
	}

	
}
