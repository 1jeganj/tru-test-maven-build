package com.tru.feedprocessor.tol.price.reader;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.reader.AbstractFeedItemReader;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.price.vo.PriceVO;

/**
 * The Class PriceFeedItemPriceListReader.
 * 
 * <p>
 * doOpen read the data for the entityMap store into Iterator
 * </p>
 * 
 * <P> doRead will read next set of Iterator for the process. </p>
 * 
 * @version 1.0
 *  @author Professional Access
 */
public class PriceFeedItemPriceListReader extends AbstractFeedItemReader {
	/**
	 * The m iterator.
	 */
	Iterator<? extends BaseFeedProcessVO> mIterator = null;
	
	/**
	 * The identifierIdStoreKey.
	 */
	private String mIdentifierIdStoreKey;
	
	/**
	 * Gets the identifier id store key.
	 * 
	 * @return the Identifier id Store Key
	 */
	public String getIdentifierIdStoreKey() {
		return mIdentifierIdStoreKey;
	}

	/**
	 * Sets the identifier id store key.
	 * 
	 * @param pIdentifierIdStoreKey
	 *            the IdentifierIdStoreKey to set
	 */
	public void setIdentifierIdStoreKey(String pIdentifierIdStoreKey) {
		mIdentifierIdStoreKey = pIdentifierIdStoreKey;
	}

	/**
	 * Next.
	 * 
	 * @return the base feed process vo
	 */
	protected BaseFeedProcessVO next() {
		if (mIterator.hasNext()) {
			return mIterator.next();
		}
		return null;
	}

	/**
	 * (non-Javadoc).
	 * 
	 * @see com.tru.feedprocessor.tol.base.reader.AbstractFeedItemReader#doOpen()
	 */
	@Override
	protected void doOpen() {
		Map<String, Map<String, String>> lEntityIdMap = (Map<String, Map<String, String>>) retriveData(getIdentifierIdStoreKey());
		
		List<BaseFeedProcessVO> lBaseFeedProcessVOList = new ArrayList<BaseFeedProcessVO>();
		
		if(lEntityIdMap!=null && !lEntityIdMap.isEmpty()){
			for(String key:lEntityIdMap.keySet()){
				PriceVO vo = new PriceVO();
				vo.setEntityName(FeedConstants.PRICELIST);
				vo.setRepositoryId(key);
				lBaseFeedProcessVOList.add(vo);
			}
		}
		
		mIterator = lBaseFeedProcessVOList.iterator();
	}

	/**
	 * (non-Javadoc).
	 * 
	 * @return the base feed process vo
	 * @see com.tru.feedprocessor.tol.base.reader.AbstractFeedItemReader#doRead()
	 */
	@Override
	protected BaseFeedProcessVO doRead() {
		BaseFeedProcessVO nextVo = next();
		while((nextVo!=null) && (nextVo.isSkipped()))
		{
			nextVo = next();
		}
		return nextVo;
	}
}