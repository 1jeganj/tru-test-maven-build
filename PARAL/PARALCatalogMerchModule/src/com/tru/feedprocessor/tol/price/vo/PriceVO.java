package com.tru.feedprocessor.tol.price.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;



/**
 * The Class PriceVO.
 *  @author Professional Access
 *  @version 1.0
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="price")
public class PriceVO extends BaseFeedProcessVO {
	
	/**
	 * Instantiates a new price vo.
	 */
	public PriceVO(){
		super.setEntityName(FeedConstants.PRICE_ENTITY_NAME);
	}
	
	/** The m price list id. */
	private String mPriceListId;
	
	/** The Sku id. */
	@XmlElement(name="skuId")
	private String mSkuId;
	
	
	/**
	 * Gets the sku id.
	 *
	 * @return the mSkuId
	 */
	public String getSkuId() {
		return mSkuId;
	}

	/**
	 * Sets the sku id.
	 *
	 * @param pSkuId the new sku id
	 */
	public void setSkuId(String pSkuId) {
		this.mSkuId = pSkuId;
	}

	/**
	 * Gets the price list id.
	 *
	 * @return the mPriceListId
	 */
	public String getPriceListId() {
		return mPriceListId;
	}

	/**
	 * Sets the price list id.
	 *
	 * @param pPriceListId the mPriceListId to set
	 */
	public void setPriceListId(String pPriceListId) {
		mPriceListId=pPriceListId;
			
	}
}
