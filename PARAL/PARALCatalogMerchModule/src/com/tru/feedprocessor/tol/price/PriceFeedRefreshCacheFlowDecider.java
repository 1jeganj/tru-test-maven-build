package com.tru.feedprocessor.tol.price;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.decider.AbstractFeedDecider;

/**
 * The Class PriceFeedRefreshCacheFlowDecider.
 * 
 * A decider for refresh cache FlowExecutionStatus returns the caches refreshment flag configure in configuration
 * 
 *  @version 1.1
 *  @author Professional Access
 */
public class PriceFeedRefreshCacheFlowDecider extends AbstractFeedDecider {

	/**
	 *  
	 *
	 * @param pJobexecution the jobexecution
	 * @param pStepexecution the stepexecution
	 * @return the flow execution status
	 * @see org.springframework.batch.core.job.flow.JobExecutionDecider#decide(org
	 * .springframework.batch.core.JobExecution,
	 * org.springframework.batch.core.StepExecution)
	 */
	@Override
	public FlowExecutionStatus doDecide(JobExecution pJobexecution,
			StepExecution pStepexecution) {
		String versionType = ((String)getConfigValue(FeedConstants.REFRESH_CACHE));
		return new FlowExecutionStatus(versionType.toUpperCase());
	}

}
