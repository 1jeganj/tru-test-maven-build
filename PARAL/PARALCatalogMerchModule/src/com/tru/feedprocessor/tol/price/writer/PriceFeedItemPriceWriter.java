package com.tru.feedprocessor.tol.price.writer;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.base.mapper.IFeedItemMapper;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.writer.AbstractFeedItemWriter;
import com.tru.feedprocessor.tol.price.PriceFeedProperty;
import com.tru.feedprocessor.tol.price.vo.PriceVO;

/**
 * The Class PriceFeedItemPriceWriter.
 * 
 * <P> Write the data into Repository by calling add or update based on the data in mEntityIdMap.
 * 
 *  @version 1.0
 *  @author Professional Access
 */
public class PriceFeedItemPriceWriter extends AbstractFeedItemWriter {

	/** The entity id map. */
	private Map<String, Map<String, String>> mEntityIdMap = null;
	
	/** The m feed catalog property. */
	private PriceFeedProperty mFeedProperty;


	/**
	 * The identifierIdStoreKey.
	 */
	private String mIdentifierIdStoreKey;
	
	/**
	 * Gets the identifier id store key.
	 * 
	 * @return the identifierIdStoreKey
	 */
	public String getIdentifierIdStoreKey() {
		return mIdentifierIdStoreKey;
	}

	/**
	 * Sets the identifier id store key.
	 * 
	 * @param pIdentifierIdStoreKey
	 *            the identifierIdStoreKey to set
	 */
	public void setIdentifierIdStoreKey(String pIdentifierIdStoreKey) {
		mIdentifierIdStoreKey = pIdentifierIdStoreKey;
	}

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the feedCatalogProperty
	 */
	public PriceFeedProperty getFeedProperty() {
		return mFeedProperty;
	}

	/**
	 * Sets the feed catalog property.
	 *
	 * @param pFeedProperty the new feed catalog property
	 */
	public void setFeedProperty(
			PriceFeedProperty pFeedProperty) {
		mFeedProperty = pFeedProperty;
	}

	/**
	 * Check entity exists.
	 *
	 * @param pVo the vo
	 * @return true, if successful
	 */
	private boolean checkEntityExists(PriceVO pVo) {
		boolean exists=false;
		if(pVo !=null){
			Map<String, String> priceMap=mEntityIdMap.get(pVo.getEntityName());
			
			if(priceMap.containsKey(pVo.getSkuId())){
				exists=true;
			}
		}
		return exists;
	}
	/**
	 * (non-Javadoc).
	 *
	 * @param pVOItems the vO items
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 * @see com.tru.feedprocessor.tol.catalog.writer.AbstractFeedItemWriter.
	 * AbstractFeedItemWriter#write(java.util.List)
	 */
	@Override
	public void doWrite(List<? extends BaseFeedProcessVO> pVOItems) throws RepositoryException, FeedSkippableException{
		if(mEntityIdMap!=null && !mEntityIdMap.isEmpty()){
			for(BaseFeedProcessVO lBaseFeedProcessVO:pVOItems){
				PriceVO lfeedVO = (PriceVO)lBaseFeedProcessVO;
					if(checkEntityExists(lfeedVO)){
						updateItem(lBaseFeedProcessVO);
					}else{
						addItem(lBaseFeedProcessVO);
					}
			}
		}
	}
	
	/** @see com.tru.feedprocessor.tol.base.writer.AbstractFeedItemWriter#doOpen()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void doOpen(){
		
		Collection values = getMapperMap().values();
		Iterator iterator = values.iterator();
		while (iterator.hasNext()) {
			AbstractFeedItemMapper itemMapper = (AbstractFeedItemMapper) iterator.next();
			itemMapper.setFeedHelper(getFeedHelper());
		}
		
		mEntityIdMap = (Map<String, Map<String, String>>)retriveData(getIdentifierIdStoreKey());
	}

	/**
	 * Update item.
	 *
	 * @param pFeedVO the feed vo
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	public void updateItem(BaseFeedProcessVO pFeedVO) throws RepositoryException, FeedSkippableException{
		PriceVO vo = (PriceVO)pFeedVO;
		MutableRepositoryItem lCurrentItem=null;
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
				
		if(lItemDiscriptorName!=null){
			IFeedItemMapper	lICatFeedItemMapper =(IFeedItemMapper) getMapperMap().get(lItemDiscriptorName);
			String repId = mEntityIdMap.get(vo.getPriceListId()).get(vo.getSkuId());
			lCurrentItem=(MutableRepositoryItem) getRepository(lRepositoryName).getItem(repId,lItemDiscriptorName);
		
			beforeUpdateItem(pFeedVO,lCurrentItem);
		
			lCurrentItem=lICatFeedItemMapper.map(pFeedVO, lCurrentItem);
			
			getRepository(lRepositoryName).updateItem(lCurrentItem);
			addUpdateItemToList(pFeedVO);
		}

	}
	/**
	 * Before add item.
	 *
	 * @param pFeedVO the feed vo
	 * @param pCurrentItem the current item
	 * @throws RepositoryException the repository exception
	 */
	public void beforeAddItem(BaseFeedProcessVO pFeedVO, MutableRepositoryItem pCurrentItem) throws RepositoryException{
		PriceVO vo = (PriceVO)pFeedVO;
		RepositoryItem lCurrentItem=null;
		String lRepositoryName =getEntityRepositoryName(pFeedVO.getEntityName());
		lCurrentItem=getRepository(lRepositoryName).getItem(vo.getPriceListId(),FeedConstants.PRICELIST);
		pCurrentItem.setPropertyValue(getFeedProperty().getPriceListName(), lCurrentItem);
	}
	
	/**
	 * Before update item.
	 *
	 * @param pFeedVO the feed vo
	 * @param pCurrentItem the current item
	 * @throws RepositoryException the repository exception
	 */
	public void beforeUpdateItem(BaseFeedProcessVO pFeedVO, MutableRepositoryItem pCurrentItem) throws RepositoryException{
		PriceVO vo = (PriceVO)pFeedVO;
		RepositoryItem lCurrentItem=null;
		String lRepositoryName =getEntityRepositoryName(pFeedVO.getEntityName());
		lCurrentItem=getRepository(lRepositoryName).getItem(vo.getPriceListId(),FeedConstants.PRICELIST);
		pCurrentItem.setPropertyValue(getFeedProperty().getPriceListName(), lCurrentItem);
	}

	/**
	 * Adds the item.
	 *
	 * @param pFeedVO the feed vo
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	public void addItem(BaseFeedProcessVO pFeedVO)throws RepositoryException, FeedSkippableException{
		MutableRepositoryItem lCurrentItem=null;
		
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
		
		if(lItemDiscriptorName!=null){
			IFeedItemMapper lIPriceFeedItemMapper =(IFeedItemMapper) getMapperMap().get(lItemDiscriptorName);
			lCurrentItem=getRepository(lRepositoryName).createItem(lItemDiscriptorName);
			beforeAddItem(pFeedVO,lCurrentItem);
			lCurrentItem=lIPriceFeedItemMapper.map(pFeedVO, lCurrentItem);
			
			getRepository(lRepositoryName).addItem(lCurrentItem);
			addInsertItemToList(pFeedVO);
		}

	}
}