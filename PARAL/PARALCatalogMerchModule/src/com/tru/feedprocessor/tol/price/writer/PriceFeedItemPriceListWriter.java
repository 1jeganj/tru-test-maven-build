package com.tru.feedprocessor.tol.price.writer;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.base.mapper.IFeedItemMapper;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.writer.AbstractFeedItemWriter;
import com.tru.feedprocessor.tol.price.vo.PriceVO;

/**
 * The Class PriceFeedItemPriceListWriter.
 * 
 * <P> Write the data into Repository by calling add based on the data in mEntityIdMap.
 * 
 *  @version 1.0
 *  @author Professional Access
 */
public class PriceFeedItemPriceListWriter extends AbstractFeedItemWriter {

	
	/**The entity id map. */
	private Map<String, String> mEntityIdMap = null;
	
	/**
	 * The identifierIdStoreKey.
	 */
	private String mIdentifierIdStoreKey;
	
	/**
	 * Gets the identifier id store key.
	 * 
	 * @return the Identifier id Store Key
	 */
	public String getIdentifierIdStoreKey() {
		return mIdentifierIdStoreKey;
	}

	/**
	 * Sets the identifier id store key.
	 * 
	 * @param pIdentifierIdStoreKey
	 *            the IdentifierIdStoreKey to set
	 */
	public void setIdentifierIdStoreKey(String pIdentifierIdStoreKey) {
		mIdentifierIdStoreKey = pIdentifierIdStoreKey;
	}

	/**
	 * Check entity exists.
	 *
	 * @param pVo the vo
	 * @return true, if successful
	 */
	private boolean checkEntityExists(PriceVO pVo) {
		boolean exists=false;
			if(mEntityIdMap.containsKey(pVo.getRepositoryId())){
				exists=true;
		}
		return exists;
	}
	/**
	 * (non-Javadoc).
	 *
	 * @param pVOItems the vO items
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 * @see com.tru.feedprocessor.tol.catalog.writer.AbstractFeedItemWriter.
	 * AbstractFeedItemWriter#write(java.util.List)
	 */
	@Override
	public void doWrite(List<? extends BaseFeedProcessVO> pVOItems) throws RepositoryException,FeedSkippableException{
		
		for(BaseFeedProcessVO lBaseFeedProcessVO:pVOItems){
			PriceVO priceVO = (PriceVO)lBaseFeedProcessVO;
				if(mEntityIdMap!=null && mEntityIdMap.isEmpty() && !checkEntityExists(priceVO)){
					addItem(lBaseFeedProcessVO);
				}
		}
	}
	/**
	 * Open.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void doOpen(){
		
		Collection values = getMapperMap().values();
		Iterator iterator = values.iterator();
		while (iterator.hasNext()) {
			AbstractFeedItemMapper itemMapper = (AbstractFeedItemMapper) iterator.next();
			itemMapper.setFeedHelper(getFeedHelper());
		}
		mEntityIdMap = (Map<String, String>)retriveData(getIdentifierIdStoreKey());
	}

	/**
	 * Update item.
	 *
	 * @param pFeedVO the feed vo
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	public void updateItem(BaseFeedProcessVO pFeedVO) throws RepositoryException, FeedSkippableException{
		MutableRepositoryItem lCurrentItem=null;
		
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
		
		if(lItemDiscriptorName!=null){
			IFeedItemMapper	lICatFeedItemMapper =(IFeedItemMapper) getMapperMap().get(lItemDiscriptorName);
			lCurrentItem=(MutableRepositoryItem) getRepository(lRepositoryName).getItem(pFeedVO.getRepositoryId(),lItemDiscriptorName);
			beforeUpdateItem(pFeedVO,lCurrentItem);
			lCurrentItem=lICatFeedItemMapper.map(pFeedVO, lCurrentItem);
			
			getRepository(lRepositoryName).updateItem(lCurrentItem);
			addUpdateItemToList(pFeedVO);
		}

	}

	/**
	 * Before update item.
	 *
	 * @param pFeedVO the feed vo
	 * @param pCurrentItem the current item
	 */
	public void beforeUpdateItem(BaseFeedProcessVO pFeedVO, MutableRepositoryItem pCurrentItem){
		//TODO
	}

	/**
	 * Adds the item.
	 *
	 * @param pFeedVO the feed vo
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	public void addItem(BaseFeedProcessVO pFeedVO)throws RepositoryException, FeedSkippableException{
		MutableRepositoryItem lCurrentItem=null;
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
		
		if(lItemDiscriptorName!=null){
			IFeedItemMapper lICatFeedItemMapper =(IFeedItemMapper) getMapperMap().get(lItemDiscriptorName);
			
			lCurrentItem=getRepository(lRepositoryName).createItem(pFeedVO.getRepositoryId(),lItemDiscriptorName);
			beforeAddItem(pFeedVO,lCurrentItem);
			lCurrentItem=lICatFeedItemMapper.map(pFeedVO, lCurrentItem);
			
			getRepository(lRepositoryName).addItem(lCurrentItem);
			addInsertItemToList(pFeedVO);
		}

	}

	/**
	 * Before add item.
	 *
	 * @param pFeedVO the feed vo
	 * @param pCurrentItem the current item
	 * @throws RepositoryException the repository exception
	 */
	public void beforeAddItem(BaseFeedProcessVO pFeedVO, MutableRepositoryItem pCurrentItem) throws RepositoryException{
		//TODO
	}
}