package com.tru.feedprocessor.tol.price;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.EntityDataProvider;
import com.tru.feedprocessor.tol.base.FeedHelper;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Class PriceFeedRangePartitioner.
 * 
 * The class making a range of partition based on grid size configure in price feed configuration
 * It will evaluate TotalSize/grid and set entity range for 'from' and 'to'  for each partition as thread
 * 
 * Suppose total feed for three entity price is 100 and the grid size is 10 configure in configuration
 * then 100/10 = 10 Thread, So for each partition that equally distributed as EntityName(From,To) and remaining will added in last thread
 * 
 *  @version 1.1
 *  @author Professional Access
 */
public class PriceFeedRangePartitioner implements Partitioner {

	/** The Feed feed helper. */
	private FeedHelper mFeedHelper;
	
	/**
	 * The mStepName.
	 */
	private String mStepName;

	/**
	 * Gets the step name.
	 * 
	 * @return the stepName
	 */
	 public String getStepName() {
		return mStepName;
	 }

	/**
	 * Gets the feed feed helper.
	 *
	 * @return the feed feed helper
	 */
	public FeedHelper getFeedHelper() {
		return mFeedHelper;
	}


	/**
	 * Sets the feed feed helper.
	 *
	 * @param pFeedHelper the new feed feed helper
	 */
	public void setFeedHelper(FeedHelper pFeedHelper) {
		this.mFeedHelper = pFeedHelper;
	}
	

	/**
	 * Gets the range.
	 *
	 * @return the range
	 */
	public int getRange() {
		
		EntityDataProvider lEntityDataProvider = (EntityDataProvider) retriveData(FeedConstants.ENTITY_PROVIDER);
		Map<String,List<? extends BaseFeedProcessVO>> lEntityList =  lEntityDataProvider.getAllEntityList();
	
		int lSize=0;
		if(lEntityList !=null){
			for(String key:lEntityList.keySet()){
				lSize = lSize + lEntityList.get(key).size();
			}
		}
		
		return lSize;
	}


	/**
	 * Partition.
	 *
	 * @param pGridSize the grid size
	 * @return the map
	 * @see org.springframework.batch.core.partition.support.Partitioner#partition(int)
	 */
	@Override
	public Map<String, ExecutionContext> partition(int pGridSize) {
		int gridSize=pGridSize;
	
		Map<String, ExecutionContext> lResult 
                       = new HashMap<String, ExecutionContext>();
		
		Long individualRange = Long.valueOf((getRange())/gridSize);
		
		Long fromId = Long.valueOf(FeedConstants.NUMBER_ZERO);
		Long toId = individualRange; //-FeedConstants.NUMBER_ONE;
		Long remainder=Long.valueOf((getRange())%(gridSize));
		
		if(individualRange==FeedConstants.NUMBER_ZERO)	{
			gridSize=FeedConstants.NUMBER_ONE;
		}
		for (int i = FeedConstants.NUMBER_ONE; i <= gridSize; i++) {
			ExecutionContext value = new ExecutionContext();
		
			if(i==gridSize)			{
				toId=toId+remainder;
			}
			
			// give each thread a name, thread 1,2,3
			
			value.putInt(FeedConstants.FROM, fromId.intValue());
			value.putInt(FeedConstants.TO, toId.intValue());
			lResult.put(FeedConstants.PARTITION + i, value);
 
			fromId = toId;// + FeedConstants.NUMBER_ONE;
			toId += individualRange;
 
		}
		
//		getPartitionManager().setRange(PartitionList);
 
		return lResult;
	}
	/**
	 * Retrive data.
	 * 
	 * @param pDataMapKey
	 *            the data map key
	 * @return the object
	 */
	public Object retriveData(String pDataMapKey) {
		return getFeedHelper().retriveData(pDataMapKey);
	}
	
	/**
	 * Gets the entity list.
	 * 
	 * @return the entity list
	 */

	public List<String> getEntityList() {
		return getFeedHelper().getStepEntityList(getStepName());
	}
	
}
