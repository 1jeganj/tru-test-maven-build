package com.tru.feedprocessor.tol.price;


/**
 * The Class PriceFeedProperty.
 * 
 *  @version 1.1
 *  @author Professional Access
 */
public class PriceFeedProperty {
	
	/**
	 * The Price list name.
	 */
	private String mPriceListName;
	
	/**
	 * Gets the price list name.
	 * 
	 * @return the priceListName
	 */
	public String getPriceListName() {
		return mPriceListName;
	}

	/**
	 * Sets the price list name.
	 * 
	 * @param pPriceListName
	 *            the priceListName to set
	 */
	public void setPriceListName(String pPriceListName) {
		mPriceListName = pPriceListName;
	}

}
