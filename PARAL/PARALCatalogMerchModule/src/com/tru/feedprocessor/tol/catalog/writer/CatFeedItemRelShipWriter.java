package com.tru.feedprocessor.tol.catalog.writer;


import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.springframework.batch.item.ItemStreamException;

import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.base.mapper.IFeedItemMapper;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.writer.AbstractFeedItemWriter;



/**
 * The Class CatFeedItemRelShipWriter.
 * 
 * <P> Write the data into Repository by calling update based on the data in mEntityIdMap. </p>
 * 
 * @version 1.1
 * @author Professional Access
 * 
 */
public class CatFeedItemRelShipWriter extends AbstractFeedItemWriter {
	
	/**
	 * Do open.
	 *
	 * @throws ItemStreamException the item stream exception
	 * @see com.tru.feedprocessor.tol.base.writer.AbstractFeedItemWriter#doOpen()
	 */
	@Override
	public void doOpen() throws ItemStreamException {
		Collection values = getMapperMap().values();
		Iterator iterator = values.iterator();
		while (iterator.hasNext()) {
			AbstractFeedItemMapper itemMapper = (AbstractFeedItemMapper) iterator.next();
			itemMapper.setFeedHelper(getFeedHelper());
		}
	}
	
	/**
	 * Do write.
	 *
	 * @param pVOItems the VO items
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 * @see com.tru.feedprocessor.tol.base.writer.AbstractFeedItemWriter#doWrite(java.util.List)
	 */
	@Override
	public void doWrite(List<? extends BaseFeedProcessVO> pVOItems)
			throws RepositoryException, FeedSkippableException{
		for(BaseFeedProcessVO lBaseFeedProcessVO:pVOItems){
			updateItem(lBaseFeedProcessVO);
			
		}

	}
	/**
	 * Sets the mapper map.
	 *
	 * @param pFeedVO the feed vo
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	protected void updateItem(BaseFeedProcessVO pFeedVO) throws RepositoryException, FeedSkippableException {
		MutableRepositoryItem lCurrentItem=null;
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
		
		if(lItemDiscriptorName!=null){
			IFeedItemMapper	lICatFeedItemMapper =(IFeedItemMapper) getMapperMap().get(pFeedVO.getEntityName());
			if(lICatFeedItemMapper!=null){
				lCurrentItem=(MutableRepositoryItem) getRepository(lRepositoryName).getItem(pFeedVO.getRepositoryId(),lItemDiscriptorName);
				beforeUpdateItem(pFeedVO,lCurrentItem);
				lCurrentItem=lICatFeedItemMapper.map(pFeedVO, lCurrentItem);
				getRepository(lRepositoryName).updateItem(lCurrentItem);

				addUpdateItemToList(pFeedVO);
			}
		}
	}
	/**
	 * Sets the mapper map.
	 *
	 * @param pFeedVO the pFeedVO
	 * @param pCurrentItem the pCurrentItem
	 * 
	 */
	private void beforeUpdateItem(BaseFeedProcessVO pFeedVO,MutableRepositoryItem pCurrentItem) {
		// TODO 
	}
}
