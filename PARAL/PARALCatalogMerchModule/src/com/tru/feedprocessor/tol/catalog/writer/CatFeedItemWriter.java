package com.tru.feedprocessor.tol.catalog.writer;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atg.nucleus.ServiceMap;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.base.mapper.IFeedItemMapper;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.writer.AbstractFeedItemWriter;
import com.tru.merchutil.tol.workflow.WorkFlowTools;


/**
 * The Class CatFeedItemWriter.
 * 
 * <P> Write the data into Repository by calling add or update based on the data in mEntityIdMap.
 * 
 * @version 1.1
 * @author Professional Access
 * 
 */
public class CatFeedItemWriter extends AbstractFeedItemWriter {
	
	/**
	 * The entity id map.
	 */
	private Map<String,Map<String,String>> mEntityIdMap = null;
	
	
	/** The m workflow tool. */
	private WorkFlowTools mWorkflowTool;
	
	/**
	 * The m mapper map.
	 */
	private ServiceMap mMapperMap = null;
	
	/**
	 * The IdentifierIdStoreKey  
	 */
	private String mIdentifierIdStoreKey;
	
	
	/**
	 * Gets the workflow tool.
	 * 
	 * @return the workflow tool
	 */
	public WorkFlowTools getWorkflowTool() {
		return mWorkflowTool;
	}

	/**
	 * Sets the workflow tool.
	 * 
	 * @param pWorkflowTool
	 *            the new workflow tool
	 */
	public void setWorkflowTool(WorkFlowTools pWorkflowTool) {
		mWorkflowTool = pWorkflowTool;
	}
	

	/**
	 * Gets the mapper map.
	 * @return the mapper map
	 */
	public ServiceMap getMapperMap(){
		return mMapperMap;
	}

	/**
	 * Sets the mapper map.
	 * 
	 * @param pMapperMap    the new mapper map
	 */
	public void setMapperMap(ServiceMap pMapperMap){
		mMapperMap = pMapperMap;
	}
	
	/** 
	 * Return IdentifierIdStoreKey
	 * 
	 * @return the IdentifierIdStoreKey 
	 * 
	 */
	public String getIdentifierIdStoreKey() {
		return mIdentifierIdStoreKey;
	}

	/**
	 * @param pIdentifierIdStoreKey the IdentifierIdStoreKey to set
	 */
	public void setIdentifierIdStoreKey(String pIdentifierIdStoreKey) {
		mIdentifierIdStoreKey = pIdentifierIdStoreKey;
	}
	
	/**
	 * doOpen.
	 * 
	 *  <p>
	 *  Execute once during calling of Open method
	 *  get the List of custom mapper and set it into abstract from the helper configuration 
	 *  </p>
	 *  
	 *  <p> Retrive mEntityIdMap from the feed session based on IdentifierIdStoreKey configure in component </p>  
	 *  
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void doOpen(){
		
		Collection values = getMapperMap().values();
		Iterator iterator = values.iterator();
		while (iterator.hasNext()) {
			AbstractFeedItemMapper itemMapper = (AbstractFeedItemMapper) iterator.next();
			itemMapper.setFeedHelper(getFeedHelper());
		}
		
		mEntityIdMap = (Map<String, Map<String, String>>)retriveData(getIdentifierIdStoreKey());
	}

	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.base.writer.AbstractFeedItemWriter#doWrite(java.util.List)
	 */
	@Override
	public void doWrite(List<? extends BaseFeedProcessVO> pVOItems)
			throws RepositoryException, FeedSkippableException {
		for(BaseFeedProcessVO lBaseFeedProcessVO:pVOItems){
			
			 String lItemDiscriptorName = getItemDescriptorName(lBaseFeedProcessVO.getEntityName());
			 
				if(mEntityIdMap!=null && mEntityIdMap.get(lItemDiscriptorName).get(lBaseFeedProcessVO.getRepositoryId())!=null){
					updateItem(lBaseFeedProcessVO);
				}else{
					addItem(lBaseFeedProcessVO);
					
				}
		}
		
	}
	/**
	 * Before update item.
	 * 
	 * <p> Need to customized by team before updateItem if any changes required in current Repository </p>
	 * 
	 * @param pFeedVO - pFeedVO
	 * @param pCurrentItem - pCurrentItem
	 * @throws RepositoryException - RepositoryException
	 * @throws FeedSkippableException - FeedSkippableException
	 */
	protected void beforeUpdateItem(BaseFeedProcessVO pFeedVO, MutableRepositoryItem pCurrentItem) throws RepositoryException,FeedSkippableException{
		//TODO method stub
	}

	/**
	 * Adds the item.
	 *
	 * <p>
	 * Get MutableRepositoryItem based on ItemDiscriptorName and set the current item from VO via map()
	 * 
	 * <p>Get the Repository based on RepositoryName and add currentItem into it </P>
	 *
	 * @param pFeedVO the feed vo
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	public void addItem(BaseFeedProcessVO pFeedVO)throws RepositoryException, FeedSkippableException{
		MutableRepositoryItem lCurrentItem=null;
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
		
		if(lItemDiscriptorName!=null){
			IFeedItemMapper lFeedItemMapper =(IFeedItemMapper) getMapperMap().get(pFeedVO.getEntityName());
			lCurrentItem=getRepository(lRepositoryName).createItem(pFeedVO.getRepositoryId(),lItemDiscriptorName);
			beforeAddItem(pFeedVO,lCurrentItem);
			lCurrentItem=lFeedItemMapper.map(pFeedVO, lCurrentItem);
			getRepository(lRepositoryName).addItem(lCurrentItem);
		
			addInsertItemToList(pFeedVO);
		}
			
	}
	/**
	 * Update item.
	 * 
	 * <p>
	 * Get MutableRepositoryItem based on ItemDiscriptorName and set the current item from VO via map()
	 * 
	 * <p>Get the Repository based on RepositoryName and update currentItem into it </P>
	 *
	 * @param pFeedVO the feed vo
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	public void updateItem(BaseFeedProcessVO pFeedVO) throws RepositoryException, FeedSkippableException{
		MutableRepositoryItem lCurrentItem=null;
		
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
		
		if(lItemDiscriptorName!=null){
			IFeedItemMapper	lFeedItemMapper =(IFeedItemMapper) getMapperMap().get(pFeedVO.getEntityName());
			lCurrentItem=(MutableRepositoryItem) getRepository(lRepositoryName).getItem(mEntityIdMap.get(lItemDiscriptorName).get(pFeedVO.getRepositoryId()),lItemDiscriptorName);
			beforeUpdateItem(pFeedVO,lCurrentItem);
			lCurrentItem = lFeedItemMapper.map(pFeedVO, lCurrentItem);
			getRepository(lRepositoryName).updateItem(lCurrentItem);
			
			addUpdateItemToList(pFeedVO);
		}
	}
	/** Before add item.
	 * 
	 * <p> Need to customized by team before addItem if any changes required in current Repository </p>
	 *  
	 * @param pFeedVO - pFeedVO
	 * @param pCurrentItem - pCurrentItem
	 * @throws RepositoryException - RepositoryException
	 * @throws FeedSkippableException - FeedSkippableException
	 */
	protected void beforeAddItem(BaseFeedProcessVO pFeedVO, MutableRepositoryItem pCurrentItem) throws RepositoryException,FeedSkippableException{
		// TO-DO
	}
}
