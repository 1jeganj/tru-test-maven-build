package com.tru.feedprocessor.tol.catalog;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.EntityDataProvider;
import com.tru.feedprocessor.tol.base.FeedHelper;
import com.tru.feedprocessor.tol.base.Range;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Class CatFeedRangePartitioner.
 * 
 * The class making a range of partition based on grid size configure in catalog feed configuration
 * It will evaluate TotalSize/grid and set entity range for 'from' and 'to'  for each partition as thread
 * 
 * Example: Suppose total feed for three entity category=15 ,product=30 and sku=55 is 100 and the grid size is 10 configure in configuration
 * then 100/10 = 10 Thread, So for each partition that equally distributed as EntityName(From,To) and remaining will added in last thread
 * 
 * Remaining for Category 15%10 = 5, Product 30%10 = 0 and  sku 55%10 = 5
 * 					
 * Where Thread 1 = Category (0,1)  
 * 					Product  (0,3)
 * 					Sku		 (0,5)	
 * 		 Thread 2 = Category (1,2)  
 * 					Product  (3,6)
 * 					Sku		 (5,10)
 * 		 Thread 3 = Category (1,2)  
 * 					Product  (3,6)
 * 					Sku		 (5,10)  and So on
 * 					.............
 *  	Thread 10 = Category (9,10+5)  here 5 is added because after equally distributed the remaining will added in last thread   
 * 					Product  (27,30+0)
 * 					Sku		 (45,50+5) here 5 is added because after equally distributed the remaining will added in last thread
 * 
 * Finally all the threads will store into ExecutionContext by a key rangeMap
 * 
 *  @version 1.1
 *  @author Professional Access
 */
public class CatFeedRangePartitioner implements Partitioner {
	
	/** The EntityDataProvider. */
	private EntityDataProvider mEntityDataProvider;
	
	/** The Feed helper. */
	private FeedHelper mFeedHelper;
	
	/** The Individual thread range. */
	private Map<String, Range> mIndividualThreadRange;
	
	/** The Entity list. */
	private List<String> mEntityList;
	
	/** The Cat feed data provider. */
	private Map<String, List<? extends BaseFeedProcessVO>> mCatFeedDataProvider;
	
	/**
	 * The mStepName.
	 */
	private String mStepName;
	
	/**
	 * Gets the step name.
	 * 
	 * @return the stepName
	 */
	public String getStepName() {
		return mStepName;
	}
	
	/**
	 * Sets the step name.
	 * 
	 * @param pStepName
	 *            the stepName to set
	 */
	public void setStepName(String pStepName) {
		mStepName = pStepName;
	}

	/**
	 * Gets the feed helper.
	 *
	 * @return the feed helper
	 */
	public FeedHelper getFeedHelper() {
		return mFeedHelper;
	}

	/**
	 * Sets the feed helper.
	 *
	 * @param pFeedHelper -  the new feed helper
	 */
	public void setFeedHelper(FeedHelper pFeedHelper) {
		this.mFeedHelper = pFeedHelper;
	}

	/**
	 * Gets the range.
	 *
	 * @return the range
	 */
	public int getRange() {
		int size=0;
		mEntityDataProvider = (EntityDataProvider) retriveData(FeedConstants.ENTITY_PROVIDER);
		mCatFeedDataProvider = mEntityDataProvider.getAllEntityList();
		if (mCatFeedDataProvider!= null){
			for(String entity:getEntityList()){
				if(mCatFeedDataProvider.get(entity)!=null){
					size += mCatFeedDataProvider.get(entity).size();
				}
			}
			return size;
		}
		else{
			return FeedConstants.NUMBER_ONE;
		}
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.partition.support.Partitioner#partition(int)
	 */
	@Override
	public Map<String, ExecutionContext> partition(int pGridSize) {
		int temp = pGridSize;
		mEntityList = getEntityList();
		mIndividualThreadRange = new HashMap<String, Range>();

		Map<String, ExecutionContext> result = new HashMap<String, ExecutionContext>();

		if (((getRange()) / temp) == FeedConstants.NUMBER_ZERO) {

			temp = FeedConstants.NUMBER_ONE;
		}
		if (mEntityList != null) {
			for (int i = FeedConstants.NUMBER_ONE; i <= temp; i++) {

				ExecutionContext value = new ExecutionContext();
				for (String EntityName : mEntityList) {
					int entitySize;

					if (mCatFeedDataProvider != null) {
						entitySize = (mCatFeedDataProvider
								.get(EntityName) == null) ? FeedConstants.NUMBER_ZERO
								: mCatFeedDataProvider
										.get(EntityName).size();
					} else {
						entitySize = FeedConstants.NUMBER_ZERO;
					}

					Long individualRange = Long.valueOf((entitySize) / temp);

					Range CurrentEntityRange = null;
					Long fromId = null;
					Long toId = null;
					Long remainder = null;

					if (i == FeedConstants.NUMBER_ONE) {
						fromId = Long.valueOf(FeedConstants.NUMBER_ZERO);
						if (individualRange == FeedConstants.NUMBER_ZERO) {
							toId = Long.valueOf(FeedConstants.NUMBER_ZERO);
						} else {
							toId = individualRange;
						}

					} else {
						CurrentEntityRange = mIndividualThreadRange
								.get(EntityName);
						fromId = Long.valueOf(CurrentEntityRange.getFrom());
						toId = Long.valueOf(CurrentEntityRange.getTo());

						if (individualRange == FeedConstants.NUMBER_ZERO) {
							fromId = Long.valueOf(FeedConstants.NUMBER_ZERO);
							toId = Long.valueOf(FeedConstants.NUMBER_ZERO);
						} else {
							fromId = toId ;
							toId += individualRange;
						}
					}
					remainder = Long.valueOf((entitySize) % (temp));
					if (i == temp) {
						toId = toId + remainder;

					}
					
					mIndividualThreadRange.put(EntityName, new Range(fromId
							.intValue(), toId.intValue()));
				}
				Map<String, Range> lThreadRange = new HashMap<String, Range>();
				lThreadRange.putAll(mIndividualThreadRange);
				value.put(FeedConstants.RANGE_MAP, lThreadRange);
				result.put(FeedConstants.PARTITION + i, value);
			}
		}
		return result;
	}
	/**
	 * Retrive data.
	 * 
	 * @param pDataMapKey
	 *            the data map key
	 * @return the object
	 */
	public Object retriveData(String pDataMapKey) {
		return getFeedHelper().retriveData(pDataMapKey);
	}
	
	/**
	 * Gets the entity list.
	 * 
	 * @return the entity list
	 */

	public List<String> getEntityList() {
		return getFeedHelper().getStepEntityList(getStepName());
	}
}
