package com.tru.feedprocessor.tol.catalog;

import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.config.AbstractFeedConfig;

/**
 * The Class CatFeedConfig.
 * 
 * The class is basically used to set the corresponding property parameters configure in the catalog configuration component.
 * and after property set it notify in console for missing mandatory parameter from configuration
 *   
 * @version 1.1
 * @author Professional Access
 * 
 */
public class CatFeedConfig extends AbstractFeedConfig{

	/**
	 * After properties set.
	 *
	 * @throws Exception the exception
	 * @see com.tru.feedprocessor.tol.AbstractFeedConfig#afterPropertiesSet()
	 */
	@Override
	 public void afterPropertiesSet() throws Exception{

			if(StringUtils.isBlank( getConfigValue(FeedConstants.FILENAME).toString())){
				getLogger().logError("File name can not be null in catalog feed configuration");
			}
			if(StringUtils.isBlank( getConfigValue(FeedConstants.FILEPATH).toString())){
				getLogger().logError("File path can not be null in catalog feed configuration");
			}
			if(StringUtils.isBlank( getConfigValue(FeedConstants.ENTITY_LIST).toString())&& StringUtils.isBlank( getConfigValue(FeedConstants.STEP_ENTITY_LIST).toString())){
				getLogger().logError("Entity list or Step entity list can not be null in catalog feed configuration");
			}
			if(StringUtils.isBlank(getConfigValue(FeedConstants.ENTITY_SQL).toString())){
				getLogger().logError("Entity sql can not be null in catalog feed configuration");
			}
			if(StringUtils.isBlank(getConfigValue(FeedConstants.ENTITYTOITEMDESCRIPTONNAMEMAP).toString())){
				getLogger().logError("entityToItemDescriptorNameMap can not be null in catalog feed configuration");
			}
			if(StringUtils.isBlank(getConfigValue(FeedConstants.ENTITYTOREPOSITORYNAMEMAP).toString())){
				getLogger().logError("entityToRepositoryNameMap can not be null in catalog feed configuration");
			}
			String voName = (String) getConfigValue(FeedConstants.DATA_LOADER_CLASS_FQCN);
			if(StringUtils.isBlank(voName)){
				getLogger().logError("File Loader class FQCN can not be null in Catalog feed configuration");
			}
			try {
				Class.forName(voName);
			} catch (ClassNotFoundException e) {
			if (isLoggingError()) {
					logError("File Parse class FQCN must be a valid entry", e);	
				}
			}
			
			for(String key :((List<String>)getConfigValue(FeedConstants.ENTITY_LIST))){
				if(!((Map<String, String>)getConfigValue(FeedConstants.ENTITY_SQL)).containsKey(key)){
					getLogger().logError("SQL is mandatory for "+key+" entity provided in entity list in Catalog feed configuration");
				}
					
			}
			
			if(!StringUtils.isBlank(getConfigValue(FeedConstants.FILE_DOWNLOADER_FLAG).toString()) 
					&& getConfigValue(FeedConstants.FILE_DOWNLOADER_FLAG).equals(Boolean.toString(FeedConstants.TRUE))){
				
				if(StringUtils.isBlank(getConfigValue(FeedConstants.FTP_SFTP_HOST).toString())){
					getLogger().logError("Host Name for FTP and SFTP is mandatory for file downloading in catalog feed configuration");
				}
				if(StringUtils.isBlank(getConfigValue(FeedConstants.FTP_SFTP_PORT).toString())){
					getLogger().logError("Port NO for FTP and SFTP is mandatory for file downloading in catalog feed configuration");
				}
				if(StringUtils.isBlank(getConfigValue(FeedConstants.FPT_SFTP_USER).toString())){
					getLogger().logError("Server IP for FTP and SFTP is mandatory for file downloading in catalog feed configuration");
				}
				if(StringUtils.isBlank(getConfigValue(FeedConstants.FPT_SFTP_PASSWORD).toString())){
					getLogger().logError("Server IP for FTP and SFTP is mandatory for file downloading in catalog feed configuration");
				}
				if(StringUtils.isBlank(getConfigValue(FeedConstants.FPT_SFTP_SOURCE_DIR).toString())){
					getLogger().logError("Server IP for FTP and SFTP is mandatory for file downloading in catalog feed configuration ");
				}
				if(StringUtils.isBlank( getConfigValue(FeedConstants.FILEPATH).toString())){
					getLogger().logError("File path can not be null in catalog feed configuration");
				}	
				if(StringUtils.isBlank( getConfigValue(FeedConstants.PROTOCOL_TYPE).toString())){
					getLogger().logError("Protocol type for FTP or SFTP can not be null in catalog feed configuration");
				}
				if(StringUtils.isBlank( getConfigValue(FeedConstants.DOWNLOAD_FILETYPE).toString())){
					getLogger().logError("Download file type can not be null in catalog feed configuration");
				}	
			}
			
	
		 
 }

}
