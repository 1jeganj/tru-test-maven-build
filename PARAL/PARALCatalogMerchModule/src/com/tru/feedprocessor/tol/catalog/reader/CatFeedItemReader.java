package com.tru.feedprocessor.tol.catalog.reader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import atg.epub.project.Process;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.EntityDataProvider;
import com.tru.feedprocessor.tol.base.Range;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.reader.AbstractFeedItemReader;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.merchutil.tol.workflow.WorkFlowTools;

/**
 * The Class CatFeedItemReader
 * 
 * <p>
 * doOpen read the data for the entityMap, divided it into the the different
 * range and store it to mBaseFeedProcessVOList
 * </p>
 * <P> doRead will read next set of UniqueSubList for the process. </p>
 * 
 * @version 1.0
 * @author Professional Access.
 * 
 */
public class CatFeedItemReader extends AbstractFeedItemReader {

	/** The WorkFlow Tools. */
	private WorkFlowTools mWorkFlowTools;
	
	/** The Unique sub list. */
	private ThreadLocal<List<BaseFeedProcessVO>> mUniqueSubList = new ThreadLocal<List<BaseFeedProcessVO>>();

	/** The Thread index. */
	private ThreadLocal<Integer> mThreadIndex = new ThreadLocal<Integer>();

	/** The Cat feed data provider. */
	private Map<String,List<? extends BaseFeedProcessVO>> mCatFeedDataProvider;
	
	/**
	 * The EntityList.
	 */
	private List<String> mEntityList=null;

	/**
	 * The m EntityDataProvider.
	 */
	private EntityDataProvider mEntityDataProvider;
	
	/**
	 * Gets the work flow tools.
	 * 
	 * @return the workFlowTools
	 */
	public WorkFlowTools getWorkFlowTools() {
		return mWorkFlowTools;
	}

	/**
	 * Sets the work flow tools.
	 * 
	 * @param pWorkFlowTools
	 *            the workFlowTools to set
	 */
	public void setWorkFlowTools(WorkFlowTools pWorkFlowTools) {
		mWorkFlowTools = pWorkFlowTools;
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.batch.item.ItemStream#open(org.springframework.batch
	 * .item.ExecutionContext)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public synchronized void doOpen() {
		
		getWorkFlowTools().pushDevelopmentLine((Process)retriveData(FeedConstants.PROCESS));

		mEntityDataProvider = (EntityDataProvider) retriveData(FeedConstants.ENTITY_PROVIDER);
		mCatFeedDataProvider = mEntityDataProvider.getAllEntityList();

		List<BaseFeedProcessVO> lBaseFeedProcessVOList = new ArrayList<BaseFeedProcessVO>();
		
		mEntityList= getEntityList();
		
		Map<String, Range> lRange = getRange();
		for (String currentEntity : mEntityList) {
			List<BaseFeedProcessVO> tempEntityList = (List<BaseFeedProcessVO>) mCatFeedDataProvider.get(currentEntity);
			Range lCurrentEntityRange = lRange.get(currentEntity);
			if (tempEntityList != null && !tempEntityList.isEmpty() && lCurrentEntityRange.getTo() != 0) {
				lBaseFeedProcessVOList.addAll(tempEntityList.subList(lCurrentEntityRange.getFrom(),lCurrentEntityRange.getTo()));
			}
		}
		mUniqueSubList.set(lBaseFeedProcessVOList);
		mThreadIndex.set(Integer.valueOf(FeedConstants.NUMBER_ZERO));
   	}

	/**
	 * Next.
	 * 
	 * @return the base feed process vo
	 */ 
	protected BaseFeedProcessVO next() {

		int lIndex = mThreadIndex.get().intValue();
		List<BaseFeedProcessVO> lBaseFeedProcessVOList = mUniqueSubList.get();
		if (0 <= lIndex && lIndex < lBaseFeedProcessVOList.size()) {
			mThreadIndex.set(Integer.valueOf(lIndex + FeedConstants.NUMBER_ONE));
			return lBaseFeedProcessVOList.get(lIndex);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.batch.item.ItemReader#read()
	 */
	@Override
	public BaseFeedProcessVO doRead() throws FeedSkippableException {
		
		BaseFeedProcessVO nextVo = next();
		while((nextVo!=null) && (nextVo.isSkipped()))
		{
			nextVo = next();
		}
		
		return nextVo;
	}

}
