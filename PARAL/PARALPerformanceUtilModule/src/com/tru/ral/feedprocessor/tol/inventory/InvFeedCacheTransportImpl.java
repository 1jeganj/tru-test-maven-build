/*
* Copyright (C) 2015, Professional Access Pvt Ltd.
* All Rights Reserved. No use, copying or distribution
* of this work may be made except in accordance with a
* valid license agreement from PA, India. This notice
* must be included on all copies, modifications and 
* derivatives of this work.
* 
*/
package com.tru.ral.feedprocessor.tol.inventory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.adapter.gsa.GSARepository;
import atg.commerce.inventory.CachingInventoryManager;
import atg.core.util.StringUtils;
import atg.repository.MutableRepository;

import com.tru.ral.merchutil.tol.cache.AbstractCacheTransportImpl;

/**
 * The Class InvFeedCacheTransportImpl.
 * 
 * @author BPatel (PA)
 * @version 1.1
 * 
 */
public class InvFeedCacheTransportImpl extends AbstractCacheTransportImpl {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new inv feed cache transport impl.
	 * 
	 * @throws RemoteException
	 *             - RemoteException
	 */
	public InvFeedCacheTransportImpl() throws RemoteException {
		super();
	}

	
	
	/**
	 * Holds the inv cache size.
	 */
	private int mInvCacheSize = 0;


	/**
	 * Returns the property to hold mSize.
	 * 
	 * @return the invCacheSize
	 */
	public int getInvCacheSize() {
		return mInvCacheSize;
	}


	/**
	 * Sets the property to hold mSize.
	 * 
	 * @param pInvCacheSize - pInvCacheSize to set
	 * 
	 */
	public void setInvCacheSize(int pInvCacheSize) {
		mInvCacheSize = pInvCacheSize;
	}
	
	/**
	 * Property to hold mIncventoryCachePath.
	 */
	private CachingInventoryManager mInventoryCachePath;


	/**
	 * Gets the inventory cache path.
	 * 
	 * @return the mInventoryCachePath
	 */
	public CachingInventoryManager getInventoryCachePath() {
		return mInventoryCachePath;
	}

	/**
	 * Sets the inventory cache path.
	 * 
	 * @param pInventoryCachePath
	 *            the pInventoryCachePath to set
	 */
	public void setInventoryCachePath(CachingInventoryManager pInventoryCachePath) {
		mInventoryCachePath = pInventoryCachePath;
	}
	
	/**
	 * This method will invalidateInventoryCache for the Repository.
	 * 
	 * @param pRepositoryPath
	 *            - pRepositoryPath
	 * @param pMap
	 *            - pMap
	 * @throws RemoteException
	 *             - RemoteException
	 */
	@Override
	public void invalidateCache(String pRepositoryPath, Map<String, Set<String>> pMap) throws RemoteException {
		if (isLoggingDebug()) {
			vlogDebug(
					"CacheTransportImpl.invalidateInventoryCache method BEGIN :::::repository path is::{0}, map is :: {1}",
					pRepositoryPath,
					pMap);
		}
		if (pMap == null) {
			return;
		}
		if (!StringUtils.isBlank(pRepositoryPath) && getRepositoryMap() != null && getRepositoryMap().get(pRepositoryPath) != null) {
			final MutableRepository repository = (MutableRepository) getRepositoryMap().get(pRepositoryPath);
			vlogDebug("repository" + repository);
			if (repository != null) {
				if (pMap != null && !pMap.isEmpty()) {
					if (isLoggingDebug()) {
						vlogDebug("Entered into Clearing Repository Item cache::::{0}", pMap);
					}
					Set<String> cacheFlushItems = null;
					List<String> skuIds = null;
					Iterator<String> iterator = pMap.keySet().iterator();
					if (iterator != null) {
						String itemDescName = null;
						while (iterator.hasNext()) {
							itemDescName = iterator.next();
							vlogDebug("itemDescName " + itemDescName);
							if (itemDescName != null && pMap.get(itemDescName) != null
									&& pMap.get(itemDescName).size() <= getInvCacheSize()) {
								if (isLoggingDebug()) {
									vlogDebug("Invoking invalidateRepositoryItem as the map size is below the limit specified");
								}
								cacheFlushItems = pMap.get(itemDescName);
								if (isLoggingDebug()) {
									vlogDebug("CacheTransportImpl - cacheFlushItems : {0}", cacheFlushItems);
								}
								skuIds = new ArrayList<String>(cacheFlushItems);
								
								if (isLoggingDebug()) {
									vlogDebug("CacheTransportImpl - skuIds : {0}", skuIds);
								}
								getInventoryCachePath().flushCache(skuIds);

							} else{
								if (isLoggingDebug()) {
									vlogDebug("Invoking invalidateFullRepository as the map size exceeds the limit specified");
								}
								getInventoryCachePath().flushCache();
							}
						}
					}
				} else {
					if (pMap.isEmpty()) {
						if (isLoggingDebug()) {
							vlogDebug("Invoking gsaRepository.invalidateCaches() as the map is empty");
						}
						// invalidate cache of entire repository only when it is empty but not when it is null
						if (repository != null) {
							GSARepository gsaRepository = (GSARepository) repository;
							gsaRepository.invalidateCaches();
						}
					}
				}
			} else {
				if (isLoggingDebug()) {
					vlogDebug("Unable to get the Repository component from the service map for " + pRepositoryPath);
				}
			}
		} else {
			if (isLoggingDebug()) {
				vlogDebug("Repository path is empty or null");
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("CacheTransportImpl.invalidateInventoryCache method End.Cache invalidated");
		}
	}

}
