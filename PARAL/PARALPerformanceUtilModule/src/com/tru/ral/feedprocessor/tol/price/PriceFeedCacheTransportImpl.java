/*
* Copyright (C) 2015, Professional Access Pvt Ltd.
* All Rights Reserved. No use, copying or distribution
* of this work may be made except in accordance with a
* valid license agreement from PA, India. This notice
* must be included on all copies, modifications and 
* derivatives of this work.
* 
*/
package com.tru.ral.feedprocessor.tol.price;

import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import atg.adapter.gsa.GSARepository;
import atg.commerce.pricing.priceLists.PriceCacheAdapter;
import atg.core.util.StringUtils;
import atg.repository.MutableRepository;

import com.tru.ral.merchutil.tol.cache.AbstractCacheTransportImpl;
import com.tru.ral.performanceutil.cml.cache.PARALPageCacheConstants;

/**
 * The Class PriceFeedCacheTransportImpl.
 * 
 * @author BPatel (PA)
 * @version 1.1
 * 
 */
public class PriceFeedCacheTransportImpl extends AbstractCacheTransportImpl{


	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new price feed cache transport impl.
	 * 
	 * @throws RemoteException
	 *             - RemoteException
	 */
	public PriceFeedCacheTransportImpl() throws RemoteException {
		super();
	}

	/**
	 * Holds the price cache size.
	 */
	private int mPriceCacheSize = 0;
	
	/**
	 * Property to hold mPriceCachePath.
	 */
	private PriceCacheAdapter mPriceCachePath;

	
	/**
	 * Returns the holds the price cache size.
	 * 
	 * @return the priceCacheSize
	 */
	public int getPriceCacheSize() {
		return mPriceCacheSize;
	}

	/**
	 * Sets the holds the price cache size.
	 * 
	 * @param pPriceCacheSize the pPriceCacheSize to set
	 */
	public void setPriceCacheSize(int pPriceCacheSize) {
		mPriceCacheSize = pPriceCacheSize;
	}
	
	/**
	 * Gets the price cache path.
	 * 
	 * @return the mPriceCachePath
	 */
	public PriceCacheAdapter getPriceCachePath() {
		return mPriceCachePath;
	}


	/**
	 * Sets the price cache path.
	 * 
	 * @param pPriceCachePath
	 *            the pPriceCachePath to set
	 */
	public void setPriceCachePath(PriceCacheAdapter pPriceCachePath) {
		mPriceCachePath = pPriceCachePath;
	}

	
	/**
	 * This method is used for invalidating the price cache for repositories.
	 * 
	 * @param pRepositoryPath
	 *            - pRepositoryPath
	 * @param pMap
	 *            - Map [key is item descriptor name and value is set of
	 *            repository ids]
	 * @throws RemoteException
	 *             - RemoteException
	 */
	@Override
	public void invalidateCache(String pRepositoryPath, Map<String, Set<String>> pMap) throws RemoteException {
		if (isLoggingDebug()) {
			vlogDebug(
					"CacheTransportImpl.invalidatePriceCache method BEGIN :::::repository path is::{0}, map is :: {1}",
					pRepositoryPath,
					pMap);
		}
		// Clearing Price Object cache
		if (getPriceCachePath() != null && pRepositoryPath.contains(PARALPageCacheConstants.PRICE)) {
			if (isLoggingDebug()) {
				vlogDebug("Entered into Clearing Price Object cache");
			}
			getPriceCachePath().getPriceListManager().getPriceCache().flush();
		}

		if (pMap == null) {
			return;
		}

		if (!StringUtils.isBlank(pRepositoryPath) && getRepositoryMap() != null	&& getRepositoryMap().get(pRepositoryPath) != null) {
			final MutableRepository repository = (MutableRepository) getRepositoryMap().get(pRepositoryPath);
			if (repository != null) {
				if (pMap != null && !pMap.isEmpty()) {
					if (isLoggingDebug()) {
						vlogDebug("Entered into Clearing Repository Item cache::::{0}", pMap);
					}
					Iterator<String> iterator = pMap.keySet().iterator();
					if (iterator != null) {
						String itemDescName = null;
						while (iterator.hasNext()) {
							itemDescName = iterator.next();
							if (itemDescName != null && pMap.get(itemDescName) != null
									&& pMap.get(itemDescName).size() <= getPriceCacheSize()) {
								if (isLoggingDebug()) {
									vlogDebug("Invoking invalidateRepositoryItem as the map size is below the limit specified");
								}
								invalidateRepositoryItem(repository, itemDescName, pMap.get(itemDescName));

							} else
							{
								if (isLoggingDebug()) {
									vlogDebug("Invoking invalidateFullRepository as the map size exceeds the limit specified");
								}
								invalidateFullRepository(repository, itemDescName, pRepositoryPath);
							}

						}
					}
				} else {
					if (pMap.isEmpty()) {
						if (isLoggingDebug()) {
							vlogDebug("Invoking gsaRepository.invalidateCaches() as the map is empty");
						}
						// invalidate cache of entire repository only when it is empty but not when it is null
						if (repository != null) {
							GSARepository gsaRepository = (GSARepository) repository;
							gsaRepository.invalidateCaches();
						}
					}
				}
			} else {
				if (isLoggingDebug()) {
					vlogDebug("Unable to get the Repository component from the service map for " + pRepositoryPath);
				}
			}
		} else {
			if (isLoggingDebug()) {
				vlogDebug("Repository path is empty or null");
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("CacheTransportImpl.invalidatePriceCache method End ");
		}
	}
}
