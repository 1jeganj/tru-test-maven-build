package com.tru.ral.performanceutil.fhl.cacheinvalidation;

import java.util.Enumeration;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceMap;
import atg.repository.Repository;
import atg.repository.RepositoryImpl;
/**
 * This class provides method to invalidate the cache components.
 * 
 * @author P.A
 *
 */
public class PARALCacheInvalidatorService extends GenericService {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * mComponentMap
	 */
	private ServiceMap mComponentMap;
	/**
	 * mFlushCacheAllInstances
	 */
	private boolean mFlushCacheAllInstances;
	/**
	 * mRepository
	 */
	private Repository mRepository;

	/**
	 *<b>This Method will do following Operations:</b><BR>
	 * 1) It will fetch the components. 2) Flush the cache.
	 */
	public void invalidateCache() {
		if (isLoggingDebug()) {
			logDebug("Entering into DropletCacheInvalidation.invalidateCache method:");
		}
		if(isFlushCacheAllInstances()){
			RepositoryImpl repositoryImpl = (RepositoryImpl) getRepository();
			repositoryImpl.invalidateCaches();
		}
		if(getComponentMap() != null){
			Enumeration enumValues = getComponentMap().elements();
			while(enumValues.hasMoreElements()) {
				Object component = enumValues.nextElement();
				if(component != null) {
					if(component instanceof atg.service.cache.Cache) {
						atg.service.cache.Cache cacheComponent = (atg.service.cache.Cache)component;
						if(isLoggingInfo()) {
							logInfo("DropletCacheInvalidation, trying to invoke flush on service cache " + cacheComponent);
						}
						cacheComponent.flush();
						if(isLoggingInfo()) {
							logInfo("DropletCacheInvalidation, flush completed on " + cacheComponent);
						}
					} else if(component instanceof atg.droplet.Cache) {
						atg.droplet.Cache dropletCacheComp = (atg.droplet.Cache) component;
						if(isLoggingInfo()) {
							logInfo("trying to invoke flush on droplet cache" + dropletCacheComp);
						}
						dropletCacheComp.flushCache();
						if(isLoggingInfo()) {
							logInfo("flush completed with droplet cache" + dropletCacheComp);
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("exiting from DropletCacheInvalidation.invalidateCache method:");
		}
	}

	/**
	 * @return the componentMap
	 */
	public ServiceMap getComponentMap() {
		return mComponentMap;
	}

	/**
	 * @param pComponentMap the componentMap to set
	 */
	public void setComponentMap(ServiceMap pComponentMap) {
		mComponentMap = pComponentMap;
	}

	/**
	 * @return the flushCacheAllInstances
	 */
	public boolean isFlushCacheAllInstances() {
		return mFlushCacheAllInstances;
	}

	/**
	 * @param pFlushCacheAllInstances the flushCacheAllInstances to set
	 */
	public void setFlushCacheAllInstances(boolean pFlushCacheAllInstances) {
		mFlushCacheAllInstances = pFlushCacheAllInstances;
	}

	/**
	 * @return the repository
	 */
	public Repository getRepository() {
		return mRepository;
	}

	/**
	 * @param pRepository the repository to set
	 */
	public void setRepository(Repository pRepository) {
		mRepository = pRepository;
	}
}
