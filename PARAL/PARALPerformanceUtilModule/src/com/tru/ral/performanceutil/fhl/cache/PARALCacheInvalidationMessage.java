/*
* Copyright (C) 2013, Professional Access Pvt Ltd.
 * All Rights Reserved. No use, copying or distribution
 * of this work may be made except in accordance with a
 * valid license agreement from Chain Reaction Cycles LTD. This notice
 * must be included on all copies, modifications and
 * derivatives of this work.*/

package com.tru.ral.performanceutil.fhl.cache;

import java.io.Serializable;

import atg.nucleus.dms.DASMessage;
/**
 * This class is a message  that is used as invalidation message related to droplet cache.
 * @author PA
 *
 */
public class PARALCacheInvalidationMessage extends DASMessage implements Serializable {

	/** reference for servialVersionUID */
	private static final long serialVersionUID = -4791562578930066086L;

	/**
	 * key of the entry
	 */
	private String mEntryKey;

	/**
	 * pattern that should be matched
	 */
	private boolean mPaternMatch = true;

	/**
	 * @return mEntryKey entryKey
	 */
	public String getEntryKey() {
		return mEntryKey;
	}

	/**
	 * @param pEntryKey the mEntryKey to set
	 */
	public void setEntryKey(String pEntryKey) {
		this.mEntryKey = pEntryKey;
	}

	/**
	 * @return mPaternMatch - pattern match
	 */
	public boolean isPaternMatch() {
		return mPaternMatch;
	}

	/**
	 * @param pPaternMatch the mPaternMatch to set
	 */
	public void setPaternMatch(boolean pPaternMatch) {
		this.mPaternMatch = pPaternMatch;
	}

	/**
	 * This is a constructor with params pEntryKey and pPaternMatch
	 * @param pEntryKey
	 * 			entry key
	 * @param pPaternMatch 
	 * 			pattern match
	 */
	public PARALCacheInvalidationMessage(String pEntryKey, boolean pPaternMatch){
		mEntryKey = pEntryKey;
		mPaternMatch = pPaternMatch;
	}
	/**
	 * This is a constructor with params pEntryKey
	 * @param pEntryKey
	 * 			entry key	
	 */
	public PARALCacheInvalidationMessage(String pEntryKey){
		mEntryKey = pEntryKey;
	}

	/**
	 * This is default constructor.
	 */
	public PARALCacheInvalidationMessage(){
		super();
	}
}
