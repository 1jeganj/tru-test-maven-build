/*
* Copyright (C) 2013, Professional Access Pvt Ltd.
 * All Rights Reserved. No use, copying or distribution
 * of this work may be made except in accordance with a
 * valid license agreement from Chain Reaction Cycles LTD. This notice
 * must be included on all copies, modifications and
 * derivatives of this work.*/

package com.tru.ral.performanceutil.fhl.cache;

import java.io.IOException;
import java.io.Serializable;
import java.net.URLDecoder;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import atg.droplet.Cache;
import atg.nucleus.Nucleus;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.tru.ral.performanceutil.cml.cache.PARALPageCacheConstants;

/**
 * This 'PARALCacheDroplet' class extends the droplet cache and implements the
 * Serializable interface to perform the following operations:
 * 1)removedCachedElement method is used to remove the cached data based on key.
 * 2)flushCache method is used to clear the cache. 
 * 3)setCachedContent method is used to set the key and value into the map. 
 * 4)getCachedContent method is used to get the cached data based on key.
 * 5)and some specific methods are implemented to perform dyn/admin related functionality
 * ie. adding the cache statistics in admin page.
 * 
 * @author PA
 * @version 1.0
 * 
 */
public class PARALCacheDroplet extends Cache implements Serializable {
	
	/** Holds cacheContainer object */
	private atg.service.cache.Cache mCacheContainer;
	/** reference for serialVersionUID */
	private static final long serialVersionUID = -6542052658889335187L;
	/**
	 * Getter method for cacheContainer
	 * @return the cacheContainer
	 */
	public atg.service.cache.Cache getCacheContainer() {
		return mCacheContainer;
	}
	/**
	 * Setter method for cacheContainer
	 * @param pCacheContainer the cacheContainer to set
	 */
	public void setCacheContainer(atg.service.cache.Cache pCacheContainer) {
		mCacheContainer = pCacheContainer;
	}

	/**
	 * This method is to remove cached element with key
	 * @param pKey - key
	 */
	public void removeCachedElement(String pKey) {
		if(isLoggingDebug()){
			logDebug("Entering into PARALCacheDroplet.removeCachedElement() method");
			logDebug("Key name is: " + pKey);
		}
		getCacheContainer().remove(pKey);
		if (isLoggingDebug()) {
			logDebug("Removed key from cacheContainer....: " + pKey);
            logDebug("Exiting from PARALCacheDroplet.removeCachedElement() method");
		}
	}
	/**
	 * Forces cache to flush the data.
	 */
	public void flushCache() {
		if(isLoggingDebug()){
			logDebug("Entering into PARALCacheDroplet.flushCache()");
		}
		getCacheContainer().flush();
		if(isLoggingDebug()){
			logDebug("Exiting from PARALCacheDroplet.flushCache()");
		}
	}

	/**
	 * Creates and returns a new Servlet that will administer this service.
	 * @return CacheAdminServlet method
	 **/
	protected Servlet createAdminServlet() {
		return new CacheAdminServlet(this, getNucleus());
	}

	/**
	 * 'CacheAdminServlet' class will administer this service
	 * 
	 * @author Professional Access Pvt Ltd
	 * @version 1.0
	 * 
	 */
	@SuppressWarnings("rawtypes")
	class CacheAdminServlet extends atg.nucleus.ServiceAdminServlet {

		/** reference for serialVersionUID */
		private static final long serialVersionUID = -8339065070629251261L;

		/**
		 * Constructs an instance of CacheAdminServlet
		 * 
		 * @param pService - server
		 * @param pNucleus - nucleus
		 */
		public CacheAdminServlet(Object pService, Nucleus pNucleus) {
			super(pService, pNucleus);
		}

		/**
		 * 'normalAdmin' method Performs all admin functionality, then adds on
		 * the cache statistics
		 * 
		 * @param pRequest		- reference for HttpServletRequest
		 * @param pOut		    - reference for ServletOutputStream
		 * @throws IOException	if any
		 * 
		 */
		protected void normalAdmin(HttpServletRequest pRequest,ServletOutputStream pOut) 
				throws IOException {
			pOut.println(PARALPageCacheConstants.FORM_ACTION + pRequest.getRequestURI()+ PARALPageCacheConstants.METHOD_POST2);
			pOut.println(PARALPageCacheConstants.INPUT_TYPE_HIDDEN_NAME_REMOVE_ITEMS_FROM_CACHE_VALUE_TRUE);
			pOut.println(PARALPageCacheConstants.H2_CACHE_USAGE_STATISTICS_H2);
			pOut.println(PARALPageCacheConstants.TABLE_BORDER);
			String[] globalKeys = { PARALPageCacheConstants.ENTRY, PARALPageCacheConstants.CREATION_TIME, PARALPageCacheConstants.CACHE_SIZE,PARALPageCacheConstants.CACHE_DATA, PARALPageCacheConstants.REMOVE_ITEMS_FROM_CACHE };
			// print header
			pOut.println(PARALPageCacheConstants.ROW_TABLE_START);
			for (int i = PARALPageCacheConstants.INTEGER_ZERO; i < globalKeys.length; i++) {
				pOut.println(PARALPageCacheConstants.HEAD_TABLE_START + globalKeys[i] + PARALPageCacheConstants.HEAD_TABLE_END);
			}
			pOut.println(PARALPageCacheConstants.ROW_TABLE_END);
			// print cache entries
			int nEntries = PARALPageCacheConstants.INTEGER_ZERO;
			int byteSize = PARALPageCacheConstants.INTEGER_ZERO;
			int byteEntries = PARALPageCacheConstants.INTEGER_ZERO;
			int charSize = PARALPageCacheConstants.INTEGER_ZERO;
			int charEntries = PARALPageCacheConstants.INTEGER_ZERO;

			for (Iterator e = getCacheContainer().getAllKeys(); e.hasNext(); nEntries++) {
				if (nEntries > PARALPageCacheConstants.MAX_VIWABLE_ENTRIES) {
					// too many entries, print ... and finish
					pOut.print(PARALPageCacheConstants.TR_TH_COLSPAN_3);
					pOut.println(PARALPageCacheConstants.TH_TR);
					break;
				}
				Object cacheDataKey = e.next();
				Object cachedKey = null;
				if(cachedKey == null){
					try {
						cachedKey = getCacheContainer().get(cacheDataKey);
					} catch (Exception e1) {
						if(isLoggingError()){
							logError("Exception is occurred while getting the cached key",e1);
						}
					}
				}
				Date creationTime = new Date();
				int len = PARALPageCacheConstants.INTEGER_ZERO;
				String[] itemCacheValues = {
						String.valueOf(nEntries),creationTime.toString(),String.valueOf(len),
						PARALPageCacheConstants.A_HREF_CACHE_DATA_KEY + ServletUtil.escapeURLString(cacheDataKey
								.toString()) + PARALPageCacheConstants.STRING_BACK_SLASH + cacheDataKey
								+ PARALPageCacheConstants.A_HYPERLINK,
								PARALPageCacheConstants.INPUT_TYPE_CHECKBOX_NAME_REMOVE_VALUE
								+ ServletUtil.escapeURLString(cacheDataKey.toString()) + PARALPageCacheConstants.STRING_BACK_SLASH };

				pOut.println(PARALPageCacheConstants.ROW_TABLE_START);
				for (int i = PARALPageCacheConstants.INTEGER_ZERO; i < itemCacheValues.length; i++) {
					pOut.println(PARALPageCacheConstants.HEAD_TABLE_START + itemCacheValues[i] + PARALPageCacheConstants.HEAD_TABLE_END);
				}
				pOut.println(PARALPageCacheConstants.ROW_TABLE_END);
			}
			pOut.println(PARALPageCacheConstants.TABLE_END);
			pOut.println(PARALPageCacheConstants.INPUT_TYPE_SUBMIT_VALUE_REMOVE_ITEMS_FROM_CACHE_NAME_SUBMIT);
			pOut.println(PARALPageCacheConstants.FORM);
			if (nEntries <= PARALPageCacheConstants.MAX_VIWABLE_ENTRIES) {
				StringBuffer sb = new StringBuffer(PARALPageCacheConstants.P_CACHE_CONTAINS);
				sb.append(byteEntries).append(PARALPageCacheConstants.ENTRIES_CONTAINING);
				sb.append(byteSize).append(PARALPageCacheConstants.BYTES_AND).append(charEntries);
				sb.append(PARALPageCacheConstants.CONTAINING).append(charSize).append(PARALPageCacheConstants.CHARACTERS);
				pOut.println(sb.toString());
			}
		}

		/**
		 * Performs all print admin functionality, then adds on the cache statistics
		 * 
		 * @param pRequest          	- reference for HttpServletRequest
		 * @param pResponse         	- reference for HttpServletResponse
		 * @param pOut          		- reference for ServletOutputStream
		 * @throws ServletException 	-    if any
		 * @throws IOException			- if any
		 * 
		 */
		@Override
		protected void printAdmin(HttpServletRequest pRequest, HttpServletResponse pResponse, ServletOutputStream pOut)
				throws ServletException, IOException {
			String cacheDataKey = pRequest.getParameter(PARALPageCacheConstants.CACHE_DATA_KEY);
			String removeItemsFromCache = pRequest.getParameter(PARALPageCacheConstants.REMOVE_ITEMS_FROM_CACHE);
			if (isLoggingDebug()) {
				logDebug("inside method..........printAdmin");
				logDebug("cacheDataKey........:" +cacheDataKey);
				logDebug("removeItemsFromCache....: " +removeItemsFromCache);
			}
		    // then you are going to the normal admin page use to work
			if (cacheDataKey == null && removeItemsFromCache == null) {
				// normalAdmin(pRequest, pResponse, pOut);
				normalAdmin(pRequest, pOut);
			}
			if (cacheDataKey != null) {
				// you want to see the data for one cache name
				viewCacheItemAdmin(pRequest, pOut);
			}
			if (removeItemsFromCache != null) {
				// you want to remove one or more items from the cache
				removeItemsFromCacheAdmin(pRequest, pResponse, pOut);
			}
			if (isLoggingDebug()) {
				logDebug("exiting from method..........printAdmin");
			}
		}

		/**
		 * Performs all view cache item admin functionality, then adds on the cache
		 * statistics.
		 *
		 * @param pRequest 			- HttpServletRequest
		 * @param pOut 				- ServletOutputStream
		 * @throws IOException 		if any
		 */
		private void viewCacheItemAdmin(HttpServletRequest pRequest, ServletOutputStream pOut)
				throws IOException {
			String cacheDataKey = pRequest.getParameter(PARALPageCacheConstants.CACHE_DATA_KEY);
			if (cacheDataKey == null) {
				return;
			}
			pOut.println(PARALPageCacheConstants.H2_CACHE_ITEM_DATA_IS_BELOW_H2);
			pOut.println(PARALPageCacheConstants.TABLE_BORDER);
			pOut.println(PARALPageCacheConstants.TR_TD_H3_DATA_H3_TD_TR);
			pOut.println(PARALPageCacheConstants.TR_TD_START);
			Iterator e = getCacheContainer().getAllKeys();
			String cacheEntry = null;
			while (e.hasNext()) {
				Object elem = e.next();
				if (elem.toString().equals(cacheDataKey)) {
					try {
						cacheEntry = (String) getCacheContainer().get(elem);
					} catch (Exception e1) {
						if(isLoggingError()){
							logError("Exception is occurred while getting the cached element.",e1);
						}
					}
					break;
				}
			}
			String cacheData = null;
			if (cacheEntry != null) {
				cacheData = ServletUtil.escapeHtmlString(cacheEntry);
			} else {
				cacheData = PARALPageCacheConstants.COULDN_T_FIND_DATA_FOR_KEY + cacheDataKey;
			}

			pOut.println(cacheData);
			pOut.println(PARALPageCacheConstants.TD_TR_END);

			pOut.println(PARALPageCacheConstants.TR_TD_H3_HTML_H3_TD_TR);
			pOut.println(PARALPageCacheConstants.TR_TD_START);
			cacheData = cacheEntry == null ? PARALPageCacheConstants.NULL : cacheEntry;
			pOut.println(cacheData);
			pOut.println(PARALPageCacheConstants.TD_TR_END);
			pOut.println(PARALPageCacheConstants.TABLE_END);
		}
	}

	/**
	 * 'removeItemsFromCacheAdmin' method is used to remove items from
	 * cache admin
	 * 
	 * @param pRequest		- HttpServletRequest
	 * @param pResponse		- HttpServletResponse
	 * @param pOut			- ServletOutputStream
	 * @throws ServletException if any
	 * @throws IOException	if any
	 */
	@SuppressWarnings("deprecation")
	void removeItemsFromCacheAdmin(final HttpServletRequest pRequest,
			final HttpServletResponse pResponse, final ServletOutputStream pOut)
					throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("inside method removeItemsFromCacheAdmin");
		}
		String[] removeItems = pRequest.getParameterValues(PARALPageCacheConstants.REMOVE_CACHE_KEY);
		if (removeItems != null) {
			for (int i = PARALPageCacheConstants.INTEGER_ZERO; i < removeItems.length; i++) {
				String decodeKeyName = URLDecoder.decode(removeItems[i]);
				removeCachedElement(decodeKeyName);
			}
			pOut.println(PARALPageCacheConstants.H2_CACHE_ITEMS_HAVE_BEEN_REMOVED_H2);
		} else {
			pOut.println(PARALPageCacheConstants.H2_NO_CACHE_ITEMS_WERE_SELECTED_FOR_REMOVAL_H2);
		}
		if (isLoggingDebug()) {
			logDebug("exiting from  method removeItemsFromCacheAdmin");
		}
	}

	/**
	 * Returns the cached content for the given request. The key for the content
	 * is a the "key" parameter - the "key"
	 * parameter must be specified. 
	 * 
	 * @param pRequest	- reference for DynamoHttpServletRequest
	 * @return cache data / null
	 */
	public char[] getCachedContent(DynamoHttpServletRequest pRequest) {
		if(isLoggingDebug()){
			logDebug("Entering into PARALCacheDroplet.getCachedContent()");
		}
		char[] data = null;
		Object key = pRequest.getLocalParameter(PARALPageCacheConstants.KEY);
		if (isLoggingDebug()) {
			logDebug("Key name is........:" +key);
		}
		Object cachedContent = null;
		if (key == null) {
			return null;
		} else {
			try {
				cachedContent = getCacheContainer().get(key);
				if(cachedContent == null){
					return null;
				}
				data = cachedContent.toString().toCharArray();
			} catch (Exception e) {
				if(isLoggingError()){
					logError("Exception is occurred while getting the value", e);
				}
			}
			if (isLoggingDebug()) {
				logDebug("Cached Object: " +data.toString());
				logDebug("Exiting from PARALCacheDroplet.getCachedContent()");
			}
		}
		return data;
	}

	/**
	 * Sets the cached content for the given request. The key for the content is
	 * the "key" parameter, which must be
	 * specified.
	 * 
	 * @param pRequest			- reference for DynamoHttpServletRequest
	 * @param pContent			- reference for content
	 * 
	 */
	public void setCachedContent(DynamoHttpServletRequest pRequest,char[] pContent) {
		if (isLoggingDebug()) {
			logDebug("Entering into PARALCacheDroplet.setCachedContent()");
		}
		//super.getEnabled();
		Object key = pRequest.getLocalParameter(PARALPageCacheConstants.KEY);
		if (key == null) {
			return;
		}
		String cacheKey = (String) key;
		String cData = new String(pContent);
		if(cData != null){
			getCacheContainer().put(cacheKey, cData);
		}
		if (isLoggingDebug()) {
			logDebug("Exting from PARALCacheDroplet.setCachedContent()");
		}
	}
}