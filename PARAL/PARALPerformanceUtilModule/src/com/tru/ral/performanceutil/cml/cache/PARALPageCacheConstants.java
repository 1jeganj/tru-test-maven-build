package com.tru.ral.performanceutil.cml.cache;

/**
 * This class holds CacheDroplet Constants.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class PARALPageCacheConstants {

	/** constant for DROPLET_CACHING_ENABLED */
	public static final Boolean DROPLET_CACHING_ENABLED = false;
	/** constant for OBJECT_CACHING_ENABLED */
	public static final Boolean OBJECT_CACHING_ENABLED = true;
	/** Constant for PARAL_CACHE_DROPLET component */
	public static final String PARAL_CACHE_DROPLET = "PARALCacheDroplet";
	/** constant for NOT_A_CACHE_INVALIDATION_MESSAGE */
	public static final String NOT_A_CACHE_INVALIDATION_MESSAGE = "Not a CacheInvalidationMessage";
	/** constant for NOT_AN_OBJECT_MESSAGE */
	public static final String NOT_AN_OBJECT_MESSAGE = "Not an ObjectMessage";
	/** constant for STRING_EMPTY string */
	public static final String STRING_EMPTY = " ";
	/** reference for MY_RESOURCE_NAME */
	public static final String MY_RESOURCE_NAME = "atg.droplet.DropletResources";
	/** reference for 20000 */
	public static final int MAX_VIWABLE_ENTRIES = 20000;
	/** reference for cacheInvalidMessage */
	public static final String CACHE_INVALID_MESSAGE = "cacheInvalidMessage";
	/** reference for cacheNoKey */
	public static final String CACHE_NO_KEY = "cacheNoKey";
	/** reference for cacheContent */
	public static final String CACHE_CONTENT = "cacheContent";
	/** reference for CACHE_CHECK_SECONDS */
	public static final String CACHE_CHECK_SECONDS = "cacheCheckSeconds";
	/** reference for MAX_VIWABLE_ENTRIES */
	public static final String KEY = "key";
	/** reference for // */
	public static final String STRING_DOUBLE_SLASH = "//";
	/** reference for ### */
	public static final String STRING = "###";
	/** reference for true */
	public static final String TRUE = "true";
	/** constant for LONG_THOUSAND_MILISECOND */
	public static final long LONG_THOUSAND_MILISECOND = 1000L;
	/** reference for CACHE_DATA_KEY */
	public static final String CACHE_DATA_KEY = "cacheDataKey";
	/** reference for constant for REMOVE_ITEMS_FROM_CACHE */
	public static final String REMOVE_ITEMS_FROM_CACHE = "removeItemsFromCache";
	/** constant for INTEGER_ZERO */
	public static final int INTEGER_ZERO = 0;
	/** constant for INTEGER_THREE */
	public static final int INTEGER_THREE = 3;
	/** constant for INTEGER_NEGATIVE_ONE */
	public static final Long INTEGER_NEGATIVE_ONE = -1L;
	/** constant for INTEGER_FIFTY */
	public static final int INTEGER_FIFTY = 50;
	/** constant for INTEGER_HUNDRED */
	public static final int INTEGER_HUNDRED = 100;
	/** constant for ATG_JMS_DELIVERY_DATE */
	public static final String ATG_JMS_DELIVERY_DATE = "ATG_JMS_DELIVERY_DATE";
	/** reference for OUTPUT */
	public static final String OUTPUT = "output";
	/** reference for NO_DATA_STRING */
	public static final String NO_DATA_STRING = "no data";
	/** reference for DOTTED_STRINGS */
	public static final String DOTTED_STRINGS = "....";
	/** reference for ROW_TABLE_END */
	public static final String ROW_TABLE_END = "</tr>";
	/** reference for HEAD_TABLE_END */
	public static final String HEAD_TABLE_END = "</th>";
	/** reference for ROW_TABLE_START */
	public static final String HEAD_TABLE_START = "<th>";
	/** reference for ROW_TABLE_START */
	public static final String ROW_TABLE_START = "<tr>";

	/** reference for REMOVE_CACHE_KEY*/
	public static final String REMOVE_CACHE_KEY = "remove";
	/** reference for CACHE_DATA*/
	public static final String CACHE_DATA = "cacheData";
	/** reference for CACHE_SIZE*/
	public static final String CACHE_SIZE = "cacheSize";
	/** reference for CREATION_TIME*/
	public static final String CREATION_TIME = "creationTime";
	/** reference for ENTRY*/
	public static final String ENTRY = "entry";
	/** reference for H2_CACHE_USAGE_STATISTICS_H2*/
	public static final String H2_CACHE_USAGE_STATISTICS_H2 = "<h2>Cache usage statistics</h2>";
	/** reference for INPUT_TYPE_HIDDEN_NAME_REMOVE_ITEMS_FROM_CACHE_VALUE_TRUE*/
	public static final String INPUT_TYPE_HIDDEN_NAME_REMOVE_ITEMS_FROM_CACHE_VALUE_TRUE = "<input type=\"hidden\" name=\"removeItemsFromCache\" value=\"true\">";
	/** reference for TH_TR*/
	public static final String TH_TR = "</th></tr>";
	/** reference for TR_TH_COLSPAN_3*/
	public static final String TR_TH_COLSPAN_3 = "<tr><th colspan=3>   ..............";
	/** reference for A_HYPERLINK*/
	public static final String A_HYPERLINK = "</a>";
	/** reference for A_HREF_CACHE_DATA_KEY*/
	public static final String A_HREF_CACHE_DATA_KEY = "<a href=\"?cacheDataKey=";
	/** reference for INPUT_TYPE_CHECKBOX_NAME_REMOVE_VALUE*/
	public static final String INPUT_TYPE_CHECKBOX_NAME_REMOVE_VALUE = "<input type=\"checkbox\" name=\"remove\" value=\"";
	/** reference for FORM*/
	public static final String FORM = "</form>";
	/** reference for INPUT_TYPE_SUBMIT_VALUE_REMOVE_ITEMS_FROM_CACHE_NAME_SUBMIT*/
	public static final String INPUT_TYPE_SUBMIT_VALUE_REMOVE_ITEMS_FROM_CACHE_NAME_SUBMIT = "<input type=\"submit\" value=\"Remove items from Cache\" name\"submit\">";
	/** reference for CHARACTERS*/
	public static final String CHARACTERS = " characters.";
	/** reference for CONTAINING*/
	public static final String CONTAINING = " containing ";
	/** reference for TR_TD_H3_DATA_H3_TD_TR*/
	public static final String TR_TD_H3_DATA_H3_TD_TR = "<tr><td><h3>Data</h3></td></tr>";
	/** reference for TR_TD_H3_HTML_H3_TD_TR*/
	public static final String TR_TD_H3_HTML_H3_TD_TR = "<tr><td><h3>Html</h3></td></tr>";
	/** reference for TABLE_BORDER*/
	public static final String TABLE_BORDER = "<table border>";
	/** reference for H2_CACHE_ITEM_DATA_IS_BELOW_H2*/
	public static final String H2_CACHE_ITEM_DATA_IS_BELOW_H2 = "<h2>Cache item data is below</h2>";
	/** reference for COULDN_T_FIND_DATA_FOR_KEY*/
	public static final String COULDN_T_FIND_DATA_FOR_KEY = "couldn't find data for key =";
	/** reference for TABLE_END*/
	public static final String TABLE_END = "</table>";
	/** reference for TD_TR_END*/
	public static final String TD_TR_END = "</td></tr>";
	/** reference for TR_TD_START*/
	public static final String TR_TD_START = "<tr><td>";
	/** reference for BYTES_AND*/
	public static final String BYTES_AND = " bytes and ";
	/** reference for P_CACHE_CONTAINS*/
	public static final String P_CACHE_CONTAINS = "<p>Cache contains ";
	/** reference for METHOD_POST2*/
	public static final String METHOD_POST2 = "\" method=\"post\">";
	/** reference for FORM_ACTION*/
	public static final String FORM_ACTION = "<form action=\"";
	/** reference for STRING_SEMI_COLAN*/
	public static final String STRING_SEMI_COLAN = ";";
	/** reference for STRING_EQUAL*/
	public static final String STRING_EQUAL = "=";
	/** reference for JSESSIONID*/
	public static final String JSESSIONID = ";jsessionid=";
	/** reference for H2_NO_CACHE_ITEMS_WERE_SELECTED_FOR_REMOVAL_H2*/
	public static final String H2_NO_CACHE_ITEMS_WERE_SELECTED_FOR_REMOVAL_H2 = "<h2>No cache items were selected for removal</h2>";
	/** reference for H2_CACHE_ITEMS_HAVE_BEEN_REMOVED_H2*/
	public static final String H2_CACHE_ITEMS_HAVE_BEEN_REMOVED_H2 = "<h2>Cache items have been removed</h2>";
	/** reference for ENTRIES_CONTAINING*/
	public static final String ENTRIES_CONTAINING = "entries, containing";
	/** reference for NULL*/
	public static final String NULL = "null";
	/** reference for STRING_BACK_SLASH*/
	public static final String STRING_BACK_SLASH = "\">";
	/** The constant for  PARAM_KEY_WITH_PAGE_URL. */
	public static final String PARAM_KEY_WITH_PAGE_URL = "keyWithPageURL";
	/** The Constant for STRING_BOOLEAN_FALSE. */
	public static final String STRING_BOOLEAN_FALSE = "false";
	/** The Constant for STRING_BOOLEAN_TRUE. */
	public static final String STRING_BOOLEAN_TRUE = "true";
	/** Constant to hold the new line string */
	public static final String NEW_LINE = "\n";
	/** Constant to hold the SIZE string */
	public static final String SIZE = "size";
	/** Constant to hold the TIME string */
	public static final String TIME = "time";
	/** Constant to hold the DATE string */
	public static final String DATE = "date";
	
	/** Constant to hold the entity Name string */
	public static final String PRICE = "price";

}