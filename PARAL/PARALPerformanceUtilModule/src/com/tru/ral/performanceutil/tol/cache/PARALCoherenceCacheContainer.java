package com.tru.ral.performanceutil.tol.cache;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import atg.core.util.StringUtils;
import atg.nucleus.ServiceException;
import atg.service.cache.Cache;
import atg.service.cache.CacheAdapter;

import com.tangosol.net.CacheFactory;
import com.tangosol.net.NamedCache;
import com.tangosol.net.cache.NearCache;
import com.tru.ral.performanceutil.cml.cache.PARALPageCacheConstants;

/**
 * This Class works as a container for coherence cache to perform following
 * functionality: 
 * 1)This class is extending the OOTB service Cache class to
 * provide the implementation for all public method as per coherence cache
 * requirement.
 * 2)get method of service cache class to get the cache value from
 * coherence cache.
 * 3)put method of service cache class to put the key and value
 * in the NearCache map of Coherence cache.
 * 4)flush method is used to clear the
 * coherence cache.
 * 
 * @author PA
 * 
 */
@SuppressWarnings("serial")
public class PARALCoherenceCacheContainer extends Cache {

	/**
	 * Property for invalidating the cache on startup
	 */
	private boolean mInvalidateCachesAtStartup;
	
    /** Property created for internal use */
	private boolean mUseCoherence;
	/**
	 * Property to hold CacheSchema name
	 */
	private String mCacheSchema;
	/**
	 * Property to Coherence Properties like local storage, cache config and pof file location
	 */
	private Map<String, String> mCoherenceSystemProperties;
	/** Second order cache to keep existing caches */
    private final Map<String, NamedCache> mMap = new ConcurrentHashMap<String, NamedCache>();
	/**
	 * Property to hold default time to live
	 */
	private long mDefaultTTL;
	/**
	 * Property to hold mCoherenceCacheKey
	 */
	private CacheAdapter mCacheAdapter;
	
   /**
	 * Getter method for defaultTTL
	 * @return the defaultTTL
	 */
	public long getDefaultTTL() {
		return mDefaultTTL;
	}

	/**
	 * Setter method for defaultTTL
	 * @param pDefaultTTL the defaultTTL to set
	 */
	public void setDefaultTTL(long pDefaultTTL) {
		mDefaultTTL = pDefaultTTL;
	}

/**
	 * Getter method for useCoherence
	 * @return the useCoherence
	 */
	public boolean isUseCoherence() {
		return mUseCoherence;
	}

	/**
	 * Setter method for useCoherence
	 * @param pUseCoherence the useCoherence to set
	 */
	public void setUseCoherence(boolean pUseCoherence) {
		mUseCoherence = pUseCoherence;
	}
	
    /**
	 * Getter method for coherenceSystemProperties
	 * @return the coherenceSystemProperties
	 */
	public Map<String, String> getCoherenceSystemProperties() {
		return mCoherenceSystemProperties;
	}

	/**
	 * Setter method for coherenceSystemProperties
	 * @param pCoherenceSystemProperties the coherenceSystemProperties to set
	 */
	public void setCoherenceSystemProperties(
			Map<String, String> pCoherenceSystemProperties) {
		mCoherenceSystemProperties = pCoherenceSystemProperties;
	}
	/**
	 * Getter method for cacheSchema
	 * @return the cacheSchema
	 */
	public String getCacheSchema() {
		return mCacheSchema;
	}

	/**
	 * Setter method for cacheSchema
	 * @param pCacheSchema the cacheSchema to set
	 */
	public void setCacheSchema(String pCacheSchema) {
		mCacheSchema = pCacheSchema;
	}

	/**
	 * Getter method for cacheAdapter
	 * @return the cacheAdapter
	 */
	public CacheAdapter getCacheAdapter() {
		return mCacheAdapter;
	}

	/**
	 * Setter method for cacheAdapter
	 * @param pCacheAdapter the cacheAdapter to set
	 */
	public void setCacheAdapter(CacheAdapter pCacheAdapter) {
		mCacheAdapter = pCacheAdapter;
	}
	
	/**
	 * Getter method for invalidateCachesAtStartup
	 * @return the invalidateCachesAtStartup
	 */
	public boolean isInvalidateCachesAtStartup() {
		return mInvalidateCachesAtStartup;
	}

	/**
	 * Setter method for invalidateCachesAtStartup
	 * @param pInvalidateCachesAtStartup the invalidateCachesAtStartup to set
	 */
	public void setInvalidateCachesAtStartup(boolean pInvalidateCachesAtStartup) {
		mInvalidateCachesAtStartup = pInvalidateCachesAtStartup;
	}
	
	/**
	 * This function is an overridden function. This will initialize Coherence
	 * on startup which will ensure whether the coherence server is up. If not,
	 * it will user cache factory to make it up This function will also check if
	 * we need to clear the cache on start of the server. If yes, it will flush
	 * all the schemas cache
	 * 
	 * @throws ServiceException
	 *             when service is not accessible.
	 */
	public void doStartService() throws ServiceException {

		if (isLoggingDebug()) {
			logDebug("Entering into PARALCoherenceCacheContainer.doStartService : BEGIN");
		}
		if (!isUseCoherence()) {
			return;
		}
		initializeCoherenceRelatedState();
		super.doStartService();
		if (isInvalidateCachesAtStartup()) {
			if (isLoggingDebug()) {
				logDebug("Invalidating caches at startup becasue invalidateCachesAtStartup=true");
			}
			flush();
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from PARALCoherenceCacheContainer.doStartService : END");
		}
	}
	
	/**
	 * This overrided method will be used to get an object from coherence cache
	 * by using the key.
	 * 
	 * @param pKey
	 *            -Key as Object type.
	 * @return key -value as Object type
	 */
	@Override
	public Object get(Object pKey) {
		if (isLoggingDebug()) {
			logDebug("Entering into PARALCoherenceCacheContainer.get method");
			logDebug("Cached Key Name is:"+ pKey);
		}
		if (pKey == null) {
			return null;
		}
		if (!isUseCoherence()) {
		   return null;
		}
		String cacheName = getCacheSchema();
		Object obj = get(pKey,cacheName);
		if (isLoggingDebug()) {
			logDebug("Exiting from PARALCoherenceCacheContainer.get method");
		}
		return obj;
	}
	
	/**
	 * This private method will be used to get the cache based schema name.
	 * @param pKey 
	 *           As Object type.
	 * @param pCacheName
	 *           As String type.
	 * @return cacheObject as Object type.
	 */
	@SuppressWarnings("unused")
	private Object get(Object pKey, String pCacheName) {
		if (isLoggingDebug()) {
			logDebug("Entering into PARALCoherenceCacheContainer.get private method");
			logDebug("Cached Key Name is: "+ pKey);
			logDebug("Cache Schema name is: "+ pCacheName);
		}
		if(pKey == null && StringUtils.isBlank(pCacheName)){
			return null;
		}
		if (!isUseCoherence()) {
			return null;
		}
		Object cachedObject = null;
		NamedCache cache = getCache(pCacheName);
		Object cacheElement = null;
		if (cache != null) {
			cachedObject = cache.get(pKey);
			if(cachedObject == null){
				try {
					if(mCacheAdapter != null){
						cacheElement = mCacheAdapter.getCacheElement(pKey);
						return cacheElement;
					}
				} catch (Exception e) {
					if(isLoggingError()){
						logError("Adapter is not defined: returning null value", e);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from  PARALCoherenceCacheContainer.get private method");
		}
		return cachedObject;
	}

	/**
	 * This OOTB overrided method is used to get the cache object.
	 * 
	 * @param pKeys
	 *            -as Object array.
	 * @return Object array
	 */
	@Override
	public Object[] get(Object[] pKeys) {
		if (isLoggingDebug()) {
			logDebug("Entering into PARALCoherenceCacheContainer.get(Object[]) method");
		}
		if(pKeys == null){
			return null;
		}
		if (isUseCoherence()) {
			return null;
		}
		Object keyObj[] = new Object[pKeys.length];
		for (int i=0;i < pKeys.length;i++) {
		    Object val = get(pKeys[i]);
		    keyObj[i] = val;
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from PARALCoherenceCacheContainer.get(Object[]) method");
		}
		return keyObj;
	}
	
	/**
	 * This method is used to get the cache schema Object for a particular
	 * schema We are following NamedCache as we are using schema names to get
	 * the same
	 * 
	 * @param pCacheName
	 *            is the cache name in which we need to add the cache object The
	 *            cacheName property should map to coherence-cache-config.xml,
	 *            which is the named cache that we use.
	 * @return NamedCache -a Coherence cache MAP
	 */
	public NamedCache getCache(String pCacheName) {
		if (isLoggingDebug()) {
			logDebug("Entering into PARALCoherenceCacheContainer.getCache method --To initalize the cache");
		}
		if (!isUseCoherence()) {
			return null;
		}
		if (!(CacheFactory.getCluster().isRunning())) {
			initializeCoherenceRelatedState();
		}
        return initializeCache(pCacheName);
	}

	/**
	 * This function do the following operations::: Ensures the Coherence cache
	 * cluster Sets the cacheName properties for different caches. Initializes
	 * Coherence-related system properties.
	 * 
	 */
	private void initializeCoherenceRelatedState() {
		if (isLoggingDebug()) {
			logDebug("Entering into PARALCoherenceCacheContainer.initializeCoherenceRelatedState method --To read the config files and to ensure the coherence cluster");
		}
		if (getCoherenceSystemProperties() != null) {
			for (Map.Entry<String, String> entry : getCoherenceSystemProperties().entrySet()) {
			    System.setProperty(entry.getKey(), entry.getValue());
			}
		}
		CacheFactory.ensureCluster();
	}

	/**
	 * This method is used to initialize the cache map.
	 * 
	 * @param pCacheName
	 *            -as String type.
	 * @return NamedCache -as coherence map.
	 */
    @SuppressWarnings({ "unused", "rawtypes" })
	private NamedCache initializeCache(String pCacheName) {
    	if (isLoggingDebug()) {
			logDebug("Entering into PARALCoherenceCacheContainer.initializeCache method --called from getCache()");
		}
        NamedCache cache = null;
        if (!mMap.containsKey(pCacheName)) {
            synchronized (mMap) {
                if (!mMap.containsKey(pCacheName)) {
                    cache = CacheFactory.getCache(pCacheName);
                    if (cache instanceof NearCache) {
                        Map frontMap = ((NearCache) cache).getFrontMap();
                    }
                    mMap.put(pCacheName, cache);
                }
            }
        }
        if (cache == null) {
            cache = mMap.get(pCacheName);
        }
        if (isLoggingDebug()) {
			logDebug("Exiting from PARALCoherenceCacheContainer.initializeCache method");
		}
        return cache;
    }

	/**
	 * This overrided method will be used to check the key availability in map.
	 * 
	 * @param pKey
	 *            key as Object Type.
	 * @return boolean as true and false.
	 */
    @Override
    public boolean contains(Object pKey) {
    	if(pKey == null){
    		return false;
    	}
    	NamedCache cache = null;
    	String cacheName = getCacheSchema();
    	if(cacheName != null){
    	  cache = getCache(cacheName);
    	}
    	return cache.containsKey(pKey);
    }

	/**
	 * This overrided method will be used to check the arrays of key
	 * availability in initialized cache map.
	 * 
	 * @param pKeys
	 *            keys as array of Object Type.
	 * @return boolean as true and false.
	 */
    @Override
    public boolean contains(Object[] pKeys) {
    	if(pKeys == null){
    		return false;
    	}
    	String cacheName = getCacheSchema();
		NamedCache cache = getCache(cacheName);
		if(cache != null){
			for(int i = 0; i < pKeys.length; i++) {
	    		if(!cache.containsKey(pKeys[i])) {
	    		   return false;
	    		}
	    	}
		}
        return true;
    }
    
	/**
	 * This overrided method of service cache will be used to flush the
	 * coherence cache.
	 */
    @Override
    public synchronized void flush() {
    	if(isLoggingDebug()){
			logDebug("Entering into PARALCoherenceCacheContainer.flush method");
    	}
    	flush(getCacheSchema());
    	if(isLoggingDebug()){
			logDebug("Exiting from PARALCoherenceCacheContainer.flush method");
		}
    }
    
	/**
	 * This method flushes the cache for a particular schema like droplet etc It
	 * will take the cache name as a parameter
	 * 
	 * @param pCacheName
	 *            is the cache name(cache schema) which needs to be flushed.
	 */
	private void flush(String pCacheName) {
		if(!isUseCoherence()){
		   return;	
		}
		if(pCacheName == null){
			return;
		}
		NamedCache cache = getCache(pCacheName);
		if(cache != null){
			cache.clear();
			if(isLoggingDebug()){
				logDebug("Clearing the cache---flush() method is called");
			}
		}
	}
	
	/**
	 * This OOTB overrided method from service Cache is used to get the objects
	 * from coherence cache by using the keys and schema name.
	 * 
	 * @return map values as Iterator.
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public synchronized Iterator getAllElements() {
		if (isLoggingDebug()) {
			logDebug("Entering into PARALCoherenceCacheContainer.getAllElements method");
		}
		Map cachedObjects = null;
		NamedCache cache = null;
		String cacheName = getCacheSchema();
		Set keySet = null;
		if (!isUseCoherence() && cacheName == null) {
			return null;
		}
		cache = getCache(cacheName);
		keySet = cache.keySet();
		if (cache != null) {
			cachedObjects = cache.getAll(keySet);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from PARALCoherenceCacheContainer.getAllElements method");
		}
		return cachedObjects.values().iterator();
	}

	/**
	 * This OOTB method is used to get all the cached keys.
	 * 
	 * @return Set of keys as Iterator type.
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public synchronized Iterator getAllKeys() {
		if (isLoggingDebug()) {
			logDebug("Entering into PARALCoherenceCacheContainer.getAllKeys method");
		}
		String cacheName = getCacheSchema();
		if (!isUseCoherence()) {
			return null;
		}
		if (cacheName == null) {
			return null;
		}
		NamedCache cache =  getCache(cacheName);
		if(cache == null){
			return null;
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from PARALCoherenceCacheContainer.getAllKeys method");
		}
        return cache.keySet().iterator();
	}

	/**
	 * This OOTB override method of service cache is used to remove the cached
	 * keys.
	 * 
	 * @param pKey
	 *           - as Object type
	 * @return boolean -as true and false
	 */
	@Override
	public synchronized boolean remove(Object pKey) {
		if (isLoggingDebug()) {
			logDebug("Entering into PARALCoherenceCacheContainer.remove method");
		}
		String key = (String) pKey;
		boolean flag= false;
		String cacheName = getCacheSchema();
		if(!StringUtils.isBlank(key) && !StringUtils.isBlank(cacheName)){
		   removeCachedElement(key, cacheName);
		   flag = true;
		}
		return flag;
	}

	/**
	 * This Method is used to remove the cached element by using key and cache
	 * name.
	 * 
	 * @param pKey
	 *            -key as string type.
	 * @param pCacheName
	 *            -cache name as String type.
	 */
	private void removeCachedElement(String pKey, String pCacheName) {
		if (isLoggingDebug()) {
			logDebug("Entering into PARALCoherenceCacheContainer.removeCachedElement method");
		}
		if (pCacheName == null || pKey == null) {
			return;
		}
		if (!isUseCoherence()) {
			return;
		}
		NamedCache cache = getCache(pCacheName);
		if(cache != null){
		   cache.remove(pKey);
		   if(isLoggingDebug()){
			   logDebug("Removed key from cache is: " + pKey);
		   }
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from PARALCoherenceCacheContainer.removeCachedElement method");
		}
	}

	/**
	 * This OOTB method is used to put/add an object into coherence cache by
	 * using the key,value,schema name and TTL It will add the object(value) in
	 * the respective schema for a particular key.
	 * 
	 * @param pKey
	 *            -As Object type.
	 * @param pValue
	 *            -As Object type.
	 */
	@Override
	public synchronized void put(Object pKey, Object pValue) {
		if (isLoggingDebug()) {
			logDebug("Entering into PARALCoherenceCacheContainer.put method");
			logDebug("Key is : " + pKey);
			logDebug("Value is : " + pValue);
		}
		if(pKey == null && pValue == null){
			return;
		}
		if (!isUseCoherence()) {
			return;
		}
		NamedCache cache = null;
		String cacheName = getCacheSchema();
		Long timetolive = getDefaultTTL(); 
		if(cacheName != null){
			cache = getCache(cacheName);
			cache.put(pKey, pValue, timetolive);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from PARALCoherenceCacheContainer.put method");
		}
	}

	/**
	 * This OOTB overridded method is used to search the cache for the object
	 * with the specified key. If not found, then it uses the CacheAdapter to
	 * retrieve one. Returns null if unable to obtain the cached object.
	 * 
	 * @param pAStream
	 *            as PrintStream type.
	 */
	@Override
	public void dump(PrintStream pAStream) {
		if(pAStream == null){
			return;
		}else{
			String buffer = dump();
			pAStream.print(buffer);
			pAStream.flush();
		}
	}

	/**
	 * This OOTB overridden method is used to print current key/value pairs the
	 * NearCache to the supplied PrintStream.
	 * 
	 * @return key as String type.
	 */
	@Override
	public String dump() {
		if (isLoggingDebug()) {
			logDebug("Entering into PARALCoherenceCacheContainer.dump method");
		}
		if (!isUseCoherence()) {
			return null;
		}
		String cacheName = getCacheSchema();
		if (cacheName == null) {
			return null;
		}
		NamedCache cache =  getCache(cacheName);
		if(cache == null){
			return null;
		}
		String keyStr = null;
		Object value = null;
		Set keySet = cache.keySet();
		for (Object obj : keySet) {
			value = cache.get(obj);
			keyStr = (new StringBuilder()).append(keyStr).append(obj).append(PARALPageCacheConstants.NEW_LINE).append(value).append(PARALPageCacheConstants.NEW_LINE).toString();
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from PARALCoherenceCacheContainer.dump method");
		}
		return keyStr;
	}

	/**
	 * This method is OOTB method used to return all elements in this Cache
	 * ordered by entry creation time. But here is unsupported for this droplet.
	 * 
	 * @param pLatestFirst
	 *            -as boolean flag
	 * @return Iterator -as unsupported.
	 * 
	 */
	@Override
	public Iterator getAllKeysOrdered(boolean pLatestFirst) {
		throw new UnsupportedOperationException();
	}
	/**
	 * This method is OOTB method used to return the number of cache hits / total number of cache accesses
	 * But It is unsupported for this droplet.
	 * @return double -as unsupported.
	 */
	@Override
	public double getHitRatio() {
		throw new UnsupportedOperationException();
	}

	/**
	 * This method is OOTB method used to remove by partial key. But here is
	 * unsupported for this droplet.
	 * 
	 * @param pPartialKeyIndex
	 *            - key as index.
	 * @param pPartialKeyValue
	 *            - as key value.
	 * @return int -integer
	 */
	@Override
	public int removeByPartialKey(int pPartialKeyIndex,
			Object pPartialKeyValue) {
		throw new UnsupportedOperationException();
	}
	/**
	 * This method is OOTB method used to return the amount of memory taken up by the entries in the cache (includes keys).. But here is
	 * unsupported for this droplet.
	 * 
	 * @return int -integer
	 */
	@Override
	public int getUsedMemory() {
		throw new UnsupportedOperationException();
	}

	/**
	 * This method is OOTB method used to return return the ratio of memory size
	 * to maximum memory size. But it is unsupported for this droplet.
	 * 
	 * @return double -double value
	 */
	@Override
	public double getUsedMemoryCapacity() {
		throw new UnsupportedOperationException();
	}
	/**
	 * This method is OOTB method used to return the number of times entries were invalidated because they were out-of-date. But here it is
	 * unsupported method for this droplet.
	 * 
	 * @return int -as integer
	 */
	@Override
	public int getTimeoutCount() {
		throw new UnsupportedOperationException();
	}
}
