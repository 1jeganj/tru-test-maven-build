/*
 * Copyright (C) 2013, Professional Access Pvt Ltd.
 * All Rights Reserved. No use, copying or distribution
 * of this work may be made except in accordance with a
 * valid license agreement from Chain Reaction Cycles LTD. This notice
 * must be included on all copies, modifications and
 * derivatives of this work.*/

package com.tru.ral.performanceutil.tol.cache;

import java.io.PrintStream;
import java.util.Iterator;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageFormatException;
import javax.jms.ObjectMessage;

import atg.dms.patchbay.MessageSink;
import atg.service.cache.Cache;

import com.tru.ral.performanceutil.cml.cache.PARALPageCacheConstants;
import com.tru.ral.performanceutil.fhl.cache.PARALCacheInvalidationMessage;

/**
 * This class implements interface methods to provide wraper layer for
 * atg.service.Cache. The component made of PARALATGCacheContainer needs to
 * be injected in PARALCacheDroplet's cacheContainer property if ATG's out of box
 * Cache need to be used.
 * 
 * @author PA
 * @version 1.0
 */

public class PARALATGCacheContainer extends Cache implements  MessageSink {

	/** reference for serialVersionUID */
	private static final long serialVersionUID = 152070354201866123L;

	/**
	 * 'put' method overridden for key/value pair
	 * 
	 * @param pKey   - key
	 * @param pValue
	 * 			value
	 */
	@Override
	public  void put(Object pKey, Object pValue) {
		if (isLoggingDebug()) {
			logDebug("Inside  PARALATGCacheContainer.put......... method");
			logDebug("PARALATGCacheContainer key is : "+pKey);
			logDebug("PARALATGCacheContainer value : "+pValue);
		}
		super.put(pKey, pValue);
		if (isLoggingDebug()) {
			logDebug("Exiting from  PARALATGCacheContainer.put......... method");
		}
	}
	/**
	 * This is overridden put method of type void
	 * @param pKey
	 * 			key
	 * @param pValue
	 * 			value  
	 * @param pTimeout 
	 * 			timeout
	 */	
	public void put(Object pKey, Object pValue, long pTimeout) {
		if (isLoggingDebug()) {
			logDebug("Inside PARALATGCacheContainer.put......... method");
			logDebug("Object key is : "+pKey);
			logDebug("Object key value is : "+pValue);
		}
		if(pKey == null && pValue == null){
			return;
		}
		super.put(pKey, pValue);
		if(isLoggingDebug()){
			logDebug("Cache map is modified with new key and value pair");
		}
	}

	/**
	 * 'get' method overridden for key/value pair
	 * @param pKey
	 * 			key
	 * @return obj object value
	 */
	@Override
	public  Object get(Object pKey){
		Object obj = null;
		if (isLoggingDebug()) {
			logDebug("Inside PARALATGCacheContainer.get......... method");
			logDebug("Object key is : "+pKey);
		}
		try {
			obj = super.get(pKey);
		} catch (Exception e) {
			if(isLoggingError()){
				logError(e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from  PARALATGCacheContainer.get......... method");
		}		
		return obj;
	}

	/**
	 * This OOTB method is used to get all Keys from cache.
	 * 
	 * @return Keys as Iterator.
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Iterator getAllKeys() {
		if (isLoggingDebug()) {
			logDebug("Inside PARALATGCacheContainer.getAllKeys......... method");
		}
		Iterator allKeys = super.getAllKeys();
		if (isLoggingDebug()) {
			logDebug("Exiting from PARALATGCacheContainer.getAllKeys......... method");
		}
		return allKeys;
	}

	/**
	 * This OOTB overrided method from service Cache is used to get the objects
	 * from cache.
	 * 
	 * @return map values as Iterator.
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Iterator getAllElements() {
		if (isLoggingDebug()) {
			logDebug("Inside PARALATGCacheContainer.getAllElements......... method");
		}
		Iterator allElements = super.getAllElements();
		if (isLoggingDebug()) {
			logDebug("Exiting from PARALATGCacheContainer.getAllElements......... method");
		}
		return allElements;
	}
	/**
	 * 'receiveMessage' method for receiving message
	 * 
	 * @param pPortName - reference for portName
	 * @param pMessage	- reference for message
	 * @throws JMSException if any
	 */
	public void receiveMessage(String pPortName,  Message pMessage) 
			throws JMSException 
			{
		if (isLoggingDebug()) {
			logDebug("Inside PARALATGCacheContainer......receiveMessage method.....");
		}
		if(!(pMessage instanceof ObjectMessage)){
			throw new MessageFormatException(PARALPageCacheConstants.NOT_AN_OBJECT_MESSAGE);
		}

		ObjectMessage objMessage = (ObjectMessage)pMessage;
		Object obj = objMessage.getObject();

		if(isLoggingDebug()) {
			logDebug((new StringBuilder()).append("Received").append(obj.toString()).append(" on port ").append(pPortName).toString());
			logDebug("objMessage.....:"+objMessage);
			logDebug("obj.............: " +obj);
		}
		if(obj instanceof PARALCacheInvalidationMessage) {
			// call processCacheInvalidation
			processCacheInvalidation((PARALCacheInvalidationMessage)obj);
		} else {
			throw new MessageFormatException(PARALPageCacheConstants.NOT_A_CACHE_INVALIDATION_MESSAGE);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from PARALATGCacheContainer......receiveMessage method.....");
		}
			}


	/**
	 * 'processCacheInvalidation' method for process cache message
	 * 
	 * @param pCacheInvalidationMessage  - reference for PARALCacheInvalidationMessage
	 */
	@SuppressWarnings("unchecked")
	private void processCacheInvalidation(PARALCacheInvalidationMessage pCacheInvalidationMessage){
		if (isLoggingDebug()) {
			logDebug("Inside PARALATGCacheContainer.processCacheInvalidation method.....");
			logDebug("pCacheInvalidationMessage........: " +pCacheInvalidationMessage);
		}
		StringBuffer stbuff = new StringBuffer();
		if(pCacheInvalidationMessage.isPaternMatch()){
			for (Iterator iterator = getAllKeys(); iterator.hasNext();) {
				String key = (String) iterator.next();
				if (isLoggingDebug()) {
					logDebug("key.....: " + key);
				}
				if(key.toLowerCase().contains(pCacheInvalidationMessage.getEntryKey().toLowerCase()) && remove(key)){
					stbuff.append(key);
					stbuff.append(PARALPageCacheConstants.STRING_EMPTY);
				}
			}
		} else if (remove(pCacheInvalidationMessage.getEntryKey())){
			stbuff.append(pCacheInvalidationMessage.getEntryKey());
		}
		if(isLoggingDebug()) {
			logDebug("For " + pCacheInvalidationMessage.getEntryKey() +" following key removed : " + stbuff.toString());
			logDebug("Exiting from PARALATGCacheContainer.processCacheInvalidation method.....");
		}
	}

	/**
	 * overridden remove method of type boolean
	 * @param pKey
	 * 			key   
	 * @return boolean false
	 */
	public boolean remove(Object pKey) {
		if (isLoggingDebug()) {
			logDebug("Inside PARALATGCacheContainer.remove......... method");
			logDebug("Object key is : "+pKey);
		}
		boolean removeFlag = super.remove(pKey);
		if(isLoggingDebug()){
			logDebug("Cached key values are removed...");
		}
		return removeFlag;
	}

	/**
	 * This is OOTB overridden method used to clear the cache.
	 */
	@Override
	public void flush() {
		if (isLoggingDebug()) {
			logDebug("Inside PARALATGCacheContainer.flush......... method");
		}
		super.flush();
		if(isLoggingDebug()){
			logDebug("Cache is flushed...");
		}
	}

	/**
	 * This OOTB overridden method is used to search the cache for the object
	 * with the specified key.
	 * 
	 * @param pAStream
	 *            as PrintStream type.
	 */
	@Override
	public void dump(PrintStream pAStream) {
		if (isLoggingDebug()) {
			logDebug("Inside PARALATGCacheContainer.dump......... method");
		}
		if(pAStream == null){
			return;
		}else{
			String buffer = super.dump();
			pAStream.print(buffer);
			pAStream.flush();
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from PARALATGCacheContainer.dump......... method");
		}
	}
}
