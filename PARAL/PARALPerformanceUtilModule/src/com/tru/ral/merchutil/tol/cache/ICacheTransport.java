/*
* Copyright (C) 2015, Professional Access Pvt Ltd.
* All Rights Reserved. No use, copying or distribution
* of this work may be made except in accordance with a
* valid license agreement from PA, India. This notice
* must be included on all copies, modifications and 
* derivatives of this work.
* 
*/


package com.tru.ral.merchutil.tol.cache;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Map;
import java.util.Set;

/**
 * This interface ICacheTransport used for invalidating the repository and item level
 * cache.
 *
 * @author BPatel (PA)
 * @version 1.1
 * 
 */
public interface ICacheTransport extends Remote {

	/**
	 * This method is used for invalidating the cache for repositories.
	 * 
	 * @param pRepositoryPath
	 *            -- repository path like /atg/commerce/inventory/Inventory
	 * @param pMap
	 *            -- Map [key is item descriptor name and value is set of
	 *            repository ids]
	 * @throws RemoteException
	 *             -- remote exception
	 */
	public void invalidateCache(String pRepositoryPath, Map<String, Set<String>> pMap) throws RemoteException;

}
