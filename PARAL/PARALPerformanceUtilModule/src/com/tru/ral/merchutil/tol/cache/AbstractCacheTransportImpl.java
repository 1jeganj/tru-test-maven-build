/*
* Copyright (C) 2015, Professional Access Pvt Ltd.
* All Rights Reserved. No use, copying or distribution
* of this work may be made except in accordance with a
* valid license agreement from PA, India. This notice
* must be included on all copies, modifications and 
* derivatives of this work.
* 
*/


package com.tru.ral.merchutil.tol.cache;

import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.Set;

import atg.adapter.gsa.GSARepository;
import atg.adapter.gsa.invalidator.GSAInvalidationReceiver;
import atg.adapter.gsa.invalidator.GSAInvalidatorService;
import atg.core.util.StringUtils;
import atg.nucleus.GenericRMIService;
import atg.nucleus.GenericService;
import atg.nucleus.ServiceMap;
import atg.repository.ItemDescriptorImpl;
import atg.repository.MutableRepository;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;

import com.tru.ral.performanceutil.cml.cache.PARALPageCacheConstants;
/**
 * The class AbstractCacheTransportImpl.
 * 
 * The AbstractCacheTransportImpl used for invalidating the repository and item
 * level cache.
 * 
 * @author BPatel (PA)
 * @version 1.1
 * 
 */
public abstract class AbstractCacheTransportImpl extends GenericRMIService implements ICacheTransport {

	/**
	 * serial version id.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Variable to hold the repository map.
	 */
	private ServiceMap mRepositoryMap;

	/**
	 * Property to hold mRepository.
	 */
	private MutableRepository mRepository;

	/**
	 * Property to hold mInvalidatorService.
	 */
	private GSAInvalidatorService mInvalidatorService;
	
	
	/** The Invalidator reciver. */
	private GSAInvalidationReceiver mInvalidatorReciver;

	/**
	 * Gets the invalidator service.
	 * 
	 * @return the invalidatorService
	 */
	public GSAInvalidatorService getInvalidatorService() {
		return mInvalidatorService;
	}

	/**
	 * Sets the invalidator service.
	 * 
	 * @param pInvalidatorService
	 *            the invalidatorService to set
	 */
	public void setInvalidatorService(GSAInvalidatorService pInvalidatorService) {
		mInvalidatorService = pInvalidatorService;
	}

	/**
	 * Gets the repository.
	 * 
	 * @return the mRepository
	 */
	public MutableRepository getRepository() {
		return mRepository;
	}


	/**
	 * Sets the repository.
	 * 
	 * @param pRepository
	 *            the repository to set
	 */
	public void setRepository(MutableRepository pRepository) {
		mRepository = pRepository;
	}


	/**
	 * Gets the repository map.
	 * 
	 * @return the mRepositoryMap
	 */

	public ServiceMap getRepositoryMap() {
		return mRepositoryMap;
	}

	/**
	 * Sets the repository map.
	 * 
	 * @param pRepositoryMap
	 *            the repositoryMap to set
	 */

	public void setRepositoryMap(ServiceMap pRepositoryMap) {
		mRepositoryMap = pRepositoryMap;
	}

	/**
	 * This is default constructor.
	 * 
	 * @throws RemoteException
	 *             -- remote exception
	 */
	public AbstractCacheTransportImpl() throws RemoteException {
		super();
	}

	/**
	 * This method will invalidate full cache for entire Repository.
	 * 
	 * @param pRepository
	 *            - pRepository
	 * @param pItemDescriptor
	 *            - pItemDescriptor
	 * @param pRepositoryPath
	 *            - pRepositoryPath
	 */
	protected void invalidateFullRepository(MutableRepository pRepository, String pItemDescriptor, String pRepositoryPath) {
		if (isLoggingDebug()) {
			vlogDebug("Start of invalidateFullRepository method  of CacheTransportImpl");
		}
		RepositoryView repositoryView = null;
		QueryBuilder queryBuilder = null;
		Query query = null;
		final MutableRepository repository = (MutableRepository) getRepositoryMap().get(pRepositoryPath);
		// invalidate cache of entire repository
		if (pRepository != null) {
			if (isLoggingDebug()) {
				vlogDebug("Invoking gsaRepository.invalidateCaches() as the map size exceeded the specified limit");
			}


			// loading all the repository items of that item type in to that cache
			try {
				GSARepository gsaRepository = (GSARepository) pRepository;
				gsaRepository.invalidateCaches();
				repositoryView = repository.getView(PARALPageCacheConstants.PRICE);
				if (repositoryView != null) {
					queryBuilder = repositoryView.getQueryBuilder();
					query = queryBuilder.createUnconstrainedQuery();
					if (isLoggingDebug()) {
						vlogDebug("Executing the UnconstrainedQuery to load all the repository items of the item type in to that cache ");
					}
					RepositoryItem[] items = repositoryView.executeQuery(query);
					// to get the items from repository
					if (items != null) {
						for (RepositoryItem item : items) {
							item.getRepositoryId();
							if (isLoggingDebug()) {
								vlogDebug("item.getRepositoryId() after executing UnconstrainedQuery:::::{0}", item.getRepositoryId());
							}
						}
					}
				}
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					vlogError(repositoryException,"RepositoryException occured in CacheTransportImpl.invalidateFullRepository method ");
				}
			}

		}
		if (isLoggingDebug()) {
			vlogDebug("End of invalidateFullRepository method  of CacheTransportImpl");
		}
	}

	/**
	 * This method will invalidateRepositoryItem cache for the Repository.
	 * 
	 * @param pRepository
	 *            -- which repository
	 * @param pItemDescriptor
	 *            -- item descriptor name
	 * @param pRepositoryIds
	 *            -- set of repository id's
	 */
	protected void invalidateRepositoryItem(MutableRepository pRepository, String pItemDescriptor, Set<String> pRepositoryIds) {
		if (isLoggingDebug()) {
			vlogDebug("Start of invalidateRepositoryItem method  of CacheTransportImpl");
		}
		if (pRepository != null && !StringUtils.isBlank(pItemDescriptor)) {

			ItemDescriptorImpl itemDesc;
			try {
				itemDesc = (ItemDescriptorImpl) pRepository.getItemDescriptor(pItemDescriptor);
				if (itemDesc == null) {
					return;
				}
				if (pRepository != null && pRepositoryIds != null && !pRepositoryIds.isEmpty()) {
					Iterator<String> iterator = pRepositoryIds.iterator();
					String id = null;
					while (iterator.hasNext()) {
						id = iterator.next();

						if (getInvalidatorService() != null && id != null) {
							if (isLoggingDebug())
							{
								vlogDebug("Invoking the getInvalidatorService().invalidate");
							}
							
							getInvalidatorReciver().setEnabled(true);
							String repositoryPath = ((GenericService) pRepository).getAbsoluteName(); 	
							getInvalidatorService().invalidate(repositoryPath, pItemDescriptor, id);
						}

					}
				}

			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					vlogError(
							repositoryException,
							"RepositoryException occured in CacheTransportImpl.invalidateRepositoryItem method ");
				}

			} catch (RemoteException remoteException) {
				if (isLoggingError()) {
					vlogError(
							remoteException,
							"remoteException occured in CacheTransportImpl.invalidateRepositoryItem method ");
				}
			}

		}
		if (isLoggingDebug()) {
			vlogDebug("End of invalidateRepositoryItem method  of CacheTransportImpl");
		}
	}

	/**
	 * @return the invalidatorReciver
	 */
	public GSAInvalidationReceiver getInvalidatorReciver() {
		return mInvalidatorReciver;
	}

	/**
	 * @param pInvalidatorReciver the invalidatorReciver to set
	 */
	public void setInvalidatorReciver(GSAInvalidationReceiver pInvalidatorReciver) {
		this.mInvalidatorReciver = pInvalidatorReciver;
	}
}
