-- drop table ral_errorhandler_xlate;
-- drop table ral_errorhandler_error_xlate;
-- drop table ral_errorhandler;

CREATE TABLE ral_errorhandler ( 
	ASSET_VERSION    DECIMAL(19,0) NOT NULL,
    WORKSPACE_ID     VARCHAR(40) NOT NULL,
    BRANCH_ID        VARCHAR(40) NOT NULL,
    IS_HEAD          TINYINT NOT NULL,
    VERSION_DELETED  TINYINT NOT NULL,
    VERSION_EDITABLE TINYINT NOT NULL,
    PRED_VERSION     DECIMAL(19,0) NULL,
    CHECKIN_DATE DATETIME(6) NULL,
    error_id varchar(40) NOT NULL,
	error_key varchar(254)NOT NULL,
	error_message varchar(4000) NULL,
	constraint ral_errorhandler_p PRIMARY KEY(error_id,error_key,asset_version) ); 
	
CREATE TABLE ral_errorhandler_error_xlate ( 
	asset_version DECIMAL(38) NOT NULL,
	error_id varchar(40) NOT NULL, 
	locale varchar(254) NOT NULL, 
	translation_id varchar(254) NULL, 
	constraint ral_errorhandler_error_xlate_p PRIMARY KEY(error_id, locale, asset_version) ); 
	

CREATE INDEX ral_errorhandler_error_xlate_error_idx ON ral_errorhandler_error_xlate(error_id, asset_version, locale);

CREATE TABLE ral_errorhandler_xlate ( 
	ASSET_VERSION    DECIMAL(19,0) NOT NULL,
    WORKSPACE_ID     VARCHAR(40) NOT NULL,
    BRANCH_ID        VARCHAR(40) NOT NULL,
    IS_HEAD          TINYINT NOT NULL,
    VERSION_DELETED  TINYINT NOT NULL,
    VERSION_EDITABLE TINYINT NOT NULL,
    PRED_VERSION     DECIMAL(19,0) NULL,
    CHECKIN_DATE DATETIME(6) NULL,
	translation_id varchar(254) NOT NULL,
	error_message varchar(4000) NULL, 
	constraint ral_errorhandler_xlate_p PRIMARY KEY(translation_id, asset_version) );