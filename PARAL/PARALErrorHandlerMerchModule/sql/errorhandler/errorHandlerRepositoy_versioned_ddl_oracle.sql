-- drop table ral_errorhandler_xlate;
-- drop table ral_errorhandler_error_xlate;
-- drop table ral_errorhandler;

CREATE TABLE ral_errorhandler ( 
	ASSET_VERSION    NUMBER(19,0) NOT NULL,
    WORKSPACE_ID     VARCHAR2(40) NOT NULL,
    BRANCH_ID        VARCHAR2(40) NOT NULL,
    IS_HEAD          NUMBER(1,0) NOT NULL,
    VERSION_DELETED  NUMBER(1,0) NOT NULL,
    VERSION_EDITABLE NUMBER(1,0) NOT NULL,
    PRED_VERSION     NUMBER(19,0) NULL,
    CHECKIN_DATE TIMESTAMP(6) NULL,
    error_id varchar2(40) NOT NULL,
	error_key varchar2(254)NOT NULL,
	error_message varchar2(4000) NULL,
	constraint ral_errorhandler_p PRIMARY KEY(error_id,error_key,asset_version) ); 
	
CREATE TABLE ral_errorhandler_error_xlate ( 
	asset_version INTEGER NOT NULL,
	error_id varchar2(40) NOT NULL, 
	locale varchar2(254) NOT NULL, 
	translation_id varchar2(254) NULL, 
	constraint ral_errorhandler_error_xlate_p PRIMARY KEY(error_id, locale, asset_version) ); 
	

CREATE INDEX ral_errorhandler_error_xlate_error_idx ON ral_errorhandler_error_xlate(error_id, asset_version, locale);

CREATE TABLE ral_errorhandler_xlate ( 
	ASSET_VERSION    NUMBER(19,0) NOT NULL,
    WORKSPACE_ID     VARCHAR2(40) NOT NULL,
    BRANCH_ID        VARCHAR2(40) NOT NULL,
    IS_HEAD          NUMBER(1,0) NOT NULL,
    VERSION_DELETED  NUMBER(1,0) NOT NULL,
    VERSION_EDITABLE NUMBER(1,0) NOT NULL,
    PRED_VERSION     NUMBER(19,0) NULL,
    CHECKIN_DATE TIMESTAMP(6) NULL,
	translation_id varchar2(254) NOT NULL,
	error_message varchar2(4000) NULL, 
	constraint ral_errorhandler_xlate_p PRIMARY KEY(translation_id, asset_version) );