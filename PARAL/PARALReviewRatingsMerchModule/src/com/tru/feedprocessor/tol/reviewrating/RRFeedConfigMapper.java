package com.tru.feedprocessor.tol.reviewrating;

import java.io.File;
import java.util.Map;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.FeedHelper;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class PrFeedConfigMapper.
 * @author Professional Access
 * @version 1.0
 *  
 */
public class RRFeedConfigMapper {

	/** The m feed helper. */
	private FeedHelper mFeedHelper;

	/**
	 * Gets the feed helper.
	 * 
	 * @return the FeedHelper
	 */
	public FeedHelper getFeedHelper() {
		return mFeedHelper;
	}

	/**
	 * Sets the feed helper.
	 * 
	 * @param pFeedHelper
	 *            the feed helper to set
	 */
	public void setFeedHelper(FeedHelper pFeedHelper) {
		mFeedHelper = pFeedHelper;
	}
	/**
	 * Gets the resource name.
	 * 
	 * @return the resource name
	 */
	public String getResourceName() {
		StringBuffer lresourceName = new StringBuffer();
		FeedExecutionContextVO curExecCntx = getCurrentFeedExecutionContext();
		if(curExecCntx == null ){
			lresourceName.append(FeedConstants.FILE);
			lresourceName.append(getConfigValue(FeedConstants.FILEPATH));
			lresourceName.append(getConfigValue(FeedConstants.FILENAME)+FeedConstants.DOT+getConfigValue(FeedConstants.FILETYPE));
		}
		else{
			lresourceName.append(FeedConstants.FILE);
			lresourceName.append(getConfigValue(FeedConstants.FILEPATH));
			File lFile = new File(curExecCntx.getFilePath()+curExecCntx.getFileName());
			lresourceName.append(lFile.getName());
		}
 		return lresourceName.toString();
	}

	/**
	 * Gets the root element.
	 * 
	 * @return the root element
	 */
	public String getRootElement() {
		return (String) getConfigValue(FeedConstants.ROOT_ELEMENT);
	}

	/**
	 * Gets the root tag.
	 * 
	 * @return the root tag
	 */
	public String getXmlRootTag(){
		return (String) getConfigValue(FeedConstants.ROOT_TAG);
	}
	
	/**
	 * Gets the root element attributes.
	 * 
	 * @return the rootElementAttributes
	 */
	public Map<String,String> getRootElementAttributes(){
		return (Map<String, String>) getConfigValue(FeedConstants.ROOT_ELEMENT_ATTRIBUTES);
	}
	
	/**
	 * Gets the aliases.
	 * 
	 * @return the aliases
	 */
	public Map<String, String> getAliases(){
		return (Map<String, String>) getConfigValue(FeedConstants.ALIASES);
	}
	
	/**
	 * Gets the omitted fields.
	 * 
	 * @return the omittedFields
	 */
	public Map<String, String> getOmittedFields(){
		return (Map<String, String>) getConfigValue(FeedConstants.OMITTEDFIELDS);
	}
	
	/**
	 * Gets the annotated classes.
	 * 
	 * @return the annotatedClasses
	 */
	public Class<?>[] getAnnotatedClasses(){
		return  (Class<?>[]) getConfigValue(FeedConstants.ANNOTATEDCLASSES);
		
	}
	
	/**
	 * Gets the current feed execution context.
	 * 
	 * @return - CurrentFeedExecutionContext
	 */
	public FeedExecutionContextVO getCurrentFeedExecutionContext() {
		return (FeedExecutionContextVO) retriveData(FeedConstants.CURRENT_FEEDCONTEXT);
	}
	/**
	 * Retrive data.
	 * 
	 * @param pDataMapKey
	 *            the data map key
	 * @return the object
	 */
	public Object retriveData(String pDataMapKey) {
		return getFeedHelper().retriveData(pDataMapKey);
	}
	
	/**
	 * Gets the config value.
	 * 
	 * @param pConfigKey
	 *            the pConfigKey
	 * @return value
	 */
	public Object getConfigValue(String pConfigKey) {
		return getFeedHelper().getConfigValue(pConfigKey);
	}
	
	/**
	 * Gets the autodetectAnnotations flag.
	 * 
	 * @return the autodetectAnnotations flag
	 */
	public boolean getAutodetectAnnotations(){
		return Boolean.parseBoolean(getConfigValue(FeedConstants.AUTODETECTANNOTATIONS).toString());
	}
}
