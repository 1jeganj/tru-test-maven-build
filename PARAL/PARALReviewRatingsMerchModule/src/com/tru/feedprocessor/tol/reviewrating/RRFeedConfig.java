package com.tru.feedprocessor.tol.reviewrating;

import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.config.AbstractFeedConfig;

/**
 * The Class InvFeedConfiguration. This class is specific to powerreview feed,
 * which will hold the configuration details of powerreview.
 * @author Professional Access
 * @version 1.0
 * 
 */
public class RRFeedConfig extends AbstractFeedConfig { 
	
	/**
	 * The mAnnotatedClasses.
	 */
	private Class<?>[] mAnnotatedClasses;
	
	/**
	 * The mOmittedFields.
	 */
	private Map<Class,String> mOmittedFields;
	
	/**
	 * The mAliases.
	 */
	private Map<String,String> mAliases;
	
	/**
	 * The mEntityToRqlMap.
	 */
	private Map<String,String> mEntityToRqlMap;
	
	/**
	 * The mEntityVOMap.
	 */
	private Map<String,String> mEntityVOMap;
	
	/**
	 * The mEntityToGroupName.
	 */
	private Map<String,String> mEntityToGroupName;
	
	/**
	 * The mXmlRootTag.
	 */
	private String mXmlRootTag;
	
	/**
	 * The mRootElementAttributes.
	 */
	private Map<String, String> mRootElementAttributes;
	
	/**
	 * Property to hold mAutodetectAnnotations.
	 */
	private boolean mAutodetectAnnotations;
	
	/**
	 * Gets the omitted fields.
	 * 
	 * @return the omittedFields
	 */
	public Map<Class, String> getOmittedFields() {
		return mOmittedFields;
	}
	
	/**
	 * Sets the omitted fields.
	 * 
	 * @param pOmittedField
	 *            - OmittedField
	 */
	public void setOmittedFields(Map<Class,String>  pOmittedField) {
		mOmittedFields = pOmittedField;
		setConfigValue(FeedConstants.OMITTEDFIELDS, mOmittedFields);
	}
	
	/**
	 * Gets the annotated classes.
	 * 
	 * @return the annotatedClasses
	 */
	public Class<?>[] getAnnotatedClasses() {
		return mAnnotatedClasses;
	}
	
	/**
	 * Sets the annotated classes.
	 * 
	 * @param pAnnotatedClasses
	 *            - AnnotatedClasses
	 */
	public void setAnnotatedClasses(Class<?>[] pAnnotatedClasses){
		mAnnotatedClasses=pAnnotatedClasses;
		setConfigValue(FeedConstants.ANNOTATEDCLASSES, mAnnotatedClasses);
	}

	/**
	 * Gets the aliases.
	 * 
	 * @return the aliases
	 */
	public Map<String, String> getAliases() {
		return mAliases;
	}
	
	/**
	 * Sets the aliases.
	 * 
	 * @param pAliases
	 *            - Aliases
	 */
	public void setAliases(Map<String,String>  pAliases) {
		mAliases=pAliases;
		setConfigValue(FeedConstants.ALIASES, mAliases);
	}
	
	/**
	 * Gets the entity to rql map.
	 * 
	 * @return the entityToRqlMap
	 */
	public Map<String, String> getEntityToRqlMap() {
		return mEntityToRqlMap;
	}
	
	/**
	 * Sets the entity to rql map.
	 * 
	 * @param pEntityToRqlMap
	 *            - EntityToRqlMap
	 */
	public void setEntityToRqlMap(Map<String,String>  pEntityToRqlMap) {
		mEntityToRqlMap=pEntityToRqlMap;
		setConfigValue(FeedConstants.ENTITYTORQLMAP, mEntityToRqlMap);
	}
	
	/**
	 * Gets the entity vo map.
	 * 
	 * @return the entityVOMap
	 */
	public Map<String, String> getEntityVOMap() {
		return mEntityVOMap;
	}
	
	/**
	 * Sets the entity vo map.
	 * 
	 * @param pEntityVOMap
	 *            - EntityVOMap
	 */
	public void setEntityVOMap(Map<String,String> pEntityVOMap) {
		mEntityVOMap=pEntityVOMap;
		setConfigValue(FeedConstants.ENTITY_VO_MAP,
				mEntityVOMap);
	}
	
	/**
	 * Gets the entity to group name.
	 * 
	 * @return the entityToGroupName
	 */
	public Map<String, String> getEntityToGroupName() {
		return mEntityToGroupName;
	}
	
	/**
	 * Sets the entity to group name.
	 * 
	 * @param pEntityToGroupName
	 *            - EntityToGroupName
	 */
	public void setEntityToGroupName(Map<String,String> pEntityToGroupName){
		mEntityToGroupName=pEntityToGroupName;
		setConfigValue(FeedConstants.ENTITY_TO_GROUP_NAME, mEntityToGroupName);
	}
	
	/**
	 * Gets the xml root tag.
	 * 
	 * @return the xmlRootTag
	 */
	public String getXmlRootTag() {
		return mXmlRootTag;
	}
	
	/**
	 * Sets the xml root tag.
	 * 
	 * @param pRootTag
	 *            - RootTag
	 */
	public void setXmlRootTag(String pRootTag) {
		mXmlRootTag=pRootTag;
		setConfigValue(FeedConstants.ROOT_TAG, mXmlRootTag);
	}
	
	/**
	 * Gets the root element attributes.
	 * 
	 * @return the rootElementAttributes
	 */
	public Map<String, String> getRootElementAttributes() {
		return mRootElementAttributes;
	}
	
	/**
	 * Sets the root element attributes.
	 * 
	 * @param pRootElementAttributes
	 *            - RootElementAttributes
	 */
	public void setRootElementAttributes(Map<String, String> pRootElementAttributes){
		mRootElementAttributes=pRootElementAttributes;
		setConfigValue(FeedConstants.ROOT_ELEMENT_ATTRIBUTES, mRootElementAttributes);
	}
	
	/**
	 * Checks if is autodetect annotations.
	 * 
	 * @return the autodetectAnnotations
	 */
	public boolean isAutodetectAnnotations() {
		return mAutodetectAnnotations;
	}

	/**
	 * Sets the autodetect annotations.
	 * 
	 * @param pAutodetectAnnotations
	 *            the autodetectAnnotations to set
	 */
	public void setAutodetectAnnotations(boolean pAutodetectAnnotations) {
		mAutodetectAnnotations = pAutodetectAnnotations;
		setConfigValue(FeedConstants.AUTODETECTANNOTATIONS, mAutodetectAnnotations);
	}
	/**
	 * This method is used to to process the feed files.
	 * @throws Exception - This will throws exception if any error occurred while processing the feed file.
	 */
	@SuppressWarnings("unchecked")
	@Override
	 public void afterPropertiesSet() throws Exception {
		try{
			if(StringUtils.isBlank( getConfigValue(FeedConstants.FILENAME).toString())){
				getLogger().logError("File name can not be null in  feed configuration");
			} if(StringUtils.isBlank( getConfigValue(FeedConstants.FILEPATH).toString())){
				getLogger().logError("File path can not be null in  feed configuration");
			} if(StringUtils.isBlank((String) getConfigValue(FeedConstants.FILETYPE))){
				getLogger().logError("File Type can not be null in feed configuration");
			}
			String rootTag=(String) getConfigValue(FeedConstants.ROOT_TAG);
			if(rootTag == null){
				rootTag = getXmlRootElement();
				 if(StringUtils.isBlank(rootTag)){
					 	getLogger().logError("Xml root tag can not be null in feed configuration");
				 }
			}
			if(((List<String>) getConfigValue(FeedConstants.ENTITY_LIST)).isEmpty()){
				getLogger().logError("Entity list can not be null in feed configuration");
			}
			String downloadFlag = (String) getConfigValue(FeedConstants.FILE_DOWNLOADER_FLAG);
			String uploadFlag =(String)getConfigValue(FeedConstants.FILE_UPLOADER_FLAG);
			if( (downloadFlag == null || StringUtils.isBlank(downloadFlag)) && (uploadFlag == null || StringUtils.isBlank(uploadFlag))){
				getLogger().logError("No download Flag or upload flag is define or it is null");
			}
			else{		
				if((downloadFlag !=null && downloadFlag.equals(Boolean.toString(FeedConstants.TRUE))) 
						|| (uploadFlag !=null && uploadFlag.equals(Boolean.toString(FeedConstants.TRUE)))){

					if(StringUtils.isBlank(getConfigValue(FeedConstants.FTP_SFTP_HOST).toString())){
						getLogger().logError("Host Name for FTP and SFTP is mandatory for file downloading in feed configuration");
					} if(StringUtils.isBlank(getConfigValue(FeedConstants.FTP_SFTP_PORT).toString())){
						getLogger().logError("Port NO for FTP and SFTP is mandatory for file downloading in  feed configuration");
					} if(StringUtils.isBlank(getConfigValue(FeedConstants.FPT_SFTP_USER).toString())){
						getLogger().logError("Server IP for FTP and SFTP is mandatory for file downloading in  feed configuration");
					} if(StringUtils.isBlank(getConfigValue(FeedConstants.FPT_SFTP_PASSWORD).toString())){
						getLogger().logError("Server IP for FTP and SFTP is mandatory for file downloading in  feed configuration");
					} if(StringUtils.isBlank(getConfigValue(FeedConstants.FPT_SFTP_SOURCE_DIR).toString())){
						getLogger().logError("Server IP for FTP and SFTP is mandatory for file downloading in  feed configuration ");
					} if(StringUtils.isBlank( getConfigValue(FeedConstants.FILEPATH).toString())){
						getLogger().logError("File path can not be null in  feed configuration");
					} if(StringUtils.isBlank( getConfigValue(FeedConstants.PROTOCOL_TYPE).toString())){
						getLogger().logError("Protocol type for FTP or SFTP can not be null in  feed configuration");
					}
				}
				if( downloadFlag !=null && downloadFlag.equals(Boolean.toString(FeedConstants.TRUE)) && 
						StringUtils.isBlank( getConfigValue(FeedConstants.DOWNLOAD_FILETYPE).toString())){
					getLogger().logError("Download file type can not be null in  feed configuration");
				} if( uploadFlag !=null && uploadFlag.equals(Boolean.toString(FeedConstants.TRUE)) && 
						StringUtils.isBlank( getConfigValue(FeedConstants.UPLOAD_FILETYPE).toString())){
					getLogger().logError("Upload file type can not be null in  feed configuration");
				}
			}
		} catch (Exception e) {
			if (isLoggingError()) {
				logError("Please provide proper values in feed configuration", e);
			}
		}
		}
		

}
