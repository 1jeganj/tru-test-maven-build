package com.tru.storelocator.mgl;

import java.util.Collection;

import atg.json.JSONArray;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;

import com.tru.storelocator.tol.PARALStoreLocatorTools;

/**
 * The class holds the methods that interacts with PARALStoreLocatorTools to get the 
 * Stores from repository.
 * 
 * @author PA
 * 
 */
public class PARALStoreLocatorManager extends GenericService {

	/**
	 * Holds the mLocationTools
	 */
	PARALStoreLocatorTools mLocationTools;

	/**
	 * This method used to get the list of stores as JSONArray.
	 * @param pLocationResults Collection
	 * @param pDistance Double
	 * @param pMyStoreId MyStoreId
	 * @return JSONArray
	 */
	public JSONArray generateJSONResponse(Collection<RepositoryItem> pLocationResults, Double pDistance,String pMyStoreId){

		return getLocationTools().generateJSONResponse(pLocationResults,pDistance, pMyStoreId);

	}
	
	
	/**
	 * This method used to get the list of stores as JSONArray.
	 * @param pLocationResults Collection
	 * @param pDistance Double
	 * @param pSkuId SkuId
	 * @param pMyStoreId StoreId
	 * @return JSONArray
	 */
	public JSONArray generateJSONResponse(Collection<RepositoryItem> pLocationResults, Double pDistance,String pSkuId,String pMyStoreId){
		
		return getLocationTools().generateJSONResponse(pLocationResults, pDistance, pSkuId, pMyStoreId);
		
	}
	
	
	/**
	 * @return the locationTools
	 */
	public PARALStoreLocatorTools getLocationTools() {
		return mLocationTools;
	}
	/**
	 * @param pLocationTools
	 *            the locationTools to set
	 */
	public void setLocationTools(PARALStoreLocatorTools pLocationTools) {
		this.mLocationTools = pLocationTools;
	}	
}

