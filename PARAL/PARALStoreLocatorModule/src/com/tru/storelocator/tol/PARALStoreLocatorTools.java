package com.tru.storelocator.tol;



import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import atg.core.util.StringUtils;
import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.util.CurrentDate;

import com.tru.common.tol.IResourceBundleTools;
import com.tru.storelocator.cml.GeoLocatorConfigurations;
import com.tru.storelocator.cml.StoreLocatorConstants;

/**
 * The PARALStoreLocatorTools class is used to get interacts with location Repository.
 *
 * @author PA
 *
 */
public class PARALStoreLocatorTools extends GenericService {
	
	/** Property to hold mStoreLocatorSignatureGoogleMapURL. */
	private String mStoreLocatorSignatureGoogleMapURL;
	
	/** Property to hold mStoreLocatorSignatureKey. */
	private String mStoreLocatorSignatureKey;
	
	/** Property to hold mItemQuantity. */
	private String mItemQuantity;


	/** Holds the mStoreRepository. */
	Repository mStoreRepository;

	/** Holds the mLocationConfiguration. */
	GeoLocatorConfigurations mLocationConfiguration;

	/** Holds the mLocationPropertyManager. */
	StoreLocatorPropertyManager mLocationPropertyManager;

	/** Property to hold mWeekNameMap. */
	private Map<String,String> mWeekNameMap;

	/** Property to hold mClosingHoursMsg. */
	private String mClosingHoursMsg;

	/** Property to hold mCurrentDate. */
  	private CurrentDate mCurrentDate;

  	/** Property to hold mToysrusExpressServiceName. */
  	private String mToysrusExpressServiceName;

  	/** Property to hold mBabysrusExpressServiceName. */
  	private String mBabysrusExpressServiceName;

  	/** Property to hold mToysMap. */
  	private Map<String,String> mToysMap;

  	/** Property to hold mToysMap. */
  	private boolean mEnableOnHandQtyEligibility;

  	/** Holds the constant of Error Handler Tools. */
	private IResourceBundleTools mErrorHandlerTools;

	/**  Holds the  mStorePickupServiceName. */
	private String mStorePickupServiceName;

	/**  Holds the  mShipToStoreServiceName. */
	private String mShipToStoreServiceName;

	/**  Holds the  storeHoursMsgList. */
	private List<String> mStoreHoursMsgList;


	/** Holds the mEnableTimeout. */
	private boolean mEnableTimeout;

	/** Holds the mEnableProxy. */
	private boolean mEnableProxy;

	/** Holds the mTimeoutInSeconds. */
	private int mTimeoutInSeconds;

	/** Holds the mProxyIp. */
	private String mProxyIp;

	/** Holds the mProxyPort. */
	private int mProxyPort;

	/** Holds the mSiteIdsList. */
	private List<String> mSiteIdsList;

	/** Holds the mStoreLocatoreGeoCodeExceErrorMsgKey. */
	private String mStoreLocatoreGeoCodeExceErrorMsgKey;

	/** Holds the mStoreLocatoreGeoCodeEmptyResultsErrorMsgKey. */
	private String mStoreLocatoreGeoCodeEmptyResultsErrorMsgKey;

	/**  Holds the mDistanceInMiles. */
	private double mDistanceInMiles;

	/**  Holds the mSuperStoreServiceName. */
	private String mSuperStoreServiceName;

	/**  Holds the mStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForBabiesrus. */
	private String mStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForBabiesrus;

	/**  Holds the mStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForToysrus. */
	private String mStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForToysrus;

	/**  Holds the mStoreLocatoreGeoCodeEmptyRepoResults.*/
	private String mStoreLocatoreGeoCodeEmptyRepoResults;


	/**  Holds the mServiceNameList. */
	private List<String> mServiceNameList;

	/**
	 * @return the mServiceNameList
	 */
	public List<String> getServiceNameList() {
		return mServiceNameList;
	}

	/**
	 * @param pServiceNameList the mServiceNameList to set
	 */
	public void setServiceNameList(List<String> pServiceNameList) {
		this.mServiceNameList = pServiceNameList;
	}

	/**
	 * Gets the super store service name.
	 *
	 * @return the mSuperStoreServiceName
	 */
	public String getSuperStoreServiceName() {
		return mSuperStoreServiceName;
	}

	/**
	 * Sets the super store service name.
	 *
	 * @param pSuperStoreServiceName the mSuperStoreServiceName to set
	 */
	public void setSuperStoreServiceName(String pSuperStoreServiceName) {
		mSuperStoreServiceName = pSuperStoreServiceName;
	}

	/**
	 * Gets the distance in miles.
	 *
	 * @return the distanceInMiles
	 */
	public double getDistanceInMiles() {
		return mDistanceInMiles;
	}

	/**
	 * Sets the distance in miles.
	 *
	 * @param pDistanceInMiles the distanceInMiles to set
	 */
	public void setDistanceInMiles(double pDistanceInMiles) {
		mDistanceInMiles = pDistanceInMiles;
	}

	/**
	 * Gets the store locatore geo code exce error msg key.
	 *
	 * @return the storeLocatoreGeoCodeExceErrorMsgKey
	 */
	public String getStoreLocatoreGeoCodeExceErrorMsgKey() {
		return mStoreLocatoreGeoCodeExceErrorMsgKey;
	}

	/**
	 * Sets the store locatore geo code exce error msg key.
	 *
	 * @param pStoreLocatoreGeoCodeExceErrorMsgKey the storeLocatoreGeoCodeExceErrorMsgKey to set
	 */
	public void setStoreLocatoreGeoCodeExceErrorMsgKey(
			String pStoreLocatoreGeoCodeExceErrorMsgKey) {
		mStoreLocatoreGeoCodeExceErrorMsgKey = pStoreLocatoreGeoCodeExceErrorMsgKey;
	}

	/**
	 * Gets the store locatore geo code empty results error msg key.
	 *
	 * @return the storeLocatoreGeoCodeEmptyResultsErrorMsgKey
	 */
	public String getStoreLocatoreGeoCodeEmptyResultsErrorMsgKey() {
		return mStoreLocatoreGeoCodeEmptyResultsErrorMsgKey;
	}

	/**
	 * Sets the store locatore geo code empty results error msg key.
	 *
	 * @param pStoreLocatoreGeoCodeEmptyResultsErrorMsgKey the storeLocatoreGeoCodeEmptyResultsErrorMsgKey to set
	 */
	public void setStoreLocatoreGeoCodeEmptyResultsErrorMsgKey(
			String pStoreLocatoreGeoCodeEmptyResultsErrorMsgKey) {
		mStoreLocatoreGeoCodeEmptyResultsErrorMsgKey = pStoreLocatoreGeoCodeEmptyResultsErrorMsgKey;
	}

	/**
	 * Gets the site ids list.
	 *
	 * @return the siteIdsList
	 */
	public List<String> getSiteIdsList() {
		return mSiteIdsList;
	}

	/**
	 * Sets the site ids list.
	 *
	 * @param pSiteIdsList the siteIdsList to set
	 */
	public void setSiteIdsList(List<String> pSiteIdsList) {
		mSiteIdsList = pSiteIdsList;
	}

	/**
	 * Checks if is enable timeout.
	 *
	 * @return the enableTimeout
	 */
	public boolean isEnableTimeout() {
		return mEnableTimeout;
	}

	/**
	 * Sets the enable timeout.
	 *
	 * @param pEnableTimeout the enableTimeout to set
	 */
	public void setEnableTimeout(boolean pEnableTimeout) {
		mEnableTimeout = pEnableTimeout;
	}

	/**
	 * Checks if is enable proxy.
	 *
	 * @return the enableProxy
	 */
	public boolean isEnableProxy() {
		return mEnableProxy;
	}

	/**
	 * Sets the enable proxy.
	 *
	 * @param pEnableProxy the enableProxy to set
	 */
	public void setEnableProxy(boolean pEnableProxy) {
		mEnableProxy = pEnableProxy;
	}

	/**
	 * Gets the timeout in seconds.
	 *
	 * @return the timeoutInSeconds
	 */
	public int getTimeoutInSeconds() {
		return mTimeoutInSeconds;
	}

	/**
	 * Sets the timeout in seconds.
	 *
	 * @param pTimeoutInSeconds the timeoutInSeconds to set
	 */
	public void setTimeoutInSeconds(int pTimeoutInSeconds) {
		mTimeoutInSeconds = pTimeoutInSeconds;
	}

	/**
	 * Gets the proxy ip.
	 *
	 * @return the proxyIp
	 */
	public String getProxyIp() {
		return mProxyIp;
	}

	/**
	 * Sets the proxy ip.
	 *
	 * @param pProxyIp the proxyIp to set
	 */
	public void setProxyIp(String pProxyIp) {
		mProxyIp = pProxyIp;
	}

	/**
	 * Gets the proxy port.
	 *
	 * @return the proxyPort
	 */
	public int getProxyPort() {
		return mProxyPort;
	}

	/**
	 * Sets the proxy port.
	 *
	 * @param pProxyPort the proxyPort to set
	 */
	public void setProxyPort(int pProxyPort) {
		mProxyPort = pProxyPort;
	}


	/**
	 * Gets the store hours msg list.
	 *
	 * @return the storeHoursMsgList
	 */
	public List<String> getStoreHoursMsgList() {
		return mStoreHoursMsgList;
	}

	/**
	 * Sets the store hours msg list.
	 *
	 * @param pStoreHoursMsgList the storeHoursMsgList to set
	 */
	public void setStoreHoursMsgList(List<String> pStoreHoursMsgList) {
		mStoreHoursMsgList = pStoreHoursMsgList;
	}

	/**
	 * Gets the store pickup service name.
	 *
	 * @return the storePickupServiceName
	 */
	public String getStorePickupServiceName() {
		return mStorePickupServiceName;
	}

	/**
	 * Sets the store pickup service name.
	 *
	 * @param pStorePickupServiceName the storePickupServiceName to set
	 */
	public void setStorePickupServiceName(String pStorePickupServiceName) {
		mStorePickupServiceName = pStorePickupServiceName;
	}

	/**
	 * Gets the ship to store service name.
	 *
	 * @return the shipToStoreServiceName
	 */
	public String getShipToStoreServiceName() {
		return mShipToStoreServiceName;
	}

	/**
	 * Sets the ship to store service name.
	 *
	 * @param pShipToStoreServiceName the shipToStoreServiceName to set
	 */
	public void setShipToStoreServiceName(String pShipToStoreServiceName) {
		mShipToStoreServiceName = pShipToStoreServiceName;
	}

	/**
	 * Gets the error handler tools.
	 *
	 * @return mErrorHandlerTools
	 */
	public IResourceBundleTools getErrorHandlerTools() {
		return mErrorHandlerTools;
	}

	/**
	 * Sets the error handler tools.
	 *
	 * @param pErrorHandlerTools ErrorHandlerTools
	 */
	public void setErrorHandlerTools(IResourceBundleTools pErrorHandlerTools) {
		this.mErrorHandlerTools = pErrorHandlerTools;
	}

	/**
	 * Checks if is enable on hand qty eligibility.
	 *
	 * @return the enableOnHandQtyEligibility
	 */
	public boolean isEnableOnHandQtyEligibility() {
		return mEnableOnHandQtyEligibility;
	}

	/**
	 * Sets the enable on hand qty eligibility.
	 *
	 * @param pEnableOnHandQtyEligibility the enableOnHandQtyEligibility to set
	 */
	public void setEnableOnHandQtyEligibility(boolean pEnableOnHandQtyEligibility) {
		mEnableOnHandQtyEligibility = pEnableOnHandQtyEligibility;
	}



	/**
	 * Gets the toys map.
	 *
	 * @return the toysMap
	 */
	public Map<String,String> getToysMap() {
		return mToysMap;
	}

	/**
	 * Sets the toys map.
	 *
	 * @param pToysMap the toysMap to set
	 */
	public void setToysMap(Map<String,String> pToysMap) {
		mToysMap = pToysMap;
	}

	/**
	 * Gets the toysrus express service name.
	 *
	 * @return the toysrusExpressServiceName
	 */
	public String getToysrusExpressServiceName() {
		return mToysrusExpressServiceName;
	}

	/**
	 * Sets the toysrus express service name.
	 *
	 * @param pToysrusExpressServiceName the toysrusExpressServiceName to set
	 */
	public void setToysrusExpressServiceName(String pToysrusExpressServiceName) {
		mToysrusExpressServiceName = pToysrusExpressServiceName;
	}

	/**
	 * Gets the babysrus express service name.
	 *
	 * @return the babysrusExpressServiceName
	 */
	public String getBabysrusExpressServiceName() {
		return mBabysrusExpressServiceName;
	}

	/**
	 * Sets the babysrus express service name.
	 *
	 * @param pBabysrusExpressServiceName the babysrusExpressServiceName to set
	 */
	public void setBabysrusExpressServiceName(String pBabysrusExpressServiceName) {
		mBabysrusExpressServiceName = pBabysrusExpressServiceName;
	}

	/**
	  * Sets the CurrentDate component.
	  * @param pCurrentDate CurrentDate
	 */
	 public void setCurrentDate(CurrentDate pCurrentDate) {
	   mCurrentDate = pCurrentDate;
	 }
	 /**
	  * Gets the CurrentDate component.
	  * @return the CurrentDate
	 */
	 public CurrentDate getCurrentDate() {
	   return mCurrentDate;
	 }

	/**
	 * Gets the closing hours msg.
	 *
	 * @return the mClosingHoursMsg
	 */
	public String getClosingHoursMsg() {
		return mClosingHoursMsg;
	}

	/**
	 * Sets the closing hours msg.
	 *
	 * @param pClosingHoursMsg the mClosingHoursMsg to set
	 */
	public void setClosingHoursMsg(String pClosingHoursMsg) {
		mClosingHoursMsg = pClosingHoursMsg;
	}

	/**
	 * Gets the week name map.
	 *
	 * @return the mWeekNameMap
	 */
	public Map<String, String> getWeekNameMap() {
		return mWeekNameMap;
	}

	/**
	 * Sets the week name map.
	 *
	 * @param pWeekNameMap the mWeekNameMap to set
	 */
	public void setWeekNameMap(Map<String, String> pWeekNameMap) {
		mWeekNameMap = pWeekNameMap;
	}

	/**
	 * This method used to get interacts with repository to fetch the stores in JSON response.
	 *
	 * @param pLocationItems the location items
	 * @param pDistance the distance
	 * @param pMyStoreId the my store id
	 * @return the JSON array
	 */
	public JSONArray generateJSONResponse(Collection<RepositoryItem> pLocationItems,Double pDistance, String pMyStoreId){
		//Filter the items based on the Store End Date
		Collection<RepositoryItem> locationItem = null;
		locationItem = getEligibleStoreItems(pLocationItems);
		JSONObject storeArray[] = null;
		if(locationItem != null & !locationItem.isEmpty()){
			 storeArray = new JSONObject[pLocationItems.size()];
			 storeArray(locationItem, pMyStoreId, storeArray, pDistance);
		}
		return new JSONArray(storeArray);

	}

	/**
	 * This method used to get interacts with repository to fetch the stores in JSON response.
	 *
	 * @param pLocationItems the location items
	 * @param pMyStoreId the my store id
	 * @param pStoreArray the store array
	 * @param pDistance the distance
	 */
	private void storeArray(Collection<RepositoryItem> pLocationItems,
			String pMyStoreId, JSONObject[] pStoreArray,Double pDistance) {
		RepositoryItem storeItem = null;
		 int index=0;

		 double dist = convertMetersToMiles(pDistance);
		 Long distance = Math.round(dist);
		Iterator<RepositoryItem> locateList = null;
			if(pLocationItems != null){
				if(!StringUtils.isBlank(pMyStoreId)){
					for (RepositoryItem item : pLocationItems) {
						if(item != null && !StringUtils.isBlank(pMyStoreId) && pMyStoreId.equalsIgnoreCase(((String)item.getPropertyValue(getLocationPropertyManager().getLocationIdPropertyName())))){
							pStoreArray[index] = new JSONObject(populateStoreItemToMap(item, getLocationConfiguration().getZoomLevelMap().get(distance.toString())));
							index++;
							break;
						}
					}
				}

				locateList = pLocationItems.iterator();
				while(locateList.hasNext()){
					storeItem = locateList.next();
					if(storeItem != null){
						if( !StringUtils.isBlank(pMyStoreId) && pMyStoreId.equalsIgnoreCase(((String)storeItem.getPropertyValue(getLocationPropertyManager().getLocationIdPropertyName()))) ){
							continue;
						}
						pStoreArray[index] = new JSONObject(populateStoreItemToMap(storeItem, getLocationConfiguration().getZoomLevelMap().get(distance.toString())));
						index++;
					}
				}

			}
	}

	/**
	 * This method is used to convert distance from Miles to Meters.
	 * @param pDistance Double
	 * @return double - distance in miles
	 *
	 */
	public double convertMetersToMiles(Double pDistance) {
		if (isLoggingDebug()) {
			logDebug("PARALGeoLocatorFormHandler :convertMilesToMeters : Starts");
		}
		if (isLoggingDebug()) {
			logDebug("Distance in Miles: "+pDistance);
		}
		double kms = pDistance / StoreLocatorConstants.KMS_TO_METER_CONVERTER;
		double miles = kms * StoreLocatorConstants.KMS_TO_MILES_CONVERTER;
		if (isLoggingDebug()) {
			logDebug("Distance in Meters: "+miles);
		}
		if (isLoggingDebug()) {
			logDebug("PARALGeoLocatorFormHandler :convertMilesToMeters : Ends");
		}
		return miles;
	}

	/**
	 * This method used to stored the list of stores in Map.
	 * @param pStoreItem RepositoryItem
	 * @param pZoomLevel String
	 * @return storeItemMap
	 */
	public Map<String, Object> populateStoreItemToMap(
			RepositoryItem pStoreItem, String pZoomLevel) {
		if (isLoggingDebug()) {
			logDebug("Start : populateStoreItemToMap(RepositoryItem pStoreItem, String pZoomLevel) ");
		}
		Map<String, Object> storeItemMap = new HashMap<String, Object>();

		if (null != getLocationPropertyManager()) {
			storeItemMap.put(getLocationPropertyManager().getStoreNamePropertyName(), pStoreItem.getPropertyValue(getLocationPropertyManager().getStoreNamePropertyName()));
			storeItemMap.put(getLocationPropertyManager().getLatitudePropertyName(), pStoreItem.getPropertyValue(getLocationPropertyManager().getLatitudePropertyName()));
			storeItemMap.put(getLocationPropertyManager().getLongitudePropertyName(), pStoreItem.getPropertyValue(getLocationPropertyManager().getLongitudePropertyName()));
			storeItemMap.put(getLocationPropertyManager().getPhoneNumberPropertyName(),  pStoreItem.getPropertyValue(getLocationPropertyManager().getPhoneNumberPropertyName()));
			storeItemMap.put(getLocationPropertyManager().getCityPropertyName(), pStoreItem.getPropertyValue(getLocationPropertyManager().getCityPropertyName()));
			storeItemMap.put(getLocationPropertyManager().getStateAddressPropertyName(), pStoreItem.getPropertyValue(getLocationPropertyManager().getStateAddressPropertyName()));
			storeItemMap.put(getLocationPropertyManager().getPostalCodePropertyName(), pStoreItem.getPropertyValue(getLocationPropertyManager().getPostalCodePropertyName()));
			storeItemMap.put(getLocationPropertyManager().getTypePropertyName(), pStoreItem.getPropertyValue(getLocationPropertyManager().getTypePropertyName()));
			storeItemMap.put(getLocationPropertyManager().getChainCodePropertyName(), pStoreItem.getPropertyValue(getLocationPropertyManager().getChainCodePropertyName()));
			storeItemMap.put(getLocationPropertyManager().getStoreDistancePropertyName(), pStoreItem.getPropertyValue(getLocationPropertyManager().getStoreDistancePropertyName()));

			storeItemMap.put(getLocationPropertyManager().getWarehouseLocationCodePropertyName(), pStoreItem.getPropertyValue(getLocationPropertyManager().getWarehouseLocationCodePropertyName()));

			//Add distance value in miles to to the storeItem
			//convert the distance from the meters to miles and then set into the storeItemMap
			/*double distanceInMetres = StoreLocatorConstants.ZERO_IN_DOUBLE;
			if(pStoreItem.getPropertyValue(getLocationPropertyManager().getDistancePropertyName()) == null){
				distanceInMetres = StoreLocatorConstants.ZERO_IN_DOUBLE;
			}else {
				distanceInMetres = (Double) (pStoreItem.getPropertyValue(getLocationPropertyManager().getDistancePropertyName()));
			}
			double distInMiles = convertMetersToMiles(distanceInMetres);
			DecimalFormat df = new DecimalFormat();
			df.setMaximumFractionDigits(StoreLocatorConstants.INTEGER_TWO);
			String distanceInString = df.format(distInMiles);*/
			Double doubleObj = (Double)pStoreItem.getPropertyValue(getLocationPropertyManager().getStoreDistancePropertyName());
			if(doubleObj == null){
				doubleObj = StoreLocatorConstants.DOUBLE_ZERO;
			}
			storeItemMap.put(StoreLocatorConstants.DISTANCE_IN_MILES, String.valueOf(doubleObj));

			if (!StringUtils.isBlank(pZoomLevel)) {
				storeItemMap.put(StoreLocatorConstants.ZOOM_LEVEL, pZoomLevel);
			}
			String address1 = (String) pStoreItem.getPropertyValue(getLocationPropertyManager().getAddress1PropertyName());
			String center=address1.concat(StoreLocatorConstants.COMMA).concat((String) pStoreItem.getPropertyValue(getLocationPropertyManager().getCityPropertyName())).concat(StoreLocatorConstants.COMMA).concat((String) pStoreItem.getPropertyValue(getLocationPropertyManager().getStateAddressPropertyName()));
			center =center.replace(StoreLocatorConstants.SPACE,StoreLocatorConstants.SPLUS);
			String signature=generateSignature(center);
			String address2 = (String) pStoreItem.getPropertyValue(getLocationPropertyManager().getAddress2PropertyName());
			String storeAddress = null;
			if (!StringUtils.isBlank(address2)) {
				storeAddress = new StringBuilder(address1).append(StoreLocatorConstants.SPACE).append(address2).toString();
			} else {
				storeAddress = address1;
			}
			storeItemMap.put(StoreLocatorConstants.SIGNATURE,signature);
			storeItemMap.put(StoreLocatorConstants.STORE_ADDRESS1, address1);
			storeItemMap.put(StoreLocatorConstants.STORE_ADDRESS2, address2);
			storeItemMap.put(StoreLocatorConstants.STREET_ADDRESS, storeAddress);
			String storeId = (String) pStoreItem.getPropertyValue(getLocationPropertyManager().getLocationIdPropertyName());
			storeItemMap.put(StoreLocatorConstants.STORE_ID, storeId);

			//populate the store working hours
			Map<String,String> storeHoursMap = getStoreWorkingHours(pStoreItem);

			if(storeHoursMap == null || storeHoursMap.isEmpty()){
				storeItemMap.put(StoreLocatorConstants.STORE_HOURS, new JSONObject());
			}else{
				storeItemMap.put(StoreLocatorConstants.STORE_HOURS, new JSONObject(storeHoursMap));
			}

			//storeItemMap.put(StoreLocatorConstants.STORE_HOURS, getStoreWorkingHoursInJsonFormat(pStoreItem));

			String closingTimeMsg = getStoreClosingTime(storeHoursMap);
			storeItemMap.put(StoreLocatorConstants.CLOSING_TIME_MSG, closingTimeMsg);

			storeItemMap.put(StoreLocatorConstants.STORE_SERVICES, getStoreServicesInJsonFormat(pStoreItem));

			storeItemMap.put(StoreLocatorConstants.IS_ELIGIBLE_FOR_FAV_STORE, getFavStoreEligibility(pStoreItem));

			storeItemMap.put(StoreLocatorConstants.SHOW_TIMINGS_FROM_MON_TO_FRI_IN_ONE_LINE, showTingsFromMonToFriInOneLine(pStoreItem));

			storeItemMap.put(StoreLocatorConstants.IS_SUPER_STORE_SERVICE_AVAIL, isTheGivenServiceConfiguredForGivenStore(pStoreItem,getSuperStoreServiceName()));

			Date stoteItemEndDate = (Date)pStoreItem.getPropertyValue(getLocationPropertyManager().getEndDatePropertyName());
			storeItemMap.put(StoreLocatorConstants.STORE_END_DATE, stoteItemEndDate);



			if (isLoggingDebug()) {
				logDebug("End : populateStoreItemToMap(RepositoryItem pStoreItem, String pZoomLevel) ");
			}
		}
		return storeItemMap;
	}

	/**
	 * @param pAddress1 address1
	 * @return String Signature
	 */
	private String generateSignature(String pAddress1) {
		String signature = null;
		try {
			URL url = new URL(getStoreLocatorSignatureGoogleMapURL().concat(pAddress1));
			byte[] key;
			key= urlSigner(getStoreLocatorSignatureKey());
			 signature = signRequest(url.getPath(),url.getQuery(),key);
		} catch (MalformedURLException e) {
			if(isLoggingError()){
				logError("MalformedURLException in generateSignature method ",e);
			}
		} catch (IOException e) {
			if(isLoggingError()){
				logError("IOException in generateSignature method ",e);
			}
		} catch (InvalidKeyException e) {
			if(isLoggingError()){
				logError("InvalidKeyException in generateSignature method ",e);
			}
		} catch (NoSuchAlgorithmException e) {
			if(isLoggingError()){
				logError("NoSuchAlgorithmException in generateSignature method ",e);
			}
		} catch (URISyntaxException e) {
			if(isLoggingError()){
				logError("URISyntaxException in generateSignature method ",e);
			}
		}
		return signature;
	}
	
	
	 /**
	 * @param pKeyString pKeyString
	 * @return byte
	 * @throws IOException IOException
	 */
	private byte[] urlSigner(String pKeyString) throws IOException {
		    // Convert the key from 'web safe' base 64 to binary
			String lKeyString = pKeyString;
			lKeyString = lKeyString.replace(StoreLocatorConstants.MINUS, StoreLocatorConstants.PLUS);
			lKeyString = lKeyString.replace(StoreLocatorConstants.UNDERSCORE, StoreLocatorConstants.SLASH);
		   // Base64 is JDK 1.8 only - older versions may need to use Apache Commons or similar.
		    return com.sun.org.apache.xerces.internal.impl.dv.util.Base64.decode(lKeyString);
		    		
		  }

		  /**
		 * @param pPath path
		 * @param pQuery query
		 * @param pKey key
		 * @return String
		 * @throws NoSuchAlgorithmException NoSuchAlgorithmException
		 * @throws InvalidKeyException InvalidKeyException
		 * @throws UnsupportedEncodingException UnsupportedEncodingException
		 * @throws URISyntaxException URISyntaxException
		 */
		private String signRequest(String pPath, String pQuery, byte[] pKey) throws NoSuchAlgorithmException,
		    InvalidKeyException, UnsupportedEncodingException, URISyntaxException {
		    
		    // Retrieve the proper URL components to sign
		    String resource = pPath + StoreLocatorConstants.QUESTION_MARK + pQuery;
		    
		    // Get an HMAC-SHA1 signing key from the raw key bytes
		    SecretKeySpec sha1Key = new SecretKeySpec(pKey, StoreLocatorConstants.SECRET_KEY_SPEC);

		    // Get an HMAC-SHA1 Mac instance and initialize it with the HMAC-SHA1 key
		    Mac mac = Mac.getInstance(StoreLocatorConstants.SECRET_KEY_SPEC);
		    mac.init(sha1Key);

		    // compute the binary signature for the request
		    byte[] sigBytes = mac.doFinal(resource.getBytes());

		    // base 64 encode the binary signature
		    // Base64 is JDK 1.8 only - older versions may need to use Apache Commons or similar.
		    String signature = org.apache.axis.encoding.Base64.encode(sigBytes);
		    
		    // convert the signature to 'web safe' base 64
		    signature = signature.replace(StoreLocatorConstants.PLUS, StoreLocatorConstants.MINUS);
		    signature = signature.replace(StoreLocatorConstants.SLASH, StoreLocatorConstants.UNDERSCORE);
		    
		    return signature;
		  }
	
	

	/**
	 * Gets the store repository.
	 *
	 * @return the storeRepository
	 */
	public Repository getStoreRepository() {
		return mStoreRepository;
	}

	/**
	 * Sets the store repository.
	 *
	 * @param pStoreRepository            the storeRepository to set
	 */
	public void setStoreRepository(Repository pStoreRepository) {
		this.mStoreRepository = pStoreRepository;
	}

	/**
	 * Gets the location configuration.
	 *
	 * @return the locationConfiguration
	 */
	public GeoLocatorConfigurations getLocationConfiguration() {
		return mLocationConfiguration;
	}

	/**
	 * Sets the location configuration.
	 *
	 * @param pLocationConfiguration            the locationConfiguration to set
	 */
	public void setLocationConfiguration(
			GeoLocatorConfigurations pLocationConfiguration) {
		this.mLocationConfiguration = pLocationConfiguration;
	}

	/**
	 * Gets the location property manager.
	 *
	 * @return the locationPropertyManager
	 */
	public StoreLocatorPropertyManager getLocationPropertyManager() {
		return mLocationPropertyManager;
	}

	/**
	 * Sets the location property manager.
	 *
	 * @param pLocationPropertyManager            the locationPropertyManager to set
	 */
	public void setLocationPropertyManager(
			StoreLocatorPropertyManager pLocationPropertyManager) {
		this.mLocationPropertyManager = pLocationPropertyManager;
	}



	/**
	 * This method used to get interacts with repository to fetch the stores in JSON response.
	 *
	 * @param pLocationItems the location items
	 * @param pDistance the distance
	 * @param pSkuId the sku id
	 * @param pMyStoreId the my store id
	 * @return the JSON array
	 */
	public JSONArray generateJSONResponse(Collection<RepositoryItem> pLocationItems,Double pDistance,String pSkuId,String pMyStoreId){
		if (isLoggingDebug()) {
			logDebug("Start : PARALStoreLocatorTools.generateJSONResponse (Collection<RepositoryItem> pLocationItems,Double pDistance,String pSkuId)");
		}
		//Note : The actual implemenation of this class is available in TRU.Store.Commerce module
		JSONArray jsonArray = new JSONArray();
		if (isLoggingDebug()) {
			logDebug("End : PARALStoreLocatorTools.generateJSONResponse (Collection<RepositoryItem> pLocationItems,Double pDistance,String pSkuId)");
		}
		return jsonArray;
	}


	/**
	 * This method is used to convert the 24Hour format time to 12 hour format time.
	 *
	 * @param pTime - The input time
	 * @param pTimeType - The timeType i.e. opening time or closing time
	 * @return storeArray - The 12 hour format value
	 */
	public String convert24HourTo12Hours(Double pTime,String pTimeType){
		if (isLoggingDebug()) {
			logDebug("Start : PARALStoreLocatorTools.convert24HourTo12Hours (Double pTime,String pTimeType)");
		}
		if(pTimeType == null){
			return null;
		}

		if(pTime == null){
			if(pTimeType.equalsIgnoreCase(StoreLocatorConstants.OPEN)){
				return StoreLocatorConstants.TEN_AM;
			}else{
				return StoreLocatorConstants.SEVEN_PM;
			}
		}
		//Convert Double to String which should have four digits
		String inputTime = String.valueOf(pTime);

		inputTime = getFourDigitString(inputTime);
		String finalValue = null;
		try {
			Date _24HourDt = null;
	        SimpleDateFormat _24HourSDF = new SimpleDateFormat(StoreLocatorConstants.TWENTY_FOUR_HOUR_FORMAT, Locale.getDefault());
	        SimpleDateFormat _12HourSDF = new SimpleDateFormat(StoreLocatorConstants.TWELVE_HOUR_FORMAT, Locale.getDefault());

	       boolean isValid =   validTimeFormat(inputTime);

	       if(isValid)
	       {
	    	   _24HourDt = _24HourSDF.parse(inputTime);
	       }else
	       {
	    	   if(pTimeType.equalsIgnoreCase(StoreLocatorConstants.OPEN)){
		        	finalValue = StoreLocatorConstants.TEN_AM;
				}else{
					finalValue = StoreLocatorConstants.SEVEN_PM;
				}
	       }

	        if(_24HourDt instanceof Date){
	        	finalValue = _12HourSDF.format(_24HourDt);
	        }

	    } catch (ParseException pe) {
	    	if(isLoggingError()){
				logError("ParseException occurred while parsing the given time i.e. : {0}",pe);
			}
	        if(pTimeType.equalsIgnoreCase(StoreLocatorConstants.OPEN)){
	        	finalValue = StoreLocatorConstants.TEN_AM;
			}else{
				finalValue = StoreLocatorConstants.SEVEN_PM;
			}
	    }

		if(null!=finalValue && finalValue.startsWith(String.valueOf(StoreLocatorConstants.INT_ZERO)))
		{
			finalValue = finalValue.substring(StoreLocatorConstants.INT_ONE,finalValue.length());
		}

		if (isLoggingDebug()) {
			logDebug("End : PARALStoreLocatorTools.convert24HourTo12Hours (Double pTime,String pTimeType)");
		}
		return finalValue;
	}
	/**
	 * This method is used to validate time
	 *
	 * @param pTime - The input store time
	 * @return storeHoursMap - The store working hours map
	 */
	private boolean validTimeFormat(String pTime) {

		if (isLoggingDebug()) {
			logDebug("Start : PARALStoreLocatorTools.validTimeFormat");
		}
        boolean isValidate = false;
		String data = pTime;
		isValidate = data.matches(StoreLocatorConstants.STORE_TIME_REG);
		if (isLoggingDebug()) {
			logDebug("End :PARALStoreLocatorTools.validTimeFormat");
		}
	 return isValidate;

	}

	/**
	 * This method is used to get the storeWorkingHour for the given storeItem.
	 *
	 * @param pStoreItem - The input store item
	 * @return storeHoursMap - The store working hours map
	 */
	public Map<String,String> getStoreWorkingHours(RepositoryItem pStoreItem){
		if (isLoggingDebug()) {
			logDebug("Start : PARALStoreLocatorTools.getStoreWorkingHours (RepositoryItem pStoreItem)");
		}
		if(pStoreItem == null){
			return null;
		}

		List<RepositoryItem> storeHoursRepoItemsList = (List<RepositoryItem>)pStoreItem.getPropertyValue(getLocationPropertyManager().getStoreHoursPropertyName());
		if(storeHoursRepoItemsList == null){
			return null;
		}

		Map<String,String> storeHoursMap = new LinkedHashMap<String,String>();
		StringBuilder timeBuilder = new StringBuilder();
		for(RepositoryItem storeItem : storeHoursRepoItemsList){
			if(storeItem == null){
				continue;
			}
			String weekName = (String)storeItem.getPropertyValue(getLocationPropertyManager().getDayPropertyName());
			timeBuilder.setLength(0);
			Double openingTime = (Double)storeItem.getPropertyValue(getLocationPropertyManager().getOpeningHoursPropertyName());
			Double closingTime = (Double)storeItem.getPropertyValue(getLocationPropertyManager().getClosingHoursPropertyName());
			timeBuilder.append(convert24HourTo12Hours(openingTime, StoreLocatorConstants.OPEN));
			timeBuilder.append(StoreLocatorConstants.HYPON);
			timeBuilder.append(convert24HourTo12Hours(closingTime, StoreLocatorConstants.CLOSE));

			storeHoursMap.put(getWeekNameMap().get(weekName), timeBuilder.toString());
		}
		if (isLoggingDebug()) {
			logDebug("End : PARALStoreLocatorTools.getStoreWorkingHours (RepositoryItem pStoreItem)");
		}
		return storeHoursMap;
	}

	/**
	 * This method is used to get the store closing hours for the given store item
	 *
	 * The store hours format is : "FRI":"11:00 PM-09:00 AM".
	 *
	 * @param pStoreHoursMap - The store hours map
	 * @return dynamicMessage - The closing hours message
	 */
	public String getStoreClosingTime(Map<String,String> pStoreHoursMap){
		if (isLoggingDebug()) {
			logDebug("Start : PARALStoreLocatorTools.getStoreClosingTime(Map<String,String> pStoreHoursMap)");
		}
		if(pStoreHoursMap == null || pStoreHoursMap.isEmpty()){
			if(isLoggingWarning()){
				logWarning("The storeHoursMap is NULL or Empty");
			}
			return null;
		}
		//get the Current Day from the CurrentDate component
		//The getCurrentDate().getDayOfWeekName() will be in the format of : "Sunday", "Monday", etc.
		String currentDay = getWeekNameMap().get(getCurrentDate().getDayOfWeekName());
		//storeHours format is : 09:00 AM - 09:00 PM
		String storeHours = pStoreHoursMap.get(currentDay);
		if(StringUtils.isBlank(storeHours)){
			if (isLoggingDebug()) {
				vlogDebug("The storeHours is not configured for the day : {0} ",currentDay);
				logDebug("End : PARALStoreLocatorTools.getStoreServices(RepositoryItem pStoreItem)");
			}
			return null;
		}
		String[] storeHoursArray = StringUtils.splitStringAtCharacter(storeHours, StoreLocatorConstants.CHAR_HIPHEN);
		String endingHours = null;
		String dynamicMessage = getClosingHoursMsg();
		String closingHoursPartTime = null;
		StringBuilder endingHoursBuilder = new StringBuilder();
		if(storeHoursArray != null && storeHoursArray.length >= StoreLocatorConstants.INTEGER_TWO ){
			//closingHours format is : 08:00 PM
			String closingHours = storeHoursArray[StoreLocatorConstants.INT_ONE];
			if(!StringUtils.isBlank(closingHours)){
				//closingHoursArray format 09:00 PM
				String[] closingHoursArray = StringUtils.splitStringAtCharacter(closingHours, StoreLocatorConstants.CHAR_COLON);
				if(closingHoursArray != null && closingHoursArray.length >= StoreLocatorConstants.INTEGER_TWO ){

					endingHours = closingHoursArray[StoreLocatorConstants.INT_ZERO];
					//If First letter is zero remove it
					if(endingHours.startsWith(StoreLocatorConstants.STRING_ZERO)){
						endingHours = endingHours.substring(StoreLocatorConstants.INT_ONE);
					}
					closingHoursPartTime = closingHoursArray[StoreLocatorConstants.INT_ONE].substring(StoreLocatorConstants.INT_ZERO,StoreLocatorConstants.INT_TWO);

					if(closingHoursPartTime.equalsIgnoreCase(StoreLocatorConstants.STRING_THIRTY)){
						endingHours = endingHours + StoreLocatorConstants.CHAR_COLON + closingHoursPartTime;
					}

					endingHoursBuilder.append(endingHours);
					if(closingHoursArray[StoreLocatorConstants.INT_ONE] != null && closingHoursArray[StoreLocatorConstants.INT_ONE].length() >= StoreLocatorConstants.INT_FIVE){
						//Get the AM or PM value also
						String timeFormat = closingHoursArray[StoreLocatorConstants.INT_ONE].substring(StoreLocatorConstants.INT_THREE);
						if(!StringUtils.isBlank(timeFormat)){
							endingHoursBuilder.append(timeFormat.toLowerCase());
						}
					}
				}
			}
		}
		if(!StringUtils.isBlank(endingHours)){
			//Prepare message
			dynamicMessage = StringUtils.replace(dynamicMessage, StoreLocatorConstants.ZERO_IN_FLOWER_BRACKETS, endingHoursBuilder.toString());
		}else{
			if(isLoggingWarning()){
				vlogWarning("The endingHours value is null for the day : {0} for the storeHoursMap : {1}", currentDay,pStoreHoursMap);
			}
			//The default message
			//dynamicMessage = StringUtils.replace(dynamicMessage, StoreLocatorConstants.ZERO_IN_FLOWER_BRACKETS, StoreLocatorConstants.TIME_SEVEN_PM);
		}
		if (isLoggingDebug()) {
			vlogDebug("The closing time message is : {0} for the storeHoursMap : {1} and for the currentDay : {2}",dynamicMessage,pStoreHoursMap,currentDay);
			logDebug("End : PARALStoreLocatorTools.getStoreClosingTime(Map<String,String> pStoreHoursMap)");
		}
		return dynamicMessage;
	}

	/**
	 * This method is used to get the store services by giving the storeItem.
	 *
	 * @param pStoreItem - The store item
	 * @return serviceDetailsMap - The service details map
	 */
	public Map<String,String> getStoreServices(RepositoryItem pStoreItem){
		if (isLoggingDebug()) {
			logDebug("Start : PARALStoreLocatorTools.getStoreServices(RepositoryItem pStoreItem)");
		}
		if(pStoreItem == null){
			if(isLoggingWarning()){
				logWarning("The given StoreItem is NULL");
			}
			return null;
		}
		List<RepositoryItem> serviceRepoItems = (List)(pStoreItem.getPropertyValue(getLocationPropertyManager().getLocationServicesPropertyName()));
		if(serviceRepoItems == null || serviceRepoItems.isEmpty()){
			return null;
		}
		Map<String,String> serviceDetailsMap = new HashMap<String,String>();
		for(RepositoryItem serviceItem : serviceRepoItems){
			if(serviceItem == null){
				continue;
			}
			serviceDetailsMap.put((String)serviceItem.getPropertyValue(getLocationPropertyManager().getServiceNamePropertyName()), (String)serviceItem.getPropertyValue(getLocationPropertyManager().getServiceDescriptionPropertyName()));
		}
		if (isLoggingDebug()) {
			vlogDebug("The serviceDetails Map : {0} for the given item is : {1}",serviceDetailsMap,pStoreItem);
			logDebug("End : PARALStoreLocatorTools.getStoreServices(RepositoryItem pStoreItem)");
		}
		return serviceDetailsMap;
	}

	/**
	 * This method is used to convet the given map to Json.
	 *
	 * @param pGenericMap - The given map
	 * @return jsonObject - The json object
	 */
	public JSONObject convertMapToJson(Map<String,String> pGenericMap){
		if (isLoggingDebug()) {
			logDebug("Start : PARALStoreLocatorTools.convertMapToJson(Map<String,String> pGenericMap)");
		}
		JSONObject jsonObject = new JSONObject();
		if(pGenericMap == null || pGenericMap.isEmpty()){
			if(isLoggingWarning()){
				logWarning("The given input Map is NULL or Empty");
			}
			return jsonObject;
		}
		for(String key : pGenericMap.keySet()){
			if(StringUtils.isBlank(key)){
				continue;
			}
			String value = pGenericMap.get(key);
			try {
				jsonObject.put(key, value);
			} catch (JSONException jsonExec) {
				if(isLoggingError()){
					logError("JSONException occurred while calling the jsonObject.put() method",jsonExec);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("The JSONObject value is : {0}",jsonObject);
			logDebug("End : PARALStoreLocatorTools.convertMapToJson(Map<String,String> pGenericMap)");
		}
		return jsonObject;
	}

	/**
	 * This method is used to get the store services by giving the storeItem.
	 *
	 * @param pStoreItem - The store item
	 * @return serviceDetailsMap - The service details map
	 */
	public JSONArray getStoreServicesInJsonFormat(RepositoryItem pStoreItem){
		if (isLoggingDebug()) {
			logDebug("Start : PARALStoreLocatorTools.getStoreServicesInJsonFormat(RepositoryItem pStoreItem)");
		}
		JSONArray jsonArray = new JSONArray();
		if(pStoreItem == null){
			if(isLoggingWarning()){
				logWarning("The given StoreItem is NULL");
			}
			return jsonArray;
		}
		List<RepositoryItem> serviceRepoItems = (List)(pStoreItem.getPropertyValue(getLocationPropertyManager().getLocationServicesPropertyName()));
		
		if(serviceRepoItems == null || serviceRepoItems.isEmpty()){
			return jsonArray;
		}
		Collections.sort(serviceRepoItems, new Comparator<RepositoryItem>(){
				public int compare(RepositoryItem pServiceRepoItems1, RepositoryItem pServiceRepoItems2 ){
					return ((String) pServiceRepoItems1.getPropertyValue(getLocationPropertyManager().getServiceNamePropertyName()))
							.compareTo((String) pServiceRepoItems2.getPropertyValue(getLocationPropertyManager().getServiceNamePropertyName()));
				}
			});
		  
		for(RepositoryItem serviceItem : serviceRepoItems){
			if(serviceItem == null){
				continue;
			}
			JSONObject jsonObj = new JSONObject();
			try {

               String serviceName=(String) serviceItem.getPropertyValue(getLocationPropertyManager().getServiceNamePropertyName());
				List<String> serviceNameList=getServiceNameList();
				if(!serviceNameList.contains(serviceName)){
					if (isLoggingDebug()) {
						vlogDebug("The excluded serviceName is : ",serviceName);
					}
					continue;
				}
					jsonObj.put( getLocationPropertyManager().getServiceNamePropertyName(), serviceItem.getPropertyValue(getLocationPropertyManager().getServiceNamePropertyName()));
					jsonObj.put( getLocationPropertyManager().getServiceDescriptionPropertyName(), serviceItem.getPropertyValue(getLocationPropertyManager().getServiceDescriptionPropertyName()));
					jsonObj.put( getLocationPropertyManager().getLearnMorePropertyName(), serviceItem.getPropertyValue(getLocationPropertyManager().getLearnMorePropertyName()));

				} catch (JSONException jsonExec) {
				if(isLoggingError()){
					logError("JSONException occurred while calling the jsonObject.put() method",jsonExec);
				}
			}
			jsonArray.add(jsonObj);
		}
		if (isLoggingDebug()) {
			vlogDebug("The JSONArray value is : {0} for the given item is : {1}",jsonArray,pStoreItem);
			logDebug("End : PARALStoreLocatorTools.getStoreServicesInJsonFormat(RepositoryItem pStoreItem)");
		}
		return jsonArray;
	}

	/**
	 * This method is used to check whether this store item is eligible for FavStore not.
	 *
	 * @param pStoreItem - The store item
	 * @return favStoreEligible - The favStore eligible value
	 */
	public boolean getFavStoreEligibility(RepositoryItem pStoreItem){
		if (isLoggingDebug()) {
			logDebug("Start : PARALStoreLocatorTools.getFavStoreEligibility(RepositoryItem pStoreItem)");
		}
		boolean favStoreEligible = true;
		if(pStoreItem == null){
			if(isLoggingWarning()){
				logWarning("The given StoreItem is NULL");
			}
			return true;
		}
		List<RepositoryItem> serviceRepoItems = (List)(pStoreItem.getPropertyValue(getLocationPropertyManager().getLocationServicesPropertyName()));
		if(serviceRepoItems == null || serviceRepoItems.isEmpty()){
			return true;
		}
		for(RepositoryItem serviceItem : serviceRepoItems){
			if(serviceItem == null){
				continue;
			}
			String serviceName = (String)serviceItem.getPropertyValue(getLocationPropertyManager().getServiceNamePropertyName());
			if(getToysrusExpressServiceName().equalsIgnoreCase(serviceName) || getBabysrusExpressServiceName().equalsIgnoreCase(serviceName)){
				favStoreEligible = false;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("The favStoreEligible value is : {0} for the storeItem : {1}",favStoreEligible,pStoreItem);
			logDebug("End : PARALStoreLocatorTools.getFavStoreEligibility(RepositoryItem pStoreItem)");
		}
		return favStoreEligible;
	}

	/**
	 * This method is used to convert the 3 digit number to the 4 digit number.
	 *
	 * @param pTime - The input 3 digit String
	 * @return pTime - The four digit number
	 */
	public String getFourDigitString(String pTime){
		if (isLoggingDebug()) {
			logDebug("Start : PARALStoreLocatorTools.getFourDigitString(String pTime)");
		}
		if(StringUtils.isBlank(pTime)){
			return null;
		}
		StringBuilder stringBuilder = null;
		String[] timeArray = StringUtils.splitStringAtCharacter(pTime, StoreLocatorConstants.DOT_CHAR);
		if(timeArray != null && timeArray.length >= StoreLocatorConstants.INT_ONE){
			String time = timeArray[StoreLocatorConstants.INT_ZERO];
			if(!StringUtils.isBlank(time) && time.length() == StoreLocatorConstants.INT_THREE){
				stringBuilder = new StringBuilder();
				return stringBuilder.append(StoreLocatorConstants.INT_ZERO).append(time).append(StoreLocatorConstants.DOT_STRING).append(StoreLocatorConstants.INT_ZERO).toString();
			}
		}
		if (isLoggingDebug()) {
			logDebug("End : PARALStoreLocatorTools.getFourDigitString(String pTime)");
		}
		return pTime;
	}

	/**
	 * This method is used get the message value for the given key.
	 *
	 * @param pKeyName - The input key
	 * @return messageValue - The message value
	 */
	public String getMessageValue(String pKeyName){
		if (isLoggingDebug()) {
			logDebug("Start : PARALStoreLocatorTools.getMessageValue(String pKeyName)");
		}
		if(StringUtils.isBlank(pKeyName)){
			return null;
		}
		String messageValue = null;
		try {
			messageValue = getErrorHandlerTools().getKeyValue(pKeyName,null);
		} catch (RepositoryException e) {
			if(isLoggingError()){
				vlogError(e,"RepositoryException occurred while accessing the  getResourceBundleManager().getResourceBundleTools().getKeyValue(pKeyName,null).For the given key : {0}", pKeyName);
			}
		}
		if(StringUtils.isBlank(messageValue)){
			if(isLoggingWarning()){
				vlogWarning("The message value is NULL or Empty for the given key : {0}", pKeyName);
			}
			messageValue = pKeyName;
		}
		if (isLoggingDebug()) {
			vlogDebug("The message value : {0} for the given key is : {1}",messageValue,pKeyName);
			logDebug("End : PARALStoreLocatorTools.getMessageValue(String pKeyName)");
		}
		return messageValue;
	}


	/**
	 * This method is used to check whether the store timings from Monday to Friday is same or not.
	 *
	 * @param pStoreItem - The input store item
	 * @return showTingsFromMonToFriInOneLine - This flag will tell whether we have to show Mon to Fri timings in one line or not
	 */
	public boolean showTingsFromMonToFriInOneLine(RepositoryItem pStoreItem){
		if (isLoggingDebug()) {
			logDebug("Start : PARALStoreLocatorTools.showTingsFromMonToFriInOneLine (RepositoryItem pStoreItem)");
		}
		if(pStoreItem == null){
			return false;
		}

		List<RepositoryItem> storeHoursRepoItemsList = (List<RepositoryItem>)pStoreItem.getPropertyValue(getLocationPropertyManager().getStoreHoursPropertyName());
		if(storeHoursRepoItemsList == null){
			return false;
		}

		Set<Double> openTimingsSet = new HashSet<Double>();
		Set<Double> closingTimingsSet = new HashSet<Double>();
		boolean showTingsFromMonToFriInOneLine = false;
		int count = StoreLocatorConstants.INT_ZERO;
		for(RepositoryItem storeItem : storeHoursRepoItemsList){
			if(storeItem == null){
				continue;
			}
			String weekName = (String)storeItem.getPropertyValue(getLocationPropertyManager().getDayPropertyName());
			if(StoreLocatorConstants.SATURDAY.equalsIgnoreCase(weekName) || StoreLocatorConstants.SUNDAY.equalsIgnoreCase(weekName)){
				continue;
			}
			count = count+StoreLocatorConstants.INT_ONE;
			Double openingTime = (Double)storeItem.getPropertyValue(getLocationPropertyManager().getOpeningHoursPropertyName());
			Double closingTime = (Double)storeItem.getPropertyValue(getLocationPropertyManager().getClosingHoursPropertyName());
			openTimingsSet.add(openingTime);
			closingTimingsSet.add(closingTime);
		}

		if(openTimingsSet.size() == StoreLocatorConstants.INT_ONE && closingTimingsSet.size() == StoreLocatorConstants.INT_ONE){
			//From Mon to Fri day only
			showTingsFromMonToFriInOneLine = true;
		}

		if (isLoggingDebug()) {
			logDebug("End : PARALStoreLocatorTools.showTingsFromMonToFriInOneLine (RepositoryItem pStoreItem)");
		}
		return showTingsFromMonToFriInOneLine;
	}

	/**
	 * This method is used to get the storeHours in JSONArray format for the given storeItem.
	 *
	 * @param pStoreItem - The input store item
	 * @return jsonArry - The JSONArray object
	 */
	public JSONArray getStoreWorkingHoursInJsonFormat(RepositoryItem pStoreItem){
		if (isLoggingDebug()) {
			logDebug("Start : PARALStoreLocatorTools.getStoreWorkingHoursInJsonFormat (RepositoryItem pStoreItem)");
		}
		JSONObject[] storeHoursArray = null;
		if(pStoreItem == null){
			return new JSONArray();
		}
		List<RepositoryItem> storeHoursRepoItemsList = (List<RepositoryItem>)pStoreItem.getPropertyValue(getLocationPropertyManager().getStoreHoursPropertyName());
		if(storeHoursRepoItemsList == null){
			return new JSONArray();
		}
		storeHoursArray = new JSONObject[storeHoursRepoItemsList.size()];
		StringBuilder timeBuilder = new StringBuilder();
		int count = 0;
		for(RepositoryItem storeItem : storeHoursRepoItemsList){
			if(storeItem == null){
				continue;
			}
			String weekName = (String)storeItem.getPropertyValue(getLocationPropertyManager().getDayPropertyName());
			timeBuilder.setLength(StoreLocatorConstants.INT_ZERO);
			Double openingTime = (Double)storeItem.getPropertyValue(getLocationPropertyManager().getOpeningHoursPropertyName());
			Double closingTime = (Double)storeItem.getPropertyValue(getLocationPropertyManager().getClosingHoursPropertyName());
			timeBuilder.append(convert24HourTo12Hours(openingTime, StoreLocatorConstants.OPEN));
			timeBuilder.append(StoreLocatorConstants.HIPHEN_STRING);
			timeBuilder.append(convert24HourTo12Hours(closingTime, StoreLocatorConstants.CLOSE));
			JSONObject dayJson = new JSONObject();
			try {
				dayJson.put(getWeekNameMap().get(weekName), timeBuilder.toString());
				storeHoursArray[count] = dayJson;
				count = count + StoreLocatorConstants.INT_ONE;
			} catch (JSONException e) {
				if(isLoggingError()){
					logError("JSONException", e);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End : PARALStoreLocatorTools.getStoreWorkingHoursInJsonFormat (RepositoryItem pStoreItem)");
		}
		return new JSONArray(storeHoursArray);
	}


	/**
	 * This is generic method which is used to check whether the given service is configured for the given store or not.
	 *
	 * @param pStoreItem - The store item
	 * @param pServiceName - ServiceName
	 * @return isTheGivenServiceConfiguredForGivenStore - The boolean value
	 */
	public boolean isTheGivenServiceConfiguredForGivenStore(RepositoryItem pStoreItem,String pServiceName){
		if (isLoggingDebug()) {
			logDebug("Start : PARALStoreLocatorTools.isTheGivenServiceConfiguredForGivenStore(RepositoryItem pStoreItem,String pServiceName)");
		}
		if(pStoreItem == null){
			if(isLoggingWarning()){
				logWarning("The given StoreItem is NULL");
			}
			return false;
		}
		List<RepositoryItem> serviceRepoItems = (List)(pStoreItem.getPropertyValue(getLocationPropertyManager().getLocationServicesPropertyName()));
		if(serviceRepoItems == null || serviceRepoItems.isEmpty()){
			return false;
		}
		boolean isTheGivenServiceConfiguredForGivenStore = false;
		for(RepositoryItem serviceItem : serviceRepoItems){
			if(serviceItem == null){
				continue;
			}
			String storeServiceName = (String)serviceItem.getPropertyValue(getLocationPropertyManager().getServiceNamePropertyName());
			if(pServiceName.equalsIgnoreCase(storeServiceName)){
				isTheGivenServiceConfiguredForGivenStore = true;
				break;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("The isTheGivenServiceConfiguredForGivenStore value is : {0} for the given item is : {1} and for the given service name : {2}",isTheGivenServiceConfiguredForGivenStore,pStoreItem,pServiceName);
			logDebug("End : PARALStoreLocatorTools.isTheGivenServiceConfiguredForGivenStore(RepositoryItem pStoreItem,String pServiceName)");
		}
		return isTheGivenServiceConfiguredForGivenStore;
	}

	/**
	 * This method is used to filter the store items based on the Store end date.
	 *
	 * @param pLocationItems - The store items list
	 * @return storeItemList - The filtered store items
	 */
	public Collection<RepositoryItem> getEligibleStoreItems(Collection<RepositoryItem> pLocationItems){
		List<RepositoryItem> storeItemList = new ArrayList<RepositoryItem>();
		if(pLocationItems == null || pLocationItems.isEmpty()){
			return storeItemList;
		}
		for(RepositoryItem storeItem  : pLocationItems){
			if(storeItem == null){
				continue;
			}
			Date stoteItemEndDate = (Date)storeItem.getPropertyValue(getLocationPropertyManager().getEndDatePropertyName());
			if(stoteItemEndDate == null || stoteItemEndDate.after(getCurrentDate().getSecondAsDate())){
				storeItemList.add(storeItem);
			}
		}
		return storeItemList;
	}

	/**
	 * @return the mStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForBabiesrus
	 */
	public String getStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForBabiesrus() {

		return mStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForBabiesrus;
	}

	/**
	 * @param pStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForBabiesrus the StoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForBabiesrus to set
	 */
	public void setStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForBabiesrus(
			String pStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForBabiesrus) {
		this.mStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForBabiesrus = pStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForBabiesrus;
	}

	/**
	 * @return the mStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForToysrus
	 */
	public String getStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForToysrus() {
		return mStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForToysrus;
	}

	/**
	 * @param pStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForToysrus the StoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForToysrus to set
	 */
	public void setStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForToysrus(
			String pStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForToysrus) {
		this.mStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForToysrus = pStoreLocatoreGeoCodeEmptyResultsErrorMsgKeyForToysrus;
	}

	/**
	 * @return the mStoreLocatoreGeoCodeEmptyRepoResults
	 */
	public String getStoreLocatoreGeoCodeEmptyRepoResults() {
		return mStoreLocatoreGeoCodeEmptyRepoResults;
	}

	/**
	 * @param pStoreLocatoreGeoCodeEmptyRepoResults the StoreLocatoreGeoCodeEmptyRepoResults to set
	 */
	public void setStoreLocatoreGeoCodeEmptyRepoResults(
			String pStoreLocatoreGeoCodeEmptyRepoResults) {
		this.mStoreLocatoreGeoCodeEmptyRepoResults = pStoreLocatoreGeoCodeEmptyRepoResults;
	}

	/**
	 * @return the mItemQuantity
	 */
	public String getItemQuantity() {
		return mItemQuantity;
	}

	/**
	 * @param pItemQuantity the mItemQuantity to set
	 */
	public void setItemQuantity(String pItemQuantity) {
		this.mItemQuantity = pItemQuantity;
	}

	/**
	 * @return the mStoreLocatorSignatureGoogleMapURL
	 */
	public String getStoreLocatorSignatureGoogleMapURL() {
		return mStoreLocatorSignatureGoogleMapURL;
	}

	/**
	 * @param pStoreLocatorSignatureGoogleMapURL the mStoreLocatorSignatureGoogleMapURL to set
	 */
	public void setStoreLocatorSignatureGoogleMapURL(
			String pStoreLocatorSignatureGoogleMapURL) {
		this.mStoreLocatorSignatureGoogleMapURL = pStoreLocatorSignatureGoogleMapURL;
	}

	/**
	 * @return the mStoreLocatorSignatureKey
	 */
	public String getStoreLocatorSignatureKey() {
		return mStoreLocatorSignatureKey;
	}

	/**
	 * @param pStoreLocatorSignatureKey the mStoreLocatorSignatureKey to set
	 */
	public void setStoreLocatorSignatureKey(String pStoreLocatorSignatureKey) {
		this.mStoreLocatorSignatureKey = pStoreLocatorSignatureKey;
	}



}