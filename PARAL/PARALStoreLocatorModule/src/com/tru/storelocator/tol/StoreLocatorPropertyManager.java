package com.tru.storelocator.tol;

/**
 * StoreLocator property Manager is used to hold store property names.
 * 
 * @author PA
 */
public class StoreLocatorPropertyManager {

	/**
	 * property to hold Longitude  property name.
	 */
	private String mLongitudePropertyName;
	/**
	 * property to hold Latitude property name.
	 */
	private String mLatitudePropertyName;
	/**
	 * property to hold StoreName property name.
	 */
	private String mStoreNamePropertyName;
	/**
	 * property to hold PostalCode property name.
	 */
	private String mPostalCodePropertyName;
	/**
	 * property to hold County property name.
	 */
	private String mCountyPropertyName;
	/**
	 * property to hold Sites property name.
	 */
	private String mSitesPropertyName;
	/**
	 * property to hold StoreItemDescriptor property name.
	 */
	private String mStoreItemDescriptorPropertyName;
	/**
	 * property to hold City property name.
	 */
	private String mCityPropertyName;
	/**
	 * property to hold StateAddress property name.
	 */
	private String mStateAddressPropertyName;
	/**
	 * PropertyName for mLocationIdPropertyName.
	 */
	private String mLocationIdPropertyName;
	/**
	 * property to hold State property name.
	 */
	private String mStatePropertyName;
	/**
	 * property to hold ZipCode property name.
	 */
	private String mZipCodePropertyName;
	/**
	 * property to hold StoreId property name.
	 */
	private String mStoreIdPropertyName;
	/**
	 * property to hold Email property name.
	 */
	private String mEmailPropertyName;
	/**
	 * property to hold Address1 property name.
	 */
	private String mAddress1PropertyName;
	/**
	 * property to hold Address2 property name.
	 */
	private String mAddress2PropertyName;
	/**
	 * property to hold PhoneNumber property name.
	 */
	private String mPhoneNumberPropertyName;
	/**
	 * property to hold Day property name.
	 */
	private String mDayPropertyName;
	/**
	 * property to hold OpeningHours property name.
	 */
	private String mOpeningHoursPropertyName;
	/**
	 * property to hold ClosingHours property name.
	 */
	private String mClosingHoursPropertyName;
	/**
	 * property to hold Country property name.
	 */
	private String mCountryPropertyName;
	/**
	 * property to hold FaxNumber property name.
	 */
	private String mFaxNumberPropertyName;
	/**
	 * property to hold StoreHours property name.
	 */
	private String mStoreHoursPropertyName;
	/**
	 * property to hold AdditionalInfo property name.
	 */
	private String mAdditionalInfoPropertyName;
	/**
	 * property to hold TradingHoursItemDescriptor name.
	 */
	private String mTradingHoursItemDescriptorPropertyName;
	/**
	 * PropertyName for mTypePropertyName.
	 */
	private String mTypePropertyName;
	/**
	 * PropertyName for mChainCodePropertyName.
	 */
	private String mChainCodePropertyName;
	/**
	 * PropertyName for mStoreDistancePropertyName.
	 */
	private String mStoreDistancePropertyName;
	
	/**
	 * PropertyName for mDistancePropertyName.
	 */
	private String mDistancePropertyName;
	
	/**
	 * PropertyName for mWarehouseLocationCodePropertyName.
	 */
	private String mWarehouseLocationCodePropertyName;
	
	/**
	 * PropertyName for mLocationServicesPropertyName.
	 */
	private String mLocationServicesPropertyName;
	
	/**
	 * PropertyName for mServiceNamePropertyName.
	 */
	private String mServiceNamePropertyName;
	
	/**
	 * PropertyName for mServiceDescriptionPropertyName.
	 */
	private String mServiceDescriptionPropertyName;
	
	/**
	 * PropertyName for LearnMorePropertyName.
	 */
	private String mLearnMorePropertyName;
	
	/**
	 * PropertyName for mEndDatePropertyName.
	 */
	private String mEndDatePropertyName;
	
	/**
	 * @return the mEndDatePropertyName
	 */
	public String getEndDatePropertyName() {
		return mEndDatePropertyName;
	}

	/**
	 * @param pEndDatePropertyName the mEndDatePropertyName to set
	 */
	public void setEndDatePropertyName(String pEndDatePropertyName) {
		this.mEndDatePropertyName = pEndDatePropertyName;
	}

	/**
	 * @return the learnMorePropertyName
	 */
	public String getLearnMorePropertyName() {
		return mLearnMorePropertyName;
	}

	/**
	 * @param pLearnMorePropertyName the learnMorePropertyName to set
	 */
	public void setLearnMorePropertyName(String pLearnMorePropertyName) {
		mLearnMorePropertyName = pLearnMorePropertyName;
	}
	
	/**
	 * @return the locationServicesPropertyName
	 */
	public String getLocationServicesPropertyName() {
		return mLocationServicesPropertyName;
	}

	/**
	 * @param pLocationServicesPropertyName the locationServicesPropertyName to set
	 */
	public void setLocationServicesPropertyName(String pLocationServicesPropertyName) {
		mLocationServicesPropertyName = pLocationServicesPropertyName;
	}

	/**
	 * @return the serviceNamePropertyName
	 */
	public String getServiceNamePropertyName() {
		return mServiceNamePropertyName;
	}

	/**
	 * @param pServiceNamePropertyName the serviceNamePropertyName to set
	 */
	public void setServiceNamePropertyName(String pServiceNamePropertyName) {
		mServiceNamePropertyName = pServiceNamePropertyName;
	}

	/**
	 * @return the serviceDescriptionPropertyName
	 */
	public String getServiceDescriptionPropertyName() {
		return mServiceDescriptionPropertyName;
	}

	/**
	 * @param pServiceDescriptionPropertyName the serviceDescriptionPropertyName to set
	 */
	public void setServiceDescriptionPropertyName(
			String pServiceDescriptionPropertyName) {
		mServiceDescriptionPropertyName = pServiceDescriptionPropertyName;
	}

	/**
	 * @return the mWarehouseLocationCodePropertyName
	 */
	public String getWarehouseLocationCodePropertyName() {
		return mWarehouseLocationCodePropertyName;
	}
	
	/**
	 * @param pWarehouseLocationCodePropertyName the mWarehouseLocationCodePropertyName to set
	 */
	public void setWarehouseLocationCodePropertyName(
			String pWarehouseLocationCodePropertyName) {
		mWarehouseLocationCodePropertyName = pWarehouseLocationCodePropertyName;
	}

	/**
	 * @return the mDistancePropertyName
	 */
	public String getDistancePropertyName() {
		return mDistancePropertyName;
	}
	
	/**
	 * @param pDistancePropertyName the mDistancePropertyName to set
	 */
	public void setDistancePropertyName(String pDistancePropertyName) {
		mDistancePropertyName = pDistancePropertyName;
	}

	/**

	 * @return the StoreDistancePropertyName
	 */
	public String getStoreDistancePropertyName() {
		return mStoreDistancePropertyName;
	}

	/**
	 * @param pStoreDistancePropertyName the StoreDistancePropertyName to set
	 */
	public void setStoreDistancePropertyName(String pStoreDistancePropertyName) {
		mStoreDistancePropertyName = pStoreDistancePropertyName;
	}
	/**

	 * @return the typePropertyName
	 */
	public String getTypePropertyName() {
		return mTypePropertyName;
	}

	/**
	 * @param pTypePropertyName the typePropertyName to set
	 */
	public void setTypePropertyName(String pTypePropertyName) {
		mTypePropertyName = pTypePropertyName;
	}
	/**
	 * @return the ChainCodePropertyName
	 */
	public String getChainCodePropertyName() {
		return mChainCodePropertyName;
	}

	/**
	 * @param pChainCodePropertyName the ChainCodePropertyName to set
	 */
	public void setChainCodePropertyName(String pChainCodePropertyName) {
		mChainCodePropertyName = pChainCodePropertyName;
	}	
	
	/**
	 * @return the locationIdPropertyName
	 */
	public String getLocationIdPropertyName() {
		return mLocationIdPropertyName;
	}

	/**
	 * @param pLocationIdPropertyName the locationIdPropertyName to set
	 */
	public void setLocationIdPropertyName(String pLocationIdPropertyName) {
		mLocationIdPropertyName = pLocationIdPropertyName;
	}

	/**
	 * @return the stateAddressPropertyName
	 */
	public String getStateAddressPropertyName() {
		return mStateAddressPropertyName;
	}

	/**
	 * @param pStateAddressPropertyName
	 *            the stateAddressPropertyName to set
	 */
	public void setStateAddressPropertyName(String pStateAddressPropertyName) {
		mStateAddressPropertyName = pStateAddressPropertyName;
	}

	/**
	 * @return the statePropertyName
	 */
	public String getStatePropertyName() {
		return mStatePropertyName;
	}

	/**
	 * @param pStatePropertyName
	 *            the statePropertyName to set
	 */
	public void setStatePropertyName(String pStatePropertyName) {
		mStatePropertyName = pStatePropertyName;
	}

	/**
	 * @return the longitudePropertyName
	 */
	public String getLongitudePropertyName() {
		return mLongitudePropertyName;
	}

	/**
	 * @param pLongitudePropertyName
	 *            the longitudePropertyName to set
	 */
	public void setLongitudePropertyName(String pLongitudePropertyName) {
		mLongitudePropertyName = pLongitudePropertyName;
	}

	/**
	 * @return the countryPropertyName
	 */
	public String getCountryPropertyName() {
		return mCountryPropertyName;
	}

	/**
	 * @param pCountryPropertyName
	 *            the countryPropertyName to set
	 */
	public void setCountryPropertyName(String pCountryPropertyName) {
		mCountryPropertyName = pCountryPropertyName;
	}

	/**
	 * @return the faxNumberPropertyName
	 */
	public String getFaxNumberPropertyName() {
		return mFaxNumberPropertyName;
	}

	/**
	 * @param pFaxNumberPropertyName
	 *            the faxNumberPropertyName to set
	 */
	public void setFaxNumberPropertyName(String pFaxNumberPropertyName) {
		mFaxNumberPropertyName = pFaxNumberPropertyName;
	}

	/**
	 * @return the storeHoursPropertyName
	 */
	public String getStoreHoursPropertyName() {
		return mStoreHoursPropertyName;
	}

	/**
	 * @param pStoreHoursPropertyName
	 *            the storeHoursPropertyName to set
	 */
	public void setStoreHoursPropertyName(String pStoreHoursPropertyName) {
		mStoreHoursPropertyName = pStoreHoursPropertyName;
	}

	/**
	 * @return the additionalInfoPropertyName
	 */
	public String getAdditionalInfoPropertyName() {
		return mAdditionalInfoPropertyName;
	}

	/**
	 * @param pAdditionalInfoPropertyName
	 *            the additionalInfoPropertyName to set
	 */
	public void setAdditionalInfoPropertyName(String pAdditionalInfoPropertyName) {
		mAdditionalInfoPropertyName = pAdditionalInfoPropertyName;
	}

	/**
	 * @return the tradingHoursItemDescriptorPropertyName
	 */
	public String getTradingHoursItemDescriptorPropertyName() {
		return mTradingHoursItemDescriptorPropertyName;
	}

	/**
	 * @param pTradingHoursItemDescriptorPropertyName
	 *            the tradingHoursItemDescriptorPropertyName to set
	 */
	public void setTradingHoursItemDescriptorPropertyName(
			String pTradingHoursItemDescriptorPropertyName) {
		mTradingHoursItemDescriptorPropertyName = pTradingHoursItemDescriptorPropertyName;
	}

	/**
	 * @return the latitudePropertyName
	 */
	public String getLatitudePropertyName() {
		return mLatitudePropertyName;
	}

	/**
	 * @param pLatitudePropertyName
	 *            the latitudePropertyName to set
	 */
	public void setLatitudePropertyName(String pLatitudePropertyName) {
		mLatitudePropertyName = pLatitudePropertyName;
	}

	/**
	 * @return the storeNamePropertyName
	 */
	public String getStoreNamePropertyName() {
		return mStoreNamePropertyName;
	}

	/**
	 * @param pStoreNamePropertyName
	 *            the storeNamePropertyName to set
	 */
	public void setStoreNamePropertyName(String pStoreNamePropertyName) {
		mStoreNamePropertyName = pStoreNamePropertyName;
	}

	/**
	 * @return the postalCodePropertyName
	 */
	public String getPostalCodePropertyName() {
		return mPostalCodePropertyName;
	}

	/**
	 * @param pPostalCodePropertyName
	 *            the postalCodePropertyName to set
	 */
	public void setPostalCodePropertyName(String pPostalCodePropertyName) {
		mPostalCodePropertyName = pPostalCodePropertyName;
	}

	/**
	 * @return the countyPropertyName
	 */
	public String getCountyPropertyName() {
		return mCountyPropertyName;
	}

	/**
	 * @param pCountyPropertyName
	 *            the countyPropertyName to set
	 */
	public void setCountyPropertyName(String pCountyPropertyName) {
		mCountyPropertyName = pCountyPropertyName;
	}

	/**
	 * @return the sitesPropertyName
	 */
	public String getSitesPropertyName() {
		return mSitesPropertyName;
	}

	/**
	 * @param pSitesPropertyName
	 *            the sitesPropertyName to set
	 */
	public void setSitesPropertyName(String pSitesPropertyName) {
		mSitesPropertyName = pSitesPropertyName;
	}

	/**
	 * @return the storeItemDescriptorPropertyName
	 */
	public String getStoreItemDescriptorPropertyName() {
		return mStoreItemDescriptorPropertyName;
	}

	/**
	 * @param pStoreItemDescriptorPropertyName
	 *            the storeItemDescriptorPropertyName to set
	 */
	public void setStoreItemDescriptorPropertyName(
			String pStoreItemDescriptorPropertyName) {
		mStoreItemDescriptorPropertyName = pStoreItemDescriptorPropertyName;
	}

	/**
	 * @return the cityPropertyName
	 */
	public String getCityPropertyName() {
		return mCityPropertyName;
	}

	/**
	 * @param pCityPropertyName
	 *            the cityPropertyName to set
	 */
	public void setCityPropertyName(String pCityPropertyName) {
		mCityPropertyName = pCityPropertyName;
	}

	/**
	 * @return the zipCodePropertyName
	 */
	public String getZipCodePropertyName() {
		return mZipCodePropertyName;
	}

	/**
	 * @param pZipCodePropertyName
	 *            the zipCodePropertyName to set
	 */
	public void setZipCodePropertyName(String pZipCodePropertyName) {
		mZipCodePropertyName = pZipCodePropertyName;
	}

	/**
	 * @return the storeIdPropertyName
	 */
	public String getStoreIdPropertyName() {
		return mStoreIdPropertyName;
	}

	/**
	 * @param pStoreIdPropertyName
	 *            the storeIdPropertyName to set
	 */
	public void setStoreIdPropertyName(String pStoreIdPropertyName) {
		mStoreIdPropertyName = pStoreIdPropertyName;
	}

	/**
	 * @return the emailPropertyName
	 */
	public String getEmailPropertyName() {
		return mEmailPropertyName;
	}

	/**
	 * @param pEmailPropertyName
	 *            the emailPropertyName to set
	 */
	public void setEmailPropertyName(String pEmailPropertyName) {
		mEmailPropertyName = pEmailPropertyName;
	}

	/**
	 * @return the address1PropertyName
	 */
	public String getAddress1PropertyName() {
		return mAddress1PropertyName;
	}

	/**
	 * @param pAddress1PropertyName
	 *            the address1PropertyName to set
	 */
	public void setAddress1PropertyName(String pAddress1PropertyName) {
		mAddress1PropertyName = pAddress1PropertyName;
	}

	/**
	 * @return the address2PropertyName
	 */
	public String getAddress2PropertyName() {
		return mAddress2PropertyName;
	}

	/**
	 * @param pAddress2PropertyName
	 *            the address2PropertyName to set
	 */
	public void setAddress2PropertyName(String pAddress2PropertyName) {
		mAddress2PropertyName = pAddress2PropertyName;
	}

	/**
	 * @return the phoneNumberPropertyName
	 */
	public String getPhoneNumberPropertyName() {
		return mPhoneNumberPropertyName;
	}

	/**
	 * @param pPhoneNumberPropertyName
	 *            the phoneNumberPropertyName to set
	 */
	public void setPhoneNumberPropertyName(String pPhoneNumberPropertyName) {
		mPhoneNumberPropertyName = pPhoneNumberPropertyName;
	}

	/**
	 * @return the dayPropertyName
	 */
	public String getDayPropertyName() {
		return mDayPropertyName;
	}

	/**
	 * @param pDayPropertyName
	 *            the dayPropertyName to set
	 */
	public void setDayPropertyName(String pDayPropertyName) {
		mDayPropertyName = pDayPropertyName;
	}

	/**
	 * @return the openingHoursPropertyName
	 */
	public String getOpeningHoursPropertyName() {
		return mOpeningHoursPropertyName;
	}

	/**
	 * @param pOpeningHoursPropertyName
	 *            the openingHoursPropertyName to set
	 */
	public void setOpeningHoursPropertyName(String pOpeningHoursPropertyName) {
		mOpeningHoursPropertyName = pOpeningHoursPropertyName;
	}

	/**
	 * @return the closingHoursPropertyName
	 */
	public String getClosingHoursPropertyName() {
		return mClosingHoursPropertyName;
	}

	/**
	 * @param pClosingHoursPropertyName
	 *            the closingHoursPropertyName to set
	 */
	public void setClosingHoursPropertyName(String pClosingHoursPropertyName) {
		mClosingHoursPropertyName = pClosingHoursPropertyName;
	}

	private String mDatePropertyName;

	/**
	 * @return the datePropertyName
	 */
	public String getDatePropertyName() {
		return mDatePropertyName;
	}

	/**
	 * @param pDatePropertyName
	 *            the datePropertyName to set
	 */
	public void setDatePropertyName(String pDatePropertyName) {
		mDatePropertyName = pDatePropertyName;
	}
}
