package com.tru.storelocator.cml;

import java.util.HashMap;
import java.util.Map;

/**
 * This class contains store locator specific configurations.
 * 
 * 
 * @author PA
 * 
 */
public class GeoLocatorConfigurations {
	/**
	 * Property to hold DistanceMap
	 */
	private Map<String, String> mDistanceMap = new HashMap<String, String>();

	/**
	 * Property to hold the zoomlevelMap
	 */
	private Map<String, String> mZoomLevelMap = new HashMap<String, String>();

	/**
	 * property to hold StorelocatorLng
	 */
	private Map<String, String> mStorelocatorLat = new HashMap<String, String>();

	/**
	 * property to hold StorelocatorLng
	 */
	private Map<String, String> mStorelocatorLng = new HashMap<String, String>();
	
	/**
	 * Holds the mSearchableDistance
	 */
	private String mSearchableDistance;
	
	/**
	 * Holds the mDistanceinMiles
	 */
	private boolean mDistanceinMiles;
		
	/**
	 * Holds the mMaxResultsPerPage
	 */
	private int mMaxResultsPerPage;
			
	/**
	 * @return the maxResultsPerPage
	 */
	public int getMaxResultsPerPage() {
		return mMaxResultsPerPage;
	}

	/**
	 * @param pMaxResultsPerPage the maxResultsPerPage to set
	 */
	public void setMaxResultsPerPage(int pMaxResultsPerPage) {
		mMaxResultsPerPage = pMaxResultsPerPage;
	}

	/**
	 * @return the searchableDistance
	 */
	public String getSearchableDistance() {
		return mSearchableDistance;
	}

	/**
	 * @param pSearchableDistance the searchableDistance to set
	 */
	public void setSearchableDistance(String pSearchableDistance) {
		this.mSearchableDistance = pSearchableDistance;
	}

	/**
	 * @return the distanceinMiles
	 */
	public boolean isDistanceinMiles() {
		return mDistanceinMiles;
	}

	/**
	 * @param pDistanceinMiles the distanceinMiles to set
	 */
	public void setDistanceinMiles(boolean pDistanceinMiles) {
		mDistanceinMiles = pDistanceinMiles;
	}

	/**
	 * @return the zoomLevelMap
	 */

	public Map<String, String> getZoomLevelMap() {
		return mZoomLevelMap;
	}

	/**
	 * @param pZoomLevelMap
	 *            the zoomLevelMap to set
	 */
	public void setZoomLevelMap(Map<String, String> pZoomLevelMap) {
		mZoomLevelMap = pZoomLevelMap;
	}	

	/**
	 * @return the distanceMap
	 */
	public Map<String, String> getDistanceMap() {
		return mDistanceMap;
	}

	/**
	 * @param pDistanceMap the distanceMap to set
	 */
	public void setDistanceMap(Map<String, String> pDistanceMap) {
		mDistanceMap = pDistanceMap;
	}

	/**
	 * @return mStorelocatorLat
	 */
	public Map<String, String> getStorelocatorLat() {
		return mStorelocatorLat;
	}

	/**
	 * Sets the latitude
	 * 
	 * @param pStorelocatorLat the StorelocatorLat to set
	 */
	public void setStorelocatorLat(Map<String, String> pStorelocatorLat) {
		this.mStorelocatorLat = pStorelocatorLat;
	}

	/**
	 * @return mStorelocatorLng
	 */
	public Map<String, String> getStorelocatorLng() {
		return mStorelocatorLng;
	}

	/**
	 * Sets the longitude
	 * 
	 * @param pStorelocatorLng the StorelocatorLng	 to set
	 */
	public void setStorelocatorLng(Map<String, String> pStorelocatorLng) {
		this.mStorelocatorLng = pStorelocatorLng;
	}

}