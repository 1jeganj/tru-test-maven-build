package com.tru.storelocator.cml;

import atg.commerce.locations.Coordinate;
/**
 * This class used to set the coordinates.
 * @author PA
 *
 */
public class TRUCoordinate extends Coordinate{

	private boolean mGeoCodeResultsEmpty;
	
	private boolean mGeoCodeExceptionOccurred;
	
	/**
	 * @return the geoCodeResultsEmpty
	 */
	public boolean isGeoCodeResultsEmpty() {
		return mGeoCodeResultsEmpty;
	}
	/**
	 * @param pGeoCodeResultsEmpty the geoCodeResultsEmpty to set
	 */
	public void setGeoCodeResultsEmpty(boolean pGeoCodeResultsEmpty) {
		mGeoCodeResultsEmpty = pGeoCodeResultsEmpty;
	}
	/**
	 * @return the geoCodeExceptionOccurred
	 */
	public boolean isGeoCodeExceptionOccurred() {
		return mGeoCodeExceptionOccurred;
	}
	/**
	 * @param pGeoCodeExceptionOccurred the geoCodeExceptionOccurred to set
	 */
	public void setGeoCodeExceptionOccurred(boolean pGeoCodeExceptionOccurred) {
		mGeoCodeExceptionOccurred = pGeoCodeExceptionOccurred;
	}
	/**
	 * Empty constructor.
	 */
	public TRUCoordinate(){
		//Empty Constructor
	}
	/**
	 * Constructor to assign pLatitude and pLongitude.
	 * @param pLatitude - Latitude
	 * @param pLongitude - Longitude
	 */
	public TRUCoordinate(double pLatitude, double pLongitude)
	  {
		//Call the Super constructor
	    super(pLatitude,pLongitude);
	  }
	
	/**
	 * @return  coOrdrdinateBuilder.toString() - This will show the information available in TRUCodinate.java object
	 */
	public String toString(){
		StringBuilder coOrdrdinateBuilder = new StringBuilder();
		coOrdrdinateBuilder.append("Latitude : ").append(getLatitude()).append(" , Longitude : ").append(getLongitude()).append(" , GeoCodeExceptionOccurred : ").append(isGeoCodeExceptionOccurred()).append(" , GeoCodeResultsEmpty : ").append(isGeoCodeResultsEmpty());
		return coOrdrdinateBuilder.toString();
	}
	

}
