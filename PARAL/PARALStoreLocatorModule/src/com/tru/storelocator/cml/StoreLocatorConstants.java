package com.tru.storelocator.cml;

/**
 * StoreLocatorConstants holds all the constants related to StoreLocatorFunctionality.
 * 
 * @author PA
 * @version 1.0
 */
public class StoreLocatorConstants {
	/**
	 * Constant to hold 'latitude'.
	 */
	public static final String LATITUDE = "latitude";

	/**
	 * Constant to hold 'longitude'.
	 */
	public static final String LONGITUDE = "longitude";

	/** Constant for GEOCODERSTATUS_OK. */
	public static final String GEOCODERSTATUS_OK = "OK";

	/** Constant for GEOCODERSTATUD_ZERORESULTS. */
	public static final String GEOCODERSTATUS_ZERO_RESULTS = "ZERO_RESULTS";

	/** Constant for Application/JSON. */
	public static final String APPLICATIONORJSON = "application/json";

	/** Constant for Accept. */
	public static final String ACCEPT = "Accept";

	/** Constant for postalCode. */
	public static final String POSTAL_CODE = "postalCode";

	/** Constant for Text/JSON. */
	public static final String TEXTORJSON = "text/json";


	/**
	 * constant for storeName.
	 */
	public static final String STORE_NAME = "storeName";

	/**
	 * Constant to hold 'output'.
	 */
	public static final String OUTPUT = "output";

	/**
	 * Constant to hold 'EMPTY_PARAM'.
	 */
	public static final String EMPTY_PARAM = "empty";

	/** Constant for StoreId. */
	public static final String STORE_ID = "StoreID";

	/** Constant for StoreNumber. */
	public static final String STORE_NUMBER = "StoreNumber";

	/** Constant for LocationName. */
	public static final String LOCATION_NAME = "LocationName";

	/** Constant for Street Address. */
	public static final String STREET_ADDRESS = "StreetAddress";

	/** Constant for City. */
	public static final String CITY = "City";

	/** Constant for State. */
	public static final String STATE = "State";

	/** Constant for ZipCode. */
	public static final String ZIPCODE = "ZipCode";

	/** Constant for PhoneNumber. */
	public static final String PHONE_NUMBER = "PhoneNumber";


	/**
	 * constant for ',' character.
	 */
	public static final String COMMA = ",";

	/** Constant for Error. */
	public static final String ERROR = "error";
	/**
	 * Constant for store zoomLevel .
	 */
	public static final String ZOOM_LEVEL = "zoomLevel";

	/**
	 * Constant to hold Store Phone No Format.
	 */
	public static final String STORE_PHONE_NO_FORMAT = "FtPhoneNumber";
	/**
	 * Constant to hold 'phone number'.
	 */
	public static final String STORE_PHONE_NUMBER = "phoneNumber";

	/**
	 * constant for space character.
	 */
	public static final String SPACE = " ";

	/** The Constant EMPTY STRING. */
	public static final String EMPTY_STRING = "";

	/**
	 * constant to hold location.
	 */
	public static final String LOCATION = "location";

	/**
	 * constant to hold distance.
	 */
	public static final String DISTANCE = "distance";

	/**
	 * constant to hold locationResults.
	 */
	public static final String LOCATION_RESULTS = "locationResults";

	/**
	 * constant to hold locationArrayList.
	 */
	public static final String LOCATION_ARRAY_LIST = "locationArrayList";

	/**
	 * constant to hold Please enter valid zipcode.
	 */
	public static final String VALID_ZIPCODE = "Please enter valid zipcode";

	/**
	 * constant to hold empty zipcode.
	 */
	public static final String EMPTY_ZIPCODE = "emptyZipCode";

	/**
	 * constant to hold Sorry!!!. We are not found any reults.
	 */
	public static final String NOT_FOUND_ANY_RESULTS = "Sorry!!!. No Stores Found for the given address.";

	/**
	 * constant to hold No results.
	 */
	public static final String NO_RESULTS = "No results";

	/** Constant for ERROR_SRING. */
	public static final String ERROR_SRING = "error";

	/**
	 * Constant to hold 'isSucess'.
	 */
	public static final String IS_SUCESS = "isSuccess";
	
	/**
	 * Constant to hold 'MILES_TO_KMS_CONVERTER'.
	 */
	public static final double MILES_TO_KMS_CONVERTER = 1.609;
	
	/**
	 * Constant to hold 'KMS_TO_METER_CONVERTER'.
	 */
	public static final double KMS_TO_METER_CONVERTER = 1000;
	
	/**
	 * Constant to hold 'KMS_TO_MILES_CONVERTER'.
	 */
	public static final double KMS_TO_MILES_CONVERTER = 0.621;
	
	
	/**
	 * Constant to hold 'ZERO_IN_DOUBLE'.
	 */
	public static final double ZERO_IN_DOUBLE = 0.00;
	
	
	/**
	 * Constant to hold 'INTEGER_TWO'.
	 */
	public static final int INTEGER_TWO = 2;
	
	
	/** Constant for DISTANCE_IN_MILES. */
	public static final String DISTANCE_IN_MILES = "distanceInMiles";
	
	/** Constant for TEN_AM. */
	public static final String TEN_AM = "10:00 AM";
	
	/** Constant for SEVEN_PM. */
	public static final String SEVEN_PM = "7:00 PM";
	
	/** Constant for OPEN. */
	public static final String OPEN = "open";
	
	/** Constant for CLOSE. */
	public static final String CLOSE = "close";
	
	/** Constant for STORE_HOURS. */
	public static final String STORE_HOURS = "storeHours";
	
	/** Constant for TWENTY_FOUR_HOUR_FORMAT. */
	public static final String TWENTY_FOUR_HOUR_FORMAT = "HHmm.s";
	
	/** Constant for TWELVE_HOUR_FORMAT. */
	public static final String TWELVE_HOUR_FORMAT = "hh:mm a";
	
	/** Constant for SAME_DAY. */
	public static final String SAME_DAY = "SameDay";
	
	/** Constant for NEXT_DAY. */
	public static final String NEXT_DAY = "NextDay";
	
	/** Constant for ON_HAND_QTY. */
	public static final String ON_HAND_QTY = "onHandQty";
	
	/** Constant for DELIVERY. */
	public static final String DELIVERY = "delivery";
	
	/** Constant for STORE_ITEM_REPO_ID. */
	public static final String STORE_ITEM_REPO_ID = "storeItemRepoId";
	
	
	/** Constant for SKU_ID. */
	public static final String SKU_ID = "skuId";
	
	/** Constant for STORES. */
	public static final String STORES = "stores";
	
	/** Constant for INT_ZERO. */
	public static final int INT_ZERO = 0;
	
	/** Constant for INT_ONE. */
	public static final int INT_ONE = 1;
	
	
	/** Constant for STRING_ZERO. */
	public static final String STRING_ZERO = "0";
	
	/** Constant for CHAR_COLON. */
	public static final char CHAR_COLON = ':';
	
	/** Constant for CHAR_HIPHEN. */
	public static final char CHAR_HIPHEN = '-';
	
	/** Constant for CLOSING_TIME_MSG. */
	public static final String CLOSING_TIME_MSG = "closingTimeMsg";
	
	/** Constant for STORE_SERVICES. */
	public static final String STORE_SERVICES = "storeServices";
	
	/** Constant for GOOGLE_MAPS */
	public static final String GOOGLE_MAPS = "googleMaps";
	
	/** Constant for IS_ELIGIBLE_FOR_FAV_STORE. */
	public static final String IS_ELIGIBLE_FOR_FAV_STORE = "isEligibleForfavStore";
	
	/** Constant for DOT_STRING. */
	public static final String DOT_STRING = ".";
	
	/** Constant for SHOW_TIMINGS_FROM_MON_TO_FRI_IN_ONE_LINE. */
	public static final String SHOW_TIMINGS_FROM_MON_TO_FRI_IN_ONE_LINE = "showTingsFromMonToFriInOneLine";
	
	/** Constant for SATURDAY. */
	public static final String SATURDAY = "Saturday";
	
	/** Constant for SUNDAY. */
	public static final String SUNDAY = "Sunday";
	
	/** Constant for GEO_CODE_EXCEPTION. */
	public static final String GEO_CODE_EXCEPTION = "GeoCodeException";
	
	/** Constant for GEO_CODE_RESULTS_EMPTY. */
	public static final String GEO_CODE_RESULTS_EMPTY = "GeoCodeResultsEmpty";
	
	/** Constant for DOUBLE_ZERO. */
	public static final double DOUBLE_ZERO = 0.00;
	
	/** Constant for HIPHEN_STRING. */
	public static final String HIPHEN_STRING = "-";
	
	/** Constant for DOT_CHAR. */
	public static final char DOT_CHAR = '.';
	
	/** Constant for INT_THREE. */
	public static final int INT_THREE = 3;
	
	/** Constant for STRING_SEVEN. */
	public static final String STRING_SEVEN = "7";
	
	/** Constant for ZERO_IN_FLOWER_BRACKETS. */
	public static final String ZERO_IN_FLOWER_BRACKETS = "{0}";
	
	
	/** Constant for INT_TWO. */
	public static final int INT_TWO = 2;
	
	/** Constant for STORE. */
	public static final String STORE = "store";
	
	/** Constant for STORE_HOURS_MSG. */
	public static final String STORE_HOURS_MSG = "storeHoursMsg";
	
	/** Constant for DOUBLE_ZERO_FORMAT. */
	public static final double DOUBLE_ZERO_FORMAT = 0.0D;
	
	/** Constant for DOUBLE_MINUS_ONE. */
	public static final double DOUBLE_MINUS_ONE = -1.0D;
	
	/** Constant for JSON_CONTENT_TYPE. */
	public static final String JSON_CONTENT_TYPE = "application/json";
	
	/** Constant for MON_FRI. */
	public static final String MON_FRI = "MON-FRI";
	
	/** Constant for WEEK_NAME_MON. */
	public static final String WEEK_NAME_MON = "MON";
			 
	/** Constant for WEEK_NAME_SAT. */
	public static final String WEEK_NAME_SAT = "SAT";
	
	/** Constant for WEEK_NAME_SUN. */
	public static final String WEEK_NAME_SUN = "SUN";
	
	/** Constant for TIME_SEVEN_PM. */
	public static final String TIME_SEVEN_PM = "7pm";
	
	/** Constant for INT_FIVE. */
	public static final int INT_FIVE = 5;
	
	/** Constant for IS_SUPER_STORE_AVAIL. */
	public static final String IS_SUPER_STORE_SERVICE_AVAIL = "isSuperStoreServiceAval";
	
	/** Constant for REST_CONTEXT. */
	public static final String REST_CONTEXT = "/rest";
	
	/** Constant for INT_FIVE. */
	public static final int INT_THOUSAND = 1000;
	
	/** Constant for MYSTORE_ID. */
	public static final String MYSTORE_ID ="myStoreId";
	
	/** Constant for ERROR_MESSAGE. */
	public static final String ERROR_MESSAGE="errorMessage";
	
	/** Constant for HYPON. */
	public static final String HYPON="-";
	
	/** Constant for STORE_END_DATE. */
	public static final String STORE_END_DATE = "storeEndDate";
	
	/** Constant for STORE_ADDRESS1. */
	public static final String STORE_ADDRESS1 = "address1";
	
	/** Constant for STORE_ADDRESS1. */
	public static final String STORE_ADDRESS2 = "address2";
  	/** Constant for Regular Expression */
	public static final String STORE_TIME_REG = "[0-9]{4}.\\d";
	
	/** Constant for STRING_THIRTY. */
	public static final String STRING_THIRTY = "30";
	
	/** Constant for SIGNATURE. */
	public static final String SIGNATURE ="signature";
	
	/** Constant for PLUS. */
	public static final char PLUS ='+';
	
	/** Constant for SPLUS. */
	public static final String SPLUS ="+";
	
	/** Constant for MINUS. */
	public static final char MINUS ='-';
	
	/** Constant for SLASH. */
	public static final char SLASH ='/';
	
	/** Constant for UNDERSCORE. */
	public static final char UNDERSCORE ='_';
	
	/** Constant for QUESTION_MARK. */
	public static final char QUESTION_MARK ='?';
	
	/** Constant for SECRET_KEY_SPEC. */
	public static final String SECRET_KEY_SPEC = "HmacSHA1";
	
}
