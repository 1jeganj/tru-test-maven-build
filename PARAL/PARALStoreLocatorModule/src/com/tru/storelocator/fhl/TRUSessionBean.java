/**
 * 
 */
package com.tru.storelocator.fhl;


import atg.nucleus.GenericService;



// TODO: Auto-generated Javadoc
/**
 * TRUSessionBean class extends GenericService and hold setters and getters of property JsonArrayObject.
 *
 * @author PA
 */
public class TRUSessionBean extends GenericService {

	/** Property to hold mJsonArrayObject of type String. */
	private String mJsonArrayObject;
	
	
	/**
	 * Gets the json array object.
	 *
	 * @return mJsonArrayObject
	 */
	public String getJsonArrayObject() {
		return mJsonArrayObject;
	}

	/**
	 * Sets the json array object.
	 *
	 * @param pJsonArrayObject - JsonArrayObject to set
	 */
	public void setJsonArrayObject(String pJsonArrayObject) {
		this.mJsonArrayObject = pJsonArrayObject;
	}

	
}
