package com.tru.storelocator.fhl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpMethodParams;

import atg.commerce.locations.Coordinate;
import atg.core.util.StringUtils;

import com.google.code.geocoder.AdvancedGeoCoder;
import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;
import com.google.code.geocoder.model.GeocoderStatus;
import com.tru.logging.TRUIntegrationInfoLogger;
import com.tru.storelocator.cml.StoreLocatorConstants;
import com.tru.storelocator.cml.TRUCoordinate;
import com.tru.storelocator.tol.PARALStoreLocatorTools;
/**
 * This class extends AbstractGeoLocatorProvider class and implements GeoLocatorProvider interface methods.
 * This class uses Google GeoCoder service to get longitude and latitude of given postalcode/ address/city/state.
 *  
 * @author PA
 * 
 */
public class DefaultGeoLocatorProvider extends AbstractGeoLocatorProvider {
	/**
	 * Holds the mGeocoderStatus
	 */
	GeocoderStatus mGeocoderStatus;	
	
	/**
	 * Holds the mGeoCodeExceOccurred
	 */
	private boolean mGeoCodeExceOccurred;
	
	/**
	 * Holds the mGeoCodeResultsEmpty
	 */
	private boolean mGeoCodeResultsEmpty;
	
	/**
	 * Holds the mLocationTools
	 */
	PARALStoreLocatorTools mLocationTools;
	
	/** The mIntegrationInfoLogger. */
	private TRUIntegrationInfoLogger mIntegrationInfoLogger;
	
	/**
	 * @return the mIntegrationInfoLogger
	 */
	public TRUIntegrationInfoLogger getIntegrationInfoLogger() {
		return mIntegrationInfoLogger;
	}

	/**
	 * @param pIntegrationInfoLogger the mIntegrationInfoLogger to set
	 */
	public void setIntegrationInfoLogger(TRUIntegrationInfoLogger pIntegrationInfoLogger) {
		mIntegrationInfoLogger = pIntegrationInfoLogger;
	}
	
	/**
	 * @return the locationTools
	 */
	public PARALStoreLocatorTools getLocationTools() {
		return mLocationTools;
	}
	/**
	 * @param pLocationTools
	 *            the locationTools to set
	 */
	public void setLocationTools(PARALStoreLocatorTools pLocationTools) {
		this.mLocationTools = pLocationTools;
	}	
	
	/**
	 * @return the geoCodeExceOccurred
	 */
	public boolean isGeoCodeExceOccurred() {
		return mGeoCodeExceOccurred;
	}
	/**
	 * @param pGeoCodeExceOccurred the geoCodeExceOccurred to set
	 */
	public void setGeoCodeExceOccurred(boolean pGeoCodeExceOccurred) {
		mGeoCodeExceOccurred = pGeoCodeExceOccurred;
	}
	/**
	 * @return the geoCodeResultsEmpty
	 */
	public boolean isGeoCodeResultsEmpty() {
		return mGeoCodeResultsEmpty;
	}
	/**
	 * @param pGeoCodeResultsEmpty the geoCodeResultsEmpty to set
	 */
	public void setGeoCodeResultsEmpty(boolean pGeoCodeResultsEmpty) {
		mGeoCodeResultsEmpty = pGeoCodeResultsEmpty;
	}
	
	/**
	 * @return the geocoderStatus
	 */
	public GeocoderStatus getGeocoderStatus() {
		return mGeocoderStatus;
	}
	/**
	 * @param pGeocoderStatus
	 *            the geocoderStatus to set
	 */
	public void setGeocoderStatus(GeocoderStatus pGeocoderStatus) {
		this.mGeocoderStatus = pGeocoderStatus;
	}
	
	/**
	 * This method is overridden to fetch Longitude and latitude of postal code or address provided by the user.
	 * 
	 * @param pPostalAddress String
	 * @param pLocale Locale
	 * @return coordinate
	 */
	public Coordinate getGeoLocation(String pPostalAddress, Locale pLocale) {
		if (isLoggingDebug()) {
			logDebug("DefaultGeoLocatorProvider :getGeoLocation : Starts");
			logDebug("PostalCode/Address::"+pPostalAddress+"and Locale: "+pLocale);
		}
		Coordinate coordiante = getLocation(pPostalAddress,null,pLocale);
		if (isLoggingDebug()) {
			logDebug("DefaultGeoLocatorProvider :getGeoLocation : Ends");
		}
		return coordiante;
	}
	/**
	 * This method is overridden to fetch Longitude and latitude of city and state provided by the user.
	 * @param pCity String
	 * @param pState String
	 * @param pLocale Locale
	 * @return coordinate
	 */
	public Coordinate getGeoLocation(String pCity, String pState,Locale pLocale) {
		if (isLoggingDebug()) {
			logDebug("DefaultGeoLocatorProvider :getGeoLocation : Starts");
			logDebug("City::"+pCity+"and State: "+pState);
		}
		Coordinate coordiante =  getLocation(pCity, pState, pLocale);
		if (isLoggingDebug()) {
			logDebug("DefaultGeoLocatorProvider :getGeoLocation : Ends");
		}
		return coordiante;
	}

	/**
	 * This method used to fetch the longitude and latitude and returns the Coordinate object.
	 * @param pParamString1 postalCode/address/city
	 * @param pParamString2 state
	 * @param pLocale Locale
	 * @return coordinate
	 */
	public Coordinate getLocation(String pParamString1, String pParamString2, Locale pLocale){
		if (isLoggingDebug()) {
			logDebug("DefaultGeoLocatorProvider :getGeoLocation : Starts");
			logDebug("pParamString1::"+pParamString1+"and pParamString2: "+pParamString2);
		}
		Coordinate coordinate = null;
		//this method returns GeocoderResult list.
		List resultList = getGeoCoderList(pParamString1, pParamString2, pLocale);
		
		if(isGeoCodeExceOccurred()){
			//GeoCode exception occurred
			TRUCoordinate truCoordinate = new TRUCoordinate();
			truCoordinate.setGeoCodeExceptionOccurred(true);
			return truCoordinate;
		}
		if(isGeoCodeResultsEmpty()){
			//Geocode results are empty
			TRUCoordinate truCoordinate = new TRUCoordinate();
			truCoordinate.setGeoCodeResultsEmpty(true);
			return truCoordinate;
		}
		
		if(resultList == null){
			//GeoCode exception occurred
			TRUCoordinate truCoordinate = new TRUCoordinate();
			truCoordinate.setGeoCodeExceptionOccurred(true);
			return truCoordinate;
			
		}
		if(resultList.isEmpty()){
			//Geocode results are empty
			TRUCoordinate truCoordinate = new TRUCoordinate();
			truCoordinate.setGeoCodeResultsEmpty(true);
			return truCoordinate;
		}
		
		if(resultList != null && !resultList.isEmpty()){
			Iterator itr = resultList.iterator();
			GeocoderResult geocoderResult = null;
			while (itr.hasNext()){
				//Start : TUW-29671[Store locator - Search results are not displayed when searched by City but display results when searched by Zipcode of same city on PDP] issue is fixed
				geocoderResult = (GeocoderResult) itr.next();
				break;
				//End : TUW-29671[Store locator - Search results are not displayed when searched by City but display results when searched by Zipcode of same city on PDP] issue is fixed
			}
			//get the latitude and longitude from GeocoderResult.
			if(geocoderResult != null){
				double latitude = geocoderResult.getGeometry().getLocation().getLat().doubleValue();
				double longitude = geocoderResult.getGeometry().getLocation().getLng().doubleValue();
				coordinate  = new Coordinate(latitude, longitude);
			}
		}
		if(coordinate != null && isLoggingInfo()){
			vlogInfo("The Coordinate latitude : {0} and the Coordinate longitude is : {1} " , coordinate.getLatitude(),coordinate.getLongitude());
		}
		if (isLoggingDebug()) {
			logDebug("DefaultGeoLocatorProvider :getGeoLocation : Ends");
		}
		return coordinate;
	}
	
	/**
	 * This method used to fetch the geoCoderResult list using Google GeoCoder API.
	 * @param pParamString1 String
	 * @param pParamString2 String
	 * @param pLocale String
	 * @return resultList
	 */
	private List getGeoCoderList(String pParamString1, String pParamString2, Locale pLocale){
		if (isLoggingDebug()) {
			logDebug("DefaultGeoLocatorProvider :getGeoCoderList : Starts");
			logDebug("pParamString1::"+pParamString1+"and pParamString2: "+pParamString2);
		}
		List<GeocoderResult> resultList = null;
		//Start : Timeout and proxy configuration
		Geocoder geocoder = null;
		HttpClient httpClient = getHtppClient();
		if(httpClient == null){
			geocoder = new Geocoder();
		}else{
			geocoder = new AdvancedGeoCoder(httpClient);
		}
		//End : Timeout and proxy configuration
		
		GeocoderRequest geocoderRequest = null;
		//If postal Code or Address or city or state is passed, search based on the input passed.
		if(!StringUtils.isBlank(pParamString1) && StringUtils.isBlank(pParamString2)){
			geocoderRequest = new GeocoderRequestBuilder().setAddress(pParamString1).setLanguage(pLocale.getLanguage()).getGeocoderRequest();
		}else if(!StringUtils.isBlank(pParamString1) &&  !StringUtils.isBlank(pParamString2)){
			//If City and state both are present, search based on city and state.
			StringBuffer cityAndState = new StringBuffer();
			cityAndState.append(pParamString1);
			cityAndState.append(StoreLocatorConstants.COMMA);
			cityAndState.append(pParamString2);
			geocoderRequest = new GeocoderRequestBuilder().setAddress(cityAndState.toString()).setLanguage(pLocale.getLanguage()).getGeocoderRequest();
		}
		GeocodeResponse geocoderResponse;
		Date geocodeServiceStartTime = null;
		Date geocodeServiceEndTime = null;
		try {
			getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(StoreLocatorConstants.GOOGLE_MAPS), 
					getIntegrationInfoLogger().getInterfaceNameMap().get(StoreLocatorConstants.STORE_SERVICES), null, null);
			geocodeServiceStartTime = getLocationTools().getCurrentDate().getSecondAsDate();
			if(isLoggingInfo()){
				vlogInfo("GeocodeServiceStartTime value : {0} for the request : {1} ", geocodeServiceStartTime.toString(),pParamString1);
			}
			geocoderResponse = geocoder.geocode(geocoderRequest);
			geocodeServiceEndTime = getLocationTools().getCurrentDate().getSecondAsDate();
			if(isLoggingInfo()){
				vlogInfo("GeocodeServiceEndTime1 value : {0} for the request : {1} ", geocodeServiceEndTime.toString(),pParamString1);
			}
			if(geocoderResponse != null){
				resultList = new ArrayList<GeocoderResult>();
				resultList = geocoderResponse.getResults();
				setGeocoderStatus(geocoderResponse.getStatus());
				getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(StoreLocatorConstants.GOOGLE_MAPS), 
						getIntegrationInfoLogger().getInterfaceNameMap().get(StoreLocatorConstants.STORE_SERVICES), null, null);
			}
		}catch (IOException e) {
			geocodeServiceEndTime = getLocationTools().getCurrentDate().getSecondAsDate();
			if(isLoggingInfo()){
				vlogInfo("GeocodeServiceEndTime2 value : {0} for the request : {1} ", geocodeServiceEndTime.toString(),pParamString1);
				vlogInfo(e,"GEOCode Service IOexception occurred while calling the geocoder.geocode(geocoderRequest) for the GeoCodeRequest : {0} ",geocoderRequest);
			}
			if(isLoggingError()){
				vlogError(e,"GEOCode Service IOexception occurred while calling the geocoder.geocode(geocoderRequest) for the GeoCodeRequest : {0} ",geocoderRequest);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Result list in getGeoCoderList()method ::" +resultList);
			logDebug("DefaultGeoLocatorProvider :getGeoLocation : Ends");
		}
		return resultList;		
	}
	
	/**
	 * This method is used to get the HTTPClient which is used to set the Timeout and proxy
	 * @return httpClient - The HTTPClient object
	 */
	public HttpClient getHtppClient(){
		if (isLoggingDebug()) {
			logDebug("Start : DefaultGeoLocatorProvider.getHtppClient()");
		}
		HttpClient httpClient = new org.apache.commons.httpclient.HttpClient(new MultiThreadedHttpConnectionManager());
		if(getLocationTools().isEnableTimeout()){
			//Is Timeout enabled
			//org.apache.commons.httpclient.HttpClient is OLD and which is depricated(HttpClient3.x)
			//So use the latest org.apache.http.client.HttpClient (HttpClient4.x)
			
			
			//HttpClient httpClient = new HttpClient(new MultiThreadedHttpConnectionManager());
			httpClient.getParams().setParameter(HttpMethodParams.SO_TIMEOUT, getLocationTools().getTimeoutInSeconds() * StoreLocatorConstants.INT_THOUSAND); //15s
		}
		if(getLocationTools().isEnableProxy()){
			//Is Proxy enabled
			httpClient.getHostConfiguration().setProxy(getLocationTools().getProxyIp(), getLocationTools().getProxyPort());
		}
		if(getLocationTools().isEnableTimeout() || getLocationTools().isEnableProxy()){
			return httpClient;
		}
		if (isLoggingDebug()) {
			logDebug("End : DefaultGeoLocatorProvider.getHtppClient()");
		}
		return null;
	}
	
	
}

