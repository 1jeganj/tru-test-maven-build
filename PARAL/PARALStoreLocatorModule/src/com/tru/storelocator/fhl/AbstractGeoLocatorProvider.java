package com.tru.storelocator.fhl;

import atg.commerce.locations.GeoLocatorProvider;
import atg.nucleus.GenericService;

/**
 * This Abstract class implements the GeoLocatorProvider interface and define methods like
 * isAllowCityAndState() and isAllowFreeFormEntry() methods.
 * 
 * @author PA
 */
public abstract  class AbstractGeoLocatorProvider extends GenericService implements GeoLocatorProvider {

	/**
	 * Holds the mAllowCityAndState property
	 */
	public boolean mAllowCityAndState;

	/**
	 * Holds the mAllowFreeFormEntry property
	 */
	public boolean mAllowFreeFormEntry;

	/**
	 * @return the allowCityAndState
	 */
	public boolean isAllowCityAndState() {
		return mAllowCityAndState;
	}

	/**
	 * @param pAllowCityAndState
	 *            the allowCityAndState to set
	 */
	public void setAllowCityAndState(boolean pAllowCityAndState) {
		this.mAllowCityAndState = pAllowCityAndState;
	}

	/**
	 * @return the allowFreeFormEntry
	 */
	public boolean isAllowFreeFormEntry() {
		return mAllowFreeFormEntry;
	}

	/**
	 * @param pAllowFreeFormEntry
	 *            the allowFreeFormEntry to set
	 */
	public void setAllowFreeFormEntry(boolean pAllowFreeFormEntry) {
		this.mAllowFreeFormEntry = pAllowFreeFormEntry;
	}	
}
