package com.tru.storelocator.fhl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;

import atg.commerce.locations.Coordinate;
import atg.commerce.locations.GeoLocatorFormHandler;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.google.code.geocoder.model.GeocoderStatus;
import com.tru.storelocator.cml.GeoLocatorConfigurations;
import com.tru.storelocator.cml.StoreLocatorConstants;
import com.tru.storelocator.cml.TRUCoordinate;
import com.tru.storelocator.mgl.PARALStoreLocatorManager;

/**
 * This class extends GeoLocatorFormHandler.
 *  handleLocateItems() method is overridden to validate the input postal Address and
 *  to convert the distance from Miles to meters.
 *
 * @author PA
 */

public class PARALGeoLocatorFormHandler extends GeoLocatorFormHandler {
	/**
	 * Holds the mPostalAddress property
	 */
	private String mPostalAddress;

	/**
	 * Holds the mAddressStatus property
	 */
	private String mAddressStatus;

	/**
	 * Holds the mCurrentDate property
	 */
	private String mCurrentDate;

	/**
	 * Holds the mGeocoderStatus
	 */
	GeocoderStatus mGeocoderStatus;

	/**
	 * Holds the mDistanceinMiles
	 */
	private boolean mDistanceinMiles=false;

	/**
	 * Holds the mDistanceinMiles
	 */
	private GeoLocatorConfigurations mGeoConfigurations;
	/**
	 * SessionBean
	 */
	private TRUSessionBean mSessBean;
	/**
	 * holds the mLocationManager.
	 */
	private PARALStoreLocatorManager mLocationManager;

	/** Property to hold mJsonArrayObject of type String. */
	private String mJsonArrayObject;

	/** Property to hold mSkuId */
	private String mSkuId;

	/** Property to hold mGeoCodeResultsEmpty */
	private boolean mGeoCodeResultsEmpty;

	/** Property to hold mGeoCodeExceptionOccurred */
	private boolean mGeoCodeExceptionOccurred;

	/** Property to hold mRepositoryExceOccurred */
	private boolean mRepositoryExceOccurred;

	/** Property to hold mMyStoreId */
	private String mMyStoreId;
	
	/** Property to hold mRestService */
	private boolean mRestService;
	
	/** Property to hold mItemQuantity */
	private String mItemQuantity;
	
	/** Property to hold errorURLCustom */
	private String mErrorURLCustom;
	
	/** Property to hold successURLCustom */
	private String mSuccessURLCustom;
	
	/**
	 * @return the mErrorURLCustom
	 */
	public String getErrorURLCustom() {
		return mErrorURLCustom;
	}
	/**
	 * @param pErrorURLCustom the mErrorURLCustom to set
	 */
	public void setErrorURLCustom(String pErrorURLCustom) {
		mErrorURLCustom = pErrorURLCustom;
	}
	/**
	 * @return the mSuccessURLCustom
	 */
	public String getSuccessURLCustom() {
		return mSuccessURLCustom;
	}
	/**
	 * @param pSuccessURLCustom the mSuccessURLCustom to set
	 */
	public void setSuccessURLCustom(String pSuccessURLCustom) {
		mSuccessURLCustom = pSuccessURLCustom;
	}
	/**
	 * @return the mMyStoreId
	 */
	public String getMyStoreId() {
		return mMyStoreId;
	}
	/**
	 * @param pMyStoreId the mMyStoreId to set
	 */
	public void setMyStoreId(String pMyStoreId) {
		this.mMyStoreId = pMyStoreId;
	}



	/**
	 * @return the repositoryExceOccurred
	 */
	public boolean isRepositoryExceOccurred() {
		return mRepositoryExceOccurred;
	}
	/**
	 * @param pRepositoryExceOccurred the repositoryExceOccurred to set
	 */
	public void setRepositoryExceOccurred(boolean pRepositoryExceOccurred) {
		mRepositoryExceOccurred = pRepositoryExceOccurred;
	}


	/**
	 * @return the geoCodeResultsEmpty
	 */
	public boolean isGeoCodeResultsEmpty() {
		return mGeoCodeResultsEmpty;
	}
	/**
	 * @param pGeoCodeResultsEmpty the geoCodeResultsEmpty to set
	 */
	public void setGeoCodeResultsEmpty(boolean pGeoCodeResultsEmpty) {
		mGeoCodeResultsEmpty = pGeoCodeResultsEmpty;
	}
	/**
	 * @return the geoCodeExceptionOccurred
	 */
	public boolean isGeoCodeExceptionOccurred() {
		return mGeoCodeExceptionOccurred;
	}
	/**
	 * @param pGeoCodeExceptionOccurred the geoCodeExceptionOccurred to set
	 */
	public void setGeoCodeExceptionOccurred(boolean pGeoCodeExceptionOccurred) {
		mGeoCodeExceptionOccurred = pGeoCodeExceptionOccurred;
	}
	/**
	 * Gets the SkuId
	 * @return mSkuId
	 */
	public String getSkuId() {
		return mSkuId;
	}
	/**
	 * Sets the SkuId.
	 * @param pSkuId - SkuId
	 */
	public void setSkuId(String pSkuId) {
		mSkuId = pSkuId;
	}

	/**
	 * Gets the json array object.
	 *
	 * @return mJsonArrayObject
	 */
	public String getJsonArrayObject() {
		return mJsonArrayObject;
	}

	/**
	 * Sets the json array object.
	 *
	 * @param pJsonArrayObject - JsonArrayObject to set
	 */
	public void setJsonArrayObject(String pJsonArrayObject) {
		this.mJsonArrayObject = pJsonArrayObject;
	}
	/**
	 * @return the locationManager
	 */
	public PARALStoreLocatorManager getLocationManager() {
		return mLocationManager;
	}
	/**
	 * @param pLocationManager
	 *            the locationManager to set
	 */
	public void setLocationManager(PARALStoreLocatorManager pLocationManager) {
		this.mLocationManager = pLocationManager;
	}

	/**
	 * @param pSessBean to set the Session bean
	 */
	public void setSessBean(TRUSessionBean pSessBean) {
		this.mSessBean = pSessBean;
	}

	/**
	 * @return mSessBean
	 */
	public TRUSessionBean getSessBean() {
		return mSessBean;
	}

	
	/**
	 * @return the mRestService
	 */
	public boolean isRestService() {
		return mRestService;
	}
	/**
	 * @param pRestService the mRestService to set
	 */
	public void setRestService(boolean pRestService) {
		this.mRestService = pRestService;
	}
	
	/**
	 * @return the mItemQuantity
	 */
	public String getItemQuantity() {
		return mItemQuantity;
	}
	/**
	 * @param pItemQuantity the mItemQuantity to set
	 */
	public void setItemQuantity(String pItemQuantity) {
		this.mItemQuantity = pItemQuantity;
	}
	/**
	 * This method used to validate and set postal address to address.
	 * This method converts distance from Miles to meters or kilometers to meters.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean value
	 * @throws ServletException - throws ServletException
	 * @throws IOException - throws IOException
	 */
	public boolean preLocateItems(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("PARALGeoLocatorFormHandler :preLocateItems : Starts");
		}
		boolean isValidData = true;
		resetFormExceptions();
		validatePostalAddressField(getPostalAddress(), getCity(), getState(), getAddress());
		if (isRestService() && getFormError()) {
			return checkFormRedirect(null, getErrorURLCustom(), pRequest, pResponse);
		} else if (getFormError()) {
			return checkFormRedirect(getSuccessURL(), getErrorURL(), pRequest, pResponse);
		}
		//If the distance is in miles, converting from miles to meters
		if(isDistanceinMiles()){
			convertMilesToMeters();
		} else {
			//If isDistanceinMiles is false, converting from kilometers to meters
			convertKmsToMeters();
		}
		//set the input postalAddress value to postalCode and address.
		if ((getGeoLocatorService().isAllowFreeFormEntry() || getGeoLocatorService().isAllowCityAndState()) &&
				(getPostalAddress() != null && !getPostalAddress().isEmpty())) {
				setPostalCode(getPostalAddress());
				setAddress(getPostalAddress());
		}
		if (isLoggingDebug()) {
			logDebug("PARALGeoLocatorFormHandler :preLocateItems : Ends");
		}
		return isValidData;
	}

	/**
	 * This method is used to convert distance from Miles to Meters.
	 */
	private void convertMilesToMeters() {
		if (isLoggingDebug()) {
			logDebug("PARALGeoLocatorFormHandler :convertMilesToMeters : Starts");
		}
		double distance =getDistance();
		if (isLoggingDebug()) {
			logDebug("Distance in Miles: "+distance);
		}
		double kms = distance * StoreLocatorConstants.MILES_TO_KMS_CONVERTER;
		double meters = kms * StoreLocatorConstants.KMS_TO_METER_CONVERTER;
		if (isLoggingDebug()) {
			logDebug("Distance in Meters: "+meters);
		}
		setDistance(meters);
		if (isLoggingDebug()) {
			logDebug("PARALGeoLocatorFormHandler :convertMilesToMeters : Ends");
		}
	}

	/**
	 * This method is used to convert distance from kiloMeters to Meters.
	 */
	private void convertKmsToMeters() {
		if (isLoggingDebug()) {
			logDebug("PARALGeoLocatorFormHandler :convertKmsToMeters : Starts");
		}
		double distanceinkms =getDistance();
		if (isLoggingDebug()) {
			logDebug("Distance in KiloMeters: "+distanceinkms);
		}
		double meters = distanceinkms * StoreLocatorConstants.KMS_TO_METER_CONVERTER;
		if (isLoggingDebug()) {
			logDebug("Distance in Meters: "+meters);
		}
		setDistance(meters);
		if (isLoggingDebug()) {
			logDebug("PARALGeoLocatorFormHandler :convertKmsToMeters : Ends");
		}
	}

	/**
	 * This method used to fetch the stores for the given postalAddress and distance.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException - throws ServletException
	 * @throws IOException - throws IOException
	 * @return boolean returns boolean value
	 */
	@Override
	public boolean handleLocateItems(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingInfo()) {
			logInfo("Start : PARALGeoLocatorFormHandler.handleLocateItems(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse)");
		}
		JSONObject responseJSON = new JSONObject();
		preLocateItems(pRequest, pResponse);
		if(!getFormError()) {
			//super.handleLocateItems(pRequest, pResponse);
			customHandleLocateItems(pRequest, pResponse);
			if(!getFormError()) {
				setCurrentDate(new Date().toString());
				//set the GeocoderStatus to AddressStatus
				if(((DefaultGeoLocatorProvider)getGeoLocatorService().getProvider()).getGeocoderStatus()!= null){
					mAddressStatus  = ((DefaultGeoLocatorProvider)getGeoLocatorService().getProvider()).getGeocoderStatus().toString();
				}
				if (isLoggingDebug()) {
					logDebug("handleLocateItems(): Address Status: : " + mAddressStatus);
				}

				if(isRequestFromPDP()){
					if(isGeoCodeExceptionOccurred() || isRepositoryExceOccurred()){
						setErrorMsgJsonToResponse(pRequest, pResponse, getLocationManager().getLocationTools().getStoreLocatoreGeoCodeExceErrorMsgKey());
						return false;
					}
					if(isGeoCodeResultsEmpty()){
						setErrorMsgJsonToResponse(pRequest, pResponse, getLocationManager().getLocationTools().getStoreLocatoreGeoCodeEmptyResultsErrorMsgKey());
						return false;
					}
				}

				//If the AddressStatus is not 'OK' or if the status is 'ZERO_RESULTS', set the location results to null.
				if(!StoreLocatorConstants.GEOCODERSTATUS_OK.equalsIgnoreCase(getAddressStatus()) || StoreLocatorConstants.GEOCODERSTATUS_ZERO_RESULTS.equalsIgnoreCase(getAddressStatus())){
					if(isLoggingInfo()){
						vlogInfo("The GeoCode stats is NOT OK for the postalAddress : {0} , distance : {1} , skuId : {2} .Hence JSON response will be empty", getPostalAddress(),getDistance(),getSkuId());
					}
					setLocationResults(null);
				}
				if(getLocationResults() != null && !(getLocationResults().isEmpty())){

					 getLocationResults().toArray();

					 JSONArray stores = null;


					 Collection<RepositoryItem> locationItems = getLocationResults();

					 if(getLocationManager()!=null)
					 {
						 locationItems =	 getLocationManager().getLocationTools().getEligibleStoreItems(locationItems);
						 if(locationItems != null && !locationItems.isEmpty()){
							 setLocationResults((ArrayList) locationItems);
						 }
					}
					if (isRestService() && StringUtils.isNotEmpty(getItemQuantity())) {
						getLocationManager().getLocationTools().setItemQuantity(getItemQuantity());
					}
					// removeExpiredStore(locationItems);

						if(locationItems != null && !locationItems.isEmpty()){

							if(StringUtils.isBlank(getSkuId())){

								stores = getLocationManager().generateJSONResponse(locationItems, getDistance(),getMyStoreId());
							}else{

								stores = getLocationManager().generateJSONResponse(locationItems, getDistance(),getSkuId(), getMyStoreId());
							}

							try {
								responseJSON.put(StoreLocatorConstants.STORES,stores);
								/*if(pRequest.getContextPath().startsWith(StoreLocatorConstants.REST_CONTEXT)){
									responseJSON.put(StoreLocatorConstants.STORES_COUNT, stores.size());
								}*/

							} catch (JSONException jsonExce) {
								if(isLoggingError()){
									vlogError(jsonExce,"JSON exception occurred while setting the data to the responseJson object");
								}
							}
						}else{
							//Adding error message to the Json object saying that no store results found for the given address
							if(isLoggingInfo()){
								vlogInfo("The Store Results are EMPTY or NULL for the given postalAddress : {0}  , distance : {1} and skuId : {2}", getPostalAddress(),getDistance(),getSkuId());
							}
							if(isRequestFromPDP() || pRequest.getContextPath().startsWith(StoreLocatorConstants.REST_CONTEXT)){
								setErrorMsgJsonToResponse(pRequest, pResponse, getLocationManager().getLocationTools().getStoreLocatoreGeoCodeEmptyRepoResults());
								return false;
							}
						}
					}else{
						//Adding error message to the Json object saying that no store results found for the given address
						if(isLoggingInfo()){
							vlogInfo("The Store Results are EMPTY or NULL for the given postalAddress : {0}  , distance : {1} and skuId : {2}", getPostalAddress(),getDistance(),getSkuId());
						}
						if(isRequestFromPDP() || pRequest.getContextPath().startsWith(StoreLocatorConstants.REST_CONTEXT)){
							setErrorMsgJsonToResponse(pRequest, pResponse, getLocationManager().getLocationTools().getStoreLocatoreGeoCodeEmptyRepoResults());
							return false;
						}
					}
					 if (getSessBean() != null) {
						 getSessBean().setJsonArrayObject(responseJSON.toString());
					 }
					 if(isRequestFromPDP() || pRequest.getContextPath().startsWith(StoreLocatorConstants.REST_CONTEXT)){
						 //Add the json data to the Dynamo response object
						 addJsonDataToResponseObject(pRequest,pResponse,responseJSON);
						 //There is no redirect or forward.The json response is already added to the Dynamo response object
						 return false;
					 }
			}
		}

		if(!getFormError() && (getLocationResults() == null || getLocationResults().isEmpty()) && isRequestFromPDP()){
			//Populate empty json to the response object
			 //Add the json data to the Dynamo response object
			 addJsonDataToResponseObject(pRequest,pResponse,new JSONObject());
			 return false;
		}
		if (isLoggingInfo()) {
			logInfo("End : PARALGeoLocatorFormHandler.handleLocateItems(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse)");
		}
		if (isRestService()) {
			return checkFormRedirect(getSuccessURLCustom(), getErrorURLCustom(), pRequest, pResponse);	
		} else {
			return checkFormRedirect(getSuccessURL(), getErrorURL(), pRequest, pResponse);
		}
	}


	/**
	 * This method used to get the list of page numbers.
	 * @return List<Integer> page number list.
	 */
	public List<Integer> getPageNumbers() {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: PARALGeoLocatorFormHandler.getPageNumbers method..");
		}
		List<Integer> pageNumbers = null;
		if (isEnableCountQuery() && getMaxResultsPerPage() > 0) {
			pageNumbers = new ArrayList<Integer>();
			for (int i = 1; i <= getResultPageCount(); ++i) {
				pageNumbers.add(i);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: PARALGeoLocatorFormHandler.getPageNumbers method.. "
					+ pageNumbers);
		}
		return pageNumbers;
	}

	/**
	 * This method used to validate the postalAddress
	 * @param pPostalAddress String
	 * @param pState String
	 * @param pCity String
	 * @param pCountry String
	 */
	private void validatePostalAddressField(String pPostalAddress, String pState, String pCity, String pCountry) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: PARALGeoLocatorFormHandler.validatePostalAddressField method..");
		}
		// Added State, City and Country checks for CSC implementation
			if ((pPostalAddress == null || StringUtils.isEmpty(pPostalAddress)) && (pState == null || StringUtils.isEmpty(pState)) && (pCity == null || StringUtils.isEmpty(pCity)) && (pCountry == null && StringUtils.isEmpty(pCountry))){
				addFormException(new DropletException(StoreLocatorConstants.VALID_ZIPCODE,StoreLocatorConstants.EMPTY_ZIPCODE));
			}
	}

	/**
	 * @return the postalAddress
	 */
	public String getPostalAddress() {
		return mPostalAddress;
	}

	/**
	 * @param pPostalAddress
	 *            the postalAddress to set
	 */
	public void setPostalAddress(String pPostalAddress) {
		this.mPostalAddress = pPostalAddress;
	}

	/**
	 * @return the addressStatus
	 */
	public String getAddressStatus() {
		return mAddressStatus;
	}

	/**
	 * @param pAddressStatus
	 *            the addressStatus to set
	 */
	public void setAddressStatus(String pAddressStatus) {
		this.mAddressStatus = pAddressStatus;
	}

	/**
	 * @return the currentDate
	 */
	public String getCurrentDate() {
		return mCurrentDate;
	}

	/**
	 * @param pCurrentDate
	 *            the currentDate to set
	 */
	public void setCurrentDate(String pCurrentDate) {
		this.mCurrentDate = pCurrentDate;
	}
	/**
	 * @return the geocoderStatus
	 */
	public GeocoderStatus getGeocoderStatus() {
		return mGeocoderStatus;
	}
	/**
	 * @param pGeocoderStatus
	 *            the geocoderStatus to set
	 */
	public void setGeocoderStatus(GeocoderStatus pGeocoderStatus) {
		this.mGeocoderStatus = pGeocoderStatus;
	}

	/**
	 * @return the distanceinMiles
	 */
	public boolean isDistanceinMiles() {
		if(mGeoConfigurations != null){
			return mGeoConfigurations.isDistanceinMiles();
		} else{
			return mDistanceinMiles;
		}
	}

	/**
	 * @param pDistanceinMiles the distanceinMiles to set
	 */
	public void setDistanceinMiles(boolean pDistanceinMiles) {
		mDistanceinMiles = pDistanceinMiles;
	}

	/**
	 * @return the geoConfigurations
	 */
	public GeoLocatorConfigurations getGeoConfigurations() {
		return mGeoConfigurations;
	}
	/**
	 * @param pGeoConfigurations the geoConfigurations to set
	 */
	public void setGeoConfigurations(GeoLocatorConfigurations pGeoConfigurations) {
		mGeoConfigurations = pGeoConfigurations;
	}

	/**
	 * This method is used to set the json data to the response object
	 *
	 * @param pRequest - DynamoHttpServletRequest
	 * @param pResponse - DynamoHttpServletResponse
	 * @param pJSONResponse - JSONObject
	 * @throws ServletException - throws ServletException
	 * @throws IOException - throws IOException
	 */
	public void addJsonDataToResponseObject(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse,JSONObject pJSONResponse) throws ServletException, IOException{
		if (isLoggingDebug()) {
			logDebug("Start : PARALGeoLocatorFormHandler (addJsonDataToResponseObject)");
		}
		if(pRequest == null){
			return ;
		}
		if(pResponse == null){
			return ;
		}
		if(pJSONResponse == null){
			return ;
		}
		pResponse.setContentType(StoreLocatorConstants.JSON_CONTENT_TYPE);
		PrintWriter out;
		out = pResponse.getWriter();
		out.print(pJSONResponse.toString());
		out.flush();
		if (isLoggingDebug()) {
			logDebug("End : PARALGeoLocatorFormHandler (addJsonDataToResponseObject)");
		}
		return ;
	}

	/**
	 * Holds the mRequestFromPDP property
	 */
	private boolean mRequestFromPDP;

	/**
	 * @return the mRequestFromPDP
	 */
	public boolean isRequestFromPDP() {
		return mRequestFromPDP;
	}

	/**
	 * @param pRequestFromPDP - The mRequestFromPDP to set
	 */
	public void setRequestFromPDP(boolean pRequestFromPDP) {
		mRequestFromPDP = pRequestFromPDP;
	}

	/**
	 * This method is used to get the siteids
	 * @return siteIds - The site ids
	 */
	  @Override
	public Collection<String> getSiteIds()
	  {
		Collection<String> siteIds = super.getSiteIds();
		if(siteIds == null || siteIds.isEmpty()){
			List<String> siteIdList = new  ArrayList<String>();
			siteIdList.add(getLocationManager().getLocationTools().getSiteIdsList().get(StoreLocatorConstants.INT_ZERO));
			siteIdList.add(getLocationManager().getLocationTools().getSiteIdsList().get(StoreLocatorConstants.INT_ONE));
			return siteIdList;
		}
	    return siteIds;
	  }

	  /**
	    * This method is used to call the below things
	    * a)Call the GeoCode service
	    * b)Query the Location repository for Store Results
	    * C)If any GeoCode exception occurred then we are adding that error msg to the Json
	    * @param pRequest DynamoHttpServletRequest
	    * @param pResponse DynamoHttpServletResponse
	    * @throws ServletException - throws ServletException
	    * @throws IOException - throws IOException
	    * @return boolean returns boolean value
	  */
	  public boolean customHandleLocateItems(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			    throws ServletException, IOException
	  {
		if (isLoggingDebug()) {
			logDebug("Start : PARALGeoLocatorFormHandler.customHandleLocateItems(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)");
		}
	    double distance = getDistance();
	    if (getUnitOfMeasureTools() != null) {
	      distance = getUnitOfMeasureTools().convertValue(distance, getUnitOfMeasureKey());

	    }

	    if (((distance > getMaximumDistance()) && (getMaximumDistance() >= StoreLocatorConstants.DOUBLE_ZERO_FORMAT)) || ((distance <= StoreLocatorConstants.DOUBLE_MINUS_ONE) && (getMaximumDistance() >= StoreLocatorConstants.DOUBLE_ZERO_FORMAT))) {
	      distance = getMaximumDistance();
	    }

	    Collection siteIds = generateSiteFilter();

	    try
	    {
	      Coordinate coordinate = getLocation(pRequest);
	      TRUCoordinate truCOOrdinate = null;
	      //Add the errors to the StoreLocatorFormHandler request object
	      if(coordinate instanceof TRUCoordinate){
	    	  truCOOrdinate = (TRUCoordinate)coordinate;
	    	  if(truCOOrdinate.isGeoCodeExceptionOccurred()){
	    		  setGeoCodeExceptionOccurred(true);
	    	  }
	    	  if(truCOOrdinate.isGeoCodeResultsEmpty()){
	    		  setGeoCodeResultsEmpty(true);
	    	  }
	    	  if(isRequestFromPDP() && (isGeoCodeExceptionOccurred() || isGeoCodeResultsEmpty()) ){
	    		  //If there is any GeoCode error or GeoCode results empty then don't call the LocationRepository
	    		  //Add error to the JSON object

	    		  return false;
	    	  }
	      }
	      ArrayList items = getItems(coordinate, distance, siteIds);


	      this.mStartIndex = StoreLocatorConstants.INT_ZERO;
	      this.mEndIndex = StoreLocatorConstants.INT_ZERO;
	      setResultSetSize(StoreLocatorConstants.INT_ZERO);
	      setLocationResults(null);
	      setCurrentResultPageNum(StoreLocatorConstants.INT_ONE);


	      setLocationResults(items);
	      if (items != null) {
	        setResultSetSize(items.size());
	      }

	      if (isEnableCountQuery())
	      {
	        this.mStartIndex = ((getCurrentResultPageNum() - StoreLocatorConstants.INT_ONE) * getMaxResultsPerPage());
	        this.mEndIndex = (this.mStartIndex + getMaxResultsPerPage());

	        if (this.mEndIndex > getResultSetSize()){
	        	this.mEndIndex = getResultSetSize();
	        }
	      }
	    }
	    catch (RepositoryException e)
	    {
	    	if(isLoggingError()){
	    		vlogError(e,"RepositoryException occurred while calling the getItems(coordinate, distance, siteIds) for the given postalAddrss : {0} ,distance : {1} and skuId : {2}",getPostalAddress(),getDistance(),getSkuId());
	    	}
	    	if(isRequestFromPDP()){
		    	setRepositoryExceOccurred(true);
		    	return false ;
	    	}
	    	throw new ServletException(e);
	    }
	    if (isLoggingDebug()) {
			logDebug("End : PARALGeoLocatorFormHandler.customHandleLocateItems(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)");
		}
	    return checkFormRedirect(getSuccessURL(), getErrorURL(), pRequest, pResponse);
	  }

	  /**
	   * This method is used to add the error json object to the Dynamo Response object
	   *  @param pRequest DynamoHttpServletRequest
	   *  @param pResponse DynamoHttpServletResponse
	   *  @param pKey - The input key
	 */
	  public void setErrorMsgJsonToResponse(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse,String pKey){
		    if(StringUtils.isBlank(pKey)){
		    	return ;
		    }
		  	//Get the error message
			String errorMsg = getLocationManager().getLocationTools().getMessageValue(pKey);

			//Add error messages to the Json object.If geocode exception occurred or geocode results are empty
			JSONArray jsonArray = new JSONArray();
			try {
				JSONObject jsonObj = new JSONObject();
				jsonObj.put(StoreLocatorConstants.ERROR_MESSAGE, errorMsg);
				jsonArray.add(jsonObj);
			} catch (JSONException e) {
				if(isLoggingError()){
					vlogError(e,"JSONException occurred while accessing the jsonObj.put().For the key : {0}",pKey);
				}
			}
			JSONObject errJsonObj = new JSONObject();
			try {
				errJsonObj.put(StoreLocatorConstants.ERROR_SRING, jsonArray);
			} catch (JSONException e1) {
				if(isLoggingError()){
					vlogError(e1,"JSONException occurred while accessing the jsonObj.put() ");
				}
			}
			try {
				addJsonDataToResponseObject(pRequest, pResponse, errJsonObj);
			} catch (ServletException e) {
				if(isLoggingError()){
					vlogError(e,"ServletException occurred while calling the addJsonDataToResponseObject(pRequest, pResponse, errJsonObj) for the given key : {0}",pKey);
				}
			} catch (IOException e) {
				if(isLoggingError()){
					vlogError(e,"IOException occurred while calling the addJsonDataToResponseObject(pRequest, pResponse, errJsonObj) for the key : {0}",pKey);
				}
			}
	  }
}

