package com.tru.storelocator.fhl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

import javax.servlet.ServletException;

import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.storelocator.cml.StoreLocatorConstants;
import com.tru.storelocator.mgl.PARALStoreLocatorManager;

/**
 * This class is used to get the list of json stores map based on distance and locationResults.
 * The stores fetched from the repository are converted to an array of JSON objects and 
 * sets the JSONArray as output param.
 *
 * @author PA
 *
 */
public class GenerateJSONResponseDroplet extends DynamoServlet {
	/**
	 * holds the mLocationManager.
	 */
	private PARALStoreLocatorManager mLocationManager;
		
	/**
	 * This method used to get the stores JSONArray by passing distance and locationResults as a parameters.
	 * This method gets storeItems and distance as input params.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException 
	 * @throws IOException 
	 */
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN::GenerateJSONResponseDroplet.service");
		}
		Collection<RepositoryItem> locationItems = (Collection<RepositoryItem>) pRequest.getObjectParameter(StoreLocatorConstants.LOCATION_RESULTS);
		Double distance = (Double) pRequest.getLocalParameter(StoreLocatorConstants.DISTANCE);
		String myStoreId = (String) pRequest.getLocalParameter(StoreLocatorConstants.MYSTORE_ID);
		if(locationItems != null && !locationItems.isEmpty()){
			JSONArray locationList = getLocationManager().generateJSONResponse(locationItems, distance,myStoreId);
			if(locationList == null){
				populateStoreNotFoundResponse(pResponse);
				pRequest.serviceLocalParameter(StoreLocatorConstants.EMPTY_PARAM, pRequest, pResponse);
			} else{
				pRequest.setParameter(StoreLocatorConstants.LOCATION_ARRAY_LIST, locationList);
				pRequest.serviceLocalParameter(StoreLocatorConstants.OUTPUT, pRequest, pResponse);
			}
			
		} else {
			populateStoreNotFoundResponse(pResponse);
			pRequest.serviceLocalParameter(StoreLocatorConstants.EMPTY_PARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("End::GenerateJSONResponseDroplet.service");
		}
					
	}
	
	/**
	 * 
	 * The method is invoked when the responseJSON object is to be populated
	 * with the error message stating that the stores are not available for the
	 * user entered address
	 * 
	 * @param pResponse DynamoHttpServletResponse
	 * @throws IOException
	 *             -IOException if any
	 * 
	 */
	private void populateStoreNotFoundResponse(DynamoHttpServletResponse pResponse)
			throws IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN::GenerateJSONResponseDroplet.populateStoreNotFoundResponse");
			logDebug("Populate the JSONResponse with success as False and the error code");
		}
		JSONObject responseJson = new JSONObject();
		try{
			responseJson.put(StoreLocatorConstants.IS_SUCESS, Boolean.FALSE);
			responseJson.put(StoreLocatorConstants.ERROR,
					StoreLocatorConstants.NOT_FOUND_ANY_RESULTS);
			pResponse.setContentType(StoreLocatorConstants.APPLICATIONORJSON);
		}catch (JSONException jsonException) {
			if (isLoggingError()) {
				logError("JSONException in GenerateJSONResponseDroplet",jsonException);
			}
		}
		PrintWriter out = pResponse.getWriter();
		out.print(responseJson.toString());
		out.flush();
		if (isLoggingDebug()) {
			logDebug("END::GenerateJSONResponseDroplet.populateStoreNotFoundResponse");
		}
	}

	/**
	 * @return the locationManager
	 */
	public PARALStoreLocatorManager getLocationManager() {
		return mLocationManager;
	}
	/**
	 * @param pLocationManager
	 *            the locationManager to set
	 */
	public void setLocationManager(PARALStoreLocatorManager pLocationManager) {
		this.mLocationManager = pLocationManager;
	}
}
