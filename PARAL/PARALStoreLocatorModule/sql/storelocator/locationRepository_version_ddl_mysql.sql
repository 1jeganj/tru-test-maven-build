--drop table STORE_LOCATOR_EXTENSIONS;
--drop table STORE_LOCATOR_MEDIA;
--drop table STORE_TRADING_HOURS;


CREATE TABLE STORE_LOCATOR_EXTENSIONS (
            location_id               VARCHAR(40)  NOT NULL,
            store_thumbnail_image_id	varchar(40)	null,
            SEQUENCE_NUMBER        DECIMAL(38)       NOT NULL,
            TRADING_HOUR_ID        VARCHAR(40)  NULL,
			ASSET_VERSION				DECIMAL(19,0)	NOT NULL,
			PRIMARY KEY(location_id,SEQUENCE_NUMBER,ASSET_VERSION)
);

CREATE TABLE STORE_LOCATOR_MEDIA (
            location_id                    VARCHAR(40)   NOT NULL,
            store_thumbnail_image_id	varchar(40)	null,
            ASSET_VERSION				DECIMAL(19,0)	NOT NULL,
	    	CONSTRAINT STORE_LOCATOR_MEDIA_PK PRIMARY KEY(location_id,ASSET_VERSION)
);

CREATE TABLE STORE_TRADING_HOURS (
			ASSET_VERSION 		decimal(38) 		not null,
			workspace_id 		varchar(254) 	null,
			branch_id 			varchar(254) 	null,
			is_head 			INT 		null,
			version_deleted 	INT 		null,
			version_editable 	INT 		null,
			pred_version 		decimal(38) 		null,
			checkin_date		datetime			null,
            ID                              VARCHAR(40)  NOT NULL,
            DAY                             DECIMAL(38)       NOT NULL,
            OPENING_HOURS                   VARCHAR(254)  NOT NULL,
            CLOSING_HOURS       	    VARCHAR(254)  NOT NULL,
            ADDITIONAL_INFO           	    VARCHAR(254)  NULL,
            CONSTRAINT STORE_TRADING_HOURS_PK PRIMARY KEY(ID,ASSET_VERSION)
);