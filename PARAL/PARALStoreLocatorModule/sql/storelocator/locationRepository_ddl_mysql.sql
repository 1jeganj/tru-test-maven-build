CREATE TABLE STORE_LOCATOR_EXTENSIONS (
            location_id               VARCHAR(40)  NOT NULL,
            SEQUENCE_NUMBER        DECIMAL(38)       NOT NULL,
            TRADING_HOUR_ID        VARCHAR(40)  NULL REFERENCES STORE_TRADING_HOURS(ID),
	    CONSTRAINT STORE_WKCAL_STORE_ID_FK FOREIGN KEY(location_id) REFERENCES dcs_location(location_id),
            CONSTRAINT STORE_WKCAL_PK PRIMARY KEY(location_id, SEQUENCE_NUMBER)
);

CREATE TABLE STORE_LOCATOR_MEDIA (
            location_id                       VARCHAR(40)   NOT NULL,
            store_thumbnail_image_id	varchar(40)	null,
	    	CONSTRAINT STORE_LOCATOR_MEDIA_FK FOREIGN KEY(location_id) REFERENCES dcs_location(location_id),
            CONSTRAINT STORE_LOCATOR_MEDIA_PK PRIMARY KEY(location_id)
);

CREATE TABLE STORE_TRADING_HOURS (
            ID                              VARCHAR(40)  NOT NULL,
            DAY                             DECIMAL(38)       NOT NULL,
            OPENING_HOURS                   VARCHAR(254)  NOT NULL,
            CLOSING_HOURS       	    VARCHAR(254)  NOT NULL,
            ADDITIONAL_INFO           	    VARCHAR(254)  NULL,
            CONSTRAINT STORE_TRADING_HOURS_PK PRIMARY KEY(ID)
);