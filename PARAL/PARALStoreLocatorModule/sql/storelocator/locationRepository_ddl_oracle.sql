CREATE TABLE STORE_LOCATOR_EXTENSIONS (
            location_id               VARCHAR2(40)  NOT NULL,
            SEQUENCE_NUMBER        INTEGER       NOT NULL,
            TRADING_HOUR_ID        VARCHAR2(40)  NULL REFERENCES STORE_TRADING_HOURS(ID),
	    CONSTRAINT STORE_WKCAL_STORE_ID_FK FOREIGN KEY(location_id) REFERENCES dcs_location(location_id),
            CONSTRAINT STORE_WKCAL_PK PRIMARY KEY(location_id, SEQUENCE_NUMBER)
);

CREATE TABLE STORE_LOCATOR_MEDIA (
            location_id                       VARCHAR2(40)   NOT NULL,
            store_thumbnail_image_id	varchar2(40)	null,
	    	CONSTRAINT STORE_LOCATOR_MEDIA_FK FOREIGN KEY(location_id) REFERENCES dcs_location(location_id),
            CONSTRAINT STORE_LOCATOR_MEDIA_PK PRIMARY KEY(location_id)
);

CREATE TABLE STORE_TRADING_HOURS (
            ID                              VARCHAR2(40)  NOT NULL,
            DAY                             INTEGER       NOT NULL,
            OPENING_HOURS                   VARCHAR2(254)  NOT NULL,
            CLOSING_HOURS       	    VARCHAR2(254)  NOT NULL,
            ADDITIONAL_INFO           	    VARCHAR2(254)  NULL,
            CONSTRAINT STORE_TRADING_HOURS_PK PRIMARY KEY(ID)
);