CREATE TABLE STORE_LOCATOR_EXTENSIONS (
            location_id               VARCHAR2(40)  NOT NULL,
            store_thumbnail_image_id	varchar2(40)	null,
            SEQUENCE_NUMBER        INTEGER       NOT NULL,
            TRADING_HOUR_ID        VARCHAR2(40)  NULL,
			ASSET_VERSION				NUMBER(19,0)	NOT NULL,
			PRIMARY KEY(location_id,SEQUENCE_NUMBER,ASSET_VERSION)
);

CREATE TABLE STORE_LOCATOR_MEDIA (
            location_id                    VARCHAR2(40)   NOT NULL,
            store_thumbnail_image_id	varchar2(40)	null,
            ASSET_VERSION				NUMBER(19,0)	NOT NULL,
	    	CONSTRAINT STORE_LOCATOR_MEDIA_PK PRIMARY KEY(location_id,ASSET_VERSION)
);

CREATE TABLE STORE_TRADING_HOURS (
			ASSET_VERSION 		integer 		not null,
			workspace_id 		varchar2(254) 	null,
			branch_id 			varchar2(254) 	null,
			is_head 			number(1) 		null,
			version_deleted 	number(1) 		null,
			version_editable 	number(1) 		null,
			pred_version 		integer 		null,
			checkin_date		date			null,
            ID                              VARCHAR2(40)  NOT NULL,
            DAY                             INTEGER       NOT NULL,
            OPENING_HOURS                   VARCHAR2(254)  NOT NULL,
            CLOSING_HOURS       	    VARCHAR2(254)  NOT NULL,
            ADDITIONAL_INFO           	    VARCHAR2(254)  NULL,
            CONSTRAINT STORE_TRADING_HOURS_PK PRIMARY KEY(ID,ASSET_VERSION)
);