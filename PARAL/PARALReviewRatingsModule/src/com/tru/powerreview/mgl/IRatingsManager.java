package com.tru.powerreview.mgl;

import java.util.Map;

import atg.commerce.order.Order;
import atg.json.JSONObject;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.Profile;

import com.tru.common.cml.SystemException;

/**
 * This interface has methods that interact with RatingsRepositoryTools class.
 *
 */
public interface IRatingsManager {

	/**
	 * This method is used to call the tools class getRatingsForProduct method to get
	 * the average rating and reviews count for the given product id.
	 * @param pProductId - the product id.
	 * @return Map.
	 * @throws SystemException - when error occurs.
	 */
	Map<String,String> getRatingsForProduct(String pProductId) throws SystemException;
	/**
	 * This method is used to construct JSONArray for PowerReview Post Interaction Email.
	 * 
	 * @param pRequest --  DynamoHttpServletRequest
	 * @param pOrder --  Order instance
	 * @param pProfile --  Profile instance
	 * @return JSONObject
	 * @throws SystemException --  throws SystemException when any exception occured
	 */
	JSONObject constructPIEJSONArray(DynamoHttpServletRequest pRequest,Order pOrder,Profile pProfile) throws SystemException;

}
