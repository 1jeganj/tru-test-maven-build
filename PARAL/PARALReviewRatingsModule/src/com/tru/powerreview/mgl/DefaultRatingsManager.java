package com.tru.powerreview.mgl;

import java.util.Map;

import atg.commerce.order.Order;
import atg.core.util.StringUtils;
import atg.json.JSONObject;
import atg.nucleus.GenericService;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.Profile;

import com.tru.common.cml.SystemException;
import com.tru.powerreview.tol.IRatingsRepositoryTools;

/**
 * This Class is used to interact with RepositoryTools Class.
 *
 */
public class DefaultRatingsManager extends GenericService implements
		IRatingsManager {

	/**
	 * Property to hold mRepositoryTools.
	 */
	private IRatingsRepositoryTools mRepositoryTools;
	/**
	 * Getter for RepositoryTools.
	 * @return mRepositoryTools - the RepositoryTools.
	 */
	public IRatingsRepositoryTools getRepositoryTools() {
		return mRepositoryTools;
	}

	/**
	 * Setter for RepositoryTools.
	 * @param pRepositoryTools - the RepositoryTools.
	 */
	public void setRepositoryTools(IRatingsRepositoryTools pRepositoryTools) {
		mRepositoryTools = pRepositoryTools;
	}
	/**
	 * This method is used to call the tools class getRatingsForProduct method to get
	 * the average rating and reviews count for the given product id.
	 * @param pProductId - the product id.
	 * @return Map.
	 * @throws SystemException - when error occured.
	 */
	public Map<String, String> getRatingsForProduct(String pProductId)
			throws SystemException {
		if(isLoggingDebug()){
			logDebug("DefaultRatingsManager.getRatingsForProduct(String) :: Starts");
		}
		Map<String,String> ratingsReviewMap=null;
		if(!StringUtils.isBlank(pProductId)){
			ratingsReviewMap = getRepositoryTools().getRatingsForProduct(pProductId);
		}
		
		if(isLoggingDebug()){
			logDebug("DefaultRatingsManager.getRatingsForProduct(String) :: Ends");
		}
		return ratingsReviewMap;
	}
	
	/**
	 * This method calls constructPIEJSONArray() method of  RatingsRepositoryTools class to construct Json Object
	 * 
	 * @param pRequest --  DynamoHttpServletRequest
	 * @param pOrder -- Order instance
	 * @param pProfile --  Profile instance
	 * @return JSONObject
	 * @throws SystemException  -- when exception occures throws SystemException
	 */
	public JSONObject constructPIEJSONArray(DynamoHttpServletRequest pRequest,Order pOrder,Profile pProfile) throws SystemException{
		if(isLoggingDebug()){
			logDebug("DefaultRatingsManager.constructPIEJSONArray(Order,Profile) :: Starts");
		}
		
		JSONObject jsonObj = getRepositoryTools().constructPIEJSONArray(pRequest,pOrder,pProfile);
		
		if(isLoggingDebug()){
			logDebug("DefaultRatingsManager.constructPIEJSONArray(Order,Profile) :: Ends");
		}
		return jsonObj;
	}
}
