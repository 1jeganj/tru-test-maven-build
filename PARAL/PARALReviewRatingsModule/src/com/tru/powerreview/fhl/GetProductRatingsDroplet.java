package com.tru.powerreview.fhl;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.cml.SystemException;
import com.tru.powerreview.cml.PowerReviewConstants;
import com.tru.powerreview.mgl.IRatingsManager;

/**
 * This class is used to get the average rating and reviews count for a given product id.
 *
 */
public class GetProductRatingsDroplet extends DynamoServlet {
	
	
	/**
	 * property to hold the RatingsManager.
	 */
	private IRatingsManager mRatingsManager;
	
	/**
	 * property to hold the mPropertiesMap.
	 */
	private	Map<String, String> mPropertiesMap;
	
	/**
	 * property to hold the PropertiesList.
	 */
	private List<String> mPropertiesList;
	
	/**
     * This method will get the average rating and reviews count for given product id.<br/>
     * 
     * Output parameter : <b>averageRating</b> -  holds average rating for given product id.<br/>
     * Output parameter : <b>reviewsCount</b> - holds reviews count for given product id.<br/>
     * Open Parameter : <b>output</b> - when average rating or/and reviews count is available.<br/>
     * Open Parameter : <b>empty</b> -  when no data available.
     *
     * @param pRequest - DynamoHttpServletRequest.
     * @param pResponse - DynamoHttpServletResponse.
     * @throws ServletException - when error occurs.
     * @throws IOException - when error occurs.
     */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if(isLoggingDebug()){
			logDebug("GetProductRatingsDroplet.service(DynamoHttpServletRequest, DynamoHttpServletResponse) :: Starts");
		}
		String productId = (String) pRequest.getLocalParameter(PowerReviewConstants.PRODUCT_ID_PARAM);
		if(isLoggingDebug()){
			logDebug("ProductId in GetProductRatingsDroplet.service() :: "+productId);
		}
		if(!StringUtils.isBlank(productId)){
			try {
				Map<String, String> ratingsReviewMap = getRatingsManager().getRatingsForProduct(productId);
				
				if(ratingsReviewMap != null && !ratingsReviewMap.isEmpty() && getPropertiesList() != null && getPropertiesMap() != null) {
					for(String property: getPropertiesList()){
						String propertyValue = (String) getPropertiesMap().get(property);
						if(propertyValue != null && ratingsReviewMap.containsKey(propertyValue)){
							pRequest.setParameter(property, ratingsReviewMap.get(propertyValue));
						}
					}
					pRequest.serviceLocalParameter(PowerReviewConstants.OUTPUT_OPEN_PARAMETER, pRequest, pResponse);
				} else{
					pRequest.serviceLocalParameter(PowerReviewConstants.EMPTY_OPEN_PARAMETER, pRequest, pResponse);
				}
			} catch (SystemException se) {
				if(isLoggingError()){
					logError("Error in GetRatingsAndReviewsDroplet.service(DynamoHttpServletRequest, DynamoHttpServletResponse)", se);
				}
			}
		}
		
		if(isLoggingDebug()){
			logDebug("GetProductRatingsDroplet.service(DynamoHttpServletRequest, DynamoHttpServletResponse) :: Ends");
		}
		
	}

	/**
	 * Getter for PowerReviewManager.
	 * @return mRatingsManager - the PowerReviewManager.
	 */
	public IRatingsManager getRatingsManager() {
		return mRatingsManager;
	}

	/**
	 * Setter for PowerReviewManager.
	 * @param pRatingsManager - the PowerReviewManager.
	 */
	public void setRatingsManager(IRatingsManager pRatingsManager) {
		mRatingsManager = pRatingsManager;
	}

	/**
	 * @return the propertiesMap
	 */
	public Map<String, String> getPropertiesMap() {
		return mPropertiesMap;
	}
	/**
	 * Gets the PropertiesList.
	 * 
	 * @return the propertiesList
	 */
	public List<String> getPropertiesList() {
		return mPropertiesList;
	}

	/**
	 * Sets the PropertiesList
	 * 
	 * @param pPropertiesList the propertiesList to set
	 */
	public void setPropertiesList(List<String> pPropertiesList) {
		this.mPropertiesList = pPropertiesList;
	}
	/**
	 * @param pPropertiesMap the propertiesMap to set
	 */
	public void setPropertiesMap(Map<String, String> pPropertiesMap) {
		mPropertiesMap = pPropertiesMap;
	}
}
