package com.tru.powerreview.fhl;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.powerreview.cml.PowerReviewConstants;
import com.tru.powerreview.cml.utils.PowerReviewUtils;

/**
 * This class is used to generate the User Authentication String for PowerReview.
 *
 */
public class UserTokenGenerateDroplet extends DynamoServlet {
	
	/**
	 * property to hold the PowerReviewUtils.
	 */
	private PowerReviewUtils mPowerReviewUtils;
	
	/**
     * This method will generate encrypted user token based on userId, current date and shared key.<br/>
     * 
     * Output parameter : <b>userToken</b> - holds user token.<br/>
     * Open Parameter : <b>output</b> - when user token is generated.
     *
     * @param pRequest - DynamoHttpServletRequest.
     * @param pResponse - DynamoHttpServletResponse.
     * @throws ServletException - when error occurs.
     * @throws IOException - when error occurs.
     */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if(isLoggingDebug()){
        	logDebug("UserTokenGenerateDroplet.service(DynamoHttpServletRequest,DynamoHttpServletResponse) :: Starts ");
		}
		String userToken=null;
		String profileId = (String) pRequest.getLocalParameter(PowerReviewConstants.USER_ID);
		if(!StringUtils.isBlank(profileId)){
			userToken=getPowerReviewUtils().generateUserAuthenticationString(profileId);
		}
		if(isLoggingDebug()){
        	logDebug("UserToken value in UserTokenGenerateDroplet.service() :: "+userToken);
		}
		if(!StringUtils.isBlank(userToken)){
			pRequest.setParameter(PowerReviewConstants.USER_TOKEN, userToken);
			pRequest.serviceLocalParameter(PowerReviewConstants.OUTPUT_OPEN_PARAMETER, pRequest, pResponse);
		}
		if(isLoggingDebug()){
        	logDebug("UserTokenGenerateDroplet.service(DynamoHttpServletRequest,DynamoHttpServletResponse) :: Ends ");
		}		
	}

	/**
	 * Getter for PowerReviewUtils
	 * @return mPowerReviewUtils.
	 */
	public PowerReviewUtils getPowerReviewUtils() {
		return mPowerReviewUtils;
	}

	/**
	 * Setter for PowerReviewUtils.
	 * @param pPowerReviewUtils - the PowerReviewUtils
	 */
	public void setPowerReviewUtils(PowerReviewUtils pPowerReviewUtils) {
		mPowerReviewUtils = pPowerReviewUtils;
	}

}
