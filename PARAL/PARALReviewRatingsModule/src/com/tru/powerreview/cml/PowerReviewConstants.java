package com.tru.powerreview.cml;

/**
 * This Class contains PowerReview constants.
 */
public class PowerReviewConstants {
	
	/**
	 * Constant to hold date format value.
	 */
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	
	/**
	 * Constant to hold date param value.
	 */
	public static final String DATE_PARAM = "date";
	
	/**
	 * Constant to hold max age param value.
	 */
	public static final String MAX_AGE_PARAM = "maxage";
	
	/**
	 * Constant to hold user param value.
	 */
	public static final String USER_PARAM = "userid";
	
	/** Constant for EQUAL_SIGN */
	public static final String EQUAL_SIGN = "=";
	
	/** Constant for AMPERSAND */
	public static final String AMPERSAND = "&";
	
	/**
	 * Constant to hold md5 value.
	 */
	public static final String MD_5 = "MD5";
	
	/**
	 * Constant to hold hex value.
	 */
	public static final long HEX_1 = 0xff;
	
	/**
	 * Constant to hold hex value.
	 */
	public static final long HEX_2 = 0x10;
	
	/**
	 * Constant to hold count sixteen value.
	 */
	public static final int COUNT_SIXTEEN = 16;
	
	/**
	 * Constant for zero value.
	 */
	public static final String ZERO_STRING = "0";
	
	/**
	 * Constant to hold hex string value.
	 */
	public static final String HEX_VAL = "%040x";
	
	/**
	 * Constant to hold userId value.
	 */
	public static final String USER_ID = "userId";
	
	/**
	 * Constant to hold user token value.
	 */
	public static final String USER_TOKEN = "userToken";
	
	/**
	 * Stores constant for output open parameter.
	 */
	public static final String OUTPUT_OPEN_PARAMETER = "output";
	
	/**
	 * Stores constant for empty open parameter.
	 */
	public static final String EMPTY_OPEN_PARAMETER = "empty";
	/**
	 * Constant to hold ReviewsCount parameter name.
	 */
	public static final String REVIEWS_COUNT_PARAM = "reviewsCount";
	
	/**
	 * Constant to hold ProductId parameter name.
	 */
	public static final String PRODUCT_ID_PARAM = "productId";
	
	/** Constant for CONST_ZERO */
	public static final int CONSTANT_VALUE_ZERO = 0;
	
	
	
	
	
	

	/** Constant for CONS_SPACE */
	public static final String CONS_SPACE = " ";

	/** Constant for URL_CONNECTOR */
	public static final String URL_CONNECTOR = "?";
	
	/** Constant for PASS_KEY */
	public static final String PASS_KEY = "passkey";
	
	/** Constant for API_VERSION */
	public static final String API_VERSION = "apiversion";
	
	/** Constant for FILTER */
	public static final String FILTER = "Filter";
	
	/** Constant for AUTHORID */
	public static final String AUTHORID = "AuthorId";
	
	/** Constant for COLON */
	public static final String COLON = ":";
	
	/** Constant for DATA */
	public static final String DATA = "data";
	
	/** Constant for DOT */
	public static final String DOT = ".";

	/** Constant for ITEM_TYPE_USER */
	public static final String ITEM_TYPE_USER = "user";

	/** Constant for ITEM_TYPE_QUESTION */
	public static final String ITEM_TYPE_QUESTION = "questionDetails";
	/** Constant for TIME_STAMP_FORMAT */
	public static final String TIME_STAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

	/** Constant for TOTAL_RESULT_TAG */
	public static final String TOTAL_RESULT_TAG = "TotalResults";
	
	/** Constant for DATAAPI_REPONSE_TAG */
	public static final String DATAAPI_REPONSE_TAG = "DataApiResponse";
	
	/** Constant for SUBMISSION_TIME_TAG */
	public static final String SUBMISSION_TIME_TAG = "SubmissionTime";
	
	/** Constant for QUESTION_SUMMARY_TAG */
	public static final String QUESTION_SUMMARY_TAG = "QuestionSummary";

	/** Constant for PRODUCT_ID_TAG */
	public static final String PRODUCT_ID_TAG = "ProductId";
	
	/** Constant for AUTHOR_ID_TAG */
	public static final String AUTHOR_ID_TAG = "AuthorId";
	
	/** Constant for PHOTOS_TAG */
	public static final String PHOTOS_TAG = "Photos";
	
	/** Constant for VIDEOS_TAG */
	public static final String VIDEOS_TAG = "Videos";

	/** Constant for HAS_ERRORS */
	public static final String HAS_ERRORS = "HasErrors";
	
	/** Constant for CATEGORY_ID */
	public static final String CATEGORY_ID = "CategoryId";
	
	/** Constant for ANSWER_ID */
	public static final String ANSWER_ID = "AnswerIds";
	
	/** Constant for QUESTION_TAG */
	public static final String QUESTION_TAG = "Question";
	
	/** Constant for QUESTIONS_XML_FILE */
	public static final String QUESTIONS_XML_FILE = "questions";
	
	/** Constant for TIME_ZONE */
	public static final String TIME_ZONE = "T";
	
	/** Constant for TIME_ZONE */
	public static final int CONSTANT_VALUE_HUNDRED = 100;
	
	/** Constant for ANSWER_TEXT */
	public static final String ANSWER_TEXT = "AnswerText";
	
	/** Constant for QUESTION_ID */
	public static final String QUESTION_ID = "QuestionId";
	
	/** Constant for REVIEW_TEXT */
	public static final String REVIEW_TEXT = "ReviewText";

	/** Constant for REVIEW_XML_TAG */
	public static final String REVIEW_XML_TAG = "Review";
	
	/** Constant for REVIEWS_XML_FILE */
	public static final String REVIEWS_XML_FILE = "reviews";

	/** Constant for ANSWERS_XML_FILE */
	public static final String ANSWERS_XML_FILE = "answers";
	
	/** Constant for ANSWER_XML_TAG */
	public static final String ANSWER_XML_TAG = "Answer";
	
	/** Constant for CONST_ONE */
	public static final int CONST_ONE = 1;

	/** Constant for CONST_ONE */
	public static final String FORWARD_SLASH = "/";
	
	/** Constant for USER_POINTS */
	public static final String USER_POINTS = "points";
	
	/** Constant for PRODUCT_REVIEW_POINT */
	public static final String PRODUCT_REVIEW_POINT = "ProductReviewPoint";

	/** Constant for REVIEW_WITH_PHOTO_POINT */
	public static final String REVIEW_WITH_PHOTO_POINT = "ReviewWithPhotoPoint";

	/** Constant for REVIEW_WITH_VIDEO_POINT */
	public static final String REVIEW_WITH_VIDEO_POINT = "ReviewWithVideoPoint";

	/** Constant for ASK_QUESTION_POINT */
	public static final String ASK_QUESTION_POINT = "AskQuestionPoint";

	/** Constant for ANSWER_WITH_PHOTO_POINT */
	public static final String ANSWER_WITH_PHOTO_POINT = "AnswerWithPhotoPoint";

	/** Constant for ANSWER_QUESTION_POINT */
	public static final String ANSWER_QUESTION_POINT = "AnswerQuestionPoint";

	/** Constant for ANSWER_WITH_VIDEO_POINT */
	public static final String ANSWER_WITH_VIDEO_POINT = "AnswerWithVideoPoint";

	/** Constant for AVTAR_IMAGE_POINT */
	public static final String AVTAR_IMAGE_POINT = "AvtarImagePoint";

	/** Constant for ABOUT_ME */
	public static final String ABOUT_ME = "AboutMePoint";
	
	/** Constant for LOCAl_RR_FILE_NAME */
	public static final String LOCAL_RR_FILE_NAME = "pr_chainreactioncycles_ratings.xml";
	
	/** Constant for PRODUCT_NODE */
	public static final String PRODUCT_NODE = "Product";
	
	/** Constant for REVIEW_STATISTICS_NODE */
	public static final String REVIEW_STATISTICS_NODE = "ReviewStatistics";
	
	/** Constant for AVERAGE_OVERALLRATING_NODE */
	public static final String AVERAGE_OVERALLRATING_NODE = "AverageOverallRating";
	
	/** Constant for TOTAL_REVIEWCOUNT_NODE */
	public static final String TOTAL_REVIEWCOUNT_NODE = "TotalReviewCount";
	
	/** The Constant ZIP_BUFFER. */
	public static final int ZIP_BUFFER = 1024;
	
	/** The Constant DECIMAL_ROUND_OFF. */
	public static final String DECIMAL_ROUND_OFF = "#0.0";

	/** The Constant RR_SCHEDULAR. */
	public static final String RR_SCHEDULAR = "ReviewRatingSchedular.doScheduledTask";

	/** The Constant PR_INTEGRATION_SCHEDULAR. */
	public static final String PR_INTEGRATION_SCHEDULAR = "PRIntegrationSchedular.doScheduledTask";

	/** The Constant RR_SCHEDULAR_PARAM. */
	public static final String RR_SCHEDULAR_PARAM = "calculateRankPoint";
	
	/** The Constant DELETE_PROCEDURE_NAME. */
	public static final String DELETE_PROCEDURE_NAME = "deleteLeaderBoardQuery";
	
	/** The Constant INCLUDE_PARAMETER. */
	public static final String INCLUDE_PARAMETER = "Include";
	
	/** The Constant ANSWERS_PARAMETER. */
	public static final String ANSWERS_PARAMETER = "Answers";
	
	/** The Constant SEO_SCHEDULER. */
	public static final String SEO_SCHEDULER = "SmartSeoScheduler.doScheduledTask";
	
	
	
	
	
	/** Constant for UNDER_SCORE */
	public static final String UNDER_SCORE = "_";

	/** Constant for REPOSITORY_ITEM */
	public static final String REPOSITORY_ITEM = "RepositoryItem";

	/** Constant for TIMESTAMP_FORMAT_FOR_EXPORT */
	public static final String TIMESTAMP_FORMAT_FOR_EXPORT = "yyyy_MM_dd_HH_mm_ss";

	/** Constant for FILE_TYPE_XML */
	public static final String FILE_TYPE_XML = ".xml";

	/** Constant for NUMBER_ONE */
	public static final int NUMBER_ONE = 1;

	/** Constant for NUMBER_ZERO */
	public static final int NUMBER_ZERO = 0;

	/** Constant for JAXB_CONTEXT_REPOSITORY_DUMP */
	public static final String JAXB_CONTEXT_REPOSITORY_DUMP = "com.data.repository";

	/** The Constant PRODUCT_CONST. */
	public static final String PRODUCT_CONST = "product";

	/** The Constant CATEGORY_CONST. */
	public static final String CATEGORY_CONST = "category";

	/** The Constant BRAND_CONST. */
	public static final String BRAND_CONST = "brand";
	
	/** The Constant PRODUCT_PAGE_URL. */
	public static final String PRODUCT_PAGE_URL = "productPageURL";
	
	/** The Constant CATEGORY_PAGE_URL. */
	public static final String CATEGORY_PAGE_URL = "categoryPageUrl";
	
	/** The Constant IMAGE_URL. */
	public static final String IMAGE_URL = "imageUrl";

	/** The Constant DOT_CONSTANT. */
	public static final String DOT_CONSTANT = "\\.";
	
	/** The Constant ZIP_FILE_EXTENSION. */
	public static final String ZIP_FILE_EXTENSION = ".zip";
	
	/** The Constant TEXT_FILE_EXTENTION. */
	public static final String TEXT_FILE_EXTENTION = ".txt";
	
	/** The Constant BUFFER_SIZE. */
	public static final int BUFFER_SIZE = 100000;
	
	/** The Constant SLASH. */
	public static final String SLASH = "/";
	
	/** The Constant EXCEPTION_TRANSFORM_XSLT_NULL. */
	public static final String EXCEPTION_TRANSFORM_XSLT_NULL = "Unable To Transform as XSLT File Stream is null.";

	/** The Constant EXCEPTION_TRANSFORM_INPUTXML_NULL. */
	public static final String EXCEPTION_TRANSFORM_INPUTXML_NULL = "Unable To Transform as InputXML file is null.";

	/** The Constant EXCEPTION_TRANSFORM_OUTPUTXML_NULL. */
	public static final String EXCEPTION_TRANSFORM_OUTPUTXML_NULL = "Unable To Transform as OutputXML file is null.";

	/** The Constant EXCEPTION_TRANSFORM_INTERNAL_ERROR. */
	public static final String EXCEPTION_TRANSFORM_INTERNAL_ERROR = "Unable To Transform due to internal error.";
	
    /** The Constant TASK_OUTCOME_ID_DEFAULT. */
    public static final String TASK_OUTCOME_ID_DEFAULT = "4.1.1";
    
    /** The Constant PRODUCT_DESC_DEFAULT. */
    public static final String PRODUCT_DESC_DEFAULT = "descriptionDefault";

    /** The Constant OFF_SET. */
    public static final String OFF_SET = "Offset";
    
    /** The Constant LIMIT. */
    public static final String LIMIT = "Limit";
    
    /** The Constant LIMIT. */
    public static final String LOADING_STARTGEY = "lazy";
    
    /** The Constant IMAGE_PRESET. */
    public static final String IMAGE_PRESET = "small";
    
    /** The Constant PRODUCT_PAGE_URLS. */
    public static final String PRODUCT_PAGE_URLS = "ProductPageUrls";
    
    /** The Constant CATEGORY_PAGE_URLS. */
    public static final String CATEGORY_PAGE_URLS = "CategoryPageUrls";

    /** The Constant PLUS_SIGN. */
    public static final String PLUS_SIGN = "+";
    
    /** The Constant GZIP_EXTENSION. */
    public static final String GZIP_EXTENSION = ".gz";

	/** Constant for TAG_DIMENSIONS_TAG */
	public static final String TAG_DIMENSIONS_TAG = "TagDimensions";
	
	/** Constant for CONTENT_LOCALE */
	public static final String CONTENT_LOCALE = "ContentLocale";
	
	/** Constant for USER_NICK_NAME */
	public static final String USER_NICK_NAME = "UserNickname";
	
	/** The Constant BLANK. */
	public static final String BLANK = "";
	
	/** The Constant SUBMISSION_FILTER. */
	public static final String SUBMISSION_FILTER = "&filter=SubmissionTime:gt:";
	
	/** The Constant LATEST_TIME. */
	public static final String LATEST_TIME = "latestTime";
	
	/** The Constant CONST_UNDER_SCORE. */
	public static final String CONST_UNDER_SCORE = "_";
	
	/** The Constant CONST_UNDER_SCORE. */
	public static final int SECOND = 1000;
	
	/** The Constant CONTEXT_DATA_VALUES. */
	public static final String CONTEXT_DATA_VALUES = "ContextDataValues";

	/** The Constant CONTEXT_DATA_VALUES. */
	public static final String CONTEXT_DATA_VALUE = "ContextDataValue";
	
	/** The Constant ATTITUDE. */
	public static final String ATTITUDE = "Attitude";
	
	/** The Constant GENDER. */
	public static final String GENDER = "Gender";
	
	/** The Constant VALUE. */
	public static final String VALUE = "Value";
	
	/** The Constant TAG_DIMENSION. */
	public static final String TAG_DIMENSION = "TagDimension";
	
	/** The Constant VALUES. */
	public static final String VALUES = "Values";
	
	/** The Constant INTERESTS. */
	public static final String INTERESTS = "Interests";
	
	/** The Constant DEFAULT_LONG. */
	public static final long DEFAULT_LONG = 0L;
	
	/** Constant for CONST_MINUS_ONE */
	public static final int CONST_MINUS_ONE = -1;	
	
	/** The Constant IMAGE_URL. */
	public static final String IMAGE_URL_CAP = "imageURL";
	
	/** The Constant CONS_PLUS. */
	public static final String CONS_PLUS = "+";

	/** The Constant CONS_MINUS. */
	public static final String CONS_MINUS = "-";
	
	/** The Constant CURRENT_DATE. */
	public static final String CURRENT_DATE = "currentDate";
	
	/** The Constant FEED_EXPORT_TIMESTAMP. */
	public static final String FEED_EXPORT_TIMESTAMP = "yyyy-MM-dd'T'HH:mm:ss";
	
	/** The Constant PR_PRODUCT_FEED_SCHEDULAR. */
	public static final String PR_PRODUCT_FEED_SCHEDULAR = "FeedExportJob.doFeedJob";
	
	/** The Constant PR_LOCALE. */
	public static final String PR_USERID = "userId";
	
	/** The Constant PR_LOCALE. */
	public static final String PR_LOCALE = "locale";
	
	/** The Constant PR_EMAIL. */
	public static final String PR_EMAIL = "email";
	
	/** The Constant PR_FIRSTNAME. */
	public static final String PR_FIRSTNAME = "firstName";
	
	/** The Constant PR_CITY. */
	public static final String PR_CITY = "city";
	
	/** The Constant PR_STATE. */
	public static final String PR_STATE = "state";
	
	/** The Constant PR_COUNTRY. */
	public static final String PR_COUNTRY = "country";
	
	/** The Constant PR_ORDERID. */
	public static final String PR_ORDERID= "orderId";
	
	/** The Constant PR_TAX. */
	public static final String PR_TAX = "tax";
	
	/** The Constant PR_SHIPPING. */
	public static final String PR_SHIPPING = "shipping";
	
	/** The Constant PR_SKU. */
	public static final String PR_SKU = "sku";
	
	/** The Constant PR_PRODUCT. */
	public static final String PR_PRODUCT = "product";
	
	/** The Constant PR_NAME. */
	public static final String PR_NAME = "name";
	
	/** The Constant PR_IMAGEURL. */
	public static final String PR_IMAGEURL = "imageURL";
	
	/** The Constant PR_CATEGORY. */
	public static final String PR_CATEGORY = "category";
	
	/** The Constant PR_PRICE. */
	public static final String PR_PRICE = "price";
	
	/** The Constant PR_QUANTITY. */
	public static final String PR_QUANTITY = "quantity";
	
	/** The Constant PR_ITEMS. */
	public static final String PR_ITEMS = "items";
	
	/** The Constant PR_NICKNAME. */
	public static final String PR_NICKNAME = "nickname";
	
	/** The Constant PR_TOTAL. */
	public static final String PR_TOTAL = "total";
	
	/** The Constant PR_ORDER. */
	public static final String PR_ORDER = "order";
	
	/** The Constant PR_PROFILE. */
	public static final String PR_PROFILE = "profile";
	
	/** The Constant PR_PIEJSON. */
	public static final String PR_PIEJSON = "pieJson";
	
	/** The Constant DOUBLE_FORWARD_SLASH. */
	public static final String DOUBLE_FORWARD_SLASH = "//";

}
