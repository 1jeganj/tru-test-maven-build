package com.tru.powerreview.cml.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.powerreview.cml.PowerReviewConstants;
import com.tru.powerreview.cml.RRIntegrationConfiguration;

/**
 * This class is used to generate the Bazaarvoice User Authenticated String(UAS token). 
 *
 */
public class PowerReviewUtils extends GenericService{
	
	/**
	 * property to hold RRIntegrationConfiguration.
	 */
	private RRIntegrationConfiguration mPrConfiguration;

	
	/**
	 * This method is used to generate the user encrypted token.
	 * 
	 * @param pUserId - String.
	 * @return String - encrypted user token.
	 */
	public String generateUserAuthenticationString(String pUserId){
		if(isLoggingDebug()){
			logDebug("PowerReviewUtils.generateUserAuthenticationString(String) :: Starts");
		}
		StringBuffer userStr = new StringBuffer();
		//Get the current date and append to userStr
		SimpleDateFormat sdf = new SimpleDateFormat(PowerReviewConstants.DATE_FORMAT,Locale.US);
    	userStr.append(PowerReviewConstants.DATE_PARAM+PowerReviewConstants.EQUAL_SIGN);
    	userStr.append(sdf.format(new Date()));
    	//maxage default value is 1.
    	if(getPrConfiguration().getMaxage()>PowerReviewConstants.CONSTANT_VALUE_ZERO){
    		userStr.append(PowerReviewConstants.AMPERSAND+PowerReviewConstants.MAX_AGE_PARAM+PowerReviewConstants.EQUAL_SIGN);
    		userStr.append(getPrConfiguration().getMaxage());
    	}
    	//Appending Profile Id, system date and maxage.
    	if(!StringUtils.isBlank(pUserId)){
    		userStr.append(PowerReviewConstants.AMPERSAND+PowerReviewConstants.USER_PARAM+PowerReviewConstants.EQUAL_SIGN);
    		userStr.append(pUserId.trim());
    	}
    	//encrypt the final string and PowerReview shared key(Configurable value).
    	String encryptedStr = encryptToMD5(getPrConfiguration().getSharedKey(), userStr.toString());
    	if(isLoggingDebug()){
    		logDebug("Encrypted String value in PowerReviewUtils.generateUserAuthenticationString(String) :: "+encryptedStr);
    	}
    	String hexStr = convertToHex(userStr.toString());
    	if(isLoggingDebug()){
    		logDebug("HexString value in PowerReviewUtils.generateUserAuthenticationString(String) :: "+hexStr);
    	}
		
		if(isLoggingDebug()){
			logDebug("PowerReviewUtils.generateUserAuthenticationString(String) :: Ends");
		}
		return encryptedStr+hexStr;
		
	}
	
	
	/**
	 * This method is used to encrypt the text according to MD5 algorithm.
	 * @param pSharedKey -  Shared key.
	 * @param pUserString - User authentication string.
	 * @return encryptedString - Encrypted string.
	 */
	public String encryptToMD5(String pSharedKey,String pUserString){
       
		if(isLoggingDebug()){
			logDebug("PowerReviewUtils.encryptToMD5(String,String) :: Starts ");
		}
        
        StringBuilder encrptedString = new StringBuilder();
        try {
        	if(isLoggingDebug()){
        		logDebug("SharedKey value in PowerReviewUtils.encryptToMD5(String,String) :: "+pSharedKey);
        		logDebug("UserString value in PowerReviewUtils.encryptToMD5(String,String) :: "+pUserString);
        	}
            if (!StringUtils.isEmpty(pSharedKey) && !StringUtils.isEmpty(pUserString)) {
            	String plainTxt=pSharedKey+pUserString;
                byte[] plainTxtBytes = plainTxt.getBytes();
                MessageDigest md = MessageDigest.getInstance(PowerReviewConstants.MD_5);
                md.reset();
                md.update(plainTxtBytes);
                byte[] mdBytes = md.digest();
                for (int i = 0; i < mdBytes.length; i++) {
                    if ((mdBytes[i] & PowerReviewConstants.HEX_1) < PowerReviewConstants.HEX_2) {
                    	encrptedString.append(PowerReviewConstants.ZERO_STRING);
                    }
                    encrptedString.append(Long.toString(mdBytes[i] & PowerReviewConstants.HEX_1, PowerReviewConstants.COUNT_SIXTEEN));
                }
                if(isLoggingDebug()){
            		logDebug("EncryptedString value in PowerReviewUtils.encryptToMD5(String,String) :: "+encrptedString.toString());
            	}
            }
        } catch (NoSuchAlgorithmException ecx) {
            if (isLoggingError()) {
                logError("No such algorithm found in PowerReviewUtils.encryptToMD5(String,String)", ecx);
            }
        }
        
        if(isLoggingDebug()){
        	logDebug("PowerReviewUtils.encryptToMD5(String,String) :: Ends ");
        }
        return encrptedString.toString();
    }
	
	/**
	 * This method convert the user string to HEX.
	 * 
	 * @param pUserStr
	 *            - User authentication string.
	 * @return string
	 */
    public String convertToHex(String pUserStr) {
    	return String.format(PowerReviewConstants.HEX_VAL, new BigInteger(pUserStr.getBytes()));
    }
		
	/**
	 * getter for RRIntegrationConfiguration.
	 * @return mPrConfiguration.
	 */
	public RRIntegrationConfiguration getPrConfiguration() {
		return mPrConfiguration;
	}

	/**
	 * Setter for RRIntegrationConfiguration.
	 * @param pPrConfiguration -  Bazaar Voice configuration.
	 */
	public void setPrConfiguration(
			RRIntegrationConfiguration pPrConfiguration) {
		mPrConfiguration = pPrConfiguration;
	}

}
