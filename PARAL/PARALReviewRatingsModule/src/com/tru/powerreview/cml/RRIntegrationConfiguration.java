package com.tru.powerreview.cml;

/**
 *  This component stores the configuration specific to a powerreview integration.
 *
 */
public class RRIntegrationConfiguration {
	
	/**
	 * property to hold shared key.
	 */
	private String mSharedKey;
	
	/**
	 * property to hold Maxage - Number of days before the UAS expires.
	 */
	private int mMaxage;
	
	/**
	 * property to hold powerreview DomainName.
	 */
	private String mPrDomainName;
	
	/**
	 * property to hold powerreview DisplayCode.
	 */
	private String mPrDisplayCode;
	
	/**
	 * property to hold powerreview FtpHostName.
	 */
	private String mFtpHostName;
	
	/**
	 * property to hold powerreview SftpHostName.
	 */
	private String mSftpHostName;
	
	/**
	 * property to hold PrjavascriptUrl.
	 */
	private String mPrjavascriptUrl;
	/**
	 * property to hold mUseLocale.
	 */
	private boolean mUseLocale;
	/**
	 * property to hold mRatingboxid.
	 */
	private boolean mRatingboxid;
	/**
	 * property to hold mCompanyid.
	 */
	private boolean mCompanyid;
	/**
	 * property to hold mRatingsystemcss.
	 */
	private boolean mRatingsystemcss;
	
	/**
	 * @return the ratingboxid
	 */
	public boolean isRatingboxid() {
		return mRatingboxid;
	}

	/**
	 * @param pRatingboxid the ratingboxid to set
	 */
	public void setRatingboxid(boolean pRatingboxid) {
		mRatingboxid = pRatingboxid;
	}

	/**
	 * @return the companyid
	 */
	public boolean isCompanyid() {
		return mCompanyid;
	}

	/**
	 * @param pCompanyid the companyid to set
	 */
	public void setCompanyid(boolean pCompanyid) {
		mCompanyid = pCompanyid;
	}

	/**
	 * @return the ratingsystemcss
	 */
	public boolean isRatingsystemcss() {
		return mRatingsystemcss;
	}

	/**
	 * @param pRatingsystemcss the ratingsystemcss to set
	 */
	public void setRatingsystemcss(boolean pRatingsystemcss) {
		mRatingsystemcss = pRatingsystemcss;
	}

	/**
	 * @return the useLocale
	 */
	public boolean isUseLocale() {
		return mUseLocale;
	}

	/**
	 * @param pUseLocale the useLocale to set
	 */
	public void setUseLocale(boolean pUseLocale) {
		mUseLocale = pUseLocale;
	}

	/**
	 * @return the prjavascriptUrl
	 */
	public String getPrjavascriptUrl() {
		return mPrjavascriptUrl;
	}

	/**
	 * @param pPrjavascriptUrl the prjavascriptUrl to set
	 */
	public void setPrjavascriptUrl(String pPrjavascriptUrl) {
		mPrjavascriptUrl = pPrjavascriptUrl;
	}

	/**
	 * @return the prDomainName
	 */
	public String getPrDomainName() {
		return mPrDomainName;
	}

	/**
	 * @param pPrDomainName the prDomainName to set
	 */
	public void setPrDomainName(String pPrDomainName) {
		mPrDomainName = pPrDomainName;
	}

	/**
	 * @return the prDisplayCode
	 */
	public String getPrDisplayCode() {
		return mPrDisplayCode;
	}

	/**
	 * @param pPrDisplayCode the prDisplayCode to set
	 */
	public void setPrDisplayCode(String pPrDisplayCode) {
		mPrDisplayCode = pPrDisplayCode;
	}

	/**
	 * @return the ftpHostName
	 */
	public String getFtpHostName() {
		return mFtpHostName;
	}

	/**
	 * @param pFtpHostName the ftpHostName to set
	 */
	public void setFtpHostName(String pFtpHostName) {
		mFtpHostName = pFtpHostName;
	}

	/**
	 * @return the sftpHostName
	 */
	public String getSftpHostName() {
		return mSftpHostName;
	}

	/**
	 * @param pSftpHostName the sftpHostName to set
	 */
	public void setSftpHostName(String pSftpHostName) {
		mSftpHostName = pSftpHostName;
	}

	/**
	 * Getter for SharedKey.
	 * @return mSharedKey.
	 */
	public String getSharedKey() {
		return mSharedKey;
	}

	/**
	 * Setter for Shared key.
	 * @param pSharedKey - the shared key.
	 */
	public void setSharedKey(String pSharedKey) {
		this.mSharedKey = pSharedKey;
	}

	/**
	 * Getter for Maxage.
	 * @return mMaxage
	 */
	public int getMaxage() {
		return mMaxage;
	}

	/**
	 * Setter for Maxage.
	 * @param pMaxage - the maxage
	 */
	public void setMaxage(int pMaxage) {
		mMaxage = pMaxage;
	}


}
