package com.tru.powerreview.tol;

/**
 * This class contains PowerReview properties.
 *
 */
public class PowerReviewPropertyManager {

	/**
	 * property to hold Average Rating property name.
	 */
	private String mAverageRatingPropName;
	
	/**
	 * property to hold Reviews Count property name.
	 */
	private String mReviewsCountPropName;
	
	/**
	 * property to hold PrReviewRatingItemDescName.
	 */
	private String mPrReviewRatingItemDescName;
	
	/**
	 * property to hold FirstName Property Name.
	 */
	private String mFirstNamePropName;
	/**
	 * property to hold HomeAddress Property Name.
	 */
	private String mHomeAddressPropName;
	/**
	 * property to hold City Property Name.
	 */
	private String mCityPropName;
	/**
	 * property to hold State Property Name.
	 */
	private String mStatePropName;
	/**
	 * property to hold Country Property Name..
	 */
	private String mCountryPropName;
	/**
	 * property to hold Product Property Name..
	 */
	private String mProductPropName;
	/**
	 * property to hold DisplayName Property Name.
	 */
	private String mDisplayNamePropName;
	/**
	 * property to hold ThumbnailImage Property Name.
	 */
	private String mThumbnailImagePropName;
	/**
	 * property to hold Url Property Name.
	 */
	private String mUrlPropName;
	/**
	 * property to hold Email Property Name.
	 */
	private String mEmailPropName;
	/**
	 * property to hold ParentCategories Property Name.
	 */
	private String mParentCategoriesPropName;
	/**
	 * property to hold Locale Property Name.
	 */
	private String mLocalePropName;
	/**
	 * property to hold ProductId Property Name.
	 */
	private String mProductIdPropName;
	/**
	 * property to hold CommerceItems Property Name.
	 */
	private String mCommerceItemsPropName;
	/**
	 * property to hold PriceInfo Property Name.
	 */
	private String mPriceInfoPropName;
	/**
	 * property to hold Amount Property Name.
	 */
	private String mAmountPropName;
	/**
	 * property to hold ProfileId Property Name.
	 */
	private String mProfileIdPropName;
	/**
	 * property to hold User Property Name.
	 */
	private String mUserPropName;
	/**
	 * property to hold SubmittedDate Property Name.
	 */
	private String mSubmittedDatePropName;
	/**
	 * @return the prReviewRatingItemDescName
	 */
	public String getPrReviewRatingItemDescName() {
		return mPrReviewRatingItemDescName;
	}

	/**
	 * @param pPrReviewRatingItemDescName the prReviewRatingItemDescName to set
	 */
	public void setPrReviewRatingItemDescName(String pPrReviewRatingItemDescName) {
		mPrReviewRatingItemDescName = pPrReviewRatingItemDescName;
	}

	/**
	 * Getter for Average Rating property name.
	 * @return mAverageRatingPropName.
	 */
	public String getAverageRatingPropName() {
		return mAverageRatingPropName;
	}

	/**
	 * Setter for Average Rating property name.
	 * @param pAverageRatingPropName - the Average Rating property name.
	 */
	public void setAverageRatingPropName(String pAverageRatingPropName) {
		mAverageRatingPropName = pAverageRatingPropName;
	}

	/**
	 * Getter for Reviews Count property name.
	 * @return mReviewsCountPropName
	 */
	public String getReviewsCountPropName() {
		return mReviewsCountPropName;
	}

	/**
	 * Setter for Reviews Count property name.
	 * @param pReviewsCountPropName - the Reviews Count property name.
	 */
	public void setReviewsCountPropName(String pReviewsCountPropName) {
		mReviewsCountPropName = pReviewsCountPropName;
	}

	/**
	 * @return the firstNamePropName
	 */
	public String getFirstNamePropName() {
		return mFirstNamePropName;
	}

	/**
	 * @param pFirstNamePropName the firstNamePropName to set
	 */
	public void setFirstNamePropName(String pFirstNamePropName) {
		mFirstNamePropName = pFirstNamePropName;
	}

	/**
	 * @return the homeAddressPropName
	 */
	public String getHomeAddressPropName() {
		return mHomeAddressPropName;
	}

	/**
	 * @param pHomeAddressPropName the homeAddressPropName to set
	 */
	public void setHomeAddressPropName(String pHomeAddressPropName) {
		mHomeAddressPropName = pHomeAddressPropName;
	}

	/**
	 * @return the cityPropName
	 */
	public String getCityPropName() {
		return mCityPropName;
	}

	/**
	 * @param pCityPropName the cityPropName to set
	 */
	public void setCityPropName(String pCityPropName) {
		mCityPropName = pCityPropName;
	}

	/**
	 * @return the statePropName
	 */
	public String getStatePropName() {
		return mStatePropName;
	}

	/**
	 * @param pStatePropName the statePropName to set
	 */
	public void setStatePropName(String pStatePropName) {
		mStatePropName = pStatePropName;
	}

	/**
	 * @return the countryPropName
	 */
	public String getCountryPropName() {
		return mCountryPropName;
	}

	/**
	 * @param pCountryPropName the countryPropName to set
	 */
	public void setCountryPropName(String pCountryPropName) {
		mCountryPropName = pCountryPropName;
	}

	/**
	 * @return the productPropName
	 */
	public String getProductPropName() {
		return mProductPropName;
	}

	/**
	 * @param pProductPropName the productPropName to set
	 */
	public void setProductPropName(String pProductPropName) {
		mProductPropName = pProductPropName;
	}

	/**
	 * @return the displayNamePropName
	 */
	public String getDisplayNamePropName() {
		return mDisplayNamePropName;
	}

	/**
	 * @param pDisplayNamePropName the displayNamePropName to set
	 */
	public void setDisplayNamePropName(String pDisplayNamePropName) {
		mDisplayNamePropName = pDisplayNamePropName;
	}

	/**
	 * @return the thumbnailImagePropName
	 */
	public String getThumbnailImagePropName() {
		return mThumbnailImagePropName;
	}

	/**
	 * @param pThumbnailImagePropName the thumbnailImagePropName to set
	 */
	public void setThumbnailImagePropName(String pThumbnailImagePropName) {
		mThumbnailImagePropName = pThumbnailImagePropName;
	}

	/**
	 * @return the urlPropName
	 */
	public String getUrlPropName() {
		return mUrlPropName;
	}

	/**
	 * @param pUrlPropName the urlPropName to set
	 */
	public void setUrlPropName(String pUrlPropName) {
		mUrlPropName = pUrlPropName;
	}

	/**
	 * @return the emailPropName
	 */
	public String getEmailPropName() {
		return mEmailPropName;
	}

	/**
	 * @param pEmailPropName the emailPropName to set
	 */
	public void setEmailPropName(String pEmailPropName) {
		mEmailPropName = pEmailPropName;
	}

	/**
	 * @return the parentCategoriesPropName
	 */
	public String getParentCategoriesPropName() {
		return mParentCategoriesPropName;
	}

	/**
	 * @param pParentCategoriesPropName the parentCategoriesPropName to set
	 */
	public void setParentCategoriesPropName(String pParentCategoriesPropName) {
		mParentCategoriesPropName = pParentCategoriesPropName;
	}

	/**
	 * @return the localePropName
	 */
	public String getLocalePropName() {
		return mLocalePropName;
	}

	/**
	 * @param pLocalePropName the localePropName to set
	 */
	public void setLocalePropName(String pLocalePropName) {
		mLocalePropName = pLocalePropName;
	}

	/**
	 * @return the productIdPropName
	 */
	public String getProductIdPropName() {
		return mProductIdPropName;
	}

	/**
	 * @param pProductIdPropName the productIdPropName to set
	 */
	public void setProductIdPropName(String pProductIdPropName) {
		mProductIdPropName = pProductIdPropName;
	}

	/**
	 * @return the commerceItemsPropName
	 */
	public String getCommerceItemsPropName() {
		return mCommerceItemsPropName;
	}

	/**
	 * @param pCommerceItemsPropName the commerceItemsPropName to set
	 */
	public void setCommerceItemsPropName(String pCommerceItemsPropName) {
		mCommerceItemsPropName = pCommerceItemsPropName;
	}

	/**
	 * @return the priceInfoPropName
	 */
	public String getPriceInfoPropName() {
		return mPriceInfoPropName;
	}

	/**
	 * @param pPriceInfoPropName the priceInfoPropName to set
	 */
	public void setPriceInfoPropName(String pPriceInfoPropName) {
		mPriceInfoPropName = pPriceInfoPropName;
	}

	/**
	 * @return the amountPropName
	 */
	public String getAmountPropName() {
		return mAmountPropName;
	}

	/**
	 * @param pAmountPropName the amountPropName to set
	 */
	public void setAmountPropName(String pAmountPropName) {
		mAmountPropName = pAmountPropName;
	}

	/**
	 * @return the profileIdPropName
	 */
	public String getProfileIdPropName() {
		return mProfileIdPropName;
	}

	/**
	 * @param pProfileIdPropName the profileIdPropName to set
	 */
	public void setProfileIdPropName(String pProfileIdPropName) {
		mProfileIdPropName = pProfileIdPropName;
	}

	/**
	 * @return the userPropName
	 */
	public String getUserPropName() {
		return mUserPropName;
	}

	/**
	 * @param pUserPropName the userPropName to set
	 */
	public void setUserPropName(String pUserPropName) {
		mUserPropName = pUserPropName;
	}

	/**
	 * @return the submittedDatePropName
	 */
	public String getSubmittedDatePropName() {
		return mSubmittedDatePropName;
	}

	/**
	 * @param pSubmittedDatePropName the submittedDatePropName to set
	 */
	public void setSubmittedDatePropName(String pSubmittedDatePropName) {
		mSubmittedDatePropName = pSubmittedDatePropName;
	}	
	
}
