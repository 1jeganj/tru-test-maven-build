package com.tru.powerreview.tol;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.beans.DynamicBeanMap;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.core.util.StringUtils;
import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.Profile;

import com.tru.common.cml.IErrorId;
import com.tru.common.cml.SystemException;
import com.tru.powerreview.cml.PowerReviewConstants;

/**
 * The Repository Tools Class for Rating * 
 */
public class RatingsRepositoryTools extends GenericService implements IRatingsRepositoryTools {

	/**
	 * property to hold Ratings repository.
	 */
	private Repository mRatingsRepository;
	/**
	 * property to hold Ratings repository.
	 */
	private Repository mProductCatalogRepository;

	/**
	 * Property to hold Bazaarvoice Property Manager.
	 */
	private PowerReviewPropertyManager mPropertyManger;

	/**
	 * @return the productCatalogRepository
	 */
	public Repository getProductCatalogRepository() {
		return mProductCatalogRepository;
	}

	/**
	 * @param pProductCatalogRepository
	 *            the productCatalogRepository to set
	 */
	public void setProductCatalogRepository(Repository pProductCatalogRepository) {
		mProductCatalogRepository = pProductCatalogRepository;
	}

	/**
	 * Getter for RatingsRepository.
	 * 
	 * @return mRatingsRepository.
	 */
	public Repository getRatingsRepository() {
		return mRatingsRepository;
	}

	/**
	 * Setter for RatingsRepository.
	 * 
	 * @param pRatingsRepository
	 *            - the RatingsRepository.
	 */
	public void setRatingsRepository(Repository pRatingsRepository) {
		mRatingsRepository = pRatingsRepository;
	}

	/**
	 * Getter for Bazaarvoice Property Manager.
	 * 
	 * @return mPropertyManger.
	 */
	public PowerReviewPropertyManager getPropertyManger() {
		return mPropertyManger;
	}

	/**
	 * Setter for Bazaarvoice Property Manager.
	 * 
	 * @param pPropertyManger
	 *            - the Bazaarvoice Property Manager.
	 */
	public void setPropertyManger(PowerReviewPropertyManager pPropertyManger) {
		mPropertyManger = pPropertyManger;
	}

	/**
	 * This method is used to get the average rating and reviews count for the
	 * given product id.
	 * 
	 * @param pProductId
	 *            - the product id.
	 * @return ratingsReviewMap - map of average rating and reviews count.
	 * @throws RepositoryException
	 *             - when error occurs.
	 */
	public Map<String, String> getRatingsForProduct(String pProductId) throws SystemException {
		if (isLoggingDebug()) {
			logDebug("RatingsRepositoryTools.getRatingsAndReviewsForProduct(String) :: Starts");
		}
		Map<String, String> ratingsReviewMap = null;
		if (!StringUtils.isBlank(pProductId)) {
			if (isLoggingDebug()) {
				logDebug("ProductID in RatingsRepositoryTools.getRatingsAndReviewsForProduct(String) :: " + pProductId);
			}
			try {
				RepositoryItem prodRatingItem = getRatingsRepository().getItem(pProductId.trim(),getPropertyManger().getPrReviewRatingItemDescName());
				if (null != prodRatingItem) {
					ratingsReviewMap = new DynamicBeanMap(prodRatingItem);
				}
				if (isLoggingDebug()) {
					logDebug("Ratings Review Map in RatingsRepositoryTools.getRatingsAndReviewsForProduct(String) :: "+ratingsReviewMap);
				}
			} catch (RepositoryException re) {
				throw new SystemException(IErrorId.UNABLE_TO_GET_THE_ITEM, re);
			}
		}

		if (isLoggingDebug()) {
			logDebug("RatingsRepositoryTools.getRatingsAndReviewsForProduct(String) :: Ends");
		}
		return ratingsReviewMap;

	}

	/**
	 * This method constructs JSONObject with UserParameters,OrderParameters and
	 * ItemParameters
	 * 
	 * @param pRequest 
	 * 			   --  DynamoHttpServletRequest
	 * @param pOrder
	 *            -- instance of Order
	 * @param pProfile
	 *            -- instance of Profile
	 * @throws SystemException
	 *             -- when exception occured
	 * @return JSONObject
	 */
	public JSONObject constructPIEJSONArray(DynamoHttpServletRequest pRequest,Order pOrder, Profile pProfile)
			throws SystemException {
		if (isLoggingDebug()) {
			logDebug("RatingsRepositoryTools.constructPIEJsonArray(Order,Profile,DynamoHttpServletRequest) :: Starts");
		}
		JSONObject json = new JSONObject();
		if (pOrder != null && pProfile != null) {
			// call this method to construct User Parameters
			constructUserParameters(json, pOrder, pProfile);
			// call this method to construct Order Parameters
			constructOrderParameters(json, pOrder);
			// call this method to construct Item Parameters
			constructItemParameters(json, pOrder,pRequest);
		}
		if (isLoggingDebug()) {
			logDebug("RatingsRepositoryTools.constructPIEJsonArray(Order,Profile,DynamoHttpServletRequest) :: Ends");
		}
		return json;
	}

	/**
	 * This method constructs UserParameters for JSONOject.
	 * 
	 * @param pJSONObject
	 *            -- instance of JSONObject
	 * @param pOrder
	 *            -- instance of Order
	 * @param pProfile
	 *            -- instance of Profile
	 * @throws SystemException
	 *             -- when exception occured
	 */
	protected void constructUserParameters(JSONObject pJSONObject, Order pOrder,Profile pProfile) throws SystemException {
		if (isLoggingDebug()) {
			logDebug("RatingsRepositoryTools.constructUserParameters(JSONObject,Order,Profile) :: Starts");
		}
		String userId = null;
		String locale = null;
		String email = null;
		String nickName = null;
		RepositoryItem homeAddress = null;
		String city = null;
		String state = null;
		String country = null;
		userId = pProfile.getRepositoryId();
		try {
			if (!StringUtils.isBlank(userId)) {
				pJSONObject.put(PowerReviewConstants.PR_USERID, userId);
			}
			locale = (String) pProfile.getPropertyValue(getPropertyManger().getLocalePropName());
			if (!StringUtils.isBlank(locale)) {
				pJSONObject.put(PowerReviewConstants.PR_LOCALE, locale);
			}

			email = (String) pProfile.getPropertyValue(getPropertyManger().getEmailPropName());
			if (!StringUtils.isBlank(email)) {
				pJSONObject.put(PowerReviewConstants.PR_EMAIL, email);
			}
			nickName = (String) pProfile.getPropertyValue(getPropertyManger().getFirstNamePropName());
			if (!StringUtils.isBlank(nickName)) {
				pJSONObject.put(PowerReviewConstants.PR_NICKNAME, nickName);
			}
			homeAddress = (RepositoryItem) pProfile.getPropertyValue(getPropertyManger().getHomeAddressPropName());
			if (homeAddress != null) {
				city = (String) homeAddress
						.getPropertyValue(getPropertyManger().getCityPropName());
				if (!StringUtils.isBlank(city)) {
					pJSONObject.put(PowerReviewConstants.PR_CITY, city);
				}
				state = (String) homeAddress.getPropertyValue(getPropertyManger().getStatePropName());
				if (!StringUtils.isBlank(state)) {
					pJSONObject.put(PowerReviewConstants.PR_STATE, state);
				}
				country = (String) homeAddress.getPropertyValue(getPropertyManger().getCountryPropName());
				if (!StringUtils.isBlank(country)) {
					pJSONObject.put(PowerReviewConstants.PR_COUNTRY, country);
				}
			}
			if (isLoggingDebug()) {
				logDebug("RatingsRepositoryTools.constructUserParameters(JSONObject,Order,Profile) :: Ends");
			}
		} catch (JSONException pJSONException) {
			throw new SystemException(
					IErrorId.UNABLE_TO_ADD_ITEMS_TO_JSONOBJECT, pJSONException);
		}
	}

	/**
	 * This method constructs OrderParameters for JSONOject.
	 * 
	 * @param pJSONObject
	 *            -- instance of JSONObject
	 * @param pOrder
	 *            -- instance of Order
	 * @throws SystemException
	 *             -- when exception occured
	 */
	protected void constructOrderParameters(JSONObject pJSONObject, Order pOrder) throws SystemException {
		if (isLoggingDebug()) {
			logDebug("RatingsRepositoryTools.constructOrderParameters(JSONObject,Order) :: Starts");
		}
		String orderId = pOrder.getId();
		try {
			if (!StringUtils.isBlank(orderId)) {
				pJSONObject.put(PowerReviewConstants.PR_ORDERID, orderId);
			}
			double tax = pOrder.getPriceInfo().getTax();
			pJSONObject.put(PowerReviewConstants.PR_TAX, tax);
			
			Double shipping = pOrder.getPriceInfo().getShipping();
			pJSONObject.put(PowerReviewConstants.PR_SHIPPING,shipping.toString());
			
			Double amt = pOrder.getPriceInfo().getAmount();
			pJSONObject.put(PowerReviewConstants.PR_TOTAL, amt.toString());
			if (isLoggingDebug()) {
				logDebug("RatingsRepositoryTools.constructOrderParameters(JSONObject,Order) :: Ends");
			}
		} catch (JSONException pJSONException) {
			throw new SystemException(
					IErrorId.UNABLE_TO_ADD_ITEMS_TO_JSONOBJECT, pJSONException);
		}
	}

	/**
	 * This method constructs ItemParameters for JSONOject.
	 * 
	 * @param pJSONObject
	 *            -- instance of JSONObject
	 * @param pOrder
	 *            -- instance of Order
	 * @param pRequest 
	 * 			  --  DynamoHttpServletRequest
	 * @throws SystemException
	 *             -- when exception occured
	 */
	protected void constructItemParameters(JSONObject pJSONObject, Order pOrder,DynamoHttpServletRequest pRequest)
			throws SystemException {
		if (isLoggingDebug()) {
			logDebug("RatingsRepositoryTools.constructItemParameters(JSONObject,Order,DynamoHttpServletRequest) :: Starts");
		}
		JSONArray jsonArray = new JSONArray();
		RepositoryItem categoryItem = null;
		RepositoryItem productItem = null;
		String productName = null;
		String categoryName = null;
		List commItemList = pOrder.getCommerceItems();
		try {
			if (commItemList!=null && !commItemList.isEmpty()) {
				Iterator iter = commItemList.iterator();
				while (iter.hasNext()) {
					JSONObject jsonItems = new JSONObject();
					CommerceItem commitem = (CommerceItem) iter.next();
					String prodId = commitem.getAuxiliaryData().getProductId();
					if (!StringUtils.isBlank(prodId)) {
						jsonItems.put(PowerReviewConstants.PR_SKU, prodId);
						try {
							productItem = getProductCatalogRepository().getItem(prodId,getPropertyManger().getProductPropName());
							if (productItem != null) {
								productName = (String) productItem.getPropertyValue(getPropertyManger().getDisplayNamePropName());
								if (!StringUtils.isBlank(productName)) {
									jsonItems.put(PowerReviewConstants.PR_NAME,productName);
								}
							}
						} catch (RepositoryException pRepositoryException) {
							throw new SystemException(IErrorId.UNABLE_TO_GET_THE_ITEM,pRepositoryException);
						}
					}
					// call constructImageUrl method to get the constructed imageUrl
					String imageUrl = constructImageUrl(productItem,pRequest);
					if (!StringUtils.isBlank(imageUrl)) {
						jsonItems.put(PowerReviewConstants.PR_IMAGEURL, imageUrl);
					}
					Set<RepositoryItem> parentCategories = (Set<RepositoryItem>) productItem.getPropertyValue(getPropertyManger().getParentCategoriesPropName());
					if (parentCategories!=null && !parentCategories.isEmpty()) {
						categoryItem = (RepositoryItem) parentCategories.toArray()[0];
						if (categoryItem != null) {
							categoryName = (String) categoryItem.getPropertyValue(getPropertyManger().getDisplayNamePropName());
							if (!StringUtils.isBlank(categoryName)) {
								jsonItems.put(PowerReviewConstants.PR_CATEGORY,categoryName);
							}
						}
					}
					if (commitem.getPriceInfo() != null) {
						jsonItems.put(PowerReviewConstants.PR_PRICE, commitem.getPriceInfo().getAmount());
					}
					jsonItems.put(PowerReviewConstants.PR_QUANTITY,commitem.getQuantity());
					jsonArray.add(jsonItems);
				}
			}
			pJSONObject.put(PowerReviewConstants.PR_ITEMS, jsonArray);
			if (isLoggingDebug()) {
				logDebug("RatingsRepositoryTools.constructItemParameters(JSONObject,Order,DynamoHttpServletRequest) :: Ends");
			}
		} catch (JSONException pJSONException) {
			throw new SystemException(
					IErrorId.UNABLE_TO_ADD_ITEMS_TO_JSONOBJECT, pJSONException);
		}
	}
	
	/**
	 * This method constructs imageUrl
	 * 
	 * @param pRepoItem --  Repository Item
	 * @param pRequest --  DynamoHttpServletRequest
	 * @return String -- imageUrl
	 */
	protected String constructImageUrl(RepositoryItem pRepoItem,DynamoHttpServletRequest pRequest) {
		if (isLoggingDebug()) {
			logDebug("RatingsRepositoryTools.constructImageUrl(RepositoryItem,DynamoHttpServletRequest) :: Starts");
		}
		String imageUrl = null;
		RepositoryItem thumbnailImage = (RepositoryItem) pRepoItem.getPropertyValue(getPropertyManger().getThumbnailImagePropName());
		if (thumbnailImage != null) {
			String imagePath = (String) thumbnailImage.getPropertyValue(getPropertyManger().getUrlPropName());
			imageUrl=pRequest.getScheme()+PowerReviewConstants.COLON+PowerReviewConstants.DOUBLE_FORWARD_SLASH+pRequest.getServerName()+PowerReviewConstants.COLON+pRequest.getServerPort()+imagePath;
		}
		if (isLoggingDebug()) {
			logDebug("RatingsRepositoryTools.constructImageUrl(RepositoryItem,DynamoHttpServletRequest) :: Ends");
		}
		return imageUrl;
	}
}
