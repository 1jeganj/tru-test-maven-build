package com.tru.common.cml;


/**
 * The Class ResourceBundleConfiguration.
 *  @author Professional Access
 *  @version 1.0
 *
 */
public class ResourceBundleConfiguration {
		
	/** Holds the Constant of mDefaultSiteId. */
	private String mDefaultSiteId;
	
	/** Holds the Constant of mMessageProcessorKey. */
	private String mMessageProcessorKey;
	
	/**
	 * Gets the default site id.
	 *
	 * @return mDefaultSiteId
	 */
	public String getDefaultSiteId() {
		return mDefaultSiteId;
	}
	
	/**
	 * Sets the default site id.
	 *
	 * @param pDefaultSiteId DefaultSiteId
	 */
	public void setDefaultSiteId(String pDefaultSiteId) {
		this.mDefaultSiteId = pDefaultSiteId;
	}		

	/**
	 * Gets the error message processor key.
	 *
	 * @return the siteKeyName
	 */
	public String getMessageProcessorKey() {
		return mMessageProcessorKey;
	}

	/**
	 * Sets the error message processor key.
	 *
	 * @param pMessageProcessorKey - String
	 */
	public void setMessageProcessorKey(String pMessageProcessorKey) {
		mMessageProcessorKey = pMessageProcessorKey;
	}
	
}
