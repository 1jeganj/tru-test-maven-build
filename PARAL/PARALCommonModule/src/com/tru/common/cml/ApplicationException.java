package com.tru.common.cml;

/**
 * The Class ApplicationException.
 * @author Professional Access
 * @version 1.0
 */
public class ApplicationException extends BaseException{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3862465564314792256L;

	/**
	 * Instantiates a new application exception.
	 *
	 * @param pErrorId - String
	 * @param pMsgCntx - Object[]
	 * @param pCause - Throwable
	 */
	public ApplicationException(String pErrorId, Object[] pMsgCntx, Throwable pCause) 
	{
		super(pErrorId, pMsgCntx, pCause);
	}

	/**
	 * Instantiates a new application exception.
	 *
	 * @param pErrorId - String
	 */
	public ApplicationException(String pErrorId)
	{
		super(pErrorId, null, null);
	}

	/**
	 * Instantiates a new application exception.
	 *
	 * @param pErrorId - String
	 * @param pMsgCntx - Object[]
	 */
	public ApplicationException(String pErrorId, Object[] pMsgCntx)
	{
		super(pErrorId, pMsgCntx, null);
	}

	/**
	 * Instantiates a new application exception.
	 *
	 * @param pErrorId - String
	 * @param pCause - Throwable
	 */
	public ApplicationException(String pErrorId, Throwable pCause)
	{
		super(pErrorId, null, pCause);
	}
}
