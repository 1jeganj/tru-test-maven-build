package com.tru.common.cml;
import java.io.InputStream;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;

/**
 * The Class AbstractComponent.
 *
 *  @version 1.0
 *  @author Professional Access
 */
public abstract class AbstractComponent extends GenericService{

	/**
	 * Initcomponent.
	 *
	 * @throws SystemException - when error occurs.
	 */
	protected abstract void initComponent() throws SystemException;

	/**
	 * Do start service.
	 *
	 * @throws ServiceException the service exception
	 * @see atg.nucleus.GenericService#doStartService()
	 */
	@Override
	public void doStartService() throws ServiceException
	{
		super.doStartService();
		try
		{
			initComponent();
		}
		catch (SystemException e) 
		{
			throw new ServiceException(e);
		}

	}

	/**
	 * Load resource.
	 *
	 * @param pResourcePath - String
	 * @return InputStream inpStrm
	 * @throws SystemException SystemException
	 */
	public InputStream loadResource(String pResourcePath) throws SystemException
	{
		if ((pResourcePath == null) || (pResourcePath.trim().length() == 0))
		{
			throw new SystemException(IErrorId.INVALID_RESOURCE_PATH, new Object[] { pResourcePath });
		}
		
		InputStream inpStrm = Thread.currentThread().getContextClassLoader().getResourceAsStream(pResourcePath);

		if (inpStrm == null)
		{		
			throw new SystemException(IErrorId.UNABLE_TO_LOAD_RESOURCE, new Object[] { pResourcePath });
		}
		
		return inpStrm;
	}

}