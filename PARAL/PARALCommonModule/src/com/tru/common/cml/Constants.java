package com.tru.common.cml;

/**
 * The Interface IConstants.
 *
 *  @version 1.0
 *  @author Professional Access
 */
public class Constants {
	
	/** constant for Validator. */	
	public final static String COMPONENT_VALIDATOR = "Validator";
	/** The Constant ERROR_ID_00000. */
	public static final String ERROR_ID_00000="00000";
	/** The Constant CONSTANT_EMPTY. */
	public static final String CONSTANT_EMPTY = "";
	/** The Constant COMMA. */
	public static final String COMMA = ",";
	/** The Constant EQUALS. */
	public static final String EQUALS = "=";
	/** The Constant VALUE_ZERO. */
	public static final int VALUE_ZERO = 0;
	/** The Constant UNDERSCORE. */
	public static final String UNDERSCORE = "_";
	/** The Constant VALUE_ONE. */
	public static final int VALUE_ONE = 1;
	/** The Constant TRANSLATIONS. */
	public static final String TRANSLATIONS = "translations";
	/** The Constant REPOSITORY_EXCEPTION. */
	public static final String REPOSITORY_EXCEPTION = "Repository Exception";
	/** The Constant NO_ERROR_KEY. */
	public static final String NO_ERROR_KEY = "No ErrorKey Found";
	/** The Constant SYSTEM_EXPECTION_NO_DATA. */
	public static final String SYSTEM_EXPECTION_NO_DATA = "No data";
	/** The Constant TRANSACTION_EXCEPTION_MSG. */
	public static final String TRANSACTION_EXCEPTION_MSG="Transaction Demarcation Exception Occured";
	/** The Constant RESOURCE_BUNDLE_MANAGER. */
	public static final String RESOURCE_BUNDLE_MANAGER="dynamo:/com/tru/resourcebundle/mgl/ResourceBundleManager";
	/** The Constant TIMESTAMP_FORMAT_FOR_IMPORT. */
	public static final String TIMESTAMP_FORMAT_FOR_IMPORT= "yyyy-MM-dd hh:mm:ss";
	/** The Constant ZERO. */
	public static final int ZERO = 0;
	/** The Constant DOT. */
	public static final String DOT = ".";
	/** The constant for replacing text*/
	public static final String SOURCE_STRING= "(?<!')'(?!')";
	/** The constant for DESTINATION text*/
	public static final String DESTINATION_STRING= "''";
}