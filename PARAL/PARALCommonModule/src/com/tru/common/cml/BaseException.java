package com.tru.common.cml;


/**
 * The Class BaseException.
 *
 *  @version 1.0
 *  @author Professional Access
 */
public class BaseException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -715784222207803167L;
	
	/** The m error id. */
	private String mErrorId;
	
	/** The m mg cntx. */
	private Object[] mMgCntx;

	/**
	 * Instantiates a new base exception.
	 *
	 * @param pErrorId - String
	 * @param pMsgCntx - Object[]
	 * @param pCause - Throwable
	 */
	public BaseException(String pErrorId, Object[] pMsgCntx, Throwable pCause)
	{
		super(pCause);
		this.mErrorId = pErrorId;
		//Object[] msgContext = null;
		this.mMgCntx = pMsgCntx;
	}
	
	/**
	 * Getter method for ErrorId.
	 *
	 * @return errorId - String
	 */
	public String getErrorId()
	{
		return mErrorId;
	}
	
	/**
	 * Getter method for MessageContext.
	 *
	 * @return msgCntx - Object[]
	 */
	public Object[] getMessageContext()
	{
		return mMgCntx;
	}
	
	/** @see java.lang.Throwable#printStackTrace()
	 */
	@Override
	public void printStackTrace()
	{
		if (getCause() == null)
		{
			super.printStackTrace();
		}
		else
		{
			getCause().printStackTrace();
		}
	}

}