package com.tru.common.cml;

/**
 * The Class SystemException.
 *
 *  @version 1.0
 *  @author Professional Access
 */
public class SystemException extends BaseException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6221315449705875058L;
	
	/**
	 * Instantiates a new system exception.
	 *
	 * @param pErrorId - String
	 * @param pMsgCntx - Object[]
	 * @param pCause - Throwable
	 */
	public SystemException(String pErrorId, Object[] pMsgCntx, Throwable pCause) 
	{
		super(pErrorId, pMsgCntx, pCause);
	}

	/**
	 * Instantiates a new system exception.
	 *
	 * @param pErrorId - String
	 */
	public SystemException(String pErrorId)
	{
		super(pErrorId, null, null);
	}

	/**
	 * Instantiates a new system exception.
	 *
	 * @param pErrorId - String
	 * @param pMsgCntx - Object[]
	 */
	public SystemException(String pErrorId, Object[] pMsgCntx)
	{
		super(pErrorId, pMsgCntx, null);
	}

	/**
	 * Instantiates a new system exception.
	 *
	 * @param pErrorId - String
	 * @param pCause - Throwable
	 */
	public SystemException(String pErrorId, Throwable pCause)
	{
		super(pErrorId, null, pCause);
	}

}