package com.tru.common.cml;

/**
 * The Class ValidationError.
 *
 *  @version 1.0
 *  @author Professional Access
 */
public class ValidationError {

	/** The m error id. */
	private String mErrorId;
	
	/** The m message cntx. */
	private Object[] mMessageCntx = null;
	
	/**
	 * Instantiates a new validation error.
	 *
	 * @param pErrorId - String
	 */
	public ValidationError(String pErrorId)
	{
		this.mErrorId = pErrorId;
	}

	/**
	 * Instantiates a new validation error.
	 *
	 * @param pErrorId - String
	 * @param pMsgCntx - Object[]
	 */
	public ValidationError(String pErrorId, Object[] pMsgCntx)
	{
		this.mErrorId = pErrorId;
		mMessageCntx = pMsgCntx;
	}	

	/**
	 * This method returns ErrorId.
	 *
	 * @return mErrorId - String
	 */
	public String getErrorId()
	{
		return mErrorId;
	}

	/**
	 * This method returns Message Context.
	 *
	 * @return mMessageCntx - Object array
	 */
	public Object[] getMessageContext()
	{
		return mMessageCntx ;
	}	

}