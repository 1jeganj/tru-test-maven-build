package com.tru.common.cml;

import java.util.LinkedList;
import java.util.List;

/**
 * The Class ValidationExceptions.
 *
 *  @version 1.0
 *  @author Professional Access
 */
public class ValidationExceptions extends BaseException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4918816536919157443L;
	
	/**
	 * Property to hold ValidationErrors.
	 */
	protected List<ValidationError> mValidationErrors;

	/**
	 * constructor.
	 */
	public ValidationExceptions()
	{
		super(Constants.ERROR_ID_00000, null, null);
		mValidationErrors = new LinkedList<ValidationError>();
	}
	
	/**
	 * Gets the validation errors.
	 *
	 * @return the validationErrors
	 */
	public List<ValidationError> getValidationErrors()
	{
		return mValidationErrors;
	}
	
	/**
	 * Checks for validation errors.
	 *
	 * @return the validationErrors
	 */
	public boolean hasValidationErrors()
	{
		return (mValidationErrors.isEmpty()) ? false : true;
	}

	/**
	 * Adds the validation error.
	 *
	 * @param pErrorId - String
	 * @param pMsgCntx - Object[]
	 */
	public void addValidationError(String pErrorId, Object[] pMsgCntx)
	{
		mValidationErrors.add(new ValidationError(pErrorId, pMsgCntx));
	}

}