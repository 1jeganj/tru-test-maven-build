package com.tru.common.cml;

/**
 * The Interface IErrorId.
 *
 *  @version 1.0
 *  @author Professional Access
 */
public class IErrorId {
	
	/** constant for component_initilization_error. */
	public final static String COMPONENT_INITIALIZATION_ERROR = "component_initilization_error";
	
	/** constant for invalid_resource_path. */
	public final static String INVALID_RESOURCE_PATH = "invalid_resource_path";
	
	/** constant for unable_to_load_resource. */
	public final static String UNABLE_TO_LOAD_RESOURCE = "unable_to_load_resource";
	
	/** constant for NO_EXCEPTION_OR_HANDLER. */
	public final static String NO_EXCEPTION_OR_HANDLER = "No_Exception_Provided";
	
	/** constant for validator error. */
	public final static String VALIDATOR_ERROR = "validator error";
	
	/** constant for UNABLE_TO_GET_THEITEM. */
	public final static String UNABLE_TO_GET_THE_ITEM = "Unable to get the item from Repository";
	
	/** constant for INSTANTIATION_ERROR. */
	public final static String INSTANTIATION_ERROR = "unable_to_create_instance";
	
	/** constant for CLASS_NOT_FOUND_EXCEPTION. */
	public final static String CLASS_NOT_FOUND_EXCEPTION = "no_class_found_exception";
	
	/** constant for validator error. */
	public final static String NOSUCHMETHOD_EXCEPTION = "nosuchmethod_exception";
	
	/** constant for INVOCATIONTARGET_EXCEPTION. */
	public final static String INVOCATIONTARGET_EXCEPTION = "invocationTargetException_occured";
	
	/** constant for ILLEGALACCESS_EXCEPTION. */
	public final static String ILLEGALACCESS_EXCEPTION = "illegalAccessException_occured";
	
	/** constant for INVALID_PROCESSORKEY. */
	public final static String INVALID_PROCESSORKEY = "invalid_processorkey";
	
	/** constant for KEY_NOT_FOUND. */
	public final static String KEY_NOT_FOUND = " Not Found in Repository";
	
	/** constant for FILE_NOT_FOUND. */
	public final static String FILE_NOT_FOUND = "file_not_found";
	
	/** constant for CANNOT_READ_FILE. */
	public final static String CANNOT_READ_FILE = "cannot_read_from_file";
	
	/**
	 *  Constant to hold the value of UNABLE_TO_ADD_ITEMS_TO_JSONOBJECT
	 */
	public static final String UNABLE_TO_ADD_ITEMS_TO_JSONOBJECT = "Unable to add items to JSONObject";
	
	/**
	 *  Constant to hold the value of PAYPAL_EXCEPTION
	 */
	public static final String PAYPAL_EXCEPTION = "Exception Occured While processing Paypal.Please check.";
	
	/**
	 *  Constant to hold the value of UNSUPPORTED_ENCODING_EXCEPTION
	 */
	public static final String UNSUPPORTED_ENCODING_EXCEPTION = "Exception occured while Encoding/Decoding Request";
}