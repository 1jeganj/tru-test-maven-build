package com.tru.common.mgl;

import java.util.Map;

/**
 * This is wrapper class for holding some properties.
 * @author PA
 *
 */

public class CommonItem {
	/**
	 * Used to holds the mKeyName.
	 */
	private String mKeyName;
	
	/**
	 * Used to holds the mType.
	 */
	private String mType;
	
	/**
	 * Used to holds the mProperties.
	 */
	private Map<String, Object> mProperties;
	
	/**
	 * Used to holds the mPropertyName.
	 */
	private String mPropertyName;

	/**
	 * @return the propertyName
	 */
	public String getPropertyName() {
		return mPropertyName;
	}

	/**
	 * @param pPropertyName the propertyName to set
	 */
	public void setPropertyName(String pPropertyName) {
		mPropertyName = pPropertyName;
	}

	/**
	 * @return the keyName
	 */
	public String getKeyName() {
		return mKeyName;
	}

	/**
	 * @param pKeyName the keyName to set
	 */
	public void setKeyName(String pKeyName) {
		mKeyName = pKeyName;
	}

	/**
	 * @return the properties
	 */
	public Map<String, Object> getProperties() {
		return mProperties;
	}

	/**
	 * @param pProperties the properties to set
	 */
	public void setProperties(Map<String, Object> pProperties) {
		mProperties = pProperties;
	}

	/**
	 * @return the keyType
	 */
	public String getType() {
		return mType;
	}

	/**
	 * @param pType the Type to set
	 */
	public void setType(String pType) {
		mType = pType;
	}
	
}
