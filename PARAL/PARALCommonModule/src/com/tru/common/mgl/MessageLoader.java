package com.tru.common.mgl;

import atg.nucleus.GenericService;

/**
 * This class is common Loader class to load Error Messages or Resource bundle messages in to 
 * respective repositories.
 *  @author Professional Access
 *  @version 1.0
 *
 */
public class MessageLoader extends GenericService{
	
	/** Used to hold the mFeedProcessor. */
	private MessageFeedProcessor mFeedProcessor;

	/** Holds the mCsvFilePath object. */
	private String mCsvFilePath;

	/**
	 * Gets the csv file path.
	 *
	 * @return the CsvFilePath
	 */
	public String getCsvFilePath() {
		return mCsvFilePath;
	}

	/**
	 * Sets the csv file path.
	 *
	 * @param pCsvFilePath the CsvFilePath to set
	 */
	public void setCsvFilePath(String pCsvFilePath) {
		mCsvFilePath = pCsvFilePath;
	}
		
	/**
	 * Gets the Message feed processor.
	 *
	 * @return the MessageFeedProcessor
	 */
	public MessageFeedProcessor getFeedProcessor() {
		return mFeedProcessor;
	}

	/**
	 * Sets the Message feed processor.
	 *
	 * @param pFeedProcessor the  MessageFeedProcessor to set
	 */
	public void setFeedProcessor(
			MessageFeedProcessor pFeedProcessor) {
		mFeedProcessor = pFeedProcessor;
	}
	
	/**
	 * This method is used to load the error message or resource bundle messages in to respective 
	 * repositories by using Feed in BCC.
	 */
	public void loadMessages()
	{
		if(isLoggingDebug()){
			logDebug("MessageLoader - (loadMessages-[]):Start");
		}
		getFeedProcessor().processFeedJob(getCsvFilePath());
		if(isLoggingDebug()){
			logDebug("MessageLoader - (loadMessages-[]):Start");
		}
	}

	/**
	 * This method is used to clear the data from repository.
	 */
	public void clearData() {
		if(isLoggingDebug()){
			logDebug("MessageLoader - (clearStoreData-[]):Start");
		}
		getFeedProcessor().getRepositoryTools().clearData();
		
		if(isLoggingDebug()){
			logDebug("MessageLoader - (clearStoreData-[]):Start");
		}

	}
}
