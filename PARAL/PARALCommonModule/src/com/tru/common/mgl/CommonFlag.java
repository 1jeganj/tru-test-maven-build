package com.tru.common.mgl;

/**
 * This is wrapper class for holding boolean flag.
 * @author PA
 */
public class CommonFlag {
	/**
	 * This property is used to hold the isTrue value.
	 * 
	 */
	private boolean mIsTrue;

	/**
	 * This is getter method for isTrue.
	 * @return isTrue boolean value
	 */
	public boolean isTrue() {
		return mIsTrue;
	}

	/**
	 * This is setter method for isTrue.
	 * @param pIsTrue boolean value
	 */
	public void setTrue(boolean pIsTrue) {
		this.mIsTrue = pIsTrue;
	}
	
	/**
	 * toString method override.
	 * @return pIsTrue - boolean value
	 */
	public String toString() {
		return Boolean.toString(this.mIsTrue);
	}
}
