package com.tru.common.mgl;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import au.com.bytecode.opencsv.CSVReader;

import com.tru.common.cml.IErrorId;
import com.tru.common.cml.SystemException;
import com.tru.common.tol.RepositoryTools;
/**
 * This class is common Loader class to load Error Messages or Resource bundle messages in to 
 * respective repositories.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class MessageFeedProcessor extends GenericService{
	
	/**
	 *  Used to hold the mRepositoryTools. 
	 */
	private RepositoryTools mRepositoryTools;
	
	/**
	 * Gets the repository tools.
	 *
	 * @return the mRepositoryTools
	 */
	public RepositoryTools getRepositoryTools() {
		return mRepositoryTools;
	}
	
	/**
	 * Sets the repository tools.
	 *
	 * @param pRepositoryTools the RepositoryTools to set
	 */
	public void setRepositoryTools(RepositoryTools pRepositoryTools) {
		mRepositoryTools = pRepositoryTools;
	}
	
	/**
	 * This method is used to read the input csv file and to create Items
	 *
	 * @param pInputCSVFile - String
	 * @return list of  Item
	 * @throws SystemException -  when error occurs
	 */
	public List<CommonItem> processCSVFile(String pInputCSVFile) throws SystemException{
		FileReader inputFile=null;
		String[] columnNames = null;
		String[]row;
		List<CommonItem> itemList = new ArrayList<CommonItem>();
		try {
			inputFile = new FileReader(pInputCSVFile);
			
		} catch (FileNotFoundException fe) {
			throw new SystemException(IErrorId.FILE_NOT_FOUND,fe);
		}
		
		CSVReader inputFileCSV = new CSVReader(inputFile);
		try {
			columnNames = inputFileCSV.readNext();
			while((row = inputFileCSV.readNext())!=null)
			{	
				Map<String, Object> properties = new HashMap<String, Object>();
				CommonItem rowItem = new CommonItem();
				rowItem.setKeyName(row[0]);
				for(int i=0; i < row.length;i++){
					properties.put(columnNames[i], row[i]);
				}
				rowItem.setProperties(properties);
				itemList.add(rowItem);
			}
		} catch (IOException ie) {
			if(isLoggingError()){
				throw new SystemException(IErrorId.CANNOT_READ_FILE,ie);
		}
		}
		finally{
			if(inputFileCSV != null){
				try {
						inputFileCSV.close();
					} catch (IOException e) {
						if(isLoggingError()){
							logError("TO Exception occured while closing the file", e);
				}
				}
			inputFileCSV=null;
			}
		}
		return itemList;
	}
	
	/**
	 * This method is used to process the csv file.
	 *
	 * @param pFilePath - String
	 */
	public void processFeedJob(String pFilePath)
	{
		List<CommonItem> itemsList = null;
		try{
			//startProjectInBCC();
			if(StringUtils.isBlank(pFilePath)){
				throw new SystemException(IErrorId.FILE_NOT_FOUND);
			}else{	
				itemsList = processCSVFile(pFilePath);
			
				for(CommonItem item : itemsList){
					mRepositoryTools.processItemInTransaction(item);
				}
			}
		} catch(SystemException se){
			if(isLoggingError()){
				logError(se.getErrorId(),se.getCause());
			}
		}
	}
}
