package com.tru.common.tol;


import atg.core.util.StringUtils;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;

import com.tru.common.cml.Constants;
import com.tru.common.cml.ResourceBundleConfiguration;

/**
 * The class extends AbstractProcessResourceBundle. This class has method to get ErrorMessage from ErrorHandler Repository or keyValue from 
 * Resource bundle repository based on Site.
 * 
 *  @author Professional Access
 *  @version 1.0
 */

public class ProcessResourceBundleSiteImpl extends AbstractProcessResourceBundle {
	
	/**
	 * Constructor.
	 *
	 * @param pRepository Repository
	 * @param pPropertyManager PropertyManager
	 * @param pConfiguration Configuration
	 */ 
	public ProcessResourceBundleSiteImpl(Repository pRepository,CommonPropertyManager pPropertyManager,ResourceBundleConfiguration pConfiguration){
		super(pRepository,pPropertyManager,pConfiguration);
	}
	
	/**
	 * This method is used to perform the following operations:
	 * 1) If siteId is null, append siteId with Key
	 * 2) Get the default SiteId from Configuration file and append it with Key
	 * 3) If siteId and defaultSiteId not null, invoke getSiteMessage(siteKey,DefaultSiteKey) method
	 * 4) If more than one RepositoryItem found, get the siteId from key by splitting it with delimeter "_" and compare 
	 * with pSiteId.
	 * 5)If pSiteId is equals to any of the siteIds, than retrieve that errorMessage/keyvalue.
	 * @param pKeyName KeyName
	 * @param pSiteId SiteKey
	 * @return ErrorMessage
	 * @throws RepositoryException - when error occurs
	 */
	public String getKeyValue(String pKeyName, String pSiteId) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("ProcessErrorMessageSiteImpl (getErrorMessage[pErrorKey,pSiteKey]) : BEGIN");
			logDebug("ErrorKey = " + pKeyName + "Site Key = " + pSiteId);
		}
		RepositoryItem[] keyValue = null;
		String finalKeyMessage = null;
		String defaultSiteId = null;
		if(getConfiguration() != null){
			defaultSiteId = getConfiguration().getDefaultSiteId();
		}
		if(StringUtils.isBlank(pKeyName)){
			return null;
		}
		String siteErrorId = null;
		if(!StringUtils.isBlank(pSiteId)){
			StringBuffer siteKey = new StringBuffer();
			siteErrorId = siteKey.append(pSiteId).append(Constants.UNDERSCORE).append(pKeyName).toString();
		}
		String defaultSiteErrorId = null;
		if(!StringUtils.isBlank(defaultSiteId)){
			StringBuffer defaulKey = new StringBuffer();
			defaultSiteErrorId = defaulKey.append(defaultSiteId).append(Constants.UNDERSCORE).append(pKeyName).toString();
		}
		if(!StringUtils.isBlank(siteErrorId) && !StringUtils.isBlank(defaultSiteErrorId)){
			if(siteErrorId.equals(defaultSiteErrorId)){
				keyValue = getSiteMessage(siteErrorId);
			} else{
				keyValue = getSiteMessage(siteErrorId,defaultSiteErrorId);
			}			
		} else if(!StringUtils.isBlank(siteErrorId) && (StringUtils.isBlank(defaultSiteErrorId))) {
			keyValue = getSiteMessage(siteErrorId);

		} else if(StringUtils.isBlank(siteErrorId) && !StringUtils.isBlank(defaultSiteErrorId)){
			keyValue = getSiteMessage(defaultSiteErrorId);
		} 
		// If errorMessage Repository Item returns more than on errorMessage, split the Errorkey and compare it with siteId.
		if ((keyValue != null) && (keyValue.length > Constants.VALUE_ZERO)) {
			if (keyValue.length > Constants.VALUE_ONE) {
				String lFirstKeyStr = (String) keyValue[Constants.VALUE_ZERO].getPropertyValue(getPropertyManager().getKeyNamePropertyName());
				//if(lFirstKeyStr!=null){
				//	lFirstKeyStr=lFirstKeyStr.toString();
				//}
				String lSecondKeyStr = (String) keyValue[Constants.VALUE_ONE].getPropertyValue(getPropertyManager().getKeyNamePropertyName());
				//if(lSecondKeyStr!=null){
				//	lSecondKeyStr=lSecondKeyStr.toString();
				//}
				String firstSiteId = null;
				String secondSiteId = null;
				if(!StringUtils.isBlank(lFirstKeyStr)){
					String[] lToken = lFirstKeyStr.split(Constants.UNDERSCORE);
					firstSiteId = lToken[0];
				} 
				if(!StringUtils.isBlank(lSecondKeyStr)){
					String token[] = lSecondKeyStr.split(Constants.UNDERSCORE);
					secondSiteId = token[0];
				}
				if(firstSiteId.equals(pSiteId)){
					finalKeyMessage = (String) keyValue[Constants.VALUE_ZERO].getPropertyValue(getPropertyManager().getKeyMessagePropertyName());
					// if(finalKeyMessage!=null){
					//	finalKeyMessage=finalKeyMessage.toString();
					// }
				} else if(secondSiteId.equals(pSiteId)){
					finalKeyMessage = (String) keyValue[Constants.VALUE_ONE].getPropertyValue(getPropertyManager().getKeyMessagePropertyName());
					// if(finalKeyMessage!=null){
					//	finalKeyMessage=finalKeyMessage.toString();
					// }
				}
			} else {
				finalKeyMessage = (String) keyValue[Constants.VALUE_ZERO].getPropertyValue(getPropertyManager().getKeyMessagePropertyName());
				// if(finalKeyMessage!=null){
				//	finalKeyMessage=finalKeyMessage.toString();
				// }
			}
		} else{
			keyValue = getSiteMessage(pKeyName);
			if(keyValue != null && keyValue.length > 0){
				finalKeyMessage = (String) keyValue[Constants.VALUE_ZERO].getPropertyValue(getPropertyManager().getKeyMessagePropertyName());
				// if(finalKeyMessage!=null && !finalKeyMessage.isEmpty()){
				//	finalKeyMessage = finalKeyMessage.toString();
				// }
			}
		}
		if (isLoggingDebug()) {
			logDebug("ProcessErrorMessageSiteImpl (getErrorMessage[pErrorKey,pSiteKey]) : END");
		}	
		return finalKeyMessage;		
	}
	/**
	 * This method is used to get the ErrorMessage with the given ErrorKey.
	 * @param pErrorKey ErrorKey
	 * 
	 * @return errorTextMessageItems - RepositoryItem[]
	 * @throws RepositoryException - when error occurs
	 */
	private RepositoryItem[] getSiteMessage(String pErrorKey) throws RepositoryException{
		if (isLoggingDebug()) {
			logDebug("ProcessErrorMessageSiteImpl (getSiteErrorMessage[pErrorKey]) : BEGIN");
		}
		RepositoryItemDescriptor errorTextMessageItemDesc = getRepository().getItemDescriptor(getPropertyManager().getRepositoryItemDescPropertyName());
		//Get the Repository View and create Query Builder
		RepositoryView errorTextMessageRepositoryView = errorTextMessageItemDesc.getRepositoryView();
		QueryBuilder oQueryBuilder = errorTextMessageRepositoryView.getQueryBuilder();
		//constructing query errorKey = pErrorKey
		QueryExpression oKeyQE1 = oQueryBuilder.createPropertyQueryExpression(getPropertyManager().getKeyNamePropertyName());
		QueryExpression oKeyValueQE1 = oQueryBuilder.createConstantQueryExpression(pErrorKey); 
		Query oQuery1 = oQueryBuilder.createComparisonQuery(oKeyQE1, oKeyValueQE1, QueryBuilder.EQUALS);
		RepositoryItem[] errorTextMessageItems = errorTextMessageRepositoryView.executeQuery(oQuery1);
		if (isLoggingDebug()) {
			logDebug("ProcessErrorMessageSiteImpl (getSiteErrorMessage[pErrorKey]) : END");
		}
		return errorTextMessageItems;
		
	}
	/**
	 * This method is used to get the ErrorMessage with the given siteId and DefaultSiteId. This method constructs OR query and
	 * retrieves the Repository Items
	 * @param pSiteErrorId SiteErrorKey
	 * @param pDefaultSiteId DefaultSiteErrorKey
	 * @return errorTextMessageItems - RepositoryItem[]
	 * @throws RepositoryException - when error occurs
	 */
	private RepositoryItem[] getSiteMessage(String pSiteErrorId,String pDefaultSiteId) throws RepositoryException{
		if (isLoggingDebug()) {
			logDebug("ProcessErrorMessageSiteImpl (getSiteErrorMessage[pSiteErrorId,pDefaultSiteId]) : BEGIN");
		}
		RepositoryItem[] errorMessage = null;
		//Get the RepositoryItemDescriptor 
		RepositoryItemDescriptor errorTextMessageItemDesc = getRepository().getItemDescriptor(getPropertyManager().getRepositoryItemDescPropertyName());
		//Get the Repository View and create Query Builder
		RepositoryView errorTextMessageRepositoryView = errorTextMessageItemDesc.getRepositoryView();
		QueryBuilder queryBuilder = errorTextMessageRepositoryView.getQueryBuilder();
		//constructing query errorKey = pErrorKey
		QueryExpression keyQE1 = queryBuilder.createPropertyQueryExpression(getPropertyManager().getKeyNamePropertyName());
		QueryExpression keyValueQE1 = queryBuilder.createConstantQueryExpression(pSiteErrorId);
		Query query1 = queryBuilder.createComparisonQuery(keyQE1, keyValueQE1, QueryBuilder.EQUALS);
		//constructing query errorKey = pSiteKey
		QueryExpression keyQE2 = queryBuilder.createPropertyQueryExpression(getPropertyManager().getKeyNamePropertyName());
		QueryExpression keyValueQE2 = queryBuilder.createConstantQueryExpression(pDefaultSiteId);
		Query query2 = queryBuilder.createComparisonQuery(keyQE2, keyValueQE2, QueryBuilder.EQUALS);
		Query[] queryElements = {query1, query2};
		//Create Or Query and execute
		Query queryOr = queryBuilder.createOrQuery(queryElements);
		errorMessage = errorTextMessageRepositoryView.executeQuery(queryOr);
		if (isLoggingDebug()) {
			logDebug("ProcessErrorMessageSiteImpl (getSiteErrorMessage[pSiteErrorId,pDefaultSiteId]) : END");
		}
		return errorMessage;
	}
	
}
