package com.tru.common.tol;

/**
 * The Class CommonPropertyManager.
 *  @version 1.0 
 *  @author Professional Access
 *
 */
public class CommonPropertyManager {
		
	/** Holds the Constant of mKeyNamePropertyName. */
	private String mKeyNamePropertyName;	
	
	/** Holds the Constant of mKeyMessagePropertyName. */
	private String mKeyMessagePropertyName;
	
	/** Holds the Constant of mRepositoryItemDescPropertyName. */
	private String mRepositoryItemDescPropertyName;
	
	/** Holds the Constant of mTranslationItemDescPropertyName. */
	private String mTranslationItemDescPropertyName;

	/** Holds the Constant of mTranslationPropertyName. */
	private String mTranslationPropertyName;
	
	/** Holds the Constant of mPropertyName. */
	private String mPropertyName;
			
	/**
	 * Gets the property name.
	 *
	 * @return the propertyName
	 */
	public String getPropertyName() {
		return mPropertyName;
	}

	/**
	 * Sets the property name.
	 *
	 * @param pPropertyName the propertyName to set
	 */
	public void setPropertyName(String pPropertyName) {
		mPropertyName = pPropertyName;
	}

	/**
	 * Gets the translation property name.
	 *
	 * @return the translationPropertyName
	 */
	public String getTranslationPropertyName() {
		return mTranslationPropertyName;
	}

	/**
	 * Sets the translation property name.
	 *
	 * @param pTranslationPropertyName the translationPropertyName to set
	 */
	public void setTranslationPropertyName(String pTranslationPropertyName) {
		mTranslationPropertyName = pTranslationPropertyName;
	}

	/**
	 * @return the keyNamePropertyName
	 */
	public String getKeyNamePropertyName() {
		return mKeyNamePropertyName;
	}

	/**
	 * @param pKeyNamePropertyName the keyNamePropertyName to set
	 */
	public void setKeyNamePropertyName(String pKeyNamePropertyName) {
		mKeyNamePropertyName = pKeyNamePropertyName;
	}

	/**
	 * @return the keyMessagePropertyName
	 */
	public String getKeyMessagePropertyName() {
		return mKeyMessagePropertyName;
	}

	/**
	 * @param pKeyMessagePropertyName the keyMessagePropertyName to set
	 */
	public void setKeyMessagePropertyName(String pKeyMessagePropertyName) {
		mKeyMessagePropertyName = pKeyMessagePropertyName;
	}

	/**
	 * @return the repositoryItemDescPropertyName
	 */
	public String getRepositoryItemDescPropertyName() {
		return mRepositoryItemDescPropertyName;
	}

	/**
	 * @param pRepositoryItemDescPropertyName the repositoryItemDescPropertyName to set
	 */
	public void setRepositoryItemDescPropertyName(
			String pRepositoryItemDescPropertyName) {
		mRepositoryItemDescPropertyName = pRepositoryItemDescPropertyName;
	}

	/**
	 * @return the translationItemDescPropertyName
	 */
	public String getTranslationItemDescPropertyName() {
		return mTranslationItemDescPropertyName;
	}

	/**
	 * @param pTranslationItemDescPropertyName the translationItemDescPropertyName to set
	 */
	public void setTranslationItemDescPropertyName(
			String pTranslationItemDescPropertyName) {
		mTranslationItemDescPropertyName = pTranslationItemDescPropertyName;
	}	
}
