package com.tru.common.tol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.TransactionManager;

import atg.adapter.gsa.GSAItemDescriptor;
import atg.adapter.gsa.Utils;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;

import com.tru.common.cml.Constants;
import com.tru.common.cml.SystemException;
import com.tru.common.mgl.CommonFlag;
import com.tru.common.mgl.CommonItem;

/**
 * The Class RepositoryTools.
 *  @author Professional Access
 *  @version 1.0
 */
public class RepositoryTools extends GenericService{
	
	/** Property to hold mTransactionManager. */
	protected TransactionManager mTransactionManager;
	
	/** Holds the Constant of mRepository. */
	private MutableRepository mRepository;
	
	/** Property to hold mItemDescriptorName 	 *. */
	private List<String> mItemDescriptorList;
	
	/** Holds the Constant of mPropertyManager. */
	private CommonPropertyManager mPropertyManager;
	
	/**
	 * Gets the item descriptor list.
	 *
	 * @return the itemDescriptorList
	 */
	public List<String> getItemDescriptorList() {
		return mItemDescriptorList;
	}
	
	/**
	 * Sets the item descriptor list.
	 *
	 * @param pItemDescriptorList the itemDescriptorList to set
	 */
	public void setItemDescriptorList(List<String> pItemDescriptorList) {
		mItemDescriptorList = pItemDescriptorList;
	}
	
	/**
	 * Getter method for mTransactionManager.
	 *
	 * @return TransactionManager : mTransactionManager
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}
	
	/**
	 * Setter method for mTransactionManager.
	 *
	 * @param pTransactionManager : TransactionManager to set value to  mTransactionManager
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}
	
	/**
	 * Gets the repository.
	 *
	 * @return the Repository
	 */
	public MutableRepository getRepository() {
		return mRepository;
	}

	/**
	 * Sets the repository.
	 *
	 * @param pRepository the Repository to set
	 */
	public void setRepository(MutableRepository pRepository) {
		mRepository = pRepository;
	}

	/**
	 * Gets the property manager.
	 *
	 * @return the propertyManager
	 */
	public CommonPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(CommonPropertyManager pPropertyManager){
		mPropertyManager = pPropertyManager;
	}
	
	/**
	 * This method invokes processItem() method in Transaction.
	 *
	 * @param pItem - Item
	 * @throws SystemException - when error occurs
	 */
	public void processItemInTransaction(CommonItem pItem)throws SystemException{
		
		TransactionDemarcation lTransactionDemarcation = new TransactionDemarcation();
		boolean lRollbackFlag = false;
		if(mTransactionManager != null) {
			try {
				lTransactionDemarcation.begin(mTransactionManager, TransactionDemarcation.REQUIRES_NEW);
				processItem(pItem);
			} catch (TransactionDemarcationException transactionDemarcationException) {
				lRollbackFlag = true;
				throw new SystemException(Constants.TRANSACTION_EXCEPTION_MSG, transactionDemarcationException);
			}   finally {
				try {
					if(mTransactionManager != null) {
						lTransactionDemarcation.end(lRollbackFlag);
					}
				} catch(TransactionDemarcationException transactionDemarcationException) {
					if (isLoggingError()) {
						logError(Constants.TRANSACTION_EXCEPTION_MSG , transactionDemarcationException);
					}
				}
			}
		}
	}
	
	/**
	 * This method gets the RepositoryItem for update and set properties of Repository Item from Item.
	 * @param pItem - Item
	 * @return RepositoryItem
	 * @throws SystemException - when error occurs
	 */
	private RepositoryItem processItem(CommonItem pItem)throws SystemException{
		RepositoryItem repositoryItem = null;
		MutableRepositoryItem handlerItem = null;
		CommonFlag isNew = new CommonFlag();
		if(pItem == null){
			throw new SystemException(Constants.SYSTEM_EXPECTION_NO_DATA);
		}
		try{			
			handlerItem = getProcessItemForUpdate(pItem.getKeyName(), isNew);
			
			if(handlerItem != null){
				handlerItem = setPropertiesToItem(handlerItem,pItem);
			}
			repositoryItem = updateItem(handlerItem, isNew);
		
		} catch(RepositoryException re){
			throw new SystemException(re.getMessage(), re);
		}
		
		return repositoryItem;
	}
	
	/**
	 * This method is used to add/update the RepositoryItem based on the Flag.
	 * @param pItem - MutableRepositoryItem
	 * @param pIsNew - Flag
	 * @return RepositoryItem
	 * @throws SystemException - when error occurs
	 */
	private RepositoryItem updateItem(MutableRepositoryItem pItem, CommonFlag pIsNew) throws SystemException{
		RepositoryItem repositoryItem = null;
		if(pItem != null &&  pIsNew != null){
			try{
				if(pIsNew.isTrue()){
					repositoryItem = getRepository().addItem(pItem);
					
				} else{
					getRepository().updateItem(pItem);
				}
			} catch(RepositoryException re){
				throw new SystemException(re.getMessage(), re);
			}
			
		}
		
		return repositoryItem;
	}
	
	/**
	 * This method is used to set the properties to Handler Repository item.
	 *
	 * @param pHandlerItem - MutableRepositoryItem
	 * @param pItem - Item
	 * @return MutableRepositoryItem
	 * @throws SystemException - when error occurs
	 */
	private MutableRepositoryItem setPropertiesToItem(MutableRepositoryItem pHandlerItem, CommonItem pItem) throws SystemException{
		Map<String, Object> properties = null;
		Set<String> keys = null;
		Iterator<String> keyIterator = null;
		String key = null;
		Object value = null;
		if(pItem != null && pHandlerItem != null){
			properties = pItem.getProperties();
			if(properties != null && !(properties.isEmpty())){
				keys = properties.keySet();
				if(keys != null && !(keys.isEmpty())){
					keyIterator = keys.iterator();
					while(keyIterator.hasNext()){
						key = keyIterator.next();
						value = properties.get(key);
						if(key.equalsIgnoreCase(Constants.TRANSLATIONS)){
							setTranslationValues(pHandlerItem,value);
						} else{
							setPropertyValues(pHandlerItem,key, value);
						}
					}
				}
				
			}
		}
		
		return pHandlerItem;
	}
	
	/**
	 * This method is used to set the translation values by passing the key,value,Item.
	 *
	 * @param pHandlerItem - MutableRepositoryItem
	 * @param pValue - Object
	 * @throws SystemException - when error occurs
	 */
	private void setTranslationValues(MutableRepositoryItem pHandlerItem, Object pValue) throws SystemException{
		Map<String,RepositoryItem> translationMap = new HashMap<String, RepositoryItem>();
		RepositoryItem translaionItem = null;
		if(pValue instanceof String){
			String[] translations = ((String)pValue).split(Constants.COMMA);
			if(translations != null && translations.length >Constants.VALUE_ZERO){
				for(String translator : translations){
					String[] translatorKeys = translator.split(Constants.EQUALS);
					if(translatorKeys != null && translatorKeys.length > Constants.VALUE_ONE){
						translaionItem = getTranslatorItem(translatorKeys[Constants.VALUE_ONE]);
						translationMap.put(translatorKeys[0], translaionItem);
					}
				}
			}
			pHandlerItem.setPropertyValue(getPropertyManager().getTranslationPropertyName(), translationMap);
		}
		if(isLoggingDebug()){
			logDebug("Exit RepositoryTools.setTranslationValues()");
		}
	}
	
	/**
	 * This method is used to get the translator the item by using value.
	 *
	 * @param pValue - String
	 * @return RepositoryItem
	 * @throws SystemException - when error occurs
	 */
	private RepositoryItem getTranslatorItem(String pValue) throws SystemException
	{
		RepositoryItem transltorItem = null;
		try{
			MutableRepositoryItem translationItem = getRepository().createItem(getPropertyManager().getTranslationItemDescPropertyName());
		
				setPropertyValues(translationItem,getPropertyManager().getKeyMessagePropertyName(), pValue);
				
				transltorItem = getRepository().addItem(translationItem);
			} catch(RepositoryException re){
				throw new SystemException(Constants.REPOSITORY_EXCEPTION,re);
			}
		return transltorItem;
		
	}
	
	/**
	 * This method is set the Repository Item property values.
	 *
	 * @param pRepositoryItem - MutableRepositoryItem
	 * @param pKey - String
	 * @param pValue - Object
	 */
	private void setPropertyValues(MutableRepositoryItem pRepositoryItem, String pKey, Object pValue){
		if (isLoggingDebug()) {
			logDebug("Entering into RepositoryTools setPropertyValues() method");
		}
		if(pRepositoryItem != null){
			getPropertyManager().setPropertyName(pKey);
			pRepositoryItem.setPropertyValue(getPropertyManager().getPropertyName(), pValue);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from RepositoryTools setPropertyValues() method");
		}
	}
	
	/**
	 * This method is used to get the Repository Item for update.If RepositoryItem is null create new Item.
	 * @param pKeyName - String
	 * @param pIsNew - Flag
	 * @return MutableRepositoryItem
	 * @throws RepositoryException - when error occurs
	 * @throws SystemException - when error occurs
	 */
	private MutableRepositoryItem getProcessItemForUpdate(String pKeyName, CommonFlag pIsNew) throws RepositoryException, SystemException{
		RepositoryItem repositoryItem = null;
		MutableRepositoryItem mutableItem = null;
		if(StringUtils.isBlank(pKeyName)){
			throw new SystemException(Constants.NO_ERROR_KEY);
		} else{
			repositoryItem = getMessageItem(pKeyName);
			if(repositoryItem == null){
				mutableItem = getRepository().createItem(getPropertyManager().getRepositoryItemDescPropertyName());
				if (pIsNew != null) {
					pIsNew.setTrue(true);
				}
			} else{
				mutableItem = (MutableRepositoryItem)repositoryItem;
				if (pIsNew != null) {
					pIsNew.setTrue(false);
				}
			}
			
		}
		
		return mutableItem;
	}
	
	/**
	 * This method is used to get the handler item.
	 *
	 * @param pKeyName - String
	 * @return RepositoryItem
	 * @throws RepositoryException - when error occurs
	 */
	private RepositoryItem getMessageItem(String pKeyName) throws RepositoryException{
		RepositoryItem messageItem = null;
		RepositoryItemDescriptor messageItemDesc = getRepository().getItemDescriptor(getPropertyManager().getRepositoryItemDescPropertyName());
		RepositoryView repositoryView = messageItemDesc.getRepositoryView();
		QueryBuilder oQueryBuilder = repositoryView.getQueryBuilder();
		QueryExpression oKeyQE1 = oQueryBuilder.createPropertyQueryExpression(getPropertyManager().getKeyNamePropertyName());
		QueryExpression oKeyValueQE1 = oQueryBuilder.createConstantQueryExpression(pKeyName); 
		Query oQuery1 = oQueryBuilder.createComparisonQuery(oKeyQE1, oKeyValueQE1, QueryBuilder.EQUALS);
		RepositoryItem[] messageItems = repositoryView.executeQuery(oQuery1);
		if ((messageItems != null) && (messageItems.length > Constants.VALUE_ZERO)) {
			messageItem = messageItems[Constants.VALUE_ZERO];
		}
		return messageItem;
	}
	
	/**
	 * This method is used to clear the data from repository.
	 *
	 */
	public void clearData(){
		List<Repository> repositoryList = new ArrayList<Repository>();
		GSAItemDescriptor gsaItemDescriptor = null;
		TransactionDemarcation td = new TransactionDemarcation();
		TransactionManager transactionManager  =  getTransactionManager();
		boolean isRollBack = false;
		try{
			td.begin(transactionManager, TransactionDemarcation.REQUIRED);
			repositoryList.add((MutableRepository)getRepository());
			Iterator<String> itemsIterator  = getItemDescriptorList().iterator();
			MutableRepository repository = (MutableRepository) getRepository();
			while(itemsIterator.hasNext()){
				gsaItemDescriptor = (GSAItemDescriptor)repository.getItemDescriptor(itemsIterator.next());	
				Utils.deleteAllItems(gsaItemDescriptor, repositoryList, true);
				Utils.deleteAllItems(gsaItemDescriptor, repositoryList, false);
			}
			
		}catch (RepositoryException e) {
			if(isLoggingError()) {
				logError("RepositoryException in doFlush() of  CustomerDataFeedImportJob",e);
			}
			isRollBack = true;
		} catch (TransactionDemarcationException tde) {
			if(isLoggingError()) {
				logError("Transaction Exception while migration of data to ATG repository in doFlush() of  CustomerDataFeedImportJob",tde);
			}
			isRollBack = true;
		}finally{
			try {
				td.end(isRollBack);
			} catch (TransactionDemarcationException tde) {
				if(isLoggingError()) {
					logError("Transaction Exception to end the transaction successfully in doFlush() of  CustomerDataFeedImportJob",tde);
				}
			}
		}
	}
}
