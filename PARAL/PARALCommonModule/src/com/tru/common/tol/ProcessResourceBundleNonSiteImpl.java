package com.tru.common.tol;

import atg.core.util.StringUtils;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;

import com.tru.common.cml.Constants;
import com.tru.common.cml.ResourceBundleConfiguration;
/**
 * The class extends AbstractProcessResourceBundle. This class has method to get ErrorMessage from ErrorHandler Repository or keyValue from 
 * Resource bundle repository for Non site Implementation.
 * 
 *  @author Professional Access
 *  @version 1.0
 * 
 */
public class ProcessResourceBundleNonSiteImpl extends AbstractProcessResourceBundle{
	
	/**
	 * Instantiates a new process error message non site impl.
	 *
	 * @param pRepository Repository
	 * @param pPropertyManager PropertyManager
	 * @param pConfiguration Configuration
	 */ 
	public ProcessResourceBundleNonSiteImpl(Repository pRepository,CommonPropertyManager pPropertyManager,ResourceBundleConfiguration pConfiguration){
		super(pRepository,pPropertyManager,pConfiguration);
	}

	/**
	 * This method will get ErroMessage/KeyValue for the given  key.
	 * @param pKeyName KeyName
	 * @param pSiteId SiteId
	 * @return ErrorMessage
	 * @throws RepositoryException - when error occurs
	 */
	public String getKeyValue(String pKeyName, String pSiteId) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("ProcessErrorMessageNonSiteImpl (getErrorMessage[pErrorKey]) : BEGIN");
		}
		String keyValue = null;
		if (!StringUtils.isBlank(pKeyName)) {

			if (isLoggingDebug()) {
				logDebug("pSiteKey value in ProcessErrorMessageNonSiteImpl (getErrorMessage[pErrorKey]) is : "+pKeyName);
			}	
			//Get the RepositoryItemDescriptor 
			RepositoryItemDescriptor repositoryItemDesc = getRepository().getItemDescriptor(getPropertyManager().getRepositoryItemDescPropertyName());
			//Get the Repository View and create Query Builder
			RepositoryView repositoryView = repositoryItemDesc.getRepositoryView();
			QueryBuilder oQueryBuilder = repositoryView.getQueryBuilder();
			//constructing query errorKey = pErrorKey
			QueryExpression oKeyQE1 = oQueryBuilder.createPropertyQueryExpression(getPropertyManager().getKeyNamePropertyName());
			QueryExpression oKeyValueQE1 = oQueryBuilder.createConstantQueryExpression(pKeyName); 
			Query oQuery1 = oQueryBuilder.createComparisonQuery(oKeyQE1, oKeyValueQE1, QueryBuilder.EQUALS);
			RepositoryItem[] repositoryItems = repositoryView.executeQuery(oQuery1);
			//get the first reository item
			if ((repositoryItems != null) && (repositoryItems.length > Constants.VALUE_ZERO)) {
				keyValue = repositoryItems[Constants.VALUE_ZERO].getPropertyValue(getPropertyManager().getKeyMessagePropertyName()).toString();
			}
		}
		if (isLoggingDebug()) {
			logDebug("ProcessErrorMessageNonSiteImpl (getErrorMessage[pErrorKey]) : END");
		}
		return keyValue;
	}
}
