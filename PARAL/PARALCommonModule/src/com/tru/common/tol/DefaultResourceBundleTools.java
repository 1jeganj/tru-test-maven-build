package com.tru.common.tol;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;

import com.tru.common.cml.AbstractComponent;
import com.tru.common.cml.Constants;
import com.tru.common.cml.IErrorId;
import com.tru.common.cml.ResourceBundleConfiguration;
import com.tru.common.cml.SystemException;
/**
 * The class implements IResourceBundleTools and extends AbsractComponent.
 * This class overrides the initComponent() method for instantiating ProcessResourceBundleMessage class.Based on processorKeyName
 * it invokes appropriate IProcessReousrceBundleMessage class.
 * 
 *  @author Professional Access
 *  @version 1.0
 */

public class DefaultResourceBundleTools extends AbstractComponent implements IResourceBundleTools{

	/** Holds the Constant of mProcessorMap. */
	private Map<String, String> mProcessorMap;
	
	/** Holds the Constant of mRepository. */
	private Repository mRepository;
	
	/** Holds the Constant of mPropertyManager. */
	private CommonPropertyManager mPropertyManager;
	
	/** Holds the Constant of mProcessMessage. */
	protected IProcessResourceBundleMessage mProcessMessage;
	
	/** Holds the Constant of Configuration. */
	private ResourceBundleConfiguration mConfiguration;	

	/**
	 * @return the processorMap
	 */
	public Map<String, String> getProcessorMap() {
		return mProcessorMap;
	}

	/**
	 * @param pProcessorMap the processorMap to set
	 */
	public void setProcessorMap(Map<String, String> pProcessorMap) {
		mProcessorMap = pProcessorMap;
	}

	/**
	 * @return the repository
	 */
	public Repository getRepository() {
		return mRepository;
	}

	/**
	 * @param pRepository the repository to set
	 */
	public void setRepository(Repository pRepository) {
		mRepository = pRepository;
	}

	/**
	 * @return the processMessage
	 */
	public IProcessResourceBundleMessage getProcessMessage() {
		return mProcessMessage;
	}

	/**
	 * @param pProcessMessage the processMessage to set
	 */
	public void setProcessMessage(IProcessResourceBundleMessage pProcessMessage) {
		mProcessMessage = pProcessMessage;
	}

	/**
	 * Gets the property manager.
	 *
	 * @return the propertyManager
	 */
	public CommonPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(CommonPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}	
	
	/**
	 * Gets the configuration.
	 *
	 * @return the Configuration
	 */
	public ResourceBundleConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * Sets the configuration.
	 *
	 * @param pConfiguration the Configuration to set
	 */
	public void setConfiguration(ResourceBundleConfiguration pConfiguration) {
		this.mConfiguration = pConfiguration;
	}
	/**
	 * This method is used to instantiate ProcessResourceBundleMessage class.ErrorMapKey property is of Map type and contains processorKeyName as key 
	 * and Class name as value. Based on Configuration.processorKeyName this method instantiates appropriate class.
	 * @throws SystemException SystemException
	 */
	protected void initComponent() throws SystemException {
		if(isLoggingDebug()){
			logDebug("DefaultResourceBundleTools - (initComponent Method):Start");
		}
		String processClassName = getProcessorMap().get(getConfiguration().getMessageProcessorKey());
		if(StringUtils.isBlank(processClassName)){
			throw new SystemException(IErrorId.INVALID_PROCESSORKEY);
		}else{
			try{
				Constructor processClass = Class.forName(processClassName).getConstructor(Repository.class, CommonPropertyManager.class,ResourceBundleConfiguration.class);
				mProcessMessage = (IProcessResourceBundleMessage)processClass.newInstance(mRepository, mPropertyManager,mConfiguration);
			} catch(InstantiationException inste){
				throw new SystemException(IErrorId.INSTANTIATION_ERROR, inste);		
			}catch(IllegalAccessException iae){
				throw new SystemException(IErrorId.ILLEGALACCESS_EXCEPTION, iae);	
			}catch(ClassNotFoundException cnfe){
				throw new SystemException(IErrorId.CLASS_NOT_FOUND_EXCEPTION, cnfe);
			}catch(NoSuchMethodException nse){
				throw new SystemException(IErrorId.NOSUCHMETHOD_EXCEPTION, nse);
			} catch(InvocationTargetException ite){
				throw new SystemException(IErrorId.INVOCATIONTARGET_EXCEPTION, ite);
			}
		} 
		if(isLoggingDebug()){
			logDebug("DefaultResourceBundleTools - (initComponent Method):End");
		}
	}

	/**
	 * This method invokes IProcessResourceBundleMessage getKeyValue() method. If ErrorMessage is null, assigns default Error Message.
	 *
	 * @param pKeyName KeyName
	 * @param pSiteId SiteId
	 * @return String ErrorMessage
	 * @throws RepositoryException - when error occurs
	 */
	public String getKeyValue(String pKeyName, String pSiteId) throws RepositoryException {
		if(isLoggingDebug()){
			logDebug("DefaultResourceBundleTools - (getKeyValue Method[pKeyName,pSiteId]):Start");
		}
		String keyValue = Constants.CONSTANT_EMPTY;
		keyValue = mProcessMessage.getKeyValue(pKeyName, pSiteId);
		if(StringUtils.isBlank(keyValue)){
			keyValue = new StringBuffer(pKeyName).append(IErrorId.KEY_NOT_FOUND).toString();
		}
		if(isLoggingDebug()){
			logDebug("DefaultResourceBundleTools - (getKeyValue Method[pKeyName,pSiteId]):End");
		}
		return keyValue;		
	}
	
	/**
	 * This method will get all the keys
	 * @return lArraylist - Enumeration type
	 * @throws RepositoryException - Exception may be thrown while performing Repository related operations
	 */
	public Enumeration<String> findAllKeys() throws RepositoryException {
		if(isLoggingDebug()){
			logDebug("DefaultResourceBundleTools - (findAllKeys[]) : Start");
		}
		RepositoryItemDescriptor resourceBundleDesc = getRepository().getItemDescriptor(getPropertyManager().getRepositoryItemDescPropertyName());
		RepositoryView bundleRepoView = resourceBundleDesc.getRepositoryView();
		QueryBuilder queryBuilder = bundleRepoView.getQueryBuilder();
		Query resourceBundleQuery = queryBuilder.createUnconstrainedQuery();
		RepositoryItem[] resourceBundleItems = bundleRepoView.executeQuery(resourceBundleQuery);
		List<String> keysList = new ArrayList<String>();
		if(null!=resourceBundleItems){
			for(RepositoryItem resourceBundleItem : resourceBundleItems){
				keysList.add((String)resourceBundleItem.getPropertyValue(getPropertyManager().getKeyNamePropertyName()));
			}
		}
		if(isLoggingDebug()){
			logDebug("List of Keys in DefaultResourceBundleTools - (findAllKeys[]) is : "+keysList);
			logDebug("DefaultResourceBundleTools - (findAllKeys[]) : End");
		}
		return Collections.enumeration(keysList);
	}
}
