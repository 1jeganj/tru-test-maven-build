package com.tru.common.tol;

import atg.repository.RepositoryException;

/**
 * This is interface has method to get key value from errorHandle or resourceBundle repository.
 *  @author Professional Access
 *  @version 1.0
 */

public interface IProcessResourceBundleMessage {
	
	/**
	 * This is abstract method to get key value from errorHandle or resourceBundle repository.
	 *
	 * @param pKeyName KeyName
	 * @param pSiteId SiteId
	 * @return ErrorMessage
	 * @throws RepositoryException - when error occurs
	 */
	String getKeyValue(String pKeyName, String pSiteId) throws RepositoryException;
}
