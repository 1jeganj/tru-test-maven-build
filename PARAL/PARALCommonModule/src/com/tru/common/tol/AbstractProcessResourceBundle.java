package com.tru.common.tol;

import atg.nucleus.GenericService;
import atg.repository.Repository;

import com.tru.common.cml.ResourceBundleConfiguration;


/**
 * This is abstract class which holds common properties like PropertyManager and Repository.
 * 
 * @author Professional Access
 * @version 1.0
 */
public abstract class AbstractProcessResourceBundle extends GenericService implements IProcessResourceBundleMessage{
	
	/**
	 * Constructor.
	 *
	 * @param pRepository Repository
	 * @param pPropertyManager PropertyManager
	 * @param pConfiguration Configuration
	 */
	public AbstractProcessResourceBundle(Repository pRepository,CommonPropertyManager pPropertyManager,ResourceBundleConfiguration pConfiguration){
		mPropertyManager = pPropertyManager;
		mRepository = pRepository;
		mConfiguration = pConfiguration;
	}
	
	/** Holds the Constants for Configuration. */
	private ResourceBundleConfiguration mConfiguration;

	/** Holds the Constants for PropertyManager. */
	private CommonPropertyManager mPropertyManager;
	
	/** Holds the Constants for Repository. */
	private Repository mRepository;
	
	/**
	 * Gets the property manager.
	 *
	 * @return mPropertyManager
	 */
	public CommonPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 *
	 * @param pPropertyManager the PropertyManager to set
	 */
	public void setPropertyManager(CommonPropertyManager pPropertyManager) {
		this.mPropertyManager = pPropertyManager;
	}

	/**
	 * @return the repository
	 */
	public Repository getRepository() {
		return mRepository;
	}

	/**
	 * @param pRepository the repository to set
	 */
	public void setRepository(Repository pRepository) {
		mRepository = pRepository;
	}

	/**
	 * Gets the configuration.
	 *
	 * @return the configuration
	 */
	public ResourceBundleConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * Sets the configuration.
	 *
	 * @param pConfiguration the configuration to set
	 */
	public void setConfiguration(ResourceBundleConfiguration pConfiguration) {
		mConfiguration = pConfiguration;
	}
	
}
