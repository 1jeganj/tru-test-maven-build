package com.tru.common.tol;
import java.util.Enumeration;

import atg.repository.RepositoryException;

/**
 * This is interface has method to get Key Value from ErrorHandler or ResourceBundle Repository.
 *  @author Professional Access
 *  @version 1.0
 * 
 */

public interface IResourceBundleTools {

	/**
	 * This is abstract method to get Key Value from ErrorHandler or ResourceBundle Repository.
	 *
	 * @param pKeyName KeyName
	 * @param pSiteId SiteId
	 * @return String ErrorMessage
	 * @throws RepositoryException - when error occurs
	 */
	String getKeyValue(String pKeyName, String pSiteId)  throws RepositoryException;
	/**
	 * This method will get all the keys from repository
	 * @return lArraylist - Enumeration type
	 * @throws RepositoryException - when error occurs.
	 */
	Enumeration<String> findAllKeys() throws RepositoryException;
}
