package com.tru.resourcebundle.mgl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.common.cml.Constants;
import com.tru.common.cml.IErrorId;
import com.tru.common.cml.SystemException;
import com.tru.common.mgl.MessageFeedProcessor;
import com.tru.common.tol.RepositoryTools;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.merchutil.tol.workflow.WorkFlowTools;
import com.tru.merchutil.tol.workflow.exception.WorkFlowException;

/**
 * This class is used to load ErrorMessages/ResourceBundle messages in to Core and pub Schemas.
 * 
 * @version : 1.0
 * @author Professional Access
 */
public class PUBResourceBundleMessageLoader extends GenericService{
	
	/** Used to hold the mMessageFeedProcessor. */
	private MessageFeedProcessor mFeedProcessor;
		
	/**
     * Property to hold mBCCUserName
     */
	protected String mBCCUserName;
	/**
	 * Holds the WorkFlowTools object
	 */
	private WorkFlowTools mWorkFlowTools;
	
	/**
	 * Holds the RepositoryTools object
	 */
	private RepositoryTools mRepositoryTools;
	
	/**
	 * Holds the WorkFlowPath object
	 */
	private String mWorkFlowPath;
	/**
	 * Holds the ErrorHandlerProjectName object
	 */
	private String mBCCProjectName;
	
	/**
	 * Holds the CsvFilePath object
	 */
	private String mCsvFilePath;

	/**
	 * @return the CsvFilePath
	 */
	public String getCsvFilePath() {
		return mCsvFilePath;
	}

	/**
	 * @param pCsvFilePath the CsvFilePath to set
	 */
	public void setCsvFilePath(String pCsvFilePath) {
		mCsvFilePath = pCsvFilePath;
	}
	
	/**
	 *  Getter method for mBCCUserName
	 * @return String : mBCCUserName 
	 */
	public String getBCCUserName() {
		return mBCCUserName;
	}
	/**
	 *  Setter method for mBCCUserName
	 * @param pBCCUserName : String to set value to  mBCCUserName
	 */
	public void setBCCUserName(String pBCCUserName) {
		mBCCUserName = pBCCUserName;
	}

	/**
	 * @return the mBCCProjectName
	 */
	public String getBCCProjectName() {
		return mBCCProjectName;
	}
	/**
	 * @param pBCCProjectName the mBCCProjectName to set
	 */
	public void setBCCProjectName(String pBCCProjectName) {
		this.mBCCProjectName = pBCCProjectName;
	}

	/**
	 * @return the workFlowPath
	 */
	public String getWorkFlowPath() {
		return mWorkFlowPath;
	}

	/**
	 * @param pWorkFlowPath the workFlowPath to set
	 */
	public void setWorkFlowPath(String pWorkFlowPath) {
		mWorkFlowPath = pWorkFlowPath;
	}

	/**
	 * @return the workFlowTools
	 */
	public WorkFlowTools getWorkFlowTools() {
		return mWorkFlowTools;
	}

	/**
	 * @param pWorkFlowTools
	 *            the workFlowTools to set
	 */
	public void setWorkFlowTools(WorkFlowTools pWorkFlowTools) {
		mWorkFlowTools = pWorkFlowTools;
	}
	
	/**
	 * @return the MessageFeedProcessor
	 */
	public MessageFeedProcessor getFeedProcessor() {
		return mFeedProcessor;
	}

	/**
	 * @param pFeedProcessor the MessageFeedProcessor to set
	 */
	public void setFeedProcessor(
			MessageFeedProcessor pFeedProcessor) {
		mFeedProcessor = pFeedProcessor;
	}
	
	/**
	 * @return the repositoryTools
	 */
	public RepositoryTools getRepositoryTools() {
		return mRepositoryTools;
	}

	/**
	 * @param pRepositoryTools the repositoryTools to set
	 */
	public void setRepositoryTools(RepositoryTools pRepositoryTools) {
		mRepositoryTools = pRepositoryTools;
	}
	/**
	 *  This method is used to load the error message by using Feed in BCC 
	 * @throws WorkFlowException - WorkFlowException
	 * @throws FeedSkippableException - FeedSkippableException
	 */
	public void loadMessages() throws WorkFlowException, FeedSkippableException
	{
		try{
			if(StringUtils.isBlank(getCsvFilePath())){
				throw new SystemException(IErrorId.FILE_NOT_FOUND);
			}else{
				if(mBCCProjectName != null){
					startProjectInBCC();
				}
				getFeedProcessor().processFeedJob(getCsvFilePath());
				getWorkFlowTools().finishTask();
			
			}
		} catch(SystemException se){
			if(isLoggingError()){
				logError(se.getErrorId(),se.getCause());
			}
		}
	}
	
	/**
	 * This method invokes to create the new project in BCC
	 * @throws SystemException - when error occurs.
	 * @throws WorkFlowException 
	 * @throws FeedSkippableException 
	 */
	private void startProjectInBCC() throws SystemException, WorkFlowException, FeedSkippableException{
		if (isLoggingDebug()) {
			logDebug("PUBMessageLoader.startProjectInBCC() method Started.");
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.TIMESTAMP_FORMAT_FOR_IMPORT, Locale.getDefault());
		String dateString = simpleDateFormat.format(Calendar.getInstance().getTime());
		String projectName = new StringBuilder().append(getBCCProjectName()).append(
				Constants.UNDERSCORE).append(dateString).toString();
		
		// This workFlow will create the project in BCC
		getWorkFlowTools().initiateWorkflow(projectName, getWorkFlowPath(),getBCCUserName());
		if (isLoggingDebug()) {
			logDebug("project created in BCC, project name :" + projectName);
		}
		if (isLoggingDebug()) {
			logDebug("PUBMessageLoader.startProjectInBCC() method Ended.");
		}
	}
}
