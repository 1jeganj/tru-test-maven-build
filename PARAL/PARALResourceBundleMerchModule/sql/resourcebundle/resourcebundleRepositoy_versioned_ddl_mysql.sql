-- drop table ral_resourcebundle_xlate;
-- drop table ral_resourcebundle_key_xlate;
-- drop table ral_resourcebundle;

CREATE TABLE ral_resourcebundle ( 
    ASSET_VERSION    DECIMAL(19,0) NOT NULL,
    WORKSPACE_ID     VARCHAR(40) NOT NULL,
    BRANCH_ID        VARCHAR(40) NOT NULL,
    IS_HEAD          TINYINT NOT NULL,
    VERSION_DELETED  TINYINT NOT NULL,
    VERSION_EDITABLE TINYINT NOT NULL,
    PRED_VERSION     DECIMAL(19,0) NULL,
    CHECKIN_DATE DATETIME(6) NULL,
    key_id varchar(40) NOT NULL,
    key_name varchar(254)NOT NULL,
    key_value varchar(4000) NULL,
    constraint ral_resourcebundle_p PRIMARY KEY(key_id,key_name,asset_version) ); 
	
CREATE TABLE ral_resourcebundle_key_xlate ( 
	asset_version DECIMAL(38) NOT NULL,
	key_id varchar(40) NOT NULL, 
	locale varchar(254) NOT NULL, 
	translation_id varchar(254) NULL, 
	constraint ral_resourcebundle_key_xlate_p PRIMARY KEY(key_id, locale, asset_version) ); 
	

CREATE INDEX ral_resourcebundle_key_xlate_idx ON ral_resourcebundle_key_xlate(key_id, asset_version, locale);

CREATE TABLE ral_resourcebundle_xlate ( 
	ASSET_VERSION    DECIMAL(19,0) NOT NULL,
    WORKSPACE_ID     VARCHAR(40) NOT NULL,
    BRANCH_ID        VARCHAR(40) NOT NULL,
    IS_HEAD          TINYINT NOT NULL,
    VERSION_DELETED  TINYINT NOT NULL,
    VERSION_EDITABLE TINYINT NOT NULL,
    PRED_VERSION     DECIMAL(19,0) NULL,
    CHECKIN_DATE DATETIME(6) NULL,
	translation_id varchar(254) NOT NULL,
	key_value varchar(4000) NULL, 
	constraint ral_resourcebundle_xlate_p PRIMARY KEY(translation_id, asset_version) );