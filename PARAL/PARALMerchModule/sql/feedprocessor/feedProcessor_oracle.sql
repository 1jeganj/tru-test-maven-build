--------------------------------------------------------
--  File created - Thursday-December-19-2013   
--------------------------------------------------------
  DROP TABLE "BATCH_JOB_EXECUTION" cascade constraints;
  DROP TABLE "BATCH_JOB_EXECUTION_CONTEXT" cascade constraints;
  DROP TABLE "BATCH_JOB_EXECUTION_PARAMS" cascade constraints;
  DROP TABLE "BATCH_JOB_INSTANCE" cascade constraints;
  DROP TABLE "BATCH_STEP_EXECUTION" cascade constraints;
  DROP TABLE "BATCH_STEP_EXECUTION_CONTEXT" cascade constraints;
--------------------------------------------------------
--  DDL for Table BATCH_JOB_EXECUTION
--------------------------------------------------------

  CREATE TABLE "BATCH_JOB_EXECUTION" 
   (	"JOB_EXECUTION_ID" NUMBER(19,0), 
	"VERSION" NUMBER(19,0), 
	"JOB_INSTANCE_ID" NUMBER(19,0), 
	"CREATE_TIME" TIMESTAMP (6), 
	"START_TIME" TIMESTAMP (6) DEFAULT NULL, 
	"END_TIME" TIMESTAMP (6) DEFAULT NULL, 
	"STATUS" VARCHAR2(10), 
	"EXIT_CODE" VARCHAR2(20), 
	"EXIT_MESSAGE" VARCHAR2(2500), 
	"LAST_UPDATED" TIMESTAMP (6)
   ) ;
/
--------------------------------------------------------
--  DDL for Table BATCH_JOB_EXECUTION_CONTEXT
--------------------------------------------------------

  CREATE TABLE "BATCH_JOB_EXECUTION_CONTEXT" 
   (	"JOB_EXECUTION_ID" NUMBER(19,0), 
	"SHORT_CONTEXT" VARCHAR2(2500), 
	"SERIALIZED_CONTEXT" CLOB
   ) ;
/
--------------------------------------------------------
--  DDL for Table BATCH_JOB_EXECUTION_PARAMS
--------------------------------------------------------

  CREATE TABLE "BATCH_JOB_EXECUTION_PARAMS" 
   (	"JOB_EXECUTION_ID" NUMBER(19,0), 
	"TYPE_CD" VARCHAR2(6), 
	"KEY_NAME" VARCHAR2(100), 
	"STRING_VAL" VARCHAR2(250), 
	"DATE_VAL" TIMESTAMP (6) DEFAULT NULL, 
	"LONG_VAL" NUMBER(19,0), 
	"DOUBLE_VAL" FLOAT(126), 
	"IDENTIFYING" VARCHAR2(1)
   ) ;
/
--------------------------------------------------------
--  DDL for Table BATCH_JOB_INSTANCE
--------------------------------------------------------

  CREATE TABLE "BATCH_JOB_INSTANCE" 
   (	"JOB_INSTANCE_ID" NUMBER(19,0), 
	"VERSION" NUMBER(19,0), 
	"JOB_NAME" VARCHAR2(100), 
	"JOB_KEY" VARCHAR2(2500)
   ) ;
/
--------------------------------------------------------
--  DDL for Table BATCH_STEP_EXECUTION
--------------------------------------------------------

  CREATE TABLE "BATCH_STEP_EXECUTION" 
   (	"STEP_EXECUTION_ID" NUMBER(19,0), 
	"VERSION" NUMBER(19,0), 
	"STEP_NAME" VARCHAR2(100), 
	"JOB_EXECUTION_ID" NUMBER(19,0), 
	"START_TIME" TIMESTAMP (6), 
	"END_TIME" TIMESTAMP (6) DEFAULT NULL, 
	"STATUS" VARCHAR2(10), 
	"COMMIT_COUNT" NUMBER(19,0), 
	"READ_COUNT" NUMBER(19,0), 
	"FILTER_COUNT" NUMBER(19,0), 
	"WRITE_COUNT" NUMBER(19,0), 
	"READ_SKIP_COUNT" NUMBER(19,0), 
	"WRITE_SKIP_COUNT" NUMBER(19,0), 
	"PROCESS_SKIP_COUNT" NUMBER(19,0), 
	"ROLLBACK_COUNT" NUMBER(19,0), 
	"EXIT_CODE" VARCHAR2(20), 
	"EXIT_MESSAGE" VARCHAR2(2500), 
	"LAST_UPDATED" TIMESTAMP (6)
   ) ;
/
--------------------------------------------------------
--  DDL for Table BATCH_STEP_EXECUTION_CONTEXT
--------------------------------------------------------

  CREATE TABLE "BATCH_STEP_EXECUTION_CONTEXT" 
   (	"STEP_EXECUTION_ID" NUMBER(19,0), 
	"SHORT_CONTEXT" VARCHAR2(2500), 
	"SERIALIZED_CONTEXT" CLOB
   ) ;
/
--------------------------------------------------------
--  Constraints for Table BATCH_JOB_EXECUTION
--------------------------------------------------------

  ALTER TABLE "BATCH_JOB_EXECUTION" MODIFY ("JOB_INSTANCE_ID" NOT NULL ENABLE);
 
  ALTER TABLE "BATCH_JOB_EXECUTION" MODIFY ("CREATE_TIME" NOT NULL ENABLE);
 
  ALTER TABLE "BATCH_JOB_EXECUTION" ADD PRIMARY KEY ("JOB_EXECUTION_ID") ENABLE;
/
--------------------------------------------------------
--  Constraints for Table BATCH_JOB_EXECUTION_CONTEXT
--------------------------------------------------------

  ALTER TABLE "BATCH_JOB_EXECUTION_CONTEXT" MODIFY ("SHORT_CONTEXT" NOT NULL ENABLE);
 
  ALTER TABLE "BATCH_JOB_EXECUTION_CONTEXT" ADD PRIMARY KEY ("JOB_EXECUTION_ID") ENABLE;
/
--------------------------------------------------------
--  Constraints for Table BATCH_JOB_EXECUTION_PARAMS
--------------------------------------------------------

  ALTER TABLE "BATCH_JOB_EXECUTION_PARAMS" MODIFY ("JOB_EXECUTION_ID" NOT NULL ENABLE);
 
  ALTER TABLE "BATCH_JOB_EXECUTION_PARAMS" MODIFY ("TYPE_CD" NOT NULL ENABLE);
 
  ALTER TABLE "BATCH_JOB_EXECUTION_PARAMS" MODIFY ("KEY_NAME" NOT NULL ENABLE);
 
  ALTER TABLE "BATCH_JOB_EXECUTION_PARAMS" MODIFY ("IDENTIFYING" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table BATCH_JOB_INSTANCE
--------------------------------------------------------

  ALTER TABLE "BATCH_JOB_INSTANCE" MODIFY ("JOB_NAME" NOT NULL ENABLE);
 
  ALTER TABLE "BATCH_JOB_INSTANCE" ADD PRIMARY KEY ("JOB_INSTANCE_ID") ENABLE;
/
--------------------------------------------------------
--  Constraints for Table BATCH_STEP_EXECUTION
--------------------------------------------------------

  ALTER TABLE "BATCH_STEP_EXECUTION" MODIFY ("VERSION" NOT NULL ENABLE);
 
  ALTER TABLE "BATCH_STEP_EXECUTION" MODIFY ("STEP_NAME" NOT NULL ENABLE);
 
  ALTER TABLE "BATCH_STEP_EXECUTION" MODIFY ("JOB_EXECUTION_ID" NOT NULL ENABLE);
 
  ALTER TABLE "BATCH_STEP_EXECUTION" MODIFY ("START_TIME" NOT NULL ENABLE);
 
  ALTER TABLE "BATCH_STEP_EXECUTION" ADD PRIMARY KEY ("STEP_EXECUTION_ID") ENABLE;
/
--------------------------------------------------------
--  Constraints for Table BATCH_STEP_EXECUTION_CONTEXT
--------------------------------------------------------

  ALTER TABLE "BATCH_STEP_EXECUTION_CONTEXT" MODIFY ("SHORT_CONTEXT" NOT NULL ENABLE);
 
  ALTER TABLE "BATCH_STEP_EXECUTION_CONTEXT" ADD PRIMARY KEY ("STEP_EXECUTION_ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table BATCH_JOB_EXECUTION
--------------------------------------------------------

  ALTER TABLE "BATCH_JOB_EXECUTION" ADD CONSTRAINT "JOB_INSTANCE_EXECUTION_FK" FOREIGN KEY ("JOB_INSTANCE_ID")
	  REFERENCES "BATCH_JOB_INSTANCE" ("JOB_INSTANCE_ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table BATCH_JOB_EXECUTION_CONTEXT
--------------------------------------------------------

  ALTER TABLE "BATCH_JOB_EXECUTION_CONTEXT" ADD CONSTRAINT "JOB_EXEC_CTX_FK" FOREIGN KEY ("JOB_EXECUTION_ID")
	  REFERENCES "BATCH_JOB_EXECUTION" ("JOB_EXECUTION_ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table BATCH_JOB_EXECUTION_PARAMS
--------------------------------------------------------

  ALTER TABLE "BATCH_JOB_EXECUTION_PARAMS" ADD CONSTRAINT "JOB_EXEC_PARAMS_FK" FOREIGN KEY ("JOB_EXECUTION_ID")
	  REFERENCES "BATCH_JOB_EXECUTION" ("JOB_EXECUTION_ID") ENABLE;
/

--------------------------------------------------------
--  Ref Constraints for Table BATCH_STEP_EXECUTION
--------------------------------------------------------

  ALTER TABLE "BATCH_STEP_EXECUTION" ADD CONSTRAINT "JOB_EXECUTION_STEP_FK" FOREIGN KEY ("JOB_EXECUTION_ID")
	  REFERENCES "BATCH_JOB_EXECUTION" ("JOB_EXECUTION_ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table BATCH_STEP_EXECUTION_CONTEXT
--------------------------------------------------------

  ALTER TABLE "BATCH_STEP_EXECUTION_CONTEXT" ADD CONSTRAINT "STEP_EXEC_CTX_FK" FOREIGN KEY ("STEP_EXECUTION_ID")
	  REFERENCES "BATCH_STEP_EXECUTION" ("STEP_EXECUTION_ID") ENABLE;
/

--------------------------------------------------------
--  Ref SEQUENCE for Table BATCH_JOB_EXECUTION
--------------------------------------------------------

CREATE SEQUENCE "FEED"."BATCH_JOB_EXECUTION_SEQ" MINVALUE 1 MAXVALUE
  9999999999999999999999999999 INCREMENT BY 1 START WITH 1701 CACHE 20 NOORDER
  NOCYCLE ;
/

CREATE SEQUENCE "FEED"."BATCH_JOB_SEQ" MINVALUE 1 MAXVALUE
  9999999999999999999999999999 INCREMENT BY 1 START WITH 1681 CACHE 20 NOORDER
  NOCYCLE ;
/

CREATE SEQUENCE "FEED"."BATCH_STEP_EXECUTION_SEQ" MINVALUE 1 MAXVALUE
  9999999999999999999999999999 INCREMENT BY 1 START WITH 5501 CACHE 20 NOORDER
  NOCYCLE ;
/
