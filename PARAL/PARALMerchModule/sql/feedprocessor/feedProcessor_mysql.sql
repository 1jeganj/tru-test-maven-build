--------------------------------------------------------
--  DDL for Table BATCH_JOB_EXECUTION
--------------------------------------------------------
create table BATCH_JOB_EXECUTION
(
	JOB_EXECUTION_ID  NUMERIC(19,0),
	VERSION NUMERIC(19,0),
	JOB_INSTANCE_ID NUMERIC(19,0),
	CREATE_TIME TIMESTAMP,
	START_TIME TIMESTAMP  NULL,
	END_TIME TIMESTAMP  NULL, 
	STATUS VARCHAR(10), 
	EXIT_CODE VARCHAR(20), 
	EXIT_MESSAGE VARCHAR(2500),
	LAST_UPDATED TIMESTAMP 
);
--------------------------------------------------------
--  DDL for Table BATCH_JOB_EXECUTION_CONTEXT
--------------------------------------------------------
 CREATE TABLE BATCH_JOB_EXECUTION_CONTEXT 
 (
   	JOB_EXECUTION_ID NUMERIC(19,0), 
	SHORT_CONTEXT VARCHAR(2500), 
	SERIALIZED_CONTEXT LONGTEXT
);
--------------------------------------------------------
--  DDL for Table BATCH_JOB_EXECUTION_PARAMS
------------------------------------------------------
 CREATE TABLE BATCH_JOB_EXECUTION_PARAMS
 (
   	JOB_EXECUTION_ID NUMERIC(19,0), 
	TYPE_CD VARCHAR(6), 
	KEY_NAME VARCHAR(100), 
	STRING_VAL VARCHAR(250), 
	DATE_VAL TIMESTAMP NULL, 
	LONG_VAL NUMERIC(19,0), 
	DOUBLE_VAL FLOAT(12,6), 
	IDENTIFYING VARCHAR(1)
) ;
--------------------------------------------------------
--  DDL for Table BATCH_JOB_INSTANCE
-------------------------------------------------------- 
CREATE TABLE BATCH_JOB_INSTANCE 
(
   	JOB_INSTANCE_ID NUMERIC(19,0), 
	VERSION NUMERIC(19,0), 
	JOB_NAME VARCHAR(100), 
	JOB_KEY VARCHAR(2500)
) ;
--------------------------------------------------------
--  DDL for Table BATCH_STEP_EXECUTION
-------------------------------------------------------- 
  CREATE TABLE BATCH_STEP_EXECUTION 
  (
  	STEP_EXECUTION_ID NUMERIC(19,0), 
	VERSION NUMERIC(19,0), 
	STEP_NAME VARCHAR(100), 
	JOB_EXECUTION_ID NUMERIC(19,0), 
	START_TIME TIMESTAMP, 
	END_TIME TIMESTAMP NULL, 
	STATUS VARCHAR(10), 
	COMMIT_COUNT NUMERIC(19,0), 
	READ_COUNT NUMERIC(19,0), 
	FILTER_COUNT NUMERIC(19,0), 
	WRITE_COUNT NUMERIC(19,0), 
	READ_SKIP_COUNT NUMERIC(19,0), 
	WRITE_SKIP_COUNT NUMERIC(19,0), 
	PROCESS_SKIP_COUNT NUMERIC(19,0), 
	ROLLBACK_COUNT NUMERIC(19,0), 
	EXIT_CODE VARCHAR(20), 
	EXIT_MESSAGE VARCHAR(2500), 
	LAST_UPDATED TIMESTAMP 
   ) ;
--------------------------------------------------------
--  DDL for Table BATCH_STEP_EXECUTION_CONTEXT
--------------------------------------------------------
CREATE TABLE BATCH_STEP_EXECUTION_CONTEXT
(	
	STEP_EXECUTION_ID NUMERIC(19,0), 
	SHORT_CONTEXT VARCHAR(2500), 
	SERIALIZED_CONTEXT LONGTEXT
) ;
--------------------------------------------------------
--  Constraints for Table BATCH_JOB_EXECUTION
--------------------------------------------------------
ALTER TABLE BATCH_JOB_EXECUTION MODIFY JOB_INSTANCE_ID NUMERIC(19,0) NOT NULL ;
ALTER TABLE BATCH_JOB_EXECUTION MODIFY CREATE_TIME TIMESTAMP NOT NULL;
ALTER TABLE BATCH_JOB_EXECUTION ADD PRIMARY KEY (JOB_EXECUTION_ID);

--------------------------------------------------------
--  Constraints for Table BATCH_JOB_EXECUTION_CONTEXT
--------------------------------------------------------
ALTER TABLE BATCH_JOB_EXECUTION_CONTEXT MODIFY SHORT_CONTEXT  NUMERIC(19,0) NOT NULL;
ALTER TABLE BATCH_JOB_EXECUTION_CONTEXT ADD PRIMARY KEY (JOB_EXECUTION_ID) ;

--------------------------------------------------------
--  Constraints for Table BATCH_JOB_EXECUTION_PARAMS
--------------------------------------------------------
ALTER TABLE BATCH_JOB_EXECUTION_PARAMS MODIFY JOB_EXECUTION_ID NUMERIC(19,0) NOT NULL ;
ALTER TABLE BATCH_JOB_EXECUTION_PARAMS MODIFY TYPE_CD VARCHAR(6) NOT NULL ;
ALTER TABLE BATCH_JOB_EXECUTION_PARAMS MODIFY KEY_NAME VARCHAR(100) NOT NULL ;
ALTER TABLE BATCH_JOB_EXECUTION_PARAMS MODIFY IDENTIFYING VARCHAR(1) NOT NULL ;

--------------------------------------------------------
--  Constraints for Table BATCH_JOB_INSTANCE
--------------------------------------------------------
ALTER TABLE BATCH_JOB_INSTANCE MODIFY JOB_NAME NUMERIC(19,0) NOT NULL ;
ALTER TABLE BATCH_JOB_INSTANCE ADD PRIMARY KEY (JOB_INSTANCE_ID);
/
--------------------------------------------------------
--  Constraints for Table BATCH_STEP_EXECUTION
--------------------------------------------------------

  ALTER TABLE BATCH_STEP_EXECUTION MODIFY VERSION NUMERIC(19,0) NOT NULL ;
 
  ALTER TABLE BATCH_STEP_EXECUTION MODIFY STEP_NAME VARCHAR(100) NOT NULL ;
 
  ALTER TABLE BATCH_STEP_EXECUTION MODIFY JOB_EXECUTION_ID NUMERIC(19,0) NOT NULL;
 
  ALTER TABLE BATCH_STEP_EXECUTION MODIFY START_TIME TIMESTAMP NOT NULL ;
 
  ALTER TABLE BATCH_STEP_EXECUTION ADD PRIMARY KEY (STEP_EXECUTION_ID) ;
/
--------------------------------------------------------
--  Constraints for Table BATCH_STEP_EXECUTION_CONTEXT
--------------------------------------------------------

  ALTER TABLE BATCH_STEP_EXECUTION_CONTEXT MODIFY SHORT_CONTEXT VARCHAR(2500) NOT NULL;
 
  ALTER TABLE BATCH_STEP_EXECUTION_CONTEXT ADD PRIMARY KEY (STEP_EXECUTION_ID) ;
/
--------------------------------------------------------
--  Ref Constraints for Table BATCH_JOB_EXECUTION
--------------------------------------------------------

  ALTER TABLE BATCH_JOB_EXECUTION ADD CONSTRAINT JOB_INSTANCE_EXECUTION_FK FOREIGN KEY (JOB_INSTANCE_ID)
	  REFERENCES BATCH_JOB_INSTANCE (JOB_INSTANCE_ID) ;
/
--------------------------------------------------------
--  Ref Constraints for Table BATCH_JOB_EXECUTION_CONTEXT
--------------------------------------------------------

  ALTER TABLE BATCH_JOB_EXECUTION_CONTEXT ADD CONSTRAINT JOB_EXEC_CTX_FK FOREIGN KEY (JOB_EXECUTION_ID)
	  REFERENCES BATCH_JOB_EXECUTION (JOB_EXECUTION_ID) ;
/
--------------------------------------------------------
--  Ref Constraints for Table BATCH_JOB_EXECUTION_PARAMS
--------------------------------------------------------

  ALTER TABLE BATCH_JOB_EXECUTION_PARAMS ADD CONSTRAINT JOB_EXEC_PARAMS_FK FOREIGN KEY (JOB_EXECUTION_ID)
	  REFERENCES BATCH_JOB_EXECUTION (JOB_EXECUTION_ID) ;
/

--------------------------------------------------------
--  Ref Constraints for Table BATCH_STEP_EXECUTION
--------------------------------------------------------

  ALTER TABLE BATCH_STEP_EXECUTION ADD CONSTRAINT JOB_EXECUTION_STEP_FK FOREIGN KEY (JOB_EXECUTION_ID)
	  REFERENCES BATCH_JOB_EXECUTION (JOB_EXECUTION_ID) ;
/
--------------------------------------------------------
--  Ref Constraints for Table BATCH_STEP_EXECUTION_CONTEXT
--------------------------------------------------------

  ALTER TABLE BATCH_STEP_EXECUTION_CONTEXT ADD CONSTRAINT STEP_EXEC_CTX_FK FOREIGN KEY (STEP_EXECUTION_ID)
	  REFERENCES BATCH_STEP_EXECUTION (STEP_EXECUTION_ID) ;
/

CREATE TABLE BATCH_STEP_EXECUTION_SEQ (
	ID BIGINT NOT NULL,
	UNIQUE_KEY CHAR(1) NOT NULL,
	constraint UNIQUE_KEY_UN unique (UNIQUE_KEY)
);

INSERT INTO BATCH_STEP_EXECUTION_SEQ (ID, UNIQUE_KEY) select * from (select 0 as ID, '0' as UNIQUE_KEY) as tmp where not exists(select * from BATCH_STEP_EXECUTION_SEQ);

CREATE TABLE BATCH_JOB_EXECUTION_SEQ (
	ID BIGINT NOT NULL,
	UNIQUE_KEY CHAR(1) NOT NULL,
	constraint UNIQUE_KEY_UN unique (UNIQUE_KEY)
);

INSERT INTO BATCH_JOB_EXECUTION_SEQ (ID, UNIQUE_KEY) select * from (select 0 as ID, '0' as UNIQUE_KEY) as tmp where not exists(select * from BATCH_JOB_EXECUTION_SEQ);

CREATE TABLE BATCH_JOB_SEQ (
	ID BIGINT NOT NULL,
	UNIQUE_KEY CHAR(1) NOT NULL,
	constraint UNIQUE_KEY_UN unique (UNIQUE_KEY)
);

INSERT INTO BATCH_JOB_SEQ (ID, UNIQUE_KEY) select * from (select 0 as ID, '0' as UNIQUE_KEY) as tmp where not exists(select * from BATCH_JOB_SEQ);