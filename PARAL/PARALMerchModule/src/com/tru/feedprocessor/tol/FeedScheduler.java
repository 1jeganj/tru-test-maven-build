package com.tru.feedprocessor.tol;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import atg.core.util.StringUtils;
import atg.nucleus.ServiceException;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;


/**
 * The Class FeedScheduler 
 *
 * This class is responsible for catalog feed Processing. This scheduler runs in
 * every 1 hour 
 *  
 * @version 1.0
 * @author Professional Access
 */
public class FeedScheduler extends SingletonSchedulableService {

	/** Property to hold mFeedJob. */
	private BatchInvoker mFeedJob;

	/**
	 * Gets the feed job.
	 * 
	 * @return the inventoryFeedJob
	 */
	public BatchInvoker getFeedJob() {
		return mFeedJob;
	}

	/**
	 * Sets the inventory feed job.
	 * 
	 * @param pFeedJob
	 *            the new feed job
	 */
	public void setFeedJob(BatchInvoker pFeedJob) {
		mFeedJob = pFeedJob;
	}

	/**
	 * Method responsible for starting the feed scheduling based on
	 * configured scheduler.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	@Override
	public void doStartService() throws ServiceException {
		if (isLoggingDebug()) {
			logDebug("Entering into feedfile import scheduler doStartService() method");
		}

		if(StringUtils.isBlank(getJobName())){
			final ScheduledJob job = new ScheduledJob(
					getJobName() , FeedConstants.COLON,getAbsoluteName(), getSchedule(), 
				this,ScheduledJob.SCHEDULER_THREAD);
			if (isLoggingDebug()) {
				logDebug("The created job object is " + job);
			}
			
			mJobId = getScheduler().addScheduledJob(job);
			
			if (isLoggingDebug()) {
				logDebug("The created jobId is  " + mJobId);
			}
		}
		else{
			if (isLoggingError()) {
				logError("Job Name is Empty or Null in FeedSchedular and could not proceed the job.");
			}	
		}
	}

	/**
	 * Method responsible for stopping the feed scheduling based on
	 * configured scheduler.
	 * 
	 * @throws ServiceException
	 *             - Exception
	 */
	@Override
	public void doStopService() throws ServiceException {
		if (isLoggingDebug()) {
			logDebug("Entering into feedfile import scheduler: doStopService() method");
		}
		getScheduler().removeScheduledJob(getJobId());
		if (isLoggingDebug()) {
			logDebug("Exiting from InventoryFileImportScheduler: doStopService() method");
		}
	}

	/**
	 * This method is extended from SingletonSchedulableService class to execute
	 * the scheduler task.
	 * 
	 * @param pScheduler
	 *            - Scheduler
	 * @param pScheduledJob
	 *            - ScheduledJob
	 */
	@Override
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		if (isLoggingInfo()) {
			logInfo("BEGIN : feedFileImportScheduler(doScheduledTask)");
		}
		List<String> lParams = new ArrayList<String>();
		Calendar cal = Calendar.getInstance();
		lParams.add(cal.getTime().toString());
		
		getFeedJob().startJob(new String[]{pScheduledJob.getJobName()});
		
		if (isLoggingInfo()) {
			logInfo("END : feedFileImportScheduler(doScheduledTask)");
		}
	}
}
