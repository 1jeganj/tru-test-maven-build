package com.tru.feedprocessor.tol;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersIncrementer;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.converter.DefaultJobParametersConverter;
import org.springframework.batch.core.converter.JobParametersConverter;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.launch.support.ExitCodeMapper;
import org.springframework.batch.core.launch.support.JvmSystemExiter;
import org.springframework.batch.core.launch.support.SimpleJvmExitCodeMapper;
import org.springframework.batch.core.launch.support.SystemExiter;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.tru.common.cml.AbstractComponent;
import com.tru.common.cml.SystemException;

/**
 * <p>
 * Basic launcher for starting jobs from the command line. In general, it is
 * assumed that this launcher will primarily be used to start a job via a script
 * from an Enterprise Scheduler. Therefore, exit codes are mapped to integers so
 * that schedulers can use the returned values to determine the next course of
 * action. The returned values can also be useful to operations teams in
 * determining what should happen upon failure. For example, a returned code of
 * 5 might mean that some resource wasn't available and the job should be
 * restarted. However, a code of 10 might mean that something critical has
 * happened and the issue should be escalated.
 * </p>
 * 
 * <p>
 * With any launch of a batch job within Spring Batch, a Spring context
 * containing the {@link Job} and some execution context has to be created. This
 * command line launcher can be used to load the job and its context from a
 * single location. All dependencies of the launcher will then be satisfied by
 * autowiring by type from the combined application context. Default values are
 * provided for all fields except the {@link JobLauncher} and {@link JobLocator}
 * . Therefore, if autowiring fails to set it (it should be noted that
 * dependency checking is disabled because most of the fields have default
 * values and thus don't require dependencies to be fulfilled via autowiring)
 * then an exception will be thrown. It should also be noted that even if an
 * exception is thrown by this class, it will be mapped to an integer and
 * returned.
 * </p>
 * 
 * <p>
 * Notice a property is available to set the {@link SystemExiter}. This class is
 * used to exit from the main method, rather than calling System.exit()
 * directly. This is because unit testing a class the calls System.exit() is
 * impossible without kicking off the test within a new JVM, which it is
 * possible to do, however it is a complex solution, much more so than
 * strategizing the exiter.
 * </p>
 * 
 * <p>
 * The arguments to this class are as follows:
 * </p>
 * 
 * <code>
 * jobPath <options> jobName (jobParameters)*
 * </code>
 * 
 * <p>
 * The command line options are as follows
 * <ul>
 * <li>jobPath: the xml application context containing a {@link Job}
 * <li>-restart: (optional) to restart the last failed execution</li>
 * <li>-next: (optional) to start the next in a sequence according to the
 * {@link JobParametersIncrementer} in the {@link Job}</li>
 * <li>jobName: the bean id of the job.
 * <li>jobParameters: 0 to many parameters that will be used to launch a job.
 * </ul>
 * </p>
 * 
 * <p>
 * If the <code>-next</code> option is used the parameters on the command line
 * (if any) are appended to those retrieved from the incrementer, overriding any
 * with the same key.
 * </p>
 * 
 * <p>
 * The combined application context must contain only one instance of
 * {@link JobLauncher}. The job parameters passed in to the command line will be
 * converted to {@link Properties} by assuming that each individual element is
 * one parameter that is separated by an equals sign. For example,
 * "vendor.id=290232". Below is an example arguments list: "
 * 
 * <p>
 * <code>
 * java org.springframework.batch.execution.bootstrap.support.ATGSpringBatchInvoker testJob.xml 
 * testJob schedule.date=2008/01/24 vendor.id=3902483920 
 * </code>
 * </p>
 * 
 * <p>
 * Once arguments have been successfully parsed, autowiring will be used to set
 * various dependencies. The {@JobLauncher} for example, will be
 * loaded this way. If none is contained in the bean factory (it searches by
 * type) then a {@link BeanDefinitionStoreException} will be thrown. The same
 * exception will also be thrown if there is more than one present. Assuming the
 * JobLauncher has been set correctly, the jobName argument will be used to
 * obtain an actual {@link Job}. If a {@link JobLocator} has been set, then it
 * will be used, if not the beanFactory will be asked, using the jobName as the
 * bean id.
 * </p>
 * 
 * 
 * @since 1.0
 */
/**
 * The Class BatchInvoker extends AbstractComponent .
 * 
 *  @version 1.1
 *  @author Professional Access
 */
public class BatchInvoker extends AbstractComponent {

	/** The m context. */
	ConfigurableApplicationContext mContext;

	/** The exit code mapper. */
	private ExitCodeMapper mExitCodeMapper = new SimpleJvmExitCodeMapper();

	/** The mLauncher. */
	private JobLauncher mLauncher;

	/** The job locator. */
	private JobLocator mJobLocator;

	// Package private for unit test
	/** The system exiter. */
	private static SystemExiter mSystemExiter = new JvmSystemExiter();

	/** The message. */
	private static String mMessage = FeedConstants.EMPTY;

	/** The job parameters converter. */
	private JobParametersConverter mJobParametersConverter = new DefaultJobParametersConverter();

	/** The job explorer. */
	private JobExplorer mJobExplorer;
	
	/** The jobConfigList */
	private List<String> mJobConfigList;
	/** The jobName */
	private String mJobName;
	
	/**
	 * @return the jobName
	 */
	public String getJobName() {
		return mJobName;
	}

	/**
	 * @param pJobName - the jobName to set
	 */
	public void setJobName(String pJobName) {
		this.mJobName = pJobName;
	}

	/**
	 * @return the jobConfigList
	 */
	public List<String> getJobConfigList() {
		return mJobConfigList;
	}

	/**
	 * @param pJobConfigList - the jobConfigList to set
	 */
	public void setJobConfigList(List<String> pJobConfigList) {
		this.mJobConfigList = pJobConfigList;
	}

	/**
	 * Gets the context.
	 * 
	 * @return the context
	 */
	public ConfigurableApplicationContext getContext() {
		return mContext;
	}

	/**
	 * Sets the context.
	 * 
	 * @param pContext
	 *            the new context
	 */
	public void setContext(ConfigurableApplicationContext pContext) {
		mContext = pContext;
	}

	/**
	 * Injection setter for the {@link JobLauncher}.
	 * 
	 * @param pLauncher
	 *            the new launcher
	 */
	public void setLauncher(JobLauncher pLauncher) {
		this.mLauncher = pLauncher;
	}

	/**
	 * Injection setter for {@link mJobExplorer}.
	 * 
	 * @param pJobExplorer
	 *            the new job explorer
	 */
	public void setJobExplorer(JobExplorer pJobExplorer) {
		this.mJobExplorer = pJobExplorer;
	}

	/**
	 * Injection setter for the {@link ExitCodeMapper}.
	 * 
	 * @param pExitCodeMapper
	 *            the new exit code mapper
	 */
	public void setExitCodeMapper(ExitCodeMapper pExitCodeMapper) {
		this.mExitCodeMapper = pExitCodeMapper;
	}

	/**
	 * Static setter for the {@link SystemExiter} so it can be adjusted before
	 * dependency injection. Typically overridden by
	 * 
	 * @param pSystemExiter
	 *            the system exiter {@link #setSystemExiter(SystemExiter)}.
	 */
	public static void presetSystemExiter(SystemExiter pSystemExiter) {
		BatchInvoker.mSystemExiter = pSystemExiter;
	}

	/**
	 * Retrieve the error message set by an instance of.
	 * 
	 * @return the error message {@link BatchInvoker} as it exits. Empty if the
	 *         last job launched was successful.
	 */
	public static String getErrorMessage() {
		return mMessage;
	}

	/**
	 * Injection setter for the {@link SystemExiter}.
	 * 
	 * @param pSystemExiter
	 *            the new system exiter
	 */
	public void setSystemExiter(SystemExiter pSystemExiter) {
		BatchInvoker.mSystemExiter = pSystemExiter;
	}

	/**
	 * Injection setter for {@link JobParametersConverter}.
	 * 
	 * @param pJobParametersConverter
	 *            the new job parameters converter
	 */
	public void setJobParametersConverter(
			JobParametersConverter pJobParametersConverter) {
		this.mJobParametersConverter = pJobParametersConverter;
	}

	/**
	 * Delegate to the exiter to (possibly) exit the VM gracefully.
	 * 
	 * @param pStatus
	 *            the status
	 */
	public void exit(int pStatus) {
		mSystemExiter.exit(pStatus);
	}

	/**
	 * Sets the job locator.
	 * 
	 * @param pJobLocator
	 *            the new job locator {@link JobLocator} to find a job to run.
	 */
	public void setJobLocator(JobLocator pJobLocator) {
		this.mJobLocator = pJobLocator;
	}

	/**
	 * Start.
	 *
	 * Start a job by obtaining a combined classpath using the job launcher and
	 * job paths. If a JobLocator has been set, then use it to obtain an actual
	 * job, if not ask the context for it.
	 * 
	 * @param pJobName
	 *            the job name
	 * @param pParameters
	 *            the parameters
	 * @param pOpts
	 *            the opts
	 * @return the int
	 */
	public int start(String pJobName, String[] pParameters, Set<String> pOpts) {

			Assert.state(mLauncher != null,
					FeedConstants.NO_JOBLAUNCHER_PROVIDED_MSG);
			if (pOpts.contains(FeedConstants.RESTART)
					|| pOpts.contains(FeedConstants.NEXT)) {
				Assert.state(mJobExplorer != null,
						FeedConstants.NO_JOBEXPLORER_PROVIDED_MSG);
			}

			Job job;
			if (mJobLocator != null) {
				try {
					job = mJobLocator.getJob(pJobName);
				} catch (NoSuchJobException ex) {
					if(isLoggingError()){
						logError(FeedConstants.JOB_TERMINATED_ERR_MSG, ex);
					}
					BatchInvoker.mMessage = FeedConstants.JOB_TERMINATED_ERR_MSG;
					return mExitCodeMapper.intValue(ExitStatus.FAILED.getExitCode());
				}
			} else {
				getContext().refresh();
				job = (Job) getContext().getBean(pJobName);
			}

			JobParameters jobParameters = mJobParametersConverter
					.getJobParameters(StringUtils
							.splitArrayElementsIntoProperties(pParameters,
									FeedConstants.EQUAL));

			if (jobParameters != null && jobParameters.isEmpty())
            {
                           Calendar lCDateTime = Calendar.getInstance();
                  jobParameters =
                              new JobParametersBuilder().addLong(pJobName, lCDateTime.getTimeInMillis()).toJobParameters();
            }

			// To get Last Job Instance 
			/*
			List<JobExecution> result=new ArrayList<JobExecution>();
			List<JobInstance> jobInstances= mJobExplorer.getJobInstances(job.getName(), 0, 1);
			if (!CollectionUtils.isEmpty(jobInstances)) {
				// This will retrieve the latest job execution:
				List<JobExecution> jobExecutions = mJobExplorer.getJobExecutions(jobInstances.get(0));

				if (!CollectionUtils.isEmpty(jobExecutions)) {
					result.add(jobExecutions.get(0));
				}
			}
			*/
			
			return runJob(job,jobParameters);

	}

	
	/**
	 * Run job.
	 *
	 * @param pJob the job
	 * @param pJobParameters the job parameters
	 * @return the int
	 */
	private int runJob(Job pJob,JobParameters pJobParameters) {
		
		JobExecution jobExecution;
		
		try {
			
			jobExecution = mLauncher.run(pJob, pJobParameters);
			
		} catch (JobExecutionAlreadyRunningException ex) {
			if(isLoggingError()){
				logError(FeedConstants.JOB_TERMINATED_ERR_MSG, ex);
			}
			BatchInvoker.mMessage = FeedConstants.JOB_TERMINATED_ERR_MSG;
			return mExitCodeMapper.intValue(ExitStatus.FAILED.getExitCode());
		} catch (JobRestartException ex) {
			if(isLoggingError()){
				logError(FeedConstants.JOB_TERMINATED_ERR_MSG, ex);
			}
			BatchInvoker.mMessage = FeedConstants.JOB_TERMINATED_ERR_MSG;
			return mExitCodeMapper.intValue(ExitStatus.FAILED.getExitCode());
		} catch (JobInstanceAlreadyCompleteException ex) {
			if(isLoggingError()){
				logError(FeedConstants.JOB_TERMINATED_ERR_MSG, ex);
			}
			BatchInvoker.mMessage = FeedConstants.JOB_TERMINATED_ERR_MSG;
			return mExitCodeMapper.intValue(ExitStatus.FAILED.getExitCode());
		} catch (JobParametersInvalidException ex) {
			if(isLoggingError()){
				logError(FeedConstants.JOB_TERMINATED_ERR_MSG, ex);
			}
			BatchInvoker.mMessage = FeedConstants.JOB_TERMINATED_ERR_MSG;
			return mExitCodeMapper.intValue(ExitStatus.FAILED.getExitCode());
		}
		finally {
			if (mContext != null) {
				mContext.close();
			}
		}
		return mExitCodeMapper.intValue(jobExecution.getExitStatus()
				.getExitCode());
	}

	/**
	 * Load context.
	 * 
	 * Loading XML into Spring context
	 * @param pJobPath
	 *            the job path
	 */
	public void loadContext(String[] pJobPath) {
		
		setContext(new ClassPathXmlApplicationContext(pJobPath));
		getContext().getAutowireCapableBeanFactory().autowireBeanProperties(
				this, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
	}

	/**
	 * Launch a batch job using a {@link BatchInvoker}. Creates a new Spring
	 * context for the job execution, and uses a common parent for all such
	 * contexts. No exception are thrown from this method, rather exceptions are
	 * logged and an integer returned through the exit status in a
	 * {@link JvmSystemExiter} (which can be overridden by defining one in the
	 * Spring context).<br/>
	 * Parameters can be provided in the form key=value, and will be converted
	 * using the injected {@link JobParametersConverter}.
	 * 
	 * @param pArgs
	 *            the args
	 */

	public void startJob(String[] pArgs) {

		Set<String> lOpts = new HashSet<String>();
		List<String> lParams = new ArrayList<String>();
		Calendar cal = Calendar.getInstance();
		lParams.add(cal.getTime().toString());

		int count = 0;

		String lJobName = null;

		for (String arg : pArgs) {
			if (arg.startsWith(FeedConstants.HYPHEN)) {
				lOpts.add(arg);
			} else {
				switch (count) {
				case 0:
					lJobName = arg;
					break;
				default:
					lParams.add(arg);
					break;
				}
				count++;
			}
		}

		if (lJobName == null) {
			if(isLoggingError()){
				logError(FeedConstants.JOB_FAIL_NO_PARAM_JOB_NAME);
			}
			BatchInvoker.mMessage = FeedConstants.JOB_FAIL_NO_PARAM_JOB_NAME;

		}

		String[] lParameters = lParams.toArray(new String[lParams.size()]);

		start(lJobName, lParameters, lOpts);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tru.common.cml.AbstractComponent#initComponent()
	 */
	@Override
	protected void initComponent() throws SystemException {
		loadContext(getJobConfigList().toArray(new String[0]));
	}

}
