package com.tru.feedprocessor.tol;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * The Class FeedConstants.
 * 
 * @version 1.1
 * @author Professional Access
 */
public class FeedConstants extends BaseFeedConstants {

	/** The Constant FEEDCONTEXT_QUEUE. */
	public final static String FEEDCONTEXT_QUEUE = "feedContextQueue";

	/** The Constant CURRENT_FEEDCONTEXT. */
	public final static String CURRENT_FEEDCONTEXT = "feedContextData";

	/** The Constant HAS_MORE_CONTEXT. */
	public final static String HAS_MORE_CONTEXT = "hasMoreContextData";

	/** The Constant PROCESSED_CONTEXTS. */
	public final static String PROCESSED_CONTEXTS = "processedContexts";

	/** The Constant STEP_ENTITY_LIST. */
	public final static String STEP_ENTITY_LIST = "stepEntityList";

	/** The Constant FEED_HELPER. */
	public final static String FEED_HELPER = "feedHelper";

	/** The Constant LINE_MAPPER_IS_REQUIRED. */
	public final static String LINE_MAPPER_IS_REQUIRED = "LineMapper is required";

	/** The Constant UNEXCEPTED_END_OF_FILE. */
	public final static String UNEXCEPTED_END_OF_FILE = "Unexpected end of file before record complete";

	/** The Constant UNABLE_TO_RAD_FROM_SOURCE. */
	public final static String UNABLE_TO_RAD_FROM_SOURCE = "Unable to read from resource :";

	/** The Constant RECORD_MUSTBE_OPENED. */
	public final static String RECORD_MUSTBE_OPENED = "Reader must be open before it can be read.";

	/** The Constant PARSING_EROR. */
	public final static String PARSING_EROR = "Parsing error at line:";

	/** The Constant USEAUTODEPLOY_WORKFLOW. */
	public final static String USEAUTODEPLOY_WORKFLOW = "useAutoDeployWorkflow";

	/** The Constant MANUAL_WORKFLOW. */
	public final static String MANUAL_WORKFLOW = "manualWorkflowName";

	/** The Constant AUTODEPLOY_WORKFLOW. */
	public final static String AUTODEPLOY_WORKFLOW = "autoWorkflowName";

	/** The Constant FILE_DOWNLOADER_FLAG. */
	public final static String FILE_DOWNLOADER_FLAG = "fileDownloaderFlag";

	/** The Constant FILE_UPLOADER_FLAG. */
	public final static String FILE_UPLOADER_FLAG = "fileUploaderFlag";

	/** The Constant FTP_SFTP_HOST. */
	public final static String FTP_SFTP_HOST = "hostName";

	/** The Constant FTP_SFTP_PORT. */
	public final static String FTP_SFTP_PORT = "hostPort";

	/** The Constant FPT_SFTP_USER. */
	public final static String FPT_SFTP_USER = "userName";

	/** The Constant FPT_SFTP_PASSWORD. */
	public final static String FPT_SFTP_PASSWORD = "password";

	/** The Constant FPT_SFTP_SOURCE_DIR. */
	public final static String FPT_SFTP_SOURCE_DIR = "sourceDirectory";

	/** The Constant PROTOCOL_TYPE. */
	public final static String PROTOCOL_TYPE = "protocolType";

	/** The Constant SFTP_PROTOCOL. */
	public final static String SFTP_PROTOCOL = "SFTP";

	/** The Constant FTP_PROTOCOL. */
	public final static String FTP_PROTOCOL = "FTP";

	/** The Constant FTP_REFUSED_CONNECTION. */
	public final static String FTP_REFUSED_CONNECTION = "FTP_REFUSED_CONNECTION";

	/** The Constant FILE_DOWNLOAD_FAILED. */
	public final static String FILE_DOWNLOAD_FAILED = "File Download Failed...";

	/** The Constant FILE_UPLOAD_FAILED. */
	public final static String FILE_UPLOAD_FAILED = "File Upload Failed...";

	/** The Constant DOWNLOAD_SUCCESS. */
	public final static String DOWNLOAD_SUCCESS = "FILES DOWNLOAD_SUCCESS ";

	/** The Constant NO_FILES_TO_DOWNLOAD. */
	public final static String NO_FILES_TO_DOWNLOAD = "NO_FILES_TO_DOWNLOAD";

	/** The Constant NO_FILES_TO_UPLOAD. */
	public final static String NO_FILES_TO_UPLOAD = "NO_FILES_TO_UPLOAD";

	/** The Constant SKIP_FILE_DOWNLOADING. */
	public final static String SKIP_FILE_DOWNLOADING = "SKIPPED FILE DOWNLOADING";

	/** The Constant SKIP_FILE_UPLOADING. */
	public final static String SKIP_FILE_UPLOADING = "SKIPPED FILE UPLOADING";

	/** The Constant MISSING_PARAM. */
	public final static String MISSING_PARAM = "Parameters Missing for Feed File Downloading ";

	/** The Constant CONNECTION_FAILED. */
	public final static String CONNECTION_FAILED = "Connection Failure..";

	/** The Constant DOWNLOAD_FILETYPE. */
	public final static String DOWNLOAD_FILETYPE = "downloadFileType";

	/** The Constant UPLOAD_FILETYPE. */
	public final static String UPLOAD_FILETYPE = "uploadFileType";

	/** The Constant UPLOAD_FILENAME. */
	public final static String UPLOAD_FILENAME = "uploadFileName";

	/** The Constant DOWNLOAD_FILENAME. */
	public final static String DOWNLOAD_FILENAME = "downloadFileName";

	/** The Constant STRICT_HOST_KEY_CHECKING. */
	public final static String STRICT_HOST_KEY_CHECKING = "StrictHostKeyChecking";

	/** The Constant NO_OPTION. */
	public final static String NO_OPTION = "no";

	/** The Constant STRING_ZERO. */
	public final static String STRING_ZERO = "0";

	/** The Constant ZIP_TYPE. */
	public final static String ZIP_TYPE = "zip";

	/** The Constant GZIP_TYPE. */
	public final static String GZIP_TYPE = "gzip";

	/** The Constant TAR_ZIP_TYPE. */
	public static final String TAR_ZIP_TYPE = "gz";

	/** The Constant FILE_UNZIP. */
	public final static String FILE_UNZIP = "File Unzip";

	/** The Constant BUFFER_SIZE_BYTE. */
	public final static int BUFFER_SIZE_BYTE = 1024;

	/** The Constant NO_CONTEXT_CREATED. */
	public final static String NO_CONTEXT_CREATED = "No Context is creted due to no Feed file found in the source";

	/** The Constant DATA_ALIGNMENT_FORMAT. */
	public final static String DATA_ALIGNMENT_FORMAT = "| %-15s | %-45s | %-15d | %-15d | %-15d | %-15d | %-15d | %-15d | %-17d | %-15d |";

	/** The Constant HEADER_ALIGNMENT_FORMAT. */
	public final static String HEADER_ALIGNMENT_FORMAT = "| %-15s | %-45s | %-15s | %-15s | %-15s | %-15s | %-15s | %-15s | %-17s | %-15s | %-20s | %-105s |";

	/** The Constant LINE_ONE_FORMAT. */
	public final static String LINE_ONE_FORMAT = "+-----------------+-----------------------------------------------+-----------------+-----------------+-----------------+";

	/** The Constant LINE_TWO_FORMAT. */
	public final static String LINE_TWO_FORMAT = "-----------------+-----------------+-----------------+-------------------+-----------------+----------------------+-----------------------------------------------------------------------------------------------------------+";

	/** The Constant TOTALTIME_FORMAT. */
	public final static String TOTALTIME_FORMAT = " %-20s |";

	/** The Constant EXCEPTION_FORMAT. */
	public final static String EXCEPTION_FORMAT = " %-105s |";

	/** The Constant READ_COUNT. */
	public final static String READ_COUNT = "Read Count";

	/** The Constant SKIP_COUNT. */
	public final static String SKIP_COUNT = "Skip Count";

	/** The Constant WRITE_COUNT. */
	public final static String WRITE_COUNT = "Write Count";

	/** The Constant WRITE_SKIP_COUNT. */
	public final static String WRITE_SKIP_COUNT = "WriteSkip Count";

	/** The Constant COMMIT_COUNT. */
	public final static String COMMIT_COUNT = "Commit Count";

	/** The Constant READ_SKIP_COUNT. */
	public final static String READ_SKIP_COUNT = "ReadSkip Count";

	/** The Constant ROLLBACK_COUNT. */
	public final static String ROLLBACK_COUNT = "Rollback Count";

	/** The Constant PROCESS_SKIP_COUNT. */
	public final static String PROCESS_SKIP_COUNT = "ProcessSkip Count";

	/** The Constant FILENAME. */
	public final static String FILENAME = "fileName";

	/** The Constant FILEPATH. */
	public final static String FILEPATH = "filePath";

	/** The Constant ENTITY_DATA. */
	public final static String ENTITY_DATA = "entity_data";

	/** The Constant ENTITY_SQL. */
	public final static String ENTITY_SQL = "entity_sql";

	/** The Constant ENTITY_LIST. */
	public final static String ENTITY_LIST = "entity_list";

	/** The Constant ENTITY_PROVIDER_LIST. */
	public final static String ENTITY_PROVIDER = "entity_provider";

	/** The Constant ENTITY_LIST. */
	public final static String ROOT_ELEMENT = "rootElement";
	/** The Constant ENTITY_MERCH_DATASOURCE. */
	public static final String ENTITY_MERCH_DATASOURCE = "publishing-dataSource";

	/** The Constant ENTITY_MERCH_DATASOURCE. */
	public static final String ENTITY_CORE_DATASOURCE = "production-dataSource";

	/** The Constant ENTITY_PROVIDER_FCQN. */
	public static final String DATA_LOADER_CLASS_FQCN = "dataLoaderClassFQCN";

	/** The Constant ENTITY_PROVIDER_FQCN. */
	public static final String ENTITY_PROVIDER_FQCN = "entityProviderFQCN";

	/** The Constant DATA_TYPE_TIMESTAMP. */
	public static final String DATA_TYPE_TIMESTAMP = "Timestamp";

	/** The Constant DATA_TYPE_FILE. */
	public static final String DATA_TYPE_FILE = "File";

	/** The Constant DATA_TYPE_BOOLEAN. */
	public static final String DATA_TYPE_BOOLEAN = "Boolean";

	/** The Constant DATA_TYPE_DOUBLE. */
	public static final String DATA_TYPE_DOUBLE = "Double";

	/** The Constant DATA_TYPE_INTEGER. */
	public static final String DATA_TYPE_INTEGER = "Integer";

	/** The Constant DATA_TYPE_LONG. */
	public static final String DATA_TYPE_LONG = "Long";

	/** The Constant DATA_TYPE_BYTE_ARRAY. */
	public static final String DATA_TYPE_BYTE_ARRAY = "byte[]";

	/** The Constant DATA_TYPE_DATE. */
	public static final String DATA_TYPE_DATE = "Date";

	/** The Constant DATA_TYPE_STRING. */
	public static final String DATA_TYPE_STRING = "String";

	/** The Constant SLASH. */
	public static final String SLASH = "/";

	/** The Constant COLON. */
	public static final String COLON = ":";

	/** The Constant UNDER_SCORE. */
	public static final String UNDER_SCORE = "_";

	/** The Constant IFUN. */
	public static final String IFUN = "-";

	/** The Constant BRACE_OPEN. */
	public static final String BRACE_OPEN = "{";

	/** The Constant BRACE_CLOSE. */
	public static final String BRACE_CLOSE = "}";

	/** The Constant DOT. */
	public static final String DOT = ".";

	/** The Constant PROPERTY_PROCESS_NAME. */
	public static final String PROPERTY_PROCESS_NAME = "processName";

	/** The Constant EXPORT. */
	public static final String EXPORT = "export";

	/** The Constant REPOSITORY_ITEM. */
	public static final String REPOSITORY_ITEM = "RepositoryItem";

	/** The Constant DATE_FORMAT_STRING. */
	public static final String DATE_FORMAT_STRING = "yyyy-mm-dd";

	/** The Constant TIMESTAMP_FORMAT_STRING. */
	public static final String TIMESTAMP_FORMAT_STRING = "MM_dd_yyyy_HH_mm_ss";

	/** The Constant TIMESTAMP_FORMAT_FOR_EXPORT. */
	public static final String TIMESTAMP_FORMAT_FOR_EXPORT = "yyyy_MM_dd_HH_mm_ss";

	/** The Constant TIMESTAMP_FORMAT_FOR_IMPORT. */
	public static final String TIMESTAMP_FORMAT_FOR_IMPORT = "MM/dd/yyyy HH:mm:ss";

	/** The Constant TIMESTAMP_FORMAT_FOR_RR. */
	public static final String TIMESTAMP_FORMAT_FOR_RR = "yyyy-MM-dd-hh:mm:ss:mmmmmm";

	/** The Constant FILE_TYPE_XML. */
	public static final String FILE_TYPE_XML = ".xml";

	/** The Constant NUMBER_ZERO. */
	public static final int NUMBER_ZERO = 0;

	/** The Constant NUMBER_ONE. */
	public static final int NUMBER_ONE = 1;

	/** The Constant NUMBER_TWO. */
	public static final int NUMBER_TWO = 2;

	/** The Constant NUMBER_THREE. */
	public static final int NUMBER_THREE = 3;

	/** The Constant NUMBER_FOUR. */
	public static final int NUMBER_FOUR = 4;

	/** The Constant NUMBER_FIVE. */
	public static final int NUMBER_FIVE = 5;
	
	/** The Constant NUMBER_SIX. */
	public static final int NUMBER_SIX = 6;

	/** The Constant NUMBER_THOUSAND. */
	public static final int NUMBER_THOUSAND = 1000;

	/** The Constant DATE_FORMAT. */
	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
			DATE_FORMAT_STRING, Locale.getDefault());

	/** The Constant DATE_FORMAT_IMPORT. */
	public static final SimpleDateFormat DATE_FORMAT_IMPORT = new SimpleDateFormat(
			TIMESTAMP_FORMAT_FOR_IMPORT, Locale.getDefault());

	/** The Constant JAXB_CONTEXT_REPOSITORY_DUMP. */
	public static final String JAXB_CONTEXT_REPOSITORY_DUMP = "com.data.repository";

	/** The Constant JAXB_CONTEXT_PROMOTIONS. */
	public static final String JAXB_CONTEXT_PROMOTIONS = "com.data.promotion";

	/** Constant for Email Report. */
	public static final String EMAIL_REPORT = "Report";

	/** Constant for Email Report Key. */
	public static final String EMAIL_REPORT_KEY = "report";

	/** Constant for Email Message Start. */
	public static final String EMAIL_MESSAGE_START = "message_start";

	/** Constant for Email Message End. */
	public static final String EMAIL_MESSAGE_END = "message_end";

	/** Constant for Email Report Details. */
	public static final String EMAIL_REPORT_DETAILS = "Details";

	/** Constant for Email Report Time stamp. */
	public static final String EMAIL_REPORT_TIMESTAMP = "Timestamp";

	/** Constant for Email Report Status. */
	public static final String EMAIL_REPORT_STATUS = "Status";

	/** Constant for Email Report File Name. */
	public static final String EMAIL_REPORT_FILE_NAME = "File name";

	/** Constant for Email Report Export Record Count. */
	public static final String EMAIL_REPORT_TOTAL_RECORD_COUNT = "Total records to export";

	/** Constant for Email Report Export Record Count. */
	public static final String EMAIL_REPORT_SUCCESSFUL_RECORD_COUNT = "No of records exported successfully";

	/** Constant for Email Report file move. */
	public static final String EMAIL_REPORT_FILE_MOVE = "File moved to";

	/** Constant for Email Report file move. */
	public static final String EMAIL_LOG_FILE = "Log File";

	/** Constant for Download Failed. */
	public static final String DOWNLOAD_FAIL = "Download Failed";

	/** Constant for Empty. */
	public static final String EMPTY = "";

	/** Constant for COMA SEPERATOR. */
	public static final String COMA_SEPERATOR = ",";

	/** Constant for Space. */
	public static final String SPACE = " ";

	/** Constant for Left Brace. */
	public static final String LEFT_BRACE = "[ ";

	/** Constant for Right Brace. */
	public static final String RIGHT_BRACE = "] ";

	/** Constant for Importance. */
	public static final String MESSAGE_IMPORTANCE = "Importance";

	/** Constant for Import Successful. */
	public static final String IMPORT_SUCCESSFULL = "Import Successfull";

	/** Constant for Export Successful. */
	public static final String EXPORT_SUCCESSFULL = "Export Successfull";

	/** Constant for NORMAL. */
	public static final String NORMAL = "Normal";

	/** Constant for Import Successful. */
	public static final String IMPORT_FAIL = "Import Failed";

	/** Constant for Export Successful. */
	public static final String EXPORT_FAIL = "Export Failed";

	/** Constant for Upload Successful. */
	public static final String UPLOAD_FAIL = "Upload Failed";

	/** Constant for UNABLE_TO_PROCESS. */
	public static final String UNABLE_TO_PROCESS = "UNABLE TO PROCESS THE FILE";

	/** Constant for High. */
	public static final String HIGH = "High";

	/** Constant for High. */
	public static final String MULTIPLE_PROCESSED_FOUND = "Multiple Processed Found For Same Request_id.";

	/** Constant for COUNTER_INCREMENT. */
	public static final int COUNTER_INCREMENT = 1;

	/** Constant for BUFFER_SIZE. */
	public static final int BUFFER_SIZE = 100000;

	/** Constant for no of minutes for an hour. */
	public static final int NO_OF_MINUTES_FOR_HOUR = 1440;

	/** Constant for Action Remove item. */
	public static final String ACTION_REMOVE = "remove";

	/** Constant for Action Update item. */
	public static final String ACTION_UPDATE = "update";

	/** Constant for Records import Fail. */
	public static final String EXCEPTION_RECORDS_IMPORT_FAIL = "Some Record are Failed To Import.";

	/** Constant for transform XSLT Null. */
	public static final String EXCEPTION_TRANSFORM_XSLT_NULL = "Unable To Transform as XSLT File Stream is null.";

	/** Constant for transform Input XSLT Null. */
	public static final String EXCEPTION_TRANSFORM_INPUTXML_NULL = "Unable To Transform as InputXML file is null.";

	/** Constant for transform Output XSLT Null. */
	public static final String EXCEPTION_TRANSFORM_OUTPUTXML_NULL = "Unable To Transform as OutputXML file is null.";

	/** Constant for transform Internal XSLT Null. */
	public static final String EXCEPTION_TRANSFORM_INTERNAL_ERROR = "Unable To Transform due to internal error.";

	/** Constant for Item Name message. */
	public static final String ITEM_NAME_MESSAGE = "ItemName [";

	/** Constant for Item id message. */
	public static final String ITEM_ID_MESSAGE = "] or ItemId [";

	/** Constant for Null message. */
	public static final String NULL_MESSAGE = "] should not be null.";

	/** Constant for Login. */
	public static final String PROFILE_LOGIN = "login";

	/** Constant for Internal profile user. */
	public static final String PROFILE_USER = "user";

	/** Constant for project item descriptor. */
	public static final String PROJECT_ITEM_NAME = "project";

	/** Constant for editable. */
	public static final String EDITABLE = "editable";

	/** Constant for Project Display Name. */
	public static final String PROJECT_DISPLAY_NAME = "displayName";

	/** Constant for Project Workspace Name. */
	public static final String PROJECT_WORKSPACE_NAME = "workspace";

	/** Constant For True. */
	public static final boolean TRUE = true;

	/** Constant For False. */
	public static final boolean FALSE = false;

	/** Constant For New Line. */
	public static final String NEW_LINE = "\n";

	/** Constant For Tab Space. */
	public static final String TAB = "\t";

	/** Constant For Tab Space. */
	public static final String TEXT_FILE_EXTENTION = ".txt";

	/** Constant For Tab Record Level Details. */
	public static final String RECORD_LEVEL_DETAILS = "Record Level Details are as following:";

	/** Constant For Tab Error Details. */
	public static final String ERROR_DETAILS = "Error Details:";

	/** Constant For Tab Error Details. */
	public static final String FOR = "for";

	/** Constant For Tab Error Details. */
	public static final String AT = "at";

	/** Constant for PRODUCT. */
	public static final String PRODUCT = "Product";

	/** Constant for Division ID. */
	public static final String CATALOG_UPDATES = "catalogUpdates";

	/** Constant for Division ID. */
	public static final String DIVISION_ID = "divisionId";

	/** Constant for Promotion Buy Item X Get Item Y. */
	public static final String PROMOTION_BUY_ITEM_X_GET_ITEM_Y = "Buy Item X Get Item Y";

	/** Constant for Promotion Buy One Get One. */
	public static final String PROMOTION_BUY_ONE_GET_ONE = "Buy One Get One";

	/** Constant for Promotion Get Item Discount. */
	public static final String PROMOTION_GET_ITEM_DISCOUNT = "Get Item Discount";

	/** Constant for Promotion Tiered Price Break. */
	public static final String PROMOTION_TIERED_PRICE_BREAK = "Tiered Price Break";

	/** Constant for Promotion Buy Item X Get Item Y. */
	public static final String PROMOTION_TEMPLATE_BUY_ITEM_X_GET_ITEM_Y = "/item/buyItemXGetItemY.pmdt";

	/** Constant for Promotion Buy One Get One. */
	public static final String PROMOTION_TEMPLATE_BUY_ONE_GET_ONE = "/item/bogo.pmdt";

	/** Constant for Promotion Get Item Discount. */
	public static final String PROMOTION_TEMPLATE_GET_ITEM_DISCOUNT = "/item/getItemDiscount.pmdt";

	/** Constant for Promotion Tiered Price Break. */
	public static final String PROMOTION_TEMPLATE_TIERED_PRICE_BREAK = "/item/tieredPriceBreakItemDiscount.pmdt";

	/** Constant for Promotion Tiered Price Break. */
	public static final String PROMOTION_TEMPLATE_DEFAULT = "/item/bogo.pmdt";

	/** Constant for Coupon ID. */
	public static final String COUPON_ID = "id";

	/** Constant for Coupon Display Name. */
	public static final String COUPON_DISPLAY_NAME = "displayName";

	/** Constant for Coupon. */
	public static final String COUPON = "Coupon";

	/** Constant for Promotion Offer PSC Value. */
	public static final String PROMOTION_OFFER_PSC_VALUE = "offer_psc_value";

	/** Constant for Promotion Discount Offer PSC Value. */
	public static final String PROMOTION_DISCOUNT_OFFER_PSC_VALUE = "PSC_value";

	/** Constant for Promotion Condition PSC Value. */
	public static final String PROMOTION_CONDITION_PSC_VALUE = "condition_psc_value";

	/** Constant for Promotion Discount Type Value. */
	public static final String PROMOTION_DISCOUNT_TYPE_VALUE = "discount_type_value";

	/** Constant for Promotion Discount Value. */
	public static final String PROMOTION_DISCOUNT_VALUE = "discount_value";

	/** Constant for Promotion Promotion No of Items to Discount. */
	public static final String PROMOTION_NO_OF_ITEMS_TO_DISCOUNT = "no_of_items_to_discount";

	/** Constant for Promotion No of Items to Buy. */
	public static final String PROMOTION_NO_OF_ITEMS_TO_BUY = "no_of_items_to_buy";

	/** Constant for Promotion Sort Order. */
	public static final String PROMOTION_SORT_ORDER = "sort_order";

	/** Constant for Promotion Give To Anonymous Profiles. */
	public static final String PROMOTION_GIVE_TO_ANONYMOUS_PROFILE = "giveToAnonymousProfiles";

	/** Constant for Promotion Allow Multiple. */
	public static final String PROMOTION_ALLOW_MULTIPLE = "allowMultiple";

	/** Constant for Promotion Global. */
	public static final String PROMOTION_GBLOBAL = "global";

	/** Constant for Promotion ID. */
	public static final String PROMOTION_ID = "id";

	/** Constant for Promotion Priority. */
	public static final String PROMOTION_PRIORITY = "priority";

	/** Constant for Promotion Enabled. */
	public static final String PROMOTION_ENABLED = "enabled";

	/** Constant for Promotion Display Name. */
	public static final String PROMOTION_DISPLAY_NAME = "displayNameDefault";

	/** Constant for Promotion Description. */
	public static final String PROMOTION_DESCRIPTION = "descriptionDefault";

	/** Constant for Promotion Time Until lExpire. */
	public static final String PROMOTION_TIME_UNTIL_EXPIRE = "timeUntilExpire";

	/** Constant for Promotion Max Uses. */
	public static final String PROMOTION_USES = "uses";

	/** Constant for Promotion relative Expiration. */
	public static final String PROMOTION_RELATEIVE_EXPIRATION = "relativeExpiration";

	/** Constant for Promotion Type. */
	public static final String PROMOTION_TYPE = "type";

	/** Constant for Promotion Begin Usable. */
	public static final String PROMOTION_BEGIN_USABLE = "beginUsable";

	/** Constant for Promotion End Usable. */
	public static final String PROMOTION_END_USABLE = "endUsable";

	/** Constant for Promotion Sites. */
	public static final String PROMOTION_SITES = "sites";

	/** Constant for Promotion Action Add. */
	public static final String PROMOTION_ACTION_ADD = "ACTION_ADD";

	/** Constant for Promotion Action Update. */
	public static final String PROMOTION_ACTION_UPDATE = "ACTION_UPDATE";

	/** Constant for Promotion Operator Name. */
	public static final String PROMOTION_OPERATOR = "<operator name=\"";

	/** Constant for Promotion Comparator Name. */
	public static final String PROMOTION_COMPARATOR = "<comparator name=\"";

	/** Constant for Promotion Includes. */
	public static final String PROMOTION_COMPARATOR_INCLUDES = "includes";

	/** Constant for Promotion Includes. */
	public static final String PROMOTION_COMPARATOR_INCLUDES_OF = "includesallof";

	/** Constant for Promotion Includes. */
	public static final String PROMOTION_COMPARATOR_INCLUDES_ANY_OF = "includesanyof";

	/** Constant for Promotion Category List. */
	public static final String PROMOTION_CATEGORIES_LIST = "<value>item.auxiliaryData.productRef.ancestorCategoryIds</value><constant><data-type>java.util.Collection</data-type>";

	/** Constant for Promotion Category Item. */
	public static final String PROMOTION_CATEGORY_ITEM = "<value>item.auxiliaryData.productRef.ancestorCategoryIds</value><constant><data-type>java.lang.String</data-type>";

	/** Constant for Promotion Category Item. */
	public static final String PROMOTION_PRODUCT_ITEM = "<value>item.auxiliaryData.productRef.id</value><constant><data-type>java.lang.String</data-type>";

	/** Constant for Promotion Category Item. */
	public static final String PROMOTION_PRODUCTS_LIST = "<value>item.auxiliaryData.productRef.id</value><constant><data-type>java.util.Collection</data-type>";

	/** Constant for Promotion Category Item. */
	public static final String PROMOTION_SKU_ITEM = "<value>item.auxiliaryData.catalogRef.id</value><constant><data-type>java.lang.String</data-type>";

	/** Constant for Promotion Category Item. */
	public static final String PROMOTION_SKUS_LIST = "<value>item.auxiliaryData.catalogRef.id</value><constant><data-type>java.util.Collection</data-type>";

	/** Constant for Promotion Name IS. */
	public static final String PROMOTION_COMPARATOR_IS = "equals";

	/** Constant for Promotion Name IS NOT. */
	public static final String PROMOTION_COMPARATOR_IS_NOT = "not-equals";

	/** Constant for Promotion Name IS ONE OF. */
	public static final String PROMOTION_COMPARATOR_IS_ONE_OF = "isoneof";

	/** Constant for Promotion Name IS NOT ONE OF. */
	public static final String PROMOTION_COMPARATOR_IS_NOT_ONE_OF = "isnotoneof";

	/** Constant for Promotion Display Name for BOGO. */
	public static final String PROMOTION_BOGO_DISPLAY_NAME = "display-name-L2l0ZW0vYm9nby5wbWR0";

	/** Constant for Promotion Display Name for Buy Item X Get Item Y. */
	public static final String PROMOTION_BUY_ITEM_X_GET_ITEM_Y_DISPLAY_NAME = "display-name-L2l0ZW0vYnV5SXRlbVhHZXRJdG";

	/** Constant for Promotion Display Name for Get Item Discount. */
	public static final String PROMOTION_GET_ITEM_DISCOUNT_DISPLAY_NAME = "display-name-L2l0ZW0vZ2V0SXRlbURpc2NvdW";

	/** Constant for Promotion Display Name Value Tiered Price Break. */
	public static final String PROMOTION_TIERED_PRICE_BREAK_DISPLAY_NAME = "display-name-L2l0ZW0vdGllcmVkUHJpY2VCcm";

	/** Constant for Promotion Display Name Value for BOGO. */
	public static final String PROMOTION_BOGO_DISPLAY_NAME_VALUE = "BOGO";

	/** Constant for Promotion Display Name Value for Buy Item X Get Item Y. */
	public static final String PROMOTION_BUY_ITEM_X_GET_ITEM_Y_DISPLAY_NAME_VALUE = "Buy Item X Get Item Y";

	/** Constant for Promotion Display Name Value for Get Item Discount. */
	public static final String PROMOTION_GET_ITEM_DISCOUNT_DISPLAY_NAME_VALUE = "Get Item Discount (by Product, Category, Brand, etc.)";

	/** Constant for Promotion Display Name Value for Get Item Discount. */
	public static final String PROMOTION_TIERED_PRICE_BREAK_DISPLAY_NAME_VALUE = "Tiered Price Break";

	/** Constant for Promotion End tag. */
	public static final String PROMOTION_END_TAG = "\">";

	/** Constant for Promotion category. */
	public static final String PROMOTION_CATEGORY = "category";

	/** Constant for Promotion product. */
	public static final String PROMOTION_PRODUCT = "product";

	/** Constant for Promotion sku. */
	public static final String PROMOTION_SKU = "sku";

	/** Constant for Promotion Start String Value. */
	public static final String PROMOTION_START_STRING_VALUE = "<string-value>";

	/** Constant for Promotion End String Value. */
	public static final String PROMOTION_END_STRING_VALUE = "</string-value>";

	/** Constant for Promotion Comparator End tag. */
	public static final String PROMOTION_COMAPRATOR_END_TAG = "</constant></comparator>";

	/** Constant for Promotion Start Discount Detail. */
	public static final String PROMOTION_START_DISCOUNT_DETAIL = "<discount-detail>";

	/** Constant for Promotion End Discount Detail. */
	public static final String PROMOTION_END_DISCOUNT_DETAIL = "</discount-detail>";

	/** Constant for Promotion Discount Structure. */
	public static final String PROMOTION_START_DISCOUNT_STRUCTURE = "<discount-structure calculator-type=\"bulk\" discount-type=";

	/** Constant for Promotion End Discount Structure. */
	public static final String PROMOTION_END_DISCOUNT_STRUCTURE = "</discount-structure>";

	/** Constant for Promotion Start Discount Quotes. */
	public static final String PROMOTION_START_QUOTES = "\"";

	/** Constant for Promotion End Discount Quotes. */
	public static final String PROMOTION_END_QUOTES = "\">";

	/** Constant for Promotion End Discount Quotes Tag. */
	public static final String PROMOTION_END_QUOTES_TAG = "\">";

	/** Constant for Promotion Attribute Name. */
	public static final String PROMOTION_ATTRIBUTE_NAME = "<attribute name=\"";

	/** Constant for Promotion Attribute Value. */
	public static final String PROMOTION_ATTRIBUTE_VALUE = "value=\"";

	/** Constant for Promotion Start Discount Detail. */
	public static final String PROMOTION_START_DISCOUNT_STRUCTURE_PLACEHOLDER = "discountStructure";

	/** Constant for Promotion End Discount Detail. */
	public static final String PROMOTION_END_DISCOUNT_STRUCTURE_PLACEHOLDER = "endDiscountStructure";

	/** Constant for Promotion Mandatory Information. */
	public static final String PROMOTION_MANDATORY_INFORMATION = "Mandatory Informations are missing";

	/** Constant for Promotion Creation Failed. */
	public static final String PROMOTION_CREATION_FAILED = "Promotion Creation Failed due to Invalid data recieved from feed file";

	/** Constant for Price list prefix. */
	public static final String PRICE_LIST_PREFIX = "l";

	/** Constant for Five Empty Zeros. */
	public static final String FIVE_EMPTY_ZEROS = "00000";

	/** Constant for Four Empty Zeros. */
	public static final String FOUR_EMPTY_ZEROS = "0000";

	/** Constant for Three Empty Zeros. */
	public static final String THREE_EMPTY_ZEROS = "000";

	/** Constant for Two Empty Zeros. */
	public static final String TWO_EMPTY_ZEROS = "00";

	/** Constant for Empty Zero. */
	public static final String EMPTY_ZERO = "0";

	/** Constant for Empty Zero. */
	public static final String SKU_FULLFILLER = "SoftgoodFulfiller";

	/** Constant for Minus One. */
	public static final long MINUS_ONE = -1;

	/** Constant for Gift Card Template Name. */
	public static final String GIFTCARD_TEMPLATE_NAME = "_GiftCardTemplate";

	/** Constant for SKU Media External for Category. */
	public static final String CATEGORY_URL = "?$cat$";

	/** Constant for SKU Media External for Product. */
	public static final String PRODUCT_URL = "?$prod$";

	/** Constant for SKU Media External for Shop. */
	public static final String SHOP_URL = "sw?$shop$";

	/** Constant for SKU Media External for Category. */
	public static final String CATEGORY_EXTENCTION = "_cat";

	/** Constant for SKU Media External for Product. */
	public static final String PRODUCT_EXTENCTION = "_prod";

	/** Constant for SKU Media External for Shop. */
	public static final String SHOP_EXTENCTION = "_shop";

	/** Constant for UTF_FORMAT. */
	public static final String UTF_FORMAT = "UTF-8";

	/** Constant for UTF16_FORMAT. */
	public static final String UTF16_FORMAT = "UTF-16";
	/** The Constant PREPROCESS. */
	public static final String PREPROCESS = "Preprocess";

	/** The Constant PROCESS. */
	public static final String PROCESS = "Process";

	/** The Constant POSTPROCESS. */
	public static final String POSTPROCESS = "Post Process";

	/** The Constant TOTAL_TIME_OF_EXECUTION. */
	public static final String TOTAL_TIME_OF_EXECUTION = "Total time of execution ";

	/** The Constant TOTAL_TIME_OF_EXECUTION. */
	public static final String STEP_EXECUTION_TIME = "Step Execution Time";

	/** The Constant COMMITTED_RECORD_COUNT. */
	public static final String COMMITTED_RECORD_COUNT = "Committed Record Count";

	/** The Constant SKIPPED_RECORD_COUNT. */
	public static final String SKIPPED_RECORD_COUNT = "Skipped Record Count";

	/** The Constant MULTI_DELIMITER. */
	public static final String MULTI_DELIMITER = ",";

	/** The Constant CHILD_CATEGORY_PROPERTY. */
	public static final String CHILD_CATEGORY_PROPERTY = "fixedChildCategories";

	/** The Constant CATEORY_ITEM_DESCRIPTOR_NAME. */
	public static final String CATEORY_ITEM_DESCRIPTOR_NAME = "category";

	/** The Constant SUBCATEORY_ITEMDESCRIPTOR. */
	public static final String SUBCATEORY_ITEMDESCRIPTOR = "subCategory";

	/** The Constant PRODUCT_ITEMDESCRIPTOR. */
	public static final String PRODUCT_ITEMDESCRIPTOR = "product";

	/** The Constant FINISHED. */
	public static final String FINISHED = "FINISHED";

	/** The Constant CONTINUE. */
	public static final String CONTINUE = "CONTINUE";

	/** The Constant NO_INSTANCE_MSG. */
	public static final String NO_INSTANCE_MSG = "No job instance found for job=";

	/** The Constant NO_JOBLAUNCHER_PROVIDED_MSG. */
	public static final String NO_JOBLAUNCHER_PROVIDED_MSG = "A JobLauncher must be provided.  Please add one to the configuration.";

	/** The Constant NO_JOBEXPLORER_PROVIDED_MSG. */
	public static final String NO_JOBEXPLORER_PROVIDED_MSG = "A JobExplorer must be provided for a restart or start next operation.  Please add one to the configuration.";

	/** The Constant EQUAL. */
	public static final String EQUAL = "=";

	/** The Constant RESTART. */
	public static final String RESTART = "-restart";

	/** The Constant NEXT. */
	public static final String NEXT = "-next";

	/** The Constant JOB_TERMINATED_ERR_MSG. */
	public static final String JOB_TERMINATED_ERR_MSG = "Job Terminated in error: ";

	/** The Constant JOB_FAIL_MSG. */
	public static final String JOB_FAIL_MSG = "No failed or stopped execution found for job=";

	/** The Constant JOB_FAIL_NO_PARAM_MSG. */
	public static final String JOB_FAIL_NO_PARAM_MSG = "No job parameters incrementer found for job=";

	/** The Constant JOB_FAIL_NO_BOOTSTRAP_PARAM_MSG. */
	public static final String JOB_FAIL_NO_BOOTSTRAP_PARAM_MSG = "No bootstrap parameters found from incrementer for job=";

	/** The Constant HYPHEN. */
	public static final String HYPHEN = "-";

	/** The Constant JOB_FAIL_NO_PARAM_JOB_NAME. */
	public static final String JOB_FAIL_NO_PARAM_JOB_NAME = "At least 1 argument required: JobName.";

	/** The Constant COMPLETED. */
	public static final String COMPLETED = " COMPLETED";

	/** The Constant FAILED. */
	public static final String FAILED = "FAILED";

	/** The Constant ERROR. */
	public static final String ERROR = "Error";

	/** The Constant PARSE_FILE_MSG. */
	public static final String PARSE_FILE_MSG = "From parse File";

	/** The Constant SKU_ITEMDESCRIPTOR. */
	public static final String SKU_ITEMDESCRIPTOR = "sku";

	/** Incrmental Queue **/

	/**
	 * Constant to hold CONTENT_ID property.
	 */
	public static final String CONTENT_ID = "DEFAULT";

	
	/** Constant for holding SEARCH_CONFIG. */
	public static final String SEARCH_CONFIG = "searchConfig";

	
	/** Constant for holding NEXT_INCREMENTAL. */
	public static final String NEXT_INCREMENTAL = "nextIncrementalGeneration";

	/** The Constant FAILUR_DATA_ENTITY_PROVIDER. */
	public static final String FAILUR_DATA_ENTITY_PROVIDER = "failure_data_entity_provider";

	/** The Constant IS_ADDFLAG. */
	public static final String IS_ADDFLAG = "isAddFlag";

	/** The Constant SKU. */
	public static final String SKU = "sku";

	public static final CharSequence HTTP = "http://";

	/** The Constant FEED_FILE_TAIL_RECORD_KEY. */
	public static final String FEED_FILE_TAIL_RECORD_KEY = "TRAILER";
	/**
	 * The Constant PROCESSING_DIRECTORY.
	 */
	public static final String PROCESSING_DIRECTORY = "processingDirectory";
	
	/** The Constant ALERT_LOGGER. */
	public static final String ALERT_LOGGER = "Alert logger";

}
