package com.tru.feedprocessor.tol;




/**
 * The Class FeedConstants.
 * 
 *  @version 1.1
 *  @author Professional Access
 */
public class BaseFeedConstants {
	
	/** The Constant STOPPED. */
	public static final String STOPPED = "STOPPED";

	/** The Constant UNKNOWN. */
	public static final String UNKNOWN = "UNKNOWN";

	/** The Constant JOB_NAME. */
	public static final String JOB_NAME = "TestCatalog11";

	/** The Constant CATEGORY_ITEMDESCRIPTOR. */
	public static final String CATEGORY_ITEMDESCRIPTOR = "category";

	/** The Constant MEDIA_ITEMDESCRIPTOR. */
	public static final String MEDIA_ITEMDESCRIPTOR = "media";

	/** The Constant GENERIC_ERROR_ID. */
	public static final String GENERIC_ERROR_ID = "generic_error_id";
	
	/** The Constant FILE_DOES_NOT_EXISTS. */
	public static final String FILE_DOES_NOT_EXISTS = "File does not exits";
	
	/** The Constant TABLE. */
	public static final String TABLE = "table";
	
	/** The Constant RELATIONSHIP_MAPPER. */
	public static final String RELATIONSHIP_MAPPER = "Relationship mapper";
	
	/** The Constant ENTITY_IDS. */
	public static final String ENTITY_IDS = "Entity_IDs";
	
	/** The Constant CAT_FEED_CONFIG_PATH. */
	public static final String CAT_FEED_CONFIG_PATH = "classpath*:catFeedConfig.xml";
	
	/** The Constant INV_FEED_CONFIG_PATH. */
	public static final String INV_FEED_CONFIG_PATH = "classpath*:invFeedConfig.xml";
	
	/** The Constant STORE_FEED_CONFIG_PATH. */
	public final static String STORE_FEED_CONFIG_PATH="classpath*:storeFeedConfig.xml";
	
	/** The Constant RROUT_FEED_CONFIG_PATH. */
	public final static String RROUT_FEED_CONFIG_PATH="classpath*:rrOutFeedConfig.xml";
	
	/** The Constant RRIN_FEED_CONFIG_PATH. */
	public final static String RRIN_FEED_CONFIG_PATH="classpath*:rrInFeedConfig.xml";
	
	/** The Constant RRPIE_FEED_CONFIG_PATH. */
	public final static String RRPIE_FEED_CONFIG_PATH="classpath*:rrPIEFeedConfig.xml";

	/** The Constant PRICE_FEED_CONFIG_PATH. */
	public final static String PRICE_FEED_CONFIG_PATH="classpath*:priceFeedConfig.xml";
	
	/** The Constant RECOMMANDATION_FEED_CONFIG_PATH. */
	public final static String RECOMMANDATION_FEED_CONFIG_PATH="classpath*:recommendationFeedConfig.xml";
	
	/** The Constant JOB_CONFIG_PATH. */
	public static final String JOB_CONFIG_PATH = "classpath*:JobConfig.xml";
	
	/** The Constant STORE_JOB_NAME. */
	public final static String STORE_JOB_NAME="storeFeed";
	
	/** The Constant RROUT_JOB_NAME. */
	public final static String RROUT_JOB_NAME="outFeed";
	
	/** The Constant RRIN_JOB_NAME. */
	public final static String RRIN_JOB_NAME="inFeed";
	
	/** The Constant RRPIE_FEED_JOB_NAME. */
	public final static String RRPIE_FEED_JOB_NAME="rrPIEFeed";
	
	/** The Constant PRICE_JOB_NAME. */
	public final static String PRICE_JOB_NAME="priceFeed";
	
	/** The Constant INV_JOB_NAME. */
	public final static String INV_JOB_NAME="invFeed";
	
	/** The Constant CAT_JOB_NAME. */
	public final static String CAT_JOB_NAME="catalogFeed";
	
	/** The Constant RECOMMANDATION_JOB_NAME. */
	public final static String RECOMMANDATION_JOB_NAME="recommendationFeed";
	
	/** The Constant PHASE_NAME. */
	public static final String PHASE_NAME = "PhaseName";
	
	/** The Constant CATALOG_REPOSITORY. */
	public static final String CATALOG_REPOSITORY = "catalogRepository";
	
	/** The Constant INVENTORY_ITEMDESCRIPTOR. */
	public static final String INVENTORY_ITEMDESCRIPTOR = "InventoryRepository";
	
	/** The Constant XML. */
	public static final String XML = "xml";
	
	/** The Constant CSV. */
	public static final String CSV = "csv";
	
	/** The Constant FILETYPE. */
	public static final String FILETYPE = "fileType";
	
	/** The Constant CSVELEMENTNAMES. */
	public static final String CSVELEMENTNAMES = "CSVFieldNames";
	
	/** The Constant STARTED. */
	public static final String STARTED = " started";
	
	/** The Constant ENDED. */
	public static final String ENDED = " ended";
	
	/** The Constant PROJECT_NAME. */
	public static final String PROJECT_NAME = "projectName";
	
	/** The Constant FILE. */
	public static final String FILE = "file:///";
	
	/** The Constant WRITER_REPOSITORY. */
	public static final String WRITER_REPOSITORY = "repository";
	
	/** The Constant ENTITYTOREPOSITORYNAMEMAP. */
	public static final String ENTITYTOREPOSITORYNAMEMAP = "entityToRepositoryNameMap";
	
	/** The Constant ENTITYTOITEMDESCRIPTONNAMEMAP. */
	public static final String ENTITYTOITEMDESCRIPTONNAMEMAP = "entityToItemDescriptorNameMap";
	
	/** The Constant SKIPPEDEXCEPTIONS. */
	public static final String SKIPPEDEXCEPTIONS = "skippedExceptions";
	
	/** The Constant UNPROCESSED_EXCEPTION. */
	public static final String UNPROCESSED_EXCEPTION = "unProcessedException";
	
	/** The Constant EXCEPTIONS. */
	public static final String EXCEPTIONS = "Exceptions";
	
	/** The Constant SKIPPEDRECORDS. */
	public static final String SKIPPEDRECORDS = "skippedRecords";
	
	/** The Constant QUOTE. */
	public static final String QUOTE = "'";
	
	/** The Constant QUESTIONMARK. */
	public static final String QUESTIONMARK = "?";
	
	/** The Constant HASH. */
	public static final String HASH = "#";
	
	/** The Constant STAR. */
	public static final String STAR = "*";
	
	/** The Constant REGX_PATTERN. */
	public static final String REGX_PATTERN = "^.";
	
	/** The Constant VERSIONED. */
	public static final String VERSIONED = "VERSIONED";
	
	/** The Constant NONVERSIONED. */
	public static final String NONVERSIONED = "NONVERSIONED";

	/** The Constant DEFAULTPRICELIST. */
	public static final String DEFAULTPRICELIST = "defaultPriceList";
	
	/** The Constant LINESTOSKIP. */
	public static final String LINESTOSKIP = "linestoSkip";
	
	/** The Constant PRICELIST. */
	public static final String PRICELIST = "priceList";
	
	/** The Constant CLOSING_BRACKET. */
	public static final String CLOSING_BRACKET = ")";
	
	/** The Constant PRICE_OR_QUERY. */
	public static final String PRICE_OR_QUERY = "OR sku_id in (";
	
	/** The Constant WORKSPACE. */
	public static final String WORKSPACE = "workspace";
	
	/** The Constant BRAND_ITEMDESCRIPTOR_NAME. */
	public static final String BRAND_ITEMDESCRIPTOR_NAME = "brand";
	
	/** The Constant FROM. */
	public static final String FROM = "from";
	
	/** The Constant TO. */
	public static final String TO = "to";
	
	/** The Constant INVENTORY_LIST. */
	public static final String INVENTORY_LIST = "INVENTORY_LIST";
	
	/** The Constant RANGE_MAP. */
	public static final String RANGE_MAP = "RANGE_MAP";
	
	/** The Constant MULTITHREAD. */
	public static final String MULTITHREAD = "MultiThread";

	/** The Constant PRICE_ENTITY_NAME. */
	public static final String PRICE_ENTITY_NAME = "price";

	/** The Constant LIST_PRICES. */
	public static final String LIST_PRICES = "listPrices";
	
	public static final String LIST_PRICE = "listPrice";
	
	/** The Constant SALE_PRICE. */
	public static final String SALE_PRICE = "salePrices";

	/** The Constant PARTITION. */
	public static final String PARTITION = "partition";
	
	/** The Constant GRID_SIZE. */
	public static final int GRID_SIZE = 10;

	/** The Constant STEP_NAME. */
	public static final String STEP_NAME = "StepName ";
	
	/** The Constant NUMBER_SIXTYTHOUSANDS. */
	public static final int NUMBER_SIXTYTHOUSANDS = 60000;
	
	/** The Constant DISPLAY_NAME. */
	public static final String DISPLAY_NAME = "displayName";
	
	/** The Constant REFRESH_CACHE. */
	public static final String REFRESH_CACHE = "refreshCache";
	
	/** The Constant YES. */
	public static final String YES = "yes";

	/** The Constant ROOT_ELEMENT_ATTRIBUTES. */
	public final static String ROOT_ELEMENT_ATTRIBUTES = "rootElementAttributes";
	
	/** The Constant REVIEWRATING_ITEMDESCRIPTOR. */
	public static final String REVIEWRATING_ITEMDESCRIPTOR = "bazaarvoiceReviewRating";
	
	/** The Constant BAZAARVOICEREVIEWRATING. */
	public static final String BAZAARVOICEREVIEWRATING = "bazaarvoiceReviewRating";
	
	/** The Constant ROOT_TAG. */
	public static final String ROOT_TAG = "xmlRootTag";
	
	/** The Constant ENTITY_TO_GROUP_NAME. */
	public static final String ENTITY_TO_GROUP_NAME = "entityToGroupName";
	
	/** The Constant ENTITYTORQLMAP. */
	public static final String ENTITYTORQLMAP = "entityToRqlMap";
	
	/** The Constant ENTITY_VO_MAP. */
	public static final String ENTITY_VO_MAP = "entityVOMap";
	
	/** The Constant ALIASES. */
	public static final String ALIASES = "aliases";
	
	/** The Constant FIELD_ALIASES. */
	public static final String FIELD_ALIASES = "fieldAliases";
	
	/** The Constant CATEGORY. */
	public static final String CATEGORY = "Category";
	
	/** The Constant OMITTEDFIELDS. */
	public static final String OMITTEDFIELDS = "omittedField";
	
	/** The Constant DATE_FORMAT_NOW. */
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd hh:mm:ss:mmmmmm";
	
	/** The Constant XML_EXTRACTDATE. */
	public static final String XML_EXTRACTDATE = "extractDate";
	
	/** The Constant ANNOTATEDCLASSES. */
	public static final String ANNOTATEDCLASSES = "annotatedClasses";
	
	/** The Constant BRAND. */
	public static final String BRAND = "Brand";
	
	/** The Constant DAYS. */
	public static final String DAYS = "Days";
	
	/** The Constant HOURS. */
	public static final String HOURS = "Hours";
	
	/** The Constant MINUTES. */
	public static final String MINUTES = "Minutes";
	
	/** The Constant SECONDS. */
	public static final String SECONDS = "Seconds";
	
	/** The Constant AND. */
	public static final String AND = "and";
	
	/** The Constant NO_FEED_CONTEXT_TO_PROCESS. */
	public static final Object NO_FEED_CONTEXT_TO_PROCESS = "No feed file in context to process";
	
	/** The Constant TOTAL_EXECUTION_TIME_IN_MILLI_SECONDS. */
	public static final String TOTAL_EXECUTION_TIME_IN_MILLI_SECONDS = "Total Execution Time in Millisecond ";
	
	/** The Constant TOTAL_EXECUTION_TIME_OF_CURRENT_FEED. */
	public static final String TOTAL_EXECUTION_TIME_OF_CURRENT_FEED = "Total Execution Time of current Feed ";

	/** The Constant DOLLER_ONE. */
	public static final String DOLLER_ONE = "$1";
	
	/** The Constant OUTPUT_RESOURCE_EXIST. */
	public static final String OUTPUT_RESOURCE_EXIST = "Output resource must exist";
	
	/** The Constant AUTOMATICENDELEMENTS. */
	public static final String AUTOMATICENDELEMENTS = "com.ctc.wstx.automaticEndElements";
	
	/** The Constant OUTPUT_VALIDATESTRUCTURE. */
	public static final String OUTPUT_VALIDATESTRUCTURE = "com.ctc.wstx.outputValidateStructure";
	
	/** The Constant UNABLE_TO_WRITE_TO_FILE_RESOURCE. */
	public static final String UNABLE_TO_WRITE_TO_FILE_RESOURCE = "Unable to write to file resource: [";
	
	/** The Constant WITH_ENCODING. */
	public static final String WITH_ENCODING = "] with encoding=[";
	
	/** The Constant CLOSING_BRACE. */
	public static final String CLOSING_BRACE = "] ";
	
	/** The Constant XMLNS. */
	public static final String XMLNS = "xmlns";
	
	/** The Constant XMLNS_G. */
	public static final String XMLNS_G = "xmlns:g";
	
	/** The Constant XMLNS_C. */
	public static final String XMLNS_C = "xmlns:c";
	
	/** The Constant REGISTERING_PREFIX. */
	public static final String REGISTERING_PREFIX = "registering prefix: ";
	
	/** The Constant XML_END_QUOTES. */
	public static final String XML_END_QUOTES = "</";
	
	/** The Constant END_QUOTE. */
	public static final String END_QUOTE = ">";
	
	/** The Constant UNABLE_TO_CLOSE_FILE_RESOURCE. */
	public static final String UNABLE_TO_CLOSE_FILE_RESOURCE = "Unable to close file resource: [";
	
	/** The Constant FAILED_TO_DELETE_EMPTY_FILE_ON_CLOSE. */
	public static final String FAILED_TO_DELETE_EMPTY_FILE_ON_CLOSE = "Failed to delete empty file on close";
	
	/** The Constant MARSHALLER_MUST_SUPPORT_MESSAGE. */
	public static final String MARSHALLER_MUST_SUPPORT_MESSAGE = "Marshaller must support the class of the marshalled object";
	
	/** The Constant FAILED_TO_FLUSH_THE_EVENTS. */
	public static final String FAILED_TO_FLUSH_THE_EVENTS = "Failed to flush the events";
	
	/** The Constant THE_RESOURCE_MUST_BE_SET. */
	public static final String THE_RESOURCE_MUST_BE_SET = BaseFeedConstants.CLOSING_BRACE;
	
	/** The Constant FAILED_TO_WRITE_HEADER_ITEMS. */
	public static final String FAILED_TO_WRITE_HEADER_ITEMS = "Failed to write headerItems";
	
	/** The Constant INVALID_FILE. */
	public static final String INVALID_FILE = "Invalid File";
	
	/** The Constant NUMBER_SIXTY. */
	public static final int NUMBER_SIXTY = 60;
	
	/** The Constant NUMBER_TWENTYFOUR. */
	public static final int NUMBER_TWENTYFOUR = 24;
	
	/** The Constant NUMBER_MINUS_ONE. */
	public static final int NUMBER_MINUS_ONE = -1;

	/** The Constant DEFAULT_ENCODING. */
	public static final String DEFAULT_ENCODING = "UTF-8";
	
	/** The Constant DEFAULT_XML_VERSION. */
	public static final String DEFAULT_XML_VERSION = "1.0";
	
	/** The Constant DEFAULT_ROOT_TAG_NAME. */
	public static final String DEFAULT_ROOT_TAG_NAME = "root";
	
	/** The Constant RESTART_DATA_NAME. */
	public static final String RESTART_DATA_NAME = "position";
	
	/** The Constant WRITE_STATISTICS_NAME. */
	public static final String WRITE_STATISTICS_NAME = "record.count";
	
	/** The Constant REGULER_EXPRESSION_1. */
	public static final String REGULER_EXPRESSION_1 = "\\{(.*)\\}.*";
	
	/** The Constant REGULER_EXPRESSION_2. */
	public static final String REGULER_EXPRESSION_2 = "\\{.*\\}(.*)";
	
	/** The Constant REGULER_EXPRESSION_3. */
	public static final String REGULER_EXPRESSION_3 = "(.*):.*";
	
	/** The Constant REGULER_EXPRESSION_4. */
	public static final String REGULER_EXPRESSION_4 = ".*:(.*)";
	
	/** The Constant LONG_ZERO. */
	public static final long LONG_ZERO = 0L;
	
	/** The Constant STORE. */
	public static final String STORE = "store";
	
	/** The Constant TRADINGHOURS. */
	public static final String TRADINGHOURS = "tradingHours";
	
	/** The Constant LOAD_DATA_ERROR. */
	public static final String LOAD_DATA_ERROR = "LoadData Failed.., Empty data in Feed.[Check configuration for 'fileType' and 'dataLoaderClassFQCN' along with the class is correct..]";
	
	/** The Constant ITEM. */
	public static final String ITEM = "item";
	
	/** The Constant FORWORD_SLASH. */
	public static final String FORWORD_SLASH = "/";

	/**
	 * Constant for PRICE_PROCESSOR.
	 */
	public static final String PRICE_PROCESSOR = "price";
	
	/** The Constant INVENTORY_PROCESSOR. */
	public static final CharSequence INVENTORY_PROCESSOR = "inventory";
	/**
	 * Constant for ATG.
	 */
	public static final String ATG = "/atg";
	
	/** The Constant SERVER_URL. */
	public static final String SERVER_URL = "serverUrl";	 
	
	/** The Constant XML_GREGORIAN_CALENDER_FORMAT. */
	public static final String XML_GREGORIAN_CALENDER_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
	
	/** The Constant AUTODETECTANNOTATIONS. */
	public static final String AUTODETECTANNOTATIONS = "autodetectAnnotations";
	
	/** The Constant SIMPLE_DATE_FORMAT. */
	public static final String SIMPLE_DATE_FORMAT = "yyyy-MM-dd HH-mm-ss";
	
	/** The Constant MAPPER. */
	public static final String MAPPER = "Mapper";
	
	/** The Constant INTERACTION. */
	public static final String INTERACTION = "interaction";
	
	/** The Constant BUSINESS_ID_REQUIRED. */
	public static final String BUSINESS_ID_REQUIRED = "BusinessId mus be set for the entity '";
	
	/** The Constant TO_RESOLVE_DUPLICATE. */
	public static final String TO_RESOLVE_DUPLICATE = "' to resolve duplicate feed in entity.";

}
