/**
 * 
 */
package com.tru.feedprocessor.tol.base;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import atg.repository.MutableRepository;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.config.IFeedConfig;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;


/**
 * The Class FeedHelper 
 *
 * This is the class which will provide the service related to configuration and FeedDataHolder
 * This Class achievable in every step of execution
 * 
 * 
 * @version 1.0
 * @author Professional Access
 */
public class FeedHelper {

	/** property to hold feed data holder */
	private FeedDataHolder mFeedDataHolder;

	/** property to hold feed config */
	private IFeedConfig mFeedConfig;

	/**
	 * @param pFeedDataHolder
	 *            the feedDataHolder to set
	 */
	public void setFeedDataHolder(FeedDataHolder pFeedDataHolder) {
		mFeedDataHolder = pFeedDataHolder;
	}

	/**
	 * @param pFeedConfig
	 *            the feedConfig to set
	 */
	public void setFeedConfig(IFeedConfig pFeedConfig) {
		mFeedConfig = pFeedConfig;
	}

	/**
	 * 
	 * @return - CurrentFeedExecutionContext
	 */
	public FeedExecutionContextVO getCurrentFeedExecutionContext() {
		return (FeedExecutionContextVO) mFeedDataHolder.getData(FeedConstants.CURRENT_FEEDCONTEXT);
	}

	/**
	 * 
	 * @return the entity list
	 */

	public List<String> getEntityList() {
		List<String> lEntityList = getEntityList(getCurrentFeedExecutionContext(),mFeedConfig);
		if (null != lEntityList && !lEntityList.isEmpty()) {
			return lEntityList;
		}
		return null;
	}
	/**
	 * 
	 * @param pExecutionConfig - pExecutionConfig
	 * @param pJobConfig - pJobConfig
	 * @return entity list
	 */
	@SuppressWarnings("unchecked")
	protected List<String> getEntityList(FeedExecutionContextVO pExecutionConfig,
			IFeedConfig pJobConfig) {
		List<String> lEntityList = (List<String>) pExecutionConfig.getConfigValue(FeedConstants.ENTITY_LIST);
		
		return	(List<String>) (lEntityList == null ? mFeedConfig.getConfigValue(FeedConstants.ENTITY_LIST):lEntityList);
	}

	/**
	 * 
	 * @param pStepName - pStepName
	 * @return step entity list
	 */
	public List<String> getStepEntityList(String pStepName) {
		List<String> lEntityList = null;
		Map<String, String> lMap = null;
		lMap = getStepEntityList(pStepName,getCurrentFeedExecutionContext(),mFeedConfig);
		if (!lMap.isEmpty() && lMap.containsKey(pStepName)) {
			lEntityList = Arrays.asList(lMap.get(pStepName).split(FeedConstants.COMA_SEPERATOR));
		} else {
			lEntityList = getEntityList();
		}
		return lEntityList;

	}
	/**
	 * 
	 * @param pStepName - pStepName
	 * @param pExecutionConfig - pExecutionConfig
	 * @param pJobConfig - pJobConfig
	 * @return step entity list
	 */
	@SuppressWarnings("unchecked")
	protected Map<String, String> getStepEntityList(String pStepName,IFeedConfig pExecutionConfig,
			IFeedConfig pJobConfig) {
		Map<String, String> lMap = (Map<String, String>) pExecutionConfig.getConfigValue(FeedConstants.STEP_ENTITY_LIST);
		
		return	lMap == null ? (Map<String, String>) mFeedConfig.getConfigValue(FeedConstants.STEP_ENTITY_LIST):lMap ;
	}

	/**
	 * Gets the repository.
	 * 
	 * @param pRepoName
	 *            the repo name
	 * @return the repository
	 */
	@SuppressWarnings("unchecked")
	public MutableRepository getRepository(String pRepoName) {
		return (MutableRepository) (getCurrentFeedExecutionContext().getRepository(pRepoName) != null
				&& !((List<String>) getCurrentFeedExecutionContext().getRepository(pRepoName)).isEmpty() ? getCurrentFeedExecutionContext()
						.getRepository(pRepoName): mFeedConfig.getRepository(pRepoName));
	}
	/**
	 * 
	 * @return ItemDescriptorName
	 */ 
	@SuppressWarnings("unchecked")
	public Map<String, String> getEntityToItemDescriptorNameMap() {
		return (Map<String, String>) (getCurrentFeedExecutionContext().getEntityToItemDescriptorNameMap() != null
				&& !getCurrentFeedExecutionContext().getEntityToItemDescriptorNameMap().isEmpty() ? getCurrentFeedExecutionContext()
				.getEntityToItemDescriptorNameMap(): mFeedConfig.getConfigValue(FeedConstants.ENTITYTOITEMDESCRIPTONNAMEMAP));
	}

	/**
	 * 
	 * @return RepositoryName
	 */ 
	@SuppressWarnings("unchecked")
	public Map<String, String> getEntityToRepositoryNameMap() {
		return (Map<String, String>) (getCurrentFeedExecutionContext().getEntityToItemDescriptorNameMap() != null
				&& !getCurrentFeedExecutionContext().getEntityToItemDescriptorNameMap().isEmpty() ? getCurrentFeedExecutionContext()
				.getEntityToRepositoryNameMap(): mFeedConfig.getConfigValue(FeedConstants.ENTITYTOREPOSITORYNAMEMAP));
	}

	/**
	 * @return the entityToRepositoryNameMap
	 */
	@SuppressWarnings("unchecked")
	public Map<String, String> getEntityToRepository() {
		return (Map<String, String>) (getCurrentFeedExecutionContext().getEntityToRepositoryNameMap() != null
				&& !getCurrentFeedExecutionContext().getEntityToRepositoryNameMap().isEmpty() ? getCurrentFeedExecutionContext()
				.getEntityToItemDescriptorNameMap(): mFeedConfig.getConfigValue(FeedConstants.ENTITYTOREPOSITORYNAMEMAP));
	}

	/**
	 * Save data.
	 * 
	 * @param pCongifKey
	 *            the congif key
	 * @param pConfigValue
	 *            the config value
	 */
	public void saveData(String pCongifKey, Object pConfigValue) {
		mFeedDataHolder.setData(pCongifKey, pConfigValue);

	}

	/**
	 * Retrive data.
	 * 
	 * @param pDataMapKey
	 *            the data map key
	 * @return the object
	 */
	public Object retriveData(String pDataMapKey) {
		return mFeedDataHolder.getData(pDataMapKey);
	}

	/**
	 * 
	 * @param pConfigKey - pConfigKey
	 * @return value
	 */
	public Object getConfigValue(String pConfigKey) {
		return getCurrentFeedExecutionContext()==null ?  mFeedConfig.getConfigValue(pConfigKey) : getConfigValue(pConfigKey,getCurrentFeedExecutionContext(),mFeedConfig);
	}
	/**
	 * 
	 * @param pConfigKey - pConfigKey
	 * @param pExecutionconfig - pExecutionconfig
	 * @param pJobConfig - pJobConfig
	 * @return value
	 */
	protected Object getConfigValue(String pConfigKey,IFeedConfig pExecutionconfig,IFeedConfig pJobConfig) {
		Object lConfigValue=pExecutionconfig.getConfigValue(pConfigKey);
		return lConfigValue==null ? pJobConfig.getConfigValue(pConfigKey):lConfigValue;
	}

	/**
	 * 
	 * @param pName - pName
	 * @param pValue - pValue
	 */
	public void setConfigValue(String pName, Object pValue) {
		mFeedConfig.setConfigValue(pName, pValue);
	}
	/**
	 * 
	 * @param pEntityName - pEntityName
	 * @return item descriptor
	 */
	public String getEntityToItemDescriptorNameMap(String pEntityName) {
		return getEntityToItemDescriptorNameMap().get(pEntityName);
	}
	/**
	 * 
	 * @param pRepositoryName - pRepositoryName
	 * @return item descriptor
	 */
	public String getEntityToRepositoryNameMap(String pRepositoryName) {
		return getEntityToRepositoryNameMap().get(pRepositoryName);
	}

}
