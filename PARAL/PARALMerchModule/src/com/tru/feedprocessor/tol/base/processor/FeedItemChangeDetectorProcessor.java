package com.tru.feedprocessor.tol.base.processor;

import java.util.Map;

import atg.nucleus.ServiceMap;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Class FeedItemChangeDetectorProcessor.
 *
 * ChangeDetectorProcessor will inspect is there any change information in VO and repository.
 * and in <code>doProcess</code> default is <code>true</code> for <code>isDataUpdated</code>
 * 
 * It will take the effect of implemented code from the interface <code>lItemValueChangeInspector.isUpdated()</code> 
 * Team has to customized this code in there layer for Add/Update or not 
 * 
 * If <code>isDataUpdated</code> is false it will not do any thing in repository level during writing the data.
 * if <code>isDataUpdated</code> is true it will take effect for add/update in repository during writing the data. 
 * 
 * @version 1.0
 * @author Professional Access
 */
public class FeedItemChangeDetectorProcessor extends FeedItemValidateProcessor{
	
	/**
	 * The identifer id store key.
	 */
	private String mIdentifierIdStoreKey;
	
	/**
	 * ItemValueChangedMapper.
	 */
	private ServiceMap mItemValueChangeInspector;
	
	/**
	 * Gets the identifier id store key.
	 * 
	 * @return the identifieridStoreKey
	 */
	public String getIdentifierIdStoreKey() {
		return mIdentifierIdStoreKey;
	}
	
	/**
	 * Sets the identifier id store key.
	 * 
	 * @param pIdentifierIdStoreKey
	 *            the identifierIdStoreKey to set
	 */
	public void setIdentifierIdStoreKey(String pIdentifierIdStoreKey) {
		mIdentifierIdStoreKey = pIdentifierIdStoreKey;
	}
	
	/**
	 * Gets the item value change inspector.
	 * 
	 * @return the ItemValueChangeInspector
	 */
	public ServiceMap getItemValueChangeInspector() {
		return mItemValueChangeInspector;
	}
	
	/**
	 * Sets the item value change inspector.
	 * 
	 * @param pItemValueChangeInspector
	 *            - pItemValueChangeInspector
	 */
	public void setItemValueChangeInspector(
			ServiceMap pItemValueChangeInspector) {
		mItemValueChangeInspector = pItemValueChangeInspector;
	}
	
	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.base.processor.FeedItemValidateProcessor#doProcess(com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO)
	 */
	@Override
	public BaseFeedProcessVO doProcess(BaseFeedProcessVO pFeedVO)
			throws FeedSkippableException, RepositoryException {
		
		super.doProcess(pFeedVO);
	 
	 	boolean isDataUpdated=true;
		
	 	Map<String, Map<String, String>> lEntityIdMap =	(Map<String, Map<String, String>>) retriveData(getIdentifierIdStoreKey());		
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
		if (lEntityIdMap != null && (String)lEntityIdMap.get(lItemDiscriptorName).get(pFeedVO.getRepositoryId()) != null) {
			if(getItemValueChangeInspector()!=null &&!getItemValueChangeInspector().isEmpty() ){
					IItemValueChangeInspector lItemValueChangeInspector = (IItemValueChangeInspector) getItemValueChangeInspector().get(pFeedVO.getEntityName());
					RepositoryItem lCurrentItem =  getRepository(lRepositoryName).getItem(pFeedVO.getRepositoryId(), lItemDiscriptorName);
					if(lItemValueChangeInspector!=null && lCurrentItem!=null){
						isDataUpdated=lItemValueChangeInspector.isUpdated(pFeedVO, lCurrentItem);
					}
				}
			if(isDataUpdated){
				return pFeedVO;
			}else{
				return null;
			}
		}
		return pFeedVO;
	}

}
