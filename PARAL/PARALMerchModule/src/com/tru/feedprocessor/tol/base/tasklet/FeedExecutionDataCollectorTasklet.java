package com.tru.feedprocessor.tol.base.tasklet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.feedprocessor.tol.base.vo.StepInfoVO;
/**
 * The class FeedExecutionDataCollectorTasklet 
 * 
 * Notification Information of each process Context.
 *
 * @version 1.1
 * @author Professional Access
 */
public class FeedExecutionDataCollectorTasklet extends AbstractTasklet{

	/**
	 * The Temp step executions.
	 */
	private Collection<StepExecution> mTempStepExecutions = null;
	
	
	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.base.tasklet.AbstractTasklet#doExecute()
	 */
	@Override
	protected void doExecute() throws FeedSkippableException {
		//TODO method stub
	}
	
	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.base.tasklet.AbstractTasklet#execute(org.springframework.batch.core.StepContribution, org.springframework.batch.core.scope.context.ChunkContext)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public RepeatStatus execute(StepContribution pStepContribution,
			ChunkContext pChunkContext) throws Exception {
		
		super.execute(pStepContribution, pChunkContext);
		
		Collection<StepExecution> lStepExecutions = pChunkContext
				.getStepContext().getStepExecution().getJobExecution()
				.getStepExecutions();
		
		ArrayList<StepExecution> list=new ArrayList<StepExecution>(lStepExecutions);
		
		if(mTempStepExecutions!= null){
			list.removeAll(mTempStepExecutions);
		}
		
		List<StepInfoVO> lStepInfos = new ArrayList<StepInfoVO>();
		FeedExecutionContextVO lFeedExecutionContextVO = getCurrentFeedExecutionContext();
		
		for (StepExecution stepExecution : list) {
			StepInfoVO stepInfo = new StepInfoVO();
			stepInfo.setEndTime(stepExecution.getEndTime());

			List<Throwable> stepExceptions =stepExecution.getFailureExceptions();

			if(stepExceptions!=null && !stepExceptions.isEmpty()){
				for(Throwable exe:stepExceptions){
					stepInfo.setFeedFailureProcessException(exe);
				}
			}
			if(null != lFeedExecutionContextVO){
				stepInfo.setFeedSkippedDataExceptions(lFeedExecutionContextVO.getFeedSkippedStepExceptions(stepExecution.getStepName()));
			}
			stepInfo.setPhaseName((String)stepExecution.getExecutionContext().get(FeedConstants.PHASE_NAME));
			stepInfo.setReadCount(stepExecution.getReadCount());
			stepInfo.setSkipCount(stepExecution.getSkipCount());
			stepInfo.setStartTime(stepExecution.getStartTime());
			stepInfo.setStepName(stepExecution.getStepName());
			stepInfo.setWriteCount(stepExecution.getWriteCount());
			stepInfo.setCommitCount(stepExecution.getCommitCount());
			stepInfo.setReadSkipCount(stepExecution.getReadSkipCount());
			stepInfo.setProcessSkipCount(stepExecution.getProcessSkipCount());
			stepInfo.setRollbackCount(stepExecution.getRollbackCount());
			lStepInfos.add(stepInfo);
		}
	
		mTempStepExecutions = lStepExecutions;
		
		if(null != lFeedExecutionContextVO){
			lFeedExecutionContextVO.setStepInfos(lStepInfos);
		}
		
		List<FeedExecutionContextVO> lProcessedExecutionContextList = (List<FeedExecutionContextVO>) retriveData(FeedConstants.PROCESSED_CONTEXTS) ;
		
		if(null == lProcessedExecutionContextList ){
			lProcessedExecutionContextList = new ArrayList<FeedExecutionContextVO>();
			 saveData(FeedConstants.PROCESSED_CONTEXTS,lProcessedExecutionContextList);
		}	 
		
		lProcessedExecutionContextList.add(lFeedExecutionContextVO);
		
		
		return RepeatStatus.FINISHED;
	}
}
