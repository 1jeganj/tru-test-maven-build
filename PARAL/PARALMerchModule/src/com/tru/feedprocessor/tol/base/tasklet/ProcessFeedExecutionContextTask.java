package com.tru.feedprocessor.tol.base.tasklet;

import java.util.Queue;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class ProcessFeedExecutionContextTask.
 * 
 * Saved current Feed context and Feed context has to be process
 *
 * @version 1.1
 * @author Professional Access
 */
public class ProcessFeedExecutionContextTask extends AbstractTasklet {
	
	
	@Override
	protected void doExecute() throws FeedSkippableException {
		
		Queue<FeedExecutionContextVO> executionContextQueue = (Queue)retriveData(FeedConstants.FEEDCONTEXT_QUEUE);
		
		FeedExecutionContextVO currentFeedCntxVo = executionContextQueue.poll();
		
		saveData(FeedConstants.CURRENT_FEEDCONTEXT, currentFeedCntxVo);
		saveData(FeedConstants.HAS_MORE_CONTEXT, (executionContextQueue.peek() == null)? false : true);
	}

}