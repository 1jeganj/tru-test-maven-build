package com.tru.feedprocessor.tol.base.tasklet;

import java.util.List;
import java.util.Map;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.EntityDataProvider;
import com.tru.feedprocessor.tol.base.loader.IFeedDataLoader;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class FeedDataLoaderTask.
 * 
 * Parse the file and load into memory 
 * 
 * @version 1.1
 * @author Professional Access
 */
public class FeedDataLoaderTask extends AbstractTasklet {

	/** The EntityDataProvider. */
	private IFeedDataLoader mIFeedDataLoader; 
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}

	/**
	 * Gets the feed data loader.
	 * 
	 * @return the mIFeedDataParser
	 */
	public IFeedDataLoader getFeedDataLoader() {
		return mIFeedDataLoader;
	}	

	/**
	 * Sets the feed data loader.
	 * 
	 * @param pIFeedDataLoader
	 *            the pIFeedDataLoader to set
	 */
	public void setFeedDataLoader(IFeedDataLoader pIFeedDataLoader) {
		mIFeedDataLoader = pIFeedDataLoader;
	}
	
	
	/**
	 * Save entity data.
	 * 
	 * @param pEntityDataProvider
	 *            the ENTITY_PROVIDER
	 */
	protected void saveEntityData(EntityDataProvider pEntityDataProvider){
		saveData(FeedConstants.ENTITY_PROVIDER, pEntityDataProvider);
	}

	/**
	 * Parses the file.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	protected void loadData() throws Exception {
		EntityDataProvider lEntityDataProvider= null;
		FeedExecutionContextVO curExecCntx = getCurrentFeedExecutionContext();
		curExecCntx.setConfigValue(FeedConstants.ALERT_LOGGER, getAlertLogger());
		Map<String, List<? extends BaseFeedProcessVO>> lEntityVOsMap = mIFeedDataLoader.loadData(curExecCntx);
		lEntityDataProvider = new EntityDataProvider(lEntityVOsMap);
		saveEntityData(lEntityDataProvider);
	}

	/**
	 * Do execute.
	 *
	 * @throws Exception the exception
	 * @see com.tru.feedprocessor.tol.AbstractTasklet#doExecute()
	 */
	@Override
	protected void doExecute() throws Exception {

		String dataLoaderClassFQCN = (String) getConfigValue(FeedConstants.DATA_LOADER_CLASS_FQCN);
		if(dataLoaderClassFQCN !=null)
		{
			IFeedDataLoader fileParser = (IFeedDataLoader) Class.forName(dataLoaderClassFQCN).newInstance();
			setFeedDataLoader(fileParser);
		 }
		loadData();

	}
	
}
