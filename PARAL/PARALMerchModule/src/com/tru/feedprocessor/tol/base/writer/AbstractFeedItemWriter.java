package com.tru.feedprocessor.tol.base.writer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.scope.context.StepSynchronizationManager;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemWriter;

import atg.nucleus.ServiceMap;
import atg.repository.MutableRepository;
import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.FeedHelper;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;


/**
 * The Class AbstractFeedItemWriter 
 * 
 * <p> 
 * Writer for open, write and close in implemented layer 
 * The default implementation for required stepName, phaseName uniqueInsertList, uniqueUpdateList, ServieMap,JobParamMap
 * will process. So that while implementing team can achieve the required details. 
 * </p>
 *
 * @version 1.1
 * @author Professional Access
 */
public abstract class AbstractFeedItemWriter implements
		ItemWriter<BaseFeedProcessVO>, ItemStream {
	
	/** The mLogger. */
	private FeedLogger mLogger;

	/** The mphase name. */
	private String mPhaseName;

	/** The mStepName name. */
	private String mStepName;

	/** The m feed data helper. */
	private FeedHelper mFeedHelper;
	
	/** The m mapper map. */
	private ServiceMap mMapperMap = null;
	
	/** The mJobParameterMap. */
	private Map<String, JobParameter> mJobParameterMap;
	
	/** The Unique sub list. */
	private ThreadLocal<List<BaseFeedProcessVO>> mUniqueInsertItemList = new ThreadLocal<List<BaseFeedProcessVO>>();
	
	/** The Unique sub list. */
	private ThreadLocal<List<BaseFeedProcessVO>> mUniqueUpdateItemList = new ThreadLocal<List<BaseFeedProcessVO>>();
	
	/**
	 * Sets the data helper.
	 * 
	 * @param pFeedHelper
	 *            the new feed helper
	 */
	public void setFeedHelper(FeedHelper pFeedHelper) {
		mFeedHelper = pFeedHelper;
	}

	/**
	 * Gets the feed helper.
	 * 
	 * @return the mFeedHelper
	 */
	public FeedHelper getFeedHelper() {
		return mFeedHelper;
	}

	/**
	 * Gets the step name.
	 * 
	 * @return the mStepName
	 */
	public String getStepName() {
		return mStepName;
	}

	/**
	 * Gets the phase name.
	 * 
	 * @return the phase name
	 */
	public String getPhaseName() {
		return mPhaseName;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the mLogger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the new logger
	 */
	public void setLogger(FeedLogger pLogger) {
		this.mLogger = pLogger;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.batch.item.ItemStream#open(org.springframework.batch
	 * .item.ExecutionContext)
	 */
	@Override
	public void open(ExecutionContext pExecutioncontext)
			throws ItemStreamException {
			
			mStepName=StepSynchronizationManager.getContext().getStepName();
			mPhaseName=(String)(StepSynchronizationManager.getContext().getStepExecution().getExecutionContext().get(FeedConstants.PHASE_NAME));
			mJobParameterMap = StepSynchronizationManager.getContext().getStepExecution().getJobParameters().getParameters();
		
			mUniqueInsertItemList.set(new ArrayList<BaseFeedProcessVO>());
			mUniqueUpdateItemList.set(new ArrayList<BaseFeedProcessVO>());
	
			doOpen();
	}

	/**
	 * Do open.
	 * 
	 * @throws ItemStreamException
	 *             the item stream exception
	 */
	public void doOpen() throws ItemStreamException {
		//TODO method stub
	}


	/**
	 * Do write.
	 * 
	 * @param pArg0
	 *            the arg0
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	public abstract void doWrite(List<? extends BaseFeedProcessVO> pArg0)
			throws RepositoryException, FeedSkippableException;
			

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
	 */
	@Override
	public void write(List<? extends BaseFeedProcessVO> pFeedVO)
			throws FeedSkippableException, RepositoryException {
			doWrite(pFeedVO);
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.batch.item.ItemStream#close()
	 */
	@Override
	public void close() throws ItemStreamException {
		getCurrentFeedExecutionContext().addFeedItemsToUpdateList(mUniqueUpdateItemList.get());
		getCurrentFeedExecutionContext().addFeedItemsToInsertList(mUniqueInsertItemList.get());
		doClose();
	}

	/**
	 * Do close.
	 * 
	 * @throws ItemStreamException
	 *             the item stream exception
	 */
	public void doClose() throws ItemStreamException {
		//TODO method stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.batch.item.ItemStream#update(org.springframework.
	 * batch.item.ExecutionContext)
	 */
	@Override
	public void update(ExecutionContext pArg0) throws ItemStreamException {
			doUpdate();
	}

	/**
	 * Do update.
	 * 
	 * @throws ItemStreamException
	 *             the item stream exception
	 */
	public void doUpdate() throws ItemStreamException {
		//TODO method stub
	}
	
	/**
	 * Gets the mapper map.
	 * @return the mapper map
	 */
	public ServiceMap getMapperMap(){
		return mMapperMap;
	}

	/**
	 * Sets the mapper map.
	 * 
	 * @param pMapperMap the new mapper map
	 */
	public void setMapperMap(ServiceMap pMapperMap){
		mMapperMap = pMapperMap;
	}
	
	/**
	 * Gets the current feed execution context.
	 * 
	 * @return - CurrentFeedExecutionContext
	 */
	public FeedExecutionContextVO getCurrentFeedExecutionContext() {
		return  getFeedHelper().getCurrentFeedExecutionContext();
	}
	/**
	 * Retrive data.
	 * 
	 * @param pDataMapKey
	 *            the data map key
	 * @return the object
	 */
	public Object retriveData(String pDataMapKey) {
		return getFeedHelper().retriveData(pDataMapKey);
	}
	/**
	 * Save data.
	 * 
	 * @param pConfigKey
	 *            the pConfigKey
	 * @param pConfigValue
	 *            the config value
	 */
	public void saveData(String pConfigKey, Object pConfigValue) {
		 getFeedHelper().saveData(pConfigKey, pConfigValue);
	}
	
	/**
	 * Gets the entity list.
	 * 
	 * @return the entity list
	 */
	public List<String> getEntityList() {
		return getFeedHelper().getStepEntityList(getStepName());
	}
	
	/**
	 * Gets the config value.
	 * 
	 * @param pConfigKey
	 *            the pConfigKey
	 * @return value
	 */
	public Object getConfigValue(String pConfigKey) {
		return getFeedHelper().getConfigValue(pConfigKey);
	}
	
	/**
	 * Sets the config value.
	 * 
	 * @param pName
	 *            - pName
	 * @param pValue
	 *            - pValue
	 */
	public void setConfigValue(String pName, Object pValue) {
		getFeedHelper().setConfigValue(pName, pValue);
	}

	/**
	 * Gets the repository.
	 * 
	 * @param pRepoName
	 *            the repositoryName
	 * @return repository
	 */
	public MutableRepository getRepository(String pRepoName) {
		return getFeedHelper().getRepository(pRepoName);
	}
	
	/**
	 * Gets the item descriptor name.
	 * 
	 * @param pEntityName
	 *            - pEntityName
	 * @return item descriptor name
	 */
	public String getItemDescriptorName(String pEntityName){
		return getFeedHelper().getEntityToItemDescriptorNameMap(pEntityName);
		
	}
	
	/**
	 * Gets the entity repository name.
	 * 
	 * @param pEntityName
	 *            - pEntityName
	 * @return repository name
	 */
	public String getEntityRepositoryName(String pEntityName){
		return getFeedHelper().getEntityToRepositoryNameMap(pEntityName);
	}
	
	
	/**
	 * Gets the job parameter.
	 * 
	 * @param pJobKey
	 *            - pJobKey
	 * @return the JobParameter
	 */
	public Object getJobParameter(String pJobKey){
		return mJobParameterMap.get(pJobKey).getValue();
	}

	/**
	 * Adds the insert item to list.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the pBaseFeedProcessVO to set
	 */
	protected void addInsertItemToList(BaseFeedProcessVO pBaseFeedProcessVO) {
		mUniqueInsertItemList.get().add(pBaseFeedProcessVO);
	}
	
	/**
	 * Adds the update item to list.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the pBaseFeedProcessVO
	 */
	protected void addUpdateItemToList(BaseFeedProcessVO pBaseFeedProcessVO) {
		mUniqueUpdateItemList.get().add(pBaseFeedProcessVO);
	}
	/**
     * Log error message.
     * 
      * @param pMessage
     *            the message
     * @param pException
     *            the exception
     */
     public void logError(Object pMessage, Throwable pException) {
            getLogger().logErrorMessage(pMessage, pException);
            
     }

     

     /**
     * @return true/false
     */
     public boolean isLoggingError()
       {
         return getLogger().isLoggingError();
       }
     
     /**
      * Gets the unique insert item list.
      *
      * @return the unique insert item list
      */
     public List<BaseFeedProcessVO> getUniqueInsertItemList(){
    	 return mUniqueInsertItemList.get();
     }
     
     /**
      * Gets the unique update item list.
      *
      * @return the unique update item list
      */
     public List<BaseFeedProcessVO> getUniqueUpdateItemList(){
    	 return mUniqueUpdateItemList.get();
     }

}
