package com.tru.feedprocessor.tol.base.decider;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;

import com.tru.feedprocessor.tol.FeedConstants;

/**
 * The Class FileTypeDecider.
 * 
 * @version 1.1
 * @author Professional Access
 */
public class FileTypeDecider extends AbstractFeedDecider {

	/**
	 * Do decide.
	 *
	 * @param pJobexecution the jobexecution
	 * @param pStepexecution the stepexecution
	 * @return the flow execution status
	 * @see org.springframework.batch.core.job.flow.JobExecutionDecider#decide(org
	 * .springframework.batch.core.JobExecution,
	 * org.springframework.batch.core.StepExecution)
	 */
	@Override
	public FlowExecutionStatus doDecide(JobExecution pJobexecution,
			StepExecution pStepexecution) {
		String fileType = ((String)getConfigValue(FeedConstants.FILETYPE));
		return new FlowExecutionStatus(fileType.toUpperCase());
	}

}
