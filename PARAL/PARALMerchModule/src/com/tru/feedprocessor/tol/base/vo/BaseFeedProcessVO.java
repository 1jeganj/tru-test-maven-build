package com.tru.feedprocessor.tol.base.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

/**
 * The Class BaseFeedProcessVO..
 * 
 * @version 1.1
 * @author Professional Access
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class BaseFeedProcessVO {
	
	/** The m business id. */
	@XmlTransient
	private String mBusinessId;
	
	/** The m repository id. */
	@XmlTransient
	private String mRepositoryId;
	
	/** The m item discriptor name. */
	@XmlTransient
	private String mEntityName;
	/** The m Skipped name. */
	@XmlTransient
	private boolean mSkipped;
	
	/**
	 * Gets the business id.
	 *
	 * @return the business id
	 */
	public String getBusinessId() {
		return mBusinessId;
	}
	
	/**
	 * Sets the business id.
	 *
	 * @param pBusinessId the new business id
	 */
	public void setBusinessId(String pBusinessId) {
		mBusinessId = pBusinessId;
	}
	
	/**
	 * Gets the repository id.
	 *
	 * @return the repository id
	 */
	public String getRepositoryId() {
		return mRepositoryId;
	}
	
	/**
	 * Sets the repository id.
	 *
	 * @param pRepositoryId the new repository id
	 */
	public void setRepositoryId(String pRepositoryId) {
		mRepositoryId = pRepositoryId;
	}
	
	/**
	 * Gets the item discriptor name.
	 *
	 * @return the item discriptor name
	 */
	public String getEntityName() {
		return mEntityName;
	}
	
	/**
	 * Sets the item discriptor name.
	 *
	 * @param pEntityName the new entity name
	 */
	public void setEntityName(String pEntityName) {
		mEntityName = pEntityName;
	}

	/**
	 * @return the mSkipped
	 */
	public boolean isSkipped() {
		return mSkipped;
	}


	/**
	 * @param pSkipped - pSkipped
	 */
	public void setSkipped(boolean pSkipped) {
		this.mSkipped = pSkipped;
	}
	
}
