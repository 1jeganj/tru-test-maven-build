package com.tru.feedprocessor.tol.base.tasklet;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class SingleFileCreateExecutionContext.
 * 
 * create executionContext queue for a single feed 
 * 
 * @version 1.1
 * @author Professional Access
 */
public class SingleFileCreateExecutionContext extends AbstractCreateFeedExecutionContext{

	@Override
	protected Queue<FeedExecutionContextVO> createExecutionContext() {
		
		Queue executionContextQueue = new LinkedBlockingQueue<FeedExecutionContextVO>();

		FeedExecutionContextVO feedExecutionContextVO = new FeedExecutionContextVO();
		
		feedExecutionContextVO.setFileName((String)getConfigValue(FeedConstants.FILENAME)+FeedConstants.DOT+getConfigValue(FeedConstants.FILETYPE));
		feedExecutionContextVO.setFilePath((String)getConfigValue(FeedConstants.FILEPATH));
		
		executionContextQueue.add(feedExecutionContextVO);
		
		return executionContextQueue;
	}

	
}
