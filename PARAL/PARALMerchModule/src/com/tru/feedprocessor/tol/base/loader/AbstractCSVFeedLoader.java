package com.tru.feedprocessor.tol.base.loader;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.batch.item.ReaderNotOpenException;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.batch.item.file.LineCallbackHandler;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.NonTransientFlatFileException;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.separator.RecordSeparatorPolicy;
import org.springframework.batch.item.file.separator.SimpleRecordSeparatorPolicy;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;


/**
 * The Class AbstractCSVFeedLoader.
 * 
 * Team has to extend for  this class for parsing CSV file and store the data into VO map 
 * 
 * The default implementation of loading and parsing CSV file in to memory. The class store the parse date in to a VOHolderMap
 * The implemented class need to supply some parameter for corresponding corresponding VO class name and supported parameter 
 * through the component to the implemented class.
 * 
 * @version 1.1
 * @author Professional Access
 */
public abstract class AbstractCSVFeedLoader implements IFeedDataLoader {
	
	/** Property to hold logging. */
	public static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(AbstractCSVFeedLoader.class);
	
	/**
	 * the mVOHolderMap.
	 */
	protected Map<String, List<? extends BaseFeedProcessVO>> mVOHolderMap;

	/**
	 * the mResolveDupVOHolderMap.
	 */
	private Map<String, Map<String,BaseFeedProcessVO>> mResolveDupVOHolderMap;
	
	/**
	 * the lBaseFeedVO.
	 */
	private BaseFeedProcessVO mBaseFeedVO = null;

	/**
	 * the DEFAULT_CHARSET.
	 */

	public static final String DEFAULT_CHARSET = Charset.defaultCharset().name();

	/**
	 * the mRecordSeparatorPolicy.
	 */
	private RecordSeparatorPolicy mRecordSeparatorPolicy = new SimpleRecordSeparatorPolicy();

	/**
	 * the mReader.
	 */
	private BufferedReader mReader = null;

	/**
	 * the mLineCount.
	 */
	private int mLineCount = FeedConstants.NUMBER_ZERO;

	/**
	 * the mComments.
	 */
	private String[] mComments = { FeedConstants.HASH };

	/**
	 * the mNoInput.
	 */
	private boolean mNoInput = false;

	/**
	 * the mLineMapper.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private LineMapper<BaseFeedProcessVO> mLineMapper = new DefaultLineMapper();
	
	/**
	 * the mDelemetedTokenizer.
	 */
	private DelimitedLineTokenizer mDelemetedTokenizer= new DelimitedLineTokenizer();
	
	/**
	 * the mBeanWrapperFieldSetMapper.
	 */
	@SuppressWarnings("rawtypes")
	private BeanWrapperFieldSetMapper mBeanWrapperFieldSetMapper=new BeanWrapperFieldSetMapper();
	
	/**
	 * the mSkippedLinesCallback.
	 */
	private LineCallbackHandler mSkippedLinesCallback;
	
	/**
	 * the mInputStreamReader.
	 */
	private InputStreamReader mInputStreamReader;

	/**
	 * Sets the skipped lines callback.
	 * 
	 * @param pSkippedLinesCallback
	 *            - pSkippedLinesCallback
	 */
	public void setSkippedLinesCallback(LineCallbackHandler pSkippedLinesCallback){
		this.mSkippedLinesCallback = pSkippedLinesCallback;
	}

	/**
	 * Sets the line mapper.
	 * 
	 * @param pLineMapper
	 *            - pLineMapper
	 */
	public void setLineMapper(LineMapper<BaseFeedProcessVO> pLineMapper){
		this.mLineMapper = pLineMapper;
	}

	/**
	 * Gets the line mapper.
	 * 
	 * @return mLineMapper
	 */
	public LineMapper<BaseFeedProcessVO> getLineMapper(){
		return mLineMapper;
	}

	/**
	 * Sets the comments.
	 * 
	 * @param pComments
	 *            - pComments
	 */
	public void setComments(String[] pComments){
		this.mComments = new String[pComments.length];
		System.arraycopy(pComments, FeedConstants.NUMBER_ZERO, this.mComments, FeedConstants.NUMBER_ZERO, pComments.length);
	}

	/**
	 * Sets the record separator policy.
	 * 
	 * @param pRecordSeparatorPolicy
	 *            - pRecordSeparatorPolicy
	 */
	public void setRecordSeparatorPolicy(RecordSeparatorPolicy pRecordSeparatorPolicy){
		this.mRecordSeparatorPolicy = pRecordSeparatorPolicy;
	}

	/**
	 * Gets the record separator policy.
	 * 
	 * @return mRecordSeparatorPolicy
	 */
	public RecordSeparatorPolicy getRecordSeparatorPolicy(){
		return this.mRecordSeparatorPolicy;
	}

	/**
	 * Parses the line.
	 * 
	 * @param pLine
	 *            - pLine
	 * @return BaseFeedProcessVO
	 * @throws Exception
	 *             - Exception
	 */
	protected BaseFeedProcessVO parseLine(String pLine)throws Exception{
		if (this.mNoInput) {
			return null;
		}
		if (pLine == null) {
			return null;
		}
		try
		{
			BaseFeedProcessVO mapLine = this.mLineMapper.mapLine(pLine, this.mLineCount);
			return mapLine;
		}
		catch (Exception ex) {
			throw new FlatFileParseException(FeedConstants.PARSING_EROR + this.mLineCount + FeedConstants.LEFT_BRACE + pLine + FeedConstants.RIGHT_BRACE, ex, pLine, this.mLineCount);
		}
	}

	/**
	 * Read line.
	 * 
	 * @return - line
	 */
	private String readLine(){
		if (this.mReader == null) {
			throw new ReaderNotOpenException(FeedConstants.RECORD_MUSTBE_OPENED);
		}

		String line = null;
		try
		{
			line = this.mReader.readLine();
			if (line == null) {
				return null;
			}

			this.mLineCount += FeedConstants.NUMBER_ONE;
			while (isComment(line)) {
				line = this.mReader.readLine();
				if (line == null) {
					return null;
				}
				this.mLineCount += FeedConstants.NUMBER_ONE;
			}

			line = applyRecordSeparatorPolicy(line);
		}
		catch (IOException e)
		{
			this.mNoInput = true;
			throw new NonTransientFlatFileException( FeedConstants.UNABLE_TO_RAD_FROM_SOURCE + FeedConstants.LEFT_BRACE + line + FeedConstants.RIGHT_BRACE, e, line, this.mLineCount);
		}
		return line;
	}
	
	/**
	 * Checks if is comment.
	 * 
	 * @param pLine
	 *            - pLine
	 * @return true or false
	 */
	private boolean isComment(String pLine) {
		for (String prefix : this.mComments) {
			if (pLine.startsWith(prefix)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Do close.
	 * 
	 * @throws Exception
	 *             - Exception
	 */
	protected void doClose() throws Exception
	{
		this.mLineCount = FeedConstants.NUMBER_ZERO;
		if( mReader!= null ){
			mReader.close();
			mReader = null;
		}
		if(mInputStreamReader != null){
			mInputStreamReader.close();
			mInputStreamReader=null;
		}
	}

	/**
	 * validating linemapper.
	 * 
	 * @throws Exception
	 *             - Exception
	 */
	public void afterPropertiesSet() throws Exception
	{
		Assert.notNull(this.mLineMapper, FeedConstants.LINE_MAPPER_IS_REQUIRED);
	}
	
	/**
	 * Jump to item.
	 * 
	 * @param pItemIndex
	 *            - pItemIndex
	 * @throws Exception
	 *             - Exception
	 */ 
	protected void jumpToItem(int pItemIndex) throws Exception{
		for (int i = 0; i < pItemIndex; ++i){
			readLine();
		}
	}
	
	/**
	 * Apply record separator policy.
	 * 
	 * @param pLine
	 *            - pLine
	 * @return String
	 * @throws IOException
	 *             - IOException
	 */
	private String applyRecordSeparatorPolicy(String pLine)
			throws IOException
			{
		String lRecord = pLine;
		String lLine = null;
		while ((pLine != null) && (!(this.mRecordSeparatorPolicy.isEndOfRecord(lRecord)))) {
			lLine = this.mReader.readLine();
			if (lLine == null) {
				if (!(StringUtils.hasText(lRecord))) {
					break;
				}
				throw new FlatFileParseException(FeedConstants.UNEXCEPTED_END_OF_FILE, lRecord, this.mLineCount);
			}

			this.mLineCount += FeedConstants.NUMBER_ONE;
			lRecord = this.mRecordSeparatorPolicy.preProcess(lRecord) + lLine;
		}
		return this.mRecordSeparatorPolicy.postProcess(lRecord);
	}



	/**
	 * Load data.
	 *
	 * @param pCurExecCntx the cur exec cntx
	 * @return the map< string, list<? extends base feed process v o>>
	 * @throws Exception the exception
	 * @see com.tru.feedprocessor.tol.base.parse.IFeedDataParser#parseData(java.io.InputStream)
	 */
	@Override
	public Map<String, List<? extends BaseFeedProcessVO>> loadData(FeedExecutionContextVO pCurExecCntx) throws Exception {
		
		doOpen(pCurExecCntx);
		doRead();
		doClose();
		return mVOHolderMap;
	}

	/**
	 * Do open.
	 * 
	 * @param pCurExecCntx
	 *            - pCurExecCntx
	 * @throws Exception
	 *             -Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void doOpen(FeedExecutionContextVO pCurExecCntx) throws Exception
	{
		mInputStreamReader = new InputStreamReader(new FileInputStream(pCurExecCntx.getFilePath()+pCurExecCntx.getFileName()));
		mReader = new BufferedReader(mInputStreamReader);

		mDelemetedTokenizer.setNames(getColumnNames());

		if(getDelimiterString() != null && !getDelimiterString().isEmpty() ){
			mDelemetedTokenizer.setDelimiter(getDelimiterString());
		}
		Class lineMappingVOClass = getLineMappingVOClass();
		((BeanWrapperFieldSetMapper) mBeanWrapperFieldSetMapper).setTargetType(lineMappingVOClass);
		((DefaultLineMapper) this.mLineMapper).setLineTokenizer(mDelemetedTokenizer);
		((DefaultLineMapper) this.mLineMapper).setFieldSetMapper(mBeanWrapperFieldSetMapper);

		mVOHolderMap = new HashMap<String, List<? extends BaseFeedProcessVO>>();
		mResolveDupVOHolderMap = new  HashMap<String, Map<String,BaseFeedProcessVO>>();
	}

	/**
	 * Do read.
	 * 
	 * @return mVOHolderMap
	 * @throws Exception
	 *             - Exception
	 */
	protected Map<String, List<? extends BaseFeedProcessVO>> doRead() throws Exception
	{
		List<? extends BaseFeedProcessVO> entityList;

		int counter = 0;
		for (int i = 0; i < getLinesToSkip(); ++i) {
			String line = readLine();
			if (this.mSkippedLinesCallback != null) {
				this.mSkippedLinesCallback.handleLine(line);
			}
		}
		while(true){
			String line=readLine();
			if(line == null){
				break;
			}
			if(!StringUtils.isEmpty(line)&& !line.contains(FeedConstants.FEED_FILE_TAIL_RECORD_KEY)){
				counter++;
				mBaseFeedVO = parseLine(line);
				entityList = processEntityMapVO(mBaseFeedVO);
				addEntityToMap(entityList);
			}
			if (mLogging.isLoggingDebug()) {
				mLogging.logDebug("Current Line Count :::: " + counter);
			}
		}
		return mVOHolderMap;
	}

	
	/**
	 * Adds the entity to map.
	 * 
	 * @param pEntityList
	 *            - pEntityList
	 * @throws Exception
	 *             - Exception
	 */
	@SuppressWarnings("unchecked")
	protected void addEntityToMap(List<? extends BaseFeedProcessVO> pEntityList) throws Exception {
		for(BaseFeedProcessVO entityVO:pEntityList ){
			
		String entityName = entityVO.getEntityName();
		
		if(isResolveDuplicate()){
			List<BaseFeedProcessVO> mappedEntityList = resolveDuplicate(entityName,entityVO);
			mVOHolderMap.put(entityName,mappedEntityList);
			
		}else{
			if(mVOHolderMap.containsKey(entityName)){
				List<BaseFeedProcessVO> mappedEntityList = (List<BaseFeedProcessVO>) mVOHolderMap.get(entityName);
				mappedEntityList.add(entityVO);
				mVOHolderMap.put(entityName, mappedEntityList);
			}
			else{
				List<BaseFeedProcessVO> mappedEntityList = new  ArrayList<BaseFeedProcessVO>(); 
				mappedEntityList.add(entityVO);
				mVOHolderMap.put(entityName, mappedEntityList);
			}
		  }	
		}
	}

	/**
	 * Resolve duplicate.
	 * 
	 * @param pEntityName
	 *            - pEntityName
	 * @param pBaseFeedProcessVO
	 *            - pBaseFeedProcessVO
	 * @return List<BaseFeedProcessVO>
	 * @throws Exception
	 *             - Exception
	 */
	protected List<BaseFeedProcessVO> resolveDuplicate(String pEntityName,BaseFeedProcessVO pBaseFeedProcessVO) throws Exception{
		Map<String,BaseFeedProcessVO> lMap = null;
		
		if( null != pBaseFeedProcessVO.getBusinessId() && !pBaseFeedProcessVO.getBusinessId().isEmpty()){	
			
			lMap = mResolveDupVOHolderMap.get(pEntityName); 
			
			if( null == lMap){
				lMap = new HashMap<String,BaseFeedProcessVO>();
				lMap.put(pBaseFeedProcessVO.getBusinessId(), pBaseFeedProcessVO);
			}else{
				lMap.put(pBaseFeedProcessVO.getBusinessId(), pBaseFeedProcessVO);
			}
			mResolveDupVOHolderMap.put(pEntityName, lMap);
		}else{
			throw new Exception(FeedConstants.BUSINESS_ID_REQUIRED +pEntityName+ FeedConstants.TO_RESOLVE_DUPLICATE);
		}
		return new  ArrayList<BaseFeedProcessVO>(lMap.values());
	}

	/**
	 * Checks if is resolve duplicate.
	 * 
	 * @return boolean
	 */
	public abstract boolean isResolveDuplicate();

	/**
	 * Gets the column names.
	 * 
	 * @return String[]
	 */
	public abstract String[] getColumnNames();

	/**
	 * Gets the line mapping vo class.
	 * 
	 * @return String
	 * @throws Exception
	 *             - Exception
	 */
	public abstract Class<?> getLineMappingVOClass() throws Exception;

	/**
	 * Gets the delimiter string.
	 * 
	 * @return String
	 */
	public abstract String getDelimiterString();

	/**
	 * Gets the lines to skip.
	 * 
	 * @return int
	 */
	public abstract int getLinesToSkip();

	/**
	 * Process entity map vo.
	 * 
	 * @param pBaseFeedProcessVO
	 *            - pBaseFeedProcessVO
	 * @return mBaseFeedProcessVO
	 */
	public abstract List<? extends BaseFeedProcessVO> processEntityMapVO(BaseFeedProcessVO pBaseFeedProcessVO);

}