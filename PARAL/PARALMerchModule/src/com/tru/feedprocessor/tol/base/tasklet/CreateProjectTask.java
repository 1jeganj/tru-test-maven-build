package com.tru.feedprocessor.tol.base.tasklet;

import atg.epub.project.Process;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.tasklet.support.IProjectNameGenerator;
import com.tru.merchutil.tol.workflow.WorkFlowTools;


/**
 * The Class CreateProjectTask 
 *
 * This class is used to initiate a work flow to create a project in  in BCC 
 * 
 * @version 1.0
 * @author Professional Access
 */
public class CreateProjectTask extends AbstractTasklet {

	/** The m project name generator. */
	IProjectNameGenerator mProjectNameGenerator;
	
	/** The m work flow tools. */
	WorkFlowTools mWorkFlowTools;
	/**
	 * Gets the work flow tools.
	 *
	 * @return the work flow tools
	 */
	public WorkFlowTools getWorkFlowTools() {
		return mWorkFlowTools;
	}
	
	/**
	 * Sets the work flow tools.
	 *
	 * @param pWorkFlowTools the new work flow tools
	 */
	public void setWorkFlowTools(WorkFlowTools pWorkFlowTools) {
		mWorkFlowTools = pWorkFlowTools;
	}
	
	/**
	 * Gets the project name generator.
	 *
	 * @return the project name generator
	 */
	public IProjectNameGenerator getProjectNameGenerator() {
		return mProjectNameGenerator;
	}
	
	/**
	 * Sets the project name generator.
	 *
	 * @param pIProjectNameGenerator the new project name generator
	 */
	public void setProjectNameGenerator(
			IProjectNameGenerator pIProjectNameGenerator) {
		mProjectNameGenerator = pIProjectNameGenerator;
	}
	
	/**
	 * Creates the project.
	 */
	protected void createProject()  {
		
		String workflowName = null;
		
		saveProjectName(getProjectNameGenerator().getProjectName(getCurrentFeedExecutionContext()));
		
		if((Boolean)getConfigValue(FeedConstants.USEAUTODEPLOY_WORKFLOW)){
			workflowName=(String)getConfigValue(FeedConstants.AUTODEPLOY_WORKFLOW);
		}
		else{
			workflowName=(String)getConfigValue(FeedConstants.MANUAL_WORKFLOW);
		}

		Process process = mWorkFlowTools.initiateWorkflow((String) retriveData(FeedConstants.PROJECT_NAME), workflowName);

		saveData(FeedConstants.PROCESS, process);


	}

	/**
	 * Save project name.
	 * 
	 * @param pProjectName
	 *            the project name
	 */
	public void saveProjectName(String pProjectName) {
		saveData(FeedConstants.PROJECT_NAME, pProjectName);
	}

	/**
	 * Do execute.
	 *
	 * @throws FeedSkippableException the feed skippable exception
	 * @see com.tru.feedprocessor.tol.AbstractTasklet#doExecute()
	 */
	@Override
	protected void doExecute() throws FeedSkippableException {
		createProject();

	}

}
