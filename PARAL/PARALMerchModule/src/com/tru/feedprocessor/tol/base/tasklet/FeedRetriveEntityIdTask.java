package com.tru.feedprocessor.tol.base.tasklet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tru.feedprocessor.tol.FeedConstants;

/**
 * The Class FeedRetriveEntityIdTask. This is the common class which is used
 * to retreve the unique id and repository id from the corresponding data
 * source. Using teh EntiryResultExtractor it will store the data into feed
 * context.
 *
 * @version 1.1
 * @author Professional Access
 * 
 */
public class FeedRetriveEntityIdTask extends AbstractRetrieveIdForIdentifierTask {

	/**
	 * Gets the query.
	 * 
	 * @param pCurrentEntity
	 *            pCurrentEntity
	 * @return query
	 */
	protected String getQuery(String pCurrentEntity) {
		Map<String, String> lEntitySQLMap = (HashMap<String, String>) getConfigValue(FeedConstants.ENTITY_SQL);
		return lEntitySQLMap.get(pCurrentEntity);
	}

	/**
	 * Gets the identifiers.
	 *
	 * @return the identifiers
	 * @see com.tru.feedprocessor.tol.base.tasklet.AbstractRetrieveIdForIdentifierTask#getIdentifiers()
	 */
	@Override
	protected List<String> getIdentifiers() {
		return getEntityList(); 
	}
	
}
