package com.tru.feedprocessor.tol.base.formatter;

import java.util.ArrayList;
import java.util.List;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.feedprocessor.tol.base.vo.StepInfoVO;


/**
 * The Class FeedFormatter.
 *
 * The class is used to format the console base on the data process during the feed running
 * It will generate a statistics report for phaseName,stepName,ReadCount,SkipCount,WriteCount,CommitCount,RollBack,StepExecution Time and Exception if any 
 * in tabular format and finally it will print Total Execution Time in Millisecond __n__ and 
 * Total Execution Time of current Feed _n_ Days,_n_ Hours,_n__ Minutes and _n_ Seconds
 *  
 * @version 1.1
 * @author Professional Access
 */
public class FeedFormatter implements IFormatter {

	/**
	 * Format.
	 *
	 * @param pProcessedContextInfoList the processed context info list
	 * @return the string
	 * @see com.tru.feedprocessor.tol.base.formatter.IFormatter#format(java.util.List)
	 */
	@Override
	public String format(List<FeedExecutionContextVO> pProcessedContextInfoList) {

		StringBuffer lStringBuffer = new StringBuffer();
		
		if( null == pProcessedContextInfoList || pProcessedContextInfoList.isEmpty() ){
			lStringBuffer.append(FeedConstants.NO_FEED_CONTEXT_TO_PROCESS);
			return lStringBuffer.toString();
		}
		
		for (FeedExecutionContextVO lFeedExecutionContextVO : pProcessedContextInfoList){
			
			long lMilliSeconds = FeedConstants.NUMBER_ZERO;
			long startTime = FeedConstants.NUMBER_ZERO;
			long endTime=FeedConstants.NUMBER_ZERO;
			List<StepInfoVO> stepInfos= lFeedExecutionContextVO.getStepInfos();
			
			lStringBuffer.append(FeedConstants.NEW_LINE+ lFeedExecutionContextVO.getFilePath()+ lFeedExecutionContextVO.getFileName());
			
			lStringBuffer.append(FeedConstants.NEW_LINE);
			
			lStringBuffer.append(String.format(FeedConstants.LINE_ONE_FORMAT));
			lStringBuffer.append(String.format(FeedConstants.LINE_TWO_FORMAT));
			lStringBuffer.append(FeedConstants.NEW_LINE);
			lStringBuffer.append(
			String.format( FeedConstants.HEADER_ALIGNMENT_FORMAT,
					FeedConstants.PHASE_NAME, FeedConstants.STEP_NAME,
					FeedConstants.READ_COUNT, FeedConstants.SKIP_COUNT,FeedConstants.WRITE_COUNT,
					FeedConstants.COMMIT_COUNT,FeedConstants.ROLLBACK_COUNT,FeedConstants.READ_SKIP_COUNT,
					FeedConstants.PROCESS_SKIP_COUNT,FeedConstants.WRITE_SKIP_COUNT,FeedConstants.STEP_EXECUTION_TIME,FeedConstants.EXCEPTIONS ));
			
			lStringBuffer.append(FeedConstants.NEW_LINE);
			lStringBuffer.append(String.format(FeedConstants.LINE_ONE_FORMAT));
			lStringBuffer.append(String.format(FeedConstants.LINE_TWO_FORMAT));
			lStringBuffer.append(FeedConstants.NEW_LINE);
			
			if (null != stepInfos && !stepInfos.isEmpty()) {
				for (StepInfoVO vo : stepInfos) {
					
					if(startTime == FeedConstants.NUMBER_ZERO){
						startTime = vo.getStartTime().getTime();
					}
					
					long lStartTime = vo.getStartTime().getTime();
					long lEndTime;
					
					if (vo.getEndTime() == null) {
						lEndTime = lStartTime;
					} else {
						lEndTime = vo.getEndTime().getTime();
					}
					
					long lTotalTime = (lEndTime - lStartTime)/ FeedConstants.NUMBER_SIXTYTHOUSANDS;
					endTime = lEndTime;
					
					lStringBuffer.append(formatStepInfo(vo));
					lStringBuffer.append(String.format(FeedConstants.TOTALTIME_FORMAT, lTotalTime));
					lStringBuffer.append(String.format(FeedConstants.EXCEPTION_FORMAT, formatExcptions(vo))); 
					lStringBuffer.append(FeedConstants.NEW_LINE);
					
				}
			lStringBuffer.append(String.format(FeedConstants.LINE_ONE_FORMAT));
			lStringBuffer.append(String.format(FeedConstants.LINE_TWO_FORMAT));
			
			}
			
			lMilliSeconds = endTime - startTime;
			
			lStringBuffer.append(FeedConstants.NEW_LINE + FeedConstants.TOTAL_EXECUTION_TIME_IN_MILLI_SECONDS +  lMilliSeconds);
			lStringBuffer.append(FeedConstants.NEW_LINE +FeedConstants.TOTAL_EXECUTION_TIME_OF_CURRENT_FEED + evaluateTime(lMilliSeconds));
			
		}
		return lStringBuffer.toString();
	}
	
	
	/**
	 * Format step info.
	 * 
	 * @param pStepInfoVO
	 *            - pStepInfoVO
	 * @return - formatStepInfo
	 */
	protected String formatStepInfo(StepInfoVO pStepInfoVO)
	{
		return String.format( FeedConstants.DATA_ALIGNMENT_FORMAT, 
				 pStepInfoVO.getPhaseName() ,  pStepInfoVO.getStepName(),
				 pStepInfoVO.getReadCount() ,  pStepInfoVO.getSkipCount(),  pStepInfoVO.getWriteCount() , 
				 pStepInfoVO.getCommitCount(), pStepInfoVO.getRollbackCount(),  pStepInfoVO.getReadSkipCount(),
				 pStepInfoVO.getProcessSkipCount(), pStepInfoVO.getWriteSkipCount() 
				);
	}
	
	/**
	 * The Base v os.
	 */
	private List<BaseFeedProcessVO> mBaseVOs= new ArrayList<BaseFeedProcessVO>();	
	
	/**
	 * Format excptions.
	 * 
	 * @param pStepInfoVO
	 *            - pStepInfoVO
	 * @return - formatExcptions
	 */ 
	protected String formatExcptions(StepInfoVO pStepInfoVO)
	{
		StringBuffer lException =new StringBuffer();
		if(pStepInfoVO.getFeedFailureProcessException() == null)
		{
			List<String> detailMessage = new ArrayList<String>();
			List<? extends FeedSkippableException> lFeedSkipedException = pStepInfoVO.getFeedSkippedDataExceptions();
			for (FeedSkippableException exec: lFeedSkipedException){
				if(!detailMessage.contains(exec.getMessage())){
					detailMessage.add(exec.getMessage());
					lException.append(exec.getClass().getSimpleName() + FeedConstants.SPACE 
							+ FeedConstants.LEFT_BRACE + exec.getMessage() 
							+ FeedConstants.RIGHT_BRACE);
				}
			}
		}
		else{
			lException.append(format(pStepInfoVO.getFeedFailureProcessException()));
		}
		return lException.toString();
	}
	
	
	/**
	 * Evaluate time.
	 * 
	 * @param pMilliSeconds
	 *            - MilliSeconds
	 * @return - evaluateTime
	 */
	private String evaluateTime(long pMilliSeconds){
		StringBuffer timeStr = new StringBuffer();
		int lSeconds = (int) (pMilliSeconds / FeedConstants.NUMBER_THOUSAND) % FeedConstants.NUMBER_SIXTY ;
		int lMinutes = (int) (pMilliSeconds / ( FeedConstants.NUMBER_THOUSAND * FeedConstants.NUMBER_SIXTY)) % FeedConstants.NUMBER_SIXTY;
		int lHours   = (int) (pMilliSeconds / ( FeedConstants.NUMBER_THOUSAND * FeedConstants.NUMBER_SIXTY * FeedConstants.NUMBER_SIXTY)) % FeedConstants.NUMBER_TWENTYFOUR;
		int lDays	= (int) (pMilliSeconds / ( FeedConstants.NUMBER_THOUSAND * FeedConstants.NUMBER_SIXTY * FeedConstants.NUMBER_SIXTY * FeedConstants.NUMBER_TWENTYFOUR));
		timeStr.append(lDays).append(FeedConstants.SPACE+FeedConstants.DAYS)
				.append(FeedConstants.COMA_SEPERATOR).append(lHours)
				.append(FeedConstants.SPACE+FeedConstants.HOURS)
				.append(FeedConstants.COMA_SEPERATOR).append(lMinutes)
				.append(FeedConstants.SPACE+FeedConstants.MINUTES)
				.append(FeedConstants.SPACE+FeedConstants.AND)
				.append(FeedConstants.SPACE+lSeconds)
				.append(FeedConstants.SPACE+FeedConstants.SECONDS);
		
		String lTime = timeStr.toString();
		
		return lTime;
	}


	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.IFormatter#format(com.tru.feedprocessor.cml.exception.FeedSkippableException)
	 */
	@Override
	public String format(Throwable pThrowable) 
	{ 
			
		return pThrowable.getClass().getSimpleName() + FeedConstants.SPACE 
				+ FeedConstants.LEFT_BRACE + pThrowable.getMessage()
							+ FeedConstants.RIGHT_BRACE;
	}

	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.IFormatter#format(com.tru.feedprocessor.cml.exception.FeedSkippableException)
	 */
	@Override
	public String formatAll(Throwable pThrowable) 
	{ 
			
		return pThrowable.getClass().getSimpleName() + FeedConstants.SPACE 
				+ FeedConstants.LEFT_BRACE + (pThrowable.getMessage()!= null ? pThrowable.getMessage():pThrowable)
							+ FeedConstants.RIGHT_BRACE;
	}

	/**
	 * Format all excptions.
	 * 
	 * @param pStepInfoVO
	 *            - pStepInfoVO
	 * @return - formatExcptions
	 */ 
	protected String formatAllExcptions(StepInfoVO pStepInfoVO)
	{
		StringBuffer lException =new StringBuffer();
		if(pStepInfoVO.getFeedFailureProcessException() == null){	
			List<? extends FeedSkippableException> lFeedSkipedException = pStepInfoVO.getFeedSkippedDataExceptions();
			for (FeedSkippableException exec: lFeedSkipedException){
				lException.append(exec.getClass().getSimpleName() + FeedConstants.SPACE 
						+ FeedConstants.LEFT_BRACE + (exec.getMessage() != null ? exec.getMessage() :exec) 
						+ FeedConstants.RIGHT_BRACE);
						mBaseVOs.add(exec.getFeedItemVO());
			}
		}
		else{
			lException.append(formatAll(pStepInfoVO.getFeedFailureProcessException()));
		}
		return lException.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.IFormatter#format(com.tru.feedprocessor.cml.exception.FeedSkippableException)
	 */
	@Override
	public String printAllException(
			List<FeedExecutionContextVO> pProcessedExecutionContextList) {

		StringBuffer lStringBuffer = new StringBuffer();

		if (null == pProcessedExecutionContextList
				|| pProcessedExecutionContextList.isEmpty()) {
			return lStringBuffer.append(FeedConstants.NO_FEED_CONTEXT_TO_PROCESS).toString();
		}
		
		for (FeedExecutionContextVO lFeedExecutionContextVO : pProcessedExecutionContextList) {
			List<StepInfoVO> stepInfos = lFeedExecutionContextVO.getStepInfos();
			if (null != stepInfos && !stepInfos.isEmpty()) {
				for (StepInfoVO vo : stepInfos) {
					lStringBuffer.append(formatAllExcptions(vo));
				}
			}

		}
		return lStringBuffer.toString();
	}
}
