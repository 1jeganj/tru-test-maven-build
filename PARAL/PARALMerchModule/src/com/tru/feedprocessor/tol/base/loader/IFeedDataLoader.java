package com.tru.feedprocessor.tol.base.loader;

import java.util.List;
import java.util.Map;

import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Interface IFeedDataLoader.
 * 
 * This is used for implement in loadData method in loader class for parsing and load CSV,XML and DB file into memory
 * 
 * @version 1.1
 * @author Professional Access
 */
public interface IFeedDataLoader {
	
	/**
	 * Load data.
	 * 
	 * @param pCurExecCntx
	 *            - pCurExecCntx
	 * @return mBaseFeedProcessVOMap
	 * @throws Exception
	 *             - Exception
	 */
	Map<String, List<? extends BaseFeedProcessVO>> loadData(FeedExecutionContextVO pCurExecCntx) throws Exception;
}
