package com.tru.feedprocessor.tol.base.writer;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.base.mapper.IFeedItemMapper;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Class FeedRepositoryItemWriter.
 * 
 * <P> The default writer for Repository, write the data into Repository 
 * by calling add or update based on the data in mEntityIdMap.</p>
 * 
 * @version 1.0
 * @author Professional Access
 */
public class FeedRepositoryItemWriter extends AbstractFeedItemWriter {
	/**
	 * The entity id map.
	 */
	protected Map<String,Map<String,String>> mEntityIdMap = null;
	
	/**
	 * The IdentifierIdStoreKey  
	 */
	private String mIdentifierIdStoreKey;
	
	/**
	 * @return the entityIdMap
	 */
	public Map<String, Map<String, String>> getEntityIdMap() {
		return mEntityIdMap;
	}

	/** 
	 * Return IdentifierIdStoreKey
	 * 
	 * @return the IdentifierIdStoreKey 
	 * 
	 */
	public String getIdentifierIdStoreKey() {
		return mIdentifierIdStoreKey;
	}

	/**
	 * @param pIdentifierIdStoreKey the IdentifierIdStoreKey to set
	 */
	public void setIdentifierIdStoreKey(String pIdentifierIdStoreKey) {
		mIdentifierIdStoreKey = pIdentifierIdStoreKey;
	}
		
	/**
	 * (non-Javadoc).
	 *
	 * @param pVOItems the vO items
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 * @see com.tru.feedprocessor.tol.AbstractFeedItemWriter.
	 * AbstractBvFeedItemWriter#write(java.util.List)
	 */
	@Override
	public void doWrite(List<? extends BaseFeedProcessVO> pVOItems) throws RepositoryException,FeedSkippableException{
		
		for(BaseFeedProcessVO lBaseFeedProcessVO:pVOItems){
			
			 String lItemDiscriptorName = getItemDescriptorName(lBaseFeedProcessVO.getEntityName());
			 
				if(mEntityIdMap!=null && mEntityIdMap.get(lItemDiscriptorName).get(lBaseFeedProcessVO.getRepositoryId())!=null){
					updateItem(lBaseFeedProcessVO);
				}else{
					addItem(lBaseFeedProcessVO);
					
				}
		}
		
	}
	/**
	 * doOpen.
	 * 
	 *  <p>
	 *  Execute once during calling of Open method
	 *  get the List of custom mapper and set it into abstract from the helper configuration 
	 *  </p>
	 *  
	 *  <p> Retrive mEntityIdMap from the feed session based on IdentifierIdStoreKey configure in component </p>  
	 *  
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void doOpen(){
		
		Collection values = getMapperMap().values();
		Iterator iterator = values.iterator();
		while (iterator.hasNext()) {
			AbstractFeedItemMapper itemMapper = (AbstractFeedItemMapper) iterator.next();
			itemMapper.setFeedHelper(getFeedHelper());
		}
		
		mEntityIdMap = (Map<String, Map<String, String>>)retriveData(getIdentifierIdStoreKey());
	}

	/**
	 * Update item.
	 * 
	 * <p>
	 * Get MutableRepositoryItem based on ItemDiscriptorName and set the current item from VO via map()
	 * 
	 * <p>Get the Repository based on RepositoryName and update currentItem into it </P>
	 *
	 * @param pFeedVO the feed vo
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	public void updateItem(BaseFeedProcessVO pFeedVO) throws RepositoryException, FeedSkippableException{
		MutableRepositoryItem lCurrentItem=null;
		
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
		
		if(lItemDiscriptorName!=null){
			IFeedItemMapper	lFeedItemMapper =(IFeedItemMapper) getMapperMap().get(pFeedVO.getEntityName());
			
			lCurrentItem=(MutableRepositoryItem) getRepository(lRepositoryName).getItem(mEntityIdMap.get(lItemDiscriptorName).get(pFeedVO.getRepositoryId()),lItemDiscriptorName);
			//beforeUpdateItem(pFeedVO,lCurrentItem);
			lCurrentItem = lFeedItemMapper.map(pFeedVO, lCurrentItem);
			getRepository(lRepositoryName).updateItem(lCurrentItem);
			afterUpdateItem(pFeedVO, lCurrentItem);
			addUpdateItemToList(pFeedVO);
		}
	}

	/**
	 * After update item.
	 *
	 * @param pFeedVO the feed vo
	 * @param pCurrentItem the current item
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	protected void afterUpdateItem(BaseFeedProcessVO pFeedVO,
			MutableRepositoryItem pCurrentItem) throws RepositoryException,FeedSkippableException{
		
		if(!pFeedVO.getRepositoryId().equals(pCurrentItem.getRepositoryId())){
			pFeedVO.setRepositoryId(pCurrentItem.getRepositoryId());
		}
		
	}

	/**
	 * Before update item.
	 * 
	 * <p> Need to customized by team before updateItem if any changes required in current Repository </p>
	 * 
	 * @throws RepositoryException - RepositoryException
	 * @throws FeedSkippableException - FeedSkippableException
	 */
	protected void beforeUpdateItem() throws RepositoryException,FeedSkippableException{
		if (getLogger().isLoggingDebug()) {
			getLogger().logInfo("beforeUpdate() is Started... ");
		}
		saveData(FeedConstants.IS_ADDFLAG, true);
	}

	/**
	 * Adds the item.
	 *
	 * <p>
	 * Get MutableRepositoryItem based on ItemDiscriptorName and set the current item from VO via map()
	 * 
	 * <p>Get the Repository based on RepositoryName and add currentItem into it </P>
	 *
	 * @param pFeedVO the feed vo
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	public void addItem(BaseFeedProcessVO pFeedVO)throws RepositoryException, FeedSkippableException{
		MutableRepositoryItem lCurrentItem=null;
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
		
		if(lItemDiscriptorName!=null){
			IFeedItemMapper lFeedItemMapper =(IFeedItemMapper) getMapperMap().get(pFeedVO.getEntityName());
			lCurrentItem = getRepository(lRepositoryName).createItem(pFeedVO.getRepositoryId(),lItemDiscriptorName);
			beforeAddItem(pFeedVO,lCurrentItem);
			lCurrentItem=lFeedItemMapper.map(pFeedVO, lCurrentItem);
			getRepository(lRepositoryName).addItem(lCurrentItem);
			afterAddItem(pFeedVO,lCurrentItem);
			addInsertItemToList(pFeedVO);
		}
			
	}
	
	/**
	 * After add item.
	 *
	 * @param pFeedVO the feed vo
	 * @param pCurrentItem the current item
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	protected void afterAddItem(BaseFeedProcessVO pFeedVO,
			MutableRepositoryItem pCurrentItem) throws RepositoryException,FeedSkippableException{
		
		if(!pFeedVO.getRepositoryId().equals(pCurrentItem.getRepositoryId())){
			pFeedVO.setRepositoryId(pCurrentItem.getRepositoryId());
		}
	}

	/** Before add item.
	 * 
	 * <p> Need to customized by team before addItem if any changes required in current Repository </p>
	 *  
	 * @param pFeedVO - pFeedVO
	 * @param pCurrentItem - pCurrentItem
	 * @throws RepositoryException - RepositoryException
	 * @throws FeedSkippableException - FeedSkippableException
	 */
	protected void beforeAddItem(BaseFeedProcessVO pFeedVO, MutableRepositoryItem pCurrentItem) throws RepositoryException,FeedSkippableException{
		if (getLogger().isLoggingDebug()) {
			getLogger().logInfo("beforeAddItem() is Started... ");
		}
		saveData(FeedConstants.IS_ADDFLAG, true);
	}
}
