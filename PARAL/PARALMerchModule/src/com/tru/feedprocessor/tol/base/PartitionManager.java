package com.tru.feedprocessor.tol.base;

import java.util.Iterator;
import java.util.List;



/**
 * The Class PartitionManager. 
 * 
 * <p> It provide the service start and end range value for list iterator </p>
 *
 * @version 1.0
 * @author Professional Access
 */
public class PartitionManager {

	/** The first. */
	private boolean mFirst=true;
	
	/** The Iterator. */
	private Iterator<Range> mIterator = null;
	
	/** The Range. */
	private List<Range> mRange;

	/**
	 * Gets the range.
	 *
	 * @return the range
	 */
	public List<Range> getRange() {
		return mRange;
	}

	/**
	 * Sets the range.
	 *
	 * @param pRange the new range
	 */
	public void setRange(List<Range> pRange) {
		mRange = pRange;
	}
	
	/**
	 * Gets the next range.
	 *
	 * @return the next range
	 */
	public Range getNextRange()
	{
		if(mFirst)
		{
			mIterator=getRange().iterator();
		}
		return mIterator.next();
		
	}
	
	
}
