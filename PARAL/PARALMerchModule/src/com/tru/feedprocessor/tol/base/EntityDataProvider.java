package com.tru.feedprocessor.tol.base;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Class EntityDataProvider 
 * 
 * Provide service for the data based on the entity 
 *
 * @version 1.0
 * @author Professional Access
 */
public class EntityDataProvider{
	
	/** The mEntityVOsMap*/
	private Map<String, List<? extends BaseFeedProcessVO>> mEntityVOsMap=null;
	
	/**
	 * @param pEntityVOsMap - pEntityVOsMap
	 */
	public EntityDataProvider(Map<String, List<? extends BaseFeedProcessVO>> pEntityVOsMap) {
		this.mEntityVOsMap = pEntityVOsMap;
	}
	
	/**
	 * 
	 * @param pEntityName - pEntityName
	 * @return entities
	 */
	public List<? extends BaseFeedProcessVO> getEntityList(String pEntityName){
		return mEntityVOsMap.get(pEntityName);
	}
	
	/**
	 * 
	 * @return entities
	 */
	public Map<String, List<? extends BaseFeedProcessVO>> getAllEntityList(){
		return mEntityVOsMap;
	}
	
	/**
	 * 
	 * @return lCount
	 */
	public int getAllEntitiesCount() {
		int lCount=0;
		Set<String> keySet = mEntityVOsMap.keySet();
		for(String key:keySet){
			lCount+=mEntityVOsMap.get(key).size();
		}
		return lCount;
	}
}
