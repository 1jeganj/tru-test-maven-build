package com.tru.feedprocessor.tol.base.tasklet;

import java.util.List;
import java.util.Map;

import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import atg.repository.MutableRepository;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.FeedHelper;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class AbstractTasklet.
 *
 * @version 1.1
 * @author Professional Access
 */
public abstract class AbstractTasklet implements Tasklet {

	/** The mphase name. */
	private String mPhaseName;

	/** The m step name. */
	private String mStepName;

	/** The mLogger. */
	private FeedLogger mLogger;
	
	/** The m feed helper. */
	private FeedHelper mFeedHelper;
	
	/** The mJobParameterMap. */
	private Map<String, JobParameter> mJobParameterMap;

	/**
	 * Sets the feed helper.
	 *
	 * @param pFeedHelper the new feed helper
	 */
	public void setFeedHelper(FeedHelper pFeedHelper) {
		mFeedHelper = pFeedHelper;
	}

	/**
	 * Gets the feed helper.
	 * 
	 * @return the feedHelper
	 */
	public FeedHelper getFeedHelper() {
		return mFeedHelper;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the mLogger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the new logger
	 */
	public void setLogger(FeedLogger pLogger) {
		this.mLogger = pLogger;
	}

	/**
	 * Gets the step name.
	 * 
	 * @return the mStepName
	 */
	protected String getStepName() {
		return mStepName;
	}

	/**
	 * Gets the phase name.
	 * 
	 * @return the phase name
	 */
	protected String getPhaseName() {
		return mPhaseName;
	}

	/**
	 *  
	 *
	 * @param pStepContribution the step contribution
	 * @param pChunkContext the chunk context
	 * @return the repeat status
	 * @throws Exception the exception
	 * @see org.springframework.batch.core.step.tasklet.Tasklet#execute(org.
	 * springframework.batch.core.StepContribution,
	 * org.springframework.batch.core.scope.context.ChunkContext)
	 */
	@Override
	public RepeatStatus execute(StepContribution pStepContribution,
			ChunkContext pChunkContext) throws Exception {

		// setting the step name
		mStepName = pChunkContext.getStepContext().getStepName();
		// Setting the phase name so that the step name can be accessed for
		// notifying purpose
		
		JobParameters lJobParameters = pChunkContext.getStepContext().getStepExecution().getJobExecution().getJobParameters();
		mJobParameterMap = lJobParameters.getParameters();
		
		mPhaseName=(String)pChunkContext.getStepContext().getStepExecution().getExecutionContext().get(FeedConstants.PHASE_NAME);
		
		getLogger().logDebugMessage(getStepName() + FeedConstants.STARTED);
			
		doExecute();
			
		getLogger().logDebugMessage(getStepName() + FeedConstants.COMPLETED);
		return RepeatStatus.FINISHED;
	}
	
	/**
	 * Common method of all kinds of tasklets in feed process.
	 * 
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 * @throws Exception - Exception
	 *             
	 */
	protected abstract void doExecute() throws FeedSkippableException,Exception;
	
	/**
	 * Gets the current feed execution context.
	 * 
	 * @return - CurrentFeedExecutionContext
	 */
	public FeedExecutionContextVO getCurrentFeedExecutionContext() {
		return  getFeedHelper().getCurrentFeedExecutionContext();
	}
	/**
	 * Retrive data.
	 * 
	 * @param pDataMapKey
	 *            the data map key
	 * @return the object
	 */
	public Object retriveData(String pDataMapKey) {
		return getFeedHelper().retriveData(pDataMapKey);
	}
	/**
	 * Save data.
	 * 
	 * @param pConfigKey
	 *            the pConfigKey
	 * @param pConfigValue
	 *            the config value
	 */
	public void saveData(String pConfigKey, Object pConfigValue) {
		 getFeedHelper().saveData(pConfigKey, pConfigValue);
	}
	
	/**
	 * Gets the entity list.
	 * 
	 * @return the entity list
	 */

	public List<String> getEntityList() {
		return getFeedHelper().getStepEntityList(getStepName());
	}
	
	/**
	 * Gets the config value.
	 * 
	 * @param pConfigKey
	 *            the pConfigKey
	 * @return value
	 */
	public Object getConfigValue(String pConfigKey) {
		return getFeedHelper().getConfigValue(pConfigKey);
	}
	
	/**
	 * Sets the config value.
	 * 
	 * @param pName
	 *            - pName
	 * @param pValue
	 *            - pValue
	 */
	public void setConfigValue(String pName, Object pValue) {
		getFeedHelper().setConfigValue(pName, pValue);
	}

	/**
	 * Gets the repository.
	 * 
	 * @param pRepoName
	 *            the repositoryName
	 * @return repository
	 */
	public MutableRepository getRepository(String pRepoName) {
		return getFeedHelper().getRepository(pRepoName);
	}
	
	/**
	 * Gets the item descriptor name.
	 * 
	 * @param pEntityName
	 *            - pEntityName
	 * @return item descriptor name
	 */
	public String getItemDescriptorName(String pEntityName){
		return getFeedHelper().getEntityToItemDescriptorNameMap(pEntityName);
	}
	
	/**
	 * Gets the entity repository name.
	 * 
	 * @param pEntityName
	 *            - pEntityName
	 * @return repository name
	 */
	public String getEntityRepositoryName(String pEntityName){
		return getFeedHelper().getEntityToRepositoryNameMap(pEntityName);
	}

	/**
	 * Gets the job parameter.
	 * 
	 * @param pJobKey
	 *            - pJobKey
	 * @return the JobParameter
	 */
	public Object getJobParameter(String pJobKey){
		return mJobParameterMap.get(pJobKey).getValue();
	}
	
	/**
	 * Gets the processed feed execution context.
	 * 
	 * @return - ProcessedFeedExecutionContext
	 */
	@SuppressWarnings("unchecked")
	protected List<FeedExecutionContextVO> getProcessedFeedExecutionContext() {
		return (List<FeedExecutionContextVO>) retriveData(FeedConstants.PROCESSED_CONTEXTS);
	}
	
	/**
     * Log error message.
     * 
      * @param pMessage
     *            the message
     * @param pException
     *            the exception
     */
     public void logError(Object pMessage, Throwable pException) {
            getLogger().logErrorMessage(pMessage, pException);
            
     }

     

     /**
     * @return true/false
     */
     public boolean isLoggingError()
       {
         return getLogger().isLoggingError();
       }

}
