package com.tru.feedprocessor.tol.base;

import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;


/**
 * The Interface IPostProcessorDelegate 
 * 
 * <p> 
 * This interface is used to delegate any post process task
 * Team can implements for post process job and delegate from there component to achieve the functionality
 * </p>
 *    
 * @version 1.0
 * @author Professional Access
 */
public interface IPostProcessorDelegate {
	
	
	
	/**
	 * @param pFeedExecutionContextVO - pFeedExecutionContextVO
	 * 
	 */
	void postProcess(FeedExecutionContextVO pFeedExecutionContextVO);
}
