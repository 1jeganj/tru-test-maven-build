package com.tru.feedprocessor.tol.base.tasklet;

import java.io.File;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class FileExistsTask.
 * 
 * File exist task for validate the path and file in respective path 
 *  
 * @version 1.1
 * @author Professional Access
 */
public class FileExistsTask extends DataExistsTask {

	/**
	 * Do execute.
	 *
	 * @throws FeedSkippableException the feed skippable exception
	 * @throws Exception the exception
	 * @see com.tru.feedprocessor.tol.AbstractTasklet#doExecute() Project
	 * team can override this method to put their code to check for file
	 * existence.
	 */
	@Override
	protected void doExecute() throws FeedSkippableException, Exception {

		FeedExecutionContextVO curExecCntx = getCurrentFeedExecutionContext();
		if (!curExecCntx.getFileName().endsWith(FeedConstants.TABLE)) {
			File lFile = new File(curExecCntx.getFilePath()	+ curExecCntx.getFileName());
			if (lFile == null || !lFile.exists() || !lFile.isFile()	|| !lFile.canRead()) {
				throw new Exception(FeedConstants.FILE_DOES_NOT_EXISTS);
			}
		}
	}
}
