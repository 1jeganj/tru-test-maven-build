package com.tru.feedprocessor.tol.base.vo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.nucleus.ServiceMap;
import atg.repository.MutableRepository;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.config.IFeedConfig;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;

/**
 * The Class FeedExecutionContextVO.
 * 
 *  FeedExecutionContextVO hold process File name, File Path and
 *  execution Steps Info
 *  
 * @version 1.1
 * @author Professional Access
 */
public class FeedExecutionContextVO implements IFeedConfig {

	/** The mFileName. */
	private String mFileName;
	
	/** The mFilePath. */
	private String mFilePath;
	
	/** The mFileType. */
	private String mFileType;
	
	/** The mSiteId. */
	private String mSiteId;
	
	/** The  mHashMap. */
	private Map<String, Object> mHashMap = new HashMap<String, Object>();
	
	/** The mStepInfos. */
	private List<StepInfoVO> mStepInfos = new ArrayList<StepInfoVO>();
	
	/** The mSkippedDataExceptions. */
	private Map<String,List<FeedSkippableException>> mSkippedDataExceptions = new HashMap<String,List<FeedSkippableException>>();
	
	/** The mEntityToRepositoryNameMap. */
	private Map<String,String> mEntityToRepositoryNameMap;
	
	/** The mEntityToItemDescriptorNameMap. */
	private Map<String,String> mEntityToItemDescriptorNameMap;
	
	/** The m mapper map. */
	private ServiceMap mRepositoryMap = null;
	
	/** The  m FeedItemUpdateList . */
	private List<BaseFeedProcessVO> mFeedItemUpdatedList =  new ArrayList<BaseFeedProcessVO>();

	/** The  m FeedItemAddList . */
	private List<BaseFeedProcessVO> mFeedItemAddedList =  new ArrayList<BaseFeedProcessVO>();
	
	/**
	 * @param pStepName - pStepName
	 * @return - FeedSkippedDataExceptions
	 */
	@SuppressWarnings("unchecked")
	public List<? extends FeedSkippableException> getFeedSkippedStepExceptions(String pStepName) {
		
		Object lExeplistObj =  mSkippedDataExceptions.get(pStepName);
		return (lExeplistObj == null) ? new ArrayList<FeedSkippableException>():(List<? extends FeedSkippableException>)lExeplistObj;
	}

	/**
	 * @return mSkippedDataExceptions
	 */
	public  Map<String,List<FeedSkippableException>> getAllFeedSkippedException(){
		return mSkippedDataExceptions;
	}

	/**
	 * @param pSkippedException - SkippedException
	 */
	@SuppressWarnings("unchecked")
	public void addFeedSkippedDataException(FeedSkippableException pSkippedException) {
		Object lExeplistObj =  mSkippedDataExceptions.get(pSkippedException.getStepName());
		List<FeedSkippableException> lExecpList = null;
		if(lExeplistObj == null)
		{
			lExecpList = new ArrayList<FeedSkippableException>();
			mSkippedDataExceptions.put(pSkippedException.getStepName(), lExecpList);
		}
		else
		{
			lExecpList = (List<FeedSkippableException>)lExeplistObj;
		}
		
		lExecpList.add(pSkippedException);
	}
	
	/**
	 * @return the mFileName
	 */
	public String getFileName() {
		return mFileName;
	}
	/**
	 * @param pFileName the FileName to set
	 */
	public void setFileName(String pFileName) {
		this.mFileName = pFileName;
	}
	/**
	 * @return the FilePath
	 */
	public String getFilePath() {
		return mFilePath;
	}
	/**
	 * @param pFilePath the FilePath to set
	 */
	public void setFilePath(String pFilePath) {
		this.mFilePath = pFilePath;
	}
	
	/**
	 * @return the fileType
	 */
	public String getFileType() {
		return mFileType;
	}

	/**
	 * @param pFileType the fileType to set
	 */
	public void setFileType(String pFileType) {
		mFileType = pFileType;
	}

	
	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return mSiteId;
	}

	/**
	 * @param pSiteId the siteId to set
	 */
	public void setSiteId(String pSiteId) {
		mSiteId = pSiteId;
	}

	/**
	 * 	
	 * @param pStepInfos - StepInfos
	 */
	public void setStepInfos(List<StepInfoVO> pStepInfos){
		this.mStepInfos = pStepInfos;
	}
	
	/**
	 * 
	 * @return lStepInfos
	 */
	public List<StepInfoVO> getStepInfos(){
		return  this.mStepInfos;
	}
	/**
	 * @param pName the Name
	 * @return value
	 */
	public Object getConfigValue(String pName) {
		return mHashMap.get(pName);
	}
	/**
	 * @param pName the Name
	 * @param pValue the Value
	 */
	public void setConfigValue(String pName, Object pValue) {
		mHashMap.put(pName, pValue);

	}
	
	/**
	 * @return the entityToRepositoryNameMap
	 */
	public Map<String, String> getEntityToRepositoryNameMap() {
		return mEntityToRepositoryNameMap;
	}

	/**
	 * @param pEntityToRepositoryNameMap the entityToRepositoryNameMap to set
	 */
	public void setEntityToRepositoryNameMap(
			Map<String, String> pEntityToRepositoryNameMap) {
		mEntityToRepositoryNameMap = pEntityToRepositoryNameMap;
	}

	/**
	 * @return the entityToItemDescriptorNameMap
	 */
	public Map<String, String> getEntityToItemDescriptorNameMap() {
		return mEntityToItemDescriptorNameMap;
	}

	/**
	 * @param pEntityToItemDescriptorNameMap the entityToItemDescriptorNameMap to set
	 */
	public void setEntityToItemDescriptorNameMap(
			Map<String, String> pEntityToItemDescriptorNameMap) {
		mEntityToItemDescriptorNameMap = pEntityToItemDescriptorNameMap;
	}
	

	/**
	 * @return the repositoryMap
	 */
	public ServiceMap getRepositoryMap() {
		return mRepositoryMap;
	}

	/**
	 * @param pRepositoryMap the repositoryMap to set
	 */
	public void setRepositoryMap(ServiceMap pRepositoryMap) {
		mRepositoryMap = pRepositoryMap;
	}

	/**
	 * 
	 * @return the entity list
	 */
			
	@SuppressWarnings("unchecked")
	public List<String> getEntityList() {
		List<String> lEntityList=(List<String>)getConfigValue(FeedConstants.ENTITY_LIST);
		if(null!=lEntityList && !lEntityList.isEmpty()){
			return lEntityList;
		}
		return null;
	}
	
	/**
	 * 
	 * @param pStepName - pStepName
	 * @return step entity list
	 */
	@SuppressWarnings("unchecked")
	protected List<String> getStepEntityList(String pStepName){
		List<String> lEntityList=null;
		Map<String, String> lMap=null;
		lMap=(Map<String,String>)getConfigValue(FeedConstants.STEP_ENTITY_LIST);
		if(!lMap.isEmpty()&& lMap.containsKey(pStepName)){
			lEntityList=Arrays.asList(lMap.get(pStepName).split(FeedConstants.COMA_SEPERATOR));
		}
		else{
			lEntityList=getEntityList();
		}
		return lEntityList;
		
	}
	/**
	 * 
	 * @param pRepoName the pRepoName
	 * @return repository
	 */
	public MutableRepository getRepository(String pRepoName) {
		if(getRepositoryMap()!=null){
			return (MutableRepository) getRepositoryMap().get(pRepoName);
		}else{
			return null;
		}
	}


	/**
	 * @return the mFeedItemUpdatedList
	 */
	public List<BaseFeedProcessVO> getFeedItemUpdatedList() {
		return mFeedItemUpdatedList;
	}

	/**
	 * @return the mFeedItemAddedList
	 */
	public List<BaseFeedProcessVO> getFeedItemAddedList() {
		return mFeedItemAddedList;
	}
	
	
	/**
	 * @param pBaseFeedProcessVOlist the pBaseFeedProcessVOlist to set
	 */
	public synchronized void addFeedItemsToUpdateList(List<BaseFeedProcessVO> pBaseFeedProcessVOlist) {
		this.mFeedItemUpdatedList.addAll(pBaseFeedProcessVOlist);
	}

	/**
	 * @param pBaseFeedProcessVOlist the pBaseFeedProcessVOlist to set
	 */
	public synchronized void addFeedItemsToInsertList(List<BaseFeedProcessVO> pBaseFeedProcessVOlist) {
			this.mFeedItemAddedList.addAll(pBaseFeedProcessVOlist);
	}
}
