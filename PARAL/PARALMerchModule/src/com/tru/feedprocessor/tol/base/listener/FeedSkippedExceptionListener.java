package com.tru.feedprocessor.tol.base.listener;


import org.springframework.batch.core.SkipListener;

import com.tru.feedprocessor.tol.base.FeedHelper;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 *  The  class FeedSkippedExceptionListener 
 *  
 * The listener interface for receiving feedSkippedException events. The class
 * that is interested in processing a feedSkippedException event implements this
 * interface, and the object created with that class is registered with a
 * component using the component's 
 * <code>addFeedSkippedExceptionListener</code> method. When
 * the feedSkippedException event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see FeedSkippedExceptionEvent
 * 
 * @version 1.1
 * @author Professional Access
 */
public class FeedSkippedExceptionListener implements SkipListener<BaseFeedProcessVO, BaseFeedProcessVO> {

	/** The m feed data holder. */
	private FeedHelper mFeedHelper;
	
	/**
	 * Gets the feed helper.
	 *
	 * @return the mFeedHelper
	 */
	public FeedHelper getFeedHelper() {
		return mFeedHelper;
	}

	/**
	 * Sets the feed helper.
	 *
	 * @param pFeedHelper the new feed helper
	 */
	public void setFeedHelper(FeedHelper pFeedHelper) {
		mFeedHelper = pFeedHelper;
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.batch.core.SkipListener#onSkipInProcess(java.lang.Object, java.lang.Throwable)
	 */
	@Override
	public void onSkipInProcess(BaseFeedProcessVO pEntityVO, Throwable pException) {
		pEntityVO.setSkipped(true);
		FeedSkippableException feedException =(FeedSkippableException)pException;
		feedException.setFeedItemVO(pEntityVO);
		getCurrentFeedExecutionContext().addFeedSkippedDataException(feedException);
		
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.SkipListener#onSkipInRead(java.lang.Throwable)
	 */
	@Override
	public void onSkipInRead(Throwable pException) {
		getCurrentFeedExecutionContext().addFeedSkippedDataException((FeedSkippableException)pException);
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.SkipListener#onSkipInWrite(java.lang.Object, java.lang.Throwable)
	 */
	@Override
	public void onSkipInWrite(BaseFeedProcessVO pEntityVO, Throwable pException) {
		pEntityVO.setSkipped(true);
		FeedSkippableException feedException =(FeedSkippableException)pException;
		feedException.setFeedItemVO(pEntityVO);
		getCurrentFeedExecutionContext().addFeedSkippedDataException(feedException);
	}
	
	/**
	 * Gets the current feed execution context.
	 * 
	 * @return - current execution context
	 */
	protected FeedExecutionContextVO getCurrentFeedExecutionContext() {
		return getFeedHelper().getCurrentFeedExecutionContext();
	}
}
