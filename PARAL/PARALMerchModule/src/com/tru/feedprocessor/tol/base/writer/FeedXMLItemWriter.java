package com.tru.feedprocessor.tol.base.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.Result;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.WriteFailedException;
import org.springframework.batch.item.file.ResourceAwareItemWriterItemStream;
import org.springframework.batch.item.util.FileUtils;
import org.springframework.batch.item.xml.StaxUtils;
import org.springframework.batch.item.xml.StaxWriterCallback;
import org.springframework.batch.item.xml.stax.NoStartEndDocumentStreamWriter;
import org.springframework.batch.support.transaction.TransactionAwareBufferedWriter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.XmlMappingException;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
 

 /**
 * The Class FeedXMLItemWriter.
 * 
 * The  default writer for FeedXML 
 *
 * @version 1.1
 * @author Professional Access
 */
public class FeedXMLItemWriter extends AbstractFeedItemWriter
   implements ResourceAwareItemWriterItemStream<BaseFeedProcessVO>, InitializingBean
 {
	
	/**
	 * Property to hold mGroupEntityTag.
	 */
	protected String mGroupEntityTag;
	
	/**
	 * The Resource.
	 */
	protected Resource mResource;
	
	/**
	 * The Marshaller.
	 */
	protected Marshaller mMarshaller;
	
	/**
	 * The Encoding.
	 */
	protected String mEncoding = FeedConstants.DEFAULT_ENCODING;
	
	/**
	 * The Version.
	 */
	protected String mVersion =FeedConstants.DEFAULT_XML_VERSION;
	
	/**
	 * The Root tag name.
	 */
	protected String mRootTagName = FeedConstants.DEFAULT_ROOT_TAG_NAME;
	
	/**
	 * The Root tag namespace prefix.
	 */
	protected String mRootTagNamespacePrefix = FeedConstants.EMPTY;
	
	/**
	 * The Root tag namespace.
	 */
	protected String mRootTagNamespace = FeedConstants.EMPTY;
	
	/**
	 * The Root element attributes.
	 */
	protected Map<String, String> mRootElementAttributes = null;
	
	/**
	 * The Overwrite output.
	 */
	protected boolean mOverwriteOutput = true;
	
	/**
	 * The Channel.
	 */
	protected FileChannel mChannel;
	
	/**
	 * The Event writer.
	 */
	protected XMLEventWriter mEventWriter;
	
	/**
	 * The Delegate event writer.
	 */
	protected XMLEventWriter mDelegateEventWriter;
	
	/**
	 * The Current record count.
	 */
	protected long mCurrentRecordCount = FeedConstants.LONG_ZERO;
	
	/**
	 * The Save state.
	 */
	protected boolean mSaveState = true;
	
	/**
	 * The Header callback.
	 */
	protected StaxWriterCallback mHeaderCallback;
	
	/**
	 * The Footer callback.
	 */
	protected StaxWriterCallback mFooterCallback;
	
	/**
	 * The Buffered writer.
	 */
	protected Writer mBufferedWriter;
	
	/**
	 * The Transactional.
	 */
	protected boolean mTransactional = true;
	
	/**
	 * The Force sync.
	 */
	protected boolean mForceSync;
	
	/**
	 * The Should delete if empty.
	 */
	protected boolean mShouldDeleteIfEmpty = false;
 
	/**
	 * Sets the resource.
	 * 
	 * @param pResource
	 *            the resource to set
	 */
	public void setResource(Resource pResource) {
		this.mResource = pResource;
	}
	
	/**
	 * Sets the marshaller.
	 * 
	 * @param pMarshaller
	 *            the Marshaller to set
	 */
	public void setMarshaller(Marshaller pMarshaller) {
		this.mMarshaller = pMarshaller;
	}
	
	/**
	 * Sets the header callback.
	 * 
	 * @param pHeaderCallback
	 *            - HeaderCallback
	 */
	public void setHeaderCallback(StaxWriterCallback pHeaderCallback) {
		this.mHeaderCallback = pHeaderCallback;
	}
	
	/**
	 * Sets the footer callback.
	 * 
	 * @param pFooterCallback
	 *            - FooterCallback
	 */
	public void setFooterCallback(StaxWriterCallback pFooterCallback) {
		this.mFooterCallback = pFooterCallback;
	}
	
	/**
	 * Sets the transactional.
	 * 
	 * @param pTransactional
	 *            - Transactional
	 */
	public void setTransactional(boolean pTransactional) {
		this.mTransactional = pTransactional;
	}
	
	/**
	 * Sets the force sync.
	 * 
	 * @param pForceSync
	 *            - ForceSync
	 */
	public void setForceSync(boolean pForceSync) {
		this.mForceSync = pForceSync;
	}
	
	/**
	 * Sets the should delete if empty.
	 * 
	 * @param pShouldDeleteIfEmpty
	 *            - ShouldDeleteIfEmpty
	 */
	public void setShouldDeleteIfEmpty(boolean pShouldDeleteIfEmpty) {
		this.mShouldDeleteIfEmpty = pShouldDeleteIfEmpty;
	}
	
	/**
	 * Gets the encoding.
	 * 
	 * @return Encoding
	 */
	public String getEncoding() {
		return this.mEncoding;
	}
	
	/**
	 * Sets the encoding.
	 * 
	 * @param pEncoding
	 *            - Encoding
	 */
	public void setEncoding(String pEncoding) {
		this.mEncoding = pEncoding;
	}
	
	/**
	 * Gets the version.
	 * 
	 * @return - Version
	 */
	public String getVersion() {
		return this.mVersion;
	}
	
	/**
	 * Sets the version.
	 * 
	 * @param pVersion
	 *            - Version
	 */
	public void setVersion(String pVersion) {
		this.mVersion = pVersion;
	}
	
	/**
	 * Gets the root tag name.
	 * 
	 * @return RootTagName
	 */
	public String getRootTagName() {
		return this.mRootTagName;
	}
	
	/**
	 * Sets the root tag name.
	 * 
	 * @param pRootTagName
	 *            - RootTagName
	 */
	public void setRootTagName(String pRootTagName) {
		this.mRootTagName = pRootTagName;
	}
	
	/**
	 * Gets the root tag namespace prefix.
	 * 
	 * @return RootTagNamespacePrefix
	 */
	public String getRootTagNamespacePrefix() {
		return this.mRootTagNamespacePrefix;
	}
	
	/**
	 * Gets the root tag namespace.
	 * 
	 * @return RootTagNamespace
	 */
	public String getRootTagNamespace() {
		return this.mRootTagNamespace;
	}
	
	/**
	 * Gets the root element attributes.
	 * 
	 * @return RootElementAttributes
	 */
	public Map<String, String> getRootElementAttributes() {
		return this.mRootElementAttributes;
	}
	
	/**
	 * Sets the root element attributes.
	 * 
	 * @param pRootElementAttributes
	 *            - RootElementAttributes
	 */
	public void setRootElementAttributes(
			Map<String, String> pRootElementAttributes) {
		this.mRootElementAttributes = pRootElementAttributes;
	}
	
	/**
	 * Sets the overwrite output.
	 * 
	 * @param pOverwriteOutput
	 *            - OverwriteOutput
	 */
	public void setOverwriteOutput(boolean pOverwriteOutput) {
		this.mOverwriteOutput = pOverwriteOutput;
	}
	
	/**
	 * Sets the save state.
	 * 
	 * @param pSaveState
	 *            - SaveState
	 */
	public void setSaveState(boolean pSaveState) {
		this.mSaveState = pSaveState;
	}
	
	/**
	 * After properties set.
	 * 
	 */
	public void afterPropertiesSet() {
		Assert.notNull(this.mMarshaller);
		if (this.mRootTagName.contains(FeedConstants.BRACE_OPEN)) {
			this.mRootTagNamespace = this.mRootTagName.replaceAll(FeedConstants.REGULER_EXPRESSION_1,FeedConstants.DOLLER_ONE);
			this.mRootTagName = this.mRootTagName.replaceAll(FeedConstants.REGULER_EXPRESSION_2, FeedConstants.DOLLER_ONE);
			if (this.mRootTagName.contains(FeedConstants.COLON)) {
				this.mRootTagNamespacePrefix = this.mRootTagName.replaceAll(FeedConstants.REGULER_EXPRESSION_3,FeedConstants.DOLLER_ONE);
				this.mRootTagName = this.mRootTagName.replaceAll(FeedConstants.REGULER_EXPRESSION_4, FeedConstants.DOLLER_ONE);
			}
		}
	}
	
	/**
	 * opens the file at starting position.
	 * 
	 * @param pExecutionContext
	 *            - ExecutionContext
	 */
   public void open(ExecutionContext pExecutionContext)
   {		 
     super.open(pExecutionContext);
 
     Assert.notNull(this.mResource, FeedConstants.THE_RESOURCE_MUST_BE_SET);
 
     long startAtPosition = FeedConstants.LONG_ZERO;
     boolean restarted = false;

     open(startAtPosition, restarted);
 
     if (startAtPosition != FeedConstants.LONG_ZERO){
    	 return;
     }
     try {
       if (this.mHeaderCallback != null){
         this.mHeaderCallback.write(this.mDelegateEventWriter);
       }
     }
     catch (IOException e)
     {
      throw new ItemStreamException(FeedConstants.FAILED_TO_WRITE_HEADER_ITEMS, e);
     }
   }
   
   /**
	 * Open.
	 * 
	 * @param pPosition
	 *            - Position
	 * @param pRestarted
	 *            - Restarted
	 */
   protected void open(long pPosition, boolean pRestarted)
   {
		FileOutputStream os = null;
		FileChannel fileChannel = null;
		try {
			File file = this.mResource.getFile();
			FileUtils.setUpOutputFile(file, pRestarted, false,
					this.mOverwriteOutput);
			Assert.state(this.mResource.exists(), FeedConstants.OUTPUT_RESOURCE_EXIST);
			os = new FileOutputStream(file, true);
			fileChannel = os.getChannel();
			this.mChannel = os.getChannel();
			setPosition(pPosition);
     }
     catch (IOException ioe) {
      throw new DataAccessResourceFailureException(FeedConstants.UNABLE_TO_WRITE_TO_FILE_RESOURCE + this.mResource + FeedConstants.CLOSING_BRACE, ioe);
     }finally{
    	 if (os != null) {
		        try {
		        	os.close();
		        } catch (IOException ex) {
		        	if (getLogger().isLoggingError()) {
		        		getLogger().logError(
								FeedConstants.ERROR + FeedConstants.IFUN
										+ ex.getMessage(), ex);
					}
		        }
		    }
  }
 
		XMLOutputFactory outputFactory = createXmlOutputFactory();

		if (outputFactory
				.isPropertySupported(FeedConstants.AUTOMATICENDELEMENTS)) {
			outputFactory.setProperty(FeedConstants.AUTOMATICENDELEMENTS,
					Boolean.FALSE);
		}
		if (outputFactory
				.isPropertySupported(FeedConstants.OUTPUT_VALIDATESTRUCTURE)) {
			outputFactory.setProperty(FeedConstants.OUTPUT_VALIDATESTRUCTURE,
					Boolean.FALSE);
		}
		try {
			FileChannel channel = fileChannel;
			if (this.mTransactional) {
				TransactionAwareBufferedWriter writer = new TransactionAwareBufferedWriter(
						channel, new Runnable() {
							public void run() {
								FeedXMLItemWriter.this.closeStream();
							}
						});
				writer.setEncoding(this.mEncoding);
				writer.setForceSync(this.mForceSync);
				this.mBufferedWriter = writer;
			} else {
				this.mBufferedWriter = new BufferedWriter(
						new OutputStreamWriter(os, this.mEncoding));
			}
			this.mDelegateEventWriter = createXmlEventWriter(outputFactory,
					this.mBufferedWriter);
			this.mEventWriter = new NoStartEndDocumentStreamWriter(
					this.mDelegateEventWriter);
			initNamespaceContext(this.mDelegateEventWriter);
			if (!(pRestarted)) {
				startDocument(this.mDelegateEventWriter);
				if (this.mForceSync){
					channel.force(false);
				}
			}
		} catch (XMLStreamException xse) {
			throw new DataAccessResourceFailureException(
					FeedConstants.UNABLE_TO_WRITE_TO_FILE_RESOURCE + this.mResource + FeedConstants.CLOSING_BRACE,
					xse);
		} catch (UnsupportedEncodingException e) {
			throw new DataAccessResourceFailureException(
					FeedConstants.UNABLE_TO_WRITE_TO_FILE_RESOURCE+ this.mResource
							+ FeedConstants.WITH_ENCODING + this.mEncoding + FeedConstants.CLOSING_BRACE, e);
		} catch (IOException e) {
			throw new DataAccessResourceFailureException(
					FeedConstants.UNABLE_TO_WRITE_TO_FILE_RESOURCE + this.mResource + FeedConstants.CLOSING_BRACE,
					e);
		}
	}
   
   /**
	 * Creates the xml event writer.
	 * 
	 * @param pOutputFactory
	 *            - OutputFactory
	 * @param pWriter
	 *            - Writer
	 * @return XmlEventWriter
	 * @throws XMLStreamException
	 *             - XMLStreamException
	 */
	protected XMLEventWriter createXmlEventWriter(
			XMLOutputFactory pOutputFactory, Writer pWriter)
			throws XMLStreamException {
		return pOutputFactory.createXMLEventWriter(pWriter);
	}
	
	/**
	 * Creates the xml output factory.
	 * 
	 * @return - new instance of XMLOutputFactory
	 * @throws FactoryConfigurationError
	 *             - FactoryConfigurationError
	 */
	protected XMLOutputFactory createXmlOutputFactory()
			throws FactoryConfigurationError {
		return XMLOutputFactory.newInstance();
	}
	
	/**
	 * Creates the xml event factory.
	 * 
	 * @return - new instance of XMLEventFactory
	 * @throws FactoryConfigurationError
	 *             - FactoryConfigurationError
	 */
	protected XMLEventFactory createXmlEventFactory()
			throws FactoryConfigurationError {
		XMLEventFactory factory = XMLEventFactory.newInstance();
		return factory;
	}
	
	/**
	 * Creates the stax result.
	 * 
	 * @return - staxResult
	 * @throws Exception
	 *             - Exception
	 */
	protected Result createStaxResult() throws Exception {
		return StaxUtils.getResult(this.mEventWriter);
	}

/**
 * Inits the namespace context.
 * 
 * @param pWriter
 *            - Writer
 * @throws XMLStreamException
 *             - XMLStreamException
 */
protected void initNamespaceContext(XMLEventWriter pWriter)
     throws XMLStreamException
   {
     if (StringUtils.hasText(getRootTagNamespace())) {
       if (StringUtils.hasText(getRootTagNamespacePrefix())){
    	   pWriter.setPrefix(getRootTagNamespacePrefix(), getRootTagNamespace());
       }
       else {
    	   pWriter.setDefaultNamespace(getRootTagNamespace());
       }
     }
    if (!(CollectionUtils.isEmpty(getRootElementAttributes()))){
      for (Map.Entry entry : getRootElementAttributes().entrySet()) {
         String key = (String)entry.getKey();
         if (key.startsWith(FeedConstants.XMLNS) || key.startsWith(FeedConstants.XMLNS_G) || key.startsWith(FeedConstants.XMLNS_C)) {
           String prefix = FeedConstants.EMPTY;
           if (key.contains(FeedConstants.COLON)) {
             prefix = key.substring(key.indexOf(FeedConstants.COLON) + FeedConstants.NUMBER_ONE);
           }
           if(getLogger().isLoggingDebug()){
        	   getLogger().logDebug(FeedConstants.REGISTERING_PREFIX + prefix + FeedConstants.EQUAL + ((String)entry.getValue()));
         	}
           pWriter.setPrefix(prefix, (String)entry.getValue());
         }
       }
   	}
   }
 
 /**
	 * Start document.
	 * 
	 * @param pWriter
	 *            - Writer
	 * @throws XMLStreamException
	 *             - XMLStreamException
	 */
protected void startDocument(XMLEventWriter pWriter)
     throws XMLStreamException{
	   Calendar cal = Calendar.getInstance();
	   SimpleDateFormat dateFormat = new SimpleDateFormat(FeedConstants.DATE_FORMAT_NOW, Locale.getDefault());
     XMLEventFactory factory = createXmlEventFactory();
 
     pWriter.add(factory.createStartDocument(getEncoding(), getVersion()));
 
     pWriter.add(factory.createStartElement(getRootTagNamespacePrefix(), getRootTagNamespace(), getRootTagName()));
     if (StringUtils.hasText(getRootTagNamespace())) {
       if (StringUtils.hasText(getRootTagNamespacePrefix())) {
    	   pWriter.add(factory.createNamespace(getRootTagNamespacePrefix(), getRootTagNamespace()));
       }
       else {
    	   pWriter.add(factory.createNamespace(getRootTagNamespace()));
       }
     }
 
     if (!(CollectionUtils.isEmpty(getRootElementAttributes())))
     {
       for (Map.Entry entry : getRootElementAttributes().entrySet()) {
         String key = (String)entry.getKey();
         if (key.startsWith(FeedConstants.XMLNS) || key.startsWith(FeedConstants.XMLNS_G) || key.startsWith(FeedConstants.XMLNS_C)) {
           String prefix = FeedConstants.EMPTY;
           if (key.contains(FeedConstants.COLON)) {
             prefix = key.substring(key.indexOf(FeedConstants.COLON) + FeedConstants.NUMBER_ONE);
           }
           pWriter.add(factory.createNamespace(prefix, (String)entry.getValue()));
         }
         else {
        	 pWriter.add(factory.createAttribute(key, (String)entry.getValue()));
         }
       }
       pWriter.add(factory.createAttribute(FeedConstants.XML_EXTRACTDATE, dateFormat.format(cal.getTime())));
     }
 
//     pWriter.add(factory.createIgnorableSpace(FeedConstants.EMPTY));
     pWriter.flush();
   }
   
   /**
	 * End document.
	 * 
	 * @param pWriter
	 *            - Writer
	 * @throws XMLStreamException
	 *             - XMLStreamException
	 */
   protected void endDocument(XMLEventWriter pWriter)
     throws XMLStreamException
   {
     try {
    	 if(mGroupEntityTag!=null){
    		 this.mBufferedWriter.write(FeedConstants.XML_END_QUOTES + getRootTagName() + FeedConstants.END_QUOTE);
    	 }
     }
     catch (IOException ioe) {
       throw new DataAccessResourceFailureException(FeedConstants.UNABLE_TO_CLOSE_FILE_RESOURCE + this.mResource + FeedConstants.CLOSING_BRACE, ioe);
     }
   }
	
	/**
	 * closing the file.
	 */
   public void close()
   {
     super.close();
 
     XMLEventFactory factory = createXmlEventFactory();
     try {
       this.mDelegateEventWriter.add(factory.createCharacters(FeedConstants.EMPTY));
     }
     catch (XMLStreamException e) {
    	 if (isLoggingError()) {
				logError(e.getMessage(),e);
			} 
     }
     try
     {
       if (this.mFooterCallback != null) {
         this.mFooterCallback.write(this.mDelegateEventWriter);
       }
       this.mDelegateEventWriter.flush();
       try
       {
         this.mResource.getFile().delete();
       }
       catch (IOException e) {
    	   if (isLoggingError()) {
    		   logError(FeedConstants.FAILED_TO_DELETE_EMPTY_FILE_ON_CLOSE, e);
    	   }
         throw new ItemStreamException(FeedConstants.FAILED_TO_DELETE_EMPTY_FILE_ON_CLOSE, e);
       }
     }
     catch (IOException e)
     {
    	 throw new ItemStreamException(FeedConstants.FAILED_TO_DELETE_EMPTY_FILE_ON_CLOSE, e);
     }
     catch (XMLStreamException e)
     {
    	 throw new ItemStreamException(e);
     }
     finally
     {
       try
       {	
    	   this.mDelegateEventWriter.add(factory.createEndElement(FeedConstants.XML_END_QUOTES, mGroupEntityTag , FeedConstants.CLOSING_BRACE));
    	   this.mDelegateEventWriter.close();
    	   endDocument( this.mDelegateEventWriter);
       }
       catch (XMLStreamException e) {
    	   if (isLoggingError()) {
				logError(FeedConstants.UNABLE_TO_CLOSE_FILE_RESOURCE + this.mResource +  FeedConstants.CLOSING_BRACE + e,e);
			}
       }
       finally {
         try {
           this.mBufferedWriter.close();
         }
         catch (IOException e) {
        	 if (isLoggingError()) {
					logError(FeedConstants.UNABLE_TO_CLOSE_FILE_RESOURCE + this.mResource +  FeedConstants.CLOSING_BRACE + e,e);
				} 
         }
         finally {
           if (!(this.mTransactional)) {
             closeStream();
           }
         }
       }
       if ((this.mCurrentRecordCount == FeedConstants.LONG_ZERO) && (this.mShouldDeleteIfEmpty)){
         try {
           this.mResource.getFile().delete();
         }
         catch (IOException e) {
           throw new ItemStreamException(FeedConstants.FAILED_TO_DELETE_EMPTY_FILE_ON_CLOSE, e);
         }
       }
     }
   }
   
   /**
	 * close the stream.
	 */
   protected void closeStream()
   {
     try {
       this.mChannel.close();
     }
     catch (IOException ioe) {
    	 if (isLoggingError()) {
				logError(FeedConstants.UNABLE_TO_CLOSE_FILE_RESOURCE + this.mResource +FeedConstants.CLOSING_BRACE + ioe,ioe);
			}
     }
   }
 
 /**
	 * Write element.
	 * 
	 * @param pItem
	 *            - Item
	 * @throws Exception - Exception
	 */
   public void writeElement(BaseFeedProcessVO pItem)
     throws Exception
   {
	   writeStartElement(pItem);
	   
	   Result result = createStaxResult();
	   this.mMarshaller.marshal(pItem, result);
   }
   
   /**
	 * Write start element.
	 * 
	 * @param pObject
	 *            -Object
	 * @throws XMLStreamException
	 *             - XMLStreamException
	 */
   protected void writeStartElement(BaseFeedProcessVO pObject) throws XMLStreamException {
   	 XMLEventFactory factory = createXmlEventFactory();
   	 String lEntityName=pObject.getEntityName();
   	 Map<String,String> lEntityTOGroupNameMap = (Map<String, String>)getConfigValue(FeedConstants.ENTITY_TO_GROUP_NAME);
   	 if(mGroupEntityTag==null){
   		 mGroupEntityTag=lEntityTOGroupNameMap.get(lEntityName);
   		 if(mGroupEntityTag != null){
   			 mDelegateEventWriter.add(factory.createStartElement(FeedConstants.EMPTY,FeedConstants.EMPTY ,mGroupEntityTag));
   		 }
   	 }
   	 else if(mGroupEntityTag!=lEntityTOGroupNameMap.get(lEntityName)){
   		 mDelegateEventWriter.add(factory.createEndElement(FeedConstants.XML_END_QUOTES, mGroupEntityTag , FeedConstants.END_QUOTE));
   		 mDelegateEventWriter.add(factory.createStartElement(FeedConstants.EMPTY ,FeedConstants.EMPTY ,lEntityTOGroupNameMap.get(lEntityName)));
   		 mGroupEntityTag=lEntityTOGroupNameMap.get(lEntityName);
   	 }
   }
   /*
    * (non-Javadoc)
    * @see com.tru.feedprocessor.tol.AbstractFeedItemWriter#doOpen()
    */
	@Override
	public void doOpen()  {
		mGroupEntityTag=null;
	}
   /*
    * (non-Javadoc)
    * @see com.tru.feedprocessor.tol.AbstractFeedItemWriter#doWrite(java.util.List)
    */
   @Override
	public void doWrite(List<? extends BaseFeedProcessVO> pItems)
			throws RepositoryException, FeedSkippableException,
			FeedSkippableException {
		 this.mCurrentRecordCount += pItems.size();
		 
	     for (Iterator lIterator = pItems.iterator(); lIterator.hasNext(); ) {
	    	 BaseFeedProcessVO object = (BaseFeedProcessVO) lIterator.next();
	    	 Assert.state(this.mMarshaller.supports(object.getClass()),FeedConstants.MARSHALLER_MUST_SUPPORT_MESSAGE);
	 
			try {
				
				writeElement(object);
				
			} catch (XmlMappingException xe) {
				throw new FeedSkippableException(getPhaseName(),  getStepName(), xe.getMessage(),object,xe);
			} catch (Exception e) {
				throw new FeedSkippableException(getPhaseName(),  getStepName(),e.getMessage(),object,e);
			}
			 try {
			       this.mEventWriter.flush();
			       if (this.mForceSync){
			         this.mChannel.force(false);
			       }
			     }
			     catch (XMLStreamException e)
			     {
			       throw new WriteFailedException(FeedConstants.FAILED_TO_FLUSH_THE_EVENTS, e);
			     }
			     catch (IOException e) {
			       throw new WriteFailedException(FeedConstants.FAILED_TO_FLUSH_THE_EVENTS, e);
			     }
		
	     }
	}
   
   /**
	 * Gets the position.
	 * 
	 * @return - Position
	 */
   protected long getPosition()
   {
     long position  = 0;
     try
     {
			this.mEventWriter.flush();
			try {
				position = this.mChannel.position();

				if (this.mBufferedWriter instanceof TransactionAwareBufferedWriter) {
					position += ((TransactionAwareBufferedWriter) this.mBufferedWriter)
							.getBufferSize();
				}
			} catch (IOException e) {
				throw new DataAccessResourceFailureException(
						FeedConstants.UNABLE_TO_WRITE_TO_FILE_RESOURCE
								+ this.mResource + FeedConstants.CLOSING_BRACE,
						e);
			}
		}catch(XMLStreamException ex){
    	 throw new DataAccessResourceFailureException(FeedConstants.UNABLE_TO_WRITE_TO_FILE_RESOURCE+ this.mResource + FeedConstants.CLOSING_BRACE, ex);
     }
 
     return position;
   }
 
 /**
	 * Sets the position.
	 * 
	 * @param pNewPosition
	 *            - NewPosition
	 */
   protected void setPosition(long pNewPosition)
   {
     try {
       this.mChannel.truncate(pNewPosition);
       this.mChannel.position(pNewPosition);
     }
     catch (IOException e) {
       throw new DataAccessResourceFailureException(FeedConstants.UNABLE_TO_WRITE_TO_FILE_RESOURCE + this.mResource + FeedConstants.CLOSING_BRACE, e);
     }
   }

}

