package com.tru.feedprocessor.tol.base.tasklet;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;

/**
 * The Class ValidateFileTask. This class is used for validating the input feed
 * file. Project team has to override doExecute() method and do the file
 * validation. If the file validation fails then they should throw
 * FeedSkippableException.
 * @author Professional Access
 * @version 1.0
 */
public class ValidateDataTask extends AbstractTasklet {

	@Override
	protected void doExecute() throws FeedSkippableException {
		// TODO Auto-generated method stub
	}


}
