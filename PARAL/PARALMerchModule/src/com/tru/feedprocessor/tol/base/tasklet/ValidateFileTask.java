package com.tru.feedprocessor.tol.base.tasklet;

import java.io.File;
import java.net.URL;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.io.FilenameUtils;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class ValidateFileTask. This class is used for validating the input feed
 * file. Project team has to override doExecute() method and do the file
 * validation. If the file validation fails then they should throw
 * FeedSkippableException.
 * @author Professional Access
 * @version 1.0
 * 
 */
public class ValidateFileTask extends ValidateDataTask {

	/*The mXmlSchemaNsURI */
	private String mXmlSchemaNsURI = null;
	
	/*The schemaFileXsdUri */
	private String mSchemaFileXsdUri = null; 
	
	/**
	 * @return the schemaFileXsdUri
	 */
	public String getSchemaFileXsdUri() {
		return mSchemaFileXsdUri;
	}
	/**
	 * @param pSchemaFileXsdUri the schemaFileXsdUri to set
	 */
	public void setSchemaFileXsdUri(String pSchemaFileXsdUri) {
		mSchemaFileXsdUri = pSchemaFileXsdUri;
	}
	/**
	 * @return the xmlSchemaNsURI
	 */
	public String getXmlSchemaNsURI() {
		return mXmlSchemaNsURI;
	}
	/**
	 * @param pXmlSchemaNsURI the xmlSchemaNsURI to set
	 */
	public void setXmlSchemaNsURI(String pXmlSchemaNsURI) {
		mXmlSchemaNsURI = pXmlSchemaNsURI;
	}
	
	/**
	 * Do execute.
	 *
	 * @throws FeedSkippableException the feed skippable exception
	 * @see com.tru.feedprocessor.tol.AbstractTasklet#doExecute() This is the
	 * blank method where project team has to put the code which will be
	 * performed during validation of the file
	 */
	@Override
	protected void doExecute() throws FeedSkippableException {
		
		FeedExecutionContextVO curExecCntx = getCurrentFeedExecutionContext();
		File file = new File(curExecCntx.getFilePath()+curExecCntx.getFileName());
		
		boolean flag = validateFile (file);
		
		if(!flag){
			throw new FeedSkippableException(getPhaseName(), getStepName(),
					FeedConstants.INVALID_FILE,  null,null);
		}
	}
	/**
	 * 
	 * @param pFile - File
	 * @return boolean
	 */
	public boolean validateFile(File pFile){
		
		boolean isValidFeed = true;
		
		if (FilenameUtils.getExtension(pFile.toString()).equals(FeedConstants.XML)) {
			
			SchemaFactory schemaFactory= null;
			Schema schema = null;
			Source xmlFile = new StreamSource(pFile);
			String  lSchemaFileXsdUri = getSchemaFileXsdUri();
			String lXmlSchemaNsURI = getXmlSchemaNsURI();
		
			try {
				
				if(StringUtils.isEmpty(lXmlSchemaNsURI)){
					getLogger().logWarning("XML_SCHEMA_NS_URI Uri is null or Empty in Validate File Task");
				}else{
					schemaFactory = SchemaFactory.newInstance(lXmlSchemaNsURI);
				}
				
				if(StringUtils.isEmpty(lSchemaFileXsdUri)){
					getLogger().logError("XSD Uri is null or Empty in Validate File Task");
				}else{
					if(lSchemaFileXsdUri.contains(FeedConstants.HTTP)){
						schema = schemaFactory.newSchema(new URL(lSchemaFileXsdUri));
					}
					else{
						schema = schemaFactory.newSchema(new File(getSchemaFileXsdUri()));
					}
				}
				
				Validator validator = schema.newValidator();
				validator.validate(xmlFile);
				
			} catch (Exception ex) {
				if (isLoggingError()) {
					logError(xmlFile.getSystemId() + "        File is NOT valid       --> " + "Reason: - >", ex);
				}
				isValidFeed = false;
			}
		}
		return isValidFeed;
	}
}
