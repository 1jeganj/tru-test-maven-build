package com.tru.feedprocessor.tol.base.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;

import atg.nucleus.ServiceMap;
import atg.repository.MutableRepository;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
/**
 * The Class AbstractFeedConfig.
 * 
 * @version 1.1
 * @author Professional Access
 */
public abstract class AbstractFeedConfig implements IFeedConfig,InitializingBean  {

	/** The m mapper map. */
	private ServiceMap mRepositoryMap = null;
	/** The m hash map. */
	private Map<String, Object> mHashMap = new HashMap<String, Object>();
	/** The mLogger. */
	private FeedLogger mLogger;
	/** The mFileName. */
	private String mFileName;
	/** The mFilePath. */
	private String mFilePath;
	/** The mFileType. */
	private String mFileType;
	/** The mEntitySQL. */
	private Map<String,String> mEntitySQL;
	/** The mEntityList. */
	private List<String> mEntityList;
	/** The mFileParserClassFQCN. */
	private String mDataLoaderClassFQCN;
	/** The mEntityProviderFQCN. */
	private String mEntityProviderFQCN;
	/** The mEntityToRepositoryNameMap. */
	private Map<String,String> mEntityToRepositoryNameMap;
	/** The mEntityToItemDescriptorNameMap. */
	private Map<String,String> mEntityToItemDescriptorNameMap;
	/** The mUseAutoDeployWorkflow. */
	private boolean mUseAutoDeployWorkflow;
	/** The mAutoWorkflowName. */
	private String mAutoWorkflowName;
	/** The mManualWorkflowName. */
	private String mManualWorkflowName;
	/** The mStepEntityList. */
	private Map<String, String> mStepEntityList;
	
	/**
	 * The mXmlRootElement.
	 */
	private String mXmlRootElement;
	
	/**
	 * The mVersioned.
	 */
	private boolean mVersioned;
	
	/** The mFileDownloderFlag. */
	private String mFileDownloaderFlag;
	
	/** The mFileUploderFlag. */
	private String mFileUploaderFlag;
	
	/** The mServerIp. */
	private String mHostName;
	/** The mServerPort. */
	private String mHostPort;
	/** The mFptSftpUser. */
	private String mUserName;
	/** The mPassword. */
	private String mPassword;
	/** The mFtpSftpSourceDirectory. */
	private String mSourceDirectory;
	/** The mProtocolType. */
	private String mProtocolType;
	/** The mDownloadFileType. */
	private String mDownloadFileType;
	/** The mFileNameOrFormat. */
	private String mDownloadFileName;
	
	/** The mDownloadFileType. */
	private String mUploadFileType;
	/** The mFileNameOrFormat. */
	private String mUploadFileName;
	
	/**
	 * Sets the entity sql.
	 *
	 * @param pMap the map
	 */
	public void setEntityToRepositoryNameMap(Map<String, String> pMap) {
		mEntityToRepositoryNameMap=pMap;
		setConfigValue(FeedConstants.ENTITYTOREPOSITORYNAMEMAP, pMap);
	}

	/**
	 * Gets the entity to repository name map.
	 * 
	 * @return the entityToRepositoryNameMap
	 */
	public Map<String, String> getEntityToRepositoryNameMap() {
		return mEntityToRepositoryNameMap;
	}
	
	
	/**
	 * Gets the entity provider fqcn.
	 * 
	 * @return the feedEntityProviderFQCN
	 */
	public String getEntityProviderFQCN() {
		return mEntityProviderFQCN;
	}
	
	/**
	 * Sets the cat feed entry provider pEntityProviderFQCN.
	 * 
	 * @param pEntityProviderFQCN
	 *            the new cat feed entry provider pEntityProviderFQCN
	 */
	public void setEntityProviderFQCN(String pEntityProviderFQCN) {
		mEntityProviderFQCN=pEntityProviderFQCN;
		setConfigValue(FeedConstants.ENTITY_PROVIDER_FQCN,
				pEntityProviderFQCN);
	}
	
	
	/**
	 * Gets the use auto deploy workflow.
	 * 
	 * @return the useAutoDeployWorkflow
	 */
	public boolean isUseAutoDeployWorkflow() {
		return mUseAutoDeployWorkflow;
	}

	/**
	 * Sets the use auto deploy workflow.
	 * 
	 * @param pAutoDeployWorkflowFlag
	 *            - AutoDeployWorkflowFlag
	 */
	public void setUseAutoDeployWorkflow(boolean pAutoDeployWorkflowFlag){
		mUseAutoDeployWorkflow=pAutoDeployWorkflowFlag;
		setConfigValue(FeedConstants.USEAUTODEPLOY_WORKFLOW,pAutoDeployWorkflowFlag);	
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the mLogger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the new logger
	 */
	public void setLogger(FeedLogger pLogger) {
		this.mLogger = pLogger;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tru.feedprocessor.tol.IFeedConfig#getConfigValue(java.lang.String)
	 */
	@Override
	public Object getConfigValue(String pName) {
		return mHashMap.get(pName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tru.feedprocessor.tol.IFeedConfig#setConfigValue(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public void setConfigValue(String pName, Object pValue) {
		mHashMap.put(pName, pValue);

	}

	/**
	 * Gets the repository map.
	 * 
	 * @return the mRepositoryMap
	 */
	protected ServiceMap getRepositoryMap() {
		return mRepositoryMap;
	}

	/**
	 * Sets the repository map.
	 * 
	 * @param pRepositoryMap
	 *            the new repository map
	 */
	public void setRepositoryMap(ServiceMap pRepositoryMap) {
		mRepositoryMap = pRepositoryMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tru.feedprocessor.tol.IFeedConfig#getRepository(java.lang.String)
	 */
	@Override
	public MutableRepository getRepository(String pRepoName) {
		return (MutableRepository) getRepositoryMap().get(pRepoName);
	}

	/**
	 * Gets the file type.
	 * 
	 * @return the fileType
	 */
	public String getFileType() {
		return mFileType;
	}
	
	/**
	 * Sets the file type.
	 * 
	 * @param pFileType
	 *            the fileType to set
	 */
	public void setFileType(String pFileType) {
		mFileType=pFileType;
		setConfigValue(FeedConstants.FILETYPE, pFileType);
	}

	/**
	 * Gets the file name.
	 * 
	 * @return the fileName
	 */
	public String getFileName() {
		return mFileName;
	}

	/**
	 * Sets the file name.
	 * 
	 * @param pFileName
	 *            the new file name
	 */
	public void setFileName(String pFileName) {
		mFileName=pFileName;
		setConfigValue(FeedConstants.FILENAME, pFileName);
	}

	/**
	 * Gets the file path.
	 * 
	 * @return the filePath
	 */
	public String getFilePath() {
		return mFilePath;
	}
	/**
	 * Sets the file path.
	 * 
	 * @param pFilePath
	 *            the new file path
	 */
	public void setFilePath(String pFilePath) {
		mFilePath=pFilePath;
		setConfigValue(FeedConstants.FILEPATH, pFilePath);
	}
	
	/**
	 * Gets the entity to item descriptor name map.
	 * 
	 * @return the entityToItemDescriptorNameMap
	 */
	public Map<String, String> getEntityToItemDescriptorNameMap() {
		return mEntityToItemDescriptorNameMap;
	}
	/**
	 * Sets the entity sql.
	 *
	 * @param pMap the map
	 */
	public void setEntityToItemDescriptorNameMap(Map<String, String> pMap) {
		mEntityToItemDescriptorNameMap=pMap;
		setConfigValue(FeedConstants.ENTITYTOITEMDESCRIPTONNAMEMAP, pMap);
	}

	/**
	 * Gets the data loader class fqcn.
	 * 
	 * @return the feedEntityProviderFQCN
	 */
	public String getDataLoaderClassFQCN() {
		return mDataLoaderClassFQCN;
	}
	/**
	 * Sets the cat feed entry provider pFileParserClassFQCN.
	 * 
	 * @param pDataLoaderClassFQCN 
	 *            the new cat feed entry provider pDataLoaderClassFQCN
	 */
	public void setDataLoaderClassFQCN(String pDataLoaderClassFQCN) {
		mDataLoaderClassFQCN=pDataLoaderClassFQCN;
		setConfigValue(FeedConstants.DATA_LOADER_CLASS_FQCN,
				pDataLoaderClassFQCN);
	}

	/**
	 * Gets the entity sql.
	 * 
	 * @return the entitySQL
	 */
	public Map<String, String> getEntitySQL() {
		return mEntitySQL;
	}
	/**
	 * Sets the entity sql.
	 * 
	 * @param pEntitySQL
	 *            the entity sql
	 */
	public void setEntitySQL(Map<String, String> pEntitySQL) {
		mEntitySQL=pEntitySQL;
		setConfigValue(FeedConstants.ENTITY_SQL, pEntitySQL);
	}

	/**
	 * Gets the entity list.
	 * 
	 * @return the entityList
	 */
	public List<String> getEntityList() {
		return mEntityList;
	}

	/**
	 * Sets the entity list.
	 * 
	 * @param pEntityList
	 *            the new entity list
	 */
	public void setEntityList(List<String> pEntityList) {
		mEntityList=pEntityList;
		setConfigValue(FeedConstants.ENTITY_LIST, pEntityList);
	}

	/**
	 * Gets the auto workflow name.
	 * 
	 * @return the autoWorkflowName
	 */
	public String getAutoWorkflowName() {
		return mAutoWorkflowName;
	}

	
	/**
	 * Sets the auto workflow name.
	 * 
	 * @param pAutoWorkFlowName
	 *            - AutoWorkFlowName
	 */
	public void setAutoWorkflowName(String pAutoWorkFlowName){
		mAutoWorkflowName=pAutoWorkFlowName;
		setConfigValue(FeedConstants.AUTODEPLOY_WORKFLOW, pAutoWorkFlowName);
	}
	
	/**
	 * Gets the manual workflow name.
	 * 
	 * @return the manualWorkflowName
	 */
	public String getManualWorkflowName() {
		return mManualWorkflowName;
	}
	
	/**
	 * Sets the manual workflow name.
	 * 
	 * @param pManualWorkflowName
	 *            - ManualWorkflowName
	 */
	public void setManualWorkflowName(String pManualWorkflowName) {
		mManualWorkflowName=pManualWorkflowName;
			setConfigValue(FeedConstants.MANUAL_WORKFLOW, pManualWorkflowName);
	}
	
	
	/**
	 * Gets the step entity list.
	 * 
	 * @return the stepEntityList
	 */
	public Map<String, String> getStepEntityList() {
		return mStepEntityList;
	}

	/**
	 * Sets the step entity list.
	 * 
	 * @param pStepEntityList
	 *            the new step entity list
	 */
	public void setStepEntityList(Map<String,String> pStepEntityList) {
		mStepEntityList=pStepEntityList;
		setConfigValue(FeedConstants.STEP_ENTITY_LIST, pStepEntityList);
	}
	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	 public abstract void afterPropertiesSet()
		        throws Exception;

	/**
	 * Gets the file downloader flag.
	 * 
	 * @return the mFileDownloaderFlag
	 */
	public String getFileDownloaderFlag() {
		return mFileDownloaderFlag;
	}

	/**
	 * Sets the file downloader flag.
	 * 
	 * @param pFileDownloaderFlag
	 *            the mFileDownloaderFlag to set
	 */
	public void setFileDownloaderFlag(String pFileDownloaderFlag) {
		this.mFileDownloaderFlag = pFileDownloaderFlag;
		setConfigValue(FeedConstants.FILE_DOWNLOADER_FLAG, pFileDownloaderFlag);
	}


	/**
	 * Gets the host name.
	 * 
	 * @return - HostName
	 */
	public String getHostName() {
		return mHostName;
	}

	/**
	 * Sets the host name.
	 * 
	 * @param pHostName
	 *            - pHostName
	 */
	public void setHostName(String pHostName) {
		this.mHostName = pHostName;
		setConfigValue(FeedConstants.FTP_SFTP_HOST, pHostName);
	}
	
	/**
	 * Gets the host port.
	 * 
	 * @return - HostPort
	 */
	public String getHostPort() {
		return mHostPort;
	}

	
	/**
	 * Sets the host port.
	 * 
	 * @param pHostPort
	 *            - pHostPort
	 */
	public void setHostPort(String pHostPort) {
		this.mHostPort = pHostPort;
		setConfigValue(FeedConstants.FTP_SFTP_PORT, pHostPort);
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the mUserName
	 */
	public String getUserName() {
		return mUserName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param pUserName
	 *            the pUserName to set
	 */
	public void setUserName(String pUserName) {
		this.mUserName = pUserName;
		setConfigValue(FeedConstants.FPT_SFTP_USER, pUserName);
	}

	/**
	 * Gets the password.
	 * 
	 * @return the mPassword
	 */
	public String getPassword() {
		return mPassword;
	}

	/**
	 * Sets the password.
	 * 
	 * @param pPassword
	 *            the pPassword to set
	 */
	public void setPassword(String pPassword) {
		this.mPassword = pPassword;
		setConfigValue(FeedConstants.FPT_SFTP_PASSWORD, pPassword);
	}

	/**
	 * Gets the source directory.
	 * 
	 * @return the mSourceDirectory
	 */
	public String getSourceDirectory() {
		return mSourceDirectory;
	}

	/**
	 * Sets the source directory.
	 * 
	 * @param pSourceDirectory
	 *            the pSourceDirectory to set
	 */
	public void setSourceDirectory(String pSourceDirectory) {
		this.mSourceDirectory = pSourceDirectory;
		setConfigValue(FeedConstants.FPT_SFTP_SOURCE_DIR, pSourceDirectory);
	}

	/**
	 * Gets the protocol type.
	 * 
	 * @return the mProtocolType
	 */
	public String getProtocolType() {
		return mProtocolType;
	}

	/**
	 * Sets the protocol type.
	 * 
	 * @param pProtocolType
	 *            the mProtocolType to set
	 */
	public void setProtocolType(String pProtocolType) {
		this.mProtocolType = pProtocolType;
		setConfigValue(FeedConstants.PROTOCOL_TYPE, pProtocolType);
	}

	/**
	 * Gets the download file type.
	 * 
	 * @return the mDownloadFileType
	 */
	public String getDownloadFileType() {
		return mDownloadFileType;
	}

	
	/**
	 * Sets the download file type.
	 * 
	 * @param pDownloadFileType
	 *            - pDownloadFileType
	 */
	public void setDownloadFileType(String pDownloadFileType) {
		this.mDownloadFileType = pDownloadFileType;
		setConfigValue(FeedConstants.DOWNLOAD_FILETYPE, pDownloadFileType);
	}

	/**
	 * Gets the download file name.
	 * 
	 * @return the mDownloadFileName
	 */
	public String getDownloadFileName() {
		return mDownloadFileName;
	}

	/**
	 * Sets the download file name.
	 * 
	 * @param pDownloadFileName
	 *            the pDownloadFileName to set
	 */
	public void setDownloadFileName(String pDownloadFileName) {
		this.mDownloadFileName = pDownloadFileName;
		setConfigValue(FeedConstants.DOWNLOAD_FILENAME, pDownloadFileName);
	}

	/**
	 * Gets the xml root element.
	 * 
	 * @return the xmlRootElement
	 */
	public String getXmlRootElement() {
		return mXmlRootElement;
	}
	
	/**
	 * Sets the xml root element.
	 * 
	 * @param pRootElement
	 *            - RootElement
	 */
	public void setXmlRootElement(String pRootElement) {
		mXmlRootElement=pRootElement;
		setConfigValue(FeedConstants.ROOT_ELEMENT, mXmlRootElement);
	}
	
	/**
	 * Gets the versioned.
	 * 
	 * @return the mVersioned
	 */
	public boolean isVersioned() {
		return mVersioned;
	}
	/**
	 * Sets the versioned.
	 *
	 * @param pVersioned the new versioned
	 */
	public void setVersioned(boolean pVersioned){
		mVersioned=pVersioned;
		setConfigValue(FeedConstants.VERSIONED, pVersioned);
	}

	/**
	 * Gets the file uploader flag.
	 * 
	 * @return the mFileUploaderFlag
	 */
	public String getFileUploaderFlag() {
		return mFileUploaderFlag;
	}

	/**
	 * Sets the file uploader flag.
	 * 
	 * @param pFileUploaderFlag
	 *            the pFileUploaderFlag to set
	 */
	public void setFileUploaderFlag(String pFileUploaderFlag) {
		this.mFileUploaderFlag = pFileUploaderFlag;
		setConfigValue(FeedConstants.FILE_UPLOADER_FLAG, pFileUploaderFlag);
	}

	/**
	 * Gets the upload file type.
	 * 
	 * @return the mUploadFileType
	 */
	public String getUploadFileType() {
		return mUploadFileType;
	}

	/**
	 * Sets the upload file type.
	 * 
	 * @param pUploadFileType
	 *            the pUploadFileType to set
	 */
	public void setUploadFileType(String pUploadFileType) {
		this.mUploadFileType = pUploadFileType;
		setConfigValue(FeedConstants.UPLOAD_FILETYPE, pUploadFileType);
	}

	/**
	 * Gets the upload file name.
	 * 
	 * @return the mUploadFileName
	 */
	public String getUploadFileName() {
		return mUploadFileName;
	}

	/**
	 * Sets the upload file name.
	 * 
	 * @param pUploadFileName
	 *            the pUploadFileName to set
	 */
	public void setUploadFileName(String pUploadFileName) {
		this.mUploadFileName = pUploadFileName;
		setConfigValue(FeedConstants.UPLOAD_FILENAME, pUploadFileName);
	}
	
	/**
     * Log error message.
     * 
      * @param pMessage
     *            the message
     * @param pException
     *            the exception
     */
     public void logError(Object pMessage, Throwable pException) {
            getLogger().logErrorMessage(pMessage, pException);
            
     }

     

     /**
     * @return true/false
     */
     public boolean isLoggingError()
       {
         return getLogger().isLoggingError();
       }

}
