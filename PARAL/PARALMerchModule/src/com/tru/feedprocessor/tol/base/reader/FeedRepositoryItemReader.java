package com.tru.feedprocessor.tol.base.reader;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.nucleus.ServiceMap;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedVOMapper;
import com.tru.feedprocessor.tol.base.mapper.IFeedVOMapper;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Class FeedRepositoryItemReader. This is the common class which is used
 * to retreve the unique id and repository id from the corresponding data
 * source. Using teh EntiryResultExtractor it will store the data into feed
 * context.
 *
 * @version 1.1
 * @author Professional Access
 */
public class FeedRepositoryItemReader extends AbstractFeedItemReader{
	
	/**
	 * Property to hold mEntitylist.
	 */
	private List<String> mEntitylist=null;
	
	/**
	 * Property to hold mCurrentEntityIndex.
	 */
	private int mCurrentEntityIndex;
	
	/**
	 * Property to hold mCurrentItemIndex.
	 */
	private int mCurrentItemIndex;
	
	/**
	 * Property to hold mCurrentEntityName.
	 */
	private String mCurrentEntityName;
	
	/**
	 * Property to hold mRepositoryItems.
	 */
	private RepositoryItem[] mRepositoryItems = null;
	

	/**
	 * Property to hold mMapper.
	 */
	private ServiceMap mMapperMap;

	/**
	 * Gets the mapper map.
	 * 
	 * @return the mapper
	 */
	public ServiceMap getMapperMap() {
		return mMapperMap;
	}

	/**
	 * Sets the mapper map.
	 * 
	 * @param pMapperMap
	 *            the mapper to set
	 */
	public void setMapperMap(ServiceMap pMapperMap) {
		mMapperMap = pMapperMap;
	}
	
	/**
	 * Gets the query.
	 * 
	 * @param pEntity
	 *            - Entity
	 * @return RQLQuery
	 */
	@SuppressWarnings("unchecked")
	private String getQuery(String pEntity) {
		 Map<String,String> entityToRqlMap=(Map<String, String>) getConfigValue(FeedConstants.ENTITYTORQLMAP);
		return entityToRqlMap.get(pEntity);
	}
	
	/**
	 * Gets the vO for entity.
	 * 
	 * @param pVOForEntity
	 *            - VOForEntity
	 * @return - BaseFeedProcessVO
	 * @throws FeedSkippableException
	 *             - FeedSkippableException
	 */
	@SuppressWarnings("unchecked")
	protected BaseFeedProcessVO getVOForEntity(String pVOForEntity) throws FeedSkippableException {
		Class<?> FCQN = null;
		BaseFeedProcessVO lBaseVO = null;
		try {
			Map<String,String> temp =(Map<String, String>) getConfigValue(FeedConstants.ENTITY_VO_MAP);
			FCQN = Class.forName(temp.get(pVOForEntity));
			try {
				lBaseVO=(BaseFeedProcessVO) FCQN.newInstance();
			} catch (InstantiationException e) {
				throw new FeedSkippableException(getPhaseName(), getStepName(),
						e.getMessage(),lBaseVO,e);
			} catch (IllegalAccessException e) {
				throw new FeedSkippableException(getPhaseName(), getStepName(),
						e.getMessage(),lBaseVO,e);
			}
			
		} catch (ClassNotFoundException e) {
			throw new FeedSkippableException(getPhaseName(), getStepName(),
					e.getMessage(),lBaseVO,e);
		}
		return lBaseVO;
		
	}
	
	/**
	 * Process next entity.
	 * 
	 * @return - processNextRecord
	 * @throws FeedSkippableException
	 *             - FeedSkippableException
	 * @throws RepositoryException
	 *             - RepositoryException	             
	 */
	private BaseFeedProcessVO processNextEntity() throws FeedSkippableException,RepositoryException {
		 if(mCurrentEntityIndex == FeedConstants.MINUS_ONE){
			 ++mCurrentEntityIndex;
			 mCurrentEntityName=mEntitylist.get(mCurrentEntityIndex);
			 mRepositoryItems=retrieveData(mCurrentEntityName);
		 }
		 else if(mCurrentEntityIndex < mEntitylist.size() && mCurrentItemIndex==mRepositoryItems.length ){
			 mCurrentEntityName=mEntitylist.get(mCurrentEntityIndex);
			 mRepositoryItems=retrieveData(mCurrentEntityName);
			 mCurrentItemIndex=0;
		 }
		 else if(mCurrentEntityIndex==mEntitylist.size()){
			 return null;
		 }
		 return processNextRecord();
	}

	/**
	 * Process next record.
	 * 
	 * @return BaseFeedProcessVO
	 * @throws RepositoryException - RepositoryException
	 * @throws FeedSkippableException
	 *             - FeedSkippableException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private BaseFeedProcessVO processNextRecord() throws FeedSkippableException, RepositoryException {
		BaseFeedProcessVO lCurrentVO=null;
		IFeedVOMapper lBvFeedItemMapper=null;
		RepositoryItem lRepositoryItem=null;
		if(mRepositoryItems!=null && mCurrentItemIndex < mRepositoryItems.length && mCurrentEntityIndex<mEntitylist.size()){
			lBvFeedItemMapper = (IFeedVOMapper) getMapperMap().get(mEntitylist.get(mCurrentEntityIndex));
			lCurrentVO = (BaseFeedProcessVO) getVOForEntity(mEntitylist.get(mCurrentEntityIndex));
			lRepositoryItem = mRepositoryItems[mCurrentItemIndex];
			lCurrentVO = (BaseFeedProcessVO) lBvFeedItemMapper.map(lCurrentVO,lRepositoryItem);
			++mCurrentItemIndex;
			if(mCurrentItemIndex==mRepositoryItems.length){
				 ++mCurrentEntityIndex;
			}
			return lCurrentVO;
		}
		return null;
	}
	
	/**
	 * Retrieve data.
	 * 
	 * @param pEntityName
	 *            - EntityName
	 * @return repositoryItems
	 * @throws FeedSkippableException
	 *             - FeedSkippableException
	 */
	protected RepositoryItem[] retrieveData(String pEntityName) throws FeedSkippableException {
		RepositoryItem[] items = null;
		RepositoryView repoView;
		QueryBuilder queryBuilder = null;
		Query rqlQuery = null;
		try {
			
			String lRepositoryName = getEntityRepositoryName(pEntityName);
			String lItemDiscriptorName = getItemDescriptorName(pEntityName);
			
			repoView = getRepository(lRepositoryName).getView(lItemDiscriptorName.trim());
			queryBuilder = repoView.getQueryBuilder();
			RqlStatement rqlStatement = null;
			if (StringUtils.isBlank(getQuery(pEntityName))) {
				rqlQuery = queryBuilder.createUnconstrainedQuery();
			} else {
				rqlStatement = RqlStatement.parseRqlStatement(getQuery(pEntityName),true);
				rqlQuery = rqlStatement.getQuery().getQuery(queryBuilder,repoView, null);
				
			}
			items = repoView.executeQuery(rqlQuery);
		} catch (RepositoryException re) {
			String errMessage = re.getMessage();
			throw new FeedSkippableException(getPhaseName(),  getStepName(),errMessage,null,re);
		}

		return items;
	}


	/** @see com.tru.feedprocessor.tol.base.reader.AbstractFeedItemReader#doOpen()
	 */
	@Override
	public synchronized void doOpen() {
		mCurrentEntityIndex=FeedConstants.NUMBER_MINUS_ONE;
		mCurrentItemIndex=FeedConstants.NUMBER_ZERO;
		mEntitylist = (List<String>) getEntityList();
		
		Collection values = getMapperMap().values();
		Iterator iterator = values.iterator();
		while (iterator.hasNext()) {
			AbstractFeedVOMapper itemMapper = (AbstractFeedVOMapper) iterator.next();
			itemMapper.setFeedHelper(getFeedHelper());
		}
	}

	/**
	 * Do read.
	 *
	 * @return the base feed process vo
	 * @throws FeedSkippableException the feed skippable exception
	 * @throws RepositoryException the repository exception
	 * @see com.tru.feedprocessor.tol.base.reader.AbstractFeedItemReader#doRead()
	 */
	@Override
	protected BaseFeedProcessVO doRead() throws FeedSkippableException, RepositoryException {
			return processNextEntity();
	}
	

	/** @see com.tru.feedprocessor.tol.base.reader.AbstractFeedItemReader#doClose()
	 */
	@Override
	public void doClose() {
		mEntitylist=null;
		mCurrentEntityName=null;
	}
}
