package com.tru.feedprocessor.tol.base.tasklet;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.ApplicationContextHolder;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;

/**
 * The Class DataExistsTask. This is the default implementation for Data exist
 * tasklet. It will check data from table  given in getQuery. If data 
 * exists then it will proceed to the decider. If there is no data exists then
 * it should throw the Exception.
 * 
 * @version 1.1
 * @author Professional Access
 */
public class DataExistsTask extends AbstractTasklet {

	/**
	 * Do execute.
	 *
	 * @throws FeedSkippableException the feed skippable exception
	 * @throws Exception the exception
	 * @see com.tru.feedprocessor.tol.base.tasklet.AbstractTasklet#doExecute()
	 */
	@Override
	protected void doExecute() throws FeedSkippableException, Exception {
		dataValidation();
	}
	
	/**
	 * Data validation.
	 * 
	 * @throws Exception
	 *             - Exception
	 */
	protected void dataValidation() throws Exception {
		
		JdbcTemplate lJdbcTemplate = null;
		List<Map<String, Object>> lQueryMapList= null;
		
 		DataSource lDataSource = (DataSource) ApplicationContextHolder.getBean(getDataSourceName());
 		
		if (lDataSource != null) {
			lJdbcTemplate = new JdbcTemplate(lDataSource);
			String lQuery = getQuery();
			if (lQuery != null) {
				lQueryMapList = lJdbcTemplate.queryForList(lQuery);
			}
			if(lQueryMapList == null || lQueryMapList.isEmpty()){
				throw new Exception(FeedConstants.LOAD_DATA_ERROR);
			}
		}
		
	}
	
	/**
	 * Gets the query.
	 * 
	 * @return Query
	 */
	protected String getQuery() {
		return null;
	}

	/**
	 * Gets the data source name.
	 * 
	 * @return dataSourcename
	 */
	protected String getDataSourceName() {
		return null;
	}
}
