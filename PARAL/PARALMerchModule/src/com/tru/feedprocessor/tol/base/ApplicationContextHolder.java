package com.tru.feedprocessor.tol.base;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * The Class ApplicationContextHolder 
 * 
 * <p>Service class for application context holder </p>
 *
 * @version 1.0
 * @author Professional Access
 */
public class ApplicationContextHolder implements ApplicationContextAware {
	
	/** The m application context. */
	private static ApplicationContext mApplicationContext=null;
	
	/**
	 * Gets the application context.
	 *
	 * @return the application context
	 */
	public	static ApplicationContext getApplicationContext() {
		return mApplicationContext;
	}
	
	/**
	 * Sets the application context.
	 *
	 * @param pApplicationContext the new application context
	 * @throws BeansException the beans exception
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(ApplicationContext pApplicationContext)
			throws BeansException {
		
		this.mApplicationContext=pApplicationContext;
	}
	
	/**
	 * Gets the bean.
	 *
	 * @param pBeanName the bean name
	 * @return the bean
	 */
	public static Object getBean(String pBeanName){
		return getApplicationContext().getBean(pBeanName);
	}

	
}
