package com.tru.feedprocessor.tol.base.appenders;

/**
 * The Interface IAppender.
 *
 * @version 1.0
 * @author Professional Access
 */
public interface IAppender {

	/**
	 * Append.
	 *
	 * @param pArg the arg
	 */
void append(String pArg);
	
/**
 * AppendErrorMessage.
 *
 * @param pArg the arg
 */
void appendErrorMessage(String pArg);

	
}
