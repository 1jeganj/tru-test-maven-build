package com.tru.feedprocessor.tol.base.vo;

import java.util.Date;
import java.util.List;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;

/**
 * The Class StepInfoVO.
 *
 * @version 1.1
 * @author Professional Access
 */
public class StepInfoVO {
	
/** The m phase name. */
private String mPhaseName;

/** The m step name. */
private String mStepName;

/** The m start time. */
private Date mStartTime;

/** The m end time. */
private Date mEndTime;

/** The m exceptions. */
private List<? extends FeedSkippableException> mSkippedDataExceptions;

private Throwable mFeedProcessException = null;

/** The m read count. */
private int mReadCount;

/** The m skip count. */
private int mSkipCount;

/** The m write count. */
private int mWriteCount;

/** The m write skip count. */
private int mWriteSkipCount;

/** The m commit count. */
private int mCommitCount;

/** The m read skip count. */
private int mReadSkipCount;

/** The m rollback count. */
private int mRollbackCount;

/** The m ProcessSkipCount count. */
private int mProcessSkipCount;
/**
 * Gets the write skip count.
 *
 * @return the mWriteSkipCount
 */
public int getWriteSkipCount() {
	return mWriteSkipCount;
}

/**
 * Sets the write skip count.
 *
 * @param pMWriteSkipCount the mWriteSkipCount to set
 */
public void setWriteSkipCount(int pMWriteSkipCount) {
	mWriteSkipCount = pMWriteSkipCount;
}

/**
 * Gets the commit count.
 *
 * @return the mCommitCount
 */
public int getCommitCount() {
	return mCommitCount;
}

/**
 * Sets the commit count.
 *
 * @param pCommitCount the new commit count
 */
public void setCommitCount(int pCommitCount) {
	mCommitCount = pCommitCount;
}

/**
 * Gets the read skip count.
 *
 * @return the mReadSkipCount
 */
public int getReadSkipCount() {
	return mReadSkipCount;
}

/**
 * Sets the read skip count.
 *
 * @param pReadSkipCount the new read skip count
 */
public void setReadSkipCount(int pReadSkipCount) {
	mReadSkipCount = pReadSkipCount;
}

/**
 * Gets the rollback count.
 *
 * @return the mRollbackCount
 */
public int getRollbackCount() {
	return mRollbackCount;
}

/**
 * Sets the rollback count.
 *
 * @param pRollbackCount the new rollback count
 */
public void setRollbackCount(int pRollbackCount) {
	mRollbackCount = pRollbackCount;
}

/**
 * Gets the phase name.
 *
 * @return the mPhaseName
 */
public String getPhaseName() {
	return mPhaseName;
}

/**
 * Sets the phase name.
 *
 * @param pPhaseName the new phase name
 */
public void setPhaseName(String pPhaseName) {
	mPhaseName = pPhaseName;
}

/**
 * Gets the step name.
 *
 * @return the mStepName
 */
public String getStepName() {
	return mStepName;
}

/**
 * Sets the step name.
 *
 * @param pStepName the new step name
 */
public void setStepName(String pStepName) {
	mStepName = pStepName;
}

/**
 * Gets the start time.
 *
 * @return the mStartTime
 */
public Date getStartTime() {
	return mStartTime;
}

/**
 * Sets the start time.
 *
 * @param pStartTime the new start time
 */
public void setStartTime(Date pStartTime) {
	mStartTime = pStartTime;
}

/**
 * Gets the end time.
 *
 * @return the mEndTime
 */
public Date getEndTime() {
	return mEndTime;
}

/**
 * Sets the end time.
 *
 * @param pEndTime the new end time
 */
public void setEndTime(Date pEndTime) {
	mEndTime = pEndTime;
}

/**
 * Gets the exceptions.
 *
 * @return the mExceptions
 */
public List<? extends FeedSkippableException> getFeedSkippedDataExceptions() {
	return mSkippedDataExceptions;
}

/**
 * Sets the exceptions.
 *
 * @param pList the mExceptions to set
 */
public void setFeedSkippedDataExceptions(List<? extends FeedSkippableException> pList) {
	mSkippedDataExceptions = pList;
}

/**
 * @param pProcessException - pProcessException
 */
public void setFeedFailureProcessException(Throwable pProcessException)
{
	mFeedProcessException = pProcessException;	
}

/**
 * @return - FeedFailureProcessException
 */
public Throwable getFeedFailureProcessException()
{
	return mFeedProcessException;
}

/**
 * Gets the read count.
 *
 * @return the mReadCount
 */
public int getReadCount() {
	return mReadCount;
}

/**
 * Sets the read count.
 *
 * @param pReadCount the new read count
 */
public void setReadCount(int pReadCount) {
	mReadCount = pReadCount;
}

/**
 * Gets the skip count.
 *
 * @return the mSkipCount
 */
public int getSkipCount() {
	return mSkipCount;
}

/**
 * Sets the skip count.
 *
 * @param pSkipCount the new skip count
 */
public void setSkipCount(int pSkipCount) {
	mSkipCount = pSkipCount;
}

/**
 * Gets the write count.
 *
 * @return the mWriteCount
 */
public int getWriteCount() {
	return mWriteCount;
}

/**
 * Sets the write count.
 *
 * @param pWriteCount the new write count
 */
public void setWriteCount(int pWriteCount) {
	mWriteCount = pWriteCount;
}

/**
 * @return the mProcessSkipCount
 */
public int getProcessSkipCount() {
	return mProcessSkipCount;
}

/**
 * @param pProcessSkipCount - pProcessSkipCount
 */
public void setProcessSkipCount(int pProcessSkipCount) {
	this.mProcessSkipCount = pProcessSkipCount;
}

}