package com.tru.feedprocessor.tol.base.writer;

import org.springframework.batch.item.ItemStreamException;

import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Interface IFeedItemWriterDelegate.
 * 
 * @version 1.1
 * @author Professional Access
 */
public interface IFeedItemWriterDelegate {
	
	/**
	 * Do open.
	 * 
	 * @throws ItemStreamException
	 *             - ItemStreamException
	 */
	 void doOpen() throws ItemStreamException;
	 
 	/**
	 * Do write.
	 * 
	 * @param pFeedVO
	 *            - pFeedVO
	 * @throws FeedSkippableException
	 *             - FeedSkippableException
	 * @throws RepositoryException
	 *             the repository exception
	 */
	void doWrite(BaseFeedProcessVO pFeedVO) throws FeedSkippableException, RepositoryException;
	 
 	/**
	 * Do close.
	 * 
	 * @throws ItemStreamException
	 *             - ItemStreamException
	 */
	void  doClose() throws ItemStreamException;
	 
}
