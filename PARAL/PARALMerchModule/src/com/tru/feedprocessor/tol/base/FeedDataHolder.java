package com.tru.feedprocessor.tol.base;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class FeedDataHolder. This is the class which will store any data which is
 * created during feed execution. We need to clear the data once the job
 * execution is completed, preferably in post process step.
 * 
 * @version 1.0
 * @author Professional Access
 */
public class FeedDataHolder {

	
	/** The m data map. */
	private Map<String, Object> mDataMap = new HashMap<String, Object>();


	/**
	 * Gets the data from context.
	 * 
	 * @param pKey
	 *            the key
	 * @return the data
	 */
	public Object getData(String pKey) {
		return mDataMap.get(pKey);
	}

	/**
	 * Sets the data in to the context.
	 * 
	 * @param pKey
	 *            the key
	 * @param pValue
	 *            the value
	 */
	public void setData(String pKey, Object pValue) {
		mDataMap.put(pKey, pValue);
		
	}
	
}
