package com.tru.feedprocessor.tol.base.mapper;

import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;

/**
 * The Interface IFeedVOMapper.
 *
 * @version 1.1
 * @author Professional Access
 * @param <T> the generic type
 * 
 */
public interface IFeedVOMapper<T> {

	/**
	 * Map.
	 *
	 * @param pVOItem the vO item
	 * @param pRepoItem the repo item
	 * @return the T
	 * @throws FeedSkippableException - FeedSkippableException
	 * @throws RepositoryException the RepositoryException
	 */
	
	T map(T pVOItem, RepositoryItem pRepoItem)
			throws FeedSkippableException, RepositoryException;
	
	/*T map(RepositoryItem pLRepoItems)
	throws FeedSkippableException;*/
	//Class<?> map(Class<?> pLCurrentItem);
}
