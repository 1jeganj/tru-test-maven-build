package com.tru.feedprocessor.tol.base.tasklet.support;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.tru.feedprocessor.tol.FeedConstants;


/**
 * The Class IdResultSetExtractor.
 * 
 * The  IdResultSetExtractor return for key and value for the ID column into a map 
 *
 * @version 1.1
 * @author Professional Access
 */
public class IdResultSetExtractor implements ResultSetExtractor<Object> {


	/**
	 * Extract data.
	 *
	 * @param pResultSet the result set
	 * @return the object
	 * @throws SQLException the SQL exception
	 * @throws DataAccessException the data access exception
	 * @see org.springframework.jdbc.core.ResultSetExtractor#extractData(java.sql.ResultSet)
	 */
	@Override
	public Object extractData(ResultSet pResultSet) throws SQLException,
			DataAccessException {
		HashMap<String,String> lHashMap=null;
		String lId=null;
		String lRepositoryId=null;
		
		if(pResultSet!=null){	
			
			lHashMap=new HashMap<String,String>();
			pResultSet.next();
			
			int lCount=pResultSet.getMetaData().getColumnCount();
			int size = pResultSet.getRow();
			if(size >0  )
			{
				do{
					switch(lCount)
					{
					case FeedConstants.NUMBER_ONE:
						lId=pResultSet.getString(FeedConstants.NUMBER_ONE);
						lRepositoryId=lId;
						lHashMap.put(lId,lRepositoryId);
						break;
					case FeedConstants.NUMBER_TWO:
						lId=pResultSet.getString(FeedConstants.NUMBER_ONE);
						lRepositoryId=pResultSet.getString(FeedConstants.NUMBER_TWO);
						lHashMap.put(lId,lRepositoryId);
					}
				}while(pResultSet.next());
			}
			
	}
		return lHashMap;
	}

}
