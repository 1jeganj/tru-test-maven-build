package com.tru.feedprocessor.tol.base.processor.support;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;


/**
 * The Interface IFeedItemValidator.
 *
 * @version 1.1
 * @author Professional Access
 */
public interface IFeedItemValidator {
	
	/**
	 * Validate.
	 *
	 * @param pFeedVO            - pFeedVO
	 * @throws FeedSkippableException the feed skippable exception
	 */
	void validate(BaseFeedProcessVO pFeedVO) throws FeedSkippableException;
}
