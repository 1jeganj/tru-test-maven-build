package com.tru.feedprocessor.tol.base.decider;


import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;

import com.tru.feedprocessor.tol.FeedConstants;


/**
 * The Class HasMoreContextFeedDecider.
 *
 * @version 1.1
 * @author Professional Access
 */
public class HasMoreContextFeedDecider extends AbstractFeedDecider {

	/**
	 * Do decide.
	 *
	 * @param pJobexecution the jobexecution
	 * @param pStepexecution the stepexecution
	 * @return the flow execution status
	 * @see com.tru.feedprocessor.tol.base.decider.AbstractFeedDecider#doDecide(org.springframework.batch.core.JobExecution, org.springframework.batch.core.StepExecution)
	 */
	@Override
	public FlowExecutionStatus doDecide(JobExecution pJobexecution,
			StepExecution pStepexecution) {
		
		boolean hasMoreCntxFlag = (Boolean) retriveData(FeedConstants.HAS_MORE_CONTEXT);
		
		FlowExecutionStatus flowExecStatus = (hasMoreCntxFlag)? new FlowExecutionStatus(Boolean.toString(FeedConstants.TRUE).toUpperCase()) : new FlowExecutionStatus(Boolean.toString(FeedConstants.FALSE).toUpperCase());
		
		return flowExecStatus;
	}
}
