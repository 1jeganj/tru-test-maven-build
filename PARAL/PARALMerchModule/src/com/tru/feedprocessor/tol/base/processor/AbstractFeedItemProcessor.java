package com.tru.feedprocessor.tol.base.processor;

import java.util.List;

import org.springframework.batch.core.scope.context.StepSynchronizationManager;
import org.springframework.batch.item.ItemProcessor;

import atg.repository.MutableRepository;
import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.FeedHelper;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class AbstractFeedItemProcessor.
 * 
 * The default implementation item processor 
 * 
 * @version 1.1
 * @author Professional Access
 * 
 */
public abstract class AbstractFeedItemProcessor implements ItemProcessor<BaseFeedProcessVO,BaseFeedProcessVO> {
	
	/** The mLogger. */
	private FeedLogger mLogger;
	
	/** The mPhase name. */
	private String mPhaseName;

	/** The mStepName name. */
	private String mStepName;
	
	/** The m feed helper. */
	private FeedHelper mFeedHelper;
	
	/**
	 * Gets the feed helper.
	 * 
	 * @return FeedHelper
	 */
	public FeedHelper getFeedHelper() {
		return mFeedHelper;
	}

	/**
	 * Sets the feed helper.
	 * 
	 * @param pFeedHelper
	 *            - pFeedHelper
	 */
	public void setFeedHelper(FeedHelper pFeedHelper) {
		mFeedHelper = pFeedHelper;
	}
	
	/**
	 * Gets the logger.
	 * 
	 * @return the mLogger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the mLogger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		this.mLogger = pLogger;
	}

	/**
	 * Gets the phase name.
	 * 
	 * @return the mPhaseName
	 */
	public String getPhaseName() {
		return mPhaseName;
	}

	/**
	 * Gets the step name.
	 * 
	 * @return the mStepName
	 */
	public String getStepName() {
		return mStepName;
	}

	/**
	 * Process the Feed.
	 * 
	 * This method call doProcess() to process the feed data.
	 * The mStepName and mPhaseName will get from the Spring context and assign
	 *  
	 * @param pFeedV0 - pFeedV0
	 * @return the BaseFeedProcessVO
	 * @throws FeedSkippableException - FeedSkippableException
	 * @throws RepositoryException - RepositoryException
	 * 
	 */
	public BaseFeedProcessVO process(BaseFeedProcessVO pFeedV0) throws FeedSkippableException, RepositoryException
	{
		mStepName=StepSynchronizationManager.getContext().getStepName();
		mPhaseName=(String)(StepSynchronizationManager.getContext().getStepExecution().getExecutionContext().get(FeedConstants.PHASE_NAME));
		try{
			return doProcess(pFeedV0);
		}catch (FeedSkippableException fse) {
			String errMessaage=fse.getMessage();
			throw new FeedSkippableException(getPhaseName(), getStepName(), errMessaage + 
				FeedConstants.COMA_SEPERATOR + FeedConstants.SPACE + FeedConstants.AT + FeedConstants.SPACE + this.getClass().getSimpleName(), pFeedV0,fse);
		}catch (RepositoryException re) {
			String errMessaage=re.getMessage();
			throw new FeedSkippableException(getPhaseName(), getStepName(),errMessaage + 
			FeedConstants.COMA_SEPERATOR + FeedConstants.SPACE + FeedConstants.AT + FeedConstants.SPACE + this.getClass().getSimpleName(), pFeedV0,re);
		}
	}
	
	/**
	 * Do Process.
	 * 
	 * The actual process of the feed need to customize by the team by overriding
	 * 
	 * @param pFeedVO - pFeedVO
	 * @return BaseFeedProcessVO or null
	 * @throws FeedSkippableException - FeedSkippableException
	 * @throws RepositoryException - RepositoryException
	 * 
	 */
	protected abstract BaseFeedProcessVO doProcess(BaseFeedProcessVO pFeedVO) throws FeedSkippableException, RepositoryException;
	
	/**
	 * Gets the current feed execution context.
	 * 
	 * @return - CurrentFeedExecutionContext
	 */
	public FeedExecutionContextVO getCurrentFeedExecutionContext() {
		return getFeedHelper().getCurrentFeedExecutionContext();
	}
	/**
	 * Retrive data.
	 * 
	 * @param pDataMapKey
	 *            the data map key
	 * @return the object
	 */
	public Object retriveData(String pDataMapKey) {
		return getFeedHelper().retriveData(pDataMapKey);
	}
	/**
	 * Save data.
	 * 
	 * @param pConfigKey
	 *            the pConfigKey
	 * @param pConfigValue
	 *            the config value
	 */
	public void saveData(String pConfigKey, Object pConfigValue) {
		 getFeedHelper().saveData(pConfigKey, pConfigValue);
	}
	
	/**
	 * Gets the entity list.
	 * 
	 * @return the entity list
	 */

	public List<String> getEntityList() {
		return getFeedHelper().getStepEntityList(getStepName());
	}
	
	/**
	 * Gets the config value.
	 * 
	 * @param pConfigKey
	 *            the pConfigKey
	 * @return value
	 */
	public Object getConfigValue(String pConfigKey) {
		return getFeedHelper().getConfigValue(pConfigKey);
	}
	
	/**
	 * Sets the config value.
	 * 
	 * @param pName
	 *            - pName
	 * @param pValue
	 *            - pValue
	 */
	public void setConfigValue(String pName, Object pValue) {
		getFeedHelper().setConfigValue(pName, pValue);
	}

	/**
	 * Gets the repository.
	 * 
	 * @param pRepoName
	 *            the repositoryName
	 * @return repository
	 */
	public MutableRepository getRepository(String pRepoName) {
		return getFeedHelper().getRepository(pRepoName);
	}
	
	/**
	 * Gets the item descriptor name.
	 * 
	 * @param pEntityName
	 *            - pEntityName
	 * @return item descriptor name
	 */
	public String getItemDescriptorName(String pEntityName){
		return getFeedHelper().getEntityToItemDescriptorNameMap(pEntityName);
		
	}
	
	/**
	 * Gets the entity repository name.
	 * 
	 * @param pEntityName
	 *            - pEntityName
	 * @return repository name
	 */
	public String getEntityRepositoryName(String pEntityName){
		return getFeedHelper().getEntityToRepositoryNameMap(pEntityName);
	}
	
}
