package com.tru.feedprocessor.tol.base.processor.support;

import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Interface IItemValueChangeInspector.
 *
 * @version 1.1
 * @author Professional Access
 */
public interface IItemValueChangeInspector {
	
	/**
	 * Checks if is updated.
	 * 
	 * @param pFeedVO
	 *            - pFeedVO
	 * @param pRepoItem
	 *            - pRepoItem
	 * @return boolean value
	 * @throws FeedSkippableException
	 *             - FeedSkippableException
	 */
	boolean isUpdated(BaseFeedProcessVO pFeedVO,RepositoryItem pRepoItem) throws FeedSkippableException;
	
}
