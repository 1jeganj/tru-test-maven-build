package com.tru.feedprocessor.tol.base.tasklet;

import com.tru.feedprocessor.tol.base.IPostProcessorDelegate;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;


/**
 * The Class PostProcessTask. This class is used to the implement the task
 * involved in post processing phase of feed job. Project team can override the
 * doExecute() method to implement their project specific functionality. In case
 * of exception they need to throw the FeedProcess Exception.
 * 
 * @version 1.1
 * @author Professional Access
 */
public class PostProcessTask extends AbstractTasklet {

	/**
	 * This property holds mPostProcessTask
	 */
	private IPostProcessorDelegate[] mPostProcessTask ;
	

	/**
	 * @return the mPostProcessTask
	 */
	public IPostProcessorDelegate[] getPostProcessTask() {
		return mPostProcessTask;
	}


	/**
	 * @param pPostProcessTask - pPostProcessTask
	 */
	public void setPostProcessTask(IPostProcessorDelegate[] pPostProcessTask) {
		mPostProcessTask = pPostProcessTask;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tru.feedprocessor.tol.AbstractTasklet#doExecute()
	 */
	@Override
	protected void doExecute() throws FeedSkippableException {
		
		FeedExecutionContextVO lFeedExecutionContextVO = getCurrentFeedExecutionContext();
		IPostProcessorDelegate[] lPostProcessList = getPostProcessTask();
			
		if( null!=lPostProcessList && lPostProcessList.length > 0){
			for( IPostProcessorDelegate postProcessDeligate:lPostProcessList){
				postProcessDeligate.postProcess(lFeedExecutionContextVO);
		 	}	
		}
	}


}
