package com.tru.feedprocessor.tol.base.tasklet;

import java.util.List;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.appenders.IAppender;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.formatter.IFormatter;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class NotifyTask.
 * 
 * @version 1.1
 * @author Professional Access
 */
public class NotifyTask extends AbstractTasklet {
	
	/** The Formatter. */
	IFormatter mFormatter;
	
	/** The m mapper map. */
	private IAppender[] mAppenders = null;
	
	/**
	 * Gets the formatter.
	 *
	 * @return the mFormatter
	 */
	public IFormatter getFormatter() {
		return mFormatter;
	}

	/**
	 * Sets the formatter.
	 *
	 * @param pFormatter the new formatter
	 */
	public void setFormatter(IFormatter pFormatter) {
		mFormatter = pFormatter;
	}
	
	/**
	 * Gets the appenders.
	 *
	 * @return the appenders
	 */
	protected IAppender[] getAppenders() {
		return mAppenders;
	}

	/**
	 * Sets the repository map.
	 * 
	 * @param pAppenders
	 *            the new appenders
	 */
	public void setAppenders(IAppender[] pAppenders) {
		mAppenders = pAppenders;
	}

	/**
	 * 
	 * @throws FeedSkippableException
	 *             - FeedSkippableException
	 */
	protected void doNotify() throws FeedSkippableException {

		List<FeedExecutionContextVO> processedExecutionContextList = getProcessedFeedExecutionContext();
		
		String lFormattedString = getFormatter().format(processedExecutionContextList);
		getLogger().logInfo(lFormattedString);
		for (IAppender appender : getAppenders()) {
			appender.append(lFormattedString);
		}
		cleanupSaveData();
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.AbstractTasklet#doExecute()
	 */
	@Override
	protected void doExecute() throws FeedSkippableException {
		doNotify();
	}
	
	/**
	 *  cleaning saved Data's
	 */
	protected void cleanupSaveData(){
		saveData(FeedConstants.FEEDCONTEXT_QUEUE, null);
		saveData(FeedConstants.CURRENT_FEEDCONTEXT,null);
		saveData(FeedConstants.PROCESSED_CONTEXTS,null);
		saveData(FeedConstants.HAS_MORE_CONTEXT,null);
		saveData(FeedConstants.ENTITY_PROVIDER,null);
	}
	
}
