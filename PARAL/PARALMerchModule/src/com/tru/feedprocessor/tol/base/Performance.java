package com.tru.feedprocessor.tol.base;

import com.tru.feedprocessor.tol.FeedConstants;


/**
 * The Class Performance 
 *
 * <p> Provide a single tone object to test the performance of a method or a class execution </p>
 *  
 * @version 1.0
 * @author Professional Access
 */
public class Performance {

/** The . */
Performance mPerformance;

/** The Start time. */
public  long mStartTime;


/** The End time. */
public  long mEndTime;
	
	/**
	 * Gets the performance.
	 *
	 * @return the performance
	 */
	public Performance getPerformance()
	{
		if(mPerformance==null)
		{
			return new Performance();
		}
		return mPerformance;
	}
	
	/**
	 * Gets the start time.
	 *
	 * @return the startTime
	 */
	public long getStartTime() {
		return mStartTime;
	}
	
	/**
	 * Sets the start time.
	 *
	 * @param pStartTime the startTime to set
	 */
	public void setStartTime(long pStartTime) {
		mStartTime = pStartTime;
	}
	
	/**
	 * Gets the end time.
	 *
	 * @return the endTime
	 */
	public long getEndTime() {
		return mEndTime;
	}
	
	/**
	 * Sets the end time.
	 *
	 * @param pEndTime the endTime to set
	 */
	public void setEndTime(long pEndTime) {
		mEndTime = pEndTime;
	}
	
	/**
	 * Gets the time.
	 *
	 * @return the time
	 */
	public long getTime()
	{
		
		return mEndTime-mStartTime;
		
	}
	
	/**
	 * Gets the time min.
	 *
	 * @return the time min
	 */
	public double getTimeMin()
	{
		return (mEndTime-mStartTime)/FeedConstants.NUMBER_SIXTYTHOUSANDS;
	}
}
