package com.tru.feedprocessor.tol.base.tasklet;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;


/**
 * The Class DefaultPreProcessTask. This class is used to the implement the task
 * involved in pre processing phase of feed job such as taking back up of the
 * file, etc. Project team can override the doExecute() method to implement
 * their project specific task. In case of exception they need to throw the
 * FeedProcess Exception.
 * 
 * @version 1.1
 * @author Professional Access
 */
public class PreProcessTask extends AbstractTasklet {

	/**
	 *  
	 *
	 * @throws FeedSkippableException the feed skippable exception
	 * @see com.tru.feedprocessor.tol.AbstractTasklet#doExecute() This is the
	 * blank method where the project team has to put the code which will be
	 * executedduring pre processing task
	 */
	@Override
	protected void doExecute() throws FeedSkippableException {
		// Actual code to implement pre processing task
		
	}

}
