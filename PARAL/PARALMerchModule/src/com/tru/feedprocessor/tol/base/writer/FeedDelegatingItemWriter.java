package com.tru.feedprocessor.tol.base.writer;

import java.util.List;

import org.springframework.batch.item.ItemStreamException;

import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Class FeedDelegatingItemWriter.
 * 
 * The FeedDelegatingItemWriter for the BV Hour and Store
 *   
 * @version 1.1
 * @author Professional Access
 */
public class FeedDelegatingItemWriter extends AbstractFeedItemWriter{
	
	/**
	 * The writer delegate.
	 */
	private IFeedItemWriterDelegate[] mWriterDelegate;
	
	/**
	 * Gets the writer delegate.
	 * 
	 * @return the writerDelegate
	 */
	public IFeedItemWriterDelegate[] getWriterDelegate() {
		return mWriterDelegate;
	}
	
	/**
	 * Sets the writer delegate.
	 * 
	 * @param pWriterDelegate
	 *            the writerDelegate to set
	 */
	public void setWriterDelegate(IFeedItemWriterDelegate[] pWriterDelegate) {
		mWriterDelegate = pWriterDelegate;
	}
	
	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.base.writer.AbstractFeedItemWriter#doOpen()
	 */
	@Override
	public void doOpen() throws ItemStreamException {
		for(IFeedItemWriterDelegate writerDelegate:getWriterDelegate()){
			AbstractFeedItemWriterDelegate deligatingwriter=(AbstractFeedItemWriterDelegate) writerDelegate;
			deligatingwriter.setFeedHelper(getFeedHelper());
			writerDelegate.doOpen();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.base.writer.AbstractFeedItemWriter#doWrite(java.util.List)
	 */
	@Override
	public void doWrite(List<? extends BaseFeedProcessVO> pFeedVOs)
			throws RepositoryException, FeedSkippableException {
		for(BaseFeedProcessVO feedVO: pFeedVOs ){
			for(IFeedItemWriterDelegate writerDelegate:getWriterDelegate()){
				writerDelegate.doWrite(feedVO);
			}
			
		}
		
	}
	
	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.base.writer.AbstractFeedItemWriter#doClose()
	 */
	@Override
	public void doClose() throws ItemStreamException {
		for(IFeedItemWriterDelegate writerDelegate:getWriterDelegate()){
			writerDelegate.doClose();
		}
	}

}
