package com.tru.feedprocessor.tol.base.config;

import atg.repository.MutableRepository;

/**
 * The Interface IFeedConfig.
 * @author Professional Access
 * @version 1.0
 */
public interface IFeedConfig {

	/**
	 * Gets the config value.
	 * 
	 * @param pName
	 *            the name
	 * @return the config value
	 */
	Object getConfigValue(String pName);

	/**
	 * Sets the config value.
	 * 
	 * @param pName
	 *            the name
	 * @param pValue
	 *            the value
	 */
	void setConfigValue(String pName, Object pValue);

	/**
	 * Gets the repository.
	 * 
	 * @param pRepoName
	 *            the repo name
	 * @return the repository
	 */
	MutableRepository getRepository(String pRepoName);

}
