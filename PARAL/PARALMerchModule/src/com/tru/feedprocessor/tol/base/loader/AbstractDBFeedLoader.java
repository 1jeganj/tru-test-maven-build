package com.tru.feedprocessor.tol.base.loader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.ApplicationContextHolder;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;


/**
 * The Class AbstractDBFeedLoader.
 *  
 *  Team has to extend for  this class for parsing DB data and store into VO map 
 * 
 * The default implementation of loading DB data in to memory. The class store the parse date in to a VOHolderMap
 * The implemented class need to supply some parameter for corresponding corresponding VO class name and supported parameter 
 * through the component to the implemented class.
 * 
 * @version 1.1
 * @author Professional Access
 */
public abstract class AbstractDBFeedLoader implements IFeedDataLoader {

	/**
	 * the mVOHolderMap.
	 */
	private Map<String, List<? extends BaseFeedProcessVO>> mVOHolderMap;

	/**
	 * Do read.
	 * 
	 * @return lBaseFeedProcessVOList
	 * @throws Exception
	 *             - Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected  List<BaseFeedProcessVO> doRead() throws Exception{

		List<BaseFeedProcessVO> lBaseFeedProcessVOList = null;
 		JdbcTemplate lJdbcTemplate = null;
 		DataSource lDataSource = (DataSource) ApplicationContextHolder.getBean(getDataSourceName());
		
		if (lDataSource != null) {
			lJdbcTemplate = new JdbcTemplate(lDataSource);
			String lQuery = getQuery();
			Object[] params = getQueryParams();
			if (lQuery != null) {
				lBaseFeedProcessVOList = lJdbcTemplate. query(lQuery, params ,new BeanPropertyRowMapper(getRowMappingVOClass()));
			}
		}
		if(lBaseFeedProcessVOList == null || lBaseFeedProcessVOList.isEmpty()){
			throw new Exception(FeedConstants.LOAD_DATA_ERROR);
		}	
		return lBaseFeedProcessVOList;
	}

	/**
	 *  
	 *
	 * @param pCurExecCntx the cur exec cntx
	 * @return the map< string, list<? extends base feed process v o>>
	 * @throws Exception the exception
	 * @see com.tru.feedprocessor.tol.base.parse.IFeedDataLoader#loadData(java
	 * .io.InputStream)
	 */
	@Override
	public Map<String, List<? extends BaseFeedProcessVO>> loadData(
			FeedExecutionContextVO pCurExecCntx) throws Exception {

		mVOHolderMap = new HashMap<String, List<? extends BaseFeedProcessVO>>();
		List<? extends BaseFeedProcessVO> lEntityList = null;
		List<BaseFeedProcessVO> lBaseFeedProcessVOList = doRead();
		for (BaseFeedProcessVO lBaseFeedProcessVO : lBaseFeedProcessVOList) {
			lEntityList = processEntityMapVO(lBaseFeedProcessVO);
			addEntityToMap(lEntityList);
		}
		return mVOHolderMap;
	}

	/**
	 * Adds the entity to map.
	 * 
	 * @param pEntityList
	 *            - pEntityList
	 */
	@SuppressWarnings("unchecked")
	protected void addEntityToMap(List<? extends BaseFeedProcessVO> pEntityList) {

		for (BaseFeedProcessVO entityVO : pEntityList) {
			String entityName = entityVO.getEntityName();
			if (mVOHolderMap.containsKey(entityName)) {
				List<BaseFeedProcessVO> mappedEntityList = (List<BaseFeedProcessVO>) mVOHolderMap
						.get(entityName);
				mappedEntityList.add(entityVO);
				mVOHolderMap.put(entityName, mappedEntityList);
			} else {
				List<BaseFeedProcessVO> entities = new ArrayList<BaseFeedProcessVO>();
				entities.add(entityVO);
				mVOHolderMap.put(entityName, entities);
			}
		}
	}

	/**
	 * Process entity map vo.
	 * 
	 * @param pBaseFeedProcessVO
	 *            - pBaseFeedProcessVO
	 * @return mBaseFeedProcessVO
	 */
	public abstract List<? extends BaseFeedProcessVO> processEntityMapVO(
			BaseFeedProcessVO pBaseFeedProcessVO);

	/**
	 * Gets the row mapping vo class.
	 * 
	 * @return mClass
	 * @throws Exception
	 *             - Exception
	 */
	public abstract Class<?> getRowMappingVOClass() throws Exception;

	/**
	 * Gets the query.
	 * 
	 * @return pString
	 */
	protected abstract String getQuery();

	/**
	 * Gets the data source name.
	 * 
	 * @return mString
	 */
	protected abstract String getDataSourceName();

	/**
	 * Gets the query params.
	 * 
	 * @return mObject
	 */
	protected abstract Object[] getQueryParams();

}
