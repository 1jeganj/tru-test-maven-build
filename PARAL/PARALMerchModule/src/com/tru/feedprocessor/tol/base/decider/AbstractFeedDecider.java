package com.tru.feedprocessor.tol.base.decider;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;

import atg.repository.MutableRepository;

import com.tru.feedprocessor.tol.base.FeedHelper;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;


/**
 * The Class AbstractFeedDecider.
 * 
 * @version 1.1
 * @author Professional Access
 */
public abstract class AbstractFeedDecider implements JobExecutionDecider {

	/** The m phase name. */
	private String mPhaseName;

	/** The mLogger. */
	private FeedLogger mLogger;
	
	/** The m FeedHelper. */
	private FeedHelper mFeedHelper;

	/**
	 * Gets the feed data holder.
	 *
	 * @return the mFeedDataHolder
	 */
	public FeedHelper getFeedHelper() {
		return mFeedHelper;
	}

	/**
	 * Sets the feed helper.
	 *
	 * @param pFeedHelper the new feed helper
	 */
	public void setFeedHelper(FeedHelper pFeedHelper) {
		mFeedHelper = pFeedHelper;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the mLogger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the new logger
	 */
	public void setLogger(FeedLogger pLogger) {
		this.mLogger = pLogger;
	}

	/**
	 * Gets the phase name.
	 * 
	 * @return the phase name
	 */
	public String getPhaseName() {
		return mPhaseName;
	}

	/**
	 * CurrentFeedExecutionContext.
	 * 
	 * @return - CurrentFeedExecutionContext
	 */
	public FeedExecutionContextVO getCurrentFeedExecutionContext() {
		return getFeedHelper().getCurrentFeedExecutionContext();
	}
	/**
	 * Retrive data.
	 * 
	 * @param pDataMapKey
	 *            the data map key
	 * @return the object
	 */
	public Object retriveData(String pDataMapKey) {
		return getFeedHelper().retriveData(pDataMapKey);
	}
	/**
	 * Save data.
	 * 
	 * @param pCongifKey
	 *            the congif key
	 * @param pConfigValue
	 *            the config value
	 */
	public void saveData(String pCongifKey, Object pConfigValue) {
		 getFeedHelper().saveData(pCongifKey, pConfigValue);
	}
	
	/**
	 * Gets the config value.
	 * 
	 * @param pConfigKey
	 *            the pConfigKey
	 * @return value
	 */
	public Object getConfigValue(String pConfigKey) {
		return getFeedHelper().getConfigValue(pConfigKey);
	}
	
	/**
	 * Sets the config value.
	 * 
	 * @param pName
	 *            - pName
	 * @param pValue
	 *            - pValue
	 */
	public void setConfigValue(String pName, Object pValue) {
		getFeedHelper().setConfigValue(pName, pValue);
	}

	/**
	 * Gets the repository.
	 * 
	 * @param pRepoName
	 *            the repositoryName
	 * @return repository
	 */
	public MutableRepository getRepository(String pRepoName) {
		return getFeedHelper().getRepository(pRepoName);
	}
	
	/**
	 * Gets the item descriptor name.
	 * 
	 * @param pItemDescName
	 *            - pItemDescName
	 * @return item descriptor name
	 */
	public String getItemDescriptorName(String pItemDescName){
		return getFeedHelper().getEntityToItemDescriptorNameMap(pItemDescName);
		
	}
	
	/**
	 * Gets the entity repository name.
	 * 
	 * @param pRepositoryName
	 *            - pRepositoryName
	 * @return repository name
	 */
	public String getEntityRepositoryName(String pRepositoryName){
		return getFeedHelper().getEntityToRepositoryNameMap(pRepositoryName);
	}
	/**
	 * Flow execution status.
	 * 
	 * @param pJobexecution
	 *            the execution of the job
	 * @param pStepexecution
	 *            the step execution
	 * @return the flow execution status
	 */
	public FlowExecutionStatus decide(JobExecution pJobexecution,
			StepExecution pStepexecution){
		return doDecide(pJobexecution,pStepexecution);
	}
	
	/**
	 * Do decide.
	 * 
	 * @param pJobexecution
	 *            - pJobexecution
	 * @param pStepexecution
	 *            - pStepexecution
	 * @return FlowExecutionStatus
	 */
	protected abstract FlowExecutionStatus doDecide(JobExecution pJobexecution,
			StepExecution pStepexecution);

}
