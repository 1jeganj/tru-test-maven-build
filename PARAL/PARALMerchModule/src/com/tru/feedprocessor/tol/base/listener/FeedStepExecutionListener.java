package com.tru.feedprocessor.tol.base.listener;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;

/** 
 * The class FeedStepExecutionListener
 * The listener interface for receiving feedStepExecution events. The class that
 * is interested in processing a feedStepExecution event implements this
 * interface, and the object created with that class is registered with a
 * component using the component's
 * <code>addFeedStepExecutionListener</code> method. When
 * the feedStepExecution event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see FeedStepExecutionEvent
 *
 * @version 1.1
 * @author Professional Access
 */
public class FeedStepExecutionListener implements StepExecutionListener {

	/** The m mPhaseName. */
	private String mPhaseName;

	/** The mLogger. */
	private FeedLogger mLogger;
	
	/**
	 * Gets the logger.
	 * 
	 * @return the mLogger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	@Override
	public ExitStatus afterStep(StepExecution pStepExecution) {
		return ExitStatus.COMPLETED;
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	@Override
	public void beforeStep(StepExecution pStepExecution) {
		pStepExecution.getExecutionContext().putString(FeedConstants.PHASE_NAME,getPhaseName());
	}

	/**
	 * Gets the phase name.
	 * 
	 * @return the mPhaseName
	 */
	public String getPhaseName() {
		return mPhaseName;
	}

	/**
	 * Sets the phase name.
	 * 
	 * @param pPhaseName
	 *            - pPhaseName
	 */
	public void setPhaseName(String pPhaseName) {
		this.mPhaseName = pPhaseName;
	}

}
