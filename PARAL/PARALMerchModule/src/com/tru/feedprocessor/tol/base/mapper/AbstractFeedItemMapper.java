package com.tru.feedprocessor.tol.base.mapper;

import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.ApplicationContextHolder;
import com.tru.feedprocessor.tol.base.FeedHelper;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class AbstractFeedItemMapper.
 * 
 * @param <T>
 *            the generic type
 *
 * @version 1.1
 * @author Professional Access
 */
public abstract class AbstractFeedItemMapper<T> implements
		IFeedItemMapper<T> {

	/** The m feed data helper. */
	private FeedHelper mFeedHelper;

	/**
	 * Gets the feed data helper.
	 *
	 * @return the mFeedHelper
	 */
	public FeedHelper getFeedHelper() {
		if(null == mFeedHelper){
			return  (FeedHelper) ApplicationContextHolder.getBean(FeedConstants.FEED_HELPER);
		}
		return mFeedHelper;
	}

	/**
	 * Sets the data holder.
	 *
	 * @param pFeedHelper the new data holder
	 */
	public void setFeedHelper(FeedHelper pFeedHelper) {
		mFeedHelper = pFeedHelper;
	}

	/**
	 * 
	 * @return - CurrentFeedExecutionContext
	 */
	public FeedExecutionContextVO getCurrentFeedExecutionContext() {
		return (FeedExecutionContextVO) getFeedHelper().getCurrentFeedExecutionContext();
	}
	/**
	 * Retrive data.
	 * 
	 * @param pDataMapKey
	 *            the data map key
	 * @return the object
	 */
	public Object retriveData(String pDataMapKey) {
		return getFeedHelper().retriveData(pDataMapKey);
	}
	/**
	 * Save data.
	 * 
	 * @param pConfigKey
	 *            the pConfigKey
	 * @param pConfigValue
	 *            the config value
	 */
	public void saveData(String pConfigKey, Object pConfigValue) {
		 getFeedHelper().saveData(pConfigKey, pConfigValue);
	}
	/**
	 * 
	 * @param pConfigKey the pConfigKey
	 * @return value
	 */
	public Object getConfigValue(String pConfigKey) {
		return getFeedHelper().getConfigValue(pConfigKey);
	}
	/**
	 * 
	 * @param pName - pName
	 * @param pValue - pValue
	 */
	public void setConfigValue(String pName, Object pValue) {
		getFeedHelper().setConfigValue(pName, pValue);
	}

	/**
	 * 
	 * @param pRepoName the repositoryName
	 * @return repository
	 */
	public MutableRepository getRepository(String pRepoName) {
		return getFeedHelper().getRepository(pRepoName);
	}
	/**
	 * 
	 * @param pEntityName - pEntityName
	 * @return item descriptor name
	 */
	public String getItemDescriptorName(String pEntityName){
		return getFeedHelper().getEntityToItemDescriptorNameMap(pEntityName);
		
	}
	/**
	 * 
	 * @param pEntityName - pEntityName
	 * @return repository name
	 */
	public String getEntityRepositoryName(String pEntityName){
		return getFeedHelper().getEntityToRepositoryNameMap(pEntityName);
	}

	/** 
	 * 
	 * @see
	 * com.tru.feedprocessor.tol.catalogfeed.ICatFeedItemMapper#map(java.
	 * lang.Object, atg.repository.MutableRepositoryItem)
	 * @param pVOItem VOItem
	 * @param pRepoItem repoItem
	 * @return mutableRepositoryItem
	 * @throws RepositoryException RepositoryException
	 * @throws FeedSkippableException FeedSkippableException
	 */
	@Override
	public abstract MutableRepositoryItem map(T pVOItem,
			MutableRepositoryItem pRepoItem) throws RepositoryException,FeedSkippableException;

}
