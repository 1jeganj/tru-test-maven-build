package com.tru.feedprocessor.tol.base.decider;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;

import com.tru.feedprocessor.tol.FeedConstants;

/**
 * The Class VersionDecider.
 *
 * @version 1.1
 * @author Professional Access
 */
public class VersionDecider extends AbstractFeedDecider {

	/**
	 * Do decide.
	 *
	 * @param pJobexecution the jobexecution
	 * @param pStepexecution the stepexecution
	 * @return the flow execution status
	 * @see org.springframework.batch.core.job.flow.JobExecutionDecider#decide(org
	 * .springframework.batch.core.JobExecution,
	 * org.springframework.batch.core.StepExecution)
	 */
	@Override
	public FlowExecutionStatus doDecide(JobExecution pJobexecution,
			StepExecution pStepexecution) {
		boolean versionType = ((Boolean) getConfigValue(FeedConstants.VERSIONED));
		return ((versionType) ? new FlowExecutionStatus(FeedConstants.VERSIONED) : new FlowExecutionStatus(FeedConstants.NONVERSIONED));
	}

}
