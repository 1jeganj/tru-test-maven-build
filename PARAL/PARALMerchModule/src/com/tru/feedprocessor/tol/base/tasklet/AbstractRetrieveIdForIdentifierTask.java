package com.tru.feedprocessor.tol.base.tasklet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.tru.feedprocessor.tol.base.ApplicationContextHolder;
import com.tru.feedprocessor.tol.base.tasklet.support.IdResultSetExtractor;

/**
 * The Class AbstractRetrieveIdForIdentifierTask.
 * 
 * This is the common class which is used
 * to retreve the unique id and repository id from the corresponding data
 * source. Using teh EntiryResultExtractor it will store the data into feed
 * context.
 * 
 *
 * @version 1.1
 * @author Professional Access
 */
public abstract class AbstractRetrieveIdForIdentifierTask extends AbstractTasklet {

	/** The m entity result set extractor. */
	private IdResultSetExtractor mIdResultSetExtractor = new IdResultSetExtractor();
	/** The m data source name. */
	private String mDataSourceName;
	
	/** The m identifier id store Key. */
	private String mIdentifierIdStoreKey;

	/**
	 * Gets the identifier id store key.
	 * 
	 * @return the identifierIdStoreKey
	 */
	public String getIdentifierIdStoreKey() {
		return mIdentifierIdStoreKey;
	}

	/**
	 * Sets the identifier id store key.
	 * 
	 * @param pIdentifierIdStoreKey
	 *            the identifierIdStoreKey to set
	 */
	public void setIdentifierIdStoreKey(String pIdentifierIdStoreKey) {
		mIdentifierIdStoreKey = pIdentifierIdStoreKey;
	}

	/**
	 * Gets the data source name.
	 * 
	 * @return the mDataSource
	 */
	public String getDataSourceName() {
		return mDataSourceName;
	}

	/**
	 * Sets the data source name.
	 * 
	 * @param pDataSourceName
	 *            the new data source name
	 */
	public void setDataSourceName(String pDataSourceName) {
		mDataSourceName = pDataSourceName;
	}

	/**
	 * Gets the entity result set extractor.
	 * 
	 * @return the entity result set extractor
	 */
	public IdResultSetExtractor getIdResultSetExtractor() {
		return mIdResultSetExtractor;
	}

	/**
	 * Sets the entity result set extractor.
	 * 
	 * @param pIdResultSetExtractor
	 *            the new Id result set extractor
	 */
	public void setIdResultSetExtractor(
		IdResultSetExtractor pIdResultSetExtractor) {
		mIdResultSetExtractor = pIdResultSetExtractor;
	}

	/**
	 * Store entity ids.
	 * 
	 * @param pEntityIdMap
	 *            the entity id map
	 */
	protected void storeIds(Map<String, Map<String, String>> pEntityIdMap) {
		saveData(getIdentifierIdStoreKey(), pEntityIdMap);
	}

	/**
	 * Retrive entity ids. This method will get the list of entities and their
	 * corresponding sql's from feed configuration. Then it will execute all
	 * those queries using spring batch's jdbc template. Result will be store in
	 * feed data holder
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@SuppressWarnings("unchecked")
	protected void retriveIdsForIdentifiers() throws Exception {

		JdbcTemplate lJdbcTemplate = null;
		DataSource lDataSource = (DataSource) ApplicationContextHolder.getBean(getDataSourceName());
		
		if (lDataSource != null) {
			Map<String, Map<String, String>> lIdHolderMap = new HashMap<String, Map<String, String>>();
			List<String> lIdentifierList = getIdentifiers();
			Map<String, String> lIdResultMap = null;
			if (lIdentifierList != null) {
				lJdbcTemplate = new JdbcTemplate(lDataSource);
				String lQuery = null;
				for (String lIdentifier : lIdentifierList) {
					lQuery = getQuery(lIdentifier);
					if (lQuery != null) {
						lIdResultMap = (HashMap<String, String>) lJdbcTemplate.query(lQuery, getQueryParams(lIdentifier),
											getIdResultSetExtractor());
						lIdHolderMap.put(lIdentifier, lIdResultMap);
					}
				}
				storeIds(lIdHolderMap);
			}
		}

	}
	
	/**
	 * Gets the identifiers.
	 * 
	 * @return identifiers
	 */
	protected abstract List<String> getIdentifiers();
	
	/**
	 * Gets the query.
	 * 
	 * @param pCurrentIdentifier
	 *            the CurrentIdentifier
	 * @return query
	 */
	protected abstract String getQuery(String pCurrentIdentifier); 

	/**
	 *  
	 *
	 * @throws Exception the exception
	 * @see com.tru.feedprocessor.tol.AbstractTasklet#doExecute() project
	 * team can override this method to implement their own way of getting the
	 * uniqueid and repository id from the data base.
	 */
	@Override
	protected void doExecute() throws Exception {
		retriveIdsForIdentifiers();
	}
	
	/**
	 * Gets the query params.
	 * 
	 * @param pCurrentIdentifier
	 *            - currentIdentifier
	 * @return query params
	 */
	protected Object[] getQueryParams(String pCurrentIdentifier) {
		return new Object[] {};
	}
}
