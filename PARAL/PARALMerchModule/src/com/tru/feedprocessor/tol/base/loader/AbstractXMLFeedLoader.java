package com.tru.feedprocessor.tol.base.loader;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class AbstractXMLFeedLoader.
 * 
 * Team has to extend for  this class for parsing XML file and store the data into EntityVOMap 
 * 
 * The default implementation of loading and parsing XML file in to memory. The class store the parse date in to a EntityVOMap
 * The implemented class need to supply some parameter for corresponding corresponding VO class name and supported parameter 
 * through the component to the implemented class.
 * 
 * @version 1.1
 * @author Professional Access
 * 
 */
public abstract class AbstractXMLFeedLoader implements IFeedDataLoader{


	/**
	 * Load data.
	 *
	 * @param pCurExecCntx the cur exec cntx
	 * @return the map< string, list<? extends base feed process v o>>
	 * @throws Exception the exception
	 * @see com.tru.feedprocessor.tol.base.parse.IFeedDataParser#parseData(java.io.InputStream)
	 */
	@Override
	public Map<String, List<? extends BaseFeedProcessVO>> loadData(FeedExecutionContextVO pCurExecCntx) throws Exception {
		InputStream lFileInputStream = null;
		Map<String, List<? extends BaseFeedProcessVO>> lEntityVOMap = null;
		try{
			lFileInputStream = new FileInputStream(pCurExecCntx.getFilePath()+pCurExecCntx.getFileName());
		
			JAXBContext jaxbContext = null;
			jaxbContext = JAXBContext.newInstance(getRootMappingVOClass());

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			
			Object obj = jaxbUnmarshaller.unmarshal(lFileInputStream);

			lEntityVOMap = processEntityMapVO(obj);
			
		}finally{
			if(lFileInputStream != null){
				lFileInputStream.close();
			}
		}
		return lEntityVOMap;
	}
	
	
	/**
	 * Gets the root mapping vo class.
	 * 
	 * @return mClass
	 * @throws Exception
	 *             - Exception
	 */
	public abstract Class<?> getRootMappingVOClass() throws Exception;

	/**
	 * Process entity map vo.
	 * 
	 * @param pObj
	 *            - pObj
	 * @return mBaseFeedProcessVO
	 * @throws Exception
	 *             - Exception
	 */
	public abstract Map<String, List<? extends BaseFeedProcessVO>> processEntityMapVO(Object pObj) throws Exception; 
}
