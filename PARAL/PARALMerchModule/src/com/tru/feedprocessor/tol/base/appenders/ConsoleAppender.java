package com.tru.feedprocessor.tol.base.appenders;

import com.tru.feedprocessor.tol.base.logger.FeedLogger;



/**
 * The Class ConsoleAppender 
 * 
 * Appender for console message through the FeedLogger  
 *  
 * @version 1.0
 * @author Professional Access
 */
public class ConsoleAppender implements IAppender {
	
	/** The Feed logger. */
	FeedLogger mFeedLogger;
	
	
	/**
	 * Gets the feed logger.
	 *
	 * @return the feed logger
	 */
	public FeedLogger getFeedLogger() {
		return mFeedLogger;
	}

	
	/**
	 * Sets the feed logger.
	 *
	 * @param pFeedLogger the new feed logger
	 */
	public void setFeedLogger(FeedLogger pFeedLogger) {
		mFeedLogger = pFeedLogger;
	}


	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.base.appenders.IAppender#append(java.lang.String)
	 */
	@Override
	public void append(String pArg) {
		getFeedLogger().logDebugMessage(pArg);
		
	}

	
	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.base.appenders.IAppender#appendErrorMessage(java.lang.String)
	 */
	@Override
	public void appendErrorMessage(String pArg) {
		getFeedLogger().logError(pArg);
		
	}

}
