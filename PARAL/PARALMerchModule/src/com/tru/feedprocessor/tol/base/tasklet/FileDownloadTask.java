package com.tru.feedprocessor.tol.base.tasklet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;

/**
 * The Class FileDownloadTask.
 * 
 * @version 1.1
 * @author Professional Access
 */
public class FileDownloadTask extends AbstractTasklet {
	
	/** The Host . */
	private String mHost;
	/** The port. */
	private int mPort;
	/** The User. */
	private String mUser;
	/** The Password. */
	private String mPass;
	/** The SourceDirectory. */
	private String mSourceDir;
	/** The destination Directory. */
	private String mDestinationDir;
	/** The ProtocolType. */
	private String mProtocolType;
	/** The FileCount. */
	private int mFileCount = 0;
	/** The FileType. */
	private String mFileType;
	/** The ZipFile. */
	private String mZipFile;

	/**
	 * Do execute.
	 *
	 * @throws FeedSkippableException the feed skippable exception
	 * @throws Exception the exception
	 * @see com.tru.feedprocessor.tol.AbstractTasklet#doExecute()
	 */
	@Override
	protected void doExecute() throws FeedSkippableException,Exception {

		String lFileDownloaderFlag = nullIfEmpty(getConfigValue(
				FeedConstants.FILE_DOWNLOADER_FLAG).toString());

		if (lFileDownloaderFlag != null && lFileDownloaderFlag.equals(Boolean.toString(FeedConstants.TRUE))) {

			String lStrPort = nullIfEmpty(getConfigValue(
					FeedConstants.FTP_SFTP_PORT).toString());
			mHost = nullIfEmpty(getConfigValue(FeedConstants.FTP_SFTP_HOST)
					.toString());
			mPort = Integer.parseInt((lStrPort == null) ? FeedConstants.STRING_ZERO : lStrPort);
			mUser = nullIfEmpty(getConfigValue(FeedConstants.FPT_SFTP_USER)
					.toString());
			mPass = nullIfEmpty(getConfigValue(FeedConstants.FPT_SFTP_PASSWORD)
					.toString());
			mSourceDir = nullIfEmpty(getConfigValue(
					FeedConstants.FPT_SFTP_SOURCE_DIR).toString());
			mDestinationDir = nullIfEmpty(getConfigValue(FeedConstants.FILEPATH)
					.toString());
			mProtocolType = nullIfEmpty(getConfigValue(FeedConstants.PROTOCOL_TYPE)
					.toString());

			if (null != mHost && mPort != 0 && null != mUser && null != mPass
					&& null != mSourceDir && null != mDestinationDir
					&& null != mProtocolType) {
				if (FeedConstants.SFTP_PROTOCOL.equals(mProtocolType)) {
					sftpFileDownloder();
				} else if (FeedConstants.FTP_PROTOCOL.equals(mProtocolType)) {
					ftpFileDownloader();
				}

				if (FeedConstants.ZIP_TYPE.equals(mFileType) && mFileCount != 0) {
					unZipFile(mZipFile, mDestinationDir);
				}

			} else {
				throw new Exception(FeedConstants.FILE_DOWNLOAD_FAILED);
			}
		} else{
			getLogger().logDebugMessage(FeedConstants.SKIP_FILE_DOWNLOADING);
		}
	}

	/**
	 * sftpFileDownloder
	 */
	private void sftpFileDownloder() {

		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;

		try {
			JSch jsch = new JSch();
			session = jsch.getSession(mUser, mHost, mPort);
			session.setPassword(mPass);

			session.setConfig(FeedConstants.STRICT_HOST_KEY_CHECKING, FeedConstants.NO_OPTION);
			session.connect();

			channel = session.openChannel(FeedConstants.SFTP_PROTOCOL.toLowerCase());
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(mSourceDir);

			mFileType = getConfigValue(FeedConstants.DOWNLOAD_FILETYPE)
					.toString();

			String filePattern = (nullIfEmpty(getConfigValue(
					FeedConstants.DOWNLOAD_FILENAME).toString()) == null ? FeedConstants.STAR
					: getConfigValue(FeedConstants.DOWNLOAD_FILENAME)
							.toString())
					+ FeedConstants.DOT + mFileType;

			List<ChannelSftp.LsEntry> filesToDownload = channelSftp.ls(filePattern);
			if (filesToDownload.isEmpty()) {
				getLogger().logInfo(
						FeedConstants.SPACE
								+ FeedConstants.NO_FILES_TO_DOWNLOAD);
			} else {
				for (ChannelSftp.LsEntry entry : filesToDownload) {

					channelSftp.get(entry.getFilename(), mDestinationDir
							+ entry.getFilename());
					mZipFile = entry.getFilename();
					mFileCount++;
					/*
					 * getLogger().logInfo( "DOWNLOAD_SUCCESS_MSG " +
					 * entry.getFilename());
					 */
				}
				getLogger().logInfo(
						mFileCount + FeedConstants.SPACE
								+ FeedConstants.DOWNLOAD_SUCCESS);
			}

		} catch (JSchException ex) {
			if (isLoggingError()) {
				logError(FeedConstants.FTP_REFUSED_CONNECTION + FeedConstants.IFUN
						+ ex.getMessage(), ex);
			}
		} catch (SftpException ex) {
			if (isLoggingError()) {
				logError(FeedConstants.FILE_DOWNLOAD_FAILED + FeedConstants.IFUN + ex.getMessage(),
					ex);
			}
		} catch (Exception ex) {
			if (isLoggingError()) {
				logError(FeedConstants.ERROR + FeedConstants.IFUN + ex.getMessage(), ex);	
			}
		} finally {
			if (channelSftp.isConnected()) {
				try {
					session.disconnect();
					channel.disconnect();
					channelSftp.quit();
					// getLogger().logInfo("FTP_DISCONNECT");
				} catch (Exception ioe) {
					if (isLoggingError()) {
						logError(FeedConstants.NO_FILES_TO_DOWNLOAD + FeedConstants.IFUN
								+ ioe.getMessage(), ioe);	
					}
				}
			}
		}

	}

	/**
	 * ftpFileDownloader
	 */
	public void ftpFileDownloader() {
		FTPClient client = new FTPClient();
		FileOutputStream fos = null;

		try {

			client.connect(mHost, mPort);
			boolean result = client.login(mUser, mPass);

			if (!result){
				getLogger().logInfoMessage(FeedConstants.CONNECTION_FAILED);
			}

			// Get the files stored on FTP Server and store them into an array
			// of FTPFiles
			FTPFile[] lFiles = client.listFiles(mSourceDir);

			for (FTPFile ftpFile : lFiles) {
				// Check the file type and print result
				if (ftpFile.getType() == FTPFile.FILE_TYPE) {

					String lDownloadFileName = ftpFile.getName();

					mFileType = getConfigValue(FeedConstants.DOWNLOAD_FILETYPE)
							.toString();

					String filePattern = FeedConstants.REGX_PATTERN
							+ (nullIfEmpty(getConfigValue(
									FeedConstants.DOWNLOAD_FILENAME)
									.toString()) == null ? FeedConstants.STAR
									: getConfigValue(
											FeedConstants.DOWNLOAD_FILENAME)
											.toString()) + FeedConstants.DOT
							+ mFileType;

					Pattern pattern = Pattern.compile(filePattern);
					Matcher matcher = pattern.matcher(mSourceDir
							+ lDownloadFileName);

					if (matcher.find()) {
						fos = new FileOutputStream(mDestinationDir
								+ lDownloadFileName);
						client.retrieveFile(lDownloadFileName, fos);
						mFileCount++;
						/*
						 * getLogger().logInfoMessage( "File: " + ftpFile.getName() +
						 * "size-> " + FileUtils .byteCountToDisplaySize(ftpFile
						 * .getSize()));
						 */
					}
				}
				getLogger().logInfoMessage(
						mFileCount + FeedConstants.SPACE
								+ FeedConstants.DOWNLOAD_SUCCESS);
			}
			client.logout();
		} catch (IOException ex) {
			if (isLoggingError()) {
				logError(FeedConstants.ERROR + FeedConstants.IFUN
							+ ex.getMessage(), ex);
			}
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
				client.disconnect();
			} catch (IOException ex) {
				if (isLoggingError()) {
					logError(FeedConstants.ERROR + FeedConstants.IFUN
							+ ex.getMessage(), ex);	
				}
			}

		}
	}

	/**
	 * @param pString - pString
	 * @return nullIfEmpty
	 */
	public String nullIfEmpty(String pString) {
		if (org.springframework.util.StringUtils.isEmpty(pString)
				|| pString == null) {
			return null;
		}
		return pString;
	}
	
	/**
	 * @param pZipFile - pZipFile
	 * @param pOutputFolder - pOutputFolder
	 */
	private void unZipFile(String pZipFile, String pOutputFolder) {

		byte[] buffer = new byte[FeedConstants.BUFFER_SIZE_BYTE];

		try {

			// create output directory is not exists
			File folder = new File(mDestinationDir);
			if (!folder.exists()) {
				folder.mkdir();
			}

			// get the zip file content
			ZipInputStream zis = new ZipInputStream(
					new FileInputStream(mDestinationDir+pZipFile));
			// get the zipped file list entry
			ZipEntry ze = zis.getNextEntry();

			while (ze != null) {

				String fileName = ze.getName();
				File newFile = new File(pOutputFolder + fileName);

				getLogger().logInfoMessage(FeedConstants.FILE_UNZIP + FeedConstants.SPACE + FeedConstants.COLON + FeedConstants.SPACE + newFile.getAbsoluteFile());

				// create all non exists folders
				// else you will hit FileNotFoundException for compressed folder
				new File(newFile.getParent()).mkdirs();

				FileOutputStream fos = new FileOutputStream(newFile);

				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}

				fos.close();
				ze = zis.getNextEntry();
			}

			zis.closeEntry();
			zis.close();

			//getLogger().logInfo("Done");

		} catch (IOException ex) {
			if(isLoggingError()){
				logError(FeedConstants.ERROR + FeedConstants.IFUN
							+ ex.getMessage(), ex);
			}
		}
	}
}
