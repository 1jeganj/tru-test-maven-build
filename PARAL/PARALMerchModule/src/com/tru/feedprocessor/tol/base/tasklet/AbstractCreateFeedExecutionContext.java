package com.tru.feedprocessor.tol.base.tasklet;

import java.util.Queue;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class AbstractCreateFeedExecutionContext.
 *   
 * Creating Queue for process Context
 * 
 * @version 1.1
 * @author Professional Access
 */
public abstract class AbstractCreateFeedExecutionContext extends AbstractTasklet {
	
	/**
	 * Do execute.
	 * 
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 * @throws Exception
	 *             the exception
	 */
	@Override
	protected void doExecute() throws FeedSkippableException,Exception {
		Queue<FeedExecutionContextVO> executionContextQueue = createExecutionContext();
		if(executionContextQueue==null)
		{
			throw new Exception(FeedConstants.NO_CONTEXT_CREATED);
		}
		saveData(FeedConstants.FEEDCONTEXT_QUEUE, executionContextQueue);		
	}
	
	/**
	 * Creates the execution context.
	 * 
	 * @return - excecutionContext
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 * @throws Exception
	 *             the exception
	 */
	protected abstract Queue<FeedExecutionContextVO> createExecutionContext() throws FeedSkippableException, Exception;
	
}