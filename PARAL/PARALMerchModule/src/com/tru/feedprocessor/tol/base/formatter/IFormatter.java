package com.tru.feedprocessor.tol.base.formatter;

import java.util.List;

import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;


/**
 * The Interface IFormatter.
 *
 * @version 1.1
 * @author Professional Access
 */
public interface IFormatter {
 
 /**
	 * Format.
	 * 
	 * @param pContextExecutionInfoList
	 *            - ContextExecutionInfoList
	 * @return foramat
	 */
String format(List<FeedExecutionContextVO> pContextExecutionInfoList);

/**
 * Format.
 * 
 * @param pThrowable
 *            - pThrowable
 * @return IFormatter
 */
String format(Throwable pThrowable);

/**
 * Format all.
 * 
 * @param pThrowable
 *            - pThrowable
 * @return IFormatter
 */
String formatAll(Throwable pThrowable);

/**
 * Prints the all exception.
 * 
 * @param pContextExecutionInfoList
 *            - ContextExecutionInfoList
 * @return foramat
 */
String printAllException(List<FeedExecutionContextVO> pContextExecutionInfoList);
}
