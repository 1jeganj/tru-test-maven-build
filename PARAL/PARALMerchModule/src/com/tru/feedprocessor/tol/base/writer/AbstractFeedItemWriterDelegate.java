package com.tru.feedprocessor.tol.base.writer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.core.scope.context.StepSynchronizationManager;
import org.springframework.batch.item.ItemStreamException;

import atg.repository.MutableRepository;
import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.FeedHelper;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.mapper.IFeedItemMapper;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class AbstractFeedItemWriterDelegate.
 * 
 * @version 1.1
 * @author Professional Access
 */
public abstract class AbstractFeedItemWriterDelegate implements IFeedItemWriterDelegate {
	
	/**
	 * The Feed Helper.
	 */
	private FeedHelper mFeedHelper; 
	
	/** The mLogger. */
	private FeedLogger mLogger;

	/** The mphase name. */
	private String mPhaseName;

	/** The mStepName name. */
	private String mStepName;
	
	/** The Unique sub list. */
	private ThreadLocal<List<BaseFeedProcessVO>> mUniqueInsertItemList = new ThreadLocal<List<BaseFeedProcessVO>>();
	
	/** The Unique sub list. */
	private ThreadLocal<List<BaseFeedProcessVO>> mUniqueUpdateItemList = new ThreadLocal<List<BaseFeedProcessVO>>();
	
	/**
	 * Gets the phase name.
	 * 
	 * @return the phaseName
	 */
	public String getPhaseName() {
		return mPhaseName;
	}

	/**
	 * The m mapper map.
	 */
	private IFeedItemMapper mMapper = null;
	
	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * Gets the mapper.
	 * 
	 * @return the mapper
	 */
	public IFeedItemMapper getMapper() {
		return mMapper;
	}

	/**
	 * Sets the mapper.
	 * 
	 * @param pMapper
	 *            the mapper to set
	 */
	public void setMapper(IFeedItemMapper pMapper) {
		mMapper = pMapper;
	}

	/**
	 * Gets the step name.
	 * 
	 * @return the stepName
	 */
	public String getStepName() {
		return mStepName;
	}

	/**
	 * Gets the feed helper.
	 * 
	 * @return the feedHelper
	 */
	public FeedHelper getFeedHelper() {
		return mFeedHelper;
	}

	/**
	 * Sets the feed helper.
	 * 
	 * @param pFeedHelper
	 *            the feedHelper to set
	 */
	public void setFeedHelper(FeedHelper pFeedHelper) {
		mFeedHelper = pFeedHelper;
	}

	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.base.writer.IFeedItemWriterDelegate#doOpen()
	 */
	@Override
	public void doOpen() throws ItemStreamException {
		mStepName=StepSynchronizationManager.getContext().getStepName();
		mPhaseName=(String)(StepSynchronizationManager.getContext().getStepExecution().getExecutionContext().get(FeedConstants.PHASE_NAME));
		
		mUniqueInsertItemList.set(new ArrayList<BaseFeedProcessVO>());
		mUniqueUpdateItemList.set(new ArrayList<BaseFeedProcessVO>());
		
	}

	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.base.writer.IFeedItemWriterDelegate#doWrite(com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO)
	 */
	@Override
	public abstract void doWrite(BaseFeedProcessVO pFeedVO)
			throws FeedSkippableException, RepositoryException;
	
	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.base.writer.IFeedItemWriterDelegate#doClose()
	 */
	@Override
	public void doClose() throws ItemStreamException {
		getCurrentFeedExecutionContext().addFeedItemsToUpdateList(mUniqueInsertItemList.get());
		getCurrentFeedExecutionContext().addFeedItemsToInsertList(mUniqueUpdateItemList.get());
	}


	/**
	 * Gets the current feed execution context.
	 * 
	 * @return - CurrentFeedExecutionContext
	 */
	public FeedExecutionContextVO getCurrentFeedExecutionContext() {
		return  getFeedHelper().getCurrentFeedExecutionContext();
	}
	/**
	 * Retrive data.
	 * 
	 * @param pDataMapKey
	 *            the data map key
	 * @return the object
	 */
	public Object retriveData(String pDataMapKey) {
		return getFeedHelper().retriveData(pDataMapKey);
	}
	/**
	 * Save data.
	 * 
	 * @param pCongifKey
	 *            the congif key
	 * @param pConfigValue
	 *            the config value
	 */
	public void saveData(String pCongifKey, Object pConfigValue) {
		getFeedHelper().saveData(pCongifKey, pConfigValue);
	}
	
	/**
	 * Gets the entity list.
	 * 
	 * @return the entity list
	 */

	public List<String> getEntityList() {
		return getFeedHelper().getStepEntityList(getStepName());
	}
	
	/**
	 * Gets the config value.
	 * 
	 * @param pConfigKey
	 *            the pConfigKey
	 * @return value
	 */
	public Object getConfigValue(String pConfigKey) {
		return getFeedHelper().getConfigValue(pConfigKey);
	}
	
	/**
	 * Sets the config value.
	 * 
	 * @param pName
	 *            - pName
	 * @param pValue
	 *            - pValue
	 */
	public void setConfigValue(String pName, Object pValue) {
		getFeedHelper().setConfigValue(pName, pValue);
	}

	/**
	 * Gets the repository.
	 * 
	 * @param pRepoName
	 *            the repositoryName
	 * @return repository
	 */
	public MutableRepository getRepository(String pRepoName) {
		return getFeedHelper().getRepository(pRepoName);
	}
	
	/**
	 * Gets the item descriptor name.
	 * 
	 * @param pItemDescName
	 *            - pItemDescName
	 * @return item descriptor name
	 */
	public String getItemDescriptorName(String pItemDescName){
		return getFeedHelper().getEntityToItemDescriptorNameMap(pItemDescName);
		
	}
	
	/**
	 * Gets the entity repository name.
	 * 
	 * @param pRepositoryName
	 *            - pRepositoryName
	 * @return repository name
	 */
	public String getEntityRepositoryName(String pRepositoryName){
		return getFeedHelper().getEntityToRepositoryNameMap(pRepositoryName);
	}

	/**
	 * Adds the insert item to list.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the pBaseFeedProcessVO to set
	 */
	protected void addInsertItemToList(BaseFeedProcessVO pBaseFeedProcessVO) {
		mUniqueInsertItemList.get().add(pBaseFeedProcessVO);
	}
	
	/**
	 * Adds the update item to list.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the pBaseFeedProcessVO
	 */
	protected void addUpdateItemToList(BaseFeedProcessVO pBaseFeedProcessVO) {
		mUniqueUpdateItemList.get().add(pBaseFeedProcessVO);
	}
}
