package com.tru.feedprocessor.tol.base.reader;

import java.util.List;
import java.util.Map;

import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.scope.context.StepSynchronizationManager;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import atg.repository.MutableRepository;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.FeedHelper;
import com.tru.feedprocessor.tol.base.Range;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;


/**
 * The Class AbstractFeedItemReader 
 *
 * <p> 
 * Reader for open, read and close in implemented layer 
 * The default implementation for required stepName, phaseName , ServieMap,JobParamMap
 * will process. So that while implementing team can achieve the required details. 
 * </p>
 * 
 * @version 1.0
 * @author Professional Access
 */
public abstract class AbstractFeedItemReader implements
		ItemReader<BaseFeedProcessVO>, ItemStream {

	/** The Individual thread range. */
	private Map<String, Range> mIndividualThreadRange;

	/** The Exec context. */
	private ExecutionContext mExecContext;

	/** The mLogger. */
	private FeedLogger mLogger;

	/** The mphase name. */
	private String mPhaseName;

	/** The mStepName name. */
	private String mStepName;

	/** The m feed data helper. */
	private FeedHelper mFeedHelper;
	
	/** The mJobParameterMap. */
	Map<String, JobParameter> mJobParameterMap;
		
	/**
	 * Gets the exec context.
	 * 
	 * @return the exec context
	 */
	public ExecutionContext getExecContext() {
		return mExecContext;
	}

	/**
	 * Sets the exec context.
	 * 
	 * @param pExecContext
	 *            the new exec context
	 */
	public void setExecContext(ExecutionContext pExecContext) {
		mExecContext = pExecContext;
	}

	/**
	 * Gets the feed helper.
	 * 
	 * @return the mFeedHelper
	 */
	public FeedHelper getFeedHelper() {
		return mFeedHelper;
	}

	/**
	 * Sets the feed helper.
	 * 
	 * @param pFeedHelper
	 *            the new feed helper
	 */
	public void setFeedHelper(FeedHelper pFeedHelper) {
		mFeedHelper = pFeedHelper;
	}

	/**
	 * Gets the step name.
	 * 
	 * @return the mStepName
	 */
	public String getStepName() {
		return mStepName;
	}

	/**
	 * Gets the phase name.
	 * 
	 * @return the phase name
	 */
	public String getPhaseName() {
		return mPhaseName;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the mLogger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the new logger
	 */
	public void setLogger(FeedLogger pLogger) {
		this.mLogger = pLogger;
	}


	/**
	 * Do open.
	 * 
	 * @throws ItemStreamException
	 *             the item stream exception
	 */
	protected void doOpen() throws ItemStreamException{
		// TODO 
	}

	/**
	 * Do close.
	 * 
	 * @throws ItemStreamException
	 *             the item stream exception
	 */
	protected void doClose() throws ItemStreamException {
		// TODO 
	}

	/**
	 * Do read.
	 * 
	 * @return the base feed process vo
	 * @throws Exception
	 *             the exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	protected abstract BaseFeedProcessVO doRead() throws Exception, FeedSkippableException;

	/**
	 * Close.
	 *
	 * @throws ItemStreamException the item stream exception
	 * @see org.springframework.batch.item.ItemStream#close()
	 */
	@Override
	public void close() throws ItemStreamException {
			doClose();
	}

	/**
	 *  
	 *
	 * @param pExecContext the exec context
	 * @throws ItemStreamException the item stream exception
	 * @see org.springframework.batch.item.ItemStream#open(org.springframework.batch
	 * .item.ExecutionContext)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public synchronized void open(ExecutionContext pExecContext)
			throws ItemStreamException {
		
			mStepName=StepSynchronizationManager.getContext().getStepName();
			mPhaseName=(String)(StepSynchronizationManager.getContext().getStepExecution().getExecutionContext().get(FeedConstants.PHASE_NAME));
		
			mJobParameterMap = StepSynchronizationManager.getContext().getStepExecution().getJobParameters().getParameters();
		
			getLogger().logDebugMessage(getStepName() + FeedConstants.STARTED);
			setExecContext(pExecContext);
		
			mIndividualThreadRange = (Map<String, Range>) pExecContext.get(FeedConstants.RANGE_MAP);
		
			doOpen();
			
			getLogger().logDebugMessage(getStepName() + FeedConstants.FINISHED);
	}

	/**
	 *  
	 *
	 * @param pExecContext the exec context
	 * @throws ItemStreamException the item stream exception
	 * @see org.springframework.batch.item.ItemStream#update(org.springframework.
	 * batch.item.ExecutionContext)
	 */
	@Override
	public void update(ExecutionContext pExecContext)
			throws ItemStreamException {
			doUpdate();
	}

	/**
	 * Do close.
	 * 
	 * @throws ItemStreamException
	 *             the item Stream exception
	 */
	protected void doUpdate() throws ItemStreamException {
		//TODO
	}

	/**
	 * Read.
	 *
	 * @return the base feed process vo
	 * @throws Exception the exception
	 * @throws UnexpectedInputException the unexpected input exception
	 * @throws ParseException the parse exception
	 * @throws NonTransientResourceException the non transient resource exception
	 * @throws FeedSkippableException the feed skippable exception
	 * @see org.springframework.batch.item.ItemReader#read()
	 */
	@Override
	public BaseFeedProcessVO read() throws Exception, UnexpectedInputException,
			ParseException, NonTransientResourceException,
			FeedSkippableException {
		BaseFeedProcessVO VO = null;
		VO = doRead();
		return VO;
	}

	/**
	 * Gets the start index.
	 * 
	 * @return the start index
	 */
	public synchronized int getStartIndex() {
		return getExecContext().getInt(FeedConstants.FROM, 0);
	}

	/**
	 * Gets the last index.
	 * 
	 * @param pDefaultSize
	 *            the default size
	 * @return the last index
	 */
	public synchronized int getLastIndex(int pDefaultSize) {
		return getExecContext().getInt(FeedConstants.TO, pDefaultSize);
	}

	/**
	 * Gets the range.
	 * 
	 * @return the range
	 */
	public synchronized Map<String, Range> getRange() {
		return mIndividualThreadRange;
	}
	/**
	 * 
	 * @return - CurrentFeedExecutionContext
	 */
	public FeedExecutionContextVO getCurrentFeedExecutionContext() {
		return (FeedExecutionContextVO) getFeedHelper().getCurrentFeedExecutionContext();
	}
	/**
	 * Retrive data.
	 * 
	 * @param pDataMapKey
	 *            the data map key
	 * @return the object
	 */
	public Object retriveData(String pDataMapKey) {
		return getFeedHelper().retriveData(pDataMapKey);
	}
	/**
	 * Save data.
	 * 
	 * @param pConfigKey
	 *            the pConfigKey
	 * @param pConfigValue
	 *            the config value
	 */
	public void saveData(String pConfigKey, Object pConfigValue) {
		 getFeedHelper().saveData(pConfigKey, pConfigValue);
	}
	
	/**
	 * 
	 * @return the entity list
	 */
	public List<String> getEntityList() {
		return getFeedHelper().getStepEntityList(getStepName());
	}
	/**
	 * 
	 * @param pConfigKey the pConfigKey
	 * @return value
	 */
	public Object getConfigValue(String pConfigKey) {
		return getFeedHelper().getConfigValue(pConfigKey);
	}
	/**
	 * 
	 * @param pName - pName
	 * @param pValue - pValue
	 */
	public void setConfigValue(String pName, Object pValue) {
		getFeedHelper().setConfigValue(pName, pValue);
	}

	/**
	 * 
	 * @param pRepoName the repositoryName
	 * @return repository
	 */
	public MutableRepository getRepository(String pRepoName) {
		return getFeedHelper().getRepository(pRepoName);
	}
	/**
	 * 
	 * @param pEntityName - pEntityName
	 * @return item descriptor name
	 */
	public String getItemDescriptorName(String pEntityName){
		return getFeedHelper().getEntityToItemDescriptorNameMap(pEntityName);
		
	}
	/**
	 * 
	 * @param pEntityName - pEntityName
	 * @return repository name
	 */
	public String getEntityRepositoryName(String pEntityName){
		return getFeedHelper().getEntityToRepositoryNameMap(pEntityName);
	}
	
	/**
	 * @param pJobKey - pJobKey
	 * @return the JobParameter
	 */
	public Object getJobParameter(String pJobKey){
		return mJobParameterMap.get(pJobKey).getValue();
	}
}
