package com.tru.feedprocessor.tol.base.mapper;

import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;

/**
 * The Interface ICatFeedItemMapper.
 * 
 * @version 1.1
 * @author Professional Access
 * @param <T>
 *            the generic type
 * 
 */
public interface IFeedItemMapper<T> {

	/**
	 * Map.
	 * 
	 * @param pVOItem
	 *            the vO item which will be extending the BaseFeedProcessVo
	 * @param pRepoItem
	 *            the repo item
	 * @return the mutable repository item
	 * 
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 * @throws RepositoryException   
	 * 				the RepositoryException 
	 */
	MutableRepositoryItem map(T pVOItem, MutableRepositoryItem pRepoItem)
			throws RepositoryException,FeedSkippableException;
}
