package com.tru.feedprocessor.tol.base.processor;

import atg.nucleus.ServiceMap;
import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.processor.support.IFeedItemValidator;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;


/**
 * The Class FeedItemValidateProcessor.
 * 
 * For validating used <code>lFeedValidator.validate(pFeedVO)</code> will validate any value comes from the feed
 * team has to override this method for any validation of the value of the feed
 *
 * @version 1.0
 * @author Professional Access
 */

public class FeedItemValidateProcessor extends AbstractFeedItemProcessor {
	
	/**
	 * The validators.
	 */
	private ServiceMap mValidators;
	
	/**
	 * Gets the validators.
	 * 
	 * @return the validators
	 */
	public ServiceMap getValidators() {
		return mValidators;
	}
	
	/**
	 * Sets the validators.
	 * 
	 * @param pValidators
	 *            the validators to set
	 */
	public void setValidators(ServiceMap pValidators) {
		mValidators = pValidators;
	}
	
	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.AbstractFeedItemProcessor#process(com.tru.feedprocessor.tol.vo.BaseFeedProcessVO)
	 */
	@Override
	public BaseFeedProcessVO doProcess(BaseFeedProcessVO pFeedVO) throws FeedSkippableException, RepositoryException{
		IFeedItemValidator lFeedValidator=null;
		if(getValidators()!=null && !getValidators().isEmpty()){
			lFeedValidator=(IFeedItemValidator) getValidators().get(pFeedVO.getEntityName());
			try{
				lFeedValidator.validate(pFeedVO);
			}catch (FeedSkippableException pFeedSkippableException) {
				throw new FeedSkippableException(getPhaseName(), getStepName(),pFeedSkippableException.getMessage(), pFeedVO,pFeedSkippableException);
			}	
		}
		return pFeedVO;
	}
}
