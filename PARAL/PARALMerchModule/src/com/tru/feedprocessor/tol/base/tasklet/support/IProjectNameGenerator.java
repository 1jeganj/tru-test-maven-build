package com.tru.feedprocessor.tol.base.tasklet.support;

import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Interface IProjectNameGenerator.
 * 
 * The  IProjectNameGenerator 
 *
 * @version 1.1
 * @author Professional Access
 */
public interface IProjectNameGenerator {

	/**
	 * Gets the name of the project which will be used in BCC project creation.
	 * @param pFeedExecCntx - pFeedExecCntx
	 * @return the project name
	 */
	String getProjectName(FeedExecutionContextVO pFeedExecCntx);
}
