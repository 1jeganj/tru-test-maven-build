package com.tru.feedprocessor.tol.base.tasklet;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.commons.net.ftp.FTPClient;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.SftpProgressMonitor;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;

/**
 * The Class FileUploadTask.
 *
 * @version 1.1
 * @author Professional Access
 */
public class FileUploadTask extends AbstractTasklet {

	/** The Host . */
	private String mHost;
	/** The port. */
	private int mPort;
	/** The User. */
	private String mUser;
	/** The Password. */
	private String mPass;
	/** The SourceDirectory. */
	private String mSourceDir;
	/** The destination Directory. */
	private String mDestinationDir;
	/** The ProtocolType. */
	private String mProtocolType;
	
	
	/** The FileType. */
	private String mFileType;
	
	/** The FileType. */
	private String mUploadFileType;

	@Override
	protected void doExecute() throws FeedSkippableException, Exception {
		String	lFileUploadFlag = nullIfEmpty(getConfigValue(FeedConstants.FILE_UPLOADER_FLAG).toString());
	
		if (lFileUploadFlag != null
				&& lFileUploadFlag.equals(Boolean.toString(FeedConstants.TRUE))) {
			String lStrPort = nullIfEmpty(getConfigValue(
					FeedConstants.FTP_SFTP_PORT).toString());
			mHost = nullIfEmpty(getConfigValue(FeedConstants.FTP_SFTP_HOST)
					.toString());
			mPort = Integer.parseInt((lStrPort == null) ? FeedConstants.STRING_ZERO
					: lStrPort);
			mUser = nullIfEmpty(getConfigValue(FeedConstants.FPT_SFTP_USER)
					.toString());
			mPass = nullIfEmpty(getConfigValue(FeedConstants.FPT_SFTP_PASSWORD)
					.toString());
			mDestinationDir = nullIfEmpty(getConfigValue(
					FeedConstants.FPT_SFTP_SOURCE_DIR).toString());
			mSourceDir = nullIfEmpty(getConfigValue(FeedConstants.FILEPATH)
					.toString());
			mProtocolType = nullIfEmpty(getConfigValue(FeedConstants.PROTOCOL_TYPE)
					.toString().trim());

			mFileType = getConfigValue(FeedConstants.FILETYPE).toString();
			
			mUploadFileType = getConfigValue(FeedConstants.UPLOAD_FILETYPE).toString();
			
			String fileNameOrPattern = (nullIfEmpty(getConfigValue(
					FeedConstants.UPLOAD_FILENAME).toString()) == null ? FeedConstants.STAR
							: getConfigValue(FeedConstants.UPLOAD_FILENAME)
							.toString())
							+ FeedConstants.DOT + mFileType;
			
			if (null != mHost && mPort != 0 && null != mUser && null != mPass
					&& null != mSourceDir && null != mDestinationDir
					&& null != mProtocolType) {
				
				String source_filepath = mSourceDir + fileNameOrPattern;
				StringBuffer destinaton_filepath = new StringBuffer();
				destinaton_filepath.append(destinaton_filepath);
				
				if (FeedConstants.ZIP_TYPE.equals(mUploadFileType)) {
					destinaton_filepath.append(FeedConstants.DOT).append(FeedConstants.ZIP_TYPE);
					createZipFile(new File(source_filepath), destinaton_filepath.toString());
				}
				
				if (FeedConstants.GZIP_TYPE.equals(mUploadFileType)) {
					//destinaton_filepath = destinaton_filepath + FeedConstants.DOT + FeedConstants.GZIP_TYPE;
					destinaton_filepath.append(FeedConstants.DOT).append(FeedConstants.ZIP_TYPE);
					createGzipFile(source_filepath, destinaton_filepath.toString());
				}	
			
				if (FeedConstants.SFTP_PROTOCOL.equals(mProtocolType)) {
					sftpFileUpload(destinaton_filepath.substring(destinaton_filepath.lastIndexOf(FeedConstants.FORWORD_SLASH) + FeedConstants.NUMBER_ONE));
				} else if (FeedConstants.FTP_PROTOCOL.equals(mProtocolType)) {
					ftpFileUpload(destinaton_filepath.substring(destinaton_filepath.lastIndexOf(FeedConstants.FORWORD_SLASH) + FeedConstants.NUMBER_ONE));
				}
			}
		}
		else{
			getLogger().logDebugMessage(FeedConstants.SKIP_FILE_UPLOADING);
		}
	}

	/**
	 * @param pSource_filepath - pSource_filepath
	 * @param pDestinaton_zip_filepath - pDestinaton_zip_filepath
	 */
	public void createGzipFile(String pSource_filepath, String pDestinaton_zip_filepath) {

		byte[] buffer = new byte[FeedConstants.BUFFER_SIZE_BYTE];
		FileInputStream fileInput = null;
		FileOutputStream fileOutputStream = null;
		GZIPOutputStream gzipOuputStream = null;
		try {
			
			fileOutputStream =new FileOutputStream(pDestinaton_zip_filepath);
			gzipOuputStream = new GZIPOutputStream(fileOutputStream);

			fileInput = new FileInputStream(pSource_filepath);

			int bytes_read;
			
			while ((bytes_read = fileInput.read(buffer)) > FeedConstants.NUMBER_ZERO) {
				gzipOuputStream.write(buffer, FeedConstants.NUMBER_ZERO, bytes_read);
			}

			fileInput.close();

			gzipOuputStream.finish();
			gzipOuputStream.close();
			
			//getLogger().logInfo("The file was compressed successfully!");

		} catch (IOException ex) {
			getLogger().logError(FeedConstants.ERROR + FeedConstants.IFUN +ex.getMessage(), ex);
		}finally{
			 if (fileOutputStream != null) {
			        try {
			        	fileOutputStream.close();
			        } catch (IOException ex) {
			        	if (getLogger().isLoggingError()) {
			        		getLogger().logError(
									FeedConstants.ERROR + FeedConstants.IFUN
											+ ex.getMessage(), ex);
						}
			        }
			    }
			if(null != fileInput){
				try {
					fileInput.close();
				} catch (IOException e) {
							getLogger().logError(FeedConstants.ERROR + FeedConstants.IFUN +e.getMessage(), e);
				}
				fileInput=null;
			}
			 if (gzipOuputStream != null) {
			        try {
			        	gzipOuputStream.close();
			        } catch (IOException ex) {
			        	if (getLogger().isLoggingError()) {
			        		getLogger().logError(
									FeedConstants.ERROR + FeedConstants.IFUN
											+ ex.getMessage(), ex);
						}
			        }
			    }
			
		}
	}
	
	 /**
	 * @param pInputFile - pInputFile
	 * @param pZipFilePath -pZipFilePath 
	 */
	public void createZipFile(File pInputFile, String pZipFilePath) {
		FileInputStream fileInputStream = null;   
		try {

	            // Wrap a FileOutputStream around a ZipOutputStream
	            // to store the zip stream to a file. Note that this is
	            // not absolutely necessary
	            FileOutputStream fileOutputStream = new FileOutputStream(pZipFilePath);
	            ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);

	            // a ZipEntry represents a file entry in the zip archive
	            // We name the ZipEntry after the original file's name
	            ZipEntry zipEntry = new ZipEntry(pInputFile.getName());
	            zipOutputStream.putNextEntry(zipEntry);

	            fileInputStream = new FileInputStream(pInputFile);
	            byte[] buf = new byte[FeedConstants.BUFFER_SIZE_BYTE];
	            int bytesRead;

	            // Read the input file by chucks of 1024 bytes
	            // and write the read bytes to the zip stream
	            while ((bytesRead = fileInputStream.read(buf)) > FeedConstants.NUMBER_ZERO) {
	                zipOutputStream.write(buf, FeedConstants.NUMBER_ZERO, bytesRead);
	            }

	            // close ZipEntry to store the stream to the file
	            zipOutputStream.closeEntry();

	            zipOutputStream.close();
	            fileOutputStream.close();

	          //getLogger().logInfo("Regular file :" + pInputFile.getCanonicalPath()+" is zipped to archive :"+pZipFilePath);

	        } catch (IOException ex) {
			if (isLoggingError()) {
	        		logError(FeedConstants.ERROR + FeedConstants.IFUN +ex.getMessage(), ex);
	        	}
	        }
			finally{
				if(null != fileInputStream){
					try {
						fileInputStream.close();
					} catch (IOException e) {
					if (isLoggingError()) {
						logError(FeedConstants.ERROR + FeedConstants.IFUN + e.getMessage(), e);
					}
					}
					fileInputStream=null;
				}	
			}

	    }

	/**
	 * @param pFilenameOrPattern - pFilenameOrPattern
	 */
	private void ftpFileUpload(String pFilenameOrPattern) {
		FTPClient client = new FTPClient();
		FileInputStream fis = null;
		try {
			client.connect(mHost, mPort);
			boolean result = client.login(mUser, mPass);

			if (!result) {
				getLogger().logInfoMessage(FeedConstants.CONNECTION_FAILED);
			}

			File dir = new File(mSourceDir);
			FileFilter filter = new RegexFileFilter(pFilenameOrPattern);
			File[] patternMatchfiles = dir.listFiles(filter);
			for (File fileName : patternMatchfiles) {
				fis = new FileInputStream(fileName.getAbsoluteFile());
				// Store file on server and logout
				client.storeFile(mDestinationDir + fileName.getName(), fis);
			}
			client.logout();
		} catch (IOException ex) {
			if (isLoggingError()) {
				logError(FeedConstants.ERROR + FeedConstants.IFUN + ex.getMessage(), ex);
			}
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
				client.disconnect();
			} catch (IOException ex) {
				if (isLoggingError()) {
					logError(FeedConstants.ERROR + FeedConstants.IFUN +ex.getMessage(), ex);
				}
			}
		}

	}


	/**
	 * @param pString - pString
	 * @return nullIfEmpty
	 */
	public String nullIfEmpty(String pString) {
		if (org.springframework.util.StringUtils.isEmpty(pString)
				|| pString == null) {
			return null;
		}
		return pString;
	}
	
	/**
	 * @param pFilenameAndPattern - pFilenameAndPattern
	 */
	private void sftpFileUpload(String pFilenameAndPattern) {
		Session session = null;

		Channel channel = null;
		ChannelSftp sftpChannel = null;
		try {
			JSch jsch = new JSch();
			session = jsch.getSession(mUser, mHost, mPort);
			session.setPassword(mPass);

			session.setConfig(FeedConstants.STRICT_HOST_KEY_CHECKING,
					FeedConstants.NO_OPTION);
			session.connect();

			channel = session.openChannel(FeedConstants.SFTP_PROTOCOL
					.toLowerCase());
			channel.connect();

			sftpChannel = (ChannelSftp) channel;

			File dir = new File(mSourceDir);
			FileFilter filter = new RegexFileFilter(pFilenameAndPattern);
			File[] patternMatchfiles = dir.listFiles(filter);

			for (File fileName : patternMatchfiles) {
				sftpChannel.get(mDestinationDir + fileName.getName(),
						(SftpProgressMonitor) fileName.getAbsoluteFile());
				// OR
				// InputStream in = sftpChannel.get( "remote-file" );
			}
		} catch (JSchException ex) {
			if (isLoggingError()) {
				logError(FeedConstants.FTP_REFUSED_CONNECTION + FeedConstants.IFUN
						+ ex.getMessage(), ex);
			}
		} catch (SftpException ex) {
			if (isLoggingError()) {
				logError(FeedConstants.FILE_UPLOAD_FAILED + FeedConstants.IFUN
						+ ex.getMessage(), ex);
			}
		} catch (Exception ex) {
			if (isLoggingError()) {
				logError(FeedConstants.ERROR + FeedConstants.IFUN + ex.getMessage(),
						ex);
			}
		} finally {
			if (sftpChannel.isConnected()) {
				try {
					session.disconnect();
					channel.disconnect();
					sftpChannel.quit();
					// getLogger().logInfo("FTP_DISCONNECT");
				} catch (Exception ioe) {
					if (isLoggingError()) {
						logError(FeedConstants.NO_FILES_TO_UPLOAD
								+ FeedConstants.IFUN + ioe.getMessage(),
						ioe);
					}
				}
			}
		}
	}
}