package com.tru.feedprocessor.tol.base.tasklet;

import atg.epub.project.Process;
import atg.process.action.ActionException;
import atg.workflow.ActorAccessException;
import atg.workflow.MissingWorkflowDescriptionException;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.merchutil.tol.workflow.WorkFlowTools;

/**
 * The Class DeployProjectTasklet.
 * 
 * This is used to deploy the project from BCC based on the configuration USEAUTODEPLOY_WORKFLOW flag
 * if <code>USEAUTODEPLOY_WORKFLOW</code> is true then deploy 
 * other wise it just finish the task
 *    
 * @version 1.1
 * @author Professional Access
 */
public class DeployProjectTask extends AbstractTasklet {
	
	/** The m work flow tools. */
	WorkFlowTools mWorkFlowTools;
	
	/**
	 * Gets the work flow tools.
	 *
	 * @return the work flow tools
	 */
	public WorkFlowTools getWorkFlowTools() {
		return mWorkFlowTools;
	}
	
	/**
	 * Sets the work flow tools.
	 *
	 * @param pWorkFlowTools the new work flow tools
	 */
	public void setWorkFlowTools(WorkFlowTools pWorkFlowTools) {
		mWorkFlowTools = pWorkFlowTools;
	}
	
	/** @see com.tru.feedprocessor.tol.AbstractDeployProjectTasklet#deployProject()
	 */
	/**
	 * Deploy project.
	 */
	protected void deployProject() {

		if (getLogger().isLoggingDebug()) {
			getLogger()
					.logDebugMessage(
							"Entering into method:[deployProject] class [DeployProjectTask]");
		}

		atg.epub.project.Process lProcess = (Process) retriveData(FeedConstants.PROCESS);
		
		if ((Boolean) getConfigValue(FeedConstants.USEAUTODEPLOY_WORKFLOW)) {
				try {
					getWorkFlowTools().advanceWorkflow(lProcess,
							getWorkFlowTools().getTaskOutcomeId());
				} catch (MissingWorkflowDescriptionException pExe) {
					if (isLoggingError()) {
						logError(pExe.getMessage(), pExe);
					}
				} catch (ActorAccessException pExe) {
					if (isLoggingError()) {
						logError(pExe.getMessage(), pExe);	
					}
				} catch (ActionException pExe) {
					if (isLoggingError()) {
						logError(pExe.getMessage(), pExe);
					}
				}
			}

			getWorkFlowTools().finishTask();

			if (getLogger().isLoggingDebug()) {
				getLogger()
						.logDebugMessage(
								"Exit from method:[deployProject] class [DeployProjectTask]");
			}
	}
	
	/**
	 * Retrive project name.
	 *
	 * @return the string
	 */
	protected String retriveProjectName(){
		return (String)retriveData(FeedConstants.PROJECT_NAME);
		
	}

	/**
	 * Do execute.
	 *
	 * @throws FeedSkippableException the feed skippable exception
	 * @see com.tru.feedprocessor.tol.AbstractTasklet#doExecute()
	 */
	@Override
	protected void doExecute() throws FeedSkippableException {
		deployProject();
	}

}
