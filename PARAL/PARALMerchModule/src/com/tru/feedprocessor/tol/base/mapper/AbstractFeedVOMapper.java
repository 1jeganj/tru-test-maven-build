package com.tru.feedprocessor.tol.base.mapper;

import atg.repository.MutableRepository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.FeedHelper;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class AbstractFeedVOMapper.
 * 
 * @version 1.1
 * @author Professional Access 
 */
public abstract class AbstractFeedVOMapper implements IFeedVOMapper<BaseFeedProcessVO>{
	/** The m feed data helper. */
	private FeedHelper mFeedHelper;

	/**
	 * Gets the feed data helper.
	 *
	 * @return the mFeedHelper
	 */
	public FeedHelper getFeedHelper() {
		return mFeedHelper;
	}

	/**
	 * Sets the data holder.
	 *
	 * @param pFeedHelper the new data holder
	 */
	public void setFeedHelper(FeedHelper pFeedHelper) {
		mFeedHelper = pFeedHelper;
	}

	/**
	 * Gets the current feed execution context.
	 * 
	 * @return - CurrentFeedExecutionContext
	 */
	public FeedExecutionContextVO getCurrentFeedExecutionContext() {
		return (FeedExecutionContextVO) getFeedHelper().getCurrentFeedExecutionContext();
	}
	/**
	 * Retrive data.
	 * 
	 * @param pDataMapKey
	 *            the data map key
	 * @return the object
	 */
	public Object retriveData(String pDataMapKey) {
		return getFeedHelper().retriveData(pDataMapKey);
	}
	/**
	 * Save data.
	 * 
	 * @param pConfigKey
	 *            the pConfigKey
	 * @param pConfigValue
	 *            the config value
	 */
	public void saveData(String pConfigKey, Object pConfigValue) {
		 getFeedHelper().saveData(pConfigKey, pConfigValue);
	}
	
	/**
	 * Gets the config value.
	 * 
	 * @param pConfigKey
	 *            the pConfigKey
	 * @return value
	 */
	public Object getConfigValue(String pConfigKey) {
		return getFeedHelper().getConfigValue(pConfigKey);
	}
	
	/**
	 * Sets the config value.
	 * 
	 * @param pName
	 *            - pName
	 * @param pValue
	 *            - pValue
	 */
	public void setConfigValue(String pName, Object pValue) {
		getFeedHelper().setConfigValue(pName, pValue);
	}

	/**
	 * Gets the repository.
	 * 
	 * @param pRepoName
	 *            the repositoryName
	 * @return repository
	 */
	public MutableRepository getRepository(String pRepoName) {
		return getFeedHelper().getRepository(pRepoName);
	}
	
	/**
	 * Gets the item descriptor name.
	 * 
	 * @param pEntityName
	 *            - pEntityName
	 * @return item descriptor name
	 */
	public String getItemDescriptorName(String pEntityName){
		return getFeedHelper().getEntityToItemDescriptorNameMap(pEntityName);
		
	}
	
	/**
	 * Gets the entity repository name.
	 * 
	 * @param pEntityName
	 *            - pEntityName
	 * @return repository name
	 */
	public String getEntityRepositoryName(String pEntityName){
		return getFeedHelper().getEntityToRepositoryNameMap(pEntityName);
	}
	
	/** @see com.tru.feedprocessor.tol.base.mapper.IFeedVOMapper#map(java.lang.Object, atg.repository.RepositoryItem)
	 * @param pVOItem VOItem
	 * @param pRepoItem repoItem
	 * @return baseFeedProcessVO 
	 * @throws RepositoryException RepositoryException
	 * @throws FeedSkippableException FeedSkippableException
	 */
	@Override
	public abstract BaseFeedProcessVO map(BaseFeedProcessVO pVOItem,
			RepositoryItem pRepoItem) throws RepositoryException,FeedSkippableException;
}
