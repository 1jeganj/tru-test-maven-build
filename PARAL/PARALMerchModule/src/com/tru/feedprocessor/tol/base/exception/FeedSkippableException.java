package com.tru.feedprocessor.tol.base.exception;

import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;


/**
 * The Class FeedSkippableException.
 * 
 * This is the exception class which will be used for feed process
 * During step execution if any error comes then we will catch the exception in abstract class
 * and create a object of this class and it will be stored in the step execution.
 * 
 * @version 1.1
 * @author Professional Access
 */
public class FeedSkippableException extends Exception {
	

	/** The Constant serialVersionUID.  */
	private static final long serialVersionUID = 8125910957353746230L;
	
	/** The m phase name. */
	private String mPhaseName;
	
	/** The m step name. */
	private String mStepName;
	
	/** The mFeedItemVO. */
	private BaseFeedProcessVO mFeedItemVO;
	
	/**
	 * FeedSkippableException constructor.
	 * 
	 * @param pMessage the Message
	 */
	public FeedSkippableException(String pMessage){
		super(pMessage);
	}
	
	/**
	 * Instantiates a new feed skippable exception.
	 *
	 * @param pPhaseName the phase name
	 * @param pStepName the step name
	 * @param pMessage the message
	 * @param pFeedVO the feed vo
	 * @param pException the exception
	 */
	public FeedSkippableException(String pPhaseName,String pStepName,String pMessage,BaseFeedProcessVO pFeedVO, Exception pException){
		
		super(pMessage,pException);
		mPhaseName=pPhaseName;
		mStepName=pStepName;
		mFeedItemVO=pFeedVO;
		
	}
	
	/**
	 * Gets the phase name.
	 *
	 * @return the mPhaseName
	 */
	public String getPhaseName() {
		return mPhaseName;
	}

	/**
	 * Gets the step name.
	 *
	 * @return the mStepName
	 */
	public String getStepName() {
		return mStepName;
	}
	/**
	 * Gets the feed item vo.
	 *
	 * @return the mFeedItemVO
	 */
	public BaseFeedProcessVO getFeedItemVO() {
		return mFeedItemVO;
	}

	/**
	 * Sets the feed item vo.
	 *
	 * @param pFeedItemVO the new feed item vo
	 */
	public void setFeedItemVO(BaseFeedProcessVO pFeedItemVO) {
		mFeedItemVO = pFeedItemVO;
	}
	
}
