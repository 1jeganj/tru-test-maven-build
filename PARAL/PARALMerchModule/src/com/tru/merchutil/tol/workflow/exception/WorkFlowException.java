package com.tru.merchutil.tol.workflow.exception;

import atg.core.exception.ContainerException;

/**
 * This class is used for collecting the feed processing exception details.
 *
 * @version : 1.0
 * @author Professional Access
 */
public class WorkFlowException extends ContainerException {
	
	/** Constant for Serial Version UID. */
	private static final long serialVersionUID = 1L;
	
	/**
	 * This method is used for get the exception details.
	 *
	 * @param pMessage 		- String
	 * @param pSourceException - Throwable
	 */
	public WorkFlowException(String pMessage, Throwable pSourceException) {
		super(pMessage, pSourceException);
	}
	
	/**
	 * This method is used for get the exception details.
	 *
	 * @param pMessage 	- String
	 */
	public WorkFlowException(String pMessage) {
		super(pMessage);
	}
	
	/**
	 * This method is used for get the exception details.
	 *
	 * @param pSourceException - Throwable
	 */
	public WorkFlowException(Throwable pSourceException) {
		super(pSourceException);
	}
}
