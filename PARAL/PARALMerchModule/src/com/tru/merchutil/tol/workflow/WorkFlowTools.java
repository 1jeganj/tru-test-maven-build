package com.tru.merchutil.tol.workflow;

import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.transaction.TransactionManager;

import atg.dtm.TransactionDemarcationException;
import atg.epub.project.Process;
import atg.epub.project.ProcessHome;
import atg.epub.project.Project;
import atg.epub.project.ProjectConstants;
import atg.nucleus.GenericService;
import atg.process.action.ActionConstants;
import atg.process.action.ActionException;
import atg.repository.MutableRepository;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.security.Persona;
import atg.security.ThreadSecurityManager;
import atg.security.User;
import atg.security.UserAuthority;
import atg.versionmanager.VersionManager;
import atg.versionmanager.WorkingContext;
import atg.versionmanager.Workspace;
import atg.versionmanager.exceptions.VersionException;
import atg.workflow.ActorAccessException;
import atg.workflow.MissingWorkflowDescriptionException;
import atg.workflow.WorkflowConstants;
import atg.workflow.WorkflowException;
import atg.workflow.WorkflowManager;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.merchutil.tol.workflow.exception.WorkFlowException;


/**
 * <p>This class will do the following operations:</p><br>
 * 1. Creating the BCC Project on the specified work flow.
 * 2. Push all feed data to the Project.
 * 
 * @version : 1.0
 * @author Professional Access
 */
public class WorkFlowTools extends GenericService  {
	

	/** The m project name. */
	private String mProjectName;
	
	/** Property to hold mVersionManager. */
	private VersionManager mVersionManager;
	
	/** Property to hold mWorkflowManager. */
	private WorkflowManager mWorkflowManager;
	
	/** Property to hold mUserAuthority. */
	private UserAuthority mUserAuthority;
	
	/** Property to hold mPersonaPrefix. */
	private String mPersonaPrefix;
	
	/** Property to hold mActivity. */
	private String mActivity;
	
	/** Property to hold mWorkSpaceName. */
	private String mWorkSpaceName;
	
	/** Property to hold mInternalProfileRepository. */
	private MutableRepository mInternalProfileRepository;
	
	/** Property to hold mPublishingRepository. */
	private MutableRepository mPublishingRepository;	
	
	/** The m workflow name. */
	protected String mWorkflowName;
	
	/** Property to hold mBCCUserName. */
	protected String mBCCUserName;
	
	/**
	 * This method will create BCC project and intiate the workflow
	 * It will also insert the data to the project
	 * 
	 * @param pProjectName				- String
	 * @param pWorkflowName				- String
	 * @param pUserName					- String
	 * @return Process					- Process
	 * @throws WorkFlowException		- Throws WorkFlowException if an error occurs
	 */
	public Process initiateWorkflow(String pProjectName, String pWorkflowName, String pUserName) throws WorkFlowException {
		if (isLoggingDebug()) {
			logDebug("Enter into WorkFlowTools.initiateWorkflow(" + pProjectName + "," + pWorkflowName + "," + pUserName + ")");
		}
		Process process = null;
		ProcessHome processHome = null;
		Workspace workspace = null;
		Collection<Process> processes = null;
		try {
			assumeUserIdentity(pUserName);
			workspace = getOpenProject(pProjectName);
			if (workspace == null) {

				processHome = ProjectConstants.getPersistentHomes().getProcessHome();
				if (processes == null || processes.isEmpty()) {
					process = processHome.createProcessForImport(pProjectName,pWorkflowName);
					process.setActivity(getActivity());
					RepositoryItem user = getBccUser(pUserName);
					if(user != null) {
						process.setCreator(user);
						process.getProject().setCreator(user);
					}
					if (isLoggingDebug()) {
						logDebug("Project is Created::"+ pProjectName );
					}
				} else if (processes.size() > FeedConstants.NUMBER_ONE) {
					throw new WorkFlowException(FeedConstants.MULTIPLE_PROCESSED_FOUND);
				} else {
					for (Iterator<Process> iterator = processes.iterator(); iterator.hasNext();) {
						process = iterator.next();
					}
				}
				workspace = mVersionManager.getWorkspaceByName(process.getProject().getWorkspace());
				WorkingContext.pushDevelopmentLine(workspace);
			} else {
				WorkingContext.pushDevelopmentLine(workspace);
			 }
		} catch (WorkflowException workflowException) {
			throw new WorkFlowException(workflowException);
		} catch (TransactionDemarcationException transactionDemarcationException) {
			throw new WorkFlowException(transactionDemarcationException.getMessage(), transactionDemarcationException);
		} catch (EJBException ejbException) {
			throw new WorkFlowException(ejbException.getMessage(), ejbException);
		} catch (ActionException actionException) {
			throw new WorkFlowException(actionException.getMessage(), actionException);
		} catch (CreateException createException) {
			throw new WorkFlowException(createException.getMessage(), createException);
		} catch (VersionException versionException) {
			throw new WorkFlowException(versionException.getMessage(), versionException);
		} catch (RepositoryException repositoryException) {
			throw new WorkFlowException(repositoryException.getMessage(), repositoryException);
		}
		if (isLoggingDebug()) {
			logDebug("Exit From WorkFlowTools.initiateWorkflow()::" + process );
		}
		return process;
	}
	
	/**
	 * This method is used to get the current user name in BCC.
	 *
	 * @param pUserName 					- String
	 * @return projectCreator				- RepositoryItem
	 * @throws WorkFlowException the work flow exception
	 */
	private RepositoryItem getBccUser(String pUserName) throws WorkFlowException {
		if (isLoggingDebug()) {
			logDebug("Enter into WorkFlowTools.getBccUser(" + pUserName + " )");
		}
		RepositoryView reporitoryView = null;
		RepositoryItem[] reporitoryItems = null;
		MutableRepository internalProfileRepository = null;
		RepositoryItem projectCreator = null;
		try {
			internalProfileRepository = getInternalProfileRepository();
			reporitoryView = internalProfileRepository.getView(FeedConstants.PROFILE_USER);
			QueryBuilder queryBuilder = reporitoryView.getQueryBuilder();
			QueryExpression keyProperty = queryBuilder.createPropertyQueryExpression(FeedConstants.PROFILE_LOGIN);
			QueryExpression keyValue = queryBuilder.createConstantQueryExpression(pUserName);
			Query query = queryBuilder.createComparisonQuery(keyValue,keyProperty, QueryBuilder.EQUALS);
			reporitoryItems = reporitoryView.executeQuery(query);
			if ((reporitoryItems != null) && (reporitoryItems.length > FeedConstants.NUMBER_ZERO)) {	  
				projectCreator = reporitoryItems[FeedConstants.NUMBER_ZERO];
			}
		} catch (RepositoryException repositoryException) {
			throw new WorkFlowException(repositoryException.getMessage(), repositoryException);
		}
		if (isLoggingDebug()) {
			logDebug("Exit From WorkFlowTools.getBccUser()::" + projectCreator);
		}
	  return projectCreator;
	}
	
	/**
	 * <b>This method will give access permissions to create a project </b>.
	 *
	 * @param pUserName 		- String
	 * @return true				- Boolean
	 */
	public boolean assumeUserIdentity(String pUserName) {
		if (isLoggingDebug()) {
			logDebug("Enter into WorkFlowTools.assumeUserIdentity(" + pUserName + ")");
		}
		User newUser = null;
		Persona persona = null;
		if (mUserAuthority != null) {
			newUser = new User();
			persona =(Persona) mUserAuthority.getPersona(new StringBuilder().append(mPersonaPrefix).append(pUserName).toString());
			if (persona != null) {
				newUser.addPersona(persona);
				ThreadSecurityManager.setThreadUser(newUser);
				return true;
			}
			if (isLoggingDebug()) {
				logDebug("Exit From WorkFlowTools.assumeUserIdentity()::" + true );
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit From WorkFlowTools.assumeUserIdentity()::" + false );
		}
		return false;
	}
	
	/**
	 * This method will be used to manage the Advanced workflow for creating projects.
	 *
	 * @param pProcess 							- Process
	 * @param pTaskOutcomeId 					- String
	 * @throws MissingWorkflowDescriptionException - Throws MissingWorkflowDescriptionException if an error occurs
	 * @throws ActorAccessException 				- Throws ActorAccessException if an error occurs
	 * @throws ActionException 					- Throws ActionException if an error occurs
	 */
	public void advanceWorkflow(Process pProcess, String pTaskOutcomeId)
		throws MissingWorkflowDescriptionException, ActorAccessException,
		ActionException {
		if (isLoggingDebug()) {
			logDebug("Enter into WorkFlowTools.advanceWorkflow(" + pProcess + "," + pTaskOutcomeId + ")");
		}
		mWorkflowManager.getWorkflowView(ThreadSecurityManager.currentUser()).fireTaskOutcome(
				pProcess.getProject().getWorkflow().getPropertyValue(FeedConstants.PROPERTY_PROCESS_NAME).toString(),
				WorkflowConstants.DEFAULT_WORKFLOW_SEGMENT,
				pProcess.getId(), pTaskOutcomeId,
				ActionConstants.ERROR_RESPONSE_DEFAULT);
		if (isLoggingDebug()) {
			logDebug("Exit From WorkFlowTools.advanceWorkflow()");
		}
	}
	
	/**
	 * This method used to finish all the tasks for the BCC Project.
	 */
	public void finishTask() {
		WorkingContext.popDevelopmentLine();
		ThreadSecurityManager.setThreadUser(null);
	}
	
	/**
	 * This method is used to get the existing project which is in Editable mode.
	 *
	 * @param pProjectName 					- String
	 * @return workspace						- Workspace
	 * @throws CreateException 				- Throws CreateException
	 * @throws RepositoryException 			- Throws RepositoryException
	 * @throws VersionException 				- Throws VersionException
	 */
	public Workspace getOpenProject(String pProjectName) throws CreateException, RepositoryException, VersionException {
		if (isLoggingDebug()) {
			logDebug("Entering into WorkFlowTools.getOpenProject method");
		}
	
		RepositoryItem openProject = null;			
		Workspace workspace = null;
		String existingProjectName = null;
		String workSpace = null;
		
		RepositoryView reporitoryView = getPublishingRepository().getView(FeedConstants.PROJECT_ITEM_NAME);
		QueryBuilder queryBuilder = reporitoryView.getQueryBuilder();
		QueryExpression keyProperty = queryBuilder.createPropertyQueryExpression(FeedConstants.EDITABLE);
		QueryExpression keyValue = queryBuilder.createConstantQueryExpression(FeedConstants.TRUE);
		Query query = queryBuilder.createComparisonQuery(keyValue,keyProperty, QueryBuilder.EQUALS);
		RepositoryItem[] reporitoryItems = reporitoryView.executeQuery(query);
		
		if ((reporitoryItems != null) && (reporitoryItems.length > FeedConstants.NUMBER_ZERO)) {
			if (isLoggingDebug()) {
				logDebug(" 1 or more Projects are in open state !!!");
			}
			for (int i = FeedConstants.NUMBER_ZERO; i < reporitoryItems.length; i++) {
				existingProjectName = reporitoryItems[i].getPropertyValue(FeedConstants.PROJECT_DISPLAY_NAME).toString();
					if(existingProjectName !=null && existingProjectName.matches(pProjectName)) {
						if (isLoggingDebug()) {
							logDebug("Actual Item : - "+ reporitoryItems[i].getPropertyValue(FeedConstants.PROJECT_DISPLAY_NAME).toString());
						}
					openProject = reporitoryItems[i];
				}
		     }
		} else {
			if (isLoggingDebug()) {
				logDebug("Projects are not in open state !!!");
			}
		}
		if (openProject != null) {
			workSpace = openProject.getPropertyValue(getWorkSpaceName()).toString();
			if (isLoggingDebug()) {
				logDebug("Workspace name for the open project is : " + workSpace);
			}
			workspace = getVersionManager().getWorkspaceByName(workSpace);
			if (isLoggingDebug()) {
				logDebug("Work space Name "+workSpace);
				logDebug("Work space Object "+workspace.getName());
			}
			if (isLoggingDebug()) {
				logDebug("Work space Object "+workspace.getName());
			}
			if (isLoggingDebug()) {
				logDebug("Exiting from WorkFlowTools.getOpenProject()");
			}
			return workspace;
		} else {
			if (isLoggingDebug()) {
				logDebug("Exit from WorkFlowTools.getOpenProject method");
			}
			return workspace;
		}
	}
	
	/**
	 * Gets the version manager.
	 *
	 * @return 		- mVersionManager
	 */
	public VersionManager getVersionManager() {
		return mVersionManager;
	}
	
	/**
	 * Sets the version manager.
	 *
	 * @param pVersionManager 	- mVersionManager
	 */
	public void setVersionManager(VersionManager pVersionManager) {
		mVersionManager = pVersionManager;
	}
	
	/**
	 * Gets the workflow manager.
	 *
	 * @return 			- mWorkflowManager
	 */
	public WorkflowManager getWorkflowManager() {
		return mWorkflowManager;
	}
	
	/**
	 * Sets the workflow manager.
	 *
	 * @param pWorkflowManager 	- mWorkflowManager
	 */
	public void setWorkflowManager(WorkflowManager pWorkflowManager) {
		mWorkflowManager = pWorkflowManager;
	}
	
	/**
	 * Gets the user authority.
	 *
	 * @return 			- mUserAuthority
	 */
	public UserAuthority getUserAuthority() {
		return mUserAuthority;
	}
	
	/**
	 * Sets the user authority.
	 *
	 * @param pUserAuthority    - mUserAuthority
	 */
	public void setUserAuthority(UserAuthority pUserAuthority) {
		mUserAuthority = pUserAuthority;
	}
	
	/**
	 * Gets the persona prefix.
	 *
	 * @return 		- mPersonaPrefix
	 */
	public String getPersonaPrefix() {
		return mPersonaPrefix;
	}
	
	/**
	 * Sets the persona prefix.
	 *
	 * @param pPersonaPrefix 	- mPersonaPrefix
	 */
	public void setPersonaPrefix(String pPersonaPrefix) {
		mPersonaPrefix = pPersonaPrefix;
	}
	
	/**
	 * Gets the internal profile repository.
	 *
	 * @return the mInternalProfileRepository
	 */
	public MutableRepository getInternalProfileRepository() {
		return mInternalProfileRepository;
	}
	
	/**
	 * Sets the internal profile repository.
	 *
	 * @param pInternalProfileRepository the mInternalProfileRepository to set
	 */
	public void setInternalProfileRepository(
			MutableRepository pInternalProfileRepository) {
		this.mInternalProfileRepository = pInternalProfileRepository;
	}
	
	/**
	 * Gets the activity.
	 *
	 * @return the mActivity
	 */
	public String getActivity() {
		return mActivity;
	}
	
	/**
	 * Sets the activity.
	 *
	 * @param pActivity the mActivity to set
	 */
	public void setActivity(String pActivity) {
		this.mActivity = pActivity;
	}
	/**
	 * Returns MutableRepository component.
	 *
	 * @return the mPublishingRepository
	 */
	public MutableRepository getPublishingRepository() {
		return mPublishingRepository;
	}
	
	/**
	 * Sets the publishingRepository value to MutableRepository component.
	 *
	 * @param pPublishingRepository - mPublishingRepository
	 */
	public void setPublishingRepository(MutableRepository pPublishingRepository) {
		this.mPublishingRepository = pPublishingRepository;
	}
	
	/**
	 * Gets the work space name.
	 *
	 * @return the mWorkSpaceName
	 */
	public String getWorkSpaceName() {
		return mWorkSpaceName;
	}
	
	/**
	 * Sets the work space name.
	 *
	 * @param pWorkSpaceName the mWorkSpaceName to set
	 */
	public void setWorkSpaceName(String pWorkSpaceName) {
		this.mWorkSpaceName = pWorkSpaceName;
	}

	/**
	 * Gets the project name.
	 *
	 * @return the project name
	 */
	public String getProjectName() {
		return mProjectName;
	}
	
	/**
	 * Sets the project name.
	 *
	 * @param pProjectName the new project name
	 */
	public void setProjectName(String pProjectName) {
		mProjectName = pProjectName;
	}
	/**
	 * Gets the bCC user name.
	 *
	 * @return the bCC user name
	 */
	public String getBCCUserName() {
		return mBCCUserName;
	}
	
	/**
	 * Sets the bCC user name.
	 *
	 * @param pBCCUserName the new bCC user name
	 */
	public void setBCCUserName(String pBCCUserName) {
		mBCCUserName = pBCCUserName;
	}
	
	/**
	 * the mTransactionManager
	 */
	private TransactionManager mTransactionManager;
	
	
	/**
	 * @return mTransactionManager
	 * 
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	/**
	 * @param pTransactionManager - pTransactionManager
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}
	
	/**
	 *  the mTaskOutcomeId
	 */
	private String mTaskOutcomeId;
	/**
	 * @return the mTaskOutcomeId
	 */
	public String getTaskOutcomeId() {
		return mTaskOutcomeId;
	}

	/**
	 * @param pTaskOutcomeId the pTaskOutcomeId to set
	 */
	public void setTaskOutcomeId(String pTaskOutcomeId) {
		mTaskOutcomeId = pTaskOutcomeId;
	}

	/**
	 * Initiate workflow.
	 *
	 * @param pProjectName the project name
	 * @param pWorkflowName workflow Name
	 * @return the process
	 */
	public Process initiateWorkflow(String pProjectName, String pWorkflowName) {
		setProjectName(pProjectName);
		Process lProcess=null;
		try {
			lProcess=initiateWorkflow(pProjectName ,pWorkflowName, mBCCUserName);
		} catch (WorkFlowException e) {
			if(isLoggingError()){
				logError(FeedConstants.ERROR_DETAILS,e);
				}
		}
		return lProcess;
	}
	
/**
 * pushDevelopmentLine
 * @param pProcess - pProcess 
 * 
 */
public  synchronized void pushDevelopmentLine(Process pProcess){
	Workspace workspace;
	try {
		workspace = mVersionManager.getWorkspaceByName(pProcess.getProject().getWorkspace());
		WorkingContext.pushDevelopmentLine(workspace);
	} catch (EJBException e) {
		if(isLoggingError()){
			logError(FeedConstants.ERROR_DETAILS,e);
		}
	} catch (VersionException e) {
		if(isLoggingError()){
			logError(FeedConstants.ERROR_DETAILS,e);
		}
	}
	}
}
