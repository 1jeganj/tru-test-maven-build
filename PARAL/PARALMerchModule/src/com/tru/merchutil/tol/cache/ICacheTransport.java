package com.tru.merchutil.tol.cache;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Map;
import java.util.Set;

/**
 * This Interface ICacheTransport used for invalidating the repository and item level
 * cache.
 *
 * @version 1.1
 * @author Professional Access
 */
public interface ICacheTransport extends Remote {

	/**
	 * This method is used for invalidating the cache for repositories.
	 * 
	 * @param pRepositoryPath
	 *            -- repository path like /atg/commerce/inventory/Inventory
	 * @param pMap
	 *            -- Map [key is item descriptor name and value is set of
	 *            repository ids]
	 * @throws RemoteException
	 *             -- remote exception
	 */
	void invalidateCache(String pRepositoryPath, Map<String, Set<String>> pMap) throws RemoteException;

}
