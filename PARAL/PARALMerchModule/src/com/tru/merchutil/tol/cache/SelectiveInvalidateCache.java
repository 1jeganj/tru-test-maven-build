package com.tru.merchutil.tol.cache;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.deployment.common.DeploymentException;
import atg.deployment.server.AgentRef;
import atg.deployment.server.DeploymentServer;
import atg.deployment.server.Target;
import atg.nucleus.GenericService;

import com.tru.feedprocessor.tol.BaseFeedConstants;

/**
 * This class SelectiveInvalidateCache used for invoking the RMI method for cache
 * invalidation.
 * 
 * @version 1.1
 * @author Professional Access
 */
public class SelectiveInvalidateCache extends GenericService {

	/**
	 * Variable to hold the deployment server component.
	 */

	private DeploymentServer mDeploymentServer;

	/**
	 * Variable to hold the agent component path.
	 */

	private String mAgentComponentPath;

	/**
	 * Gets the deploymentServer.
	 * 
	 * @return the deploymentServer
	 */

	public DeploymentServer getDeploymentServer() {
		return mDeploymentServer;
	}

	/**
	 * Sets the deploymentServer.
	 * 
	 * @param pDeploymentServer
	 *            the deploymentServer to set
	 */

	public void setDeploymentServer(DeploymentServer pDeploymentServer) {
		mDeploymentServer = pDeploymentServer;
	}

	/**
	 * Gets the agentComponentPath.
	 * 
	 * @return the agentComponentPath
	 */

	public String getAgentComponentPath() {
		return mAgentComponentPath;
	}

	/**
	 * Sets the agentComponentPath.
	 * 
	 * @param pAgentComponentPath
	 *            the agentComponentPath to set
	 */

	public void setAgentComponentPath(String pAgentComponentPath) {
		mAgentComponentPath = pAgentComponentPath;
	}

	
	/**
	 * the mRepositoryPathList.
	 */
	private List<String> mRepositoryPathList = new ArrayList<String>();
	
		
	/**
	 * Gets the repository path list.
	 * 
	 * @return the mRepositoryPathList
	 */
	public List<String> getRepositoryPathList() {
		return mRepositoryPathList;
	}

	/**
	 * Sets the repository path list.
	 * 
	 * @param pRepositoryPathList
	 *            - pRepositoryPathList
	 */
	public void setRepositoryPathList(List<String> pRepositoryPathList) {
		mRepositoryPathList = pRepositoryPathList;
	}

	/**
	 * this method will call the publishing agent using RMI. It invalidates the caches for all the feed services.
	 * 
	 * @param pCacheMap - pCacheMap
	 * 
	 */
	public void invalidateCache(Map<String, Set<String>> pCacheMap){
		
		StringBuilder rmiPath = null;
		
		if (getDeploymentServer() != null) {
			try {
				Target[] targets = getDeploymentServer().getDeploymentTargets();
				AgentRef[] agentRefs = null;
				ICacheTransport agent = null;
				String uri = null;
				for (Target tar : targets) {
					if(tar != null) {
						agentRefs = tar.getAgents();
						if (agentRefs != null) {
							for (AgentRef ref : agentRefs) {
								if(ref != null) {
									uri = ref.getTransportURI();
									if(uri != null) {
										rmiPath = new StringBuilder().append(uri.substring(0,	uri.indexOf(BaseFeedConstants.ATG)));
										if(rmiPath != null) {
											rmiPath.append(getAgentComponentPath());
											agent = (ICacheTransport) Naming.lookup(rmiPath.toString());
											if(agent != null) {
												if (isLoggingDebug()) {
													vlogDebug("rmi path is :::::{0} ", rmiPath.toString());
												}
												if (isLoggingDebug()) {
													vlogDebug("repositoryPathList in  invalidateCache method:::::{0}", mRepositoryPathList);
												}
												if (!mRepositoryPathList.isEmpty()) {
													if (isLoggingDebug()) {
														vlogDebug("repositoryPathList is not empty");
													}
													for (String repPath : mRepositoryPathList) {

														if (isLoggingDebug()) {
															vlogDebug("repostoryPath ::::{0}", repPath);
														}
														agent.invalidateCache(repPath, pCacheMap);
													}
												}
												if (isLoggingDebug()) {
													vlogDebug("agent invalidation cache RMI is called");
												}
											}
										}
									}
								}
							}
						}
					}
				}
			} catch (DeploymentException deploymentException) {
					vlogError(deploymentException, "DeploymentException occured while calling RMI ");
			} catch (MalformedURLException malformedURLException) {
					vlogError(malformedURLException, "MalformedURLException occured while calling RMI " );
			} catch (RemoteException remoteException) {
					vlogError(remoteException,"RemoteException occured while calling RMI");
			} catch (NotBoundException notBoundException) {
					vlogError(notBoundException, "NotBoundException occured while calling RMI");
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of invalidateCache method in SelectiveInvalidation");
		}
	}
}