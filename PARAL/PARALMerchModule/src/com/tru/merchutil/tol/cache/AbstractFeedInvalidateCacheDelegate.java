package com.tru.merchutil.tol.cache;

import java.util.Map;
import java.util.Set;


import com.tru.feedprocessor.tol.base.IPostProcessorDelegate;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class AbstractFeedInvalidateCacheDelegate.
 * 
 * @version 1.1
 * @author Professional Access
 */
abstract public class AbstractFeedInvalidateCacheDelegate implements IPostProcessorDelegate  {


	/** The FeedInvalidateCache. */
	private SelectiveInvalidateCache mFeedInvalidateCache;

	/**
	 * Post process.
	 *
	 * @param pFeedExecutionContextVO the feed execution context vo
	 * @see com.tru.feedprocessor.tol.base.IPostProcessorDelegate#postProcess(com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO)
	 */
	@Override
	public void postProcess(FeedExecutionContextVO pFeedExecutionContextVO) {
				
		Map<String, Set<String>> cachedMap=invalidateCacheProcess(pFeedExecutionContextVO);
	
		//putting allUpdatedSkus to cacheMap for cacheInvalidation
		if(null != cachedMap && !cachedMap.isEmpty() ){
			getFeedInvalidateCache().invalidateCache(cachedMap);
		}
	}

	/**
	 * Gets the feed invalidate cache.
	 * 
	 * @return the mFeedInvalidateCache
	 */
	public SelectiveInvalidateCache getFeedInvalidateCache() {
		return mFeedInvalidateCache;
	}

	/**
	 * Sets the feed invalidate cache.
	 * 
	 * @param pFeedInvalidateCache
	 *            the pFeedInvalidateCache to set
	 */
	public void setFeedInvalidateCache(SelectiveInvalidateCache pFeedInvalidateCache) {
		this.mFeedInvalidateCache = pFeedInvalidateCache;
	}
	
	/**
	 * Returns an Map object that can be hold the entityName as key and Set of SKU'S Id. as value 
	 * 
	 * This method always returns immediately, whether or not the 
	 * map object which is used for the clearing selective cache 
	 * based on the entityName and Sku's ID  
	 * 
	 * @param pFeedExecutionContextVO - pFeedExecutionContextVO  
	 * @return Map<String, Set<String>>
	 */
	public abstract Map<String, Set<String>> invalidateCacheProcess(FeedExecutionContextVO pFeedExecutionContextVO);
}
