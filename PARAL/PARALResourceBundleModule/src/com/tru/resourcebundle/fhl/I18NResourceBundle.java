package com.tru.resourcebundle.fhl;

import java.util.Enumeration;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.NamingException;

import atg.core.util.StringUtils;

import com.tru.common.cml.Constants;
import com.tru.common.cml.SystemException;
import com.tru.resourcebundle.mgl.DefaultResourceBundleManager;
import com.tru.resourcebundle.mgl.IResourceBundleManager;

/**
 * This class extends ResourceBundle. This class is used to get the message from the repository for the given key.
 * @author Professional Access
 * @version 1.0
 */
public class I18NResourceBundle extends ResourceBundle {

	
	/** Holds the constant of ResourceBundleManger. */
	private static IResourceBundleManager mResourceBundleManager;
	
	/**
	 * Static block to resolve the ResourceBundleManger component
	 */
	static {
			Context ctx = null;
			try {
				ctx = new javax.naming.InitialContext ();
			
				mResourceBundleManager = (DefaultResourceBundleManager) ctx.lookup (Constants.RESOURCE_BUNDLE_MANAGER);
			} catch (NamingException e) {
				if (mResourceBundleManager.isLoggingError()){
					mResourceBundleManager.logError("NamingException : ",e);
		        }
			}
	}
	
	@Override
	public Enumeration<String> getKeys() {
		if(mResourceBundleManager.isLoggingDebug()){
			mResourceBundleManager.logDebug("I18NResourceBundle - (getKeys[]) : Start");
		}
		Enumeration<String> keys = null;
		try {
			keys = mResourceBundleManager.findAllKeys();
		} catch (SystemException se) {
			if(mResourceBundleManager.isLoggingError()){
				mResourceBundleManager.logError("Error occured in I18NResourceBundle - (getKeys[]) : ",se);
			}
		}
		if(mResourceBundleManager.isLoggingDebug()){
			mResourceBundleManager.logDebug("I18NResourceBundle - (getKeys[]) : End");
		}
		return keys;
	}

	@Override
	protected Object handleGetObject(String pKey) {
		if(mResourceBundleManager.isLoggingDebug()){
			mResourceBundleManager.logDebug("I18NResourceBundle - (handleGetObject[pKey]) : Start");
		}
		Object keyValue=null;
		if(!StringUtils.isBlank(pKey)){
			try {
				keyValue = mResourceBundleManager.getKeyValue(pKey);
			} catch (SystemException se) {
				if(mResourceBundleManager.isLoggingError()){
					mResourceBundleManager.logError("Error occured in I18NResourceBundle - (handleGetObject[pKey]) : ",se);
				}
			}	
		}
		if(mResourceBundleManager.isLoggingDebug()){
			mResourceBundleManager.logDebug("KeyValue in I18NResourceBundle - (handleGetObject[pKey]) is : "+keyValue);
			mResourceBundleManager.logDebug("I18NResourceBundle - (handleGetObject[pKey]) : End");
		}
		return keyValue;
	}

}
