package com.tru.resourcebundle.mgl;

import java.util.Enumeration;

import atg.nucleus.logging.ApplicationLogging;

import com.tru.common.cml.SystemException;
/**
 * This is interface has methods that interact with Common ResourceBundleTools class to get keyValue from 
 * i18N ResourceBundle Repository.
 * @author Professional Access
 * @version 1.0
 */
public interface IResourceBundleManager extends ApplicationLogging{
	
	/**
	 * This method is used to call tools class getKeyValue method to get the keyValue from repository.
	 *
	 * @param pKeyName KeyName
	 * @return String
	 * @throws SystemException the system exception
	 */
	String getKeyValue(String pKeyName) throws SystemException;
	
	/**
	 * This method is used to call tools class findAllKeys method to get all the key value pair
	 * from repository.
	 *
	 * @return lArraylist - Enumeration type
	 * @throws SystemException the system exception
	 */
	Enumeration<String> findAllKeys() throws SystemException;

}
