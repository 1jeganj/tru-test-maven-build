package com.tru.resourcebundle.mgl;

import java.util.Enumeration;

import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;

import com.tru.common.cml.Constants;
import com.tru.common.cml.IErrorId;
import com.tru.common.cml.SystemException;
import com.tru.common.tol.IResourceBundleTools;

/**
 * The class holds the methods that interacts with ResourceBundleTools to get the 
 * KeyValue from i18N ResourceBundle Repository.
 * 
 * @author Professional Access
 * @version 1.0
 */

public class DefaultResourceBundleManager extends GenericService implements IResourceBundleManager {
	
	/** Holds the constant of IResourceBundleTools. */
	private IResourceBundleTools mResourceBundleTools;
	
	/**
	 * Gets the resource bundle tools.
	 *
	 * @return mResourceBundleTools
	 */
	public IResourceBundleTools getResourceBundleTools() {
		return mResourceBundleTools;
	}
	
	/**
	 * Sets the resource bundle tools.
	 *
	 * @param pResourceBundleTools ResourceBundleTools
	 */
	public void setResourceBundleTools(IResourceBundleTools pResourceBundleTools) {
		this.mResourceBundleTools = pResourceBundleTools;
	}
	
	/**
	 * This method is used to append the site name with the ErrorId.
	 * @return String pErrorKey
	 */
	public String getSiteId(){		
		if(isLoggingDebug()){
			logDebug("ErrorHandlerManager - (getSiteKey-[pErrorId]):Start");
		}
		String currentSiteId = null;
		
		currentSiteId = SiteContextManager.getCurrentSiteId();
		if(isLoggingDebug()){
			logDebug("currentSite :"+ currentSiteId);
		}
				
		if(isLoggingDebug()){
			logDebug("ErrorHandlerManager - (getSiteKey-[pErrorId]):End");
		}
		return currentSiteId;
		
	}
	
	/**
	 * This method Invokes tools class getErrorMessage by passing ErrorId
	 * and SiteId.
	 *
	 * @param pKeyName - String
	 * @return String errorMessage
	 * @throws SystemException the system exception
	 */
	public String getKeyValue(String pKeyName) throws SystemException{
		
		if(isLoggingDebug()){
			logDebug("ErrorHandlerManager - (getErrorMessage-[pErrorId]):Start");
		}
		String siteId = null;	
		String errorMessage = Constants.CONSTANT_EMPTY;
		siteId = getSiteId();
		
		try{
			errorMessage = getResourceBundleTools().getKeyValue(pKeyName,siteId);
		} catch(RepositoryException re){
			throw new SystemException(IErrorId.UNABLE_TO_GET_THE_ITEM, re);
		}
		if(isLoggingDebug()){
			logDebug("ErrorHandlerManager - (getErrorMessage-[pErrorId]):End");
		}
		return errorMessage;		
	}
		
	/**
	 * This method will get all the keys
	 * @return lArraylist - Enumeration type
	 * @throws SystemException - when error occurs.
	 */
	public Enumeration<String> findAllKeys() throws SystemException {
		if(isLoggingDebug()){
			logDebug("ResourceBundleManager - (findAllKeys[]) : Start");
		}
		Enumeration<String> keys=null;
		try {
			keys = getResourceBundleTools().findAllKeys();
		} catch (RepositoryException re) {
			throw new SystemException(IErrorId.UNABLE_TO_LOAD_RESOURCE, re);
		}
		if(isLoggingDebug()){
			logDebug("ResourceBundleManager - (findAllKeys[]) : END");
		}
		return keys;
	}

}
