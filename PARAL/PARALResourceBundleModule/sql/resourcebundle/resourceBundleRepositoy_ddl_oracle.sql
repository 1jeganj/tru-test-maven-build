-- drop table ral_resourcebundle_xlate;
-- drop table ral_resourcebundle_key_xlate;
-- drop table ral_resourcebundle;

CREATE TABLE ral_resourcebundle ( 
	key_id varchar2(40) NOT NULL,
	key_name varchar2(254) NOT NULL,
	key_value varchar2(4000) NULL,
	constraint ral_resourcebundle_p PRIMARY KEY(key_id,key_name) );

CREATE TABLE ral_resourcebundle_key_xlate ( key_id varchar2(40) NOT NULL, locale varchar2(254) NOT NULL, translation_id varchar2(254) NOT NULL,constraint ral_resourcebundle_key_xlate_p primary key (key_id,locale),	constraint ral_resourcebundle_key_xlate_f foreign key (translation_id) references ral_resourcebundle_xlate (translation_id));

CREATE TABLE ral_resourcebundle_xlate (translation_id varchar2(254) NOT NULL, key_value varchar2(4000) NULL,constraint ral_resourcebundle_xlate_p PRIMARY KEY(translation_id) );