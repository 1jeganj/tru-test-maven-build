package com.tru.validator.cml;
/**
 * The class ValidatorConstants
 * @author Professional Access
 * @version 1.0
 */
public class ValidatorConstants {

	/** The Constant COM_PA_RAL_COMMON_CML_VALIDATIONEXCEPTIONS. */
	public static final String COM_PA_RAL_COMMON_CML_VALIDATIONEXCEPTIONS="com.tru.common.cml.ValidationExceptions";
	/** The Constant KEY_VALUE_ZERO. */
	public static final int KEY_VALUE_ZERO = 0;
	/** The Constant DOUBLE_ZERO. */
	public static final double DOUBLE_ZERO = 0.0;
	/** The Constant KEY_VALUE_ONE. */
	public static final int KEY_VALUE_ONE = 1;
	/** The Constant FIELDNAME. */
	public static final String FIELDNAME="fieldName";
	/** The Constant REQUIRED. */
	public static final String REQUIRED="required";
	/** The Constant AND. */
	public static final String AND="AND";
	/** The Constant FIELDJOIN. */
	public static final String FIELDJOIN="fieldJoin";
	/** The Constant FIELDO. */
	public static final String FIELDO="field[";
	/** The Constant CLOSE. */
	public static final String CLOSE="]";
	/** The Constant FIELDTESTO. */
	public static final String FIELDTESTO="fieldTest[";
	/** The Constant FIELDVALUEO. */
	public static final String FIELDVALUEO="fieldValue[";
	/** The Constant FIELDINDEXEDO. */
	public static final String FIELDINDEXEDO="fieldIndexed[";
	/** The Constant FALSE. */
	public static final String FALSE="false";
	/** The Constant TRUE. */
	public static final String TRUE="true";
	/** The Constant OPEN. */
	public static final String OPEN="[";
	/** The Constant INT_MINUS_ONE. */
	public static final int INT_MINUS_ONE=-1;
	/** The Constant DOT. */
	public static final String DOT=".";
	/** The Constant REQUIREDDIF. */
	public static final String REQUIREDDIF="requiredif";
	/** The Constant MASK. */
	public static final String MASK="mask";
	/** The Constant BYTE. */
	public static final String BYTE="byte";
	/** The Constant SHORT. */
	public static final String SHORT="short";
	/** The Constant INTEGER. */
	public static final String INTEGER="integer";
	/** The Constant LONG. */
	public static final String LONG="long";
	/** The Constant FLOAT. */
	public static final String FLOAT="float";
	/** The Constant DATE. */
	public static final String DATE="date";
	/** The Constant DOUBLE. */
	public static final String DOUBLE="double";
	/** The Constant DATEPATTERN. */
	public static final String DATEPATTERN="datePattern";
	/** The Constant DATEPATTERNSTRICT. */
	public static final String DATEPATTERNSTRICT="datePatternStrict";
	/** The Constant MIN. */
	public static final String MIN="min";
	/** The Constant MAX. */
	public static final String MAX="max";
	/** The Constant INTRANGE. */
	public static final String INTRANGE="intRange";
	/** The Constant MININTEGERVALUE. */
	public static final String MININTEGERVALUE="minIntegerValue";
	/** The Constant MINUS_ONE. */
	public static final float MINUS_ONE=-1;
	/** The Constant DOUBLERANGE. */
	public static final String DOUBLERANGE="doubleRange";
	/** The Constant MIN_DOUBLE_VALUE. */
	public static final String MIN_DOUBLE_VALUE="minDoubleValue";
	/** The Constant FLOAT_RANGE. */
	public static final String FLOAT_RANGE="floatRange";
	/** The Constant CREDIT_CARD. */
	public static final String CREDIT_CARD="creditCard";
	/** The Constant MAX_LENGTH. */
	public static final String MAX_LENGTH="maxlength";
	/** The Constant MIN_LENGTH. */
	public static final String MIN_LENGTH="minlength";
	/** The Constant EMAIL. */
	public static final String EMAIL="email";
	/** The Constant LONG_ZERO. */
	public static final long LONG_ZERO=0L;
	/** The Constant ALLOW_2_SLASHES. */
	public static final String ALLOW_2_SLASHES="allow2slashes";
	/** The Constant NO_FRAGMENTS. */
	public static final String NO_FRAGMENTS="nofragments";
	/** The Constant ALLOW_ALL_SCHEMES. */
	public static final String ALLOW_ALL_SCHEMES="allowallschemes";
	/** The Constant URL. */
	public static final String URL="url";
	/** The Constant COMMA. */
	public static final String COMMA = ",";
	/** The Constant SCHEMES. */
	public static final String SCHEMES="schemes";
}
