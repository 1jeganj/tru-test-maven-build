package com.tru.validator.fhl;

import com.tru.common.cml.SystemException;
import com.tru.common.cml.ValidationExceptions;

/**
 * The Interface IValidator.
 *
 * @version 1.0
 * @author Professional Access
 */
public interface IValidator {
	
	/**
	 * Validate.
	 *
	 * @param pFormName - String
	 * @param pObject - Object
	 * @throws SystemException the system exception
	 * @throws ValidationExceptions - ValidationExceptions.
	 */
	void validate(String pFormName, Object pObject)
	  throws SystemException, ValidationExceptions;

}