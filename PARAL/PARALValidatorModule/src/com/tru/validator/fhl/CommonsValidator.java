package com.tru.validator.fhl;

import java.util.StringTokenizer;

import org.apache.commons.validator.Arg;
import org.apache.commons.validator.Field;
import org.apache.commons.validator.GenericTypeValidator;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.Validator;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.commons.validator.util.ValidatorUtils;

import com.tru.common.cml.ValidationExceptions;
import com.tru.validator.cml.ValidatorConstants;

/**
 * The Class CommonsValidator.
 *
 * @version 1.0
 * @author Professional Access
 */
public class CommonsValidator {
	
	/** The Constant FIELD_TEST_NULL. */
	public static final String FIELD_TEST_NULL = "NULL";
	
	/** The Constant FIELD_TEST_NOTNULL. */
	public static final String FIELD_TEST_NOTNULL = "NOTNULL";
	
	/** The Constant FIELD_TEST_EQUAL. */
	public static final String FIELD_TEST_EQUAL = "EQUAL";
	
	/** The Constant FIELD_TEST_NOT_EQUAL. */
	public static final String FIELD_TEST_NOT_EQUAL = "NOT_EQUAL";
	
	/** declare private constructor. */
	private CommonsValidator(){}
	/**
	 * Adds the error.
	 *
	 * @param pField - Field
	 * @param pValExceptions - ValidationExceptions
	 * @param pValidationName - String
	 * @param pErrorId - String
	 */
	private static void addError(Field pField, ValidationExceptions pValExceptions, String pValidationName, String pErrorId)
	{addError(pField,pValExceptions, pValidationName, pErrorId, new Object[]{});}
	
	/**
	 * Adds the error.
	 *
	 * @param pField - Field
	 * @param pValExceptions - ValidationExceptions
	 * @param pValidationName - String
	 * @param pErrorId - String
	 * @param pMsgCntx - Object[]
	 */	
	private static void addError(Field pField, ValidationExceptions pValExceptions, String pValidationName, String pErrorId, Object[] pMsgCntx)
	{
		Object[] finalMsgCntx = new Object[pMsgCntx.length+ValidatorConstants.KEY_VALUE_ONE];
		Arg fieldName = pField.getArg(ValidatorConstants.FIELDNAME, 0);
		finalMsgCntx[0] = fieldName.getKey();
		System.arraycopy(pMsgCntx, ValidatorConstants.KEY_VALUE_ZERO, finalMsgCntx, ValidatorConstants.KEY_VALUE_ONE, pMsgCntx.length);
		pValExceptions.addValidationError(pErrorId, finalMsgCntx);
	}

	/**
	 * Checks if the field isn't null and length of the field is greater than
	 * zero not including whitespace.
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return true if meets stated requirements, false otherwise.
	 */
	public static boolean validateRequired(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		String value = null;
		boolean retVal = true;
		
		value = (isString(pBean))? (String) pBean: ValidatorUtils.getValueAsString(pBean, pField.getProperty());

		if (GenericValidator.isBlankOrNull(value))
		{
			addError(pField, pValExceptions, ValidatorConstants.REQUIRED, IValidatorErrorId.REQUIRED_FIELD_EMPTY);
			retVal = false;
		}
		return retVal;
	}

	/**
	 * Checks if the field isn't null based on the values of other fields.
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValidator the validator
	 * @param pValExceptions the val exceptions
	 * @return true if meets stated requirements, false otherwise.
	 */
	public static boolean validateRequiredIf(Object pBean, Field pField, Validator pValidator, ValidationExceptions pValExceptions)
	{
		Object form = pValidator.getParameterValue(org.apache.commons.validator.Validator.BEAN_PARAM);
		String value = null;
		boolean required = false;
		boolean retVal = true;

		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
		
		if (!GenericValidator.isBlankOrNull(value))
		{
			int i = 0;
			String fieldJoin = ValidatorConstants.AND;
			if (!GenericValidator.isBlankOrNull(pField.getVarValue(ValidatorConstants.FIELDJOIN)))
			{
				fieldJoin = pField.getVarValue(ValidatorConstants.FIELDJOIN);
			}
	
			if (fieldJoin.equalsIgnoreCase(ValidatorConstants.AND))
			{
				required = true;
			}
	
			while (!GenericValidator.isBlankOrNull(pField.getVarValue(ValidatorConstants.FIELDO + i + ValidatorConstants.CLOSE)))
			{
				String dependProp = pField.getVarValue(ValidatorConstants.FIELDO + i + ValidatorConstants.CLOSE);
				String dependTest = pField.getVarValue(ValidatorConstants.FIELDTESTO + i + ValidatorConstants.CLOSE);
				String dependTestValue = pField.getVarValue(ValidatorConstants.FIELDVALUEO + i + ValidatorConstants.CLOSE);
				String dependIndexed = pField.getVarValue(ValidatorConstants.FIELDINDEXEDO + i + ValidatorConstants.CLOSE);
	
				if (dependIndexed == null)
				{
					dependIndexed = ValidatorConstants.FALSE;
				}
	
				String dependVal = null;
				boolean thisRequired = false;
				if (pField.isIndexed() && dependIndexed.equalsIgnoreCase(ValidatorConstants.TRUE))
				{
					String key = pField.getKey();
					if ((key.indexOf(ValidatorConstants.OPEN) > ValidatorConstants.INT_MINUS_ONE) && (key.indexOf(ValidatorConstants.CLOSE)> ValidatorConstants.INT_MINUS_ONE))
					{
						String ind = key.substring(ValidatorConstants.KEY_VALUE_ZERO, key.indexOf(ValidatorConstants.DOT) + ValidatorConstants.KEY_VALUE_ONE);
						StringBuffer appendStr = new StringBuffer();
						dependProp = appendStr.append(ind).append(dependProp).toString();
					}
				}
	
				dependVal = ValidatorUtils.getValueAsString(form, dependProp);
				if (dependTest.equals(FIELD_TEST_NULL))
				{
					thisRequired = ((dependVal != null) && (dependVal.length() > 0))?false:true;
				}
	
				if (dependTest.equals(FIELD_TEST_NOTNULL))
				{
					thisRequired = ((dependVal != null) && (dependVal.length() > 0))?true:false;
				}
	
				if (dependTest.equals(FIELD_TEST_EQUAL))
				{
					thisRequired = dependTestValue.equalsIgnoreCase(dependVal);
				}
	
				if (fieldJoin.equalsIgnoreCase(ValidatorConstants.AND))
				{
					required = required && thisRequired;
				}
				else
				{
					required = required || thisRequired;
				}
	
				i++;
			}
	
			if (!required)
			{
				addError(pField, pValExceptions, ValidatorConstants.REQUIREDDIF, IValidatorErrorId.REQUIRED_IF_FAILED);
				retVal = false;
			}
		}
		return retVal;
	}

	/**
	 * Checks if the field matches the regular expression in the field's mask
	 * attribute.
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return true if field matches mask, false otherwise.
	 */
	public static boolean validateMask(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		String mask = pField.getVarValue(ValidatorConstants.MASK);
		String value = null;
		boolean retVal = true;
		
		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
		
		if ( !GenericValidator.isBlankOrNull(value) && !GenericValidator.matchRegexp(value, mask))
	
			{
				addError(pField, pValExceptions, ValidatorConstants.MASK, IValidatorErrorId.INVALID_FORMAT);
				retVal = false;
			}
		
		return retVal;
	}

	/**
	 * Checks if the field can safely be converted to a byte primitive.
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return true if valid, false otherwise.
	 */
	public static boolean validateByte(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		String value = null;
		boolean retVal = true;
		
		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
		
		if (!GenericValidator.isBlankOrNull(value) && GenericTypeValidator.formatByte(value) == null)

			{
				addError(pField, pValExceptions, ValidatorConstants.BYTE, IValidatorErrorId.INVALID_BYTE);
				retVal = false;
			}
		
		return retVal;
	}

	/**
	 * Checks if the field can safely be converted to a short primitive.
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return true if valid, false otherwise.
	 */
	public static boolean validateShort(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		String value = null;
		boolean retVal = true;

		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
		
		if (!GenericValidator.isBlankOrNull(value) && GenericTypeValidator.formatShort(value) == null)	
			{
				addError(pField, pValExceptions, ValidatorConstants.SHORT, IValidatorErrorId.INVALID_SHORT);
				retVal = false;
			}
		return retVal;
	}

	/**
	 * Checks if the field can safely be converted to an int primitive.
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return true if valid, false otherwise.
	 */
	public static boolean validateInteger(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		String value = null;
		boolean retVal = true;
		
		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
		
		if (!GenericValidator.isBlankOrNull(value) && GenericTypeValidator.formatInt(value) == null)		
			{
				addError(pField, pValExceptions, ValidatorConstants.INTEGER, IValidatorErrorId.INVALID_INTEGER);
				retVal = false;
			}
		
		return retVal;
	}

	/**
	 * Checks if the field can safely be converted to a long primitive.
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return true if valid, false otherwise.
	 */
	public static boolean validateLong(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		String value = null;
		boolean retVal = true;

		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
		
		if ((!GenericValidator.isBlankOrNull(value)) && (GenericTypeValidator.formatLong(value) == null))
			{
				addError(pField, pValExceptions, ValidatorConstants.LONG, IValidatorErrorId.INVALID_LONG);
				retVal = false;
			}
		
		return retVal;
	}

	/**
	 * Checks if the field can safely be converted to a float primitive.
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return true if valid, false otherwise.
	 */
	public static boolean validateFloat(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		String value = null;
		boolean retVal = true;
		
		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
		
		if ((!GenericValidator.isBlankOrNull(value)) && (GenericTypeValidator.formatFloat(value)==null))
			{
				addError(pField, pValExceptions, ValidatorConstants.FLOAT, IValidatorErrorId.INVALID_FLOAT);
				retVal = false;
			}
		return retVal;
	}

	/**
	 * Checks if the field can safely be converted to a double primitive.
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return true if valid, false otherwise.
	 */
	public static boolean validateDouble(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		String value = null;
		boolean retVal = true;

		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
		
		if ((!GenericValidator.isBlankOrNull(value)) && (GenericTypeValidator.formatDouble(value) == null))

			{
				addError(pField, pValExceptions, ValidatorConstants.DOUBLE, IValidatorErrorId.INVALID_DOUBLE);
				retVal = false;
			}
		return retVal;
	}

	/**
	 * Checks if the field is a valid date. If the field has a datePattern
	 * variable, that will be used to format
	 * <code>java.text.SimpleDateFormat</code>. If the field has a
	 * datePatternStrict variable, that will be used to format
	 * <code>java.text.SimpleDateFormat</code> and the length will be checked
	 * so '2/12/1999' will not pass validation with the format 'MM/dd/yyyy'
	 * because the month isn't two digits. If no datePattern variable is
	 * specified, then the field gets the DateFormat.SHORT format for the
	 * locale. The setLenient method is set to <code>false</code> for all
	 * variations.
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return true if valid, false otherwise.
	 */
	public static boolean validateDate(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		Object result = null;
		String value = null;
		boolean retVal = true;
		
		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
		
		String datePattern = pField.getVarValue(ValidatorConstants.DATEPATTERN);
		String datePatternStrict = pField.getVarValue(ValidatorConstants.DATEPATTERNSTRICT);
		// Locale locale = RequestUtils.getUserLocale(null, null);
		boolean datePatternStrictBol = (!GenericValidator.isBlankOrNull(value)&& 
								datePatternStrict.equalsIgnoreCase(ValidatorConstants.TRUE))?true:false;

		if (!GenericValidator.isBlankOrNull(value))
		{
			if (datePattern != null && datePattern.length() > 0)
			{
				result = GenericTypeValidator.formatDate(value, datePattern, datePatternStrictBol);
			}
			//else if (datePatternStrict != null && datePatternStrict.length() > 0)
			//{
			//	result = GenericTypeValidator.formatDate(value, datePatternStrict, true);
			//}
			// else {
			// result = GenericTypeValidator.formatDate(value, locale);
			// }
			
			if (result == null)
			{
				addError(pField, pValExceptions, ValidatorConstants.DATE, IValidatorErrorId.INVALID_DATE);
				retVal = false;
			}
		}
		return retVal;
	}

	/**
	 * Checks if a fields value is within a range (min &amp; max specified in
	 * the vars attribute).
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return True if in range, false otherwise.
	 */
	public static boolean validateIntRange(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		String value = null;
		boolean retVal = true;
		int min = ValidatorConstants.INT_MINUS_ONE;
		int max = 0;
		
		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
		
		if (!GenericValidator.isBlankOrNull(value) && (GenericTypeValidator.formatInt(value) != null))
		{
			int intValue = Integer.parseInt(value);
			String minValStr = pField.getVarValue(ValidatorConstants.MIN);
			String maxValStr = pField.getVarValue(ValidatorConstants.MAX);
				
			if(!GenericValidator.isBlankOrNull(minValStr) && (GenericTypeValidator.formatInt(minValStr) != null))
			{
				min = Integer.parseInt(minValStr);
			}
			
			if(!GenericValidator.isBlankOrNull(maxValStr) && (GenericTypeValidator.formatInt(maxValStr) != null))
			{
				max = Integer.parseInt(maxValStr);
			}
			
			if (!GenericValidator.isInRange(intValue, min, max))
			{
				addError(pField, pValExceptions, ValidatorConstants.INTRANGE, IValidatorErrorId.INVALID_RANGE, new Object[] { Integer.valueOf(min), Integer.valueOf(max)});
				retVal = false;
			}
		}
		return retVal;
	}

	/**
	 * Validate min int value.
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return true, if successful
	 */
	public static boolean validateMinIntValue(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		String value = null;
		boolean retVal = true;
		int min = ValidatorConstants.INT_MINUS_ONE;

		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
		
		if (!GenericValidator.isBlankOrNull(value) && (GenericTypeValidator.formatInt(value) != null))
		{
			int intValue = Integer.parseInt(value);
			String minValStr = pField.getVarValue(ValidatorConstants.MIN);
				
			if(!GenericValidator.isBlankOrNull(minValStr) && (GenericTypeValidator.formatDouble(minValStr) != null))
			{
				min = Integer.parseInt(minValStr);
			}
			
			if (!GenericValidator.minValue(intValue, min))
			{
				addError(pField, pValExceptions, ValidatorConstants.MININTEGERVALUE, IValidatorErrorId.INVALID_MIN_INTEGER, new Object[] { Integer.valueOf(min)});
				retVal = false;
			}
		}
		return retVal;
	}


	/**
	 * Checks if a fields value is within a range (min &amp; max specified in
	 * the vars attribute).
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return True if in range, false otherwise.
	 */
	public static boolean validateDoubleRange(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		String value = null;
		boolean retVal = true;
		double min = ValidatorConstants.MINUS_ONE;
		double max = ValidatorConstants.DOUBLE_ZERO;

		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
		
		if (!GenericValidator.isBlankOrNull(value) && (GenericTypeValidator.formatDouble(value) != null))
		{
			double doubleValue = Double.parseDouble(value);
			String minValStr = pField.getVarValue(ValidatorConstants.MIN);
			String maxValStr = pField.getVarValue(ValidatorConstants.MAX);
				
			if(!GenericValidator.isBlankOrNull(minValStr) && (GenericTypeValidator.formatDouble(minValStr) != null))
			{
				min = Double.parseDouble(minValStr);
			}
			
			if(!GenericValidator.isBlankOrNull(maxValStr) && (GenericTypeValidator.formatInt(maxValStr) != null))
			{
				max = Double.parseDouble(maxValStr);
			}
				
			if (!GenericValidator.isInRange(doubleValue, min, max))
			{
				addError(pField, pValExceptions, ValidatorConstants.DOUBLERANGE, IValidatorErrorId.INVALID_RANGE, new Object[] { new Double(min), new Double(max) });
				retVal = false;
			}
		}
		return retVal;
	}

	/**
	 * Validate min double value.
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return true, if successful
	 */
	public static boolean validateMinDoubleValue(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		String value = null;
		boolean retVal = true;
		double min = ValidatorConstants.MINUS_ONE;

		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
		
		if (!GenericValidator.isBlankOrNull(value) && (GenericTypeValidator.formatDouble(value) != null))
		{
			double doubleValue = Double.parseDouble(value);
			String minValStr = pField.getVarValue(ValidatorConstants.MIN);
				
			if(!GenericValidator.isBlankOrNull(minValStr) && (GenericTypeValidator.formatDouble(minValStr) != null))
			{
				min = Double.parseDouble(minValStr);
			}
			
			if (!GenericValidator.minValue(doubleValue, min))
			{
				addError(pField, pValExceptions, ValidatorConstants.MIN_DOUBLE_VALUE, IValidatorErrorId.INVALID_MIN_DOUBLE, new Object[] { new Double(min)});
				retVal = false;
			}
		}
		return retVal;
	}

	
	/**
	 * Checks if a fields value is within a range (min &amp; max specified in
	 * the vars attribute).
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return True if in range, false otherwise.
	 */
	public static boolean validateFloatRange(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		String value = null;
		boolean retVal = true;
		float min = ValidatorConstants.MINUS_ONE;
		float max = 0;
		
		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
		
		if (!GenericValidator.isBlankOrNull(value) && (GenericTypeValidator.formatFloat(value) != null))
		{
			float floatValue = Float.parseFloat(value);
			String minValStr = pField.getVarValue(ValidatorConstants.MIN);
			String maxValStr = pField.getVarValue(ValidatorConstants.MAX);
				
			if(!GenericValidator.isBlankOrNull(minValStr) && (GenericTypeValidator.formatFloat(minValStr) != null))
			{
				min = Float.parseFloat(minValStr);
			}
			
			if(!GenericValidator.isBlankOrNull(maxValStr) && (GenericTypeValidator.formatFloat(maxValStr) != null))
			{
				max = Float.parseFloat(maxValStr);
			}
			
			if (!GenericValidator.isInRange(floatValue, min, max))
			{
				addError(pField, pValExceptions, ValidatorConstants.FLOAT_RANGE, IValidatorErrorId.INVALID_RANGE, new Object[] { new Float(min), new Float(max)});
				retVal = false;
			}
		}
		return retVal;
	}

	/**
	 * Checks if the field is a valid credit card number.
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return true if valid, false otherwise.
	 */
	public static boolean validateCreditCard(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		String value = null;
		boolean retVal = true;
		
		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
				
		if(!GenericValidator.isBlankOrNull(value) && GenericTypeValidator.formatCreditCard(value) == null)
		{
			addError(pField, pValExceptions, ValidatorConstants.CREDIT_CARD, IValidatorErrorId.INVALID_CREDIT_CARD_NUMBER, new Object[] {});
			retVal = false;
		}		
		return retVal;
	}

	/**
	 * Checks if the field's length is less than or equal to the maximum value.
	 * A <code>Null</code> will be considered an error.
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return True if stated conditions met.
	 */
	public static boolean validateMaxLength(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		String value = null;
		int maxlength = 0;
		boolean retVal = true;
		
		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
		
		if (!GenericValidator.isBlankOrNull(value))
		{
			String maxLengthStr = pField.getVarValue(ValidatorConstants.MAX_LENGTH);
			
			if(!GenericValidator.isBlankOrNull(maxLengthStr) && (GenericTypeValidator.formatInt(maxLengthStr) != null))
			{
				maxlength = Integer.parseInt(maxLengthStr);
			}
			
			if (!GenericValidator.maxLength(value, maxlength))
			{
				addError(pField, pValExceptions, ValidatorConstants.MAX_LENGTH, IValidatorErrorId.INVALID_MAX_LENGTH, new Object[] { Integer.valueOf(maxlength) });
				retVal = false;
			}
		}

		return retVal;
	}

	/**
	 * Checks if the field's length is greater than or equal to the minimum
	 * value. A <code>Null</code> will be considered an error.
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return True if stated conditions met.
	 */
	public static boolean validateMinLength(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		String value = null;
		int minlength = 0;
		boolean retVal = true;
		
		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
		
		if (!GenericValidator.isBlankOrNull(value))
		{
			String minLengthStr = pField.getVarValue(ValidatorConstants.MIN_LENGTH);
			
			if(!GenericValidator.isBlankOrNull(minLengthStr) && (GenericTypeValidator.formatInt(minLengthStr) != null))
			{
				minlength = Integer.parseInt(minLengthStr);
			}
			if (!GenericValidator.minLength(value, minlength))
			{
				addError(pField, pValExceptions, ValidatorConstants.MIN_LENGTH, IValidatorErrorId.INVALID_MIN_LENGTH, new Object[] { Integer.valueOf(minlength) });
				retVal = false;
			}
		}
		return retVal;
	}

	/**
	 * Checks if a field has a valid e-mail address.
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return True if valid, false otherwise.
	 */
	public static boolean validateEmail(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{
		String value = null;
		boolean retVal = true;
		
		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());
		
		if (!GenericValidator.isBlankOrNull(value) && !GenericValidator.isEmail(value)){
				addError(pField, pValExceptions, ValidatorConstants.EMAIL, IValidatorErrorId.INVALID_EMAIL_ADDRESS_FORMAT, new Object[] {});
				retVal = false;			
		}
		return retVal;
	}

	/**
	 * Checks if a field has a valid url. Four optional variables can be
	 * specified to configure url validation.
	 * <ul>
	 * <li>Variable <code>allow2slashes</code> can be set to
	 * <code>true</code> or <code>false</code> to control whether two
	 * slashes are allowed - default is <code>false</code> (i.e. two slashes
	 * are NOT allowed).</li>
	 * <li>Variable <code>nofragments</code> can be set to <code>true</code>
	 * or <code>false</code> to control whether fragments are allowed -
	 * default is <code>false</code> (i.e. fragments ARE allowed).</li>
	 * <li>Variable <code>allowallschemes</code> can be set to
	 * <code>true</code> or <code>false</code> to control if all schemes are
	 * allowed - default is <code>false</code> (i.e. all schemes are NOT
	 * allowed).</li>
	 * <li>Variable <code>schemes</code> can be set to a comma delimited list
	 * of valid schemes. This value is ignored if <code>allowallschemes</code>
	 * is set to <code>true</code>. Default schemes allowed are "http",
	 * "https" and "ftp" if this variable is not specified.</li>
	 * </ul>
	 *
	 * @param pBean the bean
	 * @param pField the field
	 * @param pValExceptions the val exceptions
	 * @return True if valid, false otherwise.
	 */
	public static boolean validateUrl(Object pBean, Field pField, ValidationExceptions pValExceptions)
	{

		String value = null;
		boolean retVal = true;
		
		value = (isString(pBean))?(String) pBean:ValidatorUtils.getValueAsString(pBean, pField.getProperty());

		if (!GenericValidator.isBlankOrNull(value))
		{
			// Get the options and schemes Vars
			boolean allowallschemes = (ValidatorConstants.TRUE).equalsIgnoreCase(pField.getVarValue(ValidatorConstants.ALLOW_ALL_SCHEMES));
			long options = allowallschemes ? UrlValidator.ALLOW_ALL_SCHEMES : ValidatorConstants.LONG_ZERO;
	
			if ((ValidatorConstants.TRUE).equalsIgnoreCase(pField.getVarValue(ValidatorConstants.ALLOW_2_SLASHES)))
			{
				options += UrlValidator.ALLOW_2_SLASHES;
			}
	
			if ((ValidatorConstants.TRUE).equalsIgnoreCase(pField.getVarValue(ValidatorConstants.NO_FRAGMENTS)))
			{
				options += UrlValidator.NO_FRAGMENTS;
			}
	
			String schemesVar = allowallschemes ? null : pField.getVarValue(ValidatorConstants.SCHEMES);
	
			// No options or schemes - use GenericValidator as default
			if (options == 0 && schemesVar == null)
			{
				if (!GenericValidator.isUrl(value))
				{
					addError(pField, pValExceptions, ValidatorConstants.URL, IValidatorErrorId.INVALID_URL, new Object[] {});
					retVal = false;
				}
			}
			else
			{
				// Parse comma delimited list of schemes into a String[]
				String[] schemes = null;
				if (schemesVar != null)
				{
					StringTokenizer st = new StringTokenizer(schemesVar, ValidatorConstants.COMMA);
					schemes = new String[st.countTokens()];
		
					int i = 0;
					while (st.hasMoreTokens())
					{
						schemes[i++] = st.nextToken().trim();
					}
		
				}
		
				// Create UrlValidator and validate with options/schemes
				UrlValidator urlValidator = new UrlValidator(schemes, options);
				if (!urlValidator.isValid(value))
				{
					addError(pField, pValExceptions, ValidatorConstants.URL, IValidatorErrorId.INVALID_URL, new Object[] {});
					retVal = false;
				}
			}
		}
		return retVal;
	}

	/**
	 * Return <code>true</code> if the specified object is a String or a
	 * <code>null</code> value.
	 * 
	 * @param pObj - Object to be tested
	 * @return The string value
	 */
	protected static boolean isString(Object pObj)
	{
		return (pObj == null) ? true : String.class.isInstance(pObj);
	}
}