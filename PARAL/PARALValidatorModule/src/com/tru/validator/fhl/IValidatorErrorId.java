package com.tru.validator.fhl;

/**
 * This class contains contants for error id.
 * @author Professional Access
 * @version 1.0
 *
 */
public class IValidatorErrorId {
	
	/** The Constant REQUIRED_FIELD_EMPTY. */
	public static final String REQUIRED_FIELD_EMPTY = "required_field_empty";
	
	/** The Constant REQUIRED_IF_FAILED. */
	public static final String REQUIRED_IF_FAILED = "required_if_failed";
	
	/** The Constant INVALID_FORMAT. */
	public static final String INVALID_FORMAT = "invalid_format";
	
	/** The Constant INVALID_BYTE. */
	public static final String INVALID_BYTE = "invalid_byte";
	
	/** The Constant INVALID_SHORT. */
	public static final String INVALID_SHORT = "invalid_short";
	
	/** The Constant INVALID_INTEGER. */
	public static final String INVALID_INTEGER = "invalid_integer";
	
	/** The Constant INVALID_MIN_INTEGER. */
	public static final String INVALID_MIN_INTEGER = "invalid_min_integer";
	
	/** The Constant INVALID_LONG. */
	public static final String INVALID_LONG = "invalid_long";
	
	/** The Constant INVALID_FLOAT. */
	public static final String INVALID_FLOAT = "invalid_float";
	
	/** The Constant INVALID_DOUBLE. */
	public static final String INVALID_DOUBLE = "invalid_double";
	
	/** The Constant INVALID_MIN_DOUBLE. */
	public static final String INVALID_MIN_DOUBLE = "invalid_min_double";
	
	/** The Constant INVALID_DATE. */
	public static final String INVALID_DATE = "invalid_date";
	
	/** The Constant INVALID_RANGE. */
	public static final String INVALID_RANGE = "invalid_range";
	
	/** The Constant INVALID_CREDIT_CARD_NUMBER. */
	public static final String INVALID_CREDIT_CARD_NUMBER = "invalid_credit_card_number";
	
	/** The Constant INVALID_EMAIL_ADDRESS_FORMAT. */
	public static final String INVALID_EMAIL_ADDRESS_FORMAT = "invalid_email_address_format";
	
	/** The Constant INVALID_MAX_LENGTH. */
	public static final String INVALID_MAX_LENGTH = "invalid_max_length";
	
	/** The Constant INVALID_MIN_LENGTH. */
	public static final String INVALID_MIN_LENGTH = "invalid_min_length";
	
	/** The Constant INVALID_URL. */
	public static final String INVALID_URL = "invalid_url";
	
}
