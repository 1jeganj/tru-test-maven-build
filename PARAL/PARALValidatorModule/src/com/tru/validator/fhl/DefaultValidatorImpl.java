package com.tru.validator.fhl;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.validator.Validator;
import org.apache.commons.validator.ValidatorException;
import org.apache.commons.validator.ValidatorResources;
import org.xml.sax.SAXException;

import com.tru.common.cml.AbstractComponent;
import com.tru.common.cml.Constants;
import com.tru.common.cml.IErrorId;
import com.tru.common.cml.SystemException;
import com.tru.common.cml.ValidationExceptions;
import com.tru.validator.cml.ValidatorConstants;

/**
 * The Class DefaultValidatorImpl.
 *
 * @version 1.0
 * @author Professional Access
 */
public class DefaultValidatorImpl extends AbstractComponent implements IValidator {
	
	/** The m validatio n_ exceptio n_ classname. */
	protected final String mVALIDATION_EXCEPTION_CLASSNAME = ValidatorConstants.COM_PA_RAL_COMMON_CML_VALIDATIONEXCEPTIONS;
	
	/** The m validation files. */
	protected List<String> mValidationFiles;
	
	/** The m validator resources. */
	protected ValidatorResources mValidatorResources;


	/**
	 * Initcomponent.
	 *
	 * @throws SystemException - when error occurs
	 */
	protected void initComponent() throws SystemException
	{
		try
		{
			InputStream inp[] = new InputStream[mValidationFiles.size()];
			for (int i = 0; i < inp.length; i++)
			{
				inp[i] = loadResource((String) mValidationFiles.get(i));
			}

			mValidatorResources = new ValidatorResources(inp);
		}
		catch (IOException e)
		{
			throw new SystemException(IErrorId.COMPONENT_INITIALIZATION_ERROR, new Object[] {Constants.COMPONENT_VALIDATOR}, e);
		}
		catch (SAXException e)
		{
			throw new SystemException(IErrorId.COMPONENT_INITIALIZATION_ERROR, new Object[] {Constants.COMPONENT_VALIDATOR}, e);
		}

	}

	/**
	 * Validate.
	 *
	 * @param pFormName - String
	 * @param pObject - Object
	 * @throws SystemException - when error occurs
	 * @throws ValidationExceptions - when error occurs
	 */
	public void validate(String pFormName, Object pObject)
	  throws SystemException, ValidationExceptions
	{
		try
		{
			ValidationExceptions valExceptions = new ValidationExceptions();
			Validator validator = new Validator(mValidatorResources, pFormName);
			validator.setParameter(Validator.BEAN_PARAM, pObject);
			validator.setParameter(mVALIDATION_EXCEPTION_CLASSNAME, valExceptions);
			validator.validate();
			if (valExceptions.hasValidationErrors()){
				throw valExceptions;}
		}
		catch (ValidatorException e)
		{
			throw new SystemException(IErrorId.VALIDATOR_ERROR, new Object[] { pFormName }, e);
		}
	}

	/**
	 * Sets the validation files.
	 *
	 * @param pValidationFiles - List
	 */
	public void setValidationFiles(List<String> pValidationFiles)
	{
		this.mValidationFiles = pValidationFiles;
	}

}