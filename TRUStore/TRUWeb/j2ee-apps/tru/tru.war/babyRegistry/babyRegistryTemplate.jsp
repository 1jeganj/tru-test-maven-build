<dsp:page>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<dsp:getvalueof var="contextPath"	value="${originatingRequest.contextPath}" />
 
<dsp:getvalueof var="giftFinderURL" bean="Profile.giftFinderURL"/>

<div class="offcanvas baby-registry-menu sub-category">
    <div class="tse-scrollable gift-finder-wrapper registry">
        <div class="tse-content">
            <div class="baby-registry">
                <div class="baby-registry-content">
                    <div class="baby-registry-hero">
                        <div class="baby-registry-logo"></div>
                        <div class="baby-registry-main-text">
                            You can count on "Us" to help you find everything to get you and your baby off to a happy start.
                        </div>
                    </div>
                    <hr class="horizontal-divider">
                    <div class="baby-registry-container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="baby-registry-find">
                                    <div class="baby-registry-find-header">
                                        find a <span>registry</span>
                                    </div>
                                    <div class="baby-registry-find-name">
                                        first name
                                        <input type="text" name="bru-registry-fname">
                                    </div>
                                    <br>
                                    <div class="baby-registry-find-name">
                                        last name
                                        <input type="text" name="bru-registry-lname">
                                    </div>
                                    <div class="baby-registry-find-submit">
                                        <button>find</button>
                                        <a href="#" class="pull-right baby-registry-advanced-search">advanced search</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="baby-registry-create">
                                    <div class="baby-registry-create-header">
                                        create a <span>registry</span>
                                    </div>
                                    <div class="baby-registry-create-text">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam egestas eleifend sapien nec pellentesque.
                                    </div>
                                    <div class="baby-registry-create-submit">
                                        <button>create</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="baby-registry-update">
                                    <div class="baby-registry-update-header">
                                        update a <span>registry</span>
                                    </div>
                                    <div class="baby-registry-update-text">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam egestas eleifend sapien nec pellentesque.
                                    </div>
                                    <div class="baby-registry-update-submit">
                                        <button>update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="horizontal-divider">
                    <div class="baby-registry-footer">
                        <a href="./template-sub-category.html">
                            <div class="continue-shopping-left-arrow inline"></div>back to registry
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="fade-to-black-2"></div>

</dsp:page>