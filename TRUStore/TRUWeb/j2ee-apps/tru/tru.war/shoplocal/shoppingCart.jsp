<dsp:page>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/commerce/cart/droplet/ShopLocalDroplet"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
<dsp:droplet name="ShopLocalDroplet">
	<dsp:oparam name="output">
		<dsp:getvalueof var="shopLocalList" param="shopLocalList" />
			<dsp:droplet name="ForEach">
				<dsp:param name="array" param="shopLocalList"/>
				<dsp:param name="elementName" value="item"/>
				<dsp:param name="indexName" value="itemIndex"/>
				<dsp:oparam name="outputStart">
				    <dsp:setvalue bean="CartModifierFormHandler.addItemCount" paramvalue="size"/>
				    <dsp:getvalueof var="shopLocalRequest" vartype="java.lang.string" value="shopLocal" />
				    <dsp:setvalue bean="CartModifierFormHandler.shopLocalRequest" value="${shopLocalRequest}"/>
				</dsp:oparam>
				<dsp:oparam name="output">
					<dsp:getvalueof var="item" param="item" />
					<dsp:setvalue bean="CartModifierFormHandler.items[param:itemIndex].quantity" value="${item.quantity}"/>
					<dsp:setvalue bean="CartModifierFormHandler.items[param:itemIndex].productId" value="${item.productId}" />
					<dsp:setvalue bean="CartModifierFormHandler.items[param:itemIndex].catalogRefId" value="${item.skuId}" /> 
				</dsp:oparam>
			</dsp:droplet>
			<dsp:setvalue bean="CartModifierFormHandler.addItemToOrderSuccessURL" value="${contextPath}cart/shoppingCart.jsp" />
			<dsp:setvalue bean="CartModifierFormHandler.addItemToOrderErrorURL" value="${contextPath}cart/shoppingCart.jsp" />
			<dsp:setvalue bean="CartModifierFormHandler.addItemToOrder" value="submit" />
	</dsp:oparam>
</dsp:droplet>
</dsp:page>