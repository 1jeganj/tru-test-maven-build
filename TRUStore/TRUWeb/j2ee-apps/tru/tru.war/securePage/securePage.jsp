<html>	
<body>
<script>
window.addEventListener('message', function(event) {
    // Domain restriction (to not leak variables to any page..)
    console.log(event.origin, event.data);
    if (event.origin.indexOf('toysrus') > -1 || event.origin.indexOf('babiesrus')) {
    	localStorage.setItem(event.data.key, event.data.data);
    }else{
		console.debug('Error: Unknown Origin');
	}
});
</script>
</body>
</html>