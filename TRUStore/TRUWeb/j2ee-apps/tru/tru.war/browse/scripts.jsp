<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:importbean bean="com/tru/common/TRUStoreConfiguration"/>
	<dsp:getvalueof var="staticAssetsVersionFormat" bean="TRUStoreConfiguration.staticAssetsVersion"/>
	<dsp:getvalueof var="versionFlag" bean="TRUStoreConfiguration.versioningFlag"/>
	<dsp:getvalueof value='<%=atg.nucleus.DynamoEnv.getProperty("staticAssetVersion")%>' var="versionNumbers"/>
	
	<c:choose>
		<c:when test="${versionFlag}">
			<link rel="stylesheet" 
				href="${TRUCSSPath}css/productDetails.css?${staticAssetsVersionFormat}=${versionNumbers}">
			<script type="text/javascript" charset="UTF-8" 
				src="${TRUJSPath}javascript/productDetails.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
		</c:when>
		<c:otherwise>
			<link rel="stylesheet" href="${TRUCSSPath}css/productDetails.css">
			<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/productDetails.js"></script>
		</c:otherwise>
	</c:choose>
</dsp:page>