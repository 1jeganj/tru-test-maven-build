<dsp:page>
<dsp:importbean bean="/com/tru/integrations/common/TRUHookLogicConfiguration"/>
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:getvalueof var="hooklogicURL" bean="TRUHookLogicConfiguration.hooklogicURL"/>
 <dsp:getvalueof var="hlptorder" bean="TRUHookLogicConfiguration.hlptOrder"/>
 
  <dsp:getvalueof var="sku" param="hookLogicDetails.sku"/>
 <dsp:getvalueof var="price" param="hookLogicDetails.price"/>
 <dsp:getvalueof var="quantity" param="hookLogicDetails.quantity"/>
 <dsp:getvalueof var="orderId" param="hookLogicDetails.orderId"/>
  <dsp:getvalueof var="parentSku" param="hookLogicDetails.parentSku"/>
<dsp:getvalueof var="regularPrice" param="hookLogicDetails.regularPrice"/>
 <dsp:getvalueof var="orderSubTotal" param="hookLogicDetails.orderSubTotal"/>
 <dsp:getvalueof var="pubPageType" bean="TRUHookLogicConfiguration.orderpubPageType"/>
 <dsp:getvalueof var="addId" bean="TRUHookLogicConfiguration.addId"/>
 <dsp:getvalueof var="customerId" bean="Profile.id"/>
 <dsp:getvalueof var="loginStatus" bean="Profile.transient" />
 <c:if test="${loginStatus eq 'true'}">
		<c:set var="customerId" value=""/>
	</c:if>
	

    <div id="hl_order"> </div>
		<script type="text/javascript">
			$.getScript("${hooklogicURL}",function() {
				HLLibrary.newOrder()
				 .setProperty("cUserId", "${customerId}")
				 .setProperty("hlPageType","${hlptorder}")
		         .setProperty("parentSku","${parentSku}")
		         .setProperty("sku","${sku}")
		         .setProperty("price","${price}")
		         .setProperty("quantity","${quantity}")
		         .setProperty("orderId","${orderId}")
		         .setProperty("regularPrice", "${regularPrice}") 
				 .setProperty("orderSubTotal", "${orderSubTotal}") 
				 .setProperty("pubPageType", "${pubPageType}") 
				 .setProperty("addId","${addId}")
		         .submit();
			});
			
		</script>

</dsp:page>



