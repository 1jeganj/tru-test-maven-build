<dsp:page>
<dsp:importbean bean="/com/tru/integrations/common/TRUHookLogicConfiguration"/>
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:getvalueof var="hooklogicURL" bean="TRUHookLogicConfiguration.hooklogicURL"/>
 <dsp:getvalueof var="taxonomy" bean="TRUHookLogicConfiguration.taxonomy"/>
 <dsp:getvalueof var="hlpt" bean="TRUHookLogicConfiguration.hlpt"/>
 <dsp:getvalueof var="minorganic" bean="TRUHookLogicConfiguration.minorganic"/>
 <dsp:getvalueof var="minads" bean="TRUHookLogicConfiguration.minads"/>
 <dsp:getvalueof var="maxads" bean="TRUHookLogicConfiguration.maxads"/>
 <dsp:getvalueof var="pcount" bean="TRUHookLogicConfiguration.pcount"/>
 <dsp:getvalueof var="pgsize" bean="TRUHookLogicConfiguration.pgsize"/>
 <dsp:getvalueof var="pgn" bean="TRUHookLogicConfiguration.pgn"/>
 <dsp:getvalueof var="sort" bean="TRUHookLogicConfiguration.sort"/>
 <dsp:getvalueof var="customerId" bean="Profile.id"/>
 <dsp:getvalueof var="loginStatus" bean="Profile.transient" />
 <c:if test="${loginStatus eq 'true'}">
		<c:set var="customerId" value=""/>
 </c:if>
 <dsp:getvalueof var="categoryid" param="categoryId" />
  <dsp:getvalueof var="hpageType" param="hpageType" />
  <dsp:getvalueof var="search" param="search" />
  <c:if test="${search != null}">
    <c:set var="hlpt" value="S"/>
    <c:set var="hpageType" value="${search}"/>
    <dsp:getvalueof var="keyword" param="keyword" />
  </c:if>

	<div id="hl_1"> </div>
	<c:if test="${hpageType ne 'search' }">
	<script type="text/javascript">
		$(window).load(function(){
					$.getScript( "${hooklogicURL}" )
		.done(function( script, textStatus ) {
	  			HLLibrary.newRequest()
				 	 .setModule("replatform")
			         .setTaxonomy("${categoryid}")
			         .setProperty("cUserId", "${customerId}")
			         .setProperty("hlPageType","${hlpt}")
			         .setProperty("pubpagetype","${hpageType}")
			         .setProperty("organicSKUs","${minorganic}")
			         .setProperty("minads","${minads}")
			         .setProperty("maxads","${maxads}")
			         .setProperty("pcount","${pcount}")
			         .setProperty("pgsize","${pgsize}")
			         .setProperty("pgn","${pgn}")
			         .setProperty("sort","${sort}")
			         .submit("hl_1");
  });
		});
		</script>
	</c:if>
	<c:if test="${hpageType eq 'search' }">
	<script type="text/javascript">
		$(window).load(function(){
					$.getScript( "${hooklogicURL}" )
		.done(function( script, textStatus ) {
	  			HLLibrary.newRequest()
				 	 .setModule("replatform")
			         .setTaxonomy("${categoryid}")
			         .setProperty("cUserId", "${customerId}")
			         .setProperty("hlPageType","${hlpt}")
			         .setKeyword("${keyword}")
			         .setProperty("pubpagetype","${hpageType}")
			         .setProperty("organicSKUs","${minorganic}")
			         .setProperty("minads","${minads}")
			         .setProperty("maxads","${maxads}")
			         .setProperty("pcount","${pcount}")
			         .setProperty("pgsize","${pgsize}")
			         .setProperty("pgn","${pgn}")
			         .setProperty("sort","${sort}")
			         .submit("hl_1");
  });
		});
		</script>
	</c:if>
	

</dsp:page>



