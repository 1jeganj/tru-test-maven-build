<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
	<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations" />
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
	<dsp:getvalueof var="pdpPageUrl" bean="EndecaConfigurations.pdpPageUrl" />
	<dsp:getvalueof var="collectionPageUrl"
		bean="EndecaConfigurations.collectionPageUrl" />
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:getvalueof var="record" param="record" />
	<dsp:getvalueof var="enablePriceFromATG" bean="/atg/multisite/Site.enablePriceFromATG"/>

	
<dsp:getvalueof var="count" param="count" />
<dsp:getvalueof var="numRecords" value="${record.numRecords}" />


	<c:forEach var="records" items="${record.records}">
		<dsp:getvalueof var="type"
			value="${records.attributes['product.type']}" />
		 <c:set var="ageRange" value="${records.attributes['ageRange']}"/>
	</c:forEach>
	
	 <c:set var="hasPriceRange" value="${record.attributes['hasPriceRange']}"/>
	 <c:set var="collectionImageURL" value="${record.attributes['collectionImage']}"/>
            <c:set var="CollectionName" value="${record.attributes['CollectionName']}"/>
             <c:set var="skuListSize" value="${record.attributes['skuListSize']}"/>
         
            
            <fmt:parseNumber var="skuListSize" type="number" value="${skuListSize}" />

	<c:choose>
		<c:when test="${type eq 'collectionProduct'}">
			<dsp:getvalueof var="pdpPageUrl" value="${collectionPageUrl}" />
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="pdpPageUrl" value="${pdpPageUrl}" />
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${type eq 'collectionProduct' and skuListSize gt 1}">
			<dsp:getvalueof var="searchImageUrl" value="${collectionImageURL}" />
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="searchImageUrl"
		value="${record.attributes['searchImageUrl']}" />
		</c:otherwise>
	</c:choose>

	<dsp:getvalueof var="akamaiImageUrl" value="" />
	<dsp:getvalueof var="gridViewHomePageAkamaiImageResizeMap" bean="TRUStoreConfiguration.GridViewHomePageAkamaiImageResizeMap" />
	<dsp:getvalueof var="viewItems" param="viewItems" />
	<dsp:getvalueof var="imageHeight" value="" />
	<dsp:getvalueof var="imageWidth" value="" />
	<c:forEach var="entry" items="${gridViewHomePageAkamaiImageResizeMap}">
		<c:if test="${entry.key eq 'gridMediumVertical'}">
			<c:set var="gridMediumVertical" value="${entry.value}" />
			<c:set var="dateParts" value="${fn:split(gridMediumVertical, ':')}" />
			<dsp:getvalueof var="gridMediumVerticalImageHeight"
				value="${dateParts[0]}" />
			<dsp:getvalueof var="gridMediumVerticalImageWidth"
				value="${dateParts[1]}" />
		</c:if>
		<c:if test="${entry.key eq 'gridLarge'}">
			<c:set var="gridLarge" value="${entry.value}" />
			<c:set var="dateParts" value="${fn:split(gridLarge, ':')}" />
			<dsp:getvalueof var="gridLargeImageHeight" value="${dateParts[0]}" />
			<dsp:getvalueof var="gridLargeImageWidth" value="${dateParts[1]}" />
		</c:if>
		<c:if test="${entry.key eq 'gridMedium'}">
			<c:set var="gridMedium" value="${entry.value}" />
			<c:set var="dateParts" value="${fn:split(gridMedium, ':')}" />
			<dsp:getvalueof var="gridMediamImageHeight" value="${dateParts[0]}" />
			<dsp:getvalueof var="gridMediamImageWidth" value="${dateParts[1]}" />
		</c:if>
		<c:if test="${entry.key eq 'gridSmall'}">
			<c:set var="gridSmall" value="${entry.value}" />
			<c:set var="dateParts" value="${fn:split(gridSmall, ':')}" />
			<dsp:getvalueof var="gridSmallImageHeight" value="${dateParts[0]}" />
			<dsp:getvalueof var="gridSmallImageWidth" value="${dateParts[1]}" />
		</c:if>
	</c:forEach>
	<c:choose>
		<c:when
			test="${not empty viewItems and viewItems eq 'grid-medium-vertical'}">
			<dsp:getvalueof var="imageHeight"
				value="${gridMediumVerticalImageHeight}" />
			<dsp:getvalueof var="imageWidth"
				value="${gridMediumVerticalImageWidth}" />
		</c:when>
		<c:when test="${not empty viewItems and viewItems eq 'grid-large'}">
			<dsp:getvalueof var="imageHeight" value="${gridLargeImageHeight}" />
			<dsp:getvalueof var="imageWidth" value="${gridLargeImageWidth}" />
		</c:when>
		<c:when test="${not empty viewItems and viewItems eq 'grid-small'}">
			<dsp:getvalueof var="imageHeight" value="${gridSmallImageHeight}" />
			<dsp:getvalueof var="imageWidth" value="${gridSmallImageWidth}" />
		</c:when>
		<c:when test="${not empty viewItems and viewItems eq 'grid-medium'}">
			<dsp:getvalueof var="imageHeight" value="${gridMediamImageHeight}" />
			<dsp:getvalueof var="imageWidth" value="${gridMediamImageWidth}" />
		</c:when>
	</c:choose>

	<dsp:droplet name="/com/tru/common/droplet/TRUAkamaiImageDroplet">
		<dsp:param name="imageName" value="${searchImageUrl}" />
		<dsp:param name="imageHeight" value="${imageHeight}" />
		<dsp:param name="imageWidth" value="${imageWidth}" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="akamaiImageUrl" param="akamaiUrl" />
		</dsp:oparam>
	</dsp:droplet>
	<a href="javascript:void(0)" class="image-grid-link"
		title="${record.attributes['sku.displayName']}"><img
		src="${akamaiImageUrl}" class="toy-grid-image"
		alt="${record.attributes['sku.displayName']}" /></a>

	<!-- <div class="modal fade in" id="shoppingCartModal"
		data-target="#shoppingCartModal" tabindex="-1" role="dialog"
		aria-labelledby="basicModal" aria-hidden="false"></div> -->

	    <div class="toy-grid-summary">
	    	 <c:choose>
				<c:when test="${type eq 'collectionProduct' and skuListSize>1}">
			   </c:when>
		    <c:otherwise>
              		 <div class="quick-view-container">
				<%-- <div class="product-hover-view product-quick-view"
					data-toggle="modal" data-target="#quickviewModal"
					onclick="loadQuickView('${record.attributes['product.repositoryId']}','')">
					<a href="javascript:void(0);"><fmt:message key="tru.productdetail.label.quickview" /></a>
				</div> --%>
			
							<%-- 	<div class="add-to-cart" 
					onclick="addItemToCart('${record.attributes['product.repositoryId']}','')">
					<a href="javascript:void(0);">add to cart</div> --%>
					
					<div class="product-add-to-cart" data-skuid="${record.attributes['sku.repositoryId']}" data-productid="${record.attributes['product.repositoryId']}">add to cart</div>
								
                           </div>
	       	</c:otherwise>
						
			</c:choose>
        <div class="quickview-container"
      
			data-href="${record.detailsAction.recordState}">
			<div class="toy-grid-details">
             <fmt:parseNumber var="reviewRating" type="number"
					value="${record.attributes['sku.reviewRating']}" parseLocale="en_US"/>

           <c:choose>
			<c:when test="${type eq 'collectionProduct' and skuListSize>1}">
		   </c:when>
		   <c:otherwise>
				<c:forEach begin="1" end="${reviewRating}">
					<div class="star-rating-on"></div>
				</c:forEach>

				<c:forEach begin="1" end="${5-reviewRating}">
					<div class="star-rating-off"></div>
				</c:forEach>
	      </c:otherwise>
	   </c:choose>
	    
           	<div class="toy-grid-product-description">
				
				<c:choose>
						<c:when test="${type eq 'collectionProduct' and skuListSize>1}">
						
					
						<a href="${record.detailsAction.recordState}">${CollectionName}
					     </a>
						</c:when>
					   <c:otherwise>
					
						<a href="${record.detailsAction.recordState}">${record.attributes['sku.displayName']}
					     </a>
						</c:otherwise>
			  </c:choose>
				   <%-- <c:if test="${type eq 'collectionProduct'}">
					<a href="${pdpPageUrl}${record.detailsAction.recordState}">${record.attributes['sku.displayName']}
					</a>
					
					</c:if>
						<c:if test="${type eq 'collectionProduct'}">
					<a href="${pdpPageUrl}${record.detailsAction.recordState}">${record.attributes['sku.displayName']}
					</a>
					
					</c:if> --%>
				</div>
				
			     	<c:choose>
			           <c:when test="${type eq 'collectionProduct' &&  skuListSize>1}">
			           </c:when>
                       <c:when test="${not empty ageRange}">
			    <div class="toy-grid-product-age">Age:${ageRange}</div>
			           </c:when>
                        <c:otherwise>
                        </c:otherwise>
                    </c:choose>
				
			<!-- 	<div class="toy-grid-product-age">Age: 6 years +</div> -->
				<!--<div class="toy-grid-product-price">$15.99-->
				<dsp:getvalueof var="numRecords" value="${record.numRecords}" />
				
			<c:if test="${enablePriceFromATG eq true}">
				<!-- ko -->
					<div class="product-block-price" data-bind="html:$root.getPriceMessage('${record.attributes['sku.repositoryId']}')"></div>
				<!-- /ko -->		
			</c:if>
			<c:if test="${enablePriceFromATG eq false}">
				<div class="product-block-price">
					<c:choose>
						<c:when test="${hasPriceRange ne 'true'}">
					<dsp:getvalueof var="isStrikeThrough"
								value="${record.attributes['isStrikeThrough']}" />

							<c:choose>
								<c:when test="${isStrikeThrough eq 'true'}">
									<span class="product-block-original-price"><fmt:message
											key="pricing.dollar" />
										<fmt:parseNumber var="listPrice" type="number"
											value="${record.attributes['sku.listPrice']}" parseLocale="en_US"/>
										<fmt:formatNumber type="number" minFractionDigits="2"
											maxFractionDigits="2" value="${listPrice}" /></span>
									<span class="product-block-sale-price"> &nbsp;<fmt:message
											key="pricing.dollar" />
										<fmt:parseNumber var="salePrice" type="number"
											value="${record.attributes['sku.salePrice']}" parseLocale="en_US"/>
										<fmt:formatNumber type="number" minFractionDigits="2"
											maxFractionDigits="2" value="${salePrice}" /></span>
									<div class="product-block-amount-saved">
										you save <span><fmt:message key="pricing.dollar" />
											<fmt:parseNumber var="savedAmount" type="number"
												value="${record.attributes['savedAmount']}" parseLocale="en_US"/>
											<fmt:formatNumber type="number" minFractionDigits="2"
												maxFractionDigits="2" value="${savedAmount}" />
											<fmt:parseNumber var="savedPercentage" type="number"
												value="${record.attributes['savedPercentage']}"  parseLocale="en_US"/>&nbsp;(<fmt:formatNumber
												type="number" minFractionDigits="2" maxFractionDigits="2"
												value="${savedPercentage}"/>%)</span>
									</div>
								</c:when>
								
							 
								<c:otherwise>
								
								 <fmt:parseNumber var="salePrice" type="number" value="${record.attributes['sku.salePrice']}" />
									<c:if test="${salePrice gt 0}">
							
									<span class="product-block-sale-price"><fmt:message
											key="pricing.dollar" />
								
										<fmt:parseNumber var="salePrice1" type="number"
											value="${record.attributes['sku.salePrice']}" parseLocale="en_US"/>
										<fmt:formatNumber type="number" minFractionDigits="2"
											maxFractionDigits="2" value="${salePrice1}" /> </span>
									</c:if>
								</c:otherwise>
							</c:choose>


						</c:when>

						<c:otherwise>
								<c:if test="${not empty record.attributes['minimumPrice'] and not empty record.attributes['maximumPrice']}">
							<span class="product-block-price-range">
							<fmt:message key="pricing.dollar" />
								<fmt:parseNumber var="minimumPrice" type="number" value="${record.attributes['minimumPrice']}" parseLocale="en_US" />
								<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${minimumPrice}" />&ndash;
								<fmt:message key="pricing.dollar" />
								<fmt:parseNumber var="maximumPrice" type="number" value="${record.attributes['maximumPrice']}" parseLocale="en_US"/>
								<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${maximumPrice}" /> </span>
							</c:if>

						</c:otherwise>
					</c:choose>
				</div>
			</c:if>
				<c:forEach items="${record.attributes['sku.skuQualifiedForPromos']}" var="item" varStatus="status">
					<input type="hidden" name="${status.index}" value="${item}" class="promo${count} promoContent">
				</c:forEach>
				<c:if test="${type ne 'collectionProduct'}">
					<div  class="product-block-incentivePromo promoResultDiv sku-${record.attributes['sku.repositoryId']}">
					</div>
				</c:if>
			</div>
			
		</div>

		
	</div>
</dsp:page>