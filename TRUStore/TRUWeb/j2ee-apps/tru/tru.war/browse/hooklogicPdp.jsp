<dsp:page>
<dsp:importbean bean="/com/tru/integrations/common/TRUHookLogicConfiguration"/>
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:getvalueof var="hooklogicURL" bean="TRUHookLogicConfiguration.hooklogicURL"/>
<dsp:getvalueof var="customerId" bean="Profile.id"/>
 <dsp:getvalueof var="loginStatus" bean="Profile.transient" />
 <c:if test="${loginStatus eq 'true'}">
		<c:set var="customerId" value=""/>
	</c:if>
 <dsp:getvalueof param="productInfo" var="productInfo" />
 <dsp:getvalueof param="productInfo.productId" var="parentSku" />

<script src="${hooklogicURL}" type="text/javascript">
</script>

<input type="hidden" id="hookLogicCId" value='${customerId}'/>
<input type="hidden" id="hookLogicSku" value='${parentSku}'/>
<input type="hidden" id="skuArr" value=''/>
<input type="hidden" id="regPrice" value=''/>
<input type="hidden" id="price" value=''/>
<input type="hidden" id="stock" value=''/>
   <div id="hl_pdp"> </div>
		<script type="text/javascript">
		function hooklogicPdp(){
		var jsonData = JSON.parse($("[class^=productInfoJson]").val());
		var skuArr = [], regPrice = [], price = [],stock =[];
		 for(var i = 0; i< jsonData.mActiveSKUsList.length; i++){
			var item = jsonData.mActiveSKUsList[i];
				skuArr.push(item.mId);
				var lp=parseFloat(item.mListPrice).toFixed(2);
				regPrice.push(lp);
				var sp=parseFloat(item.mSalePrice).toFixed(2);
				price.push(sp);
				var status=item.mInventoryStatus;
				if(status == "inStock" )
				stock.push(1);
				else
				stock.push(0);
		}
		$("#skuArr").val(skuArr.join(' | '));
		$("#regPrice").val(regPrice.join(' | '));
		$("#price").val(price.join(' | '));
		$("#stock").val(stock.join(' | '));
		HLLibrary.newUpdate()
			.setProperty("hlPageType","P")
			.setProperty("cUserId", "${customerId}")
			.setProperty("pubPageType","product")
			.setProperty("availability","web|storeeligible")
			.setProperty("inStock",stock.join(' | '))
			.setProperty("sku", skuArr.join(' | '))
			.setProperty("parentSku","${parentSku}")
			.setProperty("price",price.join(' | '))
			.setProperty("regularPrice",regPrice.join(' | '))
			.submit();
		}
		</script>
</dsp:page>



