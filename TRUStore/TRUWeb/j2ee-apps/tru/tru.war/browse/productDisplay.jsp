<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
	<input type="hidden" value="${contextPath}" class="hiddenContextPath" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof var="contentItem" param="contentItem" />
	<dsp:getvalueof var="skuIdArray" value="${contentItem.skuIdArray}" />
	<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations" />
	<dsp:getvalueof var="pdpPageUrl" bean="EndecaConfigurations.pdpPageUrl" />
	<dsp:getvalueof var="enablePriceFromATG" bean="/atg/multisite/Site.enablePriceFromATG"/>
	<dsp:getvalueof var="siteURL" param="siteURL" />
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
	<dsp:getvalueof var="resultListAkamaiImageResizeMap" bean="TRUStoreConfiguration.resultListAkamaiImageResizeMap" />
	<dsp:getvalueof var="noImageUrl" bean="TRUStoreConfiguration.noImageUrl" />
	<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
		<dsp:oparam name="output">
		<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
		</dsp:oparam>
	</dsp:droplet>
	<c:forEach var="entry" items="${resultListAkamaiImageResizeMap}">
		<c:if test="${entry.key eq 'collectionProductListImageBlock'}">
			<c:set var="collectionProductListImageBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'swatchImageBlock'}">
			<c:set var="swatchImageBlock" value="${entry.value}" />
		</c:if>
	</c:forEach>	 
    <dsp:include page="/jstemplate/registryModalDialog.jsp">
		<dsp:param name="page" value="compare"/>
    </dsp:include>	
	<dsp:getvalueof var="catID" param="catID" />
	<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="queryString" value="${originatingRequest.queryString}" />
	<div class="modal fade in" id="quickviewModal" tabindex="-1"
		role="dialog" aria-labelledby="basicModal" aria-hidden="false">
		<dsp:include page="/jstemplate/quickView.jsp"></dsp:include>
	</div>
	<div class="modal fade in" id="shoppingCartModal"
				data-target="#shoppingCartModal" tabindex="-1" role="dialog"
				aria-labelledby="basicModal" aria-hidden="false"></div>
	<dsp:include page="/jstemplate/includePopup.jsp" >
	<dsp:param name="page" value="quickView"/>
	</dsp:include>
	
	<div class="modal fade in pdpModals esrbratingModel" id="esrbratingModel" tabindex="-1" role="dialog"
					aria-labelledby="basicModal" aria-hidden="false">
		<dsp:include page="/jstemplate/esrbMature.jsp">
		</dsp:include>
	</div>

	<!-- ko foreach: recordsList -->
	
		
	<!--  ko if:$data.hasOwnProperty('attributes') -->
	<div class="col-md-3 col-no-padding product-block-padding productBlock">
	<div class="product-block product-block-unselected price-range" data-bind="attr:{id:'blockStrike'+$index()}">
		<c:if test="${enablePriceFromATG eq false}">
				<!-- ko if: attributes.hasPriceRange    -->				
					<!-- ko if: attributes.hasPriceRange[0] == 'false'   -->
						<!-- ko if :attributes.isStrikeThrough -->
							<!-- ko if :$root.checkStrikeThrough(attributes.isStrikeThrough[0],$index())  -->
							<!-- /ko -->
						<!-- /ko -->
					<!-- /ko -->
				<!-- /ko -->
		</c:if>
				<!-- ko if:attributes['product.type'] !='collectionProduct' -->
				<!-- ko if:attributes['sku.isNew'] == "true" -->
					<div class="product-flag-bubble product-${siteId}">
						<span class="promotional-sticker New"></span>
					</div>
				<!-- /ko -->
				<!-- ko if:attributes['sku.isNew'] == "false" -->
					<!-- ko if:attributes['sku.promotionalStickerDisplay'] -->
						<div class="product-flag-bubble product-${siteId}">
						<span data-bind="attr:{class:'promotional-sticker '+attributes['sku.promotionalStickerDisplay']}"></span>
						</div>
					<!-- /ko -->
				<!-- /ko -->
						<!-- /ko -->
		
			<!-- compare check Box -->
			
			<dsp:getvalueof var="isCompareProduct" param="isCompareProduct" />
			<div class="product-choose">
			<!-- ko if:attributes['product.type'] !='collectionProduct' -->
		<c:if test="${fn:contains(queryString,'keyword') eq false && (not empty isCompareProduct && isCompareProduct eq true)}">
					<div data-bind="attr:{class:'product-compare-checkbox-container compareSkuId-'+attributes['sku.repositoryId']}">
						<%-- <span class="compareCategoryId_span">${catID}</span> --%>
						<input type="hidden" value="${catID}" class="compareCategoryId" />
						<input type="hidden" data-bind="attr:{value:attributes['product.repositoryId']}" class="productId" /> 
						<input type="hidden" data-bind="attr:{value:attributes['sku.repositoryId']}" class="skuId" />
						<input type="hidden" data-bind="attr:{value:attributes['onlinePID']}" class="onlinePID" />
						<input type="hidden" data-bind="attr:{value:attributes['isStylized']}" class="isStylized" />
						<input type="hidden" data-bind="attr:{value:attributes['product.displayName']}" class="skuDisplayName" />
						<div data-bind="attr:{class:'product-compare-checkbox-new '+attributes['product.repositoryId']+'_'+attributes['sku.repositoryId']+' compareUnChecked'}" tabindex="0"></div>
						<label class="">Compare</label>
					</div>
			</c:if>
			<!-- /ko -->
			</div>
			<div class="product-selection">
				<div class="selection-checkbox-off"></div>
				<div class="selection-text-off">Select</div>
			</div>
			
			<div class="modal fade in" id="findInStoreModal" tabindex="-1"
				role="dialog" aria-labelledby="basicModal" aria-hidden="true">
				<dsp:include page="/jstemplate/findInStoreInfo.jsp" />
			</div>
				<%-- <dsp:include page="/jstemplate/includePopup.jsp"/> --%>
			<!-- Product Image -->
			<div class="product-block-image-container">
				<div class="product-block-image">
					<div class="product-image product-hover quickview">
			
					<!-- ko if:(attributes['product.type'] =='collectionProduct'  &&  attributes['skuListSize']>1)-->
					
	
					
						<!-- ko if: attributes['collectionImage']  -->
						<a class="bnsImgBlock" data-bind="click: $root.omniGridwallClick.bind($data,attributes['product.repositoryId'], $index()) ,attr :{href :'${scheme}'+((detailsAction.recordState).replace('&format=json' ,'')) }">
							<img src="${noImageUrl}" alt="${attributes['collectionImage']}" class="product-block-img" data-bind="attr:{src:attributes['collectionImage']+'${collectionProductListImageBlock}',alt:attributes['product.displayName']}"/>
							<input type="hidden" data-bind="attr:{value:attributes['collectionImage']+'${collectionProductListImageBlock}',id:attributes['product.repositoryId'],class:'imageUrlForCompare_'+attributes['product.repositoryId']+'_'+attributes['sku.repositoryId']}" />
						</a>
						<!-- /ko -->
						<!-- ko ifnot:attributes['collectionImage']  -->
				
						<a class="bnsImgBlock" data-bind="click: $root.omniGridwallClick.bind($data,attributes['product.repositoryId'], $index()) ,attr :{href :'${scheme}'+((detailsAction.recordState).replace('&format=json' ,'')) }">
							<img src="${noImageUrl}" alt="${attributes['collectionImage']}" class="product-block-img" data-bind="attr:{src:'${noImageUrl }'+'${collectionProductListImageBlock}',alt:attributes['product.displayName']}"/>
							<input type="hidden" data-bind="attr:{value:'${noImageUrl}'+'${collectionProductListImageBlock}',id:attributes['product.repositoryId'],class:'imageUrlForCompare_'+attributes['product.repositoryId']+'_'+attributes['sku.repositoryId']}" />
						</a>
					<!-- /ko -->
					
					
						
						<!-- /ko -->
				
						
					<!-- ko if:(attributes['product.type'] !='collectionProduct' || attributes['skuListSize']<=1) -->
				
					
						<!-- ko if: attributes['searchImageUrl']  -->
						<a class="bnsImgBlock" data-bind="click: $root.omniGridwallClick.bind($data,attributes['product.repositoryId'], $index()) ,attr :{href :'${scheme}'+((detailsAction.recordState).replace('&format=json' ,'')) }">
							<img src="${noImageUrl}" alt="${attributes['searchImageUrl']}" class="product-block-img" data-bind="attr:{src:attributes['searchImageUrl']+'${collectionProductListImageBlock}',alt:attributes['product.displayName']}"/>
							<input type="hidden" data-bind="attr:{value:attributes['searchImageUrl']+'${collectionProductListImageBlock}',id:attributes['product.repositoryId'],class:'imageUrlForCompare_'+attributes['product.repositoryId']+'_'+attributes['sku.repositoryId']}" />
						</a>
						<!-- /ko -->
						<!-- ko ifnot:attributes['searchImageUrl']  -->
						<a class="bnsImgBlock" data-bind="click: $root.omniGridwallClick.bind($data,attributes['product.repositoryId'], $index()) ,attr :{href :'${scheme}'+((detailsAction.recordState).replace('&format=json' ,'')) }">
							<img src="${noImageUrl}" alt="${attributes['searchImageUrl']}" class="product-block-img" data-bind="attr:{src:'${noImageUrl }'+'${collectionProductListImageBlock}',alt:attributes['product.displayName']}"/>
							<input type="hidden" data-bind="attr:{value:'${noImageUrl}'+'${collectionProductListImageBlock}',id:attributes['product.repositoryId'],class:'imageUrlForCompare_'+attributes['product.repositoryId']+'_'+attributes['sku.repositoryId']}" />
						</a>
					<!-- /ko -->
					
					
					
				<!-- /ko -->
						
					</div>
				</div>
			</div>
			<!-- product color -->
			<div class="modal fade" id="notifyMeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        	<dsp:include page="/jstemplate/notifyMePopUp.jsp">
				</dsp:include>                           
   			 </div>
   			 <dsp:include page="/jstemplate/privacyPolicyTemplate.jsp">
  			  </dsp:include>
					
			<!-- wish list -->
			
			<div class="wish-list-heart-block">
			
				<div class="wish-list-heart-off"></div>
			</div>
			<!--product rating  -->
			<!-- ko if:(attributes['product.type'] !='collectionProduct' || attributes['skuListSize']<=1) -->
			<div class="star-rating-block" aria-label="having 5 star rating" tabindex="0">
				<!-- ko ifnot:records[0].attributes['sku.reviewRating'] -->
					<!-- ko foreach : ko.utils.range(1,5) -->
						<div class="star-rating-off"></div>
					<!--/ko-->
				<!--/ko-->
				<!-- ko if:records[0].attributes['sku.reviewRating'] -->
					<!-- ko if:$root.calculateStars(records[0].attributes['sku.reviewRating']) -->
						<!-- ko foreach : ko.utils.range(1, parseInt(records[0].attributes['sku.reviewRating'])) -->
							<div class="star-rating-on"></div>
						<!--/ko-->
						<div class="star-rating-half"></div>
						<!-- ko foreach : ko.utils.range(1, 5-parseInt(records[0].attributes['sku.reviewRating'])-1) -->
							<div class="star-rating-off"></div>
						<!--/ko-->
					<!--/ko-->
					<!-- ko ifnot:$root.calculateStars(records[0].attributes['sku.reviewRating']) -->
						<!-- ko foreach : ko.utils.range(1, parseInt(records[0].attributes['sku.reviewRating'])) -->
							<div class="star-rating-on"></div>
						<!--/ko-->
						
						<!-- ko foreach : ko.utils.range(1, 5-parseInt(records[0].attributes['sku.reviewRating'])) -->
							<div class="star-rating-off"></div>
						<!--/ko-->
					<!--/ko-->
				<!--/ko-->
			</div>
		<!--/ko-->
			<!-- product information like Displaying Name,Age,Price,Disc -->
			<div class="product-block-information">
			
				<!--  ko if:attributes['CollectionName'] -->
				<div class="product-name">
					<a data-bind="click: $root.omniGridwallClick.bind($data,attributes['product.repositoryId'] , $index()) ,attr :{href :'${scheme}'+((detailsAction.recordState).replace('&format=json' ,''))}">
					  <span data-bind="text:attributes['CollectionName']"></span>
				    </a>
				</div>
			
				
			<!--/ko-->
				<!--  ko ifnot:attributes['CollectionName'] -->
			<!-- ko if:(''+attributes['sku.displayName']).length>70 -->
				<div class="product-name">
					<a data-bind="click: $root.omniGridwallClick.bind($data,attributes['product.repositoryId'], $index()) ,attr :{href :'${scheme}'+((detailsAction.recordState).replace('&format=json' ,''))}">
					  <span data-bind="text:(''+attributes['sku.displayName']).substring(0, 70)+'...'"></span>
				    </a>
				</div>
			<!--/ko-->
			<!-- ko ifnot:(''+attributes['sku.displayName']).length>70 -->
				<div class="product-name">
					<a data-bind="click: $root.omniGridwallClick.bind($data,attributes['product.repositoryId'] , $index()) ,attr :{href :'${scheme}'+((detailsAction.recordState).replace('&format=json' ,''))}">
					  <span data-bind="text:attributes['sku.displayName']"></span>
				    </a>
				</div>
			<!--/ko-->
			
					<!--/ko-->
			<%--<!-- ko if:(attributes['product.type'] !='collectionProduct' || attributes['skuListSize']<=1) -->
		
				<!--  ko if:attributes['ageRange'] -->
					<div class="age-range">
						Age: <span data-bind="text:attributes['ageRange']"></span>
					</div>
				<!--  /ko -->
				<!--  /ko -->--%>
				
				
				<!-- Product Price Display -->
				<c:if test="${enablePriceFromATG eq true}">
					<div class="product-block-price" data-bind="html:$root.getPriceMessage(attributes['sku.repositoryId'][0])"></div>		
				</c:if>
			
				<c:if test="${enablePriceFromATG eq false}">
		 			<div class="product-block-price "> 
			     
					<!-- ko if:attributes['sku.priceDisplay'] =='Price on Cart Page' -->
						<!-- ko if:$root.isCartRepositoryId(attributes['sku.repositoryId']) -->
							<dsp:include page="/browse/endecaPriceDisplay.jsp"> 
							</dsp:include>
						<!--/ko-->
						<!-- ko ifnot:$root.isCartRepositoryId(attributes['sku.repositoryId'])-->
							<fmt:message key="tru.pricing.label.priceTooLowToDisplay" />
						<!--/ko-->
					<!--/ko-->
					<!-- ko ifnot:attributes['sku.priceDisplay'] =='Price on Cart Page' -->
						<dsp:include page="/browse/endecaPriceDisplay.jsp">
						</dsp:include>
					<!--/ko-->	
					<!-- ko if:attributes['sku.isPreSellable'] == "true" -->
						<div class="preOrder">
						<fmt:message key="tru.productdetail.label.preorder"/>
						</div>
					<!-- /ko -->
					</div>
				</c:if>
		
			</div>
			<div class="product-block-incentive">
				<span data-bind="text:$root.getPromoMessage(records[0].attributes['sku.repositoryId'])"></span>
            </div>
            <!-- ko if: attributes['sku.videoGameEsrb']  -->
            <div class="product-block-esrbRating">	
				<span class="esrb-rating-text">ESRB Rating:</span> <span data-bind="text:attributes['sku.videoGameEsrb']"></span>
			</div>	
			<!--/ko-->
			<!-- Quick View-->
						<!-- ko if:(attributes['product.type'] !='collectionProduct' || attributes['skuListSize']<=1) -->
							<div class="addtocart-container ">
								<!-- ko if:attributes['product.type'] !='collectionProduct' -->
									<div class="colors" style='display:block;'>
										<!-- ko if:numRecords > 1 -->
										
											<!-- ko foreach:attributes['product.swatchColors'] -->
												<!-- ko if:$index()<= 3 -->
													<div class="product-color-block swatchSelected" >
													<!-- ko if: $data.split(';')[0]  -->
														<img src="../images/noimage.jpg" alt=""  data-bind="click: $root.isPromoApplicable,attr:{imgsrc:$data.split(';')[0]+'${swatchImageBlock}',id:$data.split(';')[1],src:$data.split(';')[2]+'${swatchImageBlock}',alt:$data.split(';')[1]}"/>
													<!--/ko-->
													<!-- ko ifnot: $data.split(';')[0]  -->
														<img src="../images/noimage.jpg" alt=""  data-bind="click: $root.isPromoApplicable,attr:{imgsrc:'${noImageUrl}'+'${swatchImageBlock}',id:$data.split(';')[1],src:$data.split(';')[2]+'${swatchImageBlock}',alt:$data.split(';')[1]}"/>
													<!--/ko-->
													</div>
												<!--/ko-->
												<!-- ko if:$index()==4 -->
													<a class="viewMoreSwatchImages product-color-block" data-bind="attr :{href :'http://'+(($parents[0].detailsAction.recordState).replace('&format=json' ,''))}" >
														more
													</a>	
												<!--/ko-->
											<!--/ko-->
											
										<!--/ko-->
											</div>
									
									<!--/ko-->
								<%-- <div class="product-hover-view product-quick-view" data-toggle="modal" data-target="#quickviewModal"
									data-bind="attr:{id : attributes['product.repositoryId']+'|'+attributes['onlinePID']}" onclick='loadQuickView(this.id);loadOmniQuickView();'>
									<!-- quick view -->
									<a href="javascript:void(0)"><fmt:message key="tru.productdetail.label.quickview" /></a>
								</div> --%> 
								<!-- ko if:(attributes['isStylized'] =='false') -->
								<div class="product-hover-view product-add-to-cart " data-bind="css:$root.isHavingInventry(records[0].attributes['sku.repositoryId'][0]),attr:{'data-skuid':records[0].attributes['sku.repositoryId'][0],'data-productid':attributes['product.repositoryId']},'text':btnText"></div>
								<!-- /ko -->
						        <!-- ko if:(attributes['product.type'] !='collectionProduct' || attributes['isStylized'] =='true') -->
										<!-- ko if:(numRecords > 1 || attributes['isStylized'] =='true')-->
										<!-- ko if:(attributes['product.swatchColors'] || attributes['isStylized'] =='true')-->
											<div class="product-hover-view product-view-details" >
												<a data-bind="attr :{href :'http://'+((detailsAction.recordState).replace('&format=json' ,''))}"> View Details</a>
											</div>
											<!--/ko-->
										<!--/ko-->
											
								 <!--/ko-->
									
								
							</div>
							
						<!-- /ko -->
						<!-- ko if:attributes['product.type'] =='collectionProduct' -->
						<div class="addtocart-container ">
						<div class="colors" style='display:block;'></div>
						<div class="product-hover-view product-view-details">
						<a data-bind="attr :{href :'http://'+((detailsAction.recordState).replace('&format=json' ,''))}">
							View Details</a>
							</div>
							</div>
						<!--/ko-->
				<!-- /ko -->
			
			<%-- <div class="promo-block-container">
				<div class="promotion-block">
					<div class="header">
						<h2>20% off</h2>
						<p>all Graco Stollers-online only</p>
					</div>
					<br> <img src="${TRUImagePath}images/subcat-promo-img.jpg"
						alt="cannot find image">
					<button class="shop-now">shop now</button>
				</div>
			</div> --%>
			<div class="product-block-border"></div>
		</div>
	</div><!-- /ko -->
	   <!-- <div class="col-md-3 col-no-padding product-block-padding hookDiv" id="hookDiv1"/> -->
        <!-- <div class="col-md-3 col-no-padding product-block-padding hookDiv" id="hookDiv2"/>
        <div class= "col-md-3 col-no-padding product-block-padding hookDiv" id="hookDiv3"/>
        <div class="col-md-3 col-no-padding product-block-padding hookDiv" id="hookDiv4"/> -->
		<!--  ko ifnot:$data.hasOwnProperty('attributes') -->
		<div data-bind="html: $data"></div>
		   <!-- /ko -->
		   <!--  ko if : $index() ===3 -->
			
		 <!-- /ko -->
	<!-- /ko -->
	
</dsp:page>