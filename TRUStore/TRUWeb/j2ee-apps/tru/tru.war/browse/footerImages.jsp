   

<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
   <div class="footer-images">
            <div class="row row-no-padding">
                <div class="footer-image-row col-md-12">
                    <div class="footer-image">
                        <div class="image-holder">
                            <a href="http://cloud.toysrus.resource.com/redesign/template-home-main.html#" class="footer-image-link"><img class="footer-image" src="${TRUImagePath}images/registry.jpg" alt="ToysRus registry">
                            </a>
                        </div>
                        <div class="image-holder">
                            <a href="http://cloud.toysrus.resource.com/redesign/template-home-main.html#" class="footer-image-link"><img class="footer-image" src="${TRUImagePath}images/credit-card.jpg" alt="ToysRus creadit card">
                            </a>
                        </div>
                        <div class="image-holder">
                            <a href="http://cloud.toysrus.resource.com/redesign/template-home-main.html#" class="footer-image-link"><img class="footer-image" src="${TRUImagePath}images/big-book.jpg" alt="ToysRus bigbook">
                            </a>
                        </div>
                        <div class="image-holder">
                            <a href="http://cloud.toysrus.resource.com/redesign/template-home-main.html#" class="footer-image-link"><img class="footer-image" src="${TRUImagePath}images/gift-finder.jpg" alt="ToysRus gift finder">
                            </a>
                        </div>
                        <div class="image-holder">
                            <a href="http://cloud.toysrus.resource.com/redesign/template-home-main.html#" class="footer-image-link"><img class="footer-image" src="${TRUImagePath}images/savings-center.jpg" alt="ToysRus saving sender">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
</dsp:page>