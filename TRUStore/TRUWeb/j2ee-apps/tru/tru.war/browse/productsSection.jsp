<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUConvertContentItemToJSON" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUSkuPromotionDetailsDroplet"/>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
<dsp:importbean bean="/com/tru/common/TRUIntegrationConfiguration" />
<dsp:importbean bean="/com/tru/common/TRUSOSIntegrationConfiguration" />
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations" />
<dsp:getvalueof var="pdpPageUrl" bean="EndecaConfigurations.pdpPageUrl" />
<dsp:getvalueof var="siteURL" param="siteURL" />
<dsp:getvalueof var="promotionCountinPDP" bean="TRUConfiguration.promotionCountinPDP"/>
<dsp:getvalueof var="contentItem" param="contentItem" />
<dsp:getvalueof var="record" value="${contentItem.records}"/>
<dsp:getvalueof var="isFirstLoad" param="isFirstLoad" />
<dsp:getvalueof var="resultsListItem" param="resultsListItem" />
<dsp:getvalueof var="EndValue" param="Nrpp" />
<dsp:getvalueof var="BeginValue" param="1" />
<dsp:getvalueof var="hpageType" param="hpageType" />
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
<dsp:getvalueof var="storeEnableHookLogic" bean="TRUIntegrationConfiguration.enableHookLogic"/>
<dsp:getvalueof var="sosEnableHookLogic" bean="TRUSOSIntegrationConfiguration.enableHookLogic"/>
<dsp:getvalueof var="skuIdArray" value="${contentItem.skuIdArray}" />
<dsp:getvalueof var="skuStoreEligibleArray" value="${contentItem.skuStoreEligibleArray}" />
<dsp:getvalueof var="SKU_PRICE_ARRAY" value="${contentItem.SKU_PRICE_ARRAY}" />
<dsp:droplet name="GetSiteTypeDroplet">
	<dsp:param name="siteId" value="${site}"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="site" param="site"/>
		<c:set var="site" value ="${site}" scope="request"/>
	</dsp:oparam>
</dsp:droplet>

<c:choose>
	<c:when test="${site eq 'sos'}">
		<c:set var="hookLogicEnabled" value="${sosEnableHookLogic}" scope="request" />
	</c:when>
	<c:otherwise>
		<c:set var="hookLogicEnabled" value="${storeEnableHookLogic}" scope="request" />
	</c:otherwise>
</c:choose>
		
		<script type="text/javascript">
			var searchResponse=
				<dsp:droplet name="TRUConvertContentItemToJSON">
					<dsp:param name="contentItems" bean="/OriginatingRequest.contentItem" />
				</dsp:droplet>;
				
			var jsSkuIdArray = '${skuIdArray}';
			var skuStoreEligibleArray = '${skuStoreEligibleArray}';
			var SKU_PRICE_ARRAY = '${SKU_PRICE_ARRAY}';
			
			var familyProdDisplayName = [];
			var familyProdID = [];
			var familyProdSku = [];
			var familyProdSalePrice = [];
			var familyProdRepositoryId = [];

			if(typeof searchResponse != 'undefined' && typeof searchResponse.records != 'undefined' && searchResponse.records.length > 0){

				 $.each(searchResponse.records, function(recKey, recVal){
		               if(typeof recVal == "object" && typeof recVal.attributes != 'undefined' && typeof recVal.attributes["product.displayName"] != 'undefined'){
		                    var prdName = recVal.attributes["product.displayName"][0];
		                    familyProdDisplayName.push(prdName);
		               }
		         });

		          $.each(searchResponse.records, function(recKey, recVal){
		               if(typeof recVal == "object" && typeof recVal.attributes != 'undefined' && typeof recVal.attributes["onlinePID"] != 'undefined'){
		                    var prdID = recVal.attributes["onlinePID"][0];
		                    familyProdID.push(prdID);
		               }
		          });

		          $.each(searchResponse.records, function(recKey, recVal){
		               if(typeof recVal == "object" && typeof recVal.attributes != 'undefined' && typeof recVal.attributes["sku.repositoryId"] != 'undefined'){
		                    var prdSku = recVal.attributes["sku.repositoryId"][0];
		                    familyProdSku.push(prdSku);
		               }
		          });

		          $.each(searchResponse.records, function(recKey, recVal){
		               if(typeof recVal == "object" && typeof recVal.attributes != 'undefined' && typeof recVal.attributes["sku.salePrice"] != 'undefined'){
		            		var prdSalePrice = recVal.attributes["sku.salePrice"][0];
			           		familyProdSalePrice.push(prdSalePrice);
		               }
		          });

		    }

		</script>
		<c:if test="${hookLogicEnabled eq true}">
			<dsp:include page="/browse/hooklogic.jsp" >
			<dsp:param name="hpageType" value="${hpageType}" />
			</dsp:include>
		</c:if>
		<div class="row row-no-padding">
		<dsp:include page="/browse/productDisplay.jsp" >
			<dsp:param name="contentItem" value="${contentItem}" />
			<dsp:param name="begin" value="${BeginValue}" />
			<dsp:param name="end" value="${EndValue}" />
		</dsp:include>
		</div>
		<div class="row row-no-padding afterHookProduct">
		</div>
	<c:set var="contentItemForTealium" value="${contentItem}" scope="request"/>
</dsp:page>