<dsp:page>
	<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
	
	<dsp:getvalueof var="powerReviewsURL" bean="TRUConfiguration.powerReviewsURL"/>
	<dsp:getvalueof var="merchantId" bean="TRUConfiguration.merchantId"/>
	<dsp:getvalueof var="merchantGroupId" bean="TRUConfiguration.merchantGroupId"/>
	<dsp:getvalueof var="powerReviewsApiKey" bean="TRUConfiguration.powerReviewsApiKey"/>
	<dsp:getvalueof var="pid" param="productId" />
	<dsp:getvalueof var="onlinePID" param="onlinePID" /> 
	
	<!--Power review snippet Start-->
	
	<input type="hidden" id='powerReviewsURL' value="${powerReviewsURL}"/>
	<input type="hidden" id='pwr_apiKey' value="${powerReviewsApiKey}"/>
	<input type="hidden" id='pwr_locale' value="en_US"/>
	<input type="hidden" id='pwr_merchantGroupId' value="${merchantGroupId}"/>
	<input type="hidden" id='pwr_merchantId' value="${merchantId}"/>
	<input type="hidden" id="pwr_productId" value="${onlinePID}"/>

	<div id="pr-reviewsnippet"></div>
	<!--Power review snippet End-->
	
</dsp:page>
