<dsp:page>
<dsp:importbean bean="/atg/endeca/assembler/droplet/InvokeAssembler" />
<dsp:getvalueof var="giftFinderSearchResults" value="0"/>
<dsp:getvalueof var="giftFinderName" param="giftFinderName"/>
 <dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
<dsp:droplet name="InvokeAssembler">
	<dsp:param name="includePath" value="/pages/${siteId}/ajaxResultlist" />
	<dsp:oparam name="output">
		<dsp:getvalueof var="ContentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	</dsp:oparam>
	<dsp:getvalueof var="Content" value="${ContentItem.contents}" />

	
	<c:forEach var="element" items="${Content[0].MainProduct}">
	<dsp:getvalueof var="giftFinderSearchResults" value="${element.totalNumRecs}"/>
		<c:choose>
			<c:when test="${not empty giftFinderName}">
				<dsp:getvalueof var="giftFinderSearchResults" value="${element.totalNumRecs}"/>
			</c:when>
			<c:otherwise>
				<dsp:renderContentItem contentItem="${element}" >
					<dsp:param name="page" value="ajaxResultList"/>
				 </dsp:renderContentItem>
			</c:otherwise>
		</c:choose>
	 </c:forEach>
</dsp:droplet>



	<div>
		<div id="giftFinderTotal">${giftFinderSearchResults}</div>
	</div>

</dsp:page>