<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<!-- ko if: attributes['hasPriceRange'] != 'true'   -->
		<!-- ko if:attributes['isStrikeThrough'] && attributes['isStrikeThrough'] == 'true' -->
			<!-- ko if:attributes['sku.listPrice'] -->
				<span class="product-block-original-price" data-bind="text:'$'+parseFloat(attributes['sku.listPrice']).toFixed(2)">
				</span>
			<!-- /ko -->
			<!-- ko if:attributes['sku.salePrice'] -->	
				<span class="product-block-sale-price " data-bind="text:'&nbsp; $'+parseFloat(attributes['sku.salePrice']).toFixed(2)">
				</span>
			<!-- /ko -->
			<div class="product-block-amount-saved">
				<!-- ko if:attributes['savedAmount'] -->
					you save <span data-bind="text:'$'+parseFloat(attributes['savedAmount']).toFixed(2)"></span>
				<!-- /ko -->	
				<!-- ko if:attributes['savedPercentage'] -->
						 (<span data-bind="text:parseFloat(attributes['savedPercentage']).toFixed(2)+'%'"></span>)
				<!-- /ko -->
			</div>
		<!-- /ko -->
		<!-- ko if:attributes['isStrikeThrough'] && attributes['isStrikeThrough'] != 'true'  -->
			<span class="product-block-original-price">
			<!-- ko if:attributes['sku.salePrice'] && attributes['sku.salePrice'] === 0 -->
					<span data-bind="text:'$'+parseFloat(attributes['sku.listPrice']).toFixed(2)"></span>
			<!-- /ko -->
			<!-- ko if:attributes['sku.salePrice'] && attributes['sku.salePrice'] != 0  -->
					<span data-bind="text:'$'+parseFloat(attributes['sku.salePrice']).toFixed(2)"></span>
			<!-- /ko -->
			</span>
		<!-- /ko -->
	<!-- /ko -->
	<!-- ko if:attributes['hasPriceRange'] == 'true'-->
              <span>
                   <!-- ko if: parseFloat(attributes['minimumPrice']) == parseFloat(attributes['maximumPrice'])  -->
                   <span data-bind="text:'$'+parseFloat(attributes['minimumPrice']).toFixed(2)"></span>
              
                 <!-- /ko -->
              <!-- ko if: parseFloat(attributes['minimumPrice']) != parseFloat(attributes['maximumPrice'])  -->
                     <!-- ko if:attributes['minimumPrice'] -->
                           <span data-bind="text:'$'+parseFloat(attributes['minimumPrice']).toFixed(2)"></span>
                     <!-- /ko --> 
                      <!-- ko if:attributes['maximumPrice'] -->
                           <span data-bind="text:'&nbsp;- $'+parseFloat(attributes['maximumPrice']).toFixed(2)"></span>
                     <!-- /ko -->
                     <!-- /ko --> 
              </span>
       <!--/ko-->

</dsp:page>
