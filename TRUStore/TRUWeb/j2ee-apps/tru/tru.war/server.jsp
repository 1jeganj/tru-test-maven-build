<dsp:page>
<dsp:importbean bean="/atg/dynamo/Configuration" />
<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
	<dsp:oparam name="output">
	<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
	</dsp:oparam>
</dsp:droplet>
<c:choose>
	<c:when test="${scheme == 'http://'}">
		<dsp:getvalueof var="serverName" bean="Configuration.thisHostname" vartype="java.lang.String">${serverName}:</dsp:getvalueof>
		<dsp:getvalueof var="httpPort" bean="Configuration.httpPort" vartype="java.lang.String">${httpPort}</dsp:getvalueof>
	</c:when>
	<c:otherwise>
		<dsp:getvalueof var="serverName" bean="Configuration.thisHostname" vartype="java.lang.String">${serverName}:</dsp:getvalueof>
		<dsp:getvalueof var="httpsPort" bean="Configuration.httpsPort" vartype="java.lang.String">${httpsPort}</dsp:getvalueof>
	</c:otherwise>
</c:choose>
</dsp:page>