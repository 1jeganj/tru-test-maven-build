<dsp:page>

<dsp:importbean bean="/atg/commerce/locations/StoreLocatorFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
<dsp:importbean bean="/atg/core/i18n/LocaleTools"/>

	<dsp:getvalueof var="maxResultsPerPage" bean="StoreLocatorFormHandler.maxResultsPerPage"/>
	<dsp:getvalueof var="totalResults" bean="StoreLocatorFormHandler.resultSetSize"/>
	<dsp:getvalueof id="contextPath" bean="/OriginatingRequest.contextPath" />
	
	<dsp:getvalueof var="todayDate" bean="StoreLocatorFormHandler.currentDate"/>
	
	<dsp:droplet name="ForEach">
		<dsp:param name="array" bean="StoreLocatorFormHandler.locationResults" />
		<dsp:param name="elementName" value="store"/>
		<dsp:oparam name="output">
		
			<dsp:getvalueof var="storeId" param="store.repositoryId"/>
			<dsp:getvalueof var="storeDistance" param="store.distance" />
			<dsp:getvalueof var="chainCode" param="store.chainCode" />
				<div class="store-info ${chainCode}results">
					<div class="row row-no-padding">
						<div class="col-md-9 col-no-padding">
							<h2>
								<dsp:valueof param="store.name" /><br><dsp:valueof param="store.address1" />
							</h2>
							<dsp:getvalueof var="storeHoursMap" param="store.storeHours"/>
							<dsp:getvalueof var="openingHours" value="${storeHoursMap[whatDay]}"/>
							<p>open until ${openingHours}</p>
							<p class="make-store-link make-store-link-${storeId}">
								<a href="javascript:void(0);" class="make-my-store-link make-my-store-link-${storeId}" onclick="javascript:makeMyStore('${contextPath}','${storeId}');">make this my store</a>
							</p>
						</div>
						<div class="col-md-3 col-no-padding pull-right">
							<p>${storeDistance} mi.</p>
							<a class="store-details" onclick="javascript:viewStoreDetails('${contextPath}','${storeId}','${storeDistance}','${openingHours}');" href="javascript:void(0);">details </a>
						</div>
					</div>
				</div>
				<hr>
		</dsp:oparam>
		<dsp:oparam name="outputEnd">
			<c:if test="${totalResults > maxResultsPerPage}">
				<dsp:getvalueof bean="StoreLocatorFormHandler.currentResultPageNum" var="currentResultPageNum"/>
				<div class="show-more-stores show-more-${currentResultPageNum+1}">
					<button onclick="showMoreStores('${currentResultPageNum+1}');"><!--load more  --><fmt:message key="tru.storelocator.label.loadmore"/></button>
				</div>
				<dsp:include page="storeLocatorPagination.jsp">
					<dsp:param name="startIndex" bean="StoreLocatorFormHandler.startIndex"/>
					<dsp:param name="endIndex" bean="StoreLocatorFormHandler.endIndex"/>
				</dsp:include>
				<div class="load-more-stores-${currentResultPageNum+1}"></div>
			</c:if>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>