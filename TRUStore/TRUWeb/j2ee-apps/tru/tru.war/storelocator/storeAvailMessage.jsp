<dsp:page>

<dsp:importbean bean="/com/tru/commerce/locations/TRUStoreAvailabilityDroplet"/>
<dsp:getvalueof param="pagename" var="pagename"/>
<dsp:getvalueof param="ispu" var="ispu" />
<dsp:getvalueof param="s2s" var="s2s" />

<dsp:droplet name="TRUStoreAvailabilityDroplet">
    <%--locationId we are getting from the Cookie in the TRUStoreAvailabilityDroplet  --%>
	<dsp:param name="skuId" param="skuId" />
	<dsp:param name="locationId" param="locationId" />
	<dsp:param name="isReqFromAjax" value="true" />
	<dsp:oparam name="available">
		<dsp:getvalueof var="storeAvailMsg" param="storeAvailMsg" />
		<dsp:getvalueof var="favStore" param="favStore" />
		<c:choose>
			<c:when test="${pagename eq 'PDP'}">
			<dsp:include page="/jstemplate/pdpStoreMesssage.jsp">
			<dsp:param name="storeAvailMsg" value="${storeAvailMsg}"/>
			<dsp:param name="ispu" value="${ispu}"/>
			<dsp:param name="s2s" value="${s2s}"/>
			</dsp:include>
			</c:when>
			<c:otherwise>
		<div class="store-image">
			<div class="ICN-Store inline"></div>
		</div>
		<span class="find-in-store-message">${storeAvailMsg}</span>
			</c:otherwise>
		</c:choose>
	</dsp:oparam>
	<dsp:oparam name="backorderable">
		<dsp:getvalueof var="storeAvailMsg" param="storeAvailMsg" />
		<dsp:getvalueof var="favStore" param="favStore" />
		<c:choose>
			<c:when test="${pagename eq 'PDP'}">
			<dsp:include page="/jstemplate/pdpStoreMesssage.jsp">
			<dsp:param name="storeAvailMsg" value="${storeAvailMsg}"/>
			<dsp:param name="ispu" value="${ispu}"/>
			<dsp:param name="s2s" value="${s2s}"/>
			</dsp:include>
			</c:when>
			<c:otherwise>
		<div class="store-image">
					<div class="ICN-Store inline"></div>
		</div>	
		<span class="find-in-store-message">${storeAvailMsg}</span>
			</c:otherwise>
		</c:choose>
	</dsp:oparam>
	<dsp:oparam name="preorderable">
		<dsp:getvalueof var="storeAvailMsg" param="storeAvailMsg" />
		<dsp:getvalueof var="favStore" param="favStore" />
		<c:choose>
			<c:when test="${pagename eq 'PDP'}">
			<dsp:include page="/jstemplate/pdpStoreMesssage.jsp">
			<dsp:param name="storeAvailMsg" value="${storeAvailMsg}"/>
			<dsp:param name="ispu" value="${ispu}"/>
			<dsp:param name="s2s" value="${s2s}"/>
			</dsp:include>
			</c:when>
			<c:otherwise>
		<div class="store-image">
					<div class="ICN-Store inline"></div>
		</div>
		<span class="find-in-store-message">${storeAvailMsg}</span>
			</c:otherwise>
		</c:choose>
	</dsp:oparam>
	<dsp:oparam name="unavailable">
		<dsp:getvalueof var="storeAvailMsg" param="storeAvailMsg" />
		<dsp:getvalueof var="favStore" param="favStore" />
		<c:choose>
			<c:when test="${pagename eq 'PDP'}">
			<dsp:include page="/jstemplate/pdpStoreMesssage.jsp">
			<dsp:param name="storeAvailMsg" value="${storeAvailMsg}"/>
			<dsp:param name="ispu" value="${ispu}"/>
			<dsp:param name="s2s" value="${s2s}"/>
			</dsp:include>
			</c:when>
			<c:otherwise>
		<div class="store-image">
					<div class="ICN-Store inline"></div>
		</div>	
		<span class="find-in-store-message">${storeAvailMsg}</span>
			</c:otherwise>
		</c:choose>
	</dsp:oparam>
	<dsp:oparam name="error">
		<dsp:getvalueof var="storeAvailMsg" param="storeAvailMsg" />
		<dsp:getvalueof var="favStore" param="favStore" />
		<c:choose>
			<c:when test="${pagename eq 'PDP'}">
			<dsp:include page="/jstemplate/pdpStoreMesssage.jsp">
			<dsp:param name="storeAvailMsg" value="${storeAvailMsg}"/>
			<dsp:param name="ispu" value="${ispu}"/>
			<dsp:param name="s2s" value="${s2s}"/>
			</dsp:include>
			</c:when>
			<c:otherwise>
		<div class="store-image">
					<div class="ICN-Store inline"></div>
		</div>	
		<span class="find-in-store-message">${storeAvailMsg}</span>
			</c:otherwise>
		</c:choose>
	</dsp:oparam>
	<dsp:oparam name="default">
		<dsp:getvalueof var="storeAvailMsg" param="storeAvailMsg" />
		<dsp:getvalueof var="favStore" param="favStore" />
		<c:choose>
			<c:when test="${pagename eq 'PDP'}">
			<dsp:include page="/jstemplate/pdpStoreMesssage.jsp">
			<dsp:param name="storeAvailMsg" value="${storeAvailMsg}"/>
			<dsp:param name="ispu" value="${ispu}"/>
			<dsp:param name="s2s" value="${s2s}"/>
			</dsp:include>
			</c:when>
			<c:otherwise>
		<div class="store-image">
					<div class="ICN-Store inline"></div>
		</div>	
		<span class="find-in-store-message">${storeAvailMsg}</span>
			</c:otherwise>
		</c:choose>
	</dsp:oparam>
</dsp:droplet>
</dsp:page>