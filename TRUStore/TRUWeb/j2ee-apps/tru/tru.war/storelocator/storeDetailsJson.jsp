<%@ page contentType="application/json"%>
<dsp:page>

<dsp:importbean bean="/atg/commerce/locations/StoreLookupDroplet"/>
<dsp:importbean bean="/com/tru/commerce/locations/TRUStoreDetailsDroplet"/>

<dsp:getvalueof var="storeId" param="storeId" />
	
	<dsp:droplet name="StoreLookupDroplet">
		<dsp:param name="id" value="${storeId}" />
		<dsp:param name="elementName" value="storeItem" />
		<dsp:oparam name="output">
			
			<%--Start : This droplet is used to add the jsonobject content to the response object --%>
			<dsp:droplet name="TRUStoreDetailsDroplet">
				<dsp:param name="storeItem" param="storeItem"/>
				<dsp:param name="returnStoreServicesInJsonFormat" value="true"/>
			</dsp:droplet>
			<%--End : This droplet is used to add the jsonobject content to the response object --%>
			
		</dsp:oparam>
	</dsp:droplet>
	
	
</dsp:page>