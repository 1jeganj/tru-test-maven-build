<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ page contentType="application/json"%>
<dsp:page>
    <dsp:importbean bean="/atg/commerce/locations/StoreLocatorFormHandler"/>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
    <dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
    <dsp:importbean bean="/atg/dynamo/droplet/Switch" />
    
    <dsp:getvalueof var="formExceptions" bean="StoreLocatorFormHandler.formExceptions" />
    
    <json:object prettyPrint="true">
	    <dsp:droplet name="Switch">
			<dsp:param bean="StoreLocatorFormHandler.formError" name="value" />
			<dsp:oparam name="true">
				 <json:array name="errors" var="formException" items="${formExceptions}">
	                <json:object>
	                    <json:property name="errorMessage" value="${formException.message}"/>
	                    <%--
	                    	<json:property name="errorProperties" value="${formException.propertyPath}"/>
	                    --%>
	                </json:object>
            	</json:array>
			</dsp:oparam>
		</dsp:droplet>
	</json:object>
		
    
      
</dsp:page>