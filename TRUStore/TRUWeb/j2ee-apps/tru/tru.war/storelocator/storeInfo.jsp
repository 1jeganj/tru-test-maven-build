 <fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
 <%-- This page is used to display Store Details in popup. This page accepts storeId as input parameter and
 uses StoreLookupDroplet to display Store Details. 
--%>
<dsp:page>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/commerce/locations/StoreLookupDroplet"/>
<dsp:importbean bean="/com/tru/commerce/locations/TRUMyStoreDroplet"/>
<dsp:importbean bean="/com/tru/commerce/locations/TRUStoreDetailsDroplet"/>
<dsp:getvalueof var="googleStaticUrl" bean="TRUConfiguration.storeLocatorStaticGoogleMapURL"/>
<dsp:getvalueof var="storeId" param="storeId" />
<dsp:getvalueof var="storeDistance" param="storeDistance" />
<dsp:getvalueof var="openingHours" param="openingHours" />
<dsp:getvalueof var="signature" param="signature" />

<dsp:getvalueof id="contextPath" bean="/OriginatingRequest.contextPath" />

	<%-- <dsp:droplet name="TRUMyStoreDroplet">
		<dsp:oparam name="output"> --%>
			<dsp:getvalueof var="cookieStoreId" param="cookieStoreId" />
		<%-- </dsp:oparam>
	</dsp:droplet> --%>

	<dsp:droplet name="StoreLookupDroplet">
		<dsp:param name="id" param="storeId" />
		<dsp:param name="elementName" value="store" />
		<dsp:oparam name="output">
		
			 <dsp:droplet name="TRUStoreDetailsDroplet">
				<dsp:param name="storeItem" param="store"/>
				<dsp:oparam name="output">
					<dsp:getvalueof var="storeHoursMap" param="storeHoursMap" />
					<dsp:getvalueof var="storeClosingTime" param="storeClosingTime" />
					<dsp:getvalueof var="storeServicesVOList" param="storeServicesVOList" />
					<dsp:getvalueof var="isEligibleForfavStore" param="isEligibleForfavStore" />
					
				</dsp:oparam>
			</dsp:droplet> 
		
		
			<div class="back-to-store">
				<img class="inline" src="${TRUImagePath}images/small-back-arrow.png" alt="">
				<a href="javascript:void(0)">back to store results</a>
			</div>
			<dsp:getvalueof var="storeCity" param="store.city" />
			<dsp:getvalueof var="storeStateAddress" param="store.stateAddress" />
			<dsp:getvalueof var="storeAddress" param="store.address1" />
			<dsp:getvalueof var="chainCode" param="store.chainCode" />
			
			<a class="showGoogleMapButton" data-href="/storelocator/showGoogleMap.jsp">
			<img src="${googleStaticUrl}&center=${fn:replace(storeAddress,' ','+')},${fn:replace(storeCity,' ','+')},${fn:replace(storeStateAddress,' ','+')}&signature=${signature}">
			</a> <img class="you-are-here"
				src="${TRUImagePath}images/my_${chainCode}.png"
				alt="image not found">
			<div class="store-locator-store-details">
				<h2 class="store_name">
					<dsp:getvalueof var="storeNameText" param="store.name"/>
					<c:set var="storeNameText" value="${fn:substringBefore(storeNameText,'[')}" />
					${storeNameText }
				</h2>
				<p>
					<c:set var="storeDistanceLength" value="${fn:length(storeDistance)}" />
					<c:if test="${storeDistanceLength gt '0'}">
						${storeDistance} miles away
					</c:if>
					<c:set var="storeClosingTimeLength" value="${fn:length(storeClosingTime)}" />
					<c:if test="${storeClosingTimeLength gt '0'}">
						<span>&#xb7;</span>${storeClosingTime}
					</c:if>
					
				</p>
				<c:choose>
					<c:when test="${(not empty cookieStoreId) and (cookieStoreId eq storeId)}">
						<div class="make-my-store">
							<div class="my-store"><fmt:message key="checkout.store.myStore"/></div>
						</div>
					</c:when>
					<c:otherwise>
					<dsp:getvalueof param="store.postalCode" var="postalCode"/>
						<c:if test="${isEligibleForfavStore eq true}">
							<p class="make-store-link make-store-link-${storeId}">
							<a class="make-my-store-link make-my-store-link-${storeId}" onclick="javascript:makeMyStore(event,'${contextPath}','${storeId}','${postalCode}','${storeCity}',this);">make this my store</a>
						</p>
						</c:if>
					</c:otherwise>
				</c:choose>
				<div class="tse-scrollable store-locator-tooltip-store-details">
					<div class="tse-scrollbar" >
						<div class="drag-handle"></div>
					</div>
					<div class="tse-scroll-content">
						<div class="tse-content">
							<div class="store-address">
								<p class="address_line_1"><dsp:valueof param="store.address1" /></p>
								<p class="address_line_2">
									<dsp:valueof param="store.city" />,
									&nbsp;<dsp:valueof param="store.stateAddress" />
									&nbsp;<dsp:valueof param="store.postalCode" />
								</p>
								<p><dsp:valueof param="store.phoneNumber"/></p>
								<dsp:getvalueof param="store.address1" var="address1"/>
								<dsp:getvalueof param="store.city" var="city"/>
								<dsp:getvalueof param="store.postalCode" var="postalCode"/>
								<a onclick="getDrivingDirections('${address1}','${city}','${postalCode}');">get directions</a>
							</div>
							
							                          <%--  <dsp:droplet name="ForEach">
                                                       <dsp:param name="array" value="${storeHoursMap}" />
                                                       <dsp:param name="elementName" value="storeTimings" /> --%>
                                                       <c:set var="storeHoursMapLength" value="${fn:length(storeHoursMap)}"/>
                                                      
                                                      <%--  <dsp:oparam name="output">
                                                       <c:if test="${storeHoursMapLength gt '0'}">
                                                              <c:set var="enableStoreHours" value=true/>
                                                              </c:if>
                                                       </dsp:oparam>
                                                       <dsp:oparam name="empty">

                                                       </dsp:oparam>
                                                       </dsp:droplet> --%>
							
							<%--  <dsp:param name="array" value="${storeHoursMap}"/>
							 <c:set var="storeHoursMapLength" value="${fn:length(storeHoursMap)}"/>
							 storeHoursMapLengthstoreHoursMapLengthstoreHoursMapLength ::: ${storeHoursMapLength} --%>
							 <%-- <c:if test="${enableStoreHours eq true }">hello</c:if> --%>
							<c:if test="${storeHoursMapLength gt '0'}">
								<div class="store-hours">
									<div class="weekday"></div><div class="week-timings"></div>
									
										<%-- <dsp:droplet name="ForEach">
											<dsp:param name="array" value="${storeHoursMap}" />
											<dsp:param name="elementName" value="storeTimings" />
											<dsp:oparam name="output">
												<dsp:getvalueof var="serviceName" param="key" />
												<p>
													<dsp:valueof param="key" />
													:
												</p>
											</dsp:oparam>
											<dsp:oparam name="empty">

											</dsp:oparam>
										</dsp:droplet>
									</div>
									<div class="week-timings">
										<dsp:droplet name="ForEach">
											<dsp:param name="array" value="${storeHoursMap}" />
											<dsp:param name="elementName" value="storeTimings" />
											<dsp:oparam name="output">
												<dsp:getvalueof var="serviceName" param="key" />
												<p>${storeHoursMap[serviceName]}</p>
											</dsp:oparam>
											<dsp:oparam name="empty">

											</dsp:oparam>
										</dsp:droplet> --%>
										<c:set var="storeHoursLength" value="${fn:length(storeHoursMap)}" />
										<c:set var="storeHoursIndex" value="1" />
										<div class="hide storeHoursJson">
											{
												"storeHours" : {
												<dsp:droplet name="ForEach">
													<dsp:param name="array" value="${storeHoursMap}" />
													<dsp:param name="elementName" value="storeTimings" />
													<dsp:oparam name="output">
														<dsp:getvalueof var="serviceName" param="key" />
														"<dsp:valueof param="key" />":
													</dsp:oparam>
													<dsp:oparam name="empty">
													</dsp:oparam>
													<dsp:param name="array" value="${storeHoursMap}" />
													<dsp:param name="elementName" value="storeTimings" />
													<dsp:oparam name="output">
														<dsp:getvalueof var="serviceName" param="key" />
													 	"${storeHoursMap[serviceName]}"
													 	<c:if test="${storeHoursIndex ne  storeHoursLength}">,</c:if>
														<c:set var="storeHoursIndex" value="${storeHoursIndex + 1}" />
													</dsp:oparam>
													<dsp:oparam name="empty">
													</dsp:oparam>
												</dsp:droplet>
												}
											}
										</div>
									</div>
							</c:if>

							<br>
							<dsp:droplet name="ForEach">
								<dsp:param name="array" value="${storeServicesVOList}"/>
								<dsp:param name="elementName" value="storeServiceVO"/>
								<dsp:oparam name="outputStart">
									<header>store services</header>
									<ul>
								</dsp:oparam>
								<dsp:oparam name="output">
									<dsp:getvalueof var="storeServiceVO" param="storeServiceVO"/>
									<dsp:getvalueof var="learnMore" value="${storeServiceVO.learnMore}"/>
									<input type="hidden" value="${learnMore}" id="storelocater-lernmorelinkDetails"/>
									<li><dsp:valueof param="storeServiceVO.serviceName" />
										<p><dsp:valueof param="storeServiceVO.serviceDesc" /></p>
										<c:if test="${not empty learnMore}">
										    <a href="javascript:void(0)" data-placement="bottom" data-content="${learnMore}" class="sl-learnmore_mystore">learn more</a>
										</c:if>										
									</li>
								</dsp:oparam>
								<dsp:oparam name="outputEnd">
									</ul>
								</dsp:oparam>
							</dsp:droplet>
						</div>
					</div>
				</div>
			</div>
		</dsp:oparam>
	</dsp:droplet>
	<script type="text/javascript">
	
	/* $(document).ready(function(){
		 var slDetailsInfo=$("#storelocater-lernmorelinkDetails").val();
		  LearnMore Tool Tip ****
		 setTimeout(function(){
		      var slLearnmoreTooltip = $('.store-locator-content .sl-learnmore');
		
		     var slLearnmorePopoverTemplate = '<div  class="popover myclass-popover detailWrapper paypalDdetailsWrapper" role="tooltip"><div class="arrow"></div><p>'+slDetailsInfo+'</p></div>';
		
		     slLearnmoreTooltip.click(function( event ) {
		                                event.preventDefault();
		     });
		
		    /*  slLearnmoreTooltip.popover({
                          animation : 'true',
                          html : 'true',
                          content : 'asdfasdf',
                          placement : 'bottom',
                          template : slLearnmorePopoverTemplate,
                          trigger : 'hover'
		     }); ****
		     
		     
		     
		
		   
										

									}, 500);

						}); */
	</script>
</dsp:page>

