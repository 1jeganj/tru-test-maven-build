 <%-- This page is used to display Store Details in popup. This page accepts storeId as input parameter and
 uses StoreLookupDroplet to display Store Details. 
--%>

<dsp:page>

	<div class="global-nav-your-store-locator-tooltip popup-marker">
		<div class="global-nav-store-locator fav-store-popover popover" role="tooltip">
			<div class="arrow"></div>
			<div class="global-nav-store-details partial">
				<div class="store-locator-content flagstore-locator-content"></div>
			</div>
		</div>
	</div>
			
</dsp:page>

