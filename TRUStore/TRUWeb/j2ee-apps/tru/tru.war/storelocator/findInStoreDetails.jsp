<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/atg/commerce/locations/StoreLookupDroplet"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/multisite/Site" />
<dsp:importbean bean="/com/tru/commerce/locations/TRUStoreDetailsDroplet"/>
	
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	
	<div class="tse-content">
		<dsp:droplet name="StoreLookupDroplet">
			<dsp:param name="id" param="storeId" />
			<dsp:param name="elementName" value="store" />
			<dsp:oparam name="output">
			
				<dsp:droplet name="TRUStoreDetailsDroplet">
					<dsp:param name="storeItem" param="store"/>
					<dsp:oparam name="output">
						<dsp:getvalueof var="storeHoursMap" param="storeHoursMap" />
						<dsp:getvalueof var="storeClosingTime" param="storeClosingTime" />
						<dsp:getvalueof var="storeServicesVOList" param="storeServicesVOList" />
					</dsp:oparam>
				</dsp:droplet>
			
				<div class="store-address">
                    <p><dsp:valueof param="store.address1" /></p>
                    <p><dsp:valueof param="store.city" />,
                    	&nbsp;<dsp:valueof param="store.stateAddress" />
                    	&nbsp;<dsp:valueof param="store.postalCode" /></p>
                    <p>(305) 443-9004</p>
                    <dsp:getvalueof param="store.address1" var="address1"/>
					<dsp:getvalueof param="store.city" var="city"/>
					<dsp:getvalueof param="store.postalCode" var="postalCode"/>
                    <a onclick="getDrivingDirections('${address1}','${city}','${postalCode}');">get directions</a>
                </div>
                <div class="store-hours">
                    <div>
                    	<dsp:droplet name="ForEach">
							<dsp:param name="array" value="${storeHoursMap}"/>
							<dsp:param name="elementName" value="storeTimings"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="serviceName" param="key"/>
								<p><dsp:valueof param="key" />:</p>
								<p>${storeHoursMap[serviceName]}</p>
							</dsp:oparam>
							<dsp:oparam name="empty">
								<fmt:message  key="store_locator.popup.nostore"/>
							</dsp:oparam>
						</dsp:droplet>
                    </div>
                </div>
                <br>
                <dsp:droplet name="ForEach">
					<dsp:param name="array" value="${storeServicesVOList}"/>
					<dsp:param name="elementName" value="storeServiceVO"/>
					<dsp:oparam name="outputStart">
						<header><!-- store services --><fmt:message key="tru.storelocator.label.storeservices" /></header>
						<ul>
					</dsp:oparam>
					<dsp:oparam name="output">
						<dsp:getvalueof var="storeServiceVO" param="storeServiceVO"/>
						<dsp:getvalueof var="learnMore" value="${storeServiceVO.learnMore}"/>
						<li><dsp:valueof param="storeServiceVO.serviceName" />
							<p><dsp:valueof param="storeServiceVO.serviceDesc" /></p> <a>learn more</a>
						</li>
					</dsp:oparam>
					<dsp:oparam name="outputEnd">
						</ul>
					</dsp:oparam>
				</dsp:droplet>
            </dsp:oparam>
        </dsp:droplet>
    </div>
                    
  </dsp:page>