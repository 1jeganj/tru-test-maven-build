<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/atg/commerce/locations/StoreLocatorFormHandler"/>
<dsp:importbean bean="/com/tru/storelocator/cml/GeoLocatorConfigurations"/>
<dsp:importbean bean="/atg/multisite/Site" />

	<dsp:getvalueof var="siteID" bean="Site.id" />
	<dsp:getvalueof var="maxResultsPerPage" bean="GeoLocatorConfigurations.maxResultsPerPage"/>
	 <%--<dsp:getvalueof var="searchableDistance" bean="GeoLocatorConfigurations.searchableDistance"/>--%>
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	              
       <div class="find-in-store-search">
           <h2>find</h2>
           <p>Enter a location: street address, city, state or ZIP</p>
           <div class="find-input-group">
               <%-- <dsp:form id="findInStoreLocatorSearch"  method="post" action="#" formid="findInStoreLocSearch" requiresSessionConfirmation="false">
					<input type="hidden" value="${contextPath}" class="contextpath" name="contextpath"/>
					<dsp:input type="hidden" bean="StoreLocatorFormHandler.siteIds" value="{Toysrus,Babyrus}" id="findInStoreSearchSiteIds"/>
					<dsp:input type="hidden" bean="StoreLocatorFormHandler.maxResultsPerPage" value="${maxResultsPerPage}" id="maxResultsPerPage"/>
					<dsp:input type="hidden" bean="StoreLocatorFormHandler.successURL" value="${contextPath}storelocator/findInStoreSearchResults.jsp"/>
					<dsp:input type="hidden" bean="StoreLocatorFormHandler.errorURL" value="${contextPath}storelocator/findInStoreSearch.jsp"/>
					<dsp:input type="hidden" bean="GeoLocatorConfigurations.storelocatorLat.${siteID}" id="latitudeCode" />
					<dsp:input type="hidden" bean="GeoLocatorConfigurations.storelocatorLng.${siteID}" id="longitudeCode" />
					<dsp:input type="text" bean="StoreLocatorFormHandler.postalAddress" iclass="inline storeLocatorField" maxlength="255" paramvalue="storeLocatorField" autocomplete="off" />
					<dsp:input type="hidden" bean="StoreLocatorFormHandler.distance" value="100"/>
					<dsp:input id="findInStoreSearch" priority="-10" type="hidden" bean="StoreLocatorFormHandler.locateItems" value="find" name="findInStoreSearchName"/>
				</dsp:form>
				<button onclick="javascript:findInStoreSearch();">find</button> --%>
            </div>
      </div>  	
    </dsp:page>