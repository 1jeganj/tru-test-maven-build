<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
 <%-- This page is used to mark selected store as my favorite store
 	and it gets displayed as stylized icon.
--%>

<dsp:importbean bean="/com/tru/commerce/locations/TRUMakeMyStoreDroplet"/>
<dsp:page>
	<dsp:droplet name="TRUMakeMyStoreDroplet">
		<dsp:param name="storeId" param="storeId" />
		<dsp:param name="address" param="address" />
		<dsp:param name="postalCode" param="postalCode" />
		<dsp:param name="storeName" param="storeName" />
		<dsp:oparam name="output">
			<div class="make-my-store">
				<div class="my-store"><fmt:message key="checkout.store.myStore"/></div>
			</div>
		</dsp:oparam>
		<dsp:oparam name="error">
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>

