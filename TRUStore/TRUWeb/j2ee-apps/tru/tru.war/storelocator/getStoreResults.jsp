<%@ page contentType="application/json"%>
<dsp:page>

    <dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />


	<dsp:importbean bean="/atg/commerce/locations/StoreLocatorFormHandler"/>
	<dsp:importbean bean="/atg/multisite/Site" />
	<dsp:importbean bean="/com/tru/storelocator/cml/GeoLocatorConfigurations"/>


	<dsp:importbean bean="/com/tru/storelocator/tol/PARALStoreLocatorTools"/>

	<dsp:getvalueof var="postalAddress" param="postalAddress" />
	<dsp:getvalueof var="skuId" param="skuId" />
	<dsp:getvalueof var="myStoreId" param="myStoreId" />
	<dsp:getvalueof var="itemQuantity" param="quantity" />


	<dsp:getvalueof var="distance" param="distance" />
	<c:if test="${empty distance}">
		<dsp:getvalueof var="distance" bean="PARALStoreLocatorTools.distanceInMiles" />
	</c:if>

	<dsp:getvalueof var="zipcode" param="zipcode" />
	<dsp:getvalueof var="address" param="address" />
	<dsp:getvalueof var="city" param="city" />
	<dsp:getvalueof var="state" param="state" />
	<dsp:setvalue bean="PARALStoreLocatorTools.itemQuantity" value="${itemQuantity}"/>
	<dsp:setvalue bean="StoreLocatorFormHandler.postalAddress" value="${postalAddress}"/>
	<dsp:setvalue bean="StoreLocatorFormHandler.skuId" value="${skuId}"/>


	<%--
		<dsp:setvalue bean="StoreLocatorFormHandler.postalCode" value="${zipcode}"/>
		<dsp:setvalue bean="StoreLocatorFormHandler.address" value="${address}"/>
		<dsp:setvalue bean="StoreLocatorFormHandler.city" value="${city}"/>
		<dsp:setvalue bean="StoreLocatorFormHandler.state" value="${state}"/>
		<dsp:setvalue bean="StoreLocatorFormHandler.siteIds" value="{Toysrus,Babyrus}"/>
	 --%>

	<dsp:setvalue bean="StoreLocatorFormHandler.myStoreId" value="${myStoreId}"/>

	<dsp:setvalue bean="StoreLocatorFormHandler.distance" value="${distance}"/>

	<dsp:setvalue bean="StoreLocatorFormHandler.requestFromPDP" value="true"/>

	<dsp:setvalue bean="StoreLocatorFormHandler.useForwards" value="true"/>

	<dsp:setvalue bean="StoreLocatorFormHandler.errorURL" value="populateErrorDetailsToJson.jsp"/>

	<%--This method will return the json data --%>
	<dsp:setvalue bean="StoreLocatorFormHandler.locateItems" value="Submit"/>

</dsp:page>