<dsp:page>
<dsp:importbean bean="com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="versionFlag" bean="TRUStoreConfiguration.versioningFlag" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
<dsp:getvalueof var="storeLocatorGetDirectionsGoogleMapURL" bean="TRUConfiguration.storeLocatorGetDirectionsGoogleMapURL" />
<dsp:getvalueof var="staticAssetsVersionFormat"	bean="TRUStoreConfiguration.staticAssetsVersion" />
<dsp:getvalueof value='<%=atg.nucleus.DynamoEnv.getProperty("staticAssetVersion")%>' var="versionNumbers" />

	    <tru:pageContainer>
	  		<jsp:attribute name="directionPageName">getDirection</jsp:attribute>
			<jsp:body>
			 <c:choose>
	    	<c:when test="${versionFlag}">
	    		<link rel="stylesheet" href="${TRUCSSPath}css/getDirection.css?${staticAssetsVersionFormat}=${versionNumbers}">		
	    	</c:when>
	    	<c:otherwise>
	    		<link rel="stylesheet" href="${TRUCSSPath}css/getDirection.css">
	    	</c:otherwise>
	    </c:choose>
				<div class="get-direction-page-container">
				    <input id="pac-input-start-formatted" type="hidden" />
				    <input id="pac-input-start-lat" type="hidden" />
				    <input id="pac-input-start-lng" type="hidden" />
				    
				    <input id="pac-input-destination-formatted" type="hidden" />
				    <input id="pac-input-destination-lat" type="hidden" />
				    <input id="pac-input-destination-lng" type="hidden" />
				   
				    <div id="left-panel">
				    	 <div id="direction-inputs-panel">
						    <div id="direction-inputs">
						    	<div class="direction-title">get directions</div>
							    <div id="direction-to"><span id="direction-desc"></span></div>
							    <div class="clear-both"></div>
							    <div class="input-flieds-section">
						    		<div class="start-label">
						    			<div class="field-container">
						    			<input id="pac-input-start" class="controls" type="text" placeholder="enter a location"/>
						    			<label for="pac-input-start" class="floating-label">
						    				<span class="avenir-heavy">enter a location</span>
						    			</label>
						    			</div>
						    		</div>
							    	<div class="end-label">
							    	<div class="field-container">
							    		<input disabled="disabled" id="pac-input-destination" class="controls" type="text" placeholder="enter a destination"/>
							    		<label for="pac-input-destination" class="floating-label">
						    				<span class="avenir-heavy">enter a destination</span>
						    			</label>
							    		</div>
							    	</div>
							    	<span class="toggle-direction"></span>
							    	<div>
						    			<button class="get-direction-btn">get directions</button>
						    		</div>
						    	</div>
						    	<div class="directions-not-found-message">
						    		<div class="not-found-title">We could not find "<span class="not-found-text"></span>"</div>
						    		<div class="not-found-message-empty">Please enter an address</div>
						    		<div class="directions-error-suggestions">
						    			<div class="directions-light">Make sure your search is spelled correctly.</div>
						    			<div class="directions-light">Try adding a city, state, or zip code.</div>
						    		</div>
						    	</div>
						    </div>
					    </div>
					    
					    <div id="direction-details-panel"></div>
				    </div>
				    <div id="map"></div>
				</div>
			    
			    <dsp:getvalueof var="enableGetDirectionParam" param="enableGetDirectionParam" />
			    <c:choose>
			    	<c:when test="${versionFlag}">
			    		<script src="${TRUJSPath}javascript/getDirections.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>		
			    	</c:when>
			    	<c:otherwise>
			    		<script src="${TRUJSPath}javascript/getDirections.js"></script>
			    	</c:otherwise>
			    </c:choose>
			    <script async defer src="${storeLocatorGetDirectionsGoogleMapURL}"></script>
	 		</jsp:body>
		</tru:pageContainer>

</dsp:page>