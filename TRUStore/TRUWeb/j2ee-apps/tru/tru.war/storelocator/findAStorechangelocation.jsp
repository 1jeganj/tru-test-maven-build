 <%-- This page contains StoreLocator form and div tags to display store locator results.--%>

<dsp:page>
<dsp:importbean bean="/atg/commerce/locations/StoreLocatorFormHandler"/>
<dsp:importbean bean="/atg/multisite/Site" />
<dsp:importbean bean="/com/tru/storelocator/cml/GeoLocatorConfigurations"/>

<dsp:getvalueof id="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof var="maxResultsPerPage" bean="GeoLocatorConfigurations.maxResultsPerPage"/>
 <%--<dsp:getvalueof var="searchableDistance" bean="GeoLocatorConfigurations.searchableDistance"/>--%>
<dsp:getvalueof var="siteID" bean="Site.id" />
<dsp:getvalueof var="findStore" param="findStore"/>

			<div class="store-locator-zip ${findStore eq 'true' ? 'nearby-stores-find-a-store' : ''}">
				<h3>
					find <img class="change-location-dismiss"
						src="${TRUImagePath}images/close.png"
						alt="dismiss find a store">
				</h3>
				<p>Enter a location: street address, city, state or ZIP</p>
				<dsp:form id="changestoreLocatorSearch"  method="post" action="#" formid="changestoreLocSearch">
					<input type="hidden" value="${contextPath}" class="contextpath" name="contextpath"/>
					<dsp:input type="hidden" bean="StoreLocatorFormHandler.siteIds" value="{Toysrus,Babyrus}" id="siteIds"/>
					<dsp:input type="hidden" bean="StoreLocatorFormHandler.maxResultsPerPage" value="250" id="maxResultsPerPage"/>
					<dsp:input type="hidden" bean="StoreLocatorFormHandler.successURL" value="${contextPath}storelocator/storeLocatorResults.jsp"/>
					<dsp:input type="hidden" bean="StoreLocatorFormHandler.errorURL" value="${contextPath}storelocator/findAStore.jsp"/>
					<dsp:input type="hidden" bean="GeoLocatorConfigurations.storelocatorLat.${siteID}" id="latitudeCode" />
					<dsp:input type="hidden" bean="GeoLocatorConfigurations.storelocatorLng.${siteID}" id="longitudeCode" />
					<dsp:input type="text" bean="StoreLocatorFormHandler.postalAddress" iclass="inline storeLocatorField" maxlength="255" paramvalue="storeLocatorField" autocomplete="off" />
					<dsp:input type="hidden" bean="StoreLocatorFormHandler.distance" value="100"/>
					<dsp:input id="storeSearch1" priority="-10" type="hidden" bean="StoreLocatorFormHandler.locateItems" value="find" name="storeSearchName"/>
				</dsp:form>
				<button   onclick="javascript:findAStore1();">find</button>
				<br>
			</div>
</dsp:page>