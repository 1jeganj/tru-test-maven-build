<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/atg/commerce/locations/StoreLocatorFormHandler"/>
<dsp:importbean bean="/atg/multisite/Site" />
<dsp:importbean bean="/com/tru/storelocator/cml/GeoLocatorConfigurations"/>
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:getvalueof var="googleUrl" bean="TRUConfiguration.storeLocatorGoogleMapURL"/>
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration"/>
<dsp:importbean bean="/com/tru/storelocator/tol/PARALStoreLocatorTools"/>
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:getvalueof id="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof var="maxResultsPerPage" bean="GeoLocatorConfigurations.maxResultsPerPage"/>
<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>
<dsp:getvalueof var="siteCode" value="TRU" />
<dsp:getvalueof var="baseBreadcrumb" value="tru" />
<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
<dsp:getvalueof var="pageName" bean="TRUTealiumConfiguration.storeLocatorLandingPageName"/>
<dsp:getvalueof var="pageType" bean="TRUTealiumConfiguration.storeLocatorLandingPageType"/>
<dsp:getvalueof var="siteID" bean="Site.id" />
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
<c:if test="${siteID eq babySiteCode}">
	<dsp:getvalueof var="siteCode" value="BRU" />
	<dsp:getvalueof var="baseBreadcrumb" value="bru" />
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
</c:if>
<dsp:getvalueof var="customerFirstName" bean="Profile.firstName"/>
	<dsp:getvalueof var="customerLastName" bean="Profile.lastName"/>
	<c:set var="space" value=" "/>
	<c:choose>
	<c:when test="${empty customerFirstName && empty customerLastName}">
		<c:set var="customerName" value='${fn:substring(customerEmail, 0, fn:indexOf(customerEmail, "@"))}'/>
	</c:when>
	<c:when test="${not empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value="${customerFirstName}"/>
	</c:when>
	<c:when test="${empty customerFirstName && not empty customerLastName}">
		   <c:set var="customerName" value="${customerLastName}"/>
	</c:when>
	<c:otherwise>
			<c:set var="customerName" value="${customerFirstName}${space}${customerLastName}"/>
	</c:otherwise>
 </c:choose>
 <dsp:importbean bean="/atg/multisite/Site" />
	<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id" />
 <c:choose>
	<c:when test="${siteName eq 'ToysRUs'}" >
		<dsp:getvalueof var="contextRoot" value="TRU" />
		</c:when>
			<c:when test="${siteName eq 'BabyRUs'}" >
		<dsp:getvalueof var="contextRoot" value="BRU" />
		</c:when>

		</c:choose>
<dsp:getvalueof var="findStore" param="findStore"/>
<dsp:importbean bean="com/tru/common/TRUStoreConfiguration"/>
	<dsp:getvalueof var="staticAssetsVersionFormat" bean="TRUStoreConfiguration.staticAssetsVersion"/>
	<dsp:getvalueof var="versionFlag" bean="TRUStoreConfiguration.versioningFlag"/>
	<dsp:getvalueof value='<%=atg.nucleus.DynamoEnv.getProperty("staticAssetVersion")%>' var="versionNumbers"/>
	<%-- <dsp:getvalueof var="globalRadius" bean="Site.globalRadius" />
	<dsp:getvalueof var="lightBoxRadius" bean="Site.lightBoxRadius" /> --%>

	<c:set var="store" value="store" scope ="request"/>

<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
<c:choose>
<c:when test="${isSosVar eq 'true'}">
<c:set var="selectedStore" value="sos"/>
</c:when>
<c:otherwise>
<c:set var="selectedStore" value=""/>
</c:otherwise>
</c:choose>

<tru:pageContainer>
<jsp:body>
<input type="hidden" value="${contextRoot}" name="site" id="site"/>
<input type="hidden" value="Store-Locator" name="currentpage" id="currentpage"/>
	<script type='text/javascript'>

		var utag_data = {
			page_name : "${siteCode}: ${pageName}",
			page_type : "${pageType}",
			partner_name:"${partnerName}",
			event_type:"",
			oas_breadcrumb : "${baseBreadcrumb}/storelocator",
		    oas_taxonomy : "level1=storelocator&level2=null&level3=null&level4=null&level5=null&level6=null",
			oas_sizes : [ ['TopRight', [777,34]],['Frame2', [970,90]] ],
			customer_name:"${customerName}",
			selected_store:"${selectedStore}",
			tru_or_bru : "${siteCode}"
		};
	</script>
	<!-- Start: Script for Tealium Integration -->
	<script type="text/javascript">
	    (function(a,b,c,d){
	    a='${tealiumURL}';
	    b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
	    a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
	    })();
	</script>

    <!-- LOADING GOOGLE MAPS-->
    <script src="${googleUrl}"></script>
    <c:choose>
		<c:when test="${versionFlag}">
			 <script type="text/javascript"
			 	src="${TRUJSPath}javascript/markerwithlabel.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
		</c:when>
		<c:otherwise>
			 <script type="text/javascript" src="${TRUJSPath}javascript/markerwithlabel.js"></script>
		</c:otherwise>
	</c:choose>


<input type="hidden" value="<fmt:message  key="storelocator.babiesrus.geoResultsEmpty" />" id="bru_no_results_error"/>
<input type="hidden" value="<fmt:message  key="storelocator.toysrus.geoResultsEmpty"/>"  id="tru_no_results_error"/>
<!-- Start Store Locator Template template -->
<div class="container-fluid store-locator-template" >

    <div class="default-margin ad-zone-777-34">
		<div id="OAS_TopRight"></div>
	</div>
    <div class="store-locator-template-content">
        <div class="store-locator-struct">
            <div class="googleMapGroup">
			<div class="siteLogoForPrintPage">
		    	<span><img src="/images/tru-logo-sm.png" alt="toysrus logo"></span>
				<span> <img src="/images/babiesrus-logo-small.png" alt="babiesrus logo"></span>
			</div>
			 <div id="store-locator-results" class="store-locator-results">
			 	<div id="stores_count_display" class="stores_count_display">
					   <h3 >
							<span class="store-count"> </span> <!--stores nearby  --><fmt:message key="tru.storelocator.label.storesnearby"/>
						</h3>
						<p class="search_term_p"><span class="selectedaddress"></span><span class="sl_search_term"></span></p>
						
						<a href="javascript:void(0);" class="change-zip"><!--change location --><fmt:message key="tru.storelocator.label.changelocation"/></a>
						
						<!-- <div class="store-locator-find-store find-store-dismiss">
							<header>find a store</header>
							<img class="change-location-dismiss" src="${TRUImagePath}images/close.png" alt="dismiss find a store">
							<p>enter an address, city, state or zip</p>
							<div class="find-input-group">
								<input type="text">
								<button>find</button>
							</div>
						</div> -->

						</div>
						<div class="store-locator-zip nearby-stores-find-a-store" >
							<h3 class="slp-findInStore-header">
								<!-- find --><fmt:message key="tru.storelocator.label.find"/><a href="javascript:void(0)" id="change-location-cross"  class="change-location-dismiss"><img  src="${TRUImagePath}images/close.png" alt="dismiss find a store"></a>
							</h3>
							<label class="slp-zip-label" for="storeLocatorAddressEntry"><!--Enter a location: street address, city, state or ZIP  --><fmt:message key="tru.storelocator.label.searchText" var="storeTxt"/></label>
							<p class="sl_wrongsearch_errormsg"></p>
								<input type="hidden" value="${contextPath}" class="contextpath" name="contextpath"/>
								<input type="text" id="storeLocatorAddressEntry"  class="inline" maxlength="255"  autocomplete="off" aria-describedby="findStore-error" placeholder="${storeTxt}"/>
								<input id="findInStoreSearch"  type="hidden" value="find" name="storeSearchName" />

							<button class="" id="storeLocatorFindButton" onclick="findInStoreSearch()">find stores</button>

						</div>
						<p class="sl_errormsg" id="findStore-error"></p>
						<ul id="store-locator-results-tabs" class="store-locator-results-tabs nav nav-tabs" >
							<li id="filterAll" class="active">
								<a href="javascript:void(0)" class="product-review-tab-text" data-toggle="tab"><!-- All --><fmt:message key="tru.storelocator.label.all" /></a>
							</li>
							<li id="filterTRU">
								<a href="javascript:void(0)" class="product-review-tab-text" data-toggle="tab"><!--Toys"R"Us  --><fmt:message key="tru.storelocator.label.toysrustab" /></a>
							</li>
							<li id="filterBRU">
								<a href="javascript:void(0)" class="product-review-tab-text" data-toggle="tab"><!--Babies"R"Us --><fmt:message key="tru.storelocator.label.babiesrustab" /></a>
							</li>
                       </ul>
				       <div id="store-locator-results-scrollable" class="tse-scrollable">


								<div class="tse-content">
									<ul>

									</ul>
									<div id="show-more-stores" class="show-more"><a href="javascript:void(0)"><span><!--show more  --><fmt:message key="tru.storelocator.label.showmore" /></span></a> </div>
								</div>

					 </div>
					 <div class="no_specific_chain_stores"><p></p></div>
				   </div>

                <div class="store-locator-store-details-infowindow">
                    <div class="store-locator-store-details">
                        <h2>
                           <span class="store-loc"></span>
                        </h2>
                         <p class="store-miles"> <span id="distanceInMilesInfoBox">3</span> miles away<span id="closingTimeMsgInfoBox"><span>&#xb7;</span>open until 11pm today</span><span>&#xb7;</span>
                            <span class="makeThisMyStoreInfoBoxSpan"><a href="javascript:void(0)" id="makeThisMyStoreInfoBox"><!-- make this my store --><fmt:message key="tru.storelocator.label.makethismystore"/></a></span>
							<span class="my-store-span" ><span class="my-store"><fmt:message key="checkout.store.myStore"/></span></span>
                         </p>
                        <hr>
                        <div id="store-locator-store-details-scrollable" class="tse-scrollable">

                            <div class="tse-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="store-address section">
                                            <a href="javascript:void(0)"><!--get directions  --><fmt:message key="tru.storelocator.label.getdirections" /></a>
                                        </div>
                                        <div id="storeHoursInfoBox" class="store-hours section">
                                            <div class="inline" id="daysInfoBox">
                                                <p>Mon-Fri:</p>
                                                <p>Sat:</p>
                                                <p>Sun:</p>
                                            </div>
                                            <div class="inline" id="timingsInfoBox">
                                                <p>10:00am-9:00pm</p>
                                                <p>9:00am-9:00pm</p>
                                                <p>10:00am-7:00pm</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h5>store services</h5>
                                        <ul id="storeServicesInfoBox">
                                            <li id="layawayInfoBox">
                                                layaway
                                                <p>Shop today and take time to pay!</p>
                                                <a href="http://www.toysrus.com/helpdesk/index.jsp?display=payment&index=15&subdisplay=options#layaway" target="_blank" title="">learn more</a>
                                            </li>
                                            <li id="parentingClassesInfoBox">
                                                parenting classes
                                                <p>Informative classes on everything baby from breastfeeding to infant CPR.</p>
                                                <a href="http://www.toysrus.com/helpdesk/index.jsp?display=payment&index=15&subdisplay=options#layaway" target="_blank" title="">learn more</a>
                                            </li>
                                            <li id="personalRegistryAdvisorInfoBox">
                                                personal registry advisor
                                                <p>Schedule a one-on-one appointment with a Personal Registry Advisor to help you create the perfect registry.</p>
                                                <a href="http://www.toysrus.com/helpdesk/index.jsp?display=payment&index=15&subdisplay=options#layaway" target="_blank" title="">learn more</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <img src="${TRUImagePath}images/store-details_panel_down-arrow.png" alt="details panel down arrow">
                    </div>
                </div>
                <div class="tooltip-container">
                    <div class="popover" role="tooltip">
                        <div class="arrow">
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                </div>
				  <div id="googleMap">

				 </div>
            </div>
        </div>
    </div>
    <!--  Zone is coming from targetter -->
    <!-- <div class="row stitched"> -->

		<dsp:droplet name="/atg/targeting/TargetingFirst">
              <dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/Storelocator/StoreLocatorCMSContent"/>
              <dsp:param name="howMany" value="10"/>
              <dsp:oparam name="output">
                     <dsp:valueof param="element.data" valueishtml="true"/>
              </dsp:oparam>
       </dsp:droplet>


	<!-- </div> -->
	<!--  Zone is coming from targetter -->
	<div class="row">
		<div class="ad-zone-970-90">
			<div id="OAS_Frame2"></div>
		</div>
	</div>



   <div id="store-locator-results-cloner" ></div>

<div class="results-single-outer">
    <div class="store-locator-results-single">
        <div class="row">
            <div class="results-col-1">
                <div id="addressicon" class="truMyStoreDot"></div>
            </div>
            <div class="results-col-2">
                <h4>store Name</h4>
                <p id="closingTimeMsg"></p>
                <p class="address"></p>
                <a class="make-my-store" href="#"><!--make this my store--><fmt:message key="tru.storelocator.label.makethismystore"/></a>
                <div class="my-store"><fmt:message key="checkout.store.myStore"/></div>
                <a href="javascript:void(0)">store details</a>
            </div>
            <div class="results-col-2">
                <p class="line_height_adjuster_for_details"><span class="miles">3</span><!-- mi.  -->&nbsp;<fmt:message key="tru.storelocator.label.milesaway"/></p>
            </div>
        </div>
        <input type="hidden" class="storeIdOmni" />
    </div>
</div>

<!-- /End Store Locator Template template -->


</div>

</jsp:body>
	</tru:pageContainer>
</dsp:page>