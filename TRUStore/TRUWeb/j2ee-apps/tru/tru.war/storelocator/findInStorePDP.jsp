<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/atg/commerce/locations/StoreLocatorFormHandler"/>
<dsp:importbean bean="/com/tru/storelocator/cml/GeoLocatorConfigurations"/>
<dsp:importbean bean="/atg/multisite/Site" />

	<dsp:getvalueof var="siteID" bean="Site.id" />
	<dsp:getvalueof var="maxResultsPerPage" bean="GeoLocatorConfigurations.maxResultsPerPage"/>
	 <%--<dsp:getvalueof var="searchableDistance" bean="GeoLocatorConfigurations.searchableDistance"/>--%>
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	              
       <div class="find-in-store-search">
	       <img src="${TRUImagePath}images/close.png" alt="close modal">
          <h2><fmt:message key="tru.storelocator.label.find" /></h2>
           <p><!--Enter a location: street address, city, state or ZIP  --><%-- <fmt:message key="tru.storelocator.label.searchText"/>--%></p>
           <div class="find-input-group">
			                    <input type="hidden" value="${contextPath}" class="contextpath" name="contextpath"/>
								<input type="text" id=FindinStorePDPField class="inline" maxlength="255" autocomplete="off" placeholder="enter a street address, city, state or ZIP" />
								<input id="findInStorePDP" type="hidden" value="find" name="storeSearchName"/>
								<button class="" onclick="javascript:findInStorePDP();">find</button>

            </div>
      </div>  	
    </dsp:page>