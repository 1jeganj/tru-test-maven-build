<dsp:page>
	<dsp:importbean bean="/atg/commerce/locations/StoreLocatorFormHandler" />
	
	<dsp:getvalueof id="contextPath" bean="/OriginatingRequest.contextPath"/>
	
	<dsp:getvalueof bean="StoreLocatorFormHandler.currentResultPageNum" var="findInCurrentResultPageNum"/>
	<dsp:getvalueof var="strIndex" param="startIndex"/>
	<dsp:getvalueof var="edIndex" param="endIndex"/>
		<dsp:form id="findInStoreLocatorPagination"  method="post" action="#" formid="findInStoreLocatorPagination">
			<dsp:input type="hidden" value="${strIndex}" bean="StoreLocatorFormHandler.startIndex" id="startIndex"/>
			<dsp:input type="hidden" value="${edIndex}" bean="StoreLocatorFormHandler.endIndex" id="endIndex" />
			<dsp:input type="hidden" bean="StoreLocatorFormHandler.successURL" value="${contextPath}storelocator/findInStoreSearchResults.jsp"/>
			<dsp:input type="hidden" bean="StoreLocatorFormHandler.currentResultPageNum" id="findInCurrentResultPageNum"/>
		</dsp:form>
</dsp:page>
