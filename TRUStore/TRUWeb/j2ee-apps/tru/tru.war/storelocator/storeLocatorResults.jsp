<%-- This page is used to display Store Search Details in an overlay
	along with control to change location already searched upon.
--%>

<dsp:page>

<dsp:importbean bean="/atg/commerce/locations/StoreLocatorFormHandler"/>
<dsp:importbean bean="/com/tru/storelocator/cml/GeoLocatorConfigurations"/>

	<dsp:getvalueof id="contextPath" bean="/OriginatingRequest.contextPath" />
	<dsp:getvalueof var="totalResults" bean="StoreLocatorFormHandler.resultSetSize"/>

	<c:choose>
		<c:when test="${totalResults gt '0'}">
			<h1>${totalResults} nearby stores</h1>
			<p>for 43215</p>
			<a href="javascript:void(0);" class="change-zip">change location</a>
			
			<dsp:include page="/storelocator/findAStore.jsp">
				<dsp:param name="findStore" value="true"/>
			</dsp:include>
			<ul id="store-locator-results-tabs"
				class="tooltip-store-locator-results-tabs nav nav-tabs">
				<li class="site-All"><a class="product-review-tab-text"
					href="#customer-review-summary" data-toggle="tab" onclick="findAllStore('${contextPath}');">All</a></li>
				<li class="site-Toysrus"><a class="product-review-tab-text"
					href="#customer-review-summary" data-toggle="tab" onclick="findSiteStore('Toysrus');">Toys"R"Us</a></li>
				<li class="site-Babyrus"><a class="product-review-tab-text"
					href="#customer-review-summary" data-toggle="tab" onclick="findSiteStore('Babyrus');">Babies"R"Us</a>
				</li>
				<dsp:include page="/storelocator/findSiteStore.jsp"/>
			</ul>
			<div id="nearby-stores" class="tse-scrollable">
				<div class="tse-scrollbar" style="display: none;">
					<div class="drag-handle"></div>
				</div>
				<div class="tse-scroll-content"
					style="width: 100px; height: 420px;">
					<div class="tse-content">
						<dsp:include page="/storelocator/storeLocatorLoadMoreResults.jsp">
						</dsp:include>
					</div>
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<dsp:include page="/storelocator/findAStore.jsp"/>
			Sorry!!! No Stores Found for the given address..."
		</c:otherwise>
	</c:choose>
</dsp:page>