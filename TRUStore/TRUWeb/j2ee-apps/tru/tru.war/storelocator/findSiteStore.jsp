 <%-- This page contains StoreLocator form and div tags to display store locator results.--%>

<dsp:page>
<dsp:importbean bean="/atg/commerce/locations/StoreLocatorFormHandler"/>
<dsp:importbean bean="/atg/multisite/Site" />
<dsp:importbean bean="/com/tru/storelocator/cml/GeoLocatorConfigurations"/>

<dsp:getvalueof id="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof var="maxResultsPerPage" bean="GeoLocatorConfigurations.maxResultsPerPage"/>
 <%--<dsp:getvalueof var="searchableDistance" bean="GeoLocatorConfigurations.searchableDistance"/>--%>
<dsp:getvalueof var="siteID" bean="Site.id" />

	<dsp:form id="storeSiteLocatorSearch"  method="post" action="#" formid="storeSiteLocSearch">
		<input type="hidden" value="${contextPath}" class="contextpath" name="contextpath"/>
		<dsp:input type="hidden" bean="StoreLocatorFormHandler.siteIds" id="siteIds"/>
		<dsp:input type="hidden" bean="StoreLocatorFormHandler.maxResultsPerPage" value="${maxResultsPerPage}" id="maxResultsPerPage"/>
		<dsp:input type="hidden" bean="StoreLocatorFormHandler.successURL" value="${contextPath}storelocator/storeLocatorResults.jsp"/>
		<dsp:input type="hidden" bean="StoreLocatorFormHandler.errorURL" value="${contextPath}storelocator/findAStore.jsp"/>
		<dsp:input type="hidden" bean="GeoLocatorConfigurations.storelocatorLat.${siteID}" id="latitudeCode" />
		<dsp:input type="hidden" bean="GeoLocatorConfigurations.storelocatorLng.${siteID}" id="longitudeCode" />
		<dsp:input type="hidden" bean="StoreLocatorFormHandler.distance" value="100"/>
		<dsp:input id="storeSiteSearch" priority="-10" type="hidden" bean="StoreLocatorFormHandler.locateItems" value="find" name="storeSiteSearchName"/>
	</dsp:form>
</dsp:page>