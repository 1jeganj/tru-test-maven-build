<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/atg/commerce/locations/StoreLocatorFormHandler"/>
<dsp:importbean bean="/com/tru/storelocator/cml/GeoLocatorConfigurations"/>
<dsp:importbean bean="/atg/commerce/locations/StoreLocatorFormHandler"/>
<dsp:importbean bean="/atg/commerce/locations/StoreLookupDroplet"/>
<dsp:importbean bean="/atg/dynamo/droplet/Range" />
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/multisite/Site" />
<dsp:getvalueof param="selectedSkuId" var="pdpskuId" />
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:getvalueof var="relationshipItemId" param="relationshipItemId"/>

<dsp:getvalueof var="quantity" param="quantity"/>

	<dsp:getvalueof var="siteID" bean="Site.id" />
	<dsp:getvalueof var="findStore" param="findStore"/>
	<dsp:getvalueof var="maxResultsPerPage" bean="GeoLocatorConfigurations.maxResultsPerPage"/>
	<dsp:getvalueof var="totalResults" bean="StoreLocatorFormHandler.resultSetSize"/>
	
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	
    <dsp:droplet name="/atg/commerce/catalog/SKULookup"> 
              <dsp:param name="id" value="${pdpskuId}"/> 
              <dsp:param name="elementName" value="SKUItem"/> 
              <dsp:oparam name="output"> 
                <dsp:getvalueof param="SKUItem.primaryImage" var="imgUrl" />
                 <dsp:getvalueof param="SKUItem.displayName" var="skuName"/>  
              </dsp:oparam> 
     </dsp:droplet>
 	
        <div class="modal-dialog">
            <div class="modal-content sharp-border">
                <div class="find-in-store-overlay">
                    <div class="row">
					             <input type="hidden" id="pdpSkuId" value="${pdpskuId}" />
								 <input type="hidden" id="relationshipItemId" value="${relationshipItemId}" />
                     
								<div class="col-md-6 border-right product-side">
									<h3><dsp:valueof value="${skuName}" valueishtml="true" /></h3>
									
								   <dsp:droplet name="IsEmpty">
										<dsp:param name="value" value="${imgUrl}" />
										<dsp:oparam name="false">
											<dsp:droplet name="/com/tru/common/droplet/TRUAkamaiImageDroplet">
												<dsp:param name="imageName" value="${imgUrl}" />
												<dsp:param name="imageHeight" value="250" />
												<dsp:param name="imageWidth" value="240" />
												<dsp:oparam name="output">
													<dsp:getvalueof var="akamaiImageUrl" param="akamaiUrl" />
												</dsp:oparam>
											</dsp:droplet>
											<img  id="product-thumb-storelocator" class="product-thumb" src="${akamaiImageUrl}">
										</dsp:oparam>
										<dsp:oparam name="true">
											<img id="product-thumb-storelocator" class="product-thumb" src="${TRUImagePath}images/no-image500.gif">
										</dsp:oparam>
									</dsp:droplet>
									<div class="sticky-quantity">
										<div class="options-text">
											<fmt:message key="tru.productdetail.label.qty"/> : <span class="amount">${quantity}</span>
										</div>
									</div>
								</div>
                     
		                        <div class="col-md-6 sl_root">
		                        <div class="sl_parent">
								    <input type="hidden" value="${contextPath}" id="contextpathfindinstore" name="contextpath"/>
								     <img class="findin-storemodels-close closeFindInStoreModal" data-dismiss="modal" src="${TRUImagePath}images/close.png" alt="close modal" />
		                            <div class="find-in-store-results" id="find-in-store-results">
		                                <div class="store-locator-results">
		                                    <header>
		                                        <span class="nearby-stores-count"></span> nearby stores
		                                        <p>for <span class="forzip"></span></p>
		                                        <p><a class="change-location">change location</a>
		                                        </p>
		                                    </header>
											<dsp:include page="/storelocator/findInStorePDP.jsp"/>
											<p class="sl_errormsg"></p>
		                                    <ul id="store-locator-results-tabs" class="store-locator-results-tabs nav nav-tabs">
		                                        <li class="All">
		                                            <a class="product-review-tab-text" href="#store-locator-results-scrollable" data-toggle="tab">All</a>
		                                        </li>
		                                        <li>
		                                            <a class="product-review-tab-text" href="#store-locator-results-scrollable" data-toggle="tab">Toys "R" Us</a>
		                                        </li>
		                                        <li>
		                                            <a class="product-review-tab-text" href="#store-locator-results-scrollable" data-toggle="tab">Babies "R" Us</a>
		                                        </li>
		                                    </ul>
		                                   
											<div id="store-locator-results-scrollable" class="tse-scrollable findinstore-pdp-results">
											
											
												<div class="tse-scroll-content">
												  <div class="tse-content">
													<ul>
													 
													</ul>
													 <p class="noresults">No Results Found</p>
													 <a class="separatorPDPa" href="javascript:void(0)">
													 	<div id="show-more-stores" class="show-more forPDP">
													 	show more
													 	</div>
													 </a>
												  </div>	
												</div>
											 </div>
											 <div class="no_specific_chain_stores"><p></p></div>
		                                </div>
		                            </div>
		                        </div>
			                 	<dsp:include page="/storelocator/viewStoreDetails.jsp" />
							  	<dsp:include page="/storelocator/storeDetails.jsp"/>
		                  </div>
                    </div>
                    
                    
                    <div id="store-locator-tooltip-store-detailsPDP" class="tse-scrollable">
						<div class="tse-content">
							<div class="store-address w3c_section">
							</div>
							<div class="store-hours w3c_section">
								<div class="weekday">
								</div>
								<div class="week-timings">
								</div>
							</div>
							<br>
							<header>store services</header>
							<ul>
								<li>
									layaway
									<p>Shop today and take time to pay!</p>
									<a>learn more</a>
								</li>
								<li>
									parenting classes
									<p>Informative classes on everything baby from breastfeeding to infant CPR.</p>
									<a>learn more</a>
								</li>
								<li>
									personal registry advisor
									<p>Schedule a one-on-one appointment with a Personal Registry Advisor to help you create the perfect registry.</p>
									<a>learn more</a>
								</li>
							</ul>
						</div>
					</div>
					
                </div>
            </div>
        </div>
	 <div class="results-single-outer">
		<div class="store-locator-results-single find-in-store-single tru-result">
			<div class="row">
			  <div class="results-col-3">
					<h4>Toys R Us—Boca Raton</h4>
					<p class="address">20429 State Road 7</p>
					<p class="opentimings">open until 11pm today</p>
				    <p class="pdp-shoppingcart-btn"></p>
				</div>
				<div class="results-col-1"></div>
				<div class="results-col-2">
					<p><span class="miles"></span>miles</p>
					<a href="javascript:void(0)" class="findinstore-details" onclick="">details</a>
				</div>
			</div>
			<div class="free-store-sec"><div class="green-list-dot inline"></div><p class="free-store-pickup">outofstock-msg</p></div>
			<div class="row outofstock-sec">
                   <p class="unavailable"><img src="${TRUImagePath}images/unavailable-bullet.png"> <span class="outofstock-msg"></span></p>
            </div>
		</div>
	</div>
	<%-- <script type="text/javascript">
	function omniIspuOutOfStock(storeId){
		if( typeof utag != 'undefined' ){
		utag.link({
   			'event_type':'ispu_out_of_stock',
   			'selected_store':storeId
   			});
		}
	}
	function omniIspuStoreServed(storeId){
		if( typeof utag != 'undefined' ){
		utag.link({
   			'event_type':'ispu_store_served',
   			'selected_store':storeId
   			});
		}
	}
	</script> --%>
	
  </dsp:page>