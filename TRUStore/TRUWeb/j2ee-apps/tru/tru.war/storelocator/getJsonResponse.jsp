<dsp:page>

<dsp:importbean bean="/atg/commerce/locations/StoreLocatorFormHandler"/>
<dsp:importbean bean="/com/tru/storelocator/fhl/GenerateJSONResponseDroplet"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>

<dsp:droplet name="IsEmpty">
	<dsp:param name="value" bean="StoreLocatorFormHandler.locationResults"/>
    <dsp:oparam name="true">
		<json:object>
			<json:property name="error" value="Sorry!!!. No Stores Found for the given address."/>
			<json:property name="isSuccess" value="false"/>
		</json:object>
    </dsp:oparam>
    <dsp:oparam name="false">
    	<dsp:droplet name="GenerateJSONResponseDroplet">
        	<dsp:param name="locationResults" bean="StoreLocatorFormHandler.locationResults"/>
            <dsp:param name="distance" bean="StoreLocatorFormHandler.distance"/>
            	<dsp:getvalueof var="locationList" param="locationArrayList"/>
                	<dsp:oparam name="output">
	                	<json:object>
	                    	<json:property name="stores" value="${locationList}"/>
	                    	<json:property name="isSuccess" value="true"/>
	                    </json:object>
                    </dsp:oparam>
            </dsp:droplet>
    </dsp:oparam>
</dsp:droplet>
	
</dsp:page>