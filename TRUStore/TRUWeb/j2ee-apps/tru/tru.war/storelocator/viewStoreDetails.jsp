 <%-- This page is used to display Store Details in popup. This page accepts storeId as input parameter and
 uses StoreLookupDroplet to display Store Details. 
--%>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>

<dsp:page>

	<div class="global-nav-store-details">
				<div class="store-locator-content">
					<div class="back-to-store">
						<img class="inline" src="${TRUImagePath}images/bacak-to-store-i-c-n.png" alt="">
						<a href="javascript:void(0)"><!--back to store results  --><fmt:message key="tru.storelocator.label.backtostoreresults"/></a>
					</div>
					<a class="showGoogleMapButton" data-href="storelocator/showGoogleMap.jsp">
					    <img id="googlemini-map" src="${TRUImagePath}images/placeholder_for_storeLocator.png" alt="placeholder_for_storeLocator">
					</a> 
					<img class="you-are-here" src="${TRUImagePath}images/placeholder_for_storeLocator.png" alt="You are here">
					<div class="store-locator-store-details">
						<div class="store-address makemy-store-get-directions-wrapper">
							<h2 class="store_name">	</h2>
							<p class="address_line_1"></p>
							<p class="address_line_2"></p>
							<p class="phone-number"></p>						
							<a href="javascript:void(0)" onclick="getDrivingDirections('${address1}','${city}','${postalCode}');" class="get-directions">get directions </a>
						</div>
						<div class="tse-scrollable store-locator-tooltip-store-details">
							<div class="tse-content">
								<%--<div class="store-address">
									<p class="address_line_1"></p>
									<p class="address_line_2"></p>
									<p class="phone-number"></p>
									<a href="javascript:void(0)" onclick="getDrivingDirections('${address1}','${city}','${postalCode}');" class="get-directions">get directions </a>
								</div> --%>
								<div class="store-hours">
									<div class="weekday"></div>
									<div class="week-timings"></div>
								</div>
								<br>
								<h5>store services</h5>
								<ul>
									
								</ul>
							</div>
						</div>
					</div>
				</div>
	</div>
				
	<%-- <script type="text/javascript">
	
	$(document).ready(function(){
		 var slDetailsInfo=$("#storelocater-lernmorelinkDetails").val();
		 /* LearnMore Tool Tip*/
		 setTimeout(function(){
		     var slLearnmoreTooltip = $('.store-locator-content .sl-learnmore');
		
		     var slLearnmorePopoverTemplate = '<div  class="popover myclass-popover detailWrapper paypalDdetailsWrapper" role="tooltip"><div class="arrow"></div><p>'+slDetailsInfo+'</p></div>';
		
		     slLearnmoreTooltip.click(function( event ) {
		                                event.preventDefault();
		     });
		
		     slLearnmoreTooltip.popover({
                          animation : 'true',
                          html : 'true',
                          content : 'asdfasdf',
                          placement : 'bottom',
                          template : slLearnmorePopoverTemplate,
                          trigger : 'hover'
		     });
		 }, 500);
	 });

	
	
	
	</script> --%>
</dsp:page>

