<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/atg/commerce/locations/StoreLocatorFormHandler" />
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	
	<dsp:getvalueof var="maxResultsPerPage" bean="StoreLocatorFormHandler.maxResultsPerPage"/>
	<dsp:getvalueof var="totalResults" bean="StoreLocatorFormHandler.resultSetSize"/>
	
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	
	<dsp:getvalueof var="todayDate" bean="StoreLocatorFormHandler.currentDate"/>
	
		<dsp:droplet name="ForEach">
			<dsp:param name="array" bean="StoreLocatorFormHandler.locationResults" />
			<dsp:param name="elementName" value="store" />
			<dsp:oparam name="outputStart">
				<ul class="">
			</dsp:oparam>
			<dsp:oparam name="output">
				<dsp:getvalueof var="storeId" param="store.repositoryId" />
				<dsp:getvalueof var="storeDistance" param="store.distance" />
				<li>
					<div
						class="store-locator-results-single find-in-store-single tru-result">
						<div class="row">
							<div class="results-col-3">
								<header>
									<a href="javascript:void(0);"><dsp:valueof param="store.name" /></a>
								</header>
								<p><dsp:valueof param="store.address1" /></p>
								<p>open until 11pm today</p>
								<dsp:include page="/storelocator/truStoreAvailabilityLookup.jsp">
									<dsp:param name="selectedSkuId" param="selectedSkuId" />
								</dsp:include>
							</div>
							<div class="results-col-1"></div>
							<div class="results-col-2">
								<p>${storeDistance} miles</p>
								<a href="javascript:void(0);" onclick="javascript:findInStoreDetails('${contextPath}','${storeId}','${storeDistance}');"><fmt:message key="tru.productdetail.label.details" /></a>
							</div>
						</div>
					</div>
				</li>
				<hr class="tru-result">
			</dsp:oparam>
			<dsp:oparam name="empty">
				<fmt:message  key="store_locator.search.nostore"/>
			</dsp:oparam>
			<dsp:oparam name="outputEnd">
				<c:if test="${totalResults > maxResultsPerPage}">
					<dsp:getvalueof bean="StoreLocatorFormHandler.currentResultPageNum" var="findInCurrentResultPageNum"/>
					<div class="show-more-stores find-in-show-more-${currentResultPageNum+1}">
						<button onclick="findInMoreStores('${findInCurrentResultPageNum+1}');">load more</button>
					</div>
					<dsp:include page="/storelocator/findInStoreLocatorPagination.jsp">
						<dsp:param name="startIndex" bean="StoreLocatorFormHandler.startIndex"/>
						<dsp:param name="endIndex" bean="StoreLocatorFormHandler.endIndex"/>
					</dsp:include>
					<div class="find-in-load-more-stores-${currentResultPageNum+1}"></div>
				</c:if>
				</ul>
			</dsp:oparam>
		</dsp:droplet>
</dsp:page>