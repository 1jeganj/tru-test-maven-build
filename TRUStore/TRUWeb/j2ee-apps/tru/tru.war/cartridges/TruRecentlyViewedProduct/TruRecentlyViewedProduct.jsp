<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUItemLevelPromotionDroplet"/>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUDisplayRecentlyViewedItemsDroplet"/>
   <div class="my-account-product-carousel">
            <div class="product-carousel-title">
                recently viewed items
            </div>
                    <div id="slider-product-block-container" class="slider-product-block-container" >
         
        <!-- <div class="slider-product-block-container">
		
            <div debug-id="slide_container"></div></div> -->
			<div data-u="slides" id="slides-product-block" class="slides-product-block">
				<dsp:droplet name="TRUDisplayRecentlyViewedItemsDroplet">
					<dsp:param name="profile" bean="/atg/userprofiling/Profile" />
					<dsp:oparam name="outputStart">
					</dsp:oparam>

					<dsp:oparam name="output">
					
						<dsp:getvalueof param="productInfo" var="productInfo" />
						<dsp:getvalueof param="productInfo.productId" var="productId" />
						<dsp:getvalueof param="productInfo.displayName" var="displayName" />
						<dsp:getvalueof param="productInfo.defaultSKU.id" var="skuId" />
						<dsp:getvalueof param="productInfo.defaultSKU.primaryImage" var="primaryImage" />
						<dsp:getvalueof param="productInfo.defaultSKU.mfrSuggestedAgeMin" var="suggestedAgeMin" />
						<dsp:getvalueof param="productInfo.defaultSKU.mfrSuggestedAgeMax" var="suggestedAgeMax" />
						<dsp:getvalueof param="productInfo.defaultSKU.reviewRating" var="reviewRating" />

						<div class="product-block-padding">
							<div class="product-block product-block-unselected no-select">
								<div class="product-flag"></div>
								<div class="product-choose">
									<div class="product-compare-checkbox-container">
										<input class="product-compare-checkbox" type="checkbox">
										<label class="product-compare-checkbox-label">compare</label>
									</div>
								</div>
								<div class="product-selection">
									<div class="selection-checkbox-off"></div>
									<div class="selection-text-off">Select</div>
								</div>
								<div class="product-block-image-container">
									<div class="product-block-image">
										<div class="product-image">
											<a class="bnsImgBlock" href="${TRUImagePath}images/Toolkit.html"> <img
												class="product-block-img"
												src="${primaryImage}" alt="Product Image">
											</a>
											<div>
												<div class="quick-view-container">
													<div class="product-hover-view product-quick-view"
														data-toggle="modal" data-target="#quickviewModal"
														onclick="loadQuickView('11111823586','')">
														<!-- quick view -->
														<fmt:message key=" tru.productdetail.label.quickview" />
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="colors">
									<div class="color-block red color-selected">
										<div class="color-block-inner"></div>
									</div>
									<div class="color-block yellow">
										<div class="color-block-inner"></div>
									</div>
									<div class="color-block orange">
										<div class="color-block-inner"></div>
									</div>
								</div>
								<div class="wish-list-heart-block">
									<div class="wish-list-heart-off"></div>
								</div>
								<div class="star-rating-block">
								<fmt:parseNumber var="reviewRating" type="number" value="${reviewRating}" parseLocale="en_US"/>
								       <c:forEach begin="1" end="${reviewRating}">
											<div class="star-rating-on"></div>
										</c:forEach>

										<c:forEach begin="1" end="${5-reviewRating}">
											<div class="star-rating-off"></div>
										</c:forEach>
								</div>
								<div class="product-block-information">
									<div class="product-name">
										<a href="./Toolkit_files/Toolkit.html">${displayName}</a>
									</div>
									<div class="age-range">Age:&nbsp;${suggestedAgeMin}-${suggestedAgeMax}&nbsp;years</div>
									<dsp:include page="/jstemplate/recentProductPrice.jsp">
									   <dsp:param name="skuId" value="${skuId}" />
										<dsp:param name="productId" value="${productId}" />
									</dsp:include>
								</div>
								  								<dsp:droplet name="/atg/commerce/catalog/SKULookup">
									<dsp:param name="id" value="${skuId}" />
									<dsp:param name="elementName" value="SKUItem" />
									<dsp:oparam name="output">
										<dsp:getvalueof param="SKUItem" var="defaultSkuItem"></dsp:getvalueof>
										<dsp:getvalueof var="adjustments" value="${defaultSkuItem.skuQualifiedForPromos}"/>
									</dsp:oparam>
								</dsp:droplet>

								<dsp:droplet name="TRUItemLevelPromotionDroplet">
									<dsp:param name="adjustments" value="${adjustments}" />
									<dsp:oparam name="output">
										<dsp:getvalueof var="rankOnePromotion"
											param="rankOnePromotion" />
										<dsp:getvalueof var="promotions" param="promotions" />
										<dsp:getvalueof var="promotionCount" param="promotionCount" />
									</dsp:oparam>
								</dsp:droplet>
								<c:if test="${not empty rankOnePromotion}">
								   <div class="product-block-incentive">
								      <dsp:valueof value="${rankOnePromotion.displayName}" />
								   </div>
							    </c:if>
								<div class="promo-block-container">
									<div class="promotion-block">
										<div class="header">
											<h2>20% off</h2>
											<p>all Graco Stollers-online only</p>
										</div>
										<br> <img src="./Toolkit_files/subcat-promo-img.jpg"
											alt="cannot find image">
										<button class="shop-now">shop now</button>
									</div>
								</div>
								<div class="product-block-border"></div>
							</div>
							<div class="slider-product-block-loading-div" data-u="loading">
								<div></div>
							</div>
						</div>
						</dsp:oparam>
						</dsp:droplet>

			</div>
			
			<div data-u="navigator" id="multi-slide-bullets" class="jssor-bullet">
               <div data-u="prototype" class="av"></div>
			</div>
			
			<span id="multi-carousel-left-arrow" data-u="arrowleft" class="jssora11l"></span>
			<span id="multi-carousel-right-arrow" data-u="arrowright" class="jssora11r"></span>
		</div>
        </div>
    </dsp:page>