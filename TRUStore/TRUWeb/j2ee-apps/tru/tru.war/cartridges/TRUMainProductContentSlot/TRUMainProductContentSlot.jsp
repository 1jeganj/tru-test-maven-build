<dsp:page>
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<%-- Start for loop --%>
	<dsp:getvalueof var="resultsListItem" param="resultsListItem"/>
	<dsp:getvalueof var="totalNumRecs" param="totalNumRecs"/>
	<dsp:getvalueof var="isCompareProduct" param="isCompareProduct"/>
	<dsp:getvalueof var="categoryId" param="categoryId"/>
	<dsp:getvalueof var="hpageType" param="hpageType"/>

	<c:forEach var="element" items="${contentItem.contents}">
		<c:forEach var="element1" items="${element.MainProduct}">
			<dsp:renderContentItem contentItem="${element1}">
				<dsp:param name="resultsListItem" value="${resultsListItem}" />
				<dsp:param name="totalNumRecs" value="${totalNumRecs}" />
				<dsp:param name="isCompareProduct" value="${isCompareProduct}" />
				<dsp:param name="categoryId" value="${categoryId}" />
				<dsp:param name="hpageType" value="${hpageType}" />
			</dsp:renderContentItem>
		</c:forEach>
	</c:forEach>
</dsp:page>