<dsp:page>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations" />
<dsp:importbean bean="/atg/endeca/assembler/cartridge/manager/AssemblerSettings" />
<dsp:importbean bean="/com/tru/common/TRUComponentExists" />
	<dsp:getvalueof var="atgTargeterpathCheck" bean="TRUStoreConfiguration.targeterpathCheck" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
	<dsp:importbean bean="/atg/dynamo/Configuration" var="Configuration"/>
	<dsp:importbean bean="/com/tru/common/TRUConfiguration" var="TRUConfiguration"/>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/>
	<dsp:getvalueof var="contextPath" value="${originatingRequest.contextPath}"/> 
	<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id" />
	<dsp:importbean bean="/com/tru/droplet/TRUGetSiteDroplet" />
	<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
	<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale" />
	<dsp:getvalueof var="previewPromoEnableInStaging" bean="TRUConfiguration.previewPromoEnableInStaging"/>
	<dsp:getvalueof var="akamaiSiteParam" bean="TRUConfiguration.akamaiSiteParam"/>
	<dsp:getvalueof var="bruSiteCheck" bean="EndecaConfigurations.bruSiteCheck"/>
	<dsp:getvalueof var="previewEnabled" bean="AssemblerSettings.previewEnabled"/>
	<input type="hidden" id="previewPromoEnableInStaging" value="${previewPromoEnableInStaging}"/>
	
	
	<%-- START:: Changes made for creating Site cookie for every page request --%>
	<c:forEach var="entry" items="${TRUConfiguration.customParamToSiteIdMapping}">
		<c:if test="${entry.value eq siteName}">
			<dsp:getvalueof var="akamaiSiteCookieValue" value="${entry.key}"/>
		</c:if>
	</c:forEach>
	<c:if test="${TRUConfiguration.enableSiteCookieCreationInJS}">
		<input type="hidden" id="currentSiteCookieParam" value="${akamaiSiteParam}"/>
		<input type="hidden" id="currentSiteCookieValue" value="${akamaiSiteCookieValue}"/>
	</c:if>
	<%-- END:: Changes made for creating Site cookie for every page request --%>

	<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
	<dsp:droplet name="GetSiteTypeDroplet">
		<dsp:param name="siteId" value="${siteName}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="site" param="site"/>
			<c:set var="site" value ="${site}" scope="request"/>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
		<dsp:oparam name="output">
		<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
		</dsp:oparam>
	</dsp:droplet>
	<input type="hidden" id="minSearchLength" value="${content.minAutoSuggestInputLength}"/>
	<dsp:getvalueof var="componentPath"	value="${content.HeaderLinkContentTargeterPath}" />
	<div class="nav-bar-fixed global-nav">
		<div class="global-navigation-top">
        	<nav class="navbar">
            	<div class="navbar-top container-fluid">
					<div class="navbar-header">
						<div class="navbar-brand">
							<span class="navbar-top-breadcrumb"></span>   	
                        	   	<c:choose>
									<c:when test="${siteName eq 'ToysRUs'}">
									<dsp:droplet name="TRUGetSiteDroplet">
										<dsp:param name="siteId" value='BabyRUs' />
										<dsp:oparam name="output">
											<dsp:getvalueof var="siteUrl" param="siteUrl"/>
										</dsp:oparam>
									</dsp:droplet>
									<c:if test="${not empty siteUrl and bruSiteCheck eq true and previewEnabled ne true and fn:contains(siteUrl,'.toysrus.com') and fn:indexOf(siteUrl,'aos')==-1}">
									<dsp:getvalueof var="siteUrl" value="${fn:replace(siteUrl,'.toysrus.com','.babiesrus.com')}"/>
									</c:if>
									<a href="${scheme}${siteUrl}" title="BRU-logo" data-ab-link="BRU-Logo">
										<div id="logo-swap-sm" class="bru-logo-sm inline domain-identity" data-site-identity-cookie-name="${akamaiSiteParam}" data-site-identity-cookie-value="bru"></div>
									</a>
								</c:when>
								<c:otherwise>
									<dsp:droplet name="TRUGetSiteDroplet">
										<dsp:param name="siteId" value='ToysRUs' />
										<dsp:oparam name="output">
											<dsp:getvalueof var="siteUrl" param="siteUrl"/> 
										</dsp:oparam>
									</dsp:droplet>
									<a href="${scheme}${siteUrl}?site=tru" title="TRU-logo" data-ab-link="TRU-Logo">
										<div id="logo-swap-sm" class="tru-logo-sm inline domain-identity" data-site-identity-cookie-name="${akamaiSiteParam}" data-site-identity-cookie-value="tru"></div>
									</a>
	                            </c:otherwise>
                           	</c:choose>
                 		</div>
               		</div>
               		<span id="sosHeaderFrg"></span>
                    <ul class="nav navbar-nav navbar-right">
                    <c:if test="${previewPromoEnableInStaging}">
                       <li class="" data-original-title="" title="">
						    <span><a href="${contextPath}preview/previewDate.jsp"> <span class='label-text'>PromotionPreview</span></a></span>
						</li>
					</c:if>
                        <li id="my-baby-reg">
                            <span>
                                <!-- <a href="https://babyregistrysoftlaunch.babiesrus.com/sign-in"> Fix - GEOFUS-1684 -->
                                <a href="https://babyregistrysoftlaunch.babiesrus.com/home">
                                    <span>
                                        <em class='sprite sprite-baby-registry '></em>
                                        <span class='label-text'>baby registry</span>
                                    </span>
                                </a>
                            </span>
                        </li>
						<c:if test="${content.findastore}">
							<dsp:include page="/header/findastore.jsp"/>
						</c:if>
						<c:choose>
							<c:when test="${site eq 'sos' and isSosVar}">
								<dsp:getvalueof var="atgTargeterpath"	value="/atg/registry/RepositoryTargeters/TRU/Header/SOSHeaderLinkTargeter" />
								<dsp:getvalueof var="cacheKey"	value="${atgTargeterpath}${siteName}${locale}" />
								<dsp:droplet name="/com/tru/cache/TRUSOSHeaderLinkTargeterCacheDroplet">
									<dsp:param name="key" value="${cacheKey}" />
									<dsp:oparam name="output">
										<%-- 	Start TargetingFirst  for rendering the targeter --%>
										<dsp:droplet name="/atg/targeting/TargetingFirst">
											<dsp:param name="targeter" bean="${atgTargeterpath}" />
											<dsp:oparam name="output">
												<dsp:valueof param="element.data" valueishtml="true" />
											</dsp:oparam>
										</dsp:droplet>
									</dsp:oparam>
								</dsp:droplet>
							</c:when>
							<c:otherwise>
								<dsp:getvalueof var="atgTargeterpath"	value="/atg/registry/RepositoryTargeters${componentPath}" />
								<dsp:droplet name="TRUComponentExists">
									<dsp:param name="path" value="${atgTargeterpath}"/><dsp:oparam name="true"><dsp:getvalueof var="atgTargeterpathCheck" value="true"/></dsp:oparam>
								    <dsp:oparam name="false"></dsp:oparam>
	                            </dsp:droplet>
								<c:if test="${not empty componentPath && atgTargeterpathCheck eq 'true'}">
									<dsp:getvalueof var="cacheKey"	value="${atgTargeterpath}${siteName}${locale}" />
									<dsp:droplet name="/com/tru/cache/TRUHeaderLinksCacheDroplet">
										<dsp:param name="key" value="${cacheKey}" />
										<dsp:oparam name="output">
											<%-- 	Start TargetingFirst  for rendering the targeter --%>
											<dsp:droplet name="/atg/targeting/TargetingFirst">
												<dsp:param name="targeter" bean="${atgTargeterpath}" />
												<dsp:oparam name="output">
													<dsp:valueof param="element.data" valueishtml="true" />
												</dsp:oparam>
											</dsp:droplet>
										</dsp:oparam>
									</dsp:droplet>
								</c:if>
							</c:otherwise>
						</c:choose>
              		</ul>
             	</div>
         	</nav>
      	</div>
      	<dsp:include page="/header/MegaMenuCategoryContentTargeterPath.jsp"/>
      	<dsp:include page="/header/storeLocatorPopup.jsp"/>
      	<%-- <div id="exploreBox" class="clearfix"></div> --%>
      	<input type="hidden" id="sites" value="<c:out value="${site}"/>"/>
	</div>
</dsp:page>