<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/>
	<c:forEach var="element" items="${content.contents}">
	 <dsp:renderContentItem contentItem="${element}" />
	 </c:forEach>
<dsp:page>