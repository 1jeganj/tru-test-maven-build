<dsp:page>
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:getvalueof var="componentPath"	value="${contentItem.componentPath}" />
<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters${componentPath}"/>
	<c:if test="${not empty componentPath}">
		<dsp:droplet name="/atg/targeting/TargetingForEach">
			<dsp:param name="targeter" bean="${atgTargeterpath}"/>
			<dsp:oparam name="output">
				<dsp:valueof param="element.data" valueishtml="true"/>
			</dsp:oparam>
		</dsp:droplet>
	</c:if>	
</dsp:page>
