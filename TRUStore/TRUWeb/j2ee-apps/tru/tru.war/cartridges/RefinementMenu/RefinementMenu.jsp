<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations" />
	<dsp:importbean
		bean="/com/tru/commerce/droplet/TRUFacetSelectionsDroplet" />
	<dsp:getvalueof var="contentItem"
		vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:getvalueof var="cnt" value="1" />
	<dsp:getvalueof var="refinementCrumbs" param="refinementCrumbs" />
	<h3	class="filter-window-header ui-accordion-header ui-state-default ui-corner-all ui-accordion-icons"
		role="tab" id="ui-id-3" aria-controls="ui-id-4" aria-selected="false"
		aria-expanded="false" tabindex="0">
		<span class="ui-accordion-header-icon ui-icon ui-icon-plus"></span>
		<div class="filter-window-header-text">${contentItem.name}</div>
	</h3>
	<div
		class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
		id="ui-id-4" aria-labelledby="ui-id-3" role="tabpanel"
		aria-hidden="true">
		<dsp:getvalueof var="cnt" value="1" />
		<c:choose>
			<c:when test="${contentItem.dimensionName eq 'product.category'}">
				<c:forEach var="refinement" items="${contentItem.refinements}">
					<div>
						<input type="checkbox" class="sub-category-filter-option-checkbox"
							id="${fn:toLowerCase(contentItem.name)}${cnt}"> <label
							for="${fn:toLowerCase(contentItem.name)}${cnt}">${refinement.label}(${refinement.count})</label>
						<input type="hidden" class="navigationId" value="${refinement.navigationState}" />
						<dsp:getvalueof var="cnt" value="${cnt+1}" />
					</div>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<dsp:droplet name="TRUFacetSelectionsDroplet">
					<dsp:param name="refinements" value="${contentItem.refinements}" />
					<dsp:param name="refinementcrumbs" value="${refinementCrumbs}" />
					<dsp:param name="dimensionname"
						value="${contentItem.dimensionName}" />
					<dsp:oparam name="nonselected">
						<dsp:getvalueof var="refinement" param="refinement" />
						<dsp:getvalueof var="navCheck"
							value="${refinement.navigationState}" />
						<%-- DCOM-1053 changes --%>

						<dsp:getvalueof var="navurl" value="${refinement.navigationState}" />
						<div>
							<input type="checkbox"
								class="sub-category-filter-option-checkbox"
								id="${fn:toLowerCase(contentItem.name)}${cnt}"> <label
								for="${fn:toLowerCase(contentItem.name)}${cnt}">${refinement.label}(${refinement.count})</label>								
							<input type="hidden" class="navigationId" value="${navurl}" />
							<dsp:getvalueof var="cnt" value="${cnt+1}" />
						</div>
					</dsp:oparam>
					<dsp:oparam name="selected">
						<dsp:getvalueof var="refinements" param="refinements" />
						<dsp:getvalueof var="selectedRefinement"
							param="selectedRefinement" />
						<dsp:getvalueof var="removeNavCheck"
							value="${refinement.removeAction.navigationState}" />
						<div>
							<input type="checkbox"
								class="sub-category-filter-option-checkbox" checked="true"
								id="${fn:toLowerCase(contentItem.name)}${cnt}">
							<dsp:getvalueof var="navurl"
								value="${selectedRefinement.removeAction.navigationState}" />
							<label for="${fn:toLowerCase(contentItem.name)}${cnt}">${selectedRefinement.label}</label>
							<input type="hidden" class="navigationId" value="${navurl}" />
							<dsp:getvalueof var="cnt" value="${cnt+1}" />
						</div>
					</dsp:oparam>
				</dsp:droplet>
			</c:otherwise>
		</c:choose>
		<a class="more-values" href="javascript:void(0)">see more</a>
	</div>
</dsp:page>