<dsp:page>
    <dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
    <dsp:importbean bean="/com/tru/common/TRUComponentExists" />
	<dsp:getvalueof var="atgTargeterpathCheck" bean="TRUStoreConfiguration.targeterpathCheck" />
	<dsp:importbean bean="/atg/targeting/TargetingFirst" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
	<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale" />
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/>
	<dsp:getvalueof var="componentPath"	value="${content.componentPath}" />
	<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters${componentPath}"/>
	
	<dsp:droplet name="TRUComponentExists">
	<dsp:param name="path" value="${atgTargeterpath}"/><dsp:oparam name="true"><dsp:getvalueof var="atgTargeterpathCheck" value="true"/></dsp:oparam>
    <dsp:oparam name="false"></dsp:oparam>
	</dsp:droplet>

<%-- 	Rendering Content From ATG Targeter --%>
<div class="row-max-width">
<c:if test="${not empty componentPath && atgTargeterpathCheck eq 'true'}">
	
	<dsp:getvalueof var="cacheKey" value="${atgTargeterpath}${siteId}${locale}" />
	<dsp:droplet name="/com/tru/cache/TRUCategoryTopBrandCache">
		<dsp:param name="key" value="${cacheKey}" />
			<dsp:oparam name="output">
			<%-- 	Start TargetingFirst  for rendering the targeter --%>
				<dsp:droplet name="/atg/targeting/TargetingFirst">
						<dsp:param name="targeter" bean="${atgTargeterpath}"/>
						<dsp:oparam name="output">
							<dsp:valueof param="element.data" valueishtml="true"/>
						</dsp:oparam>
					</dsp:droplet>
			<%-- End TargetingFirst  for rendering the targeter --%>		
			</dsp:oparam>
		</dsp:droplet>
	</c:if>	 
</div>   
</dsp:page>