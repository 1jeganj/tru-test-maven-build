<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:importbean bean="/atg/multisite/Site"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
	<dsp:oparam name="output">
	<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
	</dsp:oparam>
</dsp:droplet>
<tru:pageContainer>
<jsp:attribute name="fromsiteMap">siteMap</jsp:attribute>
<dsp:getvalueof var="navigation"  param="N"/>




<div class="default-margin">
<c:forEach items="${content.MainProduct}" var="mainContent">
<c:if test="${mainContent['@type'] eq 'TRUCrumbFacets' }">
	<!--apgination and breadcrumb start-->
	<dsp:getvalueof var="breadCrumbs" value="${mainContent}"/>
		<div class="row">
			<div class="col-md-6">
				<div class="row product-breadcrumb-row">
					<div class="col-md-8 product-breadcrumb-col">
						<div>	
							<ul class="product-breadcrumb">
									<li>
										<a href="/">
											home
										</a>
									</li>
								<c:forEach var="refCrumb" items="${breadCrumbs.refinementCrumbs}">
								<c:if test="${refCrumb.displayName eq 'product.category'}">
									<c:if test="${not empty refCrumb.ancestors}">
										<c:forEach items="${refCrumb.ancestors}"var="ancestor">
										<dsp:droplet name="/atg/commerce/endeca/cache/DimensionValueCacheDroplet">
											<dsp:param name="repositoryId" value="${ancestor.properties['category.repositoryId']}" />
											<dsp:oparam name="output">
												<dsp:getvalueof var="dimCacheObject" param="dimensionValueCacheEntry" />
											</dsp:oparam>
										</dsp:droplet>
										<li><img src="images/breadcrumb-right-arrow-small.png" alt=""></li>
										<li><a href="${dimCacheObject.url}" class="gray-link">${ancestor.label}</a></li>								
										</c:forEach>	
									</c:if>			
									
									<dsp:droplet name="/atg/commerce/endeca/cache/DimensionValueCacheDroplet">
										<dsp:param name="repositoryId" value="${refCrumb.properties['category.repositoryId']}" />
										<dsp:oparam name="output">
											<dsp:getvalueof var="dimCacheObject" param="dimensionValueCacheEntry" />
										</dsp:oparam>
									</dsp:droplet>
									<li><img src="images/breadcrumb-right-arrow-small.png" alt=""></li>
									<li><a href="${dimCacheObject.url}" class="gray-link">${refCrumb.label}</a></li>	
									</c:if>
								</c:forEach>								
								
																					
							</ul>
						</div>
					</div>               
				</div>
			</div>
	
		
</c:if>
<c:if test="${mainContent['@type'] eq 'TRUResultsList' }">
<dsp:getvalueof var="resultsList" value="${mainContent}"/>
<dsp:getvalueof var="recsPerPage"  value="${resultsList.recsPerPage}" />
<dsp:getvalueof var="totalNumRecs"  value="${resultsList.totalNumRecs}" />
<dsp:getvalueof var="pagingActionTemplate"  value="${resultsList.pagingActionTemplate}" />

<dsp:getvalueof var="numberOfPages"  value="${totalNumRecs/recsPerPage + 1}" />
		<div class="col-md-12 text-right">
				<!-- <div class="product-breadcrumb-row"> -->
					<div class="product-breadcrumb-col">
						<div>	
							<ul class="pagination pagination-sm">
							<c:set var="count" value="1"/>
									<c:set var="No" value="0"/>
								
							<c:forEach begin="1" end="${numberOfPages}">
							 <li><a href="${contextPath}siteMapResults?N=${navigation}&No=${No}">${count}</a></li>
							 <dsp:getvalueof var="count" value="${count+1}"/>
							  <dsp:getvalueof var="No" value="${No+recsPerPage}"/>
							 
							</c:forEach>
							
							</ul>
						</div>
					</div>               
				<!-- </div> -->
			</div>
		</div>
		<!--apgination and breadcrumb END-->
<div class="site-map-wrapper">
<c:if test="${not empty resultsList.records}">
<c:forEach items="${resultsList.records}" var="record">
	<dsp:getvalueof var="hasPriceRange" value="${record.attributes['hasPriceRange']}" />
			<dsp:getvalueof var="isStrikeThrough" value="${record.attributes['isStrikeThrough']}" />
			<div class="row">
				<div class="col-md-8">
			<a href="${scheme}${record.detailsAction.recordState}">${record.attributes['sku.displayName']}</a>
					<p>by: ${record.attributes['sku.brand']}</p>
				</div>
				<div class="col-md-4 text-right">
				<c:choose>
		<c:when test="${hasPriceRange eq 'false'}">
		<c:choose>
		<c:when test="${isStrikeThrough eq 'true'}">
		<p>Our Price:&nbsp;<fmt:message key="pricing.dollar" />
		<fmt:parseNumber var="salePrice" type="number" value="${record.attributes['sku.salePrice']}" parseLocale="en_US"/> 
		<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"	value="${salePrice}" />
		
		</p>
		</c:when>
		<c:otherwise>
		<p>Our Price:&nbsp;<fmt:message key="pricing.dollar" />
		<fmt:parseNumber var="listPrice" type="number" value="${record.attributes['sku.listPrice']}" parseLocale="en_US"/> 
		<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listPrice}"/>
		</p>
		</c:otherwise>
		</c:choose>
		</c:when>
		<c:otherwise>
		<p>Our Price:&nbsp;<fmt:message key="pricing.dollar" />
			    <fmt:parseNumber var="minimumPrice" type="number" value="${record.attributes['minimumPrice']}" parseLocale="en_US"/>
				<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${minimumPrice}" /> 
				<fmt:parseNumber var="maximumPrice" type="number" value="${record.attributes['maximumPrice']}" parseLocale="en_US"/> - 
				<fmt:message key="pricing.dollar" />
				<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${maximumPrice}" /></p>
		</c:otherwise>
		</c:choose>
				
				</div>
			</div>
			<hr>
</c:forEach>
</c:if>
</div>		
		<!--bottom pagination start-->
		<div class="row">
			<div class="col-md-12 text-right">
				<div class="">
					<div class="product-breadcrumb-col">
						<div>	
							<ul class="pagination pagination-sm">
								<c:set var="count" value="1"/>
									<c:set var="No" value="0"/>
								
							<c:forEach begin="1" end="${numberOfPages}">
							 <li><a href="${contextPath}siteMapResults?N=${navigation}&No=${No}">${count}</a></li>
							 <dsp:getvalueof var="count" value="${count+1}"/>
							  <dsp:getvalueof var="No" value="${No+recsPerPage}"/>
							 
							</c:forEach>
							</ul>
						</div>
					</div>               
				</div>
			</div>
	   </div>
	   <!--bottom pagination end-->
</c:if>

</c:forEach>

		
   </div>
<jsp:body>
</jsp:body>
</tru:pageContainer>
</dsp:page>