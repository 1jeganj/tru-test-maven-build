<dsp:page>
	<dsp:getvalueof param="seoH1" var="seoH1" />
	<c:choose>
		<c:when test="${not empty seoH1}">
			<div class="row row-no-padding">
				<div class="col-md-12 col-no-padding">
					<div class="my-store-header">
						<h1 class="my-store-header-title">${seoH1}</h1>
					</div>
				</div>
			</div>
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>
</dsp:page>