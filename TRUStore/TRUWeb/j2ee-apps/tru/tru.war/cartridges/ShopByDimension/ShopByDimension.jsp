<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath"/>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" bean="/OriginatingRequest.contentItem" />
	<dsp:getvalueof var="requestURI" bean="/OriginatingRequest.RequestURI"/>
	<dsp:getvalueof var="dimensionsList" vartype="java.util.List" value="${content.dimensionsList}"/>
	<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id" />
	<c:choose>
		<c:when test="${fn:contains(requestURI, 'shopByBrand')}">
			<fmt:message key="tru.shopByBrand.label.allBrands" var="brandsOrCharactersZone" />
		</c:when>
		<c:otherwise>
			<fmt:message key="tru.shopByCharacter.label.allCharacters" var="brandsOrCharactersZone" />
		</c:otherwise>
	</c:choose>
	<div class="shop-by-character-list">
		<h4>${brandsOrCharactersZone}</h4>
		<ul id="shopByTabs" class="shop-by-tabs nav nav-tabs">
			<li class="active">
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab" aria-expanded="false">A</a>
			</li>
			<li class="">
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab" aria-expanded="false">B</a>
			</li>
			<li class="">
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab" aria-expanded="false">C</a>
			</li>
			<li class="">
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab" aria-expanded="false">D</a>
			</li>
			<li class="">
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab" aria-expanded="false">E</a>
			</li>
			<li class="">
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab" aria-expanded="false">F</a>
			</li>
			<li class="">
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab" aria-expanded="false">G</a>
			</li>
			<li class="">
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab" aria-expanded="true">H</a>
			</li>
			<li class="">
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab" aria-expanded="false">I</a>
			</li>
			<li>
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab">J</a>
			</li>
			<li class="">
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByK" data-toggle="tab" aria-expanded="false">K</a>
			</li>
			<li>
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab">L</a>
			</li>
			<li>
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab">M</a>
			</li>
			<li>
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab">N</a>
			</li>
			<li>
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab">O</a>
			</li>
			<li>
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab">P</a>
			</li>
			<li>
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab">Q</a>
			</li>
			<li>
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab">R</a>
			</li>
			<li>
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab">S</a>
			</li>
			<li>
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab">T</a>
			</li>
			<li>
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab">U</a>
			</li>
			<li>
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab">V</a>
			</li>
			<li>
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab">W</a>
			</li>
			<li>
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab">X</a>
			</li>
			<li>
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab">Y</a>
			</li>
			<li>
				<a class="product-review-tab-text" href="http://cloud.toysrus.resource.com/redesign/template-shop-by-template.html#shopByA" data-toggle="tab">Z</a>
			</li>

		</ul>
		<c:choose>
			<c:when test="${siteName eq 'BabyRUs'}">
				<c:set var="letterClassName" value ="shop-by-letter-image-bru"/>
			</c:when>
			<c:otherwise>
				<c:set var="letterClassName" value ="shop-by-letter-image"/>
			</c:otherwise>
		</c:choose>
		<div class="tab-content active-letter" id="shop-by-sort-list-items">
			<div class="tab-pane active row ">
				<div class="col-md-3">
				
					<div class="${letterClassName}">
					<span>A</span>
					</div>
				</div>
				<div class="col-md-9">
					<ul class="listContainer">
						<c:forEach var="element" items="${dimensionsList}">
						<c:set var="brandClass" value="${fn:replace(element.dimensionName,' ', '')}" />
							<li class="${fn:toLowerCase(brandClass)}"><dsp:a page="${siteURL}${element.dimensionURL}" >${element.dimensionName}</dsp:a></li>
						</c:forEach>
					</ul>
				</div>
			</div>
		</div>
	</div>
</dsp:page>