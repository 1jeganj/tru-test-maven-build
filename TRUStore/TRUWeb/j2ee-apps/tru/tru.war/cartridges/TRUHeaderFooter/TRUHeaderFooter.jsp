<dsp:page>
  <dsp:getvalueof var="contentItem"	vartype="com.endeca.infront.assembler.ContentItem"	param="contentItem"/>
	<%-- Start for loop --%>
    <c:forEach var="element" items="${contentItem.contents}">
      <dsp:renderContentItem contentItem="${element}"/>
    </c:forEach>
	<%-- End for loop--%>

</dsp:page>