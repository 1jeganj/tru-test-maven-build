<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof value="${originatingRequest.contextPath}"
		var="contextPath" />
	<dsp:getvalueof var="contentItem"
		vartype="com.endeca.infront.assembler.ContentItem"
		bean="/OriginatingRequest.contentItem" />
	<dsp:getvalueof var="contextPath"
		value="${originatingRequest.contextPath}" />
	<dsp:getvalueof var="currentCatId"
		value="${contentItem.CurrentCategoryId}" />
	<dsp:getvalueof var="twoLevelCategoryTreeMap"
		value="${contentItem.twoLevelCategoryTreeMap}" />
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
	<dsp:getvalueof var="categoryImageResizeMap" bean="TRUStoreConfiguration.categoryImageResizeMap" />
	<dsp:getvalueof var="imageHeight" value=""/>
 	<dsp:getvalueof var="imageWidth" value=""/>
 	<c:forEach var="entry" items="${categoryImageResizeMap}">
		<c:if test="${entry.key eq 'imageHeight'}">
			<c:set var="categoryImageHeight" value="${entry.value}" />
     	</c:if>
     	<c:if test="${entry.key eq 'imageWidth'}">
			<c:set var="categoryImageWidth" value="${entry.value}" />
     	</c:if>
	</c:forEach>
	<c:if test="${not empty twoLevelCategoryTreeMap}">
		<!-- <div class="row-footer-margin"> -->
			<div class="row row-no-padding">
				<div class="col-md-12 col-no-padding">
					<div class="browse-by"><fmt:message key="tru.subcat.browse"/></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div id="slider-image-block-container"
						class="slider-image-block-container">
							<div class="slider-image-block-loading-div" data-u="loading">
									<div></div>
								</div>
								
									<div id="slides-image-block" data-u="slides"
										class="slides-image-block">
										<c:forEach var="levelTwoCategory"
											items="${twoLevelCategoryTreeMap[currentCatId]}">
										<div class="hover-hover">
											<!-- <a class="product-image" href="javascript:void(0)"> --> 
											<c:if test="${ not empty levelTwoCategory.categoryImage}">
											<dsp:droplet name="/com/tru/common/droplet/TRUAkamaiImageDroplet">
												<dsp:param name="imageName" value="${levelTwoCategory.categoryImage}"/>
												<dsp:param name="imageHeight" value="${categoryImageHeight}"/>
												<dsp:param name="imageWidth" value="${categoryImageWidth}"/>
												<dsp:oparam name="output">
													<dsp:getvalueof var="akamaiImageUrl" param="akamaiUrl"/>
												</dsp:oparam>
											</dsp:droplet>
											<div class="sabcat-slides-image-block">
											<a class="bnsImgBlock" href="${levelTwoCategory.categoryURL}&catdim=bcmb">
											<img src="${akamaiImageUrl}" alt="category block image" class="product-block-img subcat-product-bloack-img"> </a>
											</div>
												</c:if>
									<c:if test="${ empty levelTwoCategory.categoryImage}">
									<dsp:droplet name="/com/tru/common/droplet/TRUAkamaiImageDroplet">
												<dsp:param name="imageHeight" value="${categoryImageHeight}"/>
												<dsp:param name="imageWidth" value="${categoryImageWidth}"/>
												<dsp:oparam name="output">
													<dsp:getvalueof var="akamaiImageUrl" param="akamaiUrl"/>
												</dsp:oparam>
									</dsp:droplet>
									<div class="sabcat-slides-image-block">
									<a class="bnsImgBlock" href="${levelTwoCategory.categoryURL}&catdim=bcmb"><img src="${akamaiImageUrl}" alt="" class="product-block-img">
									</a>
									</div>
									</c:if>
									<div class="product-label-brand ">
									${levelTwoCategory.categoryName}</div>
									</div>
										<!-- Repeated section -->
									</c:forEach>
									</div>

								<div data-u="navigator" id="multi-slide-bullets" class="jssor-bullet">

								<div data-u="prototype"></div>
								</div>
								<span id="multi-carousel-left-arrow" data-u="arrowleft"
									class="jssora11l">
								</span>
								<span id="multi-carousel-right-arrow" data-u="arrowright" class="jssora11r">
								</span>
							</div>
				</div>
			</div>
		<!-- </div> -->
		
	</c:if>
</dsp:page>