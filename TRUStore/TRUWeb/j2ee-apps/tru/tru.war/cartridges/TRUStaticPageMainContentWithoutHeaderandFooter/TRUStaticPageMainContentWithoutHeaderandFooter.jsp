<dsp:page>
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />	
	<dsp:importbean bean="/com/tru/droplet/TRUContentLookupDroplet" />
	<c:if test="${contentItem['@type'] eq 'TRUStaticPageMainContentWithoutHeaderandFooter'}">	
		<dsp:droplet name="TRUContentLookupDroplet">
			<dsp:param name="articleContentKey" value="${contentItem['contentKey']}"/>
			<dsp:oparam name="output">
				<dsp:getvalueof var="articleBody" param="articleBody" />
					${articleBody}
			</dsp:oparam>
		</dsp:droplet>
	</c:if>
</dsp:page>

