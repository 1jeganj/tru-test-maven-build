<dsp:page>
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:getvalueof var="previewEnabled" bean="/atg/endeca/assembler/cartridge/manager/AssemblerSettings.previewEnabled"/>
	<c:set var="rootContentItem" scope="request" value="${contentItem}"/>
	
	<%-- Start for loop --%>
	<dsp:getvalueof param="keyword" var="searchKey" />
	<dsp:importbean bean="/atg/multisite/Site" />

	<dsp:setvalue param="searchKeyWord" value="${searchKey}" />
	<dsp:getvalueof var="displayGiftFinderTab" value="${contentItem.displayGiftFinderTab}" scope="request"/>
	<dsp:getvalueof var="staticAssetsTargeter" value="${contentItem.componentPath}" scope="request"/>

	<c:if test="${not empty staticAssetsTargeter}">
		<c:set var="staticAssetsTargeter" value="${staticAssetsTargeter}" scope="request" />
	</c:if>

	<c:forEach var="element" items="${contentItem.contentPaths}">
		<dsp:setvalue param="pageName" value="${element}" />
	</c:forEach>
	
	<c:if test="${contentItem['endeca:contentPath'] eq '/shopby'}">
		<dsp:getvalueof var="shopbylanding" value="shopby" scope="request" />
	</c:if>
	
	<dsp:setvalue param="pageType" value="${contentItem['@type']}" />
	
	<c:choose>
 		<c:when test="${previewEnabled}">
 			<%--
      			When in an Endeca preview environment include the paths to the Endeca
      			preview Javascript and CSS files.
    		--%>
    		<c:if test="${empty pageHeadDefined}">
 				<!DOCTYPE html> <!--  From page container jsp -->
				<html lang="en">
				<head>
     				<endeca:pageHead rootContentItem="${contentItem}"/>
     				<c:set var="pageHeadDefined" scope="request" value="true"/>
    		</c:if>
    		<endeca:previewAnchor contentItem="${contentItem}">
	    		<c:forEach var="element" items="${contentItem.contents}">
					<c:forEach var="element1" items="${element.MainHeader}">
						<c:if test="${element1['@type'] eq 'TruBreadcrumbs'}">
							<dsp:getvalueof var="refinementCrumbs" value="${element1.refinementCrumbs[0]}" />
							<dsp:getvalueof var="catrepositoryId" value="${refinementCrumbs.properties['category.repositoryId']}" scope="request" />
						</c:if>
					</c:forEach>
					<dsp:renderContentItem contentItem="${element}" />
				</c:forEach>
			</endeca:previewAnchor>
 		</c:when>
 		<c:otherwise>
 			<c:forEach var="element" items="${contentItem.contents}">
				<c:forEach var="element1" items="${element.MainHeader}">
					<c:if test="${element1['@type'] eq 'TruBreadcrumbs'}">
						<dsp:getvalueof var="refinementCrumbs" value="${element1.refinementCrumbs[0]}" />
						<dsp:getvalueof var="catrepositoryId" value="${refinementCrumbs.properties['category.repositoryId']}" scope="request" />
					</c:if>
				</c:forEach>
				<dsp:renderContentItem contentItem="${element}" />
			</c:forEach>
 		</c:otherwise>
 	</c:choose>
</dsp:page>