<dsp:page>
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath"/>
	<tru:pageContainer>
		<jsp:attribute name="fromShopBy">ShopBy</jsp:attribute>
		<jsp:body>
			<ul class="shop-by-breadcrumb">
				<li>
					<a href="/">
						home
					</a>
				</li>
				<li>shop by characters/themes</li>
			</ul>
			<div class="shop-by-header">
				<header>
					featured characters
				</header>
				<hr><%-- ${contextPath }/${contextPath}/images/brandpage/brandpage --%>
			</div>
			<c:forEach var="element" items="${contentItem.MainHeader}">
      			<dsp:renderContentItem contentItem="${element}"/>
    		</c:forEach>
    		<dsp:include page="/brandIndex/brandIndexMenu.jsp"/>
		</jsp:body>
	</tru:pageContainer>
</dsp:page>