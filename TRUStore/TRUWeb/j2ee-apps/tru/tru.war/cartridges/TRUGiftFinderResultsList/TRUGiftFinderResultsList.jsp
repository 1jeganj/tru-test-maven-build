<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/>
 <dsp:include page="/common/sortOption.jsp">
        <dsp:param name="contentItem" value="${content}" />
        </dsp:include>
	<div id="filtered-products">
		<div class="fade-to-black" id="product-disabled-modal"></div>
		<div class="row-footer-margin">
			<div class="row row-no-padding">
	<dsp:include page="/browse/productsSection.jsp">
        <dsp:param name="contentItem" value="${content}" />
        </dsp:include>
</div>
<dsp:getvalueof param="subcat" var="subcat" />
<c:if test="${(not empty subcat) and (subcat eq 'subcat')}">
<div id="loadMoreProducts" class="family-load-more read-more text-center">
                    load 24 more items
 </div>					
</c:if>
</div>
</div>
</dsp:page>