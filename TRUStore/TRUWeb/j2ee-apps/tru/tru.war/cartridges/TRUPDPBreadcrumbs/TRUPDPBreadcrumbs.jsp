<dsp:page>
	<dsp:importbean
		bean="/com/tru/droplet/TRUCategoryPrimaryPathDroplet" />
	<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations" />
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
	<dsp:getvalueof var="homeBread" bean="EndecaConfigurations.homeBread" />
	<dsp:getvalueof var="contentItem"
		vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:getvalueof param="collectionPage" var="collectionPage" />
	<dsp:getvalueof param="productDisplayName" var="productDisplayName" />
	<dsp:getvalueof param="skuId" var="skuId" />
	<dsp:getvalueof param="collectionId" var="collectionId" />

	<div class="product-breadcrumb-row default-margin">
		<dsp:droplet name="/com/tru/droplet/TRUCategoryPrimaryPathDroplet">
			<dsp:param name="skuId" value="${skuId}" />
			<dsp:param name="collectionId" param="collectionId" />
			<dsp:oparam name="output">
			<dsp:getvalueof var="primaryPathCategoryVOList" param="primaryPathCategoryVOList" />
			</dsp:oparam>
		</dsp:droplet>
	
		<ul class="product-breadcrumb">
		<!--  BackToRegistryWishlist  -->
			<li class=""><div class="backToRegistryOrWishlist"></div></li>
			<li class=""><a href="${contextPath}" class="breadcrumb-home-icon">home</a></li>
			<c:forEach var="breadList" items="${primaryPathCategoryVOList}">
				<c:if
					test="${not empty breadList.categoryDisplayStatus and breadList.categoryDisplayStatus ne 'NO DISPLAY'}">
					<li class="pdp-update-breadcrumb">
						<a href="${breadList.categoryURL}&catdim=bcmb" class="gray-link">${breadList.categoryName}</a>
					</li>
				</c:if>
			</c:forEach>
		</ul>
	</div>

</dsp:page>
