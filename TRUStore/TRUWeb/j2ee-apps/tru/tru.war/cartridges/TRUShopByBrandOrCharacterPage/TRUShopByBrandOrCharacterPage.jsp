<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	<dsp:getvalueof var="previewEnabled" bean="/atg/endeca/assembler/cartridge/manager/AssemblerSettings.previewEnabled"/>
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
	<dsp:getvalueof var="requestURI" bean="/OriginatingRequest.RequestURI"/>
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath"/>
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration"/>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>
	<dsp:getvalueof var="siteCode" value="TRU" />
	<dsp:getvalueof var="baseBreadcrumb" value="tru" />
	<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
	<dsp:getvalueof var="pageName" bean="TRUTealiumConfiguration.shopbyPageName"/>
	<dsp:getvalueof var="pageType" bean="TRUTealiumConfiguration.shopbyPageType"/>
	<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
	<dsp:getvalueof var="customerFirstName" bean="Profile.firstName"/>
	<dsp:getvalueof var="customerLastName" bean="Profile.lastName"/>
	<dsp:getvalueof var="customerEmail" bean="Profile.login"/>

	<c:set var="space" value=" "/>
	<c:choose>
		<c:when test="${empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value='${fn:substring(customerEmail, 0, fn:indexOf(customerEmail, "@"))}'/>
		</c:when>
		<c:when test="${not empty customerFirstName && empty customerLastName}">
				<c:set var="customerName" value="${customerFirstName}"/>
		</c:when>
		<c:when test="${empty customerFirstName && not empty customerLastName}">
			   <c:set var="customerName" value="${customerLastName}"/>
		</c:when>
		<c:otherwise>
				<c:set var="customerName" value="${customerFirstName}${space}${customerLastName}"/>
		</c:otherwise>
 	</c:choose>

	<c:if test="${siteId eq babySiteCode}">
		<dsp:getvalueof var="siteCode" value="BRU" />
		<dsp:getvalueof var="baseBreadcrumb" value="bru" />
		<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
	</c:if>
	<c:choose>
		<c:when test="${fn:contains(requestURI, 'shopByBrand')}">
			<fmt:message key="tru.shopByBrand.label.shopByBrands" var="shopByBreadcrumb" />
			<fmt:message key="tru.shopByBrand.label.featuredBrands" var="shopByIconListing" />
		</c:when>
		<c:otherwise>
			<fmt:message key="tru.shopByCharacter.label.shopByCharacters" var="shopByBreadcrumb" />
			<fmt:message key="tru.shopByCharacter.label.featuredCharacters" var="shopByIconListing" />
		</c:otherwise>
	</c:choose>
	<dsp:droplet name="/com/tru/common/droplet/TRUTealiumCategoryModifierDroplet">
		<dsp:param name="categoryName" value="/${shopByBreadcrumb}"/>
		<dsp:param name="pageName" value="null"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="modifiedBreadcrumb" param="modifiedCategory"/>
			<dsp:getvalueof var="taxonomyString" param="breadCrumbLevels"/>
		</dsp:oparam>
	</dsp:droplet>

	<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
	<c:choose>
		<c:when test="${isSosVar eq 'true'}">
			<c:set var="selectedStore" value="sos"/>
		</c:when>
		<c:otherwise>
			<c:set var="selectedStore" value=""/>
		</c:otherwise>
	</c:choose>
	<tru:pageContainer>
		<jsp:attribute name="fromShopBy">ShopBy</jsp:attribute>
		<jsp:body>

			 <c:choose>
			 <c:when test="${previewEnabled}">
			    <endeca:pageBody rootContentItem="${rootContentItem}" contentItem="${contentItem}">

			<script type='text/javascript'>
				var isMegaMenuClicked = localStorage.getItem('pageFrom');
				var utag_data = {
					page_name : "${siteCode}: ${pageName}",
					page_type : "${pageType}",
					partner_name:"${partnerName}",
					oas_breadcrumb : "${baseBreadcrumb}${modifiedBreadcrumb}",
				    oas_taxonomy : "${taxonomyString}",
					oas_sizes : [ ['Frame2', [970,90]]],
					flash_spot:"",
					event_type:"",
					customer_name:"${customerName}",
					selected_store:"${selectedStore}",
					tru_or_bru : "${siteCode}"
				};

				if ( typeof isMegaMenuClicked === 'string' && (ismegamenuClicked.indexOf('megamenuClick') > -1) ) {
					utag_data.event_type = ["shop_by_mega_menu", "gridwall_impression"];
					localStorage.setItem("pageFrom",'');
				}
			
				(function(a,b,c,d){
					a='${tealiumURL}';
					b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
					a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
				})();
			</script>
			<%-- End: Script for Tealium Integration --%>

			<div class="template-content">
				<ul class="shop-by-breadcrumb">
					<li>
						<a href="/">
							home
						</a>
					</li>
					<li>${shopByBreadcrumb}</li>
				</ul>
				<div class="shop-by-header">
					<header>
						${shopByIconListing}
					</header>
					<hr><%-- ${contextPath }/${contextPath}/images/brandpage/brandpage --%>
				</div>

				<c:forEach var="element" items="${contentItem.MainHeader}">
	      			<dsp:renderContentItem contentItem="${element}"/>
	    		</c:forEach>
    		</div>

    		</endeca:pageBody>
			 </c:when>
			 <c:otherwise>
			 <%-- <!-- start: Script for Tealium Integration --> --%>
			 <script type='text/javascript'>
			 	var isMegaMenuClicked = localStorage.getItem('pageFrom');
				var utag_data = {
					page_name : "${siteCode}: ${pageName}",
					page_type : "${pageType}",
					partner_name:"${partnerName}",
					oas_breadcrumb : "${baseBreadcrumb}${modifiedBreadcrumb}",
				    oas_taxonomy : "${taxonomyString}",
					oas_sizes : [ ['Frame2', [970,90]]],
					flash_spot:"",
					event_type:"",
					customer_name:"${customerName}",
					selected_store:"${selectedStore}",
					tru_or_bru : "${siteCode}"
				};

				if ( typeof isMegaMenuClicked === 'string' && (isMegaMenuClicked.indexOf('megaMenuClick') > -1) ) {
					utag_data.event_type = ["shop_by_mega_menu", "gridwall_impression"];
					localStorage.setItem("pageFrom",'');
				}
			
				(function(a,b,c,d){
					a='${tealiumURL}';
					b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
					a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
				})();
			</script>
			<%-- End: Script for Tealium Integration --%>

			<div class="template-content">
				<ul class="shop-by-breadcrumb">
					<li>
						<a href="/">
							home
						</a>
					</li>
					<li>${shopByBreadcrumb}</li>
				</ul>
		
				<div class="shop-by-header">
					<header>
						${shopByIconListing}
					</header>
					<hr><%-- ${contextPath }/${contextPath}/images/brandpage/brandpage --%>
				</div>

				<c:forEach var="element" items="${contentItem.MainHeader}">
	      			<dsp:renderContentItem contentItem="${element}"/>
	    		</c:forEach>
    		</div>
			 </c:otherwise>
			 </c:choose>

 		</jsp:body>
	</tru:pageContainer>
</dsp:page>