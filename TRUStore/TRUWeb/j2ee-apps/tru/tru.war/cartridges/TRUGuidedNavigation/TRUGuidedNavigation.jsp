<dsp:page>
    <fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/com/tru/commerce/droplet/TRUConvertContentItemToJSON" />
	<dsp:importbean bean="/atg/multisite/Site" />

	<script>
		var searchResponseNavigation = <dsp:droplet name="TRUConvertContentItemToJSON">
		<dsp:param name="contentItems" bean="/OriginatingRequest.contentItem" />
		</dsp:droplet>;
	</script>
	<dsp:getvalueof var="subcat" param="subcat" />
	<dsp:getvalueof var="family" param="family" />
	<dsp:getvalueof var="Ntt" param="keyword" />
	<dsp:getvalueof var="defaultval" param="defaultval" />
	
	<dsp:getvalueof var="siteID" bean="Site.id" />
	<input id="currentEndecaSiteId" type="hidden"  value="${siteID}" />
	<c:choose>
		<c:when test="${siteID eq 'ToysRUs'}">
		<c:set var="tealiumSite" value="TRU"/>	
		</c:when>
		<c:when test="${siteID eq 'BabyRUs'}">
		<c:set var="tealiumSite" value="BRU"/>
		</c:when>
		<c:otherwise>
			
		</c:otherwise>
	</c:choose>
	
	<c:choose>
		<c:when test="${family eq 'family'}">
		<c:set var="tealiumPage" value="Family"/>	
		</c:when>
		<c:when test="${Ntt ne 'empty' }">
		<c:set var="tealiumPage" value="Search"/>
		</c:when>
		<c:otherwise>
			
		</c:otherwise>
	</c:choose>
	
	<div class="fixed-narrow-menu" id="guidedNavigation">
		<!-- sorting start -->
		<div class="row default-margin">
			<div class="col-md-6 col-no-padding col-narrow-by">
				<div class="narrow-by-container">
					<button class="narrow-by-button compare-toggle" id="sub-category-narrow-by-button" type="button" title="narrow-by-button"> <!-- modified for B&S Spark UI Changes. -->
						<span class="narrow-by-button-label selected">
						<!-- <span class="filter-icon-BG">
							<img src="images/filter.png" class="ICN-Filter">
						</span> -->
							<!-- <span class="expanded"></span> 
							<span class="minimized"></span> -->
						</span>
						
					</button>
				</div>
					<div class="filter-results">
					<div class="row filter-header-section">
						<span class="filter-text">filters</span>
						<!-- ko if: totalRecords()>lastRecordNumber()  -->
							<span class="showing-results">
								<span data-bind="text:numCMSTotal"></span>&nbsp;<fmt:message key="tru.search.result"/>
							</span>
						<!-- /ko -->
					 	<!-- ko if: totalRecords()==lastRecordNumber() || totalRecords()<lastRecordNumber()  -->
							<span class="showing-results">
								<span data-bind="text:numCMSTotal"></span>&nbsp;<fmt:message key="tru.search.result"/>
							</span>
						<!-- /ko -->
					</div>
					<div class="row default-margin">
						<div class="col-md-12 col-no-padding sub-cat-category-wrapper">
							<div class="sub-cat-category-output">
								<!-- ko foreach: $root.refinementCrumbsArray -->
									<div class="sub-cat-filter-list inline">
										<span data-bind="text: label"></span>&nbsp;(<span data-bind="text: count"></span>)
										<div data-bind="click: $root.unselectRefinement,attr:{id:'clearTopFilter'}" class="dismiss-x inline"></div>
										
									
									</div>
								<!-- /ko  -->
							</div>
							<a href="javascript:void(0)" class="sub-cat-more hide">more...</a> 
							<a href="javascript:void(0)" class="clear-all-filterss hide" data-bind="click:$root.unselectRefinement,attr:{id:$root.clearAllFilter()}">clear All</a>
							<div data-bind="click:$root.unselectRefinement,attr:{id:$root.clearAllFilter()}" class="dismiss-x inline clearAllFilter"></div>
						</div>
					</div>
				</div>
			</div>
			
			<!-- sorting Start -->
			<div class="col-md-6 text-right col-narrow-by">
				<%-- <div class="btn-group sort-byNew"><!-- Sorting Button start -->
					<span class="showing-results sort-by-titleNew"><fmt:message key="subcat.sortby"/></span>
				</div> --%>
				<!-- Sorting Button end -->
				<div class="btn-group sort-by" id="sort-by-dropdown">
					<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						 <span class="sort-by-title" data-bind="text:sortBylabel"></span>
						 <span id="sort-by-arrow" class="sort-by-down-arrow"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><img class="sort-by-dropdown-arrow" src="images/arrow-grey.svg" alt="arrow_grey_svg" aria-label = "dropdown_arrow" title="arrow_grey_svg" /></li>
						<!-- ko foreach:sortOptionsArray -->
							<li>
							
								<!-- ko if:$root.pageName() === 'SearchPage'  -->
									<!-- ko if:$root.changeBestMatch($root.pageflag()) -->
										<a href="javascript:void(0);" data-bind="click:$root.sortOption " onclick='javascript:utag.link({"engagement_impression":"${tealiumSite}: ${tealiumPage}:Sort"});'>
											<span data-bind = "text:label" ></span>
										</a>
									<!-- /ko -->
								<!-- /ko -->
								<!-- ko ifnot:$root.pageName() === 'SearchPage' -->
								<!-- ko if:label !== 'best match'  -->
									<!-- ko if:$root.changeBestSell($root.sellflag(),$root.defaultSortOption()) -->
									<a href="javascript:void(0);" data-bind="click:$root.sortOption " onclick='javascript:utag.link({"engagement_impression":"${tealiumSite}: ${tealiumPage}:Sort"});'>
									<span data-bind = "text:label" ></span>
									</a>
									<!-- /ko -->
								<!-- /ko -->	
								<!-- /ko -->
							</li>
						<!--/ko -->
					</ul>
				</div>
			</div>
			<!-- sorting End -->
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<hr class="thick-hr">
			</div>
		</div>
		<div class="compare-menu sub-cat-container in">
		<div id='side-overlay'></div>
			<div class="tse-scrollable facets-container">
				<div class="tse-scroll-content facets-container-scroll-content">
					<div class="tse-content">
						<div data-bind="foreach: { data: refinementsArray, afterRender:reInitializeAccordain }" id="filter-accordion" 
							class="filter-window-container ui-accordion ui-widget ui-helper-reset" role="tablist">
							<!-- ko if: refinements.length>0 -->
							
								<h3 class="filter-window-header ui-accordion-header ui-state-default ui-corner-all ui-accordion-icons"
									role="tab" id="ui-id-3" aria-controls="ui-id-4" aria-selected="false" aria-expanded="false" tabindex="0">
									<span class="ui-accordion-header-icon ui-icon ui-icon-plus"></span>
									<span class="filter-window-header-text">
										<span class="findMe" data-bind="text:name"></span>
									</span>
									
								</h3>
								
								<div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"
									 aria-labelledby="ui-id-3" role="tabpanel" aria-hidden="true" data-bind="attr:{id:'ui-id-'+$index()}">
									 <!-- <div id='side-overlay'></div>  for W3c violation -->
									 <div class="tse-scrollable">
										<div class="tse-scroll-content">
											<div class="tse-content">
									<!-- ko foreach: $root.refinementCrumbsArray -->
										<!-- ko if: (dimensionName ===$parent.dimensionName)  && dimensionName != 'product.category' -->
											<div>
												<input id="selectedFacet" type="checkbox" checked class="sub-category-filter-option-checkbox" >
												<label class="filter-lbl-container" data-bind="click: $root.unselectRefinement">
													<span class="filter-lbl"></span><span class="filter-text" data-bind="text: label+'&nbsp;('+count+')'"></span>
												</label>
											</div>
										<!-- /ko -->
									<!-- /ko -->
								<!-- ko if: (dimensionName !='sku.itemDimensionMap.itemWeight' && dimensionName !='sku.reviewRatDim') -->


									
										<!-- ko foreach: refinements -->
										<!-- ko ifnot: $data.dummyVal -->
											<div>
												<input type="checkbox" class="sub-category-filter-option-checkbox" /> 
												<label class="filter-lbl-container" data-bind="click: $root.refineMentOpntion">
													<span class="filter-lbl"></span><span class="filter-text" data-bind="text: label+'&nbsp;('+count+')'"></span>
												</label>
											</div>
									
										<!-- /ko -->
										<!-- /ko -->
									<!-- /ko -->
									
									<!-- ko if: (dimensionName ==='sku.itemDimensionMap.itemWeight') -->
										<!-- ko foreach: refinements -->
										<!-- ko ifnot: $data.dummyVal -->
											<div>
												<input type="checkbox" class="sub-category-filter-option-checkbox" /> 
												<label class="filter-lbl-container" data-bind="click: $root.refineMentOpntion">
													<span class="filter-lbl"></span><span class="filter-text" data-bind="text: label+'&nbsp;('+count+')'"></span>
												</label>
											</div>
									
										<!-- /ko -->
										<!-- /ko -->
									<!-- /ko -->
																			
			<!--Facet Review rating :: Start -->
												<!-- ko if:(dimensionName ==='sku.reviewRatDim') -->
												
										
												<!-- ko foreach: refinements -->
												<!-- ko ifnot: $data.dummyVal -->
												<div>
												<input type="checkbox" class="sub-category-filter-option-checkbox" /> 
												<label class="filter-lbl-container" data-bind="click: $root.refineMentOpntion">
												<span class="filter-lbl"></span>
												<div class="star-rating-block" aria-label="having 5 star rating" tabindex="0">
												<!-- ko ifnot:label -->
												<!-- ko foreach : ko.utils.range(1,5) -->
												<div class="star-rating-off"></div>
												<!--/ko-->
												<!--/ko-->
												<!-- ko if:label -->
												<!-- ko if:$root.calculateStars(label) -->
												<!-- ko foreach : ko.utils.range(1, parseInt(label)) -->
												<div class="star-rating-on"></div>
												<!--/ko-->
												<div class="star-rating-half"></div>
												<!-- ko foreach : ko.utils.range(1, 5-parseInt(label)-1) -->
												<div class="star-rating-off"></div>
												<!--/ko-->
												<!--/ko-->
												<!-- ko ifnot:$root.calculateStars(label) -->
												<!-- ko foreach : ko.utils.range(1, parseInt(label)) -->
												<div class="star-rating-on"></div>
												<!--/ko-->
												
												<!-- ko foreach : ko.utils.range(1, 5-parseInt(label)) -->
												<div class="star-rating-off"></div>
												<!--/ko-->
												<!--/ko-->
												<!--/ko-->
												&nbsp;(<span data-bind="text: count"></span>)
												</div>
												</label>
												</div>
												<!-- /ko -->
												<!--/ko-->
												<!--/ko-->
												<!--Facet Review rating :: End -->
													<!-- <a class="more-values" href="javascript:void(0)" data-bind="attr:{id:'ui-id-'+$index()}">see more</a> --> <!-- modified for B&S spark UI Changes -->
												</div>
											</div>
										</div>
									</div>
							<!-- /ko -->
						</div>
					</div>
				</div>
			</div>
		</div>
<!-- 	</div> -->
<div id="ui-id-4"></div>
	<c:if test="${empty Ntt}">
		<dsp:include page="/compare/productCompareResults.jsp"/>
	</c:if>
</dsp:page>
