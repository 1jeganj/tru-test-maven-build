<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations" />
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/>
	<dsp:getvalueof var="contextPath" value="${originatingRequest.contextPath}"/> 
<dsp:getvalueof var="searchPageUrl" bean="EndecaConfigurations.searchPageUrl"/>
<dsp:getvalueof var="totalNumberOfResults" param="totalNumRecs"/>
<dsp:getvalueof var="noSearchPage" param="noSearchPage"/>
<c:forEach var="element" items="${content.originalTerms}">
<dsp:getvalueof var="originalTerm" value="${element}" />	
<c:set var="searchKeyWord" scope="request" value="${originalTerm}"/>	
</c:forEach>
<c:if test="${noSearchPage ne 'noSearchPage'}">
	<c:if test="${not empty originalTerm}">
	<div class="search-breadcrumb-block default-margin">
        <ul class="category-breadcrumb">
			<li>
				<a href="${contextPath}">
					home
				</a>
			</li>
			<li>
				<span>search <fmt:message key="tru.search.result"/> "${originalTerm}"</span>
			</li>
		</ul>
    </div>
	</c:if>
	</c:if>
   <c:if test="${empty totalNumberOfResults or totalNumberOfResults eq 0}">
		<h2 class="search-breadcrumb-block no-results default-margin"><fmt:message key="tru.nosearch.resultFound"/> "${originalTerm}"
		</h2>
		<div class="did-you-mean">
			<c:if test="${not empty originalTerm and not empty content.suggestedSearches[originalTerm]}">
			
		<span class="search-breadcrumb-block "><fmt:message key="searchPage.didyou.mean"/>&nbsp;</span>
			<c:forEach var="suggestion"	items="${content.suggestedSearches[originalTerm]}" varStatus="status">
					<a class="blue-link"  href="${siteURL}${searchPageUrl}${suggestion.navigationState}">${fn:toUpperCase(fn:substring(suggestion.label, 0, 1))}${fn:toLowerCase(fn:substring(suggestion.label, 1,fn:length(suggestion.label)))}</a><span class="question-mark"><c:if test="${!status.last}">, </c:if></c:forEach>?</span>			
		</c:if>
		</div>
	<%-- 	<hr> --%>
</c:if>
<c:if test="${totalNumberOfResults gt 0}">
	<div class="suggested-search-block">
        <div class="suggested-search">
            
			<c:if test="${not empty originalTerm and not empty content.adjustedSearches[originalTerm]}">
			
			<div class="inline suggested-search-text"><fmt:message key="tru.search.suggested"/></div>
            <ul class="search-suggestions">
			<c:forEach var="adjustedSearch"	items="${content.adjustedSearches[originalTerm]}" varStatus="status">
			
			<li>
					<a href="${siteURL}${searchPageUrl}?keyword=${adjustedSearch.adjustedTerms}" >${adjustedSearch.adjustedTerms}</a>
				
			</li>
			</c:forEach>
			</ul>
			</c:if>
        </div>
    </div>
	</c:if>
</dsp:page>