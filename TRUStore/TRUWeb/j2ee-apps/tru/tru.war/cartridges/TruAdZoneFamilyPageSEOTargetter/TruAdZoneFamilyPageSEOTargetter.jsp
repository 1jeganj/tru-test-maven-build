<dsp:page>
<dsp:importbean bean="/atg/targeting/TargetingFirst" />
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/>
<dsp:getvalueof var="componentPath"	value="${content.componentPath}" />
<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters${componentPath}"/>
<div class="default-margin">
<div class="row category-ad-row">
          <%-- 	Rendering Content From ATG Targeter --%>
			<c:if test="${not empty componentPath}">
				<dsp:droplet name="/atg/targeting/TargetingFirst">
					<dsp:param name="targeter" bean="${atgTargeterpath}"/>
					<dsp:oparam name="output">
					<dsp:valueof param="element.data" valueishtml="true"/>
				</dsp:oparam>
				</dsp:droplet>
			</c:if>            		
           
        </div>
        
		</div>
</dsp:page>