

<dsp:page> 

 <div class="row row-no-padding">
            <div class="col-md-12 col-no-padding">
                <h2 class="now-trending-header">recommended just for you</h2>
            </div>
        </div>
        <div class="row block-2-row">
            <div class="col-md-3 block-2-column">
                <div class="product-block product-block-unselected no-select">
                    <div class="product-flag"></div>
                    <div class="product-choose">
                        <div class="product-compare-checkbox-container">
                            <input class="product-compare-checkbox" type="checkbox">
                            <label class="product-compare-checkbox-label">compare</label>
                        </div>
                    </div>
                    <div class="product-selection">
                        <div class="selection-checkbox-off"></div>
                        <div class="selection-text-off">Select</div>
                    </div>
                    <div class="product-block-image-container">
                        <div class="product-block-image">
                            <div class="product-image">
                                <a class="bnsImgBlock" href="http://cloud.toysrus.resource.com/redesign/template-category-main.html#">
                                    <img class="product-block-img" src="images/product-block-image.jpg" alt="ToysRUs Block Image">
                                </a>
                                <div>
                                    <div class="quick-view-container">
                                        <div class="product-hover-view product-quick-view" data-toggle="modal" data-target="#quickviewModal">quick view</div>
                                        <div class="product-hover-view product-add-to-cart" data-toggle="modal" data-target="#shoppingCartModal">add to cart</div>
                                        <div class="add-to-product-container">
                                            <div class="inline add-to-product-hover">
                                                <img src="images/wish-list.png" alt="wish list" class="add-to-image">add to wish list
                                            </div>
                                            <div class="inline add-to-product-hover add-to-product-hover-right">
                                                <img src="images/baby-registry.png" alt="baby registry" class="add-to-image">add to registry
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="colors">
                        <div class="color-block red color-selected">
                            <div class="color-block-inner">

                            </div>
                        </div>
                        <div class="color-block yellow">
                            <div class="color-block-inner">

                            </div>
                        </div>
                        <div class="color-block orange">
                            <div class="color-block-inner">

                            </div>
                        </div>
                    </div>
                    <div class="wish-list-heart-block">
                        <div class="wish-list-heart-off"></div>
                    </div>
                    <div class="star-rating-block">
                        <div class="star-rating-on"></div>
                        <div class="star-rating-on"></div>
                        <div class="star-rating-on"></div>
                        <div class="star-rating-on"></div>
                        <div class="star-rating-off"></div>
                    </div>
                    <div class="product-block-information">
                        <div class="product-name">
                            <a href="http://cloud.toysrus.resource.com/redesign/template-category-main.html#">Disney Frozen Cuddle Pillow</a>
                        </div>
                        <div class="age-range">
                            Age: 3-7 years
                        </div>
                        <div class="product-block-price">
                            <span class="product-block-original-price">$9,999.99</span>
                            <span class="product-block-sale-price">$4,999.99</span>
                            <div class="product-block-amount-saved">
                                you save <span>$5000 (50%)</span>
                            </div>
                        </div>
                    </div>
                    <div class="product-block-incentive">
                        ships free with purchase of $49 or more!
                    </div>
                    <div class="promo-block-container">
                        <div class="promotion-block">
                            <div class="header">
                                <h2>20% off</h2>
                                <p>
                                    all Graco Stollers-online only
                                </p>
                            </div>
                            <br>
                            <img src="./Toolkit_files/subcat-promo-img.jpg" alt="cannot find image">
                            <button class="shop-now">shop now</button>
                        </div>
                    </div>
                    <div class="product-block-border"></div>
                </div>
            </div>
            <div class="col-md-3 block-2-column">
                <div class="product-block product-block-unselected no-select">
                    <div class="product-flag"></div>
                    <div class="product-choose">
                        <div class="product-compare-checkbox-container">
                            <input class="product-compare-checkbox" type="checkbox">
                            <label class="product-compare-checkbox-label">compare</label>
                        </div>
                    </div>
                    <div class="product-selection">
                        <div class="selection-checkbox-off"></div>
                        <div class="selection-text-off">Select</div>
                    </div>
                    <div class="product-block-image-container">
                        <div class="product-block-image">
                            <div class="product-image">
                                <a class="bnsImgBlock" href="http://cloud.toysrus.resource.com/redesign/template-category-main.html#">
                                    <img class="product-block-img" src="images/product-block-image.jpg" alt="ToysRUs Block Image">
                                </a>
                                <div>
                                    <div class="quick-view-container">
                                        <div class="product-hover-view product-quick-view" data-toggle="modal" data-target="#quickviewModal">quick view</div>
                                        <div class="product-hover-view product-add-to-cart" data-toggle="modal" data-target="#shoppingCartModal">add to cart</div>
                                        <div class="add-to-product-container">
                                            <div class="inline add-to-product-hover">
                                                <img src="images/wish-list.png" alt="wish list" class="add-to-image">add to wish list
                                            </div>
                                            <div class="inline add-to-product-hover add-to-product-hover-right">
                                                <img src="images/baby-registry.png" alt="baby registry" class="add-to-image">add to registry
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="colors">
                        <div class="color-block red color-selected">
                            <div class="color-block-inner">

                            </div>
                        </div>
                        <div class="color-block yellow">
                            <div class="color-block-inner">

                            </div>
                        </div>
                        <div class="color-block orange">
                            <div class="color-block-inner">

                            </div>
                        </div>
                    </div>
                    <div class="wish-list-heart-block">
                        <div class="wish-list-heart-off"></div>
                    </div>
                    <div class="star-rating-block">
                        <div class="star-rating-on"></div>
                        <div class="star-rating-on"></div>
                        <div class="star-rating-on"></div>
                        <div class="star-rating-on"></div>
                        <div class="star-rating-off"></div>
                    </div>
                    <div class="product-block-information">
                        <div class="product-name">
                            <a href="http://cloud.toysrus.resource.com/redesign/template-category-main.html#">Disney Frozen Cuddle Pillow</a>
                        </div>
                        <div class="age-range">
                            Age: 3-7 years
                        </div>
                        <div class="product-block-price">
                            <span class="product-block-original-price">$9,999.99</span>
                            <span class="product-block-sale-price">$4,999.99</span>
                            <div class="product-block-amount-saved">
                                you save <span>$5000 (50%)</span>
                            </div>
                        </div>
                    </div>
                    <div class="product-block-incentive">
                        ships free with purchase of $49 or more!
                    </div>
                    <div class="promo-block-container">
                        <div class="promotion-block">
                            <div class="header">
                                <h2>20% off</h2>
                                <p>
                                    all Graco Stollers-online only
                                </p>
                            </div>
                            <br>
                            <img src="./Toolkit_files/subcat-promo-img.jpg" alt="cannot find image">
                            <button class="shop-now">shop now</button>
                        </div>
                    </div>
                    <div class="product-block-border"></div>
                </div>
            </div>
            <div class="col-md-3 block-2-column">
                <div class="product-block product-block-unselected no-select">
                    <div class="product-flag"></div>
                    <div class="product-choose">
                        <div class="product-compare-checkbox-container">
                            <input class="product-compare-checkbox" type="checkbox">
                            <label class="product-compare-checkbox-label">compare</label>
                        </div>
                    </div>
                    <div class="product-selection">
                        <div class="selection-checkbox-off"></div>
                        <div class="selection-text-off">Select</div>
                    </div>
                    <div class="product-block-image-container">
                        <div class="product-block-image">
                            <div class="product-image">
                                <a class="bnsImgBlock" href="http://cloud.toysrus.resource.com/redesign/template-category-main.html#">
                                    <img class="product-block-img" src="images/product-block-image.jpg" alt="ToysRUs Block Image">
                                </a>
                                <div>
                                    <div class="quick-view-container">
                                        <div class="product-hover-view product-quick-view" data-toggle="modal" data-target="#quickviewModal">quick view</div>
                                        <div class="product-hover-view product-add-to-cart" data-toggle="modal" data-target="#shoppingCartModal">add to cart</div>
                                        <div class="add-to-product-container">
                                            <div class="inline add-to-product-hover">
                                                <img src="images/wish-list.png" alt="wish list" class="add-to-image">add to wish list
                                            </div>
                                            <div class="inline add-to-product-hover add-to-product-hover-right">
                                                <img src="images/baby-registry.png" alt="baby registry" class="add-to-image">add to registry
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="colors">
                        <div class="color-block red color-selected">
                            <div class="color-block-inner">

                            </div>
                        </div>
                        <div class="color-block yellow">
                            <div class="color-block-inner">

                            </div>
                        </div>
                        <div class="color-block orange">
                            <div class="color-block-inner">

                            </div>
                        </div>
                    </div>
                    <div class="wish-list-heart-block">
                        <div class="wish-list-heart-off"></div>
                    </div>
                    <div class="star-rating-block">
                        <div class="star-rating-on"></div>
                        <div class="star-rating-on"></div>
                        <div class="star-rating-on"></div>
                        <div class="star-rating-on"></div>
                        <div class="star-rating-off"></div>
                    </div>
                    <div class="product-block-information">
                        <div class="product-name">
                            <a href="http://cloud.toysrus.resource.com/redesign/template-category-main.html#">Disney Frozen Cuddle Pillow</a>
                        </div>
                        <div class="age-range">
                            Age: 3-7 years
                        </div>
                        <div class="product-block-price">
                            <span class="product-block-original-price">$9,999.99</span>
                            <span class="product-block-sale-price">$4,999.99</span>
                            <div class="product-block-amount-saved">
                                you save <span>$5000 (50%)</span>
                            </div>
                        </div>
                    </div>
                    <div class="product-block-incentive">
                        ships free with purchase of $49 or more!
                    </div>
                    <div class="promo-block-container">
                        <div class="promotion-block">
                            <div class="header">
                                <h2>
			20% off
		</h2>
                                <p>
                                    all Graco Stollers-online only
                                </p>
                            </div>
                            <br>
                            <img src="./Toolkit_files/subcat-promo-img.jpg" alt="cannot find image">
                            <button class="shop-now">shop now</button>
                        </div>
                    </div>
                    <div class="product-block-border"></div>
                </div>
            </div>
            <div class="col-md-3 block-2-column">
                <div class="product-block product-block-unselected no-select">
                    <div class="product-flag"></div>
                    <div class="product-choose">
                        <div class="product-compare-checkbox-container">
                            <input class="product-compare-checkbox" type="checkbox">
                            <label class="product-compare-checkbox-label">compare</label>
                        </div>
                    </div>
                    <div class="product-selection">
                        <div class="selection-checkbox-off"></div>
                        <div class="selection-text-off">Select</div>
                    </div>
                    <div class="product-block-image-container">
                        <div class="product-block-image">
                            <div class="product-image">
                                <a class="bnsImgBlock" href="http://cloud.toysrus.resource.com/redesign/template-category-main.html#">
                                    <img class="product-block-img" src="images/product-block-image.jpg" alt="ToysRUs Block Image">
                                </a>
                                <div>
                                    <div class="quick-view-container">
                                        <div class="product-hover-view product-quick-view" data-toggle="modal" data-target="#quickviewModal">quick view</div>
                                        <div class="product-hover-view product-add-to-cart" data-toggle="modal" data-target="#shoppingCartModal">add to cart</div>
                                        <div class="add-to-product-container">
                                            <div class="inline add-to-product-hover">
                                                <img src="images/wish-list.png" alt="wish list" class="add-to-image">add to wish list
                                            </div>
                                            <div class="inline add-to-product-hover add-to-product-hover-right">
                                                <img src="images/baby-registry.png" alt="baby registry" class="add-to-image">add to registry
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="colors">
                        <div class="color-block red color-selected">
                            <div class="color-block-inner">

                            </div>
                        </div>
                        <div class="color-block yellow">
                            <div class="color-block-inner">

                            </div>
                        </div>
                        <div class="color-block orange">
                            <div class="color-block-inner">

                            </div>
                        </div>
                    </div>
                    <div class="wish-list-heart-block">
                        <div class="wish-list-heart-off"></div>
                    </div>
                    <div class="star-rating-block">
                        <div class="star-rating-on"></div>
                        <div class="star-rating-on"></div>
                        <div class="star-rating-on"></div>
                        <div class="star-rating-on"></div>
                        <div class="star-rating-off"></div>
                    </div>
                    <div class="product-block-information">
                        <div class="product-name">
                            <a href="http://cloud.toysrus.resource.com/redesign/template-category-main.html#">Disney Frozen Cuddle Pillow</a>
                        </div>
                        <div class="age-range">
                            Age: 3-7 years
                        </div>
                        <div class="product-block-price">
                            <span class="product-block-original-price">$9,999.99</span>
                            <span class="product-block-sale-price">$4,999.99</span>
                            <div class="product-block-amount-saved">
                                you save <span>$5000 (50%)</span>
                            </div>
                        </div>
                    </div>
                    <div class="product-block-incentive">
                        ships free with purchase of $49 or more!
                    </div>
                    <div class="promo-block-container">
                        <div class="promotion-block">
                            <div class="header">
                                <h2>20% off</h2>
                                <p>
                                    all Graco Stollers-online only
                                </p>
                            </div>
                            <br>
                            <img src="./Toolkit_files/subcat-promo-img.jpg" alt="cannot find image">
                            <button class="shop-now">shop now</button>
                        </div>
                    </div>
                    <div class="product-block-border"></div>
                </div>
            </div>
        </div>
</dsp:page>