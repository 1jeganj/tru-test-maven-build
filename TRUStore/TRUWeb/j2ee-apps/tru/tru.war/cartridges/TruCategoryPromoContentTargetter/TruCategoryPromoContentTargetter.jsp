<dsp:page>
	<dsp:importbean bean="/atg/targeting/TargetingForEach" />  
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
    <dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/> 
  	<dsp:getvalueof var="componentPath"	value="${content.componentPath}" />
  	<dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:getvalueof var="customerId" bean="Profile.id"/>
  	<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
  	<%-- Certona parameter Start --%>
  	<dsp:getvalueof var="siteCode" value="TRU"/>
	<dsp:getvalueof var="category" value=""/>
	<dsp:getvalueof var="agefilter" value=""/>
	<dsp:getvalueof var="gender" value=""/>
	<dsp:getvalueof var="brand" value=""/>
	<dsp:getvalueof var="charactertheme" value=""/>
	<dsp:getvalueof var="currentprice" value=""/>
	<%-- Certona parameter End --%>
	<%--<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters${componentPath}"/>
	<c:if test="${not empty componentPath}">
		<dsp:droplet name="TargetingForEach">
			<dsp:param bean="${atgTargeterpath}" name="targeter"/>
			<dsp:param name="howMany" value="1"/>
			<dsp:oparam name="output">
			
				<dsp:valueof param="element.contentData" valueishtml="true"/>
				
			</dsp:oparam>
		</dsp:droplet>
    </c:if>--%>
    
   	<%-- Certona related Divs Start --%>
   		<c:choose>
			<c:when test="${site eq 'ToysRUs'}">
				<div class="full_width_gray">
					<div id="tcategory_rr" style="visibility: visible;">  
						<!-- Toysrus category page recommendations appear here -->
					</div>
				</div>
			</c:when>
			<c:otherwise>
				<div class="full_width_gray">
					<div id="bcategory_rr" style="visibility: visible;"> 
					 	<!-- Babiesrus category page recommendations appear here -->
					 </div>
				</div>
				<dsp:getvalueof var="siteCode" value="BRU" />			
			</c:otherwise>
		</c:choose>
	<%-- Certona related Divs End --%>
	  <div id="rdata" style="visibility:hidden;">
		   <div id="site">${siteCode}</div>  
		   <div id="category">${category}</div>  
		   <div id="agefilter">${agefilter}</div> 
		   <div id="gender">${gender}</div>  
		   <div id="brand">${brand}</div>  
		   <div id="charactertheme">${charactertheme}</div>  
		   <div id="currentprice">${currentprice}</div>
		   <div id="customerid">${customerId}</div>
	    </div>
	 <%-- Certona related Divs End --%>

</dsp:page>