<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
	<dsp:getvalueof var="previewEnabled" bean="/atg/endeca/assembler/cartridge/manager/AssemblerSettings.previewEnabled"/>
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="siteCode" value="TRU" />
	<dsp:getvalueof var="baseBreadcrumb" value="tru" />
	<%-- <dsp:getvalueof var="loginStatus" bean="Profile.transient" /> --%>
	<%-- <dsp:getvalueof var="customerEmail" bean="Profile.login"/> --%>
	<%-- <dsp:getvalueof var="customerID" bean="Profile.id"/> --%>
	<dsp:getvalueof var="customerDOB" bean="Profile.dateOfBirth"/>
	<dsp:getvalueof param="searchKeyWord" var="tSearchKeyWord" />
	<dsp:getvalueof param="pageName" var="tPageName" />
	<dsp:getvalueof param="pageType" var="tPageType" />
	<dsp:getvalueof param="page" var="page" />
	<dsp:getvalueof var="tealiumProductImpressionId" param="tealiumProductImpressionId" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchPageName" var="searchPageName" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchPageType" var="searchPageType" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchOASSizes" var="tOasSizes" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchOASBreadcrumb" var="tOasBreadcrumb" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchOASTaxonamy" var="tOasTaxonomy" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.browserId" var="tBrowserID" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.deviceType" var="tDeviceType" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchOrsoCode" var="searchOrsoCode" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchIspuSource" var="searchIspuSource" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchRefinementType" var="searchRefinementType" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchRecent" var="searchRecent" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchSuccess" var="searchSuccess" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchSynonym" var="searchSynonym" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchTest" var="searchTest" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchRefinementValue" var="searchRefinementValue" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchIntCampaignPage" var="searchIntCampaignPage" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchFinderSortGroup" var="finderSortGroup" />
	<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
	<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>
	<dsp:getvalueof var="customerFirstName" bean="Profile.firstName"/>
	<dsp:getvalueof var="customerLastName" bean="Profile.lastName"/>

	<c:set var="space" value=" "/>

	<c:choose>
		<%-- <c:when test="${empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value='${fn:substring(customerEmail, 0, fn:indexOf(customerEmail, "@"))}'/>
		</c:when> --%>
		<c:when test="${not empty customerFirstName && empty customerLastName}">
				<c:set var="customerName" value="${customerFirstName}"/>
		</c:when>
		<c:when test="${empty customerFirstName && not empty customerLastName}">
			   <c:set var="customerName" value="${customerLastName}"/>
		</c:when>
		<c:otherwise>
				<c:set var="customerName" value="${customerFirstName}${space}${customerLastName}"/>
		</c:otherwise>
 	</c:choose>

	<%-- <c:choose>
		<c:when test="${loginStatus eq 'true'}">
			<c:set var="customerStatus" value="Guest"/>
		</c:when>
		<c:otherwise>
			<c:set var="customerStatus" value="Registered"/>
			<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" bean="Profile.shippingAddress.id" />
				<dsp:oparam name="false">
					<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="Profile.secondaryAddresses"/>
						<dsp:param name="elementName" value="address"/>
						<dsp:oparam name="output">
							<dsp:getvalueof param="address.id" var="addressId" />
							<c:if test="${addressId eq shippingAddressId}">
						 		<dsp:getvalueof param="address.city" var="customerCity"/>
						 		<dsp:getvalueof param="address.state" var="customerState"/>
						 		<dsp:getvalueof param="address.postalCode" var="customerZip"/>
						 		<dsp:getvalueof param="address.country" var="customerCountry"/>
				 			</c:if>
						</dsp:oparam>
				   	</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</c:otherwise>
	</c:choose> --%>

	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
 	<c:if test="${site eq babySiteCode}">
		<dsp:getvalueof var="siteCode" value="BRU" />
		<dsp:getvalueof var="baseBreadcrumb" value="bru" />
		<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
 	</c:if>

 	<dsp:droplet name="/com/tru/common/droplet/TRUTealiumCategoryModifierDroplet">
		<dsp:param name="categoryName" value="${tSearchKeyWord}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="modifiedKeyWord" param="modifiedCategory"/>
		</dsp:oparam>
	</dsp:droplet>

	<tru:pageContainer>
		<jsp:attribute name="fromsearch">Search</jsp:attribute>
		<jsp:body>
		<c:choose>
		<c:when test="${previewEnabled}">
		 <endeca:pageBody rootContentItem="${rootContentItem}" contentItem="${content}">

			<input type="hidden" value="Successful-Search-Results" name="currentpage" id="currentpage"/>
		 	<c:set var="search" scope="request" value="search"/>
				
			<c:forEach var="element" items="${content.MainProduct}">
				<c:forEach var="element1" items="${element.contents}">
					<c:forEach var="element2" items="${element1.MainProduct}">
						<c:if test="${element2['@type'] eq 'TRUResultsList'}">
							<dsp:getvalueof var="resultsListItem" value="${element2}" />
							<dsp:getvalueof var="totalNumRecs" value="${element2.totalNumRecs}" />
							<c:set var="tSearchResults" value="${totalNumRecs}" />
        					<c:set var="tSearchResultItem" value="${resultsListItem}" />
						</c:if>
					</c:forEach>
				</c:forEach>
			</c:forEach>

       		<c:forEach var="element" items="${content.MainHeader}">
		 		<dsp:renderContentItem contentItem="${element}">
		 			<dsp:param name="totalNumRecs" value="${totalNumRecs}" />
		 		</dsp:renderContentItem>
			</c:forEach>

		 	<div class="row search-main-search-block"></div>
			<div class="row default-margin">
		    	<div class="col-mid-12 col-no-padding category-name search-results-title">
		    		<h2>search results</h2>
			 	</div>
		  	</div>
		 	<div id="narrow-by-scroll" class="product-content">
				<c:forEach var="element" items="${content.MainProduct}">
		 			<dsp:renderContentItem contentItem="${element}">
		  				<dsp:param name="resultsListItem" value="${resultsListItem}" />
		  				<dsp:param name="totalNumRecs" value="${totalNumRecs}" />
		 			</dsp:renderContentItem>
				</c:forEach>
				<input type="hidden" class="pageName" value="search" />
			</div>
			<div class="spacer"></div>

       		<c:forEach var="element" items="${content.MainFooter}">
		 		<dsp:renderContentItem contentItem="${element}" />
			</c:forEach>

			<c:set var="tealiumSearchIndex" value="0"/>
			<c:set var="searchRecords" value="${contentItemForTealium.records}"/>
			<c:forEach var="record" items="${searchRecords}">
				<dsp:getvalueof id="prodId" value="${record.attributes['product.repositoryId']}" />
				<c:choose>
	 				<c:when test="${searchRecords.size() gt '1' && tealiumSearchIndex lt (searchRecords.size()-1)}">
						<c:set var="searchResultItem" value="${searchResultItem}'${prodId}',"/>
					</c:when>
					<c:otherwise>
						<c:set var="searchResultItem" value="${searchResultItem}'${prodId}'"/>
		            </c:otherwise>
 				</c:choose>
				<c:set var="tealiumSearchIndex" value="${tealiumSearchIndex + 1}"/>
			</c:forEach>
			
			<dsp:droplet name="/com/tru/commerce/droplet/TRUProductInfoLookupDroplet">
				<dsp:param name="productId" value="${element.productId}"/>
				<dsp:param name="siteId" bean="/atg/multisite/Site.id"/>
					<dsp:oparam name="output">
				        <dsp:getvalueof param="productInfo.defaultSKU.relatedProducts" var="relatedProducts"/>
				        <c:set var="orderXsellProduct" value="${orderXsellProduct}'${relatedProducts}'${separator}"/>
			       </dsp:oparam>
			</dsp:droplet>
		
			<dsp:droplet name="/com/tru/commerce/locations/TRUGetCookieDroplet">
					<dsp:param name="cookieName" value="favStore" />
					<dsp:oparam name="output">
						<dsp:getvalueof var="cookieValue" param="cookieValue" />
						<dsp:getvalueof var="locationIdFromCookie" param="locationId" />
					</dsp:oparam>
					<dsp:oparam name="empty">
					</dsp:oparam>
				</dsp:droplet>

		<c:set var="session_id" value="${cookie.sessionID.value}"/>
		<c:set var="visitor_id" value="${cookie.visitorID.value}"/>
		<c:set var="customerEmail" value="${cookie.VisitorUsaFullName.value}"/>
		<c:set var="customerID" value="${cookie.customerID.value}"/>
		<c:set var="loginStatus" value="${cookie.custStatusCookie.value}"/>
		
		<c:choose>
		<c:when test="${loginStatus eq 'true'}">
					<c:set var="customerStatus" value="Guest" />
				</c:when>
				<c:otherwise>
					<c:set var="customerStatus" value="Registered" />
				</c:otherwise>
			</c:choose>
			<!-- Script added for Tealium integration -->

			<script type='text/javascript'>
				var isGridwallOnLoad = true; /* to avoid on page load, gridwall_impression click even trigger*/
				var utag_data = {
					customer_status : "${customerStatus}",
				    customer_city : "",
				    customer_country : "",
				    customer_email : "${customerEmail}",
				    customer_id : "${customerID}",
				    customer_type : "${customerStatus}",
				    customer_state : "",
				    customer_zip : "",
				    event_type:["search_performed", "gridwall_impression"],
				    customer_dob : "${customerDOB}",
					page_name : "${siteCode}: ${searchPageName}",
					page_type : "${siteCode}: ${searchPageType}",
					browser_id : "${pageContext.session.id}",
					search_keyword : "${tSearchKeyWord}",
					search_results : "${tSearchResults}",
					search_result_item : [${searchResultItem}],
					oas_breadcrumb : "${baseBreadcrumb}/searchresults/${modifiedKeyWord}",
					oas_taxonomy : "level1=searchresults&level2=${modifiedKeyWord}&level3=null&level4=null&level5=null&level6=null",
					oas_sizes :${tOasSizes},
					device_type : "${tDeviceType}",
					orso_code : "${searchOrsoCode}",
					ispu_source : "${searchIspuSource}",
					search_refinement_type:"${searchRefinementType}",
					search_recent:"${searchRecent}",
					search_success :"${searchSuccess}",
					search_synonym:"${tSearchSynonym}",
					search_test:"${tSearchKeyWord}",
					search_refinement_value:"${searchRefinementValue}",
					internal_campaign_page:"${searchIntCampaignPage}",
					finder_sort_group :"best match",
					partner_name:"${partnerName}",
					product_impression_id:[${tealiumProductImpressionId}],
					product_gridwall_location:"",
					customer_name:"${customerName}",
					xsell_product:[${orderXsellProduct}],
					store_locator:"${locationIdFromCookie}",
					session_id : "${session_id}",
	   			    visitor_id : "${visitor_id}",
	   			 	tru_or_bru : "${siteCode}"
				};
			</script>

			<!-- Start: Script for Tealium Integration -->
				<script type="text/javascript">

						(function(a,b,c,d){
						a='${tealiumURL}';
						b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
						a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
						})();
				</script>
			    <!-- End: Script for Tealium Integration -->

	       </endeca:pageBody>
		
		</c:when>
		<c:otherwise>
			<input type="hidden" value="Successful-Search-Results" name="currentpage" id="currentpage"/>
		 	<c:set var="search" scope="request" value="search"/>
				
			<c:forEach var="element" items="${content.MainProduct}">
				<c:forEach var="element1" items="${element.contents}">
					<c:forEach var="element2" items="${element1.MainProduct}">
						<c:if test="${element2['@type'] eq 'TRUResultsList'}">
							<dsp:getvalueof var="resultsListItem" value="${element2}" />
							<dsp:getvalueof var="totalNumRecs" value="${element2.totalNumRecs}" />
							<c:set var="tSearchResults" value="${totalNumRecs}" />
        					<c:set var="tSearchResultItem" value="${resultsListItem}" />
						</c:if>
					</c:forEach>
				</c:forEach>
			</c:forEach>

       		<c:forEach var="element" items="${content.MainHeader}">
		 		<dsp:renderContentItem contentItem="${element}">
		 			<dsp:param name="totalNumRecs" value="${totalNumRecs}" />
		 		</dsp:renderContentItem>
			</c:forEach>

		 	<div class="row search-main-search-block"></div>
			<div class="row default-margin">
		    	<div class="col-mid-12 col-no-padding category-name search-results-title">
		    		<h2>search results</h2>
			 	</div>
		  	</div>
		 	<div id="narrow-by-scroll" class="product-content">
				<c:forEach var="element" items="${content.MainProduct}">
		 			<dsp:renderContentItem contentItem="${element}">
		  				<dsp:param name="resultsListItem" value="${resultsListItem}" />
		  				<dsp:param name="totalNumRecs" value="${totalNumRecs}" />
		 			</dsp:renderContentItem>
				</c:forEach>
				<input type="hidden" class="pageName" value="search" />
			</div>
			<div class="spacer"></div>

       		<c:forEach var="element" items="${content.MainFooter}">
		 		<dsp:renderContentItem contentItem="${element}" />
			</c:forEach>

			<c:set var="tealiumSearchIndex" value="0"/>
			<c:set var="searchRecords" value="${contentItemForTealium.records}"/>
			<c:forEach var="record" items="${searchRecords}">
				<dsp:getvalueof id="prodId" value="${record.attributes['product.repositoryId']}" />
				<c:choose>
	 				<c:when test="${searchRecords.size() gt '1' && tealiumSearchIndex lt (searchRecords.size()-1)}">
						<c:set var="searchResultItem" value="${searchResultItem}'${prodId}',"/>
					</c:when>
					<c:otherwise>
						<c:set var="searchResultItem" value="${searchResultItem}'${prodId}'"/>
		            </c:otherwise>
 				</c:choose>
				<c:set var="tealiumSearchIndex" value="${tealiumSearchIndex + 1}"/>
			</c:forEach>
			
			<dsp:droplet name="/com/tru/commerce/droplet/TRUProductInfoLookupDroplet">
				<dsp:param name="productId" value="${element.productId}"/>
				<dsp:param name="siteId" bean="/atg/multisite/Site.id"/>
					<dsp:oparam name="output">
				        <dsp:getvalueof param="productInfo.defaultSKU.relatedProducts" var="relatedProducts"/>
				        <c:set var="orderXsellProduct" value="${orderXsellProduct}'${relatedProducts}'${separator}"/>
			       </dsp:oparam>
			</dsp:droplet>
		
			<dsp:droplet name="/com/tru/commerce/locations/TRUGetCookieDroplet">
					<dsp:param name="cookieName" value="favStore" />
					<dsp:oparam name="output">
						<dsp:getvalueof var="cookieValue" param="cookieValue" />
						<dsp:getvalueof var="locationIdFromCookie" param="locationId" />
					</dsp:oparam>
					<dsp:oparam name="empty">
					</dsp:oparam>
				</dsp:droplet>

		<c:set var="session_id" value="${cookie.sessionID.value}"/>
		<c:set var="visitor_id" value="${cookie.visitorID.value}"/>
		<c:set var="customerEmail" value="${cookie.VisitorUsaFullName.value}"/>
		<c:set var="customerID" value="${cookie.customerID.value}"/>
		<c:set var="loginStatus" value="${cookie.custStatusCookie.value}"/>
		
		<c:choose>
		<c:when test="${loginStatus eq 'true'}">
					<c:set var="customerStatus" value="Guest" />
				</c:when>
				<c:otherwise>
					<c:set var="customerStatus" value="Registered" />
				</c:otherwise>
			</c:choose>
			<!-- Script added for Tealium integration -->

			<script type='text/javascript'>
				var isGridwallOnLoad = true; /* to avoid on page load, gridwall_impression click even trigger*/
				var prodID=[];
		  		if( typeof familyProdID != 'undefined' ){
		  			prodID=familyProdID;
		  		}
				var utag_data = {
					customer_status : "${customerStatus}",
				    customer_city : "",
				    customer_country : "",
				    customer_email : "${customerEmail}",
				    customer_id : "${customerID}",
				    customer_type : "${customerStatus}",
				    customer_state : "",
				    customer_zip : "",
				    event_type:["search_performed", "gridwall_impression"],
				    customer_dob : "${customerDOB}",
					page_name : "${siteCode}: ${searchPageName}",
					page_type : "${siteCode}: ${searchPageType}",
					browser_id : "${pageContext.session.id}",
					search_keyword : "${tSearchKeyWord}",
					search_results : "${tSearchResults}",
					search_result_item : [${searchResultItem}],
					oas_breadcrumb : "${baseBreadcrumb}/searchresults/${modifiedKeyWord}",
					oas_taxonomy : "level1=searchresults&level2=${modifiedKeyWord}&level3=null&level4=null&level5=null&level6=null",
					oas_sizes :${tOasSizes},
					product_id:prodID,
					device_type : "${tDeviceType}",
					orso_code : "${searchOrsoCode}",
					ispu_source : "${searchIspuSource}",
					search_refinement_type:"${searchRefinementType}",
					search_recent:"${searchRecent}",
					search_success :"${searchSuccess}",
					search_synonym:"${tSearchSynonym}",
					search_test:"${tSearchKeyWord}",
					search_refinement_value:"${searchRefinementValue}",
					internal_campaign_page:"${searchIntCampaignPage}",
					finder_sort_group :"best match",
					partner_name:"${partnerName}",
					product_impression_id:[${tealiumProductImpressionId}],
					product_gridwall_location:"",
					customer_name:"${customerName}",
					xsell_product:[${orderXsellProduct}],
					store_locator:"${locationIdFromCookie}",
					session_id : "${session_id}",
	   			    visitor_id : "${visitor_id}",
	   			 	tru_or_bru : "${siteCode}"
				};
			</script>

			<!-- Start: Script for Tealium Integration -->
				<script type="text/javascript">

						(function(a,b,c,d){
						a='${tealiumURL}';
						b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
						a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
						})();
				</script>
			    <!-- End: Script for Tealium Integration -->

	       
		</c:otherwise>
		</c:choose>

		</jsp:body>
	</tru:pageContainer>

</dsp:page>