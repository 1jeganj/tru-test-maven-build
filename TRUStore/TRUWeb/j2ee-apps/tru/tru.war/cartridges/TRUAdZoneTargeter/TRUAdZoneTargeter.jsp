<dsp:page>
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
    <dsp:importbean bean="/com/tru/common/TRUComponentExists" />
	<dsp:getvalueof var="atgTargeterpathCheck" bean="TRUStoreConfiguration.targeterpathCheck" />
	<dsp:importbean bean="/atg/multisite/Site"/>
	<dsp:getvalueof var="enableTriad" bean="Site.enableTriad"/>
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:getvalueof var="componentPath"	value="${contentItem.componentPath}" />
	<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale" />
	<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters${componentPath}" />
	
	<dsp:droplet name="TRUComponentExists">
	<dsp:param name="path" value="${atgTargeterpath}"/><dsp:oparam name="true"><dsp:getvalueof var="atgTargeterpathCheck" value="true"/></dsp:oparam>
    <dsp:oparam name="false"></dsp:oparam>
	</dsp:droplet>
   
    <div class="sosAddZone">
	    <c:if test="${enableTriad eq 'true'}">
			<c:if test="${not empty componentPath && atgTargeterpathCheck eq 'true'}">
				
				<dsp:getvalueof var="cacheKey" value="${atgTargeterpath}${siteId}${locale}" />
			
			<dsp:droplet name="/com/tru/cache/TRUAdZoneTargeterCache">
				<dsp:param name="key" value="${cacheKey}" />
				<dsp:oparam name="output">
				
					<dsp:droplet name="/atg/targeting/TargetingForEach">
						<dsp:param name="targeter" bean="${atgTargeterpath}"/>
						<dsp:oparam name="output">
							<dsp:valueof param="element.data" valueishtml="true"/>
						</dsp:oparam>
					</dsp:droplet>
				
				</dsp:oparam>
			</dsp:droplet>
			</c:if>	
	    </c:if>	
    </div>
    
    <%-- <script>
		$(document).ready(function(){
			var isSOS = $.cookie("isSOS");
			var sites = '<c:out value="${site}"/>';
			if(typeof isSOS != 'undefined' && isSOS != null && isSOS == 'true'){
				$(".sosAddZone").hide();
			}else{
				$(".sosAddZone").show();
			}
			
			/* var pathname = window.location.pathname;
			if (pathname == '/home'){
				$('#OAS_Frame2').parent().after('<hr>')
			} */
		}); 
	</script>	 --%>
</dsp:page>
