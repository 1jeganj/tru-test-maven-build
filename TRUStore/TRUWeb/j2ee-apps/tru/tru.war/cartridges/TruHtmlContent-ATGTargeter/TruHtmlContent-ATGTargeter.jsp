<dsp:page>
     <dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
     <dsp:importbean bean="/com/tru/common/TRUComponentExists" />
	 <dsp:getvalueof var="atgTargeterpathCheck" bean="TRUStoreConfiguration.targeterpathCheck" />
	 <dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	
	<dsp:getvalueof var="componentPath"	value="${contentItem.componentPath}" />
	<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale" />
	<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters${componentPath}" />
	
	<dsp:droplet name="TRUComponentExists">
	<dsp:param name="path" value="${atgTargeterpath}"/><dsp:oparam name="true"><dsp:getvalueof var="atgTargeterpathCheck" value="true"/></dsp:oparam>
    <dsp:oparam name="false"></dsp:oparam>
	</dsp:droplet>

	<c:if test="${not empty componentPath  and atgTargeterpathCheck eq 'true'}">
		
		<dsp:getvalueof var="cacheKey" value="${atgTargeterpath}${siteId}${locale}" />
		<%-- <dsp:getvalueof var="categId" value="${content.CurrentCategoryId}" /> --%>
		<%-- <c:if test="${not empty categId}">
			<dsp:getvalueof var="cacheKey" value="${cacheKey}${categId}" />
		</c:if> --%>
		<dsp:droplet name="/com/tru/cache/TRUHtmlContentCacheDroplet">
			<dsp:param name="key" value="${cacheKey}" />
			<dsp:oparam name="output">
				<%-- 	Start TargetingFirst  for rendering the targeter --%>
				<dsp:droplet name="/atg/targeting/TargetingFirst">
					<dsp:param name="targeter" bean="${atgTargeterpath}" />
					<dsp:oparam name="output">
						<dsp:valueof param="element.data" valueishtml="true" />
					</dsp:oparam>
				</dsp:droplet>
				<%-- End TargetingFirst  for rendering the targeter --%>
			</dsp:oparam>
		</dsp:droplet>
	</c:if>
</dsp:page>