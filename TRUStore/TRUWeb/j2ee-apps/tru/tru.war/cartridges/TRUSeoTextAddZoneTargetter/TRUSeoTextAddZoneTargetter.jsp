<dsp:page>
    <dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
    <dsp:importbean bean="/com/tru/common/TRUComponentExists" />
	<dsp:getvalueof var="atgTargeterpathCheck" bean="TRUStoreConfiguration.targeterpathCheck" />
	<dsp:importbean bean="/atg/targeting/TargetingFirst" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
	<dsp:importbean bean="/atg/multisite/Site" />
	<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale" />
	<dsp:getvalueof var="enableTriad" bean="Site.enableTriad" />
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/>
	<dsp:getvalueof var="componentPath"	value="${content.componentPath}" />
	<dsp:getvalueof var="advertisementComponentPath"	value="${content.advertisementComponentPath}" />
	<dsp:getvalueof var="displayTargeter"	value="${content.displayTargeter}" />
	<dsp:getvalueof var="classificationSeoText"	value="${content.ClassificationSeoText}" />	
	<dsp:getvalueof var="cat" param="cat"/>
	<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters${componentPath}" />
	
	<dsp:droplet name="TRUComponentExists">
	<dsp:param name="path" value="${atgTargeterpath}"/><dsp:oparam name="true"><dsp:getvalueof var="atgTargeterpathCheck" value="true"/></dsp:oparam>
    <dsp:oparam name="false"></dsp:oparam>
	</dsp:droplet>
	
<div class="default-margin category-adzone-container">
<hr class="horizontal-divider">
<div class="row category-ad-row">
          <%-- 	Rendering Content From ATG Targeter --%>
        <c:if test="${enableTriad eq 'true' }">
				<c:choose>
					<c:when test="${not empty classificationSeoText}">
						<dsp:valueof value="${classificationSeoText}" valueishtml="true" />
					</c:when>
					<c:when test="${displayTargeter and not empty componentPath && atgTargeterpathCheck eq 'true' }">
						
						<%-- <dsp:getvalueof var="categId" value="${content.CurrentCategoryId}" /> --%>
						<dsp:getvalueof var="cacheKey" value="${atgTargeterpath}${siteId}${locale}" />
						<%-- <c:if test="${not empty categId}">
							<dsp:getvalueof var="cacheKey" value="${cacheKey}${categId}" />
						</c:if> --%>
						<dsp:droplet name="/com/tru/cache/TRUSeoTextCacheDroplet">
							<dsp:param name="key" value="${cacheKey}" />
							<dsp:oparam name="output">
								<dsp:droplet name="/atg/targeting/TargetingFirst">
									<dsp:param name="targeter" bean="${atgTargeterpath}" />
									<dsp:oparam name="output">
										<dsp:valueof param="element.data" valueishtml="true" />
									</dsp:oparam>
								</dsp:droplet>
							</dsp:oparam>
						</dsp:droplet>
					</c:when>
				</c:choose>
				
            <div class="col-md-9">
				<dsp:getvalueof var="atgTargeterpath"
							value="/atg/registry/RepositoryTargeters${advertisementComponentPath}" />
						<div class="SosSeoAddZone">
						<c:if test="${not empty advertisementComponentPath && atgTargeterpathCheck eq 'true'}">
							<dsp:getvalueof var="cacheKey" value="${atgTargeterpath}${siteId}${locale}" />
							<dsp:droplet name="/com/tru/cache/TRUSeoAddZoneCache">
							<dsp:param name="key" value="${cacheKey}" />
								<dsp:oparam name="output">
								<dsp:droplet name="/atg/targeting/TargetingForEach">
									<dsp:param name="targeter" bean="${atgTargeterpath}" />
									<dsp:oparam name="output">
										<dsp:valueof param="element.data" valueishtml="true" />
									</dsp:oparam>
								</dsp:droplet>
							
							</dsp:oparam>
						</dsp:droplet>
						</c:if>
						</div>
					</div>
					</c:if>	
		</div>
	
		<hr class="horizontal-divider">
</div>	
</dsp:page>