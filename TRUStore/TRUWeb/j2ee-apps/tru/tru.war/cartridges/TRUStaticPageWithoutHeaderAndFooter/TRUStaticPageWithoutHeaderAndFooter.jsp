<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
	<dsp:importbean bean="com/tru/common/TRUStoreConfiguration" />
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
	<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>
	<dsp:getvalueof var="oasSizes" bean="TRUTealiumConfiguration.homeOASSizes"/>
	<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
	<dsp:getvalueof var="baseBreadcrumb" value="tru" />

	<dsp:getvalueof var="siteCode" value="TRU" />
	<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
	<c:if test="${site eq babySiteCode}">
	<dsp:getvalueof var="siteCode" value="BRU" />
	<dsp:getvalueof var="baseBreadcrumb" value="bru" />
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
	</c:if>
	<dsp:getvalueof var="pageName" param="pageName" />
	<dsp:getvalueof var="pageType" param="pageType"/>
	<dsp:getvalueof bean="TRUTealiumConfiguration.deviceType" var="deviceType"/>
	<c:set var="session_id" value="${cookie.sessionID.value}"/>
	<c:set var="visitor_id" value="${cookie.visitorID.value}"/>

	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:getvalueof var="loginStatus" bean="Profile.transient" />
	<c:choose>
		<c:when test="${loginStatus eq 'true'}">
			<c:set var="customerStatus" value="Guest"/>
		</c:when>
		<c:otherwise>
			<c:set var="customerStatus" value="Registered"/>
		</c:otherwise>
	</c:choose>
	
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
		<dsp:getvalueof var="staticAssetsVersionFormat"	bean="TRUStoreConfiguration.staticAssetsVersion" />
			<dsp:getvalueof var="versionFlag" bean="TRUStoreConfiguration.versioningFlag" />
				<dsp:getvalueof value='<%=atg.nucleus.DynamoEnv.getProperty("staticAssetVersion")%>' var="versionNumbers" />
					<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
					<dsp:getvalueof var="domainPath" bean="TRUConfiguration.domainUrl" />
    <!DOCTYPE html> 
	<html lang="en">
	<head>
           <c:choose>
	           <c:when test="${versionFlag}">
						<link rel="stylesheet"
							href="${domainUrl}${contextPath}css/globalHeader.css?${staticAssetsVersionFormat}=${versionNumbers}">
						<script type="text/javascript" charset="UTF-8"
							src="${domainUrl}${contextPath}javascript/globalHeader.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
							<script type="text/javascript" charset="UTF-8"
				src="${TRUJSPath}javascript/globalFooter.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
		       </c:when>				
		       <c:otherwise>
						<link rel="stylesheet"
							href="${domainUrl}${contextPath}css/globalHeader.css">
						<script type="text/javascript" charset="UTF-8"
							src="${domainUrl}${contextPath}javascript/globalHeader.js"></script>
							<script type="text/javascript" charset="UTF-8"
				src="${TRUJSPath}javascript/globalFooter.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
		     </c:otherwise>
	       </c:choose>	
	       <c:forEach var="element" items="${content.HeaderContent}">				
				   <dsp:renderContentItem contentItem="${element}">
				   	<dsp:param name="custized" value="yes" />	
				   	</dsp:renderContentItem>
			 </c:forEach>
		  
	</head>
		<body>	
			<div class="ad-zone-970-90">
           		<div id="OAS_Frame1"></div>             
    		</div>
		
			<c:forEach var="element" items="${content.MainContent}">		
				<dsp:renderContentItem contentItem="${element}">
					<dsp:param name="custized" value="yes" />	
				</dsp:renderContentItem>
					
		</c:forEach>
		<div class="ad-zone-970-90">
         	<div id="OAS_Frame2"></div>             
    	</div>
	
		</body>

			<script type='text/javascript'>
				var URLsplit = window.location.pathname.split("/");
				var sitecodeFromURL = URLsplit[1];
				var pagetypeFromURL = URLsplit[2];
				var pagenameFromURL = URLsplit[3];
				var isMegaMenuClicked = localStorage.getItem('pageFrom');
				var utag_data = {
					page_name : sitecodeFromURL + ": " + pagenameFromURL,
					page_type : sitecodeFromURL + ": " + pagetypeFromURL,
					browser_id : "${pageContext.session.id}",
					customer_status : "${customerStatus}",
				    device_type : "${deviceType}",
				    event_type:"",
				    session_id : "${session_id}",
				    visitor_id : "${visitor_id}",
				    tru_or_bru : sitecodeFromURL,
				    oas_breadcrumb : "${baseBreadcrumb}/shoptemplate/trustaticpagewithoutheaderandfooter",
				    oas_taxonomy : "level1=shoptemplate&level2=null&level3=null&level4=null&level5=null&level6=null",
					oas_sizes : "${oasSizes}"
				};

				if ( pagenameFromURL === undefined ) {
					utag_data.page_name = "${siteCode}: ${pageName}";
					utag_data.page_type = "${siteCode}: ${pageType}";
				}
			
				if ( typeof isMegaMenuClicked === 'string' && (isMegaMenuClicked.indexOf('megaMenuClick') > -1) ) {
					utag_data.event_type = ["shop_by_mega_menu", "gridwall_impression"];
					localStorage.setItem("pageFrom",'');
				}
			
				(function(a,b,c,d){
					a='${tealiumURL}';
					b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
					a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
				})();
			</script>
		</html>
</dsp:page>