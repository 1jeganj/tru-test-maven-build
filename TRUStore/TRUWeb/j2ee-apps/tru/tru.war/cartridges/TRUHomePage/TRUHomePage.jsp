<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<%@ page trimDirectiveWhitespaces="true" %>
	<dsp:getvalueof var="queryString" bean="/OriginatingRequest.queryString" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
	<dsp:importbean bean="/com/tru/commerce/email/TRUEmailNotificationFormHandler" />
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:getvalueof var="isTransient" bean="Profile.transient"/>
    <fmt:message key="homePage.emailBlank.message" var="blankEmail" />
    <fmt:message key="homepage.validEmail.message" var="invalidEmail" />
    <dsp:getvalueof var="gridViewEnabled" value="false"/>
    <dsp:getvalueof var="previewEnabled" bean="/atg/endeca/assembler/cartridge/manager/AssemblerSettings.previewEnabled"/>
    <dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
	<tru:pageContainer>
		<jsp:attribute name="fromHome">home</jsp:attribute>
		<jsp:body>
		<c:choose>
		<c:when test="${previewEnabled}">
		<endeca:pageBody rootContentItem="${rootContentItem}" contentItem="${content}">
			<input type="hidden" value="Home-Page" name="currentpage" id="currentpage"/>
        	<input type="hidden" id="queryURLString" value="${queryString}"/>
			<c:forEach var="element" items="${content.mainContent}">
				<c:if test="${element['@type'] eq 'TruHeroBanner'}">
					<dsp:renderContentItem contentItem="${element}" />
				</c:if>
			</c:forEach>
				<dsp:getvalueof var="skuArray" value=""/>
				<script>
					var skuPriceArrayHomePage = [];
				</script>
			<div class="default-margin sos-banner-bottom">
          		<div class="max-width">
		  			<c:forEach var="element" items="${content.mainContent}">
		  				<c:if
								test="${element['@type'] eq 'TruGridViewSmallProductsView' or element['@type'] eq 'TruGridViewLargeProductsView'}">

                      <dsp:getvalueof var="skuArray"
									value="${skuArray}${element.skuStoreEligibleArray}" />
		  					<dsp:getvalueof var="gridViewEnabled" value="true" />
		  				</c:if>
						<c:if test="${element['@type'] ne 'TruHeroBanner'}">
							<dsp:renderContentItem contentItem="${element}" />
						</c:if>
   					</c:forEach>
   				
   					<c:if test="${gridViewEnabled}">
   					</c:if>
				</div>
			</div>
 			<div id="emailSignupPopupContainer">
 				<dsp:include page="/common/emailSignUpPopUp.jsp" />
 			</div>
 			<div id="tealiumContent">
 			<%-- <dsp:include page="/common/tealiumForHomePage.jsp"/>  --%>
 			</div>
 			<c:if test="${site eq 'BabyRUs'}">
 			<dsp:include page="/common/frag/tealiumForBRUHomePage.jsp"/>
 			</c:if>
 			</endeca:pageBody>
		</c:when>
		<c:otherwise>
			<input type="hidden" value="Home-Page" name="currentpage" id="currentpage"/>
        	<input type="hidden" id="queryURLString" value="${queryString}"/>
			<c:forEach var="element" items="${content.mainContent}">
				<c:if test="${element['@type'] eq 'TruHeroBanner'}">
					<dsp:renderContentItem contentItem="${element}" />
				</c:if>
			</c:forEach>
				<dsp:getvalueof var="skuArray" value=""/>
				<script>
					var skuPriceArrayHomePage = [];
				</script>
			<div class="default-margin sos-banner-bottom">
          		<div class="max-width">
		  			<c:forEach var="element" items="${content.mainContent}">
		  				<c:if
								test="${element['@type'] eq 'TruGridViewSmallProductsView' or element['@type'] eq 'TruGridViewLargeProductsView'}">

                      <dsp:getvalueof var="skuArray"
									value="${skuArray}${element.skuStoreEligibleArray}" />
		  					<dsp:getvalueof var="gridViewEnabled" value="true" />
		  				</c:if>
						<c:if test="${element['@type'] ne 'TruHeroBanner'}">
							<dsp:renderContentItem contentItem="${element}" />
						</c:if>
   					</c:forEach>
   				
   					<c:if test="${gridViewEnabled}">
	   					<div class="modal fade in pdpModals" id="esrbratingModel" tabindex="-1" role="dialog" aria-labelledby="basicModal"	aria-hidden="false">
	   						<dsp:include page="/jstemplate/esrbMature.jsp"></dsp:include>
	   					</div>
	   					<div class="modal fade in" id="shoppingCartModal" data-target="#shoppingCartModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false"></div>
	   					<dsp:include page="/jstemplate/includePopup.jsp" />
	   					<div class="modal fade in" id="findInStoreModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	   						<dsp:include page="/jstemplate/findInStoreInfo.jsp" />
	   					</div>
   					</c:if>
				</div>
			</div>
 			<div id="emailSignupPopupContainer">
 				<dsp:include page="/common/emailSignUpPopUp.jsp" />
 			</div>
 			<div id="tealiumContent">
 			<%-- <dsp:include page="/common/tealiumForHomePage.jsp"/>  --%>
 			</div>
 			<c:if test="${site eq 'BabyRUs'}">
 				<dsp:include page="/common/frag/tealiumForBRUHomePage.jsp"/>
 			</c:if>
		</c:otherwise>
		</c:choose>
			
		</jsp:body>
		<script>
		var skuStoreEligibleArray = '${skuArray}';
		var site = "${site}";
		if(site =='ToysRUs'){
			$(window).load(function() {
	    		ajaxCallForTealium('${originatingRequest.contextPath}');
			});
		}
   		</script>
		
	</tru:pageContainer>
</dsp:page>

