<div class="row default-margin">
            <div class="col-md-12 col-no-padding">
                <div class="ad-zone-970-90">
                    
                </div>
                <div class="gift-finder-results-footer">
                    <header>more ways to give</header>

                    <div class="row-center">
                        <div class="col-md-8 col-no-padding">
                            <div class="row">
                                <div class="col-md-3">
                                    <a href="#"><img src="toolkit/images/gift-finder-results/whats-hot.png" alt="">
                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <a href="#"><img src="toolkit/images/gift-finder-results/oversized-joy.png" alt="">
                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <a href="#"><img src="toolkit/images/gift-finder-results/babys-first-christmas.png" alt="">
                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <a href="#"><img src="toolkit/images/gift-finder-results/gifts-under-30.png" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <ul>
                                <li><a href="#">gift cards <img src="toolkit/images/gift-finder-results/right-arrow.png"></a>
                                </li>
                                <li><a href="#">toy guide for differently-abled kids <img src="toolkit/images/gift-finder-results/right-arrow.png"></a>
                                </li>
                                <li><a href="#">Birthdays "R" Us <img src="toolkit/images/gift-finder-results/right-arrow.png"></a>
                                </li>
                                <li><a href="#">Geoffrey's Birthday Club <img src="toolkit/images/gift-finder-results/right-arrow.png"></a>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>