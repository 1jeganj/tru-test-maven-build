<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" bean="/OriginatingRequest.contentItem" />
	<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations" />
	<dsp:getvalueof var="prodCategory" bean="EndecaConfigurations.prodCategory" />
	<dsp:getvalueof var="contextPath" value="${originatingRequest.contextPath}" />
	<dsp:importbean	bean="/atg/commerce/catalog/comparison/ProductListHandler" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:getvalueof var="teliumBreadCrumbString" value="" scope="request" />
	<dsp:getvalueof var="subcat" param="subcat" />
	<dsp:droplet name="/atg/dynamo/droplet/ForEach">
		<dsp:param bean="ProductListHandler.productList.items" name="array" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="prodListCatId" param="element.category.id" />
		</dsp:oparam>
	</dsp:droplet>
	<c:forEach var="element" items="${content.MainContent}">
		<c:if test="${element['@type'] eq 'TruBreadcrumbs'}">
			<dsp:getvalueof var="refinementCrumbs" value="${element.refinementCrumbs[0]}" />
			<dsp:getvalueof var="catName" value="${refinementCrumbs.properties['displayName_en']}" />
			<dsp:getvalueof var="repositoryId" value="${refinementCrumbs.properties['category.repositoryId']}" />
		</c:if>
	</c:forEach>
	      <c:if test="${not empty contentItem.refinementCrumbs}">
		<c:forEach var="refineCrumb" items="${contentItem.refinementCrumbs}">
			<c:if test="${refineCrumb.properties['category.repositoryId'] != prodListCatId}">
				<dsp:setvalue bean="ProductListHandler.clearList" value="true" />
			</c:if>
			<c:if test="${refineCrumb.dimensionName eq prodCategory}">
				<dsp:getvalueof var="classificationBreadcrumb" value="${refineCrumb}" />

				<c:if test="${not empty classificationBreadcrumb.ancestors}">
					<dsp:getvalueof var="ancestorsIds" value="" />

					<c:forEach var="ancestors" items="${classificationBreadcrumb.ancestors}" varStatus="status">
                         <c:if test="${status.count eq 1}">
							<dsp:getvalueof var="ancestorsIds" value="${ancestors.properties['category.repositoryId']}" />
						</c:if>
						   <c:if test="${status.count gt 1}">
							<c:set var="ancestorsIds" value="${ancestorsIds}:${ancestors.properties['category.repositoryId']}" />
						   </c:if>
					</c:forEach>
				</c:if>
		<dsp:getvalueof var="categoryPageId" value="${refineCrumb.properties['category.repositoryId']}"/>
		<dsp:droplet name="/atg/commerce/endeca/cache/DimensionValueCacheDroplet">
					<dsp:param name="repositoryId" value="${categoryPageId}" />
					<dsp:param name="ancestors" value="${ancestorsIds}" />
					<dsp:oparam name="output">
						<dsp:getvalueof var="dimCacheObject" param="dimensionValueCacheEntry" />
					</dsp:oparam>
				</dsp:droplet>
				<dsp:getvalueof var="NValue" value="${dimCacheObject.dimvalId}"/>
				<input type="hidden" value= "${NValue}" id="Nvalue"/>
			</c:if>
			<c:if test="${refineCrumb.dimensionName eq 'Brand' or refineCrumb.dimensionName eq 'Character' or refineCrumb.dimensionName eq 'product.brand'}">
				<dsp:getvalueof var="charandBrandBreadcrumb" value="${refineCrumb.label}" />
			</c:if>
		</c:forEach>
	</c:if>
	<dsp:getvalueof var="parentCategoryId" value=""/>	
	<c:choose>
		<c:when test="${not empty classificationBreadcrumb}">
			<%-- Start Breadcrumbs div --%>
			<div class="PLPBreadcrumbTemplate">
				<div class="row row-no-padding">
					<div class="col-md-12 col-no-padding">
						<div>
							<ul class="category-breadcrumb">
								<li>
									<a href="${contextPath}" >
										home
									</a>
								</li>
								<c:choose>
									<c:when test="${not empty classificationBreadcrumb.ancestors}">
										<c:forEach var="ancestors" items="${classificationBreadcrumb.ancestors}" varStatus="status">
											<c:if test="${ancestors.properties['dimval.prop.category.displayStatus'] ne 'NO DISPLAY'}">
												<dsp:getvalueof var="ancestorLabel" value="${ancestors.label}"/>
												<dsp:getvalueof var="parentCategoryId" value="${ancestors.properties['category.repositoryId']}"/>
												<c:if test="${not empty parentCategoryId}">
													<dsp:getvalueof var="crossReferenceDisplayList" value="${ancestor.properties['dimval.prop.category.crossRefDisplayName']}"/>
													
													<c:forEach var="crossReferenceDisplayListItem" items="${crossReferenceDisplayList}">
													<c:if test="${fn:contains(crossReferenceDisplayListItem, parentCategoryId)}">
													<c:set var="crossReferenceDisplayItem" value="${fn:split(crossReferenceDisplayListItem, '||')}" />
													<c:if test="${fn:length(crossReferenceDisplayItem) gt 1}">
													<c:set var="ancestorLabel" value="${crossReferenceDisplayItem[1]}"/>
													</c:if>
													</c:if>
													</c:forEach>					
												</c:if>
												<li class="plp-update-breadcrumb">
													<c:choose>
														<c:when test="${status.first}">
															<dsp:getvalueof var="teliumBreadCrumbString" value="/${ancestorLabel}" scope="request" />
															<c:if test="${!(contentItem.disableDropdown eq 'true')}">
																<dsp:getvalueof var="showDD" value="showDropDown"></dsp:getvalueof>
															</c:if>
															<c:if test="${!(contentItem.disableDropdown eq 'true') and subcat ne 'subcat'}">
																<dsp:getvalueof var="showDD" value="showDropDown removeMargin"/>
															</c:if>
																<c:forEach var="rootClassification" items="${contentItem.rootClassifications}">
																	<c:forEach var="levelOneCategory"
																		items="${rootClassification.value}">
																		<c:if test="${ancestorLabel eq levelOneCategory.key}">
																		<a href="${siteURL}${levelOneCategory.value}&catdim=bcmb">
																				${ancestorLabel}
																			</a>
																		</c:if>
																	</c:forEach>
																</c:forEach>
														</c:when>
														<c:otherwise>
															<c:if test="${ancestors.properties['dimval.prop.category.displayStatus'] ne 'NO DISPLAY'}">
																<dsp:getvalueof var="teliumBreadCrumbString" value="${teliumBreadCrumbString}/${ancestorLabel}" scope="request" />
																<dsp:getvalueof var="categoryId" value="${ancestors.properties['category.repositoryId']}" />

																<dsp:droplet name="/atg/commerce/endeca/cache/DimensionValueCacheDroplet">
																	<dsp:param name="repositoryId" value="${categoryId}" />
																	<dsp:oparam name="output">
																		<dsp:getvalueof var="dimCacheObject" param="dimensionValueCacheEntry" />
																	</dsp:oparam>
																</dsp:droplet>
																<a href="${siteURL}${dimCacheObject.url}&catdim=bcmb">
																 	<c:set var="telcatname" value="${ancestorLabel}"	scope="request" />
																	${ancestorLabel}
																</a>
															</c:if>
														</c:otherwise>
													</c:choose>
												</li>
											</c:if>
											<dsp:getvalueof var="parentCategoryId" value="${ancestors.properties['category.repositoryId']}"/>
										</c:forEach>
										<c:if test="${not empty parentCategoryId}">
													<dsp:getvalueof var="crossReferenceDisplayList" value="${classificationBreadcrumb.properties['dimval.prop.category.crossRefDisplayName']}"/>
													
													<c:forEach var="crossReferenceDisplayListItem" items="${crossReferenceDisplayList}">
													<c:if test="${fn:contains(crossReferenceDisplayListItem, parentCategoryId)}">
													<c:set var="crossReferenceDisplayItem" value="${fn:split(crossReferenceDisplayListItem, '||')}" />
													<c:if test="${fn:length(crossReferenceDisplayItem) gt 1}">
													<c:set var="breadcrumbLabel" value="${crossReferenceDisplayItem[1]}"/>
													</c:if>
													</c:if>
													</c:forEach>					
												</c:if>
										<li class="plp-update-breadcrumb">
											<dsp:getvalueof var="teliumBreadCrumbString" value="${teliumBreadCrumbString}/${classificationBreadcrumb.label}" scope="request" />
											<input type="hidden" id="CurrentCategoryRepositoryId" value=${classificationBreadcrumb.properties['category.repositoryId']} />
											<c:choose>
												<c:when test="${not empty breadcrumbLabel}">
													${breadcrumbLabel}
												</c:when>
												<c:otherwise>
													${classificationBreadcrumb.label}
												</c:otherwise>
											</c:choose>
										</li>
									</c:when>
									<c:otherwise>
										<dsp:getvalueof var="teliumBreadCrumbString" value="/${classificationBreadcrumb.label}" scope="request" />
									</c:otherwise>
								</c:choose>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
		</c:when>
		<c:when test="${not empty charandBrandBreadcrumb}">
			<div class="search-breadcrumb-block default-margin">
				<div class="col-md-12 col-no-padding">
					<div>
						<ul class="category-breadcrumb">
							<li><a href="${contextPath}">home</a></li>
							<li>
								${charandBrandBreadcrumb}
							</li>
						</ul>
					</div>
				</div>
			</div>
		</c:when>
	</c:choose>
</dsp:page>
