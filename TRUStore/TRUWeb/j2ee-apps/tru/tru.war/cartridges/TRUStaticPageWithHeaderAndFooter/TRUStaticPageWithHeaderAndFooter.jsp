<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
	<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>
	<dsp:getvalueof var="previewEnabled" bean="/atg/endeca/assembler/cartridge/manager/AssemblerSettings.previewEnabled"/>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
	<dsp:getvalueof var="oasSizes" bean="TRUTealiumConfiguration.homeOASSizes"/>
	<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
	<dsp:getvalueof var="baseBreadcrumb" value="tru" />

	<dsp:getvalueof var="siteCode" value="TRU" />
	<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
	<c:if test="${site eq babySiteCode}">
	<dsp:getvalueof var="siteCode" value="BRU" />
	<dsp:getvalueof var="baseBreadcrumb" value="bru" />
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
	</c:if>
	<dsp:getvalueof var="pageName" param="pageName" />
	<dsp:getvalueof var="pageType" param="pageType"/>
	<dsp:getvalueof bean="TRUTealiumConfiguration.deviceType" var="deviceType"/>
	<c:set var="session_id" value="${cookie.sessionID.value}"/>
	<c:set var="visitor_id" value="${cookie.visitorID.value}"/>

	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:getvalueof var="loginStatus" bean="Profile.transient" />
	<c:choose>
		<c:when test="${loginStatus eq 'true'}">
			<c:set var="customerStatus" value="Guest"/>
		</c:when>
		<c:otherwise>
			<c:set var="customerStatus" value="Registered"/>
		</c:otherwise>
	</c:choose>

	<tru:pageContainer>
			<jsp:attribute name="fromStatic">staticPage</jsp:attribute>
			<jsp:body>
				<style>.staticContentPage{ margin-top: 130px;}</style>
				<div class="staticContentPage">
					<div class="ad-zone-970-90">
				           <div id="OAS_Frame1"></div>             
				    </div>
					<c:choose>
					<c:when test="${previewEnabled}">
					  <endeca:pageBody rootContentItem="${rootContentItem}" contentItem="${content}">
					<c:forEach var="element" items="${content.MainContent}">				
						<dsp:renderContentItem contentItem="${element}">				
						</dsp:renderContentItem>	
					</c:forEach>
					</endeca:pageBody>
					</c:when>
					<c:otherwise>
					<c:forEach var="element" items="${content.MainContent}">				
						<dsp:renderContentItem contentItem="${element}">				
						</dsp:renderContentItem>	
					</c:forEach>
					</c:otherwise>
					</c:choose>	
					<div class="ad-zone-970-90">
				         <div id="OAS_Frame2"></div>             
				    </div>		
			   </div>	
			</jsp:body>
		</tru:pageContainer>
		
	
		<%-- <!-- start: Script for Tealium Integration --> --%>
		<script type='text/javascript'>
			var URLsplit = window.location.pathname.split("/");
			var sitecodeFromURL = URLsplit[1];
			var pagetypeFromURL = URLsplit[2];
			var pagenameFromURL = URLsplit[3];
			var isMegaMenuClicked = localStorage.getItem('pageFrom');
			var utag_data = {
				page_name : sitecodeFromURL + ": " + pagenameFromURL,
				page_type : sitecodeFromURL + ": " + pagetypeFromURL,
				browser_id : "${pageContext.session.id}",
			    device_type : "${deviceType}",
			    customer_status : "${customerStatus}",
			    event_type:"",
			    session_id : "${session_id}",
			    visitor_id : "${visitor_id}",
			    tru_or_bru : sitecodeFromURL,
			    oas_breadcrumb : "${baseBreadcrumb}/shoptemplate/trustaticpagewithheaderandfooter",
			    oas_taxonomy : "level1=shoptemplate&level2=null&level3=null&level4=null&level5=null&level6=null",
				oas_sizes : "${oasSizes}"
			};

			if ( pagenameFromURL === undefined ) {
				utag_data.page_name = "${siteCode}: ${pageName}";
				utag_data.page_type = "${siteCode}: ${pageType}";
			}
			
			if ( typeof isMegaMenuClicked === 'string' && (isMegaMenuClicked.indexOf('megaMenuClick') > -1) ) {
				utag_data.event_type = ["shop_by_mega_menu", "gridwall_impression"];
				localStorage.setItem("pageFrom",'');
			}
		
			(function(a,b,c,d){
				a='${tealiumURL}';
				b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
				a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
			})();
		</script>
		<%-- End: Script for Tealium Integration --%>
		
</dsp:page>