<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations" />
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration"/>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
	<dsp:importbean bean="/com/tru/commerce/email/TRUEmailNotificationFormHandler" />
	<dsp:getvalueof var="giftFinderUrl" bean="EndecaConfigurations.giftFinder" />
		<dsp:importbean bean="/atg/userprofiling/Profile"/>
       <dsp:getvalueof var="giftFinderURL" bean="Profile.giftFinderURL"/>
     	<dsp:getvalueof var="Nf" value="${originatingRequest.QueryString}" />
     	     <dsp:getvalueof var="N" param="N" />
    <dsp:getvalueof var="isTransient" bean="Profile.transient"/>
    <dsp:getvalueof var="Ntt" param="keyword"/>
    <dsp:getvalueof var="contextPath"	value="${originatingRequest.contextPath}" />
 	<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
	<dsp:getvalueof var="siteCode" value="TRU" />
	<dsp:getvalueof var="baseBreadcrumb" value="tru" />
 	<dsp:getvalueof var="loginStatus" bean="Profile.transient" />
	<dsp:getvalueof var="customerEmail" bean="Profile.login"/>
	<dsp:getvalueof var="customerId" bean="Profile.id"/>
	<dsp:getvalueof var="customerDob" bean="Profile.dateOfBirth"/>
 	<dsp:getvalueof var="customerStatus" bean="TRUTealiumConfiguration.customerStatus"/>
	<dsp:getvalueof var="pageName" bean="TRUTealiumConfiguration.giftFinderPageName"/>
	<dsp:getvalueof var="pageMsg" value="Successful Search Results"/>
	<dsp:getvalueof var="pageType" bean="TRUTealiumConfiguration.giftFinderPageType"/>
	<dsp:getvalueof var="finderSortGroup" bean="TRUTealiumConfiguration.giftFinderFinderSortGroup"/>
	<dsp:getvalueof var="finderType" bean="TRUTealiumConfiguration.giftFinderFinderType"/>
	<dsp:getvalueof var="browserId" bean="TRUTealiumConfiguration.browserId"/>
	<dsp:getvalueof var="deviceType" bean="TRUTealiumConfiguration.deviceType"/>
	<dsp:getvalueof var="customerType" bean="TRUTealiumConfiguration.customerType"/>
	<dsp:getvalueof var="internalCampaignPage" bean="TRUTealiumConfiguration.internalCampaignPage"/>
	<dsp:getvalueof var="orsoCode" bean="TRUTealiumConfiguration.orsoCode"/>
	<dsp:getvalueof var="ispuSource" bean="TRUTealiumConfiguration.ispuSource"/>
	<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
	<dsp:getvalueof var="toysSiteCode" bean="TRUTealiumConfiguration.toysSiteCode"/>  
    <dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>
    <dsp:getvalueof var="customerFirstName" bean="Profile.firstName"/>
	<dsp:getvalueof var="customerLastName" bean="Profile.lastName"/>
	<c:set var="space" value=" "/>
	<c:choose>
	<c:when test="${empty customerFirstName && empty customerLastName}">
		<c:set var="customerName" value='${fn:substring(customerEmail, 0, fn:indexOf(customerEmail, "@"))}'/>
	</c:when>
	<c:when test="${not empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value="${customerFirstName}"/>						
	</c:when>
	<c:when test="${empty customerFirstName && not empty customerLastName}">
		   <c:set var="customerName" value="${customerLastName}"/>						
	</c:when>
	<c:otherwise>
			<c:set var="customerName" value="${customerFirstName}${space}${customerLastName}"/>		
	</c:otherwise>
 </c:choose>
	<c:choose>
		<c:when test="${loginStatus eq 'true'}">
			<c:set var="customerStatus" value="Guest"/>
		</c:when>
		<c:otherwise>
			<c:set var="customerStatus" value="Registered"/>
			<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" bean="Profile.shippingAddress.id" />
				<dsp:oparam name="false">
					<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="Profile.secondaryAddresses"/>
						<dsp:param name="elementName" value="address"/>
						<dsp:oparam name="output">
							<dsp:getvalueof param="address.id" var="addressId" />
							<c:if test="${addressId eq shippingAddressId}">						 				
								 <dsp:getvalueof param="address.city" var="customerCity"/> 
								 <dsp:getvalueof param="address.state" var="customerState"/> 
								 <dsp:getvalueof param="address.postalCode" var="customerZip"/>
								 <dsp:getvalueof param="address.country" var="customerCountry"/>							
							 </c:if>
						</dsp:oparam>
				   </dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</c:otherwise>
	</c:choose>
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
	<c:if test="${site eq babySiteCode}">
		<dsp:getvalueof var="siteCode" value="BRU" />
		<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
		<dsp:getvalueof var="baseBreadcrumb" value="bru" />			
 	</c:if>   
  <c:set var="isSosVar" value="${cookie.isSOS.value}"/>
	<c:choose>
	<c:when test="${isSosVar eq 'true'}">
	<c:set var="selectedStore" value="sos"/>
	</c:when>
	<c:otherwise>
	<c:set var="selectedStore" value=""/>
	</c:otherwise>
	</c:choose>
      <dsp:getvalueof var="deletefinder" value="false"/>
     <fmt:message key="homePage.emailBlank.message" var="blankEmail" />
    <fmt:message key="homepage.validEmail.message" var="invalidEmail" />
	<tru:pageContainer>
	<jsp:attribute name="fromGiftFinder">GiftFinder</jsp:attribute>
	<jsp:body>	
	   <script type='text/javascript'>	
			  var utag_data = {			
			    customer_status : "${customerStatus}",			
			    page_name : "${siteCode}: ${pageName}: ${pageMsg}",			
			    page_type : "${pageType}",			
			    browser_id : "${pageContext.session.id}",
			    device_type : "${deviceType}",			
			    finder_sort_group : "${finderSortGroup}",			
			    finder_type : "${finderType}",			
			    customer_city : "${customerCity}",
			    customer_country : "${customerCountry}",
			    customer_email : "${customerEmail}",
			    customer_id : "${customerId}",
			    customer_type : "${customerStatus}",
			    customer_state : "${customerState}",
			    customer_zip : "${customerZip}",
			    customer_dob : "${customerDob}",
			    internal_campaign_page : "${internalCampaignPage}",
				oas_breadcrumb : "${baseBreadcrumb}/giftfinder",
			    oas_taxonomy : "level1=giftfinder&level2=null&level3=null&level4=null&level5=null&level6=null",
				oas_sizes : [ ['Frame1', [970,90]],['Frame2', [970,90]] ],
			    orso_code : "${ orsoCode}",
			    ispu_source : "${ispuSource}",
			    partner_name:"${partnerName}",
			    customer_name:"${customerName}",
			    selected_store:"${selectedStore}",
			    tru_or_bru : "${siteCode}"
			};
	
		</script>				
		
		<!-- Start: Script for Tealium Integration -->
		<script type="text/javascript">

				(function(a,b,c,d){
				a='${tealiumURL}';
				b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
				a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
				})();

		</script>	
    <c:forEach var="element" items="${content.MainProduct}">
		<c:if test="${element['@type']  eq 'TRUResultsList'}">
		<dsp:getvalueof var="resultsListItem" value="${element}"/>
		</c:if>
		</c:forEach>
	<c:forEach var="element" items="${content.MainHeader}">
					<c:if test="${element['@type'] eq 'TruHtmlContent-ATGTargeter'  or element['@type'] eq 'TRUAdZoneTargeter' }">		
				<dsp:renderContentItem contentItem="${element}" />
		  </c:if>
			
			</c:forEach>
	<div>
	<div class="row default-margin">
		<div class="col-md-12 col-no-padding">
			<div>
				<ul class="gift-finder-results-breadcrumb">
					<li>
						<a href="/">
							home
						</a>
					</li>
					<li>
						gift finder
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="row default-margin">
	<div class="col-mid-12 col-no-padding">
		<div class="gift-finder-results-select-zone">
			<div class="select-wrapper">
				<select id="giftNameOption">
				      <c:choose>
				      <c:when test="${!isTransient}">
				       <c:forEach var="entry" items="${giftFinderURL}">
				        <dsp:getvalueof var="name"
		value="${fn:substringAfter(entry.value,'giftFinder?')}" />
							<option  value="${contextPath}${entry.value}" <c:if test="${Nf eq name}"> selected="selected"</c:if> >${entry.key}</option>
                               </c:forEach>
                               <c:if test="${giftFinderURL.size() gt 1}"> 
                     	    <dsp:getvalueof var="deletefinder" value="true" />
                     	</c:if>  
                               </c:when>
                               <c:otherwise>
                             <dsp:droplet
				name="/com/tru/commerce/droplet/TRUGiftFinderDroplet">	
	<dsp:oparam name="output">
		<dsp:getvalueof param="element" var="abc" />	
	 <c:forEach var="entry" items="${abc}">
	 <dsp:getvalueof var="name"
		value="${fn:substringAfter(entry.value,'giftFinder?')}" />
						<option  value="${contextPath}${entry.value}" <c:if test="${Nf eq name}"> selected="selected"</c:if> >${entry.key}</option>
                               </c:forEach>
                     	<c:if test="${abc.size() gt 1}"> 
                     	    <dsp:getvalueof var="deletefinder" value="true" />
                     	</c:if>     
		</dsp:oparam name="output">
	</dsp:droplet>
                               </c:otherwise>
					</c:choose>
				</select>
			</div>
		<c:if test="${deletefinder}">
			
			<div><a href="javascript:void(0);" id="deleteThisFinder">delete this finder</a>
		</div>
		</c:if>
	</div>
</div>
</div>
		<div id="narrow-by-scroll" class="product-content">
		<c:forEach var="element" items="${content.MainProduct}">
		
	     <c:if test="${element['@type'] eq 'TRUCrumbFacets'}">		
				
					<dsp:renderContentItem contentItem="${element}" />
			
			</c:if>
			<c:if test="${element['@type'] eq 'TRUGuidedNavigation'}">		
				
					<dsp:renderContentItem contentItem="${element}">
					 <dsp:param name="resultsListItem" value="${resultsListItem}" />
					</dsp:renderContentItem>
			
			</c:if>
			<c:if test="${element['@type'] eq 'TRUResultsList'}">		
				
					<dsp:renderContentItem contentItem="${element}" />
			
			</c:if>
			
		</c:forEach>
		</div>
		
			<c:forEach var="element" items="${content.MainFooter}">
				
					<c:if test="${element['@type'] eq 'TRUAdZoneTargeter'}">		
				<dsp:renderContentItem contentItem="${element}" />
					</c:if>
						<c:if test="${element['@type'] eq 'TruHtmlContent-ATGTargeter'}">		
				<dsp:renderContentItem contentItem="${element}" />
					</c:if>
			
			</c:forEach>
	
	</jsp:body>
	
		</tru:pageContainer>
		
		</dsp:page>