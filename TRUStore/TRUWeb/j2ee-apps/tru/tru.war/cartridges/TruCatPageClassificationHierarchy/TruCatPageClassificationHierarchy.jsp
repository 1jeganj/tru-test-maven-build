<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" bean="/OriginatingRequest.contentItem" />
	<dsp:getvalueof var="contextPath" value="${originatingRequest.contextPath}" />
	<dsp:getvalueof var="twoLevelCategoryTreeMap" value="${contentItem.twoLevelCategoryTreeMap}" />
	<dsp:getvalueof var="currentCatId" value="${contentItem.CurrentCategoryId}" />
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
	<dsp:getvalueof var="categoryImageResizeMap" bean="TRUStoreConfiguration.categoryImageResizeMap" />
	<dsp:getvalueof var="imageHeight" value=""/>
 	<dsp:getvalueof var="imageWidth" value=""/>
 	<c:forEach var="entry" items="${categoryImageResizeMap}">
		<c:if test="${entry.key eq 'imageHeight'}">
			<c:set var="categoryImageHeight" value="${entry.value}" />
     	</c:if>
     	<c:if test="${entry.key eq 'imageWidth'}">
			<c:set var="categoryImageWidth" value="${entry.value}" />
     	</c:if>
	</c:forEach>
	<c:if test="${not empty twoLevelCategoryTreeMap}">
	<c:set var="categoryBlockThirdElement" value="0" />
	<div class="row-max-width catListinginCat">
	<h2 class='featuredcategories'>featured categories</h2>
		<c:forEach var="levelTwoCategory" items="${twoLevelCategoryTreeMap[currentCatId]}">
		<c:set var="categoryBlockThirdElement" value="${categoryBlockThirdElement+1}" />
			<dsp:getvalueof var="secondLevelCatId" value="${levelTwoCategory.categoryId}" />
			<c:choose>
				<c:when test="${categoryBlockThirdElement % 3 eq 1 }">
					<div class="col-md-4 block-3-column clear-fix">
				</c:when>
				<c:otherwise>
					<div class="col-md-4 block-3-column">
				</c:otherwise>
			</c:choose>
					<div class="category-image-link-block">
						<c:if test="${ not empty levelTwoCategory.categoryImage}">
						<dsp:droplet name="/com/tru/common/droplet/TRUAkamaiImageDroplet">
 							<dsp:param name="imageName" value="${levelTwoCategory.categoryImage}"/>
 							<dsp:param name="imageHeight" value="300"/>
 							<dsp:param name="imageWidth" value="300"/>
 							<dsp:oparam name="output">
 							<dsp:getvalueof var="akamaiImageUrl" param="akamaiUrl"/>
 							</dsp:oparam>
					 </dsp:droplet>
					 <div class="subcatogary-image-wrapper">
							<a href="${levelTwoCategory.categoryURL}">
								<img src="${akamaiImageUrl}" alt="category block image"> 
							</a></div>
						</c:if>
						<c:if test="${ empty levelTwoCategory.categoryImage}">
						<dsp:droplet name="/com/tru/common/droplet/TRUAkamaiImageDroplet">
 							<dsp:param name="imageHeight" value="${categoryImageHeight}"/>
 							<dsp:param name="imageWidth" value="${categoryImageWidth}"/>
 							<dsp:oparam name="output">
 								<dsp:getvalueof var="akamaiImageUrl" param="akamaiUrl"/>
 							</dsp:oparam>
					 </dsp:droplet>
							<a href="${levelTwoCategory.categoryURL}&catdim=bcmb">
								<img src="${akamaiImageUrl}" alt=""> 
							</a>
						</c:if>
						<ul class="category-image-link-block-list">
							<li>
								<a href="${levelTwoCategory.categoryURL}&catdim=bcmb">${levelTwoCategory.categoryName}</a>
							</li>
							<c:forEach var="levelThreeCategory"
								items="${twoLevelCategoryTreeMap[secondLevelCatId]}" end="5">
								<li>
									<a href="${levelThreeCategory.categoryURL}&catdim=bcmb">${levelThreeCategory.categoryName}</a>
								</li>
							</c:forEach>
							<c:choose>
								<c:when	test="${fn:length(twoLevelCategoryTreeMap[secondLevelCatId]) gt 6}">
									<li class="category-image-link-list">
										<a class="category-more-cta" href="${levelTwoCategory.categoryURL}"><fmt:message key="tru.cat.more"/></a>
									</li>
								</c:when>
								<c:otherwise>
									<li></li>
								</c:otherwise>
							</c:choose>
						</ul>
					</div>
				</div>
		</c:forEach>
		<c:set var="categoryBlockThirdElement" value="0" />
	</div>
	</c:if>
</dsp:page>