<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations" />
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
	<dsp:getvalueof var="previewEnabled" bean="/atg/endeca/assembler/cartridge/manager/AssemblerSettings.previewEnabled"/>
	<dsp:getvalueof var="contextPath" value="${originatingRequest.contextPath}" />
	<dsp:getvalueof var="searchPageUrl"	bean="EndecaConfigurations.searchPageUrl" />
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
	<dsp:importbean bean="/com/tru/common/TRUIntegrationConfiguration" />
	<dsp:importbean bean="/com/tru/common/TRUSOSIntegrationConfiguration" />
	<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="siteCode" value="TRU" />
	<dsp:getvalueof var="baseBreadcrumb" value="tru" />
	<c:set var="noSearchPage" scope="request" value="noSearchPage" />

	<dsp:getvalueof var="loginStatus" bean="Profile.transient" />
	<dsp:getvalueof var="customerEmail" bean="Profile.login" />
	<dsp:getvalueof var="customerID" bean="Profile.id" />
	<dsp:getvalueof var="customerDOB" bean="Profile.dateOfBirth" />

	<c:forEach var="element" items="${content.MainContent}">
		<c:if test="${element['@type'] eq 'SearchAdjustments'}">
			<c:forEach var="element1" items="${element.originalTerms}">
				<dsp:getvalueof var="originalTerm" value="${element1}" />
			</c:forEach>
		</c:if>
	</c:forEach>

	<dsp:getvalueof param="pageName" var="tPageName" />
	<dsp:getvalueof param="pageType" var="tPageType" />
	<dsp:getvalueof param="page" var="page" />

	<dsp:getvalueof bean="TRUTealiumConfiguration.searchNoResultsPageName" var="searchPageName" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchPageType" var="searchPageType" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchOASSizes" var="tOasSizes" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchOASBreadcrumb" var="tOasBreadcrumb" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchOASTaxonamy" var="tOasTaxonomy" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.browserId" var="tBrowserID" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.deviceType" var="tDeviceType" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchOrsoCode" var="searchOrsoCode" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchIspuSource" var="searchIspuSource" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchRefinementType"	var="searchRefinementType" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchRecent"	var="searchRecent" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchSuccess" var="searchSuccess" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchSynonym" var="searchSynonym" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchTest" var="searchTest" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchRefinementValue" var="searchRefinementValue" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchIntCampaignPage" var="searchIntCampaignPage" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.searchFinderSortGroup" var="finderSortGroup" />
	<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode" />
	<dsp:getvalueof var="toysSiteCode" bean="TRUTealiumConfiguration.toysSiteCode" />
	<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL" />
	<dsp:getvalueof var="storeCertona" bean="TRUIntegrationConfiguration.enableCertona" />
	<dsp:getvalueof var="sosCertona" bean="TRUSOSIntegrationConfiguration.enableCertona" />

	<dsp:getvalueof var="customerFirstName" bean="Profile.firstName" />
	<dsp:getvalueof var="customerLastName" bean="Profile.lastName" />
	<c:set var="space" value=" " />
	<c:choose>
		<c:when test="${empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value='${fn:substring(customerEmail, 0, fn:indexOf(customerEmail, "@"))}' />
		</c:when>
		<c:when test="${not empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value="${customerFirstName}" />
		</c:when>
		<c:when	test="${empty customerFirstName && not empty customerLastName}">
			<c:set var="customerName" value="${customerLastName}" />
		</c:when>
		<c:otherwise>
			<c:set var="customerName" value="${customerFirstName}${space}${customerLastName}" />
		</c:otherwise>
	</c:choose>

	<c:choose>
		<c:when test="${loginStatus eq 'true'}">
			<c:set var="customerStatus" value="Guest" />
		</c:when>
		<c:otherwise>
			<c:set var="customerStatus" value="Registered" />
			<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" bean="Profile.shippingAddress.id" />
				<dsp:oparam name="false">
					<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="Profile.secondaryAddresses" />
						<dsp:param name="elementName" value="address" />
						<dsp:oparam name="output">
							<dsp:getvalueof param="address.id" var="addressId" />
							<c:if test="${addressId eq shippingAddressId}">
								<dsp:getvalueof param="address.city" var="customerCity" />
								<dsp:getvalueof param="address.state" var="customerState" />
								<dsp:getvalueof param="address.postalCode" var="customerZip" />
								<dsp:getvalueof param="address.country" var="customerCountry" />
							</c:if>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</c:otherwise>
	</c:choose>

	<dsp:getvalueof var="keyWord" param="keyword" />
	<dsp:getvalueof var="partnerName" bean="TRUTealiumConfiguration.toysPartnerName" />
	<c:if test="${site eq babySiteCode}">
		<dsp:getvalueof var="siteCode" value="BRU" />
		<dsp:getvalueof var="baseBreadcrumb" value="bru" />
		<dsp:getvalueof var="partnerName" bean="TRUTealiumConfiguration.babyPartnerName" />
	</c:if>

	<dsp:droplet name="/com/tru/common/droplet/TRUTealiumCategoryModifierDroplet">
		<dsp:param name="categoryName" value="${keyWord}" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="modifiedKeyWord" param="modifiedCategory" />
		</dsp:oparam>
	</dsp:droplet>

	<c:set var="isSosVar" value="${cookie.isSOS.value}" />
	<c:choose>
		<c:when test="${isSosVar eq 'true'}">
			<c:set var="selectedStore" value="sos" />
		</c:when>
		<c:otherwise>
			<c:set var="selectedStore" value="" />
		</c:otherwise>
	</c:choose>

	<tru:pageContainer>
		<jsp:attribute name="fromNoSearch">NoSearch</jsp:attribute>
		<jsp:body>
		<c:choose>
		<c:when test="${previewEnabled}">
		 <endeca:pageBody rootContentItem="${rootContentItem}" contentItem="${content}">
			<!-- Script added for Tealium integration -->
			<input type="hidden" value="Failed-Search-Results" name="currentpage" id="currentpage" />
			<script type='text/javascript'>
				var utag_data = {
					customer_status : "${customerStatus}",
				    customer_city : "${customerCity}",
				    customer_country : "${customerCountry}",
				    customer_email : "${customerEmail}",
				    customer_id : "${customerID}",
				    customer_type : "${customerStatus}",
				    customer_state : "${customerState}",
				    customer_zip : "${customerZip}",
				    customer_dob : "${customerDOB}",
					page_name : "${siteCode}: ${searchPageName}",
					page_type : "${siteCode}: ${searchPageName}",
					browser_id : "${pageContext.session.id}",
					search_keyword : "${originalTerm}",
					search_results : "0",
					event_type:"",
					search_result_item : "${tSearchResultItem}",
					oas_breadcrumb : "${baseBreadcrumb}/searchresults/${modifiedKeyWord}",
					oas_taxonomy : "level1=searchresults&level2=${modifiedKeyWord}&level3=null&level4=null&level5=null&level6=null",
					oas_sizes : ${tOasSizes},
					device_type : "${tDeviceType}",
					orso_code : "${searchOrsoCode}",
					ispu_source : "${searchIspuSource}",
					search_refinement_type:"${searchRefinementType}",
					search_recent:"${searchRecent}",
					search_success :"Failed",
					search_synonym:"${searchSynonym}",
					search_test:"${searchTest}",
					search_refinement_value:"${searchRefinementValue}",
					internal_campaign_page:"${searchIntCampaignPage}",
					finder_sort_group : "${finderSortGroup}",
					partner_name:"${partnerName}",
					customer_name:"${customerName}",
					selected_store:"${selectedStore}",
					tru_or_bru : "${siteCode}",
					event_type : 'empty_search'
				};
			</script>

			<!-- Start: Script for Tealium Integration -->
			<script type="text/javascript">
			    (function(a,b,c,d){
			    a='${tealiumURL}';
			    b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
			    a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
			    })();
			    $(document).ready(function(){
				    setTimeout(function(){
				    	utag.view({
					    	event_type:'search_performed'
					    })
				    },1000);	
		   		});
			</script>

			<dsp:droplet name="GetSiteTypeDroplet">
				<dsp:param name="siteId" value="${site}" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="currentSite" param="site" />
					<c:set var="currentSite" value="${currentSite}" scope="request" />
				</dsp:oparam>
			</dsp:droplet>

			<c:choose>
				<c:when test="${currentSite eq 'sos'}">
					<c:set var="certona" value="${sosCertona}" scope="request" />
				</c:when>
				<c:otherwise>
					<c:set var="certona" value="${storeCertona}" scope="request" />
				</c:otherwise>
			</c:choose>

			<c:forEach var="element" items="${content.MainContent}">
				<c:if test="${element['name'] eq 'TRUAdZoneTargetersec1'}">
					<dsp:renderContentItem contentItem="${element}" />
				</c:if>
			</c:forEach>

			<div class="search-breadcrumb-block default-margin">
		        <div class="search-breadcrumb">
		            <ul class="category-breadcrumb">
						<li>
							<a href="${contextPath}" >home</a>
						</li>
						<li>
							<span>search results for "${originalTerm}"</span>
						</li>
					</ul>
		        </div>
		    </div>

			<c:forEach var="element" items="${content.MainContent}">
				<c:if test="${element['name'] ne 'TRUAdZoneTargetersec1'}">
					<c:choose>
						<c:when test="${element['name'] eq 'TRUAdZoneTargetersec2'}">
							<dsp:getvalueof var="AdZoneContent2" value="${element}"/>
						</c:when>
						<c:otherwise>
							<dsp:renderContentItem contentItem="${element}" />
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>


			<!--START: Added for Quickview popup -->
			<div class="modal fade in" id="quickviewModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false">
				<dsp:include page="/jstemplate/quickView.jsp"></dsp:include>
			</div>

			<dsp:include page="/jstemplate/includePopup.jsp" />

			<!--END: Added for Quickview popup -->

			<!-- Certona related Divs Start -->
			<c:if test="${certona eq 'true'}">
			  	<c:choose>
					<c:when test="${site eq 'ToysRUs'}">
						<div class="full_width_gray">
							<div id="tnosearch_rr">
								<!-- Toysrus no search results page recommendations appear here  -->
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<div class="full_width_gray">
							<div id="bnosearch_rr">
							 	<!-- Babiesrus no search results page recommendations appear here  -->
							 </div>
						</div>
						<dsp:getvalueof var="siteCode" value="BRU" />
					</c:otherwise>
				</c:choose>
				<div class="rdata" style="visibility: hidden;">
		 			<div class="site">${siteCode}</div>
		 			<div class="customerid">${customerID}</div>
				</div>
			</c:if>
			<!-- Certona related Data End -->

			<c:if test="${not empty AdZoneContent2}">
				<dsp:renderContentItem contentItem="${AdZoneContent2}" />
			</c:if>

		</endeca:pageBody>
		</c:when>
		<c:otherwise>
		<!-- Script added for Tealium integration -->
			<input type="hidden" value="Failed-Search-Results" name="currentpage" id="currentpage" />
			<script type='text/javascript'>
				var utag_data = {
					customer_status : "${customerStatus}",
				    customer_city : "${customerCity}",
				    customer_country : "${customerCountry}",
				    customer_email : "${customerEmail}",
				    customer_id : "${customerID}",
				    customer_type : "${customerStatus}",
				    customer_state : "${customerState}",
				    customer_zip : "${customerZip}",
				    customer_dob : "${customerDOB}",
					page_name : "${siteCode}: ${searchPageName}",
					page_type : "${siteCode}: ${searchPageName}",
					browser_id : "${pageContext.session.id}",
					search_keyword : "${originalTerm}",
					search_results : "0",
					event_type:"",
					search_result_item : "${tSearchResultItem}",
					oas_breadcrumb : "${baseBreadcrumb}/searchresults/${modifiedKeyWord}",
					oas_taxonomy : "level1=searchresults&level2=${modifiedKeyWord}&level3=null&level4=null&level5=null&level6=null",
					oas_sizes : ${tOasSizes},
					device_type : "${tDeviceType}",
					orso_code : "${searchOrsoCode}",
					ispu_source : "${searchIspuSource}",
					search_refinement_type:"${searchRefinementType}",
					search_recent:"${searchRecent}",
					search_success :"Failed",
					search_synonym:"${searchSynonym}",
					search_test:"${searchTest}",
					search_refinement_value:"${searchRefinementValue}",
					internal_campaign_page:"${searchIntCampaignPage}",
					finder_sort_group : "${finderSortGroup}",
					partner_name:"${partnerName}",
					customer_name:"${customerName}",
					selected_store:"${selectedStore}",
					tru_or_bru : "${siteCode}",
					event_type : 'empty_search'
				};
			</script>

			<!-- Start: Script for Tealium Integration -->
			<script type="text/javascript">
			    (function(a,b,c,d){
			    a='${tealiumURL}';
			    b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
			    a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
			    })();
			    $(document).ready(function(){
				    setTimeout(function(){
				    	utag.view({
					    	event_type:'search_performed'
					    })
				    },1000);	
			    });
			</script>

			<dsp:droplet name="GetSiteTypeDroplet">
				<dsp:param name="siteId" value="${site}" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="currentSite" param="site" />
					<c:set var="currentSite" value="${currentSite}" scope="request" />
				</dsp:oparam>
			</dsp:droplet>

			<c:choose>
				<c:when test="${currentSite eq 'sos'}">
					<c:set var="certona" value="${sosCertona}" scope="request" />
				</c:when>
				<c:otherwise>
					<c:set var="certona" value="${storeCertona}" scope="request" />
				</c:otherwise>
			</c:choose>

			<c:forEach var="element" items="${content.MainContent}">
				<c:if test="${element['name'] eq 'TRUAdZoneTargetersec1'}">
					<dsp:renderContentItem contentItem="${element}" />
				</c:if>
			</c:forEach>

			<div class="search-breadcrumb-block default-margin">
	            <ul class="category-breadcrumb">
					<li>
						<a href="${contextPath}">
							home
						</a>
					</li>
					<li>
			            <span>search results for "${originalTerm}"</span>
					</li>
				</ul>
		    </div>

			<c:forEach var="element" items="${content.MainContent}">
				<c:if test="${element['name'] ne 'TRUAdZoneTargetersec1'}">
					<c:choose>
						<c:when test="${element['name'] eq 'TRUAdZoneTargetersec2'}">
							<dsp:getvalueof var="AdZoneContent2" value="${element}"/>
						</c:when>
						<c:otherwise>
							<dsp:renderContentItem contentItem="${element}" />
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>


			<!--START: Added for Quickview popup -->
			<div class="modal fade in" id="quickviewModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false">
				<dsp:include page="/jstemplate/quickView.jsp"></dsp:include>
			</div>

			<dsp:include page="/jstemplate/includePopup.jsp" />

			<!--END: Added for Quickview popup -->

			<!-- Certona related Divs Start -->
			<c:if test="${certona eq 'true'}">
			  	<c:choose>
					<c:when test="${site eq 'ToysRUs'}">
						<div class="full_width_gray">
							<div id="tnosearch_rr">
								<!-- Toysrus no search results page recommendations appear here  -->
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<div class="full_width_gray">
							<div id="bnosearch_rr">
							 	<!-- Babiesrus no search results page recommendations appear here  -->
							 </div>
						</div>
						<dsp:getvalueof var="siteCode" value="BRU" />
					</c:otherwise>
				</c:choose>
				<div class="rdata" style="visibility: hidden;">
		 			<div class="site">${siteCode}</div>
		 			<div class="customerid">${customerID}</div>
				</div>
			</c:if>
			<!-- Certona related Data End -->

			<c:if test="${not empty AdZoneContent2}">
				<dsp:renderContentItem contentItem="${AdZoneContent2}" />
			</c:if>

		</c:otherwise>
		</c:choose>

		</jsp:body>
	</tru:pageContainer>
</dsp:page>