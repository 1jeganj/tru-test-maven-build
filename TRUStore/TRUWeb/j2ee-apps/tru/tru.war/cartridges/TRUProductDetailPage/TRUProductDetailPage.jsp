<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
	<dsp:getvalueof var="requestUri" value="${originatingRequest.RequestURI}"/>
	<dsp:getvalueof var="onlineProductId" param="productId"/>
	<dsp:getvalueof var="parentProductId" param="parentProductId"/>
	<c:choose>
		<c:when test="${not empty requestUri and requestUri eq '/collection'}">
		<dsp:getvalueof var="productId" param="productId"/>
			<dsp:include page="/product/collectionDetails.jsp">
				<dsp:param name="collectionId" value="${productId}"/>
			</dsp:include>
		</c:when>
		<c:otherwise>
			<dsp:include page="/product/productDetails.jsp">
				<dsp:param name="onlineProductId" value="${onlineProductId}"/>
				<dsp:param name="productId" value="${parentProductId}"/>
			</dsp:include>
		</c:otherwise>
	</c:choose>
</dsp:page>