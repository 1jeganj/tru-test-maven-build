<dsp:page>
	<dsp:importbean bean="/atg/commerce/catalog/CatalogNavHistoryCollector" />
    <dsp:importbean bean="/atg/commerce/catalog/CatalogNavHistory"/>
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
	<dsp:importbean bean="/com/tru/common/TRUIntegrationConfiguration" />
	<dsp:importbean bean="/com/tru/common/TRUSOSIntegrationConfiguration" />
	<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="siteCode" value="TRU" />
	<dsp:getvalueof var="baseBreadcrumb" value="tru" />
	<dsp:getvalueof var="tealiumProductImpressionId" param="tealiumProductImpressionId" />
	<dsp:getvalueof var="loginStatus" bean="Profile.transient" />
	<dsp:getvalueof var="customerEmail" bean="Profile.login" />
	<dsp:getvalueof var="customerId" bean="Profile.id" />
	<dsp:getvalueof var="customerDob" bean="Profile.dateOfBirth" />
	<dsp:getvalueof var="customerFirstName" bean="Profile.firstName"/>
	<dsp:getvalueof var="customerLastName" bean="Profile.lastName"/>
	<dsp:getvalueof var="storeCertona" bean="TRUIntegrationConfiguration.enableCertona"/>
	<dsp:getvalueof var="sosCertona" bean="TRUSOSIntegrationConfiguration.enableCertona"/>
	<c:set var="space" value=" "/>
		<c:choose>
	<c:when test="${empty customerFirstName && empty customerLastName}">
		<c:set var="customerName" value='${fn:substring(customerEmail, 0, fn:indexOf(customerEmail, "@"))}'/>
	</c:when>
	<c:when test="${not empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value="${customerFirstName}"/>
	</c:when>
	<c:when test="${empty customerFirstName && not empty customerLastName}">
		   <c:set var="customerName" value="${customerLastName}"/>
	</c:when>
	<c:otherwise>
			<c:set var="customerName" value="${customerFirstName}${space}${customerLastName}"/>
	</c:otherwise>
 </c:choose>
	<dsp:getvalueof var="pageName"
		bean="TRUTealiumConfiguration.categoryPageName" />
	<dsp:getvalueof var="pageType"
		bean="TRUTealiumConfiguration.categoryPageType" />
	<dsp:getvalueof var="pageCategory"
		bean="TRUTealiumConfiguration.categoryPageCategory" />
	<dsp:getvalueof var="siteSection"
		bean="TRUTealiumConfiguration.categorySiteSection" />
	<dsp:getvalueof var="browserId"
		bean="TRUTealiumConfiguration.browserId" />
	<dsp:getvalueof var="productCompared"
		bean="TRUTealiumConfiguration.categoryProductCompared" />
	<dsp:getvalueof var="deviceType"
		bean="TRUTealiumConfiguration.deviceType" />
	<dsp:getvalueof var="oasBreadcrumb"
		bean="TRUTealiumConfiguration.categoryOASBreadcrumb" />
	<dsp:getvalueof var="oasTaxonomy"
		bean="TRUTealiumConfiguration.categoryOASTaxonamy" />
	<dsp:getvalueof var="oasSizes"
		bean="TRUTealiumConfiguration.categoryOASSizes" />
	<dsp:getvalueof var="customerType"
		bean="TRUTealiumConfiguration.customerType" />
	<c:set var="cat" scope="request" value="cat" />
	<dsp:getvalueof var="internalCampaignPage"
		bean="TRUTealiumConfiguration.internalCampaignPage" />
	<dsp:getvalueof var="orsoCode" bean="TRUTealiumConfiguration.orsoCode" />
	<dsp:getvalueof var="ispuSource"
		bean="TRUTealiumConfiguration.ispuSource" />
	<dsp:getvalueof var="babySiteCode"
		bean="TRUTealiumConfiguration.babySiteCode" />
	<dsp:getvalueof var="toysSiteCode"
		bean="TRUTealiumConfiguration.toysSiteCode" />
		<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
		<c:if test="${site eq babySiteCode}">
		<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
		</c:if>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof var="content"
		vartype="com.endeca.infront.assembler.ContentItem"
		value="${originatingRequest.contentItem}" />
	<dsp:getvalueof var="tealiumURL"
		bean="TRUTealiumConfiguration.tealiumURL" />
<c:choose>
	<c:when test="${loginStatus eq 'true'}">
		<c:set var="customerStatus" value="Guest" />
	</c:when>
	<c:otherwise>
		<c:set var="customerStatus" value="Registered" />
		<dsp:getvalueof var="shippingAddressId"
			bean="Profile.shippingAddress.id"></dsp:getvalueof>
		<dsp:droplet name="IsEmpty">
			<dsp:param name="value" bean="Profile.shippingAddress.id" />
			<dsp:oparam name="false">
				<dsp:droplet name="ForEach">
					<dsp:param name="array" bean="Profile.secondaryAddresses" />
					<dsp:param name="elementName" value="address" />
					<dsp:oparam name="output">
						<dsp:getvalueof param="address.id" var="addressId" />
						<c:if test="${addressId eq shippingAddressId}">
							<dsp:getvalueof param="address.city" var="customerCity" />
							<dsp:getvalueof param="address.state" var="customerState" />
							<dsp:getvalueof param="address.postalCode" var="customerZip" />
							<dsp:getvalueof param="address.country" var="customerCountry" />
						</c:if>
					</dsp:oparam>
				</dsp:droplet>
			</dsp:oparam>
		</dsp:droplet>
	</c:otherwise>
</c:choose>
<dsp:droplet name="GetSiteTypeDroplet">
	<dsp:param name="siteId" value="${site}"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="currentSite" param="site"/>
		<c:set var="currentSite" value ="${currentSite}" scope="request"/>
	</dsp:oparam>
</dsp:droplet>
<c:choose>
	<c:when test="${currentSite eq 'sos'}">
		<c:set var="certona" value="${sosCertona}" scope="request"/>
	</c:when>
	<c:otherwise>
		<c:set var="certona" value="${storeCertona}" scope="request"/>
	</c:otherwise>
</c:choose>
<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
<c:choose>
<c:when test="${isSosVar eq 'true'}">
<c:set var="selectedStore" value="sos"/>
</c:when>
<c:otherwise>
<c:set var="selectedStore" value=""/>
</c:otherwise>
</c:choose>

<c:forEach var="element" items="${content.MainContent}">
	<c:if test="${element['@type'] eq 'TruBreadcrumbs'}">
		<dsp:getvalueof var="refinementCrumbs"
			value="${element.refinementCrumbs[0]}" />
		<dsp:getvalueof var="catName"
			value="${refinementCrumbs.properties['displayName_en']}" />
		<dsp:getvalueof var="repositoryId"
			value="${refinementCrumbs.properties['category.repositoryId']}" />
	</c:if>
</c:forEach>
<dsp:getvalueof var="NValue" value="${repositoryId}"/>

	<dsp:droplet name="CatalogNavHistoryCollector">
	     <dsp:param name="item" value="${repositoryId}" />
	     <dsp:param name="navAction" value="push"/>
	 </dsp:droplet>
    <%-- <input type="hidden" id="categoryId" value="${repositoryId}"/> --%>
<dsp:droplet
	name="/com/tru/common/droplet/TRUTealiumCategoryModifierDroplet">
	<dsp:param name="categoryName" value="${catName}" />
	<dsp:oparam name="output">
		<dsp:getvalueof var="modifiedCategory" param="modifiedCategory" />
	</dsp:oparam>
</dsp:droplet>
<c:if test="${site eq babySiteCode}">
	<dsp:getvalueof var="siteCode" value="BRU" />
	<dsp:getvalueof var="baseBreadcrumb" value="bru" />
</c:if>
		<tru:pageContainer>
			<jsp:attribute name="fromCat">cat</jsp:attribute>
			<jsp:body>
         <div class="modal fade in" id="quickviewModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false">
			<dsp:include page="/jstemplate/quickView.jsp"></dsp:include>
		</div>
		<%-- <dsp:include page="/jstemplate/registryModalDialog.jsp">
			<dsp:param name="page" value="compare"/>
   		</dsp:include> --%>
			<dsp:include page="/jstemplate/includePopup.jsp" />
			<div class="modal fade" id="notifyMeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        	<dsp:include page="/jstemplate/notifyMePopUp.jsp"></dsp:include>                           
   			 </div>
   			 <dsp:include page="/jstemplate/privacyPolicyTemplate.jsp">
  			  </dsp:include>
  			<div class="modal fade in pdpModals esrbratingModel" id="esrbratingModel" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false">
				<dsp:include page="/jstemplate/esrbMature.jsp"></dsp:include>
			</div>
	<%-- Certona parameter Start --%>
	<dsp:getvalueof var="category" value="${catName}" />
	<dsp:getvalueof var="agefilter" value="" />
	<dsp:getvalueof var="gender" value="" />
	<dsp:getvalueof var="brand" value="" />
	<dsp:getvalueof var="charactertheme" value="" />
	<dsp:getvalueof var="currentprice" value="" />
	<%-- Certona parameter End --%>
	<%-- <div class="container-fluid default-template category-template"> --%>
		<c:forEach var="element" items="${content.MainContent}">
			<c:if test="${element['@type'] eq 'TruBreadcrumbs'}">
				<dsp:getvalueof var="catId"
					value="${refinementCrumbs.properties['category.repositoryId']}"
					scope="request" />
				<c:set var="catID" value="${catId}" scope="request" />
			</c:if>
		</c:forEach>
		<input type="hidden" value="Category" name="currentpage" id="currentpage"/>
		<c:set var="cat" scope="request" value="cat" />
		<c:forEach var="element" items="${content.MainContent}">
           <c:if test="${element['@type'] eq 'TruBreadcrumbs'}">
	          <dsp:getvalueof var="catId"
							value="${refinementCrumbs.properties['category.repositoryId']}"
							scope="request" />
				<c:set var="catID" value="${catId}" scope="request" />
		</c:if>
                 <dsp:renderContentItem contentItem="${element}" />
		</c:forEach>
                 <dsp:getvalueof var="teliumBreadCrumbString"
					param="teliumBreadCrumbString" />
	<dsp:droplet
					name="/com/tru/common/droplet/TRUTealiumCategoryModifierDroplet">
		<dsp:param name="categoryName" value="${teliumBreadCrumbString}" />
		<dsp:param name="pageName" value="main" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="modifiedTealiumBreadcrumb"
							param="modifiedCategory" />
			<dsp:getvalueof var="taxonomyString" param="breadCrumbLevels" />
		</dsp:oparam>
	</dsp:droplet>

<dsp:droplet name="/com/tru/commerce/droplet/TRUProductInfoLookupDroplet">
	<dsp:param name="productId" value="${element.productId}"/>
	<dsp:param name="siteId" bean="/atg/multisite/Site.id"/>
		<dsp:oparam name="output">
	        <dsp:getvalueof param="productInfo.defaultSKU.relatedProducts" var="relatedProducts"/>
	        <c:set var="orderXsellProduct" value="${orderXsellProduct}'${relatedProducts}'${separator}"/>
       </dsp:oparam>
</dsp:droplet>

<dsp:droplet name="/com/tru/commerce/locations/TRUGetCookieDroplet">
	<dsp:param name="cookieName" value="favStore" />
	<dsp:oparam name="output">
		<dsp:getvalueof var="cookieValue" param="cookieValue" />
		<dsp:getvalueof var="locationIdFromCookie" param="locationId" />
	</dsp:oparam>
	<dsp:oparam name="empty">
	</dsp:oparam>
</dsp:droplet>

<c:set var="session_id" value="${cookie.sessionID.value}"/>
<c:set var="visitor_id" value="${cookie.visitorID.value}"/>

<dsp:getvalueof var="search_keyword" param="searchKey"/>
<c:if test="${not empty search_keyword}">
	<c:set var="search_success" value="Search Redirect"/>
	<c:set var="search_synonym" value="Category:${search_keyword}"/>
	<script type="text/javascript">
		$(document).ready(function(){
		    setTimeout(function(){
		    	utag.view({
			    	event_type:'search_performed'
			    })
		    },1000);	
	    });
	</script>
</c:if>
<%-- <!-- start: Script for Tealium Integration --> --%>
  <script type='text/javascript'>
  		var isGridwallOnLoad = true;
  		var isMegaMenuClicked = localStorage.getItem('pageFrom');
  		var prodID=[];
  		if( typeof familyProdID != 'undefined' ){
  			prodID=familyProdID;
  		}
		var utag_data = {
			customer_status : "${customerStatus}",
			page_name : "${siteCode}: ${pageName}: ${NValue}: ${catName}",
			page_type : "${siteCode}: ${pageType}",
			page_category : "${catName}",
			site_section : "${catName}",
			browser_id : "${pageContext.session.id}",
		    product_compared :"",
		    device_type : "${deviceType}",
		    oas_breadcrumb : "${baseBreadcrumb}${modifiedTealiumBreadcrumb}/main",
		    oas_taxonomy : "${taxonomyString}",
			oas_sizes : ${oasSizes},
		    customer_city : "${customerCity}",
		    customer_country : "${customerCountry}",
		    customer_email : "${customerEmail}",
		    customer_id : "${customerId}",
		    customer_type : "${customerStatus}",
		    customer_state : "${customerState}",
		    customer_zip : "${customerZip}",
		    customer_dob : "${customerDob}",
		    internal_campaign_page : "${internalCampaignPage}",
		    orso_code : "${orsoCode}",
		    ispu_source : "${ispuSource}",
		    partner_name:"${partnerName}",
		    customer_name:"${customerName}",
		    product_id:prodID,
		    event_type:"gridwall_impression",
		    product_impression_id:[${tealiumProductImpressionId}],
		    product_gridwall_location:"",
		    selected_store:"${selectedStore}",
		    xsell_product:[${orderXsellProduct}],
		    store_locator:"${locationIdFromCookie}",
		    session_id : "${session_id}",
		    visitor_id : "${visitor_id}",
		    search_success :"${search_success}",
		    search_synonym:"${search_synonym}",
		    search_keyword :"${search_keyword}",
		    tru_or_bru : "${siteCode}"
		};

		if ( typeof isMegaMenuClicked === 'string' && (isMegaMenuClicked.indexOf('megaMenuClick') > -1) ) {
			utag_data.event_type = ["shop_by_mega_menu", "gridwall_impression"];
			localStorage.setItem("pageFrom",'');
		}
	
		(function(a,b,c,d){
			a='${tealiumURL}';
			b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
			a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
		})();
		</script>
	    <%-- End: Script for Tealium Integration --%>
	 	 
	</jsp:body>
		</tru:pageContainer>
<%-- 	</div>

</body>
	</html> --%>
</dsp:page>