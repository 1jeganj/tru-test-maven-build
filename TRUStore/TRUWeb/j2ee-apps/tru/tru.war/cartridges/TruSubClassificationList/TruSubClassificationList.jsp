<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" bean="/OriginatingRequest.contentItem"/>
<dsp:getvalueof var="contextPath"	value="${originatingRequest.contextPath}" />
	
	<c:if test="${not empty contentItem.refinementCrumbs}">
		<c:forEach var="refineCrumb" items="${contentItem.refinementCrumbs}">
			<c:if test="${refineCrumb.dimensionName eq 'product.classification'}">
				<dsp:getvalueof var="classificationBreadcrumb" value="${refineCrumb}"/>
			</c:if>
		</c:forEach>
	</c:if>
	
	<c:if test="${not empty classificationBreadcrumb}">
		<!-- Start Stroller div -->
		<c:if test="${not empty contentItem.breadCrumbs}">
			<div class="row-max-width">
		        <div class="row block-3-row row-strollers">
		        	<c:forEach var="levelTwoCategory" items="${contentItem.breadCrumbs.subCategories}">
		        		
			            <div class="col-md-4 block-3-column">
			                <div class="category-image-link-block">
			                    <a class="bnsImgBlock" href="${levelTwoCategory.categoryURL}"><img src="${levelTwoCategory.categoryImage}" alt="category block image">
			                    </a>
			                    <ul class="category-image-link-block-list">
			                        <li><a href="${levelTwoCategory.categoryURL}">${levelTwoCategory.categoryName}</a>
			                        </li>
			                        <c:forEach var="levelThreeCategory" items="${levelTwoCategory.subCategories}">
			                        	
			                        	<li><a href="${levelThreeCategory.categoryURL}">${levelThreeCategory.categoryName} &gt;</a>
			                        	</li>
			                        </c:forEach>
			                        
									<li class="category-image-link-list"><a class="category-more-cta" href="${levelTwoCategoryCache.url}">more</a>
									</li>
									
			                    </ul>
			                </div>
			            </div>
		            </c:forEach>
		        </div>
		    </div>
	    </c:if>
	    
	    </c:if>
	    
</dsp:page>