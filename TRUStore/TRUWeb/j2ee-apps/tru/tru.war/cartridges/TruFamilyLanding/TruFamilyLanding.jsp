<dsp:page>
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
	<dsp:importbean bean="/atg/commerce/catalog/CatalogNavHistoryCollector" />
    <dsp:importbean bean="/atg/commerce/catalog/CatalogNavHistory"/>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="siteCode" value="TRU" />
	<dsp:getvalueof var="baseBreadcrumb" value="tru" />
	<dsp:getvalueof var="previewEnabled" bean="/atg/endeca/assembler/cartridge/manager/AssemblerSettings.previewEnabled"/>
	<dsp:getvalueof var="loginStatus" bean="Profile.transient" />
	<dsp:getvalueof var="customerEmail" bean="Profile.login" />
	<dsp:getvalueof var="customerId" bean="Profile.id" />
	<dsp:getvalueof var="customerDob" bean="Profile.dateOfBirth" />
	<dsp:getvalueof var="pageName"
		bean="TRUTealiumConfiguration.familyPageName" />
	<dsp:getvalueof var="pageType"
		bean="TRUTealiumConfiguration.familyPageType" />
	<dsp:getvalueof var="browserId"
		bean="TRUTealiumConfiguration.browserId" />
	<dsp:getvalueof var="deviceType"
		bean="TRUTealiumConfiguration.deviceType" />
	<dsp:getvalueof var="oasBreadcrumb"
		bean="TRUTealiumConfiguration.familyOASBreadcrumb" />
	<dsp:getvalueof var="oasTaxonomy"
		bean="TRUTealiumConfiguration.familyOASTaxonamy" />
	<dsp:getvalueof var="oasSizes"
		bean="TRUTealiumConfiguration.familyOASSizes" />
	<dsp:getvalueof var="customerType"
		bean="TRUTealiumConfiguration.customerType" />
	<dsp:getvalueof var="tealiumURL"
		bean="TRUTealiumConfiguration.tealiumURL" />
	<dsp:getvalueof var="internalCampaignPage"
		bean="TRUTealiumConfiguration.internalCampaignPage" />
	<dsp:getvalueof var="orsoCode" bean="TRUTealiumConfiguration.orsoCode" />
	<dsp:getvalueof var="ispuSource"
		bean="TRUTealiumConfiguration.ispuSource" />
	<dsp:getvalueof var="babySiteCode"
		bean="TRUTealiumConfiguration.babySiteCode" />
	<dsp:getvalueof var="customerFirstName" bean="Profile.firstName"/>
	<dsp:getvalueof var="customerLastName" bean="Profile.lastName"/>
<c:set var="isSosVar" value="${cookie.isSOS.value}"/>

<c:choose>
<c:when test="${isSosVar eq 'true'}">
<c:set var="selectedStore" value="sos"/>
</c:when>
<c:otherwise>
<c:set var="selectedStore" value=""/>
</c:otherwise>
</c:choose>
	<c:set var="space" value=" "/>
	<c:choose>
	<c:when test="${empty customerFirstName && empty customerLastName}">
		<c:set var="customerName" value='${fn:substring(customerEmail, 0, fn:indexOf(customerEmail, "@"))}'/>
	</c:when>
	<c:when test="${not empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value="${customerFirstName}"/>
	</c:when>
	<c:when test="${empty customerFirstName && not empty customerLastName}">
		   <c:set var="customerName" value="${customerLastName}"/>
	</c:when>
	<c:otherwise>
			<c:set var="customerName" value="${customerFirstName}${space}${customerLastName}"/>
	</c:otherwise>
 </c:choose>
	<c:choose>
		<c:when test="${loginStatus eq 'true'}">
			<c:set var="customerStatus" value="Guest" />
		</c:when>
		<c:otherwise>
			<c:set var="customerStatus" value="Registered" />
			<dsp:getvalueof var="shippingAddressId"
				bean="Profile.shippingAddress.id"></dsp:getvalueof>
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" bean="Profile.shippingAddress.id" />
				<dsp:oparam name="false">
					<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="Profile.secondaryAddresses" />
						<dsp:param name="elementName" value="address" />
						<dsp:oparam name="output">
							<dsp:getvalueof param="address.id" var="addressId" />
							<c:if test="${addressId eq shippingAddressId}">
								<dsp:getvalueof param="address.city" var="customerCity" />
								<dsp:getvalueof param="address.state" var="customerState" />
								<dsp:getvalueof param="address.postalCode" var="customerZip" />
								<dsp:getvalueof param="address.country" var="customerCountry" />
							</c:if>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</c:otherwise>
	</c:choose>
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
	<c:if test="${site eq babySiteCode}">
		<dsp:getvalueof var="siteCode" value="BRU" />
		<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
		<dsp:getvalueof var="baseBreadcrumb" value="bru" />
	</c:if>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof var="content"
		vartype="com.endeca.infront.assembler.ContentItem"
		value="${originatingRequest.contentItem}" />
	<dsp:getvalueof var="tealiumURL"
		bean="TRUTealiumConfiguration.tealiumURL" />

<c:forEach var="element" items="${content.MainHeader}">
	<c:if test="${element['@type'] eq 'TruBreadcrumbs'}">
		<dsp:getvalueof var="refinementCrumbs"
			value="${element.refinementCrumbs[0]}" />
		<dsp:getvalueof var="catName"
			value="${refinementCrumbs.properties['displayName_en']}" />
		<dsp:getvalueof var="repositoryId"
			value="${refinementCrumbs.properties['category.repositoryId']}" />
		<dsp:getvalueof var="isCompareProduct"
			value="${refinementCrumbs.properties['dimval.prop.category.isCompareProduct']}" />
		<c:set var="catID" value="${repositoryId}" scope="request" />
	</c:if>
	<dsp:droplet
		name="/com/tru/common/droplet/TRUTealiumCategoryModifierDroplet">
		<dsp:param name="categoryName" value="${catName}" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="modifiedCategory" param="modifiedCategory" />
		</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="parentCatName"
		value="${refinementCrumbs.ancestors[0].label}" />
	<dsp:droplet
		name="/com/tru/common/droplet/TRUTealiumCategoryModifierDroplet">
		<dsp:param name="categoryName" value="${parentCatName}" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="modifiedParentCategory" param="modifiedCategory" />
		</dsp:oparam>
	</dsp:droplet>
</c:forEach>
	<dsp:droplet name="CatalogNavHistoryCollector">
        <dsp:param name="item" value="${repositoryId}" />
        <dsp:param name="navAction" value="push"/>
    </dsp:droplet>
	<%-- <input type="hidden" id="categoryId" value="${repositoryId}"/> --%>
	<tru:pageContainer>
		<jsp:attribute name="fromFamily">Family</jsp:attribute>
		<jsp:body>
      <input type="hidden" value="Family" name="currentpage" id="currentpage"/>
     <dsp:getvalueof var="N" param="N" />
     <c:set var="defaultval" scope="request" value="${N}" />
       <dsp:getvalueof var="telcatname" param="telcatname" />
       <dsp:getvalueof var="tealiumProductImpressionId" param="tealiumProductImpressionId" />
<c:set var="family" scope="request" value="family" />
<%-- 	<c:forEach var="element" items="${content.MainProduct}">
		<c:if test="${element['@type']  eq 'TRUResultsList'}">
			<dsp:getvalueof var="resultsListItem" value="${element}" />
		</c:if>
	</c:forEach> --%>
	
    	<c:forEach var="element" items="${content.MainProduct}">
			<c:forEach var="element1" items="${element.contents}">
		<c:forEach var="element2" items="${element1.MainProduct}">
			<c:if test="${element2['@type'] eq 'TRUResultsList'}">
				<dsp:getvalueof var="resultsListItem" value="${element2}" />
			</c:if>
		</c:forEach>
</c:forEach>
	</c:forEach>
	<c:forEach var="element" items="${content.MainHeader}">
		 <dsp:renderContentItem contentItem="${element}" />
	</c:forEach>
	<div class="row default-margin">
    	<div class="col-mid-12 col-no-padding category-name">
				<c:forEach var="element" items="${content.MainHeader}">

				<c:if test="${not empty element.refinementCrumbs[0] }">

				<dsp:getvalueof value="${fn:escapeXml(element.refinementCrumbs[0].label)}" var="familyName"/>
					<c:set var="familyName" value="${familyName}" scope="request"/>
			      <input type="hidden" value="${familyName}" name="familyname" id="familyname"/>
           
           </c:if>
	</c:forEach>
     	</div>
  	</div>
    	<div id="narrow-by-scroll" class="product-content">
    	 <%-- This div is getting closed in compareDrawer.jsp --%>
    	 <input type="hidden" id="hiddenCategoryId" value="${repositoryId}" />
    	 <div class="sub-category-template">
              	<%-- <div class="fixed-narrow-menu">  --%>
		 <c:forEach var="element" items="${content.MainProduct}">
		 <dsp:renderContentItem contentItem="${element}">
		 <dsp:param name="resultsListItem" value="${resultsListItem}" />
		 <dsp:param name="hpageType" value="family" />
                     <dsp:param name="categoryId"
									value="${repositoryId}" />
                     <dsp:param name="isCompareProduct"
									value="${isCompareProduct}" />
		 </dsp:renderContentItem>
		 </c:forEach>
		  </div>
		 </div>
		<c:forEach var="element" items="${content.MainFooter}">
			 <dsp:renderContentItem contentItem="${element}" />
		</c:forEach>
		<dsp:getvalueof var="teliumBreadCrumbString"
					param="teliumBreadCrumbString" />
			<dsp:droplet
					name="/com/tru/common/droplet/TRUTealiumCategoryModifierDroplet">
				<dsp:param name="categoryName" value="${teliumBreadCrumbString}" />
				<dsp:param name="pageName" value="family" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="modifiedTealiumBreadcrumb"
							param="modifiedCategory" />
					<dsp:getvalueof var="taxonomyString" param="breadCrumbLevels" />
				</dsp:oparam>
		</dsp:droplet>

		<c:set var="singleQuote" value="'" />
		<c:set var="doubleQuote" value='"' />
		<c:set var="slashSingleQuote" value="\'" />
		<c:set var="slashDoubleQuote" value='\"' />
		<c:if test="${fn:contains(catName,singleQuote)}">
	     <c:set var="catName" value="${fn:replace(catName,singleQuote,slashSingleQuote)}"/>
		 </c:if>
		 <c:if test="${fn:contains(catName,doubleQuote)}">
	     <c:set var="catName" value="${fn:replace(catName,doubleQuote,slashDoubleQuote)}"/>
		 </c:if>
		<dsp:droplet name="/com/tru/commerce/droplet/TRUProductInfoLookupDroplet">
				<dsp:param name="productId" value="${element.productId}"/>
				<dsp:param name="siteId" bean="/atg/multisite/Site.id"/>
					<dsp:oparam name="output">
				        <dsp:getvalueof param="productInfo.defaultSKU.relatedProducts" var="relatedProducts"/>
				        <c:set var="orderXsellProduct" value="${orderXsellProduct}'${relatedProducts}'${separator}"/>
			       </dsp:oparam>
		</dsp:droplet>

		<dsp:droplet name="/com/tru/commerce/locations/TRUGetCookieDroplet">
			<dsp:param name="cookieName" value="favStore" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="cookieValue" param="cookieValue" />
				<dsp:getvalueof var="locationIdFromCookie" param="locationId" />
			</dsp:oparam>
			<dsp:oparam name="empty">
			</dsp:oparam>
		</dsp:droplet>
		<c:set var="session_id" value="${cookie.sessionID.value}"/>
		<c:set var="visitor_id" value="${cookie.visitorID.value}"/>
		
	<dsp:getvalueof var="search_keyword" param="searchKey"/>
	<c:if test="${not empty search_keyword}">
		<c:set var="search_success" value="Search Redirect"/>
		<c:set var="search_synonym" value="Category:${search_keyword}"/>
		<script type="text/javascript">
			$(document).ready(function(){
			    setTimeout(function(){
			    	utag.view({
				    	event_type:'search_performed'
				    })
			    },1000);	
		    });
		</script>
	</c:if>
<%-- <!-- start: Script for Tealium Integration --> --%>
<script type='text/javascript'>
	var totalProd = '${resultsListItem.totalNumRecs}';
	var prodParentCat = [];
	for(var i=0;i<totalProd;i++){
		prodParentCat.push("${parentCatName}");
	}

	var isGridwallOnLoad = true;
	var isMegaMenuClicked = localStorage.getItem('pageFrom'); 
	var utag_data = {
	    customer_status:"${customerStatus}",
	    page_name:"${siteCode}: ${pageName}: ${catID}: ${catName}",
	    page_type:"${siteCode}: ${pageType}",
	    browser_id:"${pageContext.session.id}",
	    device_type:"${deviceType}",
	    oas_breadcrumb:"${baseBreadcrumb}${modifiedTealiumBreadcrumb}/family",
	    oas_taxonomy:"${taxonomyString}",
	    oas_sizes:${oasSizes},
	    customer_city : "${customerCity}",
	    customer_country : "${customerCountry}",
	    customer_email : "${customerEmail}",
	    customer_id : "${customerId}",
	    customer_type : "${customerStatus}",
	    customer_state : "${customerState}",
	    customer_zip : "${customerZip}",
	    customer_dob : "${customerDob}",
	    internal_campaign_page : "${internalCampaignPage}",
	    orso_code : "${ orsoCode}",
	    ispu_source : "${ispuSource}",
	    partner_name:"${partnerName}",
	    customer_name:"${customerName}",
	    selected_store:"${selectedStore}",
	    event_type:"gridwall_impression",
	    product_brand:"",
	    product_image_url:"",
	    product_list_price:"",
	    product_subcategory:"",
	    product_finding_method:"",
	    product_compared :"",
	    page_category:"${parentCatName}",
	    page_subcategory:"${telcatname}",
	    site_section:"${catName}",
	    product_id:familyProdID,
	    product_category:prodParentCat,
	    product_sku:familyProdSku,
	    product_store:["${siteCode}"],
	    product_impression_id:familyProdID,
	    product_gridwall_location:"",
	    product_name:familyProdDisplayName,
	    product_unit_price:familyProdSalePrice,
		page_family : "${catName}",
		xsell_product:[${orderXsellProduct}],
		store_locator:"${locationIdFromCookie}",
		session_id : "${session_id}",
	    visitor_id : "${visitor_id}",
	    search_success :"${search_success}",
	    search_synonym:"${search_synonym}",
	    search_keyword :"${search_keyword}",
	    tru_or_bru : "${siteCode}"
	};
		
	if ( typeof isMegaMenuClicked === 'string' && (isMegaMenuClicked.indexOf('megaMenuClick') > -1) ) {
		utag_data.event_type = ["shop_by_mega_menu", "gridwall_impression"];
	    localStorage.setItem("pageFrom",'');
	}

	(function(a,b,c,d){
		a='${tealiumURL}';
		b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
		a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
	})();
</script>
<%-- <!-- End: Script for Tealium Integration --> --%>

		</jsp:body>
	</tru:pageContainer>

	<!-- /End Home Main template -->
<!-- </body>
	</html> -->
</dsp:page>