<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations" />
<dsp:getvalueof var="gridView1divClassMapping" bean="EndecaConfigurations.gridView1divClassMapping" />
<dsp:getvalueof var="gridView2divClassMapping" bean="EndecaConfigurations.gridView2divClassMapping" />
<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
<dsp:getvalueof var="noOfRecords" value="0" />
<dsp:getvalueof var="skuIdArray" value="${contentItem.skuIdArray}"/>
<dsp:getvalueof var="SKU_PRICE_ARRAY" value="${content.SKU_PRICE_ARRAY}" />

<dsp:getvalueof var="enablePriceFromATG" bean="/atg/multisite/Site.enablePriceFromATG"/>

<c:forEach var="records" items="${content.records}">
<dsp:getvalueof var="noOfRecords" value="${noOfRecords+1}" />
</c:forEach>

<script>
	var jsSkuIdArray = '${skuIdArray}';
	if(typeof skuPriceArrayHomePage != 'undefined') {
		skuPriceArrayHomePage.push('${SKU_PRICE_ARRAY}');
	} else {
		var skuPriceArrayHomePage = [];
		skuPriceArrayHomePage.push('${SKU_PRICE_ARRAY}');
	}
</script>


	
<dsp:getvalueof var="recordNumber" value="0" />
<dsp:getvalueof var="lastGridView" value=""/>
<c:if test="${noOfRecords > 0}">
	<div class="main-toy-grid toygrid-textover">
		<div class="row row-no-padding">
		
				<c:forEach var="viewItems" items="${content.GridViewProductTypes}" varStatus="status">
				<c:if test="${recordNumber lt  noOfRecords}">
		        <dsp:getvalueof var="count" value="${status.index}"/>
				<c:set var="firstDivClass" value="${gridView1divClassMapping[viewItems]}" />
				<c:set var="secondDivClass" value="${gridView2divClassMapping[viewItems]}" />
				<c:choose>
					<c:when test="${viewItems eq 'grid-small'}">
					<c:if test="${lastGridView ne viewItems}">
						<div class="${firstDivClass}">
					</c:if>
						<div class="second-div">
							<div class="${secondDivClass}">
								<dsp:include page="/browse/frag/productGridViewDisplay.jsp">
								    <c:if test="${recordNumber lt noOfRecords}">
									<dsp:param name="record" value="${content.records[recordNumber]}" />
									<dsp:param name="viewItems" value="${viewItems}" />
										<dsp:param name="count" value="${count}" />
								</c:if>
								</dsp:include>
								<dsp:getvalueof var="recordNumber" value="${recordNumber+1}" />
							</div>
						</div>
						<c:if test="${lastGridView eq viewItems}">
							</div>
						</c:if>
					</c:when>
					<c:otherwise>
						<div class="${firstDivClass}">
							<div class="${secondDivClass}">
								<dsp:include page="/browse/frag/productGridViewDisplay.jsp">
							       <c:if test="${recordNumber lt noOfRecords}">	
									<dsp:param name="record" value="${content.records[recordNumber]}" />
									<dsp:param name="viewItems" value="${viewItems}" />
										<dsp:param name="count" value="${count}" />
								</c:if>
								</dsp:include>
							</div>
						</div>
						<dsp:getvalueof var="recordNumber" value="${recordNumber+1}" />
					</c:otherwise>
				</c:choose>
				<dsp:getvalueof var="lastGridView" value="${viewItems}"/>
				</c:if>
			</c:forEach>
		</div>
	</div>
	
</c:if>
</dsp:page>