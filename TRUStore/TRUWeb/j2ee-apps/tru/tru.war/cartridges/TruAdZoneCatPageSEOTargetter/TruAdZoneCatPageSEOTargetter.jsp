<dsp:page>
<dsp:importbean bean="/atg/targeting/TargetingFirst" />
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/>
<dsp:getvalueof var="componentPath"	value="${content.componentPath}" />
<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters${componentPath}"/>
<div class="default-margin">
<div class="row category-ad-row">
          <%-- 	Rendering Content From ATG Targeter --%>
			<c:if test="${not empty componentPath}">
			<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters${componentPath}"/>
				<dsp:droplet name="/atg/targeting/TargetingFirst">
					<dsp:param name="targeter" bean="${atgTargeterpath}"/>
					<dsp:oparam name="output">
					<dsp:valueof param="element.data" valueishtml="true"/>
				</dsp:oparam>
				</dsp:droplet>
			</c:if>            		
            <div class="col-md-9">
			<body>
				<div class="ad-zone-728-90">
		  			<div id="OAS_Bottom1"></div>              
		 		</div>
			</body>			
            </div>
        </div>
        <!--<hr class="bottom-hr">-->
</div>
</dsp:page>