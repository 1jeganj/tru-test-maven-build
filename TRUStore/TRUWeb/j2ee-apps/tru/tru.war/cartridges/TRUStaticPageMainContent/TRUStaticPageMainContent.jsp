<dsp:page>
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" /> 
<dsp:importbean bean="/com/tru/droplet/TRUContentLookupDroplet" />
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL" />
<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />	
<dsp:getvalueof var="custized" param="custized"/>
	<c:if test="${contentItem['@type'] eq 'TRUStaticPageMainContent'}">	
		<c:choose>
			<c:when test="${not empty custized}">
				<dsp:droplet name="TRUContentLookupDroplet">
					<dsp:param name="articleContentKey" value="${contentItem['contentKey']}" />
					<dsp:oparam name="output">				
						<dsp:getvalueof var="articleBody" param="articleBody" />
						${articleBody}	
					</dsp:oparam>
				</dsp:droplet>
			</c:when>
			<c:otherwise>
				<dsp:getvalueof var="articleBody" value="${requestScope.articleBody}"/>
				<div class="default-margin">
					<div class="articleBody-data">
						<span>${articleBody}</span>
					</div>
				</div>
			</c:otherwise>
		</c:choose>		
	</c:if>
	<jsp:body>
		<input type="hidden" value="Help-Desk" name="currentpage" id="currentpage"/>
	</jsp:body>
</dsp:page>
