<dsp:page>
	<dsp:importbean bean="/com/tru/commerce/droplet/TRUConvertContentItemToJSON" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/>
	<script type="text/javascript">
		var searchResponseCrumbfacet= 
		<dsp:droplet name="TRUConvertContentItemToJSON">
			<dsp:param name="contentItems" bean="/OriginatingRequest.contentItem" />
		</dsp:droplet>;
	</script>
</dsp:page>