<dsp:page>
    <dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
    <dsp:importbean bean="/com/tru/common/TRUComponentExists" />
	<dsp:getvalueof var="atgTargeterpathCheck" bean="TRUStoreConfiguration.targeterpathCheck" />
	<dsp:importbean bean="/atg/targeting/TargetingFirst" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/>
	<dsp:getvalueof var="classificationHeroBanner"	value="${content.ClassificationHeroBanner}" />	
	<dsp:getvalueof var="componentPath"	value="${content.componentPath}" />
	<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale" />
	<dsp:getvalueof var="displayTargeter"	value="${content.displayTargeter}" />
	<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters${componentPath}"/>
	
	<dsp:droplet name="TRUComponentExists">
	<dsp:param name="path" value="${atgTargeterpath}"/><dsp:oparam name="true"><dsp:getvalueof var="atgTargeterpathCheck" value="true"/></dsp:oparam>
    <dsp:oparam name="false"></dsp:oparam>
	</dsp:droplet>
		
	<div class="row row-no-padding">
		<div class="col-md-12 col-no-padding category-hero-zone-container row-category-header">
			<c:choose>
				<c:when test="${not empty classificationHeroBanner}">
					<dsp:valueof value="${classificationHeroBanner}" valueishtml="true"/>
				</c:when>
				<c:when test="${displayTargeter and not empty componentPath && atgTargeterpathCheck eq 'true'}">
					
					<%-- <dsp:getvalueof var="categId" value="${content.CurrentCategoryId}" /> --%>
						<dsp:getvalueof var="cacheKey" 	value="${atgTargeterpath}${siteId}${locale}" />
						<%-- <c:if test="${not empty categId}">
							<dsp:getvalueof var="cacheKey" value="${cacheKey}${categId}" />
						</c:if> --%>
					<dsp:droplet
						name="/com/tru/cache/TRUCategoryHeroBannerTargetterCacheDroplet">
						<dsp:param name="key" value="${cacheKey}" />
						<dsp:oparam name="output">
							<dsp:droplet name="/atg/targeting/TargetingFirst">
								<dsp:param name="targeter" bean="${atgTargeterpath}" />
								<dsp:oparam name="output">
									<dsp:valueof param="element.data" valueishtml="true" />
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>
				</c:when>
				<c:otherwise>
				</c:otherwise>
			</c:choose>
		</div>
		
	</div>
	<script>
		setH1HeaderPosition();
	</script>
</dsp:page>