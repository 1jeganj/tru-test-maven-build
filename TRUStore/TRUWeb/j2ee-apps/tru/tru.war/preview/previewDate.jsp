<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof value="${originatingRequest.contextPath}"
		var="contextPath" />
	<dsp:importbean bean="/com/tru/preview/TRUPreviewFormHandler" />
	<dsp:importbean bean="/com/tru/common/TRUConfiguration" var="TRUConfiguration"/>
	<dsp:importbean bean="/atg/dynamo/service/CurrentDate" />
	<dsp:importbean bean="/com/tru/preview/SessionCurrentDate" />
	<dsp:importbean bean="/atg/commerce/catalog/RunServiceFormHandler" />
	<dsp:importbean bean="/com/tru/cms/CheckCMSStatusDroplet" />
<dsp:getvalueof var="previewPromoEnableInStaging" bean="TRUConfiguration.previewPromoEnableInStaging"/>
	<fmt:setBundle basename="atg.web.WebAppResources" />

	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<c:choose>
	<c:when test="${previewPromoEnableInStaging}"> 
	  <dsp:getvalueof bean="TRUPreviewFormHandler.systemDate" var="systemDate" />
		<tru:pageContainer>
			<jsp:body>
				<div class="container-fluid my-account-sign-in-template preview-section">
				<div class="template-contentnew">
					<c:set var="unescapedURL" value="${param.url}" />
					<div class="left-text ">Global Date:- </div> <div class="right-text"><dsp:valueof value="${systemDate}"/></div><div class="clear-fix"></div>
						<div class="left-text">Session Date:- </div> <div class="right-text"><dsp:valueof bean="SessionCurrentDate.timeAsTimestamp" /></div><div class="clear-fix"></div>
				
				<dsp:form>
					<div class="left-text">Choose Date:- </div>
					<div class="right-text"><dsp:input name="previewDate" id="previewDate" type="text" bean="TRUPreviewFormHandler.previewDate" /></div>
					<dsp:input type="submit" bean="TRUPreviewFormHandler.updatePreviewDate" class="submit-button-new"/>
					<dsp:input type="hidden" bean="TRUPreviewFormHandler.previewDateSuccessURL" value="${contextPath}?previewdate=" />
					<dsp:input type="hidden" bean="TRUPreviewFormHandler.previewDateErrorURL" value="${contextPath}?previewdate=" />
				</dsp:form>
				</div>
				</div>
				<dsp:droplet name="CheckCMSStatusDroplet">
					<dsp:oparam name="output">
						 <dsp:getvalueof param="isLocked" var="isLocked"/>
					</dsp:oparam>
				</dsp:droplet>
				<c:choose>
					<c:when test="${isLocked}"> 
						<div class="container-fluid my-account-sign-in-template preview-section">
						<div class="template-contentnew">
							<p class="cmsStatusInfo" > Catalog Maintenance service is in progress... Please Refresh the page After Some time</p>
						</div>
						</div>	
					</c:when>
					<c:otherwise>
					<div class="container-fluid my-account-sign-in-template preview-section">
								<div class="template-contentnew">
									<input type="hidden" class="contextPath" value="${contextPath}"/>
									<p>Invoke Catalog Maintenance:<input type="submit" id="cmsFormSubmit" class="submit-button-new"/>
									<p class="cmsStatusInfo" style="display: none;"> Catalog Maintenance service is in progress...</p>
								</div>
								</div>
					</c:otherwise>
				</c:choose>
			</jsp:body>
		</tru:pageContainer>
	</c:when>
	<c:otherwise>
			Please access staging layer to validate Preview on promotion
	</c:otherwise>
</c:choose>
</dsp:page>