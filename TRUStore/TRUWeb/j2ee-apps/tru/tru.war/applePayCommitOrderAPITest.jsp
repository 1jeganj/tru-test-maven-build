<!DOCTYPE html>
<html lang="en">
	<head>
		<title>ApplePay Commit Order API Test</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
<dsp:page>
	<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
	<dsp:getvalueof bean="TRUConfiguration.applePayTestURL" var="applePayTestURL" />
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>	<script type="text/javascript">
		function myfunc () {
			$("form").submit(function(e){
		        e.preventDefault();
		    });
			var email=$('#email').val();
			var pageName=$('#pageName').val();
			var version=$('#version').val();
			var data=$('#data').val();
			var signature=$('#signature').val();
			var publicKey=$('#publicKey').val();
			var transactionId=$('#transactionId').val();
			var publicKeyHash=$('#publicKeyHash').val();
			var deviceId=$('#deviceId').val();
			var firstName=$('#firstName').val();
			var lastName=$('#lastName').val();
			var address1=$('#address1').val();
			var city=$('#city').val();
			var state=$('#state').val();
			var postalCode=$('#postalCode').val();
			var phoneNumber=$('#phoneNumber').val();
			var country=$('#country').val();
			var shipCity=$('#shipCity').val();
			var shipFirstName=$('#shipFirstName').val();
			var shipLastName=$('#shipLastName').val();
			var shipAddress1=$('#shipAddress1').val();
			var shipCity=$('#shipCity').val();
			var shipState=$('#shipState').val();
			var shipPostalCode=$('#shipPostalCode').val();
			var restService=true;
			$.ajaxSetup({
				headers:{
			    	'X-APP-API_KEY':'apiKey',
			    	'X-APP-API_CHANNEL':'mobile'
			      }});
			$.ajax({
			                url :'${applePayTestURL}',
			                type: 'POST',
			                data: {email,restService,pageName,version,data,signature,publicKey,transactionId,publicKeyHash,deviceId,firstName,lastName,address1,city,state,postalCode,phoneNumber,country,shipCity,shipFirstName,shipLastName,shipAddress1,shipCity,shipPostalCode,shipState},
			                xhrFields: {
			                      'withCredentials': true
			                },
			                dataType: "json",
							success : function (data) {
								//console.log(data.products);
								//$( 'div' ).html(JSON.stringify(data));
								var jsonString =JSON.stringify(data);
								$('#textArea').val(jsonString);
			                },
			                error : function (data) {
								console.log(data);
								$( 'div' ).html( "<span class='red'>Hello <b>Failure</b></span>" );

							}
			});
		}
	</script>
	<body>
	<form action="#" id="formval" method="post">
	Email:<br>
	<input type="text" name="email" id="email"><br>
	PageName:<br>
	<input type="text" name="pageName" id="pageName"><br>
	Version:<br>
	<input type="text" name="version" id="version"><br>
	Data:<br>
	<input type="text" name="data" id="data"><br>
	Signature:<br>
	<input type="text" name="signature" id="signature"><br>
	PublicKey:<br>
	<input type="text" name="publicKey" id="publicKey"><br>
	TransactionId:<br>
	<input type="text" name="transactionId" id="transactionId"><br>
	PublicKeyHash:<br>
	<input type="text" name="publicKeyHash" id="publicKeyHash"><br>
	DeviceId:<br>
	<input type="text" name="deviceId" id="deviceId"><br>
	FirstName:<br>
	<input type="text" name="firstName" id="firstName"><br>
	LastName:<br>
	<input type="text" name="lastName" id="lastName"><br>
	Address1:<br>
	<input type="text" name="address1" id="address1"><br>
	City:<br>
	<input type="text" name="city" id="city"><br>
	State:<br>
	<input type="text" name="state" id="state"><br>
	PostalCode:<br>
	<input type="text" name="postalCode" id="postalCode"><br>
	PhoneNumber:<br>
	<input type="text" name="phoneNumber" id="phoneNumber"><br>
	ShipFirstName:<br>
	<input type="text" name="shipFirstName" id="shipFirstName"><br>
	ShipLastName:<br>
	<input type="text" name="shipLastName" id="shipLastName"><br>
	ShipAddress1:<br>
	<input type="text" name="shipAddress1" id="shipAddress1"><br>
	ShipCity:<br>
	<input type="text" name="shipCity" id="shipCity"><br>
	ShipState:<br>
	<input type="text" name="shipState" id="shipState"><br>
	ShipPostalCode:<br>
	<input type="text" name="shipPostalCode" id="shipPostalCode"><br>
	Country:<br>
	<input type="text" name="country" id="country"><br>
	<input type="submit" value="Call API" onclick="myfunc()" />
	</form>	
	<div>
	<textarea rows="5" cols="200" id="textArea"></textarea>
	</div>
	</body>
	</dsp:page>
	
</html>
