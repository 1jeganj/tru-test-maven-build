<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/endeca/assembler/droplet/InvokeAssembler" />
	<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
	
	<dsp:getvalueof var="fromHome" param="fromHome" />
	<dsp:getvalueof var="fromSubCat" param="fromSubCat" />
	<dsp:getvalueof var="fromFamily" param="fromFamily" />
	<dsp:getvalueof var="fromCat" param="fromCat" />
	<dsp:getvalueof var="fromPdp" param="fromPdp" />
	<dsp:getvalueof var="fromShopBy" param="fromShopBy" />
	<dsp:getvalueof var="staticPageTitleText" param="staticPageTitleText" />
	<dsp:getvalueof param="shopbylanding" var="shopbylanding" />
	<dsp:getvalueof var="queryString" bean="/OriginatingRequest.queryString" />
	<dsp:getvalueof var="onlineProductId" param="onlineProductId"/>
	<dsp:getvalueof var="requesturi" value="${originatingRequest.requesturi}" />
	<dsp:getvalueof var="serverName" value="${originatingRequest.serverName}"/>
	<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
		<dsp:oparam name="output">
			<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="requestUrl" value="${scheme}${serverName}${requesturi}"/>
	<c:set var="pageNameContext" value="${originatingRequest.RequestURI}" scope="request"/>
	<c:set var="includePathValue" value="" scope="request" />
	<c:set var="title" value="" />
	<c:set var="catH1Text" value="" />
	<c:set var="catDescription" value="" />
	<c:set var="id" value="" />
	<c:set var="categoryName" value="" />
	<c:set var="keyWords" value="" />
	<c:set var="pdpH1" value="" />
	<c:set var="catH1" value="" />
	<c:set var="canonicalUrl" value="" />
	<c:set var="homeMetaDescription" value="" />
	<c:set var="queryParamIdName" value="" />
	<c:set var="homeSeoTitleOverride" value="" />
	<c:if test="${not empty shopbylanding and shopbylanding eq 'shopby'}">
		<c:set var="includePathValue" value="shopby" scope="request" />
	</c:if>
	<c:if test="${not empty fromCat and fromCat eq 'cat'}">
		<c:set var="includePathValue" value="cat" scope="request" />
	</c:if>
	<c:if test="${not empty fromSubCat and fromSubCat eq 'subcat'}">
		<c:set var="includePathValue" value="subcat" scope="request" />
	</c:if>
	<c:if test="${not empty fromFamily and fromFamily eq 'Family'}">
		<c:set var="includePathValue" value="family" scope="request"/>
	</c:if>
	<c:if test="${not empty fromPdp and fromPdp eq 'pdp'}">
		<c:set var="includePathValue" value="pdp" />
	</c:if>
	<c:if test="${not empty fromHome and fromHome eq 'home'}">
		<c:set var="includePathValue" value="home" />
	</c:if>
	
	<c:if
		test="${pageNameContext eq '/category' or pageNameContext eq '/family'  or pageNameContext eq '/subcat'}">
		<dsp:getvalueof var="queryParamIdName" param="categoryid" />
	</c:if>
	
	<c:if
		test="${pageNameContext eq '/search' or pageNameContext  eq '/nosearchresults'}">
		<dsp:getvalueof var="queryParamIdName" param="keyword" />
	</c:if>
	
	<c:if
		test="${pageNameContext eq '/product'}">
		<dsp:getvalueof var="queryParamIdName" param="onlineProductId" />
	 </c:if>
	 
	  <c:if
		test="${pageNameContext eq '/collection'}">
		<dsp:getvalueof var="queryParamIdName" param="collectionId" />
	</c:if>
	
	<c:if test="${pageNameContext eq '/shopby'}">
		<dsp:getvalueof var="queryParamIdName" param="N" />
	</c:if>


	<c:if test="${not empty fromHome and fromHome eq 'home'}">
		<dsp:getvalueof var="catalogId"
			bean="/atg/multisite/Site.defaultCatalog" />

		<dsp:droplet name="/com/tru/droplet/TRUSEODroplet">
			<dsp:param name="page" value="${fromHome}" />
			<dsp:param name="id" value="${catalogId}" />
			<dsp:param name="siteId" bean="/atg/multisite/Site.id" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="seoDescription" param="homePageMetaDescription" />
				<dsp:getvalueof var="seoMetaKeyword" param="keywords" />
				<dsp:getvalueof var="homeSeoTitle" param="homeSeoTitle" />
				<dsp:getvalueof var="homeSeoH1" param="homeSeoH1" />
				<c:if test="${not empty seoMetaKeyword}">
					<c:set var="keyWords" value="${seoMetaKeyword}" />
				</c:if>
				<c:if test="${not empty seoDescription}">
					<c:set var="homeMetaDescription" value="${seoDescription}" />
				</c:if>
				<c:if test="${not empty homeSeoTitle}">
					<c:set var="homeSeoTitleOverride" value="${homeSeoTitle}" />
				</c:if>
				<c:if test="${not empty homeSeoH1}">
					<c:set var="seoH1" value="${homeSeoH1}" scope="request" />
				</c:if>
			</dsp:oparam>
			<dsp:oparam name="empty">
			</dsp:oparam>
		</dsp:droplet>
	</c:if>

	<c:if
		test="${not empty includePathValue and includePathValue eq 'shopby'}">
		<c:forEach var="element" items="${contentItem.MainProduct}">
			<c:forEach var="element1" items="${element.contents}">
				<c:forEach var="element2" items="${element1.MainProduct}">
					<c:if test="${element2['@type'] eq 'TRUResultsList'}">
						<dsp:getvalueof var="brandName"
							value="${element2.records[0].records[0].attributes['Brand']}" />
						<c:set var="title" value="${brandName}" />
						<c:set var="catH1Text" value="${brandName}" />
						<c:set var="catDescription" value="${brandName}" />
					</c:if>
				</c:forEach>
			</c:forEach>
		</c:forEach>
	</c:if>
	<c:if
		test="${not empty includePathValue and includePathValue eq 'cat'}">
		<c:forEach var="element" items="${contentItem.MainContent}">
			<c:if
				test="${element['@type'] eq 'TruCatPageClassificationHierarchy'}">
				<dsp:getvalueof var="categoryName"
					value="${element.CurrentCategoryName}" />
				<c:set var="title" value="${element.CurrentCategoryName}" />
				<c:set var="id" value="${element.CurrentCategoryId}" />
				<c:set var="catH1Text" value="${element.CurrentCategoryName}" />
				<c:set var="catDescription" value="${element.CurrentCategoryName}" />
			</c:if>
		</c:forEach>
	</c:if>
	<c:if
		test="${not empty includePathValue and includePathValue eq 'subcat'}">
		<c:forEach var="element" items="${contentItem.MainHeader}">
			<c:if
				test="${element['@type'] eq 'TruSubCatPageClassificationHierarchy'}">
				<dsp:getvalueof var="categoryName"
					value="${element.CurrentCategoryName}" />
				<c:set var="title" value="${element.CurrentCategoryName}" />
				<c:set var="id" value="${element.CurrentCategoryId}" />
				<c:set var="catH1Text" value="${element.CurrentCategoryName}" />
				<c:set var="catDescription" value="${element.CurrentCategoryName}" />
			</c:if>
		</c:forEach>
	</c:if>
	<c:if test="${not empty fromFamily and fromFamily eq 'Family'}">
		<c:set var="familyName" value="" />
		
		<c:forEach var="element" items="${contentItem.MainHeader}">
		<dsp:getvalueof var="refinementCrumbs" value="${element.refinementCrumbs[0]}" />
		<dsp:getvalueof var="familyCrossRefName" value="${refinementCrumbs.properties['dimval.prop.category.crossRefDisplayName']}" />
		
		<c:choose>
    		<c:when test="${not empty familyCrossRefName and fn:contains(familyCrossRefName, '||')}">
    			<c:set var="crossReferenceDisplayItem" value="${fn:split(familyCrossRefName, '||')}" />
					<c:if test="${fn:length(crossReferenceDisplayItem) gt 1}">
						<c:set var="ancestorLabel" value="${crossReferenceDisplayItem[1]}"/>
					</c:if>
					<dsp:getvalueof var="familyName" value="${ancestorLabel}"/>
			</c:when>
			<c:otherwise>
					<c:if test="${not empty refinementCrumbs }">
						<dsp:getvalueof  value="${refinementCrumbs.label}" var="familyName" />
				  </c:if>
			</c:otherwise>
		</c:choose>
		<c:set var="catH1Text" value="${familyName}" />
		
	<%-- 			<c:if test="${not empty element.refinementCrumbs[0]}">
				<c:set var="catH1Text" value="${element.refinementCrumbs[0].label}" />

			</c:if> --%>
				<c:set var="title" value="${familyName}" />
				<dsp:getvalueof var="familyId"
					value="${refinementCrumbs.properties['category.repositoryId']}" />
				<c:set var="id"
					value="${refinementCrumbs.properties['category.repositoryId']}" />

				<c:set var="catDescription" value="${refinementCrumbs.label}" />
			
		</c:forEach>
	</c:if>
	<c:if
		test="${not empty includePathValue and includePathValue eq 'cat' or includePathValue eq 'subcat' or includePathValue eq 'family'}">
		<dsp:droplet name="/com/tru/droplet/TRUSEODroplet">
			<dsp:param name="page" value="${includePathValue}" />
			<dsp:param name="id" value="${id}" />
			<dsp:param name="siteId" bean="/atg/multisite/Site.id" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="catSeoTitle" param="catSeoTitle" />
				<dsp:getvalueof var="catSeoH1Text" param="catSeoH1Text" />
				<dsp:getvalueof var="catSeoDescription" param="catSeoDescription" />
				<dsp:getvalueof var="canonicalUrl" param="canonicalUrl" />

				<dsp:getvalueof var="seoMetaKeyword" param="keywords" />
				<c:if test="${not empty seoMetaKeyword}">
					<c:set var="keyWords" value="${seoMetaKeyword}" />
				</c:if>
			</dsp:oparam>
			<dsp:oparam name="empty">
			</dsp:oparam>
		</dsp:droplet>
	</c:if>

	<c:set var="skuID" value="" />
	<c:set var="description" value="" />
	<c:set var="productDescription" value="" />

	<c:if
		test="${not empty includePathValue and includePathValue eq 'pdp'}">
		<!-- seo override for SKU -->
		<dsp:droplet name="/com/tru/droplet/TRUSEODroplet">
			<dsp:param name="page" value="${includePathValue}" />
			<dsp:param name="id" value="${onlineProductId}" />
			<dsp:param name="siteId" bean="/atg/multisite/Site.id" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="pdpTitleOverride" param="productSeoTitle" />
				<dsp:getvalueof var="pdpH1TextOverride" param="productSeoH1Text" />
				<dsp:getvalueof var="pdpDescriptionOverride"
					param="productSeoDescription" />
				<dsp:getvalueof var="seoMetaKeyword" param="keywords" />
				<dsp:getvalueof var="canonicalUrl" param="canonicalUrl" />
				<dsp:getvalueof var="ageRange" param="ageRange" />
				<dsp:getvalueof var="productSeoDescriptionDefault"
					param="productSeoDescriptionDefault" />
				<dsp:getvalueof var="title" param="productSeoTitleDefault" />

				<c:if test="${not empty seoMetaKeyword}">
					<c:set var="keyWords" value="${seoMetaKeyword}" />
				</c:if>

				<c:choose>
					<c:when test="${not empty ageRange}">
						<c:set var="description"
							value="Age:${ageRange}. ${productSeoDescriptionDefault}" />
					</c:when>
					<c:otherwise>
						<c:set var="description" value="${productSeoDescriptionDefault}" />
					</c:otherwise>
				</c:choose>
			</dsp:oparam>
			<dsp:oparam name="empty">
			</dsp:oparam>
		</dsp:droplet>
		<c:set var="pdpH1" value="${pdpH1TextOverride}" scope="request" />
	</c:if>

	<c:choose>
		<c:when
			test="${not empty includePathValue and includePathValue eq 'cat' or includePathValue eq 'subcat' or includePathValue eq 'family' or includePathValue eq 'shopby' or includePathValue eq 'pdp'}">
			<c:choose>
				<c:when test="${not empty catSeoTitle}">
					<title>${catSeoTitle}</title>
				</c:when>
				<c:when
					test="${not empty pdpTitleOverride and includePathValue eq 'pdp'}">
					<title>${pdpTitleOverride}</title>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${not empty title and fn:length(title) gt 43}">
							<c:set var="displayTilte" value="${fn:substring(title, 0, 42)}" />
						</c:when>
						<c:otherwise>
							<c:set var="displayTilte" value="${title}" />
						</c:otherwise>
					</c:choose>
					<c:if test="${not empty displayTilte}">
						<c:choose>
							<c:when test="${not empty site and site eq 'BabyRUs'}">
								<title>${displayTilte}- BabiesRUs</title>
							</c:when>
							<c:otherwise>
								<title>${displayTilte}- ${site}</title>
							</c:otherwise>
						</c:choose>
					</c:if>
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${not empty staticPageTitleText}">
					<title>${staticPageTitleText}</title>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${not empty homeSeoTitleOverride}">
							<title>${homeSeoTitleOverride}</title>
						</c:when>
						<c:otherwise>
							<title>Toysrus.com, The Official Toys&#34;R&#34;Us Site -
								Toys,Games, &#38; More</title>
						</c:otherwise>
					</c:choose>
				</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>
	
	<c:if
		test="${not empty includePathValue and includePathValue eq 'cat' or includePathValue eq 'subcat' or includePathValue eq 'family' or includePathValue eq 'shopby'}">
		<c:choose>
			<c:when test="${not empty catSeoH1Text}">
				<c:set var="catH1" value="${catSeoH1Text}" scope="request" />
			</c:when>
			<c:otherwise>
				<c:if test="${not empty catH1Text}">
					<c:set var="catH1" value="${catH1Text}" scope="request" />
				</c:if>
			</c:otherwise>
		</c:choose>

		<c:choose>
			<c:when test="${not empty catSeoDescription}">
				<dsp:getvalueof value="${fn:escapeXml(catSeoDescription)}"
					var="catSeoDescription" />
				<meta name="description" content="${catSeoDescription}" />
			</c:when>
			<c:otherwise>
				<c:if
					test="${not empty catDescription and not empty site and site eq 'ToysRUs'}">

					<dsp:getvalueof value="${fn:escapeXml(catDescription)}"
						var="catDescription" />
					<meta name="description"
						content="Buy ${catDescription} products at ToysRUs.com. The leading toy store for toys, games, educational toys and more." />
				</c:if>
				<c:if
					test="${not empty catDescription and not empty site and site eq 'BabyRUs'}">
					<dsp:getvalueof value="${fn:escapeXml(catDescription)}"
						var="catDescription" />
					<meta name="description"
						content="Buy ${catDescription} products at BabiesRUS.com. The leading baby store for nursery furniture, baby products and more." />
				</c:if>
			</c:otherwise>
		</c:choose>
	</c:if>

	<c:choose>
		<c:when test="${not empty canonicalUrl}">
				<c:set var="canonicalURL" value="${canonicalUrl}" scope="request" />
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when
					test="${pageNameContext eq '/category' or pageNameContext eq '/family'  or pageNameContext eq '/subcat'}">
					<c:set var="canonicalURL" value="${requestUrl}?categoryid=${queryParamIdName}"
						scope="request" />
				</c:when>
				<c:when	test="${pageNameContext eq '/search' or pageNameContext  eq '/nosearchresults'}">
					<c:set var="canonicalURL" value="${requestUrl}?keyword=${queryParamIdName}" scope="request" />
				</c:when>
				<c:when	test="${pageNameContext eq '/product' or pageNameContext  eq '/collection' }">
					<c:set var="canonicalURL" value="${requestUrl}?productId=${queryParamIdName}" scope="request" />
				</c:when>
				<c:when	test="${pageNameContext eq '/shopby'}">	
			       	<c:set var="canonicalURL" value="${requestUrl}?N=${queryParamIdName}" scope="request" />
				</c:when>
		        <c:when test="${pageNameContext eq '/shopByBrand' or  pageNameContext eq '/shopByCharacter'}">
		        		<c:set var="canonicalURL" value="${requestUrl}/" scope="request" />
		        </c:when>
		        <c:otherwise>
		        		<c:set var="canonicalURL" value="${requestUrl}" scope="request" />
		        </c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>

	<c:if test="${not empty includePathValue and includePathValue eq 'pdp'}">
		<c:choose>
			<c:when test="${not empty pdpDescriptionOverride}">
				<c:set var= "pdpDescriptionOverride" value="${fn:escapeXml(pdpDescriptionOverride)}"/>
				<meta name="description" content="${pdpDescriptionOverride}" />
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${not empty description and fn:length(description) gt 155}">
						<c:set var="productDescription" value="${fn:substring(description, 0, 154)}" />
						<c:set var= "productDescription" value="${fn:escapeXml(productDescription)}" />
						<meta name="description" content="${productDescription}" />
					</c:when>
					<c:otherwise>
						<c:if test="${not empty description}">
							<c:set var= "description" value="${fn:escapeXml(description)}"/>
							<meta name="description" content="${description}" />
						</c:if>
					</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>
	</c:if>
	<c:if test="${not empty includePathValue and includePathValue eq 'home'}">
		<c:if test="${not empty homeMetaDescription}">
			<meta name="description" content="${homeMetaDescription}" />
		</c:if>
	</c:if>
	<c:if test="${not empty keyWords}">
			<meta name="keywords" content="${keyWords}">
	</c:if>
<%-- 	<c:if test="${not empty includePathValue and includePathValue eq 'cat' or includePathValue eq 'subcat' or includePathValue eq 'family' and not empty keyWords}">
		<meta name="keywords" content="${keyWords}">
	</c:if> --%>

</dsp:page>