<dsp:page>
	<dsp:importbean bean="/com/tru/common/TRUSOSValidationFormHandler" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/multisite/Site"/>
	<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id" />
	 <dsp:droplet name="Switch">
		<dsp:param bean="TRUSOSValidationFormHandler.formError" name="value" />
		<dsp:oparam name="true">
			<dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
				<dsp:param name="exceptions"
					bean="TRUSOSValidationFormHandler.formExceptions" />
				<dsp:oparam name="output">
					<dsp:valueof param="message"></dsp:valueof>
					<br>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet> 
	
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	
	<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
		<dsp:oparam name="output">
		<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="/com/tru/droplet/TRUGetSiteDroplet">
		<dsp:param name="siteId" value="${siteName}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="siteUrl" param="siteUrl"/> 
		</dsp:oparam>
	</dsp:droplet>
	
	<dsp:getvalueof var="fullyQualifiedcontextPathUrl" value="${scheme}${siteUrl}${originatingRequest.contextPath}"/>

	<html lang="en">
		<head>
			<link rel="stylesheet" href="${TRUCSSPath}css/toolkit.css">
			<link rel="stylesheet" href="${TRUCSSPath}css/tRus_custom.css">
			<link rel="stylesheet" href="${contextPath}css/fonts.css">
			<link rel="stylesheet" href="${contextPath}css/global-footer.css">
		</head>
		<body calss="sos-body">
		<script type="text/javascript" src="/javascript/sos.js"></script>				
				
		<!-- new login page implementation start -->
		
			<!-- Header start -->
			<div class="container-fluid sos-login default-template">
				<div class="sos-nav">
					<nav class="navbar top-navbar">
				    	<div class="container-fluid">
				        	<ul class="nav navbar-nav">
				            	<li class="inline ">
				                	<img src="${TRUImagePath}images/tru-logo-sm.png" alt="toysrus logo">
				                </li>
				                <li class="inline">
				                	<img src="${TRUImagePath}images/babiesrus-logo-small.png" alt="babiesrus logo">
				                </li>
				            </ul>			                
				        </div>
				    </nav>	
			    </div>		        
			</div>	
			<!-- Header end -->		
			
			
			<!-- form start -->
			<%-- <dsp:form name="returningCustomer" action="#" method="post" onsubmit="valLogin()" class="sosForm">  --%>
			<dsp:form name="returningCustomer" action="#" method="post"> 
			<!-- <input type="hidden" name="action" value="aosLogin"> -->
			<div class="sos-form-container">
				<form action="" method="">
					<div class="sos-form-header">
						<div>
							<header>In-Store Kiosk - sign in</header>
						</div>				
					</div>
					<div class="sos-form">
						<div>
							<label>Employee Name</label>
		                   <!--  <input type="text" name="email"> -->
		                    <dsp:input type="text" name="associate_name"  bean="TRUSOSValidationFormHandler.employeeName" />
							<label>Employee Number</label>
		                   <!--  <input type="text" name="email">  --> 
		                    <dsp:input type="text" name="associate_number"  bean="TRUSOSValidationFormHandler.employeeNumber" />                  
							<label>Store Number</label>
		                    <!-- <input type="text" name="email"> -->
		                    <dsp:input type="text" name="store_number"  bean="TRUSOSValidationFormHandler.storeNumber" />
		                    
		                    <dsp:input type="hidden" value="${fullyQualifiedcontextPathUrl}index.jsp" bean="TRUSOSValidationFormHandler.successURL" />
		                    
		                    <dsp:input type="hidden" value="${fullyQualifiedcontextPathUrl}sos/index.jsp" bean="TRUSOSValidationFormHandler.errorURL" />
		                </div>
						<div class="button-container">
							<dsp:input type="hidden"  bean="TRUSOSValidationFormHandler.validateSosLogin" value="true" />
							 <a href="javascript: valLogin();">sign in</a>         
		                </div>
					</div>
				</form>
			</div>
			</dsp:form>
			<!-- form end -->			
		<!-- new login page implementation end -->
		
		</body>
	</html>
</dsp:page>