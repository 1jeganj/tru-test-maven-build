<dsp:page>
	<dsp:getvalueof var="enableSOS" bean="/com/tru/common/TRUConfiguration.enableSOS" />
	<dsp:importbean bean="/com/tru/common/droplet/TRUSOSHeaderInfoDroplet" />
	<dsp:importbean bean="/com/tru/common/droplet/TRUSOSGetIntegrationConfigDroplet" />
	<dsp:importbean bean="/com/tru/common/droplet/TRUGetIntegrationConfigDroplet" />
	<dsp:importbean bean="/com/tru/common/TRUSOSValidationFormHandler"/>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id" />
	<dsp:importbean bean="/com/tru/droplet/TRUGetSiteDroplet" />
	<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
	<dsp:getvalueof var="isRedirect" vartype="java.lang.boolean" value="${cookie.isRedirect.value}"></dsp:getvalueof>
	
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:droplet name="GetSiteTypeDroplet">
		<dsp:param name="siteId" value="${siteName}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="site" param="site"/>
			<c:set var="site" value ="${site}" scope="request"/>
		</dsp:oparam>
	</dsp:droplet>
	
	<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
		<dsp:oparam name="output">
		<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="/com/tru/droplet/TRUGetSiteDroplet">
		<dsp:param name="siteId" value="${siteName}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="siteUrl" param="siteUrl"/> 
		</dsp:oparam>
	</dsp:droplet>
	
	<dsp:getvalueof var="fullyQualifiedcontextPathUrl" value="${scheme}${siteUrl}${originatingRequest.contextPath}"/>
	
	<c:choose>
		<c:when test="${enableSOS eq true && (site eq 'sos')}">
			<dsp:droplet name="TRUSOSHeaderInfoDroplet">
				<dsp:oparam name="sos">
					<dsp:getvalueof var="isSOS" param="isSOS" scope="request"/>
					<input type="hidden" id="sosvalue" value="${isSOS}" >
					<dsp:getvalueof var="SosEmpName" param="SosEmpName" />
					<dsp:getvalueof var="SosEmpNum" param="SosEmpNum" />
					<dsp:getvalueof var="SosStoreNum" param="SosStoreNum" />

					<div class="inline sos-header-main">
						<ul class="list-inline sos-header-message">
							<li class="sos-header-first-li">
								<p><fmt:message key="sos.store.ordering" /> </p> 
								<p class="employeeName"><b><fmt:message key="sos.employee.name"/> ${SosEmpName}</b><b>&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="sos.store.number"/> ${SosStoreNum}</b></p>
							</li>
							<c:if test="${isRedirect ne true}">
							<li class="sos-header-second-li text-right">
							<dsp:form id="sosHeaderLogoutForm">
								<dsp:input bean="TRUSOSValidationFormHandler.logoutSuccessURL"  type="hidden" value="${fullyQualifiedcontextPathUrl}sos/index.jsp"/>
								<dsp:input type="submit" class="hide" id="sosHeaderLogoutId" bean="TRUSOSValidationFormHandler.logout" value="Logout"/>
								<a href="javascript:void(0);" class="sos-logout-btn">LOG OUT</a>
							</dsp:form>
								
								<%-- <dsp:a class="sos-logout-btn" href="javascript:void(0);" bean="TRUSOSValidationFormHandler.logout" value="Logout">LOG OUT</dsp:a> --%>
							</li>
							</c:if>
						</ul>
					</div>
				</dsp:oparam>
			</dsp:droplet>
			<dsp:droplet name="TRUSOSGetIntegrationConfigDroplet">
				<dsp:oparam name="output">
					<dsp:getvalueof var="isEnableAddressDoctor" param="isEnableAddressDoctor" scope="request" />
					<dsp:getvalueof var="isEnableCouponService" param="isEnableCouponService" scope="request" />
					<dsp:getvalueof var="isEnableEpslon" param="isEnableEpslon" scope="request" />
					<dsp:getvalueof var="isEnableHookLogic" param="isEnableHookLogic" scope="request" />
					<dsp:getvalueof var="isEnablePowerReviews" param="isEnablePowerReviews" scope="request" />
				</dsp:oparam>
			</dsp:droplet>
		</c:when>
		<c:otherwise>
			<dsp:droplet name="TRUGetIntegrationConfigDroplet">
				<dsp:oparam name="output">
					<dsp:getvalueof var="isEnableAddressDoctor" param="isEnableAddressDoctor" scope="request" />
					<dsp:getvalueof var="isEnableCouponService" param="isEnableCouponService" scope="request" />
					<dsp:getvalueof var="isEnableEpslon" param="isEnableEpslon" scope="request" />
					<dsp:getvalueof var="isEnableHookLogic" param="isEnableHookLogic" scope="request" />
					<dsp:getvalueof var="isEnablePowerReviews" param="isEnablePowerReviews" scope="request" />
				</dsp:oparam>
			</dsp:droplet>
		</c:otherwise>
	</c:choose>			
</dsp:page>