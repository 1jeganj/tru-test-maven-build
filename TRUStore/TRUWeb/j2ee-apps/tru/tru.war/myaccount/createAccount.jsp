<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:getvalueof var="customerID" bean="Profile.id"/>
<dsp:getvalueof var="contextPath" value="${originatingRequest.contextPath}"/>
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
<c:if test="${site eq babySiteCode}">
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
</c:if>

	<form id="createAccountForm">
		<div class="col-md-4">
		    <div class="my-account-sign-in-col create-account">
		        
		        <div class="truLoginPage" id="truSignUp">
		        <p class="global-error-display"></p>
			        <h2><fmt:message key="myaccount.create.account" /></h2>
			        <p><fmt:message key="myaccount.signup.message" /></p>
			        <!-- div for password strength tooltip positioning -->
		            <label for="loginEmail"><fmt:message key="myaccount.emailaddress" /></label>
		            <input type="text" id="loginEmail" name="email" autocomplete="off" maxlength="60"/>
		            <fmt:message key="myaccount.passworddetails" var="passwordDetails"/>
		            <fmt:message key="myaccount.passworddetailscont" var="passwordDetailsConti"/>
		            <input type="hidden" id="passwordDetails" value="${passwordDetails}"/>
		            <input type="hidden" id="passwordDetailsConti" value="${passwordDetailsConti}"/>
		            <label for="passwordInput"><fmt:message key="myaccount.password" /><span style="display:inline-block"><span style="padding:0 10px">&#xb7;</span><a class="detailsInfo showOnFocus" href="javascript:void(0)"><fmt:message key="myaccount.details" /></a></span></label>
					<div class="password-tools">
							<!-- <input type="password" id="passwordInput" class="passwordInput" name="password" data-target="#myAccountAddAddressModal"> -->
						<input type="password" id="passwordInput" name="password" class="passwordInput" maxlength="50" autocomplete="off" aria-label="passwordInput" title="passwordInput"/>
					</div>
					<label class="confirm-password" for="ConfirmPassword" style="display:block"><fmt:message key="myaccount.confirm.password" /></label>
					           <!--  <input class="confirm-password" type="password" name="confirm_password" style="display:block"> -->
					<input class="confirm-password" type="password" id="ConfirmPassword" name="confirm_password" style="display:block" maxlength="50" autocomplete="off" aria-label="ConfirmPassword" title="ConfirmPassword"/>
				</div>
		        <div class="button-container">
		            <!--  Added onclick function  for tealium  -->
		            <input type="submit" id="createAccountSubmitBtn" value="sign up" class="profileForm" />
		            
		        </div>
		    </div>
		</div>

	</form>

<script>

function loadOmniRegScript(){
	var email=$("#loginEmail").val();
	 utag.link({
	    event_type:'account_created',
	    customer_email:email,
	    customer_id: "${customerID}",
	    partner_name: '${partnerName}'
	});
}
</script>
</dsp:page>