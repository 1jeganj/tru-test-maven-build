<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<dsp:importbean bean="com/tru/common/TRUStoreConfiguration"/>
<dsp:getvalueof var="versionFlag" bean="TRUStoreConfiguration.versioningFlag"/>
<dsp:getvalueof var="staticAssetsVersionFormat" bean="TRUStoreConfiguration.staticAssetsVersion"/>
<dsp:getvalueof var="contextPath" value="${originatingRequest.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
	<base target="_parent">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta http-equiv="content-style-type" content="text/css" />
	<c:choose>
			<c:when test="${versionFlag}">
				<link rel="stylesheet" href="${contextPath}css/toolkit.css?${staticAssetsVersionFormat}=${versionNumbers}">
				<link rel="stylesheet" href="${contextPath}css/components.css?${staticAssetsVersionFormat}=${versionNumbers}">
				<link rel="stylesheet" href="${contextPath}css/structures.css?${staticAssetsVersionFormat}=${versionNumbers}">
				<link rel="stylesheet" href="${contextPath}css/templates.css?${staticAssetsVersionFormat}=${versionNumbers}">
				<link rel="stylesheet" href="${contextPath}css/tRus_custom.css?${staticAssetsVersionFormat}=${versionNumbers}">
			</c:when>
			<c:otherwise>
				<link rel="stylesheet" href="${contextPath}css/toolkit.css">
				<link rel="stylesheet" href="${contextPath}css/components.css">
				<link rel="stylesheet" href="${contextPath}css/structures.css">
				<link rel="stylesheet" href="${contextPath}css/templates.css">
				<link rel="stylesheet" href="${contextPath}css/tRus_custom.css">
			</c:otherwise>
		</c:choose>
		<style>
			.sign-in-overlay {
  width: 100%;
  background-color: #fff; }
  .sign-in-overlay > div {
    display: table; }
  .sign-in-overlay p {
    font-size: 14px;
    color: #666; }
  .sign-in-form input {
    height: 35px;
    width: 340px;
    border: 1px solid #dedede; }
  .sign-in-form button {
    font-size: 18px;
    color: #fff;
    background-color: #009DDB;
    border: none;
    height: 35px;
    width: 140px; }
  .sign-in-form {
    float: left;
    position: relative; }
   .sign-in-form div {
      margin-bottom: 15px; }
    .sign-in-form p {
      margin-bottom: 3px; }
    .sign-in-form a {
      float: right;
      color: #004EBC;
      padding-top: 8px; }
       .sign-in-header {
    width: 100%;
    margin-bottom: 25px; }
#signin-submit-btn {
    width: 140px;
    height: 36px;
    float: left;
    cursor: pointer;
    font-size: 18px;
    color: #fff;
    background-color: #009DDB;
}
#signin-submit-btn:hover {
    background-color: #fff;
    color: #009DDB;
    border: 3px solid #009DDB;
}
			
		</style>
</head>
<body>
<div>
	<div class="sign-in-form">
	<form class="JSValidation"  id="hearderLoginOverlay">
		<p class="global-error-display"></p>
		<div>
			<label for="login"><fmt:message key="myaccount.email" /></label>
			<input name="email" id="login" type="text" maxlength="60" />
		</div>
		<div>
			<label for="sign-in-password-input"><fmt:message key="myaccount.password" /></label>
			<input name="password" id="sign-in-password-input" maxlength="50" type="password" />
		</div>
		<div>
			<!-- <button>Sign In</button> -->
			 <input type="button" value="Sign In" class="submit-btn" id="signin-submit-btn"/>  
			 <input type="hidden" value="${contextPath}" class="contextPath"/>  
			<a id="forgotPasswordPopupLink" href="javascript:void(0);"  class="reset-password-flink" onclick="javascript:forgotPassword();"><fmt:message key="myaccount.forgotpassword" /></a>
			
			
			<input type="hidden" id="modal-custId" value="${customerID}"/>
			<input type="hidden" id="modal-partnerName" value="${partnerName}"/>
		</div>
	</form>
	</div>
</div>
<%-- <dsp:include page="/myaccount/truForgotPassword.jsp" /> --%>
<c:choose>
		<c:when test="${versionFlag}">
			<script type="text/javascript" charset="UTF-8"
				src="${contextPath}javascript/globalHeader.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
				
			<script type="text/javascript" charset="UTF-8"
				src="${contextPath}javascript/tru_clientSideValidation.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>	
			
			<script type="text/javascript" charset="UTF-8"
				src="${contextPath}javascript/tRus_custom.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
		</c:when>
		<c:otherwise>
			<script type="text/javascript" charset="UTF-8"
				src="${contextPath}javascript/globalHeader.js"></script>
			<script type="text/javascript" charset="UTF-8"
				src="${contextPath}javascript/tru_clientSideValidation.js"></script>	
			<script type="text/javascript" charset="UTF-8"
				src="${contextPath}javascript/tRus_custom.js"></script>
		</c:otherwise>
	</c:choose>
<%-- <script src="${contextPath}javascript/globalHeader.js"></script>
<script type="text/javascript" charset="UTF-8" src="${contextPath}javascript/tru_clientSideValidation.js"></script>
<script src="${contextPath}javascript/tRus_custom.js"></script> --%>

<script> 
$(document).on("click", '#signin-submit-btn',function(){
	headerSignInSubmit();
});
function headerSignInSubmit(){
    $('.errorDisplay').remove();
    var contextPath=$(".contextPath").val();
    var email = $('#login').val();
    var modalCustId = $('#modal-custId').val();
    var modalPartnerName = $('#modal-partnerName').val();
    var currentUri = window.location.href;
    currentUri = currentUri.split('#')[0];
    //window.parent.ss();

    $.ajax({
           type: "POST",
           cache:false,
           /*async:false,*/
           dataType: "html",
           url: "/myaccount/intermediate/ajaxIntermediateRequest.jsp?formID=hearderLoginOverlay",
           data: $("#hearderLoginOverlay").serialize(),
           success: function(data) {
        	   if(data.indexOf('Following are the form errors:') > -1){
                  	$('#signInModal #signin-submit-btn').removeAttr('disabled');
                    addBackendErrorsToForm(data,  $('#hearderLoginOverlay'));
                    omniLoginFail();
              }else{
             	 console.log('justLoggedIn from header');
             	 var currentDomain = window.location.host;
           	  	 var crDomain = 'http://'+window.location.host;
           	  	 window.parent.postMessage("TRUSigninReload",crDomain);
              }
           },
     	  error: function(xhr, ajaxOptions, thrownError) {
 		      if(xhr.status=='409'){
 		    	  var contextPath = $(".contextPath").val();
 		          location.href = contextPath + 'myaccount/myAccount.jsp?sessionExpired=true';
 		      }
     	  }
    });

    return false;
}
function forgotPassword(){
	var currentDomain = window.location.host;
	var crDomain = 'http://'+window.location.host;
	window.parent.postMessage("forgotPasswordIframe",crDomain);
}
</script>
</body>
</dsp:page>