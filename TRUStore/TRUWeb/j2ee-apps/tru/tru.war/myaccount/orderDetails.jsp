<dsp:page>
<dsp:importbean bean="/com/tru/integration/oms/TRUCustomerOrderDetailsDroplet"/>
<dsp:getvalueof var="orderId" name="orderId"/>
<dsp:droplet name="TRUCustomerOrderDetailsDroplet">
	<dsp:param name="orderId" value="${orderId}"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="response" param="response"/>
		<json:object>
			<json:property name="response" value="${response}"/>
		</json:object>
	</dsp:oparam>
</dsp:droplet>
</dsp:page>