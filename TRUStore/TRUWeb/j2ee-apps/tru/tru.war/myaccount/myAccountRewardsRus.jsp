<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/multisite/Site"/>
<dsp:getvalueof bean="Site.olsonSignUpURL" var="olsonSignUpURL" />
<dsp:getvalueof bean="Site.olsonLookUpURL" var="olsonLookUpURL" />
<div class="rewards-zone">
<dsp:getvalueof bean="Profile.rewardNo" var="rewardNumber"></dsp:getvalueof>
<c:choose>
<c:when test="${empty rewardNumber}">
	<c:set var="rewardNumPresent" value="false"></c:set>
</c:when>
	<c:otherwise>
	<c:set var="rewardNumPresent" value="true"></c:set>
	</c:otherwise>
</c:choose>

	<div class="my-account-rewards-no-membership <c:if test="${rewardNumPresent eq true}">hide-data</c:if>" id="my-account-rewards-no-membership"  >
		<h3><fmt:message key="myaccount.rewardsrus" /></h3>
		<p><fmt:message key="myaccount.enter.membership.number.earn.rewards" /></p>
		<div>
			<div class="membership-number-group">
				<h3>
					<fmt:message key="myaccount.enter.membership.number" />  <span>&#xb7;</span><a id="membershipNum"
						data-toggle="popover" class="where-is-it"
						href="javascript:void(0);"
						data-original-title="" title=""><fmt:message key="myaccount.where.is.it" /></a><span>&#xb7;</span><a
						href="${olsonSignUpURL}"
						class="" target="_blank"><fmt:message key="myaccount.sign.up.now" /></a>
						<div class="popover fade bottom in" role="tooltip" data-toggle="popover">
							<div class="arrow"></div>
							<p><fmt:message key="myaccount.find.membershipnumber.back.of.card" /></p>
							<img src="${TRUImagePath}images/rru-card-help.png" alt="rewards r us help" />
						</div>						
				</h3>
				
				<div class="inline">
					<form name="membershipForm" class="JSValidation" id="membershipForm" >
					<p class="global-error-display"></p>
						<div class="enterMembershipIDWrapper"><input type="text" id="enterMembershipID" aria-label="Membership ID" name="enterMembershipID" value="${rewardNumber}" maxlength="13" onkeypress="return isNumberOnly(event,14)" class="chkNumbersOnly"/></div>
						<div class="saveMembershipIDWrapper"><input type="submit" value="save" id="saveMembershipID" class="profileForm"/></div>
					</form>
				</div>
			</div>
			<a href="javascript:void(0);" class="" onclick="window.open('${olsonLookUpURL}','_blank','width=990,height=400')">
			<fmt:message key="myaccount.forgot.your.number" /></a>
			<p class="inline"><fmt:message key="myaccount.we.can.send" /></p>
		</div>
	</div>
	
	<div class="my-account-rewards-authenticated <c:if test="${rewardNumPresent eq false}">hide-data</c:if>" id="my-account-rewards-authenticated">
		<h2><fmt:message key="myaccount.rewardsrus" /></h2>
		<div>
			<fmt:message key="myaccount.membership.number" /> <span class="membershipID">${rewardNumber}</span><span>&#xb7;</span><a
				href="javascript:void(0);"
				id="update-rewards-authenticated"
				class="" onclick="updateRewards();"><fmt:message key="myaccount.update" /></a><span>&#xb7;</span>
				<a href="javascript:void(0);" data-target="#myAccountRemoveRewardCardNumModal" data-toggle="modal"
				class=""><fmt:message key="myaccount.remove" /></a>
				
		</div>
		<a
			href="${olsonSignUpURL}"
			class="" target="_blank"><fmt:message key="myaccount.see.my.rewards" /></a>
	</div>
</div>
</dsp:page>