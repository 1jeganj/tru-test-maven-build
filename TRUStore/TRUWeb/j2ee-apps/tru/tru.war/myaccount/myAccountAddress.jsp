<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<div class="address-book">
		<h3>
			<fmt:message key="myaccount.address.book" /><span>&#xb7;</span><a data-target="#myAccountAddAddressModal"
				data-toggle="modal"
				href="javascript: void(0);"
				title="add an Address or view all"
				class="myaccountAddressModalRestrictor" onclick="javascript:forConsoleErrorFixing(this);"><fmt:message key="myaccount.add.address" /></a>
		</h3>
		
		<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
		<dsp:droplet name="IsEmpty">
			<dsp:param name="value" bean="Profile.shippingAddress.id" />
			<dsp:oparam name="false">
				<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="Profile.secondaryAddresses"/>
				<dsp:param name="elementName" value="address"/>
				<dsp:oparam name="output">
				<dsp:getvalueof param="address.isBillingAddress" var="isBillingAddress" />
				<dsp:getvalueof param="address.id" var="addressId" />
				<c:if test="${addressId eq shippingAddressId and not isBillingAddress}">
						 <p class="home-address"><dsp:valueof param="key" /></p>
						<dsp:getvalueof var="nickName" param="key" />
						 <p class="avenir-heavy"><dsp:valueof param="address.firstName" />&nbsp;<dsp:valueof param="address.lastName" /></p>
						 <p><dsp:valueof param="address.address1" />&nbsp;<dsp:valueof param="address.address2" /></p>
						 <p><dsp:valueof param="address.city" />, <dsp:valueof param="address.state" />&nbsp;<dsp:valueof param="address.postalCode" /></p>
							<p>
								<span class="nickNameInHiddenSpan">${nickName}</span>
								<a data-target="#myAccountAddAddressModal" data-toggle="modal" href="javascript:void(0);" title="edit Address" class="myAccountEditAddress" onclick="javascript:forConsoleErrorFixing(this);">
									<fmt:message key="myaccount.edit" /></a><span>&#xb7;</span><span class="default-address"><fmt:message key="myaccount.default.address" /></span>
							</p>
				 </c:if>
				</dsp:oparam>
			   </dsp:droplet>
			</dsp:oparam>
			<dsp:oparam name="true">
				<dsp:droplet name="IsEmpty">
					<dsp:param name="value" bean="Profile.secondaryAddresses" />
					<dsp:oparam name="true">
						<p class="grey"><fmt:message key="myaccount.no.address.saved" /></p>
					</dsp:oparam>
				</dsp:droplet>
			</dsp:oparam>
		</dsp:droplet>
	</div>
</dsp:page>