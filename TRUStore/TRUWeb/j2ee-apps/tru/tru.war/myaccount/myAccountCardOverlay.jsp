<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<div class="modal fade" id="myAccountAddCreditCardModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >
		<div class="modal-dialog modal-lg" id="creditCardAjaxFragment">
			<dsp:include page="/myaccount/intermediate/cardOverlayFragment.jsp"/>
		</div>
	</div>
	<div class="modal fade in toStopScroll" id="myAccountCardDeleteModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false" >

			<div class="modal-dialog modal-lg">
				<div class="modal-content sharp-border">
					<div class="my-account-delete-cancel-overlay">
						<div>
							<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
						</div>
						<h2><fmt:message key="myaccount.delete.credit.card" /></h2>
		                <p><fmt:message key="myaccount.sure.remove.card" /></p>
		                <div>
							<form id="removeCreditCardFormId" method="POST" onsubmit="javascript: return removeCard();">
								<input type="hidden" id="removeCardHiddenId" name="removeCardHiddenName" value="" />
								<input type="submit" id="removeCardId" name="removeCardId" value="confirm"  style="display: none;"/>
								
                                <button id="delete-credit-card-btn" onclick="return deleteCard();"><fmt:message key="myaccount.confirm" /></button><a href="javascript:void(0)" data-dismiss="modal"><fmt:message key="myaccount.cancel" /></a>
							</form>
						</div>
					</div>
				</div>
			</div>
	</div>
<div class="modal fade in" id="creditCardModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false" >
   <div class="modal-dialog modal-lg">
      <div class="modal-content sharp-border">
          <dsp:droplet name="/atg/targeting/TargetingFirst">
			<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/Checkout/CreditCardOverlayTargeter" />
			<dsp:oparam name="output">
				<dsp:valueof param="element.data" valueishtml="true" />
			</dsp:oparam>
		</dsp:droplet>
      </div>
   </div>
</div>
	
</dsp:page>