<dsp:page>
<dsp:importbean bean="/com/tru/integration/oms/TRUCustomerOrderHistoryDroplet"/>
<dsp:getvalueof var="pageNumber" param="pageNumber"/>
<dsp:getvalueof var="customerId" param="customerId"/>
<dsp:droplet name="TRUCustomerOrderHistoryDroplet">
	<dsp:param name="customerId" value="${customerId}"/>
	<dsp:param name="pageNumber" value="${pageNumber}"/>
	<dsp:param name="noOfRecordsPerPage" value="${noOfRecordsPerPage}"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="response" param="response"/>
		${response}
	</dsp:oparam>
</dsp:droplet>
</dsp:page>