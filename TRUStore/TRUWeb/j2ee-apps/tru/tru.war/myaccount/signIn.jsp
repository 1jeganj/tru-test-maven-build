<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
	<dsp:getvalueof var="contextPath" value="${originatingRequest.contextPath}"/>
	<dsp:getvalueof param="sessionExpired" var="sessionExpired"/>
    <dsp:getvalueof var="customerID" bean="Profile.id"/>
    <dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
	<c:if test="${site eq babySiteCode}">
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
	</c:if>
	<c:if test="${sessionExpired}">
		<dsp:getvalueof var="loginFromCookie" value="${cookie.VisitorUsaFullName.value}"/>
	</c:if>
	<div class="col-md-4">
		<div class="my-account-sign-in-col returning-customers">
			
			<form class="JSValidation" id="signInForm">
				<div class="truLoginPage" id="truSignIn">
					<p class="global-error-display"></p>
					<h2><fmt:message key="myaccount.returning.customers" /></h2>
					<p><fmt:message key="myaccount.returning.customers.message" /></p>
					<!-- div for password strength tooltip positioning -->
					<label for="email"><fmt:message key="myaccount.emailaddress" /></label>
					<input name="email" id="email" value="${loginFromCookie}"  type="text" autocomplete="off" maxlength="60"/>
					<label for="password"><fmt:message key="myaccount.password" /></label>
					<input name="password" class="sigInPassword" id="password"  maxlength="50" type="password" autocomplete="off"/>
					<div class="signin-error">
               			<fmt:message key="myaccount.resetpassword.migrationmessage2" />
                	</div>				
				</div>
				<input type="hidden" class="contextPath" value="${contextPath}"/>
				<div class="button-container">
					<input type="submit" id="loginSubmitBtn" value="sign in" class="profileForm" />
					
					<a data-target="#forgotPasswordModel" id="forgotPasswordLandingLink" data-toggle="modal" href="javascript:void(0);" style="display: inline-block" onclick="javascript:forConsoleErrorFixing(this);"><fmt:message key="myaccount.forgotpassword" /></a>
					
					<input type="hidden" id="signin-custId" value="${customerID}"/>
					<input type="hidden" id="signin-partnerName" value="${partnerName}"/>
				</div>
			</form>
		</div>
	</div>	
	
</dsp:page>