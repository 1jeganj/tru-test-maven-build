
<dsp:page>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
 <dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
  <dsp:droplet name="Switch">
	<dsp:param bean="ProfileFormHandler.formError" name="value"/>
	<dsp:oparam name="true">
		 <dsp:droplet name="ErrorMessageForEach">
	        <dsp:param name="exceptions" bean="ProfileFormHandler.formExceptions"/>
			<dsp:oparam name="outputStart">
			</dsp:oparam>
			<dsp:oparam name="output">
			   <div style="color:red"><dsp:valueof param="message"/></div>
			</dsp:oparam>
		</dsp:droplet>
	</dsp:oparam>
  </dsp:droplet>
</dsp:page>