<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:getvalueof bean="Profile.email" var="email"/>
	<%-- Start: Email change popup --%>
	<div class="update-email-header">
		<a href="javascript:void(0)" class="closeDailog" data-dismiss="modal">
			<span class="sprite-icon-x-close"></span>
		</a>
		<div>
			<h2 class="custmHeading"><fmt:message key="myaccount.update.email" /></h2>
		</div>
	</div>
	<div class="update-email-body">											
		<form id="emailAddressFormValidation" class="JSValidation">
			<div>
				<p><fmt:message key="myaccount.current.email" /></p>
				<p id="email-word-wrap">${email}</p>
			</div>
			<div>
				<label for="new_email"><fmt:message key="myaccount.new.email" /></label>
				<input type="text" id="new_email" name="new_email" value="" maxlength="60"/>
			</div>
			<div>
				<label for="con_email"><fmt:message key="myaccount.confirm.email" /></label>
				<input type="text" name="con_email"  id="con_email" value="" maxlength="60"/> 
			</div>
			<div>													
				<input type="submit"  value="save changes" class="submit-btn" id="emailAddress-submit-btn" />			  										
			</div>
		</form>			
	</div>
	<%-- End:email change popup --%>
</dsp:page>