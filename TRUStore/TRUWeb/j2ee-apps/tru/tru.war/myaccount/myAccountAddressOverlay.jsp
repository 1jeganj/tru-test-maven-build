<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	
	<dsp:droplet name="IsEmpty">
		<dsp:param name="value" param="editAddress" />
		<dsp:oparam name="true">
			<dsp:getvalueof var="isEditAddress" value="false" />
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:getvalueof var="isEditAddress" value="true" />
		</dsp:oparam>
	</dsp:droplet>

   	<div class="modal fade" id="myAccountAddAddressModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" id="addressAjaxFragment">
           	<%-- This will be replaced by ajaxResponse in case of save or edit address scenario, on page load, just it is including --%>
           	<dsp:include page="/myaccount/intermediate/addressAjaxResponse.jsp"/>
        </div>
    </div>
    <!-- delete credit card popup start-->
		<div class="modal fade in toStopScroll" id="myAccountDeleteCancelModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false" >
				<div class="modal-dialog modal-lg">
					<div class="modal-content sharp-border">
						<div class="my-account-delete-cancel-overlay">
							<div>
								<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
							</div>
							<h2><fmt:message key="myaccount.delete.address" /></h2>
							<p><fmt:message key="myaccount.sure.remove.address" /></p>
							<div>
							<form id="removeShippingAddress" method="POST" onsubmit="javascript: return removeAddressConfirm();">
								<input type="hidden" id="removeAddressHiddenId" class="removeAddressHiddenId" value="" />
								
								<input type="submit" id="removeShippingAddressId" name="removeShippingAddress" value="confirm"  style="display: none;"/>
								
                                <button onclick="return deleteAddress();"><fmt:message key="myaccount.confirm" /></button><a href="javascript:void(0)" data-dismiss="modal"><fmt:message key="myaccount.cancel" /></a>
							</form>
							</div>
						</div>
					</div>
				</div>
		</div>

		<!-- delete credit card popup end-->
</dsp:page>