<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/com/tru/common/droplet/TRUCreditCardExpiryCheckDroplet"/>
<dsp:importbean bean="/com/tru/common/droplet/TRUCardDisplayDroplet"/>
<dsp:getvalueof var="defaultCardExists" vartype="java.lang.boolean" value="false" />
<div class="saved-credit-card cardDetailsRow">
    <h3><fmt:message key="myaccount.saved.credit.cards" /><span>&#xb7;</span><a data-target="#myAccountAddCreditCardModal" data-toggle="modal" href="javascript:void(0);" class="myaccountAddEditCardModalRestrictor" onclick="javascript:forConsoleErrorFixing(this);"><fmt:message key="myaccount.add.view.card" /></a></h3>
<dsp:droplet name="IsEmpty">
 <dsp:param bean="Profile.defaultCreditCard.id" name="value"/>
 <dsp:oparam name="false">
			<dsp:droplet name="/atg/dynamo/droplet/ForEach">
				<dsp:param name="array" bean="Profile.creditCards" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="defaultId" bean="Profile.defaultCreditCard.id"/>
					<dsp:getvalueof id="creditCardId" param="element.id" />
					<c:if test="${defaultId eq creditCardId}">
						<dsp:getvalueof var="nickName" param="key"/>
						<dsp:getvalueof var="defaultCardExists" vartype="java.lang.boolean" value="true"/>
					</c:if>
				</dsp:oparam>
			</dsp:droplet>	
 	<div class="row"> 	
 	<dsp:droplet name="TRUCreditCardExpiryCheckDroplet">
		<dsp:param name="month" bean="Profile.defaultCreditCard.expirationMonth"/>
		<dsp:param name="year" bean="Profile.defaultCreditCard.expirationYear"/>
		<dsp:oparam name="true">
			<dsp:getvalueof var="expiredClass" value="cardExpired"></dsp:getvalueof>
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:getvalueof var="expiredClass" value=""></dsp:getvalueof>
		</dsp:oparam>
	</dsp:droplet>
		<div class="col-md-5 creditCard-adjustment ${expiredClass}">
			<dsp:getvalueof var="cardType" bean="Profile.defaultCreditCard.creditCardType" vartype="java.lang.String"></dsp:getvalueof>
			<dsp:getvalueof var="cardId" bean="Profile.defaultCreditCard.id" vartype="java.lang.String"></dsp:getvalueof>
			<c:choose>
				<c:when test="${cardType eq 'visa'}">
					<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Visa-Thumb-No-Outline.jpg" alt="visa card">
				</c:when>
				<c:when test="${cardType eq 'discover'}">
					<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Discover-Thumb.jpg" alt="discover">
				</c:when>
				<c:when test="${cardType eq 'masterCard'}">
					<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Mastercard-Thumb-No-Outline.jpg" alt="Master card">
				</c:when>
				<c:when test="${cardType eq 'americanExpress'}">
					<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Amex-Thumb-No-Outline.jpg" alt="Amex">
				</c:when>
				<c:when test="${cardType eq 'RUSPrivateLabelCard'}">
					<img class="credit-card" src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-1.jpg" alt="PLCCCard">
				</c:when>
				<c:when test="${cardType eq 'RUSCoBrandedMasterCard'}">
					<img class="credit-card" src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-2.jpg" alt="RUSCoBCard">
				</c:when>
				<c:otherwise>
				No Image Available
				</c:otherwise>
			</c:choose>
			
			<dsp:valueof bean="Profile.defaultCreditCard.creditCardNumber" converter="creditcard" maskCharacter="x"/>	
		</div>
		<div class="col-md-6 ${expiredClass}">
			<div class="cardExpDate">
			<c:if test="${cardType ne 'RUSPrivateLabelCard'}">
				<dsp:valueof bean="Profile.defaultCreditCard.expirationMonth"></dsp:valueof>/<dsp:valueof bean="Profile.defaultCreditCard.expirationYear"></dsp:valueof>
			</c:if>
					<c:if test="${expiredClass eq 'cardExpired'}">
						<br/><fmt:message key="myaccount.card.expired" />
					</c:if>
			</div><span>&#xb7;</span><a data-target="#myAccountAddCreditCardModal" data-toggle="modal" href="javascript:void(0);" title="edit card details" class="" onclick="onEditCreditCardOverlay('${nickName}')"><fmt:message key="myaccount.edit" /></a><span>&#xb7;</span>
			<span class="default-card"><fmt:message key="myaccount.default.card" /></span>
		</div>
		<div class="col-md-1">
			<a href="javascript:void(0)" class="delete-icon" title="delete card" data-toggle="modal" data-target="#myAccountCardDeleteModalLanding" onclick="removeCardConfirmLanding('${nickName}')"><img src="${TRUImagePath}images/delete-icon.png" alt="delete card icon"></a>
		</div>
	</div>
</dsp:oparam>
</dsp:droplet>
<dsp:droplet name="IsEmpty">
 <dsp:param bean="Profile.creditCards" name="value"/>
 <dsp:oparam name="false">
<dsp:droplet name="TRUCardDisplayDroplet">
	<dsp:param name="creditCards" bean="Profile.creditCards" />
	<dsp:param name="defaultCreditCard" bean="Profile.defaultCreditCard" />
	<dsp:param name="defaultCardExists" value="${defaultCardExists}" />
	<dsp:oparam name="output">
		<dsp:droplet name="ForEach">
		<dsp:param name="array" param="element" />
		<dsp:param name="sortProperties" value="-lastActivity"/>
		<dsp:oparam name="output">
		<dsp:getvalueof var="creditCardId" param="element.id" />
		<dsp:getvalueof var="creditCardObj" param="element" />
		<dsp:droplet name="ForEach">
			<dsp:param name="array" bean="Profile.creditCards" />
				<dsp:oparam name="output">
					<dsp:getvalueof id="elementId" param="element.id" />
						<c:if test="${creditCardId eq elementId}">
							<dsp:getvalueof var="nickName" param="key"></dsp:getvalueof>
						</c:if>
				</dsp:oparam>
		</dsp:droplet>
		<div class="row">  
  			<dsp:droplet name="TRUCreditCardExpiryCheckDroplet">
				<dsp:param name="month" param="element.expirationMonth"/>
				<dsp:param name="year" param="element.expirationYear"/>
				<dsp:oparam name="true">
					<dsp:getvalueof var="expiredClass" value="cardExpired"></dsp:getvalueof>
				</dsp:oparam>
				<dsp:oparam name="false">
					<dsp:getvalueof var="expiredClass" value=""></dsp:getvalueof>
				</dsp:oparam>
			</dsp:droplet>
  			<div class="col-md-5 creditCard-adjustment ${expiredClass}">
  				<dsp:getvalueof var="cardType" param="element.creditCardType" vartype="java.lang.String"></dsp:getvalueof>
  				<dsp:getvalueof var="creditCard" param="element"></dsp:getvalueof>
  				<c:choose>
					<c:when test="${cardType eq 'visa'}">
						<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Visa-Thumb-No-Outline.jpg" alt="visa card">
					</c:when>
					<c:when test="${cardType eq 'discover'}">
						<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Discover-Thumb.jpg" alt="discover">
					</c:when>
					<c:when test="${cardType eq 'masterCard'}">
						<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Mastercard-Thumb-No-Outline.jpg" alt="Master card">
					</c:when>
					<c:when test="${cardType eq 'americanExpress'}">
						<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Amex-Thumb-No-Outline.jpg" alt="Amex">
					</c:when>
					<c:when test="${cardType eq 'RUSPrivateLabelCard'}">
						<img class="credit-card" src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-1.jpg" alt="PLCCCard">
					</c:when>
					<c:when test="${cardType eq 'RUSCoBrandedMasterCard'}">
						<img class="credit-card" src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-2.jpg" alt="RUSCoBCard">
					</c:when>
					<c:otherwise>
					No Image Available
					</c:otherwise>
			</c:choose>
				<dsp:valueof value="${creditCardObj.creditCardNumber}" converter="creditcard" maskCharacter="x"/>
			</div>
			
				<div class="col-md-6 ${expiredClass}">
					<div class="cardExpDate">
					<c:if test="${cardType ne 'RUSPrivateLabelCard'}">
						<dsp:valueof value="${creditCardObj.expirationMonth}"></dsp:valueof>/<dsp:valueof value="${creditCardObj.expirationYear}"></dsp:valueof>
					</c:if>
						<c:if test="${expiredClass eq 'cardExpired'}">
							<br/><fmt:message key="myaccount.card.expired" />
						</c:if>
					</div><span>&#xb7;</span><a data-target="#myAccountAddCreditCardModal" data-toggle="modal" href="javascript:void(0);" title="edit card details" class="" onclick="onEditCreditCardOverlay('${nickName}')"><fmt:message key="myaccount.edit" /></a><span>&#xb7;</span>
					<span class="default-card"><a href="javascript:void(0);" title="set as default card" class="" onclick="defaultCard('${nickName}','landingPage'); return false"><fmt:message key="myaccount.set.default.card" /></a></span>
				</div>
			
				<div class="col-md-1">
					<a href="javascript:void(0)" class="delete-icon" title="delete card" data-toggle="modal" data-target="#myAccountCardDeleteModalLanding" onclick="removeCardConfirmLanding('${nickName}')"><img src="${TRUImagePath}images/delete-icon.png" alt="delete card icon"></a>
				</div>
			</div>
		</dsp:oparam>
		</dsp:droplet>
</dsp:oparam>															
</dsp:droplet>
 </dsp:oparam>
  <dsp:oparam name="true">
  		<p class="grey"><fmt:message key="myaccount.no.card.saved" /></p>
  </dsp:oparam>
  </dsp:droplet>

</div>
