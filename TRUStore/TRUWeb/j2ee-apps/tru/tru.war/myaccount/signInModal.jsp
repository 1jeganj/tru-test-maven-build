<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration" var="TRUConfiguration"/>
<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
<dsp:getvalueof var="contextPath" value="${originatingRequest.contextPath}"/> 
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
<dsp:getvalueof var="host" value="${originatingRequest.host}"/>
<dsp:getvalueof var="signInIframeUrl" value="https://${host}${contextPath}myaccount/signinIframeContent.jsp"/>
<dsp:getvalueof var="previewPromoEnableInStaging" bean="TRUConfiguration.previewPromoEnableInStaging"/>
<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
	<dsp:oparam name="output">
	<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
	</dsp:oparam>
</dsp:droplet>
<c:if test="${site eq babySiteCode}">
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
</c:if>
<dsp:getvalueof var="customerID" bean="Profile.id"/>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<div class="modal fade" id="signInModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content sharp-border">
			<div class="sign-in-overlay">
				<div class="sign-in-header">
					<div>
						<h3 class="w3ValidationSigninHeading"><fmt:message key="myaccount.signin" /></h3>
					</div>
					<div>
						<a href="javascript:void(0)" data-dismiss="modal">
							<span class="sprite-icon-x-close"></span>
						</a>
					</div>
				</div>
				<c:choose>
					<c:when test="${fn:contains(scheme,'http:') and not previewPromoEnableInStaging}">
						<iframe data-src=${signInIframeUrl}  id="secureSigninIframe" ></iframe>
					</c:when>
					<c:otherwise>
						<div>
							<div class="sign-in-form">
							<form class="JSValidation"  id="hearderLoginOverlay">
								<p class="global-error-display"></p>
								<div>
									<label for="login"><fmt:message key="myaccount.email" /></label>
									<input name="email" id="login" type="text" maxlength="60" />
								</div>
								<div>
									<label for="sign-in-password-input"><fmt:message key="myaccount.password" /></label>
									<input name="password" id="sign-in-password-input" maxlength="50" type="password" />
								</div>
								<div>
									<!-- <button>Sign In</button> -->
									 <input type="button" value="Sign In" class="submit-btn" id="signin-submit-btn"/>  
									 <input type="hidden" value="${contextPath}" class="contextPath"/>  
									<a data-target="#forgotPasswordModel" id="forgotPasswordPopupLink" data-toggle="modal" href="javascript:void(0);"  class="reset-password-flink" onclick="javascript:forConsoleErrorFixing(this);"><fmt:message key="myaccount.forgotpassword" /></a>
									
									
									<input type="hidden" id="modal-custId" value="${customerID}"/>
									<input type="hidden" id="modal-partnerName" value="${partnerName}"/>
								</div>
							</form>
							</div>
						</div>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
</div>

</dsp:page>