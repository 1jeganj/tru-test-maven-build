<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<%-- Start update password popup --%>
	<div class="update-password-header">
		<div>
			<h2 class="custmHeading"><fmt:message key="myaccount.update.password" /></h2>
		</div>
		<div>
			<a href="javascript:void(0)"  data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
		</div>
	</div>
	
	<div>
		<div class="update-password-form">
			<form class="JSValidation" id="passwordFormValidation" >
				<p class="global-error-display"></p>
				<div>
					<label for="current_password"><fmt:message key="myaccount.current.password" /></label>
					<input type="password" value="" name="current_password" id="current_password" maxlength="50"/>
				</div>
				<div>
					<label for="passwordInput"><fmt:message key="myaccount.new.password" /></label>
					<div class="password-tools">
						<input id="passwordInput"  value=""  type="password" class="passwordInput" name="password" maxlength="50"/>
					</div>
				</div>
				<div>
					<label for="confirm_password"><fmt:message key="myaccount.confirm.new.password" /></label>
					<input type="password" name="confirm_password"  id="confirm_password" value="" maxlength="50"/>
				</div>
				<div>
					 <input type="submit" value="save changes" class="submit-btn" id="password-submit-btn"/>  
				</div>
			</form>
		</div>
		<div class="update-password-requirements">
			<p>
				<fmt:message key="myaccount.changePassword.rules" />
			</p>			
		</div>
	</div>
	<%-- End update password popup --%>	
	</dsp:page>