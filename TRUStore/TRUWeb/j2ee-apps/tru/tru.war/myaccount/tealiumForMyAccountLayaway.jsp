<dsp:page>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration"/>
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<%-- <dsp:importbean bean="/com/tru/integrations/cardinal/CardinalJwtDroplet" /> --%>
<dsp:importbean bean="/com/tru/commerce/droplet/TRURecentlyViewedHistoryCollectorDroplet"/>


<dsp:getvalueof bean="TRUTealiumConfiguration.accountPageName" var="accountPageName"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.accountPageType" var="accountPageType"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.browserID" var="browserID"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.accountInternalCampaignPage" var="internalCampaignPage"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.accountOrsoCode" var="orsoCode"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.accountIspuSource" var="ispuSource"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.deviceType" var="deviceType"/>
<dsp:getvalueof var="myAccountPage" value="My Account"/>
<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>
<dsp:getvalueof var="customerEmail" bean="Profile.login"/>
<dsp:getvalueof var="customerId" bean="Profile.id"/>
<dsp:getvalueof var="customerDOB" bean="Profile.dateOfBirth"/>
<dsp:getvalueof var="loginStatus" bean="Profile.transient"/>
<dsp:getvalueof var="isFromSignIn" param="isFromSignIn"/>
<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
<dsp:getvalueof var="siteCode" value="TRU" />
<c:set var="tPageType" value="account"/>


<dsp:getvalueof var="customerFirstName" bean="Profile.firstName"/>
	<dsp:getvalueof var="customerLastName" bean="Profile.lastName"/>
	<c:set var="space" value=" "/>
	<c:choose>
	<c:when test="${empty customerFirstName && empty customerLastName}">
		<c:set var="customerName" value='${fn:substring(customerEmail, 0, fn:indexOf(customerEmail, "@"))}'/>
	</c:when>
	<c:when test="${not empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value="${customerFirstName}"/>
	</c:when>
	<c:when test="${empty customerFirstName && not empty customerLastName}">
		   <c:set var="customerName" value="${customerLastName}"/>
	</c:when>
	<c:otherwise>
			<c:set var="customerName" value="${customerFirstName}${space}${customerLastName}"/>
	</c:otherwise>
 </c:choose>

<c:choose>
<c:when test="${loginStatus eq 'true'}">
<c:set var="customerStatus" value="Guest"/>
</c:when>
<c:otherwise>
<c:set var="customerStatus" value="Registered"/>
<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
		<dsp:droplet name="IsEmpty">
			<dsp:param name="value" bean="Profile.shippingAddress.id" />
			<dsp:oparam name="false">
				<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="Profile.secondaryAddresses"/>
				<dsp:param name="elementName" value="address"/>
				<dsp:oparam name="output">
				<dsp:getvalueof param="address.id" var="addressId" />
				<c:if test="${addressId eq shippingAddressId}">

						 <dsp:getvalueof param="address.city" var="customerCity"/>
						 <dsp:getvalueof param="address.state" var="customerState"/>
						 <dsp:getvalueof param="address.postalCode" var="customerZip"/>
						 <dsp:getvalueof param="address.country" var="customerCountry"/>

				 </c:if>
				</dsp:oparam>
			   </dsp:droplet>
			</dsp:oparam>
</dsp:droplet>
</c:otherwise>
</c:choose>

<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
<c:choose>
<c:when test="${isSosVar eq 'true'}">
<c:set var="selectedStore" value="sos"/>
</c:when>
<c:otherwise>
<c:set var="selectedStore" value=""/>
</c:otherwise>
</c:choose>
<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
<c:if test="${siteId eq babySiteCode}">
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
<dsp:getvalueof var="siteCode" value="BRU" />
</c:if>


<dsp:droplet name="/com/tru/commerce/locations/TRUGetCookieDroplet">
	<dsp:param name="cookieName" value="favStore" />
	<dsp:oparam name="output">
		<dsp:getvalueof var="cookieValue" param="cookieValue" />
		<dsp:getvalueof var="locationIdFromCookie" param="locationId" />
	</dsp:oparam>
	<dsp:oparam name="empty">
	</dsp:oparam>
</dsp:droplet>

<c:set var="session_id" value="${cookie.sessionID.value}"/>
<c:set var="visitor_id" value="${cookie.visitorID.value}"/>
		

<!-- Start: Script for Tealium Integration (My Account Page)-->
<script type='text/javascript'>
	var utag_data = {
		customer_status: "${customerStatus}",
		customer_city: "${customerCity}",
		customer_country: "${customerCountry}",
		customer_email:"${customerEmail}",
		customer_id:"${customerId}",
		customer_type:"${customerStatus}",
		customer_state:"${customerState}",
		customer_zip:"${customerZip}",
		customer_dob:"${customerDOB}",
		page_name:"${myAccountPage}: Layaway",
		page_type:"${accountPageType} Layaway",
		browser_id:"${pageContext.session.id}",
		internal_campaign_page:"${internalCampaignPage}",
		orso_code:"${orsoCode}",
		ispu_source:"${ispuSource}",
		device_type:"${deviceType}",
		partner_name:"${partnerName}",
		customer_name:"${customerName}",
		selected_store:"${selectedStore}",
		store_locator:"${locationIdFromCookie}",
		session_id : "${session_id}",
		visitor_id : "${visitor_id}",
		tru_or_bru : "${siteCode}"
	};

    (function(a,b,c,d){
    a='${tealiumURL}';
    b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
    a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
    })();
</script>
<!-- End: Script for Tealium Integration -->

<c:if test="${isFromSignIn eq 'true'}">
	<script type='text/javascript'>
		utag_data.event_type = "login";
	</script>
</c:if>

</dsp:page>