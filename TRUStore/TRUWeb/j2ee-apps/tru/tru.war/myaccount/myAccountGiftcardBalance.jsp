<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<div class="my-account-gift-card-balance">
	<h2 class="custmHeading"><fmt:message key="checkout.giftcard.balance"/></h2>
	<p>
		<fmt:message key="myaccount.giftcard.instruction"/>
	</p>
		
	<div class="card-input">
		<form id='myAccountCheckBalanceForm' autocomplete="off">
			<p class="global-error-display"></p>
			<div class="inline gcNumberWrapper ">
				<label for="crdNo2"><fmt:message key="checkout.payment.giftCardNumber"/></label>
				<input type="text" name="gcNumber" class="chkNumbersOnly" id="crdNo2" onkeypress="return isNumberOnly(event, 16);" />
			</div>
			<div class="inline eanWrapper">
				<label for="pin2"><fmt:message key="checkout.payment.pin"/></label>
				<input type="password" name="ean" class="chkNumbersOnly" id="pin2" onkeypress="return isNumberOnly(event, 4);" />
			</div>
			<button id="giftCardBalanceMyAccountBtn" ><fmt:message key="checkout.check.balance"/></button>
			<%-- <input type="submit" value='<fmt:message key="checkout.check.balance"/>' class="submit-btn personal-submit-btn" id="giftCardBalanceMyAccountBtn"> --%>
		</form>
	</div>
	<div class="my-account-gift-card-balance-container">
		<div></div>
		<a href="javascript:void(0);" class="my-account-check-another"><fmt:message key="checkout.check.another.card"/></a>
	</div>
	<div class="giftcardTooltip-wrapper">
		<!-- <a id="giftcardTooltip" href="javascript:void(0);" data-target="#checkoutGiftCardHelp" data-toggle="modal" tabindex="0" data-original-title="" title="" class="">gift card help</a> -->
		<a id="giftcardTooltip" href="javascript:void(0)" data-toggle="popover" data-original-title="" title=""><fmt:message key="checkout.payment.giftCardHelp"/></a>
		
		<!-- <div class="giftcardTooltippopover popover fade bottom in" role="tooltip" data-toggle="popover" style="display: none;" data-original-title="" title="">
			<div class="arrow"></div>				
		</div> -->
	</div>	
</div>