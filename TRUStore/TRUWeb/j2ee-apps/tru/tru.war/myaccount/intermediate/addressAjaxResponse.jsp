<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>

	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	

	<dsp:getvalueof var="isEditSavedAddress" vartype="java.lang.boolean" value="false" />
	<dsp:getvalueof var="fromSuggestedAddress" vartype="java.lang.boolean" value="false" />
	<dsp:droplet name="Switch">
		<dsp:param name="value" param="fromSuggestedAddress" />
		<dsp:oparam name="true">
			<dsp:getvalueof var="fromSuggestedAddress" vartype="java.lang.boolean" value="true" />
			<dsp:getvalueof var="nickName" param="nickName" />
			<dsp:getvalueof var="firstName" param="firstName" />
			<dsp:getvalueof var="lastName" param="lastName" />
			<dsp:getvalueof var="address1" param="address1" />
			<dsp:getvalueof var="address2" param="address2" />
			<dsp:getvalueof var="city" param="city" />
			<dsp:getvalueof var="state" param="state" />
			<dsp:getvalueof var="country" param="country" />
			<dsp:getvalueof var="postalCode" param="postalCode" />
			<dsp:getvalueof var="phoneNumber" param="phoneNumber" />
			
			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="Profile.secondaryAddresses" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="addressKey" param="key" />
					<c:if test="${nickName eq addressKey}">
						<dsp:getvalueof var="isEditSavedAddress" vartype="java.lang.boolean" value="true" />
						<dsp:getvalueof var="editNickName" param="nickName" />
					</c:if> 
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="default">
			<%--Edit Address check starts --%>
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" param="editNickName" />
				<dsp:oparam name="false">
					<dsp:getvalueof var="editNickName" param="editNickName" />
					<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="Profile.secondaryAddresses" />
						<dsp:param name="elementName" value="editAddress" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="addressKey" param="key" />
							<c:if test="${editNickName eq addressKey}">
								<dsp:getvalueof var="isEditSavedAddress" vartype="java.lang.boolean" value="true" />
								<dsp:getvalueof var="editAddress" param="editAddress" />
							</c:if> 
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
			<%--Edit Address check ends --%>
		</dsp:oparam>
	</dsp:droplet>
	
	<div class="modal-content sharp-border">
    	<div class="my-account-add-address-overlay">
			<div class="row">
		        <div class="col-md-5 col-md-offset-7 your-addresses-background">
		            <div class="row top-margin">
		                <div class="col-md-1 col-md-offset-10">
		                    <a href="javascript:void(0)" title="close" data-dismiss="modal" role="button"> <span class="clickable"><span class="sprite-icon-x-close"></span></span></a>
		                </div>
		            </div>
		        </div>
		    </div>
			<div class="row">
				<div class="col-md-7 set-height">
					<form id="myAccountAddressForm" class="JSValidation" method="POST" onsubmit="javascript: return onSubmitAddress();">
			                        <div class="row">
			                            <div class="col-md-12">
			                                <h2 class="myaccount-addressInfo-header"><fmt:message key="myaccount.address.information" /></h2>
			                                <p><fmt:message key="myaccount.asterisk" />&nbsp;<fmt:message key="myaccount.shipping.availability" /></p>
			                            </div>
			                        </div>
			                      
			                        <div class="tse-scrollable"><div class="tse-scrollbar" style="display: none;"><div class="drag-handle visible"></div></div><div class="tse-scrollbar" style="display: none;"><div class="drag-handle visible"></div></div>
			                            <div class="tse-scroll-content" style="width: 503px; height: 380px;">
			                                <div class="tse-content">
			                                <p class="global-error-display"></p>
			                                    <div class="row">
			                                        <div class="col-md-9">
			                                        	<input type="hidden" name="addressValidated" value="false" />
			                                        	<input type="hidden" name="isEditSavedAddress" value="${isEditSavedAddress}" />
			                                        	
			                                        	  <dsp:getvalueof var="editCountry" value="${editAddress.country}"/>
			                                        	  <dsp:getvalueof var="editState" value="${editAddress.state}"/>
			                                        	
											             <c:choose>
											                 <c:when test="${(isEditSavedAddress eq true && (editCountry ne 'US'))}">
											                 	<c:if test="${fromSuggestedAddress ne true}">  
													                 <label class="avenir-heavy" for="editAddresscountry">
													                        <span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span> <fmt:message key="myaccount.address.country" />
													                 </label>  
													                 <div class="select-wrapper">                                                      	                                         	
													                      <select name="editAddresscountry" id="editAddresscountry" onchange="onCountryChangeInAddress();">
																			<dsp:droplet name="ForEach">
																			  <dsp:param name="array" bean="/atg/commerce/util/CountryList.places"/>
																			  <dsp:param name="elementName" value="billingCountry"/>
																			  <dsp:oparam name="output">
																				  <dsp:getvalueof var="billingCountry" param="billingCountry.code"></dsp:getvalueof>
																				  <option value='<dsp:valueof param="billingCountry.code" />'
												                                        <dsp:droplet name="/atg/dynamo/droplet/Compare">
												                                                <dsp:param name="obj1" param="billingCountry.code"/>
												                                                <dsp:getvalueof var="billingAddressCountry" value="${address.country}"></dsp:getvalueof>
												                                                <dsp:param name="obj2" value="${editCountry}"/>		                                                	
												                                                <dsp:oparam name="equal">
												                                                    selected="selected"
												                                                </dsp:oparam>
												                                        </dsp:droplet> >
												                                       <dsp:valueof param="billingCountry.displayName" />
												                                   </option>
																			  </dsp:oparam>
																			</dsp:droplet>
																		</select>
																	</div>
																</c:if>
															</c:when>
														</c:choose>
                                     	
			                                            <label class="avenir-heavy" for="nkName">
		                                                    <span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span> <fmt:message key="myaccount.address.nickname" />
		                                                </label>
			                                            <c:choose>
			                                            	<c:when test="${fromSuggestedAddress eq true}">
			                                            		<c:choose>
			                                            			<c:when test="${isEditSavedAddress eq true}">
			                                            				<input type="text" name="editNickName" value="${editNickName}" disabled="disabled" />   
																		<input type="hidden" name="nickName" value="${editNickName}" /> 
			                                            			</c:when>
			                                            			<c:otherwise>
			                                            				<input type="text" name="nickName" id="nkName" value="${nickName}" maxlength="42"/>
			                                            			</c:otherwise>
			                                            		</c:choose>
			                                            	</c:when>
			                                            	<c:when test="${isEditSavedAddress eq true }">
			                                            		<input  type="text" name="editNickName" value="${editNickName}" disabled="disabled" />   
																<input type="hidden" name="nickName" value="${editNickName}" />                                            	
			                                            	</c:when>
			                                            	<c:otherwise>
			                                            		<input type="text" name="nickName" id="nkName" value="" placeholder="(Home, Office)" maxlength="42"/>
			                                            	</c:otherwise>
			                                            </c:choose>
			                                            <label class="avenir-heavy" for="fstName">
		                                                    <span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span> <fmt:message key="myaccount.address.firstname" />
		                                                </label>
			                                            <c:choose>
			                                            	<c:when test="${fromSuggestedAddress eq true}">
			                                            		<input type="text" name="firstName" id="fstName" value="${firstName}" maxlength="30"/>
			                                            	</c:when>
			                                            	<c:when test="${isEditSavedAddress eq true }">
																<input type="text" name="firstName" id="fstName" value="${editAddress.firstName}" maxlength="30"/>                                            	
			                                            	</c:when>
			                                            	<c:otherwise>
			                                            		<input type="text" name="firstName" id="fstName" value="" maxlength="30"/>
			                                            	</c:otherwise>
			                                            </c:choose>
			                                            <label class="avenir-heavy" for="lstName">
			                                                <span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span> <fmt:message key="myaccount.address.lastname" />
			                                            </label>
			                                            <c:choose>
			                                            	<c:when test="${fromSuggestedAddress eq true}">
			                                            		<input type="text" name="lastName" id="lstName" value="${lastName}" maxlength="30"/>
			                                            	</c:when>
			                                            	<c:when test="${isEditSavedAddress eq true }">
			                                            		<input type="text" name="lastName" id="lstName" value="${editAddress.lastName}" maxlength="30"/>
			                                            	</c:when>
			                                            	<c:otherwise>
			                                            		<input type="text" name="lastName" id="lstName" value="" maxlength="30"/>
			                                            	</c:otherwise>
			                                            </c:choose>
			                                            <label class="avenir-heavy" for="addr1">
			                                                <span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span> <fmt:message key="myaccount.address.addressOne" />
			                                            </label>
			                                            <c:choose>
			                                            	<c:when test="${fromSuggestedAddress eq true}">
			                                            		<input type="text" name="address1" id="addr1" value="${address1}" maxlength="50"/>
			                                            	</c:when>
			                                            	<c:when test="${isEditSavedAddress eq true }">
			                                            		<input type="text" name="address1" id="addr1" value="${editAddress.address1}" maxlength="50"/>
			                                            	</c:when>
			                                            	<c:otherwise>
			                                            		<input type="text" name="address1" id="addr1" value="" maxlength="50"/>
			                                            	</c:otherwise>
			                                            </c:choose>
			                                            <label class="avenir-heavy" for="aptOrSuite">
			                                                <fmt:message key="myaccount.address.apt.suite.optional" />
			                                            </label>
			                                            <c:choose>
			                                            	<c:when test="${fromSuggestedAddress eq true}">
			                                            		<input type="text" name="address2" id="aptOrSuite" value="${address2}" maxlength="50"/>
			                                            	</c:when>
			                                            	<c:when test="${isEditSavedAddress eq true }">
			                                            		<input type="text" name="address2" id="aptOrSuite" value="${editAddress.address2}" maxlength="50"/>
			                                            	</c:when>
			                                            	<c:otherwise>
			                                            		<input type="text" name="address2" id="aptOrSuite" value="" maxlength="50"/>
			                                            	</c:otherwise>
			                                            </c:choose>
			                                            <label class="avenir-heavy" for="city1">
			                                                <span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span> <fmt:message key="myaccount.address.city" />
			                                            </label>
			                                            <c:choose>
			                                            	<c:when test="${fromSuggestedAddress eq true}">
			                                            		<input type="text" name="city" id="city1" value="${city}" maxlength="30"/>
			                                            	</c:when>
			                                            	<c:when test="${isEditSavedAddress eq true }">
			                                            		<input type="text" name="city" id="city1" value="${editAddress.city}" maxlength="30"/>
			                                            	</c:when>
			                                            	<c:otherwise>
			                                            		<input type="text" name="city" id="city1" value="" maxlength="30"/>
			                                            	</c:otherwise>
			                                            </c:choose>
			                                            <c:if test="${isEditSavedAddress eq true && (editCountry ne 'US')}">      
																<dsp:getvalueof var="displayInlineblock" value="display-inlineblock"></dsp:getvalueof>														
															</c:if>	
			                                            <label  class="avenir-heavy creditCardStateLabel"  for="state">
			                                                <span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span> <fmt:message key="myaccount.address.state.province" />
			                                            </label>
			                                            <label  class="avenir-heavy creditCardOtherStateLabel ${displayInlineblock}"  for="state">
			                                                Other State\Province
			                                            </label>
			                                            <br>
			                                            <div class="select-wrapper creditCardOtherStateWrapper">
			                                         	<c:choose>
			                                         		<c:when test="${fromSuggestedAddress eq true}">
			                                            		<select name="state" id="state" value="${state}" onchange="onShippingSelectedStateChange(this)">
				                                                    <option value="">please select</option>
				                                                    <dsp:droplet name="ForEach">
																	  <dsp:param name="array" bean="/atg/commerce/util/StateList_US.places"/>
																	  <dsp:param name="elementName" value="state"/>
																	  <dsp:oparam name="output">
																		  <dsp:getvalueof var="stateCode" param="state.code"></dsp:getvalueof>
																		  	<option value='<dsp:valueof param="state.code" />'
									                                              <dsp:droplet name="/atg/dynamo/droplet/Compare">
									                                                      <dsp:param name="obj1" param="state.code"/>
									                                                      <dsp:param name="obj2" value="${state}"/>
									                                                      <dsp:oparam name="equal">
									                                                          selected="selected"
									                                                      </dsp:oparam>
									                                              </dsp:droplet> >
									                                              <dsp:valueof param="state.code"/>
									                                         </option>
																	  </dsp:oparam>
																</dsp:droplet>
				                                                </select>
			                                            	</c:when>
			                                            	<c:when test="${isEditSavedAddress eq true && (editCountry ne 'US')}">      
																<select name="state" id="state" onchange="onShippingSelectedStateChange(this)">
					                                                <option value="">please select</option>					                                                	
				                                                    <option value="other" selected="selected">other</option>
				                                                </select>	
				                                               <c:choose>
																	<c:when test="${editState eq 'NA'}">
																		<input type="text" name="otherState" class="${displayInlineblock}" id="otherState" maxlength="20" value=""/>
																	</c:when>
																	<c:otherwise>
																		 <input type="text" name="otherState" class="${displayInlineblock}" id="otherState" maxlength="20" value="${editState}"/>
																	</c:otherwise>
																</c:choose>
															</c:when>			                                            	
															<c:when test="${isEditSavedAddress eq true }">	
				                                                <select name="state" id="state" value="${editAddress.state}" onchange="onShippingSelectedStateChange(this)">
				                                                    <option value="">please select</option>
				                                                    <dsp:droplet name="ForEach">
																	  <dsp:param name="array" bean="/atg/commerce/util/StateList_US.places"/>
																	  <dsp:param name="elementName" value="state"/>
																	  <dsp:oparam name="output">
																		  <dsp:getvalueof var="stateCode" param="state.code"></dsp:getvalueof>
																		  	<option value='<dsp:valueof param="state.code" />'
									                                              <dsp:droplet name="/atg/dynamo/droplet/Compare">
									                                                      <dsp:param name="obj1" param="state.code"/>
									                                                      <dsp:param name="obj2" value="${editAddress.state}"/>
									                                                      <dsp:oparam name="equal">
									                                                          selected="selected"
									                                                      </dsp:oparam>
									                                              </dsp:droplet> >
									                                              <dsp:valueof param="state.code"/>
									                                         </option>
																	  </dsp:oparam>
																</dsp:droplet>
				                                                    <!-- <option value="other">other</option> -->
				                                                </select>
				                                          		 <input type="text" name="otherState" id="otherState" maxlength="20" value="NA"/>
															</c:when>															 
															<c:otherwise>
					                                            <select name="state" id="state" onchange="onShippingSelectedStateChange(this)">
					                                                <option value="">please select</option>
					                                                	<dsp:droplet name="ForEach">
																		  <dsp:param name="array" bean="/atg/commerce/util/StateList_US.places"/>
																		  <dsp:param name="elementName" value="state"/>
																		  <dsp:oparam name="output">
																			  <dsp:getvalueof var="stateCode" param="state.code"></dsp:getvalueof>
																			  <option value="${stateCode}"><dsp:valueof param="state.code" /></option>
																		  </dsp:oparam>
																		</dsp:droplet>
				                                                    <!-- <option value="other">other</option> -->
				                                                </select>
				                                                <input type="text" name="otherState" id="otherState" maxlength="20" value="NA"/>
				                                           </c:otherwise>
			                                           </c:choose>
												<c:choose>
													<c:when test="${fromSuggestedAddress eq true}">
														<input type="hidden" name="country" id="changedCountry" value="US" /> 
													</c:when>
													<c:when
														test="${isEditSavedAddress eq true && (editCountry ne 'US')}">
														<input type="hidden" name="country" id="changedCountry" value="${editCountry}" />
													</c:when>
													<c:otherwise>
														<input type="hidden" name="country" id="changedCountry" value="US" />														
													</c:otherwise>
												</c:choose>


											</div>
			                                            
			                                            <label class="avenir-heavy" for="shippingZip">
			                                                <span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span> <fmt:message key="myaccount.address.zip.postal" />
			                                            </label>
			                                            <c:choose>
			                                            	<c:when test="${fromSuggestedAddress eq true}">
			                                            		<input type="text" name="postalCode" value="${postalCode}" id="shippingZip" maxlength="10" />
			                                            	</c:when>
			                                            	<c:when test="${isEditSavedAddress eq true }">
			                                            		<input type="text" name="postalCode" value="${editAddress.postalCode}" id="shippingZip" maxlength="10" />
			                                            	</c:when>
			                                            	<c:otherwise>
			                                            		<input type="text" name="postalCode" value="" id="shippingZip" maxlength="10" />
			                                            	</c:otherwise>
			                                            </c:choose>
			                                            <label class="avenir-heavy" for="telephone">
			                                                <span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span> <fmt:message key="myaccount.address.telephone" />
			                                            </label>
			                                            <c:choose>
			                                            	<c:when test="${fromSuggestedAddress eq true}">
			                                            		<input type="text" name="phoneNumber" id="telephone" value="${phoneNumber}" class="splCharCheck"  onkeypress="return isPhoneNumberFormat(event)" maxlength="20"/>
			                                            	</c:when>
			                                            	<c:when test="${isEditSavedAddress eq true }">
			                                            		<input type="text" name="phoneNumber" id="telephone" value="${editAddress.phoneNumber}" class="splCharCheck"  onkeypress="return isPhoneNumberFormat(event)" maxlength="20"/>
			                                            	</c:when>
			                                            	<c:otherwise>
			                                            		<input type="text" name="phoneNumber" id="telephone" value="" class="splCharCheck"  onkeypress="return isPhoneNumberFormat(event)" maxlength="20"/>
			                                            	</c:otherwise>
			                                            </c:choose>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                        <div class="row top-border attach-to-bottom">
			                            <div class="col-md-12">
			                            	<input type="submit" id="submitAddressId" priority="-10" name="addShippingAddress" value="submit" style="display: none;" />
			                                <button onclick="return modifyAddress();"><fmt:message key="myaccount.save" /></button>
			                            </div>
			                        </div>
					</form>
			       </div>
		       	<dsp:include page="/myaccount/intermediate/myAccountProfileAddresses.jsp"/>
			</div>
		</div>
	</div>
</dsp:page>