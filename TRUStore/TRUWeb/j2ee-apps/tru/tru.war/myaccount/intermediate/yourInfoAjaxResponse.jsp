<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
 	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
 	<dsp:getvalueof bean="Profile.login" var="oldEmail"/>
 	<dsp:getvalueof var="info" param="info" />
 	<c:choose>
	 	<c:when test="${info eq 'name'}">
	 		<dsp:setvalue bean="ProfileFormHandler.value.firstName" paramvalue="firstName" />
			<dsp:setvalue bean="ProfileFormHandler.value.lastName" paramvalue="lastName" />
			<dsp:setvalue  value="false" bean="ProfileFormHandler.emailUpdate"/> 
			<dsp:setvalue  bean="ProfileFormHandler.updateProfileInfo" value="submit"  />  	
		</c:when>
		<c:when test="${info eq 'email'}">
	 		<dsp:setvalue bean="ProfileFormHandler.value.email" paramvalue="new_email" />
	 		<dsp:setvalue bean="ProfileFormHandler.confirmEmail" paramvalue="con_email" />
	 		<dsp:setvalue value="true" bean="ProfileFormHandler.emailUpdate"/>
	 		<dsp:setvalue value="${oldEmail}" bean="ProfileFormHandler.userOldEmailAddress"/>  	
	 		<dsp:setvalue  bean="ProfileFormHandler.updateProfileInfo" value="submit"  />  	 	
	 	</c:when> 	
	 	<c:when test="${info eq 'password'}">
	 		<dsp:setvalue bean="ProfileFormHandler.value.oldpassword" paramvalue="current_password" />
	 		<dsp:setvalue bean="ProfileFormHandler.value.password" paramvalue="password" /> 	
	 		<dsp:setvalue bean="ProfileFormHandler.value.confirmpassword" paramvalue="confirm_password" /> 		
	 		<dsp:setvalue  bean="ProfileFormHandler.changePassword" value="submit"  />  	
	 	</c:when>
 	</c:choose>
 	
 	
 	<dsp:droplet name="Switch">
	<dsp:param bean="ProfileFormHandler.formError" name="value"/>
	<dsp:oparam name="true">
		 <dsp:droplet name="ErrorMessageForEach">
	        <dsp:param name="exceptions" bean="ProfileFormHandler.formExceptions"/>
			<dsp:oparam name="outputStart">
					Following are the form errors:
			</dsp:oparam>
			<dsp:oparam name="output">
					 <dsp:valueof param="message"/>
			</dsp:oparam>
		</dsp:droplet>
	</dsp:oparam>	
	<dsp:oparam name="false">	
		<dsp:include page="/myaccount/myAccountUserDetails.jsp"/>			
	</dsp:oparam>
	</dsp:droplet>
 	

 </dsp:page>