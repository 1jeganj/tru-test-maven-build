<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/com/tru/common/droplet/DisplayMonthAndYearDroplet"/>
	<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
	<dsp:getvalueof var="isEditSavedCard" vartype="java.lang.boolean" value="false" />
	<dsp:getvalueof var="formID" param="formID"/>	
	<dsp:getvalueof var="profileId" bean="Profile.id"/>
	<dsp:getvalueof var="currencyCode" bean="TRUConfiguration.currencyCode"/>
	
	<div class="col-md-6 set-height" id="addOrEditId" style="height: 456px;">
	
		<dsp:getvalueof var="isEditSavedCard" vartype="java.lang.boolean" value="false" />
		<dsp:getvalueof var="fromSuggestedAddress" vartype="java.lang.boolean" value="false" />
		<dsp:droplet name="Switch">
			<dsp:param name="value" param="fromSuggestedAddress" />
			<dsp:oparam name="true">
				<dsp:getvalueof var="fromSuggestedAddress" vartype="java.lang.boolean" value="true" />
				
				<dsp:getvalueof var="nameOnCard" param="nameOnCard" />
				<dsp:getvalueof var="creditCardNumber" param="creditCardNumber" />
				<dsp:getvalueof var="expirationMonth" param="expirationMonth" />
				<dsp:getvalueof var="expirationYear" param="expirationYear" />
				<dsp:getvalueof var="expirationMonth" param="expirationMonth" />
				
				<dsp:getvalueof var="nickName" param="nickName" scope="request" />
				<dsp:getvalueof var="firstName" param="firstName" scope="request" />
				<dsp:getvalueof var="lastName" param="lastName" scope="request" />
				<dsp:getvalueof var="address1" param="address1" scope="request" />
				<dsp:getvalueof var="address2" param="address2" scope="request" />
				<dsp:getvalueof var="city" param="city" scope="request" />
				<dsp:getvalueof var="state" param="state" scope="request" />
				<dsp:getvalueof var="country" param="country" scope="request" />
				<dsp:getvalueof var="postalCode" param="postalCode" scope="request" />
				<dsp:getvalueof var="phoneNumber" param="phoneNumber" scope="request" />
				
				<dsp:droplet name="IsEmpty">
					<dsp:param name="value" param="editCardNickName" />
					<dsp:oparam name="false">
						<dsp:getvalueof var="editCardNickName" param="editCardNickName" />
						<dsp:droplet name="ForEach">
							<dsp:param name="array" bean="Profile.creditCards" />
							<dsp:param name="elementName" value="creditCard" />
							<dsp:oparam name="output">
							<dsp:getvalueof var="cardNickname" param="key" />
							<c:if test="${cardNickname eq editCardNickName}">
								<dsp:getvalueof var="isEditSavedCard" vartype="java.lang.boolean" value="true" />
								<dsp:getvalueof var="creditCard" param="creditCard" />
							</c:if> 
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
				</dsp:droplet>
			</dsp:oparam>
			<dsp:oparam name="default">
				 <%--Edit Credit card check starts --%>
				<dsp:droplet name="IsEmpty">
					<dsp:param name="value" param="creditCardNickName" />
					<dsp:oparam name="false">
						<dsp:getvalueof var="editCardNickName" param="creditCardNickName" />
						<dsp:droplet name="ForEach">
							<dsp:param name="array" bean="Profile.creditCards" />
							<dsp:param name="elementName" value="creditCard" />
							<dsp:oparam name="output">
							<dsp:getvalueof var="nickname" param="key" />
								<c:if test="${nickname eq editCardNickName}">
									<dsp:getvalueof var="isEditSavedCard" vartype="java.lang.boolean" value="true" />
									<dsp:getvalueof var="creditCard" param="creditCard" />
								</c:if> 
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
				</dsp:droplet>
				<%--Edit Credit card check ends --%>
			</dsp:oparam>
		</dsp:droplet>
		 <input type="hidden" name="currencyCode" id="currencyCode" value="${currencyCode}"/>
		 <input type="hidden" name="profileId" id="profileId" value="${profileId}"/>
		 <input type="hidden" name="cardinalToken" id="cardinalToken"/>
		 <input type="hidden" value="" name="caBinNum" id="caBinNum" />
		 <input type="hidden" value="" name="caLength" id="caLength" />
		 		 
		<form id="addOrEditCreditCard" method="POST" class="JSValidation"  onsubmit="javascript:return startTokenizationMyAccount();"> <!-- onSubmitAddCreditCard() -->
			<input type="hidden" name="editCardNickName" value="${editCardNickName}" id="editCardNickNameID" />
			<input type="hidden" name="isEditSavedCard" id="isEditSavedCard" value="${isEditSavedCard}"/>
			
			<c:choose>
				<c:when test="${isEditSavedCard eq true }">
					<div class="row left-padding">
						<div class="col-md-12">
							<h2><fmt:message key="myaccount.edit.credit.card" /></h2>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="row left-padding">
						<div class="col-md-12">
							<h2><fmt:message key="myaccount.add.credit.card" /></h2>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
			 <div class="tse-scrollable" ><div class="tse-scrollbar" style="display: none;"><div class="drag-handle visible"></div></div>
                 <div class="tse-scroll-content" >
                     <div class="tse-content">
                      <p class="global-error-display"></p>
						<div class="row left-padding">
							<div class="col-md-9">
								<div class="row">
									<c:choose>
										<c:when test="${fromSuggestedAddress eq true}">
											<c:choose>
												<c:when test="${isEditSavedCard eq true }">
													<input type="hidden" name="nameOnCard" id="nameOnCard" value="${creditCard.nameOnCard}" maxlength="40"/>
												</c:when>
												<c:otherwise>
													<div class="col-md-12">
														<label class="avenir-heavy" for="nameOnCard">
															<fmt:message key="myaccount.name.on.card" />
														</label>
													</div>
													<div class="col-md-8">
														<input type="text" name="nameOnCard" id="nameOnCard" value="${nameOnCard}" maxlength="40"/>
													</div>
												</c:otherwise>
											</c:choose>
										</c:when>
                                    	<c:when test="${isEditSavedCard eq true }">
                                    		<input type="hidden" name="nameOnCard" id="nameOnCard" value="${creditCard.nameOnCard}" maxlength="40"/>
                                    	</c:when>
                                    	<c:otherwise>
											<div class="col-md-12">
												<label class="avenir-heavy" for="nameOnCard">
													<fmt:message key="myaccount.name.on.card" />
												</label>
											</div>
											<div class="col-md-8">
												<input type="text" name="nameOnCard" id="nameOnCard" value="" maxlength="40"/>
											</div>
										</c:otherwise>
									</c:choose>
								</div>
								<div class="row">
									<div class="col-md-12">
										<label class="avenir-heavy" for="creditCardNumber">
											<fmt:message key="myaccount.card.number" />
										</label>
									</div>
								</div>
								<div class="row">
									
										<dsp:getvalueof var="cardType" value="${creditCard.creditCardType}" />
										<input id="creditCardType" name="creditCardType" type="hidden" value="${cardType}" />
										<c:choose>
											<c:when test="${fromSuggestedAddress eq true}">
												<c:choose>
													<c:when test="${isEditSavedCard eq true }">
														<div class="col-md-2">
															<c:choose>
																<c:when test="${cardType eq 'visa'}">
																	<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Visa-Thumb-No-Outline.jpg" alt="visa card">
																</c:when>
																<c:when test="${cardType eq 'discover'}">
																	<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Discover-Thumb.jpg" alt="discover card">
																</c:when>
																<c:when test="${cardType eq 'masterCard'}">
																	<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Mastercard-Thumb-No-Outline.jpg" alt="master card">
																</c:when>
																<c:when test="${cardType eq 'americanExpress'}">
																	<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Amex-Thumb-No-Outline.jpg" alt="amex">
																</c:when>
																<c:when test="${cardType eq 'RUSPrivateLabelCard'}">
																	<img class="credit-card" src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-1.jpg" alt="PLCCCard">
																</c:when>
																<c:when test="${cardType eq 'RUSCoBrandedMasterCard'}">
																	<img class="credit-card" src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-2.jpg" alt="RUSCoBCard">
																</c:when>
																<c:otherwise>
																</c:otherwise>
															</c:choose>
														</div>
														<div class="col-md-8">
															<p class="avenir-light">
																<dsp:valueof value="${creditCard.creditCardNumber}"	converter="creditcard" maskcharacter="x" />
															</p>
															<input type="hidden" name="tokenCreditCardNumber" id="creditCardNumber" value="${creditCard.creditCardNumber}" />
														</div>
													</c:when>
													<c:otherwise>
														<div class="col-md-8">
															<input type="text" id="creditCardNumber"  name="creditCardNumber" value="${creditCardNumber}"  class="card-number chkNumbersOnly"/>
														</div>
													</c:otherwise>
												</c:choose>
											</c:when>
											<c:when test="${isEditSavedCard eq true }">
												<div class="col-md-2">
													<c:choose>
														<c:when test="${cardType eq 'visa'}">
															<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Visa-Thumb-No-Outline.jpg" alt="visa card">
														</c:when>
														<c:when test="${cardType eq 'discover'}">
															<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Discover-Thumb.jpg" alt="discover card">
														</c:when>
														<c:when test="${cardType eq 'masterCard'}">
															<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Mastercard-Thumb-No-Outline.jpg" alt="master card">
														</c:when>
														<c:when test="${cardType eq 'americanExpress'}">
															<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Amex-Thumb-No-Outline.jpg" alt="amex">
														</c:when>
														<c:when test="${cardType eq 'RUSPrivateLabelCard'}">
															<img class="credit-card" src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-1.jpg" alt="PLCCCard">
														</c:when>
														<c:when test="${cardType eq 'RUSCoBrandedMasterCard'}">
															<img class="credit-card" src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-2.jpg" alt="RUSCoBCard">
														</c:when>
														<c:otherwise>
														</c:otherwise>
													</c:choose>
												</div>
												<div class="col-md-8">
													<p class="avenir-light">
														<dsp:valueof value="${creditCard.creditCardNumber}"	converter="creditcard" maskcharacter="x" />
													</p>
													<input type="hidden" id="creditCardNumber" name="tokenCreditCardNumber" value="${creditCard.creditCardNumber}" onkeypress="return isNumberOnly(event,16)" />
												</div>
											</c:when>
											<c:otherwise>
												<div class="col-md-8">
													<input type="text" id="creditCardNumber" name="creditCardNumber" value="" onkeypress="return isNumberOnly(event,16)" class="card-number chkNumbersOnly"/>
												</div>
											</c:otherwise>
										</c:choose>
									<div class="col-md-1 col-md-pull-1 display-card">
										<img class="valid-credit-card" src="${TRUImagePath}images/Payment_Green-Checkmark.jpg" alt="payment check mark">
									</div>
									<div class="col-md-1 col-md-pull-1 display-card">
										<img class="valid-credit-card credit-card" src="${TRUImagePath}images/Payment_Small-Mastercard-Thumb-No-Outline.jpg" alt="master card">
									</div>								
								</div>
								<label class="avenir-heavy" for="expirationMonth"> 
									<fmt:message key="myaccount.card.expires" />
								</label>
								<div class="row">
									<div class="col-md-5">
										<div class="select-wrapper">
										<c:choose>
											<c:when test="${cardType eq 'RUSPrivateLabelCard'}">
												<select name="expirationMonth" id="expirationMonth" class="grayedOut" disabled="disabled">											
													<option value="">MM</option>
												</select>
											</c:when>
											<c:otherwise>
												<c:choose>
													<c:when test="${fromSuggestedAddress eq true }">	
														<select name="expirationMonth" id="expirationMonth">
															<option value="">MM</option>
															<dsp:droplet name="DisplayMonthAndYearDroplet">
																<dsp:param name="monthList" value="monthList"/>
																<dsp:oparam name="output">
																	<dsp:droplet name="ForEach">
																		<dsp:param name="array" param="monthList"/>
																		<dsp:param name="elementName" value="month"/>
																		<dsp:oparam name="output">
																			<dsp:getvalueof var="month" param="month" />
																			<option value='<dsp:valueof param="month" />'
									                                              <dsp:droplet name="/atg/dynamo/droplet/Compare">
									                                                      <dsp:param name="obj1" value="${month}"/>
									                                                      <dsp:param name="obj2" value="${expirationMonth}"/>
									                                                      <dsp:oparam name="equal">
									                                                          selected="selected"
									                                                      </dsp:oparam>
									                                              </dsp:droplet> >
									                                              <dsp:valueof param="month"/>
									                                         </option>
									                                         </dsp:oparam>
																		</dsp:droplet>
																	</dsp:oparam>
																</dsp:droplet>
															</select>
														</c:when>
													<c:when test="${isEditSavedCard eq true }">	
														<select name="expirationMonth" id="expirationMonth">
															<option value="">MM</option>
															<dsp:droplet name="DisplayMonthAndYearDroplet">
																<dsp:param name="monthList" value="monthList"/>
																<dsp:oparam name="output">
																	<dsp:droplet name="ForEach">
																		<dsp:param name="array" param="monthList"/>
																		<dsp:param name="elementName" value="month"/>
																		<dsp:oparam name="output">
																			<dsp:getvalueof var="month" param="month" />
																			<option value='<dsp:valueof param="month" />'
									                                              <dsp:droplet name="/atg/dynamo/droplet/Compare">
									                                                      <dsp:param name="obj1" value="${month}"/>
									                                                      <dsp:param name="obj2" value="${creditCard.expirationMonth}"/>
									                                                      <dsp:oparam name="equal">
									                                                          selected="selected"
									                                                      </dsp:oparam>
									                                              </dsp:droplet> >
									                                              <dsp:valueof param="month"/>
									                                         </option>
									                                         </dsp:oparam>
																		</dsp:droplet>
																	</dsp:oparam>
																</dsp:droplet>
															</select>
														</c:when>
														<c:otherwise>
															<select name="expirationMonth" id="expirationMonth">												
																<option value="">MM</option>
																<dsp:droplet name="DisplayMonthAndYearDroplet">
																	<dsp:param name="monthList" value="monthList"/>
																	<dsp:oparam name="output">
																		<dsp:droplet name="ForEach">
																			<dsp:param name="array" param="monthList"/>
																			<dsp:param name="elementName" value="month"/>
																			<dsp:oparam name="output">
																				<dsp:getvalueof var="month" param="month" />
																				<option value="${month}"><dsp:valueof param="month"/></option>
																			</dsp:oparam>
																		</dsp:droplet>
																	</dsp:oparam>
																</dsp:droplet>
															</select>
														</c:otherwise>
													</c:choose>
											</c:otherwise>
										</c:choose>
										</div>
									</div>
									<div class="col-md-5 col-md-offset-1">
										<div class="select-wrapper">
										<c:choose>
											<c:when test="${cardType eq 'RUSPrivateLabelCard'}">
												<select name="expirationYear" id="expirationYear" class="grayedOut" disabled="disabled">											
													<option value="">YYYY</option>
												</select>
											</c:when>
											<c:otherwise>
											<c:choose>
												<c:when test="${fromSuggestedAddress eq true }">	
													<select name="expirationYear" id="expirationYear">
														<option value="">YYYY</option>
														<dsp:droplet name="DisplayMonthAndYearDroplet">
															<dsp:param name="yearList" value="yearList"/>
															<dsp:oparam name="output">
																<dsp:droplet name="ForEach">
																	<dsp:param name="array" param="yearList"/>
																	<dsp:param name="elementName" value="year"/>
																	<dsp:oparam name="output">
																		<dsp:getvalueof var="year" param="year" />
																		<dsp:getvalueof var="creditCardExpYr" value="${expirationYear}"></dsp:getvalueof>
																		<option value="${year}"
																		<c:if test="${year eq creditCardExpYr}">
																			selected="selected"
																		</c:if>
																		>
								                                              <dsp:valueof param="year"/>
								                                         </option>
																	</dsp:oparam>
																</dsp:droplet>
															</dsp:oparam>
														</dsp:droplet>
													</select>
												</c:when>
												<c:when test="${isEditSavedCard eq true }">	
													<select name="expirationYear" id="expirationYear">
														<option value="">YYYY</option>
														<dsp:droplet name="DisplayMonthAndYearDroplet">
															<dsp:param name="yearList" value="yearList"/>
															<dsp:oparam name="output">
																<dsp:droplet name="ForEach">
																	<dsp:param name="array" param="yearList"/>
																	<dsp:param name="elementName" value="year"/>
																	<dsp:oparam name="output">
																		<dsp:getvalueof var="year" param="year" />
																		<dsp:getvalueof var="creditCardExpYr" value="${creditCard.expirationYear}"></dsp:getvalueof>
																		<option value="${year}"
																		<c:if test="${year eq creditCardExpYr}">
																			selected="selected"
																		</c:if>
																		>
								                                              <dsp:valueof param="year"/>
								                                         </option>
																	</dsp:oparam>
																</dsp:droplet>
															</dsp:oparam>
														</dsp:droplet>
													</select>
												</c:when>
												<c:otherwise>
												<select name="expirationYear" id="expirationYear">
														<option value="">YYYY</option>
														<dsp:droplet name="DisplayMonthAndYearDroplet">
															<dsp:param name="yearList" value="yearList"/>
															<dsp:oparam name="output">
																<dsp:droplet name="ForEach">
																	<dsp:param name="array" param="yearList"/>
																	<dsp:param name="elementName" value="year"/>
																	<dsp:oparam name="output">
																		<dsp:getvalueof var="year" param="year" />
																		<option value="${year}"><dsp:valueof param="year"/></option>
																	</dsp:oparam>
																</dsp:droplet>
															</dsp:oparam>
														</dsp:droplet>
													</select>
												</c:otherwise>
											</c:choose>
										</c:otherwise>
										</c:choose>
										</div>
									</div>
								</div>
								<br>
                                   <label class="avenir-heavy" for="creditCardCVVCode">
                                       <fmt:message key="myaccount.security.code" />
                                   </label>
                                   <div class="row">
                                       <div class="col-md-12">
                                       <dsp:getvalueof param="creditCardCVV" var="creditCardCVVFromSuggPage"></dsp:getvalueof>
                                       <c:choose>
	                                       <c:when test="${formID eq 'editUserEnteredAddressForCreditCard'}">
	                                       <dsp:getvalueof var="cvv" value="${creditCardCVVFromSuggPage}"></dsp:getvalueof>
	                                       </c:when>
	                                       <c:otherwise>
	                                       	<dsp:getvalueof var="cvv" value=""></dsp:getvalueof>
	                                       </c:otherwise>
                                       </c:choose>
                                       	<p><input type="password" name="creditCardCVV" value="${cvv}" maxlength="4" class="splCharCheck creditCardCVV" id="creditCardCVVCode"/></p>
                                           <%-- <p><input type="text" name="creditCardCVV_tmp" value="${cvv}" maxlength="4" class="splCharCheck creditCardCVV_tmp" id="creditCardCVVCode"/>
                                           <input type="hidden" name="creditCardCVV" id="creditCardCVV" value="${cvv}" maxlength="4" /></p> --%>
                                           <p class="top-margin-cvv"><a href="#" data-target="#creditCardModal" data-toggle="modal" class="creditCardModalClass"><fmt:message key="myaccount.whats.this" /></a>
                                           </p>
                                       </div>
                                   </div>
								<br>
								<dsp:getvalueof var="isEditCCAddressAvailable" value="false" />
								<dsp:getvalueof var="isBillingAddressAvailable" value="false"/>
								<dsp:droplet name="IsEmpty">
									<dsp:param name="value" bean="Profile.secondaryAddresses" />
									<dsp:oparam name="false">
										<dsp:getvalueof var="isBillingAddressFromAddressBook" vartype="java.lang.boolean" value="false"/>
										<dsp:getvalueof var="hasSecondaryAddressesForProfile" vartype="java.lang.boolean" value="false"/>
										<dsp:droplet name="ForEach">
											<dsp:param name="array" bean="Profile.secondaryAddresses" />
											<dsp:param name="elementName" value="secondaryAddress" />
											<dsp:oparam name="output">
												<dsp:droplet name="Switch">
													<dsp:param name="value" param="secondaryAddress.isBillingAddress" />
													<dsp:oparam name="true">
														<dsp:getvalueof var="isBillingAddressAvailable" value="true"/>
													</dsp:oparam>
												</dsp:droplet>
											</dsp:oparam>
										</dsp:droplet>
										
										<c:if test="${isBillingAddressAvailable eq true}">
										<label class="avenir-heavy" for="selectedCardBillingAddress">
											<fmt:message key="myaccount.select.billing.address" />
										</label>
										<br>
										<div class="row">
											<div class="col-md-12">
												<div class="select-wrapper-wide">
													<select name="selectedCardBillingAddress" id="selectedCardBillingAddress" class="select-billing-address blue" onchange="onBillingAddressChange();">
														<dsp:droplet name="ForEach">
															<dsp:param name="array" bean="Profile.secondaryAddresses" />
															<dsp:param name="elementName" value="secondaryAddress" />
															<dsp:oparam name="outputStart">
																    <option value="">please select</option>
																<dsp:getvalueof var="hasSecondaryAddressesForProfile" vartype="java.lang.boolean" value="true"/>
															</dsp:oparam>
															<dsp:oparam name="output">
																<dsp:getvalueof var="key" param="key" />
																<dsp:getvalueof var="secondaryAddress" param="secondaryAddress" />
																<dsp:getvalueof param="secondaryAddress.isBillingAddress" var="isBillingAddress" />
																<dsp:getvalueof param="secondaryAddress.address1" var="address1" />
																<c:if test="${isBillingAddress}">
																	<c:choose>
																		<c:when test="${formID eq 'editUserEnteredAddressForCreditCard'}">
																			<dsp:getvalueof var="selectedCardBillingAddress" param="selectedCardBillingAddress"/>
																			<c:choose>
																				<c:when	test="${key eq selectedCardBillingAddress}">
																					<dsp:getvalueof var="isEditCCAddressAvailable" value="true" />
																					<option selected="selected" value="${key}">${address1}</option>
																					<dsp:getvalueof var="isBillingAddressFromAddressBook" vartype="java.lang.boolean" value="true"/>
																				</c:when>
																				<c:otherwise>
																					<option value="${key}">${address1}</option>																				
																				</c:otherwise>
																			</c:choose>
																		</c:when>
																		<c:when test="${isEditSavedCard eq true }">
																			<c:choose>
																				<c:when	test="${creditCard.billingAddress.repositoryId eq secondaryAddress.repositoryId}">
																					<dsp:getvalueof var="isEditCCAddressAvailable" value="true" />
																					<option selected="selected" value="${key}">${address1}</option>
																					<dsp:getvalueof var="isBillingAddressFromAddressBook" vartype="java.lang.boolean" value="true"/>
																				</c:when>
																				<c:otherwise>
																					<option value="${key}">${address1}</option>																				
																				</c:otherwise>
																			</c:choose>
																		</c:when>
																		<c:otherwise>
																			<option value="${key}">${address1}</option>
																			<dsp:getvalueof var="isBillingAddressFromAddressBook" vartype="java.lang.boolean" value="true"/>
																		</c:otherwise>
																	</c:choose>
																</c:if>
															</dsp:oparam>
															<dsp:oparam name="outputEnd">
															<dsp:getvalueof var="lastOptionSelected" param="lastOptionSelected"/>
															<c:choose>
																<c:when test="${lastOptionSelected eq true and formID eq 'editUserEnteredAddressForCreditCard'}">
																	<option selected="selected" value=" "><fmt:message key="myaccount.add.dropdown.address" /></option>
																</c:when>
																<c:otherwise>
																	<option value=" "><fmt:message key="myaccount.add.dropdown.address" /></option>
																</c:otherwise>
															</c:choose>
																
															</dsp:oparam>
														</dsp:droplet>
													</select>
													
													<input type="hidden" name="isEditCCAddressAvailable" id="isEditCCAddressAvailable" value="${isEditCCAddressAvailable}" />
												</div>
											</div>
										</div>
										</c:if>
										<c:if test="${not isBillingAddressFromAddressBook and not lastOptionSelected}">
										<input type="hidden" name="isBillingAddressFromAddressBook" id="isAddressFromAddressBookID" value="${isBillingAddressFromAddressBook}" />
										</c:if>
									</dsp:oparam>
								</dsp:droplet>
							</div>
						</div>
						<input type="hidden" name="isEditModeOnChange" id="isEditModeOnChange" />
						<dsp:include page="billingAddress.jsp">
							<dsp:param name="isEditSavedCard" value="${isEditSavedCard}" />
							<dsp:param name="fromSuggestedAddress" value="${fromSuggestedAddress}" />
							<dsp:param name="creditCard" value="${creditCard}" />
							<dsp:param name="isBillingAddressFromAddressBook" value="${isBillingAddressFromAddressBook}" />
						</dsp:include>
					</div>
				</div>
			</div>
			<div class="row top-border attach-to-bottom left-padding">
				<div class="col-md-12">
						<input type="submit" id="submitCreditCardId" priority="-10" name="addCreditCardHiddenBtn" value="submit" style="display: none;" />
                        <button type="button" onclick="return addCreditCard();"><fmt:message key="myaccount.save" /></button><!-- addCreditCard() -->
				</div>
			</div>
		</form>
	</div>

</dsp:page>