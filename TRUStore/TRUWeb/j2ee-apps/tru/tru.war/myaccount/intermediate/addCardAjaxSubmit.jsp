
<dsp:page>

	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
	<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
	<dsp:getvalueof var="truConf" bean="TRUConfiguration" />
	
	<dsp:getvalueof var="nameOnCard" param="nameOnCard" scope="request" />
	<dsp:getvalueof var="creditCardNumber" param="creditCardNumber" scope="request" />	
	<dsp:getvalueof var="expirationMonth" param="expirationMonth" scope="request" />
	<dsp:getvalueof var="expirationYear" param="expirationYear" scope="request" />
	<dsp:getvalueof var="selectedCardBillingAddress" param="selectedCardBillingAddress" scope="request" />
	<dsp:getvalueof var="billingAddressNickName" param="nickName" scope="request" />
	<dsp:getvalueof var="editCardNickName" param="editCardNickName" scope="request" />
	<dsp:getvalueof var="creditCardCVV" param="creditCardCVV" scope="request" />
	
	<dsp:getvalueof var="nickName" param="nickName" scope="request" />
	<dsp:getvalueof var="firstName" param="firstName" scope="request" />
	<dsp:getvalueof var="lastName" param="lastName" scope="request" />
	<dsp:getvalueof var="address1" param="address1" scope="request" />
	<dsp:getvalueof var="address2" param="address2" scope="request" />
	<dsp:getvalueof var="city" param="city" scope="request" />
	<dsp:getvalueof var="state" param="state" scope="request" />
	<dsp:getvalueof var="postalCode" param="postalCode" scope="request" />
	<dsp:getvalueof var="country" param="country" scope="request" />
	<dsp:getvalueof var="phoneNumber" param="phoneNumber" scope="request" />
	<dsp:getvalueof var="lastOptionSelected" param="lastOptionSelected" vartype="java.lang.boolean" scope="request" />
	<dsp:getvalueof var="bilingAddressFieldVisible" param="bilingAddressFieldVisible" vartype="java.lang.boolean"/>
	<dsp:getvalueof var="creditCardToken" param="token" scope="request" />
	<dsp:getvalueof var="cBinNum" param="cBinNum" scope="request" />
	<dsp:getvalueof var="cLength" param="cLength" scope="request" />
	
	
	<dsp:getvalueof var="ccNum" param="cBinNum" />
	
	<dsp:setvalue bean="ProfileFormHandler.creditCardInfoMap.nameOnCard" paramvalue="nameOnCard" />
	<dsp:setvalue bean="ProfileFormHandler.creditCardInfoMap.creditCardToken" value="${creditCardToken}" />
	<%-- <dsp:setvalue bean="ProfileFormHandler.creditCardInfoMap.creditCardNumber" value="${cBinNum}" /> --%>
	<dsp:setvalue bean="ProfileFormHandler.creditCardInfoMap.cardLength" value="${cLength}"/>
	<dsp:setvalue bean="ProfileFormHandler.creditCardInfoMap.expirationMonth" paramvalue="expirationMonth"/>
	<dsp:setvalue bean="ProfileFormHandler.creditCardInfoMap.expirationYear" paramvalue="expirationYear"/>
	<dsp:setvalue bean="ProfileFormHandler.creditCardInfoMap.creditCardType" paramvalue="creditCardType"/>
	
	<dsp:setvalue bean="ProfileFormHandler.creditCardInfoMap.creditCardVerificationNumber" paramvalue="creditCardCVV" />
	<dsp:setvalue bean="ProfileFormHandler.selectedCardBillingAddress" paramvalue="selectedCardBillingAddress" />
	<dsp:setvalue bean="ProfileFormHandler.creditCardCVV" paramvalue="creditCardCVV" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.country" paramvalue="country" />
	<dsp:setvalue bean="ProfileFormHandler.billingAddressNickName" paramvalue="nickName" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.firstName" paramvalue="firstName" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.nickName" paramvalue="nickName" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.lastName" paramvalue="lastName" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.address1" paramvalue="address1" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.address2" paramvalue="address2" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.city" paramvalue="city" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.state" paramvalue="state" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.otherState" paramvalue="otherState" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.postalCode" paramvalue="postalCode" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.phoneNumber" paramvalue="phoneNumber" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.addressValidated" paramvalue="billingAddressValidated" />
	<dsp:setvalue bean="ProfileFormHandler.editCardNickName" paramvalue="editCardNickName"/>
	<dsp:setvalue bean="ProfileFormHandler.lastOptionSelected" value="${lastOptionSelected}"/>
	<dsp:setvalue bean="ProfileFormHandler.addressValue.bilingAddressFieldVisible" value="${bilingAddressFieldVisible}" />
	
	<dsp:droplet name="Switch">
		<dsp:param name="value" param="isEditSavedCard" />
		<dsp:oparam name="true">
			<dsp:setvalue bean="ProfileFormHandler.creditCardInfoMap.creditCardNumber" value="${creditCardToken}" />
			<dsp:setvalue bean="ProfileFormHandler.updateCreditCard"
				value="submit" />
		</dsp:oparam>
		<dsp:oparam name="default">
			<dsp:setvalue bean="ProfileFormHandler.creditCardInfoMap.creditCardNumber" value="${cBinNum}" />
			<dsp:setvalue bean="ProfileFormHandler.addCreditCard" value="submit" />
		</dsp:oparam>
	</dsp:droplet>

	<dsp:droplet name="Switch">
		<dsp:param bean="ProfileFormHandler.formError" name="value" />
		<dsp:oparam name="true">
			<dsp:droplet name="ErrorMessageForEach">
				<dsp:param name="exceptions"
					bean="ProfileFormHandler.formExceptions" />
				<dsp:oparam name="outputStart">
					Following are the form errors:
				</dsp:oparam>
				<dsp:oparam name="output">
					<dsp:valueof param="message" />
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:droplet name="Switch">
				<dsp:param name="value" bean="ProfileFormHandler.addressDoctorProcessStatus" />
				<dsp:oparam name="VERIFIED">
					<dsp:include page="/myaccount/intermediate/cardOverlayFragment.jsp" />
				</dsp:oparam>
				<dsp:oparam name="CORRECTED">
					<dsp:droplet name="Switch">
						<dsp:param name="value" bean="ProfileFormHandler.addressDoctorResponseVO.derivedStatus" />
						  <dsp:oparam name="YES">
								<dsp:include page="/myaccount/intermediate/suggestedAddressAjaxResponse.jsp">
									<dsp:param name="mode" value="creditCard" />
								</dsp:include>
						  </dsp:oparam>
						   <dsp:oparam name="NO">
						   		<dsp:include page="/myaccount/intermediate/cardOverlayFragment.jsp" />
						   </dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="VALIDATION ERROR">
					<dsp:include page="/myaccount/intermediate/suggestedAddressAjaxResponse.jsp">
						<dsp:param name="mode" value="creditCard" />
					</dsp:include>
				</dsp:oparam>
				<dsp:oparam name="default">
					<dsp:include page="/myaccount/intermediate/cardOverlayFragment.jsp" />
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>
