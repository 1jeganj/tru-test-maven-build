<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>

<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<div class="col-md-5 your-addresses-background set-height">
           <div class="row">
               <div class="col-md-12">
                   <h3><fmt:message key="myaccount.your.addresses" /></h3>
               </div>
           </div>
           <dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
    <div class="tse-scrollable">
		<div class="tse-scroll-content">
		  <div class="tse-content">           
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" bean="Profile.shippingAddress.id" />
				<dsp:oparam name="false">
			<dsp:droplet name="ForEach">
           	<dsp:param name="array" bean="Profile.secondaryAddresses"/>
           	<dsp:param name="elementName" value="address"/>
           	<dsp:oparam name="output">
			<dsp:getvalueof param="address.id" var="addressId" />
			<dsp:getvalueof param="address.isBillingAddress" var="isBillingAddress" />
			<dsp:getvalueof var="nickName" param="key" />
			<c:if test="${addressId eq shippingAddressId and not isBillingAddress}">
           		<div class="small-spacer"></div>
             <div class="row">
                 <div class="col-md-12">
                     <p><span class="dark-blue avenir-heavy"><dsp:valueof param="key" /></span> . <span class="default-address"><fmt:message key="myaccount.default.address" /></span></p>
                     <p class="avenir-heavy"><dsp:valueof param="address.firstName" />&nbsp;<dsp:valueof param="address.lastName" /></p>
                     <p><dsp:valueof param="address.address1" />&nbsp;<dsp:valueof param="address.address2" /></p>
                     <p><dsp:valueof param="address.city" />, <dsp:valueof param="address.state" />&nbsp;<dsp:valueof param="address.postalCode" /></p>
                     <div class="row">
                         <div class="col-md-8">
                             <p>
                             	<span class="blue">
                             		<a href="javascript:void(0);" class="myAccountEditAddress" title="edit address"><fmt:message key="myaccount.edit" /></a>
                             		<span class="nickNameInHiddenSpan">${nickName}</span>
                             	</span>
                             </p>
                         </div>
                         <div class="col-md-1">
                       		<a href="javascript:void(0)" class="delete-icon myAccountRemoveAddress" title="delete address" data-toggle="modal" data-target="#myAccountDeleteCancelModal"> <img src="${TRUImagePath}images/delete-icon.png" alt="delete"></a>
                            <span class="nickNameInHiddenSpan">${nickName}</span>
                         </div>
                     </div>
                 </div>
             </div>
			 </c:if>
           	</dsp:oparam>
           </dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
           <dsp:droplet name="ForEach">
           	<dsp:param name="array" bean="Profile.secondaryAddresses"/>
           	<dsp:param name="elementName" value="address"/>
           	<dsp:param name="sortProperties" value="-lastActivity"/>
           	<dsp:oparam name="output">
			<dsp:getvalueof param="address.id" var="addressId" />
			<dsp:getvalueof var="nickName" param="key" />
			<dsp:getvalueof param="address.isBillingAddress" var="isBillingAddress" />
			<c:if test="${addressId ne shippingAddressId and not isBillingAddress}">
           	<div class="spacer"></div>
             <div class="row">
                 <div class="col-md-12">
                 <dsp:getvalueof param="address.country" var="country"/>
                 <c:if test="${country ne 'US'}">
                 	<p><fmt:message key="myaccount.billing.purpose" /></p>
               	 </c:if>
                     <p class="dark-blue avenir-heavy"><dsp:valueof param="key" /></p>
                     <p class="avenir-heavy"><dsp:valueof param="address.firstName" />&nbsp;<dsp:valueof param="address.lastName" /></p>
                     <p><dsp:valueof param="address.address1" />&nbsp;<dsp:valueof param="address.address2" /></p>
                     <p><dsp:valueof param="address.city" />, <dsp:valueof param="address.state" />&nbsp;<dsp:valueof param="address.postalCode" /></p>
                     <c:if test="${country ne 'US'}">
                 		 <p><dsp:valueof param="address.country"/></p>
               	 	</c:if>
                     <div class="row">
                         <div class="col-md-8">
                             <p>
                             	<span class="blue">
                             		<a href="javascript:void(0);" class="myAccountEditAddress"><fmt:message key="myaccount.edit" /></a>
                             		<span class="nickNameInHiddenSpan">${nickName}</span>
                             	</span> .
                             <c:choose>
                             	<c:when test="${country eq 'US'}">
                             		<span class="blue">
										<a class="setDefaultLink myAccountSetDefault" href="javascript:void(0);" id=""><fmt:message key="myaccount.set.default" /></a>
										<span class="nickNameInHiddenSpan">${nickName}</span>
									</span>
                             	</c:when>
                             	<c:otherwise>
                             		<fmt:message key="myaccount.set.default" />
                             	</c:otherwise>
                             </c:choose>
                             </p>
                         </div>
                         <div class="col-md-1">
                            <img class="delete-icon myAccountRemoveAddress" src="${TRUImagePath}images/delete-icon.png" data-toggle="modal" data-target="#myAccountDeleteCancelModal">
                            <span class="nickNameInHiddenSpan">${nickName}</span>
                         </div>
                     </div>
                 </div>
             </div>
			 </c:if>
           	</dsp:oparam>
           </dsp:droplet>
           </div>
         </div>
       </div>
           <div class="spacer"></div>
           <div class="row">
               <div class="col-md-12">
                   <p class="blue"><a href="javascript:void(0);" class="" onclick="onAddAddressInOverlay();"><fmt:message key="myaccount.add.another.address" /></a>
                   </p>
               </div>
           </div>
           <div class="spacer"></div>
       </div>
       
       