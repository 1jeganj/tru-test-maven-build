<dsp:page>
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler"/>
	
	<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardNumber" paramvalue="accountNumber"/>
	<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.expirationMonth" paramvalue="expirationMonth"/>
	<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.expirationYear" paramvalue="expirationYear"/>
	<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardVerificationNumber"  paramvalue="cardCode"/>
	<dsp:setvalue bean="BillingFormHandler.addressInputFields.firstName" paramvalue="firstName"/>
	<dsp:setvalue bean="BillingFormHandler.addressInputFields.lastName" paramvalue="lastName"/>
	<dsp:setvalue bean="BillingFormHandler.addressInputFields.address1" paramvalue="address1"/>
	<dsp:setvalue bean="BillingFormHandler.addressInputFields.address2" paramvalue="address2"/>
	<dsp:setvalue bean="BillingFormHandler.addressInputFields.city" paramvalue="city"/>
	<dsp:setvalue bean="BillingFormHandler.addressInputFields.state" paramvalue="state"/>
	<dsp:setvalue bean="BillingFormHandler.addressInputFields.postalCode" paramvalue="zip"/>
	<dsp:setvalue bean="BillingFormHandler.addressInputFields.phoneNumber" paramvalue="phoneNumber"/>
	<dsp:setvalue bean="BillingFormHandler.addressInputFields.country" paramvalue="country"/>
	<dsp:setvalue bean="BillingFormHandler.email" paramvalue="orderEmail"/>
	
	<dsp:setvalue bean="BillingFormHandler.moveToOrderReview"  value="submit"/>
			<dsp:droplet name="/atg/dynamo/droplet/Switch">
	           <dsp:param bean="BillingFormHandler.formError" name="value" />
	           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="errorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
            </dsp:oparam>
            <dsp:oparam name="false">
					address-saved-success-from-ajax: 
            </dsp:oparam>
         </dsp:droplet>
         
	<%-- <form id="paymentCreditCardForm" class="formValidation" method="POST" action="${contextPath}/myaccount/layaway/myAccountLayaway.jsp">
		<input type="hidden" id="action" name="action" value="${action}" />
		<input type="hidden" name="firstName" value="${firstName}" />
		<input type="hidden" name="lastName" value="${lastName}" />
		<input type="hidden" name="address1" id="suggestedAddress1Id" value="" maxlength="50"/>
		<input type="hidden" name="address2" id="suggestedAddress2Id" value="" maxlength="50"/>
		<input type="hidden" name="city" id="suggestedCityId" value="" maxlength="30"/>
		<input type="hidden" name="state" id="suggestedStateId" value="" />
		<input type="hidden" name="country" id="suggestedCountryId" value="" />
		<input type="hidden" name="postalCode" id="suggestedPostalCodeId" value="" />
		<input type="hidden" name="phoneNumber" value="${phoneNumber}" />
	</form> --%>
	
</dsp:page>