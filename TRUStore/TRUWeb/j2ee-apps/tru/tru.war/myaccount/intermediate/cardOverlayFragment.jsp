<dsp:page>
	<div class="modal-content sharp-border">
		<div class="my-account-add-credit-card-overlay" style="height: 496px;">
			<div class="row">
				<div class="col-md-6 col-md-offset-6 your-saved-cards-background padding-adjustment-right">
					<div class="row top-margin">
						<div class="col-md-1 col-md-offset-10">
							<a href="javascript:void(0)" title="Close" data-dismiss="modal" role="button"><span class="clickable"><span class="sprite-icon-x-close"></span></span></a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<dsp:include page="/myaccount/intermediate/addOrEditCard.jsp"/>
				<dsp:include page="/myaccount/intermediate/yourSavedCards.jsp"/>
			</div>
		</div>
	</div>
</dsp:page>