<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>

	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>

	<dsp:getvalueof var="mode" param="mode" />
	<dsp:getvalueof var="isEditSavedAddress" param="isEditSavedAddress" />
	<dsp:getvalueof var="isEditSavedCard" param="isEditSavedCard" />
	
	<div class="modal-content addressDoctor sharp-border">
		<div class="address-doctor">
			<div>
				<c:choose>
					<c:when test="${mode eq 'creditCard'}">
						<span class="sprite-icon-x-close" onclick="editUserEnteredAddressForCreditCard()"></span>
					</c:when>
					<c:otherwise>
						<span class="sprite-icon-x-close" onclick="editUserEnteredAddress()"></span>
					</c:otherwise>
				</c:choose>
			</div>
			<h3><fmt:message key="myaccount.addressdoctor.header" /></h3>
			<p class="address-doctor-p"><fmt:message key="myaccount.addressdoctor.address.suggestions" /></p>
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" bean="ProfileFormHandler.addressDoctorResponseVO" />
				<dsp:oparam name="false">
					<dsp:getvalueof var="addressRecognized" vartype="java.lang.boolean" bean="ProfileFormHandler.addressDoctorResponseVO.addressRecognized" />
					<c:choose>
						<c:when test="${not addressRecognized}">
							<p>
								<fmt:message key="myaccount.address.not.recognised" />
								<fmt:message key="myaccount.address.choose.existing.address" />
								<fmt:message key="myaccount.address.enter.new" />
							</p>
						</c:when>
					</c:choose>
					<!--<header>we need more detail about the address entered-->
					<!--	<p>Please select the best match below.</p>-->
					<!--</header>-->
					<!--<div class="valid-st-num">-->
					<!--<input name="valid-address" type="radio"/>-->
					<!--<span class="address-doctor-address">enter a valid street number</span>-->
					<!--<input class="street-number-text" type="text" disabled="disabled"/>-->
					<!--</div>-->
					<p class="global-error-display"></p>
					<div class="address-doctor-current">
						<p><fmt:message key="myaccount.use.entered.address" /></p>
						<div>
						<input name="valid-address" onclick="suggestedAddressSelect();" type="radio" id="suggestedAddressCount-0" value="0" checked>
							<p class="address-doctor-address">							
								${address1}<c:if test="${not empty address2}">&nbsp;${address2}</c:if>, ${city}, ${state}, ${postalCode} <span>&#xb7;</span>
								<c:choose>
									<c:when test="${mode eq 'creditCard'}">
										<a href="#" onclick="editUserEnteredAddressForCreditCard()"><fmt:message key="myaccount.addressdoctor.edit" /></a>
									</c:when>
									<c:otherwise>
										<a href="#" onclick="editUserEnteredAddress()"><fmt:message key="myaccount.addressdoctor.edit" /></a>
									</c:otherwise>
								</c:choose>
							</p>
							<input type="hidden" name="suggestedAddress1-0" id="suggestedAddress1-0" value="${address1}" maxlength="50"/>
							<input type="hidden" name="suggestedAddress2-0" id="suggestedAddress2-0" value="${address2}" maxlength="50"/>
							<input type="hidden" name="suggestedCity-0" id="suggestedCity-0" value="${city}" maxlength="30"/>
							<input type="hidden" name="suggestedState-0" id="suggestedState-0" value="${state}" />
							<input type="hidden" name="suggestedPostalCode-0" id="suggestedPostalCode-0" value="${postalCode}" />
							<input type="hidden" name="suggestedCountry-0" id="suggestedCountry-0" value="${country}" />
						</div>
					</div>		
					<dsp:droplet name="IsEmpty">
						<dsp:param name="value" bean="ProfileFormHandler.addressDoctorResponseVO.addressDoctorVO" />
						<dsp:oparam name="false">
							<div class="address-doctor-alt">
								<p><fmt:message key="myaccount.did.you.mean.one.of.following" /></p>					
								<dsp:droplet name="ForEach">
									<dsp:param name="array" bean="ProfileFormHandler.addressDoctorResponseVO.addressDoctorVO" />
									<dsp:param name="elementName" value="addressDoctorVO" />
									<dsp:oparam name="output">
										<dsp:getvalueof var="suggestedAddressCount" param="addressDoctorVO.count"/>
										<dsp:getvalueof var="addressDoctorVO" param="addressDoctorVO"/>
										<div>
											<c:choose>
												<c:when test="${suggestedAddressCount == 1}">
													<input name="valid-address" onclick="suggestedAddressSelect();" type="radio" id="suggestedAddressCount-${suggestedAddressCount}" value="${suggestedAddressCount}" checked>
												</c:when>
												<c:otherwise>
													<input name="valid-address" onclick="suggestedAddressSelect();" type="radio" id="suggestedAddressCount-${suggestedAddressCount}" value="${suggestedAddressCount}">
												</c:otherwise>
											</c:choose>
											<p class="address-doctor-address">
												<dsp:valueof param="addressDoctorVO.fullAddressName" />
											</p>
											<input type="hidden" name="suggestedAddress1-${suggestedAddressCount}" id="suggestedAddress1-${suggestedAddressCount}" value="${addressDoctorVO.address1}" maxlength="50"/>
											<input type="hidden" name="suggestedAddress2-${suggestedAddressCount}" id="suggestedAddress2-${suggestedAddressCount}" value="${addressDoctorVO.address2}" maxlength="50"/>
											<input type="hidden" name="suggestedCity-${suggestedAddressCount}" id="suggestedCity-${suggestedAddressCount}" value="${addressDoctorVO.city}" maxlength="30"/>
											<input type="hidden" name="suggestedState-${suggestedAddressCount}" id="suggestedState-${suggestedAddressCount}" value="${addressDoctorVO.state}" />
											<input type="hidden" name="suggestedPostalCode-${suggestedAddressCount}" id="suggestedPostalCode-${suggestedAddressCount}" value="${addressDoctorVO.postalCode}" />
											<input type="hidden" name="suggestedCountry-${suggestedAddressCount}" id="suggestedCountry-${suggestedAddressCount}" value="${addressDoctorVO.country}" />
										</div>
									</dsp:oparam>
								</dsp:droplet>
							</div>
						</dsp:oparam >
					</dsp:droplet>
					<c:choose>
						<c:when test="${mode eq 'creditCard'}">
							<button onclick="addCreditCard()"><fmt:message key="myaccount.addressdoctor.save.address" /></button>
						</c:when>
						<c:otherwise>
							<button onclick="modifyAddress()"><fmt:message key="myaccount.addressdoctor.save.address" /></button>
						</c:otherwise>
					</c:choose>
				</dsp:oparam>
			</dsp:droplet>
		</div>
	</div>	
	<c:choose>
		<c:when test="${mode eq 'creditCard'}">
			<dsp:getvalueof var="suggestedAddressFormId" value="addOrEditCreditCard" />
			<dsp:getvalueof var="onSubmitJavascriptFunction" value="onSubmitAddCreditCard" />
			<dsp:getvalueof var="suggestedAddressButtonId" value="submitCreditCardId" />
			<dsp:getvalueof var="lastOptionSelected" param="lastOptionSelected" />
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="suggestedAddressFormId" value="myAccountAddressForm" />
			<dsp:getvalueof var="onSubmitJavascriptFunction" value="onSubmitAddress" />
			<dsp:getvalueof var="suggestedAddressButtonId" value="submitAddressId" />
		</c:otherwise>
	</c:choose>
	
	<form id="${suggestedAddressFormId}" method="POST" onsubmit="javascript: return ${onSubmitJavascriptFunction}();">
		<c:choose>
			<c:when test="${mode eq 'creditCard'}">
				<input type="hidden" name="nameOnCard" value="${nameOnCard}" maxlength="40"/>
				<input type="hidden" name="creditCardNumber" value="${creditCardNumber}" />
				<input type="hidden" name="expirationMonth" value="${expirationMonth}" />
				<input type="hidden" name="expirationYear" value="${expirationYear}" />
				<input type="hidden" name="creditCardCVV" value="${creditCardCVV}" />
				<input type="hidden" name="selectedCardBillingAddress" value="${selectedCardBillingAddress}" />
				<input type="hidden" name="billingAddressNickName" value="${billingAddressNickName}" id="billingAddressNickName1234" />
				<input type="hidden" name="editCardNickName" value="${editCardNickName}" id="editCardNickName1234"/>
				<input type="hidden" id="addressValidated" name="billingAddressValidated" value="true" />
				<input type="hidden" name="isEditSavedCard" value="${isEditSavedCard}" />
				<input type="hidden" name="isFromAddressSuggesstionOverlay" id="isFromAddressSuggesstionOverlay" value="true" />
				<input type="hidden" name="lastOptionSelected" id="lastOptionSelected" value="${lastOptionSelected}" />
				<input type="hidden" name="token" value="${creditCardToken}" />
			</c:when>
			<c:otherwise>
				<input type="hidden" id="addressValidated" name="addressValidated" value="true" />
				<input type="hidden" name="isEditSavedAddress" value="${isEditSavedAddress}" />
			</c:otherwise>
		</c:choose>
		<input type="hidden" name="nickName" value="${nickName}" />
		<input type="hidden" name="firstName" value="${firstName}" />
		<input type="hidden" name="lastName" value="${lastName}" />
		<input type="hidden" name="address1" id="suggestedAddress1Id" value="" maxlength="50"/>
		<input type="hidden" name="address2" id="suggestedAddress2Id" value="" maxlength="50"/>
		<input type="hidden" name="city" id="suggestedCityId" value="" maxlength="30"/>
		<input type="hidden" name="state" id="suggestedStateId" value="" />
		<input type="hidden" name="country" id="suggestedCountryId" value="" />
		<input type="hidden" name="postalCode" id="suggestedPostalCodeId" value="" />
		<input type="hidden" name="phoneNumber" value="${phoneNumber}" />
		<input type="submit" name="suggestedAddressBtn" value="btn" id="${suggestedAddressButtonId}" style="display:none;" />
	</form>
	
</dsp:page>