
<dsp:page>

	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>

	<dsp:getvalueof var="nickName" param="nickName" scope="request" />
	<dsp:getvalueof var="firstName" param="firstName" scope="request" />
	<dsp:getvalueof var="lastName" param="lastName" scope="request" />
	<dsp:getvalueof var="address1" param="address1" scope="request" />
	<dsp:getvalueof var="address2" param="address2" scope="request" />
	<dsp:getvalueof var="city" param="city" scope="request" />
	<dsp:getvalueof var="state" param="state" scope="request" />
	<dsp:getvalueof var="postalCode" param="postalCode" scope="request" />
	<dsp:getvalueof var="country" param="country" scope="request" />
	<dsp:getvalueof var="phoneNumber" param="phoneNumber" scope="request" />

	<dsp:setvalue bean="ProfileFormHandler.addressValue.nickName" paramvalue="nickName" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.firstName" paramvalue="firstName" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.lastName" paramvalue="lastName" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.address1" paramvalue="address1" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.address2" paramvalue="address2" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.city" paramvalue="city" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.state" paramvalue="state" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.otherState" paramvalue="otherState" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.postalCode" paramvalue="postalCode" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.country" paramvalue="country" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.phoneNumber" paramvalue="phoneNumber" />
	<dsp:setvalue bean="ProfileFormHandler.addressValue.addressValidated" paramvalue="addressValidated" />
	
	<dsp:droplet name="Switch">
		<dsp:param name="value" param="isEditSavedAddress" />
		<dsp:oparam name="true">
			<dsp:setvalue bean="ProfileFormHandler.editShippingAddress" value="submit" />
		</dsp:oparam>
		<dsp:oparam name="default">
			<dsp:setvalue bean="ProfileFormHandler.addShippingAddress" value="submit" />
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="Switch">
		<dsp:param bean="ProfileFormHandler.formError" name="value"/>
		<dsp:oparam name="true">
			<dsp:droplet name="ErrorMessageForEach">
		     	<dsp:param name="exceptions" bean="ProfileFormHandler.formExceptions"/>
				<dsp:oparam name="outputStart">
					Following are the form errors:
				</dsp:oparam>
				<dsp:oparam name="output">
		  			<dsp:valueof param="message"/>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="false">
		
			<dsp:droplet name="Switch">
				<dsp:param name="value" bean="ProfileFormHandler.addressDoctorProcessStatus" />
				<dsp:oparam name="VERIFIED">
					<dsp:include page="/myaccount/intermediate/addressAjaxResponse.jsp"/>
				</dsp:oparam>
				<dsp:oparam name="CORRECTED">
				   <dsp:droplet name="Switch">
						<dsp:param name="value" bean="ProfileFormHandler.addressDoctorResponseVO.derivedStatus" />
						  <dsp:oparam name="YES">
								<dsp:include page="/myaccount/intermediate/suggestedAddressAjaxResponse.jsp">
								<dsp:param name="mode" value="address" />
							</dsp:include>
						  </dsp:oparam>
						   <dsp:oparam name="NO">
						   		<dsp:include page="/myaccount/intermediate/addressAjaxResponse.jsp"/>
						   </dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="VALIDATION ERROR">
						<dsp:include page="/myaccount/intermediate/suggestedAddressAjaxResponse.jsp">
							<dsp:param name="mode" value="address" />
						</dsp:include>
				</dsp:oparam>
				<dsp:oparam name="default">
					<dsp:include page="/myaccount/intermediate/addressAjaxResponse.jsp"/>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	
	
	
	
</dsp:page>