<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/ForgotPasswordHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:getvalueof var="formID" param="formID"></dsp:getvalueof>

	<c:choose>
		<c:when test="${formID eq 'removeShippingAddress'}">
			<dsp:setvalue bean="ProfileFormHandler.value.nickName" paramvalue="name" />
			<dsp:setvalue bean="ProfileFormHandler.removeShippingAddress" value="submit" />
			<dsp:include page="/myaccount/intermediate/addressAjaxResponse.jsp"/>
		</c:when>
		<c:when test="${formID eq 'confirmRemoveMembershipForm'}">
			<dsp:setvalue bean="ProfileFormHandler.removeRewardMember" value="submit" />
			<dsp:include page="/myaccount/myAccountRewardsRus.jsp"/>
		</c:when>
		<c:when test="${formID eq 'defaultAddress'}">
			<dsp:setvalue bean="ProfileFormHandler.value.nickName" paramvalue="defaultAddNickName" />
			<dsp:setvalue bean="ProfileFormHandler.setAddressAsDefault" value="submit" />
			<dsp:include page="/myaccount/intermediate/addressAjaxResponse.jsp"/>
		</c:when>
		<c:when test="${formID eq 'forgotPassowrd'}">
			<dsp:getvalueof var="email" param="email"/>
			<dsp:setvalue bean="ForgotPasswordHandler.value.email" value="${email}" />
			<dsp:setvalue bean="ForgotPasswordHandler.forgotPassword" value="submit" />
			<dsp:droplet name="Switch">
					<dsp:param bean="ForgotPasswordHandler.formError" name="value"/>
					<dsp:oparam name="true">
						 <dsp:droplet name="ErrorMessageForEach">
					        <dsp:param name="exceptions" bean="ForgotPasswordHandler.formExceptions"/>
							<dsp:oparam name="outputStart">
								Following are the form errors:
							</dsp:oparam>
							<dsp:oparam name="output">
							 <dsp:valueof param="errorCode"/>
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
					<dsp:oparam name="false">
						<c:if test="${not empty email}">
							<span id="emailSentSuccess" class="successMessage" ><fmt:message key="myaccount.email.sent.success" /></span>
						</c:if>
					</dsp:oparam>
				  </dsp:droplet>
		</c:when>
		<c:when test="${formID eq 'membershipForm' or formID eq 'membershipFormFromHeader'}">
			<dsp:setvalue bean="ProfileFormHandler.ajax" value="true"/>
			<dsp:setvalue bean="ProfileFormHandler.rewardNumberValid" paramvalue="flag" />
			<dsp:setvalue bean="ProfileFormHandler.rewardNumber" paramvalue="rewardsCard" />
			<dsp:setvalue bean="ProfileFormHandler.addRewardMember" value="submit" />
				  <dsp:droplet name="Switch">
					<dsp:param bean="ProfileFormHandler.formError" name="value"/>
					<dsp:oparam name="true">
						 <dsp:droplet name="ErrorMessageForEach">
					        <dsp:param name="exceptions" bean="ProfileFormHandler.formExceptions"/>
							<dsp:oparam name="outputStart">
								Following are the form errors:
							</dsp:oparam>
							<dsp:oparam name="output">
							   <dsp:valueof param="message"/>
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
					<dsp:oparam name="false">
					<c:choose>
						<c:when test="${formID eq 'membershipForm'}">
							<dsp:include page="/myaccount/myAccountRewardsRus.jsp" />
						</c:when>
						<c:when test="${formID eq 'membershipFormFromHeader'}">
						<dsp:include page="/header/rewardsRUsHeaderSection.jsp" />
						</c:when>
					</c:choose>
					</dsp:oparam>
				  </dsp:droplet>
			
		</c:when>
		<c:when test="${formID eq 'defaultCreditCard'}">
			<dsp:setvalue bean="ProfileFormHandler.value.nickName" paramvalue="defaultCardNickName" />
			<dsp:setvalue bean="ProfileFormHandler.setCreditCardAsDefault" value="submit" />
			<dsp:getvalueof var="id" param="id" />
			<c:choose>
				<c:when test="${id eq 'overlay'}">
					<dsp:include page="/myaccount/intermediate/cardOverlayFragment.jsp" />
				</c:when>
				<c:when test="${id eq 'landingPage'}"> 
					<dsp:include page="/myaccount/myAccountCard.jsp" />
				</c:when>
			</c:choose>
		</c:when>
		<c:when test="${formID eq 'removeCard'}">
		<dsp:getvalueof var="nickName" param="nickName"></dsp:getvalueof>
			<dsp:setvalue bean="ProfileFormHandler.deleteCardNickName" paramvalue="nickName"/>
			<dsp:setvalue bean="ProfileFormHandler.deleteCreditCard" value="submit"/>
			<dsp:include page="/myaccount/intermediate/cardOverlayFragment.jsp"></dsp:include>
		</c:when>
		<c:when test="${formID eq 'removeCardLanding'}">
		<dsp:getvalueof var="nickName" param="nickName"></dsp:getvalueof>
			<dsp:setvalue bean="ProfileFormHandler.deleteCardNickName" paramvalue="nickName"/>
			<dsp:setvalue bean="ProfileFormHandler.deleteCreditCard" value="submit"/>
			<dsp:include page="/myaccount/myAccountCard.jsp"></dsp:include>
		</c:when>
		<c:when test="${formID eq 'hearderLoginOverlay'}">
			<dsp:setvalue bean="ProfileFormHandler.ajax" value="true"/>
			<dsp:setvalue bean="ProfileFormHandler.value.login" paramvalue="email"/>
			<dsp:setvalue bean="ProfileFormHandler.value.password" paramvalue="password"/>
			<dsp:setvalue bean="ProfileFormHandler.login" value="submit"/>
			<dsp:droplet name="Switch">
				<dsp:param bean="ProfileFormHandler.formError" name="value"/>
				<dsp:oparam name="true">
					 <dsp:droplet name="ErrorMessageForEach">
				        <dsp:param name="exceptions" bean="ProfileFormHandler.formExceptions"/>
						<dsp:oparam name="outputStart">
							Following are the form errors:
						</dsp:oparam>
						<dsp:oparam name="output">
						   <dsp:valueof param="message"/>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="false">
					<dsp:getvalueof var="order" bean="/atg/commerce/ShoppingCart.current" vartype="java.lang.boolean"/>
					<dsp:getvalueof var="isOrderInstorePickup" value="${order.orderIsInStorePickUp}" vartype="java.lang.boolean"/>
					<dsp:getvalueof var="showMergeCartOverlay" value="${order.transientData.showMergeCartOverlay}" vartype="java.lang.boolean"/>
					<dsp:getvalueof var="redirectToShipping" value="${order.transientData.redirectToShipping}" vartype="java.lang.boolean"/>
					isOrderInstorePickup:${isOrderInstorePickup}
					showMergeCartOverlay:${showMergeCartOverlay}
					redirectToShipping:${redirectToShipping}
				</dsp:oparam>
			  </dsp:droplet>
		</c:when>
		<c:when test="${formID eq 'changeCountry'}">
			<dsp:getvalueof var="selectCountry" param="selectCountry"></dsp:getvalueof>
			<select name="state" onchange="onCreditCardSelectedStateChange();">
				<option value="">please select</option>
				<c:choose>
					<c:when test="${selectCountry eq 'US'}">
						<dsp:droplet name="ForEach">
						  <dsp:param name="array" bean="/atg/commerce/util/StateList_US.places"/>
						  <dsp:param name="elementName" value="state"/>
						  <dsp:oparam name="output">
							  <dsp:getvalueof var="stateCode" param="state.code"></dsp:getvalueof>
							  <option value="${stateCode}"><dsp:valueof param="state.code" /></option>
						  </dsp:oparam>
						</dsp:droplet>
					</c:when>
					<c:otherwise>
						<option value="other" selected="selected">other</option>
					</c:otherwise>
				</c:choose>
			</select>
		</c:when>
		<c:when test="${formID eq 'signInForm'}">
			<dsp:setvalue bean="ProfileFormHandler.extractDefaultValuesFromProfile"	value="false" />
			<dsp:setvalue bean="ProfileFormHandler.ajax" value="true"/>
			<dsp:setvalue bean="ProfileFormHandler.value.login" paramvalue="email"/>
			<dsp:setvalue bean="ProfileFormHandler.value.password" paramvalue="password"/>
			<dsp:setvalue bean="ProfileFormHandler.login" value="submit"/>
			<dsp:droplet name="Switch">
				<dsp:param bean="ProfileFormHandler.formError" name="value"/>
				<dsp:oparam name="true">
					 <dsp:droplet name="ErrorMessageForEach">
				        <dsp:param name="exceptions" bean="ProfileFormHandler.formExceptions"/>
						<dsp:oparam name="outputStart">
							Following are the form errors:
						</dsp:oparam>
						<dsp:oparam name="output">
						   <dsp:valueof param="message"/>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			  </dsp:droplet>
		</c:when>
		<c:when test="${formID eq 'createAccountForm'}">
			<dsp:setvalue bean="ProfileFormHandler.ajax" value="true"/>
			<dsp:setvalue bean="ProfileFormHandler.value.login" paramvalue="email"/>
			<dsp:setvalue bean="ProfileFormHandler.value.password" paramvalue="password"/>
			<dsp:setvalue bean="ProfileFormHandler.value.confirmpassword" paramvalue="confirm_password"/>
			<dsp:setvalue bean="ProfileFormHandler.create" value="submit"/>
			<dsp:droplet name="Switch">
				<dsp:param bean="ProfileFormHandler.formError" name="value"/>
				<dsp:oparam name="true">
					 <dsp:droplet name="ErrorMessageForEach">
				        <dsp:param name="exceptions" bean="ProfileFormHandler.formExceptions"/>
						<dsp:oparam name="outputStart">
							Following are the form errors:
						</dsp:oparam>
						<dsp:oparam name="output">
						   <dsp:valueof param="message"/>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			  </dsp:droplet>
		</c:when>
		<c:when test="${formID eq 'submitResetPasswordForm'}">
			<dsp:setvalue bean="ProfileFormHandler.ajax" value="true"/>
			<dsp:setvalue bean="ProfileFormHandler.value.Password" paramvalue="password"/>
			<dsp:setvalue bean="ProfileFormHandler.value.confirmPassword"  paramvalue="confirmPassword"/>
			<dsp:setvalue bean="ProfileFormHandler.value.email" paramvalue="resetPwdEmail"/>
			<dsp:setvalue bean="ProfileFormHandler.resetPassword" value="submit"/>
			
			<dsp:droplet name="Switch">
				<dsp:param bean="ProfileFormHandler.formError" name="value"/>
				<dsp:oparam name="true">
				<dsp:droplet name="ErrorMessageForEach">
	        		 <dsp:param name="exceptions" bean="ProfileFormHandler.formExceptions"/>
	        		 <dsp:oparam name="outputStart">
							Following are the form errors:
					 </dsp:oparam>
					 <dsp:oparam name="output">
			   			<div style="color:red"><dsp:valueof param="message"/></div>
					 </dsp:oparam>
				</dsp:droplet>
				</dsp:oparam>
  			</dsp:droplet>
		</c:when>
	</c:choose>
</dsp:page>