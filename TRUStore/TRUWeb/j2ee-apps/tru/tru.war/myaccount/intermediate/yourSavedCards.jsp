<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg//atg/dynamo/droplet/ForEach" />
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/com/tru/common/droplet/TRUCreditCardExpiryCheckDroplet"/>
<div class="col-md-6 your-saved-cards-background set-height padding-adjustment-right padding-adjustment-left" id="savedCreditCards" style="height: 456px;">
<div class="row">
	<div class="col-md-12">
		<h3><fmt:message key="myaccount.your.saved.cards" /></h3>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="small-spacer"></div>
	</div>
</div>
<div class="tse-scrollable" ><div class="tse-scrollbar" style="display: none;"><div class="drag-handle visible"></div></div>
                 <div class="tse-scroll-content" >
                     <div class="tse-content">
<div class="row">
<div class="col-md-12">
<table>
<dsp:droplet name="IsEmpty">
 <dsp:param bean="Profile.defaultCreditCard.id" name="value"/>
 <dsp:oparam name="false">
			<dsp:droplet name="/atg/dynamo/droplet/ForEach">
			<dsp:param name="array" bean="Profile.creditCards" />
			<dsp:oparam name="output">
			<dsp:getvalueof var="defaultId" bean="Profile.defaultCreditCard.id"></dsp:getvalueof>
			<dsp:getvalueof id="creditCardId" param="element.id" />
			<c:if test="${defaultId eq creditCardId}">
			<dsp:getvalueof var="nickName" param="key"></dsp:getvalueof>
			</c:if>
			</dsp:oparam>
			</dsp:droplet>		
 	<tr> 	
 	<dsp:droplet name="TRUCreditCardExpiryCheckDroplet">
					<dsp:param name="month" bean="Profile.defaultCreditCard.expirationMonth"/>
					<dsp:param name="year" bean="Profile.defaultCreditCard.expirationYear"/>
					<dsp:oparam name="true">
						<dsp:getvalueof var="expiredClass" value="cardExpired"></dsp:getvalueof>
					</dsp:oparam>
					<dsp:oparam name="false">
						<dsp:getvalueof var="expiredClass" value=""></dsp:getvalueof>
					</dsp:oparam>
				</dsp:droplet>
		<td>
			<dsp:getvalueof var="cardType" bean="Profile.defaultCreditCard.creditCardType" vartype="java.lang.String"></dsp:getvalueof>
			<dsp:getvalueof var="cardId" bean="Profile.defaultCreditCard.id" vartype="java.lang.String"></dsp:getvalueof>
			<c:choose>
				<c:when test="${cardType eq 'visa'}">
					<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Visa-Thumb-No-Outline.jpg" alt="visa card">
				</c:when>
				<c:when test="${cardType eq 'discover'}">
					<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Discover-Thumb.jpg" alt="discover">
				</c:when>
				<c:when test="${cardType eq 'masterCard'}">
					<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Mastercard-Thumb-No-Outline.jpg" alt="master card">
				</c:when>
				<c:when test="${cardType eq 'americanExpress'}">
					<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Amex-Thumb-No-Outline.jpg" alt="amex">
				</c:when>
				<c:when test="${cardType eq 'RUSPrivateLabelCard'}">
					<img class="credit-card" src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-1.jpg" alt="PLCCCard">
				</c:when>
				<c:when test="${cardType eq 'RUSCoBrandedMasterCard'}">
					<img class="credit-card" src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-2.jpg" alt="RUSCoBCard">
				</c:when>
				<c:otherwise>
				No Image Available
				</c:otherwise>
			</c:choose>
		</td>
		<td class="${expiredClass}">
			<p class="avenir-light">
				<dsp:valueof bean="Profile.defaultCreditCard.creditCardNumber" converter="creditcard" maskCharacter="x"/>				
			</p>
		</td>
				
		<td class="${expiredClass}">
			<p>
				<span class="avenir-light expiry">
				<c:if test="${cardType ne 'RUSPrivateLabelCard'}">
					<fmt:message key="myaccount.card.exp" />&nbsp;<dsp:valueof bean="Profile.defaultCreditCard.expirationMonth"></dsp:valueof>/<dsp:valueof bean="Profile.defaultCreditCard.expirationYear"></dsp:valueof>
				</c:if>
				<c:if test="${expiredClass eq 'cardExpired'}">
				<br /><fmt:message key="myaccount.card.expired" />
				</c:if>
				</span> &#183; <span class="blue">
				<a href="#" class="" onclick='javascript: onEditCreditCardOverlay("${nickName}");' title="edit card detail"><fmt:message key="myaccount.edit" /></a></span> &#183; <fmt:message key="myaccount.default.card" />
				<!--  � default card	 -->
			</p>
		</td>
		<td>
			<a href="javascript:void(0)" class="delete-icon" title="delete card" data-toggle="modal" data-target="#myAccountCardDeleteModal" onclick="removeCardConfirm('${nickName}')"><img src="${TRUImagePath}images/delete-icon.png" alt="delete saved card"></a>
		</td>
	</tr>
</dsp:oparam>
</dsp:droplet>
<dsp:getvalueof var="defaultId" bean="Profile.defaultCreditCard.id">
</dsp:getvalueof>
<dsp:droplet name="/atg/dynamo/droplet/ForEach">
<dsp:param name="array" bean="Profile.creditCards" />
<dsp:param name="sortProperties" value="-lastActivity"/>
<dsp:oparam name="output">
<dsp:getvalueof id="creditCardId" param="element.id" />
<dsp:getvalueof var="nickName" param="key"></dsp:getvalueof>
<c:if test="${defaultId ne creditCardId}">

  			<tr>
  			<dsp:droplet name="TRUCreditCardExpiryCheckDroplet">
					<dsp:param name="month" param="element.expirationMonth"/>
					<dsp:param name="year" param="element.expirationYear"/>
					<dsp:oparam name="true">
						<dsp:getvalueof var="expiredClass" value="cardExpired"></dsp:getvalueof>
					</dsp:oparam>
					<dsp:oparam name="false">
						<dsp:getvalueof var="expiredClass" value=""></dsp:getvalueof>
					</dsp:oparam>
				</dsp:droplet>  
  			<td>
  				<dsp:getvalueof var="cardType" param="element.creditCardType" vartype="java.lang.String"></dsp:getvalueof>
  				<dsp:getvalueof var="creditCard" param="element"></dsp:getvalueof>
  				<c:choose>
					<c:when test="${cardType eq 'visa'}">
						<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Visa-Thumb-No-Outline.jpg" alt="visa card">
					</c:when>
					<c:when test="${cardType eq 'discover'}">
						<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Discover-Thumb.jpg" alt="discover">
					</c:when>
					<c:when test="${cardType eq 'masterCard'}">
						<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Mastercard-Thumb-No-Outline.jpg" alt="master card">
					</c:when>
					<c:when test="${cardType eq 'americanExpress'}">
						<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Amex-Thumb-No-Outline.jpg" alt="amex">
					</c:when>
					<c:when test="${cardType eq 'RUSPrivateLabelCard'}">
						<img class="credit-card" src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-1.jpg" alt="PLCCCard">
					</c:when>
					<c:when test="${cardType eq 'RUSCoBrandedMasterCard'}">
						<img class="credit-card" src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-2.jpg" alt="RUSCoBCard">
					</c:when>
					<c:otherwise>
					No Image Available
					</c:otherwise>
			</c:choose>
			</td>
				<td class="${expiredClass}">
					<p class="avenir-light">
						<dsp:valueof converter="creditcard" param="element.creditCardNumber" maskCharacter="x"/>
					</p>
				</td>
				
				<td class="${expiredClass}">
					<p>
						<span class="avenir-light expiry">
						<c:if test="${cardType ne 'RUSPrivateLabelCard'}">
							<fmt:message key="myaccount.card.exp" />&nbsp;<dsp:valueof param="element.expirationMonth"></dsp:valueof>/<dsp:valueof param="element.expirationYear"></dsp:valueof>
						</c:if>
						<c:if test="${expiredClass eq 'cardExpired'}">
							<br/> <fmt:message key="myaccount.card.expired" />
						</c:if>
						</span> &#183; <span class="blue">
						<a href="#" class="" onclick='javascript: onEditCreditCardOverlay("${nickName}");' title="edit card detail"><fmt:message key="myaccount.edit" /></a></span> &#183;
						<span class="blue"><a href="javascript:void(0);" onclick="defaultCard('${nickName}','overlay'); return false" title="set as default card"><fmt:message key="myaccount.set.default" /></a></span>
						<!--  � default card	 -->
					</p>
				</td>
			
				<td>
					<a href="javascript:void(0)" class="delete-icon" title="delete card" data-toggle="modal" data-target="#myAccountCardDeleteModal" onclick="removeCardConfirm('${nickName}')"><img src="${TRUImagePath}images/delete-icon.png" alt="delete saved card"></a>
				</td>
			</tr>
			</c:if>

</dsp:oparam>															
</dsp:droplet>
</table>
</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="small-spacer"></div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<p class="blue"><a href="#" onclick="onAddCreditCardOverlay();"><fmt:message key="myaccount.add.another.card" /></a>
		</p>
	</div>
</div>
</div>
</div>
</div>


</div>







