<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:getvalueof bean="Profile.securityStatus" var="securityStatus"/>
<c:choose>
<c:when test="${securityStatus lt 4}">
<json:object>
	<json:property name="sessionExpired" value="session expired" />
</json:object>
</c:when>
<c:otherwise>
<json:object>
	<json:property name="sessionExpired" value="session not expired" />
</json:object>
</c:otherwise>
</c:choose>