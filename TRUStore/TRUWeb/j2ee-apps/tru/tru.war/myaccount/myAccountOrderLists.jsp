<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>  
  <div class="my-account-order-history">
                    <div class="order-history-header row">
                        <div class="col-md-5">
                            <h2 class="custmHeading"><fmt:message key="myaccount.orderHistory.orderHistory" /></h2>
                            <p><fmt:message key="myaccount.orderHistory.statusRecentOrder" /></p>
                        </div>
                        <div class="col-md-3 pull-right">
                            <a class="orderHistoryViewAll" href="orderHistory.jsp"><fmt:message key="myaccount.orderHistory.viewAll" /></a>
                        </div>
                    </div>
                    <div class="order-history-table-header row">
                        <div class="col-md-3">
                            <fmt:message key="myaccount.orderHistory.orderPlaced" />
                        </div>
                        <div class="col-md-3">
                            <fmt:message key="myaccount.orderHistory.orderNumber" />
                        </div>
                        <div class="col-md-3">
                            <fmt:message key="myaccount.orderHistory.orderStatus" />
                        </div>
                        <div class="col-md-3 pull-right">
                            <fmt:message key="myaccount.orderHistory.orderTotal" />
                        </div>
                        <%-- <div class="row row-load-more hide">
							<div id="orderHistoryLoadMore" class="load-more"><a href="javascript:void(0)"><fmt:message key="myaccount.orderHistory.loadMore" /></a></div>
						</div> --%>
                    </div>
                    <div class="order-summary-block"></div>
        <!-- Added radialOm myAccountOrderDetails.jsp path to for radial order details display remove radialOm for default display for R3 -->            
       <dsp:include page="/myaccount/radialOm/myAccountOrderDetails.jsp" >
       </dsp:include>
  	</div>
  
  <div class="modal fade" id="orderHistoryCancelOrder" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false" style="display: none; padding-left: 17px;">
			<div class="modal-dialog modal-lg">
				<div class="modal-content sharp-border">
					<div class="my-account-delete-cancel-overlay">
						<div>
							<a href="javascript:void(0)" data-dismiss="modal">
                                <span class="sprite-icon-x-close"></span>
							</a>
						</div>
						<h2><fmt:message key="myaccount.orderHistory.cancelOrder" /></h2>
						<p><fmt:message key="myaccount.orderHistory.cancelConfirm" /></p>
						<div>
							<input type="hidden" value="" class="cancelOrderNumber" />
							<button onclick="cancelOrder(this);"><fmt:message key="myaccount.orderHistory.confirm" /></button><a href="javascript:void(0)" data-dismiss="modal"><fmt:message key="myaccount.orderHistory.cancel" /></a>
						
						</div>
					</div>
				</div>
			</div>
		</div> 
</dsp:page>