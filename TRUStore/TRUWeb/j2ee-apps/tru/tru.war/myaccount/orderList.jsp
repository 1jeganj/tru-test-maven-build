<dsp:page>
<dsp:importbean bean="/com/tru/integration/oms/TRUCustomerOrderHistoryDroplet"/>
<dsp:getvalueof var="pageNumber" name="pageNumber"/>
<dsp:getvalueof var="customerId" name="customerId"/>
<dsp:droplet name="TRUCustomerOrderHistoryDroplet">
	<dsp:param name="customerId" value="${customerId}"/>
	<dsp:param name="pageNumber" value="${pageNumber}"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="response" param="response"/>
		<json:object>
			<json:property name="response" value="${response}"/>
		</json:object>
	</dsp:oparam>
</dsp:droplet>
</dsp:page>