<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<tru:pageContainer>
	<input type="hidden" name="contextPath" class="contextPath" value="${originatingRequest.contextPath}"/>
		<jsp:body>
		<dsp:setvalue param="pageName" value="myAccount"/>
		<input type="hidden" value="My-Account" name="currentpage" id="currentpage"/>
		<div class="modal fade in" id="quickviewModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false">
			<dsp:include page="/jstemplate/quickView.jsp"></dsp:include>
        </div>
        
        <dsp:include page="/myaccount/resourceBundleMyAccount.jsp"/>
        <dsp:include page="/jstemplate/includePopup.jsp" />
        
        <div class="modal fade in" id="shoppingCartModal" data-target="#shoppingCartModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false"></div>
        <div class="modal fade" id="orderHistoryCancelOrder" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false" style="display: none; padding-left: 17px;">
			<div class="modal-dialog modal-lg">
				<div class="modal-content sharp-border">
					<div class="my-account-delete-cancel-overlay">
						<div>
							<a href="javascript:void(0)" data-dismiss="modal">
								<span class="sprite-icon-x-close"></span>
							</a>
						</div>
						<h2><fmt:message key="myaccount.orderHistory.cancelOrder" /></h2>
						<p><fmt:message key="myaccount.orderHistory.cancelConfirm" /></p>
						<div>
							<input type="hidden" value="" class="cancelOrderNumber" />
							<button onclick="cancelOrder(this);"><fmt:message key="myaccount.orderHistory.confirm" /></button><a href="javascript:void(0)" data-dismiss="modal"><fmt:message key="myaccount.orderHistory.cancel" /></a>		
						</div>
					</div>
				</div>
			</div>
		</div> 
			<div id="akamai_login_page">
			<dsp:include page="/myaccount/myAccountSignIn.jsp"/> 
			</div>
			<div id="akamai_myAccount_landing_page">
				<dsp:include page="/myaccount/myAccountLanding.jsp"/>
			</div>
			<div class="modal fade accountOrderHistoryDetailModalPopup" id="orderHistoryDetailModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="display: none;">
            <dsp:include page="/myaccount/tealiumForMyAccount.jsp"/> 
      </div>
		</jsp:body>
	</tru:pageContainer>

</dsp:page>