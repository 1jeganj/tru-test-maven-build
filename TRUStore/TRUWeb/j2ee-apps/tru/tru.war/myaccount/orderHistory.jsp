<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/invalidatebrowsercache.jspf" %>

<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
<dsp:importbean bean="/com/tru/common/TRUIntegrationConfiguration" />
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
<dsp:getvalueof var="customerId" bean="Profile.id"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof var="storeCertona" bean="TRUIntegrationConfiguration.enableCertona"/>
<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>
<input type="hidden" id="customerId" value="${customerId}">
<input type="hidden" id="pageNumber" value="1">
<input type="hidden" id="noOfRecordsPerPage" value="5">
<dsp:getvalueof var="siteCode" value="TRU" />

<dsp:importbean bean="/com/tru/common/droplet/TRUUrlConstructDroplet"/>
<dsp:droplet name="TRUUrlConstructDroplet">
		<dsp:param name="orderListParam" value="orderList" />
		<dsp:param name="orderDetailParam" value="orderDetail" />
		<dsp:param name="orderCancelParam" value="orderCancel" />
		<dsp:oparam name="output">
			<dsp:getvalueof var='omsOrderDetailRestAPIURL' param='orderDetailURL'/>
			<input type="hidden" id="omsOrderDetailRestAPIURL" value="${omsOrderDetailRestAPIURL}">
			<dsp:getvalueof var='omsOrderListRestAPIURL' param='orderListURL'/>
			<input type="hidden" id="omsOrderListRestAPIURL" value="${omsOrderListRestAPIURL}">
			<dsp:getvalueof var='omsCancelOrderRestAPIURL' param='orderCancelURL'/>
			<input type="hidden" id="omsCancelOrderRestAPIURL" value="${omsCancelOrderRestAPIURL}">
		</dsp:oparam>
</dsp:droplet>
<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
	<dsp:droplet name="IsEmpty">
			<dsp:param name="value" bean="Profile.shippingAddress.id" />
			<dsp:oparam name="false">
				<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="Profile.secondaryAddresses"/>
				<dsp:param name="elementName" value="address"/>
				<dsp:oparam name="output">
				<dsp:getvalueof param="address.id" var="addressId" />
				<c:if test="${addressId eq shippingAddressId}">
						 <dsp:getvalueof param="address.city" var="customerCity"/>
						 <dsp:getvalueof param="address.state" var="customerState"/>
						 <dsp:getvalueof param="address.postalCode" var="customerZip"/>
						 <dsp:getvalueof param="address.country" var="customerCountry"/>
				 </c:if>
				</dsp:oparam>
			   </dsp:droplet>
			</dsp:oparam>
  </dsp:droplet>

	<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode" />

	<dsp:getvalueof var="partnerName" bean="TRUTealiumConfiguration.toysPartnerName" />
	<c:if test="${site eq babySiteCode}">
		<dsp:getvalueof var="partnerName" bean="TRUTealiumConfiguration.babyPartnerName" />
	</c:if>

	<dsp:getvalueof bean="TRUTealiumConfiguration.accountOrsoCode" var="orsoCode" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.accountIspuSource" var="ispuSource" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.deviceType" var="deviceType" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.orderHistoryPage" var="myOrderHistoryPage" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.orderHistoryPageName" var="orderHistoryPageName" />
	<dsp:getvalueof var="customerFirstName" bean="Profile.firstName" />
	<dsp:getvalueof var="customerLastName" bean="Profile.lastName" />
	<c:set var="space" value=" "/>
	<c:choose>
	<c:when test="${empty customerFirstName && empty customerLastName}">
		<c:set var="customerName" value='${fn:substring(customerEmail, 0, fn:indexOf(customerEmail, "@"))}'/>
	</c:when>
	<c:when test="${not empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value="${customerFirstName}"/>
	</c:when>
	<c:when test="${empty customerFirstName && not empty customerLastName}">
		   <c:set var="customerName" value="${customerLastName}"/>
	</c:when>
	<c:otherwise>
			<c:set var="customerName" value="${customerFirstName}${space}${customerLastName}"/>
	</c:otherwise>
 </c:choose>
  <dsp:getvalueof var="customerEmail" bean="Profile.login"/>
  <dsp:getvalueof var="loginStatus" bean="Profile.transient"/>
  <dsp:getvalueof var="customerDOB" bean="Profile.dateOfBirth"/>
	<c:choose>
		<c:when test="${loginStatus eq 'true'}">
			<c:set var="customerStatus" value="Guest" />
		</c:when>
		<c:otherwise>
			<c:set var="customerStatus" value="Registered" />
		</c:otherwise>
	</c:choose>

			<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
			<dsp:getvalueof var="toysSiteCode"
				bean="TRUTealiumConfiguration.toysSiteCode" />
			<!-- Certona parameter Declaration Start -->
			<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
			<input type="hidden" id="pushSiteId" value="${site}" />
			<!-- Certona parameter Declaration End -->

			<tru:pageContainer>
				<jsp:body>
				 <dsp:setvalue param="pageName" value="myOrderHistory"/>
              <br class="hideOnPrint">
              <br class="hideOnPrint">
			  <div class="container-fluid order-history-template default-template">
					<div class="siteLogoForPrintPage">
   						<span><img src="${TRUImagePath}images/tru-logo-sm.png" alt="toysrus logo"></span>
						<span> <img src="${TRUImagePath}images/babiesrus-logo-small.png" alt="babiesrus logo"></span>
					</div>
              <div class="template-content">
        <div>

           <div class="order-history-breadcrumb">
                <span><a href="${contextPath}home">home</a></span>
                <span>&gt;</span>
                <span><a
										href="${contextPath}myaccount/myAccount.jsp">my account</a></span>
                <span>&gt;</span>
                <span>order history</span>
            </div>
            <div class="orderHistoryCancelOrder hide"><fmt:message key="myaccount.orderHistory.cancelOrderMessage" /></div>
            <div class="orderHistoryCancelOrderError hide"><fmt:message key="myaccount.orderDetails.serviceDown" /></div>
                     <div class="order-history-header">
                <header>
										<fmt:message key="myaccount.orderHistory.orderHistory" />
									</header>
            </div>
                     <hr>
        </div>
              <div class="row">
            <div class="order-history-left-col">
                <div class="order-history-table">
                    <div class="row">
                        <div class="col-md-3">
												<fmt:message key="myaccount.orderHistory.orderPlaced" />
											</div>
                        <div class="col-md-3">
												<fmt:message key="myaccount.orderHistory.orderNumber" />
											</div>
                        <div class="col-md-3">
												<fmt:message key="myaccount.orderHistory.orderStatus" />
											</div>
                        <div class="col-md-3">
												<fmt:message key="myaccount.orderHistory.orderTotal" />
											</div>
                    </div>
                </div>
                <div class="row row-load-more hide">
						<div id="orderHistoryLoadMore" class="load-more more">
											<fmt:message key="myaccount.orderHistory.loadMore" />
										</div>
				  </div>
            </div>
            <div class="order-history-right-col">
                <dsp:include page="myAccountQuickHelp.jsp" />
                <div class="my-account-norton"
										id="load-my-account-norton-script">
            		<dsp:include page="../common/nortonSealScript.jsp" />
                </div>
            </div>
        </div>

    </div>

       <div class="modal fade orderHistoryDetailModalPopup"
							id="orderHistoryDetailModal" tabindex="-1" role="dialog"
							aria-labelledby="basicModal" aria-hidden="true"
							style="display: none;">

    </div>
    <!-- Added radialOm order-history-template.jsp path to for radial order details display remove radialOm for default display for R3 -->
   <dsp:include page="/myaccount/radialOm/order-history-template.jsp" />

  <div class="modal fade" id="orderHistoryCancelOrder" tabindex="-1"
							role="dialog" aria-labelledby="basicModal" aria-hidden="false"
							style="display: none; padding-left: 17px;">
			<div class="modal-dialog modal-lg">
				<div class="modal-content sharp-border">
					<div class="my-account-delete-cancel-overlay">
						<div>
							<a href="javascript:void(0)" data-dismiss="modal">
								<span class="sprite-icon-x-close"></span>
							</a>
						</div>
						<h2>
											<fmt:message key="myaccount.orderHistory.cancelOrder" />
										</h2>
						<p>
											<fmt:message key="myaccount.orderHistory.cancelConfirm" />
										</p>
						<div>
							<input type="hidden" value="" class="cancelOrderNumber" />
							<button onclick="cancelOrder(this);">
												<fmt:message key="myaccount.orderHistory.confirm" />
											</button>
											<a href="javascript:void(0)" data-dismiss="modal"><fmt:message
													key="myaccount.orderHistory.cancel" /></a>
						</div>
					</div>
				</div>
			</div>
		</div>


              <br>
              <br>
              <br>
              <br>
              </div>
    <dsp:droplet name="GetSiteTypeDroplet">
		<dsp:param name="siteId" value="${site}" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="currentSite" param="site" />
			<c:set var="currentSite" value="${currentSite}" scope="request" />
		</dsp:oparam>
	</dsp:droplet>
	<c:set var="certona" value="${storeCertona}" scope="request" />
       	<!-- Certona related Divs Start -->
       	<c:if test="${certona eq 'true'}">
  		<c:choose>
		<c:when test="${site eq toysSiteCode}">
			<div class="full_width_gray">
				<div id="tmyacctOrderHist_rr">
					<!-- Toysrus order history page recommendations appear here  -->
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div class="full_width_gray">
				<div id="bmyacctOrderHist_rr">
				 	<!-- Babiesrus order history page recommendations appear here -->
				 </div>
			</div>
			<dsp:getvalueof var="siteCode" value="BRU" />
		</c:otherwise>
		</c:choose>
		<div id="rdata" style="visibility: hidden;">
			<div id="site">${siteCode}</div>
			<div id="customerid">${customerId}</div>
		</div>
	</c:if>
	
	<dsp:droplet name="/com/tru/commerce/locations/TRUGetCookieDroplet">
		<dsp:param name="cookieName" value="favStore" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="cookieValue" param="cookieValue" />
			<dsp:getvalueof var="locationIdFromCookie" param="locationId" />
		</dsp:oparam>
		<dsp:oparam name="empty">
		</dsp:oparam>
	</dsp:droplet>
	
	<c:set var="session_id" value="${cookie.sessionID.value}"/>
	<c:set var="visitor_id" value="${cookie.visitorID.value}"/>
		
	<!-- Certona related Data End -->
       <!-- Start: Script for Tealium Integration -->

  		<script type='text/javascript'>
  		 var utag_data = {
  				customer_city: "${customerCity}",
			    customer_country: "${customerCountry}",
		        customer_email:"${customerEmail}",
		        customer_id:"${customerId}",
	            customer_type:"${customerStatus}",
			    customer_state:"${customerState}",
			    customer_zip:"${customerZip}",
			    event_type:"",
			    customer_dob:"${customerDOB}",
			    customer_status: "${customerStatus}",
			    customer_name:"${customerName}",
			    page_name:"${myOrderHistoryPage}:${orderHistoryPageName}",
				page_type:"${myOrderHistoryPage}",
				browser_id:"${pageContext.session.id}",
		        internal_campaign_page:"",
				orso_code:"${orsoCode}",
				ispu_source:"${ispuSource}",
				device_type:"${deviceType}",
				partner_name:"${partnerName}",
				store_locator:"${locationIdFromCookie}",
				session_id : "${session_id}",
			    visitor_id : "${visitor_id}",
			    tru_or_bru : "${siteCode}"
				};
		</script>

		<script type="text/javascript">
		    (function(a,b,c,d){
		    a='${tealiumURL}';
		    b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
		    a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
		    })();

		</script>
	 <!-- End: Script for Tealium Integration -->

              </jsp:body>
			</tru:pageContainer>
</dsp:page>





