<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile" />

	<dsp:getvalueof bean="Profile.firstName" var="firstName" />
	<dsp:getvalueof bean="Profile.lastName" var="lastName" />
	<dsp:getvalueof bean="Profile.email" var="email" />

	<div class="my-account-your-info-cont">
		<h2 class="custmHeading">
			<fmt:message key="myaccount.your.info" />
		</h2>
		<div class="mb">
			<h3>
				<fmt:message key="myaccount.name" />
			</h3>
			<p class="Text">
				<span class="userName"> <c:choose>
						<c:when test="${empty firstName && empty lastName}">
					${fn:substring(email, 0, fn:indexOf(email, "@"))}						
				</c:when>
						<c:when test="${not empty firstName && empty lastName}">
					${firstName}						
				</c:when>
						<c:when test="${empty firstName && not empty lastName}">
					${lastName}						
				</c:when>
						<c:otherwise>
					${firstName}&nbsp;${lastName}		
				</c:otherwise>
					</c:choose></span><span>&#xb7;</span> 
					<a data-target="#namChangeModel"
					class="changeNameModal" data-toggle="modal"
					href="javascript:void(0);" title="edit name" class="" onclick="javascript:forConsoleErrorFixing(this);"><fmt:message
							key="myaccount.edit" /></a>
				

			</p>
		</div>
		<div class="mb">
			<h3>
				<fmt:message key="myaccount.emailaddress" />

			</h3>
			<p class="Text">
				<span class="userName"> ${email}</span><span>&#xb7;</span> 
				<a
					data-target="#emailChangeModel" class="changeEmailModal"
					data-toggle="modal" href="javascript:void(0);" title="edit email id" class="" onclick="javascript:forConsoleErrorFixing(this);"><fmt:message
						key="myaccount.edit" /></a>
			</p>
		</div>
		<div>
			<h3>
				<fmt:message key="myaccount.password" /><span>&#xb7;</span><a
					data-target="#myAccountUpdatePasswordModal"
					class="changePasswordModal" data-toggle="modal"
					href="javascript:void(0);" title="edit password" class="" onclick="javascript:forConsoleErrorFixing(this);"><fmt:message
						key="myaccount.edit" /></a>
			</h3>
			<p>
				xxxxxxxxxxxxx
			</p>

		</div>
	</div>


</dsp:page>
