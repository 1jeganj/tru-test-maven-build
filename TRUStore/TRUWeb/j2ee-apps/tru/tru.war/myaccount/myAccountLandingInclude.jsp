<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/invalidatebrowsercache.jspf" %>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration"/>
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<%-- <dsp:importbean bean="/com/tru/integrations/cardinal/CardinalJwtDroplet" /> --%>
<dsp:importbean bean="/com/tru/commerce/droplet/TRURecentlyViewedHistoryCollectorDroplet"/>
<dsp:page>

<%-- Start - Writing hidden input field for expiry month & year for PLCC card --%>
<dsp:getvalueof var="truConf" bean="TRUConfiguration" />
<input type="hidden" name="expMonthForPLCC" id="expMonthForPLCC" value="${truConf.expMonthAdjustmentForPLCC}"/>
<input type="hidden" name="expYearForPLCC" id="expYearForPLCC" value="${truConf.expYearAdjustmentForPLCC}"/>
<%-- End - Writing hidden input field for expiry month & year for PLCC card --%>

<dsp:getvalueof bean="TRUTealiumConfiguration.accountPageName" var="accountPageName"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.accountPageType" var="accountPageType"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.browserID" var="browserID"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.accountInternalCampaignPage" var="internalCampaignPage"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.accountOrsoCode" var="orsoCode"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.accountIspuSource" var="ispuSource"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.deviceType" var="deviceType"/>
<dsp:getvalueof var="myAccountPage" value="My Account"/>

<!--  Added For Tealium Integration -->
<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>

<dsp:getvalueof param="searchKeyWord" var="tSearchKeyWord"/>
<dsp:getvalueof param="pageName" var="tPageName"/>
<dsp:getvalueof param="pageType" var="tPageType"/>
<dsp:getvalueof var="customerEmail" bean="Profile.login"/>
<dsp:getvalueof var="customerId" bean="Profile.id"/>
<dsp:getvalueof var="customerDOB" bean="Profile.dateOfBirth"/>
<c:set var="tPageName" value="/myaccount/myAccountLanding"/>
<input type="hidden" id="customerId" value="${customerId}">
<input type="hidden" id="pageNumber" value="1">
<input type="hidden" id="noOfRecordsPerPage" value="3">
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof var="origin" value="${originatingRequest.Origin}"/>

<dsp:getvalueof var="orderId" bean="ShoppingCart.current.id" />
<input type="hidden" id="orderId" value="${orderId}">
<input type="hidden" id="pushSiteId" value="${site}"/>
<dsp:importbean bean="/com/tru/common/droplet/TRUUrlConstructDroplet"/>
<dsp:droplet name="TRUUrlConstructDroplet">
		<dsp:param name="orderListParam" value="orderList" />
		<dsp:param name="orderDetailParam" value="orderDetail" />
		<dsp:param name="orderCancelParam" value="orderCancel" />
		<dsp:param name="integrationLoggingParam" value="integrationLogging"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var='omsOrderDetailRestAPIURL' param='orderDetailURL'/>
			<input type="hidden" id="omsOrderDetailRestAPIURL" value="${omsOrderDetailRestAPIURL}">
			<dsp:getvalueof var='omsOrderListRestAPIURL' param='orderListURL'/>
			<input type="hidden" id="omsOrderListRestAPIURL" value="${omsOrderListRestAPIURL}">
			<dsp:getvalueof var='omsCancelOrderRestAPIURL' param='orderCancelURL'/>
			<input type="hidden" id="omsCancelOrderRestAPIURL" value="${omsCancelOrderRestAPIURL}">
			<dsp:getvalueof var="truIntegrationLoggingRestAPIURL" param='integrationLoggingURL'/>
			<input type="hidden" id="truIntegrationLoggingRestAPIURL" value="${truIntegrationLoggingRestAPIURL}">
		</dsp:oparam>
</dsp:droplet>
<c:set var="tPageType" value="account"/>

<dsp:getvalueof var="customerFirstName" bean="Profile.firstName"/>
	<dsp:getvalueof var="customerLastName" bean="Profile.lastName"/>
	<c:set var="space" value=" "/>
	<c:choose>
	<c:when test="${empty customerFirstName && empty customerLastName}">
		<c:set var="customerName" value='${fn:substring(customerEmail, 0, fn:indexOf(customerEmail, "@"))}'/>
	</c:when>
	<c:when test="${not empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value="${customerFirstName}"/>
	</c:when>
	<c:when test="${empty customerFirstName && not empty customerLastName}">
		   <c:set var="customerName" value="${customerLastName}"/>
	</c:when>
	<c:otherwise>
			<c:set var="customerName" value="${customerFirstName}${space}${customerLastName}"/>
	</c:otherwise>
 </c:choose>

<%-- <dsp:droplet name="CardinalJwtDroplet">
        <dsp:oparam name="output">
        	<dsp:getvalueof var="jwt" param="jwt"/>
        </dsp:oparam>
</dsp:droplet> 

<input type="hidden" name="jwt" id="jwt" value="${jwt}"/>--%>

<c:set var="customerStatus" value="Registered"/>
<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
		<dsp:droplet name="IsEmpty">
			<dsp:param name="value" bean="Profile.shippingAddress.id" />
			<dsp:oparam name="false">
				<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="Profile.secondaryAddresses"/>
				<dsp:param name="elementName" value="address"/>
				<dsp:oparam name="output">
				<dsp:getvalueof param="address.id" var="addressId" />
				<c:if test="${addressId eq shippingAddressId}">
						 <dsp:getvalueof param="address.city" var="customerCity"/>
						 <dsp:getvalueof param="address.state" var="customerState"/>
						 <dsp:getvalueof param="address.postalCode" var="customerZip"/>
						 <dsp:getvalueof param="address.country" var="customerCountry"/>

				 </c:if>
				</dsp:oparam>
			   </dsp:droplet>
			</dsp:oparam>
</dsp:droplet>

<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
<c:choose>
<c:when test="${isSosVar eq 'true'}">
<c:set var="selectedStore" value="sos"/>
</c:when>
<c:otherwise>
<c:set var="selectedStore" value=""/>
</c:otherwise>
</c:choose>

<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>

<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
<c:if test="${site eq babySiteCode}">
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
</c:if>
	
	 

		<!-- Start Home Main template -->
			<div class="container-fluid my-account-template default-template">
			<input type="hidden" id="isLandingPage" value="true"/>
				<!--Start My Account content -->
				<div class="template-content">
					<dsp:include page="myAccountErrormessageDisplay.jsp"/>
					<div class="my-account-welcome-back-cont">
						<dsp:include page="myAccountProfile.jsp" />
					</div>
					<div class="row">
						<div class="my-account-left-col">
							<hr>
							<div class="orderHistoryCancelOrder hide"><fmt:message key="myaccount.orderHistory.cancelOrderMessage" /></div>
							<div class="orderHistoryCancelOrderError hide"><fmt:message key="myaccount.orderDetails.serviceDown" /></div>
							<div class="my-account-your-info">
								<div class="row">
									<div class="col-md-4">
										<dsp:include page="myAccountUserDetails.jsp" />
										<div id="my-info-default-address">
											<dsp:include page="myAccountAddress.jsp" />
										</div>
									</div>
									<div class="col-md-8">
										<div id="myAccountRewardsSecId">
											<dsp:include page="myAccountRewardsRus.jsp" />
										</div>
										<div id="myAccountCardsSecId">
											<dsp:include page="myAccountCard.jsp" />
										</div>
									</div>
								</div>
							</div>
							<div class="modal fade" id="namChangeModel" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
								<div class="modal-dialog modal-dialog-forgotpassword ">
									<div class="modal-content sharp-border">
										<div class="namChangeModel-overlay">
											<dsp:include page="editPersonalInfo.jsp"/>
										</div>
									</div>
								</div>
							</div>

							<div class="modal fade" id="emailChangeModel" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
								<div class="modal-dialog modal-dialog-forgotpassword ">
									<div class="modal-content sharp-border">
										<div class="emailChangeModel-overlay">
											<dsp:include page="editEmailAddress.jsp"/>
										</div>
									</div>
								</div>
							</div>

							<div class="modal fade" id="myAccountUpdatePasswordModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
								<div class="modal-dialog modal-lg">
									<div class="modal-content sharp-border">
										<div class="my-account-update-password-overlay">
											<dsp:include page="changePassword.jsp"/>
										</div>
									</div>
								</div>
							</div>
							<hr>
							<dsp:include page="myAccountOrderLists.jsp" >
							</dsp:include>
							<div id="myAccountFeatures">
								<%-- <dsp:include page="myAccountFeatures.jsp" /> --%>
							</div>
						</div>
						<div class="my-account-right-col">
							<div id="myAccountQuickHelp">
								<%-- <dsp:include page="myAccountQuickHelp.jsp" /> --%>
							</div>
							<dsp:include page="myAccountGiftcardBalance.jsp" />

							<div class="my-account-norton" id="load-my-account-norton-script">
							<%-- <dsp:include page="../common/nortonSealScript.jsp"/> --%>
							</div>
						</div>
					</div>
				</div>
				<div class="my-account-recommended-products">
					<dsp:include page="recommendedProducts.jsp" />
				</div>
				<div class="modal" id="findInStoreModal" tabindex="-1"
				role="dialog" aria-labelledby="basicModal" aria-hidden="true">
					<dsp:include page="/jstemplate/findInStoreInfo.jsp" />
				</div>
				<!-- End My Account content -->
				<!-- add credit card popup start-->
				<dsp:include page="/myaccount/myAccountCardOverlay.jsp"/>
				<!-- add credit card popup end-->

				<!-- credit card information popup Start-->
				<div class="modal fade" id="myAccountEditCreditCardModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="display: none;">
					<%-- <dsp:include page="editCreditCard.jsp"/> --%>
				</div>
				<!--  credit card information popup End-->
				<!-- Start Address information popup-->
				<dsp:include page="/myaccount/myAccountAddressOverlay.jsp" />
				<!-- End Address information popup  -->
			</div>
			<!-- Start Home Address information popup -->
			<!-- End Home Address information popup -->

	<dsp:droplet name="TRURecentlyViewedHistoryCollectorDroplet">
		<dsp:param name="profile" bean="/atg/userprofiling/Profile" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="recentlyViewedItems" param="recentViewedItemsParam"/>
			<iframe id="nonSecureRecentlyViewProducts" src="/securePage/nonSecurePage.jsp"></iframe>
			<c:if test="${not empty recentlyViewedItems}">
				<script>
					createLocalStorageforSignedInUser(${recentlyViewedItems},'myAccount');
				</script>
			</c:if>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>
