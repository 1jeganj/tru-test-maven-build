<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<%-- for shipping messages --%>
<fmt:message key="tru.myaccount.addresUpdationMessage" var="addresUpdationMessage"/> 
<fmt:message key="tru.myaccount.addresDeletionMessage" var="addresDeletionMessage"/>
<fmt:message key="tru.myaccount.cardSavedMessage" var="cardSavedMessage"/>
<fmt:message key="tru.myaccount.cardDeletionMessage" var="cardDeletionMessage"/>
<fmt:message key="tru.myaccount.cardRemovedSuccessfully" var="cardRemovedSuccessfully"/>
<fmt:message key="tru.myaccount.nameUpdationMessage" var="nameUpdationMessage"/>
<fmt:message key="tru.myaccount.emailAddressupdate" var="emailAddressupdate"/>
<fmt:message key="tru.myaccount.passwordUpdate" var="passwordUpdate"/>
<fmt:message key="tru.myaccount.rewardsMessageUpdate" var="rewardsMessageUpdate"/>
<fmt:message key="tru.myaccount.sucessRemoval" var="sucessRemoval"/>
<fmt:message key="tru.myaccount.enterGiftCardAndPin" var="enterGiftCardAndPin"/>
<script type="text/javascript">
var myacc_addresUpdationMessage = "${fn:escapeXml(addresUpdationMessage)}" ;
var myacc_addresDeletionMessage = "${fn:escapeXml(addresDeletionMessage)}" ;
var myacc_cardSavedMessage = "${fn:escapeXml(cardSavedMessage)}" ;
var myacc_cardDeletionMessage = "${fn:escapeXml(cardDeletionMessage)}" ;
var myacc_cardRemovedSuccessfully = "${fn:escapeXml(cardRemovedSuccessfully)}" ;
var myacc_nameUpdationMessage = "${fn:escapeXml(nameUpdationMessage)}" ;
var myacc_emailAddressupdate = "${fn:escapeXml(emailAddressupdate)}" ;
var myacc_passwordUpdate = "${fn:escapeXml(passwordUpdate)}" ;
var myacc_rewardsMessageUpdate = "${fn:escapeXml(rewardsMessageUpdate)}" ;
var myacc_sucessRemoval = "${fn:escapeXml(sucessRemoval)}" ;
var myacc_enterGiftCardAndPin = "${fn:escapeXml(enterGiftCardAndPin)}" ;  
 

</script>	
</dsp:page>