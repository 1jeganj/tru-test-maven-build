<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<!-- Start : Cardianl Turn off / on changes -->
<dsp:importbean bean="/com/tru/common/TRUIntegrationConfiguration" />
<dsp:importbean bean="/com/tru/radial/common/TRURadialConfiguration" />
<dsp:getvalueof var="enableVerboseDebug" bean="TRURadialConfiguration.enableVerboseDebug"/>
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
<dsp:getvalueof var="storeCardinal" bean="TRUIntegrationConfiguration.enableCardinal"/>
<dsp:getvalueof var="enableIntegrationLog" bean="TRURadialConfiguration.enableIntegrationLog"/>
<input type="hidden" name="cardinalEnabled" id="cardinalEnabled" value="${storeCardinal}"/>
<input type="hidden" name="enableVerboseDebug" id="enableVerboseDebug" value="${enableVerboseDebug}"/>
<input type="hidden" name="enableIntegrationLog" id="enableIntegrationLog" value="${enableIntegrationLog}"/>
<dsp:getvalueof var="maxNonceRetryAttempts" bean="TRURadialConfiguration.maxNonceRetryAttempts"/>
<dsp:getvalueof var="maxGetTokenRetryAttempts" bean="TRURadialConfiguration.maxGetTokenRetryAttempts"/>
<!-- End : Cardianl Turn off / on changes -->
		
				
		<!-- Changes for MY Account landing page -->
		<div id="ajaxMyAccountLandingPage"></div>
		
		<div id="myAccountRecommendedProducts"></div>
		<!-- Changes for MY Account landing page -->
		
		<div class="modal fade in" id="myAccountCardDeleteModalLanding" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false" >
				<div class="modal-dialog modal-lg">
					<div class="modal-content sharp-border">
						<div class="my-account-delete-cancel-overlay">
							<div>
								<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
							</div>
							<h2><fmt:message key="myaccount.delete.credit.card" /></h2>
							<p><fmt:message key="myaccount.sure.remove.card" /></p>
							<div>
							<form id="removeCreditCardFormId" method="POST" onsubmit="javascript: return removeCardLanding();">
								<input type="hidden" id="removeCardHiddenLandingId" name="removeCardHiddenLandingName" value="" />								
								<input type="submit" id="removeCardLandingId" name="removeCardLandingId" value="confirm"  style="display: none;"/>								
                                <button onclick="return deleteCardLanding();"><fmt:message key="myaccount.confirm" /></button><a href="javascript:void(0)" data-dismiss="modal"><fmt:message key="myaccount.cancel" /></a>
							</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			 <!-- delete rewards card popup start-->
			<div class="modal fade in" id="myAccountRemoveRewardCardNumModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false" >
			
					<div class="modal-dialog modal-lg">
						<div class="modal-content sharp-border">
							<div class="my-account-delete-cancel-overlay">
								<div>
									<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
								</div>
								<h2><fmt:message key="myaccount.delete.card.number" /></h2>
				                <p><fmt:message key="myaccount.sure.delete.rewardscard" /></p>
				                <div>
									<form id="confirmRemoveMembershipForm" method="POST" onsubmit="javascript: return removeRewardsCard();">
										
										<input type="submit" id="removeMembershipIdPopup" name="removeMembershipIdPopup" value="confirm"  style="display: none;"/>
										
			                               <button onclick="return removeRewardsCardConfirm();"><fmt:message key="myaccount.confirm" /></button><a href="javascript:void(0)" data-dismiss="modal"><fmt:message key="myaccount.cancel" /></a>
									</form>
								</div>
							</div>
						</div>
					</div>
			</div>
			
			<div id="myAccountFeaturesContent" class="hide"><dsp:include page="myAccountFeatures.jsp" /></div>
			<div id="myAccountQuickHelpContent" class="hide"><dsp:include page="myAccountQuickHelp.jsp" /></div>
			
	<script>
	$(window).load(function(){
		maxNonceRetryAttempts = ${maxNonceRetryAttempts};
		maxGetTokenRetryAttempts = ${maxGetTokenRetryAttempts};
		if($('#cardinalEnabled').val() == 'true'){
			isCardinalEnabled = true;
		} else {
			isCardinalEnabled = false;
		}
		if($('#enableVerboseDebug').val() == 'true'){
			enableVerboseDebug = true;
		} else {
			enableVerboseDebug = false;
		}
		if($('#enableIntegrationLog').val() == 'true'){
			enableIntegrationLog = true;
		} else{
			enableIntegrationLog = false;
		}
	});
	</script>
</dsp:page>
