<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
<dsp:importbean bean="/atg/targeting/TargetingFirst" />
<dsp:importbean bean="/com/tru/common/TRUIntegrationConfiguration" />
<dsp:setvalue bean="ProfileFormHandler.extractDefaultValuesFromProfile"	value="false" />
<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale" />
<dsp:getvalueof param="sessionExpired" var="sessionExpired"/>
<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
<dsp:getvalueof var="siteCode" value="TRU"/>
<c:if test="${site eq babySiteCode}">
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
</c:if>

<!-- Certona parameter Start -->

<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
<dsp:getvalueof var="toysSiteCode" bean="TRUTealiumConfiguration.toysSiteCode"/>
<!-- Certona parameter End -->

<dsp:page>

<dsp:getvalueof bean="TRUTealiumConfiguration.accountPageName" var="accountPageName"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.accountPageType" var="accountPageType"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.browserID" var="browserID"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.accountInternalCampaignPage" var="internalCampaignPage"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.accountOrsoCode" var="orsoCode"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.accountIspuSource" var="ispuSource"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.deviceType" var="deviceType"/>
<dsp:getvalueof var="myAccountPage" value="My Account"/>

<!--  Added For Tealium Integration -->
<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>

<dsp:getvalueof param="pageName" var="tPageName"/>
<dsp:getvalueof param="pageType" var="tPageType"/>
<dsp:getvalueof var="customerEmail" value=""/>
<dsp:getvalueof var="customerId" bean="Profile.id"/>
<dsp:getvalueof var="customerDOB" value=""/>
<dsp:getvalueof var="storeCertona" bean="TRUIntegrationConfiguration.enableCertona"/>
<c:set var="tPageName" value="/myaccount/myAccountLanding"/>

<c:set var="tPageType" value="account"/>
<c:set var="customerName" value=""/>
<c:set var="customerStatus" value="Guest"/>
<c:set var="customerCity" value=""/>
<c:set var="customerState" value=""/>
<c:set var="customerZip" value=""/>
<c:set var="customerCountry" value=""/>

<c:set var="certona" value="${storeCertona}" scope="request"/>
<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
<c:choose>
<c:when test="${isSosVar eq 'true'}">
<c:set var="selectedStore" value="sos"/>
</c:when>
<c:otherwise>
<c:set var="selectedStore" value=""/>
</c:otherwise>
</c:choose>

<!-- Start Home Main template -->
<div class="container-fluid my-account-sign-in-template default-template">
	<input type="hidden" id="isLandingPage" value="false"/>
    	<!--Start My Account content -->
	<div class="template-content">
        <div class="my-account-sign-in-top-container">
            <div class="my-account-sign-in-main-heading">
                <div class="row">
                    <div class="col-md-12">
                        <h1><fmt:message key="myaccount.signin.signup" /></h1>
                        <p><fmt:message key="myaccount.signin.message" /></p>
						<p class="format-error"><fmt:message key="myaccount.resetpassword.migrationmessage1" /></p>
                    </div>
                </div>
            </div>
            <div class="sign-in-columns-container">
            	<c:if test="${sessionExpired}">
            		<span class="errorDisplay">
           				<fmt:message key="myaccount.sessionexpired.message" />
           			</span>
           		</c:if>
                <div class="row <c:if test='${sessionExpired}'>adjustSession</c:if>">
                    	<dsp:include page="signIn.jsp"/>
                    	<dsp:include page="createAccount.jsp"/>
                    	<dsp:include page="orderStatus.jsp"/>
                </div>
                <div class="row">
                    <div class="col-md-12">
                    	<dsp:getvalueof var="atgTargeterpath1" value="/atg/registry/RepositoryTargeters/TRU/Checkout/ShopWithConfidenceTargeter" />
						<dsp:getvalueof var="cacheKey1" value="${atgTargeterpath1}${siteId}${locale}" />
						<dsp:droplet name="/com/tru/cache/TRUShopWithConfidenceCacheDroplet">
							<dsp:param name="key" value="${cacheKey1}" />
							<dsp:oparam name="output">
								<dsp:droplet name="TargetingFirst">
									<dsp:param name="targeter" bean="${atgTargeterpath1}" />
									<dsp:oparam name="output">
										<dsp:valueof param="element.data" valueishtml="true" />
									</dsp:oparam>
								</dsp:droplet>
							</dsp:oparam>
						</dsp:droplet>
                    </div>
                </div>
            </div>
            
			<dsp:getvalueof var="atgTargeterpath2" value="/atg/registry/RepositoryTargeters/TRU/AccountManagement/EasyReturnsTargeter" />
			<dsp:getvalueof var="cacheKey2" value="${atgTargeterpath2}${siteId}${locale}" />
			<dsp:droplet name="/com/tru/cache/TRUEasyReturnsCacheDroplet">
				<dsp:param name="key" value="${cacheKey2}" />
				<dsp:oparam name="output">
		       		 <dsp:droplet name="TargetingFirst">
						<dsp:param name="targeter" bean="${atgTargeterpath2}" />
						<dsp:oparam name="output">
							<dsp:valueof param="element.data" valueishtml="true" />
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>

			<dsp:getvalueof var="atgTargeterpath3" value="/atg/registry/RepositoryTargeters/TRU/AccountManagement/SecureShoppingGuaranteeTargeter" />
			<dsp:getvalueof var="cacheKey3" value="${atgTargeterpath3}${siteId}${locale}" />
			<dsp:droplet name="/com/tru/cache/TRUSecureShoppingGuaranteeCacheDroplet">
				<dsp:param name="key" value="${cacheKey3}" />
				<dsp:oparam name="output">
					<dsp:droplet name="TargetingFirst">
						<dsp:param name="targeter" bean="${atgTargeterpath3}" />
						<dsp:oparam name="output">
							<dsp:valueof param="element.data" valueishtml="true" />
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>

			<dsp:getvalueof var="atgTargeterpath4" value="/atg/registry/RepositoryTargeters/TRU/AccountManagement/PrivacyPolicyTargeter" />
			<dsp:getvalueof var="cacheKey4" value="${atgTargeterpath4}${siteId}${locale}" />
			<dsp:droplet name="/com/tru/cache/TRUPrivacyPolicyCacheDroplet">
				<dsp:param name="key" value="${cacheKey4}" />
				<dsp:oparam name="output">
					<dsp:droplet name="TargetingFirst">
						<dsp:param name="targeter" bean="${atgTargeterpath4}" />
						<dsp:oparam name="output">
							<dsp:valueof param="element.data" valueishtml="true" />
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
        </div>
    </div>
    	<!-- <div class="my-account-product-carousel">
 -->

        <!-- <div class="slider-product-block-container">

            <div debug-id="slide_container"></div></div> -->

 	   	<!-- Certona related Divs Start -->
 	   	<c:if test="${certona eq 'true'}">
		  	<c:choose>
				<c:when test="${site eq toysSiteCode}">
					<div class="full_width_gray">
						<div id="tmyaccount_rr">
							<!-- Toysrus account not logged in page recommendations appear here  -->
						</div>
					</div>
					<dsp:getvalueof var="site" value="TRU" />
				</c:when>
				<c:otherwise>
					<div class="full_width_gray">
						<div id="bmyaccount_rr">
						 	<!-- Babiesrus account not logged in page recommendations appear here -->
						 </div>
					</div>
					<dsp:getvalueof var="site" value="BRU" />
				</c:otherwise>
			</c:choose>
				<div id="rdata" style="visibility:hidden;">
			 		<div id="site">${site}</div>
			 		<div id="customerid">${customerId}</div>
				</div>
			</c:if>
			<!-- Certona related Data End -->
	<!-- </div> -->

	    <!-- End My Account content -->
</div>



	

<!-- /End Home Main template -->


</dsp:page>

