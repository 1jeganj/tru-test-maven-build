<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/ForgotPasswordHandler"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
	<div class="forgotpasswordmodel-overlay">
		<div class="row">
			<div class="col-md-12">
				 <a href="javascript:void(0)" data-dismiss="modal"><span class="clickable"><span class="sprite-icon-x-close"></span></span> </a>
			</div>
		</div>
		<dsp:getvalueof var="email" param="email"></dsp:getvalueof>
		<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
		<div class="row">
			<div class="col-md-12 set-height ourVal">
				<div class="row">
					<div class="col-md-11">
					
					
						<h2 class=""><fmt:message key="myaccount.reset.your.password" /></h2>
						<p><fmt:message key="myaccount.enter.emailaddress.message" /></p>
						<p><fmt:message key="myaccount.send.email.message" /></p>
						
					</div>
					
				</div>
				<form id="forgotPassowrd" class="JSValidation" onsubmit="javascript: return forgotPassowrd();" >
				<p class="global-error-display"></p>
				<p id="emailSentSuccess"></p>
					<div class="row">
						<div class="col-md-9">
							<label class="avenir-heavy" for="loginForgotPwd" id="forgetEmailLabel">
								<fmt:message key="myaccount.email" />
							</label>
							<input type="text" id="loginForgotPwd" name="email" value="${email}" maxlength="60"/>
						</div>
					</div>
					<div class="row ">
						<div class="col-md-12 button-container">
							<input type="submit" value="reset password" class="passResetForm" id="passResetForm"  />
						</div>
					</div>
				</form> 
			</div>
		</div>
	</div>
</dsp:page>