<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:getvalueof bean="Profile.firstName" var="firstName"/>
<dsp:getvalueof bean="Profile.lastName" var="lastName"/>
<%-- start: popup for edit personal info --%>
	
	<div class="update-name-header">
		<a href="javascript:void(0)" class="closeDailog" data-dismiss="modal">
			<span class="sprite-icon-x-close"></span>
		</a>
		<div>
			<h2 class="custmHeading"><fmt:message key="myaccount.update.name" /></h2>
		</div>
	</div>
	<div class="update-name-body">
		<form class="JSValidation" id="personalInfoFormValidation" >
			<div>
				<label for="firstname"><fmt:message key="myaccount.firstname" /></label>
				<input type="text" name="firstName" id="firstname" maxlength="30"  value="${firstName}" />
			</div>
			<div>
				<label for="lastname"><fmt:message key="myaccount.lastname" /></label>
				<input type="text" name="lastName" id="lastname" maxlength="30"  value="${lastName}"/>
			</div>
			<div>
				<input type="submit"  value="save changes" class="submit-btn personal-submit-btn" id="personal-submit-btn"  />  		
			</div>
		</form>
	</div>
			
<%-- end: popup for edit personal info --%>
</dsp:page>