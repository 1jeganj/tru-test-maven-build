<%@ page isELIgnored="true"%>
<dsp:page>
	<script type='text/x-jquery-tmpl' id='myaccount-order-history-template'>
				{{if orders != null}}
					{{if typeof orders.customerTransactionListResponse != "undefined" &&  orders.customerTransactionListResponse != null}}
					{{if orders.customerTransactionListResponse.messages.message.description== 'InvalidCustomerId'}}
						<p class="errorMsg">${customerOrderDetails.noOrderMessage}</p>
					{{/if}}
					{{if orders.customerTransactionListResponse.messages.message.description== 'ServiceDown'}}
						<p class="errorMsg">${customerOrderDetails.serviceDown}</p>
					{{/if}}
					{{else}}
                {{each orders}} 
					<div class="order-history-order row">
                        <div class="col-md-3">
                            ${OrderDate}
                        </div>
                        <div class="col-md-3">
                            <a href="" class="orderHistoryDetailModal" data-toggle="modal" title="${WebOrderId}">${WebOrderId}</a>
                        </div>
                        <div class="col-md-3">
							<p>${OrderStatus}</p>
							<a href="javascript:void(0)" class="orderHistoryDetailModal" data-toggle="modal" title="${WebOrderId}">${customerOrderDetails.seeDetails}</a>
                            {{if OrderStatus=='New'}}   
								<br>       
		                  		<a href="javascript:void(0)" data-toggle="modal" data-target="#orderHistoryCancelOrder" class="cancelOrder">${customerOrderDetails.cancelOrder}</a>
                       		{{/if}}
                        </div>
                        <div class="col-md-3 pull-right">
                            ${customerOrderDetails.dollar}${OrderTotal}
                        </div>
                    </div>
				{{/each}}
				{{/if}}
				{{else}}
				<p>${customerOrderDetails.noOrderMessage}</p>
				{{/if}}
</script>

<script type='text/x-jquery-tmpl' id='myaccount-order-history-details'>


{{if typeof customerOrder.CustomerOrderDetailResponse == "undefined"}}
<div class="modal-dialog order-history-detail-modal">
	<div class="modal-content sharp-border">
		<div class="modal-title">
			${customerOrderDetails.orderDetail} <a href="javascript:void(0)"
				data-dismiss="modal">
				<span class="sprite-icon-x-close"></span>
				</a>
			<div>${customerOrderDetails.orderedOn}
				${customerOrder.OrderHeader.OrderDate} ${customerOrderDetails.order}
				${customerOrderDetails.hash}${customerOrder.OrderHeader.PartnerOrderId}
			</div>
		</div>
			<div class="tse-scrollable">
				<div class="tse-scrollbar" style="display: none;">
					<div class="drag-handle visible" style="top: 34px; height: 185px;"></div>
				</div>
				<div class="tse-scroll-content tse-order-history-content" style="height: 580px;">
					<div class="tse-content order-history-detail-modal-content">
					 <div class="row">
						<div class="col-md-8 col-no-padding">
							<div class="row">
								<div class="col-md-5 col-no-padding">
									{{if customerOrder.OrderHeader.OrderPaymentList != null}}
									<div class="order-detail-small-header">${customerOrderDetails.billingAddress}</div>
									 <br>
									<div class="address">
										<span>${customerOrder.OrderHeader.BillingAddress.CustomerName}</span>
										<div>${customerOrder.OrderHeader.BillingAddress.Line1}</div>
										<div>${customerOrder.OrderHeader.BillingAddress.City}${customerOrderDetails.comma}${customerOrder.OrderHeader.BillingAddress.State}
											${customerOrder.OrderHeader.BillingAddress.PostalCode}</div><div>${customerOrder.OrderHeader.BillingAddress.Phone}</div><div>${customerOrder.OrderHeader.BillingAddress.CountryCode}</div>
										</div>
                                            <br>
											{{if customerOrder.OrderHeader.LOYALTY_ACCOUNT != null}}
												Rewards"R"Us Member #:${customerOrder.OrderHeader.LOYALTY_ACCOUNT}
											{{else}}
												No Rewards Cards To Display	
											{{/if}}
											{{else}}
											{{if customerOrder.OrderHeader.LOYALTY_ACCOUNT != null}}
											<div class="order-detail-small-header">${customerOrderDetails.billingAddress}</div>
											<br>
											<div class="address">
												Rewards"R"Us Member #:${customerOrder.OrderHeader.LOYALTY_ACCOUNT}
											</div>
                                            <br>
											{{else}}
											<div class="order-detail-small-header">${customerOrderDetails.billingAddress}</div>
											<br>
											<div class="address">
												No Billing Information	
											</div>
                                            <br>
											{{/if}}	
											{{/if}}
									<p class="returns-warranties">
                                                <a href=${customerOrder.helpAndReturns}>${customerOrderDetails.helpReturnWarranties}</a>
                                            </p>
										       {{if customerOrder.OrderHeader.OrderStatus == 'New'}}   
											<div>
												<input type= "hidden" class="hiddenOrderNumber" value=${customerOrder.OrderHeader.PartnerOrderId}>	
												<a onclick="cancelOrder(false);" href="javascript:void(0)" data-toggle="modal" data-target="#orderHistoryCancelOrder" class="cancelOrder">cancel order</a>                                                          
 											</div>
                                           {{/if}}
								</div>
								<div class="col-md-5 col-no-padding">
									{{if customerOrder.OrderHeader.OrderPaymentList != null}}
									<div class="order-detail-small-header">${customerOrderDetails.paymentMethod}</div>
									<br>
									<div class="payment-method">
										{{each customerOrder.OrderHeader.OrderPaymentList.OrderPayment}}  
                                        {{if PaymentType == 'Credit Card' || PaymentType == 'CreditCard'}} 
										{{if PaymentMethod == 'Visa Card' || PaymentMethod == 'Visa'}} 
											<span>${PaymentMethod}</span>
										<div class="card-number">
											<div class="visa-logo-sm inline"></div>
											<div class="inline">${AccountNumber}</div>
										</div>
										{{/if}} 
										{{if PaymentMethod == 'Master Card'}} 
                                            <span>${PaymentMethod}</span>
										<div class="card-number">
											<div class="mastercard-logo-sm inline"></div>
											<div class="inline">${AccountNumber}</div>
										</div>
										{{/if}} 
										{{if PaymentMethod == 'MasterCard'}}
                                            <span>${PaymentMethod}</span>
										<div class="card-number">
											<div class="mastercard-logo-sm inline"></div>
											<div class="inline">${AccountNumber}</div>
										</div>
										{{/if}} 
										{{if PaymentMethod == 'Amex'}}
                                           <span>${PaymentMethod}</span>
										<div class="card-number">
											<div class="amex-logo-sm inline"></div>
											<div class="inline">${AccountNumber}</div>
										</div>
										{{/if}} {{if PaymentMethod == 'Discover'}}
                                            <span>${PaymentMethod}</span>
										<div class="card-number">
											<div class="discover-logo-sm inline"></div>
											<div class="inline">${AccountNumber}</div>
										</div>
										{{/if}} 
										{{if PaymentMethod == 'RUS Co Branded CC'}}
                                          					 <span>${PaymentMethod}</span>
										<div class="card-number">
											<div class="rewardsrus-card-logo-sm inline"></div>
											<div class="inline">${AccountNumber}</div>
										</div>
										{{/if}}
										{{if PaymentMethod == 'Co-Brand Master Card'}}
                                           <span>${PaymentMethod}</span>
										<div class="card-number">
											<div class="rewardsrus-card-logo-sm inline"></div>
											<div class="inline">${AccountNumber}</div>
										</div>
										{{/if}} 
										{{if PaymentMethod == 'Private Label Card' || PaymentMethod == 'TRU Private Label CC'}}
                                           <span>${PaymentMethod}</span>
										<div class="card-number">
											<div class="rewardsrus-mastercard-logo-sm inline"></div>
											<div class="inline">${AccountNumber}</div>
										</div>
										{{/if}}
										{{if PaymentMethod == 'PayPal'}}
                                           <span>${PaymentMethod}</span>
										<div class="card-number">
											<div class="inline">${AccountNumber}</div>
										</div>
										{{/if}}
										<div>${customerOrderDetails.dollar}${Amount} ${customerOrderDetails.balanceText}</div>
										{{else}} 
											<span>${PaymentMethod}</span>
										 {{if PaymentType =='Gift Card'}}
										<div class="inline"></div>
										<div class="card-number">
											<div class="inline">${AccountNumber}</div>
										</div>
												{{/if}} 
											{{/if}}
										{{/each}}
									</div>
									{{/if}}
								</div>
							</div>
						</div>
					<div class="col-md-3 col-no-padding pull-right">
                                    <div class="order-history-order-summary shopping-cart-summary-block">
                                        <div class="summary-block-border">
                                            <div class="summary-block-padded-content" tabindex="0">
                                                <div class="order-summary">
                                                   ${customerOrderDetails.orderSummary}
                                                </div>
                                                <div class="subtotal order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.subTotal} (${customerOrder.OrderHeader.orderdLineItems} ${customerOrderDetails.items})</span>
                                                    <span class="summary-price pull-right order-summary-item-value">${customerOrderDetails.dollar}${customerOrder.OrderHeader.OrderSummary.OrderSubtotal}</span>
                                                </div>
												{{if customerOrder.OrderHeader.OrderSummary.PromotionSavingsAmount > 0}}
											    <div class="summary-promotions order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.promotionalSavings}</span>
                                                    <span class="summary-price pull-right order-summary-item-value">-${customerOrderDetails.dollar}${customerOrder.OrderHeader.OrderSummary.PromotionSavingsAmount}</span>
                                                </div>	
												{{/if}}
												{{if customerOrder.OrderHeader.OrderSummary.GiftWrapAmount > 0}}
                                                <div class="summary-gift-wrap order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.giftWrap}</span>
                                                    <span class="summary-price pull-right order-summary-item-value">${customerOrderDetails.dollar}${customerOrder.OrderHeader.OrderSummary.GiftWrapAmount}</span>
                                                </div>
												{{/if}}
												{{if customerOrder.OrderHeader.OrderSummary.ShippingAmount > 0}}
                                                <div class="summary-shipping order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.shipping}</span>
                                                    <span class="summary-price order-summary-item-value">${customerOrderDetails.dollar}${customerOrder.OrderHeader.OrderSummary.ShippingAmount}</span>
                                                </div>
												{{else}} 
												{{if  customerOrder.OrderHeader.onlyDonationExists == false}}
												 <div class="summary-shipping order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.shipping}</span>
                                                    <span class="summary-price order-summary-item-value">${customerOrderDetails.free}</span>
                                                </div>
												{{/if}}
												{{/if}}
												{{if customerOrder.OrderHeader.OrderSummary.ShippingSurchargeAmount > 0}}
											    <div class="summary-shipping-surcharge order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.surcharge}</span>
                                                    <span class="summary-price pull-right order-summary-item-value">${customerOrderDetails.dollar}${customerOrder.OrderHeader.OrderSummary.ShippingSurchargeAmount}</span>
                                                </div>		
												{{/if}}
												{{if  customerOrder.OrderHeader.OrderSummary.SalesTaxAmount >= 0}}
											    <div class="summary-sales-tax order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.salesTax}</span>
                                                    <span class="summary-price pull-right order-summary-item-value">${customerOrderDetails.dollar}${customerOrder.OrderHeader.OrderSummary.SalesTaxAmount}</span>
                                                </div>		
												{{/if}}
                                     			{{if customerOrder.OrderHeader.OrderSummary.LocalTaxAmount > 0}}
											    <div class="summary-sales-tax order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.localTaxAmount}</span>
                                                    <span class="summary-price pull-right order-summary-item-value">${customerOrderDetails.dollar}${customerOrder.OrderHeader.OrderSummary.LocalTaxAmount}</span>
                                                </div>		
												{{/if}}
                                                {{if  customerOrder.OrderHeader.OrderSummary.IslanTaxAmount > 0}}
											    <div class="summary-sales-tax order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.islanTaxAmount}</span>
                                                    <span class="summary-price pull-right order-summary-item-value">${customerOrderDetails.dollar}${customerOrder.OrderHeader.OrderSummary.IslanTaxAmount}</span>
                                                </div>		
												{{/if}}
												 {{if  customerOrder.OrderHeader.OrderSummary.EwasteAmount > 0}}
											    <div class="summary-shipping-surcharge order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.ewaste}</span>
                                                    <span class="summary-price pull-right order-summary-item-value">${customerOrderDetails.dollar}${customerOrder.OrderHeader.OrderSummary.EwasteAmount}</span>
                                                </div>		
												{{/if}}												
												{{if  customerOrder.OrderHeader.OrderSummary.GiftCardAmount > 0}}
											    <div class="summary-shipping-surcharge order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.giftCard}</span>
                                                    <span class="summary-price pull-right order-summary-item-value">${customerOrderDetails.dollar}${customerOrder.OrderHeader.OrderSummary.GiftCardAmount}</span>
                                                </div>
												{{if  customerOrder.OrderHeader.OrderSummary.BalanceAmount > 0}}
											    <div class="summary-gift-card order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.balance}</span>
                                                    <span class="summary-price pull-right order-summary-item-value">${customerOrderDetails.dollar}${customerOrder.OrderHeader.OrderSummary.BalanceAmount}</span>
                                                </div>
												{{/if}}
												{{else}}
                                    			 <div class="footer-total">
                                                    <span class="summary-block-footer-total">${customerOrderDetails.balance}</span>
                                                    <span class="summary-price order-summary-item-value">${customerOrderDetails.dollar}${customerOrder.OrderHeader.OrderSummary.OrderTotal}</span>
                                                </div>	
												{{/if}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
				<hr class="horizontal-divider">
                            <div class="pull-right">
                                <div class="norton-secured">
                                    <img src="${TRUImagePath}images/Global_Norton-Logo.jpg" alt="Norton Logo">
                                </div>
                            </div>
				<div class="row">
									<div class="col-md-9 col-no-padding">
									<div class="checkout-review-items-shipping-to items-shipping-to-no-edit">
										{{each  customerOrder.DisplayOrderArray}}
											<div class="row">
												<div class="col-md-12 items-shipping registry">
													<div class="">
													{{if DeliveryMode == 'DONATION'}}
													donation
													{{else}}
													{{if DeliveryMode == 'PICKUP' || DeliveryMode == 'ISPU'}}
														${TotalLineItemCount} ${customerOrderDetails.itemsPickupInstore} ${customerOrderDetails.at}
														<div class="items-shipping-address"> 
															 ${ShippingAddress.storeName}<span>&#xb7;</span>${ShippingAddress.Address.Line1}
															 
															${ShippingAddress.Address.City},${ShippingAddress.Address.State},${ShippingAddress.Address.PostalCode}
														</div>
														<div class="">
														{{if IspuItem}}
														{{if ShippingAddress.Address.CustomerName != "" && ShippingAddress.Address.CustomerName != null}}
															${customerOrderDetails.pickedUpBy} ${ShippingAddress.Address.CustomerName}
														{{/if}}
														{{if isProxyExists}}
														{{if ProxyAddress.Address.CustomerName != "" && ProxyAddress.Address.CustomerName != null}}
														 ${customerOrderDetails.andOr} ${ProxyAddress.Address.CustomerName}
														{{/if}}
														{{/if}}
														{{/if}}
														</div>
													{{else}}
														${TotalLineItemCount} ${customerOrderDetails.itemsShipping} ${ShippingAddress.CustomerName} 
													<div class="items-shipping-address">
														${ShippingAddress.Line1}, ${ShippingAddress.City}, ${ShippingAddress.State} ${ShippingAddress.PostalCode}
													 </div>
													{{/if}}
													{{/if}}
												</div>
											</div>
										</div>
									{{each shippmentInfo}}
											 <div class="row extra-information">
												<div class="col-md-12">
													<div class="order-shipped-on">
														{{if DeliveryMode != 'DONATION'  && DeliveryMode != 'PICKUP' && DeliveryMode != 'Ship to store'}}
														{{if trackingExists == true}}
														 <div class="row extra-information">
														<div>
														<div class="order-shipped-on">
															<p>${customerOrderDetails.shipment} ${shippmentCount} of ${totalshippmentInfoCount} shipped on ${trackOrderShipDate}<span>·</span>${customerOrderDetails.trackingNumber}<a class="review-order-tracking-number" href=${TrackingUrl}>${TrackingNumber}</a>
															</p>
														<header>${shipVia}</header>
																</div>
															</div>
														</div>
														<hr>
														{{else}}
														 <p>${customerOrderDetails.shipment} ${shippmentCount} of ${totalshippmentInfoCount}</p>
														{{/if}}
														{{/if}}
														{{if  customerOrder.OrderHeader.t1OrderExists == false}}
														{{if  dateExists == true}}
														<p>You can expect to recieve your items between ${EDDSTARTDATE} and ${EDDENDDATE} </p>
														{{/if}}    
														{{/if}} 
														
												{{each LineDetails}}
												{{if customerOrder.OrderHeader.t1OrderExists == false}}
													 <header>${ShippingMethod}</header> 
												{{/if}} 
													{{if isGiftMessage}}	
													<div class="gift-message">
														<header>${customerOrderDetails.giftMessage}</header>
														<p class="giftMessage"> ${GiftMessage}</p>	
													</div>
													{{/if}} 
												</div>
											

										  <hr>
										<div class="checkout-review-product-block product-block-no-edit gift-wrapped purchased-protection-plan">
											<div class="checkout-review-egift-card">
												<h2>eGift Card 1 of 2</h2>
												<p>To: Von</p>
												<!-- <hr class="horizontal-divider"> -->
											</div>
											<div class="product-information">
												<div class="product-information-header">
												{{if customerOrder.OrderHeader.t1OrderExists == false}}
												{{if shippmentInfo.DeliveryMode  != 'DONATION'}}
													<div class="product-header-title">
														<a href=${PDPURL}>${ItemDescription}</a>
													</div>
												{{else}}
												<div class="product-header-title">
														Donation Item
													</div>
												{{/if}}
												{{else}}
												{{if shippmentInfo.DeliveryMode  != 'DONATION'}}
													<div class="product-header-title">
														${ItemDescription}
													</div>
												{{else}}
												<div class="product-header-title">
														Donation Item
												</div>
												{{/if}}
												{{/if}}
													<div class="product-header-price">
														{{if customerOrder.OrderHeader.t1OrderExists == false}}
															${customerOrderDetails.dollar}${itemPrice}
														{{else}}
															${customerOrderDetails.dollar}${Price}
														{{/if}}
														<div class="shipping-surcharge">
														   ${customerOrderDetails.dollar}${surcharge}
														</div>
													</div>
												</div>
												<div class="product-information-content">
													<div class="row row-no-padding">
														<div class="col-md-2">
														<div>
														{{if imageURLExists == true}}
																<img class="product-description-image" src=${IMAGEURL} alt="shopping cart product image">
														{{else}}
															<img class="img-responsive product-description-image image-not-available" src="/images/no-image500.gif?fit=inside|124:124" alt="product image"/>
														{{/if}}
														</div>
														</div>
														<div class="col-md-6 donation-amount"></div>
														<div class="col-md-5 product-information-description" tabindex="0">
															{{if itemColor}}
															<div class="product-description-color">${customerOrderDetails.color}${Color}</div>
															{{/if}}
															{{if itemSize}}
															<div class="product-description-size">${customerOrderDetails.size}${Size}</div>
															{{/if}}
															{{if DeliveryMode != 'DONATION'}}
																  <div class="qty">${customerOrderDetails.quantity}${OrderQuantity}</div>
															{{else}}
															<div class="qty">
																	  Thank you for your donation!
															  </div>
															{{/if}}
															{{if isGiftWrap == true}}
															<div class="checkout-item-gift-wrapped">
																<img src="${TRUImagePath}images/item-gift-wrapped.png" alt="Image could not be loaded">
																<div>${customerOrderDetails.giftWrapText}${siteNameForGW} gift wrap</div>
															</div>
															{{/if}}

															<p>
																</p><div class="edit"><a href="http://cloud.toysrus.resource.com/redesign/template-my-account-main.html#">edit</a><span>·</span><a href="http://cloud.toysrus.resource.com/redesign/template-my-account-main.html#">remove</a>
																</div>
															<p></p>
														</div>
														<div class="col-md-5">
															<div class="days-until-ready"><span>${shipViaDescription}</span>
															</div>
														</div>
														<div class="col-md-5 change-store-container">
															<div class="change-store">
																<p><a href="http://cloud.toysrus.resource.com/redesign/template-my-account-main.html#">change your store</a>
																</p>
															</div>
														</div>
													</div>
												</div>
											</div>
										{{if bppExists}}
											<div class="protection-plan">
												<div class="inline">
													<div class="buyer-protection-plan-image inline"></div>
												</div>
												<div class="inline">
													<div class="checkbox-group">
														${customerOrderDetails.protectWith}<span>${customerOrderDetails.squareTradePlan}</span> <a target="_blank" href="http://www.toysrus.com/helpdesk/popup.jsp?display=returns&subdisplay=bpp">${customerOrderDetails.learnMore}</a>
														<span class="protection-plan-static-copy">${bppTerm}</span> <span class="protection-plan-static-copy">${customerOrderDetails.dollar}${bppChargeAmount}</span>
													</div>
													<p>${customerOrderDetails.squareTradePlanText}</p>
												</div>
											</div>
											{{/if}}
										<!-- <hr class="horizontal-divider"> -->
									{{/each}}
									{{/each}}							
									{{/each}} 
								</div>
								</div>
							</div>
						</div>                           
					</div>
			</div>
		</div>  
	</div>
</div>
</div>
{{else}}
<p class="">${customerOrder.CustomerOrderDetailResponse.messages.message.description}</p>

{{/if}}
</script>
</dsp:page>