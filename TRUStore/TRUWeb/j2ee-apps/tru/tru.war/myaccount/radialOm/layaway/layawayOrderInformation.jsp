<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" /> 

<dsp:page>
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:getvalueof var="loginStatus" bean="Profile.transient" />

	<h4><fmt:message key="layaway.guestMakePayment.orderInformation"/></h4>
	<div class="col-md-8">
		<p><fmt:message key="layaway.guestMakePayment.layawayNumber"/></p>
		<p><fmt:message key="layaway.guestMakePayment.dateCreated"/></p>
		<p><fmt:message key="layaway.guestMakePayment.nameOnAccount"/></p>
		<p><fmt:message key="layaway.guestMakePayment.howToGetIt"/></p> <!-- Commented for OMS history Changes-chaitanya -->
		<p><fmt:message key="layaway.guestMakePayment.status"/></p>
	 	<c:if test="${loginStatus eq 'false'}">
	 		<p><a id="LayawayLogoutNotYouId" class="not-account-link" href="javascript:void(0);"><fmt:message key="layaway.guestMakePayment.notYourAccount"/></a></p>
	 	</c:if>
	</div>
	<%@ page isELIgnored ="true" %>
	<div class="col-md-4 order-info font-adjustment">
	
	</div>
	<script id="layaway-order-information-details" type='text/x-jquery-tmpl'>
	
 	<p id="customerOrderNumber">${OrderDetail.PartnerOrderNumber}</p>
	<p>${OrderDetail.OrderCreationDate}</p>
		<p id="customerLayawayName">${OrderDetail.FirstName} ${OrderDetail.LastName}</p>
		<p>${OrderDetail.HowToGetIt}</p>
	<p class="layawayOrderLineStatus">${OrderDetail.OrderStatus}</p>
	</script>
</dsp:page>