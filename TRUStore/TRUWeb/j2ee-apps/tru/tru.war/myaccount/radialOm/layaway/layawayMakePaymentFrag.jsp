<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" /> 
<dsp:page>
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>

<%@ page isELIgnored ="true" %>
<h4><fmt:message key="layaway.guestMakePayment.paymentHistory"/></h4>
	<div class="col-md-7 layawayPaymentInformationDetailsLabel">
		
	</div>
	<script id="layaway-payment-information-details-label" type="text/html">
		<p>${customerOrderDetails.price}</p>
						{{each OrderDetail.PaymentHistory}}
						<p>${Date}</p>
						{{/each}}
						<br>
						{{if typeof OrderDetail.DownPayment != 'undefined'}}
						<p>${customerOrderDetails.outstandingBalance}</p>
						{{if OrderDetail.nextPaymentDue != 'undefined' && OrderDetail.nextPaymentDue != null}}
							<p>${customerOrderDetails.nextPaymentDue}</p>
							<p>${customerOrderDetails.amountDue}</p>
						{{/if}}
						{{/if}}
						<p class="enterPaymentDetails">enter payment amount</p>
		</script>
	
	<div id="cardianlOrderDetails">
		<dsp:getvalueof var="orderId" bean="ShoppingCart.layawayOrder.layawayOrderid" />
		<dsp:getvalueof var="orderAmount"bean="ShoppingCart.layawayOrder.paymentAmount" />
		<dsp:getvalueof var="currencyCode" bean="TRUConfiguration.currencyCode" />
		<dsp:getvalueof var="profileId" bean="Profile.id" />
	
		<input type="hidden" name="cardianlOrderId" id="cardianlOrderId"value="${orderId}" />
		<input type="hidden" name="cardianlOrderAmount" id="cardianlOrderAmount" value="${orderAmount}" />
		<input type="hidden" name="currencyCode" id="currencyCode" value="${currencyCode}" />
		<input type="hidden" name="profileId" id="profileId" value="${profileId}" />
	</div>
	
	<div class="col-md-5 payment-column col-no-padding">
		<div class="layawayPaymentInformationDetailsValue">
		
		</div>
		<script id="layaway-payment-information-details-values" type="text/html">
			<p>${customerOrderDetails.dollar}${OrderDetail.OrderTotal}</p>
						{{each OrderDetail.PaymentHistory}}
						<p>${customerOrderDetails.dollar}${AmountPaid}</p>
						{{/each}}
						<br>
			{{if typeof OrderDetail.DownPayment != 'undefined'}}
						<p id="outStandingAmt">${customerOrderDetails.dollar}${OrderDetail.OutstandingBalance}</p>
						{{if OrderDetail.nextPaymentDue != 'undefined' && OrderDetail.nextPaymentDue != null}}
						<p>${OrderDetail.nextPaymentDue}</p>
						<p id="amountDue">${customerOrderDetails.dollar}${OrderDetail.nextPaymentAmount}</p>
						{{/if}}		
						{{/if}}
		<form id="layawayPaymentAmountForm" class="JSValidation" method="POST" onsubmit="javascript: return false;" class="display-none">
			<div class="layawayPaymentAmountWrapper">
				<span class="dollar-sign inline">$</span>
				<input class="input-dollars inline" name="layawayPaymentAmount" id="layawayPaymentAmount" type="text" maxlength="16" placeholder="1.00"  onkeypress="return isNumber(event)" onchange="isZero(this)">
			</div>
			<a href="javascript:void(0);" id="make-payment-submit-button" class="edit-button">submit</a>
			<a href="javascript:void(0);" id="make-payment-edit-button" class="edit-button display-none">edit</a>
		</form>
			</script>
		
	</div>
</dsp:page>