	<dsp:page>
	<%@ page isELIgnored ="true" %>
		<div class="layaway-order-product">
		<div class="layawayOrderProduct">
			
				<br>
			</div>
			<script id="layaway-order-product-details" type="text/html">
						<h4>${customerOrderDetails.productInOrder}</h4>
						<hr>
						{{each OrderDetail.LineItem}}
						<div class="layaway-products">
							<div class="inline">
								<span class="product-description">${Description}</span>
								<div>
									<p class="inline">
										Qty: <span>${Quantity}<span>
									</p>
								</div>
							</div>
							<div class="pull-right">
								<p>
									status: ${LineStatus}
								</p>
							</div>
						</div>
						<br>
						<hr>
						{{/each}}
				</script>
			<dsp:include page="/myaccount/layaway/layawayButtons.jsp"/> 
		</div>
</dsp:page>