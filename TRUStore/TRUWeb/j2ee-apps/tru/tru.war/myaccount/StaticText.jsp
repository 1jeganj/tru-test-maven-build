<dsp:page>
<dsp:importbean bean="/com/tru/jda/integration/radialom/TRUCustomerOrderErrorMessages"/>
<dsp:importbean bean="/com/tru/jda/integration/configuration/TRURadialOMConfiguration"/>
<dsp:getvalueof var="imageFitRatio" bean="TRURadialOMConfiguration.imageFitRatio"/>
<dsp:droplet name="TRUCustomerOrderErrorMessages">
		<dsp:param name="layawayNoOrderMessage" value="myaccount.layaway.layawayNoOrderMessage"/>
		<dsp:param name="noOrderMessage" value="myaccount.orderDetails.noOrderMessage"/>
		<dsp:param name="noOrderFoundMessage" value="myaccount.orderDetails.noOrderFoundMessage"/>
		<dsp:param name="layawayNoOrderFoundMessage" value="myaccount.orderDetails.layawayNoOrderFoundMessage"/>
		<dsp:param name="zipCodeNotMatchMessage" value="myaccount.orderDetails.guestUser.zipCodeNotMatchMessage"/>
		<dsp:param name="giftCardpayInStoreErrorMessgae" value="myaccount.orderDetails.guestUser.giftCardpayInStoreErrorMessgae"/>
		<dsp:oparam name="output">
        	<dsp:getvalueof var="layawayNoOrderMessage" param="layawayNoOrderMessage"/>
        	<dsp:getvalueof var="noOrderMessage" param="noOrderMessage"/>
        	<dsp:getvalueof var="noOrderFoundMessage" param="noOrderFoundMessage"/>
        	<dsp:getvalueof var="layawayNoOrderFoundMessage" param="layawayNoOrderFoundMessage"/>
        	<dsp:getvalueof var="zipCodeNotMatchMessage" param="zipCodeNotMatchMessage"/>
        	<dsp:getvalueof var="giftCardpayInStoreErrorMessgae" param="giftCardpayInStoreErrorMessgae"/>
        </dsp:oparam>
	</dsp:droplet>

<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>

<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<fmt:message var="orderedOn" key="myaccount.orderDetails.orderedOn" />
<fmt:message var="orderHistory" key="myaccount.orderDetails.orderHistory" />
<fmt:message var="orderPlaced" key="myaccount.orderDetails.orderPlaced" />
<fmt:message var="hash" key="myaccount.orderDetails.hash" />
<fmt:message var="orderStatus" key="myaccount.orderDetails.orderStatus" />
<fmt:message var="orderDetail" key="myaccount.orderDetails.orderDetail" />
<fmt:message var="orderedon" key="myaccount.orderDetails.orderedon" />
<fmt:message var="billingAddress" key="myaccount.orderDetails.billingAddress" />
<fmt:message var="paymentMethod" key="myaccount.orderDetails.paymentMethod" />
<fmt:message var="orderSummary" key="myaccount.orderDetails.orderSummary" />
<fmt:message var="subTotal" key="myaccount.orderDetails.subTotal" />
<fmt:message var="items" key="myaccount.orderDetails.items" />
<fmt:message var="dollar" key="myaccount.orderDetails.dollar" />
<fmt:message var="giftWrap" key="myaccount.orderDetails.giftWrap" />
<fmt:message var="promotionalSavings" key="myaccount.orderDetails.promotionalSavings" />
<fmt:message var="shipping" key="myaccount.orderDetails.shipping" />
<fmt:message var="shippingSurcharge" key="myaccount.orderDetails.shippingSurcharge" />
<fmt:message var="salesTax" key="myaccount.orderDetails.salesTax" />
<fmt:message var="giftCard" key="myaccount.orderDetails.giftCard" />
<fmt:message var="ewaste" key="myaccount.orderDetails.ewaste" />
<fmt:message var="balance" key="myaccount.orderDetails.balance" />
<fmt:message var="size" key="myaccount.orderDetails.size" />
<fmt:message var="color" key="myaccount.orderDetails.color" />
<fmt:message var="quantity" key="myaccount.orderDetails.quantity" />
<fmt:message var="readyIn" key="myaccount.orderDetails.readyIn" />
<fmt:message var="days" key="myaccount.orderDetails.days" />
<fmt:message var="seeTerms" key="myaccount.orderDetails.seeTerms" />
<fmt:message var="cancelOrder" key="myaccount.orderDetails.cancelOrder" />
<fmt:message var="trackingNumbers" key="myaccount.orderDetails.trackingNumbers" />
<fmt:message var="seeDetails" key="myaccount.orderDetails.seeDetails" />
<fmt:message var="helpReturnWarranties" key="myaccount.orderDetails.helpReturnWarranties" />
<fmt:message var="itemsShipping" key="myaccount.orderDetails.itemsShipping" />
<fmt:message var="trackingNumber" key="myaccount.orderDetails.trackingNumber" />
<fmt:message var="giftMessage" key="myaccount.orderDetails.giftMessage" />
<fmt:message var="giftMessageText" key="myaccount.orderDetails.giftMessageText" />
<fmt:message var="balanceText" key="myaccount.orderDetails.balanceText" />
<fmt:message var="shipment" key="myaccount.orderDetails.shipment" />
<fmt:message var="protectWith" key="myaccount.orderDetails.protectWith" />
<fmt:message var="squareTradePlan" key="myaccount.orderDetails.squareTradePlan" />
<fmt:message var="squareTradePlanDuration" key="myaccount.orderDetails.squareTradePlanDuration" />
<fmt:message var="squareTradePlanText" key="myaccount.orderDetails.squareTradePlanText" />
<fmt:message var="giftWrapText" key="myaccount.orderDetails.giftWrapText" />
<fmt:message var="hyphen" key="myaccount.orderDetails.hyphen" />
<fmt:message var="comma" key="myaccount.orderDetails.comma" />
<fmt:message var="learnMore" key="myaccount.orderDetails.learnMore" />
<fmt:message var="makePayment" key="myaccount.layaway.makePayment" />
<fmt:message var="layawayAccount" key="myaccount.layaway.layawayAccount" />
<fmt:message var="viewStatus" key="myaccount.layaway.viewStatus" />
<fmt:message var="orderInformation" key="myaccount.layaway.orderInformation" />
<fmt:message var="layawayNumber" key="myaccount.layaway.layawayNumber" />
<fmt:message var="dateCreated" key="myaccount.layaway.dateCreated" />
<fmt:message var="nameOnAccount" key="myaccount.layaway.nameOnAccount" />
<fmt:message var="howToGetIt" key="myaccount.layaway.howToGetIt" />
<fmt:message var="status" key="myaccount.layaway.status" />
<fmt:message var="paymentHistory" key="myaccount.layaway.paymentHistory" />
<fmt:message var="price" key="myaccount.layaway.price" />
<fmt:message var="outstandingBalance" key="myaccount.layaway.outstandingBalance" />
<fmt:message var="initialPayment" key="myaccount.layaway.initialPayment" />
<fmt:message var="nextPaymentDue" key="myaccount.layaway.nextPaymentDue" />
<fmt:message var="amountDue" key="myaccount.layaway.amountDue" />
<fmt:message var="productInOrder" key="myaccount.layaway.productInOrder" />
<fmt:message var="backToOrder" key="myaccount.layaway.backToOrder" />
<fmt:message var="continueShopping" key="myaccount.layaway.continueShopping" />
<fmt:message var="order" key="myaccount.orderDetails.order" />
<fmt:message var="itemsPickupInstore" key="myaccount.orderDetails.itemsPickupInstore" />
<fmt:message var="free" key="myaccount.orderDetails.free" />
<fmt:message var="at" key="myaccount.orderDetails.at" />
<fmt:message var="pickedUpBy" key="myaccount.orderDetails.pickedUpBy" />
<fmt:message var="andOr" key="myaccount.orderDetails.andOr" />
<fmt:message var="islanTaxAmount" key="myaccount.orderDetails.islanTaxAmount" />
<fmt:message var="localTaxAmount" key="myaccount.orderDetails.localTaxAmount" />
<fmt:message var="surcharge" key="myaccount.orderDetails.surcharge" />
<fmt:message var="giftCardAmount" key="myaccount.orderDetails.giftCardAmount" />
<fmt:message var="serviceDown" key="myaccount.orderDetails.serviceDown" />
<fmt:message var="invalidEmailIDFound" key="myaccount.orderDetails.invalidEmailIDFound" />


<json:object>
<json:object name="customerOrderDetails">
<json:property name="order" value="${order}"/>
<json:property name="layawayNoOrderMessage" value="${layawayNoOrderMessage}"/>
<json:property name="zipCodeNotMatchMessage" value="${zipCodeNotMatchMessage}"/>
<json:property name="giftCardpayInStoreErrorMessgae" value="${giftCardpayInStoreErrorMessgae}"/>
<json:property name="noOrderFoundMessage" value="${noOrderFoundMessage}"/>
<json:property name="layawayNoOrderFoundMessage" value="${layawayNoOrderFoundMessage}"/>
<json:property name="noOrderMessage" value="${noOrderMessage}"/>
<json:property name="serviceDown" value="${serviceDown}"/>
<json:property name="backToOrder" value="${backToOrder}"/>
<json:property name="invalidEmailIDFound" value="${invalidEmailIDFound}"/>
<json:property name="continueShopping" value="${continueShopping}"/>
<json:property name="productInOrder" value="${productInOrder}"/>
		<json:property name="paymentHistory" value="${paymentHistory}"/>
		<json:property name="price" value="${price}"/>
		<json:property name="outstandingBalance" value="${outstandingBalance}"/>
		<json:property name="initialPayment" value="${initialPayment}"/>
		<json:property name="nextPaymentDue" value="${nextPaymentDue}"/>
		<json:property name="amountDue" value="${amountDue}"/>
	  <json:property name="makePayment" value="${makePayment}"/>
	  <json:property name="layawayAccount" value="${layawayAccount}"/>
	  <json:property name="viewStatus" value="${viewStatus}"/>
	  <json:property name="orderInformation" value="${orderInformation}"/>
	  <json:property name="layawayNumber" value="${layawayNumber}"/>
	  <json:property name="dateCreated" value="${dateCreated}"/>
	  <json:property name="nameOnAccount" value="${nameOnAccount}"/>
	  <json:property name="howToGetIt" value="${howToGetIt}"/>
	  <json:property name="status" value="${status}"/>
	  <json:property name="orderedOn" value="${orderedOn}"/>
	  <json:property name="orderHistory" value="${orderHistory}"/>
	  <json:property name="orderPlaced" value="${orderPlaced}"/>
	  <json:property name="hash" value="${hash}"/>
	  <json:property name="orderStatus" value="${orderStatus}"/>
	  <json:property name="orderDetail" value="${orderDetail}"/>
	  <json:property name="orderedon" value="${orderedon}"/>
	  <json:property name="billingAddress" value="${billingAddress}"/>
	  <json:property name="paymentMethod" value="${paymentMethod}"/>
	  <json:property name="orderSummary" value="${orderSummary}"/>
	  <json:property name="subTotal" value="${subTotal}"/>
	  <json:property name="items" value="${items}"/>
	  <json:property name="dollar" value="${dollar}"/>
  	  <json:property name="giftWrap" value="${giftWrap}"/>
	  <json:property name="promotionalSavings" value="${promotionalSavings}"/>
	  <json:property name="shipping" value="${shipping}"/>
	  <json:property name="shippingSurcharge" value="${shippingSurcharge}"/>
	  <json:property name="salesTax" value="${salesTax}"/>
	  <json:property name="giftCard" value="${giftCard}"/>
	  <json:property name="ewaste" value="${ewaste}"/>
	  <json:property name="balance" value="${balance}"/>
	  <json:property name="size" value="${size}"/>
	  <json:property name="color" value="${color}"/>
	  <json:property name="quantity" value="${quantity}"/>
	  <json:property name="readyIn" value="${readyIn}"/>
	  <json:property name="days" value="${days}"/>
	  <json:property name="seeTerms" value="${seeTerms}"/>
	  <json:property name="cancelOrder" value="${cancelOrder}"/>
	  <json:property name="trackingNumbers" value="${trackingNumbers}"/>
	  <json:property name="seeDetails" value="${seeDetails}"/>
	  <json:property name="helpReturnWarranties" value="${helpReturnWarranties}"/>
	  <json:property name="itemsShipping" value="${itemsShipping}"/>
	  <json:property name="trackingNumber" value="${trackingNumber}"/>
	  <json:property name="giftMessage" value="${giftMessage}"/>
	  <json:property name="giftMessageText" value="${giftMessageText}"/>
	  <json:property name="balanceText" value="${balanceText}"/>
	  <json:property name="shipment" value="${shipment}"/>
	  <json:property name="protectWith" value="${protectWith}"/>
	  <json:property name="squareTradePlan" value="${squareTradePlan}"/>
	  <json:property name="learnMore" value="${learnMore}"/>
	  <json:property name="squareTradePlanDuration" value="${squareTradePlanDuration}"/>
	  <json:property name="squareTradePlanText" value="${squareTradePlanText}"/>
	  <json:property name="giftWrapText" value="${giftWrapText}"/>
	  <json:property name="hyphen" value="${hyphen}"/>
	  <json:property name="comma" value="${comma}"/>
	  <json:property name="itemsPickupInstore" value="${itemsPickupInstore}"/>
	  <json:property name="at" value="${at}"/>
	  <json:property name="pickedUpBy" value="${pickedUpBy}"/>
	  <json:property name="andOr" value="${andOr}"/>
	  <json:property name="imageFitRatio" value="${imageFitRatio}"/>
	  <json:property name="giftWrapAmount" value="${giftWrapAmount}"/>
	  <json:property name="shippingAmount" value="${shippingAmount}"/>
	  <json:property name="shippingSurchargeAmount" value="${shippingSurchargeAmount}"/>
	  <json:property name="salesTaxAmount" value="${salesTaxAmount}"/>
	  <json:property name="localTaxAmount" value="${localTaxAmount}"/>
	  <json:property name="islanTaxAmount" value="${islanTaxAmount}"/>
	  <json:property name="ewaste" value="${ewaste}"/>
	  <json:property name="mattressFeeAmount" value="${mattressFeeAmount}"/>
	  <json:property name="pifFeeAmount" value="${pifFeeAmount}"/>
	  <json:property name="giftCardAmount" value="${giftCardAmount}"/>
	  <json:property name="promotionSavingsAmount" value="${promotionSavingsAmount}"/>
	  <json:property name="free" value="${free}"/>
	  <json:property name="surcharge" value="${surcharge}"/>
	  
</json:object>
</json:object>
</dsp:page>