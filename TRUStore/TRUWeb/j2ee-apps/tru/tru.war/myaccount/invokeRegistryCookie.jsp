<%-- This page is used to invoke the Registry Cookie --%>
<dsp:page>
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:getvalueof bean="TRUConfiguration.restInvokeCookieURL" var="restInvokeCookieURL"/>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
       <script >
			$(function(){
				var curentUrl = location.href;
				var registry_id = '';
				
				if(curentUrl.indexOf('registry_id') > -1){
					registry_id = (curentUrl.split('registry_id=')[1]).split('&')[0];
				}
				
				$.getJSON('${restInvokeCookieURL}/myaccount/registryCookie.jsp?callback=?','registry_id='+registry_id, function(res){
					
				});
			
			});
	</script>
 </dsp:page>