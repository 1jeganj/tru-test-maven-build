<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<tru:pageContainer>
		<!-- Start Home Main template -->
		<jsp:body>
			<dsp:setvalue param="pageName" value="resetPassword"/>
			<div class="container-fluid my-account-sign-in-template ">
				<!--Start My Account content -->
				<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
				<dsp:importbean bean="/com/tru/commerce/profile/droplet/TRUResetPasswordLinkExpiryCheckDroplet"/>
				<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
				<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
                <dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>
				<dsp:getvalueof var="contextPath" value="${originatingRequest.contextPath}"/>
				<dsp:getvalueof var="email" param="pe"/>
				<dsp:getvalueof var="timestamp" param="pt"/>
				
				<dsp:droplet name="TRUResetPasswordLinkExpiryCheckDroplet">
					<dsp:param name="encodedEmail" value="${email}"/>
					<dsp:param name="encodedTimeStamp" value="${timestamp}"/>
					<dsp:oparam name="output">
						<dsp:getvalueof var="generatedPassword" param="generatedPassword"/>
						<dsp:getvalueof var="linkExpired" param="linkExpired"/>
					</dsp:oparam>
				</dsp:droplet>
				
				<input type="hidden" name="generatedPassword" id="generatedPasswordId" value="${generatedPassword}"/>
				<input type="hidden" name="linkExpired" id="linkExpiredId" value="${linkExpired}"/>
				
				<div class="template-content">
			        <div class="reset-password-container">
			            <div class="sign-in-columns-container">
			                <div class="row">
								<div class="col-md-12">
									<a href="javascript:void(0)" title="banner">
										<div class="promotions-banner-at-reset-password-top"></div>
									</a>
								</div>
								<div class="clearfix"></div>
			                    <div class="col-md-12">
			                        <div class="col-md-12">
										<h2 class=""><fmt:message key="myaccount.reset.your.password" /></h2>
										<span class="errorDisplay resetPassword">
											<c:choose>
												<c:when test="${linkExpired eq 'true'}">
													<fmt:message key="myaccount.reset.password.link.expired" />
												</c:when>
												<c:when test="${generatedPassword eq 'false'}">
													<fmt:message key="myaccount.reset.password.already.reset" />
												</c:when>
												<c:when test="${linkExpired eq 'true' and generatedPassword eq 'false'}">
													<fmt:message key="myaccount.reset.password.already.reset" />
												</c:when>
											</c:choose>
										</span>
										<p><fmt:message key="myaccount.received.reset.request"/></p>
			                        </div>
			                    </div>
			                    <div class="col-md-5">
			                        <div class="reset-password-coloum create-account">
			                       
									<form id="reset-password-page"  class="JSValidation">  
			                            <div>
			                                <label for="passwordInput"><fmt:message key="myaccount.new.password" /></label>
			                                <div class="password-tools">
			                                	<input type="password" id="passwordInput" name="password" class="passwordInput" maxlength="50"/>
			                                </div>
			                                <label class="confirm-password" for="ConfirmPassword" style="display:block"><fmt:message key="myaccount.confirm.new.password" /></label>
			                                	<input type="password" id="ConfirmPassword" name="confirmPassword" class="confirm-password" maxlength="50"/>
			                                	<input type="hidden"  id="resetPasswordEmail" name="resetPwdEmail" value="${email}"/>
			                            </div>
			                            <div class="button-container">
			                                	<!-- <input type="submit" value="continue" class="profileForm" id="passResetForm-page"/> -->
			                                	<button type="submit" class="profileForm" id="passResetForm-page" style="width: 95px;height: 33px;">continue</button>
			                                	<input type="hidden" class="contextPath" value="${contextPath}"/>
			                            </div>
									  </form>
			                        </div>
			                    </div>
			                    <div class="col-md-6">
			                        <div class="order-status">
			                            <div class="">
			                              <p><fmt:message key="myaccount.password.rule.one" />&nbsp;<fmt:message key="myaccount.password.rule.one.sub" /></p>
			                                   <fmt:message key="myaccount.password.rule.two"/>
			                            </div>
			                        </div>
			                    </div>
								<div class="clearfix"></div>
								<div class="col-md-12">
									<a href="javascript:void(0)" title="banner">
									<div class="promotions-banner-at-reset-password-bottom"></div>
									</a>
								</div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		<!-- End My Account content -->
		<script type="text/javascript">
		    (function(a,b,c,d){
		    a='${tealiumURL}';
		    b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
		    a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
		    })();
		</script>
		</jsp:body>
	</tru:pageContainer>
</dsp:page>
