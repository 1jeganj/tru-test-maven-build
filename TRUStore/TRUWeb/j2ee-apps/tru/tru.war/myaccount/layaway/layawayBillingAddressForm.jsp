<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" /> 
<dsp:page>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:getvalueof var="loginStatus" bean="Profile.transient" />

<div class="checkout-shipping-address-form-header">
		<div class="inline address-form-header">
			<fmt:message key="layaway.guestMakePayment.billingAddress"/>
			<p class="required-label"><span class="orange-astericks">* </span><fmt:message key="layaway.guestMakePayment.required"/></p>
		</div>
	</div>
	<div class="layaway-payment-error"></div>
	<div class="checkout-shipping-address-form-content">
		<div>
			<span class="orange-astericks">* </span>
			<label for="country"><fmt:message key="layaway.guestMakePayment.country"/></label>
			<div class="select-wrapper billing layawayCountryField">
				<select name="country" id="country" onchange="onCountryChangeInPayment();">
				<option value="">please select</option>
				<dsp:droplet name="ForEach">
					<dsp:param name="array" bean="/atg/commerce/util/CountryList.places" />
					<dsp:param name="elementName" value="billingCountry" />
					<dsp:oparam name="output">
						<dsp:getvalueof var="billingCountry" param="billingCountry.code"></dsp:getvalueof>
						<option value='<dsp:valueof param="billingCountry.code" />'>
							<dsp:valueof param="billingCountry.displayName" />
						</option>
					</dsp:oparam>
				</dsp:droplet>
			</select>
			</div>
		</div>
		<div>
			<span class="orange-astericks">* </span>
			<label for="firstName"><fmt:message key="layaway.guestMakePayment.firstName"/></label>
			<input id="firstName" maxlength="30"  name="firstName" type="text">
		</div>
		<div>
			<span class="orange-astericks">* </span>
			<label for="lastName"><fmt:message key="layaway.guestMakePayment.lastName"/></label>
			<input id="lastName" name="lastName" maxlength="30" type="text">
		</div>
		<div>
			<span class="orange-astericks">* </span>
			<label for="address1"><fmt:message key="layaway.guestMakePayment.address"/></label>
			<input id="address1" name="address1" type="text" maxlength="50">
		</div>
		<div>
			<label for="aptsuite"><fmt:message key="layaway.guestMakePayment.suite"/></label>
			<input id="aptsuite" name="address2" type="text" maxlength="50">
		</div>
		<div>
			<span class="orange-astericks">* </span>
			<label for="city"><fmt:message key="layaway.guestMakePayment.city"/></label>
			<input id="city" name="city" type="text">
		</div>
		<div>

			<span class="orange-astericks"><fmt:message key="myaccount.asterisk" /></span>
			<label  for="state" class="avenir-heavy creditCardStateLabel"><fmt:message key="layaway.guestMakePayment.state"/></label>
			<div class="select-wrapper creditCardOtherStateWrapper">
			
			<select name="state" id="state" onchange="onCreditCardSelectedStateChange(this)">
					<option value="">please select</option>
					<dsp:droplet name="/atg/dynamo/droplet/ForEach">
						<dsp:param name="array" bean="/atg/commerce/util/StateList_US.places" />
						<dsp:param name="elementName" value="state" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="stateCode" param="state.code"></dsp:getvalueof>
									<option value="${stateCode}"><dsp:valueof param="state.code" /></option>
						</dsp:oparam>
					</dsp:droplet>
			</select>
			</div>
		</div>
		
		<div class="creditCardOtherStateLabel hide">
			<label for="otherState"><fmt:message key="checkout.shipping.otherstate"/></label><br>
			<input id="otherState" name="otherState" type="text" maxlength="20" value="NA">
		</div>
		
		<div>
			<span class="orange-astericks">* </span>
			<label for="postalCode"><fmt:message key="layaway.guestMakePayment.postalCode"/></label>
			<input id="postalCode" name="postalCode" type="text" maxlength="10">
		</div>
		<div>
			<span class="orange-astericks">* </span>
			<label for="phoneNumber"><fmt:message key="layaway.guestMakePayment.telephone"/></label>
			<input id="phoneNumber" name="phoneNumber" type="text" maxlength="20">
		</div>
		<div>
			
			<c:choose>
				<c:when test="${loginStatus eq 'true'}">
					<span class="orange-astericks">* </span>
					<label for="orderEmail"><fmt:message key="layaway.guestMakePayment.emailAddress"/></label>
					<input id="orderEmail" name="orderEmail" type="text">
				</c:when>
				<c:otherwise>
					<dsp:getvalueof bean="Profile.email" var="orderEmail" />
					<input id="orderEmail" name="orderEmail" type="hidden" value="${orderEmail}">
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</dsp:page>