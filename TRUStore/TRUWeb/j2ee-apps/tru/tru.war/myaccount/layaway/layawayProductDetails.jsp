	<dsp:page>
	<%@ page isELIgnored ="true" %>
		<div class="layaway-order-product">
		<div class="layawayOrderProduct">
			
				<br>
			</div>
			<script id="layaway-order-product-details" type="text/html">
						<h4>${customerOrderDetails.productInOrder}</h4>
						<hr>
						{{each customerOrder.orderLines.orderLine}}
						<div class="layaway-products">
							<div class="inline">
								<a href=${productPageUrl}>${itemDescription}</a>
								<div>
									<img class="inline imagesize" src=${itemImageFilename2}${customerOrderDetails.imageFitRatio}>
									<p class="inline">
										Qty: <span>${orderedQuantity}<span>
									</p>
								</div>
							</div>
							<div class="pull-right">
								<p>
									status: ${customerOrder.layawayStatus}
								</p>
							</div>
						</div>
						<br>
						<hr>
						{{/each}}
				</script>
			<dsp:include page="layawayButtons.jsp"/> 
		</div>
</dsp:page>