<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
<!-- Start : Cardianl Turn off / on changes -->
<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
<dsp:importbean bean="/com/tru/common/TRUIntegrationConfiguration" />
<dsp:importbean bean="/com/tru/common/TRUSOSIntegrationConfiguration" />
<dsp:importbean  bean="/com/tru/radial/common/TRURadialConfiguration"/>
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
<dsp:getvalueof var="enableVerboseDebug" bean="TRURadialConfiguration.enableVerboseDebug"/>
<dsp:getvalueof var="enableIntegrationLog" bean="TRURadialConfiguration.enableIntegrationLog"/>
<dsp:getvalueof var="storeCardinal" bean="TRUIntegrationConfiguration.enableCardinal"/>
<dsp:getvalueof var="sosCardinal" bean="TRUSOSIntegrationConfiguration.enableCardinal"/>
<!-- End : Cardianl Turn off / on changes -->
<dsp:getvalueof var="loginStatus" bean="Profile.transient" />
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>
<dsp:page>
<tru:pageContainer>
<input type="hidden" id="loginStatus" value="${loginStatus}" />
<input type="hidden" name="contextPath" class="contextPath" value="${originatingRequest.contextPath}"/>
<input type="hidden" name="enableIntegrationLog" id="enableIntegrationLog" value="${enableIntegrationLog}"/>
<!-- Start : Cardianl Turn off / on changes -->
<dsp:droplet name="GetSiteTypeDroplet">
	<dsp:param name="siteId" value="${site}"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="site" param="site"/>
		<c:set var="site" value ="${site}" scope="request"/>
	</dsp:oparam>
</dsp:droplet>

<c:choose>
	<c:when test="${site eq 'sos'}">
		<input type="hidden" name="cardinalEnabled" id="cardinalEnabled" value="${sosCardinal}"/>
	</c:when>
	<c:otherwise>
		<input type="hidden" name="cardinalEnabled" id="cardinalEnabled" value="${storeCardinal}"/>
	</c:otherwise>
</c:choose>	
<!-- End : Cardianl Turn off / on changes -->

	
				  <jsp:body>
				  <dsp:include page="/myaccount/tealiumForMyAccountLayaway.jsp"/> 
				  <dsp:setvalue param="pageName" value="myAccountLayaway"/>
				  <div class="container-fluid layaway-template default-template">
					<div class="template-content">
						<ul class="layaway-breadcrumb">
							<li>
								<a href="/">
									home
								</a>
							</li>
							<li><span>layaway program</span></li>
						</ul>
						<div class="row">
							<div class="col-md-9">
							<p id="layawayGlobalErrorDisplay" class="global-error-display"></p>
								<div class="layaway-header">
									<h1>
										<fmt:message key="myaccount.layaway.header" />
									</h1>
									<p>
										<fmt:message key="myaccount.layaway.headerMessage" />
										<fmt:message key="myaccount.layaway.headerMessage1" />
										<fmt:message key="myaccount.layaway.headerMessage2" />
									</p>
								</div>
								<hr>
								<ul id="layaway-tabs" class="layaway-tabs nav nav-tabs">
									<li><a class="product-review-tab-text" href="#layaway-how-does" data-toggle="tab" aria-expanded="true">
										<fmt:message key="myaccount.layaway.howDoesItWork" /></a>
									</li>
									<li class="">
										<a class="product-review-tab-text" href="#layaway-faq" data-toggle="tab" aria-expanded="false">
										<fmt:message key="myaccount.layaway.FAQ" /></a>
									</li>
									<li class="">
										<a id="account-and-payment" class="product-review-tab-text" href="#layaway-guest-account" data-toggle="tab" aria-expanded="false">
											<fmt:message key="myaccount.layaway.account" /> &amp; 
											<fmt:message key="myaccount.layaway.payment" /></a>
									</li>
								</ul>
								<textarea cols="60" rows="20" id="field_d1" name="field_d1"  hidden="true"></textarea>
								<div class="tab-content">
									<dsp:droplet name="/atg/targeting/TargetingFirst">
										<dsp:param name="targeter"
											bean="/atg/registry/RepositoryTargeters/TRU/Layaway/LayawayHowDoesTargeter" />
										<dsp:oparam name="output">
											<dsp:valueof param="element.data" valueishtml="true" />
										</dsp:oparam>
									</dsp:droplet> 
									<dsp:droplet name="/atg/targeting/TargetingFirst">
										<dsp:param name="targeter"
											bean="/atg/registry/RepositoryTargeters/TRU/Layaway/LayawayFAQTargeter" />
										<dsp:oparam name="output">
											<dsp:valueof param="element.data" valueishtml="true" />
										</dsp:oparam>
									</dsp:droplet>
								 <dsp:include page="layawayAccountsAndPayment.jsp" /> 
								</div>
							</div>
						</div>
					</div>
				</div>
			</jsp:body>
<script type="text/javascript">
	$(window).load(function(){
		var req = new XMLHttpRequest();
		req.open('GET', document.location, false);
		req.send(null);
		Accept_Encoding = req.getResponseHeader('Accept-Encoding');
		Content_Encoding = req.getResponseHeader('Content-Encoding');
		if($('#cardinalEnabled').val() == 'true'){
		isCardinalEnabled = true;
		}
		if($('#enableVerboseDebug').val() == 'true'){
			enableVerboseDebug = true;
		}
		if($('#enableIntegrationLog').val() == 'true'){
			enableIntegrationLog = true;
		}
});
</script>
				</tru:pageContainer>
		
</dsp:page>