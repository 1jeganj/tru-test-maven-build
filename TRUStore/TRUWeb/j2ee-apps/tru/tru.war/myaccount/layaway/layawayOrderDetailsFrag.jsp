<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" /> 
<dsp:page>
	<p><span><fmt:message key="layaway.guestaccount.orderDetails.header"/></span></p>

	<p><fmt:message key="layaway.guestaccount.orderDetails.message"/></p>

	<div class="layaway-guest">
		<form class="JSValidation" id="layawayGuestOrderNumberCheckupForm">
			<label for="guest-order-number"><fmt:message key="layaway.guestMakePayment.layawayNumber"/></label>
			<input name="orderStatus" type="text" class="guest-order-number" id="guest-order-number" maxlength="16"><br>
			<div class="orderError orderProgramError"></div>
			<a href="javascript:void(0);" id="layawayGuestOrderNumberCheckup" class="enter-button guest-order-submit layaway-order-num"><fmt:message key="layaway.guestaccount.orderDetails.enter"/></a>
			<div class="tab-content"></div>
		</form>
	</div>
</dsp:page>