<dsp:page>
<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/atg/commerce/order/purchase/LayawayPaymentInfoFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
<%-- <dsp:importbean bean="/com/tru/integrations/cardinal/CardinalJwtDroplet" /> --%>
<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
<dsp:getvalueof var="truConf" bean="TRUConfiguration" />
<dsp:getvalueof var="loginStatus" bean="Profile.transient" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>


<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:getvalueof var="formID" param="formID"></dsp:getvalueof>

	<c:choose>
		<c:when test="${formID eq 'layawayOrderLookUpForm'}">
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.layawayNumber" paramvalue="layawayOrderNumber"/>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.layawayOrderDetails"  value="submit"/>
		</c:when>
		<c:when test="${formID eq 'removePaymentGroupsFromOrder'}">
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.removeAllGiftCard" value="submit"/>
		</c:when>
		<c:when test="${formID eq 'checkIfGiftCardSufficient'}">
		<div>
			<div id="remainingAmount">
				<dsp:getvalueof var="remainingAmount" bean="ShoppingCart.layawayOrder.remainingAmount"/>
				<input type="hidden" id="remainingAmountValue" value="${remainingAmount}"/>
			</div>
		</div>
		</c:when> 
		<c:when test="${formID eq 'billingInfoApplicationForm'}">
			
			<dsp:getvalueof var="firstName" param="firstName" scope="request" />
			<dsp:getvalueof var="lastName" param="lastName" scope="request" />
			<dsp:getvalueof var="address1" param="address1" scope="request" />
			<dsp:getvalueof var="address2" param="address2" scope="request" />
			<dsp:getvalueof var="city" param="city" scope="request" />
			<dsp:getvalueof var="state" param="state" scope="request" />
			<dsp:getvalueof var="otherState" param="otherState" scope="request" />
			<dsp:getvalueof var="postalCode" param="postalCode" scope="request" />
			<dsp:getvalueof var="country" param="country" scope="request" />
			<dsp:getvalueof var="phoneNumber" param="phoneNumber" scope="request" />
			<dsp:getvalueof var="layawayAmountDue" param="layawayAmountDue" scope="request" />
			<dsp:getvalueof var="orderEmail" param="orderEmail" scope="request" />
			<dsp:getvalueof var="layawayPaymentAmount" param="layawayPaymentAmount" scope="request" />
			
			<dsp:getvalueof var="deviceID" param="deviceID" scope="request" />
			
			
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.billingAddressNickName" value="layawaynickName"/>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.firstName" paramvalue="firstName"/>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.lastName" paramvalue="lastName"/>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.address1" paramvalue="address1"/>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.address2" paramvalue="address2"/> 
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.city" paramvalue="city"/>
			<c:choose>
				<c:when test="${country ne 'US'}">
					<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.state" paramvalue="otherState"/>
				</c:when>
				<c:otherwise>
					<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.state" paramvalue="state"/>
				</c:otherwise>
			</c:choose>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.postalCode" paramvalue="postalCode"/>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.phoneNumber" paramvalue="phoneNumber"/>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.country" paramvalue="country"/>
			
			<c:if test="${loginStatus eq 'true'}">
				<dsp:setvalue bean="LayawayPaymentInfoFormHandler.email" paramvalue="orderEmail"/>
			</c:if>
			
			<dsp:getvalueof var="layawayPaymentAmountTmp" param="layawayPaymentAmount" />
			<c:choose>
				<c:when test="${not empty layawayPaymentAmountTmp}" >
					<dsp:setvalue bean="LayawayPaymentInfoFormHandler.paymentAmount" paramvalue="layawayPaymentAmount"/>
				</c:when>
				<c:otherwise>
					<dsp:setvalue bean="LayawayPaymentInfoFormHandler.paymentAmount" value="0.0"/>
				</c:otherwise>	
			</c:choose>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.amountDue" paramvalue="layawayAmountDue" />
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.deviceID" paramvalue="deviceID" />
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.browserAcceptEncoding" paramvalue="browserAcceptEncoding" /> 
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.commitLayawayOrder"  value="submit"/>
		</c:when> 
		<c:when test="${formID eq 'creditCardInfoApplicationForm' || formID eq 'addLayawayAddFromSuggestedOverlay'}">
			<dsp:getvalueof var="firstName" param="firstName" scope="request" />
			<dsp:getvalueof var="lastName" param="lastName" scope="request" />
			<dsp:getvalueof var="address1" param="address1" scope="request" />
			<dsp:getvalueof var="address2" param="address2" scope="request" />
			<dsp:getvalueof var="city" param="city" scope="request" />
			<dsp:getvalueof var="state" param="state" scope="request" />
			<dsp:getvalueof var="otherState" param="otherState" scope="request" />
			<dsp:getvalueof var="postalCode" param="postalCode" scope="request" />
			<dsp:getvalueof var="country" param="country" scope="request" />
			<dsp:getvalueof var="phoneNumber" param="phoneNumber" scope="request" />
			<dsp:getvalueof var="layawayAmountDue" param="layawayAmountDue" scope="request" />
			<dsp:getvalueof var="orderEmail" param="orderEmail" scope="request" />
			<dsp:getvalueof var="layawayPaymentAmount" param="layawayPaymentAmount" scope="request" />
			
			<dsp:getvalueof var="cardinalToken" param="cardinalToken" scope="request" />
			<dsp:getvalueof var="cBinNum" param="cBinNum" scope="request" />
			<dsp:getvalueof var="cLength" param="cLength" scope="request" />
			<dsp:getvalueof var="nameOnCard" param="nameOnCard" scope="request" />
			<dsp:getvalueof var="expirationMonth" param="expirationMonth" scope="request" />
			<dsp:getvalueof var="expirationYear" param="expirationYear" scope="request" />
			<dsp:getvalueof var="creditCardCVV" param="creditCardCVV" scope="request" />
		<%-- 	<dsp:getvalueof var="respJwt" param="respJwt" scope="request" /> --%>
			<dsp:getvalueof var="deviceID" param="deviceID" scope="request" />
			
			<dsp:getvalueof var="ccNum" param="cBinNum" />
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.billingAddressNickName" value="layawaynickName"/>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.creditCardInfoMap.creditCardNumber" paramvalue="cBinNum"/>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.creditCardInfoMap.cardLength" paramvalue="cLength"/>
			 <c:choose>
				<c:when test="${ccNum eq '604586'}">
					<dsp:setvalue bean="LayawayPaymentInfoFormHandler.creditCardInfoMap.expirationMonth" value="${truConf.expMonthAdjustmentForPLCC}"/>
					<dsp:setvalue bean="LayawayPaymentInfoFormHandler.creditCardInfoMap.expirationYear" value="${truConf.expYearAdjustmentForPLCC}"/>
				</c:when>
				<c:otherwise>
					<dsp:setvalue bean="LayawayPaymentInfoFormHandler.creditCardInfoMap.expirationMonth" paramvalue="expirationMonth"/>
					<dsp:setvalue bean="LayawayPaymentInfoFormHandler.creditCardInfoMap.expirationYear" paramvalue="expirationYear"/>
				</c:otherwise>
			</c:choose> 
			
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.creditCardInfoMap.creditCardVerificationNumber"  paramvalue="creditCardCVV"/>  
			<%-- <dsp:setvalue bean="LayawayPaymentInfoFormHandler.creditCardInfoMap.cardinalJwt" paramvalue="respJwt"/> --%>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.creditCardInfoMap.nameOnCard"  paramvalue="nameOnCard"/>  
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.creditCardInfoMap.creditCardToken"  paramvalue="cardinalToken"/>  
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.firstName" paramvalue="firstName"/>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.lastName" paramvalue="lastName"/>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.address1" paramvalue="address1"/>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.address2" paramvalue="address2"/> 
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.city" paramvalue="city"/>
			<%-- <c:choose>
				<c:when test="${country ne 'US'}">
					<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.state" paramvalue="otherState"/>
				</c:when>
				<c:otherwise> --%>
					<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.state" paramvalue="state"/>
				<%-- </c:otherwise>
			</c:choose> --%>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.postalCode" paramvalue="postalCode"/>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.phoneNumber" paramvalue="phoneNumber"/>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressInputFields.country" paramvalue="country"/>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.addressValidated" paramvalue="billingAddressValidated"/>
			
			<c:if test="${loginStatus eq 'true'}">
				<dsp:setvalue bean="LayawayPaymentInfoFormHandler.email" paramvalue="orderEmail"/>
			</c:if>
			
			<dsp:getvalueof var="layawayPaymentAmountTmp" param="layawayPaymentAmount" />
			<c:choose>
				<c:when test="${not empty layawayPaymentAmountTmp}" >
					<dsp:setvalue bean="LayawayPaymentInfoFormHandler.paymentAmount" paramvalue="layawayPaymentAmount"/>
				</c:when>
				<c:otherwise>
					<dsp:setvalue bean="LayawayPaymentInfoFormHandler.paymentAmount" value="0.0"/>
				</c:otherwise>	
			</c:choose>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.amountDue" paramvalue="layawayAmountDue" />
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.deviceID" paramvalue="deviceID" />
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.browserAcceptEncoding" paramvalue="browserAcceptEncoding" />
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.commitLayawayOrder"  value="submit"/>
		</c:when>
		<c:when test="${formID eq 'giftCardInfoApplicationForm'}">
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.giftCardNumber" paramvalue="gcNumber"/>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.giftCardPin" paramvalue="ean"/>
			<dsp:getvalueof var="layawayPaymentAmountTmp" param="layawayPaymentAmount" />
			<c:choose>
				<c:when test="${not empty layawayPaymentAmountTmp}" >
					<dsp:setvalue bean="LayawayPaymentInfoFormHandler.paymentAmount" paramvalue="layawayPaymentAmount"/>
				</c:when>
				<c:otherwise>
					<dsp:setvalue bean="LayawayPaymentInfoFormHandler.paymentAmount" value="0.0"/>
				</c:otherwise>	
			</c:choose>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.applyGiftCard"  value="submit"/>
		</c:when>
		<c:when test="${formID eq 'layawayPaymentAmountForm'}">
		<dsp:getvalueof var="layawayError" value="layawayError"/>
		<dsp:setvalue bean="LayawayPaymentInfoFormHandler.layawayOMSId" paramvalue="layawayOrderId"/>
		<dsp:setvalue bean="LayawayPaymentInfoFormHandler.layawayCustomerName" paramvalue="layawayCustomerName"/>
			<dsp:getvalueof var="layawayPaymentAmountTmp" param="layawayPaymentAmount" />
			<c:choose>
				<c:when test="${not empty layawayPaymentAmountTmp}" >
					<dsp:setvalue bean="LayawayPaymentInfoFormHandler.paymentAmount" paramvalue="layawayPaymentAmount"/>
				</c:when>
				<c:otherwise>
					<dsp:setvalue bean="LayawayPaymentInfoFormHandler.paymentAmount" value="0.0"/>
				</c:otherwise>	
			</c:choose>
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.amountDue" paramvalue="layawayAmountDue" />
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.orderPaymentAmount"  value="submit"/>
		</c:when>
		<c:when test="${formID eq 'layawayGiftCardRemoveForm'}">
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.cardId" paramvalue="cardId" />
			<dsp:setvalue bean="LayawayPaymentInfoFormHandler.removeGiftCard" value="submit" />
		</c:when>
	</c:choose>
	<dsp:droplet name="/atg/dynamo/droplet/Switch">
	           <dsp:param bean="LayawayPaymentInfoFormHandler.formError" name="value" />
	           <dsp:oparam name="true">
		           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
		             <dsp:param name="exceptions" bean="LayawayPaymentInfoFormHandler.formExceptions" />
		             <dsp:oparam name="outputStart">
		             	Following are the form errors: 
		             </dsp:oparam>
		              <dsp:oparam name="output">
		                  <dsp:valueof param="message"/>
		               </dsp:oparam>
		               <dsp:oparam name="outputEnd">
							
					   </dsp:oparam>
		           </dsp:droplet>
	           </dsp:oparam>
		        <dsp:oparam name="false">
				        <c:if test="${formID eq 'layawayPaymentAmountForm'}">
				         <div>
							<div class="cardinalUpdateList">
					        	<dsp:getvalueof var="orderId" bean="ShoppingCart.layawayOrder.layawayOrderid" />
								<dsp:getvalueof var="orderAmount"bean="ShoppingCart.layawayOrder.paymentAmount" />
								<dsp:getvalueof var="currencyCode" bean="TRUConfiguration.currencyCode" />
								<dsp:getvalueof var="profileId" bean="Profile.id" />
							
								<%-- <dsp:droplet name="CardinalJwtDroplet">
									<dsp:oparam name="output">
								       	<dsp:getvalueof var="jwt" param="jwt"/>
								     </dsp:oparam>
								</dsp:droplet>  
							
								<input type="hidden" name="jwt" id="jwt" value="${jwt}" />--%>
								<input type="hidden" name="cardianlOrderId" id="cardianlOrderId" value="${orderId}" />
								<input type="hidden" name="cardianlOrderAmount" id="cardianlOrderAmount" value="${orderAmount}" />
								<input type="hidden" name="currencyCode" id="currencyCode" value="${currencyCode}" />
								<input type="hidden" name="profileId" id="profileId" value="${profileId}" />
								</div>
							</div>	
				        </c:if>
			           <c:if test="${formID eq 'layawayPaymentAmountForm' || formID eq 'giftCardInfoApplicationForm' || formID eq 'layawayGiftCardRemoveForm' || formID eq 'removePaymentGroupsFromOrder'}">
						<div>
							<div class="giftCardList">
								<dsp:include page="../layawayDisplayGiftCard.jsp" />
							</div>					
						</div>
					</c:if>
					<c:if test="${formID eq 'creditCardInfoApplicationForm' || formID eq 'billingInfoApplicationForm' }">
						<dsp:droplet name="/atg/dynamo/droplet/Switch">
						<dsp:param name="value" bean="LayawayPaymentInfoFormHandler.addressDoctorProcessStatus" />
						<dsp:oparam name="VERIFIED">
							address-saved-success-from-ajax: 
						</dsp:oparam>
						<dsp:oparam name="CORRECTED">
							<dsp:droplet name="Switch">
								<dsp:param name="value" bean="LayawayPaymentInfoFormHandler.addressDoctorResponseVO.derivedStatus" />
								  <dsp:oparam name="YES">
										address-doctor-overlay-from-ajax:
										<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
												<dsp:param name="mode" value="layaway" />
										</dsp:include>
								  </dsp:oparam>
								   <dsp:oparam name="NO">
								   		address-saved-success-from-ajax:
								   </dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
						<dsp:oparam name="VALIDATION ERROR">
							address-doctor-overlay-from-ajax:
							<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
								<dsp:param name="mode" value="layaway" />
							</dsp:include>
						</dsp:oparam>
						<dsp:oparam name="default">														
							address-saved-success-from-ajax:							
						</dsp:oparam>
					</dsp:droplet>
					</c:if> 
				</dsp:oparam>
   	</dsp:droplet>
   		 	
</dsp:page>

