<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" /> 		
<dsp:page>	
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:getvalueof var="loginStatus" bean="Profile.transient" />	
		<div class="layaway-guest-details" id="layawayGuestDetails"> 
			<h3><fmt:message key="layaway.accountpayemnt.header"/></h3>
			<p><fmt:message key="layaway.accountpayment.message"/></p>
			<br>
			<div class="row guest-details-content">
				<div class="col-md-6 align-left">
					<dsp:include page="/myaccount/radialOm/layaway/layawayOrderInformation.jsp"/>
				</div>
				<div class="col-md-6">
					<dsp:include page="/myaccount/radialOm/layaway/layawayMakePaymentFrag.jsp"/>
				</div>
			</div>
		</div> 
		<c:choose>
		<c:when test="${loginStatus eq 'false'}">
			<dsp:include page="/myaccount/radialOm/layaway/layawayProductDetails.jsp"/>
		</c:when>
		<c:otherwise>
			<dsp:include page="layawayButtonsCS.jsp"/> 
			<hr>
		</c:otherwise>
		</c:choose>
		<div id="payment-section" class="display-none">
			<dsp:include page="layawayPaymentSection.jsp"/>
		</div>
</dsp:page>