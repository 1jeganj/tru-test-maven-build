<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" /> 

<dsp:page>
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/multisite/Site"/>
<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id" />
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof var="loginStatus" bean="Profile.transient" />

<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
	<dsp:oparam name="output">
	<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
	</dsp:oparam>
</dsp:droplet>
<dsp:droplet name="/com/tru/droplet/TRUGetSiteDroplet">
	<dsp:param name="siteId" value="${siteName}"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="siteUrl" param="siteUrl"/> 
	</dsp:oparam>
</dsp:droplet>

<dsp:getvalueof var="fullyQualifiedcontextPathUrl" value="${scheme}${siteUrl}${originatingRequest.contextPath}"/>

	<h4><fmt:message key="layaway.guestMakePayment.orderInformation"/></h4>
	<div class="col-md-8">
		<p><fmt:message key="layaway.guestMakePayment.layawayNumber"/></p>
		<p><fmt:message key="layaway.guestMakePayment.dateCreated"/></p>
		<p><fmt:message key="layaway.guestMakePayment.nameOnAccount"/></p>
		<p><fmt:message key="layaway.guestMakePayment.howToGetIt"/></p>
		<p><fmt:message key="layaway.guestMakePayment.status"/></p>
		 <c:if test="${loginStatus eq 'false'}">
		 <p><a id="LayawayLogoutNotYouId" class="not-account-link" href="javascript:void(0);"><fmt:message key="layaway.guestMakePayment.notYourAccount"/></a></p>
		 </c:if>
		<dsp:form name="layawayNotYouLogOutForm">
				<dsp:input bean="ProfileFormHandler.logoutSuccessURL" type="hidden" value="${fullyQualifiedcontextPathUrl}myaccount/layaway/myAccountLayaway.jsp"/>
				<dsp:input type="submit" id="layawayLogout" bean="ProfileFormHandler.logout" value="sign out" class="display-none"/>
		</dsp:form>
	</div>
	<%@ page isELIgnored ="true" %>
	<div class="col-md-4 order-info font-adjustment">
	
	</div>
	<script id="layaway-order-information-details" type="text/html">
	
 	<p id="customerOrderNumber">${customerOrder.orderNumber}</p>
	<p>${customerOrder.orderCapturedDate}</p>
		<p id="customerLayawayName">${customerOrder.customerInfo.customerFirstName} ${customerOrder.customerInfo.customerLastName}</p>
	<p>${customerOrder.deliveryOption}</p>

	<p class="layawayOrderLineStatus">${customerOrder.layawayStatus}</p>
	</script>
</dsp:page>