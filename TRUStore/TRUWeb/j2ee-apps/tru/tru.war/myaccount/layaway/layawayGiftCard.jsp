<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" /> 
<dsp:page>

<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<div class="layaway-giftcard-error"></div>
<header><fmt:message key="layaway.guestMakePayment.giftCard"/></header>
	<div class="inline"><fmt:message key="layaway.guestMakePayment.message1"/> 
	<span><fmt:message key="layaway.guestMakePayment.message2"/></span>
	<fmt:message key="layaway.guestMakePayment.message3"/>
	</div>
	<p class="global-error-display"></p>
	<div>
		<div class="giftCardFormFieldContainer">
			<label for="gc_Number">
				<fmt:message key="layaway.guestMakePayment.cardNumber"/>
			</label>
			<div class="gcNumberWrapper">
				<input type="text" name="gcNumber" id="gc_Number" class="chkNumbersOnly" onkeypress="return isNumberOnly(event, 16);">
			</div>
		</div>
		<div class="giftCardFormFieldContainer">
			<label for="gmp_pin">
				<fmt:message key="layaway.guestMakePayment.PinNumber"/>
			</label>
			<div class="eanWrapper">
			<input type="text" type="password" name="ean" id="gmp_pin" class="chkNumbersOnly" onkeypress="return isNumberOnly(event, 4);">
			</div>
			<button id="layaway-payment-gift-card" ><fmt:message key="layaway.guestMakePayment.apply"/></button>
		</div>
		<div class="giftCardFormFieldContainer">
			<p>
				<a class="checkGiftCarddetails" href="JavaScript:void(0)" data-toggle="modal" data-target="#giftCardBalanceModal"><fmt:message key="layaway.guestMakePayment.checkGiftCardBalance"/></a>
			</p>
			
			
			
				<span class="giftcardTooltip-wrapper layaway">
					<a id="giftcardTooltip" href="javascript:void(0)" data-toggle="popover" data-original-title="" title=""><fmt:message key="layaway.guestMakePayment.giftCardHelp"/></a>
					
					<!-- <div class="giftcardTooltippopover popover fade bottom in" role="tooltip" data-toggle="popover" style="display: none;" data-original-title="" title="">
						<div class="arrow"></div>						
					</div> -->
				</span>	
				<div class="display-none" id="giftCardHelpContent" style="display:none;">
					<dsp:droplet name="/atg/targeting/TargetingFirst">
						<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/AccountManagement/GiftCardHelpTargeter" />
						<dsp:oparam name="output">
							<dsp:valueof param="element.data" valueishtml="true" />
						</dsp:oparam>
					</dsp:droplet>
					<!-- <img src="/images/gift-card-help.png"/> -->
				</div>
				 <%-- <a  data-toggle="modal" data-target="#checkoutGiftCardHelp" href="javascript:void(0);"><fmt:message key="layaway.guestMakePayment.giftCardHelp"/></a> --%> 			
			
		</div>
		<dsp:include page="layawayDisplayGiftCard.jsp" />
	</div>
</dsp:page>
