<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" /> 
<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<div class="row layawayButtonCS">
		<div class="col-md-4">
			<div class="button"><a id="backToOrderLookUp" class="backToOrderLookUp" href="javascript:void(0)"><fmt:message key="layaway.accountpayment.back"/></a></div>
		</div>
		<div class="col-md-4">
			<div class="button"><a href="${contextPath}home"><fmt:message key="layaway.accountpayment.continue"/></a></div>
		</div>
</div>
</dsp:page>