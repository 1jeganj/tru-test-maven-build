<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" /> 
<dsp:page>
<div class="layaway-payment-receipt tab-pane" id="layawayPaymentReceipt">
	<h3><fmt:message key="layaway.accountpayemnt.header"/></h3>
	<p><fmt:message key="layaway.accountpayment.message"/></p>
	<br>
	<h4><fmt:message key="layaway.paymentReciept.message"/></h4>
	<div class="row receipt-info">
		<div class="col-md-12">
			<p><fmt:message key="layaway.paymentReciept.orderNumber"/><span>194061976</span></p>
			<p><fmt:message key="layaway.paymentReciept.paymentAmount"/> <span>$75.00</span></p>
			<p><fmt:message key="layaway.paymentReciept.outstandingBalance"/><span>$123.45</span></p>
		</div>
	</div>
	<div id="titleRow" class="row">
		<div class="col-md-5ths">
			<fmt:message key="layaway.paymentReciept.layawayNumber"/>
		</div>
		<div class="col-md-5ths">
			<fmt:message key="layaway.paymentReciept.dateCreated"/>
		</div>
		<div class="col-md-5ths">
			<fmt:message key="layaway.paymentReciept.name"/>
		</div>
		<div class="col-md-5ths">
			<fmt:message key="layaway.paymentReciept.zipCode"/>
		</div>
		<div class="col-md-5ths">
			<fmt:message key="layaway.paymentReciept.balance"/>
		</div>
	</div>
	<div class="row table-content">
		<div class="col-md-5ths">
			194061976
		</div>
		<div class="col-md-5ths">
			2-14-2015
		</div>
		<div class="col-md-5ths">
			John Smith
		</div>
		<div class="col-md-5ths">
			19406
		</div>
		<div class="col-md-5ths">
			$123.45
		</div>
	</div>
	<dsp:include page="layawayButtons.jsp"/>
</div>
</dsp:page>