<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" /> 
<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<div class="layaway-buttons">
		<a href="javascript:void(0)" class="enter-button backToOrderLookUp"><fmt:message key="layaway.accountpayment.back"/></a>
		<a href="${contextPath}" class="enter-button"><fmt:message key="layaway.accountpayment.continue"/></a>
	</div>
</dsp:page>