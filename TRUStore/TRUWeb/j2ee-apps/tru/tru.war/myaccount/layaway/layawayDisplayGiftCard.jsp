<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftCardFormHandler"/>
<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<c:set var="gcCount" value="0" />
	<div class="gift-layaway-card-table">
		<table>
			<tbody>
				<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="ShoppingCart.layawayOrder.paymentGroups" />
				<dsp:param name="elementName" value="paymentGroup" />
				<dsp:oparam name="output">	
						<dsp:getvalueof var="pgType" param="paymentGroup.paymentGroupClassType" />					
						<c:choose>
							<c:when test="${pgType eq 'giftCard'}">
								<c:set var="gcCount" value="${gcCount+1}"/>
								<dsp:getvalueof var="gcNumber" param="paymentGroup.giftCardNumber" />
								<dsp:getvalueof var="gcAmount" param="paymentGroup.amount" />
								<dsp:getvalueof var="cardId" param="paymentGroup.id" />
								<tr>
									<td><dsp:valueof param="paymentGroup.giftCardNumber" converter="creditcard" maskcharacter="x" /></td>
									<td><dsp:valueof value="${gcAmount}" converter="currency"/> Applied</td>
									<td><a class="pull-layaway-right" data-id="${cardId}" href="javascript:void(0);">remove</a></td>
								</tr>
							</c:when>
						</c:choose>
					</dsp:oparam>
				</dsp:droplet>	
			</tbody>
		</table>
		<input type="hidden" id="gcAppliedCount" name="gcAppliedCount" value="${gcCount}" />
	</div>					
</dsp:page>