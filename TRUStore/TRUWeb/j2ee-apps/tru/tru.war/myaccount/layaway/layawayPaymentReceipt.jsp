<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" /> 
<dsp:page>
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<div class="layaway-payment-receipt tab-pane layaway-orders" id="layawayPaymentReceipt">
	<h3><fmt:message key="layaway.accountpayemnt.header"/></h3>
	<p><fmt:message key="layaway.accountpayment.message"/></p>
	<br>
	<h4><fmt:message key="layaway.paymentReciept.message"/></h4>
	<div class="row receipt-info">
		<div class="col-md-12">
			<p><fmt:message key="layaway.paymentReciept.orderNumber"/><span><dsp:valueof bean="ShoppingCart.lastLayawayOrder.layawayOrderid"/></span></p>
			<p><fmt:message key="layaway.paymentReciept.paymentAmount"/> <span><dsp:valueof bean="ShoppingCart.lastLayawayOrder.paymentAmount" converter="currency"/></span></p>
			<p><fmt:message key="layaway.paymentReciept.outstandingBalance"/><span><dsp:valueof bean="ShoppingCart.lastLayawayOrder.layawayDueAmount"  converter="currency"/></span></p>
		</div>
	</div>
	<div id="titleRow" class="row">
		<div class="col-md-5ths">
			<fmt:message key="layaway.paymentReciept.layawayNumber"/>
		</div>
		<div class="col-md-5ths">
			<fmt:message key="layaway.paymentReciept.dateCreated"/>
		</div>
		<div class="col-md-5ths">
			<fmt:message key="layaway.paymentReciept.name"/>
		</div>
		<div class="col-md-5ths">
			<fmt:message key="layaway.paymentReciept.zipCode"/>
		</div>
		<div class="col-md-5ths">
			<fmt:message key="layaway.paymentReciept.balance"/>
		</div>
	</div>
	<div class="row table-content">
		<div class="col-md-5ths">
			<dsp:valueof bean="ShoppingCart.lastLayawayOrder.layawayOMSOrderId"/>
		</div>
		<div class="col-md-5ths">
			<dsp:valueof bean="ShoppingCart.lastLayawayOrder.creationDate" converter="date" date="M/dd/yyyy"/>
		</div>
		<div class="col-md-5ths">
			<dsp:valueof bean="ShoppingCart.lastLayawayOrder.layawayCustName"/>
		</div>
		<div class="col-md-5ths">
			<dsp:valueof bean="ShoppingCart.lastLayawayOrder.postalCode"/>
		</div>
		<div class="col-md-5ths">
			<dsp:valueof bean="ShoppingCart.lastLayawayOrder.layawayDueAmount" converter="currency"/>
		</div>
	</div>
	<dsp:include page="layawayButtons.jsp"/>
</div>
</dsp:page>