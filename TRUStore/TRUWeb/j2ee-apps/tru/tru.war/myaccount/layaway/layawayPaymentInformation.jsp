<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" /> 
<dsp:page>
<h4><fmt:message key="layaway.guestMakePayment.paymentHistory"/></h4>
	<div class="col-md-7">
		<p><fmt:message key="layaway.guestMakePayment.price"/></p>
		<p><fmt:message key="layaway.guestMakePayment.initialPayment"/></p>
		<p>2-27-2015</p>
		<p>3-14-2015</p>
		<p>3-28-2015</p>
		<p>3-28-2015</p>
		<br>
		<p><fmt:message key="layaway.guestMakePayment.outstandingBalance"/></p>
		<p><fmt:message key="layaway.guestMakePayment.nextPaymentDue"/></p>
		<p><fmt:message key="layaway.guestMakePayment.amountDue"/></p>
	</div>
	<div class="col-md-5 payment-column col-no-padding">
		<p>$427.98</p>
		<p>$100.00</p>
		<p>$100.00</p>
		<p>$75.00</p>
		<p>$50.00</p>
		<p>$50.00</p>
		<br>
		<p>$427.98</p>
		<p>$100.00</p>
		<p>$100.00</p>
	</div>
</dsp:page>