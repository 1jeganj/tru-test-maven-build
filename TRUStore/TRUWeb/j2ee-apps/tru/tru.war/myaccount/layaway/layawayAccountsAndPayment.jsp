<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<dsp:page>
	<dsp:include page="layawayOrderLookup.jsp"/> 
	<dsp:include page="layawayMakePayment.jsp"/> 
	<dsp:include page="/myaccount/radialOm/layaway/layawayOrderHistory.jsp"/>
	<dsp:include page="layawayPaymentReceipt.jsp"/>
	<%-- <dsp:include page="layawayOrderDetails.jsp"/> --%>
	<div class="modal fade" id="layaway-page-suggested-address" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog modal-dialog-shipping-page-suggested"></div>
	</div>
</dsp:page>

