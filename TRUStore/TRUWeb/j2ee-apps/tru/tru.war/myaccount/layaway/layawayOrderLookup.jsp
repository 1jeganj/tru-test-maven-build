<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" /> 
<dsp:page>
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:getvalueof var="loginStatus" bean="Profile.transient" />
<div id="layaway-guest-account" class="tab-pane layaway-orders">
		<h3><fmt:message key="layaway.accountpayemnt.header"/></h3>
		<p><fmt:message key="layaway.accountpayment.message"/></p>
		<br>
		<div class="layaway-order-header"></div>
		<div class="row guest-account-details">
			<div class="col-md-6">
				<dsp:include page="layawayOrderDetailsFrag.jsp"/>
			</div>
		<c:if test="${loginStatus eq 'true'}">
			<div class="col-md-6">
				<dsp:include page="layawayRegisteredUsers.jsp"/>
			</div>
		</c:if>
		</div>
</div>
</dsp:page>