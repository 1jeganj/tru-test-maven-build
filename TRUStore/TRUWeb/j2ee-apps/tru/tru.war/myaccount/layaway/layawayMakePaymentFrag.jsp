<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" /> 
<dsp:page>
<%@ page isELIgnored ="true" %>
<h4><fmt:message key="layaway.guestMakePayment.paymentHistory"/></h4>
	<div class="col-md-7 layawayPaymentInformationDetailsLabel">
		
	</div>
	<script id="layaway-payment-information-details-label" type="text/html">
		<p>${customerOrderDetails.price}</p>
						{{each customerOrder.customerOrderNotes.customerOrderNote}}
						{{if isTranctionDateAndPriceAvailable == true}}
						<p>${transactionDate}</p>
						{{/if}}
						{{/each}}
						<br>
						{{if customerOrder.customerOrderNotes.isRemainingAmountAvailable == true}}
						<p>${customerOrderDetails.outstandingBalance}</p>
						<p>${customerOrderDetails.nextPaymentDue}</p>
						<p>${customerOrderDetails.amountDue}</p>
						{{/if}}
						<p class="enterPaymentDetails">enter payment amount</p>
		</script>
	<div class="col-md-5 payment-column col-no-padding">
		<div class="layawayPaymentInformationDetailsValue">
		
		</div>
		<script id="layaway-payment-information-details-values" type="text/html">
			<p>${customerOrder.orderTotals.formattedGrandTotal}</p>
						{{each customerOrder.customerOrderNotes.customerOrderNote}}
						{{if isTranctionDateAndPriceAvailable == true}}
						<p>${transactionTotalAmount}</p>
						{{/if}}
						{{/each}}
						<br>
						{{if customerOrder.customerOrderNotes.isRemainingAmountAvailable == true}}
						<p>${customerOrder.customerOrderNotes.remainingAmount}</p>
						<p>${customerOrder.customerOrderNotes.nextPaymentDue}</p>
						<p id="amountDue">${customerOrder.customerOrderNotes.nextPaymentAmount}</p>
						{{/if}}
		<form id="layawayPaymentAmountForm" class="JSValidation" method="POST" onsubmit="javascript: return false;" class="display-none">
			<div class="layawayPaymentAmountWrapper">
				<span class="dollar-sign inline">$</span>
				<input class="input-dollars inline" name="layawayPaymentAmount" id="layawayPaymentAmount" type="text" maxlength="16" placeholder="1.00"  onkeypress="return isNumber(event)" onchange="isZero(this)">
			</div>
			<a href="javascript:void(0);" id="make-payment-submit-button" class="edit-button">submit</a>
			<a href="javascript:void(0);" id="make-payment-edit-button" class="edit-button display-none">edit</a>
		</form>
			</script>
		
	</div>
</dsp:page>