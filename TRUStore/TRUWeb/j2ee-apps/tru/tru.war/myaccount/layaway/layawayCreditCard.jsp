<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:importbean bean="/com/tru/common/droplet/DisplayMonthAndYearDroplet" />
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:page>

	<div class="row">
		<div class="col-md-12">
			<h4><fmt:message key="layaway.guestMakePayment.creditCard"/></h4>
		</div>
		<div class="layaway-creditCard-error"></div>
		<div class="col-md-7 credit-card-left-section">
		<!-- start-->
		<div class="credit-component">
		<label for="nameOnCard"><fmt:message key="layaway.guestMakePayment.nameOnCard"/></label>
		<input type="text" id="nameOnCard" name="nameOnCard" class="inline">
		</div>
		<!--end -->
			<div class="credit-component credit-card-number">
				<label for="ccnumber"><fmt:message key="layaway.guestMakePayment.cardNumber"/></label>
				<div class='layAwayCreditCardNo'>
					<input type="text" value="" id="ccnumber" name="creditCardNumber" onkeypress="return isNumberOnly(event,16)" onkeyup="identifyCardType(this)" class="inline card-number chkNumbersOnly">
				</div>
				<div class="display-card check-mark">
					<img class="valid-credit-card" src="${TRUImagePath}images/Payment_Green-Checkmark.jpg" alt="Visa Credit Card">
				</div>
				<div class="display-card">
					<img class="valid-credit-card credit-card" src="${TRUImagePath}images/Payment_Small-Mastercard-Thumb-No-Outline.jpg" alt="MasterCard">
				</div>
			</div>
			<div class="credit-component">
				<div class="inline">
					<label for="expirationMonth"><fmt:message key="layaway.guestMakePayment.expire"/></label>
					<div class="expirationMonthWrapper">
						<select name="expirationMonth" id="expirationMonth">
						<option value="">MM</option>
						<dsp:droplet name="DisplayMonthAndYearDroplet">
							<dsp:param name="monthList" value="monthList" />
							<dsp:oparam name="output">
								<dsp:droplet name="ForEach">
									<dsp:param name="array" param="monthList" />
									<dsp:param name="elementName" value="month" />
									<dsp:oparam name="output">
										<dsp:getvalueof var="month" param="month" />
										<option value="${month}" ><dsp:valueof param="month" /></option>
									</dsp:oparam>
								</dsp:droplet>
							</dsp:oparam>
						</dsp:droplet>
						</select>
					</div>
				</div>
				<div class="inline">
				<div class="expirationYearWrapper">
					<select name="expirationYear" id="expirationYear" aria-label="expiration Year">
						<option value="">YYYY</option>
						<dsp:droplet name="DisplayMonthAndYearDroplet">
							<dsp:param name="yearList" value="yearList" />
							<dsp:oparam name="output">
								<dsp:droplet name="ForEach">
									<dsp:param name="array" param="yearList" />
									<dsp:param name="elementName" value="year" />
									<dsp:oparam name="output">
										<dsp:getvalueof var="year" param="year" />
										<option value="${year}" ><dsp:valueof param="year" /></option>
									</dsp:oparam>
								</dsp:droplet>
							</dsp:oparam>
						</dsp:droplet>
					</select>
				</div>
				</div>
			</div>
			<div class="credit-component">
				<label for="creditCard_CVV"><fmt:message key="layaway.guestMakePayment.securityCode"/></label>
				<div class='layAwayCreditCardCVV'>
					<input type="password" name="creditCardCVV" id="creditCardCVV" value="" maxlength="4" class="creditCardCVV splCharCheck security-code inline rusCard" onkeypress="return isNumberOnly(event,4)">
					<%-- <input type="text" name="creditCardCVV_tmp" id="creditCard_CVV" value="" maxlength="4" class="creditCardCVV_tmp splCharCheck security-code inline rusCard" onkeypress="return isNumberOnly(event,4)">
					<input type="hidden" name="creditCardCVV" id="creditCardCVV" value="${cvv}" /> --%>
				</div>
				<div class="whats-this inline"><a data-target="#creditCardModal" data-toggle="modal" href="javascript:void(0)"><fmt:message key="layaway.guestMakePayment.WhatIsThis"/></a></div>
			</div>
		</div>
		<input type="hidden" name="cardinalToken" id="cardinalToken" value=""/>
		<input type="hidden" name="cBinNum" id="cBinNum" value=""/>
		<input type="hidden" name="cLength" id="cLength" value=""/>
	</div>
	<div class="layaway-buttons">
		<a href="javascript:void(0)" class="enter-button backToOrderLookUp"><fmt:message key="layaway.guestMakePayment.backToOrder"/></a>
		<a id="layaway-credit-card-payment" href="javascript:void(0);" class="enter-button"><fmt:message key="layaway.guestMakePayment.submitPayment"/></a>
		<div class="pull-right">
			<a  data-toggle="modal" data-target="#layawayTermsAndCondition" href="javascript:void(0);"><fmt:message key="layaway.guestMakePayment.terms"/> &amp; <fmt:message key="layaway.guestMakePayment.conditions"/></a>
		</div>
	</div>
</dsp:page>