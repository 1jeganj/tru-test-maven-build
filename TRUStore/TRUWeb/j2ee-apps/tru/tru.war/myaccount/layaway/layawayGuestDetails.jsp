<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" /> 
<dsp:page>
<div class="layaway-guest-details tab-pane" id="layawayGuestDetails">
	<h3><fmt:message key="layaway.accountpayemnt.header"/></h3>
	<p><fmt:message key="layaway.accountpayment.message"/></p>
	<br>
		<div class="row guest-details-content">
			<div class="col-md-6">
				<dsp:include page="/myaccount/radialOm/layaway/layawayOrderInformation.jsp"/>
			</div>
			<div class="col-md-6">
				<dsp:include page="layawayPaymentInformation.jsp"/>
			</div>
		</div>
	<div class="row">
		<div class="col-md-12">
			<br>
		</div>
	</div>
	<dsp:include page="layawayButtonsCS.jsp"/>
</div>
</dsp:page>