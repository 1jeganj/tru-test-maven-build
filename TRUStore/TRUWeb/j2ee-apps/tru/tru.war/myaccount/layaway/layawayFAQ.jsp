<%-- <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<dsp:page>
<!-- <div class="tab-pane" id="layaway-faq">
		<h3>Frequently Asked Questions</h3>
		<p>Here are some commonly asked questions, but if you still have others that need to be answered feel free to call us at 1-800-ToysRUs (1-800-869-7787)</p>
		<br>
		<br>
		<div class="layaway-questions">
			<h4 tabindex="0">Don't see your order?</h4>
			<p>Click here: <a href="JavaScript:void(0)" id="layaway-payment-page">http://www.toysrus.com/layaway/payment.jsp</a> to be able to input your order # and check your balance due or make a payment.</p>
			<br>
			<h4>How long is a layaway contract?</h4>
			<p>Layaway contracts are 90 Days.</p>
			<br>
			<h4>Can I have multiple layaway accounts?</h4>
			<p>Yes. We don't limit the number of layaways you may have at any one time.</p>
			<br>
			<h4>Is there a minimum purchase amount to be eligible for layaway?</h4>
			<p>There is no minimum purchase amount to be eligible for layaway.</p>
			<br>
			<h4>Are there any set-up or cancellation fees?</h4>
			<p>There is a minimal $5 set-up fee and we do charge a cancellation fee. Upfront fees are waived during promotional "Free Layaway" period. In Rhode Island and Ohio, you may cancel your order within 14 days without any cancellation fee and
				re-order any items at that time. In Maryland, there is no cancellation fee for orders cancelled within 7 days of the order. Also in Maryland, if the price of the item you order is reduced within 10 days of your order, you are entitled to a credit
				for the difference in price.</p>
			<br>
			<h4>Can I change or add items to my layaway?</h4>
			<p>We do not allow changes or additions to an existing layaway. If you wish to make changes, you will need to cancel and open a new layaway account. Layaway cancellations may result in our standard layaway cancellation fee - see Layaway Cancellation
				Fee Schedule below.</p>
			<br>
			<h4>After my contract is paid in full, when can I pick up my order?</h4>
			<p>If you make your final payment in store, your order will be ready to take home with you immediately following your payment. If you make your final payment online, you will receive an email indicating that your order is ready to be picked up. </p>
			<p>Please note, if you've purchased items that need to be shipped to the store, the items will ship once final payment is received. </p>
			<br>
			<h4>How much do I have to pay to start my layaway?</h4>
			<p>A deposit of at least 10% of the total price of your order, and a $5 service fee are due when the merchandise is put into layaway. You may make additional payments at any time, but payments must be made bi-monthly as follows 20% within fifteen
				(15) days; 30% within thirty (30) days; 40% within forty five (45) days; 50% within sixty (60) days; 60% within seventy five (75) days and the total price of your order must be paid within ninety (90) days or prior to the end of the holiday layaway
				period, whichever is earlier. In Maryland, the service fee is limited to $1.00 for goods $500.00 or less and $5.00 for goods over $500.00. </p>
			<br>
			<h4>How do I find my layaway order number?</h4>
			<p>Your layaway order number is located at the top of your store register receipt, which you received when you created your order.</p>
			<br>
		</div>
	</div> -->
</dsp:page> --%>