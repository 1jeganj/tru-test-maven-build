<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" /> 
<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof var="contextPath" value="${originatingRequest.contextPath}"/>
	<p><span><fmt:message key="layaway.guestaccount.registeruser.header"/></span></p>

	<p><fmt:message key="layaway.guestaccount.registeruser.message"/></p>

	<a href="${contextPath}myaccount/myAccount.jsp"><fmt:message key="layaway.guestaccount.registeruser.goAccount"/></a>
</dsp:page>