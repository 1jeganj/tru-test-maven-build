<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:getvalueof var="customerId" bean="Profile.id"/>
<input type="hidden" id="customerId" value="${customerId}">
<input type="hidden" id="layawayPageNumber" value="1">
<input type="hidden" id="noOfRecordsPerPage" value="5">
<input type="hidden" id="layawayPage" value="true">
<dsp:importbean bean="/com/tru/common/droplet/TRUUrlConstructDroplet"/>
<dsp:droplet name="TRUUrlConstructDroplet">
		<dsp:param name="orderListParam" value="orderList" />
		<dsp:param name="orderDetailParam" value="orderDetail" />
		<dsp:param name="orderCancelParam" value="orderCancel" />
		<dsp:oparam name="output">
			<dsp:getvalueof var='omsOrderDetailRestAPIURL' param='orderDetailURL'/>
			<input type="hidden" id="omsOrderDetailRestAPIURL" value="${omsOrderDetailRestAPIURL}">
			<dsp:getvalueof var='omsOrderListRestAPIURL' param='orderListURL'/>
			<input type="hidden" id="omsOrderListRestAPIURL" value="${omsOrderListRestAPIURL}">
			<dsp:getvalueof var='omsCancelOrderRestAPIURL' param='orderCancelURL'/>
			<input type="hidden" id="omsCancelOrderRestAPIURL" value="${omsCancelOrderRestAPIURL}">
		</dsp:oparam>
</dsp:droplet>
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
<input type="hidden" id="pushSiteId" value="${site}"/>
<%@ page isELIgnored ="true" %>
	<div id="orderHistory" class="tab-pane layaway-orders">
		<div class="my-account-order-history">
			<div class="order-history-header row">
				<div class="">
					<h1><fmt:message key="layaway.accountpayemnt.header"/></h1>
					<p><fmt:message key="layaway.accountpayment.message"/></p>
				</div>
			</div>
			<div class="layaway-order-history-table-header row">
				<div class="col-md-4"><fmt:message key="layaway.orderHistory.layawayNumber"/></div>
				<div class="col-md-4"><fmt:message key="layaway.orderHistory.dateCreated"/></div>
				<div class="col-md-4"><fmt:message key="layaway.orderHistory.orderStatus"/></div>
				
			</div>
			<div class="tab-content"></div>
			<div class="row row-load-more hide">
				<div id="layawayLoadMore" class="load-more more">
					<fmt:message key="myaccount.orderHistory.loadMore" />
				</div>
		  </div>			 	
		</div>
	</div> 
	<!--<p>${customerTransactionListResponse.customerOrders.customerOrder[0].orderCreatedDate}</p>
						<p>${customerTransactionListResponse.customerOrders.customerOrder[0].customerInfo.customerFirstName}</p>
						 <span>${customerTransactionListResponse.customerOrders.customerOrder[0].orderStatus}</span>
						 <!--<span>${customerTransactionListResponse.customerOrders.customerOrder[0].orderStatus}</span>-->
<script id="layaway-order-history-template" type="text/html">
{{if customerTransactionListResponse.responseStatus == true && customerTransactionListResponse.totalNoOfRecords > 0 }}
{{each customerTransactionListResponse.customerOrders.customerOrder}}  
	<div id="orderHistory" class="order-history-order row layaway-orders" tabindex="0">
		<div class="col-md-4"><a class="order-information layaway-order-num" title="${orderNumber}" href="javascript:void(0);">${orderNumber}</a></div>
		<div class="col-md-4"><p>${orderCreatedDate}</p></div>
		<div class="col-md-4">
		<img src="${TRUImagePath}/images/layaway-order-status.png">
		<p class="inline">
		${layawayStatus}<br/>
		{{if layawayStatus == 'Active'}}  
				<span class="order-information layaway-order-num" title="${orderNumber}"> ${customerOrderDetails.makePayment}</span>
		{{/if}}
		</p></div>
	</div>

{{/each}}
{{else}}
{{if customerTransactionListResponse.messages != null}}
	${customerTransactionListResponse.messages.message.description}</p>
{{else}}
	<p>${customerOrderDetails.layawayNoOrderMessage}</p>
{{/if}}
{{/if}}
</script>

</dsp:page>