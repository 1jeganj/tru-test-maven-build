<dsp:page>
	<div class="checkout-shipping-address-form">
	<form id="billingInfoApplicationForm" class="JSValidation" method="POST" onsubmit="javascript: return false;">
		<dsp:include page="layawayBillingAddressForm.jsp"/>
	</form>
	</div>
	<hr>
	<div class="checkout-payment-giftcard">
	<form id="giftCardInfoApplicationForm" class="JSValidation" method="POST" onsubmit="javascript: return false;">
		<dsp:include page="layawayGiftCard.jsp"/>
	</form>
	</div>
	<hr>
	<div class="layaway-credit-component">
	<form id="creditCardInfoApplicationForm" class="JSValidation" method="POST" onsubmit="javascript: return false;">
		<dsp:include page="layawayCreditCard.jsp"/>
	</form>
	</div>
</dsp:page>