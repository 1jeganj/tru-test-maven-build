<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
	<%-- <dsp:importbean bean="/com/tru/integrations/cardinal/CardinalJwtDroplet" /> --%>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	
	<%-- Start - Writing hidden input field for expiry month & year for PLCC card --%>
	<dsp:getvalueof var="truConf" bean="TRUConfiguration" />
	<input type="hidden" name="expMonthForPLCC" id="expMonthForPLCC" value="${truConf.expMonthAdjustmentForPLCC}"/>
	<input type="hidden" name="expYearForPLCC" id="expYearForPLCC" value="${truConf.expYearAdjustmentForPLCC}"/>
	<%-- End - Writing hidden input field for expiry month & year for PLCC card --%>
	
	<div id="cardianlOrderDetails">
		<dsp:getvalueof var="orderId" bean="ShoppingCart.layawayOrder.layawayOrderid" />
		<dsp:getvalueof var="orderAmount"bean="ShoppingCart.layawayOrder.paymentAmount" />
		<dsp:getvalueof var="currencyCode" bean="TRUConfiguration.currencyCode" />
		<dsp:getvalueof var="profileId" bean="Profile.id" />
	
	<%-- 	<dsp:droplet name="CardinalJwtDroplet">
			<dsp:oparam name="output">
		       	<dsp:getvalueof var="jwt" param="jwt"/>
		     </dsp:oparam>
		</dsp:droplet> 
	
		<input type="hidden" name="jwt" id="jwt" value="${jwt}" /> --%>
		<input type="hidden" name="cardianlOrderId" id="cardianlOrderId"value="${orderId}" />
		<input type="hidden" name="cardianlOrderAmount" id="cardianlOrderAmount" value="${orderAmount}" />
		<input type="hidden" name="currencyCode" id="currencyCode" value="${currencyCode}" />
		<input type="hidden" name="profileId" id="profileId" value="${profileId}" />
	</div>

	<div id="guestMakePayment" class="tab-pane layaway-orders">
		<dsp:include page="layawayPaymentFragment.jsp" />
	</div>
	<dsp:include page="exitPaymentOverlay.jsp" />
	
	 <div class="modal fade" id="layawayTermsAndCondition" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content sharp-border">
			  <dsp:droplet name="/atg/targeting/TargetingFirst">
				<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/Layaway/TermsConditionsLayawayTargeter" />
				<dsp:oparam name="output">
					<dsp:valueof param="element.data" valueishtml="true" />
				</dsp:oparam>
			</dsp:droplet>  
			</div>
		</div>
	</div> 
	
	<dsp:include page="../../checkout/payment/creditCard/creditCardInfoOverlay.jsp">
		<dsp:param name="stopScroll" value="" />
	</dsp:include> 
	
	<dsp:include page="/checkout/payment/giftCardOverlay.jsp" />
	<div class="modal fade" id="layawayPaymentProcessingOverlay" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" class="display-none">
		<div class="modal-dialog modal-md">
			<div class="modal-content sharp-border">
				<img class="" src="${TRUImagePath}images/processing.gif" alt="Processing...">
				<header class="review-processing-header">
					<fmt:message key="checkout.review.processOrderOverlayHeading" />
				</header>
				<p>
					<fmt:message key="checkout.review.processOrderOverlaySubHeading" />
				</p>
			</div>
		</div>
	</div>
</dsp:page>
