<dsp:page>
	<div class="modal fade" id="layawayExitPaymentModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content sharp-border">
				<a href="javascript:void(0)" data-dismiss="modal" title="Close">
					<span class="clickable"><img  src="${TRUImagePath}images/close.png" alt="Close Popup"></span>
				</a>
				<div>
					<h2>Are you sure you want to continue?</h2>
					<p class="layawayExitMessage">Changing the payment amount will remove any Gift Cards that have been applied.</p>
					<a id="cancel-exit-process" href="javascript:void(0);">cancel</a>
					<button id="yes-exit-payment">yes</button>
				</div>
			</div>
		</div>
	</div>
</dsp:page>