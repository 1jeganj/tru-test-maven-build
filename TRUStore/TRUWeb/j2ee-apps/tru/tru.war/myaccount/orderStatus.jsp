<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> -->
<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<!-- <html lang="en">
<body> -->

<dsp:importbean bean="/com/tru/common/droplet/TRUUrlConstructDroplet"/>
<dsp:droplet name="TRUUrlConstructDroplet">
		<dsp:param name="orderDetailParam" value="orderDetail" />
		<dsp:param name="orderCancelParam" value="orderCancel" />
		<dsp:oparam name="output">
			<dsp:getvalueof var='omsOrderDetailRestAPIURL' param='orderDetailURL'/>
			<input type="hidden" id="omsOrderDetailRestAPIURL" value="${omsOrderDetailRestAPIURL}">
			<dsp:getvalueof var='omsCancelOrderRestAPIURL' param='orderCancelURL'/>
			<input type="hidden" id="omsCancelOrderRestAPIURL" value="${omsCancelOrderRestAPIURL}">
		</dsp:oparam>
</dsp:droplet>
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
 <input type="hidden" id="pushSiteId" value="${site}"/>
<div class="col-md-4">
                        <div class="my-account-sign-in-col order-status">
                            
                            <!-- div for password strength tooltip positioning -->
	                        <form class="JSValidation" id="truOrderStatusSignInForm" action="#">
	                            <div class="truLoginPage" id="truOrderStatus">
									<h2><fmt:message key="myaccount.orderDetails.guestUser.orderStatus" /></h2>
									<p><fmt:message key="myaccount.orderDetails.guestUser.likeToTrack" /></p>
	                                <label for="order-number"><fmt:message key="myaccount.orderDetails.guestUser.orderNumber" /></label>
	                                <input name="orderNumber" type="text" class="orderNumber" id="order-number">
	                                <label for="zip-code"><fmt:message key="myaccount.orderDetails.guestUser.zipCode" /></label>
	                                <input name="postalCode" type="text" class="zipCode" id="zip-code">
	                                <div class="orderError"></div>
	                                <label class="confirm-password"><fmt:message key="myaccount.orderDetails.guestUser.confirmPassword" /></label>
	                                <input class="confirm-password" type="password">
	                            </div>
	                            <div class="button-container">
	                                <button type ="button" id="orderStatusSignIn"><fmt:message key="myaccount.orderDetails.guestUser.trackYourOrder" /></button>
	                                <a href="./Toolkit_files/Toolkit.html"><fmt:message key="myaccount.orderDetails.guestUser.forgotPassword" /></a>
	                            </div>
	                        </form>
                            <div class="create-account-thank-you">
                                <h2><fmt:message key="myaccount.orderDetails.guestUser.orderStatus" /></h2>
                                <p><fmt:message key="myaccount.orderDetails.guestUser.likeToTrack" /></p>
                                <button onclick="goToMyAccount();"><fmt:message key="myaccount.orderDetails.guestUser.trackYourOrder" /></button>
                                <button ><fmt:message key="myaccount.orderDetails.guestUser.trackYourOrder" /></button>
                                <p><fmt:message key="myaccount.orderDetails.guestUser.likeToTrack" /></p>
                                <ul>
                                    <li><fmt:message key="myaccount.orderDetails.guestUser.fasterCheckout" /></li>
                                    <li><fmt:message key="myaccount.orderDetails.guestUser.personalAddress" /></li>
                                    <li><fmt:message key="myaccount.orderDetails.guestUser.keepYourCreditCard" /></li>
                                    <li><fmt:message key="myaccount.orderDetails.guestUser.tarckAndReview" /></li>
                                    <li><fmt:message key="myaccount.orderDetails.guestUser.improvedCustomer" /></li>
                                </ul>
                            </div>
                        </div>
                    </div>
        <!-- Added radialOm myAccountOrderDetails.jsp path to for radial order details display remove radialOm for default display for R3 -->            
       <dsp:include page="/myaccount/radialOm/myAccountOrderDetails.jsp" >
       </dsp:include>
<!-- </body>
</html> -->
</dsp:page>