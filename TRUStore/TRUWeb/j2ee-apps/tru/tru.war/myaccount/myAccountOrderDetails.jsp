<%@ page isELIgnored ="true" %>
<dsp:page>

		
				<script type='text/x-jquery-tmpl' id='myaccount-order-history-template'>
				{{if orders != null}}
                {{each orders}} 
                    <div class="order-history-order row">
                        <div class="col-md-3">
                            ${OrderDate}
                        </div>
                        <div class="col-md-3">
                            <a href="" class="orderHistoryDetailModal" data-toggle="modal" data-target="#orderHistoryDetailModal" title="${WebOrderId}">${WebOrderId}</a>
                        </div>
                        <div class="col-md-3">
							<p>${OrderStatus}</p>
							<a href="javascript:void(0)" class="orderHistoryDetailModal" data-toggle="modal" data-target="#orderHistoryDetailModal" title="${WebOrderId}">${customerOrderDetails.seeDetails}</a>
                            {{if OrderStatus == 'New'}}   
								<br>       
		                  		<a href="javascript:void(0)" data-toggle="modal" data-target="#orderHistoryCancelOrder" class="cancelOrder">${customerOrderDetails.cancelOrder}</a>
                       		{{/if}}
                        </div>
                        <div class="col-md-3 pull-right">
                            ${OrderTotal}
                        </div>
                    </div>
				{{/each}}
				{{else}}
				{{if customerTransactionListResponse.messages != null}}
				${customerTransactionListResponse.messages.message.description}</p>
				{{else}}
				<p>${customerOrderDetails.noOrderMessage}</p>
				{{/if}}
				{{/if}}
					</script>
					
				<script type='text/x-jquery-tmpl' id='myaccount-order-history-details'>
{{if customerOrder.responseStatus == true}}
      <div class="modal-dialog order-history-detail-modal">
            <div class="modal-content sharp-border">
                <div class="modal-title">
                    ${customerOrderDetails.orderDetail}
                    <a href="javascript:void(0)" data-dismiss="modal">
                    <span class="sprite-icon-x-close"></span>
                    </a>
                    <div>
                        ${customerOrderDetails.orderedOn} ${customerOrder.orderCapturedDate} ${customerOrderDetails.order} ${customerOrderDetails.hash}${customerOrder.orderNumber}
                    </div>
                </div>
                <div class="tse-scrollable"><div class="tse-scrollbar" style="display: none;"><div class="drag-handle visible" style="top: 34px; height: 185px;"></div></div>
                    <div class="tse-scroll-content tse-order-history-content" style="height: 580px;">
                        <div class="tse-content order-history-detail-modal-content">
                            <div class="row">
                                <div class="col-md-8 col-no-padding">
                                    <div class="row">
                                        <div class="col-md-5 col-no-padding">
											{{if customerOrder.paymentDetails != null}}	
                                            <div class="order-detail-small-header">${customerOrderDetails.billingAddress}</div>
                                            <br>
											{{each customerOrder.paymentDetails.paymentDetail}}
											{{if paymentMethod == 'Credit Card' || paymentMethod == 'Paypal'}}
                                            <div class="address">
                                                <span>${billToDetail.firstName} ${billToDetail.lastName}</span>
                                                <div>${billToDetail.addressLine1}${billToDetail.addressLine2}</div>
                                                <div>${billToDetail.city}${customerOrderDetails.comma}${billToDetail.stateProv} ${billToDetail.postalCode}</div>
												<div>${billToDetail.phone}</div>
												<div>${billToDetail.country}</div>
                                            </div>
                                            <br>
											{{/if}}	
											{{/each}}
											{{if customerOrder.referenceFields.referenceField10 != null}}
												Rewards"R"Us Member #:${customerOrder.referenceFields.referenceField10}
											{{else}}
												No Rewards Cards To Display	
											{{/if}}
											{{else}}
											{{if customerOrder.referenceFields.referenceField10 != null}}
											<div class="order-detail-small-header">${customerOrderDetails.billingAddress}</div>
											<br>
											<div class="address">
												Rewards"R"Us Member #:${customerOrder.referenceFields.referenceField10}
											</div>
                                            <br>
											{{else}}
											<div class="order-detail-small-header">${customerOrderDetails.billingAddress}</div>
											<br>
											<div class="address">
												No Billing Information	
											</div>
                                            <br>
											{{/if}}	
											{{/if}}
                                            <p class="returns-warranties">
                                                <a href=${customerOrder.helpAndReturns}>${customerOrderDetails.helpReturnWarranties}</a>
                                            </p>
                                           {{if customerOrder.orderStatus == 'New'}}   
											<div>
												<input type= "hidden" class="hiddenOrderNumber" value=${customerOrder.orderNumber}>
												<a href="javascript:void(0)" data-toggle="modal" data-target="#orderHistoryCancelOrder" class="cancelOrder">cancel order</a>                                                          
 											</div>
                                           {{/if}}
                                        </div>
                                        <div class="col-md-5 col-no-padding">
											{{if customerOrder.paymentDetails != null}}	
                                            <div class="order-detail-small-header">${customerOrderDetails.paymentMethod}</div>
                                            <br>
                                            <div class="payment-method">
											{{each customerOrder.paymentDetails.paymentDetail}}	
												{{if paymentMethod == 'Credit Card'}}
												{{if cardType == 'Visa'}}
												
                                                <div class="card-number">
                                                    <div class="visa-logo-sm inline"></div>
                                                    <div class="inline">${accountDisplayNumber}</div>
                                                </div>
												{{/if}}
												{{if cardType == 'Master Card'}}
												<span>${cardType}</span>
                                                <div class="card-number">
                                                    <div class="mastercard-logo-sm inline"></div>
                                                    <div class="inline">${accountDisplayNumber}</div>
                                                </div>
												{{/if}}
												{{if cardType == 'Mastercard'}}
												<span>${cardType}</span>
                                                <div class="card-number">
                                                    <div class="mastercard-logo-sm inline"></div>
                                                    <div class="inline">${accountDisplayNumber}</div>
                                                </div>
												{{/if}}
												{{if cardType == 'Amex'}}
												<span>${cardType}</span>
                                                <div class="card-number">
                                                    <div class="amex-logo-sm inline"></div>
                                                    <div class="inline">${accountDisplayNumber}</div>
                                                </div>
												{{/if}}
												{{if cardType == 'Discover'}}
												<span>${cardType}</span>
                                                <div class="card-number">
                                                    <div class="discover-logo-sm inline"></div>
                                                    <div class="inline">${accountDisplayNumber}</div>
                                                </div>
												{{/if}}
												{{if cardType == 'RUS Co Branded CC'}}
												<span>${cardType}</span>
                                                <div class="card-number">
                                                    <div class="rewardsrus-card-logo-sm inline"></div>
                                                    <div class="inline">${accountDisplayNumber}</div>
                                                </div>
												{{/if}}
												{{if cardType == 'RUS Private Label CC'}}
												<span>${cardType}</span>
                                                <div class="card-number">
                                                    <div class="rewardsrus-mastercard-logo-sm inline"></div>
                                                    <div class="inline">${accountDisplayNumber}</div>
                                                </div>
												{{/if}}
                                                <div>${formattedAuthorizationAmount} ${customerOrderDetails.balanceText}</div>
												{{else}}
												<span>${paymentMethod}</span>
												{{if paymentMethod == 'Gift Card'}}
												<div class="inline"></div>
                                                <div class="card-number">
                                                    <div class="inline">${accountDisplayNumber}</div>
                                                </div>
												{{/if}}
												{{/if}}
											{{/each}}
                                            </div>
											{{else}}
											{{if customerOrder.orderType != null}}
											<div class="order-detail-small-header">${customerOrderDetails.paymentMethod}</div>
												${customerOrder.orderType}
											{{/if}}
											{{/if}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-no-padding pull-right">
                                    <div class="order-history-order-summary shopping-cart-summary-block">
                                        <div class="summary-block-border">
                                            <div class="summary-block-padded-content" tabindex="0">
                                                <div class="order-summary">
                                                   ${customerOrderDetails.orderSummary}
                                                </div>
                                                <div class="subtotal order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.subTotal} (${customerOrder.totalQuantity} ${customerOrderDetails.items})</span>
                                                    <span class="summary-price pull-right order-summary-item-value">${customerOrder.orderTotals.formattedItemSubTotal}</span>
                                                </div>
												{{if customerOrder.orderTotals.giftwrap > 0}}
                                                <div class="summary-gift-wrap order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.giftWrap}</span>
                                                    <span class="summary-price pull-right order-summary-item-value">${customerOrder.orderTotals.formattedGiftwrap}</span>
                                                </div>
												{{/if}}
												{{if customerOrder.orderTotals.totalDiscounts > 0}}
                                                <div class="summary-promotions order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.promotionalSavings}</span>
                                                    <span class="summary-price pull-right order-summary-item-value">${customerOrderDetails.hyphen}${customerOrder.orderTotals.formattedTotalDiscounts}</span>
                                                </div>
												{{/if}}
												{{if customerOrder.orderTotals.shipping > 0}}
                                                <div class="summary-shipping order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.shipping}</span>
                                                    <span class="summary-price order-summary-item-value">${customerOrder.orderTotals.formattedShipping}</span>
                                                </div>
												{{/if}}
												{{if customerOrder.orderTotals.totalShippingSurcharge > 0}}
											    <div class="summary-shipping-surcharge order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.shippingSurcharge}</span>
                                                    <span class="summary-price pull-right order-summary-item-value">${customerOrder.orderTotals.formattedTotalShippingSurcharge}</span>
                                                </div>		
												{{/if}}
												{{if customerOrder.orderTotals.totalTaxes > 0}}
                                                <div class="summary-sales-tax order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.salesTax}</span>
                                                    <span class="summary-price pull-right order-summary-item-value">${customerOrder.orderTotals.formattedTotalTaxes}</span>
                                                </div>
												{{/if}}
												{{if customerOrder.orderTotals.ewaste > 0}}
                                                <div class="summary-sales-tax order-summary-item">
                                                    <span class="order-summary-item-label">${customerOrderDetails.ewaste}</span>
                                                    <span class="summary-price pull-right order-summary-item-value">${customerOrder.orderTotals.formattedEwaste}</span>
                                                </div>
												{{/if}}
                                                <div class="footer-total">
                                                    <span class="summary-block-footer-total">${customerOrderDetails.balance}</span>
                                                    <span class="summary-price order-summary-item-value">${customerOrder.orderTotals.formattedGrandTotal}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="horizontal-divider">
                            <div class="pull-right">
                                <div class="norton-secured">
                                    <img src="${TRUImagePath}images/Global_Norton-Logo.jpg" alt="Norton Logo">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9 col-no-padding">
                               {{if  customerOrder.iscartonAvailable == false}}
								{{each customerOrder.displayOrder.shippingInfoArray}}
                                    <div class="checkout-review-items-shipping-to items-shipping-to-no-edit">
                                        <div class="row">
                                            <div class="col-md-12 items-shipping registry">
                                                <div class="">
												{{if shippingInfo.shipToFacility == 'DONATION'}}
												donation
												{{else}}
												{{if shippingInfo.deliveryOption == 'Customer pickup' || shippingInfo.deliveryOption == 'Ship to store'}}
                                                	${customerOrderDetails.itemsPickupInstore} ${customerOrderDetails.at}
 													<div class="items-shipping-address"> 
														 ${shippingInfo.storeName}<span>&#xb7;</span>${shippingInfo.shippingAddress.addressLine1}
														 ${shippingInfo.shippingAddress.addressLine2}
														${shippingInfo.shippingAddress.city},${shippingInfo.shippingAddress.stateProv},${shippingInfo.shippingAddress.postalCode}
                                                	</div>
													<div class="">
													{{if PrimaryNames != "" && PrimaryNames != null}}
														${customerOrderDetails.pickedUpBy} ${PrimaryNames}
													{{/if}}
													{{if ProxyNames != "" && ProxyNames != null}}
													 ${customerOrderDetails.andOr} ${ProxyNames}
													{{/if}}
													</div>
												{{else}}
                                                    ${shippingInfo.itemCount} ${customerOrderDetails.itemsShipping} ${shippingInfo.shippingAddress.firstName} ${shippingInfo.shippingAddress.lastName}
                                                <div class="items-shipping-address">
													${shippingInfo.shippingAddress.city}, ${shippingInfo.shippingAddress.stateProv} ${shippingInfo.shippingAddress.postalCode}
                                       			 </div>
                                       			{{/if}}
												{{/if}}
                                            </div>
                                        </div>
                                    </div>
									{{each shipment}}
									{{if shippingInfo.shipToFacility != 'DONATION' && shippingInfo.deliveryOption != 'Customer pickup' && shippingInfo.deliveryOption != 'Ship to store'}}
									 <div class="row extra-information">
                                            <div class="col-md-12">
												<div class="order-shipped-on">
                                                    <p>${customerOrderDetails.shipment} ${shipmentItemCount} of ${shipItemTotalCount}</p>
													{{if startDate !="" && endDate !="" }}
													<p>You can expect to recieve your items between ${startDate} and ${endDate} </p>
													{{/if}}
                                                    <header>${shipViaDisplay}</header>
                                                </div> 
												{{if isGiftMessage}}	
												<div class="gift-message">
													<header>${customerOrderDetails.giftMessage}</header>
													<p class="giftMessage"> ${giftMessage}</p>	
												</div>
												{{/if}} 
                                            </div>
                                        </div>
                                    <hr>
									{{/if}}
									{{each orderLine}}
                                    <div class="checkout-review-product-block product-block-no-edit gift-wrapped purchased-protection-plan">
                                        <div class="checkout-review-egift-card">
                                            <h2>eGift Card 1 of 2</h2>
                                            <p>To: Von</p>
                                            <hr class="horizontal-divider">
                                        </div>
                                        <div class="product-information">
                                            <div class="product-information-header">
											{{if shippingInfo.shipToFacility != 'DONATION'}}
                                                <div class="product-header-title">
                                                    <a href=${productPageUrl}>${itemLongDescription}</a>
                                                </div>
											{{else}}
											<div class="product-header-title">
                                                    Donation Item
                                                </div>
											{{/if}}
                                                <div class="product-header-price">
                                                    ${orderLineTotals.formattedItemSubTotal}
                                                    <div class="shipping-surcharge">
                                                       ${surcharge}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-information-content">
                                                <div class="row row-no-padding">
                                                    <div class="col-md-2">
                                                        <div>
                                                            <img class="product-description-image" src=${itemImageFilename2}${customerOrderDetails.imageFitRatio} alt="shopping cart product image">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 donation-amount"></div>
                                                    <div class="col-md-5 product-information-description" tabindex="0">
														{{if colorDesc}}
                                                        <div class="product-description-color">${customerOrderDetails.color}${colorDesc}</div>
														{{/if}}
														{{if itemSize}}
                                                        <div class="product-description-size">${customerOrderDetails.size}${itemSize}</div>
													    {{/if}}
														{{if shippingInfo.shipToFacility != 'DONATION'}}
                                              				  <div class="qty">${customerOrderDetails.quantity}${orderedQuantity}</div>
														{{else}}
														<div class="qty">
                                              	    			  Thank you for your donation!
                                              			  </div>
														{{/if}}
														{{if isGift == true}}
                                                        <div class="checkout-item-gift-wrapped">
                                                            <img src="${TRUImagePath}images/item-gift-wrapped.png" alt="Image could not be loaded">
                                                            <div>${customerOrderDetails.giftWrapText}${siteNameForGW} gift wrap</div>
                                                        </div>
														{{/if}}
                                                        <p>
                                                            </p><div class="edit"><a href="http://cloud.toysrus.resource.com/redesign/template-my-account-main.html#">edit</a><span>·</span><a href="http://cloud.toysrus.resource.com/redesign/template-my-account-main.html#">remove</a>
                                                            </div>
                                                        <p></p>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="days-until-ready"><span>${shipViaDescription}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 change-store-container">
                                                        <div class="change-store">
                                                            <p><a href="http://cloud.toysrus.resource.com/redesign/template-my-account-main.html#">change your store</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                   						{{if  customerOrder.chargeDetails != null}}
										{{each customerOrder.chargeDetails.chargeDetail}}
                                        {{if chargeCategory == "Value Added Services"}}
                                        <div class="protection-plan">
                                            <div class="inline">
                                                <div class="buyer-protection-plan-image inline"></div>
                                            </div>
                                            <div class="inline">
                                                <div class="checkbox-group">
                                                    ${customerOrderDetails.protectWith}<span>${bppTerm}${customerOrderDetails.squareTradePlan}</span> <a target="_blank" href="http://www.toysrus.com/helpdesk/popup.jsp?display=returns&subdisplay=bpp">${customerOrderDetails.learnMore}</a>
                                                    <span class="protection-plan-static-copy">${bppChargeAmount}</span>
                                                </div>
                                                <p>${customerOrderDetails.squareTradePlanText}</p>
                                            </div>
                                        </div>
                                        {{/if}}
                                       {{/each}}
										{{/if}}
                                    </div>
									<hr class="horizontal-divider">
 									{{/each}}
								   {{/each}}
								{{/each}}
								{{else}}
								{{each customerOrder.displayOrder.shippingInfoArray}}
                                    <div class="checkout-review-items-shipping-to items-shipping-to-no-edit">
                                        <div class="row">
                                            <div class="col-md-12 items-shipping registry">
                                                <div class="">
												{{if shippingInfo.shipToFacility == 'DONATION'}}
												donations
												{{else}}			
												{{if shippingInfo.deliveryOption == 'Customer pickup' || shippingInfo.deliveryOption == 'Ship to store'}}
                                                	 ${customerOrderDetails.itemsPickupInstore} ${customerOrderDetails.at}
 													<div class="items-shipping-address"> 
													    ${shippingInfo.storeName}<span>&#xb7;</span>${shippingInfo.shippingAddress.addressLine1} ${shippingInfo.shippingAddress.addressLine2} ${shippingInfo.shippingAddress.city},${shippingInfo.shippingAddress.stateProv},${shippingInfo.shippingAddress.postalCode}
                                              	   </div>
													<div class="">
													{{if PrimaryNames != "" && PrimaryNames != null}}
														${customerOrderDetails.pickedUpBy} ${PrimaryNames}
													{{/if}}
													{{if ProxyNames != "" && ProxyNames != null}}
														 ${customerOrderDetails.andOr} ${ProxyNames}
													{{/if}}
													</div> 
												{{else}}
                                                    ${shipmentItemCount} ${customerOrderDetails.itemsShipping} ${shippingInfo.shippingAddress.firstName} ${shippingInfo.shippingAddress.lastName}
                                                <div class="items-shipping-address">
													${shippingInfo.shippingAddress.city}, ${shippingInfo.shippingAddress.stateProv} ${shippingInfo.shippingAddress.postalCode}
                                        		</div>
												{{/if}}
												{{/if}}
                                            </div>
                                        </div>
                                    </div>
									<br>
									{{if isCartonCheck == true}}
									{{each cartons}}
									{{if shippingInfo.shipToFacility != 'DONATION'  && shippingInfo.deliveryOption != 'Customer pickup' && shippingInfo.deliveryOption != 'Ship to store'}}
									 <div class="row extra-information">
                                            <div class="col-md-12">
                                                <div class="order-shipped-on">
                                                    <p>${customerOrderDetails.shipment} ${cartonLineItemCount} of ${cartonCount} shipped on ${formattedShipDate}<span>·</span>${customerOrderDetails.trackingNumber}<a class="review-order-tracking-number" href=${trackingWebURL}>${trackingNbr}</a>
                                                    </p>
                                                    <header>${shipVia}</header>
                                                </div>
                                            </div>
                                        </div>
                                    <hr>
									{{/if}}
									{{each orderLines}}
                                    <div class="checkout-review-product-block product-block-no-edit gift-wrapped purchased-protection-plan">
                                        <div class="checkout-review-egift-card">
                                            <h2>eGift Card 1 of 2</h2>
                                            <p>To: Von</p>
                                            <hr class="horizontal-divider">
                                        </div>
                                        <div class="product-information">
                                            <div class="product-information-header">
											{{if shippingInfo.shipToFacility != 'DONATION'}}
                                                <div class="product-header-title">
                                                    <a href=${productPageUrl}>${itemLongDescription}</a>
                                                </div>
											{{else}}
												<div class="product-header-title">
                                                    	Donation Item
                                                </div>
											{{/if}}
                                                <div class="product-header-price">
                                                    ${orderLineTotals.formattedItemSubTotal}
                                                    <div class="shipping-surcharge">
                                                       ${surcharge}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-information-content">
                                                <div class="row row-no-padding">
                                                    <div class="col-md-2">
                                                        <div>
                                                            <img class="product-description-image" src=${itemImageFilename2}${customerOrderDetails.imageFitRatio} alt="shopping cart product image">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 donation-amount"></div>
                                                    <div class="col-md-5 product-information-description" tabindex="0">
														{{if colorDesc}}
                                                        <div class="product-description-color">${customerOrderDetails.color}${colorDesc}</div>
														{{/if}}
														{{if itemSize}}
                                                        <div class="product-description-size">${customerOrderDetails.size}${itemSize}</div>
													    {{/if}}
                                                       	{{if shippingInfo.shipToFacility != 'DONATION'}}
                                              				  <div class="qty">${customerOrderDetails.quantity}${orderedQuantity}</div>
														{{else}}
														<div class="qty">
                                              	    			  Thank you for your donation!
                                              			  </div>
														{{/if}}
														{{if isGift == true}}
                                                        <div class="checkout-item-gift-wrapped">
                                                            <img src="${TRUImagePath}images/item-gift-wrapped.png" alt="Image could not be loaded">
                                                            <div>${customerOrderDetails.giftWrapText}${siteNameForGW} gift wrap</div>
                                                        </div>
														{{/if}}
                                                        <p>
                                                            </p><div class="edit"><a href="http://cloud.toysrus.resource.com/redesign/template-my-account-main.html#">edit</a><span>·</span><a href="http://cloud.toysrus.resource.com/redesign/template-my-account-main.html#">remove</a>
                                                            </div>
                                                        <p></p>
                                                    </div>
                                                    <div class="col-md-5 change-store-container">
                                                        <div class="change-store">
                                                            <p><a href="http://cloud.toysrus.resource.com/redesign/template-my-account-main.html#">change your store</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                   						{{if  customerOrder.chargeDetails != null}}
										{{each customerOrder.chargeDetails.chargeDetail}}
                                        {{if chargeCategory == "Value Added Services"}}
                                        <div class="protection-plan">
                                            <div class="inline">
                                                <div class="buyer-protection-plan-image inline"></div>
                                            </div>
                                            <div class="inline">
                                                <div class="checkbox-group">
                                                    ${customerOrderDetails.protectWith}<span>${bppTerm}${customerOrderDetails.squareTradePlan}</span> <a target="_blank" href="http://www.toysrus.com/helpdesk/popup.jsp?display=returns&subdisplay=bpp">${customerOrderDetails.learnMore}</a>
                                                    <span class="protection-plan-static-copy">${bppChargeAmount}</span>
                                                </div>
                                                <p>${customerOrderDetails.squareTradePlanText}</p>
                                            </div>
                                        </div>
                                        {{/if}}
                                       {{/each}}
										{{/if}}
                                    </div>
									<hr class="horizontal-divider">
 									{{/each}}
									{{/each}}
									{{else}}
									{{each shipment}}
									 <div class="row extra-information">
                                            <div class="col-md-12">
												<div class="order-shipped-on">
 													{{if shippingInfo.shipToFacility != 'DONATION'  && shippingInfo.deliveryOption != 'Customer pickup' && shippingInfo.deliveryOption != 'Ship to store'}}
                                                    <p>${customerOrderDetails.shipment} ${shipmentItemCount} of ${shipItemTotalCount}</p>
													{{/if}}
													{{if startDate !="" && endDate !="" }}
													<p>You can expect to recieve your items between ${startDate} and ${endDate} </p>
													{{/if}}
                                                    <header>${shipViaDisplay}</header>
                                                </div> 
												{{if isGiftMessage}}	
												<div class="gift-message">
													<header>${customerOrderDetails.giftMessage}</header>
													<p class="giftMessage"> ${giftMessage}</p>	
												</div>
												{{/if}} 
                                            </div>
                                        </div>
                                    <hr>
									{{each orderLine}}
                                    <div class="checkout-review-product-block product-block-no-edit gift-wrapped purchased-protection-plan">
                                        <div class="checkout-review-egift-card">
                                            <h2>eGift Card 1 of 2</h2>
                                            <p>To: Von</p>
                                            <hr class="horizontal-divider">
                                        </div>
                                        <div class="product-information">
                                            <div class="product-information-header">
											{{if shippingInfo.shipToFacility != 'DONATION'}}
                                                <div class="product-header-title">
                                                    <a href=${productPageUrl}>${itemLongDescription}</a>
                                                </div>
											{{else}}
												<div class="product-header-title">
                                                    	Donation Item
                                                </div>
											{{/if}}
												<div class="product-header-title">
                                                    	Donation Item
                                                </div>
                                                <div class="product-header-price">
                                                    ${orderLineTotals.formattedItemSubTotal}
                                                    <div class="shipping-surcharge">
                                                       ${surcharge}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-information-content">
                                                <div class="row row-no-padding">
                                                    <div class="col-md-2">
                                                        <div>
                                                            <img class="product-description-image" src=${itemImageFilename2}${customerOrderDetails.imageFitRatio} alt="shopping cart product image">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 donation-amount"></div>
                                                    <div class="col-md-5 product-information-description" tabindex="0">
														{{if itemColor}}
                                                        <div class="product-description-color">${customerOrderDetails.color}${colorDesc}</div>
														{{/if}}
														{{if itemSize}}
                                                        <div class="product-description-size">${customerOrderDetails.size}${itemSize}</div>
													    {{/if}}
                                                        {{if shippingInfo.shipToFacility != 'DONATION'}}
                                              				  <div class="qty">${customerOrderDetails.quantity}${orderedQuantity}</div>
														{{else}}
														<div class="qty">
                                              	    			  Thank you for your donation!
                                              			  </div>
														{{/if}}
														{{if isGift == true}}
                                                        <div class="checkout-item-gift-wrapped">
                                                            <img src="${TRUImagePath}images/item-gift-wrapped.png" alt="Image could not be loaded">
                                                            <div>${customerOrderDetails.giftWrapText}${siteNameForGW} gift wrap</div>
                                                        </div>
														{{/if}}
                                                        <p>
                                                            </p><div class="edit"><a href="http://cloud.toysrus.resource.com/redesign/template-my-account-main.html#">edit</a><span>·</span><a href="http://cloud.toysrus.resource.com/redesign/template-my-account-main.html#">remove</a>
                                                            </div>
                                                        <p></p>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="days-until-ready"><span>${shipViaDescription}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 change-store-container">
                                                        <div class="change-store">
                                                            <p><a href="http://cloud.toysrus.resource.com/redesign/template-my-account-main.html#">change your store</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                   						{{if  customerOrder.chargeDetails != null}}
										{{each customerOrder.chargeDetails.chargeDetail}}
                                        {{if chargeCategory == "Value Added Services"}}
                                        <div class="protection-plan">
                                            <div class="inline">
                                                <div class="buyer-protection-plan-image inline"></div>
                                            </div>
                                            <div class="inline">
                                                <div class="checkbox-group">
                                                    ${customerOrderDetails.protectWith}<span>${bppTerm}${customerOrderDetails.squareTradePlan}</span> <a target="_blank" href="http://www.toysrus.com/helpdesk/popup.jsp?display=returns&subdisplay=bpp">${customerOrderDetails.learnMore}</a>
                                                    <span class="protection-plan-static-copy">${bppChargeAmount}</span>
                                                </div>
                                                <p>${customerOrderDetails.squareTradePlanText}</p>
                                            </div>
                                        </div>
                                        {{/if}}
                                       {{/each}}
										{{/if}}
                                    </div>
									<hr class="horizontal-divider">
 									{{/each}}
								   {{/each}}
									{{/if}}
								   {{/each}}
								{{/if}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
{{else}}
<p>${customerOrder.messages.message.description}</p>
{{/if}}
		</script>
</dsp:page>