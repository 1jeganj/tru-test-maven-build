<dsp:page>
<dsp:importbean bean="/com/tru/integration/oms/TRUCustomerOrderCancelDroplet"/>
<dsp:getvalueof var="orderId" param="orderId"/>
<dsp:droplet name="TRUCustomerOrderCancelDroplet">
	<dsp:param name="orderId" value="${orderId}"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="response" param="response"/>
			${response}
	</dsp:oparam>
</dsp:droplet>
</dsp:page>