<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/targeting/TargetingFirst" />
	<dsp:getvalueof bean="Profile.firstName" var="firstName"/>
	<dsp:getvalueof bean="Profile.email" var="email"/>
	
	<div class="my-account-welcome-back">
		<div class="row">
			<div class="col-md-8">
				<h1 class="custmHeading">
					<fmt:message key="myaccount.welcome" />&nbsp;<c:choose>
						<c:when test="${empty firstName}">
							${fn:substringBefore(email, "@")}!
						</c:when>
						<c:otherwise>
							${firstName}!
						</c:otherwise>
					</c:choose></h1>
					<p>
						<fmt:message key="myaccount.not" />&nbsp;<c:choose>
							<c:when test="${empty firstName}">
								${fn:substringBefore(email, "@")}?					
							</c:when>
							<c:otherwise>
								${firstName}?	
							</c:otherwise>
						</c:choose> 
						<a href="javascript:void(0)" id="logoutNotYouId"><fmt:message key="myaccount.login.here" /></a>
					</p>
			</div>
			<div class="col-md-2 pull-right">
				<a 	href="#" class="" data-toggle="modal" data-target="#privacyPolicyModal"><fmt:message key="myaccount.privacy.policy" /></a>
			</div>
				<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters/TRU/AccountManagement/PrivacyPolicyContentTargeter" />
				<dsp:droplet name="TargetingFirst">
					<dsp:param name="targeter" bean="${atgTargeterpath}" />
					<dsp:oparam name="output">
						<dsp:valueof param="element.data" valueishtml="true" />
					</dsp:oparam>
				</dsp:droplet>
		</div>
	</div>
