<dsp:page>
	<dsp:importbean bean="/atg/targeting/TargetingFirst" />
	<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale" />

	<dsp:getvalueof var="atgTargeterpath1" value="/atg/registry/RepositoryTargeters/TRU/AccountManagement/QuickHelpContentTargeter" />
	<dsp:getvalueof var="cacheKey1" value="${atgTargeterpath1}${siteId}${locale}" />
	<dsp:droplet name="/com/tru/cache/TRUQuickHelpContentCacheDroplet">
		<dsp:param name="key" value="${cacheKey1}" />
		<dsp:oparam name="output">
			<dsp:droplet name="TargetingFirst">
				<dsp:param name="targeter" bean="${atgTargeterpath1}" />
				<dsp:oparam name="output">
					<dsp:valueof param="element.data" valueishtml="true" />
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>

	<dsp:getvalueof var="atgTargeterpath2" value="/atg/registry/RepositoryTargeters/TRU/AccountManagement/QuickHelpEmailOrderContentTargeter" />
	<dsp:getvalueof var="cacheKey2" value="${atgTargeterpath2}${siteId}${locale}" />
	<dsp:droplet name="/com/tru/cache/TRUQuickHelpEmailOrderContentCacheDroplet">
		<dsp:param name="key" value="${cacheKey2}" />
		<dsp:oparam name="output">
			<dsp:droplet name="TargetingFirst">
				<dsp:param name="targeter" bean="${atgTargeterpath2}" />
				<dsp:oparam name="output">
					<dsp:valueof param="element.data" valueishtml="true" />
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	
	<dsp:getvalueof var="atgTargeterpath3" value="/atg/registry/RepositoryTargeters/TRU/AccountManagement/QuickHelpDeliveryContentTargeter" />
	<dsp:getvalueof var="cacheKey3" value="${atgTargeterpath3}${siteId}${locale}" />
	<dsp:droplet name="/com/tru/cache/TRUQuickHelpDeliveryContentCacheDroplet">
		<dsp:param name="key" value="${cacheKey3}" />
		<dsp:oparam name="output">
			<dsp:droplet name="TargetingFirst">
				<dsp:param name="targeter" bean="${atgTargeterpath3}" />
				<dsp:oparam name="output">
					<dsp:valueof param="element.data" valueishtml="true" />
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	
	<dsp:getvalueof var="atgTargeterpath4" value="/atg/registry/RepositoryTargeters/TRU/AccountManagement/QuickHelpCancellationContentTargeter" />
	<dsp:getvalueof var="cacheKey4" value="${atgTargeterpath4}${siteId}${locale}" />
	<dsp:droplet name="/com/tru/cache/TRUQuickHelpCancellationContentCacheDroplet">
		<dsp:param name="key" value="${cacheKey4}" />
		<dsp:oparam name="output">
			<dsp:droplet name="TargetingFirst">
				<dsp:param name="targeter" bean="${atgTargeterpath4}" />
				<dsp:oparam name="output">
					<dsp:valueof param="element.data" valueishtml="true" />
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>

</dsp:page>
