<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" /> 
<dsp:page>
<dsp:importbean bean="/atg/multisite/Site"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:getvalueof bean="Profile.email" var="email"/>
<dsp:getvalueof bean="Site.editEmailPrefURL" var="editPrefURL"/>
<dsp:getvalueof bean="Site.myAccountWishlistUrl" var="wishlistUrl"/>
<dsp:getvalueof bean="Site.myAccountRegistryUrl" var="registryUrl"/>
<div class="my-account-features-zone">
	<div class="row">
		<div class="col-md-12">
			<h2 class="custmHeading">
				communication preferences
			</h2>
			<p>
				<fmt:message key="myaccount.accountFeature.email.message" />
				<dsp:getvalueof var="emailParam" value="?e="/>
				<a
					href="${editPrefURL}${emailParam}${email}"
					class="" target="_blank"><fmt:message key="myaccount.accountFeature.email.emailPreferences" /></a>
			</p>
		</div>
	</div>
	<hr>	
	<div class="row">
		<div class="col-md-12">
			<h2 class="custmHeading">
				wish list
			</h2>
			<p>
			<fmt:message key="myaccount.accountFeature.wishList.message" />
				<a
					href="${wishlistUrl}"
					class="" target="_blank"><fmt:message key="myaccount.accountFeature.wishList.createWishList"/></a>
			</p>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<h2 class="custmHeading">
				my registry
			</h2>
			<p>
			<fmt:message key="myaccount.accountFeature.registry.message"/>
				<a
					href="${registryUrl}"
					class="" target="_blank"><fmt:message key="myaccount.accountFeature.registry.startRegistry" /></a>
			</p>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<h2 class="custmHeading">
				my layaway orders
			</h2>
			<p>
				The Toys"R"Us Layaway Program lets you choose items in-store and
				make payments online or during your next store visit. <a
					href="layaway/myAccountLayaway.jsp"
					class="">find my layaway order</a>
			</p>
		</div>
	</div>
</div>
</dsp:page>