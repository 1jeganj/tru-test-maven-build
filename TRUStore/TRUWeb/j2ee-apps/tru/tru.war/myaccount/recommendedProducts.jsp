<dsp:page>
<!-- Certona parameter Declaration Start -->
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
	<dsp:importbean bean="/com/tru/common/TRUIntegrationConfiguration" />
    <dsp:getvalueof var="customerId" bean="Profile.id"/>
  	<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
  	<dsp:getvalueof var="toysSiteCode" bean="TRUTealiumConfiguration.toysSiteCode"/>
  	<dsp:getvalueof var="storeCertona" bean="TRUIntegrationConfiguration.enableCertona"/>
  	<dsp:getvalueof var="siteCode" value="TRU"/>
<!-- Certona parameter Declaration End -->

	<c:set var="certona" value="${storeCertona}" scope="request"/>
<!-- <div class="my-account-product-carousel"> -->
   	<!-- Certona related Divs Start --> 
   	<c:if test="${certona eq 'true'}">
  		<c:choose>  		
		<c:when test="${site eq toysSiteCode}">
			<div class="full_width_gray">
				<div id="tmyaccountlog_rr">  
					<!-- Toysrus account logged in page recommendations appear here  -->
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div class="full_width_gray">
				<div id="bmyaccountlog_rr"> 
				 	<!-- Babiesrus account logged in page recommendations appear here -->
				</div>
			</div>
			<dsp:getvalueof var="siteCode" value="BRU" />	
		</c:otherwise>		
		</c:choose>	
		<div id="rdata" style="visibility:hidden;"> 
			<div id="site">${siteCode}</div>
			<div id="customerid">${customerId}</div>
		</div>
	</c:if>
	<!-- Certona related Data End -->		
<!-- </div> -->
</dsp:page>