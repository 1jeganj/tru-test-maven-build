/* Start: CMS h1 Header Positioning */
var setH1HeaderPosition = function () {

	try {

		var titleSpan = $('#headerpos');
		var cmsHeaderZIndex = 99, cmsHeaderColor = '#004EBC';

		if (titleSpan.length > 0) {
			cmsHeaderColor = titleSpan.data('color') || '#004EBC';
			cmsHeaderZIndex = (titleSpan.data('zindex') && parseInt(titleSpan.data('zindex')) > 0) ? titleSpan.data('zindex') : 99;

			titleSpan.remove();
		}

		var h1Content = ($('.title-content').length > 0) ? ($('.title-content').text().substring(0, 50)) : 'Heading';

		$('<h1/>', {
			'id': 'cms-title',
			'text': h1Content,
			'style': 'z-index: ' + cmsHeaderZIndex + '; color: ' + cmsHeaderColor
		}).insertAfter($('.category-hero-zone-container').parent());

		$('.title-content').remove();

	} catch (e) { console.log('CMS H1 Error: ' + e); }

};
/* End: CMS h1 Header Positioning */