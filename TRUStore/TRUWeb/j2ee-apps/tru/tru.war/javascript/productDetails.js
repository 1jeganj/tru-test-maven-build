var minSalePrice, maxSalePrice;
var addToCartCloseFlag = false;
var addToCartStateFlag = false;
var isButtonSwap = false;
var selectedSize = false;
var selectedColor = false;

$(document).ready(function () {
	collectionImageGallery();
	enableWarnRegistryPopover();
	formatProductAdditionalInfo();
	/*if($('.product-details-template').length > 0){
		//thumbnailCarousel();
	}*/
	$(document).on("click", '.video-label-finder img:first', function () {
		$("body").find(".pdp-image-gallert-overlay .zoomimage").hide();
		$("body").find(".pdp-image-gallert-overlay #invodoGalleryVideo").show();
		$("body").find(".pdp-image-gallert-overlay #invodoGalleryVideo").removeClass('hide');
		$("body").find(".pdp-image-gallert-overlay").find(".pdpimages").each(function () {
			if ($(this).hasClass("gallery-image-selected")) {
				$(this).removeClass("gallery-image-selected").addClass("gallery-image-unselected");
			}
		});
	});
	/* zoom new functionality */
	if ($('.pageName').val() == 'pdpPage') {
		$('#PDPCarousel').carousel({
			interval: false
		});
		//PDPZoom();
		$(document).on("click", "[id^='carousel-selector-']", function () {
			//$(".promo-with-img-container #hero-image1").fadeOut(0);
			var id = this.id.substr(this.id.lastIndexOf("-") + 1);
			id = parseInt(id);
			if (!$("div#invodoModalButton").hasClass("refresh")) {
				var attrs = {};
				$.each($("div#invodoModalButton")[0].attributes, function (idx, attr) {
					attrs[attr.nodeName] = attr.nodeValue;
				});

				$("div#invodoModalButton").replaceWith(function () {
					return $("<div />", attrs).addClass("refresh").append($(this).contents());
				});
			}
			var imageUrl = $('img', this).attr('src');
			if (imageUrl != "") {
				var imageUrlZoom = imageUrl.split("|");
				imageUrlZoom = (imageUrlZoom[0] + "|" + 960 + ":" + 960);
				//console.log("zoom URL :",imageUrlZoom);
				//$('#hero-image1').attr('src',imageUrlZoom); 
			}
			$(".block-add").removeClass("selectedImage");
			$('#stylized_thumbnail_container .block-add#carousel-selector-' + id).addClass("selectedImage");
			$('#PDPCarousel').carousel(id);
			$('#PDPCarousel').carousel({
				interval: false
			});
			// $('#PDPCarousel [data-slide-number='+id+'] img').fadeIn().css({opacity :"1",display:'block'});
		});

		$(".promo-with-img-container").hover(function () {
			if ($(".promo-with-img-container").find("#parentWrapper.active").length > 0) {
				$(".promo-with-img-container").find(".our-exclusive").css({ "display": "none" });
			} else {
				$(this).addClass("zooom");
				$(this).find(".our-exclusive").css({ "display": "none" });
				$(this).find(".PDP-zoom-icon").css({ "display": "none" });
			}
		}, function () {
			if ($(".promo-with-img-container").find("#parentWrapper.active").length > 0) {
				$(".promo-with-img-container").find(".our-exclusive").css({ "display": "none" });
			} else {
				$(this).removeClass("zooom");
				$(this).find(".our-exclusive").css({ "display": "block", "z-index": 100 });
				$(this).find(".PDP-zoom-icon").css({ "display": "block", "z-index": 100 });
			}
		});
	}
	/* end of zoom functionality */
	/* if color is not available for an item, in chrome not available image displaying border which looks odd so if there is
	   no image available we are loading another image */
	$(".color-swatches-block .color-block-inner").each(function (index, node) {
		if ($(node).attr('src') == "")
			$(node).attr('src', '../images/color-img-not-available.jpg');
	});
	$(document).on("click", ".label-container", function (e) {
		e.stopImmediatePropagation();
		var $parent = $(this).parents(".sticky-price-contents");
		$parent.find(".sizes").slideToggle('fast', function () {
			$parent.find(".label-container").toggleClass("swatches-opened");
		});
	});
	$(document).on("click", ".size-swatches-container .swatches", function () {
		var $parent = $(this).parents(".sticky-price-contents");
		$parent.find(".size-swatches-container .swatches").removeClass("selected-swatch");
		$(this).addClass("selected-swatch");
		var selectedText = $(this).text();
		$parent.find(".spark-size-label .selected-swatch-label").text(selectedText);
	});

	$("#galleryOverlayModal").on("hidden.bs.modal", function () {
		$("body").find(".pdp-image-gallert-overlay .zoomimage").show();
		$("body").find(".pdp-image-gallert-overlay").find("#invodoGalleryVideo").hide();
		$('#galleryOverlayModal .gallery-lr').show();
	});

	$("#galleryOverlayModal").on("shown.bs.modal", function () {
		/*if(($("body").find(".pdp-image-gallert-overlay").find("#invodoVideoButton").length)?$("body").find(".pdp-image-gallert-overlay").find("#invodoVideoButton").html().length:false || $("body").find(".pdp-image-gallert-overlay").find(".product-tour-container").length){
			$(".gallerypopup-vedio").show();
		}*/
		/*if($('.customerImages .prImageSnippetImage').length>0){
			$('.customerImages .prImageSnippetImage a').removeAttr('onclick');
		}*/

		// $('.customerImages .prImageSnippetImage .gallery-image-selected').length>0

		var invodoVideoButton = $('.video-label-finder img, .video-label-finder a').length;

		if (invodoVideoButton > 0  /*|| productTourContainer > 0 || wcMiniSiteButton > 0*/) {
			$('.gallery-popup-vedio').show();
		}

		$('.customerImages img').addClass('gallery-image-unselected');
	});
	$(document).on('shown.bs.modal', '#collectionGalleryOverlayModal', function () {
		var modal = $('#collectionGalleryOverlayModal')
		modal.find('.wrapper').TrackpadScrollEmulator();
	});
	$(document).on('shown.bs.modal', '#detailsModel', function () {
		var modal = $('#detailsModel');
		if (modal.find('.tse-scroll-content').length > 0) {
			modal.find('.wrapper').TrackpadScrollEmulator('recalculate');
		}
		else { modal.find('.wrapper').TrackpadScrollEmulator(); }
	});
	$(document).on('hidden.bs.modal', '#detailsModel', function () {
		$('#detailsModel .tse-scroll-content').removeAttr("style");
	});

	$(document).on('hidden.bs.modal', '#findInStoreModal', function () {
		$(document).find("#add-to-button").find("a").attr("disabled", "disabled");
		/* Start: Fix for GEOFUS-1033 */
		var pageNameForUtag = $('.pageName').val(); 
		if( pageNameForUtag == 'pdpPage' && typeof utag_data != 'undefined'){
			var strikedPricePageTitleHidden = $('.strikedPricePageTitleHidden').val() || '';
			utag_data.striked_price_page_title = strikedPricePageTitleHidden;
		}
		/* End: Fix for GEOFUS-1033 */
	});
	/* Start: Fix for GEOFUS-1033 */
	$(document).on("shown.bs.modal", "#findInStoreModal", function () {
		var pageNameForUtag = $('.pageName').val(); 
		if(pageNameForUtag == 'pdpPage' && typeof utag_data != 'undefined' && typeof utag_data.striked_price_page_title != 'undefined'){
			delete utag_data.striked_price_page_title;
		}
	});
	/* End: Fix for GEOFUS-1033 */
	
	//Implementation to refresh the price in related products section
	$(document).on("hidden.bs.modal", "#shoppingCartModal", function () {
		var cartCookie = getCookie("cartCookie");
		cartCookie = cartCookie.split("~");
		var i = 0, cartCookieLength = cartCookie.length;
		var cartSkuList = [];
		var productIdList = [];
		for (i = 0; i < cartCookieLength; i++) {
			cartSkuList.push(cartCookie[i].split("|")[0]);
			productIdList.push(cartCookie[i].split("|")[1]);
		}
		$("#related-products").find(".product-block-information").each(function () {
			var classList = this.className.split(/\s+/);
			if (cartSkuList.indexOf(classList[1].split("-")[1]) > -1) {
				var skuId = cartSkuList[cartSkuList.indexOf(classList[1].split("-")[1])];
				var productId = productIdList[cartSkuList.indexOf(classList[1].split("-")[1])];
				$.ajax({
					type: 'POST',
					url: contextPath + 'jstemplate/intermediateRelatedPrice.jsp?skuId=' + skuId + '&productId=' + productId + '&page=fromRelated',
					success: function (data) {
						$("." + classList[1]).find(".product-block-price").html(data);
					}
				});
			}
		});
		if ($(".compare-overlay-scroll:visible").length > 0) {
			var $compareSectionElements = $(".compare-overlay-menu .compare-overlay-locked .compare-display-image").find(".col-md-2");
			$compareSectionElements.each(function () {
				var itemSkuId = $(this).find('input').val().split("-")[1];
				if (cartSkuList.indexOf(itemSkuId) > -1) {
					var skuId = itemSkuId;
					var productId = $(this).find('input').val().split("-")[0];
					var index = $(this).index() - 2;
					$.ajax({
						type: 'POST',
						url: contextPath + 'jstemplate/intermediateRelatedPrice.jsp?skuId=' + skuId + '&productId=' + productId + '&page=fromCompare',
						success: function (data) {
							//$("."+classList[1]).find(".product-block-price").html(data);
							var $priceElement = $(".compare-overlay-scroll:visible .price-compare-block .col-md-2:eq(" + index + ")");
							$priceElement.html('');
							//var prices = '<span class="crossed-out-price">'+$(data).find(".strike-out").html()+'</span><span class="sale-price">'+$(data).find(".product-block-sale-price").html()+'</span>';
							//var prices = '<span class="crossed-out-price">'+$(data).find(".strike-out").html()+'</span><span class="sale-price">'+$(data).find(".product-block-sale-price").html()+'</span>';
							$priceElement.html(data);

						}
					});
				}
			});
		}
	});

	if ($('.color-swatches-section .color-block').length < 7) {
		$('.view-more-color-section').hide();
	}
	$(document).on('click', '.view-more-color-swatch-link', function () {
		$('.color-swatches-block').animate({}, 'slow');
		var swatchSectionHeight = Math.ceil($('.color-swatches-section .color-block').length / 6) * 53;
		$('.color-swatches-section').animate({ 'height': swatchSectionHeight + 'px' }, 'fast', function () {
			$('.view-more-color-swatch-link').addClass('view-less').text('view less swatches');
		});
	});

	$(document).on('click', '.view-more-color-swatch-link.view-less', function () {
		$('.color-swatches-block').animate({}, 'slow');
		$('.color-swatches-section').animate({ 'height': '55px' }, 'fast', function () {
			$('.view-more-color-swatch-link').removeClass('view-less').text('view more swatches');
		});
	});

});

/* invodo code adding which should be triggered when click on play button of the player */
$('body').on('click', '#slider #embeddedContainer .invodo-player .invodo-play--btn', function (e) {
	utag.link({
		event_type: 'video_init'
	});
});
/* end invodo code adding which should be triggered when click on play button of the player */

/* Adding invado video tag into the PDP carousel */
var invodoVideo = function () {
	var invodoThumb = $("#stylized_thumbnail_container").find("#invodoModalButton");
	var elem = $('#PDPCarousel .carousel-inner').find('.item');
	var invadoContainer = '<div id="parentWrapper" style=width:100%; max-width:780px; min-width:360px; margin: 0 auto;"><div id="embeddedContainer"></div></div>';
	if ($(elem).length > 0) {
		if ($(invadoContainer).insertAfter($(elem)[$(elem).length - 1])) {
			$('#PDPCarousel .carousel-inner').find('#parentWrapper').addClass("item");
		}
	}
}

/* Adding class selected to invodo thumb for highlighting it when its clicked  */
$('body').on('click', '#stylized_thumbnail_container a.block-add', '#stylized_thumbnail_container #invodoModalButton img', function (e) {
	var targetVideoThumbElem = $('.invodoThumbContainer').find('#invodoModalButton');
	if ((targetVideoThumbElem.children()[0] === e.target) || (targetVideoThumbElem[0] === e.target)) {
		if ($(".promo-with-img-container").find(".our-exclusive").length > 0) {
			$(".promo-with-img-container").find(".our-exclusive").css({ "display": "none" });
		}

		setTimeout(function () {
			if ($(targetVideoThumbElem).hasClass("selectedImage")) {
				$(targetVideoThumbElem).parent().addClass("selectedImage");
			}
		}, 100)
	}
	else {
		$(targetVideoThumbElem).parent().removeClass("selectedImage");
		if ($(".promo-with-img-container").find(".our-exclusive").length > 0) {
			$(".promo-with-img-container").find(".our-exclusive").css({ "display": "block" });
		}
		// pausing the video when not active 
		if($('#stylized_thumbnail_container #invodoModalButton').find('img').length > 0){
			var invodoVideoTag = $("#slider #parentWrapper #embeddedContainer").find("video");
			if ($((invodoVideoTag).length > 0) && $(invodoVideoTag)[0].paused === false) {
				$(invodoVideoTag)[0].pause();
			}
		}
	}
});

function thumbnailCarousel() {
	var $thumbs = $("#stylized_thumbnail_container .block-add");
	var thumbsLength = $thumbs.length;

	var skuDisplayName = '';
	if ($('.product-details-template').length) {
		skuDisplayName = $('#rgs_skuDisplayName').val() || '';
	} else if ($('.collection-template').length) {
		skuDisplayName = $('.collectionDisplayName').val() || '';
	}
	if (typeof skuDisplayName != 'undefined' && skuDisplayName != '') {
		skuDisplayName = isCanonicalUrlHavingSpecChar(skuDisplayName);
	}

	if ($('.product-details-template').length > 0 && thumbsLength > 0) {
		$("#PDPCarousel .item").remove();
		for (var i = 0; i < thumbsLength; i++) {
			$($thumbs[i]).attr("id", "carousel-selector-" + i);
			var imageUrl = $($thumbs[i]).find('img').attr('src');
			//console.log("Thumb image URL ", imageUrl);
			if (typeof imageUrl != 'undefined' && imageUrl != "") {//if( imageUrl != "" && i!=0){
				//imageUrl=isCanonicalUrlHavingSpecChar(imageUrl);
				var ZoomImageUrl = imageUrl.split("|");
				var zoomImageUrl = ZoomImageUrl[0] + "|" + 480 + ":" + 480;
				var imageUrlZoom = ZoomImageUrl[0] + "|" + 960 + ":" + 960;
				//console.log("zoooooom image : ",imageUrlZoom);
				//$(".promo-with-img-container .carousel-inner").append('<div class="item" data-slide-number='+i+'><img src='+ZoomImageUrl+'></div>');
				$(".promo-with-img-container .carousel-inner").append('<div class="item" data-slide-number=' + i + '><div class="zoomContainer"><img class="zoomImage" src=' + zoomImageUrl + ' alt="' + skuDisplayName + '"><img class="largeImage" src=' + imageUrlZoom + ' alt="' + skuDisplayName + '"></div></div>');
			}
		}
		invodoVideo();
		$(".promo-with-img-container .carousel-inner .item:first").addClass("active");
	}
}
/*End Thumbnail carousel Implementation*/

$(document).on('click', '[data-target="#galleryOverlayModal"]', function () {
	if ($('.product-details-template').length > 0 || $(".collection-template").length > 0) {
		galleryOverlayModalArrowShowHide();
	}
});

$(document).on('click', '.battery-Items', function () {
	$.smoothScroll({
		scrollTarget: '.main-sec',
		offset: -98
	});
	return false;
});

function galleryOverlayModalArrowShowHide() {
	var $galleryOverlayModalImageViewer = $('#galleryOverlayModal .modal-dialog .gallery-image-viewer');
	if ($('.videosclass img').length > 0 && $('.productImages .pdpimages').length <= 0) {
		$galleryOverlayModalImageViewer.find('.gallery-lr').css({ display: "none" });
	}
	else if (($('.videosclass img').length <= 0) && ($('.productImages .pdpimages').length <= 1)) {
		$galleryOverlayModalImageViewer.find('.gallery-lr').css({ display: "none" });
	}
	else if (($('.videosclass img').length > 0) && ($('.productImages .pdpimages').length == 1)) {//&& ($('.customerImages .prImageSnippetImage').length <= 0)
		$('#galleryOverlayModal .modal-dialog .gallery-image-viewer').find('.gallery-lr.right').css({ display: "none" });
	}
	else if ($('.productImages .pdpimages:first').hasClass('gallery-image-selected') && ($('.videosclass img').length <= 0)) {
		$('#galleryOverlayModal .modal-dialog .gallery-image-viewer').find('.gallery-lr.left').css({ display: "none" });
	}
}

var replaceReviewStars = function (eachReveiw) { /* In each review section rating star image, to replace with our star image */
	if (typeof eachReveiw != 'undefined' && eachReveiw.length > 0) {
		$.each(eachReveiw, function (rKey, rVal) {
			var ratingAvgVal = $(rVal).find('.pr-snippet-rating-decimal').text() || 0;
			ratingAvgVal = Math.round(ratingAvgVal);
			var ratingPosition = (ratingAvgVal * -32) + 'px';
			$(rVal).find('.pr-rating-stars').css({ 'background-position-y': ratingPosition, visibility: 'visible' });
		});
	}
}

var onLoadFunction = function () {

	var pathname = window.location.pathname; // Returns path only
	if (getParameterByName('productId') || getParameterByName('collectionId')) {
		var productId = getProductIds("");
		if (productId != "" && productId != null && productId != undefined) {
			var productInfoJsonObj = $('.productInfoJson-' + productId).val() || '';
			if (productInfoJsonObj != '') {
				ajaxCallForInventoryAndPrice();
				notCachedRegistryButtons('');
				backToWishListOrRegistryButtons();
				/* if($('.sticky-price').length >0){*/
				notCachedPriceAndImage('', 'imageNeedChange');
				/* }*/
				if ($('.sticky-price').length > 0 || $('.collection-template').length > 0) {
					notCachedAddTocartButton('');
					/*populateVendorDemoUrlFromJSON();*/
				}
				refreshPDPAdditionalInfo();
				if (getParameterByName('collectionId') || $('.collection-template').length > 0) {
					refreshCollectionQuantityTextBox();
				}
				var hookLogicEnabledFlag = $('#hookLogicEnabledFlag').val();
				if (hookLogicEnabledFlag == 'true' && $(".product-details-template").length) {
					hooklogicPdp();
				}
				bruCss();
				invokeAkamaiURL();
				// populateRegistryCookieDetails(productId);
			}
		}
	}
	clearInput();
	displayTooltipMessageOnAddtoCartButton();
}

function populateRegistryCookieDetails(productId) {
	var pageName = $('.pageName').val();
	if (typeof productId === "undefined" || productId == '' || pageName != 'pdpPage') {
		return '';
	}
	if (pageName == 'pdpPage') {
		var isWishlist = false;
		var currentDomain = window.location.host;
		currentDomain = (currentDomain.split(".").length > 2) ? "." + currentDomain.split(".")[1] + "." + currentDomain.split(".")[2] : "." + currentDomain;
		var productDetails = generateAjaxProductDetailsByProductId(productId, isWishlist);
		document.cookie = "addToRegistryCookie=" + productDetails + "; path=/;domain=" + currentDomain;
		invokeAkamaiURL();
	}
}

$(window).load(onLoadFunction);

$(document).ready(function () {
	if (getParameterByName('productId') || getParameterByName('collectionId')) {
		notCachedRegistryButtons('');
		invokeAkamaiURL();
		var pageParam = getParameterByName("cat");
		if (typeof pageParam != 'undefined' && pageParam != "" && parseInt(pageParam) == 1) {
			//ajaxCallForPdpBreadcrumbsCookie();
		}
	}
});

function checkNumberOfImages() {
	var imageCount = $("body").find(".gallery-image-unselected.pdpimages").length + 1 + $("body").find(".prImageSnippetImage").length;
	/*if($("#invodoModalButton").html().trim().length != 0){
		imageCount++;
	}*/
	// <SPARK UI change >
	/*	if(imageCount > 3){
			$("body").find("#stylized_thumbnail_container .see-more-images").css("display","inline-block");
		}*/
	// </SPARK UI change >
}

/*function populateVendorDemoUrlFromJSON(){
	var productId = getProductIds("");
	 if(productId!="" && productId != null && productId != undefined){
			var productInfoJson=JSON.parse($('.productInfoJson-'+productId).val());
			if(productInfoJson != null){
				if(productInfoJson.hasOwnProperty("mDefaultSKU")){
						populateVendorDemoUrl(productInfoJson.mDefaultSKU)
				}
			}
	 }
}*/

function refreshPDPAdditionalInfo() {
	if ($('.sticky-price-contents').length == 1) {
		var productId = getProductIds("");
		var productInfoJson = JSON.parse($('.productInfoJson-' + productId).val());

		// Start -- fix GEOFUS-1841 -- //
		if (productInfoJson != null && productInfoJson.mDefaultSKU.mInventoryStatus === "outOfStock") {
			var htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart">out of stock</button></div>';
			$('#addToCartDivSpace').html(htmlData);
		} 
		// End -- fix GEOFUS-1841 -- //

		if (productInfoJson != null) {	
			if (productInfoJson.hasOwnProperty("mDefaultSKU") && productInfoJson.hasOwnProperty("mColorSizeVariantsAvailableStatus")) {
				if ((productInfoJson.mColorSizeVariantsAvailableStatus == "onlyColorVariants" || productInfoJson.mColorSizeVariantsAvailableStatus == "onlySizeVariants" || productInfoJson.mColorSizeVariantsAvailableStatus == "bothColorSizeAvailable")) {
					if (productInfoJson.mDefaultSKU.hasOwnProperty("mId")) {
						$(".additionalInfoUIDPlaceHolder").text(productInfoJson.mDefaultSKU.mId);
					}
					if (productInfoJson.mDefaultSKU.hasOwnProperty("mSkuUpcNumbers")) {
						$(".additionalInfoUPCNumbersPlaceHolder").text(productInfoJson.mDefaultSKU.mSkuUpcNumbers);
					} else {
						$(".additionalInfoUPCNumbersPlaceHolder").parent("li").hide();
					}
				}
			}
		}
		if (productInfoJson != null && productInfoJson != '' && productInfoJson.hasOwnProperty("mDefaultSKU") && productInfoJson.mDefaultSKU.hasOwnProperty("mBatteryInfoJsonVO")) {
			var productBatteryInfoJson = productInfoJson.mDefaultSKU.mBatteryInfoJsonVO;
			if (productBatteryInfoJson != null && productBatteryInfoJson != '') {
				refreshBatteryCrossSellsection(productBatteryInfoJson, productId);
			}
		}
	}

}

function refreshBatteryCrossSellsection(productBatteryInfoJson, productId) {
	var i = 0, productLength = productBatteryInfoJson.length;
	var myStoreCookie = $.cookie("favStore");
	var isRegistry = getParameterByName("isRegistry");
	var locationIdFromCookie = '';
	var presellQuantityUnits = '';
	var addToCartMessage = (typeof addToCartMessage != 'undefined' && addToCartMessage != '') ? addToCartMessage : 'add to cart';
	//var siteId=batterySiteID;
	if (typeof myStoreCookie != 'undefined' && myStoreCookie != '' && myStoreCookie != null) {
		locationIdFromCookie = myStoreCookie.split('|')[1];
	}
	if (productLength) {
		for (; i < productLength; i++) {
			var presellable = (typeof productBatteryInfoJson[i].mPresellable != "undefined") ? productBatteryInfoJson[i].mPresellable : false;
			var channelAvailability = productBatteryInfoJson[i].mChannelAvailability;
			if (channelAvailability == undefined || channelAvailability == '' || channelAvailability == null || channelAvailability === 'N/A') {
				channelAvailability = 'both';
			}
			if (!presellable && channelAvailability == 'In Store Only' && (locationIdFromCookie === '')) {
				if (!(productBatteryInfoJson[i].mS2s === 'N' && productBatteryInfoJson[i].mIspu === 'N')) {
					var addToCartFunction = "javascript:return loadFindInStore('" + contextPath + "','" + productBatteryInfoJson[i].mSkuId + "','',$('.QTY-" + productBatteryInfoJson[i].mProductId + "').val());";
					var onlineInventoryStatus = 'onlineInventoryStatus-' + productBatteryInfoJson[i].mSkuId;
					var htmlData = '<input id=' + onlineInventoryStatus + ' type="hidden" value="' + productBatteryInfoJson[i].mInventoryStatus + '"><button class="add-to-cart-new" data-toggle="modal" data-target="#findInStoreModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button>';
					$(".content-block.batterySku-" + productBatteryInfoJson[i].mSkuId).find(".button-part.batteryItemAddtoCart button").replaceWith(htmlData);
				} else {
					htmlData = '<div class="button-part"><button disabled="disabled" class="add-to-cart-new fade-add-to-cart">' + addToCartMessage + '</button></div>';
					$(".content-block.batterySku-" + productBatteryInfoJson[i].mSkuId).find(".button-part.batteryItemAddtoCart button").replaceWith(htmlData);
				}
			} else if (!presellable && channelAvailability == 'In Store Only' && (locationIdFromCookie != '')) {
				if (productBatteryInfoJson[i].mInventoryStatusInStore == 'inStock' && !(productBatteryInfoJson[i].mS2s === 'N' && productBatteryInfoJson[i].mIspu === 'N')) {
					var addToCartFunction = "javascript: return addItemToCart('" + productBatteryInfoJson[i].mProductId + "','" + productBatteryInfoJson[i].mSkuId + "','" + locationIdFromCookie + "','" + contextPath + "','this','fromPDPBatteries');"
					var onlineInventoryStatus = 'onlineInventoryStatus-' + productBatteryInfoJson[i].mSkuId;
					var htmlData = '<input id=' + onlineInventoryStatus + ' type="hidden" value="' + productBatteryInfoJson[i].mInventoryStatus + '"><button class="add-to-cart-new" data-toggle="modal" data-target="#shoppingCartModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button>';
					$(".content-block.batterySku-" + productBatteryInfoJson[i].mSkuId).find(".button-part.batteryItemAddtoCart button").replaceWith(htmlData);
				} else {
					htmlData = '<div class="button-part"><button disabled="disabled" class="add-to-cart-new fade-add-to-cart">' + addToCartMessage + '</button></div>';
					$(".content-block.batterySku-" + productBatteryInfoJson[i].mSkuId).find(".button-part.batteryItemAddtoCart button").replaceWith(htmlData);

				}
			} else if (!presellable && productBatteryInfoJson[i].mNotifyMe === 'Y' && !(isRegistry) && ((productBatteryInfoJson[i].mInventoryStatus === 'outOfStock' && productBatteryInfoJson[i].mS2s === 'N' && productBatteryInfoJson[i].mIspu === 'N' && locationIdFromCookie === '')
				|| (locationIdFromCookie != '' && productBatteryInfoJson[i].mInventoryStatus === 'outOfStock' && productBatteryInfoJson[i].mS2s === 'N' && productBatteryInfoJson[i].mIspu === 'N')
				|| (locationIdFromCookie != '' && productBatteryInfoJson[i].mInventoryStatus === 'outOfStock' && productBatteryInfoJson[i].mInventoryStatusInStore === 'outOfStock'))) {
				var htmlData = '<button class="add-to-cart-new email-me-when-available" data-toggle="modal" data-target="#notifyMeModal">email me when available</button>';
				$(".content-block.batterySku-" + productBatteryInfoJson[i].mSkuId).find(".button-part.batteryItemAddtoCart button").replaceWith(htmlData);
				$($(".content-block.batterySku-" + productBatteryInfoJson[i].mSkuId).find(".button-part.batteryItemAddtoCart")[i]).append("<div class='battery-out-of-stock'>out of stock</div>");
				$(".content-block.batterySku-" + productBatteryInfoJson[i].mSkuId).find(".button-part.batteryItemAddtoCart")[0].addEventListener("click", function () {
					loadNotifyMe(this, 'fromPDPBatteries', "");
				});
			} else if (!presellable && productBatteryInfoJson[i].mInventoryStatus === 'outOfStock' && productBatteryInfoJson[i].mNotifyMe === 'N' && ((locationIdFromCookie === '' && productBatteryInfoJson[i].mS2s === 'N' && productBatteryInfoJson[i].mIspu === 'N')
				|| (locationIdFromCookie != '' && productBatteryInfoJson[i].mInventoryStatus === 'outOfStock' && productBatteryInfoJson[i].mS2s === 'N' && productBatteryInfoJson[i].mIspu === 'N')
				|| (locationIdFromCookie != '' && productBatteryInfoJson[i].mInventoryStatus === 'outOfStock' && productBatteryInfoJson[i].mInventoryStatusInStore === 'outOfStock'))) {
				htmlData = '<div class="button-part"><button disabled="disabled" class="add-to-cart-new fade-add-to-cart">' + addToCartMessage + '</button></div>';
				$(".content-block.batterySku-" + productBatteryInfoJson[i].mSkuId).find(".button-part.batteryItemAddtoCart button").replaceWith(htmlData);
			}
			else if ((!presellable && productBatteryInfoJson[i].mInventoryStatus === 'outOfStock' && !(productBatteryInfoJson[i].mS2s === 'N' && productBatteryInfoJson[i].mIspu === 'N') && locationIdFromCookie === '')) {
				var addToCartFunction = "javascript:return loadFindInStore('" + contextPath + "','" + productBatteryInfoJson[i].mSkuId + "','',$('.QTY-" + productBatteryInfoJson[i].mProductId + "').val());";
				var onlineInventoryStatus = 'onlineInventoryStatus-' + productBatteryInfoJson[i].mSkuId;
				var htmlData = '<input id=' + onlineInventoryStatus + ' type="hidden" value="' + productBatteryInfoJson[i].mInventoryStatus + '"><button class="add-to-cart-new" data-toggle="modal" data-target="#findInStoreModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button>';
				$(".content-block.batterySku-" + productBatteryInfoJson[i].mSkuId).find(".button-part.batteryItemAddtoCart button").replaceWith(htmlData);
			}
			else if (!presellable && (locationIdFromCookie != '' && (productBatteryInfoJson[i].mInventoryStatus != 'outOfStock' || productBatteryInfoJson[i].mInventoryStatusInStore != 'outOfStock') && !(productBatteryInfoJson[i].mS2s === 'N' && productBatteryInfoJson[i].mIspu === 'N'))) {
				var addToCartFunction = "javascript: return addItemToCart('" + productBatteryInfoJson[i].mProductId + "','" + productBatteryInfoJson[i].mSkuId + "','" + locationIdFromCookie + "','" + contextPath + "','this','fromPDPBatteries');"
				var onlineInventoryStatus = 'onlineInventoryStatus-' + productBatteryInfoJson[i].mSkuId;
				var htmlData = '<input id=' + onlineInventoryStatus + ' type="hidden" value="' + productBatteryInfoJson[i].mInventoryStatus + '"><button class="add-to-cart-new" data-toggle="modal" data-target="#shoppingCartModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button>';
				$(".content-block.batterySku-" + productBatteryInfoJson[i].mSkuId).find(".button-part.batteryItemAddtoCart button").replaceWith(htmlData);

			} else if (locationIdFromCookie != '' && productBatteryInfoJson[i].mInventoryStatus != 'outOfStock' && productBatteryInfoJson[i].mS2s === 'N' && productBatteryInfoJson[i].mIspu === 'N' && !presellable) {
				var addToCartFunction = "javascript: return addItemToCart('" + productBatteryInfoJson[i].mProductId + "','" + productBatteryInfoJson[i].mSkuId + "','" + locationIdFromCookie + "','" + contextPath + "','this','fromPDPBatteries');"
				var onlineInventoryStatus = 'onlineInventoryStatus-' + productBatteryInfoJson[i].mSkuId;
				var htmlData = '<input id=' + onlineInventoryStatus + ' type="hidden" value="' + productBatteryInfoJson[i].mInventoryStatus + '"><button class="add-to-cart-new" data-toggle="modal" data-target="#shoppingCartModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button>';
				$(".content-block.batterySku-" + productBatteryInfoJson[i].mSkuId).find(".button-part.batteryItemAddtoCart button").replaceWith(htmlData);
			}
			else if (!productBatteryInfoJson[i].mUnCartable && presellable && productBatteryInfoJson[i].mInventoryStatus != 'outOfStock') {
				var addToCartFunction = "javascript: return addItemToCart('" + productBatteryInfoJson[i].mProductId + "','" + productBatteryInfoJson[i].mSkuId + "','" + locationIdFromCookie + "','" + contextPath + "','this','fromPDPBatteries');"
				var onlineInventoryStatus = 'onlineInventoryStatus-' + productBatteryInfoJson[i].mSkuId;
				var htmlData = '<input id=' + onlineInventoryStatus + ' type="hidden" value="' + productBatteryInfoJson[i].mInventoryStatus + '"><button class="add-to-cart-new" data-toggle="modal" data-target="#shoppingCartModal" onclick="' + addToCartFunction + '">' + preOrderAddToCartMessage + '</button>';
				$(".content-block.batterySku-" + productBatteryInfoJson[i].mSkuId).find(".button-part.batteryItemAddtoCart button").replaceWith(htmlData);
			}
			else if ((!productBatteryInfoJson[i].mUnCartable && presellable && productBatteryInfoJson[i].mPresellQuantityUnits === '0') || (productBatteryInfoJson[i].mInventoryStatus === 'outOfStock' && presellable)) {
				htmlData = '<div class="button-part"><button disabled="disabled" class="add-to-cart-new fade-add-to-cart">' + preOrderAddToCartMessage + '</button></div>';
				$(".content-block.batterySku-" + productBatteryInfoJson[i].mSkuId).find(".button-part.batteryItemAddtoCart button").replaceWith(htmlData);
			}
			else if (!productBatteryInfoJson[i].mUnCartable && productBatteryInfoJson[i].mInventoryStatus != 'outOfStock') {
				if (!presellable && productBatteryInfoJson[i].mAvailableInventory === 0) {
					htmlData = '<div class="button-part"><button disabled="disabled" class="add-to-cart-new fade-add-to-cart">' + addToCartMessage + '</button></div>';
					$(".content-block.batterySku-" + productBatteryInfoJson[i].mSkuId).find(".button-part.batteryItemAddtoCart button").replaceWith(htmlData);

				} else {
					var addToCartFunction = "javascript: return addItemToCart('" + productBatteryInfoJson[i].mProductId + "','" + productBatteryInfoJson[i].mSkuId + "','" + locationIdFromCookie + "','" + contextPath + "','this','fromPDPBatteries');"
					var onlineInventoryStatus = 'onlineInventoryStatus-' + productBatteryInfoJson[i].mSkuId;
					var htmlData = '<input id=' + onlineInventoryStatus + ' type="hidden" value="' + productBatteryInfoJson[i].mInventoryStatus + '"><button class="add-to-cart-new" data-toggle="modal" data-target="#shoppingCartModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button>';
					$(".content-block.batterySku-" + productBatteryInfoJson[i].mSkuId).find(".button-part.batteryItemAddtoCart button").replaceWith(htmlData);
				}
			}

			if (productBatteryInfoJson[i].mUnCartable) {
				htmlData = '<div class="button-part"><button disabled="disabled" class="add-to-cart-new fade-add-to-cart">' + addToCartMessage + '</button></div>';
				$(".content-block.batterySku-" + productBatteryInfoJson[i].mSkuId).find(".button-part.batteryItemAddtoCart button").replaceWith(htmlData);
			}
		}
	} else {
		$(".main-sec").hide();
	}

}

function refreshCollectionQuantityTextBox() {
	$("[class^='productInfoJson-']").each(function () {
		var productInfoJson = JSON.parse($(this).val());
		if (productInfoJson != null && productInfoJson.hasOwnProperty("mDefaultSKU")) {
			if (productInfoJson.mDefaultSKU.mSkuRegistryEligibility != undefined && productInfoJson.mDefaultSKU.mSkuRegistryEligibility != "" && productInfoJson.mDefaultSKU.mSkuRegistryEligibility != null && productInfoJson.mDefaultSKU.mRegistryWarningIndicator != undefined && productInfoJson.mDefaultSKU.mRegistryWarningIndicator != "" && productInfoJson.mDefaultSKU.mRegistryWarningIndicator != null) {
				var registryWarningIndicator = productInfoJson.mDefaultSKU.mRegistryWarningIndicator;
				var skuRegistryEligibility = productInfoJson.mDefaultSKU.mSkuRegistryEligibility;
				var inventoryStatus = productInfoJson.mDefaultSKU.mInventoryStatus;
				var inventoryStatusForStore = productInfoJson.mDefaultSKU.mInventoryStatusInStore;
				if (((typeof registryWarningIndicator === 'DENY' && skuRegistryEligibility) || (registryWarningIndicator === 'WARN' && !skuRegistryEligibility) || (registryWarningIndicator === 'ALLOW' && !skuRegistryEligibility)) && inventoryStatus == 'outOfStock') {
					if (inventoryStatusForStore) {
						if (inventoryStatusForStore == "outOfStock") {
							$(this).parents(".cp-stock-details").find(".cp-collectionQty .cp-item-qty").removeAttr('enabled').attr('disabled', 'disabled');
							$(this).parents(".cp-stock-details").find(".cp-collectionQty .cp-increase").removeClass('enabled').addClass('disabled');
						}
						else {
							$(this).parents(".cp-stock-details").find(".cp-collectionQty .cp-item-qty").removeAttr('disabled').attr('enabled', 'enabled');
							$(this).parents(".cp-stock-details").find(".cp-collectionQty .cp-increase").removeClass('disabled').addClass('enabled');
						}
					}
					else {
						$(this).parents(".cp-stock-details").find(".cp-collectionQty .cp-item-qty").removeAttr('enabled').attr('disabled', 'disabled');
						$(this).parents(".cp-stock-details").find(".cp-collectionQty .cp-increase").removeClass('enabled').addClass('disabled');
					}

				}
				else {
					$(this).parents(".cp-stock-details").find(".cp-collectionQty .cp-item-qty").removeAttr('disabled').attr('enabled', 'enabled');
					$(this).parents(".cp-stock-details").find(".cp-collectionQty .cp-increase").removeClass('disabled').addClass('enabled');
				}
			}
			/*else{
				$(this).parents(".cp-stock-details").find(".cp-collectionQty .cp-item-qty").removeAttr('disabled').attr('enabled','enabled');
				$(this).parents(".cp-stock-details").find(".cp-collectionQty .cp-increase").removeClass('disabled').addClass('enabled');
			}*/
		}
	})
}

$("body").on("click", ".select_wish_list_button", function () {
	$('#rgs_lastAccessedWishlistID').val($(this).attr('data-registry-number'));
	var pageName = $(".pageName").val();
	if (pageName == 'collectionPage') {
		registryWishListCookie('true', this);
	} else {
		registryWishListCookie('true');
	}
	var wishlistId = $('#rgs_lastAccessedWishlistID').val();
	var customerEmail = $('#customerEmail').val();
	var customerID = $('#customerID').val();
	var partnerName = $('#partnerName').val();
	if (typeof utag != 'undefined') {
		utag.link({
			event_type: 'wishlist_add',
			customer_id: customerID,
			customer_email: customerEmail,
			wishlist_id: wishlistId,
			partner_name: partnerName
		});
	}
});

function hideVideoFromOverlay() {
	$("body").find(".pdp-image-gallert-overlay .zoomimage").show();
	$("body").find(".pdp-image-gallert-overlay").find("#invodoGalleryVideo").hide();
}

/*$(document).on('click', '.shopping_cart_overlay_continue_shopping', function(){
	notCachedPriceAndImage('','');
});
*/
/* START: AJAX call  for getting the real time inventory and price */

function getProductIds(pdpCheckFlag) {
	var productIds = [];
	var contextPath = $('.hiddenContextPath').val();
	var productId = '';
	if ($('#hiddenProductId').val() && !pdpCheckFlag) {
		productId = $('#hiddenProductId').val();
		productIds[0] = productId;
	} else {
		productId = $('#hiddenProductIdQuickView').val();
		productIds[0] = productId;
	}
	if ($('#pageName').val()) {
		if ($('#pageName').val() === 'shoppingCart') {
			productId = $('#hiddenProductIdEditCart').val();
			productIds[0] = productId;
		}
	}
	if (typeof productIds[0] === 'undefined' || productIds[0] === '') {
		var a = $('.CollectionProductIds');

		if (a.length > 0) {
			$.each(a, function (index, element) {
				productIds[index] = element.value;
			});
		}
	}
	return productIds;
}

function populateShippingMessage(defaultSku, $this) {
	var presellable = (typeof defaultSku.mPresellable !== "undefined" && defaultSku.mPresellable !== '') ? defaultSku.mPresellable : true,
		inventoryStatus = (typeof defaultSku.mInventoryStatus !== "undefined" && defaultSku.mInventoryStatus !== '') ? defaultSku.mInventoryStatus : false,
		shipWindowMin = defaultSku.mShipWindowMin,
		shipWindowMax = defaultSku.mShipWindowMax,
		channelAvailability = defaultSku.mChannelAvailability,
		space = "&nbsp;", leaveWhereHouseDiv = $this.find('.leaves-warehouse');

	if (!presellable && inventoryStatus != 'outOfStock' && ((channelAvailability == undefined || channelAvailability == '') || channelAvailability != "In Store Only")) {
		if ($('.quickview-sticky-price').length > 0) {
			var html = '<div class="green-list-dot inline"></div>';
		}
		if ($('.quickview-sticky-price').length == 0) {
			var html = '<div class="leaves-warehouse-shipping-message">shipping</div>';
		}
		if (typeof shipWindowMin !== "undefined" && shipWindowMin !== "" && typeof shipWindowMax !== "undefined" && shipWindowMax !== "") {
			html += usuallyLeavesWarehouse + space + shipWindowMin + labelMinus + shipWindowMax + space + fullBusinessDays + '';
		} else {
			html += leavesWarehouse + '';
		}
		if ($('.quickview-sticky-price').length > 0) {
			html += '<div class="dot-class"></div>';
			html += '<div class="inline details-inline"><button  class="learn-more forDetails" data-toggle="modal" data-target="#detailsPopup">' + labelDetails + '</button></div>';
			html += '<div data-toggle="modal" data-target="#detailsPopup"><p class="hide" data-toggle="modal" data-target="#detailsPopup">' + avilabilityPopover + '</p></div>';
		}

		//html += '</div>';
		if (!($this.find('.leaves-warehouse').length) && $this.find('.pre-order').length) {
			$this.find('.pre-order').toggleClass("pre-order leaves-warehouse");
		}
		leaveWhereHouseDiv = $this.find('.leaves-warehouse');
		if (leaveWhereHouseDiv.length > 0) {
			leaveWhereHouseDiv.html(html).show();
		} else {
			if ($this.find('.product-availability > div:first').is('.available-online')) {
				$this.find('.product-availability > div:first').after(html);
			} else {
				$this.find('.product-availability').prepend(html);
			}
		}
	} else if ((!presellable && inventoryStatus == 'outOfStock' && channelAvailability != "In Store Only") || channelAvailability == "In Store Only") {
		$('.pdp-commerce-zone .find-store-container .store-image div').removeClass('ICN-truck').addClass('ICN-truck-disabled');
		//var wareHouseHtml = '<div class="leaves-warehouse-shipping-message">shipping</div><span>not available</span>';
		var wareHouseHtml = '<div class="leaves-warehouse-shipping-message">shipping not available</div>';
		var leaveWhereHouseDiv = $this.find('.leaves-warehouse');
		if (leaveWhereHouseDiv.length > 0) {
			leaveWhereHouseDiv.html(wareHouseHtml).show();
		}
	}
	else if (presellable && inventoryStatus === 'outOfStock') {
		$this.parents(".sticky-price").find(".product-availability .preorder-availability .preorder-estimated-shipping-avail").html("shipping not available");
		$this.parents(".sticky-price").find(".product-availability .preorder-availability .limit-1.inline").addClass('hide');
		$this.parents(".sticky-price").find(".product-availability .find-store-container .store-image").addClass('greyImage');
	}/*else if(leaveWhereHouseDiv.length > 0){
		//leaveWhereHouseDiv.hide();
	}*/

}

function notCachedPriceAndImage(pdpCheckFlag, imageFlag) {
	var pdpCheckFlagValue = (pdpCheckFlag == "pdp") ? true : false;
	var imageFlagValue = (imageFlag == "imageNeedChange") ? true : false;
	var productIds = getProductIds(pdpCheckFlagValue);
	var beforeAddToCartCloseFlag = addToCartCloseFlag;
	for (var indx in productIds) {

		$('.sticky-price-contents').each(function () {
			var pid;
			var skuId;
			var parent;
			if ($(this).parent().attr('class') != 'collections-sticky-price') {
				if ($(this).parent().hasClass("collectionLineItem")) {
					pid = $(this).attr('data-id') ? $(this).attr('data-id').split('-') : $(this).parent().attr('data-id').split('-');
					pid = pid[1];
				} else {
					pid = $(this).attr('id') ? $(this).attr('id').split('-') : $(this).parent().attr('id').split('-');
					pid = pid[1];
				}
				parent = $(this).parent();
				var productAndskuId = parent.find('.productSkuId').val();
				if (typeof productAndskuId != 'undefined' && productAndskuId != '') {
					var array = productAndskuId.split('-');
					skuId = array[1];
				}
			}
			var _this = $(this);
			var productId = productIds[indx];
			if ((productId == pid)) {
				var productInfoJson = JSON.parse($('.productInfoJson-' + productId).val());
				if (typeof productInfoJson != 'undefined') {
					var productType = productInfoJson.mColorSizeVariantsAvailableStatus;
					var activeSkus = productInfoJson.mActiveSKUsList;
					var defaultSku;
					if (parent.hasClass("collectionLineItem") && productType === 'noColorSizeAvailable'
						&& typeof activeSkus != 'undefined' && activeSkus.length > 1) {
						for (var count in activeSkus) {
							if (typeof skuId != 'undefined' && skuId != '' && activeSkus[count].mId === skuId) {
								defaultSku = activeSkus[count];
								break;
							}
						}
					} else {
						defaultSku = productInfoJson.mDefaultSKU;
					}
					if (addToCartCloseFlag && $(".collection-template").length < 1) {
						populatePriceFromCartOverlay(defaultSku, _this, productInfoJson)
					} else {
						populatePrice(defaultSku, _this, productInfoJson);
					}

					generatreProductDetailBySkuID(productId, defaultSku, _this);
					if (typeof isPopulateShippingMessage != 'undefined' && isPopulateShippingMessage != 'null' && isPopulateShippingMessage != '' && isPopulateShippingMessage == 'true' && !beforeAddToCartCloseFlag) {
						populateShippingMessage(defaultSku, _this);
					}
					//console.log(productDetail);
					if (imageFlagValue && typeof productType != 'undefined'
						&& productType != '' && productType != 'noColorSizeAvailable') {
						var $collectionImageChange = $('#collectonRefresh-' + productId);
						populateImages(defaultSku, productId, '', $collectionImageChange);
					}
					else {
						if ($('.product-details-template').length > 0 && !beforeAddToCartCloseFlag) {
							thumbnailCarousel();
						}
					}
				}
			}
		});

	}
}

function populatePriceFromCartOverlay(defaultSku, _this, productInfoJson) {
	var parentId = '';
	var productId = '';
	addToCartStateFlag = true;
	var pageName = $('.sticky-price-contents').find('.pageName').val();
	if (pageName == 'pdpPage') {
		parentId = 'pdpId';
		productId = $('#hiddenProductId').val();
	} else if (pageName == 'quickView') {
		parentId = 'quickViewId';
		productId = $('#hiddenProductIdQuickView').val();
	}
	var colorCode = $('#' + parentId + '-' + productId).length ? $('#' + parentId + '-' + productId + ' .color-block.color-selected .color-block-inner').attr('id') : $('[data-id=' + parentId + '-' + productId + '] .color-block.color-selected .color-block-inner').attr('id');
	var sizeCode = $('#' + parentId + '-' + productId).length ? $('#' + parentId + '-' + productId + ' .size-option.size-selected .content').attr('data-id') : $('[data-id=' + parentId + '-' + productId + '] .size-option.size-selected .content').attr('data-id');
	colorCode = (colorCode) ? colorCode : '';
	sizeCode = (sizeCode) ? sizeCode : '';
	var variantName;
	if ((colorCode && sizeCode) || sizeCode) {
		variantName = 'size';
	} else {
		variantName = 'color';
	}
	var defaultSku = getDefaultSku(productId, colorCode, sizeCode, productInfoJson, variantName, _this)
	populatePrice(defaultSku, _this, productInfoJson, colorCode, sizeCode);
	addToCartCloseFlag = false;
	addToCartStateFlag = false;
}

function getValueFromJson(key, obj) {
	if (key in obj) {
		return obj[key];
	} else {
		return '';
	}
}

function generatreProductDetailBySkuID(productID, defaultSku, _this) {
	//var productInfoJson = JSON.parse($('#productInfoJson-'+productId).val());
	var prodDArr = [];
	if (typeof defaultSku !== 'undefined') {
		console.log(productID, defaultSku);
		prodDArr.push(productID);
		prodDArr.push(getValueFromJson('mSknId', defaultSku));
		prodDArr.push(('mSkuUpcNumbers' in defaultSku) ? defaultSku.mSkuUpcNumbers[0] : '');
		prodDArr.push(getValueFromJson('mId', defaultSku));
		prodDArr.push(('mRmsColorCode' in defaultSku) ? defaultSku.mRmsColorCode : ((_this.find('.rgs_defaultColor').length > 0) ? _this.find('.rgs_defaultColor').val() : '0'));
		prodDArr.push(('mRmsSizeCode' in defaultSku) ? defaultSku.mRmsSizeCode : ((_this.find('.rgs_defaultSize').length > 0) ? _this.find('.rgs_defaultSize').val() : '0'));
		prodDArr.push(('mSknOriginId' in defaultSku) ? defaultSku.mSknOriginId : 0);
		//return prodDArr.join('|');
		if (_this.parents('.collection-product-block').length > 0) {
			_this.find(".collection_productDetails-" + productID).val(prodDArr.join('|'));
			//console.log(_this.find( "#collection_productDetails-" + productID ));
		} else {
			_this.find('#rgs_productDetails').val(prodDArr.join('|'));
			//console.log(_this.find('#rgs_productDetails'));
		}
	}
	//return false;
}

function notCachedAddTocartButton(pdpCheckFlag, quickViewPagename) {
	var pdpCheckFlagValue = (pdpCheckFlag == "pdp") ? true : false;
	var productIds = getProductIds(pdpCheckFlagValue);
	for (var indx = 0; indx < productIds.length; indx++) {

		/*$('.sticky-price-contents').each(function(){
			var _this = $(this);*/
		var productId = productIds[indx];
		if ($('.productInfoJson-' + productId).val()) {
			var productInfoJson = JSON.parse($('.productInfoJson-' + productId).val());
			if (typeof productInfoJson != 'undefined') {
				var defaultSku = productInfoJson.mDefaultSKU;
				changeAddTocartButton(productId, defaultSku, '', '', $('.sticky-price-contents'), quickViewPagename, indx);
				notCachedRegistryButtons(productId, indx);
			}
		}

		/*});*/
	}
}

function changeAddTocartButtonFromOverlay(pdpCheckFlag) {

	var parentId = '';
	var productId = '';
	addToCartStateFlag = true;
	var pageName = $('.sticky-price-contents').find('.pageName').val();
	if (pageName == 'pdpPage') {
		parentId = 'pdpId';
		productId = $('#hiddenProductId').val();
	} else if (pageName == 'quickView') {
		parentId = 'quickViewId';
		productId = $('#hiddenProductIdQuickView').val();
	}
	var colorCode = $('#' + parentId + '-' + productId).length ? $('#' + parentId + '-' + productId + ' .color-block.color-selected .color-block-inner').attr('id') : $('[data-id=' + parentId + '-' + productId + '] .color-block.color-selected .color-block-inner').attr('id');
	var sizeCode = $('#' + parentId + '-' + productId).length ? $('#' + parentId + '-' + productId + ' .size-option.size-selected .content').attr('data-id') : $('[data-id=' + parentId + '-' + productId + '] .size-option.size-selected .content').attr('data-id');
	colorCode = (colorCode) ? colorCode : '';
	sizeCode = (sizeCode) ? sizeCode : '';
	colorCode = (colorCode) ? colorCode : '';
	sizeCode = (sizeCode) ? sizeCode : '';
	var variantName;
	if ((colorCode && sizeCode) || sizeCode) {
		variantName = 'size';
	} else {
		variantName = 'color';
	}
	var pdpCheckFlagValue = false;
	var productIds = getProductIds(pdpCheckFlagValue);
	for (var indx = 0; indx < productIds.length; indx++) {
		$('.sticky-price-contents').each(function () {
			var _this = $(this);
			var productId = productIds[indx];
			var productInfoJson = JSON.parse($('.productInfoJson-' + productId).val());
			if (typeof productInfoJson != 'undefined') {
				var defaultSku = getDefaultSku(productId, colorCode, sizeCode, productInfoJson, variantName, _this);
				if (defaultSku) {
					changeAddTocartButton(productId, defaultSku, colorCode, sizeCode, _this);
				}
			}

		});

	}
}

function changeAddTocartButton(pProductId, pDefaultSku, pColorCode, pSizeCode, $parentDiv, quickViewPagename, ind) {
	var index = ind ? ind : 0;

	var myStoreCookie = $.cookie("favStore");
	var addToCartMessage = (typeof addToCartMessage != 'undefined' && addToCartMessage != '') ? addToCartMessage : 'add to cart';
	var _Temp = '';
	if (typeof myStoreCookie != 'undefined' && myStoreCookie != '' && myStoreCookie != null) {
		_Temp = myStoreCookie.split('|')[1];

	}
	var preSellErrorMsg = (typeof preSellErrorMsg != 'undefined' && preSellErrorMsg != '') ? preSellErrorMsg : 'This pre-sell item has reached the maximum limit and cannot be added to the cart';
	var isRegistry = getParameterByName("isRegistry");
	var contextPath = $('.hiddenContextPath').val();
	var productInfoJson = JSON.parse($('.productInfoJson-' + pProductId).val());
	var onlineInventoryStatus = pDefaultSku.mInventoryStatus,
		storeInventoryStatus = pDefaultSku.mInventoryStatusInStore,
		notifyMe = pDefaultSku.mNotifyMe,
		ispu = pDefaultSku.mIspu,
		s2s = pDefaultSku.mS2s,
		channelAvailability = pDefaultSku.mChannelAvailability,
		storeAvailMsg = pDefaultSku.mInventoryStatusInStore,
		locationIdFromCookie = _Temp,
		unCartable = pDefaultSku.mUnCartable,
		selectedColor = pColorCode,
		defaultColorCode = pDefaultSku.mColorCode,
		selectedSize = pSizeCode,
		defaultJuvenileSize = pDefaultSku.mJuvenileSize,
		// activeSKUsList=productInfoJson.mInStockSKUsList,
		activeSKUsListLength = productInfoJson.mInStockSKUsListLength,
		presellable = (typeof pDefaultSku.mPresellable != "undefined") ? pDefaultSku.mPresellable : false,
		availableInventory = pDefaultSku.mAvailableInventory,
		colorSizeVariantsAvailableStatus = productInfoJson.mColorSizeVariantsAvailableStatus,
		presellQuantityUnits = pDefaultSku.mPresellQuantityUnits,
		streetDate = pDefaultSku.mStreetDate;
	try {
		streetDate = streetDate.substring(0, streetDate.lastIndexOf("/")) + "/" + streetDate.substring(streetDate.lastIndexOf("/") + 3);
	} catch (e) { }
	var colorLength = 0;
	if (typeof productInfoJson.mColorCodeList != 'undefined' && productInfoJson.mColorCodeList != '') {
		colorLength = productInfoJson.mColorCodeList.length;
	}
	var juvenileSizesListLength = 0;
	if (typeof productInfoJson.mJuvenileSizesList != 'undefined' && productInfoJson.mJuvenileSizesList != '') {
		juvenileSizesListLength = productInfoJson.mJuvenileSizesList.length;
	}
	if (typeof ispu === 'undefined') {
		ispu = 'N';
	}
	if (typeof s2s === 'undefined') {
		s2s = 'N';
	}
	if (channelAvailability == undefined || channelAvailability == '' || channelAvailability == null || channelAvailability === 'N/A') {
		channelAvailability = 'both';
	}
	if (typeof pDefaultSku.mPrimaryCanonicalImage != 'undefined' && pDefaultSku.mPrimaryCanonicalImage != '') {
		productImage = pDefaultSku.mPrimaryCanonicalImage;
	} else if (typeof akamaiNoImageURL != 'undefined' && akamaiNoImageURL != '') {
		productImage = akamaiNoImageURL;
	} else {
		productImage = window.TRUImagePath + 'images/no-image500.gif';
	}

	var lastAccessedRegistryID = $.cookie("lastAccessedRegistryID");
	var registry_id = $.cookie("REGISTRY_USER");
	var pdpUrl = $('#productPageUrlWithDomainHidden').val();
	var siteIdNMWA = $('#siteIdHidden').val();
	var htmlData = '';
	var nmwa = false;
	if (lastAccessedRegistryID != undefined && registry_id != undefined && registry_id != 'null' && lastAccessedRegistryID != 'null' && lastAccessedRegistryID != '' && registry_id != '' && !presellable) {
		isButtonSwap = true;
	}
	if (notifyMe === 'Y' && !presellable && ((onlineInventoryStatus === 'outOfStock' && s2s === 'N' && ispu === 'N' && locationIdFromCookie === '' && channelAvailability != 'In Store Only')
		|| (onlineInventoryStatus === 'outOfStock' && locationIdFromCookie != '' && s2s === 'N' && ispu === 'N')
		|| (onlineInventoryStatus === 'outOfStock' && locationIdFromCookie != '' && storeInventoryStatus != 'inStock'))) {
		isButtonSwap = false;
		nmwa = true;
	}
	if (!presellable && channelAvailability == 'In Store Only' && (locationIdFromCookie === '')) {
		if (!(s2s === 'N' && ispu === 'N')) {
			var addToCartFunction = "javascript:return loadFindInStore('" + contextPath + "','" + pDefaultSku.mId + "','',$('.QTY-" + pProductId + "').val());";
			htmlData = '<div class="add-to-cart-section"><button class="add-to-cart" data-toggle="modal" data-target="#findInStoreModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button></div>';
			$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
		} else {
			htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart">out of stock</button></div>';
			$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
			isButtonSwap = false;
		}
	} else if (!presellable && channelAvailability == 'In Store Only' && (locationIdFromCookie != '')) {
		if (storeInventoryStatus == 'inStock' && !(s2s === 'N' && ispu === 'N')) {
			var addToCartFunction = "javascript: return addItemToCart('" + pProductId + "','" + pDefaultSku.mId + "','" + locationIdFromCookie + "','" + contextPath + "',this,'productPage');";
			htmlData = '<div class="add-to-cart-section"><button class="add-to-cart" data-toggle="modal" data-target="#shoppingCartModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button></div>';
			$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
		} else {
			htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart">out of stock</button></div>';
			$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
			isButtonSwap = false;
		}
	} else if (nmwa) {
		htmlData = '<div class="email-section"><div style="display: none;" class="NotifyMeDescription">' + pDefaultSku.mLongDescription + '</div><div style="display: none;" class="NotifyMeDskuDisplayName">' + pDefaultSku.mDisplayName + '</div><input type="hidden" class="productId" value=' + productInfoJson.mProductId + '><input type="hidden" class="NotifyMeskuId" value=' + pDefaultSku.mId + '><input type="hidden" class="NotifyMeProductPageUrl" value=' + pdpUrl + '><div style="display: none;" class="NotifyMeHelpURLName">' + pDefaultSku.mDisplayName + '</div><input type="hidden" class="NotifyMeHelpURLValue" value=' + pdpUrl + '><input type="hidden" class="primaryImage" value=' + productImage + '><input type="hidden" class="siteID" value=' + siteIdNMWA + '><button class="email-me-available" data-toggle="modal" data-target="#notifyMeModal" onclick=loadNotifyMe(this)>' + emailmeavailableMessage + '</button></div>';
		$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);

	} else if ((onlineInventoryStatus === 'outOfStock') && (colorSizeVariantsAvailableStatus === 'noColorSizeAvailable') && (locationIdFromCookie === '') && !(s2s === 'N' && ispu === 'N') && !presellable) {
		var addToCartFunction = "javascript:return loadFindInStore('" + contextPath + "','" + pDefaultSku.mId + "','',$('.QTY-" + pProductId + "').val());";
		htmlData = '<div class="add-to-cart-section"><button class="add-to-cart" data-toggle="modal" data-target="#findInStoreModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button></div>';
		$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);

	} else if ((onlineInventoryStatus === 'outOfStock') && (colorSizeVariantsAvailableStatus === 'noColorSizeAvailable') && (locationIdFromCookie != '' && storeAvailMsg === 'inStock') && !(s2s === 'N' && ispu === 'N') && !presellable) {
		var addToCartFunction = "javascript: return addItemToCart('" + pProductId + "','" + pDefaultSku.mId + "','" + locationIdFromCookie + "','" + contextPath + "',this,'productPage');";
		htmlData = '<div class="add-to-cart-section"><button class="add-to-cart" data-toggle="modal" data-target="#shoppingCartModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button></div>';
		$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);

	} else if (onlineInventoryStatus === 'outOfStock' && locationIdFromCookie != '' && colorSizeVariantsAvailableStatus === 'noColorSizeAvailable' && s2s === 'N' && ispu === 'N' && !presellable) {
		htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart">out of stock</button></div>';
		$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
		isButtonSwap = false;
	} else if (onlineInventoryStatus === 'outOfStock' && locationIdFromCookie === '' && colorSizeVariantsAvailableStatus === 'noColorSizeAvailable' && s2s === 'N' && ispu === 'N' && !presellable && (storeAvailMsg === '' || storeAvailMsg === 'outOfStock')) {
		htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart">out of stock</button></div>';
		$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
		isButtonSwap = false;
	} else if (onlineInventoryStatus === 'outOfStock' && (locationIdFromCookie != '' && storeAvailMsg === 'outOfStock') && colorSizeVariantsAvailableStatus === 'noColorSizeAvailable' && s2s === 'N' && ispu === 'Y' && !presellable && !unCartable) {
		htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart">out of stock</button></div>';
		$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
		isButtonSwap = false;
	}
	else if ((!unCartable) && (onlineInventoryStatus != 'outOfStock') && ((colorSizeVariantsAvailableStatus === 'bothColorSizeAvailable' && (selectedColor != '' && defaultColorCode === selectedColor) && (selectedSize != '' && defaultJuvenileSize === selectedSize))
		|| (colorSizeVariantsAvailableStatus === 'noColorSizeAvailable' && typeof pDefaultSku != 'undefined' && pDefaultSku != '') || (colorSizeVariantsAvailableStatus === 'onlyColorVariants' && ((selectedColor != '' && selectedSize === '') || ((colorLength === 1 || colorLength === 0) && activeSKUsListLength > 0)))
		|| (colorSizeVariantsAvailableStatus === 'onlySizeVariants' && ((selectedSize != '' && selectedColor === '') || ((juvenileSizesListLength === 0 || juvenileSizesListLength === 1) && activeSKUsListLength > 0))))) {
		if ((presellable && presellQuantityUnits === '0') || (!presellable && availableInventory === 0)) {
			htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart fade-add-to-cart">out of stock</button></div>';
			$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
			isButtonSwap = false;

		} else {
			/*var utagArgument = "{'event_type':'cart_add'}";*/
			var addToCartFunction = "javascript: return addItemToCart('" + pProductId + "','" + pDefaultSku.mId + "','','" + contextPath + "',this);";
			if (presellable) {
				htmlData = '<div class="add-to-cart-section"><button class="add-to-cart" data-toggle="modal" data-target="#shoppingCartModal" onclick="' + addToCartFunction + '">' + preOrderAddToCartMessage + ' (available on ' + streetDate + ')' + '</button></div>';
				$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
				isButtonSwap = false;
			} else {
				htmlData = '<div class="add-to-cart-section"><button class="add-to-cart" data-toggle="modal" data-target="#shoppingCartModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button></div>';
				$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
			}
		}

	} else if (addToCartStateFlag) {
		var addToCartFunction = "javascript: return addItemToCart('" + pProductId + "','" + pDefaultSku.mId + "','','" + contextPath + "',this);";
		htmlData = '<div class="add-to-cart-section"><button class="add-to-cart" data-toggle="modal" data-target="#shoppingCartModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button></div>';
		$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
		addToCartStateFlag = false;
	}
	else {
		if (presellable) {
			htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart">' + preOrderAddToCartMessage + ' (available on ' + streetDate + ')' + '</button></div>';
			$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
			isButtonSwap = false;
		} else  if ( colorSizeVariantsAvailableStatus === 'noColorSizeAvailable' ) {
			htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart">out of stock</button></div>';
			$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
			isButtonSwap = false;
		}
	}
	if (unCartable) {
		htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart">' + addToCartMessage + '</button></div>';
		$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
		isButtonSwap = false;
	}
	if ((locationIdFromCookie != '' && onlineInventoryStatus === "outOfStock" && storeInventoryStatus == "outOfStock" && colorSizeVariantsAvailableStatus === 'noColorSizeAvailable') || (onlineInventoryStatus === "outOfStock" && s2s === 'N' && ispu === 'N' && colorSizeVariantsAvailableStatus === 'noColorSizeAvailable')) {
		if (!presellable) {
			$($parentDiv[index]).find(".out-of-stock.out-of-stock-message-container").html('<span>out of stock</span><div class="small-blue inline"><button class="learn-more" data-toggle="modal" data-target="#learnMorePopupStore">learn more</button></div>');
		} else {
			$($parentDiv[index]).find(".out-of-stock.out-of-stock-message-container").html('<span class="pre-outofstock-message">' + preSellErrorMsg + '</span>');
		}
		$(".sticky-price-contents .product-availability .pre-order").show();
	} else {
		$($parentDiv[index]).find(".out-of-stock.out-of-stock-message-container").html('');
	}
	if (registrySwapFlag && isButtonSwap) {
		//buttonSwapForPDP();
	}


}

function changeAddTocartButtonNew(pProductId, pDefaultSku, pColorCode, pSizeCode, $parentDiv, quickViewPagename) {
	var myStoreCookie = $.cookie("favStore");
	var addToCartMessage = (typeof addToCartMessage != 'undefined' && addToCartMessage != '') ? addToCartMessage : 'add to cart';
	var _Temp = '';
	if (typeof myStoreCookie != 'undefined' && myStoreCookie != '' && myStoreCookie != null) {
		_Temp = myStoreCookie.split('|')[1];
	}
	var isRegistry = getParameterByName("isRegistry");
	var contextPath = $('.hiddenContextPath').val();
	var productInfoJson = JSON.parse($('.productInfoJson-' + pProductId).val());
	var onlineInventoryStatus = pDefaultSku.mInventoryStatus,
		storeInventoryStatus = pDefaultSku.mInventoryStatusInStore,
		notifyMe = pDefaultSku.mNotifyMe,
		ispu = pDefaultSku.mIspu,
		s2s = pDefaultSku.mS2s,
		channelAvailability = pDefaultSku.mChannelAvailability,
		storeAvailMsg = pDefaultSku.mInventoryStatusInStore,
		locationIdFromCookie = _Temp,
		unCartable = pDefaultSku.mUnCartable,
		selectedColor = pColorCode,
		defaultColorCode = pDefaultSku.mColorCode,
		selectedSize = pSizeCode,
		defaultJuvenileSize = pDefaultSku.mJuvenileSize,
		// activeSKUsList=productInfoJson.mInStockSKUsList,
		activeSKUsListLength = productInfoJson.mInStockSKUsListLength,
		presellable = (typeof pDefaultSku.mPresellable != "undefined") ? pDefaultSku.mPresellable : false,
		availableInventory = pDefaultSku.mAvailableInventory,
		colorSizeVariantsAvailableStatus = productInfoJson.mColorSizeVariantsAvailableStatus,
		presellQuantityUnits = pDefaultSku.mPresellQuantityUnits,
		streetDate = pDefaultSku.mStreetDate;
	try {
		streetDate = streetDate.substring(0, streetDate.lastIndexOf("/")) + "/" + streetDate.substring(streetDate.lastIndexOf("/") + 3);
	} catch (e) { }
	var colorLength = 0;
	if (typeof productInfoJson.mColorCodeList != 'undefined' && productInfoJson.mColorCodeList != '') {
		colorLength = productInfoJson.mColorCodeList.length;
	}
	var juvenileSizesListLength = 0;
	if (typeof productInfoJson.mJuvenileSizesList != 'undefined' && productInfoJson.mJuvenileSizesList != '') {
		juvenileSizesListLength = productInfoJson.mJuvenileSizesList.length;
	}
	if (typeof ispu === 'undefined') {
		ispu = 'N';
	}
	if (typeof s2s === 'undefined') {
		s2s = 'N';
	}
	if (channelAvailability == undefined || channelAvailability == '' || channelAvailability == null || channelAvailability === 'N/A') {
		channelAvailability = 'both';
	}
	if (typeof pDefaultSku.mPrimaryCanonicalImage != 'undefined' && pDefaultSku.mPrimaryCanonicalImage != '') {
		productImage = pDefaultSku.mPrimaryCanonicalImage;
	} else if (typeof akamaiNoImageURL != 'undefined' && akamaiNoImageURL != '') {
		productImage = akamaiNoImageURL;
	} else {
		productImage = window.TRUImagePath + 'images/no-image500.gif';
	}
	var lastAccessedRegistryID = $.cookie("lastAccessedRegistryID");
	var registry_id = $.cookie("REGISTRY_USER");
	var pdpUrl = $('#productPageUrlWithDomainHidden').val();
	var siteIdNMWA = $('#siteIdHidden').val();
	var htmlData = '';
	var nmwa = false;
	if (lastAccessedRegistryID != undefined && registry_id != undefined && registry_id != 'null' && lastAccessedRegistryID != 'null' && lastAccessedRegistryID != '' && registry_id != '' && quickViewPagename != "quickView" && !presellable) {
		isButtonSwap = true;
	}
	if (notifyMe === 'Y' && !presellable && ((onlineInventoryStatus === 'outOfStock' && s2s === 'N' && ispu === 'N' && locationIdFromCookie === '' && channelAvailability != 'In Store Only')
		|| (onlineInventoryStatus === 'outOfStock' && locationIdFromCookie != '' && s2s === 'N' && ispu === 'N')
		|| (onlineInventoryStatus === 'outOfStock' && locationIdFromCookie != '' && storeInventoryStatus != 'inStock'))) {
		isButtonSwap = false;
		nmwa = true;
	}
	if (!presellable && channelAvailability == 'In Store Only' && (locationIdFromCookie === '')) {
		if (!(s2s === 'N' && ispu === 'N')) {
			var addToCartFunction = "javascript:return loadFindInStore('" + contextPath + "','" + pDefaultSku.mId + "','',$('.QTY-" + pProductId + "').val());";
			htmlData = '<div class="add-to-cart-section"><button class="add-to-cart" data-toggle="modal" data-target="#findInStoreModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button></div>';
			$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
		} else {
			htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart">out of stock</button></div>';
			$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
			isButtonSwap = false;
		}
	} else if (!presellable && channelAvailability == 'In Store Only' && (locationIdFromCookie != '')) {
		if (storeInventoryStatus == 'inStock' && !(s2s === 'N' && ispu === 'N')) {
			var addToCartFunction = "javascript: return addItemToCart('" + pProductId + "','" + pDefaultSku.mId + "','" + locationIdFromCookie + "','" + contextPath + "',this,'productPage');";
			htmlData = '<div class="add-to-cart-section"><button class="add-to-cart" data-toggle="modal" data-target="#shoppingCartModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button></div>';
			$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
		} else {
			htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart">out of stock</button></div>';
			$($parentDiv[index]).find('#addToCartDivSpace').html(htmlData);
			isButtonSwap = false;

		}
	} else if (nmwa) {
		htmlData = '<div class="email-section"><div style="display: none;" class="NotifyMeDescription">' + pDefaultSku.mLongDescription + '</div><div style="display: none;" class="NotifyMeDskuDisplayName">' + pDefaultSku.mDisplayName + '</div><input type="hidden" class="productId" value=' + productInfoJson.mProductId + '><input type="hidden" class="NotifyMeskuId" value=' + pDefaultSku.mId + '><input type="hidden" class="NotifyMeProductPageUrl" value=' + pdpUrl + '><div style="display: none;" class="NotifyMeHelpURLName">' + pDefaultSku.mDisplayName + '</div><input type="hidden" class="NotifyMeHelpURLValue" value=' + pdpUrl + '><input type="hidden" class="primaryImage" value=' + productImage + '><input type="hidden" class="siteID" value=' + siteIdNMWA + '><button class="email-me-available" data-toggle="modal" data-target="#notifyMeModal" onclick=loadNotifyMe(this)>' + emailmeavailableMessage + '</button></div>';
		$parentDiv.find('#addToCartDivSpace').html(htmlData);
	} else if ((onlineInventoryStatus === 'outOfStock') && (colorSizeVariantsAvailableStatus === 'noColorSizeAvailable') && (locationIdFromCookie === '') && !(s2s === 'N' && ispu === 'N') && !presellable) {
		var addToCartFunction = "javascript:return loadFindInStore('" + contextPath + "','" + pDefaultSku.mId + "','',$('.QTY-" + pProductId + "').val());";
		htmlData = '<div class="add-to-cart-section"><button class="add-to-cart" data-toggle="modal" data-target="#findInStoreModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button></div>';
		$parentDiv.find('#addToCartDivSpace').html(htmlData);
	} else if ((onlineInventoryStatus === 'outOfStock') && (colorSizeVariantsAvailableStatus === 'noColorSizeAvailable') && (locationIdFromCookie != '' && storeAvailMsg === 'inStock') && !(s2s === 'N' && ispu === 'N') && !presellable) {
		var addToCartFunction = "javascript: return addItemToCart('" + pProductId + "','" + pDefaultSku.mId + "','" + locationIdFromCookie + "','" + contextPath + "',this,'productPage');";
		htmlData = '<div class="add-to-cart-section"><button class="add-to-cart" data-toggle="modal" data-target="#shoppingCartModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button></div>';
		$parentDiv.find('#addToCartDivSpace').html(htmlData);
	} else if (onlineInventoryStatus === 'outOfStock' && locationIdFromCookie != '' && colorSizeVariantsAvailableStatus === 'noColorSizeAvailable' && s2s === 'N' && ispu === 'N' && !presellable) {
		htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart">out of stock</button></div>';
		$parentDiv.find('#addToCartDivSpace').html(htmlData);
		isButtonSwap = false;
	} else if (onlineInventoryStatus === 'outOfStock' && locationIdFromCookie === '' && colorSizeVariantsAvailableStatus === 'noColorSizeAvailable' && s2s === 'N' && ispu === 'N' && !presellable && (storeAvailMsg === '' || storeAvailMsg === 'outOfStock')) {
		htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart">out of stock</button></div>';
		$parentDiv.find('#addToCartDivSpace').html(htmlData);
		isButtonSwap = false;
	} else if ((!unCartable) && (onlineInventoryStatus != 'outOfStock') && ((colorSizeVariantsAvailableStatus === 'bothColorSizeAvailable' && (selectedColor != '' && defaultColorCode === selectedColor) && (selectedSize != '' && defaultJuvenileSize === selectedSize))
		|| (colorSizeVariantsAvailableStatus === 'noColorSizeAvailable' && typeof pDefaultSku != 'undefined' && pDefaultSku != '') || (colorSizeVariantsAvailableStatus === 'onlyColorVariants' && ((selectedColor != '' && selectedSize === '') || ((colorLength === 1 || colorLength === 0) && activeSKUsListLength > 0)))
		|| (colorSizeVariantsAvailableStatus === 'onlySizeVariants' && ((selectedSize != '' && selectedColor === '') || ((juvenileSizesListLength === 1 || juvenileSizesListLength === 0) && activeSKUsListLength > 0))))) {
		if ((presellable && presellQuantityUnits === '0') || (!presellable && availableInventory === 0)) {
			htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart fade-add-to-cart">out of stock</button></div>';
			$parentDiv.find('#addToCartDivSpace').html(htmlData);
			isButtonSwap = false;
		} else {
			/*var utagArgument = "{'event_type':'cart_add'}";*/
			var addToCartFunction = "javascript: return addItemToCart('" + pProductId + "','" + pDefaultSku.mId + "','','" + contextPath + "',this);";
			if (presellable) {
				htmlData = '<div class="add-to-cart-section"><button class="add-to-cart" data-toggle="modal" data-target="#shoppingCartModal" onclick="' + addToCartFunction + '">' + preOrderAddToCartMessage + ' (available on ' + streetDate + ')' + '</button></div>';
				$parentDiv.find('#addToCartDivSpace').html(htmlData);
				isButtonSwap = false;
			} else {
				htmlData = '<div class="add-to-cart-section"><button class="add-to-cart" data-toggle="modal" data-target="#shoppingCartModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button></div>';
				$parentDiv.find('#addToCartDivSpace').html(htmlData);
			}
		}
	} else if (addToCartStateFlag) {
		var addToCartFunction = "javascript: return addItemToCart('" + pProductId + "','" + pDefaultSku.mId + "','','" + contextPath + "',this);";
		htmlData = '<div class="add-to-cart-section"><button class="add-to-cart" data-toggle="modal" data-target="#shoppingCartModal" onclick="' + addToCartFunction + '">' + addToCartMessage + '</button></div>';
		$parentDiv.find('#addToCartDivSpace').html(htmlData);
	}
	else {
		if (presellable) {
			htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart">' + preOrderAddToCartMessage + ' (available on ' + streetDate + ')' + '</button></div>';
			$parentDiv.find('#addToCartDivSpace').html(htmlData);
			isButtonSwap = false;
		} else if ( colorSizeVariantsAvailableStatus === 'noColorSizeAvailable' || colorSizeVariantsAvailableStatus === 'onlySizeVariants' || colorSizeVariantsAvailableStatus === 'onlyColorVariants' ) {
			htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart">out of stock</button></div>';
			$parentDiv.find('#addToCartDivSpace').html(htmlData);
			isButtonSwap = false;
		} else if ( colorSizeVariantsAvailableStatus === 'bothColorSizeAvailable' && (selectedColor != '' && defaultColorCode === selectedColor) && (selectedSize != '' && defaultJuvenileSize === selectedSize) ) {
            htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart">out of stock</button></div>';
            $parentDiv.find('#addToCartDivSpace').html(htmlData);
            isButtonSwap = false;
        }
	}
	if (unCartable) {
		htmlData = '<div class="add-to-cart-section"><button disabled="disabled" class="add-to-cart">' + addToCartMessage + '</button></div>';
		$parentDiv.find('#addToCartDivSpace').html(htmlData);
		isButtonSwap = false;
	}
	if ((locationIdFromCookie != '' && onlineInventoryStatus === "outOfStock" && storeInventoryStatus == "outOfStock" && colorSizeVariantsAvailableStatus === 'noColorSizeAvailable') || (onlineInventoryStatus === "outOfStock" && s2s === 'N' && ispu === 'N' && colorSizeVariantsAvailableStatus === 'noColorSizeAvailable')) {
		$(".out-of-stock.out-of-stock-message-container").html('<span>out of stock</span><span class="dotClass"></span><div class="small-blue inline"><button class="learn-more" data-toggle="modal" data-target="#learnMorePopupStore">learn more</button></div>');
	} else {
		$(".out-of-stock.out-of-stock-message-container").html('');
	}

	if (registrySwapFlag && isButtonSwap) {
		buttonSwapForPDP();
	}


}

function buttonSwapForPDP() {
	/*   $('.sticky-price-template').addClass('registrySwap');
$('.sticky-price-template .add-to-cart').html(carttext);
	   $('.registrySwap .add-to > div:nth-child(2)> a').addClass('registryLinkIndicator').html(addToBabyRegistry);*/
	$('.sticky-price-template').addClass('sparkRegistrySwap');
}

function backToWishListOrRegistryButtons() {
	var isRegistry = getParameterByName("isRegistry");
	var isWishlist = getParameterByName("isWishlist");
	var registry_id = $.cookie("REGISTRY_USER");
	var lastAccessedRegistryID = $.cookie("lastAccessedRegistryID");
	var wishlist_id = $.cookie("myWishlistID");
	var lastAccessedWishlistID = $.cookie("lastAccessedWishlistID");
	var lastAccessedRegistry_name = $.cookie("lastAccessedRegistryName");
	var lastAccessedWishlist_name = $.cookie("lastAccessedWishlistName");
	var pdpBackToRegistryURL = $('.pdpBackToRegistryURL').val();
	var pdpBackToAnonymousRegistryURL = $('.pdpBackToAnonymousRegistryURL').val();
	var pdpBackToAnonymousWishlistURL = $('.pdpBackToAnonymousWishlistURL').val();
	var pdpBackToWishlistURL = $('.pdpBackToWishlistURL').val();
	var siteId = $('#backTo_siteId').val();
	var babiesRUsSiteId = $('#backTo_babiesRUsSiteId').val();
	var toysRUsSiteId = $('#backTo_toysRUsSiteId').val();
	var truImagePath = $('#backTo_TRUImagePath').val();

	var dataHtml = '';
	if (isRegistry) {
		if ((typeof registry_id == 'undefined' || registry_id == 'null' || registry_id == '') && (typeof lastAccessedRegistryID != 'undefined' && lastAccessedRegistryID != 'null' && lastAccessedRegistryID != '')) {
			if (typeof lastAccessedRegistry_name !== "undefined" && lastAccessedRegistry_name != 'null' && lastAccessedRegistry_name != '') {
				var temp = $("<span>").html(backToRegistry);
				temp.find('span').html('&nbsp;' + lastAccessedRegistry_name.trim().toLowerCase() + "'s&nbsp;");
				backToRegistry = temp.html();
			}
			var pdpBackToURL = pdpBackToAnonymousRegistryURL + lastAccessedRegistryID;
			dataHtml = '<div class="back-to-wishlist"><a  class="back-to-wishlist tru-headline" href="' + pdpBackToURL + '"><span  class="i-backToRegistry" ></span>' + backToRegistry + '</a> </div>';
		}
		else if ((typeof registry_id != 'undefined' && registry_id != 'null' && registry_id != '') && (typeof lastAccessedRegistryID != 'undefined' && lastAccessedRegistryID != 'null' && lastAccessedRegistryID != '')) {
			if ((typeof lastAccessedRegistry_name !== "undefined" && lastAccessedRegistry_name != 'null' && lastAccessedRegistry_name != '') && (lastAccessedRegistryID != registry_id)) {
				var temp = $("<span>").html(backToRegistry);
				temp.find('span').html('&nbsp;' + lastAccessedRegistry_name.trim().toLowerCase() + "'s&nbsp;");
				backToRegistry = temp.html();
				var pdpBackToURL = pdpBackToAnonymousRegistryURL + lastAccessedRegistryID;
				dataHtml = '<div class="back-to-wishlist"><a  class="back-to-wishlist tru-headline" href="' + pdpBackToURL + '"><span  class="i-backToRegistry" ></span>' + backToRegistry + '</a> </div>';
			}
			else {
				var temp = $("<span>").html(backToRegistry);
				temp.find('span').html('&nbsp;' + my + '&nbsp;');
				backToRegistry = temp.html();
				dataHtml = '<div class="back-to-wishlist"><a  class="back-to-wishlist tru-headline" href="' + pdpBackToRegistryURL + '"><span  class="i-backToRegistry" ></span>' + backToRegistry + '</a> </div>';
			}

		}
	}
	if (isWishlist) {
		if ((typeof wishlist_id == 'undefined' || wishlist_id == 'null' || wishlist_id == '') && (typeof lastAccessedWishlistID != 'undefined' && lastAccessedWishlistID != 'null' && lastAccessedWishlistID != '')) {
			if ((typeof lastAccessedWishlist_name !== "undefined" && lastAccessedWishlist_name != 'null' && lastAccessedWishlist_name != '')) {
				var temp = $("<span>").html(backToWishlist);
				temp.find('span').html('&nbsp;' + lastAccessedWishlist_name.trim().toLowerCase() + "'s&nbsp;");
				backToWishlist = temp.html();
			}
			var pdpBackToURL = pdpBackToAnonymousWishlistURL + lastAccessedWishlistID;
			dataHtml = '<div class="back-to-wishlist"><a  class="tru-headline" href="' + pdpBackToURL + '"><span  class="i-backToRegistry" ></span>' + backToWishlist + '</a> </div>';
		}
		else if ((typeof wishlist_id != 'undefined' && wishlist_id != 'null' && wishlist_id != '') && (typeof lastAccessedWishlistID != 'undefined' && lastAccessedWishlistID != 'null' && lastAccessedWishlistID != '')) {
			if ((typeof lastAccessedWishlist_name !== "undefined" && lastAccessedWishlist_name != 'null' && lastAccessedWishlist_name != '') && (wishlist_id != lastAccessedWishlistID)) {
				var temp = $("<span>").html(backToWishlist);
				temp.find('span').html('&nbsp;' + lastAccessedWishlist_name.trim().toLowerCase() + "'s&nbsp;");
				backToWishlist = temp.html();
				var pdpBackToURL = pdpBackToAnonymousWishlistURL + lastAccessedWishlistID;
				dataHtml = '<div class="back-to-wishlist"><a  class="tru-headline" href="' + pdpBackToURL + '"><span  class="i-backToRegistry" ></span>' + backToWishlist + '</a> </div>';
			}
			else {
				var temp = $("<span>").html(backToWishlist);
				temp.find('span').html('&nbsp;' + my + '&nbsp;');
				backToWishlist = temp.html();
				dataHtml = '<div class="back-to-wishlist"><a  class="tru-headline" href="' + pdpBackToWishlistURL + '"><span  class="i-backToRegistry" ></span>' + backToWishlist + '</a> </div>';
			}

		}
	}
	/*if((typeof registry_id !='undefined' && registry_id !='null' && registry_id !='')&& (typeof lastAccessedRegistryID !='undefined' && lastAccessedRegistryID !='null' && lastAccessedRegistryID !='') && isRegistry){
		dataHtml='<div class="back-to-wishlist"><a href="'+pdpBackToRegistryURL+'"><img src="'+truImagePath+'images/breadcrumb-left-arrow.png">'+backToRegistry+'</a> </div>';
	}
	else if((typeof wishlist_id !='undefined' && wishlist_id !='null' && wishlist_id !='') && (typeof lastAccessedWishlistID !='undefined' && lastAccessedWishlistID !='null' && lastAccessedWishlistID !='') && isWishlist){
		if(typeof lastAccessedWishlist_name !== "undefined" && lastAccessedWishlist_name != 'null' && lastAccessedWishlist_name != ''){
			var temp = $("<span>").html(backToWishlist);
			temp.find('span').html(lastAccessedWishlist_name.trim() + '&nbsp;');
			backToWishlist = temp.html();
		}
		dataHtml='<div class="back-to-wishlist"><a href="'+pdpBackToWishlistURL+'"><img src="'+truImagePath+'images/breadcrumb-left-arrow.png">'+backToWishlist+'</a> </div>';
	}*/
	/*else if(typeof registry_id !='undefined' && registry_id !='null' && registry_id !=''
		&& typeof siteId !='undefined' && siteId !=''
			&& typeof babiesRUsSiteId !='undefined' && babiesRUsSiteId !='' && babiesRUsSiteId === siteId){
		dataHtml='<div class="back-to-wishlist"><a href="'+pdpRegistryUrl+'"><img src="'+truImagePath+'images/breadcrumb-left-arrow.png">'+backToRegistry+'</a> </div>';
	}else if(typeof wishlist_id !='undefined' && wishlist_id !='null' && wishlist_id !=''
		&& typeof siteId !='undefined' && siteId !=''
			&& typeof toysRUsSiteId !='undefined' && toysRUsSiteId !='' && toysRUsSiteId === siteId){
		dataHtml='<div class="back-to-wishlist"><a href="'+pdpWishlistUrl+'"><img src="'+truImagePath+'images/breadcrumb-left-arrow.png">'+backToWishlist+'</a> </div>';
	}*/
	$('.backToRegistryOrWishlist').html(dataHtml);
}

function notCachedRegistryButtons(productId, ind) {
	var index = ind ? ind : 0;
	var lastAccessedRegistryID = $.cookie("lastAccessedRegistryID");
	var lastAccessedWishlistID = $.cookie("lastAccessedWishlistID");
	$('#rgs_lastAccessedRegistryID').val(lastAccessedRegistryID);
	$('#rgs_lastAccessedWishlistID').val(lastAccessedWishlistID);


	var $parentDiv = $('.sticky-price-contents');
	/*if(typeof productId !='undefined' && productId != ''){
		$parentDiv = $('#quickViewId-'+productId);
	}*/
	var productType = $('#colorSizeVariantsAvailableStatus').val();
	var registryAndWishListHtml = '';
	var wishListhtml = '';
	var registryhtml = '';

	var pdpRegistryUrl = $('.pdpRegistryUrl').val();
	var pdpWishlistUrl = $('.pdpWishlistUrl').val();
	var productId = productId || getProductIds("");
	var productInfoJson = JSON.parse($('.productInfoJson-' + productId).val());
	if (productInfoJson != null) {
		if (productInfoJson.hasOwnProperty("mDefaultSKU") && productInfoJson.mDefaultSKU.hasOwnProperty("mSkuRegistryEligibility") && productInfoJson.mDefaultSKU.hasOwnProperty("mRegistryWarningIndicator")) {
			var registryWarningIndicator = productInfoJson.mDefaultSKU.mRegistryWarningIndicator;
			var skuRegistryEligibility = productInfoJson.mDefaultSKU.mSkuRegistryEligibility;
		}
	}
	//	var registryWarningIndicator = $('.registryWarningIndicator').val();
	//	var skuRegistryEligibility = $('.skuRegistryEligibility').val();

	var registry_id = $.cookie("REGISTRY_USER");
	var wishlist_id = $.cookie("myWishlistID");
	$('#rgs_registryNumber').val(registry_id);
	$('#rgs_wishlistId').val(lastAccessedWishlistID);
	var activeSkulength = $('.activeSKUsListLength').val();
	var colorLength = $('.colorLengthId').val();
	var sizeLength = $('.juvenileSizesListLengthId').val();
	var pdpaddtobabyregistry = (typeof pdpaddtobabyregistry != 'undefined' && pdpaddtobabyregistry != '') ? pdpaddtobabyregistry : 'add to baby registry';
	var pdpaddtowishlist = (typeof pdpaddtowishlist != 'undefined' && pdpaddtowishlist == '') ? pdpaddtowishlist : 'add to wish list';
	if (typeof productType != 'undefined' && productType != '' && ((productType === 'noColorSizeAvailable')
		|| (productType === 'onlySizeVariants' && productInfoJson.hasOwnProperty("mJuvenileSizesList") && (productInfoJson.mJuvenileSizesList.length == '1' || productInfoJson.mJuvenileSizesList.length == '0'))
		|| (productType === 'onlyColorVariants' && productInfoJson.hasOwnProperty("mColorCodeList") && (productInfoJson.mColorCodeList.length == '1' || productInfoJson.mColorCodeList.length == '0')))) {
		if (typeof registryWarningIndicator != 'undefined' && registryWarningIndicator != '' && registryWarningIndicator === 'ALLOW' && skuRegistryEligibility) {

			if (typeof registry_id != 'undefined' && registry_id != '' && registry_id != 'null' && registry_id != null) {
				registryhtml = '<button class="cart-button-enable cookie-registry baby-registry-addTo registryLoggedIn" data-registry="registry" >' + pdpaddtobabyregistry + '</button>';
			} else {
				registryhtml = '<button data-href="' + pdpRegistryUrl + '" class="cart-button-enable registryAjaxCall baby-registry-addTo registryAnonymous">' + pdpaddtobabyregistry + '</button>';
			}
			if (typeof wishlist_id != 'undefined' && wishlist_id != '' && wishlist_id != 'null' && wishlist_id != null) {
				wishListhtml = '<button class="cart-button-enable cookie-wishlist wish-list-addTo wishlistLoggedIn" data-registry="wishlist">' + pdpaddtowishlist + '</button>';
			} else {
				wishListhtml = '<button data-href="' + pdpWishlistUrl + '" class="cart-button-enable registryAjaxCall wishlist wish-list-addTo wishlistAnonymous">' + pdpaddtowishlist + '</button>';

			}


			registryAndWishListHtml = registryhtml + wishListhtml;
		} else if (registryWarningIndicator === 'WARN' && skuRegistryEligibility) {

			if (typeof registry_id != 'undefined' && registry_id != '' && registry_id != null) {
				registryhtml = '<button class="cart-button-enable cookie-registry warnRegistryPopover baby-registry-addTo registryLoggedIn" data-registry="registry" data-toggle="popover" data-placement="bottom" data-content="' + addToRegistry + '">' + pdpaddtobabyregistry + '</button>';
			} else {
				registryhtml = '<button data-href="' + pdpRegistryUrl + '" class="cart-button-enable warnRegistryPopover registryAjaxCall baby-registry-addTo registryAnonymous" data-toggle="popover" data-placement="bottom" data-content="' + addToRegistry + '">' + pdpaddtobabyregistry + '</button>';
			}
			if (typeof wishlist_id != 'undefined' && wishlist_id != '' && wishlist_id != null) {
				wishListhtml = '<button class="cart-button-enable cookie-wishlist warnRegistryPopover wish-list-addTo wishlistLoggedIn" data-registry="wishlist" data-toggle="popover" data-placement="bottom" data-content="' + addToWishlist + '">' + pdpaddtowishlist + '</button>';
			} else {
				wishListhtml = '<button data-href="' + pdpWishlistUrl + '" class="cart-button-enable warnRegistryPopover registryAjaxCall wishlist wish-list-addTo wishlistAnonymous" data-toggle="popover" data-placement="bottom" data-content="' + addToWishlist + '">' + pdpaddtowishlist + '</button>';
			}
			registryAndWishListHtml = registryhtml + wishListhtml

		} else {
			registryAndWishListHtml = '<button class="cart-button-disable denyWarningRegistryPopover baby-registry-addTo">' + pdpaddtobabyregistry + '</button></div><button class="cart-button-disable denyWarningWishListPopover wish-list-addTo">' + pdpaddtowishlist + "&nbsp;" + '</button>';
		}

		$($parentDiv[index]).find('.registryButtonContainer').html(registryAndWishListHtml);
	}
	enableWarnRegistryPopover();
	enableDenyRegistryWishlistpopover();
}

function notCachedRegistryButtonsNew(productId, i) {
	var lastAccessedRegistryID = $.cookie("lastAccessedRegistryID");
	var lastAccessedWishlistID = $.cookie("lastAccessedWishlistID");
	$('#rgs_lastAccessedRegistryID').val(lastAccessedRegistryID);
	$('#rgs_lastAccessedWishlistID').val(lastAccessedWishlistID);


	var $parentDiv = $('.sticky-price-contents');
	/*if(typeof productId !='undefined' && productId != ''){
		$parentDiv = $('#quickViewId-'+productId);
	}*/
	var productType = $('#colorSizeVariantsAvailableStatus').val();
	var registryAndWishListHtml = '';
	var wishListhtml = '';
	var registryhtml = '';

	var pdpRegistryUrl = $('.pdpRegistryUrl').val();
	var pdpWishlistUrl = $('.pdpWishlistUrl').val();
	var productId = productId || getProductIds("");
	var productInfoJson = JSON.parse($('.productInfoJson-' + productId).val());
	if (productInfoJson != null) {
		if (productInfoJson.hasOwnProperty("mDefaultSKU") && productInfoJson.mDefaultSKU.hasOwnProperty("mSkuRegistryEligibility") && productInfoJson.mDefaultSKU.hasOwnProperty("mRegistryWarningIndicator")) {
			var registryWarningIndicator = productInfoJson.mDefaultSKU.mRegistryWarningIndicator;
			var skuRegistryEligibility = productInfoJson.mDefaultSKU.mSkuRegistryEligibility;
		}
	}
	//	var registryWarningIndicator = $('.registryWarningIndicator').val();
	//	var skuRegistryEligibility = $('.skuRegistryEligibility').val();

	var registry_id = $.cookie("REGISTRY_USER");
	var wishlist_id = $.cookie("myWishlistID");
	$('#rgs_registryNumber').val(registry_id);
	$('#rgs_wishlistId').val(lastAccessedWishlistID);
	var activeSkulength = $('.activeSKUsListLength').val();
	var colorLength = $('.colorLengthId').val();
	var sizeLength = $('.juvenileSizesListLengthId').val();

	if (typeof productType != 'undefined' && productType != '' && ((productType === 'noColorSizeAvailable')
		|| (productType === 'onlySizeVariants' && productInfoJson.hasOwnProperty("mJuvenileSizesList") && (productInfoJson.mJuvenileSizesList.length == '1' || productInfoJson.mJuvenileSizesList.length == '0'))
		|| (productType === 'onlyColorVariants' && productInfoJson.hasOwnProperty("mColorCodeList") && (productInfoJson.mColorCodeList.length == '1' || productInfoJson.mColorCodeList.length == '0')))) {
		if (typeof registryWarningIndicator != 'undefined' && registryWarningIndicator != '' && registryWarningIndicator === 'ALLOW' && skuRegistryEligibility) {

			if (typeof registry_id != 'undefined' && registry_id != '' && registry_id != 'null') {
				registryhtml = '<a href="javascript:void(0);" class="small-orange cart-button-enable cookie-registry" data-registry="registry">' + babyregistry + '</a>';
			} else {
				registryhtml = '<a href="' + pdpRegistryUrl + '" class="small-orange cart-button-enable registryAjaxCall">' + babyregistry + '</a>';
			}
			if (typeof wishlist_id != 'undefined' && wishlist_id != '' && wishlist_id != 'null') {
				wishListhtml = '<a href="javascript:void(0);"class="small-orange cart-button-enable cookie-wishlist" data-registry="wishlist">' + wishlist + '</a>';
			} else {
				wishListhtml = '<a href="' + pdpWishlistUrl + '" class="small-orange cart-button-enable registryAjaxCall wishlist">' + wishlist + '</a>';

			}
			registryAndWishListHtml = '<div class="add-to-text inline">' + addTo + '</div><div class="inline">' + registryhtml + "&nbsp;" + '<div class="grey-arrow-icon inline"></div><div class="inline">' + wishListhtml + "&nbsp;" + '</div><div class="grey-arrow-icon inline"></div></div>';
		} else if (registryWarningIndicator === 'WARN' && skuRegistryEligibility) {

			if (typeof registry_id != 'undefined' && registry_id != '') {
				registryhtml = '<a href="javascript:void(0);" class="small-orange cart-button-enable cookie-registry warnRegistryPopover" data-registry="registry" data-toggle="popover" data-placement="bottom" data-content="' + addToRegistry + '">' + babyregistry + '</a>';
			} else {
				registryhtml = '<a href="' + pdpRegistryUrl + '" class="small-orange cart-button-enable warnRegistryPopover registryAjaxCall" data-toggle="popover" data-placement="bottom" data-content="' + addToRegistry + '">' + babyregistry + '</a>';
			}
			if (typeof wishlist_id != 'undefined' && wishlist_id != '') {
				wishListhtml = '<a href="javascript:void(0);" class="small-orange cart-button-enable cookie-wishlist warnRegistryPopover" data-registry="wishlist" data-toggle="popover" data-placement="bottom" data-content="' + addToWishlist + '">' + wishlist + '</a>';
			} else {
				wishListhtml = '<a href="' + pdpWishlistUrl + '" class="small-orange cart-button-enable warnRegistryPopover registryAjaxCall wishlist" data-toggle="popover" data-placement="bottom" data-content="' + addToWishlist + '">' + wishlist + '</a>';
			}
			registryAndWishListHtml = '<div class="add-to-text inline">' + addTo + '</div><div class="inline">' + registryhtml + "&nbsp;" + '<div class="grey-arrow-icon inline"></div><div class="inline">' + wishListhtml + "&nbsp;" + '</div><div class="grey-arrow-icon inline"></div></div>';

		} else {
			registryAndWishListHtml = '<div class="add-to-text inline">' + addTo + '</div><div class="inline"><a href="javascript:void(0);" class="small-orange cart-button-disable denyWarningRegistryPopover">' + babyregistry + '</a></div> <div class="grey-arrow-icon inline"></div><div class="inline"><a href="javascript:void(0);" class="small-orange cart-button-disable denyWarningWishListPopover">' + wishlist + "&nbsp;" + '</a></div><div class="grey-arrow-icon inline"></div>';
		}
		$parentDiv.find('.add-to').html(registryAndWishListHtml);
	}
	enableWarnRegistryPopover();
	enableDenyRegistryWishlistpopover();
}

function disableOutOfStockMessage(inventoryStatus) {
	if (inventoryStatus != "outOfStock" && $(".sticky-price-contents .pre-order").length > 0) {
		//$(".sticky-price-contents .pre-order").hide();
	}
}

/*function ajaxCallForRegistryDetails(){
	var contextPath = $('#hiddenContextPath').val();
	$.ajax({
		type: 'POST',
		url: contextPath+'jstemplate/ajaxPdpToGetRegistryDetails.jsp',
		success: function(data){
			var response = $("<div>"+data+"</div>");
			console.log(response);R
			$('#rgs_registryNumber').val(response.find("#ajax_registry_id").val());
			$('#rgs_lastAccessedRegistryID').val(response.find("#ajax_lastAccessedRegistryID").val());
			$('#rgs_lastAccessedWishlistID').val(response.find("#ajax_lastAccessedWishlistID").val());
			$('#rgs_wishlistId').val(response.find("#ajax_wishlist_id").val());
		}
	});

}*/

function ajaxCallForPdpBreadcrumbsCookie() {

	var urlPagename = location.pathname.substring(1);
	var uri = contextPath + 'jstemplate/allCategoryIdMapDetails.jsp';
	$.ajax({
		type: 'POST',
		url: uri,
		data: { urlPagename: urlPagename },
		async: false,
		success: function (responseData) {
			//console.log('My data: ' + responseData);
			if ($.trim(responseData) != '') {
				if ($('.pdp-update-breadcrumb').length > 0) {
					$('.pdp-update-breadcrumb').remove();
				}
				$('.product-breadcrumb li:last').after(responseData);
				$('.product-breadcrumb li:first').css({ 'margin-right': '24px' });
			}
		},
		error: function () {
			console.log("Error");
		}
	});


}

function ajaxCallForInventoryAndPrice(pdpCheckFlag, recommendedQuickViewProductId) {
	var pdpCheckFlagValue = (pdpCheckFlag == "pdp") ? true : false;
	var productIds;
	var collectionId;
	var onlinePID = $('#onlinePID').val();
	if (recommendedQuickViewProductId) {
		productIds = getProductIds(recommendedQuickViewProductId);
	}
	else {
		productIds = getProductIds(pdpCheckFlagValue);
	}
	var storeLocatorFlag = (pdpCheckFlag == "storeLocator") ? true : false;
	var contextPath = $('.hiddenContextPath').val();
	var currentAjaxUrl = contextPath + 'jstemplate/ajaxPdpToGetPriceAndInventory.jsp?productIds=' + productIds;
	var locationPath = location.pathname || '';
	if (locationPath != '' && locationPath.indexOf('/collection') == 0) {
		var collectionId = getParameterByName("productId");
		currentAjaxUrl += '&collectionId=' + collectionId;
	}
	else {
		currentAjaxUrl += '&onlinePID=' + onlinePID;
	}
	if (typeof productIds != 'undefined' && productIds != '') {
		$.ajax({
			type: 'POST',
			contentType: "application/json",
			processData: false,
			url: currentAjaxUrl,
			async: false,
			success: function (data) {
				//console.log("Success");
				var $addingPDPForRecentlyViewed = $(document.createElement("div")).css("display", "none");
				$addingPDPForRecentlyViewed.html(data);
				$("#addingPDPForRecentlyViewedReplace").html($addingPDPForRecentlyViewed.find("#addingPDPForRecentlyViewed").html());

				var PromotionData = '';
				var jsonData = JSON.parse($addingPDPForRecentlyViewed.find("#pdpInventoryCheckJSON").html().trim());
				if (typeof jsonData != 'undefined' && jsonData != '') {
					if (storeLocatorFlag) {
						for (key in jsonData) {
							populateinventoryInfoToJson(jsonData[key].invenventoryAndPriceDetails, key);
							populateStoreAvailabityMessage(jsonData[key].storeAvailableMessages, key);
						}
					} else {
						for (key in jsonData) {
							populateBatteryInventoryInfoToJson(jsonData[key].batteryInventoryAndPriceDetails, key);
							populateinventoryInfoToJson(jsonData[key].invenventoryAndPriceDetails, key);
							populateStoreAvailabityMessage(jsonData[key].storeAvailableMessages, key);
							populatePromotionsInJson(jsonData[key].PromotionDetails, key);
						}
					}
				}
			},
			error: function () {
				console.log("Error");
			}
		});
	}
}

function populateBatteryInventoryInfoToJson(jsonData, productId) {
	var productInfoJson = JSON.parse($('.productInfoJson-' + productId).val());
	var allActiveSkuVariants = productInfoJson.mActiveSKUsList;
	if (typeof jsonData != 'undefined' && jsonData != '' && typeof allActiveSkuVariants != 'undefined' && allActiveSkuVariants != '') {
		for (var indx in allActiveSkuVariants) {
			/*var skuId=allActiveSkuVariants[indx].mId;*/
			var batteryJson = allActiveSkuVariants[indx].mBatteryInfoJsonVO;
			for (var batteryIndx in batteryJson) {
				var skuId = batteryJson[batteryIndx].mSkuId;
				for (var key in jsonData) {
					var jsonDataValues = jsonData[key];
					/*for(var jsonDataValuesIndx in jsonDataValues){*/
					if (jsonDataValues['skuId'] === skuId) {
						batteryJson[batteryIndx].mListPrice = jsonDataValues['listPrice'];
						batteryJson[batteryIndx].mSalePrice = jsonDataValues['salePrice'];
						batteryJson[batteryIndx].mInventoryStatus = jsonDataValues['inventoryStatusOnline'];
					}
					/*}*/
				}
			}
		}
	}
	productInfoJson.mActiveSKUsList = allActiveSkuVariants;
	$('.productInfoJson-' + productId).val(JSON.stringify(productInfoJson));
}

populateStoreAvailabityMessage = function (jsonData, productId) {
	var productInfoJson = JSON.parse($('.productInfoJson-' + productId).val());
	var allActiveSkuVariants = productInfoJson.mActiveSKUsList;
	var storeId = '';
	if (typeof jsonData != 'undefined' && jsonData != '' && typeof allActiveSkuVariants != 'undefined' && allActiveSkuVariants != '') {
		for (var indx in allActiveSkuVariants) {
			var skuId = allActiveSkuVariants[indx].mId;
			/*for (var key in jsonData) {*/
			$.each(jsonData, function (key, val) {
				if (jsonData[key]['skuId'] === skuId) {
					if (typeof jsonData[key]['inventoryStoreMessage'] != 'undefined' && jsonData[key]['inventoryStoreMessage'] != '') {
						allActiveSkuVariants[indx].mStoreAvailablityMessage = jsonData[key]['inventoryStoreMessage'];
					}
					if (typeof jsonData[key]['inventoryStoreMessage1'] != 'undefined' && jsonData[key]['inventoryStoreMessage1'] != '') {
						allActiveSkuVariants[indx].mStoreAvailablityMessage1 = jsonData[key]['inventoryStoreMessage1'];
					}
					storeId = jsonData[key]['storeId'];
				}
			});
			/*}*/
		}
	}
	productInfoJson.mActiveSKUsList = allActiveSkuVariants;
	$('.productInfoJson-' + productId).val(JSON.stringify(productInfoJson));
	replaceStoreMessageInEachProduct(productId, storeId);
}

populatePromotionsInJson = function (jsonData, productId) {
	var productInfoJson = JSON.parse($('.productInfoJson-' + productId).val());
	var allActiveSkuVariants = productInfoJson.mActiveSKUsList;
	if (typeof jsonData != 'undefined' && jsonData != '' && typeof allActiveSkuVariants != 'undefined' && allActiveSkuVariants != '') {
		var defaultSku = getDefaultSkuByInventory(allActiveSkuVariants);
		for (var indx in allActiveSkuVariants) {
			var skuId = allActiveSkuVariants[indx].mId;
			for (var key in jsonData) {
				if (jsonData[key]['skuId'] === skuId) {
					if (typeof jsonData[key]['promotions'] != 'undefined' && jsonData[key]['promotions'] != '') {
						var promotions = jsonData[key]['promotions'];
						var newpromoList = [];
						if (typeof promotions != 'undefined') {
							for (var index in promotions) {
								var newpromoListItem = {
									mPromoId: promotions[index]['promoId'],
									mPromoDescription: promotions[index]['promoName'],
									mPromoDetails: promotions[index]['promoDetails']
								}
								newpromoList.push(newpromoListItem);
							}
							allActiveSkuVariants[indx].mPromoInfoList = newpromoList;
							allActiveSkuVariants[indx].mPromotionCount = jsonData[key]['promotionCount'];
						}
					}
				}
			}
		}
		var defaultSku = getDefaultSkuByInventory(allActiveSkuVariants);

		var activeskulength = allActiveSkuVariants.length;
		var productType = productInfoJson.mProductType;
		if (activeskulength > 1 && productType === 'noColorSizeAvailable') {
			defaultSku = productInfoJson.mDefaultSKU;
		}
		productInfoJson.mDefaultSKU = defaultSku;
		productInfoJson.mActiveSKUsList = allActiveSkuVariants;
		$('.productInfoJson-' + productId).val(JSON.stringify(productInfoJson));
		populatePromotionsInUI(productId, defaultSku.mPromoInfoList, defaultSku.mPromotionCount);
	}
}

replaceStoreMessageInEachProduct = function (productId, storeId) {
	var productInfoJson = JSON.parse($('.productInfoJson-' + productId).val());
	var productType = productInfoJson.mColorSizeVariantsAvailableStatus;
	var defaultSkuId = productInfoJson.mDefaultSKU.mId;
	var allActiveSkuVariants = productInfoJson.mActiveSKUsList;
	var message = '';
	var message1 = '';
	var storeAvailableStatus = '';
	var contextPath = $('.hiddenContextPath').val();
	var onClickEvent;
	var ispu = productInfoJson.mDefaultSKU.mIspu, s2s = productInfoJson.mDefaultSKU.mS2s;
	if (typeof ispu === 'undefined' || ispu === '') {
		ispu = 'N';
	}
	if (typeof s2s === 'undefined' || s2s === '') {
		s2s = 'N';
	}
	if (typeof allActiveSkuVariants != 'undefined' && allActiveSkuVariants != '' && typeof productType != 'undefined' && productType != '' && productType == 'noColorSizeAvailable') {
		for (var indx in allActiveSkuVariants) {
			var skuIdLocal = allActiveSkuVariants[indx].mId;

			if (skuIdLocal == defaultSkuId) {
				message = allActiveSkuVariants[indx].mStoreAvailablityMessage;
				storeAvailableStatus = allActiveSkuVariants[indx].mInventoryStatusInStore;
				message1 = allActiveSkuVariants[indx].mStoreAvailablityMessage1;
				break;
			}
		}
		$('.sticky-price-contents').each(function () {
			var pid;
			if ($(this).parent().attr('class') != 'collections-sticky-price') {
				if ($(this).parent().hasClass("collectionLineItem")) {
					pid = $(this).attr('data-id') ? $(this).attr('data-id').split('-') : $(this).parent().attr('data-id').split('-');
					pid = pid[1];
				} else {
					pid = $(this).attr('id') ? $(this).attr('id').split('-') : $(this).parent().attr('id').split('-');
					pid = pid[1];
				}
			}
			if ((productId == pid)) {
				var findInStoreAvailable = ($(this).find('.find-in-store a[data-target="#findInStoreModal"]').length > 0) ? $(this).find('.find-in-store a[data-target="#findInStoreModal"]') : $(this).find('.find-in-store .findInStore');
				if ($('.product-details-template').length == 0 && $('.collection-template').length == 0) {
					if (((storeAvailableStatus == "inStock" || storeAvailableStatus == "") && !(ispu == 'N' && s2s == 'N')) || (storeAvailableStatus == "outOfStock" && s2s != 'N')) {
						if ($(this).find('.find-in-store .green-location-icon').hasClass("no-store-pickup")) {
							$(this).find('.find-in-store .green-location-icon').removeClass("no-store-pickup");
						}
						$(this).find('.find-in-store .green-location-icon').css({ 'background-position': '0px 0px' });
					}
					else {
						$(this).find('.find-in-store .green-location-icon').css({ 'background-position': '0px -202px' });
					}
				}
				else {
					if (((storeAvailableStatus == "inStock" || storeAvailableStatus == "") && !(ispu == 'N' && s2s == 'N')) || (storeAvailableStatus == "outOfStock" && s2s != 'N')) {
						$(this).find('.store-pickup-container .store-image .ICN-Store-disabled').toggleClass('ICN-Store-disabled ICN-Store');
					} else {
						$(this).find('.store-pickup-container .store-image .ICN-Store').toggleClass('ICN-Store-disabled ICN-Store');
					}
				}
				if ($('.product-details-template').length > 0 || $('.collection-template').length > 0) {
					if ((typeof message1 != 'undefined' && message1 != '') || (typeof message != 'undefined' && message != '')) {
						//onClickEvent = "javascript:return loadFindInStore('"+contextPath+"','"+defaultSkuId+"','"+"',$('.QTY-"+productId+"').val());";
						if ($('.collection-template').length > 0) {
							onClickEvent = "javascript:return loadFindInStoreForCollections('" + contextPath + "','" + productId + "','" + defaultSkuId + "',$('.collectonRefresh-" + productId + " #ItemQty').val(),this);";
						}
						else {
							onClickEvent = "javascript:return loadFindInStore('" + contextPath + "','" + defaultSkuId + "','" + "',$('#ItemQty').val());";
						}
						if (typeof storeId != 'undefined' && storeId != '') {
							var tempMessage = message1.split('|');
							tempMessage = '<span class="first-line-message">' + tempMessage.join('</span><span class="sub-lines">') + '</span>';
							$(this).find('.find-in-store .find-in-store-message .find-in-store-message-text').html(tempMessage);
						}
						else {
							tempMessage = '<span class="first-line-message">' + message + '</span>';
							$(this).find('.find-in-store .find-in-store-message .find-in-store-message-text').html(tempMessage);
						}
						/*when store is selected*/
						if (typeof storeId != 'undefined' && storeId != '') {
							if ((findInStoreAvailable.length > 0) && ((storeAvailableStatus == "inStock") || (storeAvailableStatus == "outOfStock" && s2s == 'Y'))) {
								findInStoreAvailable.html(selectAnotherStoreText).attr('onclick', onClickEvent);
							} else if ((storeAvailableStatus == "inStock") || (storeAvailableStatus == "outOfStock" && s2s == 'Y')) {
								if ($('.pdp-commerce-zone').length > 0) {
									var findStoreLink = '<span class="store-display-block inline"><a class="findinstoreAjax" data-toggle="modal" data-target="#findInStoreModal" href="javascript:void(0);" >' + selectAnotherStoreText + '</a></span>';
									$(this).find('.find-in-store .find-in-store-message').append(findStoreLink);
								} else {
									var findStoreLink = '<div class="inline"><a class="findinstoreAjax" data-toggle="modal" data-target="#findInStoreModal" href="javascript:void(0);" >' + selectAnotherStoreText + '</a></div>';
									$(this).find('.find-in-store').append(findStoreLink);
								}
								$(this).find('.find-in-store').find("a.findinstoreAjax").attr("onclick", onClickEvent);
							} else {
								var findStoreLink = '';
								if (findInStoreAvailable.length > 0) {
									findInStoreAvailable.html(findStoreLink).attr('onclick', onClickEvent);
								} else {
									$(this).find('.find-in-store .find-in-store-message [data-target="#findInStoreModal"]').text(findStoreLink);
								}
							}
						} else if (!(ispu == 'N' && s2s == 'N')) {
							if (findInStoreAvailable.length > 0) {
								findInStoreAvailable.html(findinstoreText).attr('onclick', onClickEvent);
							} else {
								if ($('.pdp-commerce-zone').length > 0) {
									var findStoreLink = '<span class="store-display-block inline"><a class="findinstoreAjax" data-toggle="modal" data-target="#findInStoreModal" href="javascript:void(0);" >' + findinstoreText + '</a></span>';
									$(this).find('.find-in-store .find-in-store-message').append(findStoreLink);
								} else {
									var findStoreLink = '<div class="inline"><a class="findinstoreAjax" data-toggle="modal" data-target="#findInStoreModal" href="javascript:void(0);" >' + findinstoreText + '</a></div>';
									$(this).find('.find-in-store').append(findStoreLink);
								}
								$(this).find('.find-in-store').find("a.findinstoreAjax").attr("onclick", onClickEvent);
							}
						} else {
							var findStoreLink = '';
							if (findInStoreAvailable.length > 0) {
								findInStoreAvailable.html(findStoreLink).attr('onclick', onClickEvent);
							} else {
								$(this).find('.find-in-store .find-in-store-message [data-target="#findInStoreModal"]').text(findStoreLink);
							}
						}
					}
				} else {
					if ($('.collection-template').length > 0) {
						onClickEvent = "javascript:return loadFindInStoreForCollections('" + contextPath + "','" + productId + "','" + defaultSkuId + "',$('.QTY-" + productId + "').val(),this);";
					}
					else {
						onClickEvent = "javascript:return loadFindInStore('" + contextPath + "','" + defaultSkuId + "','" + "',$('.QTY-" + productId + "').val());";
					}
					$(this).find('.find-in-store .find-in-store-message .find-in-store-message-text').html(message);
					/*when store is selected*/
					if (typeof storeId != 'undefined' && storeId != '') {
						if (findInStoreAvailable.length > 0) {
							findInStoreAvailable.html(selectAnotherStoreText);
						} else {
							if (!(ispu == 'N' && s2s == 'N')) {
								if ($('.pdp-commerce-zone').length > 0) {
									var findStoreLink = '<span class="store-display-block inline"><a class="findinstoreAjax" data-toggle="modal" data-target="#findInStoreModal" href="javascript:void(0);" >' + selectAnotherStoreText + '</a></span>';
									$(this).find('.find-in-store .find-in-store-message').append(findStoreLink);
								} else {
									var findStoreLink = '<div class="inline"><a class="findinstoreAjax" data-toggle="modal" data-target="#findInStoreModal" href="javascript:void(0);" >' + selectAnotherStoreText + '</a></div>';
									$(this).find('.find-in-store').append(findStoreLink);
								}
								$(this).find('.find-in-store').find("a.findinstoreAjax").attr("onclick", onClickEvent);
							}
						}
					} else if (!(ispu == 'N' && s2s == 'N')) {

						if (findInStoreAvailable.length > 0) {
							findInStoreAvailable.html(findinstoreText);
						} else {
							if ($('.pdp-commerce-zone').length > 0) {
								var findStoreLink = '<span class="store-display-block inline"><a class="findinstoreAjax" data-toggle="modal" data-target="#findInStoreModal" href="javascript:void(0);" >' + findinstoreText + '</a></span>';
								$(this).find('.find-in-store .find-in-store-message').append(findStoreLink);
							} else {
								var findStoreLink = '<div class="inline"><a class="findinstoreAjax" data-toggle="modal" data-target="#findInStoreModal" href="javascript:void(0);" >' + findinstoreText + '</a></div>';
								$(this).find('.find-in-store').append(findStoreLink);
							}
							$(this).find('.find-in-store').find("a.findinstoreAjax").attr("onclick", onClickEvent);
						}
					} else {
						var findStoreLink = '';
						if (findInStoreAvailable.length > 0) {
							findInStoreAvailable.html(selectAnotherStoreText);
						} else {
							if ($('.pdp-commerce-zone').length > 0) {
								$(this).find('.find-in-store .find-in-store-message').append(findStoreLink);
							} else {
								$(this).find('.find-in-store').append(findStoreLink);
							}
						}

					}
				}
			}

		});
	}
}

populatePromotionsInUI = function (productId, pData, promotionCount) {

	var collectionDivId = '[data-id=collectionId-' + productId + "]";
	var productDivId = '#pdpId-' + productId;
	var quickViewId = '#quickViewId-' + productId;
	var seeTermsLink = "";
	var moreSpecialOffer = '';
	if (typeof pData != 'undefined' && pData != '') {
		if ($(productDivId).length > 0 && $(collectionDivId).length <= 0) {
			var promoMessage = pData[0].mPromoDescription;
			if ($(productDivId).find('.sticky-price-contents .buy-1.promotionContainer').length > 0) {
				$(productDivId).find('.sticky-price-contents .buy-1.promotionContainer').html(promoMessage);
			} else {
				var promotionContainer = '<div class="buy-1 promotionContainer">' + promoMessage + '<div>';
				$(productDivId).find('.sticky-price-contents .commercezone-promotion-section').prepend(promotionContainer);
			}
			seeTermsLink = checkSeeTermsAvailabilityNew(pData[0]);
			if (seeTermsLink != "") {
				$(productDivId).find('.sticky-price-contents .buy-1.promotionContainer').append(seeTermsLink);
			}
			moreSpecialOffer = checkSpecialOfferAvailabilityNew(pData, promotionCount);
			if ($(productDivId).find('.moreSpecialOffer').length > 0 && moreSpecialOffer != '') {
				$(productDivId).find('.moreSpecialOffer').html(moreSpecialOffer);
			} else if (moreSpecialOffer != '') {
				var moreSpecialOfferContainer = '<div class="promotionContainer moreSpecialOffer">' + moreSpecialOffer + '<div>';
				$(productDivId).find('.sticky-price-contents .commercezone-promotion-section .buy-1.promotionContainer').after(moreSpecialOfferContainer);
			}
			else {
				$(productDivId).find('.promotionContainer.moreSpecialOffer').remove();
			}
		} else if ($(collectionDivId).length > 0) {
			$(collectionDivId).find('.deal-text').addClass("showPromotion").html(pData[0].mPromoDescription + '<span>.</span>');
			seeTermsLink = checkSeeTermsAvailability(pData[0]);
			if (seeTermsLink != "") {
				$(collectionDivId).find('.deal-text').append(seeTermsLink);
			}
		}
		else if ($(quickViewId).length > 0) {
			$(quickViewId).find(".buy-1").html(pData[0].mPromoDescription + "<span>.</span>");
			seeTermsLink = checkSeeTermsAvailability(pData[0]);
			if (seeTermsLink != "") {
				$(quickViewId).find(".buy-1").append(seeTermsLink)
			}
			moreSpecialOffer = checkSpecialOfferAvailability(pData, promotionCount);
			$(quickViewId).find(".moreSpecialOffer").html(moreSpecialOffer);
		}
		$(productDivId).find('.sticky-price-contents .commercezone-promotion-section').removeClass('hide');
	}
	else if (typeof pData == 'undefined' || pData == '') {
		$(productDivId).find('.sticky-price-contents .buy-1.promotionContainer').remove();
		$(productDivId).find('.sticky-price-contents .promotionContainer.moreSpecialOffer').remove();
	}
}

function checkSeeTermsAvailability(pData) {
	var seeTermsLink = "";
	if (typeof pData != 'undefined' && typeof pData.mPromoDetails != 'undefined' && pData.mPromoDetails != '') {
		if (pData.mPromoDetails.indexOf(window.location.protocol) == 0) {
			seeTermsLink = '<a href="' + pData.mPromoDetails + '" target="_blank" >see terms</a>'
		} else if (pData.mPromoDetails.indexOf("www") == 0) {
			seeTermsLink = '<a href="'+window.location.protocol+'//' + pData.mPromoDetails + '" target="_blank" >see terms</a>'
		} else {
			seeTermsLink = '<span class="promotionDetailsSpan displayNone">' + pData.mPromoDetails + '</span><a data-toggle="modal" data-target="#detailsModel" href="#" >see terms</a>';
		}
	}
	return seeTermsLink;
}

function checkSeeTermsAvailabilityNew(pData) {
	var seeTermsLink = "";
	if (typeof pData != 'undefined' && typeof pData.mPromoDetails != 'undefined' && pData.mPromoDetails != '') {
		if (pData.mPromoDetails.indexOf(window.location.protocol) == 0) {
			seeTermsLink = '<a class="terms-link" href="' + pData.mPromoDetails + '" target="_blank" >terms</a>'
		} else if (pData.mPromoDetails.indexOf("www") == 0) {
			seeTermsLink = '<a class="terms-link" href="'+window.location.protocol+'//' + pData.mPromoDetails + '" target="_blank" >terms</a>'
		} else {
			seeTermsLink = '<span class="promotionDetailsSpan displayNone">' + pData.mPromoDetails + '</span><a class="terms-link" href="#" >terms</a>';
		}
	}
	return seeTermsLink;
}

function checkSpecialOfferAvailabilityNew(pData, promotionCount) {
	var remainingPromoCount = promotionCount - 1;
	var viewspecialoffer = 'view <span class="promotionCount">' + remainingPromoCount + '</span> special offers';
	var offerHtml = '';
	if (promotionCount > 1) {
		offerHtml = '<div class="special-offer offer-container"><div class="special-offer-label">' + viewspecialoffer + '</div><div class="plus-sign"></div></div>';
		offerHtml += '<div class="special-offer-details">';
		for (var i = 1; i < promotionCount; i++) {
			var tmpMessage = '<div class="offer-details-line-items"><div class="offer-message">' + pData[i].mPromoDescription + '</div><div class="line-item-links">';
			var seeTermsdata = checkSeeTermsAvailabilityNew(pData[i]);
			offerHtml += tmpMessage + seeTermsdata + '</div></div>';
		}
		offerHtml += '</div>';
	}
	return offerHtml;
}

function checkSpecialOfferAvailability(pData, promotionCount) {
	var moreSpecialOffer = '';
	var remainingPromoCount = promotionCount - 1;
	if (promotionCount > 1) {
		moreSpecialOffer = '<div class="btn-group special-offer"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">'
			+ '<div class="special-offer-text"><div class="plus-sign"></div>'
			+ remainingPromoCount + ' ' + specialoffer + '</div></button><ul class="dropdown-menu" role="menu">';
		var liHtml = '';
		var i;
		for (i = 1; i < promotionCount; i++) {
			liHtml += '<li class="small-grey"><div class="inline blue-bullet"></div>&nbsp;' + pData[i].mPromoDescription + '<div class="small-blue inline">';
			var seeTermsdata = checkSeeTermsAvailability(pData[i]);
			liHtml = liHtml + '. ' + seeTermsdata + '</div></li>';
		}
		moreSpecialOffer = moreSpecialOffer + liHtml + '</ul></div>';
	}
	return moreSpecialOffer;
}

function populateinventoryInfoToJson(jsonData, productId) {
	var productInfoJson = JSON.parse($('.productInfoJson-' + productId).val());
	var allActiveSkuVariants = productInfoJson.mActiveSKUsList;
	if (typeof jsonData != 'undefined' && jsonData != '' && typeof allActiveSkuVariants != 'undefined' && allActiveSkuVariants != '') {
		var defaultSkuId = $('.defaultSkuId-' + productId).val();
		var skuFound;
		for (var indx in allActiveSkuVariants) {
			var skuId = allActiveSkuVariants[indx].mId;
			for (var key in jsonData) {
				if (jsonData[key]['skuId'] === skuId) {
					allActiveSkuVariants[indx].mAvailableInventory = jsonData[key]['inventoryLevelOnline'];
					allActiveSkuVariants[indx].mAvailableInventoryInStore = jsonData[key]['inventoryLevelStore'];
					allActiveSkuVariants[indx].mInventoryStatus = jsonData[key]['inventoryStatusOnline'];
					if (typeof jsonData[key]['inventoryStatusStore'] != 'undefined') {
						allActiveSkuVariants[indx].mInventoryStatusInStore = jsonData[key]['inventoryStatusStore'];
					}
					allActiveSkuVariants[indx].mSalePrice = jsonData[key]['salePrice'];
					allActiveSkuVariants[indx].mListPrice = jsonData[key]['listPrice'];
					allActiveSkuVariants[indx].mPriceSavingAmount = jsonData[key]['savedAmount'];
					allActiveSkuVariants[indx].mPriceSavingPercentage = jsonData[key]['savedPercentage'];
					if (typeof defaultSkuId != 'undefined' && defaultSkuId != '' && skuId === defaultSkuId) {
						skuFound = allActiveSkuVariants[indx];
					}
				}
			}
		}
	}
	var defaultSku = getDefaultSkuByInventory(allActiveSkuVariants);
	if (!$("." + productId + "-minSalePrice").length) {
		$('.productInfoJson-' + productId).parent().prepend("<input type='hidden' class='" + productId + "-minSalePrice'  />");
		$('.productInfoJson-' + productId).parent().prepend("<input type='hidden' class='" + productId + "-maxSalePrice'  />");
	}
	$("." + productId + "-minSalePrice").val(minSalePrice);
	$("." + productId + "-maxSalePrice").val(maxSalePrice);

	minSalePrice = undefined;
	maxSalePrice = undefined;
	var activeskulength = allActiveSkuVariants.length;
	var productType = productInfoJson.mProductType;
	if (activeskulength > 1 && productType === 'noColorSizeAvailable' && typeof skuFound != 'undefined') {
		defaultSku = skuFound;
	} else if (activeskulength > 1 && productType === 'noColorSizeAvailable') {
		defaultSku = productInfoJson.mDefaultSKU;
	}
	productInfoJson.mDefaultSKU = defaultSku;
	productInfoJson.mActiveSKUsList = allActiveSkuVariants;
	$('.productInfoJson-' + productId).val(JSON.stringify(productInfoJson));

}

/* END: AJAX call  for getting the real time inventory and price */

//$('.x-image').click(function () {
$(document).on('click', '.x-image', function () {
	$('.pdp-shopping-cart-overlay-container').removeClass('in');
	$('#product-disabled-modal').hide();
	progressBar();
});

//$('.add-to-cart').click(function () {
$(document).on('click', '.add-to-cart', function () {
	//setTimeout(function () {
	var scrollWindow = $('.shopping-cart-overlay .left .tse-scrollable');
	scrollWindow.parent().height(332);
	scrollWindow.TrackpadScrollEmulator({ autoHide: false });
	scrollWindow.width(scrollWindow.parent().width());
	scrollWindow.height(scrollWindow.parent().height());
	scrollWindow.find('.tse-scroll-content').height(scrollWindow.height());
	//}, 5);
});

/* Start: Product details page variance selection */

function sendColorSizeCode(pageName, variantName, _this) {
	var productId = '';
	var contextPath = $('.hiddenContextPath').val();
	var parentId = '';
	var breadcrumbData = $('.product-breadcrumb-row').html();
	var cmsData = $('#product-main-content').find('.top-product-details-content').find('div:first-child').html();

	if (pageName == 'pdpPage') {
		parentId = 'pdpId';
		productId = $('#hiddenProductId').val();
	} else if (pageName == 'quickView') {
		parentId = 'quickViewId';
		productId = $('#hiddenProductIdQuickView').val();
	} else if (pageName == 'collectionPage') {
		var id = _this.parents('.collectionLineItem').attr('data-id') ? (_this.parents('.collectionLineItem').attr('data-id')).split('-') : (_this.parents('.collectionLineItem').parent().attr('data-id')).split('-');
		parentId = id[0];
		productId = id[1];
	}
	var colorCode = $('#' + parentId + '-' + productId).length ? $('#' + parentId + '-' + productId + ' .color-block.color-selected .color-block-inner').attr('id') : $('[data-id=' + parentId + '-' + productId + '] .color-block.color-selected .color-block-inner').attr('id');
	var sizeCode = $('#' + parentId + '-' + productId).length ? $('#' + parentId + '-' + productId + ' .size-option.size-selected .content').attr('data-id') : $('[data-id=' + parentId + '-' + productId + '] .size-option.size-selected .content').attr('data-id');
	colorCode = (colorCode) ? colorCode : '';
	sizeCode = (sizeCode) ? sizeCode : '';

	var customerImagePId = $('#customerImagePId').val() || '';

	var productInfoJson = JSON.parse($('.productInfoJson-' + productId).val());
	var defaultSku = getDefaultSku(productId, colorCode, sizeCode, productInfoJson, variantName, _this);
	addtorCartRefresh(defaultSku, colorCode, sizeCode, productId, _this);
	priceAndImageChangesPDP(defaultSku, _this, pageName, productInfoJson, variantName, colorCode, sizeCode);
	/*if(pageName == 'pdpPage'){
	populateVendorDemoUrl(defaultSku);
	}*/
	//console.log(_this.parents('[id$="' + productId + '"]'), _this.parent('[id$="' + productId + '"]'));
	generatreProductDetailBySkuID(productId, defaultSku, _this.parents('[id$="' + productId + '"]'));
	resistryAndWishListChange(productInfoJson.mColorSizeVariantsAvailableStatus, colorCode, sizeCode, _this, defaultSku);
	populatePromotionsInUI(productId, defaultSku.mPromoInfoList, defaultSku.mPromotionCount);
	if ($('.collectionLineItem').length > 0 && pageName != 'quickView') {
		selectedColor = colorCode ? true : false;
		selectedSize = sizeCode ? true : false;
		$(_this).parents('.collectionLineItem').find('.cp-collectionQty .cp-collectionItemDetails').val(productId + '-' + defaultSku.mId);
		if (selectedSize || selectedColor) { }
		else {
			_this.parents('.collectionLineItem').find('.cp-decrease').addClass('disabled');
			_this.parents('.collectionLineItem').find('.QTY-' + productId).val(0);
			selectedSize = false;
			selectedColor = false;
		}
		/*if(defaultSku.mInventoryStatus=="outOfStock" ){
			_this.parents('.collectionLineItem').find('.cp-decrease').removeClass('enabled').addClass('disabled');
			_this.parents('.collectionLineItem').find('.cp-increase').removeClass('enabled').addClass('disabled');
			_this.parents('.collectionLineItem').find('.QTY-' + productId).attr('disabled','disabled');
		}
		else{
			_this.parents('.collectionLineItem').find('.cp-increase').removeClass('disabled').addClass('enabled');
			if(parseInt(_this.parents('.collectionLineItem').find('.QTY-' + productId).val())==0){
				_this.parents('.collectionLineItem').find('.cp-decrease').removeClass('enabled').addClass('disabled');
			}
			_this.parents('.collectionLineItem').find('.QTY-' + productId).removeAttr('disabled','disabled');
		}*/
		//modified for fixing stylized
		if (((defaultSku.mRegistryWarningIndicator === 'DENY' && defaultSku.mSkuRegistryEligibility) || (defaultSku.mRegistryWarningIndicator === 'WARN' && !defaultSku.mSkuRegistryEligibility) || (defaultSku.mRegistryWarningIndicator === 'ALLOW' && !defaultSku.mSkuRegistryEligibility)) && defaultSku.mInventoryStatus == 'outOfStock') {
			if (defaultSku.mInventoryStatusInStore) {
				if (defaultSku.mInventoryStatusInStore == "outOfStock") {
					_this.parents('.collectionLineItem').find('.QTY-' + productId).attr('disabled', 'disabled').removeAttr('enabled');
					_this.parents('.collectionLineItem').find('.cp-increase').removeClass('enabled').addClass('disabled');
					_this.parents('.collectionLineItem').find('.cp-decrease').removeClass('enabled').addClass('disabled');
				}
				else {
					_this.parents('.collectionLineItem').find('.QTY-' + productId).removeAttr('disabled').attr('enabled', 'enabled');
					_this.parents('.collectionLineItem').find('.cp-increase').removeClass('disabled').addClass('enabled');
					if (parseInt(_this.parents('.collectionLineItem').find('.QTY-' + productId).val()) == 0) {
						_this.parents('.collectionLineItem').find('.cp-decrease').removeClass('enabled').addClass('disabled');
					}
				}
			}
			else {
				_this.parents('.collectionLineItem').find('.QTY-' + productId).removeAttr('enabled').attr('disabled', 'disabled');
				_this.parents('.collectionLineItem').find('.cp-increase').removeClass('enabled').addClass('disabled');
				if (parseInt(_this.parents('.collectionLineItem').find('.QTY-' + productId).val()) == 0) {
					_this.parents('.collectionLineItem').find('.cp-decrease').removeClass('enabled').addClass('disabled');
				}
			}

		}
		else {
			_this.parents('.collectionLineItem').find('.cp-increase').removeClass('disabled').addClass('enabled');
			if (parseInt(_this.parents('.collectionLineItem').find('.QTY-' + productId).val()) == 0) {
				_this.parents('.collectionLineItem').find('.cp-decrease').removeClass('enabled').addClass('disabled');
			}
			_this.parents('.collectionLineItem').find('.QTY-' + productId).removeAttr('disabled', 'disabled');
		}
		//collectionAddToCartRefreshForStylyzedItem(defaultSku,_this);

	}
	// notAvaialbeTooltip();
	// notCarriedTooltip();
	if (typeof isPopulateShippingMessage != 'undefined' && isPopulateShippingMessage != 'null' && isPopulateShippingMessage != '' && isPopulateShippingMessage == 'true') {
		populateShippingMessage(defaultSku, _this.parents('.sticky-price-contents'));
	}
}

function sendColorSizeCodeNew(pageName, variantName, _this) {
	var productId = '';
	var contextPath = $('.hiddenContextPath').val();
	var parentId = '';
	var breadcrumbData = $('.product-breadcrumb-row').html();
	var cmsData = $('#product-main-content').find('.top-product-details-content').find('div:first-child').html();

	if (pageName == 'pdpPage') {
		parentId = 'pdpId';
		productId = $('#hiddenProductId').val();
	} else if (pageName == 'quickView') {
		parentId = 'quickViewId';
		productId = $('#hiddenProductIdQuickView').val();
	} else if (pageName == 'collectionPage') {
		var id = _this.parents('.collectionLineItem').attr('data-id') ? (_this.parents('.collectionLineItem').attr('data-id')).split('-') : (_this.parents('.collectionLineItem').parent().attr('data-id')).split('-');
		parentId = id[0];
		productId = id[1];
	}
	var colorCode = $('#' + parentId + '-' + productId).length ? $('#' + parentId + '-' + productId + ' .color-block.color-selected .color-block-inner').attr('id') : $('[data-id=' + parentId + '-' + productId + '] .color-block.color-selected .color-block-inner').attr('id');
	//var sizeCode  = $('#' + parentId + '-' + productId).length? $('#' + parentId + '-' + productId + ' .size-option.size-selected .content').attr('data-id') : $('[data-id=' + parentId + '-' + productId + '] .size-option.size-selected .content').attr('data-id');
	var sizeCode = $('#' + parentId + '-' + productId).length ? $('#' + parentId + '-' + productId + ' .swatches.selected-swatch').attr('data-id') : $('[data-id=' + parentId + '-' + productId + '] .swatches.selected-swatch').attr('data-id');
	colorCode = (colorCode) ? colorCode : '';
	sizeCode = (sizeCode) ? sizeCode : '';

	var customerImagePId = $('#customerImagePId').val() || '';

	var productInfoJson = JSON.parse($('.productInfoJson-' + productId).val());
	var defaultSku = getDefaultSkuNew(productId, colorCode, sizeCode, productInfoJson, variantName, _this);
	addtorCartRefreshNew(defaultSku, colorCode, sizeCode, productId, _this);
	priceAndImageChangesPDP(defaultSku, _this, pageName, productInfoJson, variantName, colorCode, sizeCode);
	/*if(pageName == 'pdpPage'){
	populateVendorDemoUrl(defaultSku);
	}*/
	//console.log(_this.parents('[id$="' + productId + '"]'), _this.parent('[id$="' + productId + '"]'));
	generatreProductDetailBySkuID(productId, defaultSku, _this.parents('[id$="' + productId + '"]'));
	registryAndWishListChangeNew(productInfoJson.mColorSizeVariantsAvailableStatus, colorCode, sizeCode, _this, defaultSku);
	populatePromotionsInUI(productId, defaultSku.mPromoInfoList, defaultSku.mPromotionCount);
	if ($('.collectionLineItem').length > 0 && pageName != 'quickView') {
		selectedColor = colorCode ? true : false;
		selectedSize = sizeCode ? true : false;
		$(_this).parents('.collectionLineItem').find('.cp-collectionQty .cp-collectionItemDetails').val(productId + '-' + defaultSku.mId);
		if (selectedSize || selectedColor) { }
		else {
			_this.parents('.collectionLineItem').find('.cp-decrease').addClass('disabled');
			_this.parents('.collectionLineItem').find('.QTY-' + productId).val(0);
			selectedSize = false;
			selectedColor = false;
		}
		//modified for fixing stylized
		if (((defaultSku.mRegistryWarningIndicator === 'DENY' && defaultSku.mSkuRegistryEligibility) || (defaultSku.mRegistryWarningIndicator === 'WARN' && !defaultSku.mSkuRegistryEligibility) || (defaultSku.mRegistryWarningIndicator === 'ALLOW' && !defaultSku.mSkuRegistryEligibility)) && defaultSku.mInventoryStatus == 'outOfStock') {
			if (defaultSku.mInventoryStatusInStore) {
				if (defaultSku.mInventoryStatusInStore == "outOfStock") {
					_this.parents('.collectionLineItem').find('.QTY-' + productId).attr('disabled', 'disabled').removeAttr('enabled');
					_this.parents('.collectionLineItem').find('.cp-increase').removeClass('enabled').addClass('disabled');
					_this.parents('.collectionLineItem').find('.cp-decrease').removeClass('enabled').addClass('disabled');
				}
				else {
					_this.parents('.collectionLineItem').find('.QTY-' + productId).removeAttr('disabled').attr('enabled', 'enabled');
					_this.parents('.collectionLineItem').find('.cp-increase').removeClass('disabled').addClass('enabled');
					if (parseInt(_this.parents('.collectionLineItem').find('.QTY-' + productId).val()) == 0) {
						_this.parents('.collectionLineItem').find('.cp-decrease').removeClass('enabled').addClass('disabled');
					}
				}
			}
			else {
				_this.parents('.collectionLineItem').find('.QTY-' + productId).removeAttr('enabled').attr('disabled', 'disabled');
				_this.parents('.collectionLineItem').find('.cp-increase').removeClass('enabled').addClass('disabled');
				if (parseInt(_this.parents('.collectionLineItem').find('.QTY-' + productId).val()) == 0) {
					_this.parents('.collectionLineItem').find('.cp-decrease').removeClass('enabled').addClass('disabled');
				}
			}

		}
		else {
			_this.parents('.collectionLineItem').find('.cp-increase').removeClass('disabled').addClass('enabled');
			if (parseInt(_this.parents('.collectionLineItem').find('.QTY-' + productId).val()) == 0) {
				_this.parents('.collectionLineItem').find('.cp-decrease').removeClass('enabled').addClass('disabled');
			}
			_this.parents('.collectionLineItem').find('.QTY-' + productId).removeAttr('disabled', 'disabled');
		}
		//collectionAddToCartRefreshForStylyzedItem(defaultSku,_this);

	}
	// notAvaialbeTooltip();
	// notCarriedTooltip();
	if (typeof isPopulateShippingMessage != 'undefined' && isPopulateShippingMessage != 'null' && isPopulateShippingMessage != '' && isPopulateShippingMessage == 'true') {
		populateShippingMessage(defaultSku, _this.parents('.sticky-price-contents'));
	}
}

function collectionAddToCartRefreshForStylyzedItem(defaultSku, _this) {
	var registryWarningIndicator = defaultSku.mRegistryWarningIndicator;
	var skuRegistryEligibility = defaultSku.mSkuRegistryEligibility;
	var qtyVal = _this.parents('.collectionLineItem').find(".cp-item-qty").val();
	if (qtyVal > 0) {
		makeAjaxCollectionQuantityUpdate(_this.parents('.collectionLineItem').find(".stepper.cp-collectionQty"));
	}
	setAddToCartAddToRegistryButtonState(registryWarningIndicator, skuRegistryEligibility, qtyVal);
}

function getDefaultSku(productId, colorCode, sizeCode, productInfoJson, variantName, _this) {

	var allActiveSkuVariants = productInfoJson.mActiveSKUsList;
	var sameSizeList = [];
	var sameColorList = [];
	var defaultSku;
	var colorMap = new Object(); // or var map = {};
	var sizeMap = new Object(); // or var map = {};


	/* Change for assorted items */
	//debugger;
	if (productInfoJson.mProductType === "noColorSizeAvailable" && allActiveSkuVariants.length > 0) {
		return productInfoJson.mDefaultSKU;
	}
	/* change for assorted items */

	$.each(allActiveSkuVariants, function (index, obj) {

		if (obj.mColorCode != 'undefined' && obj.mColorCode != '' && obj.mColorCode === colorCode && colorCode != 'undefined' && colorCode != '') {
			sameColorList.push(obj);
		}
		if (obj.mJuvenileSize != 'undefined' && obj.mJuvenileSize != '' && obj.mJuvenileSize === sizeCode && sizeCode != 'undefined' && sizeCode != '') {
			sameSizeList.push(obj);
		}

	});
	if (typeof sameColorList != 'undefined' && sameColorList != '') {
		productInfoJson.mSelectedColorSKUsList = sameColorList;
	}
	if (typeof sameSizeList != 'undefined' && sameSizeList != '') {
		productInfoJson.mSelectedSizeSKUsList = sameSizeList;
	}


	if (productInfoJson.mProductType === 'bothColorSizeAvailable') {
		if (colorCode != 'undefined' && colorCode != '') {
			colorMap[colorCode] = sameColorList;

		}
		if (sizeCode != 'undefined' && sizeCode != '') {
			sizeMap[sizeCode] = sameSizeList;
		}
	}
	if (colorCode != 'undefined' && colorCode != '' && sizeCode != 'undefined' && sizeCode != '') {
		if (variantName === 'color') {
			for (var key in sameColorList) {
				if (sameColorList[key].mJuvenileSize === sizeCode) {
					defaultSku = sameColorList[key];
					break;
				} else {
					defaultSku = sameColorList[0];
				}
			}

		}
		if (variantName === 'size') {
			for (var key in sameSizeList) {
				if (sameSizeList[key].mColorCode === colorCode) {
					defaultSku = sameSizeList[key];
					break;
				} else {
					defaultSku = sameSizeList[0];
				}
			}

		}
	} else if (colorCode != 'undefined' && colorCode != '') {
		defaultSku = getDefaultSkuByInventory(sameColorList);
	} else if (sizeCode != 'undefined' && sizeCode != '') {
		defaultSku = getDefaultSkuByInventory(sameSizeList);
	}
	//fade out functionality on selection
	if (!addToCartCloseFlag) {
		fadeOut(sameColorList, sameSizeList, productInfoJson.mActiveSKUsList, variantName, _this);
	}
	return defaultSku;
}

function getDefaultSkuNew(productId, colorCode, sizeCode, productInfoJson, variantName, _this) {

	var allActiveSkuVariants = productInfoJson.mActiveSKUsList;
	var sameSizeList = [];
	var sameColorList = [];
	var defaultSku;
	var colorMap = new Object(); // or var map = {};
	var sizeMap = new Object(); // or var map = {};


	/* Change for assorted items */
	//debugger;
	if (productInfoJson.mProductType === "noColorSizeAvailable" && allActiveSkuVariants.length > 0) {
		return productInfoJson.mDefaultSKU;
	}
	/* change for assorted items */

	$.each(allActiveSkuVariants, function (index, obj) {

		if (obj.mColorCode != 'undefined' && obj.mColorCode != '' && obj.mColorCode === colorCode && colorCode != 'undefined' && colorCode != '') {
			sameColorList.push(obj);
		}
		if (obj.mJuvenileSize != 'undefined' && obj.mJuvenileSize != '' && obj.mJuvenileSize === sizeCode && sizeCode != 'undefined' && sizeCode != '') {
			sameSizeList.push(obj);
		}

	});
	if (typeof sameColorList != 'undefined' && sameColorList != '') {
		productInfoJson.mSelectedColorSKUsList = sameColorList;
	}
	if (typeof sameSizeList != 'undefined' && sameSizeList != '') {
		productInfoJson.mSelectedSizeSKUsList = sameSizeList;
	}


	if (productInfoJson.mProductType === 'bothColorSizeAvailable') {
		if (colorCode != 'undefined' && colorCode != '') {
			colorMap[colorCode] = sameColorList;

		}
		if (sizeCode != 'undefined' && sizeCode != '') {
			sizeMap[sizeCode] = sameSizeList;
		}
	}
	if (colorCode != 'undefined' && colorCode != '' && sizeCode != 'undefined' && sizeCode != '') {
		if (variantName === 'color') {
			for (var key in sameColorList) {
				if (sameColorList[key].mJuvenileSize === sizeCode) {
					defaultSku = sameColorList[key];
					break;
				} else {
					defaultSku = sameColorList[0];
				}
			}

		}
		if (variantName === 'size') {
			for (var key in sameSizeList) {
				if (sameSizeList[key].mColorCode === colorCode) {
					defaultSku = sameSizeList[key];
					break;
				} else {
					defaultSku = sameSizeList[0];
				}
			}

		}
	} else if (colorCode != 'undefined' && colorCode != '') {
		defaultSku = getDefaultSkuByInventory(sameColorList);
	} else if (sizeCode != 'undefined' && sizeCode != '') {
		defaultSku = getDefaultSkuByInventory(sameSizeList);
	}
	//fade out functionality on selection
	if (!addToCartCloseFlag) {
		fadeOutNew(sameColorList, sameSizeList, productInfoJson.mActiveSKUsList, variantName, _this);
	}
	return defaultSku;
}

priceAndImageChangesPDP = function (defaultSku, _this, pageName, productInfoJson, variantName, colorCode, sizeCode) {

	var quickViewPrimaryImageBlock = $('#quickViewPrimaryImageBlock').val() || "";
	var primaryMainImageBlock = $('#primaryMainImageBlock').val() || "";
	var zoomPrimaryMainImageBlock = $('#zoomPrimaryMainImageBlock').val() || "";
	var thumbnailResize = $('#quickViewAlt1ImageBlock').val() ? $('#quickViewAlt1ImageBlock').val() : $('#alternateImageThumbnail1Block').val();//"?resize=50:50";
	var $colorSizeChangingOption = $(_this).closest('.sticky-price-contents');
	var $collectionImageChange = $(_this).closest('.collection-product-block');

	var isPriceChangeEnable = isPriceChangeEnableMethod(productInfoJson, variantName, colorCode, sizeCode, defaultSku);
	var isImageChangeEnable = isImageChangeEnableMethod(productInfoJson, variantName, colorCode, sizeCode, defaultSku);
	if (isPriceChangeEnable) {
		populatePrice(defaultSku, $colorSizeChangingOption, productInfoJson, colorCode, sizeCode);
	}

	if (isImageChangeEnable) {
		populateImages(defaultSku, productInfoJson.mProductId, pageName, $collectionImageChange);
	}
}

/*function populateVendorDemoUrl(defaultSku){
	var productDemoUrl = defaultSku.mVendorsProductDemoUrl;
	if(typeof productDemoUrl != "undefined" && $.trim(productDemoUrl) != '')
	{
		$(".product-tour-container").attr("onclick","window.open( '"+defaultSku.mVendorsProductDemoUrl+"', '','height=450,width=670' ); return false");
		$(".product-tour-container").show();
	}else{
			$(".product-tour-container").hide();
		}
}*/

function isCanonicalUrlHavingSpecChar(image) {
	var canonicalImageUrl = '';
	if (typeof image != 'undefined' || image != '') {
		if ((/^[a-zA-Z0-9- . : \/]*$/.test(image)) == false) {
			canonicalImageUrl = encodeURI(image);
		} else {
			canonicalImageUrl = image;
		}
	}
	return canonicalImageUrl;
}

function populateImages(defaultSku, pProductId, pageName, $collectionImageChange) {
	var quickViewPrimaryImageBlock = $('#quickViewPrimaryImageBlock').val() || "";
	var primaryMainImageBlock = $('#primaryMainImageBlock').val() || "";
	var zoomPrimaryMainImageBlock = $('#zoomPrimaryMainImageBlock').val() || "";
	var thumbnailResize = $('#quickViewAlt1ImageBlock').val() ? $('#quickViewAlt1ImageBlock').val() : $('#alternateImageThumbnail1Block').val();//"?resize=50:50";
	var contextPath = $('.hiddenContextPath').val();
	var canonicalPrimaryImage = defaultSku.mPrimaryCanonicalImage;
	var canonicalSecondaryImage = defaultSku.mSecondaryCanonicalImage;
	var beforeAddToCartCloseFlag = addToCartCloseFlag;
	if (canonicalImageUrl) {
		var primaryImage = isCanonicalUrlHavingSpecChar(canonicalPrimaryImage);
		var secondaryImage = isCanonicalUrlHavingSpecChar(canonicalSecondaryImage);
	}
	/*var $collectionImageChange = $('#collectonRefresh-'+pProductId);*/
	if (typeof primaryImage === 'undefined' || primaryImage === '') {
		if (typeof akamaiNoImageURL != 'undefined' && akamaiNoImageURL != '') {
			primaryImage = akamaiNoImageURL;
		} else {
			primaryImage = contextPath + 'images/no-image500.gif';
		}
	}
	if ($collectionImageChange.length > 0) {
		var collectionPrimaryImage = defaultSku.mPrimaryImage;
		if (typeof collectionPrimaryImage === 'undefined' || collectionPrimaryImage === '') {
			if (typeof akamaiNoImageURL != 'undefined' && akamaiNoImageURL != '') {
				collectionPrimaryImage = akamaiNoImageURL;
			} else {
				collectionPrimaryImage = contextPath + 'images/no-image500.gif';
			}
		}
		$collectionImageChange.find('.collection-image').attr('src', collectionPrimaryImage + primaryMainImageBlock);
		$collectionImageChange.find(".productSkuId").val(pProductId + "-" + defaultSku.mId);

	}
	if (pageName === 'quickView') {
		populateQuickViewImages(defaultSku, pProductId, pageName);
	}
	else if (typeof defaultSku.mAlterNateCanonicalImages != 'undefined' && defaultSku.mAlterNateCanonicalImages != '') {
		var imageElement = [];
		var imageElementGallery = [];
		//for (var imgIndex in defaultSku.mAlterNateCanonicalImages) {
		for (var imgIndex = 0, imgIndexLength = defaultSku.mAlterNateCanonicalImages.length; imgIndex < imgIndexLength; imgIndex++) {
			imageElement.push('<a class="block-add" href="javascript:void(0);"><img class="uploadImage" title="alternate image thumbnail" src=' + defaultSku.mAlterNateCanonicalImages[imgIndex] + thumbnailResize + '></a>');
			if (imgIndex === '3' && (typeof secondaryImage == 'undefined' || secondaryImage == '' || secondaryImage == null)) {
				break;
			}
			else if (imgIndex === '2' && (typeof secondaryImage != 'undefined' || secondaryImage != '' || secondaryImage != null)) {
				break;
			}
		}
		var imgCount = secondaryImage ? 5 : 4;
		//for (var imgIndexGa in defaultSku.mAlterNateCanonicalImages) {
		for (var imgIndexGa = 0, imgIndexGaLength = defaultSku.mAlterNateCanonicalImages.length; imgIndexGa < imgIndexGaLength; imgIndexGa++) {
			imageElementGallery.push('<img id="gallery-image-' + imgCount + '" class="pdpimages gallery-image-unselected" src=' + defaultSku.mAlterNateCanonicalImages[imgIndexGa] + thumbnailResize + ' onClick="javascript:hideVideoFromOverlay()" title="alternate image thumbnail" >');
			if (imgIndexGa === '3' && (typeof secondaryImage == 'undefined' || secondaryImage == '' || secondaryImage == null)) {
				break;
			}
			else if (imgIndexGa === '2' && (typeof secondaryImage != 'undefined' || secondaryImage != '' || secondaryImage != null)) {
				break;
			}
			imgCount++;
		}

		$('.product-hero-area').find('#hero-image').attr('src', primaryImage + primaryMainImageBlock);// pdp
		$('.product-hero-area').find('#hero-image1').attr('src', primaryImage);// pdp Zoom Image
		$('.product-hero-area').find('.zoomimage').attr('src', primaryImage + zoomPrimaryMainImageBlock);//overlay
		$('.product-hero-area').find('#gallery-image-3').attr('src', primaryImage + thumbnailResize); //overlay
		$('.product-details').find('#firstAlternate').attr('src', primaryImage + thumbnailResize);
		$('.product-details').find('#alternatePDPImg').html(imageElement);
		//$('.product-hero-area').find('.alt-images-3').parents().find('.pdp-image-gallert-overlay').find('.productImages').find('img').not('.alt-images-3').remove();//overlay
		//$('.product-hero-area').find('.alt-images-3').after(imageElementGallery);// overlay
		if (typeof secondaryImage != 'undefined' && secondaryImage != '' && secondaryImage != null) {
			//$('img[id="gallery-image-3"],img[id="gallery-image-4"]').siblings().remove();
			$('.productImages img:not([id="gallery-image-3"],[id="gallery-image-4"])').remove();
			$('.product-details').find('#secondAlternate').attr('src', secondaryImage + thumbnailResize);
			$('.productImages #gallery-image-4').attr('src', secondaryImage + thumbnailResize);
			$('.product-details').find('#secondAlternate').show();
			$('.productImages #gallery-image-4').after(imageElementGallery);
		}
		else {
			$('.productImages img[id!="gallery-image-3"]').remove();
			$('.productImages #gallery-image-3').after(imageElementGallery);
			$('.product-details').find('#secondAlternate').hide();
		}
	}

	else {
		$('.product-hero-area').find('#hero-image').attr('src', primaryImage + primaryMainImageBlock);// pdp
		$('.product-hero-area').find('#hero-image1').attr('src', primaryImage);// pdp Zoom Image
		$('.product-details').find('#firstAlternate').attr('src', primaryImage + thumbnailResize);
		$('.product-hero-area').find('#alternatePDPImg').html('');
		$('.product-details').find('#alternatePDPImg').html('');
		$('.product-hero-area').find('.zoomimage').attr('src', primaryImage + zoomPrimaryMainImageBlock);//overlay
		$('.product-hero-area').find('#gallery-image-3').attr('src', primaryImage + thumbnailResize); //overlay
		if (typeof secondaryImage != 'undefined' && secondaryImage != '' && secondaryImage != null) {
			$('.product-details').find('#secondAlternate').attr('src', secondaryImage + thumbnailResize);
			$('.productImages img:not([id="gallery-image-3"],[id="gallery-image-4"])').remove();
			$('.product-hero-area').find('#gallery-image-4').attr('src', secondaryImage + thumbnailResize);
			$('.product-details').find('#secondAlternate').show();
		}
		else {
			$('.productImages img[id!="gallery-image-3"]').remove();
			$('.product-details').find('#secondAlternate').hide();
		}
	}

	if ($('.product-details-template').length > 0 && !beforeAddToCartCloseFlag) {
		thumbnailCarousel();
	}
}

function populateQuickViewImages(defaultSku, pProductId, pageName) {
	var quickViewPrimaryImageBlock = $('#quickViewPrimaryImageBlock').val() || "";
	var primaryMainImageBlock = $('#primaryMainImageBlock').val() || "";
	var zoomPrimaryMainImageBlock = $('#zoomPrimaryMainImageBlock').val() || "";
	var thumbnailResize = $('#quickViewAlt1ImageBlock').val() ? $('#quickViewAlt1ImageBlock').val() : $('#alternateImageThumbnail1Block').val();//"?resize=50:50";
	var contextPath = $('.hiddenContextPath').val();
	var quickViewPrimaryImage = defaultSku.mPrimaryImage;
	var quickViewSecondaryImage = defaultSku.mSecondaryImage;
	if (typeof quickViewPrimaryImage === 'undefined' || quickViewPrimaryImage === '') {
		if (typeof akamaiNoImageURL != 'undefined' && akamaiNoImageURL != '') {
			quickViewPrimaryImage = akamaiNoImageURL;
		} else {
			quickViewPrimaryImage = contextPath + 'images/no-image500.gif';
		}
	}

	if (typeof defaultSku.mAlterNateImages != 'undefined' && defaultSku.mAlterNateImages != '') {
		var imageElement = [];
		var imageElementGallery = [];
		for (var imgIndex in defaultSku.mAlterNateImages) {
			imageElement.push('<a class="block-add" href="javascript:void(0);"><img class="uploadImage" title="alternate image thumbnail" src=' + defaultSku.mAlterNateImages[imgIndex] + thumbnailResize + '></a>');
			if (imgIndex === '1' && (typeof quickViewSecondaryImage == 'undefined' || quickViewSecondaryImage == '' || quickViewSecondaryImage == null)) {
				break;
			}
			else if (imgIndex === '0' && (typeof quickViewSecondaryImage != 'undefined' || quickViewSecondaryImage != '' || quickViewSecondaryImage != null)) {
				break;
			}
		}
		var imgCount = quickViewSecondaryImage ? 5 : 4;
		for (var imgIndexGa in defaultSku.mAlterNateImages) {
			imageElementGallery.push('<img id="gallery-image-' + imgCount + '" class="pdpimages gallery-image-unselected" src=' + defaultSku.mAlterNateImages[imgIndexGa] + thumbnailResize + ' onClick="javascript:hideVideoFromOverlay()" title="alternate image thumbnail" >');
			if (imgIndexGa === '3' && (typeof quickViewSecondaryImage == 'undefined' || quickViewSecondaryImage == '' || quickViewSecondaryImage == null)) {
				break;
			}
			else if (imgIndexGa === '2' && (typeof quickViewSecondaryImage != 'undefined' || quickViewSecondaryImage != '' || quickViewSecondaryImage != null)) {
				break;
			}
			imgCount++;
		}
		$('.quickview-block').find('#hero-image').attr('src', quickViewPrimaryImage + quickViewPrimaryImageBlock);
		$('.quickview-footer').find('.firstAlternate').attr('src', quickViewPrimaryImage + thumbnailResize);
		$('.quickview-footer').find('.secondAlternate').hide();
		if (typeof quickViewSecondaryImage != 'undefined' && quickViewSecondaryImage != '' && quickViewSecondaryImage != null) {
			$('.quickview-footer').find('.secondAlternate').attr('src', quickViewSecondaryImage + thumbnailResize);
			$('.quickview-footer').find('.secondAlternate').show();
		}
		$('.quickview-footer').find('.alternatePDPImg').html(imageElement);

	}
	else {
		$('.quickview-block').find('#hero-image').attr('src', quickViewPrimaryImage + quickViewPrimaryImageBlock);
		$('.quickview-footer').find('.firstAlternate').attr('src', quickViewPrimaryImage + thumbnailResize);
		$('.quickview-footer').find('.secondAlternate').hide();
		if (typeof quickViewSecondaryImage != 'undefined' && quickViewSecondaryImage != '' && quickViewSecondaryImage != null) {
			$('.quickview-footer').find('.secondAlternate').attr('src', quickViewSecondaryImage + thumbnailResize);
			$('.quickview-footer').find('.secondAlternate').show();
		}
		$('.quickview-footer').find('.alternatePDPImg').html('');
	}
}

function populatePrice(defaultSku, $colorSizeChangingOption, productInfoJson, colorCode, sizeCode) {
	var pminSalePrice = parseFloat($("." + productInfoJson.mProductId + "-minSalePrice").val());
	var pmaxSalePrice = parseFloat($("." + productInfoJson.mProductId + "-maxSalePrice").val());
	var cartCookie = $.cookie("cartCookie");
	var addedInCart = false;
	var batteryProductSkuId;
	var batteryItemAddedInCart = false;
	var batteryProductIndex;
	var batteryProductPrice = "";
	if (typeof cartCookie != 'undefined' && cartCookie != '' && cartCookie != null) {
		var cartElement = cartCookie.split('~').length;
		for (var indx = 0; indx < cartElement; indx++) {
			var _TempSkuId = cartCookie.split('~')[indx].split('|')[0];
			if (defaultSku.mId === _TempSkuId) {
				addedInCart = true;
				break;
			}
			else if (defaultSku.mBatteryInfoJsonVO) {
				for (var bi = 0, length = defaultSku.mBatteryInfoJsonVO.length; bi < length; bi++) {
					if (defaultSku.mBatteryInfoJsonVO[bi].mSkuId == _TempSkuId) {
						batteryItemAddedInCart = true;
						batteryProductSkuId = defaultSku.mBatteryInfoJsonVO[bi].mSkuId;
						batteryProductIndex = bi;
						break;
					}
				}
			}
		}
	}
	//

	colorCode = (typeof (colorCode) == "undefined") ? "" : colorCode;
	sizeCode = (typeof (sizeCode) == "undefined") ? "" : sizeCode;
	//var productInfoJson = JSON.parse($('#productInfoJson-'+productId).val());
	var variantStatus = (typeof (productInfoJson.mColorSizeVariantsAvailableStatus) === "undefined") ? '' : productInfoJson.mColorSizeVariantsAvailableStatus;
	var stylizedItem = false;
	var skuSelected = false;
	if (variantStatus === 'onlyColorVariants') {
		stylizedItem = true;
		skuSelected = (colorCode != "") ? true : false;
	} else if (variantStatus === 'onlySizeVariants') {
		stylizedItem = true;
		skuSelected = (sizeCode != "") ? true : false;
	} else if (variantStatus === 'bothColorSizeAvailable') {
		stylizedItem = true;
		skuSelected = (sizeCode != "" && colorCode != "") ? true : false;
	}
	var SalePriceHtml = "";
	if (typeof defaultSku.mUnCartable != 'undefined' && defaultSku.mUnCartable && defaultSku.mPriceDisplay == 'Price on Cart Page') {
		$colorSizeChangingOption.find('.prices').html('<div class="priceTooLow">' + priceIsNotAvailable + '</div>');
		$colorSizeChangingOption.find('.member-price').html('');
	}
	else if (stylizedItem && !skuSelected && !addToCartCloseFlag) {
		if (defaultSku.mPriceDisplay != 'Price on Cart Page' || addedInCart) {
			if (typeof productInfoJson.mActiveSKUsList != 'undefined' && productInfoJson.mActiveSKUsList.length == 1) {
				if (productInfoJson.mActiveSKUsList[0].mSalePrice != 0 && typeof productInfoJson.mActiveSKUsList[0].mSalePrice != 'undefined') {
					var mSalePrice = productInfoJson.mActiveSKUsList[0].mSalePrice;
					var crossedOutPrice = '';
					if (productInfoJson.mActiveSKUsList[0].mListPrice != 0 && typeof productInfoJson.mActiveSKUsList[0].mListPrice != 'undefined') {
						var mListPrice = productInfoJson.mActiveSKUsList[0].mListPrice;
						if(mListPrice == mSalePrice){
							SalePriceHtml = '<span class="price">' + dollerSymbol + mSalePrice.toFixed(2) + '</span>';
						}else{
							crossedOutPrice = '<span class="crossed-out-price">' + dollerSymbol + mListPrice.toFixed(2) + '</span>';
							SalePriceHtml = crossedOutPrice + '<span class="sale-price">' + dollerSymbol + mSalePrice.toFixed(2) + '</span>';
						}
					} else {
						SalePriceHtml = crossedOutPrice + '<span class="product-price-range">' + dollerSymbol + mSalePrice.toFixed(2) + '</span>';
					}
				}
			}
			else if (typeof productInfoJson.mActiveSKUsList != 'undefined' && productInfoJson.mActiveSKUsList.length > 1) {
				if (pminSalePrice != pmaxSalePrice) {
					SalePriceHtml = '<span class="product-price-range">' + dollerSymbol + pminSalePrice.toFixed(2) + '-' + dollerSymbol + pmaxSalePrice.toFixed(2) + '</span>';
				} else {
					//SalePriceHtml = '<span class="product-price-range">' + dollerSymbol+pminSalePrice.toFixed(2) +'</span>';
					if (productInfoJson.mActiveSKUsList[0].mSalePrice != 0 && typeof productInfoJson.mActiveSKUsList[0].mSalePrice != 'undefined') {
						var mSalePrice = productInfoJson.mActiveSKUsList[0].mSalePrice;
						var crossedOutPrice = '';
						if (productInfoJson.mActiveSKUsList[0].mListPrice != 0 && typeof productInfoJson.mActiveSKUsList[0].mListPrice != 'undefined') {
							var mListPrice = productInfoJson.mActiveSKUsList[0].mListPrice;
							if(mListPrice == mSalePrice){
								SalePriceHtml = '<span class="price">' + dollerSymbol + mSalePrice.toFixed(2) + '</span>';
							}else{
								crossedOutPrice = '<span class="crossed-out-price">' + dollerSymbol + mListPrice.toFixed(2) + '</span>';
								SalePriceHtml = crossedOutPrice + '<span class="sale-price">' + dollerSymbol + mSalePrice.toFixed(2) + '</span>';
							}
						} else {
							SalePriceHtml = crossedOutPrice + '<span class="product-price-range">' + dollerSymbol + mSalePrice.toFixed(2) + '</span>';
						}
					}
				}
			}
			$colorSizeChangingOption.find('.prices').html(SalePriceHtml);
			$colorSizeChangingOption.find('.member-price').html('');
		} else {
			$colorSizeChangingOption.find('.prices').html('<div class="priceTooLow">' + priceTooLowToDisplay + '</div>');
			$colorSizeChangingOption.find('.member-price').html('');
		}
	} else {
		if ((defaultSku.mPriceDisplay != 'Price on Cart Page' || addedInCart) && !batteryItemAddedInCart) {
			$colorSizeChangingOption.find('.prices .priceTooLow').html('');
			var priceHtml = '';
			if (defaultSku.mSalePrice != 0 && typeof defaultSku.mSalePrice != 'undefined' && defaultSku.mSalePrice != defaultSku.mListPrice && defaultSku.mPriceSavingAmount != 0 && typeof defaultSku.mPriceSavingAmount != 'undefined') {
				priceHtml = '<span class="crossed-out-price">' + dollerSymbol + defaultSku.mListPrice.toFixed(2) + '</span> <span class="sale-price">' + dollerSymbol + defaultSku.mSalePrice.toFixed(2) + '</span>';
				$colorSizeChangingOption.find('.prices').html(priceHtml);
			} else if (defaultSku.mSalePrice != 0 && typeof defaultSku.mSalePrice != 'undefined') {
				priceHtml = '<span class="price">' + dollerSymbol + defaultSku.mSalePrice.toFixed(2) + '</span>';
				$colorSizeChangingOption.find('.prices').html(priceHtml);
			} else if (defaultSku.mListPrice != 0 && typeof defaultSku.mListPrice != 'undefined') {
				priceHtml = '<span class="price">' + dollerSymbol + defaultSku.mListPrice.toFixed(2) + '</span>';
				$colorSizeChangingOption.find('.prices').html(priceHtml);
			}

			if (defaultSku.mPriceSavingAmount != 0 && typeof defaultSku.mPriceSavingAmount != 'undefined') {
				$colorSizeChangingOption.find('.member-price').html(youSavemessage + '&nbsp;' + dollerSymbol + defaultSku.mPriceSavingAmount.toFixed(2) + '&nbsp;' + openperanthasis + defaultSku.mPriceSavingPercentage + percentage + closeperanthasis);
			} else {
				$colorSizeChangingOption.find('.member-price').html('');
			}
		} else if (batteryItemAddedInCart) {
			priceHtml = '<div class="price sale-price"><span class="crossed-out-price">$' + defaultSku.mBatteryInfoJsonVO[batteryProductIndex].mListPrice + '</span>&nbsp;<span class="sale-price">$' + defaultSku.mBatteryInfoJsonVO[batteryProductIndex].mSalePrice + '</span></div>';
			$(".content-block.batterySku-" + batteryProductSkuId).find(".price.batteryPriceLow").replaceWith(priceHtml);
		}
		else {
			$colorSizeChangingOption.find('.prices').html('<div class="priceTooLow">' + priceTooLowToDisplay + '</div>');
			$colorSizeChangingOption.find('.member-price').html('');
		}
	}
	$colorSizeChangingOption.find(".display-none").removeClass('display-none');
}

function isPriceChangeEnableMethod(productInfoJson, variantName, colorCode, sizeCode, defaultSku) {
	var enabled = false;
	var variantStatus = productInfoJson.mColorSizeVariantsAvailableStatus;
	if (variantStatus === 'onlyColorVariants') {
		enabled = true;
	} else if (variantStatus === 'onlySizeVariants') {
		enabled = true;
	} else if (variantStatus === 'bothColorSizeAvailable' && colorCode != '' && sizeCode != '') {
		enabled = true;
	}

	return enabled;

}

function isImageChangeEnableMethod(productInfoJson, variantName, colorCode, sizeCode, defaultSku) {
	var enabled = false;
	var variantStatus = productInfoJson.mColorSizeVariantsAvailableStatus;
	if (variantStatus === 'onlyColorVariants') {
		enabled = true;
	} else if (variantStatus === 'bothColorSizeAvailable' && colorCode != '' && variantName != 'size') {
		enabled = true;
	}

	return enabled;
}

resistryAndWishListChange = function (variantStatus, colorCode, sizeCode, _this, defaultSku) {
	var $parentDiv = $(_this).closest('.sticky-price-contents');
	var selected = false;
	var registryAndWishListHtml = '';
	var wishListhtml = '';
	var registryhtml = '';
	if (typeof variantStatus != 'undefined') {
		if (variantStatus === 'bothColorSizeAvailable' && typeof colorCode != 'undefined' && colorCode != '' && typeof sizeCode != 'undefined' && sizeCode != '') {
			selected = true;
		} else if (variantStatus === 'onlyColorVariants' && typeof colorCode != 'undefined' && colorCode != '') {
			selected = true;
		} else if (variantStatus === 'onlySizeVariants' && typeof sizeCode != 'undefined' && sizeCode != '') {
			selected = true;
		}
	}

	if (selected) {
		var pdpRegistryUrl = $('.pdpRegistryUrl').val();
		var pdpWishlistUrl = $('.pdpWishlistUrl').val();
		var registry_id = $.cookie("REGISTRY_USER");
		var wishlist_id = $.cookie("myWishlistID");
		//	var registryWarningIndicator =$parentDiv.find('.registryWarningIndicator').val();
		//	var skuRegistryEligibility = $parentDiv.find('.skuRegistryEligibility').val();
		var registryWarningIndicator = defaultSku.mRegistryWarningIndicator;
		var skuRegistryEligibility = defaultSku.mSkuRegistryEligibility;

		if (typeof registryWarningIndicator != 'undefined' && registryWarningIndicator != '' && registryWarningIndicator === 'ALLOW' && skuRegistryEligibility) {

			if (typeof registry_id != 'undefined' && registry_id != '' && registry_id != 'null') {
				registryhtml = '<a href="javascript:void(0);" class="small-orange cart-button-enable cookie-registry" data-registry="registry">' + babyregistry + '</a>';
			} else {
				registryhtml = '<a href="' + pdpRegistryUrl + '" class="small-orange cart-button-enable registryAjaxCall">' + babyregistry + '</a>';
			}
			if (typeof wishlist_id != 'undefined' && wishlist_id != '' && wishlist_id != 'null') {
				wishListhtml = '<a href="javascript:void(0);"class="small-orange cart-button-enable cookie-wishlist" data-registry="wishlist">' + wishlist + '</a>';
			} else {
				wishListhtml = '<a href="' + pdpWishlistUrl + '" class="small-orange cart-button-enable registryAjaxCall wishlist">' + wishlist + '</a>';

			}
			registryAndWishListHtml = '<div class="add-to-text inline">' + addTo + '</div><div class="inline">' + registryhtml + "&nbsp;" + '<div class="inline"></div><div class="inline">' + wishListhtml + "&nbsp;" + '</div><div class="inline"></div></div>';
		} else if (registryWarningIndicator === 'WARN' && skuRegistryEligibility) {

			if (typeof registry_id != 'undefined' && registry_id != '' && registry_id != 'null') {
				registryhtml = '<a href="javascript:void(0);" class="small-orange cart-button-enable cookie-registry warnRegistryPopover" data-registry="registry" data-toggle="popover" data-placement="bottom" data-content="' + addToRegistry + '">' + babyregistry + '</a>';
			} else {
				registryhtml = '<a href="' + pdpRegistryUrl + '" class="small-orange cart-button-enable warnRegistryPopover registryAjaxCall" data-toggle="popover" data-placement="bottom" data-content="' + addToRegistry + '">' + babyregistry + '</a>';

			}
			if (typeof wishlist_id != 'undefined' && wishlist_id != '' && wishlist_id != 'null') {
				wishListhtml = '<a href="javascript:void(0);" class="small-orange cart-button-enable cookie-registry warnRegistryPopover" data-registry="wishlist" data-toggle="popover" data-placement="bottom" data-content="' + addToWishlist + '">' + wishlist + '</a>';
			} else {
				wishListhtml = '<a href="' + pdpWishlistUrl + '" class="small-orange cart-button-enable warnRegistryPopover registryAjaxCall wishlist" data-toggle="popover" data-placement="bottom" data-content="' + addToWishlist + '">' + wishlist + '</a>';

			}
			registryAndWishListHtml = '<div class="add-to-text inline">' + addTo + '</div><div class="inline">' + registryhtml + "&nbsp;" + '<div class="inline"></div><div class="inline">' + wishListhtml + "&nbsp;" + '</div><div class="inline"></div></div>';

		} else {
			registryAndWishListHtml = '<div class="add-to-text inline">' + addTo + '</div><div class="inline"><a href="javascript:void(0);" class="small-orange cart-button-disable">' + babyregistry + "&nbsp;" + '</a></div> <div class="inline"></div><div class="inline"><a href="javascript:void(0);" class="small-orange cart-button-disable">' + wishlist + "&nbsp;" + '</a></div><div class="inline"></div>';
		}
		$parentDiv.find('.add-to').html(registryAndWishListHtml);
	}
	var isRegistry = getParameterByName("isRegistry");
	var lastAccessedRegistryID = $.cookie("lastAccessedRegistryID");
	var registry_id = $.cookie("REGISTRY_USER");
	if (lastAccessedRegistryID != undefined && registry_id != undefined && lastAccessedRegistryID != '' && registry_id != '') {
		isButtonSwap = true;
	}
	if (registrySwapFlag && isButtonSwap) {
		buttonSwapForPDP();
	}


}

registryAndWishListChangeNew = function (variantStatus, colorCode, sizeCode, _this, defaultSku) {
	var $parentDiv = $(_this).closest('.sticky-price-contents');
	var selected = false;
	var registryAndWishListHtml = '';
	var wishListhtml = '';
	var registryhtml = '';
	if (typeof variantStatus != 'undefined') {
		if (variantStatus === 'bothColorSizeAvailable' && typeof colorCode != 'undefined' && colorCode != '' && typeof sizeCode != 'undefined' && sizeCode != '') {
			selected = true;
		} else if (variantStatus === 'onlyColorVariants' && typeof colorCode != 'undefined' && colorCode != '') {
			selected = true;
		} else if (variantStatus === 'onlySizeVariants' && typeof sizeCode != 'undefined' && sizeCode != '') {
			selected = true;
		}
	}

	if (selected) {
		var pdpRegistryUrl = $('.pdpRegistryUrl').val();
		var pdpWishlistUrl = $('.pdpWishlistUrl').val();
		var registry_id = $.cookie("REGISTRY_USER");
		var wishlist_id = $.cookie("myWishlistID");
		//	var registryWarningIndicator =$parentDiv.find('.registryWarningIndicator').val();
		//	var skuRegistryEligibility = $parentDiv.find('.skuRegistryEligibility').val();
		var registryWarningIndicator = defaultSku.mRegistryWarningIndicator;
		var skuRegistryEligibility = defaultSku.mSkuRegistryEligibility;
		var pdpaddtobabyregistry = (typeof pdpaddtobabyregistry != 'undefined' && pdpaddtobabyregistry != '') ? pdpaddtobabyregistry : 'add to baby registry';
		var pdpaddtowishlist = (typeof pdpaddtowishlist != 'undefined' && pdpaddtowishlist == '') ? pdpaddtowishlist : 'add to wish list';

		if (typeof registryWarningIndicator != 'undefined' && registryWarningIndicator != '' && registryWarningIndicator === 'ALLOW' && skuRegistryEligibility) {

			if (typeof registry_id != 'undefined' && registry_id != '' && registry_id != 'null') {
				registryhtml = '<button class="cart-button-enable cookie-registry baby-registry-addTo registryLoggedIn" data-registry="registry" >' + pdpaddtobabyregistry + '</button>';
			} else {
				registryhtml = '<button data-href="' + pdpRegistryUrl + '" class="cart-button-enable registryAjaxCall baby-registry-addTo registryAnonymous">' + pdpaddtobabyregistry + '</button>';
			}
			if (typeof wishlist_id != 'undefined' && wishlist_id != '' && wishlist_id != 'null') {
				wishListhtml = '<button class="cart-button-enable cookie-wishlist wish-list-addTo wishlistLoggedIn" data-registry="wishlist">' + pdpaddtowishlist + '</button>';
			} else {
				wishListhtml = '<button data-href="' + pdpWishlistUrl + '" class="cart-button-enable registryAjaxCall wishlist wish-list-addTo wishlistAnonymous">' + pdpaddtowishlist + '</button>';

			}
			registryAndWishListHtml = registryhtml + wishListhtml;
		} else if (registryWarningIndicator === 'WARN' && skuRegistryEligibility) {

			if (typeof registry_id != 'undefined' && registry_id != '' && registry_id != 'null') {
				registryhtml = '<button class="cart-button-enable cookie-registry warnRegistryPopover baby-registry-addTo registryLoggedIn" data-registry="registry" data-toggle="popover" data-placement="bottom" data-content="' + addToRegistry + '">' + pdpaddtobabyregistry + '</button>';
			} else {
				registryhtml = '<button data-href="' + pdpRegistryUrl + '" class="cart-button-enable warnRegistryPopover registryAjaxCall baby-registry-addTo registryAnonymous" data-toggle="popover" data-placement="bottom" data-content="' + addToRegistry + '">' + pdpaddtobabyregistry + '</button>';

			}
			if (typeof wishlist_id != 'undefined' && wishlist_id != '' && wishlist_id != 'null') {
				wishListhtml = '<button class="cart-button-enable cookie-wishlist warnRegistryPopover wish-list-addTo wishlistLoggedIn" data-registry="wishlist" data-toggle="popover" data-placement="bottom" data-content="' + addToWishlist + '">' + pdpaddtowishlist + '</button>';
			} else {
				wishListhtml = '<button data-href="' + pdpWishlistUrl + '" class="cart-button-enable warnRegistryPopover registryAjaxCall wishlist wish-list-addTo wishlistAnonymous" data-toggle="popover" data-placement="bottom" data-content="' + addToWishlist + '">' + pdpaddtowishlist + '</button>';

			}
			registryAndWishListHtml = registryhtml + wishListhtml;

		} else {
			registryAndWishListHtml = '<button class="cart-button-disable denyWarningRegistryPopover baby-registry-addTo">' + pdpaddtobabyregistry + '</button></div><button class="cart-button-disable denyWarningWishListPopover wish-list-addTo">' + pdpaddtowishlist + "&nbsp;" + '</button>';
		}
		$parentDiv.find('.registryButtonContainer').html(registryAndWishListHtml);
	}
	var isRegistry = getParameterByName("isRegistry");
	var lastAccessedRegistryID = $.cookie("lastAccessedRegistryID");
	var registry_id = $.cookie("REGISTRY_USER");
	if (lastAccessedRegistryID != undefined && registry_id != undefined && lastAccessedRegistryID != '' && registry_id != '') {
		isButtonSwap = true;
	}
	if (registrySwapFlag && isButtonSwap) {
		buttonSwapForPDP();
	}


}

//after getting default sku fade-in fade-out addtoCartbutton based on Condition
addtorCartRefresh = function (pDefaultSku, pColorCode, pSizeCode, pProductId, _this) {
	var $parentDiv = $(_this).closest('.sticky-price-contents');
	changeAddTocartButton(pProductId, pDefaultSku, pColorCode, pSizeCode, $parentDiv);
	displayTooltipMessageOnAddtoCartButton(true);

}

//after getting default sku fade-in fade-out addtoCartbutton based on Condition
addtorCartRefreshNew = function (pDefaultSku, pColorCode, pSizeCode, pProductId, _this) {
	var $parentDiv = $(_this).closest('.sticky-price-contents');
	changeAddTocartButtonNew(pProductId, pDefaultSku, pColorCode, pSizeCode, $parentDiv);
	//displayTooltipMessageOnAddtoCartButton(true);
}


fadeOut = function (pSameColorList, pSameSizeList, pActiveList, variantName, _this) {
	//console.log(pActiveList);
	var $colorSizeChangingOption = $(_this).closest('.sticky-price-contents');
	if (variantName === 'color') {
		$colorSizeChangingOption.find('.size-option').each(function () {
			$(this).addClass("out-of-stock");
			$(this).addClass("not-carried");
			$(this).removeClass("not-available");
			$(this).popover('destroy');
		});
		for (var colorIndex in pSameColorList) {
			$('.size-option .content').each(function () {
				for (var activeListIndex in pActiveList) {
					if (pSameColorList[colorIndex]['mId'] === pActiveList[activeListIndex]['mId'] && $(this).text().indexOf(pActiveList[activeListIndex]['mJuvenileSize']) > -1) {
						$(this).parent().removeClass("not-carried");
						if (pActiveList[activeListIndex]['mInventoryStatus'] != 'outOfStock' || (typeof pActiveList[activeListIndex]['mInventoryStatusInStore'] != 'undefined' && pActiveList[activeListIndex]['mInventoryStatusInStore'] != null && pActiveList[activeListIndex]['mInventoryStatusInStore'] != 'outOfStock' && pActiveList[activeListIndex]['mInventoryStatusInStore'].length)) {
							$(this).parent().removeClass("out-of-stock not-available not-carried");
						} else {
							$(this).parent().addClass("not-available");
						}
					}
				}
			});
		}
		$("body").find(".color_text .inline").removeClass("limit-1");
		$colorSizeChangingOption.find(".color_text .inline").text($colorSizeChangingOption.find(".color-block.color-selected img").attr("id"));
	} else if (variantName === 'size') {
		$colorSizeChangingOption.find('.color-block').each(function () {
			$(this).addClass("out-of-stock");
			$(this).addClass("not-carried");
			$(this).removeClass("not-available");
			$(this).popover('destroy');
		});
		for (var sizeIndex in pSameSizeList) {
			$('.color-block img').each(function () {
				for (var activeListIndex in pActiveList) {
					if (pSameSizeList[sizeIndex]['mId'] === pActiveList[activeListIndex]['mId']) {
						if ($(this).attr("id").indexOf(pActiveList[activeListIndex]['mColorCode']) > -1) {
							$(this).parents(".color-block").addClass("not-carried");
							if (pActiveList[activeListIndex]['mInventoryStatus'] != 'outOfStock' || (typeof pActiveList[activeListIndex]['mInventoryStatusInStore'] != 'undefined' && pActiveList[activeListIndex]['mInventoryStatusInStore'] != null && pActiveList[activeListIndex]['mInventoryStatusInStore'] != 'outOfStock' && pActiveList[activeListIndex]['mInventoryStatusInStore'].length)) {
								$(this).parents(".color-block").removeClass("out-of-stock not-available not-carried");
							} else {
								$(this).parents(".color-block").addClass("not-available");
							}
						}
					}
				}
			});
		}
		$("body").find("#size_text .inline").removeClass("limit-1");
		$colorSizeChangingOption.find("#size_text .inline").text($colorSizeChangingOption.find(".size-option.size-selected div").text());
	}
}

fadeOutNew = function (pSameColorList, pSameSizeList, pActiveList, variantName, _this) {
	//console.log(pActiveList);
	var $colorSizeChangingOption = $(_this).closest('.sticky-price-contents');
	if (variantName === 'color') {
		$colorSizeChangingOption.find('.swatches').each(function () {
			$(this).addClass("out-of-stock");
			$(this).addClass("not-carried");
			$(this).removeClass("not-available");
			$(this).popover('destroy');
		});
		for (var colorIndex in pSameColorList) {
			$('.swatches').each(function () {
				for (var activeListIndex in pActiveList) {
					if (pSameColorList[colorIndex]['mId'] === pActiveList[activeListIndex]['mId'] && $(this).text().indexOf(pActiveList[activeListIndex]['mJuvenileSize']) > -1) {
						$(this).removeClass("not-carried");
						if (pActiveList[activeListIndex]['mInventoryStatus'] != 'outOfStock' || (typeof pActiveList[activeListIndex]['mInventoryStatusInStore'] != 'undefined' && pActiveList[activeListIndex]['mInventoryStatusInStore'] != null && pActiveList[activeListIndex]['mInventoryStatusInStore'] != 'outOfStock' && pActiveList[activeListIndex]['mInventoryStatusInStore'].length)) {
							$(this).removeClass("out-of-stock not-available not-carried");
						} else {
							$(this).addClass("not-available");
						}
					}
				}
			});
		}
		$("body").find(".color_text .inline").removeClass("limit-1");
		$colorSizeChangingOption.find(".color_text .inline").text($colorSizeChangingOption.find(".color-block.color-selected img").attr("id"));
	} else if (variantName === 'size') {
		$colorSizeChangingOption.find('.color-block').each(function () {
			$(this).addClass("out-of-stock");
			$(this).addClass("not-carried");
			$(this).removeClass("not-available");
			$(this).popover('destroy');
		});
		for (var sizeIndex in pSameSizeList) {
			$('.color-block img').each(function () {
				for (var activeListIndex in pActiveList) {
					if (pSameSizeList[sizeIndex]['mId'] === pActiveList[activeListIndex]['mId']) {
						if ($(this).attr("id").indexOf(pActiveList[activeListIndex]['mColorCode']) > -1) {
							$(this).parents(".color-block").addClass("not-carried");
							if (pActiveList[activeListIndex]['mInventoryStatus'] != 'outOfStock' || (typeof pActiveList[activeListIndex]['mInventoryStatusInStore'] != 'undefined' && pActiveList[activeListIndex]['mInventoryStatusInStore'] != null && pActiveList[activeListIndex]['mInventoryStatusInStore'] != 'outOfStock' && pActiveList[activeListIndex]['mInventoryStatusInStore'].length)) {
								$(this).parents(".color-block").removeClass("out-of-stock not-available not-carried");
							} else {
								$(this).parents(".color-block").addClass("not-available");
							}
						}
					}
				}
			});
		}
		$("body").find("#size_text .inline").removeClass("limit-1");
		$colorSizeChangingOption.find("#size_text .inline").text($colorSizeChangingOption.find(".swatches.selected-swatch").text());
	}
}

function getDefaultSkuByInventory(skuList) {
	var maxInventory = 0;
	var defaultSku;
	for (var key in skuList) {
		var availableInventoryForStore = 0;
		var availableInventory = 0;
		var inventoryStatus = skuList[key].mInventoryStatus;
		if (skuList[key].hasOwnProperty('mInventoryStatusInStore')) {
			var inventoryStatusforStore = skuList[key].mInventoryStatusInStore;
			if (inventoryStatusforStore != 'outOfStock') {
				availableInventoryForStore = skuList[key].mAvailableInventoryInStore;
				if (availableInventoryForStore === -1) {
					defaultSku = skuList[key];
					break;
				}
			}
		}
		if (inventoryStatus != 'outOfStock') {
			availableInventory = skuList[key].mAvailableInventory;
			if (availableInventory === -1) {
				defaultSku = skuList[key];
				break;
			}
		}
		if (availableInventory > maxInventory) {
			maxInventory = availableInventory;
		}
		if (availableInventoryForStore > maxInventory && availableInventoryForStore > availableInventory) {
			maxInventory = availableInventoryForStore;
		}
		if (typeof (minSalePrice) == "undefined" || typeof (maxSalePrice) == "undefined") {
			minSalePrice = skuList[key]['mSalePrice'];
			maxSalePrice = skuList[key]['mSalePrice'];
		} else {
			minSalePrice = (minSalePrice > skuList[key]['mSalePrice']) ? skuList[key]['mSalePrice'] : minSalePrice;
			maxSalePrice = (maxSalePrice < skuList[key]['mSalePrice']) ? skuList[key]['mSalePrice'] : maxSalePrice;
		}

	}
	if (maxInventory > 0) {
		var localAvailableInventoryForStore = 0;
		var localAvailableInventory = 0;
		for (var key in skuList) {
			if (skuList[key].hasOwnProperty('mInventoryStatusInStore')) {
				localAvailableInventoryForStore = skuList[key].mAvailableInventoryInStore;
			}
			localAvailableInventory = skuList[key].mAvailableInventory;
			if (maxInventory === localAvailableInventoryForStore || maxInventory === localAvailableInventory) {
				defaultSku = skuList[key];
				break;
			}
		}
	} else {
		defaultSku = skuList[0];
	}
	return defaultSku;
}

function sendColorSizeCodeToCartOverlay(pageName, variantName, _this) {
	var productId = '';
	var contextPath = $('.hiddenContextPath').val();
	var parentId = '';
	var relationshipItemId = '';
	var commerceItemId = '';
	if (pageName == 'editCart') {
		parentId = 'editCartId';
		productId = $('#hiddenProductIdEditCart').val();
		relationshipItemId = $('#relationshipItemId').val();
		commerceItemId = $('#commerceItemId').val();
	}
	var colorCode = $('#editCartModal .color-block.color-selected .color-block-inner').attr('id');
	var sizeCode = $('#editCartModal .size-option.size-selected .content').attr('data-id');
	colorCode = (colorCode) ? colorCode : '';
	sizeCode = (sizeCode) ? sizeCode : '';
	var productInfoJson = JSON.parse($('.productInfoJson-' + productId).val());
	var defaultSku = getDefaultSku(productId, colorCode, sizeCode, productInfoJson, variantName, _this);
	console.debug(defaultSku);
	priceAndImageChangesEditCart(defaultSku);
	updateButtonEditCart(defaultSku, sizeCode, colorCode, variantName, productId, productInfoJson);

}

updateButtonEditCart = function (defaultSku, selectedSize, selectedColor, variantName, productId, productInfoJson) {
	var pageName = $('#pagenameIdEdit').val();
	if (typeof pageName === 'undefined') {
		pageName = '';
	}
	colorSizeVariantsAvailableStatus = productInfoJson.mColorSizeVariantsAvailableStatus;
	if (colorSizeVariantsAvailableStatus === 'onlySizeVariants') {
		selectedColor = defaultSku.mColorCode;
	}
	var relationshipItemId = $('#relationshipItemIdEdit').val();
	var commItemId = $('#commItemIdEdit').val();
	var wrapSkuId = $('#wrapSkuIdEdit').val();
	if (typeof wrapSkuId === 'undefined') {
		wrapSkuId = '';
	}
	var quantity = $('#QuantityEdit').val();
	var contextPath = $('.hiddenContextPath').val();
	var button = '';
	var sameSizeList = productInfoJson.mSelectedSizeSKUsList;
	var samecolorList = productInfoJson.mSelectedColorSKUsList;
	editCartItemInReviewFunction = "javascript: return editCartItemInReview('" + productInfoJson.mProductId + "','" + defaultSku.mId + "','" + commItemId + "','" + relationshipItemId + "','" + wrapSkuId + "', '" + quantity + "')";
	editCartItemInShippingFunction = "javascript: return editCartItemInShipping('" + productInfoJson.mProductId + "','" + defaultSku.mId + "','" + commItemId + "','" + relationshipItemId + "','" + wrapSkuId + "');"
	editCartItemFunction = "javascript: return editCartItem('" + productInfoJson.mProductId + "','" + defaultSku.mId + "','" + commItemId + "','" + relationshipItemId + "', '" + quantity + "','" + contextPath + "');"
	var onclickEvent = "";
	var onlineInventoryStatus = defaultSku.mInventoryStatus;
	if (typeof defaultSku.mColorCode != 'undefined' && defaultSku.mColorCode != '' && typeof defaultSku.mJuvenileSize != 'undefined' && defaultSku.mJuvenileSize != '') {
		if (typeof selectedSize != 'undefined' && selectedSize != '' && variantName === 'size' && defaultSku.mColorCode === selectedColor && onlineInventoryStatus != 'outOfStock') {
			if (pageName === 'Review' || pageName === 'store-pickup') {
				button = '<button type="submit" class="cart-edit-update" >update</button>';
				onclickEvent = editCartItemInReviewFunction;
			} else if (pageName === 'Shipping') {
				button = '<button type="submit" class="cart-edit-update">update</button>';
				onclickEvent = editCartItemInShippingFunction;
			} else {
				button = '<button type="submit" class="cart-edit-update">update</button>';
				onclickEvent = editCartItemFunction;
			}
		} else if (typeof selectedColor != 'undefined' && selectedColor != '' && variantName === 'color' && defaultSku.mJuvenileSize === selectedSize && onlineInventoryStatus != 'outOfStock') {
			if (pageName === 'Review' || pageName === 'store-pickup') {
				button = '<button type="submit" class="cart-edit-update" >update</button>';
				onclickEvent = editCartItemInReviewFunction;
			} else if (pageName === 'Shipping') {
				button = '<button type="submit" class="cart-edit-update">update</button>';
				onclickEvent = editCartItemInShippingFunction;
			} else {
				button = '<button type="submit" class="cart-edit-update">update</button>';
				onclickEvent = editCartItemFunction;
			}
		} else {
			if (pageName === 'Review' || pageName === 'store-pickup') {
				button = '<button type="submit" disabled="disabled" class="cart-edit-update fade-add-to-cart">update</button>';
				onclickEvent = editCartItemInReviewFunction;
			} else if (pageName === 'Shipping') {
				button = '<button type="submit" disabled="disabled" class="cart-edit-update fade-add-to-cart">update</button>';
				onclickEvent = editCartItemInShippingFunction;
			} else {
				button = '<button type="submit" disabled="disabled" class="cart-edit-update fade-add-to-cart">update</button>';
				onclickEvent = editCartItemFunction;
			}
		}
	} else {
		if (pageName === 'Review' || pageName === 'store-pickup') {
			button = '<button type="submit" class="cart-edit-update">update</button>';
			onclickEvent = editCartItemInReviewFunction;
		} else if (pageName === 'Shipping') {
			button = '<button type="submit" class="cart-edit-update">update</button>';
			onclickEvent = editCartItemInShippingFunction;
		} else {
			button = '<button type="submit" class="cart-edit-update">update</button>';
			onclickEvent = editCartItemFunction;
		}
	}
	var _Temp = "#editCartId-" + productInfoJson.mProductId;
	$(_Temp).children('.cart-edit-update').replaceWith(button);
	$(_Temp).children('.cart-edit-update').attr("onclick", onclickEvent);
}

priceAndImageChangesEditCart = function (defaultSku) {

	if (defaultSku.mSalePrice != 0 && defaultSku.mSalePrice != defaultSku.mListPrice && defaultSku.mPriceSavingAmount != 0 && typeof defaultSku.mPriceSavingAmount != 'undefined') {
		$('.editCartPrice').html('<span class="crossed-out-price price-save">' + dollerSymbol + defaultSku.mListPrice.toFixed(2) + '</span>&nbsp;<span>' + dollerSymbol + defaultSku.mSalePrice.toFixed(2) + '</span><span class="price-save">&nbsp; .</span> <span class="price-save save-text">&nbsp;' + youSavemessage + '&nbsp;' + dollerSymbol + defaultSku.mPriceSavingAmount + '&nbsp;' + openperanthasis + defaultSku.mPriceSavingPercentage + percentage + closeperanthasis + '</span>');
	} else if (defaultSku.mListPrice != 0) {
		$('.editCartPrice').html('<span>' + dollerSymbol + defaultSku.mSalePrice.toFixed(2) + '</span>');
	} else {
		$('.editCartPrice').html('');
	}
	var primaryImage = defaultSku.mPrimaryImage;
	if (typeof defaultSku.mPrimaryImage === 'undefined' || defaultSku.mPrimaryImage === '') {
		primaryImage = contextPath + 'images/no-image500.gif';
	}
	$('.cart-edit-popup-image').find('.shopping-cart-product-edit-image').attr('src', primaryImage + '?resize=385:385');
}
/* END: Product details page variance selection */

function fixedProductTitle() {
	$('.header-part').removeClass('fixed-nav');
	var $window = $(window),
		$stickyEl = $('.fixed-product-title'),
		elTop = $stickyEl.offset().top;

	$(window).scroll(function () {
		$stickyEl.toggleClass('sticky', $(this).scrollTop() > elTop);
	});

	doResize();

	$window.resize(doResize);
}

function doResize() {
	if ($(window).width() < 964) {
		$('.pdp-shopping-cart-overlay-container').css('left', 0);
	} else {
		var widthDifference = $(window).width() - 963;
		$('.pdp-shopping-cart-overlay-container').css('left', widthDifference / 2);
	}
	$('.pdp-shopping-cart-overlay-container').removeClass('in');
	$('#product-disabled-modal').hide();
}

/* End: Product details page vaariance selection */

$(window).load(function () {

	/* Start: Enhancement description */
	if ($('.enhanced-content div').length > 0) {
		$('.no-enhanced-content').removeClass('no-enhanced-content');
	}
	/* End: Enhancement description */

	var rcount = $('.pr-review-wrap').length;
	if ($('#customer-review-summary  .pr-other-attributes ul.pr-other-attributes-list li.pr-other-attributes-group').length > 0) {
		var attributesheight = $('#customer-review-summary  .pr-other-attributes').height();
		$('.pr-snapshot-body-wrapper .pr-review-points').css("margin-top", attributesheight + 45);
		$('#customer-review-summary #pr-snapshot-histogram #pr-snapshot-histogram-container').css("margin-top", attributesheight);
	}
	if ($('.pr-snapshot-consensus').length == 0 && $('#customer-review-summary  .pr-other-attributes ul.pr-other-attributes-list li.pr-other-attributes-group').length == 0) {
		if (rcount > 3) {
			//$('#customer-review-summary #pr-snapshot-histogram #pr-snapshot-histogram-container').css("top","32px");
			$('.pr-snapshot-body-wrapper .pr-review-points').css("margin-top", "0px");
			$('#pr-snapshot-histogram').find('#pr-snapshot-histogram-container').css({ 'top': '0 !important' });
		}
	}


	$('.review-total').text('reviewed by ' + $('.pr-snapshot-average-based-on-text').find('span').text() + ' customers');
	$('.questions-total').text($('.prPaHeader .prPaCounts').text());

	$('.pr-attribute-value-list-empty').hide();
	if (rcount == 1) {
		$('.pr-snippet-link').html('1 Review');
	}
	if (rcount == 0) {

		//megamenu fix
		var data1 = $('.pr-snapshot-no-ratings').html();
		if (data1 != undefined) {
			var data2 = data1.split('<');
			$('.pr-snapshot-no-ratings').html('<' + data2[1] + '<' + data2[2]);
			var data3 = $('.pr-snapshot-title').html();
			var data4 = data3.split('<');
			$('.pr-snapshot-title').html('');
			$('.pr-snapshot-title').html('<' + data4[2]);

			$('.product-reviews').find('#customer-review-summary').height(61);
			$('.pr-snapshot-footer').css('top', '-7px');
			$('.pr-snapshot-footer').css('right', '0px');
		}
	}
	if (rcount > 0) {
		$('#noReview').hide();
		$('#customer-review-summary .pr-snapshot-write-review').text('write a review');
	}
	else {
		$('#noReview').show();
		$('.review-total').text(' Be The First To Review This Product ');
		$('#customer-review-summary .pr-snapshot-footer a.pr-write-review-link').text('write a review');
	}
	var qcount = $('.prPaQaDialog').length;
	if (qcount == '0') {
		$('#noQuestion').show();
		$('.questions-total').text(' Be The First To Query about this Product ');
	}
	else
		$('#noQuestion').hide();

	//setTimeout(function(){
	$('.pr-fb-reviewComment').find('span').find('a').text('add a comment');
	//},100);

	//window.scrollTop(0);

});

function enableWarnRegistryPopover() {
	/*var compareDenyRegistryMessages = compareDenyRegistryMessage;
	if(compareDenyRegistryMessages ==''|| compareDenyRegistryMessages == 'undefined'){
		compareDenyRegistryMessages = "The item cannot be added to your registry. Please select alternate item(s) or consider purchasing the item(s) now!";
	}*/
	$(".warnRegistryPopover").popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		trigger: 'hover',
		delay: {
			show: 50,
			hide: 200
		}
	});
	$(".compareDenyPopover").popover({
		animation: 'true',
		html: 'true',
		content: 'compareDenyPopover',
		trigger: 'hover',
		delay: {
			show: 50,
			hide: 200
		}
	});
}

function enableDenyRegistryWishlistpopover() {
	var pdpDenyRegistryMessages = pdpDenyRegistryMessage;
	var pdpDenyWishlistMessages = pdpDenyWishlistMessage;
	if (pdpDenyRegistryMessages == '' || pdpDenyRegistryMessages == 'undefined') {
		pdpDenyRegistryMessages = "The item cannot be added to your registry. Please select alternate item(s) or consider purchasing the item(s) now!";
	}
	if (pdpDenyWishlistMessages == '' || pdpDenyWishlistMessages == 'undefined') {
		pdpDenyWishlistMessages == "The item cannot be added to your wishlist. Please select alternate item(s) or consider purchasing the item(s) now!";
	}
	$(".denyWarningRegistryPopover").popover({
		animation: 'true',
		html: 'true',
		content: pdpDenyRegistryMessages,
		trigger: 'hover',
		delay: {
			show: 50,
			hide: 200
		}
	});
	$(".denyWarningWishListPopover").popover({
		animation: 'true',
		html: 'true',
		content: pdpDenyWishlistMessages,
		trigger: 'hover',
		delay: {
			show: 50,
			hide: 200
		}
	});

}

// PDP findinstore START *******
var showallstores = false;
$(document).ready(function () {
	var counter_stores = 0;

	collectionStickyPrice();

	$(document).find('.findInStore').popover({
		animation: 'true',
		html: 'true',
		content: 'Please select a quantity.',
		placement: 'bottom',
		trigger: 'manual'
	});
	$(document).find('.cp-increase').popover({
		animation: 'true',
		html: 'true',
		content: 'This item is not available for ship to home.Please check for the availability at a store near you using the "Find in store" link and add the item to cart.',
		placement: 'bottom',
		trigger: 'manual'
	});
});

var getfavStore;

function findinstorecommonOLD(isBtnClick) {
	if ($('#FindinStorePDPField').val().trim() != "") {
		var findinstorejsonResponse;
		var postaladdress;
		var $this = $(this);
		if (isBtnClick == 'YES')
			postaladdress = $('#FindinStorePDPField').val();
		else
			postaladdress = getfavStore;
		$(".find-in-store-overlay").find("p.sl_errormsg").hide();
		$(".find-in-store-overlay").find("p.sl_errormsg").parent().find("#FindinStorePDPField").removeClass("sl_input_error_msg");
		var lightBoxRadius = '100';
		if ($("#lightBoxRadius").length) {
			lightBoxRadius = $("#lightBoxRadius").val();
		}
		$.ajax({
			type: 'post',
			dataType: 'text',
			async: false,
			url: '/storelocator/getStoreResults.jsp?postalAddress=' + postaladdress + '&distance=' + lightBoxRadius + '&skuId=' + $('#pdpSkuId').val(),
			success: function (data) {
				findinstorejsonResponse = $.trim(data);
				var result = JSON.parse(findinstorejsonResponse);
				if (result.errors) {
					$(".find-in-store-overlay").find("p.sl_errormsg").text(result.errors[0].errorMessage);
					$(".find-in-store-overlay").find("p.sl_errormsg").show();
					$(".find-in-store-overlay").find("p.sl_errormsg").parent().find("#FindinStorePDPField").addClass("sl_input_error_msg");
				} else {
					//var store_locator_assigner = ($(this).data("store_locator") == null)? populateStoresFromJson($this, findinstorejsonResponse):
					populateStoresFromJson($this, findinstorejsonResponse);
				}
			},
			error: function (e) {
				console.log(e);
			}
		});
	}
	else {
		$(".find-in-store-overlay").find("p.sl_errormsg").text("Please enter an address to get started");
		$(".find-in-store-overlay").find("p.sl_errormsg").show();
		$(".find-in-store-overlay").find("p.sl_errormsg").parent().find("#FindinStorePDPField").addClass("sl_input_error_msg");
		$(".find-in-store-overlay").find("#FindinStorePDPField").focus();

	}
}

function reInitScrollBarfindinstore(formId, height) {
	var scrollable = '';
	if (formId == 'store-locator-results-scrollable') {
		scrollable = $('.find-in-store-overlay').find('.tse-scrollable');
	}
	var recalculate = function () {
		setTimeout(function () {
			//scrollable.TrackpadScrollEmulator('recalculate');
			scrollable.find('.tse-scroll-content').height(height);
			if (formId == 'onEditCreditCardOverlay') {
				$('.my-account-add-credit-card-overlay').height(496);
				$('.my-account-add-credit-card-overlay').find('.set-height').height(456);
			}
		}, 500);
	};

	scrollable.TrackpadScrollEmulator({
		wrapContent: false,
		autoHide: false
	});

	recalculate();
}

function SelectAStoreRelation(relationshipItemId, storeid) {
	console.log(relationshipItemId + " ::::" + storeid);
}

// PDP findinstore END ********

function loademailToFriend(displayName, primaryImage, productId, skuId, pdpPageURL) {
	var displayName = displayName ? displayName : document.getElementById('skuDisplayName').value;
	$.ajax({
		type: 'POST',
		dataType: 'html',
		data: { displayName: displayName, primaryImage: primaryImage, productId: productId, skuRepoId: skuId, pdpPageURL: pdpPageURL },
		url: '/jstemplate/emailMePopUp.jsp',
		async: false,
		success: function (data) {
			$('#emailMePopUp').html(data);
			reInitScrollBar('emailToFriend', 340);
		}

	});
}

$(document).on('click', '#sendFormData', function () {
	var displayName = $('#displayName').val();
	var skuId = $('#skuId').val();
	var productId = $('#collectionEmailProductId').val();
	var pdpPageURL = $('#pdpPageURL').val();

	if (!$("#sendFormData").hasClass("disable")) {
		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: $('#emailToFriend').serialize(),
			url: "/jstemplate/ajaxIntermediateEmail.jsp?displayName=" + displayName + "&skuId=" + skuId + "&productId=" + productId + "&pdpPageURL=" + pdpPageURL,
			async: false,
			success: function (data) {
				if (data.indexOf('Following are the form errors:') > -1) {
					$('#emailme span li').remove();
					$('#emailme').prepend(data.split('Following are the form errors:')[1]);
					if (typeof reloadCaptcha !== "undefined") {
						reloadCaptcha();
					}
				} else {
					//$('#emailme').html('Thank You.');
					$('#emailme').addClass("hide");
					$("#emailMePopUp").scrollTop(0)
					$('#emailMePopUp .pdp-email-me-friend-show').addClass("hide");
					$(".email-a-friend .email-friend-heading").addClass("hide");
					$('#emailMePopUp .pdp-email-friend-Success').removeClass("hide");
					$(window).trigger("resize");
				}
			}

		});
	}
})

function loadNotifyMe($this, fromPDPBatteries, notifymeProductData) {
	if (fromPDPBatteries == 'fromPDPBatteries') {
		var $parent = $($this).parents(".content-block");
		var NotifyMeDescription = $parent.find(".NotifyMeDescription").html();
		var NotifyMeDskuDisplayName = $parent.find(".NotifyMeDskuDisplayName").val();
		var NotifyMeskuId = $parent.find(".NotifyMeskuId").val();
		var NotifyMeproductId = $parent.find(".productId").val();
		var NotifyMeOriginalParentProduct = $parent.find(".skn").val();
		var NotifyMeProductPageUrl = $parent.find(".NotifyMeProductPageUrl").val();
		var NotifyMeHelpURLName = $parent.find(".NotifyMeHelpURLName").val();
		var NotifyMeHelpURLValue = $parent.find(".NotifyMeHelpURLValue").val();
		var NotifyMePrimaryImage = $parent.find(".NotifyMePrimaryImage").val();
		var NotifyMeSiteID = $parent.find(".siteID").val();
	}
	else {
		var $this = $($this);
		var NotifyMeDescription = $this.parents(".email-section").find(".NotifyMeDescription").html();
		var NotifyMeDskuDisplayName = $this.parents(".email-section").find(".NotifyMeDskuDisplayName").val() || $this.parents(".email-section").find(".NotifyMeDskuDisplayName").html();
		var NotifyMeskuId = $this.parents(".email-section").find(".NotifyMeskuId").val();
		var NotifyMeproductId = $this.parents(".email-section").find(".productId").val();
		var NotifyMeOriginalParentProduct = $this.parents(".email-section").find(".skn").val();
		var NotifyMeProductPageUrl = $this.parents(".email-section").find(".NotifyMeProductPageUrl").val();
		var NotifyMeHelpURLName = $this.parents(".email-section").find(".NotifyMeHelpURLName").text();
		var NotifyMeHelpURLValue = $this.parents(".email-section").find(".NotifyMeHelpURLValue").val();
		var NotifyMePrimaryImage = $this.parents(".email-section").find(".primaryImage").val();
		var NotifyMeSiteID = $this.parents(".email-section").find(".siteID").val();
		var onlinePID = '';
		if ($('.collection-template').length > 0) {
			onlinePID = $this.parents(".collections-availability").find(".collectionOnlinePID").val();
		} else {
			onlinePID = $this.parents(".email-section").find(".onlinePID").val();
		}
	}
	$.ajax({
		type: 'POST',
		dataType: 'html',
		data: { itemDescription: NotifyMeDescription, itemName: NotifyMeDskuDisplayName, itemNumber: NotifyMeskuId, productId: NotifyMeproductId, itemURL: NotifyMeProductPageUrl, helpURLName: NotifyMeHelpURLName, helpURLValue: NotifyMeHelpURLValue, primaryImage: NotifyMePrimaryImage, siteID: NotifyMeSiteID, skn: NotifyMeOriginalParentProduct, onlinePID: onlinePID },
		url: '/jstemplate/notifyMePopUp.jsp',
		async: false,
		success: function (data) {
			$('#notifyMeModal').html(data);
		}

	});
}

$(document).ready(function () {
	var sendSpecialofferToMail = false;
	$(document).on('submit', '#notifyMe', function () {
		var mailId = $('#mailId').val();
		var itemDescription = "" + $(".notify-me-wrapper").find(".NotifyMeDescription").html();
		var itemName = $('#itemName').val();
		var productId = $('#productId').val();
		var itemURL = $('#itemURL').val();
		var itemNumber = $('#itemNumber').val();
		var helpURLName = $('#helpURLName').val();
		var helpURLValue = $('#helpURLValue').val();
		var siteID = $('#siteID').val();
		var onlinePID = $('#onlinePID').val();
		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: { formId: 'notifyMe', sendSpecialofferToMail: sendSpecialofferToMail, notifyMeMailId: mailId, productId: productId, itemDescription: itemDescription, itemName: itemName, itemNumber: itemNumber, itemURL: itemURL, helpURLName: helpURLName, helpURLValue: helpURLValue, siteID: siteID, onlinePID: onlinePID },
			url: '/product/intermediate/ajaxIntermediateRequest.jsp',
			async: false,
			success: function (data) {
				if (data.indexOf('Following are the form errors:') > -1) {
					$('#errorNotifyMe').html(data.split('Following are the form errors:')[1]);
				} else {
					//$('#errorNotifyMe').html('<div style="color:#666666;">Thank You..We will notify you when this item is back in stock.</div>');
					$('#notifyMeModal [class*="spark-"] .modal-header').addClass('hide');
					$('#notifyMeModal [class*="spark-"] .modalHeader .modalTitle').css({ visibility: 'hidden' });
					$('#notifyMeModal [class*="spark-"] .modal-content.sharp-border').css({ height: 'auto' });
					$('#notifyMeModal [class*="spark-"] .notifymeSuccess').removeClass('hide');
					$('#notifyMeModal').focus();
					$('#errorNotifyMe').addClass('hide');
				}
			}

		});
		return false;
	});

	$('body').on('click', 'input[type="checkbox"]', function () {
		if ($(this).prop("checked") == true) {
			sendSpecialofferToMail = true;
		}
		else if ($(this).prop("checked") == false) {
			sendSpecialofferToMail = false;
		}
	});

	$(document).on('hidden.bs.modal', '#notifyMeModal', function () {
		$("#notifyMeModal").find(".notify-me-wrapper").find("input").val("");
		$("#errorNotifyMe").html("");
		$("#pdp-notify-checkbox").prop('checked', false);
	});
	$(document).on('hidden.bs.modal', '#emailMePopUp', function () {
		$('#emailme').removeClass("hide");
		if ($('#emailme span li')) {
			$('#emailme span li').remove();
		}
		$('#emailMePopUp .pdp-email-me-friend-show').removeClass("hide");
		$('#emailMePopUp .pdp-email-friend-Success').addClass("hide");
		$("#emailToFriend")[0].reset();
	});
	$(document).on('hidden.bs.modal', '#shoppingCartModal,.show-hide-addToCart-section', function () {
		if ($('.collections-sticky-price').length > 0 && !($(this).attr('id') == 'multiple-items-added-success-wishlist')) {
			$(document).find('.collections-sticky-price .small-orange').addClass("desableLinkW3c")
			$(document).find('.collection_registry_unavailable_error').addClass('hide');
		}
		addToCartCloseFlag = true;
		notCachedPriceAndImage('', '');
	});

	$(document).on('click', '.findin-storemodels-close', function () {
		var pageName = $(".pageName").val();
		if(pageName == "pdpPage")
			{
				var searchTerm = $("#searchTearm").val();
				var storeZipCode = $("#storeZipCode").val();
				tealiumForStore(searchTerm,storeZipCode,"close");
			}
		$('#findInStoreModal').modal("hide");
		if ($('html').hasClass("modal-open")) {
			$('html').removeClass("modal-open")
			$('body').css("padding-right", "");
		}
	});
	$('body').on('click', '.cookie-registry', function () {
		registryWishListCookie("false", this);
		invokeHooklogicEvent();
	});
	$('body').on('click', '.cookie-wishlist', function () {
		invokeWishlistService(this);
	});
});

/* START - For Invoking registry functionality */
function registryWishListCookie(isWishlist, _this) {
	var $doc = $(document);
	var pageName = $(".pageName").val();
	if (pageName == 'collectionPage') {
		$doc = $(_this).parents('.collectionLineItem');
	}
	if (isWishlist == null) {
		isWishlist = "false";
	}
	var registryType;
	var registryNumber;
	var lastAccessedID;
	if (isWishlist == 'true') {
		registryNumber = $doc.find('#rgs_wishlistId').val() || $.cookie("myWishlistID") || '';
		lastAccessedID = $doc.find('#rgs_lastAccessedWishlistID').val() || $.cookie("lastAccessedWishlistID") || '';
		requestType = "addWishlist";
	} else {
		registryNumber = $.cookie("REGISTRY_USER") || $doc.find('#rgs_registryNumber').val() || '';
		lastAccessedID = $.cookie("lastAccessedRegistryID") || $doc.find('#rgs_lastAccessedRegistryID').val() || '';
		requestType = "addRegistry";
	}
	var productDetails = $doc.find('#rgs_productDetails').val() || '';

	var primaryimage = '';
	var skuDisplayName = '';
	if (pageName == 'collectionPage') {
		if ($('#multiple-items-added-success-wishlist').is(':visible')) {
			primaryimage = $('#multiple-items-added-success-wishlist .mws-product-image img').attr('src');
		} else {
			primaryimage = $(_this).parents(".collection-product-block").find("img.collection-image").attr("src");
		}
		if (typeof primaryimage != 'undefined' && primaryimage != null && primaryimage != '') {
			primaryimage = primaryimage.split("?")[0];
		}
		skuDisplayName = $(_this).parents(".collection-product-block").find("h4 a").text();
	} else {
		if (pageName == 'pdpPage') {
			primaryimage = $("#carousel-bounding-box .item.active img.zoomImage").attr('src') || '';
			skuDisplayName = $doc.find('#rgs_skuDisplayName').val();
		} else {
			var primaryImgSkuId = $(_this).parents('.addToCartJspSection').find('.primary-img-skuId').val() || '';
			primaryimage = $('.primary-img-' + primaryImgSkuId).attr('src') || '';
			skuDisplayName = $(_this).parents('.addToCartJspSection').find('#compareSkuDisplayName').val();
		}

	}
	if (primaryimage) {
		if (primaryimage.lastIndexOf('?') > -1) {
			primaryimage = primaryimage.substring(0, primaryimage.lastIndexOf('?')) + "?fit=inside|240:240";
		} else {
			primaryimage = primaryimage + "?fit=inside|240:240";
		}
	} else if (typeof akamaiNoImageURL != 'undefined' && akamaiNoImageURL != '' && akamaiNoImageURL != null) {
		primaryimage = akamaiNoImageURL + "?fit=inside|240:240";
	}
	var timeout = $doc.find('#rgs_timeout').val();

	if (typeof productDetails != 'undefined' && productDetails != null && productDetails != '' && productDetails.length > 0) {
		productDetails = productDetails.split('|');
		var quantity = '';
		if ($('#multiple-items-added-success-wishlist').is(':visible') && pageName == 'collectionPage') {
			quantity = $('#wishlistCPProductQty').val() || 1;
		} else {
			quantity = $('[id$="' + productDetails[0] + '"]').find('#ItemQty').val() || 1;
		}
		if (typeof quantity !== "undefined" && quantity !== "") {
			productDetails.push(quantity);
		}
		productDetails = productDetails.join('|');
	} else {
		var productId = '';
		if ($('.collection-template').length > 0) {
			var pId = $(_this).parents('.collectionLineItem').attr('data-id') ? ($(_this).parents('.collectionLineItem').attr('data-id')).split('-') : ($(_this).parents('.collectionLineItem').parent().attr('data-id')).split('-');
			productId = (pId.length > 1) ? (pId[1]) : '';
		} else {
			productId = $(_this).parents('[id^=pdpId]').attr('id');
			productId = (typeof productId !== "undefined") ? productId.split('-')[1] : ($('#hiddenProductId').val());
		}
		productDetails = generateAjaxProductDetailsByProductId(productId, isWishlist);
	}
	//console.log(productDetails);
	$.ajax({
		type: 'POST',
		contentType: 'application/json',
		dataType: 'json',
		url: '/jstemplate/ajaxRegistryIntermediate.jsp?lastAccessedID=' + lastAccessedID + '&registryNumber=' + registryNumber + '&productDetails=' + productDetails + '&isWishlist=' + isWishlist + '&pageName=' + pageName + '&requestType=' + requestType,
		success: function (response) {
			//console.log(response);
			$("#multiple-items-added-success-wishlist, #quickviewModal").modal("hide");
			if (typeof response.mResponseStatus !== "undefined" && response.mResponseStatus.mIndicator == "SUCCESS") {
				if (isWishlist != 'true') {
					$("#add-to-registry").find(".registrySkuDisplayName").text(skuDisplayName);
					$("#add-to-registry").find(".image-name-container img").attr('src', primaryimage);
					$("#add-to-registry").find(".registrySuccessMsgCatID").html("(#" + registryNumber + ")");
					$("#add-to-registry").modal("show");

					var customerEmail = $('#customerEmail').val();
					var customerID = $('#customerID').val();
					var productCategories = $('#productCategories').val();
					var productId = $('#productId').val();
					var productName = $('#productName').val();
					var qty = $("#ItemQty").val();
					var productListPrice = $('#productListPrice').val();
					var productSku = $('#productSku').val();
					var productSubCategories = $('#productSubCategories').val();
					var partnerName = $('#partnerName').val();
					var registryId = $('.registry_id').val();
					var productFamily = $('#productFamily').val();

					if (typeof utag != 'undefined') {
						utag.link({
							event_type: 'registry_add',
							customer_email: customerEmail,
							customer_id: customerID,
							product_category: [productCategories],
							product_id: [productId],
							product_name: [productName],
							product_list_price: [productListPrice],
							product_quantity: [qty],
							product_sku: [productSku],
							product_subcategory: [productSubCategories],
							partner_name: partnerName,
							registry_id: registryId,
							product_family: [productFamily]
						});
					}
				} else {
					$("#items-added-success-wishlist").find(".wishlistSkuDisplayName").text(skuDisplayName);
					$("#items-added-success-wishlist").find(".image-name-container img").attr('src', primaryimage);
					$("#items-added-success-wishlist").find(".gift_recipient_name").text("#" + lastAccessedID);
					$("#items-added-success-wishlist").modal("show");
				}
				//console.log("Sample Response for Baby Registry:  " + response);
			} else if ((typeof response.mValidation != 'undefined' && typeof response.mValidation.mError.mInformational !== "undefined" && response.mValidation.mError.mInformational == "error")
				|| (typeof response.mResponseStatus !== "undefined" && response.mResponseStatus.mIndicator == "FAILURE")) {

				$("#error-registry-dailog").modal("show");

			} else {
				$("#error-registry-dailog").modal("show");
				console.log("error");
			}
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			$("#quickviewModal").modal("hide");
			if (!$(this).hasClass("cookie-registry")) {
				$("#multiple-items-added-success-wishlist").modal("hide");
			}
			$("#error-registry-dailog").modal("show");
			console.log(errorThrown)
		},
		timeout: timeout
	});

}
/* END - For Invoking registry functionality */

/* START - For Invoking wishlist functionality */
function invokeWishlistService(_this) {
	var $doc = $(document);
	var pageName = $(".pageName").val();
	if (pageName == 'collectionPage') {
		$doc = $(_this).parents('.collectionLineItem');
	}
	var registryNumber = $.cookie("myWishlistID") || '';
	//var	registryNumber = $doc.find('#rgs_wishlistId').val();
	//var	registryNumber = $(_this).parents('.cp-stock-details').find('#rgs_wishlistId').val();
	var showWishList = 'true';
	var primaryimage = '';
	var skuDisplayName = '';
	//url = "/jstemplate/ajaxTestingRegistry.jsp";
	timeout = $doc.find('#rgs_timeout').val();
	$.ajax({
		type: 'POST',
		contentType: 'application/json',
		dataType: 'json',
		url: '/jstemplate/ajaxRegistryIntermediate.jsp?registryNumber=' + registryNumber + '&showWishList=' + showWishList + '&pageName=' + pageName,
		//url: '/jstemplate/ajaxTestingRegistry.jsp',
		success: function (response) {
			$("#quickviewModal").modal("hide");
			var CPProductQty = $(_this).parents('.cp-stock-details').find('#ItemQty').val() || 1;
			if (typeof response.mRegistryDetail !== "undefined" && typeof response.mRegistryDetail.mResponseStatus !== "undefined" && response.mRegistryDetail.mResponseStatus.mIndicator == "SUCCESS") {
				var registryDetails = response.mRegistryDetail.mRegistry.mRegistryAccountDetailList, registryDetailsLength = registryDetails.length;
				if (registryDetailsLength == 1) {
					if (pageName == 'collectionPage') {
						registryWishListCookie('true', _this);
					} else {
						registryWishListCookie('true');
					}
					var eventType = registryDetails[0].mEventDescription;
					if (typeof eventType === "undefined") {
						eventType = registryDetails[0].mEventType;
					};
					//$("#items-added-success-wishlist").find(".gift_recipient_name").text(registryDetails[0].mCoregistrantFirstName);
					$("#items-added-success-wishlist").find(".event_type_name").text(eventType);
					$("#items-added-success-wishlist").find(".event_date").text(registryDetails[0].mEventDate);
					$("#items-added-success-wishlist").modal("show");
					var wishlistId = $('#rgs_wishlistId').val();
					var customerEmail = $('#customerEmail').val();
					var customerID = $('#customerID').val();
					var partnerName = $('#partnerName').val();
					if (typeof utag != 'undefined') {
						utag.link({
							event_type: 'wishlist_add',
							customer_id: customerID,
							customer_email: customerEmail,
							wishlist_id: wishlistId,
							partner_name: partnerName
						});
					}
				} else if (registryDetailsLength > 1) {
					//console.log(registryDetails);
					if ($('.product-details-template').length > 0 || $('.collection-template').length > 0) {
						var primaryimage = '';
						if (pageName == 'collectionPage') {
							primaryimage = $(_this).parents(".collection-product-block").find("img.collection-image").attr("src") || '';
							skuDisplayName = $(_this).parents(".collection-product-block").find("h4 a").text();
						} else {
							primaryimage = $("#carousel-bounding-box .item.active img.zoomImage").attr('src') || '';
						}
						if (primaryimage) {
							if (primaryimage.lastIndexOf('?') > -1) {
								primaryimage = primaryimage.substring(0, primaryimage.lastIndexOf('?')) + "?fit=inside|136:136";
							} else {
								primaryimage = primaryimage + "?fit=inside|136:136";
							}

						} else if (typeof akamaiNoImageURL != 'undefined' && akamaiNoImageURL != '' && akamaiNoImageURL != null) {
							primaryimage = akamaiNoImageURL + "?fit=inside|136:136";
						}
						var wishlistTemplateContainer = $("#multiple-items-added-success-wishlist .wishlist-holder");
						$(wishlistTemplateContainer).html("");
						var wishlistTemplate = "";
						for (var item = 0; item < registryDetails.length; item++) {
							var eventType = registryDetails[item].mEventDescription;
							if (typeof eventType === "undefined") {
								eventType = registryDetails[item].mEventType ? registryDetails[item].mEventType : "-";
							};
							var firstName = registryDetails[item].mCoregistrantFirstName ? registryDetails[item].mCoregistrantFirstName : "-";
							var mEventDate = registryDetails[item].mEventDate ? registryDetails[item].mEventDate : "-";

							var mRegistryNumber = registryDetails[item].mRegistryNumber;
							wishlistTemplate += '<div class="content"><span>' + firstName + '</span><span>' + eventType + '</span><span>' + mEventDate + '</span><button class="select_wish_list_button" data-registry-number=' + mRegistryNumber + '>select wish list</button></div>'
						}
						$(wishlistTemplateContainer).append(wishlistTemplate);
						$multipleItemsAddedSuccessWishlist = $("#multiple-items-added-success-wishlist");
						$multipleItemsAddedSuccessWishlist.find(".wishlistCount").text(registryDetailsLength);
						$multipleItemsAddedSuccessWishlist.find('#wishlistCPProductQty').val(CPProductQty);
						$multipleItemsAddedSuccessWishlist.modal("show");
						$multipleItemsAddedSuccessWishlist.find(".image-name-container img").attr('src', primaryimage);
						if (pageName == 'collectionPage') {
							$("#multiple-items-added-success-wishlist").find(".mws-product-name").text(skuDisplayName);

						}
					}
					else {
						$("#multiple-items-added-success-wishlist").find(".wish-lists .row").html('');
						for (var item = 0; item < registryDetails.length; item++) {
							var wishlistTemplate = document.createElement("div");
							var eventType = registryDetails[item].mEventDescription;
							if (typeof eventType === "undefined") {
								eventType = registryDetails[item].mEventType;
							};
							$(wishlistTemplate).html($("#multiple-wishlist-js-template").html());
							$(wishlistTemplate).find(".gift_recipient_name").text(registryDetails[item].mCoregistrantFirstName);
							$(wishlistTemplate).find(".event_type_name").text(eventType);
							$(wishlistTemplate).find(".event_date").text(registryDetails[item].mEventDate);
							$(wishlistTemplate).find(".select_wish_list_button").attr('data-registry-number', registryDetails[item].mRegistryNumber);
							//$(wishlistTemplate).addClass("col-md-3 multiple-wishlist-template");
							$("#multiple-items-added-success-wishlist").find(".wish-lists .row").append(wishlistTemplate);
							$("#multiple-items-added-success-wishlist").find(".multiple-wishlist-count").text(registryDetailsLength);
							$("#multiple-items-added-success-wishlist").modal("show");
							/*$("#multiple-items-added-success-wishlist").find('.tse-scrollable').TrackpadScrollEmulator('recalculate');*/
						}
					}
				} else {
					$("#error-registry-dailog").modal("show");
				}
			} else if ((typeof response.mValidation != 'undefined' && typeof response.mValidation.mError.mInformational !== "undefined" && response.mValidation.mError.mInformational == "error")
				|| (typeof response.mRegistryDetail !== "undefined" && response.mRegistryDetail.mResponseStatus.mIndicator == "FAILURE")) {
				$("#error-registry-dailog").modal("show");
			} else {
				$("#error-registry-dailog").modal("show");
				console.log("error");
			}
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			$("#error-registry-dailog").modal("show");
			console.log(errorThrown)
		},
		timeout: timeout
	});
}
/* END - For Invoking wishlist functionality */

/* For Collection Page */
var maximumValue = 5;
var minValue = 0;

//var counter = $('.stepper > .counter');
//var decrease = $('.stepper > .decrease');
//var increase = $('.stepper > .increase');
function clearInput() {
	var qtyTag = $('.cp-item-qty');
	var itemsSelectedText = (typeof collectionItemsSelected != 'undefined' && collectionItemsSelected != '') ? collectionItemsSelected : 'items Selected';
	var zeroVal = (typeof zero != 'undefined' && zero != '') ? zero : 0;
	/*if(zeroVal =='' || zeroVal =='undefined'){
		zeroVal=0;
		}
	if(itemsSelectedText =='' || itemsSelectedText == 'undefined'){
		itemsSelectedText="items Selected";
	}*/

	if (qtyTag.length) {
		qtyTag.val(0);
		$(document).find(".add-to-cart").attr("disabled", "disabled");
		$('.cp-decrease').attr("disabled", "disabled");
		$('.cp-decrease').addClass('disabled').removeClass('enabled');
		$(document).find(".collections-items-selected").text(zeroVal + " " + itemsSelectedText);
		makeFlushSessionComponent();
	}

}

$(document).on('shown.bs.modal', '#multiple-items-added-success-wishlist', function () {
	$('#multiple-items-added-success-wishlist').find('.tse-scrollable').TrackpadScrollEmulator('recalculate');
});

$(document).ready(function () {
	$("#multiple-items-added-success-wishlist").find('.tse-scrollable').TrackpadScrollEmulator();
	var qtyTag = $('.cp-item-qty');
	if (qtyTag.length) {
		makeFlushSessionComponent();
	}
	var collectionBlocks = $(".collection-product-block");
	//var compareLimit = $(".collectionLineItem").find(".cp-stock-details");

	/*for(var i=0,len1=collectionBlocks.length;i<len1;i++){
		var stackLimit = collectionBlocks[i].find(".collectionLineItem").find(".cp-stock-details").find(".colors").children();
		for(var j=0,len=stackLimit.length;j<len;j++){
			if($(stackLimit[j]).hasClass("out-of-stock")){
				if(j==len){
					$(stackLimit[i]).closest(".collectionLineItem").find(".cp-collectionQty").find(".cp-increase").removeClass('enabled').addClass('disabled');
				}
			}
			else{
				return;
			}
		}
	}*/
	$(collectionBlocks).each(function (i, val) {
		var stackLimit = $(val).find(".collectionLineItem").find(".cp-stock-details").find(".colors").children();
		for (var j = 0, len = stackLimit.length; j < len; j++) {
			if ($(stackLimit[j]).hasClass("out-of-stock")) {
				if (j == len - 1) {
					$(stackLimit[j]).closest(".collectionLineItem").find(".cp-collectionQty").find(".cp-increase").removeClass('enabled').addClass('disabled');
				}
			}
			else {
				break;
			}
		}
	});
	var PDPpageName = $('.pageName').val() || '';
	if (PDPpageName != '') {
		forStickyPriceOnload();
	}

});

function forStickyPriceOnload() {
	if ($('.product-details-template').length > 0) {
		position = $('.buy-together-and-save').offset().top;
		stickyHeight = $('.sticky-price').height();
		if ($(window).scrollTop() < 200) {
			$('.sticky-price').css({
				"position": "absolute",
				top: 0
				/*"top" : "29px"*/
			});
		}
		else if ($(window).scrollTop() >= (position - stickyHeight - 225)) {
			$('.sticky-price').css({ "position": "absolute", "top": (position - stickyHeight - 275) });
		} else {
			$('.sticky-price').css({
				"position": "fixed",
				"top": "0px",
				"z-index": "210"
			});
		}
	}
}

function makeFlushSessionComponent() {
	$.ajax({
		url: '/jstemplate/ajaxIntermediateCollection.jsp?usedFor=flush',
		type: "POST",
		data: {},
		success: function (data) {
			console.log(data);
			$("#colletinToCart").val($(data).html());
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log(errorThrown)
		}
	});
	if (enableRegistryAJAX) {
		$.ajax({
			url: '/jstemplate/ajaxIntermediateCollection.jsp?usedFor=flushRegistry',
			type: "POST",
			data: {},
			success: function (data) {
				console.log(data);
				$("#collectionToRegistry").val($(data).html());
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown)
			}
		});
	}
}

function makeAjaxCollectionQuantityUpdate(obj) {
	var productDetails = obj.find(".cp-collectionItemDetails").val();
	var inventoryStatus = obj.find(".inventoryStatus").val();
	var qtyVal = obj.find('.cp-item-qty').val();
	var ids = productDetails.split("-");
	var locationID = "";
	var productId = ids[0];
	var skuId = ids[1];
	var productTypeAndLength = obj.find(".cp-productType-activeSkulength").val();
	var array;
	var productType = '';
	var activeSkulength = 0;
	if (typeof productTypeAndLength != 'undefined') {
		array = productTypeAndLength.split("-");
		productType = array[0];
		activeSkulength = array[1];
	}
	//var productInfoJson=JSON.parse($('#productInfoJson-'+pProductId).val());
	//if(typeof locationID == 'undefined' || locationID == '' ){
	var productInfoJson = JSON.parse($('.productInfoJson-' + productId).val());
	if ((typeof productInfoJson.mColorCodeList != 'undefined' && productInfoJson.mColorCodeList.length > 0) || (typeof productInfoJson.mJuvenileSizesList != 'undefined' && productInfoJson.mJuvenileSizesList.length > 0)) {
		var colorFlag = true;
		var sizeFlag = true;
		var $element = $(obj).parents(".row.cp-stock-details");
		if (productInfoJson.mColorCodeList.length > 0) {
			colorFlag = $element.find('.colors .color-block.color-selected').length > 0;
		}
		if (productInfoJson.mJuvenileSizesList.length > 0) {
			sizeFlag = $element.find('.sticky-sizes .size-option.size-selected').length > 0;
		}
		if (colorFlag && sizeFlag) { }
		else {
			return;
		}
	}
	var allActiveSkuVariants = productInfoJson.mActiveSKUsList;
	var onlineInventoryStatus = '';
	var storeInventoryStatus = '';
	var ispu = '';
	var s2s = '';
	if (typeof allActiveSkuVariants != 'undefined') {
		for (var indx in allActiveSkuVariants) {
			var skuIdLocal = allActiveSkuVariants[indx].mId;
			if (skuIdLocal === skuId) {
				onlineInventoryStatus = allActiveSkuVariants[indx].mInventoryStatus;
				storeInventoryStatus = allActiveSkuVariants[indx].mInventoryStatusInStore;
				ispu = allActiveSkuVariants[indx].mIspu;
				s2s = allActiveSkuVariants[indx].mS2s;
				break;
			}
		}
	}
	//if(typeof onlineInventoryStatus !='undefined' && onlineInventoryStatus !='' && onlineInventoryStatus =='outOfStock'){
	var locationString = $.cookie('favStore');
	var values = '';
	if (typeof locationString != 'undefined') {
		values = locationString.split('|');
	}
	if (typeof values != 'undefined' && values.length >= 2) {
		locationID = values[1];
	}
	//	}
	//}

	$.ajax({
		url: '/jstemplate/ajaxIntermediateCollection.jsp',
		type: "POST",
		data: { productId: ids[0], skuId: ids[1], locationId: locationID, inventoryStatus: inventoryStatus, storeInventoryStatus: storeInventoryStatus, ispu: ispu, s2s: s2s, quantity: qtyVal, usedFor: 'constructions', productType: productType, activeSkulength: activeSkulength },
		async: false,
		success: function (data) {
			//console.log(data);
			$("#colletinToCart").val($(data).html());
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log(errorThrown)
		}
	});
	if (enableRegistryAJAX) {
		$.ajax({
			url: '/jstemplate/ajaxIntermediateCollection.jsp',
			type: "POST",
			data: { productId: ids[0], skuId: ids[1], locationId: locationID, inventoryStatus: inventoryStatus, quantity: qtyVal, usedFor: 'registry', productType: productType, activeSkulength: activeSkulength },
			async: false,
			success: function (data) {
				//console.log(data);
				$("#collectionToRegistry").val($(data).html());
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown)
			}
		});
	}

}

function setAddToRegistryButtonState(obj, qtyVal) {
	try {
		var productInfoJson = JSON.parse($(obj).parents(".cp-stock-details").find("[class^='productInfoJson']").val());
	}
	catch (e) { console.log(e) }
	var ispu = productInfoJson.mDefaultSKU.mIspu;
	var s2s = productInfoJson.mDefaultSKU.mS2s;
	if (productInfoJson.hasOwnProperty("mDefaultSKU") && productInfoJson.mDefaultSKU.hasOwnProperty("mSkuRegistryEligibility") && productInfoJson.mDefaultSKU.hasOwnProperty("mRegistryWarningIndicator")) {
		var registryWarningIndicator = productInfoJson.mDefaultSKU.mRegistryWarningIndicator;
		var skuRegistryEligibility = productInfoJson.mDefaultSKU.mSkuRegistryEligibility;
	}
	else {
		var registryWarningIndicator = obj.find(".registryWarningIndicator").val();
		var skuRegistryEligibility = obj.find(".skuRegistryEligibility").val();
	}
	setAddToCartAddToRegistryButtonState(registryWarningIndicator, skuRegistryEligibility, qtyVal)
}

function setAddToCartAddToRegistryButtonState(registryWarningIndicator, skuRegistryEligibility, qtyVal) {
	var selectedItemsCounter = 0;
	var selectedItemsCounterToAddtoCart = 0;
	var selectedItemsCounterToRegistry = 0;
	var showRegistryWarnToolTip = 0;
	//var countText="0 items selected";
	var itemsSelectedText = collectionItemsSelected;
	var zeroVal = zero;
	if (zeroVal == '' || zeroVal == 'undefined') {
		zeroVal = 0;
	}
	if (itemsSelectedText == '' || itemsSelectedText == 'undefined') {
		itemsSelectedText = "items Selected";
	}
	var countText = zeroVal + " " + itemsSelectedText;
	$(document).find(".collection-product-block").each(function () {
		if ($(this).find(".cp-item-qty").val() > 0) {
			/*var colorSelected = ($(this).find(".color-block").length > 0) ? (($(this).find(".color-selected").length > 0) ? true : false) : true;
			var sizeSelected = ($(this).find(".size-option").length > 0) ? (($(this).find(".size-selected").length > 0) ? true : false) : true;*/
			var colorSelectedForRegistry = ($(this).find(".color-block").length > 0) ? (($(this).find(".color-selected").length > 0) ? true : false) : true;
			var sizeSelectedForRegistry = ($(this).find(".size-option").length > 0) ? (($(this).find(".size-selected").length > 0) ? true : false) : true;
			var colorSelected = ($(this).find(".color-block").length > 0) ? (($(this).find(".color-selected").length > 0) ? (($(this).find(".color-selected.out-of-stock").length > 0) ? false : true) : false) : true;
			var sizeSelected = ($(this).find(".size-option").length > 0) ? (($(this).find(".size-selected").length > 0) ? (($(this).find(".size-selected.out-of-stock").length > 0) ? false : true) : false) : true;
			try {
				var productInfoJson = JSON.parse($(this).find(".cp-stock-details").find("[class^='productInfoJson']").val());
			}
			catch (e) { console.log(e) }
			if (productInfoJson.hasOwnProperty("mDefaultSKU")) {
				var registryWarningIndicator = productInfoJson.mDefaultSKU.mRegistryWarningIndicator;
				var skuRegistryEligibility = productInfoJson.mDefaultSKU.mSkuRegistryEligibility;
				var inventoryStatus = productInfoJson.mDefaultSKU.mInventoryStatus;
				var storeAvailability = productInfoJson.mDefaultSKU.mInventoryStatusInStore;
				var ispu = productInfoJson.mDefaultSKU.mIspu;
				var s2s = productInfoJson.mDefaultSKU.mS2s;
			}
			else {
				var storeAvailability = $(this).find(".storeAvailability").val();
				var registryWarningIndicator = $(this).find(".registryWarningIndicator").val();
				var skuRegistryEligibility = $(this).find(".skuRegistryEligibility").val();
				var inventoryStatus = $(this).find(".inventoryStatus").val();
			}
			var locationIdFromCookie = $(this).find(".locationIdFromCookie").val();
			if ((colorSelectedForRegistry && sizeSelectedForRegistry)) {
				selectedItemsCounter++;
			}
			if ((colorSelected && sizeSelected) && ((locationIdFromCookie.length <= 0 && inventoryStatus != "outOfStock") || (locationIdFromCookie.length > 0 && !(s2s === 'N' && ispu === 'N') && (inventoryStatus == "inStock" || storeAvailability == "inStock")) || (locationIdFromCookie.length > 0 && s2s === 'N' && ispu === 'N' && inventoryStatus == "inStock"))) {
				selectedItemsCounterToAddtoCart++;
			}
			if ((colorSelectedForRegistry && sizeSelectedForRegistry) && (registryWarningIndicator == "ALLOW" || registryWarningIndicator == "WARN") && skuRegistryEligibility == true) {
				selectedItemsCounterToRegistry++;
			}
			if (!(registryWarningIndicator == "ALLOW" || registryWarningIndicator == "Allow")) {
				showRegistryWarnToolTip++;
			}
		}
	});
	if (selectedItemsCounterToAddtoCart == 0) {
		$(document).find(".add-to-cart").attr("disabled", "disabled");
		$(document).find(".collections-items-selected").text(countText);
	}
	if (selectedItemsCounter == 0) {
		$(document).find("#add-to-button .small-orange").addClass("desableLinkW3c");
	} else {
		$.ajax({
			url: '/jstemplate/collectionAddToRegistry.jsp',
			type: "POST",
			async: false,
			data: { registryWarningIndicator: registryWarningIndicator, skuRegistryEligibility: skuRegistryEligibility, qtyVal: qtyVal, selectedItemsCounterToRegistry: selectedItemsCounterToRegistry },
			success: function (data) {
				$("#add-to-button").html(data);
				if (selectedItemsCounterToAddtoCart == 0) {
					$(document).find(".add-to-cart").attr("disabled", "disabled");
					$(document).find(".collections-items-selected").text(countText);
					$(".collections-sticky-price #add-to-button .inline>a").popover("destroy");
				} else {
					if (selectedItemsCounterToAddtoCart == 1) {
						countText = selectedItemsCounterToAddtoCart + " " + itemsSelectedText;
					} else {
						countText = selectedItemsCounterToAddtoCart + " " + itemsSelectedText;
					}
					$(document).find(".add-to-cart").removeAttr("disabled");
					$(document).find(".collections-items-selected").text(countText);
					if ((showRegistryWarnToolTip > 0)) {
						$(".collections-sticky-price #add-to-button .inline>a").popover({
							animation: 'true',
							html: 'true',
							content: 'The items(s) you want to add may not be avilable for purchase from your wishlist in the future. Consider purchasing the item(s) now, and check your local store if we are out of stock online.',
							placement: 'top',
							trigger: 'hover'
						});
					}
				}
				//cosnole.log(qtyVal);
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
			}
		});
	}

}

$(document).on('click', '.cp-decrease', function () {
	var parent = $(this).closest('.stepper.cp-collectionQty');
	parent.parent().find('.cp-qty-errorMsg').addClass('hide');
	var qtyVal = parent.find('.cp-item-qty').val();
	parent.find('.cp-increase').popover('hide');
	flag = 0;

	if ($(this).hasClass('disabled') || isNaN(qtyVal) || (parseInt(qtyVal) == 0)) return false;
	var thisCounter = parent.find('.counter.cp-item-qty');
	var thisQtyLimit = parent.find('.cp-quantity-limit-value');
	maximumValue = thisQtyLimit.val();
	var value = parseInt(thisCounter.val()) - 1;
	//console.log(value);
	if (isNaN(value))
		value = 0;
	else if (value > maximumValue)
		value = maximumValue;
	else if (value < minValue)
		value = minValue;
	else if (value == 0) {
		parent.find('.counter.cp-item-qty').val(value);
		$(document).find(".add-to-cart").attr("disabled", "disabled");
		$('.collections-sticky-price .small-orange').addClass("desableLinkW3c");
	}
	thisCounter.val(value);
	setButtonState(thisCounter);
	var fromOverlay;
	if ($(this).hasClass('overlay')) {
		fromOverlay = 'addToCartOverlay';
	}
	if (value == 0) {
		//$(this).parents(".sticky-quantity").find(".collection_registry_unavailable_error").remove();
		$(this).parents(".sticky-quantity").find('.collection_registry_unavailable_error').addClass('hide');
		//$("a[onclick*=checkAddToRegistryAvailability]")
		$(".collections-sticky-price #add-to-button .inline>a").popover("destroy");
	}
	//setStatusAddToCartButtonState($(this));
	makeAjaxCollectionQuantityUpdate(parent);
	setAddToRegistryButtonState(parent, value);
	//updateCartItemQuantity($(this).attr("id"), value, fromOverlay);
});

var flag = 0;

$(document).on('click', '.cp-increase', function () {
	var parent = $(this).closest('.stepper.cp-collectionQty');
	var productId = $(parent).find(".cp-collectionItemDetails").val().split("-")[0];
	var productInfoJson = JSON.parse($(this).parents('.cp-stock-details').find('.productInfoJson-' + productId).val());
	if (productId == productInfoJson.mProductId) {
		if ((productInfoJson.mDefaultSKU.mInventoryStatus == "outOfStock") && ((productInfoJson.mDefaultSKU.mIspu == "Y" && (productInfoJson.mDefaultSKU.mS2s == "N" || productInfoJson.mDefaultSKU.mS2s == "Y")) || (productInfoJson.mDefaultSKU.mIspu == "N" && productInfoJson.mDefaultSKU.mS2s == "Y"))) {
			$(this).popover("toggle");
		}
	}
	var qtyVal = parent.find('.cp-item-qty').val();
	var locationIdFromCookie = parent.find('.locationIdFromCookie').val();
	var channelAvailability = parent.find('.channelAvailability').val();
	var inventoryStatus = parent.find('.inventoryStatus').val();
	flag++;
	if (inventoryStatus.indexOf('outOfStock') >= 0 && channelAvailability.indexOf('In Store Only') >= 0 && locationIdFromCookie == "") {
		//if(inventoryStatus.indexOf('outOfStock') >= 0){
		if (flag <= 1) {
			channelAvailablityTooltip();
			parent.find('.cp-increase').popover('show')
		} else {
			parent.find('.cp-increase').popover('hide')
		}
	}

	if ($(this).hasClass('disabled') || isNaN(qtyVal)) return false;
	//var thisStepper = $(this).parent();
	var thisCounter = parent.find('.counter.cp-item-qty');
	var thisQtyLimit = parent.find('.cp-quantity-limit-value');
	//var isPresalable = thisStepper.find('#isPreSalableQuantityLimit').val();
	maximumValue = parseInt(thisQtyLimit.val());
	var value = parseInt(thisCounter.val()) + 1;
	if (isNaN(value)) {
		value = 0;
	} else if (value >= maximumValue) {
		value = maximumValue;
		parent.parent().find('.cp-qty-errorMsg').removeClass('hide');

	} else if (value < minValue) {
		value = minValue;
	}
	thisCounter.val(value);
	setButtonState(thisCounter);
	var fromOverlay;
	if ($(this).hasClass('overlay')) {
		fromOverlay = 'addToCartOverlay';
	}
	//updateCartItemQuantity($(this).attr("id"), value, fromOverlay);
	//setStatusAddToCartButtonState($(this));
	makeAjaxCollectionQuantityUpdate(parent);
	setAddToRegistryButtonState(parent, value);
	if (productInfoJson.mDefaultSKU.mRegistryWarningIndicator && (productInfoJson.mDefaultSKU.mRegistryWarningIndicator == "DENY" || productInfoJson.mDefaultSKU.mSkuRegistryEligibility == false) || (productInfoJson.mDefaultSKU.mSkuRegistryEligibility && productInfoJson.mDefaultSKU.mRegistryWarningIndicator == "WARN" && productInfoJson.mDefaultSKU.mSkuRegistryEligibility == true)) {
		//$("a[onclick*=checkAddToRegistryAvailability]").popover({
		$(".collections-sticky-price #add-to-button .inline>a").popover({
			animation: 'true',
			html: 'true',
			placement: 'top',
			trigger: 'hover'
		});
	}

});

/*pdp product channelAvailablityTooltip start*/
function channelAvailablityTooltip() {
	var itemsnotavailableText = itemsnotavailable;

	if (itemsnotavailable == '' || itemsnotavailable == 'undefined') {
		itemsnotavailable = 'This item is not available for ship to home.Please check for the availability at a store near you using the "Find in store" link and add the item to car';
	}
	$(document).on('click', '.cp-increase', function (e) {
		e.preventDefault();
	});
	$('.cp-increase').popover({
		animation: 'true',
		html: 'true',
		content: 'itemsnotavailable',
		placement: 'bottom',
		trigger: 'manual'
	});
}
/*pdp product channelAvailablityTooltip end*/


$(document).on('keyup', '.cp-item-qty', function () {
	var qtyVal = $(this).val();
	var parent = $(this).closest('.stepper.cp-collectionQty');
	var qtyLimit = parseInt(parent.find('.cp-quantity-limit-value').val());
	//var isPresalable1 = parent2.find('#isPreSalableQuantityLimit').val();
	maximumValue = parseInt(($(this).parent().find('.cp-quantity-limit-value').val() == "") ? "999" : $(this).parent().find('.cp-quantity-limit-value').val());
	var qty = parseInt(qtyVal);
	//var maxQtyLimit = parseInt(qtyLimit);
	if (qty >= 999) {
		$(this).parent().find('.cp-increase').addClass('disabled').removeClass('enabled');
		$(this).parent().find('.cp-decrease').removeClass('disabled').addClass('enabled');
		parent.parent().find('.cp-qty-errorMsg').removeClass('hide');
	} else if (qty <= 0 || qty == "" || isNaN(qty)) {
		parent.parent().find('.cp-qty-errorMsg').addClass('hide');
		$(this).parent().find('.cp-increase').addClass('enabled').removeClass('disabled');
		$(this).parent().find('.cp-decrease').removeClass('enabled').addClass('disabled');
	}
	else if (qty > qtyLimit) {
		parent.find(".cp-increase").removeClass('enabled').addClass('disabled');
	}
	else {
		parent.parent().find('.cp-qty-errorMsg').addClass('hide');
		$(this).parent().find('.cp-increase, .decrease').removeClass('disabled');
		$(this).parent().find('.cp-increase, .decrease').addClass('enabled');
	}
	var $errorMsg = $(this).closest('.cp-stock-details').find(".cp-qty-errorMsg");

	if (qty == maximumValue) {
		$(this).parent().find('.cp-increase').addClass('disabled').removeClass('enabled');
		$errorMsg.removeClass('hide');
	} else if (qty > maximumValue) {
		$(this).parent().find('.cp-increase').addClass('disabled').removeClass('enabled');
		$errorMsg.addClass('hide');
	} else {
		$errorMsg.addClass('hide');
		$(this).parent().find('.cp-increase').addClass('enabled').removeClass('disabled');
	}
	//setStatusAddToCartButtonState($(this))
	setAddToRegistryButtonState(parent, qty);
	/* if($('.product-details-template').length == 0){
		setStatusAddToCartButtonState($(this));
	} */
});

$('input[type=text]').on('keyup', '.cp-item-qty', function (e) {
	if (e.which == 13) {
		e.preventDefault();
		var parent = $(this).closest('.stepper.cp-collectionQty');
		var thisCounter = parent.find('.counter.cp-item-qty');
		var value = parseInt(thisCounter.val());
		if (isNaN(value)) value = 0;
		else if (value > maximumValue) value = maximumValue;
		else if (value < minValue) value = minValue;
		thisCounter.val(value);
		setButtonState(thisCounter);
	}
});

function setButtonState(counter) {
	var value = parseInt(counter.val());
	var decrease = counter.parent().find('.decrease');
	var increase = counter.parent().find('.increase');
	if (value <= minValue) {
		decrease.removeClass('enabled');
		decrease.addClass('disabled');
	}
	else {
		decrease.removeClass('disabled');
		decrease.addClass('enabled');
	}
	if (value >= maximumValue) {
		increase.removeClass('enabled');
		increase.addClass('disabled');
	}
	else {
		increase.removeClass('disabled');
		increase.addClass('enabled');
	}
}

/*function setStatusAddToCartButtonState($this) {
	var selectedItemsCounter = 0;
	$(document).find(".collection-product-block").each(function(){
		if($(this).find(".cp-item-qty").val() > 0){
			var colorSelected = ($(this).find(".color-block").length > 0) ? (($(this).find(".color-selected").length > 0) ? true : false) : true;
			var sizeSelected = ($(this).find(".size-option").length > 0) ? (($(this).find(".size-selected").length > 0) ? true : false) : true;
			var locationIdFromCookie = $(this).find("#locationIdFromCookie").val();
			var inventoryStatus = $(this).find("#inventoryStatus").val();
			var storeAvailability = $(this).find("#storeAvailability").val();

			if((colorSelected && sizeSelected) && ((locationIdFromCookie.length <= 0 && inventoryStatus != "outOfStock") || (locationIdFromCookie.length > 0 && (inventoryStatus != "outOfStock" || storeAvailability == "available")))){
				$(document).find(".add-to-cart").removeAttr("disabled");
				selectedItemsCounter++;
			}
		}
	});
	var countText="0 items selected";
	if(selectedItemsCounter == 0){
		$(document).find(".add-to-cart").attr("disabled","disabled");
		$(document).find(".collections-items-selected").text(countText);
	}else{
		if(selectedItemsCounter == 1){
			countText = selectedItemsCounter + " item selected";
		}else{
			countText = selectedItemsCounter + " items selected"
		}
		$(document).find(".collections-items-selected").text(countText);
	}

}*/

$(document).on('blur', '.cp-item-qty', function () {
	var maxvalue = 999;
	var qty = $(this).val();
	var value = parseInt($(this).val());
	var parent = $(this).closest('.stepper.cp-collectionQty');
	var thisQtyLimit = parent.find('.cp-quantity-limit-value').val();
	maxQtyValue = parseInt(thisQtyLimit);
	if (value >= maxvalue) {
		parent.find(".cp-increase").addClass('disabled');
	} else if (value > maxQtyValue) {
		parent.find(".cp-increase").removeClass('enabled').addClass('disabled');
	}
	else {
		parent.find(".cp-increase").removeClass('disabled').addClass('enabled');
	}
	if (isNaN(value)) {
		value = 0;
	}
	$(this).val(value);
	makeAjaxCollectionQuantityUpdate(parent);
});

$(document).on('keyup', '.cp-item-qty', function () {
	if ($(this).val() > 0) {
		makeAjaxCollectionQuantityUpdate($(this).parent());
	}
});

//var learnMoreTooltip = $('.learn-more-tooltip');
//learnMoreTooltipTemplate = $('.tooltip-container').html();

/* START - pdp product not available tooltip */
function notAvaialbeTooltip() {
	$(document).on('mouseenter', '.not-available', function (e) {
		e.preventDefault();
	});
	try {
		$('.not-available').popover({
			animation: 'true',
			html: 'true',
			content: 'this selection is currently not available for purchase',
			placement: 'top',
			trigger: 'hover'
		});
	}
	catch (e) {
		console.log(e);
	}
}
/* END - pdp product not available tooltip */

/* START - pdp product not carried tooltip */
function notCarriedTooltip() {
	$(document).on('mouseenter', '.not-carried', function (e) {
		e.preventDefault();
	});
	try {
		$('.not-carried').popover({
			animation: 'true',
			html: 'true',
			content: 'Sorry; we do not carry this combination',
			placement: 'top',
			trigger: 'hover'
		});
	}
	catch (e) {
		console.log(e);
	}
}
/* END - pdp product not carried tooltip */

/* End Collection Page */
function collectionStickyPrice() {
	if ($(".collection-template").length) {
		var position = $('.collection-hero').offset().top;
		var stickyHeight = $('.collections-sticky-price').height();
		position -= stickyHeight;
		var carousalScrollTop;
		if ($('.my-account-product-carousel').length) {
			carousalScrollTop = ($('.my-account-product-carousel').offset().top);
		}
		else {
			carousalScrollTop = $(".collections-sticky-price").parents('.row').offset().top + $(".collections-sticky-price").parents('.row').height();
			//carousalScrollTop = 2035;
		}
		$('.collections-sticky-price').css({
			"position": "absolute",
			"top": "0px"
		});
		$(window).scroll(function () {
			//alert(1);
			position = $('.collection-hero').offset().top;
			stickyHeight = $('.collections-sticky-price').height();
			if ($(window).scrollTop() <= 500 - stickyHeight) {
				$('.collections-sticky-price').css({ "position": "absolute", "top": "0px" });
			}
			else if ($(window).scrollTop() >= (carousalScrollTop - stickyHeight - 235)) {
				var stickyPriceTop = (carousalScrollTop - stickyHeight - 350 - $('.collection-hero').height()) + "px";
				//console.log(stickyPriceTop);
				$('.collections-sticky-price').css({ "position": "absolute", "top": stickyPriceTop });
			}
			else {
				$('.collections-sticky-price').css({
					"position": "fixed",
					"top": "176px"
				});
			}
		});
	}
}

function loadFindInStoreForCollections(contextPath, selectedProductId, selectedSkuId, relationshipItemId, relItemQtyInCart, $this) {
    /*var quantity;
    var $this = $($this);
        quantity = $this.parents(".collection-product-block").find(".cp-item-qty").val();
	if(!relationshipItemId == '')
		quantity = relItemQtyInCart;*/
	$(".productIdForCart").val(selectedProductId);
	/*if(quantity == 0|| quantity == undefined || quantity== null){
		findInStoreTooltipfn();
		$this.parent().find(".findInStore").popover("toggle");
	}else{*/
	var quantity = $('#collectonRefresh-' + selectedProductId + ' #ItemQty').val();
	loadFindInStore(contextPath, selectedSkuId, relationshipItemId, quantity, selectedProductId);

	/*
	$this.siblings('.findInStoreModal-btn').click();
	$('#findInStoreModal').find('.modal-dialog').before('<div class="modal-backdrop fade in" style="height: 100%;"></div>');
	$.ajax({
		type: 'POST',
		dataType: 'html',
		url: '/tru/storelocator/findInStorePDPInfo.jsp?relationshipItemId='+relationshipItemId+"&quantity="+quantity,
		data: 'selectedSkuId='+selectedSkuId,
		async:false,
		success: function(resData){
			$('#findInStoreModal').html(resData);

			$('#findInStoreModal').find("#find-in-store-results header, #find-in-store-results #store-locator-results-tabs, #find-in-store-results #store-locator-results-scrollable, #find-in-store-results .find-in-store-search img").hide();
			$('#findInStoreModal').find("#find-in-store-results .find-in-store-search").show();

			$('#findInStoreModal').show();
			$('#quickviewModal').hide();
			getfavStore = getCookie("favStore");
			if(getfavStore != ""){
				findinstorecommon('NO');
			}
		}
	});
	*/
	/*}
*/

	return true;
}
var findInStoreTooltipfn = function () {
	$('#findInStoreModal').find('.modal-dialog').before('<div class="modal-backdrop fade in" style="height: 100%;"></div>');

	$(document).on('click', '.findInStore', function (e) {
		e.preventDefault();
	});
}

$(document).on("click", function (event) {
	if (!$(event.target).closest('.findInStore').length && !$(event.target).is('.findInStore')) {
		if ($(".findInStore").parent().find('.popover').is(":visible")) {
			$('.findInStore').popover("hide");
		}
	}
});

var collectionImageGallery = function () {
	if ($(".collections-template").length) {
		bruCss();
		$(document).on("click", ".collectionLineItem .sticky-price-contents .image-gallery-link", function () {
			var id = $(this).closest(".row.collection-product-block").find(".productSkuId").val().split("-");
			var collectionId = $(this).closest(".template-content.collection-template").find(".hiddenCollectionId").val();
			var childProductZoomImageBlock = '';
			var childProductPriAltImageBlock = '';
			var childProductAltImageBlock = '';
			var val1 = $('#childProductZoomImageBlock').val();
			var val2 = $('#childProductPriAltImageBlock').val();
			var val3 = $('#childProductAltImageBlock').val();
			var val4 = $('#childProductSecAltImageBlock').val();
			if (typeof val1 != 'undefined') {
				childProductZoomImageBlock = $('#childProductZoomImageBlock').val();
			}
			if (typeof val2 != 'undefined') {
				childProductPriAltImageBlock = $('#childProductPriAltImageBlock').val();
			}
			if (typeof val4 != 'undefined') {
				childProductSecAltImageBlock = $('#childProductSecAltImageBlock').val();
			}
			if (typeof val3 != 'undefined') {
				childProductAltImageBlock = $('#childProductAltImageBlock').val();
			}
			$.ajax({
				type: 'POST',
				dataType: 'html',
				data: { productId: id[0], skuId: id[1], collectionId: collectionId, childProductZoomImageBlock: childProductZoomImageBlock, childProductPriAltImageBlock: childProductPriAltImageBlock, childProductSecAltImageBlock: childProductSecAltImageBlock, childProductAltImageBlock: childProductAltImageBlock },
				url: '/jstemplate/ajaxCollectionLineItemImagegallery.jsp',
				success: function (data) {
					//console.log(data);
					//var data = $(data).find(".modal-dialog").html();
					$("#galleryOverlayModal").html(data).modal('show');
					galleryOverlayModalArrowShowHide();
					$("#galleryOverlayModal").find('.wrapper').TrackpadScrollEmulator();
				},
				error: function (e) {
					console.log(e);
				}
			});
		})
	}
}

var checkAddToRegistryAvailability = function (ele) {
	var $this = $(ele);
	if ($this.attr("disabled") != "disabled") {
		var selectedItemsCounterToRegistry = 0, nonEligibleProducts = 0;
		$(document).find(".collection-product-block").each(function () {
			if ($(this).find(".cp-item-qty").val() > 0) {
				var colorSelected = ($(this).find(".color-block").length > 0) ? (($(this).find(".color-selected").length > 0) ? true : false) : true;
				var sizeSelected = ($(this).find(".size-option").length > 0) ? (($(this).find(".size-selected").length > 0) ? true : false) : true;
				var registryWarningIndicator = $(this).find(".registryWarningIndicator").val();
				var skuRegistryEligibility = $(this).find(".skuRegistryEligibility").val();
				if ((colorSelected && sizeSelected) && ((registryWarningIndicator == "ALLOW" || registryWarningIndicator == "WARN") && skuRegistryEligibility == "true")) {
					selectedItemsCounterToRegistry++;
				} else {
					nonEligibleProducts++;
					var qtyBlockWidth = $(this).find(".stepper.cp-collectionQty").outerWidth();
					/*if(!$(this).find(".stepper.cp-collectionQty").parent().find(".collection_registry_unavailable_error").length){
						$(this).find(".stepper.cp-collectionQty").after("<p class='collection_registry_unavailable_error'>some of the items is not eligible for registry<p>");
					}*/
					if ($(ele).data("href").indexOf("wishlist") > 0) {
						$(this).find(".sticky-quantity").find('.collection_registry_unavailable_error').html(wishlistErrorMessage).removeClass('hide');
						/*$('.collection_registry_unavailable_error').remove();
						$(this).find(".stepper.cp-collectionQty").after("<p class='collection_registry_unavailable_error'>item is not eligible for wishlist<p>");*/
					}
					else {
						$(this).find(".sticky-quantity").find('.collection_registry_unavailable_error').html(registryErrorMessage).removeClass('hide');
						/*$('.collection_registry_unavailable_error').remove();
						$(this).find(".stepper.cp-collectionQty").after("<p class='collection_registry_unavailable_error'>item is not eligible for registry<p>");*/
					}
					//$(this).find(".collection_registry_unavailable_error").css("left",qtyBlockWidth+20);
				}
			}
		});
		if (!nonEligibleProducts) {
			var registryUser = $.cookie('REGISTRY_USER');
			var lastAccessedRegistryID = $.cookie("lastAccessedRegistryID");
			var lastAccessedWishlistID = $.cookie("lastAccessedWishlistID");
			var wishlist_id = $.cookie("myWishlistID");
			var isRegistryEnabled = (typeof registryUser !== "undefined" && registryUser != "" && registryUser != "null" && typeof lastAccessedRegistryID !== 'undefined' && lastAccessedRegistryID != "" && lastAccessedRegistryID != "null");
			var isWishListEnabled = (typeof wishlist_id !== "undefined" && wishlist_id != "" && wishlist_id != "null" && typeof lastAccessedWishlistID !== 'undefined' && lastAccessedWishlistID != "" && lastAccessedWishlistID != "null");
			if (enableRegistryAJAX) {
				var collectionCart = $('body').find("#collectionToRegistry").val();
			} else {
				var collectionCart = $('body').find("#colletinToCart").val();
			}
			if (collectionCart != "") {
				var pArry = [], collectionArray = collectionCart.split('~');
				for (var i = 0; i < collectionArray.length; i++) {
					pArry.push(collectionArray[i].split('|')[0]);
				}
				collectionCart = pArry;
			}
			if (isRegistryEnabled || isWishListEnabled) {
				var registryArray = [];
				var isWishlist = $this.hasClass('checkWishlist');
				var doc = $(document);
				doc.find('.cp-item-qty').each(function (index, obj) {
					if ($(obj).attr("data-id") != undefined && $(obj).attr("data-id") != null && $(obj).attr("data-id") != '') {
						var itemId = $(obj).attr("data-id").split('-')[1];
						if ($(obj).val() > 0) {
							registryArray.push(doc.find(".collection_productDetails-" + itemId).val() + '|' + doc.find('.QTY-' + itemId).val());
						}
					}
				});
				doc.find('#rgs_productDetails').val(registryArray.join(','));
				console.log(doc.find('#rgs_productDetails').val());
				if (isWishlist && isWishListEnabled) {
					$(document).find('#rgs_wishlistId').val(lastAccessedWishlistID);
					invokeWishlistService();
				} else if (isRegistryEnabled && !isWishlist) {
					registryWishListCookie("false");
				} else {
					//window.location.assign($this.attr("data-href"));
					/*ajaxRegistryCall(generateAjaxProductDetailsByProductId(collectionCart),$this.hasClass('checkWishlist'),$this.attr("data-href"));*/
					var cname = ($this.hasClass('checkWishlist')) ? "addToWishListCookie " : "addToRegistryCookie";
					setRegistryWishlistCookie(cname, generateAjaxProductDetailsByProductId(collectionCart, $this.hasClass('checkWishlist')), $this.attr("data-href"));
				}
			} else {
				//window.location.assign($this.attr("data-href"));
				//ajaxRegistryCall(generateAjaxProductDetailsByProductId(collectionCart),$this.hasClass('checkWishlist'),$this.attr("data-href"));
				var cname = ($this.hasClass('checkWishlist')) ? "addToWishListCookie " : "addToRegistryCookie";
				setRegistryWishlistCookie(cname, generateAjaxProductDetailsByProductId(collectionCart, $this.hasClass('checkWishlist')), $this.attr("data-href"));
			}
		}
	}
}

$("#detailsModel,#detailsPopup,#learnMorePopup,#pdpPreOrderNow").on("shown.bs.modal", function () {
	if ($("body").find("#quickviewModal").css("display") == "block") {
		$(this).addClass("toStopScroll");
	}
});

function invokeHooklogicEvent() {
	var hookLogicCId = $('#hookLogicCId').val();
	var hookLogicSku = $('#hookLogicSku').val();
	var hookLogicRegId = $('.registry_id').val();

	if (typeof HLLibrary != 'undefined') {
		HLLibrary.newEvent()
			.setProperty("cUserId", hookLogicCId)
			.setProperty("sku", hookLogicSku)
			.setProperty("eventType", "add|registry")
			.setProperty("addId", hookLogicRegId).submit();
	}
	console.log("hookLogicCId==" + hookLogicCId);
	console.log("hookLogicSku==" + hookLogicSku);
	console.log("hookLogicRegId==" + hookLogicRegId);
}

$(document).on('click', '.gallery-lr.right', function () {
	var resizeValue = $('#zoomPrimaryMainImageBlock').val();
	if ($('.videosclass img:first')) {
		$('.gallery-lr.left').show();
	}
	var modal = $('#galleryOverlayModal');
	var displayImage = $('#galleryOverlayModal .tse-content img.zoomimage, #collectionGalleryOverlayModal .tse-content img.zoomimage');
	var selectedImage = $('.gallery-overlay-footer img.gallery-image-selected');

	if (selectedImage.next('img').length || ($('.customerImages .prImageSnippetImage .gallery-image-selected').length > 0)) {
		if ($('.customerImages .prImageSnippetImage .gallery-image-selected').length > 0) {
			selectedImage.parents('.prImageSnippetImage').next('.prImageSnippetImage').find('img').toggleClass('gallery-image-selected gallery-image-unselected');
			selectedImage.toggleClass('gallery-image-selected gallery-image-unselected');
			var selected = $('.customerImages .prImageSnippetImage .gallery-image-selected').attr('src');
			if (selected.lastIndexOf("?") >= 0) {
				selected = selected.substr(0, selected.lastIndexOf("?")) + resizeValue;
				displayImage.removeAttr('id');
			}
			else {
				displayImage.attr('id', 'gallery-noimage');
			}
			//newImageURL = selected;
			displayImage.attr('src', selected);
			if ($('.customerImages .prImageSnippetImage:last img').hasClass('gallery-image-selected')) {
				$('.gallery-lr.right').hide();
			}
			return;
		}
		selectedImage.next('img').toggleClass('gallery-image-selected gallery-image-unselected');
		selectedImage.toggleClass('gallery-image-selected gallery-image-unselected');
		var selectedImageURL = selectedImage.next().attr('src');
		if (selectedImageURL.lastIndexOf("?") >= 0) {
			selectedImageURL = selectedImageURL.substr(0, selectedImageURL.lastIndexOf("?")) + resizeValue;
			displayImage.removeAttr('id');
		}
		else {
			displayImage.attr('id', 'gallery-noimage');
		}
		var newImageURL = '';

		if (selectedImage.siblings('p').hasClass("galleryPopup-images") ||
			selectedImage.siblings('p').hasClass("customerpopup-images")) {
			newImageURL = selectedImageURL;
		}
		displayImage.attr('src', newImageURL);
		if ($('.videosclass img:first').hasClass('gallery-image-selected')) {
			$('.gallery-lr.left').hide();
		}
		else {
			$('.gallery-lr.left').show();
		}
		if ($('#galleryOverlayModal .productImages img:last').hasClass('gallery-image-selected')) {//$('.customerImages .prImageSnippetImage').length <=0 &&
			$('.gallery-lr.right').hide();
		}
		return;
	}
	var idvalue = $('.videosclass img:last').attr("id");
	if (selectedImage.attr('id') === idvalue) {
		selectedImage.toggleClass("gallery-image-selected gallery-image-unselected");
		$('#gallery-image-3').toggleClass("gallery-image-selected gallery-image-unselected");
		var selected = $('#gallery-image-3').attr('src');
		if (selected.lastIndexOf("?") >= 0) {
			selected = selected.substr(0, selected.lastIndexOf("?")) + resizeValue;
		}
		newImageURL = selected;
		displayImage.attr('src', newImageURL);
		if ($('.invodo-icon.js-invodo-action-play').hasClass('invodo-icon--pause')) {
			$('.invodo-icon.js-invodo-action-play').trigger('click');
			displayImage.removeAttr('id');
		}
		else {
			displayImage.attr('id', 'gallery-noimage');
		}
		$('#galleryOverlayModal .tse-content iframe, #collectionGalleryOverlayModal .tse-content iframe, #invodoGalleryVideo').hide();
		displayImage.show();
		//document.getElementById("ytplayer").contentWindow.postMessage('{"event":"command","func" : "pauseVideo", "args":""}', '*');
	} else if (selectedImage.attr('id') === 'gallery-image-7') {
		selectedImage.toggleClass("gallery-image-selected gallery-image-unselected");
		$('#gallery-image-8').toggleClass("gallery-image-selected gallery-image-unselected");
		displayImage.attr('src', '');
	}
	/* else if ($('.customerImages .prImageSnippetImage').length > 0){
		  selectedImage.toggleClass("gallery-image-selected gallery-image-unselected");
		  $('.customerImages .prImageSnippetImage:first img').toggleClass("gallery-image-selected gallery-image-unselected");
		  var selected=$('.customerImages .prImageSnippetImage .gallery-image-selected').attr('src');
		  newImageURL = selected;
		  displayImage.attr('src', newImageURL);

	 }*/
	if ($('.videosclass img:first').hasClass('gallery-image-selected')) {
		$('.gallery-lr.left').hide();
	}
	else {
		$('.gallery-lr.left').show();
	}
	if ($('#galleryOverlayModal .productImages img:last').hasClass('gallery-image-selected')) {//$('.customerImages .prImageSnippetImage').length <=0 &&
		$('.gallery-lr.right').hide();
	}
	modal.find('.wrapper').TrackpadScrollEmulator('recalculate');
	$('#quickviewGalleryOverlayModal').find('.wrapper').TrackpadScrollEmulator('recalculate');
});

$(document).on('click', '.gallery-lr.left', function () {
	var resizeValue = $('#zoomPrimaryMainImageBlock').val();
	$('.gallery-lr.right').show();
	var modal = $('#galleryOverlayModal');
	var displayImage = $('#galleryOverlayModal .tse-content img.zoomimage, #collectionGalleryOverlayModal .tse-content img.zoomimage');
	var selectedImage = $('.gallery-overlay-footer img.gallery-image-selected');

	if (selectedImage.prev('img').length || ($('.customerImages .prImageSnippetImage .gallery-image-selected').length > 0)) {
		if ($('.customerImages .prImageSnippetImage:first img').hasClass('gallery-image-selected')) {
			selectedImage.toggleClass('gallery-image-selected gallery-image-unselected');
			$('#galleryOverlayModal .productImages img:last').toggleClass('gallery-image-selected gallery-image-unselected');
			var selected = $('.productImages img:last').attr('src');
			if (selected.lastIndexOf("?") >= 0) {
				selected = selected.substr(0, selected.lastIndexOf("?")) + resizeValue;
				displayImage.removeAttr('id');
			}
			else {
				displayImage.attr('id', 'gallery-noimage');
			}
			//newImageURL = selected;
			displayImage.attr('src', selected);
			return;
		}
		if ($('.customerImages .prImageSnippetImage .gallery-image-selected').length > 0) {
			selectedImage.parents('.prImageSnippetImage').prev('.prImageSnippetImage').find('img').toggleClass('gallery-image-selected gallery-image-unselected');
			selectedImage.toggleClass('gallery-image-selected gallery-image-unselected');
			var selected = $('.customerImages .prImageSnippetImage .gallery-image-selected').attr('src');
			if (selected.lastIndexOf("?") >= 0) {
				selected = selected.substr(0, selected.lastIndexOf("?")) + resizeValue;
				displayImage.removeAttr('id');
			}
			else {
				displayImage.attr('id', 'gallery-noimage');
			}
			//newImageURL = selected;
			displayImage.attr('src', selected);
			return;
		}
		selectedImage.prev('img').toggleClass('gallery-image-selected gallery-image-unselected');
		selectedImage.toggleClass('gallery-image-selected gallery-image-unselected');

		var selectedImageURL = selectedImage.prev().attr('src');
		if (selectedImageURL.lastIndexOf("?") >= 0) {
			selectedImageURL = selectedImageURL.substr(0, selectedImageURL.lastIndexOf("?")) + resizeValue;
			displayImage.removeAttr('id');
		}
		else {
			displayImage.attr('id', 'gallery-noimage');
		}
		var newImageURL = '';

		if (selectedImage.siblings('p').hasClass("galleryPopup-images") ||
			selectedImage.siblings('p').hasClass("customerpopup-images")) {
			newImageURL = selectedImageURL;
		}

		displayImage.attr('src', newImageURL);
	}
	if (selectedImage.attr('id') === 'gallery-image-8') {
		selectedImage.toggleClass("gallery-image-selected gallery-image-unselected");
		$('#gallery-image-7').toggleClass("gallery-image-selected gallery-image-unselected");
		newImageURL = 'images/product-2-hero.jpg';
		displayImage.attr('src', newImageURL);
	} else if (selectedImage.attr('id') === 'gallery-image-3') {
		/* selectedImage.toggleClass("gallery-image-selected gallery-image-unselected");
					$('.videosclass img:last').toggleClass("gallery-image-selected gallery-image-unselected");
					displayImage.attr('src', '');
					$('#galleryOverlayModal .tse-content iframe, #collectionGalleryOverlayModal .tse-content iframe').show();
			displayImage.hide();*/
		selectedImage.toggleClass("gallery-image-selected gallery-image-unselected");
		$('.videosclass img:last').toggleClass("gallery-image-selected gallery-image-unselected");
		if ($('.videosclass img:first').hasClass('gallery-image-selected')) {
			$('.gallery-lr.left').hide();
		}
		else {
			$('.gallery-lr.left').show();
		}
		$('.videosclass img:last').trigger('click');
		displayImage.attr('src', '');
		$('#galleryOverlayModal .tse-content iframe, #collectionGalleryOverlayModal .tse-content iframe, #invodoGalleryVideo').show();
		$('#galleryOverlayModal .tse-content iframe, #collectionGalleryOverlayModal .tse-content iframe, #invodoGalleryVideo video').get(0).play();
		displayImage.hide();
		return;
	}
	if ($('.videosclass img:first').hasClass('gallery-image-selected')) {
		$('.gallery-lr.left').hide();
	}
	else {
		$('.gallery-lr.left').show();
	}
	if ($('#galleryOverlayModal .productImages img:first').hasClass('gallery-image-selected') && $('.videosclass img').length <= 0) {//$('.customerImages .prImageSnippetImage').length <=0 &&
		$('.gallery-lr.left').hide();
	}
	modal.find('.wrapper').TrackpadScrollEmulator('recalculate');
	$('#quickviewGalleryOverlayModal').find('.wrapper').TrackpadScrollEmulator('recalculate');
});

$(document).on('click', '.gallery-overlay-footer img', function () {
	$('.gallery-lr.left').show();
	$('.gallery-lr.right').show();
	var modal = $('#galleryOverlayModal'), newImageURL;
	var displayImage = $('#galleryOverlayModal .tse-content img.zoomimage, #collectionGalleryOverlayModal .tse-content img.zoomimage');
	if ($(this).parents('div').hasClass("customerImages")) {
		newImageURL = $(this).attr('src');
		newImageURL = newImageURL.newImageURL(0, newImageURL.lastIndexOf("?")) + resizeValue;
	}
	displayImage.attr('src', newImageURL);
	modal.find('.wrapper').TrackpadScrollEmulator('recalculate');
	if ($(this).parents('div').hasClass('videosclass')) {
		$('.gallery-lr.left').hide();
	}
	else if ($(this).parents('div').hasClass('productImages')) {
		var altImgs = $('.productImages').children('img').length;
		var ind = $(this).index();
		if (altImgs == 1 && $('.videosclass img').length <= 0) {
			$('.gallery-lr').hide();
		}
		else if (ind == 1 && $('.videosclass img').length <= 0) {
			//&& $('.customerImages .prImageSnippetImage').length <=0
			$('.gallery-lr.left').hide();
		}
		else if (ind == altImgs) {
			//&& $('.customerImages .prImageSnippetImage').length <=0
			$('.gallery-lr.right').hide();
		}
	}
	//$('#quickviewGalleryOverlayModal').find('.wrapper').TrackpadScrollEmulator('recalculate');
});

$('body').on('click', '.addToRegistryCompare', function (event) {
	invokeAkamaiURL();
	event.preventDefault();
	var productDetails = $(this).parents('.addToCartJspSection').find('#registryDetails').val();
	setRegistryAkamaiCookie(productDetails, '', false, $(this).attr('href'));
	//setRegistryWishlistCookie('addToRegistryCookie',prodDetails,$(this).attr('href'));
});

$("body").on('click', '.registryAjaxCall', ajaxIntermidiateRegistryURL);

function generateAjaxProductDetailsByProductId(productId, isWishlist) {
	if (typeof productId === "undefined" || productId == '') {
		return '';
	}
	if (Object.prototype.toString.call(productId) === '[object Array]') {
		var productDetail = [];
		for (var i = 0; i < productId.length; i++) {
			productDetail.push(generateAjaxProductDetailsByProductId(productId[i], isWishlist));
		}
		return productDetail.join(',');
	}
	var skuId;
	var parent;
	var quantity = 0;
	if ($("[data-id=collectionId-" + productId + "]").length) {
		skuId = $("[data-id=collectionId-" + productId + "]").find(".productSkuId").val().split("-")[1];
		quantity = parseInt($("[data-id=collectionId-" + productId + "]").find("#ItemQty").val());
	} else if ($("#quickViewId-" + productId).length) {
		skuId = $(".defaultSkuId-" + productId).val();
		quantity = parseInt($("#quickViewId-" + productId).find("#ItemQty").val());
	} else {
		skuId = $("#pdpSkuId").val();
		quantity = parseInt($("#ItemQty").val());
	}
	var productInfoJson = JSON.parse($('.productInfoJson-' + productId).val()), prodDArr = [];
	if (typeof productInfoJson != 'undefined') {
		var productType = productInfoJson.mColorSizeVariantsAvailableStatus;
		var activeSkus = productInfoJson.mActiveSKUsList;
		var defaultSku;
		if (productType === 'noColorSizeAvailable'
			&& typeof activeSkus != 'undefined' && activeSkus.length > 1) {
			for (var count in activeSkus) {
				if (typeof skuId != 'undefined' && skuId != '' && activeSkus[count].mId === skuId) {
					defaultSku = activeSkus[count];
					break;
				}
			}
		} else {
			defaultSku = productInfoJson.mDefaultSKU;
		}
		if (isWishlist) {
			prodDArr.push(($("rgs_wishlistRegistryType").length) ? ($("rgs_wishlistRegistryType").val().trim() != "") ? $("rgs_wishlistRegistryType").val().trim() : "G" : "G");
		} else {
			prodDArr.push(($("rgs_registryType").length) ? ($("rgs_registryType").val().trim() != "") ? $("rgs_registryType").val().trim() : "B" : "B");
		}
		var rmsColor = (typeof defaultSku.mRmsColorCode != "undefined") ? defaultSku.mRmsColorCode : 0;
		var rmsSize = (typeof defaultSku.mRmsSizeCode != "undefined") ? defaultSku.mRmsSizeCode : 0;
		prodDArr.push(getValueFromJson('mSknId', defaultSku));
		prodDArr.push(('mSkuUpcNumbers' in defaultSku) ? defaultSku.mSkuUpcNumbers[0] : '');
		prodDArr.push(getValueFromJson('mId', defaultSku));
		prodDArr.push((rmsColor || rmsSize) ? 1 : 0);
		prodDArr.push(rmsColor);
		prodDArr.push(rmsSize);
		prodDArr.push(quantity);
		return prodDArr.join('|');
	}
}

function invokeAkamaiURL() {
	var iframe = document.createElement("iframe");
	$(iframe).attr("src", "/addtoregistry.php");
	$(iframe).css({
		height: 1,
		width: 1
	});
	document.body.appendChild(iframe);
	iframe.onload = function () {
		setTimeout(function () {
			console.log('IFrame loaded.');
		}, 3000)
	}
}

function ajaxIntermidiateRegistryURL(event) {
	var hookLogicEnabledFlag = $('#hookLogicEnabledFlag').val() || '';
	if (hookLogicEnabledFlag == 'true') {
		invokeHooklogicEvent();
	}
	event.preventDefault();
	var _this = $(this), isWishlist = (_this.hasClass('wishlist'));
	var productId = '';
	if ($('.collection-template').length > 0) {
		var pId = _this.parents('.collectionLineItem').attr('data-id') ? (_this.parents('.collectionLineItem').attr('data-id')).split('-') : (_this.parents('.collectionLineItem').parent().attr('data-id')).split('-');
		productId = (pId.length > 1) ? (pId[1]) : '';
	} else {
		productId = _this.parents('[id^=pdpId],[id^=quickViewId]').attr('id');
		productId = (typeof productId !== "undefined") ? productId.split('-')[1] : ($('#hiddenProductId').val() || $('#hiddenProductIdQuickView').val());
	}
	////productDetails= skn | UPC | UID| | mRmsColorCode | mRmsSizeCode | mSknOriginId | qty ~
	productDetails = generateAjaxProductDetailsByProductId(productId, isWishlist);
	var cname = (isWishlist) ? "addToWishListCookie " : "addToRegistryCookie";
	var pageName = $('.pageName').val();
	/* if( pageName == 'pdpPage'){
	 setRegistryWishlistCookie(cname,productDetails, _this.attr('data-href')); 
	 }else{
	 setRegistryWishlistCookie(cname,productDetails, _this.attr('href'));
	 }*/

	//ajaxRegistryCall(productDetails, isWishlist, _this.attr('href'));
	var redirectionUrl = _this.attr('data-href')
	//var intermediateRedirection = contextPath+"jstemplate/registryCookieIntermediate.jsp?"+redirectionUrl;
	setRegistryAkamaiCookie(productDetails, pageName, isWishlist, redirectionUrl);
}

function setRegistryAkamaiCookie(productDetails, pageName, isWishlist, redirectionUrl) {
	var timeout = $(document).find('#rgs_timeout').val() ? $(document).find('#rgs_timeout').val() : 5000;
	$.when(
		invokeAkamaiURL(),
		$.ajax({
			type: 'POST',
			url: contextPath + 'jstemplate/addToRegistryCookie.jsp',
			async: false,
			dataType: 'html',
			data: { cvalue: productDetails, pagename: pageName, iswishlist: isWishlist },
			success: function (responseData) {
				console.log("responseData::" + responseData);
			}
		})
	).then(function (data, textStatus, jqXHR) {
		invokeAkamaiURL();
		setTimeout(function () {
			window.location.assign(redirectionUrl);
		}, timeout)

	});
}

//Setting cookie for the registry and wishlist
function setRegistryWishlistCookie(cname, cvalue, hrefValue) {
	var currentDomain = window.location.host;
	var redirectionDomain = hrefValue.split("//");
	redirectionDomain = (redirectionDomain.length > 1) ? (redirectionDomain[1].split("/")[0]) : (redirectionDomain[0].split("/")[0]);
	var domainName = (redirectionDomain.split(".").length > 2) ? "." + redirectionDomain.split(".")[1] + "." + redirectionDomain.split(".")[2] : "." + redirectionDomain;
	currentDomain = (currentDomain.split(".").length > 2) ? "." + currentDomain.split(".")[1] + "." + currentDomain.split(".")[2] : "." + currentDomain;
	if (currentDomain == domainName) {
		document.cookie = cname + "=" + cvalue + "; path=/;domain=" + domainName;

	} else {
		document.cookie = cname + "=" + cvalue + "; path=/;domain=" + currentDomain;
		var iframeWindowBRU = document.getElementById('crossDomainCookiePersisterIframeBRU');
		if ($(iframeWindowBRU).length) {
			iframeWindowBRU.contentWindow.postMessage({ key: cname, data: cvalue, domainName: domainName }, iframeWindowBRU.src);
		}
	}
	//$(".cp-item-qty").val(0);
	window.location.assign(hrefValue);
}

/* function ajaxRegistryCall(productDetails, isWishlist, href){
	  if(typeof productDetails === "undefined" || productDetails == ''){
		  return false;
	  }
	  $.ajax({
				type: 'POST',
				url: contextPath+'jstemplate/registryWishlistCookie.jsp',
				data:{isWishlist : isWishlist, productDetails : productDetails },
				async:false,
				success:function(){
					window.location.assign(href);
				},
				error:function(){
					window.location.assign(href);
				}
			});
  }*/



$(document).on("hidden.bs.modal", ".modal#add-to-registry,.modal#items-added-success-wishlist,.modal#error-registry-dailog", function () {
	if ($(".collection-template").length > 0) {
		clearInput();
	}
});

function displayTooltipMessageOnAddtoCartButton(swatchSelected, quickview) {
	var message = "";
	var colorSize = (typeof selectcolorsize != 'undefined' && selectcolorsize != '') ? selectcolorsize : 'Please select color & size.';
	var pleaseSelectColor = (typeof selectcolor != 'undefined' && selectcolor != '') ? selectcolor : 'Please select color.';
	var pleaseSelectSize = (typeof selectsize != 'undefined' && selectsize != '') ? selectsize : 'Please select size.';
	/*if(colorSize ==""){
		colorSize == "Please select color & size.";		 
	}
	if(pleaseSelectColor == ""){
		pleaseSelectColor == "Please select color."		 
	}
	if(pleaseSelectSize == ""){
		pleaseSelectSize == "Please select size."		 
	}*/
	var $addToCartDivSpace = $("#addToCartDivSpace");
	$addToCartDivSpace.find(".add-to-cart.fade-add-to-cart").removeAttr("disabled");
	$addToCartDivSpace.find(".add-to-cart.fade-add-to-cart").popover('destroy');
	$($addToCartDivSpace.find(".add-to-cart.fade-add-to-cart")).click(function (e) {
		e.preventDefault();
		return false;
	});
	var fromQuickview = quickview || ($("#quickviewModal").hasClass("in").length > 0)
	if (!swatchSelected && !fromQuickview) {
		if ($(".color_text .multipleColors").length > 0) {
			message = pleaseSelectColor;
			var multipleColor = true;
		}
		if ($("#size_text .multipleSizes").length > 0) {
			message = pleaseSelectSize;
			if (multipleColor) {
				message = colorSize;
			}
		}
	}
	else if (swatchSelected && !fromQuickview) {
		if ($(".colors .color-block img").length && $(".colors .color-block.color-selected").length <= 0) {
			message = pleaseSelectColor;
		}
		if ($(".sizes .size-option").length && $(".sizes .size-option.size-selected").length <= 0) {
			message = pleaseSelectSize;
		}
	}
	else if (!swatchSelected && fromQuickview) {
		$addToCartDivSpace = $("#quickviewModal #addToCartDivSpace");
		$addToCartDivSpace.popover('destroy');
		if ($("#quickviewModal .colors .color-block img").length && $("#quickviewModal .colors .color-block.color-selected").length <= 0) {
			message = pleaseSelectColor;
			var quickviewMultipleColor = true;
		}
		if ($("#quickviewModal .sizes .size-option").length && $("#quickviewModal .sizes .size-option.size-selected").length <= 0) {
			message = pleaseSelectSize;
			if (quickviewMultipleColor) {
				message = colorSize;
			}
		}
	}
	if ($addToCartDivSpace.find(".add-to-cart.fade-add-to-cart").length > 0) {
		$addToCartDivSpace.css('display', 'inline-block');
		$addToCartDivSpace.find(".add-to-cart.fade-add-to-cart").removeAttr("disabled");
		showAddtoCartTooltip($addToCartDivSpace.find(".add-to-cart.fade-add-to-cart"), message)
	}
}

function showAddtoCartTooltip($addToCartDivSpace, message) {
	$addToCartDivSpace.click(function (event) {
		event.preventDefault();
	});
	setTimeout(function () {
		$addToCartDivSpace.popover({
			content: message,
			placement: 'bottom',
			trigger: 'hover'
		});
	}, 220);
}

/*new functions for new requirement of spark PDP changes*/
$(document).ready(function () {
	$(document).on("click", '.registryButtonContainer button,#addToCartDivSpace button', function (e) {
		var redirectURL = $(this).attr("data-href");
		if (redirectURL) {
			window.location.href = redirectURL;
		}
	});
	$(".promotionDiscriptionContainer").hide();
	$(document).on("click", '.promotionHeadingContainer .glyphicon-plus', function (e) {
		$(this).toggleClass("glyphicon-minus");
		$(".promotionDiscriptionContainer").slideToggle();
	});
	$(document).on("click", '.offer-container .plus-sign', function (e) {
		$(this).toggleClass('minus-sign');
		//$('.promotionCount').toggleClass('show-count');
		$('.special-offer-details').slideToggle();
	});
});

var zoomInPause = false,
	zoomOutPause = false;

$(document).on("mouseenter", ".item.active .zoomContainer,.collection-hero-image .zoomContainer", function () {
	var $myContainer = $(this),
		$myImage = $(this).find(".zoomImage"),
		$largeImage = $(this).find(".largeImage"),
		myPosition = $myContainer.offset(),
		rightOffset = myPosition.left,
		bottomOffset = myPosition.top;
	$largeImage.css({ opacity: "1" }).fadeIn();
	$myContainer.unbind("mousemove");
	if (!zoomInPause) {
		zoomInPause = true;
		$myImage.fadeOut();
		zoomInPause = false;
	}
	$myContainer.mousemove(function (event) {
		event = event || window.event;
		event = jQuery.event.fix(event);
		if ($(this).parents(".item.active").length > 0 && $(this).find(".zoomImage").width() > 200) {
			rightString = (event.pageX - rightOffset - 781) / 4.333 + "px";
			bottomString = event.pageY - bottomOffset - 480 + "px";
			$largeImage.css({ "bottom": bottomString, "right": rightString });
		}
		else if ($(this).parents(".item.active").length > 0 && $(this).find(".zoomImage").width() <= 200) {
			rightString = (event.pageX - rightOffset + 400) / 4.333 + "px";
			bottomString = event.pageY - bottomOffset - 480 + "px";
			$largeImage.css({ "bottom": bottomString, "right": rightString });
		}
		else {
			rightString = (event.pageX - rightOffset - 781) / 4.333 + "px";
			bottomString = event.pageY - bottomOffset - 500 + "px";
			$largeImage.css({ "bottom": bottomString, "right": rightString });
		}
	})
});

$(document).on("mouseleave", ".item.active .zoomContainer,.collection-hero-image .zoomContainer", function () {
	var $myContainer = $(this),
		$myImage = $(this).find(".zoomImage"),
		$largeImage = $(this).find(".largeImage");
	if (!zoomOutPause) {
		zoomOutPause = true;
		$myImage.fadeIn().css({ opacity: "1" });
		$largeImage.fadeOut();
		$largeImage.css({ opacity: "0" });
		zoomOutPause = false;
	}
});

/*$(document).on("mouseenter",".promo-with-img-container",function(){
	var $myContainer = $(".promo-with-img-container"),
	$myImage = $(".promo-with-img-container #PDPCarousel .item.active img"),
	$largeImage = $("#hero-image1"),
	myPosition = $myContainer.offset(),
	rightOffset = myPosition.left,
	bottomOffset =  myPosition.top;
	$largeImage.fadeIn().css({opacity :"1",display:'block'});
	$myContainer.unbind("mousemove");
	if(!zoomInPause) {
		zoomInPause = true;
		$myImage.fadeOut();				
		zoomInPause = false;
	}
	//$largeImage.delay(250).animate({opacity: 1},750);
	$myContainer.mousemove(function(){
		rightString = (event.pageX - rightOffset - 1200) / 4.333 + "px"; //780) / 4.333 + "px";
		bottomString = event.pageY - bottomOffset - 400 + "px"; // 380 + "px";
		$largeImage.css({"bottom": bottomString, "right": rightString});
	})
});

$(document).on("mouseleave",".promo-with-img-container",function(){
	var $myContainer = $(".promo-with-img-container"),
	$myImage = $(".promo-with-img-container #PDPCarousel .item.active img"),
	$largeImage = $("#hero-image1");
	if(!zoomOutPause) {
		zoomOutPause = true;
		$myImage.fadeIn().css({opacity :"1",display:'block'});
		$largeImage.fadeOut();
		$largeImage.css({opacity :"0", display:'none'});
		zoomOutPause = false;
	}
});*/

/*var PDPZoom = function(id){
		var $myContainer = $(".promo-with-img-container"),
			$myImage = $(".promo-with-img-container #PDPCarousel [data-slide-number="+id+"] img"),
		$largeImage = $("#hero-image1"),
		myPosition = $myContainer.offset(),
		rightOffset = myPosition.left,
		bottomOffset =  myPosition.top,
		rightString,
		bottomString,
		zoomInPause = false,
		zoomOutPause = false,zoomInPause
		videoTab = false;
	$(".thumb-container .block-add").first().addClass("selected");
	$myContainer.hover(zoomIn, zoomOut);

	function zoomIn() {
			//$myContainer.unbind("mousemove").mousemove(panImage);
			//if(!zoomInPause) {
			zoomInPause = true;
			$myImage.fadeOut('750', function() {					
				zoomInPause = false;
			});
				//$largeImage.delay(250).animate({opacity: 1},750);	
				$largeImage.animate({opacity: 1},750);
			//}
	}
	function zoomOut() {
			//if(!zoomOutPause) {
			zoomOutPause = true;
				$largeImage.fadeOut('150', function() {
				$largeImage.css({opacity :"0", display: ""});
				zoomOutPause = false;
			});
				$myImage.fadeIn(0);
			//};
	};
	function panImage(event) {
		rightString = (event.pageX - rightOffset - 780) / 4.333 + "px";
		bottomString = event.pageY - bottomOffset - 380 + "px";
		$largeImage.css({"bottom": bottomString, "right": rightString});
	};
	 
};
*/

$(document).ready(function () {
	/* adding selected class to the dropdown menu links when user click on sort by drop down to heighlight the color  */
	function fassetsSortby(e) {
		$('.product-content .fixed-narrow-menu .btn-group button, .product-content .fixed-narrow-menu #sort-by-arrow').on('click', function (e) {
			var buttonHtml = $('.product-content .fixed-narrow-menu .btn-group button .sort-by-title').html();
			var listHtml = "";
			var elements = $('.product-content .fixed-narrow-menu .btn-group .dropdown-menu li a span');
			$(elements).each(function (index, currentElm) {
				if ($(currentElm).html() === buttonHtml) {
					$(currentElm).parents("li").addClass("selected");
				}
			});
		});
	}
	fassetsSortby();
	/* fileters alignment fix TUW-73786 */
	function filtersAlignment() {
		var filterCount = $('.filter-results .sub-cat-category-output').find('.sub-cat-filter-list').length;
		var targetElements = $('.sub-cat-more').nextAll();
		if (targetElements.length > 0) {
			$(targetElements).appendTo($('.sub-cat-category-output'));
		}
	} filtersAlignment();// calling page on load
	$('body').on('click', '.sub-cat-container .filter-lbl-container', function (e) {
		filtersAlignment();
		setTimeout(function () {
			facetsAlignment();
		}, 2000);
	});

	/* end of filter alignment fix */
	/* End  Fix TUW-73613 - adding selected class to the dropdown menu links when user click on sort by drop down to heighlight the color */
	function subCatCarouselHeight() {
		var carouselBoxesCount = $("#slider-image-block-container").find(".hover-hover").length / 2;
		if ((carouselBoxesCount < 7) && (carouselBoxesCount != 0)) {
			$(".sub-category-template").find(".category-name").css({ "margin-top": "-25px" });
		}
		else {
			$(".sub-category-template").find(".category-name").css({ "margin-top": "3px" });
		}
	}
	subCatCarouselHeight();
	/* facets alignment according to the search key on stickey header - Fix for TUW-73969 _R-F-19 */
	function facetsAlignment() {
		if ($('#guidedNavigation .sub-cat-category-output').find('.sub-cat-filter-list').length > 0 && $('.fixed-narrow-menu.sticky').length > 0) {
			var heightStickyHeader = $('#guidedNavigation').height() + 20;
			$('#guidedNavigation').find('.sub-cat-container').css({ 'top': heightStickyHeader });
		}
		else if ($('#guidedNavigation .sub-cat-category-output').find('.sub-cat-filter-list').length > 0 && $('.fixed-narrow-menu').length > 0) {
			var heightStickyHeader = $('#guidedNavigation').height() + 20;
			$('#guidedNavigation').find('.sub-cat-container').css({ 'top': heightStickyHeader });
		}
		else {
			$('#guidedNavigation').find('.sub-cat-container').css({ 'top': '89px' });
		}
	} facetsAlignment();
	$(window).scroll(function () {
		facetsAlignment();
	});
	$('body').on('click', '.sub-cat-category-output .dismiss-x', function (e) {
		setTimeout(function () {
			facetsAlignment();
		}, 2000);
	});
	/* End of facets alignment according to the search key on stickey header - Fix for TUW-73969 _R-F-19 */

	// ISPU overlay show timings
	$('body').on('click', '.pdp-findinstore-popup .findinstore-timings', function (e) {
		var showTimingsData = "";
		$(e.target).parents(".store-locator-results-single").find("#show-timings-container").toggle(600, function () {
			if ($(e.target).parents(".store-locator-results-single").find("#show-timings-container").is(":visible")) {
				$(e.target).html("hide details");
				var storeID = parseInt($(e.target).parents(".store-locator-results-single").find(".storeIdForPDP").val());
				if (storeID == "" || typeof storeID === "undefined" || storeID === null) {
					showTimingsData = "no data available";
					$(e.target).parents(".store-locator-results-single").find("#show-timings-container").html(showTimingsData);
				}
				else if ($(e.target).parents(".store-locator-results-single").find("#show-timings-container .weekday-week-timings").length > 0) {
					return;
				}
				else {
					ispuOverlayShowTimings(storeID);
				}

			}
			else {
				$(e.target).html("details");
			}
		});

		var ispuOverlayShowTimings = function (storeID) {
			var storeID = storeID;
			$.ajax({
				url: "/storelocator/storeInfo.jsp?storeId=" + storeID,
				type: "GET",
				success: function (data) {
					var weekTime = "";
					var jsonData = JSON.parse($(data).find(".storeHoursJson").text());
					var hoursHead = "<div class='hours-heading'>hours</div>";
					hoursHead += "<div class='weekday-week-timings'>";
					$.each(jsonData, function () {
						$.each(this, function (day, time) {
							weekTime += "<div class='timingsWrapper'><span class='weekday'>" + day + ":</span><span class='week-timings'> " + time + "</span></div>";
						});
					});
					showTimingsData = hoursHead + weekTime + "</div>";
					$(e.target).parents(".store-locator-results-single").find("#show-timings-container").html(showTimingsData);
				},
				error: function (jqXHR, textStatus, errorThrown) {
					console.log(jqXHR);
				}
			});
		}

	});
	// add to cart - error message container moving above the stepper
	$('body').on('click', '.product-information .cart-info .stepper a', function () {
		setTimeout(function () {
			var elm = $(".product-information .cart-info");
			$(elm).each(function (index, currentElm) {
				if ($(currentElm).find("span.cart-error-message").length > 0) {
					var target = $(currentElm).find("span.cart-error-message");
					$(target).insertBefore(target.prev());
					$(target).css({ 'display': 'block' });
				}
			})
		}, 1500);
	})
	// tell a friend overlay form validations
	var tellAFriendEmailValidation = function () {
		var emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		$(document).on('focusout', "input#friendsemail", function () {
			var emailAddress = $("#friendsemail").val();
			var valid = emailRegex.test(emailAddress);
			emailValidation(valid, this);
			//loopTheForm();
		});
		$(document).on('focusout', 'input#youremail', function () {
			var emailAddress = $("#youremail").val();
			var valid = emailRegex.test(emailAddress);
			emailValidation(valid, this);
		});
		$(document).on('focusout', 'input#youremail', function () {
			var emailAddress = $("#youremail").val();
			var valid = emailRegex.test(emailAddress);
			emailValidation(valid, this);
		});
		$(document).on('keypress', 'input#friendsName', function () {
			charLengthValidation(30, this);
		});
		$(document).on('focusout', 'input#friendsName', function () { // user should not able to paste more then 30 char
			charLengthValidation(30, this);
		});
		$(document).on('keypress', 'input#yourname', function () {
			charLengthValidation(30, this);
		});
		$(document).on('focusout', 'input#yourname', function () {
			charLengthValidation(30, this);
		});
		$(document).on('keypress', 'textarea#personalMessage', function () {
			charLengthValidation(200, this);
		});
		function charLengthValidation(limit, elem) {
			if ($(elem).val().length >= limit) {
				$(elem).val($(elem).val().slice(0, limit - 1));
				return false;
			}
		}
		function emailValidation(valid, elem) {
			if (!valid) {
				if ($(elem).hasClass('no-error')) {
					$(elem).removeClass('no-error');
					$(elem).addClass('error');
				}
				return false;
			} else
				return true;
		}

		$(document).on('focusout', 'input', function () {
			loopTheForm();
		});
	}
	tellAFriendEmailValidation();
});

function loopTheForm() {
	var count = 0;
	$('#emailToFriend input:enabled').each(function () {
		if ($(this).hasClass("no-error")) {
			count = count + 1;
		}
	});
	if (count == 4 && ($("#g-recaptcha-response").val() != "")) {
		$("#sendFormData").removeClass("disable");
	}
	else {
		$("#sendFormData").addClass("disable");
	}

}

/**
 * PTP-12 (https://toysrus.atlassian.net/browse/PTP-12)
 * 
 * Format PDP additional information
 */
function formatProductAdditionalInfo() {
	//format weight information to show only 1 decimal place
	var $weightsArr = $('.pdp-additional-info-weight');

	$weightsArr.each(function (i, ele) {
		var weight = +ele.innerText;
		if (!isNaN(weight)) {
			ele.innerText = weight.toFixed(1);
		}
	});
}
/* adding the border to the invado video thumb once it is loaded in the page part of GEOFUS-1567 fix*/

$(window).load(function() {
	setTimeout(function () {
		var invodoExisted = $(".pdp-product-details-block .invodoThumbContainer #invodoModalButton").find("img");
	 		if($(invodoExisted).length > 0){
		  $("#stylized_thumbnail_container .invodoThumbContainer").removeClass("noborder");
	 	}
	}, 2000) 	
});

/* spoil alert tooltip on ATC */
$(document).on('mouseover', '.surprise-wrapper', function (e) {
	$(this).find('.popoverCart').show().css({'position':'fixed'});
	var positionTop = $(this).offset().top;
	var positionLeft = $(this).offset().left - $(this).offset().left + 150 ;	
	var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // fix If Internet Explorer
    {
    	positionTop = $(this).offset().top + 30;
    	positionLeft = $(this).offset().left - 50;	
    	$(this).find('.popoverCart').show().css({'left':positionLeft, 'top':positionTop});
    }
    else  // If another browser, return 0
    {
    	$(this).find('.popoverCart').show().css({'left':positionLeft, 'top':positionTop});    	
    }
});
$(window).bind('mousewheel DOMMouseScroll', function(event){
	$(".cart-content").find('.popoverCart').hide().css({'position':'relative'});
});
$(document).on('mouseout', '.surprise-wrapper', function (e) {
	$(this).find('.popoverCart').hide().css({'position':'relative'});
})
/* End of spoil alert tooltip on ATC */