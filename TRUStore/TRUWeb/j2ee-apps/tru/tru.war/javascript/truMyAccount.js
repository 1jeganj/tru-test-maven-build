var cardinalApi = '';
var cardinalSetup = false;
var cardinalErrorMessage = '';
var isCardinalEnabled = false;
var contextPath = '';
var isCardNumInLimit = true;
var cardTypeForCvvValidation = '';
var enableVerboseDebug = false;
var enableIntegrationLog = false;
var nonceRetryAttempts = 0;
var getTokenRetryAttempts = 0;
var maxNonceRetryAttempts = 0
var maxGetTokenRetryAttempts = 0
var nonceSetup = false;
$(document).ready(function () {
	$('#zip').mask('AAAAA-AAAA');
	$('#shippingZip').mask('AAAAA-AAAA');
	$("#myAccountAddAddressModal .addressDoctor").find("img").click(function () {
		$('p.address-doctor-address a').click();
	});
});
function makeAjax(param) {

	$('.errorDisplay').remove();
	$(document).find('.success-adjustment').removeClass('success-adjustment');
	$(document).find('.successMessage').remove();
	remove_highlight_class();

	var reqestData = (typeof (param.data.formSerialize) != 'undefined') ? (param.data.formSerialize + '&formID=' + param.data.formID) : ({ formID: param.data.formID });


	if (param.data.formID == 'defaultAddress' || param.data.formID == 'onEditAddressInOverlay' || param.data.formID == 'removeShippingAddress') {

		var txt = param.data.formSerialize;
		txt = (txt).substring((txt).indexOf('=') + 1, txt.length);

		if (param.data.formID == 'defaultAddress') {
			reqestData = { defaultAddNickName: txt, formID: param.data.formID };
		}

		if (param.data.formID == 'onEditAddressInOverlay') {
			reqestData = { editNickName: txt, formID: param.data.formID };
		}

		if (param.data.formID == 'removeShippingAddress') {
			reqestData = { name: txt, formID: param.data.formID };
		}
	}

	$.ajax({
		type: "POST",
		dataType: "html",
		async: false,
		cache: false,
		data: reqestData,
		url: param.url,
		success: function (data) {
			switch (param.data.formID) {
				case 'onLoadAddressInOverlay':
					$("#addressAjaxFragment").html(data);
					reInitScrollBar('addressOverlay', 340);
					$('#myAccountAddressForm #shippingZip').mask('AAAAA-AAAA');
					bruCss();
					break;

				case 'addressAjaxRequest':
					if (data.indexOf('Following are the form errors:') > -1) {
						setTimeout(function () {
							if ($('#myAccountAddAddressModal .addressDoctor').length) {
								addBackendErrorsToForm(data, $("#myAccountAddAddressModal .addressDoctor"));
							} else {
								addBackendErrorsToForm(data, $("#myAccountAddressForm"));
							}
						}, 150);
					} else {
						$("#addressAjaxFragment").html(data);
						$('#my-info-default-address').html(loadMyaccountAddressOrCard('/myaccount/myAccountAddress.jsp'));
						$('#myAccountAddressForm .tse-scroll-content').scrollTop(0);
						reInitScrollBar('addressOverlay', 320);
						$('#myAccountAddressForm .tse-content').prepend('<div class="successMessage">' + myacc_addresUpdationMessage + '</div>');
						bruCss();
					}
					break;

				case 'onEditAddressInOverlay':
					$("#addressAjaxFragment").html(data);
					reInitScrollBar('addressOverlay', 340);

					setTimeout(function () {
						var isCountryAvailable = $('.my-account-template #myAccountAddressForm #editAddresscountry').length;
						var selectCountry = $('#editAddresscountry').val();
						if ((isCountryAvailable == 1 && selectCountry == 'US') || isCountryAvailable == 0) {
							$('#myAccountAddressForm #shippingZip').mask('AAAAA-AAAA');
						} else {
							$('#myAccountAddressForm #shippingZip').mask('AAAAAAAAAA');
						}
					}, 600);
					bruCss();
					break;

				case 'removeShippingAddress':
					$("#addressAjaxFragment").html(data);
					$('#my-info-default-address').html(loadMyaccountAddressOrCard('/myaccount/myAccountAddress.jsp'));
					$('#myAccountAddressForm .tse-content').prepend('<div class="successMessage">' + myacc_addresDeletionMessage + '</div>');
					reInitScrollBar('addressOverlay', 340);
					$('#myAccountAddressForm .tse-scroll-content').scrollTop(0);
					$('#myAccountDeleteCancelModal').modal('hide');
					bruCss();
					break;

				case 'defaultAddress':
					$("#addressAjaxFragment").html(data);
					$('#my-info-default-address').html(loadMyaccountAddressOrCard('/myaccount/myAccountAddress.jsp'));
					reInitScrollBar('addressOverlay', 340);
					bruCss();
					break;

				case 'onLoadCreditCardOverlay':

					$("#creditCardAjaxFragment").html(data);

					showHideBillingAddress();
					setTimeout(function () { reInitScrollBar('onEditCreditCardOverlay', 300); }, 1000);
					bruCss();
					break;

				case 'onAddCreditCard':
					if (data.indexOf('Following are the form errors:') > -1) {
						setTimeout(function () {
							if ($("#myAccountAddCreditCardModal").hasClass("in")) {
								editUserEnteredAddressForCreditCard();
							}
							addBackendErrorsToForm(data, $('#addOrEditCreditCard'));
							$('#addOrEditCreditCard .tse-scroll-content').scrollTop(0);
						}, 150);
					} else {
						$("#creditCardAjaxFragment").html(data);
						$('#addOrEditCreditCard .tse-content .successMessage').remove();
						$('#myAccountCardsSecId').html(loadMyaccountAddressOrCard('/myaccount/myAccountCard.jsp'));
						$('#my-info-default-address').html(loadMyaccountAddressOrCard('/myaccount/myAccountAddress.jsp'));
						$('#creditCardAjaxFragment .billing-address').hide();
						setTimeout(function () { reInitScrollBar('onEditCreditCardOverlay', 300); $('#addOrEditCreditCard .tse-content').prepend('<div class="successMessage">' + myacc_cardSavedMessage + '</div>'); }, 500);
						bruCss();
					}
					break;

				case 'onEditCreditCardOverlay':
					$("#creditCardAjaxFragment").html(data);

					//setTimeout(function () {
					reInitScrollBar('onEditCreditCardOverlay', 300);
					var selectCountry = $('#country').val();
					if (selectCountry == 'US') {
						$('#zip').mask('AAAAA-AAAA');
					} else {
						$('#zip').mask('AAAAAAAAAA');
						var zipVlaue = $('#zip').val();
						if (zipVlaue != '' && $('#zip').hasClass('error')) {
							$('#zip').removeClass('error').next('#zip-Validation-error').remove();
						}
					}
					//},1000);

					bruCss();
					break;

				case 'removeCard':
					$("#creditCardAjaxFragment").html(data);
					$('#addOrEditCreditCard .tse-scrollable .tse-content').prepend('<div class="successMessage">' + myacc_cardDeletionMessage + '</div>');
					$('#myAccountCardsSecId').html(loadMyaccountAddressOrCard('/myaccount/myAccountCard.jsp'));
					$('#myAccountCardDeleteModal').modal('hide');
					showHideBillingAddress();
					setTimeout(function () { reInitScrollBar('onEditCreditCardOverlay', 300); }, 1000);
					bruCss();
					break;

				case 'defaultCreditCard':
					if (param.data.id == 'overlay') {
						$("#creditCardAjaxFragment").html(data);
						$('#myAccountCardsSecId').html(loadMyaccountAddressOrCard('/myaccount/myAccountCard.jsp'));
						bruCss();
					} else if (param.data.id == 'landingPage') {
						$(".saved-credit-card.cardDetailsRow").replaceWith(data);
					}
					showHideBillingAddress();
					setTimeout(function () { reInitScrollBar('onEditCreditCardOverlay', 300); }, 1000);
					break;

				case 'removeCardLanding':
					$(".my-account-your-info .saved-credit-card").replaceWith(data);
					$('.my-account-template .my-account-left-col hr').first().after("<div class='successMessage'>" + myacc_cardRemovedSuccessfully + "</div>");
					$('.my-account-template .my-account-your-info').addClass('success-adjustment');
					$('#myAccountCardDeleteModalLanding').modal('hide');
					bruCss();
					break;

				case 'name':
					if (data.indexOf('Following are the form errors:') > -1) {
						var msg = data.split('Following are the form errors:')[1];
						$('#namChangeModel .update-name-header').after('<span class="errorDisplay">' + msg + '</span>');
					} else {
						$(".my-account-your-info-cont").replaceWith(data);
						$(".my-account-welcome-back-cont").load("myAccountProfile.jsp");
						//$(".head_welcome_cont").load("/tru/header/headWelcomeContent.jsp"); 
						setFirstNameCookie();
						$('.my-account-template .my-account-left-col hr').first().after("<div class='successMessage'>" + myacc_nameUpdationMessage + "</div>");
						$('.my-account-template .my-account-your-info').addClass('success-adjustment');
						$('#namChangeModel').modal('hide');
					}
					break;

				case 'email':
					if (data.indexOf('Following are the form errors:') > -1) {
						var msg = data.split('Following are the form errors:')[1];
						$('#emailChangeModel .update-email-header').after('<span class="errorDisplay">' + msg + '</span>');
						addError('emailAddressFormValidation');
					} else {
						$(".my-account-your-info-cont").replaceWith(data);
						$(".my-account-welcome-back-cont").load("myAccountProfile.jsp");
						//$(".head_welcome_cont").load("/tru/header/headWelcomeContent.jsp"); 
						setFirstNameCookie();
						$('.my-account-template .my-account-left-col hr').first().after("<div class='successMessage'>" + myacc_emailAddressupdate + "</div>");
						$('.my-account-template .my-account-your-info').addClass('success-adjustment');
						$('#emailChangeModel').modal('hide');
					}
					break;

				case 'password':
					$('.errorDisplay').remove();
					if (data.indexOf('Following are the form errors:') > -1) {
						addBackendErrorsToForm(data, $("#passwordFormValidation"));
						/*var msg = data.split('Following are the form errors:')[1];
						$('#myAccountUpdatePasswordModal .update-password-header').after('<span class="errorDisplay">' + msg + '</span>');
						addError('passwordFormValidation');*/
					} else {
						$(".my-account-your-info-cont").replaceWith(data);
						$('.my-account-template .my-account-left-col hr').first().after("<div class='successMessage'>" + myacc_passwordUpdate + "</div>");
						$('.my-account-template .my-account-your-info').addClass('success-adjustment');
						$('#myAccountUpdatePasswordModal').modal('hide');
					}
					break;

				case 'membershipForm':
					if (data.indexOf('Following are the form errors:') > -1) {
						/*$("#myAccountRewardsSecId .rewards-zone").prepend('<span class="errorDisplay">' + data.split('Following are the form errors:')[1] + '</span>');
						addError('membershipForm');*/
						setTimeout(function () {
							addBackendErrorsToForm(data, $("#membershipForm"));
						}, 150);
					} else {
						$("#myAccountRewardsSecId .rewards-zone").replaceWith(data);
						$('.my-account-template .my-account-left-col hr').first().after('<div class="successMessage">' + myacc_rewardsMessageUpdate + '</div>');
						$('.my-account-template .my-account-your-info').addClass('success-adjustment');
						$('#my-account-popover-struct').load('/header/headerMyAccountPopover.jsp');
						bruCss();
					}
					break;

				case 'confirmRemoveMembershipForm':
					$("#myAccountRewardsSecId").html(data);
					$('.my-account-template .my-account-left-col hr').first().after("<div class='successMessage'>" + myacc_sucessRemoval + "</div>");
					$('.my-account-template .my-account-your-info').addClass('success-adjustment');
					$('#my-account-popover-struct').load('/header/headerMyAccountPopover.jsp');
					$('#myAccountRemoveRewardCardNumModal').modal('hide');
					bruCss();
					break;

				case 'editUserEnteredAddressForCreditCard':
					$("#creditCardAjaxFragment").html(data);
					//var cvv = $("#creditCardAjaxFragment .creditCardCVV_tmp").val();
					//$("#creditCardAjaxFragment .creditCardCVV_tmp").val(cvv.replace(/[0-9]/g,'*'));
					setTimeout(function () { reInitScrollBar('onEditCreditCardOverlay', 300); }, 1000);
					bruCss();
					break;

				case 'editUserEnteredAddress':
					$("#addressAjaxFragment").html(data);
					reInitScrollBar('addressOverlay', 340);
					bruCss();
					break;

				case 'onChangeAddressInCCOverlay':
					$('#billing-address-container').replaceWith(data);
					break;

				default:
					console.log('Ajax: default case.');
					break;
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				window.event.cancelBubble = true;
				var redirectionUrl = xhr.getResponseHeader('Referer');
				$(document).find("[data-toggle=modal]").each(function () {
					$(this).attr("data-target", "");
				});

				//setTimeout(function(){
				if (redirectionUrl != '' && redirectionUrl != null && redirectionUrl != 'undefined' && !($('#VisitorFirstNamecookieDIV').html() == 'my account')) {
					if (redirectionUrl.indexOf('?') > -1) {
						redirectionUrl = redirectionUrl + '&sessionExpired=true';
					} else {
						redirectionUrl = redirectionUrl + '?sessionExpired=true';
					}
					if (redirectionUrl.indexOf('orderHistory.jsp') > -1) {
						redirectionUrl = redirectionUrl.replace('orderHistory.jsp', 'myAccount.jsp');
					}
					location.href = redirectionUrl;
				} else {
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'myaccount/myAccount.jsp?sessionExpired=true';
				}
				//},300);
			}
			return false;
		}
	});
}

function reInitScrollBar(formId, height) {
	var scrollable = '';
	if (formId == 'onEditCreditCardOverlay') {
		scrollable = $('.my-account-add-credit-card-overlay').find('.tse-scrollable');
	} else {
		scrollable = $('.my-account-add-address-overlay').find('.tse-scrollable');
	}
	var recalculate = function () {
		setTimeout(function () {
			scrollable.TrackpadScrollEmulator('recalculate');
			scrollable.find('.tse-scroll-content').height(height);
			if (formId == 'onEditCreditCardOverlay') {
				$('.my-account-add-credit-card-overlay').height(496);
				$('.my-account-add-credit-card-overlay').find('.set-height').height(456);
			}
		}, 500);
	};

	scrollable.TrackpadScrollEmulator({
		wrapContent: false,
		autoHide: false
	});

	recalculate();

}

function showHideBillingAddress() {
	if ($('#creditCardAjaxFragment #selectedCardBillingAddress').length > 0) {
		$('#creditCardAjaxFragment .billing-address').hide();
		/*$('#creditCardAjaxFragment .billing-address').html("");*/
	} else {
		$('#creditCardAjaxFragment .billing-address').show();
	}
}

function loadMyaccountAddressOrCard(reqUrl) {
	var resData = '';
	$.ajax({
		type: "POST",
		cache: false,
		async: false,
		dataType: "html",
		url: reqUrl,
		success: function (data) {
			resData = data;
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				window.event.cancelBubble = true;
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'myaccount/myAccount.jsp?sessionExpired=true';
			}
		}
	});
	return resData;
}

$(document).on('click', '#forgotPasswordLandingLink, #forgotPasswordPopupLink', function () {
	$('#forgotPasswordModel .modal-content.sharp-border').html(loadMyaccountAddressOrCard('/myaccount/forgotPasswordResponse.jsp'));
	universalFormValidationHandler();
	$('.errorDisplay').remove();
	remove_highlight_class();
	bruCss();
});

$("body").on('click', '.changeNameModal', function () {
	$('#namChangeModel .namChangeModel-overlay').html(loadMyaccountAddressOrCard('/myaccount/editPersonalInfo.jsp'));
	universalFormValidationHandler();
	$('.errorDisplay').remove();
	remove_highlight_class();
	bruCss();
});

$("body").on('click', '.changeEmailModal', function () {
	$('#emailChangeModel .emailChangeModel-overlay').html(loadMyaccountAddressOrCard('/myaccount/editEmailAddress.jsp'));
	universalFormValidationHandler();
	$('.errorDisplay').remove();
	remove_highlight_class();
	bruCss();
});

$("body").on('click', '.changePasswordModal', function () {
	$('#myAccountUpdatePasswordModal .my-account-update-password-overlay').html(loadMyaccountAddressOrCard('/myaccount/changePassword.jsp'));
	universalFormValidationHandler();
	passwordMeter();
	$('.errorDisplay').remove();
	$("input[type=password]").maskPassword();
	remove_highlight_class();
	bruCss();
});

function remove_highlight_class() {
	$(document).find('input').removeClass('error-highlight');
}

$(document).on('click', '.my-account-check-another', function () {
	$('.card-input input').val('');
	$('.card-input').show();
	$('.my-account-gift-card-balance-container').hide();

});

$(document).on('click', '#giftCardBalanceMyAccountBtn', function () {
	if ($("#myAccountCheckBalanceForm").find("#crdNo2").val().trim() == "" && $("#myAccountCheckBalanceForm").find("#pin2").val().trim() == "") {
		errorKeyJson = JSON.parse(localStorage.getItem('errorKeyJson'));
		var tru_error_gcNumberPin_empty = myacc_enterGiftCardAndPin;
		if (errorKeyJson.hasOwnProperty(tru_error_gcNumberPin_empty)) {
			tru_error_gcNumberPin_empty = errorKeyJson.tru_error_gcNumberPin_empty;
		}
		$("#myAccountCheckBalanceForm").find("#crdNo2").addClass("error");
		$("#myAccountCheckBalanceForm").find("#pin2").addClass("error");
		$("#myAccountCheckBalanceForm").find(".global-error-display").html(tru_error_gcNumberPin_empty);
		checkMaskPasswordError($("#myAccountCheckBalanceForm"));
		focusOnElement($("#myAccountCheckBalanceForm").find(".global-error-display"));
	} else {
		$("#myAccountCheckBalanceForm").find(".global-error-display").html("");
		if ($("#myAccountCheckBalanceForm").valid()) {
			checkGiftCardBalance('myAccountCheckBalanceForm');
		} else {
			checkMaskPasswordError($("#myAccountCheckBalanceForm"));
		}
	}


	return false;
});

function ajaxCallForMyAccountLandingPage(contextPath) {
	var queryString = window.location.search.substring(1);
	$.ajax({
		type: "POST",
		url: contextPath + "myaccount/myAccountLandingInclude.jsp",
		data: queryString,
		dataType: "html",
		async: false,
		processData: false,
		success: function (data, status, xhr) {
			successMyAccountPageCallback(data);
		},
		error: function (jqXHR, textStatus, errorThrown) {
			//errorMyAccountPageCallback(jqXHR, textStatus, errorThrown);
		}
	});
}

var initGiftCardPopover = function () {
	var giftcardTooltip = $("#giftcardTooltip");

	if (typeof giftcardTooltip.data("bs.popover") === "undefined") {

		var giftcardTooltipTemplate = '<div class="popover" role="tooltip"><div class="arrow"></div></div>';
		giftcardTooltip.click(function (event) {
			event.preventDefault();
			var userAgent = navigator.userAgent;
			if (userAgent.indexOf('Macintosh') != -1 && userAgent.indexOf('Safari') != -1) {
				if (typeof $(this).attr('aria-describedby') === "undefined") {
					giftcardTooltip.popover('show');
				}
			}
		});
		giftcardTooltip.popover({
			animation: 'true',
			html: 'true',
			content: 'asdfasdf',
			placement: 'bottom',
			template: giftcardTooltipTemplate,
			trigger: 'focus'
		});
	}
};

var successMyAccountPageCallback = function (data) {
	var tmpData = $(data);
	var isLandingPage = $(tmpData).find('#isLandingPage').val();
	if (isLandingPage == 'true') {
		tmpData.find("#load-my-account-norton-script").html($("#loadNortonHiddenDiv").clone());
		$("#ajaxMyAccountLandingPage").html(tmpData);
		tmpData.find("#myAccountFeatures").html($("#myAccountFeaturesContent").html());
		tmpData.find("#myAccountQuickHelp").html($("#myAccountQuickHelpContent").html());
		initGiftCardPopover();
		$("#akamai_myAccount_landing_page").show();
		$("#akamai_login_page").hide();
	} else {
		$("#akamai_myAccount_landing_page").hide();
		$("#akamai_login_page").show();
	}
}
//var errorMyAccountPageCallback = function() {}

/*function to remove model-open class for html when model close*/
$("body").on("hide.bs.modal", function () {
	$("html").removeClass("modal-open");
});

function getNONCEResponce() {
	var errorFlag = false;
	if (enableVerboseDebug) {
		console.log("Inside getNONCEResponce()");
	}
	$.ajax({
		url: "/checkout/common/nonceAjax.jsp",
		type: "POST",
		dataType: "json",
		async: false,
		cache: false,
		success: function (nonceData) {
			if (enableVerboseDebug) {
				console.log("getNONCEResponce() Radial Nonce:" + JSON.stringify(nonceData));
				console.log("getNONCEResponce() START Radial setup()");
			}
			if (typeof nonceData == 'object' && typeof nonceData.nonce != 'undefined' && typeof nonceData.jwt != 'undefined' && nonceData.nonce != '' && nonceData.radialPaymentErrorKey != '50002') {
				Radial.setup(nonceData.nonce, nonceData.jwt);
				if (enableVerboseDebug) {
					console.log("getNONCEResponce() END calling Radial setup()");
				}
				jwt = nonceData.jwt;
				nonceSetup = true;
				nonceRetryAttempts = 0;
				return false;
			}
			nonceRetryAttempts++;
			if (enableVerboseDebug) {
				console.log("Error in getNONCEResponce() and showing the Error radialPaymentErrorKey:" + nonceData.radialPaymentErrorKey);
			}
			if (nonceRetryAttempts == maxNonceRetryAttempts) {
				if (enableVerboseDebug) {
					console.log("Error in getNONCEResponce() and showing the Error msg");
				}
				nonceRetryAttempts = 0;
				nonceSetup = false;
				var radailPaymentErrorCode = nonceData.radialPaymentErrorKey;
				var radailPaymentKey = errorKeyJson[radailPaymentErrorCode];
				var radailPaymentMsg = errorKeyJson[radailPaymentKey];
				if ($('.layaway-template.container-fluid').length == 1) {
					$('.layaway-payment-error').eq(0).before('<span class="errorDisplay">' + radailPaymentMsg + '</span>');
				} else {
					$('#addOrEditCreditCard .tse-content').prepend('<span class="errorDisplay">' + radailPaymentMsg + '</span>');
					$('#addOrEditCreditCard .tse-scroll-content').scrollTop(0);
				}
				return false;
			}
			errorFlag = true;
		},
		error: function (e) {
			console.log(e);
		}
	});
	if (errorFlag) {
		if (enableVerboseDebug) {
			console.log("Error in getNONCEResponce() and Re_trying with Re_try attempts count is: " + nonceRetryAttempts);
		}
		getNONCEResponce();
	}
}

//cardinal tokenization call
function startTokenization() {
	var accountNumber = $('#creditCardNumber').val();
	var expirationMonth = $('#expirationMonth').val();
	var expirationYear = $('#expirationYear').val();
	var cardCode = $('#creditCardCVVCode').val();
	var nameOnCard = $('#nameOnCard').val();
	var creditCardType = $('#creditCardType').val();
	$('.errorDisplay').remove();
	/* if(!cardinalSetup && isCardinalEnabled){
		 console.error('ERROR : Error in processing request with payment service');
		 if(cardinalErrorMessage == ''){
			 $('#addOrEditCreditCard .tse-content').prepend('<span class="errorDisplay">Error in processing request with card validation service, please try after some time.</span>');
			 $('#addOrEditCreditCard .tse-scroll-content').scrollTop(0);
			 return false;
		   } else {
			   $('#addOrEditCreditCard .tse-content').prepend('<span class="errorDisplay">'+cardinalErrorMessage+'</span>');
			   $('#addOrEditCreditCard .tse-scroll-content').scrollTop(0);
			   return false;
		   }
	 }*/
	var v = parseInt(accountNumber.substr(0, 6));
	if (v == '604586' || creditCardType == 'RUSPrivateLabelCard') {
		if (accountNumber == '' || cardCode == '' || nameOnCard == '') {
			$('#addOrEditCreditCard .tse-content').prepend('<span class="errorDisplay">Please enter complete card details</span>');
			$('#addOrEditCreditCard .tse-scroll-content').scrollTop(0);
			return false;
		}
		expirationMonth = $('#expMonthForPLCC').val();
		expirationYear = $('#expYearForPLCC').val();
	} else {
		if (accountNumber == '' || expirationMonth == '' || expirationYear == '' || cardCode == '' || nameOnCard == '') {
			$('#addOrEditCreditCard .tse-content').prepend('<span class="errorDisplay">Please enter complete card details</span>');
			$('#addOrEditCreditCard .tse-scroll-content').scrollTop(0);
			return false;
		}
	}
	var d = new Date();
	var timeStamp = d.getTime();

	var currentMonth = d.getMonth() + 1;
	var currentYear = d.getFullYear();
	if (parseInt(expirationMonth) < currentMonth && parseInt(expirationYear) == currentYear) {
		$('#addOrEditCreditCard .tse-content').prepend('<span class="errorDisplay">The expiration date you entered occurs in the past. Please re-enter the credit card number, expiration date and security code.</span>');
		$('#addOrEditCreditCard .tse-scroll-content').scrollTop(0);
		return false;
	}
	var isEditSavedCard = $('#isEditSavedCard').val();
	if (isEditSavedCard == 'false') {
		if (isCardinalEnabled) {
			//Calling nonce service.
			getNONCEResponce();
			if (!nonceSetup) {
				return false;
			}
			if (enableVerboseDebug) {
				console.log("calling tokenizePan() for accountNumber:" + accountNumber);
			}

			//logging success of token
			if (enableIntegrationLog) {
				integrationLogCall('true', 'token');
			}

			//Invoking Radial tokenize method
			Radial.tokenize(accountNumber, tokenizeCallBackHandler);
		} else {
			$('#cardinalToken').val($('#creditCardNumber').val());
			$('#caBinNum').val($('#creditCardNumber').val().substring(0, 6));
			$('#caLength').val($('#creditCardNumber').val().length);
			onSubmitAddCreditCard();
		}
	} else {
		onSubmitAddCreditCard();
	}
	return false;
}

function integrationLogCall(entry, interfaceName) {
	var truIntegrationLoggingRestAPIURL = $("#truIntegrationLoggingRestAPIURL").val();
	var orderId = $("#orderId").val();
	var pushSite = $("#pushSiteId").val();
	$.ajax({
		url: truIntegrationLoggingRestAPIURL + "?orderId=" + orderId + "&pushSite=" + pushSite + "&entry=" + entry + "&interfaceName=" + interfaceName,
		type: "post",
		headers: {
			"X-APP-API_KEY": "apiKey",
			"X-APP-API_CHANNEL": "mobile",
			"Content-Type": "application/x-www-form-urlencoded"
		},
		dataType: "json",
		async: false,
		success: function (data) {
			console.log();
		}
	});
}

function onSubmitAddress() {
	if ($("#myAccountAddressForm").valid()) {
		var param = {
			url: '/myaccount/intermediate/addressAjaxRequest.jsp',
			data: {
				formSerialize: $("#myAccountAddressForm").serialize(),
				formID: 'addressAjaxRequest'
			}
		};
		makeAjax(param);
	}
	return false;
}

function suggestedAddressSelect() {
	var count = $('input[name="valid-address"]:checked').val();
	if (typeof (count) != "undefined") {
		$("#suggestedAddress1Id").val($("#suggestedAddress1-" + count).val());
		$("#suggestedAddress2Id").val($("#suggestedAddress2-" + count).val());
		$("#suggestedCityId").val($("#suggestedCity-" + count).val());
		$("#suggestedStateId").val($("#suggestedState-" + count).val());
		$("#suggestedCountryId").val($("#suggestedCountry-" + count).val());
		$("#suggestedPostalCodeId").val($("#suggestedPostalCode-" + count).val());
	}
}

function editUserEnteredAddress() {
	$("#suggestedAddress1Id").val($("#suggestedAddress1-0").val());
	$("#suggestedAddress2Id").val($("#suggestedAddress2-0").val());
	$("#suggestedCityId").val($("#suggestedCity-0").val());
	$("#suggestedStateId").val($("#suggestedState-0").val());
	$("#suggestedCountryId").val($("#suggestedCountry-0").val());
	$("#suggestedPostalCodeId").val($("#suggestedPostalCode-0").val());
	$("#addressValidated").val('false');

	var param = {
		url: '/myaccount/intermediate/addressAjaxResponse.jsp',
		data: {
			formSerialize: $("#myAccountAddressForm").serialize() + '&fromSuggestedAddress=true',
			formID: 'editUserEnteredAddress'
		}
	};
	makeAjax(param);
}

function editUserEnteredAddressForCreditCard() {
	$("#suggestedAddress1Id").val($("#suggestedAddress1-0").val());
	$("#suggestedAddress2Id").val($("#suggestedAddress2-0").val());
	$("#suggestedCityId").val($("#suggestedCity-0").val());
	$("#suggestedStateId").val($("#suggestedState-0").val());
	$("#suggestedCountryId").val($("#suggestedCountry-0").val());
	$("#suggestedPostalCodeId").val($("#suggestedPostalCode-0").val());
	$("#addressValidated").val('false');
	var billingAddressNickname = $('#billingAddressNickName1234').val();
	var editCardNickName = $('#editCardNickName1234').val();

	var param = {
		url: '/myaccount/intermediate/cardOverlayFragment.jsp',
		data: {
			formSerialize: $("#addOrEditCreditCard").serialize() + '&fromSuggestedAddress=true&billingAddressNickname=' + billingAddressNickname + '&editCardNickName=' + editCardNickName,
			formID: 'editUserEnteredAddressForCreditCard'
		}
	};
	makeAjax(param);
	$('#billing-address-container .billing-address').show();
	$(".card-number").trigger("keyup");
}

function modifyAddress() {
	if ($('#myAccountAddressForm').valid()) {
		$("#submitAddressId").click();
		suggestedAddressSelect();
	}
	return false;
}

function deleteAddress() {
	$("#removeShippingAddressId").click();
	return false;
}


function removeAddress(nickName) {
	$(".removeAddressHiddenId").val(nickName);
}
function removeAddressConfirm() {
	var name = $(".removeAddressHiddenId").val();
	var param = {
		url: '/myaccount/intermediate/ajaxIntermediateRequest.jsp',
		data: {
			formSerialize: 'name=' + name,
			formID: 'removeShippingAddress'
		}
	};
	makeAjax(param);
	return false;
}

function onSubmitPersonalInfo() {

	var param = {
		url: '/myaccount/intermediate/yourInfoAjaxResponse.jsp',
		data: {
			formSerialize: $("#personalInfoFormValidation").serialize() + '&info=name',
			formID: 'name',
		}
	};
	makeAjax(param);
	bruCss();
	return false;
}

function onSubmitAddress() {
	if ($("#myAccountAddressForm").valid()) {
		var param = {
			url: '/myaccount/intermediate/addressAjaxRequest.jsp',
			data: {
				formSerialize: $("#myAccountAddressForm").serialize(),
				formID: 'addressAjaxRequest'
			}
		};
		makeAjax(param);
	}
	return false;
}

function onSubmitPersonalInfo() {

	var param = {
		url: '/myaccount/intermediate/yourInfoAjaxResponse.jsp',
		data: {
			formSerialize: $("#personalInfoFormValidation").serialize() + '&info=name',
			formID: 'name',
		}
	};
	makeAjax(param);
	bruCss();
	return false;
}

function onSubmitResetPasswordForm() {
	var contextPath = $(".contextPath").val();
	$('.errorDisplay').remove();
	$.ajax({
		type: "POST",
		cache: false,
		url: contextPath + "myaccount/intermediate/ajaxIntermediateRequest.jsp?formID=submitResetPasswordForm",
		data: $("#reset-password-page").serialize(),
		success: function (data) {
			$('.errorDisplay').remove();
			if (data.indexOf('Following are the form errors:') > -1) {
				var msg = data.split('Following are the form errors:')[1];
				$('.reset-password-coloum').prepend('<span class="errorDisplay">' + msg + '</span>');
				addError('reset-password-page');
				$("#passResetForm").attr("aria-label", msg);
			} else {
				location.href = contextPath + "myaccount/myAccount.jsp";
			}
		},
		dataType: "html"
	});

	return false;
}

//Calling tokenize CallBackHandler method.
function tokenizeCallBackHandler(data) {
	if (enableVerboseDebug) {
		console.log("tokenizeCallBackHandler() Start***");
		console.log("tokenizeCallBackHandler() response data:" + JSON.stringify(data));
	}

	var truIntegrationLoggingRestAPIURL = $("#truIntegrationLoggingRestAPIURL").val();
	var orderId = $("#orderId").val();
	var pushSite = $("#pushSiteId").val();


	var radailPaymentErrorCode;
	switch (data.ActionCode) {
		case "FAILURE":
			radailPaymentErrorCode = data.failure_code;
			break;
		case "ERROR":
			radailPaymentErrorCode = data.error_code;
			break;
	}

	//logging success of token
	if (enableIntegrationLog) {
		integrationLogCall('false', 'token');
	}

	//getting nonce again and re_try token
	if (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '50002' && getTokenRetryAttempts < maxGetTokenRetryAttempts) {
		getTokenRetryAttempts++;
		//Added  time out for defect=TSJ-6625
		setTimeout(function () { startTokenization(); }, 4000);
		return false;
	}

	//re_try token
	if (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '50001' || radailPaymentErrorCode == '50003' && getTokenRetryAttempts < maxGetTokenRetryAttempts) {
		getTokenRetryAttempts++;
		//Added  time out for defect=TSJ-6625
		setTimeout(function () { startTokenization(); }, 4000);
		return false;
	}

	if ($('.layaway-template.container-fluid').length == 1) {
		if (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '40001' || radailPaymentErrorCode == '40002' || radailPaymentErrorCode == '40003' || radailPaymentErrorCode == '40004' || radailPaymentErrorCode == '50001' || radailPaymentErrorCode == '50002' || radailPaymentErrorCode == '50003') {
			getTokenRetryAttempts = 0;
			$('.layaway-payment-error').eq(0).before('<span class="errorDisplay">' + radailPaymentErrorCode + '</span>');
			return false;
		}

		$('#cardinalToken').val(data.account_token);
		$('#cBinNum').val($('#ccnumber').val().substring(0, 6));
		$('#cLength').val($('#ccnumber').val().length);
		if (enableVerboseDebug) {
			console.log("In tokenizeCallBackHandler layaway order placing with ....... Action Code :" + data.ActionCode);
		}
		commitCreditCard();
		return false;
	} else {
		if (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '40001' || radailPaymentErrorCode == '40002' || radailPaymentErrorCode == '40003' || radailPaymentErrorCode == '40004' || radailPaymentErrorCode == '50001' || radailPaymentErrorCode == '50002' || radailPaymentErrorCode == '50003') {
			getTokenRetryAttempts = 0;
			var radailPaymentKey = errorKeyJson[radailPaymentErrorCode];
			var radailPaymentMsg = errorKeyJson[radailPaymentKey];
			$('#addOrEditCreditCard .tse-content').prepend('<span class="errorDisplay">' + radailPaymentMsg + '</span>');
			$('#addOrEditCreditCard .tse-scroll-content').scrollTop(0);
			return false;
		}
		var creditCardNumber = data.account_token;
		$('#cardinalToken').val(creditCardNumber);
		$('#caBinNum').val(creditCardNumber.substring(0, 6));
		$('#caLength').val($('#creditCardNumber').val().length);
		onSubmitAddCreditCard();
	}

}