var guestFlag;
var contextPath = '';
var TRUImagePath = '';
var totalNumberOfLayawayOrders;
var isCardNumInLimit = true;
var cardTypeForCvvValidation = '';

/* Start: Fix for GEOFUS-1684 (point no: 3). Moved codes to top of this file */  
function displayWelcomeAndCartItemCountBru() {
	var domainName = $('.urlFullContextPath').val() || '';
	$.ajax({
		headers: {
			'X-APP-API_KEY': 'apiKey',
			'X-APP-API_CHANNEL': 'webstore'
		},
		url: domainName + 'rest/model/atg/commerce/ShoppingCartActor/accountCartSummary?pushSite=ToysRUs',
		type: 'GET',
		xhrFields: {
			'withCredentials': true
		},
		success: function (responseData) {
			jsonPopulateyWelcomeMsgAndCartCount(responseData);
		},
		error: function (errorData) {
			console.log("Bru ajax fail: " + errorData);
		}
	});
}

function jsonPopulateyWelcomeMsgAndCartCount(myData) {
	var welcomeMsg = 'my account';
	/* For welcome message */
	if (myData.hasOwnProperty('profile')) {

		if (myData.profile.hasOwnProperty('firstName') && myData.profile.firstName != null && myData.profile.firstName != '') {

			welcomeMsg = 'welcome, ' + myData.profile.firstName;
		} else if (myData.profile.hasOwnProperty('login')) {

			var emailName = myData.profile.login;
			welcomeMsg = 'welcome, ' + emailName.substring(0, emailName.indexOf('@'));
		}
	}

	/*  Cart item count */
	var cartCount = 0;
	if (myData.hasOwnProperty('orderSummary') && myData.orderSummary.hasOwnProperty('itemCount')) {
		cartCount = myData.orderSummary.itemCount;
	}

	$('#VisitorFirstNamecookieDIV').html(welcomeMsg);
	$('#ajaxCount').text(cartCount);
}

function setFirstNameCookie() {
		var welcomeName = $('#welcomeName').val();
		var myAccountName = $('#myAccount').val();
		if (isEmpty(welcomeName)) {
			welcomeName = "welcome";
		}
		if (isEmpty(myAccountName)) {
			myAccountName = "my account";
		}
		var VisitorFirstNameValue = $.cookie("VisitorFirstName");

		if (document.getElementById("VisitorFirstNamecookieDIV")) {
			if (!isEmpty(VisitorFirstNameValue)) {
				var welComeMessageWithFirstName = welcomeName.concat(",&nbsp", VisitorFirstNameValue)
				document.getElementById("VisitorFirstNamecookieDIV").innerHTML = welComeMessageWithFirstName;
			} else {
				document.getElementById("VisitorFirstNamecookieDIV").innerHTML = myAccountName;
			}
		}
}

function setProgressBarCookie() {
	var freeShippingProgressBarCookieValue = $.cookie("freeShippingProgressBarCookie");
	if (!isEmpty(freeShippingProgressBarCookieValue)) {
		ajaxProgressBar(freeShippingProgressBarCookieValue);
	}
}

function setOrderItemCountCookie() {
	var orderItemCountCookieValue = $.cookie("itemCountCookie");
	if (!isEmpty(orderItemCountCookieValue)
		&& document.getElementById("ajaxCount")) {
		document.getElementById("ajaxCount").innerHTML = orderItemCountCookieValue;
	}
}
$(window).load(function () {
	var pageName = $("#pageName").val();
	var curentDomainName = window.location.hostname;
	if (curentDomainName.indexOf('.babiesrus.com') > -1) {
		displayWelcomeAndCartItemCountBru();
	} else {
		setFirstNameCookie();
		if(typeof pageName == 'undefined' || pageName == '' || pageName != 'shoppingCart'){
			setProgressBarCookie();
		}
		setOrderItemCountCookie();
	}
});

/* End: Fix for GEOFUS-1684 (point no: 3). Moved codes to top of this file */

HistoryAPI = (function (H) {
	var isSupported = !!(window.history && history.pushState), vm = this;
	vm.pushStateFunc = function (data, title, url) {
		if (!isSupported) return false;
		H.pushState(data, title, url);
	};
	vm.popStateEvent = function (func) {
		if (!isSupported || typeof func !== "function") return false;
		window.onpopstate = func;
	};
	return (!isSupported) ? {} : {
		pushState: vm.pushStateFunc,
		popStateEvent: vm.popStateEvent,
		presentStateData: ""
	};
})(history || {});
function formatCreditCardDisplay() {
	if (($('#review-tab').length > 0 || $('#nongoodbye_order_confirmation').length > 0) && $('.creditcard-format').length > 0 && ($('.creditcard-format').html().trim()).length > 0 && ($('.creditcard-format').html().trim()).charAt(4) != ' ') {
		$('.creditcard-format').html($('.creditcard-format').html().trim().replace(/(.{4})(?!$)/g, '$1.').replace(/\./gi, " "));
	}
}
var customerId;
var pageNumber;
var noOfRecordsPerPage;
var layawayOrderInformationTtemplate;
var orderDetails = {};
var pushSite;

//elements cache
var $searchTerm = $("#searchText");

function hideFeedbackFloatForSOS() {
	if ($('.sos-header-message').length > 0) {
		$('.oo_feedback_float').hide();
	}
}

//credit card 4 digit format validation start here
function formatCreditCardDisplay() {
	if (($('#review-tab').length > 0 || $('#nongoodbye_order_confirmation').length > 0) && $('.creditcard-format').length > 0 && ($('.creditcard-format').html().trim()).length > 0 && ($('.creditcard-format').html().trim()).charAt(4) != ' ') {
		$('.creditcard-format').html($('.creditcard-format').html().trim().replace(/(.{4})(?!$)/g, '$1.').replace(/\./gi, " "));
	}
}
//credit card 4 digit format validation End here

//Start: To block UI
var blockUI = function () {
	$('#ajaxLoaderUI').show();
	$('html').addClass('ajaxBlockUI');
}
//End: To block UI
//Start: To unblock UI
var unBlockUI = function () {
	$('#ajaxLoaderUI').hide();
	$('html').removeClass('ajaxBlockUI');
}
//End: To unblock UI

function hideFeedbackFloatForSOS() {
	if ($('.sos-header-message').length > 0) {
		$('.oo_feedback_float').hide();
	}
}

function estimatedTaxTooltip() {
	if ($('#estimatedIslandTaxLabel').length > 0) {
		var estimatedIslandTaxLabel = $('#estimatedIslandTaxLabel');

		var estimatedIslandTaxTooltipTemplate = $('.estimatedIslandTaxTooltip');

		estimatedIslandTaxLabel.click(function (event) {
			event.preventDefault();
		});

		estimatedIslandTaxLabel.popover({
			animation: 'true',
			html: 'true',
			content: 'asdfasdf',
			placement: 'bottom',
			template: estimatedIslandTaxTooltipTemplate,
			trigger: 'focus'
		});
	}
	if ($('#estimatedLocalTaxLabel').length > 0) {
		var estimatedLocalTaxLabel = $('#estimatedLocalTaxLabel');

		var estimatedLocalTaxTooltipTemplate = $('.estimatedLocalTaxTooltip');

		estimatedLocalTaxLabel.click(function (event) {
			event.preventDefault();
		});

		estimatedLocalTaxLabel.popover({
			animation: 'true',
			html: 'true',
			content: 'asdfasdf',
			placement: 'bottom',
			template: estimatedLocalTaxTooltipTemplate,
			trigger: 'focus'
		});
	}
}

function bindServerErrorValidation(obj, message) {
	obj = $(obj);
	if (obj.length <= 0) {
		return false;
	}
	obj.addClass('error');

	obj.after('<label class="errorDisplay" for="' + obj.attr('id') + '">' + message + '</label>');
	/*obj.on("keyup",function(){
		$(this).removeClass('error');
		$(this).next('orderError').remove;
		//obj.off("keyup");
	});*/
};

function clearPrePopulatedData() {
	if ($("#crdNo2,.gcNumber,.ean,#pin2").length > 0) {
		$("#crdNo2,.gcNumber,.ean,#pin2").val("");
	}
}

var setCookieValue = function (cookieName, cookieValue, currentDomain) {
	var persistDays = $('.persistDays').val();
	var persistInt = (parseInt(persistDays) * 60) / (60 * 60 * 24);
	$.cookie(cookieName, cookieValue, { path: '/', expires: 366, domain: currentDomain });
}

function isSearchTermValid(searchTerm) {
	//google PII issue fix. prevents the user from entering his email address in the search term, so it wont be appended to the URL
	var emailRegex = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/g;
	return !!searchTerm && searchTerm.trim().length > 0 && !emailRegex.test(searchTerm.trim());
}

$(document).ready(function () {

	setTimeout(function () {
		clearPrePopulatedData();
	}, 4000);

	initTRUComponents();

	$(document).on('click', '.domain-identity', function (evt) {
		var cookieDomainVar = $(this).attr('data-site-identity-cookie-name');
		var cookieDomainValue = $(this).attr('data-site-identity-cookie-value');
		//var currentDomain = extractDomainName(window.location.host);
		var currentDomain = '.toysrus.com';
		if (typeof cookieDomainVar != 'undefined' && cookieDomainVar != null && cookieDomainVar != '') {
			setCookieValue(cookieDomainVar, cookieDomainValue, currentDomain);
		}
	});

	$(document).on('focusout', '#sendFormData', function () { /* Fix for TUW-63865 */
		$(this).parents('.modal').find('.close-modal').focus();
	});

	$(document).on("keypress", '[name="nickName"], [name="nickname"]', function (evt) {
		if (!evt.charCode) return true;
		var part1 = this.value.substring(0, this.selectionStart);
		var part2 = this.value.substring(this.selectionEnd, this.value.length);
		if (! /^[A-Za-z0-9 ]*$/.test(part1 + String.fromCharCode(evt.charCode) + part2))
			return false;
	});

	$(document).on("paste", '[name="nickName"], [name="nickname"]', function (evt) {
		var clipboardData = evt.clipboardData || window.clipboardData;
		var txtTmp = '';
		if (clipboardData && clipboardData.getData && clipboardData.getData('Text')) {
			txtTmp = clipboardData.getData('Text')
		} else {
			txtTmp = evt.originalEvent.clipboardData.getData('text');
		}
		if (! /^[A-Za-z0-9 ]*$/.test(txtTmp)) {
			evt.preventDefault();
		}
	});

	$(document).on("keypress", '.creditCardCVV', function (evt) {
		if (!evt.charCode) return true;
		//var part1 = this.value.substring(0, this.selectionStart);
		//var part2 = this.value.substring(this.selectionEnd, this.value.length);
		//if (! /^[0-9]*$/.test(part1 + String.fromCharCode(evt.charCode) + part2))
		if (! /^[0-9]*$/.test(String.fromCharCode(evt.charCode)))
			return false;
	});

	$(document).on("paste", '.creditCardCVV', function (evt) {
		evt.preventDefault();
	});

	$(document).on("click", function () {
		$(".adzonne-container, .sosAddZone, .ads_container, [class^=ad-zone], .ad-zone-970-90").css("z-index", 99);
		/*var $this = $(this);
		if($(".getFocus-quickView").length){
			$(".getFocus-quickView").each(function(){
				if($this.parents(".product-block-image") != $(this).parents(".product-block-image")){
					$(this).removeClass("getFocus-quickView");
				}
			});
		}*/
	});

	$(document).on('click', '[data-target="#giftWrapDetailModel"]', function () {
		setTimeout(function () {
			$('#giftWrapDetailModel .tse-scrollable').TrackpadScrollEmulator({
				autoHide: false,
				wrapContent: true
			});
		}, 600);
	});
	
	/* Start: Fix for GEOFUS-1855 */
	$(document).on('hidden.bs.modal', '#shoppingCartModal', function () {
		$('html, body').removeClass("modal-open");
	});
	/* End: Fix for GEOFUS-1855 */
	/* GEOFUS-1094 - Fix for ESRP popup on PLP pages */
	$(document).on('click','#shoppingCartModal .x-image', function(){
		if ($('html').hasClass("modal-open")) {
			$('html').removeClass("modal-open")
		}
	})

	$(document).on('click', '.im-mature-btn.fromPLP', function () { // clicked the I'm matured button
		plpContinueAddToCart = true;
		$('.esrbratingModel').modal('hide');
		$('.product-add-to-cart.esrbAddToCart').trigger("click");
	});
	/* end GEOFUS-1094 - Fix for ESRP popup on PLP pages */
	
	/* Start: PLP page addToCart */
	var plpContinueAddToCart = false; // flag created for ESRB modal display
	$(document).on('click', '.inventry-avail-PLP-add-to-cart, .store-inventory-avail-PLP-add-to-cart', function () {
		
		/* GEOFUS-1094 - Fix for ESRP popup on PLP pages */
		var isEsrbProduct = parseInt($(this).parents(".product-block").find(".product-block-esrbRating").length);
			if(isEsrbProduct > 0 && plpContinueAddToCart === false){ // enabling the esrb popup
				$('#shoppingCartModal').modal("hide");
				$(this).addClass("esrbAddToCart");
				$('.esrbratingModel').modal('show');
				console.log("This is ESRB product");
				$('.im-mature-btn').addClass('fromPLP');
			}

			else{
			$('#shoppingCartModal').modal("hide");
			plpContinueAddToCart = false;
			/* end GEOFUS-1094 - Fix for ESRP popup on PLP pages */
			var productId = $(this).data('productid') ? $(this).data('productid') : '';
			var skuId = $(this).data('skuid') ? $(this).data('skuid') : '';
			var myStoreCookie = $.cookie("favStore");
			var myStoreID = '';
			if ($(this).hasClass('store-inventory-avail-PLP-add-to-cart') && typeof myStoreCookie != 'undefined' && myStoreCookie != '' && myStoreCookie != null) {
				try {
					var myStoreArr = myStoreCookie.split('|');
					myStoreID = (myStoreArr.length > 1) ? myStoreArr[1] : '';
				} catch (e) { }
			}
			var parameters = {
				formID: 'addItemToCart',
				quantity: 1,
				productId: productId,
				skuId: skuId,
				locationId: myStoreID,
				channelItemFromPDP: false
			};
			$.ajax({
				type: "POST",
				async: false,
				cache: false,
				dataType: "html",
				url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp",
				data: parameters,
				success: function (data) {
					$("#minicart").load(contextPath + "cart/mini_cart.jsp", function () {
						progressBar();
						bruCss();
					});
					initCartScroll();
					$('#shoppingCartModal').html(data);
					$('#shoppingCartModal').modal("show");
					var cartAddSource = $("#cartAddSource").val();
					if (cartAddSource == 'Product List Page' || (cartAddSource == undefined && ($(".search-template").length || $(".template-family").length || $(".category-template").length || $(".sub-category-template").length))) {
						//$("#quickviewModal").modal("hide");// commented as per TUREP-2158-since quickview functionality deprecated, needs to be triggered on PLP
						loadOmniScriptQuickView("Product List Page");//For omniture call(addToCart from QuickView)
					}
	
				},
				error: function (xhr, ajaxOptions, thrownError) {
					if (xhr.status == '409') {
						var visitorName = $.cookie("VisitorFirstName");
						if (isEmpty(visitorName)) {
							location.reload();
						}
						else {
							var contextPath = $(".contextPath").val();
							location.href = contextPath + 'myaccount/myAccount.jsp?sessionExpired=true';
						}
					}
				}
	
			});
			return false;
		} // else closing GEOFUS-1094 - Fix for ESRP popup on PLP pages
	});

	$(document).on('click', '.store-avail-PLP-add-to-cart', function () {

		var contextPath = $('.contextPath').val() || '/';
		var skuId = $(this).data('skuid') ? $(this).data('skuid') : '';
		if (!$("#productIdForCart").length) {
			var productIdForCart = document.createElement("INPUT");
			productIdForCart.setAttribute("type", "hidden");
			$(productIdForCart).attr("id", "productIdForCart");
			document.body.appendChild(productIdForCart);
		}
		$("#productIdForCart").val($(this).data('productid'));

		$.ajax({
			type: 'POST',
			dataType: 'html',
			url: contextPath + 'storelocator/findInStorePDPInfo.jsp?relationshipItemId=&quantity=1&updateInStoreData=true',
			data: $("#inStorePickup").serialize() + '&selectedSkuId=' + skuId,
			async: false,
			success: function (resData) {
				$('#findInStoreModal').html(resData);
				$('#findInStoreModal').closest('.product-block-unselected').css('z-index', '9999');
				$('#findInStoreModal').find("#find-in-store-results header, #find-in-store-results #store-locator-results-tabs, #find-in-store-results #store-locator-results-scrollable, #find-in-store-results .find-in-store-search img").hide();
				$('#findInStoreModal').find("#find-in-store-results .find-in-store-search").show();

				$('#findInStoreModal').modal("show");

				/*getfavStore = getCookie("favStore");
				if(getfavStore != ""){
					findinstorecommon('NO');
				}*/
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					var visitorName = $.cookie("VisitorFirstName");
					if (isEmpty(visitorName)) {
						location.reload();
					}
					else {
						var contextPath = $(".contextPath").val();
						location.href = contextPath + 'myaccount/myAccount.jsp?sessionExpired=true';
					}
				}

			}
		});
		return false;
	});
	/* End: PLP page addToCart */
	$(document).on("click", ".sos-logout-btn", function () {
		$("#sosHeaderLogoutId").trigger("click");
		signOutForTealium();
	});

});
//Contact Us Page
$(document).on('click', '#help-customer-service-submit', function () {
	$('.successMessage').remove();
	if ($('#contactus').valid()) {
		contactUsSubmit();
	}
	return false;
});
function contactUsSubmit() {
	var contactus = {
		name: $('#contactus [name="name"]').val(),
		phoneNumber: $('#contactus [name="phoneNumber"]').val(),
		email: $('#contactus [name="email"]').val(),
		subject: $('#contactus [name="contactSubject"]').val(),
		orderNumber: $('#contactus [name="contactOrderNum"]').val(),
		question: $('#contactus [name="contactComment"]').val()
	};

	$.ajaxSetup({
		headers: {
			'X-APP-API_KEY': 'apiKey',
			'X-APP-API_CHANNEL': 'webstore'
		}
	});
	$.ajax({
		url: '/rest/model/com/tru/contactus/ContactUsActor/contactUs?&pushSite=ToysRUs',
		type: 'POST',
		data: contactus,
		xhrFields: {
			'withCredentials': true
		},
		dataType: "json",
		success: function (responseData) {
			removeBackendErrorMessagesInline($('.help-customer-service-content'));
			if (typeof responseData.s != 'undefined' && responseData.s == '1') {
				$('.help-customer-service-confirmation').show();
				$('.help-customer-service-content').hide();
				$(window).scrollTop(0);
			} else {
				addBackendErrorsToForm(window.contactErroMsg, $('.help-customer-service-content'));
			}
		},
		error: function (data) {
			console.log(data);
		}
	});

}
$(document).on('click', '.order-history-template a,order-history-template button,.my-account-template a,.my-account-template button', function () {
	if (!$('.orderHistoryCancelOrder').hasClass('hide')) {
		$('.orderHistoryCancelOrder').addClass('hide')
	}
})

$(document).ready(function () {
	if ($(document).find('#account-and-payment')) {
		$(document).find('#account-and-payment').trigger('click');
	}

	$(document).on('click', '.new-credit-card-open', function () {  /* For TUW-58619 */
		$('#payment-page-apply-today').modal('show');
		setTimeout(function () {
			$('#payment-page-apply-today .tse-scrollable').TrackpadScrollEmulator({
				wrapContent: false,
				autoHide: false
			});
		}, 600);
	});

	var $headerHeight = $(".SOSHeader .navbar-nav.navbar-right"), $windowWidth = $(window).width();
	setTimeout(function () {
		if ($headerHeight.length > 0 && $headerHeight.children().length > 5) {
			if ($('.collection-template').length > 0) {
				if ($windowWidth > 999 && $windowWidth < 1300) {
					$('.collection-template').css("padding-top", 129 + $headerHeight.height() + "px");
				}
				else if ($windowWidth > 1300 && $windowWidth < 3000) {
					$('.collection-template').css("padding-top", 90 + $headerHeight.height() + "px");
				}
				else if ($windowWidth > 3000) {
					$('.collection-template').css("padding-top", 189 + $headerHeight.height() + "px");
				}
			}

		}
		else if ((($('#nongoodbye_order_confirmation') || $('.order-confirmation-page-container')).length > 0) && $headerHeight > 0) {
			if ($windowWidth > 999 && $windowWidth < 1300) {
				$('.order-confirmation-page-container').css("margin-top", 104 + $headerHeight.height() + "px");
			}
			else if ($windowWidth > 1300 && $windowWidth < 3000) {
				$('.order-confirmation-page-container').css("margin-top", 104 + "px");
			}
			else if ($windowWidth > 3000) {
				$('.order-confirmation-page-container').css("margin-top", 200 + $headerHeight.height() + "px");
			}
		}
	}, 0);

	var headerLink = window.location.href;
	$('#sdm-header').attr('href', headerLink);
	setTimeout(function () {
		$(".my-account-template").parents("body").find("#scrollToTop").addClass("HideToTopBtn");
	}, 1000);

	window.contextPath = $(".contextPath").val() || '/';

	$(document).on('click', '#exploreButton', function () {
		populateRecentlyViewedItems('megamenu');
		if ($('.explore-recently-viewed-single').length > 0) {
			$('#exploreBox').removeClass('noRecentlyViewedProducts').css({ 'width': '', 'max-width': '' });

			$('#recentProducts .tse-scroll-content').css('width', '100%');
			setTimeout(function () {
				$('#recently-viewed-header #clearRecentlyViewedItems').focus();
				$(document).on('focusout', '#recentlyViewedCookie a:last', function () {
					$('#exploreBox #close-shopby').focus()
				});
			}, 600);
		}
	});
	/* Start: fix for Megamenu - ITR4_EarlyEnd */
	/* 	if($('#checkout-container').length != 1){
			megamenuOverlay();
		} */
	/* Start: fix for Megamenu - ITR4_EarlyEnd */
	//setTimeout(function(){
	if (($(".order-history-template").length > 0)) {
		loadorderHistoryOrders(false);
	}
	//},1000);

	var isSOS = $('#sosvalue').val();
	if (isSOS == 'true') {
		$('.main-toy-grid').css('margin-top', '130px');
	}
	//}
	//disable click Functionality
	$('body').on('click', '.disableClickFunction', function (e) {
		e.stopImmediatePropagation();
		return false;
	});

	$(document).on('change', '.select-state-option', function () {
		var selectedOption = $(this).val();
		if (selectedOption == 'other') {
			$('#otherState').addClass('display-block');
		} else {
			$('#otherState').removeClass('display-block').val('');
		}
	});

	$(document).on('click', '.cancelOrder', function () {
		var cancelOrderValue = $(this).parents(".order-history-order").find(".orderHistoryDetailModal").html();
		$("#orderHistoryCancelOrder .cancelOrderNumber").val(cancelOrderValue);
	});

	formatCreditCardDisplay();

	/* Start: popover for see terms in pickup tab */
	var storePickupSeeTerms = $('a[data-target="pickup-see-terms-popover"]');

	var storePickupSeeTermsTemplate = $('.storePickupSeeTerms');

	storePickupSeeTerms.hover(function (event) {
		event.preventDefault();
	});

	storePickupSeeTerms.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: storePickupSeeTermsTemplate,
		trigger: 'hover'
	});
	/* End: popover for see terms in pickup tab */
	// demo issues

	$(document).on('mouseover', '.imidiateChild', function () {
		var content = $(this).find('.mega-menu-categories-ad').html().trim().length;
		$('#shopByContainer .searchByCategories').find('.border-space').hide();
		if (content <= 0) {
			$('.border-space').css('display', 'none');
			$('.mega-menu-categories').css('border-right', 'none');
			$('#shopByContainer .searchByCategories').find('.border-space').hide();
		} else {
			$('.mega-menu-categories').css('border-right', '#d2d2d2 solid 1px');
			$('#shopByContainer .searchByCategories').find('.border-space').show();
		}
	});
	// demo issues

	//TUW-35305
	$(document).on('click', '#signInBtn', function (e) {
		$(".navbar-right").find("#my-account-popover-struct").hide();
		$("#hearderLoginOverlay #login, #hearderLoginOverlay #sign-in-password-input, #hearderLoginOverlay #sign-in-password-inputmaskedPassword0").val("");
	});
	//TUW-35305

	var referrer = "";
	if (localStorage.getItem("referrer")) {
		referrer = localStorage.getItem("referrer");
	}

	localStorage.setItem("referrer", window.location.href);


	$(document).on('click', '.welcome-back-overlay .welcome-checkbox-group span:first-child', function () {
		$(this).toggleClass('checkbox-sm-on');
		document.cookie = 'abandonCheckbox=' + $(this).hasClass("checkbox-sm-on");
		//setTimeout(function(){
		$('.welcome-back-overlay').find('[data-dismiss="modal"]').click();
		//}, 50);
	});
	//setting present state in History Api
	if (HistoryAPI.presentStateData === "" && $(".sub-cat-container").length && $(".sub-cat-category-wrapper").length && $(".showing-results").length && $("#filtered-products .row-footer-margin").length && $(".text-right.col-narrow-by").length && $(".sub-cat-category-output").length && $('#showFacetsAccordion').length) {
		HistoryAPI.presentStateData = {
			subCatContainerData: $(".sub-cat-container").html(),
			subcatCategorywrapper: $(".sub-cat-category-wrapper").html(),
			showingResults: $(".showing-results").html(),
			filteredProducts: $("#filtered-products .row-footer-margin").html(),
			textRightNarrowBy: $(".text-right.col-narrow-by").html(),
			subcatOutput: $(".sub-cat-category-output").html(),
			showFacets: $('#showFacetsAccordion').val(),
			filterCount: $(".sub-cat-filter-list").length
		};

		//History API on state change
		HistoryAPI.popStateEvent(function (e) {
			var stateObj = e.state || HistoryAPI.presentStateData;
			$(".sub-cat-container").html(stateObj.subCatContainerData);
			$(".sub-cat-category-wrapper").html(stateObj.subcatCategorywrapper);
			$(".showing-results").html(stateObj.showingResults);
			$("#filtered-products .row-footer-margin").html(stateObj.filteredProducts);
			$(".text-right.col-narrow-by").html(stateObj.textRightNarrowBy);
			$(".sub-cat-category-output").html(stateObj.subcatOutput);
			$('#showFacetsAccordion').val(stateObj.showFacets);

			refreshNarrowByThirdPartyInitializations();

			if (stateObj.filterCount > 0)
				$(".clear-all-filters").removeClass('hide');
			// modified for B&S spark UI changes
			//Facet close if it is false 
			/*if(stateObj.showFacets == "false"){  
				$('.compare-menu').offcanvas('hide');
				$("#sub-category-narrow-by-button").addClass('disableClickFunction');
			}else{
				$("#sub-category-narrow-by-button").removeClass('disableClickFunction');
			}*/
		});
	}






	$.ajaxSetup({ cache: false });
	bruCss()
	//reInitValidation();
	passwordMeter();
	quickviewpopup();
	if ($(".home-jssor-bullet > div").length == 1) { $('#carousel-right-arrow').hide(); }
	var suggesssionWidth = $("#searchText").css("width");
	$(".tt-dropdown-menu").css({ "width": suggesssionWidth });


	var cardFromOrder = $("#cardFromOrder").attr("value");
	if (cardFromOrder == "true") {
		$("#radio-creditcard").click();
	}

	// no review css

	var countreview = $('.pr-snippet-review-count').text();
	if (countreview == "(No reviews)")
		$('.product-details-template div#pr_snippet_category').css(" min-width:", "95px");
	else
		$('.product-details-template div#pr_snippet_category').css(" min-width:", "170px");

	// no review css

	/* $('body').on('click','.disableClick',function(e){
			e.preventDefault();
			return false;
	 });*/
	$("#carousel-left-arrow").hide();
	//setTimeout(function(){
	$(".tru-logo-sm").parents(".global-nav").find(".navbar-bru").css("background-color", "#5e2d91");
	//}, 'fast');
	//setTimeout(function(){
	$(".bru-logo-sm").parents(".global-nav").find(".navbar-tru").css("background-color", "#004EBC");
	//}, 'fast');

	$(document).on('mouseover', '#mainMenu li.imidiateChild', function () {
		$('#exploreBox').removeClass('noRecentlyViewedProducts').css({ 'width': '', 'max-width': '' });
		$('#recentProducts').hide();
		var id = $(this).attr('id').split('-')[1];
		$('#mainMenu li > div').hide();
		$('#' + id).show();
		//$('.border-space').show();
		try {
			var scrollable = $(this).find('.tse-scrollable');
			var recalculate = function () {
				setTimeout(function () {
					scrollable.TrackpadScrollEmulator('recalculate');
					scrollable.find('.tse-scroll-content').height(460);
				}, 100);
			};
			scrollable.TrackpadScrollEmulator({
				wrapContent: true,
				autoHide: true
			});
		}
		catch (e) {
			console.log('Error(Mega Menu):' + e);
		}
	});
	$(document).on('mouseover', '#mainMenu .imidiateChild .tse-scroll-content', function () {
		var tseScrollContentTop = $(this).scrollTop();
		var tseScrollContentHeight = $(this).height();
		var tseContentHeight = $(this).find('.tse-content').height();

		if (tseScrollContentTop >= (tseContentHeight - tseScrollContentHeight)) {
			$('.fadeout.recentproducts').hide('fast');
		} else {
			$('.fadeout.recentproducts').show('fast');
		}
	});

	$(".flag-button").click(function () {
		$('html').css({ 'overflow': '', 'height': '' });
		$('body').css("overflow", "");
	});

	$(document).on('click', '.search-icon, .search-icon-tru, .search-icon-bru', function () {
		var searchTerm = $searchTerm.val();
		if (!isSearchTermValid(searchTerm)) {
			$searchTerm.attr('placeholder', '').val('');
			$('#searchErrorMessage').show();
			return false;
		}
		$("#searchForm").submit();
	});
	$('body').on('keypress keydown', '#searchText', function () {
		if ($('#searchErrorMessage[style*=block]').length) {
			$('#searchErrorMessage').hide();
		}
	});
	$(document).on('submit', '#searchForm', function () {
		var searchTerm = $searchTerm.val();
		if (!isSearchTermValid(searchTerm)) {
			$searchTerm.attr('placeholder', '').val('');
			$('#searchErrorMessage').show();
			return false;
		}
		var ttInputTmp = searchTerm.replace(/[&|]/g, '');
		if (ttInputTmp == '') return false;
		$searchTerm.val(ttInputTmp);
	});

	$('#cancelRemCardNum').click(function () {
		$('#myAccountUpdatePasswordModal').hide();
	});

	$(document).on('click', '#personal-submit-btn', function () {
		if ($("#personalInfoFormValidation").valid()) {
			onSubmitPersonalInfo();
		}
		return false;
	});

	$(document).on('click', '#loginSubmitBtn', function () {
		if ($('#signInForm').valid()) {
			onSubmitSigninForm();
			return false;
		} else {
			checkMaskPasswordError($('#signInForm'));
			return false;
		}

	});

	$(document).on('click', '#createAccountSubmitBtn', function () {
		if ($('#createAccountForm').valid()) {
			onSubmitCreateAccountForm();
		} else {
			checkMaskPasswordError($("#createAccountForm"));
		}
		return false;
	});

	$(document).on('click', '#passResetForm-page', function () {
		if ($('#reset-password-page').valid()) {
			onSubmitResetPasswordForm();
		}
		else {
			$('.errorDisplay').remove();
			checkMaskPasswordError($('#reset-password-page'));
		}
		return false;
	});

	$(document).on('submit', '#emailAddressFormValidation', function () {
		if ($("#emailAddressFormValidation").valid()) {
			var param = {
				url: '/myaccount/intermediate/yourInfoAjaxResponse.jsp',
				data: {
					formSerialize: $("#emailAddressFormValidation").serialize() + '&info=email',
					formID: 'email'
				}
			};
			makeAjax(param);
		}
		return false;
	});

	$(document).on('submit', '#passwordFormValidation', function () {
		//removeBackendErrorMessagesInline($('#passwordFormValidation'));
		$('#myAccountUpdatePasswordModal .errorDisplay').remove();
		if ($('#passwordFormValidation').valid()) {
			var param = {
				url: '/myaccount/intermediate/yourInfoAjaxResponse.jsp',
				data: {
					formSerialize: $("#passwordFormValidation").serialize() + '&info=password',
					formID: 'password'
				}
			};
			makeAjax(param);
		} else {
			setTimeout(function () {
				checkMaskPasswordError($("#passwordFormValidation"));
			}, 1000);
		}
		return false;
	});

	$('.drop-down select').change(function () {
		location.href = $(this).val();
	});

	$(document).on("click", '#signin-submit-btn', function () {
		$('.errorDisplay').remove();
		$(this).attr('disabled', 'disabled');
		if ($('#hearderLoginOverlay').valid()) {
			var requestPage = $("#requestPage").val();
			if (requestPage != undefined) {
				checkoutSignInSubmit(requestPage);
			}
			else headerSignInSubmit();
		} else {
			$('#signInModal #signin-submit-btn').removeAttr('disabled');
			checkMaskPasswordError($('#hearderLoginOverlay'));
		}
		return false;
	});

	$(document).on('click', '.reset-password-flink', function () {
		$('#signInModal').modal('hide');
		//$('#forgotPasswordModel').modal('show');
	});


	$(document).on('submit', '#lightBoxSubmit', function (e) {
		$("#submitEmail").trigger('click');
		e.preventDefault();
	});

	if ( $( '#SignUpemail' ).length > 0 ) {
		$( '#signup-email-button' ).on( 'click', getEmail() );
	}

	function getEmail() {
		var email = '';
		if($( '#SignUpemail' ).length > 0){
			email = $( '#SignUpemail' ).val() || '';
			email = email.trim();
		}
			var signUpCustId = $( '#epsilonCust_ID' ).val(),
			signUpPartnerName = $( '#epsilonPartnerName' ).val(),
			url = '/myaccount/epslonEmailSignUp.jsp?email=' + email;
			omniEmailSignUP( email, signUpCustId, signUpPartnerName );

		$( '#epslonEmail' ).load( url );
	}

	$(document).on('click', '#emailSignUpModal #submitEmail', function (e) {
		var values = $('#emailPopupTxt').val().length;
		var error = $('#blankEmailId').val();
		var error1 = $('#invalidEmailId').val();
		$(".error-blanck, .error-invalid").remove();
		var email = $("#emailPopupTxt").val();
		var customerID = $('#customer_ID').val();
		var partnerName = $('#partner_Name').val();
		omniEmailSignUP(email, customerID, partnerName);
		var url = "/myaccount/epslonEmailSignUp.jsp?email=" + email;
		$('#epslonEmailPopup').load(url);
		if (values == 0) {
			//$('<p class="error-blanck">'+error+'</p>').insertAfter( "#emailPopupTxt" );
			$('<p class="error-blanck">' + error + '</p>').insertBefore(".txtemail");  /* defect 275 */
			$(document).find('#emailSignUpModal #emailPopupTxt').focus();
		} else if (!ValidateEmail($("#emailPopupTxt").val())) {
			$('.error-blanck').remove();
			//$('<p class="error-invalid">'+error1+'</p>').insertAfter( "#emailPopupTxt" );
			$('<p class="error-invalid">' + error1 + '</p>').insertBefore(".txtemail"); /* defect 275 */
			$(document).find('#emailSignUpModal #emailPopupTxt').focus();
		} else {
			if ($('#lightBoxSubmit').valid() && window.location.href.indexOf("babiesrus\.com") <= 0 ) {
				$.ajax({
					type: 'post',
					data: { formID: 'emailSubmit' },
					dataType: 'html',
					url: '/common/email.jsp',
					success: function (response) {
						console.log('Email submit called.');
						$('#emailSignUpModal .email-sign-up-form').hide();
						$('#emailSignUpModal .modal-content').animate({ height: 130 }, 500, function () {
							$('#emailSignUpModal header').html('Thank you for signing up!');
							$('#emailSignUpModal .email-sign-up-thanks').show();
						});

						$('#emailSignUpModal #close-email-popup').removeAttr('id');
					}
				});
			} else if (window.location.href.indexOf("babiesrus\.com") > 0) {
				var subDomain = window.location.href.split('.')[0].split('//')[1];
				$.ajax({
					type: 'post',
					data: { formID: 'emailSubmit' },
					dataType: 'html',
					url: 'https://' + subDomain + '.babiesrus.com/common/email.jsp',
					success: function (response) {
						console.log('Email submit called.');
						$('#emailSignUpModal .email-sign-up-form').hide();
						$('#emailSignUpModal .modal-content').animate({ height: 130 }, 500, function () {
							$('#emailSignUpModal header').html('Thank you for signing up!');
							$('#emailSignUpModal .email-sign-up-thanks').show();
						});

						$('#emailSignUpModal #close-email-popup').removeAttr('id');
					}
				});
			}
			return false;
		}
	});

	$(document).on('submit', '#emailSignUpFooter', function (e) {
		$("#submitEmailFooter").trigger('click');
		e.preventDefault();
	});

	$(document).on('click', '#emailSignUpFooter #submitEmailFooter', function (e) {
		var values = $('#emailPopupTxtFooter').val().length;
		var error = $('#blankEmailIdFooter').val();
		var error1 = $('#invalidEmailIdFooter').val();
		$(".error-blankFooter, .error-invalidFooter").remove();
		var email = $("#emailPopupTxtFooter").val();
		if (typeof utag != 'undefined') {
			utag.link({
				event_type: 'email_signup',
				customer_email: email
			});
		}
		var url = "/myaccount/epslonEmailSignUp.jsp?email=" + email;
		$('#epslonEmailPopup').load(url);
		if (values == 0) {
			$('<p class="error-blankFooter">' + error + '</p>').insertBefore("#emailPopupTxtFooter");
			$(document).find('#emailSignUpFooter #emailPopupTxtFooter').focus();
		} else if (!ValidateEmail($("#emailPopupTxtFooter").val())) {
			$('.error-blankFooter').remove();
			$('<p class="error-invalidFooter">' + error1 + '</p>').insertBefore("#emailPopupTxtFooter");
			$(document).find('#emailSignUpFooter #emailPopupTxtFooter').focus();
		} else {
			if ($('#emailSignUpFooter').valid() && window.location.href.indexOf("babiesrus\.com") <= 0 ) {
				$.ajax({
					type: 'post',
					data: { formID: 'emailSubmit' },
					dataType: 'html',
					url: '/common/email.jsp',
					success: function (response) {
						$('#emailSignUpFooter #emailPopupTxtFooter, #emailSignUpFooter #submitEmailFooter').hide();
						$('#emailSignUpFooter .email-sign-up-thanksFooter').show();
					}
				});
			} else if (window.location.href.indexOf("babiesrus\.com") > 0) {
				var subDomain = window.location.href.split('.')[0].split('//')[1];
				var epsilonURL = $("#epslonEmailURL").val();
				$.ajax({
					type: 'get',
					data: { formID: 'emailSubmit' },
					dataType: 'html',
					url: epsilonURL+'?EMAIL='+email+'&TRU_EMAIL_OPTIN=1&SOURCE=TRU_HEADER_POST&MESSAGEID=a9a674ed-6280-4745-8f09-76b8ed503c6c',
					success: function (response) {
						$('#emailSignUpFooter #emailPopupTxtFooter, #emailSignUpFooter #submitEmailFooter').hide();
						$('#emailSignUpFooter .email-sign-up-thanksFooter').show();
					}
				});
			}
			return false;
		}
	});

	$(document).on('click', '#emailSignUpModal #close-email-popup', function (e) {
		$.cookie("lightBoxCookieExpirationDate", getCurrentDateInDDMMYYYY());
		$.ajax({
			type: 'post',
			data: { formID: 'emailCancel' },
			dataType: 'html',
			url: '/common/email.jsp',
			success: function (response) {
				console.log('Close email called.');
			}
		});
	});

	$(document).on('click', '#emailSignUpModal #checkboxMsgId', function (e) {
		$(this).removeClass('checkbox-sm-off');
		$(this).addClass('checkbox-sm-on');
		$('#emailSignUpModal #close-email-popup').removeAttr('id');
		$.ajax({
			type: 'post',
			data: { formID: 'checkBox' },
			dataType: 'html',
			url: '/common/email.jsp',
			success: function (response) {
				//setTimeout(function(){
				console.log('Checkbox do not show called.');
				$('[data-dismiss="modal"]').click();
				//}, 50);
			}
		});
	});

	$(document).on('keyup', '#shippingZipCode', function (e) {
		var zip = $('#shippingZipCode').val();

		if (zip.length == 6 && zip.substr(zip.length - 1, 1) == '-') {
			$('#shippingZipCode').val(zip.substr(0, zip.length - 1));
		} else if (zip.length == 6) {
			var tmp = zip.substr(0, zip.length - 1) + '-' + zip.substr(zip.length - 1, 1);
			$('#shippingZipCode').val(tmp);
		}

	});

	$(document).on('keyup', '.zipcodeformat', function (e) {
		var zip = $('.zipcodeformat').val();

		if (zip.length == 6 && zip.substr(zip.length - 1, 1) == '-') {
			$('.zipcodeformat').val(zip.substr(0, zip.length - 1));
		} else if (zip.length == 6) {
			var tmp = zip.substr(0, zip.length - 1) + '-' + zip.substr(zip.length - 1, 1);
			$('.zipcodeformat').val(tmp);
		}

	});
	/* $(document).on('paste', '.splCharCheck', function(e) {
			e.preventDefault();
	 });*/

	$(document).on('click', '#membershipNum', function () {
		var thisPosition = $(this).position();
		$('.membership-number-group .popover').toggle().css({ 'top': thisPosition.top + 17, 'left': thisPosition.left - 105 });
		$('.errorDisplay').remove();
		$('input').removeClass('error-highlight');
	});

	$(document).on('paste', '.chkNumbersOnly,.splCharCheck', function (e) {
		var clipboardData = e.clipboardData || window.clipboardData;
		// var txt = clipboardData.getData('Text');
		if (clipboardData && clipboardData.getData && clipboardData.getData('Text')) {
			var txt = clipboardData.getData('Text')
		}
		else {
			var txt = e.originalEvent.clipboardData.getData('text');
		}
		if (isNaN(txt)) {
			e.preventDefault();
		}
	});
	$(window).scroll(function () {
		if ($(this).scrollTop() > 200) {
			$("#scrollToTop").show();
		}
		else {
			$("#scrollToTop").hide();
		}
	});
	$(document).on('click', '#scrollToTop', function () {
		$("html, body").animate({ scrollTop: 0 }, "slow");
	});
	$(document).on('click', '#quickviewModal .add-to-cart-section', function (e) {
		if (!$(this).find('button').is(':disabled')) {
			$('#quickviewModal').modal("hide");
		}
	});


	/* Detail Tool Tip*/
	var pdppagedetailTooltip = $('.details-tooltip');

	var pdpDetailsPopoverTemplate = '<div class="popover detailWrapper paypalDdetailsWrapper" role="tooltip"><div class="arrow"></div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div>';

	pdppagedetailTooltip.click(function (event) {
		event.preventDefault();
	});

	pdppagedetailTooltip.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: pdpDetailsPopoverTemplate,
		trigger: 'click'
	});
	/* LearnMore Tool Tip*/
	var pdppageLearnmoreTooltip = $('.learn-more-tooltiplink');

	var pdpLearnmorePopoverTemplate = '<div class="popover detailWrapper paypalDdetailsWrapper" role="tooltip"><div class="arrow"></div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div>';

	pdppageLearnmoreTooltip.click(function (event) {
		event.preventDefault();
	});

	pdppageLearnmoreTooltip.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: pdpLearnmorePopoverTemplate,
		trigger: 'click'
	});

	/* SeeTerm Tool Tip*/
	var pdppageSeetermTooltip = $('.see-termlink');

	var pdpSeetermPopoverTemplate = '<div class="popover detailWrapper paypalDdetailsWrapper" role="tooltip"><div class="arrow"></div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div>';

	pdppageSeetermTooltip.click(function (event) {
		event.preventDefault();
	});

	pdppageSeetermTooltip.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: pdpSeetermPopoverTemplate,
		trigger: 'click'
	});
	/* Estimated Detail Tool Tip*/
	var pdppageEstimatedDetailTooltip = $('.estimated-details-tooltip');

	var pdpEstimatedDetailPopoverTemplate = '<div class="popover detailWrapper paypalDdetailsWrapper" role="tooltip"><div class="arrow"></div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div>';

	pdppageEstimatedDetailTooltip.click(function (event) {
		event.preventDefault();
	});

	pdppageEstimatedDetailTooltip.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: pdpEstimatedDetailPopoverTemplate,
		trigger: 'click'
	});



	$(document).on('keyup', '.card-number', function () {
		var cardType = '';
		var value = $(this).val();
		var imageContextPath = window.TRUImagePath + 'images/';
		var img = [imageContextPath + 'Payment_Small-Visa-Thumb-No-Outline.jpg', imageContextPath + 'Payment_Small-Mastercard-Thumb-No-Outline.jpg', imageContextPath + 'Payment_Small-Amex-Thumb-No-Outline.jpg', imageContextPath + 'Payment_Small-Discover-Thumb.jpg', imageContextPath + 'Payment_Small-R-US-card-Thumb-1.jpg', imageContextPath + 'Payment_Small-R-US-card-Thumb-2.jpg'];
		if (value.length > 5) {
			var v = parseInt(value.substr(0, 6));

			if ((v >= 400000 && v <= 499999) && (value.length > 5 || value.length == 13 || value.length == 16)) { /*visa card*/
				$('.display-card .credit-card').attr('src', img[0]);
				$(".display-card").show();
				$(this).attr('maxlength', '16');
				cardType = 'visa';
			} else if ((((v >= 510000 && v <= 523769) || (v >= 523771 && v <= 524362) || (v >= 524364 && v <= 559999)) && (value.length > 5 || value.length == 16))) {
				$('.display-card .credit-card').attr('src', img[1]);
				$(".display-card").show();
				$(this).attr('maxlength', '16');
				cardType = 'masterCard';
			} else if ((v >= 360000 && v <= 369999) && (value.length > 5 || value.length == 14)) {
				$('.display-card .credit-card').attr('src', img[3]);
				$(".display-card").show();
				$(this).attr('maxlength', '14');
				cardType = 'discover';
			} else if (((v >= 370000 && v <= 379999) || (v >= 340000 && v <= 349999)) && (value.length > 5 || value.length == 15)) { /*Amex card*/
				$('.display-card .credit-card').attr('src', img[2]);
				$(".display-card").show();
				$(this).attr('maxlength', '15');
				cardType = 'americanExpress';
			} else if ((v >= 601100 && v <= 601199 || v >= 622126 && v <= 622925 || v >= 628200 && v <= 628899 || v >= 644000 && v <= 659999 || v >= 300000 && v <= 305999 || v >= 309500 && v <= 309599 || v >= 352800 && v <= 358999 || v >= 380000 && v <= 399999 || v >= 624000 && v <= 625999) && (value.length > 5 || value.length == 16)) { /*discover card*/
				if ((v == 601104 || (v >= 601110 && v < 601120) || (v >= 601150 && v < 601174) || (v >= 601175 && v < 601177) || (v >= 601180 && v < 601186)) && (value.length > 5 || value.length == 16)) {
					$(".display-card").hide();
					$(".display-card img.credit-card").attr('src', '');
					cardType = 'invalid';
				} else {
					$('.display-card .credit-card').attr('src', img[3]);
					$(".display-card").show();
					$(this).attr('maxlength', '16');
					cardType = 'discover';
				}
			} else if (v == 604586 && (value.length > 5 || value.length == 16)) { /*PLCC card*/
				$('.display-card .credit-card').attr('src', img[4]);
				$(".display-card").show();
				$(this).attr('maxlength', '16');
				cardType = 'RUSPrivateLabelCard';
			} else if (v == 524363 && (value.length > 5 || value.length == 16)) { /*USCOBMasterCard card*/
				$('.display-card .credit-card').attr('src', img[5]);
				$(".display-card").show();
				$(this).attr('maxlength', '16');
				cardType = 'RUSCoBrandedMasterCard';
			} else if (v == 523770 && (value.length > 5 || value.length == 16)) { /*PRCOBMasterCard card*/
				$('.display-card .credit-card').attr('src', img[5]);
				$(".display-card").show();
				$(this).attr('maxlength', '16');
				cardType = 'RUSCoBrandedMasterCard';
			} else {
				$(".display-card").hide();
				$(".display-card img.credit-card").attr('src', '');
				$(this).removeAttr('maxlength');
				cardType = 'invalid';
			}
		} else {
			$(".display-card").hide();
			$(".display-card img.credit-card").attr('src', '');
			$(this).removeAttr('maxlength');
			cardType = 'invalid';
		}

		cardTypeForCvvValidation = cardType;
		enableDisableExpiryDate(cardType, this);
		checkForFinancing();
	});
	/* End: credit card number functionality */

	$(document).on('click', '.creditCardModalClass', function () {

		var scrollableHelp = $('.credit-card-help-desk-overlay').find('.tse-scrollable');

		var recalculateHelpDesk = function () {
			setTimeout(function () {
				scrollableHelp.TrackpadScrollEmulator('recalculate');
				scrollableHelp.find('.tse-scroll-content').height(580);
				scrollableHelp.find('.tse-scroll-content').width(848);
			}, 500);
		};

		scrollableHelp.TrackpadScrollEmulator({
			wrapContent: false,
			autoHide: false
		});

		recalculateHelpDesk();

		return true;
	});

	/*Scroll for what's this link in CVV- Start*/
	$(document).on('click', '[data-target="#creditCardModal"]', function () {
		//setTimeout(function(){
		$('#creditCardModal .tse-scrollable').TrackpadScrollEmulator({
			wrapContent: false,
			autoHide: false
		});
		//},700);
	});
	/*Scroll for what's this link in CVV- End*/

	//$('.truLoginPage').height( Math.max( $('#truSignIn').height(), $('#truSignUp').height(), $('#truOrderStatus').height() ) - 4);
	$('.truLoginPage').css('min-height', Math.max($('#truSignIn').height(), $('#truSignUp').height(), $('#truOrderStatus').height()) - 4);

	///////////// not required so change click event by anurag for knockout implementation start
	$(document).on(
		'click_1',
		'.loadMoreProducts',
		function () {
			$(".checkCondition").remove();
			var count = parseInt($("#counts").val());
			var contextPath = $(".contextPath").val();
			var No = $("#paginationValue").val();
			var Nrpp = $("#Nrpp").val();
			var addNrpp = parseInt($("#Nrpp").val());
			var checkCondition = $(
				"#checkCondition").val();
			var totalNumRecs = parseInt($(
				"#totalNumRecs").val());
			var N = $("#N").val();
			var keyword = $("#keyword").val();
			var Ns = $("#Nsval").val();



			var load = "";
			var restToLoad = totalNumRecs - count;
			if (restToLoad < Nrpp) {
				$(".ldMore").html(restToLoad);
			} else {
				$(".ldMore").html(Nrpp);
			}
			var loadmore = parseInt(count) - Nrpp;

			if (count >= totalNumRecs) {

				var shownrec = parseInt(count)
					- Nrpp;
				Nrpp = totalNumRecs - shownrec;
				load = Nrpp;
				checkCondition = false;
				$(".loadMoreProducts").hide();
				/*$(".ldMore").html(3);*/



				count = totalNumRecs;
			}
			$(".loaded").html(count);
			var queryParam = "No=" + No + "&Nrpp="
				+ Nrpp;
			if (N !== "") {
				queryParam = queryParam + "&N=" + N;
			}
			if (keyword !== "") {
				queryParam = queryParam + "&keyword="
					+ keyword;
			}
			if (Ns !== "") {
				queryParam = queryParam + "&Ns="
					+ Ns;
			}
			var url = contextPath
				+ "browse/ajaxLoadmore.jsp?"
				+ queryParam;


			$
				.ajax({
					type: 'post',
					dataType: 'html',
					url: url
					+ '&isFirstLoad=No',
					success: function (response) {
						var count = parseInt($(
							"#counts")
							.val())
							+ parseInt($(
								"#Nrpp")
								.val());
						var paginationValue = parseInt($(
							"#paginationValue")
							.val()) + addNrpp;

						$("#paginationValue")
							.val(
							paginationValue);
						$("#counts").val(count);
						$("#load").val(load);
						// $('#filtered-products .row').append(response);
						$(
							'#filtered-products .afterHookProduct')
							.append(
							response);

					}
				});
		});


	$(document).on('click', '.gift-finder-shopping-for-gender label', function () {
		var giftFinderName = $('#giftFinderName').val();
		var gender = $(this).attr('for');
		getGiftFinderResult(giftFinderName, gender);
	});

	$('body').on('click', '.gift-interest-selection', function () {
		var giftFinderName = $('#giftFinderName').val();
		var gender = $(this).attr('for');
		getGiftFinderResult(giftFinderName, gender);
	});

	/* $(document).on('mouseup', '#age-slider .ui-slider-handle', function(){
		   alert(1);
	}); */
	passwordMeter();

	//Commented becausen its hiding even there is an image ad
	setInterval(function () { hideAd(); }, 500);
	function hideAd() {

		$(".ad-zone-970-90, .adZoneContainer").each(function () {
			var content = $.trim($(this).find("[id^=OAS]").html());
			var isSOS = $.cookie("isSOS");
			if (content == "") {
				$(this).hide();
				$(this).parents('.adZoneContainer').hide();
				if ($(this).parents('.category-adzone-container').length && !($(this).parents('.category-adzone-container .seo-header').length > 0)) {
					$(this).parents('.category-adzone-container').find("hr").hide()
				} else {
					$(this).parents(".sosAddZone").hide();
					/*if($(this).parents('.category-adzone-container .seo-header').length > 0){
						$(this).parents('.category-adzone-container').find("hr").show();
					}*/
				}
			} else {
				$(this).show();
				$(this).parents('.adZoneContainer').show();
				if ($(this).parents('.category-adzone-container').length || $(this).parents('.category-adzone-container .seo-header').length) {
					$(this).parents('.category-adzone-container').find("hr").hide()
				} else {
					if (typeof isSOS != 'undefined' && isSOS != null && isSOS == 'true') {
						$(this).parents(".sosAddZone").hide();
					} else {
						$(this).parents(".sosAddZone").show();
					}

				}
			}
		});

		$(".ad-zone-728-90, .partial-ad-zone-728-90").each(function () {
			var content1 = $.trim($(this).find("[id^=OAS]").html());
			if (content1 == "") {
				$(this).hide();
				$(this).parents('.partial-ad-zone-728-90').hide();
			} else {
				$(this).show();
				$(this).parents('.partial-ad-zone-728-90').show();
			}
		});

		$(".ad-zone-777-34, .partial-ad-zone-777-34").each(function () {
			var content2 = $.trim($(this).find("[id^=OAS]").html());
			if (content2 == "") {
				$(this).hide();
				$(this).parents('.partial-ad-zone-777-34').hide();
			} else {
				$(this).show();
				$(this).parents('.partial-ad-zone-777-34').show();
			}
		});

		/*   $(".ad-zone-728-90, .shipping-addzone").each(function() {
			   var content3 = $.trim($(this).find("[id^=OAS]").html());
				if (content3 == "") {
					 $(this).hide();
					 $(this).parents('.shipping-addzone').hide();
			   } else {
					 $(this).show();
					 $(this).parents('.shipping-addzone').show();
				   }
				}); */
	}

	/*close popup for checkout and cart*/
	$(document).on('click', '.close_edit', function (e) {
		$(this).parents(".shopping-cart-edit-product-content").hide();
		return false;
	});

	var maximumValue = 5;
	var minValue = 1;
	$(document).on('click', '.stepper > .increase.cart', function () {
		if ($(this).hasClass('disabled')) return false;
		var thisStepper = $(this).parent();
		var thisCounter = thisStepper.find('.counter');
		var thisQtyLimit = thisStepper.attr('quantityLimitVal');
		var thisRelItemQtyInCart = thisStepper.attr('relItemQtyInCart');
		maximumValue = thisQtyLimit;
		var value = parseInt(thisCounter.val()) + 1;
		if (isNaN(value))
			value = 1;
		else if (value > maximumValue && maximumValue > 0)
			value = maximumValue;
		else if (value < minValue)
			value = minValue;

		thisCounter.val(value);
		setButtonState(thisCounter);
		var fromOverlay;
		if (thisStepper.hasClass('overlay')) {
			fromOverlay = 'addToCartOverlay';
		}
		if (!thisStepper.hasClass('editCart')) {
			updateCartItemQuantity(thisStepper.attr("relationshipItemId"), parseInt(thisRelItemQtyInCart) + 1, fromOverlay, thisStepper.attr('giftWrapSkuId'));
		} else {
			$('.lineItemQtyEditCart').val(value);
			if (value >= maximumValue) {
				$(".qtys-text").show();
			}
			$(".qtys-valid-text").hide();
		}
	});

	$(document).on('click', '.stepper > .decrease.cart', function () {
		if ($(this).hasClass('disabled')) return false;
		var thisStepper = $(this).parent();
		var thisCounter = thisStepper.find('.counter');
		var thisQtyLimit = thisStepper.attr('quantityLimitVal');
		var thisRelItemQtyInCart = thisStepper.attr('relItemQtyInCart');
		maximumValue = thisQtyLimit;
		var value = parseInt(thisCounter.val()) - 1;
		if (isNaN(value))
			value = 1;
		else if (value > maximumValue && maximumValue > 0)
			value = maximumValue;
		else if (value < minValue)
			value = minValue;

		thisCounter.val(value);
		setButtonState(thisCounter);
		var fromOverlay;
		if (thisStepper.hasClass('overlay')) {
			fromOverlay = 'addToCartOverlay';
		}
		if (!thisStepper.hasClass('editCart')) {
			updateCartItemQuantity(thisStepper.attr("relationshipItemId"), parseInt(thisRelItemQtyInCart) - 1, fromOverlay, thisStepper.attr('giftWrapSkuId'));
		} else {
			$('.lineItemQtyEditCart').val(value);
			$(".qtys-text").hide();
			$(".qtys-valid-text").hide();
		}
	});
	$(document).on('keyup', '.lineItemQtyEditCart', function () {
		var thisStepper = $(this).parent();
		var thisCounter = thisStepper.find('.counter');
		var thisQtyLimit = thisStepper.attr('quantityLimitVal');
		var relationshipItemId = thisStepper.attr('relationshipItemId');
		maximumValue = thisQtyLimit;
		var str = $(this).val();
		if (str == 0 && str != '') {
			$(this).val('1');
			return false;
		}
		setButtonState(thisCounter);
		if (parseInt(str) >= parseInt(maximumValue)) {
			$(".qtys-text").show();
		} else {
			$(".qtys-text").hide();
			$(".qtys-valid-text").hide();
		}
	});
	function setButtonState(counter) {
		var value = parseInt(counter.val());
		var decrease = counter.parent().find('.decrease');
		var increase = counter.parent().find('.increase');
		if (value <= minValue) {
			decrease.removeClass('enabled');
			decrease.addClass('disabled');
		}
		else {
			decrease.removeClass('disabled');
			decrease.addClass('enabled');
		}
		if (value >= maximumValue) {
			increase.removeClass('enabled');
			increase.addClass('disabled');
		}
		else {
			increase.removeClass('disabled');
			increase.addClass('enabled');
		}
	}

	/*close popup for checkout and cart*/

	$(document).on('click', '[data-target="#printTemplate"]', function () {
		//var printScroll = $('#printTemplate .tse-scrollable');
		setTimeout(function () {
			$("#printTemplate .confirmation-seeTerm,#printTemplate .pickup-step a").popover('destroy');
			$('#printTemplate .tse-scrollable').TrackpadScrollEmulator({
				wrapContent: false,
				autoHide: false
			});
		}, 700);
	});

	// On to go

	$('body').on('change', '#giftNameOption', function () {
		//alert($(this).val());
		window.location.href = $(this).val();
	});
	var contextPath = $(".contextPath").val();

	// Start - For browser back button functionality in order confirmation
	if (localStorage) {
		if ($('#nongoodbye_order_confirmation').length > 0) {
			localStorage.setItem('isFromOrderConfimationPage', 'true');
		}
		else if ($("#checkout-container").length > 0) {
			var isFromOrderConfimationPage = localStorage.getItem('isFromOrderConfimationPage');
			if (isFromOrderConfimationPage == 'true') {
				cancelExitBrowserConfimation();
				//window.location.href = contextPath+"/cart/shoppingCart.jsp";
			}
		}
		else {
			localStorage.setItem('isFromOrderConfimationPage', 'false');
		}
	} else {
		console.log('Error: Html5');
	}
	// END - For browser back button functionality in order confirmation

	/* Start: TUW-23455 */
	$(document).on('click', '[data-target="#creditCardModal"]', function () {
		//setTimeout(function(){
		$('#creditCardModal .tse-scrollable').TrackpadScrollEmulator({
			wrapContent: false,
			autoHide: false
		});
		//}, 600);
	});
	/* End: TUW-23455 */
	function setSiteNameCookie() {
		var siteParam = $('#currentSiteCookieParam').val() || '';
		var siteValue = $('#currentSiteCookieValue').val() || '';
		var currentDomain = '.toysrus.com';
		if (siteParam != '' && siteValue != '') {
			setCookieValue(siteParam, siteValue, currentDomain);
		}
	}
	function makeAjaxHeaderBru() {
		var domainName = $('.urlFullContextPath').val() || '';
		$.ajax({
			headers: {
				'X-APP-API_KEY': 'apiKey',
				'X-APP-API_CHANNEL': 'webstore'
			},
			url: domainName + 'rest/model/atg/commerce/ShoppingCartActor/accountCartSummary?pushSite=ToysRUs',
			type: 'GET',
			xhrFields: {
				'withCredentials': true
			},
			success: function (responseData) {
				jsonPopulateyAccountPopover(responseData);
			},
			error: function (errorData) {
				console.log("Bru ajax fail: " + errorData);
			}
		});
	}

	function jsonPopulateyAccountPopover(myData) {
		var htmlContent = '';
		if (myData.hasOwnProperty('profile')) {
			htmlContent = $('#jsonPopoverForLoggedInUser').tmpl(myData);
		} else {
			htmlContent = $('#jsonPopoverForGuestUser').tmpl(myData);
		}
		if (htmlContent.length > 0) {
			$('#my-account-popover-struct').html(htmlContent);
			$("#my-account-popover-struct").toggle();
		}
	}

	$(window).load(function () {
		countryChangeOverlay();
		loadEmailSignupPopupContent();
		setStoreAddressCookie();
		alignStoreLocatorPageWithHeader();
		setSiteNameCookie();

		try {
			var generatedPasswordId = $('#generatedPasswordId').val() || '';
			var linkExpiredId = $('#linkExpiredId').val() || '';
			if (generatedPasswordId == 'false' || linkExpiredId == 'true') {
				$('#reset-password-page input, #reset-password-page button').attr('disabled', 'disabled');
			}
		} catch (e) { }
		setTimeout(function () {
			hideFeedbackFloatForSOS();
		}, 5000);

		var promoResultDiv = $('.product-block-incentivePromo.promoResultDiv');
		if (typeof skuIdWithPromoMsg == 'object' && typeof skuIdWithPromoMsg.promotionDetails != 'undefined' && promoResultDiv.length > 0) {
			$.each(skuIdWithPromoMsg.promotionDetails, function (pKey, pVal) {
				var currentPromoMsg = pVal.promotions[0].promoName;
				$('.product-block-incentivePromo.promoResultDiv.sku-' + pVal.skuId).html(currentPromoMsg);
			});
		}
		if ($('#checkout-container').length == 0) {
			$.datetimepicker.setLocale('en');//set the locale
			$('#previewDate').datetimepicker({
				dayOfWeekStart: 1,
				lang: 'en',
				startDate: '27/09/2016'    //on clicking ,the calender opens with this date.
			});
		}

		$(document).on('click', '#my-account-popover', function () {
			var contextPath = $('.urlFullContextPath').val() || $(".contextPath").val();
			var visitorFirstNamecookie = $('#VisitorFirstNamecookieDIV').html()
			var param = {
				url: contextPath + 'header/headerMyAccountPopover.jsp',
				data: {
					formSerialize: 'contextPath=' + contextPath + '&visitorFirstNamecookie=' + visitorFirstNamecookie,
					formID: 'myAccountPopover'
				}
			};

			var curentDomainName = window.location.hostname;
			if (curentDomainName.indexOf('.babiesrus.com') > -1) {
				makeAjaxHeaderBru();
			} else {
				makeAjaxHeader(param);
			}

			bruCss();
		});

		$(document).on('click', '#headerLogoutLink, #logoutNotYouId', function () {
			var clikedID = $(this).attr('id') || '';
			doSignOut(clikedID);
			signOutForTealium();
		});
		estimatedTaxTooltip();
	});

	var loadEmailSignupPopupContent = function () {
		var signUpCookieDescriptionDate = $.cookie("lightBoxCookieExpirationDate");
		var isSubscriptionPopUpRequired = true;
		var isTransient = $.cookie("VisitorFirstName");

		if (isEmpty(isTransient)) {
			if (typeof signUpCookieDescriptionDate != 'undefined' && signUpCookieDescriptionDate != null) {
				var cookieSavedDate = signUpCookieDescriptionDate.split("/");
				var currentDate = getCurrentDateInDDMMYYYY().split("/");

				var cookieSavedDateAfterFormated = new Date(cookieSavedDate[2], cookieSavedDate[1] - 1, cookieSavedDate[0]);
				var currentDateAfterFormated = new Date(currentDate[2], currentDate[1] - 1, currentDate[0]);

				if (cookieSavedDateAfterFormated >= currentDateAfterFormated) {
					isSubscriptionPopUpRequired = false;
				}
			}
			var loadSignUpJson = {};
			loadSignUpJson["isSubscriptionPopUpRequired"] = isSubscriptionPopUpRequired;
			if ($('#emailSignUpScript').length > 0) {
				var loadSignUpTemp = $('#emailSignUpScript').tmpl(loadSignUpJson);
				$('#emailSignupPopupContainer').html(loadSignUpTemp);
			}

			if ($('.home-template').length && $('#emailSignUpModal').length) {
				$('#emailSignUpModal').modal('show');
			}
		}
	};
	/* BPP code changes - start */
	$(document).on('click', '.protection-plan .checkbox-sm-off', function (e) {
		var pageName = $("#pageName").val();
		if (typeof pageName == 'undefined' || pageName.trim() == '') {
			pageName = "shoppingCart";
		}

		var relId = $(this).attr('data-relId');
		var bppSkuId = $(this).attr('data-bppSkuId');
		var bppItemId;
		var bppItemCount = $(this).attr('data-bppItemCount');
		var dataRelIDoff = $(this).data('relid');

		if (!isEmpty(bppItemCount) && parseInt(bppItemCount) == 1) {
			bppItemId = $(this).attr('data-bppItemId');
			addUpdateBppItem(relId, '', bppSkuId, bppItemId, pageName);
		} else {
			$('.protection-plan .checkbox-sm-off[data-relid="' + dataRelIDoff + '"]').parents('.checkbox-group').find('select').prop('disabled', false);
		}
		$('.protection-plan .checkbox-sm-off[data-relid="' + dataRelIDoff + '"]').toggleClass('checkbox-sm-off checkbox-sm-on');
	});
	$(document).on('click', '.protection-plan .checkbox-sm-on', function (e) {
		var pageName = $("#pageName").val();
		if (typeof pageName == 'undefined' || pageName.trim() == '') {
			pageName = "shoppingCart";
		}
		var bppItemInfoId = $(this).attr('data-bppInfoId');
		var relId = $(this).attr('data-relId');
		var dataRelIDon = $(this).data('relid');

		if (!isEmpty(bppItemInfoId) || ($.trim(bppItemInfoId) == '' && pageName == 'store-pickup')) {
			removeBppItem(relId, bppItemInfoId, pageName);
			if (pageName == 'store-pickup') {
				$(e.delegateTarget).find('select').prop('disabled', 'disabled');
				$(e.delegateTarget).find('select').val("select").change();
			}
		} else {
			$('.protection-plan .checkbox-sm-on[data-relid="' + dataRelIDon + '"]').parents('.checkbox-group').find('select').prop('disabled', 'disabled');
		}
		$('.protection-plan .checkbox-sm-on[data-relid="' + dataRelIDon + '"]').toggleClass('checkbox-sm-on checkbox-sm-off');
	});
	$(document).on('change', '.protection-plan select', function (e) {
		var pageName = $("#pageName").val();
		if (typeof pageName == 'undefined' || pageName.trim() == '') {
			pageName = "shoppingCart";
		}

		/*TUW-56861*/
		if (pageName == 'store-pickup') {
			var bppItemInfoId = '';
		} else {
			var bppItemInfoId = $(this).attr('data-bppInfoId');
		}
		/*TUW-56861*/

		var relId = $(this).attr('data-relId');
		var bppItemId = $(this).val();
		var bppSkuId = $("#bppSKU_" + bppItemId).val();
		if ('select' == bppItemId) {
			return false;
		}
		if (isEmpty(bppItemInfoId)) {
			addUpdateBppItem(relId, '', bppSkuId, bppItemId, pageName);
		} else {
			addUpdateBppItem(relId, bppItemInfoId, bppSkuId, bppItemId, pageName);
		}
	});
	/* BPP code changes - end */

	/* BPP code changes in cart overlay - start */
	$(document).on('click', '.cart-overlay-protection-plan .compare-checkbox', function (e) {
		var relId = $(this).attr('data-relId');
		var bppItemId;
		if ($(this).is(":not(:checked)")) {
			var bppItemInfoId = $(this).attr('data-bppInfoId');
			if (!isEmpty(bppItemInfoId)) {
				removeBppItem(relId, bppItemInfoId, 'fromCartOverlay');
			} else {
				$(e.delegateTarget).find('select').prop('disabled', 'disabled');
			}
		} else if ($(this).is(":checked")) {
			var bppItemCount = $(this).attr('data-bppItemCount');
			if (!isEmpty(bppItemCount) && parseInt(bppItemCount) == 1) {
				bppItemId = $(this).attr('data-bppItemId');
				var bppSkuId = $(this).attr('data-bppSkuId');
				addUpdateBppItem(relId, '', bppSkuId, bppItemId, 'fromCartOverlay');
			} else {
				$(e.delegateTarget).find('select').prop('disabled', false);
			}
		}
	});

	$(document).on('change', '.cart-overlay-protection-plan select', function (e) {

		var bppItemInfoId = $(this).attr('data-bppInfoId');
		var relId = $(this).attr('data-relId');
		var bppItemId = $(this).val();
		var bppSkuId = $("#bppSKU_" + bppItemId).val();
		if ('select' == bppItemId) {
			return false;
		}
		if (isEmpty(bppItemInfoId)) {
			addUpdateBppItem(relId, '', bppSkuId, bppItemId, 'fromCartOverlay');
		} else {
			addUpdateBppItem(relId, bppItemInfoId, bppSkuId, bppItemId, 'fromCartOverlay');
		}
	});
	/* BPP code changes in cart overlay - end */

	//Welcome back popup

	//Fix for TUW-74324
	$("body").on("keyup", "#hearderLoginOverlay input[type=text]", function (e) {
		if (e.keyCode == 13) {
			$("#hearderLoginOverlay #signin-submit-btn").trigger("click")
		}
	})
});

/* BPP code changes - start */
function addUpdateBppItem(relId, bppItemInfoId, bppSkuId, bppItemId, pageName) {
	blockUI();
	$.ajax({
		type: "POST",
		url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?relationShipId=" + relId + "&bppItemInfoId=" + bppItemInfoId
		+ "&bppSkuId=" + bppSkuId + "&bppItemId=" + bppItemId + "&formID=addUpdateBppItem&fromCartOverlay=" + pageName,
		dataType: "html",
		success: function (data) {
			if (pageName == 'Shipping') {
				//continueToCheckout('shipping-tab');
				$.ajax({
					type: "POST",
					url: window.contextPath + "checkout/shipping/multiShipping.jsp",
					data: { pageName: 'Shipping' },
					dataType: "html",
					success: function (responseData) {
						$("#co-shipping-tab").html($(responseData).html());
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == '409') {
							cancelExitBrowserConfimation();
							var contextPath = $(".contextPath").val();
							location.href = contextPath + 'cart/shoppingCart.jsp';
						}
					}
				});
			} else if (pageName == 'store-pickup') {
				//continueToCheckout('pickup-tab');
				$.ajax({
					type: "POST",
					url: window.contextPath + "checkout/common/checkoutOrderSummary.jsp",
					data: { pageName: 'Store-Pickup' },
					dataType: "html",
					success: function (data3) {
						$("#order-summary-block").html($(data3).html());
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == '409') {
							cancelExitBrowserConfimation();
							var contextPath = $(".contextPath").val();
							location.href = contextPath + 'cart/shoppingCart.jsp';
						}
					}
				});
			} else if (pageName == 'Review') {
				//continueToCheckout('review-tab');
				$.ajax({
					type: "POST",
					url: window.contextPath + "checkout/review/orderReviewInclude.jsp",
					data: { pageName: 'Review' },
					dataType: "html",
					success: function (responseData) {
						$(".checkout-confirmation-content").html($(responseData).html());
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == '409') {
							cancelExitBrowserConfimation();
							var contextPath = $(".contextPath").val();
							location.href = contextPath + 'cart/shoppingCart.jsp';
						}
					}
				});
			} else {
				if (pageName === 'fromCartOverlay') {
					$("#shoppingCartModal .shopping-cart-overlay").html(data);
					progressBar();
					initCartScroll();
				} else {
					$("#shoppingCart").html(data);
				}
				bindCartEvents();
				$(window).scroll();
			}
			unBlockUI();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			unBlockUI();
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		},
		cache: false
	});
}

function removeBppItem(relId, bppItemInfoId, pageName) {
	blockUI();
	$.ajax({
		type: "POST",
		url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?formID=removeBPPItemInfo&relationShipId="
		+ relId + "&fromCartOverlay=" + pageName,
		dataType: "html",
		cache: false,
		success: function (data) {
			if (pageName == 'Shipping') {
				//continueToCheckout('shipping-tab');
				$.ajax({
					type: "POST",
					url: window.contextPath + "checkout/shipping/multiShipping.jsp",
					data: { pageName: 'Shipping' },
					dataType: "html",
					success: function (responseData) {
						$("#co-shipping-tab").html($(responseData).html());
					},
					error: function (e) {
						console.log(e);
					}
				});
			} else if (pageName == 'store-pickup') {
				/*continueToCheckout('pickup-tab');*/
				$.ajax({
					type: "POST",
					url: window.contextPath + "checkout/common/checkoutOrderSummary.jsp",
					data: { pageName: 'Store-Pickup' },
					dataType: "html",
					success: function (data3) {
						$("#order-summary-block").html($(data3).html());
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == '409') {
							cancelExitBrowserConfimation();
							var contextPath = $(".contextPath").val();
							location.href = contextPath + 'cart/shoppingCart.jsp';
						}
					}
				});
			} else if (pageName == 'Review') {
				//continueToCheckout('review-tab');
				$.ajax({
					type: "POST",
					url: window.contextPath + "checkout/review/orderReviewInclude.jsp",
					data: { pageName: 'Review' },
					dataType: "html",
					success: function (responseData) {
						$(".checkout-confirmation-content").html($(responseData).html());
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == '409') {
							cancelExitBrowserConfimation();
							var contextPath = $(".contextPath").val();
							location.href = contextPath + 'cart/shoppingCart.jsp';
						}
					}
				});
			} else {
				if (pageName === 'fromCartOverlay') {
					$("#shoppingCartModal .shopping-cart-overlay").html(data);
					progressBar();
					initCartScroll();
				} else {
					$("#shoppingCart").html(data);
				}
				bindCartEvents();
				$(window).scroll();
			}
			unBlockUI();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			unBlockUI();
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
}
/* BPP code changes - end */

$(window).load(function () {
	$('ul.all-products-sec > li.active:eq(0)').trigger('click');
});
var getParameterByName = function (name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
var getParameterByName = function (name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

/* start for loading mega menu overlay, signin popup and minicart content */
function megamenuOverlay() {
	var contextPath = $(".contextPath").val();
	$.ajax({
		type: "POST",
		dataType: "html",
		/*async:false,*/
		url: contextPath + "header/megamenu.jsp",
		success: function (data) {
			// var displayData=$(data).html()
			$('#exploreBox').replaceWith(data);
			bruCss();
		},
		error: function () {

		}
	});
}

function countryChangeOverlay() {
	var contextPath = $('.urlFullContextPath').val() || $(".contextPath").val() || '/';

	var urlPagename = location.pathname.substring(1);
	var urlAB = getParameterByName('ab');
	var urlCatID = getParameterByName('categoryid');
	var cat = getParameterByName('cat');
	var plpPathCheck = $("#plpPathCheck").val();


	var catdim = getParameterByName('catdim');
	if (location.host.indexOf('.babiesrus.com') == -1) {
		$.ajax({
			type: "POST",
			dataType: "html",
			/*async: false,*/
			data: { urlPagename: urlPagename, urlAB: urlAB, urlCatID: urlCatID, cat: cat, catdim: catdim },
			url: contextPath + "footer/i18N.jsp",
			success: function (data) {
				// var displayData=$(data).html()
				$('#internationalizastionModal .modal-header').append(data);


				if ((typeof urlPagename != 'undefined' && (urlPagename == "category" || urlPagename == "family" || urlPagename == "subcat")) && plpPathCheck == 'true') {
					//ajaxCallForPLPBreadcrumbsCookie();
				}

			},
			error: function () {
			}
		});

	}
}
function myAccountPopover() {
	var contextPath = $(".contextPath").val();



	$.ajax({
		type: "POST",
		dataType: "html",
		/*async: false,*/
		url: contextPath + "header/headerMyAccountPopover.jsp",
		success: function (data) {
			//var displayData=$(data).html()
			$('#my-account-popover-struct').html(data);
		},
		error: function () {

		}
	});
}
function miniCart() {
	var contextPath = $(".contextPath").val();
	$.ajax({
		type: "POST",
		dataType: "html",
		url: contextPath + "cart/mini_cart.jsp",
		success: function (data) {
			//var displayData=$(data).html()
			//$('#my-account-popover-struct').html(data);
		},
		error: function () {

		}
	});
}

/* end for loading mega menu overlay, signin popup and minicart content*/

function closeRemoveModal() {
	$('[data-dismiss="modal"]').click();
}

function categoryFilter(navigationId, taskName) {

	var contextPath = $(".contextPath").val();
	var defaultval = $("#defaultval").val();
	var NttVal = $("#keyword").val();
	var Nrpp = $("#Nrpp").val();



	var cURL = contextPath + "common/TRUAjaxOneColumnPage.jsp" + navigationId + ((typeof NttVal !== "undefined" && NttVal != "" && NttVal != 0) ? "" : "&defaultval=" + defaultval);

	$.ajax({
		type: 'post',
		dataType: 'html',
		//url: contextPath + "/common/TRUAjaxOneColumnPage.jsp" + navigationId + "&defaultval=" + defaultval ,
		url: cURL,
		success: function (response) {
			//$("#defaultN").val(defaultN);]
			response = '<div>' + response + '</div>';

			/*This is used to hide the check boxes on ajax calls except for clear all*/
			var tempDiv = $(document.createElement('div'));
			tempDiv.css("display", "none");
			tempDiv.html(response);
			if (taskName != 'clearAll') {
				tempDiv.find(".product-compare-checkbox-container").css("display", "none");
				$("body").find(".compare-product-selection").hide();
			} else {
				$(".compare-images").find(".image-1").removeClass("compare-image").addClass("no-compare-image");
			}
			$(window).scrollTop($(window).scrollTop() + 1);
			response = tempDiv.html();
			/*END OF This is used to hide the check boxes on ajax calls except for clear all*/

			if (taskName == 'filter' || taskName == 'clearAll'
				|| taskName == 'remove') {
				var numberOfProducts = $(response).find(".col-md-3.col-no-padding.product-block-padding");
				var redirectingUrl = $(numberOfProducts).find(".product-name a").attr("href");
				if (numberOfProducts.length == 1 && redirectingUrl) {
					window.location.href = redirectingUrl;
					return;
				}
				var narrowDropDown = $(response).find(".sub-cat-container")
					.html();
				var narrowDropDownMenu = $(response).find(
					".sub-cat-category-wrapper").html();
				var showingResults = $(response).find(".showing-results")
					.html();
				var filteredProducts = $(response).find("#filtered-products .row-footer-margin").html();
				var sortOptions = $(response).find(".text-right.col-narrow-by").html();
				var subcategoryoutput = $(response).find(".sub-cat-category-output").html();
				var showFacets = $(response).find('#showFacetsAccordion').val();
				var filterCount = $(response).find(".sub-cat-filter-list").length;

				var stateObj = {
					subCatContainerData: narrowDropDown,
					subcatCategorywrapper: narrowDropDownMenu,
					showingResults: showingResults,
					filteredProducts: filteredProducts,
					textRightNarrowBy: sortOptions,
					subcatOutput: subcategoryoutput,
					showFacets: showFacets,
					filterCount: filterCount
				};

				HistoryAPI.pushState(stateObj, $('html').find('title').text(), navigationId); //Pushing state to history


				$('#showFacetsAccordion').val(showFacets);
				$(".sub-cat-container").html(narrowDropDown);
				$(".sub-cat-category-wrapper").html(narrowDropDownMenu);

				$(".showing-results").html(showingResults);
				var txt = $('#showResults').val();
				$(".showing-results").html(txt);

				$("#filtered-products .row-footer-margin").html(
					filteredProducts);
				$(".text-right.col-narrow-by").html(sortOptions);

				$('.sub-cat-category-output').html(subcategoryoutput);

				refreshNarrowByThirdPartyInitializations();

				if (filterCount > 0)
					$(".clear-all-filters").removeClass('hide');
				// modified for B&S spark UI changes
				//Facet close if it is false
				/*if(showFacets == "false"){
					$('.compare-menu').offcanvas('hide');
					$("#sub-category-narrow-by-button").addClass('disableClickFunction');
				}else{
					$("#sub-category-narrow-by-button").removeClass('disableClickFunction');
				}*/

			} else if (taskName == 'sort') {

				var filteredProducts = $(response).find(
					"#filtered-products .row-footer-margin").html();
				$("#filtered-products .row-footer-margin").html(
					filteredProducts);
				$('.sort-by .sort-by-title')
					.text($('#selectedSortLabel').val());
				$('#filtered-products #product-disabled-modal').hide();
				/*var Nrppvalue=$(response).find("#Nrpp").val();
				$(".loaded").html(Nrppvalue);*/
				$(".showing-results").html($(response).find(".showing-results").html());
			}

		}
	});
}


//function to refreshNarrowByThirdPartyInitializations

function refreshNarrowByThirdPartyInitializations() {
	var options = {
		"icons": {
			"header": "ui-icon-plus",
			"activeHeader": "ui-icon-minus"
		},
		"collapsible": "true",
		"active": "false",
		"autoHeight": "true",
		"heightStyle": "content"
	};
	var animationDuration = 1000;
	// Build into sub-accordions so we can have multiple open.
	$('*[id=filter-accordion]').accordion(options);
	/*$('.filter-scroll-wrapper').TrackpadScrollEmulator({
		autoHide : false
	});*/

	$('*[id=filter-accordion]').accordion(
		{
			beforeActivate: function (event, ui) {
				var interval = setInterval(function () {
					$('.filter-scroll-wrapper')
						.TrackpadScrollEmulator(
						'recalculate');
				}, 20);
				setTimeout(function () {
					clearInterval(interval);
				}, animationDuration);
			}
		});
}


/* Start: Gift finder ajax */

function getGiftFinderResult(giftFinderName, gender) {
	var contextPath = $(".contextPath").val();
	//var giftFinderName = $(this).val();

	/* var gender = minAge = maxAge = minPrice = maxPrice = '';

	if($('.gift-finder-shopping-for-gender input:checked').length > 0 ){
		   gender = $('.gift-finder-shopping-for-gender input:checked').attr('id');
	} */

	var minAge = $('#hiddenMinAge').val();
	var maxAge = $('#hiddenMaxAge').val();

	var minPrice = $('#hiddenMinPrice').val();
	var maxPrice = $('#hiddenMaxPrice').val();
	var ajaxUrl = contextPath + "browse/ajaxLoadmore.jsp?";
	var urlBuilder = [], nValues = [];

	var priceage = '';

	if (maxPrice != "0" && maxAge != "0") {
		priceage = "Nf=sku.activePrice|BTWN+" + minPrice + "+" + maxPrice + "|minAge|BTWN+" + minAge + "+" + maxAge;

	}

	if (maxAge != "0") {
		if (minPrice == "0" && maxPrice == "0") {
			priceage = "Nf=minAge|BTWN+" + minAge + "+" + maxAge;

		}

	}

	if (maxPrice != "0") {
		if (minAge == "0" && maxAge == "0") {
			priceage = "Nf=sku.activePrice|BTWN+" + minPrice + "+" + maxPrice;

		}

	}

	if (gender) {
		urlBuilder.push("Nr=AND(product.gender:" + gender + ")");
	}
	if (priceage) {
		urlBuilder.push(priceage);
	}


	$('.gift-finder-options-container').find('input[type=checkbox]:checked').each(function () {
		nValues.push($(this).val());
	});

	if (nValues.length) {
		urlBuilder.push('N=' + nValues.join('+'));
	}
	if (urlBuilder.length <= 0) {
		$("#giftFinderTotal").html(0);
		return;
	}

	ajaxUrl += urlBuilder.join('&');
	$.ajax({
		type: "POST",
		dataType: "html",
		data: { giftFinderName: giftFinderName },
		url: ajaxUrl,
		success: function (responseData) {
			var finderValue = ($(responseData).find('#giftFinderTotal').text()) ? $(responseData).find('#giftFinderTotal').text() : 0;
			$('#gift-finder-menu-container #giftFinderTotal').text(finderValue);
			console.log('success: ' + responseData);
		},
		error: function (e) {
			console.log('Error: ' + e);
		}
	});
}

function passwordMeter() {
	"use strict";
	var options = {
		bootstrap3: true,
		onLoad: function () {
			$('#messages').text('Start typing password');
		},
		onKeyUp: function (evt) {
			$(evt.target).pwstrength("outputErrorList");
		}
	};
	$('.passwordInput').pwstrength(options);
	$('.passwordInput').bind("keyup", function (event) {
		if ($(this).val().length <= 0) {
			$(this).nextAll(".passMeter").hide();
			$(this).nextAll(".password-verdict").hide();
			return;
		}
		$('.progress,.passMeter').show();
		$(".passHint,.arrow-left").remove();
		$(this).nextAll(".password-verdict,.progress").show().wrapAll('<div class="passMeter"></div>');
		$(this).nextAll(".passMeter").append(" <div class='arrow-left'></div><p class='passHint'>To protect your account, password must be 8-50 charector in length and include atleast one uppper case letter, one lower case letter and one number</p>");
		var pass = $('.passMeter').clone();
		$('password-tools').append(pass);
		$('.passMeter div.progress').not("div.progress:eq(0)").hide();
		if ($(this).parents('div.reset-password-coloum').length > 0)
			$('.reset-password-coloum .password-tools').find('div.passMeter').css('right', '-287px');
	});
	$('.passwordInput,.confirm-password').blur(function () {
		$('.passMeter').hide();
	});

}

$(document).on("paste", '.passwordInput', function () {
	$(this).keyup();
});

function onCountryChange() {
	var selectCountry = $('#country').val();
	$('#otherState').removeClass('display-inlineblock').val('');
	$.ajax({
		url: '/myaccount/intermediate/ajaxIntermediateRequest.jsp?selectCountry=' + selectCountry + "&formID=changeCountry",
		dataType: 'html',
		success: function (data) {
			$('#myAccountAddCreditCardModal #state').html(data);
			if (selectCountry != 'US') {
				$('input[id="otherState"]').addClass('display-inlineblock');
				$("#otherState").val("NA");
				$(".creditCardOtherStateLabel").addClass('display-inlineblock');
			} else {
				$('input[id="otherState"]').removeClass('display-inlineblock');
				$(".creditCardOtherStateLabel").removeClass('display-inlineblock');
			}
		}
	});
	if (selectCountry == 'US') {
		$('#zip').mask('AAAAA-AAAA');
	}
	else {
		$('#zip').mask('AAAAAAAAAA');
		var zipVlaue = $('#zip').val();
		if (zipVlaue != '' && $('#zip').hasClass('error')) {
			$('#zip').removeClass('error').next('#zip-Validation-error').remove();
		}
	}
	universalFormValidationHandler();
}
function onCountryChangeInAddress() {
	var selectCountry = $('#editAddresscountry').val();
	$('#otherState').removeClass('display-inlineblock').val('');
	$('#changedCountry').val(selectCountry);
	$.ajax({
		url: '/myaccount/intermediate/ajaxIntermediateRequest.jsp?selectCountry=' + selectCountry + "&formID=changeCountry",
		dataType: 'html',
		success: function (data) {
			$('#myAccountAddAddressModal #state').html(data);
			$('#myAccountAddressForm #shippingZip').unmask();
			if (selectCountry != 'US') {
				$('input[id="otherState"]').addClass('display-inlineblock');
				$(".creditCardOtherStateLabel").addClass('display-inlineblock');
				$('#myAccountAddressForm #shippingZip').mask('AAAAAAAAA');
			} else {
				$('input[id="otherState"]').removeClass('display-inlineblock');
				$(".creditCardOtherStateLabel").removeClass('display-inlineblock');
				$('#myAccountAddressForm #shippingZip').mask('AAAAA-AAAA');
			}
		}
	});
}
function onShippingSelectedStateChange(ele) {
	var $this = $(ele);
	if ($this.val() == "other" && $('#editAddresscountry').val() != "US") {
		$this.parent().find("#otherState").addClass("display-inlineblock");
		$(".creditCardOtherStateLabel").addClass('display-inlineblock');
	} else {
		$this.parent().find("#otherState").removeClass("display-inlineblock");
		$(".creditCardOtherStateLabel").removeClass('display-inlineblock');
	}
}
function onCreditCardSelectedStateChange(ele) {
	var $this = $(ele);
	if ($this.val() == "other" && $('#country').val() != "US") {
		$this.parents(".select-wrapper").find("#otherState").addClass("display-inlineblock");
		$(".creditCardOtherStateLabel").addClass('display-inlineblock');
	} else {
		$this.parents(".select-wrapper").find("#otherState").removeClass("display-inlineblock");
		$(".creditCardOtherStateLabel").removeClass('display-inlineblock');
	}
}
function updateRewards() {
	$('#my-account-rewards-no-membership').show();
	$('#my-account-rewards-authenticated').hide();
}

function defaultAddress(defaultAddNickName) {
	var param = {
		url: '/myaccount/intermediate/ajaxIntermediateRequest.jsp',
		data: {
			formSerialize: 'defaultAddNickName=' + defaultAddNickName,
			formID: 'defaultAddress'
		}
	};
	makeAjax(param);
}

$(document).on("click", '.myAccountSetDefault', function () {
	var defaultAddNickName = $(this).parent().find('.nickNameInHiddenSpan').text();
	defaultAddress(defaultAddNickName);
});

function forgotPassowrd() {
	if ($("#forgotPassowrd").valid()) {
		$.ajax({
			type: "POST",
			data: $("#forgotPassowrd").serialize(),
			url: "/myaccount/intermediate/ajaxIntermediateRequest.jsp?formID=forgotPassowrd",
			success: function (data) {
				/* $(".forgotpasswordmodel-overlay").replaceWith(data);*/
				if (data.indexOf("Following are the form errors:") > -1) {
					/*addError('forgotPassowrd');*/
					$("#forgotPassowrd").find("#emailSentSuccess").hide();
					addBackendErrorsToForm(data, $("#forgotPassowrd"));
					$('.forgotpasswordmodel-overlay #passResetForm').focus();
					//addBackendErrorsToForm(data, $("#forgotPassowrd"))
				} else {
					$("#forgotPassowrd").find("#emailSentSuccess").html(data).show();
					$('.forgotpasswordmodel-overlay #passResetForm').focus();
					$('#loginEmail').val('');
				}
			},
			dataType: "html",
			cache: false
		});
	}
	return false;
}

function fpass() {
	//if($("#loginForgotPwdOverlay").valid()){
	$('#passResetForm').click();
	//}
	return false;
}

function suggestedAddressSelect() {
	var count = $('input[name="valid-address"]:checked').val();
	if (typeof (count) != "undefined") {
		$("#suggestedAddress1Id").val($("#suggestedAddress1-" + count).val());
		$("#suggestedAddress2Id").val($("#suggestedAddress2-" + count).val());
		$("#suggestedCityId").val($("#suggestedCity-" + count).val());
		$("#suggestedStateId").val($("#suggestedState-" + count).val());
		$("#suggestedCountryId").val($("#suggestedCountry-" + count).val());
		$("#suggestedPostalCodeId").val($("#suggestedPostalCode-" + count).val());
	}
}

function editUserEnteredAddress() {
	$("#suggestedAddress1Id").val($("#suggestedAddress1-0").val());
	$("#suggestedAddress2Id").val($("#suggestedAddress2-0").val());
	$("#suggestedCityId").val($("#suggestedCity-0").val());
	$("#suggestedStateId").val($("#suggestedState-0").val());
	$("#suggestedCountryId").val($("#suggestedCountry-0").val());
	$("#suggestedPostalCodeId").val($("#suggestedPostalCode-0").val());
	$("#addressValidated").val('false');

	var param = {
		url: '/myaccount/intermediate/addressAjaxResponse.jsp',
		data: {
			formSerialize: $("#myAccountAddressForm").serialize() + '&fromSuggestedAddress=true',
			formID: 'editUserEnteredAddress'
		}
	};
	makeAjax(param);
}

function editUserEnteredAddressForCreditCard() {
	$("#suggestedAddress1Id").val($("#suggestedAddress1-0").val());
	$("#suggestedAddress2Id").val($("#suggestedAddress2-0").val());
	$("#suggestedCityId").val($("#suggestedCity-0").val());
	$("#suggestedStateId").val($("#suggestedState-0").val());
	$("#suggestedCountryId").val($("#suggestedCountry-0").val());
	$("#suggestedPostalCodeId").val($("#suggestedPostalCode-0").val());
	$("#addressValidated").val('false');
	var billingAddressNickname = $('#billingAddressNickName1234').val();
	var editCardNickName = $('#editCardNickName1234').val();

	var param = {
		url: '/myaccount/intermediate/cardOverlayFragment.jsp',
		data: {
			formSerialize: $("#addOrEditCreditCard").serialize() + '&fromSuggestedAddress=true&billingAddressNickname=' + billingAddressNickname + '&editCardNickName=' + editCardNickName,
			formID: 'editUserEnteredAddressForCreditCard'
		}
	};
	makeAjax(param);
	$('#billing-address-container .billing-address').show();
	$(".card-number").trigger("keyup");
}

function modifyAddress() {
	if ($('#myAccountAddressForm').valid()) {
		$("#submitAddressId").click();
		suggestedAddressSelect();
	}
	return false;
}

function deleteAddress() {
	$("#removeShippingAddressId").click();
	return false;
}


function removeAddress(nickName) {
	$(".removeAddressHiddenId").val(nickName);
}
function removeAddressConfirm() {
	var name = $(".removeAddressHiddenId").val();
	var param = {
		url: '/myaccount/intermediate/ajaxIntermediateRequest.jsp',
		data: {
			formSerialize: 'name=' + name,
			formID: 'removeShippingAddress'
		}
	};
	makeAjax(param);
	return false;
}

var returningCustomersHeight = 0;
var createAccountWrapper = $(".create-account").height();
function onSubmitSigninForm() {
	var contextPath = $(".contextPath").val();
	var email = $('#email').val();
	var signinCustId = $('#signin-custId').val();
	var signinPartnerName = $('#signin-partnerName').val();


	$('.errorDisplay').remove();
	$.ajax({
		type: "POST",
		cache: false,
		url: contextPath + "myaccount/intermediate/ajaxIntermediateRequest.jsp?formID=signInForm",
		data: $("#signInForm").serialize(),
		success: function (data) {
			$('.errorDisplay').remove();
			if (data.indexOf('Following are the form errors:') > -1) {
				addBackendErrorsToForm(data, $("#signInForm"))
				omniLoginFail();
			} else {
				localStorage.setItem('justLoggedIn', 'yes');
				console.log('justLoggedIn from my account');
				location.href = contextPath + "myaccount/myAccount.jsp?isFromSignIn=true";
			}
		},
		dataType: "html"
	});

	return false;
}

function initAddressZipCodeMask() {
	if ($('.my-account-template').length > 0 || $('#checkout-container').length > 0) {
		//setTimeout(function(){
		var selectedCountry = $('#country').val() || '';
		var isAddressModalOpened = $('.my-account-template #myAccountAddAddressModal').hasClass('in');
		if ((!isAddressModalOpened && (($('#country').length > 0 && selectedCountry == 'US') || $('#country').length == 0)) || (isAddressModalOpened && $('#editAddresscountry').length == 0)) {
			$('#zip').mask('AAAAA-AAAA');
			$('#shippingZip').mask('AAAAA-AAAA');
		} else {
			$('#zip').mask('AAAAAAAAAA');
			$('#shippingZip').mask('AAAAAAAAAA');
		}
		//},1000);
	}
}

function addError(formId) {
	$('#' + formId + ' input, ' + '#' + formId + ' select').removeClass('error-highlight');
	$('#' + formId + ' input, ' + '#' + formId + ' select').filter(function () {
		return !this.value && !$(this).is(':disabled') && !$(this).hasClass('non-mandatory');
	}).addClass('error-highlight');


	switch (formId) {
		case 'addOrEditCreditCard':
			$('#' + formId + ' input[name=address2]').removeClass('error-highlight');
			if ($('#selectedCardBillingAddress option').last().prop('selected'))
				$('#selectedCardBillingAddress').removeClass('error-highlight');
			break;
		case 'myAccountAddressForm':
			$('#' + formId + ' input[name=address2]').removeClass('error-highlight');
			break;
		case 'myShippingAddressForm':
			$('#' + formId + ' input[name=address2]').removeClass('error-highlight');
			break;
		case 'saveShippingAddress':
			$('#' + formId + ' input[name=address2]').removeClass('error-highlight');
			break;
		case 'billingInfoApplicationForm': $('#' + formId + ' input[name=address2]').removeClass('error-highlight');
			break;

	}

}

var createAccountHeight = 0;
var returningCustomersWrapper = $(".returning-customers").height();

function onSubmitCreateAccountForm() {
	var contextPath = $(".contextPath").val();
	$('.errorDisplay').remove();
	$.ajax({
		type: "POST",
		cache: false,
		context: this,
		url: contextPath + "myaccount/intermediate/ajaxIntermediateRequest.jsp?formID=createAccountForm",
		data: $("#createAccountForm").serialize(),
		success: function (data) {
			$('.errorDisplay').remove();
			if (data.indexOf('Following are the form errors:') > -1) {
				addBackendErrorsToForm(data, $("#createAccountForm"));
			} else {
				location.href = contextPath + "myaccount/myAccount.jsp";
				//Added for omniture
				loadOmniRegScript();
			}
		},
		dataType: "html"
	});

	return false;
}

$("body").on('click', '.myAccountEditAddress', function () {
	var nickName = $(this).parent().find('.nickNameInHiddenSpan').text();
	var param = {
		url: '/myaccount/intermediate/addressAjaxResponse.jsp',
		data: {
			formSerialize: 'editNickName=' + nickName,
			formID: 'onEditAddressInOverlay'
		}
	};
	makeAjax(param);
	bruCss();
});

$(document).on('click', '.myAccountRemoveAddress', function () {
	var nickName = $(this).parent().find('.nickNameInHiddenSpan').text();
	removeAddress(nickName);
});

$(document).on('click', '.checkoutRemoveAddress', function () {
	var nickName = $(this).parent().find('.nickNameInHiddenSpan').text();
	removeAddress(nickName);
});

$("body").on("click", ".myaccountAddressModalRestrictor", function () {
	onAddAddressInOverlay();
});
function onAddAddressInOverlay() {
	var param = {
		url: '/myaccount/intermediate/addressAjaxResponse.jsp',
		data: { formID: 'onLoadAddressInOverlay' }
	};
	makeAjax(param);
	bruCss();
}

function addCreditCard() {
	suggestedAddressSelect();
	$("#submitCreditCardId").click();
	return false;
}
var aTokenNumber = '';
var acBinNum = '';
var acLength = '';
function onSubmitAddCreditCard() {

	var isLastOptionSelected = $('#selectedCardBillingAddress option').last().prop('selected');
	var isFromSuggesstion = ($('#isFromAddressSuggesstionOverlay').length > 0) ? ($('#isFromAddressSuggesstionOverlay').val()) : '';
	aTokenNumber = $('#cardinalToken').val() || $('#creditCardNumber').val() || aTokenNumber;
	acBinNum = $('#caBinNum').val() || acBinNum;
	acLength = $('#caLength').val() || acLength;

	if (aTokenNumber == '') {
		aTokenNumber = $('#creditCardNumber').val();
	}
	var bilingAddressFieldVisible = ($('#billing-address-container .billing-address.pull-up').css('display') == 'block' || isFromSuggesstion == 'true') ? 'true' : 'false';

	var param = {
		url: '/myaccount/intermediate/addCardAjaxSubmit.jsp',
		data: {
			formSerialize: $("#addOrEditCreditCard").serialize() + '&lastOptionSelected=' + isLastOptionSelected + '&bilingAddressFieldVisible=' + bilingAddressFieldVisible + '&token=' + aTokenNumber + '&cBinNum=' + acBinNum + '&cLength=' + acLength,
			formID: 'onAddCreditCard',
			isFromSuggesstion: isFromSuggesstion
		}
	};
	makeAjax(param);
	return false;
}

function removeRewardsCardConfirm() {
	$("#removeMembershipIdPopup").click();
	return false;
}

function removeRewardsCard() {

	var param = {
		url: '/myaccount/intermediate/ajaxIntermediateRequest.jsp',
		data: {
			formSerialize: $("#confirmRemoveMembershipForm").serialize(),
			formID: 'confirmRemoveMembershipForm'
		}
	};
	makeAjax(param);
	return false;
}

$(document).on("submit", "#membershipForm", function () {
	if ($("#membershipForm").valid()) {
		updateRewardsCard();
	}
	return false;
});
function updateRewardsCard() {
	/*if($("#membershipForm").valid()){*/
	var rewardsCard = $("#enterMembershipID").val();
	var flag = validateDenaliAccountNum(rewardsCard);
	var param = {
		url: '/myaccount/intermediate/ajaxIntermediateRequest.jsp',
		data: {
			formSerialize: 'rewardsCard=' + rewardsCard + '&flag=' + flag,
			formID: 'membershipForm'
		}
	};
	makeAjax(param);
	/*}*/
	return false;
}

function defaultCard(creditCardNickName, id) {
	var param = {
		url: '/myaccount/intermediate/ajaxIntermediateRequest.jsp',
		data: {
			formSerialize: 'defaultCardNickName=' + creditCardNickName + '&id=' + id,
			formID: 'defaultCreditCard',
			id: id
		}
	};
	makeAjax(param);
	bruCss();
}

function removeCardConfirm(nickName) {
	$("#removeCardHiddenId").val(nickName);
	return false;
}

function removeCard() {

	var nickName = $("#removeCardHiddenId").val();

	var param = {
		url: '/myaccount/intermediate/ajaxIntermediateRequest.jsp',
		data: {
			formSerialize: 'nickName=' + nickName,
			formID: 'removeCard'
		}
	};
	makeAjax(param);
	return false;
}

function deleteCard() {
	$("#removeCardId").click();
	return false;
}


function removeCardConfirmLanding(nickName) {
	$("#removeCardHiddenLandingId").val(nickName);
	return false;
}


function deleteCardLanding() {
	$("#removeCardLandingId").click();
	return false;
}

function removeCardLanding() {
	var nickName = $("#removeCardHiddenLandingId").val();
	var param = {
		url: '/myaccount/intermediate/ajaxIntermediateRequest.jsp',
		data: {
			formSerialize: 'nickName=' + nickName,
			formID: 'removeCardLanding'
		}
	};
	makeAjax(param);
	return false;
}

function headerSignInSubmit() {
	$('.errorDisplay').remove();
	var contextPath = $(".contextPath").val();
	var email = $('#login').val();
	var modalCustId = $('#modal-custId').val();
	var modalPartnerName = $('#modal-partnerName').val();
	var currentUri = window.location.href;
	currentUri = currentUri.split('#')[0];

	$.ajax({
		type: "POST",
		cache: false,
		/*async:false,*/
		dataType: "html",
		url: contextPath + "myaccount/intermediate/ajaxIntermediateRequest.jsp?formID=hearderLoginOverlay",
		data: $("#hearderLoginOverlay").serialize(),
		success: function (data) {
			if (data.indexOf('Following are the form errors:') > -1) {
				/* var msg = data.split('Following are the form errors:')[1];
				 $('body').find('.errorDisplay').remove();
				 $('#signInModal .sign-in-header').after('<span class="errorDisplay">'+msg+'</span>');
				 addError('hearderLoginOverlay'); */
				$('#signInModal #signin-submit-btn').removeAttr('disabled');
				addBackendErrorsToForm(data, $('#hearderLoginOverlay'));
				omniLoginFail();
			} else {
				localStorage.setItem('justLoggedIn', 'yes');
				console.log('justLoggedIn from header');
				omniLoginScript(email, modalCustId, modalPartnerName);
				window.location.href = currentUri;
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'myaccount/myAccount.jsp?sessionExpired=true';
			}
		}
	});

	return false;
}


function onEditCreditCardOverlay(creditCardNickName) {
	var param = {
		url: '/myaccount/intermediate/cardOverlayFragment.jsp',
		data: {
			formSerialize: 'creditCardNickName=' + creditCardNickName,
			formID: 'onEditCreditCardOverlay'
		},
	};
	makeAjax(param);
	$('#isEditModeOnChange').val('true');
	bruCss();
}

$("body").on('click', ".myaccountAddEditCardModalRestrictor", function () {
	onAddCreditCardOverlay();
});
function onAddCreditCardOverlay() {
	var param = {
		url: '/myaccount/intermediate/cardOverlayFragment.jsp',
		data: { formID: 'onLoadCreditCardOverlay' },
	};
	makeAjax(param);
	$('#isEditModeOnChange').val('false');
	bruCss();
	//startSetup();
}

function AddUpdateRewardsFromHeader() {
	removeBackendErrorMessagesInline($("#headerMembershipForm"));
	if ($("#headerMembershipForm").valid()) {
		var rewardsCard = $("#enterMemberIDFromHeader").val();
		var flag = validateDenaliAccountNum(rewardsCard);

		var param = {
			url: '/myaccount/intermediate/ajaxIntermediateRequest.jsp',
			data: {
				formSerialize: 'rewardsCard=' + rewardsCard + '&flag=' + flag,
				formID: 'membershipFormFromHeader'
			},
		};
		$('.errorDisplay').remove();
		$(document).find('.success-adjustment').removeClass('success-adjustment');
		$(document).find('.successMessage').remove();

		var reqestData = (typeof (param.data.formSerialize) != 'undefined') ? (param.data.formSerialize + '&formID=' + param.data.formID) : ({ formID: param.data.formID });

		$.ajax({
			type: "POST",
			dataType: "html",
			async: false,
			cache: false,
			data: reqestData,
			url: param.url,
			success: function (data) {
				if (data.indexOf('Following are the form errors:') > -1) {
					setTimeout(function () {
						addBackendErrorsToForm(data, $("#headerMembershipForm"));
					}, 150);
				} else {
					$('.my-account-popover-struct .my-account-logged-in').find('#membership-number').remove();
					$('.my-account-popover-struct .my-account-logged-in').find('#membership-id').remove();
					$('.my-account-popover-struct .my-account-logged-in').find('hr').after(data);
					$('#myAccountRewardsSecId').load('/myaccount/myAccountRewardsRus.jsp');
					bruCss();
				}
			}
		});



	}

	return false;
}
function updateRewardsLink() {
	$('#membership-number').hide();
	$('#membership-id').show();
	$('.header-rewards-zone #membership-id-slide').click();
	$('#headerMembershipForm #enterMemberIDFromHeader').focus();
}


function reInitScrollBar(formId, height) {
	var scrollable = '';
	if (formId == 'onEditCreditCardOverlay') {
		scrollable = $('.my-account-add-credit-card-overlay').find('.tse-scrollable');
	} else {
		scrollable = $('.my-account-add-address-overlay').find('.tse-scrollable');
	}
	var recalculate = function () {
		setTimeout(function () {
			scrollable.TrackpadScrollEmulator('recalculate');
			scrollable.find('.tse-scroll-content').height(height).css("overflow", "auto");
			if (formId == 'onEditCreditCardOverlay') {
				$('.my-account-add-credit-card-overlay').height(496);
				$('.my-account-add-credit-card-overlay').find('.set-height').height(456);
			}
		}, 800);
	};

	scrollable.TrackpadScrollEmulator({
		wrapContent: false,
		autoHide: false
	});

	recalculate();

}
function isNumberOnly(evt, maxLength) {
	var maxLength = maxLength - 1;
	var value = evt.currentTarget.value.length;
	var len = evt.currentTarget.selectionEnd - evt.currentTarget.selectionStart;
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (evt.ctrlKey) {
		isCardNumInLimit = true;
		return true;
	}
	if (evt.which != 8 && evt.which != 0) {
		if (isNaN(String.fromCharCode(evt.which)) || value > maxLength && len == 0 && !(evt.ctrlKey && (charCode == 65 || charCode == 86 || charCode == 90 || charCode == 97 || charCode == 118 || charCode == 122)) || evt.which == 32) {
			evt.preventDefault();
			evt.stopPropagation();
			isCardNumInLimit = false;
			return false;
		}
	}
	isCardNumInLimit = true;
	return true;
}

function onBillingAddressChange() {
	var isFirstOptionSelected = $('#selectedCardBillingAddress option').first().prop('selected');
	var isLastOptionSelected = $('#selectedCardBillingAddress option').last().prop('selected');
	var isEditModeOnChange = $('#isEditModeOnChange').val();
	var isBillingAddressFromAddressBook = $('#isAddressFromAddressBookID').val();
	$('.my-account-add-credit-card-overlay').find('.drag-handle').height($('.set-height').height() / 6);
	reInitScrollBar('billingAddressChangeTrue', 350);
	if (isFirstOptionSelected && isBillingAddressFromAddressBook == 'false') {
		$('.col-md-9 .display-or').show();
	} else {
		$('.col-md-9 .display-or').hide();
	}
	var isEditCCAddressAvailable = $('#isEditCCAddressAvailable').val();
	if ((isFirstOptionSelected && isEditModeOnChange == 'false') || (isFirstOptionSelected && isEditModeOnChange == 'true' && isEditCCAddressAvailable == 'true')) {
		$('#billing-address-container .billing-address').hide();
		reInitScrollBar('billingAddressChangeTrue', 350);
		$("#myAccountAddCreditCardModal #addOrEditCreditCard .drag-handle").css({ "top": "50", "height": "60px" });
	} else {
		$('#billing-address-container .billing-address').show();
		reInitScrollBar('billingAddressChangeFalse', 350);
		var editCardNickNameID = $('#editCardNickNameID').val();
		var billingAddressNickname = $('#selectedCardBillingAddress').val();
		var param = {
			url: '/myaccount/intermediate/billingAddress.jsp',
			data: {
				formSerialize: 'billingAddressNickname=' + billingAddressNickname + '&editCardNickNameID=' + editCardNickNameID + '&isLastOptionSelected=' + isLastOptionSelected + '&isEditModeOnChange=' + isEditModeOnChange,
				formID: 'onChangeAddressInCCOverlay'
			},
		};
		makeAjax(param);
		$('#addOrEditCreditCard .tse-scroll-content').scrollTop(368);
	}

	if (isLastOptionSelected) {
		$('#billing-address-container input').val('');
		$('#billing-address-container #country option[value="US"]').attr('selected', 'selected');
		$('#billing-address-container #country').trigger('onchange');
	}

}

function isNumber(evt) {
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode == 45 || charCode == 32 || (charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)) return true;
	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	}
	return true;
}
function isPhoneNumberFormat(e) {
	if (e.which != 8 && e.which != 0) {
		if (isNaN(String.fromCharCode(e.which)) || e.which == 32) {
			e.preventDefault();
			e.stopPropagation();
			return false;
		}
	}

}

/*Start popover for password detai*/
var detailInfo = $('.detailsInfo');
var passwordDetails = $('#passwordDetails').val();
var passwordDetailsConti = $('#passwordDetailsConti').val();

var detailInfoTemplate = '<div class="popover detailWrapper" role="tooltip"><div class="arrow"></div><p>' + passwordDetails + passwordDetailsConti + '</p></div>';

detailInfo.hover(function (event) {
	event.preventDefault();
});

detailInfo.popover({
	animation: 'true',
	html: 'true',
	content: 'asdfasdf',
	placement: 'right',
	template: detailInfoTemplate,
	trigger: 'hover'
});
/*End popover for password detai*/


function loadQuickView(relatedproductId, pdp) {
	var ids;
	var productid = '';
	var onlinePid = '';
	var locationIdFromCookie = '';
	var myStoreCookie = $.cookie("favStore");
	if (typeof myStoreCookie != 'undefined' && myStoreCookie != '') {
		locationIdFromCookie = myStoreCookie.split('|')[1];
	}
	if (typeof relatedproductId != 'undefined' && relatedproductId != '') {
		ids = relatedproductId.split('|');
		productId = ids[0];
		if (ids.length > 1) {
			onlinePid = ids[1];
		}
	}
	pdp = (pdp) ? pdp : '';
	var contextPath = $('.contextPath').val();
	var quickViewPageName = "quickView";
	var quickViewPageType = pdp;

	var familyName = $('#familyname').val();
	$.ajax({
		type: 'POST',
		dataType: 'html',
		data: { relatedproductId: productId, quickViewPageType: quickViewPageType, onlinePid: onlinePid, familyName: familyName, locationIdFromCookie: locationIdFromCookie },
		url: contextPath + 'jstemplate/quickView.jsp',
		async: false,
		success: function (data) {
			$('#quickviewModal').html(data);
			//modalpopupClickFn();
			if (!productId && ($(data).find("[id^='quickViewId-']") && $(data).find("[id^='quickViewId-']").attr('id'))) {
				var recommendedQuickViewProductId = $(data).find("[id^='quickViewId-']").attr('id').split('-')[1];
			}
			quickviewpopup();
			recommendedQuickViewProductId ? ajaxCallForInventoryAndPrice(pdp, recommendedQuickViewProductId) : ajaxCallForInventoryAndPrice(pdp);
			notCachedAddTocartButton(productId, quickViewPageName);
			notCachedRegistryButtonsNew();
			if (productId != "" && productId != null && productId != undefined) {
				var productInfoJson = JSON.parse($(".productInfoJson-" + productId).val())
				var defaulstSku = productInfoJson.mDefaultSKU;
				populatePrice(defaulstSku, $('.sticky-price-contents'), productInfoJson);
			}
			bruCss();
			displayTooltipMessageOnAddtoCartButton('', 'quickview');
		},
		error: function (e) {
			$.ajax({
				type: 'GET',
				dataType: 'html',
				url: contextPath + 'jstemplate/errorProductDetailsTemplate.jsp',
				async: false,
				success: function (data) {
					$('#quickviewModal').html(data);
				}
			})
		}
	});
}



function identifyCardType(elm, currentEvent) {
	if (typeof currentEvent == 'undefined') {
		return false;
	}
	var keyCode = currentEvent.which || currentEvent.keyCode || currentEvent.charCode;
	if (keyCode >= 65 && keyCode <= 90) { return false; }
	var imageContextPath = window.contextPath + "images/";
	var value = $(elm).val();
	var img = [imageContextPath + "Payment_Small-Visa-Thumb-No-Outline.jpg", imageContextPath + "Payment_Small-Mastercard-Thumb-No-Outline.jpg", imageContextPath + "Payment_Small-Amex-Thumb-No-Outline.jpg", imageContextPath + "Payment_Small-Discover-Thumb.jpg", imageContextPath + "Payment_Small-R-US-card-Thumb-1.jpg", imageContextPath + "Payment_Small-R-US-card-Thumb-2.jpg"];
	var cardType = '';
	if (value.length > 5) {
		var v = parseInt(value.substr(0, 6));

		if ((v >= 400000 && v <= 499999) && (value.length > 5 || value.length == 13 || value.length == 16)) { /*visa card*/
			$(".display-card").show();
			$('.display-card .credit-card').attr('src', img[0]);
			$(this).attr('maxlength', '16');
			cardType = 'visa';
		} else if ((((v >= 510000 && v <= 523769) || (v >= 523771 && v <= 524362) || (v >= 524364 && v <= 559999)) && (value.length > 5 || value.length == 16))) {
			$(".display-card").show();
			$('.display-card .credit-card').attr('src', img[1]);
			$(this).attr('maxlength', '16');
			cardType = 'masterCard';
		} else if ((v >= 360000 && v <= 369999) && (value.length > 5 || value.length == 14)) {
			$(".display-card").show();
			$('.display-card .credit-card').attr('src', img[3]);
			$(this).attr('maxlength', '14');
			cardType = 'discover';
		} else if (((v >= 370000 && v <= 379999) || (v >= 340000 && v <= 349999)) && (value.length > 5 || value.length == 15)) { /*Amex card*/
			$(".display-card").show();
			$('.display-card .credit-card').attr('src', img[2]);
			$(this).attr('maxlength', '15');
			cardType = 'americanExpress';
		} else if ((v >= 601100 && v <= 601199 || v >= 622126 && v <= 622925 || v >= 628200 && v <= 628899 || v >= 644000 && v <= 659999 || v >= 300000 && v <= 305999 || v >= 309500 && v <= 309599 || v >= 352800 && v <= 358999 || v >= 380000 && v <= 399999 || v >= 624000 && v <= 625999) && (value.length > 5 || value.length == 16)) { /*discover card*/
			if ((v == 601104 || (v >= 601110 && v < 601120) || (v >= 601150 && v < 601174) || (v >= 601175 && v < 601177) || (v >= 601180 && v < 601186)) && (value.length > 5 || value.length == 16)) {
				$(".display-card").hide();
				cardType = 'invalid';
			} else {
				$(".display-card").show();
				$('.display-card .credit-card').attr('src', img[3]);
				$(this).attr('maxlength', '16');
				cardType = 'discover';
			}
		} else if (v == 604586 && (value.length > 5 || value.length == 16)) { /*PLCC card*/
			$(".display-card").show();
			$('.display-card .credit-card').attr('src', img[4]);
			$(this).attr('maxlength', '16');
			cardType = 'RUSPrivateLabelCard';
		} else if (v == 524363 && (value.length > 5 || value.length == 16)) { /*USCOBMasterCard card*/
			$(".display-card").show();
			$('.display-card .credit-card').attr('src', img[5]);
			$(this).attr('maxlength', '16');
			cardType = 'RUSCoBrandedMasterCard';
		} else if (v == 523770 && (value.length > 5 || value.length == 16)) { /*PRCOBMasterCard card*/
			$(".display-card").show();
			$('.display-card .credit-card').attr('src', img[5]);
			$(this).attr('maxlength', '16');
			cardType = 'RUSCoBrandedMasterCard';
		} else {
			$(".display-card").hide();
			$(this).removeAttr('maxlength');
			cardType = 'invalid';
		}
	} else {
		$(".display-card").hide();
		$(this).removeAttr('maxlength');
		cardType = 'invalid';
	}
	if (typeof (utag) != 'undefined' && cardType != 'undefined') {
		utag_data.order_payment_type = cardType;
	}
	cardTypeForCvvValidation = cardType;
	if ($('#layaway-tabs').length == 0) {
		enableDisableExpiryDate(cardType, elm);
		checkForFinancing();
		if ((value.length == 16 || value.length == 15) && (typeof isCardNumInLimit != "undefined" && isCardNumInLimit == true)) {
			//setCartTypeInOrder(cardType);
		}
		try {
			/*var keyCode 				= currentEvent.which || currentEvent.keyCode || currentEvent.charCode; Commented because we are restricting the alphabets before calling the setCardType function so initialised in  the begining of the function.*/
			var isDelOrBackKey = (value.length == 5 && (keyCode == 8 || keyCode == 46));
			var isCutOrPaste = ((currentEvent.ctrlKey && (keyCode == 118 || keyCode == 120 || keyCode == 86 || keyCode == 88)) && value.length < 6);
			if (value.length == 0 || isDelOrBackKey || isCutOrPaste) {
				//setCartTypeInOrder('');
			}
		} catch (e) { }
	}

}

/* Start: To avoid additional ajax request for promotions */
$(document).on("keydown", "#paymentCreditCardForm .credit-component input[type='text']", function () {
	var _this = $(this);

	if (_this.hasClass('card-number')) {
		window.cardNumLengthOn = _this.val().length;
	}
	else if (typeof _this.attr('id') != 'undefined' && _this.attr('id').indexOf('creditCard_CVVmaskedPassword') == 0) {
		window.cvvNumLengthOn = _this.val().length;
	}
	else if (_this.is('#nameOnCard')) {
		window.nameCardLengthOn = _this.val().length;
	}
});

$(document).on('focusout', "#paymentCreditCardForm .credit-component input[type='text']", function () {
	var _this = $(this);

	if (_this.hasClass('card-number')) {
		window.cardNumLengthOn = 15;
	}
	else if (typeof _this.attr('id') != 'undefined' && _this.attr('id').indexOf('creditCard_CVVmaskedPassword') == 0) {
		window.cvvNumLengthOn = 3;
	}
	else if (_this.is('#nameOnCard')) {
		window.nameCardLengthOn = 0;
	}
});
/* End: To avoid additional ajax request for promotions */

$(document).on("change", "#paymentCreditCardForm #credit-card-info select", function () {
	validateCredtcardFormForPromotion();
});
$(document).on("keyup", "#paymentCreditCardForm .credit-component input[type='text']", function () {
	if (typeof event.which != 'undefined' && event.which == 8) {
		window.cvvNumLengthOn = 3;
	}
	validateCredtcardFormForPromotion();
});

function validateCredtcardFormForPromotion() {
	var promotionFLag;
	if ($('#co-credit-card').length > 0 && !$('#co-credit-card option:selected').hasClass('addAnotherCardPaymentPage')) {
		promotionFLag = true;
		cardTypeForCvvValidation = $('#co-credit-card option:selected').data('cardtype');
	} else {
		if (document.getElementById("paymentCreditCardForm") && document.getElementById("paymentCreditCardForm")[name = "cardNumber"]) {
			if ((document.getElementById("paymentCreditCardForm")[name = "cardNumber"].value.length >= 16 || (cardTypeForCvvValidation == 'americanExpress' && document.getElementById("paymentCreditCardForm")[name = "cardNumber"].value.length >= 15)) &&
				(typeof isCardNumInLimit != "undefined") &&
				((cardTypeForCvvValidation != 'americanExpress' && document.getElementById("paymentCreditCardForm")[name = "creditCard_CVV"].value.length >= 3) || (cardTypeForCvvValidation == 'americanExpress' && document.getElementById("paymentCreditCardForm")[name = "creditCard_CVV"].value.length >= 4))) {
				//var eleSelector = document.getElementById("paymentCreditCardForm");
				var eleSelector = $("#paymentCreditCardForm");
				if ((eleSelector.find('[name="expirationYear"][disabled!="disabled"]').length && eleSelector.find('[name="expirationYear"][disabled!="disabled"]').val() == "") ||
					(eleSelector.find('[name="expirationMonth"][disabled!="disabled"]').length && eleSelector.find('[name="expirationMonth"][disabled!="disabled"]').val() == "") ||
					(eleSelector.find('[name="nameOnCard"]').val() === "") || (window.cardNumLengthOn == 16 || window.cvvNumLengthOn == 4 || window.nameCardLengthOn != 0)) {

					promotionFLag = false;
				}
				else {
					promotionFLag = true;
				}
			}
		}
	}

	if (promotionFLag) {
		tenderBasePromotionCheck(cardTypeForCvvValidation);
	}
}
function tenderBasePromotionCheck(cardTypeVal) {
	setCartTypeInOrder(cardTypeVal);
	loadGiftCardPromoCodeSection();
}
function loadGiftCardPromoCodeSection() {
	reloadGiftCardSection();
	reloadPromoCodeSection();
}
function enableDisableExpiryDate(cardType, _this) {
	if (cardType == 'RUSPrivateLabelCard') {
		$(_this).parents('form').find('#expirationMonth').addClass('grayedOut');
		$(_this).parents('form').find('#expirationYear').addClass('grayedOut');
		$(_this).parents('form').find('#expirationMonth').attr('disabled', 'disabled');
		$(_this).parents('form').find('#expirationYear').attr('disabled', 'disabled');
		$(_this).parents('form').find('#expirationYear, #expirationMonth').removeClass('error');
	} else if (cardType != 'RUSPrivateLabelCard' && $(_this).parents('form').find('#expirationMonth').hasClass('grayedOut')) {
		$(_this).parents('form').find('#expirationMonth').removeClass('grayedOut');
		$(_this).parents('form').find('#expirationYear').removeClass('grayedOut');
		$(_this).parents('form').find('#expirationMonth').removeAttr('disabled', 'disabled');
		$(_this).parents('form').find('#expirationYear').removeAttr('disabled', 'disabled');
	}
}

function checkoutSignInSubmit(requestPageRoot) {

	/* $('.errorDisplay').remove();*/
	removeBackendErrorMessagesInline($("#hearderLoginOverlay"));
	var contextPath = $(".contextPath").val();
	var email = $('#login').val();
	var modalCustId = $('#cartSign-custId').val();
	var modalPartnerName = $('#cartSign-partnerName').val();
	$.ajax({
		type: "POST",
		cache: false,
		/*async: false,*/
		dataType: "html",
		url: contextPath + "myaccount/intermediate/ajaxIntermediateRequest.jsp?formID=hearderLoginOverlay",
		data: $("#hearderLoginOverlay").serialize(),
		success: function (data) {
			if (data.indexOf('Following are the form errors:') > -1) {
				/*var msg = data.split('Following are the form errors:')[1];
				$('#signInModal .sign-in-header').after('<span class="errorDisplay">'+msg+'</span>');*/
				addBackendErrorsToForm(data, $("#hearderLoginOverlay"));
				$('#signInModal #signin-submit-btn').removeAttr('disabled');
				omniLoginFail();
			} else {
				localStorage.setItem('justLoggedIn', 'yes');
				console.log('justLoggedIn on checkout');
				//omniLoginScript(email, modalCustId, modalPartnerName);
				if (requestPageRoot == 'shipping') {
					//window.location.href = contextPath+"/checkout/shipping/shipping.jsp";
					continueToCheckout('shipping-tab');
					if ($("html").hasClass("modal-open")) {
						$("html").removeClass("modal-open");
						$("body").css('padding-right', '');
					}
					if (data.indexOf('showMergeCartOverlay:true') > -1) {
						$('#informationModal').modal('show');
						console.log("cart reminder 1");
						utag.link({
							branded_cart_reminder: 'Proceed to Cart'
						});
					}
					signInForTealium();
				}
				if (requestPageRoot == 'ispu') {
					if (data.indexOf('isOrderInstorePickup:true') > -1) {
						if (data.indexOf('redirectToShipping:true') > -1) {
							continueToCheckout('shipping-tab');
						} else {
							continueToCheckout('pickup-tab');
						}
						if (data.indexOf('showMergeCartOverlay:true') > -1) {
							$('#informationModal').modal('show');
							console.log("cart reminder 2");
							utag.link({
								branded_cart_reminder: 'Proceed to Cart'
							});
						}
					} else {
						continueToCheckout('shipping-tab');
						if (data.indexOf('showMergeCartOverlay:true') > -1) {
							$('#informationModal').modal('show');
							console.log("cart reminder 3");
							utag.link({
								branded_cart_reminder: 'Proceed to Cart'
							});
						}
					}
					if ($("html").hasClass("modal-open")) {
						$("html").removeClass("modal-open");
						$("body").css('padding-right', '');
					}
					signInForTealium();
				}
				if (requestPageRoot == 'cart') {
					window.location.href = contextPath + "cart/shoppingCart.jsp";
				}
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
	return false;
}



function onEditCreditCardOverlay_checkout(creditCardNickName) {
	$.ajax({
		type: "POST",
		url: "/checkout/payment/creditCard/checkoutOverlay.jsp",
		data: { creditCardNickName: creditCardNickName },
		/*async:false,*/
		dataType: "html",
		success: function (data) {
			$("#saved_card_details").remove();
			$(".credit-card-left-section").append(data);
			$(".editCreditCard").hide();
			$(".editCreditCard_cancel").show();
			$("#co-credit-card").attr('disabled', true);
			$("#co-credit-card_msdd").css('pointer-events', 'none');
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}

	});
	//setTimeout(function () {
	var selectCountry = $('#country').val();
	if (selectCountry == 'US') {
		$('#zip').mask('AAAAA-AAAA');
	} else {
		$('#zip').mask('AAAAAAAAAA');
		var zipVlaue = $('#zip').val();
		if (zipVlaue != '' && $('#zip').hasClass('error')) {
			$('#zip').removeClass('error').next('#zip-Validation-error').remove();
		}
	}
	//},1000);
}

function removeAddress_checkout() {
	var refreshDiv = "";
	if ($("#shippingAddressFormFragment").length) {
		refreshDiv = "shippingAddressFormFragment";
	} else if ($("#creditCardAjaxFragment").length) {
		refreshDiv = "creditCardAjaxFragment";
	}
	var nickName = $("#removeAddressHiddenId").val();
	$('#myShippingAddressForm .tse-scrollable .tse-content div.successMessage').remove();
	$.ajax({
		type: "POST",
		data: { nickName: nickName, formID: 'removeShippingAddress' },
		dataType: "html",
		cache: false,
		/*async:false,*/
		url: "/checkout/intermediate/shippingAddressAjaxFormSubmit.jsp",
		success: function (data) {
			//$("#shippingAddressFormFragment").html(data);
			$('#' + refreshDiv).html(data);
			$('#myShippingAddressForm .tse-content').prepend('<div class="successMessage">Address Removed Successfully</div>');
			$('#' + refreshDiv).find('.tse-scroll-content').scrollTop(0);
			reInitScrollBar('addressOverlay', 320);
			$('#myAccountDeleteCancelModal').modal('hide');
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}

	});
	return false;
}

function onAddCreditCardOverlay_checkout() {
	$.ajax({
		type: "POST",
		url: "/checkout/payment/creditCard/checkoutOverlay.jsp",
		dataType: "html",
		success: function (data) {
			$("#creditCardAjaxFragment").html(data);
			showHideBillingAddressInPayment();
			reInitScrollBar('onEditCreditCardOverlay', 300);
			setTimeout(function () {
				var selectedCountry = $('#country').val();
				if (($('#country').length > 0 && selectedCountry == 'US') || $('#country').length == 0) {
					$('#zip').mask('AAAAA-AAAA');
				} else {
					$('#zip').mask('AAAAAAAAAA');
				}
			}, 1000);
			//reInitValidation();
			/* Start: credit card number functionality */
			$(".card-number").keyup(function (e) {
				identifyCardType(this, e);
			});

			/* Start: credit card number functionality */
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
}

function addCreditCard_checkout() {
	$(document).find('#addOrEditCreditCard .successMessage').remove();
	suggestedShippingAddressSelect('1');
	$("#submitCreditCardId").click();
	$("#co-credit-card").msDropDown({ visibleRows: 4 });
	return false;
}
function deleteAddress_checkout() {
	$("#removeShippingAddressId").click();
	return false;
}

var tokenNumber = '';
var cBinNum = '';
var cLength = '';
function onSubmitAddCreditCard_checkout() {

	tokenNumber = $('#cardinalToken').val() || $('#addOrEditCreditCard #creditCardNumber').val() || tokenNumber;
	cBinNum = $('#cOlBinNum').val() || cBinNum;
	cLength = $('#cOlLength').val() || cLength;

	if (!$(".errorDisplay").hasClass('expiredMessage')) {
		$(".errorDisplay").remove();
	}
	if (tokenNumber == '') {
		tokenNumber = $('#creditCardNumber').val();
	}

	$('.successMessage').remove();
	if ($('#addOrEditCreditCard').valid()) {
		$.ajax({
			type: "POST",
			/*async:false,*/
			cache: false,
			url: "/checkout/intermediate/addcardAjaxSubmit.jsp",
			data: $("#addOrEditCreditCard").serialize() + '&ccToken=' + tokenNumber + '&cBinNum=' + cBinNum + '&cLength=' + cLength,
			dataType: "html",
			success: function (data) {
				if (data.indexOf('Following are the form errors:') > -1) {
					/*var msg = data.split('Following are the form errors:')[1];
					$('#addOrEditCreditCard .tse-scrollable .tse-content').prepend('<span class="errorDisplay errorAddCr">' + msg + '</span>');*/
					$('#addOrEditCreditCard .tse-scroll-content').scrollTop(0);
					/*addError('addOrEditCreditCard');*/
					addBackendErrorsToForm(data, $('#addOrEditCreditCard'));
				} else {
					if ($(".errorDisplay").hasClass('expiredMessage')) {
						$(".errorDisplay").remove();
					}
					$("#creditCardAjaxFragment").html(data);
					$('#addOrEditCreditCard .tse-content .successMessage').remove();
					$('#addOrEditCreditCard .tse-content').prepend('<div class="successMessage">Card Details Saved Successfully</div>');
					$('#addOrEditCreditCard #selectedCardBillingAddress').trigger('change');
					reInitScrollBar('onEditCreditCardOverlay', 300);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					cancelExitBrowserConfimation();
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'cart/shoppingCart.jsp';
				}
			}
		});
	}
	return false;
}

$(document).on('click', '.promotionContainer .terms-link,.cart-promotion-word-wrap .terms-link', function () {
    /*var content = $(this).parent().find('span.promotionDetailsSpan').html();
    $('#detailsModel #promoDetailContent').html(content);*/
	var seeTermLinkLabel = $(this).attr('target') || '';
	if(seeTermLinkLabel != '_blank'){
		var content = $(this).parent().find('span.promotionDetailsSpan').html();
		$('#detailsModel #promoDetailContent').html(content);
		$('#detailsModel').modal('show');	
	}
});



function ValidateEmail(email) {
	var expr = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	return expr.test(email);
};

function bruCss() {
	//return true;
	var excludeList = [];
	excludeList.push('.size-swatches-container .swatches');
	excludeList.push('.loadMoreProducts');
	excludeList.push('.loadMoreProducts span');
	if ($('.tru-logo-sm').length > 0 || $('.bru-shopping-cart').length > 0) {
		var truLinks = $('a').filter(function () {
			return $(this).css("color") == "rgb(0, 78, 188)" || $(this).hasClass("bru-primary-link");
		});
		//primary color change
		var toysColor = $('*').not(excludeList.join(',')).filter(function () {
			return $(this).css("color") == "rgb(0, 157, 219)" || $(this).css("color") == "rgb(0, 78, 188)" || $(this).hasClass("bru-headline");
		});
		/*for header*/
		var backGrndClr = $('.product-by-cat-zone header, .product-by-cat-zone h1, .product-by-cat-zone h2, .product-by-cat-zone h4, .product-by-cat-zone h5, .product-by-cat-zone h6').filter(function () {
			return $(this).css("background-color") == "rgb(0, 78, 188)";
		});
		backGrndClr.css({ "background-color": "rgb(94, 45, 145)" });
		toysColor.addClass("bru-headline");
		$('.navbar-tru').addClass('navbar-bru').removeClass('navbar-tru');
		$(".our-exclusive").addClass("bru-background");

		$('.tru-footer').addClass("bru-background");
		if ($(".collections-template").length) {
			truLinks.addClass("bru-primary-link");
			truLinks.filter(function () {
				return $(this).hasClass('pickup-see-terms-popover-class');
			}).removeClass("bru-headline");
		}
		else {
			truLinks.addClass("bru-primary-link");
			truLinks.filter(function () {
				return $(this).hasClass('pickup-see-terms-popover-class');
			}).removeClass("bru-headline");
		}
	}
	else {
		if ($(".collections-template").length) {
			$(".collections-availability .product-availability a").css({ color: "#009DDB" });
		}
	}
}

function onSubmitConfirmSignUpForm() {
	var contextPath = $(".contextPath").val();
	$('.errorDisplay').remove();
	if ($("#oc-signupEmail").css('display') == 'none') {
		var value = $("#oc-signupEmailDiv").text().trim();
		$("#oc-signupEmail").val(value);
	}
	$.ajax({
		type: "POST",
		cache: false,
		/*async:false,*/
		context: this,
		url: "/checkout/intermediate/ajaxIntermediateRequest.jsp?formID=confirmSignUpForm",
		data: $("#confirmSignUpForm").serialize(),
		success: function (data) {
			$('span.errorDisplay').remove();
			if (data.indexOf('Following are the form errors:') > -1) {
				var msg = data.split('Following are the form errors:')[1];
				/*$('.checkout-order-complete .order-complete-columns').before('<span class="errorDisplay">'+msg+'</span>');
		  addError('confirmSignUpForm');*/
				$("#confirmSignUpSubmitBtn").css({ position: 'inherit' });
				$("#confirmSignUpSubmitBtn").parent('.button-container').css({ minHeight: '43px', marginTop: '10px' });
				$("#confirmSignUpSubmitBtn").parents('.bordered-column').css({ height: 'auto' });
				addBackendErrorsToForm(data, $("#confirmSignUpForm"));
			} else {
				$(".col-md-4.create-an-account .bordered-column").fadeOut(400);
				$(".col-md-4.create-an-account .create-account-thank-you").fadeIn(400);
				removeBackendErrorMessagesInline($("#confirmSignUpForm"));
				$(".head_welcome_cont").load(window.contextPath + "header/headWelcomeContent.jsp", function () {
					setFirstNameCookie();
				});
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp?sessionExpired=true';
			}
		},
		dataType: "html"
	});

	return false;
}
$(document).on('click', '#confirmSignUpSubmitBtn', function () {
	removeBackendErrorMessagesInline($("#confirmSignUpForm"));
	var signUpEmailDiv = $('#oc-signupEmailDiv').css('display');
	if (signUpEmailDiv == 'block' && $(document).find("#oc-signupEmailDiv").length && $(document).find("#oc-signupEmail").length && $(document).find("#oc-signupEmail").val() == "") {
		$(document).find("#oc-signupEmail").val($(document).find("#oc-signupEmailDiv").text().trim());
	}
	if (!$('#confirmSignUpForm').valid()) {
		checkMaskPasswordError($('#confirmSignUpForm'));
		return false;
	}
	onSubmitConfirmSignUpForm();
});
$(document).on("click", "#oc-editEmail", function () {
	var value = $("#oc-signupEmailDiv").text().trim();
	$("#oc-signupEmail").css({ display: "inline" });
	$("#oc-signupEmail").val(value);
	$("#oc-signupEmail").focus();
	$("#oc-signupEmailDiv").css({ display: "none" });
	$("#oc-editEmail").css({ display: "none" });
});

/*disable caps lock tool tip in IE*/
document.msCapsLockWarningOff = true;

function onEmailSignup() {
	var emailAddress = $('#emailAddressTxt').val();
	var values = $('#emailAddressTxt').val().length;
	var error = $('#blankEmailId').val();
	var error1 = $('#invalidEmailId').val();
	if (values == 0) {
		$('<p class="error-blanck">' + error + '</p>').insertBefore(".email-alerts-sign-up");
	} else if (!ValidateEmail(emailAddress)) {
		$('<p class="error-invalid">' + error1 + '</p>').insertBefore(".email-alerts-sign-up");
	}
	else {
		$.ajax({
			type: 'post',
			data: { formID: 'emailSubmit' },
			dataType: 'html',
			url: '/common/email.jsp?email=' + emailAddress,
			success: function (response) {
				console.log('Email submit called.');
				$("#email-alerts .bordered-column").fadeOut(400);
				$("#email-alerts .create-account-thank-you").fadeIn(400);
			}
		});
	}
}
function redirectToMyAccount() {
	var contextPath = $(".contextPath").val();
	location.href = contextPath + "myaccount/myAccount.jsp";
}
function showMenuOptions() {

	var filterCount = $(".sub-cat-filter-list:visible").length;
	if (filterCount == 0) $(".clear-all-filters").addClass('hide');

	if (filterCount >= 10) $(".sub-cat-more").removeClass('hide');
	else $(".sub-cat-more").addClass('hide');

}

function quickviewpopup() {

	var learnMoreTooltips = $('.learn-more-tooltips').html();
	var detailsTooltipsContent = $(".details-tooltips-content").html();

	var pdppagedetailTooltip = $('.details-tooltip');

	var pdpDetailsPopoverTemplate = '<div class="popover detailWrapper paypalDdetailsWrapper " role="tooltip"><div class="arrow"></div><p>' + detailsTooltipsContent + '</p></div>';

	pdppagedetailTooltip.click(function (event) {

		event.preventDefault();
	});

	pdppagedetailTooltip.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: pdpDetailsPopoverTemplate,
		trigger: 'click'
	});
	var pdppageLearnmoreTooltip = $('.learn-more-tooltiplink');

	var pdpLearnmorePopoverTemplate = '<div class="popover detailWrapper learn-more-tooltips" role="tooltip"><div class="arrow"></div><p>' + learnMoreTooltips + '</p></div>';

	pdppageLearnmoreTooltip.click(function (event) {
		event.preventDefault();
	});

	pdppageLearnmoreTooltip.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: pdpLearnmorePopoverTemplate,
		trigger: 'click'
	});

	var pdppageSeetermTooltip = $('.see-termlink');

	var pdpSeetermPopoverTemplate = '<div class="popover detailWrapper paypalDdetailsWrapper" role="tooltip"><div class="arrow"></div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div>';

	pdppageSeetermTooltip.click(function (event) {
		event.preventDefault();
	});

	pdppageSeetermTooltip.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: pdpSeetermPopoverTemplate,
		trigger: 'click'
	});

	var pdppageEstimatedDetailTooltip = $('.estimated-details-tooltip');

	var estimatedDetailsTooltipContent = $(".estimated-details-tooltip-content").html();

	var pdpEstimatedDetailPopoverTemplate = '<div class="popover detailWrapper paypalDdetailsWrapper" role="tooltip"><div class="arrow"></div><p>' + estimatedDetailsTooltipContent + '</p></div>';

	pdppageEstimatedDetailTooltip.click(function (event) {
		event.preventDefault();
	});

	pdppageEstimatedDetailTooltip.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: pdpEstimatedDetailPopoverTemplate,
		trigger: 'click'
	});

	$(window).load(bruCss);
}

function progressBar() {
	setProgressBarCookie();
	setOrderItemCountCookie();
	
	var freeShippingProgressBarCookieValue = $.cookie("freeShippingProgressBarCookie");
	if (isEmpty(freeShippingProgressBarCookieValue)) {
	var promotionValue = parseInt($("#promotionValue").val());

	var orderValue = parseInt($("#orderValue").val());

	var total = orderValue / promotionValue * 100;
	$(".summary-block-free-shipping-bar").attr('value', total);
	$('.summary-block-free-shipping-bar').each(function () {
		var progressbar = $(this),
			progressvalue = $(this).attr('value');
		if (progressvalue >= 0) {
			progressbar.progressbar({
				value: false,

			});

			function progress() {
				if (progressbar && progressbar.progressbar) {
					try {
						var val = progressbar.progressbar("value");
						progressbar.progressbar("value", val + 1).addClass("end");
						if (val < progressvalue) {
							setTimeout(progress, 'fast');
						}
					}
					catch (e) { }
				}
			}

			setTimeout(progress, 'fast');
		}
	});
	}
}
function ajaxProgressBar(pData) {
	var cookieData = pData.split('|');
	if (cookieData.length > 1) {
		$('#showProgressBar').css("display", "block");
		var remainingValue = parseInt(cookieData[0]);
		var promotionValue = parseInt(cookieData[1]);
		if (promotionValue === 0) {
			$('#showProgressBar').css("display", "none");
		}
		if (remainingValue < 0) {
			remainingValue = 0;
		}
		var total = (promotionValue - remainingValue) / promotionValue * 100;
		if (remainingValue === 0) {
			$('#qualifyPromotion').css("display", "block");
			$('#notQualifyPromotion').css("display", "none");
		} else {
			$('#amountToQualify').html(remainingValue);
			$('#notQualifyPromotion').css("display", "block");
			$('#qualifyPromotion').css("display", "none");
		}
		/*Commented as it is dupicated code*/
		$(".summary-block-free-shipping-bar").attr('value', total);
		$('.summary-block-free-shipping-bar').each(function () {
			var progressbar = $(this),
				progressvalue = $(this).attr('value');
			console.log(progressvalue);
			if (progressvalue > 0) {
				progressbar.progressbar({
					value: false,

				});

				function progress() {

					var val = progressbar.progressbar("value");

					progressbar.progressbar("value", val + 1).addClass("end");


					if (val < progressvalue) {
						setTimeout(progress, 'fast');
					}
				}

				setTimeout(progress, 'fast');
			}
		});
	}
	$('#global-nav-typeahead').find('.tt-dropdown-menu').css('width', $("#searchText").outerWidth() + 'px'); //for search suggesions bar
}

$(document).ready(function () {
	try {
		if ($('#checkout-container').length == 0) {
			$("input[type=password]").maskPassword();//password masking
		}
	} catch (e) { }



	$('.formValidation').validate({
		rules: {
			email: {
				required: true,
				email: true
			},
		},
		messages: {
			email: {
				required: 'invalid email address',
				email: 'invalid email address'
			}
		}
	});

	/* product-categories section */

	$('ul.all-products-sec li').each(function () {

		if ($(this).hasClass('active'))
			productbycategoties($(this), true);
		else {
			productbycategoties($(this), false);
		}
	});

	$('body').on('click', 'ul.all-products-sec li', function () {
		if ($(this).hasClass('inactive')) { return };
		productbycategoties($(this), true);
	});

	function productbycategoties($this, boolvalue) {
		var selectedvalue = $this.find('a').html().toLowerCase();
		var firstchar = selectedvalue.split('-')[0];
		var secondchar = selectedvalue.split('-')[1];

		var listContainer = $('#character .char-section');
		var list = listContainer.find('div');
		list.removeClass('show');
		list.removeClass('hide');
		list.addClass('hide');
		if (boolvalue == true) {
			if (firstchar == 0) {
				for (i = 0; i < 10; i++) {
					listContainer.find('div[class^=' + i + ']').removeClass('hide').addClass('show');
				}
			}
			else {
				listContainer.find('div[class^=' + firstchar + ']').removeClass('hide').addClass('show');
				listContainer.find('div[class^=' + secondchar + ']').removeClass('hide').addClass('show');
			}
		}
		else {
			$this.addClass('inactive');
			if (listContainer.find('div[class^=' + firstchar + ']').length > 0 || listContainer.find('div[class^=' + secondchar + ']').length > 0) {
				$this.removeClass('inactive');
				console.log("test" + firstchar + secondchar);
			}

		}


	}


	$('#shop-by-sort-list-items.active-letter li[class^="a"]').addClass('show');

	$('body').on('click', 'ul#shopByTabs li', function () {
		var selctedAlphabate = $(this).find('a').html().toLowerCase(),
			listIndex = $(this).index(),
			listContainer = $('#shop-by-sort-list-items.active-letter');
		var list = listContainer.find('li');
		list.removeClass('show');
		list.removeClass('hide');
		listContainer.find('li[class^=' + selctedAlphabate + ']').addClass('show');
		list.not('[class^=' + selctedAlphabate + ']').addClass('hide');
		//listContainer.find('.shop-by-letter-image').css('background-position','0px -'+ ( listIndex * 62 ) +'px');
		listContainer.find('[class^=shop-by-letter-image]').html("<span>" + selctedAlphabate.toUpperCase() + "</span>");
	});
});


function startTokenizationMyAccount() {
	$('.global-error-display').html('');
	if (!$('#addOrEditCreditCard').valid()) {
		return false;
	}
	var isEditMode = $('#addOrEditCreditCard #isEditSavedCard').val();
	if (isEditMode == 'true') {
		startTokenization();
	} else {
		$('#addOrEditCreditCard').valid();
		var cardNum = $('#addOrEditCreditCard #creditCardNumber').val();
		var isValidCard = validateCreditCardNumber(cardNum);
		if (isValidCard) {
			startTokenization();
		} else {
			addBackendErrorsToForm(window.invalidCardErroMsg, $('#addOrEditCreditCard'));
			//$('.global-error-display').text(window.invalidCardErroMsg);
			//$('#addOrEditCreditCard .tse-scroll-content').scrollTop(0);
		}
	}
	return false;
}


$(document).on('submit', '#chekcoutSignUpEmailForm', function () {
	$('.successMessage').remove();
	$('.errorDisplay').remove();
	$(".passwordInput,.confirmPasswordInput").removeClass("error-highlight");
	if (!$('#chekcoutSignUpEmailForm').valid()) {
		return false;
	}
	var email = $('#email').val();
	var contextPath = $('.contextPath').val();
	var pattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	var isValidEmail = pattern.test(email);
	if (isValidEmail) {
		$('.email-signup-thank-you').fadeIn();
		$('.email-signup-container').hide();
		$.ajax({
			type: "POST",
			url: contextPath + 'checkout/intermediate/ajaxIntermediateRequest.jsp',
			data: { formID: 'emailSignUp', email: email },
			dataType: "html",
			success: function (responseData) {
				if (responseData.indexOf('Following are the form errors:') > -1) {
					addBackendErrorsToForm(responseData, $("#chekcoutSignUpEmailForm"));
				}
			},
			error: function (e) {
				console.log(e);
			}
		});
	} else {
		$('#chekcoutSignUpEmail').before('<span class="errorDisplay">Invalid email address</span>');
	}
	return false;
});

function onEditCreditCardOnPaymentPage() {
	var nickname = $("#co-credit-card option:selected").attr("value");
	onEditCreditCardOverlay_checkout(nickname);
	$('#co-credit-card').attr('disabled', 'disabled');
}

function goToMyAccount() {
	window.location.href = "/myaccount/myAccount.jsp";
}


$(document).on('keyup', '.creditCardCVV', function (event) {
	checkForFinancing();
});

function checkForFinancing() {
	var cardNum = '', subCardNum = '', canEnable = false;

	var cardType = $('#co-credit-card option:selected').data('cardtype');
	var isSavedRUSCard = (cardType == "RUSCoBrandedMasterCard" || cardType == "RUSPrivateLabelCard");
	var canEnable = false;
	if (isSavedRUSCard == true && $('#radio-creditcard').is(':checked')) {
		canEnable = true;
		if ($(".card-number").length) {
			cardNum = $(".card-number").val();
		} else if ($("#creditCardNumber").length) {
			cardNum = $("#creditCardNumber").val();
		}
	} else if ($(".card-number").length) {
		cardNum = $(".card-number").val();
		subCardNum = cardNum.substring(0, 6);
		canEnable = cardNum.length == 16 && (subCardNum == 604586 || subCardNum == 524363 || subCardNum == 523770) && $(".creditCardCVV").val().length > 2;
	}
	if (cardType == "RUSCoBrandedMasterCard" || cardType == "RUSPrivateLabelCard" || cardType == undefined) {
		cardType = 'credit-card';
	} else {
		cardType = 'rus-credit-card';
	}
	if (canEnable) {


		$.ajax({
			type: "POST",
			cache: false,
			/*async:false,*/
			dataType: "html",
			url: '/checkout/payment/specialFinancing.jsp',
			data: { isCreditCardSelected: true, creditCardNumber: cardNum, cardType: cardType },
			success: function (responseData) {
				$('#six-month-financing-credit-card-div').html(responseData);
				$('.six-month-financing.credit-card .finacingActionDiv').removeClass('finaceGrayedOut');
				$('.six-month-financing.credit-card .finacingActionDiv input').removeAttr('disabled');
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					cancelExitBrowserConfimation();
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'cart/shoppingCart.jsp';
				}
			}
		});

	} else {
		$('.six-month-financing.credit-card').find('#yes-six-month-financing').attr('checked', false);
		$('.six-month-financing.credit-card').find('#no-six-month-financing').attr('checked', false);
		$('.six-month-financing.credit-card .finacingActionDiv').addClass('finaceGrayedOut');
		$('.six-month-financing.credit-card .finacingActionDiv input').attr('disabled', 'disabled');
		$('.six-month-financing.credit-card').find('.disclosure-agreed').hide();
		$('.six-month-financing.credit-card').find('#electronicTermsNotify').hide();
	}
}

function cancelExitBrowserConfimation() {
	window.onbeforeunload = '';

}

function checkGiftCardBalance(formID) {

	//$('.errorDisplay').remove();
	$('#giftCardBalanceModal p.global-error-display').css({ 'display': 'none' });
	if (!$('#' + formID).valid()) {
		return false;
	}
	if ($(".my-account-template").length) {
		pageName = "myAccount";
	} else {
		pageName = "checkout";
	}
	$.ajax({
		type: "POST",
		url: '/checkout/payment/checkBalance.jsp?pageName=' + pageName,
		data: $('#' + formID).serialize() + '&formID=' + formID,
		/*async : false,*/
		success: function (responseData) {

			switch (formID) {
				case 'myAccountCheckBalanceForm':
					if (responseData.indexOf('Following are the form errors:') > -1) {
						addBackendErrorsToForm(responseData, $('#myAccountCheckBalanceForm'));
					}
					else {
						$('.card-input').hide();
						$('.my-account-gift-card-balance-container div').html(responseData);
						$('.my-account-gift-card-balance-container').show();
					}
					break;

				case 'giftCardBalanceForm':
					if (responseData.indexOf('Following are the form errors:') > -1) {
						addBackendErrorsToForm(responseData, $('#giftCardBalanceForm'));
					}
					else {
						$('.gift-card-container').hide();
						$('.gift-card-balance-container div').html(responseData);
						$('.gift-card-balance-container').show();
					}
					break;
			}

		},
		error: function (a, b, c) {
			console.log(a);
		},
		dataType: "html"
	});
	return false;
}

var successCallback = function (data) {
	setProgressBarCookie();
	setOrderItemCountCookie();
	setStoreAddressCookie();
	setFirstNameCookie();
}
var errorCallback = function () {
	setProgressBarCookie();
	setOrderItemCountCookie();
	setStoreAddressCookie();
	setFirstNameCookie();
}

function setStoreAddressCookie() {
	var myStoreAddress = $.cookie("myStoreAddressCookie");
	if (!isEmpty(myStoreAddress)
		&& document.getElementById("myStoreAddressCookieValueDIV")) {
		document.getElementById("myStoreAddressCookieValueDIV").innerHTML = myStoreAddress;
	}
}

function isEmpty(val) {
	return (val === undefined || $.trim(val) == '') ? true : false;
}

function ajaxCall(pUrl, pSuccessCallback, pErrorCallback) {
	$.ajax({
		type: "POST",
		url: pUrl,
		dataType: "html",
		async: false,
		processData: false,
		success: function (data, status, xhr) {
			pSuccessCallback(data);
		},
		error: function (jqXHR, textStatus, errorThrown) {
			pErrorCallback(jqXHR, textStatus, errorThrown);
		}
	});

}

function checkWelcomeBackShoppingPopup() {
	var abandonCheckbox = $.cookie('abandonCheckbox');
	var showWelcomeBack = $.cookie('showWelcomeBack');
	var itemCountCookie = $.cookie('itemCountCookie');
	var reminderCountCookie = $.cookie('reminderCountCookie');
	var sessionExpired = $(".sessionExpired").val();
	var siteReminderCount = $.cookie('siteReminderCount');
	if ((typeof siteReminderCount != "undefined" && siteReminderCount > 0) && (typeof abandonCheckbox == "undefined" || abandonCheckbox !== "true") && (sessionExpired !== 'true') && (parseInt(itemCountCookie) > 0) && (showWelcomeBack == "true") && (typeof reminderCountCookie == "undefined" || parseInt(reminderCountCookie) < parseInt(siteReminderCount))) {
		bruCss();
		$('#welcomeBackModal').modal('show');
		if (typeof utag != 'undefined') {
			console.log('calling omniture abandoned cart reminder');
			utag.link({
				branded_cart_reminder: 'Proceed to Cart'
			});
		}
		abandonCartEmailIdChek();
		var remCountCookie = $.cookie('reminderCountCookie');
		if (typeof remCountCookie != "undefined") {
			var finalCount = parseInt(remCountCookie) + 1;
			document.cookie = 'reminderCountCookie' + "=" + finalCount + "; path=/";
		} else {
			document.cookie = 'reminderCountCookie' + "=" + 1 + "; path=/";
		}
	};

}

/*checkout seeTerm tooltip*/
$(document).on('click', '.confirmation-seeTerm', function (e) {
	e.preventDefault();
});
$(document).ajaxComplete(function () {

	universalFormValidationHandler();
	initAddressZipCodeMask();
	if ($('#creditCardCVVCodemaskedPassword0').length == 0) {
		$('input#creditCardCVVCode').maskPassword();
	}

	if ($('#co-credit-card').length == 1) {
		//checkForFinancing();
	}

	$('.confirmation-seeTerm:last').addClass('reviewLastItem');


	var shippingToolInConfirm = $('.shippingToolInConfirm').html();

	$('.confirmation-seeTerm:not(.reviewLastItem)').popover({
		animation: 'true',
		html: 'true',
		content: shippingToolInConfirm,
		placement: 'bottom',
		trigger: 'hover'
	});

	$('.confirmation-seeTerm.reviewLastItem').popover({
		animation: 'true',
		html: 'true',
		content: shippingToolInConfirm,
		placement: 'top',
		trigger: 'hover'
	});
	estimatedTaxTooltip();
});

function isValidEmail(email) {
	var patern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4})(\]?)$/;
	return patern.test(email);
}


$(document).on('click', '#clearRecentlyViewedItems', function () {
	var visitorFirstName = $.cookie("VisitorFirstName");

	if (visitorFirstName != null) {
		localStorage.removeItem('recentViewedItemsForloggedIn');
	} else {
		localStorage.removeItem('recentViewedItemsForAnonymous');
	}
	populateRecentlyViewedItems('megamenu');

	try {
		$('#exploreBox').animate({ 'width': '1000px', 'max-width': '1000px' }, 400, function () {
			$('#exploreBox').animate({ 'width': '240px' }, 500, function () {
				$('#exploreBox').addClass('noRecentlyViewedProducts');
			});
		});
	} catch (e) { }

});



function createLocalStorageDataRecentlyViewed(pdata) {
	//alert("createLocalStorageDataRecentlyViewed"+pdata);
	var visitorFirstName = $.cookie("VisitorFirstName");

	//console.log("recently view item called");
	var iframeWindow = document.getElementById('recentlyViewedItemsIframe');

	if (visitorFirstName != null) {
		createLocalStorageforSignedInUser(pdata, 'PDP');
	} else {

		var localStorageType = 'recentViewedItemsForAnonymous';
		var storedData = localStorage.getItem(localStorageType);

		if (storedData == null || storedData == '') {
			if (pdata != undefined) {
				var tmpData = [];
				tmpData.unshift(JSON.stringify(pdata));
				storedData = localStorage.setItem(localStorageType, JSON.stringify(tmpData));
			}
		}
		else {
			// Check whether header data is empty or not
			if (pdata != undefined && pdata.onlinePID != undefined && pdata.onlinePID != '') {

				var tmpStorage = $.parseJSON(storedData);

				var filteredStorage = tmpStorage.filter(function (val, i) {
					var tmpVal = $.parseJSON(val);
					var flag = true;

					try {
						if (tmpVal.produtType != 'undefined' && tmpVal.productType == 'Collection') {
							flag = tmpVal.collectionId != pdata.collectionId;
						} else {
							flag = tmpVal.onlinePID != pdata.onlinePID;
						}
					} catch (e) { console.log('Error: create recently viewed localstorage.'); }

					return flag;
				});

				filteredStorage.unshift(JSON.stringify(pdata));

				// Max capacity check
				var maxLimit = $('#recentlyViewedMaxLimit').val() || 8;

				if (filteredStorage.length > parseInt(maxLimit)) {
					filteredStorage.pop();
				}

				localStorage.setItem(localStorageType, JSON.stringify(filteredStorage));
			}


			else {
				if (pdata != undefined && pdata.collectionId != undefined && pdata.collectionId != '') {

					var tmpStorage = $.parseJSON(storedData);

					var filteredStorage = tmpStorage.filter(function (val, i) {
						return $.parseJSON(val).collectionId != pdata.collectionId;
					});

					filteredStorage.unshift(JSON.stringify(pdata));

					// Max capacity check
					var maxLimit = $('#recentlyViewedMaxLimit').val() || 8;

					if (filteredStorage.length > parseInt(maxLimit)) {
						filteredStorage.pop();
					}

					localStorage.setItem(localStorageType, JSON.stringify(filteredStorage));



				}
			}


		}
		if (iframeWindow !== null) {
			iframeWindow.contentWindow.postMessage({ key: localStorageType, data: localStorage.getItem(localStorageType) }, "https://" + window.location.host);
		}
	}

}


function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1);
		if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
	}
	return "";
}

function populateRecentlyViewedItems(pageName) {


	var productJson = '';

	var visitorFirstName = $.cookie("VisitorFirstName");

	if (visitorFirstName != null) {
		productJson = $.parseJSON(localStorage.getItem('recentViewedItemsForloggedIn'));
	} else {
		productJson = $.parseJSON(localStorage.getItem('recentViewedItemsForAnonymous'));

		if (productJson != "{}" && productJson != "" && productJson != null && typeof productJson != 'undefined') {
			for (var item = 0; item < productJson.length; item++) {
				productJson[item] = $.isEmptyObject($.parseJSON(productJson[item])) ? '' : $.parseJSON(productJson[item]);
			}
		} else {
			productJson = '';
		}
	}


	var recentlyViewedHtml = '';
	try {
		recentlyViewedHtml = (productJson != null && productJson != '') ? $('#recentlyViewedTemplate').tmpl(productJson) : '';
	} catch (e) {
		console.log('Template error.');
	}



	$('#recentlyViewedCookie').html(recentlyViewedHtml);






	if (pageName == 'collection') {

		try {

			var productIds = [];
			$.each(productJson, function (pKey, pVal) {
				if (pVal.productType != 'Collection') {
					productIds.push(pVal.productId);
				}
			});

			var promos = [];
			if (productIds.length > 0) {
				$.ajax({
					type: "POST",
					url: '/jstemplate/promoRecentlyViewed.jsp',
					data: { productIdsForPromo: productIds.join() },
					dataType: 'JSON',
					async: false,
					cache: false,
					success: function (responseData) {
						//console.log(responseData);
						promos = responseData;
					}
				});
			}


			var productJsonTmp = [];
			$.each(productJson, function (pKey, pVal) {

				var tmpProd = pVal;
				var tmpPromoMsg = {};

				$.each(promos, function (pmKey, pmVal) {
					if (tmpProd.productType != 'Collection' && tmpProd.productId == pmKey) {
						tmpPromoMsg = { "message": pmVal };
					}
				});

				$.extend(tmpProd, { 'promotionPrd': tmpPromoMsg });
				productJsonTmp.push(tmpProd);

			});

			productJson = productJsonTmp;



			var recentlyViewedCollectionHtml = (productJson != null && productJson != '') ? $('#recentlyViewedCollectionTemplateBody').tmpl({ "productList": productJson }) : '';
			$('.my-account-product-carousel').html(recentlyViewedCollectionHtml);
			if (recentlyViewedCollectionHtml != ' ' && recentlyViewedCollectionHtml != '' && recentlyViewedCollectionHtml != null) {
				$('.my-account-product-carousel').css({ "margin-top": "-15px" });
			}
		} catch (e) { console.log('recentlyViewed(Collection):' + e); }

	}
}

var successPDPCallback = function (data, pheader) {
	//PDP inner HTML

	$(".sticky-price-template").html(data);
	createLocalStorageDataRecentlyViewed(pheader);

}
var errorPDPCallback = function () {
}
function populatePDPData(productId) {

	var recentViewedLocalStorage = localStorage.getItem('recentViewedItems');
	var contextPath = $(".contextPath").val();
	ajaxPDPCall(contextPath, productId, successPDPCallback, errorPDPCallback);
}
function ajaxPDPCall(contextPath, pProductId, pSuccessCallback, pErrorCallback) {

	$.ajax({
		type: "POST",
		url: contextPath + 'jstemplate/skuVariantsAndPriceSection.jsp?productId=' + pProductId,
		dataType: "html",
		processData: false,
		success: function (data, status, xhr) {
			var headervalue = xhr.getResponseHeader("recentViewedItems");
			if (typeof headervalue !== 'undefined' && headervalue !== null) {
				pSuccessCallback(data, headervalue);
			}
			pSuccessCallback(data);

		},
		error: function (jqXHR, textStatus, errorThrown) {
			pErrorCallback(jqXHR, textStatus, errorThrown);
		}
	});
}

function orderSummarySection() {
	var summaryBlock = $('#order-summary-block');

	if (summaryBlock.length > 0) {

		var cartNav = $('.shopping-cart-nav');
		var btmBoundary = $('.shopping-cart-grey-placeholder');
		var scrollStart = cartNav.height();
		var scrollStop = btmBoundary.offset().top - summaryBlock.height() - 12;
		if (scrollStop < 0) {
			scrollStop = btmBoundary.offset().top - summaryBlock.height() - 12 + 90;
		}
		$(window).scroll(function () {
			scrollTop = $(window).scrollTop();

			if (scrollTop >= scrollStop) {
				summaryBlock.css({
					"position": "absolute",
					"top": scrollStop + 15
				});
			}
			else if (scrollTop >= scrollStart) {
				summaryBlock.css({
					"position": "fixed",
					"top": 15,
					"margin-top": 0,
				});
			}
			else {
				summaryBlock.removeAttr('style');
			}
		});

	}
}

function createLocalStorageforSignedInUser(recentlyViewedItems, fromPage) {
	//alert('createLocalStorageforSignedInUser='+recentlyViewedItems);
	var iframeName = "nonSecureRecentlyViewProducts",
		host = window.location.protocol + "//" + window.location.host;
	if (!isEmpty(recentlyViewedItems)) {
		localStorage.setItem('recentViewedItemsForloggedIn', JSON.stringify(recentlyViewedItems));
		if (typeof fromPage !== "undefined" && fromPage == "PDP") {
			iframeName = "recentlyViewedItemsIframe";
			host = window.location.protocol + "//" + window.location.host;
		}
		var iframeWindow = document.getElementById(iframeName);
		if (iframeWindow !== null) {
			iframeWindow.contentWindow.postMessage({ key: 'recentViewedItemsForloggedIn', data: JSON.stringify(recentlyViewedItems) }, host);
		}
	}
}

function ajaxCallForOrderConfirmationPage(contextPath) {
	var queryString = window.location.search.substring(1);
	$.ajax({
		type: "POST",
		url: contextPath + "checkout/confirm/orderConfirmationInclude.jsp",
		dataType: "html",
		processData: false,
		success: function (data, status, xhr) {
			try {
				$("#orderConfirmationInclude").html(data);
				passwordMeter();
				$("#orderConfirmationInclude").find("input[type=password]").maskPassword();
				if ($(document).find("#oc-signupEmailDiv").length && $(document).find("#oc-signupEmail").length && $(document).find("#oc-signupEmail").val() == "") {
					$(document).find("#oc-signupEmail").val($(document).find("#oc-signupEmailDiv").text().trim());
				}
				clientValidation();
				$(window).trigger('resize');
			} catch (e) { console.log(e) }


			/* Start: popover for see terms in pickup tab */
			var storePickupSeeTerms = $('a[data-target="pickup-see-terms-popover"]');

			if (storePickupSeeTerms.length > 0) {
				try {
					var storePickupSeeTermsTemplate = $('.storePickupSeeTerms');

					storePickupSeeTerms.hover(function (event) {
						event.preventDefault();
					});

					storePickupSeeTerms.popover({
						animation: 'true',
						html: 'true',
						content: 'asdfasdf',
						placement: 'bottom',
						template: storePickupSeeTermsTemplate,
						trigger: 'hover'
					});
				} catch (e) { }
			}
			setTimeout(function () {
				if ($("#load-checkout-norton-script").length) {
					$("[id=load-checkout-norton-script]").html($("#loadNortonHiddenDiv").html());
				}
			}, 4000);
			/* End: popover for see terms in pickup tab */
			formatCreditCardDisplay();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}


	});
}

function ajaxCallForCheckoutPage(contextPath) {
	$.ajax({
		url: contextPath + "checkout/common/emptyCartRedirect.jsp",
		type: "POST",
		dataType: "json",
		/*async:false,*/
		cache: false,
		success: function (data) {
			var successData = data;
			if (successData.redirect == "true" || successData.redirect == true) {
				cancelExitBrowserConfimation();
				window.location.href = contextPath + "cart/shoppingCart.jsp";
			} else {
				var queryString = window.location.search.substring(1);
				var paypalErrorMessages = $.trim($("#payPalErrorMessages").text());
				$.ajax({
					type: "POST",
					url: contextPath + "checkout/checkoutInclude.jsp",
					data: queryString + "&paypalErrorMessages=" + paypalErrorMessages,
					dataType: "html",
					processData: false,
					/*async : false,*/
					success: function (data, status, xhr) {
						setTimeout(function () {
							if ($("#load-checkout-norton-script").length) {
								$("[id=load-checkout-norton-script]").html($("#loadNortonHiddenDiv").html());
							}
						}, 4000);
						$("#checkOutInclude").html(data);
						if (paypalErrorMessages != '') {
							var erroeMsgTmp = "Following are the form errors:" + paypalErrorMessages;
							addBackendErrorsToForm(erroeMsgTmp, $('#checkout-container'));
						}
						$("#payPalErrorMessages").html("");
						$("#sign-in-password-input").maskPassword();
						checkDefaultStore();
						if (undefined != $('#isFromSynchrony').val() && undefined != $('#synchronyAppDecision').val() && $('#isFromSynchrony').val() && $('#synchronyAppDecision').val() == 'A') {
							$.ajax({
								type: "POST",
								cache: false,
								async: false,
								dataType: "html",
								url: '/checkout/payment/congratulationModal.jsp',
								data: {},
								success: function (responseData) {
									$('#toysrus-creditcard').html(responseData);
									$('#toysrus-creditcard').modal('show');
								},
								error: function (xhr, ajaxOptions, thrownError) {
									if (xhr.status == '409') {
										cancelExitBrowserConfimation();
										var contextPath = $(".contextPath").val();
										location.href = contextPath + 'cart/shoppingCart.jsp';
									}
								}
							});

						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == '409') {
							cancelExitBrowserConfimation();
							var contextPath = $(".contextPath").val();
							location.href = contextPath + 'cart/shoppingCart.jsp';
						}
					}
				});
				window.contextPath = $('.contextPath').val() || '/';
			}
		}
	});
}


function ajaxCallForSOSPage(contextPath) {
	var queryString = window.location.search.substring(1);
	$.ajax({
		type: "POST",
		url: contextPath + "sos/sosHeader.jsp",
		data: queryString,
		dataType: "html",
		processData: false,
		/*async : false,*/
		success: function (data, status, xhr) {
			$("#sosHeaderFrg").html(data);
		},
		error: function (jqXHR, textStatus, errorThrown) {

		}
	});
	window.contextPath = $('.contextPath').val() || '/';
}

/* Start: TUW-54278 and TUW-54242*/
function alignStoreLocatorPageWithHeader() {
	if ($('.container-fluid.store-locator-template').length > 0) {
		try {
			var headerHeight = $('.header-part.fixed-nav').height() || 106;
			$('.container-fluid.store-locator-template').css('margin-top', (headerHeight - 1) + 'px');
		} catch (e) { }
	}
	if ($('.container-fluid.home-template').length > 0) {
		try {
			var headerHeight = $('.header-part.fixed-nav').height() || 106;
			$('.carousel-margin.zero-margin').css('margin-top', (headerHeight - 1) + 'px');
		} catch (e) { }
	}
}
/* End: TUW-54278 and TUW-54242 */

/* show popup on focus ADA*/
$(document).on('focusin', '.showPopupOnFosus', function () {
	$('.showPopupOnFosus').trigger('mouseleave');
	$(this).trigger('mouseenter');
});

$(document).on('focusin', '.showOnFocus', function () {
	$(this).trigger('mouseenter');
});
$(document).on('focusout', '.showOnFocus', function () {
	$(this).trigger('mouseleave');
});

//product compare
$(document).on("keypress", ".product-compare-checkbox-new", function () {
	$(this).trigger("click");
});


$(document).on("focusin", ".product-block-image .product-image > a", function () {
	$('.product-block-image .product-image > a').siblings('div').removeClass("getFocus-quickView");
	$(this).siblings('div').addClass("getFocus-quickView");
});
$(document).on("focusin", ".getFocus-quickView .product-hover-view > a", function () {
	$(this).parent('.product-quick-view').addClass("focus");
});
$(document).on("focusout", ".getFocus-quickView .product-hover-view > a", function () {
	$(this).parent('.product-quick-view').removeClass("focus");
	$(this).parents('.getFocus-quickView').removeClass("getFocus-quickView");

	// need to add code

});
$(document).on("mouseenter", ".product-block-image", function () {
	$('.product-block-image .product-image > a').siblings('div').removeClass("getFocus-quickView");
});



$(document).on("focusin", ".main-toy-grid toygrid-textover .toy-grid > a.image-grid-link", function () {
	$(this).siblings(".toy-grid-summary").css("style", "visibility: visible;");
});
$(document).on("focusin", ".toy-grid-summary .product-quick-view > a", function () {
	$(this).parent(".product-quick-view").addClass("focus");
});
$(document).on("focusout", ".toy-grid-summary .product-quick-view > a", function () {
	$(this).parent(".product-quick-view").removeClass("focus");
	$(this).parents(".toy-grid-summary").removeAttr("style");
});



$(document).on("keydown", ".gift-checkbox, #same-billing-address-checkbox,#gift-options-checkbox,.checkbox-sm-on, .checkbox-sm-off, .shopping-cart-gift-checkbox-on, .shopping-cart-gift-checkbox-off, .gift-wrap-none, .gift-wrap-tru, #checkboxMsgId, .alternate-pickup-label", function (e) {

	if (e.which == 32 || e.keyCode == 32) {//space bar
		$(this).trigger("click");
	}

});


// PickUp Page accordion

$(document).on("keydown", "#ui-id-1", function () {
	if ($(this).hasClass("ui-state-active")) {
		$(this).find("h3").html("hide items");
		$(this).find(".accordion-expand-icon").css("display", "none");
	} else {
		$(this).find("h3").html("Show items");
		$(this).find(".accordion-expand-icon").css("display", "block");
	}
});


//trigger click

$(document).on("keydown", ".color-block , .size-option,.read-more, .email[data-target='#emailMePopUp'], .zoom-icon,label.gift-both ", function (e) {

	if (e.which == 13 || e.keyCode == 13) {//enter
		$(this).trigger("click");
	}

});


$(document).on('click', '.color-block , .size-option', function () { bruCss(); }); //change link colors in babiesrus

//Chkout... Keyboard access
var _thisId = "";
$(document).on("focusin", ".lineItemQty", function () {

	if (_thisId == "") {
		_thisId = $(this).parents(".stepper").attr("id");
	}
});

$(document).on("keydown", ".increase", function () {

	_thisId = $(this).parents(".stepper").attr("id");

});

$(document).on("focusout", ".lineItemQty", function () {

	_thisId = $(this).parents(".stepper").attr("id");
	$(this).eq(_thisId).siblings(".increase").focus();

});


$(document).on("focusin", "#cartHeaderOverlay a", function () {

	if (_thisId != "") {

		$(".product-information").eq(_thisId).find(".increase").focus();
		_thisId = "";
	}

});


//read more focus
$(document).on("focusin", ".read-more", function () {

	/*$(this).attr("style","background-color: #fff; color: #666;");*/
	$(this).attr("style", "background-color: #29ace1; border: solid 2px #29ace1;color: #ffffff; font-size: 18px;");

});
$(document).on("focusout", ".read-more", function () {

	$(this).removeAttr("style");

});


//removing focus effect on click
$(document).on("click", "div[tabindex='0'],h3.filter-window-header, a, button,#footer .drop-down select, .gift-interest-both + label, .gift-finder-shopping-for-zone .ui-slider-handle, .shop-by-character-block img, .continue-shopping .btn, .checkout-nav-bottom div.we-can-help .btn, .shopping-cart-bottom-nav div.we-can-help .btn, .gift-finder-shopping-for-gender input[type='radio']", function (e) {
	$(".noOutline").removeClass("noOutline");
	$(this).addClass("noOutline");
});
$(document).on("focusout", "div[tabindex='0'], a, button,h3.filter-window-header,#footer .drop-down select, .gift-interest-both + label, .gift-finder-shopping-for-zone .ui-slider-handle, .shop-by-character-block img, .continue-shopping .btn, .checkout-nav-bottom div.we-can-help .btn, .shopping-cart-bottom-nav div.we-can-help .btn, .gift-finder-shopping-for-gender input[type='radio']", function (e) {
	$(this).removeClass("noOutline");
});
$(document).on("mouseleave", "button, .read-more", function () {
	$(this).blur();
});


$(document).on('click', '.pr-write-review-link', function () {
	utag.link({
		event_type: 'write_review'
	});
});

$(document).on('keypress', '#SignUpemail', function (e) {
	if (e.keyCode == 13) {
		$(".savings-sign-up-button").trigger('click');
	}
});
$(document).on('show.bs.modal', '#signInModal', function () {
	$(this).find("input[type=text]").val("");
});

function forConsoleErrorFixing(ele) {
	try {
		if ($(ele) && $(ele).attr("href") && $(ele).attr("href").indexOf("void") > 0) {
			$(ele).removeAttr("href");
		}
		setTimeout(function () {
			$(ele).attr("href", "javascript:void(0);");
		}, 1000);
	}
	catch (e) { }
}

$("#invodoModalButton").click(function () {
	var videoContent = $.trim($("#invodoModalButton").html()).length;
	if (videoContent > 0) {
		utag.link({
			event_type: 'video_init'
		});
	}
});

/*omniture integration implementation*/

function omniLoginScript(email, customer_id, partner_name) {
	if (typeof (utag) != "undefined") {
		utag.link({
			event_type: 'login',
			customer_email: email,
			customer_id: customer_id,
			partner_name: partner_name
		});
	}
}

function omniLoginFail() {
	if (typeof (utag) != "undefined") {
		utag.link({
			event_type: 'login_failed'
		});
	}
}

function omniEmailSignUP(email, customerID, partnerName) {
	console.log('calling omniture for email signup');
	if (typeof utag != 'undefined') {
		utag.link({
			event_type: 'email_signup',
			partner_name: partnerName,
			customer_email: email,
			customer_id: customerID
		});
	}
	else {
		console.log('utag is ' + typeof utag);
	}
}

function getCurrentDateInDDMMYYYY() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //January is 0!

	var yyyy = today.getFullYear();
	if (dd < 10) {
		dd = '0' + dd
	}
	if (mm < 10) {
		mm = '0' + mm
	}
	var today = dd + '/' + mm + '/' + yyyy;
	return today;
}

function ajaxCallForTealium(contextPath) {
	var urlFullContextPath = $('.urlFullContextPath').val() || contextPath || '/';
	$.ajax({
		type: "POST",
		url: urlFullContextPath + "common/tealiumForHomePage.jsp",
		dataType: "html",
		processData: false,
		/*async : false,*/
		success: function (data) {
			$("#tealiumContent").html(data);
		},
		error: function () {

		}
	});
}

$("#sub-category-narrow-by-button").on("click", function () {
	$(".accordion-content-active").each(function () {
		if ($(this).css('display') == 'block') {
			if ($(this).prev("h3.filter-window-header").find("span.ui-accordion-header-icon").hasClass("ui-icon-plus")) {
				$(this).prev("h3.filter-window-header").find("span.ui-accordion-header-icon").removeClass("ui-icon-plus").addClass("ui-icon-minus");
			}
		}
	});
});

/* As per issue TSJ-9173.this event was not triggerring because megamenu functionality is changed.*/
function tealiumMegaMenuClick() {
	if ($('#exploreButton').find('em').hasClass('sprite sprite-hamburger-icon')) {
		console.log("clicked megamenu");
		utag.link({
			'event_type': 'shop_by_mega_menu'
		});
	}

}
/* Start:Fix for TUW-52773 */
$(document).on("input", "#promo-code-payment-page", function () {
	var value = $(this).val();
	if (value != '') {
		$('#promo-code-payment-page-btn').removeAttr('disabled');
	}
})
/* End:Fix for TUW-52773 */

function returnFalse() {
	return false;
}

/*###########Registry Loggedin User Session and Cookies########*/
function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}
testRegLoggedinUser();
function testRegLoggedinUser() {
	var lastAccessedRegistryID = getCookie("lastAccessedRegistryID");
	var registry_id = getCookie("REGISTRY_USER");

	if ((typeof registry_id != 'undefined' && registry_id != 'null' && registry_id != '') && (typeof lastAccessedRegistryID != 'undefined' && lastAccessedRegistryID != 'null' && lastAccessedRegistryID != '')) {
		$("#my-baby-reg").find("a").find("span.label-text").html("my baby registry");
	}

}
/*##################################*/

/* TRIAD checks */

function triAdZIndexChange() {
	$('.adzonne-container, .sosAddZone, .ads_container, [class^=ad-zone]').css('z-index', '99');
	$('.sosAddZone').off('change');
}

$(window).scroll(triAdZIndexChange);

$(document).on('mouseenter', '.sosAddZone', function () {
	$(this).on('change', triAdZIndexChange);
});

/* END TRIAD checks */

//For quick view modal background not greying out properly after laoding the image. If you want to use this for other modals, make this generic or contact: Vaibhav
var quickviewModalInterval;
(function () {
	$("body").on("shown.bs.modal", "#quickviewModal", function () {
		quickviewModalInterval = setInterval(function () {
			if ($("[id=quickviewModal]").find(".modal-backdrop").height() < $("[id=quickviewModal]").find(".modal-dialog").outerHeight(true)) {
				$(window).trigger("resize");
			}
		}, 250);
	});
	$("body").on("hide.bs.modal", "#quickviewModal", function () {
		clearInterval(quickviewModalInterval);
		console.log(quickviewModalInterval);
	});


})();

/* START abandonCart EMAIL-ID CHECK */

function abandonCartEmailIdChek() {

	var visitorFirstName = $.cookie("VisitorFirstName");
	var visitorUsaFullName = $.cookie("VisitorUsaFullName");

	if (document.getElementById("welcomeEmail")) {
		if (!isEmpty(visitorFirstName) && !isEmpty(visitorUsaFullName)) {
			document.getElementById("welcomeEmail").innerHTML = visitorUsaFullName;
		}
	}
}

/* END abandonCart EMAIL-ID CHECK */

function layawayHideDisplayName() {
	alert("no display name");
}

/* Added for omniture */
function omniCancelOrder() {
	utag.link({
		event_type: 'order_cancel'
	});
}
/*JS start for SOS header display*/
$(window).load(function () {
	var isSOS = $.cookie("isSOS");
	var sites = $("#sites").val();
	var contextPath = $('.contextPath').val();
	if (typeof isSOS != 'undefined' && isSOS != null && isSOS == 'true') {
		$("#sosBanner").hide();
		$("#sosBanner").parent().find('.default-margin.sos-banner-bottom').css('margin-top', '110px');
		$(".SosSeoAddZone").hide();
		$("#sosAdd_Zone").hide();
		$("#sosCmsAddZone").hide();
		$(".sosAddZone2").hide();
		$(".sosAddZone3").hide();
		$(".header-part").addClass("SOSHeader");
		$("#sosMyAccount").hide();
		$("#sosPdpAddZone1").hide();
		$("#sosPdpAddZone2").hide();
		$("#sosPdpAddZone3").hide();
		$(".sosAddZone").hide();
		ajaxCallForSOSPage(contextPath);
	} else {
		$("#sosBanner").show();
		$(".SosSeoAddZone").show();
		$("#sosAdd_Zone").show();
		$("#sosCmsAddZone").show();
		$(".sosAddZone2").show();
		$(".sosAddZone3").show();
		$("#sosMyAccount").show();
		$("#sosPdpAddZone1").show();
		$("#sosPdpAddZone2").show();
		$(".sosAddZone").show();
	}
});
/*JS end for SOS header display*/

function validateCreditCardNumber(value) {
	// accept only digits, dashes or spaces
	if (/[^0-9-\s]+/.test(value))
		return false;
	// The Luhn Algorithm. It's so pretty.
	var nCheck = 0, nDigit = 0, bEven = false;
	value = value.replace(/\D/g, "");
	for (var n = value.length - 1; n >= 0; n--) {
		var cDigit = value.charAt(n), nDigit = parseInt(cDigit, 10);
		if (bEven) {
			if ((nDigit *= 2) > 9)
				nDigit -= 9;
		}
		nCheck += nDigit;
		bEven = !bEven;
	}
	if ((nCheck % 10) == 0) {
		var firstSixDigitCardNo = value.substr(0, 6)
		var isValidCard = false;
		$.ajax({
			type: "POST",
			async: false,
			dataType: "html",
			url: window.contextPath + "checkout/intermediate/billingAjaxFormSubmit.jsp",
			data: 'formID=validateCreditCarBinRanges&cardNumber=' + firstSixDigitCardNo,
			success: function (data) {
				if (data.indexOf('Following are the form errors:') > -1) {
					isValidCard = false;
				} else {
					isValidCard = true;
				}
			}
		});
		return isValidCard;
	} else {
		return false;
	}
}

function omniIspuOutOfStock(storeId) {
	if (typeof utag != 'undefined') {
		utag.link({
			event_type: 'ispu_out_of_stock',
			'selected_store': storeId
		});
	}
}

/*Passing ispu_store_code and store_locator as fix for GEOFUS-1040 and GEOFUS-1041*/
function omniIspuStoreServed(storeId) {
	if (typeof utag != 'undefined') {
		utag.link({
			event_type: 'ispu_store_served',
			selected_store: storeId,
			ispu_store_code: storeId,
			store_locator: storeId
		});
	}
}
function signInForTealium() {
	if (typeof utag !== undefined) {
		var isJustLoggedIn = localStorage.getItem('justLoggedIn');
		console.log('Logged In::' + isJustLoggedIn);
		if (typeof isJustLoggedIn == 'string' && (isJustLoggedIn.indexOf('yes') > -1)) {
			var customerEmail = $('#customerEmailSignIn').val();
			var customerID = $('#customerIDSignIn').val();
			var partnerName = $('#partnerNameSignIn').val();
			utag.link({
				event_type: 'login',
				customer_email: customerEmail,
				customer_id: customerID,
				partner_name: partnerName
			});
		}
		localStorage.setItem("justLoggedIn", '');
	}
}
function signOutForTealium() {
	if (typeof utag !== undefined) {
		utag.link({
			event_type: 'logout'
		});
	}
}
$(document).ready(
	function () {
		var contextPath = $(".contextPath").val();
		TRUImagePath = $('.TRUImagePath').val();
		var headerLink = window.location.href;
		/* This part will be loaded on document.ready only when my account page is loading. */
		if (headerLink.indexOf("myaccount/myAccount.jsp") > -1) {
			var visitorFirstName = $.cookie("VisitorFirstName");
			if (typeof visitorFirstName !== 'undefined' && visitorFirstName !== null) {
				ajaxCallForMyAccountLandingPage(contextPath);
				if (($(".my-account-template").length > 0)) {
					loadorderHistoryOrders(false);
				}
			} else {
				$("#akamai_myAccount_landing_page").hide();
				$("#akamai_login_page").show();
			}
		}

		var slDetailsInfo = $("#storelocater-lernmorelinkDetails").val();
		/* LearnMore Tool Tip*/
		//setTimeout(function(){
		var slLearnmoreTooltip = $('.store-locator-content .sl-learnmore');

		var slLearnmorePopoverTemplate = '<div  class="popover myclass-popover detailWrapper paypalDdetailsWrapper" role="tooltip"><div class="arrow"></div><p>' + slDetailsInfo + '</p></div>';

		slLearnmoreTooltip.click(function (event) {
			event.preventDefault();
		});

		slLearnmoreTooltip.popover({
			animation: 'true',
			html: 'true',
			content: 'asdfasdf',
			placement: 'bottom',
			template: slLearnmorePopoverTemplate,
			trigger: 'hover'
		});
		//}, 500);
		/*js to get recommended product ids */
		var pageName = $('.pageName,#pageName').val();

		var count = 0;
		var myFunction = setInterval(function () {
			var productIdArr = [];
			/*var productString = "";*/
			$('#tproduct_rr,#cart_rr,#bproduct_rr').find('.quick-view-container').find('.product-quick-view').each(function () {
				var productId = '';
				if (typeof $(this).attr('id') != 'undefined') {
					productId = $(this).attr('id').split('|')[1];
				}
				productIdArr.push(productId);
			});
			if (typeof utag_data != 'undefined' && typeof utag_data.xsell_product != 'undefined' && productIdArr.length != 0) {
				utag_data.xsell_product = productIdArr.toString();
			}

			if (productIdArr.length !== 0) {
				clearInterval(myFunction);
			}
			else if (count > 5) {
				clearInterval(myFunction);
			}
			count++;
		}, 2000);
		/*   js to get recommended product ids */
		var tealiumcheck = setInterval(function () {
			if (typeof utag !== "undefined") {
				checkWelcomeBackShoppingPopup();
				clearInterval(tealiumcheck);
			}
		}, 2000);
		$(document).on('cut copy', "input[name*='maskedPassword']", function (evt) {
			return false;
		});
	});
(function () {
	var beforePrint = function () {
		if ($('.product-details-template').length > 0) {
			try {
				if ($(".pr-snapshot-body .pr-review-points-attr-wrapper") && $(".pr-snapshot-body .pr-review-points-attr-wrapper").length > 0) {
					$(".pr-snapshot-body .pr-review-points-attr-wrapper div.pr-attribute-group:last").after('<div class="ratingForPrint"></div>');
					$(".pr-snapshot-body .pr-review-points-attr-wrapper div.ratingForPrint").append($('#pr-snapshot-histogram-container').html());
					$(".pr-snapshot-body .pr-review-points-attr-wrapper div.ratingForPrint").css('float', 'left');
				}
			}
			catch (e) { }
		}
	}
	var afterPrint = function () {
		try {
			if ($(".pr-snapshot-body .pr-review-points-attr-wrapper div.ratingForPrint") && $(".pr-snapshot-body .pr-review-points-attr-wrapper div.ratingForPrint").length > 0) {
				$(".pr-snapshot-body .pr-review-points-attr-wrapper div.ratingForPrint").remove();
			}
		}
		catch (e) { }
	}
	if (window.matchMedia) {
		var mediaQueryList = window.matchMedia('print');
		mediaQueryList.addListener(function (mql) {
			if (mql.matches) {
				beforePrint();
			} else {
				afterPrint();
			}
		});
	}

	window.onbeforeprint = beforePrint;
	window.onafterprint = afterPrint;
}());

/*Start Preventing Ajax call for double click*/
var globalAjaxUrl = "";
var globalAjaxData = "";
$(document).ajaxSend(function (event, jqxhr, settings) {
	if (settings.url == globalAjaxUrl) {
		if (globalAjaxData == settings.data) {
			jqxhr.abort();
			return
		}
	}
	globalAjaxUrl = settings.url;
	globalAjaxData = settings.data;
});
$(document).ajaxComplete(function (event, request, settings) {
	globalAjaxUrl = "";
	globalAjaxData = "";
});

/*End Preventing Ajax call for double click*/

/*script for event6 for tealium*/
$(document).on("click", "#tproduct_rr .product-block-image-container a, #tproduct_rr .product-block-information a,#bproduct_rr .product-block-image-container a, #bproduct_rr .product-block-information a,#cart_rr .product-block-image-container a, #cart_rr .product-block-information a,#tmyaccount_rr .product-block-image-container a,#tmyaccount_rr .product-block-information a,#tnosearch_rr .product-block-image-container a,#tnosearch_rr .product-block-information a,#tcategory_rr .product-block-image-container a,#tcategory_rr .product-block-information a", function () {
	var productId = $(this).closest(".product-block").find(".product-quick-view").attr("id").split("|")[1];
	utag.link({ "event_type": "xsell_clicked", "xsell_product": productId });
});
/*script for event6 for tealium*/

//Logging ajax Errors to server
$(document).ajaxError(function (event, request, settings) {
	var failedRequestURL = settings.url;
	var requestStatus = request.status + " " + request.statusText;
	var contextPath = $('.urlFullContextPath').val() || $('.contextPath').val() || '/';

	if (failedRequestURL != contextPath + 'common/ajaxErrorLogs.jsp') {
		$.ajax({
			type: "POST",
			url: contextPath + 'common/ajaxErrorLogs.jsp',
			data: { "requestURL": failedRequestURL, "status": requestStatus }
		});
	}

});

function makeAjaxHeader(param) {

	var reqestData = (typeof (param.data.formSerialize) != 'undefined') ? (param.data.formSerialize + '&formID=' + param.data.formID) : ({ formID: param.data.formID });
	$.ajax({
		type: "POST",
		dataType: "html",
		async: false,
		cache: false,
		data: reqestData,
		url: param.url,
		success: function (data) {

			console.log(param.data.formID);

			switch (param.data.formID) {
				case 'membershipFormFromHeader':
					if (data.indexOf('Following are the form errors:') > -1) {
						setTimeout(function () {
							addBackendErrorsToForm(data, $("#headerMembershipForm"));
						}, 150);
					} else {
						$('.my-account-popover-struct .my-account-logged-in').find('#membership-number').remove();
						$('.my-account-popover-struct .my-account-logged-in').find('#membership-id').remove();
						$('.my-account-popover-struct .my-account-logged-in').find('hr').after(data);
						$('#myAccountRewardsSecId').load('/myaccount/myAccountRewardsRus.jsp');
						bruCss();
					}
					break;

				case 'myAccountPopover':
					$('#my-account-popover-struct').html(data);
					if ($('#my-account-popover-struct').css('display') === 'none' && $('#exploreButton').hasClass('active')) {
						$('#exploreButton').click();
					}
					$("#my-account-popover-struct").toggle();
					$("#rewardsHdrAddUpdate li").hide();
					bruCss();
					break;

				default:
					console.log('Ajax: default case.');
					break;
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				window.event.cancelBubble = true;
				var redirectionUrl = xhr.getResponseHeader('Referer');
				var contextPath = $(".contextPath").val();
				$(document).find("[data-toggle=modal]").each(function () {
					$(this).attr("data-target", "");
				});

				//setTimeout(function(){
				if (redirectionUrl != '' && redirectionUrl != null && redirectionUrl != 'undefined' && (!($('#VisitorFirstNamecookieDIV').html() == 'my account')) || redirectionUrl.indexOf('orderConfirmation.jsp') > -1) {
					if (redirectionUrl.indexOf('?') > -1) {
						redirectionUrl = redirectionUrl + '&sessionExpired=true';
					} else {
						redirectionUrl = redirectionUrl + '?sessionExpired=true';
					}

					if (redirectionUrl.indexOf('orderConfirmation.jsp') > -1) {
						redirectionUrl = contextPath;
					}

					if (redirectionUrl.indexOf('orderHistory.jsp') > -1) {
						redirectionUrl = redirectionUrl.replace('orderHistory.jsp', 'myAccount.jsp');
					}
					location.href = redirectionUrl;
				} else {
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'myaccount/myAccount.jsp?sessionExpired=true';
				}
				//},300);
			}
			return false;
		}
	});
}

$('#searchText').on('click', function () {
	if ($('#my-account-popover-struct').css('display') === 'none' && $('#exploreButton').hasClass('active')) {
		$('#exploreButton').click();
	}
});

function ajaxCallForPLPBreadcrumbsCookie() {

	var contextPath = $(".contextPath").val() || '/';

	var urlPagename = location.pathname.substring(1);
	var urlAB = getParameterByName('ab');
	var urlCatID = getParameterByName('categoryid');
	var cat = getParameterByName('cat');
	var uri = contextPath + 'jstemplate/allCategoryIdMapDetails.jsp';
	$.ajax({
		type: 'POST',
		url: uri,
		data: { urlPagename: urlPagename, urlAB: urlAB, urlCatID: urlCatID, cat: cat },
		async: false,
		success: function (responseData) {
			if ($.trim(responseData) != '') {
				if ($('.plp-update-breadcrumb').length > 0) {
					$('.plp-update-breadcrumb').remove();
				}
				$('.category-breadcrumb li:last').after(responseData);
				/*$('.category-breadcrumb li:first').css({'margin-right':'24px'}); commented because issue in spacing*/
			}
			//$(".category-breadcrumb").html()
		},
		error: function () {
			console.log("Error");
		}
	});

	$('.category-template .category-breadcrumb, .sub-category-template .category-breadcrumb, .template-family .category-breadcrumb').show();
}

$(document).on('shown.bs.modal', function (e) {
	try {
		var targetID = '#' + e.target.id;
		var lastAnchor = $(targetID + ' a:last:not([data-dismiss="modal"])');
		if ((targetID == "#emailMePopUp")) {
			lastAnchor = $(targetID + ' #emailToFriend button');
		}
		if ((targetID == "#shipping-page-suggested-address") || (targetID == "shipping-page-suggested-address")) {
			lastAnchor = $(targetID + ' button#continueWithSugestedAddr');
		}
		var lastInput = $(targetID + ' input:last');
		var modalOpen = lastAnchor.length ? lastAnchor : lastInput;
		$(document).on('keydown', modalOpen.selector, function (e) {
			var keyCode = e.keyCode || e.which;
			if (keyCode == 9) {
				$(targetID + ' a[data-dismiss="modal"]')[0].focus();
				return false;
			}
		});

		if ((targetID == "#quickviewModal") && ($(targetID).find(".warnRegistryPopover").length > 0)) {
			enableWarnRegistryPopover();
		}
	}
	catch (e) { }

});
function tabBindingCheckoutEditAddress(modalId) {
	var targetID = $('#' + modalId).closest('.modal').attr('id');
	lastAnchor = $('#' + targetID + ' button#continueWithSugestedAddr');
	$(document).on('keydown', '#' + targetID + ' a:last', function (e) {
		var keyCode = e.keyCode || e.which;
		if (keyCode == 9) {
			$(lastAnchor.selector)[0].focus();
			return false;
		}
	})
	$(document).on('keydown', lastAnchor.selector, function (e) {
		var keyCode = e.keyCode || e.which;
		if (keyCode == 9) {
			$('#' + targetID + ' a[data-dismiss="modal"]')[0].focus();
			return false;
		}
	});
}

/*Fix for TUW-65317 starts */
$(".button-container #createAccountSubmitBtn").mouseenter(function () {
	$(this).css({ "background-color": "white", color: "#009DDB" });
}).mouseleave(function () {
	$(this).css({ "background-color": "#009DDB", "color": "white" });
});
/*Fix for TUW-65317 ends */
window.addEventListener('message', function (event) {
	if (event.data == "TRUSigninReload") {
		localStorage.setItem('justLoggedIn', 'yes');
		window.location.reload();
	}
	if (event.data == "forgotPasswordIframe") {
		$("#signInModal").modal('hide');
		$("#forgotPasswordModel").modal('show');
	}
}, false);
$("body").on("hidden.bs.modal", "#signInModal", function () {
	var iframeUrl = '';
	if(document.getElementById('secureSigninIframe') != null){
		iframeUrl = document.getElementById('secureSigninIframe').src;
		document.getElementById('secureSigninIframe').src = "";
	}
	$('#secureSigninIframe').attr("data-src", iframeUrl);
});
$("body").on("shown.bs.modal", "#signInModal", function () {
	var iframeUrl = $('#secureSigninIframe').attr("data-src");
	if(document.getElementById('secureSigninIframe') != null){
		document.getElementById('secureSigninIframe').src = iframeUrl;	
	}
});
$(window).load(function () {
	setTimeout(function () {
		var pageName = $(".pageName").val();
		if (pageName == "pdpPage") {
			if (!$('#wc-power-page').find("#wc-aplus").length) {
				$(".from-the-manufacturer").hide();
			}
			if($("h2.no-enhanced-content").length > 0) {
				$("#wc-read-button a").click();
			}
		}
	}, 600);
})
function doSignOut(clikedID) {
	var domainName = $('.urlFullContextPath').val() || '/';
	$.ajax({
		headers: {
			'X-APP-API_KEY': 'apiKey',
			'X-APP-API_CHANNEL': 'webstore'
		},
		url: domainName + 'rest/model/atg/userprofiling/ProfileActor/logoutService?pushSite=ToysRUs',
		type: 'POST',
		dataType: 'json',
		xhrFields: {
			'withCredentials': true
		},
		success: function (responseData) {
			if (responseData.hasOwnProperty('loggedOut') && responseData.loggedOut != null && responseData.loggedOut != '' && responseData.loggedOut == true) {
				if(location.host.indexOf('.babiesrus.com') > -1 || clikedID == 'LayawayLogoutNotYouId'){
					window.location.reload();
				} else if(clikedID == 'logoutNotYouId') {
					location.href = domainName + "myaccount/myAccount.jsp";
				} else {
					location.href = domainName;
				}
			}
			else {
				console.log("Logout ajax else: " + responseData);
			}
		},
		error: function (errorData) {
			console.log("Logout ajax fail: " + errorData);
		}
	});
}

function initTRUComponents() {
	//TRU Green Rounded Checkbox
	$(document).on('click', '.tru-green-round-checkbox', function () {
		$(this).find('.checkout-flow-sprite').toggleClass('checkout-flow-radio-selected');
	});

	//TRU Floating Label
	$(document).on('input focus', '.tru-float-label-input', function() {
		var $field = $(this).parents('.tru-float-label-field');
		if (this.value) {
			$field.addClass('tru-float-label-field-not-empty input-not-empty');
		} else {
			$field.removeClass('tru-float-label-field-not-empty input-not-empty');
		}
	});
	$(document).on('blur', '.tru-float-label-input', function() {
		$(this).parents('.tru-float-label-field').removeClass('tru-float-label-field-not-empty');
	});
	$('.tru-float-label-input').each(function() {
		var $field = $(this).parents('.tru-float-label-field');
		if (this.value) {
			$field.addClass('tru-float-label-field-not-empty input-not-empty');
		} else {
			$field.removeClass('tru-float-label-field-not-empty input-not-empty');
		}		
	});
}