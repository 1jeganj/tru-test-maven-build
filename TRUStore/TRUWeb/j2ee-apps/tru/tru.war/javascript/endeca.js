//The gift finder module function

var TRU = (function ($, truMod) {
	var giftFinderForm = $('#giftFinderForm');
	truMod.giftFinder = giftFinderForm;
	truMod.giftFinder.clearAll = function () {
		giftFinderForm.find('input[type=hidden]').val(0);
		giftFinderForm.find('input[type=text]').val('');
		giftFinderForm.find('.gift-finder-shopping-for-gender input[type=radio]').prop('checked', false);
		//giftFinderForm.find('.ui-slider').slider('value',0,0);
		giftFinderForm.find('#giftFinderName').val('');
		giftFinderForm.find('#female').attr('checked', false);
		giftFinderForm.find('#male').attr('checked', false);
		giftFinderForm.find('#both').attr('checked', false);
		giftFinderForm.find('.ui-slider').find('.ui-slider-handle').css('left', '0%');
		giftFinderForm.find('.ui-slider').find('.ui-slider-range').css('width', '0');
		giftFinderForm.find('#price-slider').find('#minAge, #maxAge').html('0 years');
		giftFinderForm.find('#maxAge , #maxPrice').css('display', 'none');
		giftFinderForm.find('#price-slider').find('#minPrice, #maxPrice').html('$0');
		giftFinderForm.find('.gift-interest-selection').attr('checked', false);
		giftFinderForm.find('#giftFinderTotal').html('0');
	},
		truMod.giftFinder.checkSearchKeyAvailability = function (keyValue) {
			var giftFinderNameVal = keyValue.trim().toLowerCase(), keysArray = [];
			if (giftFinderNameVal, length <= 0) return false;
			$('#recentSearchList').find('li > a').each(function () {
				keysArray.push($(this).html().toLowerCase());
			});
			if (keysArray.indexOf(giftFinderNameVal) != -1) {
				return true;
			};
			return false;
		}
	return truMod;
})(jQuery, TRU || {});

jQuery(document).ready(function () {

	$(document).on('change', '#giftNameOption', function () {
		location.href = $(this).val();
	});

	$(document).on('click', '#deleteThisFinder', function () {
		var giftFinderNxtOpt = $('#giftNameOption option:first'),
			giftFinderNxtOptVal = $('#giftNameOption option:not([selected]):eq(0)').val();
		//giftFinderNxtOpt.remove();
		var url = $('#giftNameOption').val();
		var finderKey = $('#giftNameOption option:selected').text();

		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: { formId: 'giftFinderKey', fiderKey: finderKey },
			url: '/giftfinder/ajaxIntermediateGiftFinder.jsp',
			async: false,
			success: function (data) {
				$('#gift').html(data);
				location.href = (typeof giftFinderNxtOptVal === "undefined") ? "/home" : giftFinderNxtOptVal;
			}
		});

	});

	$(document).on('click', '#giftSubmit', function () {

		var dUrl = "giftFinder?",
			hiddenMaxAge = 0,
			hiddenMaxPrice = 0;

		var giftFinderName1 = $('#giftFinderName').val().trim();

		var gender = '';
		var nValues = [], urlBuilder = [];

		if ($('.gift-finder-shopping-for-gender input:checked').length > 0) {
			gender = $('.gift-finder-shopping-for-gender input:checked').attr('id');
		}

		var priceagesection = '';

		if ($('#hiddenMaxPrice').val() != "0" && $('#hiddenMaxAge').val() != "0") {
			priceagesection = "Nf=sku.activePrice|BTWN+" + $('#hiddenMinPrice').val() + "+" + $('#hiddenMaxPrice').val() + "|minAge|BTWN+" + $('#hiddenMinAge').val() + "+" + $('#hiddenMaxAge').val();
			hiddenMaxPrice = $('#hiddenMaxPrice').val();
		}

		if ($('#hiddenMaxAge').val() != "0") {
			if ($('#hiddenMinPrice').val() == "0" && $('#hiddenMaxPrice').val() == "0") {
				priceagesection = "Nf=minAge|BTWN+" + $('#hiddenMinAge').val() + "+" + $('#hiddenMaxAge').val();
			}
			hiddenMaxAge = $('#hiddenMaxAge').val();
		}

		if ($('#hiddenMaxPrice').val() != "0") {
			if ($('#hiddenMinAge').val() == "0" && $('#hiddenMaxAge').val() == "0") {
				priceagesection = "Nf=sku.activePrice|BTWN+" + $('#hiddenMinPrice').val() + "+" + $('#hiddenMaxPrice').val();
			}
			hiddenMaxPrice = $('#hiddenMaxPrice').val();
		}

		//alert(priceagesection);

		/*if (priceagesection !='' && gender != '') {
			//dUrl += "keyword=" + giftFinderName1 + "&Nr=AND(product.gender:" + gender + ")" + "&" + priceagesection;
			dUrl += "Nr=AND(product.gender:" + gender + ")" + "&" + priceagesection;
			
		} else if(priceagesection =='' && gender != '') {
			//dUrl += "keyword=" + giftFinderName1 + "&Nr=AND(product.gender:" + gender + ")";
			dUrl += "Nr=AND(product.gender:" + gender + ")";
			
		} else if(priceagesection !='' && gender != '') {
			dUrl += "Nr=AND(product.gender:" + gender + ")" + "&" + priceagesection;
			
		} else if (priceagesection !='' && gender == '') {
			//dUrl += "keyword=" + giftFinderName1 + "&" + priceagesection;
			dUrl += priceagesection;
			
		} else if (priceagesection !='' && gender == '') {
			dUrl += priceagesection;
			
		} else if (priceagesection =='' && gender == '')	{
		    //dUrl += "keyword=" + giftFinderName1;
			
		} else if (priceagesection =='' && gender != ''){
			dUrl += "Nr=AND(product.gender:" + gender + ")";
			
		}*/

		if (gender != '') {
			urlBuilder.push("Nr=AND(product.gender:" + gender + ")");
		}
		if (priceagesection != '') {
			urlBuilder.push(priceagesection);
		}

		$('.gift-finder-options-container').find('input[type=checkbox]:checked').each(function () {
			nValues.push($(this).val());
		});
		//alert(urlBuilder.length);
		if (nValues.length) {
			urlBuilder.push('N=' + nValues.join('+'));
		}

		if (urlBuilder.length <= 0) {
			$("#giftFinderTotal").html(0);
			return;
		}

		var giftFCount = parseInt($('body').find('#giftFinderTotal').text());
		//alert(dUrl);
		if (isNaN(giftFCount) || giftFCount == 0)
			return;

		//if( TRU.giftFinder.checkSearchKeyAvailability(giftFinderName1) || ( giftFinderName1.length <= 0 && hiddenMaxPrice == 0 && hiddenMaxAge == 0 && gender == "")) return false;
		if (hiddenMaxPrice == 0 && hiddenMaxAge == 0 && gender == "" && nValues.length <= 0) return;
		// console.log(giftFinderName1, gender, priceagesection, hiddenMaxPrice, hiddenMaxAge);
		// alert("form submitted");
		// return;
		//alert('submitted');
		//return;
		dUrl += urlBuilder.join('&');
		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: { formId: 'giftFinderName', giftFinderName: giftFinderName1, giftFinderUrl: dUrl },
			url: '/giftfinder/ajaxIntermediateGiftFinder.jsp',
			async: false,
			success: function (data) {
				$('#gift').html(data);
			}
		});

		/* End: Persisting the giftFinderNameURL based on key */
		//alert(dUrl);
		location.href = dUrl;

	});
	var search_placeholder = "";
	$(document).on('focus', '#searchText', function () {
		$('#searchErrorMessage').hide();
		if (search_placeholder == "") {
			search_placeholder = $(this).attr("placeholder");
		}
		$(this).attr('placeholder', '');
	});
	$(document).on('blur', '#searchText', function () {
		if ($(this).val().trim() == "") {
			$('#searchErrorMessage').hide();
			$(this).attr('placeholder', search_placeholder);
		}
	});

	if ($('#slider-image-block-container').length > 0) {
		var imageBlockContainer = $('#slider-image-block-container #slides-image-block');
		if (typeof imageBlockContainer[1] != 'undefined') {
			var imgCount = $(imageBlockContainer[1]).find('.hover-hover').length;
			if (imgCount < 7) {
				$('#slider-image-block-container #multi-carousel-left-arrow').attr('id', '#multi-carousel-left-arrow-hide');
				$('#slider-image-block-container #multi-carousel-right-arrow').attr('id', '#multi-carousel-left-arrow-hide');
				$('#slider-image-block-container #multi-slide-bullets').html('');
			}
		}
	}

	//Clear all in gift finder in toys R Us

	$('body').on('click', '#clear-recent-searches', function () {
		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: { formId: 'clearAll' },
			url: '/giftfinder/ajaxIntermediateGiftFinder.jsp',
			async: false,
			success: function (data) {
				$('.gift-finder-recent-searches').hide();
			}
		});
	});

	$('body').on('click', '#clearGiftFinder', TRU.giftFinder.clearAll);

	$('body').on('blur', '#giftFinderName', function () {
		if (TRU.giftFinder.checkSearchKeyAvailability($(this).val())) {
			$('#errorGiftFinderDuplicate').show();
		}
	});
	$('body').on('focus', '#giftFinderName', function () {
		$('#errorGiftFinderDuplicate').css('display', 'none');
	});
	$('body').on('keyup', '#giftFinderName', function () {
		if ($(this).val().length <= 0) {
			$('body').find('#giftFinderTotal').html('0');
		}
	});
	$('.gift-interest-option').each(function () {
		$(this).find('.gift-finder-checkbox-container:gt(1)').addClass('hide');
	});
	/* gift finder stack implimentaion */
	$('body').on('click', '.gift-interest-refresh', function () {
		var $giftoptContainer = $(this).next(),
			optlen = $giftoptContainer.find('.gift-finder-checkbox-container').length,
			showIndex = $giftoptContainer.find('.gift-finder-checkbox-container:not(.hide)').index();
		//alert(showIndex);
		$giftoptContainer.find('div.interestCheck').hide();
		$(this).parent().find('.gift-both, .gift-interest-or').show();
		if (optlen == 2) return;
		$giftoptContainer.parent().find('.gift-finder-checkbox-container').addClass('hide');
		if (showIndex == optlen - 1 || showIndex == optlen - 2) {
			$giftoptContainer.find('.gift-finder-checkbox-container:lt(2)').removeClass('hide');
		} else {
			$giftoptContainer.find('.gift-finder-checkbox-container').each(function (index, ele) {
				if (index == showIndex + 2 || index == showIndex + 3) {
					$(this).removeClass('hide');
					//return false;													
				}
			});
		}
		if ((showIndex == optlen - 2 || showIndex == optlen - 3) && (optlen % 2 !== 0))
			$(this).parent().find('.gift-both, .gift-interest-or').hide();
	});

	$('body').on('click', '.all-products-sec > li', function () {
		var sortOpt = $(this), sortId = sortOpt.find('a').text();
		//sortOpt.parent().find('li').removeClass('active');
		//sortOpt.addClass('active');
		$('.all-products-sec > li').removeClass('active');
		$('.all-products-sec > li').find('a').filter(function () {
			return ($(this).text() == sortId);
		}).parent().addClass('active');
		$('.product-categories > div').removeClass('active');
		$('.product-categories').find('#' + sortId).addClass('active');
	});

	$(document).on('click', '.toy-grid-summary .quickview-container', function () {
		location.href = $(this).data('href');
	});
});
/*JS start For Add to cart btn display functionality on product in PLP page for spak B&S changes*/
$(document).on('mouseover', '.productBlock .product-block', function (evt) {
	var modalDisplay = $("#findInStoreModal").css('display');
	if (modalDisplay == 'block') {
		$(this).css('z-index', '9999');
	}
	else {
		$(this).removeAttr('style');
	}
	$(this).addClass('productBlock-hover');
	$(this).find('.addtocart-container').show();
});
$(document).on('mouseleave', '.productBlock .product-block', function () {
	$(this).removeClass('productBlock-hover');
	$(this).find('.addtocart-container').hide();
});
$(document).ready(function () {
	//$('.compare-menu').offcanvas({disableScrolling: false});
	//$('#sub-category-narrow-by-button').trigger('click');
	$('#sub-category-narrow-by-button').on('click', function (e) {
		if ($('.compare-menu').hasClass('in')) {
			$('#filtered-products').removeClass('productBlock-withFilter');
			$(this).addClass('filters-closed');
			$('.compare-menu').animate({
				'left': '-9999px',//'left': '-350px',
				'display': 'none'
			}, 'slow');
			$('.compare-menu').removeClass('in');
			$("#sub-category-narrow-by-button").find('.narrow-by-button-label').removeClass('selected');
			if ($('.search-template').length > 0) {
				$('#filtered-products').animate({ 'width': '108.3%', 'left': '-5.3%' }, 'slow');

			}
			else {
				$('#filtered-products').animate({ 'width': '104%', 'left': '-1%' }, 'slow');//$('#filtered-products').animate({'width':'100%','left':'0'},'slow');
			}
			dynamicImageSize();
		}
		else {
			$('#filtered-products').addClass('productBlock-withFilter');
			$(this).removeClass('filters-closed');
			$('.compare-menu').animate({
				'left': '0px',
				'display': 'block'
			}, 'slow');
			$('.compare-menu').addClass('in');
			$("#sub-category-narrow-by-button").find('.narrow-by-button-label').addClass('selected');
			if ($('.search-template').length > 0) {
				$('#filtered-products').animate({ 'width': '79.9%', 'left': '22.3%' }, 'slow');//$('#filtered-products').animate({'width':'76%','left':'23%'},'slow');

			}
			else {
				$('#filtered-products').animate({ 'width': '77%', 'left': '25%' }, 'slow');
				//$('#filtered-products').animate({'width':'75.9%','left':'25%'},'slow');
				//$('#filtered-products').animate({'width':'76%','left':'23%'},'slow');
			}
			dynamicImageSize();
		}
	});
	$(document).on('click', '#filter-accordion .filter-window-header', function () {
		var $this = $(this);
		setTimeout(function () {
			$(document).find('.accordion-content-active').find('.tse-scrollable').TrackpadScrollEmulator({
				wrapContent: false,
				autoHide: false
			});
		}, 300);
	});
	dynamicImageSize();
});
$(window).load(function () {
	setTimeout(function () {
		$(document).find('.compare-menu .tse-scrollable.facets-container').TrackpadScrollEmulator({
			wrapContent: false,
			autoHide: false
		});
	}, 500);
})
function checkForProductCompareSection() {
	if ($('.compare-product-selection').css('display') == 'block') {
		$('.compare-menu').addClass('compare-selected');
	}
	else {
		$('.compare-menu').removeClass('compare-selected');
	}
}



var dynamicImageSize = function () {
	var productBlock = $('#filtered-products .productBlock');
	var imgHeight = 0;
	var imgWidth = 0;
	if ($('#filtered-products').hasClass('productBlock-withFilter')) {
		imgHeight = 245;
		imgWidth = 245;
	}
	else {
		imgHeight = 338;
		imgWidth = 338;
	}
	$(productBlock).each(function (index, currentElm) {
		if ($(currentElm).find(".product-block-image .bnsImgBlock img").length > 0) {
			var targetImage = $(currentElm).find(".product-block-image .bnsImgBlock img").attr('src');
			var imageUrl = targetImage.split("|");
			var NwImageUrl = (imageUrl[0] + "|" + imgHeight + ":" + imgWidth);
			$(currentElm).find(".product-block-image .bnsImgBlock img").attr('src', NwImageUrl);
			/* $(currentElm).css({'height':'500px'});
			 $(currentElm).find('.product-block').css({'height':'100%','overflow':'inherit'});*/
			/*$(currentElm).find('.product-block-image .bnsImgBlock').css({'height':'338px','width':'338px'});*/
		}
	})
}


$(window).load(function () {
	checkForProductCompareSection();
})
/*JS ends For Add to cart btn display functionality on product in PLP page for spak B&S changes*/