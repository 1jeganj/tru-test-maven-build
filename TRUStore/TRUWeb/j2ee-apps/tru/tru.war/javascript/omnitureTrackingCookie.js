(function(){
	// Remove I-Frame call for single domain
	var previewPromoEnableInStaging = $("#previewPromoEnableInStaging").val();
	if(previewPromoEnableInStaging != 'true')
	{
		//crossDomainCookieIframeLoader();
	}
})();
function crossDomainCookieIframeLoader() {
	var iFrameURL = getCookie("alternateDomain");
	if(iFrameURL != ""){
		iFrameURL = location.protocol + '//' + iFrameURL +"/common/crossDomainCookiePersister.jsp";
		var ifrm = document.createElement("iframe");
		ifrm.setAttribute("onload", "javascript:setOmnitureTrackingCookies()");
		ifrm.setAttribute("id", "omniCrossDomainIframe");
		ifrm.setAttribute("src", iFrameURL);
		ifrm.style.width = "0px";
		ifrm.style.height = "0px";
		document.body.appendChild(ifrm);
	}	
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function setOmnitureTrackingCookies() {
	setCrossDomainCookie("visitorID",getCookie("visitorID"));
	setCrossDomainCookie("customerID",getCookie("customerID"));
	setCrossDomainCookie("browserID",getCookie("browserID"));
	setCrossDomainCookie("sessionID",getCookie("sessionID"));
} 
function setCrossDomainCookie(cname, cvalue) {
	var iframeWindow = document.getElementById('omniCrossDomainIframe');
	var alternateDomain = extractDomainName(iframeWindow.getAttribute("src"));
	if(alternateDomain != ""){
		iframeWindow.contentWindow.postMessage({ key:cname , data:cvalue, domainName:alternateDomain} , iframeWindow.src);
	}
}
function extractDomainName(domainName) {
	var redirectionDomain = domainName.split("//"), extractedDomain = "";
	domainName = (redirectionDomain.length>1) ? (redirectionDomain[1].split("/")[0]):(redirectionDomain[0].split("/")[0]);
	extractedDomain = (domainName.split(".").length>2) ? "."+domainName.split(".")[1]+"."+domainName.split(".")[2] : "."+domainName;
	return extractedDomain;
}