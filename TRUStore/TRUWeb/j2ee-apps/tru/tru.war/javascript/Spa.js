
var i = "";

SearchViewModel = function () {
	var self = this;

	self.boolenFlag = ko.observable(true);
	self.refinementsArray = ko.observableArray([]);
	self.refinementCrumbsArray = ko.observableArray([]);
	self.sortOptionsArray = ko.observableArray([]);
	self.recordsList = ko.observableArray([]);
	self.currentnavState = ko.observable('');
	self.defaultSortOption = ko.observable('');
	self.displayNumOfRec = ko.observable('');
	self.firstRecordNum = ko.observable('');
	self.recordperPage = ko.observable('');
	self.totalRecords = ko.observable('');
	self.lastRecordNumber = ko.observable('');
	self.sortBylabel = ko.observable('sort by');
	self.currentRecordState = ko.observable('');
	self.clearAllFilter = ko.observable('');
	self.previousNavState = ko.observable('');
	self.loadMoreData = ko.observable(false);
	self.isSortClick = ko.observable(false);
	self.autoCallObj = ko.observable(true);
	self.pageName = ko.observable('');
	self.defaultVal = ko.observable('');
	self.howmany = ko.observable('');
	self.totalRecorsCountVisible = ko.observable('');
	self.emptyrefinementsCrumbs = ko.observable(false);
	self.cartCookieArray = ko.observable([]);
	self.cmsSpotArray = ko.observable([]);
	self.cmsStartCount = ko.observable('');
	self.numCMSTotal = ko.observable('');
	self.pageflag = ko.observable(true);
	self.sellflag = ko.observable(true);
	self.numCMS = ko.observable('');
	self.active = ko.observable(true);
	self.btnText = ko.observable('');
	self.historyListData = ko.observable('');
	self.historyNavData  = ko.observable('');

	var contextPath = $(".contextPath").val();
	var currentClick;
	var lastClick = '';
	var _NoOfRecPerPage = 0;
	var searchRefinementType = [];
	var searchRefinementValue = [];
	var tealiumSearchTypeIndex = 0;
	var tealiumSearchValueIndex = 0;

	//for refinements
	var _TRUGuidedNavigation;
	var _testTRUGuidedNavigation;
	var _tTRUCrumbFacets;

	/* To Open Accordion after AJAX call */
	var _tempArr = [];
	var _tempAccordion = 0;
	var _currentEndecaSiteId = $("#currentEndecaSiteId").val();

	//for showing load more records
	self.howmany = ko.pureComputed(function () {
		if ((self.totalRecords() - self.lastRecordNumber()) > self.recordperPage()) {
			return self.recordperPage();
		} else {
			return self.totalRecords() - self.lastRecordNumber();
		}
	}, self);

	//showing display number of records
	self.displayNumOfRec = ko.pureComputed(function () {
		return self.lastRecordNumber();
	}, self);

	//holding the previous Navigational state
	self.currentnavState.subscribe(function (previousValue) {
		self.previousNavState(previousValue);
	}, this, "beforeChange");

	//parse the cart Cookie
	self.parseCartCookie = function () {
		var cartcookie = $.cookie("cartCookie");
		var loggedinCookie = $.cookie("loggedinCartCookie");
		var savedCookie;
		var dummyData = '38E3C868|productId|quantity|locationId|channelId|channelType|channelUserName|channelCoUserName|channelname~SKUId1|productId1|quantity1|locationId1|channelId1|channelType1|channelUserName1|channelCoUserName1|channelname1';
		if (typeof cartcookie !== 'undefined' && cartcookie !== null) {
			savedCookie = cartcookie;
		} else if (typeof loggedinCookie !== 'undefined' && loggedinCookie !== null) {
			savedCookie = loggedinCookie;
		}
		else {
			savedCookie = dummyData;
		}
		// Check whether cookie data is empty or not
		if (savedCookie != null && savedCookie.length > 1) {
			var cookieArray = savedCookie.split('~');
			var cartObject = null;
			if (cookieArray != null && cookieArray.length >= 1) {
				cartObject = new Array(cookieArray.length);
				for (item = 0; item < cookieArray.length; item++) {
					cartObject[item] = cookieArray[item];
					var cartItem = cookieArray[item];
					var itemInfo = cartItem.split('|');
					if (itemInfo != null && itemInfo.length >= 1) {
						cartObject[item] = itemInfo[0];
					}
				}
				self.cartCookieArray(cartObject);
			}
		}
	}

	self.isPromoApplicablesingle = function (skuQualifiedForPromos) {

		if (typeof skuQualifiedForPromos !== 'undefined') {
			skuQualifiedForPromos.sort(function (item1, item2) {
				return item1.charAt(item1.length - 1) - item2.charAt(item2.length - 1);
			});
			
			var size = skuQualifiedForPromos.length;
			var qualifiedPromoArr = [];
			for (iterate = 0; iterate < size; iterate++) {

				splitArray = skuQualifiedForPromos[iterate].split('|');
				var splitByTime = (splitArray[2]).replace(/[:. ]/g, '-');
				var tempvar = splitByTime.split('-');
				promoStartDate = new Date(tempvar[0], tempvar[1], tempvar[2], tempvar[3], tempvar[4], tempvar[5]);


				var splitByTime1 = (splitArray[3]).replace(/[:. ]/g, '-');
				var tempvar1 = splitByTime1.split('-');
				promoEndDate = new Date(tempvar1[0], tempvar1[1], tempvar1[2], tempvar1[3], tempvar1[4], tempvar1[5]);
				var timeStampCookie = $.cookie("currentTimeStamp");
				var currentDate = '';
				if (typeof timeStampCookie !== 'undefined' && timeStampCookie != '') {
					timeStampCookie = timeStampCookie.replace(/[:. ]/g, '-');
					var tempvar2 = timeStampCookie.split('-');
					currentDate = new Date(tempvar2[0], tempvar2[1], tempvar2[2], tempvar2[3], tempvar2[4], tempvar2[5]);
				}
				//return active promotion
				if (promoStartDate < currentDate && currentDate < promoEndDate) {
					var obj = { 'promotion': splitArray[1], 'priority': splitArray[4] };
					qualifiedPromoArr.push(obj);
				} else {
					window._promoCode = '';
				}
			}
			if (qualifiedPromoArr.length !== 0) {
				min = Math.min.apply(null, qualifiedPromoArr.map(function (item) {
					return item.priority;
				}));
				qualifiedPromos = qualifiedPromoArr.filter(function (obj) {
					return obj.priority == min;
				});
				window._promoCode = qualifiedPromos[0].promotion;
			}
			else {
				window._promoCode = '';
			}
		}
		return window._promoCode;
	}

	self.isHavingInventry = function (currentSkuId) {

		var disableClass = 'hide-PLP-add-to-cart';
		btnText = 'out of stock';

		if (typeof skuIdWithInventry == 'object' && skuIdWithInventry.hasOwnProperty(currentSkuId)) {

			var iVal = skuIdWithInventry[currentSkuId];

			if (iVal == 'InventoryAvalialble') {
				disableClass = 'inventry-avail-PLP-add-to-cart';
				btnText = 'add to cart';
			} else if (iVal == 'StoreInventoryAvalialble') {
				disableClass = 'store-inventory-avail-PLP-add-to-cart';
				btnText = 'add to cart';
			} else if (iVal == 'StoreAvailable') {
				disableClass = 'store-avail-PLP-add-to-cart';
				btnText = 'pickup in store';
			} else if (iVal == 'isPreSellable') {
				disableClass = 'inventry-avail-PLP-add-to-cart';
				btnText = 'pre-order now';
			}
			else {
				disableClass = 'hide-PLP-add-to-cart';
				btnText = 'out of stock';
			}
		}

		return disableClass;
	}

	self.getPromoMessage = function (currentSkuId) {
		var currentPromoMsg = '';
		if (typeof skuIdWithPromoMsg == 'object' && typeof skuIdWithPromoMsg.promotionDetails != 'undefined') {
			$.each(skuIdWithPromoMsg.promotionDetails, function (pKey, pVal) {
				if (pVal.skuId == currentSkuId[0]) {
					if (pVal.promotions.length > 0 && typeof pVal.promotions[0].mDetails != 'undefined') {
						currentPromoMsg = pVal.promotions[0].mDetails;
					}
				}
			});
		}

		return currentPromoMsg;

	}

	self.getPriceMessage = function (currentSkuId) {
		var priceBlock = '';
		var priceListPriceBlock = '';
		var priceSalePriceBlock = '';

		if (typeof skuIdWithPrice == 'object' && skuIdWithPrice.hasOwnProperty(currentSkuId)) {

			var currentObj = skuIdWithPrice[currentSkuId];
			if (currentObj.hasOwnProperty("hasPriceRange") && (currentObj["hasPriceRange"] == "false" || currentObj["hasPriceRange"] == false)) {

				if (currentObj.hasOwnProperty("isStrikeThrough") && (currentObj["isStrikeThrough"] == "true" || currentObj["isStrikeThrough"] == true)) {

					if (currentObj.hasOwnProperty("sku.listPrice") && currentObj["sku.listPrice"] != "") {
						priceListPriceBlock = '<span class="product-block-original-price price-strike-through">$' + currentObj["sku.listPrice"].toFixed(2) + '</span>';
					}
					if (currentObj.hasOwnProperty("sku.salePrice") && currentObj["sku.salePrice"] != "") {
						priceSalePriceBlock = '<span class="product-block-sale-price">$' + currentObj["sku.salePrice"].toFixed(2) + '</span>';
					}
					priceBlock = priceListPriceBlock + priceSalePriceBlock;
				}
				else {
					if (currentObj.hasOwnProperty("sku.salePrice") && currentObj["sku.salePrice"] == "0.0") {
						//show list price
						priceBlock = '$' + currentObj["sku.listPrice"].toFixed(2);
					} else if (currentObj.hasOwnProperty("sku.salePrice")) {
						//show sale price
						priceBlock = '$' + currentObj["sku.salePrice"].toFixed(2);
					}
				}

			}
			else {
				if (currentObj.hasOwnProperty("minimumPrice") && currentObj.hasOwnProperty("maximumPrice") && currentObj["minimumPrice"] == currentObj["maximumPrice"]) {
					priceBlock = ('$' + currentObj["minimumPrice"].toFixed(2)) || '';
				}
				else if (currentObj.hasOwnProperty("minimumPrice") && currentObj.hasOwnProperty("maximumPrice")) {
					priceBlock = ('$' + currentObj["minimumPrice"].toFixed(2)) + ' - ' + ('$' + currentObj["maximumPrice"].toFixed(2));
				}
			}
		}

		return priceBlock;
	}

	self.checkPromo = function (mydata) {
		if (mydata.numRecords > 1) {
			self.isPromoApplicable(mydata.records[0].attributes['product.swatchColors'][0]);
		}
		if (mydata.numRecords < 2) {

			self.isPromoApplicablesingle(mydata.records[0].attributes['sku.skuQualifiedForPromos']);
		}
	}

	self.isPromoApplicable = function (skuQualifiedForPromos) {
		if (skuQualifiedForPromos.indexOf('||') > -1) {
			if (skuQualifiedForPromos.indexOf(';') > -1) {
				var splitArr = skuQualifiedForPromos.split(';')[3];
				var promoarray = splitArr.split('||');
				for (var index in promoarray) {
					if (promoarray[index] === "") {
						promoarray.splice(index, 1);
					}
				}
				skuQualifiedForPromos = promoarray;
			}
			var size = skuQualifiedForPromos.length;
			// sorting promos on the basis of priority
			skuQualifiedForPromos.sort(function (item1, item2) {
				return item1.charAt(item1.length - 1) - item2.charAt(item2.length - 1);
			});
			var splitArray;
			for (iterate = 0; iterate < size; iterate++) {
				splitArray = skuQualifiedForPromos[iterate].split('|');
				var splitByTime = (splitArray[2]).replace(/[:. ]/g, '-');
				var tempvar = splitByTime.split('-');
				promoStartDate = new Date(tempvar[0], tempvar[1], tempvar[2], tempvar[3], tempvar[4], tempvar[5]);


				var splitByTime1 = (splitArray[3]).replace(/[:. ]/g, '-');
				var tempvar1 = splitByTime1.split('-');
				promoEndDate = new Date(tempvar1[0], tempvar1[1], tempvar1[2], tempvar1[3], tempvar1[4], tempvar1[5]);
				var timeStampCookie = $.cookie("currentTimeStamp");
				var currentDate = '';
				if (typeof timeStampCookie !== 'undefined') {
					timeStampCookie = timeStampCookie.replace(/[:. ]/g, '-');
					var tempvar2 = timeStampCookie.split('-');
					currentDate = new Date(tempvar2[0], tempvar2[1], tempvar2[2], tempvar2[3], tempvar2[4], tempvar2[5]);
				}
				// return active promotion
				if (promoStartDate < currentDate && currentDate < promoEndDate) {
					window._promoCode = splitArray[1];
					break;
				} else {
					window._promoCode = '';
				}

			}
		} else {
			window._promoCode = '';
		}
		return window._promoCode;
	}

	//Checks if cookie Array consist of the skuId or not
	self.isCartRepositoryId = function (skuRepositoryId) {
		if (self.cartCookieArray().indexOf(skuRepositoryId[0]) > -1) {
			return true;
		} else {
			return false;
		}
	}

	//function to check isStrike throu price based on that change the classes
	self.checkStrikeThrough = function (StrikeThroughVal, index) {
		if (StrikeThroughVal == 'true') {
			$("#blockStrike" + index).addClass('on-sale');
		}
		return false;
	}

	//method to check start rating
	self.calculateStars = function (startRating) {
		if (startRating % 1 != 0) {
			return true;
		} else {
			return false;
		}
	}

	self.changeBestMatch = function (flag) {
		if (flag) {
			$('.sort-by-title').text(self.sortOptionsArray()[0].label);
			self.pageflag(false);
		}
		return true;
	}

	self.changeBestSell = function (flag, defaultSortOption) {
		if (flag) {
			if (typeof defaultSortOption != 'undefined' && defaultSortOption !== '') {
				$('.sort-by-title').text(defaultSortOption);
			} else {
				$('.sort-by-title').text(self.sortOptionsArray()[1].label);
			}
			self.sellflag(false);
		}
		return true;
	}

	//select refinement Option Call
	self.refineMentOpntion = function (dataObj, event) {
		if(typeof(event) != 'undefined')
		 {
			 $(event.currentTarget).children(".filter-lbl").addClass("selected");
		 }
		self.boolenFlag(true);
		$("#side-overlay").show();

		var navigationStateURI = (dataObj.hasOwnProperty("navigationState")) ? dataObj.navigationState : dataObj;

		//change request assembler content while filter or clearfilter
		navigationStateURI = navigationStateURI.replace('SearchCollection', 'AjaxknockoutCollection');

		//save current requested navigation state later on used to get records
		var url = contextPath + "assembler" + navigationStateURI;

		if (url.indexOf("format=json") === -1) { 
			url = url + "&assemblerContentCollection=%2Fcontent%2F" + _currentEndecaSiteId + "%2FAjaxknockoutCollection&format=json"; 
		}

		self.currentnavState(url);

		var pageName = $(".pageName").val();
		var refinement = (pageName == 'search') ? 'Search:' : 'Parametric:';

		window.url = url;
		ajaxCall(url, self.filterSuccess, self.filterFailure);

		//Added for omniture
		if (typeof event != 'undefined') {
			var type = event.currentTarget.parentNode.parentElement.previousElementSibling && event.currentTarget.parentNode.parentElement.previousElementSibling.lastElementChild.firstElementChild.innerHTML;
			var containsType = (searchRefinementType.indexOf(type) > -1);
			if (!containsType) {
				searchRefinementType[tealiumSearchTypeIndex] = refinement + type;
				tealiumSearchTypeIndex = tealiumSearchTypeIndex + 1;
			}
		}

		if (dataObj.hasOwnProperty("label")) {
			var containsValue = (searchRefinementValue.indexOf(dataObj.label) > -1);
			if (!containsValue) {
				searchRefinementValue[tealiumSearchValueIndex] = refinement + type + ':' + dataObj.label;
				tealiumSearchValueIndex = tealiumSearchValueIndex + 1;
			}
		}
		utag_data.search_refinement_type = searchRefinementType.toString();
		utag_data.search_refinement_value = searchRefinementValue.toString();
		utag.link({
			event_type: 'search_refinement_selection',
			search_refinement_type: searchRefinementType.toString(),
			search_refinement_value: searchRefinementValue.toString()
		});
		utag.link({ event_type: 'parametric_refinement_selection' });
	}

	//clear filter call
	self.unselectRefinement = function (dataObj, event) {
		if(typeof(event) != 'undefined')
		 {
			 $(event.currentTarget).children(".filter-lbl").addClass("unSelected");
		 }
		var navigationStateURI;
		var unselectedValue = dataObj.label;
		var containsValue = (searchRefinementValue.indexOf(unselectedValue) > -1);
		var containsString;

		for (i = 0; i < searchRefinementValue.length; i++) {
			if (searchRefinementValue[i].indexOf(unselectedValue) > -1) {
				containsString = searchRefinementValue[i];
				break;
			}
		}
		
		if (containsString !== undefined) {
			var containsParentString = containsString.split(':');
			containsParentString = containsParentString[0] + ":" + containsParentString[1];
		}

		if (containsParentString) {
			searchRefinementType.splice(searchRefinementType.indexOf(containsParentString), 1);
			tealiumSearchTypeIndex = tealiumSearchTypeIndex - 1;
		}
		if (containsString) {
			searchRefinementValue.splice(searchRefinementValue.indexOf(containsString), 1);
			tealiumSearchValueIndex = tealiumSearchValueIndex - 1;
		}
		utag_data.search_refinement_type = searchRefinementType.toString();
		utag_data.search_refinement_value = searchRefinementValue.toString();
		utag.link({
			event_type: 'search_refinement_selection',
			search_refinement_type: searchRefinementType.toString(),
			search_refinement_value: searchRefinementValue.toString()
		});

		var nParam = '';
		var nValue = $('#Nvalue').val();

		if (typeof nValue !== 'undefined' && nValue !== '') {
			nParam = nValue;
		} else {
			nParam = self.getParameterByName("N");
		}

		var nttParam = self.getParameterByName("keyword");
		if (nParam && nttParam === '') {
			self.defaultVal(nParam);
		}
		
		if (event.target.id == 'clearTopFilter') {
			navigationStateURI = dataObj.removeAction.navigationState;
			self.currentnavState(navigationStateURI);
			if (navigationStateURI.indexOf('N=') < 0) {
				navigationStateURI = navigationStateURI + "&N=" + self.defaultVal();
			}
			self.refineMentOpntion(navigationStateURI);
		} else {
			//fetch navigation state for clear filter
			if (dataObj.hasOwnProperty('removeAction')) {
				navigationStateURI = dataObj.removeAction.navigationState;
			}
			else {
				navigationStateURI = dataObj.clearAllFilter();
				if (navigationStateURI != 'undefined' && (navigationStateURI.indexOf('keyword=') > -1)) {
					searchRefinementType = ['Clear:All'];
					searchRefinementValue = ['Clear:All'];
					tealiumSearchTypeIndex = 0;
					tealiumSearchValueIndex = 0;
					utag_data.search_refinement_type = searchRefinementType.toString();
					utag_data.search_refinement_value = searchRefinementValue.toString();
				}
			}
			if (navigationStateURI.indexOf('N') < 0) { }
			else if (navigationStateURI.indexOf('categoryid') > -1) {
				searchRefinementType = ['Clear:All'];
				searchRefinementValue = ['Clear:All'];
				tealiumSearchTypeIndex = 0;
				tealiumSearchValueIndex = 0;
				utag_data.search_refinement_type = searchRefinementType.toString();
				utag_data.search_refinement_value = searchRefinementValue.toString();
			}
			self.currentnavState(navigationStateURI);
			//pass clear filter state to refineMentOpntion method
			self.refineMentOpntion(navigationStateURI);
		}
	};

	self.decodeURLParms = function (parm) {
		return JSON.parse('{"' + decodeURI(parm).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') + '"}');
	}

	self.filterSuccess = function (data) {
		window.redirectFlag = true;
		var url = window.url.split('/assembler?')[1];
		var urlParms = self.decodeURLParms(url);
		var arr = {};
		if (typeof urlParms.N !== "undefined") {
			arr.N = urlParms.N;
		}
		if (typeof urlParms.keyword !== "undefined") {
			arr.keyword = urlParms.keyword;
		}
		if (typeof urlParms.categoryid !== "undefined") {
			arr.categoryid = urlParms.categoryid;
		}
		url = decodeURIComponent($.param(arr, true));
		self.populatedata(data);
		if (window.redirectFlag) {
			HistoryAPI.pushState(data, "newPushState", window.location.origin + window.location.pathname + '?' + url);
		}

		if (typeof onloadCompareImageSection !== 'undefined')
		{ onloadCompareImageSection(false); }
		$("#side-overlay").hide();
	}

	HistoryAPI.popStateEvent(function (e) {
		var stateObj = e.state || HistoryAPI.presentStateData;
		if (stateObj.hasOwnProperty('contents')) {
			self.populatedata(stateObj);
		} else {
			self.populatedatafirst(stateObj);
			self.refinementCrumbsArray([]);
			self.populatedatafirst(self.historyListData());
			self.populatedatafirst(self.historyNavData());
			$(".clear-all-filterss").addClass('hide');
			$(".clearAllFilter").hide();
		}

	});

	self.filterFailure = function () {
		$(".filter-lbl").removeClass("selected");
		$(".filter-lbl").removeClass("unSelected");
	}

	//reinitialize the Jquery  accordion after Rendering forEach
	self.reInitializeAccordain = function (element, data) {
		if (self.refinementsArray.indexOf(data) === self.refinementsArray().length - 1) {
			$('*[id=filter-accordion]').accordion("refresh");
			if (self.autoCallObj()) {
				$('*[id=filter-accordion]').accordion({ collapsible: true, active: false });
			}
			self.autoCallObj(false);
			var options = {
				"icons": {
					"header": "ui-icon-plus",
					"activeHeader": "ui-icon-minus"
				},
				"collapsible": "false",
				"active": "false",
				"autoHeight": "true",
				"heightStyle": "content"
			};
			var animationDuration = 1000;
			// Build into sub-accordions so we can have multiple open.
			$('.filter-scroll-wrapper').TrackpadScrollEmulator({
				autoHide: false
			});

			$('*[id=filter-accordion]').accordion({
				beforeActivate: function (event, ui) {
					// The accordion believes a panel is being opened
					if (ui.newHeader[0]) {
						var currHeader = ui.newHeader;
						var currContent = currHeader.next('.ui-accordion-content');
						// The accordion believes a panel is being closed
					} else {
						var currHeader = ui.oldHeader;
						var currContent = currHeader.next('.ui-accordion-content');
					}
					// Since we've changed the default behavior, this detects the actual status
					var isPanelSelected = currHeader.attr('aria-selected') == 'true';

					// Toggle the panel's header
					currHeader.toggleClass('ui-corner-all', isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top', !isPanelSelected).attr('aria-selected', ((!isPanelSelected).toString()));

					// Toggle the panel's icon
					currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e', isPanelSelected).toggleClass('ui-icon-triangle-1-s', !isPanelSelected);

					// Toggle the panel's content
					currContent.toggleClass('accordion-content-active', !isPanelSelected)

					var _t = "";
					if (_tempAccordion === 0) {
						_t = currContent.prev().find(".findMe").text()
						var index = _tempArr.indexOf(_t);
						if (index > -1) {
							_tempArr.splice(index, 1);
						}
					}
					if (isPanelSelected) {
						currContent.slideUp();
						$(currContent).prev().find(".ui-icon-minus").removeClass('ui-icon-minus').addClass('ui-icon-plus');
					} else {
						currContent.slideDown();
						$(currContent).prev().find(".ui-icon-plus").removeClass('ui-icon-plus').addClass('ui-icon-minus');
						if (_tempAccordion === 0) {
							_tempArr.push(_t);
						}
					}

					$('.filter-scroll-wrapper').TrackpadScrollEmulator('recalculate');

					return false; // Cancel the default action
				}
			});


			if ($('*[id=filter-accordion]').find('h3').length > 0) {
				_tempAccordion = 1;
				$('*[id=filter-accordion]').find(".findMe").each(function (i) {
					var ctl = this;
					$.each(_tempArr, function (i1, val) {
						if ($(ctl).text() == val) {
							$('*[id=filter-accordion]').accordion("option", "active", i);
						}
					})
				});
				_tempAccordion = 0;
			}

		}
	}

	self.sortOption = function (sortObject) {
		self.cmsSpotArray("");
		self.boolenFlag(true);
		self.sortBylabel(sortObject.label);
		var finderSortGroup = sortObject.label;
		utag_data.finder_sort_group = finderSortGroup;
		if( typeof utag != 'undefined' ){
			utag.link({
				'event_type':'refinement_search_selected',
	   			'finder_sort_group':finderSortGroup
				});
		}
		var URI;
		var sortOptionURI = sortObject.navigationState;
		if (sortOptionURI.indexOf("format=json") > -1) {
		} else {
			sortOptionURI = "/assembler" + sortOptionURI + "&assemblerContentCollection=%2Fcontent%2F" + _currentEndecaSiteId + "%2FAjaxknockoutCollection&format=json";

		}
		
		self.isSortClick(true);
		ajaxCall(sortOptionURI, self.sortOptionSuccess, self.sortOptionFailure);
	}

	self.sortOptionSuccess = function (data) {
		self.populatedata(data);
		if (typeof onloadCompareImageSection !== 'undefined')
		{ onloadCompareImageSection(false); }

	}

	self.sortOptionFailure = function () { }

	self.loadMore = function (e) {
		if (!self.active()) return false;
		self.active(false);
		var temp = self.currentRecordState();
		if (temp.indexOf("format=json") > -1) {
		} else {
			temp = "assembler" + temp + "&assemblerContentCollection=%2Fcontent%2F" + _currentEndecaSiteId + "%2FAjaxknockoutCollection&format=json";
		}

		temp = temp.replace('%7BrecordsPerPage%7D', self.recordperPage());
		temp = temp.replace('%7Boffset%7D', self.lastRecordNumber());
		self.loadMoreData(true);
		ajaxCall(temp, self.loadMoreSuccess, self.loadMoreFailure);
		bruCss();
	}

	self.loadMoreSuccess = function (data) {
		self.populatedata(data);
		var checkedProducts = $("body").find("#narrow-by-scroll").find(".product-compare-checkbox-container").find(".product-compare-checkbox-new.compareChecked").length;
		if (checkedProducts == 4) {
			var checkBoxes = $(".product-compare-checkbox-new");
			$(checkBoxes).each(function (i, val) {
				if ($(val).hasClass("compareUnChecked")) {
					$(val).closest(".product-compare-checkbox-container").css("visibility", "hidden");
				}
			});
		}
		if (typeof onloadCompareImageSection !== 'undefined')
		{ onloadCompareImageSection(false); }
		self.loadMoreData(false);
	}

	self.loadMoreFailure = function () { }

	self.populatedatafirst = function (data) {
		if (typeof data != "undefined" && data != null) {
			if (data['@type'] == "TRUGuidedNavigation") {
				_TRUGuidedNavigation = data;

				self.populateGuidedNavigation(data);

			} else if (data['@type'] == "TRUResultsList") {
				self.populateResultList(data);
			} else if (data['@type'] == "TRUCrumbFacets") {
				self.populateCrumbFacets(data);
			}
			var nttParam = self.getParameterByName("keyword");
			if (nttParam != 'undefined' && nttParam !== '') {
				self.pageName('SearchPage');
			}
		}
	}

	self.populatedata = function (data) {
		var size = data.contents[0].MainProduct[0].contents[0].MainProduct.length;
		for (iterate = 0; iterate < size; iterate++) {
			if (data.contents[0].MainProduct[0].contents[0].MainProduct[iterate]['@type'] == "TRUGuidedNavigation") {
				_testTRUGuidedNavigation = data.contents[0].MainProduct[0].contents[0].MainProduct[iterate];
				if (_testTRUGuidedNavigation.navigation.length < _TRUGuidedNavigation.navigation.length) {
					$.each(_TRUGuidedNavigation.navigation, function (i, ob) {
						var _find = 0;
						$.each(_testTRUGuidedNavigation.navigation, function (ind, obj) {
							if (ob.name === obj.name && _find === 0) {
								_find = 1;
							}
						});
						if (_find === 0) {
							_find = 1;
							$.each(_tTRUCrumbFacets.refinementCrumbs, function (ind1, obj1) {
								if (ob.dimensionName === obj1.dimensionName && _find === 1) {
									_find = 0;
								}
							});
						}

						if (_find === 0) {
							var _tObj1 = {};
							var _tObj2 = {};
							var refinements = [];
							_tObj2.label = "dummy";
							_tObj2.count = 0;
							_tObj2.navigationState = "";
							_tObj2.dummyVal = 1;
							refinements.push(_tObj2)
							_tObj1.dimensionName = ob.dimensionName;
							_tObj1.displayName = ob.displayName;
							_tObj1.name = ob.name;
							_tObj1.multiSelect = true;
							_tObj1.dummyVal = 1;
							_tObj1.refinements = refinements;
						}
					})
				}
				var temp = iterate;
				iterate = self.populateGuidedNavigation(_testTRUGuidedNavigation);
				iterate = temp;
			} else if (data.contents[0].MainProduct[0].contents[0].MainProduct[iterate]['@type'] == "TRUResultsList") {
				getPromoForSku(data.contents[0].MainProduct[0].contents[0].MainProduct[iterate].skuIdArray, data.contents[0].MainProduct[0].contents[0].MainProduct[iterate].skuStoreEligibleArray, data.contents[0].MainProduct[0].contents[0].MainProduct[iterate].SKU_PRICE_ARRAY);
				var temp = iterate;
				iterate = self.populateResultList(data.contents[0].MainProduct[0].contents[0].MainProduct[iterate]);
				iterate = temp;

			} else if (data.contents[0].MainProduct[0].contents[0].MainProduct[iterate]['@type'] == "TRUCrumbFacets") {
				var temp = iterate;
				_tTRUCrumbFacets = data.contents[0].MainProduct[0].contents[0].MainProduct[iterate];
				iterate = self.populateCrumbFacets(data.contents[0].MainProduct[0].contents[0].MainProduct[iterate]);
				iterate = temp;
			}
		}
	}

	self.populateGuidedNavigation = function (data) {
		if (data.hasOwnProperty("navigation")) {
			var newLen = data.navigation.length;
			for (var temp = 0; temp < newLen; temp++) {
				if (data.navigation[temp].dimensionName === "product.category") {
					var refinementsArray = data.navigation[temp].refinements;
					for (var another_temp = 0; another_temp < refinementsArray.length; another_temp++) {
						if (typeof refinementsArray[another_temp].properties !== 'undefined') {
							var displayStatus = refinementsArray[another_temp].properties['dimval.prop.category.displayStatus'];
							if (typeof displayStatus !== 'undefined' && displayStatus.toLowerCase() === 'no display') {
								refinementsArray.splice(another_temp, 1);
							}
						}
					}
				}
			}
		}
		if (data.hasOwnProperty("navigation")) {
			if (data.navigation.length > 0 && data.navigation[0].hasOwnProperty("refinements")) {

				$('#guidedNavigation').css('display', 'block');
				var narrowByflag = true;
				for (var temp = 0; temp < data.navigation.length; temp++) {
					if (data.navigation[temp].refinements.length > 0) {
						narrowByflag = false;
					}
				}
				if (narrowByflag == false) {
					$("#sub-category-narrow-by-button").css('pointer-events', 'all');
					$("#sub-category-narrow-by-button").css('opacity', '1');
					$(".compare-menu").show();
					$("#filtered-products").addClass('productBlock-withFilter');
				} else {
					$("#sub-category-narrow-by-button").css('pointer-events', 'none');
					$("#sub-category-narrow-by-button").css('opacity', '0.4');
					$(".compare-menu").hide();
					$("#filtered-products").removeClass('productBlock-withFilter');
					$('.fade-to-black').click();

				}
				self.refinementsArray(data.navigation);
			} else {
				self.refinementsArray([]);
				$("#sub-category-narrow-by-button").css('pointer-events', 'none');
				$("#sub-category-narrow-by-button").css('opacity', '0.4');
				$('.fade-to-black').click();
				$('#guidedNavigation').css('display', 'block');
				$(".compare-menu").hide();
				$("#filtered-products").removeClass('productBlock-withFilter');
			}
		} else {
			self.refinementsArray([]);
			$("#sub-category-narrow-by-button").css('pointer-events', 'none');
			$("#sub-category-narrow-by-button").css('opacity', '0.4');
			$('.fade-to-black').click();
			$('#guidedNavigation').css('display', 'block');
			$(".compare-menu").hide();
			$("#filtered-products").removeClass('productBlock-withFilter');
		}

	}

	self.populateResultList = function (data) {

		if (data.hasOwnProperty("records") && data.records[0].hasOwnProperty('attributes')) {
			if (typeof data.noOFCMSSPOTS != 'undefined' && data.noOFCMSSPOTS === 4) {
				if (data.totalNumRecs >= 4) {
					if (!self.loadMoreData()) {
						(data.records).splice(0, 0, data.CMSSPOT[0]);
						(data.records).splice(3, 0, data.CMSSPOT[1]);
						(data.records).splice(4, 0, data.CMSSPOT[2]);
						(data.records).splice(7, 0, data.CMSSPOT[3]);

					}
				}
				if (data.totalNumRecs === 2) {
					if (!self.loadMoreData()) {
						(data.records).splice(0, 0, data.CMSSPOT[0]);
						(data.records).splice(3, 0, data.CMSSPOT[1]);
						(data.records).splice(4, 0, data.CMSSPOT[2]);
						(data.records).splice(5, 0, data.CMSSPOT[3]);
					}

				}
				if (data.totalNumRecs === 3) {
					if (!self.loadMoreData()) {
						(data.records).splice(0, 0, data.CMSSPOT[0]);
						(data.records).splice(3, 0, data.CMSSPOT[1]);
						(data.records).splice(4, 0, data.CMSSPOT[2]);
						(data.records).splice(6, 0, data.CMSSPOT[3]);
					}

				}
			}
			if (typeof data.noOFCMSSPOTS != 'undefined' && data.noOFCMSSPOTS === 3) {
				if (data.totalNumRecs >= 4) {
					if (!self.loadMoreData()) {
						(data.records).splice(0, 0, data.CMSSPOT[0]);
						(data.records).splice(3, 0, data.CMSSPOT[1]);
						(data.records).splice(4, 0, data.CMSSPOT[2]);
					}
				}
				if (data.totalNumRecs === 2) {
					if (!self.loadMoreData()) {
						(data.records).splice(0, 0, data.CMSSPOT[0]);
						(data.records).splice(3, 0, data.CMSSPOT[1]);
						(data.records).splice(4, 0, data.CMSSPOT[2]);
					}

				}
				if (data.totalNumRecs === 3) {
					if (!self.loadMoreData()) {
						(data.records).splice(0, 0, data.CMSSPOT[0]);
						(data.records).splice(3, 0, data.CMSSPOT[1]);
						(data.records).splice(4, 0, data.CMSSPOT[2]);
					}

				}
			}
			if (typeof data.noOFCMSSPOTS != 'undefined' && data.noOFCMSSPOTS === 2) {
				if (data.totalNumRecs >= 4) {
					if (!self.loadMoreData()) {

						(data.records).splice(0, 0, data.CMSSPOT[0]);
						(data.records).splice(9, 0, data.CMSSPOT[1]);
					}
				}
				if (data.totalNumRecs === 2) {
					if (!self.loadMoreData()) {
						(data.records).splice(0, 0, data.CMSSPOT[0]);
						(data.records).splice(3, 0, data.CMSSPOT[1]);
					}

				}
				if (data.totalNumRecs === 3) {
					if (!self.loadMoreData()) {
						(data.records).splice(0, 0, data.CMSSPOT[0]);
						(data.records).splice(3, 0, data.CMSSPOT[1]);
					}

				}
			}
			if (typeof data.noOFCMSSPOTS != 'undefined' && data.noOFCMSSPOTS === 1) {
				if (data.totalNumRecs >= 4) {
					if (!self.loadMoreData()) {
						(data.records).splice(0, 0, data.CMSSPOT[0]);
					}
				}
				if (data.totalNumRecs === 2) {
					if (!self.loadMoreData()) {
						(data.records).splice(0, 0, data.CMSSPOT[0]);
					}

				}
				if (data.totalNumRecs === 3) {
					if (!self.loadMoreData()) {
						(data.records).splice(0, 0, data.CMSSPOT[0]);
					}

				}
			}

			if (data.totalNumRecs === 1) {
				var pdpUrl = (data.records[0].detailsAction.recordState).replace('&format=json', '');
				window.redirectFlag = false;
				window.location.href = window.location.protocol + "//" + pdpUrl;

			} else {
				if (!self.loadMoreData()) {
					self.recordsList(data.records);
				} else {
					self.recordsList.push.apply(self.recordsList, data.records);
				}

				if (typeof data.noOFCMSSPOTS != 'undefined' && self.boolenFlag()) {
					data.recsPerPage = data.recsPerPage + data.noOFCMSSPOTS;
					data.lastRecNum = data.lastRecNum;
					self.numCMS(data.noOFCMSSPOTS);
					self.boolenFlag(false);
				}

				self.recordperPage(data.recsPerPage);
				self.totalRecords(data.totalNumRecs);
				self.lastRecordNumber(data.lastRecNum);
				self.firstRecordNum(data.firstRecNum);
				self.sortOptionsArray(data.sortOptions);
				self.defaultSortOption(data.defaultSortOption);
				self.currentRecordState(data.pagingActionTemplate.navigationState);
				self.numCMSTotal(self.totalRecords());
				var productCount = self.displayNumOfRec();
				var tealiumProductCount = parseInt(productCount) + 1;
				var gridwallLocations = [];
				for (count = 1; count < tealiumProductCount; count++) {

					var rowNumber;
					if (count < 4) {
						rowNumber = 1;
					} else {
						rowNumber = count / 4;
					}
					var slotNumber = count % 4;
					if (slotNumber == 0) {
						slotNumber = 4;
					}
					rowNumber = Math.ceil(rowNumber);
					var location = "Row" + rowNumber + "-Slot" + slotNumber;
					gridwallLocations.push(location);
				}
				var tealiumCount = 0;
				var tealiumCheck = setInterval(function () {
					if (typeof utag != 'undefined' && typeof utag_data != 'undefined') {
						if (typeof isGridwallOnLoad != 'undefined' && isGridwallOnLoad == false) {
							utag_data.product_gridwall_location = gridwallLocations;
							utag_data.product_impression_id = productId
						} else {
							if (productCount != 'undefined' || productCount != '') {
								utag.link({
									event_type: 'gridwall_impression',
									product_gridwall_location: gridwallLocations,
									product_impression_id: productId
								});
							}
						}
						clearInterval(tealiumCheck);
					}
					if (tealiumCount > 3) {
						clearInterval(tealiumCheck);
					}
					tealiumCount++;
				}, 2000);
				isGridwallOnLoad = false;
				$('.productBlock').css('display', 'block');
				$('#guidedNavigation').css('display', 'block');
			}
		}
		if ($('.search-template').length > 0) {
			dynamicImageSize();
		}

	}

	//populate the facetcrumbs
	self.populateCrumbFacets = function (data) {
		if (data.refinementCrumbs.length > 0) {
			self.refinementCrumbsArray(data.refinementCrumbs);
			var nParam = '';
			var nValue = $('#Nvalue').val();
			var currentCategory = self.getParameterByName('categoryid');

			if (typeof nValue !== 'undefined' && nValue !== '') {
				nParam = nValue;
			} else {
				nParam = self.getParameterByName("N");
			}
			nttParam = self.getParameterByName("keyword");
			//remove current category from refinement crumb array
			self.refinementCrumbsArray.remove(function (item) {
				return (item.displayName === "product.category" && item.properties['category.repositoryId']) === currentCategory;
			});
			if (nttParam) {
				if ((data.removeAllAction.navigationState).indexOf('?') !== -1) {
					self.clearAllFilter((data.removeAllAction.navigationState) + "&keyword=" + nttParam);
				} else {
					self.clearAllFilter((data.removeAllAction.navigationState) + "?keyword=" + nttParam);
				}

			} else if (nParam) {
				if ((data.removeAllAction.navigationState).indexOf('?') !== -1) {
					self.clearAllFilter((data.removeAllAction.navigationState) + "&N=" + nParam);
				} else {
					self.clearAllFilter((data.removeAllAction.navigationState) + "?N=" + nParam);
				}
			}
			if (self.refinementCrumbsArray().length > 0) {
				$(".clear-all-filterss").removeClass('hide').css({ "color": "#014dbb", "font-size": "14px", "display": "inline-block" });
				$(".clearAllFilter").show();
			} else {
				$(".clear-all-filterss").addClass('hide');
				$(".clearAllFilter").hide();
			}
		} else {
			self.refinementCrumbsArray([]);
			$(".clear-all-filterss").addClass('hide');
			$(".clearAllFilter").hide();
		}

	}

	self.getParameterByName = function (name) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	// Custom for each handler for iteration over array of object
	ko.bindingHandlers.foreachcustom = {
		transformObject: function (obj) {
			var properties = [];
			for (var key in obj) {
				if (obj.hasOwnProperty(key)) {
					properties.push({ key: key, value: obj[key] });
				}
			}
			return properties;
		},
		init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
			var value = ko.utils.unwrapObservable(valueAccessor()),
				properties = ko.bindingHandlers.foreachcustom.transformObject(value);
			ko.applyBindingsToNode(element, { foreach: properties }, bindingContext);
			return { controlsDescendantBindings: true };
		}
	};

	ko.virtualElements.allowedBindings.foreachcustom = true;

	//send data to omniture
	self.omniGridwallClick = function (productId, index) {
		var productIndex = index + 1;
		var rowNumber;
		if (index < 4) {
			rowNumber = 1;
		}
		else {
			rowNumber = productIndex / 4;
		}
		var slotNumber = productIndex % 4;
		if (slotNumber == 0) {
			slotNumber = 4;
		}
		rowNumber = Math.ceil(rowNumber);
		var gridwallLocation = "Row" + rowNumber + "-Slot" + slotNumber;
		
		localStorage.setItem('pdpGridwallLocation', gridwallLocation);
		utag.link({
			event_type: 'gridwall_clicks',
			product_gridwall_location: ['Row' + rowNumber + '-Slot' + slotNumber],
			product_id: productId
		});
		return true;
	}

	//Ajax main Call to server
	function ajaxCall(pUrl, pSuccessCallback, pErrorCallback) {
		var $productGrid = $('#filtered-products');
		$productGrid.addClass('ajax-spinner');

		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: pUrl,
			dataType: "json",
			processData: false,
			success: function (data) {
				$productGrid.removeClass('ajax-spinner');

				pSuccessCallback(data);
				self.active(true);
				setTimeout(function () {
					$('.accordion-content-active').find('.tse-scrollable').TrackpadScrollEmulator({
						wrapContent: false,
						autoHide: false
					});
				}, 500);
				if ($("#sub-category-narrow-by-button").hasClass('filters-closed')) {
					$("#filtered-products").removeClass('productBlock-withFilter');
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$productGrid.removeClass('ajax-spinner');

				pErrorCallback(jqXHR, textStatus, errorThrown);
				self.active(true);
			}
		});
	}

	if (typeof searchResponse !== 'undefined' && searchResponse !== null) {
		self.parseCartCookie();
		var compareCookie = $.cookie("productCompareCookie");
		var categoryId = $('.product-compare-checkbox-container').find(".categoryId").val();
		
		if (typeof compareCookie !== 'undefined' && compareCookie !== '' && compareCookie != null) {
			if (!(compareCookie.indexOf(categoryId) > 0)) {
				$.cookie("productCompareCookie", '', { path: '/' });
			}
		}
		self.populatedatafirst(searchResponse);
		self.historyListData(searchResponse);
		searchResponse = null;
	}

	if (typeof searchResponseNavigation !== 'undefined' && searchResponseNavigation !== null) {
		if (searchResponseNavigation.hasOwnProperty("navigation")) {
			var newLen = searchResponseNavigation.navigation.length;
			for (var temp = 0; temp < newLen; temp++) {
				if (searchResponseNavigation.navigation[temp].dimensionName === "product.category") {
					var refinementsArray = searchResponseNavigation.navigation[temp].refinements;
					for (var another_temp = 0; another_temp < refinementsArray.length; another_temp++) {
						var displayStatus = refinementsArray[another_temp].properties['dimval.prop.category.displayStatus'];
						if (typeof displayStatus !== 'undefined' && displayStatus.toLowerCase() === 'no display') {
							refinementsArray.splice(another_temp, 1);
						}
					}
				}
			}
		}
		self.populatedatafirst(searchResponseNavigation);
		self.historyNavData(searchResponseNavigation);
		searchResponseNavigation = null;
	}

	if (typeof searchResponseCrumbfacet !== 'undefined' && searchResponseCrumbfacet !== null) {
		if (searchResponseCrumbfacet.hasOwnProperty("refinementCrumbs")) {
			self.populatedatafirst(searchResponseCrumbfacet);
			searchResponseCrumbfacet = null;
		}
	}
}

$(document).ready(function () {
	$(document).on('click', '.more-values', function () {
		var clickedParent = $(this).parent();
		if (clickedParent.attr('id') === $(this).attr('id') && !clickedParent.hasClass('expanded')) {
			clickedParent.addClass('expanded');
			clickedParent.find('div:nth-child(n+6)').slideDown(400, function () {
				clickedParent.find('.more-values').text('see fewer');
			});
		} else {
			clickedParent.removeClass('expanded');
			clickedParent.find('div:nth-child(n+6)').slideUp(400, function () {
				clickedParent.find('.more-values').text('more');
			});
		}
		$('.filter-scroll-wrapper').TrackpadScrollEmulator('recalculate');
		return false;
	});


	$(document).on('click', '.swatchSelected', function (event) {
		var cur = $(event.target);
		if (cur.is("div")) {
			cur = cur.children();
		}
		cur.parents('.colors').find('.product-color-block').removeClass('color-selected');
		cur.parents('.product-block').find(".product-block-img").attr("src", cur.attr("imgsrc"));
		cur.parent().addClass('color-selected');
		cur.parents('.product-block').find('.product-block-incentive span:first').html(window._promoCode);

	});
	loadHomeGridAddToCart();
});

/* It is used to get the promo message based on sku ids */
var skuIdWithPromoMsg = '';
var skuIdWithInventry = '';
var skuIdWithPrice = '';
function getPromoForSku(dataSkuIdArray, skuStoreEligibleArray, SKU_PRICE_ARRAY) {
	var pramoID = getParameterByName('pramoid');
	var contextPath = $('.urlFullContextPath').val() || $('.contextPath').val() || '/';

	if (location.host.indexOf('.babiesrus.com') == -1) {
		$.ajax({
			type: 'POST',
			async: false,
			url: contextPath + 'common/ajaxPromoResponse.jsp',
			data: { skuPromoDetails: dataSkuIdArray, pramoID: pramoID, skuStoreEligibleArray: skuStoreEligibleArray, SKU_PRICE_ARRAY: SKU_PRICE_ARRAY },
			success: function (responseData) {
				var resJ = JSON.parse(responseData);
				skuIdWithPromoMsg = resJ.promotionJSON;
				skuIdWithInventry = resJ.inventryJSON;
				skuIdWithPrice = resJ.priceMapJSON;

				$.each(skuIdWithPromoMsg.promotionDetails, function (pKey, pVal) {
					var promoMsg = '';
					$.each(pVal.promotions, function (pKey, pVal) {
						promoMsg = pVal.mDetails;
						return promoMsg;
					});
					$(".sku-" + pVal.skuId).html(promoMsg);
				});
			},
			error: function (e) {
				$(".product-add-to-cart").hide();
			}
		});
	} else {
		$.ajax({
			headers: {
				'X-APP-API_KEY': 'apiKey',
				'X-APP-API_CHANNEL': 'webstore'
			},
			url: contextPath + 'rest/model/com/tru/commerce/promotion/PromoActor/getPriceAndPromo?skuPrice=' + SKU_PRICE_ARRAY + '&sku=' + dataSkuIdArray,
			type: 'POST',
			async: false,
			dataType: 'json',
			success: function (responseData) {
				var resJ = JSON.parse(JSON.stringify(responseData));
				skuIdWithPromoMsg = resJ.promotionDisplay;
				skuIdWithPrice = resJ.priceMap;

				$.each(skuIdWithPromoMsg.promotionDetails, function (pKey, pVal) {
					var promoMsg = '';
					$.each(pVal.promotions, function (pKey, pVal) {
						promoMsg = pVal.promotionName;
						return promoMsg;
					});
					$(".sku-" + pVal.skuId).html(promoMsg);
				});
				$(".product-add-to-cart").hide();
			},
			error: function (errorData) {
			}
		});
	}
}
if (typeof jsSkuIdArray != 'undefined' && typeof skuStoreEligibleArray != 'undefined' && (typeof SKU_PRICE_ARRAY != 'undefined' || typeof skuPriceArrayHomePage != 'undefined')) {
	if (typeof SKU_PRICE_ARRAY != 'undefined') {
		priceSkuList = SKU_PRICE_ARRAY;
	} else if (typeof skuPriceArrayHomePage != 'undefined') {
		priceSkuList = skuPriceArrayHomePage.join('');
	}
	getPromoForSku(jsSkuIdArray, skuStoreEligibleArray, priceSkuList);
}

function loadHomeGridAddToCart() {

	var homeGridAddToCartBtns = $('.main-toy-grid .product-add-to-cart');
	if (location.host.indexOf('.babiesrus.com') > -1) {
		$(".product-add-to-cart").hide();
		return;
	}
	if (typeof skuIdWithInventry == 'object' && homeGridAddToCartBtns.length > 0) {

		$.each(homeGridAddToCartBtns, function (btnKey, btnVal) {

			var gridProductId = $(btnVal).data('productid') ? $(btnVal).data('productid') : '';
			var gridSkuId = $(btnVal).data('skuid') ? $(btnVal).data('skuid') : '';

			var availableClass = 'hide-PLP-add-to-cart';
			var btnLabel = 'out of stock';

			if (skuIdWithInventry.hasOwnProperty(gridSkuId)) {
				var skuVal = skuIdWithInventry[gridSkuId];
				if (skuVal == 'InventoryAvalialble') {
					availableClass = 'inventry-avail-PLP-add-to-cart';
					btnLabel = 'add to cart';
				} else if (skuVal == 'StoreInventoryAvalialble') {
					availableClass = 'store-inventory-avail-PLP-add-to-cart';
					btnLabel = 'add to cart';
				} else if (skuVal == 'StoreAvailable') {
					availableClass = 'store-avail-PLP-add-to-cart';
					btnLabel = 'pickup in store';
				} else {
					availableClass = 'hide-PLP-add-to-cart';
					btnLabel = 'out of stock';
				}
			}

			$(btnVal).text(btnLabel);
			$(btnVal).addClass(availableClass);

		});
	}
}

