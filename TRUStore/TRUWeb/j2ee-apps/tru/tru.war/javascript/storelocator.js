/*$(document).ready(function() {
	var contextPath = $('.contextPath').val();
    if($('.storeLocator').length > 0) {
    	storeLocator($('#storeLocatorSearch'), $('#map_canvas'), $('.results'));
    }
    $('#bfCache').hide();
    $('.store-locator-pick-store').hide();    
});
*/

var isFromISPUStoreOverlay = false;
var currModule = '';
var keepMapStatus = false;
var loadcurrentData
var map;
var activeTab = '';
var loaded = false;
var allStore;
var truStore;
var bruStore;
var clientIdForMaps = "gme-toysrus";
var $bru_ref
var $tru_ref
var $allRef

var showallstores = false;
var us_lat = 37.090240;
var us_long = -105.712891;
var default_zoom = 4;
var stores;
var defaultStores;
var latitude = us_lat, longitude = us_long, zoom_percentage = default_zoom;
var desktopCookieValue = "";
var moreStoresFlag = false;
var scrollFocusShowMore = 0;

var firstLoad = false;
var contextPath = $(".contextPath").val();
var storeLocatorScrollable = $('#store-locator-results-scrollable');
if (storeLocatorScrollable != undefined) {
	storeLocatorScrollable.TrackpadScrollEmulator({
		wrapContent: true,
		autoHide: true
	});
}

var counter_stores = 0;
var findinstorejsonResponseRowData;
var lms_all_page_count = 1, lms_tru_page_count = 1, lms_bru_page_count = 1;
var lms_per_pagination = 10;

$(document).ready(function () {

	if ($('.store-locator-template').length) {
		// $("body").find(".store-locator-tooltip").addClass("active");
	}

	var contextPath = $('.contextPath').val();
	if ($('.storeLocator').length > 0) {
		storeLocator($('#storeLocatorSearch'), $('#map_canvas'), $('.results'));
	}
	$('#bfCache').hide();
	$('.store-locator-pick-store').hide();

	$(document).on('click111111111', '#store-locator-results-tabs li', function () {
		console.log("store-locator-results-tabs li click")
		var bru_length = ($(".store-locator-pick-store").find(".bru-results").length) ? $(".store-locator-pick-store").find(".bru-results").length : $(".store-locator-pick-store").find(".BRUresults").length;
		var tru_length = ($(".store-locator-pick-store").find(".tru-results").length) ? $(".store-locator-pick-store").find(".tru-results").length : $(".store-locator-pick-store").find(".TRUresults").length;
		var $bru_ref = ($(".store-locator-pick-store").find(".bru-results").length) ? $(".store-locator-pick-store").find(".bru-results") : $(".store-locator-pick-store").find(".BRUresults");
		var $tru_ref = ($(".store-locator-pick-store").find(".tru-results").length) ? $(".store-locator-pick-store").find(".tru-results") : $(".store-locator-pick-store").find(".TRUresults");


		var all_length = tru_length + bru_length;
		if ($(this).hasClass("site-Toysrus")) {
			$bru_ref.hide();
			$tru_ref.hide();
			if (!showallstores && tru_length > 10) {
				$tru_ref.each(function () {
					if (counter_stores < 10) {
						counter_stores++;
						$(this).show();
					}
				});
			} else {
				$tru_ref.show();
			}


		} else if ($(this).hasClass("site-Babyrus")) {
			$tru_ref.hide();
			$bru_ref.hide();
			if (!showallstores && bru_length > 10) {
				$bru_ref.each(function () {
					if (counter_stores < 10) {
						counter_stores++;
						$(this).show();
					}
				});
			} else {
				$bru_ref.show();
			}
		} else {
			$tru_ref.hide();
			$bru_ref.hide();
			var $ulRef = $bru_ref.parent();
			if (!showallstores && all_length > 10) {

				$ulRef.find("li").each(function () {
					if (counter_stores < 10) {
						counter_stores++;
						$(this).show();
					}
				});
			} else {
				$ulRef.find("li").show();
			}
		}
		counter_stores = 0;

	});

	/* Use in showGoogleMap.jsp */

	$(document).on('click', '#store-locator-results #show-more-stores', function () {
		showallstores = true;
		keepMapStatus = true;
		$(document).find("#store-locator-results #store-locator-results-tabs").find(".active a").trigger("click");
	});

	$(document).on('click', '.show-more-stores', function () {
		showallstores = true;
		$(document).find("#store-locator-results-tabs").find(".active a").trigger("click");
	});



	//process the find-site-store form
	$(document).on('click', '#storeSiteSearch', function (e) {
		var siteId = $("#siteIds").val();
		var divClass = "site-" + siteId;
		$.ajax({
			type: "POST",
			cache: false,
			url: contextPath + 'storelocator/storeLocatorResults.jsp',
			data: $('#storeSiteLocatorSearch').serializeArray(),
			datatype: "html",
			success: function (data) {
				if ($('.store-locator-zip').hasClass('nearby-stores-find-a-store')) {
					$('.store-locator-pick-store').html(data);
					$('.store-locator-pick-store').show();
					$('.nearby-stores-find-a-store').slideToggle();
					$('.' + divClass).addClass('active');
				} else {
					$('.store-locator-pick-store').html(data);
					$('.store-locator-pick-store').show();
					$('.nearby-stores-find-a-store').slideToggle();
				}
			}
		});
		return false;
	});
	//process the find-a-store form
	$(document).on('click', '.storeSearch', function (e) {
		var search_term = $(this).parent().find(".storeLocatorField").val().trim();
		var $this = $(this);
		if (search_term != "") {
			$this.parents(".sl_parent").find("p.sl_errormsg").hide();
			$this.parents(".sl_parent").find("#storeLocatorAddressEntry").removeClass("sl_input_error_msg");
			$this.parents(".sl_parent").find(".storeLocatorField").removeClass("sl_input_error_msg");
			var globalRadius = '100';
			if ($("#globalRadius").length) {
				if ($("#globalRadius").val() != "") {
					globalRadius = $("#globalRadius").val();
				}
			}
			getStoreResults(search_term, globalRadius, doStoreSearch, this);

		} else {
			$this.parents(".sl_parent").find("p.sl_errormsg").text(addressToGetStarted);
			$this.parents(".sl_parent").find("p.sl_errormsg").show();
			$this.parents(".sl_parent").find("#storeLocatorAddressEntry").addClass("sl_input_error_msg");
			$this.parents(".sl_parent").find(".storeLocatorField").removeClass("sl_input_error_msg");
			$this.parents(".sl_parent").find(".storeLocatorField").focus();
		}
		return false;
	});
	//process the find-a-store form
	$(document).on('click', '#storeSearch1', function (e) {
		$.ajax({
			type: "POST",
			cache: false,
			url: contextPath + 'storelocator/storeLocatorResults.jsp',
			data: $('#changestoreLocatorSearch').serializeArray(),
			datatype: "html",
			success: function (data) {
				$('.store-locator-pick-store').html(data);
				$('.store-locator-pick-store').show();
				$('#location-zip').hide();

			}
		});
		return false;
	});

	//process the show-more-stores form
	$(document).on('click', '#currentResultPageNum', function (e) {
		var pageNum = $("#currentResultPageNum").val();
		var showDivClass = "load-more-stores-" + pageNum;
		var hideDivClass = "show-more-" + pageNum;
		$.ajax({
			type: "POST",
			cache: false,
			url: contextPath + 'storelocator/storeLocatorLoadMoreResults.jsp',
			data: $('#storeLocatorPagination').serializeArray(),
			datatype: "html",
			success: function (data) {
				$('.' + hideDivClass).hide();
				$('.' + showDivClass).html(data);
				$('.' + showDivClass).show();
			}
		});
		return false;
	});

	$(document).on('keyup', ".googleMapGroup #storeLocatorAddressEntry, .sl_root .storeLocatorField, .find-in-store-results #FindinStorePDPField", function (e) {
		if (e.keyCode == 13) {
			if ($(this).attr("id") == "storeLocatorAddressEntry") {
				findInStoreSearch();
			} else if ($(this).attr("id") == "storeLocatorField") {
				findAStore();
			} else if ($(this).attr("id") == "FindinStorePDPField") {
				findInStorePDP();
			} else if ($(this).attr("data-id") == "find-store-header") {
				findAStore();
			}
		}
	});

	//process the find-a-store form
	$(document).on('click', '#findInStoreSearch', function (e) {
		var search_term = $(".googleMapGroup").find("#storeLocatorAddressEntry").val().trim();
		
		var $this = $(this);
		if (search_term != "") {
			$(".googleMapGroup").find("p.sl_errormsg").hide();
			$(".googleMapGroup").find("p.sl_wrongsearch_errormsg").hide();
			$(".googleMapGroup").find("#storeLocatorAddressEntry").removeClass("sl_input_error_msg");
			var globalRadius = '100';
			if ($("#globalRadius").length) {
				if ($("#globalRadius").val() != "") {
					globalRadius = $("#globalRadius").val();
				}
			}
			getStoreResults(search_term, globalRadius, doFindInStoreSearch, this);
		}
		else {
			$this.parents(".googleMapGroup").find("p.sl_errormsg").text(addressToGetStarted);
			$(".googleMapGroup").find("p.sl_wrongsearch_errormsg").hide();
			$this.parents(".googleMapGroup").find("p.sl_errormsg").show();
			$(".googleMapGroup").find("#storeLocatorAddressEntry").addClass("sl_input_error_msg");
			$this.parents(".googleMapGroup").find("#storeLocatorAddressEntry").focus();
		}

	});

	/* home header page tab Nav */
	$(document).on('click', '.sl_parent #sl_store-locator-results-tabs li', function (e) {

		/*
		var $bru_ref = ($(".sl_parent .store-locator-pick-store").find(".bru-results").length) ? $(".store-locator-pick-store").find(".bru-results") : $(".store-locator-pick-store").find(".BRUresults");
		var $tru_ref = ($(".sl_parent .store-locator-pick-store").find(".tru-results").length) ? $(".store-locator-pick-store").find(".tru-results"): $(".store-locator-pick-store").find(".TRUresults");
		*/

		var $ulRef = $bru_ref.parent();
		//var $allRef = $ulRef.find("li");


		/*var bru_length=($(".store-locator-pick-store").find(".bru-results").length) ? $(".store-locator-pick-store").find(".bru-results").length : $(".store-locator-pick-store").find(".BRUresults").length;
		var tru_length= ($(".store-locator-pick-store").find(".tru-results").length) ? $(".store-locator-pick-store").find(".tru-results").length : $(".store-locator-pick-store").find(".TRUresults").length;*/

		var tru_length = $tru_ref.length;
		var bru_length = $bru_ref.length;
		var all_length = $allRef.length;

		/*if(tru_length >20)
			tru_length--;*/

		$bru_ref.hide();
		$tru_ref.hide();


		/*
		var ref_count = $ref.length;
		lms_counter = (lms_counter * lms_per_pagination < max_limit) ? (lms_counter * lms_per_pagination) : max_limit;
		if(lms_counter > ref_count){
			$('.sl_parent .sl_show-more-stores').text("SHOW LESS");
			lms_counter = lms_per_pagination;
		}else {
			$('.sl_parent .sl_show-more-stores').text("SHOW MORE");
		}
		*/

		if ($(this).find("a").hasClass("tru_results_tab")) {
			if (tru_length > 0) {
				$tru_ref.removeClass('bru-results').addClass('tru-results')
				load_more_pagination($tru_ref, lms_tru_page_count, tru_length);
				$(".sl_root").find("#nearby-stores").show();
				$(".sl_root").find(".no_specific_chain_stores").hide();
			} else {
				$(".sl_root").find("#nearby-stores").hide();
				$(".sl_root").find(".no_specific_chain_stores").text($("#tru_no_results_error").val()).show();
			}
		} else if ($(this).find("a").hasClass("bru_results_tab")) {
			if (bru_length > 0) {
				$bru_ref.removeClass('tru-results').addClass('bru-results')
				load_more_pagination($bru_ref, lms_bru_page_count, bru_length);
				$(".sl_root").find("#nearby-stores").show();
				$(".sl_root").find(".no_specific_chain_stores").hide();
			} else {
				$(".sl_root").find("#nearby-stores").hide();
				$(".sl_root").find(".no_specific_chain_stores").text($("#bru_no_results_error").val()).show();
			}
		} else {
			$(".sl_root").find("#nearby-stores").show();
			$(".sl_root").find(".no_specific_chain_stores").hide();
			$allRef.filter(".bru-tru-results").removeClass('bru-results').addClass('tru-results')
			load_more_pagination($allRef, lms_all_page_count, all_length);
		}
		e.stopPropagation();
	});

	/*  Home Use in Header Find Store */
	$(document).on('click', '.sl_parent .sl_show-more-stores,#store-locator-results .tse-content .show-more', function () {
		var $a_ref = $(document).find("#sl_store-locator-results-tabs").find(".active a");


		// First approach calling the function

		/*
			var $bru_ref = ($(".store-locator-pick-store").find(".bru-results").length) ? $(".store-locator-pick-store").find(".bru-results") : $(".store-locator-pick-store").find(".BRUresults");
			var $tru_ref = ($(".store-locator-pick-store").find(".tru-results").length) ? $(".store-locator-pick-store").find(".tru-results"): $(".store-locator-pick-store").find(".TRUresults");
			var $ulRef = $bru_ref.parent();
			
			
			var tru_length=$tru_ref.length;
			var bru_length=$bru_ref.length;
			var all_length = bru_length + tru_length;
			$bru_ref.hide();
			$tru_ref.hide();
			if ($(this).find("a").hasClass("tru_results_tab")) {
				lms_tru_page_count+= 1;
				load_more_pagination($tru_ref, lms_tru_page_count, tru_length);
			} else if ($(this).find("a").hasClass("bru_results_tab")) {
				lms_bru_page_count+= 1;
				load_more_pagination($bru_ref, lms_bru_page_count, bru_length);
			} else {
				lms_all_page_count+= 1;
				load_more_pagination($allRef, lms_all_page_count, all_length);
			}
		*/


		//Second approach triggering the click
		if ($a_ref.hasClass("tru_results_tab")) {
			lms_tru_page_count += 1;
		} else if ($a_ref.hasClass("bru_results_tab")) {
			lms_bru_page_count += 1;
		} else {
			lms_all_page_count += 1;
		}
		$(document).find("#sl_store-locator-results-tabs").find(".active").trigger("click");


	});

	//Code for removing the jump when a modal is opened on top of a modal
	$(document).on("hidden.bs.modal", ".modal", function () {
		if ($("body").find(".modal-backdrop.in").length) {
			var modalDisplay = $("body").find(".modal-backdrop.in").parent(".modal.in").css("display");
			if (modalDisplay == 'block') {
				$("html").addClass("modal-open");
				//$("html").css("padding-right","17px");
			}
		}
	});
	$(document).on("show.bs.modal", "#myAccountAddCreditCardModal", function () {
		$(window).scrollTop(0);
	});


	$(document).on("click", ".showGoogleMapButton", function () {
		/*	not in scope */
		/* 
			var ShowMapStore = $(this).parent().find("#hdnStoreID").val()
			setCookie('ShowMapStore', ShowMapStore);
			
			sessionStorage.getMapFromJSP = true;
			window.location.assign($(this).data("href"));
		*/
	});

	$(document).on("click", ".find-in-store-single h4", function () {
		$(this).closest(".find-in-store-single").find(".findinstore-details").click();
	});

	$(document).mouseup(function (e) {
		if ($(".find-in-store-overlay").length) {
			var storeDetails = $(".find-in-store-overlay").find("#store-locator-tooltip-store-detailsPDP");
			if (!storeDetails.is(e.target)
				&& storeDetails.has(e.target).length === 0) {
				storeDetails.hide();
			}
		}
	});

	$(document).on('click', function (e) {
		var element = $(e.target);
		if (element.parents('.popover').length > 0) {
			hideAllLearnMorePopovers();
			hideAllLearnMorePopoversPDP();
			hideAllLearnMorePopoversHeader();
		}
	});
	$(document).on("hide.bs.popover", '.your-store-locator-tooltip', function () {
		hideAllLearnMorePopovers();
	});
	/*$(document).on("hide.bs.popover",'#findInStoreModal .learnmorePDPdetails',function(){
		hideAllLearnMorePopoversPDP();
	});*/
	$(document).on("hide.bs.modal", '#findInStoreModal', function () {
		if ($("#findInStoreModal").hasClass("cartItemFlag")) {
			$("#findInStoreModal").removeClass("cartItemFlag");
		}
	});


	$("body").on("click", ".googleMapGroup .store-locator-results-single", function () {
		//setTimeout(function(){
		$("body").find(".info_box_dismiss").focus();
		//},50)
	});
	checkDefaultStore();
	/*crossDomainCookieIframeLoader();*/
	/*fetchDesktopCookieValue();*/
	$(document).on("show.bs.modal", ".modal", function () {
		$("html").addClass("modal-open");
		if ($('.sticky-price').css('position') == "fixed") {
			$('.sticky-price').addClass('header-pos-abs');
		}
		$('.oo_feedback_float').addClass('header-pos-abs');
		$("#scrollToTop").css('z-index', "99");
		/*$('.fixed-narrow-menu.sticky').addClass('header-pos-abs').css('z-index',"99");*/
		$(".shopping-cart-template #order-summary-block").css('right', "37px");

	});
	$(document).on("hidden.bs.modal", ".modal", function () {
		if ($("body").find(".modal-backdrop.in").length) {
			var modalDisplay = $("body").find(".modal-backdrop.in").parent(".modal.in").css("display");
			if (modalDisplay == 'block') {
				$("html").addClass("modal-open");
				$("body").css("padding-right", "17px")
			}
		} else {
			$("html").removeClass("modal-open");
			$('.col-md-12.col-no-padding .fixed-nav').removeClass('header-pos-abs');
			$('#scrollToTop').removeClass('header-pos-abs').css("z-index", "99999");
			$('.fixed-narrow-menu.sticky').removeClass('header-pos-abs').css('z-index', "999");
			$('.sticky-price').removeClass('header-pos-abs');
			$('.oo_feedback_float').removeClass('header-pos-abs');
			$(".shopping-cart-template #order-summary-block").css('right', "20px");
		}
	});
	$(document).on("click", ".add-to-cart", function () {
		if ($(this).parent(".addToCartJspSection").length) {
			if (!$("#productIdForCompare").length) {
				var productIdForCompare = document.createElement("INPUT");
				productIdForCompare.setAttribute("type", "hidden");
				$(productIdForCompare).attr("id", "productIdForCompare");
				document.body.appendChild(productIdForCompare);
			}
			if ($(this).parent(".addToCartJspSection").find(".compareProductId").val()) {
				$("#productIdForCompare").val($(this).parent(".addToCartJspSection").find(".compareProductId").val());
			}
		}
	});
});
/// END $(document).ready(function()


// Function for displaying the pagination stores
function load_more_pagination($ref, lms_counter, max_limit) {


	var temp_counter = 0;
	lms_counter = (lms_counter * lms_per_pagination < max_limit) ? (lms_counter * lms_per_pagination) : max_limit;
	var loadmoreCounter = (lms_counter != max_limit) ? (parseInt(lms_counter / 10) - 1) * 10 : (parseInt(lms_counter / 10)) * 10;
	if (max_limit <= lms_counter || max_limit < 11) {
		$(".sl_show-more-stores").hide();
	}
	else {
		$(".sl_show-more-stores").show();
	}
	$ref.each(function () {
		if (temp_counter < lms_counter) {
			$(this).show();
			if (loadmoreCounter <= temp_counter && lms_counter >= temp_counter) {
				loadMoreOmnitureCalls(false, $(this));
			}
		} else {
			return false;
		}
		temp_counter++;
	});

	$ref.parents(".sl_parent").find(".sl_store_count").text(max_limit);
	$('.sl_parent').find('.tse-scrollable').TrackpadScrollEmulator('recalculate');


}

//For populating the store details
function populateStoreLocatorLanding(resultStores, $this) {

	stores = resultStores;

	$('.selectedaddress').text($('#storeLocatorAddressEntry').val());
	$('.store-count').text(stores.length);
	$('#store-locator-results-scrollable .tse-scroll-content .tse-content ul li').remove();
	//Assigning values based on the condition check for latitude and longitude

	if (stores.length) {
		latitude = stores[0].latitude;
		longitude = stores[0].longitude;
		zoom_percentage = 11;
		showallstores = false;
	}

	initialize();

	///DefaultPopup();

}

function updateMaps() {

}

var markers = [];
var infoBoxes = [];
var visibleMarkers = [];
var allVisibleMarkers = [];
var truVisibleMarkers = [];
var bruVisibleMarkers = [];
var allCurrentDisplayCount = 0;
var tRUCurrentDisplayCount = 0;
var bRUCurrentDisplayCount = 0;
var bound = false;
var loadcurrentData;

function initialize() {
	$("body").addClass("scrollbarFixIE");
	markers = [];
	allVisibleMarkers = [];
	truVisibleMarkers = [];
	bruVisibleMarkers = [];
	loadcurrentData = 0;
	if (keepMapStatus == false) {
		loadcurrentData = 9;
		var numberVisible = 10;
		allCurrentDisplayCount = numberVisible;
		tRUCurrentDisplayCount = numberVisible;
		bRUCurrentDisplayCount = numberVisible;
	}

	//FILTER TABS
	var filterAll = $('#store-locator-results #filterAll');
	var filterTRU = $('#store-locator-results #filterTRU');
	var filterBRU = $('#store-locator-results #filterBRU');
	if (activeTab == "" || activeTab == 'showAll') {
		filterAll.addClass('active');
		filterTRU.removeClass('active');
		filterBRU.removeClass('active');
	} else if (activeTab == 'showTRU') {
		filterAll.removeClass('active');
		filterTRU.addClass('active');
		filterBRU.removeClass('active');
	} else if (activeTab == 'showBRU') {
		filterAll.removeClass('active');
		filterTRU.removeClass('active');
		filterBRU.addClass('active');
	}

	var favStoreId = getCookie("favStore");
	favStoreId = favStoreId.split("|");
	favStoreId = favStoreId[1];

	var mapOptions = {
		zoom: zoom_percentage,
		center: new google.maps.LatLng(latitude, longitude),
		zoomControl: true,
		zoomControlOptions: {
			position: google.maps.ControlPosition.RIGHT_BOTTOM,
			style: google.maps.ZoomControlStyle.SMALL
		},
		panControl: false,
		streetViewControl: false,
		mapTypeControl: false,
		scrollwheel: false
	};
	/*if(!stores.length){
		mapOptions = {
				zoom: zoom_percentage,
				center: new google.maps.LatLng(latitude, longitude),
				zoomControl: false,
				zoomControlOptions: {
					position: google.maps.ControlPosition.RIGHT_BOTTOM,
					style: google.maps.ZoomControlStyle.SMALL
				},
				panControl: false,
				streetViewControl: false,
				mapTypeControl: false,
				scrollwheel: false,
				draggable: false
		};
	}*/
	if (zoom_percentage == 4) {
		mapOptions = {
			zoom: zoom_percentage,
			center: new google.maps.LatLng(latitude, longitude),
			zoomControl: false,
			zoomControlOptions: {
				position: google.maps.ControlPosition.RIGHT_BOTTOM,
				style: google.maps.ZoomControlStyle.SMALL
			},
			panControl: false,
			streetViewControl: false,
			mapTypeControl: false,
			scrollwheel: false,
			draggable: false,
			disableDoubleClickZoom: true
		};
	}



	//$("#googleMap").html("");
	/*if (keepMapStatus == false){*/
	if (!firstLoad) {
		map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);
		firstLoad = true;
	} else {
		var map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);
	}


	var userPosition = window.TRUImagePath + 'images/your-here-full.png';

	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function showUserPosition(position) {
			var marker = new MarkerWithLabel({
				position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
				draggable: false,
				map: map,
				labelContent: "",
				clickable: false,
				icon: userPosition
			});

		});

	}
	/*keepMapStatus == true;*/
	/*}*/
	$("#store-locator-results").show();
	if ((zoom_percentage == 4)) {
		$(".googleMapGroup").find("#store-locator-results-tabs").hide();
		$(".googleMapGroup").find("#store-locator-results-scrollable").hide();
		$(".store-locator-find-store").css("padding-left", 20);
		$(".store-locator-find-store").show();
		$("#stores_count_display").hide();
		$(".nearby-stores-find-a-store.store-locator-zip").show();


	} else {
		$(".nearby-stores-find-a-store.store-locator-zip").css("display", "none");
		$(".googleMapGroup").find("#stores_count_display, #store-locator-results-tabs, #store-locator-results-scrollable").css("display", "block");
		$(".googleMapGroup").find(".store-locator-zip.nearby-stores-find-a-store").css("display", "none");

	}
	// fix for TOYSRDS-897
	//var youAreHere = new google.maps.Marker({
	//                            position: new google.maps.LatLng(25.7877, -80.2241),
	//                            icon: 'toolkit/images/store-locator/you-are-here.png',
	//                            map: map
	//            });


	var truMyStore = window.TRUImagePath + 'images/my_TRU.png';
	var bruMyStore = window.TRUImagePath + 'images/my_BRU.png';
	var truMyStoreDot = window.TRUImagePath + 'images/my_TRU_Dot.png';
	var bruMyStoreDot = window.TRUImagePath + 'images/my_BRU_Dot.png';
	var truMarkers = window.TRUImagePath + 'images/tru_markers/Numbered-Locations_';
	var bruMarkers = window.TRUImagePath + 'images/bru_markers/Numbered-Locations_';
	var truDots = window.TRUImagePath + 'images/tru_dots/Numbered-Dots_';
	var bruDots = window.TRUImagePath + 'images/bru_dots/Numbered-Locations_';

	var dismissMarkerURL = window.TRUImagePath + 'images/tru_dots/close.png';


	var storeResults = document.getElementById('store-locator-results');
	var findStore = document.getElementById('store-locator-find-store');
	var storeResultsSingle = $('.store-locator-results-single');

	var showMoreStores = $('#show-more-stores');



	//infobox options
	/*  var myOptions = {
		 content: storeDetails
		,disableAutoPan: false
		,closeBoxURL: dismissMarkerURL
		,closeBoxMargin: '20px 20px 0px 0px'
		,pixelOffset: new google.maps.Size(-280, -75)
		,infoBoxClearance: new google.maps.Size(350, 16)
		,alignBottom: true
		,isHidden: false
		,enableEventPropagation: false
	  };*/
	var favStoreId = getCookie("favStore");
	favStoreId = favStoreId.split("|");
	favStoreId = favStoreId[1];
	var numbering_flag_fav_store = false;
	if (stores.length) {
		if (allStore[0].StoreID == favStoreId) {
			numbering_flag_fav_store = true;
		}

		$(".no_specific_chain_stores").hide();
		$("#store-locator-results-scrollable").show();

		//for omniture calls extracting the last 10 stores
		if (activeTab === '' || activeTab === 'showAll') {
			loadcurrentData = allCurrentDisplayCount - 1;
		}
		else if (activeTab === 'showTRU') {
			loadcurrentData = tRUCurrentDisplayCount - 1;
		}
		else if (activeTab === 'showBRU') {
			loadcurrentData = bRUCurrentDisplayCount - 1;
		}
		loadcurrentData = (loadcurrentData > stores.length) ? stores.length : loadcurrentData;
		var remaningStores = ((loadcurrentData + 1) % 10 == 0) ? 10 : (loadcurrentData % 10) + 1;
		var minOmniCount = loadcurrentData - remaningStores;


		var isBruSite = false;
		if ($('.tru-logo-sm').length > 0 || $('.bru-shopping-cart').length > 0) {
			isBruSite = true;
		}
		//add markers, marker icons, and marker events to map
		for (i = 0; i < stores.length; i++) {

			var store = stores[i];
			var markerIcon = '';
			var dotIcon = '';
			if (store.address1 != null) {
				if (store.address2 != null) {
					store.StreetAddress = store.address1 + "<br>" + store.address2;
				} else {
					store.StreetAddress = store.address1;
				}
			}


			$(".store-locator-store-details-infowindow h2 .store-loc").html(store.name);
			$(".store-locator-store-details-infowindow .store-address").prepend("<p>" + store.StreetAddress + "</p><p>" + store.city + ", " + store.stateAddress + " " + store.postalCode + "</p><p>" + store.phoneNumber + "</p>");
			$(".store-locator-store-details-infowindow .store-address a").attr("onclick", "javascript:getDrivingDirections('" + store.StreetAddress + "','" + store.city + "','" + store.postalCode + "','" + store.latitude + "','" + store.longitude + "','" + store.name + "');");
			/*$(".store-locator-store-details-infowindow").find("#");*/
			$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#daysInfoBox, #timingsInfoBox").html("");

			/*$(".store-locator-store-details-infowindow").find(".store-miles #makeThisMyStoreInfoBox").html("");
			$(".store-locator-store-details-infowindow").find(".store-miles #makeThisMyStoreInfoBox").append("make this my store");*/

			var storeName = convertToHtmlEntities(store.name);
			$(".store-locator-store-details-infowindow").find(".store-miles #makeThisMyStoreInfoBox").attr("onclick", "javascript:makeMyStore(event,'" + contextPath + "','" + store.StoreID + "','" + store.postalCode + "', '" + store.city + "' ,this,'" + storeName + "','" + store.distance + "','" + store.signature + "');");



			$(".store-locator-store-details-infowindow").find(".store-miles #makeThisMyStoreInfoBox").attr("data-address", store.StreetAddress);
			$(".store-locator-store-details-infowindow").find(".store-miles #makeThisMyStoreInfoBox").attr("data-mms-map-infoBox", store.StoreID);

			if (!store.isEligibleForfavStore) {
				$(".store-locator-store-details-infowindow").find(".store-miles #makeThisMyStoreInfoBox").css("display", "none");
				$(".store-locator-store-details-infowindow").find(".store-miles .my-store-span .my-store").css("display", "none");
			} else {

				if (store.StoreID === favStoreId) {
					$(".store-locator-store-details-infowindow").find(".store-miles #makeThisMyStoreInfoBox").css("display", "none");
					$(".store-locator-store-details-infowindow").find(".store-miles .my-store-span .my-store").css("display", "inline-block");
				} else {
					$(".store-locator-store-details-infowindow").find(".store-miles #makeThisMyStoreInfoBox").css("display", "inline-block");
					$(".store-locator-store-details-infowindow").find(".store-miles .my-store-span .my-store").css("display", "none");

				}
			}

			$(".store-locator-store-details-infowindow").find("#distanceInMilesInfoBox").text(store.distance);
			if (store.hasOwnProperty('storeHours')) {
				/*if(store.showTingsFromMonToFriInOneLine){
					$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#daysInfoBox").append("<p>Mon-Fri:</p>");
					for ( item in store.storeHours ){
						if(item == "MON"){
							$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#timingsInfoBox").append("<p>"+store.storeHours[item]+"</p>");
						}
					}
					for ( item in store.storeHours ){
						if(item == "SAT"){
							$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#daysInfoBox").append("<p>Sat:</p>");
							$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#timingsInfoBox").append("<p>"+store.storeHours[item]+"</p>");
						}else if(item == "SUN"){
							$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#daysInfoBox").append("<p>Sun:</p>");
							$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#timingsInfoBox").append("<p>"+store.storeHours[item]+"</p>");
						}
					}
				}else{
					for ( item in store.storeHours ){
						$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#daysInfoBox").append("<p>"+item+"</p>");
						$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#timingsInfoBox").append("<p>"+store.storeHours[item]+"</p>");
					}
					if(store.storeHours.hasOwnProperty('MON')){
						$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#daysInfoBox").append("<p>Mon:</p>");
						$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#timingsInfoBox").append("<p>"+store.storeHours.MON+"</p>");
					}
					if(store.storeHours.hasOwnProperty('TUE')){
						$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#daysInfoBox").append("<p>Tue:</p>");
						$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#timingsInfoBox").append("<p>"+store.storeHours.TUE+"</p>");
					}
					if(store.storeHours.hasOwnProperty('WED')){
						$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#daysInfoBox").append("<p>Wed:</p>");
						$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#timingsInfoBox").append("<p>"+store.storeHours.WED+"</p>");
					}
					if(store.storeHours.hasOwnProperty('THU')){
						$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#daysInfoBox").append("<p>Thu:</p>");
						$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#timingsInfoBox").append("<p>"+store.storeHours.THU+"</p>");
					}
					if(store.storeHours.hasOwnProperty('FRI')){
						$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#daysInfoBox").append("<p>Fri:</p>");
						$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#timingsInfoBox").append("<p>"+store.storeHours.FRI+"</p>");
					}
					if(store.storeHours.hasOwnProperty('SAT')){
						$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#daysInfoBox").append("<p>Sat:</p>");
						$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#timingsInfoBox").append("<p>"+store.storeHours.SAT+"</p>");
					}
					if(store.storeHours.hasOwnProperty('SUN')){
						$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#daysInfoBox").append("<p>Sun:</p>");
						$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#timingsInfoBox").append("<p>"+store.storeHours.SUN+"</p>");
					}
				}*/
				var storeHoursText = constructStoreHoursHtml(store);
				for (var item = 0; item < storeHoursText.weekday.length; item++) {
					$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#daysInfoBox").append("<p>" + storeHoursText.weekday[item] + ":</p>");
					$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#timingsInfoBox").append("<p>" + storeHoursText.weekTimings[item] + "</p>");;
				}
			}

			/* Start: Store service list */
			var servicesInfo = '';
			$.each(store.storeServices, function (key, val) {
				servicesInfo += '<li id="layawayInfoBox">';
				servicesInfo += store.storeServices[key].serviceName;
				servicesInfo += '<p>' + store.storeServices[key].serviceDescription + '</p>';
				//servicesInfo += '<a href="javascript:void(0);" class="learn-more-tooltip" data-original-title="" data-content="'+store.storeServices[key].learnMore+'" title="">learn more</a>';

				if (typeof store.storeServices[key].learnMore !== "undefined" && store.storeServices[key].learnMore !== null && store.storeServices[key].learnMore !== "") {
					//servicesInfo += '<a href="javascript:void(0);" class="learn-more-tooltip" data-original-title="" data-content="'+store.storeServices[key].learnMore+'" title="">learn more</a>';
					servicesInfo += '<a href="'+window.location.protocol+'//www.toysrus.com/helpdesk/index.jsp?display=payment&index=15&subdisplay=options#layaway" target="_blank" title="">learn more</a>';
				}
				servicesInfo += '</li>';
			});

			$(".store-locator-store-details-infowindow").find('#storeServicesInfoBox').html(servicesInfo);
			if (servicesInfo.length === 0) {
				$(".store-locator-store-details-infowindow").find('#storeServicesInfoBox').parent().hide()
			} else {
				$(".store-locator-store-details-infowindow").find('#storeServicesInfoBox').parent().show()
			}

			/* End: Store service list */

			if (store.closingTimeMsg != "undefined" && store.closingTimeMsg != null) {
				$(".store-locator-store-details-infowindow").find("#closingTimeMsgInfoBox").html("<span>&#xb7;</span>" + store.closingTimeMsg);
			} else {
				$(".store-locator-store-details-infowindow").find("#closingTimeMsgInfoBox").html("");
			}



			//$(".store-locator-store-details-infowindow").find("#storeServicesInfoBox").html("");
			//store.storeServices=JSON.parse([{"learnMore":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat","serviceDescription":"YOUR HERE","serviceName":"New york"},{"learnMore":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat","serviceDescription":"Informative classes on everything baby from breastfeeding to infant CPR","serviceName":"parenting classes"},{"learnMore":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat","serviceDescription":"Schedule a one-on-one appointment with a Personal Registry Advisor to help you create the perfect registry","serviceName":"personal registry advisor"}]);


			/* if(store.hasOwnProperty('storeServices')){
				 for(i=0;i<store.storeServices.length;i++){			
							$(".store-locator-store-details-infowindow").find("#storeServicesInfoBox").append("<li id='"+ store.storeServices[i].serviceName +"'>"+ store.storeServices[i].serviceName + " <p>"+store.storeServices[i].serviceDescription+"</p><a href='#' class='learn-more-tooltip' data-original-title='"+store.storeServices[i].learnMore+"'>learn more</a></li>");
				 }
			}
*/


			/*$(".store-locator-store-details-infowindow").find("#storeHoursInfoBox").find("#daysInfoBox").text(store.distanceInMiles);*/



			var storeDetails = $('.store-locator-store-details-infowindow').html();
			var myOptions = {
				content: storeDetails,
				disableAutoPan: false,
				closeBoxURL: dismissMarkerURL,
				closeBoxMargin: '20px 20px 0px 0px',
				pixelOffset: new google.maps.Size(-280, -75),
				infoBoxClearance: new google.maps.Size(350, 16),
				alignBottom: true,
				isHidden: false,
				enableEventPropagation: false
			};

			$(".store-locator-store-details-infowindow .store-address p").remove();
			$(".store-locator-store-details-infowindow h3 .store-loc").empty();


			if (store.chainCode === 'TRU') {

				markerIcon = '' + truMarkers + 'marker.png';

				if (favStoreId === store.StoreID) {
					markerIcon = truMyStore
				}
			} else if (store.chainCode === 'BRU') {

				markerIcon = '' + bruMarkers + 'marker.png';
				if (favStoreId === store.StoreID) {
					markerIcon = bruMyStore
				}
			}


			if (store.isSuperStoreServiceAval === true) {
				if (activeTab === '' || activeTab === 'showAll') {
					markerIcon = '' + truMarkers + 'marker.png';
					//loadcurrentData = allCurrentDisplayCount -1; 
					if (favStoreId === store.StoreID) {
						markerIcon = truMyStore
					}
				}
				else if (activeTab === 'showTRU') {
					markerIcon = '' + truMarkers + 'marker.png';
					///loadcurrentData = tRUCurrentDisplayCount -1;
					if (favStoreId === store.StoreID) {
						markerIcon = truMyStore
					}
				}
				else if (activeTab === 'showBRU') {
					markerIcon = '' + bruMarkers + 'marker.png';
					///loadcurrentData = bRUCurrentDisplayCount -1 ;
					if (favStoreId === store.StoreID) {
						markerIcon = bruMyStore
					}
				}
			}



			var storeResultItem = appendStoreResultItem(i, store, store.chainCode, numbering_flag_fav_store);
			if (store.chainCode === 'TRU') {
				if (store.isSuperStoreServiceAval == true && activeTab == 'showBRU') {
					storeResultItem.addClass('bru-results');
					storeResultItem.find('#addressicon').removeClass('truDots').addClass('bruDots');
				}
				else {
					storeResultItem.addClass('tru-results');
					storeResultItem.find('#addressicon').removeClass('bruDots').addClass('truDots');
				}

			} else if (store.chainCode === 'BRU') {
				if (store.isSuperStoreServiceAval == 'true' && activeTab == 'showBRU') {
					storeResultItem.addClass('tru-results');
					storeResultItem.find('#addressicon').removeClass('bruDots').addClass('truDots');
				}
				else {
					storeResultItem.addClass('bru-results');
					storeResultItem.find('#addressicon').removeClass('truDots').addClass('bruDots');
				}
			}
			var marker;
			if (numbering_flag_fav_store) {
				marker = new MarkerWithLabel({
					position: new google.maps.LatLng(store.latitude, store.longitude),
					map: map,
					icon: markerIcon,
					draggable: false,
					labelContent: (favStoreId === store.StoreID) ? '' : parseInt(store.sequenceIndex),
					labelClass: (i > 9) ? ((i > 98) ? 'googleiconlabel2' : 'googleiconlabel1') : 'googleiconlabel', // the CSS class for the label
					labelInBackground: true
				});
			} else {
				marker = new MarkerWithLabel({
					position: new google.maps.LatLng(store.latitude, store.longitude),
					map: map,
					icon: markerIcon,
					draggable: false,
					labelContent: (favStoreId === store.StoreID) ? '' : parseInt(store.sequenceIndex) + 1,
					labelClass: (i > 8) ? ((i > 98) ? 'googleiconlabel2' : 'googleiconlabel1') : 'googleiconlabel', // the CSS class for the label
					labelInBackground: true
				});
			}


			/*var marker = new google.maps.Marker({
				position: new google.maps.LatLng(store.latitude, store.longitude),
				icon: markerIcon,
				map: map
			});*/

			var ib = new InfoBox(myOptions);

			google.maps.event.addListener(ib, 'domready', function () {

				setTSEScrollable();

				setToolTips();

			});

			google.maps.event.addListener(ib, 'closeclick', function () {
				setPanelItemsToUnclicked();
			});

			customAddListener(storeResultItem, marker, store.chainCode);

			if (i > loadcurrentData) {
				storeResultItem.css('display', 'none');
				marker.setVisible(false);
			}

			markers.push({
				"type": store.chainCode,
				"isSuperStoreServiceAval": store.isSuperStoreServiceAval,
				"marker": marker,
				"ib": ib,
				"storeResult": storeResultItem
			});

			/*google.maps.event.addListener(marker, 'click', function() {
				map.panTo(this.getPosition());
				closeInfoBoxes();
				console.log(ib.getContent());
				//ib.open(map, this);
			});*/

			//google.maps.event.addListener(marker, 'click', addCustomMarkerListener(marker, ib, map));

			google.maps.event.addListener(map, 'click', function () {
				closeInfoBoxes();
				setPanelItemsToUnclicked();
			});
			if (moreStoresFlag) {
				if (i > minOmniCount && i <= loadcurrentData) {
					loadMoreOmnitureCalls(false, $(storeResultItem));
					//omniIspuStoreServed(store.StoreID);
				}
			} else if (i <= loadcurrentData) {
				loadMoreOmnitureCalls(false, $(storeResultItem));
			}

		} //end for loop
	} else {

		var error_msg = "";
		if (activeTab === 'showTRU') {
			error_msg = $("#tru_no_results_error").val();
		} else if (activeTab === 'showBRU') {
			error_msg = $("#bru_no_results_error").val();
		}
		$(".no_specific_chain_stores p").text(error_msg);
		$(".no_specific_chain_stores").show();
		$("#store-locator-results-scrollable").hide();
	}


	for (m = 0; m < markers.length; m++) {
		google.maps.event.addListener(markers[m].marker, 'click', function () {
			this.map.panTo(this.getPosition());
			closeInfoBoxes();
			getMarkerWrapper(this).ib.open(this.map, this);
		});
	}

	function getMarkerWrapper(marker) {
		for (m = 0; m < markers.length; m++) {
			if (markers[m].marker.labelContent == marker.labelContent) {
				return markers[m];
			}
		}
	}








	var listStores = storeLocatorScrollable.find('li');
	var hDividers = storeLocatorScrollable.find('hr');



	allVisibleMarkers = markers;

	bruVisibleMarkers = markers.filter(function (item) {
		return (item.type == 'BRU' || item.isSuperStoreServiceAval == true)
	});
	truVisibleMarkers = markers.filter(function (item) {
		return (item.type == 'TRU' || item.isSuperStoreServiceAval == true)
	});


	$("#show-more-stores").show();
	if (activeTab === '' || activeTab === 'showAll') {
		if (loadcurrentData >= allVisibleMarkers.length) {
			$("#show-more-stores").hide();
		}
	}
	else if (activeTab === 'showTRU') {
		if (loadcurrentData >= truVisibleMarkers.length) {
			$("#show-more-stores").hide();
		}
	}
	else if (activeTab === 'showBRU') {
		if (loadcurrentData >= bruVisibleMarkers.length) {
			$("#show-more-stores").hide();
		}
	}

	if (activeTab == "" || activeTab == 'showAll') {
		filterAll.find("a").focus();
	} else if (activeTab == 'showTRU') {
		filterTRU.find("a").focus();
	} else if (activeTab == 'showBRU') {
		filterBRU.find("a").focus();
	}


	if (!bound) {
		showMoreStores.on('click', function () {
			//visibleMarkers = markers;
			/*for (i = 0; i < markers.length; i++) {
				markers[i].marker.setVisible(true);
				markers[i].storeResult.css('display', 'block');
			}*/

			if (filterAll.hasClass('active')) {
				//filterAll.trigger('click');
				//All Tab Show More
				allCurrentDisplayCount += 10;
				/*
				if(allCurrentDisplayCount > markers.length) {
					allCurrentDisplayCount = markers.length;
					$("#show-more-stores").hide();
				}
				
				
				for (i = 0; i < allCurrentDisplayCount; i++) {
					markers[i].marker.labelContent = i + 1;
					markers[i].marker.setVisible(true);
					markers[i].storeResult.css('display', 'block');
				}
				allVisibleMarkers = markers.slice(0, allCurrentDisplayCount);
				*/
			}
			else if (filterTRU.hasClass('active')) {
				//filterTRU.trigger('click');
				//TRU tab Show More
				tRUCurrentDisplayCount += 10;
				/*
				if(tRUCurrentDisplayCount > truVisibleMarkers.length) {
					tRUCurrentDisplayCount = truVisibleMarkers.length;
					$("#show-more-stores").hide();
				}
				
				
				var i=0;
				var totalCount=0;
				while (i < tRUCurrentDisplayCount) {
					if(markers[totalCount].type=='TRU') {
						markers[totalCount].marker.labelContent = i + 1;
						markers[totalCount].marker.setVisible(true);
						markers[totalCount].storeResult.css('display', 'block');
						i++;
					} else {
						markers[totalCount].marker.setVisible(false);
						markers[totalCount].storeResult.css('display', 'none');
					}
					totalCount++;
					
					if(totalCount > markers.length) break;
				}
				truVisibleMarkers = markers.slice(0, totalCount);
				*/
			}
			else if (filterBRU.hasClass('active')) {
				//filterBRU.trigger('click');


				//BRU Tab Show More	
				bRUCurrentDisplayCount += 10;
				/*
				if(bRUCurrentDisplayCount > bruVisibleMarkers.length) {
					bRUCurrentDisplayCount = bruVisibleMarkers.length;
					$("#show-more-stores").hide();
				}
					
				var i=0;
				var totalCount=0;
				while (i < bRUCurrentDisplayCount) {
					if(markers[totalCount].type=='BRU') {
						markers[totalCount].marker.labelContent = i + 1;
						markers[totalCount].marker.setVisible(true);
						markers[totalCount].storeResult.css('display', 'block');
						i++;
					} else {
						markers[totalCount].marker.setVisible(false);
						markers[totalCount].storeResult.css('display', 'none');
					}
					totalCount++;
					
					if(totalCount > markers.length) break;
				}
				bruVisibleMarkers = markers.slice(0, totalCount);
				*/
			}
			moreStoresFlag = true;
			scrollFocusShowMore = $(".googleMapGroup #store-locator-results-scrollable .tse-scroll-content").scrollTop();
		});

		function setTSEScrollable() {
			storeDetailsScrollable = $('#googleMap #store-locator-store-details-scrollable');
			storeDetailsScrollable.TrackpadScrollEmulator({
				autoHide: true
			});
		}



		function customAddListener(r, m, t) {
			google.maps.event.addListener(m, 'click', function () {

				setPanelItemsToUnclicked();

				for (i = 0; i < markers.length; i++) {
					var sr = $(markers[i].storeResult);
					sr.removeClass('result-selected-tru result-selected-bru');
				}
				if (t === 'TRU') {
					$(r).find('.store-locator-results-single').addClass('result-selected-tru');
				} else if (t === 'BRU') {
					$(r).find('.store-locator-results-single').addClass('result-selected-bru');
				}
			});

		}



		filterAll.on('click', function () {
			/*
			for (i = 0; i < markers.length; i++) {
				markers[i].marker.labelContent = i + 1;
				markers[i].marker.setVisible(false);
				markers[i].storeResult.css('display', 'none');
			}
			
			if(allCurrentDisplayCount >= allVisibleMarkers.length) {
				$("#show-more-stores").hide();
			}
			else{
				$("#show-more-stores").show();
			}
			
			if(allCurrentDisplayCount > markers.length) {
				allCurrentDisplayCount = markers.length;
			}
			
			for (i = 0; i < allCurrentDisplayCount; i++) {
				allVisibleMarkers[i].marker.setVisible(true);
				allVisibleMarkers[i].storeResult.css('display', 'block');
				allVisibleMarkers[i].storeResult.find("#addressicon").removeClass("bruDots").addClass("truDots")
				allVisibleMarkers[i].marker.icon = ''+truMarkers + 'marker.png';
			}
			
			closeInfoBoxes();
			setPanelItemsToUnclicked();
			*/
			keepMapStatus = true;
			activeTab = 'showAll';
			populateStoreLocatorLanding(allStore, null);

		}); //end on click

		filterTRU.on('click', function () {
			/*
				for (i = 0; i < markers.length; i++) {
					markers[i].marker.setVisible(false);
					markers[i].storeResult.css('display', 'none');
				}
			
				if(tRUCurrentDisplayCount > markers.length) {
					tRUCurrentDisplayCount = markers.length;
				}
				*/
			/***********truVisibleMarkers = markers.filter(function(item) {
				return (item.type == 'TRU')
			});
			 *************/
			/*
			if(tRUCurrentDisplayCount >= truVisibleMarkers.length) {
				$("#show-more-stores").hide();
			}
			else{
				$("#show-more-stores").show();
			}
			
			for (i = 0; i < tRUCurrentDisplayCount; i++) {
				truVisibleMarkers[i].marker.setVisible(true);
				truVisibleMarkers[i].storeResult.css('display', 'block');
				truVisibleMarkers[i].storeResult.find("#addressicon").removeClass("bruDots").addClass("truDots")
				truVisibleMarkers[i].marker.icon =''+truMarkers + 'marker.png';
			}
				*/
			activeTab = 'showTRU'
			keepMapStatus = true;
			populateStoreLocatorLanding(truStore, null);

			//closeInfoBoxes();
			//setPanelItemsToUnclicked();

		}); //end on click

		filterBRU.on('click', function (index) {
			/*
			for (i = 0; i < markers.length; i++) {
				markers[i].marker.setVisible(false);
				markers[i].storeResult.css('display', 'none');
			}
			
			if(bRUCurrentDisplayCount > markers.length) {
				bRUCurrentDisplayCount = markers.length;
			}
			*/
			/*********
			bruVisibleMarkers = markers.filter(function(item) {
				return (item.type == 'BRU')
			});
			*********/
			/*
			if(bRUCurrentDisplayCount >= bruVisibleMarkers.length) {
				$("#show-more-stores").hide();
			}
			else{
				$("#show-more-stores").show();
			}
			
			for (i = 0; i < bRUCurrentDisplayCount; i++) {
				bruVisibleMarkers[i].marker.setVisible(true);
				bruVisibleMarkers[i].storeResult.css('display', 'block');
				bruVisibleMarkers[i].storeResult.find("#addressicon").removeClass("truDots").addClass("bruDots")
				bruVisibleMarkers[i].marker.icon = ''+bruMarkers + 'marker.png';
			}
			
			closeInfoBoxes();
			setPanelItemsToUnclicked();
			*/
			activeTab = 'showBRU'
			keepMapStatus = true;
			populateStoreLocatorLanding(bruStore, null);

		}); //end on click
		bound = true;
	}

	function setTSEScrollable() {
		storeDetailsScrollable = $('#googleMap #store-locator-store-details-scrollable');
		storeDetailsScrollable.TrackpadScrollEmulator({
			autoHide: true
		});
	}

	function setToolTips() {
		var learnMoreTooltip = $('.learn-more-tooltip');
		learnMoreTooltipTemplate = $('.tooltip-container').html();

		learnMoreTooltip.popover({
			animation: 'true',
			html: 'true',
			placement: 'bottom',
			trigger: 'focus'
		});

		/*learnMoreTooltip.popover({
			animation: 'true',
			html: 'true',
			content: 'asdfasdf',
			placement: 'bottom',
			template: learnMoreTooltipTemplate,
			trigger: 'focus'
		});*/

		learnMoreTooltip.click(function (event) {
			event.preventDefault();
		});

	}

	function customAddListener(r, m, t) {
		google.maps.event.addListener(m, 'click', function () {

			setPanelItemsToUnclicked();

			for (i = 0; i < markers.length; i++) {
				var sr = $(markers[i].storeResult);
				sr.removeClass('result-selected-tru result-selected-bru');
			}
			if (t === 'TRU') {
				$(r).find('.store-locator-results-single').addClass('result-selected-tru');
				$(r).parents(".tse-scroll-content").scrollTop(0);
				var parentOffsetTop = $(r).parents(".tse-scroll-content").offset().top;
				var selectedStoreOffsetTop = $(r).offset().top;
				if (selectedStoreOffsetTop > parentOffsetTop) {
					$(r).parents(".tse-scroll-content").scrollTop(selectedStoreOffsetTop - parentOffsetTop);
				}
				else if (selectedStoreOffsetTop == parentOffsetTop) {

				}
				else {
					$(r).parents(".tse-scroll-content").scrollTop(selectedStoreOffsetTop + (parentOffsetTop - selectedStoreOffsetTop));
				}
			}
			else if (t === 'BRU') {
				$(r).find('.store-locator-results-single').addClass('result-selected-bru');
				$(r).parents(".tse-scroll-content").scrollTop(0);
				var parentOffsetTop = $(r).parents(".tse-scroll-content").offset().top;
				var selectedStoreOffsetTop = $(r).offset().top;
				if (selectedStoreOffsetTop > parentOffsetTop) {
					$(r).parents(".tse-scroll-content").scrollTop(selectedStoreOffsetTop - parentOffsetTop);
				}
				else if (selectedStoreOffsetTop == parentOffsetTop) {

				}
				else {
					$(r).parents(".tse-scroll-content").scrollTop(selectedStoreOffsetTop + (parentOffsetTop - selectedStoreOffsetTop));
				}
			}
		});

	}

	//CLOSE ALL INFOBOXES
	function closeInfoBoxes() {
		if (markers.length) {
			for (i = 0; i < markers.length; i++) {
				markers[i].ib.close();
			}
		}
	}

	//REMOVE CLICKED STATE CLASSES FROM STORE RESULTS PANEL ITEMS
	function setPanelItemsToUnclicked() {
		$('#store-locator-results .store-locator-results-single').each(function () {
			$(this).removeClass('result-selected-tru result-selected-bru');
		});
	}

	//CREATE LI ELEMENTS AND APPEND TO UL IN STORE RESULTS PANEL
	function appendStoreResultItem(i, store, type, numbering_flag_fav_store) {
		var listElement = $(document.createElement('li'));
		var resultsSingleContent = $('.results-single-outer');
		var sequenceIndex = (numbering_flag_fav_store) ? store.sequenceIndex - 1 : store.sequenceIndex;
		/*if(numbering_flag_fav_store){
			sequenceIndex = store.sequenceIndex-1;
		}else{
			sequenceIndex = store.sequenceIndexl;
		}*/
		/*if (type === 'TRU') {
			listElement.addClass("tru-results");
		}
		else{
			listElement.addClass("rru-results");
		}*/
		var favStoreId = getCookie("favStore");
		favStoreId = favStoreId.split("|");
		favStoreId = favStoreId[1];
		var storeName = convertToHtmlEntities(store.name);
		if (store.StoreID === favStoreId) {
			resultsSingleContent.find('.make-my-store').hide();
			resultsSingleContent.find('.my-store').show();
			resultsSingleContent.find('h4').html(store.name);
			resultsSingleContent.find('p.address').html(store.StreetAddress);
			resultsSingleContent.find('span.miles').html(store.distance);
			resultsSingleContent.find('a.make-my-store').attr('onclick', "javascript:makeMyStore(event,'" + contextPath + "','" + store.StoreID + "','" + store.postalCode + "','" + store.city + "',this,'" + storeName + "','" + store.distance + "','" + store.signature + "');");


			/*resultsSingleContent.find('#addressicon').attr('class', 'truMyStoreDot');*/


		} else {
			resultsSingleContent.find('.make-my-store').hide();
			resultsSingleContent.find('.my-store').hide();
			resultsSingleContent.find('h4').html(store.name);
			resultsSingleContent.find('p.address').html(store.StreetAddress);
			resultsSingleContent.find('span.miles').html(store.distance);
			resultsSingleContent.find('a.make-my-store').attr('onclick', "javascript:makeMyStore(event,'" + contextPath + "','" + store.StoreID + "','" + store.postalCode + "', '" + store.city + "' ,this,'" + storeName + "','" + store.distance + "','" + store.signature + "');");

			/*resultsSingleContent.find('#addressicon').attr('class', 'truDots');
			resultsSingleContent.find('#addressicon').html(i+1);*/

		}
		resultsSingleContent.find('a.make-my-store').attr('data-mms-map-pageContent', store.StoreID);

		if (!store.isEligibleForfavStore) {
			resultsSingleContent.find('a.make-my-store').css("display", "none");
		} else {
			resultsSingleContent.find('a.make-my-store').css("display", "block");
			if (store.StoreID === favStoreId) {
				resultsSingleContent.find('a.make-my-store').css("display", "none");
			}
		}
		if (type === 'TRU') {
			listElement.on('click', function () {
				$(this).find('.store-locator-results-single').addClass('result-selected-tru');
				$(this).siblings().find('.store-locator-results-single').removeClass('result-selected-tru result-selected-bru');
			});

			if (store.StoreID === favStoreId) {
				resultsSingleContent.find('#addressicon').attr('class', 'truMyStoreDot');
				resultsSingleContent.find('#addressicon').html('')
			} else {
				resultsSingleContent.find('#addressicon').attr('class', 'truDots');
				resultsSingleContent.find('#addressicon').html(parseInt(sequenceIndex) + 1);

			}



		} else if (type === 'BRU') {
			listElement.on('click', function () {
				$(this).find('.store-locator-results-single').addClass('result-selected-bru');
				$(this).siblings().find('.store-locator-results-single').removeClass('result-selected-tru result-selected-bru');
			});

			if (store.StoreID === favStoreId) {
				resultsSingleContent.find('#addressicon').attr('class', 'bruMyStoreDot');
				resultsSingleContent.find('#addressicon').html('')
			} else {
				resultsSingleContent.find('#addressicon').attr('class', 'bruDots');
				resultsSingleContent.find('#addressicon').html(parseInt(sequenceIndex) + 1);
			}


			/*resultsSingleContent.find('#addressicon').attr('class', 'bruDots');
			resultsSingleContent.find('#addressicon').html(i+1);*/

		}

		if (store.isSuperStoreServiceAval === true) {
			if (activeTab === '' || activeTab === 'showAll') {
				resultsSingleContent.find("#addressicon").removeClass("bruDots").addClass("truDots")
			}
			else if (activeTab === 'showTRU') {
				resultsSingleContent.find("#addressicon").removeClass("bruDots").addClass("truDots")
			}
			else if (activeTab === 'showBRU') {
				resultsSingleContent.find("#addressicon").removeClass("truDots").addClass("bruDots")
			}
		}

		resultsSingleContent.find(".storeIdOmni").val(store.StoreID);
		listElement.append(resultsSingleContent.html());
		listElement.append("<hr>")
		/*if (store.StoreID === favStoreId) {
			$('#store-locator-results-scrollable ul').prepend(listElement);
		} else {*/
		$('#store-locator-results-scrollable ul').append(listElement);
		/*}*/
		if (!store.isEligibleForfavStore) {
			listElement.find('a.make-my-store').remove();
		}
		if (store.closingTimeMsg != "undefined" && store.closingTimeMsg != null) {
			listElement.find("#closingTimeMsg").text(store.closingTimeMsg);
		} else {
			listElement.find("#closingTimeMsg").text("");
		}

		//omniIspuStoreServed(store.StoreID);
		return listElement;
	}

	//IF MARKER IS LESS THAN 10, APPEND '0' TO MARKER NUMBER
	function getMarkerNumber(index) {
		var returnNumber = '0';
		if (i > 9)
			returnNumber = i;
		else
			returnNumber = '0' + index;
		return returnNumber;
	}

	// $("#store-locator-results-cloner").html($(storeResults).clone());
	// if($(".googleMapGroup").children("#store-locator-results").length == 0){
	// $(".googleMapGroup").append($("#store-locator-results-cloner").html());
	// }

	//map.controls[google.maps.ControlPosition.LEFT_TOP].push($(".googleMapGroup").children("#store-locator-results"));
	//map.controls[google.maps.ControlPosition.LEFT_TOP].push(findStore);

	//$(document).find("#filterAll a").trigger("click");

	setTimeout(function () {
		if ((zoom_percentage == 4)) {
			$(".googleMapGroup").find("#store-locator-results-tabs").hide();
			$(".googleMapGroup").find("#store-locator-results-scrollable").hide();
			$(".store-locator-find-store").css("padding-left", 20);
			$(".store-locator-find-store").show();
			$("#stores_count_display").hide();
			$(".nearby-stores-find-a-store.store-locator-zip").show();

		} else {
			$(".nearby-stores-find-a-store.store-locator-zip").css("display", "none");
			$(".googleMapGroup").find("#stores_count_display, #store-locator-results-tabs").css("display", "block");
			$(".googleMapGroup").find(".store-locator-zip.nearby-stores-find-a-store").css("display", "none");
			if (stores.length > 0) {
				$("#store-locator-results-scrollable").css("display", "block");
			}
		}
	}, 1000);


	//STORE RESULTS
	var storeResults = $('.store-locator-results-single');

	storeResults.each(function (index) {
		$(this).on('click', function () {
			closeInfoBoxes();
			map.panTo(markers[index].marker.getPosition());
			markers[index].ib.open(map, markers[index].marker);

		});
	});

	if (loaded == false) {
		var ShowMapStore = getCookie('ShowMapStore');
		setCookie('ShowMapStore', '');
		var _t = ShowMapStore.split("|")
		if (_t.length > 1) {
			var SearchZipCode = _t[0];
			var StoreID = _t[1];
			var _index = _t[2];

			map.panTo(markers[_index].marker.getPosition());
			markers[_index].ib.open(map, markers[_index].marker);

			/*
			$(".googleMapGroup").find("#store-locator-results-tabs").hide();
			$(".googleMapGroup").find("#store-locator-results-scrollable").hide();
			$(".nearby-stores-find-a-store.store-locator-zip").show();
			$(".store-locator-find-store").show();
			$("#stores_count_display").hide();
			$(".nearby-stores-find-a-store.store-locator-zip").show();
			*/
			$('.selectedaddress').text(SearchZipCode);
		}
		loaded == true;
	}
	//To move to the more stores 
	if (moreStoresFlag) {
		moreStoresFlag = false;
		/*var focusElementNumber = loadcurrentData-10;
		var focusElementTop = $(".googleMapGroup #store-locator-results-scrollable li:nth-child("+focusElementNumber+")").position().top;*/
		$(".googleMapGroup #store-locator-results-scrollable .tse-scroll-content").scrollTop(scrollFocusShowMore);
	}
} //// END Of initialize

function DefaultPopup() {

	var curreentPage = location.pathname.split('/').slice(-1)[0].toLowerCase();

	if (curreentPage === 'showgooglemap.jsp' && loaded == false) {
		loaded = true;
		ShowMapStore = getCookie('ShowMapStore');
		var _t = ShowMapStore.split("|")
		if (_t.length > 1) {
			var SearchZipCode = _t[0];
			var StoreID = _t[1];
			var _index = _t[2];
			var findInStoreSearch = document.getElementById("findInStoreSearch")
			var globalRadius = '100';
			if ($("#globalRadius").length) {
				if ($("#globalRadius").val() != "") {
					globalRadius = $("#globalRadius").val();
				}
			}
			getStoreResults(SearchZipCode, globalRadius, doFindInStoreSearch, null, StoreID)
			//setCookie('ShowMapStore', '');
			if (markers.length) {
				for (i = 0; i < markers.length; i++) {
					markers[i].ib.close();
				}
			}
			var map;
			var mapOptions = {
				center: new google.maps.LatLng(latitude, longitude),
				zoom: zoom_percentage,
				zoomControl: true,
				zoomControlOptions: {
					position: google.maps.ControlPosition.RIGHT_BOTTOM,
					style: google.maps.ZoomControlStyle.SMALL
				},
				panControl: false,
				streetViewControl: false,
				mapTypeControl: false,
				scrollwheel: false
			};

			map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);
			map.panTo(markers[_index].marker.getPosition());
			markers[_index].ib.open(map, markers[_index].marker);

			/*
			setTimeout(function(){
			$(".googleMapGroup").find("#store-locator-results-tabs").hide();
			$(".googleMapGroup").find("#store-locator-results-scrollable").hide();
			$(".nearby-stores-find-a-store.store-locator-zip").show();
			$(".store-locator-find-store").show();
			$("#stores_count_display").hide();
			$(".nearby-stores-find-a-store.store-locator-zip").show();
			},500);
			*/
		}
		//getStoreResults( ,'100',doFindInStoreSearch,this)
	}
}
//for find-a-store
function findAStore() {
	$('.storeSearch').click();
}




function findAStore1() {
	$('#storeSearch1').click();
}



//process the find-site-store form -- METHOD DEPRECATED AS IMPLEMENTATION OF THIS CHANGED 
/*function findAllStore(contextPath) {
	$.ajax({
        type        : "POST",
        cache       : false,
        url         : contextPath+'/storelocator/storeLocatorResults.jsp',
        data        : 'notifyme=true',
        datatype    : "html",
        success: function(data) {
        	if($('.store-locator-zip').hasClass('nearby-stores-find-a-store')) {
	        	$('.store-locator-pick-store').html(data);
	        	$('.store-locator-pick-store').show();
	        	$('.nearby-stores-find-a-store').slideToggle();
	        	$('.site-All').addClass('active');
			} else {
				$('.store-locator-pick-store').html(data);
	        	$('.store-locator-pick-store').show();
	        	$('.nearby-stores-find-a-store').slideToggle();
			}
        }
	});
	return false;
}
*/

//process the find-site-store form
function findSiteStore(siteId) {
	$(".store-locator-pick-store").hide();
	$("#siteIds").val(siteId);
	$('#storeSiteSearch').click();
}



//process the show more stores details
function showMoreStores(pageNumber) {
	$("#currentResultPageNum").val(pageNumber);
	$('#currentResultPageNum').click();
}

function constructStoreHoursHtml(currentStore) {
	var storeHoursText = {}, weekdayText = [], weekTimingsText = [];
	if (currentStore.storeHours.hasOwnProperty('MON-FRI')) {
		weekdayText.push("MON-FRI");
		weekTimingsText.push(currentStore.storeHours["MON-FRI"].toLowerCase());
	}
	if (currentStore.storeHours.hasOwnProperty('MON')) {
		weekdayText.push("mon");
		weekTimingsText.push(currentStore.storeHours.MON.toLowerCase());
	}
	if (currentStore.storeHours.hasOwnProperty('TUE')) {
		if (weekTimingsText[weekTimingsText.length - 1] == currentStore.storeHours.TUE.toLowerCase()) {
			var splitValue = weekdayText[weekdayText.length - 1].split("-");
			splitValue[1] = "tue";
			weekdayText[weekdayText.length - 1] = splitValue[0] + "-" + splitValue[1];
		} else {
			weekTimingsText[weekdayText.length] = currentStore.storeHours.TUE.toLowerCase();
			weekdayText[weekdayText.length] = "tue";
		}
	}
	if (currentStore.storeHours.hasOwnProperty('WED')) {
		if (weekTimingsText[weekTimingsText.length - 1] == currentStore.storeHours.WED.toLowerCase()) {
			var splitValue = weekdayText[weekdayText.length - 1].split("-");
			splitValue[1] = "wed:";
			weekdayText[weekdayText.length - 1] = splitValue[0] + "-" + splitValue[1];
		} else {
			weekTimingsText[weekdayText.length] = currentStore.storeHours.WED.toLowerCase();
			weekdayText[weekdayText.length] = "wed";
		}
	}
	if (currentStore.storeHours.hasOwnProperty('THU')) {
		if (weekTimingsText[weekTimingsText.length - 1] == currentStore.storeHours.THU.toLowerCase()) {
			var splitValue = weekdayText[weekdayText.length - 1].split("-");
			splitValue[1] = "thu";
			weekdayText[weekdayText.length - 1] = splitValue[0] + "-" + splitValue[1];
		} else {
			weekTimingsText[weekdayText.length] = currentStore.storeHours.THU.toLowerCase();
			weekdayText[weekdayText.length] = "thu";
		}
	}
	if (currentStore.storeHours.hasOwnProperty('FRI')) {
		if (weekTimingsText[weekTimingsText.length - 1] == currentStore.storeHours.FRI.toLowerCase()) {
			var splitValue = weekdayText[weekdayText.length - 1].split("-");
			splitValue[1] = "fri";
			weekdayText[weekdayText.length - 1] = splitValue[0] + "-" + splitValue[1];
		} else {
			weekTimingsText[weekdayText.length] = currentStore.storeHours.FRI.toLowerCase();
			weekdayText[weekdayText.length] = "fri";
		}
	}
	if (currentStore.storeHours.hasOwnProperty('SAT')) {
		if (weekTimingsText[weekTimingsText.length - 1] == currentStore.storeHours.SAT.toLowerCase()) {
			var splitValue = weekdayText[weekdayText.length - 1].split("-");
			splitValue[1] = "sat";
			weekdayText[weekdayText.length - 1] = splitValue[0] + "-" + splitValue[1];
		} else {
			weekTimingsText[weekdayText.length] = currentStore.storeHours.SAT.toLowerCase();
			weekdayText[weekdayText.length] = "sat";
		}
	}
	if (currentStore.storeHours.hasOwnProperty('SUN')) {
		if (weekTimingsText[weekTimingsText.length - 1] == currentStore.storeHours.SUN.toLowerCase()) {
			var splitValue = weekdayText[weekdayText.length - 1].split("-");
			splitValue[1] = "sun";
			weekdayText[weekdayText.length - 1] = splitValue[0] + "-" + splitValue[1];
		} else {
			weekTimingsText[weekdayText.length] = currentStore.storeHours.SUN.toLowerCase();
			weekdayText[weekdayText.length] = "sun";
		}
	}
	storeHoursText.weekday = weekdayText;
	storeHoursText.weekTimings = weekTimingsText;
	return storeHoursText;

}
function convertSplToEntity(txt) {
	var entityTxt = (typeof txt != 'undefined' && txt != '') ? (txt.replace(/"/g, "&quot;")) : txt;
	return entityTxt;
}

//process the view-store-details form
function viewStoreDetailsPDP(ele, i, contextpath) {

	var $this = $(ele);
	var currentStore = stores[i];
	var ourStoreName = convertSplToEntity(currentStore.name);
	var getdirections = "javascript:getDrivingDirections('" + currentStore.StreetAddress + "', '" + currentStore.city + "','" + currentStore.postalCode + "','" + currentStore.latitude + "','" + currentStore.longitude + "','" + ourStoreName + "')";


	$("#store-locator-tooltip-store-detailsPDP").find(".store-address").html("");
	$("#store-locator-tooltip-store-detailsPDP").find(".store-address").append("<p>" + currentStore.StreetAddress + "</p>");
	$("#store-locator-tooltip-store-detailsPDP").find(".store-address").append("<p>" + currentStore.city + ", " + currentStore.stateAddress + " " + currentStore.postalCode + "</p>");
	$("#store-locator-tooltip-store-detailsPDP").find(".store-address").append('<a href="javascript:void(0)" onclick="' + getdirections + '" class="get-directions">get directions</a>');
	$("#store-locator-tooltip-store-detailsPDP").find(".weekday").html("");
	$("#store-locator-tooltip-store-detailsPDP").find(".week-timings").html("");
	if (currentStore.hasOwnProperty('storeHours')) {
		if (JSON.stringify(currentStore.storeHours).length > 2) {
			var storeHoursText = constructStoreHoursHtml(currentStore);
			/*for(item in storeHoursText.weekday){
				$("#store-locator-tooltip-store-detailsPDP").find(".weekday").append("<p>"+storeHoursText.weekday[item]+":</p>");
				$("#store-locator-tooltip-store-detailsPDP").find(".week-timings").append("<p>"+storeHoursText.weekTimings[item].split(' ').join("")+"</p>");;
			}*/
			for (var i = 0; i < storeHoursText.weekday.length; i++) {
				$("#store-locator-tooltip-store-detailsPDP").find(".weekday").append("<p>" + storeHoursText.weekday[i] + ":</p>");
				$("#store-locator-tooltip-store-detailsPDP").find(".week-timings").append("<p>" + storeHoursText.weekTimings[i].split(' ').join("") + "</p>");
			}
		}
	}
	$("#store-locator-tooltip-store-detailsPDP").find('ul').html("");

	if (currentStore.storeServices.length > 0) {
		$(".store_services_link_detailsPDP").show();
		//htmlText = '<header>store services</header>';
	}
	else {
		$(".store_services_link_detailsPDP").hide();
	}
	$.each(currentStore.storeServices, function (i, v) {
		var learnMoreLink = "";
		if (typeof v.learnMore !== "undefined" && v.learnMore !== null && v.learnMore !== "") {
			learnMoreLink = '<a data-content="' + v.learnMore + '" data-placement="bottom" class="learnmorePDPdetails"> Learn More </a>';
		}
		$("#store-locator-tooltip-store-detailsPDP").find('ul').append('<input type=hidden value=' + v.learnMore + 'id=storelocater-lernmorelinkDetails/><li>' + v.serviceName + '<p>' + v.serviceDescription + '</p>' + learnMoreLink + '</li>');
	});

	$("#store-locator-tooltip-store-detailsPDP").show();

	var sl_learnmore = $(document).find('#findInStoreModal').find(".learnmorePDPdetails")
	//$(document).find('.store-locator-content.flagstore-locator-content li:last-of-type').find(".sl-learnmore").attr("data-placement","top");
	sl_learnmore.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		trigger: 'manual'
	}).on('click', function (e) {
		if (!$(this).hasClass("clicked")) {
			hideAllLearnMorePopoversPDP();
			var $this = $(this);
			setTimeout(function () {
				$this.popover('show');
			}, 200);
		} else {
			$(this).popover('hide');
		}
		e.stopPropagation();
	});


	sl_learnmore.on('show.bs.popover', function () {
		$(this).addClass('clicked');
	});

	sl_learnmore.on('hide.bs.popover', function () {
		$(this).removeClass('clicked');
	});


	/*Old Implementation*/

	/*var googleminmap="https://maps.googleapis.com/maps/api/staticmap?center="+currentStore.address1+","+currentStore.city +","+currentStore.stateAddress+"&zoom=14&size=250x100"
	var htmlText = '';
	var storeInfo = $(".storeLocatorField").val() + "|" + currentStore.StoreID + "|" + i
	var getdirections = 'javascript:getDrivingDirections("'+currentStore.StreetAddress+'", "'+ currentStore.city +'","'+currentStore.postalCode+'")';
	var storeName = convertToHtmlEntities(currentStore.name);
	var makemystore = 'javascript:makeMyStore(event,"'+contextpath+'","'+currentStore.StoreID+'", "'+ currentStore.postalCode +'","'+currentStore.city+'",this,"'+storeName+'")';
	
	
	htmlText+= '<div class="global-nav-store-details">';
	htmlText+= '<div class="store-locator-content" style="display: block;">';
	htmlText+= '<div class="back-to-store">';
	htmlText+= '<img class="inline" src="/images/small-back-arrow.png">';
	htmlText+= '<a href="javascript:void(0)">back to store results</a>';
	htmlText+= '</div>';
	htmlText+= '<a class="showGoogleMapButton" style="cursor: default;">';
	htmlText+= '<img id="googlemini-map" src="' + googleminmap +'"/>';
	
	if (currentStore.chainCode === "TRU")
      htmlText+= '<img class="you-are-here" src="../images/my_TRU.png" alt="image not found"/>';
    else
      htmlText+= '<img class="you-are-here" src="../images/my_BRU.png" alt="image not found"/>';

	htmlText+= '<div class="store-locator-store-details">';
	htmlText+= '<h3 class="store_name">'+ currentStore.name + '</h3>';
	if(currentStore.closingTimeMsg != "undefined" && currentStore.closingTimeMsg != null){
		htmlText+="<p>" + currentStore.distance + 'miles away<span>.</span>' + currentStore.closingTimeMsg +'</p><a href=javascript:void(0); class=make-my-store-link>make this my store</a><div class=my-store>my store <input type="hidden" id="hdnStoreID" value="' + storeInfo +'"/>'
	}else{
		htmlText+="<p>" + currentStore.distance + 'miles away</p><a href=javascript:void(0); class=make-my-store-link>make this my store</a><div class=my-store>my store <input type="hidden" id="hdnStoreID" value="' + storeInfo +'"/>'
	}
	htmlText+= '<div id="store-locator-tooltip-store-details1" class="tse-scrollable"><div class="tse-scrollbar" style="display: none;">';
	htmlText+= '<div class="drag-handle"></div></div>';
	htmlText+= '<div class="tse-scrollbar" style="display: none;">';
	htmlText+= '<div class="drag-handle"></div></div>';
	htmlText+= '<div class="tse-scroll-content"><div class="tse-scroll-content"><div class="tse-content">';
	htmlText+= '<div class="store-address">';
	htmlText+= '<p class="address_line_1">' + currentStore.StreetAddress +'</p>';
	htmlText+= '<p class="address_line_2">' + currentStore.city + ', ' +currentStore.stateAddress + ' ' + currentStore.postalCode + '</p>';
	htmlText+= '<p class="phone-number">' + currentStore.phoneNumber + '</p>';
	htmlText+= '<a href="javascript:void(0)" onclick="' + getdirections +'" class="get-directions">get directions</a>';
	htmlText+= '</div>';
	htmlText+= '<div class="store-hours">';
	htmlText+= '<div class="weekday">';
	$.each(currentStore.storeHours,function(i,v){
		htmlText+= '<p>'+i+'</p>';
	});
	htmlText+= '</div>';
	htmlText+= '<div class="week-timings">';
	$.each(currentStore.storeHours,function(i,v){
		htmlText+= '<p>'+v+'</p>';
	});
	htmlText+= '</div>';
	htmlText+= '</div>';
	htmlText+= '<br>';
	if (currentStore.storeServices.length >0){
		htmlText+= '<h5>store services</h5>';
		htmlText+= '<ul>';

		$.each(currentStore.storeServices,function(i,v){
			htmlText+= '<input type=hidden value=' + v.learnMore +' id="storelocater-lernmorelinkDetails" /><li>'+v.serviceName+'<p>'+v.serviceDescription+'</p><a href="javascript:void(0)" data-content="'+v.learnMore+'" data-toggle="popover" class="learn-more-tooltip">Learn More</a></li>';
		});
		htmlText+= '</ul>';
	}
	htmlText+= '</div></div></div></div></div>';
	
	$('.find-in-store-overlay #store-locator-tooltip-store-details_1').html(htmlText);
	
	
	
	var p = $(".find-in-store-overlay #find-in-store-results");
	var offset = p.offset();
								
	var top = (p.height() + offset.top) *-1
	$('.find-in-store-overlay #store-locator-tooltip-store-details_1').height(p.height())
	$('.find-in-store-overlay #store-locator-tooltip-store-details_1').width(p.width())
	$('.find-in-store-overlay #store-locator-tooltip-store-details_1').offset({top: top,left: p.width()})
	$('.find-in-store-overlay #store-locator-tooltip-store-details_1').find('.make-my-store-link').attr('onclick',makemystore)
	
	
	$(".find-in-store-overlay #find-in-store-results").slideToggle()
	$('.find-in-store-overlay #store-locator-tooltip-store-details_1').slideToggle();*/

	/*END OF Old Implementation*/

	///$('.find-in-store-overlay #store-locator-tooltip-store-details_1').addClass('sl_root1');

	/*
	$(".showGoogleMapButton").removeAttr("data-href");
	$(".showGoogleMapButton").css('cursor', 'default')
	
	$this.parents(".sl_root").find('.store-locator-content').slideToggle();
    $this.parents(".sl_root").find('.sl_parent').slideToggle();
    */
	//setToolTips();
	//return false;

	/*
	
	$this.parents(".sl_root").find(".store-locator-content").find('header').html();
	
	if(currentStore.isEligibleForfavStore){
		$this.parents(".sl_root").find(".store-locator-content").find('.make-store-link').hide();
	}
	
	if (currentStore.storeServices.length>0){
		$this.parents(".sl_root").find(".store-locator-content").find('.store-locator-tooltip-store-details').find('header').html("store services");
	}else{
		$this.parents(".sl_root").find(".store-locator-content").find('.store-locator-tooltip-store-details').find('header').html("");
		}
	var makemystore = 'javascript:makeMyStore(event,"'+contextpath+'","'+currentStore.StoreID+'", "'+ currentStore.postalCode +'","'+currentStore.city+'",this)';
	$this.parents(".sl_root").find(".store-locator-content").find('header').find('.make-my-store-link').attr('onclick',makemystore);


	$this.parents(".sl_root").find(".store-locator-content").find('.store-hours').find('.weekday').html("");
	$this.parents(".sl_root").find(".store-locator-content").find('.store-hours').find('.week-timings').html("");
	$this.parents(".sl_root").find(".store-locator-content").find('ul').html("");
	$.each(currentStore.storeHours,function(i,v){
		$this.parents(".sl_root").find(".store-locator-content").find('.store-hours').find('.weekday').append('<p>'+i+'</p>');
		$this.parents(".sl_root").find(".store-locator-content").find('.store-hours').find('.week-timings').append('<p>'+v+'</p>');
	});
	
	$this.parents(".sl_root").find(".store-locator-content").find('.store-address').find('.address_line_1').html(currentStore.StreetAddress);
	$this.parents(".sl_root").find(".store-locator-content").find('.store-address').find('.address_line_2').html(currentStore.city + ', ' +currentStore.stateAddress + ' ' + currentStore.postalCode);
	$this.parents(".sl_root").find(".store-locator-content").find('.store-address').find('.phone-number').html(currentStore.phoneNumber);
	var getdirections = 'javascript:getDrivingDirections("'+currentStore.StreetAddress+'", "'+ currentStore.city +'","'+currentStore.postalCode+'")';	
	$this.parents(".sl_root").find(".store-locator-content").find('.store-address').find('.get-directions').attr('onclick',getdirections);
	
	
	$.each(currentStore.storeServices,function(i,v){
		$this.parents(".sl_root").find(".store-locator-content").find('ul').append('<input type=hidden value=' + v.learnMore +'id=storelocater-lernmorelinkDetails/><li>'+v.serviceName+'<p>'+v.serviceDescription+'</p><a class=sl-learnmore>'+v.learnMore+'</a></li>');
	});

	var googleminmap="https://maps.googleapis.com/maps/api/staticmap?center="+currentStore.city +","+currentStore.stateAddress+"&zoom=14&size=250x100"
    $this.parents(".sl_root").find(".store-locator-content").find('#googlemini-map').attr("src",googleminmap);
	*/

}

//process the view-store-details form store-locator-store-details
function viewStoreDetails(ele, i, contextpath) {
	var $this = $(ele);
	var currentStore = stores[i];
	//var root = $(ele+'.store-locator-content');
	var storeInfo = $(".storeLocatorField").val() + "|" + currentStore.StoreID + "|" + i
	if (currentStore.closingTimeMsg != "undefined" && currentStore.closingTimeMsg != null) {
		$this.parents(".sl_root").find(".store-locator-content").find('h2').html(currentStore.name + "<p>" + currentStore.distance + milesAway + "<span>.</span>" + currentStore.closingTimeMsg + "</p><a href='javascript:void(0);' class='make-my-store-link'>" + makeThisMyStore + "</a><div class=my-store>" + myStore + "<input type='hidden' id='hdnStoreID' value='" + storeInfo + "'/> </div>");
	} else {
		$this.parents(".sl_root").find(".store-locator-content").find('h2').html(currentStore.name + "<p>" + currentStore.distance + milesAway + "</p><a href='javascript:void(0);' class='make-my-store-link'>" + makeThisMyStore + "</a><div class=my-store>" + myStore + "<input type='hidden' id='hdnStoreID' value='" + storeInfo + "'/> </div>");
	}
	var storeName = convertToHtmlEntities(currentStore.name);
	var makemystore = 'javascript:makeMyStore(event,"' + contextpath + '","' + currentStore.StoreID + '", "' + currentStore.postalCode + '","' + currentStore.city + '",this,"' + storeName + '","' + currentStore.distance + '","' + currentStore.signature + '")';
	var favStoreId = getCookie("favStore");
	favStoreId = favStoreId.split("|");
	favStoreId = favStoreId[1];

	if (!currentStore.isEligibleForfavStore || currentStore.StoreID == favStoreId) {
		$this.parents(".sl_root").find(".store-locator-content").find('.make-my-store-link').hide();
	} else {
		$this.parents(".sl_root").find(".store-locator-content").find('.make-my-store-link').attr("onclick", makemystore);
	}

	if (currentStore.StoreID == favStoreId) {
		$this.parents(".sl_root").find(".store-locator-content").find('.my-store').show();
	}

	if (currentStore.storeServices.length > 0) {
		$this.parents(".sl_root").find(".store-locator-content").find('.store-locator-tooltip-store-details').show();
		$this.parents(".sl_root").find(".store-locator-content").find('.store-locator-tooltip-store-details').find('h5').html("store services");
	} else {
		$this.parents(".sl_root").find(".store-locator-content").find('.store-locator-tooltip-store-details').find('h5').html("");
	}
	var storeName = convertToHtmlEntities(currentStore.name);
	var makemystore = 'javascript:makeMyStore(event,"' + contextpath + '","' + currentStore.StoreID + '", "' + currentStore.postalCode + '","' + currentStore.city + '",this,"' + storeName + '","' + currentStore.distance + '","' + currentStore.signature + '")';
	$this.parents(".sl_root").find(".store-locator-content").find('h2').find('.make-my-store-link').attr('onclick', makemystore);


	$this.parents(".sl_root").find(".store-locator-content").find('.store-hours').find('.weekday').html("");
	$this.parents(".sl_root").find(".store-locator-content").find('.store-hours').find('.week-timings').html("");
	$this.parents(".sl_root").find(".store-locator-content").find('ul').html("");
	/*$.each(currentStore.storeHours,function(i,v){
		$this.parents(".sl_root").find(".store-locator-content").find('.store-hours').find('.weekday').append('<p>'+i+'</p>');
		$this.parents(".sl_root").find(".store-locator-content").find('.store-hours').find('.week-timings').append('<p>'+v+'</p>');
	});*/

	var weekdayFlag = "";
	var allDayInfo = "";
	if (currentStore.hasOwnProperty('storeHours')) {
		var storeHoursText = constructStoreHoursHtml(currentStore);
		/*for(item in storeHoursText.weekday){*/
		for (var item = 0; item < storeHoursText.weekday.length; item++) {
			$this.parents(".sl_root").find(".store-locator-content").find('.store-hours').find('.weekday').append("<p>" + storeHoursText.weekday[item] + ":</p>");
			$this.parents(".sl_root").find(".store-locator-content").find('.store-hours').find('.week-timings').append("<p>" + storeHoursText.weekTimings[item] + "</p>");;
		}
	}

	if (JSON.stringify(currentStore.storeHours).length > 2) {
		$this.parents(".sl_root").find(".store-locator-content").find('.store-hours').show();
	} else {
		$this.parents(".sl_root").find(".store-locator-content").find('.store-hours').hide();
	}





	$this.parents(".sl_root").find(".store-locator-content").find('.store-address').find('.address_line_1').html(currentStore.StreetAddress);
	$this.parents(".sl_root").find(".store-locator-content").find('.store-address').find('.address_line_2').html(currentStore.city + ', ' + currentStore.stateAddress + ' ' + currentStore.postalCode);
	$this.parents(".sl_root").find(".store-locator-content").find('.store-address').find('.phone-number').html('');
	if (currentStore.hasOwnProperty("phoneNumber")) {
		$this.parents(".sl_root").find(".store-locator-content").find('.store-address').find('.phone-number').html(currentStore.phoneNumber);
	}
	var getdirections = "javascript:getDrivingDirections('" + currentStore.StreetAddress + "','" + currentStore.city + "','" + currentStore.postalCode + "','" + currentStore.latitude + "','" + currentStore.longitude + "','" + currentStore.name + "')";
	$this.parents(".sl_root").find(".store-locator-content").find('.store-address').find('.get-directions').attr('onclick', getdirections);


	$.each(currentStore.storeServices, function (i, v) {
		/*For Enabling tooltip*/
		var learnMoreLink = "";
		if (typeof v.learnMore !== "undefined" && v.learnMore !== null && v.learnMore !== "") {
			//learnMoreLink = '<a data-content="'+v.learnMore+'" data-placement="bottom" data-toggle="popover" class="learn-more-tooltipHeader">Learn More</a>';
			learnMoreLink = '<a href="'+window.location.protocol+'//www.toysrus.com/helpdesk/index.jsp?display=payment&index=15&subdisplay=options#layaway" target="_blank">Learn More</a>';

		}
		$this.parents(".sl_root").find(".store-locator-content").find('ul').append('<input type=hidden value=' + v.learnMore + 'id=storelocater-lernmorelinkDetails/><li>' + v.serviceName + '<p>' + v.serviceDescription + '</p>' + learnMoreLink + '</li>');

		/*For disabling tooltip*/
		/*$this.parents(".sl_root").find(".store-locator-content").find('ul').append('<input type=hidden value=' + v.learnMore +'id=storelocater-lernmorelinkDetails/><li>'+v.serviceName+'<p>'+v.serviceDescription+'</p><a data-content="'+v.learnMore+'" class="">Learn More</a></li>');*/
	});

	var googleStaticUrl = $('#googleStaticUrl').val();
	var center = "&center=" + currentStore.address1 + "," + currentStore.city + "," + currentStore.stateAddress;
	var googleminmap = googleStaticUrl + center.replace(/[ ]/g, '+') + "&signature=" + currentStore.signature;

	$this.parents(".sl_root").find(".store-locator-content").find('#googlemini-map').attr("src", googleminmap);

	if (currentStore.chainCode === "TRU")
		$this.parents(".sl_root").find(".store-locator-content").find('.you-are-here').attr("src", "../images/my_TRU.png");
	else
		$this.parents(".sl_root").find(".store-locator-content").find('.you-are-here').attr("src", "../images/my_BRU.png");


	$(".showGoogleMapButton").removeAttr("data-href");
	$(".showGoogleMapButton").css('cursor', 'default')


	$this.parents(".sl_root").find('.store-locator-content').slideToggle();
	$this.parents(".sl_root").find('.sl_parent').slideToggle();

	var sl_learnmore = $(document).find('.sl_root').find(".learn-more-tooltipHeader");
	//$(document).find('.store-locator-content.flagstore-locator-content li:last-of-type').find(".sl-learnmore").attr("data-placement","top");
	sl_learnmore.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		trigger: 'manual'
	}).on('click', function (e) {
		if (!$(this).hasClass("clicked")) {
			hideAllLearnMorePopoversHeader();
			var $this = $(this);
			setTimeout(function () {
				$this.popover('show');
			}, 200);
		} else {
			$(this).popover('hide');
		}
		e.stopPropagation();
	});


	sl_learnmore.on('show.bs.popover', function () {
		$(this).addClass('clicked');
	});

	sl_learnmore.on('hide.bs.popover', function () {
		$(this).removeClass('clicked');
	});


	//setToolTip();

	return false;
}

//process the show-my-store link
function showMyStore(contextPath, storeId, storeDistance, openingHours) {
	$.ajax({
		type: "POST",
		cache: false,
		url: contextPath + 'storelocator/storeInfo.jsp',
		data: 'storeId=' + storeId + '&storeDistance=' + storeDistance + '&openingHours=' + openingHours,
		datatype: "html",
		success: function (data) {
			$('.store-locator-content').html(data);
			var currentStoreVal = parseJson($('.store-locator-content .storeHoursJson').text());
			var storeHoursMap = constructStoreHoursHtml(currentStoreVal);
			for (item in storeHoursMap.weekday) {
				$('.store-locator-content').find(".weekday").append("<p>" + storeHoursMap.weekday[i] + ":</p>");
				$('.store-locator-content').find(".week-timings").append("<p>" + storeHoursMap.weekTimings[i].split(' ').join("") + "</p>");
			};
			$('.store-locator-content .storeHoursJson').remove();
			$('.store-locator-content').show();
		}
	});
	return false;
}
function hideAllLearnMorePopovers() {
	$(document).find(".sl-learnmore_mystore").each(function () {

		$(this).popover('hide');
	});
};
function hideAllLearnMorePopoversPDP() {
	$(document).find("#findInStoreModal .learnmorePDPdetails").each(function () {
		$(this).popover('hide');
	});
};
function hideAllLearnMorePopoversHeader() {
	$(document).find('.sl_root .learn-more-tooltipHeader').each(function () {
		$(this).popover('hide');
	});
};

function showMyFavStore(contextPath, storeId, storeDistance, openingHours) {
	var signature = getCookie('myStoreAddressSignature');
	$.ajax({
		type: "POST",
		cache: false,
		url: contextPath + 'storelocator/storeInfo.jsp?cookieStoreId=' + storeId,
		data: 'storeId=' + storeId + '&storeDistance=' + storeDistance + '&openingHours=' + openingHours + '&signature=' + signature,
		datatype: "html",
		success: function (data) {
			setTimeout(function () {
				hideAllLearnMorePopovers();
				$(document).find('.store-locator-content.flagstore-locator-content').each(function () {
					$(this).html(data);
					var currentStoreVal = JSON.parse($(this).find('.storeHoursJson').text());
					var storeHoursMap = constructStoreHoursHtml(currentStoreVal);
					for (var item = 0; item < storeHoursMap.weekday.length; item++) {
						$(this).find(".weekday").append("<p>" + storeHoursMap.weekday[item].toUpperCase() + ":</p>");
						$(this).find(".week-timings").append("<p>" + storeHoursMap.weekTimings[item].split(' ').join("") + "</p>");
					}
					$(this).find('.storeHoursJson').remove();
				});
				var sl_learnmore = $(document).find('.store-locator-content.flagstore-locator-content').find(".sl-learnmore_mystore")
				//$(document).find('.store-locator-content.flagstore-locator-content li:last-of-type').find(".sl-learnmore").attr("data-placement","top");
				sl_learnmore.popover({
					animation: 'true',
					html: 'true',
					content: 'asdfasdf',
					trigger: 'manual'
				}).on('click', function (e) {
					if (!$(this).hasClass("clicked")) {
						hideAllLearnMorePopovers();
						var $this = $(this);
						setTimeout(function () {
							$this.popover('show');
						}, 200);
					} else {
						$(this).popover('hide');
					}
					e.stopPropagation();
				});


				sl_learnmore.on('show.bs.popover', function () {
					$(this).addClass('clicked');
				});

				sl_learnmore.on('hide.bs.popover', function () {
					$(this).removeClass('clicked');
				});

				$(document).find('.store-locator-content.flagstore-locator-content').show();
				/*$(".sl-learnmore").tooltip();*/
			}, 200);

		}
	});
	return false;
}
//Check the prepopulation Condition
function checkPrepopulationAvailability(postalAddress, ele, rc) {

}

//Prepopulation of stores
function prepopulateStores(postalAddress, resultContainer) {
	var rc = $(rc);
	var searchField = rc.find("input#FindinStorePDPField");
	if (searchField.length) {
		searchField.val(postalAddress);
		searchField.parent().find("button").trigger("click");
	}
}


//process the make my store form
function makeMyStore(event, contextPath, storeId, postalCode, city, ele, storeName, storeDistance, signature) {

	/*omniMakeMyStore(storeId,city,postalCode);*/
	//Added for omniture
	//omniStoreSelect();
	//Added for omniture
	event.stopPropagation();
	var address = "";
	var cvalue = '' + postalCode + "|" + storeId;
	setCookie('favStore', cvalue);
	var $this;
	var showMakeMyStoreClass = "make-store-link-" + storeId;
	var hideMakeMyStoreClass = "make-my-store-link-" + storeId;
	if( typeof utag != 'undefined' ){
		utag.link({
   			'event_type':'selected_store',
   			'selected_store':storeId
			});
	}
	/*if($(ele).length){*/
	$this = $(ele);
	/*if($this.parents(".find-in-store-overlay").length){
		omniStoreSelect(storeId);
	}else{
		omniMakeMyStore(storeId,city,postalCode);
	}*/
	omniStoreSelect(storeId);
	/*if(!($this.attr("id") == "makemystoreModel-yes")){
		event.stopPropagation();
	}*/
	if ($this.attr("id") == "makeThisMyStoreInfoBox" || $this.attr("id") == "toolipMakeMyStore") {
		address = $this.data("address");
	} else if ($this.hasClass("make-my-store-link")) {
		address = $this.parents(".sl_root").find(".store-locator-store-details").find(".store-address .address_line_1").text();
	} else if ($this.attr("id") == "makemystoreModel-yes") {
		address = $this.attr("data-address");
	} else {
		address = $this.parent().find(".address").text();
	}
	/*}else{
		address = addressLine;
	}*/



	setCookie('myStoreAddressCookie', address);
	setCookie('myStoreAddressSignature', signature);
	//var storename_html = convertToHtmlEntities(storeName);
	var storeNameText = $('<textarea />').html(storeName).text();
	//storeNameText = "'"+storeNameText+"'";
	storeNameText = storeNameText.split("[")[0];
	storeNameText = storeNameText + "|" + storeDistance;
	setCookie('myStoreName', storeNameText);
	$('#mystoreSuccessModal').modal('show');
	$('#mystoreSuccessModal').find('.mystore-city').html(city);
	/*$.ajax({
        type        : "POST",
        cache       : false,
        url         : contextPath+'/storelocator/makeMyStore.jsp',
        data        : 'storeId='+storeId+'&address='+address,
        datatype    : "html",
        success: function(data) {*/

	if ($this.attr("id") == "toolipMakeMyStore") {
		$('body').find('.sl_parent .sl_store_listing_ul .store-info').each(function () {
			$(this).find("#toolipMakeMyStore").show();
			$(this).find(".my-store").hide();
		});
	} else {
		/*$('.'+showMakeMyStoreClass).html(data);
		$('.'+hideMakeMyStoreClass).hide();*/
		$('body').find('.tse-content ul li .store-locator-results-single').find(".make-my-store").show();
		$('body').find('.tse-content ul li .store-locator-results-single').find(".my-store").hide();
	}

	$this.parent().find(".my-store").show();
	if ($this.attr("id") != "makemystoreModel-yes") {
		$this.hide();
	}


	$('.your-store-default').remove();
	var mystoreHtml;
	mystoreHtml = '<li class="your-store-bullet"><span>&#xb7;</span></li><li class="your-store your-store-locator-tooltip my-store-container" data-original-title="" title=""><span class="myDefaultStore">' + myStore + ':</span><span class="myDefaultStoreName">&nbsp' + storeName + '</span></li>';
	if ($('.store-locator-tooltip').parent().find(".your-store-locator-tooltip").length) {
		$('.store-locator-tooltip').parent().find(".your-store-locator-tooltip .myDefaultStoreName").html("&nbsp;" + storeName);
	} else {
		$('.store-locator-tooltip').after(mystoreHtml);
	}
	enablePopoverMyStore(storeDistance);
	//$('.store-locator-tooltip').parent().find(".your-store-locator-tooltip").attr("onmouseover","javascript:showMyFavStore('"+  contextPath +"','"+ storeId +"','','');");

	//MMS Map for tooltip and the page content 
	$('#mystoreSuccessModal').on("shown.bs.modal", function () {
		if ($this.attr("data-mms-map-pagecontent") || $this.attr("data-mms-map-infobox")) {
			/*$(".store-locator-store-details").each(function(){
				if($(this).find("#makeThisMyStoreInfoBox").attr("data-mms-map-infobox") == $this.attr("data-mms-map-pagecontent")){
					$(this).find("#makeThisMyStoreInfoBox").hide();
					$(this).find(".my-store").show();
				}
			});*/

			allStore = assigningDefaultStoreToFirstPosition(allStore);
			truStore = allStore.filter(function (item) {
				return (item.chainCode === 'TRU' || item.isSuperStoreServiceAval === true)
			});

			bruStore = allStore.filter(function (item) {
				return (item.chainCode === 'BRU' || item.isSuperStoreServiceAval === true)
			});

			if (activeTab != "") {
				if (activeTab == 'showAll')
					populateStoreLocatorLanding(allStore, null);
				else if (activeTab == 'showTRU')
					populateStoreLocatorLanding(truStore, null);
				else if (activeTab == 'showBRU')
					populateStoreLocatorLanding(bruStore, null);
			} else {
				populateStoreLocatorLanding(allStore, null);
			}
		}
		if ($this.attr("data-mms-map-infobox")) {
			$(document).find("#store-locator-results-scrollable").find("li:first-child .store-locator-results-single").click();
		}
	});


	//findInStoreSearch();
	///findAStore();
	//onclick="javascript:showMyStore(\'/tru','','','');"

	/*}
});*/
	//event.stopPropagation();
	$("#makemystoreModel").modal("hide");
    /*if($("body").find(".product-details-template").length){
    	var productId = $("#hiddenProductId").val();
    	var skuId = $("#pdpSkuId").val();
    	$.ajax({
            type        : "POST",
            cache       : false,
            url         : contextPath+'jstemplate/ajaxPDPStoreAvailMessage.jsp',
            data        : 'productId='+productId+'&skuId='+skuId+'&locationId='+storeId,
            success: function(data) {
            	$(".product-availability").find(".find-in-store").html(data);
            }
    	});
    }*/
	//productAvailablitiyMessageSL();
	if ($(".product-details-template").length || $(".collection-template").length) {
		ajaxCallForInventoryAndPrice("storeLocator");
		notCachedAddTocartButton("");
	}
	$.ajax({
		type: "POST",
		cache: false,
		url: contextPath + 'storelocator/makeMyStore.jsp',
		data: 'storeId=' + storeId + '&address=' + address + '&postalCode=' + postalCode + '&storeName=' + storeNameText,
		datatype: "html",
		success: function (data) {
			syncCookieByIframe();
		}
	});
    /*else if($("body").find(".collections-template").length){
    	$.ajax({
            type        : "POST",
            cache       : false,
            url         : contextPath+'jstemplate/ajaxCollectionStoreAvailMessage.jsp',
            data        : ,
            success: function(data) {
            	//$(".product-availability").html(data);
            }
    	});
    }*/
	alignStoreLocatorPageWithHeader();

	return false;
}




/*Cross Domain cookie functionality*/
$(window).load(function () {
	// Remove I-Frame call for single domain
	var previewPromoEnableInStaging = $("#previewPromoEnableInStaging").val();
	if (previewPromoEnableInStaging != 'true') {
		//	crossDomainCookieIframeLoader();
	}
});

//Cross domain iFrame loader
function crossDomainCookieIframeLoader() {
	/*	if(!$("#crossDomainCookiePersisterIframe").length){
			var iFrameURL = window.location.host;
			if(iFrameURL.indexOf("toysrus") > -1){
				iFrameURL = iFrameURL.replace("toysrus","babiesrus");
			}else{
				iFrameURL = iFrameURL.replace("babiesrus","toysrus");
			}
			iFrameURL = "http://" + iFrameURL +"/common/crossDomainCookiePersister.jsp";
			$('<iframe src="'+iFrameURL+'" frameborder="0" scrolling="no" id="crossDomainCookiePersisterIframe"></iframe>').appendTo('#crossDomainCookieIframeLoader'); 
		}*/
	var iFrameURL = getCookie("alternateDomain");
	if (iFrameURL != "" && !$("#crossDomainCookiePersisterIframe").length) {
		iFrameURL = location.protocol + '//' + iFrameURL + "/common/crossDomainCookiePersister.jsp";
		$('<iframe src="' + iFrameURL + '" frameborder="0" scrolling="no" onload="javascript:setDesktopCrossDomainCookie();" id="crossDomainCookiePersisterIframe"></iframe>').appendTo('#crossDomainCookieIframeLoader');
		/* Start: For baby registry */
		var iFrameURLBruFull = getBRUURL();
		$('<iframe src="' + iFrameURLBruFull + '" frameborder="0" scrolling="no" id="crossDomainCookiePersisterIframeBRU"></iframe>').appendTo('#crossDomainCookieIframeLoader');

		if (iFrameURLBruFull.indexOf('https') > -1) {
			iFrameURLBruFull = iFrameURLBruFull.replace('https', 'http');
		} else {
			iFrameURLBruFull = iFrameURLBruFull.replace('http', 'https');
		}
		$('<iframe src="' + iFrameURLBruFull + '" frameborder="0" scrolling="no" id="crossDomainCookiePersisterIframeBRU"></iframe>').appendTo('#crossDomainCookieIframeLoader');
		/* End: For baby registry */
	}
	setTimeout(function () {
		/*setDesktopCrossDomainCookie();*/
		checkSOSCookies();
	}, 3000);
}
function getBRUURL() {
	var babyRegistryLink = $('.baby-registry-addTo').attr('data-href') || '';
	babyRegistryLink = (babyRegistryLink.split('//').length > 1) ? (babyRegistryLink.split('//')[1]) : '';
	babyRegistryLink = (babyRegistryLink.split('/').length) ? (babyRegistryLink.split('/')[0]) : '';
	var firstDotIndex = babyRegistryLink.indexOf('.');
	var prefixDomain = getCookie("alternateDomain") || '';
	var iFrameURLBru = prefixDomain.split('.')[0] + babyRegistryLink.substr(firstDotIndex);
	var iFrameURLBruFull = location.protocol + '//' + iFrameURLBru + "/common/crossDomainCookiePersister.jsp";
	return iFrameURLBruFull;
}
//Setting cookie for the Cross domain 
function setCrossDomainCookie(cname, cvalue) {
	var iframeWindow = document.getElementById('crossDomainCookiePersisterIframe');
	var alternateDomain = extractDomainName(iframeWindow.getAttribute("src"));
	if (alternateDomain != "") {
		iframeWindow.contentWindow.postMessage({ key: cname, data: cvalue, domainName: alternateDomain }, iframeWindow.src);
	}
}
function fetchDesktopCookieValue() {
	/*var cname = "desktopView";
	desktopCookieValue = getCookie(cname);
	delete_cookie(cname);*/
}
function setDesktopCrossDomainCookie() {
	/*var cname = "desktopView";
	var desktopCookie = getCookie(cname);
	if(desktopCookie != ""){
		var cvalue = desktopCookie.split("|")[0];
		var iframeWindow = document.getElementById('crossDomainCookiePersisterIframe');
		//delete_cookie(cname);
		//Setting cookie for the current domain
		var currentDomain = extractDomainName(window.location.host);
		document.cookie = cname + "=" + cvalue +"; path=/;domain="+currentDomain;
		//setting cookie for alternate domain
		if(iframeWindow != null){
			//var currentDomain = extractDomainName(currentDomain);
			var alternateDomain = extractDomainName($(iframeWindow).attr("src"));
			if(alternateDomain != ""){
				iframeWindow.contentWindow.postMessage({ key:cname , data:cvalue, domainName:alternateDomain} , iframeWindow.src);
				//For removing the server side cookie
				iframeWindow.contentWindow.postMessage({ key:cname , data:cvalue, domainName:alternateDomain, removeCookie:true} , iframeWindow.src);
				delete_cookie(cname);
			}
		}
	}*/
}
function checkSOSCookies() {
	setSOSCrossDomainCookie("isSOS");
	setSOSCrossDomainCookie("sosEmpName");
	setSOSCrossDomainCookie("sosEmpNumber");
	setSOSCrossDomainCookie("sosStoreNumber");
}

function setSOSCrossDomainCookie(cname) {
	var cvalue = getCookie(cname);
	if (cvalue != "") {
		var iframeWindow = document.getElementById('crossDomainCookiePersisterIframe');
		//delete_cookie(cname);
		//Setting cookie for the current domain
		var currentDomain = extractDomainName(window.location.host);
		document.cookie = cname + "=" + cvalue + "; path=/;domain=" + currentDomain;
		//setting cookie for alternate domain
		if (iframeWindow != null) {
			var alternateDomain = extractDomainName($(iframeWindow).attr("src"));
			if (alternateDomain != "") {
				iframeWindow.contentWindow.postMessage({ key: cname, data: cvalue, domainName: alternateDomain }, iframeWindow.src);
				iframeWindow.contentWindow.postMessage({ key: cname, data: cvalue, domainName: alternateDomain, removeCookie: true }, iframeWindow.src);
			}
		}
		delete_cookie(cname);
	}
}
function delete_cookie(name) {
	if (getCookie(name)) {
		document.cookie = name + "=test;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT";
	}
}
/*END Cross Domain cookie functionality*/


function extractDomainName(domainName) {
	var redirectionDomain = domainName.split("//"), extractedDomain = "";
	domainName = (redirectionDomain.length > 1) ? (redirectionDomain[1].split("/")[0]) : (redirectionDomain[0].split("/")[0]);
	extractedDomain = (domainName.split(".").length > 2) ? "." + domainName.split(".")[1] + "." + domainName.split(".")[2] : "." + domainName;
	return extractedDomain;
}
//setCookie
function setCookie(cname, cvalue) {
	document.cookie = cname + "=" + cvalue + "; path=/";
}
//Get cookie
function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');

	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1);
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function enablePopoverMyStore(storeDistance) {
	var yourStoreLocatorTooltip = $("body").find('.your-store-locator-tooltip');
	var yourStoreLocatorTooltipTemplate = $('.global-nav-your-store-locator-tooltip').html();
	var favStore = getCookie("favStore");
	var favStoreId = favStore.split("|");
	favStoreId = favStoreId[1];

	yourStoreLocatorTooltip.click(function (event) {
		event.preventDefault();
	});
	yourStoreLocatorTooltip.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: yourStoreLocatorTooltipTemplate,
		trigger: 'hover',
		delay: { show: 50, hide: 400 }
	});

	yourStoreLocatorTooltip.on('show.bs.popover', function () {
		$(this).addClass('clicked');
	});

	yourStoreLocatorTooltip.on('hidden.bs.popover', function () {
		$(this).removeClass('clicked');
	});
	yourStoreLocatorTooltip.attr("onmouseover", "javascript:showMyFavStore('" + contextPath + "','" + favStoreId + "','" + storeDistance + "','');");
}

//for find-a-store
function findInStoreSearch() {
	keepMapStatus = false
	$('#findInStoreSearch').click();
}

//For populating the stores from the JSON response to the corresponding modal or overlay
function populateStoresFromJsonSL($this, resultStores) {
	var $root = $this.parents(".sl_parent");
	var $parent = $root.find(".store-locator-pick-store");

	stores = resultStores;

	if ($parent.find('.sl_store_listing_ul li').length > 0)
		$parent.find('.sl_store_listing_ul').html("");

	$parent.removeClass("default_view");
	$parent.find('.sl_store_count').text(stores.length);
	$parent.find('.sl_search_term').text($parent.find('.storeLocatorField').val());
	if (stores.length) {
		for (i = 0; i < stores.length; i++) {
			var store = stores[i];
			var FindinstoreResultItem = appendFindInStoreResultItemSL(i, store, store.chainCode, $this);
		}



		// ---------------
		var scrollable = '';
		//if(formId == 'nearby-stores'){
		scrollable = $('.sl_parent').find('.tse-scrollable');
		scrollable.TrackpadScrollEmulator('recalculate');
		//}
		// -------------------


		$bru_ref = ($(".sl_parent .store-locator-pick-store").find(".bru-results,.bru-tru-results").length) ? $(".store-locator-pick-store").find(".bru-results,.bru-tru-results") : $(".store-locator-pick-store").find(".BRUresults,.bru-tru-results");
		$tru_ref = ($(".sl_parent .store-locator-pick-store").find(".tru-results,.bru-tru-results").length) ? $(".store-locator-pick-store").find(".tru-results,.bru-tru-results") : $(".store-locator-pick-store").find(".TRUresults,.bru-tru-results");
		$allRef = $(".sl_parent .store-locator-pick-store").find(".bru-results ,.tru-results ")

		$root.find(".sl_store_search_form").slideUp();
		$root.find(".sl_store_search_form").removeClass("ft-size-adjuster-sl");
		var flagForActive = true;
		$root.find("#sl_store-locator-results-tabs li").each(function () {
			if ($(this).hasClass("active")) {
				$(this).find("a").click();
				flagForActive = false;
			}
		});
		if (flagForActive) {
			$root.find("#sl_store-locator-results-tabs .all_results_tab").trigger("click");
		}



	}
	else {
		//$parent.addClass("zero_results");
		$parent.find('p.noresults').show();
		$parent.find('p.noresults').text("");
	}

	$('#nearby-stores').TrackpadScrollEmulator({
		autoHide: true
	});
}

/* PDP  */
function appendFindInStoreResultItemSL(i, store, type, $this) {

	var $root = $this.parents(".sl_parent");
	var $parent = $root.find(".store-locator-pick-store");
	var $ul_ref = $parent.find(".sl_store_listing_ul");
	var contextpath = $(".navbar.navbar-bottom").find(".contextPath").val();
	space = "&nbsp;";
	var signature = getCookie('myStoreAddressSignature');
	var favStoreId = getCookie("favStore");
	favStoreId = favStoreId.split("|");
	favStoreId = favStoreId[1];

	var listElement = $(document.createElement('li'));
	listElement.addClass('store-info');
	if (type === 'BRU')
		listElement.addClass('bru-results');
	else
		listElement.addClass('tru-results');
	if (i > 9) {
		listElement.css("display", "none");
	}

	if (store.isSuperStoreServiceAval === true) {
		listElement.addClass('bru-tru-results');
	}
	//var contextpath="'"+$('#contextpathfindinstore').val()+"'";
	var detailsparms = 'javascript:viewStoreDetails(this,"' + i + '","' + contextpath + '")';
	var storeName = convertToHtmlEntities(store.name);
	var makemystore = 'javascript:makeMyStore(event,"' + contextpath + '","' + store.StoreID + '", "' + store.postalCode + '","' + store.city + '",this,"' + storeName + '","' + store.distance + '","' + store.signature + '")';

	/*	if(store.closingTimeMsg != "undefined" && store.closingTimeMsg != null){
			listElement.append('<div class="row row-no-padding"><div class="col-md-9 col-no-padding"><h2>'+store.name+' '+ store.city +'<br>'+store.StreetAddress+'</h2><p>'+store.closingTimeMsg+'</p><div class="make-store-link"><a href="javascript:void(0);" id="toolipMakeMyStore" class="make-my-store-link">make this my store</a><div class="my-store">my store</div></div></div><div class="col-md-3 col-no-padding pull-right"><p class="line_height_adjuster_distance"><span class="miles">'+ store.distance +'</span> mi.</p><a href="javascript:void(0)" class="store-details" >details</a></div></div><hr>');
		}else{
			listElement.append('<div class="row row-no-padding"><div class="col-md-9 col-no-padding"><h2>'+store.name+' '+ store.city +'<br>'+store.StreetAddress+'</h2><div class="make-store-link"><a href="javascript:void(0);" id="toolipMakeMyStore" class="make-my-store-link">make this my store</a><div class="my-store">my store</div></div></div><div class="col-md-3 col-no-padding pull-right"><p class="line_height_adjuster_distance"><span class="miles">'+ store.distance +'</span> mi.</p><a href="javascript:void(0)" class="store-details" >details</a></div></div><hr>');
		}
	*/
	if (store.closingTimeMsg != "undefined" && store.closingTimeMsg != null) {
		listElement.append('<div class="row row-no-padding"><div class="col-md-9 col-no-padding"><h2>' + store.name + '</h2><p>' + store.closingTimeMsg + '</p><p class="store-address">' + store.StreetAddress + '</p><div class="make-store-link"><a href="javascript:void(0);" id="toolipMakeMyStore" class="make-my-store-link">' + makeThisMyStore + '</a><div class="my-store">' + myStore + '</div></div><div class="store-details-container"><a href="javascript:void(0)" class="store-details" >' + detailsDisplay + '</a></div></div><div class="col-md-3 col-no-padding pull-right"><p class="line_height_adjuster_distance"><span class="miles">' + store.distance + '</span>' + space + milesAway + '</p></div></div><hr>');
	} else {
		listElement.append('<div class="row row-no-padding"><div class="col-md-9 col-no-padding"><h2>' + store.name + '</h2><p class="store-address">' + store.StreetAddress + '</p><div class="make-store-link"><a href="javascript:void(0);" id="toolipMakeMyStore" class="make-my-store-link">' + makeThisMyStore + '</a><div class="my-store">' + myStore + '</div></div><div class="store-details-container"><a href="javascript:void(0)" class="store-details" >' + detailsDisplay + '</a></div></div><div class="col-md-3 col-no-padding pull-right"><p class="line_height_adjuster_distance"><span class="miles">' + store.distance + '</span>' + space + milesAway + '</p></div></div><hr>');
	}



	listElement.find('.make-my-store-link').attr('onclick', makemystore);

	listElement.find('.make-my-store-link').data('address', store.StreetAddress);

	if (!store.isEligibleForfavStore) {
		listElement.find('.make-my-store-link').remove();
	} else {
		listElement.find('.make-my-store-link').css("display", "block");
	}

	if (store.StoreID === favStoreId) {
		listElement.find('.my-store').show();
		listElement.find('.make-my-store-link').remove();
	}
	else {
		listElement.find('.my-store').hide();
	}



	listElement.find('a.store-details').attr('onclick', detailsparms);
	listElement.find('h2').attr('onclick', detailsparms);
	listElement.append("<input type='hidden' class='storeIdOmni' />")
	listElement.find("storeIdOmni").val(store.StoreID);
	$ul_ref.append(listElement);

	reInitScrollBarfindinstore1('nearby-stores', 340);
	///////////////////// scrollable.TrackpadScrollEmulator('recalculate');  Need to work
	//Added for omniture
	//omniIspuStoreServed(store.StoreID);
	return listElement;
}

function reInitScrollBarfindinstore1(formId, height) {
	var scrollable = '';
	var scrollableContent = '';
	if (formId == 'nearby-stores') {
		scrollable = $('.sl_parent').find('.tse-scrollable');
		///tse-scroll-content
	} else if (formId == 'store-locator-results-scrollable') {
		scrollable = $(".find-in-store-results").find('.tse-scrollable');
		scrollableContent = $(".find-in-store-results").find('.tse-scrollable').find(".tse-scroll-content");
	}

	var recalculate = function () {
		setTimeout(function () {
			////////////////console.log("PPPPPPPPP");
			//scrollable.TrackpadScrollEmulator('recalculate');
			if (formId == 'nearby-stores') {
				scrollable.find('.tse-scroll-content').height(height);
			} else if (formId == 'store-locator-results-scrollable') {
				scrollable.height(height);
				scrollableContent.height(height);
			}
		}, 500);
	};

	scrollable.TrackpadScrollEmulator({
		wrapContent: false,
		autoHide: true
	});


	recalculate();
}
var mmsCity = {};
function appendFindInStoreResultItem(i, store, type) {
	var listElement = $(document.createElement('li'));
	var findinstoreresultsSingleContent = $('.results-single-outer');
	var favStoreId = getCookie("favStore");
	favStoreId = favStoreId.split("|");
	if (favStoreId.length > 1) {
		favStoreId = favStoreId[1];
		if (favStoreId.length > 0) {
			myStoreID = "&myStoreId=" + favStoreId + ""
		}
	}
	if (type === 'BRU')
		listElement.addClass('bru-results');
	else
		listElement.addClass('tru-results');
	if (store.isSuperStoreServiceAval === true) {
		listElement.addClass('bru-tru-results');
	}
	if (i > 9) {
		listElement.css("display", "none");
	}
	var contextpath = "'" + $('#contextpathfindinstore').val() + "'";
	var currentStore = store;
	///var detailsparms = 'javascript:findInStoreDetails('+contextpath +","+store.StoreID+","+store.distance+')';
	//var detailsparms = 'javascript:viewStoreDetailsPDP(this,"'+i+'","'+contextpath+'")';
	//var detailsparms = 'javascript:viewStoreDetailsPDP(this,"'+i+'","'+contextpath+'")';
	var detailsparms = "javascript:getDrivingDirections('" + currentStore.StreetAddress + "','" + currentStore.city + "','" + currentStore.postalCode + "','" + currentStore.latitude + "','" + currentStore.longitude + "','" + currentStore.name + "')";

	var productId = $(".productIdForCart").val();
	if (productId == undefined) {
		productId = $("#productIdForCart").val();
	}
	if (productId == undefined) {
		productId = $("#productIdForCompare").val();
	}
	var skuId = $("#pdpSkuId").val();
	var addToCart = "javascript:addItemToCart(" + "'" + productId + "'" + "," + "'" + skuId + "'" + "," + store.StoreID + "," + contextpath + ",this,'fromStore')";
	//var addToCartParams = '('+"'"+productId+"'"+","+"'"+skuId+"'"+","+store.StoreID+"," +contextpath+')';
	/*mmsCity = '{"city":"'+store.city+'"}';
	temp = JSON.parse(mmsCity)*/
	var storename = convertToHtmlEntities(store.name);
	var showConfirmationWindowForDefaultStore = 'showConfirmationWindowForDefaultStore("' + productId + '","' + skuId + '",' + store.StoreID + ',' + contextpath + ',"' + store.postalCode + '",this,"' + store['city'] + '","' + store.StreetAddress + '","' + storename + '","' + store.distance + '","' + $('#relationshipItemId').val() + '")';
	var matureVal = $('.maturevalue').length ? $('.maturevalue').val() : "false";
	if (!$('#findInStoreModal').hasClass('pdp-findinstore-popup')) {
		var makemystore = 'javascript:setCookie(' + $('#FindinStorePDPField').val() + "," + store.StoreID + ')';
		findinstoreresultsSingleContent.find('.make-my-store').attr('onclick', makemystore);
		findinstoreresultsSingleContent.find('.my-store').hide();
	}

	if (store.closingTimeMsg != "undefined" && store.closingTimeMsg != null) {
		findinstoreresultsSingleContent.find('p.opentimings').html(store.closingTimeMsg);
	} else {
		findinstoreresultsSingleContent.find('p.opentimings').html("");
	}


	findinstoreresultsSingleContent.find('a.findinstore-details').attr('onclick', detailsparms);
	findinstoreresultsSingleContent.find('h4').html(store.name);
	findinstoreresultsSingleContent.find('p.address').html(store.StreetAddress);
	findinstoreresultsSingleContent.find('span.miles').html(store.distance + " ");
	if (store.unCartable) {
		findinstoreresultsSingleContent.find('p.pdp-shoppingcart-btn').html("<button disabled='disabled' class='add-to-cart fade-add-to-cart' >add to cart</button>");
	} else {
		if ($('#findInStoreModal').hasClass('shoppingcart-findinstore') && $('#findInStoreModal').hasClass('cartItemFlag')) {
			if (store.StoreID == favStoreId) {
				var relationshipItemIdForStore = 'javascript:updateLineItemISPUSG(' + "'" + $('#relationshipItemId').val() + "'" + "," + store.StoreID + "," + "'shoppingCart'" + ')';
				findinstoreresultsSingleContent.find('p.pdp-shoppingcart-btn').html("<button class=select-a-store onclick=" + relationshipItemIdForStore + ">" + selectAStore + "</button>");
			}
			else {
				findinstoreresultsSingleContent.find('p.pdp-shoppingcart-btn').html("<button class=select-a-store data-dismiss=modal data-toggle=modal data-target=#  >" + selectAStore + "</button>");
				findinstoreresultsSingleContent.find('p.pdp-shoppingcart-btn').find("button").attr("onclick", showConfirmationWindowForDefaultStore);
			}
		} else if ($('#findInStoreModal').hasClass('shoppingcart-findinstore') && !$('#findInStoreModal').hasClass('cartItemFlag')) {
			findinstoreresultsSingleContent.find('p.pdp-shoppingcart-btn').html("<button data-dismiss=modal data-toggle=modal onclick=" + addToCart + " data-target='#shoppingCartModal' class=add-to-cart >add to cart</button>");
		} else if ($('#findInStoreModal').hasClass('inStorePickUp-findinstore')) {
			var relationshipItemIdForStore = 'javascript:updateLineItemISPUSG(' + "'" + $('#relationshipItemId').val() + "'" + "," + store.StoreID + "," + "'inStorePickUp'" + ')';
			findinstoreresultsSingleContent.find('p.pdp-shoppingcart-btn').html("<button class=select-a-store onclick=" + relationshipItemIdForStore + ">" + selectAStore + "</button>");
		} else if ($('#findInStoreModal').hasClass('review-findinstore')) {
			var relationshipItemIdForStore = 'javascript:updateLineItemISPUSG(' + "'" + $('#relationshipItemId').val() + "'" + "," + store.StoreID + "," + "'Review'" + ')';
			findinstoreresultsSingleContent.find('p.pdp-shoppingcart-btn').html("<button class=select-a-store onclick=" + relationshipItemIdForStore + ">" + selectAStore + "</button>");
		} else {
			if (store.StoreID == favStoreId) {
				if (matureVal == "true") {
					findinstoreresultsSingleContent.find('p.pdp-shoppingcart-btn').html("<button data-dismiss=modal data-toggle=modal onclick='javascript:esrbOnClickAddToCart(this)' data-productId=" + productId + " data-skuId=" + skuId + " data-StoreID=" + store.StoreID + " data-contextpath=" + contextpath + " data-onclick=" + addToCart + " data-target='.esrbratingModel' class=add-to-cart >add to cart</button>");
				} else {
					findinstoreresultsSingleContent.find('p.pdp-shoppingcart-btn').html("<button data-dismiss=modal data-toggle=modal onclick=" + addToCart + " data-target='#shoppingCartModal' class=add-to-cart >add to cart</button>");
				}

			} else {
				/*if(matureVal){
					findinstoreresultsSingleContent.find('p.pdp-shoppingcart-btn').html("<button data-dismiss=modal data-toggle=modal data-target=# class=add-to-cart >add to cart</button>");
					findinstoreresultsSingleContent.find('p.pdp-shoppingcart-btn').find("button").attr("data-onclick",showConfirmationWindowForDefaultStore);
				}else{*/
				findinstoreresultsSingleContent.find('p.pdp-shoppingcart-btn').html("<button data-dismiss=modal data-toggle=modal data-target=# class=add-to-cart >add to cart</button>");
				findinstoreresultsSingleContent.find('p.pdp-shoppingcart-btn').find("button").attr("onclick", showConfirmationWindowForDefaultStore);
				/*}*/
			}

		}
	}

	//Check for unavailable at this store
	if ((store.storeHoursMsg != undefined) && (store.storeHoursMsg.toLowerCase().indexOf("out of stock") > -1 || store.storeHoursMsg.toLowerCase().indexOf("outofstock") > -1 || store.storeHoursMsg.toLowerCase().indexOf("unavailable at this store") > -1)) {
		findinstoreresultsSingleContent.find('.outofstock-sec').css("display", "block");
		findinstoreresultsSingleContent.find('.free-store-sec').css("display", "none");
		findinstoreresultsSingleContent.find('span.outofstock-msg').html(store.storeHoursMsg);
		findinstoreresultsSingleContent.find('p.pdp-shoppingcart-btn').css("display", "none");
		findinstoreresultsSingleContent.find('.outOfStockStatus').val("true");
		/*omniIspuStoreServed();
		omniIspuOutOfStock();*/
		/*omniIspuStoreServed(store.StoreID);
		omniIspuOutOfStock(store.StoreID);*/
	}
	else {
		findinstoreresultsSingleContent.find('.outofstock-sec').css("display", "none");
		findinstoreresultsSingleContent.find('.free-store-sec').css("display", "block");
		findinstoreresultsSingleContent.find('p.free-store-pickup').html(store.storeHoursMsg);
		findinstoreresultsSingleContent.find('p.pdp-shoppingcart-btn').css("display", "block");
		findinstoreresultsSingleContent.find('.outOfStockStatus').val("false");
		/*omniIspuStoreServed();*/
		/*omniIspuStoreServed(store.StoreID);*/
	}
	findinstoreresultsSingleContent.find('.storeIdForPDP').val(store.StoreID);
	listElement.append(findinstoreresultsSingleContent.html());
	listElement.append("<hr>")
	$('#find-in-store-results #store-locator-results-scrollable ul').append(listElement);
	//reInitScrollBarfindinstore('store-locator-results-scrollable', 340);

	reInitScrollBarfindinstore1('nearby-stores', 340);
	return listElement;
}

// For populating the stores from the JSON response to the corresponding modal or overlay
function populateStoresFromJson($this, resultStores) {
	var findinstores = resultStores;
	/* _R-F-22 */
	$('#findInStoreModal').find("#store-locator-results-header, .find-in-store-overlay .shipping-timelines").css("display", "block");
	$(".pdp-findinstore-popup .modal-content").css("height", "590px");
	/* _R-F-22 */

	$('#findInStoreModal').find("#find-in-store-results header, #find-in-store-results .change-location").show();

	$('#findInStoreModal').find("#find-in-store-results .find-in-store-search,#find-in-store-results #store-locator-results-tabs, #find-in-store-results #store-locator-results-scrollable").hide();
	/* _R-F-22 */
	$("#find-in-store-results .find-in-store-search").find("h2").insertBefore('.find-in-store-overlay .sl_root .sl_parent');
	/* _R-F-22 */
	if ($(".findinstore-pdp-results .tse-scroll-content ul li").length > 0)
		$(".findinstore-pdp-results .tse-scroll-content ul li").remove();

	$('.findinstore-pdp-results .tse-scroll-content p.noresults').css("display", "none");
	if (findinstores != undefined) {

		$('.nearby-stores-count').text(findinstores.length);
		$('.forzip').text($('#FindinStorePDPField').val());
		if (findinstores.length) {
			$('#findInStoreModal').find("#find-in-store-results #store-locator-results-tabs, #find-in-store-results #store-locator-results-scrollable").show();
			for (i = 0; i < findinstores.length; i++) {
				var store = findinstores[i];
				var FindinstoreResultItem = appendFindInStoreResultItem(i, store, store.chainCode);
			}
			$('.find-in-store-overlay #store-locator-results-tabs li').removeClass('active');
			$('.All').addClass('active');
			$bru_ref = $(".find-in-store-results #store-locator-results-scrollable").find(".bru-results,.bru-tru-results")
			$tru_ref = $(".find-in-store-results #store-locator-results-scrollable").find(".tru-results,.bru-tru-results")
			$allRef = $(".find-in-store-results #store-locator-results-scrollable").find(".bru-results ,.tru-results ")
		}
		reInitScrollBarfindinstore1('store-locator-results-scrollable', 340);
		//reInitScrollBarfindinstore1('store-locator-results-scrollable', 480); // reverted to 340px since content is overflowing from the ISPU overlay
		/*$(".find-in-store-overlay").find(".tse-scrollbar").show();
		$(".find-in-store-overlay").find(".tse-scrollbar .drag-handle").addClass("visible");*/

		if ($allRef.length <= lms_per_pagination) {
			$(".find-in-store-overlay #show-more-stores").hide();
		} else {
			$(".find-in-store-overlay #show-more-stores").show();
		}

		var flagForActive = true;
		$(".find-in-store-overlay .store-locator-results-tabs li").each(function () {
			if ($(this).hasClass("active")) {
				$(this).find("a").click();
				flagForActive = false;
			}
		});
		if (flagForActive) {
			/*.find-in-store-overlay #store-locator-results-tabs li a*/
			$(".find-in-store-overlay #store-locator-results-tabs li:first-child a").trigger("click");
		}
		$('.find-in-store-overlay .findinstore-pdp-results li hr.whiteColor').removeClass("whiteColor");
		$('.find-in-store-overlay .findinstore-pdp-results li:visible:last hr').addClass("whiteColor");
	}
	else {
		$('.findinstore-pdp-results .tse-scroll-content p.noresults').css("display", "block");
	}
}

//process the show more stores details
function findInMoreStores(pageNumber) {
	$('#findInCurrentResultPageNum').val(pageNumber);
	$('#findInCurrentResultPageNum').click();
}

//process the show-more-stores form
$(document).on('click', '#findCurrentResultPageNum', function (e) {
	var pageNum = $("#findInCurrentResultPageNum").val();
	var showDivClass = "find-in-load-more-stores-" + pageNum;
	var hideDivClass = "find-in-show-more-" + pageNum;
	$.ajax({
		type: "POST",
		cache: false,
		url: contextPath + 'storelocator/findInStoreSearchResults.jsp',
		data: $('#findInStoreLocatorPagination').serializeArray(),
		datatype: "html",
		success: function (data) {
			$('.' + hideDivClass).hide();
			$('.' + showDivClass).html(data);
			$('.' + showDivClass).show();
		}
	});
	return false;
});

//process the find-a-store form
function findInStoreDetails(contextPath, storeId, storeDistance) {
	$('.store-locator-tooltip-store-details').hide();
	$.ajax({
		type: "POST",
		cache: false,
		url: contextPath + 'storelocator/findInStoreDetails.jsp',
		data: 'storeId=' + storeId + '&storeDistance=' + storeDistance,
		datatype: "html",
		success: function (data) {
			$('.store-locator-tooltip-store-details').html(data);
			$('.store-locator-tooltip-store-details').show();
		}
	});
}

//process the find-site-store form
function findInSiteStore(siteId) {
	$("#findInStoreSiteIds").val(siteId);
	$('#findInStoreSiteSearch').click();
}

//process the find-site-store form
$(document).on('click', '#findInStoreSiteSearch', function (e) {
	var siteId = $("#findInStoreSiteIds").val();
	var divClass = "site-" + siteId;
	$.ajax({
		type: "POST",
		cache: false,
		url: contextPath + 'storelocator/findInStoreSearchResults.jsp',
		data: $('#findInStoreSiteLocatorSearch').serializeArray(),
		datatype: "html",
		success: function (data) {
			$('.store-locator-tooltip-store-details').html(data);
			$('.store-locator-tooltip-store-details').show();
		}
	});
	return false;
});

//process the find-site-store form
function findInAllStore(contextPath) {
	$('.find-in-store-results').slideToggle();
	$.ajax({
		type: "POST",
		cache: false,
		url: contextPath + 'storelocator/findInStoreSearchResults.jsp',
		data: 'notifyme=true',
		datatype: "html",
		success: function (data) {
			$('.find-in-store-results').slideToggle();
			$('#store-locator-results-scrollable').html(data);
			$('#store-locator-results-scrollable').show();
			$('.site-All').addClass('active');
		}
	});
	return false;
}

//This function is used to load Google map in storelocator page.

function storeLocator(searchForm, mapCont, resultList) {

	var latitudeCode = $('#latitudeCode').val();
	var longitudeCode = $('#longitudeCode').val();
	//Display google map with default longitude and latitude
	var latlng = new google.maps.LatLng(latitudeCode, longitudeCode);
	var myOptions = {
		zoom: 3,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		scrollwheel: false
	},
		locationField = searchForm.find('input[type="text"]');
	window.storeLocatorMap = {
		resultList: resultList
	};
	window.storeMap = new google.maps.Map(mapCont[0], myOptions);
	//call ajax function when user clicks on Search button
	searchForm.find('a').click(function () {
		searchForm.submit();
		return false;
	});
	if ($.trim(locationField.val()) == '') {
		storeLocatorMap.origLabel = searchForm.find('label').html()
		locationField.val(storeLocatorMap.origLabel);
		locationField.one('focus', function () {
			$(this).val('');
		});
	}
	searchForm.submit(function () {
		var searchForm = $(this),
			locationField = searchForm.find('input[type="text"]');
		var errorEl = searchForm.siblings('.error');
		if ($.trim(locationField.val()) != '' && locationField.val() != storeLocatorMap.origLabel) {
			// Remove all existing markers on new search
			if (typeof storeLocatorMap.markers != 'undefined') {
				for (var m = 0; m < storeLocatorMap.markers.length; m++) {
					storeLocatorMap.markers[m].marker.setMap(null);
				};
			}
			storeLocatorMap.resultList.children().remove();
			// Load results and place markers
			submitSearchForm();
			$('.post label').show();

		} else if (errorEl.length > 0) {
			if (typeof storeLocatorMap.markers != 'undefined') {
				for (var m = 0; m < storeLocatorMap.markers.length; m++) {
					storeLocatorMap.markers[m].marker.setMap(null);
				};
			}
			storeLocatorMap.resultList.children().remove();
			errorEl.html(validZipCodeOrCity).show();
		}
		return false;
	});
}

function submitSearchForm() {
	storeLocatorMap.markers = [];
	$('#storeLocatorSearch').ajaxSubmit({
		beforeSubmit: function () {
			errorEl = $('#storeLocatorSearch').siblings('.error');
			if (errorEl.length > 0) {
				errorEl.hide();
			}
		},
		url: contextPath + "storelocator/getJsonResponse.jsp",
		success: function (response) {
			if (response.isSuccess == 'true') {
				var stores = (typeof response.stores) == 'string' ? eval('(' + response.stores + ')') : response.stores;
				if (stores != null && stores != '') {
					$('.store_address').css({ "display": "block" });
					$("#searchresults").load(contextPath + 'storelocator/storeLocatorResults.jsp');
					showMapStores(stores);
				} else {
					$('.store_address').css({ "display": "none" });
					$('.locatorHead .error').html(unableToCater).show();
				}

			} else {
				$('.store_address').css({ "display": "none" });
				$('.locatorHead .error').html(response.error).show();
			}
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			$('.store_address').css({ "display": "none" });
			$('.locatorHead .error').html(unableToCater).show();
		},
		dataType: "json"
	});
}

// This function is used to display store locations on map based on store results.

function showMapStores(stores) {
	var points = [];
	if (stores.length != 0) {
		for (var i = 0; i <= stores.length; i++) {
			if (stores[i] != undefined) {
				var point = new google.maps.LatLng(stores[i].latitude, stores[i].longitude),
					marker = createMarker(point, i, stores[i].name, stores[i].phoneNumber, stores[i].city + ", " + stores[i].stateAddress + " " + stores[i].postalCode, stores[i].StreetAddress, stores[i].StoreID);
				marker.setMap(storeMap);
				if (i == 0) {
					var zoomLevel = stores[i].zoomLevel;
					storeMap.setCenter(point, zoomLevel);
					storeMap.setZoom(parseInt(zoomLevel));
					points.push(point);
				}
			}
		}
	}
}

//This function is used to display Markers on Google map.
function createMarker(point, index, storeName, phoneNumber, cityStateZip, streetAddress, storeID) {
	//If phone number is null display empty String
	if (phoneNumber == null) {
		phoneNumber = '';
	}
	var marker = new google.maps.Marker({
		position: point,
		animation: google.maps.Animation.DROP
	});
	marker.setIcon("http://googlemaps.googlermania.com/img/marker_flag.png");
	marker.setShadow("http://googlemaps.googlermania.com/img/marker_shadow.png");
	var markerHtml = '<dl class="storeInfo">' +
		'<dd>' +
		'<div class="main_storeaddress">' +
		'<div class="storeAddress">' +
		'<div class="storeAddress_inner">' +
		'<label> ' + storeName + '</label>' +
		'<div class="addressStreet"></div>' + streetAddress +
		'<div class="addressCityStateZip"></div>' + cityStateZip +
		'</div>' +
		'</div>' +
		'<div class="storePhone">' +
		'<label>' + phone + ':</label>' + phoneNumber +
		'</div>'
	'</div>' +
		'</dd>' +
		'</dl>';
	storeLocatorMap.markers.push({});
	storeLocatorMap.markers[index].el = $(markerHtml);
	$(markerHtml).find('dt').html("<img src='http://googlemaps.googlermania.com/img/marker_flag.png'/>");

	infoWindow = new google.maps.InfoWindow();
	google.maps.event.addListener(marker, "click", function () {
		infoWindow.setContent(markerHtml);
		infoWindow.open(storeMap, marker);
	});

	var resultStoreItem = storeLocatorMap.markers[index].el.clone();
	resultStoreItem.find('#getDrivingDirections').click(function () { $.getDrivingDirections(streetAddress, cityStateZip, userAddress) });
	storeLocatorMap.resultList.append($('<li></li>').append(resultStoreItem));
	storeLocatorMap.markers[index].marker = marker;
	return marker;
}
//This function is used to call StoreLocatorFormHandler.handleCurrentResultPageNum method 
//when user clicks on page number in store Results page.
function nextPage(pageNumber) {
	if (typeof storeLocatorMap.markers != 'undefined') {
		for (var m = 0; m < storeLocatorMap.markers.length; m++) {
			if (storeLocatorMap.markers[m].marker != 'undefined') {
				storeLocatorMap.markers[m].marker.setMap(null);
			}
		};
	}
	storeLocatorMap.markers = [];
	$("#currentResultPageNum").val(pageNumber);
	dataString = $('#storeLocatorPagination').serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		url: contextPath + "storeLocator/getJsonResponse.jsp",
		dataType: "json",
		success: function (response) {
			if (response.isSuccess == 'true') {
				var stores = (typeof response.stores) == 'string' ? eval('(' + response.stores + ')') : response.stores;
				if (stores != null && stores != '') {
					$("#searchresults").load(contextPath + 'storelocator/storeLocatorResults.jsp');
					showMapStores(stores);
				} else {
					$('.store_address').css({ "display": "none" });
					$('.locatorHead .error').html(response.error).show();
				}
			} else {
				$('.store_address').css({ "display": "none" });
				$('.locatorHead .error').html(response.error).show();
			}
		},
		error: function (jqXHR, textStatus, errorThrown) {
			$('.store_address').css({ "display": "none" });
			$('.locatorHead .error').html(response.error).show();
		}
	});

}
function viewThisStore(storeID) {
	storepop(storeID);
}

function storepop(storeID) {
	$('.store_more_fullBox').show();
	$.ajax({
		url: contextPath + "storelocator/viewStoreDetails.jsp?storeId=" + storeID,
		type: "POST",
		success: function (data) {
			$(".store_more").html(data);
			/**********************************/
			var winWidth = $(window).width();
			var winHeight = $(window).height();
			var popWidth = $('.store_more').width();
			var popHeight = $('.store_more').height();
			var popupLeftPos = (winWidth - popWidth) / 2; 	//(popupLeftPos);
			var popupTopPos = (winHeight - popHeight) / 2; 	//(popupLeftPos);
			$(".store_more").css("left", popupLeftPos);
			$(".store_more").css("top", popupTopPos);
			$(window).resize(function (e) {
				var winWidth = $(window).width();
				var winHeight = $(window).height();
				var popWidth = $('.store_more').width();
				var popHeight = $('.store_more').height();
				var popupLeftPos = (winWidth - popWidth) / 2; 	//(popupLeftPos);
				var popupTopPos = (winHeight - popHeight) / 2; 	//(popupLeftPos);
				$(".store_more").css("left", popupLeftPos);
				$(".store_more").css("top", popupTopPos);
			});
			/**********************************/
			$(".store_more").show();
			$('.smClose, #smCloseX').click(function () {
				$('.store_more_fullBox').css('display', 'none');
				$('.store_more').css('display', 'none');
			})
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log(jqXHR);
		}
	});
}

//This function contains Driving Directions functionality.
function getDrivingDirections(streetAddress, cityState, zipCode, lat, lng, storeName) {
	var windowWidth = 600;
	var windowHeight = 600;
	var windowLeft = parseInt((screen.availWidth / 2) - (windowWidth / 2));
	var windowTop = parseInt((screen.availHeight / 2) - (windowHeight / 2));
	var windowSize = "width=" + windowWidth + ",height=" + windowHeight + "left=" + windowLeft + ",top=" + windowTop + "screenX=" + windowLeft + ",screenY=" + windowTop;
	streetAddress = streetAddress.split("<br>").join(" ");
	var destination = streetAddress + " " + cityState;

	var param = '?desti=' + destination + '&lat=' + lat + '&lng=' + lng + '&storename=' + storeName;
	var directionWindow = window.open('/storelocator/getDirectionMap.jsp' + param, '_blank');
	directionWindow.focus();
}


function doPDPStoreSearch(result, curObject) {
	var $this = $(curObject);

	if (result.errors) {
		$(".find-in-store-overlay").find("p.sl_errormsg").text(result.errors[0].errorMessage);
		$(".find-in-store-overlay").find("p.sl_errormsg").show();
		$(".find-in-store-overlay").find("#FindinStorePDPField").addClass("sl_input_error_msg");
		$(".find-in-store-overlay #store-locator-results-tabs").hide();
		$(".find-in-store-overlay #store-locator-results-scrollable").hide();
		$(".store-locator-results").find('h3').first().hide();
		$(".store-locator-results").find('a').first().hide();
		$(".find-in-store-search").find('img').first().hide();
	} else if (result.error) {
		$(".find-in-store-overlay").find("p.sl_errormsg").text(result.error[0].errorMessage);
		$(".find-in-store-overlay").find("p.sl_errormsg").show();
		$(".find-in-store-overlay").find("#FindinStorePDPField").addClass("sl_input_error_msg");
		$(".find-in-store-overlay #store-locator-results-tabs").hide();
		$(".find-in-store-overlay #store-locator-results-scrollable").hide();
		$(".store-locator-results").find('h3').first().hide();
		$(".store-locator-results").find('a').first().hide();
		$(".find-in-store-search").find('img').first().hide();
	} else {
		populateStoresFromJson($this, result.stores);
	}
}

function doFindInStoreSearch(result, curObject) {

	var $this = $(curObject);
	if (result.errors) {
		$this.parents(".googleMapGroup").find("p.sl_wrongsearch_errormsg").text(weCouldNotFind + '"' + $this.parents(".googleMapGroup").find("#storeLocatorAddressEntry").val() + '"');
		if ((result.errors[0].errorMessage.indexOf("No results found") > -1)) {
			$this.parents(".googleMapGroup").find("p.sl_wrongsearch_errormsg").hide();
		} else {
			$this.parents(".googleMapGroup").find("p.sl_wrongsearch_errormsg").show();
		}
		$this.parents(".googleMapGroup").find("p.sl_errormsg").text(result.errors[0].errorMessage);
		$this.parents(".googleMapGroup").find("p.sl_errormsg").show();
		$this.parents(".googleMapGroup").find("#storeLocatorAddressEntry").addClass("sl_input_error_msg");
		$this.parents(".googleMapGroup").find("#change-location-cross").hide();
		$this.parents(".googleMapGroup").find(".no_specific_chain_stores").hide();

		$("#stores_count_display").hide();
		$(".googleMapGroup").find("#store-locator-results-tabs").hide();
		$("#store-locator-results-scrollable").hide();

		var mapOptions = {
			zoom: 4,
			center: new google.maps.LatLng(us_lat, us_long),
			zoomControl: false,
			zoomControlOptions: {
				position: google.maps.ControlPosition.RIGHT_BOTTOM,
				style: google.maps.ZoomControlStyle.SMALL
			},
			panControl: false,
			streetViewControl: false,
			mapTypeControl: false,
			scrollwheel: false,
			draggable: false
		};

		map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);

	} else if (result.error) {
		$this.parents(".googleMapGroup").find("p.sl_wrongsearch_errormsg").text(weCouldNotFind + '"' + $this.parents(".googleMapGroup").find("#storeLocatorAddressEntry").val() + '"');
		if ((result.error[0].errorMessage.indexOf("No results found") > -1)) {
			$this.parents(".googleMapGroup").find("p.sl_wrongsearch_errormsg").hide();
		} else {
			$this.parents(".googleMapGroup").find("p.sl_wrongsearch_errormsg").show();
		}
		$this.parents(".googleMapGroup").find("p.sl_errormsg").text(result.error[0].errorMessage);
		$this.parents(".googleMapGroup").find("p.sl_errormsg").show();
		$this.parents(".googleMapGroup").find("#storeLocatorAddressEntry").addClass("sl_input_error_msg");
		$this.parents(".googleMapGroup").find("#change-location-cross").hide();
		$this.parents(".googleMapGroup").find(".no_specific_chain_stores").hide();

		$("#stores_count_display").hide();
		$(".googleMapGroup").find("#store-locator-results-tabs").hide();
		$("#store-locator-results-scrollable").hide();

		var mapOptions = {
			zoom: 4,
			center: new google.maps.LatLng(us_lat, us_long),
			zoomControl: false,
			zoomControlOptions: {
				position: google.maps.ControlPosition.RIGHT_BOTTOM,
				style: google.maps.ZoomControlStyle.SMALL
			},
			panControl: false,
			streetViewControl: false,
			mapTypeControl: false,
			scrollwheel: false,
			draggable: false
		};

		map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);

	} else {
		if (activeTab != "") {
			if (activeTab == 'showAll')
				populateStoreLocatorLanding(allStore, null);
			else if (activeTab == 'showTRU')
				populateStoreLocatorLanding(truStore, null);
			else if (activeTab == 'showBRU')
				populateStoreLocatorLanding(bruStore, null);
		} else {
			populateStoreLocatorLanding(result.stores, $this);
		}
	}
}

function doStoreSearch(result, curObject) {
	lms_all_page_count = 1;
	lms_tru_page_count = 1;
	lms_bru_page_count = 1;

	var $this = $(curObject);

	if (result.errors) {
		$this.parents(".sl_parent").find("p.sl_errormsg").text(result.errors[0].errorMessage);
		$this.parents(".sl_parent").find("p.sl_errormsg").show();
		$this.parents(".sl_parent").find("#storeLocatorAddressEntry").addClass("sl_input_error_msg");
		$this.parents(".sl_parent").find(".storeLocatorField").addClass("sl_input_error_msg");
		$this.parents(".sl_parent").find(".change-location-dismiss").hide();
		$(".sl_store_locator_results").hide();
		$(".storePopulationDetails").hide();

	} else if (result.error) {
		$this.parents(".sl_parent").find("p.sl_errormsg").text(result.error[0].errorMessage);
		$this.parents(".sl_parent").find("p.sl_errormsg").show();
		$this.parents(".sl_parent").find("#storeLocatorAddressEntry").addClass("sl_input_error_msg");
		$this.parents(".sl_parent").find(".storeLocatorField").addClass("sl_input_error_msg");
		$this.parents(".sl_parent").find(".change-location-dismiss").hide();
		$(".sl_store_locator_results").hide();
		$(".storePopulationDetails").hide();
	} else {
		$(".sl_store_locator_results").show();
		$(".storePopulationDetails").show();
		populateStoresFromJsonSL($this, result.stores);
	}
}

function setToolTip() {
	var learnMoreTooltip = $('.learn-more-tooltip');
	//learnMoreTooltipTemplate = $('.tooltip-container').html();

	learnMoreTooltip.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',

		trigger: 'hover'
	});

	learnMoreTooltip.click(function (event) {
		//		/event.preventDefault();
	});

}

function getStoreResults(search_term, distance, callBackFunction, curObject, skuId) {
	var tskuId = '', myStoreID = '', tquantity = '';
	if (skuId != undefined) {
		tskuId = '&skuId=' + skuId;
		tquantity = '&quantity=' + $(".find-in-store-overlay .product-side span.amount").text();
	}
	var favStoreId = getCookie("favStore");
	favStoreId = favStoreId.split("|");
	if (favStoreId.length > 1) {
		favStoreId = favStoreId[1];
		if (favStoreId.length > 0) {
			myStoreID = "&myStoreId=" + favStoreId + ""
		}
	}
	var url = contextPath + 'storelocator/getStoreResults.jsp?postalAddress="' + search_term + '"&distance=' + distance + tskuId + myStoreID + tquantity;

	$.ajax({
		type: 'post',
		dataType: 'text',
		async: false,
		url: url,
		success: function (data) {
			if (typeof utag != 'undefined' && typeof utag_data != "undefined") {
				utag_data.event_type = "";
			}
			findinstorejsonResponseRowData = "";
			findinstorejsonResponseRowData = $.trim(data);
			var result = (findinstorejsonResponseRowData != "") ? JSON.parse(findinstorejsonResponseRowData) : JSON.parse('{"stores":[]}');
			if (result.stores != undefined) {
				allStore = result.stores;
				/*if(callBackFunction == doPDPStoreSearch){
					allStore = truncateStoreNamePDP(allStore);
				}else{
					allStore = truncateStoreName(allStore);
				}*/
				allStore = addingTheNumberToJson(allStore);
				stores = allStore;
				truStore = allStore.filter(function (item) {
					return (item.chainCode === 'TRU' || item.isSuperStoreServiceAval === true)
				});

				bruStore = allStore.filter(function (item) {
					return (item.chainCode === 'BRU' || item.isSuperStoreServiceAval === true)
				});
				//Added for omniture integration 
				var storeCity = stores[0].city;
				var storeState = stores[0].stateAddress;
				var storeZipCode = stores[0].postalCode;
				if (isFromISPUStoreOverlay) {
					omniISPUInit();
					$("#searchTearm").val(search_term);
					$("#storeZipCode").val(storeZipCode);
					tealiumForStore(search_term, storeZipCode);
				} else {
					omniStoreLocator(search_term, storeCity, storeState, storeZipCode);
				}
				isFromISPUStoreOverlay = false;
				
				//Added for omniture integration 
			}
			callBackFunction(result, curObject);
		},
		error: function (e) {
			console.log(e);
		}
	});
}

function addingTheNumberToJson(stores) {
	for (i = 0; i < stores.length; i++) {
		stores[i].sequenceIndex = i;
		stores[i].distance = roundToOne(stores[i].distance);
	}
	return stores;
}
function roundToOne(num) {
	return +(Math.round(num + "e+1") + "e-1");
}
function truncateStoreName(stores) {
	for (i = 0; i < stores.length; i++) {
		stores[i].name = stores[i].name.split("[")[0];
	}
	return stores;
}
/*function truncateStoreNamePDP(stores){
	for(i=0; i < stores.length; i++){
		stores[i].name = stores[i].name.split("]")[0]+"]";
	}
	return stores;
}*/

/* PDP StoreLocator  */
var counter_stores = 0;
currModule = location.pathname.split('/').slice(-1)[0].toLowerCase();

$(document).on('click', '.find-in-store-overlay #store-locator-results-tabs li a', function (e) {

	var bru_length = $bru_ref.length;
	var tru_length = $tru_ref.length;
	var all_length = $allRef.length;

	var temp_counter = 0;
	var lms_counter;
	var loadmoreCounter;
	if ($(this).html() === '<!--Toys "R" Us  -->') {

		lms_counter = (lms_tru_page_count * lms_per_pagination < tru_length) ? (lms_tru_page_count * lms_per_pagination) : tru_length;
		loadmoreCounter = (parseInt(lms_counter / 10) - 1) * 10;
		$(".find-in-store-overlay").find(".bru-results").hide();
		$(".find-in-store-overlay").find(".tru-results").hide();
		if (tru_length > 0) {
			$(".find-in-store-overlay").find(".no_specific_chain_stores").hide();
			$(".find-in-store-overlay").find("#store-locator-results-scrollable").show();
			if ((!showallstores && tru_length > lms_per_pagination) && (lms_counter < tru_length) && (tru_length > 10)) {
				$tru_ref.each(function () {
					if (counter_stores < lms_counter) {
						counter_stores++;
						$(this).show();
						if (loadmoreCounter < counter_stores && lms_counter >= counter_stores) {
							loadMoreOmnitureCalls(true, $(this));
						}
					}
				});
				$(".find-in-store-overlay #show-more-stores").show();
			} else {
				/*$tru_ref.show();*/
				loadmoreCounter += 10;
				lms_counter = tru_length;
				$tru_ref.each(function () {
					if (counter_stores < lms_counter) {
						counter_stores++;
						$(this).show();
						if (loadmoreCounter < counter_stores && lms_counter >= counter_stores) {
							loadMoreOmnitureCalls(true, $(this));
						}
					}
				});
				$(".find-in-store-overlay #show-more-stores").hide();
			}
		} else {
			$(".find-in-store-overlay").find(".no_specific_chain_stores").text($("#tru_no_results_error").val()).show();
			$(".find-in-store-overlay").find("#store-locator-results-scrollable").hide();
		}
		$(".find-in-store-overlay").find(".nearby-stores-count").text(tru_length);

	} else if ($(this).html() === '<!-- Babies "R" Us -->') {

		lms_counter = (lms_bru_page_count * lms_per_pagination < bru_length) ? (lms_bru_page_count * lms_per_pagination) : bru_length;
		loadmoreCounter = (parseInt(lms_counter / 10) - 1) * 10;
		$(".find-in-store-overlay").find(".tru-results").hide();
		$(".find-in-store-overlay").find(".bru-results").hide();
		if (bru_length > 0) {
			$(".find-in-store-overlay").find(".no_specific_chain_stores").hide();
			$(".find-in-store-overlay").find("#store-locator-results-scrollable").show();
			if ((!showallstores && bru_length > lms_per_pagination) && (lms_counter < bru_length) && (bru_length > 10)) {
				$bru_ref.each(function () {
					if (counter_stores < lms_counter) {
						counter_stores++;
						$(this).show();
						if (loadmoreCounter < counter_stores && lms_counter >= counter_stores) {
							loadMoreOmnitureCalls(true, $(this));
						}
					}
				});
				$(".find-in-store-overlay #show-more-stores").show();
			} else {
				/*$bru_ref.show();*/
				lms_counter = bru_length;
				loadmoreCounter += 10;
				$bru_ref.each(function () {
					if (counter_stores < lms_counter) {
						counter_stores++;
						$(this).show();
						if (loadmoreCounter < counter_stores && lms_counter >= counter_stores) {
							loadMoreOmnitureCalls(true, $(this));
						}
					}
				});
				$(".find-in-store-overlay #show-more-stores").hide();
			}
		} else {
			$(".find-in-store-overlay").find(".no_specific_chain_stores").text($("#bru_no_results_error").val()).show();
			$(".find-in-store-overlay").find("#store-locator-results-scrollable").hide();
		}
		$(".find-in-store-overlay").find(".nearby-stores-count").text(bru_length);
	} else {

		lms_counter = (lms_all_page_count * lms_per_pagination < all_length) ? (lms_all_page_count * lms_per_pagination) : all_length;
		loadmoreCounter = (parseInt(lms_counter / 10) - 1) * 10;
		$(".find-in-store-overlay").find("#store-locator-results-scrollable").show();
		$(".find-in-store-overlay").find(".tru-results").hide();
		$(".find-in-store-overlay").find(".bru-results").hide();
		$(".find-in-store-overlay").find(".no_specific_chain_stores").hide();
		//var $ulRef = $(".find-in-store-overlay").find(".bru-results").parent();
		if ((!showallstores && all_length > lms_per_pagination) && (lms_counter < all_length) && (all_length > 10)) {
			$allRef.each(function () {
				if (counter_stores < lms_counter) {
					counter_stores++;
					$(this).show();
					if (loadmoreCounter < counter_stores && lms_counter >= counter_stores) {
						loadMoreOmnitureCalls(true, $(this));
					}
				}
			});
			$(".find-in-store-overlay #show-more-stores").show();
		} else {
			//$allRef.find("li").show();
			//$allRef.show();
			/*$allRef.show()*/
			loadmoreCounter += 10;
			lms_counter = all_length;
			$allRef.each(function () {
				if (counter_stores < lms_counter) {
					counter_stores++;
					$(this).show();
					if (loadmoreCounter < counter_stores && lms_counter >= counter_stores) {
						loadMoreOmnitureCalls(true, $(this));
					}
				}
			});
			$(".find-in-store-overlay #show-more-stores").hide();

		}
		$(".find-in-store-overlay").find(".nearby-stores-count").text(all_length);
	}
	$('.find-in-store-overlay .findinstore-pdp-results li hr.whiteColor').removeClass("whiteColor");
	$('.find-in-store-overlay .findinstore-pdp-results li:visible:last hr').addClass("whiteColor");
	counter_stores = 0;
});


$(document).on('click', '.find-in-store-overlay #show-more-stores', function () {
	///showallstores = true;

	var activeTab = $(".find-in-store-overlay #store-locator-results-tabs").find(".active a").html().trim();
	if (activeTab === '<!--All -->') {
		lms_all_page_count += 1;
	}
	else if (activeTab === '<!-- Babies "R" Us -->') {
		lms_bru_page_count += 1
	}
	else if (activeTab === '<!--Toys "R" Us  -->') {
		lms_tru_page_count += 1
	}
	
	$(".find-in-store-overlay #store-locator-results-tabs").find(".active a").trigger("click");
	var searchTerm = $("#searchTearm").val();
	var storeZipCode = $("#storeZipCode").val();
	tealiumForStore(searchTerm,storeZipCode,"showMore");
});

//Moving storelocator related functionality from producDetails.js
var getfavStore;
$(document).on('click', '#findInStorePDP', function (e) {
	isFromISPUStoreOverlay = true;
	findinstorecommon('YES');

});
function findinstorecommon(isBtnClick) {
	lms_all_page_count = 1;
	lms_tru_page_count = 1;
	lms_bru_page_count = 1;

	if (isBtnClick === 'YES') {
		if ($('#FindinStorePDPField').val().trim() != "") {
			var findinstorejsonResponse;
			var postaladdress;

			if (isBtnClick == 'YES')
				postaladdress = $('#FindinStorePDPField').val();
			else
				postaladdress = getfavStore;

			$(".find-in-store-overlay").find("p.sl_errormsg").hide();
			$(".find-in-store-overlay").find("p.sl_errormsg").parent().find("#FindinStorePDPField").removeClass("sl_input_error_msg");
			var skuId = $('#pdpSkuId').val();
			var lightBoxRadius = '100';
			if ($("#lightBoxRadius").length) {
				if ($("#lightBoxRadius").val() != "") {
					lightBoxRadius = $("#lightBoxRadius").val();
				}
			}
			getStoreResults(postaladdress, lightBoxRadius, doPDPStoreSearch, this, skuId);
		}
		else {
			$(".find-in-store-overlay").find("p.sl_errormsg").text(addressToGetStarted);
			$(".find-in-store-overlay").find("p.sl_errormsg").show();
			$(".find-in-store-overlay").find("p.sl_errormsg").parent().find("#FindinStorePDPField").addClass("sl_input_error_msg");
			$(".find-in-store-overlay").find("#FindinStorePDPField").focus();

		}
	}
	//Added for Omniture
	//Vaibhav; removing this because of the utag is not defined error. Will enable this after the utag issue is resolved by performance team.
	/*utag.link({
		event_type:'ispu_init'
	});*/
}
function updateLineItemISPUSG(relationShipId, locationId, pageName) {
	var relAndMetaId = relationShipId.split("_");
	var relId = relAndMetaId[0];
	var metaInfoId = relAndMetaId[1];
	var contextPath = $('.contextPath').val();
	$.ajax({
		type: "POST",
		url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?relationShipId="
		+ relId
		+ "&locationId="
		+ locationId
		+ "&metaInfoId="
		+ metaInfoId
		+ "&formID=updateLineItemSG",
		success: function (data) {

			if (data.indexOf('Following are the form errors:') > -1) {
				var msg = data.split('Following are the form errors:')[1];
				$('.errorDisplay').html(msg);
			} else {
				if (pageName == "shoppingCart") {
					$("#shoppingCart").html(data);
					$("html").removeClass("modal-open");
					$("body").css('padding-right', '');
					if ($("#load-cart-norton-script").length) {
						$("#load-cart-norton-script").html($("#loadNortonHiddenDiv").clone());
					}
					if ($(".sos-redirect-product-block").length) {
						location.reload();
					}
					bindCartEvents();
				} else if (pageName == "inStorePickUp") {
					continueToCheckout('pickup-tab');
					$("html").removeClass("modal-open");
				} else if (pageName == "Review") {
					continueToCheckout('review-tab');
					$("html").removeClass("modal-open");
				}

			}
			$('.closeFindInStoreModal').click();
		},
		dataType: "html",
		cache: false
	});
	return false;
}
function loadFindInStore(contextPath, selectedSkuId, relationshipItemId, relItemQtyInCart, ele, cartFlag) {
	var quantity;
	try {
		if ($(ele) && $(ele).attr("href") && $(ele).attr("href").indexOf("void") > 0) {
			$(ele).removeAttr("href");
		}
		setTimeout(function () {
			$(ele).attr("href", "javascript:void(0);");
		}, 1000);
	}
	catch (e) { }
	if (relationshipItemId == '')
		quantity = $('#ItemQty').val();
	else if (relationshipItemId == 'Collection_page') {
		quantity = relItemQtyInCart;
	} else {
		quantity = relItemQtyInCart;
	}
	var cartPageFlag = false;
	if (cartFlag != null && cartFlag != "undefined") {
		cartPageFlag = true;
	}

	/*if(relationshipItemId == 'Collection_page'){
		relationshipItemId = '';
	}*/
	
	$.ajax({
		type: 'POST',
		dataType: 'html',
		url: '/storelocator/findInStorePDPInfo.jsp?relationshipItemId=' + relationshipItemId + "&quantity=" + quantity + "&updateInStoreData=true",
		data: $("#inStorePickup").serialize() + '&selectedSkuId=' + selectedSkuId,
		async: false,
		success: function (resData) {
			$('#findInStoreModal').html(resData);
			$('#findInStoreModal').find("#find-in-store-results header, #find-in-store-results #store-locator-results-tabs, #find-in-store-results #store-locator-results-scrollable, #find-in-store-results .find-in-store-search img").hide();
			$('#findInStoreModal').find("#find-in-store-results .find-in-store-search").show();
			if (relationshipItemId == 'Collection_page') {
				$('#findInStoreModal').modal("show");
			}
			$('#quickviewModal').modal("hide");
			if (cartPageFlag) {
				$('#findInStoreModal').addClass("cartItemFlag");
			}
			getfavStore = getCookie("favStore");
			if (getfavStore != "") {
				findinstorecommon('NO');
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				window.event.cancelBubble = true;
				var redirectionUrl = xhr.getResponseHeader('Referer');
				$(document).find("[data-toggle=modal]").each(function () {
					$(this).attr("data-target", "");
				});

				//setTimeout(function(){
				var visitorName = $.cookie("VisitorFirstName");
				if (isEmpty(visitorName)) {
					location.reload();
				}
				else {
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'myaccount/myAccount.jsp?sessionExpired=true';
				}
				//},300);
			}
			return false;
		}

	});
	return true;
}
function findInStorePDP() {
	$('#findInStorePDP').click();
}
// END OF Moving storelocator related functionality from producDetails.js


function indexOfDefaultStore(allStore, favStoreId) {

	/*$.each(store.storeServices, function(key, val){
		servicesInfo += '<li id="layawayInfoBox">';
		servicesInfo += store.storeServices[key].serviceName;
		servicesInfo += '<p>' + store.storeServices[key].serviceDescription + '</p>';
		//servicesInfo += '<a href="javascript:void(0);" class="learn-more-tooltip" data-original-title="" data-content="'+store.storeServices[key].learnMore+'" title="">learn more</a>';
		servicesInfo += '<a href="javascript:void(0);" class="learn-more-tooltip" data-original-title="" data-content="'+store.storeServices[key].learnMore+'" title="">learn more</a>';
		servicesInfo += '</li>';
	});*/

	/*allStores.stores[]*/
	var indexOfFavStoreId = 0;
	for (store in allStore) {
		if (allStore[store].StoreID == favStoreId) {
			return indexOfFavStoreId;
		} else {
			indexOfFavStoreId++;
		}
	}
	return indexOfFavStoreId;
}
function assigningDefaultStoreToFirstPosition(allStore) {
	var favStoreId = getCookie("favStore");
	favStoreId = favStoreId.split("|");
	favStoreId = favStoreId[1];
	var indexOfFavStore = indexOfDefaultStore(allStore, favStoreId);
	var favStoreJson = allStore[indexOfFavStore];
	allStore.splice(indexOfFavStore, 1);
	allStore.unshift(favStoreJson);
	addingTheNumberToJson(allStore);
	return allStore;
}
function showConfirmationWindowForDefaultStore(productId, skuId, StoreID, contextpath, postalCode, ele, mmsCity, address, storeName, storeDistance, relationShipId) {
	$("#makemystoreModel").attr("data-productId", productId);
	$("#makemystoreModel").attr("data-skuId", skuId);
	$("#makemystoreModel").attr("data-StoreID", StoreID);
	$("#makemystoreModel").attr("data-contextpath", contextpath);
	$("#makemystoreModel").modal("show");
	$("#makemystoreModel").find("#makemystoreModel-yes").attr("data-address", address);
	$("#makemystoreModel").find(".makemystoreModel-address").html(storeName);
	var pageName = $(".pageName").val();
	var signature = getCookie('myStoreAddressSignature');
	//var storename = convertToHtmlEntities(storeName);
	var yesMakemystore = "javascript:makeMyStore(event,'" + contextPath + "','" + StoreID + "','" + postalCode + "', '" + mmsCity + "' ,this,'" + storeName + "','" + storeDistance + "','" + signature + "')";
	$("#makemystoreModel").find("#makemystoreModel-yes").attr("onclick", yesMakemystore);
	$("#makemystoreModel").one('hide.bs.modal', function () {
		if ($(".maturevalue").val() == "true") {
			setTimeout(function () {
				if ($("#mystoreSuccessModal").hasClass("in")) {
					$("#mystoreSuccessModal").one('hide.bs.modal', function () {
						$(".esrbratingModel").modal("show");
					});
					setTimeout(function () {
						$("#mystoreSuccessModal").modal("hide");
					}, 3000);
				} else {
					$(".esrbratingModel").modal("show");
				}
			}, 100);
			$(".esrbratingModel").addClass("fromConfirmDefaultFindInStore");
		} else {
			if (relationShipId != "" && relationShipId != 'undefined' && pageName != "collectionPage") {
				updateLineItemISPUSG(relationShipId, StoreID, "shoppingCart");
			} else {
				addToCartFromFindInStore();
			}
		}
	});
	omniStoreSelect(StoreID);
}
function addToCartFromFindInStore(fromDefaultStoreConfirmation) {
	var productId = $("#makemystoreModel").attr("data-productId");
	var skuId = $("#makemystoreModel").attr("data-skuId");
	var StoreID = parseInt($("#makemystoreModel").attr("data-StoreID"));
	var contextpath = $("#makemystoreModel").attr("data-contextpath");
	/*if(!fromDefaultStoreConfirmation){*/
	var response = addItemToCart(productId, skuId, StoreID, contextpath, '', 'fromStore');
	if (response) {
		if ($("#mystoreSuccessModal").hasClass("in")) {
			$('#mystoreSuccessModal').modal('hide');
		}
		$('#shoppingCartModal').modal('show');
	}
	/*}else{
		setTimeout(function(){
			var response = addItemToCart(productId,skuId,StoreID,contextpath,'','fromStore');
			if(response){
				$('#mystoreSuccessModal').modal('hide');
				$('#shoppingCartModal').modal('show');
			}
		},3000);
	}*/
}
function esrbOnClickAddToCart(_this) {
	var $this = $(_this);
	$("#makemystoreModel").attr("data-productId", $this.attr("data-productId"));
	$("#makemystoreModel").attr("data-skuId", $this.attr("data-skuId"));
	$("#makemystoreModel").attr("data-StoreID", $this.attr("data-StoreID"));
	$("#makemystoreModel").attr("data-contextpath", $this.attr("data-contextpath"));
	$('.esrbratingModel').addClass("fromFindInStore");
}
function convertToHtmlEntities(str) {
	return String(str).replace(/&/g, '&amp;').replace(/"/g, '&quot;');
}
function checkDefaultStore() {
	var address = getCookie("myStoreAddressCookie");
	var myStoreName = getCookie("myStoreName");
	var storeDistance = myStoreName.split("|")[1];
	myStoreName = myStoreName.split("|")[0];
	storeNameText = $('<textarea />').html(myStoreName).text();
	var favStore = getCookie("favStore");
	var favStoreId = favStore.split("|");
	favStoreId = favStoreId[1];
	if (address != "" && favStore != "") {
		$('.your-store-default').remove();
		var mystoreHtml;
		if ($('#checkout-container').length) {
			mystoreHtml = '<li class="find-store-top-border-filler your-store-locator-tooltip"><div class="nav-bar-top-text"><div class="find-store inline"></div><span><a href="javascript:void(0)"><span>' + yourStore + ': </span>&nbsp;<span class="myDefaultStore">' + address + '</a><span></span></div></li>';
			if ($(".sos-header-message").length) {
				mystoreHtml = '<li class="find-store-top-border-filler your-store-locator-tooltip"><div class="nav-bar-top-text aosSite"><div class="find-store inline"></div><span><a href="javascript:void(0)"><span>' + yourStore + ': </span>&nbsp; <span class="myDefaultStore">' + address + '</span></a></span></div></li>';
			}
			if ($('#checkout-container').length) {
				$('.nav.navbar-nav.forDefaultStore').html(mystoreHtml);
			} else {
				$(".nav.navbar-nav .continue-shopping").after(mystoreHtml);
			}
		} else {
			mystoreHtml = '<li class="your-store-bullet"><span>&#xb7;</span></li><li class="your-store your-store-locator-tooltip my-store-container"  data-original-title="" title=""><span><a href="javascript:void(0)"><span class="myDefaultStore">' + myStore + ':</span><span class="myDefaultStoreName">&nbsp' + storeNameText + '</span></a></span></li>';
			$('.store-locator-tooltip').after(mystoreHtml);
		}
		enablePopoverMyStore(storeDistance);
	}
}
/*function getFavStoreCookie(){
	var favStore = getCookie("favStore");
	var favStoreId = favStore.split("|");
	favStoreId = favStoreId[1];
	return favStoreId;
}*/
function loadMoreOmnitureCalls(fromPDP, $this) {
	if (fromPDP) {
		if ($this.find(".outOfStockStatus").val() == "true") {
			//console.log($this.find("h4").text() + " out of stock");
			omniIspuStoreServed($this.find(".storeIdForPDP").val());
			omniIspuOutOfStock($this.find(".storeIdForPDP").val());
		} else {
			//console.log($this.find("h4").text() + " in stock");
			omniIspuStoreServed($this.find(".storeIdForPDP").val());
		}
	} else {
		//console.log($this.find("h2").text());
		omniIspuStoreServed($this.find(".storeIdOmni").val());
	}
}
function omniISPUInit() {
	if (typeof utag != 'undefined') {
		utag.link({
			event_type: 'ispu_init',
			selected_store: ""
		});
	}
}
//Added for omniture
function omniStoreSelect(storeId) {
	if (typeof (utag) != 'undefined') {
		utag_data.event_type = "ispu_store_select";
		utag.link({
			event_type: 'ispu_store_select',
			selected_store: storeId
		});
	}
}
