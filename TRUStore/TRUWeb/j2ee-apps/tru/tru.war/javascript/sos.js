function isValidNumber(number) {
	var matchArray = number.match(/^[0-9]+$/)
	if (matchArray == null) {
		return false;
	}
	return true;
}


function valLogin() {
	var f = document.returningCustomer;
	if ((f.associate_name.value.length < 34 && f.associate_name.value.length != 0)
		&& (isValidNumber(f.store_number.value) && f.store_number.value.length == 4)
		&& (isValidNumber(f.associate_number.value) && f.associate_number.value.length == 7)) {
		f.submit();
	}
	else {
		alert("Please check the values entered");
	}

}