var defaultMarker = true;
var defaultStore = true;
var marker = '';
var startEnd = [];
var map = '';
var icons = '';
var directionsDisplay = '';
var startMarker = '';
var endMarker = '';
var searchIconClicked = false;

var getParameterByName = function (name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};

var initMap = function () {
	directionsDisplay = new google.maps.DirectionsRenderer({ suppressMarkers: true });
	var directionsService = new google.maps.DirectionsService;
	var latValue = parseFloat(getParameterByName('lat'));
	var lngValue = parseFloat(getParameterByName('lng'));
	var latLngValue = { lat: latValue, lng: lngValue };

	$('#pac-input-destination-lat').val(latValue);
	$('#pac-input-destination-lng').val(lngValue);

	icons = {
		start: new google.maps.MarkerImage('/images/a-pin-point-new.png'),
		end: new google.maps.MarkerImage('/images/b-pin-point-new.png')
	};

	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 7,
		center: latLngValue,
		streetViewControl: true,
		streetViewControlOptions: {
			position: google.maps.ControlPosition.RIGHT_TOP
		},
		zoomControl: true,
		zoomControlOptions: {
			position: google.maps.ControlPosition.RIGHT_TOP
		}
	});

	makeMarkerEnd(latLngValue, icons.end, map);

	setDirectionDisplayToMap(directionsDisplay, map);
	directionsDisplay.setPanel(document.getElementById('direction-details-panel'));

	var startInput = document.getElementById('pac-input-start');
	var destinationInput = document.getElementById('pac-input-destination');
	var directionInputs = document.getElementById('direction-inputs');
	directionInputs.style.display = 'block';

	var autocompleteStart = new google.maps.places.Autocomplete(startInput);
	var autocompleteDest = new google.maps.places.Autocomplete(destinationInput);

	startInput.addEventListener('change', function () {
		clearFormattedAddressInputByID(['pac-input-start-formatted', 'pac-input-start-lat', 'pac-input-start-lng']);
	});
	destinationInput.addEventListener('change', function () {
		clearFormattedAddressInputByID(['pac-input-destination-formatted', 'pac-input-destination-lat', 'pac-input-destination-lng']);
	});
	autocompleteStart.addListener('place_changed', function () {
		var placeStart = autocompleteStart.getPlace();
		document.getElementById('pac-input-start-formatted').value = placeStart.formatted_address;
		setStartLatLng(placeStart);
	});

	autocompleteDest.addListener('place_changed', function () {
		var placeDest = autocompleteDest.getPlace();
		document.getElementById('pac-input-destination-formatted').value = placeDest.formatted_address;
		setDestinationLatLng(placeDest);
	});

	$(document).on('click', '.get-direction-btn', function () {
		directionPanVisiblity('hidden');
		hideErrorMessage();
		clearStartEndMarker();
		clearCalculatedRoute(directionsDisplay);
		calculateAndDisplayRoute(directionsService, directionsDisplay, map);
	});

	$(document).on('click', '.toggle-direction', function () {
		directionPanVisiblity('hidden');
		toggleStartEndValues();
		calculateAndDisplayRoute(directionsService, directionsDisplay, map);
	});

	document.getElementById('pac-input-destination').value = getParameterByName('desti');
	document.getElementById('pac-input-destination-formatted').value = getParameterByName('desti');
	var headingdesc = document.getElementById('direction-desc');
	headingdesc.innerHTML = getParameterByName('storename');


};



var makeMarkerStart = function (position, icon, map) {
	startMarker = initMarker(position, icon, map);
};

var makeMarkerEnd = function (position, icon, map) {
	endMarker = initMarker(position, icon, map);
};

var initMarker = function (position, icon, map) {
	return new google.maps.Marker({
		position: position,
		map: map,
		icon: icon,
	});
};

var clearStartEndMarker = function () {
	(startMarker) ? startMarker.setMap(null) : '';
	(endMarker) ? endMarker.setMap(null) : '';
};

var calculateAndDisplayRoute = function (directionsService, directionsDisplay, map) {

	var start = document.getElementById('pac-input-start-formatted').value;
	var end = document.getElementById('pac-input-destination-formatted').value;

	if (start == '') {
		start = $('#pac-input-start').val();
	}

	if (end == '') {
		end = $('#pac-input-destination').val();
	}

	directionsService.route({
		origin: start,
		destination: end,
		travelMode: 'DRIVING'
	}, function (response, status) {
		if (status === 'OK') {
			clearStartEndMarker();
			directionsDisplay.setDirections(response);

			var leg = response.routes[0].legs[0];
			makeMarkerStart(leg.start_location, icons.start, map);
			makeMarkerEnd(leg.end_location, icons.end, map);

			setDirectionDisplayToMap(directionsDisplay, map);
			directionPanVisiblity('visible');
		} else {
			directionPanVisiblity('hidden');
			showErrorMessage();
			clearStartEndMarker();
			clearCalculatedRoute(directionsDisplay);
			moveToStartOrEnd();
		}

	});
	setTimeout(function () {
		changeVerbiageIssues();
	}, 900);

};

var directionPanVisiblity = function (mode) {
	$('#direction-details-panel').css('visibility', mode);
}
var changeVerbiageIssues = function () {

	var verbiageIssueSection = $('#direction-details-panel .adp-summary [jstcache="50"]');

	var tempText = verbiageIssueSection.text();
	tempText = tempText.replace("Hours", "h").replace("hours", "h").replace("Hour", "h").replace("hour", "h");
	tempText = tempText.replace("Mins", "min").replace("mins", "min");

	verbiageIssueSection.text(tempText);

};

var moveToStartOrEnd = function () {
	var startLat = document.getElementById('pac-input-start-lat').value;
	var startLng = document.getElementById('pac-input-start-lng').value;

	var destinationLat = document.getElementById('pac-input-destination-lat').value;
	var destinationLng = document.getElementById('pac-input-destination-lng').value;

	var latLng = '';
	var ABMarker = '';

	if (startLat != '' && startLng != '') {
		latLng = { lat: parseFloat(startLat), lng: parseFloat(startLng) };
		ABMarker = 'A';
	} else if (destinationLat != '' && destinationLng != '') {
		latLng = { lat: parseFloat(destinationLat), lng: parseFloat(destinationLng) };
		ABMarker = 'B';
	}

	if (latLng != '') {
		map = new google.maps.Map(document.getElementById('map'), {
			zoom: 7,
			center: latLng
		});

		if (ABMarker == 'A') {
			makeMarkerStart(latLng, icons.start, map);
		} else if (ABMarker == 'B') {
			makeMarkerEnd(latLng, icons.end, map);
		}
	}

};

var showErrorMessage = function () {
	var invalidInput = '';
	var start = $('#pac-input-start-formatted').val();
	var end = $('#pac-input-destination-formatted').val();

	if (start == '') {
		invalidInput = $('#pac-input-start').val() || '';
	}
	if (end == '') {
		invalidInput = $('#pac-input-destination').val() || '';
	}

	$('.directions-not-found-message .not-found-text').text(invalidInput);
	$('.directions-not-found-message').show();

	if (invalidInput == '') {
		$('.not-found-message-empty').show();
		$('.not-found-title').hide();
	} else {
		$('.not-found-message-empty').hide();
		$('.not-found-title').show();
	}
};

var hideErrorMessage = function () {
	$('.directions-not-found-message, .directions-not-found-message-empty').hide();
};

var clearCalculatedRoute = function (directionsDisplay) {
	directionsDisplay.setMap(null);
	directionsDisplay.setPanel(null);
};

var setDirectionDisplayToMap = function (directionsDisplay, map) {
	directionsDisplay.setMap(map);
	directionsDisplay.setPanel(document.getElementById('direction-details-panel'));
};

var setStartLatLng = function (address) {
	document.getElementById('pac-input-start-lat').value = address.geometry.location.lat();
	document.getElementById('pac-input-start-lng').value = address.geometry.location.lng();
};

var setDestinationLatLng = function (address) {
	document.getElementById('pac-input-destination-lat').value = address.geometry.location.lat();
	document.getElementById('pac-input-destination-lng').value = address.geometry.location.lng();
};

var clearDefaultStoreName = function () {
	if (defaultStore) {
		document.getElementById('direction-to').style.visibility = 'hidden';
		defaultStore = false;
	}
};

var clearFormattedAddressInputByID = function (elementIDs) {
	for (i = 0; i < elementIDs.length; i++) {
		document.getElementById(elementIDs[i]).value = '';
	}
};


var toggleStartEndValues = function () {
	toggleValues($('#pac-input-start'), $('#pac-input-destination'));
	toggleValues($('#pac-input-start-lat'), $('#pac-input-destination-lat'));
	toggleValues($('#pac-input-start-lng'), $('#pac-input-destination-lng'));
	toggleValues($('#pac-input-start-formatted'), $('#pac-input-destination-formatted'));
	toggleDisabled($('#pac-input-start'));
	toggleDisabled($('#pac-input-destination'));
	$('.start-label input').removeClass("getDirection-error");
	$('.end-label input').removeClass("getDirection-error");
};

var toggleValues = function (startElm, endElm) {
	var tempValue = startElm.val();
	startElm.val(endElm.val());
	endElm.val(tempValue);
};

var toggleDisabled = function (elem) {
	if (elem.is(':disabled')) {
		elem.removeAttr('disabled');
	} else {
		elem.attr('disabled', 'disabled');
	}
};

$(document).on('blur keyup', '.start-label input[type="text"]', function () {
	if ($(this).val().length > 0) {
		$(this).removeClass("getDirection-error");
		$(this).addClass("getDirection-no-error");
	}
	else {
		$(this).removeClass("getDirection-no-error");
		$(this).addClass("getDirection-error");
	}
});
$(document).on('blur keyup', '.end-label input[type="text"]', function () {
	if ($(this).val().length > 0) {
		$(this).removeClass("getDirection-error");
		$(this).addClass("getDirection-no-error");
	}
	else {
		$(this).removeClass("getDirection-no-error");
		$(this).addClass("getDirection-error");
	}
});
