var contextPath;
jQuery(document).ready(function () {
	contextPath = $(".contextPath").val();
	initShoppingCartPage();
	/*shoppingCartTolltip();*/
	$(document).on('mouseover', '#order-summary-block .paypal-checkout.disabled-paypal', function () {
		$('#order-summary-block .paypal-tooltip').show();
	});

	$(document).on('mouseout', '#order-summary-block .paypal-checkout.disabled-paypal', function () {
		$('#order-summary-block .paypal-tooltip').hide();
	});

	/* Start: TUW-41882 */
	$(document).on('click', '.my-acount-popover-close', function () {
		$("#my-account-popover-struct").hide();
	});
	/* End: TUW-41882 */

	if ($('.sales-tax').text() == "_")
		$('.shopping-cart-summary-block .summary-block-border .summary-block-padded-content span.sales-tax').addClass('sales-tax-hyphon')
	else
		$('.shopping-cart-summary-block .summary-block-border .summary-block-padded-content span.sales-tax').removeClass('sales-tax-hyphon');


	$(document).on('change', '.lineItemQty', function () {
		var thisStepper = $(this).parent();
		var qty = $(this).val();
		var thisOtherMetaInfoQty = thisStepper.attr('otherMetaInfoQty');
		var lineItemQtyInCart = thisStepper.attr('lineItemQtyInCart');
		if (isEmpty(qty) && (parseInt(qty) - parseInt(lineItemQtyInCart) == 0)) {
			return false;
		}
		var fromOverlay;
		if (thisStepper.hasClass('overlay')) {
			fromOverlay = 'addToCartOverlay';
		}
		updateCartItemQuantity(thisStepper.attr('relationshipItemId'), parseInt(qty) + parseInt(thisOtherMetaInfoQty), fromOverlay, thisStepper.attr('giftWrapSkuId'));

	});
	$(document).on('blur', '.sticky-quantity  #ItemQty', function () {
		var maxvalue = $("#maxItemQty").attr("value");;
		var qty = $(this).val();
		var value = parseInt($(this).val());
		if (value != maxvalue) {
			$("#qtys-text p").remove();
		}
		var thisQtyLimit = $("#quantityLimitVal").attr("value");
		maxvalue = parseInt(thisQtyLimit);
		if (value >= maxvalue) {
			// value=maxvalue;
			$(".increase").addClass('disabled');
		} else {
			$(".increase").removeClass('disabled').addClass('enabled');
		}
		if (isNaN(value)) {
			value = 1;
		}
		$(this).val(value);
	});
	
	$(document).on('click', '.shipping-radio-item', function() {
		var $radioInput = $(this).parent().find('input[type=radio]'); 
		var radioBtName = $radioInput.attr('name');
		var radioBtVal = $radioInput.attr('value');
		var locationIdfromCookie = false;
		$('input[type=radio][name='+radioBtName+']').parent().find('div.checkout-flow-sprite').removeClass('checkout-flow-radio-selected');
		$radioInput.parent().find('div.checkout-flow-sprite').toggleClass('checkout-flow-radio-selected');
		if (radioBtVal === 'free-store-pickup') {
			var favStoreCookie = 'favStore' + "=";
			var favStoreCookieVal = '';
			var locationId = '';
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var c = cookies[i];
				while (c.charAt(0) == ' ') c = c.substring(1);
				if (c.indexOf(favStoreCookie) == 0) {
					favStoreCookieVal = c.substring(favStoreCookie.length, c.length);
					break;
				}
			}
			if (!isEmpty(favStoreCookieVal)) {
				favStoreCookieVal = favStoreCookieVal.split('|');
				locationId = favStoreCookieVal[1];
				locationIdfromCookie = true;
			}
			if(!isEmpty(locationId)) {
				updateLineItemSG($radioInput.attr("sg_rel_id"), locationId,locationIdfromCookie,$radioInput);
			} else {
				$('#padded-details_S2H_'+$radioInput.attr("sg_rel_index_id")).css("visibility","hidden");
				$('#padded-details_ISPU_'+$radioInput.attr("sg_rel_index_id")).show();
			}
		} else if (radioBtVal === 'ship-to-home') {
			if($('#selectedRelationShipId_'+$radioInput.attr("sg_rel_index_id")).val() == $radioInput.attr("sg_rel_index_id")
					&& !isEmpty($('#locationId_'+$radioInput.attr("sg_rel_index_id")).val())) {
				updateLineItemSG($radioInput.attr("sg_rel_id"), '', locationIdfromCookie);
			} else {
				$('#padded-details_S2H_'+$radioInput.attr("sg_rel_index_id")).css("visibility","visible");;
				$('#padded-details_ISPU_'+$radioInput.attr("sg_rel_index_id")).hide();
				$('.shopping-cart-error-state').hide();
				$('.product-information').removeClass("error");
			}
		}
		shoppingCartTolltip();
	});

	var timer, value;
	/*$(document).on('keypress', '.lineItemQty,.lineItemQtyEditCart', function(e) {
		if ( ($(this).val().length - (this.selectionEnd - this.selectionStart)) == 0 && e.which == 48 ){
			return false;
		}
	});*/
	$(document).on('keyup', '.lineItemQty', function (e) {
		clearTimeout(timer);
		var tabKey = e.which || e.keyCode;
		if (tabKey == 9) {
			return;
		}
		var thisStepper = $(this).parent();
		var lineItemQtyInCart = thisStepper.attr('lineItemQtyInCart');

		var thisOtherMetaInfoQty = thisStepper.attr('otherMetaInfoQty');
		var str = $(this).val();
		var qty = $(this).val();

		if (isEmpty($(this).val()) && (parseInt($(this).val()) - parseInt(lineItemQtyInCart) == 0)) {
			return false;
		}
		/*var thisStepper = '';
		var qty = '';*/
		var fromOverlay = '';

		if (str.length > 0 && value != str) {
			timer = setTimeout(function () {
				/*thisStepper = $(this).parent();
				qty = $(this).val();*/
				if (thisStepper.hasClass('overlay')) {
					fromOverlay = 'addToCartOverlay';
				}
				updateCartItemQuantity(thisStepper.attr('relationshipItemId'), parseInt(qty) + parseInt(thisOtherMetaInfoQty), fromOverlay, thisStepper.attr('giftWrapSkuId'));
			}, 2000);
		}

	});

	$(document).on('click', '.product-description-image, .displayName', function (e) {
		$(this).parents(".product-information").find('.edit-product-button.closed').trigger('click');
		return false;
	});

	//var customersAlsoViewed = setInterval(function(){
	if ($("#cart_rr").length) {
		if ($("#cart_rr").html().trim() != "") {
			setTimeout(function () {
				bruCss();
			}, 2000);
			//clearInterval(customersAlsoViewed);
		}
	}
	//},500);
});
$(window).load(function () {
	//setTimeout(function(){
	$(document).on('click', 'button.sign-in', function () {
		$('.errorDisplay').remove();
		$("#hearderLoginOverlay #login, #hearderLoginOverlay #sign-in-password-input").val("");
		$('#signInModal').modal('show');
	});

	$(document).on('click', '#my-account-popover-cart', function () {
		var contextPath = $(".contextPath").val();
		var param = {
			url: contextPath + 'header/headerMyAccountPopover.jsp',
			data: {
				formSerialize: 'contextPath=' + contextPath,
				formID: 'myAccountPopover'
			}
		};
		var reqestData = (typeof (param.data.formSerialize) != 'undefined') ? (param.data.formSerialize + '&formID=' + param.data.formID) : ({ formID: param.data.formID });
		$.ajax({
			type: "POST",
			dataType: "html",
			async: true,
			cache: false,
			data: reqestData,
			url: param.url,
			success: function (data) {
				$('#my-account-popover-struct').html(data);
				$("#my-account-popover-struct").toggle();
				$("#rewardsHdrAddUpdate li").hide();

				if (!isEmpty($.cookie("VisitorFirstName"))) {
					$(".my-account-logged-in").show();
					$(".my-account-logged-out").hide();
				} else {
					$(".my-account-logged-out").show();
					$(".my-account-logged-in").hide();
				}

				bruCss();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					var redirectionUrl = xhr.getResponseHeader('Referer');
					if (redirectionUrl != '' && redirectionUrl != null && redirectionUrl != 'undefined' && !($('#VisitorFirstNamecookieDIV').html() == 'my account')) {
						if (redirectionUrl.indexOf('?') > -1) {
							redirectionUrl = redirectionUrl + '&sessionExpired=true';
						} else {
							redirectionUrl = redirectionUrl + '?sessionExpired=true';
						}
						location.href = redirectionUrl;
					} else {
						var contextPath = $(".contextPath").val();
						location.href = contextPath + 'myaccount/myAccount.jsp?sessionExpired=true';
					}
				}
			}
		});

	});
	//},2800);
});
$(document).on('click', '.cart-forgot-password', function (e) {
	$('#signInModal').modal('hide');
});

function hideAllCartLearnMorePopovers() {
	$(document).find(".learn-more-giftWrap").each(function () {
		$(this).popover('hide');
	});
	$(document).find(".see-terms").each(function () {
		$(this).popover('hide');
	});
}
$(document).on('click', function (e) {
	var element = $(e.target);
	if (!element.parents('.popover').length) {
		hideAllCartLearnMorePopovers();
	}
});

function shoppingCartTolltip() {
	// 'see terms' popover
    /*var seeTerms = $('.see-terms');
    var seeTermsTemplate = $('.ship-to-home-popover');
    $(document).on('click','.see-terms', function(evt){
        evt.preventDefault();
    });
    seeTerms.each(function(i,val){
    	$(val).popover({
            animation : 'true',
            html : 'true',
            content : 'asdfasdf',
            placement : 'bottom',
            template : seeTermsTemplate,
            trigger : 'click'
        });
    })*/

	var seeTerms = $('.see-terms');
	var seeTermsTemplate = $('.ship-to-home-popover');
	seeTerms.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		template: seeTermsTemplate,
		placement: 'bottom',
		trigger: 'manual'
	}).on('click', function (e) {
		if (!$(this).hasClass("clicked")) {
			hideAllCartLearnMorePopovers();
			var $this = $(this);
			setTimeout(function () {
				$this.popover('show');
			}, 200);
		} else {
			$(this).popover('hide');
		}
		e.stopPropagation();
	});
	seeTerms.on('show.bs.popover', function () {
		$(this).addClass('clicked');
	});

	seeTerms.on('hide.bs.popover', function () {
		$(this).removeClass('clicked');
	});
    /*var learnMore = $('.learn-more');
    var learnMoreTemplate = $('.learn-more-tooltip');
    $(document).on('click','.learn-more-tooltip', function(){
        event.preventDefault();
    });
    learnMore.popover({
        animation : 'true',
        html : 'true',
        content : 'asdfasdf',
        placement : 'bottom',
        template : learnMoreTemplate,
        trigger : 'focus'
    });*/

	var learnMore = $(".learn-more-giftWrap");
	var learnMoreTemplate = $('.learn-more-tooltip');
	learnMore.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		template: learnMoreTemplate,
		placement: 'bottom',
		trigger: 'manual'
	}).on('click', function (e) {
		if (!$(this).hasClass("clicked")) {
			hideAllCartLearnMorePopovers();
			var $this = $(this);
			setTimeout(function () {
				$this.popover('show');
			}, 200);
		} else {
			$(this).popover('hide');
		}
		e.stopPropagation();
	});


	learnMore.on('show.bs.popover', function () {
		$(this).addClass('clicked');
	});

	learnMore.on('hide.bs.popover', function () {
		$(this).removeClass('clicked');
	});

	var surcharge = $('.see-terms-surcharge');
	var surchargeTemplate = $('.surcharge-popover');
	$(document).on('click', '.see-terms-surcharge', function () {
		event.preventDefault();
	});
	surcharge.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: surchargeTemplate,
		trigger: 'focus'
	});

	var paypalTooltip = $(".paypalTooltip").html();
	var paypalInfo = $('.disabled-paypal');
	var detailInfoTemplate = '<div class="popover detailWrapper" role="tooltip"><div class="arrow"></div><p>' + paypalTooltip + '</p></div>';

	paypalInfo.hover(function (event) {
		event.preventDefault();
	});

	paypalInfo.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'left',
		template: detailInfoTemplate,
		trigger: 'hover'
	});

	/* promotion tooltip*/
	var cartSeetermTooltipMsg = $(".promotionDetailsSpan").html();
	var cartSeetermTooltip = $('.cart-seeterm-tooltip');

	var cartSeetermTooltipTemplate = '<div class="popover detailWrapper termOfUSeDetailsWrapper" role="tooltip"><div class="arrow"></div><div class="saved-address-header"></div><p>' + cartSeetermTooltipMsg + '</p></div>';

	cartSeetermTooltip.hover(function (event) {
		event.preventDefault();
	});

	cartSeetermTooltip.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'right',
		template: cartSeetermTooltipTemplate,
		trigger: 'hover'
	});
	/* promotion tooltip */

	/* promocode not applied tooltip*/
	var promcodeTermUseMsg = $(".promcodeTermUseMsg").html();
	var promcodeTermUseTooltip = $('.promcodeTermUse');

	var promcodeTermUseTemplate = '<div class="popover detailWrapper termOfUSeDetailsWrapper" role="tooltip"><div class="arrow"></div><div class="saved-address-header"></div><p>' + promcodeTermUseMsg + '</p></div>';

	promcodeTermUseTooltip.hover(function (event) {
		event.preventDefault();
	});

	promcodeTermUseTooltip.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: promcodeTermUseTemplate,
		trigger: 'hover'
	});
	/* promocode not applied tooltip  promcodeTermUse*/

	/* promocode tooltip*/
	var cartPromoSeetermTooltipMsg = $(".promotionDetailsSpan").html();
	var promocodeSeeTermTooltip = $('.cart-promo-seeterm-tooltip');

	var promocodeSeeTermTooltipTemplate = '<div class="popover detailWrapper termOfUSeDetailsWrapper" role="tooltip"><div class="arrow"></div><div class="saved-address-header"></div><p>' + cartPromoSeetermTooltipMsg + '</p></div>';

	promocodeSeeTermTooltip.hover(function (event) {
		event.preventDefault();
	});

	promocodeSeeTermTooltip.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: promocodeSeeTermTooltipTemplate,
		trigger: 'hover'
	});
	/* promotion tooltip */

	/* promocode not applied tooltip*/
	var NotAppliedTooltip = $('.NotAppliedTooltip');

	var NotAppliedTooltipTemplate = '<div class="popover detailWrapper NotAppliedTooltipText" role="tooltip"><div class="arrow"></div><div class="saved-address-header"></div><div class="saved-address-header">Promotional Status Applied </div><p>Indicates that you have successfully entered a promotional code.</p><div class="saved-address-header">Not Applied</div><ul><li>You have typed the code incorrectly or used the wrong case. Codes are CaSe SeNsItIvE.</li><li>You have not added all of the required items to your cart (including any gift with purchase items).</li><li>Many codes are one-time use. You have already used this code on a previous order.</li><li>You have not added all of the required items to your cart (including any gift with purchase items).</li><li>Your order has not met all of the promotional requirements. (Note: Not all items are eligible for all promotions.)</li><li>The code entered is not within the valid effective dates</li><li>The promotional code entered requires a specific payment type. If so, please re-enter your payment information and any discounts will be applied after you click the Continue Checkout button.</li></ul></div>';

	NotAppliedTooltip.hover(function (event) {
		event.preventDefault();
	});

	NotAppliedTooltip.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: NotAppliedTooltipTemplate,
		trigger: 'hover'
	});
	/* promocode not applied tooltip  */

	/* promocode not applied tooltip*/
	var NotAppliedTooltip = $('.NotAppliedTooltip');

	var NotAppliedTooltipTemplate = '<div class="popover detailWrapper NotAppliedTooltipText" role="tooltip"><div class="arrow"></div><div class="saved-address-header"></div><div class="saved-address-header">Promotional Status Applied </div><p>Indicates that you have successfully entered a promotional code.</p><div class="saved-address-header">Not Applied</div><ul><li>You have typed the code incorrectly or used the wrong case. Codes are CaSe SeNsItIvE.</li><li>You have not added all of the required items to your cart (including any gift with purchase items).</li><li>Many codes are one-time use. You have already used this code on a previous order.</li><li>You have not added all of the required items to your cart (including any gift with purchase items).</li><li>Your order has not met all of the promotional requirements. (Note: Not all items are eligible for all promotions.)</li><li>The code entered is not within the valid effective dates</li><li>The promotional code entered requires a specific payment type. If so, please re-enter your payment information and any discounts will be applied after you click the Continue Checkout button.</li></ul></div>';

	NotAppliedTooltip.hover(function (event) {
		event.preventDefault();
	});

	NotAppliedTooltip.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: NotAppliedTooltipTemplate,
		trigger: 'hover'
	});
	/* promocode not applied tooltip  */


}

function updateCartItemQuantity(lineItemRelId, quantity, fromOverlay, giftWrapSkuId) {
	var orerSummaryStyle = $(document).find("#order-summary-block").attr('style');
	blockUI();
	$.ajax({
		type: "POST",
		url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?" + lineItemRelId
		+ "=" + quantity + "&relationShipId=" + lineItemRelId + "&fromOverlay=" + fromOverlay + "&giftWrapSkuId=" + giftWrapSkuId + "&formID=updateCartLineItemQuantity",
		dataType: "html",
		cache: false,
		success: function (data) {
			if (fromOverlay === 'addToCartOverlay') {
				$("#shoppingCartModal .shopping-cart-overlay").html(data);
				$("#minicart").load(contextPath + "cart/mini_cart.jsp", function () {
					progressBar();
				});

				initCartScroll();
			} else {
				$("#shoppingCart").html(data);
				bindCartEvents();
				if (typeof orerSummaryStyle !== "undefined") {
					$(document).find("#order-summary-block").attr('style', orerSummaryStyle);
				}
			}
			if ($("#load-cart-norton-script").length) {
				$("#load-cart-norton-script").html($("#loadNortonHiddenDiv").html());
			}
			unBlockUI();
		},
		error: function() {
			unBlockUI();
		}
	});
	return false;
}

function loadShoppingCart(data, lineItemRelId) {
	$("#shoppingCart").load(contextPath + "cart/shoppingCartInclude.jsp", function () {
		bindCartEvents();
	});
}

function initCartScroll() {
	setTimeout(function () {

		var scrollWindow = $('.shopping-cart-overlay .tse-scrollable');
		scrollWindow.parent().height('525px').width('617px !important');
		scrollWindow.TrackpadScrollEmulator({ autoHide: false });
		scrollWindow.width(scrollWindow.parent().width());
		scrollWindow.height(scrollWindow.parent().height());
		scrollWindow.find('.tse-scroll-content').height(scrollWindow.height());
	}, 500)
}


function isEmpty(val) {
	return (val === undefined || val == null || val.length <= 0) ? true : false;
}

function updateLineItemSG(relationShipId, locationId, locationIdfromCookie, selectedSGElement) {
	var relAndMetaId = relationShipId.split("_");
	var relId = relAndMetaId[0];
	var metaInfoId = relAndMetaId[1];
	var orerSummaryStyle = $(document).find("#order-summary-block").attr('style');
	blockUI();
	$.ajax({
		type: "POST",
		dataType: "html",
		cache: false,		
		url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?relationShipId="
		+ relId
		+ "&locationId="
		+ locationId
		+ "&metaInfoId="
		+ metaInfoId
		+ "&formID=updateLineItemSG",
		success: function (data) {
			$("#shoppingCart").html(data);
			var tmpData = $("<div>" + data + "</div>");
			if (tmpData.find(".shopping-cart-error-state").length > 1 && locationIdfromCookie) {
				var name = selectedSGElement.attr('name');
				$('input[name=' + name + ']').attr('checked', false);
				$('input[name=' + name + ']:eq(1)').prop('checked', true);
				$('#padded-details_S2H_' + selectedSGElement.attr("sg_rel_index_id")).css("visibility", "hidden");
				$('#padded-details_ISPU_' + selectedSGElement.attr("sg_rel_index_id")).show();
			}
			if ($("#load-cart-norton-script").length) {
				$("#load-cart-norton-script").html($("#loadNortonHiddenDiv").html());
			}
			bindCartEvents();
			if (typeof orerSummaryStyle !== "undefined") {
				$(document).find("#order-summary-block").attr('style', orerSummaryStyle);
			}
			unBlockUI();
		}
	});
	return false;
}
function removeItemFromCart() {

	if ($('#removeDonationItemId').val() === '') {
		removeItem();
	} else {
		removeDonationItemFromCart();
	}
	return false;
}
function tealiumForRemoveCart() {
	var productMerchandisingCategory = $('#omniProdMerchCat').val();
	var omniProductID = $('#omniProductID').val();
	if (typeof utag != 'undefined') {
		utag.view({
			'event_type': 'cart_remove',
			'product_merchandising_category': [],
			'product_id': [omniProductID]
		});
	}
}

function removeItem() {
	var commerceItemIdAndQty = $('#removeItemId').val().split("_");
	var commerceItemId = commerceItemIdAndQty[0];
	var qty = commerceItemIdAndQty[1];
	var action = commerceItemIdAndQty[2];
	var giftWrapSkuId = commerceItemIdAndQty[3];
	$.ajax({
		type: "POST",
		url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?commerceItemId="
		+ commerceItemId + "&formID=removeLineItemFromCart&action=" + action + "&" + commerceItemId + "=" + qty + "&giftWrapSkuId=" + giftWrapSkuId + "&quantity=" + qty,
		dataType: "html",
		success: function (data) {
			var tmpData = $("<div>" + data + "</div>");
			if (tmpData.find("#shoppingCartBody").length > 0) {
				$('#removeModelClose').click();
				$("#shoppingCart").html(data);
				if ($(".sos-redirect-product-block").length) {
					location.reload();
				}
				setFirstNameCookie();
				checkDefaultStore();
				bindCartEvents();
				tealiumForRemoveCart();
				if ($('.sales-tax').text() == "_")
					$('.shopping-cart-summary-block .summary-block-border .summary-block-padded-content span.sales-tax').addClass('sales-tax-hyphon')
				else
					$('.shopping-cart-summary-block .summary-block-border .summary-block-padded-content span.sales-tax').removeClass('sales-tax-hyphon');

				if ($("#load-cart-norton-script").length) {
					$("#load-cart-norton-script").html($("#loadNortonHiddenDiv").html())
				}
			} else {
				location.reload();
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				window.event.cancelBubble = true;
				var visitorName = $.cookie("VisitorFirstName");
				if (isEmpty(visitorName)) {
					location.reload();
				}
				else {
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'myaccount/myAccount.jsp?sessionExpired=true';
				}
			}
			return false;
		}
	});
}

function addGiftWrapToItem(itemId) {
	if (itemId == '' || itemId != undefined) {
		var itemiddiv = '#' + itemId;
		$
			.ajax({
				type: "POST",
				cache: false,
				url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?formID=selectGiftWrapToItem&commerceItemId="
				+ itemId,
				success: function (data) {
					if (data.indexOf('Following are the form errors:') > -1) {
						$(itemiddiv)
							.prepend(
							'<span class="errorDisplay">'
							+ data
								.split('Following are the form errors:')[1]
							+ '</span>');
					} else {
						$(itemiddiv).toggleClass(
							'checkbox-sm-off checkbox-sm-on');
					}

				},
				dataType: "html"
			});

		return false;
	}
}

function isNumberKey(evt) {
	var targetElement = evt.currentTarget.id;
	$('#' + targetElement).bind("paste", function (e) {
		e.preventDefault();
	});
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	}

	return true;
}
$(document).on("paste", ".cart-products .donation-text", function (e) {
	e.preventDefault();
});
function validateDonationAmount(evt) {
	$('.donation-text').attr('maxlength', '7');
	var enteredAmount = $('.donation-text:last').val();
	var afterDot = enteredAmount.split('.')[1];
	var beforeDot = enteredAmount.split('.')[0];
	var charCode = (evt.which) ? evt.which : event.keyCode;

	var currentPosition = evt.currentTarget.selectionStart;
	var tmpDotIndex = enteredAmount.indexOf('.');

	if (charCode === 13) {
		$('.donate-button').click();
	}
	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	}
	if ((charCode === 46 && enteredAmount.indexOf('.') >= 0) || (typeof afterDot !== "undefined" && currentPosition > tmpDotIndex && afterDot.length >= 2)) {
		return false;
	}
	/*if(enteredAmount.indexOf('.') != -1 && enteredAmount.substring(enteredAmount.indexOf('.'), enteredAmount.indexOf('.').length).length > 2){
		return false;
	}*/
	return true;

}


$('.donation-text').keyup(function () {
	if ($(this).val().indexOf('.') != -1) {
		if ($(this).val().split(".")[1].length > 2) {
			if (isNaN(parseFloat(this.value))) return;
			this.value = parseFloat(this.value).toFixed(2);
		}
	}
	return this; //for chaining
});

/*$(document).on('blur', 'input.donation-text', function() {
	var donationTtext = $(this).val();
		if(donationTtext!=""){
			if( isNaN( parseFloat( donationTtext ) ) ) return;
			$(this).val(parseFloat($(this).val()).toFixed(2));
		}                                                 
 });*/

$(document).on('click', '.shopping-cart-overlay-header-continue-shopping', function () {
	var mydata = $('.sticky-price-contents .logo .limit-1').html();
	if ($('.sticky-price-contents .logo').length == 1) {
		if (mydata.indexOf('Mature: 17 years and u') >= 1) {
			/*$('.maturevalue').val('true');*/
		}
	}
});

/*Reverting changes for fixing TUW-56931 
 * $(document).on('click','#addToCartDivSpace .add-to-cart',function(){
	if($(this).closest('.maturevalue')){
		$('.add-to-cart').attr('data-target', '#shoppingCartModal');
	}
	$('#addToCartDivSpace .add-to-cart').removeClass('currentClick');
	$(this).addClass('currentClick');
});
*/
$(document).on('click', '.im-mature-btn', function () {
    /*if(){
           $('#addToCartDivSpace .add-to-cart').attr('data-target', '#shoppingCartModal');
    }*/
	/*$('.add-to-cart').attr('data-target', '#shoppingCartModal');*/
	//notCachedAddTocartButton("");
	if(!$(this).hasClass('fromPLP')){
		$('.esrbratingModel').modal('hide');
		$('.maturevalue').val('false');
		if ($('.esrbratingModel').hasClass("fromConfirmDefaultFindInStore")) {
			addToCartFromFindInStore();
			$('.esrbratingModel').removeClass("fromConfirmDefaultFindInStore");
		} else if ($('.esrbratingModel').hasClass("fromFindInStore")) {
			addToCartFromFindInStore();
			$('.esrbratingModel').removeClass("fromFindInStore");
		} else {
			$('.add-to-cart.esrbAddToCart').attr("data-target", "#shoppingCartModal");
			var mValId = $('.add-to-cart.esrbAddToCart').attr("data-matureVal-id");
			$(mValId).val("false");
			$('.add-to-cart.esrbAddToCart').trigger("click");
			$("#shoppingCartModal").modal("show");
			$(mValId).val('true'); /* Added this line for TUW-67610 */
		}	
	}
});



function addItemToCart(productId, skuId, locationId, contextPath, $this, fromPage) {
	var qty, locationCookieId = '', onlineInventoryStatus = '';
	if ($(".collection-template").length) {
		qty = $("#findInStoreModal").find(".amount").text() || $($this).parents(".sticky-price-contents").find("#ItemQty").val();
	} else if ($('.template-family, .sub-category-template, .search-template').length) {
		qty = 1;
	} else {
		qty = $("#ItemQty").val();
	}
	var productInfoJson;
	if (fromPage === 'fromCompare' || fromPage === 'fromPDPBatteries') {
		onlineInventoryStatus = $('#onlineInventoryStatus-' + skuId).val();
	} else {
		if ($('.productInfoJson-' + productId).val()) {
			productInfoJson = JSON.parse($('.productInfoJson-' + productId).val());
			var allActiveSkuVariants = productInfoJson.mActiveSKUsList;
			if (typeof allActiveSkuVariants != 'undefined') {
				for (var indx in allActiveSkuVariants) {
					var skuIdLocal = allActiveSkuVariants[indx].mId;
					if (skuIdLocal === skuId) {
						onlineInventoryStatus = allActiveSkuVariants[indx].mInventoryStatus;
						break;
					}
				}
			}
		}
	}
	if (fromPage === 'fromStore' || fromPage === 'fromPDPBatteries' || fromPage === 'productPage') {
		locationCookieId = locationId;
	}/*else{
		if(typeof onlineInventoryStatus !='undefined' && onlineInventoryStatus !='' && onlineInventoryStatus =='outOfStock'){
			var locationString = $.cookie('favStore');
			if(locationString){
				var values=locationString.split('|');
			}
			if(typeof values != 'undefined' && values.length >= 2){
				locationCookieId = values[1];
			}
		}
	}*/
	//var maturevalId='#isAvailableOnlyForMature-'+${productId};  
	var matureval = $('#isAvailableOnlyForMature-' + productId).val();
	$('.add-to-cart.esrbAddToCart').removeClass("esrbAddToCart");
	if (isNaN(qty) || qty < 1) {
		return false;
	} else {
		if (matureval == "true") {
			$($this).attr('data-target', '.esrbratingModel');
			$($this).attr("data-matureVal-id", "#isAvailableOnlyForMature-" + productId)
			$($this).addClass("esrbAddToCart");
		} else {
			$('.esrbratingModel').modal('hide');
			// $('#shoppingCartModal').modal('show');
			/*$('.add-to-cart').attr('data-target', '#shoppingCartModal');*/
			/*$('.add-to-cart').attr('data-toggle', 'modal');*/
			//changeAddTocartButtonFromOverlay("");
			//$(window).scrollTop(0);
			var lastPage = $(".shopping-cart-template").length;
			if (lastPage > 0) {
				if (fromPage != 'fromCompare') {
					//notCachedAddTocartButton("");
				}
				$.ajax({
					type: "POST",
					async: false,
					dataType: "html",
					cache: false,
					url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?formID=addItemFromCartPage&productId="
					+ productId
					+ "&skuId="
					+ skuId
					+ "&quantity="
					+ qty
					+ "&locationId="
					+ locationCookieId,
					success: function (data) {
						var tmpData = $("<div>" + data + "</div>");
						if (tmpData.find("#shoppingCartBody").length > 0) {
							$("#shoppingCart").html(data);
							bindCartEvents();
							var cartAddSource = $("#cartAddSource").val();
							if (cartAddSource == 'quickView') {
								$("#quickviewModal").modal("hide");
								loadOmniScriptQuickView(cartAddSource);//For omniture call(addToCart from QuickView)
							}
						} else {
							location.reload();
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == '409') {
							window.event.cancelBubble = true;
							var redirectionUrl = xhr.getResponseHeader('Referer');
							$(document).find("[data-toggle=modal]").each(function () {
								$(this).attr("data-target", "");
							});
							var visitorName = $.cookie("VisitorFirstName");
							if (isEmpty(visitorName)) {
								location.reload();
							}
							else {
								var contextPath = $(".contextPath").val();
								location.href = contextPath + 'myaccount/myAccount.jsp?sessionExpired=true';
							}
						}
						return false;
					}
				});
			} else {
				if (fromPage != 'fromCompare') {
					//notCachedAddTocartButton("");
				}
				$.ajax({
					type: "POST",
					async: false,
					url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?formID=addItemToCart&productId="
					+ productId
					+ "&skuId="
					+ skuId
					+ "&quantity="
					+ qty
					+ "&locationId="
					+ locationCookieId
					+ getRegistryWishlistCookiesAsParams(),
					success: function (data) {
						var tmpData = $("<div>" + data + "</div>");
						if (tmpData.find(".shopping-cart-overlay").length > 0) {
							$("#minicart").load(contextPath + "cart/mini_cart.jsp", function () {
								progressBar();
								bruCss();
							});
							initCartScroll();
							syncCookieByIframe();
							var cartAddSource = $("#cartAddSource").val();
							if (cartAddSource != undefined) {
								if (cartAddSource == 'Product Detail Page') {
									loadOmniScript(cartAddSource);//For omniture call(addToCart from PDP)
								}
								if (cartAddSource == 'quickView') {
									$("#quickviewModal").modal("hide");
									loadOmniScriptQuickView(cartAddSource);//For omniture call(addToCart from QuickView)
								}
							}
							$('#shoppingCartModal').html(data);
							/*if($(".collection-template").length && locationCookieId != ""){
								$('#shoppingCartModal').modal("show");
							}*/
						} else {
							location.reload();
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == '409') {
							window.event.cancelBubble = true;
							var redirectionUrl = xhr.getResponseHeader('Referer');
							$(document).find("[data-toggle=modal]").each(function () {
								$(this).attr("data-target", "");
							});
							var visitorName = $.cookie("VisitorFirstName");
							if (isEmpty(visitorName)) {
								location.reload();
							}
							else {
								var contextPath = $(".contextPath").val();
								location.href = contextPath + 'myaccount/myAccount.jsp?sessionExpired=true';
							}
						}
						return false;
					},
					dataType: "html",
					cache: false
				});
			}
			// $('#shoppingCartModal').modal('show');
			return true;
		}
	}
}



/*function addItemToCart(productId, skuId, locationId,contextPath, $this, fromPage) {
	var qty,locationCookieId = '',onlineInventoryStatus='';
	if($($this).length>0){
		qty = $($this).parent().find('#ItemQty').val();
	}else if($($this).parents(".collection-template").length){
		qty = $($this).parents("#findInStoreModal").find(".amount").text();
	}else{
		qty = $("#ItemQty").val();
	}
	var productInfoJson;
	if(fromPage === 'fromCompare'){
		onlineInventoryStatus= $('#onlineInventoryStatus-'+skuId).val();
	}else{
		productInfoJson = JSON.parse($('#productInfoJson-'+productId).val());
		var allActiveSkuVariants=productInfoJson.mActiveSKUsList;
		if(typeof allActiveSkuVariants != 'undefined'){
		for (var indx in allActiveSkuVariants) {
			var skuIdLocal=allActiveSkuVariants[indx].mId;
					if(skuIdLocal === skuId){
						onlineInventoryStatus=allActiveSkuVariants[indx].mInventoryStatus;
						break;
					}
			}
		}
	}
	if(fromPage === 'fromStore'){
		locationCookieId = locationId;
	}else{
	if(typeof onlineInventoryStatus !='undefined' && onlineInventoryStatus !='' && onlineInventoryStatus =='outOfStock'){
	var locationString = $.cookie('favStore');
	if(locationString){
		var values=locationString.split('|');
	}
	if(typeof values != 'undefined' && values.length >= 2){
		locationCookieId = values[1];
	}
	}
	}
	//var maturevalId='#isAvailableOnlyForMature-'+${productId};	
	var matureval = $('.maturevalue').val();	
	
	if (isNaN(qty) || qty < 1) {
		return false;
	} else {
		if (matureval == "true") {
			$('.add-to-cart').attr('data-target', '.esrbratingModel');
			// $('.esrbratingModel').modal('show');
			$('#shoppingCartModal').modal('hide');
		} else {
			$('.esrbratingModel').modal('hide');			
			// $('#shoppingCartModal').modal('show');
			$('.add-to-cart').attr('data-target', '#shoppingCartModal');
			$('.add-to-cart').attr('data-toggle', 'modal');
			$(window).scrollTop(0);
			$.ajax({
					type : "POST",
					async : false,
					url : contextPath +"cart/intermediate/ajaxIntermediateRequest.jsp?formID=addItemToCart&productId="
							+ productId
							+ "&skuId="
							+ skuId
							+ "&quantity="
							+ qty
							+ "&locationId="
							+ locationCookieId,
					success : function(data) {
						$('#shoppingCartModal').html(data);
						$("#minicart").load(contextPath+"cart/mini_cart.jsp",function(){
							progressBar();
						});
						initCartScroll();
						syncCookieByIframe();
						var cartAddSource= $("#cartAddSource").val();
						if(cartAddSource != undefined){
							if(cartAddSource == 'Product Detail Page'){
						       loadOmniScript(cartAddSource);//For omniture call(addToCart from PDP)
							}
							if(cartAddSource == 'quickView'){
								$("#quickviewModal").modal("hide");
								loadOmniScriptQuickView(cartAddSource);//For omniture call(addToCart from QuickView)
							}
						}
						
					},
					dataType : "html",
					cache : false
					});
			// $('#shoppingCartModal').modal('show');
			return true;
		}
	}
}*/

$(document).on('click', '.donate-button', function (e) {
	var productId = $('#donationProductId').val();
	var skuId = $('#donationSKUId').val();
	var donationAmount = $('.donation-text').val();
	if (isNaN(parseFloat(donationAmount)) || donationAmount < 1) {
		$('.donation-error-message').show();
	} else if (isEmpty(productId) || isEmpty(skuId)) {
		return false;
	} else {
		$('.donation-error-message').hide();
		$.ajax({
			type: "POST",
			async: false,
			url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?formID=addDonationItemToCart&productId="
			+ productId
			+ "&skuId="
			+ skuId
			+ "&donationAmount="
			+ donationAmount,
			success: function (data) {
				$("#shoppingCart").html(data);
				if ($("#load-cart-norton-script").length) {
					$("#load-cart-norton-script").html($("#loadNortonHiddenDiv").html());
				}
				bindCartEvents();
			},
			dataType: "html",
			cache: false
		});
		return true;
	}
});

function removeDonationItemFromCart() {
	var commerceItemId = $('#removeDonationItemId').val();
	$.ajax({
		type: "POST",
		async: false,
		url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?formID=removeDonationItemFromCart&commerceItemId="
		+ commerceItemId,
		success: function (data) {
			$('#removeModelClose').click();
			$("#shoppingCart").html(data);
			bindCartEvents();
			tealiumForRemoveCart();
			if ($("#load-cart-norton-script").length) {
				$("#load-cart-norton-script").html($("#loadNortonHiddenDiv").clone())
			}
		},
		dataType: "html",
		cache: false
	});
	return true;
}

function applyAbandonCartPromotion() {
	var promotionId = $('.abandonCartPromoCode').val();
	if (isEmpty(promotionId)) {
		window.location.href = contextPath + "cart/shoppingCart.jsp";
	} else {
		$.ajax({
			type: "POST",
			async: false,
			url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?formID=applyAbandonCartPromotion&promotionId="
			+ promotionId,
			success: function (data) {
				window.location.href = contextPath + "cart/shoppingCart.jsp";
			},
			dataType: "html",
			cache: false
		});
	}
}

function removeCoupon(couponId) {
	var couponCode = escape(couponId);
	blockUI();
	$.ajax({
		type: "POST",
		cache: false,
		url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?couponId="
		+ couponCode + "&formID=removecoupon",
		dataType: "html",
		data: $("#removecoupon").serialize(),
		success: function (data) {
			$("#shoppingCart").load(contextPath + "cart/shoppingCartInclude.jsp", function () {
				refreshShoppingCartPage();
				unBlockUI();
			});
		},
	});
};

function applyCoupon() {
	var couponId = escape($("#promoCode").val());
	var orderId = $("#orderId").val();
	if (couponId == '') {
		couponId = escape($("#promoCode_error").val());
	}
	var orerSummaryStyle = $("#order-summary-block").attr('style');
	$('.enter-promo').removeClass('has-error');
	blockUI();
	$.ajax({
		type: "POST",
		cache: false,
		url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?formID=applyCoupon&couponId="
		+ couponId + "&orderId=" + orderId,
		dataType: "html",
		data: $("#applyCoupon").serialize(),
		success: function (data) {
			$("#shoppingCart").load(contextPath + "cart/shoppingCartInclude.jsp?couponId=" + couponId, function() {
				var coupon_msg_with_errorcode = $($.parseHTML(data)).filter("#coupon_msg_with_errorcode").text();
				var coupon_msg = $($.parseHTML(data)).filter("#coupon_msg").text();
				var coupon_error_code = $.trim($($.parseHTML(data)).filter("#coupon_error_code").text());
				//Added for omniture
				couponId = unescape(couponId);
				if (coupon_error_code == 'Not Applied') {
					$('.enter-promo').addClass('has-error');
				} else {
					if (!isEmpty(coupon_msg)) {
						$('.enter-promo').addClass('has-error');
						omniInvalidCoupon();
					}
					else {
						//success
						omniPromoSubmit();
					}
				}
				refreshShoppingCartPage();
				unBlockUI();
			});
		}
	});
};

function showDetails(promoId) {
	var contextPath = $(".contextPath").val();
	var url = contextPath + 'pricing/promotionDetailsPopup.jsp?promoId=' + promoId;
	window.open(url, '_blank', 'height=300,width=500,scrollbars=yes,status=yes');
};

function editCartItem(productId, skuId, updateCommerItemIds, relationShipId, otherMetaInfoQty, contextPath) {
	var qty = $(".lineItemQtyEditCart").val();
	if (!isEmpty(qty)) {
		qty = parseInt(qty) + parseInt(otherMetaInfoQty);
	} else {
		$(".qtys-valid-text").show();
		return;
	}
	$.ajax({
		type: "POST",
		url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?formID=editCartItem&productId=" + productId +
		"&skuId=" + skuId + "&quantity=" + qty + "&updateCommerItemIds=" + updateCommerItemIds + "&relationShipId=" + relationShipId,
		dataType: "html",
		success: function (data) {
			$("#shoppingCart").html(data);
			$("#editCartModal").parents("html").removeClass("modal-open")
			$("#editCartModal").parents("body").removeAttr("style");
			$("#editCartModal").modal('hide');
			bindCartEvents();
		},
		cache: false
	});
	//Added for omniture
	omniCartUpdate();
}

function updateGiftItem(obj, shipGroupId, commerceItemRelationshipId, commerceItemId, quantity, giftWrapProductId, giftWrapSkuId) {
	$(obj).toggleClass('checkout-flow-radio-selected');
	var giftItem = $(obj).hasClass("checkout-flow-radio-selected");
	$.ajax({
		type: "POST",
		url: contextPath + "checkout/gifting/ajaxUpdateGiftItem.jsp",
		data: {
			shipGroupId: shipGroupId, commerceItemRelationshipId: commerceItemRelationshipId, commerceItemId: commerceItemId,
			giftWrapProductId: giftWrapProductId, quantity: quantity, giftWrapSkuId: giftWrapSkuId, giftItem: giftItem
		},
		dataType: "html",
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
}

function esrbmaturevefication(productId, skuId) {
	var qty = $("#ItemQty").val();

	//addItemToCart(productId,skuId);
	$('.esrbratingModel').modal('hide');

	$('.add-to-cart').attr('data-target', '#shoppingCartModal');
	$.ajax({
		type: "POST",
		async: false,
		url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?formID=addItemToCart&productId=" + productId + "&skuId=" + skuId + "&quantity=" + qty + getRegistryWishlistCookiesAsParams(),
		success: function (data) {
			$('#shoppingCartModal').html(data);
			$("#minicart").load(contextPath + "cart/mini_cart.jsp", function () {
				progressBar();
				initCartScroll();
			});
		},
		dataType: "html",
		cache: false
	});
}


function promocodeSlideToggle() {
	//PROMO CODE
	var scHeader = $('.shopping-cart-promo-code .promo-header');
	var scPromoBtn = $('.shopping-cart-promo-code .toggle-promo');

	scHeader.on('click', promoClickHandler);
	scPromoBtn.on('click', promoClickHandler);

	function promoClickHandler() {
		$('.enter-promo').slideToggle(function () {
			scPromoBtn.toggleClass('expand minimize');
			if ($(this).attr("aria-expanded") == "true") {
				$(this).attr("aria-expanded", "false");
				$(this).siblings().attr("aria-expanded", "false").focus();
				setFocusFlagCart = -1;
			}
			else {
				$(this).attr("aria-expanded", "true");
				$(this).siblings().attr("aria-expanded", "true").focus();
				setFocusFlagCart = -1;
			}
		});
	}

	//PROMO CODE REMOVE
	var promoCodeRemove = $('.remove-promo');

	promoCodeRemove.each(function () {
		$(this).on('click', function (event) {
			event.preventDefault();
			var parentContainer = $(this).closest('.applied-promo');
			parentContainer.next('hr').hide();
			parentContainer.hide();
			return false;
		});
	});

}
function handlePreviousPage(homePagePath) {
	var lastPage = document.referrer;
	if (lastPage.toLowerCase().indexOf("checkout") >= 0 || lastPage.toLowerCase().indexOf("cart") >= 0 || lastPage == '') {
		location.replace(homePagePath);
	} else {
		location.replace(lastPage);
	}

}

function handleAbandonPreviousPage() {
	var lastPage = document.referrer;
	if (lastPage.toLowerCase().indexOf("checkout") >= 0 || lastPage.toLowerCase().indexOf("shoppingcart.jsp") >= 0 || lastPage == '') {
		location.replace(window.location.href);
	} else {
		location.replace(lastPage);
	}
}

$(document).on('click', '.cart_previous_page', function () {
	var homePagePath = $('.cart_previous_page').attr("data-id");
	var lastPage = document.referrer;
	var abParamValue = $(".abParameterCart").val();
	var abParamName = "ab";
	var pattern = new RegExp('\\b(' + abParamName + '=).*?(&|$)');
	if (lastPage == '') {
		window.history.back();
	}
	else if (lastPage.toLowerCase().indexOf("checkout") >= 0 || lastPage.toLowerCase().indexOf("shoppingcart.jsp") >= 0 || lastPage == '') {
		homePagePath = homePagePath + '/?ab=' + abParamValue;
		location.replace(homePagePath);
	} else {
		if (lastPage != '') {
			if (lastPage.search(pattern) >= 0) {
				lastPage = lastPage.replace(pattern, '$1' + abParamValue + '$2');
			} else {
				lastPage = lastPage + (lastPage.indexOf('?') > 0 ? '&' : '?') + abParamName + '=' + abParamValue;
			}
		}
		location.replace(lastPage);
	}
});
$(document).on('click', '.add-to-cart.collection', function () {
	var colletinToCart = $("#colletinToCart").val();
	var colletinToCartSplitValue = colletinToCart.split(",");
	var colletinToCartString = colletinToCartSplitValue[0];
	var addItemCount = colletinToCartSplitValue[1];
	var contextPath = $(".contextPath").val();
	$
		.ajax({
			type: "POST",
			cache: false,
			url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp",
			data: { formID: 'addCollectionToCart', colletinToCartString: colletinToCartString, addItemCount: addItemCount },
			success: function (data) {
				//$(data).find('.modal-backdrop.fade.in').remove();
				//setTimeout(function() {
				$("#shoppingCartModal").html(data);
				$("#shoppingCartModal").modal('show');
				progressBar();
				syncCookieByIframe();
				//}, 500);
				initCartScroll();
			},
			dataType: "html"
		});
});

function contactUsPopupWindow(url) {
	popupWindow = window.open(
		url, 'popUpWindow', 'height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
function termsConditionPopupWindow(url) {
	popupWindow = window.open(
		url, 'popUpWindow', 'height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
function privacyPolicyPopupWindow(url) {
	popupWindow = window.open(
		url, 'popUpWindow', 'height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}


function bindCartEvents() {
	orderSummarySection();
	progressBar();
	promocodeSlideToggle();
	shoppingCartTolltip();
	bruCss();
	syncCookieByIframe();
	notAvaialbeTooltip();
	initShoppingCartPage();
	$(window).scroll();
	if ($('.shopping-cart-error-state, .shopping-cart-info-state').is(':visible')) {
		$(window).scrollTop(0);
	}
	$('html').removeClass('modal-open')
	$('body').removeAttr('style');
	$("#sign-in-password-input").maskPassword();
}

function moveToPurchaseInfo() {
	$.ajax({
		type: "POST",
		url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?formID=moveToPurchaseInfo",
		dataType: "json",
		success: function (data) {
			var successData = data;
			if (successData.hasValidationErrors == "true") {
				ajaxCallForCartPage(contextPath);
				checkDefaultStore();
				/*$.ajax({
					type : "POST",
					url : contextPath+"cart/shoppingCartInclude.jsp",
					dataType : "html",
					success : function(data) {
						if($(data).find("#load-cart-norton-script").length){
							$(data).find("#load-cart-norton-script").html($("#loadNortonHiddenDiv").html());
						}
						$( "#shoppingCart" ).html(data);
						bindCartEvents();
					},
					cache : false
				});*/
			} else {
				window.location.href = contextPath + "checkout/checkout.jsp";
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			/*var contextPath = $(".contextPath").val();
			$( "#shoppingCart" ).load( contextPath+"cart/shoppingCartInclude.jsp");*/
			cancelExitBrowserConfimation();
			var contextPath = $(".contextPath").val();
			location.href = contextPath + 'cart/shoppingCart.jsp';
		},
		cache: false
	});
}


/* Start: This Method will call from Shopping Cart Page on Paypal click */
function initializePaypal() {
	$('.payPalErrorDisplay').remove();
	var payPalToken = $('#payPalToken').val();
	var apiName = $('#payPalApiUserName').val();
	var env = $('#payPalhiddenContextPath').val();
	var paypalURL = $('#paypalRedirectURL').val();

	if (payPalToken == 'notTokenExist') {
		$.ajax({
			type: "POST",
			url: contextPath + "checkout/intermediate/paypalAjaxFormSubmit.jsp",
			data: "formID=cartSetExpressCheckoutWithPayPalForm",
			dataType: "html",
			async: false,
			success: function (data) {
				if (data.indexOf('Following are the form errors:') > -1) {
					var error = data.split('Following are the form errors:')[1];
					if (error.indexOf("orderValidationFailed") > -1) {
						ajaxCallForCartPage(contextPath);
					} else {
						$(".payPalErrorDisplay").html("");
						$("#shoppingCart").prepend(data.split('Following are the form errors:')[1]);
					}
					//addError('cartSetExpressCheckoutWithPayPalForm');
				} else {
					var tkn = data.split(":");
					paypal.checkout.setup(apiName, {
						environment: env,
						container: 'paypalPaymentExpresscheckoutId'
					});
					paypal.checkout.initXO();
					paypal.checkout.startFlow(paypalURL + tkn[1]);
					//document.getElementById("paypalPaymentExpresscheckoutId").click();
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					cancelExitBrowserConfimation();
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'cart/shoppingCart.jsp';
				}
			}
		});
	}
	//Added for omniture
	omniPaypalExpressClick();
}

function ajaxCallForCartPage(contextPath) {
	//blockUI();
	$.ajax({
		type: "POST",
		url: contextPath + "cart/shoppingCartInclude.jsp?cartLoad=true",
		async: false,
		dataType: "html",
		processData: false,
		success: function (data, status, xhr) {
			successCartPageCallback(data);
			showCartItemMultiInfoMessage();
			try {
				if ($('.product-information.out-of-stock .buy-1-get-free.inline') && $('.product-information.out-of-stock .buy-1-get-free.inline').text().trim() != "") {
					$('.product-information.out-of-stock .buy-1-get-free.inline').addClass("promotionTrue");
				}
			}
			catch (e) { }
			unBlockUI();
			setFirstNameCookie();
		},
		error: function (jqXHR, textStatus, errorThrown) {
			errorCartPageCallback(jqXHR, textStatus, errorThrown);
			unBlockUI();
			setFirstNameCookie();
		}
	});
}

function showCartItemMultiInfoMessage() {
	if ($("#multi_items_info_message").length > 0) {
		$("#row_order_summary_top").before($("#multi_items_info_message").html());
	}
	if ($('.multipleItemsInlineGenericMsgs').text() != "") {
		try {
			var jdata = $.parseJSON($.trim($('.multipleItemsInlineGenericMsgs').text()));
			$.each(jdata, function (iKey, iVal) {
				$('#limit_' + iKey).parents('.product-information')
					.addClass('error');
				$('#limit_' + iKey).replaceWith(
					'<span class="cart-error-message">' + iVal + '</span>');
			});
		}
		catch (e) { }
	}
}

var successCartPageCallback = function (data) {
	var tmpData = $("<div>" + data + "</div>");
	if (tmpData.find("#load-cart-norton-script").length) {
		tmpData.find("#load-cart-norton-script").html($("#loadNortonHiddenDiv").html());
	}
	$("#cartHeader").html(tmpData.find("#cartHeaderInclude").html());
	TRUPaypalCheckoutReady();
	var shopLocalError = $('#multi_items_info_message.errorFromShopLocal').wrap();
	$("#shoppingCart").html(tmpData.find('#shoppingCartBody').html());
	$("#shoppingCart").find('#OAS_TopRight').remove();
	$("#shoppingCart").prepend(shopLocalError);
	if ($(".shopping-cart-empty").length > 0) {
		$(".errorFromShopLocal").show();
	}
	bindCartEvents();
	var height = $("#shoppingCart").find("#order-summary-block").height();
	$("#shoppingCart").find(".cart-products").css('min-height', height + "px");
}

var errorCartPageCallback = function (jqXHR, textStatus, errorThrown) {
	console.log(errorThrown);
}

/*var setFocusFlagCart = -1;


$(document).on("focusout",".lineItemQty",function(){
	
	setFocusFlagCart++;
	
});



$(document).on("focusin",".my-acount-popover-close",function(){
	
	
	if(setFocusFlagCart != -1){
		
		
		$(".increaseCount").eq(Number(setFocusFlagCart)).focus();
		
	}

	
	
});

*/

//Chkout... Keyboard access
var _thisFocus = "";
$(document).on("focusin", ".lineItemQty", function () {

	if (_thisFocus == "") {

		_thisFocus = $(this).parents(".stepper").attr("id");

	}
});


$(document).on("focusout", ".lineItemQty", function () {

	$(this).eq(_thisFocus).siblings(".increase").focus();

});


$(document).on("focusin", ".my-acount-popover-close, .donation-text", function () {

	if (_thisFocus != "") {

		$(".product-information-description").eq(_thisFocus).find(".increase").focus();
		_thisFocus = "";
	}

})

function removeNonAppliedCoupon(obj) {
	var couponId = $(obj).parents(".applied-bad-promo").find("#error_expire_message").html();
	blockUI();
	$.ajax({
		type: "POST",
		cache: false,
		url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?couponId="
		+ couponId + "&formID=removecoupon",
		dataType: "html",
		data: $("#removecoupon").serialize(),
		success: function (data) {
			$("#shoppingCart").load(contextPath + "cart/shoppingCartInclude.jsp", function () {
				refreshShoppingCartPage();
				unBlockUI();
			});
		},
	});
};
//Added for omniture integration
function omniInvalidCoupon() {
	if (typeof utag != 'undefined') {
		utag.link({
			event_type: 'invalid_coupon_code'
		});
	}
}

function omniPromoSubmit() {
	if (typeof utag != 'undefined') {
		utag.link({
			event_type: 'promotion_submitted'
		});
	}
}
function omniPaypalExpressClick() {
	if (typeof utag != 'undefined') {
		utag.link({
			event_type: 'paypal_express_click'
		});
	}
}

function getRegistryWishlistCookiesAsParams() {

	var registryUser = $.cookie('REGISTRY_USER');
	var wishlist_id = $.cookie("myWishlistID");
	var lastAccessedRegistryID = $.cookie("lastAccessedRegistryID");
	var lastAccessedWishlistID = $.cookie("lastAccessedWishlistID");
	var isRegistry = getParameterByName("isRegistry");
	var isWishlist = getParameterByName("isWishlist");

	var cookieObj = {};
	cookieObj.channelItemFromPDP = false;
	if (typeof registryUser !== "undefined" && registryUser !== "" && typeof lastAccessedRegistryID !== "undefined" && lastAccessedRegistryID !== "" && isRegistry) {
		cookieObj.channelId = lastAccessedRegistryID;
		cookieObj.channelType = "registry";
		cookieObj.channelItemFromPDP = true;
	}
	if (typeof wishlist_id !== "undefined" && wishlist_id !== "" && typeof lastAccessedWishlistID !== "undefined" && lastAccessedWishlistID !== "" && isWishlist) {
		cookieObj.channelId = lastAccessedWishlistID;
		cookieObj.channelType = "wishlist";
		cookieObj.channelItemFromPDP = true;
	}
	return ($.param(cookieObj)) ? '&' + $.param(cookieObj) : '';
}
//Added for omniture
function omniCartUpdate() {
	var pMCategory = $('#pMCategory').val();
	utag.link({ "event_type": "cart_update", "product_merchandising_category": pMCategory });
}



function TRUPaypalCheckoutReady() {
	var apiName = $('#payPalApiUserName').val();
	var env = $('#payPalhiddenContextPath').val();
	try {
		window.paypalCheckoutReady = function () {
			paypal.checkout.setup(apiName, {
				environment: env,
				button: ['paypalPaymentExpresscheckoutId', 'paypalPaymentExpresscheckoutId']
			});
		}
	} catch (e) {
		console.log('paypal script is getting failed on shopping cart.' + e);
	}
}
$(document).on('click', '#cmsFormSubmit', function () {
	$("#cmsFormSubmit").addClass('disabled');
	$(".cmsStatusInfo").show();
	$.ajax({
		type: "POST",
		async: true,
		url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?formID=cmsForm&performSwitch=false",
		dataType: "html",
		cache: false,
		success: function (data) {
			var interval1 = null;
			interval1 = setInterval(checkCMSStatus1, 120000);
			function checkCMSStatus1() {
				$.ajax({
					type: "POST",
					async: true,
					url: contextPath + "preview/cmsStatus.jsp",
					dataType: "html",
					cache: false,
					success: function (data) {
						var isLocked1 = data.trim();
						if (isLocked1 == "false") {
							clearInterval(interval1);
							$.ajax({
								type: "POST",
								async: true,
								url: contextPath + "cart/intermediate/ajaxIntermediateRequest.jsp?formID=cmsForm&performSwitch=true",
								dataType: "html",
								cache: false,
								success: function (data) {
									var interval = null;
									interval = setInterval(checkCMSStatus, 120000);
									function checkCMSStatus() {
										$.ajax({
											type: "POST",
											async: true,
											url: contextPath + "preview/cmsStatus.jsp",
											dataType: "html",
											cache: false,
											success: function (data) {
												var isLocked = data.trim();
												if (isLocked == "false") {
													clearInterval(interval);
													$("#cmsFormSubmit").removeClass('disabled').addClass('enabled');
													$(".cmsStatusInfo").hide();
												}
											},
										});
									}
								},
							});
						}
					},
				});
			}

		},
	});
});

function initShoppingCartPage(isCartRefresh) {
	//elements cache
	var $promoInputBox, $promoApplyBtn;
	initElementsCache();
	bindEvents();

	if(isCartRefresh) {
		openPromoCodeSection();
	}

	function initElementsCache() {
		$promoInputBox = $('.shopping-cart-promo-code .apply-promo-code-textbox');
		$promoApplyBtn = $('.shopping-cart-promo-code .apply-promo-code');
	}

	function bindEvents() {
		$('.shopping-cart-promo-code .enter-promo-code-btn').on('click', function() {
			openPromoCodeSection();
		});

		$('.shopping-cart-promo-code .promo-code-textbox-clear').on('click', function() {
			$promoInputBox.val('');
			hidePromoApplyButton();
		});

		$promoApplyBtn.on('click', function() {
			applyCoupon();
		});

		$promoInputBox.on('keyup input propertychange', function(e) {
			var charCode = (e.which) ? e.which : event.keyCode;

			if($(this).val().trim().length){
				showPromoApplyButton();
				if(charCode == 13) {
					$promoApplyBtn.click();
				}
			}
			else {
				hidePromoApplyButton();
			}
		});
	}

	function showPromoApplyButton() {
		$('.enter-promo').addClass('has-coupon');
		$promoApplyBtn.show('fast');
	}

	function hidePromoApplyButton() {
		$promoApplyBtn.hide('fast');
		$('.enter-promo').removeClass('has-coupon has-error');
	}

	function openPromoCodeSection() {
		$('.shopping-cart-promo-code .toggle-promo-code-container').hide('fast');
		$('.shopping-cart-promo-code .enter-promo').show('fast');
	}
}

function refreshShoppingCartPage() {
	initShoppingCartPage(true);
	orderSummarySection();
	progressBar();
	shoppingCartTolltip();
	bruCss();
	$(window).scroll();
}