function validateDenaliAccountNum(cardNum) {
	var accountNumber = cardNum;
	var isValid = false;
	if (undefined != accountNumber && accountNumber.length == 13) {
		if (accountNumber.substring(0, 2) == '21') {
			var csum = accountNumber.charAt(12);
			if (accountNumber != null && accountNumber.length == 13) {
				accountNumber = accountNumber.substring(0, 12);
			}
			if (accountNumber == null || accountNumber.length != 12) {
				return false;
			}
			var oddsum = '0';
			var evensum = '0';
			var charPos = '0';
			for (var i = accountNumber.length - 1; i >= 0; i--) {
				charPos = eval((accountNumber.length - i) - 1);
				if ((accountNumber.length - i) % 2 == 0) {
					evensum = parseInt(evensum) + parseInt(accountNumber.charAt(charPos));
				} else {
					oddsum = parseInt(oddsum) + parseInt(accountNumber.charAt(charPos));
				}
			}
			var check = 10 - ((evensum * 3 + oddsum) % 10);
			if (check >= 10) check = 0;
			isValid = (csum == check);
		}
	}
	return isValid;
}