var Accept_Encoding;
var Content_Encoding;
cardinalApi = '';
var cardinalErrorMessage = '';
var checkoutTabUrl = [];
var contextPath = '';
var synchronySelectionDone = false;
var cardinalSetup = false;
//var isCardinalEnabled = false;
var emptyCartRedirect = true;
var checkoutProgress = true;
var jwt = '';
var nonceSetup = false;
//var enableVerboseDebug = false;
var nonceRetryAttempts = 0;
var getTokenRetryAttempts = 0;
var tokenizeAndAuthorizeRetryAttempts = 0;
var authorizeSavedTokenRetryAttempts = 0;
//var maxNonceRetryAttempts = 0
//var maxGetTokenRetryAttempts = 0
//var maxTokenizeAndAuthorizeRetryAttempts = 0
//var maxAuthorizeSavedTokenRetryAttempts = 0
var radialInvalidCardErrorCodes = '';
var isFromPromoFinance = false;
$(document).on("click", "#co-gift-options-item-checkbox>div", function () {
	$(this).toggleClass('checkbox-sm-off checkbox-sm-on');
	$(this).parents('.gifting-items-shipping-to-content').find(".giftWrapMessage").val('');
	if ($(this).parents('.gifting-items-shipping-to-content').find("#co-gift-options-item-checkbox").find('div').hasClass("checkbox-sm-off"))
		$(this).parents('.gifting-items-shipping-to-content').find("#co-gift-options-item-checkbox").find('.giftReceipt').val("false");
	else
		$(this).parents('.gifting-items-shipping-to-content').find("#co-gift-options-item-checkbox").find('.giftReceipt').val("true");
});
function loadTabUrl() {
	try {
		checkoutTabUrl['shipping-tab'] = window.contextPath + 'checkout/shipping/shipping.jsp';
		checkoutTabUrl['gifting-tab'] = window.contextPath + 'checkout/gifting/giftOptions.jsp';
		checkoutTabUrl['pickup-tab'] = window.contextPath + 'checkout/storepickup/inStorePickUp.jsp';
		checkoutTabUrl['payment-tab'] = window.contextPath + 'checkout/payment/payment.jsp';
		checkoutTabUrl['review-tab'] = window.contextPath + 'checkout/review/orderReview.jsp';
	} catch (e) { }
}

$(document).ready(function () {
	if ($("#country").length) {
		var countryValue = $("#country").val();
		if (countryValue == 'US') {
			$('#zip').mask('AAAAA-AAAA');
		}
		else {
			$('#zip').mask('AAAAAAAAAA');
		}

	}
	else {
		$('#zip').mask('AAAAA-AAAA');
	}

	formatCreditCardDisplay();
	clearPrePopulatedData();
	/*enable/disable special financing */
	var coCreditCardVal = $("#co-credit-card option:selected").attr('data-coCreditCardVal');
	//enableDisableFinacingSection(coCreditCardVal);

	/*enable/disable special financing */

	// $(document).on('hidden.bs.modal','#checkoutAddCreditCardModal', function () {
	window.contextPath = $('.contextPath').val() || '/';

	loadTabUrl();

	$(document).on("click", "#creditCardModal .modal-backdrop", function () {
		$("body").removeAttr("style")
		$("html").removeClass("modal-open");
	});


	$(document).on('blur', '#co-pickup-tab .store-pickup-form .firstname,#co-pickup-tab .store-pickup-form .lastname,#co-pickup-tab .store-pickup-form .phonenumber,#co-pickup-tab .store-pickup-form .pickupstoreemail', function () {

		var fname = $(this).parents('.store-pickup-form').find('.firstname').val();
		var lname = $(this).parents('.store-pickup-form').find('.lastname').val();
		var phonenumber = $(this).parents('.store-pickup-form').find('.phonenumber').val();
		var email = $(this).parents('.store-pickup-form').find('.pickupstoreemail').val();
		if (fname != "" && lname != "" && phonenumber != "" && email != "") {
			$('#co-pickup-tab .store-pickup-form').each(function () {
				if ($(this).find('.firstname').val() == "" && $(this).find('.lastname').val() == "" && $(this).find('.phonenumber').val() == "" && $(this).find('.pickupstoreemail').val() == "") {
					$(this).find('.firstname').val(fname);
					$(this).find('.lastname').val(lname);
					$(this).find('.phonenumber').val(phonenumber);
					$(this).find('.pickupstoreemail').val(email);
				}
			});
		}

	});

	$(document).on('blur', '#co-pickup-tab .store-pickup-form .alternatefirstname,#co-pickup-tab .store-pickup-form .alternatelastname,#co-pickup-tab .store-pickup-form .alternatephonenumber,#co-pickup-tab .store-pickup-form .alternateemail', function () {
		var fname = $(this).parents('.store-pickup-form').find('.alternatefirstname').val();
		var lname = $(this).parents('.store-pickup-form').find('.alternatelastname').val();
		var phonenumber = $(this).parents('.store-pickup-form').find('.alternatephonenumber').val();
		var email = $(this).parents('.store-pickup-form').find('.alternateemail').val();
		if (fname != "" && lname != "" && phonenumber != "" && email != "") {
			$('#co-pickup-tab .store-pickup-form').each(function () {
				if ($(this).find('.alternatefirstname').val() == "" && $(this).find('.alternatelastname').val() == "" && $(this).find('.alternatephonenumber').val() == "" && $(this).find('.alternateemail').val() == "") {
					$(this).find('.alternatefirstname').val(fname);
					$(this).find('.alternatelastname').val(lname);
					$(this).find('.alternatephonenumber').val(phonenumber);
					$(this).find('.alternateemail').val(email);
				}
			});
		}


	});
	//added code or defect TUW-76643
	$(document).on('submit', '#saveShippingAddress', function (event) {
		event.preventDefault();
		onSubmitShippingAddCheckout('', '');
	});
	//added code or defect TUW-76792
	$(document).on('submit', '#inStorePickup', function (event) {
		event.preventDefault();
		var previousTabValue = $('#previousTabValue').val();
		if (previousTabValue == undefined) {
			previousTabValue = 'payment-tab';
		}
		instore(previousTabValue);
	});

	if ($(".checkout-payment-header").length) {
		$('.checkout-payment-header').prev(".errorDisplay").find("span").html($('.checkout-payment-header').prev(".errorDisplay").find("span").text());
	}

	$(document).on('keyup input propertychange', '#promo-code-payment-page', function () {
		promoButtonOnOff();
	});

	/*if( ($('#isFromSynchrony').val() == "true" && $('#synchronyAppDecision').val() == "A") || ($('#rusCardSelected').val() == "true")){
		$('#isFromSynchrony').val('false');
		$('#radio-rus-creditcard').click();
	}*/
	$(document).on('click', '.printOrSave', function () {
		/*var winPrint = window.open();
		var printContent = $('#electronic-disclosure .print-payment-content').html() || '';
		winPrint.document.write(printContent);
		winPrint.print();
		winPrint.close();*/
		//var winPrint = window.open();
		var printContent = $('#electronic-disclosure .print-payment-content').html().trim() || '';
		$(".synchrony-card-print-container").html(printContent);
		window.print();
	});

});

$(window).load(function () {
	if ($("#co-credit-card").length) {
		$("#co-credit-card").msDropDown();
	}
	checkForZeroTotal();
	ajaxCallForTealiumCheckout(window.contextPath);
});

function promoButtonOnOff() {
	if (($('#promo-code-payment-page').val()).trim() != '') {
		$('#promo-code-payment-page-btn').removeAttr('disabled');
	} else {
		$('#promo-code-payment-page-btn').attr('disabled', 'disabled');
	}
}

var storeLocators;
function findInStorePDP() {
	$('#findInStorePDP').click();
}

$(document).ready(function () {
	var tabName = $("#currentTab").text();
	if (tabName.indexOf("gifting-tab") > 0) {
		if ($(".giftWrapMessage").val()) {
			giftMessageCharectersCheck();
		}

	}
	$(document).on('keypress', '.checkout-review-order.move-to-order-review', function (evt) { /* Fix for TUW-63991 */
		if (evt.which == 13) {
			evt.preventDefault();
			moveToOrderReview();
		}
	});

});

// Radial cardinal TokenizationAndAuthentication call
function startTransaction() {
	$('.errorDisplay').remove();
	var accountNumber, expirationMonth, expirationYear;
	var cardCode = ($('#radio-rus-creditcard').is(':checked')) ? $('#creditCardCVV').val() : ($('#creditCard_CVV').length) ? $('#creditCard_CVV').val() : "";
	var orderEmail = $('#orderEmail').val();
	var _parent = "";
	var expirationMonth = "", cardNumber = "", expirationYear = "";
	if ($("#radio-rus-creditcard").is(":checked")) {
		_parent = "#paymentRusCardForm ";
		accountNumber = $(_parent + '#ccnumber').val();
	} else if ($("#radio-creditcard").is(":checked")) {
		_parent = "#paymentCreditCardForm "
		accountNumber = ($(_parent + '#ccnumber').length) ? $(_parent + '#ccnumber').val() : $(_parent + '#creditCardNumber').val();
	}
	expirationMonth = $(_parent + '#expirationMonth').val();
	expirationYear = $(_parent + '#expirationYear').val();
	var nameOnAccount = $(_parent + '#nameOnCard').val();
	if (_parent == '') {
		accountNumber = $(_parent + '#creditCardNumber').val();
	}
	var firstName = $('#firstName').val();
	var lastName = $('#lastName').val();
	var address1 = $('#address1').val();
	var address2 = $('#address2').val();
	var city = $('#city').val();
	var state = $('#state').val();
	var postalCode = $('#postalCode').val();
	var country = $('#country').val();
	var phoneNumber = $('#phoneNumber').val();
	var email = $('#email').val();

	if (orderEmail != undefined) {
		if (orderEmail == '') {
			$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">Please enter your email address </span>');
			return false;
		}
		if (!isValidEmail(orderEmail)) {
			$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">Email format is not valid, please enter valid email address</span>');
			return false;
		}
	}
	var v = parseInt(accountNumber.substr(0, 6));
	if (v == '604586') {
		if (accountNumber == '' || cardCode == '' || nameOnAccount == '') {
			$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">Please enter complete card details</span>');
			return false;
		}
		expirationMonth = $('#expMonthForPLCC').val();
		expirationYear = $('#expYearForPLCC').val();
	} else {
		if (accountNumber == '' || expirationMonth == '' || expirationYear == '' || cardCode == '' || nameOnAccount == '') {
			$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">Please enter complete card details</span>');
			return false;
		}
	}

	var d = new Date();
	var timeStamp = d.getTime();

	var currentMonth = d.getMonth() + 1;
	var currentYear = d.getFullYear();
	if (parseInt(expirationMonth) < currentMonth && parseInt(expirationYear) == currentYear) {
		//$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">The expiration date you entered occurs in the past. Please re-enter the credit card number, expiration date and security code.</span>');
		addBackendErrorsToForm('Following are the form errors:The expiration date you entered occurs in the past. Please re-enter the credit card number, expiration date and security code.', $('#checkout-container'));
		return false;
	}
	//parseFloat($('#cardianlOrderAmount').val()).toFixed(2);
	//var orderAmt = parseInt($('#cardianlOrderAmount').val() * 100);
	var orderAmt = parseFloat($('#cardianlOrderAmount').val()).toFixed(2).replace('.', '');
	var cardianlOrderId = $('#cardianlOrderId').val();
	var currencyCode = $('#currencyCode').val();
	var customerIp = $.cookie("customerIpAddress");
	if (customerIp == undefined) {
		customerIp = "0:0:0:0:0:0:0:1";
	}
	//Creating json object to call Radial.tokenizeAndAuthorize method

	//Get shipping address
	var shipAddressDetails = getShippingAddress();
	var shipAddress = shipAddressDetails.shipAddress;
	var RequestId = shipAddressDetails.cardinalRequestId;
	if (shipAddress == '' || shipAddress == undefined) {
		var jsonObject = {
			"shipAddressBA": {
				"firstName": firstName,
				"lastName": lastName,
				"address1": address1,
				"address2": address2,
				"city": city,
				"state": state,
				"countryCode": country,
				"postalCode": postalCode,
				"phone1": phoneNumber,
			}
		}
		shipAddress = jsonObject.shipAddressBA;
	}

	var jsonObject =
		{

			"cmpiRequestObject": {
				"RequestId": RequestId,
				"OrderNumber": cardianlOrderId,
				"CurrencyCode": currencyCode,
				"Amount": orderAmt,
				"CardExpMonth": expirationMonth,
				"CardExpYear": expirationYear,
				"CardNumber": accountNumber,
				"EMail": orderEmail,
				"BillingFirstName": firstName,
				"BillingLastName": lastName,
				"BillingAddress1": address1,
				"BillingAddress2": address2,
				"BillingCity": city,
				"BillingState": state,
				"BillingCountryCode": country,
				"BillingPostalCode": postalCode,
				"BillingPhone": phoneNumber,
				"ShippingFirstName": shipAddress.firstName,
				"ShippingLastName": shipAddress.lastName,
				"ShippingAddress1": shipAddress.address1,
				"ShippingAddress2": shipAddress.address2,
				"ShippingCity": shipAddress.city,
				"ShippingState": shipAddress.state,
				"ShippingCountryCode": shipAddress.countryCode,
				"ShippingPostalCode": shipAddress.postalCode,
				"ShippingPhone": shipAddress.phone1,
				"IPAddress": customerIp,
				"UserAgent": navigator.userAgent
			}
		}
	if (enableVerboseDebug) {
		console.log("calling getNONCEResponce() for accountNumber:" + accountNumber);
	}
	//calling nonce service.
	getNONCEResponce();
	if (!nonceSetup) {
		return false;
	}
	if (enableVerboseDebug) {
		console.log("calling tokenizeAndAuthorize() for accountNumber:" + accountNumber);
		console.log("Request data to call tokenizeAndAuthorize() method :" + JSON.stringify(jsonObject));
	}

	//logging success of tokenizationAndAuthentication
	if (enableIntegrationLog) {
		integrationInfoLogCall('true', 'tokenizationAndAuthentication');
	}

	//inovoking tokenizeAndAuthorize method
	Radial.tokenizeAndAuthorize(accountNumber, jsonObject, tokenizeAndAuthorizeCallbackHandler);
	cardinalApi = "TokenizationAndAuthentication";
}

function getNONCEResponce() {
	var errorFlag = false;
	if (enableVerboseDebug) {
		console.log("Inside getNONCEResponce()");
	}
	$.ajax({
		url: "/checkout/common/nonceAjax.jsp",
		type: "POST",
		dataType: "json",
		async: false,
		cache: false,
		success: function (nonceData) {
			if (enableVerboseDebug) {
				console.log("getNONCEResponce() Radial Nonce:" + JSON.stringify(nonceData));
				console.log("getNONCEResponce() START Radial setup()");
			}
			if (typeof nonceData == 'object' && typeof nonceData.nonce != 'undefined' && typeof nonceData.jwt != 'undefined' && nonceData.nonce != '' && nonceData.radialPaymentErrorKey != '50002') {
				Radial.setup(nonceData.nonce, nonceData.jwt);
				if (enableVerboseDebug) {
					console.log("getNONCEResponce() END calling Radial setup()");
				}
				jwt = nonceData.jwt;
				nonceSetup = true;
				nonceRetryAttempts = 0;
				return false;
			}
			nonceRetryAttempts++;
			if (enableVerboseDebug) {
				console.log("Error in getNONCEResponce() and showing the Error radialPaymentErrorKey:" + nonceData.radialPaymentErrorKey);
			}
			if (nonceRetryAttempts == maxNonceRetryAttempts) {
				if (enableVerboseDebug) {
					console.log("Error in getNONCEResponce() and showing the Error msg");
				}
				nonceRetryAttempts = 0;
				nonceSetup = false;
				var radailPaymentErrorCode = nonceData.radialPaymentErrorKey;
				var radailPaymentKey = errorKeyJson[radailPaymentErrorCode];
				var radailPaymentMsg = errorKeyJson[radailPaymentKey];
				$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">' + radailPaymentMsg + '</span>');
				return false;
			}
			errorFlag = true;
		},
		error: function (e) {
			console.log(e);
		}
	});
	if (errorFlag) {
		if (enableVerboseDebug) {
			console.log("Error in getNONCEResponce() and Re_trying with Re_try attempts count is: " + nonceRetryAttempts);
		}
		getNONCEResponce();
	}
}

function integrationInfoLogCall(entry, interfaceName) {
	var truIntegrationLoggingRestAPIURL = $("#truIntegrationLoggingRestAPIURL").val();
	var pushSite = $("#pushSiteId").val();
	var orderId = $('#cardianlOrderId').val();
	$.ajax({
		url: truIntegrationLoggingRestAPIURL + "?orderId=" + orderId + "&pushSite=" + pushSite + "&entry=" + entry + "&interfaceName=" + interfaceName,
		type: "post",
		headers: {
			"X-APP-API_KEY": "apiKey",
			"X-APP-API_CHANNEL": "mobile",
			"Content-Type": "application/x-www-form-urlencoded"
		},
		dataType: "json",
		async: false,
		success: function (data) {
			console.log();
		}
	});
}

//Inovoke TokenizeAndAuthorize  CallbackHandler method
function tokenizeAndAuthorizeCallbackHandler(data) {
	if (enableVerboseDebug) {
		console.log("tokenizeAndAuthorizeCallbackHandler() method Start*** ");
		console.log("tokenizeAndAuthorizeCallbackHandler() response data:" + JSON.stringify(data));
	}
	var _parent = "";
	if ($("#radio-rus-creditcard").is(":checked")) {
		_parent = "#paymentRusCardForm ";
	} else if ($("#radio-creditcard").is(":checked")) {
		_parent = "#paymentCreditCardForm "
	}
	var radailPaymentErrorCode;
	switch (data.ActionCode) {
		case "FAILURE":
			radailPaymentErrorCode = data.failure_code;
			break;
		case "ERROR":
			radailPaymentErrorCode = data.error_code;
			break;
	}

	//logging success of tokenizationAndAuthentication
	if (enableIntegrationLog) {
		integrationInfoLogCall('false', 'tokenizationAndAuthentication');
	}

	if (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '40005' || radailPaymentErrorCode == '50005' || radailPaymentErrorCode == '50006') {
		tokenizeAndAuthorizeRetryAttempts = 0;
		$('#cardinalToken').val(data.account_token);
		$(_parent + '#cBinNum').val($(_parent + '#ccnumber').val().substring(0, 6));
		$(_parent + '#cLength').val($(_parent + '#ccnumber').val().length);
		creditCardCommitOrder();
		return false;
	}

	//getting nonce again and re_try token
	if (radailPaymentErrorCode != undefined && (radailPaymentErrorCode == '50002' || radailPaymentErrorCode == '50004') && tokenizeAndAuthorizeRetryAttempts < maxTokenizeAndAuthorizeRetryAttempts) {
		tokenizeAndAuthorizeRetryAttempts++;
		//Added  time out for defect=TSJ-6625
		setTimeout(function () { startTransaction(); }, 4000);
		return false;
	}

	//re_try token
	if (radailPaymentErrorCode != undefined && (radailPaymentErrorCode == '50001' || radailPaymentErrorCode == '50003') && tokenizeAndAuthorizeRetryAttempts < maxTokenizeAndAuthorizeRetryAttempts) {
		tokenizeAndAuthorizeRetryAttempts++;
		//Added  time out for defect=TSJ-6625
		setTimeout(function () { startTransaction(); }, 4000);
		return false;
	}

	var token_number = data.account_token;
	if ((token_number == undefined || token_number == '') && (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '40001' || radailPaymentErrorCode == '40002' || radailPaymentErrorCode == '40003' || radailPaymentErrorCode == '40004' || radailPaymentErrorCode == '40006' || radailPaymentErrorCode == '50001' || radailPaymentErrorCode == '50002' || radailPaymentErrorCode == '50003' || radailPaymentErrorCode == '50004')) {
		tokenizeAndAuthorizeRetryAttempts = 0;
		var radailPaymentKey = errorKeyJson[radailPaymentErrorCode];
		var radailPaymentMsg = errorKeyJson[radailPaymentKey];
		$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">' + radailPaymentMsg + '</span>');
		return false;
	}

	if (token_number != undefined && token_number != '') {
		$('#cardinalToken').val(data.account_token)
		$(_parent + '#cBinNum').val($(_parent + '#ccnumber').val().substring(0, 6));
		$(_parent + '#cLength').val($(_parent + '#ccnumber').val().length);
		if (enableVerboseDebug) {
			console.log("In tokenizeAndAuthorizeCallbackHandler order placing with ....... Action Code :" + data.ActionCode);
		}
		creditCardCommitOrder();
		return false;
	}
}

function startTokenization_checkout() {
	if (!$(".errorDisplay").hasClass('expiredMessage')) {
		$(".errorDisplay").remove();
	}
	$('.errorDisplay').remove();
	var orderEmail = $('#orderEmail').val();
	if (orderEmail != undefined) {
		if (orderEmail == '') {
			$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">Please enter your email address </span>');
			return false;
		}
		if (!isValidEmail(orderEmail)) {
			$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">Email format is not valid, please enter valid email address</span>');
			return false;
		}
	}

	var _parent = "";
	var accountNumber = "", expirationMonth = "", cardNumber = "", expirationYear = "";
	if ($("#radio-rus-creditcard").is(":checked")) {
		_parent = "#paymentRusCardForm ";
		cardCode = $(_parent + '#creditCardCVV').val();
	} else if ($("#radio-creditcard").is(":checked")) {
		_parent = "#paymentCreditCardForm "
		cardCode = $(_parent + '#creditCard_CVV').val();
	}

	accountNumber = $(_parent + '#ccnumber').val();
	expirationMonth = $(_parent + '#expirationMonth').val();
	expirationYear = $(_parent + '#expirationYear').val();
	nameOnCard = $(_parent + '#nameOnCard').val();

	var v = parseInt(accountNumber.substr(0, 6));
	if (v == '604586') {
		if (accountNumber == '' || cardCode == '' || nameOnCard == '') {
			$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">Please enter complete card details</span>');
			return false;
		}
		expirationMonth = $('#expMonthForPLCC').val();
		expirationYear = $('#expYearForPLCC').val();
	} else {
		if (accountNumber == '' || expirationMonth == '' || expirationYear == '' || cardCode == '' || nameOnCard == '') {
			$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">Please enter complete card details</span>');
			return false;
		}
	}
	var d = new Date();
	var timeStamp = d.getTime();

	var currentMonth = d.getMonth() + 1;
	var currentYear = d.getFullYear();
	if (parseInt(expirationMonth) < currentMonth && parseInt(expirationYear) == currentYear) {
		//$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">The expiration date you entered occurs in the past. Please re-enter the credit card number, expiration date and security code.</span>');
		addBackendErrorsToForm("Following are the form errors:The expiration date you entered occurs in the past. Please re-enter the credit card number, expiration date and security code.", $('#checkout-container'));
		return false;
	}

	//var orderAmt = parseInt($('#cardianlOrderAmount').val() * 100);
	var orderAmt = parseFloat($('#cardianlOrderAmount').val()).toFixed(2).replace('.', '');
	if (enableVerboseDebug) {
		console.log("calling getNONCEResponce() for accountNumber:" + accountNumber);
	}
	//calling nonce service.
	getNONCEResponce();
	if (!nonceSetup) {
		return false;
	}
	if (enableVerboseDebug) {
		console.log("calling tokenizePan() for accountNumber:" + accountNumber);
	}

	//logging success of token
	if (enableIntegrationLog) {
		integrationInfoLogCall('true', 'token');
	}

	//inovoking Radial.tokenize method
	Radial.tokenize(accountNumber, tokenizeCallBackHandler);
	cardinalApi = "Tokenization_checkout";
	return false;
}



//Start : Tokenize method
function tokenizeCallBackHandler(data) {
	if (enableVerboseDebug) {
		console.log("tokenizeCallBackHandler() : Start***");
		console.log("tokenizeCallBackHandler() response response data:" + JSON.stringify(data));
	}
	var radailPaymentErrorCode;
	switch (data.ActionCode) {
		case "FAILURE":
			radailPaymentErrorCode = data.failure_code;
			break;
		case "ERROR":
			radailPaymentErrorCode = data.error_code;
			break;
	}

	//logging success of token
	if (enableIntegrationLog) {
		integrationInfoLogCall('false', 'token');
	}

	//getting nonce again and re_try token
	if (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '50002' && getTokenRetryAttempts < maxGetTokenRetryAttempts) {
		getTokenRetryAttempts++;
		//Added  time out for defect=TSJ-6625
		setTimeout(function () { startTokenization_checkout(); }, 4000);
		return false;
	}

	//re_try token
	if (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '50001' || radailPaymentErrorCode == '50003' && getTokenRetryAttempts < maxGetTokenRetryAttempts) {
		getTokenRetryAttempts++;
		//Added  time out for defect=TSJ-6625
		setTimeout(function () { startTokenization_checkout(); }, 4000);
		return false;
	}

	if (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '40001' || radailPaymentErrorCode == '40002' || radailPaymentErrorCode == '40003' || radailPaymentErrorCode == '40004' || radailPaymentErrorCode == '50001' || radailPaymentErrorCode == '50002' || radailPaymentErrorCode == '50003') {
		getTokenRetryAttempts = 0;
		$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">' + radailPaymentErrorCode + '</span>');
		return false;
	}

	var _parent = "";
	if ($("#radio-rus-creditcard").is(":checked")) {
		_parent = "#paymentRusCardForm ";
	} else if ($("#radio-creditcard").is(":checked")) {
		_parent = "#paymentCreditCardForm "
	}
	$('#cardinalToken').val(data.account_token);
	$(_parent + '#cBinNum').val($(_parent + '#ccnumber').val().substring(0, 6));
	$(_parent + '#cLength').val($(_parent + '#ccnumber').val().length);
	if (cardinalApi == 'creditCardCommitOrder') {
		creditCardCommitOrder();
	} else if (!isFromPromoFinance) {
		ccMoveToOrderReview();
	} else if (isFromPromoFinance) {
		autopopulatePrmoDetails();
	}
}

//End : Tokenize method

// Radial cardinal Authentication call
function startAuthentication() {
	var _parent = "";
	var expirationMonth = "", cardNumber = "", expirationYear = "";
	if ($("#radio-rus-creditcard").is(":checked")) {
		_parent = "#paymentRusCardForm ";
		cardNumber = $(_parent + '#ccnumber').val();
	} else if ($("#radio-creditcard").is(":checked")) {
		_parent = "#paymentCreditCardForm "
		cardNumber = $(_parent + '#creditCardNumber').val();
	}

	expirationMonth = $(_parent + '#expirationMonth').val();
	expirationYear = $(_parent + '#expirationYear').val();

	if (_parent == '') {
		cardNumber = $(_parent + '#creditCardNumber').val();
	}

	var firstName = $('#firstName').val();
	var lastName = $('#lastName').val();
	var address1 = $('#address1').val();
	var address2 = $('#address2').val();
	var city = $('#city').val();
	var state = $('#state').val();
	var postalCode = $('#postalCode').val();
	var country = $('#country').val();
	var phoneNumber = $('#phoneNumber').val();
	var email = $('#orderEmail').val();
	var d = new Date();
	var timeStamp = d.getTime();
	//var orderAmt = parseInt($('#cardianlOrderAmount').val() * 100);
	var orderAmt = parseFloat($('#cardianlOrderAmount').val()).toFixed(2).replace('.', '');
	var customerIp = $.cookie("customerIpAddress");
	if (customerIp == undefined) {
		customerIp = "0:0:0:0:0:0:0:1";
	}

	//Get shipping address
	var shipAddressDetails = getShippingAddress();
	var shipAddress = shipAddressDetails.shipAddress;
	var RequestId = shipAddressDetails.cardinalRequestId;
	if (shipAddress == '' || shipAddress == undefined) {
		var jsonObject = {
			"shipAddressBA": {
				"firstName": firstName,
				"lastName": lastName,
				"address1": address1,
				"address2": address2,
				"city": city,
				"state": state,
				"countryCode": country,
				"postalCode": postalCode,
				"phone1": phoneNumber,
			}
		}
		shipAddress = jsonObject.shipAddressBA;
	}

	var jsonObject =
		{
			"cmpiRequestObject": {
				"RequestId": RequestId,
				"OrderNumber": $('#cardianlOrderId').val(),
				"CurrencyCode": $('#currencyCode').val(),
				"Amount": orderAmt,
				"CardExpMonth": expirationMonth,
				"CardExpYear": expirationYear,
				"CardNumber": cardNumber,
				"EMail": email,
				"BillingFirstName": firstName,
				"BillingLastName": lastName,
				"BillingAddress1": address1,
				"BillingAddress2": address2,
				"BillingCity": city,
				"BillingState": state,
				"BillingCountryCode": country,
				"BillingPostalCode": postalCode,
				"BillingPhone": phoneNumber,
				"ShippingFirstName": shipAddress.firstName,
				"ShippingLastName": shipAddress.lastName,
				"ShippingAddress1": shipAddress.address1,
				"ShippingAddress2": shipAddress.address2,
				"ShippingCity": shipAddress.city,
				"ShippingState": shipAddress.state,
				"ShippingCountryCode": shipAddress.countryCode,
				"ShippingPostalCode": shipAddress.postalCode,
				"ShippingPhone": shipAddress.phone1,
				"IPAddress": customerIp,
				"UserAgent": navigator.userAgent
			}
		}
	if (enableVerboseDebug) {
		console.log("calling getNONCEResponce() for cardNumber:" + cardNumber);
	}
	//calling nonce service
	getNONCEResponce();
	//TODO display the error msg if any error as part of nonce
	if (enableVerboseDebug) {
		console.log("calling authorizeSavedToken() for cardNumber:" + cardNumber);
		console.log("Request data to call authorizeSavedToken() method :" + JSON.stringify(jsonObject));
	}

	//invoking Radial.authorizeSavedToken method
	Radial.authorizeSavedToken(cardNumber, jsonObject, authorizeSavedTokenCallbackHandler);
	return false;
}

//Calling authorizeSavedToken CallbackHandler
function authorizeSavedTokenCallbackHandler(data) {
	if (enableVerboseDebug) {
		console.log("authorizeSavedTokenCallbackHandler() Start***");
		console.log("authorizeSavedTokenCallbackHandler() response data:" + JSON.stringify(data));
	}
	var radailPaymentErrorCode;
	switch (data.ActionCode) {
		case "FAILURE":
			radailPaymentErrorCode = data.failure_code;
			break;
		case "ERROR":
			radailPaymentErrorCode = data.error_code;
			break;
	}

	if (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '40002' || radailPaymentErrorCode == '50001' || radailPaymentErrorCode == '50005' || radailPaymentErrorCode == '50006') {
		if (cardinalApi == "creditCardCommitOrder") {
			creditCardCommitOrder();
		} else if (cardinalApi == "placeOrderConfirm") {
			$("#checkoutReviewProcessingOverlay").modal('show');
			setTimeout(function () {
				placeOrderConfirm();
				$(".review_placeOrder").removeAttr("disabled");
				$("#checkoutReviewProcessingOverlay").modal('hide');
			}, 520);
		}
		return false;
	}

	//getting nonce again and re_try token
	if (radailPaymentErrorCode != undefined && (radailPaymentErrorCode == '50002' || radailPaymentErrorCode == '50004') && authorizeSavedTokenRetryAttempts < maxAuthorizeSavedTokenRetryAttempts) {
		authorizeSavedTokenRetryAttempts++;
		//Added  time out for defect=TSJ-6625
		setTimeout(function () { startAuthentication(); }, 4000);
		return false;
	}

	//re_try token
	if (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '50003' && authorizeSavedTokenRetryAttempts < maxAuthorizeSavedTokenRetryAttempts) {
		authorizeSavedTokenRetryAttempts++;
		//Added  time out for defect=TSJ-6625
		setTimeout(function () { startAuthentication(); }, 4000);
		return false;
	}
	if (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '40001' || radailPaymentErrorCode == '40006' || radailPaymentErrorCode == '50002' || radailPaymentErrorCode == '50004') {
		var radailPaymentKey = errorKeyJson[radailPaymentErrorCode];
		var radailPaymentMsg = errorKeyJson[radailPaymentKey];
		if (cardinalApi == "creditCardCommitOrder") {
			$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">' + radailPaymentMsg + '</span>');
		} else if (cardinalApi == "placeOrderConfirm") {
			$(".review_placeOrder").removeAttr("disabled");
			$('.checkout-review-header').eq(0).before('<span class="errorDisplay">' + radailPaymentMsg + '</span>');
		}
		return false;
	}
	if (cardinalApi == "creditCardCommitOrder") {
		creditCardCommitOrder();
	} else if (cardinalApi == "placeOrderConfirm") {
		$("#checkoutReviewProcessingOverlay").modal('show');
		setTimeout(function () {
			placeOrderConfirm();
			$(".review_placeOrder").removeAttr("disabled");
			$("#checkoutReviewProcessingOverlay").modal('hide');
		}, 520);
	}
	return false;
}
//End : Authentication method

function giftMessageCharectersCheck() {
	var giftwrapvalue;
	$('.gifting-items-shipping-to-content').each(function () {
		giftwrapvalue = $(this).find('.giftWrapMessage');
		var maxLength = parseInt(giftwrapvalue.attr('maxTextLength'));
		var len = giftwrapvalue.val().length;
		if (len >= maxLength) {
			giftwrapvalue.val(giftwrapvalue.val().substring(0, maxLength));
			len = maxLength;
		}
		giftwrapvalue.parent().find('.chars-left').text(maxLength - len);
	});
	/*
	 * var maxLength = parseInt($(".giftWrapMessage").attr('maxTextLength'));
	 * var len = $(".giftWrapMessage").val().length; if (len >= maxLength) {
	 * $(".giftWrapMessage").val($(".giftWrapMessage").val().substring(0,
	 * maxLength)); len = maxLength; }
	 * $(".giftWrapMessage").parent().find('.chars-left').text(maxLength - len);
	 * $("#co-gift-options-item-checkbox>div").removeClass("checkbox-sm-off");
	 * $("#co-gift-options-item-checkbox>div").addClass("checkbox-sm-on");
	 */
}


function shipToAddress() {
	var copyToBilling = $('.checkout-template .same-billing-address').find('div').hasClass('checkbox-sm-on');
	$('#billingAddressSameAsShippingAddress').val(copyToBilling);
	$("#saveShippingAddress").submit();
}

function activeTabs() {
	$.ajax({
		url: window.contextPath + "checkout/common/checkoutProgress.jsp",
		type: "POST",
		dataType: "html",
		success: function (data) {
			$('#checkout-progress-container').html(data);
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log(errorThrown)
		}
	});
}

function continueToCheckout(tabClassName) {

	if (window.emptyCartRedirect) {
		$.ajax({
			url: window.contextPath + "checkout/common/emptyCartRedirect.jsp",
			type: "POST",
			dataType: "json",
			async: false,
			cache: false,
			success: function (data) {
				var successData = data;
				if (successData.redirect == "true" || successData.redirect == true) {
					cancelExitBrowserConfimation();
					window.location.href = window.contextPath + "cart/shoppingCart.jsp";
				}
				$(window).scrollTop(0);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					cancelExitBrowserConfimation();
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'cart/shoppingCart.jsp';
				}
			}
		});
	}
	window.emptyCartRedirect = true;

	var requestUrl = (checkoutTabUrl[tabClassName]) ? checkoutTabUrl[tabClassName] : '';
	if ($("#radio-paypal").is(":checked")) {
		requestUrl += "?paymentMethod=PayPal";
	}
	$.ajax({
		url: requestUrl,
		type: "POST",
		async: false,
		success: function (data) {
			$('#checkout-container').html(data);
			if ($("#load-checkout-norton-script").length) {
				$("[id=load-checkout-norton-script]").html($("#loadNortonHiddenDiv").html());
			}
			setTimeout(function(){
				clearPrePopulatedData();	
			},3000);
			$('button.checkout-nav-bottom-tab').removeClass('checkoutActive');
			$('.checkout-nav-bottom-tab.' + tabClassName).addClass('checkoutActive');
			if (tabClassName == "review-tab") {
				$('button.checkout-nav-bottom-tab.payment-tab').addClass('checkoutActiveDefault tickVisible activeTab');
				// checkoutStoreLocator();
			}
			if (tabClassName == "gifting-tab") {
				/*
				 * if($(".giftWrapMessage").val()){
				 * giftMessageCharectersCheck(); }
				 */
			}
			if (tabClassName == "payment-tab") {
				$("#co-credit-card").msDropDown();
				var coCreditCardVal = $("#co-credit-card option:selected").attr('data-coCreditCardVal');
				enableDisableFinacingSection(coCreditCardVal);
				$('.checkout-payment-header').prev(".errorDisplay").find("span").html($('.checkout-payment-header').prev(".errorDisplay").find("span").text());
			}
			if (tabClassName == "pickup-tab") {
				var options = {
					header: ".accordion-header",
					collapsible: true,
					heightStyle: "content",
					active: false
				}
				$('.store-pickup-items-accordion').accordion(options);
			}
			if (tabClassName == "shipping-tab") {
				detailsPopover();
			}

			//if( window.checkoutProgress ){
			activeTabs();
			//}
			//window.checkoutProgress  = true;

			if (tabClassName == 'gifting-tab') {
				if ($(".giftWrapMessage").val()) {
					giftMessageCharectersCheck();
				}
			}
			$(window).scrollTop(0);
			$("html").removeClass("modal-open");
			$("body").css("padding-right", "");
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log(errorThrown)
		}
	});
}


$(document).on('click', 'button.checkout-nav-bottom-tab', function () {
	if ($(this).hasClass('activeTab')) {
		var url = $(this).filter(".checkout-nav-bottom-tab").attr("name");
		var className = $(this).filter(".checkout-nav-bottom-tab").attr("class");
		var creditcardSelected = false;
		var tabClassName;
		if (className.indexOf("shipping-tab") > 0) {
			tabClassName = "shipping-tab";
		} else if (className.indexOf("gifting-tab") > 0) {
			tabClassName = "gifting-tab";
		} else if (className.indexOf("pickup-tab") > 0) {
			tabClassName = "pickup-tab";
		} else if (className.indexOf("payment-tab") > 0) {
			tabClassName = "payment-tab";
			creditcardSelected = true;
		} else if (className.indexOf("review-tab") > 0) {
			tabClassName = "review-tab";
		} else {
			return;
		}
		continueToCheckout(tabClassName);
		checkForZeroTotal();
		ajaxCallForTealiumCheckout(window.contextPath);
		// if(creditcardSelected)$("#radio-creditcard").click();
		/*
		 * var selectedSavedCard = $("#co-credit-card").val();
		 * if(selectedSavedCard != undefined && selectedSavedCard != null ){
		 * $("#radio-creditcard").click(); }
		 */
	}
	else {
		return false
	}
});

function gifting(previousTab) {
	$('.errorDisplay').remove();
	// var giftReceiptSelected =
	// $("#co-gift-options-item-checkbox").find(".inline").hasClass("checkbox-sm-on");
	// var giftMessage = $("#giftMessage").val();
	// var shipGroupId = $("#shipGroupId").html();
	var isOrderInstorePickup = $("#isOrderInstorePickup").val();
	dataString = $('#chekcout_gift_options').serialize();
	//blockUI();
	// $('#applyGiftMessage').click();
	$.ajax({
		url: window.contextPath + "checkout/intermediate/giftingAjaxFormSubmit.jsp?action=applyGiftMessage",
		type: "POST",
		data: dataString,
		dataType: "json",
		cache: false,
		success: function (data) {
			var successData = data;
			if (successData.success) {
				if ((previousTab == undefined || previousTab == '') && isOrderInstorePickup == 'true') {
					continueToCheckout('pickup-tab');
				} else if ((previousTab == undefined || previousTab == '') && isOrderInstorePickup == 'false') {
					continueToCheckout('payment-tab');
				} else {
					continueToCheckout(previousTab);
				}
				$("body").removeClass("modal-open");
				$("html").removeClass("modal-open");
				$("body").css('padding-right', '');
				var coCreditCardVal = $("#co-credit-card option:selected").attr('data-coCreditCardVal');
				enableDisableFinacingSection(coCreditCardVal);
				ajaxCallForTealiumCheckout(window.contextPath);
			} else {
				var errorMessages = successData.errorMessages;
				$.each(errorMessages, function (key, val) {
					$('#' + key + '_giftWrapMessage').val('');
					$('#' + key + '-gifting-items').prepend('<span class="errorDisplay">' + val + '</span>');
				});
				$("body").css('padding-right', '');
			}
			//unBlockUI();
			checkForZeroTotal();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				//	  alert(contextPath);
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
}

$(document).on('keyup', '#review_giftMessage', function () {
	$('.errorDisplay').remove();
	var giftMessage = $("#review_giftMessage").val();
	if (giftMessage.length > 180) {
		$("#review_giftMessage").val(giftMessage.substring(0, 180));
		return false;
	}
});
// function reviewUpdateGiftMessage(shipGroupId) {

function instore(previousTab) {
	//blockUI();
	$.ajax({
		url: window.contextPath + "checkout/common/checkSession.jsp",
		type: "POST",
		dataType: "json",
		cache: false,
		success: function (redirectData) {
			$('.errorDisplay').remove();
			if (!$('#inStorePickup').valid()) {
				return false;
			}
			$.ajax({
				type: "POST",
				cache: false,
				url: window.contextPath + "checkout/intermediate/ajaxIntermediateRequest.jsp",
				dataType: "html",
				data: $("#inStorePickup").serialize() + '&action=instore',
				success: function (data) {
					if (data.indexOf('Following are the form errors:') > -1) {
						/*addError('inStorePickup');
						$('.store-pickup-header .store-pick-error').html( data.split('Following are the form errors:')[1] );*/
						addBackendErrorsToForm(data, $("#inStorePickup"));
					} else if (previousTab == 'review-tab') {
						continueToCheckout(previousTab);
						$(window).scrollTop(0);
						var coCreditCardVal = $("#co-credit-card option:selected").attr('data-coCreditCardVal');
						enableDisableFinacingSection(coCreditCardVal);
						ajaxCallForTealiumCheckout(window.contextPath);

					} else {
						continueToCheckout('payment-tab');
						$(window).scrollTop(0);
						var coCreditCardVal = $("#co-credit-card option:selected").attr('data-coCreditCardVal');
						enableDisableFinacingSection(coCreditCardVal);
						ajaxCallForTealiumCheckout(window.contextPath);
					}
					//unBlockUI();
					checkForZeroTotal();
				},
				error: function (xhr, ajaxOptions, thrownError) {
					if (xhr.status == '409') {
						cancelExitBrowserConfimation();
						var contextPath = $(".contextPath").val();
						//	  alert(contextPath);
						location.href = contextPath + 'cart/shoppingCart.jsp';
					}
				}
			});
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
};


$(document).on('click', '.gift-save-message', function () {
	// $('.errorDisplay').remove();

	var _this = $(this);
	var giftMessage = _this.parent().find(".giftMessageArea").val();
	var shipGroupId = _this.parent().find('.review_giftMessage_shippingGroupId').val();
	$.ajax({
		url: window.contextPath + "checkout/intermediate/giftingAjaxFormSubmit.jsp",
		type: "POST",
		data: { action: "review_applyGiftMessage", giftMessage: giftMessage, shipGroupId: shipGroupId },
		dataType: "json",
		cache: false,
		success: function (data) {
			var successData = data;
			//console.log(data);
			_this.parents('.gift-message').find('.errorDisplay').remove();
			if (successData.success) {

				var message = _this.parents('.gift-message').find('.giftMessageArea').val();
				if (message != "") {
					_this.parents('.gift-message').find('.reviewGiftMessage').html('"' + message + '"&nbsp;.');
				}
				else {
					_this.parents('.gift-message').find('.reviewGiftMessage').html('');
				}
				_this.parents('.gift-message').find('.gift-edit-message').show();
				_this.parents('.gift-message').find('.gift-message-area-wrapper').hide();
				_this.parents('.gift-message').find('.review-gift-message-container').hide();
			} else {
				var errorMessages = successData.errorMessages;
				$.each(errorMessages, function (key, val) {
					_this.parents('.gift-message').find('.giftMessageArea').val('');
					$('#' + key + '-gift-message').prepend('<span class="errorDisplay">' + val + '</span>');
				});
				return false;
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				//	  alert(contextPath);
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
});

function moveToOrderReview() {
	var orderEmail = $("#orderEmail").val();
	$.ajax({
		url: window.contextPath + "checkout/common/checkSession.jsp",
		type: "POST",
		dataType: "json",
		cache: false,
		success: function (redirectData) {
			removeBackendErrorMessagesInline($("#checkout-container"));
			$('.errorDisplay').remove();
			var isSuccess = true;
			$('.checkOut-promocode-error-state, .promocode-error-msg').hide();
			$('#promo-code-payment-page').removeClass('error-highlight');
			var rewardNumber = $("#enterMembershipIDInCheckout").val();
			if (!$("#radio-paypal").is(":checked") && $('#paymentBillingAddressForm').length > 0 && !$('#paymentBillingAddressForm').valid()) {
				return false;
			}
			if ($('#orderPaymentEmailForm').length > 0 && !$('#orderPaymentEmailForm').valid()) {
				return false;
			}
			if ($('#enterMembershipIDInCheckout').length > 0 && !$('#enterMembershipIDInCheckout').valid()) {
				return false;
			}
			if ($('#addOrEditCreditCard').length > 0 && !$('#addOrEditCreditCard').valid()) {
				return false;
			}
			var isRewardsCardValid = validateDenaliAccountNum(rewardNumber);
			var couponCode = $('#promo-code-payment-page').val();
			if (couponCode !== undefined && couponCode != "") {
				isSuccess = applyCouponCode(couponCode);
			}
			if (isSuccess) {
				//update billing adddress if changed
				if($('.billing-info.billing-info-changed').length > 0) {
					checkoutSaveBillingAddressChanges(orderEmail, rewardNumber, isRewardsCardValid);
				}			

				if ($("#radio-payinstore").is(":checked")) {
					var formData = $('#paymentBillingAddressForm').serialize();
					$.ajax({
						type: "POST",
						async: false,
						dataType: "html",
						url: window.contextPath + "checkout/intermediate/commitOrderAjaxFormSubmit.jsp",
						data: formData + '&formID=inStorePaymentOrderReviewForm&orderEmail=' + orderEmail + '&rewardNumber=' + rewardNumber + '&isRewardsCardValid=' + isRewardsCardValid,
						success: function (data) {

							if (data.indexOf('Following are the form errors:') > -1) {
								$.ajax({
									url: window.contextPath + "checkout/common/emptyCartRedirect.jsp",
									type: "POST",
									dataType: "json",
									async: false,
									cache: false,
									success: function (redirectData) {
										var successData = redirectData;
										if (successData.redirect == "true" || successData.redirect == true) {
											cancelExitBrowserConfimation();
											window.location.href = window.contextPath + "cart/shoppingCart.jsp";
										} else {
											var shipRestrictionsFound = (typeof successData.shipRestrictionsFound != 'undefined') ? successData.shipRestrictionsFound : 'false';
											$('#shipRestrictionsFound').val(shipRestrictionsFound);
											addBackendErrorsToForm(data, $('#co-payment-tab'));
											/*addError('inStorePaymentOrderReviewForm');
											addError('payment-email-form');*/
										}
									},
									error: function (xhr, ajaxOptions, thrownError) {
										if (xhr.status == '409') {
											cancelExitBrowserConfimation();
											var contextPath = $(".contextPath").val();
											location.href = contextPath + 'cart/shoppingCart.jsp';
										}
									}
								});
							} else {
								if (data.indexOf('address-doctor-overlay-from-ajax:') > -1) {
									$('#payment-page-suggested-address .modal-dialog-payment-page-suggested').html(data.split('address-doctor-overlay-from-ajax:')[1]);
									$('#payment-page-suggested-address').modal('show');
								} else {
									continueToCheckout('review-tab');
									ajaxCallForTealiumCheckout(window.contextPath);
								}
							}
						},
						error: function (xhr, ajaxOptions, thrownError) {
							if (xhr.status == '409') {
								cancelExitBrowserConfimation();
								var contextPath = $(".contextPath").val();
								location.href = contextPath + 'cart/shoppingCart.jsp';
							}
						}
					});
					return;
				}
				if ($("#radio-paypal").is(":checked")) {
					$.ajax({
						type: "POST",
						async: false,
						dataType: "html",
						url: window.contextPath + "checkout/intermediate/paypalAjaxFormSubmit.jsp",
						data: { formID: 'payPalOrderReviewForm', orderEmail: orderEmail, rewardNumber: rewardNumber, isRewardsCardValid: isRewardsCardValid },
						success: function (data) {
							if (data.indexOf('Following are the form errors:') > -1) {
								addBackendErrorsToForm(data, $('#co-payment-tab'));
								/*addError('payPalOrderReviewForm');
								addError('payment-email-form');*/
							} else {
								// continueToCheckout( window.contextPath + 'checkout/review/orderReview.jsp?paymentMethod=PayPal','review-tab');
								continueToCheckout('review-tab');
								ajaxCallForTealiumCheckout(window.contextPath);
							}
						},
						error: function (xhr, ajaxOptions, thrownError) {
							if (xhr.status == '409') {
								cancelExitBrowserConfimation();
								var contextPath = $(".contextPath").val();
								location.href = contextPath + 'cart/shoppingCart.jsp';
							}
						}
					});
					return;
				}
				if ($("#radio-creditcard").is(":checked") || $("#radio-rus-creditcard").is(":checked")) {
					var creditFlag = false;
					$(document).find(".credit-component").each(function () {
						if ($(this).hasClass("credit-card-number")) {
							creditFlag = true;
						}
					});
					if ($(".rusCardBillingAddressForm").find(".checkout-billing-address-form").length) {
						creditFlag = true;
					}
					var orderAmt1 = parseFloat($('#cardianlOrderAmount').val()).toFixed(2).replace('.', '');
					if (creditFlag && $('#paymentCreditCardForm').length && !$("#radio-rus-creditcard").is(":checked") && orderAmt1 > 0) {
						if (!$('#paymentCreditCardForm').valid()) {
							checkMaskPasswordError($('#paymentCreditCardForm'));
							return false;
						}
					} else if (creditFlag && $('#paymentRusCardForm').length && $("#radio-rus-creditcard").is(":checked") && orderAmt1 > 0) {
						if (!$('#paymentRusCardForm').valid()) {
							return false;
						}
					}
					if (($('#co-credit-card').length == 0 || ($("#co-credit-card option:last").length > 0 && $("#co-credit-card option:last").is(":selected")) || $("#radio-rus-creditcard").is(":checked")) && orderAmt1 > 0) {
						var _parent = "";
						if ($("#radio-rus-creditcard").is(":checked")) {
							_parent = "#paymentRusCardForm ";
						} else if ($("#radio-creditcard").is(":checked")) {
							_parent = "#paymentCreditCardForm "
						}
						var cardNum = $(_parent + ' #ccnumber').val();
						if (isNaN(cardNum)) {
							var isValidCard = true;
						} else {
							var isValidCard = validateCreditCardNumber(cardNum);
						}
						if (isValidCard) {
							if (isCardinalEnabled) {
								var cardinalToken = $("#cardinalToken").val();
								var callTokenization = true;
								if (cardinalToken != 'undefined' && cardinalToken != '' && !isNaN(cardinalToken)) {
									callTokenization = false;
								}
								//var v = parseInt(cardNum.substr(0, 6));
								var isSaved_Card = $(_parent + '#isSaved_Card').val() || $('#isSaved_Card').val();
								if (isSaved_Card == 'true') {
									if (isNaN(cardNum)) {
										console.log("not calling as saved PLCC");
										callTokenization = false;
										$(_parent + ' #cardinalToken').val($(_parent + ' #ccnumber').val());
										$(_parent + ' #cBinNum').val($(_parent + ' #ccnumber').val().substring(0, 6));
										$(_parent + ' #cLength').val($(_parent + ' #ccnumber').val().length);
										ccMoveToOrderReview();
										return false;
									}
									else {
										getNONCEResponce();
										if (!nonceSetup) {
											return false;
										}
										cardinalApi = 'ccMoveToOrderReview';
										Radial.tokenize(cardNum, tokenizeCallBackHandler);
										return false;
									}
								}
								if (callTokenization) {
									startTokenization_checkout();
									return false;
								}
							} else {
								$(_parent + ' #cardinalToken').val($(_parent + ' #ccnumber').val());
								$(_parent + ' #cBinNum').val($(_parent + ' #ccnumber').val().substring(0, 6));
								$(_parent + ' #cLength').val($(_parent + ' #ccnumber').val().length);
								ccMoveToOrderReview();
							}
						} else {
							/*$('.global-error-display').text(window.invalidCardErroMsg);
							$(window).scrollTop(0);*/
							addBackendErrorsToForm(window.invalidCardErroMsg, $('#co-payment-tab'));
						}
					} else {
						ccMoveToOrderReview();
						ajaxCallForTealiumCheckout(window.contextPath);
					}
				} else {
					if ($("#gcAppliedCount").val() > 0) {
						var formData = $('#paymentBillingAddressForm').serialize();
						$.ajax({
							type: "POST",
							async: false,
							dataType: "html",
							url: window.contextPath + "checkout/intermediate/billingAjaxFormSubmit.jsp",
							data: formData + '&formID=giftCardPaymentOrderReviewForm&orderEmail=' + orderEmail + '&rewardNumber=' + rewardNumber + '&isRewardsCardValid=' + isRewardsCardValid,
							success: function (data) {
								//alert(data)
								if (data.indexOf('Following are the form errors:') > -1) {
									addBackendErrorsToForm(data, $('#co-payment-tab'));
								} else {
									if (data.indexOf('address-doctor-overlay-from-ajax:') > -1) {
										$('#payment-page-suggested-address .modal-dialog-payment-page-suggested').html(data.split('address-doctor-overlay-from-ajax:')[1]);
										$('#payment-page-suggested-address').modal('show');
									} else {
										continueToCheckout('review-tab');
										ajaxCallForTealiumCheckout(window.contextPath);
									}
								}
							},
							error: function (xhr, ajaxOptions, thrownError) {
								if (xhr.status == '409') {
									cancelExitBrowserConfimation();
									var contextPath = $(".contextPath").val();
									location.href = contextPath + 'cart/shoppingCart.jsp';
								}
							}
						});
						return;
					}
					ccMoveToOrderReview();
					ajaxCallForTealiumCheckout(window.contextPath);
				}
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
			return true;
		}
	});
}
function ccMoveToOrderReview() {
	$('.errorDisplay').remove();

	var parentClass = '';
	var parentForm = '';
	if ($('#radio-rus-creditcard').is(':checked')) {
		parentClass = 'rus-credit-card';
		parentForm = '#paymentRusCardForm';
	} else if ($('#radio-creditcard').is(':checked')) {
		parentClass = 'credit-card';
		parentForm = '#paymentCreditCardForm';
	}

	var creditCardToken = $('#cardinalToken').val();
	if (creditCardToken == undefined || creditCardToken == '') {
		creditCardToken = $(parentForm + ' #ccnumber').val() || $(parentForm + ' #creditCardNumber').val();
	}
	var cardType = $(parentClass + '#cardType').val();
	$('#paymentMode').val('creditCard');
	var orderEmail = $('#orderEmail').val();
	if (orderEmail != undefined) {
		if (orderEmail == '') {
			$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">Please enter your email address </span>');
			return false;
		}
		if (!isValidEmail(orderEmail)) {
			$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">Email format is not valid, please enter valid email address</span>');
			return false;
		}
	}

	var financingOptions = $('.financingOptions').val();
	var isNoFinanceAgreed = false;
	if (financingOptions == "true") {
		try {
			var disclosureAgreementStatus = $("." + parentClass).find('#disclosure-agreement-status').val();
			var isFromSynchrony = $('#isFromSynchrony').val();
			var isRadioDisabled = $('.six-month-financing.' + parentClass + ' #yes-six-month-financing').is(':checked');
			isNoFinanceAgreed = $('.six-month-financing.' + parentClass + ' #no-six-month-financing').is(':checked');
			if (disclosureAgreementStatus == '0' && (parentClass == 'credit-card' && isRadioDisabled || parentClass == 'rus-credit-card') && !isNoFinanceAgreed) {
				autopopulatePrmoDetails('order-review'); return false;
			}
		} catch (e) { }
	} else if (financingOptions == "false" && isFromSynchrony == 'true' && !synchronySelectionDone) {
		$('#toysrus-creditcard').modal('show'); return false;
	}

	var orderSet = $("#orderSet").val();
	var selectedSavedCard = $("#co-credit-card").val();
	var rewardNumber = $("#enterMembershipIDInCheckout").val();
	var isRewardsCardValid = validateDenaliAccountNum(rewardNumber);

	var formData = "";
	if ($('#paymentBillingAddressForm,#radio-rus-creditcard').is(':checked')) {
		formData = $('#paymentBillingAddressForm,#paymentRusCardForm').serialize();
	} else if ($('#radio-creditcard').is(':checked')) {
		formData = $('#paymentBillingAddressForm,#paymentCreditCardForm,#addOrEditCreditCard').serialize();
	}
	var paymentGroupType = "";
	if ($("#radio-creditcard").is(":checked") || $("#radio-rus-creditcard").is(":checked")) {
		paymentGroupType = "creditCard";
	} else {
		var orderAmt = parseFloat($('#cardianlOrderAmount').val()).toFixed(2).replace('.', '');
		formData = $('#paymentBillingAddressForm,#paymentCreditCardForm,#addOrEditCreditCard').serialize();
		if (orderAmt != undefined && orderAmt == '000') {
			$.ajax({
				type: "POST",
				async: false,
				dataType: "html",
				url: window.contextPath + "checkout/intermediate/billingAjaxFormSubmit.jsp",
				data: formData + '&formID=paymentCreditCardForm&selectedSavedCard=' + selectedSavedCard + '&orderEmail=' + orderEmail + '&ccToken=' + creditCardToken + '&rewardNumber=' + rewardNumber + '&isRewardsCardValid=' + isRewardsCardValid + '&cardType=' + cardType + '&paymentGroupType=' + paymentGroupType + '&isNoFinanceAgreed=' + isNoFinanceAgreed,
				success: function (data) {
					//alert(1419);
					if (data.indexOf('Following are the form errors:') > -1) {
						var shipRestrictionsFound = $(data.split('Following are the form errors:')[1]).find('#shipRestrictionsFoundTemp').wrap();
						$('#shipRestrictionsFound').val(shipRestrictionsFound.val());
						addBackendErrorsToForm(data, $("#co-payment-tab"));
						$(window).scrollTop(0);
						if ($('#radio-rus-creditcard').is(':checked')) {
							$('.global-error-display').find(".errorDisplay").find("span").html($('.global-error-display').find(".errorDisplay").find("span").text());
							//addError('paymentRusCardForm');
							addBackendErrorsToForm(data, $("#co-payment-tab"));
							$(window).scrollTop(0);
						} else if ($('#radio-creditcard').is(':checked')) {
							//addError('paymentCreditCardForm');
							addBackendErrorsToForm(data, $("#co-payment-tab"));
							$(window).scrollTop(0);
							$('.global-error-display').find(".errorDisplay").find("span").html($('.global-error-display').find(".errorDisplay").find("span").text());
						}
						//addError('payment-email-form');
						/*addBackendErrorsToForm(data, $("#co-payment-tab"));*/
					} else {
						if (data.indexOf('address-doctor-overlay-from-ajax:') > -1) {
							$('#payment-page-suggested-address .modal-dialog-payment-page-suggested').html(data.split('address-doctor-overlay-from-ajax:')[1]);
							$('#payment-page-suggested-address').modal('show');

						} else if (data.indexOf('address-saved-success-from-ajax:') > -1) {
							$(".checkout-nav-bottom-tab.review-tab").css({ display: "inline" });
							continueToCheckout('review-tab');
						}
						else {
							continueToCheckout('review-tab');
						}
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					if (xhr.status == '409') {
						cancelExitBrowserConfimation();
						var contextPath = $(".contextPath").val();
						location.href = contextPath + 'cart/shoppingCart.jsp';
					}
				}
			});
			//continueToCheckout('review-tab');
			return false;
		}
	}
	//alert("orderEmail"+orderEmail);
	$.ajax({
		type: "POST",
		async: false,
		dataType: "html",
		url: window.contextPath + "checkout/intermediate/billingAjaxFormSubmit.jsp",
		data: formData + '&formID=paymentCreditCardForm&selectedSavedCard=' + selectedSavedCard + '&orderEmail=' + orderEmail + '&ccToken=' + creditCardToken + '&rewardNumber=' + rewardNumber + '&isRewardsCardValid=' + isRewardsCardValid + '&cardType=' + cardType + '&paymentGroupType=' + paymentGroupType + '&isNoFinanceAgreed=' + isNoFinanceAgreed,
		success: function (data) {
			//alert(1419);
			if (data.indexOf('Following are the form errors:') > -1) {
				var shipRestrictionsFound = $(data.split('Following are the form errors:')[1]).find('#shipRestrictionsFoundTemp').wrap();
				$('#shipRestrictionsFound').val(shipRestrictionsFound.val());
				addBackendErrorsToForm(data, $("#co-payment-tab"));
				if ($('#radio-rus-creditcard').is(':checked')) {
					$('.global-error-display').find(".errorDisplay").find("span").html($('.global-error-display').find(".errorDisplay").find("span").text());
					//addError('paymentRusCardForm');
					addBackendErrorsToForm(data, $("#co-payment-tab"));
				} else if ($('#radio-creditcard').is(':checked')) {
					//addError('paymentCreditCardForm');
					addBackendErrorsToForm(data, $("#co-payment-tab"));
					$('.global-error-display').find(".errorDisplay").find("span").html($('.global-error-display').find(".errorDisplay").find("span").text());
				}
				//addError('payment-email-form');
				/*addBackendErrorsToForm(data, $("#co-payment-tab"));*/
			} else {
				if (data.indexOf('address-doctor-overlay-from-ajax:') > -1) {
					$('#payment-page-suggested-address .modal-dialog-payment-page-suggested').html(data.split('address-doctor-overlay-from-ajax:')[1]);
					$('#payment-page-suggested-address').modal('show');

				} else if (data.indexOf('address-saved-success-from-ajax:') > -1) {
					$(".checkout-nav-bottom-tab.review-tab").css({ display: "inline" });
					continueToCheckout('review-tab');
				}
				else {
					continueToCheckout('review-tab');
				}
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});

}
function commitOrder() {
	setTimeout(function() {
		$('.shipping-address-field-input').focus();
	});
	$.ajax({
		url: window.contextPath + "checkout/common/checkSession.jsp",
		type: "POST",
		dataType: "json",
		async: false,
		cache: false,
		success: function (redirectData) {
			if ($('#currentSite').val() != 'sos') {
				InAuthRequest();
			}
			var isSuccess = true;
			$('.errorDisplay').remove();
			removeBackendErrorMessagesInline($("body"));
			var couponCode = $('#promo-code-payment-page').val().trim();
			$('.checkOut-promocode-error-state, .promocode-error-msg').hide();
			$('#promo-code-payment-page').removeClass('error-highlight');
			if ($('#paymentBillingAddressForm').length > 0 && !$('#paymentBillingAddressForm').valid()) {
				return false;
			}
			if ($('#orderPaymentEmailForm').length > 0 && !$('#orderPaymentEmailForm').valid()) {
				return false;
			}
			if ($('#addOrEditCreditCard').length > 0 && !$('#addOrEditCreditCard').valid()) {
				return false;
			}
			if (couponCode != "" && $('#promoCodePaymentPageForm').valid()) {
				isSuccess = applyCouponCode(couponCode);
			}
			if (isSuccess) {
				cancelExitBrowserConfimation();
				var orderEmail = $("#orderEmail").val();
				if (orderEmail != undefined) {
					if (orderEmail == '') {
						$("#orderEmail").closest("#payment-email-form").append('<span class="errorDisplay" style="margin-top:4px;float: left;">Please enter your email address </span>');
						return false;
					}
					if (!isValidEmail(orderEmail)) {
						$("#orderEmail").closest("#payment-email-form").append('<span class="errorDisplay" style="margin-top:4px;float: left;">Email format is not valid, please enter valid email address</span>');
						return false;
					}
				}
				if ($("#radio-payinstore").is(":checked")) {
					formData = $('#paymentBillingAddressForm').serialize();
					var rewardNumber = $("#enterMembershipIDInCheckout").val();
					var isRewardsCardValid = validateDenaliAccountNum(rewardNumber);
					$("#checkoutReviewProcessingOverlay").modal('show');
					$.when(callingFraudJsCollector()).then(function () {
						var deviceID = $("#field_d1").val();
						var browserAcceptEncoding = "";
						if (Accept_Encoding != undefined) {
							browserAcceptEncoding = Accept_Encoding;
						} else if (Content_Encoding != undefined) {
							browserAcceptEncoding = Content_Encoding;
						}
						deviceID = deviceID.substring(0, 4000);
						setTimeout(function () {
							$.ajax({
								type: "POST",
								async: false,
								dataType: "html",
								url: window.contextPath + "checkout/intermediate/commitOrderAjaxFormSubmit.jsp",
								data: formData + '&formID=inStorePaymentCommitOrderForm&orderEmail=' + orderEmail + '&rewardNumber=' + rewardNumber + '&isRewardsCardValid=' + isRewardsCardValid + '&deviceID=' + deviceID + '&browserAcceptEncoding=' + browserAcceptEncoding,
								success: function (data) {
									if (data.indexOf('Following are the form errors:') > -1) {
										$.ajax({
											url: window.contextPath + "checkout/common/emptyCartRedirect.jsp",
											type: "POST",
											dataType: "json",
											async: false,
											cache: false,
											success: function (redirectData) {
												var successData = redirectData;
												if (successData.redirect == "true" || successData.redirect == true) {
													cancelExitBrowserConfimation();
													window.location.href = window.contextPath + "cart/shoppingCart.jsp";
												} if (typeof successData.validationFailed != 'undefined' && successData.validationFailed == true) {
													continueToCheckout('review-tab');
												} else {
													addBackendErrorsToForm(data, $('#co-payment-tab'));
													$("#checkoutReviewProcessingOverlay").modal('hide');
												}
											},
											error: function (xhr, ajaxOptions, thrownError) {
												if (xhr.status == '409') {
													cancelExitBrowserConfimation();
													var contextPath = $(".contextPath").val();
													location.href = contextPath + 'cart/shoppingCart.jsp';
												}
											}
										});
									} else if (data.indexOf('address-doctor-overlay-from-ajax:') > -1) {
										$("#checkoutReviewProcessingOverlay").modal('hide');
										$('#payment-page-suggested-address .modal-dialog-payment-page-suggested').html(data.split('address-doctor-overlay-from-ajax:')[1]);
										$('#payment-page-suggested-address').modal('show');
									} else {
										window.location.href = window.contextPath + "checkout/confirm/orderConfirmation.jsp";
									}
								},
								error: function (xhr, ajaxOptions, thrownError) {
									if (xhr.status == '409') {
										cancelExitBrowserConfimation();
										var contextPath = $(".contextPath").val();
										location.href = contextPath + 'cart/shoppingCart.jsp';
									}
								}
							});
						}, 520);
						return;
					});
				} else if ($("#radio-creditcard").is(":checked") || $("#radio-rus-creditcard").is(":checked")) {
					var creditFlag = false;
					$(document).find(".credit-component").each(function () {
						if ($(this).hasClass("credit-card-number")) {
							creditFlag = true;
						}
					});
					if ($(".rusCardBillingAddressForm").find(".checkout-billing-address-form").length) {
						creditFlag = true;
					}
					var orderAmt1 = parseFloat($('#cardianlOrderAmount').val()).toFixed(2).replace('.', '');
					if (creditFlag && $('#paymentCreditCardForm').length && !$("#radio-rus-creditcard").is(":checked") && orderAmt1 > 0) {
						if (!$('#paymentCreditCardForm').valid()) {
							checkMaskPasswordError($('#paymentCreditCardForm'));
							return false;
						}
					} else if (creditFlag && $('#paymentRusCardForm').length && $("#radio-rus-creditcard").is(":checked") && orderAmt1 > 0) {
						if (!$('#paymentRusCardForm').valid()) {
							return false;
						}
					}
					if (orderAmt1 > 0) {
						if ($('#co-credit-card').length == 0 || ($("#co-credit-card option:last").length > 0 && $("#co-credit-card option:last").is(":selected")) || $("#radio-rus-creditcard").is(":checked")) {
							var cardNum = $('#ccnumber').val() || $('#creditCardNumber').val();
							var isValidCreditCardNumber = validateCreditCardNumber(cardNum);
							if (isValidCreditCardNumber) {
								if (isCardinalEnabled) {
									var callStartTransaction = true;
									var cardinalToken = $("#cardinalToken").val();
									if (cardinalToken != 'undefined' && cardinalToken != '' && !isNaN(cardinalToken)) {
										callStartTransaction = false;
									}
									var v = parseInt(cardNum.substr(0, 6));
									var isSaved_Card = $('#isSaved_Card').val();
									//START: PLCC CARD
									if (v == '604586') {
										if (isNaN(cardNum)) {
											console.log("not calling as saved PLCC");
											callStartTransaction = false;
											$('#cardinalToken').val(cardNum);
											$('#cBinNum').val(cardNum.substring(0, 6));
											$('#cLength').val(cardNum.length);
											creditCardCommitOrder();
											return false;
										} else {
											getNONCEResponce();
											if (!nonceSetup) {
												return false;
											}
											cardinalApi = 'creditCardCommitOrder';
											Radial.tokenize(cardNum, tokenizeCallBackHandler);
											return false;
										}
									}
									//END: PLCC CARD
									if (callStartTransaction) {
										//for new card
										startTransaction();
									} else {
										//saved card
										cardinalApi = "creditCardCommitOrder";
										startAuthentication();
									}
								} else {
									// start : cardinal on / off changes
									$('#cardinalToken').val($('#ccnumber').val());
									$('#cBinNum').val($('#ccnumber').val().substring(0, 6));
									$('#cLength').val($('#ccnumber').val().length);
									creditCardCommitOrder();
								}
							} else {
								//$('.global-error-display').text(window.invalidCardErroMsg);
								//$(window).scrollTop(0);
								addBackendErrorsToForm(window.invalidCardErroMsg, $('#checkout-container'));
							}
						} else {
							if (isCardinalEnabled) {
								cardinalApi = "creditCardCommitOrder";
								var cardNum = $('#ccnumber').val() || $('#creditCardNumber').val();
								var callAuthentication = true;
								var v = parseInt(cardNum.substr(0, 6));
								var isSa6ved_Card = $('#isSaved_Card').val();
								if (v == '604586') {
									callStartTransaction = false;
									creditCardCommitOrder();
									return false;
								}
								if (callAuthentication) {
									startAuthentication();
								}
							} else {
								creditCardCommitOrder();
							}
						}
					} else {
						//$("#checkoutReviewProcessingOverlay").modal('show');
						//setTimeout(function(){
						creditCardCommitOrder();
						//$("#checkoutReviewProcessingOverlay").modal('hide');
						//}, 520);
					}
				}
				else {
					giftCardCommitOrder('Payment');
				}
			}

		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
}




function giftCardCommitOrder(fromPageName) {
	if ($("#gcAppliedCount").val() > 0) {
		$("#checkoutReviewProcessingOverlay").modal('show');
	}
	var orderEmail = $("#orderEmail").val();
	var rewardNumber = $("#enterMembershipIDInCheckout").val();
	var isRewardsCardValid = validateDenaliAccountNum(rewardNumber);
	$.when(callingFraudJsCollector()).then(function () {
		var deviceID = $("#field_d1").val();
		var browserAcceptEncoding = "";
		if (Accept_Encoding != undefined) {
			browserAcceptEncoding = Accept_Encoding;
		} else if (Content_Encoding != undefined) {
			browserAcceptEncoding = Content_Encoding;
		}
		deviceID = deviceID.substring(0, 4000);
		var formData = $('#paymentBillingAddressForm').serialize();
		if ($("#gcAppliedCount").val() > 0) {
			$("#checkoutReviewProcessingOverlay").modal('show');
		}
		setTimeout(function () {
			$.ajax({
				type: "POST",
				async: false,
				dataType: "html",
				url: window.contextPath + "checkout/intermediate/commitOrderAjaxFormSubmit.jsp?" + formData,
				data: { formID: 'giftCardPaymentCommitOrderForm', orderEmail: orderEmail, fromPageName: fromPageName, rewardNumber: rewardNumber, isRewardsCardValid: isRewardsCardValid, deviceID: deviceID, browserAcceptEncoding: browserAcceptEncoding },
				success: function (data) {
					if (data.indexOf('Following are the form errors:') > -1) {
						$.ajax({
							url: window.contextPath + "checkout/common/emptyCartRedirect.jsp",
							type: "POST",
							dataType: "json",
							async: false,
							cache: false,
							success: function (redirectData) {
								var successData = redirectData;
								if (successData.redirect == "true" || successData.redirect == true) {
									cancelExitBrowserConfimation();
									window.location.href = window.contextPath + "cart/shoppingCart.jsp";
								} else {
									if (typeof successData.validationFailed != 'undefined' && successData.validationFailed == true) {
										continueToCheckout('review-tab');
									} else {
										//$('#co-payment-tab').prepend( data.split('Following are the form errors:')[1] );
										addBackendErrorsToForm(data, $("#co-payment-tab"));
										$("#checkoutReviewProcessingOverlay").modal('hide');
									}
								}
							},
							error: function (xhr, ajaxOptions, thrownError) {
								if (xhr.status == '409') {
									cancelExitBrowserConfimation();
									var contextPath = $(".contextPath").val();
									location.href = contextPath + 'cart/shoppingCart.jsp';
								}
							}
						});
					} else if (data.indexOf('address-doctor-overlay-from-ajax:') > -1) {
						$("#checkoutReviewProcessingOverlay").modal('hide');
						$('#payment-page-suggested-address .modal-dialog-payment-page-suggested').html(data.split('address-doctor-overlay-from-ajax:')[1]);
						$('#payment-page-suggested-address').modal('show');
					} else {
						window.location.href = window.contextPath + "checkout/confirm/orderConfirmation.jsp";
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					if (xhr.status == '409') {
						cancelExitBrowserConfimation();
						var contextPath = $(".contextPath").val();
						location.href = contextPath + 'cart/shoppingCart.jsp';
					}
				}
			});
		}, 520);
		return;
	});
}

function creditCardCommitOrder() {
	var financingOptions = $('.financingOptions').val();
	var isNoFinanceAgreed = false;
	if (financingOptions == "true") {
		var isFromSynchrony = $('#isFromSynchrony').val();
		var parentClass = '';
		if ($('#radio-rus-creditcard').is(':checked')) {
			parentClass = 'rus-credit-card';
		} else if ($('#radio-creditcard').is(':checked')) {
			parentClass = 'credit-card';
		}
		var disclosureAgreementStatus = $("." + parentClass).find('#disclosure-agreement-status').val();
		var isRadioDisabled = $('.six-month-financing.' + parentClass + ' #yes-six-month-financing').is(':checked');
		isNoFinanceAgreed = $('.six-month-financing.' + parentClass + ' #no-six-month-financing').is(':checked');
		if (disclosureAgreementStatus == '0' && ((parentClass == 'credit-card' || parentClass == 'rus-credit-card') && (isRadioDisabled)) && !isNoFinanceAgreed) {
			/*if(!isFinancingFlowComplete(parentClass)) return false;*/
			autopopulatePrmoDetails('place-order');
			return false;
		}
	} else if (financingOptions == "false" && isFromSynchrony == 'true' && !synchronySelectionDone) {
		/*$('#toysrus-creditcard').modal('show'); return false;*/
		autopopulatePrmoDetails('place-order');
		return false;
	}

	var orderSet = $("#orderSet").val();
	var selectedSavedCard = $("#co-credit-card").val();
	var cardinalToken = $('#cardinalToken').val();
	if (cardinalToken == '' || cardinalToken == undefined
		|| cardinalToken == 'undefined') {
		if ($('#radio-rus-creditcard').is(':checked')) {
			cardinalToken = $('#paymentRusCardForm #ccnumber').val();
		} else if ($('#radio-creditcard').is(':checked')) {
			cardinalToken = $('#paymentCreditCardForm #creditCardNumber').val();
		}
	}
	//var respJwt = $('#respJwt').val();
	var cBinNum = $('#cBinNum').val();
	var cLength = $('#cLength').val();
	var orderEmail = $('#orderEmail').val();
	var rewardNumber = $("#enterMembershipIDInCheckout").val();
	var isRewardsCardValid = validateDenaliAccountNum(rewardNumber);
	$.when(callingFraudJsCollector()).then(function () {
		var deviceID = $("#field_d1").val();
		var browserAcceptEncoding = "";
		if (Accept_Encoding != undefined) {
			browserAcceptEncoding = Accept_Encoding;
		} else if (Content_Encoding != undefined) {
			browserAcceptEncoding = Content_Encoding;
		}
		deviceID = deviceID.substring(0, 4000);
		var paymentGroupType = "creditCard";
		var cardType = cardTypeForCvvValidation;
		if ((cardType == '' || typeof cardType == 'undefined') && $('#co-credit-card').length > 0 && !$('#co-credit-card option:selected').hasClass('addAnotherCardPaymentPage')) {
			cardType = $('#co-credit-card option:selected').attr('data-cocreditcardval');
		}
		var formData = "";
		if ($('#radio-rus-creditcard').is(':checked')) {
			formData = $('#paymentBillingAddressForm,#paymentRusCardForm').serialize();
		} else if ($('#radio-creditcard').is(':checked')) {
			formData = $('#paymentBillingAddressForm,#paymentCreditCardForm,#addOrEditCreditCard').serialize();
		}
		var pageName = $('#pageName').val() || '';

		$.ajax({
			type: "POST",
			async: false,
			dataType: "html",
			url: window.contextPath + "checkout/intermediate/commitOrderAjaxFormSubmit.jsp",
			data: formData + '&formID=placeOrderCreditCardForm&selectedSavedCard=' + selectedSavedCard + '&orderEmail=' + orderEmail + '&ccToken=' + cardinalToken + '&rewardNumber=' + rewardNumber + '&isRewardsCardValid=' + isRewardsCardValid + "&paymentGroupType=" + paymentGroupType + '&cardType=' + cardType + '&cBinNum=' + cBinNum + '&cLength=' + cLength + '&pageName=' + pageName + '&isNoFinanceAgreed=' + isNoFinanceAgreed + '&deviceID=' + deviceID + '&browserAcceptEncoding=' + browserAcceptEncoding,
			success: function (data) {
				if (data.indexOf('Following are the form errors:') > -1) {
					$.ajax({
						url: window.contextPath + "checkout/common/emptyCartRedirect.jsp",
						type: "POST",
						dataType: "json",
						async: false,
						cache: false,
						success: function (redirectData) {
							var successData = redirectData;
							if (successData.redirect == "true" || successData.redirect == true) {
								cancelExitBrowserConfimation();
								window.location.href = window.contextPath + "cart/shoppingCart.jsp";
							} else {
								if (typeof successData.validationFailed != 'undefined' && successData.validationFailed == true) {
									continueToCheckout('review-tab');
								} else {
									//continueToCheckout('payment-tab');
									/* $('#co-payment-tab').prepend(data.split('Following are the form errors:')[1] );*/
									if ($('#radio-rus-creditcard').is(':checked')) {
										/* addError('paymentRusCardForm'); */
										addBackendErrorsToForm(data, $('#paymentRusCardForm'));
									} else if ($('#radio-creditcard').is(':checked')) {
										/* addError('paymentCreditCardForm'); */
										addBackendErrorsToForm(data, $('#paymentCreditCardForm'));
									} else {
										addBackendErrorsToForm(data, $('#payment-email-form'));
									}
									/* addError('payment-email-form'); */
								}
							}
						},
						error: function (xhr, ajaxOptions, thrownError) {
							if (xhr.status == '409') {
								cancelExitBrowserConfimation();
								var contextPath = $(".contextPath").val();
								location.href = contextPath + 'cart/shoppingCart.jsp';
							}
						}
					});
				} else {
					if (data.indexOf('address-doctor-overlay-from-ajax:') > -1) {
						$('#payment-page-suggested-address .modal-dialog-payment-page-suggested').html(data.split('address-doctor-overlay-from-ajax:')[1]);
						$('#payment-page-suggested-address').modal('show');
					} else if (data.indexOf('address-saved-success-from-ajax:') > -1) {
						$("#checkoutReviewProcessingOverlay").modal('show');
						setTimeout(
							function () {
								window.location.href = window.contextPath + "checkout/confirm/orderConfirmation.jsp";
							}, 600);
					} else {
						$("#checkoutReviewProcessingOverlay").modal('show');
						setTimeout(
							function () {
								window.location.href = window.contextPath + "checkout/confirm/orderConfirmation.jsp";
							}, 600);
					}
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					cancelExitBrowserConfimation();
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'cart/shoppingCart.jsp';
				}
			}
		});
	});
}

$(document).on('click', '.review_placeOrder', function () {
	$('.review_placeOrder').attr('disabled', 'disabled');
	placeOrder();
});

function placeOrder() {
	if ($('#currentSite').val() != 'sos') {
		InAuthRequest();
	}
	if ($('#membershipIDInReviewForm').length > 0 && !$('#membershipIDInReviewForm').valid()) {
		return false;
	}
	if ($("#order_review_giftCard").length > 0 && $("#order_review_creditCard").length == 0) {
		$("#checkoutReviewProcessingOverlay").modal('show');
		setTimeout(function () {
			placeOrderConfirm();
			$(".review_placeOrder").removeAttr("disabled");
			$("#checkoutReviewProcessingOverlay").modal('hide');
		}, 520);
	} else if ($("#order_review_creditCard").length > 0) {
		$.ajax({
			url: window.contextPath + "checkout/common/checkSession.jsp",
			type: "POST",
			dataType: "json",
			cache: false,
			success: function (redirectData) {
				if (isCardinalEnabled) {
					cardinalApi = "placeOrderConfirm";
					var cardNum = $('#ccnumber').val() || $('#creditCardNumber').val();
					var callAuthentication = true;
					var v = parseInt(cardNum.substr(0, 6));
					var isSaved_Card = $('#isSaved_Card').val();
					var creditCardType = $('#cardType').val();
					if (v == '604586' && isSaved_Card == 'true') {
						console.log("not calling as saved PLCC");
						callAuthentication = false;
					}
					if (callAuthentication) {
						startAuthentication();
					} else {
						$("#checkoutReviewProcessingOverlay").modal('show');
						setTimeout(function () {
							placeOrderConfirm();
							$(".review_placeOrder").removeAttr("disabled");
							$("#checkoutReviewProcessingOverlay").modal('hide');
						}, 520);
					}
				} else {
					$("#checkoutReviewProcessingOverlay").modal('show');
					setTimeout(function () {
						placeOrderConfirm();
						$(".review_placeOrder").removeAttr("disabled");
						$("#checkoutReviewProcessingOverlay").modal('hide');
					}, 520);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					cancelExitBrowserConfimation();
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'cart/shoppingCart.jsp';
				}
			}
		});
	} else {
		$("#checkoutReviewProcessingOverlay").modal('show');
		setTimeout(function () {
			placeOrderConfirm();
			$(".review_placeOrder").removeAttr("disabled");
			$("#checkoutReviewProcessingOverlay").modal('hide');
		}, 520);
	}
	return false;
}

function placeOrderConfirm() {

	// $("#checkoutReviewProcessingOverlay").css({opacity:30});
	cancelExitBrowserConfimation();
	//var respJwt = $('#respJwt').val();
	// $("#placeOrder").click();
	var rewardNumber = $("#enterMembershipIDInReview").val() || '';
	var isRewardsCardValid = validateDenaliAccountNum(rewardNumber);
	$.when(callingFraudJsCollector()).then(function () {
		var deviceID = $("#field_d1").val();
		var browserAcceptEncoding = "";
		if (Accept_Encoding != undefined) {
			browserAcceptEncoding = Accept_Encoding;
		} else if (Content_Encoding != undefined) {
			browserAcceptEncoding = Content_Encoding;
		}
		deviceID = deviceID.substring(0, 4000);
		$.ajax({
			url: window.contextPath + "checkout/intermediate/commitOrderAjaxFormSubmit.jsp",
			type: "POST",
			data: 'action=commitOrderFromReview' + '&rewardNumber=' + rewardNumber + '&isRewardsCardValid=' + isRewardsCardValid + '&deviceID=' + deviceID + '&browserAcceptEncoding=' + browserAcceptEncoding,
			async: false,
			cache: false,
			success: function (data) {
				// var successData = data;
				$(".review_placeOrder").removeAttr("disabled");
				if (data.indexOf('Following are the form errors:') > -1) {
					/* alert('error'); */
					// $("#checkoutReviewProcessingOverlay").modal('hide');

					$.ajax({
						url: window.contextPath + "checkout/common/emptyCartRedirect.jsp",
						type: "POST",
						dataType: "json",
						async: false,
						cache: false,
						success: function (redirectData) {
							var successData = redirectData;
							if (successData.redirect == "true" || successData.redirect == true) {
								cancelExitBrowserConfimation();
								window.location.href = window.contextPath + "cart/shoppingCart.jsp";
							} else {
								if (typeof successData.validationFailed != 'undefined' && successData.validationFailed == true) {
									continueToCheckout('review-tab');
									showSynchronyError();
								} else if (data.indexOf('Following are the form errors:') > -1) {
									var shipRestrictionsFound = (typeof successData.shipRestrictionsFound != 'undefined') ? successData.shipRestrictionsFound : 'false';
									$('#shipRestrictionsFound').val(shipRestrictionsFound);

									var radial_error = $(data).find("#radial_error_code").val();
									if (radial_error.indexOf('tru_error_gift_cards_balance_is_less_than_order_amount') > -1 || radial_error.indexOf('tru_radial_error') > -1) {
										continueToCheckout('payment-tab');
									}
									addBackendErrorsToForm(data, $(".checkout-confirmation-content"));
								}
							}
						},
						error: function (xhr, ajaxOptions, thrownError) {
							if (xhr.status == '409') {
								cancelExitBrowserConfimation();
								var contextPath = $(".contextPath").val();
								location.href = contextPath + 'cart/shoppingCart.jsp';
							}
						}
					});
				} else {
					$("#checkoutReviewProcessingOverlay").modal('show');
					setTimeout(function () {
						window.location.href = window.contextPath + "checkout/confirm/orderConfirmation.jsp";
					}, 600);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					cancelExitBrowserConfimation();
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'cart/shoppingCart.jsp';
				}
			}
		});
		return false;
	});
}

function changeShippingAddress() {

	if ($("#selectedShippingAddress option:last").is(":selected")) {
		$("#addAnotherAddressModal").click();
	} else {
		var shipToAddressName = $("#selectedShippingAddress option:selected").attr("value");
		var shippingMethod = $('.select-shipping-method-block.selected').find('.shipMethodCode').val();
		var shippingPrice = $('.select-shipping-method-block.selected').find('.shippingPrice').val();
		/*
		 * alert("shippingMethod\t:"+shippingMethod);
		 * alert("shippingPrice\t:"+shippingPrice);
		 */
		if (shippingMethod === undefined) {
			shippingMethod = "";
		}
		if (shippingPrice === undefined) {
			shippingPrice = "0.0";
		}
		$.ajax({
			url: window.contextPath + "checkout/intermediate/shippingAjaxFormSubmit.jsp",
			type: "POST",
			data: { action: "changeShippingAddress", shipToAddressName: shipToAddressName, shippingMethod: shippingMethod, shippingPrice: shippingPrice },
			dataType: "json",
			cache: false,
			success: function (data) {
				var successData = data;
				if (successData.success) {
					continueToCheckout('shipping-tab');
				} else {
					// Need to display the error msgs
					continueToCheckout('shipping-tab');
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					cancelExitBrowserConfimation();
					var contextPath = $(".contextPath").val();
					//	  alert(contextPath);
					location.href = contextPath + 'cart/shoppingCart.jsp';
				}
			}

		});
	}
}

$(document).on("hide.bs.modal", "#myAccountAddAddressModal, #myAccountAddCreditCardModal", function () {
	if ($('#checkout-container').length) {
		var orderEmail = '';
		if (typeof $("#orderEmail").val() !== 'undefined') {
			orderEmail = $("#orderEmail").val();
		}
		var enterMembershipIDInCheckout = '';
		if (typeof $("#enterMembershipIDInCheckout").val() !== 'undefined') {
			enterMembershipIDInCheckout = $("#enterMembershipIDInCheckout").val();
		}
		var formAction = "";
		var refreshTab = "";
		if ($("#shippingAddressFormFragment").length) {
			formAction = "closeAddressModel";
			refreshTab = "shipping-tab";
		} else if ($("#creditCardAjaxFragment").length) {
			formAction = "closeBillingAddressModel";
			refreshTab = "payment-tab";
		}
		if (formAction != '' && refreshTab != '') {
			$.ajax({
				url: window.contextPath + "checkout/intermediate/shippingAjaxFormSubmit.jsp",
				type: "POST",
				data: { action: formAction },
				cache: false,
				success: function (data) {
					var successData = data;
					if (successData.success) {
						//continueToCheckout('shipping-tab');
						continueToCheckout(refreshTab);
						$("body").removeClass("modal-open");
						$("html").removeClass("modal-open");
						$("body").css('padding-right', '');
					} else {
						// Need to display the error msgs
						//continueToCheckout('shipping-tab');
						continueToCheckout(refreshTab);
						$("body").removeClass("modal-open");
						$("html").removeClass("modal-open");
						$("body").css('padding-right', '');
					}
					if (refreshTab == "payment-tab") {
						if (typeof $("#orderEmail").val() !== 'undefined' && orderEmail != '') {
							$("#orderEmail").val(orderEmail);
						}
						if (typeof $("#enterMembershipIDInCheckout").val() !== 'undefined' && enterMembershipIDInCheckout != '') {
							$("#enterMembershipIDInCheckout").val(enterMembershipIDInCheckout);
						}
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					if (xhr.status == '409') {
						cancelExitBrowserConfimation();
						var contextPath = $(".contextPath").val();
						//	  alert(contextPath);
						location.href = contextPath + 'cart/shoppingCart.jsp';
					}
				}

			});
		}
	}
});

function shipToAddressFromOverlay() {
	suggestedShippingAddressSelect('1');
	$("#shipToAddressFromSuggested").click();
    /*$("html").removeClass("modal-open");
    $("body").css('padding-right', '');
	$('#shipping-page-suggested-address').modal('hide');*/
}
function paymentAddressFromOverlay() {
	suggestedShippingAddressSelect('1');
	$("#paymentAddressFromSuggested").click();
}

function placeOrderAddressFromOverlay() {
	suggestedShippingAddressSelect('1');
	$("#placeOrderAddressFromSuggested").click();
}

function updateShippingPrices() {
	//	alert("In updateShippingPrices");
	var state = $("#state option:selected").attr("value");
	var address1 = $("#address1").val();
	var city = $("#city").val();
	var isNewAddressForm = $("#isNewAddressForm").val();
	$.ajax({
		url: window.contextPath + "checkout/shipping/singleShippingMethodFrag.jsp",
		async: false,
		data: { address1: address1, state: state, isNewAddressForm: isNewAddressForm, city: city },
		success: function (data) {
			$("#select-shipping-method-row-id").html(data);
			detailsPopover();
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log(errorThrown)
		}
	});
}


/*$('#address1').change(function () {
	alert("address1 changed");
	updateShipMethodsZone();
});*/


/* Start :Method for updatting the shipping methods zone on change of the address1 ,city and state  values*/
function updateShipMethodsZone() {
	try {
		var state = $("#state option:selected").attr("value");
		var address1 = $("#address1").val();
		var city = $("#city").val();
		var showMeGiftingOptions = $("#gift-options-checkbox").hasClass("checkout-flow-radio-selected");
		$('.select-shipping-method-block').removeClass('selected');
		$('.select-shipping-method-block .checkout-flow-sprite').removeClass('shopping-cart-gift-checkbox-on');
		$('.select-shipping-method-block .checkout-flow-sprite').addClass('shopping-cart-gift-checkbox-off');
		if (!$(this).hasClass("selected")) {
			$(this).addClass("selected");
		}
		$(this).find('.checkout-flow-sprite').removeClass('shopping-cart-gift-checkbox-off').addClass('shopping-cart-gift-checkbox-on');
		$.ajax({
			url: window.contextPath + "checkout/intermediate/shippingAjaxFormSubmit.jsp",
			data: { action: "changeShipMethodZone", state: state, address1: address1, city: city },
			success: function (data) {
				// refreshing order summary to display order shipping price
				$.ajax({
					url: window.contextPath + "checkout/common/checkoutOrderSummary.jsp",
					data: { price: '0.0', pageName: 'Shipping', showMeGiftingOptions: showMeGiftingOptions },
					success: function (data) {
						var refreshedData = $(data).find('.summary-block-border').html();
						if (refreshedData) {
							if ($(refreshedData).find("#load-checkout-norton-script").length) {
								$(refreshedData).find("[id=load-checkout-norton-script]").html($("#loadNortonHiddenDiv").html());
							}
							$("#order-summary-block").find('.summary-block-border').html(refreshedData);
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == '409') {
							cancelExitBrowserConfimation();
							var contextPath = $(".contextPath").val();
							location.href = contextPath + 'cart/shoppingCart.jsp';
						}
					}
				});

				// refreshing shipping method block to display effective
				// prices(after promotions applied)
				var formId = $("#selectedShippingAddress");
				if (formId.length) {
					continueToCheckout('shipping-tab');
				} else {
					updateShippingPrices();
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					cancelExitBrowserConfimation();
					var contextPath = $(".contextPath").val();
					//	  alert(contextPath);
					location.href = contextPath + 'cart/shoppingCart.jsp';
				}
			}
		});
	} catch (e) {

	}

}
/* End :Method for updatting the shipping methods zone on change of the address1 /state  */





jQuery(document).ready(function ($) {
	$(document).on('click', '.select-shipping-method-block', function (e) {
		// display checkbox logic
		if ($(this).hasClass("selected")) {
			return false;
		}
		else {
			var showMeGiftingOptions = $("#gift-options-checkbox").hasClass("checkout-flow-radio-selected");
			$('.select-shipping-method-block').removeClass('selected');
			$('.select-shipping-method-block>div').removeClass('shopping-cart-gift-checkbox-on');
			$('.select-shipping-method-block>div').addClass('shopping-cart-gift-checkbox-off');
			if (!$(this).hasClass("selected")) {
				$(this).addClass("selected");
			}
			$(this).find('div').first().removeClass('shopping-cart-gift-checkbox-off').addClass('shopping-cart-gift-checkbox-on');

			// update shipping price, shipping method to order
			var shipMethodCode = $(this).find('.shipMethodCode').val();
			var shippingPrice = $(this).find('.shippingPrice').val();
			var regionCode = $(this).find('.regionCode').val();
			$.ajax({
				url: window.contextPath + "checkout/common/shipMethodAction.jsp",
				async: false,
				data: { shipMethodCode: shipMethodCode, shippingPrice: shippingPrice, regionCode: regionCode },
				success: function (data) {
					// refreshing order summary to display order shipping price
					$.ajax({
						url: window.contextPath + "checkout/common/checkoutOrderSummary.jsp",
						data: { price: shippingPrice, pageName: 'Shipping', showMeGiftingOptions: showMeGiftingOptions },
						success: function (data) {
							var refreshedData = $(data).find('.summary-block-border').html();
							if (refreshedData) {
								$("#order-summary-block").find('.summary-block-border').html(refreshedData);
							}
						},
						error: function (xhr, ajaxOptions, thrownError) {
							if (xhr.status == '409') {
								cancelExitBrowserConfimation();
								var contextPath = $(".contextPath").val();
								location.href = contextPath + 'cart/shoppingCart.jsp';
							}
						}
					});

					// refreshing shipping method block to display effective
					// prices(after promotions applied)
					/*var formId = $("#selectedShippingAddress");
					if (formId.length) {
						continueToCheckout('shipping-tab');
					} else {
						updateShippingPrices();
					}*/
					updateShippingPrices();
					checkForOrderFinancing();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					console.log(errorThrown)
				}
			});
		}

	});

	var relationshipId;
	$('.checkout-review-product-block.gift-wrapped .edit>a:nth-child(3)').click(function () {
		relationshipId = $("#relationshipId").val();
		$('#removeItemConfirmationModal').modal('show');

	})

	$("#removeItemConfirmationModal a:nth-of-type(1)").click(function () {
		$('[data-dismiss="modal"]').click();
	});

	$("#removeItemConfirmationModal a:nth-of-type(2)").click(function () {

		$.ajax({
			url: "/checkout/intermediate/cartAjaxFormSubmit.jsp",
			type: "POST",
			data: { relationshipId: relationshipId, formID: 'removeCartItemFromOrderReview' },
			success: function (data) {
				if (data.indexOf('Following are the form errors:') > -1) {
					var msg = data.split('Following are the form errors:')[1];
					$('.checkout-review-product-block.gift-wrapped').prepend('<span class="errorDisplay">' + msg + '</span>');
				} else if (data.indexOf('No Items in cart') > -1) {
					cancelExitBrowserConfimation();
					window.location.href = window.contextPath + "cart/shoppingCart.jsp";
				} else {
					$('.checkout-sticky-top').replaceWith(data);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					cancelExitBrowserConfimation();
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'cart/shoppingCart.jsp';
				}
			}
		});
		$('#removeItemConfirmationModal').modal('hide');
	})

});

function isEmailAddress(email) {
	var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
	return pattern.test(email);
}

function testing($div) {
	if ($div.find(".errorDisplay").length) {
		$div.find(".errorDisplay").remvoe();
	}
	$div.append("<span class='errorDisplay' >Please select an address</span>");
}

function removeErrorMsgAndHighlight() {
	$('.global-error-display').text('');
	$('.error-highlight').removeClass('error-highlight');
}
function onSubmitShippingAddCheckout(previousTab, actionType) {

	$.ajax({
		url: window.contextPath + "checkout/common/checkSession.jsp",
		type: "POST",
		dataType: "json",
		cache: false,
		success: function (redirectData) {
			$('.errorDisplay').remove();
			$('.checkout-error-state').hide();
			removeErrorMsgAndHighlight();
			if ($('#saveShippingAddress').length > 0 && actionType != 'moveToMultipleShip' && !$('#saveShippingAddress').valid()) {
				return false;
			}
			var multishipping = $(".checkout-multiple-shipping");
			if (multishipping.length > 0) {

				$('select[name="shipToAddressName"]').filter(function () {
					var selectedAddr = $(this).val();
					if (selectedAddr == 'please select' || selectedAddr == null || selectedAddr == '') {
						testing($(this).parent('div'));
						return true;

					}

				}).addClass('error')

				if ($('.ship-to-dropdown.error').length > 0) return false;

			}
			var showMeGiftingOptions = $("#gift-options-checkbox").hasClass("checkout-flow-radio-selected");
			$('#showMeGiftingOptions').val(showMeGiftingOptions);
			var billingAddressSameAsShippingAddress = $("#same-billing-address-checkbox").hasClass("checkout-flow-radio-selected");
			$('#billingAddressSameAsShippingAddress').val(billingAddressSameAsShippingAddress);
			var isOrderInStore = $('#isOrderInstorePickup').val();
			var formId = $("#selectedShippingAddress");

			var multishipping = $(".checkout-multiple-shipping");
			if (multishipping.length) {
				$.ajax({
					url: window.contextPath + "checkout/intermediate/shippingAjaxFormSubmit.jsp",
					type: "POST",
					data: { action: "shipToMultiAddress" },
					dataType: "json",
					async: false,
					cache: false,
					success: function (data) {
						var successData = data;
						if (successData.success) {
							if (previousTab != null && previousTab != '') {
								continueToCheckout(previousTab);
								$("html").removeClass("modal-open");
							} else if (showMeGiftingOptions == true) {
								continueToCheckout('gifting-tab');
								$("html").removeClass("modal-open");
							} else if (showMeGiftingOptions == false && isOrderInStore == 'true') {
								continueToCheckout('pickup-tab');
								$("html").removeClass("modal-open");
							} else if (showMeGiftingOptions == false) {
								continueToCheckout('payment-tab');
								$("html").removeClass("modal-open");
							} else {
								continueToCheckout('shipping-tab');
								$("html").removeClass("modal-open");
							}
							ajaxCallForTealiumCheckout(window.contextPath);
						} else {
							// Need to display the error msgs
							// continueToCheckout( window.contextPath + 'checkout/shipping/shipping.jsp','shipping-tab');
							$('.errorDisplay').remove();
							var shipRestrictionsFound = (typeof successData.shipRestrictionsFound != 'undefined') ? successData.shipRestrictionsFound : 'false';
							$('#shipRestrictionsFound').val(shipRestrictionsFound);
							var errorMsg = [];
							var errorCodes = [];
							$.each(successData.errorMessages, function (key, val) {
								errorMsg.push('<li>' + val.message + '</li>');
								errorCodes.push('#' + val.divId + ' select');
							});
							//var htmlParsed = '<span><span class="errorDisplay">' + errorMsg.join(' ') + '</span></span>';
							var htmlParsed = $.parseHTML(errorMsg.join(' '));
							showCheckoutOopsError(htmlParsed);
							$(window).scrollTop(0);
							$(errorCodes.join(', ')).addClass('error-highlight');
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == '409') {
							cancelExitBrowserConfimation();
							var contextPath = $(".contextPath").val();
							location.href = contextPath + 'cart/shoppingCart.jsp';
						}
					}

				});
			} else if (formId.length) {
				var shippingMethod = $('.select-shipping-method-block.selected').find('.shipMethodCode').val();
				var shippingPrice = $('.select-shipping-method-block.selected').find('.shippingPrice').val();
				var shipToAddressName = $("#selectedShippingAddress option:selected").attr("value");

				if (shippingMethod === undefined) {
					shippingMethod = "";
				}

				if (shippingPrice === undefined) {
					shippingPrice = "0.0";
				}

				$("#shippingMethod").val(shippingMethod);
				$("#shippingPrice").val(shippingPrice);
				$.ajax({
					url: window.contextPath + "checkout/intermediate/shippingAjaxFormSubmit.jsp",
					type: "POST",
					data: {
						action: "shipToExistingAddress", shipToAddressName: shipToAddressName, shippingMethod: shippingMethod,
						shippingPrice: shippingPrice, showMeGiftingOptions: showMeGiftingOptions, moveToShippingAction: actionType
					},
					dataType: "json",
					async: false,
					cache: false,
					success: function (data) {
						var successData = data;
						if (successData.success) {
							if (actionType == "moveToMultipleShip") {
								continueToCheckout('shipping-tab');
								$("html").removeClass("modal-open");
							} else if (previousTab != null && previousTab != '') {
								continueToCheckout(previousTab);
								$("html").removeClass("modal-open");
								$("body").css('padding-right', '');
							} else if (showMeGiftingOptions == true) {
								continueToCheckout('gifting-tab');
								$("html").removeClass("modal-open");

							} else if (showMeGiftingOptions == false && isOrderInStore == 'true') {
								continueToCheckout('pickup-tab');
								$("html").removeClass("modal-open");
							} else if (showMeGiftingOptions == false) {
								continueToCheckout('payment-tab');
								$("html").removeClass("modal-open");
								$("body").css('padding-right', '');
							} else {
								continueToCheckout('shipping-tab');
								$("html").removeClass("modal-open");
								$("body").css('padding-right', '');
							}
							ajaxCallForTealiumCheckout(window.contextPath);
						} else {
							$('.errorDisplay').remove();

							var shipRestrictionsFound = (typeof successData.shipRestrictionsFound != 'undefined') ? successData.shipRestrictionsFound : 'false';
							$('#shipRestrictionsFound').val(shipRestrictionsFound);
							var errorMsg = [];
							var errorCodes = [];
							$.each(successData.errorMessages, function (key, val) {
								if (val.message) {
									errorMsg.push('<li>' + val.message + '</li>');
								}
								errorCodes.push('#' + val.divId);
							});
							var htmlParsed = $.parseHTML(errorMsg.join(' '));

							if (successData.orderReconciliationNeeded == "true") {
								continueToCheckout('shipping-tab');
								$(errorCodes.join(', ')).addClass('error-highlight');
								$("html").removeClass("modal-open");
								$('.checkout-multiple-shipping-content .global-error-display').html(htmlParsed);
								//htmlParsed = 'tru_informational_error' + htmlParsed;									
								//addBackendErrorsToForm( htmlParsed, $('#checkout-container'));
							} else {
								showCheckoutOopsError(htmlParsed);
							}
							$(window).scrollTop(0);
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == '409') {
							cancelExitBrowserConfimation();
							var contextPath = $(".contextPath").val();
							location.href = contextPath + 'cart/shoppingCart.jsp';
						}
					}

				});
			} else {
				$('.errorDisplay').remove();
				var shippingMethod = $('.select-shipping-method-block.selected').find('.shipMethodCode').val();
				var shippingPrice = $('.select-shipping-method-block.selected').find('.shippingPrice').val();
				$("#shippingMethod").val(shippingMethod);
				$("#shippingPrice").val(shippingPrice);
				var contextPath = $(".contextPath").val();
				var isOrderInStore = $('#isOrderInstorePickup').val();
				$.ajax({
					type: "POST",
					async: false,
					url: window.contextPath + "checkout/intermediate/shippingAddressAjaxFormSubmit.jsp",
					data: $("#saveShippingAddress").serialize() + '&formID=addShippingAddress&action=' + actionType,
					success: function (data) {
						if (data.indexOf('Following are the form errors:') > -1) {
							//To display the error message
							//	updateShipMethodsZone();
							/*$('#saveShippingAddress').prepend( data.split('Following are the form errors:')[1] );*/
							addBackendErrorsToForm(data, $('#saveShippingAddress'));
						} else if (data.indexOf('orderReconciliation_DIV:') > -1) {
							try {
								var reconciliation_errorJson = JSON.parse(($.trim(data)).split('orderReconciliation_DIV:')[1]);
								var errorMsg = [];
								var errorCodes = [];
								$.each(reconciliation_errorJson.errorMessages, function (key, val) {
									errorMsg.push('<li>' + val.message + '</li>');
									errorCodes.push('#' + val.divId);
								});
								var htmlParsed = $.parseHTML(errorMsg.join(' '));
								if (reconciliation_errorJson.orderReconciliationNeeded == "true") {
									continueToCheckout('shipping-tab');
									$(errorCodes.join(', ')).addClass('error-highlight');
									$("html").removeClass("modal-open");
									$('.checkout-multiple-shipping-content .global-error-display').html(htmlParsed);
								} else {
									showCheckoutOopsError(htmlParsed);
								}
								$(window).scrollTop(0);
							} catch (e) { }
							ajaxCallForTealiumCheckout(window.contextPath);
						} else {
							if (data.indexOf('address-doctor-overlay-from-ajax:') > -1) {
								$('#shipping-page-suggested-address .modal-dialog-shipping-page-suggested').html(data.split('address-doctor-overlay-from-ajax:')[1]);
								$('#shipping-page-suggested-address').modal('show');
							} else if (data.indexOf('address-saved-success-from-ajax:') > -1) {
								if (actionType == "moveToMultipleShip") {
									continueToCheckout('shipping-tab');
								} else {
									var showGiftOptions = $("#gift-options-checkbox").hasClass("checkout-flow-radio-selected");
									if (showGiftOptions == 'true' || showGiftOptions == true) {
										continueToCheckout('gifting-tab');
									} else if (showMeGiftingOptions == false && isOrderInStore == 'true') {
										continueToCheckout('pickup-tab');
										$("html").removeClass("modal-open");
									} else {
										continueToCheckout('payment-tab');
										$("html").removeClass("modal-open");
									}
								}
								ajaxCallForTealiumCheckout(window.contextPath);
							}
						}
					},
					dataType: "html",
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == '409') {
							cancelExitBrowserConfimation();
							var contextPath = $(".contextPath").val();
							location.href = contextPath + 'cart/shoppingCart.jsp';
						} else if (xhr.status == '0') {
							cancelExitBrowserConfimation();
							var contextPath = $(".contextPath").val();
							location.href = contextPath + 'cart/shoppingCart.jsp';
						}
					}
				});

				return false;
			}
			checkForZeroTotal();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409' || xhr.status == '200') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
			return true;
		}
	});



}

function onSubmitShippingSuggestedOverlay() {
	$('.errorDisplay').remove();
	var contextPath = $(".contextPath").val();
	var action = $("#action").val();
	var isOrderInstorePickup = $("#isOrderInstorePickup").val();
	// value for checking the request for paypal payment and forward to review
	// Page
	var isOrderPayPalPayment = $("#isOrderPayPalPayment").val();
	$.ajax({
		type: "POST",
		async: false,
		url: window.contextPath + "checkout/intermediate/shippingAddressAjaxFormSubmit.jsp",
		data: $("#saveShippingAddressFromSuggested").serialize() + '&formID=addShippingAddFromSuggestedOverlay',
		success: function (data) {
			if (data.indexOf('Following are the form errors:') > -1 && !(data.indexOf('address-saved-success-from-ajax:orderReconciliationNeeded:') > -1)) {
				//$('#saveShippingAddress').prepend( data.split('Following are the form errors:')[1] );//change the error message div to the div of ovelay
				var taxwareVal = $.trim(data.split('Following are the form errors:')[1]);
				if (taxwareVal.indexOf('taxware:') > -1) {
					addBackendErrorsToForm(data.replace('taxware:', ''), $('#shipping-page-suggested-address .addressDoctor'));
				} else {
					/*Start: Added as part of MVP-97 Changes */
					var shipRestrictionsFound = $(data.split('Following are the form errors:')[1]).find('#shipRestrictionsFoundTemp').wrap();
					$('#shipRestrictionsFound').val(shipRestrictionsFound.val());
					/*END: Added as part of MVP-97 Changes */
					addBackendErrorsToForm(data, $('#co-shipping-tab'));
					$('#shipping-page-suggested-address').modal('hide');
				}

			} else {
				if (data.indexOf('address-doctor-overlay-from-ajax:') > -1) {
					$('#shipping-page-suggested-address .modal-dialog-shipping-page-suggested').html(data.split('address-doctor-overlay-from-ajax:')[1]);
					$('#shipping-page-suggested-address').modal('show');
				} else if (data.indexOf('address-saved-success-from-ajax:') > -1) {
					if (action == "moveToMultipleShip") {
						continueToCheckout('shipping-tab');
						if (data.indexOf('address-saved-success-from-ajax:orderReconciliationNeeded:') > -1) {
							var reconciliation_errorJson = JSON.parse(($.trim(data)).split('orderReconciliation_DIV:')[1]);
							var errorMsg = [];
							var errorCodes = [];
							$.each(reconciliation_errorJson.errorMessages, function (key, val) {
								errorMsg.push('<li>' + val.message + '</li>');
								errorCodes.push('#' + val.divId);
							});
							var htmlParsed = errorMsg.join(' ');
							htmlParsed = $.parseHTML(htmlParsed);
							if (reconciliation_errorJson.orderReconciliationNeeded == "true") {
								continueToCheckout('shipping-tab');
								$(errorCodes.join(', ')).addClass('error-highlight');
								$("html").removeClass("modal-open");
								//htmlParsed = 'tru_informational_error' + htmlParsed;
								$('.checkout-multiple-shipping-content .global-error-display').html(htmlParsed);
							} else {
								showCheckoutOopsError(htmlParsed);
							}
						}
					} else {
						if (data.indexOf('address-saved-success-from-ajax:orderReconciliationNeeded:') > -1) {
							var reconciliation_errorJson = JSON.parse(($.trim(data)).split('orderReconciliation_DIV:')[1]);
							var errorMsg = [];
							var errorCodes = [];
							$.each(reconciliation_errorJson.errorMessages, function (key, val) {
								errorMsg.push('<li>' + val.message + '</li>');
								errorCodes.push('#' + val.divId);
							});
							var htmlParsed = errorMsg.join(' ');
							htmlParsed = $.parseHTML(htmlParsed);
							if (reconciliation_errorJson.orderReconciliationNeeded == "true") {
								continueToCheckout('shipping-tab');
								$(errorCodes.join(', ')).addClass('error-highlight');
								$("html").removeClass("modal-open");
								$('.checkout-multiple-shipping-content .global-error-display').html(htmlParsed);
							} else {
								showCheckoutOopsError(htmlParsed);
							}
						} else {
							var showGiftOptions = $.trim(data.split('address-saved-success-from-ajax:')[1]);
							if (isOrderPayPalPayment == 'true') {
								showGiftOptions = 'false';
							}
							var previousTabValue = $('#previousTabValue').val() || '';

							if (showGiftOptions == 'true' || showGiftOptions == true) {
								continueToCheckout('gifting-tab');
							} else if (showGiftOptions == 'false' && isOrderInstorePickup == 'true') {
								continueToCheckout('pickup-tab');
							} else if (showGiftOptions == 'false' && isOrderPayPalPayment == 'true') {
								continueToCheckout('review-tab');
							} else if (previousTabValue != '') {
								continueToCheckout(previousTabValue);
							} else if (showGiftOptions == 'false') {
								continueToCheckout('payment-tab');
							}
						}
					}
				}
				ajaxCallForTealiumCheckout(window.contextPath);
			}
		},
		dataType: "html",
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
	checkForZeroTotal();
	return false;
}

function onSubmitPaymentSuggestedOverlay() {
	removeBackendErrorMessagesInline($("#co-payment-tab"));
	$.ajax({
		type: "POST",
		async: false,
		dataType: "html",
		url: window.contextPath + "checkout/intermediate/billingAjaxFormSubmit.jsp",
		data: $("#savePaymentAddressFromSuggested").serialize() + '&formID=paymentCreditCardForm',
		success: function (data) {
			if (data.indexOf('Following are the form errors:') > -1 && !(data.indexOf('address-saved-success-from-ajax:orderReconciliationNeeded:') > -1)) {
				addBackendErrorsToForm(data, $("#co-payment-tab"));
			} else {
				continueToCheckout('review-tab');
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});

	var addressRadioButtons = $(".address-doctor").find("input[type='radio']:checked");
	if (addressRadioButtons.length > 0) {
		$('#payment-page-suggested-address').modal('hide');
	}

	return false;
}

function onPlaceOrderPaymentSuggestedOverlay() {
	removeBackendErrorMessagesInline($("#co-payment-tab"));
	$.ajax({
		type: "POST",
		async: false,
		dataType: "html",
		url: window.contextPath + "checkout/intermediate/commitOrderAjaxFormSubmit.jsp",
		data: $("#savePlaceOrderAddressFromSuggested").serialize() + '&formID=placeOrderCreditCardForm',
		success: function (data) {
			if (data.indexOf('Following are the form errors:') > -1 && !(data.indexOf('address-saved-success-from-ajax:orderReconciliationNeeded:') > -1)) {
				$.ajax({
					url: window.contextPath + "checkout/common/emptyCartRedirect.jsp",
					type: "POST",
					dataType: "json",
					async: false,
					cache: false,
					success: function (redirectData) {
						var successData = redirectData;
						if (successData.redirect == "true" || successData.redirect == true) {
							cancelExitBrowserConfimation();
							window.location.href = window.contextPath + "cart/shoppingCart.jsp";
						} else {
							if (typeof successData.validationFailed != 'undefined' && successData.validationFailed == true) {
								continueToCheckout('review-tab');
							} else {
								continueToCheckout('payment-tab');
								/*$('#co-payment-tab').prepend( data.split('Following are the form errors:')[1] );*/
								if ($('#radio-rus-creditcard').is(':checked')) {
									/*addError('paymentRusCardForm');*/
									addBackendErrorsToForm(data, $('#paymentRusCardForm'));
								} else if ($('#radio-creditcard').is(':checked')) {
									/*addError('paymentCreditCardForm');*/
									addBackendErrorsToForm(data, $('#paymentCreditCardForm'));
								} else {
									addBackendErrorsToForm(data, $('#payment-email-form'));
								}
							}
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == '409') {
							cancelExitBrowserConfimation();
							var contextPath = $(".contextPath").val();
							location.href = contextPath + 'cart/shoppingCart.jsp';
						}
					}
				});
			} else {
				window.location.href = window.contextPath + "checkout/confirm/orderConfirmation.jsp";
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});

	var addressRadioButtons = $(".address-doctor").find("input[type='radio']:checked");
	if (addressRadioButtons.length > 0) {
		$('#payment-page-suggested-address').modal('hide');
	}

	return false;
}

function editUserEnteredShippingAddress() {
	$('#shipping-page-suggested-address').modal('hide');
}
function editUserEnteredPaymentAddress() {
	suggestedShippingAddressSelect('0');
	$('#payment-page-suggested-address').modal('hide');
}

function suggestedShippingAddressSelect(id) {
	var count = $('input[name="valid-address"]:checked').val();
	if (typeof (count) != "undefined") {
		$("#suggestedAddress1Id").val($("#suggestedAddress1-" + count).val());//remove the parent form id
		$("#suggestedAddress2Id").val($("#suggestedAddress2-" + count).val());
		$("#suggestedCityId").val($("#suggestedCity-" + count).val());
		$("#suggestedStateId").val($("#suggestedState-" + count).val());
		$("#suggestedCountryId").val('US');
		$("#suggestedPostalCodeId").val($("#suggestedPostalCode-" + count).val());
		document.getElementById("continueWithSugestedAddr").disabled = false;
	}
}

function modifyAddressShippingOverlay() {
	$('.errorDisplay').remove();
	suggestedShippingAddressSelect('1');
	if ($('#myShippingAddressForm').valid()) {
		$("#submitAddressId").click();
	}
	return false;
}

function onSubmitShippingAddress() {
	$('#myShippingAddressForm .tse-scrollable .tse-content span.errorDisplay').remove();
	$('#myShippingAddressForm .tse-scrollable .tse-content div.successMessage').remove();
	if (!$('#myShippingAddressForm').valid()) {
		return false;
	}
	var refreshDiv = "";
	if ($("#shippingAddressFormFragment").length) {
		refreshDiv = "shippingAddressFormFragment";
	} else if ($("#creditCardAjaxFragment").length) {
		refreshDiv = "creditCardAjaxFragment";
	}
	$.ajax({
		type: "POST",
		url: window.contextPath + "checkout/intermediate/addressAjaxRequest.jsp",
		data: $("#myShippingAddressForm").serialize(),
		success: function (data) {
			if (data.indexOf('Following are the form errors:') > -1) {
				/*var msg = data.split('Following are the form errors:')[1];
				$('#myShippingAddressForm .tse-scrollable .tse-content').prepend('<span class="errorDisplay">' + msg + '</span>');
				$('#myAccountAddAddressModal .address-doctor-current').before('<span class="errorDisplay">' + msg + '</span>');*/
				if ($('#myAccountAddAddressModal .addressDoctor').length) {
					addBackendErrorsToForm(data, $("#myAccountAddAddressModal .addressDoctor"));
				} else if ($('#myAccountAddCreditCardModal .addressDoctor').length) {
					addBackendErrorsToForm(data, $("#myAccountAddCreditCardModal .addressDoctor"));
				} else {
					addBackendErrorsToForm(data, $('#myShippingAddressForm'));
				}
				$('.modal.fade.in .tse-scroll-content').scrollTop(0);
				//reInitScrollBar('addressOverlay', 320);
				//addError('myShippingAddressForm');
			} else {
				//$("#shippingAddressFormFragment").html(data);
				$('#' + refreshDiv).html(data);
				$('#myShippingAddressForm .tse-content').prepend('<div class="successMessage">Address Saved Successfully</div>');
				//$('#shippingAddressFormFragment .tse-scroll-content').scrollTop(0);
				$('#' + refreshDiv).find('.tse-scroll-content').scrollTop(0);
				//reInitScrollBar('addressOverlay', 320);
				// reInitValidation();
			}
			if ($("#shippingAddressFormFragment").length) {
				reInitScrollBar('addressOverlay', 320);
			} else if ($("#creditCardAjaxFragment").length) {
				reInitScrollBar('addressOverlay', 360);
			}

		},
		error: function (a, b, c) {
			console.log(a);
		},
		dataType: "html"
	});
	return false;
}

function onAddAddressInShipingOverlay() {
	var refreshDiv = "";
	if ($("#shippingAddressFormFragment").length) {
		refreshDiv = "shippingAddressFormFragment";
	} else if ($("#creditCardAjaxFragment").length) {
		refreshDiv = "creditCardAjaxFragment";
	}
	$.ajax({
		type: "POST",
		url: "/checkout/shipping/addressAddEditOverlay.jsp",
		success: function (data) {
			$('#' + refreshDiv).html(data);
			if ($("#shippingAddressFormFragment").length) {
				reInitScrollBar('my-account-add-address-overlay', 330);
			} else if ($("#creditCardAjaxFragment").length) {
				reInitScrollBar('my-account-add-address-overlay', 360);
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		},
		dataType: "html"
	});
}

$(document).on('click', '.checkoutEditAddress', function () {
	var refreshDiv = "";
	if ($("#shippingAddressFormFragment").length) {
		refreshDiv = "shippingAddressFormFragment";
	} else if ($("#creditCardAjaxFragment").length) {
		refreshDiv = "creditCardAjaxFragment";
	}
	var nickName = $(this).parent().find(".nickNameInHiddenSpan").text();
	$.ajax({
		type: "POST",
		data: { editNickName: nickName },
		dataType: "html",
		async: false,
		cache: false,
		url: window.contextPath + "checkout/shipping/addressAddEditOverlay.jsp",
		success: function (data) {
			//$("#shippingAddressFormFragment").html(data);
			$('#' + refreshDiv).html(data);
			reInitScrollBar('my-account-add-address-overlay', 330);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
});

function onAddBillingAddressInPayment() {
	//alert("-------------");
}

function editUserEnteredAddressFromShippingOverlay() {
	$("#suggestedAddress1Id").val($("#suggestedAddress1-0").val());
	$("#suggestedAddress2Id").val($("#suggestedAddress2-0").val());
	$("#suggestedCityId").val($("#suggestedCity-0").val());
	$("#suggestedStateId").val($("#suggestedState-0").val());
	$("#suggestedCountryId").val($("#suggestedCountry-0").val());
	$("#suggestedPostalCodeId").val($("#suggestedPostalCode-0").val());
	$("#addressValidated").val('false');
	$.ajax({
		type: "POST",
		url: window.contextPath + "checkout/shipping/addressAddEditOverlay.jsp?fromSuggestedAddress=true",
		data: $("#myShippingAddressForm").serialize(),
		success: function (data) {
			if ($("#shippingAddressFormFragment").length) {
				$("#shippingAddressFormFragment").html(data);
			} else if ($("#creditCardAjaxFragment").length) {
				$("#creditCardAjaxFragment").html(data);
			}

			reInitScrollBar('my-account-add-address-overlay', 330);
			// reInitValidation();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		},
		dataType: "html"
	});
}
function editUserEnteredAddressFromPaymentOverlay() {
	$("#suggestedAddress1Id").val($("#suggestedAddress1-0").val());
	$("#suggestedAddress2Id").val($("#suggestedAddress2-0").val());
	$("#suggestedCityId").val($("#suggestedCity-0").val());
	$("#suggestedStateId").val($("#suggestedState-0").val());
	$("#suggestedCountryId").val($("#suggestedCountry-0").val());
	$("#suggestedPostalCodeId").val($("#suggestedPostalCode-0").val());
	$("#addressValidated").val('false');
	$.ajax({
		type: "POST",
		url: window.contextPath + "checkout/payment/creditCard/checkoutOverlay.jsp?fromSuggestedAddress=true",
		data: $("#addOrEditCreditCard").serialize(),
		success: function (data) {
			$("#creditCardAjaxFragment").html(data);
			// reInitValidation();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		},
		dataType: "html"
	});
}

$(document).on('click', '.setAsDefaultCheckout', function () {
	var refreshDiv = "";
	if ($("#shippingAddressFormFragment").length) {
		refreshDiv = "shippingAddressFormFragment";
	} else if ($("#creditCardAjaxFragment").length) {
		refreshDiv = "creditCardAjaxFragment";
	}
	var defaultAddNickName = $(this).parent().find('.nickNameInHiddenSpan').text();
	$.ajax({
		type: "POST",
		url: window.contextPath + "checkout/intermediate/shippingAddressAjaxFormSubmit.jsp",
		data: { defaultAddNickName: defaultAddNickName, formID: 'defaultAddress' },
		success: function (data) {
			//$("#shippingAddressFormFragment").html(data);
			$('#' + refreshDiv).html(data);
			$('#' + refreshDiv).find('.tse-scroll-content').scrollTop(0);
			reInitScrollBar('addressOverlay', 340);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		},
		dataType: "html"
	});
});

function onBillingAddressChangeOnPaymentPage() {
	var billingAddressNickName = $('#selectedCardBillingAddress option:selected').val();
	var isFirstOptionSelected = $('#selectedCardBillingAddress option').first().prop('selected');
	var isLastOptionSelected = $('#selectedCardBillingAddress option').last().prop('selected');
	//$('#paymentBillingAddressForm .checkoutEditBillingAddress').hide();
	if (isLastOptionSelected) {
		$('.checkoutEditBillingAddress_cancel').show();
	}
	//alert(billingAddressNickName);
	if (billingAddressNickName == 'addDropdownAddress') {
		$.ajax({
			type: "POST",
			url: "/checkout/payment/creditCard/newBillingAddress.jsp",
			data: { isEditBillingAddress: true, isFromPAymentPage: true },
			success: function (data) {
				$(".billing-info").append(data).addClass('billing-info-changed');
				$(".saved-billing-address").remove();
				$('#selectedCardBillingAddress').attr('disabled', true);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					cancelExitBrowserConfimation();
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'cart/shoppingCart.jsp';
				}
			},
			dataType: "html"
		});
	} else {
		if (isFirstOptionSelected == true) {
			$("#billing-address-container").hide();
			//$('.select-billing-address').parent().parent().parent().parent().parent().parent().find('.billing-address').hide();
			reInitScrollBar('billingAddressChangeTrue', 350);
		} else {
			$.ajax({
				type: "POST",
				async: false,
				url: window.contextPath + "checkout/intermediate/billingAjaxFormSubmit.jsp",
				data: { billingAddressNickName: billingAddressNickName, formID: 'changeBillingAddress' },
				success: function (data) {
					$.ajax({
						type: "POST",
						url: "/checkout/payment/creditCard/displayBillingAddress.jsp",
						data: { selectedAddressNickName: billingAddressNickName },
						success: function (data) {
							$(".billing-info").replaceWith(data);
						},
						error: function (xhr, ajaxOptions, thrownError) {
							if (xhr.status == '409') {
								cancelExitBrowserConfimation();
								var contextPath = $(".contextPath").val();
								location.href = contextPath + 'cart/shoppingCart.jsp';
							}
						},
						dataType: "html"
					});
				},
				dataType: "html"
			});
		}
	}
}

function onAddCardInPaymentOverlay() {
	$.ajax({
		type: "POST",
		url: "/checkout/payment/creditCard/checkoutOverlay.jsp",
		success: function (data) {
			$("#creditCardAjaxFragment").html(data);
			showHideBillingAddressInPayment();
			reInitScrollBar('onEditCreditCardOverlay', 300);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		},
		dataType: "html"
	});
}

$(document).on('click', '.checkoutEditBillingAddress', function () {
	$(this).hide();
	$('.checkoutEditBillingAddress_cancel').show();
	$.ajax({
		type: "POST",
		url: "/checkout/payment/billingAddressForm.jsp",
		data: { isEditBillingAddress: true, isFromPAymentPage: true },
		success: function (data) {
			$(".billing-info").append(data).addClass('billing-info-changed');
			$(".saved-billing-address").remove();
			$('#selectedCardBillingAddress').attr('disabled', true);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		},
		dataType: "html"
	});
});
/*Function for payment page billing address cr cancel address edit*/
$(document).on("click", ".checkoutEditBillingAddress_cancel", function () {
	var addressNickName = $("#selectedCardBillingAddress option:selected").val();
	$.ajax({
		type: "POST",
		url: "/checkout/payment/creditCard/displayBillingAddress.jsp",
		data: { addressNickName: addressNickName },
		success: function (data) {
			$(".billing-info").replaceWith(data);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		},
		dataType: "html"
	});
})
$(document).on('click', '.checkoutEditCreditCard', function () {
	var selectedAddressNickName = $(this).parent().find(".nickNameInHiddenSpan").text();
	var isEditBillingAddressWhenNoCard = true;
	$.ajax({
		type: "POST",
		url: window.contextPath + "checkout/payment/creditCard/checkoutOverlay.jsp",
		data: { isEditBillingAddressWhenNoCard: isEditBillingAddressWhenNoCard, selectedAddressNickName: selectedAddressNickName },
		async: false,
		success: function (data) {
			$("#creditCardAjaxFragment").html(data);
			reInitScrollBar('onEditCreditCardOverlay', 350);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		},
		dataType: "html"
	});
	//setTimeout(function () {
	var selectCountry = $('#country').val();
	if (selectCountry == 'US') {
		$('#zip').mask('AAAAA-AAAA');
	} else {
		$('#zip').mask('AAAAAAAAAA');
		var zipVlaue = $('#zip').val();
		if (zipVlaue != '' && $('#zip').hasClass('error')) {
			$('#zip').removeClass('error').next('#zip-Validation-error').remove();
		}
	}
	//},1000);
});

function showHideBillingAddressInPayment() {
	if ($('#addOrEditCreditCard #selectedCardBillingAddress').length > 0) {
		$('#addOrEditCreditCard .billing-address').hide();
	} else {
		$('#addOrEditCreditCard .billing-address').show();
	}
}

function updateGiftOptions() {
	var showMeGiftingOptions = $('#gift-options-checkbox').hasClass("checkout-flow-radio-selected");
	if (showMeGiftingOptions) {
		$('.checkout-nav-bottom-row .gifting-tab').show();
	} else {
		$('.checkout-nav-bottom-row .gifting-tab').hide();
	}
	$('#showMeGiftingOptions').val(showMeGiftingOptions);
	$.ajax({
		url: window.contextPath + "checkout/intermediate/giftingAjaxFormSubmit.jsp",
		type: "POST",
		data: { action: "updateGiftOptions", showMeGiftingOptions: showMeGiftingOptions },
		dataType: "json",
		cache: false,
		success: function (data) {
			var successData = data;
			if (successData.success) {
				$.ajax({
					type: "POST",
					url: window.contextPath + "checkout/common/checkoutOrderSummary.jsp",
					data: { showMeGiftingOptions: showMeGiftingOptions, pageName: 'Shipping' },
					dataType: "html",
					success: function (data3) {
						$("#order-summary-block .summary-block-border").html($(data3).find('.summary-block-border').html());
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == '409') {
							cancelExitBrowserConfimation();
							var contextPath = $(".contextPath").val();
							location.href = contextPath + 'cart/shoppingCart.jsp';
						}
					}
				});
			} else {
				// Need to display the error msgs
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				//	  alert(contextPath);
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
}

$(document).on('change', '.changeShippingMethod', function (e) {
	var tempWindowScroll = $(window).scrollTop();
	var _thisIndex = $(this).data('id');
	$.ajax({
		url: window.contextPath + "checkout/intermediate/shippingAjaxFormSubmit.jsp",
		type: "POST",
		data: $(this).closest("form").serialize() + '&action=changeShipMethodInReview',
		dataType: "json",
		async: false,
		cache: false,
		success: function (data) {
			var successData = data;
			// Both are same if and else part?
			if (successData.success) {
				continueToCheckout('review-tab');
			} else {
				continueToCheckout('review-tab');
			}
			$(window).scrollTop(tempWindowScroll);
			$(document).find('.changeShippingMethod[data-id=' + _thisIndex + ']').focus();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				//	  alert(contextPath);
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
});

function removeCartItemInReview() {
	var pageName = $('#pageName').val();
	var prodMerchCategory = $('#prodMerchCategory').val();
	var removeDonationItem = $('#removeDonationItemId').length ? $('#removeDonationItemId').val() : "";
	if (removeDonationItem == '') {
		var commerceItemIdAndQty = $('#removeItemId').val().split("_");
		var commerceItemId = commerceItemIdAndQty[0];
		var quantity = commerceItemIdAndQty[1];
		var updateOrRemove = commerceItemIdAndQty[2];
		var giftWrapSkuId = commerceItemIdAndQty[3];
		$.ajax({
			type: "POST",
			url: window.contextPath + "checkout/intermediate/cartAjaxFormSubmit.jsp?" + commerceItemId + "=" + quantity,
			data: { action: "removeCartItemInReview", commerceItemId: commerceItemId, quantity: quantity, updateOrRemove: updateOrRemove, giftWrapSkuId: giftWrapSkuId },
			dataType: "json",
			cache: false,
			success: function (data) {
				var successData = data;
				console.log(data);
				if (successData.success && successData.commerceItemCount == 0) {
					cancelExitBrowserConfimation();
					window.location.href = window.contextPath + "cart/shoppingCart.jsp";
				} else if (pageName == 'store-pickup' && successData.success && successData.inStoreSG == 'false') {
					continueToCheckout('payment-tab');
				} else if (successData.success) {
					if (pageName == 'Review') {
						continueToCheckout('review-tab');
						showSynchronyError();
					} else if (pageName == 'store-pickup') {
						continueToCheckout('pickup-tab');
					} else if (pageName == 'Shipping') {
						continueToCheckout('shipping-tab');
					}
					$("html").removeClass("modal-open");
					$("body").css('padding-right', '');
				} else {
					if (pageName == 'Review') {
						continueToCheckout('review-tab');
						showSynchronyError();
					} else if (pageName == 'store-pickup') {
						continueToCheckout('pickup-tab');
					} else if (pageName == 'Shipping') {
						continueToCheckout('shipping-tab');
					}
					$("html").removeClass("modal-open");
					$("body").css('padding-right', '');
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					cancelExitBrowserConfimation();
					var contextPath = $(".contextPath").val();
					//	  alert(contextPath);
					location.href = contextPath + 'cart/shoppingCart.jsp';
				}
			}
		});
		utag.link({
			'event_type': 'cart_remove',
			'product_merchandising_category': [],
			'product_id': [commerceItemId]
		});

		return false;
	} else {
		removeDonationItemFromReview(removeDonationItem);
	}
}

function isEmpty(val) {
	return (val === undefined || val == null || val.length <= 0) ? true : false;
}

function editCartItemInReview(productId, skuId, updateCommerItemIds, relationShipId, wrapSkuId, otherMetaInfoQty) {
	var pageName = $('#pageName').val();

	var quantity = $(".lineItemQtyEditCart").val();
	if (quantity != null && quantity.length != 0) {
		quantity = parseInt(quantity) + parseInt(otherMetaInfoQty);
	} else {
		$(".qtys-valid-text").show();
		return false;
	}
	$('html').removeClass("modal-open")
	$('body').removeAttr('style');

	var membershipIDInReview = '';
	if (pageName == 'Review') {
		membershipIDInReview = $('#enterMembershipIDInReview').val() || '';
	}
	$.ajax({
		type: "POST",
		url: window.contextPath + "checkout/intermediate/cartAjaxFormSubmit.jsp",
		data: { action: "editCartItemInReview", productId: productId, skuId: skuId, updateCommerItemIds: updateCommerItemIds, relationShipId: relationShipId, quantity: quantity, wrapSkuId: wrapSkuId, editCartFromPage: "Review" },
		dataType: "html",
		async: false,
		cache: false,
		success: function (data) {
			if (pageName == 'Review') {
				/*continueToCheckout('review-tab');
				$('#enterMembershipIDInReview').val(membershipIDInReview);
				showSynchronyError();*/
				if (data.indexOf('Following are the form errors:') > -1) {
					$.ajax({
						url: window.contextPath + "checkout/common/emptyCartRedirect.jsp",
						type: "POST",
						dataType: "json",
						async: false,
						cache: false,
						success: function (redirectData) {
							var successData = redirectData;
							if (successData.redirect == "true" || successData.redirect == true) {
								cancelExitBrowserConfimation();
								window.location.href = window.contextPath + "cart/shoppingCart.jsp";
							} else {
								continueToCheckout('review-tab');
								$('#enterMembershipIDInReview').val(membershipIDInReview);
								showSynchronyError();
								var inventoryLimitForReviewPage = $(data.split('Following are the form errors:')[1]).find('#inventoryLimitForReviewPage').val() || '';
								if(inventoryLimitForReviewPage !='' && inventoryLimitForReviewPage == 'true'){
									//Global Msg
									var htmlContent = data.split('Following are the form errors:')[1];
									var global_msg = $("<div/>").append(htmlContent).find('div.edit-review-tab-error').html();
									$('#checkout-container .global-error-display:first').after(global_msg);
									var relationShipId = $('#checkoutShipPageRelationShipId').val() || '';
									//Inline Msg
									var cartErrorMsg = $("<div/>").append(htmlContent).find('div.cart_inline_msg').html();
									var cart_inline_msg = "<div class='error'>"+cartErrorMsg+"</div>";
									if(cart_inline_msg != undefined) {
										$('#review-tab .product-information[id^="' + relationShipId + '"]').addClass('error');
										$('#review-tab .product-information[id^="' + relationShipId + '"]').find('.remove-product-item').after(cart_inline_msg);
									}
								}
							}
						},
						error: function (xhr, ajaxOptions, thrownError) {
							if (xhr.status == '409') {
								cancelExitBrowserConfimation();
								var contextPath = $(".contextPath").val();
								location.href = contextPath + 'cart/shoppingCart.jsp';
							}
						}
					});
				} else {
					continueToCheckout('review-tab');	
					$('#enterMembershipIDInReview').val(membershipIDInReview);
					showSynchronyError();
				}
			} /*else {
				$('.checkout-template #review_error_msg .checkout-confirmation-content').replaceWith(data);
			}*/

			/*var successData = data;
			console.log(data);
			if(successData.success){
				if (pageName == 'Review') {
					continueToCheckout('review-tab');
				} else if (pageName == 'store-pickup') {
					continueToCheckout('pickup-tab');
				} else if (pageName == 'Shipping') {
					continueToCheckout('shipping-tab');
				}
			}else{
				if (pageName == 'Review') {
					continueToCheckout('review-tab');
				} else if (pageName == 'store-pickup') {
					continueToCheckout('pickup-tab');
				} else if (pageName == 'Shipping') {
					continueToCheckout('shipping-tab');
				}
				// Need to display the error msgs
				$('.errorDisplay').remove();
				var errorMsg = [];
				var errorCodes = [];
				// for (i = 0; i < successData.errorMessages.length; i++) {
				$.each(successData.errorMessages, function(key, val){
					errorMsg.push(val.message);
					errorCodes.push( '.' + val.divId);
				});
				var decodeMsg = $("<div></div>").html( errorMsg.join(' '));
				var htmlParsed = '<span class="errorDisplay">' + decodeMsg.text() + '</span>';
				 $("#order-review-error").html($.parseHTML(htmlParsed));

			}*/
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				//	  alert(contextPath);
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});

	omniCartUpdate();
	return false;
}

function showSynchronyError() {
	var reviewPageSynchronyError = $('.reviewPageSynchronyError').html() || '';
	$('.reviewPageSynchronyError').remove();
	if (reviewPageSynchronyError.trim().length > 0) {
		addBackendErrorsToForm(reviewPageSynchronyError, $('#checkout-container'));
		//showCheckoutOopsError(reviewPageSynchronyError);	
	}
}


/*function defaultCard_checkout(creditCardNickName){
	$.ajax({
		type: "POST",
		url: window.contextPath + "checkout/intermediate/ajaxIntermediateRequest.jsp",
		data:{creditCardNickName:creditCardNickName,formID:'checkoutDefaultCreditCard'},
		success: function(data) {
			$("#creditCardAjaxFragment").html(data);
			reInitScrollBar('onEditCreditCardOverlay',350);
			if (creditCardNickName != "") {
				var coCreditCardVal = creditCardNickName.split("-");
				var creditCardType = coCreditCardVal[0].trim();
				tenderBasePromotionCheck(creditCardType);
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
		      if(xhr.status=='409'){
		    	  cancelExitBrowserConfimation();
		    	  var contextPath = $(".contextPath").val();
		          location.href = contextPath+'cart/shoppingCart.jsp';
		      }
		},
		dataType: "html"
	});
}*/

function deleteCard_checkout() {
	$("#removeCardId").click();
	return false;
}

/*function removeCard_checkout() {
    var nickName = $("#removeCardHiddenId").val();
    $.ajax({
           type: "POST",
           url:  window.contextPath + "checkout/intermediate/ajaxIntermediateRequest.jsp",
           data:{nickName:nickName,formID:'removeCard'},
           success: function(data) {
                  $("#creditCardAjaxFragment").html(data);
                  $('#addOrEditCreditCard .tse-scrollable .tse-scroll-content').prepend('<div class="successMessage">Card Removed Successfully</div>');
                  $('#myAccountCardDeleteModal [data-dismiss="modal"]').click();
				  $('#selectedCardBillingAddress').change();
                  reInitScrollBar('onEditCreditCardOverlay',350);
           },
           error: function(xhr, ajaxOptions, thrownError) {
			      if(xhr.status=='409'){
			    	  cancelExitBrowserConfimation();
			    	  var contextPath = $(".contextPath").val();
			          location.href = contextPath+'cart/shoppingCart.jsp';
			      }
			},
           dataType: "html"
    });
    return false;
}*/

$(document).on("click", ".editCreditCard_cancel", function () {
	$.ajax({
		type: "POST",
		url: window.contextPath + "checkout/payment/creditCard/creditCardFormFragment.jsp",
		data: { isCreditCardSelected: true },
		success: function (data) {
			$("#creditCardForm").html(data);
			$("#co-credit-card").msDropDown();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		},
		dataType: "html"
	});
})

$(document).on("change", "#co-credit-card", function () {
	if ($("#co-credit-card").length > 0) {
		if ($("#co-credit-card option:last").is(":selected")) {
			//$("#addAnotherCreditCardModal").click();
			$.ajax({
				type: "POST",
				url: window.contextPath + "checkout/payment/creditCard/newCreditCardForm.jsp",
				data: {},
				success: function (data) {
					$("#co-credit-card").attr('disabled', true);
					$("#co-credit-card_msdd").css('pointer-events', 'none');
					$("#saved_card_details").remove();
					$("#credit-card-info").html(data);
					$(".editCreditCard").hide();
					$(".editCreditCard_cancel").show();
					checkForFinancing();
				},
				error: function (e) {
					console.log(e);
				},
				dataType: "html"
			});
		} else {
			var creditCardNickName = $(this).val();
			var changeCreditCard = true;
			$('.errorDisplay').remove();
			var giftCardsExist = false;
			if ($('.gift-card-table').length > 0) {
				giftCardsExist = true;
			}
			$.ajax({
				type: "POST",
				async: false,
				url: window.contextPath + "checkout/payment/creditCard/savedCardFragment.jsp",
				data: { creditCardNickName: creditCardNickName, changeCreditCard: changeCreditCard, giftCardsExist: giftCardsExist, pageName: 'Payment' },
				success: function (data) {
					var expError = $(data).find('.expErrorMessage').html();
					var billInfo = $(data).find('.billingInfoContent').html();
					if (expError != '') {
						$('.checkout-payment-header').before(expError);
						$('.checkout-payment-header').prev(".errorDisplay").find("span").html($('.checkout-payment-header').prev(".errorDisplay").find("span").text());
					}
					$("#credit-card-info").html(billInfo);
					var priceSummary = $(data).find('.priceSummary .summary-block-padded-content').html();
					$('.summary-block-padded-content').html(priceSummary);
					if (giftCardsExist) {
						var giftCardList = $(data).find('.giftCardList').html();
						$('.gift-card-table').replaceWith(giftCardList);
					}
					var saved_card_details = $(data).find('#saved_card_details').html();
					$('#saved_card_details').html(saved_card_details);
					checkForFinancing();
					var coCreditCardVal = $("#co-credit-card option:selected").attr('data-coCreditCardVal');
					//tenderBasePromotionCheck(coCreditCardVal);
					loadGiftCardPromoCodeSection();
				},
				error: function (e) {
					console.log(e);
				},
				dataType: "html"
			});
		}
	}
});

function onCountryChangeInPayment() {
	var selectCountry = $('#country').val();
	$('#otherState').removeClass('display-inlineblock').val('');
	$.ajax({
		url: window.contextPath + 'myaccount/intermediate/ajaxIntermediateRequest.jsp?selectCountry=' + selectCountry + "&formID=changeCountry",
		dataType: 'html',
		success: function (data) {
			/* if($('#radio-rus-creditcard').is(":checked")){
				 $('#paymentRusCardForm #state').html( data );
				 if($('#paymentRusCardForm #state').hasClass("error")){
					 $('#paymentRusCardForm #state').valid();
				 }
			 }else if($('#radio-creditcard').is(":checked")){
				 $('#paymentCreditCardForm #state').html( data );
				 if($('#paymentCreditCardForm #state').hasClass("error")){
					 $('#paymentCreditCardForm #state').valid();
				 }
			 }*/
			$(".checkout-billing-address-form").find(".other-state").html(data);
			if (selectCountry != 'US') {
				$('input[id="otherState"]').addClass('display-inlineblock');
				$("#otherState").val("NA");
				$(".creditCardOtherStateLabel").addClass('display-inlineblock');
			} else {
				$('input[id="otherState"]').removeClass('display-inlineblock');
				$(".creditCardOtherStateLabel").removeClass('display-inlineblock');
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});

	if (selectCountry == 'US') {
		$('#zip').mask('AAAAA-AAAA');

	}
	else {
		$('#zip').mask('AAAAAAAAAA');
		var zipVlaue = $('#zip').val();
		if (zipVlaue != '' && $('#zip').hasClass('error')) {
			$('#zip').removeClass('error').next('#zip-Validation-error').remove();
		}
	}

}

function goodbye(e) {
	if (!e) e = window.event;
	e.cancelBubble = true;
	e.returnValue = '';
	e.returnValue = 'you are trying to leave during the checkout process';
	if (e.stopPropagation) {
		e.stopPropagation();
		e.preventDefault();
	}
}


function clearGiftCardText() {
	//setTimeout(function(){
	$('#giftCardApplicationForm input').val('');
	//},1000);
}
$(document).ready(function () {
	clearGiftCardText();
	detailsPopover();
	initTRUCheckoutComponents();
	/* Start popover for Economy details */
	/*
	 * var economyDetailInfo = $('.co-shipping-method-details'); var
	 * economyDetailInfoTemplate = '<div class="popover shippingDetailToolTip"
	 * role="tooltip"><div class="arrow"></div><div
	 * class="saved-address-header">Economy Shipping</div><p>Typically
	 * arrives in 6-9 business days.</p><p>Note</p><ul><li>Delivery times
	 * for oversized and pre-order items can be longer.</li><li>Allow 2
	 * additional days for the Pay in Store payment option.</li></ul></div>';
	 * economyDetailInfo.hover(function( event ) { event.preventDefault(); });
	 * economyDetailInfo.popover({ animation : 'true', html : 'true', content :
	 * 'asdfasdf', placement : 'top', container: $("body"), template :
	 * economyDetailInfoTemplate, trigger : 'hover',
	 *
	 * });
	 */
	/* End popover for Economy details */
	$(document).on('click', '.add-gift-wrap-checkbox', function () {

		giftWrapColumn = $(this).closest('.registry-product-content').find('.row .add-gift-wrap');// gift-wrap-border
		// border-visible

		if ($(this).hasClass('checkbox-sm-on')) {
			return;
		} else {
			giftWrapColumn.remove();
		}
	});

	if ($('#checkout-container').length > 0) {
		window.onbeforeunload = goodbye;
	}

	$(document).on('click', '#nongoodbye_shoppingCart, #nongoodbye_home, #nongoodbye_place_order_now, #nongoodbye_commitOrder, #checkout-tru-logo, #checkout-bru-logo', function (e) {

		cancelExitBrowserConfimation();

		var targetId = ($(e.currentTarget).attr('id') != undefined) ? ($(e.currentTarget).attr('id')) : '';
		var targetHasClass = $(e.currentTarget).hasClass('checkout-tru-logo') || $(e.currentTarget).hasClass('checkout-bru-logo');

		var turnOffLeaveWarning = false;
		turnOffLeaveWarning = $('#turnOffLeaveWarning').val();
		if (turnOffLeaveWarning != undefined && (turnOffLeaveWarning == false || turnOffLeaveWarning == 'false')) {
			if (targetId == "nongoodbye_shoppingCart" || targetId == "nongoodbye_home" || targetHasClass) {
				$('.checkoutExitConfirmationModal .yes-exit-checkout').attr('href', $(e.currentTarget).attr('href'));
				$('.checkoutExitConfirmationModal').modal('show');
				return false;
			}
		}
		return true;
	});

	$(document).on('click', '.no-continue-checkout', function () {
		$('.checkoutExitConfirmationModal').modal('hide');
	});
	$(document).on('click', '.co-shipping-method-details', function () {
		return false;
	});

	$(document).on('click', '.co-multi-shipping-method-details', function () {
		return false;
	});

	$(document).on('mouseover', '.co-multi-shipping-method-details', function () {
		$('.co-multi-shipping-method-details:last').addClass('lastItem');
		try {
			var contentId = $(this).parents('.ship-to-dropdown-container').find('.ship-to-dropdown').val();
			var popOverContent = $(this).parents('.ship-to-dropdown-container').find('.hidden_shipping_details.' + contentId + ' div.popover').html();
			var popoverTop = $('.co-multi-shipping-method-details').parents('.shipping-multiple-addresses').height() + 20;

			$('.co-multi-shipping-method-details').parent().find('.popover').hide();

			/*if($(this).hasClass('lastItem')){
				$(this).parent().find('.multiShippingDetailToolTip').html(popOverContent).removeClass('bottom').addClass('top').show();
				var thisPosition = $(".co-multi-shipping-method-details.lastItem").position();
				var tooltioHeight = $(this).parent().find('.multiShippingDetailToolTip').height();
				var setHeight = parseInt((thisPosition.top-tooltioHeight)-25 );
				$(this).parent().find('.multiShippingDetailToolTip').css('top', setHeight);
			}else{*/
			$(this).parent().find('.multiShippingDetailToolTip').html(popOverContent).css('top', popoverTop).show();
			/*}*/
		} catch (e) { }
	});

	$(document).on('mouseout', '.co-multi-shipping-method-details', function () {
		$('.co-multi-shipping-method-details').parent().find('.popover').hide();
	});
	/*js for savedcard dropdown image issue while refresh in paymentpage-chaitanya*/
	if ($("#co-credit-card").length >= -1) {
		setTimeout(function () {
			$("#co-credit-card").msDropDown();
		}, 3500);
	}
	/*js for savedcard dropdown image issue while refresh in paymentpage-chaitanya*/
});

/**
 * Start : JS for ship method DETAIL tooltip.
 */
function detailsPopover() {
	try {
		var isMultiShippingPag = $('#isMultiShippingPage').val();

		/* Start: Shipping method details "Standard" popover */
		var TRU_Std_Gnd;
		$(".co-shipping-method-details").on("mouseenter", function (e) {
			e.preventDefault();
			TRU_Std_Gnd = $('.popover.TRU_Std_Gnd').html();
			return true;

		});
		$('.co-shipping-method-details.TRU_Std_Gnd').popover({
			animation: 'true',
			html: 'true',
			content: function () {
				return TRU_Std_Gnd;
			},
			placement: 'top',
			container: $("body"),
			trigger: 'hover',
		});
		/* End: Shipping method details "Standard" popover */

		/* Start: Shipping method details "Economy" popover */
		var GROUND;
		$(".co-shipping-method-details").on("mouseenter", function (e) {
			e.preventDefault();
			GROUND = $('.popover.GROUND').html();
			return true;

		});
		$('.co-shipping-method-details.GROUND').popover({
			animation: 'true',
			html: 'true',
			content: function () {
				return GROUND;
			},
			placement: 'top',
			container: $("body"),
			trigger: 'hover',
		});
		/* End: Shipping method details "Economy" popover */

		/* Start: Shipping method details "Expedited" popover */
		var ANY_2Day;
		$(".co-shipping-method-details").on("mouseenter", function (e) {
			e.preventDefault();
			ANY_2Day = $('.popover.ANY_2Day').html();
			return true;

		});
		$('.co-shipping-method-details.ANY_2Day').popover({
			animation: 'true',
			html: 'true',
			content: function () {
				return ANY_2Day;
			},
			placement: 'top',
			container: $("body"),
			trigger: 'hover',
		});
		/* End: Shipping method details "Expedited" popover */

		/* Start: Shipping method details "Express" popover */
		var _1DAY;
		$(".co-shipping-method-details").on("mouseenter", function (e) {
			e.preventDefault();
			_1DAY = $('.popover.oneDAY').html();
			return true;

		});
		$('.co-shipping-method-details.oneDAY').popover({
			animation: 'true',
			html: 'true',
			content: function () {
				return _1DAY;
			},
			placement: 'top',
			container: $("body"),
			trigger: 'hover',
		});
		/* End: Shipping method details "Express" popover */

		/* Start: Shipping method details "Freight Carrier" popover */
		var TRUCK;
		$(".co-shipping-method-details").on("mouseenter", function (e) {
			e.preventDefault();
			TRUCK = $('.popover.TRUCK').html();
			return true;

		});
		$('.co-shipping-method-details.TRUCK').popover({
			animation: 'true',
			html: 'true',
			content: function () {
				return TRUCK;
			},
			placement: 'top',
			container: $("body"),
			trigger: 'hover',
		});
		/* End: Shipping method details "Freight Carrier" popover */

		/* Start: Shipping method details "USPS" popover */
		var USPS_PRIORITY;
		$(".co-shipping-method-details").on("mouseenter", function (e) {
			e.preventDefault();
			USPS_PRIORITY = $('.popover.USPS_PRIORITY').html();
			return true;

		});
		$('.co-shipping-method-details.USPS_PRIORITY').popover({
			animation: 'true',
			html: 'true',
			content: function () {
				return USPS_PRIORITY;
			},
			placement: 'top',
			container: $("body"),
			trigger: 'hover',
		});
		/* End: Shipping method details "USPS" popover */


	} catch (e) { }
}

/**
 * END : Tooltip
 */


function updateGiftItem(obj, shipGroupId, commerceItemRelationshipId, commerceItemId, selectedId, divSelectionId) {
	$(obj).toggleClass('checkbox-sm-off checkbox-sm-on');
	var giftItem = $(obj).hasClass("checkbox-sm-on");
	var giftWrapProductId;
	var giftWrapSkuId;
	// var giftItem = !($(obj).hasClass("checkbox-sm-on"));
	if (!giftItem) {
		giftWrapSkuId = $(obj).closest('.registry-product-content').find('.row .add-gift-wrap').find('.gift-wrap-border.border-visible').attr("id");
	}
	$.ajax({
		type: "POST",
		url: window.contextPath + "checkout/intermediate/giftingAjaxFormSubmit.jsp",
		data: {
			action: 'updateGiftItem', shipGroupId: shipGroupId, commerceItemRelationshipId: commerceItemRelationshipId,
			commerceItemId: commerceItemId, giftWrapProductId: giftWrapProductId, giftWrapSkuId: giftWrapSkuId, selectedId: selectedId, giftItem: giftItem
		},
		dataType: "html",
		success: function (data) {
			if (giftItem) {
				// Update the respective gift Wrap section
				$.ajax({
					type: "POST",
					url: window.contextPath + "checkout/gifting/ajaxGiftWrapEligibleItems.jsp",
					data: { isSplitGiftItems: true },
					dataType: "html",
					success: function (data2) {
						$("#" + divSelectionId).html(data2);
						// TO update the pricing section
						$.ajax({
							type: "POST",
							url: window.contextPath + "checkout/common/checkoutOrderSummary.jsp",
							data: { pageName: 'gifting' },
							dataType: "html",
							success: function (data3) {
								// TO update the pricing section
								$("#order-summary-block").html($(data3).html());
							},
							error: function (xhr, ajaxOptions, thrownError) {
								if (xhr.status == '409') {
									cancelExitBrowserConfimation();
									var contextPath = $(".contextPath").val();
									location.href = contextPath + 'cart/shoppingCart.jsp';
								}
							}
						});
					},
					error: function (e) {
						console.log(e);
					}
				});
			} else {
				$.ajax({
					type: "POST",
					url: window.contextPath + "checkout/common/checkoutOrderSummary.jsp",
					data: { pageName: 'gifting' },
					dataType: "html",
					success: function (data3) {
						// TO update the pricing section
						$("#order-summary-block").html($(data3).html());
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == '409') {
							cancelExitBrowserConfimation();
							var contextPath = $(".contextPath").val();
							location.href = contextPath + 'cart/shoppingCart.jsp';
						}
					}
				});
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				//	  alert(contextPath);
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
}

function applyGiftWrap(obj, shipGroupId, commerceItemRelationshipId, selectedId, commerceItemId,
	giftWrapProductId, giftWrapSkuId, defaultWrapProductId, defaultWrapSkuId, giftWrapSelected) {
	var previousGiftWrapSkuId = $(obj).closest('.registry-product-content').find('.gift-wrap-border.border-visible').attr("id");
	var giftWrapMark = 0;
	if ($(obj).hasClass('gift-wrap-none')) {
		giftWrapMark = $(obj).parents(".gift-wrap-border.border-visible").length;
	} else {
		giftWrapMark = $(obj).find(".gift-wrap-border.border-visible").length;
	}
	removeBackendErrorMessagesInline($("#checkout-container"));
	if (giftWrapMark < 1) {
		$.ajax({
			type: "POST",
			url: window.contextPath + "checkout/intermediate/giftingAjaxFormSubmit.jsp",
			async: false,
			data: {
				action: "applyGiftWrap", shipGroupId: shipGroupId, commerceItemRelationshipId: commerceItemRelationshipId, selectedId: selectedId,
				commerceItemId: commerceItemId, giftWrapProductId: giftWrapProductId, giftWrapSkuId: giftWrapSkuId, previousGiftWrapSkuId: previousGiftWrapSkuId, giftWrapSelected: giftWrapSelected
			},
			dataType: "html",
			success: function (data) {
				// TO update the gift wrap pricing section
				$.ajax({
					type: "POST",
					url: window.contextPath + "checkout/gifting/giftWrapPrice.jsp",
					data: { defaultWrapProductId: defaultWrapProductId, defaultWrapSkuId: defaultWrapSkuId },
					dataType: "html",
					success: function (data1) {
						$(obj).closest('.registry-product-content').find("#gift-wrap-price").html(data1);
						// TO update the pricing section
						$.ajax({
							type: "POST",
							url: window.contextPath + "checkout/common/checkoutOrderSummary.jsp",
							data: { pageName: 'gifting' },
							dataType: "html",
							success: function (data3) {
								$("#order-summary-block").html($(data3).html());
							},
							error: function (xhr, ajaxOptions, thrownError) {
								if (xhr.status == '409') {
									cancelExitBrowserConfimation();
									var contextPath = $(".contextPath").val();
									location.href = contextPath + 'cart/shoppingCart.jsp';
								}
							}
						});
						checkForOrderFinancing();
					},
					error: function (e) {
						console.log(e);
					}
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					cancelExitBrowserConfimation();
					var contextPath = $(".contextPath").val();
					//	  alert(contextPath);
					location.href = contextPath + 'cart/shoppingCart.jsp';
				}
			}
		});
	}
	else {
		return;
	}
}

function setCartTypeInOrder(cardType) {
	var giftCardsExist = false;
	if ($('.gift-card-table').length > 0) {
		giftCardsExist = true;
	}
	$.ajax({
		type: "POST",
		url: window.contextPath + "checkout/intermediate/billingAjaxFormSubmit.jsp",
		data: { formID: 'paymentType', pageName: 'Payment', cardType: cardType, giftCardsExist: giftCardsExist },
		async: false,
		dataType: "html",
		success: function (responseData) {
			var orderSummary = $(responseData).find('.orderSummary').html();
			$('#order-summary-block').replaceWith(orderSummary);
			if (giftCardsExist) {
				var giftCardList = $(responseData).find('.giftCardList').html();
				$('.gift-card-table').replaceWith(giftCardList);
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
}

function payInStoreDetailWindow(url) {
	popupWindow = window.open(
		url, 'popUpWindow', 'height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}

function forgotYourNumberWindow(url) {
	popupWindow = window.open(
		url, 'popUpWindow', 'height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}

function giftCardHelpWindow(url) {
	popupWindow = window.open(
		url, 'popUpWindow', 'height=450,width=600,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}

$(document).on('click', '.remove-promo-code-payment-page', function () {
	var couponInput = $(this).parent().find('.couponId');
	var couponId = (couponInput) ? couponInput.val() : '';
	var paymentMethod = "";
	if ($("#radio-paypal").is(":checked")) {
		paymentMethod = "PayPal";
	}
	$.ajax({
		type: "POST",
		cache: false,
		url: window.contextPath + "checkout/intermediate/ajaxIntermediateRequest.jsp",
		data: { couponId: couponId, formID: 'removeCouponFromPayment', paymentMethod: paymentMethod },
		async: false,
		dataType: "html",
		success: function (responseData) {
			$('#enter-promo-code-container').replaceWith(responseData);
			reloadOrderSummary('Payment', paymentMethod);
			reloadGiftCardSection();
			checkForOrderFinancing();
			checkForZeroTotal();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
	return false;
});


$(document).on('keypress', '#promo-code-payment-page', function (evt) {
	var promoKeyValue = evt.which || evt.keyCode;
	if (typeof promoKeyValue != undefined && promoKeyValue == 13 && $("#promo-code-payment-page").val().trim() != "") {
		showPromoCodePaymentPage();
	}
});
$(document).on('click', '#promo-code-payment-page-btn', function () {
	showPromoCodePaymentPage();
	checkForZeroTotal();
	return false;
});

function showPromoCodePaymentPage() {
	/* $('.checkOut-promocode-error-state, .promocode-error-msg').hide();
		$('#promo-code-payment-page').removeClass('error-highlight');*/
	removeBackendErrorMessagesInline($("#promoCodePaymentPageForm"));
	if (!$("#promoCodePaymentPageForm").valid()) {
		return false;
	}
	var couponCode = $('#promo-code-payment-page').val();
	var paymentMethod = "";
	if ($("#radio-paypal").is(":checked")) {
		paymentMethod = "PayPal";
	}
	$.ajax({
		type: "POST",
		url: window.contextPath + "checkout/intermediate/ajaxIntermediateRequest.jsp",
		data: { couponCode: couponCode, formID: 'applyCouponFromPayment', paymentMethod: paymentMethod },
		async: false,
		dataType: "html",
		success: function (responseData) {
			//Added for omniture integration
			if (responseData.indexOf('Following are the form errors:') > -1) {
				console.log('checkout promotion falure');
				/*var msg = responseData.split('Following are the form errors:')[1];
				$('.checkOut-promocode-error-state-subheader > span').html(msg);
				$('.checkOut-promocode-error-state').show();*/
				/*$('.promocode-error-msg').html(msg);
				$('#promo-code-payment-page').addClass('error-highlight');*/
				addBackendErrorsToForm(responseData, $('#checkout-container'));
				//Added for omniture integration
				omniCheckoutInvalidCoupon();
			}
			else {
				$('#enter-promo-code-container').replaceWith(responseData);
				console.log('checkout promotion success');
				omniCheckoutPromoSubmit();
			}
			reloadOrderSummary('Payment', paymentMethod);
			reloadGiftCardSection();
			checkForOrderFinancing();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
	return false;
}

function reloadOrderSummary(pageName, paymentMethod) {
	$.ajax({
		type: "POST",
		cache: false,
		async: false,
		dataType: "html",
		url: window.contextPath + 'checkout/common/checkoutOrderSummary.jsp',
		data: { pageName: pageName, paymentMethod: paymentMethod },
		success: function (orderSummaryResponseData) {
			$('#order-summary-block').replaceWith(orderSummaryResponseData);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
}
function reloadGiftCardSection() {
	$.ajax({
		type: "POST",
		cache: false,
		async: false,
		dataType: "html",
		url: window.contextPath + 'checkout/payment/giftCard.jsp',
		success: function (giftCardResponseData) {
			$('.checkout-payment-giftcard').replaceWith(giftCardResponseData);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
	/* Fix for Defect - GEOFUS-1096 */
	setTimeout(function(){
		clearPrePopulatedData();
	},1500);
	/* Fix for Defect - GEOFUS-1096 */
}
function reloadPromoCodeSection() {
	$.ajax({
		type: "POST",
		cache: false,
		async: false,
		dataType: "html",
		url: window.contextPath + 'checkout/payment/promotionCodes.jsp',
		success: function (promoCodeResponseData) {
			$('#enter-promo-code-container').replaceWith(promoCodeResponseData);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
}
function checkForZeroTotal() {
	var zeroOrderTotal = $(".zeroOrderTotal").val();

	if (zeroOrderTotal == 'true') {
		var selectedPayment = $("input[name='payment-type']:checked").attr("id");
		$(".checkout-payment").find('input[name="payment-type"]').prop('checked', false).attr('disabled', true);
		if ($("#previousSelectedPayment").length && typeof (selectedPayment) != 'undefined') {
			$("#previousSelectedPayment").val(selectedPayment);
		}
		else if (typeof (selectedPayment) != 'undefined') {
			var string = "<input type='hidden' id='previousSelectedPayment' value='" + selectedPayment + "'/>";
			$(".checkout-payment").append(string);
		}
		if ($(".paypal-order-message").css('display') == "block") {
			$(".paypal-order-message").slideUp();
		}
		else if ($('.credit-card-hidden').css('display') == "block") {
			$('.credit-card-hidden').slideUp();
		}
	}
	else {
		var preViousSelectedPayment = $("#previousSelectedPayment").val();
		if (typeof (preViousSelectedPayment) != "undefined") {
			$(".checkout-payment").find('input[name="payment-type"]').attr('disabled', false);
			if ($("#payinstoreDisabled").hasClass("preShow")) { $("#radio-payinstore").attr('disabled', true); }
			$("#" + preViousSelectedPayment).trigger("click");
		}
		else {
			if ($("#payinstoreDisabled").hasClass("preShow")) {
				$(".checkout-payment").find('input[name="payment-type"]').attr('disabled', false);
				$("#radio-payinstore").attr('disabled', true);
			}
			else {
				$(".checkout-payment").find('input[name="payment-type"]').attr('disabled', false);
			}
		}
	}
}
$(document).on("click", '#creditCardModal [data-dismiss="modal"]', function () {
	if ($("html").hasClass("modal-open")) {
		$("html").removeClass("modal-open");
		$("body").css('padding-right', '');
	}
});

$(document).on('click', '[data-target="#payment-page-apply-today"]', function () {
	//setTimeout(function(){
	$('#payment-page-apply-today .tse-scrollable').TrackpadScrollEmulator({
		wrapContent: false,
		autoHide: false
	});
	//}, 600);
});

$(document).ajaxComplete(function () {
	window.contextPath = $('.contextPath').val() || '/';
	try {
		clientValidation();
	}
	catch (e) { console.log(e); }
	clearPrePopulatedData();
	if ($('#creditCard_CVVmaskedPassword0').length == 0) {
		$("input#creditCard_CVV").maskPassword();
	}
	if ($('#creditCardCVVmaskedPassword0').length == 0) {
		$("input#creditCardCVV").filter(function () {
			return !$(this).parents('form#paymentRusCardForm').length;
		}).maskPassword();
	}

	var currentURL = location.href;

	if (currentURL.indexOf('checkout.jsp') > -1) {

		/* Start popover for not a member */
		var notMemberpopover = $('.notMemberpopover');

		var notMemberpopoverTemplate = '<div class="popover detailWrapper" role="tooltip"><div class="arrow"></div><p>simply signup on the reciept page and u will recieve credit for this order.</p></div>';

		notMemberpopover.hover(function (event) {
			event.preventDefault();
		});

		notMemberpopover.popover({
			animation: 'true',
			html: 'true',
			content: 'asdfasdf',
			placement: 'right',
			template: notMemberpopoverTemplate,
			trigger: 'hover'
		});
		/* End popover for not a member */

		/* Start popover for paypal details */
		var checkoutDdetailsPopover = $('.paypalDdetails');
		var paypalWhatsPayPal = $('.paypalWhatsPayPal').html();
		var checkoutDetailsPopoverTemplate = '<div class="popover detailWrapper paypalDdetailsWrapper" role="tooltip"><div class="arrow"></div><p>' + paypalWhatsPayPal + '</p></div>';

		checkoutDdetailsPopover.hover(function (event) {
			event.preventDefault();
		});

		checkoutDdetailsPopover.popover({
			animation: 'true',
			html: 'true',
			content: 'asdfasdf',
			placement: 'right',
			template: checkoutDetailsPopoverTemplate,
			trigger: 'hover'
		});
		/* End popover for paypal details */

		/* Start popover for terms of use details */
		var termOfUSeDetailsPopover = $('.termOfUSeDetails');

		var termOfUSeDetailsPopoverTemplate = '<div class="popover detailWrapper termOfUSeDetailsWrapper" role="tooltip"><div class="arrow"></div><div class="saved-address-header">Promotional Codes</div><p>Promotional codes need to be entered one at a time. Adding more than one promotional code can void promotions that have already been applied. Any promotional discounts will be reflected in the Order Details section on this page. You will need to re-enter your credit card information and Rewards"R"Us number after clicking the "Apply" button</p></div>';

		termOfUSeDetailsPopover.hover(function (event) {
			event.preventDefault();
		});

		termOfUSeDetailsPopover.popover({
			animation: 'true',
			html: 'true',
			content: 'asdfasdf',
			placement: 'right',
			template: termOfUSeDetailsPopoverTemplate,
			trigger: 'hover'
		});
		/* End popover for terms of use details */

		/* Start popover for learn More CreditCard */
		var learnMoreCreditCardPopover = $('.learn-more-credit-card');

		var learnMoreCreditCardPopoverTemplate = '<div class="popover seeTerms pickupLearnMoreTooltip fade bottom in" role="tooltip" id="popover275432" style="top: 51px; left: -75.9531px; display: block;"><div class="arrow" style="left: 46.5203%;"></div><p>Delivery of Your Item(s)The delivery date of your order depends on when the order leaves our warehouse, the destination address for the item and the shipping method. For select items, we offer an Estimated Delivery Date during checkout. An Estimated Delivery Date is a calculation of several factors, including item availability, processing time in our warehouse, the shipping method you select and the shipping destination. If you decide to use Pay In Store as the payment method, your order is not shipped until payment is made at a U.S. Toys"R"Us or Babies"R"Us store. Therefore, it is a good rule of thumb to add an additional 2 business days to the Estimated Delivery Dates displayed. Please note that Estimated Delivery Dates are estimates only, and are not guaranteed. </p></div>';

		learnMoreCreditCardPopover.click(function (event) {
			event.preventDefault();
		});

		learnMoreCreditCardPopover.popover({
			animation: 'true',
			html: 'true',
			content: 'asdfasdf',
			placement: 'bottom',
			template: learnMoreCreditCardPopoverTemplate,
			trigger: 'focus'
		});
		/* End popover for learn More CreditCard */

		/*
		 * Start popover for why not? - coupon code cannot be applied - payment
		 * page
		 */
		var whyNotCouponCodePopover = $('.why-not-promo-code-not-applied');

		var whyNotCouponCodePopoverTemplate = $('.whyNotCouponCode');

		whyNotCouponCodePopover.hover(function (event) {
			event.preventDefault();
		});

		whyNotCouponCodePopover.popover({
			animation: 'true',
			html: 'true',
			content: 'asdfasdf',
			placement: 'bottom',
			template: whyNotCouponCodePopoverTemplate,
			trigger: 'hover'
		});
		/*
		 * End popover for why not? - coupon code cannot be applied - payment
		 * page
		 */
	}
	/* Start: popover for learn More Instore pickup store */
	var storePickupInfo = $('a[data-target="pickup-info-popover"]');

	var storePickupInfoTemplate = $('.storePickupInfo');

	storePickupInfo.hover(function (event) {
		event.preventDefault();
	});

	storePickupInfo.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: storePickupInfoTemplate,
		trigger: 'hover'
	});
	/* End: popover for learn More Instore pickup store */

	/* Start: popover for see terms in pickup tab */
	var storePickupSeeTerms = $('a[data-target="pickup-see-terms-popover"]');

	var storePickupSeeTermsTemplate = $('.storePickupSeeTerms');

	storePickupSeeTerms.hover(function (event) {
		event.preventDefault();
	});

	storePickupSeeTerms.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: storePickupSeeTermsTemplate,
		trigger: 'hover'
	});
	if ($("#sign-in-password-input").css("display") != "none") {
		$("#sign-in-password-input").maskPassword();
	}

	/* End: popover for see terms in pickup tab */
});
function applySingleShipping() {
	$.ajax({
		url: window.contextPath + "checkout/intermediate/shippingAjaxFormSubmit.jsp",
		type: "POST",
		data: { action: "applySingleShipping" },
		dataType: "json",
		async: false,
		cache: false,
		success: function (data) {
			continueToCheckout('shipping-tab');
			$("html").removeClass("modal-open");
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				//	  alert(contextPath);
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
}

function selectExistingAddress(thisObj, shippingGroupId, relationShipId, uniqueId, wrapCommerceItemId) {
	if ($(thisObj).find('option:last').is(":selected")) {
		$("#addAnotherAddressModal").click();
	} else {
		var shipToAddressName = $(thisObj).find("option:selected").attr("value");
		var showMeGiftingOptions = $("#gift-options-checkbox").hasClass("checkout-flow-radio-selected");
		$.ajax({
			url: window.contextPath + "checkout/intermediate/shippingAjaxFormSubmit.jsp",
			type: "POST",
			data: {
				action: "selectExistingAddress",
				shippingGroupId: shippingGroupId, relationShipId: relationShipId, shipToAddressName: shipToAddressName, uniqueId: uniqueId,
				wrapCommerceItemId: wrapCommerceItemId
			},
			dataType: "html",
			async: false,
			cache: false,
			success: function (data) {
				//continueToCheckout('shipping-tab');
				//$("body").removeClass("modal-open");
				$.ajax({
					url: window.contextPath + "checkout/shipping/displayItemDetail.jsp",
					type: "POST",
					dataType: "html",
					data: { isAjaxRequest: true, uniqueId: uniqueId, pageName: "Shipping" },
					async: false,
					cache: false,
					success: function (displayData) {
						$("#" + uniqueId).html(displayData);
						$.ajax({
							url: window.contextPath + "checkout/common/checkoutOrderSummary.jsp",
							data: { pageName: 'Shipping', showMeGiftingOptions: showMeGiftingOptions },
							success: function (summaryData) {
								var refreshedData = $(summaryData).find('.summary-block-border').html();
								if (refreshedData) {
									$('.errorDisplay').remove();
									$("#order-summary-block").find('.summary-block-border').html(refreshedData);
								}
							},
							error: function (xhr, ajaxOptions, thrownError) {
								if (xhr.status == '409') {
									cancelExitBrowserConfimation();
									var contextPath = $(".contextPath").val();
									location.href = contextPath + 'cart/shoppingCart.jsp';
								}
							}
						});
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == '409') {
							cancelExitBrowserConfimation();
							var contextPath = $(".contextPath").val();
							location.href = contextPath + 'cart/shoppingCart.jsp';
						}
					}
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					cancelExitBrowserConfimation();
					var contextPath = $(".contextPath").val();
					//	  alert(contextPath);
					location.href = contextPath + 'cart/shoppingCart.jsp';
				}
			}

		});
	}
}

function selectShippingMethod(obj, shippingGroupId, relationShipId, uniqueId, wrapCommerceItemId) {
	var shipMethodCode = $(obj).find("option:selected").attr("value");
	var shippingPrice = $(obj).find("option:selected").attr("class");
	var regionCode = $(obj).find("option:selected").attr("id");
	var showMeGiftingOptions = $("#gift-options-checkbox").hasClass("checkout-flow-radio-selected");
	$.ajax({
		url: window.contextPath + "checkout/intermediate/shippingAjaxFormSubmit.jsp",
		type: "POST",
		data: {
			action: "updateShipMethod", shippingGroupId: shippingGroupId, relationShipId: relationShipId,
			shipMethodCode: shipMethodCode, shippingPrice: shippingPrice, regionCode: regionCode, uniqueId: uniqueId, wrapCommerceItemId: wrapCommerceItemId
		},
		dataType: "json",
		async: false,
		cache: false,
		success: function (data) {
			//continueToCheckout('shipping-tab');
			//$("body").removeClass("modal-open");
			$.ajax({
				url: window.contextPath + "checkout/shipping/displayItemDetail.jsp",
				type: "POST",
				dataType: "html",
				data: { isAjaxRequest: true, uniqueId: uniqueId, pageName: "Shipping" },
				async: false,
				cache: false,
				success: function (displayData) {
					$("#" + uniqueId).html(displayData);
					$.ajax({
						url: window.contextPath + "checkout/common/checkoutOrderSummary.jsp",
						data: { pageName: 'Shipping', showMeGiftingOptions: showMeGiftingOptions },
						success: function (summaryData) {
							var refreshedData = $(summaryData).find('.summary-block-border').html();
							if (refreshedData) {
								$("#order-summary-block").find('.summary-block-border').html(refreshedData);
							}
						},
						error: function (xhr, ajaxOptions, thrownError) {
							if (xhr.status == '409') {
								cancelExitBrowserConfimation();
								var contextPath = $(".contextPath").val();
								location.href = contextPath + 'cart/shoppingCart.jsp';
							}
						}
					});
					checkForOrderFinancing();
				},
				error: function (xhr, ajaxOptions, thrownError) {
					if (xhr.status == '409') {
						cancelExitBrowserConfimation();
						var contextPath = $(".contextPath").val();
						location.href = contextPath + 'cart/shoppingCart.jsp';
					}
				}
			});
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				//	  alert(contextPath);
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}

	});
}

function editCartItemInShipping(productId, skuId, updateCommerItemIds, relationShipId, wrapSkuId) {
	var quantity = $(".lineItemQtyEditCart").val();
	var pageName = $('#pageName').val();
	if (!isEmpty(quantity)) {
		quantity = parseInt(quantity);
	} else {
		$(".qtys-valid-text").show();
		return false;
	}
	$('html').removeClass("modal-open")
	$('body').removeAttr('style');
	$.ajax({
		type: "POST",
		url: window.contextPath + "checkout/intermediate/cartAjaxFormSubmit.jsp",
		data: { action: "editCartItemInReview", productId: productId, skuId: skuId, updateCommerItemIds: updateCommerItemIds, relationShipId: relationShipId, quantity: quantity, wrapSkuId: wrapSkuId, pageName: pageName },
		dataType: "html",
		async: false,
		cache: false,
		success: function (data) {
			if (pageName == 'Shipping') {
				continueToCheckout('shipping-tab');
			}
			var successData = "<div>" + data + "</div>";
			$('#checkout-container .global-error-display:first').after($(successData).find(".edit-review-tab-error").clone())
			var relationShipId = $('#checkoutShipPageRelationShipId').val() || '';
			$('.product-information.multiple-shipping[id^="' + relationShipId + '"]').addClass('error');
			checkForOrderFinancing();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				//	  alert(contextPath);
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
	omniCartUpdate();
	return false;
}

function removeCartItemInShipping() {
	var commerceItemIdAndQty = $('#removeItemId').val().split("_");
	var commerceItemId = commerceItemIdAndQty[0];
	var prodMerchCategory = $('#prodMerchCategory').val();
	var quantity = 1;
	var updateOrRemove = commerceItemIdAndQty[2];
	var giftWrapSkuId = commerceItemIdAndQty[3];
	$.ajax({
		type: "POST",
		url: window.contextPath + "checkout/intermediate/cartAjaxFormSubmit.jsp?" + commerceItemId + "=" + quantity,
		data: { action: "removeCartItemInShipping", commerceItemId: commerceItemId, quantity: quantity, updateOrRemove: updateOrRemove, giftWrapSkuId: giftWrapSkuId },
		dataType: "json",
		async: false,
		cache: false,
		success: function (data) {
			var successData = data;
			console.log(data);
			$('html').removeClass("modal-open");
			$('body').removeAttr('style');
			if (successData.success) {
				continueToCheckout('shipping-tab');
			} else {
				continueToCheckout('shipping-tab');
			}
			checkForOrderFinancing();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				//	  alert(contextPath);
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
	utag.link({
		'event_type': 'cart_remove',
		'product_merchandising_category': [],
		'product_id': [commerceItemId]
	});

	return false;
}

$(document).on('click', '.apply-today-close-btn', function () {
	$('#payment-page-apply-today').modal('hide');
});

$(document).on('click', '.apply-today-btn', function () {
	var inputEmail = $('#orderEmail').val();
	$.ajax({
		type: "POST",
		url: window.contextPath + 'checkout/intermediate/synchronyAjaxFormSubmit.jsp',
		data: { formID: 'applyToday', inputEmail: inputEmail },
		dataType: 'html',
		async: false,
		cache: false,
		success: function (responseData) {
			var redirectUrl = $(responseData).find('.synchronyUrl').text();
			location.href = redirectUrl;
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
});
$(document).ajaxComplete(function () {
	window.contextPath = $('.contextPath').val() || '/';
	detailsPopover();
	formatCreditCardDisplay();
	$(window).scroll();

	//Checkout page footer white space issue
	var checkoutContainerHeight = $(window).height() - ($(".checkout-nav").height() + $(".shopping-cart-minimized-footer").height());
	$(document).find("#checkout-container").css('min-height', checkoutContainerHeight + 'px');
});

$(document).on('click', '.paypal_payment_commitOrder', function () {
	$('.paypal_payment_commitOrder').attr('disabled', 'disabled');
	moveToPayPal();
});

function moveToPayPal() {
	if (typeof utag != 'undefined') {
		utag.link({
			event_type: 'paypal_express_click'
		});
	}

	if ($('#currentSite').val() != 'sos') {
		InAuthRequest();
	}
	$('.errorDisplay').remove();
	$('.payPalErrorDisplay').remove();
	if ($('#enterMembershipIDInCheckout').length > 0 && !$('#enterMembershipIDInCheckout').valid()) {
		$(".paypal_payment_commitOrder").removeAttr("disabled");
		return false;
	}
	if ($('#orderPaymentEmailForm').length > 0 && !$('#orderPaymentEmailForm').valid()) {
		$(".paypal_payment_commitOrder").removeAttr("disabled");
		return false;
	}
	var isSuccess = true;

	var tokenExist = $('#payPalToken').val() || '';
	var rewardNumber = $("#enterMembershipIDInCheckout").val();
	var isRewardsCardValid = validateDenaliAccountNum(rewardNumber);
	$('.checkOut-promocode-error-state, .promocode-error-msg').hide();
	$('#promo-code-payment-page').removeClass('error-highlight');
	var couponCode = $('#promo-code-payment-page').val();
	if (couponCode !== undefined && couponCode != "") {
		isSuccess = applyCouponCode(couponCode);
	}

	if (isSuccess) {
		//Email address may come as undefined for logged-in user
		var orderEmail = $("#orderEmail").val();
		if ($("#radio-paypal").is(":checked")) {
			$.ajax({
				type: "POST",
				async: false,
				dataType: "html",
				url: window.contextPath + "checkout/intermediate/paypalAjaxFormSubmit.jsp",
				data: 'formID=payPalOrderSubmitForm&orderEmail=' + orderEmail + '&tokenExist=' + tokenExist + '&rewardNumber=' + rewardNumber + '&isRewardsCardValid=' + isRewardsCardValid,
				success: function (data) {
					if (data.indexOf('Following are the form errors:') > -1) {
						$.ajax({
							url: window.contextPath + "checkout/common/emptyCartRedirect.jsp",
							type: "POST",
							dataType: "json",
							async: false,
							cache: false,
							success: function (redirectData) {
								var successData = redirectData;
								if (successData.redirect == "true" || successData.redirect == true) {
									cancelExitBrowserConfimation();
									window.location.href = window.contextPath + "cart/shoppingCart.jsp";
								} else {
									continueToCheckout('payment-tab');
									//$('#co-payment-tab').prepend( data.split('Following are the form errors:')[1] );
									addBackendErrorsToForm(data, $('#co-payment-tab'));
								}
							},
							error: function (xhr, ajaxOptions, thrownError) {
								if (xhr.status == '409') {
									cancelExitBrowserConfimation();
									var contextPath = $(".contextPath").val();
									location.href = contextPath + 'cart/shoppingCart.jsp';
								}
							}
						});
					} else {
						var payPalToken = $('#payPalToken').val();
						if (payPalToken == 'tokenExist') {
							//alert("tokenExist");
							//document.getElementById("paypalExpresscheckoutId").click();
							$("#checkoutReviewProcessingOverlay").modal('show');
							setTimeout(function () {
								$.ajax({
									type: "POST",
									url: contextPath + "checkout/intermediate/paypalAjaxFormSubmit.jsp",
									data: { formID: 'expressCheckoutWithPayPal', rewardNumber: rewardNumber, isRewardsCardValid: isRewardsCardValid },
									dataType: "html",
									success: function (data1) {
										if (data1.indexOf('Following are the form errors:') > -1) {
											$(".paypal_payment_commitOrder").removeAttr("disabled");
											$('#co-payment-tab').prepend(data1.split('Following are the form errors:')[1]);
											$("#checkoutReviewProcessingOverlay").modal('hide');
										} else {
											$.ajax({
												url: window.contextPath + "checkout/common/emptyCartRedirect.jsp",
												type: "POST",
												dataType: "json",
												async: false,
												cache: false,
												success: function (redirectData) {
													var successData = redirectData;
													if (successData.redirect == "true" || successData.redirect == true &&
														(typeof successData.validationFailed != 'undefined' && successData.validationFailed == true)) {
														cancelExitBrowserConfimation();
														window.location.href = window.contextPath + "cart/shoppingCart.jsp";
													} else {
														if (typeof successData.validationFailed != 'undefined' && successData.validationFailed == true) {
															continueToCheckout('review-tab');
														} else {
															//alert("success")
															window.location.href = window.contextPath + "checkout/confirm/orderConfirmation.jsp";
														}
													}
												}
											});
										}
									}
								});
							}, 520);


						} else {
							//Changes done by Bathula
							//alert("else: tokenExist");
							//document.getElementById("paypalPaymentExpresscheckoutId").click();
							$.ajax({
								type: "POST",
								url: contextPath + "checkout/intermediate/paypalAjaxFormSubmit.jsp",
								data: "formID=checkoutWithPayPal",
								dataType: "html",
								success: function (data1) {
									if (data1.indexOf('Following are the form errors:') > -1) {
										$(".paypal_payment_commitOrder").removeAttr("disabled");
										$('#co-payment-tab').prepend(data1.split('Following are the form errors:')[1]);
									} else {
										var tkn = data1.split(":");
										//alert("else: tokenExist"+tkn);

										var apiName = $('#payPalApiUserName').val();
										var env = $('#payPalhiddenContextPath').val();
										var paypalURL = $('#paypalRedirectURL').val();
										paypal.checkout.setup(apiName, {
											environment: env,
											container: 'paypalPaymentExpresscheckoutId'
										});
										paypal.checkout.initXO();
										paypal.checkout.startFlow(paypalURL + $.trim(tkn[1]) + "&useraction=commit");
										//document.getElementById("paypalPaymentExpresscheckoutId").click();
									}
								}
							});

						}
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					if (xhr.status == '409') {
						cancelExitBrowserConfimation();
						var contextPath = $(".contextPath").val();
						location.href = contextPath + 'cart/shoppingCart.jsp';
					}
				}
			});

			return;
		}
	} else {
		$(".paypal_payment_commitOrder").removeAttr("disabled");
	}
}


$(document).on('click', '.check-another', function () {
	$('.gift-card-container input').val('');
	$('.gift-card-container').show();
	$('.gift-card-balance-container').hide();
});

$(document).on('click', '.toysrus-creditcard-btn', function () {
	var monthSelected = $('#creditcard-option-month').is(':checked');
	var promoSelected = $('#creditcard-option-promo').is(':checked');
	var noSelected = $('#creditcard-option-no').is(':checked');
	var singleUseCouponCode = $('#singleUseCouponCode').val();
	var parentClass = '';
	var paymentMethod = "";
	if ($("#radio-paypal").is(":checked")) {
		paymentMethod = "PayPal";
	}
	if ($('#radio-rus-creditcard').is(':checked')) {
		parentClass = 'rus-credit-card';
	} else if ($('#radio-creditcard').is(':checked')) {
		parentClass = 'credit-card';
	}
	if (monthSelected) {
		isFromPromoFinance = true;
		/*$('#toysrus-creditcard').modal('hide');*/
		var accountNumber = '';
		var _parentForm = '';
		if ($("#radio-rus-creditcard").is(":checked")) {
			_parentForm = "#paymentRusCardForm ";
			accountNumber = $(_parentForm).find('#ccnumber').val();
		} else if ($("#radio-creditcard").is(":checked")) {
			_parentForm = "#paymentCreditCardForm ";
			accountNumber = ($(_parentForm + '#ccnumber').length) ? $(_parentForm).find('#ccnumber').val() : $(_parentForm).find('#creditCardNumber').val();
		}

		getNONCEResponce();
		if (!nonceSetup) {
			return false;
		}
		if (enableVerboseDebug) {
			console.log("calling tokenizePan() for accountNumber:" + accountNumber);
		}
		Radial.tokenize(accountNumber, tokenizeCallBackHandler);
		/*setTimeout(function(){
			autopopulatePrmoDetails();
		}, 2500);*/
	} else if (promoSelected) {
		$('.six-month-financing.' + parentClass + ' #no-six-month-financing').attr('checked', true);
		$('#promo-code-payment-page').val(singleUseCouponCode);
		promoButtonOnOff();
		synchronySelectionDone = true;
		$.ajax({
			type: "POST",
			url: window.contextPath + "checkout/intermediate/ajaxIntermediateRequest.jsp",
			data: { couponCode: singleUseCouponCode, formID: 'applyCouponFromPayment', paymentMethod: paymentMethod },
			async: false,
			dataType: "html",
			success: function (responseData) {
				//Added for omniture integration
				omniCheckoutPromoSubmit();
				if (responseData.indexOf('Following are the form errors:') > -1) {
					var msg = responseData.split('Following are the form errors:')[1];
					/* $('.payment-popup-desc').prepend('<span class="errorDisplay">'+msg+'</span><br>');*/
					$('#toysrus-creditcard .checkOut-promocode-error-state-header span').html(msg);
					$('#toysrus-creditcard .checkOut-promocode-error-state-overlay').show();
					$('#promo-code-payment-page').val('');
					$('#promo-code-payment-page-btn').attr('disabled', 'disabled');
					//Added for omniture integration
					omniCheckoutInvalidCoupon();
				}
				else {
					$('#enter-promo-code-container').replaceWith(responseData);
					$('#toysrus-creditcard').modal('hide');
				}
				reloadOrderSummary('Payment', paymentMethod);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					cancelExitBrowserConfimation();
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'cart/shoppingCart.jsp';
				}
			}
		});
	} else if (noSelected) {
		$('#toysrus-creditcard').modal('hide');
		$('.six-month-financing.' + parentClass + ' #no-six-month-financing').attr('checked', true);
		synchronySelectionDone = true;
	}
});

$(document).on('click', '.electronic-disclosure-agree', function () {
	if ($('#radio-rus-creditcard').is(':checked') || $('#radio-creditcard').is(':checked')) {
		ajaxForPreAuthCall();
	}
});

$(document).on('click', '.deselect-yes-disclosure', function () {
	var parentClass = '';
	if ($('#radio-rus-creditcard').is(':checked')) {
		parentClass = 'rus-credit-card';
	} else if ($('#radio-creditcard').is(':checked')) {
		parentClass = 'credit-card';
	}
	$('.six-month-financing.' + parentClass).find('#yes-six-month-financing').attr('checked', false);
	$('.six-month-financing.' + parentClass).find('#electronicTermsNotify').modal('hide');
	var isToysrusCreditcardOverlay = $(this).parents('#toysrus-creditcard').length;
	if (!isToysrusCreditcardOverlay) {
		$('.six-month-financing.' + parentClass).find('#no-six-month-financing').trigger('click');
	}
	$('.six-month-financing.' + parentClass).find('#electronicTermsNotify').css("display", "none");
});
$(document).on('click', ".disclosure-failure-closeBtn", function () {
	var isFromSynchrony = $("#isFromSynchrony").val();
	if (typeof isFromSynchrony != 'undefined' && isFromSynchrony == 'true') {
		$("#toysrus-creditcard").modal('show');
	}
});
function ajaxForPreAuthCall() {
	var accountNumber = '';
	var cardLength = '';
	var cBinNum = '';
	var cardinalToken = '';
	var isSavedCard = false;
	var parentClass = '';
	var callBack = $('#electronic-disclosure').attr('data-callback');
	if ($('#radio-rus-creditcard').is(':checked')) {
		parentClass = 'rus-credit-card';
	} else if ($('#radio-creditcard').is(':checked')) {
		parentClass = 'credit-card';
	}
	if ($('#radio-rus-creditcard').is(':checked')) {
		accountNumber = ($('#radio-rus-creditcard').is(':checked')) ? $(
			'#radio-rus-creditcard').parents('.credit-structure').find(
			'#ccnumber').val() : $('#radio-creditcard').parents(
				'#creditCardForm').find('#ccnumber').val();
		cardLength = accountNumber.length;
		cBinNum = parseInt(accountNumber.substr(0, 6));
		cardinalToken = $('#cardinalToken').val();
	} else if ($('#radio-creditcard').is(':checked')) {
		if ($('#co-credit-card').length == 0) {
			accountNumber = ($('#radio-rus-creditcard').is(':checked')) ? $(
				'#radio-rus-creditcard').parents('.credit-structure').find(
				'#ccnumber').val() : $('#radio-creditcard').parents(
					'#creditCardForm').find('#ccnumber').val();
			cardLength = accountNumber.length;
			cBinNum = parseInt(accountNumber.substr(0, 6));
			cardinalToken = $('#cardinalToken').val();
		} else {
			cardinalToken = $('#creditCardNumber').val() || $('#ccnumber').val();
			isSavedCard = true;
		}
	}

	var expirationMonth = ($('#radio-rus-creditcard').is(':checked')) ? $('#radio-rus-creditcard').parents('.credit-structure').find('#expirationMonth').val() : $('#radio-creditcard').parents('#creditCardForm').find('#expirationMonth').val();
	var expirationYear = ($('#radio-rus-creditcard').is(':checked')) ? $('#radio-rus-creditcard').parents('.credit-structure').find('#expirationYear').val() : $('#radio-creditcard').parents('#creditCardForm').find('#expirationYear').val();
	var cardCode = ($('#radio-rus-creditcard').is(':checked')) ? $('#creditCardCVV').val() : $('#creditCard_CVV').val() || $('#payment_creditCard_CVV').val();
	var nameOnCard = ($('#radio-rus-creditcard').is(':checked')) ? $(
		'#radio-rus-creditcard').parents('.credit-structure').find(
		'#nameOnCard').val() : $('#radio-creditcard').parents(
			'#creditCardForm').find('#nameOnCard').val();
	var v = parseInt(accountNumber.substr(0, 6));
	if (v == '604586') {
		expirationMonth = $('#expMonthForPLCC').val();
		expirationYear = $('#expYearForPLCC').val();
	}

	$.ajax({
		type: "POST",
		async: false,
		dataType: "html",
		url: window.contextPath + "checkout/intermediate/synchronyAjaxFormSubmit.jsp",
		data: {
			formID: 'electronicDisclosureForm',
			parentClass: parentClass,
			cBinNum: cBinNum,
			expirationMonth: expirationMonth,
			expirationYear: expirationYear,
			cardCode: cardCode,
			nameOnCard: nameOnCard,
			cardLength: cardLength,
			isSavedCard: isSavedCard,
			cardinalToken: cardinalToken
		},
		success: function (responseData) {

			if (responseData.indexOf('Following are the form errors:') > -1) {
				var msg = responseData.split('Following are the form errors:')[1];
				$('#toysrus-creditcard').modal('hide');
				$('#electronic-disclosure').modal('hide');
				$('#disclosure-failure .disclosure-failure-error').html(msg);
				$('#disclosure-failure').modal('show');
				$('.six-month-financing.' + parentClass + ' #yes-six-month-financing').attr('checked', false);
			} else {
				var disclosureAgreed = $(responseData).find('.disclosureAgreed').text();
				if (disclosureAgreed == 'agreed') {
					$('.six-month-financing.' + parentClass + ' #electronicTermsNotify').replaceWith('');
					$('#toysrus-creditcard').modal('hide');
					// $('#electronicTermsNotify').replaceWith('');
					$('#electronic-disclosure').modal('hide');
					$('html, body').removeClass("modal-open");
					$("body").css("padding-right", "");
					/*
					 * setTimeout(function(){ var apr_string =
					 * $(responseData).find(".percentage-valuation-from-ajax").text();
					 * 
					 * var apr_flag = ''; var apr_value = '';
					 * if(apr_string.length > 0 && apr_string.split("
					 * ").length > 0 ){ if(typeof apr_string.split("
					 * ")[0] != 'undefined'){ apr_flag =
					 * apr_string.split(" ")[0].substring(2, 4); }
					 * if(typeof apr_string.split(" ")[1] !=
					 * 'undefined'){ apr_value = apr_string.split("
					 * ")[1].substring(0,2)+"."+apr_string.split("
					 * ")[1].substring(2,4)+"%"; } }
					 * 
					 * if(apr_flag == "00"){ apr_flag="fixed"; }else
					 * if(apr_flag == "01"){ apr_flag="variable"; }
					 * $('#promotion-popup').find("#promoAPRFlag").text(apr_flag);
					 * $('#promotion-popup').find("#promoAPRValue").text(apr_value);
					 * $('#promotion-popup').modal('show'); }, 500);
					 */
					$('.' + parentClass).find('#financeAlreadyAgreed').val('1');
					$('.six-month-financing.' + parentClass + ' #yes-six-month-financing').attr('checked', true);
					$('.six-month-financing.' + parentClass + ' .disclosure-agreed').show();
					$('.' + parentClass).find('#disclosure-agreement-status').val('1');
					if (callBack == 'place-order') {
						creditCardCommitOrder();
					}
					else if (callBack == 'moveTo-orderReview') {
						ccMoveToOrderReview();
					}
				}
			}
			$('html, body').removeClass("modal-open");
			$("body").css("padding-right", "");
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
}

$(document).on('click', '.promotion-popup-print-btn', function () {
	var promotionPrintContent = $('#promotion-popup .payment-popup-desc').html();
	var winObj = window.open();
	winObj.document.write(promotionPrintContent);
	winObj.print();
	winObj.close();
});

$(document).on('click', '.on-disclosure-error', function () {
	var isFromSynchrony = $("#isFromSynchrony").val();

	/*$('#disclosure-failure').modal('hide');
	if($('#radio-rus-creditcard').is(':checked')){
		$('.rus-credit-card-details').prepend('<span class="errorDisplay">You entered an invalid number for the card selected. Please check the card type and re-enter all card information: number, expiration date and Security Code.</span><br>');
	} else if(($('#radio-creditcard').is(':checked'))){
		$('.credit-card-hidden').prepend('<span class="errorDisplay">You entered an invalid number for the card selected. Please check the card type and re-enter all card information: number, expiration date and Security Code.</span><br>');
	}*/
	$("#disclosure-failure").modal("hide");
	if (typeof isFromSynchrony != 'undefined' && isFromSynchrony == 'true') {
		$("#toysrus-creditcard").modal('show');
	}
	else {
		$("#no-six-month-financing").trigger("click");
	}
});

function isFinancingFlowComplete(parentClass) {
	var completedStatus = false;
	var yesSixMonth = $('.six-month-financing.' + parentClass + ' #yes-six-month-financing').is(':checked');
	var noSixMonth = $('.six-month-financing.' + parentClass + ' #no-six-month-financing').is(':checked');
	var isFromSynchrony = $('#isFromSynchrony').val();
	var isCrRadioSelectd = $('#radio-creditcard').is(':checked');
	/*if(yesSixMonth){
		if(isCrRadioSelectd){
			$('#electronic-disclosure').modal('show');
		}else{
			$('#toysrus-creditcard').modal('show');
		}
	}*/
	if (yesSixMonth) {
		$('#electronic-disclosure').modal('show');
	} else {
		$('#toysrus-creditcard').modal('show');
	}
	if (!yesSixMonth && !noSixMonth) {
		completedStatus = true;
	}
	return completedStatus;
}

$(document).on('click', '#giftCardBalanceOverlayBtn', function () {
	checkGiftCardBalance('giftCardBalanceForm');
	return false;
});

$(document).on('click', '.remove-donation-item-review', function () {
	that = $(this);
	var itemId = $(this).attr("id");
	$('#removeItemConfirmationModal').modal('show');
	$('#removeDonationItemId').val(itemId);
});

function removeDonationItemFromReview(commerceItemId) {
	$.ajax({
		type: "POST",
		async: false,
		url: window.contextPath + "checkout/intermediate/cartAjaxFormSubmit.jsp",
		data: { action: "removeDonationItemInReview", commerceItemId: commerceItemId },
		success: function (data) {
			var successData = data;
			if (successData.success && successData.commerceItemCount == 0) {
				cancelExitBrowserConfimation();
				window.location.href = window.contextPath + "cart/shoppingCart.jsp";
			} else {
				continueToCheckout('review-tab');
				$("html").removeClass("modal-open");
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				//	  alert(contextPath);
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		},
		dataType: "html",
		cache: false
	});
	return true;
}


$(document).on('click', '.gift-card-table .pull-right', function () {
	var cardId = $(this).data('id');
	var paymentMethod = "";
	if ($("#radio-paypal").is(":checked")) {
		paymentMethod = "PayPal";
	}
	var pageName = "";
	if ($(".my-account-template").length) {
		pageName = "myAccount";
	} else {
		pageName = "checkout";
	}
	$.ajax({
		url: window.contextPath + "checkout/common/checkSession.jsp",
		type: "POST",
		dataType: "json",
		async: false,
		cache: false,
		success: function (redirectData) {
			removeBackendErrorMessagesInline($('#giftCardApplicationForm'));
			$.ajax({
				type: "POST",
				url: window.contextPath + "checkout/payment/checkBalance.jsp?pageName=" + pageName,
				data: { cardId: cardId, formID: 'giftCardRemoveForm', paymentMethod: paymentMethod },
				dataType: "json",
				async: false,
				success: function (responseData) {
					if ($(responseData).find(".errorDisplay").length > 0) {
						$('#giftCardApplicationForm').before($(responseData).find(".errorDisplay")[0]);
					}
					else {
						var giftCardList = $(responseData).find('.giftCardList').html();
						var isPayInStoreEligible = $(responseData).find('#isPayInStoreEligible').val();
						if ($(giftCardList).find('#gcAppliedCount').val() == "0" && isPayInStoreEligible == 'true') {
							$("#radio-payinstore").removeAttr("disabled").attr("checked", false);
							$('.payInStore-disabled-message').removeClass('preShow');
						}
						$('.gift-card-table').replaceWith(giftCardList);
						var orderSummary = $(responseData).find('.orderSummary').html();
						$('#order-summary-block').replaceWith(orderSummary);
						checkForOrderFinancing();
						checkForZeroTotal();
						//var priceSummary = $(responseData).find('.priceSummary .summary-block-padded-content').html();
						//$('.summary-block-padded-content').html(priceSummary);
					}
					var gcAppliedCount = $('#gcAppliedCount').val() || 0;
					if (parseInt(gcAppliedCount) == 0) {
						validateCredtcardFormForPromotion();
					}
				},
				error: function (a, b, c) {
					console.log(a);
				},
				dataType: "html"
			});
			return false;
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
});

$(document).on('click', '.checkGiftCarddetails', function () {
	$.ajax({
		url: window.contextPath + "checkout/common/checkSession.jsp",
		type: "POST",
		dataType: "json",
		async: false,
		cache: false,
		success: function (redirectData) {
			$('.errorDisplay').remove();
			$('#giftCardBalanceModal p.global-error-display').html("");
			$('#giftCardBalanceModal input').removeClass('error-highlight');
			$('#giftCardBalanceModal .gift-card-container').show();
			$('#giftCardBalanceModal .gift-card-container input').val('');
			$('#giftCardBalanceModal .gift-card-balance-container').hide();
			if (!$(this).hasClass('giftCardDisabled')) {
				$('#giftCardBalanceForm input').removeAttr('disabled');
				$('#giftCardBalanceModal').modal('show');
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
});

$(document).on('change', '#radio-payinstore', function () {
	disableGiftCard();
});
/*$(document).on('change','#radio-creditcard, #radio-paypal, #radio-rus-creditcard',function(){
	enableGiftCard();
});*/
function disableGiftCard() {
	if ($("#radio-payinstore").is(":checked")) {
		$('.checkout-payment-giftcard').addClass('giftcard-blur');
		$('#giftCardApplicationForm a').addClass('giftCardDisabled');
		$("#giftCardApplicationForm :input").each(function () {
			var input = $(this);
			input.attr('disabled', true);
		});
	}
}
/*function enableGiftCard(){
	$('.checkout-payment-giftcard').removeClass('giftcard-blur');
	$('#giftCardApplicationForm a').removeClass('giftCardDisabled');
	$("#giftCardApplicationForm :input").each(function(){
		var input = $(this);
		input.attr('disabled', false);
	});
}*/

function applyCouponCode(couponCode) {
	var paymentMethod = "";
	if ($("#radio-paypal").is(":checked")) {
		paymentMethod = "PayPal";
	}
	var success = $.ajax({
		type: "POST",
		url: window.contextPath + "checkout/intermediate/ajaxIntermediateRequest.jsp",
		data: { couponCode: couponCode, formID: 'applyCouponFromPayment' },
		async: false,
		dataType: "html",
		success: function (responseData) {
			if (responseData.indexOf('Following are the form errors:') > -1) {
				/*var msg = responseData.split('Following are the form errors:')[1];*/
				// $('.checkOut-promocode-error-state-subheader').prepend(msg);
				/*$('.checkOut-promocode-error-state-subheader > span:first-of-type').html(msg);
				$('.checkOut-promocode-error-state, .promocode-error-msg').show();
				$('.promocode-error-msg').html(msg);*/
				/*$('#promo-code-payment-page').addClass('error');
				focusOnElement($('.checkOut-promocode-error-state-subheader > span'));*/
				addBackendErrorsToForm(responseData, $("#promoCodePaymentPageForm"));
			}
			else {
				$('#enter-promo-code-container').replaceWith(responseData);
				reloadOrderSummary('Payment', paymentMethod);
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
	return (success.responseText.indexOf('Following are the form errors:') > -1) ? false : true
}

$(document).on('click', '#rusCardBillingEdit', function () {
	$('#rus-billing-info').hide();
	$.ajax({
		type: "POST",
		url: window.contextPath + "checkout/intermediate/synchronyAjaxFormSubmit.jsp",
		data: { formID: 'rusCardBillingInfoEditForm' },
		async: false,
		dataType: "html",
		success: function (responseData) {
			$('.rusCardBillingAddressForm').html(responseData);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
});

function enableDisableFinacingSection(cardType) {
	var financingOptions = $('.financingOptions').val();

	if (financingOptions == 'true') {

		if ((cardType == 'RUSPrivateLabelCard' || cardType == 'RUSCoBrandedMasterCard') && $('#radio-creditcard').is(':checked')) {

			$('.six-month-financing.credit-card .finacingActionDiv').removeClass('finaceGrayedOut');
			$('.six-month-financing.credit-card .finacingActionDiv input').removeAttr('disabled');
		} else {
			$('.disclosure-agreed').hide();
			$('.six-month-financing').find('#yes-six-month-financing').attr('checked', false);
			$('.six-month-financing.credit-card .finacingActionDiv').addClass('finaceGrayedOut');
			$('.six-month-financing.credit-card .finacingActionDiv input').attr('disabled', 'disabled');
		}
	}
}

$('#co-credit-card').on('change', function () {
	var coCreditCardVal = $("#co-credit-card option:selected").attr('data-coCreditCardVal');
	enableDisableFinacingSection(coCreditCardVal);
});
$(window).load(function () {
	var coCreditCardVal = $("#co-credit-card option:selected").attr('data-coCreditCardVal');
	enableDisableFinacingSection(coCreditCardVal);
	setTimeout(function(){
		clearPrePopulatedData();	
	},1000);
});


$(document).on('click', '.paypal_review_commitOrder', function () {
	$('.paypal_review_commitOrder').attr('disabled', 'disabled');
	initPaypalFromReviewPage();
});

/* Start: This Method will call from Review Page on Paypal click */
function initPaypalFromReviewPage() {
	if ($('#currentSite').val() != 'sos') {
		InAuthRequest();
	}
	if ($('#membershipIDInReviewForm').length > 0 && !$('#membershipIDInReviewForm').valid()) {
		return false;
	}
	var rewardNumber = $("#enterMembershipIDInReview").val() || '';
	var isRewardsCardValid = validateDenaliAccountNum(rewardNumber);
	$.when(callingFraudJsCollector()).then(function () {
		var deviceID = $("#field_d1").val();
		var browserAcceptEncoding = "";
		if (Accept_Encoding != undefined) {
			browserAcceptEncoding = Accept_Encoding;
		} else if (Content_Encoding != undefined) {
			browserAcceptEncoding = Content_Encoding;
		}
		deviceID = deviceID.substring(0, 4000);
		$.ajax({
			type: "POST",
			async: false,
			dataType: "html",
			url: window.contextPath + "checkout/intermediate/paypalAjaxFormSubmit.jsp",
			data: 'formID=payPalSetExpress' + '&rewardNumber=' + rewardNumber + '&isRewardsCardValid=' + isRewardsCardValid + '&deviceID=' + deviceID + '&browserAcceptEncoding=' + browserAcceptEncoding,
			success: function (data) {
				if (data.indexOf('Following are the form errors:') > -1) {
					$(".paypal_review_commitOrder").removeAttr("disabled");
					if (rewardNumber != "" && rewardNumber != null && isRewardsCardValid != true) {
						addBackendErrorsToForm(data, $(".checkout-confirmation-content"));
					} else {
						continueToCheckout("payment-tab");
						addBackendErrorsToForm(data, $('#co-payment-tab'));
						//$('#co-payment-tab').prepend( data.split('Following are the form errors:')[1] );
					}
				} else {
					var payPalToken = $('#payPalReviewPageToken').val();
					if (payPalToken == 'notTokenExist') {
						//alert("notTokenExist");
						//document.getElementById("paypalReviewPageExpresscheckoutId").click();


						$.ajax({
							type: "POST",
							url: contextPath + "checkout/intermediate/paypalAjaxFormSubmit.jsp",
							data: "formID=checkoutWithPayPalFromOrderReview",
							dataType: "html",
							success: function (data1) {
								if (data.indexOf('Following are the form errors:') > -1) {
									$.ajax({
										url: window.contextPath + "checkout/common/emptyCartRedirect.jsp",
										type: "POST",
										dataType: "json",
										async: false,
										cache: false,
										success: function (redirectData) {
											var successData = redirectData;
											if (successData.redirect == "true" || successData.redirect == true &&
												(typeof successData.validationFailed != 'undefined' && successData.validationFailed == true)) {
												cancelExitBrowserConfimation();
												window.location.href = window.contextPath + "cart/shoppingCart.jsp";
											} else {
												if (typeof successData.validationFailed != 'undefined' && successData.validationFailed == true) {
													continueToCheckout('review-tab');
												} else {
													continueToCheckout("payment-tab");
													$('#co-payment-tab').prepend(data1.split('Following are the form errors:')[1]);
												}
											}
										}
									});
								} else {
									var tkn = data1.split(":");
									var apiName = $('#payPalApiUserName').val();
									var env = $('#payPalhiddenContextPath').val();
									var paypalURL = $('#paypalRedirectURL').val();
									paypal.checkout.setup(apiName, {
										environment: env,
										container: 'paypalReviewPageExpresscheckoutId'
									});
									paypal.checkout.initXO();
									paypal.checkout.startFlow(paypalURL + $.trim(tkn[1]) + "&useraction=commit");
									//document.getElementById("paypalPaymentExpresscheckoutId").click();
								}
							}
						});

						/*
					var tkn = "EC-6WM28796MK059123N";
					var paypalURL= "https://www.sandbox.paypal.com/checkoutnow?token=";
					var apiName = "devtest_api1.toysrus.com";
					var env = "sandbox";
					paypal.checkout.setup(apiName, {
						environment: env,
						container: 'paypalReviewPageExpresscheckoutId'
					});
					paypal.checkout.initXO();
					paypal.checkout.startFlow(paypalURL+tkn);  */
					}
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					cancelExitBrowserConfimation();
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'cart/shoppingCart.jsp';
				}
			}
		});
	});
}
$(document).on('click', '.checkoutGiftCardHelpClass', function () {
	if (!$(this).hasClass('giftCardDisabled')) {
		$('.checkoutGiftCardHelptooltip').toggle();

		var cc = $('.checkoutGiftCardHelptooltip').is(':visible')
		if (cc) {
			$('.checkoutGiftCardHelpClass-wrapper').css('margin-top', '-2px');
		} else {
			$('.checkoutGiftCardHelpClass-wrapper').css('margin-top', '0px');
		}
	}
});

function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	}

	return true;
}

function payPalPlaceOrder() {
	if ($('#currentSite').val() != 'sos') {
		InAuthRequest();
	}
	var rewardNumber = $("#enterMembershipIDInReview").val() || '';
	var isRewardsCardValid = validateDenaliAccountNum(rewardNumber);
	$.when(callingFraudJsCollector()).then(function () {
		var deviceID = $("#field_d1").val();
		var browserAcceptEncoding = "";
		if (Accept_Encoding != undefined) {
			browserAcceptEncoding = Accept_Encoding;
		} else if (Content_Encoding != undefined) {
			browserAcceptEncoding = Content_Encoding;
		}
		deviceID = deviceID.substring(0, 4000);
		$("#checkoutReviewProcessingOverlay").modal('show');
		setTimeout(function () {
			$.ajax({
				type: "POST",
				async: false,
				dataType: "html",
				url: window.contextPath + "checkout/intermediate/paypalAjaxFormSubmit.jsp",
				data: 'formID=expressCheckoutWithPayPal' + '&rewardNumber=' + rewardNumber + '&isRewardsCardValid=' + isRewardsCardValid + '&deviceID=' + deviceID + '&browserAcceptEncoding=' + browserAcceptEncoding,
				success: function (payPaldata) {
					if (payPaldata.indexOf('Following are the form errors:') > -1) {
						continueToCheckout('payment-tab');
						var msg = payPaldata.split('Following are the form errors:')[1];
						$('.checkOut-promocode-error-state-subheader > span').html(msg);
						$('.checkOut-promocode-error-state, .promocode-error-msg').show();
						$('.why-not-promo-code-not-applied').hide();
						$("#checkoutReviewProcessingOverlay").modal('hide');
						$("html").removeClass("modal-open");
						$("body").css('padding-right', '');
					} else {
						//Start : MVP8 for handling OOS items logic just before placing the order with paypal paymnet from review order page
						$.ajax({
							url: window.contextPath + "checkout/common/emptyCartRedirect.jsp",
							type: "POST",
							dataType: "json",
							async: false,
							cache: false,
							success: function (redirectData) {
								var successData = redirectData;
								if ((successData.redirect == "true" || successData.redirect == true) &&
									(typeof successData.validationFailed != 'undefined' && successData.validationFailed == true)) {
									cancelExitBrowserConfimation();
									window.location.href = window.contextPath + "cart/shoppingCart.jsp";
								} else if (typeof successData.validationFailed != 'undefined' && successData.validationFailed == true) {
									continueToCheckout('review-tab');
								} else {
									window.location.href = window.contextPath + "checkout/confirm/orderConfirmation.jsp";
								}
							}
						});
						//End : MVP8 for handling OOS items logic just before placing the order with paypal paymnet from review order page

					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					if (xhr.status == '409') {
						cancelExitBrowserConfimation();
						var contextPath = $(".contextPath").val();
						location.href = contextPath + 'cart/shoppingCart.jsp';
					}
				}
			});
		}, 520);
	});
}
//Added for omniture integration
function omniCheckoutInvalidCoupon() {
	if (typeof (utag) != 'undefined') {
		utag.link({
			event_type: 'invalid_coupon_code'
		});
	}
}

function omniCheckoutPromoSubmit() {
	if (typeof (utag) != 'undefined') {
		utag.link({
			event_type: 'promotion_submitted'
		});
	}
}

//Get shipping address
function getShippingAddress() {
	var respAddress;
	$.ajax({
		url: window.contextPath + "checkout/intermediate/getShippingAddressAjaxSubmit.jsp",
		type: "POST",
		async: false,
		cache: false,
		dataType: 'json',
		success: function (data) {
			respAddress = data;
		},
		error: function (e) {
			console.log(e);
		}
	});
	return respAddress;
}

$(document).on('hide.bs.modal', '#toysrus-creditcard', function () {
	$('#toysrus-creditcard .checkOut-promocode-error-state-overlay').hide();
});
/* Client side validation */

function contactUsPopupWindow(url) {
	popupWindow = window.open(
		url, 'popUpWindow', 'height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
function termsConditionPopupWindow(url) {
	popupWindow = window.open(
		url, 'popUpWindow', 'height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
function privacyPolicyPopupWindow(url) {
	popupWindow = window.open(
		url, 'popUpWindow', 'height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}

function initJSValidation() {

	/*$(".JSValidation").validate({
		rules: {
			firstName: "required",
			lastName : "required",
			address1 : "required",
			city   : "required",
			state  : "required",
			nickname: "required",
			postalCode :{
				required:true,
				validZip:true
			},
			phoneNumber:{
				required:true,
				maxlength:13,
				numeric:true
			},
			gcNumber : {
				required:true,
				numeric:true
			},
			nameOnCard : "required",
			cardNumber : {
				required:true,
				numeric:true
			},
			expirationMonth : "required",
			expirationYear : "required",
			creditCard_CVVmaskedPassword0 : "required",
			country :"required",
			ean : "required"

		},

		messages: {
			firstName: "Please enter your firstname",
			lastName: "Please enter your lastname",
			address1: "Please enter your address",
			city: "Please enter your city",
			state:"Please select state",
			nickname:"Please enter your nickname",
			postalCode:{
				required:"Please enter your ZIP/postal code",
				validZip:"Invalid ZIP/postal code"
			},
			gcNumber : {
				required:"Please enter gift card number",
				numeric:"Please enter only numbers"
			},
			phoneNumber: {
				required: "Please enter telephone number",
				numeric:"Please enter only numbers"

			},
			nameOnCard : "Please enter your name on card",
			cardNumber : {
				required: "Please enter card number",
				numeric:"Please enter only numbers"
			},
			expirationMonth : "Please enter expiration month",
			expirationYear : "Please enter expiration year",
			creditCard_CVVmaskedPassword0 : "Please enter cvv number",
			country : "Please select your country",
			ean : ""
		}

	});

	$.validator.addMethod("numeric", function(value) {
		return  /^[0-9]*$/.test(value)
	});

	$.validator.addMethod("validZip", function(value) {
		return  /^[0-9a-zA-Z]{5}(?:-[0-9a-zA-Z]{4})?$/.test(value)
	});*/
}

//InAuth JS call changes
function InAuthRequest() {
	var orderId = $('#cardianlOrderId').val();
	var apiKey = $('#inAuthKey').val();
	var url = $('#inAuthURL').val();
	var _cc = _cc || [];
	_cc.push(['ci', { 'sid': apiKey, 'tid': orderId }]);
	_cc.push(['st', 1000]);
	_cc.push(['run', ('https:' == document.location.protocol ? 'https://' : 'http://') + url]);
	(function () {
		var c = document.createElement('script'); c.type = 'text/javascript'; c.async = true;
		c.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + url + '/cc.js?ts=' + (new Date()).getTime();
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(c, s);
	})();
}

$(document).ajaxComplete(function (event, request, settings) {
	if ($('body').find("#load-checkout-norton-script").length) {
		$('body').find("[id=load-checkout-norton-script]").html($("#loadNortonHiddenDiv").html());
	}
});
function omniCartUpdate() {
	var pMCategory = $('#pMCategory').val();
	utag.link({ "event_type": "cart_update", "product_merchandising_category": [] });
}


function isCheckSessionExpired() {
	$.ajax({
		url: window.contextPath + "checkout/common/checkSession.jsp",
		type: "POST",
		dataType: "json",
		async: false,
		cache: false,
		success: function (redirectData) {
			return false;
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
			return true;
		}
	});
}

//Added for calling tealium for checkout pages
function ajaxCallForTealiumCheckout(contextPath) {
	var tealiumPageName = $("#tealiumPageName").val();
	$.ajax({
		type: "POST",
		url: contextPath + "checkout/common/tealiumForCheckout.jsp",
		data: {
			tealiumPageName: tealiumPageName
		},
		dataType: "html",
		processData: true,
		async: true,
		success: function (data) {
			$("#tealiumCheckoutContent").html(data);
		},
		error: function () {

		}
	});
}

function checkForOrderFinancing() {
	$.ajax({
		type: "POST",
		url: window.contextPath + "checkout/intermediate/synchronyAjaxFormSubmit.jsp?formID=checkForOrderFinancing",
		success: function (data) {
			if (data.indexOf('Following are the form errors:') > -1) {
				addBackendErrorsToForm(data, $("#checkout-container"));
				checkForFinancing();
			}
			else {
				checkForFinancing();
			}
		}
	});
}

function autopopulatePrmoDetails(callback) {
	var parentClass = '';
	if ($('#radio-rus-creditcard').is(':checked')) {
		parentClass = 'rus-credit-card';
	} else if ($('#radio-creditcard').is(':checked')) {
		parentClass = 'credit-card';
	}
	var cardType = cardTypeForCvvValidation;
	if (typeof cardType == 'undefined' || cardType == 'invalid' || cardType == '' && $('#radio-rus-creditcard').is(':checked')) {
		cardType = $('#cardType').val();
	}
	if (typeof cardType == 'undefined' || cardType == 'invalid' || cardType == '' && $('#radio-creditcard').is(':checked')) {
		cardType = $('#credit_Card_Type').val();
	}

	isFromPromoFinance = false;
	if ($('.' + parentClass).find('#financeAlreadyAgreed').val() != '1'
		&& ($('.' + parentClass).find('#yes-six-month-financing').is(
			':checked') || $('#creditcard-option-month').is(':checked'))) {
		$('.errorDisplay').remove();
		var cardinalToken = $('#cardinalToken').val();
		var isSavedCard = false;
		if ($('#isSaved_Card').val() != '1') {
			isSavedCard = $('#isSaved_Card').val();
		}
		//var accountNumber = ($('#radio-rus-creditcard').is(':checked')) ? $('#radio-rus-creditcard').parents('.credit-structure').find('#creditCardNumber').val() || $("#paymentRusCardForm #ccnumber").val() : $('#radio-creditcard').parents('#creditCardForm').find('#creditCardNumber').val() || $("#paymentCreditCardForm #ccnumber").val();
		var accountNumber = '';
		var _parentForm = '';
		if ($("#radio-rus-creditcard").is(":checked")) {
			_parentForm = "#paymentRusCardForm ";
			accountNumber = $(_parentForm).find('#ccnumber').val();
		} else if ($("#radio-creditcard").is(":checked")) {
			_parentForm = "#paymentCreditCardForm ";
			accountNumber = ($(_parentForm + '#ccnumber').length) ? $(_parentForm).find('#ccnumber').val() : $(_parentForm).find('#creditCardNumber').val();
		}

		var expirationMonth = ($('#radio-rus-creditcard').is(':checked')) ? $(
			'#radio-rus-creditcard').parents('.credit-structure').find(
			'#expirationMonth').val()
			: $('#radio-creditcard').parents('#creditCardForm').find(
				'#expirationMonth').val();
		var expirationYear = ($('#radio-rus-creditcard').is(':checked')) ? $(
			'#radio-rus-creditcard').parents('.credit-structure').find(
			'#expirationYear').val() : $('#radio-creditcard').parents('#creditCardForm').find('#expirationYear').val();
		var cardCode = ($('#radio-rus-creditcard').is(':checked')) ? $(
			'#creditCardCVV').val() : $('#creditCard_CVV').val()
			|| $('#payment_creditCard_CVV').val();
		var nameOnCard = ($('#radio-rus-creditcard').is(':checked')) ? $(
			'#radio-rus-creditcard').parents('.credit-structure').find(
			'#nameOnCard').val() : $('#radio-creditcard').parents(
				'#creditCardForm').find('#nameOnCard').val();

		var v = parseInt(accountNumber.substr(0, 6));
		if (v == '604586') {
			expirationMonth = $('#expMonthForPLCC').val();
			expirationYear = $('#expYearForPLCC').val();
		}
		if (accountNumber == '' || expirationMonth == '' || expirationYear == '' || cardCode == '' || nameOnCard == '') {
			$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">Please enter complete card details</span>');
			$(window).scrollTop(0);
			return false;
		}
		if (typeof cardinalToken == 'undefined' || cardinalToken == '') {
			cardinalToken = accountNumber;
		}
		var formData = $("#paymentBillingAddressForm").serialize();
		$.ajax({
			type: "POST",
			cache: false,
			async: false,
			url: window.contextPath + 'checkout/intermediate/synchronyAjaxFormSubmit.jsp?' + formData,
			data: {
				formID: 'promoFinancingOptions',
				parentClass: parentClass,
				cBinNum: v,
				expirationMonth: expirationMonth,
				expirationYear: expirationYear,
				cardCode: cardCode,
				cardinalToken: cardinalToken,
				nameOnCard: nameOnCard,
				rusCardType: cardType,
				isSavedCard: isSavedCard
			},
			success: function (data) {
				/*var responseCode = $(data).find('#p_text').text();
				var p_text = $(data).find('#p_text').text();
				var p_rate = $(data).find('#p_rate').text();
				var finance_plan = $(data).find('#p_finance_plan').text();
				var promo_variance = $(data).find('#promo_variance').text();
				var promo_auth_standIn = $(data).find('#promo_auth_standIn').text();*/
				var isFromSynchrony = $("#isFromSynchrony").val();
				if (data.indexOf('Following are the form errors:') > -1) {
					if (typeof isFromSynchrony != 'undefined' && isFromSynchrony == 'true') {
						$("#toysrus-creditcard").modal('hide');
					}
					$('#disclosure-failure').modal('show');
				}
				else {
					var responseData = $.parseJSON(data);
					if ((responseData.promoResponseCode).toLowerCase() == "success") {
						setTimeout(function () {
							// $("#electronic-disclosure
							// #promo_plan").text(p_text);

							$("#electronic-disclosure .finance_plan").text(responseData.financingTermsAvailable);
							if (responseData.promoFinanceRate != '' && responseData.aprText != 'undefined') {
								$("#electronic-disclosure #apr_text").text('APR of ' + responseData.promoFinanceRate + '%.');
							}
							else {
								$("#electronic-disclosure #apr_text").text('');
							}
							$("#electronic-disclosure .p_variance").text(responseData.promoBVariance);
							$("#electronic-disclosure #p_var_details").text(responseData.varianceDetails);
							$("#electronic-disclosure #finance_rate").text(responseData.promoFinanceRate);
							$("#electronic-disclosure .promo_auth_stand").text(responseData.promoAuthStandIn);
							if (callback == 'place-order') {
								$("#electronic-disclosure").attr('data-callBack', 'place-order');
							}
							else if (callback == 'order-review') {
								$("#electronic-disclosure").attr('data-callBack', 'moveTo-orderReview');
							}
							$('#electronic-disclosure').modal('show');
						}, 500);
					}
					else if (responseData.promoResponseCode == "Fail" || responseData.promoResponseCode == "Timeout") {
						if (typeof isFromSynchrony != 'undefined' && isFromSynchrony == 'true') {
							$("#toysrus-creditcard").modal('hide');
						}
						$('#disclosure-failure').modal('show');
					}
					else if (responseData.promoResponseCode == "Down-Time") {
						setTimeout(function () {

							$("#electronic-disclosure .finance_plan").text(responseData.financingTermsAvailable);
							if (responseData.promoFinanceRate != '' && responseData.aprText != 'undefined') {
								$("#electronic-disclosure #apr_text").text('APR of ' + responseData.promoFinanceRate + '%.');
							}
							else {
								$("#electronic-disclosure #apr_text").text('');
							}
							$("#electronic-disclosure .p_variance").text(responseData.promoBVariance);
							$("#electronic-disclosure #p_var_details").text(responseData.varianceDetails);
							$("#electronic-disclosure #finance_rate").text(responseData.promoFinanceRate);
							$("#electronic-disclosure .promo_auth_stand").text(responseData.promoAuthStandIn);
							if (callback == 'place-order') {
								$("#electronic-disclosure").attr('data-callBack', 'place-order');
							}
							else if (callback == 'order-review') {
								$("#electronic-disclosure").attr('data-callBack', 'moveTo-orderReview');
							}
							$('#electronic-disclosure').modal('show');
						}, 500);

					}
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					cancelExitBrowserConfimation();
					var contextPath = $(".contextPath").val();
					location.href = contextPath
						+ 'cart/shoppingCart.jsp';
				}
			}
		});
	}
}

$(document).on('click', '#yes-financing-continueGE', function () {

	$("#electronic-disclosure").attr('data-callBack', '');
	isFromPromoFinance = true;
	var _parent = "";
	if ($("#radio-rus-creditcard").is(":checked")) {
		_parent = "#paymentRusCardForm ";
	} else if ($("#radio-creditcard").is(":checked")) {
		_parent = "#paymentCreditCardForm "
	}
	var isSaved_Card = $(_parent + '#isSaved_Card').val() || $('#isSaved_Card').val();
	if (isSaved_Card == 'true') {
		if (_parent == "#paymentCreditCardForm ") {
			var cardNum = $(_parent + '#creditCardNumber').val();
		}
		if (_parent == "#paymentRusCardForm ") {
			var cardNum = $(_parent + '#ccnumber').val();
		}
	} else {
		var cardNum = $(_parent + '#ccnumber').val();
	}
	if (isNaN(cardNum)) {
		var isValidCard = true;
	} else {
		var isValidCard = validateCreditCardNumber(cardNum);
	}
	if (isValidCard) {
		if (isCardinalEnabled) {
			var callTokenization = true;
			//var v = parseInt(cardNum.substr(0, 6));
			if (isNaN(cardNum)) {
				console.log("not calling as saved RUS");
				callTokenization = false;
				$(_parent + '#cardinalToken').val(cardNum);
				$(_parent + ' #cBinNum').val(cardNum.substring(0, 6));
				$(_parent + ' #cLength').val(cardNum.length);
			}
			if (callTokenization) {
				startTokenization_checkout();
			}
			else {
				autopopulatePrmoDetails();
			}
		} else {
			autopopulatePrmoDetails();
		}
	}
});

$(document).on('click', '#no-six-month-financing', function () {
	var parentClass = '';
	if ($('#radio-rus-creditcard').is(':checked')) {
		parentClass = 'rus-credit-card';
	} else if ($('#radio-creditcard').is(':checked')) {
		parentClass = 'credit-card';
	}
	$('.six-month-financing.' + parentClass + ' #yes-six-month-financing').attr('checked', false);
	if ($('.' + parentClass).find('#financeAlreadyAgreed').val() == '1') {
		//$('.six-month-financing.' + parentClass + ' .disclosure-agreed').css('opacity', '0.5');
		//$('.six-month-financing.' + parentClass + ' .disclosure-agreed').modal('hide');
		$('.' + parentClass).find('.disclosure-agreed').hide();
	} else {
		//$('.six-month-financing.' + parentClass + ' #electronicTermsNotify').css('opacity', '0.5');
		//$('#electronicTermsNotify').modal('hide');
		//$('.six-month-financing.' + parentClass + ' #electronicTermsNotify').modal('hide');
		$('.' + parentClass).find('#electronicTermsNotify').hide();
	}
});
$("body").on("hide.bs.modal", function () {
	$("html").removeClass("modal-open");
});

function initTRUCheckoutComponents() {
	$(document).on('click', '.checkout-gift-options.tru-green-round-checkbox', function () {
		setTimeout(updateGiftOptions);
	});

	$(document).on('input focus', '.shipping-address-field-input', function() {
		var $field = $(this).closest('.tru-float-label-field');
		if (this.value) {
			$field.addClass('shipping-address-field-active');
		} else {
			$field.removeClass('shipping-address-field-active');
		}
		//jQuery validate error messages fix
		if($(this).hasClass('error')) {
			//find the error message height, and add this value as margin to the parent container
			var errorMsgHeight = $field.find('.jvalidate-error').height();
			$field.css('margin-bottom', errorMsgHeight);
		}		
	});

	$(document).on('blur', '.shipping-address-field-input', function() {
		var $field = $(this);
		if(!$field.val().trim() && $field.attr('aria-required')) {
			$field.parents('.shipping-address-field')
				.addClass('shipping-address-field-error')
				.removeClass('shipping-address-field-valid shipping-address-field-active input-not-empty');
		}
		else {
			$field.parents('.shipping-address-field')
				.removeClass('shipping-address-field-error shipping-address-field-active')
				.addClass('shipping-address-field-valid');
		}
	});

	$(document).on('mousedown', '.shipping-address-field-clear', function() {
		var $field = $(this).parents('.shipping-address-field');
		$field.removeClass('shipping-address-field-active input-not-empty');
		$field.find('.shipping-address-field-input').val('');
	})
}

function checkoutSaveBillingAddressChanges(orderEmail, rewardNumber, isRewardsCardValid) {
	var formData = $('#paymentBillingAddressForm').serialize();
  $.ajax({
		type: "POST",
		async: false,
		dataType: "html",
		url: window.contextPath + "checkout/intermediate/billingAjaxFormSubmit.jsp",
		data: formData + '&formID=giftCardPaymentOrderReviewForm&orderEmail=' + orderEmail + '&rewardNumber=' + rewardNumber + '&isRewardsCardValid=' + isRewardsCardValid,
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				cancelExitBrowserConfimation();
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'cart/shoppingCart.jsp';
			}
		}
	});
	return;
}