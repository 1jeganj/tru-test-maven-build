/* password Masking plugin */
(function($) {
	  $.fn.maskPassword = function(options) {
	    var opts = $.extend({}, {
			checkInterval: 10, //set timeout to check whether all the characters are the same
			transDelay: 40, //delay to transform last letter
			character: '%u25CF' //instead of the character
		}, options);

	    var checkTimeout = [];
	    var transTimeout = [];
	    var regex = new RegExp('[^' + opts.character + ']', 'gi');
	    var i = 0;

	    this.each(function(index) {
	      var suffix = 'maskedPassword' + index;
	      var $this = $(this);
	      var id = this.id;
	      var name = this.name;
	      var newId = id + suffix;
	      var newName = id + suffix;
	      if (!id || !name) {
	        //alert("Please set 'id' and 'name' attributes for elements!");
	    	  var random = Math.floor(Math.random()*90000) + 10000;
	    	if(!id){
	    		this.id = "maskedRandomId-"+random;
	    		id = this.id;
	    	}
	    	if(!name){
	    		this.name = "maskedRandomName-"+random;
	    		name = this.name;
	    	}
	        //return false;
	      }
	      var $newObj = $('<input>')
	        .attr($.extend(extractAttributes(this), { 'id': newId, 'name': newName, 'autocomplete': 'off', 'type': 'text', 'vscai':'password'}))
	      var newElem = $('#' + newId)[0];
	      var $doc = $(document);
	      $this.before($newObj).attr({'accessKey':'',tabIndex:''}).attr('autocomplete','off').hide();
	      $newObj.on('focus', function() {
	        var oldValue = $newObj.val();
	        checkTimeout[index] = setTimeout(function() {
	          checkChange(index, id, newId, oldValue, $this);
	        }, opts.checkInterval);
	      });
	      $doc.on('blur', $newObj ,function() {
	        charReplace(index, newId, $this);
	        clearTimeout(checkTimeout[index]);
	      });
	      $newObj.on('keydown',function(){
	    	  $this.trigger('keydown');
	      });
	      $newObj.on('keyup',function(){
	    	  $this.trigger('keyup');
		        setTimeout(function(){				
					  var regex = new RegExp('[!^' + opts.character + ']', 'gi');
					  var curValue = $newObj.val();
					  if(curValue.match(regex))
						$newObj.val(curValue.replace(regex, unescape(opts.character)));
				}, opts.transDelay + 100 );
	      });
	      $newObj.on('blur',function(e){
	    	  $this.trigger('blur');
	      });
	      $('label').each(function() {
	        if (this.htmlFor == id) {
	          this.htmlFor = newId
	        } else if ($this[0].parentNode.tagName.toLowerCase() == 'label') {
	          $this[0].parentNode.htmlFor = newId
	        }
	      })
	    });

	    // Return an object of element attributes
	    function extractAttributes(elem) {
	      var attr = elem.attributes,
	        copy = {}, skip = /^jQuery\d+/;
	      for (var i = 0; i < attr.length; i++) {
	        if (attr[i].specified && !skip.test(attr[i].name)) {
	          copy[attr[i].name] = attr[i].value;
	        }
	      }
	      return copy;
	    }

	    /*
	     * check whether all the characters are the same
	     * eg. user input and paste the characters
	     */
	    function checkChange(index, oldId, id, oldValue, $this) {
	      var curValue = $('#' + id).val();
	      if (curValue != oldValue) {
	        saveString(index, oldId, id);
	      } else {
	        charReplace(index, id, $this);
	      }
	      oldValue = curValue;
	      checkTimeout[index] = setTimeout(function() {
	        checkChange(index, oldId, id, oldValue, $this);
	      }, opts.checkInterval)
	    };

	    /*
	     * save string from new input
	     */
	    function saveString(index, oldId, id) {
	      var pos = getCurPos(id);
	      var lastInputChar = false;
	      var inpObj = $('#' + id);
	      var passObj = $('#' + oldId);

	      if($('[id='+oldId+']').length > 1){
	    	  passObj = inpObj.next();
	      }
	      if(inpObj && passObj){
	    	  var inputChars = inpObj.val().split('');
		      var passChars = passObj.val().split('');
		      if (transTimeout[index]) {
		        clearTimeout(transTimeout[index]);
		        transTimeout[index] = null
		      }
		      for (i = 0; i < inputChars.length; i++) {
		        if (inputChars[i] != passChars[i]) {
		          if (inputChars[i] != unescape(opts.character)) {
		            passChars.splice(i, 0, inputChars[i])
		          } else {
		            passChars[i] = passChars[i]
		          }
		        } else {
		          passChars.splice(i, 0, inputChars[i])
		        }
		      }
		      if (inputChars.length < passChars.length) {
		        passChars.splice(pos.start, passChars.length - inputChars.length, '')
		      }
		      for (i = 0; i < inputChars.length; i++) {
		        if (inputChars[i] != unescape(opts.character)) {
		          lastInputChar = i
		        }
		      }
		      for (i = 0; i < inputChars.length; i++) {
		        if (i < lastInputChar) {
		          inputChars[i] = unescape(opts.character)
		        }
		      }
		      inpObj.val(inputChars.join(''));
		      passObj.val(passChars.join(''));
		      setCurPos(id, pos)
	      }
	    };

	    function charReplace(index, id, $this) {
	      var pos = getCurPos(id);
	      var inpObj = $('#' + id);
	      var curValue = inpObj.val();
	      if (!transTimeout[index] && curValue && curValue.match(regex) != null) {
	        transTimeout[index] = setTimeout(function() {
	          inpObj.val(curValue.replace(regex, unescape(opts.character)));
	          //setCurPos(id, pos)
	        }, opts.transDelay)
	      }
	     /* setTimeout(function(){
	    	  if($this.hasClass("error")){
		    	  inpObj.addClass("error")
		      }else{
		    	  inpObj.removeClass("error")
		      }
	      },200);*/
	      /*if(inpObj.hasClass("error")){
	    	  $this.addClass("error")
	      }else{
	    	  $this.removeClass("error")
	      }*/
	    };

	    function getCurPos(id) {
	    	var input = $('#' + id)[0];
	    	var pos = {
	    			start: 0,
	    			end: 0
	    	};
	    	try{
	    		if (input.setSelectionRange) {
	    			pos.start = input.selectionStart;
	    			pos.end = input.selectionEnd
	    		} else if (input.createTextRange) {
	    			var bookmark = document.selection.createRange().getBookmark();
	    			var selection = input.createTextRange();
	    			var before = selection.duplicate();
	    			selection.moveToBookmark(bookmark);
	    			before.setEndPoint('EndToStart', selection);
	    			pos.start = before.text.length;
	    			pos.end = pos.start + selection.text.length;
	    		}

	    	}catch(e){
	    		console.log(e);
	    	}
		   	 return pos;
	    };

	    function setCurPos(id, pos) {
	    	var input = $('#' + id)[0];
	    	try{
	    		if (input.setSelectionRange) {
	    			input.setSelectionRange(pos.start, pos.end);
	    		} else if (input.createTextRange) {
	    			var selection = input.createTextRange();
	    			selection.collapse(true);
	    			selection.moveEnd('character', pos.end);
	    			selection.moveStart('character', pos.start);
	    			selection.select();
	    		}
	    	}catch(e){
	    		console.log(e);
	    	}
	    }
	  }
	})(jQuery);
