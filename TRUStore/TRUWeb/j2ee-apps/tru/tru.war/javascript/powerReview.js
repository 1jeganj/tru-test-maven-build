$(document).ready(function () {
	/* Start: Power review spark changes */
	$(document).on('click', '#pr-reviewdisplay .pr-rd-no-reviews a', function (evt) {
		evt.preventDefault();
	});

	var pageName = $(".pageName").val();
	if (pageName == 'pdpPage') {
		var powerReviewsURL = $("#powerReviewsURL").val();
		try {
			var apiKey = $("#pwr_apiKey").val();
			var pr_locale = $("#pwr_locale").val();
			var merchantGroupId = $("#pwr_merchantGroupId").val();
			var merchantId = $("#pwr_merchantId").val();
			var prproductId = $("#pwr_productId").val();
			if (typeof POWERREVIEWS != 'undefined') {
				POWERREVIEWS.display.render({
					api_key: apiKey,
					locale: pr_locale,
					merchant_group_id: merchantGroupId,
					merchant_id: merchantId,
					page_id: prproductId,
					review_wrapper_url: '/jstemplate/writereview.jsp?pr_page_id=' + prproductId,
					components: {
						ReviewSnippet: 'pr-reviewsnippet',
						ReviewDisplay: 'pr-reviewdisplay',
						QuestionSnippet: 'pr-questions',
						QuestionDisplay: 'pr-questionsdisp'
					}
				});
			}
		} catch (e) { }

		$(document).on('click', '.ask-a-question', function () {
			$.smoothScroll({
				offset: -110,
				scrollTarget: '.question-and-answers'
			});
		});

		$(document).on('click', '#pr-questionsdisp .pr-qa-display-search .pr-qa-display-clear-search-button', function () {
			$('.questionSearchCloseIcon').hide();
		});
	}
	/* End: Power review spark changes */
});

$(window).load(function () {
	/* Start: UI changes for Power review 4.0 */
	var isNewTabflag = false;

	var questionCount = $('#pr-questions .pr-answered-question-circle').text() || 0;
	if (questionCount != 0) {
		$('.ask-a-question').prepend(questionCount);
		$('.ask-a-question').show();
	} else {
		$('.ask-a-question').addClass('pr-question-custom-link').removeClass('ask-a-question').text('ask a question');
		$('.pr-question-custom-link').show();

	}

	/* Start: For ask a question link/button click event bining */
	if (isNewTabflag) {
		$(document).on('click', '.pr-question-custom-link, .pr-question-custom-btn', function (event) {
			$('#pr-questionsdisp .pr-btn-review').click();
		});
	} else {
		$(document).on('click', '.pr-question-custom-link, .pr-question-custom-btn', function (event) {
			var redirectURL = $('.pr-ask-question-link').attr('href');
			var apiKey = $("#pwr_apiKey").val();
			var pr_locale = $("#pwr_locale").val();
			var merchantGroupId = $("#pwr_merchantGroupId").val();
			var merchantId = $("#pwr_merchantId").val();
			var prproductId = $("#pwr_productId").val();
			if (redirectURL === undefined)
				redirectURL = '/jstemplate/askaquestion.jsp?pr_page_id=' + prproductId + '&pr_merchant_id=' + merchantId + '&pr_api_key=' + apiKey + '&pr_merchant_group_id=' + merchantGroupId + '&appName=askQuestion';
			window.open(redirectURL, "_blank");
		});
	}
	/* End: For ask a question link/button click event bining */

	/* Start: For review stars positioning */
	var ratingAvgVal = $('#pr-reviewsnippet .pr-snippet-rating-decimal').text() || 0;
	ratingAvgVal = Math.round(ratingAvgVal);
	var headerRatingPosition = (ratingAvgVal * -32) + 'px';
	var ratingPosition = (ratingAvgVal * -48) + 'px';
	$('#pr-reviewsnippet .pr-rating-stars').css({ 'background-position-y': headerRatingPosition, visibility: 'visible' });
	$('#pr-review-snapshot .pr-snippet-stars-container .pr-rating-stars').css({ 'background-position-y': ratingPosition, visibility: 'visible' });

	var eachReveiw = $('#pr-review-display .pr-snippet-stars.pr-snippet-stars-png');
	replaceReviewStars(eachReveiw);

	var snapshotFaceoff = $('#pr-review-snapshot .pr-review-snapshot-faceoff .pr-snippet-stars.pr-snippet-stars-png');
	replaceReviewStars(snapshotFaceoff);

	$(document).on('click', '.pr-review-snapshot-block-container .pr-ratings-histogram-bar, .pr-review-close-filter', function (event) {
		setTimeout(function () {
			var eachReveiw = $('#pr-review-display .pr-snippet-stars.pr-snippet-stars-png');
			replaceReviewStars(eachReveiw);
		}, 1000);
	});

	/* End: For review stars positioning */

	/* Start: adding write a review link on PDP page */
	var prReviewCount = $('#pr-reviewsnippet .pr-snippet-review-count').text();

	if (isNewTabflag) {
		/* Start: adding write a review link on PDP product header */
		if (prReviewCount == 'No Reviews') {
			$('#pr-reviewsnippet .pr-snippet-review-count').hide();
			$('#pr-reviewsnippet .pr-snippet-write-review-link, #pr-review-display .pr-rd-no-reviews a').text('write a review');
			$('#pr-reviewsnippet .pr-snippet-write-review-link, #pr-review-display .pr-rd-no-reviews').show();
		} else {
			$('#pr-reviewsnippet .pr-snippet-review-count').show();
		}
		/* End: adding write a review link on PDP product header */
	} else {

		if (prReviewCount == 'No Reviews') {
			$('#pr-reviewsnippet .pr-snippet-review-count').hide();
			$('#pr-reviewsnippet .pr-snippet-write-review-link, #pr-review-display .pr-rd-no-reviews a').text('write a review');
			//$('#pr-reviewsnippet .pr-snippet-write-review-link, #pr-review-display .pr-rd-no-reviews').show();

			/* For write a review link */
			var writeReviewLink = '<a href="javascript:void" class="pr-review-custom-link">write a review</a>';
			$('#pr-reviewsnippet .pr-snippet-write-review-link').after(writeReviewLink);

			/* For write a review button */
			var writeReviewBtn = '<div class="pr-review-custom-btn-container"><div class="pr-review-custom-btn">write a review</div></div>';
			$('#pr-reviewdisplay .pr-rd-no-reviews').after(writeReviewBtn);
			$('#pr-reviewdisplay .pr-review-custom-btn-container').css('margin', '0 auto');

			$(document).on('click', '.pr-review-custom-link, .pr-review-custom-btn', function (event) {
				var redirectURL = $('#pr-reviewsnippet .pr-snippet-write-review-link').attr('href');
				window.open(redirectURL, '_blank');
			});
		} else {
			$('#pr-reviewsnippet .pr-snippet-review-count').show();

			$('#pr-review-snapshot .pr-snippet-write-review-link').hide();
			/* For write a review button */
			var writeReviewBtn = '<div class="pr-review-custom-btn-container"><div class="pr-review-custom-btn">write a review</div></div>';
			$('#pr-reviewdisplay #pr-review-snapshot').after(writeReviewBtn);
			$('#pr-reviewdisplay #pr-review-snapshot').css('margin-bottom', '0');

			$(document).on('click', '.pr-review-custom-btn', function (event) {
				var redirectURL = $('#pr-review-snapshot .pr-snippet-write-review-link').attr('href');
				window.open(redirectURL, '_blank');
			});
		}

	}
	/* End: adding write a review link on PDP page */

	$(document).on('click', '.pr-ratings-histogram-bar', function (event) {
		var redirectURL = $(this).attr('data-href');
		window.open(redirectURL, '_blank');
	});

	/* Start: Close icon for question search */
	if ($('.pr-qa-display-searchbar-input').length > 0) {
		var closeIcon = '<span class="questionSearchCloseIcon"></span>';
		$('.pr-qa-display-searchbar-input').parent().append(closeIcon);

		$(document).on('focus', '.pr-qa-display-searchbar-input', function (event) {
			var txt = $(this).val() || '';
			showSearchCrossIcon(txt);
		});

		$(document).on('keyup', '.pr-qa-display-searchbar-input', function (event) {
			var txt = $(this).val() || '';
			showSearchCrossIcon(txt);
		});

		$(document).on('click', '.question-and-answers .questionSearchCloseIcon', function (event) {
			$(document).find('.pr-qa-display-searchbar-input').val('');
			$('.questionSearchCloseIcon').hide();
		});
	}
	/* End: Close icon for question search */

	/* End: UI changes for Power review 4.0 */
});

function showSearchCrossIcon(txt) {
	if (txt != '') {
		$('.questionSearchCloseIcon').show();
	} else {
		$('.questionSearchCloseIcon').hide();
	}
}