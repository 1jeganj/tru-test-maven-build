var clientValidationDeclaration = {};
var errorKeyJson = {};
var contextPath = "";
$(window).load(function () {
	contextPath = $("input.contextPath").val() || '/';
	var urlFullContextPath = $('.urlFullContextPath').val() || $("input.contextPath").val() || '/';
	$.ajax({
		type: "POST",
		cache: false,
		async: false,
		url: urlFullContextPath + 'common/errorMessagesLoad.jsp',
		datatype: "json",
		success: function (data) {
			localStorage.setItem('errorKeyJson', JSON.stringify(data));
			window.invalidCardErroMsg = 'Following are the form errors:' + data.tru_error_card_type_not_valid;
			/*window.invalidCardErroMsgPaymentPage = 'Following are the form errors: tru_error_credit_cardNumber_invalid';*//*Fix for TUW-72545*/
			window.contactErroMsg = 'Following are the form errors:' + data.tru_error_credit_card_number_invalid;
		}
	});
	errorKeyJson = JSON.parse(localStorage.getItem('errorKeyJson'));
	formFieldRules();
	formFieldMessages();
	try { clientValidation(); } catch (e) { }
	$("body").on("paste", "input", function () {
		var $this = $(this);
		setTimeout(function () {
			convertToNormalCharacters($this);
		}, 25);
	});
});
function convertToNormalCharacters($this) {
	if (typeof $this.attr('name') != "undefined") {
		if (!($this.attr('name').indexOf("maskedPassword") > -1)) {
			var inputValue = $this.val();
			var index;
			for (index = 0; index < inputValue.length; index++) {
				if (inputValue.charCodeAt(index) > 127) {
					inputValue = (inputValue.substr(0, index) + '' + inputValue.substr(index + 1));
				}
			}
			$this.val(inputValue);
		}
	}
}

$(document).ready(function () {
	$(document).on("keyup change", "#inStorePickup input", function () {
		var $this = $(this);
		var testId = $this.attr("data-id");
		$this.parent().find("#" + testId).val($this.val());
	});
	$("body").on("blur", "input,select,textarea[name=contactComment]", function () {
		if ($(this).val().trim() == "" && $(this).parents("form").id !== 'saveShippingAddress' && ($(this).parents("form").attr("novalidate") == "novalidate" || $(this).attr("id") == "emailPopupTxt") && $(this).attr("name") != "address2" && $(this).attr("name") != "enterMembershipIDInReview" && $(this).attr("id") != "otherState" && $(this).attr("id") != "enterMembershipIDInCheckout" && $(this).attr("id") != "gcNumber" && $(this).attr("id") != "ean" && $(this).attr("id") != "crdNo2" && $(this).attr("id") != "pin2" && $(this).attr("id") != 'guest-order-number' && $(this).attr("id") != 'enterMembershipID' && !$(this).hasClass("emailAlertsSignUp") && $(this).attr("id") != 'enterMemberIDFromHeader' && $(this).attr("id") != 'promo-code-payment-page' && $(this).attr("name") != 'contactOrderNum') {
			if ($(this).prop("tagName").toLowerCase() == "select") {
				if ($(this).find('option').first().prop('selected')) {
					$(this).addClass("error");
					$(this).removeClass("no-error");
					if ($(this).attr("type") && ($(this).attr("type").indexOf("password") > -1)) {
						$(this).parent().find("").addClass("error");
					}
				} else {
					$(this).removeClass("error");
				}
			} else {
				$(this).addClass("error");
				$(this).removeClass("no-error");
				if ($(this).attr("type") && ($(this).attr("type").indexOf("password") > -1)) {
					$(this).parent().find("").addClass("error");
				}
			}
		} else if ($(this).siblings('label.error').length == 0) {
			$(this).removeClass("error");
			$(this).addClass("no-error");
		}
	});

	$(document).on("show.bs.modal", "#signInModal", function () {
		removeBackendErrorMessagesInline($("#signInModal"));
	});
	if ($(document).find("#oc-signupEmailDiv").length && $(document).find("#oc-signupEmail").length) {
		$(document).find("#oc-signupEmail").val($(document).find("#oc-signupEmailDiv").text().trim());
	}
});

//Some predefined set of rules declaration for element type
function formFieldRules() {
	// Naming-Convention ::: (Element-type)RuleDeclaration
	var fieldRules = {
		name: "required",
		firstName: "required",
		lastName: "required",
		email: {
			required: true,
			emailValidation: true
		},
		altFirstName: "required",
		altLastName: "required",
		altEmail: {
			required: true,
			emailValidation: true
		},
		altPhoneNumber: {
			required: true,
			minlength: 10,
			maxlength: 20,
			numeric: true
		},
		new_email: {
			required: true,
			emailValidation: true
		},
		con_email: {
			required: true,
			emailValidation: true,
			emailMatch: true
		},
		address1: "required",
		city: "required",
		state: "required",
		nickname: "required",
		nickName: "required",
		postalCode: {
			required: true,
			minlength: 5,
			maxlength: 10,
			validZip: true
		},
		phoneNumber: {
			required: true,
			minlength: 10,
			maxlength: 20,
			numeric: true
		},
		gcNumber: {
			required: true,
			maxlength: 16,
			numeric: true
		},
		enterMembershipID: {
			required: true,
			numeric: true
		},
		addMemberFromHeader: {
			required: true,
			numeric: true
		},
		nameOnCard: "required",
		cardNumber: {
			required: true,
			numeric: true
		},
		creditCardNumber: {
			required: true,
			numeric: true
		},
		expirationMonth: "required",
		expirationYear: "required",
		creditCardCVV: {
			required: true,
			cvvLengthMin: true,
			cvvLengthOther: true,
			cvvLengthAmex: true
		},
		password: {
			required: true,
			passwordNoSpaceValidation: true
		},
		oc_password: "required",
		confirm_password: {
			required: true,
			passwordMatch: true,
			passwordNoSpaceValidation: true
		},
		confirmPassword: {
			required: true,
			passwordMatch: true,
			passwordNoSpaceValidation: true
		},
		current_password: {
			required: true,
			passwordNoSpaceValidation: true
		},
		maskedPassword: "required",
		country: "required",
		ean: "required",
		orderEmail: {
			required: true,
			emailValidation: true
		},
		orderNumber: {
			required: true
		},
		orderStatus: "required",
		promoCode: {
			maxlength: 16
		},
		layawayPaymentAmount: "required",
		enterMembershipIDInReview: {
			numeric: true,
			minlength: 13,
			maxlength: 13
		},
		enterMembershipIDInCheckout: {
			numeric: true,
			minlength: 13,
			maxlength: 13
		},
		selectedCardBillingAddress: "required",
		contactSubject: 'required',
		contactComment: 'required'

	}
	clientValidationDeclaration.fieldRules = fieldRules;
}


//Some predefined set of message declaration for the above declared rule types
function formFieldMessages() {
	var errorMsgObj = JSON.parse(localStorage.getItem('errorKeyJson'));
	//Naming-Convention ::: (Element-type)MessageDeclaration
	var fieldMessages = {};
	if (typeof errorMsgObj != 'undefined' && errorMsgObj != null && !$.isEmptyObject(errorMsgObj)) {
		fieldMessages = {
			firstName: errorMsgObj.tru_error_myaccount_firstName,
			lastName: errorMsgObj.tru_error_lastName,
			email: {
				required: errorMsgObj.tru_error_email,
				emailValidation: errorMsgObj.tru_error_invalid_email
			},
			altFirstName: errorMsgObj.tru_error_myaccount_firstName,
			altLastName: errorMsgObj.tru_error_lastName,
			altEmail: {
				required: errorMsgObj.tru_error_alt_email,
				emailValidation: errorMsgObj.tru_error_invalid_email
			},
			altPhoneNumber: {
				required: errorMsgObj.tru_error_phone_number,
				numeric: errorMsgObj.tru_error_numbers_only,
				minlength: errorMsgObj.tru_error_phone_number_invalid_minlength,
				maxlength: errorMsgObj.tru_error_phone_number_invalid_maxlength
			},
			new_email: {
				required: errorMsgObj.tru_error_new_email,
				emailValidation: errorMsgObj.tru_error_invalid_email
			},
			con_email: {
				required: errorMsgObj.tru_error_con_email,
				emailValidation: errorMsgObj.tru_error_invalid_email,
				emailMatch: errorMsgObj.tru_error_account_email_mismatch
			},
			address1: errorMsgObj.tru_error_address,
			city: errorMsgObj.tru_error_city,
			state: errorMsgObj.tru_error_select_state,
			nickname: errorMsgObj.tru_error_nickName,
			nickName: errorMsgObj.tru_error_nickName,
			postalCode: {
				required: errorMsgObj.tru_error_zipCode,
				minlength: errorMsgObj.tru_error_zipCode_invalid_minlength,
				maxlength: errorMsgObj.tru_error_zipCode_invalid_maxlength,
				validZip: errorMsgObj.tru_error_invalid_zipCode
			},
			gcNumber: {
				required: errorMsgObj.tru_error_gcNumber,
				numeric: errorMsgObj.tru_error_numbers_only
			},
			phoneNumber: {
				required: errorMsgObj.tru_error_phone_number,
				numeric: errorMsgObj.tru_error_numbers_only,
				minlength: errorMsgObj.tru_error_phone_number_invalid_minlength,
				maxlength: errorMsgObj.tru_error_phone_number_invalid_maxlength
			},
			enterMembershipID: {
				required: errorMsgObj.tru_error_membership_id,
				numeric: errorMsgObj.tru_error_numbers_only

			},
			addMemberFromHeader: {
				required: errorMsgObj.tru_error_membership_id,
				numeric: errorMsgObj.tru_error_numbers_only

			},
			nameOnCard: errorMsgObj.tru_error_name_on_card,
			cardNumber: {
				required: errorMsgObj.tru_error_ccNumber,
				numeric: errorMsgObj.tru_error_numbers_only
			},
			creditCardNumber: {
				required: errorMsgObj.tru_error_ccNumber,
				numeric: errorMsgObj.tru_error_numbers_only
			},
			expirationMonth: errorMsgObj.tru_error_expiration_month,
			expirationYear: errorMsgObj.tru_error_expiration_year,
			password: {
				required: errorMsgObj.tru_error_password,
				passwordNoSpaceValidation: errorMsgObj.tru_error_account_password_invalid
			},
			oc_password: errorMsgObj.tru_error_password,
			confirm_password:
			{
				required: errorMsgObj.tru_error_confirm_password,
				passwordMatch: errorMsgObj.tru_error_account_password_mismatch,
				passwordNoSpaceValidation: errorMsgObj.tru_error_account_password_invalid
			},
			confirmPassword:
			{
				required: errorMsgObj.tru_error_confirm_password,
				passwordMatch: errorMsgObj.tru_error_account_password_mismatch,
				passwordNoSpaceValidation: errorMsgObj.tru_error_account_password_invalid
			},
			current_password: {
				required: errorMsgObj.tru_error_current_password,
				passwordNoSpaceValidation: errorMsgObj.tru_error_account_password_invalid
			},
			maskedPassword: errorMsgObj.tru_error_password,
			creditCardCVV: {
				required: errorMsgObj.tru_error_cvv_number,
				cvvLengthMin: errorMsgObj.tru_error_cvv_number_invalid_minlength,
				cvvLengthOther: errorMsgObj.tru_error_3_digit_cvv_number,
				cvvLengthAmex: errorMsgObj.tru_error_4_digit_cvv_number,
			},
			country: errorMsgObj.tru_error_select_country,
			otherState: errorMsgObj.tru_error_other_state,
			ean: errorMsgObj.tru_error_gcPin,
			orderEmail: {
				required: errorMsgObj.tru_error_email,
				emailValidation: errorMsgObj.tru_error_invalid_email
			},
			orderNumber: {
				required: errorMsgObj.tru_error_order_number
			},
			orderStatus: errorMsgObj.tru_error_order_number,
			layawayPaymentAmount: errorMsgObj.tru_error_payable_amount,
			enterMembershipIDInReview: {
				numeric: errorMsgObj.tru_error_numbers_only
			},
			enterMembershipIDInCheckout: {
				numeric: errorMsgObj.tru_error_numbers_only
			},
			selectedCardBillingAddress: errorMsgObj.tru_error_select_address,
			contactSubject: 'Please select any subject',
			contactComment: 'Please enter the comment'
		}
	}

	/*var globalMessageDeclarations = {};
	globalMessageDeclarations.nameMessageDeclaration=nameMessageDeclaration;
	globalMessageDeclarations.phoneMessageDeclaration=phoneMessageDeclaration;
	globalMessageDeclarations.emailMessageDeclaration=emailMessageDeclaration;*/
	clientValidationDeclaration.fieldMessages = fieldMessages;
}


//Default handlers for testing
function submitHandlerFunction() {
	console.log("success");
	return false;
}
function invalidHandlerFunction(event, validator) {
	var errors = validator.numberOfInvalids();
	//console.log(errors);
	if (errors) {
		if ($('#myAccountUpdatePasswordModal .errorDisplay').length) {
			$('#myAccountUpdatePasswordModal .errorDisplay').remove();
		}
		var $focusEle = $(validator.errorList[0].element);
		if ($focusEle.prop("tagName").toLowerCase() == "input") {
			($focusEle.attr("type") == "password" && $focusEle.css("display") == "none") ? $focusEle.siblings("[id*=maskedPassword]").focus() : $focusEle.focus();
		} else {
			$focusEle.focus();
		}
		if(event.target.id !== 'confirmSignUpForm') {
			focusOnElement($focusEle);
		}
		for (var i = 0; i < validator.errorList.length; i++) {
			//console.log(validator.errorList[i].message)
			if (typeof utag != 'undefined') {
				utag.link({ 'error_message': validator.errorList[i].message, 'error_code': '' });
			}
		}
	}
	return false;
}
function focusOnElement($focusEle) {
	if ($focusEle.parents("#my-account-popover-struct").length == 0) {
		setTimeout(function () {
			var defaultMargin = 70;
			var basePosition = ($(".header-part.fixed-nav").length) ? $(".header-part.fixed-nav").outerHeight(true) : 0;
			if ($focusEle.parents(".tse-scroll-content").length) {
				var positionTop;
				if ($focusEle.prop("tagName").toLowerCase() == "input") {
					($focusEle.attr("type") == "password" && $focusEle.css("display") == "none") ? (positionTop = ($focusEle.siblings("[id*=maskedPassword]").position().top == 0) ? $focusEle.siblings("[id*=maskedPassword]").offset().top + $focusEle.siblings("[id*=maskedPassword]").parents(".tse-scroll-content").scrollTop() : $focusEle.siblings("[id*=maskedPassword]").position().top) : (positionTop = ($focusEle.position().top == 0) ? $focusEle.offset().top + $focusEle.parents(".tse-scroll-content").scrollTop() : $focusEle.position().top);
				} else {
					positionTop = ($focusEle.position().top == 0) ? $focusEle.offset().top + $focusEle.parents(".tse-scroll-content").scrollTop() : $focusEle.position().top;
				}
				//var positionTop = ($focusEle.position().top == 0) ? $focusEle.offset().top + $focusEle.parents(".tse-scroll-content").scrollTop() : $focusEle.position().top;
				var positionToScroll = ($focusEle.parents("#billing-address-container").length) ? ($focusEle.parents("#billing-address-container").siblings(".row.left-padding").outerHeight(true) + $focusEle.position().top - defaultMargin) : ($focusEle.position().top - defaultMargin);
				if (positionToScroll < 0) {
					positionToScroll = positionTop - (3 * defaultMargin);
				}
				var differenceForSmoothScroll = $focusEle.parents(".tse-scroll-content").scrollTop() - positionToScroll;
				differenceForSmoothScroll = (differenceForSmoothScroll < 0) ? -differenceForSmoothScroll : differenceForSmoothScroll;
				/*$focusEle.parents(".tse-scroll-content").animate({
					scrollTop : positionToScroll
				},differenceForSmoothScroll);*/
				$focusEle.parents(".tse-scroll-content").scrollTop(positionToScroll);
				if ($focusEle.parents(".modal").length == 0) {
					$(window).scrollTop($focusEle.parents(".tse-scroll-content").offset().top - basePosition);
				}
			} else if ($focusEle.parents(".modal").length == 0) {
				if ($focusEle.prop("tagName").toLowerCase() == "input") {
					($focusEle.attr("type") == "password" && $focusEle.css("display") == "none") ? (positionTop = ($focusEle.siblings("[id*=maskedPassword]").position().top == 0) ? $focusEle.siblings("[id*=maskedPassword]").offset().top : $focusEle.siblings("[id*=maskedPassword]").position().top) : (positionTop = $focusEle.offset().top);
				} else {
					positionTop = ($focusEle.position().top == 0) ? $focusEle.offset().top + $focusEle.parents(".tse-scroll-content").scrollTop() : $focusEle.offset().top;
				}
				$(window).scrollTop(positionTop - basePosition - defaultMargin);
			}
		}, 50);
	}
}
//$("#addOrEditCreditCard .tse-scroll-content").scrollTop($("#billing-address-container").siblings(".row.left-padding").outerHeight(true) + $("#id-fd81cd17-fccd-6b69-a2c0-6b2f5da651f4").position().top - 70)


//Universal Form Validation For all the attributes
function universalFormValidationHandler() {
	/*var universalFormRules = {
			"name": clientValidationDeclaration.globalRuleDeclarations.nameRuleDeclaration,
			"firstName": clientValidationDeclaration.globalRuleDeclarations.nameRuleDeclaration,
			"lastName": clientValidationDeclaration.globalRuleDeclarations.nameRuleDeclaration,
			"phone": clientValidationDeclaration.globalRuleDeclarations.phoneRuleDeclaration,
			"email": clientValidationDeclaration.globalRuleDeclarations.emailRuleDeclaration,
			"password": clientValidationDeclaration.globalRuleDeclarations.passwordRuleDeclaration
	};
	var universalFormMessages = {
			"name": clientValidationDeclaration.globalMessageDeclarations.nameMessageDeclaration,
			"firstName": clientValidationDeclaration.globalMessageDeclarations.nameMessageDeclaration,
			"lastName": clientValidationDeclaration.globalMessageDeclarations.nameMessageDeclaration,
			"phone": clientValidationDeclaration.globalMessageDeclarations.phoneMessageDeclaration,
			"email": clientValidationDeclaration.globalMessageDeclarations.emailMessageDeclaration,
			"password": clientValidationDeclaration.globalMessageDeclarations.passwordMessageDeclaration

	};*/


	/*var  universalFormErrorPlacement = function(error, element) {
		error.appendTo("div#errors");
	};*/
	var universalFormMessagesErrorElement = "div";

	//Dummy Submit handler. We'll invoke the older version of invoke method only
	var universalFormSubmitHandlerFunction = function () {
		return false;
	}
	var universalFormInvalidHandlerFunction = function () {
		var errors = validator.numberOfInvalids();
		console.log("Number of Error fields: " + errors);
		return false;
	}

	if (!$.isEmptyObject(clientValidationDeclaration)) {

		$(".JSValidation").each(function () {
			/*if($(this).attr("novalidate") != "novalidate"){*/
			if ($(this).attr('novalidate') != "novalidate") {
				var ignoreCase = '';
				var formId = $(this).attr('id');
				if (formId == 'paymentCreditCardForm') {
					ignoreCase = $("#billing-info #email, .co-billing-address input[name='otherState']:hidden,.billingInfoContent input:hidden");
				} else if (formId == 'paymentRusCardForm') {
					ignoreCase = $("#rus-billing-info #email");
				}
				else if (formId == 'addOrEditCreditCard') {
					ignoreCase = $("#addOrEditCreditCard").find("#otherState");
				}
				else if (formId = "paymentBillingAddressForm") {
					ignoreCase = $(".saved-billing-address").find("input[type=hidden]");
				}
				else {
					ignoreCase = "";
				}

				$(this).validate({
					onkeyup: false,
					onfocusout: false,
					ignore: ignoreCase,
					rules: clientValidationDeclaration.fieldRules,
					messages: clientValidationDeclaration.fieldMessages,
					invalidHandler: invalidHandlerFunction
				});
			}
			/*}*/
		});
		$(".JSValidation1").each(function () {
			if ($(this).attr('novalidate') != "novalidate") {
				$(this).validate({
					onkeyup: false,
					onfocusout: false,
					rules: clientValidationDeclaration.fieldRules,
					messages: clientValidationDeclaration.fieldMessages,
					invalidHandler: invalidHandlerFunction
				});
			}
		});
	}

}
$.validator.addMethod("numeric", function (value) {
	return /^[0-9]*$/.test(value)
});
$.validator.addMethod("validZip", function (value, element) {
	var countryValue = $(element).closest('.JSValidation,.JSValidation1').find('#country').val();
	if (countryValue == 'US') {
		return /^[0-9a-zA-Z]{5}(?:-[0-9a-zA-Z]{4})?$/.test(value);
	}
	else {
		return true;
	}
});

$.validator.addMethod("cvvLengthMin", function (value, element) {

	var cardTypeForCvvTemp = '';
	var cardNumForm = $(element).parents('form');

	if (cardNumForm.find('.card-number').length == 0) {
		cardTypeForCvvTemp = cardNumForm.find('#creditCardType').val() || '';
	} else {
		cardTypeForCvvTemp = cardTypeForCvvValidation;
	}

	if ((cardTypeForCvvTemp == '' || cardTypeForCvvTemp == 'invalid') && cardTypeForCvvTemp != 'americanExpress' && value.length < 3) {
		return false;
	} else {
		return true;
	}
});
$.validator.addMethod("cvvLengthOther", function (value, element) {

	var cardTypeForCvvTemp = '';
	var cardNumForm = $(element).parents('form');

	if (cardNumForm.find('.card-number').length == 0) {
		cardTypeForCvvTemp = cardNumForm.find('#creditCardType').val() || '';
	} else {
		cardTypeForCvvTemp = cardTypeForCvvValidation;
	}

	if (cardTypeForCvvTemp != '' && cardTypeForCvvTemp != 'invalid' && cardTypeForCvvTemp != 'americanExpress' && value.length != 3) {
		return false;
	} else {
		return true;
	}
});
$.validator.addMethod("cvvLengthAmex", function (value, element) {

	var cardTypeForCvvTemp = '';
	var cardNumForm = $(element).parents('form');

	if (cardNumForm.find('.card-number').length == 0) {
		cardTypeForCvvTemp = cardNumForm.find('#creditCardType').val() || '';
	} else {
		cardTypeForCvvTemp = cardTypeForCvvValidation;
	}

	if (cardTypeForCvvTemp == 'americanExpress' && value.length < 4) {
		return false;
	} else {
		return true;
	}
});



$.validator.addMethod('passwordMatch', function (value, element) {
	var pwd = $(element).closest('form').find("#passwordInput").val();
	if (pwd == value) {
		return true;
	} else {
		return false;
	}
});
$.validator.addMethod('emailMatch', function (value, element) {
	var email = $(element).closest('form').find("#new_email").val();
	if (email == value) {
		return true;
	} else {
		return false;
	}
});
$.validator.addMethod("emailValidation", function (value) {
	return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value.trim())
});
$.validator.addMethod("passwordNoSpaceValidation", function (value) {
	return /^\S*$/.test(value)
});
$.validator.prototype.elements = function () {
	var validator = this,
		rulesCache = {};

	// select all valid inputs inside the form (no submit or reset buttons)
	return $(this.currentForm)
		.find("input, select, textarea")
		.not(":submit, :reset, :image, [disabled]")
		.not(this.settings.ignore)
		.filter(function () {
			if (!this.name && validator.settings.debug && window.console) {
				console.error("%o has no name assigned", this);
			}

			// select this element if this has the same name as one in cache (may be was dynamically added)
			if (this.name in rulesCache) {
				if (!validator.objectLength($(this).rules())) {
					$(this).rules('add', rulesCache[this.name]);
				}
				return true;
			}

			// select only the element with rules specified
			if (!validator.objectLength($(this).rules())) {
				return false;
			}

			// Add rules to the cache
			rulesCache[this.name] = $(this).rules();
			return true;
		});
}
//Create Account Form validations
function createAccountFormValidations() {
	var errorPlacement = function (error, element) {
		error.appendTo("div#errors");
	};
	var createAccountFormMessagesErrorElement = "div";

	var createAccountFormSubmitHandlerFunction = function () {
		/*$('#email, #password, #passwordmaskedPassword1').val('');
		if($('#createAccountForm').valid()){*/
		onSubmitCreateAccountForm();
		/*}*/
		return false;
	}
	var createAccountFormInvalidHandlerFunction = function () {
		var errors = validator.numberOfInvalids();
		console.log("Number of Error fields: " + errors);
		return false;
	}


	$("#createAccountForm").validate({
		ignore: "",
		onkeyup: false,
		onfocusout: false,
		rules: clientValidationDeclaration.fieldRules,
		messages: clientValidationDeclaration.fieldMessages,
		invalidHandler: invalidHandlerFunction,
		submitHandler: createAccountFormSubmitHandlerFunction
	});
}
function myAccountCheckBalanceFormValidations() {
	/*var  errorPlacement = function(error, element) {
		error.appendTo("#myAccountCheckBalanceForm .global-error-display");
	};*/
	$("#myAccountCheckBalanceForm").validate({
		ignore: "",
		onkeyup: false,
		onfocusout: false,
		errorPlacement: function (error, element) {
			error.appendTo("#myAccountCheckBalanceForm .global-error-display");
		},
		rules: clientValidationDeclaration.fieldRules,
		messages: clientValidationDeclaration.fieldMessages,
		invalidHandler: invalidHandlerFunction,
	});
}

function checkMaskPasswordError($this) {
	$this.find("input[name*='maskedPassword']").each(function () {
		if ($(this).next().hasClass("error")) {
			$(this).addClass("error");
		} else {
			$(this).removeClass("error");
		}
	});
}

function addBackendErrorsToForm(response, $formRef) {
	var formattedErrorMsgs = backendErrorMsgFilter(response);
	var $focusEle = null;
	removeBackendErrorMessagesInline($formRef);
	for (i = 0; i < formattedErrorMsgs.errors.length; i++) {
		if (formattedErrorMsgs.errors[i].displayPlace == "inline") {
			//var displayElement = formattedErrorMsgs.errors[i].displayElement;
			var $displayElement = findDisplayElement($formRef, formattedErrorMsgs.errors[i].displayElement);
			addErrorMessage($displayElement, formattedErrorMsgs.errors[i].errorMessage);
			$focusEle = $displayElement;
		} else {
			if ($('#checkout-container').length) {
				if ($formRef.hasClass('addressDoctor') || ($('.modal:not(#checkoutReviewProcessingOverlay)').is(':visible') && !$(".modal#shipping-page-suggested-address").is(":visible")) || $formRef.attr('id') == 'addOrEditCreditCard' || $formRef.attr('id') == 'giftCardBalanceForm' || $formRef.attr('id') == 'promoCodePaymentPageForm') {
					$formRef.find(".global-error-display").html(formattedErrorMsgs.errors[i].errorMessage).show();
					$focusEle = $formRef.find(".global-error-display");
				}
				else {
					var errorMessage = formattedErrorMsgs.errors[i].errorMessage;
					if (errorMessage.indexOf('tru_informational_error') > -1) {
						var errorMsg = errorMessage.replace('tru_informational_error', '');
						$formRef.find(".global-error-display").html(errorMsg).show();
						$focusEle = $formRef.find(".global-error-display");
					} else {
						showCheckoutOopsError(errorMessage);
						$focusEle = $(".checkout-error-state");
					}
				}
			}
			else if ($formRef.attr("id") == "billingInfoApplicationForm" || $formRef.attr("id") == "creditCardInfoApplicationForm" || $formRef.attr("id") == "layawayPaymentAmountForm") {
				$(document).find("#layawayGlobalErrorDisplay").html(formattedErrorMsgs.errors[i].errorMessage).show();
				$focusEle = $(document).find("#layawayGlobalErrorDisplay");
			}
			else {
				if ($formRef.attr('id') == 'layawayGlobalErrorDisplay') {
					$formRef.html(formattedErrorMsgs.errors[i].errorMessage).show();
					$focusEle = $formRef;
				} else {
					$formRef.find(".global-error-display").html(formattedErrorMsgs.errors[i].errorMessage).show();
					$focusEle = $formRef.find(".global-error-display");
				}
			}
		}
	}
	focusOnElement($focusEle);
	checkMaskPasswordError($formRef);
}

function showCheckoutOopsError(errorMsg) {
	var shipRestrictionsFound = $('#shipRestrictionsFound').val() || '';
	$('.checkout-error-state .checkout-dynamic-error').html(errorMsg);
	$('.checkout-error-state').show();
	if (shipRestrictionsFound == 'true') {
		$('.checkout-error-state .checkout-error-state-subheader.shipRestriction').show();
		$('.checkout-error-state .checkout-error-state-subheader.noRestriction').hide();
	} else {
		$('.checkout-error-state .checkout-error-state-subheader.shipRestriction').hide();
		$('.checkout-error-state .checkout-error-state-subheader.noRestriction').show();
	}
	//$(window).scrollTop(0);
}

function removeBackendErrorMessagesInline($formRef) {
	$formRef.find("input.error").removeClass("error");
	$formRef.find("input+label.error").remove();
	$formRef.find(".global-error-display").hide();
	$formRef.find("select.error").removeClass("error");
	$formRef.find("select+label.error").remove();
	$('.checkout-error-state').hide();

	if ($("#order-review-global-error").length) {
		$("#order-review-global-error").hide();
	}
	if ($("#layawayGlobalErrorDisplay").length) {
		$("#layawayGlobalErrorDisplay").hide();
	}
	if ($('#co-shipping-tab').length) {
		$('.errorDisplay').remove();
		$('.product-information .shipping-multiple-addresses select.error').removeClass('error');
		$('.product-information .shipping-multiple-addresses .error-highlight').removeClass('error-highlight');
	}
}
function findDisplayElement($formRef, displayElement) {
	var firstRefDisplayElement = displayElement.split("_")[0];
	/*if($formRef.find("input[name='"+firstRefDisplayElement+"']").length){
		return $formRef.find("input[name='"+firstRefDisplayElement+"']");*/
	var $ele = null;
	$formRef.find("input").each(function () {
		if ($(this).attr("name") != undefined) {
			if (($(this).attr("name").toLowerCase() == firstRefDisplayElement.toLowerCase() || $(this).attr("name").toLowerCase() == displayElement.toLowerCase()) && !($(this).attr("name").toLowerCase().indexOf("maskedpassword") > -1)) {
				if ($ele == null) {
					$ele = $(this);
				}
			}
		}
	});
	return $ele;
	/*}else{
		return $formRef.find("input[name='"+displayElement+"']");
	}*/
}
function addErrorMessage($displayElement, errorMessage) {
	$displayElement.addClass("error");
	$displayElement.after(constructErrorMessage(errorMessage));
}
function constructErrorMessage(errorMessage) {
	return "<label class='error'>" + errorMessage + "</label>";
}
function backendErrorMsgFilter(response) {
	//response = '<div>'+response+'</div>';
	//var $response = $(response);
	var formattedErrorMsgs = {};
	formattedErrorMsgs.errors = [];
	/*var errorResponse = response.split("Following are the form errors:")[1].trim;*/
	/*$response.find("span").each(function(){*/
	//var errorKey = $(this).text().trim();
	var errorKey = response.split("Following are the form errors:")[1].trim();
	var errorKeyArray = errorKey.split(" ");
	var errorValue = {};
	if (errorKeyArray.length > 1) {
		errorValue.displayPlace = "global";
		errorValue.errorMessage = errorKey;
	} else {
		errorValue = errorMsgInlineDistinguisher(errorKey);
	}
	formattedErrorMsgs.errors.push(errorValue);
	/*});*/
	return formattedErrorMsgs;
}
function errorMsgInlineDistinguisher(errorMsg) {
	var errorValue = {};
	if (errorKeyJson.hasOwnProperty(errorMsg)) {
		errorValue.displayPlace = "inline";
		errorValue.errorMessage = errorKeyJson[errorMsg];
		errorValue.displayElement = getDisplayElement(errorMsg);
	} else {
		errorValue.displayPlace = "global";
		errorValue.errorMessage = errorMsg;
	}
	return errorValue;
}
function getDisplayElement(errorMsg) {
	var errorMsgArray = errorMsg.split("_");
	if (errorMsgArray.length > 4) {
		return errorMsgArray[3] + "_" + errorMsgArray[4];
	} else {
		return errorMsgArray[3];
	}
}

function clientValidation() {
	//Universal Form Validation Handler with universalFormValidation class
	universalFormValidationHandler();


	//My Account Login Page Validations
	createAccountFormValidations();
	myAccountCheckBalanceFormValidations();

}
