
<dsp:page>
<dsp:getvalueof var="sosEmpName" param="sosEmpName"/>
<dsp:getvalueof var="sosStoreNumber" param="sosStoreNumber"/>
<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:droplet name="/com/tru/common/droplet/TRUSOSRedirectCookieDroplet">
		<dsp:param name="sosEmpName" param="sosEmpName"/>
		<dsp:param name="sosStoreNumber" param="sosStoreNumber"/>
		<dsp:param name="urlCatID" param="urlCatID"/>
		<dsp:param name="cat" param="cat"/>
		<dsp:param name="order" bean="ShoppingCart.current"/>
		<dsp:oparam name="output">
			<dsp:include page="/cart/includeScripts.jsp"/>
			<dsp:include page="/header/storeLocatorPopup.jsp"/>
			<div class="modal fade" id="removeItemConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="basicModal"
				aria-hidden="true">
				<dsp:include page="/cart/removeLineItem.jsp">
					<dsp:param name="commerceItemId" param="commerceItemId" />
				</dsp:include>
			</div>
			<div class="modal fade in shoppingcart-findinstore" id="findInStoreModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
				<dsp:include page="/jstemplate/findInStoreInfo.jsp"/>
			</div>
			<div class="modal editField fade" id="makemystoreModel"	tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
				<div class="modal-dialog modal-md">
					<div class="modal-content sharp-border">
						<div class="makemystoreModel-popup">
							<div class="row">
								<div class="col-md-5 col-md-offset-7">
									<div class="row top-margin">
										<div class="col-md-1 col-md-offset-10">
											<span class="clickable"><img data-dismiss="modal" src="${TRUImagePath}images/close.png" alt="close model"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="makemystoreModel-popup-desc">
										<h3 class="w3ValidationMyStore">My Store</h3>
										<p>
											We would like to make <a href="javascript:void(0)" class="makemystoreModel-address">Cherry Wood</a> your
											store?
										</p>
										<p>
											Product availablity and pickup option will be displayed for
											this store.<br>You can change this store anytime using
											the store locator or My Store Link.
										</p>
									</div>
									<div class="action-buttons makemystoeModel-section">
										<button id="makemystoreModel-yes">Make this My Store</button>
										<button data-dismiss="modal">Cancel</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
			<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
			<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
			<dsp:importbean bean="com/tru/common/TRUStoreConfiguration"/>
			<dsp:getvalueof var="staticAssetsVersionFormat" bean="TRUStoreConfiguration.staticAssetsVersion"/>
			<dsp:getvalueof value='<%=atg.nucleus.DynamoEnv.getProperty("staticAssetVersion")%>' var="versionNumbers"/>
			<input type="hidden" class="contextPath" value="${contextPath}">
			<dsp:include page="/jstemplate/resourceBundle.jsp"></dsp:include>
			<c:choose>
				<c:when test="${versionFlag}">
					<script type="text/javascript" charset="UTF-8"
						src="${TRUJSPath}javascript/cartPageFooter.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
				</c:when>
				<c:otherwise>
					<script type="text/javascript" charset="UTF-8"
						src="${TRUJSPath}javascript/cartPageFooter.js"></script>
				</c:otherwise>
			</c:choose>
			<style>
				.sosRedirectPage {
					margin: 0 50px;
				}
				.sos-redirect-product-block {
					width: 65%;
				}

				.sos-redirect-product-block .remove-product-item {
					float: right;
				}
			</style>
			<div class="sosRedirectPage">
				<dsp:droplet name="ForEach">
					<dsp:param name="array" param="items" />
					<dsp:param name="elementName" value="item" />
					<dsp:oparam name="output">
						<div class="sos-redirect-product-block">
							<dsp:getvalueof var="productId" param="item.productId"/>
							<input type="hidden"  class="productIdForCart" value="${productId}"/>
							<dsp:getvalueof var="relationshipItemId" param="item.id" />
							<dsp:getvalueof var="metaInfoId" param="item.metaInfoId" />
							<dsp:getvalueof var="locationId" param="item.locationId"/>
						
							<div class="product-information-header">
								<div class="product-header-title">
									<a class="productName"><dsp:valueof param="item.displayName" /></a>
								</div>
								<div class="product-header-price">
									<dsp:valueof param="item.totalAmount" converter="currency" />
								</div>
							</div>
		
							<div class="product-information-content">
								<div class="row row-no-padding">
									<div class="col-md-2">
										<div>
											<dsp:getvalueof var="productImage" param="item.productImage" />
											<dsp:droplet name="IsEmpty">
												<dsp:param name="value" value="${productImage}" />
												<dsp:oparam name="false">
													<dsp:droplet
														name="/com/tru/common/droplet/TRUAkamaiImageDroplet">
														<dsp:param name="imageName" value="${productImage}" />
														<dsp:param name="imageHeight" value="124" />
														<dsp:param name="imageWidth" value="124" />
														<dsp:oparam name="output">
															<dsp:getvalueof var="akamaiImageUrl" param="akamaiUrl" />
														</dsp:oparam>
													</dsp:droplet>
													<a class="displayImage" href="javascript:void(0);" aria-label="shoping cart product image"><img class="product-description-image" src="${akamaiImageUrl}"
														alt="product image"></a>
												</dsp:oparam>
												<dsp:oparam name="true">
													<a class="displayImage" href="javascript:void(0);" aria-label="shoping cart product image"><img class="product-description-image image-not-available"
														src="${TRUImagePath}images/no-image500.gif" alt="product image"></a>
												</dsp:oparam>
											</dsp:droplet>
										</div>
									</div>
									<dsp:getvalueof param="item.quantity" var="qty"/>
									<div>Item #: <dsp:valueof param="item.skuId"/></br>
									Quantity:${qty}</div></br>
									<a data-toggle="modal" data-target="#findInStoreModal" href="javascript: void(0);"
									onclick="javascript: return loadFindInStore('/tru', '<dsp:valueof param='item.skuId'/>','${relationshipItemId}_${metaInfoId}','<dsp:valueof param='item.quantity'/>', this,'cartFlag');">Select Store</a>
									<dsp:getvalueof var="removalArray" value="${relationshipItemId}_${qty}_remove_${giftWrapSkuId}"/>
									<dsp:a href="javascript:void(0);" class="remove-product-item" id="${removalArray}">Remove</dsp:a>
								</div>
							</div>
							<hr/>
						</div>
					</dsp:oparam>
				</dsp:droplet>
			</div>
		</dsp:oparam>
		<dsp:oparam name="empty"/>
	</dsp:droplet>
</dsp:page>




