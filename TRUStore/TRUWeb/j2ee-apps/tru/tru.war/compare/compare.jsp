<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/commerce/catalog/comparison/ProductList" />
	<dsp:getvalueof var="contextPath"	bean="/OriginatingRequest.contextPath" />
	<dsp:getvalueof var="currentCategoryId"	bean="ProductList.currentCategoryId" />
	<fmt:setBundle	basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<div id="compareItemsBlock">
	<dsp:droplet name="/com/tru/commerce/droplet/TRUActiveProductsForCompareDroplet">
		<dsp:param name="productComparisonList" bean="ProductList.items" />
		<dsp:param name="categoryId" param="categoryId" />
		<dsp:oparam name="output" />
		<dsp:getvalueof var="activeProductList" param="activeProductList" />
	</dsp:droplet>
	  <dsp:droplet name="/com/tru/commerce/droplet/TRUCompareProductInfoLookupDroplet">
				<dsp:param name="productComparisonList" bean="ProductList.items" />
				<dsp:param name="categoryId" param="categoryId" />
					<dsp:param name="siteId" bean="/atg/multisite/Site.id" />
					<dsp:oparam name="output">
						<dsp:getvalueof param="compareProductInfo.productInfoList" var="productInfoList" />
						<dsp:getvalueof param="compareAttributeList" var="compareAttributeList" />
						<dsp:getvalueof param="setOfAttributeName" var="setOfAttributeName" />
						
						<dsp:droplet name="/atg/dynamo/droplet/ForEach">
                          <dsp:param name="array" value="${productInfoList}" />
                          <dsp:getvalueof param="element" var="productInfo"/>
                          <dsp:oparam name="output">
			                  <dsp:include page="/compare/compareProductInfo.jsp">
			                <dsp:param name="productInfo" value="${productInfo}"/>
			             	</dsp:include>
			             	
			             	</dsp:oparam>
			          </dsp:droplet>
			      <dsp:include page="DisplayComparisonProductAttribute.jsp">
					<dsp:param name="compareAttributeList" value="${compareAttributeList}" />
					<dsp:param name="setOfAttributeName" value="${setOfAttributeName}" />										
				</dsp:include>
			       </dsp:oparam>
			       <dsp:oparam name="empty">
					</dsp:oparam>
		</dsp:droplet>
	</div>
</dsp:page> 