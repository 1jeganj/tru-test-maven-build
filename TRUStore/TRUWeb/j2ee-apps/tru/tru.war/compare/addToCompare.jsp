<%-- This page is used to add products to the productList, requires productID and categoryID--%>

<dsp:page>
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
	<dsp:getvalueof var="productIdParameterName" bean="TRUStoreConfiguration.productIdParameterName" />
	<dsp:getvalueof var="categoryIdParameterName" bean="TRUStoreConfiguration.categoryIdParameterName" />
	<dsp:getvalueof var="imageUrlParameterName" bean="TRUStoreConfiguration.imageUrlParameterName" />	
	<dsp:getvalueof var="productId" param="productId" />
	<dsp:getvalueof var="categoryId" param="categoryId" />
	<dsp:getvalueof var="skuId" param="skuId" />
	<dsp:getvalueof var="imgUrl" param="imgUrl" />
	<dsp:getvalueof var="stylizedItem" param="stylizedItem" />
	<%-- This page is used to add products to the productList, requires productID and categoryID--%>
	<dsp:droplet name="/com/tru/commerce/droplet/TRUAddItemsToCompareListDroplet">
		<dsp:param name="${productIdParameterName}" value="${productId}" />
		<dsp:param name="${categoryIdParameterName}" value="${categoryId}" />
		<dsp:param name="skuId" value="${skuId}" />
		<dsp:param name="imgUrl" value="${imgUrl}" />
		<dsp:param name="stylizedItem" value="${stylizedItem}" />
		<dsp:param name="productList" bean="/atg/commerce/catalog/comparison/ProductList" />
		<dsp:param name="requestType" value="addProduct" />
		<dsp:oparam name="output">
		</dsp:oparam>
		<dsp:oparam name="error">
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>