<dsp:page>
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
	<dsp:importbean bean="/atg/commerce/catalog/comparison/ProductList" />
	<dsp:importbean bean="/atg/dynamo/droplet/Redirect" />
	<dsp:getvalueof var="previousPageUrl" bean="ProductList.previousPageUrl" />
	
	<%-- <link rel="stylesheet" property="stylesheet" href="${TRUCSSPath}css/comparePagePrint.css"> --%>
	
	<dsp:droplet name="/com/tru/commerce/droplet/TRUActiveProductsForCompareDroplet">
		<dsp:param name="productComparisonList" bean="ProductList.items" />
		<dsp:oparam name="output" />
		<dsp:getvalueof var="activeProductList" param="activeProductList" />
	</dsp:droplet>
	
	<dsp:getvalueof var="productListLength" value="${fn:length(activeProductList)}" />
	
	<c:if test="${productListLength eq 1 }">
		<dsp:droplet name="Redirect">
			<dsp:param name="url" value="${previousPageUrl}" />
		</dsp:droplet>
	</c:if>

	
	<div class="compare-overlay-menu">
	<div class="siteLogoForPrintPage">
    	<span><img src="${TRUImagePath}images/tru-logo-sm.png" alt="toysrus logo"></span>
		<span> <img src="${TRUImagePath}images/babiesrus-logo-small.png" alt="babiesrus logo"></span>
	</div>
	<div class="disabled-mode-compare-overlay-menu"></div>
		<div class="compare-overlay">
			<div class="compare-overlay-locked">
				<div class="row compare-display-image remove-compare-product">
					<dsp:include page="/compare/howdoTheyCompare.jsp"/>
					<a href="javascript:void(0)" class="compare-overlay-close" data-target=".compare-overlay-menu">
						<span class="sprite-icon-x-close"></span>
					</a>
				</div>
				<hr class="horizontal-divider">
				<div class="row compare-display-heading-text remove-compare-product">
					<div class="col-md-3"></div>
				</div>
				<div class="row compare-display-addTocart remove-compare-product">
					<div class="col-md-3"></div>
				</div>
				<hr>
			</div>
			<div class="compare-overlay-scroll">
			<div class="compare_overlay_label_list">
				<c:forEach var="attriMap" items="${productAtributeValues}">
					<c:forEach var="labels" items="${attriMap}">
						<div class="compare_overlay_label">${labels.key}</div>
					</c:forEach>
				</c:forEach>
			</div>
			</div>
					
		</div>
	</div>
</dsp:page>