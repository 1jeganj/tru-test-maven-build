<dsp:page>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
	<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
    <fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	<dsp:getvalueof var="customerEmail" bean="Profile.login"/>
	<dsp:getvalueof var="customerID" bean="Profile.id"/>
	<!-- <div id="narrow-by-scroll" class="product-content"> -->
        <!-- <div class="fixed-narrow-menu">  -->           
        <%-- <dsp:include page="/cartridges/TRUGuidedNavigation/TRUGuidedNavigation.jsp"/> --%>
         
			<div class="compare-product-selection">
                <div class="row">
                    <%-- <dsp:include page="/compare/howdoTheyCompare.jsp"/> --%>
                    <!-- <div class="col-md-1">
                    </div>	 -->	
								   <div class="compare-images">
			                    	<div class="added-compare-images">	                    
				                    	 <div class="col-md-10-percent no-compare-image image-1">
				                    	 	<span class="no-compare-text">select a product below to compare</span>
					                        <%-- <img class="imageCompareProduct" src="${TRUImagePath}images/product-compare-3.jpg" alt="product compare">
					                        <img class="close-compare-image" src="${TRUImagePath}images/close-icon-sm.png" alt="remove"> --%>
					                        <input type="hidden" value="" class="imageCompareProductId"/>
					                        <input type="hidden" value="" class="imageCompareSkuId"/>
					                    </div>
					                    <div class="col-md-10-percent no-compare-image image-2">
					                    <span class="no-compare-text">select a product below to compare</span>
					                        <%-- <img class="imageCompareProduct" src="${TRUImagePath}images/product-compare-3.jpg" alt="product compare">
					                        <img class="close-compare-image" src="${TRUImagePath}images/close-icon-sm.png" alt="remove"> --%>
					                        <input type="hidden" value="" class="imageCompareProductId"/>
					                        <input type="hidden" value="" class="imageCompareSkuId"/>
					                    </div>
					                    <div class="col-md-10-percent no-compare-image image-3">.
					                    <span class="no-compare-text">select a product below to compare</span>
					                        <%-- <img class="imageCompareProduct" src="${TRUImagePath}images/product-compare-3.jpg" alt="product compare">
					                        <img class="close-compare-image" src="images/close-icon-sm.png" alt="remove"> --%>
					                        <input type="hidden" value="" class="imageCompareProductId"/>
					                        <input type="hidden" value="" class="imageCompareSkuId"/>
					                    </div>
					                    <div class="col-md-10-percent no-compare-image image-4">
					                    <span class="no-compare-text">select a product below to compare</span>
					                        <%-- <img class="imageCompareProduct" src="${TRUImagePath}images/product-compare-3.jpg" alt="product compare">
					                        <img class="close-compare-image" src="${TRUImagePath}images/close-icon-sm.png" alt="remove"> --%>
					                        <input type="hidden" value="" class="imageCompareProductId"/>
					                        <input type="hidden" value="" class="imageCompareSkuId"/>
					                    </div>
				                    </div>				                   
			                    </div>		
			                   <!--   <div class="col-md-1">
				                    </div> -->
				                    <div class="col-md-2">
				                        <button id="compare-products-button" disabled="" ><fmt:message key="tru.productdetail.label.comparenow" /></button>
				                    </div>
			                    
			                      <div class="col-md-2">
			                        <button id="remove-products-button" class="blue-link">
			                        	remove all <img src="/images/i-c-n-close.png" class="remove-icon" alt="remove all" />
					       	 		</button>
			                    </div>
			                    <input type="hidden" value="" id="tealiumComp_customerEmail"/>
					            <input type="hidden" value="" id="tealiumComp_customerID"/>
                </div>              
            </div>
           </div>
            	<div class="sticky-menu-placeholder"></div>
          <%--   
            <dsp:include page="productCompareResults.jsp"/> --%>
            
        <!-- </div> -->
<!-- </div> -->
<div id="compareImageContainerTemplate">
	<div class="col-md-10-percent compare-image image-1">	
		<c:choose>
			 <c:when test="${not empty akamaiNoImageURL}">
			  	<img class="imageCompareProduct" src="${akamaiNoImageURL}" alt="">
			</c:when>
			 <c:otherwise>
			 	<img class="imageCompareProduct" src="${TRUImagePath}images/no-image500.gif" alt=""> 
			 </c:otherwise>
		</c:choose>
		<div class="compareProductName"></div>
		<!-- <span class="imageCompareProduct"></span> -->
		<a href="javascript:void(0)" class="close-compare-image">
			<img src="${TRUImagePath}images/close-icon-sm.png" alt=""> 
		</a>
		<input type="hidden" class="imageCompareProductId" />
		<input type="hidden" class="imageCompareSkuId" />
		<input type="hidden" class="imageCompareStylizedItem"  />
	</div>
</div>


</dsp:page>