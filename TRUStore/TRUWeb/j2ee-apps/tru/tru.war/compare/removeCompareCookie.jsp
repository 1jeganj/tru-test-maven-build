<%-- This page is used to add products to the productList, requires productID and categoryID--%>

<dsp:page>
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
		<dsp:getvalueof var="categoryIdParameterName" bean="TRUStoreConfiguration.categoryIdParameterName" />
	
	
	
	<dsp:getvalueof var="categoryId" param="categoryId" />
	
	
	
	<%-- This page is used to add products to the productList, requires productID and categoryID--%>


	<dsp:droplet name="/com/tru/commerce/droplet/TRURemoveCompareCookieDroplet">
		
		<dsp:param name="${categoryIdParameterName}" value="${categoryId}" />
		
		<dsp:oparam name="output">
		</dsp:oparam>
		<dsp:oparam name="error">
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>