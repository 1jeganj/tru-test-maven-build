<%-- This page is used to add products to the productList, requires productID and categoryID--%>

<dsp:page>
	<dsp:importbean bean="/atg/commerce/catalog/comparison/ProductList" />
	<dsp:importbean bean="/atg/commerce/catalog/comparison/ProductListHandler" />
	<dsp:getvalueof param="categoryId" var="categoryId"></dsp:getvalueof>
	<dsp:setvalue bean="ProductListHandler.categoryID" value="${categoryId}" />
	<dsp:setvalue bean="ProductListHandler.clearList" value="true" />
</dsp:page>