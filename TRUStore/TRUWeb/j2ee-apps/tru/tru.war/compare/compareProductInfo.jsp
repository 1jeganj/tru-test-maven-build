<dsp:page>
 <fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
 <dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
 <dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
 <dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
 <dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
 <dsp:importbean bean="/atg/multisite/Site" />
 <dsp:getvalueof var="siteID" bean="Site.id" />
 <dsp:getvalueof param="productInfo" var="productInfo" />
 <dsp:getvalueof param="productInfo.defaultSKU.primaryImage" var="primaryImage" />	
 <dsp:getvalueof param="productInfo.productId" var="productId" />
 <dsp:getvalueof param="productInfo.defaultSKU.id" var="skuId" />
 <dsp:getvalueof param="productInfo.defaultSKU.displayName" var="displayName" />
 <dsp:getvalueof param="productInfo.defaultSKU.longDescription" var="longDescription" />
 <dsp:getvalueof param="productInfo.defaultSKU.unCartable" var="unCartable"/>
 <dsp:getvalueof param="productInfo.defaultSKU.inventoryStatus" var="inventoryStatus" />
 <dsp:getvalueof param="productInfo.defaultSKU.notifyMe" var="notifyMe" />
 <dsp:getvalueof param="productInfo.colorSizeVariantsAvailableStatus" var="colorSizeVariantsAvailableStatus" />
 <dsp:getvalueof param="productInfo.defaultSKU.onlinePID" var="onlinePID" />
 <dsp:getvalueof param="productInfo.defaultSKU.reviewRating" var="reviewRating" />
 <dsp:getvalueof param="productInfo.defaultSKU.reviewRatingFraction" var="reviewRatingFraction" />
 <dsp:getvalueof param="productInfo.defaultSKU.suggestAgeMessage" var="suggestAgeMessage" />
 <dsp:getvalueof param="productInfo.defaultSKU.s2s" var="s2s" />
 <dsp:getvalueof param="productInfo.defaultSKU.ispu" var="ispu" />
 <dsp:getvalueof param="productInfo.defaultSKU.priceDisplay" var="priceDisplay" />
 <dsp:getvalueof param="productInfo.defaultSKU.unCartable" var="unCartable" />
 <dsp:getvalueof param="productInfo.defaultSKU.inventoryStatusInStore" var="inventoryStatusInStore" />
 <dsp:getvalueof param="productInfo.defaultSKU.channelAvailability" var="channelAvailability" />
<c:if test="${(channelAvailability eq 'N/A') || (empty channelAvailability)}">
	<dsp:getvalueof value="both" var="channelAvailability" />
</c:if>
 <c:if test="${empty s2s}">
<dsp:getvalueof value="N" var="s2s" />
</c:if>
<c:if test="${empty ispu}">
<dsp:getvalueof value="N" var="ispu" />
</c:if>
 <c:choose>
	<c:when test="${not empty pdpH1}">
        <c:set var="altH1SkuName" value="${pdpH1}"/>  
	</c:when>
	<c:otherwise>
		<c:set var="altH1SkuName" value="${displayName}"/>
	</c:otherwise>
</c:choose>
	<dsp:droplet name="/com/tru/commerce/locations/TRUGetCookieDroplet">
		<dsp:param name="cookieName" value="favStore" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="cookieValue" param="cookieValue" />
			<dsp:getvalueof var="locationIdFromCookie" param="locationId" />
		</dsp:oparam>
		<dsp:oparam name="empty">
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="/com/tru/utils/TRUPdpURLDroplet">
						<%-- <dsp:param name="productId" value="${productId}" /> --%>
						<dsp:param name="productId" value="${onlinePID}" />
						<dsp:param name="siteId" bean="/atg/multisite/Site.id" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="productPageUrl" param="productPageUrl" />
						</dsp:oparam>
					</dsp:droplet>
 				<div class="col-md-2">
					<div class="compare-product-image">				
					<input type="hidden" value="${productId}" id="productId"/>
					<input type="hidden" value="${catID}" id="categoryId"/>
					<input type="hidden" value="${skuId}" id="skuId"/>
					<c:choose>
					 <c:when test="${(colorSizeVariantsAvailableStatus ne 'noColorSizeAvailable')}">
						<input type="hidden" value="true" id="isStylizedItem"/>
				    </c:when> 
					<c:otherwise>
						<input type="hidden" value="false" id="isStylizedItem"/>
					</c:otherwise>
					</c:choose>
						<c:choose>
							<c:when test="${not empty primaryImage}">
								<img  src="${primaryImage}">
								<%-- <a href="${productPageUrl}">
								  <img src="${primaryImage}> </a> --%>
							</c:when>
							<c:otherwise> 
								<c:choose>
									 <c:when test="${not empty akamaiNoImageURL}">
									  	<img src="${akamaiNoImageURL}"  alt="${altH1SkuName}"/>
									</c:when>
									 <c:otherwise>
									 	<img src="${TRUImagePath}images/no-image500.gif"  alt="${altH1SkuName}"/> 
									 </c:otherwise>
								 </c:choose>								
							</c:otherwise>
						</c:choose>
					</div>
					<div class="pdp-page-url">${productPageUrl}</div>
					<div class="compare-column-header-text"> <a href="${productPageUrl}" >${displayName}</a></div>
					<div class="compare-reviews">
					<fmt:parseNumber var="reviewRatingNumber" type="number" value="${reviewRating}" />
										<div class="star-rating-block">
											<c:forEach begin="1" end="${reviewRatingNumber}">
												<div class="star-rating-on"></div>
											</c:forEach>
											<c:choose>
											<c:when test="${reviewRatingFraction > 0.0 }">
											<div class="star-rating-half"></div>
											<c:forEach begin="1" end="${5-(reviewRatingNumber+1)}">
												<div class="star-rating-off"></div>
											</c:forEach>
											</c:when>
											<c:otherwise>
											<c:forEach begin="1" end="${5-reviewRatingNumber}">
												<div class="star-rating-off"></div>
											</c:forEach>
											</c:otherwise>
											</c:choose>
										</div>
									</div>
						<c:if test="${not empty suggestAgeMessage }">
							<div class="compare-age">${suggestAgeMessage}</div>
						</c:if>		
					 <%-- <a href="${productPageUrl}">${displayName}</a> --%>
					<div class="addToCartJSPSection">
					<input type="hidden"  id="ItemQty"    value="1" />
					<input type="hidden" value="${skuId}" class="primary-img-skuId"/>
					<input id="onlineInventoryStatus-${skuId}" type="hidden" value="${inventoryStatus}" />
					<c:choose>
					 <c:when test="${(colorSizeVariantsAvailableStatus ne 'noColorSizeAvailable')}">
						<a id="compare-overlay-view-more" class="blue-link" href="${productPageUrl}"><fmt:message key="tru.productdetail.label.compareviewmoredetails"/></a>		
				    </c:when> 
				    <c:when test="${notifyMe eq 'Y' && (inventoryStatus eq 'outOfStock' && s2s eq 'N' && ispu eq 'N' )}">
                     	<div class="email-section">
						<div class="NotifyMeDescription"><%-- ${longDescription} --%></div>
                        <input type="hidden" class="NotifyMeDskuDisplayName" value="${displayName}">
                        <input type="hidden" class="productId" value="${productId}">
                        <input type="hidden" class="NotifyMeskuId" value="${skuId}">
                        <input type="hidden" class="NotifyMeProductPageUrl" value="${productPageUrl}">
                        <input type="hidden" class="NotifyMeHelpURLName" value="${productPageUrl}">
                        <input type="hidden" class="NotifyMeHelpURLValue" value="${productPageUrl}">
                        <input type="hidden" class="primaryImage" value="${primaryImage}">
                        <input type="hidden" class="siteID" value="${siteID}">
						<button onclick="loadNotifyMe(this)" class="email-me-available" data-toggle="modal" data-target="#notifyMeModal"><fmt:message key="tru.productdetail.label.emailmeavailable"/></button>							
						</div>							
					</c:when>							
                    <c:otherwise>
						<c:choose>
						<c:when test="${(!unCartable) && (channelAvailability eq 'In Store Only') &&(colorSizeVariantsAvailableStatus eq 'noColorSizeAvailable')}">
	                        <c:choose>
								<c:when test="${((empty locationIdFromCookie) && !(s2s eq 'N' && ispu eq 'N'))}">
									<input type="hidden" class="compareProductId" value="${productId}">
									<button  class="add-to-cart" data-toggle="modal" data-target="#findInStoreModal" onclick="javascript:return loadFindInStore('${contextPath}','${skuId}','',$('.QTY-${productId}').val());"><fmt:message key="tru.productdetail.label.addtocart"/></button>
								</c:when>       
								<c:when test="${((empty locationIdFromCookie) && s2s eq 'N' && ispu eq 'N') || ((not empty locationIdFromCookie) && s2s eq 'N' && ispu eq 'N')}">
									<button disabled="disabled" class="add-to-cart"><fmt:message key="tru.productdetail.label.addtocart"/></button>
								</c:when>
								<c:when test="${((not empty locationIdFromCookie) && (inventoryStatusInStore ne 'outOfStock') && !(s2s eq 'N' && ispu eq 'N')) }">
									<button  class="add-to-cart" data-toggle="modal" data-target="#shoppingCartModal" onclick="javascript: return addItemToCart('${productId}','${skuId }','','${contextPath}',this,'fromCompare');"><fmt:message key="tru.productdetail.label.addtocart"/></button>
								</c:when>
							</c:choose>
                      	</c:when>
						<c:when test="${(!unCartable) && ((empty locationIdFromCookie && inventoryStatus ne 'outOfStock') 
												|| (not empty locationIdFromCookie && !(s2s eq 'N' && ispu eq 'N') && (inventoryStatus ne 'outOfStock' || inventoryStatusInStore ne 'outOfStock'))
												|| ((not empty locationIdFromCookie) && s2s eq 'N' && ispu eq 'N' && (inventoryStatus ne 'outOfStock')))}">
							<button  class="add-to-cart" data-toggle="modal" data-target="#shoppingCartModal" onclick="javascript: return addItemToCart('${productId}','${skuId }','','${contextPath}',this,'fromCompare');"><fmt:message key="tru.productdetail.label.addtocart"/></button>
						</c:when>
						<c:when test="${(!unCartable) && not empty locationIdFromCookie && inventoryStatus eq 'outOfStock' && inventoryStatusInStore eq 'outOfStock'}">
							<button disabled="disabled" class="add-to-cart fade-add-to-cart"><fmt:message key="tru.productdetail.label.addtocart"/></button>
						</c:when>
						<c:when test="${(((!unCartable) && inventoryStatus eq 'outOfStock') && empty locationIdFromCookie && !(s2s eq 'N' && ispu eq 'N'))}">
							<input type="hidden" class="compareProductId" value="${productId}">
							<button  class="add-to-cart" data-toggle="modal" data-target="#findInStoreModal" onclick="javascript:return loadFindInStore('${contextPath}','${skuId}','',$('.QTY-${productId}').val());"><fmt:message key="tru.productdetail.label.addtocart"/></button>
						</c:when>
						<c:otherwise>
						     <button disabled="disabled" class="add-to-cart fade-add-to-cart"><fmt:message key="tru.productdetail.label.addtocart"/></button>
						</c:otherwise>
					</c:choose>
			 		</c:otherwise>			    
					</c:choose>
						<!-- <div class="add-to-registry">
							<div class="inline"> add to registry</div>
							<img class="inline" src="images/breadcrumb-arrow-right.png">
						</div> -->
						<dsp:include page="/jstemplate/addToRegistry.jsp">
		 				<dsp:param name="compareProduct" value="compareProduct"/>
		 				<dsp:param name="colorSizeVariantsAvailableStatus" value="${colorSizeVariantsAvailableStatus}" />
						</dsp:include>
				</div>			
				<div class="compare-price">
						<dsp:include page="/compare/compareprice.jsp">
							<dsp:param name="skuId" value="${skuId}" />
							<dsp:param name="productId" value="${productId}" />
							<dsp:param name="priceDisplay" value="${priceDisplay}" />
							<dsp:param name="unCartable" value="${unCartable}" />
						</dsp:include>
					</div>
					
				<!-- 	<div class="compare-product-langDescription">${longDescription}</div> -->
	</div>
</dsp:page>