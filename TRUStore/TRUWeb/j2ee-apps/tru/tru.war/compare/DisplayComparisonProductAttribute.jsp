<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:getvalueof var="compareAttributeList" param="compareAttributeList" />
	<dsp:getvalueof var="setOfAttributeName" param="setOfAttributeName" />
		<div id="compare_overlay_label_list">
			<c:forEach items="${setOfAttributeName}" var="attributeName">
				<div class="compare_overlay_label">${attributeName}</div>	
			</c:forEach>
		</div>
			
		<c:forEach var="attriValues" items="${compareAttributeList}">
			<div class="compare_overlay_product_values">
			<c:forEach var="valueMap" items="${attriValues}">
				<div class="compare_overlay_label_value" data-label-name="${valueMap.key}">${valueMap.value}</div>
			</c:forEach>
			</div>
		</c:forEach>			
</dsp:page>