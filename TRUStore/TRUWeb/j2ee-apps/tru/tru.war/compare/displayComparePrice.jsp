<%--This fragment will be included in the product details page fragment to display the price on the PDP. For the items which cannot be added to the cart, the price will not display. Prices will be hidden by checking the product type.
Message for MSRP products will be displayed by using content key.
Product details page fragments can be referred from the PDP TDD.
 --%>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUPriceDroplet" />
<dsp:page>
	<dsp:droplet name="TRUPriceDroplet">
		<dsp:param name="skuId" param="skuId" />
		<dsp:param name="productId" param="productId" />
		<dsp:oparam name="true">
				<dsp:getvalueof var="salePrice" param="salePrice" />
				<dsp:getvalueof var="listPrice" param="listPrice" />
			<div class="prices " tabindex="0">
				<span class="crossed-out-price"><fmt:message key="pricing.dollar" /> <fmt:setLocale	value="en_US" /><fmt:formatNumber  type="number" minFractionDigits="2" maxFractionDigits="2"	value="${listPrice}" />
				</span> <span class="sale-price"><fmt:message key="pricing.dollar" /><fmt:setLocale	value="en_US" /><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"	value="${salePrice}" />
				</span>
			</div>
			<div class="member-price" tabindex="0">
				<fmt:message key="pricing.yousave" /> <span><fmt:message key="pricing.dollar" /><dsp:valueof param="savedAmount" /> <fmt:message key="pricing.openperanthasis" /><dsp:valueof param="savedPercentage" /><fmt:message key="pricing.persentage" /><fmt:message key="pricing.closeperanthasis" /></span>
			</div>
		</dsp:oparam>
		<dsp:oparam name="false">
			<div class="prices">
				<dsp:getvalueof var="salePrice" param="salePrice" />
				<dsp:getvalueof var="listPrice" param="listPrice" />
				<c:choose>
					<c:when test="${salePrice == 0}">
						<span class="price"> <fmt:message key="pricing.dollar" /><fmt:setLocale	value="en_US" /><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"	value="${listPrice}" /></span>
					</c:when>
					<c:otherwise>
						<span class="price"> <fmt:message key="pricing.dollar" /><fmt:setLocale	value="en_US" /><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"	value="${salePrice}" /></span>
					</c:otherwise>
				</c:choose>
			</div>
		</dsp:oparam>
		<dsp:oparam name="empty">
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>