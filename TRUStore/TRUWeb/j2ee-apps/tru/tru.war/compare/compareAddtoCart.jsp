<dsp:page>
<fmt:setBundle	basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:getvalueof param="skuId" var="skuId"/>
<dsp:getvalueof param="productId" var="productId"/>
<dsp:getvalueof param="quantity" var="quantity"/>
<input type="hidden"  id="ItemQty"    value="1" />
<div class="add-to-cart-section">

<button  class="add-to-cart" data-toggle="modal" data-target="#" onclick="javascript: return addItemToCart('${productId}','${skuId }',' ');"><fmt:message key="tru.productdetail.label.addtocart"/></button>
							

</div>
</dsp:page>