<%--This jsp is used to display tag promotions. These promotions are informative. --%>
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/com/tru/commerce/cart/droplet/DisplayTagPromotionDroplet"/>
	<dsp:droplet name="DisplayTagPromotionDroplet">
		<dsp:param name="profile" bean="Profile" />
		<dsp:param name="allPromo" value="AllPromo"/>
		<dsp:param name="itemPromo" value="ItemPromo"/>
		<dsp:param name="orderPromo" value="OrderPromo"/>
		<dsp:param name="shippingPromo" value="ShippingPromo"/>
		<dsp:oparam name="output">
			<dsp:getvalueof param="tagPromos" var="tagPromos"/>
			<dsp:droplet name="ForEach">
				<dsp:param name="array" value="${tagPromos}"/>
				<dsp:param name="elementName" value="tagPromoItems" />
				<dsp:oparam name="output">
					<dsp:getvalueof param="tagPromoItems.description" var="description"/>
					<c:if test="${not empty fn:trim(description)}">
						${description}</br>
					</c:if>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>


