<%--This fragment is used to display the promotion messages in the cart and PDP pages. --%>
<!-- commerce item should be passed as parameter from included jsp -->
<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsNull" />
	<dsp:importbean bean="/com/tru/commerce/droplet/TRUItemLevelPromotionDroplet"/>
	<c:set var="msgNotDisplayed" value="true" />
	<dsp:getvalueof var="adjusted" param="item.adjusted"></dsp:getvalueof>
	<dsp:getvalueof var="fromEdit" param="fromEdit"/>
	<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
		<dsp:oparam name="output">
		<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
		</dsp:oparam>
	</dsp:droplet>
	<c:choose>
		<c:when test="${adjusted && ! fromEdit}">
			<dsp:droplet name="IsNull">
				<dsp:param name="value" param="item.promotionItemDescription" />
				<dsp:oparam name="false">
					<div class="buy-1-get-free two-for-two inline cart-promotion-word-wrap">
						<dsp:valueof param="item.promotionItemDescription" />
						&nbsp;.
						<dsp:getvalueof var="promotionDetails"
							param="item.promotionDetails" />
						<c:if test="${not empty promotionDetails}">
							<c:choose>
								<c:when test="${fn:startsWith(promotionDetails, scheme)}">
									<a href="${promotionDetails}" target="_blank">&nbsp;<fmt:message
											key="shoppingcart.see.terms" /></a>
								</c:when>
								<c:when test="${fn:startsWith(promotionDetails, 'www')}">
									<a href="${scheme}${promotionDetails}" target="_blank">&nbsp;<fmt:message
											key="shoppingcart.see.terms" /></a>
								</c:when>
								<c:otherwise>
									<span class="promotionDetailsSpan display-none">${promotionDetails}</span>
									<a  class="terms-link"  href="#">&nbsp;<fmt:message
											key="shoppingcart.see.terms" /></a>
								</c:otherwise>
							</c:choose>
						</c:if>
						<br />
					</div>
				</dsp:oparam>
			</dsp:droplet>
		</c:when>
		<c:otherwise>
			<dsp:droplet name="IsNull">
				<dsp:param name="value" param="item.skuItem" />
				<dsp:oparam name="false">
					<dsp:getvalueof var="adjustments"
						param="item.skuItem.skuQualifiedForPromos" />
					<dsp:droplet name="TRUItemLevelPromotionDroplet">
						<dsp:param name="adjustments" value="${adjustments}" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="rankOnePromotion" param="rankOnePromotion" />
							<c:if test="${not empty rankOnePromotion }">
								<div class="buy-1-get-free two-for-two inline cart-promotion-word-wrap">
									<c:choose>
										<c:when test="${not empty rankOnePromotion.description}">
											<dsp:valueof value="${rankOnePromotion.description}" />&nbsp;<span>&#xb7;</span>
										</c:when>
										<c:otherwise>
											<dsp:valueof value="${rankOnePromotion.displayName}" />&nbsp;<span>&#xb7;</span>
										</c:otherwise>
									</c:choose>
									<dsp:getvalueof var="promotionDetails"
										value="${rankOnePromotion.promotionDetails}" />
									<c:if test="${not empty promotionDetails}">
										<c:choose>
											<c:when test="${fn:startsWith(promotionDetails, scheme)}">
												<a href="${promotionDetails}" target="_blank">&nbsp;<fmt:message
														key="shoppingcart.see.terms" /></a>
											</c:when>
											<c:when test="${fn:startsWith(promotionDetails, 'www')}">
												<a href="${scheme}${promotionDetails}" target="_blank">&nbsp;<fmt:message
														key="shoppingcart.see.terms" /></a>
											</c:when>
											<c:otherwise>
												<span class="promotionDetailsSpan display-none">${promotionDetails}</span>
												<a  class="terms-link"  href="#">&nbsp;<fmt:message
														key="shoppingcart.see.terms" /></a>
											</c:otherwise>
										</c:choose>
									</c:if>
									<br />
								</div>
							</c:if>
						</dsp:oparam>
					</dsp:droplet>

				</dsp:oparam>
			</dsp:droplet>

		</c:otherwise>
	</c:choose>
</dsp:page>
