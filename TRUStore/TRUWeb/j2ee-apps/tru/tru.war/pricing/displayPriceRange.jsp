<%-- This Jsp is used to display the price range for product. This uses the OOTB droplet to retrieve the prices of child SKUs and displays the lowest and highest prices --%>

<dsp:page>
	<dsp:importbean bean="/atg/commerce/pricing/PriceRangeDroplet"/>
	<dsp:importbean bean="/atg/multisite/Site"/>
	<dsp:droplet name="PriceRangeDroplet">
		<dsp:param name="productId" param="productId"/>
		<dsp:param name="priceList" bean="Site.defaultPriceList"/>
		<dsp:param name="salePriceList" value="Site.salePriceList"/>
		<dsp:oparam name="ouput">
			<dsp:valueof param="lowestPrice"/>
			<dsp:valueof param="highestPrice"/>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>