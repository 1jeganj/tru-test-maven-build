<%--This fragment is used to display all closeness qualifier messages in the shopping cart. --%>

<dsp:page>
	<dsp:importbean
		bean="/atg/commerce/promotion/ClosenessQualifierDroplet" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:droplet name="ClosenessQualifierDroplet">
		<dsp:param name="type" value="all" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="closenessQualifier" param="closenessQualifiers" />
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="ForEach">
		<dsp:param name="array" value="${closenessQualifier}" />
		<dsp:param name="elementName" value="closenessQualifier"/>
		<dsp:oparam name="output">
			<dsp:valueof param="closenessQualifier.name" valueishtml="true" />
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>