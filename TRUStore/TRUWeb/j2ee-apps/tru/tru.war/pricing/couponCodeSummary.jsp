<%--This Fragment is used to display the coupon code summary in case any coupon is applied or not applied when the user attempts to apply a coupon.
 --%>
<!-- commerce item should be passed as parameter from included jsp -->
<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
	<dsp:importbean bean="/atg/commerce/promotion/CouponFormHandler" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:getvalueof var="appliedPromoNames" bean="ShoppingCart.current.appliedPromoNames"/>
	<dsp:getvalueof var="notAppliedPromoNames" bean="ShoppingCart.current.notAppliedPromoNames"/>
	<dsp:getvalueof var="notAppliedCouponCodes" bean="ShoppingCart.current.notAppliedCouponCode"/>
	<c:set var="msgNotDisplayed" value="true" />
	<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
		<dsp:oparam name="output">
		<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
		</dsp:oparam>
	</dsp:droplet>
	<div class="promo-codes-container">
		<table class="promo-codes-applied">
			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="Profile.activePromotions" />
				<dsp:param name="elementName" value="promotionstatus" />
				<dsp:getvalueof var="promotionDetails" param="promotionstatus.promotion.promotionDetails"/>
				<dsp:getvalueof var="promotionName" param="promotionstatus.promotion.displayName"/>
				<dsp:oparam name="output">
					<dsp:droplet name="ForEach">
						<dsp:param name="array" param="promotionstatus.coupons" />
						<dsp:param name="elementName" value="coupon" />
						<dsp:oparam name="output">
						<dsp:getvalueof var="appliedCoupons" bean="ShoppingCart.current.singleUseCoupon"/>
						<dsp:getvalueof var="couponId" param="coupon.id" />
						<c:if test="${appliedCoupons.containsValue(couponId) and appliedPromoNames.containsValue(promotionName)}">
							<dsp:getvalueof var="couponID" param="coupon.id" />
							<tr>
								<td>
									<span class="promo-text">${promotionName}</span>
									<c:if test="${not empty promotionDetails}">
										<c:choose>
										<c:when test="${fn:startsWith(promotionDetails, scheme)}">
											<a href="${promotionDetails}" target="_blank" class="see-terms cart-promo-seeterm-tooltip" data-original-title="" title="">&nbsp;<fmt:message key="shoppingcart.see.terms" /></a>
										</c:when>
										<c:when test="${fn:startsWith(promotionDetails, 'www' )}">
											<a href="${scheme}${promotionDetails}" target="_blank" class="see-terms cart-promo-seeterm-tooltip" data-original-title="" title="">&nbsp;<fmt:message key="shoppingcart.see.terms" /></a>
										</c:when>
										<c:otherwise>
											<span class="promotionDetailsSpan display-none">${promotionDetails}</span>
											<a data-toggle="modal" data-target="#detailsModel" href="#" >&nbsp;<fmt:message key="shoppingcart.see.terms" /></a>
										</c:otherwise>
										</c:choose>	
									</c:if>	
								</td>
								<td><span class="promo-text"><fmt:message key="shoppingcart.applied"/><span class="promo-text"></td>
								<td><a href="javascript:void(0);" onclick="removeCoupon('${couponId}')" class="inline pull-right remove-promo"><fmt:message key="shoppingcart.remove.coupon"/></a></td>
							</tr>
						</c:if>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</table>
	</div>
		<div class="applied-bad-promo">
		<hr>
		  <div>
		      <p class="inline"><span id ="error_expire_code"></span>
		      </p>
		      <span class="pull-right">
			      <a class="NotApplied-modal" data-target="#paymentLearnMore_modal" data-toggle="modal" href="JavaScript:void(0)"><fmt:message key="shoppingcart.coupon.learn.more"/></a>
			      <!-- <a href="JavaScript:void(0)" class="NotAppliedTooltip" data-original-title="" title="">learn more</a> -->
			      <a href="JavaScript:void(0)" onclick="removeNonAppliedCoupon(this)" class="inline remove-promo"><fmt:message key="shoppingcart.remove.coupon"/></a>
		      </span>
		    </div>
		</div>
	<c:if test="${(not empty notAppliedPromoNames) or (not empty notAppliedCouponCodes)}">
		<div class="applied-bad-promo-from-order">
		<hr>
	<dsp:droplet name="ForEach">
		<dsp:param name="array" bean="Profile.activePromotions" />
		<dsp:param name="elementName" value="promotionstatus" />
		<dsp:getvalueof var="promotionName" param="promotionstatus.promotion.displayName" />
		<dsp:oparam name="output">
			<!-- <div class="applied-bad-promo-from-order">
				<hr> -->
				<dsp:droplet name="ForEach">
					<dsp:param name="array" param="promotionstatus.coupons" />
					<dsp:param name="elementName" value="coupon" />
					<dsp:oparam name="output">
						<dsp:getvalueof var="couponId" param="coupon.id" />
						<c:choose>
							<c:when test="${notAppliedCouponCodes.containsValue(couponId)}">
								<h5>
									<p id="error_expire_message-from-order">${promotionName}</p>
								</h5>
								<div>
									<p class="inline">
										<span id="error_expire_code-from-order"><fmt:message key="shoppingcart.not.applied"/></span>
									</p>
									<span class="pull-right"> <a class="NotApplied-modal" data-target="#paymentLearnMore_modal" data-toggle="modal"
										href="JavaScript:void(0)"><fmt:message key="shoppingcart.coupon.learn.more"/></a> <!-- <a href="JavaScript:void(0)"
										 class="NotAppliedTooltip" data-original-title="" title="">learn more</a> -->
										<a href="javascript:void(0);" onclick="removeCoupon('${couponId}')" 
										class="inline remove-promo"><fmt:message key="shoppingcart.remove.coupon"/></a>
									</span>
								</div>
							</c:when>
							<c:when test="${notAppliedPromoNames.containsValue(promotionName)}">
									<h5>
									<p id="error_expire_message-from-order">${promotionName}</p>
								</h5>
								<div>
									<p class="inline">
										<span id="error_expire_code-from-order"><fmt:message key="shoppingcart.not.applied"/></span>
									</p>
									<span class="pull-right"> <a class="NotApplied-modal"
										data-target="#paymentLearnMore_modal" data-toggle="modal"
										href="JavaScript:void(0)"><fmt:message key="shoppingcart.coupon.learn.more"/></a> <!-- <a href="JavaScript:void(0)" class="NotAppliedTooltip" data-original-title="" title="">learn more</a> -->
										<a href="javascript:void(0);"
										onclick="removeCoupon('${couponId}')"
										class="inline remove-promo"><fmt:message key="shoppingcart.remove.coupon"/></a>
									</span>
								</div>
							</c:when>
						</c:choose>
					</dsp:oparam>
				</dsp:droplet>
			<!-- </div> -->
		</dsp:oparam>
	</dsp:droplet>
	</div>
	</c:if>
</dsp:page>
