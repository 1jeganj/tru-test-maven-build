<%--This fragment will be included in the product details page fragment to display the price on the PDP. For the items which cannot be added to the cart, the price will not display. Prices will be hidden by checking the product type.
Message for MSRP products will be displayed by using content key.
Product details page fragments can be referred from the PDP TDD.
 --%>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUPriceDroplet" />
<dsp:page>
<dsp:getvalueof var="quickViewPrice" param="quickViewPrice" />
	<c:if test="${quickViewPrice ne 'quickViewPrice'}">
	 <c:set var="quickViewPriceClass" value="display-none"></c:set>
	</c:if>
	<dsp:droplet name="TRUPriceDroplet">
		<dsp:param name="skuId" param="skuId" />
		<dsp:param name="productId" param="productId" />
		<dsp:oparam name="true">
		<dsp:getvalueof var="salePrice" param="salePrice" />
		<dsp:getvalueof var="listPrice" param="listPrice" />
		<dsp:getvalueof var="savedAmount" param="savedAmount" />
		<dsp:getvalueof var="savedPercentage" param="savedPercentage" />
			<div class="prices ${quickViewPriceClass}" tabindex="0">
				<span class="crossed-out-price"><fmt:message key="pricing.dollar" /><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"	value="${listPrice}" /></span> <span class="sale-price"><fmt:message key="pricing.dollar" /><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"	value="${salePrice}" />
				</span>
			</div>
			<div class="member-price ${quickViewPriceClass}" tabindex="0">
				<fmt:message key="pricing.yousave" />&nbsp;<fmt:message key="pricing.dollar" /><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${savedAmount} " />&nbsp;<fmt:message key="pricing.openperanthasis" /><fmt:formatNumber type="number" groupingUsed="false" value="${savedPercentage}" /><fmt:message key="pricing.persentage" /><fmt:message key="pricing.closeperanthasis" />
			</div>
		</dsp:oparam>
		<dsp:oparam name="false">
		<dsp:getvalueof var="salePrice" param="salePrice" />
			<div class="prices ${quickViewPriceClass}">
				<dsp:getvalueof var="salePrice" param="salePrice" />
				<dsp:getvalueof var="listPrice" param="listPrice" />
				<c:choose>
					<c:when test="${salePrice == 0}">
						<span class="price"> <fmt:message key="pricing.dollar" /><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"	value="${listPrice}" /></span>
					</c:when>
					<c:otherwise>
						<span class="price"> <fmt:message key="pricing.dollar" /><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"	value="${salePrice}" /></span>
					</c:otherwise>
				</c:choose>
			</div>
		</dsp:oparam>
		<dsp:oparam name="empty">
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>