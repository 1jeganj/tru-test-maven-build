<%--This fragment is used to display the promotion details in a popup. The ‘Details’ link will be displayed in line with the promotion name. Upon clicking the ‘Details’ link, a popup will be opened; loading one of the properties in the promotion. --%>
<!-- <div class="modal-dialog modal-dialog-forgotpassword ">
					<div class="modal-content sharp-border">
						<div class="forgotpasswordmodel-overlay">
							<div class="row">
								<div class="col-md-12">
									<a href="javascript:void(0)" data-dismiss="modal"> <span class="clickable"><span class="sprite-icon-x-close"></span></span></a>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 set-height">
									<div class="row">
										<div class="col-md-12">
											<h2 class="">Detail model</h2>
											<p id="promoDetailContent"> </p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> -->
				
				<div class="modal-dialog">
					<div class="modal-content sharp-border welcome-back-overlay">
						<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
						<h2>details</h2>
						<div class="tse-scrollable wrapper">
							<div class="tse-content"> 
								<p class="abandonCartReminder" id="promoDetailContent"></p>
							</div>
						</div> 
						
					</div>
				</div>
				