<dsp:page>
	<dsp:importbean bean="/atg/commerce/pricing/PriceItem" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<input type="hidden" class="contextPath" value="${contextPath}" />
	<div class="btn-group special-offer">
	<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
		<dsp:oparam name="output">
		<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
		</dsp:oparam>
	</dsp:droplet>
		<dsp:droplet name="PriceItem">
			<dsp:param name="product" param="prodItem" />
			<dsp:param name="item" param="prodItem.childSkus[0]" />
			<dsp:param name="quantity" value="3" />
			<dsp:param name="elementName" value="PricedSku" />
			<dsp:oparam name="output">
				<button class="btn btn-default dropdown-toggle"
					data-toggle="dropdown" aria-expanded="false">
					<div class="special-offer-text">
						<fmt:message key="tru.productdetail.label.specialoffers" />
					</div>
				</button>
				<ul class="offers-price dropdown-menu" role="menu">
					<dsp:droplet name="ForEach">
						<dsp:param name="array" param="PricedSku.priceInfo.adjustments" />
						<dsp:param name="elementName" value="adjuster" />
						<dsp:oparam name="output">
							<dsp:droplet name="IsEmpty">
								<dsp:param name="value" param="adjuster.pricingModel" />
								<dsp:oparam name="false">
									<dsp:getvalueof var="promotionId"
										param="adjuster.pricingModel.id" />
										<dsp:getvalueof var="promotionDetails"
										param="adjuster.pricingModel.promotionDetails" />
										<input id="" type="hidden" value="${promotionDetails}"/>
										<li class="small-grey">
											<div class="inline blue-bullet"></div> <dsp:valueof
												param="adjuster.pricingModel.displayName" />
												<c:if test="${not empty promotionDetails}">
													<div class="small-blue inline">
														<c:choose>
															<c:when test="${fn:startsWith(promotionDetails, scheme || fn:startsWith(promotionDetails, 'www' )}">
																<a href="${promotionDetails}" target="_blank"><fmt:message key="tru.productdetail.label.details" /></a>
															</c:when>
															<c:otherwise>
																<a href="#" id="" onclick="showDetails('${promotionId}');"><fmt:message key="tru.productdetail.label.details" /></a>
															</c:otherwise>
														</c:choose>											
													</div>
											</c:if>
										</li>
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>
				</ul>
			</dsp:oparam>
		</dsp:droplet>
	</div>
</dsp:page>