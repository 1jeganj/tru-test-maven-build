<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
	<dsp:getvalueof var="giftWrapNonEligibleItemImageSizeMap" bean="TRUStoreConfiguration.giftWrapNonEligibleItemImageSizeMap" />
	
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
  	<div class="no-gift-options">
  	    <header>
          <fmt:message key="checkout.gifting.nonEligibleItemsHeading" />
        </header>
		<dsp:droplet name="ForEach">
			<dsp:param name="array" param="nonEligibleGiftItems"  />
			<dsp:param name="elementName" value="nonEligibleGiftItem" />
			<dsp:param name="sortProperties" value="productDisplayName" />
			<dsp:oparam name="output">
		      	<div class="checkout-gifting-registry-product">
		      	<dsp:droplet name="IsEmpty">
					<dsp:param name="value" param="nonEligibleGiftItem.channel" />
					<dsp:oparam name="false">
						<header>
							<dsp:valueof param="nonEligibleGiftItem.productDisplayName" />
							<p>
								<fmt:message key="shoppingcart.added.from" />&nbsp;<dsp:valueof param="nonEligibleGiftItem.channel" valueishtml="true"/>
							</p>
						</header>
					</dsp:oparam>
					<dsp:oparam name="true">
						<header><dsp:valueof param="nonEligibleGiftItem.productDisplayName" /></header>
					</dsp:oparam>
				</dsp:droplet>
		          <div class="registry-product-content">
		              <div class="row">
		                  <div class="col-md-2">
							<dsp:getvalueof var="productImage"
								param="nonEligibleGiftItem.productImage" />
							<c:forEach var="entry" items="${giftWrapNonEligibleItemImageSizeMap}">
								<c:if test="${entry.key eq 'height'}">
									<c:set var="imageHeight" value="${entry.value}" />
								</c:if>
								<c:if test="${entry.key eq 'width'}">
									<c:set var="imageWidth" value="${entry.value}" />
								</c:if>
							</c:forEach>
							<dsp:droplet name="IsEmpty">
								<dsp:param name="value" value="${productImage}" />
								<dsp:oparam name="false">
									<dsp:droplet name="/com/tru/common/droplet/TRUAkamaiImageDroplet">
										<dsp:param name="imageName" value="${productImage}" />
										<dsp:param name="imageHeight" value="${imageHeight}" />
										<dsp:param name="imageWidth" value="${imageWidth}" />
										<dsp:oparam name="output">
											<dsp:getvalueof var="akamaiImageUrl" param="akamaiUrl" />
										</dsp:oparam>
									</dsp:droplet>
									<a class="displayImage" href="javascript:void(0);" aria-label="shoping cart product image"><img class="img-responsive product-description-image" src="${akamaiImageUrl}" alt="product image"></a>
								</dsp:oparam>
								<dsp:oparam name="true">
									<a class="displayImage" href="javascript:void(0);" aria-label="shoping cart product image"><img class="img-responsive product-description-image image-not-available" src="${TRUImagePath}images/no-image500.gif" alt="product image"/></a>
								</dsp:oparam>
							</dsp:droplet>
		                  </div>
		                  	<div class="col-md-1">
						   		<dsp:droplet name="IsEmpty">
									<dsp:param name="value" param="nonEligibleGiftItem.productColor" />
									<dsp:oparam name="false">
						        		<p><fmt:message key="checkout.gifting.itemColor" />:&nbsp;<dsp:valueof param="nonEligibleGiftItem.productColor"/></p>
						        	</dsp:oparam>
						        </dsp:droplet>
						        <dsp:droplet name="IsEmpty">
									<dsp:param name="value" param="nonEligibleGiftItem.productSize" />
									<dsp:oparam name="false">
						        		<p><fmt:message key="checkout.gifting.itemSize" />:&nbsp;<dsp:valueof param="nonEligibleGiftItem.productSize"/></p>
						        	</dsp:oparam>
						        </dsp:droplet>
						        <p><fmt:message key="checkout.gifting.itemQuantity" />:&nbsp;<dsp:valueof param="nonEligibleGiftItem.quantity"/></p>
						   </div>
		              </div>
					  <div class="row">
					  </div>
		          </div>
		      </div>
	      	<br>
	      	</dsp:oparam>
		</dsp:droplet>
  	</div>
</dsp:page>