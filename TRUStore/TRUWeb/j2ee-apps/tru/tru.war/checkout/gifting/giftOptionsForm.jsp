<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>  
<dsp:page>
	<dsp:importbean bean="/com/tru/commerce/order/droplet/GiftOptonsDetailsDroplet"/>
	<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftOptionsFormHandler"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
  	<div class="col-md-8 checkout-left-column">
		<div class="checkout-gifting-header">
	    	<h1><fmt:message key="checkout.gifting.giftingOptionsHeading" /></h1>
   		</div>
		<hr class="horizontal-divider">
  		<dsp:droplet name="GiftOptonsDetailsDroplet">
			<dsp:param name="order" bean="ShoppingCart.current" />
			<dsp:oparam name="output">
				<dsp:setvalue bean="GiftOptionsFormHandler.giftOptionsInfoCount" paramvalue="giftOptionsInfoCount" />
				<dsp:form method="post" name="chekcout_gift_options" id="chekcout_gift_options">
					<dsp:input type="hidden" bean="GiftOptionsFormHandler.giftOptionsInfoCount" priority="1000" paramvalue="giftOptionsInfoCount" />
					<dsp:getvalueof var="giftOptionsDetails" param="giftOptionsDetails"/>
					<dsp:droplet name="ForEach">
						<dsp:param name="array" param="giftOptionsDetails" />
						<dsp:param name="elementName" value="giftOptionInfo" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="shippingAdressNickName" param="key"/>
							<dsp:getvalueof var="giftOptionId" param="giftOptionInfo.giftOptionId"/>
							<dsp:getvalueof var="index" param="index" />
							  <dsp:input type="hidden" bean="GiftOptionsFormHandler.giftOptionsInfo[${index}].giftOptionId" id="${giftOptionId}_giftOptionId1" default="${giftOptionId}" value="${giftOptionId}"/>
							  <dsp:input type="hidden" bean="GiftOptionsFormHandler.giftOptionsInfo[${index}].shippingAdressNickName" id="${giftOptionId}_shippingGroupId1" default="${shippingAdressNickName}" value="${shippingAdressNickName}"/>
							  <div class="gifting-items-shipping-to">
							     <h3  tabindex="0"><fmt:message key="checkout.gifting.shippingAddressHeading" /></h3>
								 <div class="gifting-items-shipping-to-content" id="${giftOptionId}-gifting-items" >
								   <div class="row">
								       <dsp:include page="displayShippingAddress.jsp">
								       		<dsp:param name="shippingAddress" param="giftOptionInfo.shippingAddress"/>
								       		<dsp:param name="isRegistryAddress" param="giftOptionInfo.registryAddress"/>
								       </dsp:include>
								        <dsp:include page="giftNote.jsp">
									   		<dsp:param name="index" value="${index}"/>
									   		<dsp:param name="giftOptionInfo" param="giftOptionInfo"/>
									   		<dsp:param name="giftOptionId" value="${giftOptionId}"/>
									   </dsp:include>
								   </div>
									<div class="row">
										<div class="col-md-5" id="co-gift-options-item-checkbox">
											<dsp:droplet name="Switch">
												<dsp:param name="value" param="giftOptionInfo.giftReceipt" />
												<dsp:oparam name="true">
													<dsp:input type="hidden" id="${giftOptionId}_giftReceipt1" iclass="giftReceipt" bean="GiftOptionsFormHandler.giftOptionsInfo[${index}].giftReceipt" value="true"/>
													<div class="checkbox-sm-on inline" tabindex="0"></div>
												</dsp:oparam>
												<dsp:oparam name="false">
													<dsp:input type="hidden" id="${giftOptionId}_giftReceipt1"  iclass="giftReceipt" bean="GiftOptionsFormHandler.giftOptionsInfo[${index}].giftReceipt" value="false"/>
													<div class="checkbox-sm-off inline" tabindex="0"></div>
												</dsp:oparam>
											</dsp:droplet>
											<fmt:message key="checkout.gifting.giftReceiptCheck" />
										</div>
									</div>
								 </div>
							  </div>
							  <br>
							<dsp:getvalueof var="giftWrappedItems" param="giftOptionInfo.giftWrappedItems"/>
							<dsp:getvalueof var="eligibleGiftItems" param="giftOptionInfo.eligibleGiftItems"/>
							<dsp:getvalueof var="nonEligibleGiftItems" param="giftOptionInfo.nonEligibleGiftItems"/>
							<c:if test="${giftWrappedItems.size() > 0}">
								<dsp:include page="giftWrappedItems.jsp">
									<dsp:param name="giftWrappedItems" param="giftOptionInfo.giftWrappedItems"/>
								</dsp:include>
							</c:if>
							<c:if test="${eligibleGiftItems.size() > 0}">
								<dsp:include page="giftWrapEligibleItems.jsp">
									<dsp:param name="eligibleGiftItems" param="giftOptionInfo.eligibleGiftItems"/>
								</dsp:include>
							</c:if>
							<c:if test="${nonEligibleGiftItems.size() > 0}">
								<dsp:include page="giftWrapNonEligibleItems.jsp">
									<dsp:param name="nonEligibleGiftItems" param="giftOptionInfo.nonEligibleGiftItems"/>
								</dsp:include>
							</c:if>
						</dsp:oparam>
					</dsp:droplet>
					<dsp:input type="hidden" value="true" priority="-10" bean="GiftOptionsFormHandler.applyGiftMessage" />
				</dsp:form>
			</dsp:oparam>
		</dsp:droplet>
		<dsp:include page="/checkout/common/salesTaxEstimation.jsp"></dsp:include>
  	</div>
</dsp:page>