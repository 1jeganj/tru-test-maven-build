<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftOptionsFormHandler"/>
<dsp:getvalueof var="giftWrapMessage" param="giftOptionInfo.giftWrapMessage"/>
<dsp:getvalueof var="giftOptionId" param="giftOptionId" />
<dsp:getvalueof var="index" param="index" />
	<div class="col-md-8">
		<div id="shipGroupId" class="hide"><dsp:valueof param="shipGroupId"/></div>
		<fmt:message key="checkout.gifting.giftNodeHeading" />
		<span>(<span class="chars-left"><fmt:message key="checkout.gifting.text.length"/></span>&nbsp;<fmt:message key="checkout.gifting.charactersRemaining" />)</span>
		<dsp:textarea maxtextlength="180" cols="25" iclass="giftWrapMessage" rows="5" id="${giftOptionId}_giftWrapMessage" 
			bean="GiftOptionsFormHandler.giftOptionsInfo[${index}].giftWrapMessage" aria-label="include a free gift message">${giftWrapMessage}</dsp:textarea>
	</div>
</dsp:page>
