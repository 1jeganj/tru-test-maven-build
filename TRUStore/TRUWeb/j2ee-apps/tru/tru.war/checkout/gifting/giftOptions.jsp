<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:getvalueof var="currentTab" bean="ShoppingCart.current.currentTab"/>
	<dsp:getvalueof var="order" bean="ShoppingCart.current"></dsp:getvalueof>
	<%-- Start PayPal integration changes --%>
	<dsp:param name="paymentGroups" bean="ShoppingCart.current.paymentGroups" />
	<dsp:droplet name="ForEach">
		<dsp:param name="array" param="paymentGroups" />
		<dsp:param name="elementName" value="paymentGroup" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="pgType" param="paymentGroup.paymentGroupClassType" />
			<c:choose>
				<c:when test="${pgType eq 'payPal'}">
					<c:if test="${currentTab eq 'gifting-tab'}">
						<dsp:setvalue bean="ShoppingCart.current.previousTab" value="review-tab" />
					</c:if>
				</c:when>
				<c:otherwise>
					<c:if test="${currentTab ne 'shipping-tab'}">
						<dsp:setvalue bean="ShoppingCart.current.previousTab" beanvalue="ShoppingCart.current.currentTab" />
					</c:if>
				</c:otherwise>
			</c:choose>
		</dsp:oparam>
		<dsp:oparam name="empty">
			<c:if test="${currentTab ne 'shipping-tab'}">
				<dsp:setvalue bean="ShoppingCart.current.previousTab" beanvalue="ShoppingCart.current.currentTab" />
			</c:if>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:setvalue bean="ShoppingCart.current.currentTab" value="gifting-tab" />
	<%-- End PayPal integration changes --%>
	<dsp:getvalueof var="isOrderInstorePickup" value="${order.orderIsInStorePickUp}" />
	<input type="hidden" id="isOrderInstorePickup" value="${isOrderInstorePickup}"/>
	<div id="co-gifting-tab">
		<dsp:include page="/checkout/common/checkoutGlobalError.jsp">
			<dsp:param name="pageName" value="Gifting" />
		</dsp:include>
		<div class="row">
			<dsp:include page="shipmentList.jsp"></dsp:include>
			<div class="col-md-3 checkout-right-column">
				<dsp:include page="/checkout/common/checkoutOrderSummary.jsp">
					<dsp:param name="pageName" value="gifting" />
				</dsp:include>
			</div>
		</div>
	</div>
	<dsp:include page="/checkout/common/ad-Zone.jsp"></dsp:include>
	<div id="tealiumCheckoutContent"></div> 
</dsp:page>
