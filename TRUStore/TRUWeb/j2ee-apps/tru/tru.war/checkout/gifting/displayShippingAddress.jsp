<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:getvalueof var="order" bean="ShoppingCart.current" />
	<dsp:getvalueof var="shippingAddress" param="shippingAddress"/>
	<dsp:getvalueof var="isRegistryAddress" vartype="java.lang.boolean" param="isRegistryAddress"/>
    <div class="col-md-4 wrap-content" tabindex="0">
    <c:choose>
    	<c:when test="${isRegistryAddress}">
    		${shippingAddress.firstName}&nbsp;${shippingAddress.lastName}
    		<p><fmt:message key="checkout.address.provided.registrant"/></p>
    	</c:when>
    	<c:otherwise>
		          ${shippingAddress.firstName}&nbsp;${shippingAddress.lastName}
		   	<p>${shippingAddress.address1}</p>
		   	<c:if test="${not empty shippingAddress.address2}">
				<p>${shippingAddress.address2}</p>
			</c:if>
			<p>${shippingAddress.city}&nbsp;${shippingAddress.state}&nbsp;${shippingAddress.postalCode}</p>
			<dsp:getvalueof var="phoneNumberTemp" value="${shippingAddress.phoneNumber}" />
			<c:if test="${fn:length(phoneNumberTemp) gt 0}">
			<p>${fn:substring(phoneNumberTemp, 0,3)}-${fn:substring(phoneNumberTemp, 3,6)}-${fn:substring(phoneNumberTemp, 6, fn:length(phoneNumberTemp))}</p>
			</c:if>
			<%-- <p>${shippingAddress.email}</p> --%>
		</c:otherwise>
	</c:choose>
    </div>
</dsp:page>
