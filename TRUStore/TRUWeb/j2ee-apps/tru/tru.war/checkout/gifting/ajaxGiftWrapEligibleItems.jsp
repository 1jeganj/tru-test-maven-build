<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/com/tru/commerce/cart/droplet/CartItemDetailsDroplet" />
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
<dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUPriceDroplet" />
<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftOptionsFormHandler"/>
<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="giftWrapEligibleItemImageSizeMap" bean="TRUStoreConfiguration.giftWrapEligibleItemImageSizeMap" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:droplet name="ForEach">
		<dsp:param name="array" bean="ShoppingCart.current.splitGiftItems"  />
		<dsp:param name="elementName" value="eligibleGiftItem" />
		<dsp:param name="sortProperties" value="productDisplayName"/>
		<dsp:oparam name="output">
			<div class="checkout-gifting-registry-product" id='<dsp:valueof param="key"/>'>
		   		<dsp:droplet name="IsEmpty">
					<dsp:param name="value" param="eligibleGiftItem.channel" />
					<dsp:oparam name="false">
						<header>
							<dsp:valueof param="eligibleGiftItem.productDisplayName" />
							<p>
								<fmt:message key="shoppingcart.added.from" />&nbsp;<dsp:valueof param="eligibleGiftItem.channel" valueishtml="true"/>
							</p>
						</header>
					</dsp:oparam>
					<dsp:oparam name="true">
						<header><dsp:valueof param="eligibleGiftItem.productDisplayName" /></header>
					</dsp:oparam>
				</dsp:droplet>
					<div class="registry-product-content">
					    <div class="row">
					        <div class="col-md-2">
					        	<dsp:getvalueof var="productImage" param="eligibleGiftItem.productImage"/>
					        	<c:forEach var="entry" items="${giftWrapEligibleItemImageSizeMap}">
										<c:if test="${entry.key eq 'height'}">
											<c:set var="imageHeight" value="${entry.value}" />
										</c:if>
										<c:if test="${entry.key eq 'width'}">
											<c:set var="imageWidth" value="${entry.value}" />
										</c:if>
									</c:forEach> 
								 <dsp:droplet name="IsEmpty">
									<dsp:param name="value" value="${productImage}" />
									<dsp:oparam name="false">
										<dsp:droplet name="/com/tru/common/droplet/TRUAkamaiImageDroplet">
											<dsp:param name="imageName" value="${productImage}" />
											<dsp:param name="imageHeight" value="${imageHeight}" />
											<dsp:param name="imageWidth" value="${imageWidth}" />
											<dsp:oparam name="output">
												<dsp:getvalueof var="akamaiImageUrl" param="akamaiUrl" />
											</dsp:oparam>
										</dsp:droplet>
										<a class="displayImage" href="javascript:void(0);" aria-label="shoping cart product image"><img class="img-responsive product-description-image" src="${akamaiImageUrl}" alt="product image"></a>
									</dsp:oparam>
									<dsp:oparam name="true">
										<img class="img-responsive product-description-image image-not-available" src="${contextPath}images/no-image500.gif" alt="product image">
									</dsp:oparam>
								</dsp:droplet>
					   		</div>
						   <div class="col-md-1">
						   		<dsp:droplet name="IsEmpty">
									<dsp:param name="value" param="eligibleGiftItem.productColor" />
									<dsp:oparam name="false">
						        		<p><fmt:message key="checkout.gifting.itemColor" />:&nbsp;<dsp:valueof param="eligibleGiftItem.productColor"/></p>
						        	</dsp:oparam>
						        </dsp:droplet>
						        <dsp:droplet name="IsEmpty">
									<dsp:param name="value" param="eligibleGiftItem.productSize" />
									<dsp:oparam name="false">
						        		<p><fmt:message key="checkout.gifting.itemSize" />:&nbsp;<dsp:valueof param="eligibleGiftItem.productSize"/></p>
						        	</dsp:oparam>
						        </dsp:droplet>
						        <p><fmt:message key="checkout.gifting.itemQuantity" />:&nbsp;<dsp:valueof param="eligibleGiftItem.quantity"/></p>
						   </div>
							<div class="col-md-8 col-md-offset-1 add-gift-wrap display-block">
								 <dsp:include page="giftWrap.jsp">
									 	<dsp:param name="giftWrapItem" param="eligibleGiftItem"/>
								 </dsp:include>
							</div>
						</div>
					<div class="row">
			   			<div class="col-md-4">
			        		<div class="this-is-gift">
			            		<div class="checkbox-sm-on add-gift-wrap-checkbox"  tabindex="0" onclick="updateGiftItem(this,
			            							'<dsp:valueof param="eligibleGiftItem.shippingGroupId"/>', 
			            							'<dsp:valueof param="eligibleGiftItem.commItemRelationshipId"/>',
			            							 '<dsp:valueof param="eligibleGiftItem.parentCommerceId"/>',
			            							 '<dsp:valueof param="key"/>',
			            							 '<dsp:valueof param="key"/>' );">
			            		</div>
			            		<div class="shopping-cart-gift-image"></div><span><fmt:message key="checkout.gifting.giftText" /></span>
			                </div>
			            </div>
			        </div>
		    	</div>
			</div>
			</dsp:oparam>
	</dsp:droplet>
</dsp:page>   