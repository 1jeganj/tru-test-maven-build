<dsp:page>
<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftOptionsFormHandler"/>
	<dsp:setvalue bean="GiftOptionsFormHandler.shipGroupId" paramvalue="shipGroupId"/>
	<dsp:setvalue bean="GiftOptionsFormHandler.commerceItemRelationshipId" paramvalue="commerceItemRelationshipId"/>
	<dsp:setvalue bean="GiftOptionsFormHandler.commerceItemId" paramvalue="commerceItemId"/>
	<dsp:setvalue bean="GiftOptionsFormHandler.quantity" paramvalue="quantity" />
	<dsp:setvalue bean="GiftOptionsFormHandler.giftWrapProductId" paramvalue="giftWrapProductId" />
	<dsp:setvalue bean="GiftOptionsFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
	<dsp:setvalue bean="GiftOptionsFormHandler.giftItem" paramvalue="giftItem" />
	<dsp:setvalue bean="GiftOptionsFormHandler.currentPage" value="cart" />
	<dsp:setvalue bean="GiftOptionsFormHandler.updateGiftItem" paramvalue="true"/>
	
	<dsp:include page="/cart/shoppingCartInclude.jsp"/>
</dsp:page>
