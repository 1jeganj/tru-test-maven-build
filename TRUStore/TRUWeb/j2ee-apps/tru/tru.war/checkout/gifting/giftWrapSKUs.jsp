<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
  <div class="col-md-8">
		<header><fmt:message key="checkout.gifting.addGiftWrapText" /></header>
		<div class="gift-wrap-tru-block inline">
			<div class="gift-wrap-border">
				<div class="gift-wrap-opaque">
					<div class="gift-wrap-tru"></div>
				</div>
			</div>
			<div class="gift-wrap-check"></div>
			<p><fmt:message key="checkout.toysrus"/></p>
			<p><fmt:message key="checkout.giftwrap"/></p>
		</div>
		<div class="gift-wrap-bru-block inline">
			<div class="gift-wrap-border">
				<div class="gift-wrap-opaque">
					<div class="gift-wrap-bru"></div>
				</div>
			</div>
			<div class="gift-wrap-check"></div>
			<p><fmt:message key="checkout.babiesrus"/></p>
			<p><fmt:message key="checkout.giftwrap"/></p>
		</div>
		<div class="gift-wrap-none-block inline">
			<div class="gift-wrap-border border-visible">
				<div class="gift-wrap-none"  tabindex="0">
					<p><fmt:message key="checkout.gifting.none" /></p>
				</div>
			</div>
		</div>
    </div>
</dsp:page>