<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:importbean bean="/atg/registry/RepositoryTargeters/ProductCatalog/GiftWrapItem"/>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUPriceDroplet" />
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="giftWrappedItemImageSizeMap" bean="TRUStoreConfiguration.giftWrappedItemImageSizeMap" />
	<dsp:droplet name="ForEach">
		<dsp:param name="array" param="giftWrappedItems"  />
		<dsp:param name="elementName" value="giftWrappedItem" />
		<dsp:param name="sortProperties" value="productDisplayName, wrapSkuId"/>
		<dsp:oparam name="output">
			  <div class="checkout-gifting-registry-product">
				<dsp:droplet name="IsEmpty">
					<dsp:param name="value" param="giftWrappedItem.channel" />
					<dsp:oparam name="false">
						<header>
							<dsp:valueof param="giftWrappedItem.productDisplayName" />
							<p>
								<fmt:message key="shoppingcart.added.from" />&nbsp;<dsp:valueof param="giftWrappedItem.channel" valueishtml="true"/>
							</p>
						</header>
					</dsp:oparam>
					<dsp:oparam name="true">
						<header><dsp:valueof param="giftWrappedItem.productDisplayName"/></header>
					</dsp:oparam>
				</dsp:droplet>			      
				  <div class="registry-product-content">
				     <div class="row">
						<div class="col-md-2">
							<dsp:getvalueof var="productImage"
								param="giftWrappedItem.productImage" />
							<c:forEach var="entry" items="${giftWrappedItemImageSizeMap}">
								<c:if test="${entry.key eq 'height'}">
									<c:set var="imageHeight" value="${entry.value}" />
								</c:if>
								<c:if test="${entry.key eq 'width'}">
									<c:set var="imageWidth" value="${entry.value}" />
								</c:if>
							</c:forEach>
							<dsp:droplet name="IsEmpty">
								<dsp:param name="value" value="${productImage}" />
								<dsp:oparam name="false">
									<dsp:droplet
											name="/com/tru/common/droplet/TRUAkamaiImageDroplet">
										 <dsp:param name="imageName" value="${productImage}" />
										 <dsp:param name="imageHeight" value="${imageHeight}" />
										 <dsp:param name="imageWidth" value="${imageWidth}" />
										 <dsp:oparam name="output">
											<dsp:getvalueof var="akamaiImageUrl" param="akamaiUrl" />
										 </dsp:oparam>
									</dsp:droplet>
									<a class="displayImage" href="javascript:void(0);" aria-label="shoping cart product image"><img class="img-responsive product-description-image" src="${akamaiImageUrl}" alt="product image"></a>
								</dsp:oparam>
								<dsp:oparam name="true">
									<img class="img-responsive product-description-image image-not-available" src="${TRUImagePath}images/no-image500.gif" alt="product image"/>
								</dsp:oparam>
							</dsp:droplet>
					    </div>
						<div class="col-md-2">
							<dsp:droplet name="IsEmpty">
								<dsp:param name="value" param="giftWrappedItem.productColor" />
								<dsp:oparam name="false">
					        		<p><fmt:message key="checkout.gifting.itemColor" />:&nbsp;<dsp:valueof param="giftWrappedItem.productColor"/></p>
					        	</dsp:oparam>
					        </dsp:droplet>
					        <dsp:droplet name="IsEmpty">
								<dsp:param name="value" param="giftWrappedItem.productSize" />
								<dsp:oparam name="false">
					        		<p><fmt:message key="checkout.gifting.itemSize" />:&nbsp;<dsp:valueof param="giftWrappedItem.productSize"/></p>
					        	</dsp:oparam>
					        </dsp:droplet>
					        <p><fmt:message key="checkout.gifting.itemQuantity" />:&nbsp;<dsp:valueof param="giftWrappedItem.quantity"/></p>
					    </div>
					    <div class="col-md-8">
							 <dsp:include page="giftWrap.jsp">
							 	<dsp:param name="giftWrapItem" param="giftWrappedItem"/>
							 </dsp:include>
					    </div>
					 </div>
				  </div>
			  </div>
			  <hr class="horizontal-divider">
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>