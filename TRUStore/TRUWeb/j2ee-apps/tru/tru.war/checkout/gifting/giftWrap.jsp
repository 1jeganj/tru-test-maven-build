<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/registry/RepositoryTargeters/TRU/Checkout/GiftWrapItems"/>
	<dsp:importbean bean="/com/tru/commerce/droplet/TRUPriceDroplet" />
	<dsp:importbean bean="/atg/targeting/TargetingFirst"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
	<dsp:getvalueof var="previousGiftWrapProductId" param="giftWrapItem.wrapProductId" />
	<dsp:getvalueof var="previousGiftWrapSkuId" param="giftWrapItem.wrapSkuId" />
	<dsp:getvalueof var="indexValue" value="0" vartype="java.lang.Integer"/>
	<dsp:droplet name="TargetingFirst">
		<dsp:param name="targeter" bean="GiftWrapItems"/>
		<dsp:param name="elementName" value="targetedProduct"/>
		<dsp:oparam name="output">
			<dsp:droplet name="ForEach">
				<dsp:param name="array" param="targetedProduct.childSkus" />
				<dsp:param name="elementName" value="targetedSku" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="targetedSkuType" param="targetedSku.type" />
					<dsp:getvalueof var="targetedSkuSubType" param="targetedSku.subType" />
					<c:if test="${targetedSkuSubType eq 'TRUGiftWrap' or targetedSkuSubType eq 'BRUGiftWrap'}" >
						<dsp:getvalueof var="index" param="index"/>
						<c:if test="${index eq 0}">
							<dsp:getvalueof var="firstWrapProductId" param="targetedProduct.repositoryId" />
							<dsp:getvalueof var="firstWrapSkuId" param="targetedSku.repositoryId" />
							<c:choose>
								<c:when test="${empty previousGiftWrapSkuId}">
									<dsp:getvalueof var="defaultWrapProductId" value="${firstWrapProductId}" />
									<dsp:getvalueof var="defaultWrapSkuId" value="${firstWrapSkuId}" />
								</c:when>
								<c:otherwise>
									<dsp:getvalueof var="defaultWrapProductId" value="${previousGiftWrapProductId}" />
									<dsp:getvalueof var="defaultWrapSkuId" value="${previousGiftWrapSkuId}" />
								</c:otherwise>
							</c:choose>
							<div id="gift-wrap-price">
								<dsp:include page="giftWrapPrice.jsp">
									<dsp:param name="defaultWrapProductId" value="${defaultWrapProductId}" />
									<dsp:param name="defaultWrapSkuId" value="${defaultWrapSkuId}" />
								</dsp:include>
							</div>
							<dsp:getvalueof var="indexValue" value="1" vartype="java.lang.Integer"/>
						</c:if>
						<div class="gift-wrap-tru-block inline" onclick="applyGiftWrap(this,'<dsp:valueof param="giftWrapItem.shippingGroupId"/>', 
														'<dsp:valueof param="giftWrapItem.commItemRelationshipId"/>',
														'<dsp:valueof param="key"/>', 
														'<dsp:valueof param="giftWrapItem.parentCommerceId"/>',
														'<dsp:valueof param="targetedProduct.repositoryId"/>' ,
														'<dsp:valueof param="targetedSku.repositoryId"/>',
														'<dsp:valueof param="targetedProduct.repositoryId"/>' ,
														'<dsp:valueof param="targetedSku.repositoryId"/>','true')">
								<dsp:getvalueof var="wrapSkuId" param="targetedSku.repositoryId" />
								<dsp:getvalueof var="giftWrapImage" param="targetedSku.primaryImage" />
								<c:choose>
									<c:when test="${previousGiftWrapSkuId eq wrapSkuId}">
										<div class="gift-wrap-border border-visible" id='<dsp:valueof param="targetedSku.repositoryId"/>' >
											<div class="gift-wrap-opaque">
												<div class="gift-wrap-tru">
													<dsp:droplet name="IsEmpty">
														<dsp:param name="value" value="${giftWrapImage}" />
														<dsp:oparam name="false">
															<img src="${giftWrapImage}" alt="gift wrap"  width="140" height="56"/>
														</dsp:oparam>
														<dsp:oparam name="true">
															<img src="${TRUImagePath}images/no-image500.gif" alt="gift wrap"  width="140" height="56"/>
														</dsp:oparam>
													</dsp:droplet>
												</div>
											</div>
										</div>
										<div class="gift-wrap-check" style="display:block;"></div>
										<p><dsp:valueof param="targetedSku.displayName"/></p>
										<p><fmt:message key="checkout.giftwrap"/></p>
										
									</c:when>
									<c:otherwise>
										<div class="gift-wrap-border" id='<dsp:valueof param="targetedSku.repositoryId"/>' >
												<div class="gift-wrap-opaque">
													<div class="gift-wrap-tru" tabindex="0">
														<dsp:droplet name="IsEmpty">
															<dsp:param name="value" value="${giftWrapImage}" />
															<dsp:oparam name="false">
																<img src="${giftWrapImage}" alt="gift wrap"  width="140" height="56"/>
															</dsp:oparam>
															<dsp:oparam name="true">
																<img src="${TRUImagePath}images/no-image500.gif" alt="gift wrap" width="140" height="56" />
															</dsp:oparam>
														</dsp:droplet>
													</div>
												</div>
											</div>
											<div class="gift-wrap-check"></div>
											<p><dsp:valueof param="targetedSku.displayName"/></p>
											<p><fmt:message key="checkout.giftwrap"/></p>
									</c:otherwise>
								</c:choose>
						</div>
					</c:if>
				</dsp:oparam>
			</dsp:droplet><%-- END: ForEach --%>
		</dsp:oparam>
	</dsp:droplet><%-- END: TargetingFirst --%>
	<c:set var="giftWrapDivClass" value="gift-wrap-border"/>
	<c:if test="${empty previousGiftWrapSkuId }">
		<c:set var="giftWrapDivClass" value="gift-wrap-border border-visible"/>
	</c:if>
	<div class="gift-wrap-none-block inline">
		<div class='${giftWrapDivClass}' id="" >
			<div class="gift-wrap-none" tabindex="0" onclick="applyGiftWrap(this,'<dsp:valueof param="giftWrapItem.shippingGroupId"/>', 
				'<dsp:valueof param="giftWrapItem.commItemRelationshipId"/>', 
				'<dsp:valueof param="key"/>', 
				'<dsp:valueof param="giftWrapItem.parentCommerceId"/>', 
				'', '', 
				'${firstWrapProductId}',
				'${firstWrapSkuId}',
				'false')">
				<p>none</p>
			</div>
		</div>
	</div>
</dsp:page>   