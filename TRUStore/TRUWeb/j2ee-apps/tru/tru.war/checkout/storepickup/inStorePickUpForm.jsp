<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/com/tru/commerce/order/purchase/InStorePickUpFormHandler"/>

	<dsp:getvalueof var="inStorePickUpShippingGroup" param="shipmentVO.shippingGroup"/>
	<dsp:getvalueof var="storePickUpInfo" param="shipmentVO.storePickUpInfo"/>
	<dsp:getvalueof var="shippingGroupType" param="shippingGroupType"/>
	<dsp:getvalueof var="index" param="index"/>
			<div class="store-pickup-form">
				<div class="row">
					<div class="col-xs-5">
						<h3>
							<fmt:message key="checkout.pickup.pickupOwner" />
						</h3>
						<p>
							<span class="required-asterisk">*</span>
							<fmt:message key="checkout.pickup.required" />
						</p>
					</div>
					<div class="col-xs-7">
						<p class="id-disclaimer">
							<fmt:message key="checkout.pickup.validId" />
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<br>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6">
						<dsp:include page="inStorePickUpPrimaryContact.jsp" >
							<dsp:param name="storePickUpInfo" value="${storePickUpInfo}"/>
							<dsp:param name="index" param="index" />
						</dsp:include>
						<!-- End .primary-pickup-person -->
						<dsp:include page="inStorePickUpAlternateContact.jsp" >
							<dsp:param name="storePickUpInfo" value="${storePickUpInfo}"/>
							<dsp:param name="index" param="index" />
						</dsp:include>
					</div>
					<dsp:include page="storeDetails.jsp" >
						<dsp:param name="inStorePickUpShippingGroup" value="${inStorePickUpShippingGroup}"/>
					</dsp:include>
				</div>
			</div>
</dsp:page>