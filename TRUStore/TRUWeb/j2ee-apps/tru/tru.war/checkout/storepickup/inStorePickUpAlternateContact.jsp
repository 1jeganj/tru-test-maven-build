<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/com/tru/commerce/order/purchase/InStorePickUpFormHandler"/>
	
	<dsp:getvalueof var="storePickUpInfo" param="storePickUpInfo" />
	<dsp:getvalueof var="index" param="index" />			
	<c:set var="key" value="${storePickUpInfo.shippingGroupId}" />
	<dsp:getvalueof var="storePickUpInfosVOMap" bean="/atg/commerce/ShoppingCart.current.storePickUpInfos"/>
	<c:set var="v_alternateFirstName" value="${storePickUpInfosVOMap[key].alternateFirstName}" />
	<c:set var="v_alternateLastName" value="${storePickUpInfosVOMap[key].alternateLastName}" />
	<c:set var="v_alternatePhoneNumber" value="${storePickUpInfosVOMap[key].alternatePhoneNumber}" />		
	<c:set var="v_alternateEmail" value="${storePickUpInfosVOMap[key].alternateEmail}" />		
	<c:set var="v_alternateInfoRequired" value="${storePickUpInfosVOMap[key].alternateInfoRequired}" />	
		
	 <c:choose>
		<c:when test="${not empty v_alternateFirstName && not empty storePickUpInfo.alternateFirstName}">
			<c:set var="alternateFirstName" value="${v_alternateFirstName}" />
		</c:when>
		<c:when test="${not empty v_alternateFirstName && empty storePickUpInfo.alternateFirstName}">
			<c:set var="alternateFirstName" value="${v_alternateFirstName}" />
		</c:when>
		<c:otherwise>
			<c:set var="alternateFirstName" value="${storePickUpInfo.alternateFirstName}" />
		</c:otherwise>
	</c:choose>
	
	<c:choose>
		<c:when test="${not empty v_alternateLastName && not empty storePickUpInfo.alternateLastName}">
			<c:set var="alternateLastName" value="${v_alternateLastName}" />
		</c:when>
		<c:when test="${not empty v_alternateLastName && empty storePickUpInfo.alternateLastName}">
			<c:set var="alternateLastName" value="${v_alternateLastName}" />
		</c:when>
		<c:otherwise>
			<c:set var="alternateLastName" value="${storePickUpInfo.alternateLastName}" />
		</c:otherwise>
	</c:choose>
	
	<c:choose>
		<c:when test="${not empty v_alternatePhoneNumber && not empty storePickUpInfo.alternatePhoneNumber}">
			<c:set var="alternatePhoneNumber" value="${v_alternatePhoneNumber}" />
		</c:when>
		<c:when test="${not empty v_alternatePhoneNumber && empty storePickUpInfo.alternatePhoneNumber}">
			<c:set var="alternatePhoneNumber" value="${v_alternatePhoneNumber}" />
		</c:when>
		<c:otherwise>
			<c:set var="alternatePhoneNumber" value="${storePickUpInfo.alternatePhoneNumber}" />
		</c:otherwise>
	</c:choose>
	
	<c:choose>
		<c:when test="${not empty v_alternateEmail && not empty storePickUpInfo.alternateEmail}">
			<c:set var="alternateEmail" value="${v_alternateEmail}" />
		</c:when>
		<c:when test="${not empty v_alternateEmail && empty storePickUpInfo.alternateEmail}">
			<c:set var="alternateEmail" value="${v_alternateEmail}" />
		</c:when>
		<c:otherwise>
			<c:set var="alternateEmail" value="${storePickUpInfo.alternateEmail}" />
		</c:otherwise>
	</c:choose>
	
	<c:choose>
		<c:when test="${not empty v_alternateInfoRequired && not empty storePickUpInfo.alternateInfoRequired}">
			<c:set var="alternateInfoRequired" value="${v_alternateInfoRequired}" />
		</c:when>
		<c:when test="${not empty v_alternateInfoRequired && empty storePickUpInfo.alternateInfoRequired}">
			<c:set var="alternateInfoRequired" value="${v_alternateInfoRequired}" />
		</c:when>
		<c:when test="${not empty v_alternateInfoRequired}">
			<c:set var="alternateInfoRequired" value="${v_alternateInfoRequired}" />
		</c:when>
		<c:otherwise>
			<c:set var="alternateInfoRequired" value="${storePickUpInfo.alternateInfoRequired}" />
		</c:otherwise>
	</c:choose>
	
	<div class="alternate-pickup-person ${storePickUpInfo.shippingGroupId}-altChk" <c:if test="${alternateInfoRequired eq true}">style="display:block;"</c:if>>
		<div class="row">
			<div class="col-xs-12">
				<label>
					<p>
						<span class="required-asterisk">*</span>
						<fmt:message key="checkout.pickup.firstName" />
					</p>
				</label>
				<input name="altFirstName" id="altFirstName_${index}" data-id="altFirstName_ispu" class="alternatefirstname"  maxlength="30" value="${alternateFirstName}" />
				<dsp:input type="hidden" id="altFirstName_ispu" iclass="alternatefirstname" bean="InStorePickUpFormHandler.storePickUpInfos[${index}].alternateFirstName" value="${alternateFirstName}">
				</dsp:input>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<br>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<label>
					<p>
						<span class="required-asterisk">*</span>
						<fmt:message key="checkout.pickup.lastName" />
					</p>
				</label>
				<input name="altLastName" id="altLastName_${index}" data-id="altLastName_ispu" class="alternatelastname"  maxlength="30" value="${alternateLastName}" />
				<dsp:input type="hidden" id="altLastName_ispu" iclass="alternatelastname" bean="InStorePickUpFormHandler.storePickUpInfos[${index}].alternateLastName" value="${alternateLastName}">
				</dsp:input>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<br>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<label>
					<p>
						<span class="required-asterisk">*</span>
						<fmt:message key="checkout.pickup.telephone" />
					</p>
				</label>
				<input name="altPhoneNumber" id="altPhoneNumber_${index}" data-id="altPhoneNumber_ispu" class="alternatephonenumber splCharCheck"  value="${alternatePhoneNumber}" onkeypress="return isPhoneNumberFormat(event)" maxlength="20" />
				<dsp:input type="hidden" id="altPhoneNumber_ispu" iclass="alternatephonenumber splCharCheck" bean="InStorePickUpFormHandler.storePickUpInfos[${index}].alternatePhoneNumber"
							value="${alternatePhoneNumber}" onkeypress="return isPhoneNumberFormat(event)" maxlength="20">
				</dsp:input>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<br>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<br>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<label for="id-fa0b71f7-cd54-0d8b-fff1-aa05729159d1">
					<p>
						<span class="required-asterisk">*</span>
						<fmt:message key="checkout.pickup.email" />
					</p>
				</label>
				<input name="altEmail" id="altEmail_${index}" data-id="altEmail_ispu" class="alternateemail"  value="${alternateEmail}" />
				<dsp:input type="hidden" id="altEmail_ispu" iclass="alternateemail" bean="InStorePickUpFormHandler.storePickUpInfos[${index}].alternateEmail" value="${alternateEmail}">
				</dsp:input>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<br>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<br>
			</div>
		</div>
	</div>
</dsp:page>