<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	
	<dsp:getvalueof var="shippingGroupType" param="shippingGroupType" />

	<div class="store-pickup-items-accordion ui-accordion ui-widget ui-helper-reset" role="tablist">
		<div class="row accordion-header ui-accordion-header ui-state-default ui-corner-all ui-accordion-icons" role="tab" id="ui-id-1" 
																aria-controls="ui-id-2" aria-selected="false" aria-expanded="false" tabindex="0">
			<span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>
			<div class="col-xs-11">
				<h3>
					<fmt:message key="checkout.pickup.showItems" />
				</h3>
			</div>
			<div class="col-xs-1">
				<span class="accordion-expand-icon"></span>
				<span class="accordion-minimize-icon"></span>
			</div>
		</div>
		<div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" id="ui-id-2" aria-labelledby="ui-id-1" role="tabpanel"
			aria-hidden="true" style="display: none;">
			
			<dsp:droplet name="ForEach">
				<dsp:param name="array"	param="shippingDestinationVO.shipmentVOMap" />
				<dsp:param name="elementName" value="shipmentVO" />
				<dsp:oparam name="output">
                    <dsp:include page="/checkout/common/displayItemDetails.jsp">
						<dsp:param name="shipmentVO" param="shipmentVO" />
						<dsp:param name="pageName" param="pageName" />
						<dsp:param name="shippingGroupType" value="${shippingGroupType}" />
					</dsp:include>
                </dsp:oparam>
            </dsp:droplet>
		</div>
	</div>
</dsp:page>