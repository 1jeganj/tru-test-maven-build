<fmt:setBundle
	basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/com/tru/commerce/droplet/FetchStoreDetailsDroplet" />

	<dsp:getvalueof var="inStorePickUpShippingGroup" param="inStorePickUpShippingGroup" />

	<dsp:droplet name="FetchStoreDetailsDroplet">
		<dsp:param name="itemDescriptor" value="store" />
		<dsp:param name="id" value="${inStorePickUpShippingGroup.locationId}" />
		<dsp:param name="elementName" value="location" />
		<dsp:oparam name="output">
			<div class="col-xs-6">
				<div class="row">
					<div class="col-xs-12">
						<p>
							<span class="avenir-heavy"><fmt:message
									key="checkout.pickup.pickupLocation" /></span> &#183; <a
								href="#"
								class="dark-blue clickable" data-target="pickup-info-popover"
								data-original-title="" title=""><fmt:message
									key="checkout.shipping.learnMore" /></a>
									
									<div class="popover storePickupInfo" role="tooltip">
										<div class="arrow"></div>
										<ul>
										<li><fmt:message key="checkout.pickup.learnmore1of4" /></li>
										<li><fmt:message key="checkout.pickup.learnmore2of4" /></li>
										<li><fmt:message key="checkout.pickup.learnmore3of4" /></li>
										<%-- <li><fmt:message key="checkout.pickup.learnmore4of4" /></li> --%>
										</ul>
									</div>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p>
						
						<dsp:getvalueof var="storeName" param="location.name" />
						
						
						
						<c:set var="string2" value="${fn:split(storeName, '[')}" />
						
								<span>${string2[0]}</span>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p><dsp:valueof param="location.address1"/><br/><dsp:valueof param="location.address2"/></p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p><dsp:valueof param="location.city"/>,&nbsp;<dsp:valueof param="location.stateAddress"/>&nbsp;<dsp:valueof param="location.postalCode"/></p>
					</div>
				</div>
			</div>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>