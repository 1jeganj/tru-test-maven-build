<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof var="contextPath" value="${originatingRequest.contextPath}"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/com/tru/commerce/order/purchase/InStorePickUpFormHandler"/>
	<dsp:importbean bean="/com/tru/commerce/order/droplet/DisplayOrderDetailsDroplet" />
	<dsp:importbean bean="/com/tru/commerce/droplet/FetchStoreDetailsDroplet" />
	<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftCardFormHandler"/>
	<dsp:importbean bean="/com/tru/common/TRUUserSession" />
	<dsp:setvalue bean="GiftCardFormHandler.adjustPaymentGroups" value="submit" />
	<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
	 <dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
	  <dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
		<dsp:droplet name="GetSiteTypeDroplet">
			<dsp:param name="siteId" value="${site}"/>
			<dsp:oparam name="output">
				<dsp:getvalueof var="site" param="site"/>
				<c:set var="site" value ="${site}" scope="request"/>
			</dsp:oparam>
		</dsp:droplet> 

<dsp:getvalueof var="orderHasChannelISPUShippingGroups" bean="ShoppingCart.current.orderHasChannelISPUShippingGroups" />
<c:if test="${orderHasChannelISPUShippingGroups}">
	<dsp:setvalue bean="InStorePickUpFormHandler.initRegistryShippingGroups" value="submit"/>
</c:if>

	<dsp:setvalue bean="ShoppingCart.current.previousTab" beanvalue="ShoppingCart.current.currentTab"/>
	<dsp:setvalue bean="ShoppingCart.current.currentTab" value="pickup-tab"/>
	<dsp:importbean bean="/atg/commerce/locations/StoreLookupDroplet" />
	<input type="hidden" value="store-pickup" id="pageName" />
	<%-- Start Checkout Pickup template --%>
	<div class="checkout-pickup-container"
		id="co-pickup-tab">
			
			<div class="row">
				<dsp:form class="JSValidation1" id="inStorePickup" method="POST">
				<div class="col-md-8 checkout-left-column" >
					<div class="store-pickup-header">
						<div class="inline">
							<h1>
								<fmt:message key="checkout.pickup.freeStorePick" />
							</h1>
						</div>
				
						<dsp:getvalueof var="isUserAnonymous" bean="Profile.transient" />
						<dsp:getvalueof var="showSignIn" value="true"/>
						<dsp:droplet name="ForEach">
							<dsp:param name="array"	bean="ShoppingCart.current.shippingGroups"/>
							<dsp:param name="elementName" value="shippingGroup" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="sgType" param="shippingGroup.shippingGroupClassType" />
								<c:if test="${sgType eq 'hardgoodShippingGroup'}">
									<dsp:getvalueof var="nickName" param="shippingGroup.nickName" />
									<c:if test="${not empty nickName}"> 
										<dsp:getvalueof var="showSignIn" value="false"/>
									</c:if>
								</c:if>
							</dsp:oparam>
						</dsp:droplet>
						<%-- show the sign in button only if the user is annonymous and have only in store shipping group --%>
						<dsp:getvalueof var="shipToHomeItemsCount" bean="ShoppingCart.current.shipToHomeItemsCount" />
						<dsp:getvalueof var="signInHidden" bean="TRUUserSession.signInHidden"/>
                        <c:if test="${not signInHidden}">
							<c:if test="${shipToHomeItemsCount eq 0 and (isUserAnonymous eq true and showSignIn eq true and (!isSosVar || !(site eq 'sos')))}">
								<div class="store-pickup-header-signin pull-right">
									<a href="#" data-toggle="modal" data-target="#signInModal"
										id="signInBtn">&nbsp;<fmt:message key="checkout.pickup.signIn" /></a>
									<fmt:message key="checkout.for"/>&nbsp;<i><fmt:message key="checkout.faster"/></i>&nbsp;<fmt:message key="checkout.shipping.checkoutHeader"/>
								</div>
							</c:if>
                        </c:if>
						<!-- <div class="store-pick-error"></div> -->
						<p class="global-error-display"></p>
						<hr>
					</div>
						<%-- Start fix for TUW-53120 --%>
					<%-- Start PayPal integration changes --%>
<%-- 					<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="ShoppingCart.current.paymentGroups" />
						<dsp:param name="elementName" value="paymentGroup" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="pgType" param="paymentGroup.paymentGroupClassType" />
							<c:choose>
								<c:when test="${pgType eq 'payPal'}">
									<dsp:setvalue bean="ShoppingCart.current.previousTab" value="review-tab" />
								</c:when>
							</c:choose>
						</dsp:oparam>
					</dsp:droplet> --%>
					<%-- End PayPal integration changes --%>
						<%-- End fix for TUW-53120 --%>
					<dsp:droplet name="DisplayOrderDetailsDroplet">
						<dsp:param name="order" bean="ShoppingCart.current" />
						<dsp:param name="profile" bean="Profile" />
						<dsp:param name="page" value="store-pickup" />
						<dsp:param name="shippingDetails" value="false" />
						<dsp:param name="inStoreDetails" value="true" />
						<dsp:oparam name="output">
							
								<dsp:getvalueof var="shippingDestinationsVO" param="shippingDestinationsVO"/>
								<dsp:setvalue bean="InStorePickUpFormHandler.shippingGroupsCount" value="${fn:length(shippingDestinationsVO)}" />
								<dsp:input type="hidden" bean="InStorePickUpFormHandler.shippingGroupsCount" priority="1000" value="${fn:length(shippingDestinationsVO)}" />
								<dsp:droplet name="ForEach">
									<dsp:param name="array" param="shippingDestinationsVO" />
									<dsp:param name="elementName" value="shippingDestinationVO" />
									<dsp:oparam name="output">
										<dsp:getvalueof var="shippingGroupType" param="shippingDestinationVO.shippingGroupType"/>
										<dsp:getvalueof var="shippingDestinationIndex" param="index"/>
										<c:if test="${(shippingGroupType eq 'inStorePickupShippingGroup') or (shippingGroupType eq 'channelInStorePickupShippingGroup')}">
											<dsp:droplet name="ForEach">
												<dsp:param name="array"	param="shippingDestinationVO.shipmentVOMap" />
												<dsp:param name="elementName" value="shipmentVO" />
												<dsp:oparam name="output">
													<dsp:getvalueof var="locationId" param="shipmentVO.shippingGroup.locationId" />
													<div class="store-pickup-info-header">
														<h2>
															<%-- <dsp:valueof param="shippingDestinationVO.itemsCount" /> --%>
															<dsp:getvalueof var="itemsCount"
																param="shippingDestinationVO.itemsCount" />${itemsCount}&#32;
															<c:choose>
																<c:when test="${itemsCount gt 1}">
				                                                  	<fmt:message key="checkout.items.pickup"/>&#32;<fmt:message key="checkout.at"/>&#32; 
																</c:when>
																<c:otherwise>
																	<fmt:message key="checkout.item.pickup"/>&#32;<fmt:message key="checkout.at"/>&#32;
																</c:otherwise>
															</c:choose>
															<dsp:getvalueof var="inStorePickUpShippingGroup" param="inStorePickUpShippingGroup" />
															<dsp:droplet name="FetchStoreDetailsDroplet">
																	<dsp:param name="itemDescriptor" value="store" />
																	<dsp:param name="id" value="${locationId}" />
																	<dsp:param name="elementName" value="location" />
																<dsp:oparam name="output">
																	<dsp:getvalueof var="storeName" param="location.name" />
																	<c:set var="string2" value="${fn:split(storeName, '[')}" />
															          <span>${string2[0]}</span>
																</dsp:oparam>
														  </dsp:droplet>
															<%-- <dsp:droplet name="StoreLookupDroplet">
																<dsp:param name="id" value="${locationId}" />
																<dsp:param name="elementName" value="store" />
																<dsp:oparam name="output">
																	<dsp:valueof param="store.name" />
																</dsp:oparam>
															</dsp:droplet> --%>
														</h2>
													</div>
													<dsp:include page="inStorePickUpForm.jsp">
														<dsp:param name="shipmentVO" param="shipmentVO" />
														<dsp:param name="index" value="${shippingDestinationIndex}" />
														<dsp:param name="shippingGroupType" value="${shippingGroupType}" />
													</dsp:include>
													<dsp:include page="inStorePickUpItems.jsp" >
														<dsp:param name="shipmentVO" param="shipmentVO" />
														<dsp:param name="pageName" value="store-pickup" />
														<dsp:param name="shippingGroupType" value="${shippingGroupType}" />
													</dsp:include>							            
													<div class="spacer"></div>
												</dsp:oparam>
											</dsp:droplet>
										</c:if>
									</dsp:oparam>
								</dsp:droplet>
							
						</dsp:oparam>
					</dsp:droplet>
					<dsp:include page="/checkout/common/salesTaxEstimation.jsp"></dsp:include>			
				</div>
				</dsp:form>
				<div class="col-md-3 checkout-order-summary checkout-right-column">
					<dsp:include page="/checkout/common/checkoutOrderSummary.jsp">
						<dsp:param name="pageName" value="Store-Pickup" />
					</dsp:include>
				</div>
			</div>
	</div>
	<dsp:include page="/checkout/common/signInModal.jsp">
		<dsp:param name="requestPage" value="ispu" />
	</dsp:include>
	<dsp:include page="/myaccount/truForgotPassword.jsp"></dsp:include>
	<!-- /End Checkout Pickup template -->
	<div class="modal fade in inStorePickUp-findinstore" id="findInStoreModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<dsp:include page="/jstemplate/findInStoreInfo.jsp"/>
	</div>
	<div class="modal fade" id="removeItemConfirmationModal" tabindex="-1"
		role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content sharp-border">
				<a href="javascript:void(0)"  data-dismiss="modal">
					<span class="clickable"><img src="${TRUImagePath}images/close.png" alt="close"></span>
				</a>
				<div>
					<h2><fmt:message key="checkout.removeItemHeading" /></h2>
					<a href="javascript:void(0);" class="text-color" onclick="closeRemoveModal();"><fmt:message key="checkout.backtoPickup" /></a> <a href="javascript:void(0);" class="text-color" onclick="removeCartItemInReview()"><fmt:message key="checkout.removeItem" /></a>
					<input type="hidden" value="" id="removeItemId">
				</div>
			</div>
		</div>
	</div>
		<dsp:include page="/checkout/common/ad-Zone.jsp"/>
		
		<div id="tealiumCheckoutContent">
        
 			
 	   </div>

</dsp:page>