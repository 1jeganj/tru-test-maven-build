<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/com/tru/commerce/order/purchase/InStorePickUpFormHandler"/>
	
	<dsp:getvalueof var="storePickUpInfo" param="storePickUpInfo" />
	<dsp:getvalueof var="storePickUpInfosVOMap" bean="/atg/commerce/ShoppingCart.current.storePickUpInfos"/>
	<dsp:getvalueof var="storePickUpInfoVO" value="${storePickUpInfosVOMap['${storePickUpInfo.shippingGroupId}'] }" />
	
	<dsp:getvalueof var="index" param="index" />
	<dsp:input type="hidden" bean="InStorePickUpFormHandler.storePickUpInfos[${index}].shippingGroupId" value="${storePickUpInfo.shippingGroupId}" />
	<dsp:input type="hidden" bean="InStorePickUpFormHandler.storePickUpInfos[${index}].shippingGroupType" value="${storePickUpInfo.shippingGroupType}" />
	<c:set var="key" value="${storePickUpInfo.shippingGroupId}" />
	<c:set var="v_firstName" value="${storePickUpInfosVOMap[key].firstName}" />
	<c:set var="v_lastName" value="${storePickUpInfosVOMap[key].lastName}" />
	<c:set var="v_phoneNumber" value="${storePickUpInfosVOMap[key].phoneNumber}" />
	<c:set var="v_email" value="${storePickUpInfosVOMap[key].email}" />
	<c:set var="v_alternateInfoRequired" value="${storePickUpInfosVOMap[key].alternateInfoRequired}" />
	
   <c:choose>
		<c:when test="${not empty v_firstName && not empty storePickUpInfo.firstName}">
			<c:set var="firstName" value="${v_firstName}" />
		</c:when>
		<c:when test="${not empty v_firstName && empty storePickUpInfo.firstName}">
			<c:set var="firstName" value="${v_firstName}" />
		</c:when>			
		<c:otherwise>
			<c:set var="firstName" value="${storePickUpInfo.firstName}" />
		</c:otherwise>
	</c:choose>
	
	<c:choose>
		<c:when test="${not empty v_lastName && not empty storePickUpInfo.lastName}">
			<c:set var="lastName" value="${v_lastName}" />
		</c:when>
		<c:when test="${not empty v_lastName && not storePickUpInfo.lastName}">
			<c:set var="lastName" value="${v_lastName}" />
		</c:when>
		<c:otherwise>
			<c:set var="lastName" value="${storePickUpInfo.lastName}" />
		</c:otherwise>
	</c:choose>
	
	<c:choose>
		<c:when test="${not empty v_phoneNumber && not empty storePickUpInfo.phoneNumber}">
			<c:set var="phoneNumber" value="${v_phoneNumber}" />
		</c:when>
		<c:when test="${not empty v_phoneNumber && empty storePickUpInfo.phoneNumber}">
			<c:set var="phoneNumber" value="${v_phoneNumber}" />
		</c:when>
		<c:otherwise>
			<c:set var="phoneNumber" value="${storePickUpInfo.phoneNumber}" />
		</c:otherwise>
	</c:choose>
	
	<c:choose>
		<c:when test="${not empty v_email && not empty storePickUpInfo.email}">
			<c:set var="email" value="${v_email}" />
		</c:when>
		<c:when test="${not empty v_email && empty storePickUpInfo.email}">
			<c:set var="email" value="${v_email}" />
		</c:when>
		<c:otherwise>
			<c:set var="email" value="${storePickUpInfo.email}" />
		</c:otherwise>
	</c:choose>
	
	<c:choose>
		<c:when test="${not empty v_alternateInfoRequired && not empty storePickUpInfo.alternateInfoRequired}">
			<c:set var="alternateInfoRequired" value="${v_alternateInfoRequired}" />
		</c:when>
		<c:when test="${not empty v_alternateInfoRequired && empty storePickUpInfo.alternateInfoRequired}">
			<c:set var="alternateInfoRequired" value="${v_alternateInfoRequired}" />
		</c:when>
		<c:when test="${not empty v_alternateInfoRequired}">
			<c:set var="alternateInfoRequired" value="${v_alternateInfoRequired}" />
		</c:when>
		<c:otherwise>
			<c:set var="alternateInfoRequired" value="${storePickUpInfo.alternateInfoRequired}" />
		</c:otherwise>
	</c:choose>
	
	<div class="primary-pickup-person">
		<div class="row">
			<div class="col-xs-12">
				<label for="first_name">
					<span class="required-asterisk">*</span>
					<fmt:message key="checkout.pickup.firstName" />
				</label>
				<input name="firstName" id="firstName_${index}" data-id="first_name" class="firstname"  maxlength="30" value="${firstName}" />
				<dsp:input type="hidden" iclass="firstname"  id="first_name" bean="InStorePickUpFormHandler.storePickUpInfos[${index}].firstName" value="${firstName}" maxlength="30">
				</dsp:input>
				
				 
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<br>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<label for="last_name">
					<span class="required-asterisk">*</span>
					<fmt:message key="checkout.pickup.lastName" />
				</label>
				<input name="lastName" id="lastName_${index}" data-id="last_name" class="lastname"  maxlength="30" value="${lastName}" />
				<dsp:input type="hidden" iclass="lastname" id="last_name" bean="InStorePickUpFormHandler.storePickUpInfos[${index}].lastName" value="${lastName}" maxlength="30">
				</dsp:input>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<br>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<label for="phone_number">
					<span class="required-asterisk">*</span>
					<fmt:message key="checkout.pickup.telephone" />
				</label>
				<input name="phoneNumber" id="phoneNumber_${index}" data-id="phone_number" class="phonenumber splCharCheck"  onkeypress="return isPhoneNumberFormat(event)" maxlength="20" value="${phoneNumber}" />
				<dsp:input type="hidden" iclass="phonenumber splCharCheck" id="phone_number" bean="InStorePickUpFormHandler.storePickUpInfos[${index}].phoneNumber" value="${phoneNumber}"
									onkeypress="return isPhoneNumberFormat(event)" maxlength="20">
				</dsp:input>
				<button class="validateForms"><fmt:message key="checkout.place.order.now"/></button>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<br>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<label for="pickupstore_email">
					<span class="required-asterisk">*</span>
					<fmt:message key="checkout.pickup.email" />
				</label>
				<input name="email" id="email_${index}" data-id="pickupstore_email" class="pickupstoreemail"  value="${email}" />
				<dsp:input type="hidden" iclass="pickupstoreemail" id="pickupstore_email" bean="InStorePickUpFormHandler.storePickUpInfos[${index}].email" value="${email}">
				</dsp:input>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<br>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">				
				<c:choose>
					<c:when test="${alternateInfoRequired eq true}">
						<input type="checkbox" id="${storePickUpInfo.shippingGroupId}"
							class="alternate-pickup-checkbox" value="${alternateInfoRequired}" data-status="" checked="checked" />
					</c:when>
					<c:otherwise>
						<input type="checkbox" id="${storePickUpInfo.shippingGroupId}"
							class="alternate-pickup-checkbox" value="${alternateInfoRequired}" data-status="" />
					</c:otherwise>
				</c:choose>

				<label for="${storePickUpInfo.shippingGroupId}" class="alternate-pickup-label" id="${storePickUpInfo.shippingGroupId}-altChk">
					<p>
						<fmt:message key="checkout.pickup.alternatePickup" />
					</p>
				</label>
				<dsp:input type="hidden" iclass="alternate-pickup-checkbox-value ${storePickUpInfo.shippingGroupId}-altChk" 
						bean="InStorePickUpFormHandler.storePickUpInfos[${index}].alternateInfoRequired" value="${alternateInfoRequired}"/>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<br>
			</div>
		</div>
	</div>
</dsp:page>