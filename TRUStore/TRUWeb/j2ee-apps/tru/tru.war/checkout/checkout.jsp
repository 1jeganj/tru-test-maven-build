<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/atg/dynamo/droplet/Redirect" />
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:getvalueof var="currentTab" bean="ShoppingCart.current.currentTab" />
	<c:if test= "${empty currentTab}">
		<dsp:droplet name="Redirect">
			<dsp:param name="url" value="/cart/shoppingCart.jsp" />
		</dsp:droplet>
	</c:if>
	<tru:checkoutPageContainer>
		<jsp:body>
			<dsp:getvalueof var="_requestid" param="_requestid"/>  
			<!-- _requestid: ${_requestid}  -->
			<div id="payPalErrorMessages" class="truPayPal-div-hidden">
				<dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
					<dsp:param name="exceptions" bean="/atg/commerce/order/purchase/CommitOrderFormHandler.formExceptions" />
					<dsp:oparam name="output">
						<li><dsp:valueof param="message"/></li>
					</dsp:oparam>
				</dsp:droplet>
				<dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
					<dsp:param name="exceptions" bean="/atg/commerce/order/purchase/CartModifierFormHandler.formExceptions" />
					<dsp:oparam name="output">
						<li><dsp:valueof param="message"/></li>
					</dsp:oparam>
				</dsp:droplet>
			</div>
			<div id="loadNortonHiddenDiv" class="hide-data">
				<dsp:include page="../common/nortonSealScript.jsp" />
			</div>
			<div class="synchrony-card-print-container"></div>
			<div id="checkOutInclude"></div>
			<dsp:include page="/checkout/payment/exitPaymentOverlay.jsp" />
			<div class="modal fade" id="informationModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="display: none;">
				<div class="modal-dialog">
					<div class="modal-content sharp-border">
						<span class="clickable">
							<a href="javascript:void(0)" data-dismiss="modal"><img src="${TRUImagePath}images/close.png" alt="close modal"></a>
						</span>
						<div>
							<h2><fmt:message key="checkout.mergecart.orverlay.information" /></h2>
							<p><fmt:message key="checkout.mergecart.orverlay.msg" /></p>
						</div>
					</div>
				</div>
			</div>
			
			<%-- modal content --%>
			<div class="modal fade pdpModals" id="checkoutFeesPopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
				<div class="modal-dialog modal-sm">
					<div class="modal-content sharp-border">
						<div class="pdp-modal1">
							<div class="close-btn">
								<span class="sprite-icon-x-close" data-dismiss="modal"></span>
							</div>
							<header>                                                                            
							<p><fmt:message key="order.summary.ewaste.fees.text" /></p>
							</header>
						</div>
					</div>
				</div>
			</div>
			<dsp:droplet name="/com/tru/droplet/TRUFetchServerAndEarNameDroplet">
				<dsp:oparam name="output">
				<dsp:getvalueof var="earName" param="earName" />
				<dsp:getvalueof var="instanceName" param="serverName" />
				<dsp:getvalueof var="appDate" param="appDate" />
				<c:choose>
					<c:when test="${not empty instanceName and not empty earName}">
						<!-- Server name: ${instanceName} Ear: ${earName} Application Date : ${appDate} -->
					</c:when>
					<c:otherwise>
						<!-- Server name: ${instanceName}  Application Date : ${appDate} -->
					</c:otherwise>
				</c:choose>
				</dsp:oparam>
				<dsp:oparam name="empty">
				</dsp:oparam>
			</dsp:droplet>
			<textarea cols="60" rows="20" id="field_d1" name="field_d1"  hidden="true" title="textarea_field"></textarea>
		</jsp:body> 	
	</tru:checkoutPageContainer>
	<dsp:include page="/jstemplate/resourceBundle.jsp" />
</dsp:page>