<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<%-- for shipping messages --%>
<fmt:message key="tru.checkout.enterEamilAddress" var="enterEamilAddress"/>
<fmt:message key="tru.checkout.cardinalErrorMessage" var="cardinalMessage"/>
<fmt:message key="tru.checkout.creditCardExpiration" var="creditCardExpiration"/>
<fmt:message key="tru.checkout.paymentErrorPocessing" var="paymentErrorPocessing"/> 
<fmt:message key="tru.checkout.inValidEmailFormat" var="inValidEmailFormat"/>
<fmt:message key="tru.checkout.cardDetails" var="cardDetails"/>
<fmt:message key="tru.checkout.selectAddress" var="selectAddress"/>
<fmt:message key="tru.checkout.addressSavedSuccessfully" var="addressSavedSuccessfully"/>  
<fmt:message key="tru.checkout.cardRemovedSuccessfully" var="cardRemovedSuccessfully"/>
<fmt:message key="tru.checkout.inValidCardSelection" var="inValidCardSelection"/>
<script type="text/javascript">
var ch_enterEamilAddress = "${fn:escapeXml(enterEamilAddress)}" ;
var ch_cardinalMessage = "${fn:escapeXml(cardinalMessage)}" ;
var ch_creditCardExpiration = "${fn:escapeXml(creditCardExpiration)}" ;
var ch_paymentErrorPocessing = "${fn:escapeXml(paymentErrorPocessing)}" ;
var ch_inValidEmailFormat = "${fn:escapeXml(inValidEmailFormat)}" ;
var ch_cardDetails = "${fn:escapeXml(cardDetails)}" ;
var ch_selectAddress = "${fn:escapeXml(selectAddress)}" ;
var ch_addressSavedSuccessfully = "${fn:escapeXml(addressSavedSuccessfully)}" ;
var ch_cardRemovedSuccessfully = "${fn:escapeXml(cardRemovedSuccessfully)}" ;
var ch_inValidCardSelection = "${fn:escapeXml(inValidCardSelection)}" ;
</script>	

</dsp:page>