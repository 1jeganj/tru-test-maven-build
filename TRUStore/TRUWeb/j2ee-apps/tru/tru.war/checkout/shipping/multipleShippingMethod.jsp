<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:getvalueof var="shippingGroupId" param="cartItemDetailVO.shipGroupId" />
    <dsp:getvalueof var="relationShipId" param="cartItemDetailVO.id" />
    <dsp:getvalueof var="commerceItemId" param="cartItemDetailVO.commerceItemId" />
    <dsp:getvalueof var="selectedShippingMethod" param="cartItemDetailVO.selectedShippingMethod" />
    <dsp:getvalueof var="selectedShippingMethodPrice" param="cartItemDetailVO.selectedShippingMethodPrice" />
    <dsp:getvalueof var="uniqueId" param="cartItemDetailVO.uniqueId" />
    <dsp:getvalueof var="wrapCommerceItemId" param="cartItemDetailVO.wrapCommerceItemId" />
	<div class="avenir-heavy"><fmt:message key="checkout.shipping.method"/></div>
	<input type="hidden" id="isMultiShippingPage" value="true" />
	<div class="ship-to-dropdown-container ${shippingGroupId}" id="${commerceItemId}">	
		<select class="ship-to-dropdown" id="shippingMethod_${relationShipId}" 
			name="shippingMethod_${relationShipId}" onchange="selectShippingMethod(this, '${shippingGroupId}', '${relationShipId}', '${uniqueId}', '${wrapCommerceItemId}')" >
			<dsp:droplet name="/atg/dynamo/droplet/ForEach">
				<dsp:param name="array" param="shipmentVO.shipMethodVOs" />
				<dsp:param name="elementName" value="shipMethodVO" />
				<dsp:oparam name="output">
				    <dsp:getvalueof var="shipMethodName" param="shipMethodVO.shipMethodName" />
					<dsp:getvalueof var="shipMethodCode" param="shipMethodVO.shipMethodCode" />
					<dsp:getvalueof var="shippingPrice" param="shipMethodVO.shippingPrice"/>
					<dsp:getvalueof var="regionCode" param="shipMethodVO.regionCode"/>
					<dsp:getvalueof var="delivertTimeLine" param="shipMethodVO.delivertTimeLine"/>
					
					<dsp:getvalueof var="details" param="shipMethodVO.details"/>
		     		<c:choose>
			     		<c:when test="${selectedShippingMethod eq shipMethodCode}">
							<option class="${shippingPrice}" value="${shipMethodCode}" selected="selected">${shipMethodName}&nbsp;&nbsp;.&nbsp;&nbsp;${delivertTimeLine}</option>
			     			<c:set var="selectedShippingMethodCode" value="${shipMethodCode}"/>
						</c:when>
						<c:otherwise>
							<option class="${shippingPrice}" value="${shipMethodCode}">${shipMethodName} &nbsp;&nbsp;.&nbsp;&nbsp;${delivertTimeLine}</option>
						</c:otherwise>
		     		</c:choose>		  
					<c:choose>
			     		<c:when test="${shipMethodCode eq 'TRU_Std_Gnd'}">
			     			<c:set var="TRU_Std_Gnd" value="${details}"/>
			     		</c:when>
			     		<c:when test="${shipMethodCode eq 'GROUND'}">
			     			<c:set var="GROUND" value="${details}"/>
			     		</c:when>
			     		<c:when test="${shipMethodCode eq 'ANY_2Day'}">
			     			<c:set var="ANY_2Day" value="${details}"/>
			     		</c:when>
			     		<c:when test="${shipMethodCode eq '1DAY'}">
			     			<c:set var="ONEDAY" value="${details}"/>
			     		</c:when>
			     		<c:when test="${shipMethodCode eq 'USPS_PRIORITY'}">
			     			<c:set var="USPS_PRIORITY" value="${details}"/>
			     		</c:when>
			     		<c:when test="${shipMethodCode eq 'TRUCK'}">
			     			<c:set var="TRUCK" value="${details}"/>
			     		</c:when>
			     	</c:choose>
				</dsp:oparam>
				<dsp:oparam name="empty">
					<fmt:message key="checkout.shipping.no.shipmethods.found"/>
				</dsp:oparam>
			</dsp:droplet>
		</select>
		<!-- Start: Demo issue fix - multishipping page -->
		<!--<div class="spacer"></div> --> <!-- This is not matching with fabricator so removed this div block -->
		<!-- End: Demo issue fix - multishipping page -->

		<dsp:droplet name="/atg/dynamo/droplet/ForEach">
			<dsp:param name="array" param="shipmentVO.shipMethodVOs" />
			<dsp:param name="elementName" value="shipMethodVO" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="shipMethodName"
					param="shipMethodVO.shipMethodName" />
				<dsp:getvalueof var="shipMethodCode"
					param="shipMethodVO.shipMethodCode" />
				<dsp:getvalueof var="shippingPrice"
					param="shipMethodVO.shippingPrice" />
				<dsp:getvalueof var="regionCode" param="shipMethodVO.regionCode" />
				<dsp:getvalueof var="details" param="shipMethodVO.details" />

				<c:choose>
					<c:when test="${shipMethodCode eq 'TRU_Std_Gnd'}">
						<%-- <c:set var="TRU_Std_Gnd" value="${details}"/> --%>
						<span class="hidden_shipping_details TRU_Std_Gnd">${details}</span>
					</c:when>
					<c:when test="${shipMethodCode eq 'GROUND'}">
						<%-- <c:set var="GROUND" value="${details}"/> --%>
						<span class="hidden_shipping_details GROUND">${details}</span>
					</c:when>
					<c:when test="${shipMethodCode eq 'ANY_2Day'}">
						<%-- <c:set var="ANY_2Day" value="${details}"/> --%>
						<span class="hidden_shipping_details ANY_2Day">${details}</span>
					</c:when>
					<c:when test="${shipMethodCode eq '1DAY'}">
						<%-- <c:set var="ONEDAY" value="${details}"/> --%>
						<span class="hidden_shipping_details 1DAY">${details}</span>
					</c:when>
					<c:when test="${shipMethodCode eq 'USPS_PRIORITY'}">
						<%-- <c:set var="USPS_PRIORITY" value="${details}"/> --%>
						<span class="hidden_shipping_details USPS_PRIORITY">${details}</span>
					</c:when>
					<c:when test="${shipMethodCode eq 'TRUCK'}">
						<%-- <c:set var="TRUCK" value="${details}"/> --%>
						<span class="hidden_shipping_details TRUCK">${details}</span>
					</c:when>
				</c:choose>
			</dsp:oparam>
		</dsp:droplet>
		<%-- <input type="hidden" class="details" value="${details}" id="${shipMethodCode}">	 --%>
		<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
   		<dsp:param name="value" param="shipmentVO.shipMethodVOs"/>
	   		<dsp:oparam name="false">
	   			<span class="blue-text"> 
	   			<c:choose>
					<c:when test="${selectedShippingMethodPrice == 0.0}">
						<fmt:message key="shoppingcart.free" />
					</c:when>
					<c:otherwise>
						<dsp:valueof format="currency"	value="${selectedShippingMethodPrice}" locale="en_US" converter="currency" />
					</c:otherwise>
				</c:choose>
				</span>
					<span class="avenir-medium">&#xB7;</span>
					<span class="spacer-details"><a href="javascript:void(0)" class="co-multi-shipping-method-details">
						<fmt:message key="checkout.payment.details"/></a>
						<div class="popover bottom multiShippingDetailToolTip" role="tooltip"></div>
					</span>
	   		</dsp:oparam>
   		</dsp:droplet>		
	</div>
</dsp:page>
