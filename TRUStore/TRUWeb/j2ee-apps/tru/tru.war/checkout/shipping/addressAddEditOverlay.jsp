<fmt:setBundle
	basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:getvalueof var="currentTab" bean="ShoppingCart.current.currentTab" />
	<dsp:getvalueof var="isEditSavedAddress" vartype="java.lang.boolean" value="false" />
	<dsp:getvalueof var="fromSuggestedAddress" vartype="java.lang.boolean" value="false" />
			<div class="modal-content sharp-border">
                <div class="my-account-add-address-overlay normalHeight">
                    <div class="row">
                        <div class="col-md-5 col-md-offset-7 your-addresses-background">
                            <div class="row top-margin">
                                <div class="col-md-1 col-md-offset-10">
                                    <a href="javscript:void(0)" id="closeAddressModel"  data-dismiss="modal"><span class="clickable"><img src="${TRUImagePath}images/close.png" alt="close model"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
					<div class="col-md-7 set-height">
					<dsp:droplet name="Switch">
						<dsp:param name="value" param="fromSuggestedAddress" />
						<dsp:oparam name="true">
							<dsp:getvalueof var="fromSuggestedAddress" vartype="java.lang.boolean" value="true" />
							<dsp:getvalueof var="nickname" param="nickname" />
							<dsp:getvalueof var="firstName" param="firstName" />
							<dsp:getvalueof var="lastName" param="lastName" />
							<dsp:getvalueof var="address1" param="address1" />
							<dsp:getvalueof var="address2" param="address2" />
							<dsp:getvalueof var="city" param="city" />
							<dsp:getvalueof var="state" param="state" />
							<dsp:getvalueof var="country" param="country" />
							<dsp:getvalueof var="postalCode" param="postalCode" />
							<dsp:getvalueof var="phoneNumber" param="phoneNumber" />
							<dsp:getvalueof var="email" param="email" />
							<dsp:droplet name="ForEach">
								<dsp:param name="array" bean="ShippingGroupContainerService.shippingGroupMapForDisplay" />
								<dsp:param name="elementName" value="shippingGroupMap" />
								<dsp:oparam name="output">
									<dsp:getvalueof var="addressKey" param="key" />
									<c:if test="${nickname eq addressKey}">
										<dsp:getvalueof var="isEditSavedAddress" vartype="java.lang.boolean" value="true" />
										<%-- <dsp:getvalueof var="editAddress" param="shippingGroupMap.shippingAddress" /> --%>
										<dsp:getvalueof var="editNickName" param="nickName" />
									</c:if> 
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
						<dsp:oparam name="default">
							<dsp:droplet name="IsEmpty">
								<dsp:param name="value" param="editNickName" />
								<dsp:oparam name="false">
									<dsp:getvalueof var="editNickName" param="editNickName" />
									<dsp:droplet name="ForEach">
										<dsp:param name="array" bean="ShippingGroupContainerService.shippingGroupMapForDisplay" />
										<dsp:param name="elementName" value="shippingGroupMap" />
										<dsp:oparam name="output">
											<dsp:getvalueof var="addressKey" param="key" />
											<c:if test="${editNickName eq addressKey}">
												<dsp:getvalueof var="isEditSavedAddress" vartype="java.lang.boolean" value="true" />
												<dsp:getvalueof var="editAddress" param="shippingGroupMap.shippingAddress" />
												<dsp:getvalueof var="shippingGroup" param="shippingGroupMap" />
											</c:if> 
										</dsp:oparam>
									</dsp:droplet>
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>
					<dsp:getvalueof var="orderShippingAddress" vartype="java.lang.boolean" value="${shippingGroup.orderShippingAddress}" />
					<form id="myShippingAddressForm" method="POST" onsubmit="javascript: return onSubmitShippingAddress();" class="JSValidation">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2><fmt:message key="checkout.shipping.overlay.addressInformation" /></h2>
                                    <p><fmt:message key="checkout.shipping.overlay.shippingAvailability" /></p>
                                </div>
                            </div>
                            <div class="tse-scrollable"><div class="tse-scrollbar" style="display: none;"><div class="drag-handle visible"></div></div>
                                <div class="tse-scroll-content">
                                    <div class="tse-content">
                                    	<p class="global-error-display"></p>
                                        <div class="row">
                                            <div class="col-md-9">
                                            	
                                            	<c:choose>
                                            		<c:when test="${currentTab eq 'payment-tab' and orderShippingAddress}">
                                            			<input type="hidden" name="billingAddressSameAsShippingAddress" value="true" class="java.lang.boolean" />
														<input type="hidden" name="isEditSavedAddress" value="false" />
                                            		</c:when>
                                            		<c:otherwise>
		                                            	<input type="hidden" name="isEditSavedAddress" value="${isEditSavedAddress}" />
                                            		</c:otherwise>
                                            	</c:choose>
                                            	
                                            	<input type="hidden" name="addressValidated" value="false" />
                                                <label for="id-70638108-d363-aa1e-887d-4d82b1b5300b">
                                                    <span class="avenir-heavy"><span class="required-asterisk">*</span>&nbsp;<fmt:message key="myaccount.address.nickname" /></span>
                                                </label>
                                                
                                                <c:choose>
	                                                <c:when test="${fromSuggestedAddress eq true}">
		                                                <c:choose>
		                                                	<c:when test="${currentTab eq 'payment-tab' and orderShippingAddress}">
		                                                		<input type="text" name="nickname" value="${nickname}" maxlength="42"/>
		                                                	</c:when>
			                                           		<c:when test="${isEditSavedAddress eq true}">
	                                            				<input type="text" name="editNickName" value="${nickname}" disabled="disabled" />   
																<input type="hidden" name="nickname" value="${nickname}" /> 
	                                            			</c:when>
	                                            			<c:otherwise>
	                                            				<input type="text" name="nickname" value="${nickname}" maxlength="42"/>
	                                            			</c:otherwise>
	                                           			</c:choose>
		                                           	</c:when>
		                                           	<c:when test="${currentTab eq 'payment-tab' and orderShippingAddress}">
		                                           		<input type="text" name="nickname" value="${editNickName}" maxlength="42"/>
		                                           	</c:when>
	                                                <c:when test="${isEditSavedAddress eq true}">
	                                                	<input type="text" id="id-70638108-d363-aa1e-887d-4d82b1b5300b" name="editNickName" value="${editNickName}" disabled="disabled" maxlength="42"/>
	                                                	<input type="hidden" name="nickname" value="${editNickName}" />
	                                                </c:when>
	                                                <c:otherwise>
	                                                	<input type="text" id="id-70638108-d363-aa1e-887d-4d82b1b5300b" placeholder="(Home, Office)" name="nickname" maxlength="42">
	                                                </c:otherwise>
                                                </c:choose>
                                                <label for="id-c0464a8c-3288-0a55-8b4e-174d0ea2a392">
                                                <span class="avenir-heavy"><span class="required-asterisk">*</span>&nbsp;<fmt:message key="checkout.shipping.firstname" /></span>
                                                </label>
                                                <c:choose>
	                                            	<c:when test="${fromSuggestedAddress eq true}">
	                                            		<input type="text" name="firstName" value="${firstName}" maxlength="30"/>
	                                            	</c:when>                                                
	                                                <c:when test="${isEditSavedAddress eq true}">
	                                                	<input type="text" id="id-c0464a8c-3288-0a55-8b4e-174d0ea2a392" name="firstName" maxlength="30" value="${editAddress.firstName}">
	                                                </c:when>
	                                                <c:otherwise>
	                                                	<input type="text" id="id-c0464a8c-3288-0a55-8b4e-174d0ea2a392" name="firstName" maxlength="30">
	                                                </c:otherwise> 
                                                </c:choose>
                                                <label for="id-d8b9e0e4-d14d-cea7-f4f3-31c851b655dc">
                                                    <span class="avenir-heavy"><span class="required-asterisk">*</span>&nbsp;<fmt:message key="checkout.shipping.lastname" /></span>
                                                </label>
                                                
                                                <c:choose>
	                                            	<c:when test="${fromSuggestedAddress eq true}">
	                                            		<input type="text" name="lastName" value="${lastName}" maxlength="30"/>
	                                            	</c:when>                                               
	                                                <c:when test="${isEditSavedAddress eq true}">
	                                                	<input type="text" id="id-d8b9e0e4-d14d-cea7-f4f3-31c851b655dc" name="lastName" maxlength="30" value="${editAddress.lastName}">
	                                                </c:when>
	                                                <c:otherwise>
	                                                	<input type="text" id="id-d8b9e0e4-d14d-cea7-f4f3-31c851b655dc" maxlength="30" name="lastName">
	                                                </c:otherwise> 
                                                </c:choose>
                                                <label for="id-514e725f-7a00-6771-119b-c593b113dec1">
                                                    <span class="avenir-heavy"><span class="required-asterisk">*</span>&nbsp;<fmt:message key="checkout.shipping.address1" /></span>
                                                </label>
                                                <c:choose>
                                                	<c:when test="${fromSuggestedAddress eq true}">
	                                            		<input type="text" name="address1" value="${address1}" maxlength="50"/>
	                                            	</c:when>
	                                                <c:when test="${isEditSavedAddress eq true}">
	                                                	<input type="text" id="id-514e725f-7a00-6771-119b-c593b113dec1" name="address1" value="${editAddress.address1}">
	                                                </c:when>
	                                                <c:otherwise>
	                                                	<input type="text" id="id-514e725f-7a00-6771-119b-c593b113dec1" name="address1" maxlength="50">
	                                                </c:otherwise> 
                                                </c:choose>
                                                
                                                
                                                
                                                <label for="id-1822d72a-857d-7e72-250b-86fcf3382ddf">
                                                    <p><fmt:message key="checkout.shipping.suite" /> </p>
                                                </label>
                                                
                                                   <c:choose>
                                                   		<c:when test="${fromSuggestedAddress eq true}">
		                                            		<input type="text" name="address2" value="${address2}" maxlength="50"/>
		                                            	</c:when>
		                                                <c:when test="${isEditSavedAddress eq true}">
		                                                	<input type="text" id="id-1822d72a-857d-7e72-250b-86fcf3382ddf" name="address2" value="${editAddress.address2}">
		                                                </c:when>
		                                                <c:otherwise>
		                                                	<input type="text" id="id-1822d72a-857d-7e72-250b-86fcf3382ddf" name="address2" maxlength="50">
		                                                </c:otherwise> 
                                                </c:choose>
                                             
                                                <label for="id-48c949d2-7c1c-2249-acd1-e9e80fe0a0ce">
                                                    <span class="avenir-heavy"><span class="required-asterisk">*</span>&nbsp;<fmt:message key="checkout.shipping.city" /></span>
                                                </label>
                                                <c:choose>
                                                <c:when test="${fromSuggestedAddress eq true}">
                                            		<input type="text" name="city" value="${city}" maxlength="30"/>
                                            	</c:when>
                                                <c:when test="${isEditSavedAddress eq true}">
                                                	<input type="text" id="id-48c949d2-7c1c-2249-acd1-e9e80fe0a0ce" name="city" value="${editAddress.city}" maxlength="30">
                                                </c:when>
                                                <c:otherwise>
                                                	<input type="text" id="id-48c949d2-7c1c-2249-acd1-e9e80fe0a0ce" name="city" maxlength="30">
                                                </c:otherwise> 
                                                </c:choose>
                                                <label>
                                                    <span class="avenir-heavy"><span class="required-asterisk">*</span>&nbsp;<fmt:message key="checkout.shipping.state" />&nbsp;</span>
                                                </label>
                                                <br>
                                                
                                                
                                                <div class="select-wrapper">
                                                <c:choose>
                                                <c:when test="${fromSuggestedAddress eq true}">
                                            		<select name="state" id="state" value="${state}">
	                                                    <option value=""><fmt:message key="checkout.shipping.state.select"/></option>
	                                                    <dsp:droplet name="ForEach">
														  <dsp:param name="array" bean="/atg/commerce/util/StateList_US.places"/>
														  <dsp:param name="elementName" value="state"/>
														  <dsp:oparam name="output">
															  <dsp:getvalueof var="stateCode" param="state.code"></dsp:getvalueof>
															  	<option value='<dsp:valueof param="state.code" />'
						                                              <dsp:droplet name="/atg/dynamo/droplet/Compare">
						                                                      <dsp:param name="obj1" param="state.code"/>
						                                                      <dsp:param name="obj2" value="${state}"/>
						                                                      <dsp:oparam name="equal">
						                                                          selected="selected"
						                                                      </dsp:oparam>
						                                              </dsp:droplet> >
						                                              <dsp:valueof param="state.code"/>
						                                         </option>
														  </dsp:oparam>
													</dsp:droplet>
	                                                    <!-- <option value="other">other</option> -->
	                                                </select>
                                            	</c:when>
                                               <c:when test="${isEditSavedAddress eq true }">	
				                                                <select name="state" id="state" value="${editAddress.state}">
				                                                    <option value=""><fmt:message key="checkout.shipping.state.select"/></option>
				                                                    <dsp:droplet name="ForEach">
																	  <dsp:param name="array" bean="/atg/commerce/util/StateList_US.places"/>
																	  <dsp:param name="elementName" value="state"/>
																	  <dsp:oparam name="output">
																		  <dsp:getvalueof var="stateCode" param="state.code"></dsp:getvalueof>
																		  	<option value='<dsp:valueof param="state.code" />'
									                                              <dsp:droplet name="/atg/dynamo/droplet/Compare">
									                                                      <dsp:param name="obj1" param="state.code"/>
									                                                      <dsp:param name="obj2" value="${editAddress.state}"/>
									                                                      <dsp:oparam name="equal">
									                                                          selected="selected"
									                                                      </dsp:oparam>
									                                              </dsp:droplet> >
									                                              <dsp:valueof param="state.code"/>
									                                         </option>
																	  </dsp:oparam>
																</dsp:droplet>
				                                                   <!--  <option value="other">other</option> -->
				                                                </select>
				                                          		
															</c:when>															 
															<c:otherwise>
					                                            <select name="state" id="state" >
					                                                <option value=""><fmt:message key="checkout.shipping.state.select"/></option>
					                                                	<dsp:droplet name="ForEach">
																		  <dsp:param name="array" bean="/atg/commerce/util/StateList_US.places"/>
																		  <dsp:param name="elementName" value="state"/>
																		  <dsp:oparam name="output">
																			  <dsp:getvalueof var="stateCode" param="state.code"></dsp:getvalueof>
																			  <option value="${stateCode}"><dsp:valueof param="state.code" /></option>
																		  </dsp:oparam>
																		</dsp:droplet>
				                                                    <!-- <option value="other">other</option> -->
				                                                </select>
				                                           </c:otherwise>
			                                           </c:choose>
                                                </div>
                                                <label for="id-d31985ad-a9f9-0ff3-463e-5e673b75f0f9">
                                                    <span class="avenir-heavy"><span class="required-asterisk">*</span>&nbsp;<fmt:message key="checkout.shipping.zip" />&nbsp;</span>
                                                </label>
                                                <input type="hidden" name="country" id="changedCountry" value="US" />
                                                     <c:choose>
                                                     <c:when test="${fromSuggestedAddress eq true}">
	                                            		<input type="text" id="zip" name="postalCode" value="${postalCode}" maxlength="10" />
	                                            	</c:when>
                                                <c:when test="${isEditSavedAddress eq true}">
                                                	<input type="text" id="zip" name="postalCode" value="${editAddress.postalCode}" maxlength="10">
                                                </c:when>
                                                <c:otherwise>
                                                	<input type="text" id="zip" name="postalCode" maxlength="10">
                                                </c:otherwise> 
                                                </c:choose>
                                                
                                                <label for="id-0d52fb6a-c934-ed9c-b88b-65c9a304fffd">
                                                    <span class="avenir-heavy"><span class="required-asterisk">*</span>&nbsp;<fmt:message key="checkout.shipping.telephone" /></span>
                                                </label>
                                                <c:choose>
                                                <c:when test="${fromSuggestedAddress eq true}">
                                            		<input type="text" name="phoneNumber" value="${phoneNumber}" class="splCharCheck"  onkeypress="return isPhoneNumberFormat(event)" maxlength="20"/>
                                            	</c:when>
                                                <c:when test="${isEditSavedAddress eq true }">
			                                        <input type="text" name="phoneNumber" class="splCharCheck"  onkeypress="return isPhoneNumberFormat(event)" value="${editAddress.phoneNumber}" maxlength="20"/>
			                                     </c:when>
			                                     <c:otherwise>
			                                         <input type="text" class="splCharCheck"  onkeypress="return isPhoneNumberFormat(event)" name="phoneNumber" value="" maxlength="20"/>
			                                      </c:otherwise>
			                                       </c:choose>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row top-border attach-to-bottom">
                                <div class="col-md-12">
                                   <input type="submit" id="submitAddressId" priority="-10" name="addShippingAddress" value="submit" style="display: none;" />
			                                <button onclick="return modifyAddressShippingOverlay();"><fmt:message key="myaccount.save" /></button>
                                </div>
                            </div>
                            </form>
                        </div>
						<dsp:include page="displayAddress.jsp"/>
                    </div>
                </div>
           </div>
</dsp:page>