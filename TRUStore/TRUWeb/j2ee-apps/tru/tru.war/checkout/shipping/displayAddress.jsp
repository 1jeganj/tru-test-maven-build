<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:getvalueof var="currentTab" bean="ShoppingCart.current.currentTab" />
	<div class="col-md-5 your-addresses-background set-height">
		<div class="row">
			<div class="col-md-12">
				<h3>
					<fmt:message key="myaccount.your.addresses" />
				</h3>
			</div>
		</div>
		<div class="tse-scrollable">
			<div class="tse-scrollbar" style="display: none;">
				<div class="drag-handle visible"></div>
			</div>
			<div class="tse-scrollbar" style="display: none;">
				<div class="drag-handle visible"></div>
			</div>
			<div class="tse-scroll-content" style="width: 503px; height: 380px;">
				<div class="tse-content">
					<dsp:getvalueof var="defaultShippingGroupName" bean="ShippingGroupContainerService.defaultShippingGroupName" />
					<dsp:droplet name="IsEmpty">
						<dsp:param name="value" bean="ShippingGroupContainerService.defaultShippingGroupName" />
						<dsp:oparam name="false">
							<dsp:droplet name="ForEach">
					           	<dsp:param name="array" bean="ShippingGroupContainerService.shippingGroupMapForDisplay"/>
					           	<dsp:param name="elementName" value="shippingGroup"/>					           
					           	<dsp:oparam name="output">
					           		<dsp:getvalueof var="isBillingAddress" param="shippingGroup.billingAddress" />
									<c:if test="${not isBillingAddress}">
										<dsp:getvalueof var="nickName" param="key" />
										<dsp:getvalueof var="softDeleted" param="shippingGroup.softDeleted" vartype="java.lang.boolean"/>
										<c:if test="${nickName eq defaultShippingGroupName and ((currentTab ne 'payment-tab') or (currentTab eq 'payment-tab' and not softDeleted))}">
											<dsp:setvalue param="address" paramvalue="shippingGroup.shippingAddress"/>
					           				<div class="small-spacer"></div>
					             			<div class="row">
					                 			<div class="col-md-12">
					                    			<p><span class="dark-blue avenir-heavy"><dsp:valueof param="key" /></span>&nbsp;.&nbsp;<span class="default-address"><fmt:message key="myaccount.default.address" /></span></p>
					                     			<p class="avenir-heavy"><dsp:valueof param="address.firstName" />&nbsp;<dsp:valueof param="address.lastName" /></p>
					                     			<p><dsp:valueof param="address.address1" />&nbsp;<dsp:valueof param="address.address2" /></p>
					                     			<p><dsp:valueof param="address.city" />, <dsp:valueof param="address.state" />&nbsp;<dsp:valueof param="address.postalCode" /></p>
					                     			<div class="row">
					                         			<div class="col-md-8">
					                             			<p>
					                             				<span class="blue">
					                             					<a href="javascript:void(0);" class="checkoutEditAddress"><fmt:message key="myaccount.edit" /></a>
					                             					<span class="nickNameInHiddenSpan">${nickName}</span>
					                             				</span>
					                             			</p>
					                       				</div>
														<div class="col-md-1">
															<a href="javascript:void(0)" class="delete-icon checkoutRemoveAddress" title="delete address" data-toggle="modal" data-target="#myAccountDeleteCancelModal">
																 <img src="${TRUImagePath}images/delete-icon.png" alt="delete">
															</a>
															<span class="nickNameInHiddenSpan">${nickName}</span>
														</div>
													</div>
												</div>
											</div>
										</c:if>
									</c:if>
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>
					<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="ShippingGroupContainerService.shippingGroupMapForDisplay"/>
						<dsp:param name="elementName" value="shippingGroup"/>
						<dsp:param name="sortProperties" value="-lastActivity"/>
				       	<dsp:oparam name="output">
							<dsp:getvalueof var="nickName" param="key" />
							<dsp:getvalueof var="softDeleted" param="shippingGroup.softDeleted" vartype="java.lang.boolean"/>
							<dsp:getvalueof var="isBillingAddress" param="shippingGroup.billingAddress" />
								<c:if test="${not isBillingAddress}">
									<c:if test="${nickName ne defaultShippingGroupName and ((currentTab ne 'payment-tab') or (currentTab eq 'payment-tab' and not softDeleted))}">
										<dsp:setvalue param="address" paramvalue="shippingGroup.shippingAddress"/>
						           		<div class="spacer"></div>
										<div class="row">
											<div class="col-md-12">
												<dsp:getvalueof param="address.country" var="country"/>
												<c:if test="${country ne 'US'}">
													<p><fmt:message key="myaccount.billing.purpose" /></p>
												</c:if>
												<p class="dark-blue avenir-heavy"><dsp:valueof param="key" /></p>
												<p class="avenir-heavy"><dsp:valueof param="address.firstName" />&nbsp;<dsp:valueof param="address.lastName" /></p>
												<p><dsp:valueof param="address.address1" />&nbsp;<dsp:valueof param="address.address2" /></p>
												<p><dsp:valueof param="address.city" />,&nbsp;<dsp:valueof param="address.state" />&nbsp;<dsp:valueof param="address.postalCode" /></p>
												<c:if test="${country ne 'US'}">
							                 		 <p><dsp:valueof param="address.country"/></p>
							               	 	</c:if>
												<div class="row">
													<div class="col-md-8">
														<p>
															<span class="blue">
																<a href="javascript:void(0);" class="checkoutEditAddress"><fmt:message key="myaccount.edit" /></a>
																<span class="nickNameInHiddenSpan">${nickName}</span>
															</span>&nbsp;.
															<c:choose>
							                             		<c:when test="${country eq 'US'}">
							                             		<span class="blue">
																	<a class="setDefaultLink setAsDefaultCheckout" href="javascript:void(0);"><fmt:message key="myaccount.set.default"/></a>
																	<span class="nickNameInHiddenSpan">${nickName}</span>
																</span>
								                             	<%-- 	<span class="blue">
																		<a class="setDefaultLink" href="javascript:void(0);" id="" class="" onclick="defaultAddress_checkout('${nickName}'); return false"><fmt:message key="myaccount.set.default" /></a>
																	</span> --%>
							                             		</c:when>
							                             		<c:otherwise>
							                             			<fmt:message key="myaccount.set.default" />
							                             		</c:otherwise>
							                             	</c:choose>
						                             	</p>
						                         	</div>
							                         <div class="col-md-1">
							                           <a href="javascript:void(0)" class="delete-icon checkoutRemoveAddress" title="delete address" data-toggle="modal" data-target="#myAccountDeleteCancelModal">
															 <img src="${TRUImagePath}images/delete-icon.png" alt="delete">
														</a>
														<span class="nickNameInHiddenSpan">${nickName}</span>
							                         </div>
							                     </div>
											</div>
										</div>
									</c:if>
								</c:if>
						</dsp:oparam>
					</dsp:droplet>
				</div>
			</div>
		</div>
		<div class="spacer"></div>
		<div class="row">
			<div class="col-md-12">
				<p class="blue">
					<a href="javascript:void(0);" class=""
						onclick="onAddAddressInShipingOverlay();"><fmt:message
							key="myaccount.add.another.address" /></a>
				</p>
			</div>
		</div>
		<div class="spacer"></div>
	</div>
</dsp:page>