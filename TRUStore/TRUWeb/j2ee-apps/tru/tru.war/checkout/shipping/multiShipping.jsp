<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/com/tru/common/TRUUserSession" />
    <c:set var="isSosVar" value="${cookie.isSOS.value}"/>
    <dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
		<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
		<dsp:droplet name="GetSiteTypeDroplet">
			<dsp:param name="siteId" value="${site}"/>
			<dsp:oparam name="output">
				<dsp:getvalueof var="site" param="site"/>
				<c:set var="site" value ="${site}" scope="request"/>
			</dsp:oparam>
		</dsp:droplet>

<dsp:getvalueof var="isOrderHasChannelHGShippingGroups" bean="ShoppingCart.current.orderHasChannelHGShippingGroups" />
<c:if test="${isOrderHasChannelHGShippingGroups}">
	<dsp:setvalue bean="ShippingGroupFormHandler.initRegistryShippingGroups" value="true" />
</c:if>
<%-- Start Checkout Payment Template template --%>

<div class="container-fluid checkout-template checkout-multiple-shipping" id="co-shipping-tab">
    <div class="">
        <div class="checkout-multiple-shipping-content">
            <div class="row checkout-sticky-top">
                <div class="col-md-8 checkout-left-column">
                	<p class="global-error-display"></p>
					<dsp:include page="/checkout/common/checkoutGlobalError.jsp"/>
					<div class="shipping-default-header">												
						<div class="checkout-page-header inline">
							<fmt:message key="myaccount.orderDetails.shipping"/>
						</div>
						<dsp:getvalueof var="isUserAnonymous" bean="Profile.transient"/>
						<dsp:getvalueof var="signInHidden" bean="TRUUserSession.signInHidden"/>
                        <c:if test="${not signInHidden}">
	                        <c:if test="${isUserAnonymous eq true && (!isSosVar || !(site eq 'sos'))}">
		                        <div class="shipping-default-header-signin pull-right">
		                            <a href="#" data-toggle="modal" data-target="#signInModal" id="signInBtn"><fmt:message key="checkout.shipping.signIn" /></a> <fmt:message key="checkout.for"/> <i><fmt:message key="checkout.faster"/></i> <fmt:message key="checkout.shipping.checkoutHeader"/>
		                        </div>
		                    </c:if>
	                    </c:if>
					</div>
					<dsp:droplet name="Switch">
						<dsp:param bean="ShoppingCart.current.orderEligibleForSingleShipping" name="value" />
						<dsp:oparam name="true">
							<a href="#" class="single-address-link" onclick="applySingleShipping();"><fmt:message key="checkout.ship.to.single.address"/></a>
						</dsp:oparam>
					</dsp:droplet>
					<hr class="horizontal-divider">
					<input type="hidden" value="true" id="isMultiShipping" />
					<dsp:include page="/checkout/shipping/multiShippingForm.jsp"/>  					
					
					<dsp:include page="/checkout/shipping/giftOptionsFragment.jsp" />
					<dsp:include page="/checkout/common/salesTaxEstimation.jsp">
						<dsp:param name="taxPageName" value="multi-shipping-page" />
					</dsp:include>
                </div>
                <div class="modal fade" id="myAccountAddAddressModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
					<div class="modal-dialog modal-lg" id="shippingAddressFormFragment">
						<%-- <dsp:include page="addressAddEditOverlay.jsp" /> --%>
					</div>
						
				</div>
			    <div class="modal fade toStopScroll" id="myAccountDeleteCancelModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="display: none;">
						<div class="modal-dialog modal-lg">
							<div class="modal-content sharp-border">
								<div class="my-account-delete-cancel-overlay">
									<div>
										<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
									</div>
									<header><fmt:message key="myaccount.delete.address"/>?</header>
									<p><fmt:message key="myaccount.sure.remove.address"/></p>
									<div>
									<form id="removeShippingAddress" method="POST" onsubmit="javascript: return removeAddress_checkout();">
										<input type="hidden" id="removeAddressHiddenId" class="removeAddressHiddenId" value="">
										
										<input type="submit" id="removeShippingAddressId" name="removeShippingAddress" value="confirm" style="display: none;">
										
		                                <button onclick="return deleteAddress_checkout();"><fmt:message key="myaccount.confirm"/></button><a data-dismiss="modal"><fmt:message key="myaccount.cancel"/></a>
									</form>
									</div>
								</div>
							</div>
						</div>
				</div>
				<div class="modal fade editCart edit-cart-modal" id="editCartModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="display: none;">
					<div class="modal-lg modal-dialog">
					<div class="modal-content sharp-border">
					</div></div>
				</div>
                <div class="col-md-3 checkout-right-column">
 					<dsp:include page="/checkout/common/checkoutOrderSummary.jsp">
	 					<dsp:param name="pageName" param="pageName" />
	 				</dsp:include>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- /End Checkout Payment Template template --%>
</dsp:page>