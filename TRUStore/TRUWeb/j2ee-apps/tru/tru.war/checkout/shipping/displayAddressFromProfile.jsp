<fmt:setBundle
	basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
           <dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>       
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" bean="Profile.shippingAddress.id" />
				<dsp:oparam name="false">
			<dsp:droplet name="ForEach">
           	<dsp:param name="array" bean="Profile.secondaryAddresses"/>
           	<dsp:param name="elementName" value="address"/>
           	<dsp:oparam name="output">
			<dsp:getvalueof param="address.id" var="addressId" />
			<dsp:getvalueof var="nickName" param="key" />
			<c:if test="${addressId eq shippingAddressId}">
           		<div class="small-spacer"></div>
             <div class="row">
                 <div class="col-md-12">
                     <p><span class="dark-blue avenir-heavy"><dsp:valueof param="key" /></span>&nbsp;.&nbsp;<span class="default-address"><fmt:message key="myaccount.default.address" /></span></p>
                     <p class="avenir-heavy"><dsp:valueof param="address.firstName" />&nbsp;<dsp:valueof param="address.lastName" /></p>
                     <p><dsp:valueof param="address.address1" />&nbsp;<dsp:valueof param="address.address2" /></p>
                     <p><dsp:valueof param="address.city" />,&nbsp;<dsp:valueof param="address.state" />&nbsp;<dsp:valueof param="address.postalCode" /></p>
                     <div class="row">
                         <div class="col-md-8">
                             <p>
                             	<span class="blue">
                             		<a href="javascript:void(0);" class="checkoutEditAddress"><fmt:message key="myaccount.edit" /></a>
                             	</span>
                             	<span class="nickNameInHiddenSpan">${nickName}</span>
                             </p>
                         </div>
                       <div class="col-md-1">
                            <a href="javascript:void(0)" class="delete-icon checkoutRemoveAddress" title="delete address" data-toggle="modal" data-target="#myAccountDeleteCancelModal">
								 <img src="${TRUImagePath}images/delete-icon.png" alt="delete">
							</a>
                            <span class="nickNameInHiddenSpan">${nickName}</span>
                         </div>
                        
                         
                     </div>
                 </div>
             </div>
			 </c:if>
           	</dsp:oparam>
           </dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
           <dsp:droplet name="ForEach">
           	<dsp:param name="array" bean="Profile.secondaryAddresses"/>
           	<dsp:param name="elementName" value="address"/>
           	<dsp:param name="sortProperties" value="-lastActivity"/>
           	<dsp:oparam name="output">
			<dsp:getvalueof param="address.id" var="addressId" />
			<dsp:getvalueof var="nickName" param="key" />
			<c:if test="${addressId ne shippingAddressId}">
           		<div class="spacer"></div>
             <div class="row">
                 <div class="col-md-12">
                 <dsp:getvalueof param="address.country" var="country"/>
                 <c:if test="${country ne 'US'}">
                 	<p><fmt:message key="myaccount.billing.purpose" /></p>
               	 </c:if>
                     <p class="dark-blue avenir-heavy"><dsp:valueof param="key" /></p>
                     <p class="avenir-heavy"><dsp:valueof param="address.firstName" />&nbsp;<dsp:valueof param="address.lastName" /></p>
                     <p><dsp:valueof param="address.address1" />&nbsp;<dsp:valueof param="address.address2" /></p>
                     <p><dsp:valueof param="address.city" />, <dsp:valueof param="address.state" />&nbsp;<dsp:valueof param="address.postalCode" /></p>
                     <c:if test="${country ne 'US'}">
                 		 <p><dsp:valueof param="address.country"/></p>
               	 	</c:if>
                     <div class="row">
                         <div class="col-md-8">
                             <p>
                             	<span class="blue">
                             		<a href="javascript:void(0);" class="checkoutEditAddress"><fmt:message key="myaccount.edit" /></a>
                             		<span class="nickNameInHiddenSpan">${nickName}</span>
                             	</span>&nbsp;.
                             <c:choose>
                             	<c:when test="${country eq 'US'}">
                             		<span class="blue">
										<a class="setDefaultLink" href="javascript:void(0);" id="" class="" onclick="defaultAddress('${nickName}'); return false"><fmt:message key="myaccount.set.default" /></a>
									</span>
                             	</c:when>
                             	<c:otherwise>
                             		<fmt:message key="myaccount.set.default" />
                             	</c:otherwise>
                             </c:choose>
                             </p>
                         </div>
                         <div class="col-md-1">
                            <a href="javascript:void(0)" class="delete-icon checkoutRemoveAddress" title="delete address" data-toggle="modal" data-target="#myAccountDeleteCancelModal">
								 <img src="${TRUImagePath}images/delete-icon.png" alt="delete">
							</a>
                            <span class="nickNameInHiddenSpan">${nickName}</span>
                         </div>
                     </div>
                 </div>
             </div>
			 </c:if>
           	</dsp:oparam>
           </dsp:droplet>

</dsp:page>                        
                        