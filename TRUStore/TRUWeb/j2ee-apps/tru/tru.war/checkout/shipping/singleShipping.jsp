<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/targeting/TargetingFirst" />
	<dsp:importbean bean="/atg/registry/RepositoryTargeters/TRU/PrivacyPolicy/PrivacyPolicyContentTargeter" />
	<dsp:importbean bean="/com/tru/commerce/order/TRUCheckShippingAddressesDroplet" />
	<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
	<dsp:importbean bean="/com/tru/common/TRUUserSession" />
	<c:set var="isSosVar" value="${cookie.isSOS.value}" />
	<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
	<dsp:droplet name="GetSiteTypeDroplet">
		<dsp:param name="siteId" value="${site}" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="site" param="site" />
			<c:set var="site" value="${site}" scope="request" />
		</dsp:oparam>
	</dsp:droplet>

	<div class="checkout-shipping-content" id="co-shipping-tab">
		<div class="row checkout-sticky-top">
			<div class="col-md-8 checkout-left-column">
				<dsp:droplet name="TRUCheckShippingAddressesDroplet">
					<dsp:param name="shippingGroupMapContainer" bean="ShippingGroupContainerService" />
					<dsp:oparam name="empty">
						<div class="checkout-shipping-default">
							<div class="shipping-default-header">
								<div class="checkout-page-header inline">
									<fmt:message key="checkout.shipping.shippingPage" />
								</div>
								<dsp:getvalueof var="isUserAnonymous" bean="Profile.transient" />
								<dsp:getvalueof var="signInHidden" bean="TRUUserSession.signInHidden" />
								<c:if test="${not signInHidden}">
									<c:if test="${isUserAnonymous eq true && (!isSosVar || !(site eq 'sos'))}">
										<div class="shipping-default-header-signin pull-right">
											<span class="signin-link avenir-next-demibold">
												<a href="#" data-toggle="modal" data-target="#signInModal" id="signInBtn">
													<fmt:message key="checkout.shipping.signIn" />
												</a>												
											</span>
											<span class="signin-text">
												<fmt:message key="checkout.for" />&nbsp;<fmt:message key="checkout.faster"/>&nbsp;<fmt:message key="checkout.shipping.checkoutHeader" />												
											</span>
										</div>
									</c:if>
								</c:if>
							</div>
							<dsp:include page="/checkout/common/checkoutGlobalError.jsp" />
							<hr class="horizontal-divider">
							<div class="checkout-shipping-address-form">
								<div class="checkout-shipping-address-form-header">
									<div class="inline address-form-header">
										<h3 class="checkout-page-form-header"><fmt:message key="checkout.shipping.shippingAddress" /></h3>

										<div class="shipping-us-only-text">Shipping is available in the US only</div>
										<p class="required-label"><span class="orange-astericks">*&nbsp;</span>
											<fmt:message key="checkout.shipping.required" />
										</p>
									</div>
									<div class="inline pull-right checkout-privacy-link-align">
										<a href="#" class="" data-toggle="modal" data-target="#privacyPolicyModal">
											<fmt:message key="checkout.shipping.privacyPolicy" />
										</a>
									</div>
								</div>
								<div class="modal fade" id="privacyPolicyModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
									<div class="modal-dialog quick-help-modal">
										<div class="modal-content sharp-border">
											<dsp:droplet name="TargetingFirst">
												<dsp:param name="targeter" bean="PrivacyPolicyContentTargeter" />
												<dsp:oparam name="output">
													<dsp:valueof param="element.data" valueishtml="true" />
												</dsp:oparam>
											</dsp:droplet>
										</div>
									</div>
								</div>
								<dsp:include page="singleShippingForm.jsp" />
							</div>
						</div>
					</dsp:oparam>
					<dsp:oparam name="output">
						<dsp:include page="singleShippingAddresses.jsp">
							<dsp:param name="validShippingGroupsMapForDisplay" param="validShippingGroupsMapForDisplay" />
							<dsp:param name="selectedAddressNickName" param="selectedAddressNickName" />
							<dsp:param name="selectedShippingAddress" param="selectedShippingAddress" />
						</dsp:include>
					</dsp:oparam>
				</dsp:droplet>
				<div class="modal fade" id="myAccountAddAddressModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
					<div class="modal-dialog modal-lg" id="shippingAddressFormFragment">
						<%-- <dsp:include page="addressAddEditOverlay.jsp" /> --%>
					</div>

				</div>
				<div class="modal fade toStopScroll" id="myAccountDeleteCancelModal" tabindex="-1" role="dialog" aria-labelledby="basicModal"
				 aria-hidden="true" style="display: none;">
					<div class="modal-dialog modal-lg">
						<div class="modal-content sharp-border">
							<div class="my-account-delete-cancel-overlay">
								<div>
									<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close" id="closeIt"></span></a>
								</div>
								<header>
									<fmt:message key="myaccount.delete.address" />?</header>
								<p>
									<fmt:message key="myaccount.sure.remove.address" />
								</p>
								<div>
									<form id="removeShippingAddress" method="POST" onsubmit="javascript: return removeAddress_checkout();">
										<input type="hidden" id="removeAddressHiddenId" class="removeAddressHiddenId" value="">

										<input type="submit" id="removeShippingAddressId" name="removeShippingAddress" value="confirm" style="display: none;">

										<button onclick="return deleteAddress_checkout();"><fmt:message key="myaccount.confirm"/></button>
										<a href="javascript:void(0)"
										 data-dismiss="modal">
											<fmt:message key="myaccount.cancel" />
										</a>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 checkout-right-column">
				<dsp:include page="/checkout/common/checkoutOrderSummary.jsp">
					<dsp:param name="pageName" param="pageName" />
				</dsp:include>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 checkout-left-column checkout-gift-container">
				<hr class="horizontal-divider">
				<dsp:include page="giftOptionsFragment.jsp" />
				<dsp:include page="/checkout/common/salesTaxEstimation.jsp">
					<dsp:param name="taxPageName" value="single-shipping-page" />
				</dsp:include>
			</div>
		</div>
	</div>

	<div class="modal fade" id="shipping-page-suggested-address" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog modal-dialog-shipping-page-suggested"></div>
	</div>
</dsp:page>