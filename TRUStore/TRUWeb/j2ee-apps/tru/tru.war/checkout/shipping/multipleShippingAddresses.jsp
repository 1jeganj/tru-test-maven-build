<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
    <dsp:importbean bean="/com/tru/commerce/order/TRUAvailableMultipleShippingAddressesDroplet"/>
    <dsp:importbean bean="/atg/commerce/ShoppingCart" />
    <dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService" />
    <dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler" />
    <dsp:getvalueof var="shippingGroupId" param="cartItemDetailVO.shipGroupId" />
    <dsp:getvalueof var="relationShipId" param="cartItemDetailVO.id" />
    <dsp:getvalueof var="skuId" param="cartItemDetailVO.skuId" />
    <dsp:getvalueof var="uniqueId" param="cartItemDetailVO.uniqueId" />
    <dsp:getvalueof var="wrapCommerceItemId" param="cartItemDetailVO.wrapCommerceItemId" />
    <dsp:getvalueof var="selectedAddressNickName" param="cartItemDetailVO.shippingGroupName" />
    <dsp:droplet name="TRUAvailableMultipleShippingAddressesDroplet">
    	<dsp:param name="shippingGroupMapContainer" bean="ShippingGroupContainerService" />
		<dsp:param name="cartItemDetailVO" param="cartItemDetailVO" />
		<dsp:param name="order" bean="ShoppingCart.current" />		
		<dsp:oparam name="output">
			<dsp:getvalueof var="selectedContainerShippingGroup" param="validShippingGroupsMapForDisplay.${selectedAddressNickName}" />
  			<dsp:getvalueof var="shippingAddress" value="${selectedContainerShippingGroup.shippingAddress}" />
			<div class="avenir-heavy"><fmt:message key="checkout.ship.to"/></div>
			<div class="selectedAddressBlock ${skuId}-${shippingAddress.postalCode}" >
				<div>
					<select class="ship-to-dropdown avenir-next-demibold" name="shipToAddressName" 
						onchange="selectExistingAddress(this, '${shippingGroupId}', '${relationShipId}', '${uniqueId}', '${wrapCommerceItemId}');">
						<%--Start : Logic for If the previously selected address removed from the overlay--%>
						<dsp:getvalueof var="varShipGroupKeyMap" param="shipGroupKeyMap"/>
						<c:if test="${empty selectedAddressNickName || empty varShipGroupKeyMap}">									     			
		     				<option selected disabled><fmt:message key="checkout.shipping.state.select"/></option>
		     			</c:if>
		     			<%--End : Logic for If the previously selected address removed from the overlay--%>
						<dsp:droplet name="IsEmpty">
							<dsp:param name="value" param="shipGroupKeyMap"/>
								<dsp:oparam name="true">
								<%-- <option selected disabled><fmt:message key="checkout.shipping.state.select"/></option> --%>
								<option value=""><fmt:message key="myaccount.add.address"/></option>
								</dsp:oparam>	
								<dsp:oparam name="false">
									<dsp:droplet name="ForEach">
								     <dsp:param name="array" param="shipGroupKeyMap"/>								   
								     <dsp:param name="elementName" value="nickName"/>
									     <dsp:oparam name="output">									     
									     		<dsp:getvalueof var="keyValue" param="key"/>
									     		<dsp:getvalueof var="nickName" param="nickName"/>									     		 
									     		<c:choose>									     			
									     			<c:when test="${keyValue eq selectedAddressNickName}">									     			
									     				<option value="${keyValue}" selected="selected">${nickName}</option>
									     			</c:when>
									     			<c:otherwise>
									     				<option value="${keyValue}">${nickName}</option>
									     			</c:otherwise>
									     		</c:choose>
								    	 </dsp:oparam>
							     	</dsp:droplet>
				     				<option value=""><fmt:message key="myaccount.add.another.address"/></option>
				     			</dsp:oparam>
						</dsp:droplet>
					</select>
					
				</div>
				<div class="spacer"></div>
				<c:if test="${not empty selectedAddressNickName}">
					<c:choose>
						<c:when test="${selectedContainerShippingGroup.registryAddress}">
							<div class="privacy-msg"><span class="avenir-heavy"><fmt:message key="checkout.address.provided.registrant"/></span></div>
							<span class="nickNameInHiddenSpan">${selectedAddressNickName}</span>
						</c:when>
						<c:otherwise>
							<div class="multipleAddress">
								<div class="avenir-heavy">${shippingAddress.firstName}&nbsp;${shippingAddress.lastName}&nbsp;
									<%-- <a href="javascript:void(0);" data-toggle="modal"
										data-target="#myAccountAddAddressModal" 
										onclick='javascript: editShippingAddress("${selectedAddressNickName}");'>edit
									</a> --%>
									
									<a href="javascript:void(0);" data-toggle="modal" class="checkoutEditAddress" data-target="#myAccountAddAddressModal" onclick="javascript:forConsoleErrorFixing(this);">
												<fmt:message key="myaccount.edit" />
									</a>
								<span class="nickNameInHiddenSpan">${selectedAddressNickName}</span>
								</div>
								
								<div class="address-line">${shippingAddress.address1}</div>
								<div class="address-line">${shippingAddress.address2}</div>
								<div class="address-line">${shippingAddress.city}&nbsp;${shippingAddress.state}&nbsp;${shippingAddress.postalCode}</div>
								<%-- <div class="address-line">${shippingAddress.phoneNumber}</div> --%>
								<dsp:getvalueof var="phoneNumberTemp" value="${shippingAddress.phoneNumber}" />
								<c:if test="${fn:length(phoneNumberTemp) gt 0}">
								<p>${fn:substring(phoneNumberTemp, 0,3)}-${fn:substring(phoneNumberTemp, 3,6)}-${fn:substring(phoneNumberTemp, 6, fn:length(phoneNumberTemp))}</p>
								</c:if>
							</div>
						</c:otherwise>
					</c:choose>
				</c:if>
		</div>
		</dsp:oparam>
	</dsp:droplet>
	<div class="spacer spacerMoreHeight"></div>
</dsp:page>