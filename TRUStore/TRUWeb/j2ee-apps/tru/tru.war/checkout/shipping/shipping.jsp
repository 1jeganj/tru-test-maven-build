<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean	bean="/atg/commerce/order/purchase/ShippingGroupDroplet" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler" />
	<%-- <dsp:droplet name="/atg/commerce/order/purchase/RepriceOrderDroplet">
	  <dsp:param value="ORDER_TOTAL" name="pricingOp"/>
	</dsp:droplet> --%>
	<dsp:getvalueof var="currentTab" bean="ShoppingCart.current.currentTab"/>
	<c:if test="${currentTab ne 'shipping-tab'}">
		<dsp:setvalue bean="ShoppingCart.current.previousTab" beanvalue="ShoppingCart.current.currentTab"/>
	</c:if>
	<dsp:setvalue bean="ShoppingCart.current.currentTab" value="shipping-tab"/>
	<dsp:getvalueof var="order" bean="ShoppingCart.current"></dsp:getvalueof>
	<dsp:getvalueof var="isOrderInstorePickup" value="${order.orderIsInStorePickUp}"/>
	<input type="hidden" id="isOrderInstorePickup" value="${isOrderInstorePickup}"/>
	<input type="hidden" id="shipRestrictionsFound" value="false"  />
	
	<dsp:getvalueof var="init" value="true" />
	<dsp:getvalueof var="isTransient" bean="Profile.transient" />
	<dsp:droplet name="Switch">
		<dsp:param bean="ShoppingCart.current.payPalOrder" name="value" />
		<dsp:oparam name="true">
			<dsp:getvalueof var="isTransient" value="true" />
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="ShippingGroupDroplet">
		<dsp:param name="createOneInfoPerUnit" value="false" />
		<dsp:param name="clearShippingInfos" value="true" />
		<%-- <dsp:param name="clearShippingGroups" value="${not isTransient}" /> --%>
		<dsp:param name="shippingGroupTypes" value="hardgoodShippingGroup,channelHardgoodShippingGroup" />
		<dsp:param name="initShippingGroups" value="true" />
		<dsp:param name="initBasedOnOrder" value="false" />
		<dsp:oparam name="output" />
	</dsp:droplet>	
	<%--Start :MVP280 --%>
    <dsp:setvalue bean="ShippingGroupFormHandler.applyShippingGroupRules" value="true" />
    <%--End :MVP280 --%>
       
	<%-- Start PayPal integration changes --%>
	<dsp:getvalueof var="pgName" param="pageName"/>
	<dsp:droplet name="ForEach">
		<dsp:param name="array" bean="ShoppingCart.current.paymentGroups" />
		<dsp:param name="elementName" value="paymentGroup" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="pgType" param="paymentGroup.paymentGroupClassType" />
			<c:choose>
				<c:when test="${(pgType eq 'payPal' and pgName eq 'cartPage')}" >
                    <input type="hidden" id=isOrderPayPalPayment value="true"/>
				</c:when>
			</c:choose>
		</dsp:oparam>
	</dsp:droplet>
   <%-- End PayPal integration changes --%>
<!--Order_ID : <dsp:valueof bean="ShoppingCart.current.id"/>-->
	<input type="hidden" value="Shipping" id="pageName" />
	<dsp:droplet name="Switch">
		<dsp:param value="${order.orderMandateForMultiShipping || order.orderIsInMultiShip}" name="value" />
		<dsp:oparam name="false">
			<!-- For Single Shipping -->
			<dsp:include page="singleShipping.jsp">
				<dsp:param name="pageName" value="Shipping" />
			</dsp:include>
		</dsp:oparam>
		<dsp:oparam name="true">
			<!-- For Multi Shipping -->
			<dsp:include page="multiShipping.jsp">
				<dsp:param name="pageName" value="Shipping" />
			</dsp:include>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:include page="/checkout/common/ad-Zone.jsp">
		<dsp:param name="cssClassName" value="shipping-addzone" />
	</dsp:include>
	<dsp:include page="/checkout/common/signInModal.jsp">
		<dsp:param name="requestPage" value="shipping" />
	</dsp:include>
	<dsp:include page="/myaccount/truForgotPassword.jsp"></dsp:include>
	<dsp:include page="overlayModal.jsp"></dsp:include>
	
	
	<div id="tealiumCheckoutContent">
    
 	</div>
	
	
</dsp:page>