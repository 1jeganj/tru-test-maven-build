<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
    <dsp:importbean bean="/com/tru/commerce/order/TRUAvailableShippingAddressesDroplet"/>
    <dsp:importbean bean="/atg/commerce/ShoppingCart" />
    <dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService" />
    <dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler" />
    <dsp:importbean bean="/com/tru/common/TRUUserSession" />
	<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
    <c:set var="isSosVar" value="${cookie.isSOS.value}"/>
     <dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
	<dsp:droplet name="GetSiteTypeDroplet">
		<dsp:param name="siteId" value="${site}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="site" param="site"/>
			<c:set var="site" value ="${site}" scope="request"/>
		</dsp:oparam>
	</dsp:droplet>
    
    <dsp:droplet name="TRUAvailableShippingAddressesDroplet">
		<dsp:param name="shippingGroupMapContainer" bean="ShippingGroupContainerService" />
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="selectedAddressNickName" param="selectedAddressNickName" />
    		<dsp:getvalueof var="selectedShippingAddress" param="selectedShippingAddress" />
    		
		    <div class="shipping-template">
		    
				<div class="shipping-default-header">
					<div class="checkout-page-header inline">
						<fmt:message key="checkout.header.shippingHeader"/>
					</div>
					  <dsp:getvalueof var="isUserAnonymous" bean="Profile.transient"/>
					  <dsp:getvalueof var="signInHidden" bean="TRUUserSession.signInHidden"/>
                      <c:if test="${not signInHidden}">
			              <c:if test="${isUserAnonymous eq true && (!isSosVar || !(site eq 'sos'))}">
			                  <div class="shipping-default-header-signin pull-right">
			                      <a href="#" data-toggle="modal" data-target="#signInModal" id="signInBtn"><fmt:message key="checkout.shipping.signIn" /></a>&nbsp;<fmt:message key="checkout.for"/> <i><fmt:message key="checkout.faster"/></i>&nbsp;<fmt:message key="checkout.shipping.checkoutHeader"/>
			                  </div>
			              </c:if> 
		              </c:if>
				</div>								
				
				<dsp:include page="/checkout/common/checkoutGlobalError.jsp"/>
				
				<input type="hidden" value="false" id="isMultiShipping" />
				<div class="checkout-shipping-saved-address">
					<div class="checkout-shipping-saved-address-header">
						<div class="saved-address-header">
							<fmt:message key="checkout.shipping.savedAddress"/>
						</div>
					</div>
					<div class="checkout-shipping-address-saved">
						<div class="saved-address-header">
							<fmt:message key="checkout.shipping.selectAddress"/>
						</div>
						<div class="select-wrapper dropdown-saved-address">
							<select id="selectedShippingAddress" name="shipToAddressName" onchange="changeShippingAddress()">
								<%--  <option selected disabled><fmt:message key="checkout.shipping.state.select"/></option> --%>
								<dsp:droplet name="ForEach">
							     <dsp:param name="array" param="validShippingGroupsMapForDisplay"/>
							     <dsp:param name="elementName" value="shippingGroupMap"/>
								     <dsp:oparam name="output">
								     		<dsp:getvalueof var="keyValue" param="key"/>
								     		<dsp:getvalueof var="isBillingAddress" param="shippingGroupMap.billingAddress" />
								     		<c:if test="${not isBillingAddress }">
									     		<c:choose>
									     			<c:when test="${keyValue eq selectedAddressNickName}">
									     				<option value="${keyValue}" selected="selected">${keyValue}</option>
									     			</c:when>
									     			<c:otherwise>
									     				<option value="${keyValue}">${keyValue}</option>
									     			</c:otherwise>
									     		</c:choose>
								     		</c:if>
							    	 </dsp:oparam>
						     	</dsp:droplet>
						     	<option value=""><fmt:message key="myaccount.add.another.address"/></option>
							</select>
							<a href="javascript:void(0);" data-toggle="modal" id="addAnotherAddressModal"
									data-target="#myAccountAddAddressModal" onclick='javascript: onAddAddressInShipingOverlay();forConsoleErrorFixing(this);' style="display: none;"><fmt:message key="myaccount.add.another.address"/></a>
						</div>

						<c:if test="${not empty selectedAddressNickName}">
							<dsp:getvalueof var="order" bean="/atg/commerce/ShoppingCart.current"></dsp:getvalueof>
							<dsp:getvalueof  var="shippingAddress" value="${selectedShippingAddress}" />
							<div class="address-line first-line">
								${shippingAddress.firstName}&nbsp;${shippingAddress.lastName}&nbsp;
								<span class="edit-saved-address">
									<a href="javascript:void(0);" data-toggle="modal" class="checkoutEditAddress tru-link" data-target="#myAccountAddAddressModal" onclick="javascript:forConsoleErrorFixing(this);">
										<fmt:message key="myaccount.edit" />
									</a>
									<span class="nickNameInHiddenSpan">${selectedAddressNickName}</span>
								</span>
							</div>
							<div class="address-line">${shippingAddress.address1}</div>
							<div class="address-line">${shippingAddress.address2}</div>
							<div class="address-line">${shippingAddress.city}&nbsp;${shippingAddress.state}&nbsp;${shippingAddress.postalCode}</div>
							<%-- <div class="address-line">${shippingAddress.phoneNumber}</div> --%>
							<dsp:getvalueof var="phoneNumberTemp" value="${shippingAddress.phoneNumber}" />
							<c:if test="${fn:length(phoneNumberTemp) gt 0}">
							<p>${fn:substring(phoneNumberTemp, 0,3)}-${fn:substring(phoneNumberTemp, 3,6)}-${fn:substring(phoneNumberTemp, 6, fn:length(phoneNumberTemp))}</p>
							</c:if>
						</c:if>
						<%-- <div class="address-line">${shippingAddress.email}</div> --%>
					</div>
					<dsp:droplet name="Switch">
						<dsp:param bean="ShoppingCart.current.orderEligibleForMultiShipping" name="value" />
						<dsp:oparam name="true">
							<div class="ship-to-multiple-addresses">
								<a href="javascript:void(0);" class="tru-link" data-toggle="modal" onclick="onSubmitShippingAddCheckout('', 'moveToMultipleShip');forConsoleErrorFixing(this);">
									<fmt:message key="checkout.shipping.shipToMultiple" />
								</a>
							</div>
						</dsp:oparam>
					</dsp:droplet>
				</div>
			</div>
		</dsp:oparam>
	</dsp:droplet>
	<hr class="horizontal-divider">
	<dsp:include page="singleShippingMethod.jsp" />
</dsp:page>