<dsp:page>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
<dsp:setvalue bean="ShippingGroupFormHandler.saveAddressInOrder" value="true" />
	<dsp:droplet name="/atg/commerce/order/OrderLookup">
		<dsp:param name="orderId" value="o1960001" />
		<dsp:oparam name="output">
			<dsp:droplet name="/atg/dynamo/droplet/ForEach">
				<dsp:param name="array" param="result.shippingGroups" />				
				<dsp:oparam name="output">
					
					<div class="small-spacer"></div>
					<div class="row">
						<div class="col-md-12">
							<p>
								<span class="dark-blue avenir-heavy"><fmt:message key="checkout.Order"/></span>
							</p>
							<p class="avenir-heavy">
					
							<dsp:getvalueof var="shippingGroupId" param="element.id" />
						
							
								<dsp:valueof param="element.shippingAddress.firstName"></dsp:valueof>
								<dsp:valueof param="element.shippingAddress.lastName"></dsp:valueof>
							</p>
							<p>
								<dsp:valueof param="element.shippingAddress.address1" />
								<dsp:valueof param="element.shippingAddress.address2" />
							</p>
							<p>
								<dsp:valueof param="element.shippingAddress.city" />
								,
								<dsp:valueof param="element.shippingAddress.state" />
								<dsp:valueof param="element.shippingAddress.postalCode" />
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<p class="blue">
								<span class="blue">
									<a href="javascript:void(0);" class="checkoutEditAddress"><fmt:message key="myaccount.edit" /></a>
									<span class="nickNameInHiddenSpan">${nickName}</span>
								</span>
							</p>
						</div>
						   <div class="col-md-1">
                            <a href="javascript:void(0)" class="delete-icon checkoutRemoveAddress" title="delete address" data-toggle="modal" data-target="#myAccountDeleteCancelModal">
								 <img src="${TRUImagePath}images/delete-icon.png" alt="delete">
							</a>
                            <span class="nickNameInHiddenSpan">${nickName}</span>
                         </div>
					</div>
				</dsp:oparam>

			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>

</dsp:page>
