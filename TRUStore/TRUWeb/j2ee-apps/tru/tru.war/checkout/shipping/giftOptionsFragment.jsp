<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
 	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:getvalueof var="order" vartype="atg.commerce.Order" bean="ShoppingCart.current"/>
	<dsp:getvalueof var="previousTab" bean="ShoppingCart.current.previousTab" />
	<dsp:getvalueof var="showMeGiftingOptions" bean="ShoppingCart.current.showMeGiftingOptions" />
	<dsp:droplet name="Switch">
		<dsp:param value="${showMeGiftingOptions}" name="value" />
		<dsp:oparam name="true">
			<div class="checkout-shipping-gifting" id="co-sp-gift-options">
				<div class="checkout-shipping-gifting-header"><h3 class="checkout-page-form-header"><fmt:message key="checkout.shipping.gifting" /></h3></div>
					<div class="tru-green-round-checkbox checkout-gift-options">
						<div class="checkout-flow-sprite checkout-flow-radio-unselected checkout-flow-radio-selected" id="gift-options-checkbox"></div>
						<span class="tru-green-round-checkbox-label"><fmt:message key="checkout.shipping.giftingOptions" /></span>
					</div>
			</div>
		</dsp:oparam>
		<dsp:oparam name="false">
	  		<div class="checkout-shipping-gifting" id="co-sp-gift-options">
				<div class="checkout-shipping-gifting-header"><h3 class="checkout-page-form-header"><fmt:message key="checkout.shipping.gifting" /></h3></div>
					<div class="tru-green-round-checkbox checkout-gift-options">
						<div class="checkout-flow-sprite checkout-flow-radio-unselected" id="gift-options-checkbox"></div>
						<span class="tru-green-round-checkbox-label"><fmt:message key="checkout.shipping.giftingOptions" /></span>
					</div>
			</div>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>