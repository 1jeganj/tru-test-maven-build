<fmt:setBundle
	basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/com/tru/commerce/order/droplet/DisplayOrderDetailsDroplet" />
	
	<dsp:droplet name="DisplayOrderDetailsDroplet">
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:param name="profile" bean="Profile" />
		<dsp:param name="page" value="shipping" />
		<dsp:oparam name="output">
			<dsp:droplet name="ForEach">
				<dsp:param name="array" param="shippingDestinationsVO" />
				<dsp:param name="elementName" value="shippingDestinationVO" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="nickName" param="key" />
					<div class="checkout-review-items-shipping-to">
						<dsp:droplet name="ForEach">
							<dsp:param name="array"	param="shippingDestinationVO.shipmentVOMap" />
							<dsp:param name="elementName" value="shipmentVO" />
							<dsp:oparam name="output">
								<dsp:droplet name="ForEach">
									<dsp:param name="array"	param="shipmentVO.cartItemDetailVOs" />
									<dsp:param name="elementName" value="cartItemDetailVO" />
									<dsp:oparam name="output">
										<dsp:include page="displayItemDetail.jsp">
											<dsp:param name="cartItemDetailVOs" param="cartItemDetailVOs" />
											<dsp:param name="shippingDestinationVO" param="shippingDestinationVO" />
											<dsp:param name="shipmentVO" param="shipmentVO" />
											<dsp:param name="pageName" value="Shipping" />
										</dsp:include>
										<hr/>
									</dsp:oparam>
								</dsp:droplet>
							</dsp:oparam>
						</dsp:droplet>
					</div>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	
	<a href="javascript:void(0);" data-toggle="modal" id="addAnotherAddressModal"
							data-target="#myAccountAddAddressModal" onclick='javascript: onAddAddressInShipingOverlay();forConsoleErrorFixing(this);' style="display: none;"><fmt:message key="myaccount.add.another.address"/></a>
	
</dsp:page>