<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<div class="modal fade" id="checkoutReviewProcessingOverlay"
		tabindex="-1" role="dialog" aria-labelledby="basicModal"
		aria-hidden="true">
		<div class="modal-dialog modal-md">
			<div class="modal-content sharp-border">
				<img class="" src="${TRUImagePath}images/processing.gif" alt="Processing...">
				<header class="review-processing-header">&nbsp;<fmt:message key="checkout.review.processOrderOverlayHeading" />&nbsp;</header>
				<p><fmt:message key="checkout.review.processOrderOverlaySubHeading" /></p>
			</div>
		</div>
	</div>
	<div class="modal fade" id="removeItemConfirmationModal" tabindex="-1"
		role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content sharp-border">
				<a href="javascript:void(0)"  data-dismiss="modal">
					<span class="clickable"><img src="${TRUImagePath}images/close.png" alt="close"></span>
				</a>
				<div>
					<h2><fmt:message key="checkout.removeItemHeading" /></h2>
					<a href="javascript:void(0);" class="text-color" onclick="closeRemoveModal();"><fmt:message key="checkout.backToReview" /></a> <a href="javascript:void(0);" class="text-color" onclick="removeCartItemInShipping()"><fmt:message key="checkout.removeItem" /></a>
					<input type="hidden" value="" id="removeItemId">
				</div>
			</div>
		</div>
	</div>
</dsp:page>