<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<div class="select-shipping-method gift-section-height">
		<div class="checkout-page-form-header"><fmt:message key="checkout.shipping.shippingMethod" /></div>
		<div class="select-shipping-method-row" id="select-shipping-method-row-id">
			<dsp:include page="singleShippingMethodFrag.jsp"></dsp:include>
		</div>
	</div>
</dsp:page>