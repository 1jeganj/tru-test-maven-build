<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
	
	<dsp:getvalueof var="order" bean="ShoppingCart.current" />
	<dsp:getvalueof var="isAjaxRequest" param="isAjaxRequest" />
	<dsp:getvalueof var="shippingImageSizeMap" bean="TRUStoreConfiguration.shippingImageSizeMap" />
	<dsp:setvalue param="item" paramvalue="cartItemDetailVO" />
	<dsp:getvalueof var="uniqueId" param="item.uniqueId" />
	<c:if test="${ not empty isAjaxRequest && isAjaxRequest eq true }">
		<dsp:getvalueof var="uniqueId" param="uniqueId" />
		<dsp:setvalue param="cartItemDetailVOMap" value="${order.cartItemDetailVOMap}" />
		<dsp:setvalue param="item" paramvalue="cartItemDetailVOMap.${uniqueId}" />
		<dsp:setvalue param="shipmentVO" value="${order.shipmentVO}" />
	</c:if>	
	<dsp:getvalueof var="pageName" param="pageName" />
	<div class="product-information add-protection-plan multiple-shipping" id="${uniqueId}">
		<div class="product-information-header">
			<div class="product-header-title"><dsp:valueof param="item.displayName"/></div>
			<div class="product-header-price"><dsp:valueof param="item.totalAmount" converter="currency" /></div>
		</div>
		<dsp:droplet name="IsEmpty">
			<dsp:param name="value" param="item.channel" />
			<dsp:oparam name="false">
				<div class="registry inline">
					<div></div>
					<fmt:message key="shoppingcart.added.from" />&nbsp;<dsp:valueof param="item.channel" valueishtml="true"/>
				</div>
			</dsp:oparam>
		</dsp:droplet>
		<div class="shopping-cart-product-content">
			<div class="row row-no-padding">
				<div class="col-md-2">
					<div>
						<dsp:getvalueof var="productImage" param="item.productImage" />
						<c:forEach var="entry" items="${shippingImageSizeMap}">
							<c:if test="${entry.key eq 'height'}">
								<c:set var="imageHeight" value="${entry.value}" />
							</c:if>
							<c:if test="${entry.key eq 'width'}">
								<c:set var="imageWidth" value="${entry.value}" />
							</c:if>
						</c:forEach>
						<dsp:droplet name="IsEmpty">
							<dsp:param name="value" value="${productImage}" />
							<dsp:oparam name="false">
								<dsp:droplet
									name="/com/tru/common/droplet/TRUAkamaiImageDroplet">
									<dsp:param name="imageName" value="${productImage}" />
									<dsp:param name="imageHeight" value="${imageHeight}" />
									<dsp:param name="imageWidth" value="${imageWidth}" />
									<dsp:oparam name="output">
										<dsp:getvalueof var="akamaiImageUrl" param="akamaiUrl" />
									</dsp:oparam>
								</dsp:droplet>
								<img class="shopping-cart-product-image" src="${akamaiImageUrl}" alt="shopping cart product image">
							</dsp:oparam>
							<dsp:oparam name="true">
								<img class="shopping-cart-product-image image-not-available"
									src="${TRUImagePath}images/no-image500.gif"
									alt="Cart Product Image">
							</dsp:oparam>
						</dsp:droplet>
					</div>
				</div>
				<div class="col-md-4 product-cart-description" tabindex="0">
					<dsp:include page="/cart/displayItemDetailsFrag.jsp">
	               		<dsp:param name="item" param="item" />
	               		<dsp:param name="pageName" param="pageName" />
	               	</dsp:include>
					<div>
						<p>
							<dsp:droplet name="IsEmpty">
								<dsp:param name="value" value="${pageName}" />
								<dsp:oparam name="false">
	                               	<c:if test="${pageName eq 'Shipping'}">
	                                    <div class="edit">
	                                    	<%-- edit item in cart --%>
											<dsp:include page="/cart/editItemFrag.jsp">
												<dsp:param name="item" param="item" />
												<dsp:param name="pageName" param="pageName" />
											</dsp:include>
		                                    <span>.</span>
		                                    <dsp:include page="/cart/removeItemFrag.jsp">
												<dsp:param name="item" param="item" />
												<dsp:param name="pageName" param="pageName" />
											</dsp:include>
	                                    </div>
	                                   </c:if>
								</dsp:oparam>
							</dsp:droplet>
						</p>
					</div>
				</div>
				<div class="col-md-5 shipping-multiple-addresses">
					<dsp:include page="multipleShippingAddresses.jsp">
						<dsp:param name="cartItemDetailVO" param="item" />
					</dsp:include>   
					<dsp:include page="multipleShippingMethod.jsp">
						<dsp:param name="shipmentVO" param="shipmentVO"/>
						<dsp:param name="cartItemDetailVO" param="item" />
					</dsp:include>
				</div>
			</div>
			<dsp:include page="/cart/buyersProtectionPlan.jsp">
				<dsp:param name="item" param="item"/>
			</dsp:include>
		</div>
	</div>
</dsp:page>				