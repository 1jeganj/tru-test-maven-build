<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/commerce/pricing/AvailableShippingMethods"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	
	<dsp:getvalueof var="order" bean="ShoppingCart.current" />
	
	<dsp:droplet name="AvailableShippingMethods">
	<dsp:param name="currentOrder" bean="/atg/commerce/ShoppingCart.current"/>
	<dsp:param name="profile" bean="/atg/userprofiling/Profile"/>		
	<dsp:param name="state" param="state"/>
	<dsp:param name="address1" param="address1"/>
	<dsp:param name="city" param="city"/>
	<dsp:param name="isNewAddressForm" param="isNewAddressForm"/>	
	<dsp:param name="shippingGroupMapContainer" bean="ShippingGroupContainerService" />										
		<dsp:oparam name="output">			
			<dsp:getvalueof var="defaultShippingMethod" param="selectedShippingMethod" />			
			<dsp:droplet name="/atg/dynamo/droplet/ForEach">
				<dsp:param name="array" param="availableShippingMethods" />
								
				<dsp:oparam name="output">
					<dsp:getvalueof var="shipMethodCode" param="element.shipMethodCode" />
					<dsp:getvalueof var="shippingPrice" param="element.shippingPrice"/>
					<dsp:getvalueof var="regionCode" param="element.regionCode"/>	
					<dsp:getvalueof var="details" param="element.details"/>
					<div class="select-shipping-method-block <c:if test='${shipMethodCode eq defaultShippingMethod}'>selected</c:if>" tabindex="0">
						<div class="tru-green-round-checkbox">
							<c:choose>
								<c:when test="${shipMethodCode eq defaultShippingMethod}">
									<div class="checkout-flow-sprite checkout-flow-radio-unselected checkout-flow-radio-selected shopping-cart-gift-checkbox-on" tabindex="0"></div>									
								</c:when>						
								<c:otherwise>
									<div class="checkout-flow-sprite checkout-flow-radio-unselected shopping-cart-gift-checkbox-off" tabindex="0"></div>	
								</c:otherwise>
							</c:choose>
						</div>
						<p class="shipping-type-header avenir-next-demibold">
							<dsp:valueof param="element.shipMethodName"/>
						</p>
						<p class="shipping-type-time">
							(<dsp:valueof param="element.delivertTimeLine"/>)
						</p>
						<p class="co-shipping-method-price">
							<c:choose> 
								<c:when test="${(not empty order.priceInfo) and (not empty order.priceInfo.shipping) and (shipMethodCode eq defaultShippingMethod)}">
									<dsp:getvalueof var="shipping" value="${order.priceInfo.shipping}"/>
									<c:choose>
                                		<c:when test="${shipping == 0.0}">
                                			<fmt:message key="shoppingcart.free" />
                                		</c:when>
                                		<c:otherwise>
                                			<dsp:valueof format="currency" value="${shipping}" locale="en_US" converter="currency"/>
                                		</c:otherwise>
                                	</c:choose>
								</c:when>								
								<c:otherwise>
									<dsp:getvalueof var="shipping" param="element.shippingPrice"/>
									<c:choose>
                                		<c:when test="${shipping == 0.0}">
                                			<fmt:message key="shoppingcart.free" />
                                		</c:when>
                                		<c:otherwise>
                                			<dsp:valueof format="currency" value="${shippingPrice}" locale="en_US" converter="currency"/>
                                		</c:otherwise>
                                	</c:choose>
								</c:otherwise>
							</c:choose>
						</p>
						<c:choose>
							<c:when test="${shipMethodCode eq '1DAY'}">
								<dsp:getvalueof var="shipMethodCodePopover" value="one${fn:substring(shipMethodCode,1,fn:length(shipMethodCode))}" />
							</c:when>
							<c:otherwise>
								<dsp:getvalueof var="shipMethodCodePopover" value="${shipMethodCode}" />
							</c:otherwise>
						</c:choose>

						<a href="javascript:void(0)" class="co-shipping-method-details ${shipMethodCodePopover}" ><%-- <dsp:valueof param="element.details"/> --%><fmt:message key="checkout.payment.details"/></a>
						<span class="hidden_shipping_details">${details}</span>
						
						<input type="hidden" class="shipMethodCode" value="${shipMethodCode}">
						<input type="hidden" class="shippingPrice" value="${shippingPrice}">
						<input type="hidden" class="regionCode" value="${regionCode}">
						<%-- <input type="hidden" class="details" value="${details}" id="${shipMethodCode}"> --%>
						<%-- <input type="hidden" class="details" value="<div class='popover shippingDetailToolTip' role='tooltip'><div class='arrow'></div><div class='saved-address-header'>Economy Shipping</div><p>Typically arrives in 6-9 business days.</p><p>Note</p><ul><li>Delivery times for oversized and pre-order items can be longer.</li><li>Allow 2 additional days for the Pay in Store payment option.</li></ul></div>"> --%>
						<%-- <dsp:setvalue param="TESTDetails" value="<div class='popover shippingDetailToolTip' role='tooltip'><div class='arrow'></div><div class='saved-address-header'>Economy Shipping</div><p>Typically arrives in 6-9 business days.</p><p>Note</p><ul><li>Delivery times for oversized and pre-order items can be longer.</li><li>Allow 2 additional days for the Pay in Store payment option.</li></ul></div>"/>
						<span id="${shipMethodCode}" class="hidden_shipping_details ${shipMethodCode}"><dsp:valueof param="TESTDetails" valueishtml="true"></dsp:valueof> </span> --%>								
					</div>
				</dsp:oparam>				
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="error">
			<input type="hidden" class="shipMethodCode" value="">
			<input type="hidden" class="shippingPrice" value="0.0">
			<div class="errorContainerSingleShipping">
				<div class="error-state-exclamation-lg img-responsive col-md-2"></div>
				<div class="errorDisplayShipping">
					<dsp:valueof param="message"></dsp:valueof>
				</div>
			</div>
		</dsp:oparam>				
	</dsp:droplet>		
		
			
</dsp:page>