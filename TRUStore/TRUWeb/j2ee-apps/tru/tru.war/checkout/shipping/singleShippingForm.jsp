<fmt:setBundle
	basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:importbean
		bean="/atg/commerce/order/purchase/ShippingGroupFormHandler" />
	<dsp:importbean bean="/com/tru/commerce/payment/paypal/droplet/TRUPayPalProcessShippingDroplet"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:getvalueof var="commerceItems" bean="ShoppingCart.current.commerceItems"/>
	<dsp:getvalueof var="requestFrom" param="requestFrom"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsNull" />
	<dsp:droplet name="Switch">
		<dsp:param bean="CommitOrderFormHandler.formError" name="value" />
		<dsp:oparam name="true">
			<dsp:droplet name="ErrorMessageForEach">
				<dsp:param name="exceptions"
					bean="CommitOrderFormHandler.formExceptions" />
				<dsp:oparam name="outputStart">
					<br />
				</dsp:oparam>
				<dsp:oparam name="output">
					<div style="color: red"><dsp:valueof param="message" valueishtml="true"/></div>
				</dsp:oparam>
				<dsp:oparam name="outputEnd">
					<dsp:droplet name="/atg/commerce/order/purchase/RepriceOrderDroplet">
						<dsp:param value="ORDER_TOTAL" name="pricingOp" />
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	<%-- Start PayPal integration changes --%>
	<dsp:droplet name="Switch">
		<dsp:param bean="ShoppingCart.current.payPalOrder" name="value" />
		<dsp:oparam name="true">
			<dsp:droplet name="TRUPayPalProcessShippingDroplet">
				<dsp:param bean="ShoppingCart.current" name="Order" />
				<dsp:param bean="ShippingGroupContainerService" name="ShippingGroupMapContainer" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="firstName" param="defaultAddress.firstName"/>
					<dsp:getvalueof var="lastName" param="defaultAddress.lastName"/>
					<dsp:getvalueof var="address1" param="defaultAddress.address1"/>
					<dsp:getvalueof var="address2" param="defaultAddress.address2"/>
					<dsp:getvalueof var="city" param="defaultAddress.city"/>
					<dsp:getvalueof var="stateAddress" param="defaultAddress.state"/>
					<dsp:getvalueof var="postalCode" param="defaultAddress.postalCode"/>
					<dsp:getvalueof var="phoneNumber" param="defaultAddress.phoneNumber"/>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
   <%-- End PayPal integration changes --%>
	
	<div class="checkout-shipping-address-form-content">
		<form method="POST" id="saveShippingAddress" class="JSValidation">
		<p class="global-error-display"></p>
			<div class="row">
				<div class="col-md-6">
					<div class="shipping-address-field tru-float-label-field">
						<div class="shipping-address-field-required orange-astericks">&#42;</div>
						<fmt:message key="checkout.shipping.firstname" var="firstnameLabel" />
						<label class="tru-float-label" for="firstName">${firstnameLabel}</label>
						<input id="fname" class="shipping-address-field-input tru-float-label-input" type="text" maxlength="30"  name="firstName" value="${firstName}" placeholder="${firstnameLabel}" aria-required="true"/>
						<div class="row">
							<div class="col-sm-1 col-sm-push-10">
								<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
							</div>
						</div>
					</div>
					<div class="shipping-address-field adjust-margin tru-float-label-field">
						<div class="shipping-address-field-required orange-astericks">&#42;</div>
						<fmt:message key="checkout.shipping.lastname" var="lastnameLabel" />
						<label class="tru-float-label" for="lastName">${lastnameLabel}</label>
						<input id="lname" class="shipping-address-field-input tru-float-label-input" type="text" name="lastName" maxlength="30"  value="${lastName}" placeholder="${lastnameLabel}" aria-required="true"/>
						<div class="row">
							<div class="col-sm-1 col-sm-push-10">
								<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
							</div>
						</div>					
					</div>
					<div class="shipping-address-field adjust-margin tru-float-label-field">
						<div class="shipping-address-field-required orange-astericks">&#42;</div>
						<fmt:message key="checkout.shipping.address1" var="address1Label" />
						<label class="tru-float-label" for="address1">${address1Label}</label>							
						<input id="address1" class="shipping-address-field-input tru-float-label-input" type="text" name="address1" value="${address1}" onchange="updateShipMethodsZone()" placeholder="${address1Label}" aria-required="true" maxlength="50"/>
						<div class="row">
							<div class="col-sm-1 col-sm-push-10">
								<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
							</div>
						</div>					
					</div>
					<div class="shipping-address-field adjust-margin tru-float-label-field">
						<fmt:message key="checkout.shipping.suite" var="aptsuiteLabel" />
						<label class="tru-float-label" for="address2">${aptsuiteLabel}</label>									
						<input id="aptsuite" class="shipping-address-field-input tru-float-label-input" type="text" value="${address2}" name="address2" maxlength="50" placeholder="${aptsuiteLabel}"/>
						<div class="row">
							<div class="col-sm-1 col-sm-push-10">
								<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
							</div>
						</div>					
					</div>
					<div class="shipping-address-field adjust-margin tru-float-label-field">
						<div class="shipping-address-field-required orange-astericks">&#42;</div>
						<fmt:message key="checkout.shipping.city" var="cityLabel" />
						<label class="tru-float-label" for="city">${cityLabel}</label>								
						<input id="city" class="shipping-address-field-input tru-float-label-input" type="text" name="city" value="${city}" onchange="updateShipMethodsZone()" placeholder="${cityLabel}" aria-required="true" maxlength="30"/>
						<div class="row">
							<div class="col-sm-1 col-sm-push-10">
								<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
							</div>
						</div>					
					</div>
					<div class="shipping-address-field">
						<span class="orange-astericks">&#42;</span>&nbsp;<label class="shipping-address-field-label" for="state"><fmt:message
								key="checkout.shipping.state" /></label>
						<div class="select-wrapper">
							<select name="state" class="shipping-address-field-select" id="state" onchange="updateShipMethodsZone()" aria-required="true">
								<option value=""><fmt:message key="checkout.shipping.state.select"/></option>
								<dsp:droplet name="ForEach">
									<dsp:param name="array" bean="/atg/commerce/util/StateList_US.places" />
									<dsp:param name="elementName" value="state" />
									<dsp:oparam name="output">
										<dsp:getvalueof var="stateCode" param="state.code"/>
										<c:choose>
											<c:when test="${stateCode eq stateAddress}">
												<option value="${stateCode}" selected="selected"><dsp:valueof
														param="state.code" /></option>
											</c:when>
											<c:otherwise>
												<option value="${stateCode}"><dsp:valueof
														param="state.code" /></option>
											</c:otherwise>
										</c:choose>
									</dsp:oparam>
								</dsp:droplet>
								<!-- <option value="other">other</option> -->
							</select>
						</div>
					</div>
					<div class="shipping-address-field adjust-margin zip-field-adjust tru-float-label-field">
						<div class="shipping-address-field-required orange-astericks">&#42;</div>
						<fmt:message key="checkout.shipping.zip" var="zipLabel" />
						<label class="tru-float-label" for="postalCode">${zipLabel}</label>								
						<input id="zip" class="shipping-address-field-input tru-float-label-input" type="text" name="postalCode" value="${postalCode}" maxlength="10" aria-required="true" placeholder="${zipLabel}"/>
						<div class="row">
							<div class="col-sm-1 col-sm-push-10">
								<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
							</div>
						</div>					
					</div>
					<div class="shipping-address-field adjust-margin tru-float-label-field">
						<div class="shipping-address-field-required orange-astericks">&#42;</div>
						<fmt:message key="checkout.shipping.telephone" var="telephoneLabel" />
						<label class="tru-float-label" for="phoneNumber">${telephoneLabel}</label>										
						<input id="telephone" class="shipping-address-field-input tru-float-label-input" type="text" name="phoneNumber" value="${phoneNumber}" class="splCharCheck" placeholder="${telephoneLabel}" onkeypress="return isPhoneNumberFormat(event)" maxlength="20" aria-required="true"/>
						<div class="row">
							<div class="col-sm-1 col-sm-push-10">
								<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
							</div>
						</div>					
					</div>
					<dsp:droplet name="IsNull">
				   		<dsp:param name="value" bean="ShoppingCart.current.billingAddress"/>
				   		<dsp:oparam name="true">
							<div class="same-billing-address">
								<div class="tru-green-round-checkbox">
									<div class="checkout-flow-sprite checkout-flow-radio-unselected checkout-flow-radio-selected" id="same-billing-address-checkbox"></div>
									<span class="tru-green-round-checkbox-label"><fmt:message key="checkout.shipping.samebilling" /></span>
								</div>
							</div>
				   		</dsp:oparam>
				   	</dsp:droplet>
					<dsp:getvalueof var="orderEligibleForMultiShipping" bean="ShoppingCart.current.orderEligibleForMultiShipping"/>
					<c:if test="${orderEligibleForMultiShipping}">
						<div class="ship-to-multiple-addresses">
							<a href="javascript:void(0);" class="tru-link" onclick="onSubmitShippingAddCheckout('', 'moveToMultipleShip');forConsoleErrorFixing(this);">
								<fmt:message key="checkout.shipping.shipToMultiple" />
							</a>
						</div>
					</c:if>
					<input type="hidden" name="country" id="country" value="US" />
					<input name="showMeGiftingOptions" id="showMeGiftingOptions" type="hidden" name="showMeGiftingOptions" value="true" />
					<input id="billingAddressSameAsShippingAddress" type="hidden" name="billingAddressSameAsShippingAddress" value="true" />
					<input id="shippingMethod" type="hidden" name="shippingMethod" value="" />
					<input id="shippingPrice" type="hidden" name="shippingPrice" value="" />
					<input id="isNewAddressForm" type="hidden" name="isNewAddressForm" value="true" />
					
					<input id="addressValidated" name="addressValidated" id="addressValidated" type="hidden" value="false" />
				</div>
			</div>
			<button class="validateForms"><fmt:message key="checkout.place.order.now"/></button>
		</form>
	</div>
	<hr class="horizontal-divider">
	<dsp:include page="singleShippingMethod.jsp" >
		<dsp:param name="isNewAddressForm" value="true"/>
	</dsp:include>	
</dsp:page>