<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
	
	<dsp:getvalueof var="pageName" param="pageName" />
	<dsp:getvalueof var="nickName" param="nickName" />
	<dsp:getvalueof var="selectedShippingMethod" param="shipmentVO.selectedShippingMethod" />
	<dsp:getvalueof var="selectedShippingPrice" param="shipmentVO.selectedShippingPrice" />
	<dsp:getvalueof var="estimateShipWindowMin" param="shipmentVO.estimateShipWindowMin" />
	<dsp:getvalueof var="estimateShipWindowMax" param="shipmentVO.estimateShipWindowMax" />
	<dsp:getvalueof var="shippingGroupId" param="shipmentVO.shippingGroup.id"/>
	<dsp:getvalueof var="order" bean="ShoppingCart.current" />
	
	<%--  Start added for TUW-52196 --%>
	<dsp:getvalueof var="eddEnabled" bean="/atg/multisite/Site.turnOffEDD" />
	<%--   End added for TUW-52196 --%>
	   
	<div class="row extra-information">
		<c:choose>
			<c:when test="${pageName eq 'Review' }">
				<div class="col-md-12">
					<div class="order-ships-in">
					<%--start Fix for TUW-35880  --%>
						<h2 class="ship-method-section">
							<fmt:message key="checkout.review.shipmentLabel" />&nbsp;<dsp:valueof param="count" /> of <dsp:valueof param="size" />
						</h2>
						<%--End Fix for TUW-35880   --%>
						<p class="confirmation">
							<dsp:getvalueof var="streetDate" param="shipmentVO.streetDate" />
							<c:choose>
								<c:when test="${not empty streetDate}">
									<fmt:message key="checkout.presell.shipping.by"/>  &nbsp; <dsp:valueof param="shipmentVO.streetDate" date="M/dd/yy"/>
								</c:when>
								<c:when test="${eddEnabled eq false}">
									<fmt:message key="checkout.expect.receive.items"/> &nbsp;<dsp:valueof param="shipmentVO.estimateMinDate" date="M/dd/yy"/>&nbsp; <fmt:message key="shoppingcart.and"/>&nbsp; <dsp:valueof param="shipmentVO.estimateMaxDate" date="M/dd/yy"/>
								</c:when>
								<c:otherwise>
									<fmt:message key="checkout.arrive.date.on.shipping.method.turnoff"/>
								</c:otherwise>
							</c:choose> &nbsp;&nbsp;&nbsp;
							<a class="confirmation-seeTerm" href="javascript:void(0)" data-original-title="" title=""><fmt:message key="shoppingcart.see.terms"/></a>
						</p>
						<div class="ship-wrapper">
							<form method="POST">
								<select name="selectedIndex" class="ship-to-dropdown changeShippingMethod" data-id='<dsp:valueof param="count" />-of-<dsp:valueof param="size" />' aria-label="Change Shipping Method">
									<c:choose>	
									 	<c:when test="${empty selectedShippingMethod}">
											<option value="0"></option>
										</c:when>	
									</c:choose>
									<dsp:droplet name="ForEach">
										<dsp:param name="array" param="shipmentVO.shipMethodVOs" />
										<dsp:param name="elementName" value="shipMethodVO" />
										<dsp:oparam name="output">
											<dsp:getvalueof var="index" param="index" />
											<dsp:getvalueof var="shipMethodName" param="shipMethodVO.shipMethodName" />
											<dsp:getvalueof var="shipMethodCode" param="shipMethodVO.shipMethodCode" />
												<c:choose>																																								
													<c:when test="${selectedShippingMethod eq shipMethodCode}">
														<option value="${index}" selected="selected">${shipMethodName}</option>
													</c:when>
													<c:otherwise>
														<option value="${index}">${shipMethodName}</option>
													</c:otherwise>
												</c:choose>																																												
										</dsp:oparam>
									</dsp:droplet>
								</select>
								<input type="hidden" name="shipToAddressName" value="${nickName}" />
								<input type="hidden" name="shippingMethod" value="${selectedShippingMethod}" />
								<input type="hidden" name="estimateShipWindowMin" value="${estimateShipWindowMin}" />
								<input type="hidden" name="estimateShipWindowMax" value="${estimateShipWindowMax}" />
								<dsp:droplet name="ForEach">
									<dsp:param name="array" param="shipmentVO.shipMethodVOs" />
									<dsp:param name="elementName" value="shipMethodVO" />
									<dsp:oparam name="outputStart">
										<dsp:getvalueof var="size" param="size" />
										<input type="hidden" name="eligibleShipMethodsCount" value="${size}" />
									</dsp:oparam>
									<dsp:oparam name="output">
										<dsp:getvalueof var="index" param="index" />
										<dsp:getvalueof var="shipMethodName" param="shipMethodVO.shipMethodName" />
										<dsp:getvalueof var="shipMethodCode" param="shipMethodVO.shipMethodCode" />
										<dsp:getvalueof var="shippingPrice" param="shipMethodVO.shippingPrice" />
										<dsp:getvalueof var="regionCode" param="shipMethodVO.regionCode" />
										<input type="hidden" name="shipMethodName_${index}" value="${shipMethodName}" />
										<input type="hidden" name="shippingMethod_${index}" value="${shipMethodCode}" />
										<input type="hidden" name="shippingPrice_${index}" value="${shippingPrice}" />
										<input type="hidden" name="regionCode_${index}" value="${regionCode}" />
									</dsp:oparam>
								</dsp:droplet>
							</form>
						</div>
						<p>
							<span class="show-shipping-price">
								<dsp:getvalueof var="shipping" value="${selectedShippingPrice}"/>
								<c:choose>
									<c:when test="${empty selectedShippingMethod}">
                               			<%--Empty block  --%>
                               		</c:when>
                               		<c:when test="${shipping == 0.0 and not empty selectedShippingMethod}">
                               			<fmt:message key="shoppingcart.free" />
                               		</c:when>
                               		<c:otherwise>
                               			<dsp:valueof format="currency" value="${shipping}" locale="en_US" converter="currency"/>
                               		</c:otherwise>
                               	</c:choose>
							</span>
						</p>
					</div>
							
					<%-- <dsp:droplet name="Switch">
						<dsp:param name="value" param="shipmentVO.shippingGroup.giftReceipt" />
						<dsp:oparam name="true">
							<div class="gift-message">
							MVP 127 :: Start changes
								    <dsp:getvalueof var="giftWrapMessage" param="shipmentVO.shippingGroup.giftWrapMessage" />
		                            <header class="gift-message-checkout-header"><fmt:message key="checkout.gifting.header"/></header><span class="review-gift-message-container">(<span class="chars-left review-gift-message-count-container"><dsp:valueof value="${180 - fn:length(giftWrapMessage)}"></dsp:valueof></span>&nbsp;characters remaining)</span>
		                            MVP 127 :: End changes
		                            <div class="edit gift-edit-message">
	                            <span class="reviewGiftMessage">
		                            	<c:choose>
		                            		<c:when test="${not empty giftWrapMessage}">
		                            			"<dsp:valueof param="shipmentVO.shippingGroup.giftWrapMessage"  valueishtml="true" />"&nbsp;.
		                            		</c:when>
		                            	</c:choose>
		                            </span>
		                            	<a href="javascript:void(0)"><fmt:message key="shoppingcart.edit"/></a>
		                            </div>
		                            <div class="gift-message-area-wrapper display-none" id="${shippingGroupId}-gift-message">
		                                <textarea maxtextlength="180" cols="25" rows="5" class="giftMessageArea" id="${shippingGroupId}_review_giftMessage">${giftWrapMessage}</textarea>
		                                <button type="button" class="gift-save-message" id="${shippingGroupId}_gift-save-message-id"><fmt:message key="checkout.overlay.save"/></button>
		                                <input type="hidden" value="${shippingGroupId}" class="review_giftMessage_shippingGroupId" id="${shippingGroupId}_review_giftMessage_shippingGroupId" />
		                            </div>
		                     </div>
						</dsp:oparam>
					</dsp:droplet> --%>
										
                     
				</div>
			</c:when>
			<c:when test="${pageName eq 'Confirm' }">
				<div class="col-md-12 extra-information print-hrline">
					<div class="shipping-information">
						<span class="shipment-header"><fmt:message key="checkout.review.shipmentLabel" /> &nbsp;<dsp:valueof param="count" /> of <dsp:valueof param="size" /></span><br>
						<p class="confirmation">
							<dsp:getvalueof var="streetDate" param="shipmentVO.streetDate" />
							<c:choose>
								<c:when test="${not empty streetDate}">
									<fmt:message key="checkout.presell.shipping.by"/>&nbsp;<dsp:valueof param="shipmentVO.streetDate" date="M/dd/yy" />
								</c:when>	
								<c:when test="${eddEnabled eq false}">									
									<fmt:message key="checkout.expect.receive.items"/>&nbsp;<dsp:valueof param="shipmentVO.estimateMinDate" date="M/dd/yy"/>&nbsp;<fmt:message key="shoppingcart.and"/>&nbsp;<dsp:valueof param="shipmentVO.estimateMaxDate" date="M/dd/yy"/>
								</c:when>
								<c:otherwise>
									<fmt:message key="checkout.arrive.date.on.shipping.method.turnoff"/>
								</c:otherwise>
							</c:choose> &nbsp;&nbsp;&nbsp;
							<a class="confirmation-seeTerm" href="javascript:void(0)"><fmt:message key="shoppingcart.see.terms"/></a>
							
						</p>
						<div class="btn-group checkout-delivery-method">
							<span class="sort-by-title">
								<dsp:droplet name="ForEach">
									<dsp:param name="array" param="shipmentVO.shipMethodVOs" />
									<dsp:param name="elementName" value="shipMethodVO" />
									<dsp:oparam name="output">
										<dsp:getvalueof var="shipMethodName" param="shipMethodVO.shipMethodName" />
										<dsp:getvalueof var="shipMethodCode" param="shipMethodVO.shipMethodCode" />
										<c:if test="${selectedShippingMethod eq shipMethodCode}">${shipMethodName}</c:if>
									</dsp:oparam>
								</dsp:droplet>
							</span>
						</div>
						<p>
							<span class="show-shipping-price">
								<dsp:getvalueof var="shipping" value="${selectedShippingPrice}"/>
								<c:choose>
                               		<c:when test="${shipping == 0.0}">
                               			<fmt:message key="shoppingcart.free" />
                               		</c:when>
                               		<c:otherwise>
                               			<dsp:valueof format="currency" value="${shipping}" locale="en_US" converter="currency"/>
                               		</c:otherwise>
                               	</c:choose>
							</span>
						</p>
					</div>
					<dsp:droplet name="IsEmpty">
				   		<dsp:param name="value" param="shipmentVO.shippingGroup.giftWrapMessage"/>
				   		<dsp:oparam name="false">
							<div class="gift-information">
								<span><fmt:message
										key="checkout.review.giftMessageHeading" /></span><br>
								<div class="gift-message">"<dsp:valueof param="shipmentVO.shippingGroup.giftWrapMessage" valueishtml="true" />"</div>
							</div>
				   		</dsp:oparam>
				   	</dsp:droplet>
				</div>
			</c:when>
		</c:choose>
		<div class="shippingToolInConfirm" style="display:none;">
			<fmt:message key="confirmation.seeTerm.content.popover" />
		</div>
	</div>
</dsp:page>