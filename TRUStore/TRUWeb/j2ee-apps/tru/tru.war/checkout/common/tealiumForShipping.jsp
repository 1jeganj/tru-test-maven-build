<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration"/>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
	<dsp:importbean bean="/com/tru/commerce/order/droplet/DisplayOrderDetailsDroplet" />
	<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
	<dsp:getvalueof var="siteCode" value="TRU" />
	<dsp:getvalueof var="baseBreadcrumb" value="trubru" />

	<dsp:getvalueof var="loginStatus" bean="Profile.transient" />
	<dsp:getvalueof var="customerEmail" bean="Profile.login"/>
	<dsp:getvalueof var="customerId" bean="Profile.id"/>
	<dsp:getvalueof var="customerDob" bean="Profile.dateOfBirth"/>
	<dsp:getvalueof var="customerFirstName" bean="Profile.firstName"/>
	<dsp:getvalueof var="customerLastName" bean="Profile.lastName"/>

	 <dsp:getvalueof var="checkoutPageName" bean="TRUTealiumConfiguration.checkoutPageName"/>
	 <dsp:getvalueof var="pageType" bean="TRUTealiumConfiguration.checkoutPageType"/>
	 <dsp:getvalueof var="productMerchandisingCategory" bean="TRUTealiumConfiguration.checkoutProductMerchandisingCategory"/>
	 <dsp:getvalueof var="promotionName" bean="TRUTealiumConfiguration.checkoutPromotionName"/>
	 <dsp:getvalueof var="stsRevenue" bean="TRUTealiumConfiguration.checkoutStsRevenue"/>
	 <dsp:getvalueof var="browserId" bean="TRUTealiumConfiguration.browserId"/>
	 <dsp:getvalueof var="deviceType" bean="TRUTealiumConfiguration.deviceType"/>
	 <dsp:getvalueof var="customerType" bean="TRUTealiumConfiguration.customerType"/>
	 <dsp:getvalueof var="internalCampaignPage" bean="TRUTealiumConfiguration.internalCampaignPage"/>
	 <dsp:getvalueof var="orsoCode" bean="TRUTealiumConfiguration.orsoCode"/>
	 <dsp:getvalueof var="ispuSource" bean="TRUTealiumConfiguration.ispuSource"/>
	 <dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>
	 <dsp:getvalueof var="siteCode" value="TRU" />
	<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
	<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
	<dsp:getvalueof var="order" bean="ShoppingCart.current" />

	<dsp:getvalueof var="priceInfo" value="${order.priceInfo}" />
	<dsp:getvalueof var="orderInStorePickUp" value="${order.orderIsInStorePickUp}" />

	<c:choose>
	<c:when test="${orderInStorePickUp eq true}">
	<dsp:getvalueof var="isInStorePickUp" value="Y"/>
	</c:when>
	<c:otherwise>
	<dsp:getvalueof var="isInStorePickUp" value="N"/>
	</c:otherwise>
	</c:choose>
	
	<c:set var="singleQuote" value="'"/>
	<c:set var="doubleQuote" value='"'/>
	<c:set var="slashSingleQuote" value="\'"/>
	<c:set var="slashDoubleQuote" value='\"'/>
	<c:set var="comma" value=","/>
	<c:set var="andSymbol" value="&"/>
	<c:set var="hyphen" value="-"/>
	
	<c:if test="${siteId eq babySiteCode}">
			<dsp:getvalueof var="siteCode" value="BRU" />
			<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
	</c:if>
	<c:set var="space" value=" "/>
	<c:choose>
		<c:when test="${empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value='${fn:substring(customerEmail, 0, fn:indexOf(customerEmail, "@"))}'/>
		</c:when>
		<c:when test="${not empty customerFirstName && empty customerLastName}">
				<c:set var="customerName" value="${customerFirstName}"/>
		</c:when>
		<c:when test="${empty customerFirstName && not empty customerLastName}">
			   <c:set var="customerName" value="${customerLastName}"/>
		</c:when>
		<c:otherwise>
				<c:set var="customerName" value="${customerFirstName}${space}${customerLastName}"/>
		</c:otherwise>
 	</c:choose>
	 <c:choose>
		<c:when test="${loginStatus eq 'true'}">
			<c:set var="customerStatus" value="Guest"/>
		</c:when>
		<c:otherwise>
			<c:set var="customerStatus" value="Registered"/>
			<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" bean="Profile.shippingAddress.id" />
				<dsp:oparam name="false">
					<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="Profile.secondaryAddresses"/>
						<dsp:param name="elementName" value="address"/>
						<dsp:oparam name="output">
							<dsp:getvalueof param="address.id" var="addressId" />
							<c:if test="${addressId eq shippingAddressId}">
								 <dsp:getvalueof param="address.city" var="customerCity"/>
								 <dsp:getvalueof param="address.state" var="customerState"/>
								 <dsp:getvalueof param="address.postalCode" var="customerZip"/>
								 <dsp:getvalueof param="address.country" var="customerCountry"/>
							 </c:if>
						</dsp:oparam>
				   </dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</c:otherwise>
	</c:choose>

	<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
	<c:choose>
	<c:when test="${isSosVar eq 'true'}">
	<c:set var="selectedStore" value="sos"/>
	</c:when>
	<c:otherwise>
	<c:set var="selectedStore" value=""/>
	</c:otherwise>
	</c:choose>

<%--START:  Setting the dynamic values	from order --%>
    <%-- <c:set var="orderType">${order.orderClassType}</c:set> --%>
    <c:set var="orderCurrency">${priceInfo.currencyCode} </c:set>

	<c:set var="orderSubtotal">${priceInfo.rawSubtotal}</c:set>
	<c:choose>
    <c:when test="${not empty orderSubtotal}">
    <c:set var="orderSubtotal"><c:if test="${orderSubtotal.matches('[0-9]+.[0-9]+')}"><fmt:formatNumber type="number" maxFractionDigits="2">${orderSubtotal}</fmt:formatNumber></c:if></c:set>
	</c:when>
	<c:otherwise>
	<c:set var="orderSubtotal" value="0.00"/>
	</c:otherwise>
	</c:choose>

	<c:set var="orderTotal">${priceInfo.total}</c:set>
	<c:choose>
    <c:when test="${not empty orderTotal}">
    <c:set var="orderTotal"><c:if test="${orderTotal.matches('[0-9]+.[0-9]+')}"><fmt:formatNumber type="number" maxFractionDigits="2">${orderTotal}</fmt:formatNumber></c:if></c:set>
	</c:when>
	<c:otherwise>
	<c:set var="orderTotal" value="0.00"/>
	</c:otherwise>
	</c:choose>

	<c:choose>
<c:when test="${orderTotal > '0.00' && orderTotal < '49.00'}">

<c:set var="ordervalue" value="0to49"/>
</c:when>
<c:when test="${orderTotal gt '50.0' && orderTotal lt '99.99'}">
<c:set var="ordervalue" value="50to99"/>
</c:when>
<c:when test="${orderTotal gt '100.0' && orderTotal lt '174.99'}">
<c:set var="ordervalue" value="100to174"/>
</c:when>
<c:when test="${orderTotal gt '175.0' && orderTotal lt '249.99'}">
<c:set var="ordervalue" value="175to249"/>
</c:when>
<c:when test="${orderTotal gt '250.0' && orderTotal lt '349.99'}">
<c:set var="ordervalue" value="250to349"/>
</c:when>
<c:when test="${orderTotal gt '350.0' && orderTotal lt '499.99'}">
<c:set var="ordervalue" value="350to499"/>
</c:when>
<c:when test="${orderTotal gt '500.0' && orderTotal lt '699.0'}">
<c:set var="ordervalue" value="500to699"/>
</c:when>
<c:when test="${orderTotal gt '700.0' && orderTotal lt '899.0'}">
<c:set var="ordervalue" value="700to899"/>
</c:when>
<c:when test="${orderTotal gt '900.0' && orderTotal lt '1099.0'}">
<c:set var="ordervalue" value="900to1099"/>
</c:when>
<c:when test="${orderTotal gt '1100.0'}">
<c:set var="ordervalue" value="1100greater"/>
</c:when>
</c:choose>

<c:set var="orderValueTaxonomy" value="ordervalue=${ordervalue}"/>

    <c:forEach var="coupon" items="${order.singleUseCoupon}">
	<%-- <c:set var="couponCode" value="${couponCode}'${coupon.value}',"/> --%>
	<%-- <c:set var="couponType" value="${couponType}'Target Based Promotions',"/> --%>
	</c:forEach>

	<c:set var="orderShipping">${priceInfo.shipping}</c:set>
	<!-- Commented as per the requirement change-Fix for GEOFUS-1047 -->
	<%-- <c:set var="orderShippingSurcharge">${priceInfo.totalShippingSurcharge}</c:set> --%>
	<c:set var="orderGiftWrap">${priceInfo.giftWrapPrice}</c:set>
	<c:set var="orderID">${order.id}</c:set>
	<c:set var="orderTax">${order.taxPriceInfo.amount}</c:set>
<%--END:  Setting the dynamic values from order	 --%>

<!-- commented tealiumOrderDiscount as part of CR to only populate on order confirmation page -->
    <%-- <c:set var="tealiumOrderDiscount" value="0.00"/> --%>
	<c:forEach var="element" items="${priceInfo.adjustments}">
	<c:if test="${element.adjustmentDescription eq 'Order Discount' }">
	<c:if test="${not empty element.pricingModel.displayname}">
	<c:set var="promotionName" value="${promotionName}${element.pricingModel.displayname},"/>
	</c:if>
	<%-- <c:set var="orderDiscount" value="${element.totalAdjustment}"/>
	<c:set var="tealiumOrderDiscount" value="${tealiumOrderDiscount+(-orderDiscount)}"/> --%>
	</c:if>
	</c:forEach>
<c:set var="tealiumIndex" value="0"/>
<dsp:droplet name="DisplayOrderDetailsDroplet">
	<dsp:param name="order" bean="ShoppingCart.current"/>
	<dsp:param name="profile" bean="Profile" />
	<dsp:param name="page" value="Confirm" />
	<dsp:oparam name="output">
		<dsp:droplet name="ForEach">
			<dsp:param name="array" param="shippingDestinationsVO" />
			<dsp:param name="elementName" value="shippingDestinationVO" />
	
			<dsp:oparam name="output">
			<dsp:getvalueof param="shippingDestinationVO.itemsCount" var="itemCount"/>
				<dsp:getvalueof var="nickName" param="key" />
				<c:set var="orderShippingCountry">USA</c:set>
					<dsp:droplet name="ForEach">
						<dsp:param name="array"	param="shippingDestinationVO.shipmentVOMap" />
						<dsp:param name="elementName" value="shipmentVO" />
						<dsp:oparam name="output">
	
						<dsp:getvalueof var="selectedShippingMethod" param="shipmentVO.shippingGroup.shippingMethod" />
						<dsp:getvalueof param="shipmentVO.shippingGroup" var="shippingGroup"/>
	                                      <c:set var="shippingGroupType" value="${shippingGroup.shippingGroupClassType}"/>
	                                      
						<%-- Added for item shipment method --%>
						<dsp:droplet name="ForEach">
							<dsp:param name="array" param="shipmentVO.shipMethodVOs" />
							<dsp:param name="elementName" value="shipMethodVO" />
							<dsp:oparam name="output">
	
								<dsp:getvalueof var="shipMethodName" param="shipMethodVO.shipMethodName" />
								<dsp:getvalueof var="shipMethodCode" param="shipMethodVO.shipMethodCode" />
								<dsp:getvalueof var="shippingPrice" param="shipMethodVO.shippingPrice"/>
								<dsp:getvalueof param="shipMethodVO.shipMethodName" var="tealiumDefaultShipType"/>
			                    <dsp:getvalueof param="shipMethodVO.delivertTimeLine" var="tealiumDeliveryTimeLine"/>
			                    
			                    <c:choose>
				                    <c:when test="${selectedShippingMethod eq 'GROUND'}">
				                    	<c:set var="economyShipping" value="Economy Shipping Available"/>
				                    </c:when>
				                    <c:otherwise>
				                    	<c:set var="economyShipping" value="Economy Shipping Not Available"/>
				                    </c:otherwise>
				                  </c:choose>
				                  
								<c:if test="${selectedShippingMethod eq shipMethodCode}">
								 <c:choose>
	                             		<c:when test="${not empty shippingMethod}">
	                             			<c:set var="shippingMethod">${shippingMethod},'${shipMethodName}${space}${tealiumDeliveryTimeLine}'</c:set>
	                             		</c:when>
	                             		<c:otherwise>
	                             			<c:set var="shippingMethod">'${shipMethodName}${space}${tealiumDeliveryTimeLine}'</c:set>
	                             		</c:otherwise>
	                             		</c:choose>
	
							    <dsp:getvalueof var="shippingCost" value="${order.priceInfo.shipping}"/>
							    <c:choose>
	                             		<c:when test="${shippingCost == 0.0}">
	                             			<c:set var="tealiumDefaultShippingCost" value="Free"/>
	                             		</c:when>
	                             		<c:otherwise>
	                             			<c:set var="tealiumDefaultShippingCost" value="($ ${shippingPrice})"/>
	                             		</c:otherwise>
	                             		</c:choose>
	                             		 <c:choose>
	                             		<c:when test="${not empty shipping}">
							    <c:set var="shipping">${shipping},'${shipMethodName}(${tealiumDeliveryTimeLine})-${tealiumDefaultShippingCost}'</c:set>
							     </c:when>
							     <c:otherwise>
							     <c:set var="shipping">'${shipMethodName}(${tealiumDeliveryTimeLine})-${tealiumDefaultShippingCost}'</c:set>
							     </c:otherwise>
							     </c:choose>
							     </c:if>
	
							</dsp:oparam>
	
						</dsp:droplet>
						<%-- Added for item shipment method --%>
	
						<%-- Added for item details --%>
						<dsp:getvalueof param="shipmentVO.cartItemDetailVOs" var="items"/>
						<c:forEach var="element" items="${items}">
						 <!-- Removing order_type as per updated requirement and fix for GEOFUS-1035 -->
							<%-- <c:if test="${shippingGroupType eq 'hardgoodShippingGroup'}">
							<c:set var="orderType" value="${orderType}'Ship To Home',"/>
					    </c:if> --%>
					    
						<c:if test="${shippingGroupType eq 'inStorePickupShippingGroup'}">
						<c:set var="shipFromStoreEligible" value="${element.shipFromStoreEligible}"/>
						<c:set var="itemInStorePickUp" value="${element.itemInStorePickUp}"/>
						<c:set var="inStorePickUpAvailable" value="${element.inStorePickUpAvailable}"/>
						<c:set var="ispuRevenue" value="${element.totalAmount+ispuRevenue}"/>
	
						<%-- <c:if test="${shipFromStoreEligible eq 'Y' && itemInStorePickUp eq 'N'}">
	                                           <c:set var="orderType" value="${orderType}'STS',"/>
						</c:if>
						<c:if test="${shipFromStoreEligible eq 'N' && itemInStorePickUp eq 'Y'}">
	                                           <c:set var="orderType" value="${orderType}'ISPU',"/>
						</c:if> --%>
						</c:if>
						<c:choose>
						<c:when test="${tealiumIndex lt itemCount-1}">
						  <c:set var="separator" value=","/>
						</c:when>
						<c:otherwise>
						  <c:set var="separator" value=""/>
						</c:otherwise>
						</c:choose>
						<dsp:droplet name="/com/tru/commerce/droplet/TRUProductInfoLookupDroplet">
								<dsp:param name="productId" value="${element.productId}"/>
								<dsp:param name="siteId" bean="/atg/multisite/Site.id"/>
								<dsp:oparam name="output">
						          <dsp:getvalueof param="productInfo.defaultSKU.relatedProducts" var="relatedProducts"/>
						          <c:set var="orderXsellProduct" value="${orderXsellProduct}'${relatedProducts}'${separator}"/>
					          </dsp:oparam>
						   </dsp:droplet>
							
							<c:set var="tealiumIndex" value="${tealiumIndex+1}"/>
							
							<dsp:droplet name="/com/tru/commerce/droplet/TRUProductCategoryInfoDroplet">
						 	<dsp:param name="productId" value="${element.productId}" />
						 	<dsp:param name="selectedSkuId" value="${element.skuId}" />	
							<dsp:oparam name="output">
								<dsp:getvalueof param="onlinePidSku" var="onlinePID"/>
								<dsp:getvalueof param="productCatName" var="productCatName" />
								<dsp:getvalueof var="productSubcategoryName" param="productSubcategoryName"/>
							</dsp:oparam>
						 </dsp:droplet>
					   <c:set var="productID" value="${productID}'${onlinePID}'${separator}"/>
					   
					   <c:if test="${fn:contains(productCatName,hiphen)}">
					     <c:set var="productCatName" value='${fn:replace(productCatName,hyphen,spaceTrimer)}'/>
						 </c:if>
						 <c:if test="${fn:contains(productCatName,singleQuote)}">
					     <c:set var="productCatName" value='${fn:replace(productCatName,singleQuote,spaceTrimer)}'/>
						 </c:if>
						 <c:if test="${fn:contains(productCatName,doubleQuote)}">
					     <c:set var="productCatName" value='${fn:replace(productCatName,doubleQuote,spaceTrimer)}'/>
						 </c:if>
					
						 <c:if test="${fn:contains(productCatName,space)}">
					     <c:set var="productCatName" value='${fn:replace(productCatName,space,spaceTrimer)}'/>
						 </c:if>
						 <c:if test="${fn:contains(productCatName,comma)}">
					     <c:set var="productCatName" value='${fn:replace(productCatName,comma,spaceTrimer)}'/>
						 </c:if>
						 <c:if test="${fn:contains(productCatName,andSymbol)}">
					     <c:set var="productCatName" value='${fn:replace(productCatName,andSymbol,spaceTrimer)}'/>
						 </c:if>
						 <c:set var="taxproductCatName" value="${productCatName}"/> 
						<c:if test="${fn:contains(productSubcategoryName,hiphen)}">
					     <c:set var="productSubcategoryName" value='${fn:replace(productSubcategoryName,hyphen,spaceTrimer)}'/>
						 </c:if>
						 
					     <c:if test="${fn:contains(productSubcategoryName,singleQuote)}">
					     <c:set var="productSubcategoryName" value='${fn:replace(productSubcategoryName,singleQuote,spaceTrimer)}'/>
						 </c:if>
						 <c:if test="${fn:contains(productSubcategoryName,doubleQuote)}">
					     <c:set var="productSubcategoryName" value='${fn:replace(productSubcategoryName,doubleQuote,spaceTrimer)}'/>
						 </c:if>
						 <c:if test="${fn:contains(productSubcategoryName,space)}">
					     <c:set var="productSubcategoryName" value='${fn:replace(productSubcategoryName,space,spaceTrimer)}'/>
						 </c:if>
						 <c:if test="${fn:contains(productSubcategoryName,comma)}">
					     <c:set var="productSubcategoryName" value='${fn:replace(productSubcategoryName,comma,spaceTrimer)}'/>
						 </c:if>
						<c:if test="${fn:contains(productSubcategoryName,andSymbol)}">
					     <c:set var="productSubcategoryName" value='${fn:replace(productSubcategoryName,andSymbol,spaceTrimer)}'/>
						 </c:if>
						 <c:set var="taxproductSubCatName" value="${productSubcategoryName}"/>
						
						<c:set var="productCatTaxonomy" value="${productCatTaxonomy}&productcat=${taxproductCatName}"/>
						<c:set var="productSubCatTaxonomy" value="${productSubCatTaxonomy}&productsub=${taxproductSubCatName}"/>
							
						</c:forEach>
	
						<%-- Added for item details --%>
	
						</dsp:oparam>
					</dsp:droplet>
	
			</dsp:oparam>
		</dsp:droplet>
	</dsp:oparam>
</dsp:droplet>



<dsp:droplet name="/com/tru/commerce/locations/TRUGetCookieDroplet">
	<dsp:param name="cookieName" value="favStore" />
	<dsp:oparam name="output">
		<dsp:getvalueof var="cookieValue" param="cookieValue" />
		<dsp:getvalueof var="locationIdFromCookie" param="locationId" />
	</dsp:oparam>
	<dsp:oparam name="empty">
	</dsp:oparam>
</dsp:droplet>

<c:set var="session_id" value="${cookie.sessionID.value}"/>
<c:set var="visitor_id" value="${cookie.visitorID.value}"/>

<dsp:droplet name="/atg/dynamo/droplet/ForEach">
  <dsp:param name="array" value="${order.commerceItems}" />
  <dsp:oparam name="output">
   <dsp:tomap var="commerceItem" param="element"/>
     <dsp:droplet name="/atg/dynamo/droplet/ForEach">
      <dsp:param name="array" value="${commerceItem.priceInfo.adjustments}" />
      <dsp:oparam name="output">
        <dsp:tomap var="itemAdjustment" param="element"/>
        	<%-- <dsp:droplet name="IsNull">
        	 <dsp:param name="value" value="${itemAdjustment.pricingModel}" />
        	  <dsp:oparam name="false">
        	<dsp:getvalueof var="promotionName" value="${promotionName}${fn:escapeXml(itemAdjustment.pricingModel.displayName)},"/>
        	<c:set var="orderCouponType" value="${orderCouponType}'Item Level',"/>
        	</dsp:oparam>
        </dsp:droplet> --%>
        </dsp:oparam>
       </dsp:droplet>
      </dsp:oparam>
  </dsp:droplet>
  
<dsp:droplet name="/atg/dynamo/droplet/ForEach">
   <dsp:param name="array" value="${order.priceInfo.adjustments}" />
   <dsp:oparam name="output">
     <dsp:tomap var="orderAdjustment" param="element"/>
     	<%-- <dsp:droplet name="IsNull">
        	 <dsp:param name="value" value="${orderAdjustment.pricingModel}" />
        	  <dsp:oparam name="false">
     			<dsp:getvalueof var="promotionName" value="${promotionName}${fn:escapeXml(orderAdjustment.pricingModel.displayName)},"/>
     			<c:set var="orderCouponType" value="${orderCouponType}'Order Level',"/>
     		  </dsp:oparam>
     	</dsp:droplet> --%>
   </dsp:oparam>
 </dsp:droplet>
 
 <c:forEach items="${order.shippingGroups}" var="shippingGroup">
     <%-- Iterate adjustments in each shipping group --%>
     <dsp:droplet name="/atg/dynamo/droplet/ForEach">
       <dsp:param name="array" value="${shippingGroup.priceInfo.adjustments}"/>
       <dsp:oparam name="output">
         <dsp:tomap var="shippingAdjustment" param="element"/>
         	<%-- <dsp:droplet name="IsNull">
        	 <dsp:param name="value" value="${shippingAdjustment.pricingModel}" />
        	  <dsp:oparam name="false">
         		<dsp:getvalueof var="promotionName" value="${promotionName}${fn:escapeXml(shippingAdjustment.pricingModel.displayName)},"/>
         		<c:set var="orderCouponType" value="${orderCouponType}'Shipping Level',"/> 
         	  </dsp:oparam>
         	</dsp:droplet>  --%>
         	<c:if test="${shippingGroup.shippingGroupClassType eq 'hardgoodShippingGroup'}">
         	  <c:if test="${shippingAdjustment.adjustmentDescription eq 'Shipping Discount'}">
         	  	<c:set var="shippingDiscount" value="${shippingAdjustment.totalAdjustment}"/>
				<c:set var="orderShippingDiscount" value="${orderShippingDiscount+(-shippingDiscount)}"/>
         	  </c:if>
         	</c:if>
       </dsp:oparam>
     </dsp:droplet>
   </c:forEach>
   
	 <c:forEach var="paymentGroup" items="${order.paymentGroups}">
	   <c:set var="taxSelectedPaymentGroupType" value="&paymethod=${taxSelectedPaymentGroupType}${paymentGroup.paymentMethod}"/>
	   <c:set var="tealiumPaymentMethod" value="${paymentGroup.paymentMethod}"/>
	  <c:choose>
	   <c:when test="${tealiumPaymentMethod eq 'creditCard'}">
	   <c:set var="creditCardType" value="${creditCardType}${paymentGroup.creditCardType}"/>
	   <dsp:getvalueof var="cardType" bean="TRUTealiumConfiguration.displayCreditCardTypeName.${creditCardType}"/>
	   <c:set var="selectedPaymentGroupType" value="${selectedPaymentGroupType}'${cardType}',"/>
	   </c:when>
	  <c:otherwise>
		<c:choose>
		<c:when test="${paymentGroup.paymentMethod eq 'giftCard'}">
				<dsp:getvalueof var="paymentGroupTypeMethod" value="${paymentGroup.paymentMethod}"/>
				<dsp:getvalueof var="giftCardMethod" bean="TRUTealiumConfiguration.displayCreditCardTypeName.${paymentGroupTypeMethod}"/>
				<c:set var="selectedPaymentGroupType" value="${selectedPaymentGroupType}'${giftCardMethod}'," />
			</c:when>
			<c:when test="${paymentGroup.paymentMethod eq 'inStorePayment'}">
				<dsp:getvalueof var="paymentGroupTypeMethod" value="${paymentGroup.paymentMethod}"/>
				<dsp:getvalueof var="payInStoreMethod" bean="TRUTealiumConfiguration.displayCreditCardTypeName.${paymentGroupTypeMethod}"/>
				<c:set var="selectedPaymentGroupType" value="${selectedPaymentGroupType}'${payInStoreMethod}'," />
			</c:when>
			<c:otherwise>
				<c:set var="selectedPaymentGroupType" value="${selectedPaymentGroupType}'${paymentGroup.paymentMethod}'," />
			</c:otherwise>
		</c:choose>
		</c:otherwise>
	  </c:choose>
     </c:forEach>
   
   <c:if test="${empty creditCardType}">
			   <c:set var="creditCardType" value="null"/>
		</c:if>
   
   <c:if test="${empty productCatTaxonomy}">
			   <c:set var="productCatTaxonomy" value="null"/>
		</c:if>
   
   <c:if test="${empty productSubCatTaxonomy}">
			   <c:set var="productSubCatTaxonomy" value="null"/>
		</c:if>
		
		<c:if test="${empty taxSelectedPaymentGroupType}">
			   <c:set var="taxSelectedPaymentGroupType" value="&paymethod=null"/>
		</c:if>
   
   <c:set var="orderTaxonomy" value="${orderValueTaxonomy}&creditcard=${creditCardType}${productCatTaxonomy}${taxSelectedPaymentGroupType}${productSubCatTaxonomy}"/>
<c:set var="orderTaxonomy" value="${fn:toLowerCase(orderTaxonomy)}"/>

	<script type='text/javascript'>
	
	
				  var utag_data = {
				    customer_city : "${customerCity}",
				    customer_country : "${customerCountry}",
				    customer_email : "${customerEmail}",
				    customer_id : "${customerId}",
				    event_type:"",
				    customer_type : "${customerStatus}",
				    customer_state : "${customerState}",
				    customer_zip : "${customerZip}",
				    customer_dob : "${customerDob}",
				    customer_status : "${customerStatus}",
				    customer_name: "${customerName}",
				    order_total:"${orderTotal}",
			    	order_id:"${orderID}",
			    	order_payment_type:"${selectedPaymentGroupType}",
			    	order_ispu:"${isInStorePickUp}",
			    	order_payment_amount:"${orderTotal}",
			    	order_shipping:"${orderShipping}",
			    	order_shipping_discount : "${orderShippingDiscount}",
			    	shipping_discount : "${shippingDiscount}",
			    	order_shipping_country:"${tShippingCountry}",
			    	order_default_shipping_type:[${shippingMethod}],
			    	order_subtotal:"${orderSubtotal}",
			    	order_tax:"${orderTax}",
			    	order_store:"${orderStore}",
			    	order_currency:"${orderCurrency}",
				    page_name : "${checkoutPageName}",
				    page_type : "${pageType}",
				    product_id : [${productID}],
			    	product_store : ["${siteCode}"],
			    	event_type:"checkout",
			    	browser_id : "${pageContext.session.id}",
			    	internal_campaign_page : "${internalCampaignPage}",
			    	orso_code : "${ orsoCode}",
				    product_merchandising_category : ["${siteCode}: ${productCatName}"],
				    sts_revenue :"${stsRevenue}",
				    ispu_source :"${ispuSource}",
				    device_type :"${deviceType}",
				    partner_name :"${partnerName}",
				   	xsell_product : [${orderXsellProduct}],
			    	store_locator : "${locationIdFromCookie}",
			    	session_id : "${session_id}",
					visitor_id : "${visitor_id}",
					oas_breadcrumb : "${baseBreadcrumb}/checkout",
					oas_taxonomy : "${orderTaxonomy}",
					oas_sizes : [['Position1', [728,90]]],
					tru_or_bru : "${siteCode}",
					economy_shipping:"${economyShipping}"
			};
			</script>
			<%-- <c:if test="${tealiumOrderDiscount ne '0.00'}">
	    <script>utag_data.order_discount="${tealiumOrderDiscount}";</script>
	    </c:if> --%>
	     <c:if test="${not empty orderGiftWrap}">
	    <script>utag_data.order_gift_wrap="${orderGiftWrap}";</script>
	    </c:if>
	    <c:if test="${not empty ispuRevenue}">
	    <script>utag_data.ispu_revenue="${ispuRevenue}";</script>
	    </c:if>
			<%-- Start: Script for Tealium Integration --%>
		   <script type="text/javascript">
			    (function(a,b,c,d){
			    a='${tealiumURL}';
			    b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
			    a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
			    })();
			</script> 
</dsp:page>