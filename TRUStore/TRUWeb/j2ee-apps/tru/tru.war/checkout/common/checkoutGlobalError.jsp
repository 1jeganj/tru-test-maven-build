<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler" />


<dsp:getvalueof var="financingFailed" param="financingFailed"/>
<dsp:getvalueof var="shipRestrictionsFound" bean="ShoppingCart.current.shipRestrictionsFound"/>
<dsp:getvalueof var="validationFailed" bean="ShoppingCart.current.validationFailed"/>
<dsp:droplet name="Switch">
  <dsp:param name="value" value="${validationFailed}"/>   
  <%--Start :MVP 8  --%>
   	<dsp:oparam name="true">
	   	<dsp:include page="/cart/shoppingCartInfo.jsp">
		<dsp:param name="items" param="outOfStockItemsList" />
		<dsp:param name="removedInfoMessage" param="removedInfoMessage" />
		<dsp:param name="qtyAdjustedInfoMessage" param="qtyAdjustedInfoMessage" />
		<dsp:param name="pageName" param="pageName" />
		<dsp:param name="validationFailed" value="${validationFailed}" />		
	</dsp:include>
	<%--End :MVP 8  --%>
   </dsp:oparam>
   <dsp:oparam name="default">
	   	<div class="checkout-error-state">
		<div class="checkout-error-state-header row">
			<div class="checkout-error-state-exclamation-lg img-responsive col-md-2"></div>
			<div class="checkout-error-state-header-text col-md-10">
				<fmt:message key="shoppingcart.oops.issue" />
				<div class="checkout-error-state-subheader shipRestriction">
					<fmt:message key="checkout.zipcode.restriction.message" />
				<%--	Start : Place holder for displaying dynamic error messages	 --%>						
						<div class="checkout-dynamic-error"></div>
				<%--	End : Place holder for displaying dynamic error messages --%>
					<div><fmt:message key="checkout.common.click"/> <a href="/cart/shoppingCart.jsp"><fmt:message key="checkout.common.here"/></a> <fmt:message key="checkout.return.to.cart" /></div>
				</div>
				<div class="checkout-error-state-subheader noRestriction">
					<%--Start : Place holder for displaying dynamic error messages	 --%>						
						<div class="checkout-dynamic-error"></div>
					<%--End : Place holder for displaying dynamic error messages --%>
				</div>
			</div>
		</div>
		</div>
   </dsp:oparam>

</dsp:droplet>
<dsp:droplet name="Switch">
<dsp:param name="value" value="${financingFailed}"/>
<dsp:oparam name="true">
<dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	<span class="reviewPageSynchronyError"> Following are the form errors: tru_informational_error
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <dsp:valueof param="message"/>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	     </dsp:droplet>
</dsp:oparam>
</dsp:droplet>
		<%-- <div class="checkout-error-state">
			<div class="checkout-error-state-header row">
				<div class="checkout-error-state-exclamation-lg img-responsive col-md-2"></div>
				<div class="checkout-error-state-header-text col-md-10">
					<fmt:message key="shoppingcart.oops.issue" />
					<div class="checkout-error-state-subheader shipRestriction">
						Due to local ordinances,We are unable to complete current order. Please remove the restricted item(s) from your cart to proceed with checkout process.
						Start : Place holder for displaying dynamic error messages							
							<div class="checkout-dynamic-error"></div>
						End : Place holder for displaying dynamic error messages
						<div>Click <a href="/cart/shoppingCart.jsp">here</a> to return to your cart.</div>
					</div>
					<div class="checkout-error-state-subheader noRestriction">
						Start : Place holder for displaying dynamic error messages							
							<div class="checkout-dynamic-error"></div>
						End : Place holder for displaying dynamic error messages
					</div>
				</div>
			</div>
		</div> --%>
</dsp:page>