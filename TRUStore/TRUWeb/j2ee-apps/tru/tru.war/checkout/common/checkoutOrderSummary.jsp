<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/atg/multisite/Site" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:getvalueof var="enableMasterAndVisaIcons" bean="Site.enableMasterAndVisaIcons"/>

	<div class="shopping-cart-summary-block" id="order-summary-block">
		<div class="summary-block-border">
			<dsp:include page="priceSummary.jsp"/>
		</div>
	</div>
</dsp:page>