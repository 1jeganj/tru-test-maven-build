<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/com/tru/commerce/droplet/FetchStoreDetailsDroplet" />
	
	<dsp:getvalueof var="inStorePickupShippingGroup" param="inStorePickupShippingGroup"/>
	<dsp:getvalueof var="itemsCount" param="itemsCount"/>
	<dsp:droplet name="FetchStoreDetailsDroplet">
		<dsp:param name="itemDescriptor" value="store" />
		<dsp:param name="id" value="${inStorePickupShippingGroup.locationId}" />
		<dsp:param name="elementName" value="location" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="commerceItemCount" param="inStorePickupShippingGroup.commerceItemRelationshipCount" />
			<div>
			    <header> ${itemsCount}
				    <c:choose>
				    	<c:when test="${itemsCount gt 1}">
				    		&#32;<fmt:message key="checkout.items.pickup"/>&#32;
				    	</c:when>
				    	<c:otherwise>
				    		&#32;<fmt:message key="checkout.item.pickup"/>&#32;
				    	</c:otherwise>
				    </c:choose> 
				    <fmt:message key="checkout.free.store.pickup"/>
			    </header>
			    <dsp:getvalueof var="storeName" param="location.name" />
			   	<c:set var="string2" value="${fn:split(storeName, '[')}" />
			   
				    <p><fmt:message key="myaccount.orderDetails.at"/> <span class="free-store-pickup">${string2[0]}</span><span>&#xb7;</span><span class="free-store-pickup"><dsp:valueof param="location.address1"/>&#32;<dsp:valueof param="location.address2"/>&#32;-&#32;<dsp:valueof param="location.city"/>,&#32;<dsp:valueof param="location.stateAddress"/>&#32;<dsp:valueof param="location.postalCode"/></span>
			    </p>
			    <p><fmt:message key="checkout.review.pickedUpBy"/>&#32;
			    <dsp:getvalueof param="inStorePickupShippingGroup.shipToName" var="shipToName"></dsp:getvalueof>
			    	 <c:choose>
			    	 	<c:when test="${shipToName eq null }">
			    	 		<dsp:valueof param="inStorePickupShippingGroup.firstName"/> &#32;<dsp:valueof param="inStorePickupShippingGroup.lastName"/> 
			    	 	</c:when>
			    	 	<c:otherwise>
			    	 		<dsp:valueof param="inStorePickupShippingGroup.shipToName"/>
			    	 	</c:otherwise>
			    	 </c:choose>
			    	
				    <c:if test="${not empty inStorePickupShippingGroup.altFirstName}">
				    	&#32;<fmt:message key="myaccount.orderDetails.andOr"/>&#32;<dsp:valueof param="inStorePickupShippingGroup.altFirstName"/>&#32;<dsp:valueof param="inStorePickupShippingGroup.altLastName"/>
				    </c:if>
			    </p>
			</div>
		</dsp:oparam>
	</dsp:droplet>

</dsp:page>

