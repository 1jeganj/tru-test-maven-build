<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:getvalueof var="creditCartTypeMap" bean="TRUConfiguration.displayCreditCardTypeName" />
	<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUOrderRemainingAmountDroplet" />
	<dsp:getvalueof var="pgName" param="pageName" />
	<dsp:getvalueof var="order" bean="ShoppingCart.current" />
	<dsp:droplet name="TRUOrderRemainingAmountDroplet">
		<dsp:param bean="ShoppingCart.current" name="order" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="finalRemaniningAmount" param="finalRemaniningAmount" />
		</dsp:oparam>
	</dsp:droplet>
	<c:choose>
		<c:when test="${pgName eq 'Review'}">
			<dsp:getvalueof bean="Profile.rewardNo" var="rewardNumber"></dsp:getvalueof>
			<dsp:getvalueof var="paymentGroupBillingAddress" bean="ShoppingCart.current.billingAddress" />
		</c:when>
		<c:when test="${pgName eq 'Confirm'}">
			<dsp:getvalueof bean="ShoppingCart.last.rewardNumber" var="rewardNumber"></dsp:getvalueof>
			<dsp:getvalueof var="paymentGroupBillingAddress" bean="ShoppingCart.last.billingAddress" />
		</c:when>
	</c:choose>

	<dsp:getvalueof var="paymentGrouPayInStore" value="" />
	<div class="row payment-method-section">
		<div class="row col-md-6 payment-method">
			<dsp:droplet name="ForEach">
				<dsp:param name="array" param="paymentGroups" />
				<dsp:param name="elementName" value="paymentGroup" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="pgType" param="paymentGroup.paymentGroupClassType" />
					<dsp:getvalueof var="indexVal" param="index" />

					<%-- Added for tealium --%>
						<dsp:getvalueof var="orderPaymentAmount" param="paymentGroup.amount" />
						<%-- <dsp:getvalueof var="tOrderPaymentAmount" param="paymentGroup.amountAuthorized"/> --%>
							<c:set var="tOrderPaymentType" value="${pgType}" scope="request" />
							<c:set var="tOrderPaymentAmount" value="${orderPaymentAmount}" scope="request" />
							<%-- Added for tealium --%>

								<c:choose>
									<c:when test="${pgType eq 'creditCard'}">

										<div id="order_review_creditCard">
											<span>
                           		<c:if test="${indexVal eq 0}">
									<header>
										<fmt:message key="checkout.review.paymentMethod" />
										<c:if test="${pgName eq 'Review'}">
											<span>.</span>
											<!-- <a href="#" data-toggle="modal" data-target="#myAccountAddCreditCardModal" onclick="onEditCreditCardOverlay_checkout('test1')"> -->
											<a href="javascript:void(0)" onclick="continueToCheckout('payment-tab');">
												<fmt:message key="checkout.review.edit" />
											</a>
											</c:if>
											</header>
											</c:if>
											</span>
											<p>
												<!-- Master Card -->
												<dsp:getvalueof var="creditCardType" param="paymentGroup.creditCardType" />

												<dsp:valueof bean="TRUConfiguration.displayCreditCardTypeName.${creditCardType}" />
											</p>
											<p class="creditcard-format">
												<dsp:valueof converter="creditcard" param="paymentGroup.creditCardNumber" />
											</p>
										</div>
										<input type="hidden" name="cardType" id="cardType" value="${creditCardType}" />
										<dsp:getvalueof var="creditCardNumber" param="paymentGroup.creditCardNumber" />
										<dsp:getvalueof var="expirationMonth" param="paymentGroup.expirationMonth" />
										<dsp:getvalueof var="expirationYear" param="paymentGroup.expirationYear" />
										<input type="hidden" name="expirationMonth" id="expirationMonth" value="${expirationMonth}" />
										<input type="hidden" name="creditCardNumber" id="creditCardNumber" value="${creditCardNumber}" />
										<input type="hidden" name="expirationYear" id="expirationYear" value="${expirationYear}" />
										<input type="hidden" name="respJwt" id="respJwt" value="" />
										<input name="isSaved_Card" id="isSaved_Card" type="hidden" value="true" />
									</c:when>
									<c:when test="${pgType eq 'giftCard'}">
										<div id="order_review_giftCard" class="giftCardConfirmation">
											<c:if test="${indexVal eq 0}">
												<span><header>
	                                <fmt:message key="checkout.review.paymentMethod" />
									<c:if test="${pgName eq 'Review'}">
	                                  <span>.</span>
												<a href="javascript:void(0)" onclick="continueToCheckout('payment-tab');">
													<fmt:message key="checkout.review.edit" />
												</a>
											</c:if>
											</header>
											</span>
											</c:if>
											<p>
												<dsp:valueof bean="TRUConfiguration.displayPayPalPaymentTypeName.${pgType}" />
											</p>
											<p>
												<dsp:valueof converter="creditcard" param="paymentGroup.giftCardNumber" />
											</p>
											<p>
												<dsp:valueof param="paymentGroup.amount" locale="en_US" converter="currency" format="#.00" />
											</p>
										</div>
									</c:when>
									<c:when test="${pgType eq 'payPal' && orderPaymentAmount gt '0.0'}">
										<dsp:getvalueof var="payPalBillingAddress" param="paymentGroup.billingAddress" />
										<c:if test="${empty payPalBillingAddress or (empty payPalBillingAddress.firstName and empty payPalBillingAddress.lastName)}">
											<dsp:getvalueof var="paymentGroupBillingAddress" value="" />
										</c:if>
										<div>
											<c:if test="${indexVal eq 0}"><span><header>
                              <fmt:message key="checkout.review.paymentMethod" />
                              <c:if test="${pgName eq 'Review'}">
                                <span>.</span>
												<a href="javascript:void(0)" onclick="continueToCheckout('payment-tab');">
													<fmt:message key="checkout.review.edit" />
												</a>
											</c:if>
											</header>
											</span>
											</c:if>
											<p>
												<dsp:valueof bean="TRUConfiguration.displayPayPalPaymentTypeName.${pgType}" />
											</p>
											<p>
											</p>
										</div>
									</c:when>
									<c:when test="${pgType eq 'inStorePayment'}">
										<dsp:getvalueof var="paymentGrouPayInStore" value="true" />
										<div>
											<!-- <div class="col-md-5 payment-method"> -->
											<c:if test="${indexVal eq 0}"><span class="paymentMethod"><header>
                                <fmt:message key="checkout.review.paymentMethod" />
                                <c:if test="${pgName eq 'Review'}">
                                  <span>.</span>
												<!-- <a href="#" data-toggle="modal" data-target="#myAccountAddCreditCardModal" onclick="onEditCreditCardOverlay_checkout('test1')"> -->
												<a href="javascript:void(0)" onclick="continueToCheckout('payment-tab');">
													<fmt:message key="checkout.review.edit" />
												</a>
											</c:if>
											</header>
											</span>
											</c:if>
											<p>
												<fmt:message key="checkout.payment.payInStore" />
											</p>
											<!--  </div> -->

										</div>
									</c:when>
								</c:choose>
				</dsp:oparam>
				<dsp:oparam name="empty">
				</dsp:oparam>
			</dsp:droplet>
		</div>
		<%-- 	STAT: isempty check for paymentGroupBillingAddress --%>
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" value="${paymentGroupBillingAddress}" />
				<dsp:oparam name="false">
					<div class="col-md-6 billing-info billing-information">
						<h3 class="billingInfo">
							<fmt:message key="checkout.review.billingInformation" />
							<c:if test="${pgName eq 'Review' }">
								<span>.</span>&nbsp;
								<!-- <a href="#" data-toggle="modal" data-target="#myAccountAddCreditCardModal" onclick="onEditCreditCardOverlay_checkout('test1')"> -->
								<c:choose>
									<c:when test="${pgType eq 'payPal'}">
										<p>
											<fmt:message key="checkout.review.paypaleditbillingmessage" />
											<p>
									</c:when>
									<c:otherwise>
										<a href="javascript:void(0)" onclick="continueToCheckout('payment-tab');">
											<fmt:message key="checkout.review.edit" />
										</a>
									</c:otherwise>
								</c:choose>
							</c:if>
						</h3>
						<p>
							<dsp:valueof value="${paymentGroupBillingAddress.firstName}" />&nbsp;
							<dsp:valueof value="${paymentGroupBillingAddress.lastName}" />
						</p>
						<p>
							<c:if test="${ not empty paymentGroupBillingAddress.address1}">
								<dsp:valueof value="${paymentGroupBillingAddress.address1}" />,&nbsp; </c:if>
							<dsp:valueof value="${paymentGroupBillingAddress.address2}" />
						</p>
						<p>
							<c:if test="${ not empty paymentGroupBillingAddress.city}">
								<dsp:valueof value="${paymentGroupBillingAddress.city}" />,&nbsp;</c:if>
							<c:if test="${ not empty paymentGroupBillingAddress.state}">
								<dsp:valueof value="${paymentGroupBillingAddress.state}" />,&nbsp;</c:if>
							<dsp:valueof value="${paymentGroupBillingAddress.postalCode}" />
						</p>
						<dsp:getvalueof var="phoneNumberTemp" value="${paymentGroupBillingAddress.phoneNumber}" />
						<c:if test="${fn:length(phoneNumberTemp) gt 0}">
							<p>(${fn:substring(phoneNumberTemp, 0,3)})&nbsp;${fn:substring(phoneNumberTemp, 3,6)}-${fn:substring(phoneNumberTemp,
								6, fn:length(phoneNumberTemp))}</p>
						</c:if>
						<p>
							<dsp:valueof value="${paymentGroupBillingAddress.country}" />
						</p>
						<c:choose>
							<c:when test="${not empty rewardNumber}">
								<p><strong>&nbsp;<fmt:message key="checkout.review.rewardRUsMemNumber" />:</strong>&nbsp; ${rewardNumber}</p>
								<label for="membershipIDInReview"></label>
								<input type="hidden" id="enterMembershipIDInReview" name="enterMembershipIDInReview" value="${rewardNumber}" title="membershipIDInReview"
								/>
							</c:when>
							<c:when test="${empty rewardNumber and pgName eq 'Review'}">
								<p><strong>&nbsp;<fmt:message key="checkout.review.rewardRUsMemNumber" />:</strong>
									<form id="membershipIDInReviewForm" class="JSValidation">
										<label for="enterMembershipIDInReview"></label>
										<input type="text" id="enterMembershipIDInReview" name="enterMembershipIDInReview" maxlength="13" aria-label="enterMembershipIDInReview"
										 title="enterMembershipIDInReview" onkeypress="return isNumberOnly(event,13)" class="chkNumbersOnly" />
									</form>
								</p>
							</c:when>
						</c:choose>
						<input name="firstName" type="hidden" id="firstName" value="${paymentGroupBillingAddress.firstName}" />
						<input name="lastName" type="hidden" id="lastName" value="${paymentGroupBillingAddress.lastName}" />
						<input name="address1" type="hidden" id="address1" value="${paymentGroupBillingAddress.address1}" />
						<input name="address2" type="hidden" id="address2" value="${paymentGroupBillingAddress.address2}" />
						<input name="city" type="hidden" id="city" value="${paymentGroupBillingAddress.city}" />
						<input name="state" type="hidden" id="state" value="${paymentGroupBillingAddress.state}" />
						<input name="postalCode" type="hidden" id="postalCode" value="${paymentGroupBillingAddress.postalCode}" />
						<input name="country" type="hidden" id="country" value="${paymentGroupBillingAddress.country}" />
						<input name="phoneNumber" type="hidden" id="phoneNumber" value="${paymentGroupBillingAddress.phoneNumber}" />
						<input name="email" type="hidden" id="email" value="${order.email}" />
						<input name="email" type="hidden" id="orderEmail" value="${order.email}" />
					</div>
				</dsp:oparam>
				<dsp:oparam name="true">
					<div class="col-md-5 billing-info billing-information">
						<c:choose>
							<c:when test="${not empty rewardNumber}">
								<h3 class="billingInfo">
									<fmt:message key="checkout.review.billingInformation" />
									<c:choose>
										<c:when test="${pgType eq 'payPal'}">
											<p>
												<fmt:message key="checkout.review.paypaleditbillingmessage" />
												<p>
										</c:when>
									</c:choose>
								</h3>
								<p>
									<strong>&nbsp;<fmt:message
									key="checkout.review.rewardRUsMemNumber" />:
							</strong>&nbsp; ${rewardNumber}
								</p>
								<input type="hidden" id="enterMembershipIDInReview" name="enterMembershipIDInReview" title="enterMembershipIDInReview" value="${rewardNumber}"
								/>
							</c:when>
							<c:when test="${empty rewardNumber and pgName eq 'Review'}">
								<h3 class="billingInfo">
									<fmt:message key="checkout.review.billingInformation" />
									<c:choose>
										<c:when test="${pgType eq 'payPal'}">
											<p>
												<fmt:message key="checkout.review.paypaleditbillingmessage" />
												<p>
										</c:when>
									</c:choose>
								</h3>
								<p>
									<strong>&nbsp;<fmt:message key="checkout.review.rewardRUsMemNumber" />:</strong>
									<form id="membershipIDInReviewForm" class="JSValidation">
										<label for="membershipIDInReviewForm"></label>
										<input type="text" id="enterMembershipIDInReview" name="enterMembershipIDInReview" maxlength="13" aria-label="membershipIDInReviewForm"
										 title="membershipIDInReviewForm" onkeypress="return isNumberOnly(event,14)" class="chkNumbersOnly" />
									</form>
								</p>
							</c:when>
						</c:choose>
					</div>
				</dsp:oparam>
			</dsp:droplet>
	</div>
</dsp:page>