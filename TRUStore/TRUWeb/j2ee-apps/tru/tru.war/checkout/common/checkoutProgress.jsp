<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
	<dsp:getvalueof var="order" vartype="atg.commerce.Order" bean="ShoppingCart.current" />
	<dsp:getvalueof var="previousTab" bean="ShoppingCart.current.previousTab" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:getvalueof var="currentTab" bean="ShoppingCart.current.currentTab" />
	<dsp:getvalueof var="isOrderInstorePickup" value="${order.orderIsInStorePickUp}" vartype="java.lang.boolean" />
	<dsp:getvalueof var="currentTabNumber" bean="TRUConfiguration.checkoutTabNumbers.${currentTab}" />
	<dsp:getvalueof var="showMeGiftingOptions" bean="ShoppingCart.current.showMeGiftingOptions" />
	<div class="checkout-nav-bottom" id="co-tab-navigation">
		<div class="checkout-nav-bottom-row">
			<span href="#" class="active checkout-nav-bottom-tab checkout-tab">
			<div class="secure-checkout-lock inline"></div>
			<span class="archer-medium"><fmt:message key="checkout.shipping.checkoutHeader" /></span>
			</span>
			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="TRUConfiguration.checkoutTabUrls" />
				<dsp:param name="elementName" value="thisTab" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="thisTabName" param="key" />
					<dsp:getvalueof var="thisTabURL" param="thisTab" />
					<dsp:getvalueof var="thisTabURL" value="${contextPath}${thisTabURL}" />
					<dsp:getvalueof var="thisTabNumber" bean="TRUConfiguration.checkoutTabNumbers.${thisTabName}" />
					<c:set var="isActiveTab" value="false" />
					<dsp:droplet name="IsEmpty">
						<dsp:param name="value" bean="ShoppingCart.current.activeTabs.${thisTabName}" />
						<dsp:oparam name="false">
							<c:set var="isActiveTab" value="true" />
						</dsp:oparam>
						<dsp:oparam name="true">
							<c:set var="isActiveTab" value="false" />
						</dsp:oparam>
					</dsp:droplet>
					<c:choose>
						<c:when test="${thisTabNumber eq currentTabNumber}">
							<button class="checkout-nav-bottom-tab checkoutActive ${thisTabName}" name="${thisTabURL}">
			                 <a href="javascript:void(0)" class="avenir-next-demibold">
			                    <img src="${TRUImagePath}images/Blue-Checkmark.png" alt=""><fmt:message key="${thisTabName}" />
			                 </a>
			                </button>
						</c:when>
						<c:when test="${thisTabName eq 'review-tab' and isActiveTab eq true}">
							<button class="checkout-nav-bottom-tab ${thisTabName}" name="${thisTabURL}">
					                 <a href="javascript:void(0)" class="avenir-next-demibold">
					                    <img src="${TRUImagePath}images/Blue-Checkmark.png" alt=""><fmt:message key="${thisTabName}" />
					                 </a>
				                </button>
						</c:when>
						<c:when test="${thisTabName eq 'gifting-tab' }">
							<c:choose>
								<c:when test="${isActiveTab eq true and showMeGiftingOptions eq true and thisTabNumber le currentTabNumber}">
									<button class="checkout-nav-bottom-tab tickVisible activeTab ${thisTabName}" name="${thisTabURL}">
						                 <a href="javascript:void(0)" class="avenir-next-demibold">
						                    <img src="${TRUImagePath}images/Blue-Checkmark.png" alt=""><fmt:message key="${thisTabName}" />
						                 </a>
						                </button>
								</c:when>
								<c:when test="${showMeGiftingOptions eq true}">
									<button class="checkout-nav-bottom-tab ${thisTabName}" name="${thisTabURL}">
							                 <a href="javascript:void(0)" class="avenir-next-demibold">
							                    <img src="${TRUImagePath}images/Blue-Checkmark.png" alt=""><fmt:message key="${thisTabName}" />
							                 </a>
						                </button>
								</c:when>
								<c:otherwise>
									<button class="checkout-nav-bottom-tab display-none ${thisTabName}" name="${thisTabURL}">
					                		 <a href="javascript:void(0)" class="avenir-next-demibold">
					                    		<img src="${TRUImagePath}images/Blue-Checkmark.png" alt=""><fmt:message key="${thisTabName}" />
					                		 </a>
				                		</button>
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:when test="${thisTabName eq 'pickup-tab'}">
							<c:choose>
								<c:when test="${isActiveTab eq true and isOrderInstorePickup eq true and thisTabNumber le currentTabNumber}">
									<button class="checkout-nav-bottom-tab tickVisible activeTab ${thisTabName}" name="${thisTabURL}">
						                 <a href="javascript:void(0)" class="avenir-next-demibold">
						                    <img src="${TRUImagePath}images/Blue-Checkmark.png" alt=""><fmt:message key="${thisTabName}" />
						                 </a>
						                </button>
								</c:when>
								<c:when test="${isOrderInstorePickup eq true}">
									<button class="checkout-nav-bottom-tab ${thisTabName}" name="${thisTabURL}">
							               <a href="javascript:void(0)" class="avenir-next-demibold">
							                    <img src="${TRUImagePath}images/Blue-Checkmark.png" alt=""><fmt:message key="${thisTabName}" />
							               </a>
						                </button>
								</c:when>
								<c:otherwise>
									<button class="checkout-nav-bottom-tab display-none ${thisTabName}" name="${thisTabURL}">
											<a href="javascript:void(0)" class="avenir-next-demibold"> 
											<img src="${TRUImagePath}images/Blue-Checkmark.png" alt="">
											<fmt:message key="${thisTabName}" />
											</a>
										</button>
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:when test="${thisTabName eq 'review-tab'}">
							<button class="checkout-nav-bottom-tab display-none ${thisTabName}" name="${thisTabURL}">
				                 <a href="javascript:void(0)" class="avenir-next-demibold">
				                    <img src="${TRUImagePath}images/Blue-Checkmark.png" alt=""><fmt:message key="${thisTabName}" />
				                 </a>
			                </button>
						</c:when>
						<c:when test="${thisTabNumber ge currentTabNumber}">
							<button class="checkout-nav-bottom-tab ${thisTabName}" name="${thisTabURL}">
				                 <a href="javascript:void(0)" class="avenir-next-demibold">
				                    <img src="${TRUImagePath}images/Blue-Checkmark.png" alt=""><fmt:message key="${thisTabName}" />
				                 </a>
			                </button>
						</c:when>
						<c:when test="${thisTabNumber lt currentTabNumber and isActiveTab eq true}">
							<button class="checkout-nav-bottom-tab tickVisible activeTab ${thisTabName}" name="${thisTabURL}">
			                 <a href="javascript:void(0)" class="avenir-next-demibold">
			                    <img src="${TRUImagePath}images/Blue-Checkmark.png" alt=""><fmt:message key="${thisTabName}" />
			                 </a>
			                </button>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
				</dsp:oparam>
			</dsp:droplet>
			<div class="btn-group we-can-help">
				<div class="btn btn-default dropdown-toggle">
					<a href="/cobrand/help/online-help" title="Help"><em class='sprite sprite-help-icon'></em></a>
				</div>
				<ul class="dropdown-menu" role="menu">
					<li><img class="we-can-help-dropdown-arrow" src="${TRUImagePath}images/arrow.svg" alt=""></li>
					<li class="we-can-help-header">
						<fmt:message key="shoppingcart.we.can.help" />
					</li>
					<li class="divider"></li>
					<dsp:droplet name="/atg/targeting/TargetingForEach">
						<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/ShoppingCart/CartHelpLinksTargeter" />
						<dsp:oparam name="output">
							<dsp:valueof param="element.data" valueishtml="true" />
						</dsp:oparam>
					</dsp:droplet>
				</ul>
			</div>
		</div>
	</div>
</dsp:page>