<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
   <div class="shopping-cart-error-state checkout-payment-error-state">
                        <div class="shopping-cart-error-state-header row">
                            <div class="error-state-exclamation-lg img-responsive col-md-2"></div>
                            <div class="shopping-cart-error-state-header-text col-md-10">
                                <fmt:message key="checkout.errorHeader" />
                                <div class="shopping-cart-error-state-subheader"><fmt:message key="checkout.cardExpiredError" /></div>
                            </div>
                        </div>
                    </div>
</dsp:page>