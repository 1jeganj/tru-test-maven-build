<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/com/tru/commerce/cart/droplet/OrderSummaryDetailsDroplet" />
	<dsp:getvalueof var="order" bean="ShoppingCart.current" /> 
	<dsp:getvalueof var="pageName" param="pageName"/>
	<c:if test="${pageName eq 'Confirm' }">
		<dsp:getvalueof var="order" bean="ShoppingCart.last" /> 
	</c:if>
	<dsp:droplet name="OrderSummaryDetailsDroplet">
		<dsp:param name="order" value="${order}" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="commerceItemCount" param="itemCount" />
			<dsp:getvalueof var="giftCardAmount" param="giftCardAppliedAmount" />
			<dsp:getvalueof var="balanceAmount" param="balanceAmount" />
		</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="priceInfo" value="${order.priceInfo}" />
	<div class="summary-block-padded-content" tabindex="0">       
		<div class="order-summary">
			<fmt:message key="checkout.priceSummary.orderSummary" />
		</div>
		<div class="subtotal order-summary-item">
			<span class="order-summary-item-label">
				<fmt:message key="shoppingcart.subtotal" />&#32;
				<c:choose>
					<c:when test="${commerceItemCount gt 1}">
						<fmt:message key="shoppingcart.left.bracket" />
						<dsp:valueof value="${commerceItemCount}" />&nbsp;<fmt:message key="shoppingcart.items.bracket" />
					</c:when>
					<c:otherwise>
						<fmt:message key="shoppingcart.left.bracket" />
						<dsp:valueof value="${commerceItemCount}" />&nbsp;<fmt:message key="checkout.item.pickup"/>)
					</c:otherwise>
				</c:choose>
			</span>
			<span class="summary-price pull-right order-summary-item-value">
				<dsp:valueof format="currency"  value="${priceInfo.rawSubtotal}" locale="en_US" converter="currency"/>
			</span>
		</div>
		<dsp:getvalueof var="hasGiftWrapItems" vartype="java.lang.boolean" value="${order.GWItemInOrder}"/>
		<dsp:getvalueof var="giftWrapPrice" value="${priceInfo.giftWrapPrice}"/>
		<c:if test="${hasGiftWrapItems}">
			<div class="summary-gift-wrap order-summary-item">
				<span class="order-summary-item-label"><fmt:message key="checkout.priceSummary.giftWrap" /></span>
				<span class="summary-price pull-right order-summary-item-value">
					<c:choose>
						<c:when test="${giftWrapPrice <= 0.0}">
							<fmt:message key="shoppingcart.free" />
						</c:when>
						<c:otherwise>
							<dsp:valueof format="currency"  value="${giftWrapPrice}" locale="en_US" converter="currency"/>
						</c:otherwise>
					</c:choose>
				</span>
			</div>
		</c:if>
		<dsp:getvalueof var="promotionsSavings" value="${priceInfo.discountAmount}"/>
		<c:if test="${promotionsSavings ne '0.0'}"> 
			<div class="summary-promotions order-summary-item">
				<span class="order-summary-item-label"><fmt:message key="checkout.priceSummary.promotionSavings" /></span>
				<span class="summary-price pull-right order-summary-item-value">
					-<dsp:valueof format="currency"  value="${promotionsSavings}" locale="en_US" converter="currency"/>
				</span>
			</div>
		</c:if>
		<c:if test="${order.shipToHomeItemsCount > 0}">
			<dsp:getvalueof var="shipping" value="${priceInfo.shipping}"/>
			<div class="summary-shipping order-summary-item">
				<span class="order-summary-item-label"><fmt:message key="checkout.priceSummary.shipping" /></span>
				<span class="summary-price order-summary-item-value">
					<c:choose>
						<c:when test="${shipping == 0.0}">
							<c:choose>
								<c:when test="${order.shipToHomeShippingMethodsCount > 0}">
									<fmt:message key="shoppingcart.free" />
								</c:when>
								<c:otherwise>
									<fmt:message key="shoppingcart.emptyShipping" />
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<dsp:valueof format="currency" value="${shipping}" locale="en_US" converter="currency"/>
						</c:otherwise>
					</c:choose>
				</span>
			</div>
		</c:if>
		<dsp:getvalueof var="shippingSurcharge" value="${priceInfo.totalShippingSurcharge}"/>
		<c:if test="${shippingSurcharge > '0.0'}">
			<div class="summary-shipping-surcharge order-summary-item">
				<span class="order-summary-item-label"><fmt:message key="checkout.priceSummary.freeSurcharge" /> </span>
				<span class="summary-price pull-right order-summary-item-value">
					<dsp:valueof format="currency"  value="${shippingSurcharge}" locale="en_US" converter="currency"/>
				</span>
			</div>
		</c:if>
		<dsp:getvalueof var="ewasteFees" value="${priceInfo.ewasteFees}"/>
		<c:if test="${ewasteFees gt 0.0}">
			<div class="summary-fees order-summary-item">
				<span class="order-summary-item-label"> 
					<a data-target="#checkoutFeesPopup" data-toggle="modal" href="#"><fmt:message key="shoppingcart.ewaste.recycling.fee"/></a>
				</span>
				<span class="summary-price pull-right order-summary-item-value">
					<dsp:valueof format="currency" value="${ewasteFees}" locale="en_US" converter="currency"/>
				</span>
			</div>
		</c:if>
		<%-- <dsp:getvalueof var="mattressRecyclingFee" value="${priceInfo.mattressRecyclingFee}"/>
		<c:if test="${mattressRecyclingFee gt 0.0}">
			<div class="summary-fees order-summary-item">
				<span> 
					<a data-target="#checkoutFeesPopup" data-toggle="modal" href="#"><fmt:message key="shoppingcart.mattress.recycling.fee"/></a>
				</span>
				<span class="summary-price pull-right">
					<dsp:valueof format="currency" value="${mattressRecyclingFee}" locale="en_US" converter="currency"/>
				</span>
			</div>
		</c:if>  --%>
		<div class="summary-sales-tax order-summary-item">
			<c:choose>
				<c:when test="${pageName eq 'Confirm' }">
					<span class="order-summary-item-label"><fmt:message key="checkout.priceSummary.salesTax" /></span>
				</c:when>
				<c:otherwise>
					<span class="order-summary-item-label"><fmt:message key="shoppingcart.estimated.sales.tax" /></span>
				</c:otherwise>
			</c:choose>
			<span class="summary-price pull-right order-summary-item-value">
				<c:choose>
					<c:when test="${not empty order.taxPriceInfo && not empty order.taxPriceInfo.estimatedSalesTax}"> 
						<dsp:valueof format="currency" value="${order.taxPriceInfo.estimatedSalesTax}" locale="en_US" converter="currency"/>
					</c:when>
					<c:otherwise>
						<dsp:valueof format="currency" value="0.0" locale="en_US" converter="currency"/>
					</c:otherwise>
				</c:choose>
			</span>
		</div>
		<dsp:getvalueof var="estimatedLocalTax" value="${order.taxPriceInfo.estimatedLocalTax}"/>
		<c:if test="${estimatedLocalTax gt 0.0}">
			<div class="estimatedLocalTax order-summary-item">
				<span>
					<a id="estimatedLocalTaxLabel" data-toggle="popover" href="#" data-original-title="" title="">
						<fmt:message key="shoppingcart.estimated.local.tax" />
					</a>
				</span>
				<div class="popover estimatedLocalTaxTooltip" role="tooltip">
					<div class="arrow"></div>
					<span class="content"><fmt:message key="checkout.priceSummary.estimatedLocalTax"/> </span>
				</div>
				<span>
					<dsp:valueof format="currency" value="${estimatedLocalTax}" locale="en_US" converter="currency"/>
				</span>
			</div>
		</c:if>
		<dsp:getvalueof var="estimatedIslandTax" value="${order.taxPriceInfo.estimatedIslandTax}"/>
		<c:if test="${estimatedIslandTax gt 0.0}">
			<div class="estimatedIslandTax">
				<span>
					<a id="estimatedIslandTaxLabel" data-toggle="popover" href="#" data-original-title="" title="">
						<fmt:message key="shoppingcart.estimated.island.tax" />
					</a>
				</span>
				<div class="popover estimatedIslandTaxTooltip" role="tooltip">
					<div class="arrow"></div>
					<span class="content"><fmt:message key="checkout.priceSummary.estimatedIslandTax"/> </span>
				</div>
				<span>
					<dsp:valueof format="currency" value="${order.taxPriceInfo.estimatedIslandTax}" locale="en_US" converter="currency"/>
				</span>
			</div>
		</c:if>
		<dsp:getvalueof var="total" value="${priceInfo.total}"/>
		<c:choose>
			<c:when test="${total eq 0.0 || balanceAmount eq 0.0}">
				<input type="hidden" class="zeroOrderTotal" value="true"/>
			</c:when>
			<c:otherwise>
				<input type="hidden" class="zeroOrderTotal" value="false"/>
			</c:otherwise>
		</c:choose>
		<div class="footer-total order-summary-item">
			<span class="order-summary-item-label"><fmt:message key="checkout.priceSummary.total" /></span>
			<span class="summary-price order-summary-item-value"><dsp:valueof format="currency" value="${total}" locale="en_US" converter="currency"/></span>
		</div>                                
		<c:choose>
			<c:when test="${giftCardAmount ne 0.0}">
				<div class="summary-gift-card">
					<span><fmt:message key="checkout.priceSummary.giftCard" /></span>
					<span class="summary-price pull-right">-<dsp:valueof format="currency" value="${giftCardAmount}" locale="en_US" converter="currency"/></span>
				</div>
				<div class="footer-total">
					<span class="summary-block-footer-total"><fmt:message key="checkout.priceSummary.balance" /></span>
					<span class="summary-price"><dsp:valueof format="currency" value="${balanceAmount}" locale="en_US" converter="currency"/></span>
				</div>
				<input type="hidden" name="cardianlOrderAmount" id="cardianlOrderAmount" value="${balanceAmount}"/>
			</c:when>
		</c:choose>
		<dsp:getvalueof var="totalSavings" value="${priceInfo.totalSavings}"/>
		<dsp:getvalueof var="totalSavingsPercentage" value="${priceInfo.totalSavingsPercentage}"/>		
		<c:if test="${totalSavings gt 0.0}">
			<div class="order-summary__you-saved" tabindex="0">
				<fmt:message key="checkout.you.saved"/>&nbsp;<dsp:valueof value="${totalSavings}" locale="en_US" converter="currency" format="0.00" /> (<dsp:valueof value="${totalSavingsPercentage}" number="0.00"/><fmt:message key="pricing.persentage" />) <fmt:message key="checkout.on.this.order"/>
			</div>
		</c:if>
		<div id="action_controls">
			<dsp:include page="actionControls.jsp">
				<dsp:param name="pageName" param="pageName" />
			</dsp:include>
		</div>
	</div>
	<input type="hidden" name="cardianlOrderAmount" id="cardianlOrderAmount" value="${order.priceInfo.total}"/>
	<div class="summary-block-footer">
		<div class="summary-block-footer-padded">
			<div class="order-summary-secure-logos">
				<dsp:getvalueof var="pageName" param="pageName" />
				<div>
					<div class="norton-secured"  id="load-checkout-norton-script">
					<dsp:include page="../../common/nortonSealScript.jsp"/>
					<!-- <a href="http://www.norton.com/" target="_blank"><img src="${TRUImagePath}images/Global_Norton-Logo.jpg"></a> -->
					</div> 
				</div>
				<c:if test="${pageName eq 'Payment' || pageName eq 'Review' && enableMasterAndVisaIcons}">
					<dsp:getvalueof var="visaURL" bean="TRUConfiguration.visaURL"/>
					<dsp:getvalueof var="mastercardURL" bean="TRUConfiguration.mastercardURL"/>
					<div>
						<div class="secure-purchase-text">
							<p><fmt:message key="checkout.payment.securePurchase" /></p>
						</div>
						<div class="verifications">
							<div class="verified-by-mastercard inline">
							<a href="${visaURL}" target="_blank" aria-label="Verified by Visa">
								<img src="${TRUImagePath}images/Payment_Verified-by-Visa-Lockup.jpg" alt="">
							</a>
							</div>
							<div class="verified-by-visa inline">
							<a href="${mastercardURL}" target="_blank" aria-label="Master Card secure code">
								<img src="${TRUImagePath}images/Payment_MasterCard-SecureCode-Lockup.jpg" alt="payment master card">
							</a>
							</div>
						</div>
					</div>
				</c:if>
			</div>			
		</div>
	</div>
	<c:set var="epsilonOrderQuantity" value="${commerceItemCount}" scope="request"/>  
	<c:set var="epsilonOrderAmount" value="${total}" scope="request"/>
</dsp:page>