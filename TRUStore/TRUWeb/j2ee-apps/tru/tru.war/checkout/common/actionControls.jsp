<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />   
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUOrderRemainingAmountDroplet" />
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler" />
	<dsp:importbean bean="/com/tru/commerce/droplet/TRUCheckPayPalTokenDroplet" />
	<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler" />
	<%-- <dsp:importbean bean="/com/tru/radial/payment/paypal/TRUPayPalConfiguration" /> --%>
	<dsp:getvalueof var="pageName" param="pageName" />   
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:getvalueof var="previousTab" bean="ShoppingCart.current.previousTab" />
	<dsp:getvalueof var="applyGiftingOptions" bean="ShoppingCart.current.applyGiftingOptions" />
	<dsp:getvalueof var="commerceItems" bean="ShoppingCart.current.commerceItems"/>
	<dsp:getvalueof var="order" vartype="atg.commerce.Order" bean="ShoppingCart.current"/>
	<dsp:test var="items" value="${commerceItems }"/>
	<dsp:getvalueof var="showMeGiftingOptions" bean="ShoppingCart.current.showMeGiftingOptions" />
	<dsp:getvalueof var="isOrderInstorePickup" value="${order.orderIsInStorePickUp}" vartype="java.lang.boolean"/>
	<dsp:getvalueof var="paymentMethod" param="paymentMethod" />
	<dsp:droplet name="ForEach">
	<dsp:param name="array" bean="ShoppingCart.current.paymentGroups" />
	<dsp:param name="elementName" value="paymentGroup" /> 
		<dsp:oparam name="output">
	 		<dsp:getvalueof var="pgType" param="paymentGroup.paymentGroupClassType"/>	 		
	 		<c:if test="${pgType eq 'payPal'}">
	 			<dsp:setvalue param="paymentMethodValue" value="PayPal" /> 
	 			<dsp:getvalueof var="paymentMethod" param="paymentMethodValue" />
	 		</c:if>			 		
	 	</dsp:oparam>
 	</dsp:droplet>
	<dsp:droplet name="TRUOrderRemainingAmountDroplet">
		<dsp:param bean="ShoppingCart.current" name="order" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="finalRemaniningAmount"
				param="finalRemaniningAmount" />
		</dsp:oparam>
	</dsp:droplet>
	<input type="hidden" id="previousTabValue" value="${previousTab}"/>
	<c:choose>
		<c:when test="${pageName eq 'Shipping' and (showMeGiftingOptions eq true and applyGiftingOptions eq true) }">
			<button class="summary-block-secure-checkout  text-center" data-backdrop="static"  onclick="onSubmitShippingAddCheckout('', '')" >
				<span class="secure-checkout-lock inline"></span>
				<fmt:message key="checkout.priceSummary.giftingLink"/>
			</button>
		</c:when>
	    <c:when test="${pageName eq 'Shipping' and previousTab ne 'shipping-tab' and previousTab eq 'review-tab'}" >
			<button class="summary-block-secure-checkout  text-center" data-backdrop="static" onclick="onSubmitShippingAddCheckout('${previousTab}', '')" >
				<span class="secure-checkout-lock inline"></span>
				<fmt:message key="checkout.priceSummary.reviewOrderLink" />
			</button>	
		</c:when>
		<c:when test="${pageName eq 'Shipping' and previousTab ne 'shipping-tab' and previousTab eq 'payment-tab'}" >
			<button class="summary-block-secure-checkout  text-center" data-backdrop="static" onclick="onSubmitShippingAddCheckout('${previousTab}', '')" >
				<span class="secure-checkout-lock inline"></span>
				<fmt:message key="checkout.priceSummary.paymentLink" />
			</button>
		</c:when>
		<c:when test="${pageName eq 'Shipping' and previousTab ne 'shipping-tab' and previousTab eq 'pickup-tab'}" >
			<div class="summary-block-secure-checkout  text-center" data-backdrop="static" onclick="gifting('${previousTab}');">
				<span class="secure-checkout-lock inline"></span>
				<fmt:message key="checkout.priceSummary.pickupLink"/>
			</div>
		</c:when>
		<c:when test="${pageName eq 'Shipping' and previousTab ne 'shipping-tab' and showMeGiftingOptions eq true and previousTab eq 'gifting-tab'}" >
			<button class="summary-block-secure-checkout  text-center" data-backdrop="static" onclick="onSubmitShippingAddCheckout('${previousTab}', '')" >
				<span class="secure-checkout-lock inline"></span>
				<fmt:message key="checkout.priceSummary.giftingLink"/>
			</button>
		</c:when>
		<c:when test="${pageName eq 'Shipping' and showMeGiftingOptions eq true}">
			<button class="summary-block-secure-checkout  text-center" data-backdrop="static" onclick="onSubmitShippingAddCheckout('', '')" >
				<span class="secure-checkout-lock inline"></span>
				<fmt:message key="checkout.priceSummary.giftingLink"/>
			</button>
		</c:when>
		<c:when test="${pageName eq 'Shipping' and showMeGiftingOptions eq false and isOrderInstorePickup eq true}">
			<button class="summary-block-secure-checkout  text-center" data-backdrop="static"  onclick="onSubmitShippingAddCheckout('', '')" >
				<span class="secure-checkout-lock inline"></span>
				<fmt:message key="checkout.priceSummary.pickupLink"/>
			</button>
		</c:when>
		<c:when test="${pageName eq 'Shipping' and showMeGiftingOptions eq false}">
			<button class="summary-block-secure-checkout  text-center" data-backdrop="static"  onclick="onSubmitShippingAddCheckout('', '')" >
				<span class="secure-checkout-lock inline"></span>
				<fmt:message key="checkout.priceSummary.paymentLink" />
			</button>
		</c:when>
		<c:when test="${pageName eq 'Shipping'}">
			<dsp:getvalueof var="showMeGiftingOptions" bean="ShoppingCart.current.showMeGiftingOptions" />
			<dsp:droplet name="Switch">
				<dsp:param value="${showMeGiftingOptions}" name="value" />
				<dsp:oparam name="true">
					 <button class="summary-block-secure-checkout  text-center" data-backdrop="static"  onclick="onSubmitShippingAddCheckout('', '')" >
						<span class="secure-checkout-lock inline"></span>
						<fmt:message key="checkout.priceSummary.giftingLink"/>
					</button>
				</dsp:oparam>
				<dsp:oparam name="false">
			  		 <button class="summary-block-secure-checkout  text-center" data-backdrop="static"  onclick="onSubmitShippingAddCheckout('', '')" >
						<span class="secure-checkout-lock inline"></span>
						<c:choose>
							<c:when test="${isOrderInstorePickup eq true}">
								<fmt:message key="checkout.priceSummary.pickupLink"/>
							</c:when>
							<c:otherwise>
								<fmt:message key="checkout.priceSummary.paymentLink" />
							</c:otherwise>
					</c:choose>
					</button>
				</dsp:oparam>
			</dsp:droplet>
		</c:when>
		<c:when test="${pageName eq 'gifting' and previousTab eq 'pickup-tab'}" >
			<button class="summary-block-secure-checkout  text-center" data-backdrop="static" onclick="gifting('${previousTab}');">
				<span class="secure-checkout-lock inline"></span>
				<fmt:message key="checkout.priceSummary.pickupLink"/>
			</button>
		</c:when>
		<c:when test="${pageName eq 'gifting' and previousTab eq 'payment-tab'}" >
			<button class="summary-block-secure-checkout  text-center" data-backdrop="static" onclick="gifting('${previousTab}');">
				<span class="secure-checkout-lock inline"></span>
				<fmt:message key="checkout.priceSummary.paymentLink" />
			</button>
		</c:when>
		<c:when test="${pageName eq 'gifting' and previousTab eq 'review-tab'}" >
			<button class="summary-block-secure-checkout  text-center" data-backdrop="static" onclick="gifting('${previousTab}');">
				<span class="secure-checkout-lock inline"></span>
				<fmt:message key="checkout.priceSummary.reviewOrderLink" />
			</button>
		</c:when>
		<c:when test="${pageName eq 'gifting'}">
			<button class="summary-block-secure-checkout  text-center" data-backdrop="static" onclick="gifting('');">
				<span class="secure-checkout-lock inline"></span>
				<c:choose>
					<c:when test="${isOrderInstorePickup eq true}">
						<fmt:message key="checkout.priceSummary.pickupLink"/>
					</c:when>
					<c:otherwise>
						<fmt:message key="checkout.priceSummary.paymentLink" />
					</c:otherwise>
				</c:choose>
			</button>
		</c:when>
		<c:when test="${pageName eq 'Store-Pickup' and previousTab eq 'review-tab'}">
		   <button class="summary-block-secure-checkout  text-center" data-backdrop="static" onclick="instore('${previousTab}');">
				<span class="secure-checkout-lock inline"></span>
				<fmt:message key="checkout.priceSummary.reviewOrderLink" />
		   </button>
		</c:when>
		<c:when test="${pageName eq 'Store-Pickup' and previousTab ne 'review-tab'}">
		   <button class="summary-block-secure-checkout  text-center" data-backdrop="static" onclick="instore('${previousTab}');">
				<span class="secure-checkout-lock inline"></span>
				<fmt:message key="checkout.priceSummary.paymentLink" />
		   </button>
		</c:when>
		<c:when test="${items.size == 0 and pageName ne 'Confirm'}">
		   <button class="summary-block-secure-checkout  text-center" data-backdrop="static">
				<span class="secure-checkout-lock inline"></span>
				<fmt:message key="shoppingcart.secure.checkout"/>
			</button>
		</c:when>
		<c:when test="${pageName eq 'Payment'}">
			<c:choose>
				<c:when test="${paymentMethod eq 'PayPal' && finalRemaniningAmount gt '0.0'}">
					<dsp:droplet name="TRUCheckPayPalTokenDroplet">
					  <dsp:param name="order" bean="/atg/commerce/ShoppingCart.current"/>
					  <dsp:param name="userSession" bean="/com/tru/common/TRUUserSession"/>
					  <dsp:oparam name="output">
							<dsp:droplet name="Switch">
								<dsp:param param="isAlreadyToken" name="value" />
								<dsp:oparam name="true">
								    <input type="hidden" name="payPalToken" id="payPalToken" value="tokenExist"/>
									<dsp:a href="" bean="CommitOrderFormHandler.expressCheckoutWithPayPal" id="paypalExpresscheckoutId" value="expressCheckoutWithPayPal" />
								</dsp:oparam>
								<dsp:oparam name="false">
								    <input type="hidden" name="payPalToken" id="payPalToken" value="notTokenExist"/>
								</dsp:oparam>
							</dsp:droplet>				      	
					  </dsp:oparam>
					</dsp:droplet>
				   	<button class="summary-block-secure-checkout  text-center paypal_payment_commitOrder" id="nongoodbye_commitOrder"  data-backdrop="static">
						<span class="secure-checkout-lock inline"></span>
						<fmt:message key="checkout.priceSummary.paypalLink"/>
				   </button>
				</c:when>
				<c:otherwise>
					<button class="summary-block-secure-checkout  text-center" id="nongoodbye_commitOrder" 
						 onclick="return commitOrder()">
						<span class="secure-checkout-lock inline"></span>
						<fmt:message key="checkout.priceSummary.placeOrderButton" />
					</button>
				</c:otherwise>
			</c:choose>
			<div class="checkout-review-order move-to-order-review  text-center" tabindex="0" onclick="moveToOrderReview()"><%-- startTokenization_checkout() --%> 
				<fmt:message key="checkout.priceSummary.reviewOrderLink" />
				<img class="inline" src="${TRUImagePath}images/breadcrumb-arrow-right.png" alt="">
			</div>
		</c:when>
		<c:when test="${pageName eq 'Review'}">
			<c:if test="${empty paymentMethod}">
				<dsp:droplet name="Switch">
					<dsp:param bean="ShoppingCart.current.payPalOrder" name="value" />
					<dsp:oparam name="true">
						<dsp:getvalueof var="paymentMethod" value="PayPal"/>
						<dsp:getvalueof var="cartPage" value="true"/>
					</dsp:oparam>
				</dsp:droplet>
			</c:if>
			<c:choose>
				<c:when test="${(paymentMethod eq 'PayPal') and (empty cartPage) and finalRemaniningAmount gt '0.0'}">
					<dsp:droplet name="TRUCheckPayPalTokenDroplet">
					  <dsp:param name="order" bean="/atg/commerce/ShoppingCart.current"/>
					  <dsp:param name="userSession" bean="/com/tru/common/TRUUserSession"/>
					  <dsp:oparam name="output">
							<dsp:droplet name="Switch">
								<dsp:param param="isAlreadyToken" name="value" />
								<dsp:oparam name="true">
									<button class="summary-block-secure-checkout  text-center" id="nongoodbye_commitOrder" 
										data-backdrop="static" onclick="payPalPlaceOrder()">
										<span class="secure-checkout-lock inline"></span>
										<fmt:message key="checkout.priceSummary.placeOrderButton"/>
								   </button>
								</dsp:oparam>
								<dsp:oparam name="false">
								   <input type="hidden" name="payPalReviewPageToken" id="payPalReviewPageToken" value="notTokenExist"/>
								    <button class="summary-block-secure-checkout  text-center paypal_review_commitOrder" id="nongoodbye_commitOrder" 
										data-backdrop="static">
										<span class="secure-checkout-lock inline"></span>
										<fmt:message key="checkout.priceSummary.paypalLink"/>
									</button>
								</dsp:oparam>
							</dsp:droplet>				      	
					  </dsp:oparam>
					</dsp:droplet>				
				</c:when>
				<c:when test="${(paymentMethod eq 'PayPal') and (cartPage eq 'true') and finalRemaniningAmount gt '0.0'}">
					<dsp:a href="" bean="CommitOrderFormHandler.cartExpressCheckoutWithPayPal" id="paypalPaymentExpresscheckoutId" value="expressCheckoutWithPayPal">
						<button class="summary-block-secure-checkout  text-center" id="nongoodbye_place_order_now" data-backdrop="static">
							<span class="secure-checkout-lock inline"></span>
							<fmt:message key="checkout.priceSummary.placeOrderButton"/>
					   </button>
					</dsp:a>
				</c:when>
				<c:otherwise>
					<button class="summary-block-secure-checkout  text-center review_placeOrder" id="nongoodbye_place_order_now" data-backdrop="static">
						<span class="secure-checkout-lock inline"></span>
						<fmt:message key="checkout.priceSummary.placeOrderButton"/>
					</button>
				</c:otherwise>
			</c:choose>			
		</c:when>	
	</c:choose>
</dsp:page>