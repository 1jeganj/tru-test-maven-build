<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />   
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/com/tru/commerce/cart/droplet/OrderSummaryDetailsDroplet" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:getvalueof var="validationFailed" bean="ShoppingCart.current.validationFailed"/> 
	<dsp:droplet name="OrderSummaryDetailsDroplet">
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="commerceItemCount" param="itemCount" />
		</dsp:oparam>
		<dsp:oparam name="empty">
			<dsp:droplet name="/atg/commerce/order/purchase/RepriceOrderDroplet">
			  <dsp:param value="ORDER_TOTAL" name="pricingOp"/>
			</dsp:droplet>
			<dsp:getvalueof var="commerceItemCount" value="0" />
		</dsp:oparam>
	</dsp:droplet>
	<c:choose>
		
		<c:when test="${empty commerceItemCount || commerceItemCount eq 0 }">
			{"redirect": true ,
			"shipRestrictionsFound":"<dsp:valueof bean="ShoppingCart.current.shipRestrictionsFound"/>" ,
			"validationFailed":${validationFailed} 
			}
		</c:when>
		<c:otherwise>
			{"redirect": false ,
			"shipRestrictionsFound":"<dsp:valueof bean="ShoppingCart.current.shipRestrictionsFound"/>",
			"validationFailed":${validationFailed}
			}
		</c:otherwise>
	</c:choose>
</dsp:page>