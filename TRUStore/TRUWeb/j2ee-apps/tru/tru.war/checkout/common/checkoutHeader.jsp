<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/dynamo/Configuration" var="Configuration"/>
	<dsp:importbean bean="/com/tru/commerce/locations/TRUMyStoreDroplet"/>
	<dsp:importbean bean="/atg/commerce/locations/StoreLookupDroplet"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/com/tru/commerce/cart/droplet/ChannelTagDetailsDroplet"/>
	<dsp:importbean bean="/com/tru/droplet/TRUGetSiteDroplet" />
	<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations" />
	<dsp:importbean bean="/atg/endeca/assembler/cartridge/manager/AssemblerSettings" />
	<%-- <dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" /> --%>
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:getvalueof var="host" value="${originatingRequest.host}"/>
	<dsp:getvalueof var="serverName" value="${originatingRequest.serverName}"/>
	<dsp:importbean bean="/com/tru/common/TRUConfiguration" var="TRUConfiguration"/>
	<dsp:getvalueof var="akamaiSiteParam" bean="TRUConfiguration.akamaiSiteParam"/>
	<dsp:getvalueof var="bruSiteCheck" bean="EndecaConfigurations.bruSiteCheck"/>
	<dsp:getvalueof var="previewEnabled" bean="AssemblerSettings.previewEnabled"/>
	<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id" />
	<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
		<dsp:oparam name="output">
		<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
		</dsp:oparam>
	</dsp:droplet>
	
	<c:choose>
		<c:when test="${fn:containsIgnoreCase(host, ':')}">
			<dsp:getvalueof var="httpURL" value="${scheme}${serverName}:${Configuration.httpPort}"/>
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="httpURL" value="${scheme}${host}"/>
		</c:otherwise>
	</c:choose>
	
	<%-- START:: Changes made for creating Site cookie for every page request --%>
	<c:forEach var="entry" items="${TRUConfiguration.customParamToSiteIdMapping}">
		<c:if test="${entry.value eq siteName}">
			<dsp:getvalueof var="akamaiSiteCookieValue" value="${entry.key}"/>
		</c:if>
	</c:forEach>
	<c:if test="${TRUConfiguration.enableSiteCookieCreationInJS}">
		<input type="hidden" id="currentSiteCookieParam" value="${akamaiSiteParam}"/>
		<input type="hidden" id="currentSiteCookieValue" value="${akamaiSiteCookieValue}"/>
	</c:if>
	<%-- END:: Changes made for creating Site cookie for every page request --%>
	
	<%-- <dsp:getvalueof var="tealiumSyncURL" bean="TRUTealiumConfiguration.tealiumSyncURL" />
	<script src="${tealiumSyncURL}"></script> --%>
		
	<input type="hidden" class="contextPath" value="${contextPath}">
	<c:choose>
		<c:when test="${siteName eq 'ToysRUs'}">
			<dsp:getvalueof var="contextRoot" value="TRU" />
		</c:when>
		<c:when test="${siteName eq 'BabyRUs'}">
			<dsp:getvalueof var="contextRoot" value="BRU" />
		</c:when>
	</c:choose>
	<nav class="navbar navbar-default top-navbar">
           <div class="container-fluid">
               <ul class="nav navbar-nav">
					<dsp:droplet name="TRUGetSiteDroplet">
						<dsp:param name="siteId" value='ToysRUs' />
						<dsp:oparam name="output">
							<dsp:getvalueof var="siteUrl1" param="siteUrl"/>
							<li><a class="checkout-tru-logo domain-identity" href="${scheme}${siteUrl1}" data-site-identity-cookie-name="${akamaiSiteParam}" data-site-identity-cookie-value="tru"> <img src="${TRUImagePath}images/tru-logo-sm.png" alt="toysrus logo"></a>	</li>
						</dsp:oparam>
					</dsp:droplet>
					<dsp:droplet name="TRUGetSiteDroplet">
						<dsp:param name="siteId" value='BabyRUs' />
						<dsp:oparam name="output">
							<dsp:getvalueof var="siteUrl2" param="siteUrl"/> 
							<c:if test="${not empty siteUrl2 and bruSiteCheck eq true and previewEnabled ne true and fn:contains(siteUrl2,'.toysrus.com') and fn:indexOf(siteUrl2,'aos')==-1}">
								<dsp:getvalueof var="siteUrl2" value="${fn:replace(siteUrl2,'.toysrus.com','.babiesrus.com')}"/>
							</c:if>
						    <li><a class="checkout-bru-logo domain-identity" href="${scheme}${siteUrl2}" data-site-identity-cookie-name="${akamaiSiteParam}" data-site-identity-cookie-value="bru"> <img src="${TRUImagePath}images/babiesrus-logo-small.png" alt="babiesrus logo"></a></li>
						</dsp:oparam>
					</dsp:droplet>
               </ul>
               <dsp:include page="/sos/sosHeader.jsp" />
               <dsp:droplet name="TRUMyStoreDroplet">
				<dsp:oparam name="output">
				<dsp:getvalueof var="cookieStoreId" param="cookieStoreId" />
				<dsp:droplet name="StoreLookupDroplet">
					<dsp:param name="id" value="${cookieStoreId}" />
					<dsp:param name="elementName" value="store" />
					<dsp:oparam name="output">
		               <%--  <ul class="nav navbar-nav navbar-right shopping-cart-your-store no-right-margin">
		                    <li class="find-store-top-border-filler">
		                        <div class="nav-bar-top-text">
		                            <div class="find-store inline"></div><span><fmt:message key="shoppingcart.your.store" /></span>
		                            &nbsp;<dsp:valueof param="store.address1" />
		                        </div>
		                    </li>
		                </ul> --%>
		            </dsp:oparam>
                </dsp:droplet>
               </dsp:oparam>
            </dsp:droplet>
                <ul class="nav navbar-nav navbar-right no-right-margin forDefaultStore">
                <%-- <dsp:droplet name="ChannelTagDetailsDroplet">
					<dsp:param name="order" bean="ShoppingCart.current"/>
					<dsp:oparam name="output">
	                    <li class="continue-shopping">
	                        <div class="btn-group">
	                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
	                                <div class="continue-shopping-text">
	                                    <div class="continue-shopping-left-arrow inline"></div><fmt:message key="checkout.header.continueShopping" /></div>
	                            </button>
	                            <ul class="dropdown-menu" role="menu">
	                                <li><img class="continue-shopping-dropdown-arrow checkout-arrow-align" src="/images/arrow.svg" alt=""></li>
	                                This section is not yet implemented
	                                <dsp:droplet name="IsEmpty">
	                                	<dsp:param name="value" param="registryUserName" />
	                                	<dsp:oparam name="false">
	                                		<dsp:getvalueof var="registryUrl" param="registryUrl"/>
	                                		<li><a href="${registryUrl}"><dsp:valueof param="registryUserName"/><fmt:message key="checkout.header.registry" /></a></li>
	                                	</dsp:oparam>
	                                </dsp:droplet>
	                               <dsp:droplet name="IsEmpty">
	                                	<dsp:param name="value" param="wishlistUserName" />
	                                	<dsp:oparam name="false">
	                                		<dsp:getvalueof var="wishlistUrl" param="wishlistUrl"/>
	                                		<li><a href="${wishlistUrl}"><dsp:valueof param="wishlistUserName"/><fmt:message key="checkout.header.wishlist" /></a></li>
	                                	</dsp:oparam>
	                                </dsp:droplet>
	                                <li><a id="nongoodbye_shoppingCart" href="${contextPath}cart/shoppingCart.jsp?ab=${contextRoot}_Header:Utility1:continue-shopping:Checkout-Page"><fmt:message key="checkout.header.previousPage" /></a>
	                                </li>
	                                <li><a id="nongoodbye_home" href="${httpURL}${contextPath}home?ab=${contextRoot}_Header:Utility1:continue-shopping:Checkout-Page"><fmt:message key="checkout.header.home" /></a>
	                                </li>
	                            </ul>
	                        </div>
	                    </li>
                    </dsp:oparam>
                   </dsp:droplet> --%>
               </ul> 
           </div>
        </nav>
        <dsp:include page="/header/storeLocatorPopup.jsp"/>
        <dsp:include page="/storelocator/storeDetails.jsp"/>
	
</dsp:page>