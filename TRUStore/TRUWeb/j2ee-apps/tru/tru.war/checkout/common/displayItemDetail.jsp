<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
	
	<dsp:setvalue param="item" paramvalue="cartItemDetailVO" />
	<dsp:getvalueof var="uniqueId" param="item.uniqueId" />
	<dsp:getvalueof var="pageName" param="pageName" />
	<dsp:getvalueof var="shippingGroupType" param="shippingGroupType" />
	<dsp:getvalueof param="existingRelationShipId" var="existingRelationShipId"/>
	<dsp:getvalueof param="inlineMessage" var="inlineMessage"/>
	<dsp:getvalueof var="checkoutCommonImageSizeMap" bean="TRUStoreConfiguration.checkoutCommonImageSizeMap" />
	
                    <div class="product-information" tabindex="0" id="${uniqueId}">
                        <div class="product-information-header">
                            <div class="product-header-title order-confirmation-pro-title">
                                 <dsp:valueof param="item.displayName"/>
	                            </div>
				<div class="product-header-price">
                                <dsp:valueof param="item.totalAmount" converter="currency" />
                            </div>
								<c:if test="${pageName eq 'Confirm'}">
									<div id="fb-root" class="shopping-cart-product-share inline facebook"></div>
								</c:if>
                        </div>
                        <dsp:droplet name="IsEmpty">
							<dsp:param name="value" param="item.channel" />
							<dsp:oparam name="false">
								<div class="registry inline">
									<div></div>
									<fmt:message key="shoppingcart.added.from" />&nbsp;<dsp:valueof param="item.channel" valueishtml="true"/>
								</div>
							</dsp:oparam>
						</dsp:droplet>
                        <div class="product-information-content">
                            <div class="row row-no-padding">
                                <div class="col-md-2">
                                    <div>
										<dsp:getvalueof var="productImage" param="item.productImage" />
										<c:forEach var="entry" items="${checkoutCommonImageSizeMap}">
											<c:if test="${entry.key eq 'height'}">
												<c:set var="imageHeight" value="${entry.value}" />
											</c:if>
											<c:if test="${entry.key eq 'width'}">
												<c:set var="imageWidth" value="${entry.value}" />
											</c:if>
										</c:forEach>
										<dsp:droplet name="IsEmpty">
											<dsp:param name="value" value="${productImage}" />
											<dsp:oparam name="false">
												<dsp:droplet
														name="/com/tru/common/droplet/TRUAkamaiImageDroplet">
													<dsp:param name="imageName" value="${productImage}" />
													<dsp:param name="imageHeight" value="${imageHeight}" />
													<dsp:param name="imageWidth" value="${imageWidth}" />
													<dsp:oparam name="output">
														<dsp:getvalueof var="akamaiImageUrl" param="akamaiUrl" />
													</dsp:oparam>
												</dsp:droplet>
												<img class="product-description-image" src="${akamaiImageUrl}" alt="Product Image">
											</dsp:oparam>
											<dsp:oparam name="true">
												<img class="product-description-image image-not-available" src="${TRUImagePath}images/no-image500.gif" alt="Product Image">
											</dsp:oparam>
										</dsp:droplet>
                                    </div>
                                </div>
                                <div class="col-md-6 donation-amount"></div>
	                           	<div class="cart-product-info-container">  	                                
	                                <div class="col-md-5 product-information-description" tabindex="0">
	                                	<%-- display item details section --%>
	                                	<dsp:include page="/cart/displayItemDetailsFrag.jsp">
	                                		<dsp:param name="item" param="item" />
	                                		<dsp:param name="pageName" param="pageName" />
	                                		<dsp:param name="existingRelationShipId" value="${existingRelationShipId}" />
											<dsp:param name="inlineMessage" value="${inlineMessage}" />
	                                	</dsp:include>
	                                	<%-- To display the Gift Wrap Name --%>
	                                	<dsp:droplet name="IsEmpty">
											<dsp:param name="value" param="item.wrapDisplayName" />
											<dsp:oparam name="false">
			                                	<div class="checkout-item-gift-wrapped">
													<img src="${TRUImagePath}images/item-gift-wrapped.png" alt="Image could not be loaded">
													<div><fmt:message key="checkout.this.gift.with"/>&nbsp;<dsp:valueof param="item.wrapDisplayName"/>&nbsp;<fmt:message key="checkout.priceSummary.giftWrap"/></div>
												</div>
											</dsp:oparam>
										</dsp:droplet>
	                                	<dsp:droplet name="IsEmpty">
											<dsp:param name="value" value="${pageName}" />
											<dsp:oparam name="false">
			                                	<c:if test="${(pageName ne 'Confirm') and (pageName ne 'store-pickup')}">
				                                    <div class="edit">
				                                    	<%-- edit item in cart --%>
														<dsp:include page="/cart/editItemFrag.jsp">
															<dsp:param name="item" param="item" />
															<dsp:param name="pageName" param="pageName" />
														</dsp:include>
					                                    <span>.</span>
					                                    <dsp:include page="/cart/removeItemFrag.jsp">
															<dsp:param name="item" param="item" />
															<dsp:param name="pageName" param="pageName" />
														</dsp:include>
														<!-- <a class="your-store store-locator-tooltip pull-right" href="javascript:void(0);" data-original-title="" title="">change your store</a> -->
														
				                                    </div>
				                                    
			                                    </c:if>
											</dsp:oparam>
										</dsp:droplet>
	                          
	                            	<%-- Store locatore code here --%>
	                            
	                                    <p></p>
	                                </div>	    
	                                <c:choose>
	                                	<c:when test="${(shippingGroupType eq 'inStorePickupShippingGroup') or (shippingGroupType eq 'channelInStorePickupShippingGroup')}">
	                                		<div class="pickup-step pull-right">
	                                		<dsp:valueof param="item.warehouseMessage" valueishtml="true"/>
											<span class="divider">.</span><a data-target="pickup-see-terms-popover" class="pickup-see-terms-popover-class"><fmt:message key="checkout.pickup.seeTerms" /></a>
											<div class="popover storePickupSeeTerms" role="tooltip">
												<div class="arrow"></div>
												<h3><fmt:message key="checkout.pickup.seeterms.header"/></h3>
												<p><fmt:message key="checkout.pickup.seeterms.details"/></p>
											</div>
											</div>
											<br>
											<c:if test="${pageName ne 'Confirm'}">
												<div class="pickup-step pull-right clearRight">
													<%-- <a><fmt:message key="checkout.pickup.storeChange" /></a> --%>
													<a data-toggle="modal" data-target="#findInStoreModal" href="javascript: void(0);"
																		onclick="javascript: return loadFindInStore('${contextPath}', '<dsp:valueof param="item.skuId"/>', '<dsp:valueof param="item.id"/>','<dsp:valueof param="item.quantity"/>',this);"><fmt:message key="checkout.pickup.storeChange" /></a>
													
												</div>
											</c:if>
	                                	</c:when>
	                                	<%-- <c:otherwise>
	                                		<c:if test="${(pageName ne 'Review') and (pageName ne 'Confirm')}">
		                                		<div class="col-md-5">
				                                    <div class="days-until-ready">
				                                    	<span><fmt:message key="checkout.review.daysUntilReady" /> 5 - 10 days</span>
				                                    	<span>.</span><a href="${contextPath}checkout/review/orderReview.jsp#" data-target="pickup-info-popover" data-original-title="" title=""><fmt:message key="checkout.review.seeTerms" /></a>
				                                    </div>
				                                </div>
				                        	</c:if>		                                			                            
	                                	</c:otherwise> --%>
	                                </c:choose>
                                </div>
                            </div>
                        </div>
                    </div>
                    <dsp:include page="/cart/buyersProtectionPlan.jsp">
						<dsp:param name="item" param="item"/>
						<dsp:param name="pageName" param="pageName"/>
					</dsp:include>
                   
</dsp:page>				