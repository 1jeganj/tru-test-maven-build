<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
	<dsp:getvalueof var="shippingImageSizeMap" bean="TRUStoreConfiguration.shippingImageSizeMap" />
	<c:forEach var="entry" items="${shippingImageSizeMap}">
		<c:if test="${entry.key eq 'height'}">
			<c:set var="imageHeight" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'width'}">
			<c:set var="imageWidth" value="${entry.value}" />
		</c:if>
	</c:forEach>
     <dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:getvalueof var="pageName" param="pageName" />
	<div class="checkout-review-product-block partial">
       <div class="product-information" tabindex="0">
           <div class="product-information-header">
               <div class="product-header-title"><dsp:valueof param="donationItem.donationItemDisplayName"/></div>
               <div class="product-header-price">
                   <dsp:valueof param="donationItem.donationAmount" converter="currency"/>
               </div>
           </div>
           <div class="product-information-content">
               <div class="row row-no-padding">
                   <div class="col-md-2">
                       <div>
                         <dsp:getvalueof var="donationImage" param="donationItem.donationItemImageUrl"/>
                         	<dsp:droplet name="IsEmpty">
                   			<dsp:param name="value" value="${donationImage}" />
                   			<dsp:oparam name="false">
                   				<dsp:droplet
									name="/com/tru/common/droplet/TRUAkamaiImageDroplet">
									<dsp:param name="imageName" value="${donationImage}" />
									<dsp:param name="imageHeight" value="${imageHeight}" />
									<dsp:param name="imageWidth" value="${imageWidth}" />
									<dsp:oparam name="output">
										<dsp:getvalueof var="akamaiImageUrl" param="akamaiUrl" />
									</dsp:oparam>
								</dsp:droplet>
								<img class="product-description-image" src="${akamaiImageUrl}" alt="product_image">
                   			</dsp:oparam>
                   			<dsp:oparam name="true">
                   				<img class="product-description-image image-not-available" src="/images/no-image500.gif" alt="Product Image">
                   			</dsp:oparam>
                   		</dsp:droplet>
                       </div>
                   </div>
                  <%--  <div class="col-md-6 donation-amount"><dsp:valueof param="donationItem.donationAmount" converter="currency"/></div> --%>
                   <div class="col-md-5 product-information-description" tabindex="0">
	                   	<fmt:message key="checkout.review.donationThankYouMessage"/>
	                   	<c:if test="${pageName ne 'Confirm'}">
		                   	<div class="edit">
		                       	<%-- edit item in cart --%>
							<%-- 	<dsp:include page="/cart/editItemFrag.jsp">
									<dsp:param name="item" param="donationItem" />
									<dsp:param name="pageName" param="pageName" />
								</dsp:include>
		                        <span>.</span> --%>
		                        <%-- <dsp:include page="/cart/removeItemFrag.jsp">
									<dsp:param name="item" param="donationItem" />
									<dsp:param name="pageName" param="pageName" />
									<dsp:param name="donationItem" value="true"/>
								</dsp:include> --%>
								<dsp:getvalueof var="commerceItemId" param="donationItem.commerceItemId"></dsp:getvalueof>
								<a href="javascript:void(0);" class="remove-donation-item-review" id="${commerceItemId}">
								<%-- <a href="#" onclick="javascript: return removeDonationItemFromReview('${commerceItemId}');"> --%><fmt:message key="checkout.review.remove"/></a>
		                   	</div>
		                </c:if>
                   </div>
               </div>
           </div>
       </div>
    </div>
</dsp:page>