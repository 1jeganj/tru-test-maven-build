<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:getvalueof var="taxPageName" param="taxPageName"></dsp:getvalueof>
 		<p id="checkout-sales-tax-estimation" class="${taxPageName}">
			 <fmt:message key="myaccount.asterisk"/>
			 <span>sales tax is estimated, actual sales tax will be calculated at the time of shipment.</span>
		</p>
</dsp:page>