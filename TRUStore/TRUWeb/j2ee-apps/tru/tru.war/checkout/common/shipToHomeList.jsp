<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
 <dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
 <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
 <dsp:importbean bean="/atg/commerce/ShoppingCart" />
 
 <dsp:getvalueof var="pgName" param="pageName"/> 
 <dsp:include page="displayShippingAddress.jsp"><dsp:param name="shippingGroups" param="order.shippingGroups" /><dsp:param name="pageName" param="pageName"/></dsp:include>
		 <dsp:droplet name="IsEmpty">
			   <dsp:param name="value" bean="ShoppingCart.current.commerceItems"/>
			   <dsp:oparam name="false">
			        <dsp:droplet name="ForEach">
	        			<dsp:param name="array" param="order.commerceItems" />
		    			 <dsp:param name="elementName" value="item" /> 
            			 <dsp:oparam name="output">  
                				<dsp:include page="displayItemDetail.jsp"><dsp:param name="item" param="item" /></dsp:include>
              			</dsp:oparam>
              			<dsp:oparam name="empty">
           			
              			</dsp:oparam>  
            		</dsp:droplet>
			   </dsp:oparam>
			   <dsp:oparam name="true">
			   
			   </dsp:oparam>
		</dsp:droplet>        
</dsp:page>