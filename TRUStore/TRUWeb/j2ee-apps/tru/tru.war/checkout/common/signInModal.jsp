<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:getvalueof var="customerID" bean="Profile.id"/>
    <dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
	<c:if test="${site eq babySiteCode}">
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
	</c:if>
	  <div class="modal fade" id="signInModal" tabindex="-1" role="dialog"
			aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog modal-md">
				<div class="modal-content sharp-border">
					<div class="sign-in-overlay">
						<div class="sign-in-header">
							<div>
								<h2><fmt:message key="myaccount.signin"/></h2>
							</div>
							<div><a href="javascript:void(0)" data-dismiss="modal">
									<span class="sprite-icon-x-close"></span>
								</a>
							</div>
						</div>
						<div>
						<div class="errorDisplay"></div>
							<div class="sign-in-form">
							<form class="JSValidation" id="hearderLoginOverlay">
								<p class="global-error-display"></p>
								<div>
									<label for="login"><fmt:message key="checkout.header.help.email"/></label>
									<input name="email" id="login" type="text" maxlength="60" />
								</div>
								<div>
									<label for="sign-in-password-input"><fmt:message key="shoppingcart.password"/></label>
									<input name="password" id="sign-in-password-input" maxlength="50" type="password" />
								</div>
								<div>
								    <dsp:getvalueof var="requestPage" param="requestPage"/>
								    <input type="hidden" id="requestPage" value="${requestPage}">
									<!-- <button>Sign In</button> -->
									 <input type="submit" value="Sign In" class="submit-btn" id="signin-submit-btn"/>  
									<a data-target="#forgotPasswordModel" data-toggle="modal" href="javascript:void(0);"  class="reset-password-flink inline-block"><fmt:message key="shoppingcart.forgot.password"/></a>
								<input type="hidden" id="cartSign-custId" value="${customerID}"/>
					            <input type="hidden" id="cartSign-partnerName" value="${partnerName}"/>
								
								
								</div>
							</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
</dsp:page>