<fmt:setBundle
	basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
<dsp:importbean bean="/atg/multisite/Site"/>
<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
    <dsp:getvalueof var="enableTriad" bean="Site.enableTriad"/>
	<dsp:getvalueof var="cssClassName" param="cssClassName"></dsp:getvalueof>
	<dsp:getvalueof var="site" bean="Site.id"/>
	<dsp:droplet name="GetSiteTypeDroplet">
		<dsp:param name="siteId" value="${site}" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="siteforsos" param="site" />
			<c:set var="siteforsos" value="${siteforsos}" scope="request" />
		</dsp:oparam>
	</dsp:droplet>
	<%-- <script>
		$(document).ready(function() {
					var isSOS = $.cookie("isSOS");
					var sites = '<c:out value="${siteforsos}"/>';
					if (typeof isSOS != 'undefined' && isSOS != null && isSOS == 'true') {
						$("#sosAdd_Zone").hide();
					} else {
						$("#sosAdd_Zone").show();
					}
				});
	</script> --%>
	<span id="sosAdd_Zone">
		<c:if test="${enableTriad eq 'true' }"> 
		 <div class="ad-zone-728-90 partial-ad-zone-728-90 ${cssClassName}">
	        <div id="OAS_Position1"></div>
	    </div>
	</c:if> 
    </span>
</dsp:page>