<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:getvalueof var="pageName" param="pageName" />
	<div class="row">
		<div class="col-md-12 items-shipping registry" tabindex="0">
			<div class="">
				<dsp:getvalueof var="itemsCount" param="shippingDestinationVO.itemsCount" />
				${itemsCount}
				<c:choose>
					<c:when test="${itemsCount gt 1}">&#32;<fmt:message key="checkout.review.shippingAddress"/>&#32;</c:when>
					<c:otherwise>&#32;<fmt:message key="checkout.item.shipping.to"/>&#32;</c:otherwise>
				</c:choose>				
				 <dsp:getvalueof param="shippingDestinationVO.shipToName" var="shipToName"></dsp:getvalueof>
		    	 <c:choose>
		    	 	<c:when test="${shipToName eq null }">
		    	 		<dsp:valueof param="shippingDestinationVO.address.firstName"/> &#32;<dsp:valueof param="shippingDestinationVO.address.lastName"/> 
		    	 	</c:when>
		    	 	<c:otherwise>
		    	 		<dsp:valueof value="${shipToName}"/>
		    	 	</c:otherwise>
		    	 </c:choose>			    	 
			</div>
			<%-- Start PayPal integration changes --%>
			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="ShoppingCart.current.paymentGroups" />
				<dsp:param name="elementName" value="paymentGroup" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="pgType" param="paymentGroup.paymentGroupClassType" />
					<c:choose>
						<c:when test="${pgType eq 'payPal'}">
							<dsp:getvalueof var="isPayPalPayment" value="true" />
						</c:when>
					</c:choose>
				</dsp:oparam>
			</dsp:droplet>
		   <%-- End PayPal integration changes --%>
			<div class="items-shipping-address">
				<dsp:getvalueof var="isRegistryAddress" vartype="java.lang.boolean" param="shippingDestinationVO.registryAddress" />
				<c:choose>
					<c:when test="${isRegistryAddress}">
						<c:choose>
							<c:when test="${pageName eq 'Review' or pageName eq 'Confirm'}">
								<span class="show-shipping-address">
									<dsp:valueof param="shippingDestinationVO.address.city" />,&#32;
									<dsp:valueof param="shippingDestinationVO.address.state" />&#32;
									<%-- <dsp:valueof param="shippingDestinationVO.address.postalCode" />&#32; --%>
								</span>
							</c:when>
							<c:otherwise>
								<span class="sub-header"><fmt:message key="checkout.address.provided.registrant"/></span>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<span class="show-shipping-address"><dsp:valueof param="shippingDestinationVO.address.address1" />&#32;
						<dsp:valueof param="shippingDestinationVO.address.address2" />&#32;
						<dsp:valueof param="shippingDestinationVO.address.city" />,&#32;
						<dsp:valueof param="shippingDestinationVO.address.state" />&#32;
						<dsp:valueof param="shippingDestinationVO.address.postalCode" />
						<%-- START: Added For Tealium integration --%>
						
						<dsp:getvalueof var="customerCity" param="shippingDestinationVO.address.city" scope="request"/>
						<dsp:getvalueof var="customerState" param="shippingDestinationVO.address.state" scope="request"/>
						<dsp:getvalueof var="customerZip" param="shippingDestinationVO.address.postalCode" scope="request"/>
						<dsp:getvalueof var="customerCountry" param="shippingDestinationVO.address.country" scope="request"/>
						
						<%-- End: Added For Tealium integration --%>
						</span>
					</c:otherwise>
				</c:choose>
				<dsp:getvalueof var="shipCountry" param="shippingDestinationVO.address.country"/>
				<c:set var="tShippingCountry" value="${shipCountry}" scope="request"/>
				<c:if test="${pageName eq 'Review' }">
					<div class="edit inline">
						<span>.</span>
						<c:choose>
							<c:when test="${isPayPalPayment}">			
								<a href="javascript:void(0);" onclick="continueToCheckout('shipping-tab');"><fmt:message key="checkout.review.edit" /></a>
							</c:when>
							<c:otherwise>
								<a href="javascript:void(0);" onclick="continueToCheckout('shipping-tab');"><fmt:message key="checkout.review.edit" /></a>
							</c:otherwise>
						</c:choose>
					</div>
				</c:if>
			</div>
		</div>
	</div>
</dsp:page>				