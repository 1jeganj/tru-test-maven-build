<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler" />
<dsp:page>
	<dsp:setvalue bean="ShippingGroupFormHandler.shipMethodVO.shipMethodCode" paramvalue="shipMethodCode"/>
	<dsp:setvalue bean="ShippingGroupFormHandler.shipMethodVO.shippingPrice" paramvalue="shippingPrice"/>
	<dsp:setvalue bean="ShippingGroupFormHandler.shipMethodVO.regionCode" paramvalue="regionCode"/>
	<dsp:setvalue bean="ShippingGroupFormHandler.changeShipMethod" value="true"/>
</dsp:page>