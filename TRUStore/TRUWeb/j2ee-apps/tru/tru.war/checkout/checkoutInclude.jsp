<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />  
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/com/tru/commerce/cart/droplet/OrderSummaryDetailsDroplet" />
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler" />
	<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler" />
	<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
	<dsp:importbean bean="/com/tru/radial/common/TRURadialConfiguration"/>
	<dsp:importbean bean="/com/tru/common/TRUIntegrationConfiguration" />
	<dsp:importbean bean="/com/tru/common/TRUSOSIntegrationConfiguration" />
	<dsp:importbean bean="/com/tru/common/droplet/TRUUrlConstructDroplet"/>
	
	<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
	<dsp:getvalueof var="enableVerboseDebug" bean="TRURadialConfiguration.enableVerboseDebug"/>
	<dsp:getvalueof var="enableIntegrationLog" bean="TRURadialConfiguration.enableIntegrationLog"/>
	<dsp:getvalueof var="maxNonceRetryAttempts" bean="TRURadialConfiguration.maxNonceRetryAttempts"/>
	<dsp:getvalueof var="maxGetTokenRetryAttempts" bean="TRURadialConfiguration.maxGetTokenRetryAttempts"/>
	<dsp:getvalueof var="maxTokenizeAndAuthorizeRetryAttempts" bean="TRURadialConfiguration.maxTokenizeAndAuthorizeRetryAttempts"/>
	<dsp:getvalueof var="maxAuthorizeSavedTokenRetryAttempts" bean="TRURadialConfiguration.maxAuthorizeSavedTokenRetryAttempts"/>
	<dsp:getvalueof var="storeCardinal" bean="TRUIntegrationConfiguration.enableCardinal"/>
	<dsp:getvalueof var="sosCardinal" bean="TRUSOSIntegrationConfiguration.enableCardinal"/>
	<input type="hidden" id="pushSiteId" value="${site}"/>

	<dsp:droplet name="TRUUrlConstructDroplet">
			<dsp:param name="integrationLoggingParam" value="integrationLogging"/>
			<dsp:oparam name="output">
				<dsp:getvalueof var="truIntegrationLoggingRestAPIURL" param='integrationLoggingURL'/>
				<input type="hidden" id="truIntegrationLoggingRestAPIURL" value="${truIntegrationLoggingRestAPIURL}">
			</dsp:oparam>
	</dsp:droplet>

	<dsp:droplet name="GetSiteTypeDroplet">
		<dsp:param name="siteId" value="${site}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="site" param="site"/>
			<c:set var="site" value ="${site}" scope="request"/>
		</dsp:oparam>
	</dsp:droplet>
	<c:choose>
		<c:when test="${site eq 'sos'}">
			<dsp:getvalueof var="cardinalEnabled" value="${sosCardinal}"/>
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="cardinalEnabled" value="${storeCardinal}"/>
		</c:otherwise>
	</c:choose>
	
	<%-- Start - Writing hidden input field for expiry month & year for PLCC card --%>
	<dsp:getvalueof var="truConf" bean="TRUConfiguration" />
	<input type="hidden" name="expMonthForPLCC" id="expMonthForPLCC" value="${truConf.expMonthAdjustmentForPLCC}"/>
	<input type="hidden" name="expYearForPLCC" id="expYearForPLCC" value="${truConf.expYearAdjustmentForPLCC}"/>
	<%-- End - Writing hidden input field for expiry month & year for PLCC card --%>
	
	<%-- Start - Writing hidden input field for inAuth api key and url --%>
	<input type="hidden" name="inAuthURL" id="inAuthURL" value="${truConf.inAuthURL}"/>
	<input type="hidden" name="inAuthKey" id="inAuthKey" value="${truConf.inAuthKey}"/>
	<%-- End - Writing hidden input field for inAuth api key and url --%>
	<dsp:getvalueof var="siteCode" value="TRU" />
	<dsp:getvalueof var="baseBreadcrumb" value="trubru" />

	 <input type="hidden" value='<dsp:valueof bean="TRURadialConfiguration.apiUserName" />' id="payPalApiUserName" />
	 <input type="hidden" value='<dsp:valueof bean="TRURadialConfiguration.payPalEnvironment" />' id="payPalhiddenContextPath" />
	 <input type="hidden" value='<dsp:valueof bean="TRURadialConfiguration.paypalRedirectURL" />' id="paypalRedirectURL" />
	 
	
	<input type="hidden" name="paymentMode" id="paymentMode" value=""/>
	<dsp:getvalueof var="turnOffLeaveWarning" bean="/atg/multisite/Site.turnOffLeaveWarning" />
	<input type="hidden" name="turnOffLeaveWarning" id="turnOffLeaveWarning" value="${turnOffLeaveWarning}"/>
	<%-- Start :PayPal Set Up Initialization code --%>
	<div id="payPalPaymentInitialDiv" class="truPayPal-div-hidden">
	  <dsp:a href="" bean="BillingFormHandler.checkoutWithPayPal" id="paypalPaymentExpresscheckoutId" value="expressCheckoutWithPayPal" />
	</div>
	<div id="payPalReviewPageInitialDiv" class="truPayPal-div-hidden">
		<dsp:a href="" bean="CommitOrderFormHandler.checkoutWithPayPalFromOrderReview" id="paypalReviewPageExpresscheckoutId" value="expressCheckoutWithPayPal"/>
	</div>
	<script>
		var isCardinalEnabled = ${cardinalEnabled};
		var enableVerboseDebug = ${enableVerboseDebug};
		var enableIntegrationLog = ${enableIntegrationLog};
		var maxNonceRetryAttempts = ${maxNonceRetryAttempts};
		var maxGetTokenRetryAttempts = ${maxGetTokenRetryAttempts};
		var maxTokenizeAndAuthorizeRetryAttempts = ${maxTokenizeAndAuthorizeRetryAttempts};
		var maxAuthorizeSavedTokenRetryAttempts = ${maxAuthorizeSavedTokenRetryAttempts};
		console.log("isCardinalEnabled: "+isCardinalEnabled);
		console.log("enableVerboseDebug: "+enableVerboseDebug);
		console.log("enableIntegrationLog: "+enableIntegrationLog);
		console.log("maxNonceRetryAttempts: "+maxNonceRetryAttempts);
		console.log("maxGetTokenRetryAttempts: "+maxGetTokenRetryAttempts);
		console.log("maxTokenizeAndAuthorizeRetryAttempts: "+maxTokenizeAndAuthorizeRetryAttempts);
		console.log("maxAuthorizeSavedTokenRetryAttempts: "+maxAuthorizeSavedTokenRetryAttempts);
	</script>
	<script type="text/javascript">
	    window.paypalCheckoutReady = function() {
	        paypal.checkout.setup('<dsp:valueof bean="TRURadialConfiguration.apiUserName" />', {
	        environment: '<dsp:valueof bean="TRURadialConfiguration.payPalEnvironment" />',
	        button: ['paypalPaymentExpresscheckoutId','paypalReviewPageExpresscheckoutId']
	      });
	    }		
		</script>
	<%-- End :PayPal Set Up Initialization code --%>
	
 	<%-- Moved Checkout Progress bar to root page and removed from checkoutPageContainer--%>
	<dsp:setvalue bean="ShoppingCart.current.previousTab" value=""/>
	<dsp:include page="/jstemplate/resourceBundle.jsp" />
	  <div class="checkout-nav">
	       <dsp:include page="./common/checkoutHeader.jsp"/>
	       <div id="checkout-progress-container">
		      <dsp:include page="./common/checkoutProgress.jsp">
		      	<dsp:param name="pageName" param="pageName"/>
		      </dsp:include>
	       </div>
	   </div>
	   
		<dsp:getvalueof var="orderId" bean="ShoppingCart.current.id"/>
	 	<%-- <dsp:getvalueof var="orderAmount" bean="ShoppingCart.current.priceInfo.total"/>	  --%>
	 	<dsp:getvalueof var="currencyCode" bean="TRUConfiguration.currencyCode"/>
	 	<dsp:getvalueof var="profileId" bean="Profile.id"/>
		<input type="hidden" name="cardianlOrderId" id="cardianlOrderId" value="${orderId}"/>
		<input type="hidden" name="currencyCode" id="currencyCode" value="${currencyCode}"/>
		<input type="hidden" name="profileId" id="profileId" value="${profileId}"/>
		<dsp:getvalueof var="currentTab" bean="ShoppingCart.current.currentTab" />
		<dsp:getvalueof var="currentTabURL" bean="TRUConfiguration.checkoutTabUrls.${currentTab}" />
			
			<script async src="//www.paypalobjects.com/api/checkout.js"></script>
			<%-- If there is no items in the cart Redirecting back to cart page --%>
			<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
			<dsp:droplet name="OrderSummaryDetailsDroplet">
				<dsp:param name="order" bean="ShoppingCart.current" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="commerceItemCount" param="itemCount" />
					<c:if test="${empty commerceItemCount || commerceItemCount eq 0 }">
						<dsp:droplet name="Redirect">
							<dsp:param name="url" value="${contextPath}cart/shoppingCart.jsp" />
						</dsp:droplet>
					</c:if>
				</dsp:oparam>
			</dsp:droplet>
			<div class="default-margin">
				<div class="container-fluid default-template checkout-template min-width checkout-payment-tab-template checkout-payment-template" id="checkout-container">
				<div id="currentTab" style="display:none">${currentTab}</div>					
				<dsp:include page="${currentTabURL}">
					<c:choose>
						<c:when test="${currentTab eq 'pickup-tab'}">
							<dsp:param name="pageName" value="Store-Pickup" />
						</c:when>
						<c:when test="${currentTab eq 'shipping-tab'}">
							<dsp:param name="pageName" value="Shipping" />
						</c:when>
					</c:choose>
					<dsp:param name="init" value="true"/>
				</dsp:include>
				</div>
			</div>
			<dsp:include page="common/tealiumForShipping.jsp"/>
</dsp:page>