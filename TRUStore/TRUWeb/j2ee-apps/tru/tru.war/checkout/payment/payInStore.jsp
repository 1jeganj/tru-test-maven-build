<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:getvalueof var="isPayInStoreEligible" param="isPayInStoreEligible" vartype="java.lang.boolean" />
	<dsp:getvalueof var="isPayInStoreSelected" param="isPayInStoreSelected" vartype="java.lang.boolean" />
	
	<div class="pay-in-store">
		<c:choose>
			<c:when test="${isPayInStoreEligible}">
				<c:choose>
					<c:when test="${isPayInStoreSelected}">
						<input id="radio-payinstore" name="payment-type" tabindex="0" checked="checked" class="inline" type="radio" value="payInStore" aria-label="paymentMode" />
					</c:when>
					<c:otherwise>
						<input id="radio-payinstore" name="payment-type" tabindex="0" class="inline" type="radio" value="payInStore" aria-label="paymentMode"/>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<input id="radio-payinstore" name="payment-type" tabindex="0" disabled="disabled" class="inline" type="radio" value="payInStore" />
				<c:set var="PayInStoreDisabled" value="preShow"/> 
			</c:otherwise>
		</c:choose>
		<label for="radio-payinstore"><fmt:message	key="checkout.payment.payInStore" /></label> <span>&#183;</span>
		 <a href="JavaScript:payInStoreDetailWindow('http://www.toysrus.com/helpdesk/index.jsp?display=payment&amp;index=0&amp;subdisplay=options#pin');">
       		<fmt:message key="checkout.payment.details" />
         </a>
         <span id="payinstoreDisabled" class="payInStore-disabled-message ${PayInStoreDisabled}"><fmt:message	key="checkout.payment.payInStore.disabledMessage" /></span>
	</div>
</dsp:page>