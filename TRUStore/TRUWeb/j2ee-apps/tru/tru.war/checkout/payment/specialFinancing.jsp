<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/com/tru/common/TRUUserSession" />
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:getvalueof var="order" bean="ShoppingCart.current" />
<dsp:getvalueof var="eligibleForFinancing" value="${order.orderIsEligibleForFinancing}" />
<dsp:getvalueof var="financingTermsAvailable" value="${order.financingTermsAvailable}" />
<dsp:getvalueof var="rusCardSelectedCheck" bean="TRUUserSession.rusCardSelected"/>
<dsp:getvalueof var="details" bean="TRUConfiguration.detailsURL"/>
<%-- MVP#1 --%>
<dsp:getvalueof var="noFinanceAgreed" value="false" />
<dsp:getvalueof var="isCreditCardSelected" param="isCreditCardSelected" />
<dsp:getvalueof var="creditCardNumber" param ="creditCardNumber" />
<dsp:getvalueof var="cardType" param ="cardType" />
<dsp:getvalueof var="financeAgreed" value="0"/>
<c:if test="${cardType eq 'credit-card' and rusCardSelectedCheck eq true}">
	<c:set  var="creditCardNumber" value=""/>
	<c:set var="isCreditCardSelected" value="false"/>
</c:if>

<dsp:setvalue param="activeAgreementsVO" value="${order.activeAgreementsVO}" />
<dsp:droplet name="IsEmpty">
	<dsp:param name="value" param="activeAgreementsVO.${creditCardNumber}" />
	<dsp:oparam name="false">
		<dsp:getvalueof var="financeAgreed" param="activeAgreementsVO.${creditCardNumber}.financeAgreed" vartype="java.lang.Interger"/>
	</dsp:oparam>
</dsp:droplet>

<div class="promo-finance-${cardType}" 
<c:choose>
<c:when test="${!(isCreditCardSelected) and !(rusCardSelectedCheck)}">style="display:block;"</c:when>
<c:when test="${cardType eq 'credit-card' and isCreditCardSelected}">style="display:block;"</c:when>
<c:when test="${cardType eq 'rus-credit-card' and rusCardSelectedCheck}">style="display:block;"</c:when>
<c:otherwise>style="display:none;"</c:otherwise>
</c:choose>>
	<div class="six-month-financing ${cardType}"
		<c:choose>
			<c:when test="${cardType eq 'rus-credit-card' and rusCardSelectedCheck eq false}">style="display:none;"</c:when>
			<c:when test="${cardType eq 'rus-credit-card' and eligibleForFinancing eq 'false'}">style="display:none;"</c:when> 
			<c:when test="${cardType eq 'credit-card' and eligibleForFinancing eq 'false'}">style="display:none;"</c:when> 
			<c:when test="${cardType eq 'credit-card' and rusCardSelectedCheck eq true and eligibleForFinancing eq 'true'}">style="display:block;"</c:when> 
			<c:when test="${isCreditCardSelected eq 'true' and eligibleForFinancing eq 'true'}">style="display:block;"</c:when> 
		</c:choose> >
		<input type="hidden" id="financeAlreadyAgreed" value="${financeAgreed}"/>
		<hr class="horizontal-divider">
	    <img class="inline" src="${TRUImagePath}images/Payment_Small-Open-State.jpg" alt="Payment_Small-Open-State">
	    <div class="inline">
	        <div class="padded-financing">
	            <div>
	                <fmt:message key="this.purchase.qualifies.for" />${financingTermsAvailable}<fmt:message key="months.special.financing" />
	            </div>
	            <fmt:message key="if.use.rus.card.message1" />${financingTermsAvailable}<fmt:message key="if.use.rus.card.message2" />
	            <span><a href="javascript:void(0);" data-toggle="modal" data-target="#paymentDetailsPopup"><fmt:message key="special.financing.details.brace"/></a></span>
	            <span>&#xb7;</span> 
	            <a href="javascript:void(0);" class="learn-more-credit-card" data-target="pickup-info-popover" data-original-title="" title=""><fmt:message key="special.financing.learn.more" /></a>
	            <span>&#xb7;</span><a href="#" data-toggle="modal" data-target="#payment-page-apply-today"><fmt:message key="special.financing.apply.today" /></a>
	            <div class='finacingActionDiv no-padding <c:if test="${cardType eq 'credit-card' and (creditCardNumber eq '' or empty creditCardNumber)}">finaceGrayedOut</c:if>'>
		            <div>
		                <fmt:message key="special.financing.would.like.message" />
		            </div>
		            <p class="yes-six-month-financing">
		                <input id="yes-six-month-financing" class="shipping-radio-btn inline" type="radio" 
		                <c:if test="${cardType eq 'credit-card' and creditCardNumber eq ''}">disabled="disabled"</c:if> 
		                <c:if test="${financeAgreed eq 1}">checked="true"</c:if> 
		                name="shipping-type-${cardType}" value="${financingTermsAvailable}">
		                <input type="hidden" id="disclosure-agreement-status" value="${financeAgreed}" />
		                <label for="yes-six-month-financing">
		                    <fmt:message key="special.financing.yes.message1" />${financingTermsAvailable}<fmt:message key="special.financing.yes.message2" />
		                </label>
		            </p>
		            <p id="electronicTermsNotify" class="display-none">
						<strong><fmt:message key="special.financing.must.agree"/></strong>
						<a href="javascript:void(0);" id="yes-financing-continueGE"><fmt:message key="special.financing.continue.txt"/></a>
					</p>
					<p class="disclosure-agreed" <c:if test="${financeAgreed ne 1}">style="display:none;"</c:if>>
		            	<img src="${TRUImagePath}images/checkbox-sm-on.jpg" />
		            	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="special.financing.disclosure.aggreed" />
		            </p>
		            <p class="no-six-month-financing">
		                <input id="no-six-month-financing" class="shipping-radio-btn inline" type="radio" 
		                <c:if test="${cardType eq 'credit-card' and creditCardNumber eq ''}">disabled="disabled"</c:if> 
		                <c:if test="${financeAgreed eq 2}">checked="true"</c:if> 
		                name="shipping-type-${cardType}" value="${financingTermsAvailable}">
		                <label for="no-six-month-financing">
		                    <fmt:message key="special.financing.no.message1" />${financingTermsAvailable}<fmt:message key="special.financing.no.message2" />
		                </label>
		
		            </p>
		        </div>
	            <fmt:message key="special.financing.footer.note1" /><fmt:message key="special.financing.footer.note2" />
	        </div>
	    </div>
	</div>
	</div>
</dsp:page>