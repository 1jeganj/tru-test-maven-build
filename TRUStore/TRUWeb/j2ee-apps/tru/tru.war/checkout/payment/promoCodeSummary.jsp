<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>	
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
	<dsp:importbean bean="/atg/commerce/promotion/CouponFormHandler" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:getvalueof var="appliedPromoNames" bean="ShoppingCart.current.appliedPromoNames"/>
	<dsp:getvalueof var="notAppliedPromoNames" bean="ShoppingCart.current.notAppliedPromoNames"/>
	<dsp:getvalueof var="notAppliedCouponCodes" bean="ShoppingCart.current.notAppliedCouponCode"/>
	<div class="promo-code-table">
		<table>
			<tbody>
				<dsp:droplet name="ForEach">
					<dsp:param name="array" bean="Profile.activePromotions" />
					<dsp:param name="elementName" value="promotionstatus" />
					<dsp:getvalueof var="promotionDetails"
						param="promotionstatus.promotion.promotionDetails" />
						<dsp:getvalueof var="promotionName" param="promotionstatus.promotion.displayName"/>
					<dsp:oparam name="output">
						<dsp:droplet name="ForEach">
							<dsp:param name="array" param="promotionstatus.coupons" />
							<dsp:param name="elementName" value="coupon" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="couponId" param="coupon.id" />
								<dsp:getvalueof var="appliedCoupons" bean="ShoppingCart.current.singleUseCoupon"/>
								<c:if test="${appliedCoupons.containsValue(couponId) and appliedPromoNames.containsValue(promotionName)}">
									<tr>
										<td>${promotionName}</td>
										<td><fmt:message key="checkout.payment.applied" /></td>
										<td>
											<a class="pull-right remove-promo-code-payment-page" href="javascript:void(0);">
												<fmt:message key="checkout.payment.remove" />
											</a>
											<input type="hidden" value="${couponId}" class="couponId" />
										</td>
									</tr>
									</c:if>
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
				</dsp:droplet>
				<dsp:droplet name="ForEach">
					<dsp:param name="array" bean="Profile.activePromotions" />
					<dsp:param name="elementName" value="promotionstatus" />
					<dsp:getvalueof var="promotionName" param="promotionstatus.promotion.displayName"/>
					<dsp:oparam name="output">
						<dsp:droplet name="ForEach">
							<dsp:param name="array" param="promotionstatus.coupons" />
							<dsp:param name="elementName" value="coupon" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="couponID" param="coupon.id" />
								<c:choose>
									<c:when test="${notAppliedCouponCodes.containsValue(couponID)}">
										<tr>
											<td><dsp:valueof value="${promotionName}"/></td>
											<td><fmt:message key="checkout.payment.not.applied"/></td>
											<td>
											<a class="pull-right remove-promo-code-payment-page" href="javascript:void(0);">
												<fmt:message key="checkout.payment.remove" />
											</a>
											<input type="hidden" value="${couponID}" class="couponId" />
											</td>
										</tr>
									</c:when>
									<c:when test="${notAppliedPromoNames.containsValue(promotionName)}">
										<tr>
											<td><dsp:valueof value="${promotionName}"/></td>
											<td><fmt:message key="checkout.payment.not.applied"/></td>
											<td>
											<a class="pull-right remove-promo-code-payment-page" href="javascript:void(0);">
												<fmt:message key="checkout.payment.remove" />
											</a>
											<input type="hidden" value="${couponID}" class="couponId" />
											</td>
										</tr>
									</c:when>
								</c:choose>
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
				</dsp:droplet>
			</tbody>
		</table>
	</div>
</dsp:page>