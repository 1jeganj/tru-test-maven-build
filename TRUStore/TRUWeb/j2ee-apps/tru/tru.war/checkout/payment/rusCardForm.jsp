<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
	<dsp:importbean bean="/com/tru/common/TRUUserSession" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUSynchronyCardHolderDroplet"/>
	
	<dsp:getvalueof var="truConf" bean="TRUConfiguration" />
	<dsp:getvalueof var="order" bean="ShoppingCart.current" />
	<dsp:getvalueof var="hasRUSCard" bean="TRUUserSession.tSPCardInfoMap.hasRUSCard" />
	<dsp:getvalueof var="creditCardBillingAddress" bean="TRUUserSession.tSPBillingAddressMap" />
	<dsp:getvalueof var="billingSameAsShippingAddress" bean="TRUUserSession.billingAddressSameAsShippingAddress"/>
	<dsp:getvalueof var="tSPEditBillingAddress" bean="TRUUserSession.tSPEditBillingAddress"/>
	<dsp:getvalueof var="rusCardSelectedCheck" bean="TRUUserSession.rusCardSelected"/>
	<dsp:getvalueof var="rusCardSelected" value="${order.rusCardSelected}" />
	<dsp:getvalueof var="synchronySDPResponseObj" param="synchronySDPResponseObj" />
	<input type="hidden" name="cardinalToken" id="cardinalToken" value=""/>
	<dsp:getvalueof var="loginStatus" bean="Profile.transient" />
	
	<c:choose>
		<c:when test="${empty synchronySDPResponseObj and hasRUSCard eq 'true'}">
			<dsp:getvalueof var="tspCardNumber" bean="TRUUserSession.tSPCardInfoMap.tspCardNumber" />
			<dsp:getvalueof var="tspCvv" bean="TRUUserSession.tSPCardInfoMap.tspCvv" />
			<dsp:getvalueof var="tspExpMonth" bean="TRUUserSession.tSPCardInfoMap.tspExpMonth" />
			<dsp:getvalueof var="tspExpYear" bean="TRUUserSession.tSPCardInfoMap.tspExpYear" />
			<dsp:getvalueof var="isSavedRusCard" value="true"/>
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="tspCardNumber" value="${synchronySDPResponseObj.tempAcctNumber}" />
			<dsp:getvalueof var="tspCvv" value="${synchronySDPResponseObj.CVV}" />
			<dsp:getvalueof var="tspExpMonth" value="${synchronySDPResponseObj.passMonth}" />
			<dsp:getvalueof var="tspExpYear" value="${synchronySDPResponseObj.passYear}" />
			<dsp:getvalueof var="isSavedRusCard" value="false"/>
		</c:otherwise>	
	</c:choose>
	
	<dsp:droplet name="TRUSynchronyCardHolderDroplet">
	<dsp:param name="profile" bean="Profile"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="cardHolderName" param="cardHolderName"/>
		</dsp:oparam>
	</dsp:droplet>	
	
	<div class="row">
		<div class="col-md-7">
			<c:choose>
				<c:when test="${rusCardSelected || rusCardSelectedCheck}">
					<input name="payment-type" id="radio-rus-creditcard" checked="checked" class="inline" type="radio" value="rusCard" aria-label="paymentMode"/>
				</c:when>
				<c:otherwise>
					<input name="payment-type" id="radio-rus-creditcard" class="inline" type="radio" value="rusCard" aria-label="paymentMode"/>
				</c:otherwise>
			</c:choose>
			<header><fmt:message key="checkout.r.us.credit.card"/></header>
		</div>
	</div>
	 <div class="row rus-credit-card-details" <c:if test='${not rusCardSelected and not rusCardSelectedCheck}'>style="display: none;"</c:if>>
		<form id="paymentRusCardForm" class="JSValidation" method="POST" action="${contextPath}checkout/payment/payment.jsp">
			<!-- setting hidden input field values -->
			<input type="hidden" name="respJwt" id="respJwt" value=""/>
			<input type="hidden" value="" name="cBinNum" id="cBinNum" />
			<input type="hidden" value="" name="cLength" id="cLength" />
			<input type="hidden" name="nameOnCard" id="nameOnCard" value="${cardHolderName}" />
			<input type="hidden" name="ccnumber" id="ccnumber" value="${tspCardNumber}" />
			<input type="hidden" name="expirationMonth" id="expirationMonth" value="${tspExpMonth}" />
			<input type="hidden" name="expirationYear" id="expirationYear" value="${tspExpYear}" />
			<input type="hidden" name="creditCardCVV" id="creditCardCVV" value="${tspCvv}" />
			<input type="hidden" name="isSaved_Card" id="isSaved_Card" value="${isSavedRusCard}"/>
			
			
			<input type="hidden" name="isRUSCard" value="true" />
			<input type="hidden" name="orderSet" value="false" />
			
			<div class="col-md-7 rus-credit-card-left-section">
				<div class="row">
					<div class="col-md-6">
						<label><fmt:message key="checkout.payment.cardNumber"/></label>
						<div>
							<c:choose>
								<c:when test="${fn:startsWith(tspCardNumber, truConf.cardNumberPLCC)}">
									<input type="hidden" name="cardType" id="cardType" value="RUSPrivateLabelCard"/>
									<img src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-1.jpg" alt="no-image" id="rus-credit-card-img" />
								</c:when>
								<c:when test="${fn:startsWith(tspCardNumber, truConf.cardNumberRUSCOB)}">
									<input type="hidden" name="cardType" id="cardType" value="RUSCoBrandedMasterCard"/>
									<img src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-2.jpg" alt="no-image" id="rus-credit-card-img" />
								</c:when>
								<c:when test="${fn:startsWith(tspCardNumber, truConf.cardNumberPRCOB)}">
									<input type="hidden" name="cardType" id="cardType" value="RUSCoBrandedMasterCard"/>
									<img src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-2.jpg" alt="no-image" id="rus-credit-card-img" />
								</c:when>
								<c:otherwise>
									<img src="" alt="no-image" id="rus-credit-card-img" />
								</c:otherwise>
							</c:choose>
							
							<span id="rus-temp-card-number">
								<dsp:valueof value="${tspCardNumber}" converter="creditcard" maskCharacter="x"/>
							</span>
						</div>
					</div>
					<div class="col-md-2">
						 <label><fmt:message key="cvv"/></label>
						<div>${tspCvv}</div>
					</div>
					<div class="col-md-4">
						<label><fmt:message key="checkout.payment.expiry"/></label>
						<div>exp ${tspExpMonth}/${tspExpYear}</div>
					</div>
				</div>
				<%-- <div class="rusCardBillingAddressForm">
					<c:if test="${not billingSameAsShippingAddress && not tSPEditBillingAddress}">
						<dsp:include page="/checkout/payment/billingAddressForm.jsp" />
					</c:if>
				</div> --%>
			</div>
			 <%-- <c:if test="${billingSameAsShippingAddress || tSPEditBillingAddress}">
				 <div id="rus-billing-info" >
					<dsp:include page="/checkout/payment/creditCard/displayBillingAddress.jsp">
						<dsp:param name="address" value="${creditCardBillingAddress}" />
						<dsp:param name="hasSavedCards" value="false" />
						<dsp:param name="callingSection" value="rusCard" />
					</dsp:include>
				</div> 
			</c:if>   --%>
		</form>
	</div> 
	<div id="six-month-financing-rus-credit-card-div">
		<dsp:include page="specialFinancing.jsp">
			<dsp:param name="cardType" value="rus-credit-card" />
			<dsp:param name="creditCardNumber" value="tspCardNumber" />
			<dsp:param name="isCreditCardSelected" param="isCreditCardSelected" />
		</dsp:include>
	</div>
	<hr class="horizontal-divider">
</dsp:page>
