<dsp:page>
	<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUPopulateTSPInfoToUserSessionDroplet"/>
	<dsp:getvalueof var="synchronySDPResponseObj" param="synchronySDPResponseObj" />
	<dsp:droplet name="TRUPopulateTSPInfoToUserSessionDroplet">
		<dsp:param name="synchronySDPResponseObj" value="${synchronySDPResponseObj}" />
	</dsp:droplet>
</dsp:page>