<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftCardFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
<dsp:setvalue bean="GiftCardFormHandler.giftCardNumber" paramvalue="gcNumber" />
<dsp:setvalue bean="GiftCardFormHandler.giftCardPin" paramvalue="ean" />
<dsp:getvalueof var="formID" param="formID" />
<c:choose>
	<c:when test="${formID eq 'giftCardApplicationForm'}">
		<dsp:setvalue bean="GiftCardFormHandler.pageName" paramvalue="pageName" />
		<dsp:setvalue bean="GiftCardFormHandler.applyGiftCard" value="submit" />
	</c:when>
	<c:when test="${formID eq 'giftCardBalanceForm' || formID eq 'myAccountCheckBalanceForm'}">
		<dsp:setvalue bean="GiftCardFormHandler.fetchGiftCardBalance" value="submit" />
	</c:when>
	<c:when test="${formID eq 'giftCardRemoveForm'}">
		<dsp:setvalue bean="GiftCardFormHandler.cardId" paramvalue="cardId" />
		<dsp:setvalue bean="GiftCardFormHandler.removeGiftCard" value="submit" />
	</c:when>
</c:choose>
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="GiftCardFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="GiftCardFormHandler.formExceptions" />
					<dsp:oparam name="outputStart">
	            		 Following are the form errors:
					</dsp:oparam>
					<dsp:oparam name="output">
						<dsp:valueof param="message" valueishtml="true" />
					</dsp:oparam>
			</dsp:droplet>
           </dsp:oparam>
            <dsp:oparam name="false">
			<c:choose>
			<c:when test="${formID eq 'giftCardBalanceForm' || formID eq 'myAccountCheckBalanceForm'}">			
				<div class="gift-card-balance">&nbsp;<dsp:valueof bean="GiftCardFormHandler.giftCardBalance" converter="currency"/>&nbsp;</div>
				<div class="gift-card-balance-date"><fmt:message key="checkout.card.balance.as.of"/>&nbsp;<dsp:valueof
							bean="/atg/dynamo/service/CurrentDate.timeAsDate" date="MM/dd/yyyy" /></div>
			</c:when>
			<c:when test="${formID eq 'giftCardApplicationForm' || formID eq 'giftCardRemoveForm'}">
				<div>
					<div class="orderSummary">
						<dsp:include page="/checkout/common/checkoutOrderSummary.jsp" >
							<dsp:param name="pageName" value="Payment"/>
						</dsp:include> 
					</div>
					<div class="giftCardList">
						<dsp:include page="displayGiftCards.jsp" >
							<c:if test="${formID eq 'giftCardRemoveForm'}">
								<dsp:param name="giftCardRemoveForm" value="true" />
							</c:if>
						</dsp:include>
					</div>					
				</div>
			</c:when>			
			</c:choose>
            </dsp:oparam>
         </dsp:droplet>
</dsp:page>