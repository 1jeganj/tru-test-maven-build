<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:getvalueof var="order" bean="ShoppingCart.current" />
	<dsp:importbean bean="/com/tru/commerce/order/droplet/PaymentGroupTypeDroplet" />
	<dsp:getvalueof var="isCreditCardSelected" param="isCreditCardSelected" />
	<dsp:getvalueof var="isRadioCreditcardClicked" param="isRadioCreditcardClicked" />
	<!--  Start: For TUW-63873 -->
		<c:if test="${ empty isCreditCardSelected}">
			<dsp:droplet name="PaymentGroupTypeDroplet">
				<dsp:param value="${order}" name="order" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="selectedPaymentGroupType" param="selectedPaymentGroupType" />
				</dsp:oparam>
			</dsp:droplet>
			<dsp:getvalueof var="isCreditCardSelected" value="${selectedPaymentGroupType eq 'creditCard'}" />
		</c:if>
	<!-- 	 End: For TUW-63873 -->
	<dsp:droplet name="ForEach">
		<dsp:param name="array" bean="ShoppingCart.current.paymentGroups" />
		<dsp:param name="elementName" value="pg" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="pgType" param="pg.paymentGroupClassType" />
			<c:choose>
				<c:when test="${pgType eq 'creditCard' }">
					<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
						<dsp:param name="value" param="pg.billingAddress.address1" />
						<dsp:oparam name="false">
							<dsp:getvalueof var="pgBillingAddress" param="pg.billingAddress" />
							<input type="hidden" class="cr-card-section-replace" name="selectedCardBillingAddress" value="true" />
						</dsp:oparam>
						<dsp:oparam name="true">
							<input type="hidden" class="cr-card-section-replace" name="selectedCardBillingAddress" value="" />
							<%-- 	<dsp:include page="billingAddressForm.jsp"></dsp:include> --%>
						</dsp:oparam>
					</dsp:droplet>
				</c:when>
				<c:otherwise>
					<input type="hidden" class="cr-card-section-replace" name="selectedCardBillingAddress" value="" />
				</c:otherwise>
			</c:choose>
		</dsp:oparam>
	</dsp:droplet>
	
	<div id="creditCardForm">
		<dsp:include page="/checkout/payment/creditCard/creditCardFormFragment.jsp">
			<dsp:param name="isCreditCardSelected" value="${isCreditCardSelected}"/>
			<dsp:param name="isRadioCreditcardClicked" value="${isRadioCreditcardClicked}"/>
		</dsp:include>
	</div>
	<input type="hidden" class="cr-card-section-replace" id="isCreditCardSavedSelected" value="${isCreditCardSelected}"/>
</dsp:page>