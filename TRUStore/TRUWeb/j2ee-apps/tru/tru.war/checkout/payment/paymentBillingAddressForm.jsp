<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:getvalueof var="billingAddress" bean="ShoppingCart.current.billingAddress" />
	<dsp:importbean bean="/com/tru/commerce/order/droplet/PaymentGroupTypeDroplet" />
	<dsp:droplet name="PaymentGroupTypeDroplet">
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="isPayInStoreEligible" param="isPayInStoreEligible" vartype="java.lang.boolean" />
			<dsp:getvalueof var="selectedPaymentGroupType" param="selectedPaymentGroupType" />
		</dsp:oparam>
	</dsp:droplet>
	<div id="billing-info" <c:if test="${selectedPaymentGroupType eq 'payPal'}">style="display:none;"</c:if>>
		<form class="JSValidation" id="paymentBillingAddressForm">
			<div class="checkout-page-header checkout-payment-header">
				<fmt:message key="checkout.payment.billing"/>
			</div>
			<hr class="horizontal-divider billingHeader">
				<c:choose>
					<c:when test="${not empty billingAddress}">
						<dsp:include page="/checkout/payment/creditCard/displayBillingAddress.jsp">
							<dsp:param name="hasSavedCards" value="false" />
						</dsp:include>
					</c:when>
					<c:otherwise>
						<dsp:include page="/checkout/payment/creditCard/newBillingAddress.jsp"></dsp:include>
					</c:otherwise>
				</c:choose>
			<hr class="horizontal-divider billingHeader">
		</form>
	</div>
</dsp:page>