<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:getvalueof var="isPayPalSelected" param="isPayPalSelected" />
	<div class="checkout-payment-paypal">
		<c:choose>
			<c:when test="${isPayPalSelected}">
				<input id="radio-paypal" tabindex="0" name="payment-type" checked="checked" 
					class="inline" type="radio" value="paypal"
						aria-label="paymentMode"/>
						<c:set var="PayPalSelected" value="pre-show"/> 
			</c:when>
			<c:otherwise>
				<input id="radio-paypal" tabindex="0" name="payment-type" 
					class="inline" type="radio" value="paypal"
						aria-label="paymentMode"/>
			</c:otherwise>
		</c:choose>
		<div class="paypal-logo inline"></div>
		<span>&#183;</span>&nbsp;<a class="paypalDdetails"href="javascript:void(0);">
		<fmt:message key="checkout.payment.whatIsPaypal" /></a>
		<div class="paypal-order-message ${PayPalSelected}" ><fmt:message key="checkout.payment.order.submit.message" /></div>
	</div>
	<div class="paypalWhatsPayPal display-none">
		<fmt:message key="checkout.payment.whatIsPaypal.one" />
		<fmt:message key="checkout.payment.whatIsPaypal.two" />
		<fmt:message key="checkout.payment.whatIsPaypal.three" />
		<fmt:message key="checkout.payment.whatIsPaypal.four" />
	</div>
</dsp:page>