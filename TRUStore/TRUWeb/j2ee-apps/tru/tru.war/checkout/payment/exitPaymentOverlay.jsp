<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<div class="modal fade checkoutExitConfirmationModal"
		tabindex="-1" role="dialog" aria-labelledby="basicModal"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content sharp-border">
				<span class="clickable">
					<a href="javascript:void(0)" data-dismiss="modal"><img src="${TRUImagePath}images/close.png" alt="close modal"></a>
				</span>
				<div>
					<h2><fmt:message key="checkout.sure.want.to.exit"/></h2>
					<p><fmt:message key="checkout.exit.overlay.message"/></p>
					<a class="no-continue-checkout" href="javascript:void(0);"><fmt:message key="checkout.no.continue"/></a>
					<a class="yes-exit-checkout" href="javascript:void(0);"><fmt:message key="checkout.yes.exit"/></a>
				</div>
			</div>
		</div>
	</div>
</dsp:page>