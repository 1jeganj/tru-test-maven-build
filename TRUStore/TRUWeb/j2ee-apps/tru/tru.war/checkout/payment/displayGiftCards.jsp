<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftCardFormHandler"/>
<dsp:importbean bean="/com/tru/commerce/order/droplet/PaymentGroupTypeDroplet" />
<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:getvalueof var="giftCardRemoveForm" param="giftCardRemoveForm" vartype="java.lang.boolean" />
<c:set var="gcCount" value="0" />
	<div class="gift-card-table">
		<table>
			<tbody>
				<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="ShoppingCart.current.paymentGroups" />
				<dsp:param name="elementName" value="paymentGroup" />
				<dsp:oparam name="output">	
						<dsp:getvalueof var="pgType" param="paymentGroup.paymentGroupClassType" />						
						<c:choose>
							<c:when test="${pgType eq 'giftCard'}">
								<c:set var="gcCount" value="${gcCount+1}"/>
								<dsp:getvalueof var="gcNumber" param="paymentGroup.giftCardNumber" />
								<dsp:getvalueof var="gcAmount" param="paymentGroup.amount" />
								<dsp:getvalueof var="cardId" param="paymentGroup.id" />
								<tr>
									<td><dsp:valueof param="paymentGroup.giftCardNumber" converter="creditcard" maskcharacter="x" /></td>
									<td><dsp:valueof format="currency" value="${gcAmount }" locale="en_US" converter="currency"/>&nbsp;<fmt:message key="checkout.payment.applied"/></td>
									<td><a class="pull-right" data-id="${cardId}" href="javascript:void(0);"><fmt:message key="checkout.payment.remove"/></a></td>
								</tr>
							</c:when>
						</c:choose>
					</dsp:oparam>
				</dsp:droplet>	
			</tbody>
		</table>
		<input type="hidden" id="gcAppliedCount" name="gcAppliedCount" value="${gcCount}" />
		<c:if test="${giftCardRemoveForm eq true}">
			<dsp:droplet name="PaymentGroupTypeDroplet">
				<dsp:param name="order" bean="ShoppingCart.current" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="isPayInStoreEligible" param="isPayInStoreEligible" vartype="java.lang.boolean" />
					<input type="hidden" id="isPayInStoreEligible" name="isPayInStoreEligible" value="${isPayInStoreEligible}" />
				</dsp:oparam>
			</dsp:droplet>
		</c:if>
	</div>					
</dsp:page>