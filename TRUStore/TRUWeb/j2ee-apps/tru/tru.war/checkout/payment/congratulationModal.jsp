<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/com/tru/common/TRUUserSession" />
<dsp:getvalueof var="order" bean="ShoppingCart.current" />
<dsp:getvalueof var="orderIsEligibleForFinancing" value="${order.orderIsEligibleForFinancing}"/>
<dsp:getvalueof var="financingTermsAvailable" value="${order.financingTermsAvailable}"/>
<dsp:getvalueof var="rusCardSelectedCheck" bean="TRUUserSession.rusCardSelected"/>
<%-- congratulations modal starts --%>
	<!-- <div class="modal-backdrop fade in"></div> -->
	<div class="modal-dialog modal-md">
		<div class="modal-content sharp-border">
			<div class="payment-popup">
				<div class="row">
					<div class="col-md-5 col-md-offset-7">
						<div class="row top-margin">
							<div class="col-md-1 col-md-offset-10">
								<a href="javascript:void(0)"  data-dismiss="modal">
									<span class="clickable deselect-yes-disclosure"><span class="sprite-icon-x-close"></span></span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
							<div class="checkOut-promocode-error-state-overlay display-none">
								<div class="checkOut-promocode-error-state-header row">
									<div class="error-state-exclamation-lg img-responsive col-md-2"></div>
									<div class="checkOut-promocode-error-state-header-text col-md-10">
										<fmt:message key="shoppingcart.oops.issue" />
										<div class="checkOut-promocode-error-state-subheader">
											<span></span>&nbsp;<a class="why-not-promo-code-not-applied">&nbsp;<fmt:message
													key="checkout.why.not" />
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="popover seeTerms whyNotCouponCode fade bottom in" role="tooltip">
								<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters/TRU/Checkout/PromoCodeErrorDisplayTargeter" />
								<dsp:getvalueof var="cacheKey" value="${atgTargeterpath}${siteId}${locale}" />	
								<dsp:droplet name="/com/tru/cache/TRUPromoCodeErrorTargeterCacheDroplet">
									<dsp:param name="key" value="${cacheKey}" />
									<dsp:oparam name="output">
										<dsp:droplet name="/atg/targeting/TargetingFirst">
											<dsp:param name="targeter" bean="${atgTargeterpath}" />
												<dsp:oparam name="output">
													<dsp:valueof param="element.data" valueishtml="true" />
												</dsp:oparam>
										</dsp:droplet>
									</dsp:oparam>
								</dsp:droplet>
							</div>
							
							<div class="payment-popup-desc">
							<header>
								<fmt:message key="finance.disc.congratulations.header" />
							</header>
							<p>
								<fmt:message key="finance.disc.conggratulations.msg1" />
							<p>
							<p>
								<fmt:message key="finance.disc.conggratulations.msg2" />
							</p>
							<div class="options">
								<c:if test="${orderIsEligibleForFinancing}">
									<p class="overlayFinancingOptionClass">
										<label><input type="radio" name="rus-creditcard-option" id="creditcard-option-month"
											value="option1" <c:if test="${orderIsEligibleForFinancing eq false}">disabled="disabled"</c:if>>
											${financingTermsAvailable}<fmt:message key="finance.disc.conggratulations.opt1.part1" />
											<a href="http://www.toysrus.com/shop/index.jsp?categoryId=20186036" target="_blank"><fmt:message key="finance.disc.conggratulations.opt1.terms" /></a>
											<fmt:message key="finance.disc.conggratulations.opt1.part2" /></label>&nbsp;<br>&nbsp;<span class="note">
											<fmt:message key="finance.disc.conggratulations.opt1.note" /><span>
									</p>
								</c:if>
								<c:if test="${rusCardSelectedCheck eq true}">
									<p class="overlayPromoOptionClass">
										<label><input type="radio" name="rus-creditcard-option" id="creditcard-option-promo"
											value="option2"><fmt:message key="finance.disc.conggratulations.opt2" /></label>
									</p>
								</c:if>
								<p class="">
									<label><input type="radio" name="rus-creditcard-option" id="creditcard-option-no"
										value="option3"><fmt:message key="finance.disc.conggratulations.opt3" /></label>
								</p>
							</div>
						</div>
						<div class="action-buttons">
							<button class="toysrus-creditcard-btn"><fmt:message key="finance.disc.conggratulations.continue.cta" /></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<%-- congratulations modal ends --%>
</dsp:page>