<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/multisite/Site" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/com/tru/common/TRUUserSession" />
	<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUParseSynchronyResponseDroplet" />
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler" />
	<dsp:importbean bean="/com/tru/commerce/order/droplet/PaymentGroupTypeDroplet" />
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
	<dsp:getvalueof var="synchronySDPResponseObj" param="synchronySDPResponseObj" />
	<dsp:getvalueof var="isAnonymousUser" bean="Profile.transient" />
	<dsp:getvalueof var="order" bean="ShoppingCart.current" />
	<dsp:getvalueof var="displayFinancingPromoBanner" bean="Site.displayFinancingPromoBanner" />
	<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale" />
	<dsp:getvalueof var="rusCardSelected" value="${order.rusCardSelected}" />
	<dsp:getvalueof var="hasRUSCard" bean="TRUUserSession.tSPCardInfoMap.hasRUSCard" />
	<dsp:getvalueof var="inputEmail" bean="TRUUserSession.inputEmail" />
	<dsp:getvalueof var="isPayPalOrder" bean="ShoppingCart.current.payPalOrder" />


	<c:choose>
		<c:when test="${not empty synchronySDPResponseObj}">
			<dsp:getvalueof var="synchronyRewardNum" value="${synchronySDPResponseObj.olsonRewardNumber}" />
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="synchronyRewardNum" bean="TRUUserSession.tSPCardInfoMap.olsonRewardNumber" />
		</c:otherwise>
	</c:choose>


	<dsp:droplet name="PaymentGroupTypeDroplet">
		<dsp:param value="${order}" name="order" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="isPayInStoreEligible" param="isPayInStoreEligible" vartype="java.lang.boolean" />
			<dsp:getvalueof var="selectedPaymentGroupType" param="selectedPaymentGroupType" />
		</dsp:oparam>
	</dsp:droplet>

	<%-- Added for in store payment --%>
		<div class="col-md-8 checkout-left-column">
			<dsp:include page="/checkout/common/checkoutGlobalError.jsp" />
			<input type="hidden" value="${order.orderIsEligibleForFinancing}" class="financingOptions" />
			<c:choose>
				<c:when test="${not empty synchronySDPResponseObj}">
					<input type="hidden" value="true" id="isFromSynchrony" />
					<input type="hidden" value="${synchronySDPResponseObj.applicationDecision}" id="synchronyAppDecision" />
					<input type="hidden" value="${synchronySDPResponseObj.singleUseCouponCode}" id="singleUseCouponCode" />
					<input type="hidden" value="false" id="orderHasRUSCard" />
				</c:when>
				<c:when test="${hasRUSCard eq 'true'}">
					<input type="hidden" value="false" id="isFromSynchrony" />
					<input type="hidden" value="true" id="orderHasRUSCard" />
				</c:when>
				<c:otherwise>
					<input type="hidden" value="false" id="isFromSynchrony" />
					<input type="hidden" value="false" id="orderHasRUSCard" />
				</c:otherwise>
			</c:choose>
			<%-- Start :: TUW-56705 --%>
				<dsp:droplet name="Switch">
					<dsp:param name="value" bean="BillingFormHandler.orderCreditCardExpired" />
					<dsp:oparam name="true">
						<dsp:droplet name="ErrorMessageForEach">
							<dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
							<dsp:oparam name="outputStart">
								<span class="errorDisplay expiredMessage">
			       	</dsp:oparam>
			        <dsp:oparam name="output">
			            <span><dsp:valueof param="message"/></span>
							</dsp:oparam>
							<dsp:oparam name="outputEnd">
								</span>
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
				</dsp:droplet>
				<%-- End :: TUW-56705 --%>
					<%--  Added for billing Address CR --%>

						<dsp:include page="/checkout/payment/paymentBillingAddressForm.jsp" />

						<%--  Added for billing Address CR --%>
							<%-- Added for email CR --%>
								<c:choose>
									<c:when test="${isAnonymousUser eq 'true'}">
										<div class="checkout-payment-header checkout-page-form-header">
											<fmt:message key="checkout.enter.your.email" />
										</div>
										<div id="payment-email-form" class="checkout-payment-enter-email">
											<form id="orderPaymentEmailForm" class="JSValidation">
												<div class="checkout-page-form-subheader">
													<p>
														<fmt:message key="checkout.email.tosend.confirmation" />
													</p>
													<p><span class="orange-astericks"><fmt:message key="myaccount.asterisk" /></span>&nbsp;
														<fmt:message key="checkout.overlay.required"
														/>
													</p>
												</div>
												<div class="row">
													<div class="col-sm-6">
														<div class="shipping-address-field adjust-margin tru-float-label-field">
															<div class="shipping-address-field-required orange-astericks">&#42;</div>
															<fmt:message key="checkout.overlay.email" var="emailLabel" />
															<label class="tru-float-label" for="orderEmail">${emailLabel}</label>
															<c:choose>
																<c:when test="${not empty inputEmail}">
																	<input id="orderEmail" class="shipping-address-field-input tru-float-label-input" type="text" maxlength="60"  name="orderEmail" value="${inputEmail}" placeholder="${emailLabel}" aria-required="true"/>
																</c:when>
																<c:otherwise>
																	<input id="orderEmail" class="shipping-address-field-input tru-float-label-input" type="text" maxlength="60"  name="orderEmail" value="${order.email}" placeholder="${emailLabel}" aria-required="true"/>
																</c:otherwise>
															</c:choose>
															<div class="row">
																<div class="col-sm-1 col-sm-push-10">
																	<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</form>
										</div>
									</c:when>
									<c:otherwise>
										<dsp:getvalueof bean="Profile.email" var="orderEmail" />
										<input type="hidden" name="orderEmail" id="orderEmail" maxlength="60" value="${orderEmail}" />
									</c:otherwise>
								</c:choose>
								<hr class="horizontal-divider"/>
								<%-- Added for email CR --%>
									<div class="checkout-payment-header checkout-page-header">
										<fmt:message key="checkout.payment.paymentMode" />
									</div>

									<div class="credit-structure">
										<c:if test="${(displayFinancingPromoBanner eq 'true') and (empty synchronySDPResponseObj) and (hasRUSCard ne 'true')}">
											<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters/TRU/Checkout/RUSCardBannerTargeter" />
											<dsp:getvalueof var="cacheKey" value="${atgTargeterpath}${siteId}${locale}" />
											<dsp:droplet name="/com/tru/cache/TRURusContentBannerCacheDroplet">
												<dsp:param name="key" value="${cacheKey}" />
												<dsp:oparam name="output">
													<dsp:droplet name="/atg/targeting/TargetingFirst">
														<dsp:param name="targeter" bean="${atgTargeterpath}" />
														<dsp:oparam name="output">
															<dsp:valueof param="element.data" valueishtml="true" />
														</dsp:oparam>
													</dsp:droplet>
												</dsp:oparam>
											</dsp:droplet>
										</c:if>
										<c:if test="${(not empty synchronySDPResponseObj and synchronySDPResponseObj.applicationDecision eq 'A') or (hasRUSCard eq 'true')}">
											<hr class="horizontal-divider">
											<dsp:include page="rusCardForm.jsp">
												<dsp:param name="synchronySDPResponseObj" value="${synchronySDPResponseObj}" />
												<dsp:param name="isCreditCardSelected" value="${selectedPaymentGroupType eq 'creditCard'}" />
											</dsp:include>
										</c:if>
										<c:if test="${not empty synchronySDPResponseObj and synchronySDPResponseObj.applicationDecision eq 'P'}">
											<span class="synchrony-errorDisplay">
 					<span><fmt:message key="checkout.errorsynchrony.message1"/>&nbsp;<fmt:message key="checkout.errorsynchrony.message2"/></span>&nbsp;
											</span>
										</c:if>
										<dsp:include page="creditCardForm.jsp">
											<dsp:param name="isCreditCardSelected" value="${selectedPaymentGroupType eq 'creditCard'}" />
										</dsp:include>
									</div>
									<hr class="horizontal-divider">
									<dsp:include page="payPal.jsp">
										<dsp:param name="isPayPalSelected" value="${selectedPaymentGroupType eq 'payPal'}" />
									</dsp:include>
									<hr class="horizontal-divider">
									<dsp:include page="payInStore.jsp">
										<dsp:param name="isPayInStoreEligible" value="${isPayInStoreEligible}" />
										<dsp:param name="isPayInStoreSelected" value="${selectedPaymentGroupType eq 'inStorePayment'}" />
									</dsp:include>
									<hr class="horizontal-divider">
									<dsp:include page="giftCard.jsp">
										<dsp:param name="isPayInStoreEligible" value="${isPayInStoreEligible}" />
										<dsp:param name="isPayInStoreSelected" value="${selectedPaymentGroupType eq 'inStorePayment'}" />
									</dsp:include>
									<hr>
									<dsp:include page="promotionCodes.jsp" />
									<hr>
									<dsp:include page="loyaltyRewardNumber.jsp">
										<dsp:param name="synchronyRewardNum" value="${synchronyRewardNum}" />
									</dsp:include>
									<hr>
									<dsp:include page="/checkout/common/salesTaxEstimation.jsp"></dsp:include>
		</div>
		<div class="modal fade" id="payment-page-apply-today" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true"
		 style="display: none;">
			<dsp:droplet name="/atg/targeting/TargetingFirst">
				<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/Checkout/ApplyFinancingOverlayTargeter" />
				<dsp:oparam name="output">
					<dsp:valueof param="element.data" valueishtml="true" />
				</dsp:oparam>
			</dsp:droplet>
		</div>
		<div id="paymentDetailsPopup" class="modal fade paymentDetailPopup" tabindex="-1" role="dialog" aria-labelledby="basicModal"
		 aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content sharp-border welcome-back-overlay">
					<a href="javascript:void(0)" data-dismiss="modal" class=""><span class="sprite-icon-x-close"></span></a>
					<h2>details</h2>
					<div class="abandonCartReminder">
						<dsp:droplet name="/atg/targeting/TargetingFirst">
							<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/Checkout/ApplyAndBuyDetailTargeter" />
							<dsp:oparam name="output">
								<dsp:valueof param="element.data" valueishtml="true" />
							</dsp:oparam>
						</dsp:droplet>
						<!-- No interest if paid in full within 6 months* on purchases of $299 or more or No interest if paid in full within 12 months* on purchases of $749 or more made at "R"Us with your "R"Us Credit Card or "Rills MasterCard. Interest will be charged to your account from the purchase date if the promotional balance is not paid in full within 6 or 12 months. Minimum monthly payments required. Discounts applied at time of purchase will reduce your total purchase amount.
						<br/><br/>Special financing purchases do not earn reward points. *Offers subject to credit approval. Cannot be combined with account opening discount. Any discount, coupon, manufacturer rebate, or other promotional offer applied at time of purchase, will reduce your total purchase amount and may result in you not satisfying the minimum qualifying purchase amount required. Minimum monthly payments required. Valid on "R"Us purchases. 6 Month Special financing applies to purchases of $299 or more and 12 month Special financing applies to purchases of $749 or more made at "R"Us using either the "R"Us Credit Card or "R"Us MasterCard. No interest will be assessed on the promotional purchase if you pay the following (the "promotional balance") in full within 6 or 12 months: 1) the promotional purchase amount, and 2) any related optional credit insurance/debt cancellation charges. If you do not, interest will be assessed on the promotional balance from the date of the purchase. Regular account terms apply to non-promotional purchases and after promotion ends to promotional purchases. For new accounts: Purchase APR is 26.99% and minimum interest charge is $2.00 for both types of cards. Existing cardholders should see their credit card agreement for their applicable terms. -->
					</div>
				</div>
			</div>
		</div>


		<dsp:include page="financeDicclosureModals.jsp" />

		<div id="tealiumCheckoutContent">


		</div>
</dsp:page>