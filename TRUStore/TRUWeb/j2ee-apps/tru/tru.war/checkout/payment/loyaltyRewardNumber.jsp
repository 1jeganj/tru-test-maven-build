<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/multisite/Site"/>
<dsp:getvalueof bean="Site.olsonSignUpURL" var="olsonSignUpURL"></dsp:getvalueof>
<dsp:getvalueof bean="Site.olsonLookUpURL" var="olsonLookUpURL"></dsp:getvalueof>
<dsp:getvalueof bean="Profile.rewardNo" var="ProfileRewardNumber"></dsp:getvalueof>
<dsp:getvalueof var="synchronyRewardNum" param="synchronyRewardNum"/>
<c:if test="${not empty ProfileRewardNumber}">
<%-- call the handle method to copy the rewards number from profile to order --%>
	<dsp:setvalue bean="BillingFormHandler.initRewardNumberInOrder" value="submit" />
</c:if>
<dsp:getvalueof bean="ShoppingCart.current.rewardNumber" var="orderRewardNumber"/>
 <div class="payment-rewardsrus">
 
     <div class="rewardsrus-card pull-right"></div>
     <header><fmt:message key="checkout.payment.rewards" /></header>
	     <p>
	         <fmt:message key="checkout.payment.rewards.membership"/>
	     </p>
	     <div class="member-number">
	     	<form id="membershipIDInPaymentForm" class="JSValidation">
		         <div class="inline">
		             <label for="enterMembershipIDInCheckout"><fmt:message key="checkout.payment.memberNumber" /></label>
		             <c:choose>
		             	<c:when test="${not empty synchronyRewardNum}">
		             		<input type="text" id="enterMembershipIDInCheckout" name="enterMembershipIDInCheckout" value="${synchronyRewardNum}" maxlength="13" onkeypress="return isNumberOnly(event,14)" class="chkNumbersOnly"/>
		             	</c:when>
		             	<c:when test="${empty synchronyRewardNum and not empty ProfileRewardNumber }">
		             		<input type="text" id="enterMembershipIDInCheckout" name="enterMembershipIDInCheckout" value="${ProfileRewardNumber}" maxlength="13" onkeypress="return isNumberOnly(event,14)" class="chkNumbersOnly"/>
		             	</c:when>
		             	<c:otherwise>
		             		<input type="text" id="enterMembershipIDInCheckout" name="enterMembershipIDInCheckout" value="${orderRewardNumber}" maxlength="13" onkeypress="return isNumberOnly(event,14)" class="chkNumbersOnly"/>
		             	</c:otherwise>
		             </c:choose>
		         </div>
	         </form>
	         <div class="forgot-number-password pull-right">
	             <p>
				<a href="javascript:void()" class="" onclick="window.open('${olsonLookUpURL}','_blank','width=990,height=400,resizable=1,scrollbars=1')" target="_blank">
				<fmt:message key="checkout.payment.forgotNumber" /></a>
	             </p>
	             <p><a class="notMemberpopover" href="${olsonSignUpURL}"><fmt:message key="checkout.payment.notAMember" /></a>
	             </p>
	         </div>
	     </div>
 </div>
</dsp:page>