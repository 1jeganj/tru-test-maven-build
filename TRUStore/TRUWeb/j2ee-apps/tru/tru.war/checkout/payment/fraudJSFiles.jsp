<dsp:page>

	<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:getvalueof var="versionFlag" param="versionFlag" />
	<dsp:getvalueof var="staticAssetsVersionFormat" param="staticAssetsVersionFormat" />
	<dsp:getvalueof var="versionNumbers" param="versionNumbers" />
	
	<c:choose>
		<c:when test="${versionFlag}">
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/info.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/line.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/script_util.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/user_data.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/util_form.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/details.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/plan_data.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/mode.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/user.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/service.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/form_functions.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/util.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/session_d.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/condition.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/scripts.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/fraudJsCollector.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
		</c:when>
		<c:otherwise>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/info.js"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/line.js"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/script_util.js"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/user_data.js"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/util_form.js"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/details.js"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/plan_data.js"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/mode.js"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/user.js"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/service.js"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/form_functions.js"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/util.js"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/session_d.js"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/condition.js"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/scripts.js"></script>
			<script type="text/javascript" src="${TRUJSPath}javascript/fraud-js-collector/fraudJsCollector.js"></script>	
		</c:otherwise>
	</c:choose>
	

</dsp:page>