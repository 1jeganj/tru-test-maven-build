<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/com/tru/commerce/cart/droplet/OrderSummaryDetailsDroplet" />
	<dsp:droplet name="OrderSummaryDetailsDroplet">
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:param name="profile" bean="/atg/userprofiling/Profile" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="maxCouponLimit" param="maxCouponLimit" />
		</dsp:oparam>
		<dsp:oparam name="empty">
			<dsp:getvalueof var="commerceItemCount" value="0" />
		</dsp:oparam>
	</dsp:droplet>
	<div class="enter-promo-code" id="enter-promo-code-container">
		<header>
			<fmt:message key="checkout.payment.promoCode" />
		</header>
		<c:choose>
		<c:when test="${maxCouponLimit}">
		<div>
			<p>
				<label for="promo-code-payment-page"><fmt:message key="checkout.payment.codeNumber" /></label>
			</p>
			<form id="promoCodePaymentPageForm" onsubmit="javascript: return false;">
				<input maxlength="20" class="inline" id="promo-code-payment-page" name="promoCode" type="text" disabled="disabled">
				<input type="submit" value="<fmt:message key="checkout.payment.apply" />" name="promoCodePaymentPage" id="promo-code-payment-page-btn" disabled="disabled" />
			</form>
			<!-- <div class="inline">
				<div class="promocode-error-msg"></div>
			</div> -->
			<%-- <button id="apply-promo-code" type="button" readonly="readonly">
				<fmt:message key="checkout.payment.apply" />
			</button> --%>
		</div>
		<div class="promo-code-max-limit-message"><fmt:message key="shoppingcart.max.coupon.limt" /></div>
		<a class="termOfUSeDetails" href="javascript:void(0);" data-original-title="" title=""><fmt:message key="checkout.payment.termsOfUse" /></a>	
		</c:when>
		<c:otherwise>
		<div>
			<p>
				<label for="promo-code-payment-page"><fmt:message key="checkout.payment.codeNumber" /></label>
			</p>
			<form id="promoCodePaymentPageForm" onsubmit="javascript: return false;">
				<input maxlength="20" class="inline" id="promo-code-payment-page" name="promoCode" type="text">
				<input type="submit" value="<fmt:message key="checkout.payment.apply" />" name="promoCodePaymentPage" id="promo-code-payment-page-btn" disabled="disabled" />
			</form>
			<!-- <div class="inline">
				<div class="promocode-error-msg"></div>
			</div> -->
			<%-- <button id="apply-promo-code" type="button" readonly="readonly">
				<fmt:message key="checkout.payment.apply" />
			</button> --%>
		</div>
		
		<a class="termOfUSeDetails" href="javascript:void(0);" data-original-title="" title=""><fmt:message key="checkout.payment.termsOfUse" /></a>	</c:otherwise>
		</c:choose>
			
		<dsp:include page="/checkout/payment/promoCodeSummary.jsp"/>			
		
	</div>
</dsp:page>