<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:getvalueof var="order" bean="ShoppingCart.current" />

<dsp:getvalueof var="orderIsEligibleForFinancing" value="${order.orderIsEligibleForFinancing}"/>
<dsp:getvalueof var="financingTermsAvailable" value="${order.financingTermsAvailable}"/>
<%-- congratulations modal starts --%>
<div class="modal fade in" id="toysrus-creditcard" tabindex="-1"
	role="dialog" aria-labelledby="basicModal" aria-hidden="false">
	<dsp:include page="/checkout/payment/congratulationModal.jsp"/>
</div>
<%-- congratulations modal ends --%>

<%-- electronic-disclosure modal starts --%>
<div class="modal fade in" id="electronic-disclosure" tabindex="-1"
	role="dialog" aria-labelledby="basicModal" aria-hidden="false">
	<!-- <div class="modal-backdrop fade in"></div> -->
	<div class="modal-dialog modal-md">
		<div class="modal-content sharp-border">
			<div class="payment-popup">
				<div class="row">
					<div class="col-md-5 col-md-offset-7">
						<div class="row top-margin">
							<div class="col-md-1 col-md-offset-10">
								<a href="javascript:void(0)"  data-dismiss="modal">
									<span class="clickable deselect-yes-disclosure"><span class="sprite-icon-x-close"></span></span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="payment-popup-desc">
							<%-- <p>
								<fmt:message key="finance.disc.electronic.disc.msg.part1" />
								<a href="#"><fmt:message key="finance.disc.electronic.disc.imp.promo.info" /></a>
								<fmt:message key="finance.disc.electronic.disc.msg.part2" />
							<p> --%>
							<dsp:droplet name="/atg/targeting/TargetingFirst">
										<dsp:param name="targeter"				bean="/atg/registry/RepositoryTargeters/TRU/Checkout/SpecialFinancingTargeter" />
										<dsp:oparam name="output">
													<dsp:valueof param="element.data" valueishtml="true" />
										</dsp:oparam>
						    </dsp:droplet>
						</div>
						<div class="action-buttons">
							<button class="electronic-disclosure-agree">
								<fmt:message key="finance.disc.electronic.disc.agree.cta" />
							</button>
							<button data-dismiss="modal" class="deselect-yes-disclosure">
								<fmt:message key="finance.disc.electronic.disc.cancel.cta" />
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%-- electronic-disclosure modal ends --%>
<%-- electronic-disclosure-downtime modal starts --%>
 <%-- Removed for code refactor --%>
<%-- electronic-disclosure-downtime modal ends --%>		
<%-- print modal stands --%>
<div class="modal fade in" id="promotion-popup" tabindex="-1"
	role="dialog" aria-labelledby="basicModal" aria-hidden="false">
	<!-- <div class="modal-backdrop fade in"></div> -->
	<div class="modal-dialog modal-md">
		<div class="modal-content sharp-border">
			<div class="payment-popup">
				<div class="row">
					<div class="col-md-5 col-md-offset-7">
						<div class="row top-margin">
							<div class="col-md-1 col-md-offset-10">
								<a href="javascript:void(0)"  data-dismiss="modal">
									<span class="clickable"><span class="sprite-icon-x-close"></span></span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="payment-popup-desc">
							<header>
								<p>
									<fmt:message key="finance.disc.promo.popup.msg1" />
									<span class="promotion-plan-content">
										<fmt:message key="finance.disc.promo.popup.msg2" />
										${financingTermsAvailable}
										<fmt:message key="finance.disc.promo.popup.msg3" />
									</span>
								</p>
								</header>
							<p>
								<fmt:message key="finance.disc.promo.popup.msg4" />
								<span id="promoAPRFlag"></span>
								<fmt:message key="finance.disc.promo.popup.msg5" />
								<span id="promoAPRValue"></span>
								<fmt:message key="finance.disc.promo.popup.msg6" />
							<p>
						</div>
						<div class="action-buttons">
							<button class="promotion-popup-print-btn">
								<fmt:message key="finance.disc.promo.popup.print.save.cta" />
							</button>
							<button data-dismiss="modal">
								<fmt:message key="finance.disc.promo.popup.close.cta" />
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- print modal ends -->		

<div class="modal fade in" id="disclosure-failure" tabindex="-1"
	role="dialog" aria-labelledby="basicModal" aria-hidden="false">
	<!-- <div class="modal-backdrop fade in"></div> -->
	<div class="modal-dialog modal-md">
		<div class="modal-content sharp-border">
			<div class="payment-popup">
				<div class="row">
					<div class="col-md-5 col-md-offset-7">
						<div class="row top-margin">
							<div class="col-md-1 col-md-offset-10">
								<a href="javascript:void(0)"  data-dismiss="modal"  class="disclosure-failure-closeBtn">
									<span class="clickable"><span class="sprite-icon-x-close"></span></span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="disclosure-failure-error"></div>
						<div class="disclosure-failure-message">
						<div class="failureMessageHeader"><b><fmt:message key="finance.disc.failure.message.header" /></b></div>
							<p>
								<fmt:message key="finance.disc.failure.message" />
							<p>
						</div>
						<div class="action-buttons">
							<button class="on-disclosure-error">
								<fmt:message key="finance.disc.conggratulations.continue.cta" />
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</dsp:page>