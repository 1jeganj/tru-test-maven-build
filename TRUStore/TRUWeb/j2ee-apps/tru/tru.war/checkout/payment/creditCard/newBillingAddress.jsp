<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService" />
	<dsp:importbean bean="/com/tru/common/TRUUserSession" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:getvalueof var="nickNameInHiddenSpan" param="nickNameInHiddenSpan"/>
	<dsp:getvalueof var="rusBillingEdit" param="rusBillingEdit"/>
	<dsp:getvalueof var="isFromPAymentPage" param="isFromPAymentPage"/>
	<div class="co-billing-address">
		<div class="checkout-billing-address-form checkout-shipping-address-form">
		<%-- <c:if test="${isFromPAymentPage eq true}">
			<a href="javascript:void(0);" class="checkoutEditBillingAddress_cancel">
				<fmt:message key="myaccount.cancel" />
			</a>
		</c:if> --%>
			<div class="checkout-billing-address-form-header">
				<div class="inline address-form-header credit-card-billing-address-header">
					<h3 class="checkout-page-form-header"><fmt:message key="checkout.overlay.billingAddress"/></h3>
					<p class="required-label">
						<span class="orange-astericks"><fmt:message key="myaccount.asterisk" />&nbsp;</span><fmt:message key="checkout.overlay.required"/>
					</p>
				</div>
			</div>
			<div class="checkout-shipping-address-form-content pad-top-20">
				<div class="row left-padding billing-address pull-up">
					<div class="col-md-9">
						<div class="pad-bottom-0">
							<div class="shipping-address-field select-wrapper billing">
								<span class="orange-astericks">&#42;</span>&nbsp;
								<label for="country"><fmt:message key="checkout.overlay.country"/></label>								
								<select name="country" class="shipping-address-field-select" id="country" onchange="onCountryChangeInPayment();">
									<dsp:droplet name="ForEach">
										<dsp:param name="array" bean="/atg/commerce/util/CountryList.places" />
										<dsp:param name="elementName" value="billingCountry" />
										<dsp:oparam name="output">
											<dsp:getvalueof var="billingCountry" param="billingCountry.code"></dsp:getvalueof>
											<option value='<dsp:valueof param="billingCountry.code" />'
											<dsp:droplet name="/atg/dynamo/droplet/Compare">
			                                                <dsp:param name="obj1" param="billingCountry.code"/>
			                                                <dsp:param name="obj2" value="US" />
			                                                <dsp:oparam name="equal">
			                                                    selected="selected"
			                                                </dsp:oparam>
				                                        </dsp:droplet>
													>
												<dsp:valueof param="billingCountry.displayName" />
											</option>
										</dsp:oparam>
									</dsp:droplet>
								</select>
							</div>
							<div class="shipping-address-field adjust-margin tru-float-label-field">
								<div class="shipping-address-field-required orange-astericks">&#42;</div>
								<fmt:message key="checkout.shipping.firstname" var="firstnameLabel" />
								<label class="tru-float-label" for="firstName">${firstnameLabel}</label>
								<input id="firstName" class="shipping-address-field-input tru-float-label-input" type="text" maxlength="30"  name="firstName" value="${firstName}" placeholder="${firstnameLabel}" aria-required="true"/>
								<div class="row">
									<div class="col-sm-1 col-sm-push-10">
										<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
									</div>
								</div>
							</div>
							<div class="shipping-address-field adjust-margin tru-float-label-field">
								<div class="shipping-address-field-required orange-astericks">&#42;</div>
								<fmt:message key="checkout.shipping.lastname" var="lastnameLabel" />
								<label class="tru-float-label" for="lastName">${lastnameLabel}</label>
								<input id="lastName" class="shipping-address-field-input tru-float-label-input" type="text" name="lastName" maxlength="30"  value="${lastName}" placeholder="${lastnameLabel}" aria-required="true"/>
								<div class="row">
									<div class="col-sm-1 col-sm-push-10">
										<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
									</div>
								</div>					
							</div>
							<div class="shipping-address-field adjust-margin tru-float-label-field">
								<div class="shipping-address-field-required orange-astericks">&#42;</div>
								<fmt:message key="checkout.shipping.address1" var="address1Label" />
								<label class="tru-float-label" for="address1">${address1Label}</label>							
								<input id="address1" class="shipping-address-field-input tru-float-label-input" type="text" name="address1" value="${address1}" placeholder="${address1Label}" aria-required="true" maxlength="50"/>
								<div class="row">
									<div class="col-sm-1 col-sm-push-10">
										<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
									</div>
								</div>					
							</div>
							<div class="shipping-address-field adjust-margin tru-float-label-field">
								<fmt:message key="checkout.shipping.suite" var="aptsuiteLabel" />
								<label class="tru-float-label" for="address2">${aptsuiteLabel}</label>									
								<input id="address2" class="shipping-address-field-input tru-float-label-input" type="text" value="${address2}" name="address2" maxlength="50" placeholder="${aptsuiteLabel}"/>
								<div class="row">
									<div class="col-sm-1 col-sm-push-10">
										<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
									</div>
								</div>					
							</div>
							<div class="shipping-address-field adjust-margin tru-float-label-field">
								<div class="shipping-address-field-required orange-astericks">&#42;</div>
								<fmt:message key="checkout.shipping.city" var="cityLabel" />
								<label class="tru-float-label" for="city">${cityLabel}</label>								
								<input id="city" class="shipping-address-field-input tru-float-label-input" type="text" name="city" value="${city}" placeholder="${cityLabel}" aria-required="true" maxlength="30"/>
								<div class="row">
									<div class="col-sm-1 col-sm-push-10">
										<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
									</div>
								</div>					
							</div>
							<div class="shipping-address-field">
								<span class="orange-astericks">&#42;</span>&nbsp;<label class="shipping-address-field-label" for="state"><fmt:message
										key="checkout.shipping.state" /></label>
								<div class="select-wrapper">
									<select name="state" class="shipping-address-field-select" id="state" aria-required="true">
										<option value=""><fmt:message key="checkout.shipping.state.select"/></option>
										<dsp:droplet name="ForEach">
											<dsp:param name="array" bean="/atg/commerce/util/StateList_US.places" />
											<dsp:param name="elementName" value="state" />
											<dsp:oparam name="output">
												<dsp:getvalueof var="stateCode" param="state.code"/>
												<c:choose>
													<c:when test="${stateCode eq stateAddress}">
														<option value="${stateCode}" selected="selected"><dsp:valueof
																param="state.code" /></option>
													</c:when>
													<c:otherwise>
														<option value="${stateCode}"><dsp:valueof
																param="state.code" /></option>
													</c:otherwise>
												</c:choose>
											</dsp:oparam>
										</dsp:droplet>
									</select>
								</div>
							</div>							
							<div class="shipping-address-field adjust-margin zip-field-adjust tru-float-label-field">
								<div class="shipping-address-field-required orange-astericks">&#42;</div>
								<fmt:message key="checkout.shipping.zip" var="zipLabel" />
								<label class="tru-float-label" for="postalCode">${zipLabel}</label>								
								<input id="postalCode" class="shipping-address-field-input tru-float-label-input" type="text" name="postalCode" value="${postalCode}" maxlength="10" aria-required="true" placeholder="${zipLabel}"/>
								<div class="row">
									<div class="col-sm-1 col-sm-push-10">
										<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
									</div>
								</div>					
							</div>
							<div class="shipping-address-field adjust-margin tru-float-label-field">
								<div class="shipping-address-field-required orange-astericks">&#42;</div>
								<fmt:message key="checkout.shipping.telephone" var="telephoneLabel" />
								<label class="tru-float-label" for="phoneNumber">${telephoneLabel}</label>										
								<input id="phoneNumber" class="shipping-address-field-input tru-float-label-input" type="text" name="phoneNumber" value="${phoneNumber}" class="splCharCheck" placeholder="${telephoneLabel}" onkeypress="return isPhoneNumberFormat(event)" maxlength="20" aria-required="true"/>
								<div class="row">
									<div class="col-sm-1 col-sm-push-10">
										<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
									</div>
								</div>					
							</div>
							<input id="newBillingAddress" name="newBillingAddress" type="hidden" value="true" />
							<input id="editBillingAddress" name="editBillingAddress" type="hidden" value="false" />
							<input id="addressValidated" name="addressValidated" type="hidden" value="false" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</dsp:page>