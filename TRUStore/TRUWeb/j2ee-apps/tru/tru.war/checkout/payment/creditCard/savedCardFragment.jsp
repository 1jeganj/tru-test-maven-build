<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/commerce/order/purchase/PaymentGroupContainerService" />
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler" />
	<dsp:importbean bean="/com/tru/commerce/order/droplet/AvailableCreditCardsDroplet"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:getvalueof var="creditCardNickName" param="creditCardNickName" />
	<dsp:getvalueof var="isAjaxCall" param="isAjaxCall" />
	<dsp:getvalueof var="changeCreditCard" vartype="java.lang.boolean" param="changeCreditCard" />
	<dsp:getvalueof var="giftCardsExist" param="giftCardsExist" />
	<dsp:getvalueof var="pageName" param="pageName" /> 
	<div>
		<c:if test="${changeCreditCard}">
			<dsp:setvalue bean="BillingFormHandler.giftCardsExist" value="${giftCardsExist}" />
			<dsp:setvalue bean="BillingFormHandler.creditCardNickName" paramvalue="creditCardNickName" />
			<dsp:setvalue bean="BillingFormHandler.updateOrderWithExistingCard" value="submit" />
			<div class="expErrorMessage">
				<dsp:droplet name="/atg/dynamo/droplet/Switch">
		           <dsp:param bean="BillingFormHandler.formError" name="value" />
		           <dsp:oparam name="true">
							<dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
						       <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
						       	<dsp:oparam name="outputStart">
						       		<span class="errorDisplay">
						       	</dsp:oparam>
						        <dsp:oparam name="output">
						            <span><dsp:valueof param="message"/></span>
						         </dsp:oparam>
						         <dsp:oparam name="outputEnd">
									</span>
								 </dsp:oparam>
						     </dsp:droplet>
					</dsp:oparam>
					<dsp:oparam name="false">
					</dsp:oparam>
				</dsp:droplet>
			</div>
		</c:if>
		<%-- <div class="billingInfoContent">
			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="PaymentGroupContainerService.paymentGroupMap" />
				<dsp:param name="elementName" value="creditCard" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="containerCreditCardNickName" param="key" />
					<dsp:getvalueof var="pgType" param="creditCard.paymentGroupClassType"/>
					<c:if test="${pgType eq 'creditCard'}">
					------------------------------${creditCardNickName } 00000 ${containerCreditCardNickName }
						<c:if test="${creditCardNickName eq containerCreditCardNickName}">
							<dsp:getvalueof var="selectedCreditCard" param="creditCard" />
							Commented and moved code to paymentBillingAddressForm.jsp for checkout billing address cr
							<dsp:include page="/checkout/payment/creditCard/displayBillingAddress.jsp">
								<dsp:param name="address" value="${selectedCreditCard.billingAddress}" />
								<dsp:param name="addressNickName" value="${selectedCreditCard.billingAddressNickName}" />
								<dsp:param name="hasSavedCards" value="true" />
							</dsp:include>
							Commented and moved code to paymentBillingAddressForm.jsp for checkout billing address cr
							creditCardNumber: <input name="ccnumber" id="creditCardNumber" type="text" value="${selectedCreditCard.creditCardNumber}" />
							nameOnCard: <input name="nameOnCard" id="nameOnCard" type="text" value="${selectedCreditCard.nameOnCard}" />
							expirationMonth: <input name="expirationMonth" id="expirationMonth" type="text" value="${selectedCreditCard.expirationMonth}" />
							expirationYear: <input name="expirationYear" id="expirationYear" type="text" value="${selectedCreditCard.expirationYear}" />
						</c:if>
					</c:if>	
				</dsp:oparam>
			</dsp:droplet>
		</div> --%>
		<c:if test="${isAjaxCall ne false }">
			<div class="priceSummary">
				<dsp:include page="/checkout/common/priceSummary.jsp">
					<dsp:param name="pageName" param="pageName" />
				</dsp:include>
			</div>
			<div id="saved_card_details">
				<dsp:droplet name="AvailableCreditCardsDroplet">
					<dsp:param name="paymentGroupMapContainer" bean="PaymentGroupContainerService" />
					<dsp:param name="order" bean="ShoppingCart.current" />
					<dsp:param name="profile" bean="Profile" />
					<dsp:oparam name="output">
						<dsp:getvalueof var="creditCardNickName" param="creditCardNickName" />
						<dsp:setvalue param="creditCardsMap" paramvalue="creditCardsMap" />
						<dsp:getvalueof var="creditCard" param="creditCardsMap.${creditCardNickName}"/>
						<input name="saveCardType" id="credit_Card_Type" type="hidden" value="${creditCard.creditCardType}" />
						<input name="ccnumber" id="creditCardNumber" type="hidden" value="${creditCard.creditCardNumber}" />
						<input name="nameOnCard" id="nameOnCard" type="hidden" value="${creditCard.nameOnCard}" />
						<input name="expirationMonth" id="expirationMonth" type="hidden" value="${creditCard.expirationMonth}" />
						<input name="expirationYear" id="expirationYear" type="hidden" value="${creditCard.expirationYear}" />
						<input name="isSaved_Card" id="isSaved_Card" type="hidden" value="true" />
					</dsp:oparam>
				</dsp:droplet>
			</div>
			<c:if test="${giftCardsExist eq true }">
				<div class="giftCardList">
					<%-- <dsp:include page="/checkout/payment/displayGiftCards.jsp" /> --%>
				</div>
			</c:if>
		</c:if>
	</div>
</dsp:page>