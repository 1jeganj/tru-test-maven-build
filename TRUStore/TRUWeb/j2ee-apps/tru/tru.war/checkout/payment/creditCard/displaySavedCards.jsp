<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<div class="col-md-6 your-saved-cards-background set-height padding-adjustment-right padding-adjustment-left"
		id="savedCreditCards" style="height: 456px;">
		<div class="row">
			<div class="col-md-12">
				<h3>
					<fmt:message key="myaccount.your.saved.cards" />
				</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="small-spacer"></div>
			</div>
		</div>
		<div class="tse-scrollable">
			<div class="tse-scrollbar" style="display: none;">
				<div class="drag-handle visible"></div>
			</div>
			<div class="tse-scroll-content">
				<div class="tse-content">
					<div class="row">
						<div class="col-md-12">
							<dsp:include page="displayCCFromProfile.jsp"></dsp:include> 
                        </div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="small-spacer"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<p class="blue">
								<a href="#" onclick="onAddCreditCardOverlay_checkout();"><fmt:message key="myaccount.add.another.card" /></a>
							</p>
						</div>
					</div>
            	</div>
			</div>
		</div>

		
	</div>
</dsp:page>