<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/com/tru/common/droplet/DisplayMonthAndYearDroplet" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<input type="hidden" name="cardinalToken" id="cardinalToken" value=""/>
	
	<%-- Start - is check for billing address same as shipping --%>
	<%-- Commented and moved code to paymentBillingAddressForm.jsp for checkout billing address cr --%>
	<%-- <dsp:getvalueof var="billingAddressIsSame" value="false" />
	<dsp:getvalueof var="isEditBillingAddress" param="isEditBillingAddress"  vartype="java.lang.boolean"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService"/>
	<dsp:droplet name="ForEach">
		<dsp:param name="array" bean="ShippingGroupContainerService.shippingGroupMapForDisplay" />
		<dsp:param name="elementName" value="shippingGroup" />
		<dsp:oparam name="output">
				<c:if test="${not billingAddressIsSame}">
					<dsp:getvalueof var="shippingGroupType" param="shippingGroup.shippingGroupClassType" />
					<dsp:getvalueof var="billingAddressIsSame" param="shippingGroup.billingAddressIsSame" />
					<c:if test="${billingAddressIsSame}">
						<dsp:getvalueof var="addressNickName" param="shippingGroup.nickName" />
						<dsp:getvalueof var="shippingAddress" param="shippingGroup.shippingAddress" />
					</c:if>
				</c:if>
		</dsp:oparam>
	</dsp:droplet> --%>
	<%-- Commented and moved code to paymentBillingAddressForm.jsp for checkout billing address cr --%>
	<%-- End - is check for billing address same as shipping --%>
	<div class="col-md-10 credit-card-left-section">
		<div class="row">
			<div class="col-md-8">
				<div class="shipping-address-field adjust-margin tru-float-label-field">
					<div class="shipping-address-field-required orange-astericks">&#42;</div>
					<fmt:message key="checkout.overlay.creditCardHolderName" var="ccNameLabel" />
					<label class="tru-float-label" for="nameOnCard">${ccNameLabel}</label>
					<input id="nameOnCard" class="shipping-address-field-input tru-float-label-input" type="text" name="nameOnCard" value="" placeholder="${ccNameLabel}"/>
					<div class="row">
						<div class="col-sm-1 col-sm-push-10">
							<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
						</div>
					</div>
				</div>				
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="shipping-address-field adjust-margin tru-float-label-field">
					<div class="shipping-address-field-required orange-astericks">&#42;</div>
					<fmt:message key="checkout.payment.cardNumber" var="ccNumberLabel" />
					<label class="tru-float-label" for="ccnumber">${ccNumberLabel}</label>
					<input id="ccnumber" class="shipping-address-field-input tru-float-label-input card-number chkNumbersOnly" type="text" name="cardNumber" value="" placeholder="${ccNumberLabel}" onkeypress="return isNumberOnly(event,16)" onkeyup="identifyCardType(this, event)" />
				</div>
				<div class=" display-card check-mark">
					<img class="valid-credit-card" src="${TRUImagePath}images/Payment_Green-Checkmark.jpg" alt="">
				</div>
				<div class=" display-card">
					<img class="valid-credit-card credit-card" src="" alt="">
				</div>				
			</div>
			<div class="col-md-2 col-md-push-1">
				<div class="shipping-address-field credit-card-expires">
					<div class="shipping-address-field-absolute-label">
						<span class="orange-astericks">&#42;</span>&nbsp;<label class="shipping-address-field-label" for="expirationMonth"><fmt:message
								key="checkout.payment.expiry" /></label>						
					</div>			
					<select name="expirationMonth" id="expirationMonth" class="shipping-address-field-select">
						<option value=""><fmt:message key="checkout.payment.credit.card.month"/></option>
						<dsp:droplet name="DisplayMonthAndYearDroplet">
							<dsp:param name="monthList" value="monthList" />
							<dsp:oparam name="output">
								<dsp:droplet name="ForEach">
									<dsp:param name="array" param="monthList" />
									<dsp:param name="elementName" value="month" />
									<dsp:oparam name="output">
										<dsp:getvalueof var="month" param="month" />
										<option value="${month}" ><dsp:valueof param="month" /></option>
									</dsp:oparam>
								</dsp:droplet>
							</dsp:oparam>
						</dsp:droplet>
					</select>
				</div>
			</div>
			<div class="col-md-2 col-md-push-1">
				<div class="shipping-address-field credit-card-expires">		
				<select name="expirationYear" id="expirationYear" class="shipping-address-field-select" aria-label="expiration Year">
					<option value=""><fmt:message key="checkout.payment.credit.card.year"/></option>
					<dsp:droplet name="DisplayMonthAndYearDroplet">
						<dsp:param name="yearList" value="yearList" />
						<dsp:oparam name="output">
							<dsp:droplet name="ForEach">
								<dsp:param name="array" param="yearList" />
								<dsp:param name="elementName" value="year" />
								<dsp:oparam name="output">
									<dsp:getvalueof var="year" param="year" />
									<option value="${year}" ><dsp:valueof param="year" /></option>
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>
				</select>
				</div>
			</div>
			<div class="col-md-3 col-md-push-1">
				<div class="shipping-address-field adjust-margin tru-float-label-field">
					<div class="shipping-address-field-required orange-astericks">&#42;</div>
					<fmt:message key="checkout.payment.securityCode" var="ccSecurityCodeLabel" />
					<label class="tru-float-label" for="creditCard_CVV">${ccSecurityCodeLabel}</label>
					<input id="creditCard_CVV" class="shipping-address-field-input tru-float-label-input creditCardCVV splCharCheck security-code rusCard" type="password" name="creditCardCVV" value="" placeholder="${ccSecurityCodeLabel}" maxlength="4" />
				</div>			
			</div>
			<div class="col-md-1 col-md-push-1">
				<a data-target="#creditCardModal" data-toggle="modal" href="javascript:void(0)" onclick="javascript:forConsoleErrorFixing(this);"><fmt:message key="checkout.whats.this"/></a>
			</div>		
		</div>
		<input type="hidden" name="orderSet" value="false" />
		<input type="hidden" name="newCreditCard" value="true" />
		<input type="hidden" name="editCreditCard" value="false" />
		<input name="isSaved_Card" id="isSaved_Card" type="hidden" value="false" />
		
		<%-- Commented and moved code to paymentBillingAddressForm.jsp for checkout billing address cr --%>
		<%-- <c:choose>
			<c:when test="${billingAddressIsSame and isEditBillingAddress}">
				<dsp:include page="/checkout/payment/billingAddressForm.jsp" >
					<dsp:param name="nickNameInHiddenSpan" value="${addressNickName}" />
				</dsp:include>
			</c:when>
			<c:when test="${not billingAddressIsSame}">
				<dsp:include page="/checkout/payment/billingAddressForm.jsp" />
			</c:when>
		</c:choose> --%>
		<%-- Commented and moved code to paymentBillingAddressForm.jsp for checkout billing address cr --%>
	</div>
	<%-- Commented and moved code to paymentBillingAddressForm.jsp for checkout billing address cr --%>
	<%-- <c:if test="${billingAddressIsSame and not isEditBillingAddress}">
		<div id="billing-info">
			<dsp:include page="/checkout/payment/creditCard/displayBillingAddress.jsp">
				<dsp:param name="addressNickName" value="${addressNickName}" />
				<dsp:param name="address" value="${shippingAddress}" />
				<dsp:param name="hasSavedCards" value="false" />
			</dsp:include>
		</div>
	</c:if> --%>
	<%-- Commented and moved code to paymentBillingAddressForm.jsp for checkout billing address cr --%>
</dsp:page>