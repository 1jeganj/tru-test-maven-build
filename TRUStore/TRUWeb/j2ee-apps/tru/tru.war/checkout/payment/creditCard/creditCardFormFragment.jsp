<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/com/tru/commerce/order/droplet/AvailableCreditCardsDroplet"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/PaymentGroupContainerService" />
	<dsp:importbean bean="/com/tru/common/TRUUserSession" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:getvalueof var="order" bean="ShoppingCart.current" />
	<dsp:getvalueof var="rusCardSelected" value="${order.rusCardSelected}" />
	<dsp:getvalueof var="rusCardSelectedCheck" bean="TRUUserSession.rusCardSelected"/>
	<dsp:getvalueof var="isCreditCardSelected" param="isCreditCardSelected" />
	<dsp:getvalueof var="isRadioCreditcardClicked" param="isRadioCreditcardClicked" />
	<div class="row">
		<div class="col-md-7">
			<c:choose>
				<c:when test="${(isCreditCardSelected and not rusCardSelected and not rusCardSelectedCheck) or (isRadioCreditcardClicked eq 'true')}">
					<input name="payment-type" id="radio-creditcard" tabindex="0" checked="checked" class="inline" type="radio" value="creditCard" aria-label="paymentMode"/>
				</c:when>
				<c:otherwise>
					<input name="payment-type" id="radio-creditcard" tabindex="0" class="inline" type="radio" value="creditCard" aria-label="paymentMode"/>
				</c:otherwise>
			</c:choose>
			<label for="creditcard" class="creditOrDebitPayament"><fmt:message key="checkout.payment.cardPayment" /></label>
		</div>
		<div class="payment-logos col-md-5">
			<div class="rewardsrus-mastercard-logo-sm"></div>
			<div class="rewardsrus-card-logo-sm"></div>
			<div class="amex-logo-sm"></div>
			<div class="discover-logo-sm"></div>
			<div class="mastercard-logo-sm"></div>
			<div class="visa-logo-sm"></div>
		</div>
	</div>
	
	<div class="credit-card-hidden row" <c:if test='${isCreditCardSelected and not rusCardSelected and not rusCardSelectedCheck}'>style="display: block;"</c:if>>
		<form id="paymentCreditCardForm" class="JSValidation" method="POST" action="${contextPath}checkout/payment/payment.jsp">
			<input type="hidden" name="respJwt" id="respJwt" value=""/>
			<input type="hidden" value="" name="cBinNum" id="cBinNum" />
			<input type="hidden" value="" name="cLength" id="cLength" />
			<dsp:droplet name="AvailableCreditCardsDroplet">
				<dsp:param name="paymentGroupMapContainer" bean="PaymentGroupContainerService" />
				<dsp:param name="order" bean="ShoppingCart.current" />
				<dsp:param name="profile" bean="Profile" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="defaultCreditCardName" param="defaultCreditCardName" />
					<dsp:setvalue param="creditCardsMap" paramvalue="creditCardsMap" />
					<dsp:getvalueof var="defaultCreditCardNumber"  param="creditCardsMap.${defaultCreditCardName}.creditCardNumber" />
					<dsp:getvalueof var="creditCardType"  param="creditCardsMap.${defaultCreditCardName}.creditCardType" />
					<dsp:include page="/checkout/payment/creditCard/savedCards.jsp">
						<dsp:param name="creditCardsMap" param="creditCardsMap" />
						<dsp:param name="defaultCreditCardName" param="defaultCreditCardName" />
					</dsp:include>
				</dsp:oparam>
				<dsp:oparam name="empty">
					<div id="newCreditCardForm">
						<dsp:include page="/checkout/payment/creditCard/newCreditCardForm.jsp" />
					</div>
				</dsp:oparam>
			</dsp:droplet>
		</form>
	</div>
	<div id="six-month-financing-credit-card-div">
      <c:if test="${creditCardType ne 'RUSCoBrandedMasterCard' and creditCardType ne 'RUSPrivateLabelCard' }">
      	<c:set var="defaultCreditCardNumber" value=""/>
      </c:if>
		<dsp:include page="/checkout/payment/specialFinancing.jsp">
			<dsp:param name="creditCardNumber" value="${defaultCreditCardNumber}" />
			<dsp:param name="cardType" value="credit-card" />
			<dsp:param name="isCreditCardSelected" value="${isCreditCardSelected}" />
		</dsp:include>
	</div>
	
</dsp:page>