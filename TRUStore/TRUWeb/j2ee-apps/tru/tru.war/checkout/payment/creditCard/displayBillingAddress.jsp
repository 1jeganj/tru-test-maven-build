<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<%-- This page is the fragment for displaying billing address --%>
		<dsp:importbean bean="/atg/commerce/order/purchase/PaymentGroupContainerService" />
		<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService" />
		<dsp:importbean bean="/atg/commerce/ShoppingCart" />
		<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
		<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
		<dsp:importbean bean="/atg/userprofiling/Profile" />
		<dsp:getvalueof var="isUserAnonymous" bean="Profile.transient" />
		<dsp:getvalueof var="billingAddress" bean="ShoppingCart.current.billingAddress" />
		<dsp:getvalueof var="billingAddressNickName" bean="ShoppingCart.current.billingAddressNickName" />
		<dsp:getvalueof var="selectedAddressNickName" param="selectedAddressNickName" />
		<div class="billing-info credit-component col-md-5">
				<div class="saved-billing-address">
					<div class="checkout-page-form-header">
						<fmt:message key="checkout.payment.billingInformation" />
					</div>
					<c:if test="${!isUserAnonymous}">
						<div class="row">
							<div class="col-md-12">
								<label class="select-address-label"><fmt:message key="checkout.shipping.selectAddress"/></label>
								<div class="select-wrapper-wide">
									<select name="selectedCardBillingAddress" id="selectedCardBillingAddress" class="select-billing-address blue" onchange="onBillingAddressChangeOnPaymentPage();"
									aria-label="Select Billing Address" title=" ">
								<dsp:droplet name="ForEach">
									<dsp:param name="array" bean="ShippingGroupContainerService.shippingGroupMapForDisplay" />
									<dsp:param name="elementName" value="shippingGroup" />
									<dsp:oparam name="outputStart">
										<dsp:getvalueof var="orderBillingAddr"  bean="ShippingGroupContainerService.shippingGroupMapForDisplay.${billingAddressNickName}.shippingAddress" />
										<dsp:getvalueof var="address1" value="${orderBillingAddr.address1}" />
										<option selected="selected">${address1}</option>
									</dsp:oparam>
									<dsp:oparam name="output">
										<dsp:getvalueof var="addressKey" param="key" />
										<dsp:getvalueof var="isBillingAddress" param="shippingGroup.billingAddress" />
										<c:if test="${billingAddressNickName ne  addressKey and isBillingAddress}">
											<dsp:getvalueof var="secondaryAddress" param="shippingGroup.shippingAddress" />
											<dsp:getvalueof var="address1" value="${secondaryAddress.address1}" />
											<option value="${addressKey}">${address1}</option>
										</c:if>
									</dsp:oparam>
									<dsp:oparam name="outputEnd">
										<option value="addDropdownAddress"><fmt:message key="myaccount.add.dropdown.address" /></option>
									</dsp:oparam>
								</dsp:droplet>
							</select>
									<a href="javascript:void(0);" class="checkoutEditBillingAddress_cancel">
										<fmt:message key="myaccount.cancel" />
									</a>
									<input type="hidden" name="isEditCCAddressAvailable" id="isEditCCAddressAvailable" value="${isEditCCAddressAvailable}" />
									<input type="hidden" name="billingAddressValidated" value="false" />
								</div>
							</div>
						</div>
					</c:if>					
					<div>
						<span class="saved-billing-address__names">${billingAddress.firstName}&nbsp;${billingAddress.lastName}&nbsp;&#183;&nbsp;</span>
						<a href="javascript:void(0);" class="checkoutEditBillingAddress">
							<fmt:message key="myaccount.edit" />
						</a>
					</div>
					<div>${billingAddress.address1},&nbsp;${billingAddress.address2 }</div>
					<div>${billingAddress.city},&nbsp;${billingAddress.state},&nbsp;${billingAddress.postalCode }</div>
					<dsp:getvalueof var="phoneNumberTemp" value="${billingAddress.phoneNumber}" />
					<c:if test="${fn:length(phoneNumberTemp) gt 0}">
						<p>(${fn:substring(phoneNumberTemp, 0,3)})&nbsp;${fn:substring(phoneNumberTemp, 3,6)}-${fn:substring(phoneNumberTemp,
							6, fn:length(phoneNumberTemp))}</p>
					</c:if>
					<div>${billingAddress.country}</div>
					<input name="nickName" type="hidden" value="${billingAddressNickName}" />
					<input name="country" type="hidden" id="country" value="${billingAddress.country}" />
					<input name="firstName" type="hidden" id="firstName" value="${billingAddress.firstName}" />
					<input name="lastName" type="hidden" id="lastName" value="${billingAddress.lastName}" />
					<input name="address1" type="hidden" id="address1" value="${billingAddress.address1}" />
					<input name="address2" type="hidden" id="address2" value="${billingAddress.address2}" />
					<input name="city" type="hidden" id="city" value="${billingAddress.city}" />
					<input name="state" type="hidden" id="state" value="${billingAddress.state}" />
					<input name="otherState" type="hidden" id="otherState" value="${billingAddress.state}" />
					<input name="postalCode" type="hidden" id="postalCode" value="${billingAddress.postalCode}" />
					<input name="phoneNumber" type="hidden" id="phoneNumber" value="${billingAddress.phoneNumber}" />
					<input id="newBillingAddress" name="newBillingAddress" type="hidden" value="false" />
					<input id="editBillingAddress" name="editBillingAddress" type="hidden" value="false" />
					<input name="addressValidated" type="hidden" value="true" />
				</div>
		</div>
</dsp:page>