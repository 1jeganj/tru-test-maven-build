<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/PaymentGroupContainerService" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService"/>
	<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUCompareAddressesDroplet"/>
	
	<dsp:getvalueof var="creditCard" param="creditCard"/>
	<dsp:getvalueof var="selectedAddressNickName" param="selectedAddressNickName"/>
	<dsp:getvalueof var="isEditSavedCard" vartype="java.lang.boolean" param="isEditSavedCard"/>
	<dsp:getvalueof var="showAddressFields" vartype="java.lang.boolean" param="showAddressFields"/>
	<dsp:getvalueof var="fromSuggestedAddress" vartype="java.lang.boolean" param="fromSuggestedAddress"/>
	<dsp:getvalueof var="billingAddressNickname" param="billingAddressNickname" />
	<dsp:getvalueof var="isLastOptionSelected" vartype="java.lang.boolean" param="isLastOptionSelected"/>
	<dsp:getvalueof var="editCardNickNameID" param="editCardNickNameID" />
	<dsp:getvalueof var="creditCardNickName" param="creditCardNickName" />
	<dsp:getvalueof var="editCardNickName" param="editCardNickName"/>
	<dsp:getvalueof var="isBillingAddressFromAddressBook" vartype="java.lang.boolean" value="false"/>
	<dsp:getvalueof var="formID" param="formID"/>
	
	<dsp:droplet name="IsEmpty">
		<dsp:param name="value" param="editCardNickNameID" />
		<dsp:oparam name="false">
			<dsp:getvalueof var="editCardNickName" param="editCardNickNameID" />
			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="PaymentGroupContainerService.paymentGroupMap" />
				<dsp:param name="elementName" value="creditCard" />
				<dsp:oparam name="output">
				<dsp:getvalueof var="cardNickname" param="key" />
				<c:choose>
					<c:when test="${(not empty editCardNickName and cardNickname eq editCardNickName)}">
						<dsp:getvalueof var="creditCard" param="creditCard" />
					</c:when> 
				</c:choose>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="true">
			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="PaymentGroupContainerService.paymentGroupMap" />
				<dsp:param name="elementName" value="creditCard" />
				<dsp:oparam name="output">
				<dsp:getvalueof var="cardNickname" param="key" />
				<c:choose>
					<c:when test="${creditCardNickName eq cardNickname}">
						<dsp:getvalueof var="creditCard" param="creditCard" />
					</c:when> 
				</c:choose>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="ForEach">
		<dsp:param name="array" bean="ShippingGroupContainerService.shippingGroupMapForDisplay" />
		<dsp:param name="elementName" value="shippingGroup" />
		<dsp:oparam name="output">
			<dsp:setvalue param="secondaryAddress" paramvalue="shippingGroup.shippingAddress" />
			<dsp:getvalueof var="adressNickName" param="key" />
			<dsp:getvalueof var="secondaryAddress" param="secondaryAddress" />
			<c:choose>
				<c:when test="${creditCard.billingAddressNickName eq adressNickName}">
					<dsp:droplet name="TRUCompareAddressesDroplet">
						<dsp:param name="address1" value="${secondaryAddress}"/>
						<dsp:param name="address2" value="${creditCard.billingAddress}"/>
						<dsp:oparam name="true">
							<dsp:getvalueof var="address" value="${secondaryAddress}"/>
						</dsp:oparam>
						<dsp:oparam name="false">
							<dsp:getvalueof var="address" value="${creditCard.billingAddress}"/>
						</dsp:oparam>
					</dsp:droplet>
					<dsp:getvalueof var="isBillingAddressFromAddressBook" vartype="java.lang.boolean" value="true"/>
					<dsp:getvalueof var="billingAddressNickname" param="key" />
				</c:when>
				<c:when test="${(not empty selectedAddressNickName and adressNickName eq selectedAddressNickName)}">
					<dsp:getvalueof var="address" param="secondaryAddress" />
					<dsp:getvalueof var="billingAddressNickname" param="key" />
					<dsp:getvalueof var="isBillingAddressFromAddressBook" vartype="java.lang.boolean" value="true"/>
				</c:when> 
				<c:when test="${(not empty isEditSavedCard and isBillingAddressFromAddressBook eq false)}">
					<dsp:getvalueof var="address" value="${creditCard.billingAddress}" />
					<%-- <dsp:getvalueof var="billingAddressNickname" param="key" /> --%>
					<dsp:getvalueof var="isBillingAddressFromAddressBook" vartype="java.lang.boolean" value="false"/>
					</c:when>
			</c:choose>
		</dsp:oparam>
	</dsp:droplet>
	<div id="billing-address-container">
		<div class="row lef-padding billing-address">
			<div class="col-md-12">
				<div class="spacer"></div>
			</div>
		</div>
			<div class="row left-padding billing-address add-address">
				<div class="col-md-12">
					<h3><fmt:message key="myaccount.billing.address" /></h3>
				</div>
			</div>
			<div class="row left-padding billing-address pull-up add-address">
				<div class="col-md-9">
					<label>
						<span class="avenir-heavy">
							<span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<fmt:message key="myaccount.address.country" />
						</span>
					</label>&nbsp;<br>
					<div class="select-wrapper-wide">
						<input type="hidden" id="isBillingAddressFromAddressBook" name="isBillingAddressFromAddressBook" value="${isBillingAddressFromAddressBook}"/>
						<input type="hidden" name="billingAddressValidated" value="false" />
						<c:choose>
							<c:when test="${fromSuggestedAddress eq true }">
								<select name="country" id="country" onchange="onCountryChange();">
									<dsp:droplet name="ForEach">
									  <dsp:param name="array" bean="/atg/commerce/util/CountryList.places"/>
									  <dsp:param name="elementName" value="billingCountry"/>
									  <dsp:oparam name="output">
										  <dsp:getvalueof var="billingCountry" param="billingCountry.code"></dsp:getvalueof>
										  <option value='<dsp:valueof param="billingCountry.code" />'
		                                        <dsp:droplet name="/atg/dynamo/droplet/Compare">
		                                                <dsp:param name="obj1" param="billingCountry.code"/>
		                                                <dsp:param name="obj2" param="country" />
		                                                <dsp:oparam name="equal">
		                                                    selected="selected"
		                                                </dsp:oparam>
		                                        </dsp:droplet> >
		                                        <dsp:valueof param="billingCountry.displayName"/>
		                                   </option>
									  </dsp:oparam>
									</dsp:droplet>
								</select>
							</c:when>
							<c:when test="${isBillingAddressFromAddressBook eq true}"> 
								<select name="country" id="country" onchange="onCountryChange();">
									<dsp:droplet name="ForEach">
									  <dsp:param name="array" bean="/atg/commerce/util/CountryList.places"/>
									  <dsp:param name="elementName" value="billingCountry"/>
									  <dsp:oparam name="output">
										  <dsp:getvalueof var="billingCountry" param="billingCountry.code"></dsp:getvalueof>
										  <option value='<dsp:valueof param="billingCountry.code" />'
		                                        <dsp:droplet name="/atg/dynamo/droplet/Compare">
		                                                <dsp:param name="obj1" param="billingCountry.code"/>
		                                                <dsp:getvalueof var="billingAddressCountry" value="${address.country}"></dsp:getvalueof>
		                                                <c:choose>
		                                                	<c:when test="${not empty billingAddressCountry}">
		                                                		<dsp:param name="obj2" value="${billingAddressCountry}"/>
		                                                	</c:when>
		                                                	<c:otherwise>
		                                                		<dsp:param name="obj2" value="US"/>
		                                                	</c:otherwise>
		                                                </c:choose>
		                                                <dsp:oparam name="equal">
		                                                    selected="selected"
		                                                </dsp:oparam>
		                                        </dsp:droplet> >
		                                        <dsp:valueof param="billingCountry.displayName"/>
		                                   </option>
									  </dsp:oparam>
									</dsp:droplet>
								</select>
							</c:when>
							<c:otherwise>
								<select name="country" id="country" onchange="onCountryChange();">
									<dsp:droplet name="ForEach">
									  <dsp:param name="array" bean="/atg/commerce/util/CountryList.places"/>
									  <dsp:param name="elementName" value="billingCountry"/>
									  <dsp:oparam name="output">
										  <dsp:getvalueof var="billingCountry" param="billingCountry.code"></dsp:getvalueof>
										  <option value='<dsp:valueof param="billingCountry.code" />'
		                                        <dsp:droplet name="/atg/dynamo/droplet/Compare">
		                                                <dsp:param name="obj1" param="billingCountry.code"/>
		                                                <dsp:getvalueof var="billingAddressCountry" value="${creditCard.billingAddress.country}"></dsp:getvalueof>
		                                                <c:choose>
		                                                	<c:when test="${not empty billingAddressCountry}">
		                                                		<dsp:param name="obj2" value="${billingAddressCountry}"/>
		                                                	</c:when>
		                                                	<c:otherwise>
		                                                		<dsp:param name="obj2" value="US"/>
		                                                	</c:otherwise>
		                                                </c:choose>
		                                                <dsp:oparam name="equal">
		                                                    selected="selected"
		                                                </dsp:oparam>
		                                        </dsp:droplet> >
		                                       <dsp:valueof param="billingCountry.displayName" />
		                                   </option>
									  </dsp:oparam>
									</dsp:droplet>
								</select>
							</c:otherwise>
							</c:choose>
					</div>
					<label for="id-4926c9d7-e11a-c398-7bfe-346d886f21b2">
						<span class="avenir-heavy">
							<span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<fmt:message key="myaccount.address.nickname" />
						</span>
					</label>
					<%-- If the address is available in secondary address, then nick name is displayed without edit mode, 
											Otherwise new nickname can be entered by user --%>
					<c:choose>
						<c:when test="${fromSuggestedAddress eq true }">
							<c:choose>
								<c:when test="${isEditSavedCard eq true and not empty billingAddressNickname}">
									<input type="text" name="nickNameTmp" value="${billingAddressNickname}" maxlength="42" disabled="true"/>
									<input type="hidden" name="nickName" id="id-4926c9d7-e11a-c398-7bfe-346d886f21b2"	value="${billingAddressNickname}" maxlength="42"/>
								</c:when>
								<c:when test="${(not empty isEditSavedCard and isBillingAddressFromAddressBook eq false)}">
									<dsp:getvalueof var="nickName" param="nickName"/>
									<input type="text" name="nickName" id="id-4926c9d7-e11a-c398-7bfe-346d886f21b2"	value="${nickName}" maxlength="42"/>
								</c:when>
								<c:otherwise>
									<input type="text" name="nickName" id="id-4926c9d7-e11a-c398-7bfe-346d886f21b2"	value="${billingAddressNickname}" maxlength="42"/>
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:when test="${isEditSavedCard eq true and not empty billingAddressNickname}">
							<input type="text" name="nickNameTmp" value="${billingAddressNickname}" maxlength="42" disabled="true"/>
							<input type="hidden" name="nickName" id="id-4926c9d7-e11a-c398-7bfe-346d886f21b2"	value="${billingAddressNickname}" maxlength="42"/>
						</c:when>
						<c:when test="${not empty selectedAddressNickName and selectedAddressNickName != ' '}">
							<input type="text" name="nickNameTmp" value="${selectedAddressNickName}" maxlength="42" disabled="true"/>
							<input type="hidden" name="nickName" id="id-4926c9d7-e11a-c398-7bfe-346d886f21b2"	value="${selectedAddressNickName}" maxlength="42"/>
						</c:when>
						<c:otherwise>
							<input type="text" name="nickName" id="id-4926c9d7-e11a-c398-7bfe-346d886f21b2"	placeholder="(Home, Office)" maxlength="42"/>
						</c:otherwise>
					</c:choose>
					
					<label for="id-ef455140-0f0c-1e35-ac5f-b481a66a3e84">
						<span class="avenir-heavy"><span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>
							 <fmt:message key="myaccount.address.firstname" />
						</span>
					</label>
					
					<c:choose>
						<c:when test="${fromSuggestedAddress eq true }">
							<input type="text" name="firstName" id="id-ef455140-0f0c-1e35-ac5f-b481a66a3e84" value="${firstName}" maxlength="30"/>
						</c:when>
						<c:otherwise>
							<input type="text" name="firstName"	value="${address.firstName}" id="id-ef455140-0f0c-1e35-ac5f-b481a66a3e84" maxlength="30"/>
						</c:otherwise>
					</c:choose>
					
					<label for="id-fd81cd17-fccd-6b69-a2c0-6b2f5da651f4">
						<span class="avenir-heavy"><span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>
							  <fmt:message key="myaccount.address.lastname" />
						</span>
					</label>
					
					<c:choose>
						<c:when test="${fromSuggestedAddress eq true }">
							<input type="text" name="lastName" id="id-fd81cd17-fccd-6b69-a2c0-6b2f5da651f4" value="${lastName}" maxlength="30" />
						</c:when>
						<c:otherwise>
							<input type="text" name="lastName" value="${address.lastName}" id="id-fd81cd17-fccd-6b69-a2c0-6b2f5da651f4" maxlength="30"/>
						</c:otherwise>
					</c:choose>
					
					<label for="id-760cf73e-d659-83b1-77a0-62cf6193c31e">
						<span class="avenir-heavy">
							<span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<fmt:message key="myaccount.address.addressOne" />
						</span>
					</label>
					
					<c:choose>
						<c:when test="${fromSuggestedAddress eq true }">
							<input type="text" name="address1" id="id-760cf73e-d659-83b1-77a0-62cf6193c31e" value="${address1}" maxlength="50"/>
						</c:when>
						<c:otherwise>
							<input type="text" name="address1" value="${address.address1}" id="id-760cf73e-d659-83b1-77a0-62cf6193c31e" maxlength="50"/>
						</c:otherwise>
					</c:choose>
					
					<label for="id-1eaa485e-5803-ccc9-44a4-8f440578530e">
						<p><fmt:message key="myaccount.address.apt.suite.optional" /></p>
					</label>
					
					<c:choose>
						<c:when test="${fromSuggestedAddress eq true }">
							<input type="text" name="address2" value="${address2}" id="id-1eaa485e-5803-ccc9-44a4-8f440578530e" maxlength="50"/>
						</c:when>
						<c:otherwise>
							<input type="text" name="address2" value="${address.address2}" id="id-1eaa485e-5803-ccc9-44a4-8f440578530e" maxlength="50"/>
						</c:otherwise>
					</c:choose>
					
					<label for="id-e64c310a-7751-fd76-60b1-0e4bd57a0362">
						<span class="avenir-heavy">
							<span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<fmt:message key="myaccount.address.city" />
						</span>
					</label>
					
					<c:choose>
						<c:when test="${fromSuggestedAddress eq true }">
							<input type="text" name="city" value="${city}" id="id-e64c310a-7751-fd76-60b1-0e4bd57a0362" maxlength="30"/>
						</c:when>
						<c:otherwise>
							<input type="text" name="city" value="${address.city}" id="id-e64c310a-7751-fd76-60b1-0e4bd57a0362" maxlength="30"/>
						</c:otherwise>
					</c:choose>
					<dsp:getvalueof var="country" value="${address.country}"></dsp:getvalueof>
					<dsp:getvalueof var="displayInlineblock" value=""/>
					<c:if test="${not empty country && country ne 'US'}">
						<dsp:getvalueof var="displayInlineblock" value="display-inlineblock"/>
					</c:if>

					<label class="avenir-heavy creditCardStateLabel" for="state">
						<span class="avenir-heavy">
							<span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<fmt:message key="myaccount.address.state.province" />
						</span>
					</label> 
					<div class="select-wrapper other-state">
						<c:choose>
							<c:when test="${fromSuggestedAddress eq true or isEditSavedCard eq true}">
								<c:if test="${isEditSavedCard eq true}">
									<dsp:getvalueof var="country" value="${address.country}"></dsp:getvalueof>
									<dsp:getvalueof var="state" value="${address.state}"></dsp:getvalueof>
								</c:if>
								<select name="state" id="state" class="select-state-option"  onchange="onCreditCardSelectedStateChange(this);">
									<option value=""><fmt:message key="checkout.shipping.state.select" /></option>
									<c:choose>
										<c:when test="${country eq 'US'}">
											<dsp:droplet name="ForEach">
												  <dsp:param name="array" bean="/atg/commerce/util/StateList_US.places"/>
												  <dsp:param name="elementName" value="billingState"/>
												  <dsp:oparam name="output">
													  <dsp:getvalueof var="stateCode" param="billingState.code"></dsp:getvalueof>
													  <option value="${stateCode}"
													  <dsp:droplet name="/atg/dynamo/droplet/Compare">
			                                                <dsp:param name="obj1" param="billingState.code"/>
			                                                <dsp:param name="obj2" value="${state}"/>
			                                                <dsp:oparam name="equal">
			                                                    selected="selected"
			                                                </dsp:oparam>
			                                        </dsp:droplet>
													  >
													  <dsp:valueof param="billingState.code" /></option>
												  </dsp:oparam>
											</dsp:droplet>
										</c:when>
										<c:otherwise>
											<option value="other" selected="selected">
		                                        <fmt:message key="checkout.payment.billing.other"/></option>
		                                       <%--  <dsp:getvalueof var="showOtherState" value="display-block"></dsp:getvalueof> --%>
										</c:otherwise>
									</c:choose>
								</select>
								<%-- <c:choose>
									<c:when test="${state eq 'NA'}">
										<input type="text" name="otherState" id="otherState" class="${displayInlineblock}" maxlength="20" value=""/>	
									</c:when>
									<c:otherwise>
										<input type="text" name="otherState" class="${displayInlineblock}" id="otherState" maxlength="20" value="${state}"/>
									</c:otherwise>
								</c:choose> --%>
								<label class="avenir-heavy creditCardOtherStateLabel ${displayInlineblock}" for="otherState">
									<span class="avenir-heavy">
										<fmt:message key="checkout.shipping.otherstate" />
									</span>
								</label>
								<input type="text" name="otherState" class="${displayInlineblock}" id="otherState" maxlength="20" value="${state}"/>
							</c:when>
							<c:when test="${isBillingAddressFromAddressBook eq true}">
								<select name="state" id="state" onchange="onCreditCardSelectedStateChange(this);">
									<option value=""><fmt:message key="checkout.shipping.state.select" /></option>
									<c:choose>
										<c:when test="${address.country eq 'US'}">
				                              <dsp:droplet name="ForEach">
												  <dsp:param name="array" bean="/atg/commerce/util/StateList_US.places"/>
												  <dsp:param name="elementName" value="billingState"/>
												  <dsp:oparam name="output">
													  <dsp:getvalueof var="stateCode" param="billingState.code"></dsp:getvalueof>
													  <option value="${stateCode}"
														  <dsp:droplet name="/atg/dynamo/droplet/Compare">
				                                                <dsp:param name="obj1" param="billingState.code"/>
				                                                <dsp:getvalueof var="billingAddressState" value="${address.state}"></dsp:getvalueof>
				                                                <dsp:param name="obj2" value="${billingAddressState}"/>
				                                                <dsp:oparam name="equal">
				                                                    selected="selected"
				                                                </dsp:oparam>
		                                        		  </dsp:droplet> >
		                                       		<dsp:valueof param="billingState.code" /></option>
												  </dsp:oparam>
											</dsp:droplet>
										</c:when>
										<c:otherwise>
											 <option value="other" selected="selected">other</option>
										</c:otherwise>
									</c:choose>
								</select>
								<label class="avenir-heavy creditCardOtherStateLabel ${displayInlineblock}" for="otherState">
									<span class="avenir-heavy">
										<fmt:message key="checkout.shipping.otherstate" />
									</span>
								</label>
								<c:choose>
									<c:when test="${address.state eq 'NA'}">
										<input type="text" name="otherState" class="${displayInlineblock}" id="otherState" maxlength="20" value=""/>
									</c:when>
									<c:otherwise>
										<input type="text" name="otherState" class="${displayInlineblock}" id="otherState" maxlength="20" value="${address.state}"/>
									</c:otherwise>
								</c:choose>
							</c:when>
							<c:when test="${isBillingAddressFromAddressBook eq false or not empty creditCard.billingAddress.country}">
								<select name="state" id="state" onchange="onCreditCardSelectedStateChange(this);">
									<option value=""><fmt:message key="checkout.shipping.state.select" /></option>
									<c:choose>
								<%-- 	<c:when test="${creditCard.billingAddress.country eq 'US'}"> --%>
									<c:when test="${creditCard.billingAddress.country eq 'US' or empty creditCard.billingAddress.country}"> 
			                              <dsp:droplet name="ForEach">
											  <dsp:param name="array" bean="/atg/commerce/util/StateList_US.places"/>
											  <dsp:param name="elementName" value="billingState"/>
											  <dsp:oparam name="output">
												  <dsp:getvalueof var="stateCode" param="billingState.code"></dsp:getvalueof>
												  <option value="${stateCode}"
													  <dsp:droplet name="/atg/dynamo/droplet/Compare">
			                                                <dsp:param name="obj1" param="billingState.code"/>
			                                                <dsp:param name="obj2" value="${creditCard.billingAddress.state}"/>
			                                                <dsp:oparam name="equal">
			                                                    selected="selected"
			                                                </dsp:oparam>
	                                        		  </dsp:droplet> >
	                                        	<dsp:valueof param="billingState.code" /></option>
											  </dsp:oparam>
										</dsp:droplet>
										</c:when>
										<c:otherwise>
											 <option value="other" selected="selected">other</option>
										</c:otherwise>
										</c:choose>
								</select>
								<label class="avenir-heavy creditCardOtherStateLabel ${displayInlineblock}" for="otherState">
									<span class="avenir-heavy">
										<fmt:message key="checkout.shipping.otherstate" />
									</span>
								</label>
								<input type="text" name="otherState" id="otherState" maxlength="20" value="NA"/>
							</c:when>
							<c:otherwise>
								<div class="addCreditCardPaymentPageForm">
									<select name="state" id="state" class="select-state-option"  onchange="onCreditCardSelectedStateChange(this);">
									<option value=""><fmt:message key="checkout.shipping.state.select" /></option>
			                              <dsp:droplet name="ForEach">
											  <dsp:param name="array" bean="/atg/commerce/util/StateList_US.places"/>
											  <dsp:param name="elementName" value="billingState"/>
											  <dsp:oparam name="output">
												  <dsp:getvalueof var="stateCode" param="billingState.code"></dsp:getvalueof>
												  <option value="${stateCode}"
												  <dsp:droplet name="/atg/dynamo/droplet/Compare">
		                                                <dsp:param name="obj1" param="billingState.code"/>
		                                                <dsp:param name="obj2" value="${address.state}"/>
		                                                <dsp:oparam name="equal">
		                                                    selected="selected"
		                                                </dsp:oparam>
		                                        </dsp:droplet>
												  >
												  <dsp:valueof param="billingState.code" /></option>
											  </dsp:oparam>
										</dsp:droplet>
								</select>
								</div>
								<label class="avenir-heavy creditCardOtherStateLabel ${displayInlineblock}" for="otherState">
									<span class="avenir-heavy">
										<fmt:message key="checkout.shipping.otherstate" />
									</span>
								</label>
								<div class="">
									<input type="text" id="otherState" name="otherState" value="NA" class="${displayInlineblock}" maxlength="20" />
								</div>
						</c:otherwise>
						</c:choose>
						</div>
					<label for="zip">
						<span class="avenir-heavy">
							<span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<fmt:message key="myaccount.address.zip.postal" />
						</span>
					</label>
					
					<c:choose>
						<c:when test="${fromSuggestedAddress eq true }">
							<input type="text" name="postalCode"  value="${postalCode}" id="zip" maxlength="10"/>
						</c:when>
						<c:otherwise>
							<input type="text" name="postalCode" value="${address.postalCode}" id="zip" maxlength="10" />
						</c:otherwise>
					</c:choose>
					
					<label for="id-3b6ff52e-5867-73c9-5320-9ed0e53e6f1a">
						<span class="avenir-heavy">
							<span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<fmt:message key="myaccount.address.telephone" />
						</span>
					</label>
					
					<c:choose>
						<c:when test="${fromSuggestedAddress eq true }">
							<input type="text" name="phoneNumber" value="${phoneNumber}" id="id-3b6ff52e-5867-73c9-5320-9ed0e53e6f1a" onkeypress="return isPhoneNumberFormat(event)" maxlength="20" class="splCharCheck"/>
						</c:when>
						<c:otherwise>
							<input type="text" name="phoneNumber" value="${address.phoneNumber}" id="id-3b6ff52e-5867-73c9-5320-9ed0e53e6f1a" onkeypress="return isPhoneNumberFormat(event)" maxlength="20" class="splCharCheck"/>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
			<input id="addressValidated" name="addressValidated" type="hidden" value="false" />
		<div class="row left-padding billing-address">
			<div class="col-md-12">
				<div class="small-spacer"></div>
			</div>
		</div>
	</div>
</dsp:page>
