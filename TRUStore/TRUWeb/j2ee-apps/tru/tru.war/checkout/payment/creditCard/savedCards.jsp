<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:getvalueof var="orderSet" value="false" />
	<dsp:getvalueof var="userSelectedCreditCard" bean="Profile.selectedCCNickName"/>
	<div class="col-md-7 credit-card-left-section">
		<div class="credit-component">
			<p>card number</p>
			<div class="inline">
				<select name="selectedCreditCardNickName" id="co-credit-card" style="width: 300px;">
							<dsp:droplet name="ForEach">
								<dsp:param name="array" param="creditCardsMap" />
								<dsp:param name="elementName" value="creditCard" />
								<dsp:oparam name="output">
									<dsp:getvalueof var="selectedCreditCardNickName" param="key"/>
									<dsp:getvalueof var="index" param="index" />
									<c:if test="${index eq 0 and empty userSelectedCreditCard}">
										<dsp:getvalueof var="userSelectedCreditCard" param="defaultCreditCardName"/>
									</c:if>
									<c:if test="${selectedCreditCardNickName eq userSelectedCreditCard}">						
										<dsp:getvalueof var="creditCardNickName" param="key"/>
										<dsp:getvalueof var="cardType" param="creditCard.creditCardType" />
										<dsp:getvalueof var="cardNumber" param="creditCard.creditCardNumber" />
										<dsp:getvalueof var="cardExpirationMonth" param="creditCard.expirationMonth" />
										<dsp:getvalueof var="cardExpirationYear" param="creditCard.expirationYear" />
										<dsp:getvalueof var="nameOnCard" param="creditCard.nameOnCard" />
										<%-- <dsp:getvalueof var="cardCode" param="creditCard.cvv" /> --%>
										<c:choose>
											<c:when test="${cardType eq 'visa'}">
												<dsp:getvalueof var="imageURL" value="${TRUImagePath}images/Payment_Small-Visa-Thumb-No-Outline.jpg" />
											</c:when>
											<c:when test="${cardType eq 'discover'}">
												<dsp:getvalueof var="imageURL" value="${TRUImagePath}images/Payment_Small-Discover-Thumb.jpg" />
											</c:when>
											<c:when test="${cardType eq 'masterCard'}">
												<dsp:getvalueof var="imageURL" value="${TRUImagePath}images/Payment_Small-Mastercard-Thumb-No-Outline.jpg" />
											</c:when>
											<c:when test="${cardType eq 'americanExpress'}">
												<dsp:getvalueof var="imageURL" value="${TRUImagePath}images/Payment_Small-Amex-Thumb-No-Outline.jpg" />
											</c:when>
											<c:when test="${cardType eq 'RUSPrivateLabelCard'}">
												<dsp:getvalueof var="imageURL" value="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-1.jpg" />
											</c:when>
											<c:when test="${cardType eq 'RUSCoBrandedMasterCard'}">
												<dsp:getvalueof var="imageURL" value="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-2.jpg" />
											</c:when>
										</c:choose>
											<%--Start :: TUW-43619/46853 defect changes. --%>
												<dsp:getvalueof var="selecteCCImageUrl" value="${imageURL}"/>
												<dsp:getvalueof var="isCardSelected" value="true"/>
												<dsp:getvalueof var="orderSet" value="true" />
												<dsp:getvalueof var="selectedCreditCardNickName" param="key" />
												<dsp:getvalueof var="selectedCCType" param="creditCard.creditCardType" />
												<dsp:getvalueof var="selectedCCNumber" param="creditCard.creditCardNumber" />
												<dsp:getvalueof var="selectedCCExpMon" param="creditCard.expirationMonth" />
												<dsp:getvalueof var="selectedCCExpYear" param="creditCard.expirationYear" />
												
												
											<option value="${selectedCreditCardNickName}" selected="selected" data-cardType="${selectedCCType}" data-image="${selecteCCImageUrl }" data-coCreditCardVal = "${selectedCCType}">
												<img src="${selecteCCImageUrl}"/>
												<dsp:valueof value="${selectedCCNumber}" converter="creditcard" maskCharacter="x" />
												<c:if test="${cardType ne 'RUSPrivateLabelCard'}">
													&nbsp;<fmt:message key="myaccount.card.exp" />&nbsp;
													<dsp:valueof value="${selectedCCExpMon}" />
													<dsp:valueof value="${selectedCCExpYear}" />
												</c:if>
											</option>
									</c:if>
								</dsp:oparam>
						</dsp:droplet>
						<dsp:droplet name="ForEach">
							<dsp:param name="array" param="creditCardsMap" />
							<dsp:param name="elementName" value="creditCard" />
							<dsp:oparam name="output">
									<dsp:getvalueof var="index" param="index"/>
									<dsp:getvalueof var="creditCardNickName" param="key"/>
									<c:if test="${creditCardNickName ne userSelectedCreditCard }">
										<dsp:getvalueof var="cardType" param="creditCard.creditCardType" />
										<c:choose>
											<c:when test="${cardType eq 'visa'}">
												<dsp:getvalueof var="imageURL" value="${TRUImagePath}images/Payment_Small-Visa-Thumb-No-Outline.jpg" />
											</c:when>
											<c:when test="${cardType eq 'discover'}">
												<dsp:getvalueof var="imageURL" value="${TRUImagePath}images/Payment_Small-Discover-Thumb.jpg" />
											</c:when>
											<c:when test="${cardType eq 'masterCard'}">
												<dsp:getvalueof var="imageURL" value="${TRUImagePath}images/Payment_Small-Mastercard-Thumb-No-Outline.jpg" />
											</c:when>
											<c:when test="${cardType eq 'americanExpress'}">
												<dsp:getvalueof var="imageURL" value="${TRUImagePath}images/Payment_Small-Amex-Thumb-No-Outline.jpg" />
											</c:when>
											<c:when test="${cardType eq 'RUSPrivateLabelCard'}">
												<dsp:getvalueof var="imageURL" value="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-1.jpg" />
											</c:when>
											<c:when test="${cardType eq 'RUSCoBrandedMasterCard'}">
												<dsp:getvalueof var="imageURL" value="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-2.jpg" />
											</c:when>
										</c:choose>
											<%--Start :: TUW-43619/46853 defect changes. --%>
											<option value="${creditCardNickName}" data-cardType="${cardType}" data-image="${imageURL }" data-coCreditCardVal = "${cardType}">
												<img src="${imageURL }"/>
												<dsp:valueof param="creditCard.creditCardNumber" converter="creditcard" maskCharacter="x" />
												<c:if test="${cardType ne 'RUSPrivateLabelCard'}">
													&nbsp;<fmt:message key="myaccount.card.exp" />&nbsp;
													<dsp:valueof param="creditCard.expirationMonth" />
													<dsp:valueof param="creditCard.expirationYear" />
												</c:if>
											</option>
									</c:if>
							</dsp:oparam>
						</dsp:droplet>
					<option value="" data-coCreditCardVal = "invalid" class="addAnotherCardPaymentPage"><fmt:message key="checkout.overlay.addAnotherCard"/></option>
				</select>
				<input type="hidden" id="orderSet" name="orderSet" value="${orderSet}" />
				<%-- <input type="hidden" name="cardType" id="cardType" value="${cardType}"/> --%>
				
				<div id="saved_card_details">
				    <input name="saveCardType" id="credit_Card_Type" type="hidden" value="${selectedCCType}" />
					<input name="ccnumber" id="creditCardNumber" type="hidden" value="${selectedCCNumber}" />
					<input name="nameOnCard" id="nameOnCard" type="hidden" value="${nameOnCard}" />
					<input name="expirationMonth" id="expirationMonth" type="hidden" value="${selectedCCExpMon}" />
					<input name="expirationYear" id="expirationYear" type="hidden" value="${selectedCCExpYear}" />
					<input name="isSaved_Card" id="isSaved_Card" type="hidden" value="true" />
					
				</div>
				<%-- <input name="cardCode" id="payment_creditCard_CVV" type="hidden" value="${cardCode}" /> --%>
				<a href="javascript:void(0);" class="editCreditCard" onclick="onEditCreditCardOnPaymentPage()" >
					<fmt:message key="checkout.payment.billingAddress.edit" />
				</a>
				<a href="javascript:void(0);" class="editCreditCard_cancel" style="display:none;">
					<fmt:message key="checkout.payment.billingAddress.cancel" />
				</a>
				<a href="javascript:void(0);" data-toggle="modal" id="addAnotherCreditCardModal" data-target="#myAccountAddCreditCardModal"
					onclick='javascript: onAddCardInPaymentOverlay();' style="display: none;">add another card</a>
			</div>
		</div>
	</div>
	<div id="credit-card-info">
		<dsp:include page="/checkout/payment/creditCard/savedCardFragment.jsp">
			<dsp:param name="creditCardNickName" value="${userSelectedCreditCard}" />
			<dsp:param name="isAjaxCall" value="false" />
		</dsp:include>
	</div>
</dsp:page>