<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/com/tru/common/droplet/DisplayMonthAndYearDroplet"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/PaymentGroupContainerService" />
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:getvalueof var="isEditSavedCard" vartype="java.lang.boolean" value="false" />
	<dsp:getvalueof var="formID" param="formID"/>
	<dsp:getvalueof var="profileId" bean="Profile.id"/>
	<dsp:getvalueof var="currencyCode" bean="TRUConfiguration.currencyCode"/>
	<dsp:getvalueof var="orderAmount" bean="ShoppingCart.current.priceInfo.total"/>	
	<dsp:getvalueof var="isTransient" bean="Profile.transient" />
	<dsp:droplet name="IsEmpty">
		<dsp:param name="value" param="creditCardNickName" />
		<dsp:oparam name="false">
			<dsp:getvalueof var="editCardNickName" param="creditCardNickName" />
			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="PaymentGroupContainerService.paymentGroupMap" />
				<dsp:param name="elementName" value="creditCard" />
				<dsp:oparam name="output">
				<dsp:getvalueof var="nickname" param="key" />
					<c:if test="${nickname eq editCardNickName}">
						<dsp:getvalueof var="isEditSavedCard" vartype="java.lang.boolean" value="true" />
						<dsp:getvalueof var="creditCard" param="creditCard" />
					</c:if> 
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	<div class="col-md-6 set-height" id="addOrEditId">	
		<form id="addOrEditCreditCard" method="POST" class="JSValidation"> <%-- onSubmitAddCreditCard_checkout() --%>
           	<div class="editCreditCard_section">
	         	<p class="global-error-display"></p>
				<div class="row left-padding">
					<div class="col-md-9">
						<div class="row nameOnCard">
							<input type="hidden" name="nameOnCard" id="nameOnCard" value="${creditCard.nameOnCard}" maxlength="40"/>
						</div>
						<div class="row creditCardNumber">
						<label for="creditCardNumber">
								<span class="avenir-heavy">
									<span class="required-asterisk mr4"><fmt:message key="myaccount.asterisk" /></span>
								<fmt:message key="myaccount.card.number" /></span>
							</label>
							<dsp:getvalueof var="cardType" value="${creditCard.creditCardType}" />
							<input id="creditCardType" name="creditCardType" type="hidden" value="${cardType}" />
							<div class="col-md-8">
								<p class="avenir-light">
									<dsp:valueof value="${creditCard.creditCardNumber}"	converter="creditcard" maskcharacter="x" />
								</p>
								<input type="hidden" name="ccnumber" id="creditCardNumber" value="${creditCard.creditCardNumber}" />
							</div>
						</div>
						<div class="row expiration">
							<label>
								<span class="avenir-heavy"><span class="required-asterisk mr4"><fmt:message key="myaccount.asterisk" /></span><fmt:message key="myaccount.card.expires" /></span>
							</label>
							<div class="col-md-5">
								<div class="select-wrapper">
								<c:choose>
									<c:when test="${cardType eq 'RUSPrivateLabelCard'}">
										<select name="expirationMonth" id="expirationMonth" class="grayedOut" disabled="disabled">											
											<option value=""><fmt:message key="checkout.payment.credit.card.month"/></option>
										</select>
									</c:when>
									<c:otherwise>
										<select name="expirationMonth" id="expirationMonth">
											<option value=""><fmt:message key="checkout.payment.credit.card.month"/></option>
											<dsp:droplet name="DisplayMonthAndYearDroplet">
												<dsp:param name="monthList" value="monthList"/>
												<dsp:oparam name="output">
													<dsp:droplet name="ForEach">
														<dsp:param name="array" param="monthList"/>
														<dsp:param name="elementName" value="month"/>
														<dsp:oparam name="output">
															<dsp:getvalueof var="month" param="month" />
															<option value='<dsp:valueof param="month" />'
					                                              <dsp:droplet name="/atg/dynamo/droplet/Compare">
					                                                      <dsp:param name="obj1" value="${month}"/>
					                                                      <dsp:param name="obj2" value="${creditCard.expirationMonth}"/>
					                                                      <dsp:oparam name="equal">
					                                                          selected="selected"
					                                                      </dsp:oparam>
					                                              </dsp:droplet> >
					                                              <dsp:valueof param="month"/>
					                                         </option>
					                                         </dsp:oparam>
														</dsp:droplet>
													</dsp:oparam>
												</dsp:droplet>
											</select>
											</c:otherwise>
									</c:choose>
								</div>
							</div>
							<div class="col-md-5 col-md-offset-1">
								<div class="select-wrapper">
								<c:choose>
									<c:when test="${cardType eq 'RUSPrivateLabelCard'}">
										<select name="expirationYear" id="expirationYear" class="grayedOut" disabled="disabled">											
											<option value=""><fmt:message key="checkout.payment.credit.card.year"/></option>
										</select>
									</c:when>
									<c:otherwise>
										<select name="expirationYear" id="expirationYear">
											<option value=""><fmt:message key="checkout.payment.credit.card.year"/></option>
											<dsp:droplet name="DisplayMonthAndYearDroplet">
												<dsp:param name="yearList" value="yearList"/>
												<dsp:oparam name="output">
													<dsp:droplet name="ForEach">
														<dsp:param name="array" param="yearList"/>
														<dsp:param name="elementName" value="year"/>
														<dsp:oparam name="output">
															<dsp:getvalueof var="year" param="year" />
															<dsp:getvalueof var="creditCardExpYr" value="${creditCard.expirationYear}"></dsp:getvalueof>
															<option value="${year}"
															<c:if test="${year eq creditCardExpYr}">
																selected="selected"
															</c:if>
															>
					                                              <dsp:valueof param="year"/>
					                                         </option>
														</dsp:oparam>
													</dsp:droplet>
												</dsp:oparam>
											</dsp:droplet>
										</select>
									</c:otherwise>
								</c:choose>
								</div>
							</div>
						</div>
	                    <div class="row cvvCode">
		                     <label>
		                        <span class="avenir-heavy"><span class="required-asterisk mr4"><fmt:message key="myaccount.asterisk" /></span><fmt:message key="myaccount.security.code" /></span>
		                    </label>
		                    <div class="col-md-12">
		                        <dsp:getvalueof param="creditCardCVV" var="creditCardCVVFromSuggPage"></dsp:getvalueof>
		                        <c:choose>
			                         <c:when test="${formID eq 'editUserEnteredAddressForCreditCard'}">
			                         <dsp:getvalueof var="cvv" value="${creditCardCVVFromSuggPage}"></dsp:getvalueof>
			                         </c:when>
			                         <c:otherwise>
			                         	<dsp:getvalueof var="cvv" value=""></dsp:getvalueof>
			                         </c:otherwise>
		                        </c:choose>
								<p><input type="password" name="creditCardCVV" class="security-code inline creditCardCVV" value="${cvv}" maxlength="4" id="creditCardCVV"/></p>
		                     </div>
	                    </div>
						<br>
						<input type="hidden" name="newCreditCard" value="false" />
						<input type="hidden" name="editCreditCard" value="true" />
						<input type="hidden" name="editCardNickName" value="${editCardNickName}" id="editCardNickNameID" />
				</div>
			 </div>
		 </div>
		</form>
	</div>
</dsp:page>
