<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:getvalueof var="stopScroll" param="stopScroll"/>
<div class="modal fade" id="myAccountAddCreditCardModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >
		<div class="modal-dialog modal-lg" id="creditCardAjaxFragment">
		</div>
</div>
		
<div class="modal fade toStopScroll" id="myAccountDeleteCancelModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content sharp-border">
			<div class="my-account-delete-cancel-overlay">
				<div>
					<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close" id="closeIt"></span></a>
				</div>
				<header><fmt:message key="myaccount.delete.address"/>?</header>
				<p><fmt:message key="myaccount.sure.remove.address"/></p>
				<div>
				<form id="removeShippingAddress" method="POST" onsubmit="javascript: return removeAddress_checkout();">
					<input type="hidden" id="removeAddressHiddenId" class="removeAddressHiddenId" value="">
					
					<input type="submit" id="removeShippingAddressId" name="removeShippingAddress" value="confirm" style="display: none;">
					
                             <button onclick="return deleteAddress_checkout();"><fmt:message key="myaccount.confirm"/></button><a href="javascript:void(0)" data-dismiss="modal"><fmt:message key="myaccount.cancel"/></a>
				</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in" id="myAccountCardDeleteModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false" >

			<div class="modal-dialog modal-lg">
				<div class="modal-content sharp-border">
					<div class="my-account-delete-cancel-overlay">
						<div>
							<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
						</div>
						<header><fmt:message key="myaccount.delete.credit.card" />?</header>
		                <p><fmt:message key="myaccount.sure.remove.card" /></p>
		                <div>
							<form id="removeCreditCardFormId" method="POST" onsubmit="javascript: return removeCard_checkout();">
								<input type="hidden" id="removeCardHiddenId" name="removeCardHiddenName" value="" />
								<input type="submit" id="removeCardId" name="removeCardId" value="confirm"  style="display: none;"/>
								
                                <button id="delete-credit-card-btn" onclick="return deleteCard_checkout();"><fmt:message key="myaccount.confirm" /></button><a href="javascript:void(0)" data-dismiss="modal"><fmt:message key="myaccount.cancel" /></a>
							</form>
						</div>
					</div>
				</div>
			</div>
	</div>
<div class="modal fade ${stopScroll}" id="creditCardModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false" >
   <div class="modal-dialog modal-lg">
      <div class="modal-content sharp-border">
         <dsp:droplet name="/atg/targeting/TargetingFirst">
			<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/Checkout/CreditCardOverlayTargeter" />
			<dsp:oparam name="output">
				<dsp:valueof param="element.data" valueishtml="true" />
			</dsp:oparam>
		</dsp:droplet>
      </div>
   </div>
</div>
</dsp:page>