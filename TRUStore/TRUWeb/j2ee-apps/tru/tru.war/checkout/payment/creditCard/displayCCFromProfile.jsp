<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg//atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean	bean="/com/tru/common/droplet/TRUCreditCardExpiryCheckDroplet" />
	<dsp:importbean bean="/com/tru/commerce/order/droplet/AvailableCreditCardsDroplet"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/PaymentGroupContainerService" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/commerce/order/purchase/PaymentGroupDroplet"/>
	<%-- Start :: TUW-56702	 --%>	
	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
	<dsp:droplet name="PaymentGroupDroplet">
	  <dsp:param name="clearPaymentGroups" value="${not isTransient}" />
	  <dsp:param name="paymentGroupTypes" value="creditCard" />
	  <dsp:param name="initPaymentGroups" value="true" />
	  <dsp:oparam name="output" />
	</dsp:droplet>
	<%-- End :: TUW-56702	 --%>	
	<dsp:droplet name="AvailableCreditCardsDroplet">
		<dsp:param name="paymentGroupMapContainer" bean="PaymentGroupContainerService" />
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:param name="profile" bean="Profile" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="creditCardsMap" param="creditCardsMap" />
			<dsp:getvalueof var="defaultCreditCardName" param="defaultCreditCardName" />
		</dsp:oparam>
	</dsp:droplet>

	<dsp:droplet name="IsEmpty">
		<dsp:param value="${defaultCreditCardName}" name="value" />
		<dsp:oparam name="false">
			<dsp:droplet name="ForEach">
				<dsp:param name="array"	value="${creditCardsMap}" />
				<dsp:param name="elementName" value="creditCard" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="nickName" param="key" />
					<c:if test="${nickName eq defaultCreditCardName}">
						<div class="row">
							<dsp:droplet name="TRUCreditCardExpiryCheckDroplet">
								<dsp:param name="month" param="creditCard.expirationMonth" />
								<dsp:param name="year" param="creditCard.expirationYear" />
								<dsp:oparam name="true">
									<dsp:getvalueof var="expiredClass" value="cardExpired"></dsp:getvalueof>
								</dsp:oparam>
								<dsp:oparam name="false">
									<dsp:getvalueof var="expiredClass" value=""></dsp:getvalueof>
								</dsp:oparam>
							</dsp:droplet>
							<div class="col-md-1">
								<dsp:getvalueof var="cardType" param="creditCard.creditCardType" vartype="java.lang.String" />
								<c:choose>
									<c:when test="${cardType eq 'visa'}">
										<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Visa-Thumb-No-Outline.jpg">
									</c:when>
									<c:when test="${cardType eq 'discover'}">
										<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Discover-Thumb.jpg">
									</c:when>
									<c:when test="${cardType eq 'masterCard'}">
										<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Mastercard-Thumb-No-Outline.jpg">
									</c:when>
									<c:when test="${cardType eq 'americanExpress'}">
										<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Amex-Thumb-No-Outline.jpg">
									</c:when>
									<c:when test="${cardType eq 'RUSPrivateLabelCard'}">
										<img class="credit-card" src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-1.jpg">
									</c:when>
									<c:when test="${cardType eq 'RUSCoBrandedMasterCard'}">
										<img class="credit-card" src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-2.jpg">
									</c:when>
									<c:otherwise>
										<fmt:message key="checkout.payment.no.image.available"/>
									</c:otherwise>
								</c:choose>
							</div>
							<div class="col-md-3 ${expiredClass}">
								<p class="avenir-light">
									<dsp:valueof param="creditCard.creditCardNumber" converter="creditcard" maskCharacter="x" />
								</p>
							</div>

							<div class="col-md-6 ${expiredClass}">
								<p>
									<span class="avenir-light expiry">
									<c:if test="${cardType ne 'RUSPrivateLabelCard'}">
										<fmt:message key="myaccount.card.exp" />&nbsp;<dsp:valueof param="creditCard.expirationMonth" />/<dsp:valueof param="creditCard.expirationYear" />
									</c:if>
										<c:if test="${expiredClass eq 'cardExpired'}">
											<fmt:message key="myaccount.card.expired" />
										</c:if>
									</span> . <span class="blue"> <a href="#" class="" onclick='javascript: onEditCreditCardOverlay_checkout("${nickName}");'>
									<fmt:message key="myaccount.edit" /></a></span> .
									<fmt:message key="myaccount.default.card" />
								</p>
							</div>
							<div class="col-md-1">
								<img class="delete-icon" src="${TRUImagePath}images/delete-icon.png"
									data-toggle="modal" data-target="#myAccountCardDeleteModal"
									onclick="removeCardConfirm('${nickName}')">
							</div>
						</div>
					</c:if>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="ForEach">
		<dsp:param name="array"	value="${creditCardsMap}" />
		<dsp:param name="sortProperties" value="-lastActivity"/>
		<dsp:param name="elementName" value="creditCard" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="nickName" param="key" />
			<c:if test="${defaultCreditCardName ne nickName}">
				<div class="row">
					<div class="col-md-12">
						<div class="small-spacer"></div>
					</div>
				</div>
				<div class="row">
					<dsp:droplet name="TRUCreditCardExpiryCheckDroplet">
						<dsp:param name="month" param="creditCard.expirationMonth" />
						<dsp:param name="year" param="creditCard.expirationYear" />
						<dsp:oparam name="true">
							<dsp:getvalueof var="expiredClass" value="cardExpired"></dsp:getvalueof>
						</dsp:oparam>
						<dsp:oparam name="false">
							<dsp:getvalueof var="expiredClass" value=""></dsp:getvalueof>
						</dsp:oparam>
					</dsp:droplet>
					<div class="col-md-1">
						<dsp:getvalueof var="cardType" param="creditCard.creditCardType" vartype="java.lang.String" />
						<dsp:getvalueof var="creditCard" param="element"></dsp:getvalueof>
						<c:choose>
							<c:when test="${cardType eq 'visa'}">
								<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Visa-Thumb-No-Outline.jpg">
							</c:when>
							<c:when test="${cardType eq 'discover'}">
								<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Discover-Thumb.jpg">
							</c:when>
							<c:when test="${cardType eq 'masterCard'}">
								<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Mastercard-Thumb-No-Outline.jpg">
							</c:when>
							<c:when test="${cardType eq 'americanExpress'}">
								<img class="credit-card" src="${TRUImagePath}images/Payment_Small-Amex-Thumb-No-Outline.jpg">
							</c:when>
							<c:when test="${cardType eq 'RUSPrivateLabelCard'}">
								<img class="credit-card" src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-1.jpg">
							</c:when>
							<c:when test="${cardType eq 'RUSCoBrandedMasterCard'}">
								<img class="credit-card" src="${TRUImagePath}images/Payment_Small-R-US-card-Thumb-2.jpg">
							</c:when>
							<c:otherwise>
								<fmt:message key="checkout.payment.no.image.available"/>
							</c:otherwise>
						</c:choose>
					</div>
					<div class="col-md-3 ${expiredClass}">
						<p class="avenir-light">
							<dsp:valueof converter="creditcard"
								param="creditCard.creditCardNumber" maskCharacter="x" />
						</p>
					</div>

					<div class="col-md-6 ${expiredClass}">
						<p>
						<dsp:getvalueof var="country" param="creditCard.billingAddress.country"></dsp:getvalueof>
							<span class="avenir-light expiry">
							<c:if test="${cardType ne 'RUSPrivateLabelCard'}">
								<fmt:message key="myaccount.card.exp" />&nbsp;<dsp:valueof param="creditCard.expirationMonth" />/<dsp:valueof param="creditCard.expirationYear" />
							</c:if>
								<c:if test="${expiredClass eq 'cardExpired'}">
									<fmt:message key="myaccount.card.expired" />
								</c:if>
							</span> . <span class="blue"> <a href="#" class="" onclick='javascript: onEditCreditCardOverlay_checkout("${nickName}");'>
							<fmt:message key="myaccount.edit" /></a></span>.
							<span class="blue"> <a href="javascript:void(0);"
								onclick="defaultCard_checkout('${nickName}','checkoutOverlay'); return false">
									<fmt:message key="myaccount.set.default" />
								</a>
							</span>
							<%--  � default card	 --%>
						</p>
					</div>

					<div class="col-md-1">
						<img class="delete-icon" src="${TRUImagePath}images/delete-icon.png" data-toggle="modal" data-target="#myAccountCardDeleteModal" onclick="removeCardConfirm('${nickName}')">
					</div>
				</div>
			</c:if>

		</dsp:oparam>
	</dsp:droplet>

</dsp:page>