<fmt:setBundle
	basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	
			<div class="modal-content sharp-border">
                <div class="my-account-add-address-overlay normalHeight">
                    <div class="row">
                        <div class="col-md-5 col-md-offset-7 your-addresses-background">
                            <div class="row top-margin">
                                <div class="col-md-1 col-md-offset-10">
                                   <a href="javascript:void(0)" data-dismiss="modal"> <span class="clickable"><img  src="${TRUImagePath}images/close.png" alt="close model"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
					<div class="col-md-7 set-height">
					<dsp:form id="billingAddressEditForm" formid="billingAddressEditForm" name="billingAddressEditFormName" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2><fmt:message key="checkout.shipping.overlay.addressInformation" /></h2>
                                    
                                </div>
                            </div>
                            <div class="tse-scrollable"><div class="tse-scrollbar" style="display: none;"><div class="drag-handle visible"></div></div>
                                <div class="tse-scroll-content">
                                    <div class="tse-content">
                                        <div class="row">
                                            <div class="col-md-9">
                                            	<label>
													<span class="avenir-heavy">
														<span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<fmt:message key="myaccount.address.country" />
													</span>
												</label> <br>
												<div class="select-wrapper">
													<select name="country" id="country" onchange="onCountryChange();">
														<dsp:droplet name="ForEach">
														  <dsp:param name="array" bean="/atg/commerce/util/CountryList.places"/>
														  <dsp:param name="elementName" value="billingCountry"/>
														  <dsp:oparam name="output">
															  <dsp:getvalueof var="billingCountry" param="billingCountry.code"></dsp:getvalueof>
															  <option value='<dsp:valueof param="billingCountry.code" />'
							                                        <dsp:droplet name="/atg/dynamo/droplet/Compare">
							                                                <dsp:param name="obj1" param="billingCountry.code"/>
							                                                <dsp:getvalueof var="billingAddressCountry" bean="BillingFormHandler.addressInputFields.country"></dsp:getvalueof>
							                                                <c:choose>
							                                                	<c:when test="${not empty billingAddressCountry}">
							                                                		<dsp:param name="obj2" value="${billingAddressCountry}"/>
							                                                	</c:when>
							                                                	<c:otherwise>
							                                                		<dsp:param name="obj2" value="US"/>
							                                                	</c:otherwise>
							                                                </c:choose>
							                                                <dsp:oparam name="equal">
							                                                    selected="selected"
							                                                </dsp:oparam>
							                                        </dsp:droplet> >
							                                        <dsp:valueof param="billingCountry.displayName" />
							                                   </option>
														  </dsp:oparam>
														</dsp:droplet>
													</select>
												</div>
												<dsp:getvalueof var="editOrderBillingAddress" param="editOrderBillingAddress" />
                            					<dsp:getvalueof var="editBillingAddress" param="editBillingAddress" /><%-- need to pass this param on click of edit from the billing address overlay --%>
                                                <label for="id-70638108-d363-aa1e-887d-4d82b1b5300b">
                                                    <span class="avenir-heavy"><span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<fmt:message key="myaccount.address.nickname" /></span>
                                                </label>
                                                <c:choose>
                                                	<c:when test="${editOrderBillingAddress ne true and editBillingAddress ne true}">
                                                		<dsp:input type="text" id="id-70638108-d363-aa1e-887d-4d82b1b5300b" name="nickName" bean="BillingFormHandler.billingAddressNickName" maxlength="42"/>
                                                	</c:when>
                                                	<c:otherwise>
                                                		<dsp:input type="text" name="nickNameTmp" bean="BillingFormHandler.billingAddressNickName" maxlength="42" disabled="true"/>
                                                		<dsp:input type="hidden" id="id-70638108-d363-aa1e-887d-4d82b1b5300b" name="nickName" bean="BillingFormHandler.billingAddressNickName" maxlength="42"/>
                                                	</c:otherwise>
                                                </c:choose>
	                                            
                                                <label for="id-c0464a8c-3288-0a55-8b4e-174d0ea2a392">
                                                	<span class="avenir-heavy"><span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<fmt:message key="checkout.shipping.firstname" /></span>
                                                </label>
	                                            <dsp:input type="text" id="id-c0464a8c-3288-0a55-8b4e-174d0ea2a392" name="firstName" bean="BillingFormHandler.addressInputFields.firstName"/>
                                                <label for="id-d8b9e0e4-d14d-cea7-f4f3-31c851b655dc">
                                                    <span class="avenir-heavy"><span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<fmt:message key="checkout.shipping.lastname" /></span>
                                                </label>
	                                            <dsp:input type="text" id="id-d8b9e0e4-d14d-cea7-f4f3-31c851b655dc" name="lastName" bean="BillingFormHandler.addressInputFields.lastName"/>
                                                <label for="id-514e725f-7a00-6771-119b-c593b113dec1">
                                                    <span class="avenir-heavy"><span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<fmt:message key="checkout.shipping.address1" /></span>
                                                </label>
	                                            <dsp:input type="text" id="id-514e725f-7a00-6771-119b-c593b113dec1" name="address1" bean="BillingFormHandler.addressInputFields.address1"/>
                                                <label for="id-1822d72a-857d-7e72-250b-86fcf3382ddf">
                                                    <span><fmt:message key="checkout.shipping.suite" />&nbsp;</span>
                                                </label>
		                                        <dsp:input type="text" id="id-1822d72a-857d-7e72-250b-86fcf3382ddf" name="address2" bean="BillingFormHandler.addressInputFields.address2"/>
                                                <label for="id-48c949d2-7c1c-2249-acd1-e9e80fe0a0ce">
                                                    <span class="avenir-heavy"><span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<fmt:message key="checkout.shipping.city" /></span>
                                                </label>
                                                <dsp:input type="text" id="id-48c949d2-7c1c-2249-acd1-e9e80fe0a0ce" name="city" bean="BillingFormHandler.addressInputFields.city" maxlength="30"/>
                                                <label>
                                                    <span class="avenir-heavy"><span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<fmt:message key="checkout.shipping.state" />  </span>
                                                </label>
                                                <br>
                                                <div class="select-wrapper">
                                                <dsp:getvalueof var="billingAddressState" bean="BillingFormHandler.addressInputFields.state"></dsp:getvalueof>
                                                <dsp:getvalueof var="billingAddressCountry" bean="BillingFormHandler.addressInputFields.country"></dsp:getvalueof>
                                               <select name="state" id="state" bean="BillingFormHandler.addressInputFields.state">
													<option value=""><fmt:message key="checkout.shipping.state.select"/></option>
													<c:if test="${billingAddressCountry eq 'US'}">
							                              <dsp:droplet name="ForEach">
															  <dsp:param name="array" bean="/atg/commerce/util/StateList_US.places"/>
															  <dsp:param name="elementName" value="billingState"/>
															  <dsp:oparam name="output">
																  <dsp:getvalueof var="stateCode" param="billingState.code"></dsp:getvalueof>
																  <option value="${stateCode}"
																  <dsp:droplet name="/atg/dynamo/droplet/Compare">
						                                                <dsp:param name="obj1" param="billingState.code"/>
						                                                <dsp:param name="obj2" value="${billingAddressState}"/>
						                                                <dsp:oparam name="equal">
						                                                    selected="selected"
						                                                </dsp:oparam>
						                                        </dsp:droplet>
																  >
																  <dsp:valueof param="billingState.code" /></option>
															  </dsp:oparam>
														</dsp:droplet>
													</c:if>
						                            <%-- <option value="other"
						                            <c:if test="${billingAddressState eq 'other'}">
						                            	selected="selected"
					                                </c:if> >
					                                        other</option> --%>
												</select>
                                                <label for="id-d31985ad-a9f9-0ff3-463e-5e673b75f0f9">
                                                    <span class="avenir-heavy"><span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>&nbsp;&nbsp;<fmt:message key="checkout.shipping.zip" />&nbsp;</span>
                                                </label>
                                                <dsp:input type="text" id="id-d31985ad-a9f9-0ff3-463e-5e673b75f0f9" name="postalCode" bean="BillingFormHandler.addressInputFields.postalCode" maxlength="10"/>
                                                <label for="id-0d52fb6a-c934-ed9c-b88b-65c9a304fffd">
                                                    <span class="avenir-heavy"><span class="required-asterisk"><fmt:message key="myaccount.asterisk" /></span>&nbsp;&nbsp;<fmt:message key="checkout.shipping.telephone" /></span>
                                                </label>
			                                    <dsp:input type="text" id="id-0d52fb6a-c934-ed9c-b88b-65c9a304fffd" name="phoneNumber" bean="BillingFormHandler.addressInputFields.phoneNumber" maxlength="20"/>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            
                            <input type="hidden" name="editOrderBillingAddress" value="${editOrderBillingAddress}">
                            <input type="hidden" name="editBillingAddress" value="${editBillingAddress}">
                            <div class="row top-border attach-to-bottom">
                                <div class="col-md-12">
                                   <!-- <input type="submit" id="submitEditBillingAddressID" priority="-10" name="submitEditBillingAddress" value="submit" style="display: none;" /> -->
			                       <button onclick="return saveBillingAddressinPaymentPage();"><fmt:message key="myaccount.save" /></button>
                                </div>
                            </div>
                            </dsp:form>
                        </div>
						<dsp:include page="/checkout/payment/displayAddress.jsp"/>   
                    </div>
                </div>
           </div>
</dsp:page>