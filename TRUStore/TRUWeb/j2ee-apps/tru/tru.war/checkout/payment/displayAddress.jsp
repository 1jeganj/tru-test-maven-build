<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
  	<dsp:getvalueof var="isAnonymousUser" bean="Profile.transient"/> 
	<div class="col-md-5 your-addresses-background set-height">
		<div class="row">
			<div class="col-md-12">
				<h3>
					<fmt:message key="myaccount.your.addresses" />
				</h3>
			</div>
		</div>
		<div class="tse-scrollable">
			<div class="tse-scrollbar" style="display: none;">
				<div class="drag-handle visible"></div>
			</div>
			<div class="tse-scrollbar" style="display: none;">
				<div class="drag-handle visible"></div>
			</div>
			<div class="tse-scroll-content" style="width: 503px; height: 380px;">
				<div class="tse-content">
					<c:choose>
						<c:when test="${isAnonymousUser eq true }">
						<dsp:getvalueof var="secondaryAddresses" bean="ShoppingCart.current.secondaryAddresses" vartype="java.lang.Object" />
						<dsp:droplet name="IsEmpty">
							<dsp:param name="value" value="${secondaryAddresses}" />
							<dsp:oparam name="false">
								<dsp:droplet name="ForEach">
						           	<dsp:param name="array" bean="ShoppingCart.current.secondaryAddresses"/>
						           	<dsp:param name="elementName" value="secondaryAddress"/>
						           	<dsp:oparam name="output">
										<dsp:getvalueof var="nickName" param="key" />
				           				<div class="small-spacer"></div>
				             			<div class="row">
				                 			<div class="col-md-12">
				                    			<p><span class="dark-blue avenir-heavy"><dsp:valueof param="key" /></span>&nbsp;.&nbsp;<span class="default-address"><fmt:message key="myaccount.default.address" /></span></p>
				                     			<p class="avenir-heavy"><dsp:valueof param="secondaryAddress.firstName" />&nbsp;<dsp:valueof param="secondaryAddress.lastName" /></p>
				                     			<p><dsp:valueof param="secondaryAddress.address1" />, <dsp:valueof param="secondaryAddress.address2" /></p>
				                     			<p><dsp:valueof param="secondaryAddress.city" />,&nbsp;<dsp:valueof param="secondaryAddress.state" />&nbsp;<dsp:valueof param="secondaryAddress.postalCode" /></p>
				                     			<div class="row">
				                         			<div class="col-md-8">
				                             			<p><span class="blue"><a href="#" class="" onclick='javascript: onEditInBillingAddressOverlay("${nickName}");'><fmt:message key="myaccount.edit" /></a></span></p>
				                       				</div>
													<div class="col-md-1">
														<img class="delete-icon" src="${TRUImagePath}images/delete-icon.png" data-toggle="modal" data-target="#myAccountDeleteCancelModal" onclick="removeBillingAddress('${nickName}')">
													</div>
												</div>
											</div>
										</div>
									</dsp:oparam>
								</dsp:droplet>
							</dsp:oparam>
						</dsp:droplet>
						</c:when>
						<c:otherwise>
						<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
						<fmt:message key="checkout.shipping.address.id" />&nbsp;${shippingAddressId}<br>
							<dsp:droplet name="IsEmpty">
								<dsp:param name="value" bean="Profile.secondaryAddresses" />
								<dsp:oparam name="false">
								<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
									<dsp:droplet name="ForEach">
										<dsp:param name="array" bean="Profile.secondaryAddresses" />
										<dsp:param name="elementName" value="secondaryAddress" />
										<dsp:oparam name="output">
											<dsp:droplet name="IsEmpty">
												<dsp:param name="value" bean="Profile.shippingAddress.id" />
												<dsp:oparam name="false">
													<dsp:droplet name="ForEach">
											           	<dsp:param name="array" bean="Profile.secondaryAddresses"/>
											           	<dsp:param name="elementName" value="address"/>
											           	<dsp:oparam name="output">
															<dsp:getvalueof param="address.id" var="addressId" />
															<dsp:getvalueof var="nickName" param="key" />
															<c:if test="${addressId eq shippingAddressId}">
												           		<div class="small-spacer"></div>
													             <div class="row">
													                 <div class="col-md-12">
													                     <p><span class="dark-blue avenir-heavy"><dsp:valueof param="key" /></span> . <span class="default-address"><fmt:message key="myaccount.default.address" /></span></p>
													                     <p class="avenir-heavy"><dsp:valueof param="address.firstName" />&nbsp;<dsp:valueof param="address.lastName" /></p>
													                     <p><dsp:valueof param="address.address1" />&nbsp;<dsp:valueof param="address.address2" /></p>
													                     <p><dsp:valueof param="address.city" />,&nbsp;<dsp:valueof param="address.state" />&nbsp;<dsp:valueof param="address.postalCode" /></p>
													                     <div class="row">
													                         <div class="col-md-8">
													                             <p><span class="blue"><a href="#" class="" onclick='javascript: onEditInBillingAddressOverlay("${nickName}");'><fmt:message key="myaccount.edit" /></a></span></p>
													                         </div>
													                         <div class="col-md-1">
													                            <img class="delete-icon" src="${TRUImagePath}images/delete-icon.png" data-toggle="modal" data-target="#myAccountDeleteCancelModal" onclick="removeAddress('${nickName}')">
													                         </div>
													                     </div>
													                 </div>
													             </div>
															 </c:if>
											           	</dsp:oparam>
										           </dsp:droplet>
												</dsp:oparam>
											</dsp:droplet>
								           <dsp:droplet name="ForEach">
									           	<dsp:param name="array" bean="Profile.secondaryAddresses"/>
									           	<dsp:param name="elementName" value="address"/>
									           	<dsp:param name="sortProperties" value="-lastActivity"/>
									           	<dsp:oparam name="output">
													<dsp:getvalueof param="address.id" var="addressId" />
													<dsp:getvalueof var="nickName" param="key" />
													<c:if test="${addressId ne shippingAddressId}">
										           		 <div class="spacer"></div>
											             <div class="row">
											                 <div class="col-md-12">
												                 <dsp:getvalueof param="address.country" var="country"/>
												                 <c:if test="${country ne 'US'}">
												                 	<p><fmt:message key="myaccount.billing.purpose" /></p>
												               	 </c:if>
											                     <p class="dark-blue avenir-heavy"><dsp:valueof param="key" /></p>
											                     <p class="avenir-heavy"><dsp:valueof param="address.firstName" />&nbsp;<dsp:valueof param="address.lastName" /></p>
											                     <p><dsp:valueof param="address.address1" />&nbsp;<dsp:valueof param="address.address2" /></p>
											                     <p><dsp:valueof param="address.city" />, <dsp:valueof param="address.state" />&nbsp;<dsp:valueof param="address.postalCode" /></p>
											                     <c:if test="${country ne 'US'}">
											                 		 <p><dsp:valueof param="address.country"/></p>
											               	 	</c:if>
											                     <div class="row">
											                         <div class="col-md-8">
											                             <p><span class="blue"><a href="#" class="" onclick='javascript: onEditInBillingAddressOverlay("${nickName}");'><fmt:message key="myaccount.edit" /></a></span>&nbsp;.
											                             <c:choose>
											                             	<c:when test="${country eq 'US'}">
											                             		<span class="blue">
																					<a class="setDefaultLink" href="javascript:void(0);" id="" class="" onclick="defaultAddress('${nickName}'); return false"><fmt:message key="myaccount.set.default" /></a>
																				</span>
											                             	</c:when>
											                             	<c:otherwise>
											                             		<fmt:message key="myaccount.set.default" />
											                             	</c:otherwise>
											                             </c:choose>
											                             </p>
											                         </div>
											                         <div class="col-md-1">
											                            <img class="delete-icon" src="${TRUImagePath}images/delete-icon.png" data-toggle="modal" data-target="#myAccountDeleteCancelModal" onclick="removeAddress('${nickName}')">
											                         </div>
											                     </div>
											                 </div>
											             </div>
													 </c:if>
									           	</dsp:oparam>
								           </dsp:droplet>
										</dsp:oparam>
									</dsp:droplet>
								</dsp:oparam>
							</dsp:droplet>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
		<div class="spacer"></div>
		<div class="row">
			<div class="col-md-12">
				<p class="blue">
					<a href="javascript:void(0);" class=""
						onclick="onAddAddressInPaymentOverlay();"><fmt:message
							key="myaccount.add.another.address" /></a>
				</p>
			</div>
		</div>
		<div class="spacer"></div>
	</div>
</dsp:page>
