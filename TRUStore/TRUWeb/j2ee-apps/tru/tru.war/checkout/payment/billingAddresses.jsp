<fmt:setBundle
	basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/commerce/order/purchase/PaymentGroupContainerService" />
	
	<dsp:getvalueof var="defaultPaymentGroupName" bean="PaymentGroupContainerService.defaultPaymentGroupName" />
	<dsp:getvalueof param="lookupAddress" var="lookupAddress" />
	<dsp:getvalueof param="nickname" var="nickname" />
	<dsp:getvalueof param="billingAddress" var="billingAddress" />
	<dsp:getvalueof param="refreshBillingAddress" var="refreshBillingAddress"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<%-- this condition is added in case the fragment is getting refreshed due to ajax call on saving the edited billing address --%>
	<c:if test="${refreshBillingAddress eq true}">
		<dsp:droplet name="/atg/dynamo/droplet/ForEach">
			<dsp:param name="array" bean="ShoppingCart.current.paymentGroups" />
			<dsp:param name="elementName" value="pg" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="pgType" param="pg.paymentGroupClassType" />
				<c:choose>
					<c:when test="${pgType eq 'creditCard' }">
						<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
							<dsp:param name="value" param="pg.billingAddress.address1" />
							<dsp:oparam name="false">
								<dsp:getvalueof var="billingAddress" param="pg.billingAddress" />
								<dsp:getvalueof var="selectedAddressNickName" param="pg.billingAddressNickName" />
							</dsp:oparam>
						</dsp:droplet>
					</c:when>
				</c:choose>
			</dsp:oparam>
		</dsp:droplet>
		<dsp:param name="billingAddress" value="${billingAddress}" />
	</c:if>
	
	<c:choose>
		<c:when test="${empty lookupAddress && not empty billingAddress}">
			<dsp:getvalueof var="firstName" param="billingAddress.firstName" />
			<dsp:getvalueof var="lastName" param="billingAddress.lastName" />
			<dsp:getvalueof var="address1" param="billingAddress.address1" />
			<dsp:getvalueof var="address2" param="billingAddress.address2" />
			<dsp:getvalueof var="state" param="billingAddress.state" />
			<dsp:getvalueof var="city" param="billingAddress.city" />
			<dsp:getvalueof var="postalCode" param="billingAddress.postalCode" />
			<dsp:getvalueof var="phoneNumber" param="billingAddress.phoneNumber" />
			<dsp:getvalueof var="country" param="billingAddress.country" />
			
			<input name="country" type="hidden" value="${country }" />
			<input name="firstName" type="hidden" value="${firstName }" />
			<input name="lastName" type="hidden" value="${lastName }" />
			<input name="address1" type="hidden" value="${address1 }" />
			<input name="address2" type="hidden" value="${address2 }" />
			<input name="city" type="hidden" value="${city }" />
			<input name="state" type="hidden" value="${state }" />
			<input name="postalCode" type="hidden" value="${postalCode }" />
			<input name="phoneNumber" type="hidden" value="${phoneNumber }" />
			<input name="ccnumber" type="hidden" value="${creditCardNumber }" />
			<input name="expirationMonth" type="hidden" value="${expirationMonth }" />
			<input name="expirationYear" type="hidden" value="${expirationYear }" />
			<input name="addressValidated" type="hidden" value="true" />
		</c:when>
		<c:when test="${empty lookupAddress &&  empty billingAddress}">
			
			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="PaymentGroupContainerService.paymentGroupMap" />
				<dsp:param name="elementName" value="creditCard" />
				<dsp:oparam name="output">
					<c:if test="${(not empty defaultPaymentGroupName) and defaultPaymentGroupName eq creditCardNickName}">
						<dsp:getvalueof var="defaultCreditCard" param="creditCard" />
					</c:if>
				</dsp:oparam>
			</dsp:droplet>
			
			<dsp:getvalueof var="creditCardNumber" value="${defaultCreditCard.creditCardNumber}" />
			<dsp:getvalueof var="expirationMonth" value="${defaultCreditCard.expirationMonth}" />
			<dsp:getvalueof var="expirationYear" value="${defaultCreditCard.expirationYear}" />
			<dsp:getvalueof var="firstName" value="${defaultCreditCard.billingAddress.firstName}" />
			<dsp:getvalueof var="lastName" value="${defaultCreditCard.billingAddress.lastName}" />
			<dsp:getvalueof var="address1" value="${defaultCreditCard.billingAddress.address1}" />
			<dsp:getvalueof var="address2" value="${defaultCreditCard.billingAddress.address2}" />
			<dsp:getvalueof var="state" value="${defaultCreditCard.billingAddress.state}" />
			<dsp:getvalueof var="city" value="${defaultCreditCard.billingAddress.city}" />
			<dsp:getvalueof var="postalCode" value="${defaultCreditCard.billingAddress.postalCode}" />
			<dsp:getvalueof var="phoneNumber" value="${defaultCreditCard.billingAddress.phoneNumber}" />
			<dsp:getvalueof var="country" value="${defaultCreditCard.billingAddress.country}" />

			<input name="country" type="hidden" value="${country }" />
			<input name="nickName" type="hidden" value="${nickname }" />
			<input name="firstName" type="hidden" value="${firstName }" />
			<input name="lastName" type="hidden" value="${lastName }" />
			<input name="address1" type="hidden" value="${address1 }" />
			<input name="address2" type="hidden" value="${address2 }" />
			<input name="city" type="hidden" value="${city }" />
			<input name="state" type="hidden" value="${state }" />
			<input name="postalCode" type="hidden" value="${postalCode }" />
			<input name="phoneNumber" type="hidden" value="${phoneNumber }" />
			<input name="ccnumber" type="hidden" value="${creditCardNumber }" />
			<input name="expirationMonth" type="hidden" value="${expirationMonth }" />
			<input name="expirationYear" type="hidden" value="${expirationYear }" />
			<input name="addressValidated" type="hidden" value="true" />
		</c:when>
		<c:when test="${lookupAddress eq true }">
			<dsp:setvalue bean="BillingFormHandler.creditCardNickName" paramvalue="nickname" />
			<dsp:setvalue bean="BillingFormHandler.updateOrderWithExistingCard" value="submit" />
			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="PaymentGroupContainerService.paymentGroupMap" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="profNickname" param="key" />
					<c:if test="${profNickname eq nickname }">
						<dsp:getvalueof var="creditCardNumber" param="element.creditCardNumber" />
						<dsp:getvalueof var="expirationMonth" param="element.expirationMonth" />
						<dsp:getvalueof var="expirationYear" param="element.expirationYear" />
						<dsp:getvalueof var="firstName" param="element.billingAddress.firstName" />
						<dsp:getvalueof var="lastName" param="element.billingAddress.lastName" />
						<dsp:getvalueof var="address1" param="element.billingAddress.address1" />
						<dsp:getvalueof var="address2" param="element.billingAddress.address2" />
						<dsp:getvalueof var="state" param="element.billingAddress.state" />
						<dsp:getvalueof var="city" param="element.billingAddress.city" />
						<dsp:getvalueof var="postalCode" param="element.billingAddress.postalCode" />
						<dsp:getvalueof var="phoneNumber" param="element.billingAddress.phoneNumber" />
						<dsp:getvalueof var="country" param="element.billingAddress.country" />

						<input name="country" type="hidden" value="${country }" />
						<input name="nickName" type="hidden" value="${nickname }" />
						<input name="firstName" type="hidden" value="${firstName }" />
						<input name="lastName" type="hidden" value="${lastName }" />
						<input name="address1" type="hidden" value="${address1 }" />
						<input name="address2" type="hidden" value="${address2 }" />
						<input name="city" type="hidden" value="${city }" />
						<input name="state" type="hidden" value="${state }" />
						<input name="postalCode" type="hidden" value="${postalCode }" />
						<input name="phoneNumber" type="hidden" value="${phoneNumber }" />
						<input name="ccnumber" type="hidden" value="${creditCardNumber }" />
						<input name="expirationMonth" type="hidden" value="${expirationMonth }" />
						<input name="expirationYear" type="hidden" value="${expirationYear }" />
						<input name="addressValidated" type="hidden" value="true" />

						<%-- <input name="cvvnumber" type="hidden" value="${ }" /> --%>
					</c:if>
				</dsp:oparam>
			</dsp:droplet>
		</c:when>
	</c:choose>
	<c:if test="${not empty address1}">
		<div class="billing-info credit-component col-md-5">
	
			<div>
				<fmt:message key="checkout.payment.billingInformation" />
				
				<c:choose>
					<c:when test="${not empty billingAddress}">
						<span>&#183;</span>
						<a href="javascript:void(0);" data-toggle="modal" class="checkoutEditBillingAddress" data-target="#billingPageAddressModal">
							<fmt:message key="checkout.payment.billingAddress.edit" />
						</a>
						<span class="nickNameInHiddenSpan">${selectedAddressNickName}</span>
					</c:when>
				</c:choose>
			</div>
			<div>${firstName }&nbsp;${lastName }</div>
			<div>${address1 }, ${address2 }</div>
			<div>${city }, ${state},&nbsp;${postalCode }</div>
			<div>${phoneNumber }</div>
			<div>${country}</div>
		</div>
	</c:if>
</dsp:page>