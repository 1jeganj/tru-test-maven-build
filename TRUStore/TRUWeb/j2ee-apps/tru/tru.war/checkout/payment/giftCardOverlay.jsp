<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>

	<dsp:getvalueof var="isPayInStoreSelected" param="isPayInStoreSelected" vartype="java.lang.boolean" />
	
	<div class="modal fade in" id="giftCardBalanceModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false" >
		<div class="modal-dialog modal-lg">
			<div class="modal-content sharp-border">
			<form id="giftCardBalanceForm" class="JSValidation" method="POST" onsubmit="javascript: return false;">
				<div class="modal-content sharp-border gift-card-balance-overlay">
						<div>
						    <a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
					    </div>
						<header><fmt:message key="checkout.giftcard.balance" /></header>
						<p><fmt:message key="checkout.giftcardoverlay.message1" /></p>
						<div class="gift-card-container">
							<p class="global-error-display"></p>
							<div class="row gift-card-input-group">
								<div class="gift-card-number-container">
									<label for="gcNumber1"><fmt:message key="checkout.payment.giftCardNumber"/></label>
									<c:choose>
										 <c:when test="${isPayInStoreSelected}">
										 	<input type="text" name="gcNumber" class="gcNumber" disabled="disabled" class="chkNumbersOnly" aria-label="gcNumber_chkNumbersOnly" title="gcNumber_chkNumbersOnly" onkeypress="return isNumberOnly(event, 16);"/>
										 </c:when>
										 <c:otherwise>
										 	<input type="text" name="gcNumber" class="gcNumber" class="chkNumbersOnly" aria-label="gcNumber_chkNumbersOnly" title="gcNumber_chkNumbersOnly" onkeypress="return isNumberOnly(event, 16);"/>
										 </c:otherwise>
									 </c:choose>
								</div>
								<div>
									<label for="ean"><fmt:message key="checkout.payment.pin"/></label>
									<c:choose>
										 <c:when test="${isPayInStoreSelected}">
											<input type="password" name="ean" class="ean" aria-label="eanLabel" title="eanLabel" disabled="disabled" class="chkNumbersOnly" onkeypress="return  isNumberOnly(event, 4);"/>
										</c:when>
										<c:otherwise>
											<input type="password" name="ean" class="ean" aria-label="eanLabel" title="eanLabel" class="chkNumbersOnly" onkeypress="return  isNumberOnly(event, 4);"/>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
							<button id="giftCardBalanceOverlayBtn"><fmt:message key="checkout.check.balance"/></button>
						</div>
						<div class="gift-card-balance-container">
							<div></div>
							<a href="javascript:void(0);" class="check-another"><fmt:message key="checkout.check.another.card"/></a>
						</div>
						<hr>
						<img src="${TRUImagePath}images/gift-card-help.png" alt="gift card back">
					</div>
			 </form>
			</div>
		</div>
	</div>
</dsp:page>