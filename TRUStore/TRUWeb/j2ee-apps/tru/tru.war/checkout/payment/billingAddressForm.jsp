<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService" />
	<dsp:importbean bean="/com/tru/common/TRUUserSession" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:getvalueof var="nickNameInHiddenSpan" param="nickNameInHiddenSpan"/>
	<dsp:getvalueof var="rusBillingEdit" param="rusBillingEdit"/>
	<dsp:getvalueof var="isFromPAymentPage" param="isFromPAymentPage"/>
	<%-- start getting billingAddress to pre-populate fields when user clicks edit links from RUS credit card section for billing address --%>
	<%-- <c:if test="${not empty nickNameInHiddenSpan}">
		<dsp:droplet name="ForEach">
			<dsp:param name="array" bean="ShippingGroupContainerService.shippingGroupMapForDisplay" />
			<dsp:param name="elementName" value="shippingGroup"/>
	       	<dsp:oparam name="output">
				<dsp:getvalueof var="addressNickName" param="key" />
				<c:if test="${addressNickName eq nickNameInHiddenSpan}">
					<dsp:getvalueof var="billingAddress" param="shippingGroup.shippingAddress" />
					<dsp:getvalueof var="foundAddressInAddressBook" value="true" vartype="java.lang.boolean" />
				</c:if>
			</dsp:oparam>
		</dsp:droplet>
	</c:if>
	
	
	<c:if test="${rusBillingEdit eq 'true'}">
		<dsp:getvalueof var="billingAddress" bean="TRUUserSession.tSPBillingAddressMap" />
		<dsp:getvalueof var="foundAddressInAddressBook" value="true" vartype="java.lang.boolean" />
	</c:if>
	
	If address is not found in address book, show order credit card billing address
	<c:if test="${not foundAddressInAddressBook}">
		<dsp:droplet name="ForEach">
			<dsp:param name="array" bean="ShoppingCart.current.paymentGroups" />
			<dsp:param name="elementName" value="pg" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="pgType" param="pg.paymentGroupClassType" />
				<c:choose>
					<c:when test="${pgType eq 'creditCard' }">
						<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
							<dsp:param name="value" param="pg.billingAddress.address1" />
							<dsp:oparam name="false">
								<dsp:getvalueof var="billingAddress" param="creditCard.billingAddress" />
							</dsp:oparam>
						</dsp:droplet>
					</c:when>
				</c:choose>
			</dsp:oparam>
		</dsp:droplet>
	</c:if> --%>
	<%-- end getting billingAddress to pre-populate fields when user clicks edit links from RUS credit card section for billing address --%>
	
	<dsp:getvalueof var="billingAddress" bean="ShoppingCart.current.billingAddress" />
	<div class="co-billing-address">
		<div class="checkout-billing-address-form checkout-shipping-address-form">
		<%-- <c:if test="${isFromPAymentPage eq true}">
			<a href="javascript:void(0);" class="checkoutEditBillingAddress_cancel">
								<fmt:message key="myaccount.cancel" />
							</a>
		</c:if> --%>
			<div class="checkout-billing-address-form-header">
				<div class="inline address-form-header credit-card-billing-address-header">
					<h3 class="checkout-page-form-header"><fmt:message key="checkout.overlay.billingAddress"/></h3>
					<p class="required-label">
						<span class="orange-astericks"><fmt:message key="myaccount.asterisk" />&nbsp;</span><fmt:message key="checkout.overlay.required"/>
					</p>
				</div>
			</div>
			<div class="checkout-shipping-address-form-content pad-top-20">
				<div class="row left-padding billing-address pull-up">
					<div class="col-md-9">
						<div class="pad-bottom-0">
							<div class="shipping-address-field select-wrapper billing">
								<span class="orange-astericks">&#42;</span>&nbsp;
								<label for="country"><fmt:message key="checkout.overlay.country"/></label>								
								<select name="country" class="shipping-address-field-select" id="country" onchange="onCountryChangeInPayment();">
									<dsp:droplet name="ForEach">
										<dsp:param name="array" bean="/atg/commerce/util/CountryList.places" />
										<dsp:param name="elementName" value="billingCountry" />
										<dsp:oparam name="output">
											<dsp:getvalueof var="billingCountry" param="billingCountry.code"></dsp:getvalueof>
											<option value='<dsp:valueof param="billingCountry.code" />'
												<c:choose>
													<c:when test="${not empty billingAddress}">
														<dsp:droplet name="/atg/dynamo/droplet/Compare">
			                                                <dsp:param name="obj1" param="billingCountry.code"/>
			                                                <dsp:param name="obj2" value="${billingAddress.country}" />
			                                                <dsp:oparam name="equal">
			                                                    selected="selected"
			                                                </dsp:oparam>
				                                        </dsp:droplet>
													</c:when>
													<c:otherwise>
														<dsp:droplet name="/atg/dynamo/droplet/Compare">
			                                                <dsp:param name="obj1" param="billingCountry.code"/>
			                                                <dsp:param name="obj2" value="US" />
			                                                <dsp:oparam name="equal">
			                                                    selected="selected"
			                                                </dsp:oparam>
				                                        </dsp:droplet>
													</c:otherwise>
												</c:choose>>
												<dsp:valueof param="billingCountry.displayName" />
											</option>
										</dsp:oparam>
									</dsp:droplet>
								</select>
							</div>
							<div class="shipping-address-field adjust-margin tru-float-label-field">
								<div class="shipping-address-field-required orange-astericks">&#42;</div>
								<fmt:message key="checkout.shipping.firstname" var="firstnameLabel" />
								<label class="tru-float-label" for="firstName">${firstnameLabel}</label>
								<input id="firstName" class="shipping-address-field-input tru-float-label-input" type="text" maxlength="30"  name="firstName" value="${billingAddress.firstName}" placeholder="${firstnameLabel}" aria-required="true"/>
								<div class="row">
									<div class="col-sm-1 col-sm-push-10">
										<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
									</div>
								</div>
							</div>
							<div class="shipping-address-field adjust-margin tru-float-label-field">
								<div class="shipping-address-field-required orange-astericks">&#42;</div>
								<fmt:message key="checkout.shipping.lastname" var="lastnameLabel" />
								<label class="tru-float-label" for="lastName">${lastnameLabel}</label>
								<input id="lastName" class="shipping-address-field-input tru-float-label-input" type="text" name="lastName" maxlength="30"  value="${billingAddress.lastName}" placeholder="${lastnameLabel}" aria-required="true"/>
								<div class="row">
									<div class="col-sm-1 col-sm-push-10">
										<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
									</div>
								</div>					
							</div>
							<div class="shipping-address-field adjust-margin tru-float-label-field">
								<div class="shipping-address-field-required orange-astericks">&#42;</div>
								<fmt:message key="checkout.shipping.address1" var="address1Label" />
								<label class="tru-float-label" for="address1">${address1Label}</label>							
								<input id="address1" class="shipping-address-field-input tru-float-label-input" type="text" name="address1" value="${billingAddress.address1}" placeholder="${address1Label}" aria-required="true" maxlength="50"/>
								<div class="row">
									<div class="col-sm-1 col-sm-push-10">
										<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
									</div>
								</div>					
							</div>
							<div class="shipping-address-field adjust-margin tru-float-label-field">
								<fmt:message key="checkout.shipping.suite" var="aptsuiteLabel" />
								<label class="tru-float-label" for="address2">${aptsuiteLabel}</label>									
								<input id="address2" class="shipping-address-field-input tru-float-label-input" type="text" value="${billingAddress.address2}" name="address2" maxlength="50" placeholder="${aptsuiteLabel}"/>
								<div class="row">
									<div class="col-sm-1 col-sm-push-10">
										<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
									</div>
								</div>					
							</div>
							<div class="shipping-address-field adjust-margin tru-float-label-field">
								<div class="shipping-address-field-required orange-astericks">&#42;</div>
								<fmt:message key="checkout.shipping.city" var="cityLabel" />
								<label class="tru-float-label" for="city">${cityLabel}</label>								
								<input id="city" class="shipping-address-field-input tru-float-label-input" type="text" name="city" value="${billingAddress.city}" placeholder="${cityLabel}" aria-required="true" maxlength="30"/>
								<div class="row">
									<div class="col-sm-1 col-sm-push-10">
										<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
									</div>
								</div>					
							</div>
							<div class="shipping-address-field">
								<span class="orange-astericks">&#42;</span>&nbsp;<label class="shipping-address-field-label" for="state"><fmt:message
										key="checkout.shipping.state" /></label>
								<div class="select-wrapper">
									<select name="state" class="shipping-address-field-select" id="state" aria-required="true">
										<option value=""><fmt:message key="checkout.shipping.state.select"/></option>
										<dsp:droplet name="/atg/dynamo/droplet/ForEach">
											<dsp:param name="array" bean="/atg/commerce/util/StateList_US.places" />
											<dsp:param name="elementName" value="state" />
											<dsp:oparam name="output">
												<dsp:getvalueof var="stateCode" param="state.code"></dsp:getvalueof>
												<c:choose>
													<c:when test="${not empty billingAddress}">
														<dsp:droplet name="/atg/dynamo/droplet/Compare">
			                                                <dsp:param name="obj1" value="${stateCode}"/>
			                                                <dsp:param name="obj2" value="${billingAddress.state}" />
			                                                <dsp:oparam name="equal">
			                                                   <option value="${stateCode}" selected="selected"><dsp:valueof param="state.code" /></option>
			                                                </dsp:oparam>
			                                                <dsp:oparam name="default">
																<option value="${stateCode}"><dsp:valueof param="state.code" /></option>
			                                                </dsp:oparam>
				                                        </dsp:droplet>
													</c:when>
													<c:otherwise>
														<option value="${stateCode}"><dsp:valueof param="state.code" /></option>
													</c:otherwise>
												</c:choose>
											</dsp:oparam>
										</dsp:droplet>
									</select>
								</div>
							</div>							
							<div class="shipping-address-field adjust-margin zip-field-adjust tru-float-label-field">
								<div class="shipping-address-field-required orange-astericks">&#42;</div>
								<fmt:message key="checkout.shipping.zip" var="zipLabel" />
								<label class="tru-float-label" for="postalCode">${zipLabel}</label>								
								<input id="postalCode" class="shipping-address-field-input tru-float-label-input" type="text" name="postalCode" value="${billingAddress.postalCode}" maxlength="10" aria-required="true" placeholder="${zipLabel}"/>
								<div class="row">
									<div class="col-sm-1 col-sm-push-10">
										<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
									</div>
								</div>					
							</div>
							<div class="shipping-address-field adjust-margin tru-float-label-field">
								<div class="shipping-address-field-required orange-astericks">&#42;</div>
								<fmt:message key="checkout.shipping.telephone" var="telephoneLabel" />
								<label class="tru-float-label" for="phoneNumber">${telephoneLabel}</label>										
								<input id="phoneNumber" class="shipping-address-field-input tru-float-label-input" type="text" name="phoneNumber" value="${billingAddress.phoneNumber}" class="splCharCheck" placeholder="${telephoneLabel}" onkeypress="return isPhoneNumberFormat(event)" maxlength="20" aria-required="true"/>
								<div class="row">
									<div class="col-sm-1 col-sm-push-10">
										<div class="checkout-flow-sprite checkout-flow-clear shipping-address-field-clear"></div>
									</div>
								</div>					
							</div>
							<input id="newBillingAddress" name="newBillingAddress" type="hidden" value="true" />
							<input id="editBillingAddress" name="editBillingAddress" type="hidden" value="false" />
							<input id="addressValidated" name="addressValidated" type="hidden" value="false" />
						</div>						
						<!--
						<div class="pad-bottom-0">
							<span class="orange-astericks"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<label for="country"><fmt:message key="checkout.overlay.country"/></label>
							<div class="select-wrapper billing">
								<select name="country" id="country" onchange="onCountryChangeInPayment();">
									<dsp:droplet name="ForEach">
										<dsp:param name="array" bean="/atg/commerce/util/CountryList.places" />
										<dsp:param name="elementName" value="billingCountry" />
										<dsp:oparam name="output">
											<dsp:getvalueof var="billingCountry" param="billingCountry.code"></dsp:getvalueof>
											<option value='<dsp:valueof param="billingCountry.code" />'
												<c:choose>
													<c:when test="${not empty billingAddress}">
														<dsp:droplet name="/atg/dynamo/droplet/Compare">
			                                                <dsp:param name="obj1" param="billingCountry.code"/>
			                                                <dsp:param name="obj2" value="${billingAddress.country}" />
			                                                <dsp:oparam name="equal">
			                                                    selected="selected"
			                                                </dsp:oparam>
				                                        </dsp:droplet>
													</c:when>
													<c:otherwise>
														<dsp:droplet name="/atg/dynamo/droplet/Compare">
			                                                <dsp:param name="obj1" param="billingCountry.code"/>
			                                                <dsp:param name="obj2" value="US" />
			                                                <dsp:oparam name="equal">
			                                                    selected="selected"
			                                                </dsp:oparam>
				                                        </dsp:droplet>
													</c:otherwise>
												</c:choose>>
												<dsp:valueof param="billingCountry.displayName" />
											</option>
										</dsp:oparam>
									</dsp:droplet>
								</select>
							</div>
							<div>
								<span class="orange-astericks"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<label for="firstName"><fmt:message key="checkout.shipping.firstname"/></label>
								 <input type="text" name="firstName" maxlength="30"   id="firstName" value="${billingAddress.firstName}" />
							</div>
							<div>
								<span class="orange-astericks"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<label for="lname"><fmt:message key="checkout.shipping.lastname"/></label>
								 <input type="text" name="lastName" maxlength="30" id="lastName" value="${billingAddress.lastName}" />
							</div>
							<div>
								<span class="orange-astericks"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<label for="address1"><fmt:message key="checkout.shipping.address1"/>
									</label> <input id="address1" type="text" name="address1" id="address1" value="${billingAddress.address1}" />
							</div>
							<div>
								<label for="aptsuite"><fmt:message key="checkout.shipping.suite"/></label> <input
									id="address2" class="non-mandatory" type="text" name="address2" id="address2" value="${billingAddress.address2}" />
							</div>
							<div>
								<span class="orange-astericks"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<label for="city"><fmt:message key="checkout.shipping.city"/></label>
								<input type="text" id="city" name="city" id="city" value="${billingAddress.city}" />
							</div>
							<div>
								<label for="state" class="avenir-heavy creditCardStateLabel"><span class="orange-astericks"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<fmt:message key="checkout.shipping.state"/></label>
								
								<div class="select-wrapper other-state pad-bottom-0">
									<select name="state" id="state" class="select-state-option" onchange="onCreditCardSelectedStateChange(this)">
										<option value=""><fmt:message key="checkout.overlay.pleaseSelect"/></option>
										<dsp:droplet name="/atg/dynamo/droplet/ForEach">
											<dsp:param name="array" bean="/atg/commerce/util/StateList_US.places" />
											<dsp:param name="elementName" value="state" />
											<dsp:oparam name="output">
												<dsp:getvalueof var="stateCode" param="state.code"></dsp:getvalueof>
												<c:choose>
													<c:when test="${not empty billingAddress}">
														<dsp:droplet name="/atg/dynamo/droplet/Compare">
			                                                <dsp:param name="obj1" value="${stateCode}"/>
			                                                <dsp:param name="obj2" value="${billingAddress.state}" />
			                                                <dsp:oparam name="equal">
			                                                   <option value="${stateCode}" selected="selected"><dsp:valueof param="state.code" /></option>
			                                                </dsp:oparam>
			                                                <dsp:oparam name="default">
																<option value="${stateCode}"><dsp:valueof param="state.code" /></option>
			                                                </dsp:oparam>
				                                        </dsp:droplet>
													</c:when>
													<c:otherwise>
														<option value="${stateCode}"><dsp:valueof param="state.code" /></option>
													</c:otherwise>
												</c:choose>
											</dsp:oparam>
										</dsp:droplet>
									</select>
								</div>
							</div>
							<div>
								<label for="otherState" class="creditCardOtherStateLabel"><%-- <span class="orange-astericks"><fmt:message key="myaccount.asterisk" /></span> --%>&nbsp;<fmt:message key="checkout.shipping.otherstate" /></label>
								<%-- <span class="creditCardOtherStateLabel"><fmt:message key="checkout.shipping.noOtherstate" /></span> --%>
								<input type="text" id="otherState" name="otherState" maxlength="20"/>
							</div>
							<div>
								<span class="orange-astericks"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<label for="zip"><fmt:message key="checkout.shipping.zip"/></label> 
								<input type="text" id="zip" name="postalCode"
									maxlength="10" value="${billingAddress.postalCode}" />
							</div>
							<div class="pad-bottom-0">
								<span class="orange-astericks"><fmt:message key="myaccount.asterisk" /></span>&nbsp;<label for="telephone"><fmt:message key="checkout.shipping.telephone"/></label>
								<input id="phoneNumber" type="text" name="phoneNumber" onkeypress="return isPhoneNumberFormat(event)" maxlength="20"
									class="splCharCheck" value="${billingAddress.phoneNumber}" />
							</div>
							<input id="newBillingAddress" name="newBillingAddress" type="hidden" value="false" />
							<input id="editBillingAddress" name="editBillingAddress" type="hidden" value="true" />
							<input id="addressValidated" name="addressValidated" type="hidden" value="false" />
						</div>
						-->
					</div>
				</div>
			</div>
		</div>
	</div>
</dsp:page>