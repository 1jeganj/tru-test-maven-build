<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftCardFormHandler"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/multisite/SiteContext" />
	<dsp:importbean bean="/com/tru/droplet/TenderBasedCheckDroplet" />	
	<dsp:getvalueof var="isPayInStoreEligible" param="isPayInStoreEligible" vartype="java.lang.boolean" />
	<dsp:getvalueof var="isPayInStoreSelected" param="isPayInStoreSelected" vartype="java.lang.boolean" />
	<dsp:droplet name="TenderBasedCheckDroplet">
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:oparam name="output">
		<dsp:getvalueof var="showGiftCardFlag" param="showGiftCardFlag"/>
		</dsp:oparam>
		</dsp:droplet>
      <div class="checkout-payment-giftcard <c:if test="${isPayInStoreSelected or showGiftCardFlag}">giftcard-blur</c:if>">
		<header><fmt:message key="checkout.payment.giftCard" /></header>
		<div class="inline"><fmt:message key="checkout.you.can" />&nbsp;<span><fmt:message key="checkout.add.upto" />&nbsp;<dsp:valueof bean="SiteContext.site.maxGiftCardLimit" /></span>&nbsp;<fmt:message key="checkout.giftcards" /></div>
		<input type="hidden" id="isPayInStoreSelected" value="${isPayInStoreSelected}">
			<form id="giftCardApplicationForm" method="POST" onsubmit="javascript: return false;" class="JSValidation" autocomplete="off">
				<div>
					<div class="gcNumberDiv">
						<label for="gcNumber">
						   <fmt:message key="checkout.payment.giftCardNumber" />
						</label>
						<input type="text" name="gcNumber" class="gcNumber" class="chkNumbersOnly" aria-label="gcNumberLabel" title="gcNumberLabel" onkeypress="return isNumberOnly(event, 16);" <c:if test="${isPayInStoreSelected or showGiftCardFlag}">disabled="disabled"</c:if> />
					</div>
					<div class="gcEanDiv">
						<label for="ean">
						   <fmt:message key="checkout.payment.pinNumber"  />
						</label>
						<%-- <button id="co-payment-gift-card" <c:if test="${isPayInStoreSelected}">disabled="disabled"</c:if> >
						<fmt:message key="checkout.payment.apply" /></button> --%>
						<input type="password" name="ean" class="ean" class="chkNumbersOnly" aria-label="eanLabel" title="eanLabel" onkeypress="return isNumberOnly(event, 4);" <c:if test="${isPayInStoreSelected or showGiftCardFlag}">disabled="disabled"</c:if> />
						<button id="co-payment-gift-card" <c:if test="${isPayInStoreSelected or showGiftCardFlag}">disabled="disabled"</c:if> >
						<fmt:message key="checkout.payment.apply" /></button>														
					</div>
					<div class="gcBalanceDiv">
						<p>
						<a class="checkGiftCarddetails <c:if test="${isPayInStoreSelected or showGiftCardFlag}">giftCardDisabled</c:if>" href="JavaScript:void(0)"><fmt:message key="checkout.payment.giftCardBalance" /></a>
						</p>
						<p>
						<div class="checkoutGiftCardHelpClass-wrapper" style="position: relative">
							<a class="checkoutGiftCardHelpClass <c:if test="${isPayInStoreSelected or showGiftCardFlag}"> giftCardDisabled </c:if>" href="javascript:void(0);" data-toggle="popover" data-original-title="" title=""><fmt:message key="checkout.payment.giftCardHelp" /></a>
							<div class="checkoutGiftCardHelptooltip " role="tooltip" data-toggle="popover" style="display: none;" data-original-title="" title="">
								<div class="giftcardTooltippopover popover fade bottom in">
									<div class="arrow"></div>
									<div class="content">
										<%--
										<dsp:droplet name="/atg/targeting/TargetingFirst">
											<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/AccountManagement/GiftCardHelpTargeter" />
											<dsp:oparam name="output">
												<dsp:valueof param="element.data" valueishtml="true" />
											</dsp:oparam>
										</dsp:droplet>
										--%>
									</div>
								</div>
							</div>
						</div>
						
						</p>
					</div>
				</div>
			</form>
			<dsp:include page="displayGiftCards.jsp" />
	</div>
	  
	<dsp:include page="giftCardOverlay.jsp">
		<dsp:param name="isPayInStoreSelected" value="${isPayInStoreSelected}" />
	</dsp:include>
		<!-- isPayInStoreSelected: ${isPayInStoreSelected}  showGiftCardFlag : ${showGiftCardFlag} -->
</dsp:page>