<dsp:page>
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/commerce/order/purchase/PaymentGroupDroplet" />
	<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftCardFormHandler" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupDroplet" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUParseSynchronyResponseDroplet" />

	<dsp:setvalue bean="ShoppingCart.current.previousTab" beanvalue="ShoppingCart.current.currentTab" />
	<dsp:setvalue bean="ShoppingCart.current.currentTab" value="payment-tab" />
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />

	<dsp:param name="pageName" value="Payment" />
	<input type="hidden" id="tealiumPageName" value="payment" />

	<dsp:getvalueof var="isTransient" bean="Profile.transient" />
	<%--Start : MVP 86 & 97 OOPs error message for --%>
		<input type="hidden" id="shipRestrictionsFound" value="false" />
		<%--End : --%>
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" bean="ShippingGroupContainerService.shippingGroupMapForDisplay" />
				<dsp:oparam name="true">
					<%--Start :: Commented as part of performance issue in payment pag --%>
						<dsp:droplet name="ShippingGroupDroplet">
							<dsp:param name="createOneInfoPerUnit" value="false" />
							<dsp:param name="clearShippingInfos" value="true" />
							<%-- <dsp:param name="clearShippingGroups" value="${not isTransient}" /> --%>
								<dsp:param name="shippingGroupTypes" value="hardgoodShippingGroup,channelHardgoodShippingGroup" />
								<dsp:param name="initShippingGroups" value="true" />
								<dsp:param name="initBasedOnOrder" value="false" />
								<dsp:oparam name="output" />
						</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>

			<%--End :: Commented as part of performance issue in payment pag --%>

				<dsp:droplet name="PaymentGroupDroplet">
					<%-- <dsp:param name="clearPaymentGroups" value="${not isTransient}" /> --%>
						<dsp:param name="paymentGroupTypes" value="creditCard" />
						<dsp:param name="initPaymentGroups" value="true" />
						<dsp:oparam name="output" />
				</dsp:droplet>

				<dsp:droplet name="TRUParseSynchronyResponseDroplet">
					<dsp:param name="SDP" value='<%=request.getParameter("SDP")%>' />
					<dsp:oparam name="output">
						<dsp:getvalueof var="synchronySDPResponseObj" param="synchronySDPResponseObj" />
					</dsp:oparam>
					<dsp:oparam name="empty">
						<dsp:getvalueof var="synchronySDPResponseObj" value="" />
					</dsp:oparam>
				</dsp:droplet>

				<c:if test="${(not empty synchronySDPResponseObj and synchronySDPResponseObj.applicationDecision eq 'A')}">
					<dsp:include page="populateTSPInfoToUserSession.jsp">
						<dsp:param name="synchronySDPResponseObj" value="${synchronySDPResponseObj}" />
					</dsp:include>
				</c:if>

				<dsp:getvalueof var="applyExpressCheckout" bean="ShoppingCart.current.applyExpressCheckout" />
				<dsp:setvalue bean="BillingFormHandler.updateSynchronyDataInOrder" value="submit" />
				<c:choose>
					<c:when test="${applyExpressCheckout}">
						<dsp:setvalue bean="BillingFormHandler.paymentGroupType" value="creditCard" />
						<dsp:setvalue bean="BillingFormHandler.ensurePaymentGroup" value="submit" />
					</c:when>
					<c:when test="${(not empty synchronySDPResponseObj and synchronySDPResponseObj.applicationDecision eq 'A')}">
						<dsp:setvalue bean="BillingFormHandler.paymentGroupType" value="creditCard" />
						<dsp:setvalue bean="BillingFormHandler.RUSCard" value="true" />
						<dsp:setvalue bean="BillingFormHandler.ensurePaymentGroup" value="submit" />
					</c:when>
				</c:choose>
				<dsp:setvalue bean="GiftCardFormHandler.adjustPaymentGroups" value="submit" />

				<%-- Start Checkout Payment Template template --%>
					<div id="co-payment-tab">
						<input type="hidden" value="paymentPage" id="pageName" />
						<p class="global-error-display"></p>
						<dsp:droplet name="/atg/dynamo/droplet/Switch">
							<dsp:param bean="BillingFormHandler.formError" name="value" />
							<dsp:oparam name="true">
								<dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
									<dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
									<dsp:oparam name="outputStart">
										<span class="payPalErrorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
									</dsp:oparam>
									<dsp:oparam name="outputEnd">
										</span>
									</dsp:oparam>
								</dsp:droplet>
							</dsp:oparam>
							<dsp:oparam name="false">
								<dsp:droplet name="/atg/dynamo/droplet/Switch">
									<dsp:param bean="/atg/commerce/order/purchase/CommitOrderFormHandler.formError" name="value" />
									<dsp:oparam name="true">
										<dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
											<dsp:param name="exceptions" bean="/atg/commerce/order/purchase/CommitOrderFormHandler.formExceptions" />
											<dsp:oparam name="outputStart">
												<span class="payPalErrorDisplay">
			             </dsp:oparam>
			              <dsp:oparam name="output">
			                  <span><dsp:valueof param="message"/></span>
											</dsp:oparam>
											<dsp:oparam name="outputEnd">
												</span>
											</dsp:oparam>
										</dsp:droplet>
									</dsp:oparam>
									<%-- <dsp:oparam name="false">
		           		<dsp:getvalueof var="paypalErrorMessages" param="paypalErrorMessages" />
   						<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
							<dsp:param name="value" value="${paypalErrorMessages}" />
							<dsp:oparam name="false">
		   						<c:set var="splitPayPalErrorMsgs" value="${fn:split(paypalErrorMessages,'|')}" />
								<span class="payPalErrorDisplay">
									<dsp:droplet name="/atg/dynamo/droplet/ForEach">
										<dsp:param name="array" value="${splitPayPalErrorMsgs}" />
										<dsp:param name="elementName" value="paypalErrorMessage" />
										<dsp:oparam name="output">
											<span><dsp:valueof param="paypalErrorMessage"/></span><br/>
										</dsp:oparam>
									</dsp:droplet>
								</span>
							</dsp:oparam>
						</dsp:droplet>
		           </dsp:oparam> --%>
								</dsp:droplet>
							</dsp:oparam>
						</dsp:droplet>
						<dsp:include page="promoCodeErrorDisplay.jsp" />
						<div class="checkout-payment">
							<div class="row">
								<%--Start : Including the payment form for payment related one. --%>
									<dsp:include page="paymentForm.jsp">
										<dsp:param name="synchronySDPResponseObj" value="${synchronySDPResponseObj}" />
									</dsp:include>
									<%--End : Including the payment form for payment related one. --%>
										<input id="moveToOrderReview" type="submit" name="moveToOrderReview" value="MoveToReview" style="display: none;" />
										<dsp:include page="creditCard/creditCardInfoOverlay.jsp">
											<dsp:param name="stopScroll" value="toStopScroll" />
										</dsp:include>
										<div class="col-md-3 checkout-right-column" id="checkOutOrderSummary">
											<dsp:include page="/checkout/common/checkoutOrderSummary.jsp">
												<dsp:param name="pageName" param="pageName" />
											</dsp:include>
										</div>
							</div>
						</div>
					</div>
					<dsp:include page="/checkout/common/ad-Zone.jsp" />
					<dsp:include page="exitPaymentOverlay.jsp" />
					<%-- /End Checkout Payment Template template --%>
						<div class="modal fade" id="payment-page-suggested-address" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
							<div class="modal-dialog modal-dialog-payment-page-suggested"></div>
						</div>
						<div class="modal fade" id="checkoutReviewProcessingOverlay" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
							<div class="modal-dialog modal-md">
								<div class="modal-content sharp-border">
									<img class="" src="${TRUImagePath}images/processing.gif" alt="Processing...">
									<header class="review-processing-header">&nbsp;
										<fmt:message key="checkout.review.processOrderOverlayHeading" />&nbsp;</header>
									<p>
										<fmt:message key="checkout.review.processOrderOverlaySubHeading" />
									</p>
								</div>
							</div>
						</div>
</dsp:page>