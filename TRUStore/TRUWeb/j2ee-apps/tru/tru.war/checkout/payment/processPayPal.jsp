<dsp:page>

<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<img src="${TRUImagePath}images/loader-ring-alt.gif">
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:include page="/checkout/payment/fraudJSFiles.jsp"/>
<script>
function pageInit(){
	callingFraudJsCollector();
	document.payPalForm.submit();	
}
</script>
	<body onLoad="pageInit();">
   		<dsp:form name ="payPalForm" method="post" id="paypal_approvement_form" >
   			<dsp:input type="hidden" bean="CommitOrderFormHandler.payPalToken" paramvalue="token"/>
   			<dsp:input type="hidden" bean="CommitOrderFormHandler.pageName" paramvalue="pageName" />   
   			<dsp:input type="hidden" bean="CommitOrderFormHandler.deviceID" id="field_d1" />
   			<dsp:input type="hidden" bean="CommitOrderFormHandler.payerId" paramvalue="PayerID" />
   			<dsp:input type="hidden" bean="CommitOrderFormHandler.payPalCommit" value="true" priority="-10"/>
   		</dsp:form>
  </body>
</dsp:page>