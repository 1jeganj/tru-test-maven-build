<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<div class="checkOut-promocode-error-state ">
		<div class="checkOut-promocode-error-state-header row">
			<div class="error-state-exclamation-lg img-responsive col-md-2"></div>
			<div class="checkOut-promocode-error-state-header-text col-md-10">
				<fmt:message key="shoppingcart.oops.issue" />
				<div class="checkOut-promocode-error-state-subheader">
					<span></span>&nbsp;<a class="why-not-promo-code-not-applied">&nbsp;<fmt:message
							key="checkout.why.not" />
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="popover seeTerms whyNotCouponCode fade bottom in" role="tooltip">
		<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters/TRU/Checkout/PromoCodeErrorDisplayTargeter" />
		<dsp:getvalueof var="cacheKey" value="${atgTargeterpath}${siteId}${locale}" />	
		<dsp:droplet name="/com/tru/cache/TRUPromoCodeErrorTargeterCacheDroplet">
			<dsp:param name="key" value="${cacheKey}" />
			<dsp:oparam name="output">
				<dsp:droplet name="/atg/targeting/TargetingFirst">
					<dsp:param name="targeter" bean="${atgTargeterpath}" />
						<dsp:oparam name="output">
							<dsp:valueof param="element.data" valueishtml="true" />
						</dsp:oparam>
				</dsp:droplet>
			</dsp:oparam>
		</dsp:droplet>
	</div>
</dsp:page>