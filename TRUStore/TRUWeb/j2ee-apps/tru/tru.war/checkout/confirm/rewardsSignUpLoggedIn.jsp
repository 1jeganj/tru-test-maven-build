<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/atg/multisite/Site"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:getvalueof bean="Site.olsonSignUpURL" var="olsonSignUpURL" />
<dsp:getvalueof bean="Site.olsonURL" var="olsonURL" />
<dsp:getvalueof bean="Profile.rewardNo" var="rewardNumber"></dsp:getvalueof>
	<div class="col-md-4 your-order-complete">
		<div class="bordered-column checkout-order-complete__content-block">
			<div class="bordered-column checkout-order-complete__content-block-header checkout-order-complete__content-block-header--rewards">
			</div>
			<div class="checkout-order-complete__content-block-body">
				<div class="membership-adds-up">
					<div class="checkout-order-complete__content-block-title">
						<fmt:message key="checkout.confirm.membershipHeading" />
					</div>
					<ul>
						<li><fmt:message key="checkout.confirm.membershipPoint1" /></li>
						<li><fmt:message key="checkout.confirm.membershipPoint2" /></li>
						<li><fmt:message key="checkout.confirm.membershipPoint3" /></li>
					</ul>
				</div>
				<c:choose>
					<c:when test="${empty rewardNumber}">
						<a href="${olsonSignUpURL}" class="checkout-order-complete__content-block-button checkout-order-complete__content-block-button--attach-bottom">&nbsp;<fmt:message
							key="checkout.confirm.membershipEnrollButton" />
						</a>
					</c:when>
					<c:otherwise>
					 	<a href="${olsonURL}" class="checkout-order-complete__content-block-button checkout-order-complete__content-block-button--attach-bottom">
					 		<fmt:message key="checkout.confirm.goToMyAccountButton" />
					 	</a>
					</c:otherwise>
				</c:choose> 
			</div>
		</div>
	</div>
</dsp:page>                