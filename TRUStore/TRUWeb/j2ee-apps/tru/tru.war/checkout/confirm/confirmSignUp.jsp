<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
    <dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
    <dsp:getvalueof var="contextPath" value="${originatingRequest.contextPath}" />
    <dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
    <dsp:importbean bean="/atg/userprofiling/CheckoutProfileFormHandler" />
    <c:set var="isSosVar" value="${cookie.isSOS.value}" />
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
    <dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
    <dsp:importbean bean="/atg/commerce/ShoppingCart" />
    <dsp:param name="order" bean="ShoppingCart.last" />
    <%-- 	<dsp:valueof bean="ShoppingCart.current"></dsp:valueof><br>
		<dsp:valueof bean="ShoppingCart.last"></dsp:valueof> --%>
        <c:if test="${!isSosVar || !(site eq 'sos')}">
            <div class="col-md-4 create-an-account">
                <div class="create-account-thank-you">
                    <h2>
                        <fmt:message key="checkout.confirm.signUpThanksHeading" />
                    </h2>
                    <button onclick="goToMyAccount();"><fmt:message key="checkout.confirm.goToMyAccountButton" /></button>
                    <h3>
                        <p>
                            <fmt:message key="checkout.confirm.benefits" />
                        </p>
                    </h3>
                    <ul>
                        <li>
                            <fmt:message key="checkout.confirm.benefit1" />
                        </li>
                        <li>
                            <fmt:message key="checkout.confirm.benefit2" />
                        </li>
                        <li>
                            <fmt:message key="checkout.confirm.benefit3" />
                        </li>
                        <li>
                            <fmt:message key="checkout.confirm.benefit4" />
                        </li>
                        <li>
                            <fmt:message key="checkout.confirm.benefit5" />
                        </li>
                    </ul>
                </div>
                <div class="bordered-column checkout-order-complete__content-block">
                    <dsp:form id="confirmSignUpForm" method="post" class="JSValidation" autocomplete="off">
                        <p class="global-error-display"></p>
                        <div class="bordered-column checkout-order-complete__content-block-header checkout-order-complete__content-block-header--create-an-account">
                        </div>
                        <div class="checkout-order-complete__content-block-body">
                            <div class="create-account checkout-order-complete__content-block-title">
                                <fmt:message key="checkout.confirm.createAccount" />
                            </div>
                            <p>
                                <fmt:message key="checkout.confirm.createAccountMessage" />
                                <ul>
                                    <li>
                                        <fmt:message key="checkout.confirm.createAccountMessage.one" />
                                    </li>
                                    <li>
                                        <fmt:message key="checkout.confirm.createAccountMessage.two" />
                                    </li>
                                    <li>
                                        <fmt:message key="checkout.confirm.createAccountMessage.three" />
                                    </li>
                                </ul>
                            </p>
                            <div class="checkout-order-complete__content-block-my-account">
                                <div id="oc-signupEmailDivLabel" class="checkout-order-complete__content-block-my-account--bold">
                                    <fmt:message key="checkout.shipping.email" />
                                </div>
                                <input type="text" value="${email}" name="email" class="checkout-order-complete__content-block-field" id="oc-signupEmail" maxlength="60" />
                                <div id="oc-signupEmailDiv">
                                    <dsp:valueof param="order.email" />
                                </div>
                                <a href="javascript:void(0)" class="tru-link" id="oc-editEmail">
                                    <fmt:message key="checkout.review.edit" />
                                </a>
                            </div>
                            <div>
                                <fmt:message key="checkout.confirm.createPassword" var="createPwdLabel" />
                                <div class="password-tools">
                                    <label for="oc_password" title="oc_password"></label>
                                    <input type="password" name="oc_password" class="passwordInput checkout-order-complete__content-block-field" id="passwordInput" placeholder="${createPwdLabel}" aria-label="oc_password">
                                </div>
                            </div>
                            <div>
                                <fmt:message key="checkout.confirm.confirmPassword" var="confirmPwdLabel" />
                                <input type="password" name="confirm_password" class="confirmPasswordInput checkout-order-complete__content-block-field" id="ConfirmPassword" aria-label="confirmPasswordInput"
                                    placeholder="${confirmPwdLabel}" title="confirmPasswordInput">
                            </div>
                            <div class="button-container">
                                <button id="confirmSignUpSubmitBtn" class="checkout-order-complete__content-block-button">
                                    <fmt:message key="checkout.confirm.signUpSubmitButton" />
                                </button>
                            </div>                            
                        </div>
                    </dsp:form>
                </div>
            </div>
        </c:if>
</dsp:page>