<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
	<dsp:importbean bean="/com/tru/commerce/order/droplet/DisplayOrderDetailsDroplet" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUCheckPayAndShipMethodDroplet" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupDroplet" />
	<dsp:importbean bean="/atg/commerce/order/purchase/PaymentGroupDroplet" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/dynamo/service/CurrentDate" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService" />
	<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale" />
	<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
	<dsp:importbean bean="/com/tru/common/TRUIntegrationConfiguration" />
	<dsp:importbean bean="/com/tru/common/TRUSOSIntegrationConfiguration" />
	<dsp:importbean bean="/com/tru/commerce/cart/droplet/OrderSummaryDetailsDroplet" />
	<dsp:importbean bean="/com/tru/commerce/profile/droplet/TRUUserAlreadyExistsCheckDroplet" />
	<dsp:getvalueof var="orderId" bean="ShoppingCart.last.id" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:getvalueof var="epsilonPixelURL" bean="TRUConfiguration.epsilonPixelURL" />
	<dsp:getvalueof var="isAnonymousUser" bean="Profile.transient" />
	<dsp:getvalueof var="order" bean="ShoppingCart.last" />
	<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="storeEnableHookLogic" bean="TRUIntegrationConfiguration.enableHookLogic" />
	<dsp:getvalueof var="sosEnableHookLogic" bean="TRUSOSIntegrationConfiguration.enableHookLogic" />
	<c:set var="isSosVar" value="${cookie.isSOS.value}" />

	<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />

	<dsp:droplet name="GetSiteTypeDroplet">
		<dsp:param name="siteId" value="${site}" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="siteforsos" param="site" />
			<c:set var="siteforsos" value="${siteforsos}" scope="request" />
		</dsp:oparam>
	</dsp:droplet>

	<%-- TUW-57172 : Start Changes --%>
		<c:choose>
			<c:when test="${isSosVar}">
				<dsp:droplet name="PaymentGroupDroplet">
					<dsp:param name="clear" value="true" />
					<dsp:param name="paymentGroupTypes" value="creditCard" />
					<dsp:oparam name="output" />
				</dsp:droplet>
				<dsp:droplet name="ShippingGroupDroplet">
					<dsp:param name="clearShippingGroups" value="true" />
					<dsp:param name="shippingGroupTypes" value="hardgoodShippingGroup,channelHardgoodShippingGroup" />
				</dsp:droplet>
			</c:when>
			<c:otherwise>
				<dsp:droplet name="PaymentGroupDroplet">
					<dsp:param name="clear" value="false" />
					<dsp:param name="paymentGroupTypes" value="creditCard" />
					<dsp:param name="initPaymentGroups" value="true" />
					<dsp:param name="initBasedOnLastOrder" value="true" />
					<dsp:oparam name="output" />
				</dsp:droplet>
				<dsp:droplet name="ShippingGroupDroplet">
					<dsp:param name="clearShippingGroups" value="true" />
					<dsp:param name="initShippingGroups" value="true" />
					<dsp:param name="initBasedOnLastOrder" value="true" />
					<dsp:param name="shippingGroupTypes" value="hardgoodShippingGroup,channelHardgoodShippingGroup" />
				</dsp:droplet>
			</c:otherwise>
		</c:choose>
		<%-- TUW-57172 : End Changes --%>

			<%--  <script>
	   	 $(document).ready(function(){
			var isSOS = $.cookie("isSOS");
			var sites = '<c:out value="${siteforsos}"/>';
			if(typeof isSOS != 'undefined' && isSOS != null && isSOS == 'true'){
				$("#sosCmsAddZone").hide();
				$(".sosAddZone2").hide();
				$(".sosAddZone3").hide();
			}else{
				$("#sosCmsAddZone").show();
				$(".sosAddZone2").show();
				$(".sosAddZone3").show();
			}
		}); 
	 </script>	 --%>

				<dsp:droplet name="TRUCheckPayAndShipMethodDroplet">
					<dsp:param value="${order}" name="order" />
					<dsp:oparam name="output">
						<dsp:getvalueof var="payInStorePaymentGroup" param="payInStorePaymentGroup" vartype="java.lang.String" />
						<dsp:getvalueof var="storePickUpShipGroup" param="storePickUpShipGroup" vartype="java.lang.String" />
					</dsp:oparam>
				</dsp:droplet>
				<dsp:droplet name="GetSiteTypeDroplet">
					<dsp:param name="siteId" value="${site}" />
					<dsp:oparam name="output">
						<dsp:getvalueof var="site" param="site" />
						<c:set var="site" value="${site}" scope="request" />
					</dsp:oparam>
				</dsp:droplet>
				<c:choose>
					<c:when test="${site eq 'sos'}">
						<c:set var="hookLogicEnabled" value="${sosEnableHookLogic}" scope="request" />
					</c:when>
					<c:otherwise>
						<c:set var="hookLogicEnabled" value="${storeEnableHookLogic}" scope="request" />
					</c:otherwise>
				</c:choose>


				<div class="order-confirmation-page-container" id="nongoodbye_order_confirmation">
					<div class="default-margin">
						<div class="siteLogoForPrintPage hide">
							<span><img src="/images/tru-logo-sm.png" alt="toysrus logo"></span>
							<span> <img src="/images/babiesrus-logo-small.png" alt="babiesrus logo"></span>
						</div>
						<div class="checkout-order-complete">
							<div class="checkout-order-complete-header">
								<div class="row">
									<div class="col-md-12">
										<h1 class="checkout-order-complete-header__title">
											<fmt:message key="checkout.confirm.orderCompleteHeading" />
										</h1>
									</div>
									<div class="col-md-8 checkout-order-complete__subheader">
										<p>
											<fmt:message key="checkout.confirm.orderNumber" /> <span class="checkout-order-complete-header__subheader--bold"><dsp:valueof bean="ShoppingCart.last.id"/></span>.
											<fmt:message key="checkout.confirm.confirmationEmail"/>
										</p>
									</div>

									<dsp:droplet name="IsEmpty">
										<dsp:param name="value" bean="ShoppingCart.last" />
										<dsp:oparam name="false">

											<div class="col-md-4 confirmation-links">
												<div class="pull-right">
													<a href="javascript:void(0);" onclick="window.print();">
														<fmt:message key="checkout.confirm.printReceipt" />
													</a>
													<!-- <span>.</span> <a href="#"><fmt:message key="checkout.confirm.modifyOrder" /></a> -->
												</div>
											</div>
										</dsp:oparam>
									</dsp:droplet>
								</div>
							</div>
							<hr class="tru-horizontal-divider" />
							<span id="sosCmsAddZone">
								<dsp:droplet name="/atg/targeting/TargetingFirst">
									<dsp:param name="targeter"
										bean="/atg/registry/RepositoryTargeters/TRU/Checkout/OrderConfrimationCMSTargeter" />
									<dsp:oparam name="output">
													<div class="row">
														<div class="col-md-12">
													<dsp:valueof param="element.data" valueishtml="true" />
											</div>
													</div>
									</dsp:oparam>
								</dsp:droplet> 
							</span>
							<c:choose>
								<c:when test="${payInStorePaymentGroup eq true}">
									<%-- <dsp:include page="howToPay.jsp"/> --%>
										<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters/TRU/Checkout/HowToPayContentTargeter" />
										<dsp:getvalueof var="cacheKey" value="${atgTargeterpath}${siteId}${locale}" />
										<dsp:droplet name="/com/tru/cache/TRUHowToPayCacheDroplet">
											<dsp:param name="key" value="${cacheKey}" />
											<dsp:oparam name="output">
												<dsp:droplet name="/atg/targeting/TargetingFirst">
													<dsp:param name="targeter" bean="${atgTargeterpath}" />
													<dsp:oparam name="output">
														<dsp:valueof param="element.data" valueishtml="true" />
													</dsp:oparam>
												</dsp:droplet>
											</dsp:oparam>
										</dsp:droplet>
								</c:when>
								<c:when test="${storePickUpShipGroup eq true}">
									<%-- <dsp:include page="storePickUpInstructions.jsp"/> --%>
										<dsp:droplet name="/atg/targeting/TargetingFirst">
											<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/Checkout/StorePickUpInstructionsTargeter" />
											<dsp:oparam name="output">
												<dsp:valueof param="element.data" valueishtml="true" />
											</dsp:oparam>
										</dsp:droplet>
								</c:when>
							</c:choose>
							<span class="sosAddZone2">
				   <dsp:include page="adZone2.jsp"></dsp:include>
				 	  </span>
							<p class="global-error-display"></p>
							<div class="row order-complete-columns">
								<c:if test="${isAnonymousUser eq true}">
									<dsp:droplet name="TRUUserAlreadyExistsCheckDroplet">
										<dsp:param value="${order.email}" name="email" />
										<dsp:oparam name="output">
											<dsp:getvalueof param="isUserAlreadyExists" var="isUserAlreadyExists" vartype="java.lang.boolean" />
											<c:if test="${isUserAlreadyExists eq false}">
												<dsp:include page="confirmSignUp.jsp"></dsp:include>
											</c:if>
										</dsp:oparam>
									</dsp:droplet>
								</c:if>
								<c:choose>
									<c:when test="${isAnonymousUser eq true}">
										<dsp:include page="rewardsSignUp.jsp" />
									</c:when>
									<c:otherwise>
										<dsp:include page="rewardsSignUpLoggedIn.jsp" />
									</c:otherwise>
								</c:choose>
								<c:if test="${!isSosVar || !(site eq 'sos')}">
									<div id="email-alerts" class="col-md-4 email-alerts">
										<div class="create-account-thank-you">
											<h2>
												<fmt:message key="checkout.confirm.signUpThanksHeading" />
											</h2>
											<h5>
												<fmt:message key="checkout.confirm.signUpThanksMessage" />
											</h5>
											<button onclick="return redirectToMyAccount();"><fmt:message key="checkout.confirm.goToMyAccountButton" /></button>
											<p>
												<fmt:message key="checkout.confirm.benefits" />
											</p>
											<ul>
												<li>
													<fmt:message key="checkout.confirm.benefit1" />
												</li>
												<li>
													<fmt:message key="checkout.confirm.benefit2" />
												</li>
												<li>
													<fmt:message key="checkout.confirm.benefit3" />
												</li>
												<li>
													<fmt:message key="checkout.confirm.benefit4" />
												</li>
												<li>
													<fmt:message key="checkout.confirm.benefit5" />
												</li>
											</ul>
										</div>
										<div class="bordered-column checkout-order-complete__content-block">
											<div class="email-signup-thank-you">
												<h2>
													<fmt:message key="checkout.confirm.signUpThanksHeading" />
												</h2>
												<h5>
													<fmt:message key="checkout.confirm.signUpThanksMessage1" /><br>
													<fmt:message key="checkout.confirm.signUpThanksMessage2" /><br>
													<fmt:message key="checkout.confirm.signUpThanksMessage3" />
												</h5>
											</div>
											<div class="email-signup-container">
												<dsp:include page="emailSignUp.jsp"></dsp:include>
											</div>
										</div>
									</div>
								</c:if>
							</div>
							<span class="sosAddZone3">
				   <dsp:include page="adZone3.jsp"></dsp:include>
				   </span>
						</div>
					</div>
					<hr class="tru-horizontal-divider tru-horizontal-divider--no-bottom-margin">
					<div class="default-margin default-margin--zero-right">
						<div class="row order-complete-details">
							<div class="row checkout-sticky-top checkout-confirmation-content" id="orderConfirmationPage">
								<div class="col-md-8 order-confirmation-left-column">
									<dsp:droplet name="DisplayOrderDetailsDroplet">
										<dsp:param name="order" bean="ShoppingCart.last" />
										<dsp:param name="profile" bean="Profile" />
										<dsp:param name="page" value="Confirm" />
										<dsp:oparam name="output">
											<%--Start :MVP 8  --%>
												<%-- <dsp:include page="/cart/shoppingCartInfo.jsp">
									<dsp:param name="items" param="outOfStockItemsList" />
									<dsp:param name="removedInfoMessage" param="removedInfoMessage" />
									<dsp:param name="qtyAdjustedInfoMessage" param="qtyAdjustedInfoMessage" />
									<dsp:param name="pageName" value="Confirm" />
								</dsp:include> --%>
													<%--End :MVP 8  --%>
														<dsp:param name="result" bean="ShoppingCart.last"></dsp:param>
														<dsp:include page="/checkout/common/paymentSummary.jsp">
															<dsp:param name="paymentGroups" param="result.paymentGroups" />
															<dsp:param name="pageName" value="Confirm" />
														</dsp:include>
														<br>

														<dsp:droplet name="ForEach">
															<dsp:param name="array" param="shippingDestinationsVO" />
															<dsp:param name="elementName" value="shippingDestinationVO" />
															<dsp:param name="sortProperties" value="-shippingGroupType" />
															<dsp:oparam name="output">
																<%-- Added for tealium --%>
																	<dsp:getvalueof var="shippingCount" param="count" scope="request" />
																	<%-- Added for tealium --%>
																		<dsp:getvalueof var="shippingGroupType" param="shippingDestinationVO.shippingGroupType" />

																		<c:choose>
																			<c:when test="${(shippingGroupType eq 'hardgoodShippingGroup') or (shippingGroupType eq 'channelHardgoodShippingGroup')}">
																				<dsp:getvalueof var="nickName" param="key" />
																				<div class="checkout-confirmation-items-shipping-to">
																					<dsp:include page="/checkout/common/displayShippingAddress.jsp">
																						<dsp:param name="shippingDestinationVO" param="shippingDestinationVO" />
																						<dsp:param name="pageName" value="Confirm" />
																					</dsp:include>
																					<dsp:droplet name="ForEach">
																						<dsp:param name="array" param="shippingDestinationVO.shipmentVOMap" />
																						<dsp:param name="elementName" value="shipmentVO" />
																						<dsp:oparam name="outputStart">
																							<%-- <dsp:droplet name="IsEmpty">
														   		<dsp:param name="value" param="shipmentVO.shippingGroup.giftWrapMessage"/>
														   		<dsp:oparam name="false">
																	<div class="gift-information">
																		<span><fmt:message
																				key="checkout.review.giftMessageHeading" /></span><br>
																		<div class="gift-message">"<dsp:valueof param="shipmentVO.shippingGroup.giftWrapMessage" valueishtml="true" />"</div>
																	</div>
														   		</dsp:oparam>
														   	</dsp:droplet> --%>
																						</dsp:oparam>
																						<dsp:oparam name="output">
																							<dsp:include page="/checkout/common/shipmentDetails.jsp">
																								<dsp:param name="size" param="size" />
																								<dsp:param name="count" param="count" />
																								<dsp:param name="nickName" value="${nickName}" />
																								<dsp:param name="pageName" value="Confirm" />
																								<dsp:param name="shipmentVO" param="shipmentVO" />
																							</dsp:include>
																							<hr>
																							<dsp:include page="/checkout/common/displayItemDetails.jsp">
																								<dsp:param name="shipmentVO" param="shipmentVO" />
																								<dsp:param name="pageName" value="Confirm" />
																								<dsp:param name="shippingGroupType" value="${shippingGroupType}" />
																							</dsp:include>
																						</dsp:oparam>
																					</dsp:droplet>
																				</div>
																			</c:when>
																			<c:when test="${(shippingGroupType eq 'inStorePickupShippingGroup') or (shippingGroupType eq 'channelInStorePickupShippingGroup')}">
																				<dsp:droplet name="ForEach">
																					<dsp:param name="array" param="shippingDestinationVO.shipmentVOMap" />
																					<dsp:param name="elementName" value="shipmentVO" />
																					<dsp:oparam name="output">
																						<div class="checkout-review-items-free-pickup">
																							<dsp:include page="/checkout/common/displayFreePickupDetails.jsp">
																								<dsp:param name="inStorePickupShippingGroup" param="shipmentVO.shippingGroup" />
																							</dsp:include>
																						</div>
																						<br>
																						<dsp:include page="/checkout/common/displayItemDetails.jsp">
																							<dsp:param name="shipmentVO" param="shipmentVO" />
																							<dsp:param name="pageName" value="Confirm" />
																							<dsp:param name="shippingGroupType" value="${shippingGroupType}" />
																						</dsp:include>
																					</dsp:oparam>
																				</dsp:droplet>
																			</c:when>
																		</c:choose>
															</dsp:oparam>
														</dsp:droplet>
														<dsp:droplet name="IsEmpty">
															<dsp:param name="value" param="donationItemsList" />
															<dsp:oparam name="false">
																<%-- display the donation header here --%>
																	<div class="checkout-review-digital-items donation">
																		<h2>
																			<fmt:message key="checkout.review.donationHeading" />
																		</h2>
																	</div>
																	<br>
																	<dsp:droplet name="ForEach">
																		<dsp:param name="array" param="donationItemsList" />
																		<dsp:param name="elementName" value="donationItem" />
																		<dsp:oparam name="output">
																			<dsp:include page="/checkout/common/displayDonationItemDetail.jsp">
																				<dsp:param name="donationItem" param="donationItem" />
																				<dsp:param name="pageName" value="Confirm" />
																			</dsp:include>
																		</dsp:oparam>
																	</dsp:droplet>
															</dsp:oparam>
														</dsp:droplet>
														<%--Start :MVP 8  --%>
															<%-- <dsp:droplet name="IsEmpty">
									<dsp:param name="value" param="outOfStockItemsList" />
									<dsp:oparam name="false">
										display the out-of-stock items here
										<div class="checkout-review-digital-items donation">
						                    <h2><fmt:message key="checkout.out.of.stock.items" /></h2>
						                </div>
						                <br>
											<dsp:droplet name="ForEach">
												<dsp:param name="array" param="outOfStockItemsList" />
												<dsp:param name="elementName" value="outOfStockItem" />
												<dsp:oparam name="output">
													<dsp:include page="/cart/outOfStockItemDetails.jsp">
														<dsp:param name="item" param="outOfStockItem" />
														<dsp:param name="index" param="index" />
													</dsp:include>
												</dsp:oparam>
											</dsp:droplet>
									</dsp:oparam>
								</dsp:droplet> --%>
																<%--Start :MVP 8  --%>
																	<c:set var="hookLogicEnabled" value="true" />
																	<c:if test="${hookLogicEnabled eq true}">
																		<dsp:include page="/browse/hooklogicOrder.jsp">
																			<dsp:param name="hookLogicDetails" param="hookLogicDetails" />
																		</dsp:include>
																	</c:if>
										</dsp:oparam>
									</dsp:droplet>
									<%-- <dsp:include page="/checkout/common/eGiftCards.jsp"><dsp:param name="pageName" value="Confirm"/></dsp:include> --%>
								</div>
								<div class="col-md-3 checkout-order-summary order-confirmation-right-column">
									<dsp:include page="/checkout/common/checkoutOrderSummary.jsp">
										<dsp:param name="pageName" value="Confirm" /></dsp:include>
								</div>
							</div>
						</div>
					</div>
				</div>
				<dsp:include page="tealiumForOrder.jsp" />
				<div class="modal fade in" id="printTemplate" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false">
					<%-- <dsp:include page="printReceipt.jsp">
			<dsp:param name="pageName" value="Confirm" />
			</dsp:include> --%>
				</div>

				<dsp:getvalueof var="currentdate" bean="CurrentDate.timeAsTimeStamp" />
				<div class="modal fade pdpModals" id="checkoutFeesPopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
					<div class="modal-dialog modal-sm">
						<div class="modal-content sharp-border">
							<div class="pdp-modal1">
								<div class="close-btn">
									<span class="sprite-icon-x-close" data-dismiss="modal"></span>
								</div>
								<header>
									<p>
										<fmt:message key="order.summary.ewaste.fees.text" />
									</p>
								</header>
							</div>
						</div>
					</div>
				</div>
				<img src="${epsilonPixelURL}?hEventName=Purchase&hAmount=<fmt:formatNumber type="number" maxFractionDigits="2" value="${epsilonOrderAmount}
				 "/>&hQuantity=${epsilonOrderQuantity}&hCustomerTransactionId=${orderId}&hDateTime=${currentdate}" height="1" width="1" alt="" />
</dsp:page>