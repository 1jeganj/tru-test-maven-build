<dsp:page>
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
    <dsp:importbean bean="/atg/commerce/ShoppingCart"/>
    <dsp:importbean bean="/com/tru/commerce/order/droplet/DisplayOrderDetailsDroplet" />
    <dsp:importbean bean="/com/tru/commerce/order/droplet/PaymentGroupTypeDroplet" />
    <dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
    
	<dsp:getvalueof var="loginStatus" bean="Profile.transient" />
	<dsp:getvalueof var="customerID" bean="Profile.id"/>
	<dsp:getvalueof var="customerDOB" bean="Profile.dateOfBirth"/>
	<dsp:getvalueof param="pageName" var="tPageName" />
	<dsp:getvalueof param="pageType" var="tPageType" />
	<dsp:getvalueof param="page" var="page" />
    <dsp:getvalueof param="page" var="page" />
	<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>
	<dsp:getvalueof var="order" bean="ShoppingCart.last" />
	<dsp:getvalueof var="priceInfo" value="${order.priceInfo}" />
	<dsp:getvalueof var="orderTotal" value="${order.priceInfo.total}" vartype="long"/>
	<dsp:getvalueof var="orderInStorePickUp" value="${order.orderIsInStorePickUp}" />
	<dsp:getvalueof var="customerEmail" value="${order.email}"/>
    <dsp:getvalueof var="customerFirstName" bean="Profile.firstName"/>
	<dsp:getvalueof var="customerLastName" bean="Profile.lastName"/>
    <dsp:getvalueof var="defaultPromoCode" value="Promo Description Not Provided"/>
   <c:forEach var="commerceItem" items="${order.commerceItems}">
   
    <c:forEach var="adjustment" items="${commerceItem.priceInfo.adjustments}">
    
    <c:set var="itemCoupons" value="${adjustment.coupon}"/> 
    
    <c:if test="${not empty  itemCoupons}">
    <c:set var="itemPromotions" value="${adjustment.coupon.promotions}"/>
     <c:forEach var="itemPromotion" items="${itemPromotions}">
     <%-- <c:if test="${fn:containsIgnoreCase(itemPromotion,'Item Discount')}">
     
    <c:set var="orderCouponType" value="${orderCouponType}'Item Level',"/>
     </c:if> --%>
     </c:forEach>
    </c:if>
    </c:forEach>
   
   </c:forEach>
   
   <c:choose>
	<c:when test="${orderInStorePickUp eq true}">
	<dsp:getvalueof var="isInStorePickUp" value="Y"/>
	</c:when>
	<c:otherwise>
	<dsp:getvalueof var="isInStorePickUp" value="N"/>
	</c:otherwise>
	</c:choose>
   
	<c:set var="space" value=" "/>
	<c:set var="spaceTrimer" value=""/>
	<c:choose>
	<c:when test="${empty customerFirstName && empty customerLastName}">
		<c:set var="customerName" value='${fn:substring(customerEmail, 0, fn:indexOf(customerEmail, "@"))}'/>
	</c:when>
	<c:when test="${not empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value="${customerFirstName}"/>
	</c:when>
	<c:when test="${empty customerFirstName && not empty customerLastName}">
		   <c:set var="customerName" value="${customerLastName}"/>
	</c:when>
	<c:otherwise>
			<c:set var="customerName" value="${customerFirstName}${space}${customerLastName}"/>
	</c:otherwise>
    </c:choose>
	<c:set var="singleQuote" value="'"/>
	<c:set var="doubleQuote" value='"'/>
	<c:set var="slashSingleQuote" value="\'"/>
	<c:set var="slashDoubleQuote" value='\"'/>
	<c:set var="comma" value=","/>
	<c:set var="andSymbol" value="&"/>
	<c:set var="hyphen" value="-"/>

<%--START:  Setting the dynamic values	from order --%>
    <c:set var="orderCurrency">${priceInfo.currencyCode} </c:set>
	<c:set var="orderSubtotal">${priceInfo.rawSubtotal} </c:set>
	<c:choose>
    <c:when test="${not empty orderSubtotal}">
    <c:set var="orderSubtotal"><c:if test="${orderSubtotal.matches('[0-9]+.[0-9]+')}"><fmt:formatNumber type="number" maxFractionDigits="2">${orderSubtotal}</fmt:formatNumber></c:if></c:set>
	</c:when>
	<c:otherwise>
	<c:set var="orderSubtotal" value="0.00"/>
	</c:otherwise>
	</c:choose>

    <c:forEach var="coupon" items="${order.singleUseCoupon}">
	<c:set var="couponCode" value="${couponCode}${coupon.value},"/>
	</c:forEach>
	 
	<c:set var="orderShipping">${priceInfo.shipping}</c:set>
	<c:set var="orderShippingSurcharge">${priceInfo.totalShippingSurcharge}</c:set>
	<c:set var="orderGiftWrap">${priceInfo.giftWrapPrice}</c:set>
	<c:set var="orderID">${order.id}</c:set>
	<c:set var="orderTax">${order.taxPriceInfo.amount}</c:set>
    <%--END:  Setting the dynamic values from order	 --%>
    <c:set var="tealiumOrderDiscount" value="0.00"/>
   
	<c:forEach var="element" items="${priceInfo.adjustments}">
	
	<c:if test="${element.adjustmentDescription eq 'Order Discount' }">
	<c:if test="${not empty element.pricingModel.displayname}">
	<%-- <c:set var="orderCouponType" value="${orderCouponType}'Order Level',"/> --%>
	<%-- <c:set var="promotionName" value="${promotionName}${element.pricingModel.displayname},"/> --%>
	</c:if>
	<c:set var="orderDiscount" value="${element.totalAdjustment}"/>
	<c:set var="tealiumOrderDiscount" value="${tealiumOrderDiscount+(-orderDiscount)}"/>
	</c:if>
	</c:forEach>
	<c:set var="orderShippingDiscount" value="0.00"/>
    <c:set var="tealiumIndex" value="0"/>
	<dsp:droplet name="DisplayOrderDetailsDroplet">
						<dsp:param name="order" bean="ShoppingCart.last" />
						<dsp:param name="profile" bean="Profile" />
						<dsp:param name="page" value="Confirm" />
						<dsp:oparam name="output">
							<dsp:droplet name="ForEach">
								<dsp:param name="array" param="shippingDestinationsVO" />
								<dsp:param name="elementName" value="shippingDestinationVO" />

								<dsp:oparam name="output">
									<dsp:getvalueof var="nickName" param="key" />
								<dsp:getvalueof param="shippingDestinationVO.itemsCount" var="itemCount"/>
								<%-- <dsp:getvalueof param="shippingDestinationVO.registryAddress" var="registryAddress"/>
								<dsp:getvalueof param="shippingDestinationVO.shippingGroupType" var="shippingGroupType"/> --%>

									<c:set var="orderShippingCountry">USA</c:set>
										<dsp:droplet name="ForEach">
											<dsp:param name="array"	param="shippingDestinationVO.shipmentVOMap" />
											<dsp:param name="elementName" value="shipmentVO" />
											<dsp:oparam name="output">
											<dsp:getvalueof var="selectedShippingMethod" param="shipmentVO.shippingGroup.shippingMethod" />
                                              <dsp:getvalueof param="shipmentVO.shippingGroup" var="shippingGroup"/>
												<c:if test="${shippingGroup ne null}">
												
												<c:set var="shippingGroupType" value="${shippingGroup.shippingGroupClassType}"/>
												<c:set var="shippingAdjustments" value="${shippingGroup.priceInfo.adjustments}"/>
												<c:forEach var="shippingAdjustment" items="${shippingAdjustments}">
												<c:set var="appliedCoupons" value="${shippingAdjustment.coupon}"/>
												<c:if test="${not empty appliedCoupons}">
												<c:forEach var="shippingPromo" items="${shippingAdjustment.coupon.promotions}">
												
												<%-- <c:if test="${fn:containsIgnoreCase(shippingPromo,'Shipping Discount')}">
												    <c:set var="orderCouponType" value="${orderCouponType}'Shipping Level',"/>
												 </c:if> --%>
												</c:forEach>
												</c:if>
												</c:forEach>
												</c:if>


											<%-- Added for item shipment method --%>
											<dsp:droplet name="ForEach">
												<dsp:param name="array" param="shipmentVO.shipMethodVOs" />
												<dsp:param name="elementName" value="shipMethodVO" />
												<dsp:oparam name="output">

													<dsp:getvalueof var="shipMethodName" param="shipMethodVO.shipMethodName" />
													<dsp:getvalueof var="shipMethodCode" param="shipMethodVO.shipMethodCode" />
													<dsp:getvalueof var="shippingPrice" param="shipMethodVO.shippingPrice"/>
													<dsp:getvalueof param="shipMethodVO.shipMethodName" var="tealiumDefaultShipType"/>
								                    <dsp:getvalueof param="shipMethodVO.delivertTimeLine" var="tealiumDeliveryTimeLine"/>
								                    
													<c:if test="${selectedShippingMethod eq shipMethodCode}">
													 <c:choose>
			                                		<c:when test="${not empty shippingMethod}">
			                                			<c:set var="shippingMethod">${shippingMethod},'${shipMethodName}${space}${tealiumDeliveryTimeLine}'</c:set>
			                                		</c:when>
			                                		<c:otherwise>
			                                			<c:set var="shippingMethod">'${shipMethodName}${space}${tealiumDeliveryTimeLine}'</c:set>
			                                		</c:otherwise>
			                                		</c:choose>

												    <dsp:getvalueof var="shippingCost" value="${order.priceInfo.shipping}"/>
												   <c:choose>
			                                		<c:when test="${shippingCost == 0.0}">
			                                			<c:set var="tealiumDefaultShippingCost" value="Free"/>
			                                		</c:when>
			                                		<c:otherwise>
			                                			<c:set var="tealiumDefaultShippingCost" value="($ ${shippingPrice})"/>
			                                		</c:otherwise>
			                                		</c:choose>
			                                		 <c:choose>
			                                		<c:when test="${not empty shipping}">
												    <c:set var="shipping">${shipping},'${shipMethodName}(${tealiumDeliveryTimeLine})-${tealiumDefaultShippingCost}'</c:set>
												     </c:when>
												     <c:otherwise>
												     <c:set var="shipping">'${shipMethodName}(${tealiumDeliveryTimeLine})-${tealiumDefaultShippingCost}'</c:set>
												     </c:otherwise>
												     </c:choose>
												     </c:if>

												</dsp:oparam>

											</dsp:droplet>
											<%-- Added for item shipment method --%>

											<%-- Added for item details --%>
											<dsp:getvalueof param="shipmentVO.cartItemDetailVOs" var="items"/>
											<c:forEach var="element" items="${items}">
											<c:set var="channel" value="${element.channelType}"/>
											<c:if test="${channel eq 'wishlist'}">
											<c:set var="wishListId" value="${element.channelId}"/>
											</c:if>
											<c:if test="${channel eq 'registry'}">
											<c:set var="registryId" value="${element.channelId}"/>
											</c:if>
											<c:if test="${shippingGroupType eq 'hardgoodShippingGroup'}">
												<c:set var="orderType" value="${orderType}'Ship To Home',"/>
												<c:set var="orderFulFilChannel" value="${orderFulFilChannel}'Ship To Home',"/>
										    </c:if>
											<c:if test="${shippingGroupType eq 'inStorePickupShippingGroup'}">
											<c:set var="shipFromStoreEligible" value="${element.shipFromStoreEligible}"/>
											<c:set var="itemInStorePickUp" value="${element.itemInStorePickUp}"/>
											<c:set var="inStorePickUpAvailable" value="${element.inStorePickUpAvailable}"/>
											<c:set var="ispuRevenue" value="${element.totalAmount+ispuRevenue}"/>
											
											<c:choose>
											<c:when test="${shipFromStoreEligible eq 'Y' && itemInStorePickUp eq 'N'}">
												<c:set var="orderType" value="${orderType}'STS',"/>
                                                 <c:set var="orderFulFilChannel" value="${orderFulFilChannel}'Ship To Store',"/>
											</c:when>
											<c:otherwise>
												<c:set var="orderType" value="${orderType}'ISPU',"/>
                                            	<c:set var="orderFulFilChannel" value="${orderFulFilChannel}'In Store Pick Up',"/>
											</c:otherwise>
											</c:choose>
											
											</c:if>
											
											  <c:set var="separator" value=","/>
											
											<dsp:droplet name="/com/tru/commerce/droplet/TRUProductInfoLookupDroplet">
													<dsp:param name="productId" value="${element.productId}"/>
													<dsp:param name="siteId" bean="/atg/multisite/Site.id"/>
													<dsp:oparam name="output">
											         <dsp:getvalueof param="productInfo.defaultSKU.brandName" var="brandName"/>
											          <dsp:getvalueof param="productInfo.defaultSKU.collectionNames" var="collectionNames"/>
											          <dsp:getvalueof param="productInfo.defaultSKU.relatedProducts" var="relatedProducts"/>
											          <c:set var="orderXsellProduct" value="${orderXsellProduct}'${relatedProducts}'${separator}"/>
											          <c:forEach var="collections" items="${collectionNames}">
		                                              <c:set var="collectionName" value="${collections.value}"/>
		                                              </c:forEach>
		                                              <c:if test="${fn:contains(brandName,singleQuote)}">
												     <c:set var="brandName" value='${fn:replace(brandName,singleQuote,slashSingleQuote)}'/>
													 </c:if>
													 <c:if test="${fn:contains(brandName,doubleQuote)}">
												     <c:set var="brandName" value='${fn:replace(brandName,doubleQuote,slashDoubleQuote)}'/>
													 </c:if>
											         <c:set var="productBrands" value="${productBrands}'${brandName}'${separator}"/>
											       </dsp:oparam>
											   </dsp:droplet>
											   
											   <c:set var="productDisplayName" value="${element.displayName}"/>
												     
												     <c:if test="${fn:contains(productDisplayName,singleQuote)}">
												     <c:set var="productDisplayName" value='${fn:replace(productDisplayName,singleQuote,slashSingleQuote)}'/>
													 </c:if>
													 <c:if test="${fn:contains(productDisplayName,doubleQuote)}">
												     <c:set var="productDisplayName" value='${fn:replace(productDisplayName,doubleQuote,slashDoubleQuote)}'/>
													 </c:if>
													 <c:set var="productName" value="${productName}'${productDisplayName}'${separator}"/>
													 
											 
													
											 <dsp:droplet name="/com/tru/commerce/droplet/TRUProductCategoryInfoDroplet">
											 	<dsp:param name="productId" value="${element.productId}" />
											 	<dsp:param name="selectedSkuId" value="${element.skuId}" />	
												<dsp:oparam name="output">
													<dsp:getvalueof param="onlinePidSku" var="onlinePID"/>
												</dsp:oparam>
											 </dsp:droplet>

								<dsp:droplet name="/com/tru/droplet/TRUCategoryPrimaryPathDroplet">
									<dsp:param name="skuId" value="${element.skuId}" />
									<dsp:oparam name="output">
										<dsp:getvalueof var="primaryPathCategoryVOList"
											param="primaryPathCategoryVOList" />
									</dsp:oparam>
								</dsp:droplet>

								<dsp:getvalueof var="productCatName"
									value="${primaryPathCategoryVOList[0].categoryName}" />
								<dsp:getvalueof var="categoryName"
									value="${primaryPathCategoryVOList[0].categoryName}" />
								<dsp:getvalueof var="productSubcategoryName"
													value="" />
								<dsp:droplet name="ForEach">
									<dsp:param name="array" value="${primaryPathCategoryVOList}" />
									<dsp:param name="elementName" value="categoryList" />
									<dsp:oparam name="output">
										<dsp:getvalueof var="count" param="count" />
										<dsp:getvalueof var="size" param="size" />
										<c:choose>
											<c:when test="${count eq 1}">
											</c:when>
											<c:when test="${count eq size}">
											</c:when>
											<c:otherwise>
												<dsp:getvalueof var="subCategories"	param="categoryList.categoryName" />
												<c:choose>
													<c:when test="${count eq size-1}">
														<dsp:getvalueof var="subCategories" value="'${subCategories}'" />
													</c:when>
													<c:otherwise>
														<dsp:getvalueof var="subCategories" value="'${subCategories}'," />
													</c:otherwise>
												</c:choose>
												<dsp:getvalueof var="productSubcategoryName" value="${productSubcategoryName}${subCategories}" />
												<dsp:getvalueof var="pageSubCategories"	value="${subCategories}" />
											</c:otherwise>
										</c:choose>
										<dsp:getvalueof var="" param="categoryList.categoryName" />
									</dsp:oparam>
								</dsp:droplet>

								<c:if test="${fn:contains(productCatName,hiphen)}">
												     <c:set var="productCatName" value='${fn:replace(productCatName,hyphen,spaceTrimer)}'/>
													 </c:if>
													 <c:if test="${fn:contains(productCatName,singleQuote)}">
												     <c:set var="productCatName" value='${fn:replace(productCatName,singleQuote,slashSingleQuote)}'/>
													 </c:if>
													 <c:if test="${fn:contains(productCatName,doubleQuote)}">
												     <c:set var="productCatName" value='${fn:replace(productCatName,doubleQuote,spaceTrimer)}'/>
													 </c:if>

<%-- 													 <c:if test="${fn:contains(productCatName,space)}"> --%>
<%-- 												     <c:set var="productCatName" value='${fn:replace(productCatName,space,spaceTrimer)}'/> --%>
<%-- 													 </c:if> --%>
													 <c:if test="${fn:contains(productCatName,comma)}">
												     <c:set var="productCatName" value='${fn:replace(productCatName,comma,spaceTrimer)}'/>
													 </c:if>
													 <c:if test="${fn:contains(productCatName,andSymbol)}">
												     <c:set var="productCatName" value='${fn:replace(productCatName,andSymbol,spaceTrimer)}'/>
													 </c:if>
													 <c:set var="taxproductCatName" value="${productCatName}"/> 
													
													<c:if test="${fn:contains(productSubcategoryName,hiphen)}">
												     <c:set var="productSubcategoryName" value='${fn:replace(productSubcategoryName,hyphen,spaceTrimer)}'/>
													 </c:if>
													 
												     <c:if test="${fn:contains(productSubcategoryName,singleQuote)}">
												     <c:set var="productSubcategoryName" value='${fn:replace(productSubcategoryName,singleQuote,slashSingleQuote)}'/>
													 </c:if>
													 <c:if test="${fn:contains(productSubcategoryName,doubleQuote)}">
												     <c:set var="productSubcategoryName" value='${fn:replace(productSubcategoryName,doubleQuote,spaceTrimer)}'/>
													 </c:if>
<%-- 													 <c:if test="${fn:contains(productSubcategoryName,space)}"> --%>
<%-- 												     <c:set var="productSubcategoryName" value='${fn:replace(productSubcategoryName,space,spaceTrimer)}'/> --%>
<%-- 													 </c:if> --%>
													<%--  <c:if test="${fn:contains(productSubcategoryName,comma)}">
												     <c:set var="productSubcategoryName" value='${fn:replace(productSubcategoryName,comma,spaceTrimer)}'/>
													 </c:if> --%>
													<c:if test="${fn:contains(productSubcategoryName,andSymbol)}">
												     <c:set var="productSubcategoryName" value='${fn:replace(productSubcategoryName,andSymbol,spaceTrimer)}'/>
													 </c:if>
													 <c:set var="taxproductSubCatName" value="${productSubcategoryName}"/>
													
													
									             <c:if test="${not empty collectionName && not empty onlinePID}">
									         		<c:set var="productCollections" value="${productCollections}'${onlinePID}: ${collectionName}'${separator}"/>
										         </c:if>
										         <c:set var="productCatTaxonomy" value="${productCatTaxonomy}&productcat=${taxproductCatName}"/>
											    <c:set var="productSubCatTaxonomy" value="${productSubCatTaxonomy}&productsub=${taxproductSubCatName}"/>
											    <c:set var="productCategory" value="${productCategory}'${productCatName}'${separator}"/>
											    <c:set var="productSubcategory" value="${productSubcategory}'${productSubcategoryName}'${separator}"/>
												<c:set var="productID" value="${productID}'${onlinePID}'${separator}"/>
												<c:set var="productImage" value="${productImage}'${element.productImage}'${separator}"/>
												<c:set var="productDiscount" value="${productDiscount}'${element.savedAmount}'${separator}"/>
												<c:set var="productUnitPrice" value="${productUnitPrice}'${element.salePrice}'${separator}"/>
												<c:set var="productListPrice" value="${productListPrice}'${element.price}'${separator}"/>
												<c:set var="itemQuantity" value="${itemQuantity}'${element.quantity}'${separator}"/>
												<c:set var="productSKUs" value="${productSKUs}'${element.skuId}'${separator}"/>
												
												<c:set var="siteId" value="${element.siteId}"/>
												<c:choose>
												<c:when test="${siteId eq 'ToysRUs'}">
												 <c:set var="partnerName" value="${partnerName}'TRU'${separator}"/>
												</c:when>
												<c:otherwise>
												<c:set var="partnerName" value="${partnerName}'BRU'${separator}"/>
												</c:otherwise>
												</c:choose>
												<c:set var="tealiumIndex" value="${tealiumIndex+1}"/>
											</c:forEach>

											<%-- Added for item details --%>

											</dsp:oparam>
										</dsp:droplet>

								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
</dsp:droplet>
<c:choose>
<c:when test="${orderTotal > '0.00' && orderTotal < '49.00'}">

<c:set var="ordervalue" value="0to49"/>
</c:when>
<c:when test="${orderTotal gt '50.0' && orderTotal lt '99.99'}">
<c:set var="ordervalue" value="50to99"/>
</c:when>
<c:when test="${orderTotal gt '100.0' && orderTotal lt '174.99'}">
<c:set var="ordervalue" value="100to174"/>
</c:when>
<c:when test="${orderTotal gt '175.0' && orderTotal lt '249.99'}">
<c:set var="ordervalue" value="175to249"/>
</c:when>
<c:when test="${orderTotal gt '250.0' && orderTotal lt '349.99'}">
<c:set var="ordervalue" value="250to349"/>
</c:when>
<c:when test="${orderTotal gt '350.0' && orderTotal lt '499.99'}">
<c:set var="ordervalue" value="350to499"/>
</c:when>
<c:when test="${orderTotal gt '500.0' && orderTotal lt '699.0'}">
<c:set var="ordervalue" value="500to699"/>
</c:when>
<c:when test="${orderTotal gt '700.0' && orderTotal lt '899.0'}">
<c:set var="ordervalue" value="700to899"/>
</c:when>
<c:when test="${orderTotal gt '900.0' && orderTotal lt '1099.0'}">
<c:set var="ordervalue" value="900to1099"/>
</c:when>
<c:when test="${orderTotal gt '1100.0'}">
<c:set var="ordervalue" value="1100greater"/>
</c:when>
</c:choose>
<c:set var="orderValueTaxonomy" value="ordervalue=${ordervalue}"/>
<c:set var="tPageName" value="Checkout: OrderConfirmation"/>
<c:set var="tPageType" value="order"/>
<c:choose>
<c:when test="${loginStatus eq 'true'}">
<c:set var="order" value="${order}"/>
<c:set var="customerStatus" value="Guest"/>
</c:when>
<c:otherwise>
<c:set var="customerStatus" value="Registered"/>
</c:otherwise>
</c:choose>

<c:set var="isSosVar" value="${cookie.isSOS.value}"/>

<c:choose>
<c:when test="${isSosVar eq 'true'}">
<c:set var="selectedStore" value="sos"/>
</c:when>
<c:otherwise>
<c:set var="selectedStore" value=""/>
</c:otherwise>
</c:choose>

 <c:set var="creditCardType" value="null"/>
     <c:set var="index" value="0"/>
     <dsp:getvalueof var="paymentGroups" value="${order.paymentGroups}"/>
	 <c:forEach var="paymentGroup" items="${order.paymentGroups}">
	 <c:set var="paymentGroupType" value="${paymentGroup.paymentGroupClassType}"/>
	   <c:set var="taxSelectedPaymentGroupType" value="&paymethod=${taxSelectedPaymentGroupType}${paymentGroup.paymentMethod}"/>
	   <c:set var="tealiumPaymentMethod" value="${paymentGroup.paymentMethod}"/>
	   
		 <c:set var="separator" value=","/>
	   
	  <c:choose>
	   <c:when test="${tealiumPaymentMethod eq 'creditCard'}">
	   <c:set var="creditCardType" value="${paymentGroup.creditCardType}"/>
	   <dsp:getvalueof var="cardType" bean="TRUTealiumConfiguration.displayCreditCardTypeName.${creditCardType}"/>
	   <c:set var="selectedPaymentGroupType" value="${selectedPaymentGroupType}${cardType}${separator}"/>
	   </c:when>
	  <c:otherwise>
		<c:choose>
		<c:when test="${paymentGroup.paymentMethod eq 'giftCard'}">
				<dsp:getvalueof var="paymentGroupTypeMethod" value="${paymentGroup.paymentMethod}"/>
				<dsp:getvalueof var="giftCardMethod" bean="TRUTealiumConfiguration.displayCreditCardTypeName.${paymentGroupTypeMethod}"/>
				<c:set var="selectedPaymentGroupType" value="${selectedPaymentGroupType}${giftCardMethod}${separator}" />
			</c:when>
			<c:when test="${paymentGroup.paymentMethod eq 'inStorePayment'}">
				<dsp:getvalueof var="paymentGroupTypeMethod" value="${paymentGroup.paymentMethod}"/>
				<dsp:getvalueof var="payInStoreMethod" bean="TRUTealiumConfiguration.displayCreditCardTypeName.${paymentGroupTypeMethod}"/>
				<c:set var="selectedPaymentGroupType" value="${selectedPaymentGroupType}${payInStoreMethod}${separator}" />
			</c:when>
			<c:otherwise>
				<c:set var="selectedPaymentGroupType" value="${selectedPaymentGroupType}${paymentGroup.paymentMethod}${separator}" />
			</c:otherwise>
		</c:choose>
		</c:otherwise>
	  </c:choose>
     </c:forEach>
<c:set var="orderTaxonomy" value="${orderValueTaxonomy}&creditcard=${creditCardType}${productCatTaxonomy}${taxSelectedPaymentGroupType}${productSubCatTaxonomy}"/>
<c:set var="orderTaxonomy" value="${fn:toLowerCase(orderTaxonomy)}"/>

<dsp:getvalueof bean="TRUTealiumConfiguration.browserID" var="browserID"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.deviceType" var="deviceType"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.orderOasTaxonomy" var="oasTaxonomy"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.orderOasSize" var="oasSize"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.orderOasBreadcrumb" var="oasBreadcrumb"/>
<%-- <dsp:getvalueof bean="TRUTealiumConfiguration.orderProductSubcategory" var="productSubcategory"/> --%>
<dsp:getvalueof bean="TRUTealiumConfiguration.orderProductFindingMethod" var="productFindingMethod"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.orderInternalCampaignPage" var="internalCampaignPage"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.orderOrsoCode" var="orsoCode"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.orderProductMerchCategory" var="productMerchandisingCategory"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.orderDefaultShipType" var="orderDefaultShipType"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.orderStore" var="orderStore"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.orderErrorCode" var="orderErrorCode"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.orderErrorMessage" var="orderErrorMessage"/>

<dsp:getvalueof var="siteCode" value="TRU" />
<dsp:getvalueof var="baseBreadcrumb" value="trubru" />
<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
   	<c:if test="${siteId eq babySiteCode}">
		<dsp:getvalueof var="siteCode" value="BRU" />
	</c:if>

<dsp:droplet name="/com/tru/commerce/locations/TRUGetCookieDroplet">
	<dsp:param name="cookieName" value="favStore" />
	<dsp:oparam name="output">
		<dsp:getvalueof var="cookieValue" param="cookieValue" />
		<dsp:getvalueof var="locationIdFromCookie" param="locationId" />
	</dsp:oparam>
	<dsp:oparam name="empty">
	</dsp:oparam>
</dsp:droplet>

<c:set var="session_id" value="${cookie.sessionID.value}"/>
<c:set var="visitor_id" value="${cookie.visitorID.value}"/>

<dsp:droplet name="/atg/dynamo/droplet/ForEach">
  <dsp:param name="array" value="${order.commerceItems}" />
  <dsp:oparam name="output">
   <dsp:tomap var="commerceItem" param="element"/>
     <dsp:droplet name="/atg/dynamo/droplet/ForEach">
      <dsp:param name="array" value="${commerceItem.priceInfo.adjustments}" />
      <dsp:oparam name="output">
        <dsp:tomap var="itemAdjustment" param="element"/>
        	<dsp:droplet name="IsNull">
        	 <dsp:param name="value" value="${itemAdjustment.pricingModel}" />
        	  <dsp:oparam name="false">
        	<dsp:getvalueof var="promotionName" value="${promotionName}${fn:escapeXml(itemAdjustment.pricingModel.displayName)},"/>
        	<dsp:getvalueof var="promotionCode" value="${promotionCode}${fn:escapeXml(itemAdjustment.pricingModel.description)},"/>
        		<c:if test="${empty itemAdjustment.pricingModel.description}">
					<c:if test="${promotionCode eq ','}">
						<c:set var="promotionCode" value=""/>
					</c:if>
					<dsp:getvalueof var="promotionCode" value="${promotionCode}${defaultPromoCode},"/>
				</c:if>
        	<c:set var="orderCouponType" value="${orderCouponType}'Item Level',"/>
        	</dsp:oparam>
        </dsp:droplet>
        </dsp:oparam>
       </dsp:droplet>
      </dsp:oparam>
  </dsp:droplet>
  
<dsp:droplet name="/atg/dynamo/droplet/ForEach">
   <dsp:param name="array" value="${order.priceInfo.adjustments}" />
   <dsp:oparam name="output">
     <dsp:tomap var="orderAdjustment" param="element"/>
     	<dsp:droplet name="IsNull">
        	 <dsp:param name="value" value="${orderAdjustment.pricingModel}" />
        	  <dsp:oparam name="false">
     			<dsp:getvalueof var="promotionName" value="${promotionName}${fn:escapeXml(orderAdjustment.pricingModel.displayName)},"/>
     			<dsp:getvalueof var="promotionCode" value="${promotionCode}${fn:escapeXml(orderAdjustment.pricingModel.description)},"/>
     			<c:if test="${empty orderAdjustment.pricingModel.description}">
					<c:if test="${promotionCode eq ','}">
						<c:set var="promotionCode" value=""/>
					</c:if>
					<c:set var="promotionCode" value="${promotionCode}${defaultPromoCode},"/>
				</c:if>
     			<c:set var="orderCouponType" value="${orderCouponType}'Order Level',"/>
     		  </dsp:oparam>
     	</dsp:droplet>
   </dsp:oparam>
 </dsp:droplet>
 
 <c:forEach items="${order.shippingGroups}" var="shippingGroup">
     <%-- Iterate adjustments in each shipping group --%>
     <dsp:droplet name="/atg/dynamo/droplet/ForEach">
       <dsp:param name="array" value="${shippingGroup.priceInfo.adjustments}"/>
       <dsp:oparam name="output">
         <dsp:tomap var="shippingAdjustment" param="element"/>
         	<dsp:droplet name="IsNull">
        	 <dsp:param name="value" value="${shippingAdjustment.pricingModel}" />
        	  <dsp:oparam name="false">
         		<dsp:getvalueof var="promotionName" value="${promotionName}${fn:escapeXml(shippingAdjustment.pricingModel.displayName)},"/>
         		<dsp:getvalueof var="promotionCode" value="${promotionCode}${fn:escapeXml(shippingAdjustment.pricingModel.description)},"/>
         		<c:if test="${empty shippingAdjustment.pricingModel.description}">
					<c:if test="${promotionCode eq ','}">
						<c:set var="promotionCode" value=""/>
					</c:if>
					<c:set var="promotionCode" value="${promotionCode}${defaultPromoCode},"/>
				</c:if>
         		<c:set var="orderCouponType" value="${orderCouponType}'Shipping Level',"/>
         	  </dsp:oparam>
         	</dsp:droplet> 
         	<c:if test="${shippingGroup.shippingGroupClassType eq 'hardgoodShippingGroup'}">
         	  <c:if test="${shippingAdjustment.adjustmentDescription eq 'Shipping Discount'}">
         	  	<c:set var="shippingDiscount" value="${shippingAdjustment.totalAdjustment}"/>
				<c:set var="orderShippingDiscount" value="${orderShippingDiscount+(-shippingDiscount)}"/>
         	  </c:if>
         	</c:if>
       </dsp:oparam>
     </dsp:droplet>
   </c:forEach>
<script type='text/javascript'>

var promotionNameList = "${promotionName}".slice(0,-1);
var promoName = promotionNameList.split(',');
var promotionCodeList = "${promotionCode}".slice(0,-1);
var promoCode = promotionCodeList.split(',');

var selectedPaymentGroupType = "${selectedPaymentGroupType}".slice(0,-1);

var utag_data = {
	    customer_status:"${customerStatus}",
	    customer_city:"${customerCity}",
	    customer_country:"${customerCountry}",
	    customer_email:"${customerEmail}",
	    customer_id:"${customerID}",
	    event_type:"",
	    customer_type:"${customerStatus}",
	    customer_state:"${customerState}",
	    customer_zip:"${customerZip}",
	    customer_dob:"${customerDOB}",
	    customer_name:"${customerName}",
	    page_name:"${tPageName}",
	    page_type:"${tPageType}",
	    product_store:['${siteCode}'],
	    product_brand:[${productBrands}],
	    product_category:[${productCategory}],
	    product_id:[${productID}],
	    product_name:[${productName}],
	    product_quantity:[${itemQuantity}],
	    product_unit_price:[${productUnitPrice}],
	    product_list_price:[${productListPrice}],
	    product_sku:[${productSKUs}],
	    product_subcategory:[${productSubcategory}],
	    product_finding_method:"${productFindingMethod}",
	    product_image_url:[${productImage}],
    	product_merchandising_category:" ",
    	browser_id:"${pageContext.session.id}",
	    internal_campaign_page:"${internalCampaignPage}",
		oas_breadcrumb : "${baseBreadcrumb}/checkout/orderconfirmation",
	    oas_taxonomy : "${orderTaxonomy}",
		oas_sizes : [['Position2', [970,90]],['Position3', [970,90]]],
	    orso_code:"${orsoCode}",
	    product_collection:[${productCollections}],
	    ispu_source:"${ispuSource}",
	    xsell_product:[${orderXsellProduct}],
	    device_type:"${deviceType}",
	    order_subtotal:"${orderSubtotal}",
	    promotion_name:promoName,
	    promotion_code:promoCode,
	    order_total:"${orderTotal}",
	    order_id:"${orderID}",
	    order_payment_type:selectedPaymentGroupType,
	    order_ispu:"${isInStorePickUp}",
	    order_coupon_code:"${couponCode}",
	    order_payment_amount:"${tOrderPaymentAmount}",
	    order_shipping:"${orderShipping}",
	    order_shipping_discount:"${orderShippingDiscount}",
	    shipping_discount:"${shippingDiscount}",
	    order_shipping_country:"${tShippingCountry}",
	    order_shipping_type:[${shippingMethod}],
	    order_shipping_surcharge:"${orderShippingSurcharge}",
	    order_fulfillment_channel:[${orderFulFilChannel}],
	    order_tax:"${orderTax}",
	    order_store:"${orderStore}",
	    order_type:[${orderType}],
	    order_coupon_type:[${orderCouponType}],
	    order_currency:"${orderCurrency}",
	    partner_name:[${partnerName}],
	    selected_store:"${selectedStore}",
	    event_type:"order",
	    wishlist_id:"${wishListId}",
	    store_locator:"${locationIdFromCookie}",
	    session_id : "${session_id}",
		visitor_id : "${visitor_id}",
		tru_or_bru : "${siteCode}"
		};

</script>
<c:if test="${tealiumOrderDiscount ne '0.00'}">
	    <script>utag_data.order_discount="${tealiumOrderDiscount}";</script>
	    </c:if>
	    <c:if test="${not empty productDiscount}">
	    <script>utag_data.product_discount=[${productDiscount}];</script>
	    </c:if>
	    <c:if test="${not empty orderGiftWrap}">
	    <script>utag_data.order_gift_wrap="${orderGiftWrap}";</script>
	    </c:if>
	    <c:if test="${not empty ispuRevenue}">
	    <script>utag_data.ispu_revenue="${ispuRevenue}";</script>
	    </c:if>
	    <c:if test="${not empty registryId}">
	    <script>utag_data.registry_id="${registryId}";</script>
	    </c:if>
	    
<script type="text/javascript">
			var shippingCount = "${shippingCount}",
			promotionName = "${promotionName}";
			
			$.getScript("${tealiumURL}",function(){
			if(!isNaN(shippingCount) && shippingCount > 1){
				console.log("No Of Shipping Groups:"+shippingCount);
				utag_data.event_type='multiple_ship_to';
			}
			if(promotionName.length > 0){
				console.log("promotion name:"+promotionName);
				utag_data.event_type='promotion_applied';
			}
			}); 
</script>
</dsp:page>