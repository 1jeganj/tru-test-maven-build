<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/com/tru/commerce/order/droplet/DisplayOrderDetailsDroplet" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:getvalueof var="isAnonymousUser" bean="Profile.transient" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<%-- print tempplate --%>		
	<div class="modal-dialog modal-dialog-print">
		<div class="modal-content sharp-border">
			<div class="print-overlay">
				<div class="row">
					<div class="col-md-12">
						 <a href="javascript:void(0)" data-dismiss="modal"><span class="clickable"><span class="sprite-icon-x-close"></span></span></a>
					</div>
				</div>						
				<div class="tse-scrollable">
					<div class="tse-scroll-content">
						<div class="tse-content">
							   <div class="default-margin">
									<div class="checkout-order-complete">
										<div class="row">
											<div class="col-md-12">
												<h1>
											<fmt:message key="checkout.confirm.orderCompleteHeading" />
										</h1>
											</div>
											<div class="col-md-8">
												<p class="header-subtext">
													 <fmt:message key="checkout.confirm.orderNumber" />&nbsp;<span><dsp:valueof bean="ShoppingCart.last.id"/></span>. <fmt:message key="checkout.confirm.confirmationEmail" />.
												</p>																												  
											</div>
											<div class="col-md-4 confirmation-links">
												<div class="pull-right">
												<button class="print-button" onclick="window.print();"><fmt:message key="checkout.print" /></button>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-no-padding">
											<hr>
										</div>
									</div>											
									   <%-- <dsp:include page="/checkout/review/orderReviewInclude.jsp" /> --%>
									   
								<div class="row checkout-sticky-top checkout-confirmation-content">
									<div class="col-md-8">
										<dsp:param name="result" bean="ShoppingCart.last"></dsp:param>
									 	<%-- <dsp:include page="/checkout/common/paymentSummary.jsp">
											<dsp:param name="paymentGroups" param="result.paymentGroups" />
											<dsp:param name="pageName" value="Confirm" />
									  	</dsp:include>	 --%>																	
										<dsp:droplet name="DisplayOrderDetailsDroplet">
										<dsp:param name="order" bean="ShoppingCart.last" />
										<dsp:param name="profile" bean="Profile" />
										<dsp:param name="page" value="Confirm" />
										<dsp:oparam name="output">
											<dsp:droplet name="ForEach">
												<dsp:param name="array" param="shippingDestinationsVO" />
												<dsp:param name="elementName" value="shippingDestinationVO" />
												<dsp:oparam name="output">
													<dsp:getvalueof var="shippingGroupType" param="shippingDestinationVO.shippingGroupType"/>
													<c:choose>
														<c:when test="${shippingGroupType eq 'hardgoodShippingGroup'}">
															<dsp:getvalueof var="nickName" param="key" />
															<div class="checkout-confirmation-items-shipping-to">
																<dsp:include page="/checkout/common/displayShippingAddress.jsp">
																	<dsp:param name="shippingDestinationVO" param="shippingDestinationVO" />
																	<dsp:param name="pageName" value="Confirm" />
																</dsp:include>
																<dsp:droplet name="ForEach">
																	<dsp:param name="array"	param="shippingDestinationVO.shipmentVOMap" />
																	<dsp:param name="elementName" value="shipmentVO" />
																	<dsp:oparam name="output">
																		<dsp:getvalueof var="frieghtClass" param="key" />
																		<dsp:include page="/checkout/common/shipmentDetails.jsp">
																			<dsp:param name="size" param="size" />
																			<dsp:param name="count" param="count" />
																			<dsp:param name="frieghtClass" value="${frieghtClass}" />
																			<dsp:param name="pageName" value="Confirm" />
																			<dsp:param name="shipmentVO" param="shipmentVO" />
																		</dsp:include>
																		<hr>
																		<dsp:include page="/checkout/common/displayItemDetails.jsp">
																			<dsp:param name="shipmentVO" param="shipmentVO" />
																			<dsp:param name="pageName" value="Confirm" />
																			<dsp:param name="shippingGroupType" value="${shippingGroupType}" />
																		</dsp:include>
																	</dsp:oparam>
																</dsp:droplet>
															</div>
														</c:when>
														<c:when test="${shippingGroupType eq 'inStorePickupShippingGroup'}">
															<dsp:droplet name="ForEach">
																<dsp:param name="array"	param="shippingDestinationVO.shipmentVOMap" />
																<dsp:param name="elementName" value="shipmentVO" />
																<dsp:oparam name="output">
																	<div class="checkout-review-items-free-pickup">
													                    <dsp:include page="/checkout/common/displayFreePickupDetails.jsp">
													                    	<dsp:param name="inStorePickupShippingGroup" param="shipmentVO.shippingGroup" />
													                    </dsp:include>
												                    </div>
												                    <br>
												                    <dsp:include page="/checkout/common/displayItemDetails.jsp">
																		<dsp:param name="shipmentVO" param="shipmentVO" />
																		<dsp:param name="pageName" value="Confirm" />
																		<dsp:param name="shippingGroupType" value="${shippingGroupType}" />
																	</dsp:include>
												                </dsp:oparam>
												            </dsp:droplet>
														</c:when>
													</c:choose>
												</dsp:oparam>
											</dsp:droplet>
										</dsp:oparam>
						        	<dsp:droplet name="IsEmpty">
										<dsp:param name="value" param="donationItemsList" />
										<dsp:oparam name="false">
										<%-- display the donation header here --%>
											<div class="checkout-review-digital-items donation">
						                    	<h2><fmt:message key="checkout.review.donationHeading"/></h2>
						                	</div>
						               	 <br>
										<dsp:droplet name="ForEach">
											<dsp:param name="array" param="donationItemsList" />
											<dsp:param name="elementName" value="donationItem" />
											<dsp:oparam name="output">
												<dsp:include page="/checkout/common/displayDonationItemDetail.jsp">
													<dsp:param name="donationItem" param="donationItem" />
													<dsp:param name="pageName" value="Confirm" />
												</dsp:include>
											</dsp:oparam>
										</dsp:droplet>
									</dsp:oparam>
								</dsp:droplet>
									</dsp:droplet>
									</div>
									<div class="col-md-4 pull-right">
										<div class="shopping-cart-summary-block">
											<div class="summary-block-border">
												<dsp:include page="/checkout/common/priceSummary.jsp">
													<%-- <dsp:param name="price" param="price"/> --%>
													<dsp:param name="pageName" value="Confirm" />
												</dsp:include>
											</div>											
										</div>	
									</div>
									
									<div class="order-summary-secure-logos pull-right">
											<div class="pull-right fixed-width">
												<%--<div class="norton-secured"   id="load-checkout-norton-script"> -->
												 <dsp:include page="../../common/nortonSealScript.jsp"/>
													<!-- <a href="http://www.norton.com/" target="_blank"><img src="${TRUImagePath}images/Global_Norton-Logo.jpg"></a> -->
												</div> 
											</div>--%>
									</div>
								</div>
									<%--Order Summary --%>
								<button class="print-button pull-right" onclick="window.print();"><fmt:message key="checkout.print" /></button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	</div>
	<!--End print template -->
</dsp:page>	
