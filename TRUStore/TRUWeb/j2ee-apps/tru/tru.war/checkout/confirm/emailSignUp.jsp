<dsp:page>
	<dsp:importbean bean="/atg/targeting/TargetingFirst" />
	<dsp:importbean bean="/atg/registry/RepositoryTargeters/TRU/PrivacyPolicy/PrivacyPolicyContentTargeter" />

	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<div class="bordered-column checkout-order-complete__content-block-header checkout-order-complete__content-block-header--email-signup">
	</div>	
	<div class="checkout-order-complete__content-block-body">
		<div class="checkout-order-complete__content-block-title">
			<fmt:message key="checkout.confirm.emailSignUpHeading" />
		</div>
		<a href="#" class="" data-toggle="modal" data-target="#privacyPolicyModal">
			<fmt:message key="checkout.confirm.emailSignUpPrivacyPolicy" />
		</a>		
		<p>
			<fmt:message key="checkout.confirm.emailSignUpMessage" />
		</p>
		<label><fmt:message key="checkout.confirm.emailSignUpAddress" /></label>
		<form id="chekcoutSignUpEmailForm" class="JSValidation">
			<div class="email-alerts-sign-up">
				<input type="text" id="email" name="email" class="emailAlertsSignUp checkout-order-complete__content-block-field" aria-label="email" title="email" placeholder="enter email address" maxlength="60">
				<div>
					<input type="submit" class="checkout-order-complete__content-block-button checkout-order-complete__content-block-button--attach-bottom" name="chekcoutSignUpEmail" id="chekcoutSignUpEmail" value='<fmt:message key="checkout.confirm.emailSignUpButton" />'/>
				</div>
			</div>
		</form>
	</div>

	<div class="modal fade" id="privacyPolicyModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog quick-help-modal">
			<div class="modal-content sharp-border">
				<dsp:droplet name="/atg/targeting/TargetingFirst">
					<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/PrivacyPolicy/PrivacyPolicyContentTargeter" />
					<dsp:oparam name="output">
						<dsp:valueof param="element.data" valueishtml="true" />
					</dsp:oparam>
				</dsp:droplet>
			</div>
		</div>
	</div>
</dsp:page>