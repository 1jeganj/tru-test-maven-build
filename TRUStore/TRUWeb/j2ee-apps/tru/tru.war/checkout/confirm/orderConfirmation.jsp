<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:page>

	<dsp:importbean bean="/atg/multisite/Site" />
	<tru:pageContainer>
	<jsp:attribute name="removeGiftFinder">RemoveFinder</jsp:attribute>
	<jsp:attribute name="pageName">orderConfirmation</jsp:attribute>
	<jsp:body>
	<%-- <link rel="stylesheet" href="${TRUCSSPath}css/orderConfirmation.css"> --%>
		<div id="orderConfirmationInclude"></div>
	</jsp:body>
    </tru:pageContainer>
  	<script>
  	ajaxCallForOrderConfirmationPage('${originatingRequest.contextPath}');
	</script>
</dsp:page>  