<dsp:page>
	<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
	<dsp:getvalueof bean="TRUConfiguration.restInvokeCookieURL" var="restInvokeCookieURL" />
	<dsp:getvalueof bean="TRUConfiguration.restDataType" var="restDataType" />	
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
   	<script>
		$(document).ready(function() {
			$.ajax({
	                    type: "post",
	                    url: "${restInvokeCookieURL}",
						datatype : "${restDataType}"
	                });
		});
  </script>
</dsp:page>
