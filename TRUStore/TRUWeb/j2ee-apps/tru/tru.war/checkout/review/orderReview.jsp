<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/com/tru/common/TRUUserSession" />
	<dsp:setvalue bean="ShoppingCart.current.previousTab" beanvalue="ShoppingCart.current.currentTab" />
	<dsp:setvalue bean="ShoppingCart.current.currentTab" value="review-tab" />
	<dsp:setvalue bean="TRUUserSession.signInHidden" value="true" />
	<%--Start : MVP 86 & 97 OOPs error message  --%>
	<input type="hidden" id="shipRestrictionsFound" value="false"  />
	<%--End :  MVP 86 & 97 OOPs error message --%>
	<div id="review-tab">
		<input type="hidden" value="Review" id="pageName" />
		<div class="row">
			<div name="review_error_msg" id="review_error_msg" display="none">
				<dsp:include page="orderReviewInclude.jsp" >
					<dsp:param name="existingRelationShipId" value="${relationShipId}" />
					<dsp:param name="inlineMessage" value="${inlineMessage}" />
				</dsp:include>
				<dsp:include page="overlayModal.jsp" />
			</div>
		</div>
	</div>
</dsp:page>	