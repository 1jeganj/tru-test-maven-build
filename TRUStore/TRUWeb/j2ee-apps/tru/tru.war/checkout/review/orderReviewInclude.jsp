<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/com/tru/commerce/order/droplet/DisplayOrderDetailsDroplet" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftCardFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler" />
	<dsp:setvalue bean="ShippingGroupFormHandler.updateRelationShips" value="submit" />
	<dsp:setvalue bean="GiftCardFormHandler.adjustPaymentGroups" value="submit" />
	<dsp:setvalue bean="BillingFormHandler.updateSynchronyDataInOrder" value="submit" />
	<dsp:getvalueof param="existingRelationShipId" var="existingRelationShipId"/>
	<dsp:getvalueof param="inlineMessage" var="inlineMessage"/>
	<div class="row checkout-sticky-top checkout-confirmation-content">
		<dsp:droplet name="IsEmpty">
			<dsp:param name="value" bean="ShoppingCart.current.commerceItems" />
			<dsp:oparam name="false">
				<dsp:param name="result" bean="ShoppingCart.current" />
				<div class="col-md-8 checkout-left-column">
					<dsp:droplet name="DisplayOrderDetailsDroplet">
						<dsp:param name="order" bean="ShoppingCart.current" />
						<dsp:param name="profile" bean="Profile" />
						<dsp:param name="page" value="Review" />
						<dsp:oparam name="output">						
						<p class="global-error-display" id="order-review-global-error"></p>
							<dsp:include page="/checkout/common/checkoutGlobalError.jsp">
								<dsp:param name="pageName" value="Review" />
								<dsp:param name="financingFailed" value="true" />
							</dsp:include>
							<div class="checkout-review-header">
								<h2>
									<fmt:message key="checkout.review.reviewOrderHeading" />
								</h2>
								<p><fmt:message key="checkout.review.reviewOrderMessage" /></p>
							</div>							
							<div id="order-review-error"></div>
							<hr class="horizontal-divider"></hr>
							<div class="checkout-review-payment-method">
								<dsp:include page="/checkout/common/paymentSummary.jsp">
									<dsp:param name="paymentGroups" param="result.paymentGroups" />
									<dsp:param name="pageName" value="Review" />
								</dsp:include>
							</div>
							<br>
							<dsp:droplet name="ForEach">
								<dsp:param name="array" param="shippingDestinationsVO" />
								<dsp:param name="elementName" value="shippingDestinationVO" />
								<dsp:param name="sortProperties" value="-shippingGroupType"/>
								<dsp:oparam name="output">
									<dsp:getvalueof var="shippingGroupType" param="shippingDestinationVO.shippingGroupType"/>
									<dsp:getvalueof var="itemsCount" param="shippingDestinationVO.itemsCount" />
									<c:choose>
										<c:when test="${(shippingGroupType eq 'hardgoodShippingGroup') or (shippingGroupType eq 'channelHardgoodShippingGroup')}">
											<dsp:getvalueof var="nickName" param="key" />
											<div class="checkout-review-items-shipping-to">
												<dsp:include page="/checkout/common/displayShippingAddress.jsp">
													<dsp:param name="shippingDestinationVO" param="shippingDestinationVO" />
													<dsp:param name="pageName" value="Review" />
												</dsp:include>
												<dsp:droplet name="ForEach">
													<dsp:param name="array"	param="shippingDestinationVO.shipmentVOMap" />
													<dsp:param name="elementName" value="shipmentVO" />
													<dsp:param name="sortProperties" value="+estimateMinDate"/>													
													<dsp:oparam name="outputStart">
														<dsp:getvalueof var="shippingGroupId" param="shipmentVO.shippingGroup.id"/>
														<dsp:droplet name="Switch">
														<dsp:param name="value" param="shipmentVO.shippingGroup.giftReceipt" />
															<dsp:oparam name="true">
																<div class="gift-message">
																		<dsp:getvalueof var="giftWrapMessage" param="shipmentVO.shippingGroup.giftWrapMessage" />
																		<div class="gift-message-header">
											                            	<header class="gift-message-checkout-header"><fmt:message key="checkout.gifting.header"/></header><span class="review-gift-message-container">(<span class="chars-left review-gift-message-count-container"><dsp:valueof value="${180 - fn:length(giftWrapMessage)}"></dsp:valueof></span>&nbsp;<fmt:message key="checkout.gifting.charactersRemaining"/>)</span>
											                            </div>
											                            <div class="edit gift-edit-message">
											                            
											                            <span class="reviewGiftMessage">
											                            	<c:choose>
											                            		<c:when test="${not empty giftWrapMessage}">
											                            			"<dsp:valueof param="shipmentVO.shippingGroup.giftWrapMessage"  valueishtml="true" />"&nbsp;.
											                            		</c:when>
											                            	</c:choose>
											                            </span>
											                            	<a href="javascript:void(0)"><fmt:message key="shoppingcart.edit"/></a>
											                            </div>
											                            <div class="gift-message-area-wrapper display-none" id="${shippingGroupId}-gift-message">
											                                <textarea maxtextlength="180" cols="25" rows="5" class="giftMessageArea" id="${shippingGroupId}_review_giftMessage">${giftWrapMessage}</textarea>
											                                <button type="button" class="gift-save-message" id="${shippingGroupId}_gift-save-message-id"><fmt:message key="checkout.overlay.save"/></button>
											                                
											                                <input type="hidden" value="${shippingGroupId}" class="review_giftMessage_shippingGroupId" id="${shippingGroupId}_review_giftMessage_shippingGroupId" />
											                            </div>
											                     </div>
															</dsp:oparam>
														</dsp:droplet>
													</dsp:oparam>
													<dsp:oparam name="output">
														<dsp:include page="/checkout/common/shipmentDetails.jsp">
															<dsp:param name="size" param="size" />
															<dsp:param name="count" param="count" />
															<dsp:param name="nickName" value="${nickName}" />
															<dsp:param name="pageName" value="Review" />
															<dsp:param name="shipmentVO" param="shipmentVO" />
														</dsp:include>
														<hr>
														<dsp:include page="/checkout/common/displayItemDetails.jsp">
															<dsp:param name="shipmentVO" param="shipmentVO" />
															<dsp:param name="pageName" value="Review" />
															<dsp:param name="shippingGroupType" value="${shippingGroupType}" />
															<dsp:param name="existingRelationShipId" value="${existingRelationShipId}" />
															<dsp:param name="inlineMessage" value="${inlineMessage}" />
														</dsp:include>
													</dsp:oparam>
												</dsp:droplet>
											</div>
										</c:when>
										<c:when test="${(shippingGroupType eq 'inStorePickupShippingGroup') or (shippingGroupType eq 'channelInStorePickupShippingGroup')}">
											<dsp:droplet name="ForEach">
												<dsp:param name="array"	param="shippingDestinationVO.shipmentVOMap" />
												<dsp:param name="elementName" value="shipmentVO" />
												<dsp:oparam name="output">
												<div class="checkout-review-items-free-pickup">
									                    <dsp:include page="/checkout/common/displayFreePickupDetails.jsp">
									                   	<dsp:param name="itemsCount" value="${itemsCount}" />
									                    <dsp:param name="inStorePickupShippingGroup" param="shipmentVO.shippingGroup" />
									                    <dsp:param name="shippingGroupType" value="${shippingGroupType}" />
									                    </dsp:include>
								                    </div>
								                    <br>
								                    <dsp:include page="/checkout/common/displayItemDetails.jsp">
														<dsp:param name="shipmentVO" param="shipmentVO" />
														<dsp:param name="pageName" value="Review" />
														<dsp:param name="shippingGroupType" value="${shippingGroupType}" />
														<dsp:param name="existingRelationShipId" value="${existingRelationShipId}" />
														<dsp:param name="inlineMessage" value="${inlineMessage}" />
													</dsp:include>
								                </dsp:oparam>
								            </dsp:droplet>
										</c:when>
									</c:choose>
								</dsp:oparam>
							</dsp:droplet>
							<dsp:droplet name="IsEmpty">
								<dsp:param name="value" param="donationItemsList" />
								<dsp:oparam name="false">
									<%-- display the donation items here --%>
									<div class="checkout-review-digital-items donation">
					                    <h2><fmt:message key="checkout.review.donationHeading"/></h2>
					                </div>
					                <br>
									<dsp:droplet name="ForEach">
										<dsp:param name="array" param="donationItemsList" />
										<dsp:param name="elementName" value="donationItem" />
										<dsp:oparam name="output">
											<dsp:include page="/checkout/common/displayDonationItemDetail.jsp">
												<dsp:param name="donationItem" param="donationItem" />
												<dsp:param name="pageName" value="Review" />
											</dsp:include>
										</dsp:oparam>
									</dsp:droplet>
								</dsp:oparam>
							</dsp:droplet>							
							<%-- <c:if test="${validationFailed eq true}" >
								<dsp:droplet name="IsEmpty">
									<dsp:param name="value" param="outOfStockItemsList" />
									<dsp:oparam name="false">
										display the out-of-stock items here
										<div class="checkout-review-digital-items donation">
						                    <h2><fmt:message key="checkout.out.of.stock.items" /></h2>
						                </div>
						                <br>
											<dsp:droplet name="ForEach">
												<dsp:param name="array" param="outOfStockItemsList" />
												<dsp:param name="elementName" value="outOfStockItem" />
												<dsp:oparam name="output">
													<dsp:include page="/cart/outOfStockItemDetails.jsp">
														<dsp:param name="item" param="outOfStockItem" />
														<dsp:param name="index" param="index" />
													</dsp:include>
												</dsp:oparam>
											</dsp:droplet>
									</dsp:oparam>
								</dsp:droplet>
							</c:if> --%>
						</dsp:oparam>
					</dsp:droplet>
					<dsp:include page="/checkout/common/salesTaxEstimation.jsp"></dsp:include>
				</div>
				<dsp:getvalueof var="commerceItems" param="result.commerceItems" />
				<dsp:test var="items" value="${commerceItems}" />

				<div class="col-md-3 checkout-right-column">
					<dsp:include page="/checkout/common/checkoutOrderSummary.jsp">
						<dsp:param name="pageName" value="Review" />
						<dsp:param name="paymentMethod" param="paymentMethod" />
						<dsp:param name="order" param="result" />
					</dsp:include>
				</div>
			</dsp:oparam>
			<dsp:oparam name="true">
			    <div class="shopping-cart-empty">
					<div class="shopping-cart-empty-header">
						<fmt:message key="shoppingcart.cart.epmty" />
					</div>
					<div class="shopping-cart-empty-subheader">
						<a href="#"><fmt:message key="checkout.review.continueShoppingLink"/></a>.
					</div>
				</div>
			</dsp:oparam>
		</dsp:droplet>
	</div>
	<input type="hidden" id="tealiumPageName" value="orderReview"/>
	 <div id="tealiumCheckoutContent">
      
 			
 	</div> 
</dsp:page>