<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
		<div class="modal fade in" id="checkoutReviewProcessingOverlay"	tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false" style="display: none;">
		<div class="modal-dialog modal-md">
			<div class="modal-content sharp-border">
				<img class="" src="${TRUImagePath}images/processing.gif" alt="Processing...">
				<header class="review-processing-header">&nbsp;<fmt:message key="checkout.review.processOrderOverlayHeading" />&nbsp;</header>
				<p><fmt:message key="checkout.review.processOrderOverlaySubHeading" /></p>
			</div>
		</div>
	</div>
	<div class="modal fade" id="removeItemConfirmationModal" tabindex="-1"
		role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content sharp-border">
				<a href="javascript:void(0)"  data-dismiss="modal">
					<span class="clickable"><img src="${TRUImagePath}images/close.png" alt="close model"></span>
				</a>
				<div>
					<h2><fmt:message key="checkout.removeItemHeading" /></h2>
					<a href="javascript:void(0);" class="text-color" onclick="closeRemoveModal();"><fmt:message key="checkout.backToReview" /></a>&nbsp;<a href="javascript:void(0);" class="text-color" onclick="removeCartItemInReview()"><fmt:message key="checkout.removeItem" /></a>
					<input type="hidden" value="" id="removeItemId">
					<input type="hidden" value="" id="removeDonationItemId">
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade in review-findinstore" id="findInStoreModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<dsp:include page="/jstemplate/findInStoreInfo.jsp"/>
	</div>
	<div class="modal fade editCart edit-cart-modal" id="editCartModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="display: none;">
					<div class="modal-lg modal-dialog">
					<div class="modal-content sharp-border">
					</div></div>
				</div>
</dsp:page>