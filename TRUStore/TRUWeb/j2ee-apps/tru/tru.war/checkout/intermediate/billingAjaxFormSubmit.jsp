<dsp:page>

<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/CheckoutProfileFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/atg/dynamo/droplet/IsNull" />
<dsp:importbean bean="/atg/commerce/promotion/CouponFormHandler" />
<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUGetSynchronyUrlDroplet" />
<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUParseSynchronyResponseDroplet" />
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
<dsp:getvalueof var="order" bean="ShoppingCart.current" />
<dsp:getvalueof var="truConf" bean="TRUConfiguration" />
<dsp:getvalueof var="formID" param="formID"></dsp:getvalueof>
<dsp:getvalueof var="fromPageName" param="fromPageName"></dsp:getvalueof>
<dsp:getvalueof var="action" param="action"></dsp:getvalueof>
<dsp:getvalueof var="financingTermsAvailable" value="${order.financingTermsAvailable}"/>

	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler" />
	<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftOptionsFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/com/tru/commerce/order/purchase/InStorePickUpFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler"/>
	
	

 <c:choose>
 	<c:when test="${formID eq 'payPalOrderReviewForm'}">
		<dsp:setvalue bean="BillingFormHandler.email" paramvalue="orderEmail"/>
		<%--Start: Added for adding rewards number to order  --%>
        <dsp:setvalue bean="BillingFormHandler.rewardNumberValid" paramvalue="isRewardsCardValid" />
		<dsp:setvalue bean="BillingFormHandler.rewardNumber" paramvalue="rewardNumber" /> 
		<%--End: Added for adding rewards number to order  --%>
		<dsp:setvalue bean="BillingFormHandler.payPalPaymentToReviewOrder" value="submit"/>
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="BillingFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="errorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	              <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
            <dsp:oparam name="false">
				 
            </dsp:oparam>
         </dsp:droplet>
   </c:when>
	<c:when test="${formID eq 'payPalOrderSubmitForm'}">
		<dsp:setvalue bean="BillingFormHandler.email" paramvalue="orderEmail"/>
		<%--Start: Added for adding rewards number to order  --%>
        <dsp:setvalue bean="BillingFormHandler.rewardNumberValid" paramvalue="isRewardsCardValid" />
		<dsp:setvalue bean="BillingFormHandler.rewardNumber" paramvalue="rewardNumber" /> 
		<%--End: Added for adding rewards number to order  --%>
		<dsp:setvalue bean="BillingFormHandler.payPalToken" paramvalue="tokenExist"/>
		<dsp:setvalue bean="BillingFormHandler.payPalEmailValidate" value="submit"/>
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="BillingFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="errorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
            <dsp:oparam name="false">
				 address-saved-success-from-ajax: 
            </dsp:oparam>
         </dsp:droplet>
   </c:when>
   <c:when test="${formID eq 'checkoutWithPayPal'}">
		<dsp:setvalue bean="BillingFormHandler.checkoutWithPayPal" value="submit"/>
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="BillingFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="payPalErrorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
           <dsp:oparam name="false">
           	 token:<dsp:valueof bean="/com/tru/common/TRUUserSession.paypalPaymentDetailsMap.payPalToken"/>
           </dsp:oparam>
         </dsp:droplet>
   </c:when>
   <c:when test="${formID eq 'giftCardPaymentOrderReviewForm'}">
   		<dsp:getvalueof var="firstName" param="firstName" scope="request" />
		<dsp:getvalueof var="lastName" param="lastName" scope="request" />
		<dsp:getvalueof var="address1" param="address1" scope="request" />
		<dsp:getvalueof var="address2" param="address2" scope="request" />
		<dsp:getvalueof var="city" param="city" scope="request" />
		<dsp:getvalueof var="state" param="state" scope="request" />
		<dsp:getvalueof var="otherState" param="otherState" scope="request" />
		<dsp:getvalueof var="postalCode" param="postalCode" scope="request" />
		<dsp:getvalueof var="country" param="country" scope="request" />
		<dsp:getvalueof var="phoneNumber" param="phoneNumber" scope="request" />
		<dsp:getvalueof var="orderEmail" param="orderEmail" scope="request" />
		<dsp:getvalueof var="newBillingAddress" param="newBillingAddress" scope="request" />
		<dsp:getvalueof var="editBillingAddress" param="editBillingAddress" scope="request" />
		<dsp:getvalueof var="isRewardsCardValid" param="isRewardsCardValid" scope="request" />
		<dsp:getvalueof var="rewardNumber" param="rewardNumber" scope="request" />
		
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.firstName" paramvalue="firstName"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.lastName" paramvalue="lastName"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.address1" paramvalue="address1"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.address2" paramvalue="address2"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.city" paramvalue="city"/>
		
		<c:choose>
			<c:when test="${country ne 'US'}">
				<dsp:setvalue bean="BillingFormHandler.addressInputFields.state" paramvalue="otherState"/>
			</c:when>
			<c:otherwise>
				<dsp:setvalue bean="BillingFormHandler.addressInputFields.state" paramvalue="state"/>
			</c:otherwise>
		</c:choose>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.postalCode" paramvalue="postalCode"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.phoneNumber" paramvalue="phoneNumber"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.country" paramvalue="country"/>
		
		<dsp:setvalue bean="BillingFormHandler.email" paramvalue="orderEmail"/>
		<dsp:setvalue bean="BillingFormHandler.newBillingAddress" paramvalue="newBillingAddress"/>
		<dsp:setvalue bean="BillingFormHandler.editBillingAddress" paramvalue="editBillingAddress"/>
		<dsp:setvalue bean="BillingFormHandler.addressValidated" paramvalue="addressValidated"/>
				
		<dsp:setvalue bean="BillingFormHandler.rewardNumberValid" paramvalue="isRewardsCardValid" />
		<dsp:setvalue bean="BillingFormHandler.rewardNumber" paramvalue="rewardNumber" />
		<dsp:setvalue bean="BillingFormHandler.giftCardPaymentToReviewOrder"  value="submit"/>
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="BillingFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="errorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
            <dsp:oparam name="false">
				 <dsp:droplet name="Switch">
				<dsp:param name="value" bean="BillingFormHandler.addressDoctorResponseVO.derivedStatus" />
				<dsp:oparam name="NO">
					address-saved-success-from-ajax: 
				</dsp:oparam>
				<dsp:oparam name="YES">
					address-doctor-overlay-from-ajax:
					<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
						<dsp:param name="mode" value="paymentInStore" />
					</dsp:include>
				</dsp:oparam>
				<%-- <dsp:oparam name="VALIDATION ERROR">
					address-doctor-overlay-from-ajax:
					<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
						<dsp:param name="mode" value="paymentInStore" />
					</dsp:include>
				</dsp:oparam> --%>
				<dsp:oparam name="default"> 
					address-saved-success-from-ajax: 
				 </dsp:oparam>
			</dsp:droplet>
            </dsp:oparam>
         </dsp:droplet>
   </c:when>
		<c:when test="${formID eq 'paymentCreditCardForm'}">
			<dsp:getvalueof var="isNoFinanceAgreed" param="isNoFinanceAgreed" scope="request"/>
	        <dsp:getvalueof var="orderSet" param="orderSet" scope="request"/>
	    	<dsp:getvalueof var="firstName" param="firstName" scope="request" />
			<dsp:getvalueof var="lastName" param="lastName" scope="request" />
			<dsp:getvalueof var="address1" param="address1" scope="request" />
			<dsp:getvalueof var="address2" param="address2" scope="request" />
			<dsp:getvalueof var="city" param="city" scope="request" />
			<dsp:getvalueof var="state" param="state" scope="request" />
			<dsp:getvalueof var="otherState" param="otherState" scope="request" />
			<dsp:getvalueof var="postalCode" param="postalCode" scope="request" />
			<dsp:getvalueof var="country" param="country" scope="request" />
			<dsp:getvalueof var="phoneNumber" param="phoneNumber" scope="request" />
			<dsp:getvalueof var="orderEmail" param="orderEmail" scope="request" />
			<dsp:getvalueof var="ccToken" param="ccToken" scope="request" />
			<dsp:getvalueof var="isRUSCard" param="isRUSCard" scope="request" />
			<dsp:getvalueof var="cardType" param="cardType" scope="request" />
			<dsp:getvalueof var="cBinNum" param="cBinNum" scope="request" />
			<dsp:getvalueof var="ccnumber" param="ccnumber" scope="request" />
			<dsp:getvalueof var="cLength" param="cLength" scope="request" />
			<dsp:getvalueof var="expirationMonth" param="expirationMonth" scope="request" />
			<dsp:getvalueof var="expirationYear" param="expirationYear" scope="request" />
			<dsp:getvalueof var="creditCardCVV" param="creditCardCVV" scope="request" />
			<dsp:getvalueof var="nameOnCard" param="nameOnCard" scope="request" />
			<dsp:getvalueof var="editCardNickName" param="editCardNickName" scope="request" />
			<dsp:getvalueof var="newBillingAddress" param="newBillingAddress" scope="request" />
			<dsp:getvalueof var="editBillingAddress" param="editBillingAddress" scope="request" />
			
			<dsp:getvalueof var="newCreditCard" param="newCreditCard"/>
			<dsp:getvalueof var="editCreditCard" param="editCreditCard"/>
			
			<dsp:droplet name="IsNull">
		   		<dsp:param name="value" value="${newBillingAddress}" />
		   		<dsp:oparam name="false">
					<dsp:getvalueof var="newBillingAddress" value="${newBillingAddress}" scope="request"/>
		   		</dsp:oparam>
		   		<dsp:oparam name="true">
					<dsp:getvalueof var="newBillingAddress" value="false" scope="request"/>
		   		</dsp:oparam>
		   	</dsp:droplet>
		   	<dsp:droplet name="IsNull">
		   		<dsp:param name="value" value="${editBillingAddress}" />
		   		<dsp:oparam name="false">
					<dsp:getvalueof var="editBillingAddress" value="${editBillingAddress}" scope="request"/>
		   		</dsp:oparam>
		   		<dsp:oparam name="true">
					<dsp:getvalueof var="editBillingAddress" value="false" scope="request"/>
		   		</dsp:oparam>
		   	</dsp:droplet>
			<dsp:droplet name="IsNull">
		   		<dsp:param name="value" value="${newCreditCard}" />
		   		<dsp:oparam name="false">
					<dsp:getvalueof var="newCreditCard" value="${newCreditCard}" scope="request"/>
		   		</dsp:oparam>
		   		<dsp:oparam name="true">
					<dsp:getvalueof var="newCreditCard" value="false" scope="request"/>
		   		</dsp:oparam>
		   	</dsp:droplet>
		   	<dsp:droplet name="IsEmpty">
		   		<dsp:param name="value" value="${editCreditCard}" />
		   		<dsp:oparam name="false">
					<dsp:getvalueof var="editCreditCard" value="${editCreditCard}" scope="request"/>
		   		</dsp:oparam>
		   		<dsp:oparam name="true">
					<dsp:getvalueof var="editCreditCard" value="false" scope="request"/>
		   		</dsp:oparam>
		   	</dsp:droplet>
			<dsp:getvalueof var="isRewardsCardValid" param="isRewardsCardValid" scope="request" />
			<dsp:getvalueof var="rewardNumber" param="rewardNumber" scope="request" />
			<dsp:getvalueof var="paymentGroupType" param="paymentGroupType" scope="request"/>
			<dsp:getvalueof var="ccNum" param="cBinNum" scope="request"/>
			<dsp:getvalueof var="creditCardNickName" param="selectedSavedCard" scope="request"/>
			<c:choose>
			      <c:when test="${orderSet eq true}">
			         <dsp:setvalue bean="BillingFormHandler.orderSetWithDefault" value="true"/>
			      </c:when>
			      <c:otherwise>
			         <dsp:setvalue bean="BillingFormHandler.orderSetWithDefault" value="false"/>
			      </c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test="${isRUSCard eq 'true'}">
					<dsp:setvalue bean="BillingFormHandler.RUSCard" value="true"/>
				</c:when>
				<c:otherwise>
					<dsp:setvalue bean="BillingFormHandler.RUSCard" value="false"/>
				</c:otherwise>
			</c:choose>
			<%--Start :Nullchk Fix for IRIS261 --%>
			<c:choose>
				<c:when test="${isNoFinanceAgreed eq 'true'}">
					<dsp:setvalue bean="BillingFormHandler.noFinanceAgreed" value="true"/>
				</c:when>
				<c:otherwise>
					<dsp:setvalue bean="BillingFormHandler.noFinanceAgreed" value="false"/>
				</c:otherwise>
			</c:choose>
			<%--End :Nullchk Fix for  IRIS261--%>			
			<dsp:setvalue bean="BillingFormHandler.paymentGroupType" paramvalue="paymentGroupType"/>
			<dsp:setvalue bean="BillingFormHandler.creditCardNickName" paramvalue="selectedSavedCard" />
			<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.nameOnCard" paramvalue="nameOnCard"/>
			<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardToken" value="${ccToken}"/>
			<c:choose>
				<c:when test="${not empty cBinNum}">
					<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardNumber" paramvalue="cBinNum"/>
				</c:when>
				<c:otherwise>
					<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardNumber" paramvalue="ccnumber"/>
				</c:otherwise>
			</c:choose>
			
			<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.cardLength" paramvalue="cLength"/>
			<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.cardType" value="${cardType}"/>
			<c:choose>
				<c:when test="${ccNum eq '604586' and isRUSCard ne 'true'}">
					<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.expirationMonth" value="${truConf.expMonthAdjustmentForPLCC}"/>
					<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.expirationYear" value="${truConf.expYearAdjustmentForPLCC}"/>
				</c:when>
				<c:otherwise>
					<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.expirationMonth" paramvalue="expirationMonth"/>
					<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.expirationYear" paramvalue="expirationYear"/>
				</c:otherwise>
			</c:choose>
			<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardVerificationNumber"  paramvalue="creditCardCVV"/>
			<%--Start: Added for adding rewards number to order  --%>
	        <dsp:setvalue bean="BillingFormHandler.rewardNumberValid" paramvalue="isRewardsCardValid" />
			<dsp:setvalue bean="BillingFormHandler.rewardNumber" paramvalue="rewardNumber" /> 
			<%--End: Added for adding rewards number to order  --%>
					<dsp:setvalue bean="BillingFormHandler.billingAddressNickName" paramvalue="nickName"/>
					<dsp:setvalue bean="BillingFormHandler.addressInputFields.firstName" paramvalue="firstName"/>
					<dsp:setvalue bean="BillingFormHandler.addressInputFields.lastName" paramvalue="lastName"/>
					<dsp:setvalue bean="BillingFormHandler.addressInputFields.address1" paramvalue="address1"/>
					<dsp:setvalue bean="BillingFormHandler.addressInputFields.address2" paramvalue="address2"/>
					<dsp:setvalue bean="BillingFormHandler.addressInputFields.city" paramvalue="city"/>
					<c:choose>
						<c:when test="${country ne 'US'}">
							<dsp:setvalue bean="BillingFormHandler.addressInputFields.state" paramvalue="otherState"/>
						</c:when>
						<c:otherwise>
							<dsp:setvalue bean="BillingFormHandler.addressInputFields.state" paramvalue="state"/>
						</c:otherwise>
					</c:choose>
					<dsp:setvalue bean="BillingFormHandler.addressInputFields.postalCode" paramvalue="postalCode"/>
					<dsp:setvalue bean="BillingFormHandler.addressInputFields.phoneNumber" paramvalue="phoneNumber"/>
					<dsp:setvalue bean="BillingFormHandler.addressInputFields.country" paramvalue="country"/>
					<dsp:setvalue bean="BillingFormHandler.addressValidated" paramvalue="addressValidated"/>
			<dsp:setvalue bean="BillingFormHandler.email" value="${orderEmail}"/>
			<dsp:setvalue bean="BillingFormHandler.newBillingAddress" value="${newBillingAddress}"/>
			<dsp:setvalue bean="BillingFormHandler.editBillingAddress" value="${editBillingAddress}"/>
			<dsp:setvalue bean="BillingFormHandler.newCreditCard" value="${newCreditCard}"/>
			<dsp:setvalue bean="BillingFormHandler.editCreditCard" value="${editCreditCard}"/>
			<dsp:setvalue bean="BillingFormHandler.editCardNickName" value="${editCardNickName}"/>
			<dsp:setvalue bean="BillingFormHandler.moveToOrderReview"  value="submit"/>
			<dsp:droplet name="/atg/dynamo/droplet/Switch">
	           <dsp:param bean="BillingFormHandler.formError" name="value" />
	           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	<dsp:getvalueof var="shipRestrictionsFound" bean="ShoppingCart.current.shipRestrictionsFound"/>
	             	Following are the form errors:
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <li><dsp:valueof param="message"/>
	                  	<input type="hidden" id="shipRestrictionsFoundTemp" value="${shipRestrictionsFound}"  />
	                  </li>
	                  	 <dsp:droplet name="/atg/commerce/order/purchase/RepriceOrderDroplet">
							<dsp:param value="ORDER_TOTAL" name="pricingOp" />
						</dsp:droplet>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
				   </dsp:oparam>
	           </dsp:droplet>
            </dsp:oparam>
            <dsp:oparam name="false">
			<dsp:droplet name="Switch">
				<dsp:param name="value" bean="BillingFormHandler.addressDoctorResponseVO.derivedStatus" />
				<dsp:oparam name="NO">
					address-saved-success-from-ajax:
				</dsp:oparam>
				<dsp:oparam name="YES">
					address-doctor-overlay-from-ajax:
					<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
						<dsp:param name="mode" value="paymentCard" />
					</dsp:include>
				</dsp:oparam>
				<%-- <dsp:oparam name="VALIDATION ERROR">
					address-doctor-overlay-from-ajax:
					<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
						<dsp:param name="mode" value="paymentCard" />
					</dsp:include>
				</dsp:oparam> --%>
				<dsp:oparam name="default"> 
					address-saved-success-from-ajax: 
				 </dsp:oparam>
			</dsp:droplet>
            </dsp:oparam>
         </dsp:droplet>
			
	</c:when>
	<c:when test="${formID eq 'changeBillingAddress'}">
		<dsp:setvalue bean="BillingFormHandler.billingAddressNickName" paramvalue="billingAddressNickName" />
		<dsp:setvalue bean="BillingFormHandler.changeBillingAddress" value="submit" />
	</c:when>
	<c:when test="${formID eq 'paymentType'}">
		<dsp:setvalue bean="BillingFormHandler.creditCardType" paramvalue="cardType"/>
		<dsp:setvalue bean="BillingFormHandler.giftCardsExist" paramvalue="giftCardsExist"/>
		<dsp:setvalue bean="BillingFormHandler.updateCardDataInOrder" value="true"/>
		<dsp:droplet name="Switch">
			<dsp:param bean="BillingFormHandler.formError" name="value" />
			<dsp:oparam name="true">
				<dsp:droplet name="ErrorMessageForEach">
					<dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
					<dsp:oparam name="outputStart">
						Following are the form errors:<br />
					</dsp:oparam>
					<dsp:oparam name="output">
						<li class="redColor"><dsp:valueof param="message" /></li>
					</dsp:oparam>
				</dsp:droplet>
			</dsp:oparam>
			<dsp:oparam name="false">
				<div>
					<div class="orderSummary">
						<dsp:include page="/checkout/common/checkoutOrderSummary.jsp" /> 
					</div>
					<div class="giftCardList">
						<dsp:include page="/checkout/payment/displayGiftCards.jsp" />
					</div>					
				</div>
			</dsp:oparam>
		</dsp:droplet>
	</c:when>
	<c:when test="${formID eq 'PayPalpaymentType'}">
			<dsp:setvalue bean="BillingFormHandler.updateDataInOrder" value="true"/>
			<dsp:droplet name="Switch">
				<dsp:param bean="BillingFormHandler.formError" name="value" />
				<dsp:oparam name="true">
					<dsp:droplet name="ErrorMessageForEach">
						<dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
						<dsp:oparam name="outputStart">
							Following are the form errors:<br />
						</dsp:oparam>
						<dsp:oparam name="output">
							<li class="redColor"><dsp:valueof param="message" /></li>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="false">
					<div>
						<div id="checkoutOrderSummaryResponse">
							<dsp:include page="/checkout/common/checkoutOrderSummary.jsp"/>
						</div>
						<div id="displayGiftCardsResponse">
							<dsp:getvalueof var="gcAppliedCount" param="gcAppliedCount"/>
							<c:if test="${gcAppliedCount gt 0}">
								<dsp:include page="/checkout/payment/displayGiftCards.jsp"/>
							</c:if>
						</div>
					</div>					 
				</dsp:oparam>
			</dsp:droplet>
	</c:when>
	<c:when test="${formID eq 'electronicDisclosureForm'}">
			<dsp:getvalueof var="cardinalToken" param="cardinalToken" />
			<dsp:getvalueof var="expirationMonth" param="expirationMonth"/>
		<dsp:getvalueof var="expirationYear" param="expirationYear"/>
		<dsp:getvalueof var="cardCode" param="cardCode"/>
		<dsp:getvalueof var="nameOnCard" param="nameOnCard"/>
		<dsp:getvalueof var="cBinNum" param="cBinNum"/>
		<dsp:getvalueof var="cardLength" param="cardLength"/>
		<dsp:getvalueof var="isSavedCard" param="isSavedCard"/>
		<dsp:getvalueof var="paymentFromRusCard" value="true" vartype="java.lang.boolean"/>
		
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.nameOnCard"value="${nameOnCard}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardToken" value="${cardinalToken}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardNumber" value="${cBinNum}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.expirationMonth" value="${expirationMonth}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.expirationYear" value="${expirationYear}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.cardLength" value="${cardLength}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardVerificationNumber" value="${cardCode}"/>
		<dsp:setvalue bean="BillingFormHandler.RUSCard" value="${paymentFromRusCard}"/>
		<dsp:setvalue bean="BillingFormHandler.savedCard" value="${isSavedCard}"/>
		
		<dsp:setvalue bean="BillingFormHandler.financeAgreed" value="true"/>
		<dsp:setvalue bean="BillingFormHandler.financingTermsAvailed" value="${order.financingTermsAvailable}"/>
		
		<dsp:setvalue bean="BillingFormHandler.updateFinanceAgreedToOrder" value="submit"/>
		
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="BillingFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="errorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
           <dsp:oparam name="false">
           		<div>
					<div class="disclosureAgreed">agreed</div>
					<div class="percentage-valuation-from-ajax"><dsp:valueof bean="BillingFormHandler.percentageValuation"/></div>
				</div>
           </dsp:oparam>
         </dsp:droplet>
		
	</c:when>
	<c:when test="${formID eq 'checkForOrderFinancing'}">
		<dsp:setvalue bean="BillingFormHandler.updateSynchronyDataInOrder" value="submit" />
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="BillingFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="errorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
           </dsp:droplet>
	</c:when>
	<c:when test="${formID eq 'promoFinancingOptions'}">
		<dsp:getvalueof var="country" param="country" scope="request" />
		<dsp:getvalueof var="expirationMonth" param="expirationMonth"/>
		<dsp:getvalueof var="expirationYear" param="expirationYear"/>
		<dsp:getvalueof var="cardCode" param="cardCode"/>
		<dsp:getvalueof var="nameOnCard" param="nameOnCard"/>
		<dsp:getvalueof var="cBinNum" param="cBinNum"/>
		<dsp:getvalueof var="cardinalToken" param="cardinalToken" />
		<dsp:getvalueof var="cardType" param="rusCardType" />
		
		<dsp:getvalueof var="paymentFromRusCard" value="true" vartype="java.lang.boolean"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.nameOnCard"value="${nameOnCard}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardNumber" value="${cBinNum}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardToken" value="${cardinalToken}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.expirationMonth" value="${expirationMonth}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.expirationYear" value="${expirationYear}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardType" value="${cardType}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardVerificationNumber" value="${cardCode}"/>
		<dsp:setvalue bean="BillingFormHandler.RUSCard" value="${paymentFromRusCard}"/>
		<dsp:setvalue bean="BillingFormHandler.financingTermsAvailed" value="${order.financingTermsAvailable}"/>
		
		<dsp:setvalue bean="BillingFormHandler.billingAddressNickName" paramvalue="nickName"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.firstName" paramvalue="firstName"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.lastName" paramvalue="lastName"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.address1" paramvalue="address1"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.address2" paramvalue="address2"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.city" paramvalue="city"/>
		<c:choose>
			<c:when test="${country ne 'US'}">
				<dsp:setvalue bean="BillingFormHandler.addressInputFields.state" paramvalue="otherState"/>
			</c:when>
			<c:otherwise>
				<dsp:setvalue bean="BillingFormHandler.addressInputFields.state" paramvalue="state"/>
			</c:otherwise>
		</c:choose>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.postalCode" paramvalue="postalCode"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.phoneNumber" paramvalue="phoneNumber"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.country" paramvalue="country"/>
		<dsp:setvalue bean="BillingFormHandler.addressValidated" paramvalue="addressValidated"/>
		<dsp:getvalueof var="newBillingAddress" param="newBillingAddress" scope="request" />
		<dsp:getvalueof var="editBillingAddress" param="editBillingAddress" scope="request" />
		<dsp:droplet name="IsNull">
	   		<dsp:param name="value" value="${newBillingAddress}" />
	   		<dsp:oparam name="false">
				<dsp:getvalueof var="newBillingAddress" value="${newBillingAddress}" scope="request"/>
	   		</dsp:oparam>
	   		<dsp:oparam name="true">
				<dsp:getvalueof var="newBillingAddress" value="false" scope="request"/>
	   		</dsp:oparam>
	   	</dsp:droplet>
	   	<dsp:droplet name="IsNull">
	   		<dsp:param name="value" value="${editBillingAddress}" />
	   		<dsp:oparam name="false">
				<dsp:getvalueof var="editBillingAddress" value="${editBillingAddress}" scope="request"/>
	   		</dsp:oparam>
	   		<dsp:oparam name="true">
				<dsp:getvalueof var="editBillingAddress" value="false" scope="request"/>
	   		</dsp:oparam>
	   	</dsp:droplet>
		<dsp:setvalue bean="BillingFormHandler.newBillingAddress" value="${newBillingAddress}"/>
		<dsp:setvalue bean="BillingFormHandler.editBillingAddress" value="${editBillingAddress}"/>
		<dsp:setvalue bean="BillingFormHandler.populateFinancingDetails" value="submit"/>
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="BillingFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="errorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
           <dsp:oparam name="false">
           <dsp:getvalueof bean="BillingFormHandler.promoResponseCode" var="promoResponseCode"/>
           <dsp:getvalueof bean="BillingFormHandler.promoText" var="promoText"/>
           <dsp:getvalueof bean="BillingFormHandler.promoFinanceRate" var="promoFinanceRate"/>
           <dsp:getvalueof var="promoVariance" bean="BillingFormHandler.promoText"/>
           <c:set var="promoFinanceRate"><c:if test="${promoFinanceRate.matches('[0-9]+.[0-9]+')}"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2">${promoFinanceRate}</fmt:formatNumber></c:if></c:set>
           
           <c:choose>
           <c:when test="${not empty promoFinanceRate}">
           <c:if test="${fn:containsIgnoreCase(promoVariance,'Fixed')}">
           		<c:set var="promoBVariance" value=" at an "/>
           		<c:set var="varianceDetails" value="Please see any promotional advertising or other disclosures provided to you for the full terms of the promotion offered"/>
           </c:if>
           <c:if test="${fn:containsIgnoreCase(promoVariance,'Variable')}"> 
          	 	<c:set var="promoBVariance" value=" at a variable "/>
          	 	<c:set var="varianceDetails" value=" Apr will vary on the market based on the Prime rate . Please see any promotional advertising or other disclosures provided to you for the full terms of the promotion offered"/>
           </c:if>
           </c:when>
          <c:when test="${empty promoFinanceRate}">
           <c:set var="varianceDetails" value="at the APR that currently applies to new purchases to your account."/>
           </c:when>
           </c:choose>
           <c:if test="${fn:containsIgnoreCase(promoVariance,'Auth Stand-in')}">
           		<c:set var="promoAuthStandIn" value="Discounts applied to your purchase may result in you not meeting the minimum purchase requirement for special Financing."/>
           </c:if>
           {"promoResponseCode":"${promoResponseCode}","promoText":"${promoText}","promoFinanceRate":"${promoFinanceRate}","financingTermsAvailable":"${financingTermsAvailable}","promoBVariance":"${promoBVariance}","promoAuthStandIn":"${promoAuthStandIn}","varianceDetails":"${varianceDetails}"}
          <%-- <div><dsp:valueof bean="BillingFormHandler.promoResponseCode"/>
           <div id="p_response">Fail</div>
           <div id="p_text"><dsp:valueof bean="BillingFormHandler.promoText"/></div>
           <div id="p_rate"><dsp:valueof bean="BillingFormHandler.promoFinanceRate"/></div>
           <div id="p_finance_plan">${financingTermsAvailable}</div>
           <dsp:getvalueof var="promoVariance" bean="BillingFormHandler.promoText"/>
           <c:if test="${promoVariance eq 'Fixed'}">
           <div id="promo_variance">[an]</div>
           </c:if>
           <c:if test="${promoVariance eq 'Variable'}">
           <div id="promo_variance">at a variable</div>
           </c:if>
           <c:if test="${promoVariance eq 'Auth Stand In'}">
           <div id="promo_auth_standIn">Discounts applied to your purchase may result in you not meeting the minimum purchase requirement for special Financing.</div>
           </c:if>
           </div> --%>
           </dsp:oparam>
           </dsp:droplet>
	</c:when>
	<c:when test="${formID eq 'validateCreditCarBinRanges'}">
	  	<dsp:getvalueof var="cardNumber" param="cardNumber" scope="request" />
		<dsp:setvalue bean="BillingFormHandler.validateCreditCardBinRanges" value="submit" />
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="BillingFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="errorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
           </dsp:droplet>
	</c:when>
</c:choose>

	<dsp:droplet name="Switch">
	<dsp:param name="value" param="action"/>
		<dsp:oparam name="ensurePaymentGroup">
				<dsp:setvalue bean="BillingFormHandler.RUSCard" paramvalue="rusCardSelected"/>
				<dsp:setvalue bean="BillingFormHandler.paymentGroupType" paramvalue="paymentGroupType"/>
				<dsp:setvalue bean="BillingFormHandler.ensurePaymentGroup" value="submit" />
				<div>
					<div id="checkoutOrderSummaryResponse">
						<dsp:include page="/checkout/common/checkoutOrderSummary.jsp"/>
					</div>
					<div id="displayGiftCardsResponse">
						<dsp:getvalueof var="gcAppliedCount" param="gcAppliedCount"/>
						<c:if test="${gcAppliedCount gt 0}">
							<dsp:include page="/checkout/payment/displayGiftCards.jsp"/>
						</c:if>
					</div>
					<div id="orderCreditCardExpired">
						<dsp:droplet name="Switch">
					       <dsp:param name="value" bean="BillingFormHandler.orderCreditCardExpired" />
					       <dsp:oparam name="true">
								<dsp:droplet name="ErrorMessageForEach">
									<dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
								<dsp:oparam name="outputStart">
									Following are the form errors: <span class="errorDisplay">
								</dsp:oparam>
								<dsp:oparam name="output">
									<span><dsp:valueof param="message" valueishtml="true"/></span>
								</dsp:oparam>
								<dsp:oparam name="outputEnd">
									</span>
								</dsp:oparam>
							</dsp:droplet>
							</dsp:oparam>
						</dsp:droplet>
					</div>
					<div id="radioBtnPaymentCreditCardSection">
						<dsp:getvalueof param="isRadioCreditcardClicked" var="isRadioCreditcardClicked"/>
						<c:if test="${isRadioCreditcardClicked eq 'true'}">
							<dsp:include page="/checkout/payment/creditCardForm.jsp">
								<dsp:param name="isRadioCreditcardClicked" value="${isRadioCreditcardClicked}"/>
							</dsp:include>
						</c:if>
					</div>
				</div>
			</dsp:oparam>
		</dsp:droplet>
</dsp:page>