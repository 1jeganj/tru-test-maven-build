<dsp:page>

<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/CheckoutProfileFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/atg/dynamo/droplet/IsNull" />
<dsp:importbean bean="/atg/commerce/promotion/CouponFormHandler" />
<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUGetSynchronyUrlDroplet" />
<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUParseSynchronyResponseDroplet" />
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
<dsp:getvalueof var="order" bean="ShoppingCart.current" />
<dsp:getvalueof var="truConf" bean="TRUConfiguration" />
<dsp:getvalueof var="formID" param="formID"></dsp:getvalueof>
<dsp:getvalueof var="fromPageName" param="fromPageName"></dsp:getvalueof>
<dsp:getvalueof var="action" param="action"></dsp:getvalueof>
<dsp:getvalueof var="financingTermsAvailable" value="${order.financingTermsAvailable}"/>

	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler" />
	<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftOptionsFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/com/tru/commerce/order/purchase/InStorePickUpFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler"/>

 <c:choose>
	<c:when test="${formID eq 'confirmSignUpForm'}">
		<dsp:setvalue bean="CheckoutProfileFormHandler.value.login" paramvalue="email"/>
		<dsp:setvalue bean="CheckoutProfileFormHandler.value.email" paramvalue="email"/>
		<dsp:setvalue bean="CheckoutProfileFormHandler.value.password" paramvalue="oc_password"/>
		<dsp:setvalue bean="CheckoutProfileFormHandler.value.confirmPassword" paramvalue="confirm_password"/>
		<dsp:setvalue bean="CheckoutProfileFormHandler.newCustomer" value="submit"/>

			<dsp:droplet name="Switch">
				<dsp:param bean="CheckoutProfileFormHandler.formError" name="value" />
				<dsp:oparam name="true">
					<dsp:droplet name="ErrorMessageForEach">
						<dsp:param name="exceptions" bean="CheckoutProfileFormHandler.formExceptions" />
						<dsp:oparam name="outputStart">
							Following are the form errors:
						</dsp:oparam>
						<dsp:oparam name="output">
							<dsp:valueof param="message" />
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="false">
					 <dsp:include page="/checkout/common/checkoutOrderSummary.jsp" /> 
				</dsp:oparam>
			</dsp:droplet>
	</c:when>
	<c:when test="${formID eq 'emailSignUp'}">
		<dsp:getvalueof param="email" var="email"/>
		<dsp:include page="/myaccount/epslonEmailSignUp.jsp?email=${email}" />
	</c:when>
		<c:when test="${formID eq 'applyCouponFromPayment'}">
	    <dsp:getvalueof var="couponCode" param="couponCode"></dsp:getvalueof>
        <dsp:setvalue bean="CouponFormHandler.couponClaimCode" value="${couponCode}"/>
        <dsp:setvalue bean="CouponFormHandler.paymentPage" value="true"/>
		<dsp:setvalue bean="CouponFormHandler.claimCoupon" value="submit" />
        	<dsp:droplet name="Switch">
				<dsp:param bean="CouponFormHandler.formError" name="value" />
				<dsp:oparam name="true">
					<dsp:droplet name="ErrorMessageForEach">
						<dsp:param name="exceptions" bean="CouponFormHandler.formExceptions" />
						<dsp:oparam name="outputStart">
							Following are the form errors:
						</dsp:oparam>
						<dsp:oparam name="output">
							<dsp:valueof param="message" />
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="false">
					<dsp:include page="/checkout/payment/promotionCodes.jsp"></dsp:include>
				</dsp:oparam>
			</dsp:droplet>
   </c:when>
   <c:when test="${formID eq 'removeCouponFromPayment'}">
	    <dsp:getvalueof param="couponId" var="couponId"></dsp:getvalueof>
		<dsp:setvalue bean="CouponFormHandler.couponClaimCode" value="${couponId}" />
		<dsp:setvalue bean="CouponFormHandler.paymentPage" value="true"/>
		<dsp:setvalue bean="CouponFormHandler.removeCoupon" value="submit" />
        <dsp:include page="/checkout/payment/promotionCodes.jsp"></dsp:include>
   </c:when>
</c:choose>

	
	
	<dsp:droplet name="Switch">
		<dsp:param name="value" param="action"/>
		<dsp:oparam name="updateGiftOptions">
			<dsp:setvalue bean="GiftOptionsFormHandler.showMeGiftingOptions" paramvalue="showMeGiftingOptions"/>
			<dsp:setvalue bean="GiftOptionsFormHandler.updateGiftOptions" value="true" />
		</dsp:oparam>
		<dsp:oparam name="commitOrder">
      		<dsp:setvalue bean="CommitOrderFormHandler.commitOrderErrorURL" value="/checkout/review/orderReview.jsp"/>
      		<dsp:setvalue bean="CommitOrderFormHandler.commitOrderSuccessURL" value="/checkout/confirm/orderConfirmation.jsp"/>
			<dsp:setvalue bean="CommitOrderFormHandler.commitOrder" value="true" />
		</dsp:oparam>
		<dsp:oparam name="review_applyGiftMessage">
			<dsp:setvalue bean="GiftOptionsFormHandler.giftOptionsInfoCount" value="1" />
			<dsp:setvalue bean="GiftOptionsFormHandler.giftOptionsInfo[0].shippingGroupId" paramvalue="shipGroupId"/>
			<dsp:setvalue bean="GiftOptionsFormHandler.giftOptionsInfo[0].giftReceipt" value="true"/>
			<dsp:setvalue bean="GiftOptionsFormHandler.giftOptionsInfo[0].giftWrapMessage" paramvalue="giftMessage"/>
			<dsp:setvalue bean="GiftOptionsFormHandler.applyGiftMessage" value="true" />
		</dsp:oparam>
		
		<%--Start : Added to handle updating the ship methods zone on change of address1 & state--%>
		<dsp:oparam name="changeShipMethodZone">
			<dsp:setvalue bean="ShippingGroupFormHandler.state" paramvalue="state"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.address1" paramvalue="address1" />
			<dsp:setvalue bean="ShippingGroupFormHandler.city" paramvalue="city" />
			<dsp:setvalue bean="ShippingGroupFormHandler.changeShipMethodZone" value="true" />
		</dsp:oparam>
		<%--End : Added to handle updating the ship methods zone on change of address1 & state--%>
		
		<dsp:oparam name="changeShippingAddress">
			<dsp:setvalue bean="ShippingGroupFormHandler.shipToAddressName" paramvalue="shipToAddressName"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shippingMethod" paramvalue="shippingMethod" />
			<dsp:setvalue bean="ShippingGroupFormHandler.shippingPrice" paramvalue="shippingPrice"/>				
			<dsp:setvalue bean="ShippingGroupFormHandler.changeShippingAddress" value="true" />
		</dsp:oparam>
		<dsp:oparam name="shipToExistingAddress">
			<dsp:getvalueof var="moveToShippingAction" param="moveToShippingAction" />
			<c:if test="${not empty moveToShippingAction && moveToShippingAction eq 'moveToMultipleShip'}">
				<dsp:setvalue bean="ShippingGroupFormHandler.navigateToMultiShip" value="true" />
				<dsp:setvalue bean="ShippingGroupFormHandler.skipShippingRestrctions" value="true"/>
			</c:if>
			<dsp:setvalue bean="ShippingGroupFormHandler.showMeGiftingOptions" paramvalue="showMeGiftingOptions"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shipToAddressName" paramvalue="shipToAddressName"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shippingMethod" paramvalue="shippingMethod"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shippingPrice" paramvalue="shippingPrice"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shipToExistingAddress" value="true" />
		</dsp:oparam>
		<dsp:oparam name="shipToMultiAddress">			
			<dsp:setvalue bean="ShippingGroupFormHandler.shipToMultiAddress" value="true" />
		</dsp:oparam>
		<dsp:oparam name="changeShipMethodInReview">
			<dsp:setvalue bean="ShippingGroupFormHandler.estimateShipWindowMin" paramvalue="estimateShipWindowMin"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.estimateShipWindowMax" paramvalue="estimateShipWindowMax"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shipToAddressName" paramvalue="shipToAddressName"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shippingMethod" paramvalue="shippingMethod"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.eligibleShipMethodsCount" paramvalue="eligibleShipMethodsCount"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.selectedIndex" paramvalue="selectedIndex"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.changeShipMethodInReview" value="true" />
		</dsp:oparam>
		<dsp:oparam name="updateShipMethod">
			<dsp:setvalue bean="ShippingGroupFormHandler.shippingGroupId" paramvalue="shippingGroupId"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.relationShipId" paramvalue="relationShipId"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shipMethodVO.shipMethodCode" paramvalue="shipMethodCode"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shipMethodVO.shippingPrice" paramvalue="shippingPrice"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shipMethodVO.regionCode" paramvalue="regionCode"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.uniqueId" paramvalue="uniqueId"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.wrapCommerceItemId" paramvalue="wrapCommerceItemId"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.updateShipMethod" value="true" />
		</dsp:oparam>		
		<dsp:oparam name="editCartItemInReview">
			<dsp:getvalueof var="productId" param="productId" />
			<dsp:getvalueof var="quantity" param="quantity" />
			<dsp:getvalueof var="pageName" param="pageName" />
			<dsp:getvalueof  var="skuId" param="skuId" />
			<dsp:getvalueof  var="commItemId" param="updateCommerItemIds" />
			<dsp:getvalueof  var="relationShipId" param="relationShipId" />
			<dsp:getvalueof  var="editCartFromPage" param="editCartFromPage" />
			<dsp:setvalue bean="CartModifierFormHandler.productId" value="${productId}" />
			<c:set var="catalogRefIds" value="${fn:split(skuId, ' ')}" />
			<c:set var="commItemIds" value="${fn:split(commItemId, ' ')}" />
			<dsp:setvalue bean="CartModifierFormHandler.catalogRefIds" value="${catalogRefIds}" />
			<dsp:setvalue bean="CartModifierFormHandler.pageName" value="${pageName}" />
			<dsp:setvalue bean="CartModifierFormHandler.quantity" value="${quantity}" />
			<dsp:setvalue bean="CartModifierFormHandler.updateCommerItemIds" value="${commItemIds}"/>
			<dsp:setvalue bean="CartModifierFormHandler.relationShipId" value="${relationShipId}" />
			<dsp:setvalue bean="CartModifierFormHandler.giftWrapSkuId" paramvalue="wrapSkuId" />
			<dsp:setvalue bean="CartModifierFormHandler.editCartFromPage" value="${editCartFromPage}"/>
            <dsp:setvalue bean="CartModifierFormHandler.updateCommerceItem" value="update"/>
            
            
            <c:set value="" var="existingRelationShipId"/>
            <dsp:droplet name="Switch">
				<dsp:param bean="CartModifierFormHandler.formError" name="value" />
				<dsp:oparam name="true">
				   <c:set value="${relationShipId}" var="existingRelationShipId"/>
					<dsp:droplet name="ErrorMessageForEach">
						<dsp:param name="exceptions" bean="CartModifierFormHandler.formExceptions" />
						<dsp:oparam name="outputStart">
						Following are the form errors:
						<div class="edit-review-tab-error">
							<input type="hidden" id="checkoutShipPageRelationShipId" value="${relationShipId}" />
							<div class="shopping-cart-error-state">
							<div class="shopping-cart-error-state-header row">
								<div class="error-state-exclamation-lg img-responsive col-md-2"></div>
								<div class="shopping-cart-error-state-header-text col-md-10">
									<fmt:message key="shoppingcart.oops.issue" />
									<div class="shopping-cart-error-state-subheader">
						</dsp:oparam>
						<dsp:oparam name="output">
							<dsp:valueof param="message" valueishtml="true" />
							<dsp:droplet name="Switch">
								<dsp:param param="errorCode" name="value" />
								<dsp:oparam name="inline_msg">
									<dsp:getvalueof var="inlineMessage" param="message"/>
								</dsp:oparam>
							</dsp:droplet>
						
						</dsp:oparam>
						<dsp:oparam name="outputEnd">
							</div>
							</div>
							</div>
							</div>
							</div>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="removeCartItemInReview">
			<dsp:droplet name="Switch">
				<dsp:param param="updateOrRemove" name="value" />
				<dsp:oparam name="update">
				 	<dsp:getvalueof var="relationShipId" param="commerceItemId" />
					<dsp:setvalue bean="CartModifierFormHandler.splitItemRemove" value="true" />
					<dsp:setvalue bean="CartModifierFormHandler.relationShipId"	paramvalue="commerceItemId" />
					<dsp:setvalue bean="CartModifierFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
					<dsp:setvalue bean="CartModifierFormHandler.wrapItemQuantity" paramvalue="quantity" />
					<dsp:setvalue bean="CartModifierFormHandler.editCartFromPage" value="Review"/>
					<dsp:setvalue bean="CartModifierFormHandler.setOrderByRelationshipId" value="true" />
				</dsp:oparam>
				<dsp:oparam name="remove">
					<dsp:getvalueof param="commerceItemId" var="commerceItemId" />
					<c:set var="commerceItemIdArray" value="${fn:split(commerceItemId, ' ')}" />
					<dsp:setvalue bean="CartModifierFormHandler.relationShipId"	paramvalue="commerceItemId" />
					<dsp:setvalue bean="CartModifierFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
					<dsp:setvalue bean="CartModifierFormHandler.wrapItemQuantity" paramvalue="quantity" />
					<dsp:setvalue bean="CartModifierFormHandler.removalRelationshipIds"	value="${commerceItemIdArray}" />
					<dsp:setvalue bean="CartModifierFormHandler.editCartFromPage" value="Review"/>
					<dsp:setvalue bean="CartModifierFormHandler.removeItemFromOrderByRelationshipId" value="submit" />
				</dsp:oparam>
				<dsp:oparam name="outOfStock">
					<dsp:setvalue bean="CartModifierFormHandler.outOfStockId" paramvalue="commerceItemId" />
					<dsp:setvalue bean="CartModifierFormHandler.editCartFromPage" value="Review"/>
					<dsp:setvalue bean="CartModifierFormHandler.removeOutOfStockItem" value="submit" />
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="removeCartItemInShipping">
		 <dsp:droplet name="Switch">
				<dsp:param param="updateOrRemove" name="value" />
				<dsp:oparam name="update">
				 	<dsp:getvalueof var="relationShipId" param="commerceItemId" />
					<dsp:setvalue bean="CartModifierFormHandler.splitItemRemove" value="true" />
					<dsp:setvalue bean="CartModifierFormHandler.relationShipId"	paramvalue="commerceItemId" />
					<dsp:setvalue bean="CartModifierFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
					<dsp:setvalue bean="CartModifierFormHandler.wrapItemQuantity" value="1" />
					<dsp:setvalue bean="CartModifierFormHandler.setOrderByRelationshipId" value="true" />
				</dsp:oparam>
				<dsp:oparam name="remove"> 
					<dsp:getvalueof param="commerceItemId" var="commerceItemId" />
					<c:set var="commerceItemIdArray" value="${fn:split(commerceItemId, ' ')}" />
					<dsp:setvalue bean="CartModifierFormHandler.relationShipId"	paramvalue="commerceItemId" />
					<dsp:setvalue bean="CartModifierFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
					<dsp:setvalue bean="CartModifierFormHandler.wrapItemQuantity" value="1" />
					<dsp:setvalue bean="CartModifierFormHandler.removalRelationshipIds"	value="${commerceItemIdArray}" />
					<dsp:setvalue bean="CartModifierFormHandler.removeItemFromOrderByRelationshipId" value="submit" />
				 </dsp:oparam>
			</dsp:droplet> 
		 	<%-- <dsp:getvalueof var="relationShipId" param="commerceItemId" />
			<dsp:setvalue bean="CartModifierFormHandler.splitItemRemove" value="true" />
			<dsp:setvalue bean="CartModifierFormHandler.relationShipId"	paramvalue="commerceItemId" />
			<dsp:setvalue bean="CartModifierFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
			<dsp:setvalue bean="CartModifierFormHandler.wrapItemQuantity" paramvalue="quantity" />
			<dsp:setvalue bean="CartModifierFormHandler.setOrderByRelationshipId" value="true" /> --%>
		</dsp:oparam>
		<dsp:oparam name="updateGiftItem">
			<dsp:setvalue bean="GiftOptionsFormHandler.shipGroupId"	paramvalue="shipGroupId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.commerceItemRelationshipId" paramvalue="commerceItemRelationshipId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.commerceItemId" paramvalue="commerceItemId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.quantity" value="1" />
			<dsp:setvalue bean="GiftOptionsFormHandler.giftWrapProductId" paramvalue="giftWrapProductId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.giftItem" paramvalue="giftItem" />
			<dsp:setvalue bean="GiftOptionsFormHandler.selectedId" paramvalue="selectedId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.currentPage" value="gifting" />
			<dsp:setvalue bean="GiftOptionsFormHandler.updateGiftItem" value="true" />
		</dsp:oparam>
		<dsp:oparam name="applyGiftWrap">
			<dsp:setvalue bean="GiftOptionsFormHandler.shipGroupId" paramvalue="shipGroupId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.commerceItemRelationshipId"	paramvalue="commerceItemRelationshipId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.selectedId" paramvalue="selectedId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.commerceItemId" paramvalue="commerceItemId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.giftWrapProductId" paramvalue="giftWrapProductId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.previousGiftWrapSkuId" paramvalue="previousGiftWrapSkuId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.giftWrapSelected" paramvalue="giftWrapSelected" />
			
			<dsp:setvalue bean="GiftOptionsFormHandler.applyGiftWrap" value="true" />
		</dsp:oparam>
		<dsp:oparam name="applySingleShipping">
			<dsp:setvalue bean="ShippingGroupFormHandler.applySingleShipping" value="submit"/>
		</dsp:oparam>
		<dsp:oparam name="selectExistingAddress">
			<dsp:setvalue bean="ShippingGroupFormHandler.shippingGroupId" paramvalue="shippingGroupId"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.relationShipId" paramvalue="relationShipId"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shipToAddressName" paramvalue="shipToAddressName"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.uniqueId" paramvalue="uniqueId"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.wrapCommerceItemId" paramvalue="wrapCommerceItemId"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.selectExistingAddress" value="submit"/>
		</dsp:oparam>
		<dsp:oparam name="instore">
			<dsp:setvalue bean="InStorePickUpFormHandler.moveToPaymentInfo" value="submit"/>
		</dsp:oparam>
		<dsp:oparam name="removeDonationItemInReview">
			<dsp:getvalueof param="commerceItemId" var="commerceItemId"></dsp:getvalueof>
			<c:set var="commerceItemIdArray" value="${fn:split(commerceItemId, ' ')}" />
			<dsp:setvalue bean="CartModifierFormHandler.removalCommerceIds" value="${commerceItemIdArray}" />
			<dsp:setvalue bean="CartModifierFormHandler.removeItemFromOrder" value="submit" />
		</dsp:oparam>
		<dsp:oparam name="commitOrderFromReview">
			<dsp:setvalue bean="CommitOrderFormHandler.pageName" value="Review"/>
			<%-- <dsp:setvalue bean="CommitOrderFormHandler.email" bean="ShoppingCart.current.email"/> --%>
			<%--Start: Added for adding rewards number to order  --%>
	        <dsp:setvalue bean="CommitOrderFormHandler.rewardNumberValid" paramvalue="isRewardsCardValid" />
			<dsp:setvalue bean="CommitOrderFormHandler.rewardNumber" paramvalue="rewardNumber" />
			<dsp:setvalue bean="CommitOrderFormHandler.deviceID" paramvalue="deviceID" /> 
			<dsp:setvalue bean="CommitOrderFormHandler.browserAcceptEncoding" paramvalue="browserAcceptEncoding" /> 
			<%--End: Added for adding rewards number to order  --%>
		<%-- 	<dsp:getvalueof var="respJwt" param="respJwt" scope="request" />
			<dsp:setvalue bean="CommitOrderFormHandler.creditCardInfoMap.cardinalJwt" value="${respJwt}"/>		 --%>				
			<dsp:setvalue bean="CommitOrderFormHandler.commitOrder" value="submit" />
		</dsp:oparam>
		<dsp:oparam name="ensurePaymentGroup">
			<dsp:setvalue bean="BillingFormHandler.RUSCard" paramvalue="rusCardSelected"/>
			<dsp:setvalue bean="BillingFormHandler.paymentGroupType" paramvalue="paymentGroupType"/>
			<dsp:setvalue bean="BillingFormHandler.ensurePaymentGroup" value="submit" />
			<div>
				<div id="checkoutOrderSummaryResponse">
					<dsp:include page="/checkout/common/checkoutOrderSummary.jsp"/>
				</div>
				<div id="displayGiftCardsResponse">
					<dsp:getvalueof var="gcAppliedCount" param="gcAppliedCount"/>
					<c:if test="${gcAppliedCount gt 0}">
						<dsp:include page="/checkout/payment/displayGiftCards.jsp"/>
					</c:if>
				</div>
				<div id="orderCreditCardExpired">
					<dsp:droplet name="Switch">
				       <dsp:param name="value" bean="BillingFormHandler.orderCreditCardExpired" />
				       <dsp:oparam name="true">
							<dsp:droplet name="ErrorMessageForEach">
								<dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
							<dsp:oparam name="outputStart">
								Following are the form errors: <span class="errorDisplay">
							</dsp:oparam>
							<dsp:oparam name="output">
								<span><dsp:valueof param="message" valueishtml="true"/></span>
							</dsp:oparam>
							<dsp:oparam name="outputEnd">
								</span>
							</dsp:oparam>
						</dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>
				</div>
				<div id="radioBtnPaymentCreditCardSection">
					<dsp:getvalueof param="isRadioCreditcardClicked" var="isRadioCreditcardClicked"/>
					<c:if test="${isRadioCreditcardClicked eq 'true'}">
						<dsp:include page="/checkout/payment/creditCardForm.jsp">
							<dsp:param name="isRadioCreditcardClicked" value="${isRadioCreditcardClicked}"/>
						</dsp:include>
					</c:if>
				</div>
			</div>
		</dsp:oparam>
		<dsp:oparam name="closeAddressModel">
			<dsp:setvalue bean="ShippingGroupFormHandler.closeAddressModel" value="true" />
		</dsp:oparam>
		<dsp:oparam name="closeBillingAddressModel">
		</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="action" param="action"/>
	<c:choose>
		<c:when test="${action eq 'instore'}">
			<dsp:droplet name="Switch">
		         <dsp:param bean="InStorePickUpFormHandler.formError" name="value" />
		         <dsp:oparam name="true">
					<dsp:droplet name="ErrorMessageForEach">
						<dsp:param name="exceptions"
							bean="InStorePickUpFormHandler.formExceptions" />
						<dsp:oparam name="outputStart">
							Following are the form errors:
						</dsp:oparam>
						<dsp:oparam name="output">
							<dsp:valueof param="message" valueishtml="true"/>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="false">
					{"success": true}
				</dsp:oparam>
			</dsp:droplet>
		</c:when>
	</c:choose>
</dsp:page>