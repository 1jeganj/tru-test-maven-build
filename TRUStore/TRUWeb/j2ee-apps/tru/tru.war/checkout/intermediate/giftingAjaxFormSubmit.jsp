<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
	<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftOptionsFormHandler"/>
	<dsp:droplet name="Switch">
		<dsp:param name="value" param="action"/>
		<dsp:oparam name="updateGiftOptions">
			<dsp:setvalue bean="GiftOptionsFormHandler.showMeGiftingOptions" paramvalue="showMeGiftingOptions"/>
			<dsp:setvalue bean="GiftOptionsFormHandler.updateGiftOptions" value="true" />
		</dsp:oparam>
		<dsp:oparam name="review_applyGiftMessage">
			<dsp:setvalue bean="GiftOptionsFormHandler.giftOptionsInfoCount" value="1" />
			<dsp:setvalue bean="GiftOptionsFormHandler.giftOptionsInfo[0].shippingGroupId" paramvalue="shipGroupId"/>
			<dsp:setvalue bean="GiftOptionsFormHandler.giftOptionsInfo[0].giftReceipt" value="true"/>
			<dsp:setvalue bean="GiftOptionsFormHandler.giftOptionsInfo[0].giftWrapMessage" paramvalue="giftMessage"/>
			<dsp:setvalue bean="GiftOptionsFormHandler.applyGiftMessage" value="true" />
		</dsp:oparam>
		<dsp:oparam name="updateGiftItem">
			<dsp:setvalue bean="GiftOptionsFormHandler.shipGroupId"	paramvalue="shipGroupId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.commerceItemRelationshipId" paramvalue="commerceItemRelationshipId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.commerceItemId" paramvalue="commerceItemId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.quantity" value="1" />
			<dsp:setvalue bean="GiftOptionsFormHandler.giftWrapProductId" paramvalue="giftWrapProductId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.giftItem" paramvalue="giftItem" />
			<dsp:setvalue bean="GiftOptionsFormHandler.selectedId" paramvalue="selectedId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.currentPage" value="gifting" />
			<dsp:setvalue bean="GiftOptionsFormHandler.updateGiftItem" value="true" />
		</dsp:oparam>
		<dsp:oparam name="applyGiftWrap">
			<dsp:setvalue bean="GiftOptionsFormHandler.shipGroupId" paramvalue="shipGroupId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.commerceItemRelationshipId"	paramvalue="commerceItemRelationshipId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.selectedId" paramvalue="selectedId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.commerceItemId" paramvalue="commerceItemId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.giftWrapProductId" paramvalue="giftWrapProductId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.previousGiftWrapSkuId" paramvalue="previousGiftWrapSkuId" />
			<dsp:setvalue bean="GiftOptionsFormHandler.giftWrapSelected" paramvalue="giftWrapSelected" />
			<dsp:setvalue bean="GiftOptionsFormHandler.applyGiftWrap" value="true" />
		</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="action" param="action"/>
	<c:choose>
		<c:when test="${action eq 'applyGiftMessage' or action eq 'review_applyGiftMessage' }">
			<dsp:droplet name="Switch">
		         <dsp:param bean="GiftOptionsFormHandler.formError" name="value" />
		         <dsp:oparam name="true">
		         	{
		         		"success": false,
		         		"errorMessages" : {
							<dsp:droplet name="ErrorMessageForEach">
								<dsp:param name="exceptions" bean="GiftOptionsFormHandler.formExceptions" />	
								<dsp:oparam name="output">
									<dsp:getvalueof var="count" param="count" />
									<dsp:getvalueof var="size" param="size" />
									<c:choose>
										<c:when test="${count eq size}">
											"<dsp:valueof param="propertyName" />": "<dsp:valueof param="message" />"
										</c:when>
										<c:otherwise>
											"<dsp:valueof param="propertyName" />": "<dsp:valueof param="message" />",
										</c:otherwise>
									</c:choose>
								</dsp:oparam>
							</dsp:droplet>
						}
					}
				</dsp:oparam>
				<dsp:oparam name="false">
					{"success": true}
				</dsp:oparam>
			</dsp:droplet>
		</c:when>
		<c:otherwise>
			<dsp:droplet name="ErrorMessageForEach">			
				<dsp:param name="exceptions" bean="GiftOptionsFormHandler.formExceptions" />	
				<dsp:oparam name="output">				
					 	{"success":"false"}
				</dsp:oparam>
				<dsp:oparam name="empty">
					 	{"success":"true"}
				</dsp:oparam>
			</dsp:droplet> 
		</c:otherwise>
	</c:choose>
</dsp:page>