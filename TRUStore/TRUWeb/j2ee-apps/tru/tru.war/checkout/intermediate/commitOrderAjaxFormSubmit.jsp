<dsp:page>

<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/CheckoutProfileFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/atg/dynamo/droplet/IsNull" />
<dsp:importbean bean="/atg/commerce/promotion/CouponFormHandler" />
<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUGetSynchronyUrlDroplet" />
<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUParseSynchronyResponseDroplet" />
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
<dsp:getvalueof var="order" bean="ShoppingCart.current" />
<dsp:getvalueof var="truConf" bean="TRUConfiguration" />
<dsp:getvalueof var="formID" param="formID"></dsp:getvalueof>
<dsp:getvalueof var="fromPageName" param="fromPageName"></dsp:getvalueof>
<dsp:getvalueof var="action" param="action"></dsp:getvalueof>
<dsp:getvalueof var="financingTermsAvailable" value="${order.financingTermsAvailable}"/>

	<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsNull" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
	<dsp:getvalueof var="order" bean="ShoppingCart.current" />
	<dsp:getvalueof var="truConf" bean="TRUConfiguration" />
	<dsp:getvalueof var="formID" param="formID"></dsp:getvalueof>
	<dsp:getvalueof var="fromPageName" param="fromPageName"></dsp:getvalueof>
 <c:choose>
	<c:when test="${formID eq 'inStorePaymentOrderReviewForm'}">
    	<dsp:getvalueof var="firstName" param="firstName" scope="request" />
		<dsp:getvalueof var="lastName" param="lastName" scope="request" />
		<dsp:getvalueof var="address1" param="address1" scope="request" />
		<dsp:getvalueof var="address2" param="address2" scope="request" />
		<dsp:getvalueof var="city" param="city" scope="request" />
		<dsp:getvalueof var="state" param="state" scope="request" />
		<dsp:getvalueof var="otherState" param="otherState" scope="request" />
		<dsp:getvalueof var="postalCode" param="postalCode" scope="request" />
		<dsp:getvalueof var="country" param="country" scope="request" />
		<dsp:getvalueof var="phoneNumber" param="phoneNumber" scope="request" />
		<dsp:getvalueof var="orderEmail" param="orderEmail" scope="request" />
		<dsp:getvalueof var="newBillingAddress" param="newBillingAddress" scope="request" />
		<dsp:getvalueof var="editBillingAddress" param="editBillingAddress" scope="request" />
		<dsp:getvalueof var="isRewardsCardValid" param="isRewardsCardValid" scope="request" />
		<dsp:getvalueof var="rewardNumber" param="rewardNumber" scope="request" />
		
		<dsp:getvalueof var="newCreditCard" param="newCreditCard"/>
			<dsp:getvalueof var="editCreditCard" param="editCreditCard"/>
			
			<dsp:droplet name="IsNull">
		   		<dsp:param name="value" value="${newCreditCard}" />
		   		<dsp:oparam name="false">
					<dsp:getvalueof var="newCreditCard" value="${newCreditCard}" scope="request"/>
		   		</dsp:oparam>
		   		<dsp:oparam name="true">
					<dsp:getvalueof var="newCreditCard" value="false" scope="request"/>
		   		</dsp:oparam>
		   	</dsp:droplet>
		   	<dsp:droplet name="IsEmpty">
		   		<dsp:param name="value" value="${editCreditCard}" />
		   		<dsp:oparam name="false">
					<dsp:getvalueof var="editCreditCard" value="${editCreditCard}" scope="request"/>
		   		</dsp:oparam>
		   		<dsp:oparam name="true">
					<dsp:getvalueof var="editCreditCard" value="false" scope="request"/>
		   		</dsp:oparam>
		   	</dsp:droplet>
		   		
		<dsp:setvalue bean="BillingFormHandler.email" paramvalue="orderEmail"/>
		<%--Start: Added for adding rewards number to order  --%>
        <dsp:setvalue bean="BillingFormHandler.rewardNumberValid" paramvalue="isRewardsCardValid" />
		<dsp:setvalue bean="BillingFormHandler.rewardNumber" paramvalue="rewardNumber" /> 
		
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.firstName" paramvalue="firstName"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.lastName" paramvalue="lastName"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.address1" paramvalue="address1"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.address2" paramvalue="address2"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.city" paramvalue="city"/>
		<c:choose>
			<c:when test="${country ne 'US'}">
				<dsp:setvalue bean="BillingFormHandler.addressInputFields.state" paramvalue="otherState"/>
			</c:when>
			<c:otherwise>
				<dsp:setvalue bean="BillingFormHandler.addressInputFields.state" paramvalue="state"/>
			</c:otherwise>
		</c:choose>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.postalCode" paramvalue="postalCode"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.phoneNumber" paramvalue="phoneNumber"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.country" paramvalue="country"/>
		<dsp:setvalue bean="BillingFormHandler.newBillingAddress" paramvalue="newBillingAddress"/>
		<dsp:setvalue bean="BillingFormHandler.editBillingAddress" paramvalue="editBillingAddress"/>
		<dsp:setvalue bean="BillingFormHandler.addressValidated" paramvalue="addressValidated"/>
		<%--End: Added for adding rewards number to order  --%>
		<dsp:setvalue bean="BillingFormHandler.inStorePaymentToReviewOrder" value="submit"/>
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="BillingFormHandler.formError" name="value" />
           <dsp:oparam name="true">           	
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: 
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <li><dsp:valueof param="message"/></li>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
            <dsp:oparam name="false">
				<dsp:droplet name="Switch">
				<dsp:param name="value" bean="BillingFormHandler.addressDoctorResponseVO.derivedStatus" />
				<dsp:oparam name="NO">
					address-saved-success-from-ajax: 
				</dsp:oparam>
				<dsp:oparam name="YES">
					address-doctor-overlay-from-ajax:
					<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
						<dsp:param name="mode" value="paymentInStore" />
					</dsp:include>
				</dsp:oparam>
				<%-- <dsp:oparam name="VALIDATION ERROR">
					address-doctor-overlay-from-ajax:
					<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
						<dsp:param name="mode" value="paymentInStore" />
					</dsp:include>
				</dsp:oparam> --%>
				<dsp:oparam name="default"> 
					address-saved-success-from-ajax: 
				 </dsp:oparam>
			</dsp:droplet>
            </dsp:oparam>
         </dsp:droplet>
   </c:when>
   <c:when test="${formID eq 'inStorePaymentCommitOrderForm'}">
  		<dsp:getvalueof var="firstName" param="firstName" scope="request" />
		<dsp:getvalueof var="lastName" param="lastName" scope="request" />
		<dsp:getvalueof var="address1" param="address1" scope="request" />
		<dsp:getvalueof var="address2" param="address2" scope="request" />
		<dsp:getvalueof var="city" param="city" scope="request" />
		<dsp:getvalueof var="state" param="state" scope="request" />
		<dsp:getvalueof var="otherState" param="otherState" scope="request" />
		<dsp:getvalueof var="postalCode" param="postalCode" scope="request" />
		<dsp:getvalueof var="country" param="country" scope="request" />
		<dsp:getvalueof var="phoneNumber" param="phoneNumber" scope="request" />
		<dsp:getvalueof var="orderEmail" param="orderEmail" scope="request" />
		<dsp:getvalueof var="newBillingAddress" param="newBillingAddress" scope="request" />
		<dsp:getvalueof var="editBillingAddress" param="editBillingAddress" scope="request" />
		<dsp:getvalueof var="isRewardsCardValid" param="isRewardsCardValid" scope="request" />
		<dsp:getvalueof var="rewardNumber" param="rewardNumber" scope="request" />
		
		<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.firstName" paramvalue="firstName"/>
		<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.lastName" paramvalue="lastName"/>
		<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.address1" paramvalue="address1"/>
		<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.address2" paramvalue="address2"/>
		<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.city" paramvalue="city"/>
		
		<c:choose>
			<c:when test="${country ne 'US'}">
				<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.state" paramvalue="otherState"/>
			</c:when>
			<c:otherwise>
				<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.state" paramvalue="state"/>
			</c:otherwise>
		</c:choose>
		<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.postalCode" paramvalue="postalCode"/>
		<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.phoneNumber" paramvalue="phoneNumber"/>
		<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.country" paramvalue="country"/>
		<dsp:setvalue bean="CommitOrderFormHandler.newBillingAddress" paramvalue="newBillingAddress"/>
		<dsp:setvalue bean="CommitOrderFormHandler.editBillingAddress" paramvalue="editBillingAddress"/>
		<dsp:setvalue bean="CommitOrderFormHandler.addressValidated" paramvalue="addressValidated"/>

		<dsp:setvalue bean="CommitOrderFormHandler.email" paramvalue="orderEmail"/>
		<dsp:setvalue bean="CommitOrderFormHandler.rewardNumberValid" paramvalue="isRewardsCardValid" />
		<dsp:setvalue bean="CommitOrderFormHandler.rewardNumber" paramvalue="rewardNumber" /> 
		<dsp:setvalue bean="CommitOrderFormHandler.deviceID" paramvalue="deviceID" />
		<dsp:setvalue bean="CommitOrderFormHandler.browserAcceptEncoding" paramvalue="browserAcceptEncoding" /> 
		<dsp:setvalue bean="CommitOrderFormHandler.paymentGroupType" value="inStorePayment" />
		<dsp:setvalue bean="CommitOrderFormHandler.commitOrder" value="submit" />
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="CommitOrderFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="CommitOrderFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: 
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <li><dsp:valueof param="message"/></li>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						
				   </dsp:oparam>
	           </dsp:droplet>
            </dsp:oparam>
            <dsp:oparam name="false">
				<dsp:droplet name="Switch">
					<dsp:param name="value" bean="CommitOrderFormHandler.addressDoctorResponseVO.derivedStatus" />
					<dsp:oparam name="NO">
						address-saved-success-from-ajax: 
					</dsp:oparam>
					<dsp:oparam name="YES">
						address-doctor-overlay-from-ajax:
						<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
							<dsp:param name="mode" value="placeOrder" />
						</dsp:include>
					</dsp:oparam>
					<%-- <dsp:oparam name="VALIDATION ERROR">
						address-doctor-overlay-from-ajax:
						<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
							<dsp:param name="mode" value="placeOrder" />
						</dsp:include>
					</dsp:oparam> --%>
					<dsp:oparam name="default"> 
					  address-saved-success-from-ajax: 
				 </dsp:oparam>
			  </dsp:droplet> 
            </dsp:oparam>
         </dsp:droplet>
   </c:when>
   <c:when test="${formID eq 'giftCardPaymentCommitOrderForm'}">
	<c:if test="${not empty fromPageName && fromPageName eq 'Payment'}">
		<dsp:getvalueof var="firstName" param="firstName" scope="request" />
		<dsp:getvalueof var="lastName" param="lastName" scope="request" />
		<dsp:getvalueof var="address1" param="address1" scope="request" />
		<dsp:getvalueof var="address2" param="address2" scope="request" />
		<dsp:getvalueof var="city" param="city" scope="request" />
		<dsp:getvalueof var="state" param="state" scope="request" />
		<dsp:getvalueof var="otherState" param="otherState" scope="request" />
		<dsp:getvalueof var="postalCode" param="postalCode" scope="request" />
		<dsp:getvalueof var="country" param="country" scope="request" />
		<dsp:getvalueof var="phoneNumber" param="phoneNumber" scope="request" />
		<dsp:getvalueof var="orderEmail" param="orderEmail" scope="request" />
		<dsp:getvalueof var="newBillingAddress" param="newBillingAddress" scope="request" />
		<dsp:getvalueof var="editBillingAddress" param="editBillingAddress" scope="request" />
		<dsp:getvalueof var="isRewardsCardValid" param="isRewardsCardValid" scope="request" />
		<dsp:getvalueof var="rewardNumber" param="rewardNumber" scope="request" />

		<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.firstName" paramvalue="firstName"/>
		<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.lastName" paramvalue="lastName"/>
		<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.address1" paramvalue="address1"/>
		<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.address2" paramvalue="address2"/>
		<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.city" paramvalue="city"/>
		<c:choose>
			<c:when test="${country ne 'US'}">
				<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.state" paramvalue="otherState"/>
			</c:when>
			<c:otherwise>
				<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.state" paramvalue="state"/>
			</c:otherwise>
		</c:choose>
		<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.postalCode" paramvalue="postalCode"/>
		<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.phoneNumber" paramvalue="phoneNumber"/>
		<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.country" paramvalue="country"/>
		<dsp:setvalue bean="CommitOrderFormHandler.newBillingAddress" paramvalue="newBillingAddress"/>
		<dsp:setvalue bean="CommitOrderFormHandler.editBillingAddress" paramvalue="editBillingAddress"/>
		<dsp:setvalue bean="CommitOrderFormHandler.addressValidated" paramvalue="addressValidated"/>
		<dsp:setvalue bean="CommitOrderFormHandler.rewardNumberValid" paramvalue="isRewardsCardValid" />
		<dsp:setvalue bean="CommitOrderFormHandler.rewardNumber" paramvalue="rewardNumber" />
		<dsp:setvalue bean="CommitOrderFormHandler.deviceID" paramvalue="deviceID" />
		<dsp:setvalue bean="CommitOrderFormHandler.browserAcceptEncoding" paramvalue="browserAcceptEncoding" /> 
		<dsp:setvalue bean="CommitOrderFormHandler.email" paramvalue="orderEmail"/>
	</c:if>	
	<c:if test="${not empty fromPageName && fromPageName eq 'Review'}">
		<dsp:setvalue bean="CommitOrderFormHandler.pageName" value="Review"/>
		<dsp:setvalue bean="CommitOrderFormHandler.email" beanvalue="ShoppingCart.current.email"/>
	</c:if>	
		<dsp:setvalue bean="CommitOrderFormHandler.paymentGroupType" value="giftCardPayment" />
		<dsp:setvalue bean="CommitOrderFormHandler.commitOrder" value="submit" />
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="CommitOrderFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="CommitOrderFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="errorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	              	<dsp:setvalue bean="ShoppingCart.current.repriceRequiredOnCart" value="true" />
	              	<dsp:droplet name="/atg/commerce/order/purchase/RepriceOrderDroplet">
						<dsp:param value="ORDER_TOTAL" name="pricingOp" />
					</dsp:droplet>
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
            </dsp:oparam>
            <dsp:oparam name="false">
				<dsp:droplet name="Switch">
					<dsp:param name="value" bean="CommitOrderFormHandler.addressDoctorResponseVO.derivedStatus" />
					<dsp:oparam name="NO">
						address-saved-success-from-ajax: 
					</dsp:oparam>
					<dsp:oparam name="YES">
						address-doctor-overlay-from-ajax:
						<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
							<dsp:param name="mode" value="placeOrder" />
						</dsp:include>
					</dsp:oparam>
					<%-- <dsp:oparam name="VALIDATION ERROR">
						address-doctor-overlay-from-ajax:
						<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
							<dsp:param name="mode" value="placeOrder" />
						</dsp:include>
					</dsp:oparam> --%>
					<dsp:oparam name="default"> 
					  address-saved-success-from-ajax: 
				 </dsp:oparam>
			  </dsp:droplet> 
            </dsp:oparam>
         </dsp:droplet>
   </c:when>
	<c:when test="${formID eq 'placeOrderCreditCardForm'}">
			<dsp:getvalueof var="isNoFinanceAgreed" param="isNoFinanceAgreed" scope="request"/>
	        <dsp:getvalueof var="orderSet" param="orderSet" scope="request"/>
	    	<dsp:getvalueof var="firstName" param="firstName" scope="request" />
			<dsp:getvalueof var="lastName" param="lastName" scope="request" />
			<dsp:getvalueof var="address1" param="address1" scope="request" />
			<dsp:getvalueof var="address2" param="address2" scope="request" />
			<dsp:getvalueof var="city" param="city" scope="request" />
			<dsp:getvalueof var="state" param="state" scope="request" />
			<dsp:getvalueof var="otherState" param="otherState" scope="request" />
			<dsp:getvalueof var="postalCode" param="postalCode" scope="request" />
			<dsp:getvalueof var="country" param="country" scope="request" />
			<dsp:getvalueof var="phoneNumber" param="phoneNumber" scope="request" />
			<dsp:getvalueof var="orderEmail" param="orderEmail" scope="request" />
			<dsp:getvalueof var="ccToken" param="ccToken" scope="request" />
			<dsp:getvalueof var="cardType" param="cardType" scope="request" />
		<%-- 	<dsp:getvalueof var="respJwt" param="respJwt" scope="request" /> --%>			
			<dsp:getvalueof var="isRUSCard" param="isRUSCard" scope="request" />
			<dsp:getvalueof var="cBinNum" param="cBinNum" scope="request" />
			<dsp:getvalueof var="cLength" param="cLength" scope="request" />
			<dsp:getvalueof var="expirationMonth" param="expirationMonth" scope="request" />
			<dsp:getvalueof var="expirationYear" param="expirationYear" scope="request" />
			<dsp:getvalueof var="creditCardCVV" param="creditCardCVV" scope="request" />
			<dsp:getvalueof var="nameOnCard" param="nameOnCard" scope="request" />
			<dsp:getvalueof var="isRewardsCardValid" param="isRewardsCardValid" scope="request" />
			<dsp:getvalueof var="rewardNumber" param="rewardNumber" scope="request" />
			<dsp:setvalue bean="CommitOrderFormHandler.deviceID" paramvalue="deviceID" />
			<dsp:setvalue bean="CommitOrderFormHandler.browserAcceptEncoding" paramvalue="browserAcceptEncoding" /> 
			<dsp:getvalueof var="paymentGroupType" param="paymentGroupType" scope="request"/>
			<dsp:getvalueof var="ccNum" param="cBinNum" />
			<dsp:getvalueof var="ccnumber" param="ccnumber" scope="request" />
			
			<dsp:getvalueof var="editCardNickName" param="editCardNickName" scope="request" />
			<dsp:getvalueof var="newBillingAddress" param="newBillingAddress" scope="request" />
			<dsp:getvalueof var="editBillingAddress" param="editBillingAddress" scope="request" />
			
			<dsp:getvalueof var="newCreditCard" param="newCreditCard"/>
			<dsp:getvalueof var="editCreditCard" param="editCreditCard"/>
			
			<dsp:droplet name="IsNull">
		   		<dsp:param name="value" value="${newBillingAddress}" />
		   		<dsp:oparam name="false">
					<dsp:getvalueof var="newBillingAddress" value="${newBillingAddress}" scope="request"/>
		   		</dsp:oparam>
		   		<dsp:oparam name="true">
					<dsp:getvalueof var="newBillingAddress" value="false" scope="request"/>
		   		</dsp:oparam>
		   	</dsp:droplet>
		   	<dsp:droplet name="IsNull">
		   		<dsp:param name="value" value="${editBillingAddress}" />
		   		<dsp:oparam name="false">
					<dsp:getvalueof var="editBillingAddress" value="${editBillingAddress}" scope="request"/>
		   		</dsp:oparam>
		   		<dsp:oparam name="true">
					<dsp:getvalueof var="editBillingAddress" value="false" scope="request"/>
		   		</dsp:oparam>
		   	</dsp:droplet>
			<dsp:droplet name="IsNull">
		   		<dsp:param name="value" value="${newCreditCard}" />
		   		<dsp:oparam name="false">
					<dsp:getvalueof var="newCreditCard" value="${newCreditCard}" scope="request"/>
		   		</dsp:oparam>
		   		<dsp:oparam name="true">
					<dsp:getvalueof var="newCreditCard" value="false" scope="request"/>
		   		</dsp:oparam>
		   	</dsp:droplet>
		   	<dsp:droplet name="IsEmpty">
		   		<dsp:param name="value" value="${editCreditCard}" />
		   		<dsp:oparam name="false">
					<dsp:getvalueof var="editCreditCard" value="${editCreditCard}" scope="request"/>
		   		</dsp:oparam>
		   		<dsp:oparam name="true">
					<dsp:getvalueof var="editCreditCard" value="false" scope="request"/>
		   		</dsp:oparam>
		   	</dsp:droplet>
			<c:choose>
			      <c:when test="${orderSet eq true}">
			         <dsp:setvalue bean="CommitOrderFormHandler.orderSetWithDefault" value="true"/>
			      </c:when>
			      <c:otherwise>
			         <dsp:setvalue bean="CommitOrderFormHandler.orderSetWithDefault" value="false"/>
			      </c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test="${isRUSCard eq 'true'}">
					<dsp:setvalue bean="CommitOrderFormHandler.RUSCard" value="true"/>
				</c:when>
				<c:otherwise>
					<dsp:setvalue bean="CommitOrderFormHandler.RUSCard" value="false"/>
				</c:otherwise>
			</c:choose>
			<dsp:setvalue bean="CommitOrderFormHandler.noFinanceAgreed" value="${isNoFinanceAgreed}"/>
	        <dsp:setvalue bean="CommitOrderFormHandler.paymentGroupType" paramvalue="paymentGroupType"/>
			<dsp:setvalue bean="CommitOrderFormHandler.creditCardNickName" paramvalue="selectedSavedCard" />
			<dsp:setvalue bean="CommitOrderFormHandler.creditCardInfoMap.nameOnCard" paramvalue="nameOnCard"/>
			<dsp:setvalue bean="CommitOrderFormHandler.creditCardInfoMap.creditCardToken" value="${ccToken}"/>
		<%-- 	<dsp:setvalue bean="CommitOrderFormHandler.creditCardInfoMap.cardinalJwt" value="${respJwt}"/> --%>		
			<c:choose>
				<c:when test="${not empty ccNum}">
					<dsp:setvalue bean="CommitOrderFormHandler.creditCardInfoMap.creditCardNumber" value="${ccNum}"/>										
				</c:when>
				<c:otherwise>
					<dsp:setvalue bean="CommitOrderFormHandler.creditCardInfoMap.creditCardNumber" paramvalue="ccnumber"/>
				</c:otherwise>
			</c:choose>
			<%-- <dsp:setvalue bean="CommitOrderFormHandler.creditCardInfoMap.creditCardNumber" paramvalue="ccnumber"/> --%>
			<dsp:setvalue bean="CommitOrderFormHandler.creditCardInfoMap.cardType" value="${cardType}"/>
			<dsp:setvalue bean="CommitOrderFormHandler.creditCardInfoMap.creditCardType" value="${cardType}"/>
			<dsp:setvalue bean="CommitOrderFormHandler.creditCardInfoMap.cardLength" value="${cLength}"/>
			<dsp:setvalue bean="CommitOrderFormHandler.deviceID" paramvalue="deviceID" />
			<dsp:setvalue bean="CommitOrderFormHandler.browserAcceptEncoding" paramvalue="browserAcceptEncoding" /> 
			<c:choose>
				<c:when test="${ccNum eq '604586' and isRUSCard ne 'true'}">
					<dsp:setvalue bean="CommitOrderFormHandler.creditCardInfoMap.expirationMonth" value="${truConf.expMonthAdjustmentForPLCC}"/>
					<dsp:setvalue bean="CommitOrderFormHandler.creditCardInfoMap.expirationYear" value="${truConf.expYearAdjustmentForPLCC}"/>
				</c:when>
				<c:otherwise>
					<dsp:setvalue bean="CommitOrderFormHandler.creditCardInfoMap.expirationMonth" paramvalue="expirationMonth"/>
					<dsp:setvalue bean="CommitOrderFormHandler.creditCardInfoMap.expirationYear" paramvalue="expirationYear"/>
				</c:otherwise>
			</c:choose>
			<dsp:setvalue bean="CommitOrderFormHandler.creditCardInfoMap.creditCardVerificationNumber"  paramvalue="creditCardCVV"/>
			<%--Start: Added for adding rewards number to order  --%>
	        <dsp:setvalue bean="CommitOrderFormHandler.rewardNumberValid" paramvalue="isRewardsCardValid" />
			<dsp:setvalue bean="CommitOrderFormHandler.rewardNumber" paramvalue="rewardNumber" /> 
			<%--End: Added for adding rewards number to order  --%>
			<dsp:getvalueof var="selectedCardBillingAddress" param="selectedCardBillingAddress"></dsp:getvalueof>
			<dsp:setvalue bean="CommitOrderFormHandler.email" value="${orderEmail}"/>
					<dsp:setvalue bean="CommitOrderFormHandler.billingAddressNickName" paramvalue="nickName"/>
					<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.firstName" paramvalue="firstName"/>
					<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.lastName" paramvalue="lastName"/>
					<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.address1" paramvalue="address1"/>
					<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.address2" paramvalue="address2"/>
					<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.city" paramvalue="city"/>
					<c:choose>
						<c:when test="${country ne 'US'}">
							<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.state" paramvalue="otherState"/>
						</c:when>
						<c:otherwise>
							<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.state" paramvalue="state"/>
						</c:otherwise>
					</c:choose>
			<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.postalCode" paramvalue="postalCode"/>
			<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.phoneNumber" paramvalue="phoneNumber"/>
			<dsp:setvalue bean="CommitOrderFormHandler.addressInputFields.country" paramvalue="country"/>
			<dsp:setvalue bean="CommitOrderFormHandler.addressValidated" paramvalue="addressValidated"/>
			<dsp:setvalue bean="CommitOrderFormHandler.pageName" paramvalue="pageName" />
			<dsp:setvalue bean="CommitOrderFormHandler.newBillingAddress" value="${newBillingAddress}"/>
			<dsp:setvalue bean="CommitOrderFormHandler.editBillingAddress" value="${editBillingAddress}"/>
			<dsp:setvalue bean="CommitOrderFormHandler.newCreditCard" value="${newCreditCard}"/>
			<dsp:setvalue bean="CommitOrderFormHandler.editCreditCard" value="${editCreditCard}"/>
			<dsp:setvalue bean="CommitOrderFormHandler.editCardNickName" value="${editCardNickName}"/>
			<dsp:setvalue bean="CommitOrderFormHandler.commitOrder" value="submit"/>
			<dsp:droplet name="/atg/dynamo/droplet/Switch">
	           <dsp:param bean="CommitOrderFormHandler.formError" name="value" />
	           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="CommitOrderFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: 
	             </dsp:oparam>
	              <dsp:oparam name="output">
	              	<dsp:setvalue bean="ShoppingCart.current.repriceRequiredOnCart" value="true" />
	              	<dsp:droplet name="/atg/commerce/order/purchase/RepriceOrderDroplet">
						<dsp:param value="ORDER_TOTAL" name="pricingOp" />
					</dsp:droplet>
	                  <li><dsp:valueof param="message"/></li>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						
				   </dsp:oparam>
	           </dsp:droplet>
            </dsp:oparam>
            <dsp:oparam name="false">
			<dsp:droplet name="Switch">
				<dsp:param name="value" bean="CommitOrderFormHandler.addressDoctorResponseVO.derivedStatus" />
				<dsp:oparam name="NO">
					address-saved-success-from-ajax:
				</dsp:oparam>
				<dsp:oparam name="YES">
					address-doctor-overlay-from-ajax:
					<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
						<dsp:param name="mode" value="placeOrder" />
					</dsp:include>
				</dsp:oparam>
				<%-- <dsp:oparam name="VALIDATION ERROR">
					address-doctor-overlay-from-ajax:
					<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
						<dsp:param name="mode" value="placeOrder" />
					</dsp:include>
				</dsp:oparam> --%>
				<dsp:oparam name="default"> 
					address-saved-success-from-ajax: 
			 </dsp:oparam>
			</dsp:droplet> 
            </dsp:oparam>
         </dsp:droplet>
	</c:when>
</c:choose>
	<dsp:droplet name="Switch">
		<dsp:param name="value" param="action"/>
		<dsp:oparam name="commitOrder">
	   		<dsp:setvalue bean="CommitOrderFormHandler.commitOrderErrorURL" value="/checkout/review/orderReview.jsp"/>
	   		<dsp:setvalue bean="CommitOrderFormHandler.commitOrderSuccessURL" value="/checkout/confirm/orderConfirmation.jsp"/>
			<dsp:setvalue bean="CommitOrderFormHandler.commitOrder" value="true" />
			<dsp:getvalueof var="shipRestrictionsFound" bean="ShoppingCart.current.shipRestrictionsFound"/>
			<dsp:droplet name="ErrorMessageForEach">			
				<dsp:param name="exceptions" bean="CommitOrderFormHandler.formExceptions" />	
				<dsp:oparam name="output">				
					 	{"success":"false","shipRestrictionsFound":"${shipRestrictionsFound}"}
				</dsp:oparam>
				<dsp:oparam name="empty">
					 	{"success":"true"}
				</dsp:oparam>
			</dsp:droplet> 
		</dsp:oparam>
		<dsp:oparam name="commitOrderFromReview">
			<dsp:setvalue bean="CommitOrderFormHandler.pageName" value="Review"/>
			<%-- <dsp:setvalue bean="CommitOrderFormHandler.email" bean="ShoppingCart.current.email"/> --%>
			<%--Start: Added for adding rewards number to order  --%>
	        <dsp:setvalue bean="CommitOrderFormHandler.rewardNumberValid" paramvalue="isRewardsCardValid" />
			<dsp:setvalue bean="CommitOrderFormHandler.rewardNumber" paramvalue="rewardNumber" />
			<dsp:setvalue bean="CommitOrderFormHandler.deviceID" paramvalue="deviceID" /> 
			<dsp:setvalue bean="CommitOrderFormHandler.browserAcceptEncoding" paramvalue="browserAcceptEncoding" /> 
			<%--End: Added for adding rewards number to order  --%>
		<%-- 	<dsp:getvalueof var="respJwt" param="respJwt" scope="request" />
			<dsp:setvalue bean="CommitOrderFormHandler.creditCardInfoMap.cardinalJwt" value="${respJwt}"/>		 --%>				
			<dsp:setvalue bean="CommitOrderFormHandler.commitOrder" value="submit" />
			<dsp:droplet name="Switch">
			         <dsp:param bean="CommitOrderFormHandler.formError" name="value" />
			         <dsp:oparam name="true">
			         <!-- {"success": false} -->
						<dsp:droplet name="ErrorMessageForEach">
							<dsp:param name="exceptions"
								bean="CommitOrderFormHandler.formExceptions" />
							<dsp:oparam name="outputStart">
								Following are the form errors: 
							</dsp:oparam>
							<dsp:oparam name="output">
								<dsp:setvalue bean="ShoppingCart.current.repriceRequiredOnCart" value="true" />
								<dsp:droplet name="/atg/commerce/order/purchase/RepriceOrderDroplet">
									<dsp:param value="ORDER_TOTAL" name="pricingOp" />
									<dsp:param param="errorCode" name="value" />
								</dsp:droplet>
								<dsp:getvalueof var="errorCode" param="errorCode"/>
								<div>
									<input type="hidden" id="radial_error_code" value="${errorCode}"/>
									<dsp:valueof param="message" valueishtml="true"/>
								</div>
							</dsp:oparam>
							<dsp:oparam name="outputEnd">
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
					<dsp:oparam name="false">
						<!-- {"success": true} -->
					</dsp:oparam>
				</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>