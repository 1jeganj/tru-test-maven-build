<dsp:page>

<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/CheckoutProfileFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/atg/dynamo/droplet/IsNull" />
<dsp:importbean bean="/atg/commerce/promotion/CouponFormHandler" />
<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUGetSynchronyUrlDroplet" />
<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUParseSynchronyResponseDroplet" />
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
<dsp:getvalueof var="order" bean="ShoppingCart.current" />
<dsp:getvalueof var="truConf" bean="TRUConfiguration" />
<dsp:getvalueof var="formID" param="formID"></dsp:getvalueof>
<dsp:getvalueof var="fromPageName" param="fromPageName"></dsp:getvalueof>
<dsp:getvalueof var="action" param="action"></dsp:getvalueof>
<dsp:getvalueof var="financingTermsAvailable" value="${order.financingTermsAvailable}"/>

 <c:choose>
	<c:when test="${formID eq 'addShippingAddress' or formID eq 'addShippingAddFromSuggestedOverlay'}"><%-- or formID eq 'addShippingAddFromSuggestedOverlay --%>
	
			<dsp:getvalueof var="firstName" param="firstName" scope="request" />
			<dsp:getvalueof var="lastName" param="lastName" scope="request" />
			<dsp:getvalueof var="address1" param="address1" scope="request" />
			<dsp:getvalueof var="address2" param="address2" scope="request" />
			<dsp:getvalueof var="city" param="city" scope="request" />
			<dsp:getvalueof var="state" param="state" scope="request" />
			<dsp:getvalueof var="postalCode" param="postalCode" scope="request" />
			<dsp:getvalueof var="country" param="country" scope="request" />
			<dsp:getvalueof var="phoneNumber" param="phoneNumber" scope="request" />
			<dsp:getvalueof var="showMeGiftingOptions" param="showMeGiftingOptions" scope="request" />
	
			<dsp:setvalue bean="ShippingGroupFormHandler.address.firstName" paramvalue="firstName"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.address.lastName" paramvalue="lastName"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.address.address1" paramvalue="address1"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.address.address2" paramvalue="address2"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.address.city" paramvalue="city"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.address.state" paramvalue="state"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.address.postalCode" paramvalue="postalCode"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.address.phoneNumber" paramvalue="phoneNumber"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.address.country" paramvalue="country"/>
			<c:if test="${not empty action && action eq 'moveToMultipleShip'}">
				<dsp:setvalue bean="ShippingGroupFormHandler.navigateToMultiShip" value="true"/>
				<dsp:setvalue bean="ShippingGroupFormHandler.skipShippingRestrctions" value="true"/>
			</c:if>
			
			<dsp:setvalue bean="ShippingGroupFormHandler.billingAddressSameAsShippingAddress" paramvalue="billingAddressSameAsShippingAddress"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shippingMethod" paramvalue="shippingMethod"/>
			<dsp:droplet name="IsEmpty">
		   		<dsp:param name="value" param="shippingPrice" />
		   		<dsp:oparam name="false">
					<dsp:setvalue bean="ShippingGroupFormHandler.shippingPrice" paramvalue="shippingPrice"/>
		   		</dsp:oparam>
		   		<dsp:oparam name="true">
					<dsp:setvalue bean="ShippingGroupFormHandler.shippingPrice" value="0"/>
		   		</dsp:oparam>
		   	</dsp:droplet>
			<dsp:setvalue bean="ShippingGroupFormHandler.addressValidated" paramvalue="addressValidated"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.showMeGiftingOptions" paramvalue="showMeGiftingOptions"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shipToNewAddress"  value="submit"/>
			<dsp:droplet name="Switch">
				<dsp:param name="value" bean="ShippingGroupFormHandler.orderReconciliationNeeded" />
				<dsp:oparam name="true">
					address-saved-success-from-ajax:orderReconciliationNeeded:					
				</dsp:oparam>
			</dsp:droplet>	
			<dsp:droplet name="Switch">
				<dsp:param bean="ShippingGroupFormHandler.formError" name="value" />
				<dsp:oparam name="true">
				<dsp:droplet name="Switch">
					<dsp:param name="value" bean="ShippingGroupFormHandler.orderReconciliationNeeded" />
						<dsp:oparam name="true">
								 	orderReconciliation_DIV:{	"success": false,
										"orderReconciliationNeeded" : "<dsp:valueof bean="ShippingGroupFormHandler.orderReconciliationNeeded"/>",
										"errorMessages" : [
											<dsp:droplet name="ErrorMessageForEach">
												<dsp:param name="exceptions" bean="ShippingGroupFormHandler.formExceptions" />	
												<dsp:oparam name="output">
													<dsp:getvalueof var="count" param="count" />
													<dsp:getvalueof var="size" param="size" />
													<c:choose>
														<c:when test="${count eq size}">											
															{"message": "<dsp:valueof param="message" />","divId": "<dsp:valueof param="errorCode" />"}
														 
														</c:when>
														<c:otherwise>											
															{"message": "<dsp:valueof param="message" />","divId": "<dsp:valueof param="errorCode" />"},
														</c:otherwise>
													</c:choose>
												</dsp:oparam>
											</dsp:droplet>
										]
									}
						</dsp:oparam>
						<dsp:oparam name="false">
							<%--Start: Added as part of MVP-97 Changes  --%>
							<dsp:getvalueof var="shipRestrictionsFound" bean="ShoppingCart.current.shipRestrictionsFound"/>
							<%--END: Added as part of MVP-97 Changes  --%>
							<dsp:droplet name="ErrorMessageForEach">
								<dsp:param name="exceptions"
									bean="ShippingGroupFormHandler.formExceptions" />
								<dsp:oparam name="outputStart">
							Following are the form errors:
								</dsp:oparam>
								<dsp:oparam name="output">
								     <dsp:getvalueof var="propertyName" param="propertyName"/>
								     <c:choose>
								       <c:when test="${propertyName eq 'taxware'}">
								              <dsp:valueof param="propertyName" />:<dsp:valueof param="message" />
								      </c:when>
								      <c:otherwise> 
								          <li><dsp:valueof param="message" />
								          	<%--Start: Added as part of MVP-97 Changes  --%>
								          	<input type="hidden" id="shipRestrictionsFoundTemp" value="${shipRestrictionsFound}"  />
								          	<%--END: Added as part of MVP-97 Changes  --%>
								          </li>
								      </c:otherwise>
								     </c:choose>
								</dsp:oparam>
								<dsp:oparam name="outputEnd">
									
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
						</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="false">
					<dsp:droplet name="Switch">
						<dsp:param name="value" bean="ShippingGroupFormHandler.addressDoctorResponseVO.derivedStatus" />
						<dsp:oparam name="NO">
							address-saved-success-from-ajax:
						</dsp:oparam>
						<dsp:oparam name="YES">
							address-doctor-overlay-from-ajax:
							<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
								<dsp:param name="mode" value="address" />
								<dsp:param name="action" value="${action}" />
							</dsp:include>
						</dsp:oparam>
						<%-- <dsp:oparam name="VALIDATION ERROR">
							address-doctor-overlay-from-ajax:
							<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
								<dsp:param name="mode" value="address" />
								<dsp:param name="action" value="${action}" />
							</dsp:include>
						</dsp:oparam> --%>
						<dsp:oparam name="default">
							address-saved-success-from-ajax:${showMeGiftingOptions}								
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</c:when>
	<c:when test="${formID eq 'removeShippingAddress'}">
			<dsp:setvalue bean="ShippingGroupFormHandler.removeShippingAddressNickName" paramvalue="nickName" />
		    <%-- <dsp:setvalue bean="ShippingGroupFormHandler.saveAddressInOrder" value="true" /> --%>
			<%-- <dsp:setvalue bean="ShippingGroupFormHandler.shippingGroupId" paramvalue="nickName" /> --%>
			<dsp:setvalue bean="ShippingGroupFormHandler.removeShippingAddress" value="submit" />
			<dsp:include page="/checkout/shipping/addressAddEditOverlay.jsp"></dsp:include>
	</c:when>
	<c:when test="${formID eq 'defaultAddress'}">
			<dsp:setvalue bean="ShippingGroupFormHandler.defaultShippingAddressNickName" paramvalue="defaultAddNickName" />
			<dsp:setvalue bean="ShippingGroupFormHandler.setAsDefaultShippingAddress" value="submit" />
			<dsp:include page="/checkout/shipping/addressAddEditOverlay.jsp"/>
	</c:when>
</c:choose>
</dsp:page>