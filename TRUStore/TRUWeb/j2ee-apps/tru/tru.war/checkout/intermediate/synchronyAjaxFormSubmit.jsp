<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/CheckoutProfileFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/atg/dynamo/droplet/IsNull" />
<dsp:importbean bean="/atg/commerce/promotion/CouponFormHandler" />
<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUGetSynchronyUrlDroplet" />
<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUParseSynchronyResponseDroplet" />
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
<dsp:getvalueof var="order" bean="ShoppingCart.current" />
<dsp:getvalueof var="truConf" bean="TRUConfiguration" />
<dsp:getvalueof var="formID" param="formID"></dsp:getvalueof>
<dsp:getvalueof var="fromPageName" param="fromPageName"></dsp:getvalueof>
<dsp:getvalueof var="action" param="action"></dsp:getvalueof>
<dsp:getvalueof var="financingTermsAvailable" value="${order.financingTermsAvailable}"/>
 <c:choose>
	<c:when test="${formID eq 'electronicDisclosureForm'}">
			<dsp:getvalueof var="cardinalToken" param="cardinalToken" />
			<dsp:getvalueof var="expirationMonth" param="expirationMonth"/>
		<dsp:getvalueof var="expirationYear" param="expirationYear"/>
		<dsp:getvalueof var="cardCode" param="cardCode"/>
		<dsp:getvalueof var="nameOnCard" param="nameOnCard"/>
		<dsp:getvalueof var="cBinNum" param="cBinNum"/>
		<dsp:getvalueof var="cardLength" param="cardLength"/>
		<dsp:getvalueof var="isSavedCard" param="isSavedCard"/>
		<dsp:getvalueof var="paymentFromRusCard" value="true" vartype="java.lang.boolean"/>
		
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.nameOnCard"value="${nameOnCard}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardToken" value="${cardinalToken}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardNumber" value="${cBinNum}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.expirationMonth" value="${expirationMonth}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.expirationYear" value="${expirationYear}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.cardLength" value="${cardLength}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardVerificationNumber" value="${cardCode}"/>
		<dsp:setvalue bean="BillingFormHandler.RUSCard" value="${paymentFromRusCard}"/>
		<dsp:setvalue bean="BillingFormHandler.savedCard" value="${isSavedCard}"/>
		
		<dsp:setvalue bean="BillingFormHandler.financeAgreed" value="true"/>
		<dsp:setvalue bean="BillingFormHandler.financingTermsAvailed" value="${order.financingTermsAvailable}"/>
		
		<dsp:setvalue bean="BillingFormHandler.updateFinanceAgreedToOrder" value="submit"/>
		
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="BillingFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="errorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
           <dsp:oparam name="false">
           		<div>
					<div class="disclosureAgreed">agreed</div>
					<div class="percentage-valuation-from-ajax"><dsp:valueof bean="BillingFormHandler.percentageValuation"/></div>
				</div>
           </dsp:oparam>
         </dsp:droplet>
		
	</c:when>
	<c:when test="${formID eq 'checkForOrderFinancing'}">
		<dsp:setvalue bean="BillingFormHandler.updateSynchronyDataInOrder" value="submit" />
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="BillingFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="errorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
           </dsp:droplet>
	</c:when>
	<c:when test="${formID eq 'promoFinancingOptions'}">
		<dsp:getvalueof var="country" param="country" scope="request" />
		<dsp:getvalueof var="expirationMonth" param="expirationMonth"/>
		<dsp:getvalueof var="expirationYear" param="expirationYear"/>
		<dsp:getvalueof var="cardCode" param="cardCode"/>
		<dsp:getvalueof var="nameOnCard" param="nameOnCard"/>
		<dsp:getvalueof var="cBinNum" param="cBinNum"/>
		<dsp:getvalueof var="cardinalToken" param="cardinalToken" />
		<dsp:getvalueof var="cardType" param="rusCardType" />
		<dsp:getvalueof var="paymentFromRusCard" value="true" vartype="java.lang.boolean"/>
		<dsp:getvalueof var="isSavedCard" param="isSavedCard"/>

		<dsp:setvalue bean="BillingFormHandler.savedCard" value="${isSavedCard}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.nameOnCard"value="${nameOnCard}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardNumber" value="${cBinNum}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardToken" value="${cardinalToken}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.expirationMonth" value="${expirationMonth}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.expirationYear" value="${expirationYear}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardType" value="${cardType}"/>
		<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardVerificationNumber" value="${cardCode}"/>
		<dsp:setvalue bean="BillingFormHandler.RUSCard" value="${paymentFromRusCard}"/>
		<dsp:setvalue bean="BillingFormHandler.financingTermsAvailed" value="${order.financingTermsAvailable}"/>
		
		<dsp:setvalue bean="BillingFormHandler.billingAddressNickName" paramvalue="nickName"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.firstName" paramvalue="firstName"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.lastName" paramvalue="lastName"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.address1" paramvalue="address1"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.address2" paramvalue="address2"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.city" paramvalue="city"/>
		<c:choose>
			<c:when test="${country ne 'US'}">
				<dsp:setvalue bean="BillingFormHandler.addressInputFields.state" paramvalue="otherState"/>
			</c:when>
			<c:otherwise>
				<dsp:setvalue bean="BillingFormHandler.addressInputFields.state" paramvalue="state"/>
			</c:otherwise>
		</c:choose>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.postalCode" paramvalue="postalCode"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.phoneNumber" paramvalue="phoneNumber"/>
		<dsp:setvalue bean="BillingFormHandler.addressInputFields.country" paramvalue="country"/>
		<dsp:setvalue bean="BillingFormHandler.addressValidated" paramvalue="addressValidated"/>
		<dsp:getvalueof var="newBillingAddress" param="newBillingAddress" scope="request" />
		<dsp:getvalueof var="editBillingAddress" param="editBillingAddress" scope="request" />
		<dsp:droplet name="IsNull">
	   		<dsp:param name="value" value="${newBillingAddress}" />
	   		<dsp:oparam name="false">
				<dsp:getvalueof var="newBillingAddress" value="${newBillingAddress}" scope="request"/>
	   		</dsp:oparam>
	   		<dsp:oparam name="true">
				<dsp:getvalueof var="newBillingAddress" value="false" scope="request"/>
	   		</dsp:oparam>
	   	</dsp:droplet>
	   	<dsp:droplet name="IsNull">
	   		<dsp:param name="value" value="${editBillingAddress}" />
	   		<dsp:oparam name="false">
				<dsp:getvalueof var="editBillingAddress" value="${editBillingAddress}" scope="request"/>
	   		</dsp:oparam>
	   		<dsp:oparam name="true">
				<dsp:getvalueof var="editBillingAddress" value="false" scope="request"/>
	   		</dsp:oparam>
	   	</dsp:droplet>
		<dsp:setvalue bean="BillingFormHandler.newBillingAddress" value="${newBillingAddress}"/>
		<dsp:setvalue bean="BillingFormHandler.editBillingAddress" value="${editBillingAddress}"/>
		<dsp:setvalue bean="BillingFormHandler.populateFinancingDetails" value="submit"/>
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="BillingFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="errorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
           <dsp:oparam name="false">
           <dsp:getvalueof bean="BillingFormHandler.promoResponseCode" var="promoResponseCode"/>
           <dsp:getvalueof bean="BillingFormHandler.promoText" var="promoText"/>
           <dsp:getvalueof bean="BillingFormHandler.promoFinanceRate" var="promoFinanceRate"/>
           <dsp:getvalueof var="promoVariance" bean="BillingFormHandler.promoText"/>
           <c:set var="promoFinanceRate"><c:if test="${promoFinanceRate.matches('[0-9]+.[0-9]+')}"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2">${promoFinanceRate}</fmt:formatNumber></c:if></c:set>
           
           <c:choose>
           <c:when test="${not empty promoFinanceRate}">
           <c:if test="${fn:containsIgnoreCase(promoVariance,'Fixed')}">
           		<c:set var="promoBVariance"><fmt:message key='finance.disc.promo.at.an.txt'/></c:set>
           		<c:set var="varianceDetails"><fmt:message key='finance.disc.promo.please.see.txt'/></c:set>
           </c:if>
           <c:if test="${fn:containsIgnoreCase(promoVariance,'Variable')}"> 
          	 	<c:set var="promoBVariance"><fmt:message key='finance.disc.promo.at.variable.txt'/></c:set>
          	 	<c:set var="varianceDetails"><fmt:message key='finance.disc.promo.apr.vary.txt'/></c:set>
           </c:if>
           </c:when>
          <c:when test="${empty promoFinanceRate}">
           <c:set var="varianceDetails"><fmt:message key='finance.disc.promo.at.the.apr.txt'/></c:set>
           </c:when>
           </c:choose>
           <c:set var="promoAuthStandIn"><fmt:message key='finance.disc.promo.discount.applied.txt'/></c:set>
           {"promoResponseCode":"${promoResponseCode}","promoText":"${promoText}","promoFinanceRate":"${promoFinanceRate}","financingTermsAvailable":"${financingTermsAvailable}","promoBVariance":"${promoBVariance}","promoAuthStandIn":"${promoAuthStandIn}","varianceDetails":"${varianceDetails}"}
          
           </dsp:oparam>
           </dsp:droplet>
	</c:when>
	<c:when test="${formID eq 'applyToday'}">
   		<dsp:getvalueof param="inputEmail" var="inputEmail" />
	    <dsp:droplet name="TRUGetSynchronyUrlDroplet">
			<dsp:param value="${order}" name="order" />
			<dsp:param name="inputEmail" value="${inputEmail}" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="redirectionUrl" param="redirectionUrl" vartype="java.lang.String" />
			</dsp:oparam>
		</dsp:droplet>
			<div>
				<div class="synchronyUrl">${redirectionUrl}</div>
			</div>
	</c:when>
	<c:when test="${formID eq 'rusCardBillingInfoEditForm'}">
		<dsp:include page="/checkout/payment/billingAddressForm.jsp">
			<dsp:param name="rusBillingEdit" value="true"/>
		</dsp:include>
	</c:when>
</c:choose>
</dsp:page>