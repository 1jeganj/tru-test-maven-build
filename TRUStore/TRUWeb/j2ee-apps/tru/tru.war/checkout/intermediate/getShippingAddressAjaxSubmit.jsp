<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/com/tru/droplet/GetShippingAddressDroplet"/>
	<dsp:getvalueof var="order" bean="ShoppingCart.current"/>
	<dsp:droplet name="GetShippingAddressDroplet">
		<dsp:param name="order" value="${order}"/>
		<dsp:oparam name="output">
        	<dsp:getvalueof var="shippAddress" param="shippAddress"/>
			<dsp:getvalueof var="cardinalRequestId" param="cardinalRequestId"/>
			{
			 "shippAddress": ${shippAddress},
			 "cardinalRequestId": ${cardinalRequestId}
			} 
        </dsp:oparam>
	</dsp:droplet>
</dsp:page>