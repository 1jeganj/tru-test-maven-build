<dsp:page>

<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/CheckoutProfileFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/atg/dynamo/droplet/IsNull" />
<dsp:importbean bean="/atg/commerce/promotion/CouponFormHandler" />
<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUGetSynchronyUrlDroplet" />
<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUParseSynchronyResponseDroplet" />
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
<dsp:getvalueof var="order" bean="ShoppingCart.current" />
<dsp:getvalueof var="truConf" bean="TRUConfiguration" />
<dsp:getvalueof var="formID" param="formID"></dsp:getvalueof>
<dsp:getvalueof var="fromPageName" param="fromPageName"></dsp:getvalueof>
<dsp:getvalueof var="action" param="action"></dsp:getvalueof>
<dsp:getvalueof var="financingTermsAvailable" value="${order.financingTermsAvailable}"/>
 <c:choose>
 	<c:when test="${formID eq 'payPalOrderReviewForm'}">
		<dsp:setvalue bean="BillingFormHandler.email" paramvalue="orderEmail"/>
		<%--Start: Added for adding rewards number to order  --%>
        <dsp:setvalue bean="BillingFormHandler.rewardNumberValid" paramvalue="isRewardsCardValid" />
		<dsp:setvalue bean="BillingFormHandler.rewardNumber" paramvalue="rewardNumber" /> 
		<%--End: Added for adding rewards number to order  --%>
		<dsp:setvalue bean="BillingFormHandler.payPalPaymentToReviewOrder" value="submit"/>
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="BillingFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="errorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	              <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
            <dsp:oparam name="false">
				 
            </dsp:oparam>
         </dsp:droplet>
   </c:when>
	<c:when test="${formID eq 'payPalOrderSubmitForm'}">
		<dsp:setvalue bean="BillingFormHandler.email" paramvalue="orderEmail"/>
		<%--Start: Added for adding rewards number to order  --%>
        <dsp:setvalue bean="BillingFormHandler.rewardNumberValid" paramvalue="isRewardsCardValid" />
		<dsp:setvalue bean="BillingFormHandler.rewardNumber" paramvalue="rewardNumber" /> 
		<%--End: Added for adding rewards number to order  --%>
		<dsp:setvalue bean="BillingFormHandler.payPalToken" paramvalue="tokenExist"/>
		<dsp:setvalue bean="BillingFormHandler.payPalEmailValidate" value="submit"/>
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="BillingFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="errorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
            <dsp:oparam name="false">
				 address-saved-success-from-ajax: 
            </dsp:oparam>
         </dsp:droplet>
   </c:when>
   <c:when test="${formID eq 'checkoutWithPayPal'}">
		<dsp:setvalue bean="BillingFormHandler.checkoutWithPayPal" value="submit"/>
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="BillingFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="payPalErrorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
           <dsp:oparam name="false">
           	 token:<dsp:valueof bean="/com/tru/common/TRUUserSession.paypalPaymentDetailsMap.payPalToken"/>
           </dsp:oparam>
         </dsp:droplet>
   </c:when>
	<c:when test="${formID eq 'PayPalpaymentType'}">
			<dsp:setvalue bean="BillingFormHandler.updateDataInOrder" value="true"/>
			<dsp:droplet name="Switch">
				<dsp:param bean="BillingFormHandler.formError" name="value" />
				<dsp:oparam name="true">
					<dsp:droplet name="ErrorMessageForEach">
						<dsp:param name="exceptions" bean="BillingFormHandler.formExceptions" />
						<dsp:oparam name="outputStart">
							Following are the form errors:<br />
						</dsp:oparam>
						<dsp:oparam name="output">
							<li class="redColor"><dsp:valueof param="message" /></li>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="false">
					<div>
						<div id="checkoutOrderSummaryResponse">
							<dsp:include page="/checkout/common/checkoutOrderSummary.jsp"/>
						</div>
						<div id="displayGiftCardsResponse">
							<dsp:getvalueof var="gcAppliedCount" param="gcAppliedCount"/>
							<c:if test="${gcAppliedCount gt 0}">
								<dsp:include page="/checkout/payment/displayGiftCards.jsp"/>
							</c:if>
						</div>
					</div>					 
				</dsp:oparam>
			</dsp:droplet>
	</c:when>
	<c:when test="${formID eq 'cartSetExpressCheckoutWithPayPalForm'}">
 		 <dsp:setvalue bean="CartModifierFormHandler.setExpressCheckoutWithPayPal" value="submit"/>
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="CartModifierFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="CartModifierFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="payPalErrorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
           <dsp:oparam name="false">
           	 token:<dsp:valueof bean="CartModifierFormHandler.payPalToken"/>
           </dsp:oparam>
         </dsp:droplet>
   </c:when>  
<c:when test="${formID eq 'payPalSetExpress'}">
		<dsp:setvalue bean="CommitOrderFormHandler.rewardNumberValid" paramvalue="isRewardsCardValid" />
		<dsp:setvalue bean="CommitOrderFormHandler.rewardNumber" paramvalue="rewardNumber" /> 
		<dsp:setvalue bean="CommitOrderFormHandler.deviceID" paramvalue="deviceID" />
		<dsp:setvalue bean="CommitOrderFormHandler.browserAcceptEncoding" paramvalue="browserAcceptEncoding" /> 
		<dsp:setvalue bean="CommitOrderFormHandler.payPalSetExpress" value="submit"/>
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="CommitOrderFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="CommitOrderFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="errorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
            <dsp:oparam name="false">
				 address-saved-success-from-ajax: 
            </dsp:oparam>
         </dsp:droplet>
   </c:when>  
<c:when test="${formID eq 'checkoutWithPayPalFromOrderReview'}">
		<dsp:setvalue bean="CommitOrderFormHandler.checkoutWithPayPalFromOrderReview" value="submit"/>
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="CommitOrderFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="CommitOrderFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="payPalErrorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
           <dsp:oparam name="false">
           	 token:<dsp:valueof bean="/com/tru/common/TRUUserSession.paypalPaymentDetailsMap.payPalToken"/>
           </dsp:oparam>
         </dsp:droplet>
   </c:when>
<c:when test="${formID eq 'expressCheckoutWithPayPal'}">
		<dsp:setvalue bean="CommitOrderFormHandler.rewardNumberValid" paramvalue="isRewardsCardValid" />
		<dsp:setvalue bean="CommitOrderFormHandler.rewardNumber" paramvalue="rewardNumber" /> 
		<dsp:setvalue bean="CommitOrderFormHandler.deviceID" paramvalue="deviceID" />
		<dsp:setvalue bean="CommitOrderFormHandler.browserAcceptEncoding" paramvalue="browserAcceptEncoding" /> 
		<dsp:setvalue bean="CommitOrderFormHandler.expressCheckoutWithPayPal" value="submit"/>
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="CommitOrderFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="CommitOrderFormHandler.formExceptions" />
	             <dsp:oparam name="outputStart">
	             	Following are the form errors: <span class="errorDisplay">
	             </dsp:oparam>
	              <dsp:oparam name="output">
	                  <span><dsp:valueof param="message"/></span>
	               </dsp:oparam>
	               <dsp:oparam name="outputEnd">
						</span>
				   </dsp:oparam>
	           </dsp:droplet>
           </dsp:oparam>
         </dsp:droplet>
	</c:when>
</c:choose>


</dsp:page>