<dsp:page>

	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>

	<dsp:getvalueof var="nickname" param="nickname" scope="request" /> 
	<dsp:getvalueof var="firstName" param="firstName" scope="request" />
	<dsp:getvalueof var="lastName" param="lastName" scope="request" />
	<dsp:getvalueof var="address1" param="address1" scope="request" />
	<dsp:getvalueof var="address2" param="address2" scope="request" />
	<dsp:getvalueof var="city" param="city" scope="request" />
	<dsp:getvalueof var="state" param="state" scope="request" />
	<dsp:getvalueof var="postalCode" param="postalCode" scope="request" />
	<dsp:getvalueof var="country" param="country" scope="request" />
	<dsp:getvalueof var="phoneNumber" param="phoneNumber" scope="request" />
	<dsp:getvalueof var="email" param="email" scope="request" />
	<dsp:getvalueof var="isEditSavedAddress" param="isEditSavedAddress" scope="request" />
	<dsp:getvalueof var="billingAddressSameAsShippingAddress" param="billingAddressSameAsShippingAddress" scope="request" />
	<c:if test="${billingAddressSameAsShippingAddress eq true }">
		<dsp:setvalue bean="ShippingGroupFormHandler.billingAddressSameAsShippingAddress" value="${billingAddressSameAsShippingAddress}" />
	</c:if>
	<dsp:setvalue bean="ShippingGroupFormHandler.newShipToAddressName" paramvalue="nickname" /> 
	<dsp:setvalue bean="ShippingGroupFormHandler.address.firstName" paramvalue="firstName" />
	<dsp:setvalue bean="ShippingGroupFormHandler.address.lastName" paramvalue="lastName" />
	<dsp:setvalue bean="ShippingGroupFormHandler.address.address1" paramvalue="address1" />
	<dsp:setvalue bean="ShippingGroupFormHandler.address.address2" paramvalue="address2" />
	<dsp:setvalue bean="ShippingGroupFormHandler.address.city" paramvalue="city" />
	<dsp:setvalue bean="ShippingGroupFormHandler.address.postalCode" paramvalue="postalCode" />
	<dsp:setvalue bean="ShippingGroupFormHandler.address.country" paramvalue="country" />
	<dsp:setvalue bean="ShippingGroupFormHandler.address.phoneNumber" paramvalue="phoneNumber" />
	<dsp:setvalue bean="ShippingGroupFormHandler.address.email" paramvalue="email" />
	<dsp:setvalue bean="ShippingGroupFormHandler.addressValidated" paramvalue="addressValidated" />
	<dsp:getvalueof var="country" param="country" />
	
	<c:choose>
		<c:when test="${country ne 'US'}">
			<dsp:setvalue bean="ShippingGroupFormHandler.address.state" paramvalue="otherState" />
		</c:when>
		<c:otherwise>
			<dsp:setvalue bean="ShippingGroupFormHandler.address.state" paramvalue="state" />
		</c:otherwise>
	</c:choose>
	
	<dsp:droplet name="Switch">
		<dsp:param name="value" param="isEditSavedAddress" />
		<dsp:oparam name="true">
			<dsp:setvalue bean="ShippingGroupFormHandler.editShippingAddress" value="submit" />
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:setvalue bean="ShippingGroupFormHandler.addShippingAddress" value="submit" />
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="Switch">
		<dsp:param bean="ShippingGroupFormHandler.formError" name="value"/>
		<dsp:oparam name="true">
			<dsp:droplet name="ErrorMessageForEach">
		     	<dsp:param name="exceptions" bean="ShippingGroupFormHandler.formExceptions"/>
				<dsp:oparam name="outputStart">
					Following are the form errors:
				</dsp:oparam>
				<dsp:oparam name="output">
		  			 <dsp:valueof param="message"/>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:droplet name="Switch">
				<dsp:param name="value" bean="ShippingGroupFormHandler.addressDoctorResponseVO.derivedStatus" />
				<dsp:oparam name="NO">
					<dsp:include page="/checkout/shipping/addressAddEditOverlay.jsp"/>
				</dsp:oparam>
				<dsp:oparam name="YES">
					<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
						<dsp:param name="mode" value="addressBookFromShipping" />
					</dsp:include>
				</dsp:oparam>
				<%-- <dsp:oparam name="VALIDATION ERROR">
					<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
						<dsp:param name="mode" value="addressBookFromShipping" />
					</dsp:include>
				</dsp:oparam> --%>
				<dsp:oparam name="default">
					<dsp:include page="/checkout/shipping/addressAddEditOverlay.jsp"/>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>