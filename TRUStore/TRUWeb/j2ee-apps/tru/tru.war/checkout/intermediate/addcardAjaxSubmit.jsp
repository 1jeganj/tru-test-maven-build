<dsp:page>

    <dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler"/>
	<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
	<dsp:getvalueof var="truConf" bean="TRUConfiguration" />
	
	<dsp:getvalueof var="nameOnCard" param="nameOnCard" scope="request" />
	<dsp:getvalueof var="creditCardNumber" param="creditCardNumber" scope="request" />
	<dsp:getvalueof var="cOlBinNum" param="cOlBinNum" scope="request" />
	<dsp:getvalueof var="cOlLength" param="cOlLength" scope="request" />
	<dsp:getvalueof var="expirationMonth" param="expirationMonth" scope="request" />
	<dsp:getvalueof var="expirationYear" param="expirationYear" scope="request" />
	<dsp:getvalueof var="selectedCardBillingAddress" param="selectedCardBillingAddress" scope="request" />
	<dsp:getvalueof var="billingAddressNickName" param="nickName" scope="request" />
	<dsp:getvalueof var="editCardNickName" param="editCardNickName" scope="request" />
	<dsp:getvalueof var="creditCardCVV" param="creditCardCVV" scope="request" />
	<dsp:getvalueof var="isEditSavedCard" param="isEditSavedCard" scope="request" />
	
	<dsp:getvalueof var="nickName" param="nickName" scope="request" />
	<dsp:getvalueof var="firstName" param="firstName" scope="request" />
	<dsp:getvalueof var="lastName" param="lastName" scope="request" />
	<dsp:getvalueof var="address1" param="address1" scope="request" />
	<dsp:getvalueof var="address2" param="address2" scope="request" />
	<dsp:getvalueof var="city" param="city" scope="request" />
	<dsp:getvalueof var="state" param="state" scope="request" />
	<dsp:getvalueof var="otherState" param="otherState" scope="request" />
	<dsp:getvalueof var="postalCode" param="postalCode" scope="request" />
	<dsp:getvalueof var="country" param="country" scope="request" />
	<dsp:getvalueof var="phoneNumber" param="phoneNumber" scope="request" />
	<dsp:getvalueof var="creditCardToken" param="ccToken" scope="request" />
	<dsp:getvalueof var="cBinNum" param="cBinNum" scope="request" />
	<dsp:getvalueof var="cLength" param="cLength" scope="request" />	
	
	<dsp:getvalueof var="ccNum" param="cBinNum" />
    
    <dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.nameOnCard" paramvalue="nameOnCard" />
    <dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardToken" value="${creditCardToken}" />
    <%-- <dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardNumber" value="${cBinNum}"/> --%>
	<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.cardLength" value="${cLength}"/>
	<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.expirationMonth" paramvalue="expirationMonth"/>
	<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.expirationYear" paramvalue="expirationYear"/>
	<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardType" paramvalue="creditCardType"/>
		
	<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardVerificationNumber" paramvalue="creditCardCVV" />
	
	<dsp:setvalue bean="BillingFormHandler.addressInputFields.country" paramvalue="country" />

	<dsp:setvalue bean="BillingFormHandler.addressInputFields.firstName" paramvalue="firstName" /> 
	<dsp:setvalue bean="BillingFormHandler.addressInputFields.lastName" paramvalue="lastName" />
	<dsp:setvalue bean="BillingFormHandler.addressInputFields.address1" paramvalue="address1" />
	<dsp:setvalue bean="BillingFormHandler.addressInputFields.address2" paramvalue="address2" />
	<dsp:setvalue bean="BillingFormHandler.addressInputFields.city" paramvalue="city" />
	<c:choose>
		<c:when test="${country ne 'US'}">
			<dsp:setvalue bean="BillingFormHandler.addressInputFields.state" paramvalue="otherState" />
		</c:when>
		<c:otherwise>
			<dsp:setvalue bean="BillingFormHandler.addressInputFields.state" paramvalue="state" />
		</c:otherwise>
	</c:choose>
	<dsp:setvalue bean="BillingFormHandler.addressInputFields.postalCode" paramvalue="postalCode" />
	<dsp:setvalue bean="BillingFormHandler.selectedCardBillingAddress" paramvalue="selectedCardBillingAddress" />
	<dsp:setvalue bean="BillingFormHandler.addressInputFields.phoneNumber" paramvalue="phoneNumber" />
	<dsp:setvalue bean="BillingFormHandler.billingAddressNickName" paramvalue="nickName"/>
	<dsp:setvalue bean="BillingFormHandler.editCardNickName" paramvalue="editCardNickName"/>
	<dsp:setvalue bean="BillingFormHandler.addressValidated" paramvalue="billingAddressValidated" />
	<dsp:setvalue bean="BillingFormHandler.addressInputFields.email" paramvalue="email" />
	
	
	
	<dsp:droplet name="Switch">
		<dsp:param name="value" param="isEditSavedCard" />
		<dsp:oparam name="true">
			<dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardNumber" value="${creditCardToken}"/>
			<dsp:setvalue bean="BillingFormHandler.editCreditCard" value="submit" />
		</dsp:oparam>
		<dsp:oparam name="default">
			<%-- <dsp:setvalue bean="BillingFormHandler.creditCardInfoMap.creditCardNumber" value="${cBinNum}"/>
			<dsp:setvalue bean="BillingFormHandler.addCreditCard" value="submit" /> --%>
		</dsp:oparam>
	</dsp:droplet>
	
	<dsp:droplet name="Switch">
		<dsp:param bean="BillingFormHandler.formError" name="value" />
		<dsp:oparam name="true">
			<dsp:droplet name="ErrorMessageForEach">
				<dsp:param name="exceptions"
					bean="BillingFormHandler.formExceptions" />
				<dsp:oparam name="outputStart">
					Following are the form errors:
				</dsp:oparam>
				<dsp:oparam name="output">
					<span><dsp:valueof param="message" /></span>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:droplet name="Switch">
				<dsp:param name="value" bean="BillingFormHandler.addressDoctorProcessStatus" />
				<dsp:oparam name="VERIFIED">
					<dsp:include page="/checkout/payment/creditCard/checkoutOverlay.jsp"/>
				</dsp:oparam>
				<dsp:oparam name="CORRECTED">
					<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
						<dsp:param name="mode" value="paymentPage" />
					</dsp:include>
				</dsp:oparam>
				<dsp:oparam name="VALIDATION ERROR">
					<dsp:include page="/checkout/intermediate/suggestedAddressAjaxResponse.jsp">
						<dsp:param name="mode" value="paymentPage" />
					</dsp:include>
				</dsp:oparam>
				<dsp:oparam name="default">
					<dsp:include page="/checkout/payment/creditCard/checkoutOverlay.jsp"/>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>	
	


</dsp:page>