<dsp:page>

<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/CheckoutProfileFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/atg/dynamo/droplet/IsNull" />
<dsp:importbean bean="/atg/commerce/promotion/CouponFormHandler" />
<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUGetSynchronyUrlDroplet" />
<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUParseSynchronyResponseDroplet" />
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
<dsp:getvalueof var="order" bean="ShoppingCart.current" />
<dsp:getvalueof var="truConf" bean="TRUConfiguration" />
<dsp:getvalueof var="formID" param="formID"></dsp:getvalueof>
<dsp:getvalueof var="fromPageName" param="fromPageName"></dsp:getvalueof>
<dsp:getvalueof var="action" param="action"></dsp:getvalueof>
<dsp:getvalueof var="financingTermsAvailable" value="${order.financingTermsAvailable}"/>

<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler" />
	<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftOptionsFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/com/tru/commerce/order/purchase/InStorePickUpFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler"/>
	
	
	<dsp:droplet name="Switch">
		<dsp:param name="value" param="action"/>
		<%--Start : Added to handle updating the ship methods zone on change of address1 & state--%>
		<dsp:oparam name="changeShipMethodZone">
			<dsp:setvalue bean="ShippingGroupFormHandler.state" paramvalue="state"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.address1" paramvalue="address1" />
			<dsp:setvalue bean="ShippingGroupFormHandler.city" paramvalue="city" />
			<dsp:setvalue bean="ShippingGroupFormHandler.changeShipMethodZone" value="true" />
		</dsp:oparam>
		<%--End : Added to handle updating the ship methods zone on change of address1 & state--%>
		
		<dsp:oparam name="changeShippingAddress">
			<dsp:setvalue bean="ShippingGroupFormHandler.shipToAddressName" paramvalue="shipToAddressName"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shippingMethod" paramvalue="shippingMethod" />
			<dsp:setvalue bean="ShippingGroupFormHandler.shippingPrice" paramvalue="shippingPrice"/>				
			<dsp:setvalue bean="ShippingGroupFormHandler.changeShippingAddress" value="true" />
		</dsp:oparam>
		<dsp:oparam name="shipToExistingAddress">
			<dsp:getvalueof var="moveToShippingAction" param="moveToShippingAction" />
			<c:if test="${not empty moveToShippingAction && moveToShippingAction eq 'moveToMultipleShip'}">
				<dsp:setvalue bean="ShippingGroupFormHandler.navigateToMultiShip" value="true" />
				<dsp:setvalue bean="ShippingGroupFormHandler.skipShippingRestrctions" value="true"/>
			</c:if>
			<dsp:setvalue bean="ShippingGroupFormHandler.showMeGiftingOptions" paramvalue="showMeGiftingOptions"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shipToAddressName" paramvalue="shipToAddressName"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shippingMethod" paramvalue="shippingMethod"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shippingPrice" paramvalue="shippingPrice"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shipToExistingAddress" value="true" />
		</dsp:oparam>
		<dsp:oparam name="shipToMultiAddress">			
			<dsp:setvalue bean="ShippingGroupFormHandler.shipToMultiAddress" value="true" />
		</dsp:oparam>
		<dsp:oparam name="changeShipMethodInReview">
			<dsp:setvalue bean="ShippingGroupFormHandler.estimateShipWindowMin" paramvalue="estimateShipWindowMin"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.estimateShipWindowMax" paramvalue="estimateShipWindowMax"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shipToAddressName" paramvalue="shipToAddressName"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shippingMethod" paramvalue="shippingMethod"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.eligibleShipMethodsCount" paramvalue="eligibleShipMethodsCount"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.selectedIndex" paramvalue="selectedIndex"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.changeShipMethodInReview" value="true" />
		</dsp:oparam>
		<dsp:oparam name="updateShipMethod">
			<dsp:setvalue bean="ShippingGroupFormHandler.shippingGroupId" paramvalue="shippingGroupId"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.relationShipId" paramvalue="relationShipId"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shipMethodVO.shipMethodCode" paramvalue="shipMethodCode"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shipMethodVO.shippingPrice" paramvalue="shippingPrice"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shipMethodVO.regionCode" paramvalue="regionCode"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.uniqueId" paramvalue="uniqueId"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.wrapCommerceItemId" paramvalue="wrapCommerceItemId"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.updateShipMethod" value="true" />
		</dsp:oparam>		
		<dsp:oparam name="applySingleShipping">
			<dsp:setvalue bean="ShippingGroupFormHandler.applySingleShipping" value="submit"/>
		</dsp:oparam>
		<dsp:oparam name="selectExistingAddress">
			<dsp:setvalue bean="ShippingGroupFormHandler.shippingGroupId" paramvalue="shippingGroupId"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.relationShipId" paramvalue="relationShipId"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.shipToAddressName" paramvalue="shipToAddressName"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.uniqueId" paramvalue="uniqueId"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.wrapCommerceItemId" paramvalue="wrapCommerceItemId"/>
			<dsp:setvalue bean="ShippingGroupFormHandler.selectExistingAddress" value="submit"/>
		</dsp:oparam>
		<dsp:oparam name="closeAddressModel">
			<dsp:setvalue bean="ShippingGroupFormHandler.closeAddressModel" value="true" />
		</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="action" param="action"/>
	<c:choose>
		<c:when test="${action eq 'shipToExistingAddress' || action eq 'shipToMultiAddress'}">
			<dsp:droplet name="Switch">
		         <dsp:param bean="ShippingGroupFormHandler.formError" name="value" />
		         <dsp:oparam name="true">
		         	{
		         		"success": false,
		         		"orderReconciliationNeeded" : "<dsp:valueof bean="ShippingGroupFormHandler.orderReconciliationNeeded"/>",
		         		"shipRestrictionsFound":"<dsp:valueof bean="ShoppingCart.current.shipRestrictionsFound"/>",
		         		"errorMessages" : [
							<dsp:droplet name="ErrorMessageForEach">
								<dsp:param name="exceptions" bean="ShippingGroupFormHandler.formExceptions" />	
								<dsp:oparam name="output">
									<dsp:getvalueof var="count" param="count" />
									<dsp:getvalueof var="size" param="size" />
									<c:choose>
											<c:when test="${count eq size}">											
												{"message": "<dsp:valueof param="message" />","divId": "<dsp:valueof param="errorCode" />"}
											 
											</c:when>
											<c:otherwise>											
												{"message": "<dsp:valueof param="message" />","divId": "<dsp:valueof param="errorCode" />"},
											</c:otherwise>
										</c:choose>
								</dsp:oparam>
							</dsp:droplet>
						]
					}
				</dsp:oparam>
				<dsp:oparam name="false">					
					{"success": true}
				</dsp:oparam>
			</dsp:droplet>
		</c:when>		
		<c:otherwise>
			<dsp:getvalueof var="shipRestrictionsFound" bean="ShoppingCart.current.shipRestrictionsFound"/>
			<dsp:droplet name="ErrorMessageForEach">			
				<dsp:param name="exceptions" bean="ShippingGroupFormHandler.formExceptions" />	
				<dsp:oparam name="output">				
					 	{"success":"false","shipRestrictionsFound":"${shipRestrictionsFound}"}
				</dsp:oparam>
				<dsp:oparam name="empty">
					 	{"success":"true"}
				</dsp:oparam>
			</dsp:droplet> 
		</c:otherwise>
	</c:choose>
</dsp:page>