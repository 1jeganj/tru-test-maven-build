<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/CheckoutProfileFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/atg/dynamo/droplet/IsNull" />
<dsp:importbean bean="/atg/commerce/promotion/CouponFormHandler" />
<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUGetSynchronyUrlDroplet" />
<dsp:importbean bean="/com/tru/commerce/order/droplet/TRUParseSynchronyResponseDroplet" />
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
<dsp:getvalueof var="order" bean="ShoppingCart.current" />
<dsp:getvalueof var="truConf" bean="TRUConfiguration" />
<dsp:getvalueof var="formID" param="formID"></dsp:getvalueof>
<dsp:getvalueof var="fromPageName" param="fromPageName"></dsp:getvalueof>
<dsp:getvalueof var="action" param="action"></dsp:getvalueof>
<dsp:getvalueof var="financingTermsAvailable" value="${order.financingTermsAvailable}"/>

<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler" />
	<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftOptionsFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/com/tru/commerce/order/purchase/InStorePickUpFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler"/>
	
 <c:choose>
	<c:when test="${formID eq 'removeCartItemFromOrderReview'}">
			<dsp:getvalueof var="relationshipId" param="relationshipId"></dsp:getvalueof>
			<c:set var="commerceItemIdArray" value="${fn:split(relationshipId, ' ')}" />
			<dsp:setvalue bean="CartModifierFormHandler.removalRelationshipIds" value="${commerceItemIdArray}" />
			<dsp:setvalue bean="CartModifierFormHandler.removeItemFromOrderByRelationshipId" value="submit" />
			<dsp:droplet name="Switch">
				<dsp:param bean="CartModifierFormHandler.formError" name="value" />
				<dsp:oparam name="true">
					<dsp:droplet name="ErrorMessageForEach">
						<dsp:param name="exceptions" bean="CartModifierFormHandler.formExceptions" />
						<dsp:oparam name="outputStart">
							Following are the form errors:<br />
						</dsp:oparam>
						<dsp:oparam name="output">
							<li class="redColor"><dsp:valueof param="message" /></li>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="false">
					 <dsp:include page="/checkout/review/orderReviewInclude.jsp" /> 
				</dsp:oparam>
			</dsp:droplet> 
	</c:when>
</c:choose>

	
	
	<dsp:droplet name="Switch">
		<dsp:param name="value" param="action"/>
		<dsp:oparam name="editCartItemInReview">
			<dsp:getvalueof var="productId" param="productId" />
			<dsp:getvalueof var="quantity" param="quantity" />
			<dsp:getvalueof var="pageName" param="pageName" />
			<dsp:getvalueof  var="skuId" param="skuId" />
			<dsp:getvalueof  var="commItemId" param="updateCommerItemIds" />
			<dsp:getvalueof  var="relationShipId" param="relationShipId" />
			<dsp:getvalueof  var="editCartFromPage" param="editCartFromPage" />
			<dsp:setvalue bean="CartModifierFormHandler.productId" value="${productId}" />
			<c:set var="catalogRefIds" value="${fn:split(skuId, ' ')}" />
			<c:set var="commItemIds" value="${fn:split(commItemId, ' ')}" />
			<dsp:setvalue bean="CartModifierFormHandler.catalogRefIds" value="${catalogRefIds}" />
			<dsp:setvalue bean="CartModifierFormHandler.pageName" value="${pageName}" />
			<dsp:setvalue bean="CartModifierFormHandler.quantity" value="${quantity}" />
			<dsp:setvalue bean="CartModifierFormHandler.updateCommerItemIds" value="${commItemIds}"/>
			<dsp:setvalue bean="CartModifierFormHandler.relationShipId" value="${relationShipId}" />
			<dsp:setvalue bean="CartModifierFormHandler.giftWrapSkuId" paramvalue="wrapSkuId" />
			<dsp:setvalue bean="CartModifierFormHandler.editCartFromPage" value="${editCartFromPage}"/>
            <dsp:setvalue bean="CartModifierFormHandler.updateCommerceItem" value="update"/>
            
            
            <c:set value="" var="existingRelationShipId"/>
            <dsp:droplet name="Switch">
				<dsp:param bean="CartModifierFormHandler.formError" name="value" />
				<dsp:oparam name="true">
				   <c:set value="${relationShipId}" var="existingRelationShipId"/>
					<dsp:droplet name="ErrorMessageForEach">
						<dsp:param name="exceptions" bean="CartModifierFormHandler.formExceptions" />
						<dsp:oparam name="outputStart">
						Following are the form errors:
						<div class="edit-review-tab-error">
							<dsp:getvalueof  var="inventoryLimitForReviewPage" bean="CartModifierFormHandler.inventoryLimitForReviewPage" />
							<input type="hidden" id="inventoryLimitForReviewPage" value="${inventoryLimitForReviewPage}" />
							<input type="hidden" id="checkoutShipPageRelationShipId" value="${relationShipId}" />
							<div class="shopping-cart-error-state">
							<div class="shopping-cart-error-state-header row">
								<div class="error-state-exclamation-lg img-responsive col-md-2"></div>
								<div class="shopping-cart-error-state-header-text col-md-10">
									<span class="errorHeader"><fmt:message key="shoppingcart.oops.issue" /></span>
									<div class="shopping-cart-error-state-subheader">
						</dsp:oparam>
						<dsp:oparam name="output">
							<dsp:droplet name="Switch">
								<dsp:param param="errorCode" name="value" />
								<dsp:oparam name="top_msg">
									<dsp:valueof param="message" valueishtml="true" />
								</dsp:oparam>
								<dsp:oparam name="inline_msg">
									<dsp:getvalueof var="inline_error_msg" param="message" />
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
						<dsp:oparam name="outputEnd">
							</div>
							</div>
							</div>
							</div>
							</div>
						</dsp:oparam>
					</dsp:droplet>
					<div class="cart_inline_msg">
						<span class="cart-error-message"><dsp:valueof value="${inline_error_msg}" valueishtml="true" /></span>
					</div>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="removeCartItemInReview">
			<dsp:droplet name="Switch">
				<dsp:param param="updateOrRemove" name="value" />
				<dsp:oparam name="update">
				 	<dsp:getvalueof var="relationShipId" param="commerceItemId" />
					<dsp:setvalue bean="CartModifierFormHandler.splitItemRemove" value="true" />
					<dsp:setvalue bean="CartModifierFormHandler.relationShipId"	paramvalue="commerceItemId" />
					<dsp:setvalue bean="CartModifierFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
					<dsp:setvalue bean="CartModifierFormHandler.wrapItemQuantity" paramvalue="quantity" />
					<dsp:setvalue bean="CartModifierFormHandler.editCartFromPage" value="Review"/>
					<dsp:setvalue bean="CartModifierFormHandler.setOrderByRelationshipId" value="true" />
				</dsp:oparam>
				<dsp:oparam name="remove">
					<dsp:getvalueof param="commerceItemId" var="commerceItemId" />
					<c:set var="commerceItemIdArray" value="${fn:split(commerceItemId, ' ')}" />
					<dsp:setvalue bean="CartModifierFormHandler.relationShipId"	paramvalue="commerceItemId" />
					<dsp:setvalue bean="CartModifierFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
					<dsp:setvalue bean="CartModifierFormHandler.wrapItemQuantity" paramvalue="quantity" />
					<dsp:setvalue bean="CartModifierFormHandler.removalRelationshipIds"	value="${commerceItemIdArray}" />
					<dsp:setvalue bean="CartModifierFormHandler.editCartFromPage" value="Review"/>
					<dsp:setvalue bean="CartModifierFormHandler.removeItemFromOrderByRelationshipId" value="submit" />
				</dsp:oparam>
				<dsp:oparam name="outOfStock">
					<dsp:setvalue bean="CartModifierFormHandler.outOfStockId" paramvalue="commerceItemId" />
					<dsp:setvalue bean="CartModifierFormHandler.editCartFromPage" value="Review"/>
					<dsp:setvalue bean="CartModifierFormHandler.removeOutOfStockItem" value="submit" />
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="removeCartItemInShipping">
		 <dsp:droplet name="Switch">
				<dsp:param param="updateOrRemove" name="value" />
				<dsp:oparam name="update">
				 	<dsp:getvalueof var="relationShipId" param="commerceItemId" />
					<dsp:setvalue bean="CartModifierFormHandler.splitItemRemove" value="true" />
					<dsp:setvalue bean="CartModifierFormHandler.relationShipId"	paramvalue="commerceItemId" />
					<dsp:setvalue bean="CartModifierFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
					<dsp:setvalue bean="CartModifierFormHandler.wrapItemQuantity" value="1" />
					<dsp:setvalue bean="CartModifierFormHandler.setOrderByRelationshipId" value="true" />
				</dsp:oparam>
				<dsp:oparam name="remove"> 
					<dsp:getvalueof param="commerceItemId" var="commerceItemId" />
					<c:set var="commerceItemIdArray" value="${fn:split(commerceItemId, ' ')}" />
					<dsp:setvalue bean="CartModifierFormHandler.relationShipId"	paramvalue="commerceItemId" />
					<dsp:setvalue bean="CartModifierFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
					<dsp:setvalue bean="CartModifierFormHandler.wrapItemQuantity" value="1" />
					<dsp:setvalue bean="CartModifierFormHandler.removalRelationshipIds"	value="${commerceItemIdArray}" />
					<dsp:setvalue bean="CartModifierFormHandler.removeItemFromOrderByRelationshipId" value="submit" />
				 </dsp:oparam>
			</dsp:droplet> 
		 	<%-- <dsp:getvalueof var="relationShipId" param="commerceItemId" />
			<dsp:setvalue bean="CartModifierFormHandler.splitItemRemove" value="true" />
			<dsp:setvalue bean="CartModifierFormHandler.relationShipId"	paramvalue="commerceItemId" />
			<dsp:setvalue bean="CartModifierFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
			<dsp:setvalue bean="CartModifierFormHandler.wrapItemQuantity" paramvalue="quantity" />
			<dsp:setvalue bean="CartModifierFormHandler.setOrderByRelationshipId" value="true" /> --%>
		</dsp:oparam>
		<dsp:oparam name="removeDonationItemInReview">
			<dsp:getvalueof param="commerceItemId" var="commerceItemId"></dsp:getvalueof>
			<c:set var="commerceItemIdArray" value="${fn:split(commerceItemId, ' ')}" />
			<dsp:setvalue bean="CartModifierFormHandler.removalCommerceIds" value="${commerceItemIdArray}" />
			<dsp:setvalue bean="CartModifierFormHandler.removeItemFromOrder" value="submit" />
		</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="action" param="action"/>
	<c:choose>
		<c:when test="${action eq 'removeCartItemInReview'}">
			<dsp:droplet name="ErrorMessageForEach">
				<dsp:param name="exceptions" bean="CartModifierFormHandler.formExceptions" />	
				<dsp:oparam name="output">
					 	{"success":"false"}
				</dsp:oparam>
				<dsp:oparam name="empty">
						<dsp:getvalueof var="isInStorePickupCIAvailable" value="false"/>
						<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="ShoppingCart.current.shippingGroups" />
						<dsp:param name="elementName" value="shippingGroup" />
							<dsp:oparam name="output">							
								<dsp:getvalueof param="shippingGroup.shippingGroupClassType" var="shippingGroupType" />
									<c:if test="${shippingGroupType eq 'inStorePickupShippingGroup'}">
										<dsp:getvalueof var="isInStorePickupCIAvailable" value="true"/>
									</c:if>
							</dsp:oparam>
						</dsp:droplet>
					 	{"success": true,"commerceItemCount" : "<dsp:valueof bean="ShoppingCart.current.totalCommerceItemCount"/>","inStoreSG" : "<dsp:valueof value="${isInStorePickupCIAvailable}"/>"}
				</dsp:oparam>
			</dsp:droplet> 		
		</c:when>
		<c:when test="${action eq 'removeDonationItemInReview'}">
			<dsp:droplet name="ErrorMessageForEach">
				<dsp:param name="exceptions" bean="CartModifierFormHandler.formExceptions" />	
				<dsp:oparam name="output">
					 	{"success":"false"}
				</dsp:oparam>
				<dsp:oparam name="empty">
					 	{"success": true,"commerceItemCount" : "<dsp:valueof bean="ShoppingCart.current.totalCommerceItemCount"/>"}
				</dsp:oparam>
			</dsp:droplet> 		
		</c:when>			
		<c:when test="${action eq 'editCartItemInReview'}">
		</c:when>
		<c:otherwise>
			<dsp:droplet name="ErrorMessageForEach">			
				<dsp:param name="exceptions" bean="CartModifierFormHandler.formExceptions" />	
				<dsp:oparam name="output">				
					 	{"success":"false"}
				</dsp:oparam>
				<dsp:oparam name="empty">
					 	{"success":"true"}
				</dsp:oparam>
			</dsp:droplet> 
		</c:otherwise>
	</c:choose>
</dsp:page>