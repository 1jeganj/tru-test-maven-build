<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>

	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/LayawayPaymentInfoFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>

	<dsp:getvalueof var="mode" param="mode" />
	<dsp:getvalueof var="action" param="action" />
	<dsp:getvalueof var="isEditSavedAddress" param="isEditSavedAddress" />
	<dsp:getvalueof var="billingAddressSameAsShippingAddress" vartype="java.lang.boolean" param="billingAddressSameAsShippingAddress" />
	<%-- <dsp:getvalueof var="isEditSavedCard" param="isEditSavedCard" /> --%>
	<c:choose>
		<c:when test="${mode eq 'paymentCard' or mode eq 'paymentPage' or mode eq 'paymentInStore'}">
			<dsp:getvalueof var="formHandler" value="BillingFormHandler"/>
		</c:when>
		<c:when test="${mode eq 'address' or  mode eq 'addressBookFromShipping'}">
			<dsp:getvalueof var="formHandler" value="ShippingGroupFormHandler"/>
		</c:when>
		<c:when test="${mode eq 'layaway'}">
			<dsp:getvalueof var="formHandler" value="LayawayPaymentInfoFormHandler"/>
		</c:when>
		<c:when test="${mode eq 'placeOrder'}">
			<dsp:getvalueof var="formHandler" value="CommitOrderFormHandler"/>
		</c:when>
	</c:choose>
	<div class="modal-content addressDoctor sharp-border">
		<div class="address-doctor">
			<div>
				<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close" id="closeIt"></span></a>
			</div>
			<h3><fmt:message key="myaccount.addressdoctor.header" /></h3>
			<p class="address-doctor-p"><fmt:message key="myaccount.addressdoctor.address.suggestions" /></p>
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" bean="${formHandler}.addressDoctorResponseVO" />
				<dsp:oparam name="false">
					<dsp:getvalueof var="addressRecognized" vartype="java.lang.boolean" bean="${formHandler}.addressDoctorResponseVO.addressRecognized" />
					<c:choose>
						<c:when test="${not addressRecognized}">
							<p>
								<fmt:message key="myaccount.address.not.recognised" />
								<fmt:message key="myaccount.address.choose.existing.address" />
								<fmt:message key="myaccount.address.enter.new" />
							</p>
						</c:when>
					</c:choose>
					<p class="global-error-display"></p>
					<div class="address-doctor-current">
						<p><fmt:message key="myaccount.use.entered.address" /></p>
						 <div>
							<c:choose>
								<c:when test="${not addressRecognized}">
									<input name="valid-address" class="tabout-address-doctor" onclick="suggestedShippingAddressSelect('1');" type="radio" id="suggestedAddressCount-0" value="0" checked>
								</c:when>
								<c:otherwise>
									<input name="valid-address" class="tabout-address-doctor" onclick="suggestedShippingAddressSelect('1');" type="radio" id="suggestedAddressCount-0" value="0" checked>
								</c:otherwise>
							</c:choose>		
							<p class="address-doctor-address">							
								${address1}<c:if test="${not empty address2}">&nbsp;${address2}</c:if>, ${city}, <c:choose><c:when test="${country ne 'US'}">${otherState}</c:when><c:otherwise>${state}</c:otherwise></c:choose>, ${postalCode} <span>&#xb7;</span>
								<c:choose>
									<c:when test="${mode eq 'paymentCard' || mode eq 'placeOrder' || mode eq 'paymentInStore'}">
										<a href="#" class="tabout-address-doctor" onclick="editUserEnteredPaymentAddress()"><fmt:message key="myaccount.addressdoctor.edit" /></a>
									</c:when>
									<c:when test="${mode eq 'address'}">
										<a href="#" class="tabout-address-doctor" onclick="editUserEnteredShippingAddress()"><fmt:message key="myaccount.addressdoctor.edit" /></a>
									</c:when>
									<c:when test="${mode eq 'addressBookFromShipping'}">
										<a href="#" class="tabout-address-doctor" onclick="editUserEnteredAddressFromShippingOverlay()"><fmt:message key="myaccount.addressdoctor.edit" /></a>
									</c:when>
									<c:when test="${mode eq 'paymentPage'}">
										<a href="#" class="tabout-address-doctor" onclick="editUserEnteredAddressFromPaymentOverlay()"><fmt:message key="myaccount.addressdoctor.edit" /></a>
									</c:when>
									<c:when test="${mode eq 'layaway'}">
										<a href="#" class="tabout-address-doctor" onclick="editUserEnteredLayawayAddress()"><fmt:message key="myaccount.addressdoctor.edit" /></a>
									</c:when>
								</c:choose>
							</p>
							<input type="hidden" name="suggestedAddress1-0" id="suggestedAddress1-0" value="${address1}" maxlength="50"/>
							<input type="hidden" name="suggestedAddress2-0" id="suggestedAddress2-0" value="${address2}" maxlength="50"/>
							<input type="hidden" name="suggestedCity-0" id="suggestedCity-0" value="${city}" maxlength="30"/>
							<c:choose>
								<c:when test="${country ne 'US'}">
									<input type="hidden" name="suggestedState-0" id="suggestedState-0" value="${otherState}" />
								</c:when>
								<c:otherwise>
									<input type="hidden" name="suggestedState-0" id="suggestedState-0" value="${state}" />
								</c:otherwise>
							</c:choose>
							<input type="hidden" name="suggestedPostalCode-0" id="suggestedPostalCode-0" value="${postalCode}" />
							<input type="hidden" name="suggestedCountry-0" id="suggestedCountry-0" value="${country}" />
						</div>
					</div>
					<dsp:droplet name="IsEmpty">
						<dsp:param name="value" bean="${formHandler}.addressDoctorResponseVO.addressDoctorVO" />
						<dsp:oparam name="false">
							<div class="address-doctor-alt">
								<p><fmt:message key="myaccount.did.you.mean.one.of.following" /></p>			
								<dsp:droplet name="ForEach">
									<dsp:param name="array" bean="${formHandler}.addressDoctorResponseVO.addressDoctorVO" />
									<dsp:param name="elementName" value="addressDoctorVO" />
									<dsp:oparam name="output">
										<dsp:getvalueof var="size" param="size"/>
										<dsp:getvalueof var="suggestedAddressCount" param="addressDoctorVO.count"/>
										<dsp:getvalueof var="addressDoctorVO" param="addressDoctorVO"/>
										<div>
										<c:choose>
											<c:when test="${suggestedAddressCount == 1}">
												<input name="valid-address" class="tabout-address-doctor" onclick="suggestedShippingAddressSelect('2');" type="radio" id="suggestedAddressCount-${suggestedAddressCount}" value="${suggestedAddressCount}" checked>
											</c:when>
											<c:otherwise>
												<input name="valid-address" class="tabout-address-doctor" onclick="suggestedShippingAddressSelect('2');" type="radio" id="suggestedAddressCount-${suggestedAddressCount}" value="${suggestedAddressCount}">
											</c:otherwise>
										</c:choose>
											
											<p class="address-doctor-address">
												<dsp:valueof param="addressDoctorVO.fullAddressName" />
											</p>
											<input type="hidden" name="suggestedAddress1-${suggestedAddressCount}" id="suggestedAddress1-${suggestedAddressCount}" value="${addressDoctorVO.address1}" maxlength="50"/>
											<input type="hidden" name="suggestedAddress2-${suggestedAddressCount}" id="suggestedAddress2-${suggestedAddressCount}" value="${addressDoctorVO.address2}" maxlength="50"/>
											<input type="hidden" name="suggestedCity-${suggestedAddressCount}" id="suggestedCity-${suggestedAddressCount}" value="${addressDoctorVO.city}" maxlength="30"/>
											<input type="hidden" name="suggestedState-${suggestedAddressCount}" id="suggestedState-${suggestedAddressCount}" value="${addressDoctorVO.state}" />
											<input type="hidden" name="suggestedPostalCode-${suggestedAddressCount}" id="suggestedPostalCode-${suggestedAddressCount}" value="${addressDoctorVO.postalCode}" />
											<input type="hidden" name="suggestedCountry-${suggestedAddressCount}" id="suggestedCountry-${suggestedAddressCount}" value="${addressDoctorVO.country}" />
										</div>
										
									</dsp:oparam>
								</dsp:droplet>
							</div>
						</dsp:oparam >
					</dsp:droplet>
					<c:choose>
						<c:when test="${mode eq 'paymentCard' || mode eq 'paymentInStore'}">
							<button id="continueWithSugestedAddr" onclick="paymentAddressFromOverlay()"><fmt:message key="myaccount.addressdoctor.save.address" /></button>
						</c:when>
						<c:when test="${mode eq 'placeOrder'}">
							<button id="continueWithSugestedAddr" onclick="placeOrderAddressFromOverlay()"><fmt:message key="myaccount.addressdoctor.save.address" /></button>
						</c:when>
						<c:when test="${mode eq 'address'}">
							<c:choose>
								<c:when test="${addressRecognized and size gt 1}">
									<button id="continueWithSugestedAddr" onclick="shipToAddressFromOverlay()"><fmt:message key="myaccount.addressdoctor.save.address" /></button>
								</c:when>
								<c:otherwise>
									<button id="continueWithSugestedAddr" onclick="shipToAddressFromOverlay()"><fmt:message key="myaccount.addressdoctor.save.address" /></button>
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:when test="${mode eq 'addressBookFromShipping'}">
							<c:choose>
								<c:when test="${addressRecognized and size gt 1}">
									<button id="continueWithSugestedAddr" onclick="modifyAddressShippingOverlay()"><fmt:message key="myaccount.addressdoctor.save.address" /></button>
								</c:when>
								<c:otherwise>
									<button id="continueWithSugestedAddr" onclick="modifyAddressShippingOverlay()"><fmt:message key="myaccount.addressdoctor.save.address" /></button>
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:when test="${mode eq 'paymentPage'}">
							<c:choose>
								<c:when test="${addressRecognized and size gt 1}">
									<button id="continueWithSugestedAddr" onclick="addCreditCard_checkout()"><fmt:message key="myaccount.addressdoctor.save.address" /></button>
								</c:when>
								<c:otherwise>
									<button id="continueWithSugestedAddr" onclick="addCreditCard_checkout()"><fmt:message key="myaccount.addressdoctor.save.address" /></button>
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:when test="${mode eq 'layaway'}">
							<c:choose>
								<c:when test="${addressRecognized and size gt 1}">
									<button id="continueWithSugestedAddr" onclick="submitLayawayAddressFromOverlay()"><fmt:message key="myaccount.addressdoctor.save.address" /></button>
								</c:when>
								<c:otherwise>
									<button id="continueWithSugestedAddr" onclick="submitLayawayAddressFromOverlay()"><fmt:message key="myaccount.addressdoctor.save.address" /></button>
								</c:otherwise>
							</c:choose>
						</c:when>
					</c:choose>
				</dsp:oparam>
			</dsp:droplet>
		</div>
	</div>	
	<c:choose>
		<c:when test="${mode eq 'paymentCard' || mode eq 'paymentInStore'}">
			<dsp:getvalueof var="suggestedAddressFormId" value="savePaymentAddressFromSuggested" />
			<dsp:getvalueof var="onSubmitJavascriptFunction" value="onSubmitPaymentSuggestedOverlay" />
			<dsp:getvalueof var="suggestedAddressButtonId" value="paymentAddressFromSuggested" />
		</c:when>
		<c:when test="${mode eq 'placeOrder'}">
			<dsp:getvalueof var="suggestedAddressFormId" value="savePlaceOrderAddressFromSuggested" />
			<dsp:getvalueof var="onSubmitJavascriptFunction" value="onPlaceOrderPaymentSuggestedOverlay" />
			<dsp:getvalueof var="suggestedAddressButtonId" value="placeOrderAddressFromSuggested" />
		</c:when>
		<c:when test="${mode eq 'address'}">
			<dsp:getvalueof var="suggestedAddressFormId" value="saveShippingAddressFromSuggested" />
			<dsp:getvalueof var="onSubmitJavascriptFunction" value="onSubmitShippingSuggestedOverlay" />
			<dsp:getvalueof var="suggestedAddressButtonId" value="shipToAddressFromSuggested" />
			
			<dsp:getvalueof var="shippingMethod" param="shippingMethod"/>
			<dsp:getvalueof var="shippingPrice" param="shippingPrice"/>
			<dsp:getvalueof var="country" param="country"/>
			<dsp:getvalueof var="showMeGiftingOptions" param="showMeGiftingOptions"/>
		</c:when>
		<c:when test="${mode eq 'addressBookFromShipping'}">
		<%-- <dsp:getvalueof var="nickname" param="nickname"/> --%>
			<dsp:getvalueof var="suggestedAddressFormId" value="myShippingAddressForm" />
			<dsp:getvalueof var="onSubmitJavascriptFunction" value="onSubmitShippingAddress" />
			<dsp:getvalueof var="suggestedAddressButtonId" value="submitAddressId" />
		</c:when>
		<c:when test="${mode eq 'paymentPage'}">
			<dsp:getvalueof var="suggestedAddressFormId" value="addOrEditCreditCard" />
			<dsp:getvalueof var="nameOnCard" param="nameOnCard" />
			
			<dsp:getvalueof var="onSubmitJavascriptFunction" value="onSubmitAddCreditCard_checkout" />
			<dsp:getvalueof var="suggestedAddressButtonId" value="submitCreditCardId" />
		</c:when>
		<c:when test="${mode eq 'layaway'}">
			<dsp:getvalueof var="suggestedAddressFormId" value="saveLayawayAddressFromSuggested" />
			<dsp:getvalueof var="onSubmitJavascriptFunction" value="onSubmitLayawaySuggestedOverlay" />
			<dsp:getvalueof var="suggestedAddressButtonId" value="submitLayawayAddress" />
		</c:when>
	</c:choose>
	
	<form id="${suggestedAddressFormId}" method="POST" onsubmit="javascript: return ${onSubmitJavascriptFunction}();">
		<input type="hidden" id="action" name="action" value="${action}" />
		<input type="hidden" name="firstName" value="${firstName}" />
		<input type="hidden" name="lastName" value="${lastName}" />
		<input type="hidden" name="address1" id="suggestedAddress1Id" value="" maxlength="50"/>
		<input type="hidden" name="address2" id="suggestedAddress2Id" value="" maxlength="50"/>
		<input type="hidden" name="city" id="suggestedCityId" value="" maxlength="30"/>
		<input type="hidden" name="state" id="suggestedStateId" value="" />
		<input type="hidden" name="country" id="suggestedCountryId" value="" />
		<input type="hidden" name="postalCode" id="suggestedPostalCodeId" value="" />
		<input type="hidden" name="phoneNumber" value="${phoneNumber}" />
		<input type="hidden" name="selectedSavedCard" value="${creditCardNickName}" />
		<input type="hidden" name="orderSet" value="${orderSet}" />
		<c:choose>
			<c:when test="${mode eq 'paymentCard'}">
				<input type="hidden" name="isNoFinanceAgreed" value="${isNoFinanceAgreed}"/>
				<input type="hidden" name="orderSet" value="${orderSet}"/>
				<input type="hidden" name="orderEmail" value="${orderEmail}" />
				<input type="hidden" name="ccToken" value="${ccToken}" />
				<input type="hidden" name="isRUSCard" value="${isRUSCard}" />
				<input type="hidden" name="cardType" value="${cardType}" />
				<input type="hidden" name="ccnumber" value="${ccnumber}" />
				<input type="hidden" name="cBinNum" value="${cBinNum}" />
				<input type="hidden" name="cLength" value="${cLength}" />
				<input type="hidden" name="expirationMonth" value="${expirationMonth}" />
				<input type="hidden" name="expirationYear" value="${expirationYear}" />
				<input type="hidden" name="creditCardCVV" value="${creditCardCVV}" />
				<input type="hidden" name="nameOnCard" value="${nameOnCard}" />
				<input type="hidden" name="isRewardsCardValid" value="${isRewardsCardValid}" />
				<input type="hidden" name="rewardNumber" value="${rewardNumber}" />
				<input type="hidden" name="deviceID" value="${deviceID}" />
				<input type="hidden" name="paymentGroupType" value="${paymentGroupType}"/>
				<input type="hidden" id="addressValidated" name="addressValidated" value="true" />
				<input type="hidden" id="newBillingAddress" name="newBillingAddress" value="${newBillingAddress}" />
				<input type="hidden" id="editBillingAddress" name="editBillingAddress" value="${editBillingAddress}" />
				<input type="hidden" id="newCreditCard" name="newCreditCard" value="${newCreditCard}" />
				<input type="hidden" id="editCreditCard" name="editCreditCard" value="${editCreditCard}" />
				<input type="hidden" id="editCardNickName" name="editCardNickName" value="${editCardNickName}" />
			</c:when>
			<c:when test="${mode eq 'paymentInStore'}">
				<input type="hidden" name="orderEmail" value="${orderEmail}" />
				<input type="hidden" id="addressValidated" name="addressValidated" value="true" />
				<input type="hidden" id="newBillingAddress" name="newBillingAddress" value="${newBillingAddress}" />
				<input type="hidden" id="editBillingAddress" name="editBillingAddress" value="${editBillingAddress}" />
				<input type="hidden" id="isRewardsCardValid" name="isRewardsCardValid" value="${isRewardsCardValid}" />
				<input type="hidden" id="rewardNumber" name="rewardNumber" value="${rewardNumber}" />
			</c:when>
			<c:when test="${mode eq 'placeOrder'}">
				<input type="hidden" name="isNoFinanceAgreed" value="${isNoFinanceAgreed}"/>
				<input type="hidden" name="orderSet" value="${orderSet}"/>
				<input type="hidden" name="orderEmail" value="${orderEmail}" />
				<input type="hidden" name="ccToken" value="${ccToken}" />
				<input type="hidden" name="isRUSCard" value="${isRUSCard}" />
				<input type="hidden" name="cardType" value="${cardType}" />
				<input type="hidden" name="ccnumber" value="${ccnumber}" />
				<input type="hidden" name="cBinNum" value="${cBinNum}" />
				<input type="hidden" name="cLength" value="${cLength}" />
				<input type="hidden" name="expirationMonth" value="${expirationMonth}" />
				<input type="hidden" name="expirationYear" value="${expirationYear}" />
				<input type="hidden" name="creditCardCVV" value="${creditCardCVV}" />
				<input type="hidden" name="nameOnCard" value="${nameOnCard}" />
				<input type="hidden" name="isRewardsCardValid" value="${isRewardsCardValid}" />
				<input type="hidden" name="rewardNumber" value="${rewardNumber}" />
				<input type="hidden" name="deviceID" value="${deviceID}" />
				<input type="hidden" name="paymentGroupType" value="${paymentGroupType}"/>
				<input type="hidden" name="respJwt"  value="${respJwt}"/>
				<input type="hidden" id="addressValidated" name="addressValidated" value="true" />
				<input type="hidden" id="newBillingAddress" name="newBillingAddress" value="${newBillingAddress}" />
				<input type="hidden" id="editBillingAddress" name="editBillingAddress" value="${editBillingAddress}" />
				<input type="hidden" id="newCreditCard" name="newCreditCard" value="${newCreditCard}" />
				<input type="hidden" id="editCreditCard" name="editCreditCard" value="${editCreditCard}" />
				<input type="hidden" id="editCardNickName" name="editCardNickName" value="${editCardNickName}" />
			</c:when>
			<c:when test="${mode eq 'paymentPage'}">
				<input type="hidden" name="nameOnCard" value="${nameOnCard}" maxlength="40"/>
				<input type="hidden" name="creditCardNumber" value="${creditCardNumber}" />
				<input type="hidden" name="expirationMonth" value="${expirationMonth}" />
				<input type="hidden" name="expirationYear" value="${expirationYear}" />
				<input type="hidden" name="creditCardCVV" value="${creditCardCVV}" />
				<input type="hidden" name="nickName" value="${nickName}" id="billingAddressNickNameForPayment" />
				<input type="hidden" name="editCardNickName" value="${editCardNickName}" id="editCardNickNameForPayment"/>
				<input type="hidden" id="addressValidated" name="billingAddressValidated" value="true" />
				<input type="hidden" name="selectedCardBillingAddress" value="${selectedCardBillingAddress}" />
				<input type="hidden" id="nameOnCard" value="${nameOnCard}" />
				<input type="hidden" name="isEditSavedCard" value="${isEditSavedCard}"/>
			</c:when>
			<c:when test="${mode eq 'address'}">
				<input type="hidden" id="addressValidated" name="addressValidated" value="true" />
				<%-- <input type="hidden" name="firstName" value="${firstName}" />
				<input type="hidden" name="lastName" value="${lastName}" />
				<input type="hidden" name="address1" id="suggestedAddress1Id" value="" maxlength="50"/>
				<input type="hidden" name="address2" id="suggestedAddress2Id" value="" maxlength="50"/>
				<input type="hidden" name="city" id="suggestedCityId" value="" maxlength="30"/>
				<input type="hidden" name="state" id="suggestedStateId" value="" />
				<input type="hidden" name="country" id="suggestedCountryId" value="" />
				<input type="hidden" name="postalCode" id="suggestedPostalCodeId" value="" />
				<input type="hidden" name="phoneNumber" value="${phoneNumber}" />--%>
				<input type="hidden" name="billingAddressSameAsShippingAddress" value="${billingAddressSameAsShippingAddress}" />
				<input type="hidden" name="shippingMethod" value="${shippingMethod}" />
				<input type="hidden" name="shippingPrice" value="${shippingPrice}" />
				<input type="hidden" name="showMeGiftingOptions" value="${showMeGiftingOptions}" />
				<%-- <input type="submit" name="suggestedAddressBtn" value="btn" id="${suggestedAddressButtonId}" /> --%>
			</c:when>
			<c:when test="${mode eq 'addressBookFromShipping'}">
				<input type="hidden" id="addressValidated" name="addressValidated" value="true" />
				<input type="hidden" name="isEditSavedAddress" value="${isEditSavedAddress}" />
				<input type="hidden" name="nickname" value="${nickname}" />
				<input type="hidden" name="billingAddressSameAsShippingAddress" value="${billingAddressSameAsShippingAddress}" />
				<c:if test="${not empty email}">
					<input type="hidden" name="email" value="${email}"/>
				</c:if>
				<%-- <input type="hidden" name="firstName" value="${firstName}" />
				<input type="hidden" name="lastName" value="${lastName}" />
				<input type="hidden" name="address1" id="suggestedAddress1Id" value="" maxlength="50"/>
				<input type="hidden" name="address2" id="suggestedAddress2Id" value="" maxlength="50"/>
				<input type="hidden" name="city" id="suggestedCityId" value="" maxlength="30"/>
				<input type="hidden" name="state" id="suggestedStateId" value="" />
				<input type="hidden" name="country" id="suggestedCountryId" value="" />
				<input type="hidden" name="postalCode" id="suggestedPostalCodeId" value="" />
				<input type="hidden" name="phoneNumber" value="${phoneNumber}" /> 
				<input type="submit" name="suggestedAddressBtn" value="btn" id="${suggestedAddressButtonId}" />--%>
			</c:when>
			<c:when test="${mode eq 'layaway'}">
				<input type="hidden" name="cBinNum" value="${cBinNum}" />
				<input type="hidden" name="cLength" value="${cLength}" />
				<input type="hidden" name="expirationMonth" value="${expirationMonth}" />
				<input type="hidden" name="expirationYear" value="${expirationYear}" />
				<input type="hidden" name="creditCardCVV" value="${creditCardCVV}" />
				<input type="hidden" name="nameOnCard" value="${nameOnCard}" />
				<input type="hidden" name="respJwt" value="${respJwt}" />
				<input type="hidden" name="cardinalToken" value="${cardinalToken}" />
				<input type="hidden" name="layawayAmountDue" value="${layawayAmountDue}"/>
				<input type="hidden" name="layawayPaymentAmount" value="${layawayPaymentAmount}"/>
				<c:if test="${not empty orderEmail}">
					<input type="hidden" name="orderEmail" value="${orderEmail}"/>
				</c:if>
				<input type="hidden" id="addressValidated" name="billingAddressValidated" value="true" />
			</c:when>
		</c:choose>
		<input type="submit" name="suggestedAddressBtn" value="btn" id="${suggestedAddressButtonId}" class="display-none" />
		<%-- <input type="hidden" name="address1" id="suggestedAddress1Id" value="" maxlength="50"/>
		<input type="hidden" name="address2" id="suggestedAddress2Id" value="" maxlength="50"/>
		<input type="hidden" name="city" id="suggestedCityId" value="" maxlength="30"/>
		<input type="hidden" name="state" id="suggestedStateId" value="" />
		<input type="hidden" name="country" id="suggestedCountryId" value="" />
		<input type="hidden" name="postalCode" id="suggestedPostalCodeId" value="" />
		<input type="hidden" name="phoneNumber" value="${phoneNumber}" />
		<input type="submit" name="suggestedAddressBtn" value="btn" id="${suggestedAddressButtonId}" /> --%>
	</form>
	
</dsp:page>
