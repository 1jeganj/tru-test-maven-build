<dsp:page>
  	<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
	<dsp:importbean bean="/com/tru/common/TRUIntegrationConfiguration" />
	<dsp:importbean bean="/com/tru/common/TRUSOSIntegrationConfiguration" />
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/multisite/Site"/>
    <dsp:getvalueof var="enableTriad" bean="Site.enableTriad"/>	
	<dsp:getvalueof var="event" value="viewcart"/>
	<dsp:getvalueof var="itemid" value=""/>
	<dsp:getvalueof var="customerId" bean="Profile.id"/>
	<dsp:getvalueof var="quesryString" bean="/OriginatingRequest.queryString" />
	<dsp:getvalueof var="storeCertona" bean="TRUIntegrationConfiguration.enableCertona"/>
	<dsp:getvalueof var="sosCertona" bean="TRUSOSIntegrationConfiguration.enableCertona"/>
	<dsp:getvalueof var="requesturi" value="${originatingRequest.requesturi}" />
	<dsp:getvalueof var="serverName" value="${originatingRequest.serverName}"/>
	<%-- <dsp:importbean bean="/com/tru/radial/payment/paypal/TRUPayPalConfiguration" /> --%>
	<dsp:importbean bean="/com/tru/radial/common/TRURadialConfiguration"/>
	<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id" />
	<dsp:droplet name="GetSiteTypeDroplet">
		<dsp:param name="siteId" value="${site}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="sitevalue" param="site"/>
			<c:set var="sitevalue" value ="${sitevalue}" scope="request"/>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
		<dsp:oparam name="output">
			<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="requestUrl" value="${scheme}${serverName}${requesturi}"/>
	<c:choose>
		<c:when test="${sitevalue eq 'sos'}">
			<c:set var="certona" value="${sosCertona}" scope="request"/>
		</c:when>
		<c:otherwise>
			<c:set var="certona" value="${storeCertona}" scope="request"/>
		</c:otherwise>
	</c:choose>
	
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<!DOCTYPE html>
	<html lang="en">
	<head>
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration"/>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title><fmt:message key="shoppingcart.header" /></title>
	
	<!-- toolkit styles -->
	<dsp:include page="includeScripts.jsp"></dsp:include>
	<!-- /toolkit styles -->
	<!-- switchboard tags -->		
			<link rel="alternate" media="only screen and (max-width: 640px)" href="${fn:replace(requestUrl,'://','://m.')}">
			<link rel="alternate" media="only screen and (min-width: 641px) and (max-width: 1024px)" href="${fn:replace(requestUrl,'://','://t.')}">
	<!-- switchboard tags -->
	
	<script async src="//www.paypalobjects.com/api/checkout.js"></script>
	<dsp:getvalueof var="tealiumSyncURL" bean="TRUTealiumConfiguration.tealiumSyncURL"/>
	<script src="${tealiumSyncURL}"></script>
	<%-- Start :PayPal Set Up Initialization code --%>
		<div id="payPalCartPaymentInitialiseDiv" class="truPayPal-div-hidden">
			<a href="javascript:void(0)"  id="paypalPaymentExpresscheckoutId" title="payPalCartPayment">
			</a>
		</div>
	   
		<!-- <script async src="//www.paypalobjects.com/api/checkout.js"></script> -->
	<%-- End :PayPal Set Up Initialization code --%>
</head>

<body class="site-${siteName}">
<div id="ajaxLoaderUI"></div> 
<div class="modal fade in" id="quickviewModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false">
			<dsp:include page="/jstemplate/quickView.jsp"></dsp:include>
</div>



	<dsp:droplet name="/com/tru/droplet/TRUFetchServerAndEarNameDroplet">
				<dsp:oparam name="output">
				<dsp:getvalueof var="earName" param="earName" />
				<dsp:getvalueof var="instanceName" param="serverName" />
				<dsp:getvalueof var="appDate" param="appDate" />
				<c:choose>
					<c:when test="${not empty instanceName and not empty earName}">
						<!-- Server name: ${instanceName} Ear: ${earName} Application Date : ${appDate} -->
					</c:when>
					<c:otherwise>
						<!-- Server name: ${instanceName}  Application Date : ${appDate} -->
					</c:otherwise>
				</c:choose>
				</dsp:oparam>
				<dsp:oparam name="empty">
				</dsp:oparam>
	</dsp:droplet>
			
	<input type="hidden" value="${contextPath}" class="hiddenContextPath" />
	<input type="hidden" value="${TRUImagePath}" name="TRUImagePath" class="TRUImagePath" />
	<input type="hidden" value="shoppingCart" id="pageName" />
	<dsp:getvalueof var="maxItemQty" bean="/com/tru/common/TRUConfiguration.maximumItemQuantity"/>
	<input type="hidden" id="maxItemQty" value="${maxItemQty}"/>
	<!-- Start Shopping Cart template -->
	<dsp:droplet name="Switch">
		<dsp:param name="value" value="${site}"/>
		<dsp:oparam name="BabyRUs">
			<div class="container-fluid default-template shopping-cart-template bru-shopping-cart">
		</dsp:oparam>
		<dsp:oparam name="default">
			<div class="container-fluid default-template shopping-cart-template">
		</dsp:oparam>
	</dsp:droplet>
	<div id="loadNortonHiddenDiv" class="hide-data">
		<dsp:include page="/common/nortonSealScript.jsp" />
	</div>
	<div id="cartHeader"></div>
		
			<dsp:droplet name="/atg/targeting/TargetingForEach">
				<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/ShoppingCart/HassleFreeReturnsTargeter"/>
				<dsp:oparam name="output">
					<dsp:valueof param="element.data" valueishtml="true" />
				</dsp:oparam>
			</dsp:droplet>
			<%-- <div class="hassle-free-returns">
				hassle-free returns!
			</div>
			<div class="do-you-need-return">
				Do you need to return an item?
				<a href="javascript:void(0)">learn more <img class="sort-by-dropdown-arrow" aria-label="dropdown_arrow" src="../images/arrow-grey.svg" alt="dropdown_arrow" title="arrow_grey_svg"></a>
			</div> --%>
		<div class="shopping-cart-container" id="shoppingCart"></div>
		
		<div>
		<span id="soscartHeader1">
	       <c:if test="${enableTriad eq 'true' }"> 
			<div class="ad-zone-728-90 partial-ad-zone-728-90">
				<div id="OAS_Bottom1" ></div>
			</div>
			</c:if> 
		</span>
		</div>
		<!-- <div class="shopping-cart-grey"> -->
		<div class="shopping-cart-grey-placeholder"></div>
		<c:if test="${certona eq 'true'}">
			<div class="full_width_gray">
				<div id="cart_rr">
					<!--  Shared shopping cart page recommendations appear here -->
				</div>
			</div>
			<div id="rdata" style="visibility:hidden;">
			  <div id="event">${event}</div> 
			  <div id="itemid">${itemid}</div>
			  <div id="customerid">${customerId}</div>
			</div>
		</c:if>
		<!-- </div> -->
		<div class="shopping-cart-minimized-footer text-center">
			<dsp:include page="cartFooter.jsp"></dsp:include>
		</div>
		<div id="basicModal"></div>

		<div class="modal fade" id="removeItemConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="basicModal"
			aria-hidden="true">
			<dsp:include page="removeLineItem.jsp">
				<dsp:param name="commerceItemId" param="commerceItemId" />
			</dsp:include>
		</div>
		
		
		
		<div class="modal editField fade" id="makemystoreModel"	tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog modal-md">
				<div class="modal-content sharp-border">
					<div class="makemystoreModel-popup">
						<div class="row">
							<div class="col-md-5 col-md-offset-7">
								<div class="row top-margin">
									<div class="col-md-1 col-md-offset-10">
										<span class="clickable"><img data-dismiss="modal" src="${TRUImagePath}images/close.png" alt="close model"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="makemystoreModel-popup-desc">
									<h3 class="w3ValidationMyStore">My Store</h3>
									<p>
										We would like to make <a href="javascript:void(0)" class="makemystoreModel-address">Cherry Wood</a> your
										store?
									</p>
									<p>
										Product availablity and pickup option will be displayed for
										this store.<br>You can change this store anytime using
										the store locator or My Store Link.
									</p>
								</div>
								<div class="action-buttons makemystoeModel-section">
									<button id="makemystoreModel-yes">Make this My Store</button>
									<button data-dismiss="modal">Cancel</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade pdpModals" id="paymentLearnMore_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog modal-md">
				<div class="modal-content sharp-border">
					<div class="pdp-modal">
						<div class="close-btn">
							<span class="sprite-icon-x-close" data-dismiss="modal"></span>
						</div>
						<strong>Promotional Status Applied</strong><br><br>
						<strong>Indicates that you have successfully entered a promotional code.</strong>
						<p>Not Applied</p>
						<ul>
							<li>									
								<p>You have typed the code incorrectly or used the wrong case. Codes are CaSe SeNsItIvE.</p>
							</li>
							<li>
								<p>You have not added all of the required items to your cart (including any gift with purchase items).</p>
							</li>
							<li>
								<p>Many codes are one-time use. You have already used this code on a previous order.</p>	
							</li>
							<li>
								<p>You have not added all of the required items to your cart (including any gift with purchase items).</p>	
							</li>
							<li>
								<p>Your order has not met all of the promotional requirements. (Note: Not all items are eligible for all promotions.)</p>	
							</li>
							<li>
								<p>The code entered is not within the valid effective dates</p>	
							</li>
							<li>
								<p>The promotional code entered requires a specific payment type. If so, please re-enter your payment information and any discounts will be applied after you click the Continue Checkout button.</p>	
							</li>
						</ul>	
					</div>
				</div>
			</div>
		</div>

		
	</div>
	
	<!-- modal content -->
	<div class="modal fade pdpModals" id="checkoutFeesPopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content sharp-border">
				<div class="pdp-modal1">
					<div class="close-btn">
						<span class="sprite-icon-x-close" data-dismiss="modal"></span>
					</div>
					<header>                                                                            
					<p><fmt:message key="order.summary.ewaste.fees.text" /></p>
					</header>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade in pdpModals esrbratingModel" id="esrbratingModel" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false">
		<dsp:include page="/jstemplate/esrbMature.jsp">
		</dsp:include>
	</div>
	<dsp:include page="/jstemplate/privacyPolicyTemplate.jsp">
    </dsp:include>
    <div class="modal fade editCart edit-cart-modal" id="editCartModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="display: none;">
		<div class="modal-lg modal-dialog">
		<div class="modal-content sharp-border">
		</div></div>
	</div>
	<dsp:include page="/jstemplate/includePopup.jsp" />
	<input type="hidden" value='<dsp:valueof bean="TRURadialConfiguration.apiUserName" />' id="payPalApiUserName" />
	<input type="hidden" value='<dsp:valueof bean="TRURadialConfiguration.payPalEnvironment" />' id="payPalhiddenContextPath" />
	<input type="hidden" value='<dsp:valueof bean="TRURadialConfiguration.paypalRedirectURL" />' id="paypalRedirectURL" />
	<%-- <div class="modal fade" id="signInModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<dsp:include page="/cart/cartSignIn.jsp"></dsp:include>
	</div> --%>
	<dsp:include page="/myaccount/signInModal.jsp"></dsp:include>
	
	<div class="modal fade in shoppingcart-findinstore" id="findInStoreModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<dsp:include page="/jstemplate/findInStoreInfo.jsp"/>
	</div>
	
	<dsp:include page="/myaccount/truForgotPassword.jsp" />
	
	<div class="popover learn-more-tooltip" role="tooltip">
		<div class="arrow"></div>
		<p><fmt:message key="shoppingcart.learn.more.popover" /></p>
	</div>
<!-- /End Shopping Cart template -->

<!-- toolkit scripts -->

<!-- /toolkit scripts -->

<script>
ajaxCallForCartPage('${contextPath}');

$(document).ready(function(){
	var isSOS = $.cookie("isSOS");
	var sites = '<c:out value="${sitevalue}"/>';
	if(typeof isSOS != 'undefined' && isSOS != null && isSOS == 'true'){
		$("#soscartHeader1").hide();
	}else{
		$("#soscartHeader1").show();
	}
});
</script>

</body>
</html>
</dsp:page>
