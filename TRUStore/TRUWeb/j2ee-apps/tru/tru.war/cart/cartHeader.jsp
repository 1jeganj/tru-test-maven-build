<dsp:page>
    <fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
    <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
    <dsp:importbean bean="/atg/dynamo/Configuration" var="Configuration"/>
    <dsp:importbean bean="/com/tru/common/TRUConfiguration" var="TRUConfiguration"/>
    <dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
    <dsp:importbean bean="/atg/commerce/ShoppingCart"/>
    <dsp:importbean bean="/atg/commerce/locations/StoreLookupDroplet"/>
    <dsp:importbean bean="/com/tru/commerce/locations/TRUMyStoreDroplet"/>
    <dsp:importbean bean="/com/tru/commerce/cart/droplet/CartItemDetailsDroplet"/>
    <dsp:importbean bean="/com/tru/commerce/cart/droplet/ChannelTagDetailsDroplet"/>
    <dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations"/>
    <dsp:importbean bean="/atg/endeca/assembler/cartridge/manager/AssemblerSettings"/>
    <dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath"/>
    <dsp:getvalueof var="host" value="${originatingRequest.host}"/>
    <dsp:getvalueof var="serverName" value="${originatingRequest.serverName}"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:getvalueof var="locale" bean="Profile.locale"/>
    <!-- saved from url=(0070)http://cloud.toysrus.resource.com/redesign/template-shopping-cart.html -->
    <dsp:importbean bean="/atg/multisite/Site"/>
    <dsp:getvalueof var="enableTriad" bean="Site.enableTriad"/>
    <dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
    <dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id"/>
    <dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet"/>
    <dsp:getvalueof var="bruSiteCheck" bean="EndecaConfigurations.bruSiteCheck"/>
    <dsp:getvalueof var="previewEnabled" bean="AssemblerSettings.previewEnabled"/>
    <dsp:getvalueof var="akamaiSiteParam" bean="TRUConfiguration.akamaiSiteParam"/>
    <dsp:importbean bean="/com/tru/droplet/TRUGetSiteDroplet"/>
    <dsp:droplet name="TRUGetSiteDroplet">
		<dsp:param name="siteId" value="${siteName}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="siteUrl" param="siteUrl"/> 
		</dsp:oparam>
	</dsp:droplet>
    <dsp:droplet name="GetSiteTypeDroplet">
        <dsp:param name="siteId" value="${site}"/>
        <dsp:oparam name="output">
            <dsp:getvalueof var="site" param="site"/>
            <c:set var="site" value="${site}" scope="request"/>
        </dsp:oparam>
    </dsp:droplet>
    <dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
        <dsp:oparam name="output">
            <dsp:getvalueof var="scheme" param="scheme"></dsp:getvalueof>
        </dsp:oparam>
    </dsp:droplet>
    <input type="hidden" value="${scheme}${siteUrl}${contextPath}" name="urlFullContextPath" class="urlFullContextPath" />
    <dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id"/>
    <c:choose>
        <c:when test="${siteName eq 'ToysRUs'}">
            <dsp:getvalueof var="contextRoot" value="TRU"/>
        </c:when>
        <c:when test="${siteName eq 'BabyRUs'}">
            <dsp:getvalueof var="contextRoot" value="BRU"/>
        </c:when>
    </c:choose>
    <input type="hidden" value="${contextRoot}_Header:Utility1:continue-shopping:Shopping-Cart" class="abParameterCart"/>
    <c:set var="isSosVar" value="${cookie.isSOS.value}"/>
    <c:choose>
        <c:when test="${fn:containsIgnoreCase(host, ':')}">
            <dsp:getvalueof var="httpURL" value="${scheme}${serverName}:${Configuration.httpPort}"/>
        </c:when>
        <c:otherwise>
            <dsp:getvalueof var="httpURL" value="${scheme}${host}"/>
        </c:otherwise>
    </c:choose>

    <%-- START:: Changes made for creating Site cookie for every page request --%>
    <c:forEach var="entry" items="${TRUConfiguration.customParamToSiteIdMapping}">
        <c:if test="${entry.value eq siteName}">
            <dsp:getvalueof var="akamaiSiteCookieValue" value="${entry.key}"/>
        </c:if>
    </c:forEach>
    <c:if test="${TRUConfiguration.enableSiteCookieCreationInJS}">
        <input type="hidden" id="currentSiteCookieParam" value="${akamaiSiteParam}"/>
        <input type="hidden" id="currentSiteCookieValue" value="${akamaiSiteCookieValue}"/>
    </c:if>
    <%-- END:: Changes made for creating Site cookie for every page request --%>

    <div class="shopping-cart-nav">
        <nav class="navbar navbar-default top-navbar">
            <div class="container-fluid">
                <ul class="nav navbar-nav">
                    <dsp:droplet name="TRUGetSiteDroplet">
                        <dsp:param name="siteId" value='ToysRUs'/>
                        <dsp:oparam name="output">
                            <dsp:getvalueof var="siteUrl1" param="siteUrl"/>
                            <li>
                                <a id="checkout-tru-logo" class="domain-identity" href="${scheme} ${siteUrl1}" data-site-identity-cookie-name="${akamaiSiteParam}" data-site-identity-cookie-value="tru">
                                    <img src="${TRUImagePath}images/tru-logo-sm.png" alt="toysrus logo"></a></li>
                        </dsp:oparam>
                    </dsp:droplet>
                    <dsp:droplet name="TRUGetSiteDroplet">
                        <dsp:param name="siteId" value='BabyRUs'/>
                        <dsp:oparam name="output">
                            <dsp:getvalueof var="siteUrl2" param="siteUrl"/>
                            <c:if test="${not empty siteUrl2 and bruSiteCheck eq true and previewEnabled ne true and fn:contains(siteUrl2,'.toysrus.com') and fn:indexOf(siteUrl2,'aos')==-1}">
                                <dsp:getvalueof var="siteUrl2" value="${fn:replace(siteUrl2,'.toysrus.com','.babiesrus.com')}"/>
                            </c:if>
                            <li>
                                <a id="checkout-bru-logo" class="domain-identity" href="${scheme} ${siteUrl2}" data-site-identity-cookie-name="${akamaiSiteParam}" data-site-identity-cookie-value="bru">
                                    <img src="${TRUImagePath}images/babiesrus-logo-small.png" alt="babiesrus logo"></a>
                            </li>
                        </dsp:oparam>
                    </dsp:droplet>
                </ul>
                <c:if test="${isSosVar && (site eq 'sos')}">
                    <dsp:include page="/sos/sosHeader.jsp"/>
                </c:if>
                <ul class="nav navbar-right">
                    <li class="your-store store-locator-tooltip" data-original-title="" title="">
                        <span><a href="javascript:void(0)"> <span><em class='sprite sprite-find-store'></em><span class='label-text'><fmt:message key="tru.header.findAStore"/></span></span></a></span>
                    </li>
                    <li class="your-store-bullet your-store-default"><span>&#xb7;</span></li>
                    <li class="your-store your-store-default" data-original-title="" title="">
                        <span class="myDefaultStore">my store:</span><span class="myDefaultStoreName">&nbsp;Select a Store</span>
                    </li>
                </ul>
            </div>
        </nav>
        <nav class="navbar navbar-default shopping-cart-bottom-nav">
            <div class="container-fluid">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <h1 class="shopping-cart-header-text">
                            <fmt:message key="shoppingcart.header"/>
                        </h1>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="continue-shopping">
                        <div class="btn-group">
                            <!-- Added class "my-acount-popover-close" to fix TUW-41882 -->
                            <button class="btn btn-default dropdown-toggle my-acount-popover-close" data-toggle="dropdown" aria-expanded="false">
                                <div class="continue-shopping-text">
                                    <div class="continue-shopping-left-arrow inline"></div><fmt:message key="shoppingcart.continue.shopping" /></div>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <dsp:droplet name="IsEmpty">
                                    <dsp:param name="value" param="registryUrl" />
                                    <dsp:oparam name="false">
                                        <dsp:getvalueof var="registryUrl" param="registryUrl"/>
                                            <dsp:droplet name="IsEmpty">
                                            <dsp:param name="value" param="registryUserName" />
                                            <dsp:oparam name="false">
                                                <li><a href="${registryUrl}"><span class="text-capitalize"><dsp:valueof param="registryUserName"/></span>'s Registry</a></li>
                                            </dsp:oparam>
                                            <dsp:oparam name="true">
                                                <li><a href="${registryUrl}"><fmt:message key="shoppingcart.back.to.registry"/></a></li>
                                            </dsp:oparam>
                                        </dsp:droplet>
                                    </dsp:oparam>
                                </dsp:droplet>
                                <dsp:droplet name="IsEmpty">
                                    <dsp:param name="value" param="wishlistUrl" />
                                    <dsp:oparam name="false">
                                        <dsp:getvalueof var="wishlistUrl" param="wishlistUrl"/>
                                        <dsp:droplet name="IsEmpty">
                                            <dsp:param name="value" param="wishlistUserName" />
                                            <dsp:oparam name="false">
                                                <li><a href="${wishlistUrl}"><span class="text-capitalize"><dsp:valueof param="wishlistUserName"/></span>'s Wishlist</a></li>
                                            </dsp:oparam>
                                            <dsp:oparam name="true">
                                                <li><a href="${wishlistUrl}"><fmt:message key="shoppingcart.back.to.wishlist"/></a></li>
                                            </dsp:oparam>
                                        </dsp:droplet>
                                        
                                    </dsp:oparam>
                                </dsp:droplet>
                                <%--<img class="continue-shopping-dropdown-arrow" src="${TRUImagePath}images/arrow.svg">--%>
                                    <li><a href="javascript:void(0)"  class="cart_previous_page" data-id="${httpURL}"><fmt:message key="shoppingcart.previous.page" /></a>
                                </li>
                                <li><a href="${httpURL}${contextPath}?ab=${contextRoot}_Header:Utility1:continue-shopping:Shopping-Cart"><fmt:message key="shoppingcart.home" /></a>
                                </li>
                            </ul>
                        </div>
                    </li>                 
                    <li>                     
                        <div class="btn-group we-can-help">
	                        <div class="btn btn-default dropdown-toggle my-acount-popover-close" data-toggle="dropdown" aria-expanded="false">
					            <a href="/cobrand/help/online-help" title="Help"><em class='sprite sprite-help-icon'></em></a>
					         </div>
                            <%-- <button class="btn btn-default dropdown-toggle my-acount-popover-close" data-toggle="dropdown" aria-expanded="false">
                                <img src="${TRUImagePath}images/help-icon.png" alt="help icon">
                            </button> --%>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <img class="we-can-help-dropdown-arrow" src="${TRUImagePath}images/arrow.svg" alt="">
                                </li>
                                <li class="we-can-help-header"><fmt:message key="shoppingcart.we.can.help"/>
                                </li>
                                <li class="divider"></li>
                                <dsp:getvalueof var="atgTargeterpath" bean="/atg/registry/RepositoryTargeters/TRU/ShoppingCart/CartHelpLinksTargeter.AbsoluteName"/>
                                <dsp:getvalueof var="cacheKey" value="${atgTargeterpath}${site}${locale}"/>
                                <dsp:droplet name="/com/tru/cache/TRUHelpLinksTargeterCacheDroplet">
                                    <dsp:param name="key" value="${cacheKey}"/>
                                    <dsp:oparam name="output">
                                        <dsp:droplet name="/atg/targeting/TargetingForEach">
                                            <dsp:param name="targeter" bean="${atgTargeterpath}"/>
                                            <dsp:oparam name="output">
                                                <dsp:valueof param="element.data" valueishtml="true"/>
                                            </dsp:oparam>
                                        </dsp:droplet>
                                    </dsp:oparam>
                                </dsp:droplet>
                            </ul> 
                        </div>
                    </li>
                    <dsp:droplet name="/atg/dynamo/droplet/Switch">
                        <dsp:param bean="Profile.transient" name="value"/>
                        <dsp:oparam name="false">
                            <dsp:getvalueof bean="Profile.firstName" var="firstName"/>
                            <dsp:getvalueof bean="Profile.email" var="email"/>
                            <li class="signed-in-block">
                                <button id="my-account-popover-cart" data-toggle="popover" class="global-nav-my-cart" data-original-title="" title="">
                                    <img src="${TRUImagePath}images/account-icon.png" alt="account icon">
                                    <div class="head_welcome_cont">
                                        <dsp:include page="/header/headWelcomeContent.jsp"/>
                                    </div>
                                </button>
                                <!-- start My account popup -->
                                <div id="my-account-popover-struct" class="my-account-popover-struct fade popover bottom in">
                                    <dsp:include page="/header/headerMyAccountPopover.jsp"/>
                                </div>
                            </li>
                        </dsp:oparam>
                        <dsp:oparam name="true">
                            <c:if test="${!isSosVar || !(site eq 'sos')}">
                                <li class="signin-block">
                                    <button class="sign-in"><fmt:message key="shoppingcart.sign.in"/></button>
                                    <p><fmt:message key="shoppingcart.have.account"/></p>
                                </li>
                            </c:if>
                        </dsp:oparam>
                    </dsp:droplet>
                </ul>
            </div>
        </nav>
        <div>
            <dsp:include page="/header/storeLocatorPopup.jsp"/>
            <dsp:include page="/storelocator/storeDetails.jsp"/>
            <c:if test="${!isSosVar || !(site eq 'sos')}">
                <c:if test="${enableTriad eq 'true'}">
                    <div class="ad-zone-777-34 partial-ad-zone-777-34">
                        <div id="OAS_TopRight"></div>
                    </div>
                </c:if>
            </c:if>
        </div>
        <hr/>
    </div>
</dsp:page> 