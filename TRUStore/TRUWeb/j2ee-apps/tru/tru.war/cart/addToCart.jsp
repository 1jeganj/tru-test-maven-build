<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
  <dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
  <dsp:importbean bean="/atg/dynamo/droplet/Switch" />
       <dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
       <dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
  
<dsp:form>

<table>
<tr><td>Product Id:</td><td><dsp:input type="text"  bean="CartModifierFormHandler.productId"  /> </td></tr>
<tr><td>Sku Id:</td><td><dsp:input type="text"  bean="CartModifierFormHandler.catalogRefIds"  /></td></tr>
<tr><td>Quantity:</td><td><dsp:input type="text"  bean="CartModifierFormHandler.quantity"  /></td></tr>
<tr><td>Location Id:</td><td><dsp:input type="text"  bean="CartModifierFormHandler.locationId"  /></td></tr>
<tr><td>ChannelId:</td><td><dsp:input type="text"  bean="CartModifierFormHandler.channelId"  /></td></tr>
<tr><td>ChannelType:</td><td><dsp:input type="text"  bean="CartModifierFormHandler.channelType"  />(registry/wishlist)</td></tr>

<tr><td>Channel User Name:</td><td><dsp:input type="text"  bean="CartModifierFormHandler.channelUserName"  /></td></tr>
<tr><td>CO Channel User Name:</td><td><dsp:input type="text"  bean="CartModifierFormHandler.channelCoUserName"  /></td></tr>

<tr><td colspan="2" align="center"><dsp:input type="submit" bean="CartModifierFormHandler.addItemToOrder"></dsp:input></td></tr>
</table>
<dsp:input type="hidden"  bean="CartModifierFormHandler.channelItemFromPDP" value="false" />
<dsp:input type="hidden"  bean="CartModifierFormHandler.addItemToOrderSuccessURL" value="${contextPath}cart/shoppingCart.jsp" />
<dsp:input type="hidden"  bean="CartModifierFormHandler.addItemToOrderErrorURL" value="${contextPath}cart/addToCart.jsp" />
</dsp:form>
<dsp:droplet name="ErrorMessageForEach">
                                                       <dsp:param name="exceptions" bean="CartModifierFormHandler.formExceptions" />
                                                       <dsp:oparam name="output">
                                                              <dsp:valueof param="message"></dsp:valueof>
                                                       </dsp:oparam>
                                                </dsp:droplet>
</dsp:page>




