<dsp:page>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
<dsp:getvalueof var="itemQty" param="item.quantity"/>
<dsp:getvalueof var="relationshipItemId" param="item.id" />
<dsp:getvalueof var="bppItemInfoId" param="item.bppItemInfoId" />
<dsp:getvalueof var="bppSkuId" param="item.bppSkuId" />
<dsp:getvalueof var="bppItemId" param="item.bppItemId" />
<dsp:getvalueof var="bppItemCount" param="item.bppItemCount" />
<dsp:getvalueof var="pageName" param="pageName" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	<dsp:droplet name="ForEach">
       	<dsp:param name="array" param="item.bppItemList"/>
       	<dsp:param name="elementName" value="bppItem" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="itemId" param="bppItem.bppItemId"/>
			<dsp:getvalueof var="bppSKUId" param="bppItem.bppSkuId"/>
			<input type="hidden" id="bppSKU_${itemId}" value="${bppSKUId}"/>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="IsEmpty">
	   	<dsp:param name="value" param="item.bppItemList" />
	   	<dsp:oparam name="false">
	   		<c:choose>
		   		 <c:when test="${pageName ne 'Confirm'}">
			   		<div class="protection-plan bpp-image">
					    <div class="inline">
					    	<dsp:getvalueof var="bppImageUrl" param="item.bppSkuImageUrl"/>
				   			<dsp:droplet name="IsEmpty">
				   				<dsp:param name="value" value="${bppImageUrl}"/>
				   				<dsp:oparam name="false">
				   					 <div class="buyer-protection-plan-image inline"><img src="${bppImageUrl}" class="cart-bpp-mainpage-image" alt="cart_bpp_mainpage_image"></div>
				   				</dsp:oparam>
				   				<dsp:oparam name="true">
				   					<div class="buyer-protection-plan-image inline"><img src="${TRUImagePath}images/no-image500.gif" class="cart-bpp-mainpage-image" alt="cart_bpp_mainpage_image"></div>
				   				</dsp:oparam>
				   			</dsp:droplet>
					    </div>
					    <div class="inline bpp-content">
					        <div class="checkbox-group">
					        	<dsp:droplet name="ForEach">
					            	<dsp:param name="array" param="item.bppItemList"/>
					            	<dsp:param name="elementName" value="bppItem" />
					            	<dsp:oparam name="output">
					            		<dsp:getvalueof var="bppSKUId" param="bppItem.bppSkuId"/>
					            	</dsp:oparam>
						        </dsp:droplet>
			            		<dsp:droplet name="IsEmpty">
								   	<dsp:param name="value" value="${bppItemInfoId}" />
									   	<dsp:oparam name="false">
										   	<div class="checkbox-sm-on inline" tabindex="0" data-relId="${relationshipItemId}" data-bppInfoId="${bppItemInfoId}"
								            	data-bppSkuId="${bppSKUId}" data-bppItemId="${bppItemId}" data-bppItemCount="${bppItemCount}">
								            </div>
									   	</dsp:oparam>
									   	<dsp:oparam name="true">
										   	<div class="checkbox-sm-off inline" tabindex="0" data-relId="${relationshipItemId}" data-bppInfoId="${bppItemInfoId}"
								            	data-bppSkuId="${bppSKUId}" data-bppItemId="${bppItemId}" data-bppItemCount="${bppItemCount}">
								            </div>
									   	</dsp:oparam>
							   	</dsp:droplet>
					            <dsp:droplet name="Switch">
					            	<dsp:param name="value" param="item.bppItemCount"/>
					            	<dsp:oparam name="1">
					            		<dsp:droplet name="ForEach">
								            	<dsp:param name="array" param="item.bppItemList"/>
								            	<dsp:param name="elementName" value="bppItem" />
								            	<dsp:oparam name="outputStart">
								            		<dsp:getvalueof var="bppTerm" param="bppItem.bppTerm"/>
								            		<span class="bpp-content-title">
																	<fmt:message key="shoppingcart.bpp.single.option.text">
																		<fmt:param value="${bppTerm}"></fmt:param>
																	</fmt:message>
								            		</span>
									            	<a target="popup" href="javascript:void(0);" onclick="window.open('/cart/bppLearnMore.jsp', 'popup','width=600,height=600')">
									            		<fmt:message key="checkout.review.squareTradeLearnMore" />
									            	</a>
								            	</dsp:oparam>
												<dsp:oparam name="output">
												<c:choose>
		   										 <c:when test="${pageName eq 'Shipping'}">
													<span class="protection-plan-static-copy">
														<dsp:getvalueof var="bppRetail" param="bppItem.bppRetail"/>
														<fmt:setLocale value="en_US"/>
														<fmt:formatNumber value="${bppRetail}" type="currency"/>
													</span>
		   										 </c:when>
		   										 <c:otherwise>
		   										 	<span class="protection-plan-static-copy">
														<dsp:getvalueof var="bppRetail" param="bppItem.bppRetail"/>
														<fmt:setLocale value="en_US"/>
														<fmt:formatNumber value="${bppRetail*itemQty}" type="currency"/>
													</span>
		   										 </c:otherwise>
		   										 </c:choose>
												</dsp:oparam>
								            </dsp:droplet>
					            	</dsp:oparam>
					            	<dsp:oparam name="default">
					            		<span class="bpp-content-title">
														<fmt:message key="shoppingcart.bpp.multiple.option.text"/>
					            		</span>
						            	<a target="popup" href="javascript:void(0);" onclick="window.open('/cart/bppLearnMore.jsp', 'popup','width=600,height=600')">
						            		<fmt:message key="checkout.review.squareTradeLearnMore" />
						            	</a>
					            		<dsp:droplet name="IsEmpty">
										   	<dsp:param name="value" value="${bppItemInfoId}" />
										   	<dsp:oparam name="false">
											   	<select class="bppPlanMultiShippingPage displayInline" id="bppPlan_${relationshipItemId}" data-relId="${relationshipItemId}" data-bppInfoId="${bppItemInfoId}"
						            				data-bppSkuId="${bppSkuId}">
										   	</dsp:oparam>
										   	<dsp:oparam name="true">
											   	<select class="bppPlanMultiShippingPage displayInline" id="bppPlan_${relationshipItemId}" disabled="disabled" data-relId="${relationshipItemId}" data-bppInfoId="${bppItemInfoId}"
						            				data-bppSkuId="${bppSkuId}">
										   	</dsp:oparam>
									   	</dsp:droplet>
							            	<option value="select"><fmt:message key="checkout.review.selectPlan"/></option>
								            <dsp:droplet name="ForEach">
								            	<dsp:param name="array" param="item.bppItemList"/>
								            	<dsp:param name="elementName" value="bppItem" />
												<dsp:oparam name="output">
													<dsp:getvalueof var="itemId" param="bppItem.bppItemId"/>
													<dsp:droplet name="Switch">
					            						<dsp:param name="value" param="bppItem.preSelected"/>
					            						<dsp:oparam name="true">
					            							<option value="${itemId}" selected="selected">
																<dsp:valueof param="bppItem.bppPlan"/>
															</option>
					            						</dsp:oparam>
					            						<dsp:oparam name="false">
					            							<option value="${itemId}">
																<dsp:valueof param="bppItem.bppPlan"/>
															</option>
					            						</dsp:oparam>
					            					</dsp:droplet>
												</dsp:oparam>
								            </dsp:droplet>
							            </select>
					            	</dsp:oparam>
					            </dsp:droplet>
					        </div>
					        <p><fmt:message key="checkout.review.squareTradePlanEmail"/></p>
					    </div>
					</div>
				</c:when>
				 <c:otherwise>
					<dsp:droplet name="IsEmpty">
					   	<dsp:param name="value" value="${bppItemInfoId}" />
					   	<dsp:oparam name="false">
					   		<div class="protection-plan">
							    <div class="inline">
								    <dsp:getvalueof var="bppImageUrl" param="item.bppSkuImageUrl"/>
						   			<dsp:droplet name="IsEmpty">
						   				<dsp:param name="value" value="${bppImageUrl}"/>
						   				<dsp:oparam name="false">
						   					 <div class="buyer-protection-plan-image inline"><img src="${bppImageUrl}" class="cart-bpp-mainpage-image" alt="cart_bpp_mainpage_image"></div>
						   				</dsp:oparam>
						   				<dsp:oparam name="true">
						   					<div class="buyer-protection-plan-image inline"><img src="${TRUImagePath}images/no-image500.gif" class="cart-bpp-mainpage-image" alt="cart_bpp_mainpage_image"></div>
						   				</dsp:oparam>
						   			</dsp:droplet>
							    </div>
							    <div class="inline">
									<div class="checkbox-group">
										<dsp:droplet name="ForEach">
							            	<dsp:param name="array" param="item.bppItemList"/>
							            	<dsp:param name="elementName" value="bppItem" />
											<dsp:oparam name="output">
												<dsp:getvalueof var="itemId" param="bppItem.bppItemId"/>
												<dsp:droplet name="Switch">
				            						<dsp:param name="value" param="bppItem.preSelected"/>
				            						<dsp:oparam name="true">
														<dsp:getvalueof var="bppTerm" param="bppItem.bppTerm"/>
														<dsp:getvalueof var="bppRetail" param="bppItem.bppRetail"/>
														<fmt:message key="checkout.confirm.squareTradeProtection">
										            		<fmt:param value="${bppTerm}"></fmt:param>
										            	</fmt:message>
														<a target="_blank" href="http://www.toysrus.com/helpdesk/popup.jsp?display=returns&amp;subdisplay=bpp">
															<fmt:message key="checkout.review.squareTradeLearnMore" />
														</a>
														<span class="protection-plan-static-copy">
															<fmt:setLocale value="en_US"/>
															<fmt:formatNumber value="${bppRetail*itemQty}" type="currency"/>
														</span>
				            						</dsp:oparam>
				            					</dsp:droplet>
											</dsp:oparam>
							            </dsp:droplet>
									</div>
									<p><fmt:message key="checkout.review.squareTradePlanEmail" /></p>
								</div>
							</div>
					   	</dsp:oparam>
					</dsp:droplet>
				</c:otherwise> 
			</c:choose>
	   	</dsp:oparam>
    </dsp:droplet>
</dsp:page> 