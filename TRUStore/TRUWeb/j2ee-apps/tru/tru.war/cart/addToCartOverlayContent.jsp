<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:getvalueof bean="ShoppingCart.current.repositoryItem.gwpMarkers" var="gwpMarkers"/>
	<c:if test="${not empty gwpMarkers}">
		<dsp:droplet name="/atg/commerce/order/purchase/RepriceOrderDroplet">
		  <dsp:param value="ORDER_TOTAL" name="pricingOp"/>
		</dsp:droplet>
	</c:if>
	<div id="cartHeaderOverlay">
		<dsp:include page="/jstemplate/cartOverlayHeader.jsp" />
	</div>
	<hr class="shopping-cart-overlay-hr">
	<div class="cart-content">
		<div class="row">
			<div class="left-section col-md-8">
				
				<div class="tse-scrollable">
					<div class="tse-content">
						<dsp:include page="shoppingCartOverlayError.jsp" />
						<div id="cartOverlayItems">
							<dsp:include page="cartOverlayItems.jsp">
								<dsp:param name="relationShipId" param="relationShipId" />
								<dsp:param name="inlineMessage" param="inlineMessage" />
							</dsp:include>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4" id="cartSummaryOverlay">
				<dsp:include page="/cart/cartOverlaySummary.jsp"></dsp:include>
			</div>
		</div>
	</div>
</dsp:page>
