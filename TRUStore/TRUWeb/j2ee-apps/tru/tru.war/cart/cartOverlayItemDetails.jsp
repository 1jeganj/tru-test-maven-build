<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:getvalueof var="index" param="index" />
	<dsp:getvalueof var="existingRelationShipId" param="existingRelationShipId" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof var="productId" param="item.productId"/>
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:getvalueof bean="TRUTealiumConfiguration.PDPProdMerchCategory" var="productMerchandisingCategory"/>
	<div id="cart-overlay-product-info" tabindex="0">
		<h3 class="cart-overlay-product-heading">
			<div class="cart-overlay-product-title quick-shopping-popup"><dsp:valueof param="item.displayName" /></div>
			<div class="cart-overlay-price-info quick-shopping-popup-right"><dsp:valueof param="item.totalAmount" converter="currency"/></div>
		</h3>
		<div class="clear-both"></div>
		<div class="buy-1-get-free inline">
			<dsp:include page="/pricing/displayPromotions.jsp">
				<dsp:param name="item" param="item" />
			</dsp:include>
		</div>
		<dsp:droplet name="IsEmpty">
		<dsp:param name="value" param="item.channelUserName" />
			<dsp:oparam name="false">
				<div class="registry inline">
					<div></div>
					<fmt:message key="shoppingcart.added.from" />&nbsp; <span class="text-capitalize">
					<dsp:valueof  param="item.channelUserName"/></span>
					<dsp:droplet name="IsEmpty">
						<dsp:param name="value" param="item.channelCoUserName" />
						<dsp:oparam name="false">
							 and <span class="text-capitalize"><dsp:valueof  param="item.channelCoUserName"/></span>
						</dsp:oparam>
					</dsp:droplet>
					 's &nbsp;<dsp:valueof  param="item.channelType"/>
				</div>
			</dsp:oparam>
		</dsp:droplet>
		<div class="wrapper-bpb-stock">		
		<div class="product-image-bpb">
			<dsp:getvalueof var="productImage" param="item.productImage" />
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" value="${productImage}" />
				<dsp:oparam name="false">
					<dsp:droplet name="/com/tru/common/droplet/TRUAkamaiImageDroplet">
						<dsp:param name="imageName" value="${productImage}" />
						<dsp:param name="imageHeight" value="135" />
						<dsp:param name="imageWidth" value="135" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="akamaiImageUrl" param="akamaiUrl" />
						</dsp:oparam>
					</dsp:droplet>
					<img class="purchase-image" src="${akamaiImageUrl}">
				</dsp:oparam>
				<dsp:oparam name="true">
					<img class="purchase-image product-no-image-cart-overlay" src="${TRUImagePath}images/no-image500.gif">
				</dsp:oparam>
			</dsp:droplet>
		</div>
		<ul>
			<li class="in-stock inline"><fmt:message key="shoppingcart.instock" /></li>
		</ul>
		</div>
		<div class="purchase-image-block"></div>
		<div class="cart-info">
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" param="item.skuItem.onlineCollectionName"/>
				<dsp:oparam name="false">
					<dsp:droplet name="/atg/dynamo/droplet/ForEach">
						<dsp:param name="array" param="item.skuItem.onlineCollectionName"/>
						<dsp:param name="elementName" value="collectionName"/>
						<dsp:oparam name="output">
							<div class="collection-name">
								<dsp:getvalueof var="collectionName" param="collectionName"/>
								<fmt:message key="shoppingcart.part.of.collection" >
									<fmt:param value="${collectionName}"></fmt:param>
								</fmt:message><br>
							</div>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
			<dsp:getvalueof var="savedAmount" param="item.savedAmount" />
			<div class="price">
				<dsp:getvalueof var="displayStrikeThrough" param="item.displayStrikeThrough" />
				<c:choose>
					<c:when test="${displayStrikeThrough}">
						<div class="product-description-price inline">
							<span class="crossed-out-price price-save"><dsp:valueof param="item.price" converter="currency" /></span>
							<dsp:valueof param="item.salePrice" converter="currency" />

							<span class="price-save">&nbsp;.&nbsp;</span><span class="price-save save-text">&#32;<fmt:message
									key="shoppingcart.you.save" />&#32;<dsp:valueof param="item.savedAmount" locale="en_US" converter="currency" format="##"/>
								<fmt:message key="shoppingcart.left.bracket" />
								<dsp:valueof param="item.savedPercentage" converter="number" format="##"/>
								<fmt:message key="shoppingcart.percentage" />
								<fmt:message key="shoppingcart.right.bracket" />
							</span>
						</div>
					</c:when>
					<c:otherwise>
						<c:choose>
						<c:when test="${savedAmount > 0.0}">
						<div class="product-description-price inline">
						<span class="crossed-out-price price-save"><dsp:valueof param="item.price" converter="currency" /></span>
							<dsp:valueof param="item.salePrice" converter="currency" />
						<span class="price-save">&nbsp;.&nbsp;&nbsp;</span><span class="price-save save-text">&#32; <fmt:message key="shoppingcart.you.save" />&#32; 
						<dsp:valueof format="currency" value="${savedAmount}" locale="en_US" converter="currency"/>
						<fmt:message key="shoppingcart.left.bracket" /><dsp:valueof param="item.savedPercentage" converter="number" format="##"/><fmt:message key="shoppingcart.percentage" /><fmt:message key="shoppingcart.right.bracket" />
						</span>
					</div>
						</c:when>
						<c:otherwise>
							<div class="product-description-price"><dsp:valueof param="item.salePrice" converter="currency" /></div>
						</c:otherwise>
					</c:choose>
					</c:otherwise>

				</c:choose>
			</div>
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" param="item.skuId" />
				<dsp:oparam name="false">
					<div class="item_number">
						<fmt:message key="shoppingcart.item" />&#32;
						<dsp:valueof param="item.skuId" />
					</div>
				</dsp:oparam>
			</dsp:droplet>
			<dsp:droplet name="Switch">
				<dsp:param name="value" param="item.inventoryStatus" />
				<dsp:oparam name="inStock">
					<!-- <div class="green-bullet inline"></div> -->  <!-- TUW-74209 -->
					<%-- <div class="in-stock inline">
						<fmt:message key="shoppingcart.instock" />
					</div>
					<li class="stock_indicator"><fmt:message key="shoppingcart.instock" /></li> --%>
				</dsp:oparam>
				<dsp:oparam name="preOrder">
					<!-- <div class="green-bullet inline"></div> --> <!-- TUW-74209 -->
					<div class="inline pre-release-text">
						<fmt:message key="shoppingcart.prerelease" />
					</div>
				</dsp:oparam>
			</dsp:droplet>
			<div class="stepper_label">
				<fmt:message key="shoppingcart.item.qty" />
			</div>
			<dsp:getvalueof var="qty" param="item.quantity" />
			<dsp:getvalueof var="qtyLimit" param="item.customerPurchaseLimit" />
			<dsp:getvalueof var="maxItemQty" bean="/com/tru/common/TRUConfiguration.maximumItemQuantity"/>
			<c:if test="${qtyLimit eq 0}">
				<dsp:getvalueof var="qtyLimit" value="${maxItemQty}" />
			</c:if>
			<dsp:getvalueof var="relationshipItemId" param="item.id" />
			<dsp:getvalueof var="itemQtyInCart" param="item.itemQtyInCart" />
			<dsp:getvalueof var="relItemQtyInCart" param="item.relItemQtyInCart" />
			<div class="stepper overlay" id="${index}" relationshipItemId="${relationshipItemId}"
				relItemQtyInCart="${relItemQtyInCart}" lineItemQtyInCart="${qty}" otherMetaInfoQty="${relItemQtyInCart-qty}" quantityLimitVal="${qtyLimit}">
			<dsp:droplet name="Switch">
				<dsp:param name="value" param="item.gWPPromotionItem" />
			 <dsp:oparam name="true">
					<a href="javascript:void(0)" class="decrease disabled cart" title="decrease count"></a>
					<input type="text" class="counter center lineItemQty" disabled="disabled"  maxlength="0" contenteditable="true" value="${qty}" aria-label="Item Count">
					<a href="javascript:void(0)" class="increase disabled cart" title="increase count"></a>
			</dsp:oparam>
			<dsp:oparam name="false">
				<c:choose>
					<c:when test="${1 eq qty and itemQtyInCart < qtyLimit}">
						<a href="javascript:void(0)" class="decrease disabled cart"></a>
						<input type="text" class="counter center lineItemQty" onkeypress="return isNumberKey(event)" maxlength="3"
							contenteditable="true" value="${qty}" aria-label="Item Count">
						<a href="javascript:void(0)" class="increase enabled cart" title="increase count" onclick='javascript:utag.link({"event_type":"cart_update","product_merchandising_category":[" "]});'></a>
					</c:when>
					<c:when test="${qtyLimit eq qty or qtyLimit eq itemQtyInCart}">
						<dsp:droplet name="Switch">
							<dsp:param name="value" value="${qty}" />
							<dsp:oparam name="1">
								<a href="javascript:void(0)" class="decrease disabled cart"></a>
								<input type="text" class="counter center lineItemQty" onkeypress="return isNumberKey(event)" maxlength="3"
									contenteditable="true" value="${qty}" aria-label="Item Count">
								<a href="javascript:void(0)" class="increase disabled cart"></a>
							</dsp:oparam>
							<dsp:oparam name="default">
								<a href="javascript:void(0)" class="decrease enabled cart" title="decrease count" onclick='javascript:utag.link({"event_type":"cart_update","product_merchandising_category":[" "]});'></a>
								<input type="text" class="counter center lineItemQty" contenteditable="true"
									onkeypress="return isNumberKey(event)" maxlength="3" value="${qty}" aria-label="Item Count">
								<a href="javascript:void(0)" class="increase disabled cart"></a>
							</dsp:oparam>
						</dsp:droplet>
					</c:when>
					<c:otherwise>
						<a href="javascript:void(0)" class="decrease enabled cart" title="decrease count" onclick='javascript:utag.link({"event_type":"cart_update","product_merchandising_category":[" "]});'></a>
						<input type="text" class="counter center lineItemQty" onkeypress="return isNumberKey(event)"
							contenteditable="true" maxlength="3" value="${qty}" aria-label="Item Count">
						<a href="javascript:void(0)" class="increase enabled cart" title="increase count" onclick='javascript:utag.link({"event_type":"cart_update","product_merchandising_category":[" "]});'></a>
					</c:otherwise>
				</c:choose>
				</dsp:oparam>
				</dsp:droplet>
			</div>
			<dsp:getvalueof var="skuId" param="item.skuId"/>
			<dsp:getvalueof var="locationId" param="item.infoMsgLocationId"/>
			<dsp:getvalueof var="finalKey" value="${skuId}_${locationId}"/>
			<dsp:droplet name="IsEmpty">
				<dsp:param
					bean="CartModifierFormHandler.itemInlineMessages" name="value" />
				<dsp:oparam name="false">
					<dsp:droplet name="ForEach">
						<dsp:param name="array"
							bean="CartModifierFormHandler.itemInlineMessages" />
						<dsp:param name="elementName" value="message" />
						<dsp:oparam name="output">
						<dsp:getvalueof var="key" param='key'/>
						<c:if test="${key eq finalKey}">
							<span class="cart-error-message">
								<dsp:valueof param="message"/>
							</span>
						</c:if>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
			<c:choose>
			<c:when test="${relationshipItemId eq existingRelationShipId}">
				<dsp:droplet name="IsEmpty">
					<dsp:param name="value" param="inlineMessage" />
					<dsp:oparam name="false">
						<span class="cart-error-message"> <dsp:valueof
								param="inlineMessage" />
						</span>
					</dsp:oparam>
				</dsp:droplet>
			</c:when>
			</c:choose>
			<br>
			<dsp:include page="cartOverlayBPP.jsp">
				<dsp:param name="item" param="item"/>
			</dsp:include>
			<div class="row row-no-paddidng shopping-cart-surprise">
                <div class="col-md-6 col-md-offset-2">
                   <div class="surprise-wrapper">
                   <%-- Modified for the defect fix #TUMER-390 --%>
	                   <dsp:droplet name="Switch">
	                          <dsp:param name="value" param="item.skuItem.shipInOrigContainer" />
	                          <dsp:oparam name="true">
		                          <div class="shopping-cart-remove inline">
		                                 <fmt:message key="shoppingcart.spoil.surprise" /> <span>&#xb7;</span>
                                  </div>
                                  <div class="shopping-cart-surprise-image inline"></div>
                                  <div class="popoverCart learn-more-tooltip" role="tooltip">
									<p>spoiler alert! Important note for gift givers! One or more of the items you are adding to your cart may ship in the manufacturer's original packaging, which may reveal what's inside.
To avoid spoiling the surprise, consider shipping this item to another location or selecting "Store Pick Up" during the checkout process.</p>
								  </div>
	                          </dsp:oparam>
	                   </dsp:droplet>
                   </div>
                </div>
			</div>
		</div>
	</div>
</dsp:page>