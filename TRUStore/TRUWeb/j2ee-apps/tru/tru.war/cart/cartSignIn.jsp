<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
<dsp:getvalueof var="customerID" bean="Profile.id"/>
<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
<c:if test="${site eq babySiteCode}">
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
</c:if>	

	
	
	<dsp:setvalue bean="ProfileFormHandler.extractDefaultValuesFromProfile"	value="false" />
	<div class="modal-dialog modal-md">
	            <div class="modal-content sharp-border">
	                <div class="sign-in-overlay">
	                    <div class="sign-in-header">
	                        <div>
	                            <header><fmt:message key="shoppingcart.sign.in" /></header>
	                        </div>
	                        <div>
	                            <a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
	                        </div>
	                    </div>
	                    <div>
	                        <div class="sign-in-form">
							<form class="JSValidation" id="hearderLoginOverlay">
								<p class="global-error-display"></p>
	                            <div>
	                                <p><fmt:message key="shoppingcart.email" /></p>
	                                <label for="email"></label>
	                                <input name="email" id="login" type="text" maxlength="60" title="enter your email id" />
	                            </div>
	                            <div>
	                                <p><fmt:message key="shoppingcart.password" /></p>
	                                <label for="password"></label>
	                                <input name="password" id="sign-in-password-input" maxlength="50" type="password"  aria-label = "enter your password" title="password"/>
	                            </div>
	                            <div>
	                             
	                             <input type="hidden" id="requestPage" value="cart" aria-label="requestPage_for_cartSignIn"  />
	                              <input type="hidden" class="contextPath" value="${contextPath}">
	                            <input type="submit" value="Sign In" class="submit-btn" id="signin-submit-btn"/>  
								<a data-target="#forgotPasswordModel"  class="cart-forgot-password" data-toggle="modal" href="javascript:void(0);"><fmt:message key="shoppingcart.forgot.password" /></a>
	                            		<input type="hidden" id="cartSign-custId" value="${customerID}"/>
							            <input type="hidden" id="cartSign-partnerName" value="${partnerName}"/>
	                            </div>
	                      </form>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
</dsp:page> 