<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/com/tru/commerce/cart/droplet/CartItemDetailsDroplet" />
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler" />
	<%-- <dsp:importbean bean="/com/tru/radial/payment/paypal/TRUPayPalConfiguration" /> --%>
	<dsp:importbean bean="/com/tru/common/TRUUserSession" />
	<dsp:getvalueof bean="ShoppingCart.current.repositoryItem.gwpMarkers" var="gwpMarkers"/>
	<dsp:getvalueof var="enableInvodoFlag" bean="TRUStoreConfiguration.enableInvodoFlag" />
	<dsp:getvalueof var="enablePromoCodeZone" bean="/atg/multisite/Site.enablePromoCodeZone"/>
	<dsp:getvalueof param="couponId" var="couponIdTealium"></dsp:getvalueof>
	
	<dsp:getvalueof param="cartLoad" var="cartLoadExist" />
	<div class="popover learn-more-tooltip" role="tooltip">
		<div class="arrow"></div>
		<p><fmt:message key="shoppingcart.learn.more.popover" /></p>
	</div>
	
	<c:if test="${not empty gwpMarkers}">
		<dsp:setvalue bean="ShoppingCart.current.repriceRequiredOnCart" value="true"/>
		<dsp:setvalue bean="ShippingGroupFormHandler.updateRelationShips" value="submit" />
		<%-- <dsp:droplet name="/atg/commerce/order/purchase/RepriceOrderDroplet">
		  <dsp:param value="ORDER_TOTAL" name="pricingOp"/>
		</dsp:droplet> --%>
	</c:if>
	<div id="shoppingCartBody">
		<dsp:droplet name="IsEmpty">
			<dsp:param param="cartLoad" name="value" />
			<dsp:oparam name="false">
				<dsp:droplet name="IsEmpty">
					<dsp:param bean="TRUUserSession.itemInlineMessages" name="value" />
					<dsp:oparam name="false">
						<dsp:droplet name="ForEach">
							<dsp:param name="array" bean="TRUUserSession.itemInlineMessages" />
							<dsp:param name="elementName" value="message" />
							<dsp:oparam name="outputStart">
							<div class="multipleItemsInlineGenericMsgs hidden">
								{
							</dsp:oparam>
							<dsp:oparam name="output">
								<dsp:getvalueof var="count" param='count'/><br>
								<dsp:getvalueof var="size" param='size'/><br>
								"<dsp:valueof param='key'/>":"<dsp:valueof param='message'/>"
								<c:if test="${count ne size}">
								,
								</c:if>
								
							</dsp:oparam>
							<dsp:oparam name="outputEnd">
								}
								</div>
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
				</dsp:droplet>
				<dsp:droplet name="IsEmpty">
					<dsp:param bean="TRUUserSession.multipleItemsInfoMessages" name="value" />
					<dsp:oparam name="false">
						<dsp:droplet name="ForEach">
							<dsp:param name="array" bean="TRUUserSession.multipleItemsInfoMessages" />
							<dsp:param name="elementName" value="message" />
							<dsp:oparam name="outputStart">
								<div id="multi_items_info_message" class="errorFromShopLocal" style="display: none">
									<div class="shopping-cart-error-state">
										<div class="shopping-cart-error-state-header row">
											<div class="error-state-exclamation-lg img-responsive col-md-2"></div>
											<div class="shopping-cart-error-state-header-text col-md-10">
												<fmt:message key="shoppingcart.oops.issue" />
												<div class="shopping-cart-error-state-subheader">
												</dsp:oparam>
												<dsp:oparam name="output">
													<dsp:valueof param="message" valueishtml="true" />
													<br>
												</dsp:oparam>
												<dsp:oparam name="outputEnd">
												</div>
											</div>
										</div>
									</div>
								</div>
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
				</dsp:droplet>
			</dsp:oparam>
		</dsp:droplet>
		<dsp:droplet name="CartItemDetailsDroplet">
			<dsp:param name="order" bean="ShoppingCart.current" />
			<dsp:oparam name="output">
			<dsp:include page="shoppingCartError.jsp"/>
			<dsp:include page="shoppingCartInfo.jsp">
				<dsp:param name="removedInfoMessage" param="removedInfoMessage" />
				<dsp:param name="items" param="items" />
				<dsp:param name="pageName" value="cart" />
			</dsp:include>
			<%-- <div class="closenessMsg inline">
			<dsp:include page="/pricing/closenessQualifier.jsp">
			</dsp:include>
			</div> --%>
				<div class="row order-summary-top" id="row_order_summary_top">
					<div class="col-md-8 col-md-push-1 cart-products">
						<dsp:droplet name="Switch">
							<dsp:param name="value" bean="/atg/multisite/Site.enableCharityDonation"/>
							<dsp:oparam name="true">
								<dsp:include page="/cart/charityDonation.jsp"></dsp:include>
							</dsp:oparam>
						</dsp:droplet>
						<dsp:include page="/cart/cartLineItems.jsp">
							<dsp:param name="items" param="items" />
							<dsp:param name="existingRelationShipId" param="existingRelationShipId" />
							<dsp:param name="inlineMessage" param="inlineMessage" />
						</dsp:include>
						<c:if test="${enablePromoCodeZone}">
							<dsp:include page="shoppingCartPromocode.jsp">
							<dsp:param name="maxCouponLimit" value="${maxCouponLimit}"/>
							</dsp:include>
						</c:if>
						<hr>
						<div class="sales-tax-notice">
							&#42;&nbsp;sales tax is estimated, actual sales tax will be calculated at the time of shipment.
						</div>	
					</div>
					<dsp:include page="/cart/cartOrderSummary.jsp" />
				</div>
				
				<!-- Start of tealium integration -->
				<c:if test="${not empty enableInvodoFlag && enableInvodoFlag && cartLoadExist eq 'true'}" >
				<dsp:include page="intermediate/tealiumForCart.jsp" >
			          <dsp:param name="items" param="items" />
			          <dsp:param name="couponId" value="${couponIdTealium}" />
		        </dsp:include>
		        </c:if>
	              <!-- End of tealium integration --> 
	              
			</dsp:oparam>
			<dsp:oparam name="empty">
				<dsp:include page="/cart/shoppingCartInfo.jsp">
					<dsp:param name="removedInfoMessage" param="removedInfoMessage" />
					<dsp:param name="pageName" value="cart" />
				</dsp:include>
				<dsp:include page="/cart/emptyCart.jsp"></dsp:include>
				<dsp:include page="intermediate/tealiumForCart.jsp">
				<dsp:param name="couponId" value="${couponIdTealium}" />
				</dsp:include>
			</dsp:oparam>
		</dsp:droplet>
	</div>
	<div id="cartHeaderInclude" class="hide">
		<dsp:include page="/cart/cartHeader.jsp">
			<dsp:param name="items" value="${items}" />
		</dsp:include>
	</div>
	<div>
	</div>
		
</dsp:page>
