<dsp:page>
	<fmt:bundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" >
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/commerce/locations/StoreLookupDroplet" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:getvalueof var="relationshipItemId" param="item.id" />
	<dsp:getvalueof var="metaInfoId" param="item.metaInfoId" />
	<dsp:getvalueof var="locationId" param="item.locationId"/>
	<dsp:getvalueof var="index" param="index" />
	<dsp:droplet name="Switch">
		<dsp:param name="value" param="item.shipWindowMin"/>
		<dsp:oparam name="0">
			<dsp:getvalueof var="shipWindowMin" value="1"/>
		</dsp:oparam>
		<dsp:oparam name="default">
			<dsp:getvalueof var="shipWindowMin" param="item.shipWindowMin"/>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="Switch">
		<dsp:param name="value" param="item.shipWindowMax"/>
		<dsp:oparam name="0">
			<dsp:getvalueof var="shipWindowMax" value="2"/>
		</dsp:oparam>
		<dsp:oparam name="default">
			<dsp:getvalueof var="shipWindowMax" param="item.shipWindowMax"/>
		</dsp:oparam>
	</dsp:droplet>
	<input type="hidden" id="selectedRelationShipId_${index}" value="${index}" />
	<input type="hidden" id="locationId_${index}" value="${locationId}" />
	<dsp:droplet name="Switch">
		<dsp:param name="value" param="item.channelAvailability" />
		<dsp:oparam name="Online Only">
			<label for="ship-to-home" class="noPointer">
				<dsp:droplet name="IsEmpty">
					<dsp:param name="value" param="item.inventoryInfoMessage" />
					<dsp:oparam name="true">
						<div class="ship-to-home">
							<input name="shippingGroup_${index}" class="shipping-radio-btn" value="ship-to-home" type="radio" sg_rel_id="${relationshipItemId}_${metaInfoId}"  sg_rel_index_id="${index}" checked />
							<label class="shipping-radio-item" for="shippingGroup_${index}">
								<div class="checkout-flow-sprite checkout-flow-radio-unselected checkout-flow-radio-selected"></div>
								<span class="shipping-option-label"><fmt:message key="shoppingcart.ship.to.home" /></span>
							</label>
						</div>
						<div class="padded-details" id="padded-details_S2H_${index}">
							<div class="shipping-details">
								<dsp:droplet name="Switch">
									<dsp:param name="value" param="item.inventoryStatus" />
									<dsp:oparam name="inStock">
										<fmt:message key="shoppingcart.leaves.warehouse" >
											<fmt:param value="${shipWindowMin}"></fmt:param>
											<fmt:param value="${shipWindowMax}"></fmt:param>
										</fmt:message>
									</dsp:oparam>
									<dsp:oparam name="preOrder">
										<fmt:message key="shoppingcart.presale.release.date"/><br/>
										<fmt:message key="shoppingcart.presale.warehouse.message" />
										<dsp:valueof param="item.streetDate"/>
									</dsp:oparam>
								</dsp:droplet>
							</div>
							<a href="javascript:void(0);" class="see-terms" data-toggle="popover"><fmt:message
									key="shoppingcart.see.terms" /></a> </br>
							<dsp:droplet name="Switch">
								<dsp:param name="value" param="item.itemShippingSurcharge" />
								<dsp:oparam name="0.0">
								</dsp:oparam>
								<dsp:oparam name="default">
									<fmt:message key="shoppingcart.shipping.surcharge" />&nbsp;<dsp:valueof param="item.itemShippingSurcharge" format="currency" locale="en_US" converter="currency"/>
								</dsp:oparam>
							</dsp:droplet>
						</div>
								<div class="shopping-cart-surprise">
									<dsp:droplet name="Switch">
										<dsp:param name="value" param="item.skuItem.shipInOrigContainer" />
										<dsp:oparam name="true">
												<a href="javascript:void(0);" class="learn-more-giftWrap">
													<fmt:message key="shoppingcart.spoil.surprise" />
												</a>
												<div class="checkout-flow-sprite checkout-flow-learn-more learn-more-giftWrap"></div>
										</dsp:oparam>
									</dsp:droplet>
								</div>
					</dsp:oparam>
					<dsp:oparam name="false">
						<div class="ship-to-home">
							<input name="shippingGroup_${index}" class="shipping-radio-btn" value="ship-to-home" type="radio" sg_rel_id="${relationshipItemId}_${metaInfoId}"  sg_rel_index_id="${index}" checked />
							<label class="shipping-radio-item" for="shippingGroup_${index}">
								<div class="checkout-flow-sprite checkout-flow-radio-unselected checkout-flow-radio-selected"></div>
								<span class="shipping-option-label"><fmt:message key="shoppingcart.ship.to.home" /></span>
							</label>
						</div>
					</dsp:oparam>
				</dsp:droplet>
			</label>
			<div class="free-store-pickup notavailable-storepickup">
				<input id="free-store-pickup-3" class="shipping-radio-btn inline" type="radio" value="free-store-pickup" disabled="disabled" aria-label="free store pickup">
				<fmt:message key="shoppingcart.no.store" />
			</div>
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" param="item.inventoryInfoMessage" />
				<dsp:oparam name="false">
					<div class="itemInfoMessage">
						<dsp:valueof param="item.inventoryInfoMessage" valueishtml="true" />
					</div>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="In Store Only">
			<div class="free-store-pickup"> 
				<input id="free-store-pickup-3" class="shipping-radio-btn inline" type="radio" value="free-store-pickup" disabled="disabled" aria-label="free store pickup">
				<fmt:message key="shoppingcart.no.ship.home" />
			</div>
			<br>
			<dsp:droplet name="Switch">
				<dsp:param name="value" param="item.inStorePickUpAvailable" />
				<dsp:oparam name="true">
					<label for="free-store-pickup" class="noPointer">
						<div class="free-store-pickup">
							<input name="shippingGroup_${index}" class="shipping-radio-btn" value="free-store-pickup" type="radio" sg_rel_id="${relationshipItemId}_${metaInfoId}"  sg_rel_index_id="${index}" checked />
							<label class="shipping-radio-item" for="shippingGroup_${index}">
								<div class="checkout-flow-sprite checkout-flow-radio-unselected checkout-flow-radio-selected"></div>
								<span class="shipping-option-label"><fmt:message key="shoppingcart.free.store.pickup" /></span>
							</label>
						</div> 
						<dsp:droplet name="IsEmpty">
							<dsp:param name="value" param="item.inventoryInfoMessage" />
							<dsp:oparam name="true">
								<div class="padded-details" id="padded-details_ISPU_${index}">
									<span> <dsp:droplet name="StoreLookupDroplet">
											<dsp:param name="id" param="item.locationId" />
											<dsp:param name="elementName" value="store" />
											<dsp:oparam name="output">
												<dsp:getvalueof var="storeName" param="store.name" />
												<dsp:getvalueof var="storeCity" param="store.city" />
												<dsp:getvalueof var="storePostalCode" param="store.postalCode" />
												<dsp:getvalueof var="storeStateAddress" param="store.stateAddress" />
												<dsp:getvalueof var="storeAddress" param="store.address1" />
											</dsp:oparam>
										</dsp:droplet> <dsp:droplet name="IsEmpty">
											<dsp:param name="value" value="${storeName}" />
											<dsp:oparam name="false">
											<dsp:valueof param="item.warehouseMessage" valueishtml="true"/>
												<p>
													<a data-toggle="modal" data-target="#findInStoreModal" href="javascript: void(0);"
														onclick="javascript: return loadFindInStore('/tru', '<dsp:valueof param='item.skuId'/>', '${relationshipItemId}_${metaInfoId}','<dsp:valueof param='item.quantity'/>', this,'cartFlag');">${storeName}</a>
														<br/>${storeAddress}<br/>${storeCity},&nbsp;${storeStateAddress}&nbsp;${storePostalCode}
												</p>
											</dsp:oparam>
											<dsp:oparam name="true">
												<p>
													<a data-toggle="modal" data-target="#findInStoreModal" href="javascript: void(0);"
														onclick="javascript: return loadFindInStore('/tru', '<dsp:valueof param='item.skuId'/>','${relationshipItemId}_${metaInfoId}','<dsp:valueof param='item.quantity'/>', this,'cartFlag');"><fmt:message
															key="shoppingcart.select.stroe" /></a>
												</p>
											</dsp:oparam>
										</dsp:droplet>
									</span>
								</div>
																<div class="shopping-cart-surprise">
									<dsp:droplet name="Switch">
										<dsp:param name="value" param="item.skuItem.shipInOrigContainer" />
										<dsp:oparam name="true">
												<a href="javascript:void(0);" class="learn-more-giftWrap">
													<fmt:message key="shoppingcart.spoil.surprise" />
												</a>
												<div class="checkout-flow-sprite checkout-flow-learn-more learn-more-giftWrap"></div>
										</dsp:oparam>
									</dsp:droplet>
								</div>
							</dsp:oparam>
							<dsp:oparam name="false">
								<div class="itemInfoMessage">
									<dsp:valueof param="item.inventoryInfoMessage" valueishtml="true" />
									<p>
										<a data-toggle="modal" data-target="#findInStoreModal" href="javascript: void(0);"
											onclick="javascript: return loadFindInStore('/tru', '<dsp:valueof param='item.skuId'/>','${relationshipItemId}_${metaInfoId}','<dsp:valueof param='item.quantity'/>', this,'cartFlag');"><fmt:message
												key="shoppingcart.select.stroe" /></a>
									</p>
								</div>
							</dsp:oparam>
						</dsp:droplet>
					</label>
				</dsp:oparam>
				<dsp:oparam name="false">
					<br>
					<div class="free-store-pickup">
					<input id="free-store-pickup-3" class="shipping-radio-btn inline" type="radio" value="free-store-pickup" disabled="disabled" aria-label="free store pickup">
						<fmt:message key="shoppingcart.no.store" />
					</div>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="default">
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" param="item.locationId" />
				<dsp:oparam name="false">
					<label for="ship-to-home" class="noPointer">
						<div class="ship-to-home">
							<input name="shippingGroup_${index}" class="shipping-radio-btn" value="ship-to-home" type="radio" sg_rel_id="${relationshipItemId}_${metaInfoId}"  sg_rel_index_id="${index}" />
							<label class="shipping-radio-item" for="shippingGroup_${index}">
								<div class="checkout-flow-sprite checkout-flow-radio-unselected"></div>
								<span class="shipping-option-label"><fmt:message key="shoppingcart.ship.to.home" /></span>
							</label>
						</div>
						<div class="padded-details" id="padded-details_S2H_${index}" style="visibility: hidden">
							<div class="shipping-details">
								<dsp:droplet name="Switch">
									<dsp:param name="value" param="item.inventoryStatus" />
									<dsp:oparam name="inStock">
										<fmt:message key="shoppingcart.leaves.warehouse" >
											<fmt:param value="${shipWindowMin}"></fmt:param>
											<fmt:param value="${shipWindowMax}"></fmt:param>
										</fmt:message>
									</dsp:oparam>
									<dsp:oparam name="preOrder">
										<fmt:message key="shoppingcart.presale.release.date"/><br/>
										<fmt:message key="shoppingcart.presale.warehouse.message" />
										<dsp:valueof param="item.streetDate"/>
									</dsp:oparam>
								</dsp:droplet>
							</div>
							<a href="javascript:void(0);" class="see-terms"><fmt:message
									key="shoppingcart.see.terms" /></a> </br>
							<dsp:droplet name="Switch">
								<dsp:param name="value" param="item.itemShippingSurcharge" />
								<dsp:oparam name="0.0">
								</dsp:oparam>
								<dsp:oparam name="default">
									<fmt:message key="shoppingcart.shipping.surcharge" />&nbsp;<dsp:valueof param="item.itemShippingSurcharge" format="currency" locale="en_US" converter="currency"/>
								</dsp:oparam>
							</dsp:droplet>
						</div>
														<div class="shopping-cart-surprise">
									<dsp:droplet name="Switch">
										<dsp:param name="value" param="item.skuItem.shipInOrigContainer" />
										<dsp:oparam name="true">
												<a href="javascript:void(0);" class="learn-more-giftWrap">
													<fmt:message key="shoppingcart.spoil.surprise" />
												</a>
												<div class="checkout-flow-sprite checkout-flow-learn-more learn-more-giftWrap"></div>
										</dsp:oparam>
									</dsp:droplet>
								</div>
					</label>
					<br>
					<dsp:droplet name="Switch">
						<dsp:param name="value" param="item.inStorePickUpAvailable" />
						<dsp:oparam name="true">
							<label for="free-store-pickup" class="noPointer">
								<div class="free-store-pickup">
										<input name="shippingGroup_${index}" class="shipping-radio-btn" value="free-store-pickup" type="radio" sg_rel_id="${relationshipItemId}_${metaInfoId}"  sg_rel_index_id="${index}" checked />
										<label class="shipping-radio-item" for="shippingGroup_${index}">
											<div class="checkout-flow-sprite checkout-flow-radio-unselected checkout-flow-radio-selected"></div>
											<span class="shipping-option-label"><fmt:message key="shoppingcart.free.store.pickup" /></span>
										</label>
								</div> 
								<dsp:droplet name="IsEmpty">
									<dsp:param name="value" param="item.inventoryInfoMessage" />
									<dsp:oparam name="true">
										<div class="padded-details" id="padded-details_ISPU_${index}">
											<span> <dsp:droplet name="StoreLookupDroplet">
													<dsp:param name="id" param="item.locationId" />
													<dsp:param name="elementName" value="store" />
													<dsp:oparam name="output">
														<dsp:getvalueof var="storeName" param="store.name" />
														<dsp:getvalueof var="storeCity" param="store.city" />
														<dsp:getvalueof var="storePostalCode" param="store.postalCode" />
														<dsp:getvalueof var="storeStateAddress" param="store.stateAddress" />
														<dsp:getvalueof var="storeAddress" param="store.address1" />
													</dsp:oparam>
												</dsp:droplet> <dsp:droplet name="IsEmpty">
													<dsp:param name="value" value="${storeName}" />
													<dsp:oparam name="false">
														<dsp:valueof param="item.warehouseMessage" valueishtml="true"/>
														<p>
															<a data-toggle="modal" data-target="#findInStoreModal" href="javascript: void(0);"
																onclick="javascript: return loadFindInStore('/tru', '<dsp:valueof param='item.skuId'/>', '${relationshipItemId}_${metaInfoId}','<dsp:valueof param='item.quantity'/>', this,'cartFlag');">${storeName}</a>
														<br/>${storeAddress}<br/>${storeCity},&nbsp;${storeStateAddress}&nbsp;${storePostalCode}
														</p>
													</dsp:oparam>
													<dsp:oparam name="true">
														<p>
															<a data-toggle="modal" data-target="#findInStoreModal" href="javascript: void(0);"
																onclick="javascript: return loadFindInStore('/tru', '<dsp:valueof param='item.skuId'/>', '${relationshipItemId}_${metaInfoId}','<dsp:valueof param='item.quantity'/>', this,'cartFlag');"><fmt:message
																	key="shoppingcart.select.stroe" /></a>
														</p>
													</dsp:oparam>
												</dsp:droplet>
											</span>
										</div>
									<div class="shopping-cart-surprise">
									<dsp:droplet name="Switch">
										<dsp:param name="value" param="item.skuItem.shipInOrigContainer" />
										<dsp:oparam name="true">
												<a href="javascript:void(0);" class="learn-more-giftWrap">
													<fmt:message key="shoppingcart.spoil.surprise" />
												</a>
												<div class="checkout-flow-sprite checkout-flow-learn-more learn-more-giftWrap"></div>
										</dsp:oparam>
									</dsp:droplet>
								</div>
									</dsp:oparam>
									<dsp:oparam name="false">
										<div class="itemInfoMessage">
											<dsp:valueof param="item.inventoryInfoMessage" valueishtml="true" />
											<p>
												<a data-toggle="modal" data-target="#findInStoreModal" href="javascript: void(0);"
													onclick="javascript: return loadFindInStore('/tru', '<dsp:valueof param='item.skuId'/>', '${relationshipItemId}_${metaInfoId}','<dsp:valueof param='item.quantity'/>', this,'cartFlag');"><fmt:message
														key="shoppingcart.select.stroe" /></a>
											</p>
										</div>
									</dsp:oparam>
								</dsp:droplet>
							</label>
						</dsp:oparam>
						<dsp:oparam name="false">
							<br>
							<div class="free-store-pickup notavailable-storepickup">
							<input id="free-store-pickup-3" class="shipping-radio-btn inline" type="radio" value="free-store-pickup" disabled="disabled" aria-label="free store pickup">
								<fmt:message key="shoppingcart.no.store" />
							</div>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="true">
					<label for="ship-to-home" class="noPointer">
						<dsp:droplet name="IsEmpty">
							<dsp:param name="value" param="item.inventoryInfoMessage" />
							<dsp:oparam name="true">
								<div class="ship-to-home">
									<input name="shippingGroup_${index}" class="shipping-radio-btn" value="ship-to-home" type="radio" sg_rel_id="${relationshipItemId}_${metaInfoId}"  sg_rel_index_id="${index}" checked />
									<label class="shipping-radio-item" for="shippingGroup_${index}">
										<div class="checkout-flow-sprite checkout-flow-radio-unselected checkout-flow-radio-selected"></div>
										<span class="shipping-option-label"><fmt:message key="shoppingcart.ship.to.home" /></span>
									</label>
								</div> 
								<div class="padded-details" id="padded-details_S2H_${index}">
									<div class="shipping-details">
										<dsp:droplet name="Switch">
											<dsp:param name="value" param="item.inventoryStatus" />
											<dsp:oparam name="inStock">
												<fmt:message key="shoppingcart.leaves.warehouse" >
													<fmt:param value="${shipWindowMin}"></fmt:param>
													<fmt:param value="${shipWindowMax}"></fmt:param>
												</fmt:message>
											</dsp:oparam>
											<dsp:oparam name="preOrder">
												<fmt:message key="shoppingcart.presale.release.date"/><br/>
												<fmt:message key="shoppingcart.presale.warehouse.message" />
												<dsp:valueof param="item.streetDate"/>
											</dsp:oparam>
										</dsp:droplet>
									</div>
									<a href="javascript:void(0);" class="see-terms"><fmt:message
											key="shoppingcart.see.terms" /></a> </br>
									<dsp:droplet name="Switch">
										<dsp:param name="value" param="item.itemShippingSurcharge" />
										<dsp:oparam name="0.0">
										</dsp:oparam>
										<dsp:oparam name="default">
											<fmt:message key="shoppingcart.shipping.surcharge" />&nbsp;<dsp:valueof param="item.itemShippingSurcharge" format="currency" locale="en_US" converter="currency"/>
										</dsp:oparam>
									</dsp:droplet>
								</div>
								<div class="shopping-cart-surprise">
									<dsp:droplet name="Switch">
										<dsp:param name="value" param="item.skuItem.shipInOrigContainer" />
										<dsp:oparam name="true">
												<a href="javascript:void(0);" class="learn-more-giftWrap">
													<fmt:message key="shoppingcart.spoil.surprise" />
												</a>
												<div class="checkout-flow-sprite checkout-flow-learn-more learn-more-giftWrap"></div>
										</dsp:oparam>
									</dsp:droplet>
								</div>								
							</dsp:oparam>
							<dsp:oparam name="false">
								<div class="ship-to-home">
									<input name="shippingGroup_${index}" class="shipping-radio-btn" value="ship-to-home" type="radio" sg_rel_id="${relationshipItemId}_${metaInfoId}"  sg_rel_index_id="${index}" checked />
									<label class="shipping-radio-item" for="shippingGroup_${index}">
										<div class="checkout-flow-sprite checkout-flow-radio-unselected checkout-flow-radio-selected"></div>
										<span class="shipping-option-label"><fmt:message key="shoppingcart.ship.to.home" /></span>
									</label>
								</div>
								<div class="itemInfoMessage">
									<dsp:valueof param="item.inventoryInfoMessage" valueishtml="true" />
								</div>
							</dsp:oparam>
						</dsp:droplet>
					</label>
					<br>
					<dsp:droplet name="Switch">
						<dsp:param name="value" param="item.inventoryStatus" />
						<dsp:oparam name="inStock">
							<dsp:droplet name="Switch">
								<dsp:param name="value" param="item.inStorePickUpAvailable" />
								<dsp:oparam name="true">
									<label for="free-store-pickup" class="noPointer">
										<div class="free-store-pickup">
											<input name="shippingGroup_${index}" class="shipping-radio-btn" value="free-store-pickup" type="radio" sg_rel_id="${relationshipItemId}_${metaInfoId}"  sg_rel_index_id="${index}" />
											<label class="shipping-radio-item" for="shippingGroup_${index}">
												<div class="checkout-flow-sprite checkout-flow-radio-unselected"></div>
												<span class="shipping-option-label"><fmt:message key="shoppingcart.free.store.pickup" /></span>
											</label>
											<div class="padded-details" id="padded-details_ISPU_${index}">
												<p>
													<a data-toggle="modal" data-target="#findInStoreModal" href="javascript: void(0);"
														onclick="javascript: return loadFindInStore('/tru', '<dsp:valueof param='item.skuId'/>','${relationshipItemId}_${metaInfoId}','<dsp:valueof param='item.quantity'/>', this,'cartFlag');"><fmt:message
															key="shoppingcart.select.stroe" /></a>
												</p>
											</div>
										</div>
									</label>
								</dsp:oparam>
								<dsp:oparam name="false">
									<div class="free-store-pickup notavailable-storepickup">
									<input id="free-store-pickup-3" class="shipping-radio-btn inline" type="radio" value="free-store-pickup" disabled="disabled" aria-label="free store pickup">
										<fmt:message key="shoppingcart.no.store" />
									</div>
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
						<dsp:oparam name="preOrder">
							<div class="free-store-pickup notavailable-storepickup">
								<input id="free-store-pickup-3" class="shipping-radio-btn inline" type="radio" value="free-store-pickup" disabled="disabled" aria-label="free store pickup">
								<fmt:message key="shoppingcart.no.store" />
							</div>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	<div class="popover ship-to-home-popover" role="tooltip"><div class="arrow"></div><p><fmt:message key="shoppingcart.ship.home.popover" /></p></div>
	<div class="popover surcharge-popover" role="tooltip"><div class="arrow"></div><p><fmt:message key="shoppingcart.surcharge.content.popover" /></p></div>
	</fmt:bundle>
</dsp:page>