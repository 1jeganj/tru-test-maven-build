<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:importbean bean="com/tru/common/TRUStoreConfiguration"/>
<dsp:getvalueof var="versionFlag" bean="TRUStoreConfiguration.versioningFlag"/>
<dsp:getvalueof var="staticAssetsVersionFormat"	bean="TRUStoreConfiguration.staticAssetsVersion"/>
<dsp:getvalueof value='<%=atg.nucleus.DynamoEnv.getProperty("staticAssetVersion")%>' var="versionNumbers"/>
  <%-- <link rel="stylesheet" href="${contextPath}/css/toolkit.css">
    <link rel="stylesheet" href="${contextPath}/css/components.css">
    <link rel="stylesheet" href="${contextPath}/css/structures.css">
    <link rel="stylesheet" href="${contextPath}/css/templates.css">
	<link rel="stylesheet" href="${contextPath}/css/tRus_custom.css">
	<link rel="stylesheet" href="${contextPath}/css/cart.css">
	<link rel="stylesheet" href="${contextPath}/css/productDetails.css"> --%>  
	
	<c:choose>
	<c:when test="${versionFlag}">
		<link rel="stylesheet" href="${TRUCSSPath}css/cartPageHeader.css?${staticAssetsVersionFormat}=${versionNumbers}">
	</c:when>
	<c:otherwise>
		<link rel="stylesheet" href="${TRUCSSPath}css/cartPageHeader.css">
	</c:otherwise>
</c:choose>
</dsp:page> 