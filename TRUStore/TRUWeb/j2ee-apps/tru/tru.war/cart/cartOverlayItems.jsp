<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/com/tru/commerce/cart/droplet/CartItemDetailsDroplet" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />

	<dsp:getvalueof var="existingRelationShipId" param="relationShipId" />

	<dsp:droplet name="CartItemDetailsDroplet">
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:param name="fromOverlay" value="fromOverlay" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="items" param="items" />
		</dsp:oparam>
	</dsp:droplet>

	<dsp:droplet name="ForEach">
		<dsp:param name="array" value="${items}" />
		<dsp:param name="elementName" value="item" />
		<dsp:oparam name="output">

			<dsp:getvalueof var="relationshipItemId" param="item.id" />
			<dsp:getvalueof var="skuId" param="item.skuId"/>
			<dsp:getvalueof var="locationId" param="item.infoMsgLocationId"/>
			<dsp:getvalueof var="finalKey" value="${skuId}_${locationId}"/>
			<dsp:droplet name="IsEmpty">
				<dsp:param
					bean="CartModifierFormHandler.itemInlineMessages"
					name="value" />
				<dsp:oparam name="false">
					<dsp:droplet name="ForEach">
						<dsp:param name="array"
							bean="CartModifierFormHandler.itemInlineMessages" />
						<dsp:param name="elementName" value="message" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="key" param='key' />
							<c:choose>
								<c:when test="${key eq finalKey}">
									<dsp:getvalueof var="isInlineMsg" value="true"/>
								</c:when>
								<c:otherwise>
									<dsp:getvalueof var="isInlineMsg" value="false"/>
								</c:otherwise>
							</c:choose>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="true">
					<dsp:getvalueof var="isInlineMsg" value="false"/>
				</dsp:oparam>
			</dsp:droplet>
			<dsp:droplet name="Switch">
				<dsp:param name="value" param="item.inventoryStatus" />
				<dsp:oparam name="inStock">
					<c:choose>
						<c:when test="${relationshipItemId eq existingRelationShipId}">
							<dsp:droplet name="Switch">
								<dsp:param bean="CartModifierFormHandler.formError" name="value" />
								<dsp:oparam name="true">
									<div class="product-information error"
										id="product-information_${relationshipItemId}">
								</dsp:oparam>
								<dsp:oparam name="default">
									<div class="product-information" id="product-information_${relationshipItemId}">
								</dsp:oparam>
							</dsp:droplet>
						</c:when>
						<c:when test="${isInlineMsg eq true}">
							<div class="product-information error" id="product-information_${relationshipItemId}">
						</c:when>
						<c:otherwise>
							<div class="product-information" id="product-information_${relationshipItemId}">
						</c:otherwise>
						</c:choose>
						<dsp:include page="cartOverlayItemDetails.jsp">
							<dsp:param name="item" param="item" />
							<dsp:getvalueof var="index" param="index" />
							<dsp:param name="existingRelationShipId" value="${existingRelationShipId}" />
							<dsp:param name="inlineMessage" param="inlineMessage" />
						</dsp:include>
						</div>
					
				</dsp:oparam>
				<dsp:oparam name="preOrder">
					<c:choose>
						<c:when test="${relationshipItemId eq existingRelationShipId}">
							<dsp:droplet name="Switch">
								<dsp:param bean="CartModifierFormHandler.formError" name="value" />
								<dsp:oparam name="true">
									<div class="product-information partial pre-release error"
										id="product-information_${relationshipItemId}">
								</dsp:oparam>
								<dsp:oparam name="default">
									<div class="product-information partial pre-release"
										id="product-information_${relationshipItemId}">
								</dsp:oparam>
							</dsp:droplet>
						</c:when>
						<c:when test="${isInlineMsg eq true}">
							<div class="product-information partial pre-release error" id="product-information_${relationshipItemId}">
						</c:when>
						<c:otherwise>
							<div class="product-information partial pre-release" id="product-information_${relationshipItemId}">
						</c:otherwise>
						</c:choose>
						<dsp:include page="cartOverlayItemDetails.jsp">
							<dsp:param name="item" param="item" />
							<dsp:getvalueof var="index" param="index" />
							<dsp:param name="existingRelationShipId" value="${existingRelationShipId}" />
							<dsp:param name="inlineMessage" param="inlineMessage" />
						</dsp:include>
						</div>
					
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>

	</dsp:droplet>
</dsp:page>
