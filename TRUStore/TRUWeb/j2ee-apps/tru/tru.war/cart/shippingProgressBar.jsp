<dsp:page>
	<dsp:importbean bean="/com/tru/commerce/cart/droplet/FreeShippingProgressBarDroplet"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:getvalueof param="orderTotal" var="orderTotal"/>
	<dsp:droplet name="FreeShippingProgressBarDroplet">
		<dsp:param name="profile" bean="Profile" />
		<dsp:param name="orderTotal" value="${orderTotal}"/>
		<dsp:oparam name="empty">
			<dsp:getvalueof var="isEmpty" value="true" scope="request" vartype="java.lang.Boolean"/>
		</dsp:oparam>
		<dsp:oparam name="output">
			<dsp:getvalueof var="isFreeShipping" param="freeShippingParam" scope="request" vartype="java.lang.Boolean"/>
			<dsp:getvalueof var="remainingAmount" param="remainingAmount" scope="request"/>
			<dsp:getvalueof var="spendAmount" param="spendValue" scope="request"/>
			<dsp:getvalueof var="isEmpty" value="false" scope="request" vartype="java.lang.Boolean"/>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>