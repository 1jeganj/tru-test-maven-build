<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<%-- <dsp:getvalueof var="pageName" param="pageName" /> --%>
	<dsp:getvalueof var="validationFailed" param="validationFailed"></dsp:getvalueof>
	<c:choose>
		<c:when test="${pageName eq 'Review'}">
			<c:if test="${validationFailed}" >
				<dsp:droplet name="IsEmpty">
					<dsp:param name="value" param="items" />
					<dsp:oparam name="false">
						 <div class="shopping-cart-info-state">
						 	<div class="shopping-cart-info-state-header row">
								<div class="error-state-exclamation-lg img-responsive col-md-2"></div>
								<div class="shopping-cart-info-state-header-text col-md-10">
								<fmt:message key="shoppingcart.oops.issue" />
									<div class="shopping-cart-info-state-subheader">
										 <fmt:message key="inventory.no.longer.available" />
									</div>
								</div>
							 </div>
						 </div>
					</dsp:oparam>
				</dsp:droplet>
			</c:if>
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="cartLoad" param="cartLoad"/>
			<c:if test="${not empty cartLoad || validationFailed}">
				<dsp:droplet name="ForEach">
					<dsp:param name="array" param="items" />
					<dsp:param name="elementName" value="item" />
					<dsp:oparam name="output">
						<dsp:droplet name="IsEmpty">
							<dsp:param name="value" param="item.inventoryInfoMessage" />
							<dsp:oparam name="false">
							<dsp:getvalueof var="infoState" value="true"/>
							<c:if test="${infoState eq true}" >
							<div class="shopping-cart-info-state">
							</c:if>
								<div class="shopping-cart-info-state-header row">
									<div class="error-state-exclamation-lg img-responsive col-md-2"></div>
									<div class="shopping-cart-info-state-header-text col-md-10">
									<fmt:message key="shoppingcart.oops.issue" />
										<div class="shopping-cart-info-state-subheader"><dsp:valueof valueishtml="true" param="item.inventoryInfoMessage"/></div>
									</div>
								</div>
								<c:if test="${infoState eq true}" >
								 </div>
								</c:if>
								<dsp:getvalueof var="infoState" value="false"/>
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
				</dsp:droplet>
			</c:if>
		</c:otherwise>
	</c:choose>
	<dsp:droplet name="IsEmpty">
		<dsp:param name="value" param="removedInfoMessage" />
		<dsp:oparam name="false">
			<dsp:droplet name="ForEach">
				<dsp:param name="array" param="removedInfoMessage" />
				<dsp:param name="elementName" value="infoMessage" />
				<dsp:oparam name="outputStart">
					<div class="shopping-cart-info-state">
				</dsp:oparam>
				<dsp:oparam name="output">
					<div class="shopping-cart-info-state-header row">
						<div class="error-state-exclamation-lg img-responsive col-md-2"></div>
						<div class="shopping-cart-info-state-header-text col-md-10">
						<fmt:message key="shoppingcart.oops.issue" />
							<div class="shopping-cart-info-state-subheader"><dsp:valueof  valueishtml="true" param="infoMessage"/></div>
						</div>
					</div>
				</dsp:oparam>
				<dsp:oparam name="outputEnd">
					</div>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="IsEmpty">
		<dsp:param name="value" param="qtyAdjustedInfoMessage" />
		<dsp:oparam name="false">
			<dsp:droplet name="ForEach">
				<dsp:param name="array" param="qtyAdjustedInfoMessage" />
				<dsp:param name="elementName" value="infoMessage" />
				<dsp:oparam name="outputStart">
					<div class="shopping-cart-info-state">
				</dsp:oparam>
				<dsp:oparam name="output">
					<div class="shopping-cart-info-state-header row">
						<div class="error-state-exclamation-lg img-responsive col-md-2"></div>
						<div class="shopping-cart-info-state-header-text col-md-10">
						<fmt:message key="shoppingcart.oops.issue" />
							<div class="shopping-cart-info-state-subheader"><dsp:valueof  valueishtml="true" param="infoMessage"/></div>
						</div>
					</div>
				</dsp:oparam>
				<dsp:oparam name="outputEnd">
					</div>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>
