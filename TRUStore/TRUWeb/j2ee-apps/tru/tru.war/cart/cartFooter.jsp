<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof var="enableJSDebug" value="true"/>
	
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:importbean bean="com/tru/common/TRUStoreConfiguration"/>
	<dsp:getvalueof var="staticAssetsVersionFormat" bean="TRUStoreConfiguration.staticAssetsVersion"/>
	<dsp:getvalueof var="versionFlag" bean="TRUStoreConfiguration.versioningFlag"/>
	<dsp:getvalueof value='<%=atg.nucleus.DynamoEnv.getProperty("staticAssetVersion")%>' var="versionNumbers"/>
	
	<dsp:getvalueof var="enableMinification"  bean="TRUStoreConfiguration.enableJSDebug"/>
   <dsp:getvalueof var="debugFlag" param=="debug"/>
	
<dsp:importbean bean="/com/tru/integrations/common/TRUHookLogicConfiguration"/>
<dsp:getvalueof var="hooklogicURL" bean="TRUHookLogicConfiguration.hooklogicURL"/>

	<%--<c:choose>
		<c:when test="${versionFlag}">
			
			<script type="text/javascript" charset="UTF-8" 
				src="${TRUJSPath}javascript/vendor.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" charset="UTF-8" 
				src="${TRUJSPath}javascript/maskPassword.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" charset="UTF-8" 
				src="${TRUJSPath}javascript/toolkit.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" charset="UTF-8" 
				src="${TRUJSPath}javascript/jquery.validate.min.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" charset="UTF-8" 
				src="${TRUJSPath}javascript/zipCodePlugin.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" charset="UTF-8" 
				src="${TRUJSPath}javascript/pwstrength.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" charset="UTF-8" 
				src="${TRUJSPath}javascript/tRus_custom.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" charset="UTF-8" 
				src="${TRUJSPath}javascript/cart.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" charset="UTF-8" 
				src="${TRUJSPath}javascript/productDetails.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" charset="UTF-8" 
				src="${TRUJSPath}javascript/storelocator.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" charset="UTF-8" 
				src="${TRUJSPath}javascript/jquery.cookie.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" charset="UTF-8" 
				src="${TRUJSPath}javascript/reward-validation.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" charset="UTF-8" 
				src="${TRUJSPath}javascript/tru_clientSideValidation.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			<script type="text/javascript" charset="UTF-8" 
				src="${TRUJSPath}javascript/truMyAccount.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
		</c:when>
		<c:otherwise>
			
			<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/vendor.js"></script>
			<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/maskPassword.js"></script>
			<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/toolkit.js"></script>
			<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/jquery.validate.min.js"></script>
			<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/zipCodePlugin.js"></script>
			<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/pwstrength.js"></script>
			<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/tRus_custom.js"></script>
			<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/cart.js"></script>
			<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/productDetails.js"></script>
			<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/storelocator.js"></script>
			<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/jquery.cookie.js"></script>
			<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/reward-validation.js"></script>
			<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/tru_clientSideValidation.js"></script>
			<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/truMyAccount.js"></script>
			
		</c:otherwise>
	</c:choose> --%>
	
	
	 
	 <dsp:include page="/jstemplate/resourceBundle.jsp"></dsp:include>
	 
	 <c:choose>
	 	<c:when test="${enableMinification and debugFlag eq '1'}">
	 		<c:choose>
	 			<c:when test="${versionFlag}">
					<script type="text/javascript" charset="UTF-8" 
						src="${TRUJSPath}javascript/vendor.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8" 
						src="${TRUJSPath}javascript/maskPassword.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script type="text/javascript" charset="UTF-8" 
						src="${TRUJSPath}javascript/jquery.datetimepicker.full.min.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script type="text/javascript" charset="UTF-8" 
						src="${TRUJSPath}javascript/zipCodePlugin.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8" 
						src="${TRUJSPath}javascript/toolkit.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8" 
						src="${TRUJSPath}javascript/jquery.validate.min.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					
					<script type="text/javascript" charset="UTF-8" 
						src="${TRUJSPath}javascript/pwstrength.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8" 
						src="${TRUJSPath}javascript/tRus_custom.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8" 
						src="${TRUJSPath}javascript/cart.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8" 
						src="${TRUJSPath}javascript/productDetails.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8" 
						src="${TRUJSPath}javascript/storelocator.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8" 
						src="${TRUJSPath}javascript/jquery.cookie.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8" 
						src="${TRUJSPath}javascript/reward-validation.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8" 
						src="${TRUJSPath}javascript/tru_clientSideValidation.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8" 
						src="${TRUJSPath}javascript/truMyAccount.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
				</c:when>
				<c:otherwise>
					<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/vendor.js"></script>
					<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/maskPassword.js"></script>
					<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/toolkit.js"></script>
					<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/jquery.validate.min.js"></script>
					<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/zipCodePlugin.js"></script>
					<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/pwstrength.js"></script>
					<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/tRus_custom.js"></script>
					<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/cart.js"></script>
					<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/productDetails.js"></script>
					<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/storelocator.js"></script>
					<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/jquery.cookie.js"></script>
					<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/reward-validation.js"></script>
					<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/tru_clientSideValidation.js"></script>
					<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/truMyAccount.js"></script>
				</c:otherwise>
			</c:choose>
	 </c:when>
	 <c:otherwise>
	 	<c:choose>
			<c:when test="${versionFlag}">
				<script type="text/javascript" charset="UTF-8"
				src="${TRUJSPath}javascript/cartPageFooter.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
			</c:when>
			<c:otherwise>
				<script type="text/javascript" charset="UTF-8"
				src="${TRUJSPath}javascript/cartPageFooter.js"></script>
			</c:otherwise>
		</c:choose>
	 </c:otherwise>
	</c:choose>
	 	

	<dsp:droplet name="/atg/targeting/TargetingForEach">
		<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/ShoppingCart/CartFooterTargeter"/>
		<dsp:oparam name="output">
			<dsp:valueof param="element.data" valueishtml="true" />
		</dsp:oparam>
	</dsp:droplet>
	<dsp:include page="/common/syncCookieIframe.jsp"></dsp:include>
	<div id="crossDomainCookieIframeLoader" class="hide"></div>
	
	<script type="text/javascript">
			$.getScript("${hooklogicURL}",function(){
			});
	</script>
	
</dsp:page> 