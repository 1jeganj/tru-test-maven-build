<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/com/tru/commerce/cart/droplet/DonationSKULookupDroplet"/>
	<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale" />
	<dsp:getvalueof var="cacheKey" value="getDonationSKU_${siteId}${locale}" />
	<dsp:droplet name="/com/tru/cache/TRUDonationSKUDropletCache">
		<dsp:param name="key" value="${cacheKey}"/>
		<dsp:oparam name="output">
			<dsp:droplet name="DonationSKULookupDroplet">
				<dsp:oparam name="output">
					<dsp:getvalueof var="donationSKUItem" param="donationSKUItem"/>
					<dsp:getvalueof var="donationProductItem" param="donationProductItem"/>
				</dsp:oparam>
			</dsp:droplet>
			<input id="donationProductId" type="hidden" value="${donationProductItem.id}" />
			<input id="donationSKUId" type="hidden" value="${donationSKUItem.id}" />
			<dsp:getvalueof value="${originatingRequest.contextPath}"
				var="contextPath" />
			<div class="donation-error-message">
				<div class="shopping-cart-error-state">
				<div class="shopping-cart-error-state-header row">
					<div class="error-state-exclamation-lg img-responsive col-md-2"></div>
					<div class="shopping-cart-error-state-header-text col-md-10">
						<fmt:message key="shoppingcart.oops.issue" />
						<div class="shopping-cart-error-state-subheader"><fmt:message key="shoppingcart.donation.error.message"/>
						</div>
					</div>
					</div>
				</div>
			</div>
			<dsp:droplet name="/atg/targeting/TargetingForEach">
				<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/ShoppingCart/CharityDonationContentTargeter"/>
				<dsp:oparam name="output">
					<dsp:valueof param="element.data" valueishtml="true" />
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>
