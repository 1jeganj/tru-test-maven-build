<dsp:page>

	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/atg/commerce/catalog/RunServiceFormHandler" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/commerce/promotion/CouponFormHandler" />
	<dsp:importbean bean="/com/tru/commerce/cart/droplet/CollectionItemsDroplet"/>
	<dsp:importbean bean="/atg/multisite/Site" />
	<dsp:getvalueof var="formID" param="formID"></dsp:getvalueof>
	<dsp:getvalueof  var="siteId" bean="Site.id" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />

	<c:choose>
		<c:when test="${formID eq 'applyCoupon'}">
			<dsp:getvalueof param="couponId" var="couponId"></dsp:getvalueof>
			<dsp:getvalueof param="orderId" var="orderId"></dsp:getvalueof>
			<dsp:setvalue bean="CouponFormHandler.couponClaimCode" value="${couponId}" />
			<dsp:setvalue bean="CouponFormHandler.orderId" value="${orderId}" />
			<dsp:setvalue bean="CouponFormHandler.claimCoupon" value="submit" />
			<dsp:droplet name="Switch">
				<dsp:param bean="CouponFormHandler.formError" name="value" />
				<dsp:oparam name="true">
					<dsp:droplet name="ErrorMessageForEach">
						<dsp:param name="exceptions" bean="CouponFormHandler.formExceptions" />
						<dsp:oparam name="output">
						<dsp:getvalueof var="errorCode" param="errorCode" />
						<c:choose>
							<c:when test="${errorCode eq 'Not Applied'}">
								<div id="coupon_msg_with_errorcode">
									<dsp:valueof param="message" />
								</div>
								<div id="coupon_error_code">
									<dsp:valueof value="${errorCode}" />
								</div>
							</c:when>
							<c:otherwise>
								<div id="coupon_msg">
									<dsp:valueof param="message" />
								</div>
							</c:otherwise>
						</c:choose>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</c:when>
		<c:when test="${formID eq 'removecoupon'}">
		<dsp:getvalueof param="couponId" var="couponId"></dsp:getvalueof>
			<dsp:setvalue bean="CouponFormHandler.couponClaimCode" value="${couponId}" />
			<dsp:setvalue bean="CouponFormHandler.removeCoupon" value="submit" />
			
		</c:when>
		<c:when test="${formID eq 'removeLineItemFromCart'}">
		<dsp:getvalueof param="commerceItemId" var="relationShipId"></dsp:getvalueof>
			<dsp:droplet name="Switch">
				<dsp:param param="action" name="value" />
				<dsp:oparam name="update">
					<dsp:setvalue bean="CartModifierFormHandler.splitItemRemove" value="true" />
					<dsp:setvalue bean="CartModifierFormHandler.relationShipId" value="${relationShipId}" />
					<dsp:setvalue bean="CartModifierFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
					<dsp:setvalue bean="CartModifierFormHandler.wrapItemQuantity" paramvalue="quantity" />
					<dsp:setvalue bean="CartModifierFormHandler.setOrderByRelationshipId" value="submit" />
				</dsp:oparam>
				<dsp:oparam name="remove">
					<dsp:getvalueof param="commerceItemId" var="commerceItemId"></dsp:getvalueof>
					<c:set var="commerceItemIdArray" value="${fn:split(commerceItemId, ' ')}" />
					<dsp:setvalue bean="CartModifierFormHandler.relationShipId"	paramvalue="commerceItemId" />
					<dsp:setvalue bean="CartModifierFormHandler.removalRelationshipIds" value="${commerceItemIdArray}" />
					<dsp:setvalue bean="CartModifierFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
					<dsp:setvalue bean="CartModifierFormHandler.wrapItemQuantity" paramvalue="quantity" />
					<dsp:setvalue bean="CartModifierFormHandler.removeItemFromOrderByRelationshipId" value="submit" />
				</dsp:oparam>
				<dsp:oparam name="outOfStock">
					<dsp:setvalue bean="CartModifierFormHandler.outOfStockId"	value="${relationShipId}" />
					<dsp:setvalue bean="CartModifierFormHandler.removeOutOfStockItem" value="submit" />
				</dsp:oparam>
			</dsp:droplet>
			<dsp:include page="/cart/shoppingCartInclude.jsp">
						<dsp:param name="existingRelationShipId" value="${relationShipId}" />
						<dsp:param name="inlineMessage" value="${inlineMessage}" />
					</dsp:include>
		</c:when>
		<c:when test="${formID eq 'updateCartLineItemQuantity'}">
			<dsp:getvalueof param="relationShipId" var="relationShipId"></dsp:getvalueof>
			<dsp:setvalue bean="CartModifierFormHandler.relationShipId" value="${relationShipId}" />
			<dsp:setvalue bean="CartModifierFormHandler.giftWrapSkuId" paramvalue="giftWrapSkuId" />
			<dsp:setvalue bean="CartModifierFormHandler.setOrderByRelationshipId" value="submit" />
			<dsp:droplet name="Switch">
				<dsp:param bean="CartModifierFormHandler.formError" name="value" />
				<dsp:oparam name="true">
					<dsp:droplet name="ErrorMessageForEach">
						<dsp:param name="exceptions" bean="CartModifierFormHandler.formExceptions" />
						<dsp:oparam name="output">
							<dsp:droplet name="Switch">
								<dsp:param param="errorCode" name="value" />
								<dsp:oparam name="inline_msg">
									<dsp:getvalueof var="inlineMessage" param="message"/>
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
			<dsp:droplet name="Switch">
				<dsp:param param="fromOverlay" name="value" />
				<dsp:oparam name="addToCartOverlay">
					<dsp:include page="/cart/addToCartOverlayContent.jsp">
						<dsp:param name="relationShipId" value="${relationShipId}" />
						<dsp:param name="inlineMessage" value="${inlineMessage}" />
					</dsp:include>
				</dsp:oparam>
				<dsp:oparam name="default">
					<dsp:include page="/cart/shoppingCartInclude.jsp">
						<dsp:param name="existingRelationShipId" value="${relationShipId}" />
						<dsp:param name="inlineMessage" value="${inlineMessage}" />
					</dsp:include>
				</dsp:oparam>
			</dsp:droplet>
		</c:when>
		<c:when test="${formID eq 'updateLineItemSG'}">
			<dsp:getvalueof param="relationShipId" var="relationShipId"></dsp:getvalueof>
			<dsp:setvalue bean="CartModifierFormHandler.relationShipId" value="${relationShipId}" />
			<dsp:getvalueof param="locationId" var="locationId"></dsp:getvalueof>
			<dsp:getvalueof param="metaInfoId" var="metaInfoId"></dsp:getvalueof>
			<dsp:setvalue bean="CartModifierFormHandler.metaInfoId" value="${metaInfoId}" />
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" value="${locationId}" />
				<dsp:oparam name="false">
					<dsp:setvalue bean="CartModifierFormHandler.locationId" value="${locationId}" />
				</dsp:oparam>
			</dsp:droplet>
			<dsp:setvalue bean="CartModifierFormHandler.shippingGroupChange" value="submit" />
			<dsp:droplet name="Switch">
				<dsp:param bean="CartModifierFormHandler.formError" name="value" />
				<dsp:oparam name="true">
					<dsp:droplet name="ErrorMessageForEach">
						<dsp:param name="exceptions" bean="CartModifierFormHandler.formExceptions" />
						<dsp:oparam name="output">
							<dsp:droplet name="Switch">
								<dsp:param param="errorCode" name="value" />
								<dsp:oparam name="inline_msg">
									<dsp:getvalueof var="inlineMessage" param="message"/>
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" bean="CartModifierFormHandler.newRelationId" />
				<dsp:oparam name="false">
					<dsp:getvalueof  var="newRelationId" bean="CartModifierFormHandler.newRelationId"/>
				</dsp:oparam>
				<dsp:oparam name="true">
				<dsp:param name="newRelationId" value="${relationShipId}" />
				</dsp:oparam>
			</dsp:droplet>
			<dsp:include page="/cart/shoppingCartInclude.jsp">
				<dsp:param name="existingRelationShipId" value="${newRelationId}" />
				<dsp:param name="inlineMessage" value="${inlineMessage}" />
			</dsp:include>
		</c:when>
		<c:when test="${formID eq 'selectGiftWrapToItem'}">
			<dsp:setvalue bean="CartModifierFormHandler.itemIdForGiftWrap" paramvalue="commerceItemId" />
			<dsp:setvalue bean="CartModifierFormHandler.selectGiftWrapToItem" value="submit" />
			<dsp:droplet name="Switch">
				<dsp:param bean="CartModifierFormHandler.formError" name="value" />
				<dsp:oparam name="true">
					<dsp:droplet name="ErrorMessageForEach">
						<dsp:param name="exceptions" bean="CartModifierFormHandler.formExceptions" />
						<dsp:oparam name="outputStart">
							Following are the form errors:<br />
						</dsp:oparam>
						<dsp:oparam name="output">
							<dsp:valueof param="message" />
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="false">
				</dsp:oparam>
			</dsp:droplet>
		</c:when>
		<c:when test="${formID eq 'addItemToCart'}">
			<dsp:getvalueof var="productId" param="productId" />
			<dsp:getvalueof var="quantity" param="quantity" />
			<dsp:getvalueof  var="skuId" param="skuId" />
			<dsp:getvalueof  var="locationId" param="locationId" />
			<dsp:getvalueof  var="channelId" param="channelId" />
			<dsp:getvalueof  var="channelType" param="channelType" />
			<dsp:getvalueof  var="channelItemFromPDP" param="channelItemFromPDP" />
			<dsp:setvalue bean="CartModifierFormHandler.productId" value="${productId}" />
			<c:set var="catalogRefIds" value="${fn:split(skuId, ' ')}" />
			<dsp:setvalue bean="CartModifierFormHandler.catalogRefIds" value="${catalogRefIds}" />
			<dsp:setvalue bean="CartModifierFormHandler.quantity" value="${quantity}" />
			<dsp:setvalue bean="CartModifierFormHandler.locationId" value="${locationId}" />
			<dsp:setvalue bean="CartModifierFormHandler.siteId" value="${siteId}" />
			<dsp:setvalue bean="CartModifierFormHandler.channelId" value="${channelId}"/>
			<dsp:setvalue bean="CartModifierFormHandler.channelType" value="${channelType}"/>
			<dsp:setvalue bean="CartModifierFormHandler.channelItemFromPDP" value="${channelItemFromPDP}"/>
			<dsp:setvalue bean="CartModifierFormHandler.addItemToOrder" value="submit" />
			
					<dsp:include page="/cart/addToCartOverlay.jsp">
					<dsp:param name="pageFrom" value="Product List Page"/>
					<dsp:param name="productId" param="productId"/>
					</dsp:include>
				
		</c:when>
		<c:when test="${formID eq 'addItemFromCartPage'}">
			<dsp:getvalueof var="productId" param="productId" />
			<dsp:getvalueof var="quantity" param="quantity" />
			<dsp:getvalueof  var="skuId" param="skuId" />
			<dsp:getvalueof  var="locationId" param="locationId" />
			<dsp:setvalue bean="CartModifierFormHandler.productId" value="${productId}" />
			<c:set var="catalogRefIds" value="${fn:split(skuId, ' ')}" />
			<dsp:setvalue bean="CartModifierFormHandler.catalogRefIds" value="${catalogRefIds}" />
			<dsp:setvalue bean="CartModifierFormHandler.quantity" value="${quantity}" />
			<dsp:setvalue bean="CartModifierFormHandler.locationId" value="${locationId}" />
			<dsp:setvalue bean="CartModifierFormHandler.siteId" value="${siteId}" />
			<dsp:setvalue bean="CartModifierFormHandler.addItemToOrder" value="submit" />
			
			<dsp:include page="/cart/shoppingCartInclude.jsp"/>
				
		</c:when>
		<c:when test="${formID eq 'addDonationItemToCart'}">
			<dsp:getvalueof var="productId" param="productId" />
			<dsp:getvalueof var="quantity" value="1" />
			<dsp:getvalueof  var="skuId" param="skuId" />
			<dsp:getvalueof  var="donationAmount" param="donationAmount" />
			<dsp:setvalue bean="CartModifierFormHandler.productId" value="${productId}" />
			<c:set var="catalogRefIds" value="${fn:split(skuId, ' ')}" />
			<dsp:setvalue bean="CartModifierFormHandler.catalogRefIds" value="${catalogRefIds}" />
			<dsp:setvalue bean="CartModifierFormHandler.quantity" value="${quantity}" />
			<dsp:setvalue bean="CartModifierFormHandler.donationAmount" value="${donationAmount}" />
			<dsp:setvalue bean="CartModifierFormHandler.donationItem" value="true" />
			<dsp:setvalue bean="CartModifierFormHandler.addDonationItemToOrder" value="submit" />
			
			<dsp:include page="/cart/shoppingCartInclude.jsp"/>
			
		</c:when>
		<c:when test="${formID eq 'removeDonationItemFromCart'}">
			<dsp:getvalueof param="commerceItemId" var="commerceItemId"></dsp:getvalueof>
			<c:set var="commerceItemIdArray" value="${fn:split(commerceItemId, ' ')}" />
			<dsp:setvalue bean="CartModifierFormHandler.removalCommerceIds" value="${commerceItemIdArray}" />
			<dsp:setvalue bean="CartModifierFormHandler.removeItemFromOrder" value="submit" />
			
			<dsp:include page="/cart/shoppingCartInclude.jsp"/>
			
		</c:when>
		
		<c:when test="${formID eq 'applyAbandonCartPromotion'}">
			<dsp:getvalueof param="promotionId" var="promotionId"></dsp:getvalueof>
			<dsp:getvalueof var="orderId" bean="ShoppingCart.current.id"></dsp:getvalueof>
			
			<dsp:setvalue bean="CouponFormHandler.couponClaimCode" value="${promotionId}"/>
			<dsp:setvalue bean="CouponFormHandler.orderId" value="${orderId}"/>
			<dsp:setvalue bean="CouponFormHandler.claimCoupon" value="submit"/>
			<dsp:setvalue bean="CouponFormHandler.claimCouponSuccessURL" value="${contextPath}cart/shoppingCart.jsp"/>
			
		</c:when>
		
		<c:when test="${formID eq 'editCartItem'}">
			<dsp:getvalueof var="productId" param="productId" />
			<dsp:getvalueof var="quantity" param="quantity" />
			<dsp:getvalueof  var="skuId" param="skuId" />
			<dsp:getvalueof  var="commItemId" param="updateCommerItemIds" />
			<dsp:getvalueof  var="relationShipId" param="relationShipId" />
			<dsp:setvalue bean="CartModifierFormHandler.productId" value="${productId}" />
			<c:set var="catalogRefIds" value="${fn:split(skuId, ' ')}" />
			<c:set var="commItemIds" value="${fn:split(commItemId, ' ')}" />
			<dsp:setvalue bean="CartModifierFormHandler.catalogRefIds" value="${catalogRefIds}" />
			<dsp:setvalue bean="CartModifierFormHandler.quantity" value="${quantity}" />
			<dsp:setvalue bean="CartModifierFormHandler.updateCommerItemIds" value="${commItemIds}"/>
			<dsp:setvalue bean="CartModifierFormHandler.relationShipId" value="${relationShipId}" />
			<dsp:setvalue bean="CartModifierFormHandler.siteId" value="${siteId}" />
            <dsp:setvalue bean="CartModifierFormHandler.updateCommerceItem" value="update"/>
            
            <dsp:droplet name="Switch">
				<dsp:param bean="CartModifierFormHandler.formError" name="value" />
				<dsp:oparam name="true">
					<dsp:droplet name="ErrorMessageForEach">
						<dsp:param name="exceptions" bean="CartModifierFormHandler.formExceptions" />
						<dsp:oparam name="output">
							<dsp:droplet name="Switch">
								<dsp:param param="errorCode" name="value" />
								<dsp:oparam name="inline_msg">
									<dsp:getvalueof var="inlineMessage" param="message"/>
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
			<dsp:include page="/cart/shoppingCartInclude.jsp">
				<dsp:param name="existingRelationShipId" value="${relationShipId}" />
				<dsp:param name="inlineMessage" value="${inlineMessage}" />
			</dsp:include>
		</c:when>
		<c:when test="${formID eq 'addCollectionToCart'}">
		<dsp:getvalueof var="colletinToCartString" param="colletinToCartString"></dsp:getvalueof>
		<dsp:getvalueof var="addItemCount" param="addItemCount"></dsp:getvalueof>
			<dsp:droplet name="CollectionItemsDroplet">
				<dsp:param name="collection" value="${colletinToCartString}" />
				<dsp:oparam name="output">
					<dsp:droplet name="ForEach">
						<dsp:param name="array" param="collection"/>
						<dsp:param name="elementName" value="item"/>
						<dsp:param name="indexName" value="itemIndex"/>
						<dsp:oparam name="outputStart">
						    <dsp:setvalue bean="CartModifierFormHandler.addItemCount" value="${addItemCount}"/>
						    <dsp:getvalueof var="collectionRequest" vartype="java.lang.string" value="collection" />
						    <dsp:setvalue bean="CartModifierFormHandler.collectionRequest" value="${collectionRequest}"/>
						</dsp:oparam>
						<dsp:oparam name="output">
							<dsp:getvalueof var="item" param="item" />
							<dsp:setvalue bean="CartModifierFormHandler.items[param:itemIndex].quantity" value="${item.quantity}"></dsp:setvalue>
							<dsp:setvalue bean="CartModifierFormHandler.items[param:itemIndex].locationId" value="${item.locationId}"/>
							<dsp:setvalue bean="CartModifierFormHandler.items[param:itemIndex].productId" value="${item.productId}"/>
							<dsp:setvalue bean="CartModifierFormHandler.items[param:itemIndex].catalogRefId" value="${item.skuId}"/>
							<dsp:setvalue bean="CartModifierFormHandler.items[param:itemIndex].siteId" value="${siteId}" /> 
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>	
			</dsp:droplet>
			<dsp:setvalue bean="CartModifierFormHandler.addItemToOrder" value="submit" />
			<%-- <dsp:droplet name="Switch">
				<dsp:param bean="CartModifierFormHandler.formError" name="value" />
				<dsp:oparam name="true">
					<dsp:droplet name="ErrorMessageForEach">
						<dsp:param name="exceptions" bean="CartModifierFormHandler.formExceptions" />
						<dsp:oparam name="outputStart">
							Following are the form errors:<br />
						</dsp:oparam>
						<dsp:oparam name="output">
							<li style="color: red"><dsp:valueof param="message" /></li>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="false"> --%>
					<dsp:include page="/cart/addToCartOverlay.jsp" />
				<%-- </dsp:oparam>
			</dsp:droplet> --%>
		</c:when>
		<c:when test="${formID eq 'addUpdateBppItem'}">
			<dsp:getvalueof  var="bppSkuId" param="bppSkuId" />
			<dsp:getvalueof  var="bppInfoId" param="bppItemInfoId" />
			<dsp:getvalueof  var="bppItemId" param="bppItemId" />
			<dsp:getvalueof  var="relationShipId" param="relationShipId" />
			<dsp:getvalueof  var="addUpdateFlag" param="addUpdateFlag" />
			<dsp:setvalue bean="CartModifierFormHandler.bppSkuId" value="${bppSkuId}" />
			<dsp:setvalue bean="CartModifierFormHandler.bppItemId" value="${bppItemId}"/>
			<dsp:setvalue bean="CartModifierFormHandler.BPPInfoId" value="${bppInfoId}"/>
			<dsp:setvalue bean="CartModifierFormHandler.relationShipId" value="${relationShipId}" />
            <dsp:setvalue bean="CartModifierFormHandler.addUpdateBppItem" value="submit" />
            <dsp:droplet name="Switch">
				<dsp:param param="fromCartOverlay" name="value" />
				<dsp:oparam name="fromCartOverlay">
					<dsp:include page="/cart/addToCartOverlayContent.jsp"/>
				</dsp:oparam>
				<dsp:oparam name="store-pickup">
				</dsp:oparam>
				<dsp:oparam name="default">
					<dsp:include page="/cart/shoppingCartInclude.jsp"/>
				</dsp:oparam>
			</dsp:droplet>
		</c:when>
		<c:when test="${formID eq 'removeBPPItemInfo'}">
			<dsp:getvalueof param="BPPInfoId" var="BPPInfoId"></dsp:getvalueof>
			<dsp:getvalueof param="relationShipId" var="relationShipId"></dsp:getvalueof>
			<dsp:setvalue bean="CartModifierFormHandler.relationShipId" value="${relationShipId}" />
			<dsp:setvalue bean="CartModifierFormHandler.removeBPPItemRelationship" value="submit" />
			<dsp:droplet name="Switch">
				<dsp:param param="fromCartOverlay" name="value" />
				<dsp:oparam name="fromCartOverlay">
					<dsp:include page="/cart/addToCartOverlayContent.jsp"/>
				</dsp:oparam>
				<dsp:oparam name="store-pickup">
				</dsp:oparam>
				<dsp:oparam name="default">
					<dsp:include page="/cart/shoppingCartInclude.jsp"/>
				</dsp:oparam>
			</dsp:droplet>
		</c:when>
		<c:when test="${formID eq 'loadSignInOverlay'}">
			<dsp:include page="/myaccount/signInModal.jsp"/>
		</c:when>
		<c:when test="${formID eq 'moveToPurchaseInfo'}">
			<dsp:setvalue bean="CartModifierFormHandler.checkForChangedQuantity" value="false" />
			<dsp:setvalue bean="CartModifierFormHandler.moveToCheckout" value="Submit" />
			{
				"hasValidationErrors" : "<dsp:valueof bean="ShoppingCart.current.validationFailed"/>"
			}
		</c:when>
		<c:when test="${formID eq 'cmsForm'}">
		<dsp:getvalueof  var="performSwitch" param="performSwitch" />
		<dsp:setvalue bean="RunServiceFormHandler.performSwitch" value="${performSwitch}" />
		<dsp:setvalue bean="RunServiceFormHandler.runProcess" value="submit" />
		</c:when>
	</c:choose>
</dsp:page>