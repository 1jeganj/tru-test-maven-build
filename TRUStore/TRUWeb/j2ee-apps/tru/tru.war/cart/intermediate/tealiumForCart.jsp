<dsp:page>
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
<dsp:importbean bean="/atg/commerce/catalog/ProductLookup" />
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/com/tru/commerce/order/droplet/DisplayOrderDetailsDroplet" />
<dsp:importbean bean="/com/tru/commerce/cart/droplet/CartItemDetailsDroplet" />
<dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
<dsp:getvalueof var="loginStatus" bean="Profile.transient" />
<dsp:getvalueof var="customerEmail" bean="Profile.login"/>
<dsp:getvalueof var="customerID" bean="Profile.id"/>
<dsp:getvalueof var="customerDOB" bean="Profile.dateOfBirth"/>
<dsp:getvalueof param="pageName" var="tPageName" />
<dsp:getvalueof param="pageType" var="tPageType" />
<dsp:getvalueof param="page" var="page" />
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:getvalueof var="order" bean="ShoppingCart.current" />
<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>
<c:set var="tPageName" value="/cart/shoppingCartPage"/>
<c:set var="tPageType" value="cart"/>
<c:set var="orderSubTotal">${order.priceInfo.rawSubtotal}</c:set>
	<c:choose>
    <c:when test="${not empty orderSubtotal}">
    <c:set var="orderSubtotal"><c:if test="${orderSubtotal.matches('[0-9]+.[0-9]+')}"><fmt:formatNumber type="number" maxFractionDigits="2">${orderSubtotal}</fmt:formatNumber></c:if></c:set>
	</c:when>
	<c:otherwise>
	<c:set var="orderSubtotal" value="0.00"/>
	</c:otherwise>
	</c:choose>
 
<c:set var="singleQuote" value="'"/>
<c:set var="doubleQuote" value='"'/>
<c:set var="slashSingleQuote" value="\'"/>
<c:set var="slashDoubleQuote" value='\"'/>
<c:set var="comma" value=","/>
<c:set var="andSymbol" value="&"/>
<c:set var="hyphen" value="-"/>
<c:set var="spaceTrimer" value=""/>

<dsp:getvalueof var="customerFirstName" bean="Profile.firstName"/>
	<dsp:getvalueof var="customerLastName" bean="Profile.lastName"/>
	<c:set var="space" value=" "/>
	<c:choose>
	<c:when test="${empty customerFirstName && empty customerLastName}">
		<c:set var="customerName" value='${fn:substring(customerEmail, 0, fn:indexOf(customerEmail, "@"))}'/>
	</c:when>
	<c:when test="${not empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value="${customerFirstName}"/>
	</c:when>
	<c:when test="${empty customerFirstName && not empty customerLastName}">
		   <c:set var="customerName" value="${customerLastName}"/>
	</c:when>
	<c:otherwise>
			<c:set var="customerName" value="${customerFirstName}${space}${customerLastName}"/>
	</c:otherwise>
 </c:choose>
<dsp:getvalueof var="priceInfo" value="${order.priceInfo}" />
<dsp:getvalueof var="shippingCharge" value="${order.priceInfo.shipping}" />
	<%-- <dsp:getvalueof param="couponId" var="couponIdTealium"></dsp:getvalueof> --%>
	<c:forEach var="element" items="${priceInfo.adjustments}">
	<%-- <c:set var="promotionName" value="${promotionName}${element.pricingModel.displayname}|"/> --%>
</c:forEach>
<c:set var="currencyCode" value="${priceInfo.currencyCode}"/>
<c:choose>
<c:when test="${loginStatus eq 'true'}">
<c:set var="customerStatus" value="Guest"/>
</c:when>
<c:otherwise>
<c:set var="customerStatus" value="Registered"/>
<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
		<dsp:droplet name="IsEmpty">
			<dsp:param name="value" bean="Profile.shippingAddress.id" />
			<dsp:oparam name="false">
				<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="Profile.secondaryAddresses"/>
				<dsp:param name="elementName" value="address"/>
				<dsp:oparam name="output">
				<dsp:getvalueof param="address.id" var="addressId" />
				<c:if test="${addressId eq shippingAddressId}">

						 <dsp:getvalueof param="address.city" var="customerCity"/>
						 <dsp:getvalueof param="address.state" var="customerState"/>
						 <dsp:getvalueof param="address.postalCode" var="customerZip"/>
						 <dsp:getvalueof param="address.country" var="customerCountry"/>
				 </c:if>
				</dsp:oparam>
			   </dsp:droplet>
			</dsp:oparam>
</dsp:droplet>
</c:otherwise>
</c:choose>
<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
<c:choose>
<c:when test="${isSosVar eq 'true'}">
<c:set var="selectedStore" value="sos"/>
</c:when>
<c:otherwise>
<c:set var="selectedStore" value=""/>
</c:otherwise>
</c:choose>
<dsp:getvalueof param="items" var="items"/>

	<c:set var="index" value="0"/>
	<c:forEach var="element" items="${items}">

<dsp:setvalue param="productId" value="${element.productId}"/>
<dsp:setvalue param="selectSkuId" value="${element.skuId}"/>


	    <c:choose>
		 <c:when test="${items.size() gt '1' && index lt (items.size()-1)}">
		 <c:set var="separator" value=","/>
		 </c:when>
		 <c:otherwise>
		 <c:set var="separator" value=""/>
		 </c:otherwise>
		</c:choose>
		
		<dsp:droplet name="/com/tru/droplet/TRUCategoryPrimaryPathDroplet">
		<dsp:param name="skuId" value="${element.skuId}" />
		<dsp:oparam name="output">
		<dsp:getvalueof var="primaryPathCategoryVOList" param="primaryPathCategoryVOList" />
		</dsp:oparam>
	</dsp:droplet>
	
	<dsp:getvalueof var="productCategories" value="${primaryPathCategoryVOList[0].categoryName}"/>
	<dsp:getvalueof var="categoryName" value="${primaryPathCategoryVOList[0].categoryName}"/>
	<dsp:getvalueof var="productSubCategories" value=""/>
	<dsp:droplet name="ForEach">
		<dsp:param name="array" value="${primaryPathCategoryVOList}" />
		<dsp:param name="elementName" value="categoryList" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="count" param="count"/>
			<dsp:getvalueof var="size" param="size"/>
			<c:choose>
				<c:when test="${count eq 1}"> 
			</c:when>
			<c:when test="${count eq size}"> 
			</c:when>
			<c:otherwise>
			 <dsp:getvalueof var="subCategories" param="categoryList.categoryName"/>
				 <c:choose>
				 <c:when test="${count eq size-1}">
				 <dsp:getvalueof var="subCategories" value="'${subCategories}'"/>
				 </c:when>
				 <c:otherwise>
				 <dsp:getvalueof var="subCategories" value="'${subCategories}',"/>
				 </c:otherwise>
				</c:choose>
			 <dsp:getvalueof var="productSubCategories" value="${productSubCategories}${subCategories}"/>
			 <dsp:getvalueof var="pageSubCategories" value="${subCategories}"/>
			</c:otherwise>
			</c:choose>
			<dsp:getvalueof var="" param="categoryList.categoryName"/>
		</dsp:oparam>
	</dsp:droplet>
	
	<c:if test="${fn:contains(productCategories,singleQuote)}">
	     <c:set var="productCategories" value="${fn:replace(productCategories,singleQuote,slashSingleQuote)}"/>
		 </c:if>

<c:if test="${fn:contains(productCategories,doubleQuote)}">
	<c:set var="productCategories" value="${fn:replace(productCategories,doubleQuote,slashDoubleQuote)}" />
</c:if> 

<c:if test="${fn:contains(productCategories,hiphen)}">
    <c:set var="productCategories" value='${fn:replace(productCategories,hyphen,spaceTrimer)}'/>
 </c:if>

 
 <c:if test="${fn:contains(productCategories,andSymbol)}">
    <c:set var="productCategories" value='${fn:replace(productCategories,andSymbol,spaceTrimer)}'/>
 </c:if>

<c:if test="${fn:contains(productSubCategories,singleQuote)}">
	     <c:set var="productSubCategories" value="${fn:replace(productSubCategories,singleQuote,slashSingleQuote)}"/>
		 </c:if>

<c:if test="${fn:contains(productSubCategories,doubleQuote)}">
	<c:set var="productSubCategories" value="${fn:replace(productSubCategories,doubleQuote,slashDoubleQuote)}" />
</c:if>

<c:if test="${fn:contains(productSubCategories,hiphen)}">
    <c:set var="productSubCategories" value='${fn:replace(productSubCategories,hyphen,spaceTrimer)}'/>
 </c:if>
 

 
 <c:if test="${fn:contains(productSubCategories,andSymbol)}">
    <c:set var="productSubCategories" value='${fn:replace(productSubCategories,andSymbol,spaceTrimer)}'/>
 </c:if>
 
<dsp:droplet name="/com/tru/commerce/droplet/TRUProductCategoryInfoDroplet">
	 	<dsp:param name="productId" param="productId" />
	 	<dsp:param name="selectedSkuId" value="${element.skuId}" />	 	
		<dsp:oparam name="output">
			<dsp:getvalueof param="onlinePidSku" var="onlinePID"/>
		</dsp:oparam>
	 </dsp:droplet>
		
	 
		<dsp:droplet name="/com/tru/commerce/droplet/TRUProductInfoLookupDroplet">
			<dsp:param name="productId" param="productId"/>
			<dsp:param name="siteId" bean="/atg/multisite/Site.id"/>
			<dsp:oparam name="output">
	         <dsp:getvalueof param="productInfo.defaultSKU.brandName" var="brandName"/>
	         <dsp:getvalueof param="productInfo.defaultSKU.collectionNames" var="collectionNames"/>
	         <dsp:getvalueof param="productInfo.defaultSKU.relatedProducts" var="relatedProducts"/>
            <c:forEach var="collections" items="${collectionNames}">
		    <c:set var="collectionName" value="${collections.value}"/>
		    </c:forEach>
	         <c:choose>
			 <c:when test="${fn:contains(brandName,singleQuote)}">
		     <c:set var="brandName" value='${fn:replace(brandName,singleQuote,slashSingleQuote)}'/>
			 </c:when>
			 <c:when test="${fn:contains(brandName,doubleQuote)}">
		     <c:set var="brandName" value='${fn:replace(brandName,doubleQuote,slashDoubleQuote)}'/>
			 </c:when>
			 <c:otherwise>
			 <c:set var="brandName" value="${brandName}"/>
			 </c:otherwise>
		 	 </c:choose>
		 	 
	          <c:set var="cartXsellProduct" value="${cartXsellProduct}'${relatedProducts}'${separator}"/>

			 <c:set var="productBrands" value="${productBrands}'${brandName}'${separator}"/>
	         <c:if test="${not empty collectionName && not empty onlinePID}">
	         <c:set var="productCollections" value="${productCollections}'${onlinePID}: ${collectionName}'${separator}"/>
	         </c:if>
          </dsp:oparam>
	  </dsp:droplet>

	    <c:set var="productCategory" value="${productCategory}'${productCategories}'${separator}"/>
	    <c:set var="productSubcategory" value="${productSubcategory}'${productSubCategories}'${separator}"/>
	    <c:set var="productDisplayName" value="${element.displayName}"/>
	     <c:choose>
		 <c:when test="${fn:contains(productDisplayName,singleQuote)}">
	     <c:set var="productNewName" value='${fn:replace(productDisplayName,singleQuote,slashSingleQuote)}'/>
		 <c:set var="productName" value="${productName}'${productNewName}'${separator}"/>
		 </c:when>
		 <c:when test="${fn:contains(productDisplayName,doubleQuote)}">
	     <c:set var="productNewName" value='${fn:replace(productDisplayName,doubleQuote,slashDoubleQuote)}'/>
		 <c:set var="productName" value="${productName}'${productNewName}'${separator}"/>
		 </c:when>
		 <c:otherwise>
		 <c:set var="productName" value="${productName}'${productDisplayName}'${separator}"/>
		 </c:otherwise>
		</c:choose>


		 <c:set var="productID" value="${productID}${onlinePID}${separator}"/>
		<c:set var="productImage" value="${productImage}'${element.productImage}'${separator}"/>
		<c:set var="productDiscount" value="${productDiscount}'${element.savedAmount}'${separator}"/>
		<c:set var="productUnitPrice" value="${productUnitPrice}'${element.salePrice}'${separator}"/>
		<c:set var="productListPrice" value="${productListPrice}'${element.price}'${separator}"/>
		<c:set var="itemQuantity" value="${itemQuantity}'${element.quantity}'${separator}"/>
		<c:set var="productSKUs" value="${productSKUs}'${element.skuId}'${separator}"/>
		<c:set var="index" value="${index+1}"/>

	</c:forEach>
	<input type="hidden" id="omniProductID" value="${productID}"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartEconomyShipping" var="cartEconomyShipping"/>
	<c:choose>
	<c:when test="${shippingCharge == 0.0}">
		<c:set var="economyShipping" value="${cartEconomyShipping}"/>
 	</c:when>
 	<c:otherwise>
 	</c:otherwise>
	</c:choose>
<c:set var="internalCampaignPage" value=""/>
<%-- <c:set var="productSubcategory" value=""/> --%>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartPageName" var="pageName" />
<dsp:getvalueof bean="TRUTealiumConfiguration.cartPageType" var="pageType" />
<dsp:getvalueof bean="TRUTealiumConfiguration.browserID" var="browserID"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartOasTaxonomy" var="oasTaxonomy"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartSiteSection" var="siteSection"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartOasSize" var="oasSize"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartOasBreadcrumb" var="oasBreadcrumb"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.deviceType" var="deviceType"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartFinderSortGroup" var="finderSortGroup"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartPageCategory" var="pageCategory"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartPageSubcategory" var="pageSubcategory"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartProductFindingMethod" var="productFindingMethod"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartImageIcon" var="imageIcon"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartOrsoCode" var="orsoCode"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartOutOfStockBySku" var="outOfStockBySku"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartProductPathing" var="productPathing"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartRegistryOrigin" var="registryOrigin"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartIspuSource" var="ispuSource"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartStsRevenue" var="cartStsRevenue"/>
<dsp:getvalueof var="siteCode" value="TRU" />
<dsp:getvalueof var="baseBreadcrumb" value="tru" />
<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
	<c:if test="${siteId eq babySiteCode}">
		<dsp:getvalueof var="siteCode" value="BRU" />
		<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
		<dsp:getvalueof var="baseBreadcrumb" value="bru" />
 	</c:if>

<%-- <c:set var="orderShippingDiscount" value="0.00"/>
<dsp:droplet name="DisplayOrderDetailsDroplet">
<dsp:param name="order" bean="ShoppingCart.current"/>
<dsp:param name="profile" bean="Profile" />
<dsp:param name="page" value="Cart" />
<dsp:oparam name="output">
	<dsp:droplet name="ForEach">
		<dsp:param name="array" param="shippingDestinationsVO" />
		<dsp:param name="elementName" value="shippingDestinationVO" />
		<dsp:oparam name="output">
				<dsp:droplet name="ForEach">
					<dsp:param name="array"	param="shippingDestinationVO.shipmentVOMap" />
					<dsp:param name="elementName" value="shipmentVO" />
					<dsp:oparam name="output">
                     <dsp:getvalueof param="shipmentVO.shippingGroup" var="shippingGroup"/>
                        <c:if test="${shippingGroup ne null}">
							<c:choose>
								<c:when test="${shippingGroup.shippingGroupClassType eq 'hardgoodShippingGroup'}">
									<dsp:droplet name="/atg/dynamo/droplet/ForEach">
									      <dsp:param name="array" value="${shippingGroup.priceInfo.adjustments}"/>
									      <dsp:oparam name="output">
									        <dsp:tomap var="shippingAdjustment" param="element"/>
									        
									        	<c:if test="${shippingAdjustment.adjustmentDescription eq 'Shipping Discount'}">
									        		<c:set var="shippingDiscount" value="${shippingAdjustment.totalAdjustment}"/>
													<c:set var="orderShippingDiscount" value="${orderShippingDiscount+(-shippingDiscount)}"/>
									        	</c:if>
									       </dsp:oparam>
									     </dsp:droplet>
								</c:when>
							</c:choose>
						</c:if>
						</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
				</dsp:droplet>
			</dsp:oparam>
		</dsp:droplet> --%>

<dsp:droplet name="/com/tru/commerce/locations/TRUGetCookieDroplet">
	<dsp:param name="cookieName" value="favStore" />
	<dsp:oparam name="output">
		<dsp:getvalueof var="cookieValue" param="cookieValue" />
		<dsp:getvalueof var="locationIdFromCookie" param="locationId" />
	</dsp:oparam>
	<dsp:oparam name="empty">
	</dsp:oparam>
</dsp:droplet>

<c:set var="session_id" value="${cookie.sessionID.value}"/>
<c:set var="visitor_id" value="${cookie.visitorID.value}"/>

<dsp:droplet name="/atg/dynamo/droplet/ForEach">
  <dsp:param name="array" value="${order.commerceItems}" />
  <dsp:oparam name="output">
   <dsp:tomap var="commerceItem" param="element"/>
     <dsp:droplet name="/atg/dynamo/droplet/ForEach">
      <dsp:param name="array" value="${commerceItem.priceInfo.adjustments}" />
      <dsp:oparam name="output">
        <dsp:tomap var="itemAdjustment" param="element"/>
        	<%-- <dsp:droplet name="IsNull">
        	 <dsp:param name="value" value="${itemAdjustment.pricingModel}" />
        	  <dsp:oparam name="false">
        	<dsp:getvalueof var="promotionName" value="${promotionName}${fn:escapeXml(itemAdjustment.pricingModel.displayName)},"/>
        	<c:set var="orderCouponType" value="${orderCouponType}'Item Level',"/>
        	</dsp:oparam>
        </dsp:droplet> --%>
        </dsp:oparam>
       </dsp:droplet>
      </dsp:oparam>
  </dsp:droplet>
  
<dsp:droplet name="/atg/dynamo/droplet/ForEach">
   <dsp:param name="array" value="${order.priceInfo.adjustments}" />
   <dsp:oparam name="output">
     <dsp:tomap var="orderAdjustment" param="element"/>
     	<%-- <dsp:droplet name="IsNull">
        	 <dsp:param name="value" value="${orderAdjustment.pricingModel}" />
        	  <dsp:oparam name="false">
     			<dsp:getvalueof var="promotionName" value="${promotionName}${fn:escapeXml(orderAdjustment.pricingModel.displayName)},"/>
     			<c:set var="orderCouponType" value="${orderCouponType}'Order Level',"/>
     		  </dsp:oparam>
     	</dsp:droplet> --%>
   </dsp:oparam>
 </dsp:droplet>
 
 <c:forEach items="${order.shippingGroups}" var="shippingGroup">
     <%-- Iterate adjustments in each shipping group --%>
     <dsp:droplet name="/atg/dynamo/droplet/ForEach">
       <dsp:param name="array" value="${shippingGroup.priceInfo.adjustments}"/>
       <dsp:oparam name="output">
         <dsp:tomap var="shippingAdjustment" param="element"/>
         	<%-- <dsp:droplet name="IsNull">
        	 <dsp:param name="value" value="${shippingAdjustment.pricingModel}" />
        	  <dsp:oparam name="false">
         		<dsp:getvalueof var="promotionName" value="${promotionName}${fn:escapeXml(shippingAdjustment.pricingModel.displayName)},"/>
         		<c:set var="orderCouponType" value="${orderCouponType}'Shipping Level',"/> 
         	  </dsp:oparam>
         	</dsp:droplet>  --%>
         	<c:if test="${shippingGroup.shippingGroupClassType eq 'hardgoodShippingGroup'}">
         	  <c:if test="${shippingAdjustment.adjustmentDescription eq 'Shipping Discount'}">
         	  	<c:set var="shippingDiscount" value="${shippingAdjustment.totalAdjustment}"/>
				<c:set var="orderShippingDiscount" value="${orderShippingDiscount+(-shippingDiscount)}"/>
         	  </c:if>
         	</c:if>
       </dsp:oparam>
     </dsp:droplet>
   </c:forEach>

<script type='text/javascript'>


var utag_data = {
	    customer_status:"${customerStatus}",
	    customer_city:"${customerCity}",
	    customer_country:"${customerCountry}",
	    customer_email:"${customerEmail}",
	    customer_id:"${customerID}",
	    customer_type:"${customerStatus}",
	    customer_state:"${customerState}",
	    customer_zip:"${customerZip}",
	    customer_dob:"${customerDOB}",
	    page_name:"${siteCode}: ${pageName}",
	    page_type:"${pageType}",
	    product_brand:[${productBrands}],
	    product_category:[${productCategory}],
	    product_id:[${productID}],
	    product_name:[${productName}],
	    product_quantity:[${itemQuantity}],
	    product_unit_price:[${productUnitPrice}],
	    product_list_price:[${productListPrice}],
	    product_sku:[${productSKUs}],
	    product_subcategory:[${productSubcategory}],
	    product_finding_method:"${productFindingMethod}",
	    product_image_url:[${productImage}],
    	product_store:["${siteCode}"],
	    browser_id:"${pageContext.session.id}",
	    internal_campaign_page:"${internalCampaignPage}",
	    oas_breadcrumb : "${baseBreadcrumb}/shoppingcart",
	    oas_taxonomy : "level1=shoppingcart&level2=null&level3=null&level4=null&level5=null&level6=null",
		oas_sizes : [ ['TopRight', [777,34]],['Bottom1', [728,90]] ],
	    orso_code:"${orsoCode}",
	    product_collection:[${productCollections}],
	    xsell_product:"",
	    ispu_source:"${ispuSource}",
	    device_type:"${deviceType}",
	    order_subtotal:"${orderSubTotal}",
	    order_currency:"${currencyCode}",
	    sts_revenue:"${cartStsRevenue}",
	    partner_name:"${partnerName}",
	    customer_name:"${customerName}",
	    event_type:"",
	    selected_store:"${selectedStore}",
	    order_shipping_discount:"${orderShippingDiscount}",
	    shipping_discount:"${shippingDiscount}",
	    store_locator:"${locationIdFromCookie}",
	    session_id : "${session_id}",
		visitor_id : "${visitor_id}",
		tru_or_bru : "${siteCode}"
	};

</script>
  <%-- <c:if test="${not empty productDiscount}">
	    <script>utag_data.product_discount=[${productDiscount}];</script>
	    </c:if> --%>

<script type="text/javascript">
		    (function(a,b,c,d){
		    a='${tealiumURL}';
		    b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
		    a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
		    })();
</script>
</dsp:page>