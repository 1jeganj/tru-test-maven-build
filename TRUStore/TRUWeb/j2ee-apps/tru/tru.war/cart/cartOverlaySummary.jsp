<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:getvalueof var="showProgressBar" bean="ShoppingCart.freeShippingPromotion"/>
	<dsp:getvalueof var="spendAmount" bean="ShoppingCart.shippingPromotionOrderLimit"/>
	<dsp:getvalueof var="amountToQualify" bean="ShoppingCart.amountToQualifyPromotion"/>
	<dsp:getvalueof var="excludedSkusAmount" bean="ShoppingCart.excludedShippingPromotionSkusAmount"/>
	<dsp:getvalueof var="order" bean="ShoppingCart.current"/>
	<dsp:importbean bean="/com/tru/commerce/cart/droplet/OrderSummaryDetailsDroplet" />
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:getvalueof var="siteStatus" bean="Profile.siteStatus"/>
	<dsp:droplet name="OrderSummaryDetailsDroplet">
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:oparam name="output">
		<input type="hidden" id="promotionValue" value="${spendAmount}"/>
		<input type="hidden" id="orderValue" value="${order.priceInfo.amount-excludedSkusAmount}"/>
			<div class="margin-buffer">
				<div class="shopping-cart-overlay-summary">
					<div class="row">
						<div class="col-xs-12">
							<p>
								<span class="cart-summary">
									<fmt:message key="shoppingcart.overlay.summary" /></span><span class="num-items">
									<dsp:getvalueof var="commerceItemCount" bean="ShoppingCart.current.totalItemCount"/>
									<c:choose>
										<c:when test="${commerceItemCount > 1 }">
											<span> 
												<fmt:message key="shoppingcart.overlay.summary.items.text" >
													<fmt:param value="${commerceItemCount}"></fmt:param>
												</fmt:message>
											</span>
										</c:when>
										<c:otherwise>
											<span> 
												<fmt:message key="shoppingcart.overlay.summary.item.text" >
													<fmt:param value="${commerceItemCount}"></fmt:param>
												</fmt:message>
											</span>
										</c:otherwise>
									</c:choose>
								</span>
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p>
								<span class="line-item">
									<fmt:message key="shoppingcart.subtotal" />
								</span>
								<span class="line-item-value">
									<dsp:valueof format="currency" value="${order.priceInfo.totalListPrice}" locale="en_US" converter="currency"/>
								</span>
							</p>
						</div>
						<%-- <div class="col-xs-5">
							<p>
								<span class="line-item-value">
									<dsp:valueof format="currency" value="${order.priceInfo.totalListPrice}" locale="en_US" converter="currency"/>
								</span>
							</p>
						</div> --%>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<br>
						</div>
					</div>
					<c:if test="${order.priceInfo.totalItemLevelSavings gt 0.0}">
						<div class="row">
							<div class="col-xs-12">
								<p>
									<span class="line-item">
										<fmt:message key="shoppingcart.savings" />
									</span>
									<span class="line-item-value red-text">
										<dsp:valueof format="currency" value="${order.priceInfo.totalItemLevelSavings}" locale="en_US" converter="currency"/>
									</span>
								</p>
								<p>
									<span class="line-item-value red-text small">
										<fmt:message key="shoppingcart.overlay.you.save" />&nbsp;<dsp:valueof value="${order.priceInfo.totalItemLevelSavingsPercentage}" converter="number" format="##"/>%
									</span>
								</p>
							</div>
							<%-- <div class="col-xs-5">
								<p>
									<span class="line-item-value red-text">
										<dsp:valueof format="currency" value="${order.priceInfo.totalItemLevelSavings}" locale="en_US" converter="currency"/>
									</span>
								</p>
								<p>
									<span class="line-item-value red-text small">
										<fmt:message key="shoppingcart.overlay.you.save" />&nbsp;<dsp:valueof value="${order.priceInfo.totalItemLevelSavingsPercentage}" converter="number" format="##"/>%
									</span>
								</p>
							</div> --%>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<br>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<p>
									<span class="line-item">
										<fmt:message key="shoppingcart.new.subtotal" />
									</span>
									<span class="line-item-value">
										<dsp:valueof format="currency" value="${order.priceInfo.rawSubtotal}" locale="en_US" converter="currency"/>
									</span>
								</p>
							</div>
							<%-- <div class="col-xs-5">
								<p>
									<span class="line-item-value">
										<dsp:valueof format="currency" value="${order.priceInfo.rawSubtotal}" locale="en_US" converter="currency"/>
									</span>
								</p>
							</div> --%>
						</div>
					</c:if>
				</div>
			</div>
			<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id" />
			<c:choose>
				<c:when test="${siteName eq 'ToysRUs'}">
					<dsp:getvalueof var="contextRoot" value="TRU" />
				</c:when>
				<c:when test="${siteName eq 'BabyRUs'}">
					<dsp:getvalueof var="contextRoot" value="BRU" />
				</c:when>
			</c:choose>
			<div class="margin-buffer">
				<dsp:a page="${contextPath}cart/shoppingCart.jsp?ab=${contextRoot}_Header:Utility1:continue-shopping:Shopping-Cart">
					<div class="shopping-cart-overlay-checkout-button">
						<button>
							<fmt:message key="shoppingcart.view.checkout" />
						</button>
					</div>
				</dsp:a>
			</div>
			<div class="margin-buffer">
				<c:if test="${(showProgressBar) and (siteStatus != 'sos')}">
					<div class="shopping-cart-overlay-shipping-bar">
						<c:choose>
							<c:when test="${amountToQualify < 0}">
								<div class="shopping-cart-overlay-shipping-bar-text">
									<fmt:message key="shoppingcart.free.shipping" />
								</div>
							</c:when>
							<c:otherwise>
								<p><fmt:message key="shoppingcart.free.shipping.youare" />&nbsp;<dsp:valueof value="${amountToQualify}" number="0.00" locale="en_US" converter="currency" format="currency"/>&nbsp;<fmt:message key="shoppingcart.away.from.free.shipping" /></p>
							</c:otherwise>
						</c:choose>
						<div>
							<table class="promoTable">
                				<tr>
                        			<td width="5%"><span class="shopping-cart-overlay-shipping-bar-amount">$0</span></td>
                            		<td><div class="summary-block-free-shipping-bar pdp-progress-bar" value=""></div></td>
                            		<td width="10%"><span class="shopping-cart-overlay-shipping-bar-amount">$<fmt:formatNumber type="number" groupingUsed="false" value="${spendAmount}" /></span></td>
                				</tr>
							</table>
						</div>
					</div>
				</c:if>
			</div>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>