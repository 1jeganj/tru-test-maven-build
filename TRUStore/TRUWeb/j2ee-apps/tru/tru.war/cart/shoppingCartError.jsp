<dsp:page>
    <fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
    <dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
    <dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
    <dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
    <dsp:importbean bean="/atg/commerce/ShoppingCart" />
    <dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
    <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
    <dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath"/>
    <dsp:droplet name="Switch">
        <dsp:param bean="CartModifierFormHandler.formError" name="value"/>
        <dsp:oparam name="true">
            <dsp:droplet name="ErrorMessageForEach">
                <dsp:param name="exceptions" bean="CartModifierFormHandler.formExceptions"/>
                <dsp:oparam name="outputStart">
                    <div class="shopping-cart-error-state">
                    <div class="shopping-cart-error-state-header">
                    <span class="checkout-flow-sprite checkout-flow-error-state-icon"></span>
                    <div class="shopping-cart-error-state-header-text">
                    <fmt:message key="shoppingcart.oops.issue"/>
                    <div class="shopping-cart-error-state-subheader">
                </dsp:oparam>
                <dsp:oparam name="output">
                    <dsp:getvalueof param="errorCode" var="errorCode"/>
                    <c:if test="${errorCode ne 'inline_msg'}">
                        <dsp:valueof param="message" valueishtml="true"/>
                    </c:if>
                </dsp:oparam>
                <dsp:oparam name="outputEnd">
                    </div>
                    </div>
                    </div>
                    </div>
                    <dsp:setvalue bean="ShoppingCart.current.repriceRequiredOnCart" value="true"/>
                    <dsp:droplet name="/atg/commerce/order/purchase/RepriceOrderDroplet">
                        <dsp:param value="ORDER_TOTAL" name="pricingOp"/>
                    </dsp:droplet>
                </dsp:oparam>
            </dsp:droplet>
        </dsp:oparam>
    </dsp:droplet>
    <dsp:droplet name="Switch">
        <dsp:param bean="CommitOrderFormHandler.formError" name="value"/>
        <dsp:oparam name="true">
            <dsp:droplet name="ErrorMessageForEach">
                <dsp:param name="exceptions" bean="CommitOrderFormHandler.formExceptions"/>
                <dsp:oparam name="outputStart">
                    <div class="shopping-cart-error-state-header">
                    <div class="col-md-2">
                        <span class="checkout-flow-sprite checkout-flow-error-state-icon"></span>
                    </div>
                    <div class="shopping-cart-error-state-header-text col-md-10">
                    <fmt:message key="shoppingcart.oops.issue"/>
                    <div class="shopping-cart-error-state-subheader">
                </dsp:oparam>
                <dsp:oparam name="output">
                    <dsp:valueof param="message" valueishtml="true"/>
                </dsp:oparam>
                <dsp:oparam name="outputEnd">
                    </div>
                    </div>
                    </div>
                    <dsp:setvalue bean="ShoppingCart.current.repriceRequiredOnCart" value="true"/>
                    <dsp:droplet name="/atg/commerce/order/purchase/RepriceOrderDroplet">
                        <dsp:param value="ORDER_TOTAL" name="pricingOp"/>
                    </dsp:droplet>
                </dsp:oparam>
            </dsp:droplet>
        </dsp:oparam>
    </dsp:droplet>
</dsp:page>
