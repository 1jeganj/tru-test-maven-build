<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/dynamo/Configuration" var="Configuration" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:getvalueof var="host" value="${originatingRequest.host}" />
	<dsp:getvalueof var="serverName" value="${originatingRequest.serverName}" />
	<c:set var="isSosVar" value="${cookie.isSOS.value}" />
	<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="orderId" bean="/atg/commerce/ShoppingCart.current.id" />
	<input type="hidden" id="orderId" value="${orderId}" />
	<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
	<dsp:droplet name="GetSiteTypeDroplet">
		<dsp:param name="siteId" value="${site}" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="site" param="site" />
			<c:set var="site" value="${site}" scope="request" />
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
		<dsp:oparam name="output">
			<dsp:getvalueof var="scheme" param="scheme"></dsp:getvalueof>
		</dsp:oparam>
	</dsp:droplet>
	<c:choose>
		<c:when test="${fn:containsIgnoreCase(host, ':')}">
			<dsp:getvalueof var="httpURL" value="${scheme}${serverName}:${Configuration.httpPort}" />
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="httpURL" value="${scheme}${host}" />
		</c:otherwise>
	</c:choose>
	<div class="row">
		<div class="col-md-12 cart-products">
			<dsp:droplet name="Switch">
				<dsp:param name="value" bean="/atg/multisite/Site.enableCharityDonation" />
				<dsp:oparam name="true">
					<dsp:include page="charityDonation.jsp"></dsp:include>
				</dsp:oparam>
			</dsp:droplet>
			<div class="shopping-cart-empty">
				<div class="shopping-cart-empty-header">
					<fmt:message key="shoppingcart.cart.epmty" />
				</div>
				<div class="shopping-cart-empty-subheader">
					<dsp:droplet name="/atg/dynamo/droplet/Switch">
						<dsp:param bean="Profile.transient" name="value" />
						<dsp:oparam name="true">
							<c:if test="${!isSosVar || !(site eq 'sos')}">
								<a href="" data-toggle="modal" data-target="#signInModal">Sign In</a> to see what's saved in your cart, or
							</c:if>
						</dsp:oparam>
					</dsp:droplet>
					<a href="javascript:void(0);" onclick="handlePreviousPage('${httpURL}${contextPath}');">continue shopping</a>.
				</div>
				<dsp:droplet name="/atg/targeting/TargetingForEach">
					<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/ShoppingCart/EmptyCartTargeter" />
					<dsp:oparam name="output">
						<dsp:valueof param="element.data" valueishtml="true" />
					</dsp:oparam>
				</dsp:droplet>
			</div>
		</div>
	</div>
</dsp:page>