<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	
	<dsp:getvalueof var="pageName" param="pageName" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:getvalueof var="displayStrikeThrough" param="item.displayStrikeThrough" />
	<dsp:getvalueof var="savedAmount" param="item.savedAmount" />
	<dsp:getvalueof var="relationshipItemId" param="item.id" />
	<dsp:getvalueof var="productId" param="item.productId"/>
	<dsp:getvalueof var="index" param="index" />
	<dsp:getvalueof param="existingRelationShipId" var="existingRelationShipId"/>
	<dsp:getvalueof param="inlineMessage" var="inlineMessage"/>
							
	<c:if test="${pageName ne 'Shipping' and pageName ne 'store-pickup'}">	
		<c:if test="${pageName ne 'Confirm' and pageName ne 'Review'}"> 
			<c:choose>
				<c:when test="${displayStrikeThrough}">
					<div class="product-description-price inline">
						<span class="crossed-out-price price-save"><dsp:valueof param="item.price" converter="currency" /></span>
						<dsp:valueof param="item.salePrice" converter="currency" />
						<p>
							<span class="price-save save-text">&#32;<fmt:message key="shoppingcart.you.save" />&#32; 
							<dsp:valueof format="currency" value="${savedAmount}" locale="en_US" converter="currency"/>&nbsp;
							<fmt:message key="shoppingcart.left.bracket" /><dsp:valueof param="item.savedPercentage" converter="number" format="##"/><fmt:message key="shoppingcart.percentage" /><fmt:message key="shoppingcart.right.bracket" />							
						</p>
						</span>
					</div>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${savedAmount > 0.0}">
						<div class="product-description-price inline">
						<span class="crossed-out-price price-save"><dsp:valueof param="item.price" converter="currency" /></span>
						<dsp:valueof param="item.salePrice" converter="currency" />
						<p>
							<span class="price-save save-text">&#32;<fmt:message key="shoppingcart.you.save" />&#32; 
							<dsp:valueof format="currency" value="${savedAmount}" locale="en_US" converter="currency"/>&nbsp;
							<fmt:message key="shoppingcart.left.bracket" /><dsp:valueof param="item.savedPercentage" converter="number" format="##"/><fmt:message key="shoppingcart.percentage" /><fmt:message key="shoppingcart.right.bracket" />							
						</p>
					</div>
						</c:when>
						<c:otherwise>
							<div class="product-description-price"><dsp:valueof param="item.salePrice" converter="currency" /></div>
						</c:otherwise>
					</c:choose>
				</c:otherwise>
			</c:choose>
		</c:if>
	<%-- Start for TUW-56682 --%>
	<c:if test="${(pageName ne 'Review') and (pageName ne 'Confirm')}">
		<dsp:droplet name="IsEmpty">
			<dsp:param name="value" param="item.skuId" />
			<dsp:oparam name="false">
				<div class="product-description-item-number">
					<fmt:message key="shoppingcart.item" />&#32;
					<dsp:valueof param="item.skuId" />
				</div>
			</dsp:oparam>
		</dsp:droplet>
	</c:if>
	<%-- End for TUW-56682 --%>
	<dsp:droplet name="IsEmpty">
		<dsp:param name="value" param="item.color" />
		<dsp:oparam name="false">
			<div class="product-description-color">
				<fmt:message key="shoppingcart.item.color" />&#32;
				<dsp:valueof param="item.color" />
			</div>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="IsEmpty">
		<dsp:param name="value" param="item.size" />
		<dsp:oparam name="false">
			<div class="product-description-size">
				<fmt:message key="shoppingcart.item.size" />&#32;
				<dsp:valueof param="item.size" />
			</div>
		</dsp:oparam>
	</dsp:droplet>
	<div class="">
	<c:if test="${(pageName eq 'Review') or (pageName eq 'Confirm')}">
		<dsp:getvalueof var="quantity" param="item.quantity" />
		<dsp:droplet name="IsEmpty">
			<dsp:param name="value" param="item.quantity" />
			<dsp:oparam name="false">
				<div class="qty inline">
					<fmt:message key="checkout.review.itemQuantity" />:  ${quantity}
				</div>
			</dsp:oparam>
		</dsp:droplet>
	</c:if>
	<c:if test="${pageName eq 'Review'}">
			<c:choose>
				<c:when test="${relationshipItemId eq existingRelationShipId}">
				<%-- <div class="qty">
					<dsp:valueof param="inlineMessage" />
				</div> --%>
					<dsp:droplet name="IsEmpty">
						<dsp:param name="value" param="inlineMessage" />
						<dsp:oparam name="false">
							<span class="cart-error-message inline" id="cart-error-message_${relationshipItemId}">
								<dsp:valueof param="inlineMessage" />
							</span>
						</dsp:oparam>						
					</dsp:droplet>
				</c:when>
			</c:choose>
	</c:if>
	</div>
</c:if>	
<c:if test="${pageName eq 'Shipping'}">
	<dsp:droplet name="IsEmpty">
		<dsp:param name="value" param="item.color" />
		<dsp:oparam name="false">
			<div class="product-description-color">
				<fmt:message key="shoppingcart.item.color" />&#32;
				<dsp:valueof param="item.color" />
			</div>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="IsEmpty">
		<dsp:param name="value" param="item.size" />
		<dsp:oparam name="false">
			<div class="product-description-size">
				<fmt:message key="shoppingcart.item.size" />&#32;
				<dsp:valueof param="item.size" />
			</div>
		</dsp:oparam>
	</dsp:droplet>
	<div class="qty"><fmt:message key="checkout.review.itemQuantity" />:&nbsp;1</div>
</c:if>
<c:if test="${(pageName eq 'store-pickup')}">
	<dsp:droplet name="IsEmpty">
		<dsp:param name="value" param="item.color" />
		<dsp:oparam name="false">
			<div class="product-description-color">
				<fmt:message key="shoppingcart.item.color" />&#32;
				<dsp:valueof param="item.color" />
			</div>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="IsEmpty">
		<dsp:param name="value" param="item.size" />
		<dsp:oparam name="false">
			<div class="product-description-size">
				<fmt:message key="shoppingcart.item.size" />&#32;
				<dsp:valueof param="item.size" />
			</div>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="quantity" param="item.quantity" />
		<dsp:droplet name="IsEmpty">
			<dsp:param name="value" param="item.quantity" />
			<dsp:oparam name="false">
				<div class="qty">
					<fmt:message key="checkout.review.itemQuantity" />:&nbsp;${quantity}
				</div>
			</dsp:oparam>
		</dsp:droplet>
</c:if>
</dsp:page>