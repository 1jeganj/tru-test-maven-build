<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<div class="modal-dialog modal-lg">
            <div class="modal-content sharp-border">
                <a href="javascript:void(0);" id="removeModelClose" data-dismiss="modal"><span class="clickable"><img src="${TRUImagePath}images/tru_dots/close.png" alt="Close"></span></a>
                <div>
                    <h2><fmt:message key="shoppingcart.sure.remove.ietm" /></h2>
                    <a class="text-color" href="javascript:void(0);" onclick="$('#removeModelClose').click();"><fmt:message key="shoppingcart.back.review" /></a>
                    <input type="hidden" value="" id="removeItemId">
                    <input type="hidden" value="" id="removeDonationItemId">
                    <a href="javascript:void(0);" onclick="removeItemFromCart()"><fmt:message key="shoppingcart.remove.item" /></a>
                </div>
            </div>
        </div>
</dsp:page> 