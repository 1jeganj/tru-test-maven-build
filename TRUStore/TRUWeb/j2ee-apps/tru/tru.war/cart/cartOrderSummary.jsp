<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/com/tru/commerce/cart/droplet/OrderSummaryDetailsDroplet" />
	<dsp:importbean bean="/com/tru/commerce/droplet/TRUCheckPaypalExpressCheckoutDroplet" />
	<dsp:importbean bean="/com/tru/common/TRUUserSession" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/multisite/Site" />
	<%-- <dsp:importbean bean="/com/tru/radial/payment/paypal/TRUPayPalConfiguration" /> --%>
		<dsp:getvalueof var="showProgressBar" bean="ShoppingCart.freeShippingPromotion" />
		<dsp:getvalueof var="spendAmount" bean="ShoppingCart.shippingPromotionOrderLimit" />
		<dsp:getvalueof var="amountToQualify" bean="ShoppingCart.amountToQualifyPromotion" />
		<dsp:getvalueof var="excludedSkusAmount" bean="ShoppingCart.excludedShippingPromotionSkusAmount" />
		<dsp:importbean bean="/atg/userprofiling/Profile" />
		<dsp:getvalueof var="siteStatus" bean="Profile.siteStatus" />
		<dsp:droplet name="OrderSummaryDetailsDroplet">
			<dsp:param name="order" bean="ShoppingCart.current" />
			<dsp:param name="profile" bean="/atg/userprofiling/Profile" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="commerceItemCount" param="itemCount" />
				<dsp:getvalueof var="showPaypalCheckOutFlag" param="showPaypalCheckOutFlag" />
				<dsp:getvalueof var="balanceAmount" param="balanceAmount" />
				<dsp:getvalueof var="maxCouponLimit" param="maxCouponLimit" />
			</dsp:oparam>
			<dsp:oparam name="empty">
				<dsp:getvalueof var="commerceItemCount" value="0" />
			</dsp:oparam>
		</dsp:droplet>
		<dsp:getvalueof var="order" bean="ShoppingCart.current" />
		<div id="order-summary-block" class="col-md-4 pull-right">
			<div class="shopping-cart-summary-block">
				<div class="summary-block-border">
					<div class="summary-block-padded-content" tabindex="0">
						<div class="order-summary">
							<fmt:message key="shoppingcart.order.summary" />
						</div>
						<div class="subtotal">
							<c:choose>
								<c:when test="${commerceItemCount > 1 }">
									<span class="order-summary-item-label"> 
									<fmt:message key="shoppingcart.subtotal.items" >
										<fmt:param value="${commerceItemCount}"></fmt:param>
									</fmt:message>
								</span>
								</c:when>
								<c:otherwise>
									<span class="order-summary-item-label"> 
									<fmt:message key="shoppingcart.subtotal.item" >
										<fmt:param value="${commerceItemCount}"></fmt:param>
									</fmt:message>
								</span>
								</c:otherwise>
							</c:choose>
							<span class="order-summary-item-value">
							<dsp:valueof format="currency" value="${order.priceInfo.rawSubtotal}" locale="en_US" converter="currency"/>
						</span>
						</div>

						<dsp:getvalueof var="hasGiftWrapItems" vartype="java.lang.boolean" value="${order.GWItemInOrder}" />
						<dsp:getvalueof value="${order.priceInfo.giftWrapPrice}" var="giftWrapPrice" />
						<c:if test="${hasGiftWrapItems}">
							<div class="summary-gift-wrap">
								<span><fmt:message key="checkout.priceSummary.giftWrap" /></span>
								<span class="summary-price pull-right">
	                        	<c:choose>
	                       		<c:when test="${giftWrapPrice <= 0.0}">
	                       			<fmt:message key="shoppingcart.free" />
	                       		</c:when>
	                       		<c:otherwise>
	                       			<dsp:valueof format="currency"  value="${giftWrapPrice}" locale="en_US" converter="currency"/>
	                       		</c:otherwise>
	                       		</c:choose>
	                        </span>
							</div>
						</c:if>
						<fmt:formatNumber var="promotionSaving" value="${order.priceInfo.discountAmount}" pattern=".##" />
						<c:if test="${promotionSaving > 0.0 }">
							<div class="summary-promotions">
								<span class="order-summary-item-label"><fmt:message key="checkout.priceSummary.promotionSavings" /></span>
								<span class="summary-price pull-right order-summary-item-value">
							<fmt:setLocale value="en_US"/> 
							-<fmt:formatNumber type="currency" value="${promotionSaving}" />
							</span>
							</div>
						</c:if>
						<c:if test="${order.shipToHomeItemsCount > 0}">
							<div class="estimatedShipping">
								<span class="order-summary-item-label"> <fmt:message key="shoppingcart.estimated.shipping" />
						</span> <span class="order-summary-item-value"> <dsp:getvalueof value="${order.priceInfo.shipping}" var="estimatedShipping"></dsp:getvalueof> <c:choose>
								<c:when test="${estimatedShipping == 0.0}">
									 <c:choose>
                                		<c:when test="${order.shipToHomeShippingMethodsCount > 0}">
                                			<fmt:message key="shoppingcart.free" />
                                		</c:when>
                                		<c:otherwise>
                                			<fmt:message key="shoppingcart.emptyShipping" />
                                		</c:otherwise>
                            		</c:choose>
								</c:when>
								<c:otherwise>
									<dsp:valueof format="currency" value="${estimatedShipping}" locale="en_US" converter="currency"/>
								</c:otherwise>
							</c:choose>
						</span>
							</div>
						</c:if>
						<dsp:getvalueof var="totalShippingSurcharge" value="${order.priceInfo.totalShippingSurcharge}" />
						<c:if test="${totalShippingSurcharge > 0.0 }">
							<div>
								<span class="order-summary-item-label"> <fmt:message key="shoppingcart.shipping.freeSurcharge" />
							</span> <span class="order-summary-item-value"> <dsp:valueof format="currency" value="${order.priceInfo.totalShippingSurcharge}" locale="en_US" converter="currency"/>
							</span>
							</div>
						</c:if>
						<dsp:getvalueof var="ewasteFees" value="${order.priceInfo.ewasteFees}" />
						<c:if test="${ewasteFees gt 0.0}">
							<div class="summary-fees">
								<span>
								<a data-target="#checkoutFeesPopup" data-toggle="modal" href="#"><fmt:message key="shoppingcart.ewaste.recycling.fee" /></a>
							</span>
								<span class="summary-price pull-right">
								<dsp:valueof format="currency" value="${ewasteFees}" locale="en_US" converter="currency"/>
							</span>
							</div>
						</c:if>
						<%-- <dsp:getvalueof var="mattressRecyclingFee" value="${order.priceInfo.mattressRecyclingFee}"/>
					<c:if test="${mattressRecyclingFee gt 0.0}">
						<div class="summary-fees">
							<span>
								<a data-target="#checkoutFeesPopup" data-toggle="modal" href="#"><fmt:message key="shoppingcart.mattress.recycling.fee" /></a>
							</span>
							<span class="summary-price pull-right">
								<dsp:valueof format="currency" value="${mattressRecyclingFee}" locale="en_US" converter="currency"/>
							</span>
						</div>
					</c:if> --%>
							<div class="estimatedSalesTax">
								<span class="order-summary-item-label"> <fmt:message key="shoppingcart.estimated.sales.tax" /> </span>
								<span class="order-summary-item-value">
							<c:choose>
                               <c:when test="${not empty order.taxPriceInfo && not empty order.taxPriceInfo.estimatedSalesTax && order.taxPriceInfo.estimatedSalesTax gt 0.0}">
                               		<dsp:valueof format="currency" value="${order.taxPriceInfo.estimatedSalesTax}" locale="en_US" converter="currency"/>
                               </c:when>
                               <c:otherwise>
                               		<span class="sales-tax"> <fmt:message key="shoppingcart.calculated.checkout" /></span>
								</c:otherwise>
								</c:choose>
								</span>
							</div>
							<dsp:getvalueof var="estimatedLocalTax" value="${order.taxPriceInfo.estimatedLocalTax}" />
							<c:if test="${estimatedLocalTax gt 0.0}">
								<div class="estimatedLocalTax">
									<span>
								<a id="estimatedLocalTaxLabel" data-toggle="popover" href="#" data-original-title="" title=""><fmt:message key="shoppingcart.estimated.local.tax" /></a>
							</span>
									<div class="popover estimatedLocalTaxTooltip" role="tooltip">
										<div class="arrow"></div>
										<span class="content"><fmt:message
										key="checkout.priceSummary.estimatedLocalTax" /> </span>
									</div>
									<span>
                           		<dsp:valueof format="currency" value="${estimatedLocalTax}" locale="en_US" converter="currency"/>
							</span>
								</div>
							</c:if>
							<dsp:getvalueof var="estimatedIslandTax" value="${order.taxPriceInfo.estimatedIslandTax}" />
							<c:if test="${estimatedIslandTax gt 0.0}">
								<div class="estimatedIslandTax">
									<span>
								<a id="estimatedIslandTaxLabel" data-toggle="popover" href="#" data-original-title="" title=""><fmt:message key="shoppingcart.estimated.island.tax" /></a>
							</span>
									<div class="popover estimatedIslandTaxTooltip" role="tooltip">
										<div class="arrow"></div>
										<span class="content"><fmt:message key="checkout.priceSummary.estimatedIslandTax"/> </span>
									</div>
									<span>
                           		<dsp:valueof format="currency" value="${order.taxPriceInfo.estimatedIslandTax}" locale="en_US" converter="currency"/>
							</span>
								</div>
							</c:if>
							<div class="order-summary-total-container">
								<span class="summary-block-footer-total order-summary-item-label"> <fmt:message key="shoppingcart.total" />
						</span> <span class="order-summary-item-value"> <dsp:valueof format="currency" value="${order.priceInfo.total}" locale="en_US" converter="currency"/>
						</span>
							</div>
					</div>
					<div class="summary-block-footer">
						<div class="summary-block-footer-padded">
							<c:if test="${order.priceInfo.totalSavings gt 0.0}">
								<div class="summary-block-footer-saved text-center" tabindex="0">
									<fmt:message key="shoppingcart.saved " />&nbsp;
									<dsp:valueof value="${order.priceInfo.totalSavings}" number="0.00" converter="currency" />&nbsp;
									<fmt:message key="shoppingcart.left.bracket" />
									<dsp:valueof value="${order.priceInfo.totalSavingsPercentage}" converter="number" format="##" />
									<fmt:message key="shoppingcart.percentage" />
									<fmt:message key="shoppingcart.right.bracket" />&nbsp;
									<fmt:message key="shoppingcart.on.this.order" />
								</div>
							</c:if>
							<div class="summary-block-footer-points">
								<div class="yellow-star inline"></div>
								Earns 110 Rewards�??R�?�Us points
							</div>
						</div>
						<div class="order-summary-place-order">
							<c:choose>
								<c:when test="${commerceItemCount ge 0 }">
									<button class="summary-block-secure-checkout text-center" onclick="moveToPurchaseInfo();">
									<div class="secure-checkout-lock inline"></div>
									secure checkout
								</button>
								</c:when>
								<c:otherwise>
									<button class="summary-block-secure-checkout text-center">
									<div class="secure-checkout-lock inline"></div>
									secure checkout
								</button>
								</c:otherwise>
							</c:choose>
						</div>
					</div>

				<dsp:droplet name="Switch">
					<dsp:param name="value" value="${showPaypalCheckOutFlag}" />
					<dsp:oparam name="default">
						<dsp:droplet name="TRUCheckPaypalExpressCheckoutDroplet">
							<dsp:param name="order" bean="ShoppingCart.current" />
							<dsp:param name="userSession" bean="TRUUserSession" />
							<dsp:oparam name="output">
								<dsp:droplet name="Switch">
									<dsp:param param="isAlreadyToken" name="value" />
									<dsp:oparam name="true">
										<dsp:a href="" iclass="paypal-button-hidden" bean="CartModifierFormHandler.cartExpressCheckoutWithPayPal" id="cartPaypalPaymentExpressCheckoutId"
										 value="cartExpressCheckoutWithPayPal">
											<div class="paypal-checkout" tabindex="0"></div>
										</dsp:a>
									</dsp:oparam>
									<dsp:oparam name="false">
										<input type="hidden" name="payPalToken" id="payPalToken" value="notTokenExist" />
										<div class="paypal-checkout" tabindex="0" onclick="initializePaypal()"></div>
									</dsp:oparam>
								</dsp:droplet>
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
					<dsp:oparam name="false">
						<c:choose>
							<c:when test="${balanceAmount == 0.0 }">
								<div class="paypal-checkout disabled-paypal-tooltip" />
							</c:when>
							<c:otherwise>
								<div class="paypal-checkout disabled-paypal">
									<div class="inline">
										<span class="paypalTooltip" style="display: none"><fmt:message key="shoppingcart.checkout.paypal.tooltip" /></span>
									</div>
								</div>
							</c:otherwise>
						</c:choose>
					</dsp:oparam>
				</dsp:droplet>

					<!-- Included page for shipping progress bar -->
					<c:if test="${(showProgressBar) and (siteStatus != 'sos')}">
						<div class="summary-block-free-shipping" tabindex="0">
							<input type="hidden" id="promotionValue" value="${spendAmount}" />
							<input type="hidden" id="orderValue" value="${order.priceInfo.amount-excludedSkusAmount}" />
							<c:choose>
								<c:when test="${amountToQualify < 0}">
									<p>
										<fmt:message key="shoppingcart.free.shipping" />
									</p>
								</c:when>
								<c:otherwise>
									<p>
										<fmt:message key="shoppingcart.free.shipping.youare" />&nbsp;
										<dsp:valueof value="${amountToQualify}" number="0.00" locale="en_US" converter="currency" format="currency" />&nbsp;
										<fmt:message key="shoppingcart.away.from.free.shipping" />
									</p>
								</c:otherwise>
							</c:choose>
							<div>
								<table class="promoTable">
									<tr>
										<td width="5%">$0</td>
										<td>
											<div class="summary-block-free-shipping-bar inline" value="50"></div>
										</td>
										<td width="10%">$
											<fmt:formatNumber type="number" groupingUsed="false" value="${spendAmount}" />
										</td>
									</tr>
								</table>
							</div>
						</div>
					</c:if>					
				</div>
			</div>
			<div class="my-account-norton pull-right" id="load-cart-norton-script">
				<%-- <dsp:include page="../common/nortonSealScript.jsp"/>
			<!-- <a href="http://www.symantec.com/page.jsp?id=seal-transition" target="_blank"><img
				src="../images/Global_Norton-Logo.jpg"> </a> -->
		</div> --%>
			</div>
</dsp:page>
