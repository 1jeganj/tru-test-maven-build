<dsp:page>
    <fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
    <dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration"/>
    <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
    <dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
    <dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
    <dsp:importbean bean="/atg/commerce/promotion/CouponFormHandler"/>
    <dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
    <dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
    <dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath"/>
    <dsp:getvalueof bean="TRUTealiumConfiguration.PDPProdMerchCategory" var="productMerchandisingCategory"/>
    <dsp:getvalueof param="inlineMessage" var="inlineMessage"/>
    <dsp:getvalueof param="existingRelationShipId" var="existingRelationShipId"/>
    <dsp:getvalueof var="relationshipItemId" param="item.id"/>
    <dsp:getvalueof var="giftWrapSkuId" param="item.wrapSkuId"/>
    <dsp:getvalueof var="skuId" param="item.skuId"/>
    <dsp:getvalueof var="locationId" param="item.infoMsgLocationId"/>
    <dsp:getvalueof var="qty" param="item.quantity"/>
    <dsp:getvalueof var="commerceItemId" param="item.commerceItemId"/>
    <dsp:getvalueof var="index" param="index"/>
    <dsp:getvalueof var="displayStrikeThrough" param="item.displayStrikeThrough"/>
    <dsp:getvalueof var="savedAmount" param="item.savedAmount"/>
    <dsp:getvalueof var="onlinePID" param="item.skuItem.onlinePid" />
    <dsp:getvalueof var="inventoryStatus" param="item.inventoryStatus" />
    <%-- 	<dsp:getvalueof var="omniProductID" param="item.productId" /> --%>
    <%-- 	<input type="hidden" id="omniProductID" value="${omniProductID}"/> --%>
    <input type="hidden" id="omniProdMerchCat" value="${productMerchandisingCategory}"/>
    <div class="product-information-header">
        <div class="product-header-title">
            <a class="productName" href="/product?productId=${onlinePID}"><dsp:valueof param="item.displayName"/></a>
            <dsp:include page="/pricing/displayPromotions.jsp">
                <dsp:param name="item" param="item"/>
            </dsp:include>
        </div>
        <c:choose>
            <c:when test="${displayStrikeThrough || savedAmount > 0.0}">
                <div class="product-header-price product-header-price-savings">
                    <dsp:valueof param="item.totalAmount" converter="currency"/>
                </div>
            </c:when>    
            <c:otherwise>
                <div class="product-header-price">
                    <dsp:valueof param="item.totalAmount" converter="currency"/>
                </div>
            </c:otherwise>
        </c:choose>        
    </div>

    <dsp:droplet name="IsEmpty">
        <dsp:param name="value" param="item.channelUserName"/>
        <dsp:oparam name="false">
            <div class="registry inline">
                <div></div>
                <fmt:message key="shoppingcart.added.from"/> &nbsp;
                <span class="text-capitalize"><dsp:valueof param="item.channelUserName"/></span>
                <dsp:droplet name="IsEmpty">
                    <dsp:param name="value" param="item.channelCoUserName"/>
                    <dsp:oparam name="false">
                        and <span class="text-capitalize"><dsp:valueof param="item.channelCoUserName"/></span>
                    </dsp:oparam>
                </dsp:droplet>
                's &nbsp;<dsp:valueof param="item.channelType"/>
            </div>
        </dsp:oparam>
    </dsp:droplet>
    <div class="product-information-content">
        <div class="row row-no-padding">
            <div class="col-md-2 product-display-image-wraper">
                <dsp:getvalueof var="productImage" param="item.productImage"/>
                <dsp:droplet name="IsEmpty">
                    <dsp:param name="value" value="${productImage}"/>
                    <dsp:oparam name="false">
                        <dsp:droplet name="/com/tru/common/droplet/TRUAkamaiImageDroplet">
                            <dsp:param name="imageName" value="${productImage}"/>
                            <dsp:param name="imageHeight" value="135"/>
                            <dsp:param name="imageWidth" value="135"/>
                            <dsp:oparam name="output">
                                <dsp:getvalueof var="akamaiImageUrl" param="akamaiUrl"/>
                            </dsp:oparam>
                        </dsp:droplet>
                        <a class="displayImage" href="/product?productId=${onlinePID}" aria-label="shoping cart product image"><img class="product-description-image" src="${akamaiImageUrl}" alt="product image"></a>
                    </dsp:oparam>
                    <dsp:oparam name="true">
                        <a class="displayImage" href="/product?productId=${onlinePID}" aria-label="shoping cart product image"><img class="product-description-image image-not-available" src="${TRUImagePath}images/no-image500.gif" alt="product image"></a>
                    </dsp:oparam>
                </dsp:droplet>
                <div class="shopping-cart-stock">
                    <dsp:droplet name="Switch">
                        <dsp:param name="value" param="item.inventoryStatus" />
                        <dsp:oparam name="inStock">
                            <div class="in-stock"><fmt:message key="shoppingcart.instock" /></div>
                        </dsp:oparam>
                        <dsp:oparam name="preOrder">
                            <div class="inline pre-release-text"><fmt:message key="shoppingcart.prerelease" /></div>
                        </dsp:oparam>
                    </dsp:droplet>
                </div>                
            </div>
            <div class="col-md-5 product-information-description" tabindex="0">
                    <%-- display item details section --%>
                <dsp:include page="/cart/displayItemDetailsFrag.jsp">
                    <dsp:param name="item" param="item"/>
                </dsp:include>

                <dsp:droplet name="Switch">
                    <dsp:param name="value" param="item.fromMetaInfo"/>
                    <dsp:oparam name="true">
                        <dsp:getvalueof var="removalArray" value="${relationshipItemId}_${qty}_update_${giftWrapSkuId}"></dsp:getvalueof>
                    </dsp:oparam>
                    <dsp:oparam name="false">
                        <dsp:getvalueof var="removalArray" value="${relationshipItemId}_${qty}_remove_${giftWrapSkuId}"></dsp:getvalueof>
                    </dsp:oparam>
                </dsp:droplet>
                <dsp:droplet name="IsEmpty">
                    <dsp:param name="value" param="item.quantity"/>
                    <dsp:oparam name="false">
                        <div class="quantity">
                            <dsp:getvalueof var="qtyLimit" param="item.customerPurchaseLimit"/>
                            <dsp:getvalueof var="maxItemQty" bean="/com/tru/common/TRUConfiguration.maximumItemQuantity"/>
                            <c:if test="${qtyLimit eq 0}">
                                <dsp:getvalueof var="qtyLimit" value="${maxItemQty}"/>
                            </c:if>
                            <dsp:getvalueof var="itemQtyInCart" param="item.itemQtyInCart"/>
                            <dsp:getvalueof var="relItemQtyInCart" param="item.relItemQtyInCart"/>
                            <dsp:droplet name="Switch">
                                <dsp:param name="value" param="item.gWPPromotionItem"/>
                                <dsp:oparam name="true">
                                    <div class="stepper" id="${index}" relationshipItemId="${relationshipItemId}" lineItemQtyInCart="${qty}" quantityLimitVal="${qtyLimit}" relItemQtyInCart="${relItemQtyInCart}" otherMetaInfoQty="${relItemQtyInCart-qty}" giftWrapSkuId="${giftWrapSkuId}">
                                        <a href="javascript:void(0)" class="decrease disabled cart" title="decrease count"></a>
                                        <input type="text" class="counter center lineItemQty" disabled="disabled" maxlength="3" contenteditable="true" value="${qty}" aria-label="Item Count">
                                        <a href="javascript:void(0)" class="increase disabled cart" title="increase count"></a>
                                    </div>
                                </dsp:oparam>
                                <dsp:oparam name="false">
                                    <div class="stepper" id="${index}" relationshipItemId="${relationshipItemId}" lineItemQtyInCart="${qty}"
                                         quantityLimitVal="${qtyLimit}" relItemQtyInCart="${relItemQtyInCart}" otherMetaInfoQty="${relItemQtyInCart-qty}" giftWrapSkuId="${giftWrapSkuId}">
                                        <c:choose>
                                            <c:when test="${1 eq qty and itemQtyInCart < qtyLimit}">
                                                <a href="javascript:void(0)" class="decrease disabled cart" title="decrease count"></a>
                                                <input type="text" class="counter center lineItemQty" onkeypress="return isNumberKey(event)" maxlength="3" contenteditable="true" value="${qty}" aria-label="Item Count">
                                                <a href="javascript:void(0)" class="increase enabled cart increaseCount" onclick='javascript:utag.link({"event_type":"cart_update","product_merchandising_category":[" "]});' title="increase count"></a>
                                            </c:when>
                                            <c:when test="${qtyLimit eq qty or qtyLimit eq itemQtyInCart}">
                                                <dsp:droplet name="Switch">
                                                    <dsp:param name="value" value="${qty}"/>
                                                    <dsp:oparam name="1">
                                                        <a href="javascript:void(0)" class="decrease disabled cart" title="decrease count" onclick='javascript:utag.link({"event_type":"cart_update","product_merchandising_category":[" "]);'></a>
                                                        <input type="text" class="counter center lineItemQty" onkeypress="return isNumberKey(event)" maxlength="3" contenteditable="true" value="${qty}" aria-label="Item Count">
                                                        <a href="javascript:void(0)" class="increase disabled cart" title="increase count"></a>
                                                    </dsp:oparam>
                                                    <dsp:oparam name="default">
                                                        <a href="javascript:void(0)" class="decrease enabled cart" title="decrease count" onclick='javascript:utag.link({"event_type":"cart_update","product_merchandising_category":[" "]});'></a>
                                                        <input type="text" class="counter center lineItemQty" contenteditable="true" onkeypress="return isNumberKey(event)" maxlength="3" value="${qty}" aria-label="Item Count">
                                                        <a href="javascript:void(0)" class="increase disabled cart" title="increase count"></a>
                                                    </dsp:oparam>
                                                </dsp:droplet>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="javascript:void(0)" class="decrease enabled cart" title="decrease count" onclick='javascript:utag.link({"event_type":"cart_update","product_merchandising_category":[" "]});'></a>
                                                <input type="text" class="counter center lineItemQty" onkeypress="return isNumberKey(event);" contenteditable="true" maxlength="3" value="${qty}" aria-label="Item Count">
                                                <a href="javascript:void(0)" class="increase enabled cart increaseCount" title="increase count" onclick='javascript:utag.link({"event_type":"cart_update","product_merchandising_category":[" "]});'></a>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                    <dsp:getvalueof var="qtyLimitVal" param="item.customerPurchaseLimit"/>
                                    <c:choose>
                                        <c:when test="${relationshipItemId eq existingRelationShipId}">
                                            <dsp:droplet name="IsEmpty">
                                                <dsp:param name="value" param="inlineMessage"/>
                                                <dsp:oparam name="false">
                                                    <span class="cart-error-message" id="cart-error-message_${relationshipItemId}"><dsp:valueof param="inlineMessage" valueishtml="true"/></span>
                                                </dsp:oparam>
                                                <dsp:oparam name="true">
                                                    <div class="limit-1 inline" id="limit_${skuId}_${locationId}">
                                                        <c:if test="${qtyLimitVal ne 0}">
                                                            <fmt:message key="shoppingcart.item.limit"/>&nbsp;${qtyLimitVal}&nbsp;<fmt:message key="shoppingcart.item.limit.customer"/>
                                                        </c:if>
                                                    </div>
                                                </dsp:oparam>
                                            </dsp:droplet>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="limit-1 inline" id="limit_${skuId}_${locationId}">
                                                <c:if test="${qtyLimitVal ne 0}">
                                                    <fmt:message key="shoppingcart.item.limit"/>&nbsp;${qtyLimitVal}&nbsp;<fmt:message key="shoppingcart.item.limit.customer"/>
                                                </c:if>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </dsp:oparam>
                            </dsp:droplet>
                        </div>
                    </dsp:oparam>
                </dsp:droplet>
                <dsp:droplet name="IsEmpty">
                    <dsp:param name="value" param="item.locationId"/>
                    <dsp:oparam name="true">
                        <div class="shopping-cart-gift co-shopping-cart-gift">
                            <dsp:droplet name="Switch">
                                <dsp:param name="value" param="item.giftWrap"/>
                                <dsp:oparam name="true">
                                    <div class="checkout-flow-sprite checkout-flow-radio-unselected checkout-flow-radio-selected" tabindex="0" id="${commerceItemId}" onclick="updateGiftItem(this,'<dsp:valueof param="item.shipGroupId"/>','${relationshipItemId}','${commerceItemId}', '${qty}','<dsp:valueof param="item.wrapProductId"/>','<dsp:valueof param="item.wrapSkuId"/>');">
                                    </div>
                                    <div class="checkout-flow-sprite checkout-flow-gift"></div>
                                    <dsp:droplet name="IsEmpty">
                                        <dsp:param name="value" param="item.wrapDisplayName"/>
                                        <dsp:oparam name="false">
                                            <dsp:getvalueof var="giftWrapName" param="item.wrapDisplayName"></dsp:getvalueof>
                                            <div class="inline">
                                                <fmt:message key="shoppingcart.this.gift.with">
                                                    <fmt:param value="${giftWrapName}"></fmt:param>
                                                </fmt:message>
                                            </div>
                                        </dsp:oparam>
                                        <dsp:oparam name="true">
                                            <div class="inline"><fmt:message key="shoppingcart.this.gift"/></div>
                                        </dsp:oparam>
                                    </dsp:droplet>
                                </dsp:oparam>
                                <dsp:oparam name="false">
                                    <div class="checkout-flow-sprite checkout-flow-radio-unselected" tabindex="0" id="${commerceItemId}" onclick="updateGiftItem(this,'<dsp:valueof param="item.shipGroupId"/>','${relationshipItemId}','${commerceItemId}', '${qty}','<dsp:valueof param="item.wrapProductId"/>','<dsp:valueof param="item.wrapSkuId"/>');">
                                    </div>
                                    <div class="checkout-flow-sprite checkout-flow-gift"></div>
                                    <div class="inline"><fmt:message key="shoppingcart.this.gift"/></div>
                                </dsp:oparam>
                            </dsp:droplet>
                            <div class="errorDisplay display-none"></div>
                        </div>
                    </dsp:oparam>
                </dsp:droplet>
            </div>
            <div class="col-md-5 radio-checked shipping-options-wrapper">
                <dsp:include page="/cart/cartShippingOptions.jsp">
                    <dsp:param name="item" param="item"/>
                </dsp:include>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-push-2">
                <dsp:include page="/cart/buyersProtectionPlan.jsp">
                    <dsp:param name="item" param="item"/>
                </dsp:include>
            </div>
        </div>
        <div class="remove-product-item-container">
            <dsp:a href="javascript:void(0);" class="remove-product-item" id="${removalArray}">
                <fmt:message key="shoppingcart.remove"/>
            </dsp:a>
        </div>
    </div>
    <hr>
</dsp:page>