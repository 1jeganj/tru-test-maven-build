<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:getvalueof param="existingRelationShipId" var="existingRelationShipId"/>
	<dsp:droplet name="ForEach">
		<dsp:param name="array" param="items" />
		<dsp:param name="elementName" value="item" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="relationshipItemId" param="item.id" />
			<dsp:getvalueof var="index" param="index" />
			<dsp:droplet name="Switch">
				<dsp:param name="value" param="item.donationItem" />
				<dsp:oparam name="default">
					<dsp:droplet name="Switch">
						<dsp:param name="value" param="item.inventoryStatus" />
						<dsp:oparam name="inStock">
							<c:choose>
								<c:when test="${relationshipItemId eq existingRelationShipId}">
									<dsp:droplet name="Switch">
										<dsp:param bean="CartModifierFormHandler.formError" name="value" />
										<dsp:oparam name="true">
											<div class="product-information error"
												id="product-information_${relationshipItemId}">
										</dsp:oparam>
										<dsp:oparam name="default">
											<div class="product-information" id="product-information_${relationshipItemId}">
										</dsp:oparam>
									</dsp:droplet>
								</c:when>
								<c:otherwise>
									<div class="product-information" id="product-information_${relationshipItemId}">
								</c:otherwise>
							</c:choose>
							<dsp:include page="cartLineItemsDetails.jsp">
								<dsp:param name="item" param="item" />
								<dsp:param name="index" param="index" />
								<dsp:param name="inlineMessage" param="inlineMessage" />
								<dsp:param name="existingRelationShipId" param="existingRelationShipId" />
							</dsp:include>
							</div>
						</dsp:oparam>
						<dsp:oparam name="preOrder">
							<c:choose>
								<c:when test="${relationshipItemId eq existingRelationShipId}">
									<dsp:droplet name="Switch">
										<dsp:param bean="CartModifierFormHandler.formError" name="value" />
										<dsp:oparam name="true">
											<div class="product-information partial pre-release error"
												id="product-information_${relationshipItemId}">
										</dsp:oparam>
										<dsp:oparam name="default">
											<div class="product-information partial pre-release" id="product-information_${relationshipItemId}">
										</dsp:oparam>
									</dsp:droplet>
								</c:when>
								<c:otherwise>
									<div class="product-information partial pre-release" id="product-information_${relationshipItemId}">
								</c:otherwise>
							</c:choose>
							<dsp:include page="cartLineItemsDetails.jsp">
								<dsp:param name="item" param="item" />
								<dsp:param name="index" param="index" />
								<dsp:param name="inlineMessage" param="inlineMessage" />
								<dsp:param name="existingRelationShipId" param="existingRelationShipId" />
							</dsp:include>
							</div>
						</dsp:oparam>
						<dsp:oparam name="outOfStock">
							<div class="product-information out-of-stock">
								<dsp:include page="outOfStockItemDetails.jsp">
									<dsp:param name="item" param="item" />
									<dsp:param name="index" param="index" />
								</dsp:include>
							</div>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="true">
					<dsp:include page="donationItemDetails.jsp">
						<dsp:param name="item" param="item" />
					</dsp:include>
				</dsp:oparam>
			</dsp:droplet>
			
		</dsp:oparam>
	</dsp:droplet>

</dsp:page>
