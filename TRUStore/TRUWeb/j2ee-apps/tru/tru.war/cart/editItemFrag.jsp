<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:getvalueof var="pageName" param="pageName" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:getvalueof var="relationshipItemId" param="item.id"/>
	<dsp:getvalueof var="wrapSkuId" param="item.wrapSkuId"/>
	<dsp:getvalueof var="productId" param="item.productId"/>
	<dsp:getvalueof var="metaInfoId" param="item.metaInfoId"/>
	<dsp:getvalueof var="itemColor" param="item.color"/>
	<dsp:getvalueof var="itemSize" param="item.size"/>
	<dsp:getvalueof var="gWPPromotionItem" param="item.gWPPromotionItem"/>
	<input type="hidden" value="${metaInfoId}" class="metaInfoIdClass" />
	<input type="hidden" value="${relationshipItemId}" class="relationshipItemClass" />
	<input type="hidden" value="${wrapSkuId}" class="wrapSkuIdClass" />
	<input type="hidden" value="${productId}" class="productId" />
	<input type="hidden" value="${pageName}" class="pageName" />
	<input type="hidden" value="${gWPPromotionItem}" class="gWPPromotionItem" />
	<input type="hidden" value="${itemColor}" class="itemColor" />
	<input type="hidden" value="${itemSize}" class="itemSize" />
	
	<dsp:getvalueof var="item" param="item"/>
	<c:choose>
		<c:when test="${pageName eq 'cart'}">
			<c:choose>
				<c:when test="${not empty item.color || not empty item.size}">
					<a class="edit-product-button closed"  data-toggle="modal" data-target="#editCartModal" aria-label = "edit_Product_Button" title = "edit_Product_Button" href="javascript:void(0);">
					<fmt:message key="shoppingcart.edit" /></a>
				</c:when>
				<c:otherwise>
					<a class="edit-product-button closed non-stylized"  data-toggle="modal" data-target="#editCartModal" aria-label = "edit_Product_Button" title = "edit_Product_Button" href="javascript:void(0);"><fmt:message key="shoppingcart.edit" /></a>
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>
			<a class="edit-product-button closed"  data-toggle="modal" aria-label = "edit_Product_Button" title = "edit_Product_Button" href="javascript:void(0);" onclick="javascript:forConsoleErrorFixing(this);">
			<fmt:message key="shoppingcart.edit" /></a>
		</c:otherwise>
	</c:choose>
</dsp:page>