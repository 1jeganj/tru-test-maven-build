<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:getvalueof bean="/com/tru/common/TRUConfiguration.charityDonationRedirectURL" var="charityDonationRedirectURL"/>
<div class="checkout-review-product-block partial">
<div class="product-information">
          <div class="product-information-header">
              <div class="product-header-title"><a href="${charityDonationRedirectURL}" target="_blank"><dsp:valueof param="item.donationItemDisplayName" /></a></div>
              <div class="product-header-price">
                  <dsp:valueof param="item.donationAmount" converter="currency"/>
              </div>
          </div>
          <div class="product-information-content">
              <div class="row row-no-padding">
                  <div class="col-md-2">
                      	<dsp:getvalueof var="donationImage" param="item.donationItemImageUrl"/>
                   		<dsp:droplet name="IsEmpty">
                   			<dsp:param name="value" value="${donationImage}" />
                   			<dsp:oparam name="false">
                   				<a href="${charityDonationRedirectURL}" target="_blank">shopping-cart-product-image-donation
                   					<img class="shopping-cart-product-image-donation" src="${donationImage}" alt="Product_Donation_Image">
                   				</a>
                   			</dsp:oparam>
                   			<dsp:oparam name="true">
                   				<a href="${charityDonationRedirectURL}" target="_blank">shopping-cart-product-image-donation
                   					<img class="shopping-cart-product-image-donation image-not-available" src="${TRUImagePath}images/no-image500.gif" alt="Product_Donation_Image">
                   				</a>
                   			</dsp:oparam>
                   		</dsp:droplet>
                  </div>
                  <%-- <div class="col-md-6 donation-amount"><dsp:valueof param="item.donationAmount" converter="currency"/></div> --%>
                  <div class="col-md-5 product-information-description" tabindex="0"><p class="donation-thankyou"><fmt:message key="checkout.review.donationThankYouMessage"/></p>
                  		<dsp:getvalueof var="commerceItemId" param="item.commerceItemId"/>
						<div class="edit"> 
						<dsp:a href="javascript:void(0);" class="remove-donation-cart-item" id="${commerceItemId}">
	                    <fmt:message key="shoppingcart.remove" />
	            </dsp:a></div>
					</div>
              </div>
          </div>
      </div>
      <hr>
     </div>
</dsp:page>