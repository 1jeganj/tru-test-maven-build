<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration"/>
	<dsp:getvalueof bean="TRUTealiumConfiguration.PDPProdMerchCategory" var="productMerchandisingCategory"/>
	<dsp:getvalueof var="relationshipItemId" param="item.id" />
	<dsp:getvalueof var="giftWrapSkuId" param="item.wrapSkuId" />
	<dsp:getvalueof var="quantity" param="item.quantity" />
	<dsp:getvalueof var="pageName" param="pageName" />
	<dsp:getvalueof var="tealiumProductId" param="item.productId" />
	<input type="hidden" id="prodMerchCategory" value="productMerchandisingCategory"/>
	<c:choose>
		<c:when test="${pageName eq 'Shipping'}">
			<dsp:getvalueof var="parentQuantity" param="item.parentQuantity" />
			<c:choose>
				<c:when test="${parentQuantity > 1}">
					<dsp:getvalueof var="removalArray"
						value="${relationshipItemId}_${quantity}_update_${giftWrapSkuId}"></dsp:getvalueof>
				</c:when>
				<c:otherwise>
					<dsp:getvalueof var="removalArray"
						value="${relationshipItemId}_${quantity}_remove_${giftWrapSkuId}"></dsp:getvalueof>
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>
			<dsp:droplet name="Switch">
				<dsp:param name="value" param="item.fromMetaInfo" />
				<dsp:oparam name="true">
					<dsp:getvalueof var="removalArray"
						value="${relationshipItemId}_${quantity}_update_${giftWrapSkuId}"></dsp:getvalueof>
				</dsp:oparam>
				<dsp:oparam name="false">
					<dsp:getvalueof var="removalArray"
						value="${relationshipItemId}_${quantity}_remove_${giftWrapSkuId}"></dsp:getvalueof>
				</dsp:oparam>
			</dsp:droplet>
		</c:otherwise>
	</c:choose>
	<dsp:a href="javascript:void(0);" class="remove-product-item" id="${removalArray}">
		<fmt:message key="shoppingcart.remove" />
	</dsp:a>
</dsp:page>