<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:getvalueof var="orderId" bean="ShoppingCart.current.id" />
	<dsp:getvalueof var="maxCouponLimit" param="maxCouponLimit" />
	<div class="shopping-cart-promo-code">
		<h3>
			<fmt:message key="shoppingcart.have.promo" />
		</h3>		
		<div class="toggle-promo-code-container">
			<div class="row">
				<div class="col-sm-3 enter-promo-code-container">
					<button class="enter-promo-code-btn" type="button">enter a code</button>
				</div>
				<div class="col-sm-6 terms-of-use-container">
					<p class="promcodeTermUseMsg display-none">
						<fmt:message key="promotionCode.terms.of.use.toolTips" />
					</p>
					<div class="terms-of-use">
						<a href="javascript:void(0)" class="pull-left promcodeTermUse">
							<fmt:message key="shoppingcart.terms.of.use" />
						</a>
					</div>
				</div>
			</div>
		</div>
		<c:choose>
			<c:when test="${maxCouponLimit}">
				<div class="enter-promo">
					<div class="enter-promo-input-group">
						<dsp:form id="applycoupon" formid="applycoupon" onsubmit="javascript:return false;">
							<input id="orderId" type="hidden" value="${orderId}" />
							<input maxlength="20" class="inline apply-promo-code-textbox" id="promoCode" disabled="disabled" />
							<button id="apply-promo-code" class="apply-promo-code" type="button" disabled="disabled" onclick="applyCoupon()">apply</button>
						</dsp:form>
						<p id="maxCouponLimit">
							<fmt:message key="shoppingcart.max.coupon.limt" />
						</p>
					</div>

					<dsp:include page="/pricing/couponCodeSummary.jsp">
					</dsp:include>
				</div>
			</c:when>
			<c:otherwise>
				<div class="enter-promo">
					<div class="row">
						<div class="col-sm-7">
							<div class="enter-promo-input-group">
								<dsp:form id="applycoupon" formid="applycoupon" onsubmit="javascript:return false;">
									<input id="orderId" type="hidden" value="${orderId}" />
									<div class="promo-code-textbox-fieldset">promo code</div>
									<div class="promo-code-textbox-fieldset-error">invalid code</div>
									<input maxlength="20" class="inline apply-promo-code-textbox" id="promoCode" placeholder="enter promo code" />
									<div class="checkout-flow-sprite checkout-flow-clear promo-code-textbox-clear"></div>
									<button id="apply-promo-code" class="apply-promo-code" type="button">apply</button>
								</dsp:form>
							</div>
							<div class="enter-promo-input-group error-promo-input-group">
								<input maxlength="20" class="inline apply-promo-code-textbox" type="text" id="promoCode_error">
								<button id="error-apply-promo-code" class="apply-promo-code" type="button" disabled="disabled" onclick="applyCoupon()">apply</button>
								<p id="error_uyrrkeh">
								</p>
							</div>
							<dsp:include page="/pricing/couponCodeSummary.jsp">
							</dsp:include>
						</div>
						<div class="col-sm-5">
							<div class="col-sm-6 terms-of-use-container">
								<p class="promcodeTermUseMsg display-none">
									<fmt:message key="promotionCode.terms.of.use.toolTips" />
								</p>
								<div class="terms-of-use">
									<a href="javascript:void(0)" class="pull-left promcodeTermUse">
										<fmt:message key="shoppingcart.terms.of.use" />
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</c:otherwise>
		</c:choose>

	</div>
</dsp:page>
