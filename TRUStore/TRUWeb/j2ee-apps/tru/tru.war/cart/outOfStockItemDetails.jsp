<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:getvalueof var="relationshipItemId" param="item.id" />
	<dsp:getvalueof var="giftWrapSkuId" param="item.wrapSkuId" />
	<dsp:getvalueof var="displayStrikeThrough" param="item.displayStrikeThrough" />
	<dsp:getvalueof var="savedAmount" param="item.savedAmount" />
	<dsp:getvalueof var="index" param="index" />
<dsp:droplet name="/com/tru/utils/TRUPdpURLDroplet">
	<dsp:param name="productId" param="item.skuItem.onlinePID" />
	<dsp:param name="siteId" bean="/atg/multisite/Site.id" />
	<dsp:oparam name="output">
		<dsp:getvalueof var="productPageUrl" param="productPageUrl" />
	</dsp:oparam>
</dsp:droplet>
	<div class="product-information-header">
		<div class="product-header-title">
			<a href="javascript:void(0);"><dsp:valueof param="item.displayName" /></a>
		</div>
		<div class="product-header-price">
			<dsp:valueof param="item.totalAmount" converter="currency" />
		</div>
	</div>
	<div class="buy-1-get-free inline">
		<dsp:include page="/pricing/displayPromotions.jsp">
			<dsp:param name="item" param="item"/>
		</dsp:include>
	</div>
	<dsp:droplet name="IsEmpty">
		<dsp:param name="value" param="item.channel" />
		<dsp:oparam name="false">
			<div class="registry inline">
				<div></div>
				<fmt:message key="shoppingcart.added.from" />&nbsp;<dsp:valueof  param="item.channel"/>
			</div>
		</dsp:oparam>
	</dsp:droplet>
	<div class="product-information-content">
		<div class="row row-no-padding">
			<div class="col-md-2">
				<div>
					<dsp:getvalueof var="productImage" param="item.productImage"/>
					 <dsp:droplet name="IsEmpty">
						<dsp:param name="value" value="${productImage}" />
						<dsp:oparam name="false">
							<dsp:droplet name="/com/tru/common/droplet/TRUAkamaiImageDroplet">
								<dsp:param name="imageName" value="${productImage}" />
								<dsp:param name="imageHeight" value="124" />
								<dsp:param name="imageWidth" value="124" />
								<dsp:oparam name="output">
									<dsp:getvalueof var="akamaiImageUrl" param="akamaiUrl" />
								</dsp:oparam>
							</dsp:droplet>
							<a class="displayImage" href="javascript:void(0);" aria-label="shoping cart product image"><img class="product-description-image" src="${akamaiImageUrl}" alt="Cart Product Image"></a>
						</dsp:oparam>
						<dsp:oparam name="true">
							<a class="displayImage" href="javascript:void(0);" aria-label="shoping cart product image"><img class="product-description-image image-not-available" src="${TRUImagePath}images/no-image500.gif" height="124" width="124" alt="Cart Product Image"></a>
						</dsp:oparam>
					</dsp:droplet> 
				</div>
			</div>
			<div class="col-md-5 product-information-description" tabindex="0">
				<c:choose>
					<c:when test="${displayStrikeThrough}">
						<div class="product-description-price inline">
							<span class="crossed-out-price price-save"><dsp:valueof param="item.price" converter="currency" /></span>
							<dsp:valueof param="item.salePrice" converter="currency" />
							<span class="price-save">&nbsp;.&nbsp;</span><span class="price-save save-text">&#32;<fmt:message
									key="shoppingcart.you.save" />&#32;<dsp:valueof format="currency" value="${savedAmount}" locale="en_US"
									converter="currency" /> <fmt:message key="shoppingcart.left.bracket" />
								<dsp:valueof param="item.savedPercentage" />
								<fmt:message key="shoppingcart.percentage" />
								<fmt:message key="shoppingcart.right.bracket" /> </span>
						</div>
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${savedAmount > 0.0}">
								<div class="product-description-price inline">
									<dsp:valueof param="item.salePrice" converter="currency" />
									<span class="price-save">&nbsp;.&nbsp;</span><span class="price-save save-text">&#32; <fmt:message
											key="shoppingcart.you.save" />&#32;<dsp:valueof format="currency" value="${savedAmount}" locale="en_US"
											converter="currency" /> <fmt:message key="shoppingcart.left.bracket" />
										<dsp:valueof param="item.savedPercentage" />
										<fmt:message key="shoppingcart.percentage" />
										<fmt:message key="shoppingcart.right.bracket" />
									</span>
								</div>
							</c:when>
							<c:otherwise>
								<div class="product-description-price">
									<dsp:valueof param="item.salePrice" converter="currency" />
								</div>
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
				<dsp:droplet name="IsEmpty">
					<dsp:param name="value" param="item.skuId" />
					<dsp:oparam name="false">
						<div class="product-description-item-number">
							<fmt:message key="shoppingcart.item" />&#32;
							<dsp:valueof param="item.skuId" />
						</div>
					</dsp:oparam>
				</dsp:droplet>
				<dsp:getvalueof var="skuId" param="item.skuId" />
				<dsp:getvalueof var="description" param="item.sKUDescription" />
				<dsp:getvalueof var="skuDisplayName" param="item.displayName" />
				<dsp:getvalueof var="productId" param="item.productId" />
				<dsp:getvalueof var="onlinePID" param="item.skuItem.onlinePID" />
				<div class="shopping-cart-stock">
					<div class="red-bullet inline"></div>
					<div class="inline out-of-stock email-section">
						<fmt:message key="shoppingcart.outofstock" /> 
						<span class="out-of-stock-text">&nbsp;.&nbsp;</span>
						<dsp:droplet name="Switch">
							<dsp:param name="value" param="item.notifyMe" />
							<dsp:oparam name="Y">
								<a href="javascript:void(0);" onclick="loadNotifyMe(this)"
									class="out-of-stock-text" data-toggle="modal"
									data-target="#notifyMeModal"> <fmt:message
										key="shoppingcart.email.when.available" />
								</a>
								<div class="NotifyMeDescription">${description}</div>
								<input type="hidden" class="NotifyMeDskuDisplayName"
									value="${skuDisplayName}">
								<input type="hidden" class="productId" value="${productId}">
								<input type="hidden" class="onlinePID" value="${onlinePID}">
								<input type="hidden" class="NotifyMeskuId" value="${skuId}">
								<input type="hidden" class="NotifyMeProductPageUrl"
									value="${productPageUrl}">
								<input type="hidden" class="NotifyMeHelpURLName"
									value="${productPageUrl}">
								<input type="hidden" class="NotifyMeHelpURLValue"
									value="${productPageUrl}">
							</dsp:oparam>
						</dsp:droplet>
					</div>
				</div>
				<dsp:droplet name="IsEmpty">
					<dsp:param name="value" param="item.color" />
					<dsp:oparam name="false">
						<div class="product-description-color">
							<fmt:message key="shoppingcart.item.color" />&#32;
							<dsp:valueof param="item.color" />
						</div>
					</dsp:oparam>
				</dsp:droplet>
				<dsp:droplet name="IsEmpty">
					<dsp:param name="value" param="item.size" />
					<dsp:oparam name="false">
						<div class="product-description-size">
							<fmt:message key="shoppingcart.item.size" />&#32;
							<dsp:valueof param="item.size" />
						</div>
					</dsp:oparam>
				</dsp:droplet>
				<dsp:getvalueof var="relationshipItemId" param="item.id" />
				
				<div class="outOfStockRemove">&nbsp;&nbsp;<span>&#xb7;</span>&nbsp;
					<dsp:a href="javascript:void(0);" class="remove-product-item" id="${relationshipItemId}_0_outOfStock">
						<fmt:message key="shoppingcart.remove" />
					</dsp:a>
				</div>
                    <div class="modal fade" id="notifyMeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                       <dsp:include page="/jstemplate/notifyMePopUp.jsp">
									</dsp:include>
                                   
                        </div>   
				<dsp:droplet name="IsEmpty">
					<dsp:param name="value" param="item.quantity" />
					<dsp:oparam name="false">
						<div class="quantity">
							<div>
								<fmt:message key="shoppingcart.item.qty" />
							</div>
							<div class="stepper">
								<a href="javascript:void(0)" class="decrease disabled" id="${index}" title="decrease count"></a>
								<input type="text" class="counter center" id="quantityVal" disabled contenteditable="true" value="0" aria-label="Item Count"> 
								<a href="javascript:void(0)" class="increase disabled" id="${index}" title="increase count"></a>
							</div>
						</div>
					</dsp:oparam>
				</dsp:droplet>
				<dsp:droplet name="Switch">
					<dsp:param name="value" param="item.giftWrappable" />
					<dsp:oparam name="true">
						<div class="shopping-cart-gift">
							<div class="gift-checkbox checkbox-sm-off"></div>
							<div class="shopping-cart-gift-image inline"></div>
							<div class="inline">
								<fmt:message key="shoppingcart.this.gift" />
							</div>
						</div>
					</dsp:oparam>
				</dsp:droplet>
			</div>
			<div class="col-md-5">
					<label for="ship-to-home" class="outOfStockCursor">
						<div class="ship-to-home outOfStockCursor"><fmt:message key="shoppingcart.no.ship.home" /></div>
					</label>
				<br> 
					<label for="ship-to-home" class="outOfStockCursor">
						<div class="free-store-pickup outOfStockCursor"><fmt:message key="shoppingcart.no.store" /></div>
					</label>
			</div>
		</div>
		<div class="row row-no-paddidng shopping-cart-surprise">
			<div class="col-md-6 col-md-offset-2">
				<div>
					<div class="shopping-cart-surprise-image inline"></div>
					<div class="shopping-cart-remove inline">
						<fmt:message key="shoppingcart.spoil.surprise" />
						<span>�</span><a href="http://cloud.toysrus.resource.com/redesign/template-shopping-cart.html#"><fmt:message
								key="shoppingcart.learn.more" /></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr>
</dsp:page>
