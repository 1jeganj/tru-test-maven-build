<dsp:page>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
<dsp:getvalueof var="relationshipItemId" param="item.id" />
<dsp:getvalueof var="bppItemInfoId" param="item.bppItemInfoId" />
<dsp:getvalueof var="bppSkuId" param="item.bppSkuId" />
<dsp:getvalueof var="bppItemId" param="item.bppItemId" />
<dsp:getvalueof var="bppItemCount" param="item.bppItemCount" />
<dsp:getvalueof var="itemQty" param="item.quantity"/>
<dsp:getvalueof var="pageName" param="pageName" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	<dsp:droplet name="ForEach">
       	<dsp:param name="array" param="item.bppItemList"/>
       	<dsp:param name="elementName" value="bppItem" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="itemId" param="bppItem.bppItemId"/>
			<dsp:getvalueof var="bppSKUId" param="bppItem.bppSkuId"/>
			<input type="hidden" id="bppSKU_${itemId}" value="${bppSKUId}"/>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="IsEmpty">
	   	<dsp:param name="value" param="item.bppItemList" />
	   	<dsp:oparam name="false">
	   		<div class="addon-cta">
	   			<dsp:getvalueof var="bppImageUrl" param="item.bppSkuImageUrl"/>
	   			<dsp:droplet name="IsEmpty">
	   				<dsp:param name="value" value="${bppImageUrl}"/>
	   				<dsp:oparam name="false">
	   					<div class="cart-add-bpp-image"><img src="${bppImageUrl}" class="bpp-img-width"></div>
	   				</dsp:oparam>
	   				<dsp:oparam name="true">
	   					<div class="cart-add-bpp-image"><img src="${TRUImagePath}images/no-image500.gif" class="bpp-img-width"></div>
	   				</dsp:oparam>
	   			</dsp:droplet>
	   			
				<div class="cart-overlay-protection-plan">
			        <div class="checkbox-group">
			        	<dsp:droplet name="ForEach">
			            	<dsp:param name="array" param="item.bppItemList"/>
			            	<dsp:param name="elementName" value="bppItem" />
			            	<dsp:oparam name="output">
			            		<dsp:getvalueof var="bppSKUId" param="bppItem.bppSkuId"/>
			            	</dsp:oparam>
				        </dsp:droplet>
	            		 <dsp:droplet name="IsEmpty">
						   	<dsp:param name="value" value="${bppItemInfoId}" />
						   	<dsp:oparam name="false">
							   	<input class="compare-checkbox" id="bpp_check_${relationshipItemId}" type="checkbox" checked data-relId="${relationshipItemId}" 
							   		data-bppInfoId="${bppItemInfoId}" data-bppSkuId="${bppSKUId}" data-bppItemId="${bppItemId}" data-bppItemCount="${bppItemCount}">
						   	</dsp:oparam>
						   	<dsp:oparam name="true">
							   	<input class="compare-checkbox" id="bpp_check_${relationshipItemId}" type="checkbox" data-relId="${relationshipItemId}" 
							   		data-bppInfoId="${bppItemInfoId}" data-bppSkuId="${bppSKUId}" data-bppItemId="${bppItemId}" data-bppItemCount="${bppItemCount}">
						   	</dsp:oparam>
					   	</dsp:droplet>
			            <dsp:droplet name="Switch">
			            	<dsp:param name="value" param="item.bppItemCount"/>
			            	<dsp:oparam name="1">
			            		<dsp:droplet name="ForEach">
						            	<dsp:param name="array" param="item.bppItemList"/>
						            	<dsp:param name="elementName" value="bppItem" />
						            	<dsp:oparam name="outputStart">
						            		<dsp:getvalueof var="bppTerm" param="bppItem.bppTerm"/>
						            		<label class="compare-checkbox-label" for="bpp_check_${relationshipItemId}">
							            		<fmt:message key="shoppingcart.bpp.single.option.text">
								            		<fmt:param value="${bppTerm}"></fmt:param>
								            	</fmt:message>
							            	</label>
							            	<a target="popup" href="javascript:void(0);" class="addon-info" onclick="window.open('/cart/bppLearnMore.jsp', 'popup','width=600,height=600')">
							            		<fmt:message key="checkout.review.squareTradeLearnMore" />
							            	</a>
						            	</dsp:oparam>
										<dsp:oparam name="output">
											<span class="protection-plan-static-copy">
												<dsp:getvalueof var="bppRetail" param="bppItem.bppRetail"/>
												<fmt:setLocale value="en_US"/>
												<fmt:formatNumber value="${bppRetail*itemQty}" type="currency"/>
											</span>
										</dsp:oparam>
						            </dsp:droplet>
			            	</dsp:oparam>
			            	<dsp:oparam name="default">
			            		<label class="compare-checkbox-label" for="bpp_check_${relationshipItemId}">
			            			<fmt:message key="shoppingcart.bpp.multiple.option.text"/>
			            		</label>
				            	<a target="popup" href="javascript:void(0);" class="addon-info" onclick="window.open('/cart/bppLearnMore.jsp', 'popup','width=600,height=600')">
				            		<fmt:message key="checkout.review.squareTradeLearnMore" />
				            	</a>
			            		<dsp:droplet name="IsEmpty">
								   	<dsp:param name="value" value="${bppItemInfoId}" />
								   	<dsp:oparam name="false">
									   	<select class="bppPlanMultiShippingPage" id="bppPlan_${relationshipItemId}" data-relId="${relationshipItemId}" data-bppInfoId="${bppItemInfoId}"
				            				data-bppSkuId="${bppSkuId}">
								   	</dsp:oparam>
								   	<dsp:oparam name="true">
									   	<select class="bppPlanMultiShippingPage" id="bppPlan_${relationshipItemId}" disabled="disabled" data-relId="${relationshipItemId}" data-bppInfoId="${bppItemInfoId}"
				            				data-bppSkuId="${bppSkuId}">
								   	</dsp:oparam>
							   	</dsp:droplet>
					            	<option value="select"><fmt:message key="checkout.review.selectPlan"/></option>
						            <dsp:droplet name="ForEach">
						            	<dsp:param name="array" param="item.bppItemList"/>
						            	<dsp:param name="elementName" value="bppItem" />
										<dsp:oparam name="output">
											<dsp:getvalueof var="itemId" param="bppItem.bppItemId"/>
											<dsp:droplet name="Switch">
			            						<dsp:param name="value" param="bppItem.preSelected"/>
			            						<dsp:oparam name="true">
			            							<option value="${itemId}" selected="selected">
														<dsp:valueof param="bppItem.bppPlan"/>
													</option>
			            						</dsp:oparam>
			            						<dsp:oparam name="false">
			            							<option value="${itemId}">
														<dsp:valueof param="bppItem.bppPlan"/>
													</option>
			            						</dsp:oparam>
			            					</dsp:droplet>
										</dsp:oparam>
						            </dsp:droplet>
					            </select>
			            	</dsp:oparam>
			            </dsp:droplet>
			        </div>
			        <p><fmt:message key="checkout.review.squareTradePlanEmail"/></p>
			    </div>
			</div>
	   	</dsp:oparam>
    </dsp:droplet>
</dsp:page> 