<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />

	<dsp:droplet name="Switch">
		<dsp:param bean="CartModifierFormHandler.formError" name="value" />
		<dsp:oparam name="true">
			<dsp:droplet name="ErrorMessageForEach">
				<dsp:param name="exceptions" bean="CartModifierFormHandler.formExceptions" />
				<dsp:oparam name="outputStart">
				<div class="shopping-cart-error-state">
					<div class="shopping-cart-error-state-header row">
						<div class="error-state-exclamation-lg img-responsive col-md-2"></div>
						<div class="shopping-cart-error-state-header-text col-md-10">
							<fmt:message key="shoppingcart.oops.issue" />
							<div class="shopping-cart-error-state-subheader">
				</dsp:oparam>
				<dsp:oparam name="output">
					<dsp:getvalueof param="errorCode" var="errorCode" />
					<c:if test="${errorCode ne 'inline_msg'}">
						<dsp:valueof param="message" valueishtml="true" />
					</c:if>
				</dsp:oparam>
				<dsp:oparam name="outputEnd">
					</div>
					</div>
					</div>
					</div>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="IsEmpty">
		<dsp:param bean="CartModifierFormHandler.multipleItemsInfoMessages" name="value" />
		<dsp:oparam name="false">
			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="CartModifierFormHandler.multipleItemsInfoMessages" />
				<dsp:param name="elementName" value="message" />
				<dsp:oparam name="outputStart">
				<div class="shopping-cart-error-state">
					<div class="shopping-cart-error-state-header row">
						<div class="error-state-exclamation-lg img-responsive col-md-2"></div>
						<div class="shopping-cart-error-state-header-text col-md-10">
							<fmt:message key="shoppingcart.oops.issue" />
							<div class="shopping-cart-error-state-subheader">
				</dsp:oparam>
				<dsp:oparam name="output">
					<dsp:valueof param="message" valueishtml="true" />
					<br>
				</dsp:oparam>
				<dsp:oparam name="outputEnd">
					</div>
					</div>
					</div>
					</div>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>
