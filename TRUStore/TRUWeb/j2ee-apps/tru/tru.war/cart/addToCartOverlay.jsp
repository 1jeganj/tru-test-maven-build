<dsp:page>

<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:importbean bean="/com/tru/commerce/droplet/TRUPriceDroplet" />


<dsp:getvalueof var="customerEmail" bean="Profile.login"/>
<dsp:getvalueof var="customerID" bean="Profile.id"/>
<dsp:getvalueof var="productId" param="productId"/>
<dsp:getvalueof var="pageFrom" param="pageFrom"/>

<input type="hidden" id="cartAddSource" value="${pageFrom}"/> 

		<dsp:droplet name="/com/tru/commerce/droplet/TRUProductInfoLookupDroplet">
			<dsp:param name="productId" value="${productId}"/>
			<dsp:param name="siteId" bean="/atg/multisite/Site.id"/>
			<dsp:oparam name="output">
			<dsp:getvalueof param="productInfo.defaultSKU.brandName" var="brandName"/>
		        <dsp:getvalueof param="productInfo.displayName" var="productName"/>
		        <dsp:getvalueof param="productInfo.defaultSKU.id" var="productSku"/>
			</dsp:oparam>
		</dsp:droplet>
		
		<dsp:droplet name="TRUPriceDroplet">
			<dsp:param name="skuId" value="${productSku}" />
			<dsp:param name="productId" value="${productId}" />
			<dsp:oparam name="true">
				<dsp:getvalueof param="salePrice" var="productUnitPrice" />
				<dsp:getvalueof param="listPrice" var="productListPrice" />
				<dsp:getvalueof param="savedAmount" var="productDiscount" />
			</dsp:oparam>
			<dsp:oparam name="false">
				<dsp:getvalueof param="salePrice" var="productUnitPrice" />
				<dsp:getvalueof param="listPrice" var="productListPrice" />
						<c:if test="${productUnitPrice == 0}">
							<c:set var="productUnitPrice" value="${productListPrice}"/>
						</c:if>
			</dsp:oparam>
		</dsp:droplet>

	<c:set var="singleQuote" value="'" />
	<c:set var="doubleQuote" value='"' />
	<c:set var="slashSingleQuote" value="\'" />
	<c:set var="slashDoubleQuote" value='\"' />

	<c:if test="${fn:contains(productBrand,singleQuote)}">
	     <c:set var="productBrand" value="${fn:replace(productBrand,singleQuote,slashSingleQuote)}"/>
		 </c:if>
		 <c:if test="${fn:contains(productBrand,doubleQuote)}">
	     <c:set var="productBrand" value="${fn:replace(productBrand,doubleQuote,slashDoubleQuote)}"/>
		 </c:if>
		 
		 <c:if test="${fn:contains(productName,singleQuote)}">
	     <c:set var="productName" value="${fn:replace(productName,singleQuote,slashSingleQuote)}"/>
		 </c:if>
		 <c:if test="${fn:contains(productName,doubleQuote)}">
	     <c:set var="productName" value="${fn:replace(productName,doubleQuote,slashDoubleQuote)}"/>
		 </c:if>
		 
	 <dsp:droplet name="/com/tru/commerce/droplet/TRUProductCategoryInfoDroplet">
	 	<dsp:param name="productId" value="${productId}" />
	 	<dsp:param name="selectedSkuId" value="${productSku}" />	 	
		<dsp:oparam name="output">
			<dsp:getvalueof param="onlinePidSku" var="onlinePID"/>
		</dsp:oparam>
	 </dsp:droplet>
	 <dsp:getvalueof var="productQueryId" value="${onlinePID}" />
	 
		 <c:if test="${onlinePID eq ''}">
			<dsp:getvalueof var="productQueryId" value="${productId}" />
		</c:if>
		 
	<div class="modal-dialog modal-lg">
		<div class="modal-content sharp-border">
			<div class="shopping-cart-overlay">
				<dsp:include page="/cart/addToCartOverlayContent.jsp" />
			</div>
		</div>
	</div>
	<script>
	
		function loadOmniScriptQuickView(e) {
			var qty = $("#ItemQty").val();
			utag.link({
				event_type : 'cart_add',
				customer_email : "${customerEmail}",
				customer_id : "${customerID}",
				product_brand : ["${productBrand}"],
				product_category : [],
				product_id : ["${productQueryId}"],
				product_name : ["${productName}"],
				product_unit_price : ["${productUnitPrice}"],
				product_list_price : ["${productListPrice}"],
				product_quantity : [qty],
				product_sku : ["${productSku}"],
				product_subcategory : [],
				product_finding_method : [''],
				//product_discount : [''],
				product_store : ['PRODUCT STORE'],
				cart_addition_source : [e],
				product_merchandising_category : [''],
				partner_name : 'toysrus'
			});
		}
	</script>
</dsp:page>
