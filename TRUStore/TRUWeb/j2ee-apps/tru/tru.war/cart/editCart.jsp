<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPProdMerchCategory" var="productMerchandisingCategory"/>
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:getvalueof var="relationshipItemId" param="relationshipItemId" />
<dsp:getvalueof var="metaInfoId" param="metaInfoId" />
<dsp:getvalueof var="commItemId" param="commerceItemId"/>
<dsp:getvalueof var="pageName" param="pageName" />
<dsp:getvalueof var="gWPPromotionItem" param="gWPPromotionItem"/>
<dsp:getvalueof var="itemColor" param="itemColor"/>
<dsp:getvalueof var="itemSize" param="itemSize"/>
<dsp:getvalueof var="productId" param="productId"/>
<dsp:getvalueof var="wrapSkuId" param="wrapSkuId"/>


<input type="hidden" id="pMCategory" value="${productMerchandisingCategory}"/>
<%-- <dsp:droplet name="Switch">
	<dsp:param name="value" param="fromAjaxEditCart"/>
	<dsp:oparam name="true"> --%>
		<dsp:droplet name="/com/tru/commerce/cart/droplet/EditCartItemDetailsDroplet">
			<dsp:param name="order" bean="ShoppingCart.current" />
			<dsp:param name="relationShipId" value="${relationshipItemId}"/>
			<dsp:param name="metaInfoId" value="${metaInfoId}"/>
			<dsp:oparam name="output">
				<dsp:getvalueof var="item" param="item"/>
				<dsp:getvalueof var="commItemId" param="item.commerceItemId"/>
				<dsp:getvalueof var="onlinePid" param="item.skuItem.onlinePid"/>
			</dsp:oparam>
		</dsp:droplet>
		<dsp:getvalueof var="productInfo" param="productInfo"/>
		<c:choose>
			<c:when test="${empty productInfo}">
				<dsp:droplet name="/com/tru/commerce/droplet/TRUProductInfoLookupDroplet">
				<dsp:param name="productId" value="${productId}"/>
				<dsp:param name="selectedColor" value="${item.color}"/>
				<dsp:param name="selectedSize" value="${item.size}"/>
				<dsp:param name="selectedSkuId" value="${item.skuId}"/>
				
					<dsp:oparam name="output">
						<dsp:getvalueof var="productInfo" param="productInfo"/>
						<dsp:getvalueof var="productVoJson" param="productVoJson"/>
					</dsp:oparam>
				</dsp:droplet>
			</c:when>
		</c:choose>
		<dsp:droplet name="/com/tru/utils/TRUPdpURLDroplet">
			<%-- <dsp:param name="productId" value="${productId}" /> --%>
			<dsp:param name="productId" value="${onlinePid}" />
			<dsp:param name="siteId" value="${item.siteId}"/>
			<dsp:oparam name="output">
				<dsp:getvalueof var="productPageUrl" param="productPageUrl" />
			</dsp:oparam>
		</dsp:droplet>
		<dsp:droplet name="Switch">
			<dsp:param name="value" param="fromAjaxEditCart" />
			<dsp:oparam name="true">
				<dsp:getvalueof var="skuId" value="${productInfo.defaultSKU.id}" />
				<dsp:getvalueof var="selectedColorId" param="selectedColor"/>
				<dsp:getvalueof var="selectedSize" param="selectedSize"/>
			</dsp:oparam>
			<dsp:oparam name="default">
				<dsp:getvalueof var="skuId" value="${item.skuId}"/>
				<dsp:getvalueof var="selectedColorId" value="${item.color}"/>
				<dsp:getvalueof var="selectedSize" value="${item.size}"/>
			</dsp:oparam>
		</dsp:droplet>
	<%-- </dsp:oparam>
	<dsp:oparam name="default">
		<dsp:getvalueof var="item" param="item"/>
		<dsp:getvalueof var="selectedColorId" param="item.color"/>
		<dsp:getvalueof var="selectedSize" param="item.size"/>
		<dsp:getvalueof var="skuId" param="item.skuId"/>
	</dsp:oparam>
</dsp:droplet> --%>
<span class="clickable pull-right">
				<a href="javascript:void(0)"  data-dismiss="modal">
					<span class="sprite-icon-x-close close-modal close_edit close-edit pull-right"></span>
	        	</a>
       		</span>
			<c:choose>
				<c:when test="${pageName eq 'Review'}">
				  <h2><dsp:valueof value="${item.displayName}" /></h2>
				</c:when>
				<c:otherwise>
				  <h2><dsp:a href="${productPageUrl}"><dsp:valueof value="${item.displayName}" /></dsp:a></h2>
				</c:otherwise>
			</c:choose>

	<div class="cart-edit-popup">
<div class="row">
	        
            <%-- Commented product ratings code
            <div class="star-rating-on"></div>
            <div class="star-rating-on"></div>
            <div class="star-rating-on"></div>
            <div class="star-rating-on"></div>
            <div class="star-rating-off"></div>
            --%>
        <div class="col-md-5">
	        <div class="cart-edit-popup-image">
	            <dsp:getvalueof var="productImage" value="${item.productImage}"/>
				<dsp:droplet name="IsEmpty">
					<dsp:param name="value" value="${productImage}" />
					<dsp:oparam name="false">
						<dsp:droplet name="/com/tru/common/droplet/TRUAkamaiImageDroplet">
							<dsp:param name="imageName" value="${productImage}" />
							<dsp:param name="imageHeight" value="385" />
							<dsp:param name="imageWidth" value="385" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="akamaiImageUrl" param="akamaiUrl" />
							</dsp:oparam>
						</dsp:droplet>
						<c:choose>
						  <c:when test="${pageName eq 'Review'}">
						  	<img class="shopping-cart-product-edit-image" src="${akamaiImageUrl}">
						  </c:when>
						  <c:otherwise>
						  	<dsp:a href="${productPageUrl}">
								<img class="shopping-cart-product-edit-image" src="${akamaiImageUrl}">
						    </dsp:a>
						  </c:otherwise>
						</c:choose>
					</dsp:oparam>
					<dsp:oparam name="true">
						<c:choose>
							<c:when test="${pageName eq 'Review'}">
								<img class="shopping-cart-product-edit-image" src="${TRUImagePath}images/no-image500.gif">
							</c:when>
							<c:otherwise>
								<dsp:a href="${productPageUrl}">
									<img class="shopping-cart-product-edit-image" src="${TRUImagePath}images/no-image500.gif">
								</dsp:a>
							</c:otherwise>
						</c:choose>
					</dsp:oparam>
				</dsp:droplet>
	        </div>
        </div>
        <div class="col-md-7 product-information" id="editCartId-${productId}">
            <div class="buy-1-get-free">
				<dsp:include page="/pricing/displayPromotions.jsp">
					<dsp:param name="item" value="${item}"/>
					<dsp:param name="fromEdit" value="true"/>
				</dsp:include>
			</div>
			<dsp:getvalueof var="displayStrikeThrough" value="${item.displayStrikeThrough}" />
			 <c:choose>
				<c:when test="${displayStrikeThrough}">
					<c:choose>
						<c:when test="${pageName eq 'Review' }">
							<span class="product-description-price inline editCartPrice price">
							<span class="crossed-out-price price-save"><dsp:valueof value="${item.price}" converter="currency" /></span>
							<span><dsp:valueof value="${item.salePrice}" converter="currency" /></span>
		
							<span class="price-save">&nbsp;.&nbsp;</span><span class="price-save save-text">&#32; <fmt:message key="shoppingcart.you.save" />&#32;<dsp:valueof
									value="${item.savedAmount}" locale="en_US" converter="currency"/><fmt:message key="shoppingcart.left.bracket" /><dsp:valueof value="${item.savedPercentage}" converter="number" format="##"/><fmt:message key="shoppingcart.percentage" /><fmt:message key="shoppingcart.right.bracket" />
							</span>
							</span>
						</c:when>
						<c:otherwise>
							<dsp:a href="${productPageUrl}">
								<span class="product-description-price inline editCartPrice price">
								<span class="crossed-out-price price-save"><dsp:valueof value="${item.price}" converter="currency" /></span>
								<span><dsp:valueof value="${item.salePrice}" converter="currency" /></span>
		
								<span class="price-save">&nbsp;.&nbsp;</span><span class="price-save save-text">&#32; <fmt:message key="shoppingcart.you.save" />&#32;<dsp:valueof
									value="${item.savedAmount}" locale="en_US" converter="currency"/><fmt:message key="shoppingcart.left.bracket" /><dsp:valueof value="${item.savedPercentage}" converter="number" format="##"/><fmt:message key="shoppingcart.percentage" /><fmt:message key="shoppingcart.right.bracket" />
								</span>
								</span>
							</dsp:a>
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${pageName eq 'Review' }">
							<div class="product-description-price inline editCartPrice singlePriceOnly"><span><dsp:valueof value="${item.salePrice}" converter="currency" /></span></div>
						</c:when>
						<c:otherwise>
							<dsp:a href="${productPageUrl}">
								<div class="product-description-price inline editCartPrice singlePriceOnly"><span><dsp:valueof value="${item.salePrice}" converter="currency" /></span></div>
							</dsp:a>
						</c:otherwise>
					</c:choose>
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test="${pageName ne 'Review' && pageName ne 'store-pickup' && pageName ne 'Shipping' &&  (not empty selectedColorId || not empty selectedSize)}">
					<div id="colorSizeVariants" class="sticky-price-contents color-selector">
						<dsp:include page="/jstemplate/colorSizeVariantFragment.jsp">
							<dsp:param name="productInfo" value="${productInfo}" />
							<dsp:param name="productVoJson" value="${productVoJson}" />
							<dsp:param name="selectedColorId" value="${selectedColorId}" />
							<dsp:param name="selectedSize" value="${selectedSize}" />
						</dsp:include>
						<input type="hidden" class="pageName" value="editCart" /> 
						<input type="hidden" value="${productId}" id="hiddenProductIdEditCart" />
						<input type="hidden" value="${relationshipItemId}" id="relationshipItemId" /> 
						<input type="hidden" value="${commItemId}" id="commerceItemId" />
					</div>
				</c:when>
			</c:choose>
			<c:if test="${pageName eq 'Review' or pageName eq 'store-pickup' or pageName eq 'Shipping'}">
				<c:if test="${not empty item.color}">
					<div class="color-selector">
						<fmt:message key="shoppingcart.item.color" />&#32;
						${item.color}
					</div>
				</c:if>
				<c:if test="${not empty item.size}">
		            <div class="size-selector">
		            	<fmt:message key="shoppingcart.item.size" />&#32;
		            	${item.size}
		            </div>
	            </c:if>
	        </c:if>
			<div class="quantity">
                <div class="color-text">
                    <fmt:message key="shoppingcart.item.qty" />
                </div>
                <div class="qtys-text errorDisplay display-none">
                	<fmt:message key="shoppingcart.qty.error.message" />
                </div>
                <div class="qtys-valid-text errorDisplay display-none">
                	<fmt:message key="shoppingcart.qty.empty.message" />
                </div>
                <dsp:droplet name="IsEmpty">
					<dsp:param name="value" value="${item.relItemQtyInCart}" />
					<dsp:oparam name="false">
						<div class="quantity">
							<c:choose>
								<c:when test="${pageName eq 'Shipping'}">
									<dsp:getvalueof var="qty" value="1"/>
								</c:when>
								<c:otherwise>
									<dsp:getvalueof var="qty" value="${item.quantity}"/>
								</c:otherwise>
							</c:choose>
							<dsp:getvalueof var="qtyLimit" value="${item.customerPurchaseLimit}" />
							<dsp:getvalueof var="maxItemQty" bean="/com/tru/common/TRUConfiguration.maximumItemQuantity"/>
							<c:if test="${qtyLimit eq 0}">
								<dsp:getvalueof var="qtyLimit" value="${maxItemQty}" />
							</c:if>
							<dsp:getvalueof var="itemQtyInCart" value="${item.itemQtyInCart}" />
							<div class="stepper editCart" id="${index}" relationshipItemId="${relationshipItemId}" quantityLimitVal="${qtyLimit}">
							
							<dsp:droplet name="Switch">
							<dsp:param name="value" value="${gWPPromotionItem}" />
								<dsp:oparam name="true">
									 <c:choose>
				    					<c:when test="${not empty item.color and not empty item.size}">
				    					<c:choose>
				    						<c:when test="${itemColor eq item.color and itemSize eq item.size}">
				    							<a href="javascript:void(0)" class="decrease disabled cart" title="decrease count"></a>
												<input type="text" class="counter center lineItemQtyEditCart" disabled="disabled" contenteditable="true" maxlength="3" value="${qty}" aria-label="Item Count">
												<a href="javascript:void(0)" class="increase disabled cart" title="increase count"></a>
				    						</c:when>
				    						<c:otherwise>
				    							<c:choose>
													<c:when test="${1 eq qty and itemQtyInCart < qtyLimit}">
														<a href="javascript:void(0)" class="decrease disabled cart" title="decrease count"></a>
														<input type="text" class="counter center lineItemQtyEditCart" contenteditable="true" onkeypress="return isNumberKey(event)" maxlength="3" value="${qty}" aria-label="Item Count">
														<a href="javascript:void(0)" class="increase enabled cart" title="increase count"></a>
													</c:when>
													<c:when test="${qtyLimit eq qty or qtyLimit eq itemQtyInCart}">
														<dsp:droplet name="Switch">
															<dsp:param name="value" value="${qty}"/>
															<dsp:oparam name="1">
																<a href="javascript:void(0)" class="decrease disabled cart" title="decrease count"></a>
																<input type="text" class="counter center lineItemQtyEditCart" contenteditable="true" onkeypress="return isNumberKey(event)" maxlength="3" value="${qty}" aria-label="Item Count">
																<a href="javascript:void(0)" class="increase disabled cart" title="increase count"></a>
															</dsp:oparam>
															<dsp:oparam name="default">
																<a href="javascript:void(0)" class="decrease enabled cart" title="decrease count"></a>
																<input type="text" class="counter center lineItemQtyEditCart" contenteditable="true" onkeypress="return isNumberKey(event)" maxlength="3" value="${qty}" aria-label="Item Count">
																<a href="javascript:void(0)" class="increase disabled cart" title="increase count"></a>
															</dsp:oparam>
														</dsp:droplet>
													</c:when>
													<c:otherwise>
														<a href="javascript:void(0)" class="decrease enabled cart" title="decrease count"></a>
														<input type="text" class="counter center lineItemQtyEditCart" contenteditable="true" onkeypress="return isNumberKey(event)" maxlength="3" value="${qty}" aria-label="Item Count">
														<a href="javascript:void(0)" class="increase enabled cart" title="increase count"></a>
													</c:otherwise>
												</c:choose>
				    						</c:otherwise>
				    					</c:choose>
				    					</c:when>
				    					<c:otherwise>
				    						<a href="javascript:void(0)" class="decrease disabled cart" title="decrease count"></a>
											<input type="text" class="counter center lineItemQtyEditCart" disabled="disabled" contenteditable="true" maxlength="3" value="${qty}" aria-label="Item Count">
											<a href="javascript:void(0)" class="increase disabled cart" title="increase count"></a>
				    					</c:otherwise>
				    				</c:choose>
								</dsp:oparam>
								<dsp:oparam name="false">
								<c:choose>
									<c:when test="${1 eq qty and itemQtyInCart < qtyLimit}">
										<a href="javascript:void(0)" class="decrease disabled cart" title="decrease count"></a>
										<input type="text" class="counter center lineItemQtyEditCart" contenteditable="true" onkeypress="return isNumberKey(event)" maxlength="3" value="${qty}" aria-label="Item Count">
										<a href="javascript:void(0)" class="increase enabled cart" title="increase count"></a>
									</c:when>
									<c:when test="${qtyLimit eq qty or qtyLimit eq itemQtyInCart}">
										<dsp:droplet name="Switch">
											<dsp:param name="value" value="${qty}"/>
											<dsp:oparam name="1">
												<a href="javascript:void(0)" class="decrease disabled cart" title="decrease count"></a>
												<input type="text" class="counter center lineItemQtyEditCart" contenteditable="true" onkeypress="return isNumberKey(event)" maxlength="3" value="${qty}" aria-label="Item Count">
												<a href="javascript:void(0)" class="increase disabled cart" title="increase count"></a>
											</dsp:oparam>
											<dsp:oparam name="default">
												<a href="javascript:void(0)" class="decrease enabled cart" title="decrease count"></a>
												<input type="text" class="counter center lineItemQtyEditCart" contenteditable="true" onkeypress="return isNumberKey(event)" maxlength="3" value="${qty}" aria-label="Item Count">
												<a href="javascript:void(0)" class="increase disabled cart" title="increase count"></a>
											</dsp:oparam>
										</dsp:droplet>
									</c:when>
									<c:otherwise>
										<a href="javascript:void(0)" class="decrease enabled cart" title="decrease count"></a>
										<input type="text" class="counter center lineItemQtyEditCart" contenteditable="true" onkeypress="return isNumberKey(event)" maxlength="3" value="${qty}" aria-label="Item Count">
										<a href="javascript:void(0)" class="increase enabled cart" title="increase count"></a>
									</c:otherwise>
								</c:choose>
								</dsp:oparam>
							</dsp:droplet>
							</div>
							<dsp:getvalueof var="qtyLimitVal" value="${item.customerPurchaseLimit}" />
							<c:if test="${qtyLimitVal ne 0}">
								<span><fmt:message key="shoppingcart.item.limit" />&nbsp;${qtyLimitVal}&nbsp;<fmt:message key="shoppingcart.item.limit.customer" /></span>
							</c:if>
						</div>
					</dsp:oparam>
				</dsp:droplet>
            </div>

			<input type="hidden" value="${pageName}" id="pagenameIdEdit" />
			<input type="hidden" value="${relationshipItemId}" id="relationshipItemIdEdit" />
			<input type="hidden" value="${commItemId}" id="commItemIdEdit" />
			<input type="hidden" value="${wrapSkuId}" id="wrapSkuIdEdit" />
			<input type="hidden" value="${item.relItemQtyInCart-item.quantity}" id="QuantityEdit" />
			
            <c:choose>
		    	<c:when test="${not empty item.color and not empty item.size}">
		    		<dsp:droplet name="IsEmpty">
		            	<dsp:param name="value" value="${selectedSize}" />
		            	<dsp:oparam name="false">
		            		<c:choose>
		            			<c:when test="${gWPPromotionItem eq true and itemColor eq item.color and itemSize eq item.size}">
				            		<button type="submit" disabled="disabled" class="cart-edit-update fade-add-to-cart" onclick="javascript: return editCartItemInShipping('${productInfo.productId}','${skuId}','${commItemId}','${relationshipItemId}','${wrapSkuId}');">update</button>
				            	</c:when>
				            	<c:when test="${pageName eq 'Review' or pageName eq 'store-pickup'}">
				            		<button type="submit" class="cart-edit-update" onclick="javascript: return editCartItemInReview('${productInfo.productId}','${skuId}','${commItemId}','${relationshipItemId}','${wrapSkuId}', '${item.relItemQtyInCart-item.quantity}');">update</button>
				            	</c:when>
				            	
				            	<c:when test="${pageName eq 'Shipping'}">
				            		<button type="submit" class="cart-edit-update" onclick="javascript: return editCartItemInShipping('${productInfo.productId}','${skuId}','${commItemId}','${relationshipItemId}','${wrapSkuId}');">update</button>
				            	</c:when>
				            	<c:otherwise>
				            		<button type="submit" class="cart-edit-update" onclick="javascript: return editCartItem('${productInfo.productId}','${skuId}','${commItemId}','${relationshipItemId}', '${item.relItemQtyInCart-item.quantity}','${contextPath}');">update</button>
				            	</c:otherwise>
				            </c:choose>
		            	</dsp:oparam>
		            	<dsp:oparam name="true">
		            		<c:choose>
				            	<c:when test="${pageName eq 'Review' or pageName eq 'store-pickup'}">
				            		<button type="submit" disabled="disabled" class="cart-edit-update fade-add-to-cart" onclick="javascript: return editCartItemInReview('${productInfo.productId}','${skuId}','${commItemId}','${relationshipItemId}','${wrapSkuId}', '${item.relItemQtyInCart-item.quantity}');">update</button>
				            	</c:when>
				            	
				            	<c:when test="${pageName eq 'Shipping'}">
				            		<button type="submit" disabled="disabled" class="cart-edit-update fade-add-to-cart" onclick="javascript: return editCartItemInShipping('${productInfo.productId}','${skuId}','${commItemId}','${relationshipItemId}','${wrapSkuId}');">update</button>
				            	</c:when>
				            	<c:otherwise>
				            		<button type="submit" disabled="disabled" class="cart-edit-update fade-add-to-cart" onclick="javascript: return editCartItem('${productInfo.productId}','${skuId}','${commItemId}','${relationshipItemId}', '${item.relItemQtyInCart-item.quantity}','${contextPath}');">update</button>
				            	</c:otherwise>
				            </c:choose>
		            	</dsp:oparam>
		            </dsp:droplet>
		    	</c:when>
		    	<c:otherwise>
		    		<c:choose>
		    			<c:when test="${gWPPromotionItem eq true}">
		            		<button type="submit" disabled="disabled" class="cart-edit-update fade-add-to-cart" onclick="javascript: return editCartItemInShipping('${productInfo.productId}','${skuId}','${commItemId}','${relationshipItemId}','${wrapSkuId}');">update</button>
		            	</c:when>
		            	<c:when test="${pageName eq 'Review' or pageName eq 'store-pickup'}">
		            		<button type="submit" class="cart-edit-update" onclick="javascript: return editCartItemInReview('${productInfo.productId}','${skuId}','${commItemId}','${relationshipItemId}','${wrapSkuId}', '${item.relItemQtyInCart-item.quantity}');">update</button>
		            	</c:when>
		            	<c:when test="${pageName eq 'Shipping'}">
		            		<button type="submit" class="cart-edit-update" onclick="javascript: return editCartItemInShipping('${productInfo.productId}','${skuId}','${commItemId}','${relationshipItemId}','${wrapSkuId}');">update</button>
		            	</c:when>
		            	<c:otherwise>
		            		<button type="submit" class="cart-edit-update" onclick="javascript: return editCartItem('${productInfo.productId}','${skuId}','${commItemId}','${relationshipItemId}', '${item.relItemQtyInCart-item.quantity}','${contextPath}');">update</button>
		            	</c:otherwise>
		            </c:choose>
		    	</c:otherwise>
		    </c:choose>
            
            
           <!--  <div class="available-online">
                <div class="green-list-dot inline"></div>
                available online &amp; in store <span>�</span>
                <div class="small-blue inline">
                    <button class="learn-more" data-toggle="modal" data-target="#notifyMeModal">learn more</button>
                </div>
            </div> -->
          <c:if test="${pageName ne 'Review' }">
            <div class="blue-link-parent">
            	<dsp:a href="${productPageUrl}" class="blue-link"><fmt:message key="shoppingcart.view.product.description" /></dsp:a>
            </div>
         </c:if>
        </div>
</div>
</div>

</dsp:page> 