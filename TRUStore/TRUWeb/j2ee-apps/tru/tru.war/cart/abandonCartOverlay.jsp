<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/commerce/promotion/CouponFormHandler" />
<dsp:importbean bean="/atg/registry/RepositoryTargeters/TRU/ShoppingCart/AbandonCartPromotionalMessageTargeter"/>
<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
<dsp:getvalueof var="locale" bean="Profile.locale" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<div class="modal-dialog">
	<div class="modal-content sharp-border welcome-back-overlay">
		<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
		<h3 class="w3ValidationWelcomeBack"><fmt:message key="abandonCart.welcomeBack" /></h3>
		<p class="welcomeEmail" id="welcomeEmail"></p>
		
		<p class="abandonCartReminder"><fmt:message key="abandonCart.reminder" /><!-- We noticed you left items in your cart. Would you like to review them now? -->
		<dsp:droplet name="/atg/targeting/TargetingForEach">
			<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/ShoppingCart/AbandonCartPromotionalMessageTargeter"/>
			<dsp:oparam name="output">
				<dsp:valueof param="element.data" valueishtml="true" />
			</dsp:oparam>
		</dsp:droplet>
		</p>
		
		<div class="row welcome-input-group">
			<a class="submitViewCart" onclick="applyAbandonCartPromotion();"><fmt:message key="abandonCart.viewCart" /><!-- view cart --></a>
			<a href="javascript:void(0);" class="previous-page" onclick="handleAbandonPreviousPage();"><fmt:message key="abandonCart.continueShopping" /></a>
		</div>
		
		<div class="welcome-checkbox-group">
			<span class="checkbox-sm-off"></span><span class=""><fmt:message key="abandonCart.dontShowMessage" /><!-- Don't show me this message again. --></span>
		</div>
	</div>
</div>