<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
<dsp:importbean bean="/com/tru/commerce/cart/droplet/CartItemDetailsDroplet" />
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:getvalueof bean="Profile.siteStatus" var="siteStatus"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof var="contextPath" value="${originatingRequest.contextPath}"/>
<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id" />
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPProdMerchCategory" var="productMerchandisingCategory"/>

	<dsp:droplet name="/com/tru/droplet/TRUGetSiteDroplet">
		<dsp:param name="siteId" value="${siteName}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="siteUrl" param="siteUrl"/> 
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
		<dsp:oparam name="output">
		<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="fullyQualifiedcontextPathUrl" value="${scheme}${siteUrl}${contextPath}"/>
	
	 <dsp:droplet name="CartItemDetailsDroplet">
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:oparam name="output">
			<dsp:getvalueof param="items" var="items" />
		</dsp:oparam>
	</dsp:droplet>
	<c:set var="index" value="0" />
	<c:set var="tealCount" value="0" />
	<c:forEach var="element" items="${items}" >
		<dsp:setvalue param="productId" value="${element.productId}" />
		<c:set var="tealCount" value="${tealCount + 1}" />
		 
		 <dsp:droplet name="/com/tru/commerce/droplet/TRUProductCategoryInfoDroplet">
		 	<dsp:param name="productId" value="${element.productId}" />
		 	<dsp:param name="selectedSkuId" value="${element.skuId}" />	 	
			<dsp:oparam name="output">
				<dsp:getvalueof param="onlinePidSku" var="onlinePID"/>
			</dsp:oparam>
		 </dsp:droplet>
		 
		<c:choose>
			<c:when test="${items.size() gt '1' && index lt (items.size()-1)}">
				<c:set var="productID" value="${productID}'${onlinePID}'," />
				<c:set var="index" value="${index+1}" />
			</c:when>
			<c:otherwise>
				<c:set var="productID" value="${productID}'${onlinePID}'" />
				<c:set var="index" value="${index+1}" />
			</c:otherwise>
		</c:choose>
	</c:forEach>
	<dsp:a href="${fullyQualifiedcontextPathUrl}cart/shoppingCart.jsp" title="shopping Cart" data-ab-link="Shopping-Bag">
	<div class="global-nav-cart">
        <p class="mincart-section"><span id="ajaxCount" class="sprite sprite-cart-icon"></span></p>
        <c:if test="${siteStatus != 'sos'}">
        	<div id="showProgressBar">
			<span id="qualifyPromotion">
			<span><fmt:message key="shoppingcart.free.shipping" /></span>
			</span>
			
			<span id="notQualifyPromotion">
			<fmt:message key="shoppingcart.free.shipping.add" /><span id="amountToQualify"></span> <fmt:message key="shoppingcart.for.free.shipping" />
			</span>
			
			<div class="cart-progress-bar summary-block-free-shipping-bar">
             </div>
			</div>
        </c:if>
    </div>
 </dsp:a>
<script>
function openCartOmniScript(){
	var totalItems = '${tealCount}';
	
	if(typeof utag !='undefined' && totalItems == 1){
		try{
			utag.link({
			    event_type: 'cart_open',
			    product_id: [${productID}],
			    product_merchandising_category: [' ']
			});
		}catch(e){}
	}
} 
</script>
</dsp:page>