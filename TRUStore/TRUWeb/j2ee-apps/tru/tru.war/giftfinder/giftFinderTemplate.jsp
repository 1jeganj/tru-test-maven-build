<%-- <dsp:page>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations" />
<dsp:getvalueof var="noOfGFRecentsearches" bean="EndecaConfigurations.noOfGFRecentsearches" />
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
 <dsp:getvalueof var="contextPath"	value="${originatingRequest.contextPath}" />
 <dsp:getvalueof var="keysArray"	value="" />
 <dsp:getvalueof var="giftFinderURL" bean="Profile.giftFinderURL"/>
 <dsp:getvalueof var="transient" bean="Profile.transient"/>
  <dsp:importbean bean="/com/tru/commerce/droplet/TRUGiftfinderInterestZoneDroplet"/>
 <fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />

	<dsp:droplet name="/com/tru/commerce/droplet/TRUGiftFinderDroplet">
		<dsp:oparam name="output">
			<dsp:getvalueof param="element" var="cookArray" />
		</dsp:oparam>
	</dsp:droplet>


	<div class="gift-finder-intro">
						<div class="gift-finder-hero"></div>
						<div class="gift-finder-scroll-content">
						<p class="header"><fmt:message key="tru.giftFinder.label.header" /></p>
						<button class="findNewItems"><fmt:message key="tru.giftFinder.label.findSomeone" /></button>
						
						
						<c:if test="${cookArray.size() > 0 or giftFinderURL.size() > 0}">
						
						 <div class="gift-finder-recent-searches">recent searches 
							<ul id="recentSearchList">
							<c:choose>
							<c:when test="${transient eq false}">
							<c:forEach var="entry" items="${giftFinderURL}" begin="0" end="${noOfGFRecentsearches-1}">
							<li>
								<a href="${contextPath}${entry.value}">${entry.key}</a>
							</li>
                               </c:forEach>
							</c:when>
							<c:otherwise>

							<c:forEach var="entry" items="${cookArray}" begin="0" end="${noOfGFRecentsearches-1}">

								<li><a href="${contextPath}${entry.value}">${entry.key}</a>
								</li>
							</c:forEach>
						</c:otherwise>
							</c:choose>
								<li>
								<a id="clear-recent-searches" href="#">clear all</a></li>
							
							</ul>
						</div>
					
					</c:if>
					
					
					
					
					
					
					
					</div>
					</div>
			<div class="gift-finder-hero">
				<a href="javascript:void(0)"> back</a>
			</div>
			<form id="giftFinderForm" name="giftFinderForm">
				<div class="tse-scrollable gift-finder-wrapper">

					<div class="tse-content">


						<div class="gift-finder-container">
							<div>
								<div class="gift-finder-spacer">
									<div class="gift-finder-name-zone">
										<label for="giftFinderName"><fmt:message key="tru.giftFinder.label.nameGiftFinder" /></label>
										<input type="text" id="giftFinderName"
											placeholder="e.g. Kaitlyn's birthday" name="keyword">
											<div id="errorGiftFinderDuplicate" class="errorMessage">Already Exists</div>
									</div>
									<!-- <input type="hidden" id=giftFinderNameURL value="http://localhost:7001/tru/product/productDetails.jsp"> -->
								</div>
								<div class="gift-finder-spacer">
									<div class="gift-finder-shopping-for-zone">
										<fmt:message key="tru.giftFinder.label.whoYouShoppingFor" />
										<hr>
										<div class="gift-finder-gender">
											<div class="gift-finder-shopping-for-gender-label"><fmt:message key="tru.giftFinder.label.gender" /></div>
											<div class="gift-finder-shopping-for-gender">
												<input id="female" type="radio" name="gender" value="female">
												<label for="female"><fmt:message key="tru.giftFinder.label.female" /></label> <input id="male"
													type="radio" name="gender" value="male"> <label
													for="male"><fmt:message key="tru.giftFinder.label.male" /></label> <input id="both" type="radio"
													name="gender" value="both"> <label for="both"><fmt:message key="tru.giftFinder.label.both" /></label>
											</div>
										</div>
										<br>
										<div class="gift-finder-age">
											<div class="gift-finder-shopping-for-age-label"><fmt:message key="tru.giftFinder.label.age" /></div>
											<div class="gift-finder-shopping-for-age">
												<div class="age-min-image"></div>
												<div id="age-slider"
													class="ui-slider ui-slider-horizontal ui-widget ui-widget-content">
													<div class="ui-slider-range ui-widget-header ui-corner-all"></div>
													<span
														class="ui-slider-handle ui-state-default ui-corner-all"
														tabindex="0"> </span><span
														class="ui-slider-handle ui-state-default ui-corner-all"
														tabindex="0"></span>
												</div>
												<div class="age-max-image"></div>
												<div id="minAge">0 years</div>
												<div id="maxAge"></div>
											</div>
											<input type="hidden" id="hiddenMinAge" value="0" /> <input
												type="hidden" id="hiddenMaxAge" value="0" />
										</div>
										<br>
										<div class="gift-finder-price">
											<div class="gift-finder-shopping-for-price-label">price
											</div>
											<div class="gift-finder-shopping-for-price">
												<div class="price-min-image"></div>
												<div id="price-slider"
													class="ui-slider ui-slider-horizontal ui-widget ui-widget-content">
													<div class="ui-slider-range ui-widget-header ui-corner-all"></div>
													<span
														class="ui-slider-handle ui-state-default ui-corner-all"
														tabindex="0"></span><span
														class="ui-slider-handle ui-state-default ui-corner-all"
														tabindex="0"></span>
												</div>
												<div class="price-max-image"></div>
												<div id="minPrice">$0</div>
												<div id="maxPrice"></div>
												<input type="hidden" id="hiddenMinPrice" name="minPrice"
													value="0" /> <input type="hidden" id="hiddenMaxPrice"
													name="maxPrice" value="0" />

											</div>
										</div>

									</div>
								</div>
								<dsp:droplet name="TRUGiftfinderInterestZoneDroplet">
									<dsp:oparam name="output">
										<dsp:getvalueof var="giftFinderStackList" param="giftFinderStackList" />
									</dsp:oparam>
								</dsp:droplet>
								
								
								<div class="gift-finder-interest-zone">
									<fmt:message key="tru.giftFinder.label.whatAreInterested" />
									<hr>
									<div class="gift-finder-options-container">
									<dsp:droplet name="/atg/dynamo/droplet/ForEach">
									<dsp:param name="array" value="${giftFinderStackList}"/>
									<dsp:oparam name="output">
									<dsp:getvalueof param="element" var="giftFinderStackItemElement"></dsp:getvalueof>
									<dsp:getvalueof param="element.id" var="giftFinderStackItemElementId"></dsp:getvalueof>
									<dsp:getvalueof param="count" var="count"></dsp:getvalueof>
									<dsp:getvalueof var="hideClass" value=""></dsp:getvalueof>
									<c:if test="${count >3}">
										<dsp:getvalueof var="hideClass" value="hide"></dsp:getvalueof>
									</c:if>
										<div class="gift-interest-option ${hideClass}">
											<div class="gift-interest-refresh">
												<div><fmt:message key="tru.giftFinder.label.showDifferentOption" /></div>
												<div></div>
											</div>
											<div class="interest-options inline">
											<dsp:getvalueof var="giftFinderStackItems" value="${giftFinderStackItemElement.giftFinderStackItem}"></dsp:getvalueof>
											<dsp:droplet name="/atg/dynamo/droplet/ForEach">
											<dsp:param name="array" value="${giftFinderStackItems}"/>
												<dsp:oparam name="output">
												<dsp:getvalueof param="element" var="giftFinderStack"></dsp:getvalueof>
											<dsp:getvalueof param="count" var="count"></dsp:getvalueof>
											<dsp:getvalueof value="${giftFinderStack.name}" var="name"></dsp:getvalueof>
											<dsp:getvalueof value="${giftFinderStack.type}" var="type"></dsp:getvalueof>
											<dsp:getvalueof value="${giftFinderStack.nvalue}" var="nvalue"></dsp:getvalueof>
											<dsp:getvalueof value="${giftFinderStack.imageURL}" var="imageURL"></dsp:getvalueof>
												<div class="gift-finder-checkbox-container">
													<input class="gift-interest-selection" 
													     type="checkbox" name="${giftFinderStackItemElementId}[]" value="${nvalue}" id="${nvalue}_${giftFinderStackItemElementId}"> 
													<label class="gift-interest-selection-option" for="${nvalue}_${giftFinderStackItemElementId}">
														<img src="${imageURL}" alt="" />
														<span class="interestCheck"></span> 
														<span class="interest-filter"></span>
													</label>													
													<input type="hidden" data-id="gfit-finder-nvalue" value="${nvalue}" />
												</div>
												</dsp:oparam>
												</dsp:droplet>
											</div>
											<br> 
											<input id="both_${giftFinderStackItemElementId}" class="gift-interest-both" type="radio" name="interestOne"> 
											<label for="both_${giftFinderStackItemElementId}" class="gift-both"  tabindex="0"><fmt:message key="tru.giftFinder.label.both"/></label>
											<div class="gift-interest-or"></div>
										</div>	
										</dsp:oparam>
										</dsp:droplet>									
										<div class="gift-finder-load-more-interests text-center read-more" tabindex="0">load
											more</div>
									</div>
								</div>
								
								
								
							</div>
						</div>

					</div>


				</div>

				<div class="gift-finder-footer">
					<div class="gift-finder-button">
						<div>
							<!-- <button>find the perfect gift</button> -->
							<input type="hidden" id="finalvalue" value="" /> 
								<button type="button" id="giftSubmit"><fmt:message key="tru.giftFinder.label.findPerfectGift" /></button>
						</div>
						<div>
							<span class="gift-finder-number-available" id="giftFinderTotal">0</span>
							<br> <span class="gift-finder-copy"><fmt:message key="tru.giftFinder.label.giftsAvailable" /></span>
						</div>
					</div>
					<button id="clearGiftFinder" class="clearGiftFinderBtn">clear all searches</button>
				</div>
			</form>
</dsp:page> --%>