<dsp:page>
<dsp:importbean bean="/com/tru/commerce/giftfinder/GiftFinderFormHandler"/>
<dsp:getvalueof var="formId" param="formId" />

	<c:choose>
		<c:when test="${formId eq 'giftFinderName'}">
			<dsp:setvalue bean="GiftFinderFormHandler.giftFinderName" paramvalue="giftFinderName" />
			<dsp:setvalue bean="GiftFinderFormHandler.giftFinderNameURL" paramvalue="giftFinderUrl" />
			<dsp:setvalue bean="GiftFinderFormHandler.giftFinder" value="submit" />
		</c:when>
		<c:when test="${formId eq 'giftFinderKey'}">
			<dsp:setvalue bean="GiftFinderFormHandler.giftFinderName" paramvalue="fiderKey" />
			<dsp:setvalue bean="GiftFinderFormHandler.delete" value="submit" />
		</c:when>
		<c:when test="${formId eq 'clearAll'}">
			<dsp:setvalue bean="GiftFinderFormHandler.clearAll" value="submit" />
		</c:when>
	</c:choose>

</dsp:page>