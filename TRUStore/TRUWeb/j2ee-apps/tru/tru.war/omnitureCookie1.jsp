<dsp:page>
<dsp:importbean bean="/com/tru/droplet/OmnitureTrackingCookieDroplet"/>
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:getvalueof var="loginStatus" bean="Profile.transient" />

<dsp:droplet name="OmnitureTrackingCookieDroplet">
<dsp:param name="id" bean="Profile.id"/>
<dsp:param name="loginStatus" value="${loginStatus}" />
<dsp:oparam name="output">
</dsp:oparam>
</dsp:droplet>
<html>
<body>
	<script type="text/javascript" charset="UTF-8" src="${contextPath}javascript/omnitureTrackingCookie.js"></script>
</body>
</html>
</dsp:page>