<%@ include file="/includes/prelude.jspf"%>
<%@ tag language="java"%>
<%@ attribute name="checkoutOption"%>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:importbean bean="com/tru/common/TRUStoreConfiguration"/>
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:getvalueof var="staticAssetsVersionFormat" bean="TRUStoreConfiguration.staticAssetsVersion"/>
<dsp:getvalueof var="versionFlag" bean="TRUStoreConfiguration.versioningFlag"/>
<dsp:getvalueof var="requestUrl" bean="/OriginatingRequest.requestURL" />
<dsp:getvalueof var="quesryString" bean="/OriginatingRequest.queryString" />
<dsp:getvalueof value='<%=atg.nucleus.DynamoEnv.getProperty("staticAssetVersion")%>' var="versionNumbers"/>
<dsp:importbean bean="/com/tru/integrations/common/TRUHookLogicConfiguration"/>
<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
<dsp:importbean bean="/com/tru/radial/common/TRURadialConfiguration"/>
<dsp:getvalueof var="hooklogicURL" bean="TRUHookLogicConfiguration.hooklogicURL"/>
<dsp:getvalueof var="radialPaymentJSPath" bean="TRURadialConfiguration.radialPaymentJSPath"/>
<dsp:getvalueof var="siteNameIdentification" bean="/atg/multisite/Site.id"/>

<dsp:getvalueof var="enableMinification"  bean="TRUStoreConfiguration.enableJSDebug"/>
  <dsp:getvalueof var="debugFlag" param=="debug"/>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Toysrus.com, The Official Toys&#34;R&#34;Us Site - Toys, Games, &#38; More</title>
		<script type="text/javascript" charset="UTF-8" src="${radialPaymentJSPath}"></script>
		<script async src="//www.paypalobjects.com/api/checkout.js"></script>
		<dsp:include page="/checkout/resourceBundleCheckout.jsp"></dsp:include>
			
		<c:choose>
			<c:when test="${versionFlag}">
				<link rel="stylesheet" href="${domainUrl}${contextPath}css/checkoutPageHeader.css?${staticAssetsVersionFormat}=${versionNumbers}">
			</c:when>
			<c:otherwise>
				<link rel="stylesheet" href="${domainUrl}${contextPath}css/checkoutPageHeader.css">
			</c:otherwise>
		</c:choose>
	
		<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration"/>
		<dsp:getvalueof var="tealiumSyncURL" bean="TRUTealiumConfiguration.tealiumSyncURL"/>
		<script src="${tealiumSyncURL}"></script>
		<!-- switchboard tags -->
		<c:choose>
			<c:when test="${not empty quesryString}">		
				<link rel="alternate" media="only screen and (max-width: 640px)" href="${fn:replace(requestUrl,'://','://m.')}?${quesryString}">
				<link rel="alternate" media="only screen and (min-width: 641px) and (max-width: 1024px)" href="${fn:replace(requestUrl,'://','://t.')}?${quesryString}">
			</c:when>
			<c:otherwise>	
				<link rel="alternate" media="only screen and (max-width: 640px)" href="${fn:replace(requestUrl,'://','://m.')}">
				<link rel="alternate" media="only screen and (min-width: 641px) and (max-width: 1024px)" href="${fn:replace(requestUrl,'://','://t.')}">
			</c:otherwise>
		</c:choose>
		<!-- switchboard tags -->
	</head>
	<c:set var="nameBruOrTru" value=""/>
	<c:if test="${siteNameIdentification eq 'BabyRUs'}">
		<c:set var="nameBruOrTru" value="bru-site-checkout" />
	</c:if>
	<body class="${nameBruOrTru}">
		<div id="ajaxLoaderUI"></div> 
		<input id="hiddenContextPath" value="${contextPath}" type="hidden" /> 
		<input type="hidden" value="${TRUImagePath}" name="TRUImagePath" class="TRUImagePath" />
		<dsp:droplet name="GetSiteTypeDroplet">
			<dsp:param name="siteId" value="${site}"/>
			<dsp:oparam name="output">
				<dsp:getvalueof var="site" param="site"/>
				<c:set var="site" value ="${site}" scope="request"/>
			</dsp:oparam>
		</dsp:droplet>
		<input type="hidden" name="currentSite" id="currentSite" value="${site}"/>
		
		<div class="container-fluid default-template checkout-template min-width"> <!-- Start div: As per Fabricator - introduced -->
			<c:set var="template" value=""/>	   
		    <c:choose>
			    <c:when test="${checkoutOption eq 'Shipping'}">
			        <c:set var="template" value="container-fluid default-template shipping-no-save-address checkout-template min-width"/>
			    </c:when>
			    <c:when test="${checkoutOption eq 'Payment'}">
			        <c:set var="template" value="container-fluid checkout-payment-template default-template"/>
			    </c:when>
			      <c:when test="${checkoutOption eq 'Store-Pickup'}">
			        <c:set var="template" value="container-fluid checkout-pickup-template"/>
			    </c:when>
			      <c:when test="${checkoutOption eq 'Review'}">
			        <c:set var="template" value="container-fluid checkout-review-template default-template"/>
			    </c:when>
			    <c:when test="${checkoutOption eq 'Gifting'}">
			        <c:set var="template" value="container-fluid checkout-gifting-template default-template"/>
			    </c:when>
			    <c:when test="${checkoutOption eq 'Confirm'}">
			        <c:set var="template" value="container-fluid checkout-confirmation-template default-template"/>
			    </c:when>
		   </c:choose>
		   <%-- CHEKCOUT HEADER --%>
		   <div class="${template}">
		        <c:if test="${checkoutOption eq 'Confirm'}">
		             <dsp:include page="globalHeader.jsp"></dsp:include>
		        </c:if>
		        <%-- CHEKCOUT BODY --%>
		  		<jsp:doBody />
		 	</div>
		 	<%-- CHEKCOUT FOOTER --%>
		 	<div class="modal fade detailLearnMorePopup" id="detailsModel" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false" >
                <dsp:include page="/pricing/promotionDetailsPopup.jsp"/>
			</div>
			<div id="crossDomainCookieIframeLoader" class="hide"></div>
		      <c:choose>
		        <c:when test="${checkoutOption eq 'Confirm'}">
		          <div class="footer-grid">
		            <dsp:include page="globalFooter.jsp"></dsp:include>
		           </div> 
		        </c:when>
		        <c:otherwise>
                   <div class="shopping-cart-minimized-footer text-center">
                       <dsp:droplet name="/atg/targeting/TargetingForEach">
                         	<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/ShoppingCart/CartFooterTargeter"/>
                            <dsp:oparam name="output">
                                <dsp:valueof param="element.data" valueishtml="true" />
                            </dsp:oparam>
                       </dsp:droplet>
                   </div>
                </c:otherwise>
		      </c:choose>
		</div>
		<%-- CHEKCOUT INCLUEDE SCRIPTS --%>
	     <c:choose>
			<c:when test="${enableMinification and debugFlag eq '1'}">
				<c:choose>
					<c:when test="${versionFlag}">
						<script type="text/javascript" charset="UTF-8" 
							src="${TRUJSPath}javascript/vendor.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script type="text/javascript" charset="UTF-8" 
							src="${TRUJSPath}javascript/maskPassword.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script type="text/javascript" charset="UTF-8" 
							src="${TRUJSPath}javascript/jquery.datetimepicker.full.min.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script type="text/javascript" charset="UTF-8" 
							src="${TRUJSPath}javascript/toolkit.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script type="text/javascript" charset="UTF-8" 
							src="${TRUJSPath}javascript/msDropDown.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script type="text/javascript" charset="UTF-8" 
							src="${TRUJSPath}javascript/zipCodePlugin.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script type="text/javascript" charset="UTF-8" 
							src="${TRUJSPath}javascript/jquery-cookie.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script src="${TRUJSPath}javascript/jquery.validate.min.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script src="${TRUJSPath}javascript/tru_clientSideValidation.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script type="text/javascript" charset="UTF-8" 
							src="${TRUJSPath}javascript/tRus_checkout.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script 
							src="${TRUJSPath}javascript/pwstrength.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script src="${TRUJSPath}javascript/tRus_custom.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script type="text/javascript" charset="UTF-8" 
							src="${TRUJSPath}javascript/storelocator.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script type="text/javascript" charset="UTF-8" 
							src="${TRUJSPath}javascript/jquery.tmpl.min.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script type="text/javascript" charset="UTF-8" 
							src="${TRUJSPath}javascript/reward-validation.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					</c:when>
					<c:otherwise>
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/vendor.js"></script>
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/maskPassword.js"></script>
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/jquery.datetimepicker.full.min.js"></script>
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/toolkit.js"></script>
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/msDropDown.js"></script>
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/zipCodePlugin.js"></script>
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/jquery-cookie.js"></script>
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/jquery.validate.min.js"></script>
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/tru_clientSideValidation.js"></script>
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/tRus_checkout.js"></script>
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/pwstrength.js"></script>
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/tRus_custom.js"></script>
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/storelocator.js"></script>
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/reward-validation.js"></script>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${versionFlag}">
						<script type="text/javascript" charset="UTF-8"
							src="${TRUJSPath}javascript/checkoutPageFooter.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					</c:when>
					<c:otherwise>
						<script type="text/javascript" charset="UTF-8"
							src="${TRUJSPath}javascript/checkoutPageFooter.js"></script>
					</c:otherwise>
				</c:choose>	
			</c:otherwise>
		</c:choose>     
		<dsp:include page="/checkout/payment/fraudJSFiles.jsp">
			<dsp:param name="versionFlag" value="${versionFlag}" />
			<dsp:param name="staticAssetsVersionFormat" value="${staticAssetsVersionFormat}" />
			<dsp:param name="versionNumbers" value="${versionNumbers}" />
		</dsp:include>
		<script>
			$(window).load(function(){
				var req = new XMLHttpRequest();
				req.open('GET', document.location, false);
				req.send(null);
				Accept_Encoding = req.getResponseHeader('Accept-Encoding');
				Content_Encoding = req.getResponseHeader('Content-Encoding');
			});
		</script>
		<script>
			ajaxCallForCheckoutPage('${originatingRequest.contextPath}');
		</script>
		<script type="text/javascript">
				$.getScript("${hooklogicURL}",function(){
				});
		</script>
	</body>
</html>