<!DOCTYPE html> <!--  From page container jsp-->
<%@ include file="/includes/prelude.jspf"
%><%@ tag language="java"
%><%@ attribute name="header"
%><%@ attribute name="fromsearch"
%><%@ attribute name="fromFamily"
%><%@ attribute name="fromGiftFinder"
%><%@ attribute name="removeGiftFinder"
%><%@ attribute name="pageName"
%><%@ attribute name="fromSubCat"
%><%@ attribute name="fromNoSearch"
%><%@ attribute name="fromShopBy"
%><%@ attribute name="fromsiteMap"
%><%@ attribute name="fromCat"
%><%@ attribute name="fromPdp"
%><%@ attribute name="fromHome"
%><%@ attribute name="fromCollection"
%><%@ attribute name="fromStatic"
%><%@ attribute name="fromContactUs"
%>
<%@ attribute name="directionPageName" %>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations"/>
	 <dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath"/>
	 <dsp:getvalueof var="headerFooterPage" bean="EndecaConfigurations.headerFooterPage"/>
	 <dsp:getvalueof var="onlineProductId" param="productId"/>
	 <dsp:importbean bean="/atg/endeca/assembler/droplet/InvokeAssembler"/>
	 
	 <c:if test="${empty pageHeadDefined}">
	<html lang="en">
	</c:if>
		<dsp:include page="/header/header.jsp">
			<dsp:param name="header" value="${header}" />
			<dsp:param name="fromsiteMap" value="${fromsiteMap}" />
			<dsp:param name="fromsearch" value="${fromsearch}" />
			<dsp:param name="fromFamily" value="${fromFamily}" />
			<dsp:param name="fromGiftFinder" value="${fromGiftFinder}" />
			<dsp:param name="removeGiftFinder" value="${removeGiftFinder}"/>
			<dsp:param name="pageName" value="${pageName}"/>
			<dsp:param name="fromNoSearch" value="${fromNoSearch}" />
			<dsp:param name="fromSubCat" value="${fromSubCat}" />
			<dsp:param name="fromShopBy" value="${fromShopBy}" />
			<dsp:param name="fromCat" value="${fromCat}" />
			<dsp:param name="fromPdp" value="${fromPdp}" />
			<dsp:param name="fromHome" value="${fromHome}" />
			<dsp:param name="fromCollection" value="${fromCollection}" />
			<dsp:param name="fromStatic" value="${fromStatic}" />
			<dsp:param name="fromContactUs" value="${fromContactUs}" />
		</dsp:include>
		<dsp:include page="/myaccount/signInModal.jsp"/>
		<jsp:doBody />
				<dsp:getvalueof var="catID" param="catID" />
				<dsp:include page="/footer/footer.jsp">
					<dsp:param name="fromSubCat" value="${fromSubCat}" />
					<dsp:param name="fromFamily" value="${fromFamily}" />
					<dsp:param name="fromCat" value="${fromCat}" />
					<dsp:param name="fromSubCat" value="${fromSubCat}" />
					<dsp:param name="catID" value="${catID}" />
					<dsp:param name="directionPageName" value="${directionPageName}" />
				</dsp:include>
				</div>
		</body>
	</html>
