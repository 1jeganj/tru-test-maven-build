<dsp:page>
	<dsp:importbean
		bean="/com/tru/integrations/util/TestAuditMessageFormHandler" />
	<h1>Audit Message</h1>
	<dsp:form method="post">
	Select options :
		<dsp:select bean="TestAuditMessageFormHandler.option">
			<dsp:option value="orderId">Order Id</dsp:option>
			<dsp:option value="auditDate">Audit Date(dd-mm-yy)</dsp:option>
		</dsp:select>
		<br />
	Enter the value : <dsp:input type="text"
			bean="TestAuditMessageFormHandler.optValue" />
		<br />
		<dsp:input type="submit"
			bean="TestAuditMessageFormHandler.getAuditMessage"
			value="Get Messages" />
	</dsp:form>
	<dsp:droplet name="/atg/dynamo/droplet/ForEach">
		<dsp:param name="array" bean="TestAuditMessageFormHandler.auditItems" />
		<dsp:param name="sortProperties" value="+auditDate" />		
		<dsp:oparam name="empty">
  			No results found.
  		</dsp:oparam>
		<dsp:oparam name="outputStart">
							
			<table border=2 cellpadding=2>
				<tr>
					<td><h3>SRNO</h3></td>
					<td><h3>MESSAGE TYPE</h3></td>
					<td><h3>MESSAGE DATA</h3></td>
					<td><h3>Date</h3></td>
				</tr>
		</dsp:oparam>
		<dsp:oparam name="output">
			<tr><td><dsp:valueof param="count" /></td>
				<td><dsp:valueof param="element.messageType" /></td>
				<td><dsp:valueof param="element.messageData" /></td>
				<td><dsp:valueof param="element.auditDate" /></td>				
			</tr>
		</dsp:oparam>
		<dsp:oparam name="outputEnd">
			</table>
		</dsp:oparam>
	</dsp:droplet>

</dsp:page>