<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof  param="productInfo" var="productInfo"/>
<dsp:getvalueof  param="productInfo.rusItemNumber" var="rusItemNumber"/>
<dsp:getvalueof  param="productInfo.defaultSKU.upcNumbers" var="upcNumbers"/>
<dsp:getvalueof  param="productInfo.defaultSKU" var="defaultSKU"/>
<dsp:getvalueof  param="productInfo.manufacturerStyleNumber" var="manufacturerStyleNumber"/>
<dsp:getvalueof  param="productInfo.batteryIncluded" var="batteryIncluded"/>
<dsp:getvalueof  param="productInfo.batteryRequired" var="batteryRequired"/>
<dsp:getvalueof  param="productInfo.ewasteSurchargeSku" var="ewasteSurchargeSku"/>
<dsp:getvalueof  param="productInfo.batteries" var="batteries"/>
<dsp:getvalueof  param="productInfo.defaultSKU.productDimenssionLength" var="productDimenssionLength"/>
<dsp:getvalueof  param="productInfo.defaultSKU.productDimenssionWidth" var="productDimenssionWidth"/>
<dsp:getvalueof  param="productInfo.defaultSKU.productDimenssionHeight" var="productDimenssionHeight"/>
<dsp:getvalueof  param="productInfo.defaultSKU.productItemWeight" var="productItemWeight"/>
<dsp:getvalueof  param="productInfo.defaultSKU.assemblyDimenssionHeight" var="assemblyDimenssionHeight"/>
<dsp:getvalueof  param="productInfo.defaultSKU.assemblyDimenssionLength" var="assemblyDimenssionLength"/>
<dsp:getvalueof  param="productInfo.defaultSKU.assemblyDimenssionWidth" var="assemblyDimenssionWidth"/>
<dsp:getvalueof  param="productInfo.defaultSKU.assembledWeight" var="assembledWeight"/>
<dsp:getvalueof  param="productInfo.defaultSKU.assemblyRequired" var="assemblyRequired"/>
<dsp:getvalueof  param="productInfo.ewasteSurchargeState" var="ewasteSurchargeState"/>
<h2><fmt:message key="tru.productdetail.label.additionalInfo"/></h2>
		<ul class="additional-info">
			<c:if test="${not empty rusItemNumber}">
			<li><!-- “R”Web#: --><fmt:message key="tru.productdetail.label.additionalinforweb" />&nbsp;<dsp:valueof value="${rusItemNumber}"></dsp:valueof>
			</li>
			</c:if>
			<li><!-- SKU:  --><fmt:message key="tru.productdetail.label.additionalinfolabelsku" />&nbsp;<span class="additionalInfoUIDPlaceHolder"><dsp:valueof value="${defaultSKU.id}"></dsp:valueof></span>
			</li>
			<%-- <c:if test="${not empty upcNumbers}">--%>			
			<dsp:getvalueof var="upcNumbersLength" value="${fn:length(upcNumbers)}"></dsp:getvalueof>
			<li><!-- UPC/EAN/ISBN: --> <fmt:message key="tru.productdetail.label.additionalinfolabelupc" />
			 <span class="additionalInfoUPCNumbersPlaceHolder">
				<c:forEach items="${upcNumbers}" var="upcNumber" varStatus="loop">
				<dsp:getvalueof value="${loop.index}" var="index"></dsp:getvalueof>
				<c:choose>
				<c:when test="${upcNumbersLength ne  index+1}">
				<dsp:valueof value="${upcNumber}"></dsp:valueof>,
				</c:when>
				<c:otherwise>
				<dsp:valueof value="${upcNumber}"></dsp:valueof>
				</c:otherwise>
				</c:choose>
				</c:forEach>
			</span>
			</li>
			<%--</c:if>--%>			
			<%--<c:if test="${not empty manufacturerStyleNumber}">--%>
				<li><fmt:message key="tru.productdetail.label.additionalinfolabelmanufacturer" />&nbsp;<dsp:valueof value="${manufacturerStyleNumber}"></dsp:valueof></li>
			<!-- <li>Manufacturer #:  -->
			<%--</c:if>--%>
			<c:if test="${not empty batteries && batteryRequired eq 'Y'}">
			<li><fmt:message key="tru.productdetail.label.additionalinfobatteries"/>
					<c:if test="${not empty batteries && batteryRequired eq 'Y'}">
					<c:forEach items="${batteries}" var="battery">
					${battery.batteryQuantity}<dsp:valueof value=" "></dsp:valueof>	${battery.batteryType} <dsp:valueof value=" "></dsp:valueof>
					    <fmt:message key="tru.productdetail.label.batteriesrequired" />
						<!-- batteries are required --> 
						<c:if test="${not empty batteryIncluded}">
						(
						<c:choose>
						<c:when test="${batteryIncluded eq 'Y' }">
						 <fmt:message key="tru.productdetail.label.included" />
					<!-- 	included -->
						</c:when>
						<c:otherwise>
						 <fmt:message key="tru.productdetail.label.notincluded" />
						<!-- not included -->
						</c:otherwise>
						</c:choose>
						)
						</c:if>
					</c:forEach>
					</c:if>
			</li>
			</c:if>
			<c:if test="${not empty assembledWeight}">
				<!-- Assembled Weight: -->
				<li>
					<span>Assembled Weight:</span>
					<span class="pdp-additional-info-weight">&nbsp;${assembledWeight}&nbsp;</span>
					<span>lbs</span>
				</li>
			</c:if>
			<c:if test="${not empty productItemWeight}">
				<!-- Packaged Weight: -->
				<li>
					<span>Packaged Weight</span>
					<span class="pdp-additional-info-weight">&nbsp;${productItemWeight}&nbsp;</span>
					<span>lbs</span>
				</li>
			</c:if>
			<c:if test="${not empty productDimenssionLength && not empty productDimenssionWidth && not empty productDimenssionHeight }">
				<!-- Packaged Dimensions (in inches): -->
				<li>
					<span>Packaged Dimensions (in inches):</span>
					&nbsp;${productDimenssionLength} x ${productDimenssionWidth} x ${productDimenssionHeight}
				</li>
			</c:if>
			<c:if test="${not empty assemblyDimenssionLength && not empty assemblyDimenssionWidth && not empty assemblyDimenssionHeight }">
				<!-- Assembled Dimensions (in inches): -->
				<li>
					<fmt:message key="tru.productdetail.label.addinfoassembleddimensions"/>
					&nbsp;${assemblyDimenssionLength} x ${assemblyDimenssionWidth} x ${assemblyDimenssionHeight}
				</li>
			</c:if>
			<c:if test="${not empty assemblyRequired && assemblyRequired eq 'Y'}">
				<!-- Assembly Required -->
				<li>
					<fmt:message key="tru.productdetail.label.addinfoassemblyrequired"/>
				</li>
			</c:if>
			<c:if test="${not empty ewasteSurchargeState  && ewasteSurchargeState eq 'CA'}">
				<!-- Ewaste Surcharge SKU: -->
				<li>
					<fmt:message key="tru.productdetail.label.addinfoewastesku"/>&nbsp;
					<fmt:message key="tru.productdetail.label.ewastesurchargemessage"/>
				</li>
			</c:if>
		</ul>
</dsp:page>