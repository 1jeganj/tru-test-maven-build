<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof param="ispu" var="ispu" />
<dsp:getvalueof param="s2s" var="s2s" />
<dsp:getvalueof param="storeAvailMsg" var="storeAvailMsg" />
	<div class="store-image">
			<div class="ICN-Store"></div>
		</div>
		<span class="find-in-store-message">
			<span class="find-in-store-message-text">${storeAvailMsg}</span>
			<c:if test="${!(ispu eq 'N' && s2s eq 'N')}" > 
				<span class="store-display-block inline">
					<a data-toggle="modal" data-target="#findInStoreModal"
						href="javascript:void(0);" onclick="javascript:return loadFindInStore('${contextPath}','${selectedSkuId}','',$('.QTY-${productId}').val(),this);"><fmt:message key="tru.productdetail.label.selectAnotherStore" /></a>
				</span>
			</c:if>
		</span>
</dsp:page>