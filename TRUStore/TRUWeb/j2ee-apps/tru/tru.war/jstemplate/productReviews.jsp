<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />	
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<div class="question-and-answers">
	<h2 class="question-and-answers-title">
		<fmt:message key="tru.productdetail.label.productquestions" />
	</h2>
	<dsp:include page="./questionandanswers.jsp"/>
</div>
<div class="read-and-write-review">
	<h2 class="read-and-write-review-title">
		<fmt:message key="tru.productdetail.label.customerreviewsummary"/>
	</h2>
	<dsp:include page="./readreviewsummary.jsp"/>
</div>
</dsp:page>