<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />	
<dsp:getvalueof param="collectionProductInfo.collectionImage" var="collectionImage"/>
<dsp:getvalueof param="collectionProductInfo.collectionName" var="collectionName"/>
<dsp:getvalueof param="collectionZoomImageBlock" var="collectionZoomImageBlock"/>
	<div class="modal fade no-footer collectionImageGallery" id="collectionGalleryOverlayModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content sharp-border">
				<div class="pdp-image-gallert-overlay">
					<div class="gallery-image-viewer tse-scrollable wrapper">
						<a href="javascript:void(0)" class="close-modal" data-dismiss="modal">
							<span class="sprite-icon-x-close"></span>
						</a>
						<div class="tse-content">
							<c:choose>
								<c:when test="${not empty collectionImage}">
									<img src="${collectionImage}${collectionZoomImageBlock}" class="zoomimage" alt="${collectionName}"/>
								</c:when>
								<c:otherwise>
									<c:choose>
										 <c:when test="${not empty akamaiNoImageURL}">
										  	<img class="zoomimage gallery-noimage" src="${akamaiNoImageURL}" alt="${collectionName}">
											<input type="hidden" class="gallery-noimage1" value="${akamaiNoImageURL}" />
										 </c:when>
										 <c:otherwise>
										 	<img class="zoomimage gallery-noimage" src="${TRUImagePath}images/no-image500.gif" alt="${collectionName}">
											<input type="hidden" class="gallery-noimage1" value="${TRUImagePath}images/no-image500.gif" /> 
										 </c:otherwise>
									 </c:choose>											
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</dsp:page>