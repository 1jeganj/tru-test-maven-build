<dsp:page>
<dsp:getvalueof var="pid" param="onlineProductId" />
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:getvalueof var="powerReviewsURL" bean="TRUConfiguration.powerReviewsURL"/>
	<dsp:getvalueof var="merchantId" bean="TRUConfiguration.merchantId"/>
	<dsp:getvalueof var="merchantGroupId" bean="TRUConfiguration.merchantGroupId"/>
	<dsp:getvalueof var="powerReviewsApiKey" bean="TRUConfiguration.powerReviewsApiKey"/>
<script src='${powerReviewsURL}'></script>
<div class="pr-question-custom-btn-container">
	<div class="pr-question-custom-btn">ask a question</div>
</div>
<div id="pr-questions"></div>
<div id="pr-questionsdisp"></div>
</dsp:page>
