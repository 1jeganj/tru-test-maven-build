<dsp:page>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUPriceDroplet" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:droplet name="TRUPriceDroplet">
		<dsp:param name="skuId" param="skuId" />
		<dsp:param name="productId" param="productId" />
		<dsp:oparam name="true">
		<dsp:getvalueof var="salePrice" param="salePrice" />
		<dsp:getvalueof var="listPrice" param="listPrice" />
		<dsp:getvalueof var="savedAmount" param="savedAmount" />
		<dsp:getvalueof var="savedPercentage" param="savedPercentage" />
			<div class="price sale-price">
				<span class="crossed-out-price"><fmt:message key="pricing.dollar" /><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"	value="${listPrice}" /></span>
				<span class="sale-price"><fmt:message key="pricing.dollar" /><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"	value="${salePrice}" />
				</span>
			</div>
		</dsp:oparam>
		<dsp:oparam name="false">
		<dsp:getvalueof var="salePrice" param="salePrice" />
			<div class="price sale-price ">
				<dsp:getvalueof var="salePrice" param="salePrice" />
				<dsp:getvalueof var="listPrice" param="listPrice" />
				<c:choose>
					<c:when test="${salePrice == 0}">
						<span class="price"> <fmt:message key="pricing.dollar" /><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"	value="${listPrice}" /></span>
					</c:when>
					<c:otherwise>
						<span class="price"> <fmt:message key="pricing.dollar" /><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"	value="${salePrice}" /></span>
					</c:otherwise>
				</c:choose>
			</div>
		</dsp:oparam>
		<dsp:oparam name="empty">
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>