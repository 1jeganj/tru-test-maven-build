<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
 <dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
 <dsp:importbean bean="/com/tru/commerce/collection/TRUCollectionFormHandler"/>
 <dsp:importbean bean="/com/tru/commerce/locations/TRUStoreAvailabilityDroplet"/>
 <dsp:importbean bean="/com/tru/commerce/droplet/TRUItemLevelPromotionDroplet"/>
 <dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
 <dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
 <dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
 <dsp:getvalueof param="productInfo" var="productInfo" />
  <dsp:getvalueof param="productInfoJson" var="productInfoJson" />
 <dsp:getvalueof  param="productInfo.defaultSKU.promotionalStickerDisplay" var="promotionalStickerDisplay"/>
 <dsp:getvalueof param="productInfo.defaultSKU.primaryImage" var="primaryImage" />	
 <dsp:getvalueof param="productInfo.productId" var="productId" />
 <dsp:getvalueof param="productInfo.defaultSKU.id" var="skuId" />
  <dsp:getvalueof param="productInfo.defaultSKU.displayName" var="displayName" />
 <dsp:getvalueof param="productInfo.defaultSKU.longDescription" var="longDescription" />
 <dsp:getvalueof param="productInfo.defaultSKU.reviewRating" var="reviewRating" />
 <dsp:getvalueof param="productInfo.defaultSKU.customerPurchaseLimit" var="customerPurchaseLimit" />
 <dsp:getvalueof param="productInfo.defaultSKU.customerPurchaseLimit" var="toShowcustomerPurchaseLimit"/>
 <dsp:getvalueof param="productInfo.defaultSKU.unCartable" var="unCartable"/>
 <dsp:getvalueof param="productInfo.defaultSKU.inventoryStatus" var="inventoryStatus" />
  <dsp:getvalueof param="productInfo.defaultSKU.onlinePID" var="onlinePID" />
 <dsp:getvalueof param="collectionId" var="collectionId" />
  <dsp:getvalueof param="collectionProductListImageBlock" var="collectionProductListImageBlock" />
  <c:choose>
	<c:when test="${not empty pdpH1}">
        <c:set var="altH1SkuName" value="${pdpH1}"/>  
	</c:when>
	<c:otherwise>
		<c:set var="altH1SkuName" value="${displayName}"/>
	</c:otherwise>
</c:choose>
 <input type="hidden" value="${productId}"  class="CollectionProductIds hiddenCollectionProductId" />
 <input type="hidden" value="${productId}"  class="productIdForCart" />
    <input type="hidden" value="${collectionId}" class="hiddenCollectionId" />
	<dsp:droplet name="/com/tru/utils/TRUPdpURLDroplet">
		<dsp:param name="productId" value="${onlinePID}" />
		<dsp:param name="siteId" bean="/atg/multisite/Site.id" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="productPageUrl" param="productPageUrl" />
		</dsp:oparam>
	</dsp:droplet>
	<div class="row collection-product-block" id="collectonRefresh-${productId}">
	<dsp:include page="/jstemplate/collectionRegistryInformation.jsp">
	<dsp:param name="productInfo" value="${productInfo}"/>
	</dsp:include>
		
                    <div class="col-md-12 collectionLineItem" data-id="collectionId-${productId}">
                    <input type="hidden" value="${productId}-${skuId}" class="productSkuId">
                        <div class="row sticky-price-contents">
                        <input type="hidden" class="pageName" value="collectionPage">
                            <div class="collection-col-left">
					            <c:choose>
						 <c:when test="${not empty primaryImage}">
							<a href="${productPageUrl}">
								<img class="collection-image" src="${primaryImage}${collectionProductListImageBlock}" alt="${altH1SkuName}">
							</a>
						</c:when>
						<c:otherwise>
									<%-- <img id="hero-image" class="hero-image-zoom" src="${contextPath}/images/no-image500.gif">--%>
									<c:choose>
										 <c:when test="${not empty akamaiNoImageURL}">
										  	<a href="${productPageUrl}"><img class="collection-image" src="${akamaiNoImageURL}" alt="${altH1SkuName}"></a>
										 </c:when>
										 <c:otherwise>
										 	<a href="${productPageUrl}"><img class="collection-image" src="${TRUImagePath}images/no-image500.gif" alt="${altH1SkuName}"></a>
										 </c:otherwise>
									 </c:choose>	
						</c:otherwise>
					</c:choose>
					
					<%-- <p>
					<a class="image-gallery-link"><fmt:message key="tru.productdetail.label.seemoreimages"/></a>
                                </p> --%>
                            </div>
                            <div class="collection-col-center">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4><a href="${productPageUrl}">${displayName}</a></h4>
                                    </div>
                                </div>
                                <div class="row collectionPage-${productId}">
	                                <div class="collection-rating-and-price-section">
	                                 	<div class="collection-rating-section">
											<fmt:parseNumber var="reviewRating" type="number" value="${reviewRating}" />
							<div class="star-rating-block">
								<c:forEach begin="1" end="${reviewRating}">
									<div class="star-rating-on"></div>
								</c:forEach>

								<c:forEach begin="1" end="${5-reviewRating}">
									<div class="star-rating-off"></div>
								</c:forEach>
							</div>
						</div>
										<div class="collection_price_section">
                                    <dsp:include page="/jstemplate/collectionSkuVariantsAndPriceSection.jsp">
					                      <dsp:param name="productInfo" param="productInfo" />
					                      <dsp:param name="productId" value="${productId}"/>
					                      <dsp:param name="skuId" value="${skuId}"/>
				                     </dsp:include>
                                    </div>
	                                </div>
                                    <div class="col-md-12 collection-description-section">
                                        <div class="summary-text">
                                        <%-- <dsp:droplet name="/com/tru/commerce/droplet/TRUBigStringDecoderDroplet">
                    					<dsp:param name="data" param="productInfo.defaultSKU.longDescription"/>
                    					<dsp:oparam name="output">
                    						<dsp:valueof param="enocdeData" valueishtml="true" />
                    						<dsp:getvalueof param="enocdeData" var="longDescription" ></dsp:getvalueof>
                       					</dsp:oparam>
                   						 </dsp:droplet> --%>
                                           <c:set var="productDescription" value="${longDescription}"/>
                                           <c:if test="${not empty productDescription}">
                                           <c:choose>
                                           	<c:when test="${fn:length(productDescription.toString()) > 260}">
	                                           	 ${fn:substring(productDescription.toString(),0,260)}
	                                             ... <a href="${productPageUrl}">view details<%-- <fmt:message key="tru.collectionproduct.label.showmore"/> --%></a>
                                           	</c:when>
                                           	<c:otherwise>
                                           		${productDescription}
                                           	</c:otherwise>
                                           </c:choose>
                                         </c:if>
                                        </div>
                                    </div>
                                    <div class="col-md-12 collection-promo-section">
			                            <li class="deal-text">
			                               <dsp:include page="/jstemplate/displayCollectionPromotions.jsp">
			                               <dsp:param name="productInfo" param="productInfo" />
			                               </dsp:include>
			                            </li>
			                        </div>
                                </div>
					<dsp:droplet name="/com/tru/commerce/locations/TRUGetCookieDroplet">
						<dsp:param name="cookieName" value="favStore" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="cookieValue" param="cookieValue" />
							<dsp:getvalueof var="locationIdFromCookie" param="locationId" />
						</dsp:oparam>
						<dsp:oparam name="empty">
						</dsp:oparam>
					</dsp:droplet>
					 <c:if test="${not empty locationIdFromCookie}">
					<dsp:droplet name="TRUStoreAvailabilityDroplet">
						<%--locationId we are getting from the Cookie in the TRUStoreAvailabilityDroplet  --%>
						<dsp:param name="skuId" value="${selectedSkuId}" />
						<dsp:param name="locationId" value="${locationIdFromCookie}" />
						<dsp:param name="isReqFromAjax" value="true" />
						<dsp:oparam name="available">
							<dsp:getvalueof var="storeAvailMsg" param="storeAvailMsg" />
							<dsp:getvalueof var="favStore" param="favStore" />
							<dsp:getvalueof var="storeAddress" param="addressWithChianName" />
							<dsp:getvalueof var="storeAvailability" value="available" />
						</dsp:oparam>
					</dsp:droplet>
					</c:if>
                   </div>
					<div class="collection-col-right">
						<div class="cp-stock-details">
							<div class="collectionPage-${productId}">
							<dsp:include page="/jstemplate/colorSizeVariantFragment.jsp">
								<dsp:param param="productInfo" name="productInfo" />
								<dsp:param param="productInfoJson" name="productVoJson" />
								<dsp:param value="collectionPage" name="pageIncludedFrom" />
								<dsp:param param="swatchImageBlock" name="swatchImageBlock" />
							</dsp:include>
							<dsp:include page="/jstemplate/sizeChartTemplate.jsp">
							 <dsp:param param="productInfo" name="productInfo" />
							</dsp:include>
							<%-- <dsp:include page="/jstemplate/collectionQuantitySection.jsp">
								<dsp:param param="productInfo" name="productInfo" />
								<dsp:param value="${locationIdFromCookie}" name="locationIdFromCookie" />
								<dsp:param value="${storeAvailability}" name="storeAvailability" />
							</dsp:include> --%>
						</div>

							<div class="collections-availability">
                                 <div class="small-spacer"></div>
                                 <dsp:include page="/jstemplate/addToCart.jsp">
              						<dsp:param name="pageFrom" value="fromCollectionPage"/>
              						<dsp:param param="productInfo" name="productInfo" />
             					 </dsp:include>
								<dsp:include page="/jstemplate/productAvailability.jsp">
							    <dsp:param name="productId" value="${productId}" />
								<dsp:param param="productInfo" name="productInfo" />
								<dsp:param name="selectedSkuId" value="${skuId}" />
								<dsp:param value="${cookieValue}" name="cookieValue" />
								<dsp:param value="${locationIdFromCookie}"
									name="locationIdFromCookie" />
							</dsp:include>
						</div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                
               <%--  <div class="modal fade" id="galleryOverlayModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                <dsp:include page="/jstemplate/ajaxCollectionLineItemImagegallery.jsp">
				</dsp:include>
                </div>
				<div id="detailsPopup" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content sharp-border welcome-back-overlay">
							<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
							<h2>details</h2>
							<p class="abandonCartReminder">
								This message shows how many days we take to ship the item from our warehouse
							</p>
						</div>
					</div>
				</div>
						
				<div id="learnMorePopup" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content sharp-border welcome-back-overlay">
							<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
							<h2>learn more</h2>
							<p class="abandonCartReminder">
								these items is available for customers to go buy at stores only.
							</p>
						</div>
					</div>
				</div> --%>
				
				</div>
                <hr>
                </dsp:page>