<dsp:page>
	<dsp:importbean bean="/atg/multisite/Site" />
	<dsp:getvalueof var="enableTriad" bean="Site.enableTriad" />
	<dsp:getvalueof var="contentItem"
			vartype="com.endeca.infront.assembler.ContentItem"
			param="contentItem" />
	<div id="sosPdpAddZone2" class="margin-left-500">
		<c:if test="${enableTriad eq 'true' }">
			<div class="ad-zone-300-250 margin-top-50">
				<c:forEach var="element" items="${contentItem.MainContent}">
					<c:if test="${element.name eq 'TRUPDPAdZone2'}">
						<dsp:getvalueof var="componentPath"
							value="${element.componentPath}" />
						<dsp:getvalueof var="atgTargeterpath"
							value="/atg/registry/RepositoryTargeters${componentPath}" />
						<dsp:droplet name="/atg/targeting/TargetingForEach">
							<dsp:param name="targeter" bean="${atgTargeterpath}" />
							<dsp:oparam name="output">
								<dsp:valueof param="element.data" valueishtml="true" />
							</dsp:oparam>
						</dsp:droplet>
					</c:if>
				</c:forEach>
			</div>
		</c:if>
	</div>
	<%-- <script>
	$(document).ready(function(){
		var isSOS = $.cookie("isSOS");
		var sites = '<c:out value="${site}"/>';
		if(typeof isSOS != 'undefined' && isSOS != null && isSOS == 'true'){
			$("#sosPdpAddZone2").hide();
		}else{
			$("#sosPdpAddZone2").show();
		}
	}); 
</script> --%>
</dsp:page>