<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration"/>
<dsp:getvalueof  bean="TRUStoreConfiguration.collectionUrl" var="collectionUrl"/>
<dsp:getvalueof  param="productInfo.defaultSKU" var="defaultSKU"/>
<dsp:getvalueof param="productInfo.defaultSKU.batteryProducts" var="batteryProducts" />
<dsp:getvalueof  param="productInfo.defaultSKU.canBeGiftWrapped" var="canBeGiftWrapped"/>
<dsp:getvalueof  param="productInfo.defaultSKU.collectionNames" var="collectionNames"/>
<dsp:getvalueof  param="productInfo.defaultSKU.shipInOrigContainer" var="shipInOrigContainer"/>
<dsp:getvalueof  param="productInfo.batteryIncluded" var="batteryIncluded"/>
<dsp:getvalueof  param="productInfo.batteryRequired" var="batteryRequired"/>
<dsp:getvalueof param="productInfo.defaultSKU.channelAvailability" var="channelAvailability" />
<dsp:getvalueof param="productInfo.defaultSKU.onlinePID" var="onlinePID" />

<c:if test="${(not empty canBeGiftWrapped) || (not empty batteryRequired && batteryRequired ne 'N') || (not empty collectionNames) || 
		(empty channelAvailability || not empty channelAvailability) || (shipInOrigContainer eq true)}">
	<h2><fmt:message key="tru.productdetail.label.thingsToKnow"/></h2>
	<ul>
			<c:choose>
			<c:when test="${channelAvailability eq 'Online Only'}">
				<li><span><fmt:message key="tru.productdetail.label.thingstoknowonline"/></span></li>
	         </c:when>
	         <c:when test="${ empty channelAvailability or channelAvailability eq 'N/A' or channelAvailability eq 'In Store Only' }">	        
				<li><span><fmt:message key="tru.productdetail.label.thingstoknowstore"/></span></li>
	         </c:when>
	         </c:choose>
			<c:if test="${not empty batteryRequired && batteryRequired eq 'Y'}">
			<li><span><fmt:message key="tru.productdetail.label.thingstoknowbatteries"/></span><span class="things-to-know">
			<c:if test="${not empty batteryProducts}">
			<a href="#" class="battery-Items"><fmt:message key="tru.productdetail.label.thingstoknowbatteryclick"/></a>
			</c:if>
			</span></li>
		</c:if>
		<c:choose>
		<c:when test="${not empty canBeGiftWrapped && canBeGiftWrapped eq 'Y'}">
			<li><span><fmt:message key="tru.productdetail.label.thingstoknowygiftwrap"/>&nbsp;</span><span class="things-to-know"><a data-target="#giftWrapDetailModel" data-toggle="modal" href="javascript:void(0);"><fmt:message key="tru.productdetail.label.thingstoknowygiftdetail"/></a></span></li>
		</c:when>
		<c:when test="${not empty canBeGiftWrapped && canBeGiftWrapped eq 'N'}">
			<li><span><fmt:message key="tru.productdetail.label.thingstoknowngiftwrap"/>&nbsp;</span></li>
		</c:when>
		</c:choose>
		<c:if test="${shipInOrigContainer eq true }">
		<li><span><fmt:message key="tru.productdetail.label.thingstoknowshipin"/></span></li>				
		</c:if>
		<c:if test="${not empty collectionNames}">
			 <dsp:getvalueof var="collectionNamesLength" value="${fn:length(collectionNames)}"></dsp:getvalueof>
			<li><span><fmt:message key="tru.productdetail.label.thingstoknowcollectionprefix"/> 
			<%-- <c:choose>
			<c:when test="${collectionNamesLength eq 1 }">
				<a href="#" class="bold-Underlined"> <dsp:valueof value="${collectionNames[0]}"> </dsp:valueof></a>
			</c:when>
			<c:otherwise> --%>
			<c:forEach items="${collectionNames}" var="collectionNameEntry" varStatus="loop">
			<dsp:getvalueof value="${collectionNameEntry.value}" var="collectionName"></dsp:getvalueof>
			<dsp:getvalueof value="${collectionNameEntry.key}" var="collectionId"></dsp:getvalueof>
			<dsp:getvalueof var="collectionPageUrl" value="${siteURL}${collectionUrl}${collectionId}" />
			<dsp:getvalueof value="${loop.index}" var="index"></dsp:getvalueof>
			<c:choose>
			<c:when test="${collectionNamesLength ne  index+1}">
			<a href="${collectionPageUrl}" class="bold-Underlined"><dsp:valueof value="${collectionName}"></dsp:valueof></a> , </c:when>
			<c:otherwise>
			<a href="${collectionPageUrl}" class="bold-Underlined"><dsp:valueof value="${collectionName}"></dsp:valueof></a>
			</c:otherwise>
			
			</c:choose>
			
			</c:forEach>
			<%-- </c:otherwise>
			</c:choose> --%>
			</span>
			<fmt:message key="tru.productdetail.label.thingstoknowcollectionsuffix"/>
			</li>
			<%-- <span class="things-to-know"><a data-target="#detailsModel" data-toggle="modal" href="javascript:void(0);" ><fmt:message key="tru.productdetail.label.thingstoknowcollectiondetails"/></a></span></li> --%>
		</c:if>
	</ul>
	</c:if>
</dsp:page>