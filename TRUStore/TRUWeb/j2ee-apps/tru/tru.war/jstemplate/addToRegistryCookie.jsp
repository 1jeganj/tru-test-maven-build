<dsp:page>
<html lang="en">
<head></head>
<body>
<dsp:importbean bean="/com/tru/droplet/TRUAddRegistryCookieDroplet"/>
<dsp:importbean bean="/com/tru/droplet/TRUAddWishlistCookieDroplet"/>
<dsp:getvalueof var="cvalue" param="cvalue" />
<dsp:getvalueof var="pagename" param="pagename" />
<dsp:getvalueof var="iswishlist" param="iswishlist" />
<c:choose>
	<c:when test="${iswishlist ne 'true' }">
		<dsp:droplet name="TRUAddRegistryCookieDroplet">
		<dsp:param name="addToRegistryCookie" value="${cvalue}"/>
		<dsp:param name="pagename" value="${pagename}"/>
		<dsp:oparam name="output">
		</dsp:oparam>
		</dsp:droplet>
	</c:when>
	<c:when test="${iswishlist eq 'true' }">
		<dsp:droplet name="TRUAddWishlistCookieDroplet">
		<dsp:param name="addToWishListCookie" value="${cvalue}"/>
		<dsp:param name="pagename" value="${pagename}"/>
		<dsp:oparam name="output">
		</dsp:oparam>
		</dsp:droplet>
	</c:when>
</c:choose>
</body>
</html>
</dsp:page>
