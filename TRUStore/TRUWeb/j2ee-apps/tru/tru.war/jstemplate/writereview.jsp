<dsp:page>
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:getvalueof var="loginStatus" bean="Profile.transient"/>
<dsp:getvalueof var="customerId" bean="Profile.id"/>
<dsp:getvalueof var="customerEmail" bean="Profile.login"/>
<dsp:getvalueof var="powerReviewsURL" bean="TRUConfiguration.powerReviewsURL"/>
<dsp:getvalueof var="merchantId" bean="TRUConfiguration.merchantId"/>
<dsp:getvalueof var="merchantGroupId" bean="TRUConfiguration.merchantGroupId"/>
<dsp:getvalueof var="powerReviewsApiKey" bean="TRUConfiguration.powerReviewsApiKey"/>
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof var="onlinePid" param="pr_page_id"/>
<dsp:getvalueof var="pr_site_id" param="pr_site_id"/>

	<tru:pageContainer>
		<jsp:body>
			<br><br><br><br><br><br><br>
			<script src="${powerReviewsURL}"></script>
<div id="pr-write"></div>

<script>
      POWERREVIEWS.display.render({
    	  api_key : '${powerReviewsApiKey}',
  		locale : 'en_US',
  		merchant_group_id :'${merchantGroupId}',
  		merchant_id : '${merchantId}',
  		page_id : '${onlinePid}',
		components : {
			Write:'pr-write'
		}
	});
</script>
			

		</jsp:body>
	</tru:pageContainer>
</dsp:page>
