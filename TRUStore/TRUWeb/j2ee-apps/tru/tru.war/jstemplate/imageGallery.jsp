<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />	
<dsp:getvalueof param="productInfo.defaultSKU.primaryImage" var="primaryImage" />
<dsp:getvalueof param="productInfo.defaultSKU.primaryCanonicalImage" var="primaryCanImage" />
<dsp:getvalueof param="productInfo.defaultSKU.secondaryImage" var="secondaryImage" />
<dsp:getvalueof param="productInfo.defaultSKU.secondaryCanonicalImage" var="secondaryCanImage" />
<dsp:getvalueof param="productInfo.defaultSKU.displayName" var="displayName" />
<dsp:getvalueof param="productInfo.defaultSKU.alterNateImages" var="alterNateImages" />
<dsp:getvalueof param="productInfo.defaultSKU.alterNateCanonicalImages" var="alterNateCanonicalImages" />	
<dsp:getvalueof param="productInfo.defaultSKU.vendorsProductDemoUrl" var="vendorsProductDemoUrl" />
<dsp:getvalueof var="zoomPrimaryMainImageBlock" param="zoomPrimaryMainImageBlock"/>
<dsp:getvalueof var="zoomAltPriImageBlock" param="zoomAltPriImageBlock"/>
<dsp:getvalueof var="zoomAltSecImageBlock" param="zoomAltSecImageBlock"/>
<dsp:getvalueof var="zoomAlternativeImage1Block" param="zoomAlternativeImage1Block"/>
<dsp:getvalueof var="page" param="page"/>
<c:set var="primaryCanonicalImage" value="${fn:escapeXml(primaryCanImage)}"/>
<c:set var="secondaryCanonicalImage" value="${fn:escapeXml(secondaryCanImage)}"/>
<c:choose>
	<c:when test="${not empty pdpH1}">
        <c:set var="altH1SkuName" value="${pdpH1}"/>  
	</c:when>
	<c:otherwise>
		<c:set var="altH1SkuName" value="${displayName}"/>
	</c:otherwise>
</c:choose>        

                    <div class="modal-dialog">
                        <div class="modal-content sharp-border">
                         <div class="pdp-image-gallert-overlay">
                            <div class="gallery-image-viewer tse-scrollable wrapper">
                                <a href="javascript:void(0)" class="close-modal" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
                                <div class="tse-content">
                                    <%-- <c:if test="${not empty alterNateImages}"> --%>
                                    <div class="gallery-lr right">
                                        <img src="${TRUImagePath}images/forward-arrow.png" alt="">
                                    </div>
									<c:choose>
	                    				<c:when test="${page eq 'collection'}">
	                   						<dsp:include page="/jstemplate/imageBlock1.jsp">
						 						<dsp:param name="primaryImage" value="${primaryImage}"/>
												<dsp:param name="zoomPrimaryMainImageBlock" value="${zoomPrimaryMainImageBlock}"/>
											</dsp:include>
	                    				</c:when>
	                   					 <c:otherwise>	                    					
	                    					<dsp:include page="/jstemplate/imageBlock1.jsp">
						 						<dsp:param name="primaryImage" value="${primaryCanonicalImage}"/>
												<dsp:param name="zoomPrimaryMainImageBlock" value="${zoomPrimaryMainImageBlock}"/>
											</dsp:include>
	                   	 				</c:otherwise>
	                    			</c:choose>
                                 <%--    <c:choose>
	                    				<c:when test="${not empty primaryImage}">
	                   						<img class="zoomimage" id="primaryImgGallery" src="${primaryImage}${zoomPrimaryMainImageBlock}" alt="${altH1SkuName}">
	                    				</c:when>
	                   					 <c:otherwise>	                    					
	                    					<c:choose>
												<c:when test="${not empty akamaiNoImageURL}">
												  	<img class="zoomimage" id="gallery-noimage" src="${akamaiNoImageURL}" alt="${altH1SkuName}">
	                    							<input type="hidden" id="gallery-noimage1" value="${akamaiNoImageURL}" />
												</c:when>
												 <c:otherwise>
													<img class="zoomimage" id="gallery-noimage" src="${TRUImagePath}images/no-image500.gif" alt="${altH1SkuName}">
	                    							<input type="hidden" id="gallery-noimage1" value="${TRUImagePath}images/no-image500.gif" />
												</c:otherwise>
										 	</c:choose>	
	                   	 				</c:otherwise>
	                    			</c:choose> --%>
	                    			
	                    	<!-- div id="invodoGalleryVideo" class="invodoGalleryVideo-overlay"></div> -->
                                    <!-- <iframe id="ytplayer" src="http://www.youtube.com/embed/wL60hEXAL7s?version=3&amp;enablejsapi=1" frameborder="0" allowfullscreen=""></iframe> -->
                                   <%-- <c:if test="${not empty alterNateImages}"> --%>
                                    <div class="gallery-lr left">
                                        <img src="${TRUImagePath}images/back-arrow.png" alt="">
                                    </div>
                                   <%--  </c:if> --%>
	                                    <div id="invodoGalleryVideo" class="invodoGalleryVideo-overlay hide"></div>
	                                   </div>
	                                   <!-- </div> -->
                            </div>
                            <div class="gallery-overlay-footer">
									<div class="inline video-label-finder videosclass">
										<p class="gallery-popup-vedio">videos</p> 

			                            <!-- Invodo video button-->
										<div id="invodoVideoButton" class="inline"></div>
										<!--  view demo-->
										<c:if test="${not empty vendorsProductDemoUrl }">
											<a href="" class="product-tour-container"
												onclick="window.open( '${vendorsProductDemoUrl}', '','height=450,width=670' ); return false"
												target="_blank">
												<div class="inline product-tour">
													<fmt:message key="tru.productdetail.label.viewdemo" />
												</div>
											</a>
										</c:if>
			
									</div>
									<c:choose>
	                    				<c:when test="${page eq 'collection'}">
	                   						<dsp:include page="/jstemplate/imageBlock2.jsp">
						 						<dsp:param name="primaryImage" value="${primaryImage}"/>
						 						<dsp:param name="secondaryImage" value="${secondaryImage}"/>
						 						<dsp:param name="alterNateImages" value="${alterNateImages}"/>
												<dsp:param name="zoomAltPriImageBlock" value="${zoomAltPriImageBlock}"/>
												<dsp:param name="zoomAltSecImageBlock" value="${zoomAltSecImageBlock}"/>
												<dsp:param name="zoomAlternativeImage1Block" value="${zoomAlternativeImage1Block}"/>
											</dsp:include>
	                    				</c:when>
	                   					 <c:otherwise>	                    					
	                    					<dsp:include page="/jstemplate/imageBlock2.jsp">
						 						<dsp:param name="primaryImage" value="${primaryCanonicalImage}"/>
						 						<dsp:param name="secondaryImage" value="${secondaryCanonicalImage}"/>
						 						<dsp:param name="alterNateImages" value="${alterNateCanonicalImages}"/>
												<dsp:param name="zoomAltPriImageBlock" value="${zoomAltPriImageBlock}"/>
												<dsp:param name="zoomAltSecImageBlock" value="${zoomAltSecImageBlock}"/>
												<dsp:param name="zoomAlternativeImage1Block" value="${zoomAlternativeImage1Block}"/>
											</dsp:include>
	                   	 				</c:otherwise>
	                    			</c:choose>
	                         		<%-- <c:if test="${not empty primaryImage || not empty secondaryImage || not empty alterNateImages }">
	                            		<div class="inline gallery-divider"></div>                            		
	                            		<div class="inline productImages">
		                                    <p class="gallery-popup-images galleryPopup-images">product images</p> 
		                                    <c:choose>
			                                    <c:when test="${ not empty primaryImage}">
			                                    	<img id="gallery-image-3" title="primary image thumbnail" class="gallery-image-selected pdpimages" src="${primaryImage}${zoomAltPriImageBlock}" onClick="javascript:hideVideoFromOverlay()" alt="${altH1SkuName}">
			                                    </c:when>
			                                    <c:otherwise>
				                                    <c:choose>
													<c:when test="${not empty akamaiNoImageURL}">
														<img id="gallery-image-3" class="gallery-image-selected pdpimages" src="${akamaiNoImageURL}" onClick="javascript:hideVideoFromOverlay()" alt="${altH1SkuName}">
													</c:when>
													 <c:otherwise>
														<img id="gallery-image-3" class="gallery-image-selected pdpimages" src="${TRUImagePath}images/no-image500.gif" onClick="javascript:hideVideoFromOverlay()" alt="${altH1SkuName}">
													</c:otherwise>
											 		</c:choose>	
			                                    </c:otherwise>
		                                    </c:choose>
		                                 	<c:if test="${not empty secondaryImage}">
			                                   <img class="gallery-image-unselected pdpimages"  title="secondary image thumbnail" id="gallery-image-4" src="${secondaryImage}${zoomAltSecImageBlock}" onClick="javascript:hideVideoFromOverlay()" alt="${altH1SkuName}">
			                              	</c:if>
		                                    <c:if test="${not empty alterNateImages}">
			                                   <c:forEach begin="0" end="2" items="${alterNateImages}" var="alterNateImage">
			                                   		<img class="gallery-image-4 gallery-image-unselected pdpimages" title="alternate image thumbnail"  src="${alterNateImage}${zoomAlternativeImage1Block}" onClick="javascript:hideVideoFromOverlay()" alt="${altH1SkuName}">
			                                   </c:forEach>
			                              	</c:if>
	                            		</div>
	                            	</c:if> --%>
                            		 <div class="inline gallery-divider"></div>                         		
                            		<div class="inline customerImages"></div> 
                            </div>
                            </div>
                            
                           </div>
                        </div>
</dsp:page>