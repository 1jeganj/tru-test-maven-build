<dsp:page>
	<div class="modal-lg modal-dialog" id="modal-lg-errorQuickView">
		<div class="modal-content sharp-border">
			<a href="javascript:void(0)" class="close-modal" data-dismiss="modal">
				<span class="sprite-icon-x-close"></span>
			</a>
			<dsp:include page="/jstemplate/unavailableProductDetailsTemplate.jsp" />
		</div>
	</div>
</dsp:page>