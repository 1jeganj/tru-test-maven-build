<dsp:page>
<dsp:getvalueof param="productId" var="productId"></dsp:getvalueof>
<dsp:getvalueof param="skuId" var="skuId"></dsp:getvalueof>
<dsp:getvalueof param="page" var="page"></dsp:getvalueof>
<c:choose>
<c:when test ="${page eq 'fromCompare'}">
<dsp:include page="/compare/displayComparePrice.jsp">
	<dsp:param name="skuId" value="${skuId}" />
	<dsp:param name="productId" value="${productId}" />
	<%-- <dsp:param name="presellable" value="${presellable}" /> --%>
</dsp:include>
</c:when>
<c:otherwise>
 <dsp:include page="/jstemplate/relatedProductPrice.jsp">
	<dsp:param name="skuId" value="${skuId}" />
	<dsp:param name="productId" value="${productId}" />
	<%-- <dsp:param name="presellable" value="${presellable}" /> --%>
</dsp:include>
</c:otherwise>
</c:choose>
</dsp:page>