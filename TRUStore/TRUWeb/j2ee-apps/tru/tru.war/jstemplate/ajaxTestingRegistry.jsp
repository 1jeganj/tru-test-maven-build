<%@ page contentType="application/json"%>
<dsp:page>
    
    {
  "mServiceTime": 361,
  "mRegistryDetail": {
    "mRegistry": {
      "mEmail": "SANDALS@GSICOMMERCE.COM",
      "mRegistryAccountHeader": {
        "mPeopleId": 117686059,
        "mFirstName": "SUMIT",
        "mLastName": "SANDAL",
        "mMaidenName": "ASDFADS",
        "mCity": "KING OF PRUSSIA",
        "mState": "PA"
      },
      "mRegistryAccountDetailList": [
        {
          "mRegistryNumber": 34209,
          "mRegistryType": "G",
          "mCoregistrantLastName": "HARSH",
          "mEventDate": "2015-01-31",
          "mEventType": "2",
          "mCreateDate": "2015-01-28",
          "mModifiedDate": "2015-01-28",
          "mCoregistrantFirstName": "RATNANI",
          "mDeleteRegistryIndicator": "N"
        },
        {
          "mRegistryNumber": 34208,
          "mRegistryType": "G",
          "mCoregistrantLastName": "RATNANI",
          "mEventDate": "2015-01-31",
          "mEventType": "2",
          "mCreateDate": "2015-01-28",
          "mModifiedDate": "2015-01-28",
          "mDeleteRegistryIndicator": "N"
        },
        {
          "mRegistryNumber": 31056,
          "mRegistryType": "G",
          "mCoregistrantLastName": "ASDFA",
          "mEventDate": "2015-06-01",
          "mEventType": "2",
          "mCreateDate": "2013-12-18",
          "mModifiedDate": "2015-01-28",
          "mDeleteRegistryIndicator": "N"
        }
      ]
    },
    "mPreferredLanguage": "EN",
    "mResponseStatus": {
      "mIndicator": "SUCCESS",
      "mMessage": {
        "mMessageType": "INFORMATIONAL",
        "mDisplayCode": [
          "001 - Operation is successful"
        ],
        "mMessageId": "WRAPPER_SERVICE"
      }
    },
    "mLocale": {
      "mCountry": "US",
      "mLanguage": "en"
    }
  }
}
    
</dsp:page>