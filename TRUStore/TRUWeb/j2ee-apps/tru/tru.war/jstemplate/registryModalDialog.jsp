<dsp:page>
  <dsp:getvalueof param="name" var="name" />
  <dsp:getvalueof param="page" var="page" />
  <dsp:getvalueof param="primaryImage" var="primaryImage" />
  
 <%-- Registry Succcess Modal Dialog --%>
	<div class="modal fade pdpModals show-hide-addToCart-section" id="add-to-registry" tabindex="-1"
			role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog modal-md">
				<div class="modal-content sharp-border">
					 <dsp:include page="/jstemplate/registrySuccessDialog.jsp">
					 <dsp:param name="skuDisplayName" value="${name}"/>	
					 <dsp:param name="page" value="${page}"/>
					 <dsp:param name="primaryImage" value="${primaryImage}"/>			
					</dsp:include>
					
					</div>
			</div>
	</div>
	<%-- Registry Error Modal Dialog --%>
	<div class="modal fade pdpModals show-hide-addToCart-section" id="error-registry-dailog" tabindex="-1"
			role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog modal-md">
				<div class="modal-content sharp-border">
					<dsp:include page="/jstemplate/registryErrorDialog.jsp">
					 <dsp:param name="skuDisplayName" value="${name}"/>
					  <dsp:param name="page" value="${page}"/>
					 <dsp:param name="primaryImage" value="${primaryImage}"/>		
					</dsp:include>
					</div>
			</div>
	</div>		
	<%-- Wishlist Succcess Modal Dialog --%>
	<div class="modal fade pdpModals show-hide-addToCart-section" id="items-added-success-wishlist" tabindex="-1" 
	role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog modal-md">
			<div class="modal-content sharp-border">
					<dsp:include page="/jstemplate/wishlistSuccessDialog.jsp">
					<dsp:param name="skuDisplayName" value="${name}"/>	
					<dsp:param name="page" value="${page}"/>
					<dsp:param name="primaryImage" value="${primaryImage}"/>						
					</dsp:include>
			</div>
		</div>
	</div>	
	<%-- multiple Wishlist Succcess Modal Dialog --%>
	<div class="modal fade pdpModals show-hide-addToCart-section" id="multiple-items-added-success-wishlist" tabindex="-1" 
	role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content sharp-border">
					<dsp:include page="/jstemplate/multipleWishlistSuccessDialog.jsp">
					<dsp:param name="skuDisplayName" value="${name}"/>				
					<dsp:param name="page" value="${page}"/>
					<dsp:param name="primaryImage" value="${primaryImage}"/>			
					</dsp:include>
			</div>
		</div>
	</div>
</dsp:page>