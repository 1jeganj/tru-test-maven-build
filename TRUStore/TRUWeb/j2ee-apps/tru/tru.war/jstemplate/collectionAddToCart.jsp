<dsp:page>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
 <dsp:getvalueof var="registryWishlistkeyMap" bean="TRUStoreConfiguration.registryWishlistkeyMap" />
<dsp:importbean bean="/com/tru/droplet/TRURegistryCookieDroplet"/>
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:getvalueof param="collectionProductInfo" var="collectionProductInfo" />
<dsp:getvalueof param="collectionProductInfo.collectionName" var="collectionName" />


<c:set var="singleQuote" value="'"/>
<c:set var="doubleQuote" value='"'/> 
<c:set var="slashSingleQuote" value="\'"/>
<c:set var="slashDoubleQuote" value='\"'/>
<c:choose>
	     <c:when test="${fn:contains(collectionName,singleQuote)}"> 
	     <c:set var="collectionName" value="${fn:replace(collectionName,singleQuote,slashSingleQuote)}"/> 
		 </c:when>
		 <c:when test="${fn:contains(collectionName,doubleQuote)}"> 
	     <c:set var="collectionName" value="${fn:replace(collectionName,doubleQuote,slashDoubleQuote)}"/> 
		 </c:when>
		 <c:otherwise>
		 <c:set var="collectionName" value="${collectionName}"/>
		 </c:otherwise>
</c:choose>

<dsp:getvalueof param="fromCollectionPage" var="fromCollectionPage" />
<c:forEach var="entry" items="${registryWishlistkeyMap}">
		<c:if test="${entry.key eq 'timeout'}">
			<c:set var="timeout" value="${entry.value}" />
		</c:if>
		</c:forEach>
		<dsp:include page="/jstemplate/registryModalDialog.jsp">
               <dsp:param name="page" value="collection"/>
         </dsp:include>
        <%-- <div class="collections-sticky-price">
        <dsp:droplet name="TRURegistryCookieDroplet">
							<dsp:param name="retrieveRegistryCookie" value="true" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="registry_id" param="registry_id" />
								<dsp:getvalueof var="lastAccessedRegistryID" param="lastAccessedRegistryID" />
								<dsp:getvalueof var="lastAccessedWishlistID" param="lastAccessedWishlistID" />
								<dsp:getvalueof var="wishlist_id" param="wishlist_id" />
							</dsp:oparam>
						</dsp:droplet>
					    <input type="hidden" id="rgs_registryNumber" value="${registry_id}">
						<input type="hidden" id="rgs_wishlistId" value="${wishlist_id}">
						<input type="hidden" id="rgs_lastAccessedRegistryID" value="${lastAccessedRegistryID}">
						<input type="hidden" id="rgs_lastAccessedWishlistID" value="${lastAccessedWishlistID}">
						<input type="hidden" id="rgs_skuDisplayName" value="${collectionName}">
						<input type="hidden" id="rgs_productDetails" value="">
						<input type="hidden" id="rgs_timeout" value="${timeout}">
						
           <div class="sticky-price-contents">
                     <input type="hidden" id="colletinToCart" value="">
                     <input type="hidden" id="collectionToRegistry" value="">
                           <div class="collections-items-selected"><fmt:message key="tru.productdetail.label.zero"/>&nbsp;<fmt:message key="tru.productdetail.label.collectionitemsselected" /></div>
                      <!-- <div class="collections-items-selected">0 items selected</div> -->
                        <div class="add-to-cart-section">
                          <button disabled="disabled" data-toggle="modal" class="add-to-cart collection" data-path="${contextPath}"><fmt:message key="tru.productdetail.label.addtocart"/></button>
  						 </div>
					      <div class="add-to-button" id="add-to-button">
						  <div class="inline">
							<a href="javascript:void(0);" class="small-orange desableLinkW3c">
							<img src="${TRUImagePath}images/wish-list.png" alt="wish list" class="add-to-image"><fmt:message key="tru.productdetail.label.collectionAddtoWishList" />
							</a>
						  </div>
						  <div class="inline">
							<a href="javascript:void(0);" class="small-orange desableLinkW3c" >
							   <img src="${TRUImagePath}images/baby-registry.png" alt="baby registry" class="add-to-image"><fmt:message key="tru.productdetail.label.collectionAddtoRegistry" /><!-- add to registry -->
							   </a>
						  </div>
					    </div>
		   </div>
                    <dsp:include page="/jstemplate/socialShareLinks.jsp">
                      	<dsp:param name="fromCollectionPage" value="${fromCollectionPage}"/>
                      	<dsp:param name="collectionProductInfo" value="${collectionProductInfo}"/>
                    </dsp:include>
                    <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                        <div class="modal-dialog warning-modal">
                            <div class="modal-content sharp-border">
                                <div class="modal-header">
                                    <a href="javascript:void(0)" class="close-modal" data-dismiss="modal" title="close model"><span class="sprite-icon-x-close"></span></a>
                                    <div>
                                        <div class="big-red">
                                            <fmt:message key="tru.productdetail.label.Warning"/>
                                        </div>
                                        <div class="warning-message">
                        <div class="warning-text bold">
                        <fmt:message key="tru.productdetail.label.matureAudience"/>
                        </div>
                        <div class="warning-text">
							<fmt:message key="tru.productdetail.label.strongLanguage"/>
                       	</div>
                       	<div class="warning-text bold">
                       	<fmt:message key="tru.productdetail.label.strongLanguage"/>
                       	</div>
                    </div>
                                        <div class="warning-modal-footer">
                                            <div class="over-17 inline">
                                                <div class="modal-checkbox-container">
                                                    <input class="modal-checkbox" id="pdp-modal-checkbox-1" type="checkbox">
                                                    <label for="pdp-modal-checkbox-1"></label>
                                                </div>
                                                <div class="inline warning-text"><fmt:message key="tru.productdetail.label.ageconfirmabove"/></div>
                                            </div>
                                            <div class="under-17 inline">
                                                <div class="modal-checkbox-container">
                                                    <input class="modal-checkbox" id="pdp-modal-checkbox-2" type="checkbox">
                                                    <label for="pdp-modal-checkbox-2"></label>
                                                </div>
                                                <div class="inline warning-text"><fmt:message key="tru.productdetail.label.ageconfirmunder"/></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --%>
</dsp:page>