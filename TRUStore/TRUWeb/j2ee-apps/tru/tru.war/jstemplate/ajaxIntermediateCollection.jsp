<dsp:page>
<dsp:importbean bean="/com/tru/commerce/collection/TRUCollectionFormHandler" />
<dsp:importbean bean="/com/tru/commerce/collection/TRUCollectionUtils" />
<dsp:getvalueof var="usedFor" param="usedFor"></dsp:getvalueof>
	<c:choose>
		<c:when test="${usedFor eq 'constructions' }">
			<dsp:getvalueof var="productId" param="productId" />
			<dsp:getvalueof var="quantity" param="quantity" />
			<dsp:getvalueof  var="skuId" param="skuId" />
			<dsp:getvalueof  var="locationId" param="locationId" />
			<dsp:getvalueof  var="inventoryStatus" param="inventoryStatus" />
			<dsp:getvalueof  var="storeInventoryStatus" param="storeInventoryStatus" />
			<dsp:getvalueof  var="ispu" param="ispu" />
			<dsp:getvalueof  var="s2s" param="s2s" />
			<dsp:getvalueof  var="productType" param="productType" />
			<dsp:getvalueof  var="activeSkulength" param="activeSkulength" />
			<dsp:setvalue bean="TRUCollectionFormHandler.productId" value="${productId}" />
			<dsp:setvalue bean="TRUCollectionFormHandler.skuId" value="${skuId}" />
			<dsp:setvalue bean="TRUCollectionFormHandler.quantity" value="${quantity}" />
			<dsp:setvalue bean="TRUCollectionFormHandler.locationId" value="${locationId}" />
			<dsp:setvalue bean="TRUCollectionFormHandler.inventoryStatus" value="${inventoryStatus}" />
			<dsp:setvalue bean="TRUCollectionFormHandler.ispu" value="${ispu}" />
			<dsp:setvalue bean="TRUCollectionFormHandler.s2s" value="${s2s}" />
			<dsp:setvalue bean="TRUCollectionFormHandler.storeInventoryStatus" value="${storeInventoryStatus}" />
			<dsp:setvalue bean="TRUCollectionFormHandler.productType" value="${productType}" />
			<dsp:setvalue bean="TRUCollectionFormHandler.activeSkulength" value="${activeSkulength}" />
			<dsp:setvalue bean="TRUCollectionFormHandler.createCollectionString" value="submit" />
			<dsp:getvalueof bean="TRUCollectionFormHandler.finalString" var="finalString"></dsp:getvalueof>
			<dsp:getvalueof bean="TRUCollectionFormHandler.itemsCount" var="itemsCount"></dsp:getvalueof>
			
			<div class="finalString">${finalString},${itemsCount}</div>
			</c:when>
			<c:when test="${usedFor eq 'registry' }">
			<dsp:getvalueof var="productId" param="productId" />
			<dsp:getvalueof var="quantity" param="quantity" />
			<dsp:getvalueof  var="skuId" param="skuId" />
			<dsp:getvalueof  var="locationId" param="locationId" />
			<dsp:getvalueof  var="inventoryStatus" param="inventoryStatus" />
			<dsp:getvalueof  var="productType" param="productType" />
			<dsp:getvalueof  var="activeSkulength" param="activeSkulength" />
			<dsp:setvalue bean="TRUCollectionFormHandler.productId" value="${productId}" />
			<dsp:setvalue bean="TRUCollectionFormHandler.skuId" value="${skuId}" />
			<dsp:setvalue bean="TRUCollectionFormHandler.quantity" value="${quantity}" />
			<dsp:setvalue bean="TRUCollectionFormHandler.locationId" value="${locationId}" />
			<dsp:setvalue bean="TRUCollectionFormHandler.inventoryStatus" value="${inventoryStatus}" />
			<dsp:setvalue bean="TRUCollectionFormHandler.productType" value="${productType}" />
			<dsp:setvalue bean="TRUCollectionFormHandler.activeSkulength" value="${activeSkulength}" />
			<dsp:setvalue bean="TRUCollectionFormHandler.createRegistryString" value="submit" />
			<dsp:getvalueof bean="TRUCollectionFormHandler.finalRegistryString" var="finalRegistryString"></dsp:getvalueof>
			<dsp:getvalueof bean="TRUCollectionFormHandler.itemsRegistryCount" var="itemsRegistryCount"></dsp:getvalueof>
			
			<div class="finalRegistryString">${finalRegistryString},${itemsRegistryCount}</div>
			</c:when>
			<c:when test="${usedFor eq 'flush' }">
				<dsp:setvalue bean="TRUCollectionFormHandler.flushCollectionString" value="submit" />
			</c:when>
			<c:when test="${usedFor eq 'flushRegistry' }">
				<dsp:setvalue bean="TRUCollectionFormHandler.flushRegistryString" value="submit" />
			</c:when>
			</c:choose>
</dsp:page>