<dsp:page>
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof param="productInfo" var="productInfo" />
<dsp:getvalueof param="productInfo.defaultSKU.customerPurchaseLimit" var="customerPurchaseLimit" />
<dsp:getvalueof param="productInfo.defaultSKU.customerPurchaseLimit" var="toShowcustomerPurchaseLimit" />
<dsp:getvalueof param="productInfo.defaultSKU.presellable" var="presellable" />
<dsp:getvalueof param="productInfo.defaultSKU.presellQuantityUnits" var="presellQuantityUnits" />
<c:if test="${not empty customerPurchaseLimit}">
<fmt:parseNumber var="customerPurchaseLimit1" type="number" value="${customerPurchaseLimit}" />
</c:if>
<c:if test="${not empty presellQuantityUnits}">
<fmt:parseNumber var="presellQuantityUnits1" type="number" value="${presellQuantityUnits}" />
</c:if>

			<dsp:getvalueof param="isSizeAvailable" var="isSizeAvailable" />

						<c:if test="${isSizeAvailable eq 'false' }">
							<div class="color-text" id="qtys-text">
                               <%-- <fmt:message key="tru.productdetail.label.qty"/>: --%>
                               <span class="pdp-quantity-limit hide"><fmt:message key="tru.productdetail.label.quantityerrormessage"/></span>
                               <c:choose>
                               <c:when test="${presellable && not empty presellQuantityUnits && presellQuantityUnits1 le 0}">
                                     <p> <fmt:message key="tru.productdetail.label.presellthresholderror" /></p>
                              <!--  <p> This pre-sell item has reached the maximum limit and cannot be added to the cart</p> -->
                               </c:when>
                               <%-- <c:when test="${customerPurchaseLimit1 eq 1}">
                               	<p>the quantity you are attempting to add to the cart has exceeded the allowable limit per user</p>
                               </c:when> --%>
                               </c:choose>
                            </div>
						</c:if>
                            
                            <div class="stepper partial pdpQty">   
                            	<div class="decreaseFullDiv">
                                <a href="javascript:void(0)" class="decrease disabled" title="decrease count"></a>
                                </div>
                             <%--    <input type="number" class="counter center" id="ItemQty"  contenteditable="true" value="1" aria-label="Item Count"> --%>
							  <c:if test="${empty customerPurchaseLimit}">
								 <dsp:getvalueof value="999" var="customerPurchaseLimit" />
							 </c:if>
							 
							 <input type="hidden" value="${customerPurchaseLimit}" id="quantityLimitVal"/>
								 <c:choose>
								 <c:when test="${(presellable && not empty presellQuantityUnits && presellQuantityUnits1 le 0 ) || (customerPurchaseLimit1 eq 1)}">
								  <!-- <div class="sticky-quantity-borber"> -->
								  <input type="text" class="counter center" id="ItemQty" disabled="disabled"  value="1" maxlength="1" aria-label="Item Count">
								 <!--  </div> -->
								  <div class="increaseFullDiv">
	                           	  <a href="javascript:void(0)" class="increase disabled" title="increase count"></a>
	                           	  </div>
								 </c:when>
								 <c:otherwise>
								  <!-- <div class="sticky-quantity-borber"> -->
								  <input type="text" class="counter center" id="ItemQty" onkeypress="return isNumberKey(event)" contenteditable="true"  value="1" maxlength="3" aria-label="Item Count">
	                              <!-- </div> -->
	                              <div class="increaseFullDiv">
	                              	<a href="javascript:void(0)" class="increase enabled" title="increase count"></a>
	                              </div>
								 </c:otherwise>
								 </c:choose>							
                            </div>
                            <input type="hidden"  class="productIdForCart" value="${productInfo.productId}"/>
                            <c:if test="${not empty toShowcustomerPurchaseLimit}">
	                            <div class="limit-1 inline">
	                                <fmt:message key="tru.productdetail.label.limit"/>&nbsp;${toShowcustomerPurchaseLimit}&nbsp;<fmt:message key="tru.productdetail.label.itemsPerCustomer"/>
	                            </div>
                            </c:if>
</dsp:page>