<dsp:page>
<!-- Start Collections template -->
<a id="scrollToTop" href="javascript:void(0);">To Top</a>
	<div class="container-fluid collections-template">
		<div class="modal fade in pdpModals" id="esrbratingModel" tabindex="-1"
			role="dialog" aria-labelledby="basicModal" aria-hidden="false">
			<dsp:include page="/jstemplate/esrbMature.jsp">
			</dsp:include>
		</div>

		<div class="modal fade in" id="shoppingCartModal"
			data-target="#shoppingCartModal" tabindex="-1" role="dialog"
			aria-labelledby="basicModal" aria-hidden="false"></div>


		<div class="modal fade in" id="quickviewModal" tabindex="-1"
			role="dialog" aria-labelledby="basicModal" aria-hidden="false">
			<dsp:include page="/jstemplate/quickView.jsp"></dsp:include>
		</div>
		
		<dsp:include page="/jstemplate/includePopup.jsp" />
		
		<div id="detailsPopup" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content sharp-border welcome-back-overlay">
					<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
					<h2>details</h2>
					<p class="abandonCartReminder">
						This message shows how many days we take to ship the item from our warehouse
					</p>
				</div>
			</div>
		</div>
		
		<div id="learnMorePopup" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content sharp-border welcome-back-overlay">
					<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
					<h2>learn more</h2>
					<p class="abandonCartReminder">
						these items is available for customers to go buy at stores only.
					</p>
				</div>
			</div>
		</div>
		
		<div class="modal fade in" id="findInStoreModal" tabindex="-1"
			role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<dsp:include page="/jstemplate/findInStoreInfo.jsp" />
		</div>

		<div class="row row-no-padding">
			<div class="col-md-12 col-no-padding"></div>
		</div>
		<div class="template-content">
			<dsp:include page="/jstemplate/collectionHeader.jsp">
			<dsp:param name="collectionProductInfo" param="collectionProductInfo"/>
			</dsp:include>
			<dsp:include page="/jstemplate/collectionHeroContent.jsp">
			<dsp:param name="collectionProductInfo" param="collectionProductInfo"/>
			</dsp:include>
			<dsp:include page="/jstemplate/collectionProductDetailsSection.jsp">
			<dsp:param name="collectionProductInfo" param="collectionProductInfo"/>
			</dsp:include>
			<hr>
			<%-- <dsp:include page="/jstemplate/recommendedProducts.jsp" /> --%>
			<%-- <dsp:include page="/jstemplate/recentlyViewedSection.jsp" /> --%>
		</div>
		<dsp:include page="/jstemplate/sizeChartTemplate.jsp">
		</dsp:include>
		<dsp:include page="/jstemplate/collectionImageGallery.jsp">
		</dsp:include>
		<dsp:include page="/jstemplate/shoppingcart.jsp">
		</dsp:include>
		<dsp:include page="/jstemplate/collectionsTooltip.jsp">
		</dsp:include>
	</div>
	<!-- /End Collections template -->
</dsp:page>