<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:getvalueof param="productInfo" var="productInfo" />
<dsp:getvalueof param="productId" var="productId" />
<dsp:getvalueof param="selectedSkuId" var="selectedSkuId" />
<dsp:getvalueof param="productInfo.defaultSKU.channelAvailability" var="channelAvailability" />
<dsp:getvalueof param="productInfo.defaultSKU.dropShipFlag" var="dropShipFlag" />
<dsp:getvalueof param="productInfo.colorSizeVariantsAvailableStatus" var="colorSizeVariantsAvailableStatus" />
<dsp:getvalueof param="productInfo.defaultSKU.inventoryStatus" var="inventoryStatus" />
<dsp:getvalueof param="productInfo.defaultSKU.shipWindowMin" var="shipWindowMin" />
<dsp:getvalueof param="productInfo.defaultSKU.shipWindowMax" var="shipWindowMax" />
<dsp:getvalueof param="productInfo.defaultSKU.s2s" var="s2s" />
<dsp:getvalueof param="productInfo.defaultSKU.ispu" var="ispu" />
<dsp:getvalueof param="cookieValue" var="cookieValue" />
<dsp:getvalueof param="locationIdFromCookie" var="locationIdFromCookie" />
<dsp:getvalueof param="storeAvailMsg" var="storeAvailMsg" />
<dsp:getvalueof param="productInfo.defaultSKU.displayName" var="displayName" /> 
<dsp:getvalueof param="productInfo.defaultSKU.primaryImage" var="primaryImage" />
<dsp:getvalueof param="productInfo.defaultSKU.presellable" var="presellable" /> 
<c:if test="${empty s2s}">
<dsp:getvalueof value="N" var="s2s" />
</c:if>
<c:if test="${empty ispu}">
<dsp:getvalueof value="N" var="ispu" />
</c:if>
	<div class="product-availability">
	<c:if test="${colorSizeVariantsAvailableStatus eq 'noColorSizeAvailable' && !presellable}" ><%-- && ((inventoryStatus ne 'outOfStock') || (inventoryStatus eq 'outOfStock' && not empty locationIdFromCookie && not empty storeAvailMsg))}"> --%>
		<div class="available-online">
				<div class="green-list-dot inline"></div>
				<c:choose>
					<c:when test="${channelAvailability eq 'Online Only'}">
	                    <fmt:message key="tru.productdetail.label.availableOnlineOnly" /> <span class="dotClass"></span>
	                    <div class="small-blue inline">
							<%--<button href="javascript:void(0);" class="learn-more learn-more-tooltiplink" data-toggle="popover"><fmt:message key="tru.productdetail.label.learnmore"/></button>--%>	            
 							<button  class="learn-more" data-toggle="modal" data-target="#learnMorePopupOnline"><fmt:message key="tru.productdetail.label.learnmore"/></button>
 						</div> 
				<!-- <div class="learn-more-tooltips hide">
					item available for online			         
			    </div>  -->
	                </c:when>
					<c:when test="${channelAvailability eq 'In Store Only' }">
	                	<fmt:message key="tru.productdetail.label.availableStoreOnly" /> <span class="dotClass"></span>
	                	<div class="small-blue inline">
	            	<%-- <button href="javascript:void(0);" class="learn-more learn-more-tooltiplink" data-toggle="popover"><fmt:message key="tru.productdetail.label.learnmore"/></button> --%>
	            	<a class="learn-more" data-toggle="modal" data-target="#learnMorePopupStore"><fmt:message key="tru.productdetail.label.learnmore"/></a>
	            </div> 
				<!-- <div class="learn-more-tooltips hide">
					these items is available for customers to go buy at stores only			       
			    </div>  -->
	                </c:when>
					<c:otherwise>
	                    <fmt:message key="tru.productdetail.label.availableonlineandStore" /><span class="dotClass"></span>
	                    <div class="small-blue inline">
	            	<%-- <button href="javascript:void(0);" class="learn-more learn-more-tooltiplink" data-toggle="popover"><fmt:message key="tru.productdetail.label.learnmore"/></button> --%>
	            	<button  class="learn-more" data-toggle="modal" data-target="#learnMorePopupBoth"><fmt:message key="tru.productdetail.label.learnmore"/></button>
	            </div> 
				<!--  <div class="learn-more-tooltips hide">
					item available for buying online and at stores			       
			    </div>  -->
	                </c:otherwise>
				</c:choose>
			 </div>
            </c:if>               
		<c:choose>
		<c:when test="${!presellable && ((inventoryStatus eq 'outOfStock' && s2s eq 'N' && ispu eq 'N' && empty locationIdFromCookie ) ||  (inventoryStatus eq 'outOfStock' && not empty locationIdFromCookie && s2s eq 'N' && ispu eq 'N')|| (inventoryStatus eq 'outOfStock' && not empty locationIdFromCookie && empty storeAvailMsg))}">
			<div class="pre-order">
				<p>Out of Stock</p>
			</div>
		</c:when>
		<c:when test="${!presellable && inventoryStatus ne 'outOfStock' }">
			<div class="leaves-warehouse">
				<div class="green-list-dot inline"></div>
				<%-- <fmt:message key="tru.productdetail.label.leavesWarehouse"/> --%>
				<c:choose>
				<c:when test="${(not empty shipWindowMin && not empty shipWindowMax)}">
					<fmt:message key="tru.productdetail.label.usuallyleaveswarehouse"/>&nbsp;${shipWindowMin}<fmt:message key="tru.productdetail.label.minus"/>${shipWindowMax}&nbsp;<fmt:message key="tru.productdetail.label.fullbusinessdays"/> <span class="dotClass"></span>
				</c:when>
				<c:otherwise>
					<!-- usually leaves warehouse in 1-2 full business days -->
					<fmt:message key="tru.productdetail.label.leavesWarehouse"/> <span class="dotClass"></span>
				</c:otherwise>
				</c:choose>
			 	<div class="inline details-inline">
					<%-- <button class="details-tooltip learn-more" data-toggle="popover"><fmt:message
							key="tru.productdetail.label.details" /></button> --%>
							<button  class="learn-more forDetails" data-toggle="modal" data-target="#detailsPopup"><fmt:message
							key="tru.productdetail.label.details" /></button>
				</div> 
			    <div data-toggle="modal" data-target="#detailsPopup">
						<p class="hide" data-toggle="modal" data-target="#detailsPopup">
						<fmt:message key="tru.productdetail.label.avilabilitypopover"/></p>
			    </div>
			</div>
		</c:when>
		</c:choose>
		<c:if test="${!presellable && colorSizeVariantsAvailableStatus eq 'noColorSizeAvailable'}"> <!-- && (not empty ispu && not empty s2s)}"> -->
		<div class="find-in-store">
			<c:choose>
					<c:when test="${not empty locationIdFromCookie}">
					<dsp:getvalueof var="defaultSkuId" param="productInfo.defaultSKU.id"  />
					<dsp:include page="/jstemplate/quickViewStoreAvailMessage.jsp">
						<dsp:param name="skuId" value="${defaultSkuId}"/>
						<dsp:param name="locationId" value="${locationIdFromCookie}"/>
					</dsp:include> <span class="dotClass"></span>
					<c:if test="${!(ispu eq 'N' && s2s eq 'N')}" > 
					<div class="inline">
						<a data-toggle="modal" data-target="#findInStoreModal"
							href="javascript:void(0);" onclick="javascript:return loadFindInStore('${contextPath}','${selectedSkuId}','',$('.QTY-${productId}').val(),this);"><fmt:message key="tru.productdetail.label.selectAnotherStore" /></a>
					</div>
					</c:if>
					</c:when>
					<c:otherwise>
					
					<c:choose>
					<c:when test="${ispu eq 'Y' && s2s eq 'Y'}">
					<div class="green-location-icon inline"></div>
					<span class="find-in-store-message"><fmt:message key="tru.productdetail.label.storeasearlyastoday"/></span>
					</c:when>
					<c:when test="${ispu eq 'Y' && s2s eq 'N'}">
					<div class="green-location-icon inline"></div>
					<span class="find-in-store-message"><fmt:message key="tru.productdetail.label.freestorepickup"/></span>
					</c:when>
					<c:when test="${ispu eq 'N' && s2s eq 'Y'}">
					<div class="green-location-icon inline"></div>
					<span class="find-in-store-message"><fmt:message key="tru.productdetail.label.freestorepickdays"/></span>
					</c:when>
					<c:otherwise>
					<div class="green-location-icon inline no-store-pickup"></div>
					<span class="find-in-store-message"><fmt:message key="tru.productdetail.label.freestorepickupnotavailable"/></span>
					</c:otherwise>
					</c:choose>
					</c:otherwise>
			</c:choose>
			<c:if test="${!(ispu eq 'N' && s2s eq 'N') && empty locationIdFromCookie}">
			 <span class="dotClass"></span>
			<div class="inline">     
				<a data-toggle="modal" data-target="#findInStoreModal"
					href="javascript:void(0);" onclick="javascript:return loadFindInStore('${contextPath}','${selectedSkuId}','',$('.QTY-${productId}').val(),this);"><fmt:message
						key="tru.productdetail.label.findinstore"/></a>
			</div>
			</c:if>
		</div>
		</c:if>
	</div>
</dsp:page>