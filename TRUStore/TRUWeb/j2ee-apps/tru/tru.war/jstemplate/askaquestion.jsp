<dsp:page>
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:getvalueof var="powerReviewsURL" bean="TRUConfiguration.powerReviewsURL"/>
<dsp:getvalueof var="merchantId" bean="TRUConfiguration.merchantId"/>
<dsp:getvalueof var="merchantGroupId" bean="TRUConfiguration.merchantGroupId"/>
<dsp:getvalueof var="powerReviewsApiKey" bean="TRUConfiguration.powerReviewsApiKey"/>
<dsp:getvalueof var="onlinePid" param="pr_page_id"/>
<tru:pageContainer>
	<jsp:body>
			<br><br><br><br><br><br><br>
			<script src="${powerReviewsURL}"></script>
<div id="pr-write"></div>

<script>

	POWERREVIEWS.display.render({
		api_key : '${powerReviewsApiKey}',
		locale : 'en_US',
		merchant_group_id :'${merchantGroupId}',
		merchant_id : '${merchantId}',
		page_id : '${onlinePid}',
		components : {
			Write:'pr-write'
		}
	});
</script>
	</jsp:body>
	</tru:pageContainer>
</dsp:page>
