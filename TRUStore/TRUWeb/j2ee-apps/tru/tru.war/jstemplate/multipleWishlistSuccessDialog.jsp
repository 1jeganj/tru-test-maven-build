<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
<dsp:getvalueof var="pdpRegistryUrl" bean="TRUStoreConfiguration.pdpRegistryUrl" />
<dsp:getvalueof var="createWishListUrl" bean="TRUStoreConfiguration.createWishListUrl" />
<dsp:getvalueof param="skuDisplayName" var="skuDisplayName" />
<dsp:getvalueof param="page" var="page" />
<dsp:getvalueof param="primaryImage" var="primaryImage" />
<c:choose> 
	<c:when test="${(page eq 'quickView')}"> 
	<div class="pdp-modal3">
					<div class="close-btn">
						<img src="${TRUImagePath}images/close.png" data-dismiss="modal" alt="close modal">
					</div>
					<div class="wc3ValidationHeaderAddToRegistry">
					<fmt:message key="tru.productdetail.label.selectawishlist" />
						<!-- Select a Wish List -->
						<c:choose>
						<c:when test="${ page eq 'collection'}">
								<p><fmt:message key="tru.productdetail.label.wishlistfindclickmessage" /><b><fmt:message key="tru.productdetail.label.wishlistselectmessage" /></b></p>
							<!-- <p>Please find the Wish List that will be added to and click <b>SELECT WISH LIST</b></p> -->
						</c:when>
						<c:otherwise>
								<%-- <p><fmt:message key="tru.productdetail.label.wishlistfindmessage" /> <b>${skuDisplayName}</b> <fmt:message key="tru.productdetail.label.wishlistclickmessage" /> <b><fmt:message key="tru.productdetail.label.wishlistselectmessage" /></b></p> --%>
							<p>Please find the Wish List that <b>${skuDisplayName}</b> will be added to and click <b>SELECT WISH LIST</b></p>
						</c:otherwise>
						</c:choose>							
					</div>
					<!--WISH LIST start-->
					<div class="wish-lists">
						<div class="tse-scrollable">
								<div class="tse-content">
									<div class="row"></div>
								</div>
						</div>
					</div>
					<!--WISH LIST end-->					
					<div class="clearfix wishlist-result">
						<p class="result pull-left"><!-- You have --><fmt:message key="tru.productdetail.label.youhavewishlistcount" /> <b><span class="multiple-wishlist-count"></span>
						<!-- >Wish Lists --><fmt:message key="tru.productdetail.label.wishlists" /></b>
						</p>
						<button onclick="javascript:location.href='${createWishListUrl}'" class="pull-right create-btn">
						<fmt:message key="tru.productdetail.label.createanotherwishlist" />
						<!-- Create Another Wish List -->
						</button>
					</div>
	</div>
	</c:when>
<c:otherwise> 
	<div class="pdp-modal3 spark-pdp">
		<div class="image-name-container">
				<c:choose>
						<c:when test="${not empty primaryImage}">
								<div class="mws-product-image"><img src="${primaryImage}?fit=inside|480:480" alt="${skuDisplayName}"></div>								
						</c:when>
						<c:otherwise>
							<div class="mws-product-image"> <img src="${akamaiNoImageURL}" alt="no-image"></div>
						</c:otherwise>
						</c:choose>	
			<div class="mws-product-name">${skuDisplayName}</div>
		</div>
		<div class="wc3ValidationHeaderAddToRegistry">
			<div class="modalHeader">
				<div class="modalTitle">select a wish list</div>	
				<div class="close-btn"><span class="sprite-icon-x-close" data-dismiss="modal"></span></div>
			</div>
			<div class="clearfix wishlist-result">
				 <p class="result"><span>Please select a wish list for the following product.</span>
				</p> 
				<div>
					<span>You have <b class="wishlistCount"></b> wish lists:</span>
					<button onclick="javascript:location.href='${createWishListUrl}'" class="pull-right create-wish-list">
					create wish list
					</button>
					<input type="hidden" id="wishlistCPProductQty" />
				</div>
			</div>
			<div class="wish-lists">
			<div class="mws-heading">
				<span>gift recipient</span>
				<span>event type</span>
				<span>event date</span>
			</div>
				<div class="tse-scrollable">
					<div class="tse-content">
						 <div class="wishlist-holder">
						 </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	 </c:otherwise>
</c:choose> 
	<script type="text/jstemplate" id="multiple-wishlist-js-template">
		<div class="col-md-3 multiple-wishlist-template">
							<div class="pdp-wish-list">
								<b>Gift Recipient</b>
								<p class="gift_recipient_name"></p>
								<b>Event Type</b>
								<p class="event_type_name"></p>
								<b>Event Date</b>
								<p class="event_date"></p>
								<button class="select_wish_list_button">Select Wish List</button>
							</div>
		</div>
	</script>
</dsp:page>