<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="pdpRegistryUrl" bean="TRUStoreConfiguration.pdpRegistryUrl" />
<dsp:getvalueof var="pdpChecklistUrl" bean="TRUStoreConfiguration.pdpChecklistUrl" />
<dsp:getvalueof var="viewRegistryUrl" bean="TRUStoreConfiguration.viewRegistryUrl" />
<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
<dsp:getvalueof param="skuDisplayName" var="skuDisplayName" />
<dsp:getvalueof param="page" var="page" />
<dsp:getvalueof param="primaryImage" var="primaryImage" />
<c:choose> 
<c:when test="${(page eq 'quickView')}">
<div class="pdp-modal1 ">
					<div class="close-btn">
						<span class="sprite-icon-x-close" data-dismiss="modal"></span>
					</div>
					<div class="wc3ValidationHeaderAddToRegistry">
					<fmt:message key="tru.productdetail.label.registryitemsadded" />
						<!-- Items Added	 -->
						<c:choose>
						<c:when test="${ page eq 'collection'}">
						         <p class="registrySuccessMsg"><fmt:message key="tru.productdetail.label.babyregistrysuccessmessage" />&nbsp;<span class="registrySuccessMsgCatID"></span></p>
							<!-- <p class="registrySuccessMsg">You have successfully added these products to your Baby Registry <span class="registrySuccessMsgCatID"></span></p> -->
						</c:when>
						<%-- <c:when test="${ page eq 'compare'}">
							<p class="registrySuccessMsg">You have successfully added this product to your Baby Registry <span class="registrySuccessMsgCatID"></span></p>
						</c:when> --%>
						<c:otherwise>			
						     <p class="registrySuccessMsg"><fmt:message key="tru.productdetail.label.successfullyadded" />&nbsp;<b class="registrySkuDisplayName">"${skuDisplayName}"</b>&nbsp;<fmt:message key="tru.productdetail.label.tobabyregistry" />&nbsp;<span class="registrySuccessMsgCatID"></span></p>	
						<%-- <p class="registrySuccessMsg">You have successfully added the <b class="registrySkuDisplayName">"${skuDisplayName}"</b> to your Baby Registry <span class="registrySuccessMsgCatID"></span></p> --%>
						</c:otherwise>
						</c:choose>	
					</div>
					<!--action-btns start-->
					<div class="action-btns">
						<div class="row">
							<div class="col-md-4">
							         <button onclick="javascript:location.href='${viewRegistryUrl}'"><fmt:message key="tru.productdetail.label.backtomyregistry" />&nbsp;</button>
								<%-- <button onclick="javascript:location.href='${viewRegistryUrl}'">Back to my registry </button> --%>
							</div>
							<div class="col-md-4">  
							         <button onclick="javascript:location.href='${pdpChecklistUrl}'"><fmt:message key="tru.productdetail.label.backtomychecklist" />&nbsp;</button>
								<%-- <button onclick="javascript:location.href='${pdpChecklistUrl}'">Back to my checklist </button> --%>
							</div>
							<div class="col-md-4">  
							         <button data-dismiss="modal"><fmt:message key="tru.productdetail.label.registrycontinueshopping" /></button>
								<!-- <button data-dismiss="modal">Continue Shopping</button> -->
							</div>
						</div>
				</div>
</div>
</c:when>
		<c:otherwise>
			<div class="pdp-modal2 spark_PDP">
					<div class="modalHeader">
						<div class="modalTitle">item(s) added<%-- <fmt:message key="tru.productdetail.label.registryitemsadded" /> --%></div>	
						<div class="close-btn"><span class="sprite-icon-x-close" data-dismiss="modal"></span></div>
					</div>
					<div class="wc3ValidationHeaderAddToRegistry">
						    <span class="gift-sprite-image"></span>
						 <span>You have successfully added an item to your Baby Registry.</span><!-- <span class="registrySuccessMsgCatID"></span> -->		
					</div>
					<div class="image-name-container">					
						<div class="product-image"><b class="registryPrimaryImage"></b>
						<c:choose>
							<c:when test="${not empty primaryImage}">
									<img src="${primaryImage}?fit=inside|480:480" alt="${skuDisplayName}">							
							</c:when>
							<c:otherwise>
								<img src="${akamaiNoImageURL}" alt="no-image">
							</c:otherwise>
						</c:choose>	
						</div>							
						<div class="product-name"><b class="registrySkuDisplayName">${skuDisplayName}</b></div>
					</div>
					<!--action-btns start-->
					<div class="action-btns">
						<!-- <div class="row"> -->
							<div class="btns success">
								<button onclick="javascript:location.href='${viewRegistryUrl}'">view baby registry<%-- <fmt:message key="tru.productdetail.label.backtomyregistry" /> --%></button>
							</div>							
							<div class="btns cancel">
								<button data-dismiss="modal">continue shopping<%-- <fmt:message key="tru.productdetail.label.registrycontinueshopping" /> --%></button>
							</div>
						<!-- </div> -->
					</div>
				</div>  
			</c:otherwise>
</c:choose>
</dsp:page>