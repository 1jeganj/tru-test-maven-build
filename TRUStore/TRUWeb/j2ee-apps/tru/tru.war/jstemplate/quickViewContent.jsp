<dsp:page>
	<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
	<dsp:importbean bean="/com/tru/commerce/locations/TRUStoreAvailabilityDroplet"/>
	<dsp:importbean bean="/com/tru/commerce/droplet/TRUHidePriceDroplet"/>
	<dsp:importbean bean="/atg/commerce/pricing/PriceItem" />
	<dsp:importbean bean="/com/tru/commerce/droplet/TRUItemLevelPromotionDroplet"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	<dsp:getvalueof bean="TRUConfiguration.priceOnCartValue" var="priceOnCartValue" />
	<dsp:getvalueof var="priceDisplay" param="productInfo.defaultSKU.priceDisplay"/>
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
	<dsp:getvalueof param="productInfo.defaultSKU.primaryImage" var="primaryImage" />
	<dsp:getvalueof param="productInfo.productId" var="relatedproductId" />
	<dsp:getvalueof param="productInfo.defaultSKU.id" var="skuId" />
	<dsp:getvalueof param="productInfo.defaultSKU.newItem" var="newItem"></dsp:getvalueof>
	<dsp:getvalueof  param="productInfo.defaultSKU.promotionalStickerDisplay" var="promotionalStickerDisplay"/>
	<dsp:getvalueof  param="productInfo.defaultSKU.reviewRating" var="reviewRating"/>
	<dsp:getvalueof var="promotionCountinPDP" bean="TRUConfiguration.promotionCountinPDP"/>
	<dsp:getvalueof param="productInfo.defaultSKU.unCartable" var="unCartable" />  
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
	<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
<dsp:getvalueof var="quickViewAkamaiImageResizeMap" bean="TRUStoreConfiguration.quickViewAkamaiImageResizeMap" />
<dsp:getvalueof param="productInfo.defaultSKU.onlinePID" var="onlinePID" />
<dsp:getvalueof param="productInfo.defaultSKU.displayName" var="displayName" />
<dsp:getvalueof param="familyName" var="familyName" />
<dsp:getvalueof param="locationIdFromCookie" var="locationIdFromCookie" />
<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
	<dsp:oparam name="output">
	<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
	</dsp:oparam>
</dsp:droplet>
<c:choose>
	<c:when test="${not empty pdpH1}">
        <c:set var="altH1SkuName" value="${pdpH1}"/>  
	</c:when>
	<c:otherwise>
		<c:set var="altH1SkuName" value="${displayName}"/>
	</c:otherwise>
</c:choose>
	<c:forEach var="entry" items="${quickViewAkamaiImageResizeMap}">
		<c:if test="${entry.key eq 'quickViewPrimaryImageBlock'}">
			<c:set var="quickViewPrimaryImageBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'quickViewThumbnailBlock'}">
			<c:set var="quickViewThumbnailBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'quickViewSecImageBlock'}">
			<c:set var="quickViewSecImageBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'quickViewAlt1ImageBlock'}">
			<c:set var="quickViewAlt1ImageBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'quickViewAlt2ImageBlock'}">
			<c:set var="quickViewAlt2ImageBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'QuickViewSwatchImageBlock'}">
			<c:set var="QuickViewSwatchImageBlock" value="${entry.value}" />
		</c:if>
	</c:forEach>
	<input type="hidden" id ="quickViewPrimaryImageBlock" value = "${quickViewPrimaryImageBlock}">
	<input type="hidden" id ="quickViewAlt1ImageBlock" value = "${quickViewAlt1ImageBlock}">

	<dsp:droplet name="/com/tru/utils/TRUPdpURLDroplet">
		<%-- <dsp:param name="productId" value="${relatedproductId}" /> --%>
		<dsp:param name="productId" value="${onlinePID}" />
		<dsp:param name="siteId" bean="/atg/multisite/Site.id" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="productPageUrl" param="productPageUrl" />
		</dsp:oparam>
	</dsp:droplet>
		<c:if test="${not empty locationIdFromCookie}">
		<dsp:droplet name="TRUStoreAvailabilityDroplet">
			<%--locationId we are getting from the Cookie in the TRUStoreAvailabilityDroplet  --%>
			<dsp:param name="skuId" value="${skuId}" />
			<dsp:param name="locationId" value="${locationIdFromCookie}" />
			<dsp:param name="isReqFromAjax" value="true" />
			<dsp:oparam name="available">
				<dsp:getvalueof var="storeAvailMsg" param="storeAvailMsg" />
			</dsp:oparam>
		</dsp:droplet>
	</c:if>
	<div class="modal-lg modal-dialog" id="modal-lg">
            <div class="modal-content sharp-border">
                <a href="javascript:void(0)" class="close-modal" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
                <c:choose>
            	<c:when test="${newItem}">
                <div class="tru-new-product-title"></div>
                </c:when>
                <c:otherwise>
                	<div></div>
                </c:otherwise>
                </c:choose>
                <div class="row">
                    <div class="product-title"><dsp:valueof param="productInfo.defaultSKU.displayName"/></div>
                   <fmt:parseNumber var="reviewRating" type="number" value="${reviewRating}" />
										<div class="star-rating-block">
										<c:forEach begin="1" end="${reviewRating}">
												<div class="star-rating-on"></div>
										</c:forEach>
										<c:forEach begin="1" end="${5-reviewRating}">
												<div class="star-rating-off"></div>
										</c:forEach>
								</div>
                   <%-- <div class="star-rating-on"></div>
                    <div class="star-rating-on"></div>
                    <div class="star-rating-on"></div>
                    <div class="star-rating-on"></div>
                    <div class="star-rating-off"></div>--%>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-7">
                        <div class="quickview-block">
	                    	<c:if test="${not empty promotionalStickerDisplay }">
						   		<div class="our-exclusive">
	                       			<p> ${promotionalStickerDisplay}</p>
	                       		</div> 
						 	</c:if>
                               <%-- <div class="inline">
				          	 <object data="${TRUImagePath}images/svg/GiftFinderTag.svg" id="giftFinderTag" type="image/svg+xml"></object> 
				            </div>--%>
                           	<dsp:getvalueof param="productInfo.defaultSKU.primaryImage" var="primaryImage" />	
                            <c:choose>
                    			<c:when test="${not empty primaryImage}">
                    				<a href="${productPageUrl}">
			                    	<img id="hero-image" class="" src="${primaryImage}${quickViewPrimaryImageBlock}" alt="${altH1SkuName}"> </a>
			                		<!--   <img id="hero-image" class="hero-image-zoom" src="http://pramukhenterprise.com/images/contactus/no_img.jpg"> -->
			                    </c:when>
			                    <c:otherwise>
			                    	<c:choose>
										<c:when test="${not empty akamaiNoImageURL}">
										  	<img id="hero-image" class="hero-image-zoom" src="${akamaiNoImageURL}" alt="${altH1SkuName}">
										</c:when>
										 <c:otherwise>
											<img id="hero-image" class="hero-image-zoom" src="${TRUImagePath}images/no-image500.gif" alt="${altH1SkuName}">
										</c:otherwise>
								 	</c:choose>			                    	
			                    </c:otherwise>
			                 </c:choose>
                        </div>
                    </div>
                
                <dsp:droplet name="/atg/commerce/catalog/SKULookup">
				<dsp:param name="id" value="${skuId}"/>
				<dsp:param name="elementName" value="SKUItem"/>
				<dsp:oparam name="output">
				<dsp:getvalueof param="SKUItem" var="defaultSkuItem"></dsp:getvalueof>
                    
					<dsp:getvalueof var="adjustments" value="${defaultSkuItem.skuQualifiedForPromos}"/>
				</dsp:oparam>						
				</dsp:droplet> 
		
				 <dsp:droplet name="TRUItemLevelPromotionDroplet">
				<dsp:param name="adjustments" value="${adjustments}"/>
				<dsp:oparam name="output">
					<dsp:getvalueof var="rankOnePromotion" param="rankOnePromotion"/>
					<dsp:getvalueof var="promotions" param="promotions"/>
					<dsp:getvalueof var="promotionCount" param="promotionCount"/>
				</dsp:oparam>
				</dsp:droplet>    
                    <div class="col-md-5">
                        <div class="quickview-sticky-price">
						<div class="sticky-price-contents"
							id="quickViewId-${relatedproductId}">
								<div class="buy-1">
								<c:if test="${not empty rankOnePromotion }">	
									<c:choose>
											<c:when test="${not empty rankOnePromotion.description}">
												<dsp:valueof value="${rankOnePromotion.description}" />&nbsp;<span>.</span>
											</c:when>
											<c:otherwise>
												<dsp:valueof value="${rankOnePromotion.displayName}" />&nbsp;<span>.</span>
											</c:otherwise>
										</c:choose>
										<dsp:getvalueof var="rankOnePromotionDetails" value="${rankOnePromotion.promotionDetails}" />
										<c:if test="${not empty rankOnePromotionDetails}">
											<c:choose>
												<c:when test="${fn:startsWith(rankOnePromotionDetails, scheme)}">
													<a href="${rankOnePromotionDetails}" target="_blank" class="cart-seeterm-tooltip">&nbsp;<fmt:message key="tru.productdetail.label.seeterms" /></a>
												</c:when>
												<c:when test="${fn:startsWith(rankOnePromotionDetails, 'www')}">
													<a href="${scheme}${rankOnePromotionDetails}" target="_blank" class="cart-seeterm-tooltip">&nbsp;<fmt:message key="tru.productdetail.label.seeterms" /></a>
												</c:when>
												<c:otherwise>
													<span class="promotionDetailsSpan display-none">${rankOnePromotionDetails}</span>
													<a data-toggle="modal" data-target="#detailsModel" href="#" >&nbsp;<fmt:message key="tru.productdetail.label.seeterms" /></a>
												</c:otherwise>
											</c:choose>
										</c:if>
									</c:if>
							</div>
							<c:choose>
								<c:when test="${priceDisplay eq priceOnCartValue}">
									<dsp:droplet name="TRUHidePriceDroplet">
										<dsp:param name="skuId" value="${skuId}" />
										<dsp:param name="productId" value="${relatedproductId}" />
										<dsp:param name="order" bean="ShoppingCart.current" />
										<dsp:oparam name="true">
											<dsp:include page="/pricing/displayPrice.jsp">
												<dsp:param name="skuId" value="${skuId}" />
												<dsp:param name="productId" value="${relatedproductId}" />
												<dsp:param name="quickViewPrice" value="quickViewPrice" />
											</dsp:include>
										</dsp:oparam>
										<dsp:oparam name="false">
										<c:choose>
											<c:when test="${not empty unCartable && unCartable eq true }">
												<fmt:message key="tru.pricing.label.priceIsNotAvailable" />
											</c:when>
											<c:otherwise>
												<fmt:message key="tru.pricing.label.priceTooLowToDisplay" />
											</c:otherwise>
										</c:choose>
                        				<%-- <fmt:message key="tru.pricing.label.priceTooLowToDisplay" /> --%>
                        			</dsp:oparam>
									</dsp:droplet>
								</c:when>
								<c:otherwise>
									<dsp:include page="/pricing/displayPrice.jsp">
										<dsp:param name="skuId" value="${skuId}" />
										<dsp:param name="productId" value="${relatedproductId}" />
										<dsp:param name="quickViewPrice" value="quickViewPrice" />
									</dsp:include>
								</c:otherwise>
							</c:choose>
							<div class="moreSpecialOffer">
							<c:if test="${not empty promotions }">
								<div class="btn-group special-offer">
									<button class="btn btn-default dropdown-toggle"
										data-toggle="dropdown" aria-expanded="false">
										<div class="special-offer-text">
											<div class="plus-sign"></div>&nbsp;<dsp:valueof value="${promotionCount}" />&nbsp;
											<fmt:message key="tru.productdetail.label.specialoffers"/>
										</div>
									</button>
									<ul class="dropdown-menu" role="menu">

										<dsp:droplet name="ForEach">
											<dsp:param name="array" value="${promotions}" />
											<dsp:param name="elementName" value="promotion" />
											<dsp:oparam name="output">
												<dsp:getvalueof var="promotionId" param="promotion.id" />
												<dsp:getvalueof var="promotionDetails" param="promotion.promotionDetails" />
												<li class="small-grey">
													<div class="inline blue-bullet"></div> 
													<dsp:getvalueof var="desc" param="promotion.description" />
														<c:choose>
															<c:when test="${not empty desc}">
																<dsp:valueof param="promotion.description" />&nbsp;<span>.</span>
															</c:when>
															<c:otherwise>
																<dsp:valueof param="promotion.displayName" />&nbsp;<span>.</span>
															</c:otherwise>
														</c:choose>
														<c:if test="${not empty promotionDetails}">
															<div class="small-blue inline">
																<c:choose>
																		<c:when test="${fn:startsWith(promotionDetails, scheme)}">
																			<a href="${promotionDetails}" target="_blank"><fmt:message key="tru.productdetail.label.details" /></a>
																		</c:when>
																		<c:when test="${fn:startsWith(promotionDetails, 'www' )}">
																			<a href="${scheme}${promotionDetails}" target="_blank"><fmt:message key="tru.productdetail.label.details" /></a>
																		</c:when>
																		<c:otherwise>
																			<span class="promotionDetailsSpan">${promotionDetails}</span>
																			<a data-toggle="modal" data-target="#detailsModel" href="#" ><fmt:message key="tru.productdetail.label.details" /></a>
																		</c:otherwise>
																	</c:choose>	
															</div>
													</c:if>
												</li>
											</dsp:oparam>
										</dsp:droplet>
									</ul>
								</div>
							</c:if></div>
							<dsp:include page="/jstemplate/esrbRating.jsp">
							</dsp:include>
							<input type="hidden" class="pageName" value="quickView" id="pageName"/> <input
								type="hidden" value="${relatedproductId}"
								id="hiddenProductIdQuickView" />
							<dsp:include page="/jstemplate/quickViewColorSizeVariantFragment.jsp">
								<dsp:param param="productInfo" name="productInfo" />
								<dsp:param value="${QuickViewSwatchImageBlock}" name="swatchImageBlock" />
							</dsp:include>
							 <dsp:include page="/jstemplate/resourceBundle.jsp"></dsp:include>
							<dsp:include page="/jstemplate/quickViewAddToCart.jsp">
								<dsp:param name="pageFrom" value="quickView"/>
								<dsp:param param="productInfo" name="productInfo" />
								<dsp:param name="locationIdFromCookie" param="locationIdFromCookie"/>
								<dsp:param name="storeAvailMsg" value="${storeAvailMsg}"/>
							</dsp:include>						
							<dsp:include page="/jstemplate/quickViewProductAvailability.jsp">
							<dsp:param name="selectedSkuId" value="${skuId}" />
								<dsp:param param="productInfo" name="productInfo" />
								<dsp:param name="locationIdFromCookie" param="locationIdFromCookie"/>
								<dsp:param name="storeAvailMsg"  value="${storeAvailMsg}"/>
							</dsp:include>
						</div>

					</div>
                    </div>
                </div>
                <div class="row quickview-footer">
                	<dsp:include page="quickViewImages.jsp">
                	<dsp:param name="quickViewThumbnailBlock" value="${quickViewThumbnailBlock}" />
                	<dsp:param name="quickViewSecImageBlock" value="${quickViewSecImageBlock}" />
                	<dsp:param name="quickViewAlt1ImageBlock" value="${quickViewAlt1ImageBlock}" />
                	<dsp:param name="quickViewAlt2ImageBlock" value="${quickViewAlt2ImageBlock}" />
                	<dsp:param name="productPageUrl" value="${productPageUrl}"/> 
					</dsp:include>
                </div>
            	</div>
        	</div>
      <dsp:include page="/product/tealiumForQuickView.jsp">
				<dsp:param name="productInfo" param="productInfo"/>  
				<dsp:param name="familyName" value="${familyName}" />
	</dsp:include>	
        	
</dsp:page>