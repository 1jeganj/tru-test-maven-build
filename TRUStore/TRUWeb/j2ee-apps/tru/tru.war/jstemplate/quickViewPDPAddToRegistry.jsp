<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="pdpRegistryUrl" bean="TRUStoreConfiguration.pdpRegistryUrl" />
<dsp:getvalueof var="pdpWishlistUrl" bean="TRUStoreConfiguration.pdpWishlistUrl" />
<dsp:getvalueof param="productInfo" var="productInfo"></dsp:getvalueof>
<dsp:getvalueof param="registryWarningIndicator" var="registryWarningIndicator"></dsp:getvalueof>
<dsp:getvalueof param="skuRegistryEligibility" var="skuRegistryEligibility"></dsp:getvalueof>
<dsp:getvalueof var="registry_id" param="registry_id" />
<dsp:getvalueof var="wishlist_id" param="wishlist_id" />
				<c:choose>
					<c:when
						test="${not empty registryWarningIndicator && registryWarningIndicator eq 'ALLOW' && skuRegistryEligibility eq true}">
						<div class="inline">
							<c:choose>
								<c:when test="${empty registry_id}">
									<a href="${pdpRegistryUrl}"
										class="small-orange cart-button-enable"><fmt:message
											key="tru.productdetail.label.babyregistry" />&nbsp;</a>
								</c:when>
								<c:otherwise>
									<a href="javascript:void(0);"
										class="small-orange cart-button-enable cookie-registry"
										data-registry="registry"><fmt:message
											key="tru.productdetail.label.babyregistry" />&nbsp;</a>
								</c:otherwise>
							</c:choose>
							<div class="grey-arrow-icon inline"></div>
							<div class="inline">
								<c:choose>
									<c:when test="${empty wishlist_id}">
										<a href="${pdpWishlistUrl}"
											class="small-orange cart-button-enable"><fmt:message
												key="tru.productdetail.label.wishlist" />&nbsp;</a>
									</c:when>
									<c:otherwise>
										<a href="javascript:void(0);"
											class="small-orange cart-button-enable cookie-wishlist"
											data-registry="wishlist"><fmt:message
												key="tru.productdetail.label.wishlist" />&nbsp;</a>
									</c:otherwise>
								</c:choose>
							</div>
							<div class="grey-arrow-icon inline"></div>
						</div>
					</c:when>
					<c:when test="${registryWarningIndicator eq 'WARN' && skuRegistryEligibility eq true}">
						<div class="inline">
							<c:choose>
								<c:when test="${empty registry_id}">
									<a href="${pdpRegistryUrl}"
										class="small-orange cart-button-enable warnRegistryPopover"
										data-toggle="popover" data-placement="bottom"
										data-content='<fmt:message key="tru.productdetail.label.pdpwarnregistrymessage" />'><fmt:message
											key="tru.productdetail.label.babyregistry" />&nbsp;</a>
								</c:when>
								<c:otherwise>
									<a href="javascript:void(0);"
										class="small-orange cart-button-enable cookie-registry warnRegistryPopover"
										data-registry="registry" data-toggle="popover"
										data-placement="bottom"
										data-content='<fmt:message key="tru.productdetail.label.pdpwarnregistrymessage" />'><fmt:message
											key="tru.productdetail.label.babyregistry" />&nbsp;</a>
								</c:otherwise>
							</c:choose>
							<div class="grey-arrow-icon inline"></div>
							<div class="inline">
								<c:choose>
									<c:when test="${empty wishlist_id}">
										<a href="${pdpWishlistUrl}"
											class="small-orange cart-button-enable warnRegistryPopover"
											data-toggle="popover" data-placement="bottom"
											data-content='<fmt:message key="tru.productdetail.label.pdpwarnwishlistmessage" />'><fmt:message
												key="tru.productdetail.label.wishlist" />&nbsp;</a>
									</c:when>
									<c:otherwise>
										<a href="javascript:void(0);"
											class="small-orange cart-button-enable warnRegistryPopover cookie-wishlist"
											data-registry="wishlist" data-toggle="popover"
											data-placement="bottom"
											data-content='<fmt:message key="tru.productdetail.label.pdpwarnwishlistmessage" />'><fmt:message
												key="tru.productdetail.label.wishlist" />&nbsp;</a>
									</c:otherwise>
								</c:choose>
							</div>
							<div class="grey-arrow-icon inline"></div>
						</div>
					</c:when>
					<c:when
						test="${registryWarningIndicator eq 'DENY' || skuRegistryEligibility eq false}">
						<div class="inline">
							<a href="javascript:void(0);" class="small-orange cart-button-disable denyWarningRegistryPopover"
								data-toggle="popover"
								data-placement="bottom"
								data-content='<fmt:message key="tru.productdetail.label.pdpdenyregistrymessage" />'><fmt:message
									key="tru.productdetail.label.babyregistry" />&nbsp;</a>
						</div>
						<div class="grey-arrow-icon inline"></div>
						<div class="inline">
							<a href="javascript:void(0);" class="small-orange cart-button-disable denyWarningRegistryPopover"
								data-toggle="popover"
								data-placement="bottom"
								data-content='<fmt:message key="tru.productdetail.label.pdpdenywishlistmessage" />'><fmt:message
									key="tru.productdetail.label.wishlist" />&nbsp;</a>
						</div>
						<div class="grey-arrow-icon inline"></div>
					</c:when>
					<c:otherwise>
						<div class="inline">
							<a href="javascript:void(0);" class="small-orange cart-button-disable denyWarningRegistryPopover"
								data-toggle="popover"
								data-placement="bottom"
								data-content='<fmt:message key="tru.productdetail.label.pdpdenyregistrymessage" />'><fmt:message
									key="tru.productdetail.label.babyregistry" />&nbsp;</a>
						</div>
						<div class="grey-arrow-icon inline"></div>
						<div class="inline">
							<a href="javascript:void(0);" class="small-orange cart-button-disable denyWarningRegistryPopover"
								data-toggle="popover"
								data-placement="bottom"
								data-content='<fmt:message key="tru.productdetail.label.pdpdenywishlistmessage" />'><fmt:message
									key="tru.productdetail.label.wishlist" />&nbsp;</a>
						</div>
						<div class="grey-arrow-icon inline"></div>
					</c:otherwise>
				</c:choose>
</dsp:page>