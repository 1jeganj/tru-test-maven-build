<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean	bean="/atg/commerce/catalog/comparison/ProductListContains" />
<dsp:importbean bean="/atg/commerce/catalog/comparison/ProductList" />
<dsp:importbean bean="/com/tru/commerce/droplet/TRUClearProductsInCompareListDroplet" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof var="productID" param="productID" />
<dsp:getvalueof var="catID" param="catID" />
	<div class="product-choose">
		<div class="product-compare-checkbox-container">
			<input type="hidden" value="${productID}" class="productId" /> 
			<input type="hidden" value="${catID}" class="categoryId" />
			<dsp:droplet name="ProductListContains">
				<dsp:param bean="ProductList" name="productList" />
				<dsp:param value="${productID}" name="productID" />
				<dsp:param value="${catID}" name="categoryID" />
				<dsp:oparam name="true">
					<div class="product-compare-checkbox-new compareChecked"></div>

				</dsp:oparam>
				<dsp:oparam name="false">
					<div class="product-compare-checkbox-new compareUnChecked"></div>
				</dsp:oparam>
			</dsp:droplet>

			<label class="">compare</label>
		</div>
	</div>
</dsp:page>