<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
<dsp:importbean bean="/com/tru/common/TRUIntegrationConfiguration" />
<dsp:importbean bean="/com/tru/common/TRUSOSIntegrationConfiguration" />
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
<dsp:getvalueof var="enableInvodoFlag" bean="TRUStoreConfiguration.enableInvodoFlag" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof param="productInfo" var="productInfo" />
<dsp:getvalueof param="productInfo.defaultSKU.primaryCanonicalImage" var="primaryCanonicalImage" />
<dsp:getvalueof param="productInfo.defaultSKU.secondaryCanonicalImage" var="secondaryCanonicalImage" />
<dsp:getvalueof param="productInfo.defaultSKU.alterNateCanonicalImages" var="alterNateImages" />
<dsp:importbean bean="/atg/multisite/Site"/>
<dsp:getvalueof bean="Site.virtucommURL" var="virtucommURL"></dsp:getvalueof>
<dsp:getvalueof param="productInfo.defaultSKU.onlinePID" var="onlinePid"/>
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
<dsp:getvalueof var="storeEnableHookLogic" bean="TRUIntegrationConfiguration.enableHookLogic"/>
<dsp:getvalueof var="sosEnableHookLogic" bean="TRUSOSIntegrationConfiguration.enableHookLogic"/>
<dsp:getvalueof param="primaryImageThumbnailBlock" var="primaryImageThumbnailBlock"/>
<dsp:getvalueof param="secondaryImageThumbnailBlock" var="secondaryImageThumbnailBlock"/>
<dsp:getvalueof param="alternateImageThumbnail1Block" var="alternateImageThumbnail1Block"/>
<dsp:getvalueof param="alternateImageThumbnail2Block" var="alternateImageThumbnail2Block"/>
<dsp:getvalueof param="productInfo.defaultSKU.displayName" var="displayName" />
<dsp:getvalueof param="productInfo.whyWeloveIt" var="whyWeloveIt" />
<c:set var="primaryImage" value="${fn:escapeXml(primaryCanonicalImage)}"/>
<c:set var="secondaryImage" value="${fn:escapeXml(secondaryCanonicalImage)}"/>
<c:choose>
	<c:when test="${not empty pdpH1}">
        <c:set var="altH1SkuName" value="${pdpH1}"/>  
	</c:when>
	<c:otherwise>
		<c:set var="altH1SkuName" value="${displayName}"/>
	</c:otherwise>
</c:choose>
<dsp:droplet name="GetSiteTypeDroplet">
	<dsp:param name="siteId" value="${site}"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="site" param="site"/>
		<c:set var="site" value ="${site}" scope="request"/>
	</dsp:oparam>
</dsp:droplet>

<c:choose>
	<c:when test="${site eq 'sos'}">
		<c:set var="hookLogicEnabled" value="${sosEnableHookLogic}" scope="request" />
	</c:when>
	<c:otherwise>
		<c:set var="hookLogicEnabled" value="${storeEnableHookLogic}" scope="request" />
	</c:otherwise>
</c:choose>
<div class="pdp-product-details-block">
        <div class="product-details">
            <div>
            	<c:if  test="${not empty primaryImage || not empty alterNateImages || not empty secondaryImage}" >
                <div id="stylized_thumbnail_container" class="thumb-container">
                <!-- when webcollage is  available need to uncomment the block for product tour --> 
                   <%--  <a href="#" class="product-tour-container">
                        <div class="inline product-tour">
                          <fmt:message key="tru.productdetail.label.producttour" />
                        </div>
                    </a>--%>
<%--                     <!-- product Tour -->
                    <div id="wc-mini-site-button"></div>
                     <!--  View Demo -->
                 <spark UI change>
                    <c:if test="${not empty vendorsProductDemoUrl}">
                    <a href="" class="product-tour-container" onclick="window.open( '${vendorsProductDemoUrl}', '','height=450,width=670' ); return false" target="_blank">
                        <div class="inline product-tour">
                             <fmt:message key="tru.productdetail.label.viewdemo"/>
                        </div>
                    </a>
                    </c:if> 
                     </spark UI change>  --%>
                    <c:choose>
                     <c:when test="${not empty primaryImage}">
                     
                    <a class="block-add selectedImage" href="javascript:void(0)" title="product image thumbnail">
                        <img class="uploadImage" id="firstAlternate" src="${primaryImage}${primaryImageThumbnailBlock}" alt="${altH1SkuName}" />
                    </a>
                   
                    </c:when>
                    <c:when test="${(empty primaryImage && not empty alterNateImages) || (empty primaryImage && not empty secondaryImage)}">
                   
                    <a class="block-add selectedImage" href="javascript:void(0)" title="product image thumbnail">
                    		<c:choose>
								<c:when test="${not empty akamaiNoImageURL}">
									<img class="uploadImage" id="firstAlternate" src="${akamaiNoImageURL}${primaryImageThumbnailBlock}" alt="${altH1SkuName}" />
								</c:when>
								<c:otherwise>
								<img class="uploadImage" id="firstAlternate" src="${TRUImagePath}images/no-image500.gif" alt="${altH1SkuName}" />
									<%-- <img class="uploadImage selectedImage" id="firstAlternate" src="${TRUImagePath}images/no-image500.gif" alt="${altH1SkuName}" /> --%>
								</c:otherwise>
							</c:choose>
                    </a>
                    
                    </c:when>
                    </c:choose>
                    <c:if test="${not empty secondaryImage}">
                    
                   		<a class="block-add" href="javascript:void(0)" title="secondary image thumbnail">
                        <img class="uploadImage" id="secondAlternate" src="${secondaryImage}${secondaryImageThumbnailBlock}" alt="${altH1SkuName}" />
                    </a>
                    
                    </c:if>
                   
                    <span id="alternatePDPImg" >
                     <c:choose>
						<c:when test="${empty secondaryImage && not empty alterNateImages }">
						      <c:forEach begin="0" end="3" items="${alterNateImages}" var="alterNateImage">
						      		<a class="block-add" href="javascript:void(0)" title="alternate image thumbnail">
						        	 <img class="uploadImage" src="<c:out value="${alterNateImage}" escapeXml="true"/>${alternateImageThumbnail1Block}" alt="${altH1SkuName}" />
						        	 </a>
						      </c:forEach>
						</c:when>
						<c:otherwise>
							<c:if test="${not empty alterNateImages}">
						      <c:forEach begin="0" end="2" items="${alterNateImages}" var="alterNateImage">
						     	 <a class="block-add" href="javascript:void(0)" title="alternate image thumbnail">
									<img class="uploadImage" src="<c:out value="${alterNateImage}" escapeXml="true"/>${alternateImageThumbnail1Block}" alt="${altH1SkuName}" />
								 </a>
							   </c:forEach>
							</c:if>
						</c:otherwise>
					</c:choose>
                    </span>
                    <c:if test="${not empty enableInvodoFlag && enableInvodoFlag}" >
					 <a class="block-add invodoThumbContainer noborder"><div id="invodoModalButton"></div></a>
					 </c:if>
					<%-- <div class="see-more-images">
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#galleryOverlayModal" onclick="forConsoleErrorFixing(this)"><fmt:message key="tru.productdetail.label.seemoreimages" /></a>
                    </div> --%>
                </div>
                </c:if>
                <div>
                 	<dsp:include page="/jstemplate/socialShareLinks.jsp"> 
                    	<dsp:param name="" value=""/>
                   </dsp:include>
                </div>
	                <dsp:include page="textZoneMFGSection.jsp">
	                <dsp:param name="productInfo" param="productInfo"/>
	                </dsp:include>
				<hr class="Divider">
				<c:choose>
					<c:when test="${not empty whyWeloveIt}">
						 <h2 class="why-we-love-it"><fmt:message key="tru.productdetail.label.whyWeLoveIt" /></h2>
					</c:when>
					<c:when test="${not empty productInfo.defaultSKU.longDescription}">
						<h2 class="productdesc-aboutthistoy"><fmt:message key="tru.productdetail.label.productdescription" /></h2>
					</c:when>
				</c:choose>
                <div class="top-product-details-content" id="cms">
                    <div class="pdp-cms-ad-zone-content">
	                    <div class="right-section">
		                   <dsp:include page="adZonePDP2.jsp">
							</dsp:include>
							<dsp:include page="CMSSpot.jsp">
								<dsp:param name="productInfo" param="productInfo"/>
							</dsp:include>
							<div class="clearfix"></div> 
						</div>
					 <div class="clearfix"></div>
					 </div>
					 <dsp:include page="whyWeLoveItSection.jsp">
                   		</dsp:include>
					 <c:if test="${not empty productInfo.defaultSKU.longDescription}" >
					 	<c:if test="${not empty whyWeloveIt}">
					 		<h2 class="productdesc-aboutthistoy"><fmt:message key="tru.productdetail.label.productdescription" /></h2>
					 	</c:if>
					  <div class="main-rail">
	                    <div class="content-productdesc-about-history">
	                    <dsp:droplet name="/com/tru/commerce/droplet/TRUBigStringDecoderDroplet">
	                    	<dsp:param name="data" param="productInfo.defaultSKU.longDescription"/>
	                    	<dsp:oparam name="output">
	                    		<dsp:valueof param="enocdeData" valueishtml="true" />
	                       	</dsp:oparam>
	                    </dsp:droplet>
	                    <%-- <dsp:valueof param="productInfo.defaultSKU.longDescription" valueishtml="true" /> --%>
	                    </div>
                    </div>
					</c:if>
					<div class="clearfix"></div>
					
					 <!--  Need to work on  -->
					 <c:choose>
						<c:when test="${not empty productInfo.defaultSKU.longDescription}">
							<div class="product-details-content">
						</c:when>
						<c:otherwise>
							<div class="product-details-content-new">
						</c:otherwise>
					</c:choose>            
                	</div>
                	<dsp:include page="thingsToKnowSection.jsp">
                     	<dsp:param name="productInfo" param="productInfo"/>
                     </dsp:include>
                    <dsp:include page="readMoreContent.jsp">
                   	 <dsp:param name="productInfo" param="productInfo"/>
					</dsp:include>
                <div>
	                <div class="read-more text-center" id="readmore"  tabindex="0">
	                	<fmt:message key="tru.productdetail.label.readmore" />
	                </div>
                </div>
                
            </div>
           
            <div class="modal fade" id="productVideoModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                <div class="modal-dialog warning-modal">
                    <div class="modal-content sharp-border">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="document.getElementById('ytplayer').stopVideo();">X</button>
                            <div>
                                <div class="row">
                                    <div class="col-md-12">
                                         <object id="ytplayer" type="application/x-shockwave-flash" data="http://www.youtube.com/v/wL60hEXAL7s?version=3&amp;enablejsapi=1">
                                            <param name="allowScriptAccess" value="always">
                                            <embed id="ytplayer1" src="http://www.youtube.com/v/wL60hEXAL7s?version=3&amp;enablejsapi=1" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="590" height="390">
                                        </object>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <dsp:include page="/jstemplate/batteryProducts.jsp">
		<dsp:param name="productInfo" param="productInfo"/>
	</dsp:include>
	<c:if test="${not empty virtucommURL}"> 
    <div class="pdp-enhanced-description-block">
		<div class="pdp-enhanced-description">
	        <div class="enhanced-content">
	        	<h2 class="no-enhanced-content"><fmt:message key="tru.productdetail.label.enhanceddescription" /></h2>
	            <esi:include src="${virtucommURL}" onerror="continue" />
	       </div>    
       	</div>
    </div>
    </c:if>   
	 <dsp:include page="/jstemplate/manufacturerSection.jsp">
	        </dsp:include>
	 		        
	<input type="hidden" id="hookLogicEnabledFlag" value="${hookLogicEnabled}" />
	  <c:if test="${hookLogicEnabled eq true}"> 
			<dsp:include page="/browse/hooklogicPdp.jsp" >
			 <dsp:param name="productInfo" param="productInfo"/>
			</dsp:include>
	  </c:if>    
	        
    </dsp:page>