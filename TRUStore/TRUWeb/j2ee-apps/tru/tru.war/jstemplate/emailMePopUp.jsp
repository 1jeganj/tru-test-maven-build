<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath"/>
<dsp:getvalueof param="displayName" var="displayName" />
<dsp:getvalueof param="primaryImage" var="primaryImage" />
<dsp:getvalueof param="skuRepoId" var="skuRepoId"/>
<dsp:getvalueof param="pdpPageURL" var="pdpPageURL"/>
<dsp:getvalueof param="productId" var="productId"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/com/tru/commerce/email/TRUEmailNotificationFormHandler"/>
<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof bean="TRUConfiguration.recaptchaScriptURL" var="recaptchaScriptURL" />
<dsp:getvalueof bean="TRUConfiguration.recaptchaNoScriptURL" var="recaptchaNoScriptURL" />
<dsp:getvalueof bean="TRUConfiguration.recaptchaPublicKey" var="recaptchaPublicKey" />
<dsp:getvalueof bean="TRUStoreConfiguration.emailLength" var="emailLength" />
<c:choose>
	<c:when test="${not empty pdpH1}">
        <c:set var="altH1SkuName" value="${pdpH1}"/>  
	</c:when>
	<c:otherwise>
		<c:set var="altH1SkuName" value="${displayName}"/>
	</c:otherwise>
</c:choose>
<!-- Email Me- Friend Popup Start-->
        <div class="modal-dialog modal-lg">
            <div class="modal-content sharp-border">
                <div class="my-account-add-address-overlay email-a-friend">
				<div class="col-md-12 email-friend-heading">
                                    <a href="javascript:void(0)" class="close-modal pull-right" data-dismiss="modal" ><span class="sprite-icon-x-close"></span></a>
				    <h3 class="col-md-6"><fmt:message key="tru.productdetail.label.tellafriend"/> </h3>
                                </div>
				<div class="row pdp-email-me-friend-show">
					<div class="col-md-5">
						<div class="row">
							<div class="col-md-12">
								<%-- <p class="product-title">
									${displayName}
								</p> --%>
								<c:choose>
									<c:when test="${not empty primaryImage}">
										<div class="tell-friend-img">
											<img id="hero-image" src="${primaryImage}" alt="${altH1SkuName}">
										</div>
									</c:when>
									<c:otherwise>
										<div class="tell-friend-img">
											<c:choose>
										 <c:when test="${not empty akamaiNoImageURL}">
										  		<img id="hero-image" src="${akamaiNoImageURL}"  alt="${altH1SkuName}">
										 </c:when>
										 <c:otherwise>
										 	<img id="hero-image" src="${TRUImagePath}images/no-image500.gif"  alt="${altH1SkuName}"> 
										 </c:otherwise>
									 </c:choose>	
										</div>
									</c:otherwise>
								</c:choose>
								<p class="product-title">
									${displayName}
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-7  emailpdleft">
					<div class="col-md-12" id="emailme">
					</div>
						<form id="emailToFriend" name="emailToFriend" class="formValidation" novalidate="novalidate">
							<div class="row">
								<div class="col-md-12">
								<!-- <p class="avenir-heavy">
										<span class="required-asterisk">*</span> required
									</p> -->
								</div>
							</div>
							<div class="tse-scrollable">
								
							
								<div class="tse-scroll-content">
									<div class="tse-content">
										<div class="row">
											<div class="col-md-9">
											<div class="field-container">
 												<input type="text" id="friendsName" name="friendsname" required placeholder="*friend's name"/> 
 												 <label for="friendsName" class="floating-label">
													<span class="avenir-heavy">
														<%-- <span class="required-asterisk">*</span>--%>
														<fmt:message key="tru.productdetail.label.friendsname"/> 
													</span>
												</label>
												<!-- <div class="field-underline"></div> -->
											</div>
											<div class="field-container">
												<input type="text" id="friendsemail" required placeholder="*friend's email" name="friendsemail" maxlength="${emailLength}">	
												<label for="friendsemail" class="floating-label">
													<span class="avenir-heavy">
														<%--<span class="required-asterisk">*</span>--%>
														<fmt:message key="tru.productdetail.label.friendsemail"/>
													</span>
												</label>
											</div>
											<div class="field-container">	
												<input type="text" id="yourname" required placeholder="*your name" name="yourname">
												<label for="yourname" class="floating-label">
													<span class="avenir-heavy">
														<%--<span class="required-asterisk">*</span>--%>
														<fmt:message key="tru.productdetail.label.yourname"/> 
													</span>
												</label> 
											</div>
											<div class="field-container">
												<input type="text" id="youremail" name="youremail" required placeholder="*your email" maxlength="${emailLength}">	
												 <label for="youremail" class="floating-label">
													<span class="avenir-heavy">
														<%--<span class="required-asterisk">*</span>--%>
														<fmt:message key="tru.productdetail.label.yourmail"/>
													</span>
												</label> 
											</div>
												
												 <label for="personalMessage">
												
													<span class="avenir-heavy">
														<!-- <span class="required-asterisk">*</span> --><%-- <fmt:message key="tru.productdetail.label.personalmessage"/> --%>
													</span>
												</label>
										 <textarea rows="4" cols="50" name="personalMessage" id="personalMessage"></textarea>
																				<br/>
											<div class="g-recaptcha" data-sitekey="6Lf9VRgTAAAAAOpAj21f3y7L2vdyVMlPgE8iextV" data-callback="loopTheForm"></div>
           									<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=en"></script>
												<input type="hidden" name="displayName" id="displayName" value="${displayName}">
												  <input type="hidden" name="skuId" id="skuId" value="${skuRepoId}">
												   <input type="hidden" name="collectionEmailProductId" id="collectionEmailProductId" value="${productId}">
												    <input type="hidden" name="pdpPageURL" id="pdpPageURL" value="${pdpPageURL}">
											<%-- 	<label>
													<p class="avenir-heavy">
														<span class="required-asterisk">*</span>verification code
													</p>
												</label>
												<div class="verification-code">
													<img src="${TRUImagePath}images/bar-code.jpg" alt="reader" />
													<div class="text-code">
														<div class="text-fields">
															<input type="text" value="Type the text"
																palceholder="Type the text" /> <span>privacy &
																term</span>
														</div>
														<div class="controles">
															<img src="${TRUImagePath}images/refresh.gif" alt="refresh" /> <img
																src="${TRUImagePath}images/audio.gif" alt="audio" /> <img
																src="${TRUImagePath}images/help.gif" alt="help" />
														</div>
														<div class="load-refresh">
															<p class="refresh-sprite"></p>
														</div>

													</div>

												</div> --%>
												<div class="declaration">
													<p><fmt:message key="tru.productdetail.label.contactinfo"/></p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row top-border attach-to-bottom">
								<div class="col-md-12">
									<button type="button" id="sendFormData" class="disable"><fmt:message key="tru.productdetail.label.send"/></button>
								</div>
								
							</div>
							</form>
					</div>

				</div>
				<div class="pdp-email-friend-Success hide">
					<div class="col-md-12">
						<a href="javascript:void(0)" class="close-modal pull-right" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
						<h3 class="col-md-6">Thank You...</h3>
					</div>
					<div class="row pdp-email-friend-product-title">
						<!-- <div class="inline notify-me-text col-md-6">We will notify
							you when this item is back in stock.</div> -->
						<div class="col-md-6 email-friend-image-title">
							<c:choose>
								<c:when test="${not empty primaryImage}">
									<img alt="${primaryImage}" src="${primaryImage}">
								</c:when>
								<c:otherwise>
									<%-- <img alt="${TRUImagePath}images/no-image500.gif"
										src="${TRUImagePath}images/no-image500.gif" /> --%>
										 <img alt="${akamaiNoImageURL}"
										src="${akamaiNoImageURL}" />
								</c:otherwise>
							</c:choose>
							<p class="notify-me-text">${displayName}</p>
						</div>
					</div>
				</div> 
			</div>
            </div>
        </div>


</dsp:page>
