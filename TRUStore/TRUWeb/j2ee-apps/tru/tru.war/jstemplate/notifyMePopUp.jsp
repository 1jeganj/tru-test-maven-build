<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
<dsp:getvalueof param="itemDescription" var="itemDescription" />
<dsp:getvalueof param="itemName" var="itemName" />
<dsp:getvalueof param="itemNumber" var="itemNumber"/>
<dsp:getvalueof param="productId" var="productId"/>
<dsp:getvalueof param="skn" var="skn"/>
<dsp:getvalueof param="itemURL" var="itemURL"/>
<dsp:getvalueof param="helpURLName" var="helpURLName"/>
<dsp:getvalueof param="helpURLValue" var="helpURLValue"/>
<dsp:getvalueof param="primaryImage" var="primaryImage" />
<dsp:getvalueof param="siteID" var="siteID" />
<dsp:getvalueof var="onlinePID" param="onlinePID" />
<dsp:getvalueof bean="TRUStoreConfiguration.emailLength" var="emailLength" />
<c:choose>
	<c:when test="${empty skn}">
	<c:set var="productId" value="${productId}"/>	
	</c:when>	
	<c:otherwise>
	<c:set var="productId" value="${skn}"/>	
	</c:otherwise>
</c:choose>
<%-- <div class="modal-dialog notify-me-modal spark-notify-me">
		<div class="modal-content sharp-border">
			<div class="modal-header">
				<a href="javascript:void(0)" class="close-modal" data-dismiss="modal" title="close model"><span class="sprite-icon-x-close"></span></a>
				<form id="notifyMe" name="notifyMe" class="formValidation" method="post"
					action="javascript:returnFalse(); return false;" novalidate="novalidate">
					<div class="col-md-12" id="errorNotifyMe"></div>
					<div class="notify-me-header">
						<div class="notify-me-title">
							<fmt:message key="tru.productdetail.label.notifyme" />
						</div>
						<div class="row">
							<div class="notify-me-wrapper col-md-6">
								<input class="inline" id="mailId" name="notifyMeMailId"
									type="text" placeholder="<fmt:message key="tru.productdetail.label.ghosttext" />" maxlength="${emailLength}"> 
									<input type="hidden" id="itemDescription" value="${itemDescription}">
									
									<div class="NotifyMeDescription hide">${itemDescription}</div>
								<input type="hidden" id="itemName" value="${itemName}">
								<input type="hidden" id="productId" value="${productId}">
								<input type="hidden" id="itemNumber" value="${itemNumber}">
								<input type="hidden" id="itemURL" value="${itemURL}"> <input type="hidden" id="helpURLName" value="${helpURLName}">
								<input type="hidden" id="helpURLValue" value="${helpURLValue}">
								<input type="hidden" id="siteID" value="${siteID}">
								<input type="hidden" id="onlinePID" value="${onlinePID}">
								<button id="notifyMeMail" class="inline notify-me-submit-button" type="submit">submit</button>
							</div>
							<div class="col-md-6 notifyme-image-title">
							<c:choose>
							<c:when test="${not empty primaryImage}">
								  <img alt="${primaryImage}" src="${primaryImage}">
							</c:when>
							<c:otherwise>
									<c:choose>
										<c:when test="${not empty akamaiNoImageURL}">
										  	<img alt="${akamaiNoImageURL}" src="${akamaiNoImageURL}" /> 
										</c:when>
										 <c:otherwise>
											<img alt="${TRUImagePath}images/no-image500.gif" src="${TRUImagePath}images/no-image500.gif" />  
										</c:otherwise>
								 	</c:choose>	
							</c:otherwise>
						</c:choose>
								<p class="notify-me-text">${itemName}</p>
							</div>
						</div>
						<div class="notify-me-footer">
							<div class="over-17 inline">
								<div class="modal-checkbox-container">
									<input class="modal-checkbox notify-checkbox"
										id="pdp-notify-checkbox" name="checkbox" type="checkbox">
									<label for="pdp-notify-checkbox"></label>
								</div>
								<div class="inline notify-me-text"><fmt:message key="tru.productdetail.label.notifymecheckboxmessage" /></div>
								<div class="inline">
									<a href="#" data-toggle="modal" data-target="#privacyPolicyModal"><fmt:message
											key="tru.productdetail.label.privacypolicy" /></a>
								</div>
							</div>
						</div>
					</div>
					 <div class="notify-me-header notifymeSuccess hide">
						<div class="notify-me-title">Thank You...</div>
						<div class="row">
							<div class="inline notify-me-text col-md-6">We will notify
								you when this item is back in stock.</div>
							<div class="col-md-6 notifyme-image-title">
								<c:choose>
									<c:when test="${not empty primaryImage}">
										<img alt="${primaryImage}" src="${primaryImage}"/>
									</c:when>
									<c:otherwise>
									<!--	<img alt="${TRUImagePath}images/no-image500.gif" src="${TRUImagePath}images/no-image500.gif" /> -->
										<img alt="${akamaiNoImageURL}" src="${akamaiNoImageURL}" />
									</c:otherwise>
								</c:choose>
								<p class="notify-me-text">${itemName}</p>
							</div>
						</div>
					</div> 
				</form>
			</div>
		</div>
	</div>
	 --%>
	
	<div class="modal-dialog notify-me-modal spark-notify-me">
		<div class="modal-content sharp-border">
			<div class="modalHeader">
				<div class="modalTitle">email me when available</div>	
				<div class="close-btn"><a href="javascript:void(0)" data-dismiss="modal" title="close model"><span class="sprite-icon-x-close"></span></a></div>
			</div>
			<div class="modal-header">
				<p class="email-me-product-title">${itemName}</p>
				<form id="notifyMe" name="notifyMe" class="formValidation" method="post" action="javascript:returnFalse(); return false;" novalidate="novalidate">
				<div class="row">
					<div class="col-md-12" id="errorNotifyMe"></div>
					<div class="notify-me-wrapper">
						<input class="inline" id="mailId" name="notifyMeMailId"
							type="text" placeholder="<fmt:message key="tru.productdetail.label.ghosttext" />" maxlength="${emailLength}"> 
							<%-- <input type="hidden" id="itemDescription" value="${itemDescription}">--%>
							
						<div class="NotifyMeDescription hide">${itemDescription}</div>
						<input type="hidden" id="itemName" value="${itemName}">
						<input type="hidden" id="productId" value="${productId}">
						<input type="hidden" id="itemNumber" value="${itemNumber}">
						<input type="hidden" id="itemURL" value="${itemURL}"> <input type="hidden" id="helpURLName" value="${helpURLName}">
						<input type="hidden" id="helpURLValue" value="${helpURLValue}">
						<input type="hidden" id="siteID" value="${siteID}">
						<input type="hidden" id="onlinePID" value="${onlinePID}">
						<button id="notifyMeMail" class="inline notify-me-submit-button" type="submit">submit</button>
					</div>
				</div>
				<div class="row">
					<div class="notify-me-footer">
						<div class="over-17 inline">
							<div class="modal-checkbox-container">
								 <input class="modal-checkbox notify-checkbox"
									id="pdp-notify-checkbox" name="checkbox" type="checkbox">
									 <!-- <div class="pdp-notify-checkbox"></div>  -->
								 	<label for="pdp-notify-checkbox"></label>
							</div>
							<div class="inline notify-me-text">
							<fmt:message key="tru.productdetail.label.notifymecheckboxmessage" />
								<a href="javascript:void(0)" class="" data-toggle="modal" data-target="#privacyPolicyModal">
									<fmt:message key="tru.productdetail.label.privacypolicy" />
								</a>
							</div>
						</div>
					</div>
				</div>
						 
				 </form>
			</div>
			<div class="notify-me-header notifymeSuccess hide">
						<div class="notify-me-title">Thank You...</div>
						<div>
							<div class="inline notify-me-text">We will notify
								you when this item is back in stock.</div>
							<div class="notifyme-image-title">
								<%-- <c:choose>
									<c:when test="${not empty primaryImage}">
										<img alt="${primaryImage}" src="${primaryImage}"/>
									</c:when>
									<c:otherwise>
									<!--	<img alt="${TRUImagePath}images/no-image500.gif" src="${TRUImagePath}images/no-image500.gif" /> -->
										<img alt="${akamaiNoImageURL}" src="${akamaiNoImageURL}" />
									</c:otherwise>
								</c:choose> --%>
								<%-- <p class="notify-me-text">${itemName}</p> --%>
							</div>
						</div>
					</div> 
		</div>
	</div>
	
	
</dsp:page>