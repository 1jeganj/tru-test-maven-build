<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="pdpRegistryUrl" bean="TRUStoreConfiguration.pdpRegistryUrl" />
<dsp:getvalueof param="skuDisplayName" var="skuDisplayName" />
<div class="pdp-modal1 ">
					<div class="close-btn">
						<img src="${TRUImagePath}images/close.png" data-dismiss="modal" alt="close modal">
					</div>
					<header>
						Items Added						
						<p>you have successfully added the ${skuDisplayName} to your baby registry</p>
					</header>
					<!--action-btns start-->
					<div class="action-btns">
						<div class="row">
							<div class="col-md-4">
								<button onclick="javascript:location.href='${pdpRegistryUrl}'">Back to my registry</button>
							</div>
							<div class="col-md-4">
							         <button> <fmt:message key="tru.productdetail.label.backtomychecklist" /> </button>
								<!-- <button>Back to my checklist</button> -->
							</div>
							<div class="col-md-4">
								<button data-dismiss="modal"><fmt:message key="tru.productdetail.label.wishlistcontinueshopping" /></button>
							</div>
						</div>
				</div>
</div>
</dsp:page>