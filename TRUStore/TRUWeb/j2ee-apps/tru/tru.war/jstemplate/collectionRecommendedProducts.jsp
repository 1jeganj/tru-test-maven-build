<dsp:page>
<dsp:getvalueof var="productInfo" param="collectionProductInfo"/>
<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
<dsp:importbean bean="/com/tru/common/TRUIntegrationConfiguration" />
<dsp:importbean bean="/com/tru/common/TRUSOSIntegrationConfiguration" />
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
<dsp:getvalueof var="event" value="product"/>
<dsp:getvalueof var="itemid" param="collectionProductInfo.collectionId"/>
<dsp:getvalueof var="customerId" bean="Profile.id"/>
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration"/>
<dsp:getvalueof var="toysSiteCode" bean="TRUTealiumConfiguration.toysSiteCode"/>  
<dsp:getvalueof var="storeCertona" bean="TRUIntegrationConfiguration.enableCertona"/>
<dsp:getvalueof var="sosCertona" bean="TRUSOSIntegrationConfiguration.enableCertona"/>
<dsp:getvalueof var="siteCode" value="TRU" />
<dsp:droplet name="GetSiteTypeDroplet">
	<dsp:param name="siteId" value="${site}"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="currentSite" param="site"/>
		<c:set var="currentSite" value ="${currentSite}" scope="request"/>
	</dsp:oparam>
</dsp:droplet>
<c:choose>
	<c:when test="${currentSite eq 'sos'}">
		<c:set var="certona" value="${sosCertona}" scope="request"/>
	</c:when>
	<c:otherwise>
		<c:set var="certona" value="${storeCertona}" scope="request"/>
	</c:otherwise>
</c:choose>
  <!--  <div class="my-account-product-carousel1">

            <div id="slider-product-block-container1" class="slider-product-block-container1" > -->
            
 			    <!-- Certona related Divs Start -->
 			    <c:if test="${certona eq 'true'}"> 
			  		<c:choose>  		
					<c:when test="${site eq toysSiteCode}">
						<div class="full_width_gray">
							<div id="tcollection_rr">  
								<!-- Toysrus product page recommendations appear here -->
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<div class="full_width_gray">
							<div id="bcollection_rr"> 
							 	<!-- Babiesrus product page recommendations appear here -->
							 </div>
						</div>
						<dsp:getvalueof var="siteCode" value="BRU" />	
					</c:otherwise>		
					</c:choose>	
					<div id="rdata" style="visibility:hidden;">
					 <div id="site">${siteCode}</div>
					 <div id="event">${event}</div> 
					 <div id="itemid">${itemid}</div>
					 <div id="customerid">${customerId}</div>
					</div>
				</c:if>
			    <!-- Certona related Divs End -->
			    				   
				
			<!-- </div>
        </div>  -->
    </dsp:page>