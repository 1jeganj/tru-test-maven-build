<dsp:page>
<!-- Start Collections template -->
<%-- <dsp:include page="/browse/scripts.jsp"></dsp:include>--%>
<!-- <a id="scrollToTop" href="javascript:void(0);">To Top</a> -->
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
<dsp:getvalueof var="collectionAkamaiImageResizeMap" bean="TRUStoreConfiguration.collectionAkamaiImageResizeMap" />
<dsp:getvalueof bean="TRUConfiguration.recaptchaPublicKey" var="recaptchaPublicKey" />
<c:forEach var="entry" items="${collectionAkamaiImageResizeMap}">
		<c:if test="${entry.key eq 'collectionMainImageBlock'}">
			<c:set var="collectionMainImageBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'collectionProductListImageBlock'}">
			<c:set var="collectionProductListImageBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'collectionZoomImageBlock'}">
			<c:set var="collectionZoomImageBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'swatchImageBlock'}">
			<c:set var="swatchImageBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'recentlyViewedImageBlock'}">
			<c:set var="recentlyViewedImageBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'childProductZoomImageBlock'}">
			<c:set var="childProductZoomImageBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'childProductPriAltImageBlock'}">
			<c:set var="childProductPriAltImageBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'childProductSecAltImageBlock'}">
			<c:set var="childProductSecAltImageBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'childProductAltImageBlock'}">
			<c:set var="childProductAltImageBlock" value="${entry.value}" />
		</c:if>
	</c:forEach>
	<input type="hidden" id="primaryMainImageBlock" value="${collectionProductListImageBlock}">
	<input type="hidden" id="zoomPrimaryMainImageBlock" value="${collectionZoomImageBlock}">
	<%-- <input type="hidden" id ="alternateImageThumbnail1Block" value = "${alternateImageThumbnail1Block}"> --%>
	<div class="container-fluid collections-template">
		<div class="modal fade in pdpModals" id="esrbratingModel" tabindex="-1"
			role="dialog" aria-labelledby="basicModal" aria-hidden="false">
			<dsp:include page="/jstemplate/esrbMature.jsp">
			</dsp:include>
		</div>
		<div class="modal fade cp-shoppingCartModal" id="shoppingCartModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false"></div>
		<div class="modal fade" id="quickviewModal" tabindex="-1"
			role="dialog" aria-labelledby="basicModal" aria-hidden="false">
			<dsp:include page="/jstemplate/quickView.jsp"></dsp:include>
		</div>
		<div class="modal fade" id="notifyMeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                 <dsp:include page="/jstemplate/notifyMePopUp.jsp"></dsp:include>           
		</div> 
		<dsp:include page="/jstemplate/privacyPolicyTemplate.jsp">
        </dsp:include>
		
		<dsp:include page="/jstemplate/includePopup.jsp" />
		
		<!-- <div id="detailsPopup" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content sharp-border welcome-back-overlay">
					<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
					<h2>details</h2>
					<p class="abandonCartReminder">
						This message shows how many days we take to ship the item from our warehouse
					</p>
				</div>
			</div>
		</div> -->
		
		<div id="learnMorePopup" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content sharp-border welcome-back-overlay">
					<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
					<h2>learn more</h2>
					<p class="abandonCartReminder">
						these items is available for customers to go buy at stores only.
					</p>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="findInStoreModal" tabindex="-1"
			role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<%-- <dsp:include page="/jstemplate/findInStoreInfo.jsp" /> --%>
		</div>

		<div class="row row-no-padding">
			<div class="col-md-12 col-no-padding"></div>
		</div>
		<div class="template-content collection-template">
<%-- 			<dsp:include page="/jstemplate/adZoneCollection.jsp" />
 --%>			<dsp:include page="/jstemplate/collectionHeader.jsp">
			<dsp:param name="collectionProductInfo" param="collectionProductInfo"/>
			</dsp:include>
			<dsp:include page="/product/tealiumForCollections.jsp">
			<dsp:param name="collectionProductInfo" param="collectionProductInfo"/>
			</dsp:include>
			<dsp:include page="/jstemplate/collectionHeroContent.jsp">
			<dsp:param name="collectionProductInfo" param="collectionProductInfo"/>
			<dsp:param name="collectionMainImageBlock" value="${collectionMainImageBlock}"/>
			
			</dsp:include>
			<dsp:include page="/jstemplate/collectionProductDetailsSection.jsp">
			<dsp:param name="collectionProductInfo" param="collectionProductInfo"/>
			<dsp:param name="childProductZoomImageBlock" value="${childProductZoomImageBlock}"/>
			<dsp:param name="childProductPriAltImageBlock" value="${childProductPriAltImageBlock}"/>
			<dsp:param name="childProductSecAltImageBlock" value="${childProductSecAltImageBlock}"/>
			
			<dsp:param name="childProductAltImageBlock" value="${childProductAltImageBlock}"/>
			<dsp:param name="swatchImageBlock" value="${swatchImageBlock}"/>
			<dsp:param name="collectionProductListImageBlock" value="${collectionProductListImageBlock}"/>
			
			</dsp:include>
			<!-- <hr>  -->
			<%-- Start: Recently viewed section --%>
			 			
			<dsp:include page="/jstemplate/recentlyViewedSection.jsp" />
			<script>
			$(document).ready(function(){
				if(typeof populateRecentlyViewedItems != 'undefined'){
					populateRecentlyViewedItems('collection');	
				}
			});
			</script>
			<div class="my-account-product-carousel"></div>
			
			<%-- End: Recently viewed section --%>		
		</div>
					 	
			 <dsp:include page="/jstemplate/collectionRecommendedProducts.jsp">
			 <dsp:param name="collectionProductInfo" param="collectionProductInfo"/>
			 </dsp:include> 			
			<script type="text/javascript">
				var reloadCaptcha = function(){
					Recaptcha.create("${recaptchaPublicKey}","recaptcha");
				};
					/* $(window).load(function(){
						if(typeof document.write === "undefined"){
							document.write = tmpDocumentWrite;
						}
						$.getScript('${recaptchaScriptURL}${recaptchaPublicKey}');
					}); */
		
					$(window).load(function(){
						$.getScript('https://www.google.com/recaptcha/api/js/recaptcha_ajax.js');
					});
			</script>
		<dsp:include page="/jstemplate/collectionImageGallery.jsp">
		 <dsp:param name="collectionProductInfo" param="collectionProductInfo"/>
		  <dsp:param name="collectionZoomImageBlock" value="${collectionZoomImageBlock}"/>
		</dsp:include>
		<%-- <dsp:include page="/cart/shoppingcart.jsp">
		</dsp:include> --%>
		<dsp:include page="/jstemplate/collectionsTooltip.jsp">
		</dsp:include>
	</div>

	<!-- /End Collections template -->
</dsp:page>