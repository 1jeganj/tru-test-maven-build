<dsp:page>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/droplet/TRURegistryCookieDroplet"/>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="pdpRegistryUrl" bean="TRUStoreConfiguration.pdpRegistryUrl" />
<dsp:getvalueof var="pdpWishlistUrl" bean="TRUStoreConfiguration.pdpWishlistUrl" />
<dsp:getvalueof var="enableRegistryModalFlag" bean="TRUStoreConfiguration.enableRegistryModalFlag" />
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:getvalueof param="productInfo" var="productInfo" />
<dsp:getvalueof param="productInfo.defaultSKU.id" var="skuId" /> 
<dsp:getvalueof param="productInfo.productId" var="productId" /> 
<dsp:getvalueof param="productInfo.defaultSKU.customerPurchaseLimit" var="customerPurchaseLimit" />
<dsp:getvalueof param="productInfo.defaultSKU.customerPurchaseLimit" var="toShowcustomerPurchaseLimit" />
<dsp:getvalueof param="productInfo.defaultSKU" var="defaultSKU" /> 
<dsp:getvalueof param="productInfo.colorCodeList" var="colorCodeList" />
<dsp:getvalueof param="productInfo.juvenileSizesList" var="juvenileSizesList" />
<dsp:getvalueof param="productInfo.inStockSKUsList" var="activeSKUsList" />
<dsp:getvalueof param="productInfo.defaultSKU.unCartable" var="unCartable" />  
<dsp:getvalueof param="productInfo.defaultSKU.presellQuantityUnits" var="presellQuantityUnits" />  
<dsp:getvalueof param="productInfo.defaultSKU.presellable" var="presellable" /> 
<dsp:getvalueof param="productInfo.defaultSKU.streetDate" var="streetDate" />
<dsp:getvalueof param="productInfo.defaultSKU.notifyMe" var="notifyMe" />
 <dsp:getvalueof param="productInfo.defaultSKU.inventoryStatus" var="inventoryStatus" />
 <dsp:getvalueof param="productInfo.defaultSKU.availableInventory" var="availableInventory" />
 <dsp:getvalueof param="productInfo.defaultSKU.displayName" var="skuDisplayName" />
<dsp:getvalueof param="productInfo.displayName" var="productName" />
 <dsp:getvalueof param="productInfo.defaultSKU.longDescription" var="description"/>
 <dsp:getvalueof param="productInfo.defaultSKU.primaryImage" var="primaryImage" />
<dsp:getvalueof param="productInfo.defaultSKU.primaryCanonicalImage" var="primaryCanonicalImage" />
 <dsp:importbean bean="/atg/multisite/Site" />
<dsp:getvalueof var="siteID" bean="Site.id" />
<dsp:getvalueof param="productInfo.defaultSKU.registryWarningIndicator" var="registryWarningIndicator" />
<dsp:getvalueof param="productInfo.defaultSKU.skuRegistryEligibility" var="skuRegistryEligibility" />

<dsp:getvalueof var="pdpRegistryInvokeURL" bean="TRUStoreConfiguration.pdpRegistryInvokeURL" />
<dsp:getvalueof var="pdpWishlistInvokeURL" bean="TRUStoreConfiguration.pdpWishlistInvokeURL" />
<dsp:getvalueof var="wishlistRegistryType" bean="TRUStoreConfiguration.wishlistRegistryType" />
<dsp:getvalueof var="registryWishlistkeyMap" bean="TRUStoreConfiguration.registryWishlistkeyMap" />
<dsp:getvalueof var="viewRegistryUrl" bean="TRUStoreConfiguration.viewRegistryUrl" />
<dsp:getvalueof var="backToWishListUrl" bean="TRUStoreConfiguration.backToWishListUrl" />
<dsp:getvalueof var="backToAnonymousRegistryURL" bean="TRUStoreConfiguration.backToAnonymousRegistryURL" />
<dsp:getvalueof var="backToAnonymousWishlistURL" bean="TRUStoreConfiguration.backToAnonymousWishlistURL" />
<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
<dsp:getvalueof  param="productInfo.rusItemNumber" var="rusItemNumber"/>
<dsp:getvalueof  param="productInfo.defaultSKU.upcNumbers" var="upcNumbers"/> 
<dsp:getvalueof param="productInfo.defaultSKU.colorCode" var="color" />
<dsp:getvalueof var="size" param="productInfo.defaultSKU.juvenileSize"/>
<dsp:getvalueof param="productInfo.defaultSKU.id" var="uid" /> 
<dsp:getvalueof param="productInfo.productId" var="product" /> 
<dsp:getvalueof param="productInfo.defaultSKU.onlinePID" var="onlinePID" />
<dsp:getvalueof param="productInfo.defaultSKU.rmsColorCode" var="rmsColorCode" />
<dsp:getvalueof param="productInfo.defaultSKU.rmsSizeCode" var="rmsSizeCode" />
<dsp:getvalueof param="productInfo.defaultSKU.sknOrigin" var="sknOrigin" />
<dsp:getvalueof  param="productInfo.defaultSKU.originalproductIdOrSKN" var="skn"/>
<c:set var="singleQuote" value="'"/>
<c:set var="doubleQuote" value='"'/> 
<c:set var="slashSingleQuote" value="&apos;"/>
<c:set var="slashDoubleQuote" value='&quot;'/>
<c:set var="skuDisplayName" value="${fn:escapeXml(skuDisplayName)}"/>
<c:set var="contextRoot" value="${contextPath}"/>
<fmt:formatDate var="streetDateFormat" value="${streetDate}" pattern="MM/dd/yy"/>
<dsp:getvalueof param="pageFrom" var="pageFrom" />

<input type="hidden" id="cartAddSource" value="${pageFrom}"/>
<input type="hidden" class="collectionOnlinePID" value="${onlinePID}">
<c:choose>
	<c:when test="${siteID eq 'ToysRUs'}">
	<c:set var="tealiumSite" value="TRU"/>	
	</c:when>
	<c:when test="${siteID eq 'BabyRUs'}">
	<c:set var="tealiumSite" value="BRU"/>
	</c:when>
	<c:otherwise>
		
	</c:otherwise>
</c:choose>

<dsp:droplet name="/com/tru/utils/TRUPdpURLDroplet">
             <%-- <dsp:param name="productId" value="${productId}"/> --%>
              <dsp:param name="productId" value="${onlinePID}"/>
         <dsp:oparam name="output">
              <dsp:getvalueof var="productPageUrl" param="productPageUrl"/>
         </dsp:oparam>
       </dsp:droplet>
<dsp:getvalueof param="cookieValue" var="cookieValue" />
<dsp:getvalueof param="locationIdFromCookie" var="locationIdFromCookie" />
<dsp:getvalueof param="storeAvailMsg" var="storeAvailMsg" />
 
<dsp:getvalueof param="productInfo.defaultSKU.s2s" var="s2s" />
<dsp:getvalueof param="productInfo.defaultSKU.ispu" var="ispu" />
<dsp:getvalueof param="productInfo.defaultSKU.channelAvailability" var="channelAvailability" />


<dsp:getvalueof param="productInfo.defaultSKU.juvenileSize" var="defaultJuvenileSize" /> 
<dsp:getvalueof param="productInfo.defaultSKU.colorCode" var="defaultColorCode" />
<input type="hidden" value="${productPageUrl}" id="productPageUrlWithDomainHidden" />
<input type="hidden" value="${siteID}" id="siteIdHidden" />
<c:if test="${empty s2s}">
<dsp:getvalueof value="N" var="s2s" />
</c:if>
<c:if test="${empty ispu}">
<dsp:getvalueof value="N" var="ispu" />
</c:if>
<c:if test="${(channelAvailability eq 'N/A') || (empty channelAvailability)}">
<dsp:getvalueof value="both" var="channelAvailability" />
</c:if>
<c:if test="${not empty colorCodeList}">
 <dsp:getvalueof var="colorLength" value="${fn:length(colorCodeList)}"></dsp:getvalueof>
</c:if>
<c:if test="${not empty juvenileSizesList}">
 <dsp:getvalueof var="juvenileSizesListLength" value="${fn:length(juvenileSizesList)}"></dsp:getvalueof>
</c:if>
<dsp:getvalueof param="productInfo.colorSizeVariantsAvailableStatus" var="colorSizeVariantsAvailableStatus" />
<dsp:getvalueof var="selectedColor" param="selectedColorId" />
<dsp:getvalueof var="selectedSize" param="selectedSize" />
<c:if test="${not empty customerPurchaseLimit}">
<fmt:parseNumber var="customerPurchaseLimit1" type="number" value="${customerPurchaseLimit}" parseLocale="en_US" />
</c:if>
<c:if test="${not empty presellQuantityUnits}">
<fmt:parseNumber var="presellQuantityUnits1" type="number" value="${presellQuantityUnits}" parseLocale="en_US" />
</c:if>
		<c:choose>
			<c:when test="${not empty juvenileSizesList && (colorSizeVariantsAvailableStatus eq 'bothColorSizeAvailable' || colorSizeVariantsAvailableStatus eq 'onlySizeVariants')}">
			</c:when>
			<c:otherwise>
			<div class="sticky-quantity">
				<dsp:include page="/jstemplate/quantitySection.jsp" >
					<dsp:param name="isSizeAvailable" value="false"/>
				</dsp:include>
			</div>
			</c:otherwise>		
		</c:choose>
                   
                     <div class="out-of-stock out-of-stock-message-container">   
		                 <c:choose>
							<c:when test="${!presellable && ((inventoryStatus eq 'outOfStock' && s2s eq 'N' && ispu eq 'N' && empty locationIdFromCookie && (colorSizeVariantsAvailableStatus eq 'noColorSizeAvailable') ) || (inventoryStatus eq 'outOfStock' && not empty locationIdFromCookie && s2s eq 'N' && ispu eq 'N' && (colorSizeVariantsAvailableStatus eq 'noColorSizeAvailable') ) || (inventoryStatus eq 'outOfStock' && not empty locationIdFromCookie && empty storeAvailMsg && (colorSizeVariantsAvailableStatus eq 'noColorSizeAvailable') ))}">
								<div>
									<span>out of stock </span>
		                			<div class="small-blue inline">
		            					<a class="learn-more" data-toggle="modal" data-target="#learnMorePopupStore"><fmt:message key="tru.productdetail.label.learnmore"/></a>
									</div>
								</div>  
							</c:when>
		                 </c:choose> 
                       </div>
                         <div id="addToCartDivSpace">
                         <input id="onlineInventoryStatus-${skuId}" type="hidden" value="${inventoryStatus}" />
                        <c:choose>
                        <c:when test="${(channelAvailability eq 'In Store Only') && (colorSizeVariantsAvailableStatus eq 'noColorSizeAvailable')}">
	                        <c:choose>
								<c:when test="${((empty locationIdFromCookie) && !(s2s eq 'N' && ispu eq 'N'))}">
								<div class="add-to-cart-section">
									<button  class="add-to-cart" data-toggle="modal" data-target="#findInStoreModal" onclick="javascript:return loadFindInStore('${contextPath}','${skuId}','',$('.QTY-${productId}').val());"><fmt:message key="tru.productdetail.label.addtocart"/></button>
								</div>
								
								</c:when>       
								<c:when test="${((empty locationIdFromCookie) && s2s eq 'N' && ispu eq 'N') || ((not empty locationIdFromCookie) && s2s eq 'N' && ispu eq 'N')}">
									<div class="add-to-cart-section">
										<button disabled="disabled" class="add-to-cart"><fmt:message key="tru.productdetail.label.addtocart"/></button>
									</div>
								
								</c:when>
								<c:when test="${((not empty locationIdFromCookie) && (empty storeAvailMsg) && !(s2s eq 'N' && ispu eq 'N')) }">
									<div class="add-to-cart-section">
									 	<button  class="add-to-cart" data-toggle="modal" data-target="#shoppingCartModal" onclick="javascript: return addItemToCart('${productId}','${skuId }','${locationIdFromCookie}','${contextPath}',this,'productPage');"><fmt:message key="tru.productdetail.label.addtocart"/></button>
									 </div>
								</c:when>
							</c:choose>
                      	</c:when>
                       <c:when test="${notifyMe eq 'Y' && ((inventoryStatus eq 'outOfStock' && s2s eq 'N' && ispu eq 'N' && empty locationIdFromCookie && channelAvailability ne 'In Store Only') 
                       					|| (inventoryStatus eq 'outOfStock' && not empty locationIdFromCookie && s2s eq 'N' && ispu eq 'N')
                       					|| (inventoryStatus eq 'outOfStock' && not empty locationIdFromCookie && empty storeAvailMsg))}">
                       <div class="email-section">
 					    <div class="NotifyMeDescription hide"></div>
                  		<input type="hidden" class="NotifyMeDskuDisplayName" value="${skuDisplayName}">
                        <input type="hidden" class="productId" value="${productId}">
                        <input type="hidden" class="skn" value="${skn}">
                        <input type="hidden" class="NotifyMeskuId" value="${skuId}">
                        <input type="hidden" class="NotifyMeProductPageUrl" value="${productPageUrl}">
                        <input type="hidden" class="NotifyMeHelpURLName" value="${skuDisplayName}">
                        <input type="hidden" class="NotifyMeHelpURLValue" value="${productPageUrl}">
                        <c:choose>
							<c:when test="${not empty primaryImage}">
								<input type="hidden" class="primaryImage" value="${primaryImage}">
							</c:when>
							<c:otherwise>
									<c:choose>
										<c:when test="${not empty akamaiNoImageURL}">
										  	<input type="hidden" class="primaryImage" value="${akamaiNoImageURL}"> 
										</c:when>
										 <c:otherwise>
											<input type="hidden" class="primaryImage" value="${TRUImagePath}images/no-image500.gif">
										</c:otherwise>
								 	</c:choose>	
							</c:otherwise>
						</c:choose>
                        <input type="hidden" class="siteID" value="${siteID}">								
						<button onclick='loadNotifyMe(this);javascript:utag.link({"engagement_impression":"${tealiumSite}: Product Detail Page: Notify Me When Available"});' class="email-me-available" data-toggle="modal" data-target="#notifyMeModal"><fmt:message key="tru.productdetail.label.emailmeavailable"/></button>							
						
						</div>
                        </c:when>
                        <c:when test="${(inventoryStatus eq 'outOfStock') && (colorSizeVariantsAvailableStatus eq 'noColorSizeAvailable') 
                        && (empty locationIdFromCookie) && !(s2s eq 'N' && ispu eq 'N')}">
                        <div class="add-to-cart-section">
								<button  class="add-to-cart" data-toggle="modal" data-target="#findInStoreModal" onclick="javascript:return loadFindInStore('${contextPath}','${skuId}','',$('.QTY-${productId}').val());"><fmt:message key="tru.productdetail.label.addtocart"/></button>
							</div>
						</c:when>
							<c:when test="${(inventoryStatus eq 'outOfStock') && (colorSizeVariantsAvailableStatus eq 'noColorSizeAvailable')
							 && (not empty locationIdFromCookie && not empty storeAvailMsg) && !(s2s eq 'N' && ispu eq 'N')}">
								 <div class="add-to-cart-section">
								 	<button  class="add-to-cart" data-toggle="modal" data-target="#shoppingCartModal" onclick="javascript: return addItemToCart('${productId}','${skuId }','${locationIdFromCookie}','${contextPath}',this,'productPage');"><fmt:message key="tru.productdetail.label.addtocart"/></button>
								 </div>
							</c:when>
                        <c:otherwise>
                        <div class="add-to-cart-section">
						<c:choose>
							<c:when 
								test="${(!unCartable) && (inventoryStatus ne 'outOfStock')
													&& ((colorSizeVariantsAvailableStatus eq 'bothColorSizeAvailable' && (not empty selectedColor && defaultColorCode eq selectedColor) 
													&& (not empty selectedSize && defaultJuvenileSize eq selectedSize) )
															|| (colorSizeVariantsAvailableStatus eq 'noColorSizeAvailable' && not empty defaultSKU)
															|| (colorSizeVariantsAvailableStatus eq 'onlyColorVariants' && ((not empty selectedColor &&  empty selectedSize) 
															|| (colorLength eq 1 && not empty activeSKUsList)) )
															|| (colorSizeVariantsAvailableStatus eq 'onlySizeVariants' && ((not empty selectedSize && empty selectedColor)
															 || (juvenileSizesListLength eq 1 && not empty activeSKUsList)) ))}">
								<c:choose>
								<c:when test="${(presellable && not empty presellQuantityUnits && presellQuantityUnits1 eq 0)||(!presellable && availableInventory eq 0)}">
									<button disabled="disabled" class="add-to-cart"><fmt:message key="tru.productdetail.label.addtocart"/> </button>
								</c:when>
								
																
								<c:when test="${presellable}">
									<!-- add to ResourceBundle -->
									<button  class="add-to-cart" data-toggle="modal" data-target="#shoppingCartModal" onclick="javascript: return addItemToCart('${productId}','${skuId }','','${contextPath}',this);"><fmt:message key="tru.productdetail.label.preorder"/>  (available on ${streetDateFormat})</button>
								</c:when>
								
								<c:otherwise>
									<button  class="add-to-cart" data-toggle="modal" data-target="#shoppingCartModal" onclick="javascript: return addItemToCart('${productId}','${skuId }','','${contextPath}',this);"><fmt:message key="tru.productdetail.label.addtocart"></fmt:message></button>
								</c:otherwise>
								</c:choose>
							</c:when>
							<c:otherwise>
								<c:choose>
								<c:when test="${(presellable)}">
									<button disabled="disabled" class="add-to-cart fade-add-to-cart"><fmt:message key="tru.productdetail.label.preorder"/>  (available on ${streetDateFormat})</button>
								</c:when>
								<c:otherwise>
						     		<button disabled="disabled" class="add-to-cart"><fmt:message key="tru.productdetail.label.addtocart"/></button>
						     </c:otherwise>
						     </c:choose>
						</c:otherwise>
						</c:choose>
						 </div>
						 </c:otherwise>
                        </c:choose>
                        </div>
                        
                        <input type="hidden" class="pdpRegistryUrl" value="${pdpRegistryUrl}" />
                         <input type="hidden" class="pdpWishlistUrl" value="${pdpWishlistUrl}" />
                          <input type="hidden" class="registryWarningIndicator" value="${registryWarningIndicator}" />
					      <input type="hidden" class="skuRegistryEligibility" value="${skuRegistryEligibility}" />
						<dsp:droplet name="TRURegistryCookieDroplet">
							<dsp:param name="retrieveRegistryCookie" value="true" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="registry_id" param="registry_id" />
								<dsp:getvalueof var="lastAccessedRegistryID" param="lastAccessedRegistryID" />
								<dsp:getvalueof var="lastAccessedWishlistID" param="lastAccessedWishlistID" />
								<dsp:getvalueof var="wishlist_id" param="wishlist_id" />
							</dsp:oparam>
						</dsp:droplet>
						
						
						<input type="hidden" id="wishlist_id" value="${wishlist_id}" />
					      <input type="hidden" class="registry_id" value="${registry_id}" />
					      
							<input type="hidden" id="colorSizeVariantsAvailableStatus" value="${colorSizeVariantsAvailableStatus}" />
	<c:forEach var="entry" items="${registryWishlistkeyMap}">
		<c:if test="${entry.key eq 'registryType'}">
			<c:set var="registryType" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'defaultColor' }">
			<c:set var="color" value="${entry.value}" />
			<input type="hidden" class="rgs_defaultColor" value="${color}">
		</c:if>
		<c:if test="${entry.key eq 'defaultSize' }">
			<c:set var="size" value="${entry.value}" />
			<input type="hidden" class="rgs_defaultSize" value="${size}">
		</c:if>
		<%-- <c:if test="${entry.key eq 'defaultSknOrigin'}">
			<c:set var="sknOrigin" value="${entry.value}" />
		</c:if> --%>
		<c:if test="${entry.key eq 'nonSpecificIndicator'}">
			<c:set var="nonSpecificIndicator" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'itemPurchased'}">
			<c:set var="itemPurchased" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'purchaseUpdateIndicator'}">
			<c:set var="purchaseUpdateIndicator" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'source'}">
			<c:set var="source" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'messageId'}">
			<c:set var="messageId" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'country'}">
			<c:set var="country" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'language'}">
			<c:set var="language" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'deletedFlag'}">
			<c:set var="deletedFlag" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'wishlistRegistryType'}">
			<c:set var="wishlistRegistryType" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'timeout'}">
			<c:set var="timeout" value="${entry.value}" />
		</c:if>
	</c:forEach>
	<c:if test="${not empty upcNumbers}">
		 <c:forEach items="${upcNumbers}" var="upcNumber" end="0">
		   <c:set var="upc" value="${upcNumber}" />
		</c:forEach>
	</c:if>
	<c:if test="${not empty rmsColorCode}">
		<c:set var="color" value="${rmsColorCode}"></c:set>
	</c:if>
	<c:if test="${not empty rmsSizeCode}">
		<c:set var="size" value="${rmsSizeCode}"></c:set>
	</c:if>
	
	<input type="hidden" id="rgs_skuDisplayName" value="${skuDisplayName}">
	<input type="hidden" id="rgs_registryInvokeURL" value="${pdpRegistryInvokeURL}">
	<input type="hidden" id="rgs_wishlistInvokeURL" value="${pdpWishlistInvokeURL}">
	<input type="hidden" id="rgs_registryNumber" value="${registry_id}">
	<input type="hidden" id="rgs_wishlistId" value="${wishlist_id}">
	<input type="hidden" id="rgs_lastAccessedRegistryID" value="${lastAccessedRegistryID}">
	<input type="hidden" id="rgs_lastAccessedWishlistID" value="${lastAccessedWishlistID}">
	<input type="hidden" id="rgs_registryType" value="${registryType}">
	<input type="hidden" id="rgs_product" value="${product}">
	<input type="hidden" id="rgs_skn" value="${skn}">
	<input type="hidden" id="rgs_upc" value="${upc}">
	<input type="hidden" id="rgs_uid" value="${uid}">
	<input type="hidden" id="rgs_color" value="${color}">
	<input type="hidden" id="rgs_size" value="${size}">
	<input type="hidden" id="rgs_sknOrigin" value="${sknOrigin}">
	<input type="hidden" id="rgs_nonSpecificIndicator" value="${nonSpecificIndicator}">
	<input type="hidden" id="rgs_purchaseUpdateIndicator" value="${purchaseUpdateIndicator}">
	<input type="hidden" id="rgs_country" value="${country}">
	<input type="hidden" id="rgs_language" value="${language}">
	<input type="hidden" id="rgs_deletedFlag" value="${deletedFlag}">
	<input type="hidden" id="rgs_wishlistRegistryType" value="${wishlistRegistryType}">
	<input type="hidden" class="pdpRegistryUrl" value="${pdpRegistryUrl}">
	<input type="hidden" class="pdpBackToRegistryURL" value="${viewRegistryUrl}">
	<input type="hidden" class="pdpBackToWishlistURL" value="${backToWishListUrl}">
	<input type="hidden" class="pdpBackToAnonymousRegistryURL" value="${backToAnonymousRegistryURL}">
	<input type="hidden" class="pdpBackToAnonymousWishlistURL" value="${backToAnonymousWishlistURL}">
	<input type="hidden" class="registry_id" value="${registry_id}">
	<input type="hidden" class="pdpWishlistUrl" value="${pdpWishlistUrl}">
	<input type="hidden" id="rgs_timeout" value="${timeout}">
	<input type="hidden" id="rgs_productDetails" value="${product}|${skn}|${upc}|${uid}|${color}|${size}|${sknOrigin}">
<input type="hidden" id="rgs_wishlistInvokeFinalURL" value="${pdpWishlistInvokeURL}/${registry_id}/${deletedFlag}?country=${country}&language=${language}&wishlistRegistryType=${wishlistRegistryType}">
					      
					      <div class="registryButtonContainer">
					   
							<%-- <div class="add-to-text inline">
								<fmt:message key="tru.productdetail.label.addTo" />
							</div> --%>
							<!-- .baby-registry-addTo -->
						<c:choose>
                        <c:when test="${colorSizeVariantsAvailableStatus eq 'bothColorSizeAvailable'  &&  empty selectedColor &&  empty selectedSize}">						 
							<!-- <div class="inline"> -->
								<button class="cart-button-disable baby-registry-addTo registryLoggedIn"><fmt:message key="tru.productdetail.label.pdpaddtobabyregistry" />&nbsp;</button>
							<!-- </div> -->
							
							<!-- <div class="grey-arrow-icon inline"></div> -->
							<!-- <div class="inline"> -->
								<button class="cart-button-disable wish-list-addTo wishlistLoggedIn"><fmt:message key="tru.productdetail.label.pdpaddtowishlist" />&nbsp;</button>
							<!-- </div> -->
							<!-- <div class="grey-arrow-icon inline"></div> -->
						
                        </c:when>
                        <c:when test="${colorSizeVariantsAvailableStatus eq 'onlyColorVariants' && ((empty selectedColor)) || colorSizeVariantsAvailableStatus eq 'onlySizeVariants' && ((empty selectedSize))}">
                        
							<!-- <div class="inline"> -->
								<button class="cart-button-disable baby-registry-addTo registryLoggedIn"><fmt:message key="tru.productdetail.label.pdpaddtobabyregistry" />&nbsp;</button>
							<!-- </div> -->
							<!-- <div class="grey-arrow-icon inline"></div> -->
							<!-- <div class="inline"> -->
								<button class="cart-button-disable wish-list-addTo wishlistLoggedIn"><fmt:message key="tru.productdetail.label.pdpaddtowishlist" />&nbsp;</button>
							<!-- </div> -->
							<!-- <div class="grey-arrow-icon inline"></div> -->
						
                        </c:when>
                        <c:when test="${(colorSizeVariantsAvailableStatus eq 'bothColorSizeAvailable' && (not empty selectedColor && defaultColorCode eq selectedColor) && (not empty selectedSize && defaultJuvenileSize eq selectedSize) )}">
                        <dsp:include page="/jstemplate/addToRegistry.jsp">
		 				<dsp:param name="productInfo" param="productInfo"/>
		 				<dsp:param name="registry_id" value="${registry_id}" />
						<dsp:param name="lastAccessedRegistryID" value="${lastAccessedRegistryID}" />
						<dsp:param name="lastAccessedWishlistID" value="${lastAccessedWishlistID}" />
						<dsp:param name="wishlist_id" value="${wishlist_id}" />
						</dsp:include>
			                        </c:when>
					<c:when test="${((colorSizeVariantsAvailableStatus eq 'bothColorSizeAvailable' && (not empty selectedColor && defaultColorCode eq selectedColor)) ||
						 (colorSizeVariantsAvailableStatus eq 'bothColorSizeAvailable' && not empty selectedSize && defaultJuvenileSize eq selectedSize) )}">
			                            
			                            
						<!-- 	<div class="inline"> -->
								<button class="small-orange cart-button-disable"><fmt:message key="tru.productdetail.label.babyregistry" />&nbsp;</button>
							<!-- </div> <div class="grey-arrow-icon inline"></div> -->
							<!-- <div class="inline"> -->
								<button class="small-orange cart-button-disable"><fmt:message key="tru.productdetail.label.wishlist" />&nbsp;</button>
							<!-- </div> -->
							<!-- <div class="grey-arrow-icon inline"></div> -->
						
					</c:when>
					<c:when test="${(colorSizeVariantsAvailableStatus eq 'onlyColorVariants' && ((not empty selectedColor &&  empty selectedSize) || (colorLength eq 1 && not empty activeSKUsList)) )
								|| (colorSizeVariantsAvailableStatus eq 'onlySizeVariants' && ((not empty selectedSize && empty selectedColor) || (juvenileSizesListLength eq 1 && not empty activeSKUsList)))}">
                        <dsp:include page="/jstemplate/addToRegistry.jsp">
		 				<dsp:param name="productInfo" param="productInfo"/>
		 				<dsp:param name="registry_id" value="${registry_id}" />
						<dsp:param name="lastAccessedRegistryID" value="${lastAccessedRegistryID}" />
						<dsp:param name="lastAccessedWishlistID" value="${lastAccessedWishlistID}" />
						<dsp:param name="wishlist_id" value="${wishlist_id}" />
						</dsp:include>
                        </c:when>
                        <c:otherwise>
                        <dsp:include page="/jstemplate/addToRegistry.jsp">
                        <dsp:param name="productInfo" param="productInfo"/>
		 				<dsp:param name="registry_id" value="${registry_id}" />
						<dsp:param name="lastAccessedRegistryID" value="${lastAccessedRegistryID}" />
						<dsp:param name="lastAccessedWishlistID" value="${lastAccessedWishlistID}" />
						<dsp:param name="wishlist_id" value="${wishlist_id}" />
						</dsp:include>
                        </c:otherwise>
                        </c:choose>
                        </div>
                        <c:choose>                        
                        <c:when test="${(pageFrom eq 'fromCollectionPage')}">
                         <dsp:include page="/jstemplate/registryModalDialog.jsp">
	                        <dsp:param name="name" value="${skuDisplayName}" />
	                        <dsp:param name="primaryImage" value="${primaryImage}"/>
	                        </dsp:include>
                        </c:when>
                        <c:when test="${(pageFrom eq 'Product Detail Page') && (!enableRegistryModalFlag)}">
                         <dsp:include page="/jstemplate/registryModalDialog.jsp">
	                        <dsp:param name="name" value="${skuDisplayName}" />
	                        <dsp:param name="primaryImage" value="${primaryCanonicalImage}"/>
	                        </dsp:include>
                        </c:when>
                        </c:choose>
                  
</dsp:page>