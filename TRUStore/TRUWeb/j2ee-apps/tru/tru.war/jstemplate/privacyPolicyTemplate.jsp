<dsp:page>
	<div class="modal fade toStopScroll" id="privacyPolicyModal"
		tabindex="-1" role="dialog" aria-labelledby="basicModal"
		aria-hidden="false">
		<div class="modal-dialog quick-help-modal">
			<div class="modal-content sharp-border">
				<div class="modal-title">
					Privacy Policy <a href="javascript:void(0)" data-dismiss="modal"><img alt="Close model" src="${TRUImagePath}images/close.png"
						class=""></a>
				</div>
				<div class="tse-scrollable">
					<div class="tse-scroll-content tse-quickhelp-scroll-content">
						<div class="quick-help-modal-content tse-content">
							<div class="modal-static-content">
								<dsp:include page="/jstemplate/privacyPolicyContent.jsp" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</dsp:page>