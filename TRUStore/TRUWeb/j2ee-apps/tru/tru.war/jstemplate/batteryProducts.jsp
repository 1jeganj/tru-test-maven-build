<dsp:page>
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
	<dsp:importbean bean="/atg/multisite/Site" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<dsp:getvalueof param="productInfo.defaultSKU.batteryProducts" var="batteryProducts" />
	<dsp:getvalueof bean="TRUConfiguration.priceOnCartValue" var="priceOnCartValue" />
	<dsp:getvalueof var="siteID" bean="Site.id" />
	<dsp:getvalueof param="locationIdFromCookie" var="locationIdFromCookie" />
	<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />	
	<dsp:getvalueof var="batteryQuantity" bean="TRUStoreConfiguration.batteryQuantity" />	
 <c:if test="${not empty batteryProducts}">
	<div class="main-sec">
	<div class="batteryPanel">
	<h3 class="batteries_pIR"><fmt:message key="tru.productdetail.label.batteriestext"/></h3>
	 <dsp:droplet name="/atg/dynamo/droplet/Range">
			<dsp:param name="array" param="productInfo.defaultSKU.batteryProducts" />
			<dsp:param name="howMany" value="3"/>
			<dsp:param name="elementName" value="batteryProductInfo" />
			<dsp:oparam name="outputStart">
			</dsp:oparam>
			<dsp:oparam name="output">
			<dsp:getvalueof param="batteryProductInfo.displayName" var="displayName"></dsp:getvalueof>
			<c:set var="displayName" value="${fn:escapeXml(displayName)}"/>
			<dsp:getvalueof param="batteryProductInfo.pdpURL" var="pdpURL"/>
			<dsp:getvalueof param="batteryProductInfo.skuId" var="skuId"/>
			<dsp:getvalueof param="batteryProductInfo.productId" var="productId"/>
			<dsp:getvalueof param="batteryProductInfo.ispu" var="ispu"/>
			<dsp:getvalueof param="batteryProductInfo.s2s" var="s2s"/>
			<dsp:getvalueof param="batteryProductInfo.unCartable" var="unCartable" />
			<dsp:getvalueof param="batteryProductInfo.listPrice" var="listPrice" />
			<dsp:getvalueof param="batteryProductInfo.salePrice" var="salePrice" />
			<dsp:getvalueof param="batteryProductInfo.notifyMe" var="notifyMe" />
			<dsp:getvalueof param="batteryProductInfo.priceDisplay" var="priceDisplay" />
			<dsp:getvalueof param="batteryProductInfo.availableInventory" var="availableInventory" />
			<dsp:getvalueof param="batteryProductInfo.inventoryStatusInStore" var="inventoryStatusInStore" />
			<dsp:getvalueof param="batteryProductInfo.availableInventoryInStore" var="availableInventoryInStore" />
			<dsp:getvalueof param="batteryProductInfo.inventoryStatus" var="inventoryStatus" />
			<dsp:getvalueof param="batteryProductInfo.primaryImage" var="primaryImage" />
			<dsp:getvalueof param="batteryProductInfo.presellable" var="presellable" />		
			<dsp:getvalueof param="batteryProductInfo.originalproductIdOrSKN" var="originalproductIdOrSKN" />
			<dsp:getvalueof param="batteryProductInfo.longDescription" var="longDescription" />	
				<div class="batterySku-${skuId} content-block">
 				<div class="NotifyMeDescription hide"></div>
 				 <input type="hidden" class="NotifyMeDskuDisplayName" value="${displayName}">
                 <input type="hidden" class="productId" value="${productId}">
                 <input type="hidden" class="skn" value="${originalproductIdOrSKN}">
                 <input type="hidden" class="NotifyMeskuId" value="${skuId}">
                 <input type="hidden" class="NotifyMeProductPageUrl" value="${pdpURL}">
                 <input type="hidden" class="NotifyMeHelpURLName" value="${displayName}">
                 <input type="hidden" class="NotifyMeHelpURLValue" value="${pdpURL}">
                 <input type="hidden" class="NotifyMePrimaryImage" value="${primaryImage}">
                 <input type="hidden" class="siteID" value="${siteID}">
				<div class="subtext">
				<a href="${pdpURL}">${displayName}</a>
				</div>
					<dsp:include page="/jstemplate/batteryPrice.jsp">
						<dsp:param name="skuId" value="${skuId}" />
						<dsp:param name="productId" value="${productId}" />
						<dsp:param name="unCartable" value="${unCartable}" />
						<dsp:param name="priceDisplay" value="${priceDisplay}" />
						<dsp:param name="batterySection" value="batterySection" />
					</dsp:include>		
				<div class="button-part batteryItemAddtoCart">
				<input type="hidden"  id="ItemQty"  value="${batteryQuantity}" />
	 	 		<c:choose>
					<c:when test="${notifyMe eq 'Y' && ((inventoryStatus eq 'outOfStock' && s2s eq 'N' && ispu eq 'N' && empty locationIdFromCookie && channelAvailability ne 'In Store Only') 
                       					|| (inventoryStatus eq 'outOfStock' && not empty locationIdFromCookie && s2s eq 'N' && ispu eq 'N'))}">
                       <div class="battery-emailme-section">
						<div class="NotifyMeDescription hide">${description}</div>						
						<button onclick='loadNotifyMe(this);javascript:utag.link({"engagement_impression":"${tealiumSite}: Product Detail Page: Notify Me When Available"});'  class="add-to-cart-new email-me-when-available" data-toggle="modal" data-target="#notifyMeModal"><fmt:message key="tru.productdetail.label.emailmeavailable"/></button>							
						</div>
						</c:when>
						<c:when test="${(inventoryStatus eq 'outOfStock') && (empty locationIdFromCookie) && !(s2s eq 'N' && ispu eq 'N')}">
                    		<button  class="add-to-cart-new" data-toggle="modal" data-target="#findInStoreModal" onclick="javascript:return loadFindInStore('${contextPath}','${skuId}','',$('.QTY-${productId}').val());"><fmt:message key="tru.productdetail.label.addtocart"/></button>
						</c:when>
						<c:when test="${(inventoryStatus eq 'outOfStock') && (not empty locationIdFromCookie) && !(s2s eq 'N' && ispu eq 'N')}">
						 <div class="battery-addtocart-section">
						<button class="add-to-cart-new" data-toggle="modal"
						data-target="#shoppingCartModal"
						onclick="javascript: return addItemToCart('${productId}','${skuId}','','${contextPath}',this,'fromPDPBatteries');"><fmt:message key="tru.productdetail.label.addtocart"/></button>
						 </div>
						</c:when>
						<c:when test="${(!unCartable) && (inventoryStatus ne 'outOfStock')}">
						<button class="add-to-cart-new" data-toggle="modal"
						data-target="#shoppingCartModal"
						onclick="javascript: return addItemToCart('${productId}','${skuId}','','${contextPath}',this,'fromPDPBatteries');"><fmt:message key="tru.productdetail.label.addtocart"/></button>
						</c:when>
						<c:otherwise>
						<button disabled="disabled" class="add-to-cart-new fade-add-to-cart"><fmt:message key="tru.productdetail.label.addtocart"/></button>
						</c:otherwise>
				</c:choose>
				<%-- <c:if test="${presellable}">
					<div class="battery-pre-order">
						<p><fmt:message key="tru.productdetail.label.preorder"/></p>
					</div>
				</c:if> --%>
				</div>
				 
			</div>
			</dsp:oparam>
			<dsp:oparam name="outputEnd">
			</dsp:oparam>
			</dsp:droplet>
		</div>
	</div>
    <hr>
	</c:if>
</dsp:page>