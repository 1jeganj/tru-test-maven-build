<dsp:page>

<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof var="productId" param="productId" />
<dsp:getvalueof var="pageName" param="pageName" />
<dsp:getvalueof var="commerceItemId" param="commerceItemId" />
<dsp:getvalueof var="relationshipItemId" param="relationshipItemId" />
<dsp:getvalueof var="variantName" param="variantName" />
<dsp:getvalueof var="selectedColor" param="selectedColorId" />
<dsp:getvalueof var="selectedSize" param="selectedSize" />
<dsp:droplet name="/com/tru/commerce/droplet/TRUDisplayProductInfoDroplet">
<dsp:param name="productId" value="${productId}"/>
<dsp:param name="selectedColor" value="${selectedColor}"/>
<dsp:param name="selectedSize" value="${selectedSize}"/>
<dsp:param name="variantName" value="${variantName}"/>
		<dsp:oparam name="output">
	<c:choose>
		<c:when test="${pageName eq 'quickView'}">
			<dsp:include page="/jstemplate/quickViewContent.jsp">
				<dsp:param name="templateName" value="${templateName}"/>
				<dsp:param name="productInfo" param="productInfo"/>
			</dsp:include>
		
		</c:when>
		<c:when test="${pageName eq 'collectionPage'}">
			<dsp:include page="/jstemplate/collectionProduct.jsp">
				<dsp:param name="templateName" value="${templateName}"/>
				<dsp:param name="productInfo" param="productInfo"/>
			</dsp:include>
		</c:when>
		<c:when test="${pageName eq 'editCart'}">
			<dsp:include page="/cart/editCart.jsp">
				<dsp:param name="templateName" value="${templateName}"/>
				<dsp:param name="productId" value="${productId}"/>
				<dsp:param name="selectedColor" value="${selectedColor}"/>
				<dsp:param name="selectedSize" value="${selectedSize}"/>
				<dsp:param name="relationshipItemId" value="${relationshipItemId}"/>
				<dsp:param name="commerceItemId" value="${commerceItemId}"/>
				<dsp:param name="fromAjaxEditCart" value="true"/>
				<dsp:param name="productInfo" param="productInfo"/>
			</dsp:include>
		</c:when>
		<c:otherwise>
			<dsp:include page="/jstemplate/mainProductDetailSection.jsp">
				<dsp:param name="templateName" value="${templateName}"/>
				<dsp:param name="productInfo" param="productInfo"/>
			</dsp:include>
		</c:otherwise>
	</c:choose>
		
     </dsp:oparam>
     </dsp:droplet>
</dsp:page>