<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/atg/commerce/locations/StoreLocatorFormHandler"/>
<dsp:importbean bean="/com/tru/storelocator/cml/GeoLocatorConfigurations"/>
<dsp:importbean bean="/atg/commerce/locations/StoreLocatorFormHandler"/>
<dsp:importbean bean="/atg/commerce/locations/StoreLookupDroplet"/>
<dsp:importbean bean="/atg/dynamo/droplet/Range" />
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/multisite/Site" />


	<dsp:getvalueof var="siteID" bean="Site.id" />
	<dsp:getvalueof var="findStore" param="findStore"/>
	<dsp:getvalueof var="maxResultsPerPage" bean="GeoLocatorConfigurations.maxResultsPerPage"/>
	<dsp:getvalueof var="totalResults" bean="StoreLocatorFormHandler.resultSetSize"/>
	
	<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	
	
        <div class="modal-dialog">
            <div class="modal-content sharp-border">
				<span class="sprite-icon-x-close findin-storemodels-close" data-dismiss="modal"></span>
                <div class="find-in-store-overlay">
                    <div class="row">
                     
                        <div class="col-md-6 border-right product-side">
                            <h3><dsp:valueof param="productInfo.displayName" valueishtml="true" /></h3>
                            <img class="product-thumb" src="${TRUImagePath}images/find-in-store-product-thumb.png" alt="find-in-store-product-thumb">
                            <div class="options-text">
                                <span><fmt:message key=" tru.productdetail.label.color" />:</span><span class="selected-color"> Red</span>
                            </div>
                            <div class="colors">
                                <div class="color-block">
                                    <div class="color-block-inner">

                                    </div>
                                </div>
                            </div>
                            <div class="options-text">
                                <span><fmt:message key="tru.productdetail.label.size"/></span><span class="selected-size"></span>
                            </div>
                            <br>
                            <div class="sticky-quantity">
                                <div class="options-text">
                                    <fmt:message key="tru.productdetail.label.qty"/> <span class="amount"></span>
                                </div>
                            </div>
                        </div>
                        <c:choose>
							<c:when test="${totalResults gt '0'}">
		                        <div class="col-md-6">
		                            <div class="find-in-store-results">
		                                <div class="store-locator-results">
		                                    <header>
		                                        ${totalResults} nearby stores
		                                        <p>for 43215</p>
		                                        <p><a class="change-location">change location</a>
		                                        </p>
		                                        <dsp:include page="/storelocator/findInStoreSearch.jsp"/>
		                                    </header>
		                                    <ul id="store-locator-results-tabs" class="store-locator-results-tabs nav nav-tabs">
		                                        <li class="site-All">
		                                            <a class="product-review-tab-text" href="javascript:void(0);" data-toggle="tab" onclick="findInAllStore('${contextPath}');">All</a>
		                                        </li>
		                                        <li class="site-Toysrus">
		                                            <a class="product-review-tab-text" href="javascript:void(0);" data-toggle="tab" onclick="findInSiteStore('Toysrus');">Toys "R" Us</a>
		                                        </li>
		                                        <li class="site-Babyrus">
		                                            <a class="product-review-tab-text" href="javascript:void(0);" data-toggle="tab" onclick="findInSiteStore('Babyrus');">Babies "R" Us</a>
		                                        </li>
		                                    </ul>
		                                    <dsp:include page="/storelocator/findInSiteStore.jsp"/>
		                                    <div id="store-locator-results-scrollable" class="tse-scrollable">
		                                    	<div class="tse-content">
		                                    		<dsp:include page="/storelocator/findInStoreSearchResults.jsp">
		                                    			<dsp:param name="selectedSkuId" param="selectedSkuId" />
		                                    		</dsp:include>
		                                    	</div>
		                                    </div>
		                                    <div class="no_specific_chain_stores"><p></p></div>
		                                </div>
		                            </div>
		                        </div>
		                  </c:when>
		                  <c:otherwise>
		                  	<dsp:include page="/storelocator/findInStoreSearch.jsp"/>
		                  	Sorry!!! No Stores Found for the given address..."
		                  </c:otherwise>
		               </c:choose>
                    </div>
                    <div id="store-locator-tooltip-store-detailsPDP" class="tse-scrollable">
						<div class="tse-content">
							<div class="store-address w3c_section">
							</div>
							<div class="store-hours w3c_section">
								<div class="weekday">
								</div>
								<div class="week-timings">
								</div>
							</div>
							<br>
							<h3 class="w3ValidationStoteLocatorServiceHeading">store services</h3>
							<ul>
								<li>
									layaway
									<p>Shop today and take time to pay!</p>
									<a>learn more</a>
								</li>
								<li>
									parenting classes
									<p>Informative classes on everything baby from breastfeeding to infant CPR.</p>
									<a>learn more</a>
								</li>
								<li>
									personal registry advisor
									<p>Schedule a one-on-one appointment with a Personal Registry Advisor to help you create the perfect registry.</p>
									<a>learn more</a>
								</li>
							</ul>
						</div>
					</div>
					
                </div>
            </div>
        </div>
  </dsp:page>