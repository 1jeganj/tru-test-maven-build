<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="viewWishListUrl" bean="TRUStoreConfiguration.viewWishListUrl" />
<dsp:getvalueof param="skuDisplayName" var="skuDisplayName" />
<dsp:getvalueof param="page" var="page" />
<dsp:getvalueof param="primaryImage" var="primaryImage" />
<c:choose> 
	<c:when test="${(page eq 'quickView')}">
<div class="pdp-modal2">
					<div class="close-btn">
						<img src="${TRUImagePath}images/close.png" data-dismiss="modal" alt="close modal">
					</div>
					<div class="wc3ValidationHeaderAddToRegistry">
						<img src="${TRUImagePath}images/Payment_Green-Checkmark.jpg" alt="">Item(s) Added						
						
						<c:choose>
						<c:when test="${ page eq 'collection'}">
						         <p> <fmt:message key="tru.productdetail.label.wishlistsuccessmessageforproduct" />&nbsp;<b class="gift_recipient_name"></b>&nbsp;<fmt:message key="tru.productdetail.label.wishlistname" /></p>
							<!-- <p>You have successfully added these products to <b class="gift_recipient_name"></b> wishlist</p> -->
						</c:when>
						<c:otherwise>		
						          <p><fmt:message key="tru.productdetail.label.wishlistaddconfirmation" />&nbsp;<b class="wishlistSkuDisplayName">"${skuDisplayName}"</b>&nbsp;<fmt:message key="tru.productdetail.label.wishlistto" />&nbsp;<b class="gift_recipient_name"></b>&nbsp;<fmt:message key="tru.productdetail.label.wishlistname" /></p>		
							<%-- <p>you have successfully added the <b class="wishlistSkuDisplayName">"${skuDisplayName}"</b> to <b class="gift_recipient_name"></b> wishlist.</p> --%>
						</c:otherwise>
						</c:choose>
					</div>
					<!--action-btns start-->
					<div class="action-btns">
						<div class="row">
							<div class="col-md-4 col-md-offset-4">
								<button onclick="javascript:location.href='${viewWishListUrl}'"><fmt:message key="tru.productdetail.label.viewwishlist" /><!-- View Wish List --></button>
							</div>							
							<div class="col-md-4">
								<button data-dismiss="modal"><fmt:message key="tru.productdetail.label.wishlistcontinueshopping" /><!-- Continue Shopping --></button>
							</div>
						</div>
					</div>
				</div>
	</c:when>
<c:otherwise>
				<div class="pdp-modal2 spark_PDP">
					<div class="modalHeader">
						<div class="modalTitle">item(s) Added</div>	
						<div class="close-btn"><img src="/images/i-c-n-close.png" data-dismiss="modal" alt="close modal"></div>
					</div>
					<div class="wc3ValidationHeaderAddToRegistry">
						<c:choose>
						<c:when test="${ page eq 'collection'}">
						        <%--  <img src="${TRUImagePath}images/Payment_Green-Checkmark.jpg" alt=""> --%>
						        <span class="gift-sprite-image heart"></span>
						        <span> <fmt:message key="tru.productdetail.label.wishlistsuccessmessageforproduct" />&nbsp;<b class="gift_recipient_name"></b>&nbsp;<fmt:message key="tru.productdetail.label.wishlistname" /></span>
							<!-- <p>You have successfully added these products to <b class="gift_recipient_name"></b> wishlist</p> -->
						</c:when>
						<c:otherwise>		
						         <%--  <img src="${TRUImagePath}images/Payment_Green-Checkmark.jpg" alt=""> --%>
						          <span class="gift-sprite-image heart"></span>
						          <span><%-- <fmt:message key="tru.productdetail.label.wishlistaddconfirmation" /><fmt:message key="tru.productdetail.label.wishlistto" /> --%>item is successfully added to &nbsp;<b class="gift_recipient_name"></b><fmt:message key="tru.productdetail.label.wishlistname" /></span>		
							<%-- <span>you have successfully added the <b class="wishlistSkuDisplayName">"${skuDisplayName}"</b> to <b class="gift_recipient_name"></b> wishlist.</span> --%>
						</c:otherwise>
						</c:choose>
					</div>
					<div class="image-name-container">
						<div class="product-image"><b class="registryPrimaryImage"></b><img src="${primaryImage}?fit=inside|480:480" alt="${skuDisplayName}"></div>							
						<div class="product-name">${skuDisplayName}</div>
					</div>
					<!--action-btns start-->
					<div class="action-btns">
						<!-- <div class="row"> -->
							<div class="btns success">
								<button onclick="javascript:location.href='${viewWishListUrl}'"><%-- <fmt:message key="tru.productdetail.label.viewwishlist" /> --%>View Wish List </button>
							</div>							
							<div class="btns cancel">
								<button data-dismiss="modal">Continue Shopping<%-- <fmt:message key="tru.productdetail.label.wishlistcontinueshopping" /> --%><!-- Continue Shopping --></button>
							</div>
						<!-- </div> -->
					</div>
				</div> 
 </c:otherwise>
</c:choose>
</dsp:page>