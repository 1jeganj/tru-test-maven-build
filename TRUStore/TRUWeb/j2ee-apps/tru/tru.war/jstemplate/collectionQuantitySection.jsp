<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
 <dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
 <dsp:getvalueof param="productInfo.defaultSKU.customerPurchaseLimit" var="customerPurchaseLimit" />
 <dsp:getvalueof param="productInfo.defaultSKU.customerPurchaseLimit" var="toShowcustomerPurchaseLimit"/>
  <dsp:getvalueof param="productInfo.productId" var="productId" />
 <dsp:getvalueof param="productInfo.defaultSKU.id" var="skuId" />
  <dsp:getvalueof param="locationIdFromCookie" var="locationIdFromCookie" />
 <dsp:getvalueof param="productInfo.defaultSKU.unCartable" var="unCartable"/>
 <dsp:getvalueof param="productInfo.defaultSKU.inventoryStatus" var="inventoryStatus" />
 <dsp:getvalueof param="productInfo.defaultSKU.channelAvailability" var="channelAvailability" />
  <dsp:getvalueof param="productInfo.defaultSKU.registryWarningIndicator" var="registryWarningIndicator" />
  <dsp:getvalueof param="productInfo.defaultSKU.skuRegistryEligibility" var="skuRegistryEligibility" />
 <dsp:getvalueof param="productInfo.defaultSKU.s2s" var="s2s" />
 <dsp:getvalueof param="productInfo.defaultSKU.ispu" var="ispu" />
  <dsp:getvalueof param="storeAvailability" var="storeAvailability" />
  <dsp:getvalueof param="productInfo.childSKUsList" var="childSKUsList" />
<dsp:getvalueof param="productInfo.colorSizeVariantsAvailableStatus" var="colorSizeVariantsAvailableStatus" />
<c:if test="${not empty childSKUsList}">
	<dsp:getvalueof var="childSKUsListLength" value="${fn:length(childSKUsList)}"></dsp:getvalueof>
</c:if>
	<c:if test="${empty s2s}">
		<dsp:getvalueof value="N" var="s2s" />
	</c:if>
	<c:if test="${empty ispu}">
		<dsp:getvalueof value="N" var="ispu" />
	</c:if>
	<div class="sticky-quantity">
		<div class="options-text">
			<fmt:message key="tru.productdetail.label.qty" />
		</div>
		<div class="cp-qty-errorMsg hide">
		<!--the Qty you are attempting to add to cart has reached the allowable limit per user  --><fmt:message key="tru.productdetail.label.collectionquantityerrormessage"/></div>
		 <div class="collection_registry_unavailable_error hide"></div>
		<div class="stepper cp-collectionQty">
			<a href="javascript:void(0)" aria-label="decrease value" class="decrease disabled cp-decrease"></a>
			<c:if test="${empty customerPurchaseLimit}">
				<dsp:getvalueof value="999" var="customerPurchaseLimit" />
			</c:if>
			<input type="hidden" value="${productId}-${skuId}-${locationIdFromCookie}" class="cp-collectionItemDetails" />
			<input type="hidden" value="${colorSizeVariantsAvailableStatus}-${childSKUsListLength}" class="cp-productType-activeSkulength" />
			<input type="hidden" value="${locationIdFromCookie}" class="locationIdFromCookie"/>
			<input type="hidden" value="${inventoryStatus}" class="inventoryStatus" />
			<input type="hidden" value="${channelAvailability}" class="channelAvailability" />
			<input type="hidden" value="${storeAvailability}" class="storeAvailability"/>
			<input type="hidden" value="${registryWarningIndicator}" class="registryWarningIndicator"/>
			<input type="hidden" value="${skuRegistryEligibility}" class="skuRegistryEligibility"/>		 
			 
			<input type="hidden" value="${customerPurchaseLimit}" class="cp-quantity-limit-value" id="${productId}" />
			<c:choose>
				<c:when test="${(unCartable eq true) || (empty locationIdFromCookie && s2s eq 'N' && ispu eq 'N' && inventoryStatus eq 'outOfStock')|| ( not empty locationIdFromCookie && inventoryStatus eq 'outOfStock' && storeAvailability ne 'available')}">
					<input disabled="disabled" type="text"
						class="counter center cp-item-qty QTY-${productId}" data-id="QTY-${productId}"
						onkeypress="return isNumberKey(event)" contenteditable="true"
						value="0" maxlength="3" />
					<a href="javascript:void(0)" aria-label="increase value" class="increase disabled  cp-increase" data-toggle="popover"></a>
				</c:when>
				<c:otherwise>
					<input type="text" class="counter center cp-item-qty QTY-${productId}" data-id="QTY-${productId}"
						onkeypress="return isNumberKey(event)" contenteditable="true"
						value="0" maxlength="3"  aria-label="Item Count"/>
					<a href="javascript:void(0)" aria-label="increase value" class="increase enabled cp-increase" data-toggle="popover"></a>
				</c:otherwise>
			</c:choose>
		</div>
		<c:if test="${not empty toShowcustomerPurchaseLimit}">
						<div class="limit-1 inline">
							<fmt:message key="tru.productdetail.label.limit" />
							&#32;${toShowcustomerPurchaseLimit}&#32;
							<fmt:message key="tru.productdetail.label.itemsPerCustomer" />
						</div>
					</c:if>
					
	</div>
</dsp:page>