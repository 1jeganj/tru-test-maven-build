<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/droplet/TRURegistryCookieDroplet"/>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="pdpRegistryUrl" bean="TRUStoreConfiguration.pdpRegistryUrl" />
<dsp:getvalueof var="registryWishlistkeyMap" bean="TRUStoreConfiguration.registryWishlistkeyMap" />
<dsp:getvalueof param="registryWarningIndicator" var="registryWarningIndicator"></dsp:getvalueof>
<dsp:getvalueof param="skuRegistryEligibility" var="skuRegistryEligibility"></dsp:getvalueof>
<dsp:getvalueof  param="productInfo.rusItemNumber" var="skn"/>
<dsp:getvalueof  param="productInfo.defaultSKU.upcNumbers" var="upcNumbers"/> 
 <dsp:getvalueof param="productInfo.defaultSKU.displayName" var="skuDisplayName" />
<dsp:getvalueof param="productInfo.defaultSKU.id" var="uid" /> 
<dsp:getvalueof param="productInfo.productId" var="productId" /> 
<dsp:getvalueof param="productInfo.defaultSKU.rmsColorCode" var="rmsColorCode" />
<dsp:getvalueof param="productInfo.defaultSKU.rmsSizeCode" var="rmsSizeCode" />
<dsp:getvalueof param="productInfo.defaultSKU.sknOrigin" var="sknOrigin" />
<dsp:getvalueof  param="productInfo.defaultSKU.originalproductIdOrSKN" var="skn"/>
<c:forEach var="entry" items="${registryWishlistkeyMap}">
		<c:if test="${entry.key eq 'registryType'}">
			<c:set var="registryType" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'defaultColor' }">
			<c:set var="color" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'defaultSize'}">
			<c:set var="size" value="${entry.value}" />
		</c:if>
		<%-- <c:if test="${entry.key eq 'defaultSknOrigin'}">
			<c:set var="sknOrigin" value="${entry.value}" />
		</c:if> --%>
		<c:if test="${entry.key eq 'nonSpecificIndicator'}">
			<c:set var="nonSpecificIndicator" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'itemRequested'}">
			<c:set var="itemRequested" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'itemPurchased'}">
			<c:set var="itemPurchased" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'purchaseUpdateIndicator'}">
			<c:set var="purchaseUpdateIndicator" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'source'}">
			<c:set var="source" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'messageId'}">
			<c:set var="messageId" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'country'}">
			<c:set var="country" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'language'}">
			<c:set var="language" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'deletedFlag'}">
			<c:set var="deletedFlag" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'wishlistRegistryType'}">
			<c:set var="wishlistRegistryType" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'registryType'}">
			<c:set var="registryType" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'timeout'}">
			<c:set var="timeout" value="${entry.value}" />
		</c:if>
	</c:forEach>
	<c:if test="${not empty upcNumbers}">
		<c:forEach items="${upcNumbers}" var="upcNumber" end="0">
		  <c:set var="upc" value="${upcNumber}" />
		</c:forEach>
	</c:if>
	<c:if test="${not empty rmsColorCode}">
		<c:set var="color" value="${rmsColorCode}"></c:set>
	</c:if>
	<c:if test="${not empty rmsSizeCode}">
		<c:set var="size" value="${rmsSizeCode}"></c:set>
	</c:if>
	<dsp:droplet name="TRURegistryCookieDroplet">
		<dsp:param name="retrieveRegistryCookie" value="true" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="registry_id" param="registry_id" />
			<dsp:getvalueof var="lastAccessedRegistryID" param="lastAccessedRegistryID" />
		</dsp:oparam>
	</dsp:droplet>
	<input type="hidden" id="compareTimeout" value="${timeout}">
	<input type="hidden" id="compareProductDetails" value="${productId}|${skn}|${upc}|${uid}|${color}|${size}|${sknOrigin}|${itemRequested}">
	<input type="hidden" id="registryDetails" value="${registryType}|${skn}|${upc}|${uid}|${sknOrigin}|${color}|${size}|${itemRequested}">
	<input type="hidden" id="compareSkuDisplayName" value="${fn:replace(skuDisplayName ,'"', '&#034;')}">
<c:choose>
     	<c:when test="${not empty registryWarningIndicator && registryWarningIndicator eq 'ALLOW' && skuRegistryEligibility eq true}">
					<c:choose>
						<c:when test="${empty registry_id}">
							<div class="add-to-registry">
								<div class="inline">
									<a href="${pdpRegistryUrl}" class="small-orange cart-button-enable addToRegistryCompare" data-registry="registry">
										<fmt:message key="tru.productdetail.label.addTo" />&nbsp<fmt:message key="tru.productdetail.label.compareregistry" /></a>
								</div>
								<img class="inline" src="${TRUImagePath}images/breadcrumb-arrow-right.png">
							</div>
						</c:when>
						<c:otherwise>
							<div class="add-to-registry">
								<div class="inline">
									<a href="javascript:void(0);"
										class="small-orange cart-button-enable compareaddtoregistry"
										data-registry="registry" ><fmt:message key="tru.productdetail.label.addTo" />&nbsp<fmt:message
											key="tru.productdetail.label.compareregistry" /></a>
								</div>
								<img class="inline" src="${TRUImagePath}images/breadcrumb-arrow-right.png">
							</div>
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:when
					test="${registryWarningIndicator eq 'DENY' || skuRegistryEligibility eq false}">
					<div class="add-to-registry">
						<div class="inline">
							<a href="javascript:void(0);"
								class="small-orange cart-button-disable compareDenyPopover"
								data-registry="registry"
								data-content='<fmt:message key="tru.productdetail.label.compareDenyRegistryMessage"/>'><fmt:message key="tru.productdetail.label.addTo" />&nbsp<fmt:message
									key="tru.productdetail.label.compareregistry" /></a>
						</div>
						<img class="inline" src="${TRUImagePath}images/breadcrumb-arrow-right.png">
					</div>
				</c:when>
				<c:when test="${registryWarningIndicator eq 'WARN' && skuRegistryEligibility eq true}">
					<c:choose>
						<c:when test="${empty registry_id}">
							<div class="add-to-registry">
								<div class="inline">
									<a href="${pdpRegistryUrl}"
										class="small-orange cart-button-enable warnRegistryPopover"
										data-toggle="popover" data-placement="bottom"
										data-content='<fmt:message key="tru.productdetail.label.compareWarnRegistryMessage" />'><fmt:message key="tru.productdetail.label.addTo" />&nbsp<fmt:message
									key="tru.productdetail.label.compareregistry" /></a>
								</div>
								<img class="inline" src="${TRUImagePath}images/breadcrumb-arrow-right.png">
							</div>
						</c:when>
						<c:otherwise>
							<div class="add-to-registry">
								<div class="inline">
									<a href="javascript:void(0);"
								class="small-orange cart-button-enable compareDenyPopover"
								data-registry="registry" data-toggle="popover"
								data-placement="bottom"
								data-content='<fmt:message key="tru.productdetail.label.compareWarnRegistryMessage" />'><fmt:message key="tru.productdetail.label.addTo" />&nbsp<fmt:message
									key="tru.productdetail.label.compareregistry" /></a>
								</div>
								<img class="inline" src="${TRUImagePath}images/breadcrumb-arrow-right.png">
							</div>
						</c:otherwise>
					</c:choose>
				</c:when>
					<c:otherwise>
					<div class="add-to-registry">
						<div class="inline">
							<a href="javascript:void(0);" 
							class="small-orange cart-button-disable compareDenyPopover"
							data-content='<fmt:message key="tru.productdetail.label.compareDenyRegistryMessage"/>'><fmt:message key="tru.productdetail.label.addTo" />&nbsp<fmt:message
									key="tru.productdetail.label.compareregistry" /></a>
						</div>
						<img class="inline" src="${TRUImagePath}images/breadcrumb-arrow-right.png">
					</div>
					</c:otherwise>
			</c:choose>
</dsp:page>