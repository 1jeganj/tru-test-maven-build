<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUEncodeDroplet" />
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration"/>
<dsp:getvalueof bean="TRUStoreConfiguration.collectionUrl" var="collectionUrl"/>
<dsp:importbean bean="/com/tru/utils/TRUPdpURLDroplet" />
<dsp:getvalueof var="collectionProductInfo" param="collectionProductInfo" />
<dsp:getvalueof param="fromCollectionPage" var="fromCollectionPage"></dsp:getvalueof>
<dsp:importbean bean="/atg/multisite/Site" />
<dsp:getvalueof var="siteID" bean="Site.id" />
<c:choose>
	<c:when test="${siteID eq 'ToysRUs'}">
	<c:set var="tealiumSite" value="TRU"/>	
	</c:when>
	<c:when test="${siteID eq 'BabyRUs'}">
	<c:set var="tealiumSite" value="BRU"/>
	</c:when>
	<c:otherwise>
		
	</c:otherwise>
</c:choose>
	<c:choose>
		<c:when test="${fromCollectionPage eq 'true'}">
		<dsp:getvalueof value="${collectionProductInfo.collectionName}" var="skuDisplayName" />
		<dsp:getvalueof value="${collectionProductInfo.collectionId}" var="productId" />
		<dsp:getvalueof value="${productId}" var="skuId" />
		<dsp:getvalueof value="${collectionProductInfo.collectionImage}" var="primaryImage" />
		<dsp:getvalueof var="productPageUrl" value="${siteURL}${collectionUrl}${productId}" />
		</c:when>
		<c:otherwise>
			<dsp:getvalueof param="productInfo.defaultSKU.displayName" var="skuDisplayName" />
			<dsp:getvalueof param="productInfo.productId" var="productId" />
			<dsp:getvalueof param="productInfo.defaultSKU.id" var="skuId" />
			<dsp:getvalueof param="productInfo.defaultSKU.primaryImage" var="primaryImage" />
			<dsp:getvalueof param="productInfo.displayName" var="productName" />
			<dsp:getvalueof param="productInfo.defaultSKU.onlinePID" var="onlinePID" />
			<dsp:droplet name="/com/tru/utils/TRUPdpURLDroplet">
				<%-- <dsp:param name="productId" value="${productId}"/> --%>
				<dsp:param name="productId" value="${onlinePID}" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="productPageUrl" param="productPageUrl" />
				</dsp:oparam>
			</dsp:droplet>
		</c:otherwise>
	</c:choose>
	<dsp:droplet name="/com/tru/common/droplet/TRUGetNonSecureUrlDroplet">
 		<dsp:param bean="/OriginatingRequest.serverName" name="hostName"/>
 		<dsp:param name="urlPath" param="productInfo.pdpURL" />
         <dsp:oparam name="output">
         	<dsp:getvalueof var="pdpPageURL" param="nonSecureUrl"/>
         </dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="TRUEncodeDroplet">
		<dsp:param name="data" value="${pdpPageURL}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof param="enocdeData" var="encodedPDPURL" />
		</dsp:oparam>	
	</dsp:droplet>
	<dsp:droplet name="TRUEncodeDroplet">
		<dsp:param name="data" param="productInfo.displayName"/>
		<dsp:oparam name="output">
			<dsp:getvalueof param="enocdeData" var="encodedProductDescription" />
		</dsp:oparam>	
	</dsp:droplet>
	<c:set var="skuDisplayName" value="${fn:escapeXml(skuDisplayName)}"/>
	<input type="hidden" id="skuDisplayName" value="${skuDisplayName}" />
	<div class="social-banner pdp-social-banner">
		<c:set var="productPageUrl" value="${productPageUrl}%26site=${tealiumSite}"/>
		<div id="facebook" class="facebook inline" onclick="javascript:utag.link({'engagement_impression':'${tealiumSite}: Product Detail Page: Like'});" aria-label="facebook share"></div>
		<div id="twitter" class ="twitter inline" onclick="javascript:utag.link({'engagement_impression':'${tealiumSite}: Product Detail Page: Tweet'});" aria-label="twitter share"></div>
		<div id="pinterest" class ="pinterest inline" onclick="javascript:utag.link({'engagement_impression':'${tealiumSite}: Product Detail Page: Pin it'});" aria-label="pinterest share"></div>
        <div class="email inline" data-toggle="modal" data-target="#emailMePopUp" onclick="loademailToFriend('','${primaryImage}','${productId}','${skuId}','${productPageUrl}');javascript:utag.link({'engagement_impression':'${tealiumSite}: Product Detail Page: Email'});" tabindex="0" aria-label="email share"></div>
    </div>
	
	<script>
		var dd = document.getElementById('skuDisplayName').value;
		(function (str){
			document.getElementById('skuDisplayName').value = str;
		})(dd);
	</script>
</dsp:page>
