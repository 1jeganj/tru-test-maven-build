<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof bean="TRUConfiguration.recaptchaScriptURL" var="recaptchaScriptURL" />
<dsp:getvalueof bean="TRUConfiguration.recaptchaNoScriptURL" var="recaptchaNoScriptURL" />
<dsp:getvalueof bean="TRUConfiguration.recaptchaPublicKey" var="recaptchaPublicKey" />
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof var="relatedProductImageBlock" bean="TRUStoreConfiguration.relatedProductImageBlock" />	
	<div class="container-fluid  product-details-template">
	<div class="siteLogoForPrintPage">
    	<span><img src="${TRUImagePath}images/tru-logo-sm.png" alt="toysrus logo"></span>
		<span> <img src="${TRUImagePath}images/babiesrus-logo-small.png" alt="babiesrus logo"></span>
	</div>
	<input type ="hidden" value="PdpTemplate" id="pageTemplate"/>
		<div class="modal fade in" id="quickviewModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false">
			<dsp:include page="/jstemplate/quickView.jsp"></dsp:include>
		</div>
		
		
		<div class="modal fade in pdp-findinstore-popup" id="findInStoreModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<dsp:include page="/jstemplate/findInStoreInfo.jsp"/>
		</div>
		
		<div class="modal fade in pdpModals esrbratingModel" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false">
			<dsp:include page="/jstemplate/esrbMature.jsp">
			</dsp:include>
  	   </div>
		<div class="modal fade in" id="shoppingCartModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false"></div>
		<dsp:include page="/jstemplate/includePopup.jsp">
		  <dsp:param name="productInfo" param="productInfo"/>
		  </dsp:include>
		<dsp:include page="/jstemplate/adZonePDP1.jsp" />
		<dsp:include page="/jstemplate/mainProductDetailSection.jsp">
			<dsp:param name="productInfo" param="productInfo"/>
		</dsp:include>
		<dsp:include page="/jstemplate/relatedProducts.jsp">
			<dsp:param name="productInfo" param="productInfo"/>
			<dsp:param name="relatedProductImageBlock" value="${relatedProductImageBlock}"/>
		</dsp:include>
		<dsp:include page="/jstemplate/adZonePDP3.jsp" />
		<dsp:include page="/jstemplate/recommendedProducts.jsp">
		<dsp:param name="productInfo" param="productInfo"/>
		</dsp:include>
		<dsp:include page="/jstemplate/detailsModelPopUp.jsp"></dsp:include>
		<div class="modal fade in" id="emailMePopUp" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false">

		</div>		
		<script type="text/javascript">
		var reloadCaptcha = function(){
			Recaptcha.create("${recaptchaPublicKey}","recaptcha");
		};
			/* $(window).load(function(){
				if(typeof document.write === "undefined"){
					document.write = tmpDocumentWrite;
				}
				$.getScript('${recaptchaScriptURL}${recaptchaPublicKey}');
			}); */

			$(window).load(function(){
				$.getScript('https://www.google.com/recaptcha/api/js/recaptcha_ajax.js');
			});
		</script>
	</div>
	<div class="scroll-detection"></div>
</dsp:page>