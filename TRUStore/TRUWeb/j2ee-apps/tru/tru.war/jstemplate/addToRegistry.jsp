<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof param="productInfo" var="productInfo"></dsp:getvalueof>
<dsp:getvalueof param="compareProduct" var="compareProduct"></dsp:getvalueof>
<dsp:getvalueof param="colorSizeVariantsAvailableStatus" var="colorSizeVariantsAvailableStatus" />
<dsp:getvalueof param="fromPDP" var="fromPDP"></dsp:getvalueof>
<dsp:getvalueof param="productInfo.defaultSKU.registryWarningIndicator" var="registryWarningIndicator" />
<dsp:getvalueof param="productInfo.defaultSKU.skuRegistryEligibility" var="skuRegistryEligibility" />
<dsp:getvalueof param="selectedSkuId" var="selectedSkuId" />
<dsp:getvalueof  param="productInfo.rusItemNumber" var="skn"/>
<dsp:getvalueof  param="productInfo.defaultSKU.upcNumbers" var="upc"/> 
<dsp:getvalueof param="productInfo.defaultSKU.colorCode" var="color" />
<dsp:getvalueof var="size" param="productInfo.defaultSKU.juvenileSize"/>
 <dsp:getvalueof param="productInfo.defaultSKU.displayName" var="skuDisplayName" />
<dsp:getvalueof param="productInfo.defaultSKU.id" var="uid" /> 
<dsp:getvalueof param="productInfo.productId" var="product" /> 
<%-- <dsp:getvalueof value="itemRequested" var="itemRequested" />Ravi has to check --%>
<dsp:getvalueof var="registry_id" param="registry_id" />
<dsp:getvalueof var="lastAccessedRegistryID" param="lastAccessedRegistryID" />
<dsp:getvalueof var="lastAccessedWishlistID" param="lastAccessedWishlistID" />
<dsp:getvalueof var="wishlist_id" param="wishlist_id" />
	<c:choose>
		<c:when test="${compareProduct eq 'compareProduct'}">
			<c:choose>
				<c:when test="${(colorSizeVariantsAvailableStatus ne 'noColorSizeAvailable')}">
		         </c:when>
		         <c:otherwise>
		         <dsp:include page="/jstemplate/compareProductAddToRegistry.jsp">
				<dsp:param name="registryWarningIndicator" value="${registryWarningIndicator}"/>
				<dsp:param name="skuRegistryEligibility" value="${skuRegistryEligibility}"/>
				<dsp:param name="productInfo" param="productInfo"/>
				<dsp:param name="registry_id" value="${registry_id}" />
				<dsp:param name="lastAccessedRegistry_ID" value="${lastAccessedRegistryID}" />
				<dsp:param name="lastAccessedWishlistID" value="${lastAccessedWishlistID}" />
				<dsp:param name="wishlist_id" value="${wishlist_id}" />
		         </dsp:include>	
		         </c:otherwise>
	         </c:choose>
		</c:when>
		<c:otherwise>
		<dsp:include page="/jstemplate/PDPAddToRegistry.jsp">
		<dsp:param name="registryWarningIndicator" value="${registryWarningIndicator}"/>
		<dsp:param name="skuRegistryEligibility" value="${skuRegistryEligibility}"/>
		<dsp:param name="productInfo" param="productInfo"/>
		<dsp:param name="registry_id" value="${registry_id}" />
		<dsp:param name="lastAccessedRegistryID" value="${lastAccessedRegistryID}" />
		<dsp:param name="lastAccessedWishlistID" value="${lastAccessedWishlistID}" />
		<dsp:param name="wishlist_id" value="${wishlist_id}" />
          </dsp:include>
		</c:otherwise>
	</c:choose>
	
</dsp:page>