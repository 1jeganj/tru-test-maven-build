<dsp:page>
<dsp:importbean bean="/com/tru/commerce/email/TRUEmailNotificationFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/ForgotPasswordHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
			<dsp:setvalue bean="TRUEmailNotificationFormHandler.friendName" paramvalue="friendsname" />
			<dsp:setvalue bean="TRUEmailNotificationFormHandler.friendsEmail" paramvalue="friendsemail" />
			<dsp:setvalue bean="TRUEmailNotificationFormHandler.yourName" paramvalue="yourname" />
			<dsp:setvalue bean="TRUEmailNotificationFormHandler.yourMail" paramvalue="youremail" />
			<dsp:setvalue bean="TRUEmailNotificationFormHandler.personalMessage" paramvalue="personalMessage" />
			<dsp:setvalue bean="TRUEmailNotificationFormHandler.displayName" paramvalue="displayName" />
			<dsp:setvalue bean="TRUEmailNotificationFormHandler.skuId" paramvalue="skuId" />
			<dsp:setvalue bean="TRUEmailNotificationFormHandler.productId" paramvalue="productId" />
			<dsp:setvalue bean="TRUEmailNotificationFormHandler.responseField" paramvalue="g-recaptcha-response" />
			<dsp:setvalue bean="TRUEmailNotificationFormHandler.pdpPageURL" paramvalue="pdpPageURL" />
			
			<dsp:setvalue bean="TRUEmailNotificationFormHandler.emailToFriend" value="submit" />
			<dsp:droplet name="Switch">
				<dsp:param bean="TRUEmailNotificationFormHandler.formError" name="value"/>
				<dsp:oparam name="true">
					 <dsp:droplet name="ErrorMessageForEach">
				        <dsp:param name="exceptions" bean="TRUEmailNotificationFormHandler.formExceptions"/>
						<dsp:oparam name="outputStart">
							Following are the form errors:
						</dsp:oparam>
						<dsp:oparam name="output">
						   <span><li class="esrbMatureError"><dsp:valueof param="message" /></li></span>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="false">
				</dsp:oparam>
			  </dsp:droplet>
</dsp:page>