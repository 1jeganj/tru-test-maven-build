<dsp:page>
<dsp:getvalueof param="productInfo" var="productInfo" />
<dsp:getvalueof param="productInfo.defaultSKU.id" var="skuId" /> 
<dsp:getvalueof param="productInfo.productId" var="productId" /> 
<dsp:getvalueof param="matureValue" var="matureValue" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	<div class="modal-dialog modal-lg">
		<!-- <div class="modal-dialog modal-lg"> -->
			<div class="modal-content sharp-border">
				<div class="pdp-modal1">
					<div class="close-btn">
						<span class="sprite-icon-x-close" data-dismiss="modal"></span>
					</div>
					<div class="w3ValidatorWaring header-bottom">
						<span class="esrbMatureError">Warning</span>
						      <p><b><fmt:message key="tru.productdetail.label.esrbmaturemessage" /></b>
						      &nbsp;<fmt:message key="tru.productdetail.label.maturemessagewarning" /></p>
						<!-- <p><b>One or more of the items you are attempting to add to your cart is intended to mature audiences.</b> You must be 17 or older to purchase.<br>
						This item may contain intense violence, blood and gore, sexual content and/or strong language.</p> -->
					</div>
					<!--action-btns start-->
					<div class="action-btns warning-btns">
					          <p><fmt:message key="tru.productdetail.label.esrbmaturemessageconfirmation" /></p>
						<!-- <p>By ordering this item you are certifing that you are at-least of 17 years of age.</p> -->
						<div class="row">
							<!-- <div class="col-md-2">
								<button class="im-mature-btn" style="display:none">yes</button>
							</div> -->
							<div class="col-md-6">
							         <button class="im-mature-btn"><fmt:message key="tru.productdetail.label.esrbmaturemessageoverseventeen" /></button>
								<!-- <p>I am age 17 or older</p> -->
							</div>
						<!-- </div>
						<div class="row"> -->
							<!-- <div class="col-md-2">
								<button data-dismiss="modal" style="display:none">no</button>
							</div> -->
							<div class="col-md-6">
							     <button data-dismiss="modal"><fmt:message key="tru.productdetail.label.esrbmaturemessageunderseventeen" /></button>
								<!-- <p>I am under 17 age</p> -->
							</div>
						</div>
					</div>					
				</div>				
			</div>
		<!-- </div> -->
	</div>
</dsp:page>