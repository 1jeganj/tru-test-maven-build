<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:getvalueof param="collectionProductInfo" var="collectionProductInfo" />
<dsp:getvalueof param="collectionProductInfo.collectionName" var="collectionName"/>
<dsp:getvalueof param="collectionProductInfo.collectionDescription" var="collectionDescription"/>
<dsp:getvalueof param="collectionProductInfo.collectionImage" var="collectionImage"/>
<dsp:getvalueof param="collectionMainImageBlock" var="collectionMainImageBlock"/>
<!-- <div class="small-spacer"></div> -->
<%-- <div class="collection-hero" tabindex="0">
            <div class="row">
            	<div class="collection-header"><!-- pdp spark changes-->
					<div class="collection-header-content">
						<h2 class="collection-header-text">${collectionName}</h2>
						<!-- <div class="arrow left-arrow"></div>
						<div class="arrow right-arrow"></div> -->
					</div>
				</div>
                <div class="collection-hero-image">
                <c:choose>
                <c:when test="${not empty collectionImage}">
                	  <img class="collectonEmptyImage" src="${collectionImage}${collectionMainImageBlock}" alt="${collectionName}">
	                    <div class="zoom-icon" data-toggle="modal" data-target="#collectionGalleryOverlayModal" tabindex="0" aria-label="product zoom view">
	                  <img src="${TRUImagePath}images/zoom-icon.png" alt="zoom icon">  
	                    </div>
                </c:when>
				<c:otherwise>
					<c:choose>
					 <c:when test="${not empty akamaiNoImageURL}">
					  	<img class="collectonEmptyImage" src="${akamaiNoImageURL}?fit=inside|500:500" alt="${collectionName}">
					 </c:when>
					 <c:otherwise>
					 	<img class="collectonEmptyImage" src="${contextPath}images/no-image500.gif?fit=inside|500:500" alt="${collectionName}">
					 </c:otherwise>
					 </c:choose>				 
                    <div class="zoom-icon" data-toggle="modal" data-target="#collectionGalleryOverlayModal" tabindex="0" aria-label="product zoom view">
                    <img src="${TRUImagePath}images/zoom-icon.png" alt="zoom icon">
					</div>
				</c:otherwise>
                </c:choose>                    
                </div>
                <div class="col-md-7">
                    <p class="collections-hero-description">
                   ${collectionDescription}
                    </p>
                </div>
            </div>
        </div> --%> 
        <div class="collection-hero" tabindex="0">
            <div class="row">
            	<div class="collection-header"><!-- pdp spark changes-->
					<div class="collection-header-content">
						<h2 class="collection-header-text">${collectionName}</h2>
						<input type="hidden" value="${collectionName}" class="collectionDisplayName" >
						<!-- <div class="arrow left-arrow"></div>
						<div class="arrow right-arrow"></div> -->
        </div> 
				</div>
	            <div class="collection-hero-image">
	                <div class="zoomContainer">
	                <c:choose>
	                <c:when test="${not empty collectionImage}">
	                	  <img class="collectonEmptyImage zoomImage" src="${collectionImage}${collectionMainImageBlock}" alt="${collectionName}">
	                	  <img class="collectonEmptyImage largeImage" src="${collectionImage}?fit=inside|960:960" alt="${collectionName}">
		                    <%-- <div class="zoom-icon" data-toggle="modal" data-target="#collectionGalleryOverlayModal" tabindex="0" aria-label="product zoom view">
		                  <img src="${contextPath}images/zoom-icon.png" alt="zoom icon">  
		                    </div> --%>
	                </c:when>
					<c:otherwise>
						<c:choose>
						 <c:when test="${not empty akamaiNoImageURL}">
						  	<img class="collectonEmptyImage zoomImage" src="${akamaiNoImageURL}${collectionMainImageBlock}" alt="${collectionName}">
						  	<img class="collectonEmptyImage largeImage" src="${akamaiNoImageURL}?fit=inside|960:960" alt="${collectionName}">
						 </c:when>
						 <c:otherwise>
						 	<img class="collectonEmptyImage zoomImage" src="${TRUImagePath}images/no-image500.gif${collectionMainImageBlock}" alt="${collectionName}">
						 	<img class="collectonEmptyImage largeImage" src="${TRUImagePath}images/no-image500.gif?fit=inside|960:960" alt="${collectionName}">
						 </c:otherwise>
						 </c:choose>				 
	                   <%--  <div class="zoom-icon" data-toggle="modal" data-target="#collectionGalleryOverlayModal" tabindex="0" aria-label="product zoom view"> modified for spark collection -changes
	                    <img src="${contextPath}images/zoom-icon.png" alt="zoom icon"> 
						</div>--%>
					</c:otherwise>
	                </c:choose>                    
	                </div>
                </div>
                <div class="col-md-7">
                    <p class="collections-hero-description">
                   ${collectionDescription}
                    </p>
                </div>
               <div>
               <dsp:include page="/jstemplate/socialShareLinks.jsp">
               	<dsp:param name="fromCollectionPage" value="true"/>
               	<dsp:param name="collectionProductInfo" value="${collectionProductInfo}"/>
               	
                </dsp:include>
               </div>
            </div>
        </div>
</dsp:page>	