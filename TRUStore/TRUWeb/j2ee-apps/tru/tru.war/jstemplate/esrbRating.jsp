<dsp:page>
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof  param="productInfo.defaultSKU.esrbrating" var="esrbrating"/>
<dsp:getvalueof  param="productInfo.defaultSKU.videoGameEsrb" var="videoGameSKU"/>
<dsp:getvalueof  param="productInfo.productId" var="productId"/>
<dsp:getvalueof value="false" var="matureValue"></dsp:getvalueof>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<c:if test="${not empty videoGameSKU}">
	<div class="logo">
		<c:choose>
			<c:when test="${videoGameSKU eq 'Everyone'}">
				<img src="${TRUImagePath}images/esrb/E.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Early Childhood'}">
				<img src="${TRUImagePath}images/esrb/eC.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Everyone 10+'}">
				<img src="${TRUImagePath}images/esrb/E10Plus.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Mature'}">
				<dsp:getvalueof value="true" var="matureValue"></dsp:getvalueof>
				<img src="${TRUImagePath}images/esrb/M.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Teen'}">
				<img src="${TRUImagePath}images/esrb/T.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Rating Pending'}">
				<img src="${TRUImagePath}images/esrb/RP.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Adults Only'}">
				<dsp:getvalueof value="true" var="matureValue"></dsp:getvalueof>
				<img src="${TRUImagePath}images/esrb/Ao.png" class="esrbImage">
			</c:when>
		</c:choose>

		<div class="limit-1 inline">
			<span class="rating"><fmt:message
					key="tru.productdetail.label.esrbRating" /></span> <br>
				<c:if test="${not empty esrbrating}">
					<fmt:message key="${esrbrating}" />
				</c:if>
		</div>
	</div>
	</c:if>
	 <input id="isAvailableOnlyForMature-${productId}" class="maturevalue" type="hidden" value="${matureValue}">  
 
</dsp:page>