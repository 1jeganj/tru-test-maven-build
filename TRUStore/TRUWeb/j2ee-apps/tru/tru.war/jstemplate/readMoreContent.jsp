<dsp:page>
 	<div class="read-more-content">
		<dsp:include page="safetyWarningSection.jsp">
			<dsp:param name="productInfo" param="productInfo"/>
		</dsp:include>
		<dsp:include page="featuresSection.jsp">
			<dsp:param name="productInfo" param="productInfo"/>
		</dsp:include>
		<dsp:include page="additionalInfoSection.jsp">
			<dsp:param name="productInfo" param="productInfo"/>
		</dsp:include>
		<dsp:include page="howToGetItSection.jsp">
			<dsp:param name="productInfo" param="productInfo"/>
		</dsp:include>
	</div>
</dsp:page>