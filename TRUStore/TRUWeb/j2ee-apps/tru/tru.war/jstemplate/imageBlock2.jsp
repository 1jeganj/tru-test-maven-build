<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />	
<dsp:getvalueof param="primaryImage" var="primaryImage" />
<dsp:getvalueof param="secondaryImage" var="secondaryImage" />
<dsp:getvalueof param="alterNateImages" var="alterNateImages" />
<dsp:getvalueof var="zoomAltPriImageBlock" param="zoomAltPriImageBlock"/>
<dsp:getvalueof var="zoomAltSecImageBlock" param="zoomAltSecImageBlock"/>
<dsp:getvalueof var="zoomAlternativeImage1Block" param="zoomAlternativeImage1Block"/>
<c:choose>
	<c:when test="${not empty pdpH1}">
        <c:set var="altH1SkuName" value="${pdpH1}"/>  
	</c:when>
	<c:otherwise>
		<c:set var="altH1SkuName" value="${displayName}"/>
	</c:otherwise>
</c:choose>        
<c:if test="${not empty primaryImage || not empty secondaryImage || not empty alterNateImages }">
	  <div class="inline gallery-divider"></div>                            		
	       <div class="inline productImages">
		            <p class="gallery-popup-images galleryPopup-images">product images</p> 
		                <c:choose>
			               <c:when test="${ not empty primaryImage}">
			                  <img id="gallery-image-3" title="primary image thumbnail" class="gallery-image-selected pdpimages" src="${primaryImage}${zoomAltPriImageBlock}" onClick="javascript:hideVideoFromOverlay()" alt="${altH1SkuName}">
			                 </c:when>
			               <c:otherwise>
				               <c:choose>
								<c:when test="${not empty akamaiNoImageURL}">
									<img id="gallery-image-3" class="gallery-image-selected pdpimages" src="${akamaiNoImageURL}" onClick="javascript:hideVideoFromOverlay()" alt="${altH1SkuName}">
								</c:when>
								 <c:otherwise>
									<img id="gallery-image-3" class="gallery-image-selected pdpimages" src="${TRUImagePath}images/no-image500.gif" onClick="javascript:hideVideoFromOverlay()" alt="${altH1SkuName}">
								</c:otherwise>
							</c:choose>	
			              </c:otherwise>
		              </c:choose>
		             <c:if test="${not empty secondaryImage}">
			             <img class="gallery-image-unselected pdpimages"  title="secondary image thumbnail" id="gallery-image-4" src="${secondaryImage}${zoomAltSecImageBlock}" onClick="javascript:hideVideoFromOverlay()" alt="${altH1SkuName}">
			         </c:if>
			        <c:choose>
						<c:when test="${empty secondaryImage and not empty alterNateImages }">
						      <c:forEach begin="0" end="3" items="${alterNateImages}" var="alterNateImage">
						        	<img class="gallery-image-4 gallery-image-unselected pdpimages" title="alternate image thumbnail"  src="<c:out value="${alterNateImage}" escapeXml="true"/>${zoomAlternativeImage1Block}" onClick="javascript:hideVideoFromOverlay()" alt="${altH1SkuName}">
						      </c:forEach>
						</c:when>
						<c:otherwise>
							<c:if test="${not empty alterNateImages}">
						      <c:forEach begin="0" end="2" items="${alterNateImages}" var="alterNateImage">
						        	<img class="gallery-image-4 gallery-image-unselected pdpimages" title="alternate image thumbnail"  src="<c:out value="${alterNateImage}" escapeXml="true"/>${zoomAlternativeImage1Block}" onClick="javascript:hideVideoFromOverlay()" alt="${altH1SkuName}">
						      </c:forEach>
							</c:if>
						</c:otherwise>
					</c:choose>
	</div>
</c:if>
</dsp:page>