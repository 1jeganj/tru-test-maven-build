<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
<dsp:importbean bean="/com/tru/common/TRUIntegrationConfiguration" />
<dsp:importbean bean="/com/tru/common/TRUSOSIntegrationConfiguration" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />	
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof param="productInfo.defaultSKU.newItem" var="newItem"></dsp:getvalueof>
<dsp:getvalueof param="productInfo.defaultSKU.longDescription" var="longDescription"></dsp:getvalueof>
<dsp:getvalueof param="productInfo.defaultSKU.id" var="skuId" />
<dsp:getvalueof param="productInfo.defaultSKU.onlinePID" var="onlinePID" />
<dsp:getvalueof param="productInfo" var="productInfo" />
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
<dsp:getvalueof var="storeEnablePowerReviews" bean="TRUIntegrationConfiguration.enablePowerReviews"/>
<dsp:getvalueof var="sosEnablePowerReviews" bean="TRUSOSIntegrationConfiguration.enablePowerReviews"/>
<dsp:droplet name="GetSiteTypeDroplet">
	<dsp:param name="siteId" value="${site}"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="site" param="site"/>
		<c:set var="site" value ="${site}" scope="request"/>
	</dsp:oparam>
</dsp:droplet>
<c:choose>
	<c:when test="${site eq 'sos'}">
		<c:set var="powerReviewsEnabled" value="${sosEnablePowerReviews}" scope="request" />
	</c:when>
	<c:otherwise>
		<c:set var="powerReviewsEnabled" value="${storeEnablePowerReviews}" scope="request" />
	</c:otherwise>
</c:choose>
<div class="header">
	         <div class="row product-breadcrumb-row"> <!--breadcrumb -->  
                <div class="col-md-8 product-breadcrumb-col pdp-breadcrumb-section-new">
                <dsp:include page="/jstemplate/BackToRegistryWishlist.jsp"> 
				</dsp:include>
                <dsp:include page="/cartridges/TRUPDPBreadcrumbs/TRUPDPBreadcrumbs.jsp"> 
                <dsp:param value="${skuId}" name="skuId" />
                 <dsp:param name="productDisplayName" value="${productInfo.defaultSKU.displayName}"/>
				</dsp:include>
                </div>
            </div>
            
            <div class="fixed-product-title">
            <c:choose>
            	<c:when test="${newItem}">
                <div class="tru-new-product-title"></div>
                </c:when>
                <c:otherwise>
                	<div></div>
                </c:otherwise>
                </c:choose>
                

                    
        <div class="product-title-row row">
         <c:choose>
			<c:when test="${not empty pdpH1}">
					<%-- <c:set var="pdpH1" value="${fn:escapeXml(pdpH1)}"/> --%>
                     <h1 class="product-title"><dsp:valueof value="${pdpH1}" valueishtml="true"></dsp:valueof></h1>     
			</c:when>
			<c:otherwise>
			<dsp:getvalueof param="productInfo.defaultSKU.displayName" var="displayName"></dsp:getvalueof>
			<c:set var="displayName" value="${fn:escapeXml(displayName)}"/>
					<h1 class="product-title">${displayName}</h1> 
			</c:otherwise>
		</c:choose>
		</div>
                <%-- <div class="product-title-row row">
                    <div class="product-title"><dsp:valueof param="productInfo.defaultSKU.displayName" valueishtml="true" /></div>
                </div> 
                 <div class="product-title-row row">
                     <h1 class="product-title">
                     <c:choose>
                     <c:when test="${not empty pdpH1}">
                      ${pdpH1}
                     </c:when>
                     <c:otherwise>
                      <dsp:valueof param="productInfo.defaultSKU.displayName" valueishtml="true" />
                     </c:otherwise>
                     </c:choose>
                     </h1>
                    
                </div>--%>
                <div class="row product-review-row">  
                   
                    <div class="product-review-block">
                        <ul class="product-reviews-menu">
                            <c:if test="${not empty longDescription}" > <!--  1  -->
                            <li>
                                <a href="#" class="about-this-toy"><!-- product description --><fmt:message key="tru.productdetail.label.productdescription" /></a>
                            </li>
                            </c:if>
                            
                            <li class="no-enhanced-content">  <!--  2  -->
								<a href="#" class="enhanced-description"><fmt:message key="tru.productdetail.label.enhanceddescription" /></a>
							</li>
							
							<%-- <dsp:getvalueof param="productInfo.defaultSKU.relatedProducts" var="relatedProducts"></dsp:getvalueof>
                            <c:if test="${not empty relatedProducts }">
                            <li>
                                <a href="#" class="related-items"><!-- related items --><fmt:message key="tru.productdetail.label.relateditems" /></a>
                            </li></c:if> --%>
                            
                            <c:if test="${powerReviewsEnabled eq true}"> 
                              	<li> <!--  3  -->
	                                <a href="#" class="ask-a-question"><!-- product question --> product question</a>
	                            </li>
	                            <%-- <li> <!--  4  -->
	                                <a href="#" class="read-a-review"><!-- rateandreview  --><fmt:message key="tru.productdetail.label.rateandreview" /></a>
	                            </li> --%>
	                          
                            </c:if>
                        
					</ul>
                    </div>
                     <div class="inline">
                        <div class="star-rating-img">
                        <c:if test="${powerReviewsEnabled eq true}">    
						 <dsp:include page="/browse/powerreviews.jsp">
						  <dsp:param value="${onlinePID}" name="onlinePID" />
						 </dsp:include>
						</c:if>
                       </div>
                    </div>
                </div>
            </div>

        </div>


</dsp:page>	