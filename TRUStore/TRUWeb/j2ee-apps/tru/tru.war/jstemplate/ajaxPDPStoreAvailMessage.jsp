<dsp:page>
<dsp:importbean bean="/com/tru/commerce/locations/TRUStoreAvailabilityDroplet"/>
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof var="productId" param="productId" />
<dsp:getvalueof var="skuId" param="skuId" />
<dsp:getvalueof var="locationIdFromCookie" param="locationId" />
					<c:if test="${not empty locationIdFromCookie}">
					<dsp:include page="/storelocator/storeAvailMessage.jsp">
						<dsp:param name="skuId" value="${skuId}"/>
						<dsp:param name="locationId" value="${locationIdFromCookie}"/>
					</dsp:include> <span class="dotClass"></span>
					<div class="inline">
						<a data-toggle="modal" data-target="#findInStoreModal"
							href="javascript:void(0);" onclick="javascript:return loadFindInStore('${contextPath}','${skuId}','',$('.QTY-${productId}').val(),this);"> select another store</a>
					</div>
					</c:if>
	
</dsp:page>