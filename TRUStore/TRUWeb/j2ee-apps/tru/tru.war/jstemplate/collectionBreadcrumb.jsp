<dsp:page>
<div>
                        <ul class="category-breadcrumb">
                            <li>
                                <a href="http://cloud.toysrus.resource.com/redesign/template-collections.html#"><img src="${TRUImagePath}images/home-icon-small.png" alt="">
                                </a>
                            </li>
                            <li><img src="${TRUImagePath}images/breadcrumb-right-arrow-small.png" alt="">
                            </li>
                            <li>
                                <div class="btn-group category-breadcrumb-dd">
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <span class="get-going-text">collections</span> 
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                       <li> <img class="sort-by-dropdown-arrow" src="${TRUImagePath}images/arrow-grey.svg" alt="arrow_grey_svg" aria-label = "dropdown_arrow" title="arrow_grey_svg"></li>
                                        <li><a href="http://cloud.toysrus.resource.com/redesign/template-collections.html#">by color</a>
                                        </li>
                                        <li><a href="http://cloud.toysrus.resource.com/redesign/template-collections.html#">by brand</a>
                                        </li>
                                        <li><a href="http://cloud.toysrus.resource.com/redesign/template-collections.html#">by age</a>
                                        </li>
                                        <li><a href="http://cloud.toysrus.resource.com/redesign/template-collections.html#">by gender</a>
                                        </li>
                                        <li><a href="http://cloud.toysrus.resource.com/redesign/template-collections.html#">by size</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li><img src="${TRUImagePath}images/breadcrumb-right-arrow-small.png" alt="">
                            </li>
                            <li>
                                <div class="btn-group category-breadcrumb-dd">
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <span class="get-going-text">collections by color</span> 
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                       <li> <img class="sort-by-dropdown-arrow" src="${TRUImagePath}images/arrow-grey.svg" alt="arrow_grey_svg" aria-label = "dropdown_arrow" title="arrow_grey_svg"></li>
                                        <li><a href="http://cloud.toysrus.resource.com/redesign/template-collections.html#">pink</a>
                                        </li>
                                        <li><a href="http://cloud.toysrus.resource.com/redesign/template-collections.html#">red</a>
                                        </li>
                                        <li><a href="http://cloud.toysrus.resource.com/redesign/template-collections.html#">yellow</a>
                                        </li>
                                        <li><a href="http://cloud.toysrus.resource.com/redesign/template-collections.html#">green</a>
                                        </li>
                                        <li><a href="http://cloud.toysrus.resource.com/redesign/template-collections.html#">blue</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li><img src="${TRUImagePath}images/breadcrumb-right-arrow-small.png" alt="">
                            </li>
                            <li><span class="get-going-text">pink</span>
                            </li>
                        </ul>
                    </div>
</dsp:page>	