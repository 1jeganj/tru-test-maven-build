<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="pdpRegistryUrl" bean="TRUStoreConfiguration.pdpRegistryUrl" />
<dsp:getvalueof var="pdpWishlistUrl" bean="TRUStoreConfiguration.pdpWishlistUrl" />
<dsp:getvalueof param="productInfo" var="productInfo"></dsp:getvalueof>
<dsp:getvalueof param="registryWarningIndicator" var="registryWarningIndicator"></dsp:getvalueof>
<dsp:getvalueof param="skuRegistryEligibility" var="skuRegistryEligibility"></dsp:getvalueof>
<dsp:getvalueof var="registry_id" param="registry_id" />
<dsp:getvalueof var="wishlist_id" param="wishlist_id" />
				<c:choose>
					<c:when
						test="${not empty registryWarningIndicator && registryWarningIndicator eq 'ALLOW' && skuRegistryEligibility eq true}">
							<c:choose>
								<c:when test="${empty registry_id}">
									<button data-href="${pdpRegistryUrl}" class="cart-button-enable baby-registry-addTo registryAnonymous"><fmt:message key="tru.productdetail.label.pdpaddtobabyregistry" />&nbsp;</button>
								</c:when>
								<c:otherwise>
									<button class="cart-button-enable cookie-registry baby-registry-addTo registryLoggedIn"
										data-registry="registry"><fmt:message
											key="tru.productdetail.label.pdpaddtobabyregistry" />&nbsp;</button>
								</c:otherwise>
							</c:choose>
								<c:choose>
									<c:when test="${empty wishlist_id}">
										<button data-href="${pdpWishlistUrl}"
											class="cart-button-enable wish-list-addTo wishlistAnonymous"><fmt:message
												key="tru.productdetail.label.pdpaddtowishlist" />&nbsp;</button>
									</c:when>
									<c:otherwise>
										<button class="cart-button-enable cookie-wishlist wish-list-addTo wishlistLoggedIn"
											data-registry="wishlist"><fmt:message
												key="tru.productdetail.label.pdpaddtowishlist" />&nbsp;</button>
									</c:otherwise>
								</c:choose>
					</c:when>
					<c:when test="${registryWarningIndicator eq 'WARN' && skuRegistryEligibility eq true}">
							<c:choose>
								<c:when test="${empty registry_id}">
									<button data-href="${pdpRegistryUrl}"
										class="cart-button-enable warnRegistryPopover baby-registry-addTo registryAnonymous"
										data-toggle="popover" data-placement="bottom"
										data-content='<fmt:message key="tru.productdetail.label.pdpwarnregistrymessage" />'><fmt:message
											key="tru.productdetail.label.pdpaddtobabyregistry" />&nbsp;</button>
								</c:when>
								<c:otherwise>
									<button class="cart-button-enable cookie-registry warnRegistryPopover baby-registry-addTo registryLoggedIn"
										data-registry="registry" data-toggle="popover"
										data-placement="bottom"
										data-content='<fmt:message key="tru.productdetail.label.pdpwarnregistrymessage" />'><fmt:message
											key="tru.productdetail.label.pdpaddtobabyregistry" />&nbsp;</button>
								</c:otherwise>
							</c:choose>
								<c:choose>
									<c:when test="${empty wishlist_id}">
										<button data-href="${pdpWishlistUrl}"
											class="cart-button-enable warnRegistryPopover wish-list-addTo wishlistAnonymous"
											data-toggle="popover" data-placement="bottom"
											data-content='<fmt:message key="tru.productdetail.label.pdpwarnwishlistmessage" />'><fmt:message
												key="tru.productdetail.label.pdpaddtowishlist" />&nbsp;</button>
									</c:when>
									<c:otherwise>
										<button class="cart-button-enable warnRegistryPopover cookie-wishlist wish-list-addTo wishlistLoggedIn"
											data-registry="wishlist" data-toggle="popover"
											data-placement="bottom"
											data-content='<fmt:message key="tru.productdetail.label.pdpwarnwishlistmessage" />'><fmt:message
												key="tru.productdetail.label.pdpaddtowishlist" />&nbsp;</button>
									</c:otherwise>
								</c:choose>
					</c:when>
					<c:when
						test="${registryWarningIndicator eq 'DENY' || skuRegistryEligibility eq false}">
							<button class="cart-button-disable denyWarningRegistryPopover baby-registry-addTo"
								data-toggle="popover"
								data-placement="bottom"
								data-content='<fmt:message key="tru.productdetail.label.pdpdenyregistrymessage" />'><fmt:message
									key="tru.productdetail.label.pdpaddtobabyregistry" />&nbsp;</button>
							<button class="cart-button-disable denyWarningRegistryPopover wish-list-addTo"
								data-toggle="popover"
								data-placement="bottom"
								data-content='<fmt:message key="tru.productdetail.label.pdpdenywishlistmessage" />'><fmt:message
									key="tru.productdetail.label.pdpaddtowishlist" />&nbsp;</button>
					</c:when>
					<c:otherwise>
							<button class="cart-button-disable denyWarningRegistryPopover baby-registry-addTo"
								data-toggle="popover"
								data-placement="bottom"
								data-content='<fmt:message key="tru.productdetail.label.pdpdenyregistrymessage" />'><fmt:message
									key="tru.productdetail.label.pdpaddtobabyregistry" />&nbsp;</button>
							<button class="cart-button-disable denyWarningRegistryPopover wish-list-addTo"
								data-toggle="popover"
								data-placement="bottom"
								data-content='<fmt:message key="tru.productdetail.label.pdpdenywishlistmessage" />'><fmt:message
									key="tru.productdetail.label.pdpaddtowishlist" />&nbsp;</button>
					</c:otherwise>
				</c:choose>
</dsp:page>