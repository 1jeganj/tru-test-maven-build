<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="enableRegistryModalFlag" bean="TRUStoreConfiguration.enableRegistryModalFlag" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />	
<dsp:getvalueof param="productInfo" var="productInfo" />
<dsp:getvalueof param="productInfo.defaultSKU.displayName" var="displayName" />
<dsp:getvalueof param="productInfo.defaultSKU.primaryCanonicalImage" var="primaryCanonicalImage" />
<dsp:getvalueof param="productInfo.defaultSKU.primaryImage" var="primaryImage" />
<dsp:getvalueof param="page" var="name"/>
<c:choose>
			<c:when test="${not empty pdpH1}">
                     <c:set var="skuDisplayName" value="${pdpH1}"/>  
			</c:when>
			<c:otherwise>
					<c:set var="skuDisplayName" value="${displayName}"/>
			</c:otherwise>
		</c:choose>
      <div id="detailsPopup" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content sharp-border welcome-back-overlay">
					<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
					<h2><fmt:message key="tru.productdetail.label.details" /></h2>
					<p class="abandonCartReminder">
						This message shows how many days we take to ship the item from our warehouse
					</p>
				</div>
			</div>
		</div>
		
		<div id="learnMorePopupStore" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content sharp-border welcome-back-overlay">
					<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
					<h2><fmt:message key="tru.productdetail.label.learnmore" /></h2>
					<p class="abandonCartReminder">
						<fmt:message key="tru.productdetail.label.learnMorePopupStore"></fmt:message>
					</p>
				</div>
			</div>
		</div>   
		<div id="learnMorePopupOnline" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content sharp-border welcome-back-overlay">
					<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
					<h2><fmt:message key="tru.productdetail.label.learnmore" /></h2>
					<p class="abandonCartReminder">
					<fmt:message key="tru.productdetail.label.learnMorePopupOnline"></fmt:message>
					</p>
				</div>
			</div>
		</div>   
		<div id="learnMorePopupBoth" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content sharp-border welcome-back-overlay">
					<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
					<h2><fmt:message key="tru.productdetail.label.learnmore" /></h2>
					<p class="abandonCartReminder">
						<fmt:message key="tru.productdetail.label.learnMorePopupBoth"></fmt:message>
					</p>
				</div>
			</div>
		</div>
		<div class="modal fade detailLearnMorePopup" id="detailsModel" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false" >
			<div class="modal-dialog">
					<div class="modal-content sharp-border welcome-back-overlay">
						<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
						<h2>details</h2>
						<div class="tse-scrollable wrapper">
							<div class="tse-content"> 
								<p id="promoDetailContent"></p>
							</div>
						</div> 
						
					</div>
				</div>
		</div>  
		<div id="pdpPreOrderNow" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false">
			<div class="modal-dialog">
				<div class="modal-content sharp-border welcome-back-overlay">
					<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
					<h2><fmt:message key="tru.productdetail.label.details" /></h2> 
					<!-- <h2>detail</h2> -->
					<p class="abandonCartReminder">
						<!-- This pre-sell item has reached the maximum limit and cannot be added to the cart -->
						<fmt:message key="tru.productdetail.label.preorderdetailmessage" />
					</p>
				</div>
			</div>
		 </div>	
		 <div id="giftWrapDetailModel" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content sharp-border welcome-back-overlay">
					<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
					<div class="tse-scrollable">
						<div class="tse-scroll-content tse-giftWrapDetail-scroll-content">
							<div class="gift-wrap-detail-content tse-content">
								<div class="modal-static-content">
									<div class="abandonCartReminder">
										<fmt:message key="tru.productdetail.label.thingstoknowgiftwrapmessage" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		 <%-- <div id="noGiftWrapDetailModel" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content sharp-border welcome-back-overlay">
					<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
					<h2>details</h2>
					<p class="abandonCartReminder">
						<fmt:message key="tru.productdetail.label.thingstoknownotgiftwrapmessage" />
					</p>
				</div>
			</div>
		</div> --%>
		<div id="learnMorePopupHTGI" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content sharp-border welcome-back-overlay">
					<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
					<h2><fmt:message key="tru.productdetail.label.learnmore" /></h2>
					<p class="abandonCartReminder">
						<fmt:message key="tru.productdetail.label.storePickupHTGI"></fmt:message>
					</p>
				</div>
			</div>
		</div>
		<div id="thingstoKnowBatteryModel" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content sharp-border welcome-back-overlay">
					<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
					<h2>details</h2>
					<p class="abandonCartReminder">
						<fmt:message key="tru.productdetail.label.thingstoKnowBatteryModelMessage" />
					</p>
				</div>
			</div>
		</div>
		<c:if test="${enableRegistryModalFlag && name eq 'productDetails'}">
		<dsp:include page="/jstemplate/registryModalDialog.jsp">
             <dsp:param name="name" value="${skuDisplayName}" />
              <dsp:param name="primaryImage" value="${primaryCanonicalImage}" />
          </dsp:include>
        </c:if>	
</dsp:page>