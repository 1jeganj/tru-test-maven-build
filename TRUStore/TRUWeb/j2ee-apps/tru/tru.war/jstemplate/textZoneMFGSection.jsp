<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof param="productInfo" var="productInfo"></dsp:getvalueof>
<dsp:getvalueof param="productInfo.defaultSKU.brandLandingURL" var="brandingURL"></dsp:getvalueof>
<dsp:getvalueof param="productInfo.defaultSKU.brandName" var="brandName"></dsp:getvalueof>
<dsp:getvalueof param="productInfo.defaultSKU.suggestAgeMessage" var="suggestAgeMessage" />
	<div class="product-details-caption">
		<c:if test="${not empty brandName}">
		<fmt:message key="tru.productdetail.label.by"/>
		</c:if>
		 <c:choose>
		  <c:when test="${not empty brandingURL && not empty brandName}">
             <a href="${brandingURL}" title="${brandName}">${brandName}</a>
          </c:when>
          <c:otherwise>
          	<a href="javascript:void(0)" title="${brandName}">${brandName}</a>
          </c:otherwise>
         </c:choose>
		 <c:if test="${not empty brandName}">
		 <span>&middot;</span>
		 </c:if>
         <c:if test="${not empty suggestAgeMessage}">
		<fmt:message key="tru.productdetail.label.mfgage"/>&nbsp;
		<dsp:valueof value="${suggestAgeMessage}"></dsp:valueof>
		</c:if>
		<%-- <dsp:valueof param="productInfo.defaultSKU.mfrSuggestedAgeMin" />
		-
		<dsp:valueof param="productInfo.defaultSKU.mfrSuggestedAgeMax" />
		<fmt:message key="tru.productdetail.label.years"/> --%>
	</div>
</dsp:page>