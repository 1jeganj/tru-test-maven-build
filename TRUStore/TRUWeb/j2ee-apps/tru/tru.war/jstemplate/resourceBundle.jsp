<dsp:page>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
<dsp:getvalueof var="populateShippingMessage" bean="TRUStoreConfiguration.populateShippingMessage" />
<dsp:getvalueof var="changeAddToCartButtonState" bean="TRUStoreConfiguration.changeAddTocartButtonFromOverlay" />
<dsp:getvalueof var="canonicalImageUrl" bean="TRUStoreConfiguration.canonicalImageUrl" />
<dsp:getvalueof var="registrySwapFlag" bean="TRUStoreConfiguration.registrySwapFlag" />
<dsp:getvalueof var="enableRegistryAJAX" bean="TRUStoreConfiguration.enableRegistryAJAX" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<c:set var="doubleQuote" value='"'/> 
<c:set var="slashDoubleQuote" value='\"'/>
<fmt:message key="tru.productdetail.label.addtocart" var="addToCartMessage"/>
<c:if test="${fn:contains(addToCartMessage,doubleQuote)}"> 
<c:set var="addToCartMessage" value="${fn:replace(addToCartMessage,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.emailmeavailable" var="emailmeavailableMessage"/>
<c:if test="${fn:contains(emailmeavailableMessage,doubleQuote)}"> 
<c:set var="emailmeavailableMessage" value="${fn:replace(emailmeavailableMessage,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.preSellErrorMsg" var="preSellErrorMsg"/>
<c:if test="${fn:contains(preSellErrorMsg,doubleQuote)}"> 
<c:set var="preSellErrorMsg" value="${fn:replace(preSellErrorMsg,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.compareDenyRegistryMessage" var="compareDenyRegistryMessage"/>
<c:if test="${fn:contains(compareDenyRegistryMessage,doubleQuote)}"> 
<c:set var="compareDenyRegistryMessage" value="${fn:replace(compareDenyRegistryMessage,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.pdpdenyregistrymessage" var="pdpDenyRegistryMessage"/>
<c:if test="${fn:contains(pdpDenyRegistryMessage,doubleQuote)}"> 
<c:set var="pdpDenyRegistryMessage" value="${fn:replace(pdpDenyRegistryMessage,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.pdpdenywishlistmessage" var="pdpDenyWishlistMessage"/>
<c:if test="${fn:contains(pdpDenyWishlistMessage,doubleQuote)}"> 
<c:set var="pdpDenyWishlistMessage" value="${fn:replace(pdpDenyWishlistMessage,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.zero" var="zero"/>
<c:if test="${fn:contains(zero,doubleQuote)}"> 
<c:set var="zero" value="${fn:replace(zero,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.collectionitemsselected" var="collectionitemsselected"/>
<c:if test="${fn:contains(collectionitemsselected,doubleQuote)}"> 
<c:set var="collectionitemsselected" value="${fn:replace(collectionitemsselected,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.freestorepickdays" var="freestorepickdays"/>
<c:if test="${fn:contains(freestorepickdays,doubleQuote)}"> 
<c:set var="freestorepickdays" value="${fn:replace(freestorepickdays,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.selectaquantity" var="selectaquantity"/>
<c:if test="${fn:contains(selectaquantity,doubleQuote)}"> 
<c:set var="selectaquantity" value="${fn:replace(selectaquantity,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.pricing.label.priceTooLowToDisplay" var="priceTooLowToDisplay"/>
<c:if test="${fn:contains(priceTooLowToDisplay,doubleQuote)}"> 
<c:set var="priceTooLowToDisplay" value="${fn:replace(priceTooLowToDisplay,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.pricing.label.priceIsNotAvailable" var="priceIsNotAvailable"/>
<c:if test="${fn:contains(priceIsNotAvailable,doubleQuote)}"> 
<c:set var="priceIsNotAvailable" value="${fn:replace(priceIsNotAvailable,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="pricing.yousave"  var="youSavemessage"/> 
<c:if test="${fn:contains(youSavemessage,doubleQuote)}"> 
<c:set var="youSavemessage" value="${fn:replace(youSavemessage,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="pricing.dollar" var="dollerSymbol"/>
<c:if test="${fn:contains(dollerSymbol,doubleQuote)}"> 
<c:set var="dollerSymbol" value="${fn:replace(dollerSymbol,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="pricing.openperanthasis" var="openperanthasis"/>
<c:if test="${fn:contains(openperanthasis,doubleQuote)}"> 
<c:set var="openperanthasis" value="${fn:replace(openperanthasis,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="pricing.persentage" var="percentage"/>
<c:if test="${fn:contains(percentage,doubleQuote)}"> 
<c:set var="percentage" value="${fn:replace(percentage,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="pricing.closeperanthasis" var="closeperanthasis"/>
<c:if test="${fn:contains(closeperanthasis,doubleQuote)}"> 
<c:set var="closeperanthasis" value="${fn:replace(closeperanthasis,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.babyregistry" var="babyregistry"/>
<c:if test="${fn:contains(babyregistry,doubleQuote)}"> 
<c:set var="babyregistry" value="${fn:replace(babyregistry,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.wishlist" var="wishlist"/>
<c:if test="${fn:contains(wishlist,doubleQuote)}"> 
<c:set var="wishlist" value="${fn:replace(wishlist,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.addTo" var="addTo"/>
<c:if test="${fn:contains(addTo,doubleQuote)}"> 
<c:set var="addTo" value="${fn:replace(addTo,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.addToRegistry" var="addToRegistry"/>
<c:if test="${fn:contains(addToRegistry,doubleQuote)}"> 
<c:set var="addToRegistry" value="${fn:replace(addToRegistry,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.addToWishlist" var="addToWishlist"/>
<c:if test="${fn:contains(addToWishlist,doubleQuote)}"> 
<c:set var="addToWishlist" value="${fn:replace(addToWishlist,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.seeterms"  var="seeterms"/>
<c:if test="${fn:contains(seeterms,doubleQuote)}"> 
<c:set var="seeterms" value="${fn:replace(seeterms,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.findinstore" var="findinstoreText"/>
<c:if test="${fn:contains(findinstoreText,doubleQuote)}"> 
<c:set var="findinstoreText" value="${fn:replace(findinstoreText,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.selectAnotherStore" var="selectAnotherStoreText"/>
<c:if test="${fn:contains(selectAnotherStoreText,doubleQuote)}"> 
<c:set var="selectAnotherStoreText" value="${fn:replace(selectAnotherStoreText,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.backToWishlist" var="backToWishlist"/>
<c:if test="${fn:contains(backToWishlist,doubleQuote)}"> 
<c:set var="backToWishlist" value="${fn:replace(backToWishlist,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.backToRegistry" var="backToRegistry"/>
<fmt:message key="tru.productdetail.label.registryerrormessage" var="registryerrormessage"/>
<c:if test="${fn:contains(registryerrormessage,doubleQuote)}"> 
<c:set var="registryerrormessage" value="${fn:replace(registryerrormessage,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.my" var="my" />
<c:if test="${fn:contains(my,doubleQuote)}">
<c:set var="my" value="${fn:replace(my,doubleQuote,slashDoubleQuote)}"/>
</c:if>
<fmt:message key="tru.productdetail.label.wishlisterrormessage" var="wishlisterrormessage"/>
<c:if test="${fn:contains(wishlisterrormessage,doubleQuote)}"> 
<c:set var="wishlisterrormessage" value="${fn:replace(wishlisterrormessage,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<c:if test="${fn:contains(backToRegistry,doubleQuote)}"> 
<c:set var="backToRegistry" value="${fn:replace(backToRegistry,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.specialoffers" var="specialoffer"/>
<c:if test="${fn:contains(specialoffer,doubleQuote)}"> 
<c:set var="specialoffer" value="${fn:replace(specialoffer,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.carttext" var="carttext"/>
<c:if test="${fn:contains(carttext,doubleQuote)}"> 
<c:set var="carttext" value="${fn:replace(carttext,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.addToBabyRegistry" var="addToBabyRegistry"/>
<c:if test="${fn:contains(carttext,doubleQuote)}"> 
<c:set var="addToBabyRegistry" value="${fn:replace(carttext,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.selectcolorsize" var="selectcolorsize"/>
<c:if test="${fn:contains(selectcolorsize,doubleQuote)}"> 
<c:set var="selectcolorsize" value="${fn:replace(selectcolorsize,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.selectcolor" var="selectcolor"/>
<c:if test="${fn:contains(selectcolor,doubleQuote)}"> 
<c:set var="selectcolor" value="${fn:replace(selectcolor,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.selectsize" var="selectsize"/>
<c:if test="${fn:contains(selectsize,doubleQuote)}"> 
<c:set var="selectsize" value="${fn:replace(carttext,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.loadmore" var="loadmore"/>
<c:if test="${fn:contains(loadmore,doubleQuote)}"> 
<c:set var="loadmore" value="${fn:replace(loadmore,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.preorder" var="preOrderAddToCartMessage"/>
<c:if test="${fn:contains(preOrderAddToCartMessage,doubleQuote)}"> 
<c:set var="preOrderAddToCartMessage" value="${fn:replace(preOrderAddToCartMessage,doubleQuote,slashDoubleQuote)}"/> 
</c:if>

<%-- for shipping messages --%>
<fmt:message key="tru.productdetail.label.usuallyleaveswarehouse" var="usuallyLeavesWarehouse"/>
<fmt:message key="tru.productdetail.label.minus" var="labelMinus"/>
<fmt:message key="tru.productdetail.label.fullbusinessdays" var="fullBusinessDays"/>
<fmt:message key="tru.productdetail.label.leavesWarehouse" var="leavesWarehouse"/>
<fmt:message key="tru.productdetail.label.details" var="labelDetails"/>
<fmt:message key="tru.productdetail.label.avilabilitypopover" var="avilabilityPopover"/>

<%-- START:::modified for store locator label--%>
<fmt:message key="tru.storelocator.label.addresstostarted" var="addressToGetStarted"/>
<c:if test="${fn:contains(addressToGetStarted,doubleQuote)}"> 
<c:set var="addressToGetStarted" value="${fn:replace(addressToGetStarted,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.milesaway" var="milesAway"/>
<c:if test="${fn:contains(milesAway,doubleQuote)}"> 
<c:set var="milesAway" value="${fn:replace(milesAway,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.storesnearby" var="storesnearby"/>
<c:if test="${fn:contains(storesnearby,doubleQuote)}"> 
<c:set var="storesnearby" value="${fn:replace(milesAway,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.makethismystore" var="makeThisMyStore"/>
<c:if test="${fn:contains(makeThisMyStore,doubleQuote)}"> 
<c:set var="makeThisMyStore" value="${fn:replace(makeThisMyStore,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.getdirections" var="getdirections"/>
<c:if test="${fn:contains(getdirections,doubleQuote)}"> 
<c:set var="getdirections" value="${fn:replace(getdirections,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.mystore" var="myStore"/>
<c:if test="${fn:contains(myStore,doubleQuote)}"> 
<c:set var="myStore" value="${fn:replace(myStore,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.learnmore" var="learnMore"/>
<c:if test="${fn:contains(learnMore,doubleQuote)}"> 
<c:set var="learnMore" value="${fn:replace(learnMore,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.details" var="detailsDisplay"/>
<c:if test="${fn:contains(detailsDisplay,doubleQuote)}"> 
<c:set var="detailsDisplay" value="${fn:replace(detailsDisplay,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.selectastore" var="selectAStore"/>
<c:if test="${fn:contains(selectAStore,doubleQuote)}"> 
<c:set var="selectAStore" value="${fn:replace(selectAStore,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.phone" var="phone"/>
<c:if test="${fn:contains(phone,doubleQuote)}"> 
<c:set var="phone" value="${fn:replace(phone,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.wecouldnotfind" var="weCouldNotFind"/>
<c:if test="${fn:contains(weCouldNotFind,doubleQuote)}"> 
<c:set var="weCouldNotFind" value="${fn:replace(weCouldNotFind,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.yourstore" var="yourStore"/>
<c:if test="${fn:contains(yourStore,doubleQuote)}"> 
<c:set var="yourStore" value="${fn:replace(yourStore,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.validzipcodeorcity" var="validZipCodeOrCity"/>
<c:if test="${fn:contains(validZipCodeOrCity,doubleQuote)}"> 
<c:set var="validZipCodeOrCity" value="${fn:replace(validZipCodeOrCity,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.unabletocater" var="unableToCater"/>
<c:if test="${fn:contains(unableToCater,doubleQuote)}"> 
<c:set var="unableToCater" value="${fn:replace(unableToCater,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.addtocart" var="addToCartDisplay"/>
<c:if test="${fn:contains(addToCartDisplay,doubleQuote)}"> 
<c:set var="addToCartDisplay" value="${fn:replace(addToCartDisplay,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.searchText" var="searchText"/>
<c:if test="${fn:contains(searchText,doubleQuote)}"> 
<c:set var="searchText" value="${fn:replace(searchText,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.find" var="find"/>
<c:if test="${fn:contains(find,doubleQuote)}"> 
<c:set var="find" value="${fn:replace(find,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.storesnearyou" var="storesnearyou"/>
<c:if test="${fn:contains(storesnearyou,doubleQuote)}"> 
<c:set var="storesnearyou" value="${fn:replace(storesnearyou,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.for" var="for"/>
<c:if test="${fn:contains(for,doubleQuote)}"> 
<c:set var="for" value="${fn:replace(for,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.changelocation" var="changelocation"/>
<c:if test="${fn:contains(changelocation,doubleQuote)}"> 
<c:set var="changelocation" value="${fn:replace(changelocation,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.all" var="all"/>
<c:if test="${fn:contains(all,doubleQuote)}"> 
<c:set var="all" value="${fn:replace(all,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.miles" var="miles"/>
<c:if test="${fn:contains(miles,doubleQuote)}"> 
<c:set var="miles" value="${fn:replace(miles,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.toysrustab" var="toysrustab"/>
<c:if test="${fn:contains(toysrustab,doubleQuote)}"> 
<c:set var="toysrustab" value="${fn:replace(toysrustab,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.babiesrustab" var="babiesrustab"/>
<c:if test="${fn:contains(babiesrustab,doubleQuote)}"> 
<c:set var="babiesrustab" value="${fn:replace(babiesrustab,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="checkout.store.myStore" var="myStore"/>
<c:if test="${fn:contains(myStore,doubleQuote)}"> 
<c:set var="myStore" value="${fn:replace(myStore,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.nearbystores" var="nearbystores"/>
<c:if test="${fn:contains(nearbystores,doubleQuote)}"> 
<c:set var="nearbystores" value="${fn:replace(nearbystores,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.storelocator.label.storeservices" var="storeservices"/>
<c:if test="${fn:contains(storeservices,doubleQuote)}"> 
<c:set var="storeservices" value="${fn:replace(storeservices,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.pdpaddtobabyregistry" var="pdpaddtobabyregistry"/>
<c:if test="${fn:contains(pdpaddtobabyregistry,doubleQuote)}"> 
<c:set var="pdpaddtobabyregistry" value="${fn:replace(pdpaddtobabyregistry,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<fmt:message key="tru.productdetail.label.pdpaddtowishlist" var="pdpaddtowishlist"/>
<c:if test="${fn:contains(pdpaddtowishlist,doubleQuote)}"> 
<c:set var="pdpaddtowishlist" value="${fn:replace(pdpaddtowishlist,doubleQuote,slashDoubleQuote)}"/> 
</c:if>
<%-- END:::modified for store locator label --%>
<script type="text/javascript">
var addToCartMessage = "${addToCartMessage}" ;
var preSellErrorMsg = "${preSellErrorMsg}" ;
var pdpaddtobabyregistry = "${pdpaddtobabyregistry}" ;
var pdpaddtowishlist= "${pdpaddtowishlist}" ;
var preOrderAddToCartMessage = "${preOrderAddToCartMessage}" ;
var collectionItemsSelected="${collectionitemsselected}";
var zero="${zero}";
var emailmeavailableMessage = "${emailmeavailableMessage}" ;
var pdpDenyRegistryMessage = "${pdpDenyRegistryMessage}" ;
var compareDenyRegistryMessage = "${compareDenyRegistryMessage}" ;
var pdpDenyWishlistMessage = "${pdpDenyWishlistMessage}" ;
var priceTooLowToDisplay = "${priceTooLowToDisplay}" ;
var selectaquantity="${selectaquantity}";
var priceIsNotAvailable = "${priceIsNotAvailable}" ;
var dollerSymbol = "${dollerSymbol}" ;
var youSavemessage = "${youSavemessage}" ;
var openperanthasis = "${openperanthasis}" ;
var percentage = "${percentage}" ;
var selectcolorsize="${selectcolorsize}";
var selectcolor="${selectcolor}";
var selectsize="${selectsize}";
var closeperanthasis = "${closeperanthasis}" ;
var addToBabyRegistry = "${addToBabyRegistry}" ;
var carttext="${carttext}";
var babyregistry = "${babyregistry}" ;
var wishlist = "${wishlist}" ;
var my = "${my}";
var addTo = "${addTo}" ;
var addToRegistry = "${addToRegistry}" ;
var addToWishlist = "${addToWishlist}" ;
var pdpSeeTerms = "${seeterms}" ;
var findinstoreText = "${findinstoreText}" ;
var selectAnotherStoreText = "${selectAnotherStoreText}" ;
var backToWishlist = "${backToWishlist}" ;
var backToRegistry = "${backToRegistry}" ;
var registryErrorMessage="${registryerrormessage}";
var wishlistErrorMessage="${wishlisterrormessage}";
var specialoffer = "${specialoffer}" ;
var akamaiNoImageURL="${akamaiNoImageURL}";
var canonicalImageUrl="${canonicalImageUrl}";
var registrySwapFlag =${registrySwapFlag};
var isPopulateShippingMessage="${populateShippingMessage}";
var isChangeAddToCartButtonState="${changeAddToCartButtonState}"
var usuallyLeavesWarehouse = "${fn:escapeXml(usuallyLeavesWarehouse)}",
labelMinus = "${fn:escapeXml(labelMinus)}",
fullBusinessDays = "${fn:escapeXml(fullBusinessDays)}",
leavesWarehouse = "${fn:escapeXml(leavesWarehouse)}",
labelDetails = "${fn:escapeXml(labelDetails)}",
avilabilityPopover = "${fn:escapeXml(avilabilityPopover)}";
var addressToGetStarted="${addressToGetStarted}";
var milesAway="${milesAway}";
var makeThisMyStore="${makeThisMyStore}";
var makethismystore="${makethismystore}";
var myStore="${myStore}";
var learnMore="${learnMore}";
var detailsDisplay="${detailsDisplay}";
var selectAStore="${selectAStore}";
var phone="${phone}";
var weCouldNotFind="${weCouldNotFind}";
var yourStore="${yourStore}";
var validZipCodeOrCity="${validZipCodeOrCity}";
var unableToCater="${unableToCater}";
var addToCartDisplay="${addToCartDisplay}";
var enableRegistryAJAX="${enableRegistryAJAX}";
var searchText="${searchText}";
var find="${find}";
var storesnearyou="${storesnearyou}";
var all="${all}";
var toysrustab="${toysrustab}";
var babiesrustab="${babiesrustab}";
var getdirections="${getdirections}";
var storeservices="${storeservices}";

</script>	

</dsp:page>