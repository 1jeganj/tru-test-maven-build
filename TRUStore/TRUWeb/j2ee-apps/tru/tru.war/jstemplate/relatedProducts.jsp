<dsp:page>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUItemLevelPromotionDroplet"/>
<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
<dsp:importbean bean="/com/tru/common/TRUIntegrationConfiguration" />
<dsp:importbean bean="/com/tru/commerce/droplet/TRUHidePriceDroplet"/>
<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/com/tru/common/TRUSOSIntegrationConfiguration" />
<dsp:importbean bean="/atg/commerce/pricing/PriceItem" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:getvalueof bean="TRUConfiguration.priceOnCartValue" var="priceOnCartValue" />
<dsp:getvalueof var="locale" vartype="java.lang.String" bean="/atg/userprofiling/Profile.priceList.locale"/>
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof param="productInfo.defaultSKU.relatedProducts" var="relatedProducts"></dsp:getvalueof>
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>
<dsp:getvalueof var="storeEnablePowerReviews" bean="TRUIntegrationConfiguration.enablePowerReviews"/>
<dsp:getvalueof var="sosEnablePowerReviews" bean="TRUSOSIntegrationConfiguration.enablePowerReviews"/>
<dsp:droplet name="GetSiteTypeDroplet">
	<dsp:param name="siteId" value="${site}"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="site" param="site"/>
		<c:set var="site" value ="${site}" scope="request"/>
	</dsp:oparam>
</dsp:droplet>
<c:choose>
	<c:when test="${site eq 'sos'}">
		<c:set var="powerReviewsEnabled" value="${sosEnablePowerReviews}" scope="request" />
	</c:when>
	<c:otherwise>
		<c:set var="powerReviewsEnabled" value="${storeEnablePowerReviews}" scope="request" />
	</c:otherwise>
</c:choose>
<div class="product-details-power-review">
		<c:if test="${powerReviewsEnabled eq true}">
			<dsp:include page="/jstemplate/productReviews.jsp">
			</dsp:include>
		 </c:if>
	</div>

<div class="buy-together-and-save">

<c:if test="${not empty relatedProducts }">
        <div class="large-blue">
		<fmt:message key="tru.productdetail.label.relatedproducts" />
        </div>
        <div class="row row-no-padding" id="related-products">
       	 <!-- START of RELATED PRODUCT SECTION -->
       	 <c:set var="tealiumRPIndex" value="0"/>
       	 <c:set var="tealiumRPSize" value="${relatedProducts.size()}"/>
       	 <dsp:droplet name="/atg/dynamo/droplet/Range">
			<dsp:param name="array" param="productInfo.defaultSKU.relatedProducts" />
			<dsp:param name="howMany" value="4"/>
			<dsp:param name="elementName" value="relatedProductInfo" />
			<dsp:oparam name="outputStart">
			</dsp:oparam>
			<dsp:oparam name="output">
			<dsp:getvalueof param="relatedProductInfo.activeProduct" var="isActiveProduct"></dsp:getvalueof>
			<dsp:getvalueof param="relatedProductInfo.reviewRating" var="reviewRating"></dsp:getvalueof>
			<dsp:getvalueof param="relatedProductInfo.reviewRatingFraction" var="reviewRatingFraction"></dsp:getvalueof>
			<dsp:getvalueof param="relatedProductInfo.suggestedAgeMessage" var="suggestedAgeMessage"></dsp:getvalueof>
			<dsp:getvalueof param="relatedProductInfo.suggestedAgeMin" var="suggestedAgeMin"></dsp:getvalueof>
			<dsp:getvalueof param="relatedProductInfo.suggestedAgeMax" var="suggestedAgeMax"></dsp:getvalueof>
			<dsp:getvalueof param="relatedProductInfo.presellable" var="presellable"></dsp:getvalueof>
			<dsp:getvalueof param="relatedProductInfo.priceDisplay" var="priceDisplay"></dsp:getvalueof>
			<dsp:getvalueof param="relatedProductInfo.uncartable" var="uncartable"></dsp:getvalueof>
			<dsp:getvalueof param="relatedProductInfo.productId" var="relatedproductId"/>
            <dsp:getvalueof param="relatedProductInfo.onlinePid" var="onlinePID"/>
            <dsp:getvalueof param="relatedProductInfo.skuId" var="relatedskuId"/>
			
            <c:if test="${isActiveProduct}">
            <div class="col-md-3 col-no-padding product-block-padding">
                <div class="product-block product-block-unselected no-select">
                    <div class="product-flag"></div>
                    <div class="product-choose">
                        <div class="product-compare-checkbox-container">
                            <input class="product-compare-checkbox" type="checkbox">
                            <label class="product-compare-checkbox-label"><fmt:message key="tru.productdetail.label.compare"/></label>
                        </div>
                    </div>
                    <div class="product-selection">
                        <div class="selection-checkbox-off"></div>
                        <div class="selection-text-off"><!-- Select --><fmt:message key="tru.productdetail.label.select" /></div>
                    </div>
									<dsp:droplet name="/com/tru/utils/TRUPdpURLDroplet">
										<%-- <dsp:param name="productId" value="${relatedproductId}" /> --%>
										<dsp:param name="productId" value="${onlinePID}" />
										<dsp:param name="siteId" bean="/atg/multisite/Site.id" />
										<dsp:oparam name="output">
											<dsp:getvalueof var="productPageUrl" param="productPageUrl" />
										</dsp:oparam>
									</dsp:droplet>
					<dsp:include page="/jstemplate/displayProductWidgetImage.jsp">
					<dsp:param name="productPageUrl" value="${productPageUrl}"/>
                    <dsp:param name="relatedProductInfo" param="relatedProductInfo"/>
                    <dsp:param name="relatedProductImageBlock" param="relatedProductImageBlock"/>
					</dsp:include>
                    <div class="colors">
                        <div class="color-block red color-selected">
                            <div class="color-block-inner">

                            </div>
                        </div>
                        <div class="color-block yellow">
                            <div class="color-block-inner">

                            </div>
                        </div>
                        <div class="color-block orange">
                            <div class="color-block-inner">

                            </div>
                        </div>
                    </div>
                    <div class="wish-list-heart-block">
                        <div class="wish-list-heart-off"></div>
                    </div>
                    <div class="star-rating-block">
                       <fmt:parseNumber var="reviewRatingNumber" type="number" value="${reviewRating}" />
										<div class="star-rating-block">
											<c:forEach begin="1" end="${reviewRatingNumber}">
												<div class="star-rating-on"></div>
											</c:forEach>
											<c:choose>
											<c:when test="${reviewRatingFraction > 0.0 }">
											<div class="star-rating-half"></div>
											<c:forEach begin="1" end="${5-(reviewRatingNumber+1)}">
												<div class="star-rating-off"></div>
											</c:forEach>
											</c:when>
											<c:otherwise>
											<c:forEach begin="1" end="${5-reviewRatingNumber}">
												<div class="star-rating-off"></div>
											</c:forEach>
											</c:otherwise>
											</c:choose>
										</div>
                     </div>
                    <div class="product-block-information related-${relatedskuId}">
                        <div class="product-name productname">
                            <a class="anchor" onclick='javascript:utag.link({"event_type":"xsell_clicked","xsell_product":"${relatedproductId}"});' href="${productPageUrl}">
								<dsp:valueof param="relatedProductInfo.displayName" />
							</a>
                        </div>
                         <c:if test="${not empty suggestedAgeMin && not empty suggestedAgeMax}">
						<div class="age-range">
                            <fmt:message key="tru.productdetail.label.age" />:&nbsp;${suggestedAgeMessage}
                        </div>
                        </c:if>
                        
                        <c:choose>
                        	<c:when test="${priceDisplay eq priceOnCartValue}">
                        	<div class="product-block-price">
                        		<dsp:droplet name="TRUHidePriceDroplet">
                        			<dsp:param name="skuId" value="${relatedskuId}"/><%--Need to pass dynamic values --%>
                        			<dsp:param name="productId" value="${relatedproductId}"/><%--Need to pass dynamic values --%>
                        			<dsp:param name="order" bean="ShoppingCart.current"/>
                        			<dsp:oparam name="true">
	                        				<dsp:include page="/jstemplate/relatedProductPrice.jsp">
												<%-- need to pass dynamic skuId and productId --%>
												<dsp:param name="skuId" value="${relatedskuId}" />
												<dsp:param name="productId" value="${relatedproductId}" />
												<dsp:param name="presellable" value="${presellable}" />
											</dsp:include>
                        			</dsp:oparam>
                        			<dsp:oparam name="false">
                        			<div class="prices priceOnCart">
                        				<c:choose>
											<c:when test="${not empty unCartable && unCartable eq true }">
												<fmt:message key="tru.pricing.label.priceIsNotAvailable" />
											</c:when>
											<c:otherwise>
												<fmt:message key="tru.pricing.label.priceTooLowToDisplay" />
											</c:otherwise>
										</c:choose>
                        			</div>
                        				<div class="member-price" tabindex="0"></div>
                        			</dsp:oparam>
                        		</dsp:droplet>
                        		</div>
                        	</c:when>
                        	<c:otherwise>
	                        		<dsp:include page="/jstemplate/relatedProductPrice.jsp">
										<%-- need to pass dynamic skuId and productId --%>
										<dsp:param name="skuId" value="${relatedskuId}" />
										<dsp:param name="productId" value="${relatedproductId}" />
										<dsp:param name="presellable" value="${presellable}" />
									</dsp:include>
                        	</c:otherwise>
                        </c:choose>																	
                    </div>
					
				<dsp:droplet name="/atg/commerce/catalog/SKULookup">
				<dsp:param name="id" value="${relatedskuId}"/>
				<dsp:param name="elementName" value="SKUItem"/>
				<dsp:oparam name="output">
				<dsp:getvalueof param="SKUItem" var="defaultSkuItem"></dsp:getvalueof>
					<dsp:getvalueof var="adjustments" value="${defaultSkuItem.skuQualifiedForPromos}"/>
				</dsp:oparam>						
				</dsp:droplet>
		
				<dsp:droplet name="TRUItemLevelPromotionDroplet">
				<dsp:param name="adjustments" value="${adjustments}"/>
				<dsp:oparam name="output">
					<dsp:getvalueof var="rankOnePromotion" param="rankOnePromotion"/>
					<dsp:getvalueof var="promotions" param="promotions"/>
					<dsp:getvalueof var="promotionCount" param="promotionCount"/>
				</dsp:oparam>
				</dsp:droplet>
					
					<c:if test="${not empty rankOnePromotion}">
                    <div class="product-block-incentive">
                        <dsp:valueof value="${rankOnePromotion.displayName}"/>
                    </div>
					</c:if>
                    
                    <div class="product-block-border"></div>
                </div>
            </div>
            </c:if>
            </dsp:oparam>
            <dsp:oparam name="outputEnd">
			</dsp:oparam>
            </dsp:droplet>
            	 <!-- START of RELATED PRODUCT SECTION -->
            
        </div>
       </c:if>
    </div>

   </dsp:page>	