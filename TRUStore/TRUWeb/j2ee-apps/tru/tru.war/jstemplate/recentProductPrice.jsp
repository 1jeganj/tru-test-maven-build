<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUPriceDroplet" />
 
	<dsp:droplet name="TRUPriceDroplet">
		<dsp:param name="skuId" param="skuId" />
		<dsp:param name="productId" param="productId" />
		 
		<dsp:oparam name="true">
			<div class="product-block-price">
				 <span class="product-block-original-price"><fmt:message key="pricing.dollar" /> <dsp:valueof param="listPrice" /></span> 
				 <span class="product-block-sale-price"><fmt:message key="pricing.dollar" /><dsp:valueof param="salePrice" /></span>
				 
			</div>
		</dsp:oparam>
		<dsp:oparam name="false">
			<div class="prices">
				<dsp:getvalueof var="salePrice" param="salePrice" />
				<c:choose>
					<c:when test="${salePrice == 0}">
						<span class="product-block-original-price"> <fmt:message key="pricing.dollar" /><dsp:valueof param="listPrice" /></span>
					</c:when>
					<c:otherwise>
						<span class="product-block-original-price"> <fmt:message key="pricing.dollar" /><dsp:valueof param="salePrice" /></span>
					</c:otherwise>
				</c:choose>
			</div>
		</dsp:oparam>
		<dsp:oparam name="empty">
		</dsp:oparam>
		
	</dsp:droplet>
</dsp:page>