<dsp:page>

<dsp:importbean bean="/com/tru/commerce/locations/TRUStoreAvailabilityDroplet"/>

<dsp:droplet name="TRUStoreAvailabilityDroplet">
    <%--locationId we are getting from the Cookie in the TRUStoreAvailabilityDroplet  --%>
	<dsp:param name="skuId" param="skuId" />
	<dsp:param name="locationId" param="locationId" />
	<dsp:param name="isReqFromAjax" value="true" />
	<dsp:oparam name="available">
		<dsp:getvalueof var="storeAvailMsg" param="storeAvailMsg" />
		<dsp:getvalueof var="favStore" param="favStore" />
		<div class="green-location-icon inline"></div>
		<span class="find-in-store-message">${storeAvailMsg}</span>
	</dsp:oparam>
	<dsp:oparam name="backorderable">
		<dsp:getvalueof var="storeAvailMsg" param="storeAvailMsg" />
		<dsp:getvalueof var="favStore" param="favStore" />
		<div class="green-location-icon inline no-store-pickup"></div>
		<span class="find-in-store-message">${storeAvailMsg}</span>
	</dsp:oparam>
	<dsp:oparam name="preorderable">
		<dsp:getvalueof var="storeAvailMsg" param="storeAvailMsg" />
		<dsp:getvalueof var="favStore" param="favStore" />
		<div class="green-location-icon inline no-store-pickup"></div>
		<span class="find-in-store-message">${storeAvailMsg}</span>
	</dsp:oparam>
	<dsp:oparam name="unavailable">
		<dsp:getvalueof var="storeAvailMsg" param="storeAvailMsg" />
		<dsp:getvalueof var="favStore" param="favStore" />
		<div class="green-location-icon inline no-store-pickup"></div>
		<span class="find-in-store-message">${storeAvailMsg}</span>
	</dsp:oparam>
	<dsp:oparam name="error">
		<dsp:getvalueof var="storeAvailMsg" param="storeAvailMsg" />
		<dsp:getvalueof var="favStore" param="favStore" />
		<div class="green-location-icon inline no-store-pickup"></div>
		<span class="find-in-store-message">${storeAvailMsg}</span>
	</dsp:oparam>
	<dsp:oparam name="default">
		<dsp:getvalueof var="storeAvailMsg" param="storeAvailMsg" />
		<dsp:getvalueof var="favStore" param="favStore" />
		<div class="green-location-icon inline no-store-pickup"></div>
		<span class="find-in-store-message">${storeAvailMsg}</span>
	</dsp:oparam>
</dsp:droplet>
</dsp:page>