<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:getvalueof var="transient" bean="Profile.transient" />
	<dsp:getvalueof param="compareProduct" var="compareProduct"></dsp:getvalueof>
	<dsp:getvalueof param="registryWarningIndicator" var="registryWarningIndicator"/>
	<dsp:getvalueof param="skuRegistryEligibility" var="skuRegistryEligibility"/>
	<dsp:getvalueof param="selectedItemsCounterToRegistry" var="selectedItemsCounterToRegistry" />
	<dsp:getvalueof param="qtyVal" var="qtyVal"/>
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
	<dsp:getvalueof var="pdpRegistryUrl" bean="TRUStoreConfiguration.pdpRegistryUrl" />
	<dsp:getvalueof var="userStatus" bean="TRUStoreConfiguration.userStatus" />
	<dsp:getvalueof var="pdpWishlistUrl" bean="TRUStoreConfiguration.pdpWishlistUrl" />
	<dsp:getvalueof var="registryWishlistkeyMap" bean="TRUStoreConfiguration.registryWishlistkeyMap" />
	<c:forEach var="entry" items="${registryWishlistkeyMap}">
		<c:if test="${entry.key eq 'registryType'}">
			<c:set var="registryType" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'defaultColor'}">
			<c:set var="defaultColor" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'defaultSize'}">
			<c:set var="defaultSize" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'defaultSknOrigin'}">
			<c:set var="defaultSknOrigin" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'nonSpecificIndicator'}">
			<c:set var="nonSpecificIndicator" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'itemPurchased'}">
			<c:set var="itemPurchased" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'purchaseUpdateIndicator'}">
			<c:set var="purchaseUpdateIndicator" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'source'}">
			<c:set var="source" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'messageId'}">
			<c:set var="messageId" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'country'}">
			<c:set var="country" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'language'}">
			<c:set var="language" value="${entry.value}" />
		</c:if>
	</c:forEach>
	<c:choose>
		<c:when test="${selectedItemsCounterToRegistry eq 0}">
			<div class="inline">
				<a href="javascript:void(0);" 
				onclick="checkAddToRegistryAvailability(this)" 
				class="small-orange desableLinkW3c"  
				data-toggle="popover"
				data-placement="bottom"
				data-content='<fmt:message key="tru.productdetail.label.collectionDenyWishListMessage"/>'>
					<img src="${TRUImagePath}images/wish-list.png" alt="wish list" class="add-to-image"><fmt:message key="tru.productdetail.label.collectionAddtoWishList"/></a>
			</div>
			<div class="inline">
				<a href="javascript:void(0);" 
				onclick="checkAddToRegistryAvailability(this)" 
				class="small-orange desableLinkW3c"
				data-toggle="popover"
				data-placement="bottom"
				data-content='<fmt:message key="tru.productdetail.label.collectionDenyRegistryMessage"/>'>
					<img src="${TRUImagePath}images/baby-registry.png" alt="baby registry" class="add-to-image"><fmt:message key="tru.productdetail.label.collectionAddtoRegistry"/></a>
			</div>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${not empty registryWarningIndicator && registryWarningIndicator eq 'ALLOW' && skuRegistryEligibility eq true }">
					<div class="inline">
						<c:choose>
							<c:when test="${userStatus  eq false}">
								<a href="javascript:void(0);" data-href="${pdpWishlistUrl}" onclick="checkAddToRegistryAvailability(this)" class="small-orange checkWishlist"> 
								<img src="${TRUImagePath}images/wish-list.png" alt="wish list" class="add-to-image"><fmt:message key="tru.productdetail.label.collectionAddtoWishList"/></a>
							</c:when>
						</c:choose>
					</div>
					<div class="inline">
						<c:choose>
							<c:when test="${userStatus  eq false}">
								<a href="javascript:void(0);" data-href="${pdpRegistryUrl}" onclick="checkAddToRegistryAvailability(this)" class="small-orange" > 
								<img src="${TRUImagePath}images/baby-registry.png" alt="baby registry" class="add-to-image"><fmt:message key="tru.productdetail.label.collectionAddtoRegistry"/></a>
							</c:when>
						</c:choose>
					</div>
				</c:when>
				<c:when test="${registryWarningIndicator eq 'WARN' && skuRegistryEligibility eq true}">
					<div class="inline">
						<c:choose>
							<c:when test="${userStatus  eq false}">
								<a href="javascript:void(0);" data-href="${pdpWishlistUrl}"
									onclick="checkAddToRegistryAvailability(this)"
									class="small-orange checkWishlist" data-toggle="popover"
									data-placement="bottom"
									data-content='<fmt:message key="tru.productdetail.label.collectionWarnWishListMessage"/>'>
									<img src="${TRUImagePath}images/wish-list.png" alt="wish list"
									class="add-to-image"><fmt:message key="tru.productdetail.label.collectionAddtoWishList"/>
								</a>
							</c:when>
						</c:choose>
					</div>
					<div class="inline">
						<c:choose>
							<c:when test="${userStatus  eq false}">
								<a href="javascript:void(0);" data-href="${pdpRegistryUrl}"
									onclick="checkAddToRegistryAvailability(this)"
									class="small-orange" data-toggle="popover"
									data-placement="bottom"
									data-content='<fmt:message key="tru.productdetail.label.collectionWarnRegistryMessage"/>'>
									<img src="${TRUImagePath}images/baby-registry.png"
									alt="baby registry" class="add-to-image"><fmt:message key="tru.productdetail.label.collectionAddtoRegistry"/>
								</a>
							</c:when>
						</c:choose>
					</div>
				</c:when>
				<c:when test="${registryWarningIndicator eq 'DENY' || skuRegistryEligibility eq false}">
					<div class="inline">
						<a href="javascript:void(0);" data-href="${pdpWishlistUrl}"
							onclick="checkAddToRegistryAvailability(this)"
							class="small-orange checkWishlist" data-toggle="popover"
							data-placement="bottom"
							data-content='<fmt:message key="tru.productdetail.label.collectionDenyWishListMessage"/>'>
							<img src="${TRUImagePath}images/wish-list.png" alt="wish list"
							class="add-to-image"><fmt:message key="tru.productdetail.label.collectionAddtoWishList"/>
						</a>
					</div>
					<div class="inline">
						<a href="javascript:void(0);" data-href="${pdpRegistryUrl}"
							onclick="checkAddToRegistryAvailability(this)"
							class="small-orange" data-toggle="popover"
							data-placement="bottom"
							data-content='<fmt:message key="tru.productdetail.label.collectionDenyRegistryMessage"/>' >
							<img src="${TRUImagePath}images/baby-registry.png"
							alt="baby registry" class="add-to-image"><fmt:message key="tru.productdetail.label.collectionAddtoRegistry"/>
						</a>
					</div>
				</c:when>
				<c:otherwise>
					<div class="inline">
						<a href="javascript:void(0);" data-href="${pdpWishlistUrl}" onclick="checkAddToRegistryAvailability(this)" class="small-orange checkWishlist"> 
						<img src="${TRUImagePath}images/wish-list.png" alt="wish list" class="add-to-image"><fmt:message key="tru.productdetail.label.collectionAddtoWishList"/></a>
					</div>
					<div class="inline">
						<a href="javascript:void(0);" data-href="${pdpRegistryUrl}" onclick="checkAddToRegistryAvailability(this)" class="small-orange">
						 <img src="${TRUImagePath}images/baby-registry.png" alt="baby registry" class="add-to-image"><fmt:message key="tru.productdetail.label.collectionAddtoRegistry"/></a>
					</div>
				</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>
	<div class="modal fade pdpModals hide" id="add-to-registry" tabindex="-1"
		role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog modal-md">
			<div class="modal-content sharp-border"></div>
		</div>
	</div>
</dsp:page>