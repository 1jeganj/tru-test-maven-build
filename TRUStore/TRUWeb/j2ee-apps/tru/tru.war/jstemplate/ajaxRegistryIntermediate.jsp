<dsp:page>
<dsp:getvalueof param="isWishlist" var="isWishlist"></dsp:getvalueof>
<dsp:getvalueof param="showWishList" var="showWishList"></dsp:getvalueof>
<dsp:getvalueof param="lastAccessedID" var="lastAccessedID"></dsp:getvalueof>
<dsp:getvalueof param="registryNumber" var="registryNumber"></dsp:getvalueof>
<dsp:getvalueof param="productDetails" var="productDetails"></dsp:getvalueof>
<dsp:getvalueof param="requestType" var="requestType"></dsp:getvalueof>
<dsp:getvalueof param="pageName" var="pageName"></dsp:getvalueof>
<c:choose>
<c:when test="${showWishList ne 'true' }">
<dsp:droplet name="/com/tru/commerce/droplet/TRUPDPRegistryInfoDroplet">
<dsp:param name="isWishlist" value="${isWishlist}"/>
<dsp:param name="lastAccessedID" value="${lastAccessedID}"/>
<dsp:param name="registryNumber" value="${registryNumber}"/>
<dsp:param name="productDetails" value="${productDetails}"/>
<dsp:param name="requestType" value="${requestType}"/>
<dsp:param name="pageName" value="${pageName}"/>
<dsp:oparam name="output">
<dsp:getvalueof param="registryResponse" var="registryResponse"></dsp:getvalueof>
${registryResponse}
</dsp:oparam>
</dsp:droplet>
</c:when>
<c:when test="${showWishList eq 'true' }">
<dsp:droplet name="/com/tru/commerce/droplet/TRUPDPWishlistInfoDroplet">
<dsp:param name="showWishList" value="${showWishList}"/>
<dsp:param name="registryNumber" value="${registryNumber}"/>
<dsp:param name="pageName" value="${pageName}"/>
<dsp:oparam name="output">
<dsp:getvalueof param="registryResponse" var="registryResponse"></dsp:getvalueof>
${registryResponse}
</dsp:oparam>
</dsp:droplet>
</c:when>
</c:choose>
</dsp:page>