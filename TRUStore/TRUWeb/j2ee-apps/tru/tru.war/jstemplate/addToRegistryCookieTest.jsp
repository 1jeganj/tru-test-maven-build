<dsp:page>
<html lang="en">
<head></head>
<body>
<dsp:importbean bean="/com/tru/droplet/TRUAddRegistryCookieDroplet"/>
<dsp:importbean bean="/com/tru/droplet/TRUAddWishlistCookieDroplet"/>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="pdpRegistryUrl" bean="TRUStoreConfiguration.pdpRegistryUrl" />
<dsp:getvalueof var="cvalue" param="cvalue" />
<dsp:getvalueof var="pagename" param="pagename" />
<dsp:getvalueof var="iswishlist" param="iswishlist" />
<dsp:getvalueof var="timeout" param="timeout" />
<c:choose>
	<c:when test="${iswishlist ne 'true' }">
		<dsp:droplet name="TRUAddRegistryCookieDroplet">
		<dsp:param name="addToRegistryCookie" value="${cvalue}"/>
		<dsp:param name="pagename" value="${pagename}"/>
		<dsp:oparam name="output">
		</dsp:oparam>
		</dsp:droplet>
		<script type="text/javascript">
		setTimeout(function(){
			window.location.assign("${pdpRegistryUrl}");
		},${timeout})
		</script>	
		pdpRegistryUrl ::: ${pdpRegistryUrl}
	</c:when>
	<c:when test="${iswishlist eq 'true' }">
		<dsp:droplet name="TRUAddWishlistCookieDroplet">
		<dsp:param name="addToWishListCookie" value="${cvalue}"/>
		<dsp:param name="pagename" value="${pagename}"/>
		<dsp:oparam name="output">
		</dsp:oparam>
		</dsp:droplet>
		<script type="text/javascript">
		setTimeout(function(){
			window.location.assign("${pdpRegistryUrl}");
		},${timeout})
		</script>	
		pdpRegistryUrl ::: ${pdpRegistryUrl}
	</c:when>
</c:choose>
</body>
</html>
</dsp:page>
