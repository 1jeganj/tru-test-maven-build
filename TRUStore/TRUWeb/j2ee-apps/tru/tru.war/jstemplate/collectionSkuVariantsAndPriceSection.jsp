<dsp:page>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/com/tru/commerce/droplet/TRUHidePriceDroplet"/>
<dsp:importbean bean="/atg/commerce/pricing/PriceItem" />
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:getvalueof bean="TRUConfiguration.priceOnCartValue" var="priceOnCartValue" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:getvalueof param="productInfo" var="productInfo" />
<dsp:getvalueof param="productInfo.defaultSKU.id" var="skuId"/>
<dsp:getvalueof var="priceDisplay" param="productInfo.defaultSKU.priceDisplay"/>
	<c:choose>
		<c:when test="${priceDisplay eq priceOnCartValue}">
			<dsp:droplet name="TRUHidePriceDroplet">
				<dsp:param name="skuId" value="${skuId}" />
				<dsp:param name="productId" value="${productInfo.productId}" />
				<dsp:param name="order" bean="ShoppingCart.current" />
				<dsp:oparam name="true">
					<dsp:include page="/pricing/displayPrice.jsp">
						<dsp:param name="skuId" value="${skuId}" />
						<dsp:param name="productId" value="${productInfo.productId}" />
						<dsp:param name="pageName" value="collectionPage"/>
					</dsp:include>
				</dsp:oparam>
				<dsp:oparam name="false">
				<div class="prices">
                       <fmt:message key="tru.pricing.label.priceTooLowToDisplay" />
                       </div>
                       <div class="member-price" tabindex="0"></div>
                </dsp:oparam>
			</dsp:droplet>
		</c:when>
		<c:otherwise>
			<dsp:include page="/pricing/displayPrice.jsp">
				<dsp:param name="skuId" value="${skuId}" />
				<dsp:param name="productId" value="${productInfo.productId}" />
				<dsp:param name="collectionPage" value="collectionPage"/>
			</dsp:include>
		</c:otherwise>
	</c:choose>

</dsp:page>

