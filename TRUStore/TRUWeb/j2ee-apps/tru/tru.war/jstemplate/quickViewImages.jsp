<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:getvalueof param="productInfo" var="productInfo" />
<dsp:getvalueof param="productInfo.defaultSKU.primaryImage" var="primaryImage" />
<dsp:getvalueof param="productInfo.defaultSKU.secondaryImage" var="secondaryImage" />
<dsp:getvalueof param="productInfo.defaultSKU.alterNateImages" var="alterNateImages" />	
<dsp:getvalueof param="productInfo.productId" var="relatedproductId" />
<dsp:getvalueof param="productInfo.defaultSKU.displayName" var="displayName" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof param="quickViewThumbnailBlock" var="quickViewThumbnailBlock" />
<dsp:getvalueof param="quickViewSecImageBlock" var="quickViewSecImageBlock" />
<dsp:getvalueof param="quickViewAlt1ImageBlock" var="quickViewAlt1ImageBlock" />
<dsp:getvalueof param="quickViewAlt2ImageBlock" var="quickViewAlt2ImageBlock" />
<dsp:getvalueof param="productInfo.defaultSKU.onlinePID" var="onlinePID" />
<c:choose>
	<c:when test="${not empty pdpH1}">
        <c:set var="altH1SkuName" value="${pdpH1}"/>  
	</c:when>
	<c:otherwise>
		<c:set var="altH1SkuName" value="${displayName}"/>
	</c:otherwise>
</c:choose>  
<dsp:droplet name="/com/tru/utils/TRUPdpURLDroplet">
		<%-- <dsp:param name="productId" value="${relatedproductId}" /> --%>
		<dsp:param name="productId" value="${onlinePID}" />
		<dsp:param name="siteId" bean="/atg/multisite/Site.id" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="productPageUrl" param="productPageUrl" />
		</dsp:oparam>
	</dsp:droplet>
<dsp:getvalueof var="productPageUrl" param="productPageUrl" />

                  <div class="thumb-container">
                  <!-- commenting out this code as Intregation is not done with invado -->
					 <%-- <div data-toggle="modal" data-target="#productVideoModal" class="video-container inline">
                        <img class="uploadImage" src="${primaryImage}">
                        <div class="play"></div>
                    </div> --%>
                    <c:choose>
                     <c:when test="${not empty primaryImage}">
                    
                    <a class="block-add" href="javascript:void(0)" title="product image thumbnail">
                        <img class="uploadImage selectedImage firstAlternate" src="${primaryImage}${quickViewThumbnailBlock}" alt="${altH1SkuName}" />
                    </a>
                 
                    </c:when>
                    <c:when test="${empty primaryImage && not empty alterNateImages}">
                    
                    	<c:choose>
							<c:when test="${not empty akamaiNoImageURL}">
							  	<img class="uploadImage selectedImage firstAlternate" src="${akamaiNoImageURL}" alt="${altH1SkuName}" />
							</c:when>
							 <c:otherwise>
								<img class="uploadImage selectedImage firstAlternate" src="${TRUImagePath}images/no-image500.gif"  alt="${altH1SkuName}" />
							</c:otherwise>
					 	</c:choose> 
					 		
                    </c:when>
                    </c:choose>
                     <c:if test="${not empty secondaryImage}">
                     	
	                    <a class="block-add" href="javascript:void(0)" title="secondary image thumbnail">
	                        <img class="uploadImage secondAlternate" src="${secondaryImage}${quickViewSecImageBlock}" alt="${altH1SkuName}" />
	                    </a>
	                   
                    </c:if>
                    <span class="alternatePDPImg">
                    <c:choose>
	                    <c:when test="${empty secondaryImage && not empty alterNateImages}">
		                    <c:forEach begin="0" end="1" items="${alterNateImages}" var="alterNateImage">
		                    	
			                    <a class="block-add" href="javascript:void(0)" title="alternate image thumbnail">
			                        <img class="uploadImage" src="${alterNateImage}${quickViewAlt1ImageBlock}" alt="${altH1SkuName}"  />
			                    </a>
			                    
		                    </c:forEach>
		                </c:when>
                    <%-- <a class="block-add" href="#">
                        <img class="uploadImage" src="${TRUImagePath}images/product-3.jpg">
                    </a> --%>
                   
	                    <c:otherwise>
		                	<c:if test="${not empty alterNateImages}">
			                     <c:forEach begin="0" end="0" items="${alterNateImages}" var="alterNateImage">
			                     	
				                    <a class="block-add" href="javascript:void(0)" title="alternate image thumbnail">
				                        <img class="uploadImage" src="${alterNateImage}${quickViewAlt1ImageBlock}" alt="${altH1SkuName}"  />
				                    </a>
				                    
			                    </c:forEach>
			                 </c:if>
		                </c:otherwise>
	                </c:choose>
	                    
                    </span>
                    <div class="see-more-images">
                        <a href="${productPageUrl}" data-toggle="" data-target=""><fmt:message key="tru.productdetail.label.viewDetailLink" /></a>
                    </div>
                </div>
</dsp:page>
    
