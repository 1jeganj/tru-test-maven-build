<dsp:page>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
 <dsp:getvalueof var="registryWishlistkeyMap" bean="TRUStoreConfiguration.registryWishlistkeyMap" />
 <dsp:getvalueof  param="productInfo.rusItemNumber" var="skn"/>
<dsp:getvalueof  param="productInfo.defaultSKU.upcNumbers" var="upcNumbers"/> 
<dsp:getvalueof param="productInfo.defaultSKU.colorCode" var="color" />
<dsp:getvalueof var="size" param="productInfo.defaultSKU.juvenileSize"/>
 <dsp:getvalueof param="productInfo.defaultSKU.displayName" var="skuDisplayName" />
<dsp:getvalueof param="productInfo.defaultSKU.id" var="uid" />
 <dsp:getvalueof param="productInfo.productId" var="productId" />
 <dsp:getvalueof param="productInfo.defaultSKU.rmsColorCode" var="rmsColorCode" />
<dsp:getvalueof param="productInfo.defaultSKU.rmsSizeCode" var="rmsSizeCode" />
<dsp:getvalueof param="productInfo.defaultSKU.sknOrigin" var="sknOrigin" />
<dsp:getvalueof  param="productInfo.defaultSKU.originalproductIdOrSKN" var="skn"/>
 <c:forEach var="entry" items="${registryWishlistkeyMap}">
		<c:if test="${entry.key eq 'registryType'}">
			<c:set var="registryType" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'defaultColor'}">
			<c:set var="color" value="${entry.value}" />
			<input type="hidden" class="rgs_defaultColor" value="${color}">
		</c:if>
		<c:if test="${entry.key eq 'defaultSize'}">
			<c:set var="size" value="${entry.value}" />
			<input type="hidden" class="rgs_defaultSize" value="${color}">
		</c:if>
		<%-- <c:if test="${entry.key eq 'defaultSknOrigin'}">
			<c:set var="sknOrigin" value="${entry.value}" />
		</c:if> --%>
		<c:if test="${entry.key eq 'nonSpecificIndicator'}">
			<c:set var="nonSpecificIndicator" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'itemPurchased'}">
			<c:set var="itemPurchased" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'purchaseUpdateIndicator'}">
			<c:set var="purchaseUpdateIndicator" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'source'}">
			<c:set var="source" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'messageId'}">
			<c:set var="messageId" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'country'}">
			<c:set var="country" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'language'}">
			<c:set var="language" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'deletedFlag'}">
			<c:set var="deletedFlag" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'wishlistRegistryType'}">
			<c:set var="wishlistRegistryType" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'timeout'}">
			<c:set var="timeout" value="${entry.value}" />
		</c:if>
	</c:forEach>
	<c:if test="${not empty upcNumbers}">
		<c:forEach items="${upcNumbers}" var="upcNumber" end="0">
		  <c:set var="upc" value="${upcNumber}" />
		</c:forEach>
	</c:if>
	<c:if test="${not empty rmsColorCode}">
		<c:set var="color" value="${rmsColorCode}"></c:set>
	</c:if>
	<c:if test="${not empty rmsSizeCode}">
		<c:set var="size" value="${rmsSizeCode}"></c:set>
	</c:if>
<input type="hidden" class="collection_productDetails-${productId}" value="${productId}|${skn}|${upc}|${uid}|${color}|${size}|${sknOrigin}">
</dsp:page>