<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof param="relatedProductInfo" var="relatedProductInfo"/>
<dsp:getvalueof param="relatedProductInfo.productId" var="relatedproductId"/>
<dsp:getvalueof param="relatedProductInfo.primaryImage" var="relatedProductImage" />
<dsp:getvalueof param="relatedProductInfo.onlinePid" var="onlinePid" />
<dsp:getvalueof param="relatedProductImageBlock" var="relatedProductImageBlock" />
<dsp:getvalueof param="relatedProductInfo.displayName" var="displayName" />
<dsp:getvalueof param="productPageUrl" var="productPageUrl" />
	<c:choose>
			<c:when test="${not empty pdpH1}">
                     <c:set var="altH1SkuName" value="${pdpH1}"/>  
			</c:when>
			<c:otherwise>
					<c:set var="altH1SkuName" value="${displayName}"/>
			</c:otherwise>
	</c:choose>
   <div class="product-block-image-container">
                        <div class="product-block-image">
                            <div class="product-image">
                                    <c:choose>
                    				<c:when test="${not empty relatedProductImage}">
                    				<a  class="bnsImgBlock" href="${productPageUrl}" >
                    				<img class="product-block-img" src="${relatedProductImage}${relatedProductImageBlock}" alt="${altH1SkuName}">
                    				</a>
                    				</c:when>
                    				<c:otherwise>                    				
                    					<c:choose>
										 <c:when test="${not empty akamaiNoImageURL}">
										  	<a  class="bnsImgBlock" href="${productPageUrl}" ><img class="product-block-img" src="${akamaiNoImageURL}" alt="${altH1SkuName}"></a>
										 </c:when>
										 <c:otherwise>
										 	  <a  class="bnsImgBlock" href="${productPageUrl}" ><img class="product-block-img" src="${TRUImagePath}images/no-image500.gif" alt="${altH1SkuName}"></a>
										 </c:otherwise>
									    </c:choose>
                    				</c:otherwise>
                    				</c:choose>
                              <div>
					<%-- <div class="quick-view-container">
					<div  class="product-hover-view product-quick-view" data-toggle="modal" data-target="#quickviewModal" onclick="loadQuickView('${relatedproductId}|${onlinePid}','pdp')"><!-- quick view --><fmt:message key="tru.productdetail.label.quickview"/></div> 
					</div> --%>
				</div>
               </div>
              </div>
            </div>
</dsp:page>