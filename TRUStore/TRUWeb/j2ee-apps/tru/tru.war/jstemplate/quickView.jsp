<dsp:page>
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof var="relatedproductId" param="relatedproductId" />
<dsp:getvalueof var="quickViewPageType" param="quickViewPageType" />
<dsp:getvalueof var="onlinePid" param="onlinePid" />

<dsp:getvalueof var="familyName" param="familyName" />
<dsp:getvalueof param="productInfo" var="productInfo" />
<dsp:getvalueof param="productInfo.productId" var="productId"/>
<dsp:getvalueof param="productInfo.displayName" var="productName"/>
<dsp:getvalueof param="productInfo.defaultSKU.brandName" var="productBrand"/>
<dsp:getvalueof param="productInfo.defaultSKU.primaryImage" var="productImageURL"/>
<dsp:getvalueof param="productInfo.defaultSKU.id" var="productSku"/>
<dsp:getvalueof param="productInfo.defaultSKU.collectionNames" var="collectionNames"/>
<dsp:getvalueof var="locationIdFromCookie" param="locationIdFromCookie" />
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration"/>
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUPriceDroplet" />

<dsp:getvalueof var="site" bean="/atg/multisite/Site.id"/>

 <dsp:getvalueof var="loginStatus" bean="Profile.transient" />
 <dsp:getvalueof var="customerEmail" bean="Profile.login"/>
 <dsp:getvalueof var="customerId" bean="Profile.id"/>
 <dsp:getvalueof var="customerDob" bean="Profile.dateOfBirth"/> 
 
 <dsp:getvalueof var="pageName" bean="TRUTealiumConfiguration.quickviewPageName"/>
 <dsp:getvalueof var="pageType" bean="TRUTealiumConfiguration.quickviewPageType"/>
 <dsp:getvalueof var="productCategory" bean="TRUTealiumConfiguration.quickviewProductCategory"/>

 
 <dsp:getvalueof var="productMerchandisingCategory" bean="TRUTealiumConfiguration.quickviewProductMerchandisingCategory"/>
 <dsp:getvalueof var="registryOrigin" bean="TRUTealiumConfiguration.quickviewRegistryOrigin"/>
 <dsp:getvalueof var="strikedpricepage" bean="TRUTealiumConfiguration.quickviewStrikedPricePage"/> 
 <dsp:getvalueof var="strikedPricePageTitle" bean="TRUTealiumConfiguration.quickviewStrikedPricePageTitle"/>
 	
 <dsp:getvalueof var="browserId" bean="TRUTealiumConfiguration.browserId"/>

 <dsp:getvalueof var="deviceType" bean="TRUTealiumConfiguration.deviceType"/>
 <dsp:getvalueof var="customerType" bean="TRUTealiumConfiguration.customerType"/> 
 
 <dsp:getvalueof var="productSubcategory" bean="TRUTealiumConfiguration.quickviewProductSubcategory"/>
 <dsp:getvalueof var="productFindingMethod" bean="TRUTealiumConfiguration.quickviewProductFindingMethod"/>
  
 <dsp:getvalueof var="internalCampaignPage" bean="TRUTealiumConfiguration.quickviewInternalCampaignPage"/>
 <dsp:getvalueof var="outOfStockBySku" bean="TRUTealiumConfiguration.quickviewOutOfStockBySku"/>
 <dsp:getvalueof var="orsoCode" bean="TRUTealiumConfiguration.orsoCode"/>
 <dsp:getvalueof var="ispuSource" bean="TRUTealiumConfiguration.ispuSource"/>
 
 <dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/> 
 
 <dsp:droplet name="TRUPriceDroplet">
		<dsp:param name="skuId" value="${productSku}" />
		<dsp:param name="productId" value="${productId}" />
		<dsp:oparam name="true">
			<dsp:getvalueof param="salePrice" var="productUnitPrice" />
			<dsp:getvalueof param="listPrice" var="productListPrice" />
			<dsp:getvalueof param="savedAmount" var="productDiscount" /> 
		</dsp:oparam>
</dsp:droplet>

 <c:choose>
	<c:when test="${loginStatus eq 'true'}">
		<c:set var="customerStatus" value="Guest"/>
	</c:when>
	<c:otherwise>
		<c:set var="customerStatus" value="loggedIn"/>
		<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
		<dsp:droplet name="IsEmpty">
			<dsp:param name="value" bean="Profile.shippingAddress.id" />
			<dsp:oparam name="false">
				<dsp:droplet name="ForEach">
					<dsp:param name="array" bean="Profile.secondaryAddresses"/>
					<dsp:param name="elementName" value="address"/>
					<dsp:oparam name="output">
						<dsp:getvalueof param="address.id" var="addressId" />
						<c:if test="${addressId eq shippingAddressId}">						 				
							 <dsp:getvalueof param="address.city" var="customerCity"/> 
							 <dsp:getvalueof param="address.state" var="customerState"/> 
							 <dsp:getvalueof param="address.postalCode" var="customerZip"/>
							 <dsp:getvalueof param="address.country" var="customerCountry"/>							
						 </c:if>
					</dsp:oparam>
			   </dsp:droplet>
			</dsp:oparam>
		</dsp:droplet>
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${not empty collectionNames}">
		<c:set var="productCollection" value="${collectionNames}"/>
	</c:when>
	<c:otherwise>
		  <dsp:getvalueof var="productCollection" bean="TRUTealiumConfiguration.quickviewProductCollection"/>
	</c:otherwise>
</c:choose>
	<!-- Tealium script section start -->
	
	<!-- Tealium script section end -->
<c:if test="${not empty relatedproductId || not empty onlinePid }">
	<dsp:droplet name="/com/tru/commerce/droplet/TRUProductInfoLookupDroplet">
		<dsp:param name="productId" value="${relatedproductId}" />
		<dsp:param name="onlineProductId" value="${onlinePid}" />
		<dsp:oparam name="output">
		<dsp:include page="/jstemplate/quickViewContent.jsp">
			<dsp:param name="productInfo" param="productInfo"/>
			<dsp:param name="productVoJson" param="productVoJson"/>
			<dsp:param name="locationIdFromCookie" param="locationIdFromCookie"/>
			<dsp:param name="familyName" value="${familyName}" />
		</dsp:include>
		</dsp:oparam>
       <dsp:oparam name="empty">
       <div class="modal-lg modal-dialog" id="modal-lg">
            <div class="modal-content sharp-border">
                <a href="javascript:void(0)" class="close-modal" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
				<dsp:include page="/jstemplate/unavailableProductDetailsTemplate.jsp" />
				</div>
				</div>
		</dsp:oparam>
	</dsp:droplet>
</c:if>
</dsp:page>