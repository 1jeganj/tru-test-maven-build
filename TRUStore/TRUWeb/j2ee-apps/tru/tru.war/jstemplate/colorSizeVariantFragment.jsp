<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:getvalueof param="pageIncludedFrom" var="pageIncludedFrom" />
<dsp:getvalueof param="productInfo" var="productInfo" />
<dsp:getvalueof param="productVoJson" var="productVoJson" />
 <dsp:getvalueof  param="productInfo.sizeChart" var="sizeChartName"/>  
<dsp:getvalueof param="productInfo.colorSwatchImageList" var="colorSwatchImageList" />
<dsp:getvalueof param="productInfo.colorCodeList" var="colorCodeList" />
<dsp:getvalueof param="productInfo.colorCodeToSwatchImageMap" var="colorCodeToSwatchImageMap" />
<dsp:getvalueof param="productInfo.juvenileSizesList" var="juvenileSizesList" /> 
<dsp:getvalueof param="productInfo.defaultSKU.primaryImage" var="primaryImage" /> 
<dsp:getvalueof param="productInfo.defaultSKU" var="defaultSKU" />
<dsp:getvalueof param="productInfo.defaultSKU.juvenileSize" var="defaultJuvenileSize" /> 
<dsp:getvalueof param="productInfo.defaultSKU.presellQuantityUnits" var="presellQuantityUnits" /> 
<dsp:getvalueof param="productInfo.defaultSKU.presellable" var="presellable" />
<dsp:getvalueof param="productInfo.defaultSKU.colorCode" var="defaultColorCode" /> 
<dsp:getvalueof param="productInfo.colorSkusMap" var="colorSkusMap" />
<dsp:getvalueof param="productInfo.sizeSkusMap" var="sizeSkusMap" /> 
<dsp:getvalueof param="productInfo.childSKUsList" var="childSKUsList" />
<dsp:getvalueof param="productInfo.inStockSKUsList" var="activeSKUsList" />
<dsp:getvalueof param="productInfo.outofStockSKUsList" var="inActiveSKUsList" />
<dsp:getvalueof param="productInfo.colorSizeVariantsAvailableStatus" var="colorSizeVariantsAvailableStatus" />
<dsp:getvalueof param="productInfo.selectedSizeSKUsList" var="selectedSizeSKUsList" />
<dsp:getvalueof param="productInfo.selectedColorSKUsList" var="selectedColorSKUsList" />
<dsp:getvalueof var="selectedColor" param="selectedColorId" />
<dsp:getvalueof var="selectedSize" param="selectedSize" />
<dsp:getvalueof var="swatchImageBlock" param="swatchImageBlock" />
<c:if test="${not empty presellQuantityUnits}">
<fmt:parseNumber var="presellQuantityUnits1" type="number" value="${presellQuantityUnits}" />
</c:if>
<input type="hidden"  class='productInfoJson-${productInfo.productId}' value='${productVoJson}' />
<input type="hidden"  class='defaultSkuId-${productInfo.productId}' value='${defaultSKU.id}' />
<c:if test="${not empty colorCodeList }">
<dsp:getvalueof var="colorLength" value="${fn:length(colorCodeList)}"></dsp:getvalueof>
</c:if>
<c:if test="${not empty juvenileSizesList}">
<dsp:getvalueof var="juvenileSizesListLength" value="${fn:length(juvenileSizesList)}"></dsp:getvalueof>
</c:if>
<c:if test="${not empty activeSKUsList}">
<dsp:getvalueof var="activeSKUsListLength" value="${fn:length(activeSKUsList)}"></dsp:getvalueof>
</c:if>
<c:if test="${not empty childSKUsList}">
	<dsp:getvalueof var="childSKUsListLength" value="${fn:length(childSKUsList)}"></dsp:getvalueof>
</c:if>
<input type="hidden" value="${activeSKUsListLength}" class="activeSKUsListLength"/>
<input type="hidden" value="${colorLength}" class="colorLengthId"/>
<input type="hidden" value="${juvenileSizesListLength}" class="juvenileSizesListLengthId"/>
<input type="hidden" value="${contextPath}" class="hiddenContextPath" />
                        <c:if test="${not empty colorCodeList && (colorSizeVariantsAvailableStatus eq 'bothColorSizeAvailable' || colorSizeVariantsAvailableStatus eq 'onlyColorVariants')}">
                        
                        <div class="color-text color_text" tabindex="0">
                            <span><fmt:message key="tru.productdetail.label.color"/></span> 
                            <c:choose>
                            <c:when test="${((not empty selectedColor && defaultColorCode eq selectedColor) || ((colorLength eq 1)) && not empty childSKUsList )}">
                            <span class="inline">
                         		 <dsp:valueof value="${defaultSKU.colorCode}"> </dsp:valueof>
                         		 </span>
                           </c:when>
                           
                            <c:otherwise>
                            	<span class="limit-1 inline">
                               		 <fmt:message key="tru.productdetail.label.selectcolor"/>
                            	</span>
                            </c:otherwise>
                            </c:choose>
                        </div>
                        	<dsp:getvalueof var="activeSKUsListColor" value="${activeSKUsList}"></dsp:getvalueof>
                       
                         <c:if test="${(colorSizeVariantsAvailableStatus eq 'bothColorSizeAvailable') && not empty selectedSizeSKUsList && not empty selectedSize}">
                        	<dsp:getvalueof var="sameSizeSKUsList" value="${selectedSizeSKUsList}"></dsp:getvalueof>
                        </c:if> 
                        <div class="colors color-swatches-block">
                        	<div class="color-swatches-section">
		                        <c:choose>
			                        <c:when test="${(colorSizeVariantsAvailableStatus eq 'onlyColorVariants' || colorSizeVariantsAvailableStatus eq 'bothColorSizeAvailable') && (colorLength eq 1) && not empty childSKUsList}">
				                        <div class="color-block color-selected" >
											<%-- <div class="color-block-inner" rel="${colorSwatchImage}" id="${defaultSKU.colorCode}" href="javascript:void(0);"></div> --%>
											<div title="${defaultSKU.colorCode}">
												<img class="color-block-inner color-selected-img" id="${defaultSKU.colorCode}" src="${defaultSKU.swatchImage}"/>
											</div>
										</div>
			                       </c:when>
			                       <c:otherwise>
									<c:forEach items="${colorCodeList}" var="colorCode">
										<!--  need to do this -->
										<dsp:getvalueof var="selectedColorId" value="" />
										<dsp:getvalueof var="swatchColorImage" value="${colorCodeToSwatchImageMap[colorCode]}"></dsp:getvalueof>
										<dsp:getvalueof var="find" value=""></dsp:getvalueof>
										<dsp:getvalueof var="findSwatchImage" value=""></dsp:getvalueof>
										<dsp:getvalueof var="findSameSize" value=""></dsp:getvalueof>
											<dsp:getvalueof var="findSwatchImageSameSize" value=""></dsp:getvalueof>
										<c:forEach items="${activeSKUsListColor}" var="activeSKUColor">
											<dsp:getvalueof var="activeColorCode" value="${activeSKUColor.colorCode}"></dsp:getvalueof>
					                         <c:if test="${activeColorCode eq colorCode}">
												<dsp:getvalueof var="find" value="${activeColorCode}"></dsp:getvalueof>
												<dsp:getvalueof var="findSwatchImage" value="${activeSKUColor.swatchImage }"></dsp:getvalueof>
											</c:if>
					                    </c:forEach>
					                    <c:forEach items="${sameSizeSKUsList}" var="sameSizeSku">
											<dsp:getvalueof var="sameSizeSkuColor" value="${sameSizeSku.colorCode}"></dsp:getvalueof>
					                         <c:if test="${sameSizeSkuColor eq colorCode}">
												<dsp:getvalueof var="findSameSize" value="${sameSizeSkuColor}"></dsp:getvalueof>
												<dsp:getvalueof var="findSwatchImageSameSize" value="${sameSizeSku.swatchImage }"></dsp:getvalueof>
											</c:if>
					                    </c:forEach>
										<c:choose>
											<c:when test="${(not empty find && find eq colorCode && empty selectedSize) ||(not empty selectedSize && not empty findSameSize && findSameSize eq colorCode)
																|| (not empty selectedColor && selectedColor eq find)}">
											<c:if test="${((not empty find && find eq  selectedColor) || (not empty findSameSize && findSameSize eq selectedColor))}">
													<dsp:getvalueof var="selectedColorId" value="color-selected" />
												</c:if>
												<div class="color-block ${selectedColorId}" tabindex="0">
													<%-- <div class="color-block-inner" rel="${colorSwatchImage}" id="${find}" href="javascript:void(0);"></div> --%>
													<div title="${colorCode}">
														<c:choose>
														<c:when test="${not empty swatchColorImage}">
														<img class="color-block-inner" id="${colorCode}" src="${swatchColorImage}${swatchImageBlock}"/>
														</c:when>
														<c:otherwise>
														<img class="color-block-inner img-brd img-empty" id="${colorCode}" src=""/>
														</c:otherwise>
														</c:choose>
													</div>
												</div>
											</c:when>
											<c:when test="${not empty find && empty findSameSize}">
												<%-- <c:if test="${not empty selectedColor && find eq selectedColor}">
													<dsp:getvalueof var="selectedColorId1" value="color-selected" />
												</c:if> --%>
												<div class="color-block not-carried" tabindex="0">
													<%-- <div class="color-block-inner" rel="${colorSwatchImage}" id="${find}" href="javascript:void(0);"></div> --%>
													<div title="${colorCode}">
													<c:choose>
														<c:when test="${not empty swatchColorImage}">
														<img class="color-block-inner" id="${colorCode}" src="${swatchColorImage}${swatchImageBlock}"/>
														</c:when>
														<c:otherwise>
															<img class="color-block-inner img-brd img-empty" id="${colorCode}" src=""/>
														</c:otherwise>
														</c:choose>
													</div>
												</div>
											</c:when>
											<c:otherwise>
												<div class="color-block out-of-stock not-available">
													<!-- <div class="color-block-inner"></div> -->
										
													<div title="${colorCode}">
													<c:choose>
														<c:when test="${not empty swatchColorImage}">
													<img class="color-block-inner img-brd color-out-of-stock-img" id="${colorCode}" src="${swatchColorImage}${swatchImageBlock}" />
													</c:when>
													<c:otherwise>
														<img class="color-block-inner img-brd img-empty color-out-of-stock-img" id="${colorCode}" src="" />
													</c:otherwise>
													</c:choose>
													</div>
												</div>
											</c:otherwise>
										</c:choose>
									</c:forEach>
									
								</c:otherwise>
		                        </c:choose>
							</div>
                         <c:if test="${fn:length(colorCodeList) gt 6}">   
							<div class="view-more-color-section"><a href="javascript:void(0);" class="view-more-color-swatch-link">view more swatches</a></div>
						 </c:if>
                        </div>
                        <div class="small-spacer"></div>
                       </c:if>
                       
                       <c:if test="${not empty juvenileSizesList && (colorSizeVariantsAvailableStatus eq 'bothColorSizeAvailable' || colorSizeVariantsAvailableStatus eq 'onlySizeVariants')}">                        
                        <div id="size_text" class="color-text" tabindex="0">
                            <span><fmt:message key="tru.productdetail.label.size"/>: </span>
                            <c:choose>
                            	<c:when test="${(not empty selectedSize && defaultJuvenileSize eq selectedSize) || ((colorSizeVariantsAvailableStatus eq 'onlySizeVariants' && juvenileSizesListLength eq 1) && not empty childSKUsList) }">
                            	<span class="inline">
                         			<dsp:valueof value="${defaultSKU.juvenileSize}"></dsp:valueof> 
                         			</span>
                           		</c:when>
                           		<%-- <c:when test="${ not empty selectedSize && defaultJuvenileSize ne selectedSize}">
                           			Sorry; we do not carry this combination
                           		</c:when> --%>
                           		<c:otherwise>
                           			<span class="limit-1 inline">
                               			 <fmt:message key="tru.productdetail.label.selectsize"/> 
                           			 </span>
                           		</c:otherwise>
                            </c:choose>
                        </div>
						<div class="size-qty-title-section">
                        	 <div class="color-text" id="qtys-text">
                               <span class="pdp-quantity-limit hide"><fmt:message key="tru.productdetail.label.quantityerrormessage"/></span>
                               <c:choose>
	                               <c:when test="${presellable && not empty presellQuantityUnits && presellQuantityUnits1 le 0}">
	                                     <p> <fmt:message key="tru.productdetail.label.presellthresholderror" /></p>
	                               </c:when>
                               </c:choose>
                            </div>
                       	 	<div class="label-container">
	                        	<a class="spark-size-label" >
	                        	 	<span class="size-label">size&nbsp;</span>
	                        	 	<!-- <span class="selected-swatch-label">3x4</span> -->
								</a>
								<div class="extra-space"></div>
							</div>
							<div class="sticky-quantity spark-size-avaliable">
								<dsp:include page="/jstemplate/quantitySection.jsp">
									<dsp:param name="isSizeAvailable" value="true"/>
								</dsp:include>
							</div>
						</div>
                     <div class="sizes">
                       <div class="sticky-sizes">
							<div class="size-swatches-container">
                            <dsp:getvalueof var="activeSKUsListSize" value="${activeSKUsList}"></dsp:getvalueof>
                            <c:if test="${(colorSizeVariantsAvailableStatus eq 'bothColorSizeAvailable') && not empty selectedColorSKUsList && not empty selectedColor}">
                        		<dsp:getvalueof var="activeSKUsListSize" value="${selectedColorSKUsList}"></dsp:getvalueof>
                        	</c:if>
                        <c:choose>
	                        <c:when test="${(colorSizeVariantsAvailableStatus eq 'onlySizeVariants') && (juvenileSizesListLength eq 1) && not empty childSKUsList}">
			                         <span class="swatches selected-swatch" data-id="${defaultSKU.juvenileSize}">
                                    <dsp:valueof value="${defaultSKU.juvenileSize}"></dsp:valueof>
		                              </span>
	                        </c:when>
	                        <c:otherwise>
                             <c:forEach items="${juvenileSizesList}" var="juvenileSize">
									 <%-- <dsp:getvalueof var="activeSKUsListSize" value="${activeSKUsList}"></dsp:getvalueof>
		                            <c:if test="${(colorSizeVariantsAvailableStatus eq 'bothColorSizeAvailable') && not empty selectedColorSKUsList && not empty selectedColor}">
		                        		<dsp:getvalueof var="activeSKUsListSize" value="${selectedColorSKUsList}"></dsp:getvalueof>
		                        	</c:if> --%>
                                <dsp:getvalueof var="selectedSizeId" value="" />
                                <dsp:getvalueof var="findSize" value=""></dsp:getvalueof>
                                <c:forEach items="${activeSKUsListSize}" var="sizeActiveSKU">
		                                <dsp:getvalueof var="activeJuvenileSize" value="${sizeActiveSKU.juvenileSize}"></dsp:getvalueof>
		                             <c:if test="${activeJuvenileSize eq juvenileSize}">
										<dsp:getvalueof var="findSize" value="${activeJuvenileSize}"></dsp:getvalueof>
									 </c:if>
                                </c:forEach>
                                <c:choose>
                               <c:when test="${not empty findSize}">
		                               <c:if test="${findSize eq selectedSize}">
											<dsp:getvalueof var="selectedSizeId" value="selected-swatch" />
										</c:if>
											<span class="swatches" data-id="${findSize}"><dsp:valueof value="${findSize}"></dsp:valueof>
											</span>
                                </c:when>
                                <c:otherwise>
	                                <dsp:getvalueof var="inActiveFoundForThisIteration" value="false"></dsp:getvalueof>
	                                <c:forEach items="${inActiveSKUsList}" var="inActiveSKU">
		                                <dsp:getvalueof var="inActiveJuvenileSize" value="${inActiveSKU.juvenileSize}"></dsp:getvalueof>
		                                <c:if test="${inActiveJuvenileSize eq  juvenileSize}">
		                                <dsp:getvalueof var="inActiveFoundForThisIteration" value="true"></dsp:getvalueof>
		                                </c:if>
	                                </c:forEach>
	                                <c:choose>
		                                <c:when test="${inActiveFoundForThisIteration}">
						                    <span class="swatches out-of-stock not-available"  data-id="${juvenileSize}">
		                                    <dsp:valueof value="${juvenileSize}"></dsp:valueof>
						                     </span>
						                 </c:when>
		                                <c:otherwise>
						                      <span class="swatches out-of-stock not-carried"  data-id="${juvenileSize}">
		                                      <dsp:valueof value="${juvenileSize}"></dsp:valueof>
						                      </span>
		                                </c:otherwise>
	                                </c:choose>
                                </c:otherwise>
                                </c:choose>
                                 </c:forEach>
                           		</c:otherwise>
                           		</c:choose>
                            <c:if test="${(not empty sizeChartName) && ((pageIncludedFrom eq 'pdpPage') || (pageIncludedFrom eq 'collectionPage'))}">
                            <div class="size-chart">
	            				<a href="#" data-toggle="modal" data-target="#sizeChartModal"><fmt:message key="tru.productdetail.label.sizechart"/></a>
	        				</div>
	        				</c:if>
                        </div>
	                     </div>
	                  </div>
                      </c:if>

</dsp:page>