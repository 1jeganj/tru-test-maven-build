<dsp:page>
<dsp:importbean bean="/atg/targeting/TargetingFirst" />
<dsp:importbean bean="/atg/registry/RepositoryTargeters/TRU/ProductDetailPage/PrivacyPolicyTargeter"/>
<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale" />
<dsp:getvalueof var="atgTargeterpath" bean="PrivacyPolicyTargeter.AbsoluteName" />

<dsp:getvalueof var="cacheKey" value="${atgTargeterpath}${siteId}${locale}" />
<dsp:droplet name="/com/tru/cache/TRUPrivacyPolicyTargeterCache">
	<dsp:param name="key" value="${cacheKey}" />
	<dsp:oparam name="output">
		<dsp:droplet name="TargetingFirst">
			<dsp:param name="targeter" bean="PrivacyPolicyTargeter" />
			<dsp:oparam name="output">
				<dsp:valueof param="element.data" valueishtml="true" />
			</dsp:oparam>
			<dsp:oparam name="empty">
			</dsp:oparam>
		</dsp:droplet>
	</dsp:oparam>
</dsp:droplet>
</dsp:page>