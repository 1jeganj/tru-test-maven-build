<dsp:page>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/com/tru/commerce/droplet/TRUHidePriceDroplet"/>
<dsp:importbean bean="/com/tru/commerce/locations/TRUStoreAvailabilityDroplet"/>
<dsp:importbean bean="/atg/commerce/pricing/PriceItem" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUItemLevelPromotionDroplet"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<dsp:importbean bean="/atg/multisite/Site"/>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:getvalueof bean="TRUConfiguration.priceOnCartValue" var="priceOnCartValue" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:getvalueof param="productInfo" var="productInfo" />
<dsp:getvalueof param="productInfo.colorSwatchImageList" var="colorSwatchImageList" />
<dsp:getvalueof param="productInfo.colorCodeList" var="colorCodeList" />
<dsp:getvalueof param="productInfo.juvenileSizesList" var="juvenileSizesList" /> 
<dsp:getvalueof param="productInfo.defaultSKU.primaryImage" var="primaryImage" /> 
<dsp:getvalueof param="productInfo.defaultSKU" var="defaultSKU" /> 
<dsp:getvalueof param="productInfo.colorSkusMap" var="colorSkusMap" />
<dsp:getvalueof param="productInfo.sizeSkusMap" var="sizeSkusMap" /> 
<dsp:getvalueof param="productInfo.childSKUsList" var="childSKUsList" />
<dsp:getvalueof param="productInfo.activeSKUsList" var="activeSKUsList" />
<dsp:getvalueof param="productInfo.colorSizeVariantsAvailableStatus" var="colorSizeVariantsAvailableStatus" />
<dsp:getvalueof param="productInfo.selectedSizeSKUsList" var="selectedSizeSKUsList" />
<dsp:getvalueof param="productInfo.selectedColorSKUsList" var="selectedColorSKUsList" />
<dsp:getvalueof param="productInfo.defaultSKU.channelAvailability" var="channelAvailability" />
<dsp:getvalueof param="productInfo.defaultSKU.dropShipFlag" var="dropShipFlag" />
<dsp:getvalueof param="productInfo.defaultSKU.unCartable" var="unCartable" />  
<dsp:getvalueof var="selectedColor" param="selectedColorId" />
<dsp:getvalueof var="selectedSize" param="selectedSize" />
<dsp:getvalueof param="productInfo.defaultSKU.id" var="skuId"/>
<dsp:getvalueof var="promotionCountinPDP" bean="TRUConfiguration.promotionCountinPDP"/>
<dsp:getvalueof var="swatchImageBlock" param="swatchImageBlock"/>
<dsp:getvalueof var="priceDisplay" param="productInfo.defaultSKU.priceDisplay"></dsp:getvalueof><%-- This value needs to be get from SkuVO.priceDisplay --%>
<input type="hidden" value="${productInfo.productId}" id="hiddenProductId" />
<input type="hidden" value="${productInfo.defaultSKU.id}" id="pdpSkuId" />
				<%-- need to pass product item dynamically by keeping in vo--%>
<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
	<dsp:oparam name="output">
	<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
	</dsp:oparam>
</dsp:droplet>
				<dsp:droplet name="/atg/commerce/catalog/SKULookup">
				<dsp:param name="id" value="${skuId}"/>
				<dsp:param name="elementName" value="SKUItem"/>
				<dsp:oparam name="output">
				<dsp:getvalueof param="SKUItem" var="defaultSkuItem"></dsp:getvalueof>
				</dsp:oparam>
				</dsp:droplet>
				<dsp:getvalueof var="adjustments" value="${defaultSkuItem.skuQualifiedForPromos}"/>${skuQualifiedForPromos}
		
				<dsp:droplet name="TRUItemLevelPromotionDroplet">
				<dsp:param name="adjustments" value="${adjustments}"/>
				<dsp:oparam name="output">
					<dsp:getvalueof var="rankOnePromotion" param="rankOnePromotion"/>
					<dsp:getvalueof var="promotions" param="promotions"/>
					<dsp:getvalueof var="promotionCount" param="promotionCount"/>
				</dsp:oparam>
				</dsp:droplet>
				
                <div class="sticky-price" id="pdpId-${productInfo.productId}">
                    <div class="sticky-price-contents">
                    <div class="buy-1">
                    	<c:if test="${not empty rankOnePromotion}">
                    
                            	<c:choose>
									<c:when test="${not empty rankOnePromotion.description}">
										<dsp:valueof value="${rankOnePromotion.description}" /><span>.</span>
									</c:when>
									<c:otherwise>
										<dsp:valueof value="${rankOnePromotion.displayName}" /><span>.</span>
									</c:otherwise>
								</c:choose>
								<dsp:getvalueof var="rankOnePromotionDetails" value="${rankOnePromotion.promotionDetails}" />
								<c:if test="${not empty rankOnePromotionDetails}">
									<c:choose>
										<c:when test="${fn:startsWith(rankOnePromotionDetails, scheme)}">
											<a href="${rankOnePromotionDetails}" target="_blank" ><fmt:message key="tru.productdetail.label.seeterms" /></a>
										</c:when>
										<c:when test="${fn:startsWith(rankOnePromotionDetails, 'www')}">
											<a href="${scheme}${rankOnePromotionDetails}" target="_blank" ><fmt:message key="tru.productdetail.label.seeterms" /></a>
										</c:when>
										<c:otherwise>
											<span class="promotionDetailsSpan display-none">${rankOnePromotionDetails}</span>
											<a data-toggle="modal" data-target="#detailsModel" href="#" ><fmt:message key="tru.productdetail.label.seeterms" /></a>
										</c:otherwise>
									</c:choose>
								</c:if>
                        	
                        </c:if></div>
                        <c:choose>
                        	<c:when test="${priceDisplay eq priceOnCartValue}">
                        		<dsp:droplet name="TRUHidePriceDroplet">
                        			<dsp:param name="skuId" value="${skuId}"/><%--Need to pass dynamic values --%>
                        			<dsp:param name="productId" value="${productInfo.productId}"/><%--Need to pass dynamic values --%>
                        			<dsp:param name="order" bean="ShoppingCart.current"/>
                        			<dsp:oparam name="true">
	                        				<dsp:include page="/pricing/displayPrice.jsp">
												<%-- need to pass dynamic skuId and productId --%>
												<dsp:param name="skuId" value="${skuId}" />
												<dsp:param name="productId" value="${productInfo.productId}" />
											</dsp:include>
                        			</dsp:oparam>
                        			<dsp:oparam name="false">
                        			<div class="prices">
                        				<c:choose>
											<c:when test="${not empty unCartable && unCartable eq true }">
												<fmt:message key="tru.pricing.label.priceIsNotAvailable" />
											</c:when>
											<c:otherwise>
												<fmt:message key="tru.pricing.label.priceTooLowToDisplay" />
											</c:otherwise>
										</c:choose>
                        			</div>
                        				<div class="member-price" tabindex="0"></div>
                        			</dsp:oparam>
                        		</dsp:droplet>
                        	</c:when>
                        	<c:otherwise>
	                        		<dsp:include page="/pricing/displayPrice.jsp">
										<%-- need to pass dynamic skuId and productId --%>
										<dsp:param name="skuId" value="${skuId}" />
										<dsp:param name="productId" value="${productInfo.productId}" />
									</dsp:include>
                        	</c:otherwise>
                        </c:choose>
                        
						<div class="moreSpecialOffer">
				<c:if test="${not empty promotions}">
					<div class="btn-group special-offer">
						<button class="btn btn-default dropdown-toggle"
						data-toggle="dropdown" aria-expanded="false">
						<div class="special-offer-text">
							<div class="plus-sign"></div>&nbsp;<dsp:valueof value="${promotionCount}" />&nbsp;<fmt:message key="tru.productdetail.label.specialoffers"/>
						</div>
						</button>
						<ul class="offers-price dropdown-menu pdpOffers" role="menu">
							<dsp:droplet name="ForEach">
								<dsp:param name="array" value="${promotions}" />
								<dsp:param name="elementName" value="promotion" />
								<dsp:oparam name="output">
								<dsp:getvalueof var="promotionId" param="promotion.id" />
								<dsp:getvalueof var="promotionDetails" param="promotion.promotionDetails" />
									<li class="small-grey">
										<div class="inline blue-bullet"></div> 
										<dsp:getvalueof var="desc" param="promotion.description" />
											<c:choose>
												<c:when test="${not empty desc}">
													<dsp:valueof param="promotion.description" />&nbsp;.
												</c:when>
												<c:otherwise>
													<dsp:valueof param="promotion.displayName" />&nbsp;.
												</c:otherwise>
											</c:choose>
											<c:if test="${not empty promotionDetails}">
												<div class="small-blue inline">
													<c:choose>
															<c:when test="${fn:startsWith(promotionDetails, scheme)}">
																<a href="${promotionDetails}" target="_blank"><fmt:message key="tru.productdetail.label.details" /></a>
															</c:when>
															<c:when test="${fn:startsWith(promotionDetails, 'www' )}">
																<a href="${scheme}${promotionDetails}" target="_blank"><fmt:message key="tru.productdetail.label.details" /></a>
															</c:when>
															<c:otherwise>
																<span class="promotionDetailsSpan display-none">${promotionDetails}</span>
																<a data-toggle="modal" data-target="#detailsModel" href="#" ><fmt:message key="tru.productdetail.label.details" /></a>
															</c:otherwise>
														</c:choose>	
												</div>
										</c:if>
									</li>
								</dsp:oparam>
							</dsp:droplet>
						</ul>
					</div>
				</c:if></div>
				<dsp:include page="/jstemplate/esrbRating.jsp">
						</dsp:include>
			<input type="hidden" class="pageName" value="pdpPage" />
						<dsp:include page="/jstemplate/quickViewColorSizeVariantFragment.jsp">
                        	 <dsp:param param="productInfo" name="productInfo" />
                        	 <dsp:param value="pdpPage" name="pageIncludedFrom" />
                        	  <dsp:param param="swatchImageBlock" name="swatchImageBlock" />
                        </dsp:include>
                        <dsp:include page="/jstemplate/resourceBundle.jsp"></dsp:include>
						<dsp:include page="/jstemplate/sizeChartTemplate.jsp">
							<dsp:param param="productInfo" name="productInfo" />
						</dsp:include>
						<dsp:droplet name="/com/tru/commerce/locations/TRUGetCookieDroplet">
							<dsp:param name="cookieName" value="favStore" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="cookieValue" param="cookieValue" />
								<dsp:getvalueof var="locationIdFromCookie" param="locationId" />
							</dsp:oparam>
							<dsp:oparam name="empty">
							</dsp:oparam>
						</dsp:droplet>
						 <c:if test="${not empty locationIdFromCookie}">
						<dsp:droplet name="TRUStoreAvailabilityDroplet">
							<%--locationId we are getting from the Cookie in the TRUStoreAvailabilityDroplet  --%>
							<dsp:param name="skuId" value="${skuId}" />
							<dsp:param name="locationId" value="${locationIdFromCookie}" />
							<dsp:param name="isReqFromAjax" value="true" />
							<dsp:oparam name="available">
								<dsp:getvalueof var="storeAvailMsg" param="storeAvailMsg" />
								<dsp:getvalueof var="favStore" param="favStore" />
							</dsp:oparam>
						</dsp:droplet>
						</c:if>
						<%-- cookieValue: ${cookieValue}
						locationIdFromCookie: ${locationIdFromCookie}
						storeAvailMsg:${storeAvailMsg} --%>
						<dsp:include page="/jstemplate/quickViewAddToCart.jsp">
                        <dsp:param param="productInfo" name="productInfo" />
                        <dsp:param value="${cookieValue}" name="cookieValue" />
                        <dsp:param value="${locationIdFromCookie}" name="locationIdFromCookie" />
                         <dsp:param value="${storeAvailMsg}" name="storeAvailMsg" />
                          <dsp:param name="pageFrom" value="Product Detail Page"/>
                        </dsp:include>
                     
                        
                        <dsp:include page="/jstemplate/quickViewProductAvailability.jsp">
							<dsp:param param="productInfo" name="productInfo" />
							<dsp:param name="selectedSkuId" value="${skuId}" />
							<dsp:param value="${cookieValue}" name="cookieValue" />
                       		<dsp:param value="${locationIdFromCookie}" name="locationIdFromCookie" />
                       		<dsp:param value="${storeAvailMsg}" name="storeAvailMsg" />
						</dsp:include>

                        <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                            <div class="modal-dialog warning-modal">
                                <div class="modal-content sharp-border">
                                    <div class="modal-header">
                                        <a href="javascript:void(0)" class="close-modal" data-dismiss="modal" title="close model"><img src="${TRUImagePath}images/close.png" alt="close modal"></a>
                                        <div>
                                            <div class="big-red">
                                                <fmt:message key="tru.productdetail.label.Warning"/>
                                            </div>
                                            <div class="warning-message">
												<div class="warning-text bold">
												     <fmt:message key="tru.productdetail.label.maturemessage"/>
												<!-- One or more of the items you are attempting to add to your cart is intended for mature audiences. -->
												</div>
												<div class="warning-text">
												     <fmt:message key="tru.productdetail.label.maturemessagewarning"/>
												<!-- You must be 17 or older to purchase. These items may contain intense violence, blood and gore, sexual content and/or strong language. -->
												</div>
												<div class="warning-text bold">
												     <fmt:message key="tru.productdetail.label.maturemessageconfirmation"/>
											<!-- 	By ordering this item you are certifying that you are at least 17 years of age. -->
												</div>
											</div>
                                            <div class="warning-modal-footer">
                                                <div class="over-17 inline">
                                                    <div class="modal-checkbox-container">
                                                        <input class="modal-checkbox" id="pdp-modal-checkbox-1" type="checkbox">
                                                        <label for="pdp-modal-checkbox-1"></label>
                                                    </div>
                                                         <div class="inline warning-text"><fmt:message key="tru.productdetail.label.maturemessageoverseventeen"/> </div>
                                                   <!--  <div class="inline warning-text">Yes, I am age 17 or older</div> -->
                                                </div>
                                                <div class="under-17 inline">
                                                    <div class="modal-checkbox-container">
                                                        <input class="modal-checkbox" id="pdp-modal-checkbox-2" type="checkbox">
                                                        <label for="pdp-modal-checkbox-2"></label>
                                                    </div> 
                                                          <div class="inline warning-text"><fmt:message key="tru.productdetail.label.maturemessageunderseventeen"/></div>
                                                   <!--  <div class="inline warning-text">No, I am under age 17</div> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<%-- <div id="detailsPopup" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content sharp-border welcome-back-overlay">
									<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
									<h2>details</h2>
									<p class="abandonCartReminder">
										This message shows how many days we take to ship the item from our warehouse
									</p>
								</div>
							</div>
						</div> --%>
						
						<div id="learnMorePopup" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content sharp-border welcome-back-overlay">
									<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
									<h2>learn more</h2>
									<p class="abandonCartReminder">
									        <fmt:message key="tru.productdetail.label.storesonly"/>
										<!-- these items is available for customers to go buy at stores only. -->
									</p>
								</div>
							</div>
						</div>
						
                    </div>
                    <dsp:include page="/jstemplate/socialShareLinks.jsp">
                    	<dsp:param name="" value=""/>
                   		 </dsp:include>
                </div>
                 <div class="modal fade" id="notifyMeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                 	<dsp:include page="/jstemplate/notifyMePopUp.jsp"></dsp:include>           
			     </div>
				 <dsp:include page="/jstemplate/privacyPolicyTemplate.jsp">
                   </dsp:include>
			
<%-- <div class="modal fade detailLearnMorePopup" id="detailsModel" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false" >
                <dsp:include page="/pricing/promotionDetailsPopup.jsp"/>
			</div> --%>
	
</dsp:page>