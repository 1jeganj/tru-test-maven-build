<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />	
<dsp:getvalueof param="primaryImage" var="primaryImage" />
<dsp:getvalueof var="zoomPrimaryMainImageBlock" param="zoomPrimaryMainImageBlock"/>
<c:choose>
	<c:when test="${not empty pdpH1}">
        <c:set var="altH1SkuName" value="${pdpH1}"/>  
	</c:when>
	<c:otherwise>
		<c:set var="altH1SkuName" value="${displayName}"/>
	</c:otherwise>
</c:choose>
 	<c:choose>
	   	<c:when test="${not empty primaryImage}">
	       <img class="zoomimage" id="primaryImgGallery" src="${primaryImage}${zoomPrimaryMainImageBlock}" alt="${altH1SkuName}">
	    </c:when>
	    <c:otherwise>	                    					
	      <c:choose>
				<c:when test="${not empty akamaiNoImageURL}">
					<img class="zoomimage" id="gallery-noimage" src="${akamaiNoImageURL}" alt="${altH1SkuName}">
	                <input type="hidden" id="gallery-noimage1" value="${akamaiNoImageURL}" />
				</c:when>
				 <c:otherwise>
					<img class="zoomimage" id="gallery-noimage" src="${TRUImagePath}images/no-image500.gif" alt="${altH1SkuName}">
	                <input type="hidden" id="gallery-noimage1" value="${TRUImagePath}images/no-image500.gif" />
				</c:otherwise>
		</c:choose>	
	    </c:otherwise>
</c:choose>
</dsp:page>