<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof var="presellable" param="presellable"/>
<div class="product-block-price">
<dsp:importbean bean="/com/tru/commerce/droplet/TRUPriceDroplet" />
	<dsp:droplet name="TRUPriceDroplet">
		<dsp:param name="skuId" param="skuId" />
		<dsp:param name="productId" param="productId" />
		<dsp:oparam name="true">
				 <span class="product-block-original-price strike-out"><fmt:message key="pricing.dollar" /> <dsp:valueof param="listPrice" /></span> 
				<span class="product-block-sale-price">&nbsp;<fmt:message key="pricing.dollar"/><dsp:valueof param="salePrice" /></span>
		</dsp:oparam>
		<dsp:oparam name="false">
				<dsp:getvalueof var="salePrice" param="salePrice" />
				<c:choose>
					<c:when test="${salePrice == 0}">
						<span class="product-block-original-price"> <fmt:message key="pricing.dollar" /><dsp:valueof param="listPrice" /></span>
					</c:when>
					<c:otherwise>
						<span class="product-block-original-price"> <fmt:message key="pricing.dollar" /><dsp:valueof param="salePrice" /></span>
					</c:otherwise>
				</c:choose>
		</dsp:oparam>
		<dsp:oparam name="empty">
		</dsp:oparam>
	</dsp:droplet>
	<c:if test="${presellable}">
	<div class="clear-both"></div>
     <span class="preOrder">
	<fmt:message key="tru.productdetail.label.preorder"/>
	</span>
	</c:if>	
</div>
</dsp:page>