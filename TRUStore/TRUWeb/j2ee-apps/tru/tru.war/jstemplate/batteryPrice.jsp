<dsp:page>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUHidePriceDroplet"/>
<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:getvalueof bean="TRUConfiguration.priceOnCartValue" var="priceOnCartValue" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof param="productId" var="productId" />
<dsp:getvalueof param="skuId" var="skuId" />
<dsp:getvalueof param="priceDisplay" var="priceDisplay" />
<dsp:getvalueof param="unCartable" var="unCartable" />
  <c:choose>
		<c:when test="${priceDisplay eq priceOnCartValue}">
			<dsp:droplet name="TRUHidePriceDroplet">
				<dsp:param name="skuId" value="${skuId}" />
				<dsp:param name="productId" value="${productId}" />
				<dsp:param name="order" bean="ShoppingCart.current" />
				<dsp:oparam name="true">
					<dsp:include page="/jstemplate/batteryDisplayPrice.jsp">
						<dsp:param name="skuId" value="${skuId}" />
						<dsp:param name="productId" value="${productId}" />
					</dsp:include>
				</dsp:oparam>
				<dsp:oparam name="false">
				<div class="price batteryPriceLow">
						<c:choose>
							<c:when test="${not empty unCartable && unCartable eq true }">
							<div class="cart-prior-order"><fmt:message key="tru.pricing.label.priceIsNotAvailable" /></div>
							</c:when>
							<c:otherwise>
							<div class="cart-prior-order"><fmt:message key="tru.pricing.label.priceTooLowToDisplay" /></div>
							</c:otherwise>
						</c:choose>	
				</div>
				</dsp:oparam>
			</dsp:droplet>
		</c:when> 
		<c:otherwise> 
			<dsp:include page="/jstemplate/batteryDisplayPrice.jsp">
				<dsp:param name="skuId" value="${skuId}" />
				<dsp:param name="productId" value="${productId}" />
			</dsp:include>
		 </c:otherwise>
	</c:choose> 

</dsp:page>