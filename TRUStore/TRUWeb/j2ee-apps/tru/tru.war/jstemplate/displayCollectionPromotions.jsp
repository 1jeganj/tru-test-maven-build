<dsp:page>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/com/tru/commerce/droplet/TRUItemLevelPromotionDroplet"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:getvalueof param="productInfo" var="productInfo" />
<dsp:getvalueof param="productInfo.defaultSKU.id" var="skuId"/>
<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
	<dsp:oparam name="output">
	<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
	</dsp:oparam>
</dsp:droplet>
<dsp:droplet name="/atg/commerce/catalog/SKULookup">
				<dsp:param name="id" value="${skuId}"/>
				<dsp:param name="elementName" value="SKUItem"/>
		<dsp:oparam name="output">
				<dsp:getvalueof param="SKUItem" var="defaultSkuItem"></dsp:getvalueof>
				<dsp:getvalueof var="adjustments" value="${defaultSkuItem.skuQualifiedForPromos}"/>
		</dsp:oparam>
 </dsp:droplet>
	<dsp:droplet name="TRUItemLevelPromotionDroplet">
		<dsp:param name="adjustments" value="${adjustments}" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="rankOnePromotion" param="rankOnePromotion" />
			<dsp:getvalueof var="promotions" param="promotions" />
			<dsp:getvalueof var="promotionCount" param="promotionCount" />
		</dsp:oparam>
	</dsp:droplet>
	<c:if test="${not empty rankOnePromotion}">
		
			<c:choose>
				<c:when test="${not empty rankOnePromotion.description}">
					<dsp:valueof value="${rankOnePromotion.description}" />
					<span>&#xb7;</span>
				</c:when>
				<c:otherwise>
					<dsp:valueof value="${rankOnePromotion.displayName}" />
					<span>&#xb7;</span>
				</c:otherwise>
			</c:choose>
			<dsp:getvalueof var="rankOnePromotionDetails" value="${rankOnePromotion.promotionDetails}" />
			<c:if test="${not empty rankOnePromotionDetails}">
				<c:choose>
					<c:when test="${fn:startsWith(rankOnePromotionDetails, scheme)}">
						<a href="${rankOnePromotionDetails}" target="_blank"><fmt:message key="tru.productdetail.label.seeterms" /></a>
					</c:when>
					<c:when test="${fn:startsWith(rankOnePromotionDetails, 'www')}">
						<a href="${scheme}${rankOnePromotionDetails}" target="_blank"><fmt:message
								key="tru.productdetail.label.seeterms" /></a>
					</c:when>
					<c:otherwise>
						<span class="promotionDetailsSpan display-none">${rankOnePromotionDetails}</span>
						<a data-toggle="modal" data-target="#detailsModel" href="#"><fmt:message key="tru.productdetail.label.seeterms" /></a>
					</c:otherwise>
				</c:choose>
			</c:if>
		
	</c:if>
		<%-- <div class="modal fade detailLearnMorePopup" id="detailsModel" tabindex="-1" role="dialog"
		aria-labelledby="basicModal" aria-hidden="false">
		<dsp:include page="/pricing/promotionDetailsPopup.jsp"/>
		
		
	</div> --%>

</dsp:page>