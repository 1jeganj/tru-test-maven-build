<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="akamaiNoImageURL" bean="TRUStoreConfiguration.akamaiNoImageURL" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:getvalueof param="productInfo.defaultSKU.primaryCanonicalImage" var="primaryCanonicalImage" />
<dsp:getvalueof param="productInfo.defaultSKU.displayName" var="displayName" />
<dsp:getvalueof  param="productInfo.defaultSKU.promotionalStickerDisplay" var="promotionalStickerDisplay"/>	
<dsp:getvalueof param="primaryMainImageBlock" var="primaryMainImageBlock"/>
<dsp:getvalueof var="promotionalStickerDisplayTextMap" bean="TRUStoreConfiguration.promotionalStickerDisplayTextMap" />
<c:set var="primaryImage" value="${fn:escapeXml(primaryCanonicalImage)}"/>
<c:set var="promotionalStickerDisplay" value="${fn:toLowerCase(promotionalStickerDisplay)}"/>
<c:forEach var="entry" items="${promotionalStickerDisplayTextMap}">
		<c:if test="${entry.key eq 'rexclusive'}">
			<c:set var="rexclusive" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'ourexclusive' }">
			<c:set var="ourexclusive" value="${entry.value}" />
		</c:if>
		</c:forEach>
<div class="pdp-header-block">
<%-- 	<dsp:include page="/jstemplate/productHeader.jsp">
		<dsp:param name="productInfo" param="productInfo"/>
	</dsp:include> --%>
		<c:choose>
			<c:when test="${not empty pdpH1}">
                     <c:set var="altH1SkuName" value="${pdpH1}"/>  
			</c:when>
			<c:otherwise>
					<c:set var="altH1SkuName" value="${displayName}"/>
			</c:otherwise>
		</c:choose>
        <div class="toys-template">
            <div class="sticky-price-template pdp-commerce-zone">
		<dsp:include page="/jstemplate/skuVariantsAndPriceSection.jsp">
			<dsp:param name="productInfo" param="productInfo"/>
			<dsp:param name="swatchImageBlock" param="swatchImageBlock"/>
		</dsp:include>
            </div>
            <%--  <div class="inline">
              <object data="${TRUImagePath}images/svg/GiftFinderTag.svg" id="giftFinderTag" type="image/svg+xml"></object>
            </div> --%> 
             <!-- product large Image Area -->
             <div class="product-hero-area-wrapper">
            <div class="product-hero-area">
            <!--   -->
            <dsp:include page="/jstemplate/productHeader.jsp">
				<dsp:param name="productInfo" param="productInfo"/>
			</dsp:include>
             <!--   -->
                <div class="block">
		 <div class="promo-with-img-container">
			<c:if test="${not empty promotionalStickerDisplay && (promotionalStickerDisplay eq ourexclusive || promotionalStickerDisplay eq rexclusive )}">
				<div class="our-exclusive">
				<%-- <p><fmt:message key="tru.productdetail.label.ourexclusive" /></p> --%>
				</div> 
		 	</c:if>
                   <!--  <div class="PDP-zoom-icon" data-toggle="modal" tabindex="0" aria-label="product zoom view"></div> -->
                 <!-- Top part of the slider -->
                    <div id="slider">
			         <div id="carousel-bounding-box">
			            <div class="carousel slide" id="PDPCarousel">
			               <!-- Carousel items -->
			               <div class="carousel-inner">
			                  <div class="active item" data-slide-number="0">
                    <c:choose>
                    <c:when test="${not empty primaryImage}">
				                    <div class="zoomContainer">
				                    	<img class="zoomImage" src="${primaryImage}${primaryMainImageBlock}" alt="${altH1SkuName}">
                   						<img class="largeImage" src="${primaryImage}?fit=inside|960:960" alt="${altH1SkuName}">
				                   </div>				                   
                    </c:when>
                    <c:otherwise>
				                  		<div class="zoomContainer">
				                    		<img class="zoomImage" src="${akamaiNoImageURL}${primaryMainImageBlock}" alt="${altH1SkuName}">
                   							<img class="largeImage" src="${akamaiNoImageURL}?fit=inside|960:960" alt="${altH1SkuName}">
				                   		</div>
                    </c:otherwise>
                    </c:choose>
			                  </div>      
			               </div>
			               <!-- Carousel nav --> 
			            </div>
			         </div>
			      </div>
			   <!-- Top part of the slider -->
                </div>
                </div>
								<%--
                <div class="modal fade" id="galleryOverlayModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                <dsp:include page="/jstemplate/imageGallery.jsp">
                	<dsp:param name="productInfo" param="productInfo"/>
                	<dsp:param name="zoomPrimaryMainImageBlock" param="zoomPrimaryMainImageBlock"/>
                	<dsp:param name="zoomAltPriImageBlock" param="zoomAltPriImageBlock"/>
                	<dsp:param name="zoomAltSecImageBlock" param="zoomAltSecImageBlock"/>
                	<dsp:param name="zoomAlternativeImage1Block" param="zoomAlternativeImage1Block"/>
                	<dsp:param name="zoomAlternativeImage2Block" param="zoomAlternativeImage2Block"/>
                	<dsp:param name="zoomAlternativeImage3Block" param="zoomAlternativeImage3Block"/>
                	<dsp:param name="zoomAlternativeImage4Block" param="zoomAlternativeImage4Block"/>
                </dsp:include>
                </div>
								--%>
<%--                    <dsp:include page="/jstemplate/socialShareLinks.jsp"> 
                    	<dsp:param name="" value=""/>
                   </dsp:include> --%>
            </div>
            </div>
        </div>
    </div>
</dsp:page>