<dsp:page>
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
	<dsp:getvalueof var="productId" param="productId" />
	<dsp:getvalueof var="collectionId" param="collectionId" />
	<dsp:getvalueof var="skuId" param="skuId" />
	<dsp:getvalueof var="childProductZoomImageBlock" param="childProductZoomImageBlock" />
	<dsp:getvalueof var="childProductPriAltImageBlock" param="childProductPriAltImageBlock" />
	<dsp:getvalueof var="childProductSecAltImageBlock" param="childProductSecAltImageBlock" />
	<dsp:getvalueof var="childProductAltImageBlock" param="childProductAltImageBlock" />
childProductSecAltImageBlock;;#${childProductSecAltImageBlock}
	<dsp:droplet name="/com/tru/commerce/droplet/TRUCollectionLineItemInfoLookupDroplet">
	<dsp:param name="collectionId" value="${collectionId}"/>
	<dsp:param name="productId" value="${productId}"/>
	<dsp:param name="skuId" value="${skuId}"/>
		<dsp:oparam name="output">
             <dsp:getvalueof param="collectionLineItemInfo" var="productInfo"/>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:include page="/jstemplate/imageGallery.jsp">
	 <dsp:param name="page" value="collection"/>
	 <dsp:param name="productInfo" value="${productInfo}"/>
	 <dsp:param name="zoomPrimaryMainImageBlock" value="${childProductZoomImageBlock}"/>
	 <dsp:param name="zoomAltPriImageBlock" value="${childProductPriAltImageBlock}"/>
	 <dsp:param name="zoomAltSecImageBlock" value="${childProductSecAltImageBlock}"/>
	 <dsp:param name="zoomAlternativeImage1Block" value="${childProductAltImageBlock}"/>
	</dsp:include>
</dsp:page>