<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>

<dsp:getvalueof var="childProductZoomImageBlock" param="childProductZoomImageBlock"/>
<dsp:getvalueof var="childProductPriAltImageBlock" param="childProductPriAltImageBlock"/>
<dsp:getvalueof var="childProductSecAltImageBlock" param="childProductSecAltImageBlock"/>
<dsp:getvalueof var="childProductAltImageBlock" param="childProductAltImageBlock"/>
<dsp:getvalueof var="swatchImageBlock" param="swatchImageBlock"/>
<dsp:getvalueof var="collectionProductListImageBlock" param="collectionProductListImageBlock"/>
<div class="small-spacer"></div>
<div class="small-spacer"></div>
<div class="row">
            <div class="col-md-12 collection-spark-product-block">
                <h2><fmt:message key="tru.productdetail.label.chooseitems"/></h2>
                <hr/>
                <div class="small-spacer"></div>
					<dsp:getvalueof param="collectionProductInfo" var="collectionProductInfo" />
					<dsp:getvalueof param="collectionProductInfo.collectionId" var="collectionId" />
					<dsp:getvalueof param="collectionProductInfo.productInfoList" var="productInfoList" />
					<dsp:getvalueof param="collectionProductInfo.productInfoJsonMap" var="productInfoJsonMap" />
					  <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                          <dsp:param name="array" value="${productInfoList}" />
                          <dsp:getvalueof param="element" var="productInfo"/>
                          <dsp:oparam name="output">
                          <dsp:getvalueof value="${productInfo.productId }" var="productId"/>
                          <dsp:getvalueof value="${productInfoJsonMap[productId]}" var="productInfoJson"/>
			                <dsp:include page="/jstemplate/collectionProduct.jsp">
			                <dsp:param name="productInfo" value="${productInfo}"/>
			                 <dsp:param name="productInfoJson" value="${productInfoJson}"/>
			                <dsp:param name="collectionId" value="${collectionId}"/>
			                <dsp:param name="swatchImageBlock" value="${swatchImageBlock}"/>
			                <dsp:param name="collectionProductListImageBlock" value="${collectionProductListImageBlock}"/>
			             	</dsp:include>
			             	</dsp:oparam>
			          </dsp:droplet>
            </div>
         
            <div class="col-md-4">
              <dsp:include page="/jstemplate/collectionAddToCart.jsp">
              			<dsp:param name="fromCollectionPage" value="true"/>
              			<dsp:param name="collectionProductInfo" value="${collectionProductInfo}"/>
              </dsp:include>
            </div>
             <dsp:include page="/jstemplate/resourceBundle.jsp"></dsp:include>
        </div>
        
        <div class="modal fade in" id="emailMePopUp" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false">
		  <dsp:include page="/jstemplate/emailMePopUp.jsp">
		  <dsp:param name="productInfo" param="productInfo"/>
		  </dsp:include>
		</div>
		
		
				<input type="hidden" id="childProductZoomImageBlock" value="${childProductZoomImageBlock}" />
				<input type="hidden" id="childProductPriAltImageBlock" value="${childProductPriAltImageBlock}" />
				<input type="hidden" id="childProductSecAltImageBlock" value="${childProductSecAltImageBlock}" />
				<input type="hidden" id="childProductAltImageBlock" value="${childProductAltImageBlock}" />
				<div class="modal fade" id="galleryOverlayModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	                 <%-- <dsp:include page="/jstemplate/ajaxCollectionLineItemImagegallery.jsp">
					</dsp:include> --%> 
                </div>
				
				
				<!-- <div id="detailsPopup" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content sharp-border welcome-back-overlay">
							<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
							<h2>details</h2>
							<p class="abandonCartReminder">
								This message shows how many days we take to ship the item from our warehouse
							</p>
						</div>
					</div>
				</div>
						
				<div id="learnMorePopup" class="modal fade detailLearnMorePopup" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content sharp-border welcome-back-overlay">
							<a href="javascript:void(0)" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
							<h2>learn more</h2>
							<p class="abandonCartReminder">
								these items is available for customers to go buy at stores only.
							</p>
						</div>
					</div>
				</div>  -->
		


</dsp:page>	