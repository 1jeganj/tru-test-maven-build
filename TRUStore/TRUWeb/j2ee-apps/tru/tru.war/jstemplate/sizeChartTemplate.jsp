<dsp:page>
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof  param="productInfo.sizeChart" var="sizeChartName"/> 
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="pdpSizechartImageStaticPath" bean="TRUStoreConfiguration.pdpSizechartImageStaticPath" />
 
<c:if test="${not empty sizeChartName }">
 <div class="size-chart-modal modal fade" id="sizeChartModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="false" >
<div class="modal-dialog">
	<div class="modal-content sharp-border">
		<div class="modal-header">
			<a href="javascript:void(0)" class="close-modal" data-dismiss="modal"><span class="sprite-icon-x-close"></span></a>
			<div class="size-content size-chart-image">			  	
				<img src="${pdpSizechartImageStaticPath}${sizeChartName}" />
				</div>
			</div>
		</div>
	</div>
</div>
</c:if>
</dsp:page>