<dsp:page>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
    <dsp:getvalueof var="urlPagename" param="urlPagename" />
	<dsp:getvalueof var="urlAB" param="urlAB" />
	<dsp:getvalueof var="urlCatID" param="urlCatID" />
	<dsp:getvalueof var="cat" param="cat" />
<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
	
	<dsp:getvalueof var="plpPathCheck" bean="TRUStoreConfiguration.plpPathCheck" />
		


	
<c:choose>
<c:when test="${urlPagename eq 'product' or urlPagename eq 'collection' }">

<dsp:droplet name="/com/tru/droplet/TRUPDPReadCookieDroplet">
	<dsp:oparam name="output">
		<dsp:getvalueof var="breadcrumbvolist" param="breadcrumbvolist"/>
	</dsp:oparam>
	<dsp:oparam name="empty">
	</dsp:oparam>
</dsp:droplet>

<c:forEach var="breadList" items="${breadcrumbvolist}">

	<li class="pdp-update-breadcrumb">
		<%-- <img src="${TRUImagePath}images/breadcrumb-right-arrow-small.png" alt=""> --%>
	</li>
	<li class="pdp-update-breadcrumb">
		<a href="${siteURL}${breadList.categoryURL}&catdim=bcmb">${breadList.categoryName}</a>
	</li>
</c:forEach>
</c:when>




<c:when test="${(urlPagename eq 'category' or urlPagename eq 'family' or urlPagename eq 'subcat') && plpPathCheck eq 'true'}">

<dsp:droplet name="/com/tru/droplet/TRUPLPReadCookieDroplet">
	<dsp:oparam name="output">
		<dsp:getvalueof var="breadcrumbvolist" param="breadcrumbvolist"/>
	</dsp:oparam>
	<dsp:oparam name="empty">
	</dsp:oparam>
</dsp:droplet>

<c:forEach var="breadList" items="${breadcrumbvolist}">

	<li class="plp-update-breadcrumb">
		<a href="${siteURL}${breadList.categoryURL}&catdim=bcmb">${breadList.categoryName}</a>
	</li>

</c:forEach>

</c:when>
</c:choose>








</dsp:page>