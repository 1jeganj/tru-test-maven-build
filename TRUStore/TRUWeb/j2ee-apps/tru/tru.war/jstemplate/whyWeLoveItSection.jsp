<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof param="productInfo.whyWeloveIt" var="whyWeloveIt" />
	<c:if test="${not empty whyWeloveIt}">
	<div class="why-we-love-it">
		<%-- <h2><fmt:message key="tru.productdetail.label.whyWeLoveIt"/></h2> --%>
		<dsp:droplet name="/com/tru/commerce/droplet/TRUBigStringDecoderDroplet">
                 <dsp:param name="data" value="${whyWeloveIt}"/>
                  <dsp:oparam name="output">
                    		<dsp:valueof param="enocdeData" valueishtml="true" />
                   </dsp:oparam>
            </dsp:droplet>
		<%-- <dsp:valueof value="${whyWeloveIt}" valueishtml="true"></dsp:valueof> --%>
	</div>
	</c:if>
</dsp:page>