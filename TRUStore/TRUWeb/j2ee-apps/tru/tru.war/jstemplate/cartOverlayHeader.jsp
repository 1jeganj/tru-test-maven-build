<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<div class="shopping-cart-overlay-header">
        <span class="shopping-cart-image"></span>
        <span class="shopping-cart-overlay-header-num-items">
        	<dsp:getvalueof var="commerceItemCount" bean="ShoppingCart.current.totalItemCount"/>
        	<c:choose>
				<c:when test="${commerceItemCount > 1 }">
					<span> 
						<fmt:message key="shoppingcart.overlay.header.items.text" >
							<fmt:param value="${commerceItemCount}"></fmt:param>
						</fmt:message>
					</span>
				</c:when>
				<c:otherwise>
					<span> 
						<fmt:message key="shoppingcart.overlay.header.item.text" >
							<fmt:param value="${commerceItemCount}"></fmt:param>
						</fmt:message>
					</span>
				</c:otherwise>
			</c:choose>
        </span>
        <a href="javscript:void(0)"  data-dismiss="modal" tabindex="0" class="shopping_cart_overlay_continue_shopping"><span  class="shopping-cart-overlay-header-continue-shopping">continue shopping</span>
        <span class="x-image"></span></a>
    </div>
</dsp:page>