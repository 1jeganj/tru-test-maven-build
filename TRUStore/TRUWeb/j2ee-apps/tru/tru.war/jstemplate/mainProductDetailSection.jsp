<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="pdpAkamaiImageResizeMap" bean="TRUStoreConfiguration.pdpAkamaiImageResizeMap" />
<c:forEach var="entry" items="${pdpAkamaiImageResizeMap}">
		<c:if test="${entry.key eq 'primaryMainImageBlock'}">
			<c:set var="primaryMainImageBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'primaryImageThumbnailBlock'}">
			<c:set var="primaryImageThumbnailBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'secondaryImageThumbnailBlock'}">
			<c:set var="secondaryImageThumbnailBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'alternateImageThumbnail1Block'}">
			<c:set var="alternateImageThumbnail1Block" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'alternateImageThumbnail2Block'}">
			<c:set var="alternateImageThumbnail2Block" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'zoomPrimaryMainImageBlock'}">
			<c:set var="zoomPrimaryMainImageBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'zoomAltPriImageBlock'}">
			<c:set var="zoomAltPriImageBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'zoomAltSecImageBlock'}">
			<c:set var="zoomAltSecImageBlock" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'zoomAlternativeImage1Block'}">
			<c:set var="zoomAlternativeImage1Block" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'zoomAlternativeImage2Block'}">
			<c:set var="zoomAlternativeImage2Block" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'zoomAlternativeImage3Block'}">
			<c:set var="zoomAlternativeImage3Block" value="${entry.value}" />
		</c:if>
		<c:if test="${entry.key eq 'zoomAlternativeImage4Block'}">
			<c:set var="zoomAlternativeImage4Block" value="${entry.value}" />
		</c:if>
		
		<c:if test="${entry.key eq 'swatchImageBlock'}">
			<c:set var="swatchImageBlock" value="${entry.value}" />
		</c:if>
	</c:forEach>
	<input type="hidden" id="primaryMainImageBlock" value="${primaryMainImageBlock}">
	<input type="hidden" id="secondaryImageThumbnailBlock" value="${secondaryImageThumbnailBlock}">
	<input type="hidden" id="zoomPrimaryMainImageBlock" value="${zoomPrimaryMainImageBlock}">
	<input type="hidden" id ="alternateImageThumbnail1Block" value = "${alternateImageThumbnail1Block}">
<dsp:getvalueof param="productInfo" var="productInfo" />
	<div id="product-main-content" class="product-body-section">
		<dsp:include page="/jstemplate/headerblock.jsp">
			<dsp:param name="productInfo" param="productInfo"/>
			<dsp:param name="primaryMainImageBlock" value="${primaryMainImageBlock}"/>
			<dsp:param name="secondaryImageThumbnailBlock" value="${secondaryImageThumbnailBlock}"/>
			<dsp:param name="zoomPrimaryMainImageBlock" value="${zoomPrimaryMainImageBlock}"/>
			<dsp:param name="zoomAltPriImageBlock" value="${zoomAltPriImageBlock}"/>
			<dsp:param name="zoomAltSecImageBlock" value="${zoomAltSecImageBlock}"/>
			<dsp:param name="zoomAlternativeImage1Block" value="${zoomAlternativeImage1Block}"/>
			<dsp:param name="zoomAlternativeImage2Block" value="${zoomAlternativeImage2Block}"/>
			<dsp:param name="zoomAlternativeImage3Block" value="${zoomAlternativeImage3Block}"/>
			<dsp:param name="zoomAlternativeImage4Block" value="${zoomAlternativeImage4Block}"/>
			<dsp:param name="swatchImageBlock" value="${swatchImageBlock}"/>
			
		</dsp:include>
		<dsp:include page="/jstemplate/productDetailsBlock.jsp">
			<dsp:param name="productInfo" param="productInfo"/>
			<dsp:param name="primaryImageThumbnailBlock" value="${primaryImageThumbnailBlock}"/>
			<dsp:param name="secondaryImageThumbnailBlock" value="${secondaryImageThumbnailBlock}"/>
			<dsp:param name="alternateImageThumbnail1Block" value="${alternateImageThumbnail1Block}"/>
			<dsp:param name="alternateImageThumbnail2Block" value="${alternateImageThumbnail2Block}"/>
		</dsp:include>
	</div>
</dsp:page>