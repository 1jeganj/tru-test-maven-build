<dsp:page>
<dsp:importbean bean="/com/tru/droplet/TRURegistryCookieDroplet"/>
<dsp:getvalueof param="myRegistryID" var="myRegistryID"></dsp:getvalueof>
<dsp:getvalueof param="lastAccessedRegistryID" var="lastAccessedRegistryID"></dsp:getvalueof>
<dsp:getvalueof param="lastAccessedRegistryName" var="lastAccessedRegistryName"></dsp:getvalueof>
<dsp:getvalueof param="userStatus" var="userStatus"></dsp:getvalueof>
<dsp:getvalueof param="myWishlistID" var="myWishlistID"></dsp:getvalueof>
<dsp:getvalueof param="lastAccessedWishlistID" var="lastAccessedWishlistID"></dsp:getvalueof>
<dsp:getvalueof param="lastAccessedWishlistName" var="lastAccessedWishlistName"></dsp:getvalueof>
<dsp:droplet name="TRURegistryCookieDroplet">
<dsp:param name="registry_id" value="${myRegistryID}"/>
<dsp:param name="responseStatus" value="${userStatus}"/>
<dsp:param name="lastAccessedRegistryID" value="${lastAccessedRegistryID}"/>
<dsp:param name="lastAccessedRegistryName" value="${lastAccessedRegistryName}"/>
<dsp:param name="myWishlistID" value="${myWishlistID}"/>
<dsp:param name="lastAccessedWishlistID" value="${lastAccessedWishlistID}"/>
<dsp:param name="lastAccessedWishlistName" value="${lastAccessedWishlistName}"/> 
<dsp:oparam name="output">
</dsp:oparam>
</dsp:droplet>
</dsp:page>