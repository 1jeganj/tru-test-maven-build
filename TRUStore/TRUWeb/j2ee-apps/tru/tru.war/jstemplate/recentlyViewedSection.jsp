<dsp:page>
<%@ page isELIgnored ="true" %>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<script id="recentlyViewedCollectionTemplateBody" type="text/x-jquery-tmpl">
	<div class="product-carousel-title">
		<fmt:message key="tru.productdetail.label.recentlyviewed"/>
	</div>
	<div id="slider-product-block-container" class="slider-product-block-container" >
		<div data-u="slides" id="slides-product-block" class="slides-product-block" debug-id="slide-board">
			{{tmpl(productList) '#recentlyViewedCollectionPageTemplate'}}
		</div>

		<div data-u="navigator" id="multi-slide-bullets" class="jssor-bullet">
	   	<div data-u="prototype" class="av"></div>
		</div>

		<span id="multi-carousel-left-arrow" data-u="arrowleft" class="jssora11l" ></span>
		<span id="multi-carousel-right-arrow" data-u="arrowright" class="jssora11r"></span>
	</div>
</script>

<script id="recentlyViewedCollectionPageTemplate" type="text/x-jquery-tmpl">
	{{if typeof productType != 'undefined' && productType == "Collection"}}
		{{tmpl($data) "#collectionProductTemplate"}}
	{{else}}
		{{tmpl($data) "#PDPProductTemplate"}}
	{{/if}}
</script>

<script id="PDPProductTemplate" type="text/x-jquery-tmpl"> 


	<div class="col-md-3 col-no-padding product-block-padding">
		<div class="product-block product-block-unselected no-select">
			<div class="product-flag"></div>
			<div class="product-choose">
			  
			</div>
			<div class="product-selection">

				<div class="selection-checkbox-off"></div>
				<div class="selection-text-off">label select</div>
			</div>
			<div class="product-block-image-container">
				<div class="product-block-image">
					<div class="product-image">
						<a  class="bnsImgBlock recently-image-blur" href="${pdpUrl}">
							<img class="product-block-img" src="${primaryImage}" alt="product Image">
						</a>
						<div>
							<div class="quick-view-container">
								<div  class="product-hover-view product-quick-view" data-toggle="modal" data-target="#quickviewModal" onclick="loadQuickView('${productId}','')"><fmt:message key="tru.productdetail.label.quickview"/></div>
								<div><button>baby registry</button><button>wishlist</button></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="colors">
				<div class="color-block red color-selected">
					<div class="color-block-inner">

					</div>
				</div>
				<div class="color-block yellow">
					<div class="color-block-inner">

					</div>
				</div>
				<div class="color-block orange">
					<div class="color-block-inner">

					</div>
				</div>
			</div>
			<div class="wish-list-heart-block">
				<div class="wish-list-heart-off"></div>
			</div>
			<div class="star-rating-block">
				{{each(i,t) [1,2,3,4,5]}}
					{{if i + 1 > reviewRating}}
						<div class="star-rating-off"></div>
					{{else}}
						<div class="star-rating-on"></div>
					{{/if}}
				{{/each}}
			</div>
			<div class="product-block-information">
				<div class="product-name">
					<a href="${pdpUrl}">${displayName}</a>
				</div>
				{{if suggestedAgeMin}}
					<div class="age-range">
						Age:&nbsp;${suggestedAgeMin}
					</div>
				{{/if}}
				<div class="product-block-price">
					{{if savedPercentage}}
						<div class="recently-viewed-price">
							<span class="crossed-out">$${listPrice}</span>
							<span class="sale-price">$${salePrice}</span>
						</div>
					{{else}}
						<div class="prices">
							<span class="sale-price"> $
								{{if salePrice == 0}}
									${listPrice}
								{{else}}
									${salePrice}
								{{/if}}
							</span>
						</div>
					{{/if}}
					{{if presellable}}
						<div class="pre-order">pre-order now</div>
					{{else}}
						<div class="pre-order">&nbsp;</div>
					{{/if}}
					
				</div>
			</div>
			{{if typeof promotionPrd.message != undefined }}
				<div class="product-block-incentive">${promotionPrd.message}</div>
			{{/if}}
			<div class="product-block-border"></div>
		</div>
	</div> 
</script>
<script id="collectionProductTemplate" type="text/x-jquery-tmpl"> 


	<div class="col-md-3 col-no-padding product-block-padding">
		<div class="product-block product-block-unselected no-select">
			<div class="product-flag"></div>
			<div class="product-choose">
			  
			</div>
			<div class="product-selection">

				<div class="selection-checkbox-off"></div>
				<div class="selection-text-off">label select</div>
			</div>
			<div class="product-block-image-container">
				<div class="product-block-image">
					<div class="product-image">
						<a  class="bnsImgBlock recently-image-blur" href="${collectionUrl}">
							<img class="product-block-img" src="${primaryImage}" alt="product Image">
						</a>
						
					</div>
				</div>
			</div>
			<div class="colors">
				<div class="color-block red color-selected">
					<div class="color-block-inner">

					</div>
				</div>
				<div class="color-block yellow">
					<div class="color-block-inner">

					</div>
				</div>
				<div class="color-block orange">
					<div class="color-block-inner">

					</div>
				</div>
			</div>
			<div class="wish-list-heart-block">
				<div class="wish-list-heart-off"></div>
			</div>
			
			<div class="product-block-information">
				<div class="product-name">
					<a href="${collectionUrl}">${displayName}</a>
				</div>
				
				<div class="product-block-price">
					${collectionprice}		
				</div>
			</div>
			<div class="product-block-border"></div>
		</div>
	</div> 
</script>
</dsp:page>