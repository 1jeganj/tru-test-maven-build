<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<%-- <dsp:getvalueof  param="productInfo" var="productInfo"/> --%>
<dsp:getvalueof  param="productInfo.defaultSKU.safetyWarning" var="safetyWarnings"/>
<c:if test="${not empty safetyWarnings }">
	<h2><fmt:message key="tru.productdetail.label.saftywarning"/></h2>
				<ul>
					<c:forEach items="${safetyWarnings}" var="safetyWarning">
                            <li>
                            <span><dsp:valueof value="${safetyWarning}" valueishtml="true"></dsp:valueof>
                            </span>
                            </li>
                            <!-- <li><span>Elegant gown with cape</span>
                            </li>
                            <li><span>Multicolored bodice</span>
                            </li>
                            <li><span>Skirt</span>
                            </li>
                            <li><span>Accessories: black boots and a pink tiara</span>
                            </li> -->
                       </c:forEach>
                    </ul>
      </c:if>
</dsp:page>