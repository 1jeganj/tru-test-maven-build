<dsp:page>
<dsp:importbean bean="/atg/multisite/Site"/>
<dsp:getvalueof param="productIds" var="productIds"></dsp:getvalueof>
<dsp:getvalueof param="onlinePID" var="onlinePID"></dsp:getvalueof>
<dsp:getvalueof param="collectionId" var="collectionId"></dsp:getvalueof>
<dsp:getvalueof var="siteMaxAllowedRecentlyViewedItems" bean="Site.siteMaxAllowedRecentlyViewedItems" />
<div id="pdpInventoryCheckJSON">
<dsp:droplet name="/com/tru/commerce/droplet/TRUDisplayProductInfoDroplet">
<dsp:param name="productIds" param="productIds"/>
<dsp:param bean="/atg/multisite/Site" name="site"/>
<dsp:oparam name="output">
<dsp:getvalueof var="combineStringMapJson" param="combineStringMapJson"></dsp:getvalueof>
	${combineStringMapJson}
</dsp:oparam>
</dsp:droplet>
</div>
<div id="addingPDPForRecentlyViewed">
<dsp:droplet name="/com/tru/commerce/droplet/TRURecentlyViewedHistoryCollectorDroplet">
		<dsp:param name="profile" bean="/atg/userprofiling/Profile" />
		<dsp:param name="productInfo" param="productIds" />
			<dsp:param name="onlinePID" param="onlinePID" />
			<dsp:param name="collectionId" param="collectionId" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="recentlyViewedItems" param="recentViewedItemsParam" />
				<input type="hidden" id="recentlyViewedMaxLimit" value="${siteMaxAllowedRecentlyViewedItems}"/>
		</dsp:oparam>

	</dsp:droplet>
	
	<script>
	$(document).ready(function(){
		createLocalStorageDataRecentlyViewed(${recentlyViewedItems});
	});
		
	</script>
</div>
	
</dsp:page>