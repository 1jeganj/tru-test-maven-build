<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
		<div class="pdp-modal1">
					<div class="close-btn">
						<img src="${TRUImagePath}images/close.png" data-dismiss="modal" alt="close modal">
					</div>
					<div class="wc3ValidationHeaderAddToRegistry">
					<p><fmt:message key="tru.productdetail.label.pdpregistrydown" /></p>
						<!-- <p>The Items cannot be added					
						Please try again later !</p> -->
					</div>
		</div>
</dsp:page>