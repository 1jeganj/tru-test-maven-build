<dsp:page>
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale" />
	
	<c:forEach var="element" items="${contentItem.MainContent}">
		<c:if test="${element.name eq 'TRUPDPCMSZone'}">
			<dsp:getvalueof var="componentPath"	value="${element.componentPath}" />
			<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters${componentPath}"/>
			<dsp:getvalueof var="cacheKey" value="${atgTargeterpath}${siteId}${locale}" />

			<dsp:droplet name="/com/tru/cache/TRUPdpCmsZoneCacheDroplet">
				<dsp:param name="key" value="${cacheKey}" />
				<dsp:oparam name="output">
					<dsp:droplet name="/atg/targeting/TargetingForEach">
						<dsp:param name="targeter" bean="${atgTargeterpath}" />
						<dsp:oparam name="output">
							<dsp:valueof param="element.data" valueishtml="true" />
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</c:if>
		
		
	</c:forEach>
	
</dsp:page>