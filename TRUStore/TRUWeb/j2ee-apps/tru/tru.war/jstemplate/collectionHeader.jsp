<dsp:page>
<dsp:getvalueof param="collectionProductInfo" var="collectionProductInfo"/>
<div class="fade-to-black" id="product-disabled-modal"></div>
<div class="row">
            <div class="col-md-12">
		<!-- <hr class="collection-hr"> -->
			 <dsp:include page="/cartridges/TRUPDPBreadcrumbs/TRUPDPBreadcrumbs.jsp"> 
			 <dsp:param name="collectionPage" value="collectionPage"/>
			 <dsp:param name="collectionId" value="${collectionProductInfo.collectionId}"/>
			 <dsp:param name="productDisplayName" value="${collectionProductInfo.collectionName}"/>
				</dsp:include>
                </div>
            </div>
</dsp:page>	