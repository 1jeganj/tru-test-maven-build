<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:getvalueof var="pdpRegistryUrl" bean="TRUStoreConfiguration.pdpChecklistUrl" />	
<dsp:getvalueof var="pdpWishlistUrl" bean="TRUStoreConfiguration.viewWishListUrl" />
<dsp:getvalueof var="registryFlag" bean="TRUStoreConfiguration.registryFlag" />
<dsp:getvalueof var="wishlistFlag" bean="TRUStoreConfiguration.wishlistFlag" />
<dsp:getvalueof var="toysRUsSiteId" bean="TRUStoreConfiguration.toysRUsSiteId" />
<dsp:getvalueof var="babiesRUsSiteId" bean="TRUStoreConfiguration.babiesRUsSiteId" />
<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
<input id="backTo_pdpRegistryUrl" value="${pdpRegistryUrl}" type="hidden" />
<input id="backTo_pdpWishlistUrl" value="${pdpWishlistUrl}" type="hidden" />
<input id="backTo_toysRUsSiteId" value="${toysRUsSiteId}" type="hidden" />
<input id="backTo_babiesRUsSiteId" value="${babiesRUsSiteId}" type="hidden" />
<input id="backTo_siteId" value="${siteId}" type="hidden" />
<input id="backTo_TRUImagePath" value="${TRUImagePath}" type="hidden" />

<!-- <div class="backToRegistryOrWishlist"></div> -->

  <!--START Based on conditions need to show the  back to Registry/back to wishlist -->
	<%-- <c:if test="${not empty registryFlag ||  not empty wishlistFlag}">
	<c:if test="${registryFlag eq true}">
	<div class="col-md-4">
		<div class="back-to-wishlist">
			<a href="${pdpRegistryUrl}"><img
				src="${TRUImagePath}images/breadcrumb-left-arrow.png"> back to
				Registry</a>
		</div>
	</div>
	</c:if>
	<c:if test="${wishlistFlag eq true}">
	<div class="col-md-4">
		<div class="back-to-wishlist">
			<a href="${pdpWishlistUrl}"><img
				src="${TRUImagePath}images/breadcrumb-left-arrow.png"> back to
				wishlist</a>
		</div>
	</div>
	</c:if>
	</c:if> --%>
	<!-- END Based on conditions need to add the back to Registry/back to wishlist -->
	
</dsp:page>