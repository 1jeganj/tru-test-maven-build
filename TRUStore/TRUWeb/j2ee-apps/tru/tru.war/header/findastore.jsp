<%-- This page is used to display Store Details in popup. This page accepts storeId as input parameter and
	 uses StoreLookupDroplet to display Store Details. 
	--%>
	<dsp:page>
		<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
		<dsp:importbean bean="/atg/commerce/locations/StoreLookupDroplet" />
		<dsp:importbean bean="/com/tru/commerce/locations/TRUMyStoreDroplet" />

		<dsp:getvalueof id="contextPath" bean="/OriginatingRequest.contextPath" />

		<li class="your-store store-locator-tooltip" data-original-title="" title="">
			<span>
				<a href="javascript:void(0)"> <span><em class='sprite sprite-find-store'></em><span class='label-text'><fmt:message key="tru.header.findAStore"/></span></span></a>
			</span>
		</li>
	</dsp:page>