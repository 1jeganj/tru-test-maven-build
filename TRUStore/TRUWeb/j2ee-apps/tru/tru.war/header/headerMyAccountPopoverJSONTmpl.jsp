<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/atg/multisite/Site"/>
<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id" />
<dsp:getvalueof bean="Site.olsonLearnMoreURL" var="olsonLearnMoreURL"/>

<%@ page isELIgnored ="true" %>
	<script type='text/x-jquery-tmpl' id="jsonPopoverForGuestUser">
		<div class="arrow"></div>
		<div class="my-account-logged-out">
			<div class="my-account-sign-in">
				<div><a href="${jsFullyQualifiedcontextPathUrl}myaccount/myAccount.jsp" class="link-no-underLine"><fmt:message key="myaccount.signin" /></a></div>
				<div>
					<p class="header-register-link">
						<a href="${jsFullyQualifiedcontextPathUrl}myaccount/myAccount.jsp" data-ab-link="My-Account"><fmt:message key="myaccount.register" /></a> <img src="${jsTRUImagePath}images/breadcrumb-right-arrow-small.png" alt="breadcrumb_right_arrow_small_img">
					</p>
					<p><fmt:message key="myaccount.for.account" /></p>
				</div>
			</div>
			<hr>
			<div>
				<p class="manage-account"><a href="${jsFullyQualifiedcontextPathUrl}myaccount/myAccount.jsp" data-ab-link="My-Account"><fmt:message key="myaccount.manage.account" /></a></p>
				<p class="your-order-history"><a href="${jsFullyQualifiedcontextPathUrl}myaccount/myAccount.jsp" data-ab-link="My-Account"><fmt:message key="myaccount.order.history" /></a></p>
			</div>
			<hr>
			<div>
				<img src="${jsTRUImagePath}images/rewards-logo.png" alt="rewards_logo" title="rewards_logo" aria-label="rewards_logo">
				<p><fmt:message key="myaccount.earn.rewards.for.points" /></p>
				<p>
					<a href="/cobrand/services/rewards-r-us" class="" target="_blank">
						<fmt:message key="myaccount.learn.more" />&nbsp;<img src="${jsTRUImagePath}images/breadcrumb-right-arrow-small.png"	alt="breadcrumb_right_arrow_small_img">
					</a>
				</p>
			</div>
		</div>
	</script>

	<script type='text/x-jquery-tmpl' id="jsonPopoverForLoggedInUser">
		<div class="arrow"></div>
		<div class="my-account-logged-in">
		<div>
			<p class="manage-account"><a href="${jsFullyQualifiedcontextPathUrl}myaccount/myAccount.jsp"><fmt:message key="myaccount.manage.account" /></a></p>
			<p class="your-order-history"><a href="${jsFullyQualifiedcontextPathUrl}myaccount/orderHistory.jsp"><fmt:message key="myaccount.order.history" /></a></p>
		</div>
		<hr>
		
		<div>
			<div id="headerLogoutLink"><fmt:message key="myaccount.signout" /></div>
		</div>
		</div>
	</script>
</dsp:page>