<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/atg/dynamo/Configuration" var="Configuration"/>
<dsp:importbean	bean="/atg/registry/RepositoryTargeters/TRU/Header/HeaderHelpTargeter" />
<dsp:getvalueof var="contextPath" value="${originatingRequest.contextPath}"/>
<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id" />
<dsp:importbean bean="/atg/multisite/Site"/>
<dsp:importbean bean="/com/tru/droplet/TRUGetSiteDroplet" />
<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations" />
<dsp:importbean bean="/atg/endeca/assembler/cartridge/manager/AssemblerSettings" />
<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale" />
<dsp:getvalueof param="siteURL" var="siteURL"/>
<dsp:getvalueof var="bruSiteCheck" bean="EndecaConfigurations.bruSiteCheck"/>
<dsp:getvalueof var="previewEnabled" bean="AssemblerSettings.previewEnabled"/>
<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
	<nav class="navbar navbar-bottom">
	<input type="hidden" value="${contextPath}" name="contextPath" class="contextPath" />
	
	<!-- START::Added for making the Ajax calls contain Absolute urls-->
	<dsp:droplet name="TRUGetSiteDroplet">
		<dsp:param name="siteId" value="${siteName}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="siteUrl" param="siteUrl"/> 
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
		<dsp:oparam name="output">
		<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
		</dsp:oparam>
	</dsp:droplet>
	
<dsp:getvalueof var="urlFullContextPath" value="${scheme}${siteUrl}${contextPath}"/>
	<input type="hidden" value="${scheme}${siteUrl}${contextPath}" name="urlFullContextPath" class="urlFullContextPath" />
	<!-- END::Added for making the Ajax calls contain Absolute urls-->
	    <div class="navbar-tru container-fluid">
	        <div class="nav navbar-nav navbar-leftnav">
       			<c:choose>
        			<c:when test="${siteName eq 'ToysRUs'}">
            			<a href="${siteURL}${contextPath}" data-ab-link="TRU-Logo"><div id="logo-swap-lg-tru">Tru_BRU_logo_swapping</div></a>
            		</c:when>
            		<c:otherwise>
            		<c:if test="${not empty siteURL and bruSiteCheck eq true and previewEnabled ne true and fn:contains(siteURL,'.toysrus.com') and fn:indexOf(siteURL,'aos')==-1}">
						<dsp:getvalueof var="siteURL" value="${fn:replace(siteURL,'.toysrus.com','.babiesrus.com')}"/>
					</c:if>	
            			<a href="${siteURL}${contextPath}" data-ab-link="BRU-Logo"><div id="logo-swap-lg-bru"></div></a>
            		</c:otherwise>
           		</c:choose>
	        </div>


			<div class="navbar-middle">
			   <div class="navbar-left"></div>
			   <div id="exploreButton" class="navbar-left left-navbar">
			      <div>
			         <a onclick="tealiumMegaMenuClick()" href='javascript:void(0);' title="Shop By"> <em  class='sprite sprite-hamburger-icon'></em></a>
			         <p><fmt:message key="tru.header.shopBy"/></p>
			      </div>
			   </div>
			   <!-- ## ExploreBox -->
			   <!-- <div id="exploreBox" class="clearfix" style="transform-origin: 50% 50% 0px; transform: matrix(0, 0, 0, 0, 0, 0); left: 30px; top: 118px;"></div> -->
			   <dsp:include page="megamenu.jsp"/>
			   <div class="navbar-left"></div>
			   <div class="navbar-search left-navbar">
			      <dsp:include page="/common/frag/searchBox.jsp">
			      <dsp:param name="urlFullContextPath" value="${urlFullContextPath}" />
			        </dsp:include>
			      
			      
			      
			      <img class="typeahead-arrow" src="${TRUImagePath}images/arrow.svg" alt="up arrow">
					<c:choose>
						<c:when test="${siteName eq 'ToysRUs'}">
							<div class="search-icon-tru"></div>
						</c:when>
						<c:otherwise>
							<div class="search-icon-bru"></div>
						</c:otherwise>
					</c:choose>
			   </div>
				</div>
				<div class="right-navbar" id="right-navbar">
				   <div class="navbar-right"></div>
				   <div class="navbar-right" id="sosMyAccount">
						   <div id="my-account-popover" data-toggle="popover" class="global-nav-my-account" data-original-title="" title="">
						   <a href="javascript:void(0)" title="My Account"><em  class='sprite sprite-account-icon '></em></a>
						   <div class="head_welcome_cont">
						      <dsp:include page="headWelcomeContent.jsp" />
						   </div>
						 </div>
				      <!-- start My account popup -->
				      <div id="my-account-popover-struct" class="my-account-popover-struct fade popover bottom in">
				         <dsp:include page="headerMyAccountPopover.jsp" />
				      </div>
				      <dsp:include page="headerMyAccountPopoverJSONTmpl.jsp"/>
				      <!-- End My account popup -->
				   </div>
				   <div class="navbar-right"></div>
				   <div class="navbar-right">
				      <div class="btn-group we-can-help">
				         <div class="btn btn-default dropdown-toggle">
				            <a href="/cobrand/help/online-help" title="Help"><em class='sprite sprite-help-icon'></em></a>
				            <div>help</div>
				         </div>
				         <c:choose>
							<c:when test="${site eq 'sos' and isSosVar}">
								<dsp:getvalueof var="atgTargeterpath1" value="/atg/registry/RepositoryTargeters/TRU/Header/SOSHeaderHelpTargeter" />
	                            <dsp:getvalueof var="cacheKey" 	value="${atgTargeterpath1}${siteName}${locale}" />
					            <dsp:droplet name="/com/tru/cache/TRUSOSHeaderHelpTargeterCacheDroplet">
						       <dsp:param name="key" value="${cacheKey}" />
						       <dsp:oparam name="output">
							   <dsp:droplet name="/atg/targeting/TargetingFirst">
								<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/Header/SOSHeaderHelpTargeter" />
								<dsp:oparam name="output">
									<dsp:valueof param="element.data" valueishtml="true" />
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>
							</c:when>
							<c:otherwise>
					<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters/TRU/Header/HeaderHelpTargeter" />
	                <dsp:getvalueof var="cacheKey" 	value="${atgTargeterpath}${siteName}${locale}" />
					<dsp:droplet
						name="/com/tru/cache/TRUHeaderHelpTargeterCacheDroplet">
						<dsp:param name="key" value="${cacheKey}" />
						<dsp:oparam name="output">
							<dsp:droplet name="/atg/targeting/TargetingFirst">
								<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/Header/HeaderHelpTargeter" />
								<dsp:oparam name="output">
									<dsp:valueof param="element.data" valueishtml="true" />
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>
						</c:otherwise>
					     </c:choose>
				      </div>
				   </div>
				   <div class="navbar-right"></div>
				   <div class="navbar-right" id="minicart">
				      <dsp:include page="/cart/mini_cart.jsp" />
				   </div>
				</div>
		</div>
	</nav>
	<%-- <script>
	$(document).ready(function(){
			 var isSOS = $.cookie("isSOS");
			 var sites = '<c:out value="${site}"/>';
			if(typeof isSOS != 'undefined' && isSOS != null && isSOS == 'true'){
				$(".header-part").addClass("SOSHeader");
				$("#sosMyAccount").hide();
			}else{
				$("#sosMyAccount").show();
			} 
		}); 
	</script>	 --%>
	
</dsp:page>