<!-- This page fragment used to render Mega Menu contents -->
<dsp:page>
	<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/multisite/Site"/>
	<dsp:importbean bean="/atg/targeting/TargetingFirst"/>
	<dsp:importbean bean="/atg/registry/RepositoryTargeters/TRU/HOMEPAGE/MegaMenuTargeter"/>
	<dsp:importbean bean="/atg/registry/RepositoryTargeters/TRU/HOMEPAGE/BruMegaMenuTargeter"/>
	<dsp:getvalueof var="requestURI" value="${originatingRequest.RequestURI}"/>
	<dsp:getvalueof var="requestUrl" bean="/OriginatingRequest.requestURL"/>
	<dsp:getvalueof var="domainPath" bean="TRUConfiguration.domainUrl"/>
	<dsp:getvalueof var="shopLocalURL" bean="TRUConfiguration.shopLocalURL"/>
	<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id"/>
	<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale"/>
	<dsp:getvalueof param="siteURL" var="siteURL"/>
	<c:choose>
		<c:when test="${fn:containsIgnoreCase(requestUrl, shopLocalURL)}">
			<c:set var="domainUrl" value="${domainPath}"/>
		</c:when>
		<c:otherwise>
			<c:set var="domainUrl" value=""/>
		</c:otherwise>
	</c:choose>

	<div id="overlay" class="clearfix"></div>
	<div id="exploreBox" class="clearfix noRecentlyViewedProducts">

		<script>
			// TUREP-2204 DL: shop by menu tracking
	        function megaMenuClick(){
	                localStorage.setItem("pageFrom",'megaMenuClick');
	        }
		</script>

		<div id="shopByContainer">

			<div id="megamenu">

				<div id="shortCut">
					<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters/TRU/HOMEPAGE/MegaMenuTargeter"/>
					<dsp:getvalueof var="cacheKey" value="${atgTargeterpath}${siteName}${locale}"/>
					<dsp:droplet name="/com/tru/cache/TRUMegaMenuTargeterCacheDroplet">
						<dsp:param name="key" value="${cacheKey}"/>
						<dsp:oparam name="output">
							<dsp:droplet name="TargetingFirst">
								<dsp:param name="targeter" bean="${atgTargeterpath}"/>
								<dsp:oparam name="output">
									<dsp:valueof param="element.data" valueishtml="true"/>
								</dsp:oparam>
								<dsp:oparam name="empty">
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>
				</div>

				<div id="categoryLevel1">
					<dsp:droplet name="/com/tru/commerce/droplet/MegaMenuDroplet">
						<dsp:param name="catalogId" bean="Site.defaultCatalog.id"/>
						<dsp:param name="siteId" bean="Site.id"/>
						<dsp:param name="elementName" value="rootCategories"/>
						<dsp:oparam name="output">
							<dsp:getvalueof param="remainingCategories" var="remainingCategories"/>
							<dsp:getvalueof param="displaySeeMoreCategory" var="displaySeeMoreCategory"/>

							<ul class="level1">
									<%-- Iterating level one categories --%>
								<dsp:droplet name="ForEach">
									<dsp:param name="array" param="rootCategories"/>
									<dsp:param name="elementName" value="rootCategory"/>
									<dsp:oparam name="output">
										<dsp:getvalueof param="count" var="counter"/>
										<dsp:getvalueof param="rootCategory.categoryName" var="rootCategoryName"/>
										<dsp:getvalueof param="rootCategory.categoryURL" var="rootCategoryURL"/>
										<dsp:getvalueof param="rootCategory.categoryImage" var="categoryImage"/>
										<li>
											<h3 onclick="megaMenuClick();" data-url="${siteURL}${rootCategoryURL}&catdim=bcmb">
												<c:set var="arrayofmsg" value="${fn:split(rootCategoryName,' ')}"/>
												<c:forEach items="${arrayofmsg}" varStatus="loop">
													<c:choose>
														<c:when test="${loop.index == 0}">
															<span>${arrayofmsg[loop.index]}</span>
														</c:when>
														<c:otherwise>
															${arrayofmsg[loop.index]} ${' '}
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</h3>

											<dsp:oparam name="outputStart">
												<dsp:getvalueof param="count" var="counter2"/>
												<div class="mega-menu-categories-ad">
													<dsp:valueof param="rootCategory.categoryImage" valueishtml="true"/>
												</div>
											</dsp:oparam>


											<ul class="level2">
												<li>
													<h3 onclick="megaMenuClick();" data-url="${siteURL}${rootCategoryURL}&catdim=bcmb">All</h3>
												</li>
													<%-- Iterating level one categories --%>
												<dsp:droplet name="ForEach">
													<dsp:param name="array" param="rootCategory.subCategories"/>
													<dsp:param name="elementName" value="levelOneCategory"/>
													<dsp:oparam name="output">
                                                        <dsp:getvalueof value="${fn:replace(rootCategoryName,' ','-')}" var="rootCategoryName"/>
                                                        <dsp:getvalueof value="${fn:replace(rootCategoryName,'&','n')}" var="rootCategoryName"/>
                                                        <dsp:getvalueof value="${fn:replace(rootCategoryName,',','')}" var="rootCategoryName"/>
                                                        <dsp:getvalueof value="${fn:replace(rootCategoryName,'\\\'','')}" var="rootCategoryName"/>
                                                        <dsp:getvalueof value="${fn:escapeXml(rootCategoryName)}" var="rootCategoryName"/>
														<dsp:getvalueof param="levelOneCategory.categoryURL" var="levelOneCategoryURL"/>
														<li>
                                                            <dsp:getvalueof param="levelOneCategory.categoryName" var="catIName"/>
                                                            <dsp:getvalueof value="${fn:replace(catIName,' ','-')}" var="catIName"/>
                                                            <dsp:getvalueof value="${fn:replace(catIName,'&','n')}" var="catIName"/>
                                                            <dsp:getvalueof value="${fn:replace(catIName,',','')}" var="catIName"/>
                                                            <dsp:getvalueof value="${fn:replace(catIName,'\\\'','')}" var="catIName"/>
                                                            <dsp:getvalueof value="${fn:escapeXml(catIIName)}" var="catIName"/>
															<h3 onclick="megaMenuClick();" data-url="${siteURL}${levelOneCategoryURL}&catdim=bcmb"  data-ab-link="${rootCategoryName}:${catIName}">
																<dsp:valueof param="levelOneCategory.categoryName"/>
															</h3>

															<ul class="level3">

																<li>
																	<h3 onclick="megaMenuClick();" data-url="${siteURL}${levelOneCategoryURL}&catdim=bcmb">All</h3>
																</li>
																	<%-- Iterating level two categories --%>
																<dsp:droplet name="ForEach">
																	<dsp:param name="array" param="levelOneCategory.subCategories"/>
																	<dsp:param name="elementName" value="levelTwoCategory"/>
																	<dsp:oparam name="output">
																		<dsp:getvalueof param="levelTwoCategory.categoryURL" var="levelTwoCategoryURL"/>
																		<li>
                                                                            <dsp:getvalueof param="levelTwoCategory.categoryName" var="catIIName"/>
                                                                            <dsp:getvalueof value="${fn:replace(catIIName,' ','-')}" var="catIIName"/>
                                                                            <dsp:getvalueof value="${fn:replace(catIIName,'&','n')}" var="catIIName"/>
                                                                            <dsp:getvalueof value="${fn:replace(catIIName,',','')}" var="catIIName"/>
                                                                            <dsp:getvalueof value="${fn:replace(catIIName,'\\\'','')}" var="catIIName"/>
                                                                            <dsp:getvalueof value="${fn:escapeXml(catIIName)}" var="catIIName"/>

                                                                            <h3 onclick="megaMenuClick();" data-url="${siteURL}${levelTwoCategoryURL}&catdim=bcmb" data-ab-link="${rootCategoryName}:${catIIName}">
																				<dsp:valueof param="levelTwoCategory.categoryName"/>
																			</h3>
																		</li>
																	</dsp:oparam>
																</dsp:droplet>
															</ul>
														</li>
													</dsp:oparam>
												</dsp:droplet>
											</ul>
										</li>
									</dsp:oparam>
								</dsp:droplet>
							</ul>
						</dsp:oparam>
					</dsp:droplet>
				</div>

				<div id="categoryLevel2"></div>

				<div id="categoryLevel3"></div>

			</div>
				<%-- id="megamenu"--%>
		</div>
			<%-- id="shopByContainer"--%>
	</div>
	<%-- id="exploreBox"--%>

</dsp:page> <%-- end page --%>