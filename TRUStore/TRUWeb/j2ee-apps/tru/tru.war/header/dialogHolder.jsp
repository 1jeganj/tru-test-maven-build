<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	  <div class="modal fade" id="mystoreSuccessModal" tabindex="-1" role="dialog"
			aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog modal-md">
				<div class="modal-content sharp-border">
					<div class="storeLocatormystore">
							<div class="mystore-selected-msg">
								 <a href="javascript:void(0)" data-dismiss="modal" ><span class="sprite-icon-x-close" id="close-email-popup"></span></a>
                                  <h2><!-- Your Selected Store Has Been Saved!--><fmt:message key="tru.storelocator.label.selectedstoremsg" /></h2>
							</div>
							 <div class="mystore-content">
								<p><!--As you browse the website, you'll see products available for shipping to your home as well as available for Free Store Pickup in the  --><fmt:message key="tru.storelocator.label.selectedstoremsg1" /> <span class="mystore-city"></span> <!--Store.--><fmt:message key="tru.storelocator.label.selectedstore" /></p>
								<span> <!-- You can change your store anytime by selecting a new location at the top of any page or on the Store Locator page.  --><fmt:message key="tru.storelocator.label.selectedstoremsg2" /></span>
							</div>	
				   </div>
				</div>
			</div>
		</div>	
</dsp:page>


                             