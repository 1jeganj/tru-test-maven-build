<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>

<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/multisite/Site"/>
<dsp:getvalueof bean="Site.olsonSignUpURL" var="olsonSignUpURL"/>
<dsp:getvalueof bean="Site.olsonLearnMoreURL" var="olsonLearnMoreURL"/>
<dsp:getvalueof bean="Profile.rewardNo" var="rewardNumber"></dsp:getvalueof>
<c:choose>
	<c:when test="${empty rewardNumber}">
		<c:set var="rewardNumPresent" value="false"></c:set>
	</c:when>
	<c:otherwise>
		<c:set var="rewardNumPresent" value="true"></c:set>
	</c:otherwise>
</c:choose>
	    <div class="membership-number <c:if test="${rewardNumPresent eq false}">hide-data</c:if>" id="membership-number">
	        <img src="${TRUImagePath}images/rewards-logo.png" alt="rewards logo">
	
	        <div>
	            <p><fmt:message key="myaccount.membership.number" /></p>
	            <p>${rewardNumber}</p>
	            <div>
	                <a href="#" onclick="updateRewardsLink();"><fmt:message key="myaccount.update" /></a><span>&#xB7;</span><a href="${olsonSignUpURL}" class="" target="_blank"><fmt:message key="myaccount.see.my.rewards" /></a>
	            </div>
	        </div>
	    </div>
		<div class="membership-id <c:if test="${rewardNumPresent eq true}">hide-data</c:if>" id="membership-id">
			<img src="${TRUImagePath}images/rewards-logo.png" alt="rewards logo">
			<p>
				<a href="/cobrand/services/rewards-r-us" class="" target="_blank"><fmt:message key="myaccount.learn.more" /> <img src="${TRUImagePath}images/breadcrumb-right-arrow-small.png"
					alt="right arrow"></a>
			</p>
			<div class="header-rewards-zone">
				<p id="membership-id-slide" class="enter-membership">
				<fmt:message key="myaccount.enter.membership.id" /><img src="${TRUImagePath}images/plus-sm.png"
					alt="plus icon">
				</p>
				<div style="display: none;">
					<div class="membership-input-group">
						<form name="headerMembershipForm" id="headerMembershipForm" class="JSValidation" onsubmit="javascript: return AddUpdateRewardsFromHeader();">
							<p class="global-error-display"></p>
							<div class="enterMemberIDFromHeaderWrapper"><input type="text" id="enterMemberIDFromHeader" maxlength="13" name="enterMembershipID" value="${rewardNumber}" onkeypress="return isNumberOnly(event,14)" class="chkNumbersOnly"/></div>
							<input type="submit" value="save" id="saveMemberIDFromHeader" style="display: none;"/>
							<button><fmt:message key="myaccount.go" /></button>
						</form>
					</div>
					<img class="membership-image"
						src="${TRUImagePath}images/rewards-card-help.png" alt="rewards card">
					<div class="inline membership-help-text"><fmt:message key="myaccount.membership.id.back.card" /></div>
				</div>
			</div>
		</div>