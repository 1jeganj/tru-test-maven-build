<dsp:page>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>
<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
<dsp:getvalueof var="contextPath" value="${originatingRequest.contextPath}"/> 
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
<c:if test="${siteName eq babySiteCode}">
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
</c:if>
<dsp:getvalueof var="customerEmail" bean="Profile.login"/>
<dsp:getvalueof var="customerID" bean="Profile.id"/>

<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<fmt:message key="myaccount.welcome.head" var="welcomeMessage"/>
<fmt:message key="myaccount.myaccount" var="myAccountName" />

<span id="VisitorFirstNamecookieDIV"></span>

<input type="hidden" id="welcomeName" value="${welcomeMessage}" />
<input type="hidden" id="myAccount" value="${myAccountName}" />
<input type="hidden" id="customerEmailSignIn" value="${customerEmail}" />
<input type="hidden" id="customerIDSignIn" value="${customerID}" />
<input type="hidden" id="partnerNameSignIn" value="${partnerName}" />

</dsp:page>