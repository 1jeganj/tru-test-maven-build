<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof var="sortUrlA" param="sortUrlA" />
<dsp:getvalueof var="primaryImageThumbnailBlock" param="primaryImageThumbnailBlock" />
	
	<c:if test="${not empty sortUrlA }">
		<input type="hidden" class="thumbBlock" value="${primaryImageThumbnailBlock}" />
	</c:if>
	
	<div id="recentlyViewedProducts">
		<div id="recentProducts">
			<div id="recentProductsTSE" class="tse-scrollable">
				<div class="tse-content" id="tse-content">
					<div class="recently-viewed-header" id="recently-viewed-header">
						<img src="${TRUImagePath}images/clock-icon.png" alt="clock-icon">
						<h3 class="w3ValidationRecentViewHeading"><fmt:message key="tru.megaMenu.recentlyView"/></h3>
						<span>·</span> <a href="javascript:void(0)" id="clearRecentlyViewedItems"><fmt:message key="tru.megaMenu.clear"/></a>
					</div>
					<span id="recentlyViewedCookie"></span>
				</div>
			</div>
			<div class="fadeout recentproducts"></div>
		</div>
	</div>
	<script>
		$(document).ready(function(){
			if(typeof populateRecentlyViewedItems != 'undefined'){
				populateRecentlyViewedItems('megamenu');	
			}
		});
		
	</script>

<%@ page isELIgnored ="true" %>
<script>

var recentlyviewImageTumb = $(".thumbBlock").val();

</script>
<script id="recentlyViewedTemplate" type="text/x-jquery-tmpl">
	{{if typeof productType != 'undefined' && productType == "Collection"}}
		{{tmpl($data) "#recentlyViewedCollectionTemplate"}}
	{{else}}
		{{tmpl($data) "#recentlyViewedPDPTemplate"}}
	{{/if}}
</script>
<script id="recentlyViewedPDPTemplate" type="text/x-jquery-tmpl">
	<div class="explore-recently-viewed-single">
		<div class="row">
			<div class="col-md-2">
				<img src="${primaryImage}${recentlyviewImageTumb}" alt="">
			</div>
			<div class="col-md-7">
				<div>
					<div class="star-rating-block">
						{{each(i,t) [1,2,3,4,5]}}
							{{if i + 1 > reviewRating}}
								<div class="star-rating-off"></div>
							{{else}}
								<div class="star-rating-on"></div>
							{{/if}}
						{{/each}}
					</div>
				</div>
				<div class="recently-viewed-details">
					<a href="${pdpUrl}">${displayName.replace("*$","'")}</a>
				{{if suggestedAgeMin}}
					<p>Ages ${suggestedAgeMin}.</p>
				{{/if}}
				</div>
				{{if savedPercentage}}
					<div class="recently-viewed-price">
						<span class="crossed-out">$${listPrice}</span>
						<span class="sale-price">$${salePrice}</span>
						<p>you save &nbsp;$${savedAmount} (${savedPercentage}%)</p>
					</div>
				{{else}}
					<div class="prices">
						<span class="sale-price">{{if salePrice == 0}}$${listPrice}{{else}}$${salePrice}{{/if}}</span>
					</div>
				{{/if}}
			</div>
		</div>
	</div>
</script>
<script id="recentlyViewedCollectionTemplate" type="text/x-jquery-tmpl">
	<div class="explore-recently-viewed-single">
		<div class="row">
			<div class="col-md-2">
				<img src="${primaryImage}${recentlyviewImageTumb}" alt="">
			</div>
			<div class="col-md-7">
				<div class="recently-viewed-details">
					<a href="${collectionUrl}">${displayName.replace("*$","'")}</a>
				</div>


				<div class="prices">
					${collectionprice}
				</div>
			</div>
		</div>
	</div>
</script>
</dsp:page>
