<dsp:importbean bean="/com/tru/commerce/droplet/TRUPriceDroplet" />
<dsp:page>
	<dsp:droplet name="TRUPriceDroplet">
		<dsp:param name="skuId" param="skuId" />
		<dsp:param name="productId" param="productId" />
		<dsp:oparam name="true">
			<div class="recently-viewed-price">
				<span class="crossed-out">$<dsp:valueof param="listPrice" /></span><span class="sale-price">$<dsp:valueof param="salePrice" /></span>
				<p>
					you save $<dsp:valueof param="savedAmount" /> (<dsp:valueof param="savedPercentage" />%)
				</p>
			</div>
		</dsp:oparam>
		<dsp:oparam name="false">
			<div class="prices">
				<dsp:getvalueof var="salePrice" param="salePrice" />
				<c:choose>
					<c:when test="${salePrice == 0}">
						<span class="sale-price"> $<dsp:valueof param="listPrice" /></span>
					</c:when>
					<c:otherwise>
						<span class="sale-price"> $<dsp:valueof param="salePrice" /></span>
					</c:otherwise>
				</c:choose>
			</div>
		</dsp:oparam>
		<dsp:oparam name="empty">
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>