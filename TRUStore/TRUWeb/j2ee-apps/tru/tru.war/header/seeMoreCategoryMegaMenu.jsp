<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Range"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/multisite/Site"/>
	<dsp:getvalueof var="requestURI" value="${originatingRequest.RequestURI}"/>
    <dsp:getvalueof param="remainingCategories" var="remainingCategories"/>
     <dsp:getvalueof param="siteURL" var="siteURL"/>
	<dsp:getvalueof param="counter"  var="counter"/>
								<li id="menu-${counter+1}" class="imidiateChild">
									<a href="javascript:void(0);" ><fmt:message key="tru.megaMenu.seeMoreCategory"/></a>
										<div class="row" id="${counter+1}">
											 <div class="tse-scrollable">
											
											<div class="tse-content">
											<div class="mega-menu-categories">
												<h3 aria-label="See More Categories" class="w3ValidationHeadings">
													<span class="blue-heading disableClick">
												<fmt:message key="tru.megaMenu.seeMoreCategories"/></span>
												</h3>
												<%-- We need to add this class (single-li) if we have only one record --%>
												<c:choose>
													<c:when test="${counter2 == 1}">
														<ul  class="level-1 single-li">
													</c:when>
													<c:otherwise>
														<ul class="level-1">
													</c:otherwise>
												</c:choose>
										<%-- Iterating level one categories --%>
												<dsp:droplet name="ForEach">
													<dsp:param name="array" value="${remainingCategories}"/>
													<dsp:param name="elementname" value="remainingCategories"/>
													<dsp:oparam name="output">
													<dsp:getvalueof param="count"  var="counter2"/>
													<dsp:getvalueof param="element" var="remainingEachCategory"/>
														<dsp:getvalueof value="${remainingEachCategory.categoryURL}" var="levelOneCategoryURL"/>
														<li>
															<span class="L1-heading"><a onclick="tealiumMegaMenuClick()" href="${siteURL}${levelOneCategoryURL}"><dsp:valueof value="${remainingEachCategory.categoryName}" /></a></span>
															<%-- Iterating level two categories --%>
																<dsp:droplet name="ForEach">
																	<dsp:param name="array" value="${remainingEachCategory.subCategories}"/>
																	<dsp:param name="remainingCategories" value="levelTwoCategory"/>
																			<ul class="level-2">
																	<dsp:oparam name="output">
																	<dsp:getvalueof param="element" var="levelTwoCategory"/>
																	<dsp:getvalueof value="${levelTwoCategory.categoryURL}" var="levelTwoCategoryURL"/>
																			<li>
																			<a onclick="tealiumMegaMenuClick();" href="${siteURL}${levelTwoCategoryURL}" ><dsp:valueof value="${levelTwoCategory.categoryName}" /></a> </li>
																	</dsp:oparam>
																	
																			</ul>
																	
																</dsp:droplet>
														</li>
														</dsp:oparam>
												</dsp:droplet>	
														</ul>										
												</div>
												<div class="mega-menu-categories-ad">
													<dsp:valueof param="rootCategory.categoryImage" valueishtml="true" />
														</div>
												</div>
												</div>
												<div class="fadeout recentproducts"></div>
											</div>
										</li>
                              </dsp:page>