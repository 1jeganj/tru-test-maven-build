<jsp:directive.page language="java" pageEncoding="utf-8" trimDirectiveWhitespaces="true" />
<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations" />
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
	<dsp:importbean bean="/com/tru/common/TRUConfiguration" />
	<dsp:importbean bean="com/tru/common/TRUStoreConfiguration" />
	<dsp:importbean bean="/atg/multisite/Site" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale" />
	<dsp:importbean bean="/com/tru/droplet/TRUContentLookupDroplet" />
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
	<dsp:getvalueof var="headerFooterPage" bean="EndecaConfigurations.headerPage" />
	
		
	
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:getvalueof var="header" param="header" />
	<dsp:getvalueof var="enableMinification"  bean="TRUStoreConfiguration.enableJSDebug"/>
	<dsp:getvalueof var="debugFlag" param="debug"/>
	<dsp:getvalueof var="fromNoSearch" param="fromNoSearch" />
	<dsp:getvalueof var="fromSubCat" param="fromSubCat" />
	<dsp:getvalueof var="fromsearch" param="fromsearch" />
	<dsp:getvalueof var="fromFamily" param="fromFamily" />
	<dsp:getvalueof var="removeGiftFinder" param="removeGiftFinder" />
	<dsp:getvalueof var="fromGiftFinder" param="fromGiftFinder" />
	<dsp:getvalueof var="fromShopBy" param="fromShopBy" />
	<dsp:getvalueof var="fromsiteMap" param="fromsiteMap" />
	<dsp:getvalueof var="fromCat" param="fromCat" />
	<dsp:getvalueof var="fromPdp" param="fromPdp" />
	<dsp:getvalueof var="fromHome" param="fromHome" />
	<dsp:getvalueof var="fromContactUs" param="fromContactUs" />
	<dsp:getvalueof var="fromCollection" param="fromCollection" />
	<dsp:getvalueof var="fromStatic" param="fromStatic" />
	<dsp:getvalueof var="srchDexurl" bean="TRUConfiguration.searchDexUrl" />
	<dsp:getvalueof var="enableSearchDex" bean="TRUConfiguration.enableSearchDex" />
	<dsp:getvalueof var="srchDexHeaderPage" bean="TRUConfiguration.searchDexHeaderPage" />
	<dsp:getvalueof var="googleStaticUrl" bean="TRUConfiguration.storeLocatorStaticGoogleMapURL"/>
	<dsp:getvalueof var="isFileExist" value="0" vartype="java.lang.Integer" />
	<dsp:getvalueof var="pageName" param="pageName" />
	<dsp:getvalueof var="staticAssetsVersionFormat"	bean="TRUStoreConfiguration.staticAssetsVersion" />
	<dsp:getvalueof var="versionFlag" bean="TRUStoreConfiguration.versioningFlag" />
	<dsp:getvalueof var="globalRadius" bean="Site.globalRadius" />
	<dsp:getvalueof var="lightBoxRadius" bean="Site.lightBoxRadius" />
	<dsp:getvalueof var="enableSeo" bean="Site.enableSeo" />
	<dsp:getvalueof var="displayGiftFinderTab" param="displayGiftFinderTab" />
	<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id" />
	<dsp:importbean bean="/com/tru/droplet/TRUGetSiteDroplet" />
	<dsp:getvalueof var="quesryString" bean="/OriginatingRequest.queryString" />
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	<dsp:getvalueof var="pdpRegistryUrl" bean="TRUStoreConfiguration.pdpRegistryUrl" />
	<dsp:importbean bean="/com/tru/droplet/TRUContentLookupDroplet" />	
	<dsp:getvalueof var="requesturi" value="${originatingRequest.requesturi}" />
	<dsp:getvalueof var="serverName" value="${originatingRequest.serverName}"/>
	<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
		<dsp:oparam name="output">
			<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="requestUrl" value="${scheme}${serverName}${requesturi}"/>
	<dsp:getvalueof var="persistDays" bean="Site.persistOrderDays" />


	
	<c:forEach var="element" items="${contentItem.MainContent}">
		<dsp:getvalueof var="lightBoxRadius" bean="Site.lightBoxRadius" />
		<c:if test="${element['@type'] eq 'TRUStaticPageMainContent'}">
			<dsp:getvalueof var="contentKey" value="${element.contentKey}" />
		</c:if>
	</c:forEach>

	<dsp:getvalueof var="catID" param="catID" />
	<dsp:getvalueof var="store" param="store" />
    <dsp:importbean bean="/atg/endeca/assembler/droplet/InvokeAssembler" />
	<dsp:getvalueof var="domainPath" bean="TRUConfiguration.domainUrl" />
	<dsp:getvalueof var="shopLocalURL" bean="TRUConfiguration.shopLocalURL" />
	<c:set var="hideGiftFinder" value="false" />
	<dsp:getvalueof var="giftFinderURL" bean="/atg/multisite/Site.giftFinderURL" />
	
	<dsp:getvalueof var="headerConfigurationJS" bean="TRUStoreConfiguration.headerConfigurationJS"/>
    <dsp:getvalueof var="headerConfigurationCSS" bean="TRUStoreConfiguration.headerConfigurationCSS"/>

	<c:choose>
		<c:when test="${siteName eq 'ToysRUs'}" >
			<dsp:getvalueof var="contextRoot" value="tru" />
		</c:when>
		<c:when test="${siteName eq 'BabyRUs'}" >
			<dsp:getvalueof var="contextRoot" value="bru" />
		</c:when>
	</c:choose>

	<c:choose>
		<c:when test="${fn:containsIgnoreCase(requestUrl, shopLocalURL)}">
			<!--Request is from ShopLocal -->
			<!--Request URL --- ${requestUrl}-->
			<c:set var="domainUrl" value="${domainPath}" />
			<c:set var="hideGiftFinder" value="true" />
		</c:when>
		<c:otherwise>
			<!--Request is not from ShopLocal -->
			<!--Request URL: ${requestUrl}-->
			<c:set var="domainUrl" value="" />
		</c:otherwise>
	</c:choose>

	<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
	<c:choose>
		<c:when test="${site eq 'sos' and isSosVar}">
			<dsp:getvalueof var="cacheKey" value="/InvokeAssembler${siteName}Header${site}${locale}" />
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="cacheKey" value="/InvokeAssembler${siteName}Header${locale}" />
		</c:otherwise>
	</c:choose>

	<dsp:droplet name="/com/tru/cache/TRUHomePageAssemblerCache">
		<dsp:param name="key" value="${cacheKey}" />
		<dsp:oparam name="output">
			<dsp:droplet name="InvokeAssembler">
				<dsp:param name="includePath" value="${headerFooterPage}" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="headContentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
				</dsp:oparam>
			</dsp:droplet>

			<c:if test="${not empty headContentItem}">
				<c:set var="headerContentItem" value="${headContentItem}" scope="application"/>
			</c:if>

		</dsp:oparam>
	</dsp:droplet>
	
	<dsp:droplet name="TRUGetSiteDroplet">
			<dsp:param name="siteId" value="${siteName}" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="siteUrl" param="siteUrl" />
				<c:set var="siteURL" value="${scheme}${siteUrl}" scope="request" />
			</dsp:oparam>
		</dsp:droplet>
	<c:if test="${empty pageHeadDefined}">
	<head>
	</c:if>
	<dsp:getvalueof param="staticAssetsTargeter" var="staticAssetsTargeter" />
	<c:if test="${not empty staticAssetsTargeter}">
		<dsp:droplet name="TRUContentLookupDroplet">
			<dsp:param name="articleContentKey" value="${staticAssetsTargeter}" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="articleBody" param="articleBody" />
					${articleBody}
				</dsp:oparam>
		</dsp:droplet>
	</c:if>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<c:if test="${enableSeo eq 'true'}">
			<meta name="robots" content="noindex, nofollow">
		</c:if>
		
		<c:if test="${not empty contentItem.HeaderContent}">
			<c:forEach var="element" items="${contentItem.HeaderContent}">
				<c:if test="${element['@type'] eq 'TRUHeadContent'}">
					<dsp:getvalueof var="staticassetpath" value="${element.StaticAssetspath}" />
					<c:if test="${not empty staticassetpath}">
						<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters${staticassetpath}" />
						<dsp:getvalueof var="cacheKey" value="${atgTargeterpath}${siteName}${locale}" />
						<dsp:droplet name="/com/tru/cache/TRUHeadContentTargaterCache">
							<dsp:param name="key" value="${cacheKey}" />
							<dsp:oparam name="output">
								<dsp:droplet name="/atg/targeting/TargetingFirst">
									<dsp:param name="targeter" bean="${atgTargeterpath}" />
									<dsp:oparam name="output">
										<dsp:valueof param="element.data" valueishtml="true" />
									</dsp:oparam>
								</dsp:droplet>
							</dsp:oparam>
						</dsp:droplet>
					</c:if>
				</c:if>
			</c:forEach>
		</c:if>


		<c:if test="${not empty contentKey}">
			<dsp:droplet name="TRUContentLookupDroplet">
				<dsp:param name="articleContentKey" value="${contentKey}" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="seoTitleText" param="seoTitleText" />
					<dsp:getvalueof var="seoMetaDesc" param="seoMetaDesc" />
					<dsp:getvalueof var="seoMetaKeyword" param="seoMetaKeyword" />
					<dsp:getvalueof var="seoAltDesc" param="seoAltDesc" />
					<dsp:getvalueof var="articleBody" param="articleBody" />
				</dsp:oparam>
			</dsp:droplet>
			<meta name="keywords" content="${seoMetaKeyword}">
			<meta name="description" content="${seoMetaDesc},${seoAltDesc}" />
			<c:set var="articleBody" scope="request">${articleBody}</c:set>
			<script type="text/javascript">
				<!--
				var currentTime = new Date()
				var month = currentTime.getMonth() + 1
				var day = currentTime.getDate()
				var year = currentTime.getFullYear()
				var now = month + "/" + day + "/" + year
				//-->
			</script>
		</c:if>

		<dsp:include page="/seo/seoTag.jsp">
			<dsp:param name="fromCat" value="${fromCat}" />
			<dsp:param name="fromSubCat" value="${fromSubCat}" />
			<dsp:param name="fromFamily" value="${fromFamily}" />
			<dsp:param name="fromPdp" value="${fromPdp}" />
			<dsp:param name="fromHome" value="${fromHome}" />
			<dsp:param name="staticPageTitleText" value="${seoTitleText}" />
		</dsp:include>
		
		<dsp:droplet name="/com/tru/droplet/TRUFetchServerAndEarNameDroplet">
			<dsp:oparam name="output">
			<dsp:getvalueof var="earName" param="earName" />
			<dsp:getvalueof var="instanceName" param="serverName" />
			<dsp:getvalueof var="appDate" param="appDate" />
			<c:choose>
				<c:when test="${not empty instanceName and not empty earName}">
					<%-- Server name: ${instanceName} Ear: ${earName} Application Date : ${appDate}  --%>
				</c:when>
				<c:otherwise>
					 <%-- Server name: ${instanceName}  Application Date : ${appDate} --%> 
				</c:otherwise>
			</c:choose>
			</dsp:oparam>
			<dsp:oparam name="empty">
			</dsp:oparam>
		</dsp:droplet>
		
		
		<dsp:getvalueof var="canonical" value="${canonicalURL}"/>
		<c:if test="${fromHome eq 'home'}">
			<dsp:getvalueof var="canonical" value="${siteURL}/"/>
			<c:if test="${siteName eq 'BabyRUs'}">
				<dsp:getvalueof var="canonical" value="${fn:replace(canonical,'.toysrus.com','.babiesrus.com')}"/>
			</c:if>
		</c:if>
		<c:if test="${not empty canonical}">
			<link rel="canonical" href="${canonical}"/>
			<dsp:getvalueof var="mobileCanonicalUrl" value="${fn:replace(canonical,'://','://m.')}"/>
			<dsp:getvalueof var="tabletCanonicalUrl" value="${fn:replace(canonical,'://','://t.')}"/>
			<dsp:getvalueof var="mobileCanonicalUrl" value="${fn:replace(mobileCanonicalUrl,'www.','')}"/>
			<dsp:getvalueof var="tabletCanonicalUrl" value="${fn:replace(tabletCanonicalUrl,'www.','')}"/>
		
			<link rel="alternate" media="only screen and (max-width: 640px)" href="${mobileCanonicalUrl}">
			<link rel="alternate" media="only screen and (min-width: 641px) and (max-width: 1024px)" href="${tabletCanonicalUrl}">
		</c:if>
		
		
		<dsp:getvalueof value='<%=atg.nucleus.DynamoEnv.getProperty("staticAssetVersion")%>' var="versionNumbers" />
		<dsp:getvalueof var="pdpH1" param="pdpH1"/>

		<c:choose>
			
		<c:when test="${enableMinification and debugFlag eq '1'}">
				<c:choose>
				<c:when test="${versionFlag}">
					<script type="text/javascript" charset="UTF-8"
						src="${TRUJSPath}javascript/vendor.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
						src="${TRUJSPath}javascript/maskPassword.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
						src="${TRUJSPath}javascript/cmsHeader.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
						src="${TRUJSPath}javascript/jquery.datetimepicker.full.min.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
						src="${contextPath}javascript/copy-cookie.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<link rel="stylesheet"
						href="${TRUCSSPath}css/fonts.css?${staticAssetsVersionFormat}=${versionNumbers}">
					<link rel="stylesheet"
						href="${TRUCSSPath}css/toolkit.css?${staticAssetsVersionFormat}=${versionNumbers}">
					<link rel="stylesheet"
						href="${TRUCSSPath}css/components.css?${staticAssetsVersionFormat}=${versionNumbers}">
					<link rel="stylesheet"
						href="${TRUCSSPath}css/structures.css?${staticAssetsVersionFormat}=${versionNumbers}">
					<link rel="stylesheet"
						href="${TRUCSSPath}css/templates.css?${staticAssetsVersionFormat}=${versionNumbers}">
					<link rel="stylesheet"
						href="${TRUCSSPath}css/jquery.mCustomScrollbar.min.css?${staticAssetsVersionFormat}=${versionNumbers}">
					<link rel="stylesheet"
						href="${TRUCSSPath}css/tRus_custom.css?${staticAssetsVersionFormat}=${versionNumbers}">
					<link rel="stylesheet"
						href="${TRUCSSPath}css/cart.css?${staticAssetsVersionFormat}=${versionNumbers}">
					<link rel="stylesheet"
						href="${TRUCSSPath}css/endeca.css?${staticAssetsVersionFormat}=${versionNumbers}">
					<link rel="stylesheet"
						href="${TRUCSSPath}css/truLayaway.css?${staticAssetsVersionFormat}=${versionNumbers}">
					<link rel="stylesheet"
						href="${TRUCSSPath}css/powerReview.css?${staticAssetsVersionFormat}=${versionNumbers}">
					<link rel="stylesheet"
						href="${TRUCSSPath}css/productDetails.css?${staticAssetsVersionFormat}=${versionNumbers}">
					<link rel="stylesheet"
						href="${TRUCSSPath}css/jquery.datetimepicker.css?${staticAssetsVersionFormat}=${versionNumbers}">
					<link rel="stylesheet"
						href="${TRUCSSPath}css/global-footer.css?${staticAssetsVersionFormat}=${versionNumbers}">
					<link rel="stylesheet"
						href="${TRUCSSPath}css/components/product.css?${staticAssetsVersionFormat}=${versionNumbers}">
					<c:if test="${not empty headerConfigurationCSS}">
						<link rel="stylesheet"
							href="${headerConfigurationCSS}?${staticAssetsVersionFormat}=${versionNumbers}">
					</c:if>
					<c:if test="${not empty headerConfigurationJS}">
							<script type="text/javascript" charset="UTF-8" src="${headerConfigurationJS}"></script>	
						</c:if>
				</c:when>
				<c:otherwise>
					<script type="text/javascript" charset="UTF-8"
						src="${TRUJSPath}javascript/vendor.js"></script>
					<script type="text/javascript" charset="UTF-8"
						src="${TRUJSPath}javascript/maskPassword.js"></script>
					<script type="text/javascript" charset="UTF-8"
						src="${TRUJSPath}javascript/cmsHeader.js"></script>
					<script type="text/javascript" charset="UTF-8"
						src="${TRUJSPath}javascript/jquery.datetimepicker.full.min.js"></script>
					<script type="text/javascript" charset="UTF-8"
						src="${contextPath}javascript/copy-cookie.js"></script>

					<link rel="stylesheet" href="${TRUCSSPath}css/fonts.css">
					<link rel="stylesheet" href="${TRUCSSPath}css/toolkit.css">
					<link rel="stylesheet" href="${TRUCSSPath}css/components.css">
					<link rel="stylesheet" href="${TRUCSSPath}css/structures.css">
					<link rel="stylesheet" href="${TRUCSSPath}css/templates.css">
					<link rel="stylesheet"
						href="${TRUCSSPath}css/jquery.mCustomScrollbar.min.css">
					<link rel="stylesheet" href="${TRUCSSPath}css/tRus_custom.css">
					<link rel="stylesheet" href="${TRUCSSPath}css/cart.css">
					<link rel="stylesheet" href="${TRUCSSPath}css/endeca.css">
					<link rel="stylesheet" href="${TRUCSSPath}css/truLayaway.css">
					<link rel="stylesheet" href="${TRUCSSPath}css/powerReview.css">
					<link rel="stylesheet" href="${TRUCSSPath}css/productDetails.css">
					<link rel="stylesheet"
						href="${TRUCSSPath}css/jquery.datetimepicker.css">
					<link rel="stylesheet" href="${TRUCSSPath}css/global-footer.css">
					<link rel="stylesheet"
						href="${TRUCSSPath}css/components/product.css">


					<c:if test="${not empty headerConfigurationCSS}">
						<link rel="stylesheet" href="${headerConfigurationCSS}">
					</c:if>
					<c:if test="${not empty headerConfigurationJS}">
							<script type="text/javascript" charset="UTF-8" src="${headerConfigurationJS}"></script>	
						</c:if>
				</c:otherwise>
			</c:choose>
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${versionFlag}">
						<link rel="stylesheet"	href="${TRUCSSPath}css/globalHeader.css?${staticAssetsVersionFormat}=${versionNumbers}">
						<script type="text/javascript" charset="UTF-8"
							src="${TRUJSPath}javascript/globalHeader.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<!-- Added JS and CSS for Header Configuration -->
						<c:if test="${not empty headerConfigurationCSS}">
							<link rel="stylesheet" href="${headerConfigurationCSS}?${staticAssetsVersionFormat}=${versionNumbers}">
						</c:if>
						<c:if test="${not empty headerConfigurationJS}">	
							<script type="text/javascript" charset="UTF-8" src="${headerConfigurationJS}?${staticAssetsVersionFormat}=${versionNumbers}"></script>	
						</c:if>
					</c:when>
					<c:otherwise>
						<link rel="stylesheet"
							href="${TRUCSSPath}css/globalHeader.css"> 
						<script type="text/javascript" charset="UTF-8"
							src="${TRUJSPath}javascript/globalHeader.js"></script>
							<!-- Added JS and CSS for Header Configuration -->
							<c:if test="${not empty headerConfigurationCSS}">
								<link rel="stylesheet" href="${headerConfigurationCSS}">
							</c:if>
						<c:if test="${not empty headerConfigurationJS}">
							<script type="text/javascript" charset="UTF-8" src="${headerConfigurationJS}"></script>	
						</c:if>
					</c:otherwise>
				</c:choose> 
			</c:otherwise>
		</c:choose>



		<c:if  test="${fromSubCat eq 'subcat' or fromFamily eq 'Family'}">
			<link rel="stylesheet" property="stylesheet" href="${TRUCSSPath}css/comparePagePrint.css">
		</c:if >
		<c:if test="${pageName eq 'orderConfirmation'}">
			<link rel="stylesheet" href="${TRUCSSPath}css/orderConfirmation.css">
		</c:if>

		<dsp:getvalueof var="tealiumSyncURL" bean="TRUTealiumConfiguration.tealiumSyncURL" />
		<script src="${tealiumSyncURL}"></script>

		<!-- <script type="text/javascript">
		  function getEmail(){
		    var email = document.getElementById('email').value;
			var url = "${domainUrl}${contextPath}myaccount/epslonEmailSignUp.jsp?email="+email;
			$('#epslonEmail').load(url);
		   //window.open(url, '_blank');
		  }
		  </script> -->
	</head>

	<body class="site-${siteName}">
	<input type="hidden" value="${persistDays}" name="persistDays" class="persistDays" />
	<input type="hidden" value="${TRUImagePath}" name="TRUImagePath" class="TRUImagePath" />

	<dsp:getvalueof var="plpPathCheck" bean="TRUStoreConfiguration.plpPathCheck" />
			<input type="hidden" id="plpPathCheck" value="${plpPathCheck}"/>
		<div id="ajaxLoaderUI"></div>
		<input type="hidden" id="babiesrusHostURL" value="${pdpRegistryUrl}"/>
		<c:choose>
			<c:when test="${siteName eq 'ToysRUs'}">
				<dsp:getvalueof var="contextRoot" value="TRU" />
			</c:when>
			<c:when test="${siteName eq 'BabyRUs'}">
				<dsp:getvalueof var="contextRoot" value="BRU" />
			</c:when>
		</c:choose>

		<input type="hidden" value="${contextRoot}" name="site" class="site"/>

		<c:if test="${not empty includePathValue and includePathValue eq 'cat' or includePathValue eq 'subcat' or includePathValue eq 'family'}">
			<h1 class="title-content"><dsp:valueof value="${catH1}" /></h1>
		</c:if>
		<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
			<dsp:oparam name="output">
			<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
			</dsp:oparam>
		</dsp:droplet>

		<input type="hidden" id="globalRadius" value="${globalRadius}" />
		<input type="hidden" id="lightBoxRadius" value="${lightBoxRadius}" />
		<div class="scroll-detection"></div>
		<!--START Gift Finder flyout -->

		<%-- END Gift Finder flyout  --%>
		<%-- Start Baby Registry Flyout --%>
		<c:if test="${siteName eq 'BabyRUs' && displayGiftFinderTab eq 'true'}">
			<div id="giftFinderTagEventInvoker"></div>
			<div class=".inline" id="showafterLoad" data-value="${giftFinderURL}">
				<dsp:include page="/babyRegistry/babyRegistryTemplate.jsp" />
				<c:choose>
					<c:when test="${scheme eq 'https://'}">
						<object class="baby-registry-tab" data="${TRUImagePath}images/svg/BabyRegistryTag_https.svg" id="babyRegistry" type="image/svg+xml"></object>
					</c:when>
					<c:otherwise>
						<object class="baby-registry-tab" data="${TRUImagePath}images/svg/BabyRegistryTag.svg" id="babyRegistry" type="image/svg+xml"></object>
					</c:otherwise>
				</c:choose>
			</div>
		</c:if>
		<%-- End Baby Registry Flyout --%>
		
		<c:if test="${pageName ne 'orderConfirmation'}">
			<a id="scrollToTop" href="javascript:void(0);">To Top</a>
		</c:if>

		<c:choose>
			<c:when	test="${fromsearch eq 'Search'}">
				<div class="container-fluid default-template search-template">
					<c:if test="${not empty headerContentItem.HeaderContent}">
						<div class="row row-no-padding">
							<div class="col-md-12 col-no-padding">
								<c:forEach var="element" items="${headerContentItem.HeaderContent}">
									<c:if test="${element['@type'] eq 'TRUHeader'}">
										<dsp:renderContentItem contentItem="${element}" />
									</c:if>
								</c:forEach>
							</div>
						</div>
					</c:if>
			</c:when>
			<c:when	test="${fromNoSearch eq 'NoSearch'}">
				<div class="container-fluid default-template search-template">
					<c:if test="${not empty headerContentItem.HeaderContent}">
						<div class="row row-no-padding">
							<div class="col-md-12 col-no-padding">
								<c:forEach var="element" items="${headerContentItem.HeaderContent}">
									<c:if test="${element['@type'] eq 'TRUHeader'}">
										<dsp:renderContentItem contentItem="${element}" />
									</c:if>
								</c:forEach>
							</div>
						</div>
					</c:if>
			</c:when>
			<c:when test="${fromFamily eq 'Family' }">
				<div class="container-fluid template-family default-template">
					<c:if test="${not empty headerContentItem.HeaderContent}">
						<div class="row row-no-padding">
							<div class="col-md-12 col-no-padding">
								<c:forEach var="element" items="${headerContentItem.HeaderContent}">
									<c:if test="${element['@type'] eq 'TRUHeader'}">
										<dsp:renderContentItem contentItem="${element}" />
									</c:if>
								</c:forEach>
							</div>
						</div>
					</c:if>
			</c:when>
			<c:when test="${fromGiftFinder eq 'GiftFinder' }">
				<div class="container-fluid gift-finder-results-template">
					<c:if test="${not empty headerContentItem.HeaderContent}">
						<div class="row row-no-padding">
							<div class="col-md-12 col-no-padding">
								<c:forEach var="element" items="${headerContentItem.HeaderContent}">
									<c:if test="${element['@type'] eq 'TRUHeader'}">
										<dsp:renderContentItem contentItem="${element}" />
									</c:if>
								</c:forEach>

								</div>
							</div>
					</c:if>
			</c:when>
			<c:when test="${fromSubCat eq 'subcat'}">
				<div class="sub-category-template default-template container-fluid min-width">
					<!-- coming from sub category -->
					<c:if test="${not empty headerContentItem.HeaderContent}">
						<div class="row row-no-padding">
							<div class="col-md-12 col-no-padding">
								<c:forEach var="element" items="${headerContentItem.HeaderContent}">
									<c:if test="${element['@type'] eq 'TRUHeader'}">
										<dsp:renderContentItem contentItem="${element}" />
									</c:if>
								</c:forEach>
							</div>
						</div>
<!-- 					</div> -->
					</c:if>
			</c:when>
			<c:when test="${fromShopBy eq 'ShopBy'}">
				<div class="container-fluid shop-by-template default-template min-width">
					<c:if test="${not empty headerContentItem.HeaderContent}">
						<div class="row row-no-padding">
							<div class="col-md-12 col-no-padding">
								<div class="header-part  fixed-nav">
									<c:forEach var="element" items="${headerContentItem.HeaderContent}">
										<c:if test="${element['@type'] eq 'TRUHeader'}">
											<dsp:renderContentItem contentItem="${element}" />
										</c:if>
									</c:forEach>
								</div>
							</div>
						</div>
				   <!--  </div> -->
					</c:if>
			</c:when>

			<c:when test="${fromsiteMap eq 'siteMap'}">
				<div class="container-fluid default-template sitemaps-template">
					<c:if test="${not empty headerContentItem.HeaderContent}">
						<div class="row row-no-padding">
							<div class="col-md-12 col-no-padding">
								<div class="header-part  fixed-nav">
									<c:forEach var="element" items="${headerContentItem.HeaderContent}">
										<c:if test="${element['@type'] eq 'TRUHeader'}">
											<dsp:renderContentItem contentItem="${element}" />
										</c:if>
									</c:forEach>
								</div>
							</div>
						</div>
					</c:if>
			</c:when>
			<c:otherwise>
				<c:if test="${fromCat eq 'cat'}">
					<div class="container-fluid category-template min-width">
				</c:if>
				<c:if test="${fromCat ne 'cat'}">
					<c:choose>
						<c:when test="${fromContactUs eq 'ContactUs'}">
							<div class="container-fluid help-customer-service-template min-width">
						</c:when>
						<c:otherwise>
							<div class="container-fluid home-template min-width">
						</c:otherwise>
					</c:choose>
				</c:if>
				<c:if test="${not empty headerContentItem.HeaderContent}">
					<div class="row row-no-padding">
						<dsp:droplet name="Switch">
							<dsp:param name="value" value="${pageName}"/>
							<dsp:oparam name="orderConfirmation">
								<div class="col-md-12 col-no-padding">
									<c:forEach var="element" items="${headerContentItem.HeaderContent}">
										<c:if test="${element['@type'] eq 'TRUHeader'}">
											<dsp:renderContentItem contentItem="${element}" />
										</c:if>
									</c:forEach>
								</div>
							</dsp:oparam>
							<dsp:oparam name="default">
								<div class="col-md-12 col-no-padding">
									<div class="header-part  fixed-nav">
										<c:forEach var="element" items="${headerContentItem.HeaderContent}">
											<c:if test="${element['@type'] eq 'TRUHeader'}">
												<dsp:renderContentItem contentItem="${element}" />
											</c:if>
										</c:forEach>
									</div>
								</div>
							</dsp:oparam>
						</dsp:droplet>
					</div>
				</c:if>
			</c:otherwise>
		</c:choose>
		<div id="addingPDPForRecentlyViewedReplace" class="display-none"></div>
		<dsp:include page="/header/dialogHolder.jsp" />
		<%-- start: popup for make my store  --%>
		<div class="modal editField fade" id="makemystoreModel"	tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog modal-md">
				<div class="modal-content sharp-border">
					<div class="makemystoreModel-popup">
						<div class="row">
							<div class="col-md-5 col-md-offset-7">
								<div class="row top-margin">
									<div class="col-md-1 col-md-offset-10">
										<span class="clickable"><img data-dismiss="modal" src="${TRUImagePath}images/close.png" alt="close model"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="makemystoreModel-popup-desc">
									<h3 class="w3ValidationMyStore">My Store</h3>
									<p>
										We would like to make <a href="javascript:void(0)" class="makemystoreModel-address">Cherry Wood</a> your
										store?
									</p>
									<p>
										Product availablity and pickup option will be displayed for
										this store.<br>You can change this store anytime using
										the store locator or My Store Link.
									</p>
								</div>
								<div class="action-buttons makemystoeModel-section">
									<button id="makemystoreModel-yes">Make this My Store</button>
									<button data-dismiss="modal">Cancel</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<%-- end: popup for make my store --%>		
		<div id="loadNortonHiddenDiv" class="hide-data">
			<c:set var="loadNorton" value="fail" />
			<c:if test="${fn:containsIgnoreCase(requestUrl, 'myaccount')}">
				<c:set var="loadNorton" value="success" />
			</c:if>
			<c:if test="${fn:containsIgnoreCase(requestUrl, 'orderConfirmation')}">
				<c:set var="loadNorton" value="success" />
			</c:if>
			<c:if test="${loadNorton eq 'success'}">
				<dsp:include page="../common/nortonSealScript.jsp" />
			</c:if>
		</div>
		<input type="hidden" id="googleStaticUrl" value="${googleStaticUrl}"/>
		<dsp:include page="/myaccount/truForgotPassword.jsp" />
		<dsp:include page="/jstemplate/resourceBundle.jsp" />
		
</dsp:page>