<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/atg/multisite/Site"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof var="contextPath" value="${originatingRequest.contextPath}"/>
<dsp:getvalueof bean="Site.olsonLearnMoreURL" var="olsonLearnMoreURL"/>
<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id" />

	<dsp:getvalueof var="fromNoSearch" param="fromNoSearch" />
	<dsp:getvalueof var="fromSubCat" param="fromSubCat" />
	<dsp:getvalueof var="fromsearch" param="fromsearch" />
	<dsp:getvalueof var="fromFamily" param="fromFamily" />
	<dsp:getvalueof var="removeGiftFinder" param="removeGiftFinder" />
	<dsp:getvalueof var="fromGiftFinder" param="fromGiftFinder" />
	<dsp:getvalueof var="fromShopBy" param="fromShopBy" />
	<dsp:getvalueof var="fromsiteMap" param="fromsiteMap" />
	<dsp:getvalueof var="cat" param="cat" />
	<dsp:getvalueof var="fromPdp" param="fromPdp" />
	<dsp:getvalueof var="fromHome" param="fromHome" />
	<dsp:getvalueof var="fromCollection" param="fromCollection" />
	
	<dsp:droplet name="/com/tru/droplet/TRUGetSiteDroplet">
		<dsp:param name="siteId" value="${siteName}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="siteUrl" param="siteUrl"/> 
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="/com/tru/droplet/TRUSchemeDetectionDroplet">
		<dsp:oparam name="output">
		<dsp:getvalueof var ="scheme" param="scheme"></dsp:getvalueof>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="fullyQualifiedcontextPathUrl" value="${scheme}${siteUrl}${contextPath}"/>


<!-- Cheking whether he is login or not -->
<c:set var="userStatus" value="${fn:length(cookie.VisitorFirstName.value)}"/> 

<div class="arrow"></div>

<c:if test="${ userStatus == 0 }">
 		<div class="my-account-logged-out">
		<div class="my-account-sign-in">
			<div data-toggle="modal" data-target="#signInModal" id="signInBtn"><a href="javascript:void(0)" class="link-no-underLine"><fmt:message key="myaccount.signin" /></a></div>
			<div>
				<p class="header-register-link">
					<a href="${fullyQualifiedcontextPathUrl}myaccount/myAccount.jsp" data-ab-link="My-Account"><fmt:message key="myaccount.register" /></a> <img src="${TRUImagePath}images/breadcrumb-right-arrow-small.png"
						alt="breadcrumb_right_arrow_small_img">
				</p>
				<p><fmt:message key="myaccount.for.account" /></p>
			</div>
		</div>
		<hr>
		<div>
			<p class="manage-account"><a href="${fullyQualifiedcontextPathUrl}myaccount/myAccount.jsp" data-ab-link="My-Account"><fmt:message key="myaccount.manage.account" /></a></p>
			<p class="your-order-history"><a href="${fullyQualifiedcontextPathUrl}myaccount/myAccount.jsp" data-ab-link="My-Account"><fmt:message key="myaccount.order.history" /></a></p>
		</div>
		<hr>
		<div>
			<img src="${TRUImagePath}images/rewards-logo.png" alt="rewards_logo" title="rewards_logo" aria-label="rewards_logo">
			<p><fmt:message key="myaccount.earn.rewards.for.points" /></p>
			<p>
				<a href="/cobrand/services/rewards-r-us" class="" target="_blank">
					<fmt:message key="myaccount.learn.more" />&nbsp;<img src="${TRUImagePath}images/breadcrumb-right-arrow-small.png"	alt="breadcrumb_right_arrow_small_img">
				</a>
			</p>
		</div>
	</div>
</c:if>

<c:if test="${ userStatus > 0 }">
 <div class="my-account-logged-in">
		<div>
			<p class="manage-account"><a href="${fullyQualifiedcontextPathUrl}myaccount/myAccount.jsp"><fmt:message key="myaccount.manage.account" /></a></p>
			<p class="your-order-history"><a href="${fullyQualifiedcontextPathUrl}myaccount/orderHistory.jsp"><fmt:message key="myaccount.order.history" /></a></p>
		</div>
		<hr>
		<dsp:include page="rewardsRUsHeaderSection.jsp"></dsp:include>
		<div>
			<div id="headerLogoutLink"><fmt:message key="myaccount.signout" /></div>
		</div>
	</div>
</c:if>
<script>
	var jsFullyQualifiedcontextPathUrl = '${fullyQualifiedcontextPathUrl}';
	var jsTRUImagePath = '${TRUImagePath}';
</script>
</dsp:page>