<%-- This page is used to search for physical stores locations for TRU/BRU sites
		with user entering location in the search field & perform search.
--%>

<dsp:page>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
<dsp:getvalueof var="customerEmail" bean="Profile.login"/>
<dsp:getvalueof var="customerID" bean="Profile.id"/>
<dsp:getvalueof var="customerDOB" bean="Profile.dateOfBirth"/>
<dsp:getvalueof var="loginStatus" bean="Profile.transient"/>
<dsp:getvalueof var="siteCode" value="TRU" />
<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
<c:if test="${site eq babySiteCode}">
<dsp:getvalueof var="siteCode" value="BRU" />
<dsp:getvalueof var="baseBreadcrumb" value="bru" />
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
</c:if>
<c:choose>
<c:when test="${loginStatus eq 'true'}">
<c:set var="customerStatus" value="Guest"/>
</c:when>
<c:otherwise>
<c:set var="customerStatus" value="Registered"/>
<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
		<dsp:droplet name="IsEmpty">
			<dsp:param name="value" bean="Profile.shippingAddress.id" />
			<dsp:oparam name="false">
				<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="Profile.secondaryAddresses"/>
				<dsp:param name="elementName" value="address"/>
				<dsp:oparam name="output">
				<dsp:getvalueof param="address.id" var="addressId" />
				<c:if test="${addressId eq shippingAddressId}">
						 				
						 <dsp:getvalueof param="address.city" var="customerCity"/> 
						 <dsp:getvalueof param="address.state" var="customerState"/> 
						 <dsp:getvalueof param="address.postalCode" var="customerZip"/>
						 <dsp:getvalueof param="address.country" var="customerCountry"/>
							
				 </c:if>
				</dsp:oparam>
			   </dsp:droplet>
			</dsp:oparam>
</dsp:droplet>
</c:otherwise>
</c:choose>
	<%-- <div class="global-nav-your-store-locator-tooltip popup-marker">
		<div class="global-nav-store-locator fav-store-popover popover"
			role="tooltip">
			<div class="arrow"></div>
			<div class="global-nav-store-details partial">
				<div class="store-locator-content">
					<div class="back-to-store">
						<img class="inline"
							src="${TRUImagePath}images/small-back-arrow.png" alt=""><a
							href="javascript:void(0)">back to store results</a>
					</div>
					<a
						href="http://cloud.toysrus.resource.com/redesign/template-store-locator-template.html"><img
						src="${TRUImagePath}images/staticmap"> </a> <img
						class="you-are-here" src="${TRUImagePath}images/my_TRU_Dot.png"
						alt="image not found">
					<div class="store-locator-store-details">
					    <h2>Toys“R”Us—Miami</h2>
					    <p>
					        3 miles away<span>·</span>open until 11pm today
						</p>
						<p class="make-store-link">
							<a>make this my store</a>
						</p>
						<div class="tse-scrollable store-locator-tooltip-store-details">
							<div class="tse-content">
								<div class="store-address">
									<p>3301 Coral Way</p>
									<p>Miami, FL 33145</p>
									<p>(305) 443-9004</p>
									<a>get directions</a>
								</div>
								<div class="store-hours">
									<div>
										<p>Mon-Fri:</p>
										<p>Sat:</p>
										<p>Sun:</p>
									</div>
									<div>
										<p>10:00am-9:00pm</p>
										<p>9:00am-9:00pm</p>
										<p>10:00am-7:00pm</p>
									</div>
								</div>
								<br>
								<header>store services</header>
								<ul>
									<li>layaway
										<p>Shop today and take time to pay!</p> <a>learn more</a>
									</li>
									<li>parenting classes
										<p>Informative classes on everything baby from
											breastfeeding to infant CPR.</p> <a>learn more</a>
									</li>
									<li>personal registry advisor
										<p>Schedule a one-on-one appointment with a Personal
											Registry Advisor to help you create the perfect registry.</p> <a>learn
											more</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> --%>


	<div class="global-nav-store-locator-tooltip popup-marker">
		<div class="global-nav-store-locator popover header-findstore-popup" role="tooltip">
			<div class="arrow"></div>
			
			
			
			<!--  -->
			
			
			
			<!-- Naming Convention SL -  Store Locator -->
			<div class="sl_root no_scrollbar">
					<div class="sl_parent">
					<div class="store-locator-pick-store default_view">
						<div class="storePopulationDetails">
							 <h3><span class="sl_store_count"> </span><!--stores near you -->&nbsp;<fmt:message key="tru.storelocator.label.storesnearyou"/></h3>
							<p class="search_term_p"> <!--for --> <fmt:message key="tru.storelocator.label.for"/> <span class="sl_search_term"></span></p>
							<a href="javascript:void(0);" class="change-zip"><!--change location --><fmt:message key="tru.storelocator.label.changelocation"/></a>
						</div>
						<div class="sl_store_search_form ft-size-adjuster-sl row">
							<%-- <dsp:include page="/storelocator/findAStore.jsp"/> --%>
							<div class="store-locator-zip">
								<h3>
									<!--find --> <fmt:message key="tru.storelocator.label.find"/>
								</h3>
								<img class="change-location-dismiss"
										src="${TRUImagePath}images/close.png"
										alt="dismiss find a store">
								<p><!-- Enter a location: street address, city, state or ZIP --><fmt:message key="tru.storelocator.label.searchText" var="storeTxt"/></p>
								
								<input type="hidden" value="${contextPath}" class="contextPath" name="contextPath"/>
								<input type="text"  class="inline storeLocatorField" data-id="find-store-header" title="storeLocatorField" maxlength="255" autocomplete="off" placeholder="${storeTxt}" />
								<input class="storeSearch" type="hidden" value="find" name="storeSearchName"/>
								<button class="" onclick="javascript:findAStore();">find stores</button>
								<br>
							</div>
							<p class="sl_errormsg">No Results Found</p>
						</div>
							
						<div class="sl_store_locator_results">
						<ul id="sl_store-locator-results-tabs" class="tooltip-store-locator-results-tabs nav nav-tabs">
							<li class="active">
								<a href="javascript:void(0)" class="product-review-tab-text all_results_tab" data-toggle="tab"><!-- All --><fmt:message key="tru.storelocator.label.all" /></a>
							</li>
							<li>
								<a href="javascript:void(0)" class="product-review-tab-text tru_results_tab" data-toggle="tab"><!--Toys"R"Us  --><fmt:message key="tru.storelocator.label.toysrustab" /></a>
							</li>
							<li>
								<a href="javascript:void(0)" class="product-review-tab-text bru_results_tab" data-toggle="tab"><!--Babies"R"Us --><fmt:message key="tru.storelocator.label.babiesrustab" /></a>
							</li>
						</ul>
						<div id="nearby-stores" class="" >
							<div class="">
								<div class="tse-scrollable">
			                       <div class="tse-content">
										<ul class="sl_store_listing_ul"></ul>
										<div class="sl_show-more-stores">
											<button>load more</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="no_specific_chain_stores"><p></p></div>
						
						</div>
						</div>
					</div>
					<dsp:include page="/storelocator/viewStoreDetails.jsp" />
					<dsp:include page="/storelocator/storeDetails.jsp"/>
				</div>
			
			</div>
		</div>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPInternalCampaignPage" var="internalCampaignPage"/>		
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOrsoCode" var="orsoCode"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.deviceType" var="deviceType"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.cartIspuSource" var="ispuSource"/>
<input type="hidden" id="searchTearm" value=""/>
<input type="hidden" id="storeZipCode" value=""/>
<script type='text/javascript'>

function tealiumForStore(searchTerm,locatorZip,ISPUotherFeatures){
	var storeAvailText;
	if(typeof ISPUotherFeatures == 'undefined' || ISPUotherFeatures == 'undefined' || ISPUotherFeatures == '')
		{
			storeAvailText = "Continue Shopping";
		}
	else if((typeof ISPUotherFeatures != 'undefined' || ISPUotherFeatures != 'undefined') && ISPUotherFeatures == 'showMore')
		{
			storeAvailText = "More Stores";
		}
	else if((typeof ISPUotherFeatures != 'undefined' || ISPUotherFeatures != 'undefined') && ISPUotherFeatures == 'close')
		{
			storeAvailText = "Close";
		}
	var onlinePid = $("#onlinePID").val(),
	productName = $("#productName").val();
	if( typeof utag != 'undefined' ){
		utag.view({
		    customer_status:"${customerStatus}",
		    customer_city:"${customerCity}",
		    customer_country:"${customerCountry}",
		    customer_email:"${customerEmail}",
		    customer_id:"${customerID}",
		    customer_type:"${customerStatus}",
		    customer_state:"${customerState}",
		    customer_zip:"${customerZip}",
		    customer_dob:"${customerDOB}",
		    page_name:"${siteCode}:StoreList Page",
		    page_type:"${siteCode}:storelocator",
		    browser_id:"${pageContext.session.id}",
		    internal_campaign_page:"${internalCampaignPage}",
		    ispu_store_code:searchTerm,
		    ispu_zip:locatorZip,
		    orso_code:"${orsoCode}",
		    ispu_source:"en_US:${siteCode}: Product Detail:"+onlinePid+":"+productName,
		    ispu_other_features:"Store Availability Results: "+storeAvailText,
		    device_type:"${deviceType}"
		});
	}
}	
</script>

<script>

function omniStoreLocator(searchTerm,locatorCity,locatorState,locatorZip){
	if( typeof utag != 'undefined' ){
		utag.link({
		'event_type':'store_locator',
		'customer_email': '${customerEmail}',
	    'customer_id': '${customerID}',
	    'store_locator':searchTerm,
	    'partner_name':'${partnerName}',
	    'locator_city':locatorCity,
	    'locator_state':locatorState,
	    'locator_zip':locatorZip
		}); 
	}
}	

</script>
<script type="text/javascript">
	function omniIspuOutOfStock(storeId){
		if( typeof utag != 'undefined' ){
			utag.link({
   				'event_type':'ispu_out_of_stock',
   				'selected_store':storeId
   			});
		}
	}
	function omniIspuStoreServed(storeId){
		if( typeof utag != 'undefined' ){
			utag.link({
	   			'event_type':'ispu_store_served',
	   			'selected_store':storeId
   			});
		}
	}
	</script>


<script>
function omniMakeMyStore(storeId,city,postalCode){
	if( typeof utag != 'undefined' ){
		utag.link({
		'event_type':'store_locator',
		'customer_email': '${customerEmail}',
	    'customer_id': '${customerID}',
	    'store_locator':storeId,
	    'partner_name':'${partnerName}',
	    'locator_city':city,
	    'locator_zip':postalCode,
	    'selected_store':storeId
		}); 
	}
}	
</script>

</dsp:page>