<dsp:page>
	<dsp:importbean bean="/atg/endeca/assembler/droplet/InvokeAssembler" />
	<dsp:importbean bean="/com/tru/droplet/GetSiteTypeDroplet" />
	
	<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.endecaSiteId" />
	<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale" />
	
	<dsp:droplet name="GetSiteTypeDroplet">
		<dsp:param name="siteId" value="${siteId}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="site" param="site"/>
			<c:set var="site" value ="${site}" scope="request"/>
		</dsp:oparam>
	</dsp:droplet>
	<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
	
	 <c:choose>
			<c:when test="${site eq 'sos' and isSosVar}">
				<dsp:getvalueof var="cacheKey" value="/InvokeAssembler${siteName}HomeMain${site}${locale}" />
			</c:when>
			<c:otherwise>
				<dsp:getvalueof var="cacheKey" value="/InvokeAssembler${siteName}HomeMain${locale}" />
			</c:otherwise>
	</c:choose>
	
	
	<dsp:droplet name="/com/tru/cache/TRUHomePageAssemblerCache">
			<dsp:param name="key" value="${cacheKey}" />
			<dsp:oparam name="output">
			
			<dsp:droplet name="InvokeAssembler">
				<dsp:param name="includePath" value="/pages${siteName}/home" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="ContentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
				</dsp:oparam>
			</dsp:droplet>
		
			<c:if test="${not empty ContentItem}">
				<dsp:renderContentItem contentItem="${ContentItem}" />
			</c:if>
		</dsp:oparam>
	</dsp:droplet>
	
</dsp:page>
