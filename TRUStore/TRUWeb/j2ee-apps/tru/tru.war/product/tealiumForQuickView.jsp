<dsp:page>
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUPriceDroplet" />
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
<dsp:getvalueof var="siteCode" value="TRU" />
<dsp:getvalueof var="baseBreadcrumb" value="tru" />
<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
<dsp:getvalueof param="searchKeyWord" var="tSearchKeyWord"/>
<dsp:getvalueof param="pageName" var="tPageName"/>
<dsp:getvalueof param="pageType" var="tPageType"/>
<dsp:getvalueof var="customerEmail" bean="Profile.login"/>
<dsp:getvalueof var="customerID" bean="Profile.id"/>
<dsp:getvalueof var="customerDOB" bean="Profile.dateOfBirth"/>
<dsp:getvalueof var="loginStatus" bean="Profile.transient"/>
<dsp:getvalueof param="productInfo" var="productInfo" />
<dsp:getvalueof param="productInfo.productId" var="productId"/>
<dsp:getvalueof param="productInfo.displayName" var="productName"/>
<dsp:getvalueof param="productInfo.defaultSKU.brandName" var="productBrand"/>
<dsp:getvalueof param="productInfo.defaultSKU.primaryImage" var="productImageURL"/>
<dsp:getvalueof param="productInfo.defaultSKU.id" var="productSku"/>
<dsp:getvalueof param="productInfo.defaultSKU.collectionNames" var="collectionNames"/>
<dsp:getvalueof param="productInfo.defaultSKU.onlinePID" var="onlinePid"/>
<dsp:getvalueof param="familyName" var="familyName"/>
<c:set var="singleQuote" value="'"/>
<c:set var="doubleQuote" value='"'/>
<c:set var="slashSingleQuote" value="\'"/>
<c:set var="slashDoubleQuote" value='\"'/>

<dsp:getvalueof var="customerFirstName" bean="Profile.firstName"/>
	<dsp:getvalueof var="customerLastName" bean="Profile.lastName"/>
	<c:set var="space" value=" "/>
	<c:choose>
	<c:when test="${empty customerFirstName && empty customerLastName}">
		<c:set var="customerName" value='${fn:substring(customerEmail, 0, fn:indexOf(customerEmail, "@"))}'/>
	</c:when>
	<c:when test="${not empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value="${customerFirstName}"/>
	</c:when>
	<c:when test="${empty customerFirstName && not empty customerLastName}">
		   <c:set var="customerName" value="${customerLastName}"/>
	</c:when>
	<c:otherwise>
			<c:set var="customerName" value="${customerFirstName}${space}${customerLastName}"/>
	</c:otherwise>
 </c:choose>


	     <c:if test="${fn:contains(productName,singleQuote)}">
	     <c:set var="productName" value="${fn:replace(productName,singleQuote,slashSingleQuote)}"/>
		 </c:if>
		 <c:if test="${fn:contains(productName,doubleQuote)}">
	     <c:set var="productName" value="${fn:replace(productName,doubleQuote,slashDoubleQuote)}"/>
		 </c:if>

	     <c:if test="${fn:contains(productBrand,singleQuote)}">
	     <c:set var="productBrand" value="${fn:replace(productBrand,singleQuote,slashSingleQuote)}"/>
		 </c:if>
		 <c:if test="${fn:contains(productBrand,doubleQuote)}">
	     <c:set var="productBrand" value="${fn:replace(productBrand,doubleQuote,slashDoubleQuote)}"/>
		 </c:if>



<!--  Added For Tealium Integration -->
<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>

<c:forEach var="element" items="${contentItem.MainContent}">
			<c:if test="${not empty element.PDPBreadCrumbList}">
						<c:set var="catIndex" value="0"/>
						<c:forEach var="breadList" items="${element.PDPBreadCrumbList}">
						    <c:choose>
						    <c:when test="${catIndex eq '0'}">
							<c:set var="productCategories" value="${productCategories}'${breadList.categoryName}'"/>
							</c:when>
							<c:otherwise>
								<c:choose>
								<c:when test="${catIndex eq element.PDPBreadCrumbList.size()-1}">
								<c:set var="productSubCategories" value="${productSubCategories}'${breadList.categoryName}'"/>
								</c:when>
								<c:otherwise>
								<c:set var="productSubCategories" value="${productSubCategories}'${breadList.categoryName}'/"/>
								</c:otherwise>
								</c:choose>
							</c:otherwise>
							</c:choose>
							<c:set var="catIndex" value="${catIndex+1}"/>
						</c:forEach>
			</c:if>
</c:forEach>
<dsp:droplet name="/com/tru/common/droplet/TRUTealiumCategoryModifierDroplet">
	<dsp:param name="categoryName" value="${productCategories}"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="modifiedProdCategory" param="modifiedCategory"/>
	</dsp:oparam>
</dsp:droplet>
<dsp:droplet name="/com/tru/common/droplet/TRUTealiumCategoryModifierDroplet">
	<dsp:param name="categoryName" value="${productSubCategories}"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="modifiedSubCategory" param="modifiedCategory"/>
	</dsp:oparam>
</dsp:droplet>

<dsp:getvalueof var="breadCrumbString" value="/${productCategories}/${productSubCategories}"/>


<dsp:droplet name="/com/tru/common/droplet/TRUTealiumCategoryModifierDroplet">
	<dsp:param name="categoryName" value="${breadCrumbString}"/>
	<dsp:param name="pageName" value="product"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="modifiedBreadCrumb" param="modifiedCategory"/>
		<dsp:getvalueof var="taxonomyString" param="breadCrumbLevels"/>
	</dsp:oparam>
</dsp:droplet>
	   <c:forEach var="collections" items="${collectionNames}">
	    <c:set var="collectionName" value="${collections.value}"/>
	   </c:forEach>
	   <c:if test="${not empty onlinePid && not empty productName}">
       <c:set var="pcollectionName" value="${pcollectionName}'${onlinePid}: ${productName}'"/>
	   </c:if>

<c:choose>
<c:when test="${not empty pcollectionName}">
<c:set var="productCollection" value="${pcollectionName}"/>
</c:when>
<c:otherwise>
<dsp:getvalueof var="productCollection" bean="TRUTealiumConfiguration.PDPProductCollection"/>
</c:otherwise>
</c:choose>

<c:choose>
<c:when test="${loginStatus eq 'true'}">
<c:set var="customerStatus" value="Guest"/>
</c:when>
<c:otherwise>
<c:set var="customerStatus" value="Registered"/>
<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
		<dsp:droplet name="IsEmpty">
			<dsp:param name="value" bean="Profile.shippingAddress.id" />
			<dsp:oparam name="false">
				<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="Profile.secondaryAddresses"/>
				<dsp:param name="elementName" value="address"/>
				<dsp:oparam name="output">
				<dsp:getvalueof param="address.id" var="addressId" />
				<c:if test="${addressId eq shippingAddressId}">

						 <dsp:getvalueof param="address.city" var="customerCity"/>
						 <dsp:getvalueof param="address.state" var="customerState"/>
						 <dsp:getvalueof param="address.postalCode" var="customerZip"/>
						 <dsp:getvalueof param="address.country" var="customerCountry"/>

				 </c:if>
				</dsp:oparam>
			   </dsp:droplet>
			</dsp:oparam>
</dsp:droplet>
</c:otherwise>
</c:choose>
	<dsp:droplet name="TRUPriceDroplet">
		<dsp:param name="skuId" value="${productSku}" />
		<dsp:param name="productId" value="${productId}" />
		<dsp:oparam name="true">
			<dsp:getvalueof param="salePrice" var="productUnitPrice" />
			<dsp:getvalueof param="listPrice" var="productListPrice" />
			<dsp:getvalueof param="savedAmount" var="productDiscount" />
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:getvalueof param="salePrice" var="productUnitPrice" />
			<dsp:getvalueof param="listPrice" var="productListPrice" />
					<c:if test="${productUnitPrice == 0}">
						<c:set var="productUnitPrice" value="${productListPrice}"/>
					</c:if>
		</dsp:oparam>
	</dsp:droplet>

<dsp:getvalueof bean="TRUTealiumConfiguration.quickviewPageName" var="quickviewPageName"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.quickviewPageType" var="quickviewPageType"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.browserID" var="browserID"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOasTaxonomy" var="oasTaxonomy"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPSiteSection" var="siteSection"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOasSize" var="oasSize"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOasBreadcrumb" var="oasBreadcrumb"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.deviceType" var="deviceType"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPFinderSortGroup" var="finderSortGroup"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPProductFindingMethod" var="productFindingMethod"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPImageIcon" var="imageIcon"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPInternalCampaignPage" var="internalCampaignPage"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOrsoCode" var="orsoCode"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOutOfStockBySku" var="outOfStockBySku"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPProdMerchCategory" var="productMerchandisingCategory"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPRegistryOrigin" var="registryOrigin"/>
<%-- <dsp:getvalueof bean="TRUTealiumConfiguration.PDPStrikedPricePage" var="strikedPricePage"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPStrikedPricePageTitle" var="strikedPricePageTitle"/> --%>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPIspuSource" var="ispuSource"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOasSize" var="oasSize"/>
<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
<%-- <dsp:getvalueof var="sosCookie" value="${cookie.isSOS.value}"/> --%>
<dsp:getvalueof var="productQueryId" value="${onlinePid}" />

<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
<c:choose>
<c:when test="${isSosVar eq 'true'}">
<c:set var="selectedStore" value="sos"/>
</c:when>
<c:otherwise>
<c:set var="selectedStore" value=""/>
</c:otherwise>
</c:choose>

<c:if test="${onlinePid eq ''}">
	<dsp:getvalueof var="productQueryId" value="${productId}" />
</c:if>
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />

<c:if test="${site eq babySiteCode}">
	<dsp:getvalueof var="siteCode" value="BRU" />
	<dsp:getvalueof var="baseBreadcrumb" value="bru" />
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
</c:if>

<c:if test="${productDiscount gt '0'}">
	<c:set var="strikedPricePage" value="striked price"/>
	<c:set var="strikedPricePageTitle" value="${siteCode}: ${quickviewPageName}: ${productQueryId}: ${productName}"/>
</c:if>

<dsp:droplet name="/com/tru/commerce/locations/TRUGetCookieDroplet">
	<dsp:param name="cookieName" value="favStore" />
	<dsp:oparam name="output">
		<dsp:getvalueof var="cookieValue" param="cookieValue" />
		<dsp:getvalueof var="locationIdFromCookie" param="locationId" />
	</dsp:oparam>
	<dsp:oparam name="empty">
	</dsp:oparam>
</dsp:droplet>

<c:set var="session_id" value="${cookie.sessionID.value}"/>
<c:set var="visitor_id" value="${cookie.visitorID.value}"/>

<script type='text/javascript'>
$(window).load(function(){
  utag.link({
	    event_type:'quick_shop'
	});
});

</script>

<script type='text/javascript'>
function loadOmniQuickView(){
	 utag.view({
	    customer_status:"${customerStatus}",
	    customer_city:"${customerCity}",
	    customer_country:"${customerCountry}",
	    customer_email:"${customerEmail}",
	    customer_id:"${customerID}",
	    customer_type:"${customerStatus}",
	    customer_state:"${customerState}",
	    customer_zip:"${customerZip}",
	    customer_dob:"${customerDOB}",
	    page_name:"${siteCode}: ${quickviewPageName}: ${productId}: ${productName}",
	    page_type:"${siteCode}: ${quickviewPageType}",
	    product_brand:["${productBrand}"],
	    product_category:[${productCategories}],
	    product_id:["${productQueryId}"],
	    product_name:["${productName}"],
	    product_unit_price:["${productUnitPrice}"],
	    product_list_price:["${productListPrice}"],
	    product_sku :["${productSku}"],
	    product_subcategory:[${productSubCategories}],
	    product_finding_method:"${productFindingMethod}",
	    //product_discount:["${productDiscount}"],
    	product_image_url:["${productImageURL}"],
    	product_family:["${familyName}"],
	    browser_id:"${pageContext.session.id}",
	    internal_campaign_page:"${internalCampaignPage}",
	    orso_code:"${orsoCode}",
	    product_collection:[${productCollection}],
	    product_merchandising_category:"${productMerchandisingCategory}",
	    registry_origin:"${registryOrigin}",
    	striked_price_page:"${strikedPricePage}",
    	striked_price_page_title:"${strikedPricePageTitle}",
	    ispu_source:"${ispuSource}",
	    device_type:"${deviceType}",
	    finder_sort_group:"${finderSortGroup}",
	    partner_name:"${partnerName}",
	    selected_store:"${selectedStore}",
	    customer_name:"${customerName}",
	    event_type:['quick_shop','out_of_stock'],
	    store_locator:"${locationIdFromCookie}",
	 	session_id : "${session_id}",
		visitor_id : "${visitor_id}"
	});
}
</script>

<script>
/* function loadOmniScriptQuickView(e){
		var qty=$("#ItemQty").val();
		utag.link({
		    event_type: 'cart_add',
		    customer_email: "${customerEmail}",
		    customer_id: "${customerID}",
		    product_brand: ['${productBrand}'],
		    product_category: [${productCategories}],
		    product_id: ['${productQueryId}'],
		    product_name: ['${productName}'],
		    product_unit_price: ["${productUnitPrice}"],
		    product_list_price: ["${productListPrice}"],
		    product_quantity: [qty],
		    product_sku: ['${productSku}'],
		    product_subcategory: [${productSubCategories}],
		    product_finding_method: ['${productFindingMethod}'],
		    product_discount: ['${productDiscount}'],
		    product_store: ['PRODUCT STORE'],
		    cart_addition_source: [e],
		    product_merchandising_category: '${productMerchandisingCategory}',
		    partner_name: '${partnerName}'
		});
} */

</script>
<!-- Start: Script for Tealium Integration -->
		<script type="text/javascript">

		    (function(a,b,c,d){
		    a='${tealiumURL}';
		    b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
		    a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
		    })();

		</script>

<!-- End: Script for Tealium Integration -->
</dsp:page>