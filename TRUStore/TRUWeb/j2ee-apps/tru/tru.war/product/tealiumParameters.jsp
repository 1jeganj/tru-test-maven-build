<dsp:page>
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUPriceDroplet" />
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
<dsp:getvalueof var="siteCode" value="TRU" />
<dsp:getvalueof var="baseBreadcrumb" value="tru" />
<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
<dsp:getvalueof param="searchKeyWord" var="tSearchKeyWord"/>
<dsp:getvalueof param="pageName" var="tPageName"/>
<dsp:getvalueof param="pageType" var="tPageType"/>
<dsp:getvalueof var="customerEmail" bean="Profile.login"/>
<dsp:getvalueof var="customerID" bean="Profile.id"/>
<dsp:getvalueof var="customerDOB" bean="Profile.dateOfBirth"/>
<dsp:getvalueof var="loginStatus" bean="Profile.transient"/>
<dsp:getvalueof param="productInfo" var="productInfo" />
<dsp:getvalueof param="productInfo.productId" var="productId"/>
<dsp:getvalueof param="productInfo.defaultSKU.displayName" var="productName"/>
<dsp:getvalueof param="productInfo.defaultSKU.brandName" var="productBrand"/>
<dsp:getvalueof param="productInfo.defaultSKU.primaryImage" var="productImageURL"/>
<dsp:getvalueof param="productInfo.defaultSKU.id" var="productSku"/>
<dsp:getvalueof param="productInfo.defaultSKU.collectionNames" var="collectionNames"/>
<dsp:getvalueof param="productInfo.defaultSKU.onlinePID" var="onlinePid"/>
<dsp:getvalueof  param="productInfo.defaultSKU.promotionalStickerDisplay" var="promotionalStickerDisplay"/>
<dsp:getvalueof param="productInfo.defaultSKU.relatedProducts" var="relatedProducts"/>
<dsp:getvalueof param="productInfo.outofStockSKUsList" var="outOfStockSkus"/>

<dsp:droplet name="/com/tru/droplet/TRUCategoryPrimaryPathDroplet">
		<dsp:param name="skuId" value="${productSku}" />
		<dsp:oparam name="output">
		<dsp:getvalueof var="primaryPathCategoryVOList" param="primaryPathCategoryVOList" />
		</dsp:oparam>
	</dsp:droplet>
	<c:choose>
	<c:when test="${not empty pdpH1}">
        <c:set var="productName" value="${pdpH1}"/>  
	</c:when>
	<c:otherwise>
		<c:set var="productName" value="${productName}"/>
	</c:otherwise>
	</c:choose>
	
	<dsp:getvalueof var="productCategories" value="${primaryPathCategoryVOList[0].categoryName}"/>
	<dsp:getvalueof var="categoryName" value="${primaryPathCategoryVOList[0].categoryName}"/>
	
	<dsp:droplet name="ForEach">
		<dsp:param name="array" value="${primaryPathCategoryVOList}" />
		<dsp:param name="elementName" value="categoryList" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="count" param="count"/>
			<dsp:getvalueof var="size" param="size"/>
			<c:choose>
				<c:when test="${count eq 1}">
			</c:when>
			<c:when test="${count eq size}">
			</c:when>
			<c:otherwise>
			 <dsp:getvalueof var="subCategories" param="categoryList.categoryName"/>
			 <c:choose>
				 <c:when test="${count eq size-1}">
				 <dsp:getvalueof var="subCategories" value="${subCategories}"/>
				 </c:when>
				 <c:otherwise>
				 <dsp:getvalueof var="subCategories" value="${subCategories},"/>
				 </c:otherwise>
				</c:choose>
			 <dsp:getvalueof var="productSubCategories" value="${productSubCategories}${subCategories}"/>
			 <dsp:getvalueof var="pageSubCategories" value="${subCategories}"/>
			</c:otherwise>
			</c:choose>
			<dsp:getvalueof var="" param="categoryList.categoryName"/>
		</dsp:oparam>
	</dsp:droplet>
	
	<dsp:getvalueof var="customerFirstName" bean="Profile.firstName"/>
	<dsp:getvalueof var="customerLastName" bean="Profile.lastName"/>
	<c:set var="space" value=" "/>
	<c:choose>
	<c:when test="${empty customerFirstName && empty customerLastName}">
		<c:set var="customerName" value='${fn:substring(customerEmail, 0, fn:indexOf(customerEmail, "@"))}'/>
	</c:when>
	<c:when test="${not empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value="${customerFirstName}"/>
	</c:when>
	<c:when test="${empty customerFirstName && not empty customerLastName}">
		   <c:set var="customerName" value="${customerLastName}"/>
	</c:when>
	<c:otherwise>
			<c:set var="customerName" value="${customerFirstName}${space}${customerLastName}"/>
	</c:otherwise>
 </c:choose>
<c:set var="singleQuote" value="'"/>
<c:set var="doubleQuote" value='"'/>
<c:set var="slashSingleQuote" value="\'"/>
<c:set var="slashDoubleQuote" value='\"'/>

<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
<c:choose>
<c:when test="${isSosVar eq 'true'}">
<c:set var="selectedStore" value="sos"/>
</c:when>
<c:otherwise>
<c:set var="selectedStore" value=""/>
</c:otherwise>
</c:choose>
<c:set var="tealiumXsellProduct" value="${tealiumXsellProduct}'${relatedProducts}',"/>
<!--  Added For Tealium Integration -->
<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>

<c:forEach var="element" items="${contentItem.MainContent}">
			<c:if test="${not empty element.PDPBreadCrumbList}">
						<c:set var="catIndex" value="0"/>
						<c:forEach var="breadList" items="${element.PDPBreadCrumbList}">
						<c:set var="categoryName" value="${breadList.categoryName}"/>
							     <c:if test="${fn:contains(categoryName,singleQuote)}">
							     <c:set var="categoryName" value="${fn:replace(categoryName,singleQuote,slashSingleQuote)}"/>
								 </c:if>
								 <c:if test="${fn:contains(categoryName,doubleQuote)}">
							     <c:set var="categoryName" value="${fn:replace(categoryName,doubleQuote,slashDoubleQuote)}"/>
								 </c:if>
								 <c:set var="categoryName" value="${categoryName}"/>
						    <c:choose>
						    <c:when test="${catIndex eq '0'}">
							</c:when>
							<c:otherwise>
								<c:choose>
								<c:when test="${catIndex eq element.PDPBreadCrumbList.size()-1}">
								<c:set var="productfamily" value="${categoryName}"/>
								</c:when>
								</c:choose>
							</c:otherwise>
							</c:choose>
							<c:set var="catIndex" value="${catIndex+1}"/>
						</c:forEach>
			</c:if>
</c:forEach>
<dsp:droplet name="/com/tru/common/droplet/TRUTealiumCategoryModifierDroplet">
	<dsp:param name="categoryName" value="${productCategories}"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="modifiedProdCategory" param="modifiedCategory"/>
	</dsp:oparam>
</dsp:droplet>
<dsp:droplet name="/com/tru/common/droplet/TRUTealiumCategoryModifierDroplet">
	<dsp:param name="categoryName" value="${productSubCategories}"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="modifiedSubCategory" param="modifiedCategory"/>
	</dsp:oparam>
</dsp:droplet>

<c:choose>
<c:when test="${not empty productCategories && not empty productSubCategories}">
<dsp:getvalueof var="breadCrumbString" value="/${productCategories}/${productSubCategories}"/>
</c:when>
<c:when test="${not empty productCategories && empty productSubCategories}">
<dsp:getvalueof var="breadCrumbString" value="/${productCategories}"/>
</c:when>
<c:when test="${empty productCategories && not empty productSubCategories}">
<dsp:getvalueof var="breadCrumbString" value="/${productSubCategories}"/>
</c:when>
<c:otherwise>
<dsp:getvalueof var="breadCrumbString" value=""/>
</c:otherwise>
</c:choose>



<dsp:droplet name="/com/tru/common/droplet/TRUTealiumCategoryModifierDroplet">
	<dsp:param name="categoryName" value="${breadCrumbString}"/>
	<dsp:param name="pageName" value="product"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="modifiedBreadCrumb" param="modifiedCategory"/>
		<dsp:getvalueof var="taxonomyString" param="breadCrumbLevels"/>
	</dsp:oparam>
</dsp:droplet>

	     <c:if test="${fn:contains(productName,singleQuote)}">
	     <c:set var="productName" value="${fn:replace(productName,singleQuote,slashSingleQuote)}"/>
		 </c:if>
		 <c:if test="${fn:contains(productName,doubleQuote)}">
	     <c:set var="productName" value="${fn:replace(productName,doubleQuote,slashDoubleQuote)}"/>
		 </c:if>
		 <c:set var="productName" value="${productName}"/>

	     <c:if test="${fn:contains(productBrand,singleQuote)}">
	     <c:set var="productBrand" value="${fn:replace(productBrand,singleQuote,slashSingleQuote)}"/>
		 </c:if>
		 <c:if test="${fn:contains(productBrand,doubleQuote)}">
	     <c:set var="productBrand" value="${fn:replace(productBrand,doubleQuote,slashDoubleQuote)}"/>
		 </c:if>
		 <c:set var="productBrand" value="${productBrand}"/>

	   <%-- <c:forEach var="collections" items="${collectionNames}">
	    <c:set var="collectionName" value="${collections.value}"/>
	   </c:forEach> --%>
	   <c:if test="${not empty onlinePid && not empty productName}">
       <c:set var="tcollectionNames" value="${tcollectionNames}'${onlinePid}: ${productName}'"/>
	   </c:if>

<c:choose>
<c:when test="${not empty tcollectionNames}">
<c:set var="productCollection" value="${tcollectionNames}"/>
</c:when>
<c:otherwise>
<dsp:getvalueof var="productCollection" bean="TRUTealiumConfiguration.PDPProductCollection"/>
</c:otherwise>
</c:choose>




<c:choose>
<c:when test="${loginStatus eq 'true'}">
<c:set var="customerStatus" value="Guest"/>
</c:when>
<c:otherwise>
<c:set var="customerStatus" value="Registered"/>
<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
		<dsp:droplet name="IsEmpty">
			<dsp:param name="value" bean="Profile.shippingAddress.id" />
			<dsp:oparam name="false">
				<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="Profile.secondaryAddresses"/>
				<dsp:param name="elementName" value="address"/>
				<dsp:oparam name="output">
				<dsp:getvalueof param="address.id" var="addressId" />
				<c:if test="${addressId eq shippingAddressId}">

						 <dsp:getvalueof param="address.city" var="customerCity"/>
						 <dsp:getvalueof param="address.state" var="customerState"/>
						 <dsp:getvalueof param="address.postalCode" var="customerZip"/>
						 <dsp:getvalueof param="address.country" var="customerCountry"/>

				 </c:if>
				</dsp:oparam>
			   </dsp:droplet>
			</dsp:oparam>
</dsp:droplet>
</c:otherwise>
</c:choose>
	<dsp:droplet name="TRUPriceDroplet">
		<dsp:param name="skuId" value="${productSku}" />
		<dsp:param name="productId" value="${productId}" />
		<dsp:oparam name="true">
			<dsp:getvalueof param="salePrice" var="productUnitPrice" />
			<dsp:getvalueof param="listPrice" var="productListPrice" />
			<dsp:getvalueof param="savedAmount" var="productDiscount" />
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:getvalueof param="salePrice" var="productUnitPrice" />
			<dsp:getvalueof param="listPrice" var="productListPrice" />
					<c:if test="${productUnitPrice == 0}">
						<c:set var="productUnitPrice" value="${productListPrice}"/>
					</c:if>
		</dsp:oparam>
	</dsp:droplet>

<dsp:getvalueof bean="TRUTealiumConfiguration.PDPPageName" var="pdpPageName"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPPageType" var="pdpPageType"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.browserID" var="browserID"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOasTaxonomy" var="oasTaxonomy"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPSiteSection" var="siteSection"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOasSize" var="oasSize"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOasBreadcrumb" var="oasBreadcrumb"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.deviceType" var="deviceType"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPFinderSortGroup" var="finderSortGroup"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPProductFindingMethod" var="productFindingMethod"/>
<%-- <dsp:getvalueof bean="TRUTealiumConfiguration.PDPImageIcon" var="imageIcon"/> --%>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPInternalCampaignPage" var="internalCampaignPage"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOrsoCode" var="orsoCode"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOutOfStockBySku" var="outOfStockBySku"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPProdMerchCategory" var="productMerchandisingCategory"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPProductPathing" var="productPathing"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPRegistryOrigin" var="registryOrigin"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPStrikedPricePage" var="strikedPricePage"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPStrikedPricePageTitle" var="strikedPricePageTitle"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPXsellProduct" var="xsellProduct"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPIspuSource" var="ispuSource"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOasSize" var="oasSize"/>
<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
<%-- <dsp:getvalueof var="sosCookie" value="${cookie.isSOS.value}"/> --%>
<dsp:getvalueof var="productQueryId" value="${onlinePid}" />
<c:if test="${onlinePid eq ''}">
	<dsp:getvalueof var="productQueryId" value="${productId}" />
</c:if>

<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
<c:if test="${site eq babySiteCode}">
	<dsp:getvalueof var="siteCode" value="BRU" />
	<dsp:getvalueof var="baseBreadcrumb" value="bru" />
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
</c:if>

<c:if test="${productDiscount gt '0'}">
	<c:set var="strikedPricePage" value="striked price"/>
	<c:set var="strikedPricePageTitle" value="${siteCode}: ${pdpPageName}: ${productQueryId}: ${productName}"/>
</c:if>
<!-- Added hidden field to fix GEOFUS-1033 -->
<input type="hidden" class="strikedPricePageTitleHidden" value="${strikedPricePageTitle}" />

<c:set var="backSlash" value="\\" />
<c:set var="noSlash" value=""/>
<c:set var="openBracket" value="["/>
<c:set var="closeBracket" value="]"/>

<c:if test="${fn:contains(baseBreadcrumb,backSlash)}">
<c:set var="baseBreadcrumb" value="${fn:replace(baseBreadcrumb,backSlash,slashSingleQuote)}"/>
</c:if>

<c:if test="${fn:contains(modifiedBreadCrumb,backSlash)}">
<c:set var="modifiedBreadCrumb" value="${fn:replace(modifiedBreadCrumb,backSlash,slashSingleQuote)}"/>
</c:if>


<c:if test="${fn:contains(taxonomyString,backSlash)}">
<c:set var="taxonomyString" value="${fn:replace(taxonomyString,backSlash,slashSingleQuote)}"/>
</c:if>

<c:if test="${fn:contains(productFamily,doubleQuote)}">
	<c:set var="productFamily" value="${fn:replace(productFamily,doubleQuote,slashDoubleQuote)}" />
</c:if>
 
<c:if test="${fn:contains(categoryName,singleQuote)}">
	     <c:set var="categoryName" value="${fn:replace(categoryName,singleQuote,slashSingleQuote)}"/>
		 </c:if>

<c:if test="${fn:contains(categoryName,doubleQuote)}">
	<c:set var="categoryName" value="${fn:replace(categoryName,doubleQuote,slashDoubleQuote)}" />
</c:if>

<c:if test="${fn:contains(productCategories,singleQuote)}">
	     <c:set var="productCategories" value="${fn:replace(productCategories,singleQuote,slashSingleQuote)}"/>
		 </c:if>

<c:if test="${fn:contains(productCategories,doubleQuote)}">
	<c:set var="productCategories" value="${fn:replace(productCategories,doubleQuote,slashDoubleQuote)}" />
</c:if> 

<c:if test="${fn:contains(productSubCategories,singleQuote)}">
	     <c:set var="productSubCategories" value="${fn:replace(productSubCategories,singleQuote,slashSingleQuote)}"/>
		 </c:if>

<c:if test="${fn:contains(productSubCategories,doubleQuote)}">
	<c:set var="productSubCategories" value="${fn:replace(productSubCategories,doubleQuote,slashDoubleQuote)}" />
</c:if> 

<c:if test="${fn:contains(outOfStockSkus,openBracket)}">
<c:set var="outOfStockSkus" value="${fn:replace(outOfStockSkus,openBracket,noSlash)}"/>
</c:if>

<c:if test="${fn:contains(outOfStockSkus,closeBracket)}">
<c:set var="outOfStockSkus" value="${fn:replace(outOfStockSkus,closeBracket,noSlash)}"/>
</c:if>

<input type="hidden" value="${customerEmail}" id="customerEmail"/>
<input type="hidden" value="${customerID}" id="customerID"/>
<input type="hidden" value="${productId}" id="productId"/>
<input type="hidden" value="${productName}" id="productName"/>
<input type="hidden" value="${productListPrice}" id="productListPrice"/>
<input type="hidden" value="${productSku}" id="productSku"/>
<input type="hidden" value="${productFamily}" id="productFamily"/>
<input type="hidden" id="partnerName" value="${partnerName}"/>

<dsp:droplet name="/com/tru/commerce/locations/TRUGetCookieDroplet">
	<dsp:param name="cookieName" value="favStore" />
	<dsp:oparam name="output">
		<dsp:getvalueof var="cookieValue" param="cookieValue" />
		<dsp:getvalueof var="locationIdFromCookie" param="locationId" />
	</dsp:oparam>
	<dsp:oparam name="empty">
	</dsp:oparam>
</dsp:droplet>

<c:set var="session_id" value="${cookie.sessionID.value}"/>
<c:set var="visitor_id" value="${cookie.visitorID.value}"/>

<dsp:getvalueof var="searchkeyword" param="searchKey"/>
<c:set var="search_keyword" value='${fn:substring(searchkeyword, 0, 20)}'/>

<c:if test="${not empty search_keyword}">
<c:set var="search_success" value="Product"/>
<c:set var="search_synonym" value="Category:${search_keyword}"/>
<script type="text/javascript">
		$(document).ready(function(){
		    setTimeout(function(){
		    	utag.view({
			    	event_type:'search_performed'
			    })
		    },1000);	
	    });
	</script>
</c:if>

<c:set var="productStrikedPrice" value="${productListPrice}"/>
<c:if test="${productUnitPrice == productListPrice}">
	<c:set var="productStrikedPrice" value=""/>
</c:if>

	
<!-- Start: Script for Tealium Integration -->
<script type="text/javascript">

var utag_data = {
	    customer_status:"${customerStatus}",
	    customer_city:"${customerCity}",
	    customer_country:"${customerCountry}",
	    customer_email:"${customerEmail}",
	    customer_id:"${customerID}",
	    event_type:"",
	    customer_type:"${customerStatus}",
	    customer_state:"${customerState}",
	    customer_zip:"${customerZip}",
	    customer_dob:"${customerDOB}",
	    page_name:"${siteCode}: ${pdpPageName}: ${productQueryId}: ${productName}",
	    page_type:"${siteCode}: ${pdpPageType}",
	    page_category:"${categoryName}",
	    page_subcategory:"${pageSubCategories}",
	    site_section:"${siteSection}",
	    product_brand:["${productBrand}"],
	    product_category:["${productCategories}"],
	    product_id:["${productQueryId}"],
	    product_name:["${productName}"],
	    product_store:["${siteCode}"],
	    product_unit_price:["${productUnitPrice}"],
	    product_list_price:["${productListPrice}"],
	    product_sku :["${productSku}"],
	    product_subcategory:["${productSubCategories}"],
	    product_finding_method:"${productFindingMethod}",
	    product_image_url:["${productImageURL}"],
	    browser_id:"${pageContext.session.id}",
	    Image_icon_new_exclusive:"${promotionalStickerDisplay}",
	    internal_campaign_page:"${internalCampaignPage}",
	    orso_code:"${orsoCode}",
	    out_of_stock_by_sku:"${outOfStockSkus}",
	    product_collection:[${productCollection}],
	    product_merchandising_category:["${siteCode}: ${productCategories}"],
	    product_pathing:"${productPathing}",
	    registry_origin:"${registryOrigin}",
    	striked_price_page:"${productStrikedPrice}",
    	striked_price_page_title:"${strikedPricePageTitle}",
	    xsell_product:"",
	    ispu_source:"en_US:${siteCode}: Product Detail:${productQueryId}:${productName}",
	    device_type:"${deviceType}",
	    oas_breadcrumb:"${baseBreadcrumb}${modifiedBreadCrumb}/productpage",
	    oas_taxonomy:"${taxonomyString}",
	    oas_sizes:${oasSize},
	    finder_sort_group:"${finderSortGroup}",
	    partner_name:"${partnerName}",
	    customer_name:"${customerName}",
	    product_pathing:"${productQueryId}",
	    selected_store:"${selectedStore}",
	    product_impression_id:["${productQueryId}"],
	    product_gridwall_location:[],
	    store_locator:"${locationIdFromCookie}",
	    session_id : "${session_id}",
		visitor_id : "${visitor_id}",
		search_success :"${search_success}",
		search_synonym:"${search_synonym}",
		search_keyword :"${search_keyword}",
		tru_or_bru : "${siteCode}"
	};

	if ($("div.out-of-stock.out-of-stock-message-container:contains('out of stock')").length > 0) {
		utag_data.event_type = ["out_of_stock"];
	}

    (function(a,b,c,d){
	    a='${tealiumURL}';
	    b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
	    a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
    })();

$(window).load(function() {
	if(typeof localStorage != 'undefined'){
	  var productGridwallLocation = localStorage.getItem('pdpGridwallLocation');
	  utag_data.product_gridwall_location = [productGridwallLocation];
	}
});

function loadOmniScript(e){
		var qty=$("#ItemQty").val();
		utag.link({
		    event_type: 'cart_add',
		    customer_email: "${customerEmail}",
		    customer_id: "${customerID}",
		    product_brand: ['${productBrand}'],
		    product_category: ["${productCategories}"],
		    product_id: ['${productQueryId}'],
		    product_name: ['${productName}'],
		    product_unit_price: ["${productUnitPrice}"],
		    product_list_price: ["${productListPrice}"],
		    product_quantity: [qty],
		    product_sku: ['${productSku}'],
		    product_subcategory: ["${productSubCategories}"],
		    product_finding_method: ['${productFindingMethod}'],
		    //product_discount: ['${productDiscount}'],
		    product_store: ['PRODUCT STORE'],
		    cart_addition_source: [e],
		    product_merchandising_category: ['${siteCode}: ${productCategories}'],
		    partner_name: '${partnerName}'
		});
}

</script>
<c:if test="${not empty productFamilyValue}">
<script type="text/javascript">utag_data.product_family=["${productFamilyValue}"];</script>
</c:if>
<%-- <c:if test="${not empty productDiscount}">
<script>utag_data.product_discount=["${productDiscount}"];</script>
</c:if> --%>
<!-- End: Script for Tealium Integration -->
</dsp:page>