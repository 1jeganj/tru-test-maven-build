<dsp:page>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<tru:pageContainer>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
		<jsp:attribute name="fromCollection">Collection</jsp:attribute>
		<jsp:body>		
            
		     <dsp:getvalueof var="collectionId" param="collectionId" />
		<dsp:include page="/product/collectionDetailsPageSelection.jsp">
			<dsp:param name="collectionId" value="${collectionId}" />
			<dsp:param name="page" value="collectionDetails" />
		</dsp:include>
		<input type="hidden" value="Product-Detail" name="currentpage" id="currentpage"/>
		<input type="hidden" value="${collectionId}" name="collectionIdRecentlyViewed" id="collectionIdRecentlyViewed"/>
		
	
		</jsp:body>
	</tru:pageContainer>
</dsp:page>