<dsp:page>
<dsp:importbean bean="/com/tru/commerce/email/TRUEmailNotificationFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
<dsp:getvalueof param="formId" var="formId"/>
<c:choose>
		<c:when test="${formId eq 'notifyMe'}">
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.notifyMeMailId" paramvalue="notifyMeMailId" />
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.itemDescription" paramvalue="itemDescription" />
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.displayName" paramvalue="itemName" />
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.productId" paramvalue="productId" />
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.skuId" paramvalue="itemNumber" />
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.pdpPageURL" paramvalue="itemURL" />
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.helpURLName" paramvalue="helpURLName" />
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.helpURLValue" paramvalue="helpURLValue" />
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.sendSpecialofferToMail" paramvalue="sendSpecialofferToMail"/>
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.siteID" paramvalue="siteID"/>
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.onlinePID" paramvalue="onlinePID"/>
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.notifyMe" value="submit" />
			<dsp:droplet name="Switch">
				<dsp:param bean="TRUEmailNotificationFormHandler.formError" name="value"/>
				<dsp:oparam name="true">
					 <dsp:droplet name="ErrorMessageForEach">
				        <dsp:param name="exceptions" bean="TRUEmailNotificationFormHandler.formExceptions"/>
						<dsp:oparam name="outputStart">
							Following are the form errors:
						</dsp:oparam>
						<dsp:oparam name="output">
						   <div class="notify-error-message"><span class="esrbMatureError"></span><span><dsp:valueof param="message" /></span></div>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="false">
				</dsp:oparam>
			  </dsp:droplet>
		</c:when>
	</c:choose>
</dsp:page>