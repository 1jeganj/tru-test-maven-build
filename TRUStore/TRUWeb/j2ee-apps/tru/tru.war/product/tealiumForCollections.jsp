<dsp:page>
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUPriceDroplet" />
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
<dsp:getvalueof var="siteCode" value="TRU" />
<dsp:getvalueof var="baseBreadcrumb" value="tru" />
<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
<dsp:getvalueof param="searchKeyWord" var="tSearchKeyWord"/>
<dsp:getvalueof param="pageName" var="tPageName"/>
<dsp:getvalueof param="pageType" var="tPageType"/>
<dsp:getvalueof var="customerEmail" bean="Profile.login"/>
<dsp:getvalueof var="customerID" bean="Profile.id"/>
<dsp:getvalueof var="customerDOB" bean="Profile.dateOfBirth"/>
<dsp:getvalueof var="loginStatus" bean="Profile.transient"/>
<c:set var="singleQuote" value="'"/>
<c:set var="doubleQuote" value='"'/>
<c:set var="slashSingleQuote" value="\'"/>
<c:set var="slashDoubleQuote" value='\"'/>

<dsp:getvalueof param="collectionProductInfo" var="collectionProductInfo" />
<dsp:getvalueof param="collectionProductInfo.collectionId" var="collectionId" />
<dsp:getvalueof param="collectionProductInfo.collectionName" var="collectionName" />
<dsp:getvalueof param="collectionProductInfo.productInfoList" var="productInfoList" />
<dsp:droplet name="/atg/dynamo/droplet/ForEach">
    <dsp:param name="array" value="${productInfoList}" />
    <dsp:getvalueof param="element" var="productInfo"/>

    <dsp:oparam name="output">
    	<dsp:droplet name="/com/tru/commerce/droplet/TRUProductCategoryInfoDroplet">
		 	<dsp:param name="productId" value="${productInfo.productId}" />
		 	<dsp:param name="selectedSkuId" value="${productInfo.defaultSKU.id}" />	 	
			<dsp:oparam name="output">
				<dsp:getvalueof param="onlinePidSku" var="onlinePID"/>
			</dsp:oparam>
		 </dsp:droplet>
     	<c:set var="productIds" value="${productIds}'${onlinePID}',"/>
     	<c:set var="productName" value="${productInfo.displayName}"/>
     	<c:choose>
	     <c:when test="${fn:contains(productName,singleQuote)}">
	     <c:set var="productName" value="${fn:replace(productName,singleQuote,slashSingleQuote)}"/>
		 </c:when>
		 <c:when test="${fn:contains(productName,doubleQuote)}">
	     <c:set var="productName" value="${fn:replace(productName,doubleQuote,slashDoubleQuote)}"/>
		 </c:when>
		 <c:otherwise>
		 <c:set var="productName" value="${productName}"/>
		 </c:otherwise>
        </c:choose>
    	<c:set var="productNames" value="${productNames}'${productName}',"/>
    	<c:set var="productBrand" value="${productBrand}'${productInfo.defaultSKU.brandName}',"/>
    	<c:set var="productImageURL" value="${productImageURL}'${productInfo.defaultSKU.primaryImage}',"/>
    	<c:set var="productSku" value="${productSku}'${productInfo.defaultSKU.id}',"/>
		<c:set var="outOfStockBySku" value="${outOfStockBySku}${productInfo.outofStockSKUsList},"/>
		 <%--   <c:forEach var="collections" items="${productInfo.defaultSKU.collectionNames}">
		    <c:set var="collectionKey" value=" ${collections.key}"/>
		    <c:set var="collectionName" value="${collectionName}"/>
		   </c:forEach> --%>
		<c:if test="${not empty onlinePID && not empty productName}">
		 	<c:set var="collectionNames" value="${collectionNames}'${onlinePID}: ${productName}',"/>
		</c:if>
    	<dsp:droplet name="TRUPriceDroplet">
			<dsp:param name="skuId" value="${productInfo.defaultSKU.id}" />
			<dsp:param name="productId" value="${productInfo.productId}" />
			<dsp:oparam name="true">
				<dsp:getvalueof param="salePrice" var="productUnitPrice" />
				<dsp:getvalueof param="listPrice" var="productListPrice" />
				<dsp:getvalueof param="savedAmount" var="productDiscount" />
				<c:set var="productUnitPrices" value="${productUnitPrices}'${productUnitPrice}',"/>
				<c:set var="productListPrices" value="${productListPrices}'${productListPrice}',"/>
				<c:set var="productDiscounts" value="${productDiscounts}'${productDiscount}',"/>
			</dsp:oparam>
		</dsp:droplet>

    </dsp:oparam>
</dsp:droplet>

<dsp:getvalueof var="AValue" param="A"/>

<dsp:getvalueof var="productQueryId" value="${AValue}" />

<c:if test="${AValue eq ''}">
	<dsp:getvalueof var="productQueryId" value="${productId}" />
</c:if>

<!--  Added For Tealium Integration -->
<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>

<c:forEach var="element" items="${contentItem.MainContent}">
			<c:if test="${not empty element.PDPBreadCrumbList}">
						<c:set var="catIndex" value="0"/>
						<c:forEach var="breadList" items="${element.PDPBreadCrumbList}">
						    <c:choose>
						    <c:when test="${catIndex eq '0'}">
							<c:set var="productCategories" value="${productCategories}'${breadList.categoryName}'"/>
							</c:when>
							<c:otherwise>
								<c:choose>
								<c:when test="${catIndex eq element.PDPBreadCrumbList.size()-1}">
								<c:set var="productSubCategories" value="${productSubCategories}'${breadList.categoryName}'"/>
								</c:when>
								<c:otherwise>
								<c:set var="productSubCategories" value="${productSubCategories}'${breadList.categoryName}'/"/>
								</c:otherwise>
								</c:choose>
							</c:otherwise>
							</c:choose>
							<c:set var="catIndex" value="${catIndex+1}"/>
						</c:forEach>
			</c:if>
</c:forEach>

<dsp:getvalueof var="breadCrumbString" value="/${productCategories}/${productSubCategories}"/>

<dsp:droplet name="/com/tru/common/droplet/TRUTealiumCategoryModifierDroplet">
	<dsp:param name="categoryName" value="${breadCrumbString}"/>
	<dsp:param name="pageName" value="product"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="modifiedBreadCrumb" param="modifiedCategory"/>
		<dsp:getvalueof var="taxonomyString" param="breadCrumbLevels"/>
	</dsp:oparam>
</dsp:droplet>

<c:choose>
<c:when test="${not empty collectionNames}">
    <c:set var="productCollection" value="${collectionNames}"/>
</c:when>
<c:otherwise>
<dsp:getvalueof var="productCollection" bean="TRUTealiumConfiguration.PDPProductCollection"/>
</c:otherwise>
</c:choose>

<c:choose>
<c:when test="${loginStatus eq 'true'}">
<c:set var="customerStatus" value="Guest"/>
</c:when>
<c:otherwise>
<c:set var="customerStatus" value="Registered"/>
<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
		<dsp:droplet name="IsEmpty">
			<dsp:param name="value" bean="Profile.shippingAddress.id" />
			<dsp:oparam name="false">
				<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="Profile.secondaryAddresses"/>
				<dsp:param name="elementName" value="address"/>
				<dsp:oparam name="output">
				<dsp:getvalueof param="address.id" var="addressId" />
				<c:if test="${addressId eq shippingAddressId}">

						 <dsp:getvalueof param="address.city" var="customerCity"/>
						 <dsp:getvalueof param="address.state" var="customerState"/>
						 <dsp:getvalueof param="address.postalCode" var="customerZip"/>
						 <dsp:getvalueof param="address.country" var="customerCountry"/>

				 </c:if>
				</dsp:oparam>
			   </dsp:droplet>
			</dsp:oparam>
</dsp:droplet>
</c:otherwise>
</c:choose>
	<dsp:droplet name="TRUPriceDroplet">
		<dsp:param name="skuId" value="${productSku}" />
		<dsp:param name="productId" value="${productId}" />
		<dsp:oparam name="true">
			<dsp:getvalueof param="salePrice" var="productUnitPrice" />
			<dsp:getvalueof param="listPrice" var="productListPrice" />
			<dsp:getvalueof param="savedAmount" var="productDiscount" />
		</dsp:oparam>
	</dsp:droplet>

<dsp:getvalueof value="Collections" var="pdpPageName"/>
<dsp:getvalueof value="Collections" var="pdpPageType"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.browserID" var="browserID"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOasTaxonomy" var="oasTaxonomy"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPSiteSection" var="siteSection"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOasSize" var="oasSize"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOasBreadcrumb" var="oasBreadcrumb"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.deviceType" var="deviceType"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPFinderSortGroup" var="finderSortGroup"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPPageCategory" var="pageCategory"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPPageSubcategory" var="pageSubcategory"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPProductFindingMethod" var="productFindRingMethod"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPImageIcon" var="imageIcon"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPInternalCampaignPage" var="internalCampaignPage"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOrsoCode" var="orsoCode"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPProdMerchCategory" var="productMerchandisingCategory"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPProductPathing" var="productPathing"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPRegistryOrigin" var="registryOrigin"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPStrikedPricePage" var="strikedPricePage"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPStrikedPricePageTitle" var="strikedPricePageTitle"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPXsellProduct" var="xsellProduct"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPIspuSource" var="ispuSource"/>
<dsp:getvalueof bean="TRUTealiumConfiguration.PDPOasSize" var="oasSize"/>
<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>

<dsp:getvalueof var="AValue" param="A"/>
<dsp:getvalueof var="productQueryId" value="${AValue}" />
<dsp:getvalueof var="customerFirstName" bean="Profile.firstName"/>
	<dsp:getvalueof var="customerLastName" bean="Profile.lastName"/>
	<c:set var="space" value=" "/>
	<c:choose>
	<c:when test="${empty customerFirstName && empty customerLastName}">
		<c:set var="customerName" value='${fn:substring(customerEmail, 0, fn:indexOf(customerEmail, "@"))}'/>
	</c:when>
	<c:when test="${not empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value="${customerFirstName}"/>
	</c:when>
	<c:when test="${empty customerFirstName && not empty customerLastName}">
		   <c:set var="customerName" value="${customerLastName}"/>
	</c:when>
	<c:otherwise>
			<c:set var="customerName" value="${customerFirstName}${space}${customerLastName}"/>
	</c:otherwise>
 </c:choose>

<c:if test="${AValue eq ''}">
	<dsp:getvalueof var="productQueryId" value="${productId}" />
</c:if>
<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />

<c:if test="${site eq babySiteCode}">
	<dsp:getvalueof var="siteCode" value="BRU" />
	<dsp:getvalueof var="baseBreadcrumb" value="bru" />
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
</c:if>

<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
<c:choose>
<c:when test="${isSosVar eq 'true'}">
<c:set var="selectedStore" value="sos"/>
</c:when>
<c:otherwise>
<c:set var="selectedStore" value=""/>
</c:otherwise>
</c:choose>

<c:set var="noBracket" value=""/>
<c:set var="openBracket" value="["/>
<c:set var="closeBracket" value="]"/>

<c:if test="${fn:contains(outOfStockBySku,openBracket)}">
<c:set var="outOfStockBySku" value="${fn:replace(outOfStockBySku,openBracket,noSlash)}"/>
</c:if>

<c:if test="${fn:contains(outOfStockBySku,closeBracket)}">
<c:set var="outOfStockBySku" value="${fn:replace(outOfStockBySku,closeBracket,noSlash)}"/>
</c:if>

<script type='text/javascript'>

var utag_data = {
	    customer_status:"${customerStatus}",
	    customer_city:"${customerCity}",
	    customer_country:"${customerCountry}",
	    customer_email:"${customerEmail}",
	    customer_id:"${customerID}",
	    event_type:"",
	    customer_type:"${customerStatus}",
	    customer_state:"${customerState}",
	    customer_zip:"${customerZip}",
	    customer_dob:"${customerDOB}",
	    page_name:"${siteCode}: ${pdpPageName}: ${collectionId}: ${collectionName}",
	    page_type:"${siteCode}: ${pdpPageType}",
	    page_category:"${pageCategory}",
	    page_subcategory:"${pageSubcategory}",
	    site_section:"${siteSection}",
	    product_brand:[${productBrand}],
	    product_category:[${productCategories}],
	    product_id:[${productIds}],
	    product_name:[${productNames}],
	    product_unit_price:[${productUnitPrices}],
	    product_list_price:[${productListPrices}],
	    product_sku :[${productSku}],
	    product_subcategory:[${productSubCategories}],
	    product_finding_method:"${productFindingMethod}",
	    product_image_url:[${productImageURL}],
	    browser_id:"${pageContext.session.id}",
	    Image_icon_new_exclusive:"${imageIcon}",
	    internal_campaign_page:"${internalCampaignPage}",
	    orso_code:"${orsoCode}",
	    out_of_stock_by_sku:"${outOfStockBySku}",
	    product_collection:[${productCollection}],
	    product_merchandising_category:["${siteCode}: ${productCategories}"],
	    product_pathing:"${productPathing}",
	    registry_origin:"${registryOrigin}",
    	striked_price_page:"${strikedPricePage}",
    	striked_price_page_title:"${strikedPricePageTitle}",
	    xsell_product:"",
	    ispu_source:"${ispuSource}",
	    device_type:"${deviceType}",
	    oas_breadcrumb:"${baseBreadcrumb}${modifiedBreadCrumb}/productpage",
	    oas_taxonomy:"${taxonomyString}",
	     oas_sizes:${oasSize},
	    finder_sort_group:"${finderSortGroup}",
	    partner_name:"${partnerName}",
	    customer_name:"${customerName}",
	    selected_store:"${selectedStore}",
	    tru_or_bru : "${siteCode}"
	};

</script>
<%-- <c:if test="${not empty productDiscounts}">
	    <script>utag_data.product_discount=[${productDiscounts}];</script>
	    </c:if> --%>
<!-- Start: Script for Tealium Integration -->
		<script type="text/javascript">
		    (function(a,b,c,d){
		    a='${tealiumURL}';
		    b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
		    a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
		    })();
		</script>
<!-- End: Script for Tealium Integration -->
</dsp:page>