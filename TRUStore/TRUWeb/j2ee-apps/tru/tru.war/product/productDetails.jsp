<dsp:page>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof value="${originatingRequest.contextPath}" var="contextPath" />
<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
	<dsp:getvalueof var="productId" param="productId" />
	<dsp:getvalueof var="onlineProductId" param="onlineProductId" />	
	<%-- Included product details specific pages --%>
	<tru:pageContainer>
	
	<jsp:attribute name="fromPdp">pdp</jsp:attribute>
		<jsp:body>
		

			    
				<input type="hidden" value="${onlineProductId}" name="onlinePId" id="onlinePID"/>
				
		<dsp:include page="/product/productDetailsPageSelection.jsp">
			<dsp:param name="productId" value="${productId}"/>
			<dsp:param name="onlineProductId" value="${onlineProductId}"/>
			<dsp:param name="page" value="productDetails"/>
		</dsp:include>	
		 <input type="hidden" class="contextPath" value="${contextPath}">
		 <input type="hidden" value="Product-Detail" name="currentpage" id="currentpage"/>
		 	<iframe id="recentlyViewedItemsIframe" src= "/securePage/securePage.jsp"></iframe>
		
		 	
		
		</jsp:body>
	</tru:pageContainer>
</dsp:page>