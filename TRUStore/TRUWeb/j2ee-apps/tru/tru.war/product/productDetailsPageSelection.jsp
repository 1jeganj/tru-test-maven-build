<dsp:page>
	<dsp:getvalueof var="page" param="page"/>
	<dsp:getvalueof var="onlineProductId" param="onlineProductId"></dsp:getvalueof>
		<dsp:getvalueof var="productFamily" param="productFamily"/>

	<c:if test="${page eq 'productDetails'}">
		<dsp:droplet name="/com/tru/commerce/droplet/TRUProductInfoLookupDroplet">
			<dsp:param name="productId" param="productId"/>
			<dsp:param name="onlineProductId" value="${onlineProductId}"/>
			<dsp:param name="siteId" bean="/atg/multisite/Site.id"/>
			<dsp:oparam name="output">
				<dsp:include page="/jstemplate/productDetailsTemplate.jsp">
					<dsp:param name="templateName" value="${templateName}"/>
					<dsp:param name="productInfo" param="productInfo"/>
				</dsp:include>
				<dsp:include page="/product/tealiumParameters.jsp">
				<dsp:param name="productInfo" param="productInfo"/>
				<dsp:param name="productFamily" param="productFamily"/>

				</dsp:include>
			</dsp:oparam>
			<dsp:oparam name="empty">
				<dsp:include page="/jstemplate/unavailableProductDetailsTemplate.jsp" />
			</dsp:oparam>
		</dsp:droplet>
	</c:if>
</dsp:page>