<dsp:page>
	<dsp:getvalueof var="page" param="page"/>
	<c:if test="${page eq 'collectionDetails'}">
	<dsp:droplet name="/com/tru/commerce/droplet/TRUCollectionProductInfoLookupDroplet">
					<dsp:param name="collectionId" param="collectionId" />
					<dsp:param name="siteId" bean="/atg/multisite/Site.id" />
					<dsp:oparam name="output">
						<dsp:include page="/jstemplate/collectionDetailsTemplate.jsp">
						<dsp:param name="collectionProductInfo" param="collectionProductInfo"/>
						<dsp:param name="templateName" value="collectionDetails"/>
						</dsp:include>
			       </dsp:oparam>
			       <dsp:oparam name="empty">
						<dsp:include page="/jstemplate/unavailableCollectionDetailsTemplate.jsp" />
					</dsp:oparam>
		</dsp:droplet> 
	</c:if>
</dsp:page>