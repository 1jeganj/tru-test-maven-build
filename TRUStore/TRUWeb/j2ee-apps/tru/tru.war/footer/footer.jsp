<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
	<dsp:importbean bean="/atg/endeca/assembler/droplet/InvokeAssembler" />
	<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations" />
	<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
	<dsp:importbean bean="com/tru/common/TRUStoreConfiguration"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration" />
	<dsp:importbean bean="/com/tru/radial/common/TRURadialConfiguration" />
	<dsp:importbean bean="/com/tru/integrations/common/TRUHookLogicConfiguration"/>
	<dsp:getvalueof var="hooklogicURL" bean="TRUHookLogicConfiguration.hooklogicURL"/>
	<dsp:getvalueof var="enableMinification"  bean="TRUStoreConfiguration.enableJSDebug"/>
  <dsp:getvalueof var="debugFlag" param=="debug"/>
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
	<dsp:getvalueof var="locale" bean="Profile.locale" />
	<dsp:getvalueof var="footerPage" bean="EndecaConfigurations.footerPage" />
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:getvalueof var="srchDexurl" bean="TRUConfiguration.searchDexUrl"/>
	<dsp:getvalueof var="srchDexFooterPage" bean="TRUConfiguration.searchDexFooterPage"/>
	<dsp:getvalueof var="isFileExist" value="0" vartype="java.lang.Integer"/>
	<dsp:getvalueof var="showWelcomeBack" vartype="java.lang.boolean" value="${cookie.showWelcomeBack.value}"></dsp:getvalueof>
	<dsp:getvalueof var="sessionExpired" vartype="java.lang.boolean" param="sessionExpired"></dsp:getvalueof>
	<dsp:getvalueof var="abandonCheckbox" vartype="java.lang.boolean" value="${cookie.abandonCheckbox.value}"></dsp:getvalueof>
	<dsp:getvalueof var="itemCountCookie" vartype="java.lang.boolean" value="${cookie.itemCountCookie.value}"></dsp:getvalueof>
	<dsp:getvalueof var="staticAssetsVersionFormat" bean="TRUStoreConfiguration.staticAssetsVersion"/>
	<dsp:getvalueof var="versionFlag" bean="TRUStoreConfiguration.versioningFlag"/>
	<dsp:getvalueof var="fromSubCat" param="fromSubCat" />
	<dsp:getvalueof var="directionPageName" param="directionPageName" />
	<dsp:getvalueof var="fromFamily" param="fromFamily" />
	<dsp:getvalueof var="catID" param="catID" />
	<dsp:getvalueof var="fromCat" param="fromCat" />
	<dsp:getvalueof var="pageName" param="pageName" />
	<dsp:getvalueof var="requestUrl" bean="/OriginatingRequest.requestURL" />
	<dsp:getvalueof var="domainPath" bean="TRUConfiguration.domainUrl" />
	<dsp:getvalueof var="shopLocalURL" bean="TRUConfiguration.shopLocalURL" />
	<dsp:importbean bean="/atg/multisite/Site" />
	<dsp:getvalueof var="siteName" bean="Site.id" />
	<dsp:getvalueof var="footerConfigurationJS" bean="TRUStoreConfiguration.footerConfigurationJS"/>
	<dsp:getvalueof var="footerConfigurationCSS" bean="TRUStoreConfiguration.footerConfigurationCSS"/>
	<dsp:getvalueof var="radialPaymentJSPath" bean="TRURadialConfiguration.radialPaymentJSPath"/>
	<dsp:getvalueof var="customerID" bean="Profile.id"/>
	<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />

	<c:if test="${siteName eq babySiteCode}">
		<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
	</c:if>

	<input type="hidden" id="epsilonCust_ID" value="${customerID}"/>
	<input type="hidden" id="epsilonPartnerName" value="${partnerName}"/>
	<input type="hidden" value="https://apps.toysrus.com/tru/us/dp" id="epslonEmailURL"/>
	<c:choose>
		<c:when test="${siteName eq 'ToysRUs'}" >
			<dsp:getvalueof var="contextRoot" value="tru" />
		</c:when>
		<c:when test="${siteName eq 'BabyRUs'}" >
			<dsp:getvalueof var="contextRoot" value="bru" />
		</c:when>
	</c:choose>

	<c:choose>
		<c:when test="${fn:containsIgnoreCase(requestUrl, shopLocalURL)}">
			<c:set var="domainUrl" value="${domainPath}" />
		</c:when>
		<c:otherwise>
			<c:set var="domainUrl" value="" />
		</c:otherwise>
	</c:choose>

	<input type="hidden" value="${sessionExpired}"  name="sessionExpired" class="sessionExpired">
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<fmt:message key="tru.home.invalidEmailID" var="invalidEmail" />
	<fmt:message key="homePage.emailBlank.message" var="blankEmail" />
	<input type="hidden" value="${invalidEmail}"  name="invalidEmail" id="invalidEmailIdFooterSignUp">
	<input type="hidden" value="${blankEmail}"  name="blankEmail" id="blankEmailIdFooterSignUp">
	<dsp:getvalueof value='<%=atg.nucleus.DynamoEnv.getProperty("staticAssetVersion")%>' var="versionNumbers"/>
	<fmt:message key="tru.home.signUpSuccessMessage" var="signUpSuccessMessage" />
	<input type="hidden" value="${signUpSuccessMessage}"  name="signUpSuccessMessage" id="signUpSuccessMessageFooterSignUp">

	<c:set var="isSosVar" value="${cookie.isSOS.value}"/>

	<c:choose>
		<c:when test="${site eq 'sos' and isSosVar}">
			<dsp:getvalueof var="cacheKey" value="/InvokeAssembler${siteName}Footer${site}${locale}" />
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="cacheKey" value="/InvokeAssembler${siteName}Footer${locale}" />
		</c:otherwise>
	</c:choose>

	<dsp:droplet name="/com/tru/cache/TRUHomePageAssemblerCache">
		<dsp:param name="key" value="${cacheKey}" />
		<dsp:oparam name="output">
			<dsp:droplet name="InvokeAssembler">
				<dsp:param name="includePath" value="${footerPage}" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="footContentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
				</dsp:oparam>
			</dsp:droplet>
			<c:if test="${not empty footContentItem}">
				<c:set var="footerContentItem" value="${footContentItem}" scope="application"/>
			</c:if>
		</dsp:oparam>
	</dsp:droplet>

	<c:if test="${directionPageName eq 'getDirection'}">
		<c:set var="directionFooterClass" value="get-direction-page-footer" scope="page"/>
	</c:if>

	<div class="footer-grid ${directionFooterClass}">
		<div class="default-margin footer-grid-list">
			<div class="row row-center row-no-padding">
				<c:choose>
					<c:when test="${fromCat eq 'cat' or fromFamily eq 'Family' or fromSubCat eq 'subcat'}">
						<esi:include src="/trusearchdex/Archive/sdf-c$(QUERY_STRING{'categoryid'}).html"
							alt="/trusearchdex/Archive/sdf-${contextRoot}-home.html" onerror="continue" />
					</c:when>
					<c:otherwise>
						<esi:include src="/trusearchdex/Archive/sdf-${contextRoot}-home.html"
							alt="/trusearchdex/Archive/sdf-${contextRoot}-home.html" onerror="continue" />
					</c:otherwise>
				</c:choose>
			</div>
		</div>

		<c:forEach var="element" items="${footerContentItem.FooterContent}">
			<c:if test="${element['@type'] eq 'TruFooterSearchTargetter'}">
				<dsp:renderContentItem contentItem="${element}" />
			</c:if>
		</c:forEach>

		<div class="footer-images">
			<c:forEach var="element" items="${footerContentItem.FooterContent}">
				<c:if test="${element['@type'] eq 'TruHtmlFooterContent-ATGTargeter'}">
					<dsp:renderContentItem contentItem="${element}" />
				</c:if>
			</c:forEach>
		</div>

		<c:forEach var="element" items="${footerContentItem.FooterContent}">
			<c:if test="${element['@type'] ne 'TruHtmlFooterContent-ATGTargeter' &&  element['@type'] ne 'TruFooterSearchTargetter'}">
				<dsp:renderContentItem contentItem="${element}" />
			</c:if>
		</c:forEach>
	</div>

	<c:choose>
		<c:when test="${enableMinification and debugFlag eq '1'}">
			<c:choose>
				<c:when test="${versionFlag}">
				<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/jquery.mCustomScrollbar.concat.min.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/toolkit.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/jquery.tmpl.min.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/jquery.validate.min.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/tru_clientSideValidation.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/pwstrength.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/reward-validation.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/zipCodePlugin.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/jquery-cookie.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/tRus_custom.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/cart.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/endeca.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/storelocator.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/copy-cookie.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/knockout-3.4.0.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/knockout-mapping.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/Spa.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/powerReview.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/productDetails.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/truLayaway.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/msDropDown.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/tRus_checkout.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
				</c:when>
				<c:otherwise>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/jquery.mCustomScrollbar.concat.min.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/toolkit.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/jquery.tmpl.min.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/jquery.validate.min.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/tru_clientSideValidation.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/pwstrength.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/reward-validation.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/zipCodePlugin.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/jquery-cookie.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/tRus_custom.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/cart.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/endeca.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/storelocator.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/copy-cookie.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/knockout-3.4.0.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/knockout-mapping.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/Spa.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/powerReview.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/productDetails.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/truLayaway.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/msDropDown.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/tRus_checkout.js"></script>
					<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/cart.js"></script>
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${versionFlag}">
					<script type="text/javascript" charset="UTF-8"
					src="${TRUJSPath}javascript/globalFooter.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					<!-- Added JS and CSS for Footer Configuration -->
					<c:if test="${not empty footerConfigurationCSS}">
						<link rel="stylesheet" href="${footerConfigurationCSS}?${staticAssetsVersionFormat}=${versionNumbers}">
					</c:if>
					<c:if test="${not empty footerConfigurationJS}">
						<script type="text/javascript" charset="UTF-8" src="${footerConfigurationJS}?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					</c:if>
				</c:when>
				<c:otherwise>
					<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/globalFooter.js"></script>
					<!-- Added JS and CSS for Footer Configuration -->
					<c:if test="${not empty footerConfigurationCSS}">
						<link rel="stylesheet" href="${footerConfigurationCSS}">
					</c:if>
					<c:if test="${not empty footerConfigurationJS}">
						<script type="text/javascript" charset="UTF-8" src="${footerConfigurationJS}"></script>
					</c:if>
				</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>

	<c:if test="${pageName eq 'myAccount' || pageName eq 'myAccountLayaway' || pageName eq 'myOrderHistory' || pageName eq 'resetPassword'}">
		<c:choose>
			<c:when test="${enableMinification and debugFlag eq '1'}">
				<c:choose>
					<c:when test="${versionFlag}">
						<script type="text/javascript" charset="UTF-8"
							src="${TRUJSPath}javascript/truLayaway.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script type="text/javascript" charset="UTF-8"
							src="${TRUJSPath}javascript/tRus_radialOmCustom.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
						<script type="text/javascript" charset="UTF-8"
							src="${TRUJSPath}javascript/truMyAccount.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					</c:when>
					<c:otherwise>
						<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/truLayaway.js"></script>
						<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/tRus_radialOmCustom.js"></script>
						<script type="text/javascript" charset="UTF-8"src="${TRUJSPath}javascript/truMyAccount.js"></script>
					</c:otherwise>
				</c:choose>
				<c:if test="${pageName eq 'myAccountLayaway'}">
					<dsp:include page="/checkout/payment/fraudJSFiles.jsp">
						<dsp:param name="versionFlag" value="${versionFlag}" />
						<dsp:param name="staticAssetsVersionFormat" value="${staticAssetsVersionFormat}" />
						<dsp:param name="versionNumbers" value="${versionNumbers}" />
					</dsp:include>
					<script type="text/javascript" charset="UTF-8" src="${radialPaymentJSPath}"></script>
				</c:if>
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${pageName eq 'myAccount' or pageName eq 'resetPassword'}">
						<c:choose>
							<c:when test="${versionFlag}">
								<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/accountPageFooter.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
								<script type="text/javascript" charset="UTF-8" src="${radialPaymentJSPath}"></script>
							</c:when>
							<c:otherwise>
								<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/accountPageFooter.js"></script>
								<script type="text/javascript" charset="UTF-8" src="${radialPaymentJSPath}"></script>
							</c:otherwise>
						</c:choose> 
					</c:when>
					<c:when test="${pageName eq 'myAccountLayaway'}">
						<c:choose>
							<c:when test="${versionFlag}">
								<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/layawayPageFooter.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
								<script type="text/javascript" charset="UTF-8" src="${radialPaymentJSPath}"></script>
							</c:when>
							<c:otherwise>
								<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/layawayPageFooter.js"></script>
								<script type="text/javascript" charset="UTF-8" src="${radialPaymentJSPath}"></script>
							</c:otherwise>
						</c:choose>
						<dsp:include page="/checkout/payment/fraudJSFiles.jsp">
							<dsp:param name="versionFlag" value="${versionFlag}" />
							<dsp:param name="staticAssetsVersionFormat" value="${staticAssetsVersionFormat}" />
							<dsp:param name="versionNumbers" value="${versionNumbers}" />
						</dsp:include>
					</c:when>
					<c:when test="${pageName eq 'myOrderHistory'}">
						<c:choose>
							<c:when test="${versionFlag}">
								<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/orderHistoryPageFooter.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
							</c:when>
							<c:otherwise>
								<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/orderHistoryPageFooter.js"></script>
							</c:otherwise>
						</c:choose>
					</c:when>
				</c:choose>
			</c:otherwise>
		</c:choose>
	</c:if>

	<c:if test="${fromSubCat eq 'subcat' or fromFamily eq 'Family'}">
		<c:choose>
			<c:when test="${enableMinification and debugFlag eq '1'}">
				<c:choose>
					<c:when test="${versionFlag}">
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/subCatPageFooter.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					</c:when>
					<c:otherwise>
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/productCompare.js"></script>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${versionFlag}">
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/subCatPageFooter.js?${staticAssetsVersionFormat}=${versionNumbers}"></script>
					</c:when>
					<c:otherwise>
						<script type="text/javascript" charset="UTF-8" src="${TRUJSPath}javascript/subCatPageFooter.js"></script>
					</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>
	</c:if>

	<dsp:include page="/common/syncCookieIframe.jsp"></dsp:include>

	<div id="crossDomainCookieIframeLoader" class="hide"></div>

	<script type="text/javascript">
		var sessionExpired = '${sessionExpired}';
	</script>

	<div id="welcomeBackModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="display: none;">
		<dsp:include page="/cart/abandonCartOverlay.jsp"/>
	</div>

	<div id="basicModal"></div>

	<script type="text/javascript">
		$(window).load(function(){
			$( '#showafterLoad' ).addClass( 'showNow' );

			syncCookieByIframe();
		});

		var ViewModalObject = new SearchViewModel();
		ko.applyBindings(ViewModalObject);
	</script>
</dsp:page>