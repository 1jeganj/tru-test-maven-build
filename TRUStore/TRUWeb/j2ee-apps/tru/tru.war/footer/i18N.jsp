<dsp:page>
<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
<dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
<dsp:getvalueof var="locale" bean="/atg/userprofiling/Profile.locale" />
	<dsp:getvalueof var="atgTargeterpath" value="/atg/registry/RepositoryTargeters/TRU/Footer/CountryRegionTargeter" />
	<dsp:getvalueof var="cacheKey" 	value="${atgTargeterpath}${siteId}${locale}" />
	<dsp:getvalueof var="urlPagename" param="urlPagename" />
	<dsp:getvalueof var="urlAB" param="urlAB" />
	<dsp:getvalueof var="urlCatID" param="urlCatID" />
	<dsp:getvalueof var="cat" param="cat" />
	<dsp:getvalueof var="catdim" param="catdim" />
	
	
	<dsp:getvalueof var="plpPathCheck" bean="TRUStoreConfiguration.plpPathCheck" />
		
	

	<c:if test="${(urlPagename eq 'category' or urlPagename eq 'family' or urlPagename eq 'subcat') and plpPathCheck eq 'true'}">

		<dsp:droplet name="/com/tru/droplet/TRUPLPBreadcrumbsCookieDroplet">
			<dsp:param name="urlPagename" param="urlPagename" />
		
			<dsp:param name="urlAB" param="urlAB" />
			<dsp:param name="urlCatID" param="urlCatID" />
			<dsp:param name="cat" param="cat" />
			<dsp:param name="catdim" param="catdim" />
			<dsp:oparam name="output">
			</dsp:oparam>
			<dsp:oparam name="empty">
			</dsp:oparam>
		</dsp:droplet>

	    </c:if>



	<dsp:droplet name="/com/tru/droplet/TRUPDPBreadcrumbsCookieDroplet">
		<dsp:param name="urlPagename" param="urlPagename"/>
		<dsp:param name="urlAB" param="urlAB"/>
		<dsp:param name="urlCatID" param="urlCatID"/>
		<dsp:param name="cat" param="cat"/>
		<dsp:oparam name="output">
		</dsp:oparam>
		<dsp:oparam name="empty">
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="/com/tru/cache/TRUCountryRegionTargeterCacheDroplet">
		<dsp:param name="key" value="${cacheKey}" />
		<dsp:oparam name="output">
			<dsp:droplet name="/atg/targeting/TargetingFirst">
				<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/Footer/CountryRegionTargeter" />
				<dsp:oparam name="output">
					<dsp:valueof param="element.data" valueishtml="true" />
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>
