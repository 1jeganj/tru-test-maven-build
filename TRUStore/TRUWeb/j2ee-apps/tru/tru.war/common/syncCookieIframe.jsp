<dsp:importbean bean="/com/tru/droplet/TRUGetSiteDroplet" />
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id" />
<dsp:droplet name="Switch">
	<dsp:param name="value" bean="TRUConfiguration.enableSyncCookie"/>
	<dsp:oparam name="true">
		<div class='collapse'></div>
		<c:choose>
			<c:when test="${siteName eq 'ToysRUs'}">
			<dsp:droplet name="TRUGetSiteDroplet">
				<dsp:param name="siteId" value='BabyRUs' />
				<dsp:oparam name="output">
					<dsp:getvalueof var="alternateSiteURL" param="siteUrl"/>
				</dsp:oparam>
			</dsp:droplet>
		</c:when>
		<c:otherwise>
			<dsp:droplet name="TRUGetSiteDroplet">
				<dsp:param name="siteId" value='ToysRUs' />
				<dsp:oparam name="output">
					<dsp:getvalueof var="alternateSiteURL" param="siteUrl"/>
				</dsp:oparam>
			</dsp:droplet>
	    	</c:otherwise>
	    </c:choose>
		<script type="text/javascript">
			var iFrameURL='';
			$(document).ready(function(){
				iFrameURL=location.protocol+'//${alternateSiteURL}/common/childFrame.jsp';
			});
			
			function syncCookieByIframe(){
				$('.collapse').empty();
				$('<iframe src="'+iFrameURL+'" frameborder="0" scrolling="no" id="myFrame"></iframe>').appendTo('.collapse');  
			}
			
		</script>
	</dsp:oparam>
	<dsp:oparam name="default">
		<script type="text/javascript">
			//Dummy method to avoid js errors when syncCookie is disabled.
			function syncCookieByIframe(){
			}
			
		</script>
	</dsp:oparam>
</dsp:droplet>	