

<dsp:page>
<div class="row row-no-padding footer-social">
           <div class="col-md-12 col-no-padding">
                <div class="socialBanner">
                    <a href="https://www.facebook.com/toysrus?ref=br_tf" target="_blank">
                    <div class="facebook" title="ToysRUs Facebook">
                    </a>
                    <a href="https://www.pinterest.com/toysrus/" target="_blank">
                        <div class="ToysRUsPinterest" title="ToysRUs Pinterest"></div>
                    </a>
                    <a href="https://twitter.com/toysrus" target="_blank">
                    <div class="twitter" title="ToysRUs Twitter"></div>
                    </a>
                    <a href="https://www.youtube.com/user/ToysRUsOnline" target="_blank">
                    <div class="truyoutube" title="ToysRUs Youtube"></div>
                    </a>
                </div>
            </div>
        </div>
		
</dsp:page>