<dsp:page>
	<dsp:importbean bean="/com/tru/commerce/droplet/JSONInvokeAssemblerDroplet"/>
	<%-- Start InvokeAssembler Droplet--%>
	<dsp:droplet name="JSONInvokeAssemblerDroplet">
  		<dsp:param name="includePath" value="/pages/ToysRUs/typeAhead"/>
  		<dsp:oparam name="output">
    		<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
  		</dsp:oparam>
	</dsp:droplet>


</dsp:page>