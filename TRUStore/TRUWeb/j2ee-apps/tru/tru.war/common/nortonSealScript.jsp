<dsp:page>
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>

<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
	<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
	<c:choose>
	<c:when test="${isSosVar eq 'true'}">
		<c:choose>
		  <c:when test="${site eq 'ToysRUs'}">
	 	   <dsp:getvalueof var="nortonSealURL" bean="TRUConfiguration.sosTruNortonSealURL"/>
		  </c:when>
	      <c:otherwise>
		  <dsp:getvalueof var="nortonSealURL" bean="TRUConfiguration.sosBruNortonSealURL"/>
		  </c:otherwise>					   	   
		</c:choose>
	</c:when>
	<c:otherwise>
		  <c:choose>
		  <c:when test="${site eq 'ToysRUs'}">
	 	   <dsp:getvalueof var="nortonSealURL" bean="TRUConfiguration.nortonSealURL"/>
		  </c:when>
	      <c:otherwise>
		  <dsp:getvalueof var="nortonSealURL" bean="TRUConfiguration.brunortonSealURL"/>
		  </c:otherwise>					   	   
		 </c:choose>
	</c:otherwise>
	</c:choose>
	
<table class="w3cNortonTableStyle">
<tr>
<td>

<script src="${nortonSealURL}"></script>

</tr>
</table>
</dsp:page>