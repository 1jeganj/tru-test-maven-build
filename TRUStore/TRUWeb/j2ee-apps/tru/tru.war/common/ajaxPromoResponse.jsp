<dsp:page>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUSkuPromotionDetailsDroplet"/>
<dsp:importbean bean="/com/tru/commerce/droplet/TRUInventoryDetailsDroplet"/>

<dsp:getvalueof var="skuPromoDetails" param="skuPromoDetails" />
<dsp:getvalueof var="pramoID" param="pramoID" />
<dsp:getvalueof var="SKU_PRICE_ARRAY" param="SKU_PRICE_ARRAY" />
<dsp:getvalueof var="enablePriceFromATG" bean="/atg/multisite/Site.enablePriceFromATG"/>
<dsp:droplet name="TRUSkuPromotionDetailsDroplet">
	<dsp:param name="sku" value="${skuPromoDetails}" />
		<dsp:param name="promoId" value="${pramoID}" />
	<dsp:oparam name="output">
		<%-- <dsp:getvalueof var="promotionDetails" param="promotionDetails"/> --%>
		<dsp:getvalueof var="promotionDisplay" param="promotionDisplay"/>
</dsp:oparam> 
</dsp:droplet>


<dsp:droplet name="TRUInventoryDetailsDroplet">
	<dsp:param name="sku" value="${skuPromoDetails}" />
	<dsp:param name="skuStoreEligibleArray" value="${skuStoreEligibleArray}" />
	<dsp:oparam name="output">
	<dsp:getvalueof var="addToCartDetailMap" param="addToCartDetailMap"/>
	</dsp:oparam> 
</dsp:droplet>


<c:if test="${enablePriceFromATG eq true}">
<dsp:droplet name="/com/tru/commerce/droplet/TRUEndecaPriceDroplet">
			<dsp:param name="sku" value="${SKU_PRICE_ARRAY}" />
			<dsp:oparam name="output">
			<dsp:getvalueof var="priceMapJSON" param="priceMapJSON"/>
			</dsp:oparam>
			<dsp:oparam name="empty">
			</dsp:oparam>
		</dsp:droplet>

</c:if>

<c:if test="${empty priceMapJSON }">
	<dsp:getvalueof var="priceMapJSON" value="{}"/>
</c:if>

<c:if test="${empty addToCartDetailMap }">
	<dsp:getvalueof var="addToCartDetailMap" value="{}"/>
</c:if>

{
"priceMapJSON":${priceMapJSON},
"promotionJSON":${promotionDisplay},
"inventryJSON":${addToCartDetailMap}
}

</dsp:page>