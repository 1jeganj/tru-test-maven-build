<dsp:page>
<dsp:importbean bean="/com/tru/droplet/TRUAjaxLogsLookupDroplet"/>

<dsp:getvalueof var="requestURL" param="requestURL"></dsp:getvalueof>
<dsp:getvalueof var="exceptionDetails" param="status"></dsp:getvalueof>

<dsp:droplet name="TRUAjaxLogsLookupDroplet">
<dsp:param name="requestURL" value="${requestURL}"/>
<dsp:param name="exceptionDetails" value="${exceptionDetails}"/>
</dsp:droplet>
</dsp:page>