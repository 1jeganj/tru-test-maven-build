<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<fmt:message key="searchPage.see.more" var="seeMore" />
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
	<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id" />
	<input type="hidden" id="seeMore" value="${seeMore}"/>
	
		<dsp:getvalueof var="urlFullContextPath" param="urlFullContextPath" />
	


	<%-- <script type="text/javascript">
                            $(document).ready(function(event){
	                            $("#searchText").keyup(function(){
		                             $("#searchText").endecaSearchSuggest({
		                                     minAutoSuggestInputLength: 3,
		                                     collection: "${component.contentCollection}",
		                                     autoSuggestServiceUrl: '${contextPath}common/typeAhead.jsp',
		                                     searchUrl: '<%=request.getContextPath()%>',
																	containerClass : 'search-terms'
																});
											})
						});
	</script>
 --%>


	<form action="${urlFullContextPath}search" id="searchForm">
		<c:choose>
			<c:when test="${siteName eq 'ToysRUs' }">
				<fmt:message key="home.ghostMessageTRU" var="searchPlaceHolderTxt" />
			</c:when>
			<c:when test="${siteName eq 'BabyRUs' }">
				<fmt:message key="home.ghostMessageBRU" var="searchPlaceHolderTxt" />
			</c:when>
			<c:otherwise>
			</c:otherwise>
		</c:choose>

		<div id="global-nav-typeahead" class="global-nav-search-bar">
			<span class="twitter-typeahead">
				<input type="text" class="tt-input" id="searchText" title="searchTextHere"
				aria-label="search for something to keep baby happy"
				autocomplete="off" spellcheck="false" dir="auto" name="keyword"
				placeholder='${searchPlaceHolderTxt}'> <span
				class="tt-dropdown-menu">
					<span class="tt-dataset-tru"> </span>
			</span>
			</span>
			<div class="search errorMessage" id="searchErrorMessage"><fmt:message key="homePage.searchBlanck.message"/></div>
		</div>
		<img class="typeahead-arrow" src="${TRUImagePath}images/arrow.svg"
			alt="up arrow">

	</form>
</dsp:page>