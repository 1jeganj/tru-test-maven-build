<dsp:page>
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration"/>
<dsp:importbean bean="/OriginatingRequest"/>
<dsp:importbean bean="/atg/userprofiling/Profile" />
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/OriginatingRequest"/>
<dsp:importbean bean="/com/tru/droplet/OmnitureTrackingCookieDroplet"/>
<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
<dsp:getvalueof var="siteCode" value="TRU" />
<dsp:getvalueof var="baseBreadcrumb" value="tru" />
 <dsp:getvalueof var="loginStatus" bean="Profile.transient" />
 <dsp:getvalueof var="customerEmail" bean="Profile.login"/>
 <dsp:getvalueof var="customerId" bean="Profile.id"/>
 <dsp:getvalueof var="customerDob" bean="Profile.dateOfBirth"/>
 <dsp:getvalueof var="pageName" bean="TRUTealiumConfiguration.homePageName"/>
 <dsp:getvalueof var="pageType" bean="TRUTealiumConfiguration.homePageType"/>
 <dsp:getvalueof var="browserId" bean="TRUTealiumConfiguration.browserId"/>
 <dsp:getvalueof var="deviceType" bean="TRUTealiumConfiguration.deviceType"/>
 <dsp:getvalueof var="oasBreadcrumb" bean="TRUTealiumConfiguration.homeOASBreadcrumb"/>
 <dsp:getvalueof var="oasTaxonomy" bean="TRUTealiumConfiguration.homeOASTaxonamy"/>
 <dsp:getvalueof var="oasSizes" bean="TRUTealiumConfiguration.homeOASSizes"/>
 <dsp:getvalueof var="customerType" bean="TRUTealiumConfiguration.customerType"/>
<dsp:getvalueof var="tealiumURL" bean="TRUTealiumConfiguration.tealiumURL"/>
 <dsp:getvalueof var="internalCampaignPage" bean="TRUTealiumConfiguration.internalCampaignPage"/>
 <dsp:getvalueof var="orsoCode" bean="TRUTealiumConfiguration.orsoCode"/>
 <dsp:getvalueof var="ispuSource" bean="TRUTealiumConfiguration.ispuSource"/>
 <dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode"/>
 <dsp:getvalueof var="quesryString" bean="/OriginatingRequest.queryString" />
 <dsp:getvalueof var="customerFirstName" bean="Profile.firstName"/>
  <dsp:getvalueof var="customerLastName" bean="Profile.lastName"/>
  
	<c:set var="space" value=" "/>
	<c:choose>
	<c:when test="${empty customerFirstName && empty customerLastName}">
		<c:set var="customerName" value='${fn:substring(customerEmail, 0, fn:indexOf(customerEmail, "@"))}'/>
	</c:when>
	<c:when test="${not empty customerFirstName && empty customerLastName}">
			<c:set var="customerName" value="${customerFirstName}"/>
	</c:when>
	<c:when test="${empty customerFirstName && not empty customerLastName}">
		   <c:set var="customerName" value="${customerLastName}"/>
	</c:when>
	<c:otherwise>
			<c:set var="customerName" value="${customerFirstName}${space}${customerLastName}"/>
	</c:otherwise>
 </c:choose>
  <c:choose>
	<c:when test="${loginStatus eq 'true'}">
		<c:set var="customerStatus" value="Guest"/>
	</c:when>
	<c:otherwise>
		<c:set var="customerStatus" value="Registered"/>
		<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"></dsp:getvalueof>
		<dsp:droplet name="IsEmpty">
			<dsp:param name="value" bean="Profile.shippingAddress.id" />
			<dsp:oparam name="false">
				<dsp:droplet name="ForEach">
					<dsp:param name="array" bean="Profile.secondaryAddresses"/>
					<dsp:param name="elementName" value="address"/>
					<dsp:oparam name="output">
						<dsp:getvalueof param="address.id" var="addressId" />
						<c:if test="${addressId eq shippingAddressId}">
							 <dsp:getvalueof param="address.city" var="customerCity"/>
							 <dsp:getvalueof param="address.state" var="customerState"/>
							 <dsp:getvalueof param="address.postalCode" var="customerZip"/>
							 <dsp:getvalueof param="address.country" var="customerCountry"/>
						 </c:if>
					</dsp:oparam>
			   </dsp:droplet>
			</dsp:oparam>
		</dsp:droplet>
	</c:otherwise>
</c:choose>

<c:set var="isSosVar" value="${cookie.isSOS.value}"/>
<c:choose>
<c:when test="${isSosVar eq 'true'}">
<c:set var="selectedStore" value="sos"/>
</c:when>
<c:otherwise>
<c:set var="selectedStore" value=""/>
</c:otherwise>
</c:choose>
	<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.toysPartnerName" />
		<c:if test="${site eq babySiteCode}">
			<dsp:getvalueof var="siteCode" value="BRU" />
			<dsp:getvalueof var="baseBreadcrumb" value="bru" />
			<dsp:getvalueof var="partnerName"	bean="TRUTealiumConfiguration.babyPartnerName" />
		</c:if>

<dsp:droplet name="/com/tru/commerce/droplet/TRUProductInfoLookupDroplet">
	<dsp:param name="productId" value="${element.productId}"/>
	<dsp:param name="siteId" bean="/atg/multisite/Site.id"/>
		<dsp:oparam name="output">
	        <dsp:getvalueof param="productInfo.defaultSKU.relatedProducts" var="relatedProducts"/>
	        <c:set var="orderXsellProduct" value="${orderXsellProduct}'${relatedProducts}'${separator}"/>
       </dsp:oparam>
</dsp:droplet>


<dsp:droplet name="/com/tru/commerce/locations/TRUGetCookieDroplet">
	<dsp:param name="cookieName" value="favStore" />
	<dsp:oparam name="output">
		<dsp:getvalueof var="cookieValue" param="cookieValue" />
		<dsp:getvalueof var="locationIdFromCookie" param="locationId" />
	</dsp:oparam>
	<dsp:oparam name="empty">
	</dsp:oparam>
</dsp:droplet>


<dsp:droplet name="OmnitureTrackingCookieDroplet">
	<dsp:param name="id" bean="Profile.id" />
	  <dsp:param name="loginStatus" value="${loginStatus}" />
	<dsp:oparam name="output">
	<dsp:getvalueof var="visitor_id" param="visitorID"/>
	<dsp:getvalueof var="session_id" param="sessionID"/>
	</dsp:oparam>
</dsp:droplet>


<%-- <c:set var="session_id" value="${cookie.sessionID.value}"/>
<c:set var="visitor_id" value="${cookie.visitorID.value}"/> --%>

<script type='text/javascript'>
			  var utag_data = {
			    customer_status : "",
			    page_name : "${siteCode}: ${pageName}",
			    page_type : "${siteCode}: ${pageType}",
			    browser_id : "${pageContext.session.id}",
			    device_type : "${deviceType}",
			    event_type:"",
			    oas_breadcrumb : "${baseBreadcrumb}/${oasBreadcrumb}",
			    oas_taxonomy : "${oasTaxonomy}",
			    oas_sizes : "${oasSizes}",
			    customer_city : "${customerCity}",
			    customer_country : "${customerCountry}",
			    customer_email : "${customerEmail}",
			    customer_id : "",
			    customer_type : "",
			    customer_state : "${customerState}",
			    customer_zip : "${customerZip}",
			    customer_dob : "${customerDob}",
			    internal_campaign_page : "${internalCampaignPage}",
			    orso_code : "${ orsoCode}",
			    ispu_source : "${ispuSource}",
			    partner_name:"${partnerName}",
			    customer_name:"${customerName}",
			    selected_store:"${selectedStore}",
			    xsell_product:[${orderXsellProduct}],
			    store_locator:"${locationIdFromCookie}",
			    session_id : "",
			    visitor_id : "",
			    tru_or_bru : "${siteCode}"
			};

			if ( certonaBCC_product_grid !== undefined ) {
				utag_data.certona_product_grid = certonaBCC_product_grid;
			} else {
				utag_data.certona_product_grid = true;
			}
			
		</script>
<!-- Start: Script for Tealium Integration -->
		<script type="text/javascript">
				(function(a,b,c,d){
				a='${tealiumURL}';
				b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
				a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
				})();

		</script>
<!-- End: Script for Tealium Integration -->
</dsp:page>