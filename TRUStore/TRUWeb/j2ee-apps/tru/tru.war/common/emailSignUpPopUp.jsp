<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<fmt:message key="homePage.emailBlank.message" var="blankEmail" />
<fmt:message key="homepage.validEmail.message" var="invalidEmail" />
<fmt:message key="homepage.validEmail.submit" var="submit" />
<dsp:importbean bean="/atg/targeting/TargetingFirst" />
<dsp:importbean
	bean="/atg/registry/RepositoryTargeters/TRU/AccountManagement/PrivacyPolicyContentTargeter" />
<dsp:importbean bean="/com/tru/common/TRUTealiumConfiguration"/>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:getvalueof var="site" bean="/atg/multisite/Site.id" />
	<dsp:getvalueof var="babySiteCode" bean="TRUTealiumConfiguration.babySiteCode" />
	<dsp:getvalueof var="partnerName" bean="TRUTealiumConfiguration.toysPartnerName" />
	<c:if test="${site eq babySiteCode}">
		<dsp:getvalueof var="partnerName" bean="TRUTealiumConfiguration.babyPartnerName" />
	</c:if>
	<dsp:getvalueof var="isTransient" bean="Profile.transient" />
	<dsp:getvalueof var="customerID" bean="Profile.id" />

<script id="emailSignUpScript" type="text/x-jquery-tmpl">

{{if location.host.indexOf('.babiesrus.com') == -1}}


{{if isSubscriptionPopUpRequired == true }}

<div id="epslonEmailPopup"></div>
<form name="lightBoxSubmit" id="lightBoxSubmit" class=""  action="" method="POST">
	<div id="emailSignUpModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="basicModal"aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content sharp-border email-sign-up-overlay">
	<a href="javascript:void(0)"  data-dismiss="modal" id="close-email-popup" title="close model">
	<span class="sprite-icon-x-close"></span>
	</a>
	<h2><fmt:message key="homePage.signup.today"/></h2>
	<div class="email-sign-up-form">
	<p><fmt:message key="tru.homePage.specialoffers"/></p>
	<div class="row email-input-group">
	<label class="txtemail" for="emailPopupTxt"><fmt:message key="homePage.email.address"/></label>
	<div class="sign-up-input-btn">
	<input type="text" name="email" id="emailPopupTxt" value="" maxlength="60"/>
	<input type="hidden" value="${invalidEmail}" name="invalidEmail" id="invalidEmailId">
	<input type="hidden" value="${blankEmail}" name="blankEmail" id="blankEmailId">
	

    <input id="submitEmail" type="button" value="${submit}"/>
    <input id="customer_ID" type="hidden" value="${customerID}"/>
    <input id="partner_Name" type="hidden" value="${partnerName}"/>
	</div>
	
    <a 	href="#" class="" data-toggle="modal" data-target="#privacyPolicyModal"><fmt:message key="myaccount.privacy.policy" /></a>
	</div>
	<div class="email-checkbox-group">
	 <span class="checkbox-sm-off" id="checkboxMsgId" tabindex="0"></span>
	<span class=""><fmt:message key="homePage.please.donotshowmwssage"/></span>
	</div>
	</div>
	<div class="email-sign-up-thanks">
	<p>You'll start to receive special savings and more within two days.</p>
	</div>
	</div>
	</div>
	</div>
</form>

			<dsp:droplet name="TargetingFirst">
				<dsp:param name="targeter" bean="PrivacyPolicyContentTargeter" />
				<dsp:oparam name="output">
					<dsp:valueof param="element.data" valueishtml="true" />
				</dsp:oparam>
			</dsp:droplet>
{{/if}}

{{/if}}
</script>
</dsp:page>