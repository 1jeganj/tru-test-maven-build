<dsp:page>
	<dsp:importbean bean="/com/tru/commerce/email/TRUEmailNotificationFormHandler" />

	<dsp:getvalueof var="formID" param="formID" scope="request" />
	<dsp:getvalueof var="email" param="email" scope="request" />
    <dsp:setvalue bean="TRUEmailNotificationFormHandler.email"  />
	<c:choose>
		<c:when test="${formID eq 'emailSubmit'}">
			<dsp:setvalue bean="TRUEmailNotificationFormHandler.sendNewsLetter" value="submit" />
		</c:when>
		<c:when test="${formID eq 'emailCancel'}">
			<dsp:setvalue bean="TRUEmailNotificationFormHandler.cancelSubscriptionPopUp"  value="submit" />
		</c:when>
		<c:when test="${formID eq 'checkBox'}">
			<dsp:setvalue bean="TRUEmailNotificationFormHandler.donotShowSubscriptionPopUp"  value="submit" />
		</c:when>
	</c:choose>
	
</dsp:page>