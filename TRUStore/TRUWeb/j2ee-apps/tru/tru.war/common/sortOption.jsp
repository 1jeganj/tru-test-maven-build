
<dsp:page>

	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<div class="col-md-6 text-right col-narrow-by">
		<div class="btn-group sort-by">
			<button class="btn btn-default dropdown-toggle"
				data-toggle="dropdown" aria-expanded="false">
				<span class="sort-by-title">sort by</span> <span id="sort-by-arrow"
					class="sort-by-down-arrow"></span>
			</button>
			<input type="hidden" value="sort by" id="selectedSortLabel" />
			<ul class="dropdown-menu" role="menu">
				<li><img class="sort-by-dropdown-arrow" src="images/arrow-grey.svg" alt="arrow_grey_svg" aria-label = "dropdown_arrow" title="arrow_grey_svg"></li>
				<li class="search-best-match"></li>
				<dsp:getvalueof var="Ns" value="${Ns}" />
				<dsp:getvalueof var="Ntt" param="keyword" />

				<c:forEach var="element" items="${contentItem.sortOptions}">

					<dsp:getvalueof var="sortUrl" value="${element.navigationState}" />
					<c:if test="${fn:contains(sortUrl,'Nr')}">
						<dsp:getvalueof var="sortUrlA" value="${fn:substringBefore(sortUrl,'Nr')}" />
						<dsp:getvalueof var="sortUrlB" value="${fn:substringAfter(sortUrl,'Nr')}" />
						<dsp:getvalueof var="sortUrlB" value="${fn:substringAfter(sortUrlB,'&')}" />
						<dsp:getvalueof var="sortUrl" value="${sortUrlA}${sortUrlB}" />
					</c:if>

					<dsp:getvalueof var="sortUrl" value="${fn:replace(sortUrl,'%7C','|')}" />
					<c:choose>
						<c:when test="${empty Ntt and element.label eq 'best match'}">
							
						</c:when>
						<c:otherwise>
							<li><a href="javascript:void(0);" id="${sortUrl}">${element.label}</a>
							</li>
						</c:otherwise>
					</c:choose>
					
				</c:forEach>
			</ul>
		</div>

	</div>
</dsp:page>
