<dsp:page>
	<dsp:getvalueof param="familyName" var="familyName" />
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<!-- ko if: totalRecords()>lastRecordNumber()  -->
	<div class="load-more-btn-section"> <!-- modified for B&S Spark Changes -->
		<div class="loadMoreProducts family-load-more read-more text-center" data-bind="click:loadMore" tabindex="0" onclick="javascript:utag.link({'event_type':'view_more','product_family':'${familyName}'});">
			<fmt:message key="tru.subcat.load"/><span data-bind="text:$root.howmany"></span><fmt:message key="tru.subcat.moreItems"/></div>
	</div>
<!-- /ko -->
</dsp:page>


