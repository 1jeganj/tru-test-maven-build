<dsp:page>
       <dsp:importbean bean="/atg/endeca/assembler/droplet/InvokeAssembler" />
       
       
       <dsp:getvalueof var="siteId" bean="/atg/multisite/Site.id" />
       

       <dsp:droplet name="InvokeAssembler">
              <dsp:param name="includePath" value="/pages/${siteId}/ajaxpage" />
              <dsp:oparam name="output">
                     <dsp:getvalueof var="ContentItem"
                           vartype="com.endeca.infront.assembler.ContentItem"
                           param="contentItem" />
              </dsp:oparam>
              <dsp:getvalueof var="Content" value="${ContentItem.contents}" />
			  
			
              
<dsp:getvalueof var="default" param="${default}" />


              <c:forEach var="element1" items="${Content}">
			  
			

                     <c:forEach var="element" items="${element1.MainProduct}">
					 
					 
                           <c:if test="${element['@type'] eq 'TRUGuidedNavigation'}">
                                  <dsp:getvalueof var="navigationlist" value="${element.navigation}" />
										 
								 
										 
                     </c:if>
					 
					 
                           <c:if test="${element['@type'] eq 'TRUCrumbFacets'}">
                                  <dsp:getvalueof var="refinementCrumbs"
                                         value="${element.refinementCrumbs}" />
                     </c:if>
                     <c:if test="${element['@type']  eq 'TRUResultsList'}">
              <dsp:getvalueof var="resultsListItem" value="${element}" />
              </c:if>
                     </c:forEach>
					 
				
                     <c:forEach var="element" items="${element1.MainProduct}">
                           
                                  <dsp:renderContentItem contentItem="${element}">
                                         <dsp:param name="refinementCrumbs" value="${refinementCrumbs}" />
                                         <dsp:param name="resultsListItem" value="${resultsListItem}" />
										      <dsp:param name="navigationlist" value="${navigationlist}" />
										 
                                                <dsp:param name="default" value="${default}" />
                                  
              </dsp:renderContentItem>
                     </c:forEach>
              </c:forEach>
       </dsp:droplet>
</dsp:page>
