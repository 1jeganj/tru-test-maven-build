<!DOCTYPE html>
<%@page import="com.tru.orcl.coh.entity.ItemQuantityKey"%>
<%@page import="com.tru.orcl.coh.entity.ItemQuantity"%>
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Random"%>
<%@ page import="java.util.Iterator"%> 
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>

<%@ page import="java.util.ArrayList"%>

<%@ page import="com.tangosol.net.NamedCache"%>
<%@ page import="com.tangosol.net.CacheFactory"%>
<%@ page import="com.tangosol.util.extractor.ReflectionExtractor"%>
<%@ page import="com.tangosol.util.aggregator.QueryRecorder"%>
<%@ page import="com.tangosol.util.aggregator.QueryRecorder.RecordType"%>
<%@ page import="com.tangosol.util.filter.AllFilter"%>
<%@ page import="com.tangosol.util.filter.AndFilter"%>
<%@ page import="com.tangosol.util.filter.EqualsFilter"%>
<%@ page import="com.tangosol.util.processor.NumberIncrementor"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
    </head>
    <body>
	<form name="QueryCache" method="post" action="">
		<table>
		<tr><td colspan="2"><b>When querying against Network Cache (Home delivery), leave the Facility Name blank</b></td></tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td>Item name</td><td align="left"><input type="text" name="itemName" id="itemName" /></td>
		</tr>
		<tr>
			<td>Facility name</td><td align="left"><input type="text" name="facilityName" id="facilityName" /></td>
		</tr>
		<tr>
			<td></td><td><input type="submit" value="Submit" style="background-color: #4CAF50;"/></td>
		</tr>
		</table>
	</form>
	
	<% if (request.getParameter("itemName")!=null && request.getParameter("itemName").toString().trim().length()>0)  {
		
		Random rand = new Random();
		
		String status=null;
		NamedCache ic = CacheFactory.getCache("ItemQuantity");
		ItemQuantity qty = new ItemQuantity();
		
			
		ItemQuantityKey key = new ItemQuantityKey(request.getParameter("itemName"),request.getParameter("facilityName"));
		qty = (ItemQuantity)ic.get(key);
		if (qty !=null){
			if(qty.getAtcStatus()==16771){
				 status= "Available";
			}else{
				 status="Out Of Stock";
			}
	%>
	<br/><hr/><br/>
	<table>
		<tr><td>Item Name:</td><td><%= request.getParameter("itemName")%></td></tr>
		<tr><td>Facility Name:</td><td><%= request.getParameter("facilityName")%></td></tr>
		<tr><td>Quantity:</td><td><%= qty.getAtcQuantity()%></td></tr>
		<tr><td>Status:</td><td><%if(status.equalsIgnoreCase("Available")){%><font color="blue"><%=status%></font><%}else{%><font color="red"><%=status%></font><%}%></td></tr>
		<tr><td>Created:</td><td><%= qty.getCreated()%></td></tr>
		<tr><td>LastUpdated:</td><td><%= qty.getLastUpdated()%></td></tr>
		<tr><td>Record Updated:</td><td><%= qty.getRecordUpdated()%></td></tr>
	</table>
	<%}
		else {%>
		<br/><hr/><br/>
		<table>
		<tr><td>No record found in the Coherence Cache</td></tr>
		</table>
		<%
		}
		}%>
	</body>
</html>