<dsp:page>
	<dsp:importbean bean="/atg/targeting/TargetingFirst" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
	<dsp:importbean bean="/atg/multisite/Site" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean
		bean="/atg/commerce/endeca/cache/DimensionValueCacheDroplet" />
	<dsp:importbean bean="/com/tru/endeca/utils/EndecaConfigurations" />
	<dsp:getvalueof var="truSiteMapTargeter" bean="EndecaConfigurations.truSiteMapTargeter" />
	<dsp:getvalueof var="bruSiteMapTargeter" bean="EndecaConfigurations.bruSiteMapTargeter" />
	<dsp:getvalueof var="siteMapToolTargeter" bean="EndecaConfigurations.siteMapToolTargeter" />
	<dsp:getvalueof var="siteName" bean="/atg/multisite/Site.id" />
	<tru:pageContainer>
		<jsp:attribute name="fromsiteMap">siteMap</jsp:attribute>
		<jsp:body>


<div class="default-margin product-by-cat-zone row-category-header">
                <div class="brand-block-row">
				

						
					<div class="row">
					    <div class="col-md-12">
						    <ul class="category-breadcrumb">
								<li>
									<a href="/">
										home
									</a>
								</li>
								<li>Site Map</li>
							</ul>
					   </div>
					</div>	
				        
				         
				        
                                
                 <div class="row shop-by-cats">
                 
                        <c:if test="${siteName eq 'ToysRUs'}">
                           <dsp:droplet
								name="/atg/targeting/TargetingFirst">
					 <dsp:param name="targeter"
									bean="${truSiteMapTargeter}"/>
					 <dsp:oparam name="output">
						<dsp:valueof param="element.data" valueishtml="true" />
					 </dsp:oparam>
				 </dsp:droplet>
                        </c:if>
                        <c:if test="${siteName eq 'BabyRUs'}">
                        <dsp:droplet
								name="/atg/targeting/TargetingFirst">
				    <dsp:param name="targeter"
									bean="${bruSiteMapTargeter}" />
				    <dsp:oparam name="output">
				       <dsp:valueof param="element.data" valueishtml="true" />
				    </dsp:oparam>
				 </dsp:droplet>
                        </c:if>
                        
                     <dsp:droplet name="/atg/targeting/TargetingFirst">
					 <dsp:param name="targeter"
								bean="${siteMapToolTargeter}" />
					 <dsp:oparam name="output">
						 <dsp:valueof param="element.data" valueishtml="true" />
					 </dsp:oparam>
				 </dsp:droplet>
                  </div>
                  <!-- <hr class="all-brands-sec-hr"> -->
                  <div class="section-gap"></div>
				  <div class="row">
                        <div class="col-md-12">
                              <header>Products By Categories</header>
                  </div>
				  
				   <div class="row">
                        <div class="col-md-12">
                             <ul class="all-products-sec">
							   <li><a href="javascript:void(0);" rel="0-9">0-9</a></li>
							   <li class="active"><a href="javascript:void(0)" rel="A-B">A-B</a></li>
							   <li><a href="javascript:void(0)" rel="C-D">C-D</a></li>
							   <li><a href="javascript:void(0)" rel="E-F">E-F</a></li>
							   <li><a href="javascript:void(0)" rel="D">G-H</a></li>
							   <li><a href="javascript:void(0)" rel="E">I-J</a></li>
							   <li><a href="javascript:void(0)" rel="F">K-L</a></li>
							   <li><a href="javascript:void(0)" rel="G">M-N</a></li>
							   <li><a href="javascript:void(0)" rel="H">O-P</a></li>
							   <li><a href="javascript:void(0)" rel="I">Q-R</a></li>
							   <li><a href="javascript:void(0)" rel="J">S-T</a></li>
							   <li><a href="javascript:void(0)" rel="K">U-V</a></li>
							   <li><a href="javascript:void(0)" rel="L">W-X</a></li>
							   <li><a href="javascript:void(0)" rel="M">Y-Z</a></li>
							 </ul>
				            <!-- <hr class=" all-brands-sec-hr"> -->
				            <div class="section-gap"></div>
                        </div>
                   </div>
				   
				    <div class="product-categories">					
                        
                        <div class="col-md-12" id="character">
                        <dsp:droplet
									name="/com/tru/commerce/droplet/MegaMenuDroplet">
						    <dsp:param name="catalogId" bean="Site.defaultCatalog.id" />
					        <dsp:param name="siteId" bean="Site.id" />
					        <dsp:param name="elementName" value="rootCategories" />
							<dsp:oparam name="output">
							<div class="char-section">	
								<dsp:droplet name="ForEach">
									<dsp:param name="array" param="rootCategories" />
									<dsp:param name="elementName" value="rootCategory" />
									<dsp:oparam name="output">
									<dsp:getvalueof param="count" var="counter" />
									<dsp:getvalueof param="rootCategory.categoryName"
														var="rootCategoryName" />
									<dsp:getvalueof param="rootCategory.categoryURL"
														var="rootCategoryURL" />
									<dsp:getvalueof param="rootCategory.categoryImage"
														var="categoryImage" />
									<dsp:getvalueof param="rootCategory.categoryId"
														var="level1CategoryId" />
								
									<dsp:droplet name="DimensionValueCacheDroplet">
                                   <dsp:param name="repositoryId"
															value="${level1CategoryId}" />
                                   <dsp:oparam name="output">
                                   <dsp:getvalueof
																var="level1CategoryCacheEntry"
																param="dimensionValueCacheEntry" />
                                   </dsp:oparam>
                                     </dsp:droplet>
								
									
									
									
									<c:set var="rootCategory"
														value="${fn:replace(rootCategoryName,' ', '')}" />
									
								
										<div class="${fn:toLowerCase(rootCategory)}">
												<header>${rootCategoryName}</header>
												<ul class="Level-1">
												
												<li><a
																href="${originatingRequest.contextPath}siteMapResults?N=${level1CategoryCacheEntry.dimvalId}">${rootCategoryName}</a></li>
												<li>
												<ul class="Level-2">
												<dsp:droplet name="ForEach">
													<dsp:param name="array" param="rootCategory.subCategories" />
													<dsp:param name="elementName" value="levelOneCategory" />
													<dsp:oparam name="output">
													<dsp:getvalueof var="levelOneCategoryName"
																				param="levelOneCategory.categoryName" />
													<dsp:getvalueof param="levelOneCategory.categoryURL"
																				var="levelTwoCategoryURL" />
													<dsp:getvalueof param="levelOneCategory.categoryId"
																				var="level2CategoryId" />
													
													<dsp:droplet name="DimensionValueCacheDroplet">
                                                       <dsp:param
																					name="repositoryId" value="${level2CategoryId}" />
                                                       <dsp:oparam
																					name="output">
                                                      <dsp:getvalueof
																						var="level2CategoryCacheEntry"
																						param="dimensionValueCacheEntry" />
                                                     </dsp:oparam>
                                                  </dsp:droplet>
													
													
													</dsp:oparam>
														<li><a
																			href="${originatingRequest.contextPath}siteMapResults?N=${level2CategoryCacheEntry.dimvalId}">${levelOneCategoryName}</a></li>
														 <li>
														 <ul class="Level-3">
														<dsp:droplet name="ForEach">
																	<dsp:param name="array"
																						param="levelOneCategory.subCategories" />
																	<dsp:param name="elementName" value="levelTwoCategory" />
																	
																	<dsp:oparam name="output">
																	<dsp:getvalueof var="levelTwoCategoryName"
																							param="levelTwoCategory.categoryName" />
													                <dsp:getvalueof
																							param="levelTwoCategory.categoryURL"
																							var="levelThreeCategoryURL" />
													                <dsp:getvalueof
																							param="levelTwoCategory.categoryId"
																							var="level3CategoryId" />
													
													<dsp:droplet name="DimensionValueCacheDroplet">
                                                       <dsp:param
																								name="repositoryId" value="${level3CategoryId}" />
                                                       <dsp:oparam
																								name="output">
                                                      <dsp:getvalueof
																									var="level3CategoryCacheEntry"
																									param="dimensionValueCacheEntry" />
                                                     </dsp:oparam>
                                                  </dsp:droplet>
													                
													                
													                
																	
																	</dsp:oparam>
																	 <li><a
																						href="${originatingRequest.contextPath}siteMapResults?N=${level3CategoryCacheEntry.dimvalId}">${levelTwoCategoryName}</a></li>
																	<li>
																	<ul class="Level-4">
																	<dsp:droplet name="ForEach">
																	<dsp:param name="array"
																									param="levelTwoCategory.subCategories" />
																	<dsp:param name="elementName"
																									value="levelThreeCategory" />
																	
																	<dsp:oparam name="output">
																	
																	<dsp:getvalueof var="levelThreeCategoryName"
																										param="levelThreeCategory.categoryName" />
													                <dsp:getvalueof
																										param="levelThreeCategory.categoryURL"
																										var="levelFourCategoryURL" />
																	
																	
																	</dsp:oparam>
																	
																	<li>
																	<a
																									href="${levelFourCategoryURL}">${levelThreeCategoryName}</a>
																	</li>
																	
																	</dsp:droplet>
																	
																	</ul>
																	</li>
																	
																	
																	</dsp:droplet>
																	</ul>
																	</li>
													</dsp:droplet>
													</ul>
													</li>
												</ul>
										</div>
						          </dsp:oparam>
						  </dsp:droplet>
					   </div>
                      </dsp:oparam>
							</dsp:droplet>
                      
                      
                        </div>
            
                   </div>
                   
                   
                   <div class="row">
                        <div class="col-md-12">
                             <ul class="all-products-sec">
							   <li><a href="javascript:void(0);" rel="0-9">0-9</a></li>
							   <li class="active"><a href="javascript:void(0)" rel="A-B">A-B</a></li>
							   <li><a href="javascript:void(0)" rel="C-D">C-D</a></li>
							   <li><a href="javascript:void(0)" rel="E-F">E-F</a></li>
							   <li><a href="javascript:void(0)" rel="D">G-H</a></li>
							   <li><a href="javascript:void(0)" rel="E">I-J</a></li>
							   <li><a href="javascript:void(0)" rel="F">K-L</a></li>
							   <li><a href="javascript:void(0)" rel="G">M-N</a></li>
							   <li><a href="javascript:void(0)" rel="H">O-P</a></li>
							   <li><a href="javascript:void(0)" rel="I">Q-R</a></li>
							   <li><a href="javascript:void(0)" rel="J">S-T</a></li>
							   <li><a href="javascript:void(0)" rel="K">U-V</a></li>
							   <li><a href="javascript:void(0)" rel="L">W-X</a></li>
							   <li><a href="javascript:void(0)" rel="M">Y-Z</a></li>
							 </ul>
				            <hr class=" all-brands-sec-hr">
                        </div>
                   </div>
                   
                   
                   
                   
				</div>
			</div>
		</div>
		</jsp:body>

	</tru:pageContainer>
</dsp:page>