<%@ taglib prefix="dsp"
	uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_1"%>
<dsp:page>
	<html lang="en">
<head>
</head>
<body>
	<style>
.textFont {
	
	font-size: 13px;
}

.bold {
	
}

.red {
	color: red;
}
</style>
<dsp:getvalueof var="noOfRecords" param="noOfRecords"/>
<dsp:getvalueof var="exception" param="exception"/>
<dsp:getvalueof var="feedStatus" param="feedStatus"/>
		<c:choose>
		  	<c:when test="${feedStatus eq 'success'}">
		  		<p><dsp:valueof param="messageContent" /></p>	
		  		
		  		<br />
				<table border="1">
				<tr>
					<td>Date & Time</td>
					<td>Total Records Received</td>
					<td>Total Records Extracted Successfully</td>
					<td>Total Records Failed To Extract</td>
				</tr>
				<tr>
					<td><dsp:valueof
					bean="/atg/dynamo/service/CurrentDate.timeAsTimeStamp" /></td>
						<td><dsp:valueof param="totalRecordCount"/></td>
						<td><dsp:valueof param="successRecordCount"/></td>
						<td><dsp:valueof param="totalFailedRecordCount"/></td>
				</tr>
				</table>
				<br />
				<br />
		  	</c:when>
		  	<c:otherwise>
		  		<p><dsp:valueof param="messageContent" /></p>	
		  		
		  		<br />
		  			<li><b>Status  :</b> <dsp:valueof value="Price audit export failed" /><br/></li>
					<li><b>OS Host Name : </b> <dsp:valueof param="OSHostName"/><br/></li>
					<li><b>Detail Exception :</b> <dsp:valueof param="detailException" /><br/></li>	
					<br />
				<br />
		  		
		  		
		  		<dsp:valueof param="exception" /><br/>
		  	</c:otherwise>
		</c:choose>
		
		<br />
		<br />
	</body>
</dsp:page>
