<%@ taglib prefix="dsp"
	uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_1"%>
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<html>
<head>
</head>
<body>
	<style>
.textFont {
	
	font-size: 13px;
}

.bold {
	
}

.red {
	color: red;
}
</style>

		<p> <dsp:valueof param="messageContent"/> </p>
		
		<p>
		<table border="1">
			<tr>
				<td>Date & Time</td>
				<td>Total Records Received</td>
				<td>Total Records Processed Successfully</td>
				<td>Total Records Failed</td>
			</tr>
			<tr>
				<td><dsp:valueof
			bean="/atg/dynamo/service/CurrentDate.timeAsTimeStamp" /></td>
				<td><dsp:valueof param="totalRecordCount"/></td>
				<td><dsp:valueof param="totalSuccessRecordCount"/></td>
				<td><dsp:valueof param="totalFailedRecordCount"/></td>
			</tr>
		</table>
		</p>
</body>
	</html>
</dsp:page>