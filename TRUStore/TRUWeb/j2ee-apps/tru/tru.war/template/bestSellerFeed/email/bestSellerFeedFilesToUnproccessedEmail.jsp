<%@ taglib prefix="dsp"
	uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_1"%>
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<html>
<head>
</head>
<body>
	<style>
.textFont {
	
	font-size: 13px;
}

.bold {
	
}

.red {
	color: red;
}
</style>
	<p>Hi,</p>
		
		<p><dsp:valueof param="messageSubject" /></p>
		
		Please find below files moved to Unprocessed Folder:
		<dsp:droplet name="ForEach">
		<dsp:param name="array" param="filesList" />
		<dsp:param name="elementName" value="item" />
		<dsp:oparam name="output">
			<li><b>File Name : </b> <dsp:valueof param="item" /><br/></li>
		</dsp:oparam>
		</dsp:droplet>
	<br />
	<br />
	<span class="textFont"> Regards,</span>
	<br />
	<span class="textFont">TRU e-Commerce Platform</span>

</body>
	</html>
</dsp:page>