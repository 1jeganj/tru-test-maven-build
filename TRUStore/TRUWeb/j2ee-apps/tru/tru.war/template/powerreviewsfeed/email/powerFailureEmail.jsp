<%@ taglib prefix="dsp"
	uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<style>
	.textFont {
		font-size: 13px;
	}
	
	.bold {
	}
	
	.red {
		color: red;
	}
	</style>
	<p>Hi,</p>	
	<b>Power Reviews Feed : </b><p>  <dsp:valueof param="message" /></p>
	<li><b>Mail priority  :</b> <dsp:valueof value="Normal" /><br/></li>
	<li><b>User  :</b> <dsp:valueof value="wladmin" /><br/></li>
	<li><b>OS Host Name : </b> <dsp:valueof bean="/atg/dynamo/service/ServerName.serverName"/><br/></li>
	<li><b>Time-stamp :</b> <dsp:valueof param="timezone" /><br/></li>
	<br/><br/><br/>
			<table border="1">
			<tr>
				<td>Total Records Received</td>
				<td>Total Records Processed Successfully</td>
				<td>Total Records Failed To Process</td>
			</tr>
			<tr>
				<td><dsp:valueof param="totalRecordCount"/></td>
				<td><dsp:valueof param="successRecordCount"/></td>
				<td><dsp:valueof param="totalFailedRecordCount"/></td>
			</tr>
			</table>
	<br/><br/><span class="textFont"> Regards,</span>
	<br/>
	<span class="textFont">TRU e-Commerce Platform</span>			
</dsp:page>