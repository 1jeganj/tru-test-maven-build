<%@ taglib prefix="dsp"
	uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_1"%>
	<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<dsp:page>
	<html>
<head>
</head>
<body>
	<style>
.textFont {
	font-size: 13px;
}
.red {
	color: red;
}
</style>
	<p>Hi,</p>
		
		<p><dsp:valueof param="messageSubject" /></p>
		<dsp:getvalueof var="type" param="type"/>
		<c:choose >
		<c:when test="${type eq 'fileNotFound' }">
		 Unable to find files in the respective location <dsp:valueof param="inBoundDestination" />
		</c:when>
		<c:when test="${type eq 'invalidFile' }">
		<dsp:getvalueof param="invalidFiles" var="invalidFiles"/>
		<dsp:getvalueof param="errorMessage" var="errorMessage"/>
		Below are the invalid files
		<table>
			<th>File Name</th>
			<th>Error Message</th>
			<c:forEach var="file" items="${invalidFiles}">
			<tr>
				<td><dsp:valueof value="${file}" /></td>
				<td><dsp:valueof value="${errorMessage}" /></td>
			</tr>
			</c:forEach>
		</table>	
		</c:when>
		<c:when test="${type eq 'invalidRecords' }">
		<dsp:getvalueof param="fileName" var="fileName"/>
		  Below is the file for invalid records
		<table>
			<th>File Name</th>
			<tr>
			<td><dsp:valueof value="${fileName}" /></td>
			</tr>
		</table>	
		</c:when>
		</c:choose>
	<br />
	<br />
	<span class="textFont"> Regards,</span>
	<br />
	<span class="textFont">TRU e-Commerce Platform</span>

</body>
	</html>
</dsp:page>