<%@ taglib prefix="dsp"
	uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_1"%>
	<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<dsp:page>
	<html>
<head>
</head>
<body>
	<style>
.textFont {
	
	font-size: 13px;
}

.bold {
	
}

.red {
	color: red;
}
</style>
	<p>Hi,</p>
		
		<p><dsp:valueof param="messageSubject" /></p>
		<dsp:getvalueof param="processedFiles" var="processedFiles"/>
		Below are the processed files
		<c:forEach var="file" items="${processedFiles}">
		<table>
			<tr>
				<td><dsp:valueof value="${file}" /></td>
			</tr>
		</table>
		</c:forEach>
			
	<br />
	<br />
	<span class="textFont"> Regards,</span>
	<br />
	<span class="textFont">TRU e-Commerce Platform</span>

</body>
	</html>
</dsp:page>