<%@ taglib prefix="dsp" uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_1" %>
<dsp:page>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<table>
	<tr>
		<td>
		This message confirms that your Akamai content removal request has been processed by all active servers on our network.
		</td>
	</tr>
	<tr>
		<td>
		Here are the details.
		</td>
	</tr>

	<tr>
		<td>
			<table>
			
				<tr>
					<table>
								<tr>
									<td>ID:</td>
									<td><dsp:valueof param="purgeId"/></td>
								</tr>
							
								<tr>
									<td>Domain:</td>
									<td><dsp:valueof param="domain"/></td>
								</tr>
							
								<tr>
									<td>Submission time:</td>
									<td><dsp:valueof param="requestSubmissionTime"/></td>
								</tr>
							
								<tr>
									<td>Completion time:</td>
									<td><dsp:valueof param="requestCompletionTime"/></td>
								</tr>
								
								<!-- <tr>
									<td>Requestor:</td>
									<td>1-HV73D/OPEN</td>
								</tr>
							
								<tr>
									<td>Submission time:</td>
									<td>Fri, 08 Jan 2016 17:12:19 +0000</td>
								</tr>
							
								<tr>
									<td>Completion time:</td>
									<td>Fri, 08 Jan 2016 17:15:17 +0000</td>
								</tr> -->
							
								<tr>
									<td>URL/cpcode count:</td>
									<td><dsp:valueof param="cpCodeCount"/></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									
								</tr>	
								<tr>
									<td>Content purged:</td>
									<td></td>
								</tr>	
								<tr>
									<td>&nbsp;</td>
									
								</tr>	
								<tr>
								  <td>
									<table>
										<dsp:droplet name="ForEach">
											<dsp:param name="array" param="cpCodesList" />
											<dsp:param name="elementName" value="item" />
											<dsp:oparam name="output">
											<tr>
												<td>remove cpcode </td>
												<td> <dsp:valueof param="item" /></td>
											</tr>
											</dsp:oparam>
										</dsp:droplet>
									 </table>
									<td>
								</tr>
								
									
								
								
								<tr>
								<td>&nbsp;</td>
								</tr>
					</table>
				
				</tr>
				
	
				
				<tr>
				<td>
					<table>
						<tr>
						<td>Note that the above list may have been truncated if it was particularly long.</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
						<td>If you have any questions, please contact Akamai Support by opening a ticket through Akamai Luna Control Center at </td>
						</tr>
						<tr>
						<td>https://control.akamai.com/.</td>
						</tr>
					</table>
				</td>	
				</tr>
				
				
				
				<tr>
					<td>
							<table>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
								<td>Regards,</td>
								</tr>
								<tr>
								<td>Akamai Technologies</td>
								</tr>
								<tr>
								<td>https://control.akamai.com/</td>
								</tr>
								
							</table>
						</td>
				</tr>
				 
			 </table>
		 </td>
	</tr>
	
</tr>
</table>


</dsp:page>