<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dsp" uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_1" %>

This page is for Testing XSS Vulnerability<br>
To see proper result pass your Name as http://localhost:8080/test/xss.jsp?name=test<br>
To reproduce XSS attack pass URL as below<br>
  &nbsp;http://localhost:8080/test/xssall.jsp?name=&lt;script&gt;alert(&quot;QCTeam&quot;)&lt;/script&gt;<br>
  &nbsp;http://localhost:8080/test/xssall.jsp?name=&lt;script&gt;alert(document.cookie)&lt;/script&gt;<br>
  &nbsp;http://localhost:8080/test/xssall.jsp?name=&lt;form&gt;First name: &lt;input type=\&quot;text\&quot; name=\&quot;firstname\&quot;&gt;&lt;br&gt;Last name: &lt;input type=\&quot;text\&quot; name=\&quot;lastname\&quot;&gt;&lt;/form&gt;&quot;;<br>
<br>
<br>
<br>
<br>
==============================================================================================<br>
Back to index page <a href="<c:url value="index.jsp" />">HOME</a> 
==============================================================================================<br>
Parameter value is accessed directly without cleaning dirty script. This will print Parameter value as is with HTML/JS tags<br>

&lt;!-- The fn:escapeXml() function escapes characters that can be interpreted as XML markup. --&gt;<br>
&lt;!-- By default it is true to print string as HTML --&gt;<br>
Hello &lt;b&gt;&lt;c:out value=&quot;$'{'param.name'}'&quot;/&gt;&lt;/b&gt;!<br>
----------------------------------------------------------------------------------------------<br>
<!-- The fn:escapeXml() function escapes characters that can be interpreted as XML markup. -->
<!-- By default it is true to print string as HTML -->
Hello <b><c:out value="${param.name}"/></b>!<br>
==============================================================================================<br>
Parameter value is accessed directly without cleaning dirty script. This will <b>insert</b> Parameter value as a HTML/JS tags<br>

&lt;!-- The fn:escapeXml() function escapes characters that can be interpreted as XML markup. --&gt;<br>
&lt;!-- By default it is true so making it false to print string as HTML --&gt;<br>
Hello &lt;b&gt;&lt;c:out value=&quot;$'{'param.name'}'&quot; escapeXml='false'/&gt;&lt;/b&gt;!<br>
----------------------------------------------------------------------------------------------<br>
<!-- The fn:escapeXml() function escapes characters that can be interpreted as XML markup. -->
<!-- By default it is true so making it false to print string as HTML -->
Hello <b><c:out value="${param.name}" escapeXml='false'/></b>!<br>
==============================================================================================<br>
Parameter value is accessed from ATG response so dirty script is cleaned by filter servlet.<br> 

&lt;dsp:getvalueof var=&quot;dummyName&quot; param=&quot;name&quot;/&gt;<br>

&lt;!-- The fn:escapeXml() function escapes characters that can be interpreted as XML markup. --&gt;<br>
&lt;!-- By default it is true so making it false to print string as HTML --&gt;<br>
Hello &lt;b&gt;&lt;c:out value=&quot;$'{'dummyName'}'&quot; escapeXml='false'/&gt;&lt;/b&gt;!!!!<br>
----------------------------------------------------------------------------------------------<br>
<dsp:getvalueof var="dummyName" param="name"/>

<!-- The fn:escapeXml() function escapes characters that can be interpreted as XML markup. -->
<!-- By default it is true so making it false to print string as HTML -->
Hello <b><c:out value="${dummyName}" escapeXml='false'/></b>!!!!<br>
==============================================================================================


