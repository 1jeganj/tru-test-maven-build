//elements cache
var $compareProductNav, $guidedNavigation;
var flagForEmptyCheck = false;
var categoryId = $("#hiddenCategoryId").val();
var tealiumComparedProducts = [];
if (localStorage.getItem('TealiumProductCompareArray_' + categoryId) != null && localStorage.getItem('TealiumProductCompareArray_' + categoryId) != 'undefined') {
	tealiumComparedProducts = JSON.parse(localStorage.getItem('TealiumProductCompareArray_' + categoryId));
}
if (typeof (utag_data) != "undefined") { utag_data.product_compared = tealiumComparedProducts; }
function comparedProducts(data) {
	var products = $(data).children(".col-md-2");
	if (products.length == 0) {
		clearCompareList();
		return false;
	}
	var compareOverlayLockedImage = $(".compare-overlay").find(".row.compare-display-image");
	var compareOverlayLockedHeading = $(".compare-overlay").find(".row.compare-display-heading-text");
	var compareOverlayratinAndAge = $(".compare-overlay").find(".row.compare-display-rating-age");
	var compareOverlayScroll = $(".compare-overlay").find(".compare-overlay-scroll");
	var compareOverlayScrolladdToCart = $(".compare-overlay").find(".row.compare-display-addTocart");
	$(".remove-compare-product").children(".col-md-2").remove();
	var flagForEmptyCheck = false;
	compareOverlayScroll.html("");
	var priceDiv = '<div class="row remove-compare-product price-compare-block"><div class="col-md-3">Price</div>';
	var ratingDiv = '<div class="row remove-compare-product"><div class="col-md-3">Review rating</div>';
	var ageDiv = '<div class="row remove-compare-product age-compare-block"><div class="col-md-3">Age</div>';

	var ageLength = false;
	var priceLength = false;

	var lgDiv = '<div class="row remove-compare-product"><div class="col-md-3">Long Description</div>';
	products.each(function (i, val) {
		//Constructing the header and the image part inside the compare-overlay-locked
		var imageUrl = $(val).find(".compare-product-image").find('img').attr("src");//text();
		var pdpUrl = $(val).find(".pdp-page-url").text();
		var headingText = $(val).find(".compare-column-header-text").text();
		//var ratingDiv=  $(val).find(".compare-reviews-age").html();
		var productId = $(val).find(".compare-product-image").find("#productId").val();
		var categoryId = $(val).find(".compare-product-image").find("#categoryId").val();
		var skuId = $(val).find(".compare-product-image").find("#skuId").val();
		var isStylizedItem = $(val).find(".compare-product-image").find("#isStylizedItem").val();
		var addToCartJspSection = $(val).find(".addToCartJSPSection").html();
		var imageDiv = "<div class='col-md-2'><input type='hidden' value=" + productId + "-" + skuId + "-" + categoryId + "-" + isStylizedItem + "><a href='javascript:void(0)' class='close-compare-overlay-image'><img src='" + window.TRUImagePath + "images/close-icon-sm.png'></a><a href=" + pdpUrl + "><img src=" + imageUrl + " class='primary-compare-overlay-image primary-img-" + skuId + "' title=" + imageUrl + "></a></div>";
		var headingTextDiv = '<div class="col-md-2"><a href=' + pdpUrl + ' class="column-header-text">' + headingText + '</a>';
		var addToCartDiv = "<div class='col-md-2 addToCartJspSection'>" + addToCartJspSection + "</div>";
		//var ratingDivhtml= '<div class="col-md-2">'+ratingDiv+'</div>';
		//Appeding the header and the image part inside the compare-overlay-locked
		compareOverlayLockedImage.append(imageDiv);
		compareOverlayLockedHeading.append(headingTextDiv);
		//compareOverlayratinAndAge.append(ratingDivhtml);
		compareOverlayScrolladdToCart.append(addToCartDiv);

		/* Class names
		 * compare_overlay_label_list --> compare_overlay_label
		compare_overlay_product_values --> compare_overlay_label_value*/
		priceDiv += comparePriceRow(val);
		ratingDiv += compareRatingRow(val);
		ageDiv += compareAgeRow(val);

		if ($(val).find('.compare-price').html().trim() != "" && $(val).find('.compare-price').html().trim() != "-") {
			priceLength = true;
		}

		if ($(val).find('.compare-age').length) {
			ageLength = true;
		}
	});
	priceDiv += '</div><hr style="clear:both;">';
	ratingDiv += '</div><hr style="clear:both;">';
	ageDiv += '</div><hr style="clear:both;">';
	compareOverlayScroll.append(ratingDiv);
	if (ageLength) {
		compareOverlayScroll.append(ageDiv);
	}
	if (priceLength) {
		compareOverlayScroll.append(priceDiv);
	}

	var $label_list = $(data).find("#compare_overlay_label_list").children(".compare_overlay_label");
	var $product_values = $(data).find(".compare_overlay_product_values");
	var rowProductInfoDiv = "";
	$label_list.each(function () {
		var compareProductRow = compareProductsDivConstructor($(this).text(), $product_values);
		compareOverlayScroll.append(compareProductRow);
	});


	products.each(function (i, val) {
		lgDiv += compareLongDescriptionRow(val);
	});
	lgDiv += '</div><hr style="clear:both;">'
	//compareOverlayScroll.append(lgDiv);

	if (products.length <= 2) {
		//$(".remove-compare-product").find(".close-compare-overlay-image").css("display","none");
	}
	setCompareOverlayContentOffset();
	enableWarnRegistryPopover();
	$(".compare-overlay-scroll .col-md-2").each(function () {
		var $this = $(this);
		var lines = parseInt($this.css('height')) / parseInt($this.css('line-height'));
		if (lines == 1) {
			$(this).css("font-size", "24px");
		}
		else if (lines <= 4) {
			$(this).css("font-size", "18px");
		}
		else {
			$(this).css("font-size", "14px");
		}
		/*if($(this).text().length>15){
			$(this).css("font-size","18px");
		}*/
	});

	var cookieRegistry = $.cookie('REGISTRY_USER');
	var lastAccessedRegistryID = $.cookie("lastAccessedRegistryID");

	if (typeof cookieRegistry !== "undefined" && cookieRegistry != 'null' && cookieRegistry != '' && cookieRegistry != null && typeof lastAccessedRegistryID !== "undefined" && lastAccessedRegistryID != 'null' && lastAccessedRegistryID != null && lastAccessedRegistryID != '') {
		$(".compare-overlay").find(".small-orange").each(function () {
			var $this = $(this);
			if (!$this.hasClass('compareaddtoregistry')) {
				$this.addClass('compareaddtoregistry');
				$this.attr('href', 'javascript:void(0)');
			}
		});
	}
}


function compareProductsDivConstructor(label_name, $product_values) {
	var correspondingAttr = [];
	var compareProductRow = "";
	flagForEmptyCheck = false;
	var flagEmptyProductLabel = false;
	$product_values.each(function () {
		$(this).find(".compare_overlay_label_value").each(function () {
			if ($(this).attr("data-label-name") == label_name) {

				correspondingAttr.push($(this).text());
				if ($(this).text().trim() != "" && $(this).text().trim() != "-") {
					flagForEmptyCheck = true;
				}
				flagEmptyProductLabel = true;
			}


		});
		if (!flagEmptyProductLabel) {
			correspondingAttr.push("");
		}
		flagEmptyProductLabel = false;
	});
	if (flagForEmptyCheck) {
		compareProductRow = addDivStructureForCompareRow(label_name, correspondingAttr);
	}
	return compareProductRow;
}

function addDivStructureForCompareRow(label_name, correspondingAttr) {
	var attrDiv = '<div class="row remove-compare-product">';
	attrDiv += '<div class="col-md-3">' + label_name + '</div>';
	for (i = 0; i < correspondingAttr.length; i++) {
		if (correspondingAttr[i].trim() == "" || correspondingAttr[i].trim() == "-") {
			attrDiv += '<div class="col-md-2 no_info_available">-</div>';
		} else {
			attrDiv += '<div class="col-md-2">' + correspondingAttr[i] + '</div>';
		}

	}
	attrDiv += '</div><hr style="clear:both;">';
	/*class='horizontal-divider'*/
	return attrDiv;
}
function comparePriceRow(data) {
	var comparePrice = $(data).find('.compare-price');
	var priceDiv = "";
	if (comparePrice.length > 0) {
		if (comparePrice.html().trim() == "" || comparePrice.html().trim() == "-") {
			priceDiv = '<div class="col-md-2 no_info_available">';
			priceDiv += '-';
		} else {
			priceDiv = '<div class="col-md-2">';
			priceDiv += $(data).find('.compare-price').html();
		}
	} else {
		priceDiv = '<div class="col-md-2 no_info_available">';
		priceDiv += '-';
	}
	priceDiv += "</div>";
	return priceDiv;
}
function compareRatingRow(data) {
	var ratingDiv = '<div class="col-md-2">';
	ratingDiv += $(data).find('.compare-reviews').html();
	ratingDiv += "</div>";
	return ratingDiv;
}

function compareAgeRow(data) {
	var compareAge = $(data).find('.compare-age');
	var ageDiv = "";
	if (compareAge.length > 0) {
		if (compareAge.html().trim() == "" || compareAge.html().trim() == "-") {
			ageDiv = '<div class="col-md-2 no_info_available">';
			ageDiv += '-';
		} else {
			ageDiv = '<div class="col-md-2">';
			ageDiv += $(data).find('.compare-age').html();
		}
	} else {
		ageDiv = '<div class="col-md-2 no_info_available">';
		ageDiv += '-';
	}
	ageDiv += "</div>";
	return ageDiv;
}

function compareLongDescriptionRow(data) {
	var ldDiv = '<div class="col-md-2">';
	ldDiv += $(data).find('.compare-product-langDescription').html();
	ldDiv += "</div>";
	return ldDiv;
}


function checkedCompareProduct() {
	numberOfChecked1 = $('.product-choose .compareChecked').size();
	if (numberOfChecked1 > 3) {
		var checkBoxes = $(".product-compare-checkbox-new");
		$(checkBoxes).each(function (i, val) {
			if ($(val).hasClass("compareUnChecked")) {
				$(val).closest(".product-compare-checkbox-container").css("visibility", "hidden");
			}
		})
	}
	else {
		var checkBoxes = $(".product-compare-checkbox-new");
		$(checkBoxes).each(function (i, val) {
			$(val).closest(".product-compare-checkbox-container").css("visibility", "visible");
		})
	}
	$("#compare-products-button").attr("disabled", (numberOfChecked1 < 2));

	numberCompareEmpty = $('.compare-images .no-compare-image').size();
	numberCompareFull = $(".compare-images").find(".added-compare-images").find(".col-md-10-percent.compare-image.image-1").size();
	if (numberCompareEmpty == 0 || numberCompareFull > 3 ) {
		var checkBoxes = $(".product-compare-checkbox-new");
		$(checkBoxes).each(function (i, val) {
			if ($(val).hasClass("compareUnChecked")) {
				$(val).closest(".product-compare-checkbox-container").css("visibility", "hidden");
			}
		})
	}
	
}

$('#compare-products-button').click(function (e) {
	var categoryid = getParameterByName("categoryid");
	if ($.cookie("productCompareCookie_" + categoryid)) {
		e.stopImmediatePropagation();
		hideCompareProductSelection();
		$('html').addClass('disable-scrolling');
		var categoryId = $("#hiddenCategoryId").val();
		$.ajax({
			url: "/compare/compare.jsp?categoryId=" + categoryId,
			type: "POST",
			dataType: 'html',
			success: function (data) {
				//var data = data.trim();
				comparedProducts(data);
			},
			error: function (e) {
				console.log(e)
			}
		})

		$('.compare-overlay-menu').slideDown();
		$('#product-disabled-modal').fadeIn();

		$(window).scrollTop(0);
	}
	else {
		$("#remove-products-button").click();
		return false;
	}
	loadOmniCompareScript();
});

$(document).on('click', ".close-compare-overlay-image", function (e) {
	//$(this).closest(".compare-display-image").
	e.stopImmediatePropagation();
	var ind = $(this).parent().index();
	$(".remove-compare-product").children(".col-md-2:nth-of-type(" + ind + ")").remove();
	$(".remove-compare-product").each(function () {
		var latestProductCount = $(this).children(".col-md-2").length;
		var noInfoDivCount = $(this).find(".no_info_available").length;
		if (noInfoDivCount >= latestProductCount && latestProductCount > 0) {
			if ($(this).next().prop("tagName").toLowerCase() == "hr") {
				$(this).next().remove();
			}
			$(this).remove();
		}
	});
	var ids = $(this).parent().find("input[type='hidden']").val().split("-");
	var productId = ids[0];
	var skuId = ids[1];
	//var categoryId = ids[2];
	var isStylized = ids[3];

	var categoryId = $("#hiddenCategoryId").val();
	//var productId =$(this).parent().find("input").val().split("-");
	var compareImages = $(".compare-images").find(".added-compare-images").find(".col-md-10-percent.compare-image.image-1");
	$(compareImages).each(function (i, val) {
		if ((isStylized == 'true' && $(val).find("input.imageCompareProductId").val() == productId) || (isStylized == 'false' && $(val).find("input.imageCompareSkuId").val() == skuId)) {
			$(val).find(".close-compare-image").trigger("click");
		}
	});

	//Start: empty value check for age & price
	/*var ageBlock = $('.remove-compare-product.age-compare-block');
	var priceBlock = $('.remove-compare-product.price-compare-block');
	if(ageBlock.length){
		var allAgeValues = ageBlock.find('.col-md-2').text();
		allAgeValues = allAgeValues.replace(/[- ]/g,'');
		if(allAgeValues.length == 0){
			ageBlock.next('hr').remove();
			ageBlock.remove();

		}
	}
	if(priceBlock.length){
		var allPriceValues = priceBlock.find('.col-md-2').text();
		allPriceValues = allPriceValues.replace(/[- ]/g,'');
		if(allPriceValues.length == 0){
			priceBlock.next('hr').remove();
			priceBlock.remove();

		}
	}*/
	//End: empty value check for age & price

	$.ajax({
		url: "/compare/removeFromCompare.jsp?productId=" + productId + "&categoryId=" + categoryId + "&skuId=" + skuId + "&isStylized=" + isStylized,
		success: function () {
			hideCompareProductSelection();
		},
		error: function () {
			//$("#loading").hide()
		}
	});

});

$('.compare-overlay-menu .compare-overlay-close').click(function (e) {
	e.stopImmediatePropagation();
	hideCompareMenu();
	$guidedNavigation.css('display', 'block');
	numberOfChecked1 = $(".compare-images").find(".added-compare-images").find(".col-md-10-percent.compare-image.image-1").size();
	if (numberOfChecked1 > 0) {
		showCompareProductSelection();
	}
});

$(window).resize(function () {
	if ($('html').hasClass("disable-scrolling")) {
		$('html').removeClass("disable-scrolling");
	}
	if ($('.compare-overlay-menu').is(':visible')) {
		//setTimeout(function(){
		/*$.smoothScroll({
			  scrollTarget: '#narrow-by-scroll',
			  offset: 0
		});*/
		$('html').addClass('disable-scrolling');
		//},0)
	}

});
$(document).ready(function () {
	$(document).on('hidden.bs.modal', '#shoppingCartModal,#notifyMeModal', function () {
		$('html').removeClass("disable-scrolling");
		if ($('.compare-overlay-menu').is(':visible')) {
			//setTimeout(function(){
			/*$.smoothScroll({
				   scrollTarget: '#narrow-by-scroll',
				   offset: 0
			});*/
			$('html').addClass('disable-scrolling');
			//},0)
		}
	});

	initElementsCache();

	if ( $( '.compare-product-selection' ).length > 0 ) { 
		var manageCompareNav = new ManageStackingOrder();
		manageCompareNav.setStackingOrder();
	}
})


function hideCompareMenu(time) {
	if (time === undefined)
		time = 400;
	$('.compare-overlay-menu').slideUp(time);
	$('#product-disabled-modal').fadeOut(time);
	$('html').removeClass('disable-scrolling');
}

function setCompareOverlayContentOffset() {
	var overlayHeight = $('.compare-overlay').height();
	var lockedSpace = $('.compare-overlay-locked').height();
	$(".compare-overlay-scroll").css({
		"height": overlayHeight - lockedSpace
		/*"margin-top": lockedSpace*/
	});
}

function updatePlaceholder(reset) {
	if (reset === true) {
		$('.sticky-menu-placeholder').removeAttr('style');
	}
	else {
		$('.sticky-menu-placeholder').css({
			"display": "block",
			"height": $(".fixed-narrow-menu").height()
		});
	}
}

function onloadDisplayCompareImageSection(productId, skuId, cookieImgPath, isStylized, skuDisplayName) {
	//get display name by product id if not passed as parameter
	if (!skuDisplayName) {
		$("#filtered-products").find(".product-compare-checkbox-container").each(function (i, productCompareContainer) {
			var $prdCompareContainer = $(productCompareContainer);
			if ($prdCompareContainer.find(".productId").val().toString() === productId.toString() && $prdCompareContainer.find(".skuId").val().toString() === skuId.toString() && $prdCompareContainer.find(".product-compare-checkbox-new").hasClass("compareChecked")) {
				skuDisplayName = $prdCompareContainer.find(".skuDisplayName").val();
				return true;
			}
		});
	}
	numberOfChecked1 = $(".compare-images").find(".added-compare-images").find(".col-md-10-percent.compare-image.image-1").size();
	if (numberOfChecked1 <= 4) {
		var compareImageUrl = $(".imageUrlForCompare_" + productId + '_' + skuId).val();
		if (typeof cookieImgPath != 'undefined' && cookieImgPath != '') {
			compareImageUrl = cookieImgPath;
		}
		var compareImageContainer = $("#compareImageContainerTemplate");
		compareImageContainer.find(".imageCompareProduct").attr("src", compareImageUrl);
		compareImageContainer.find(".compareProductName").html(skuDisplayName);
		compareImageContainer.find(".imageCompareProductId").val(productId);
		compareImageContainer.find(".imageCompareSkuId").val(skuId);
		compareImageContainer.find(".imageCompareStylizedItem").val((isStylized || false));
		$(".compare-images").find(".added-compare-images .no-compare-image:last-child").remove();
		if ($(".compare-images").find(".added-compare-images .compare-image").length > 1) {
			$(".compare-images").find(".added-compare-images").find(".compare-image:last").after(compareImageContainer.html());
		} else if ($(".compare-images").find(".added-compare-images .compare-image").length) {
			$(".compare-images").find(".added-compare-images .compare-image").after(compareImageContainer.html());
		} else {
			$(".compare-images").find(".added-compare-images").prepend(compareImageContainer.html());
		}
	}
}
function onloadCompareImageSection(flag) {
	var checkedProducts = $("body").find("#narrow-by-scroll").find(".product-compare-checkbox-container").find(".product-compare-checkbox-new.compareChecked");
	var arr = [];
	/*$.each(checkedProducts, function(i,value){
		var productId= $(value).parent().find("input").val();
		onloadDisplayCompareImageSection(productId);
	})*/
	var categoryId = $("#hiddenCategoryId").val();
	var SplitCompareArray;
	var compareCookie = $.cookie("productCompareCookie_" + categoryId);
	if (typeof compareCookie !== 'undefined' && compareCookie !== '' && compareCookie != null) {
		if (compareCookie.indexOf('~') > -1) {
			SplitCompareArray = compareCookie.split("~");
			for (var temp = 0; temp < SplitCompareArray.length; temp++) {
				pair = SplitCompareArray[temp].split('|');
				arr.push(pair[0]);
				if ($('.product-compare-checkbox-new.' + pair[0] + '_' + pair[2]).hasClass("compareUnChecked")) {
					$('.product-compare-checkbox-new.' + pair[0] + '_' + pair[2]).toggleClass("compareUnChecked compareChecked")
				};
				if (typeof flag == 'undefined' || flag) {
					var compareSkuDisplayName = $('.compareSkuId-' + pair[2]).find('.skuDisplayName').val() || '';
					onloadDisplayCompareImageSection(pair[0], pair[2], pair[3], '', compareSkuDisplayName);
				};
			}
			//var checkedProducts = $("body").find("#narrow-by-scroll").find(".product-compare-checkbox-container").find(".product-compare-checkbox-new.compareChecked").length;

			var checkedProducts = $.cookie("productCompareCookie_" + categoryId).split('~').length;
			if (checkedProducts == 4) {
				var checkBoxes = $(".product-compare-checkbox-new");
				$(checkBoxes).each(function (i, val) {
					if ($(val).hasClass("compareUnChecked")) {
						$(val).closest(".product-compare-checkbox-container").css("visibility", "hidden");
					}
				});
			}

		}
		else {

			pair = compareCookie.split('|');
			arr.push(pair[0]);
			if ($('.product-compare-checkbox-new.' + pair[0] + '_' + pair[2]).hasClass("compareUnChecked")) {
				$('.product-compare-checkbox-new.' + pair[0] + '_' + pair[2]).toggleClass("compareUnChecked compareChecked");
			}
			if (typeof flag == 'undefined' || flag) {
				console.debug("inside if");
				onloadDisplayCompareImageSection(pair[0], pair[2], pair[3]);
			}
		}
	}

	if (arr.length > 0) {
		showCompareProductSelection();
	}
	if (arr.length > 1) {
		$("#compare-products-button").prop("disabled", false);
	}
}
$(document).one('ready', function (e) {
	e.stopImmediatePropagation();
	if ($(".compare-product-selection").length) {
		checkedCompareProduct();
		onloadCompareImageSection();
	}
});
$(document).on('click', '.product-compare-checkbox-container', function (event) {
	event.stopImmediatePropagation();
	var productId = $(this).find(".productId").val();
	var skuId = $(this).find(".skuId").val();
	var isStylized = $(this).find(".isStylized").val();
	var onlinePID = $(this).find('.onlinePID').val();/*$(this).find(".skuId").val();*/
	var productImage = $("#compareProdImg_" + productId).val();
	var categoryId = $(this).find(".compareCategoryId").val();
	var skuDisplayName = $(this).find(".skuDisplayName").val();
	//var categoryId = $(this).find(".compareCategoryId_span").text();
	var contextPath = $(".contextPath").val();
	var $checkValue = $(this).find(".product-compare-checkbox-new");
	$checkValue.toggleClass("compareUnChecked compareChecked");
	var imgUrl = $(this).closest(".product-block.product-block-unselected").find(".product-block-image-container").find("a>img");
	var imgUrlSrc = imgUrl.attr("src");
	var checkedProducts = $("body").find("#narrow-by-scroll").find(".product-compare-checkbox-container").find(".product-compare-checkbox-new.compareChecked").length; //||$("body").find(".product-compare-checkbox-container").find(".product-compare-checkbox-new.compareChecked").length;
	var tealiumContains = false;
	if (tealiumComparedProducts != null) {
		tealiumContains = (tealiumComparedProducts.indexOf(onlinePID) > -1);
	}
	var numberOfChecked1;
	var numberCompareEmpty;
	var numberCompareFull;
	if ($checkValue.hasClass("compareChecked")) {
		var _this = $(this).closest(".product-block.product-block-unselected").find(".product-block-image-container");

		$.ajax({
			url: contextPath + "compare/addToCompare.jsp?productId=" + productId + "&categoryId=" + categoryId + "&skuId=" + skuId + "&imgUrl=" + imgUrlSrc + "&stylizedItem=" + isStylized,
			success: function () {
				if (checkedProducts > 0) {
					showCompareProductSelection();
				}
				checkForProductCompareSection();
				onloadDisplayCompareImageSection(productId, skuId, '', isStylized, skuDisplayName);
			}
		});
		if (!tealiumContains) {
			tealiumComparedProducts.push(onlinePID);
			localStorage.setItem("TealiumProductCompareArray_" + categoryId, JSON.stringify(tealiumComparedProducts));
		}
		if (typeof utag_data != 'undefined') {
			utag_data.product_compared = tealiumComparedProducts;
		}
	}
	else {
		var removedItem = $(".compare-images").find(".added-compare-images").children();
		$(removedItem).each(function (i, value) {
			if ($(value).find(".imageCompareProductId").val() == productId && $(value).find(".imageCompareSkuId").val() == skuId) {
				$(value).find(".close-compare-image").trigger("click");
			}
		})

		if (tealiumContains) {
			var index = tealiumComparedProducts.indexOf(onlinePID);
			if (index > -1) {
				tealiumComparedProducts.splice(index, 1);
				localStorage.setItem("TealiumProductCompareArray_" + categoryId, JSON.stringify(tealiumComparedProducts));
			}
		}
		if (typeof utag_data != 'undefined') {
			utag_data.product_compared = tealiumComparedProducts;
		}
	}
	checkForProductCompareSection();
	$(".narrow-by-row").css("visibility", "hidden");   //Fix for issue TOYSRDS-889
	checkedCompareProduct();
});

function clearCompareList() {
	var categoryId = $("#hiddenCategoryId").val();
	$.ajax({
		url: "/compare/clearList.jsp?categoryId=" + categoryId,
		type: "POST",
		dataType: 'html',
		success: function (data) {
			//var data = data.trim();
			//comparedProducts(data);
			$(".compare-images").find(".close-compare-image").each(function () {
				$(this).click();
				$.cookie("productCompareCookie_" + categoryId, '', { path: '/' });
			});

			tealiumComparedProducts = [];
			localStorage.setItem("TealiumProductCompareArray_" + categoryId, JSON.stringify(tealiumComparedProducts));
			if (typeof utag_data != 'undefined') {
				utag_data.product_compared = tealiumComparedProducts;
			}
		},
		error: function (e) {
			console.log(e)
		}
	})
}
$('#remove-products-button').click(function (e) {
	/*if($(".offcanvas.compare-menu").hasClass("in")){
		$('.compare-menu').offcanvas('hide');
	}*/
	clearCompareList();


});


$(document).on('click', '.close-compare-image', function (e) {
	e.stopImmediatePropagation();
	/*if($(".offcanvas.compare-menu").hasClass("in")){
		$('.compare-menu').offcanvas('hide');
	}*//*modified for B&S spark UI changes*/
	var $this = $(this);
	var productId = $this.parent().find(".imageCompareProductId").val();
	var skuId = $this.parent().find(".imageCompareSkuId").val();
	var isStylized = $this.parent().find(".imageCompareStylizedItem").val();
	var contextPath = $(".contextPath").val();

	var indexOfElement = $('.close-compare-image').index(this);
	$this.parent().remove();
	var numberOfChecked1 = $(".compare-images").find(".added-compare-images").find(".col-md-10-percent.compare-image.image-1").size();
	if (numberOfChecked1 < 4) {
		$(".compare-images").find(".added-compare-images").append('<div class="col-md-10-percent no-compare-image"><span class="no-compare-text">select a product below to compare</span></div>');
		var checkBoxes = $(".product-compare-checkbox-new");
		$(checkBoxes).each(function (i, val) {
			$(val).closest(".product-compare-checkbox-container").css("visibility", "visible");
		})
	}
	if (numberOfChecked1 == 0) {
		//$(".close-compare-overlay-image").css("display",'none');
		$(".compare-overlay-close").trigger("click");
	} else {
		//$(".close-compare-overlay-image").css("display",'block');
	}
	if (!numberOfChecked1) {
		hideCompareProductSelection();
	}
	$("#filtered-products").find(".product-compare-checkbox-container").each(function (i, val) {
		if ($(val).find(".productId").val() == productId && $(val).find(".skuId").val() == skuId && $(val).find(".product-compare-checkbox-new").hasClass("compareChecked")) {
			$(val).find(".product-compare-checkbox-new").click();
			e.stopPropagation();
			return true;
		}
	});
	$("#compare-products-button").attr("disabled", (numberOfChecked1 < 2));

	var hiddenCategoryId = $("#hiddenCategoryId").val();
	var compareCookie = $.cookie("productCompareCookie_" + hiddenCategoryId);
	var categoryId = 0;
	////////////
	if (typeof compareCookie !== 'undefined' && compareCookie !== '') {
		if (compareCookie.indexOf('~') > -1) {
			SplitCompareArray = compareCookie.split("~");
			for (var temp = 0; temp < SplitCompareArray.length; temp++) {
				pair = SplitCompareArray[temp].split('|');
				if (pair[0] == productId) {
					categoryId = pair[1];
				}
			}
		}
		else {

			pair = compareCookie.split('|');
			if (pair[0] == productId) {
				categoryId = pair[1];
			}
		}

		if (categoryId != 0) {
			$.ajax({
				url: contextPath + "compare/removeFromCompare.jsp?productId=" + productId + "&categoryId=" + categoryId + "&skuId=" + skuId + "&isStylized=" + isStylized,
				async: false,
				success: function () {
					/*alert(productId);
					var tealiumContains = false;
					if(tealiumComparedProducts != null){
					tealiumContains = (tealiumComparedProducts.indexOf(productId) > -1);
					}
					alert('tealiumContains'+tealiumContains);
					alert('tealiumComparedProducts'+tealiumComparedProducts);
					if(tealiumContains){
						tealiumComparedProducts.pop(productId);
						localStorage.setItem("TealiumProductCompareArray_"+categoryId , JSON.stringify(tealiumComparedProducts));
					}
					utag_data.product_compared = tealiumComparedProducts;	*/
				},
				error: function () {
					//$("#loading").hide()
				}

			});
		}
	}
	//}
	//});
	/*$(".compare-overlay-locked .compare-display-image .col-md-2").each(function(i,val){
		//if($(".compare-overlay-locked .compare-display-image .col-md-2").length>2){
			var pid = $(val).find("input").val();
			if(pid.indexOf("-")){
				pid= pid.substr(0,pid.length-1);
			}
			if(pid == productId){
				var ind = $(val).find(".close-compare-overlay-image").parent().index();
				$(".remove-compare-product").children(".col-md-2:nth-of-type("+ind+")").remove();
				$(".remove-compare-product").each(function(){
					var latestProductCount = $(this).children(".col-md-2").length;
					var noInfoDivCount= $(this).find(".no_info_available").length;
					if( noInfoDivCount >= latestProductCount){
						$(this).remove();
					}
				});
				e.stopPropagation();
				return true;
			}
		//}
	});*/
	//var categoryId = $(".compareCategoryId").val();
	var categoryId = $(".compareCategoryId_span").text();
	/*$.ajax({
			url : contextPath + "/compare/removeFromCompare.jsp?productId=" + productId
			+ "&categoryId=" + categoryId,
			success : function() {

			},
			error : function() {
				//$("#loading").hide()
			}
		})*/

	setCompareOverlayContentOffset();
});

/*
 * This function is used for adding an item to registry from compare overlay
 */
function compareAddToRegistry(obj) {
	var $this = $(obj);
	var $parent = $this.parents('.addToCartJspSection');
	var $doc = $(document);
	var $body = $('body');
	var registryNumber = $.cookie('REGISTRY_USER');
	var lastAccessedRegisterId = $.cookie('lastAccessedRegistryID');
	if (typeof registryNumber === "undefined" || typeof lastAccessedRegisterId === "undefined" || registryNumber == "" || registryNumber == "null" || registryNumber == null || lastAccessedRegisterId == "" || lastAccessedRegisterId == "null" || lastAccessedRegisterId == null) {
		window.location.assign($this.attr('href'));
		return false;
	}
	if ($doc.find("#rgs_registryNumber").length <= 0) {
		var tmpregistryNumber = $('<input type="hidden" id="rgs_registryNumber" />');
		var tmplastAccessedRegisterId = $('<input type="hidden" id="rgs_lastAccessedRegistryID" />');
		var timeOut = $('<input type="hidden" id="rgs_timeout" />');
		var productDetails = $('<input type="hidden" id="rgs_productDetails" />');
		tmpregistryNumber.val(registryNumber);
		tmplastAccessedRegisterId.val(lastAccessedRegisterId);
		timeOut.val($parent.find('#compareTimeout').val());
		productDetails.val($parent.find('#compareProductDetails').val());
		$body.append(tmpregistryNumber);
		$body.append(tmplastAccessedRegisterId);
		$body.append(timeOut);
		$body.append(productDetails);
	} else {
		$doc.find("#rgs_timeout").val($parent.find('#compareTimeout').val());
		$doc.find("#rgs_productDetails").val($parent.find('#compareProductDetails').val());
	}
	$("#add-to-registry").find("p").html("you have successfully added the <b class='registrySkuDisplayName'>" + $parent.find("#compareSkuDisplayName").val() + "</b> to your baby registry");
	if (typeof registryNumber != "undefined" && typeof lastAccessedRegisterId != "undefined" && registryNumber != "" && registryNumber != "null" && registryNumber != null && lastAccessedRegisterId != "" && lastAccessedRegisterId != "null" && lastAccessedRegisterId != null) {
		registryWishListCookie("false", obj);
	}
}

$(document).on('click', '.compareaddtoregistry', function () {
	compareAddToRegistry(this);
});

function loadOmniCompareScript() {
	var customerEmail = $("#tealiumComp_customerEmail").val();
	var customerID = $("#tealiumComp_customerID").val();
	if (localStorage.getItem('TealiumProductCompareArray_' + categoryId) != null) {
		tealiumComparedProducts = JSON.parse(localStorage.getItem('TealiumProductCompareArray_' + categoryId));
	}
	var comparedProductsArray = tealiumComparedProducts;
	//console.log("compared products:::"+comparedProductsArray);
	if (typeof utag != 'undefined') {
		utag.link({
			event_type: 'compare_products',
			customer_email: customerEmail,
			customer_id: customerID,
			product_compared: comparedProductsArray
		});
	}
}

function showCompareProductSelection() {
	//we need to move the compare product selection div, so it can has full width size
	if ($compareProductNav.parent()[0].id !== 'narrow-by-scroll') {
		$compareProductNav.detach().appendTo('#narrow-by-scroll')
	}

	$compareProductNav.show();

	//margin bottom to the filters div, so we can position the compare product selection in that place
	$guidedNavigation.css('margin-bottom', '130px');
}

function hideCompareProductSelection() {
	$compareProductNav.hide();
	$guidedNavigation.css('margin-bottom', '4px');

	//we need to simulate a margin-top here, as absolute positioning doesn't works with margin property
	var marginTop = 20;
	var compareNavTopValue = $guidedNavigation.offset().top + $guidedNavigation.height() + marginTop;

	//we need to place the compare nav just below the #guidedNavigation element (filters div)


	$compareProductNav.css({ 'position': 'absolute', 'top': compareNavTopValue + 'px' });
}

function initElementsCache() {
	$compareProductNav = $('.compare-product-selection');
	$guidedNavigation = $('#guidedNavigation');
}

// This function handle how z-index will behave, controlling the stacking order of elements.
function ManageStackingOrder() {
    var _self = this,
        OPEN = 'open',
        DISPLAY = 'display',
        BLOCK = 'block',
        FIXED = 'fixed';

    // We need to fake margin-top, as absolute positioning doesn't consider it when calculating positioning.
    this.compareProductSelectionMarginTop = 20;

    this.initialize = function() {
        this.$window = $( window );
        this.$compareProductSelectionElement = $( '.compare-product-selection' );
        this.$guidedNavigationElement = $( '#guidedNavigation' );
        this.$subCategoryTemplateElement = $( '.sub-category-template' );
        this.$sortByDropDownElement = $( '#sort-by-dropdown' );
        this.$compareOverlayMenuElement = $( '.compare-overlay-menu' );
        this.$findInStoreModalElement = $( '#findInStoreModal' );

        this.zIndexActiveClass = 'z-index-active';

        this.MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

        this.attachEventHandler();
    };

    // Attach all event handler functions for events to the necessary elements.
    this.attachEventHandler = function() {
        this.$window.scroll( this.setStackingOrder );

        this.observer = new this.MutationObserver( function( mutations, observer ) {
            _self.setStackingOrder();
        });

        this.toObserve();
    }

    this.setStackingOrder = function() {
        // Stop listening the changes, change what we need and then start listening again.
        _self.observer.disconnect();

        var isPositionFixed = ( _self.$compareProductSelectionElement.css( 'position' ) === FIXED ),
            isVisible = ( _self.$compareProductSelectionElement.css( DISPLAY ) !== 'none' ),
            compareNavTopPosition = _self.getCompareNavTopPosition();

        if ( isVisible ) {
            if ( _self.$sortByDropDownElement.hasClass( OPEN ) ) {
                _self.$subCategoryTemplateElement.addClass( _self.zIndexActiveClass );
            }

            if ( _self.$window.scrollTop() > compareNavTopPosition ) {
                if ( isPositionFixed ) {
                	_self.$subCategoryTemplateElement.removeClass( _self.zIndexActiveClass );
                }

                _self.setCompareNavTopPosition( FIXED, '0' );
            }

            if ( _self.$window.scrollTop() < compareNavTopPosition ) {
                _self.setCompareNavTopPosition( 'absolute', compareNavTopPosition );

                if ( _self.$sortByDropDownElement.hasClass( OPEN ) ) {
                    _self.$subCategoryTemplateElement.addClass( _self.zIndexActiveClass );
                }
            }

            if ( _self.$compareOverlayMenuElement.css( DISPLAY ) === BLOCK || _self.$findInStoreModalElement.css( DISPLAY ) === BLOCK ) {
                _self.$sortByDropDownElement.removeClass( OPEN );
                _self.$subCategoryTemplateElement.addClass( _self.zIndexActiveClass );
            }
        }

        _self.toObserve();
    }

    this.toObserve = function() {
        // Observe all class changes of the element and all its children.
        _self.observer.observe( _self.$subCategoryTemplateElement[ 0 ], {
            subtree: true,
            attributes: true,
            childList: true,
            attributeFilter: [ 'class' ]
        });
    }

    this.getCompareNavTopPosition = function() {
        return _self.$guidedNavigationElement.offset().top + _self.$guidedNavigationElement.height() + _self.compareProductSelectionMarginTop;
    }

    this.setCompareNavTopPosition = function( position, top ) {
        _self.$compareProductSelectionElement.css({
            'position': position,
            'top': top + 'px'
        });
    }

    this.initialize();
}
