/*Radial OMS details for order history details and show order details and cancel orders.*/
var totalNumberOfRecords;
var loadMorePageNumber;
var totalRecordsFlag = true;
var totalNumberOfRecordsDB;
var loadOrderHistoryFirstindex = 0;
var loadOrderHistorylastindex;
var defaultnoOfrecords;
var email;
var orderHistory = {};
var displayRecordsInLayaway = false;
$(document).on("click", ".order-history-left-col .row-load-more .more,.layaway-orders #layawayLoadMore.more", function () {
	var $loadMore = $('#orderHistoryLoadMore');
	if ((totalNumberOfRecords >= -4) && ($loadMore.html() === 'load more') && $(this).attr('id') != "layawayLoadMore") {
		loadMorePageNumber = parseInt($("#pageNumber").val());
		loadOrderHistoryDetails(false);
	}
	else if ($(this).attr('id') == "layawayLoadMore") {
		loadMorePageNumber = parseInt($("#layawayPageNumber").val());
		loadOrderHistoryDetails(true);
	}
});

function loadorderHistoryOrders(layawayPage) {
	customerId = $("#customerId").val();
	email = $("#email").val();
	loadMorePageNumber = $("#pageNumber").val() ? $("#pageNumber").val() : $("#layawayPageNumber").val();
	noOfRecordsPerPage = $("#noOfRecordsPerPage").val();
	loadOrderHistorylastindex = parseInt(noOfRecordsPerPage);
	defaultnoOfrecords = parseInt(noOfRecordsPerPage);
	pushSite = $("#pushSiteId").val();
	var omsOrderListRestAPIURL = $("#omsOrderListRestAPIURL").val();
	$.ajax({
		url: omsOrderListRestAPIURL + "?customerId=" + customerId + "&pageNumber=" + loadMorePageNumber + "&email=" + email + "&noOfRecordsPerPage=" + noOfRecordsPerPage + "&layawayPage=" + layawayPage + "&pushSite=" + pushSite,
		type: "post",
		dataType: "json",
		headers: {
			"X-APP-API_KEY": "apiKey",
			"X-APP-API_CHANNEL": "mobile",
			"Content-Type": "application/x-www-form-urlencoded"
		},
		async: true,
		success: function (data) {

			orderHistory = data.customerOrder;

			loadOrderHistoryDetails(layawayPage);
		}
	});
}
function loadOrderHistoryDetails(layawayPage) {
	noOfRecordsPerPage = $("#noOfRecordsPerPage").val();
	loadMorePageNumber = $("#pageNumber").val() ? $("#pageNumber").val() : $("#layawayPageNumber").val();
	var loadMorePageNumbers = parseInt(loadMorePageNumber) + 1;
	if (layawayPage) {
		$("#layawayPageNumber").val(loadMorePageNumbers);
		$('#layaway-guest-account').addClass("active");
		if (typeof orderHistory.OrderList[0].OrderNumber != 'undefined' && orderHistory.OrderList[0].OrderNumber != '') {
			displayRecordsInLayaway = true;
			$('#orderHistory').addClass('active');
		}
		else {
			$('#orderHistory').removeClass('active');
		}
	}
	else {
		$("#pageNumber").val(loadMorePageNumbers);
	}
	if (layawayPage) {
		if (typeof orderHistory.OrderList[0].Message !== "undefined" && orderHistory.OrderList[0].Message != "") {
			var ordersPerPage = orderHistory;
		}
		else {
			var ordersPerPage = orderHistory.OrderList.slice(loadOrderHistoryFirstindex, loadOrderHistorylastindex);
		}
	}
	else {
		if (typeof orderHistory.customerTransactionListResponse != 'undefined' && orderHistory.customerTransactionListResponse != '') {
			var ordersPerPage = orderHistory;
		}
		else {
			var ordersPerPage = orderHistory.CustomerLookupResponse.OrderList.Order.slice(loadOrderHistoryFirstindex, loadOrderHistorylastindex);
		}
	}
	var ordersDisplayPerPage = { 'orders': ordersPerPage };
	loadOrderHistoryFirstindex = parseInt(loadOrderHistoryFirstindex) + parseInt(noOfRecordsPerPage);
	loadOrderHistorylastindex = parseInt(loadOrderHistoryFirstindex) + parseInt(noOfRecordsPerPage);
	ajaxCallForStaticText(ordersDisplayPerPage);
	if (!totalRecordsFlag) {
		if (layawayPage) {
			totalNumberOfLayawayOrders = orderHistory.OrderList.length;
			totalNumberOfRecordsDB = orderHistory.OrderList.length;
		}
		else {
			if (typeof orderHistory.customerTransactionListResponse != 'undefined' && orderHistory.customerTransactionListResponse != '') {
				$(".my-account-order-history").find(".order-history-table-header").hide();

			}
			else {
				totalNumberOfLayawayOrders = orderHistory.CustomerLookupResponse.OrderList.Order.length;
				totalNumberOfRecordsDB = orderHistory.CustomerLookupResponse.OrderList.Order.length;
			}
		}
		totalNumberOfRecords = totalNumberOfRecordsDB - 5;
		if (typeof totalNumberOfRecordsDB == 'undefined' || totalNumberOfRecordsDB == '' || totalNumberOfRecordsDB < 4) {
			$('.my-account-order-history .orderHistoryViewAll').hide();
		}
		if (totalNumberOfRecordsDB > 4) {
			if ((totalNumberOfRecordsDB > loadOrderHistoryFirstindex)) {
				$(".row.row-load-more").removeClass('hide');
				$("#orderHistoryLoadMore").html("load more").removeClass("less").addClass('more');
				if (layawayPage) {
					$("#layawayLoadMore").html("load more").removeClass("less").addClass('more');
				}
			}
			else{
				var $loadMore = $('#orderHistoryLoadMore');
				$("#orderHistoryLoadMore").html('load less').removeClass('more').addClass("less");
				if (layawayPage) {
					$("#layawayLoadMore").html("load less").removeClass("more").addClass('less');
				}
			}
		}
		/*else if ((totalNumberOfRecordsDB > 10)) {
			$(".row.row-load-more").removeClass('hide');
			$("#orderHistoryLoadMore").html("load more").removeClass("less").addClass('more');
			if (layawayPage) {
				$("#layawayLoadMore").html("load more").removeClass("less").addClass('more');
			}
		}*/
	}
}
function ajaxCallForStaticText(orderHistory) {
	$.ajax({
		url: '/myaccount/StaticText.jsp',
		type: "post",
		dataType: "json",
		async: false,
		success: function (data) {
			orderDetails = data;
			updateOrdersInOrderHistory(orderHistory, orderDetails);
		}
	});
}
function updateOrdersInOrderHistory(orderHistory, orderDetails) {
	var orderHistoryJson = $.extend(orderHistory, orderDetails);
	if ($(".order-history-table-header").length > 0) {
		$('#myaccount-order-history-template').tmpl(orderHistoryJson).appendTo('.order-summary-block');
		totalRecordsFlag = false;
	} else if ($(".order-history-table").length > 0) {
		$('#order-history-template').tmpl(orderHistoryJson).appendTo('.order-history-table');
		if (!totalRecordsFlag) {
			totalNumberOfRecords = totalNumberOfRecords - 5;
		}
		if ((totalNumberOfRecords <= 0)) {
			if (totalNumberOfRecordsDB > 5) {
				var $loadMore = $('#orderHistoryLoadMore');
				$loadMore.html('load less').removeClass('more').addClass("less");

			}
		}
		totalRecordsFlag = false;
	} else if ($(".layaway-order-history-table-header.row").length > 0) {
		var layawayOrderHistoryTemplate = $('#layaway-order-history-template').tmpl(orderHistoryJson);
		$('.my-account-order-history .layaway-order-history-table-header.row').after(layawayOrderHistoryTemplate);
		if (!totalRecordsFlag) {
			totalNumberOfRecords = totalNumberOfRecords - 5;
		}
		if ((totalNumberOfRecords <= 0)) {
			if (totalNumberOfRecordsDB > 5) {
				var $loadMore = $('#orderHistoryLoadMore');
				$("#layawayLoadMore").html('load less').removeClass('more').addClass("less");

			}
		}
		totalRecordsFlag = false;
	}
}
function cancelOrder(obj) {
	if (obj == false) {
		$("#orderHistoryDetailModal").modal("hide");
	}
	else {
		var orderId = $(obj).parents("#orderHistoryCancelOrder").find(".cancelOrderNumber").val() || $("#orderHistoryDetailModal").find(".hiddenOrderNumber").val();
		var omsCancelOrderRestAPIURL = $("#omsCancelOrderRestAPIURL").val();
		pushSite = $("#pushSiteId").val();
		$.ajax({
			url: omsCancelOrderRestAPIURL + "?orderId=" + orderId + "&pushSite=" + pushSite + "&email=" + email,
			type: "post",
			headers: {
				"X-APP-API_KEY": "apiKey",
				"X-APP-API_CHANNEL": "mobile",
				"Content-Type": "application/x-www-form-urlencoded"
			},
			dataType: "json",
			async: false,
			success: function (data) {
				customerOrderDetailsResponse = data.customerOrder;
				var responseStatus = customerOrderDetailsResponse.OrderCancelResponse.ResponseStatus;
				if (responseStatus == "Success") {
					$(document).find('.successMessage').remove();
					$('.orderHistoryCancelOrder').removeClass("hide");
					$('.orderHistoryCancelOrderError').addClass("hide");
				}
				if (responseStatus == "ServiceDown") {
					$(document).find('.successMessage').remove();
					$('.orderHistoryCancelOrder').addClass("hide");
					$('.orderHistoryCancelOrderError').removeClass("hide");
				}
				/*if($(".my-account-sign-in-template").length>0){
					return false;
				}	*/
				totalRecordsFlag = true;
				$("#pageNumber").val(1);
				//cancelOrderHistoryJson = data;
				if ($(".order-history-table-header").length > 0) {
					$('.order-summary-block').children('.order-history-order.row').remove();
					//$('#myaccount-order-history-template').tmpl(orderHistoryJson).appendTo('.order-summary-block');
				} else if ($(".order-history-table").length > 0) {
					//$('#order-history-template').tmpl(orderHistoryJson).appendTo('.order-history-table');
					$('.order-history-table').children('.order-history-order.row').remove();

				} else if ($(".layaway-order-history-table-header.row").length > 0) {
					//var layawayOrderHistoryTemplate = $('#layaway-order-history-template').tmpl(orderHistoryJson);
					//$('.my-account-order-history .layaway-order-history-table-header.row').after(layawayOrderHistoryTemplate);
					//$('.order-summary-block').html("");
				}
				loadOrderHistoryFirstindex = 0;
				loadOrderHistorylastindex = 0;
				loadorderHistoryOrders(false);
			}
		});
		$("#orderHistoryCancelOrder").modal("hide");
		$("#orderHistoryDetailModal").modal("hide");
		omniCancelOrder();

	}
}
$(document).on('click', '.row-load-more .less', function () {
	totalNumberOfRecords = totalNumberOfRecordsDB - 5;
	$("#pageNumber").val(2);
	$(".order-history-table .order-history-order.row").slice(5).remove();
	$("#orderHistoryLoadMore").html("load more").removeClass("less").addClass('more');
	noOfRecordsPerPage = $("#noOfRecordsPerPage").val();
	loadOrderHistoryFirstindex = parseInt(noOfRecordsPerPage);
	loadOrderHistorylastindex = loadOrderHistoryFirstindex + parseInt(noOfRecordsPerPage);
})
$(document).on('click', '#layawayLoadMore.less', function () {
	totalNumberOfRecords = totalNumberOfRecordsDB - 5;
	$("#layawayPageNumber").val(2);
	$(".layaway-orders .order-history-order.layaway-orders").slice(5).remove();
	$("#layawayLoadMore").html("load more").removeClass("less").addClass('more');
	noOfRecordsPerPage = $("#noOfRecordsPerPage").val();
	loadOrderHistoryFirstindex = parseInt(noOfRecordsPerPage);
	loadOrderHistorylastindex = loadOrderHistoryFirstindex + parseInt(noOfRecordsPerPage);
})
/*function start to display order details in layaway page*/
$("body").on("click", ".orderHistoryDetailModal,.order-information,.guest-order-submit,.order-status button", function () {
	$("#layawayGlobalErrorDisplay").hide();
	if ($(this).attr("id") == "orderStatusSignIn") {
		if (!$("#truOrderStatusSignInForm").valid()) {
			$(".errorDisplay").css('display', 'none');
			return false;
		}
	} else if ($(this).attr("id") == "layawayGuestOrderNumberCheckup") {
		if (!$("#layawayGuestOrderNumberCheckupForm").valid()) {
			return false;
		}
	}
	if ($(this).hasClass("enter-button guest-order-submit layaway-order-num")) {
		if ($("#guest-order-number").val() == "" || $("#guest-order-number").val() == null) {
			if ($(this).parent().find('.errorDisplay').length > 0) {
				$(this).parent().find('.errorDisplay').remove();
			}
			bindServerErrorValidation("#guest-order-number", "Oops! Some information is missing. You must fill in the required fields");
			return false;
		}
	}

	removeBackendErrorMessagesInline($("#layawayGuestOrderNumberCheckupForm"));

	guestFlag = $(this).hasClass("guest-order-submit");
	layawaypage = $(this).hasClass("layaway-order-num");
	$('.orderError').hide();
	$('.orderNumber,.zipCode').removeClass('error-highlight');
	if ($(this).parents(".sign-in-columns-container").length > 0) {
		var orderId = $(".orderNumber").val();
	} else if ($(this).parents(".my-account-order-history").length > 0) {
		var orderId = $(this).attr('title');
	} else if ($(this).parents(".order-history-table").length > 0) {
		var orderId = $(this).attr('title');
	} else if ($(this).parents(".guest-account-details").length > 0) {
		var orderId = $(".guest-order-number").val();
	}
	var zipCode = $(".zipCode").val();
	if ($(this).parents(".sign-in-columns-container").length > 0 && (orderId == "" || zipCode == "" || orderId == undefined || zipCode == undefined)) {
		$('.orderError').text("Oops! Some information is missing. You must fill in the required fields").show();
		if (orderId == "" || orderId == undefined) $('.orderNumber').addClass('error-highlight');
		if (zipCode == "" || zipCode == undefined) $('.zipCode').addClass('error-highlight');
		if ((orderId == "" || orderId == undefined) && (zipCode == "" || zipCode == undefined)) $('.orderNumber,.zipCode').addClass('error-highlight');
		return false;
	}

	orderId = (orderId).trim();

	if (zipCode != undefined) {
		zipCode = (zipCode).trim();
	}
	else {
		zipCode = "";
	}
	var omsOrderDetailRestAPIURL = $("#omsOrderDetailRestAPIURL").val();
	pushSite = $("#pushSiteId").val();
	$.ajax({
		url: '/myaccount/StaticText.jsp',
		type: "post",
		dataType: "json",
		/*async : false,*/
		success: function (data) {
			orderDetails = data;
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				window.event.cancelBubble = true;
				var redirectionUrl = xhr.getResponseHeader('Referer');
				$(document).find("[data-toggle=modal]").each(function () {
					$(this).attr("data-target", "");
				});
				//setTimeout(function(){
				if (redirectionUrl != '' && redirectionUrl != null && redirectionUrl != 'undefined' && !($('#VisitorFirstNamecookieDIV').html() == 'my account')) {
					if (redirectionUrl.indexOf('?') > -1) {
						location.href = redirectionUrl + '&sessionExpired=true';
					} else {
						location.href = redirectionUrl + '?sessionExpired=true';
					}
				} else {
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'myaccount/myAccount.jsp?sessionExpired=true';
				}
				//},300);
			}
			return false;
		}
	})
	$.ajax({
		url: omsOrderDetailRestAPIURL + "?orderId=" + orderId + "&layawayPage=" + layawaypage + "&postalCode=" + zipCode + "&pushSite=" + pushSite,
		type: "post",
		headers: {
			"X-APP-API_KEY": "apiKey",
			"X-APP-API_CHANNEL": "mobile",
			"Content-Type": "application/x-www-form-urlencoded"
		},
		dataType: "json",
		/*async : false,*/
		beforeSend: function () {
			$('#truOrderStatusSignInForm').find('.errorDisplay').remove();
		},
		success: function (data) {
			var loginStatus = $('#loginStatus').val();
			if (loginStatus == 'true') {
				$('#billingInfoApplicationForm input,#creditCardInfoApplicationForm input,#layawayPaymentAmountForm input').val('');
			} else {
				$('#billingInfoApplicationForm input:not(#orderEmail),#creditCardInfoApplicationForm input,#layawayPaymentAmountForm input').val('');
			}
			$('#state,#expirationMonth,#expirationYear').prop('selectedIndex', 0);
			$(".display-card,#make-payment-edit-button").hide();
			$('#layawayPaymentAmount').removeAttr('readonly', 'readonly');
			$('#make-payment-submit-button').show();
			if (typeof data == 'string') {
				var data1 = $.parseJSON(data);
			}
			else {
				var data1 = data;
			}

			if (layawaypage) {
				var response1 = data1.customerOrder;
			} else {
				var response1 = data1;
			}
			customerOrderDetailsResponse = $.extend(orderDetails, response1);
			if ($(".order-history-table-header").length > 0) {
				if (typeof customerOrderDetailsResponse.customerOrder.CustomerOrderDetailResponse != "undefined") {
					var errorMessage = customerOrderDetailsResponse.customerOrder.CustomerOrderDetailResponse.messages.message.description;
					$('.my-account-order-history .messageFromService').remove();
					$('.my-account-order-history .order-history-header').after('<label class="error messageFromService">' + errorMessage + '</label>');
					return false;
				}
				else {
					$('.my-account-order-history .messageFromService').remove();
					$('.accountOrderHistoryDetailModalPopup .order-history-detail-modal').remove();
					$('#myaccount-order-history-details').tmpl(customerOrderDetailsResponse).appendTo('.accountOrderHistoryDetailModalPopup');
					$("#orderHistoryDetailModal").modal("show");
					setTimeout(function () {
						$(document).find('.accountOrderHistoryDetailModalPopup .tse-scrollable').TrackpadScrollEmulator({
							wrapContent: false,
							autoHide: false
						});
						//$('.accountOrderHistoryDetailModalPopup').find('.tse-scrollable').find('.tse-order-history-content').height(580);
					}, 600);
				}

			} else if ($(".order-history-table").length > 0) {
				$('.messageFromService').remove();
				if (typeof customerOrderDetailsResponse.customerOrder.CustomerOrderDetailResponse != "undefined") {
					var errorMessage = customerOrderDetailsResponse.customerOrder.CustomerOrderDetailResponse.messages.message.description;
					$('.order-history-header').after('<label class="error messageFromService">' + errorMessage + '</label>');
					return false;
				}
				$('.orderHistoryDetailModalPopup .order-history-detail-modal').remove();
				$('#order-history-details').tmpl(customerOrderDetailsResponse).appendTo('.orderHistoryDetailModalPopup');
				$("#orderHistoryDetailModal").modal("show");
				setTimeout(function () {
					$(document).find('.orderHistoryDetailModalPopup .tse-scrollable').TrackpadScrollEmulator({
						wrapContent: false,
						autoHide: false
					});
				}, 600);
			} else if ($(".layaway-order-history-table-header.row").length > 0) {
				if (typeof customerOrderDetailsResponse.OrderDetail != 'undefined' && customerOrderDetailsResponse.OrderDetail != "" && typeof customerOrderDetailsResponse.OrderDetail.Message == 'undefined') {
					layawayOrderInformationDetails = $('#layaway-order-information-details').tmpl(customerOrderDetailsResponse);
					layawayPaymentInformationDetailsLabel = $('#layaway-payment-information-details-label').tmpl(customerOrderDetailsResponse);
					layawayPaymentInformationDetailsValues = $('#layaway-payment-information-details-values').tmpl(customerOrderDetailsResponse);
					layawayOrderProductDetails = $('#layaway-order-product-details').tmpl(customerOrderDetailsResponse);

					$("#layawayGuestDetails .order-info").html(layawayOrderInformationDetails);
					$("#layawayGuestDetails .layawayPaymentInformationDetailsLabel").html(layawayPaymentInformationDetailsLabel);
					$("#layawayGuestDetails .layawayPaymentInformationDetailsValue").html(layawayPaymentInformationDetailsValues);
					$("#guestMakePayment .layawayOrderProduct").html(layawayOrderProductDetails);
					try {
						$('#giftCardInfoApplicationForm').load(window.contextPath + 'myaccount/layaway/layawayGiftCard.jsp');
					} catch (e) { }
					$('.layaway-orders').removeClass('active');
					$('#guestMakePayment').addClass("active");
					var layawayOrderLineStatus = $('.layawayOrderLineStatus').html();
					if (layawayOrderLineStatus == "Active" || layawayOrderLineStatus == "active" || layawayOrderLineStatus == "ACTIVE") {
						$("#layawayPaymentAmountForm, .enterPaymentDetails").show();
						$('#payment-section').show();
						if (guestFlag) {
							$("#guestMakePayment .layawayButtonCS").hide();
							$(".layaway-order-product .layaway-buttons").hide();
						}
						else {
							$(".layaway-order-product .layaway-buttons").hide();
						}
					}
				} else {
					var layawayNoOrderFoundMessage = orderDetails.customerOrderDetails.layawayNoOrderFoundMessage;
					var layawayOrderNumber = $("#order-number").length > 0 ? "#order-number" : "#guest-order-number";
					if (layawayOrderNumber == "#guest-order-number" && $(layawayOrderNumber).parent().find('.errorDisplay').length > 0) {
						$(layawayOrderNumber).parent().find('.errorDisplay').remove();
					}
					//bindServerErrorValidation(layawayOrderNumber,layawayNoOrderFoundMessage);
					/*layawayGlobalErrorDisplayNoRecordFound(layawayOrderNumber,layawayNoOrderFoundMessage);
					$("#layawayGlobalErrorDisplay").show();*/
					$('.forNoOrderFoundMessage').remove();
					$('#layawayGuestOrderNumberCheckupForm').prepend('<label class="error forNoOrderFoundMessage">' + layawayNoOrderFoundMessage + '</label>')
					//$(".orderError").empty().append(noOrderFoundMessage).show();
				}
			} else if ($(".my-account-sign-in-col.order-status").length > 0) {
				var canShowModal = false;
				var giftCardpayInStoreErrorMessgae = customerOrderDetailsResponse.customerOrderDetails.giftCardpayInStoreErrorMessgae;
				if (typeof customerOrderDetailsResponse.customerOrder.OrderHeader != "undefined") {
					email = customerOrderDetailsResponse.customerOrder.OrderHeader.BillingAddress.EmailAddress;
					if ((customerOrderDetailsResponse.customerOrder.OrderHeader.BillingAddress.PostalCode.toString().indexOf(zipCode) == 0 && zipCode.length >= 5)) {
						canShowModal = true;
					}
				} else {
					var noOrderFoundMessage = customerOrderDetailsResponse.customerOrder.CustomerOrderDetailResponse.messages.message.description;
					var zipCodeError = customerOrderDetailsResponse.customerOrder.CustomerOrderDetailResponse.messages.message.zipCodeError;
					if ((typeof (customerOrderDetailsResponse.customerOrder.CustomerOrderDetailResponse.messages.message)).toLowerCase() == "object") {
						if (zipCodeError == "true") {
							var zipCodeNotMatchMessage = customerOrderDetailsResponse.customerOrderDetails.zipCodeNotMatchMessage;
							bindServerErrorValidation("#zip-code", zipCodeNotMatchMessage);
						} else {
							var noOrderFoundMessage = customerOrderDetailsResponse.customerOrderDetails.noOrderFoundMessage;
							bindServerErrorValidation("#order-number", noOrderFoundMessage);
						}
					}
					else if (customerOrderDetailsResponse.customerOrder.CustomerOrderDetailResponse.messages.message.length >= 0) {
						$.each(customerOrderDetailsResponse.customerOrder.messages.message, function (i, val) {
							if (i == 0) {
								//$(".orderError").empty().append(noOrderFoundMessage).show();
								bindServerErrorValidation("#zip-code", noOrderFoundMessage);
							}
						});
					}
				}
				if (canShowModal) {
					$('.accountOrderHistoryDetailModalPopup .order-history-detail-modal').remove();
					$('#myaccount-order-history-details').tmpl(customerOrderDetailsResponse).appendTo('.accountOrderHistoryDetailModalPopup');
					$("#orderHistoryDetailModal").modal("show");
					setTimeout(function () {
						$(document).find('.accountOrderHistoryDetailModalPopup .tse-scrollable').TrackpadScrollEmulator({
							wrapContent: false,
							autoHide: false
						});
					}, 600);
				}

			}
		},
		error: function (e) {
			console.log(e)
		}
	});
})
/*function end to display order details in layaway page*/
