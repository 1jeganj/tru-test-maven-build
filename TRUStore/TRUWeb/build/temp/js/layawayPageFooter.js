var Accept_Encoding;
var Content_Encoding;
var cardinalApi = '';
var layawayOrderId = '';
var layawayPaymentAmount = '';
var isCardinalEnabled = false;
var nonceSetup = false;
var enableVerboseDebug = false;
var enableIntegrationLog = false;
var nonceRetryAttempts = 0;
var tokenizeAndAuthorizeRetryAttempts = 0;
var maxNonceRetryAttempts = 0
var maxTokenizeAndAuthorizeRetryAttempts = 0


function layawayPaymentRecieptTealium() {
	if (typeof utag != 'undefined') {
		var layawayNumber = $("#customerOrderNumber").text();
		if ( layawayNumber.length > 0 ) {
		    utag.link({
		    	layaway_number:layawayNumber,
		    	event_type:'layaway_payment'
		    })
		}
	}
}

$(document).ready(function () {
	$(document).on('click', '[data-target="#giftCardBalanceModal"]', function () {
		removeBackendErrorMessagesInline($('#giftCardBalanceForm'));
	});
});

$(document).one("click", "#account-and-payment", function () {
	guestFlag = $('#loginStatus').val();
	if (guestFlag == 'false') {
		loadorderHistoryOrders(true);
	}
});
$(document).on('click', '#account-and-payment', function () {

	if (!displayRecordsInLayaway) {
		$('.layaway-orders').removeClass('active');
		$('#layaway-guest-account').addClass("active");
	} else {
		$('.layaway-orders').removeClass('active');
		$('#layaway-guest-account').addClass("active");
		$('#orderHistory').addClass("active");
	}
});

$(document).on("click", ".backToOrderHistory", function () {
	if (guestFlag) {
		$('.layaway-guest').find(".tab-content").hide();
		$(".layaway-guest").find(".guest-order-number,.guest-order-submit").show();
		$(".layaway-guest").find("input").val("");
	}
	else {
		$('.my-account-order-history').find(".tab-content").hide();
		$('.my-account-order-history .order-history-order.row').show();
	}
	$('#layaway-guest-account').addClass("active");
	//$('.layaway-guest-details').hide();
	$('#guestMakePayment').removeClass("active");
});


$(document).on("keyup", ".guest-order-number", function () {
	var guestOrderNumbe = $(this).val();
	if (guestOrderNumbe != "") {
		$(".guest-order-submit").removeClass("disableButton");
	}
	else {
		$(".guest-order-submit").addClass("disableButton");
	}
	$(".enter-button.guest-order-submit.layaway-order-num").removeClass("disableButton");
});

$(document).on("submit", "#layawayGuestOrderNumberCheckupForm", function () {
	$(this).find(".guest-order-submit").click();
	return false;
});

$(document).ajaxComplete(function () {
	var giftcardTooltip = $('#giftcardTooltip');
	var giftCardContent = $('#giftCardHelpContent').html();
	var giftcardTooltipTemplate = '<div class="popover giftcardTooltippopover" role="tooltip"><div class="arrow"></div>' + giftCardContent + '</div>';

	giftcardTooltip.click(function (event) {
		event.preventDefault();
		var userAgent = navigator.userAgent;
		if (userAgent.indexOf('Macintosh') != -1 && userAgent.indexOf('Safari') != -1) {
			if (typeof $(this).attr('aria-describedby') === "undefined") {
				giftcardTooltip.popover('show');
			}
		}
	});
	giftcardTooltip.popover({
		animation: 'true',
		html: 'true',
		content: 'asdfasdf',
		placement: 'bottom',
		template: giftcardTooltipTemplate,
		trigger: 'focus'
	});
	// Adding the front-end validations to the forms
	universalFormValidationHandler();
});


function giftCardHelpWindowLayaway(url) {
	popupWindow = window.open(
		url, 'popUpWindow', 'height=450,width=600,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}

$(document).on('click', '#giftCardBalanceOverlayBtn', function () {
	checkGiftCardBalance('giftCardBalanceForm');
	return false;
});

$(document).on('click', '.check-another', function () {
	$('.gift-card-container input').val('');
	removeBackendErrorMessagesInline($("#giftCardBalanceForm"));
	$('.gift-card-container').show();
	$('.gift-card-balance-container').hide();
});

$(document).on('click', '.checkGiftCarddetails', function () {
	$('.errorDisplay').remove();
	$('#giftCardBalanceModal input').removeClass('error-highlight');
	$('#giftCardBalanceModal .gift-card-container').show();
	$('#giftCardBalanceModal .gift-card-container input').val('');
	$('#giftCardBalanceModal .gift-card-balance-container').hide();
});

$(document).on('click', '.order-history-link', function () {
	var orderId = $(this).data('orderid');
	var status = $(this).data('status');
	if (status == 'pending') {
		$('.layaway-orders').removeClass('active');
		$('#orderDetail').addClass("active");
	} else if (status == 'active') {
		$('.layaway-orders').removeClass('active');

		$('#guestMakePayment').addClass("active");

		$.ajax({
			type: "POST",
			url: "/myaccount/layaway/layawayPaymentFragment.jsp",
			cache: false,
			async: true,
			dataType: "html",
			success: function (responseData) {
				$('#guestMakePayment').html(responseData);
			},
			error: function (a, b, c) {
				console.log(a);
			}
		});
	}

});

$(document).on('click', '#layaway-credit-card-payment', function () {
	removeBackendErrorMessagesInline($("#guestMakePayment"));
	if (!$("#layawayPaymentAmountForm").valid()) {
		return false;
	}
	if ($(document).find($("#layawayPaymentAmount")).attr("readonly") != "readonly") {
		// TODO: Write script for how to throw an error for submitting the payable amount
		$(document).find("#layawayGlobalErrorDisplay").html("We are sorry; the payment amount must be submitted first").show();
		return false;
	}
	if (!$("#billingInfoApplicationForm").valid()) {
		// TODO: Take the user to this position

		return false;
	}
	var creditTestFlag = false;
	$("#creditCardInfoApplicationForm input").each(function () {
		if ($(this).attr("type") == "text") {
			if ($(this).val().trim() != "") {
				creditTestFlag = true;
			}
		}
	});
	if (creditTestFlag) {
		if (!$("#creditCardInfoApplicationForm").valid()) {
			return false;
		}
	}
	$.ajax({
		type: "POST",
		url: "/myaccount/layaway/intermediate/ajaxLayawayPaymentResponse.jsp",
		data: { formID: 'checkIfGiftCardSufficient' },
		async: true,
		success: function (responseData) {
			var isCreditCardNotEntered = 'false';
			var remainingAmount = $(responseData).find('#remainingAmountValue').val();
			var accountNumber = $('#ccnumber').val();
			var expirationMonth = $('#expirationMonth').val();
			var expirationYear = $('#expirationYear').val();
			var cardCode = $('#creditCardCVV').val();
			var nameOnAccount = $('#nameOnCard').val();
			if (accountNumber == '' || expirationMonth == '' || expirationYear == '' || cardCode == '' || nameOnAccount == '') {
				isCreditCardNotEntered = 'true';
			}
			if ((remainingAmount == '0.0' && isCreditCardNotEntered == 'true') || layawayOrderId == '' || layawayPaymentAmount == '') {
				commitLayawayOrder();
			} else {
				removeBackendErrorMessagesInline($("#guestMakePayment"));
				if (!$("#creditCardInfoApplicationForm").valid()) {
					checkMaskPasswordError($("#creditCardInfoApplicationForm"));
					return false;
				}
				var cardNum = $('#ccnumber').val();
				var isValidCard = validateCreditCardNumber(cardNum);
				if (isValidCard) {

					checkPaymentDetails();
				} else {
					addBackendErrorsToForm(window.invalidCardErroMsg, $('.layaway-template #layawayGlobalErrorDisplay'));
				}
			}
			layawayPaymentRecieptTealium();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'myaccount/layaway/myAccountLayaway.jsp?sessionExpired=true';
			}
		},
		dataType: "html"
	});
});

function commitLayawayOrder() {
	if (!$("#billingInfoApplicationForm").valid() || !$("#layawayPaymentAmountForm").valid()) {
		return false;
	}
	$.when(callingFraudJsCollector()).then(function () {
		var browserAcceptEncoding = "";
		if (Accept_Encoding != undefined) {
			browserAcceptEncoding = Accept_Encoding;
		} else if (Content_Encoding != undefined) {
			browserAcceptEncoding = Content_Encoding;
		}
		var deviceID = $("#field_d1").val();
		deviceID = deviceID.substring(0, 4000);

		removeBackendErrorMessagesInline($("#guestMakePayment"));
		var amountDue = $("#outStandingAmt").html();
		amountDue = amountDue.replace('$', '');
		$.ajax({
			type: "POST",
			url: "/myaccount/layaway/intermediate/ajaxLayawayPaymentResponse.jsp",
			data: $("#billingInfoApplicationForm").serialize() + '&' + $("#layawayPaymentAmountForm").serialize() + '&formID=billingInfoApplicationForm' + '&layawayAmountDue=' + amountDue + '&deviceID=' + deviceID + '&browserAcceptEncoding=' + browserAcceptEncoding,
			async: true,
			success: function (responseData) {
				if (responseData.indexOf('Following are the form errors:') > -1) {
					if (layawayOrderId == '' || layawayPaymentAmount == '') {
						/*$('.layaway-header').before(responseData.split('Following are the form errors:')[1] );
						$('.errorDisplay').addClass('layawayError');
						addError('layawayPaymentAmountForm');*/
						addBackendErrorsToForm(responseData, $("#layawayPaymentAmountForm"));
						$(window).scrollTop(0);
					} else {
						addBackendErrorsToForm(responseData, $("#layawayPaymentAmountForm"));
						/*$('.layaway-payment-error').append(responseData.split('Following are the form errors:')[1] );
						addError('billingInfoApplicationForm');*/
					}
				} else {
					if (responseData.indexOf('address-doctor-overlay-from-ajax:') > -1) {
						$('#layaway-page-suggested-address .modal-dialog-shipping-page-suggested').html(responseData.split('address-doctor-overlay-from-ajax:')[1]);
						$('#layaway-page-suggested-address').modal('show');
					} else if (responseData.indexOf('address-saved-success-from-ajax:') > -1) {
						$('.layaway-orders').removeClass('active');
						$('#layawayPaymentReceipt').addClass("active");
						$.ajax({
							type: "POST",
							url: "/myaccount/layaway/layawayPaymentReceipt.jsp",
							cache: false,
							async: true,
							dataType: "html",
							success: function (responseData) {
								$(window).scrollTop(0);
								$('#layawayPaymentReceipt').html(responseData);
							},
							error: function (xhr, ajaxOptions, thrownError) {
								if (xhr.status == '409') {
									var contextPath = $(".contextPath").val();
									location.href = contextPath + 'myaccount/layaway/myAccountLayaway.jsp?sessionExpired=true';
								}
							}
						});
					}
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'myaccount/layaway/myAccountLayaway.jsp?sessionExpired=true';
				}
			},
			dataType: "html"
		});
	});
}

function commitCreditCard() {
	removeBackendErrorMessagesInline($("#guestMakePayment"));
	if (!$("#creditCardInfoApplicationForm").valid() || !$("#billingInfoApplicationForm").valid() || !$("#layawayPaymentAmountForm").valid()) {
		checkMaskPasswordError();
		return false;
	}
	$.when(callingFraudJsCollector()).then(function () {
		var browserAcceptEncoding = "";
		if (Accept_Encoding != undefined) {
			browserAcceptEncoding = Accept_Encoding;
		} else if (Content_Encoding != undefined) {
			browserAcceptEncoding = Content_Encoding;
		}
		var deviceID = $("#field_d1").val();
		deviceID = deviceID.substring(0, 4000);
		var amountDue = $("#outStandingAmt").text();
		amountDue = amountDue.replace('$', '');
		$.ajax({
			type: "POST",
			url: "/myaccount/layaway/intermediate/ajaxLayawayPaymentResponse.jsp",
			data: $("#billingInfoApplicationForm").serialize() + '&' + $("#layawayPaymentAmountForm").serialize() + '&' + $("#creditCardInfoApplicationForm").serialize() + '&formID=creditCardInfoApplicationForm' + '&layawayAmountDue=' + amountDue + '&deviceID=' + deviceID + '&browserAcceptEncoding=' + browserAcceptEncoding,
			async: true,
			success: function (responseData) {
				if (responseData.indexOf('Following are the form errors:') > -1) {
					//$('.layaway-payment-error').append(responseData.split('Following are the form errors:')[1] );
					addBackendErrorsToForm(responseData, $("#billingInfoApplicationForm"));
				} else {
					if (responseData.indexOf('address-doctor-overlay-from-ajax:') > -1) {
						$('#layaway-page-suggested-address .modal-dialog-shipping-page-suggested').html(responseData.split('address-doctor-overlay-from-ajax:')[1]);
						$('#layaway-page-suggested-address').modal('show');
					} else if (responseData.indexOf('address-saved-success-from-ajax:') > -1) {
						$('.layaway-orders').removeClass('active');
						$('#layawayPaymentReceipt').addClass("active");
						$.ajax({
							type: "POST",
							url: "/myaccount/layaway/layawayPaymentReceipt.jsp",
							cache: false,
							async: true,
							dataType: "html",
							success: function (responseData) {
								$('#layawayPaymentReceipt').html(responseData);
								$('#layawayPaymentReceipt').scrollTop(0);
							},
							error: function (xhr, ajaxOptions, thrownError) {
								if (xhr.status == '409') {
									var contextPath = $(".contextPath").val();
									location.href = contextPath + 'myaccount/layaway/myAccountLayaway.jsp?sessionExpired=true';
								}
							}
						});
					}
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == '409') {
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'myaccount/layaway/myAccountLayaway.jsp?sessionExpired=true';
				}
			},
			dataType: "html"
		});
		return false;
	});
}

$(document).on('click', '#layaway-payment-gift-card', function () {
	//$(".errorDisplay").remove();
	removeBackendErrorMessagesInline($("#guestMakePayment"));
	if (!$("#giftCardInfoApplicationForm").valid() || !$("#layawayPaymentAmountForm").valid()) {
		return false;
	}
	$.ajax({
		type: "POST",
		url: "/myaccount/layaway/intermediate/ajaxLayawayPaymentResponse.jsp",
		data: $("#giftCardInfoApplicationForm").serialize() + '&' + $("#layawayPaymentAmountForm").serialize() + '&formID=giftCardInfoApplicationForm',
		async: true,
		success: function (data) {
			if (data.indexOf('Following are the form errors:') > -1) {
				if (layawayOrderId == '' || layawayPaymentAmount == '') {
					/*$('.layaway-header').before(data.split('Following are the form errors:')[1] );
					$('.errorDisplay').addClass("layawayError");
					addError('layawayPaymentAmountForm');
					$(window).scrollTop(0);*/
					addBackendErrorsToForm(data, $("#layawayPaymentAmountForm"));
				} else {
					//$('.layaway-giftcard-error').append(data.split('Following are the form errors:')[1] );
					addBackendErrorsToForm(data, $("#giftCardInfoApplicationForm"));
				}
			}
			else {
				$(".checkout-payment-giftcard input").val('');
				var giftCardList = $(data).find('.giftCardList').html();
				$('.gift-layaway-card-table').replaceWith(giftCardList);
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'myaccount/layaway/myAccountLayaway.jsp?sessionExpired=true';
			}
		},
		dataType: "html"
	});
	return false;
});

$(document).on('click', '.gift-layaway-card-table .pull-layaway-right', function () {
	var cardId = $(this).data('id');
	$.ajax({
		type: "POST",
		url: window.contextPath + "myaccount/layaway/intermediate/ajaxLayawayPaymentResponse.jsp",
		data: { cardId: cardId, formID: 'layawayGiftCardRemoveForm' },
		dataType: "json",
		async: true,
		success: function (data) {
			if (data.indexOf('Following are the form errors:') > -1) {
				$('.layaway-giftcard-error').append(data.split('Following are the form errors:')[1]);
			}
			else {
				var giftCardList = $(data).find('.giftCardList').html();
				$('.gift-layaway-card-table').replaceWith(giftCardList);
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'myaccount/layaway/myAccountLayaway.jsp?sessionExpired=true';
			}
		},
		dataType: "html"
	});
	return false;
});

$(document).on('click', '#make-payment-submit-button', function () {
	/*$(".errorDisplay").remove();
	$('#layawayPaymentAmountForm input').removeClass('error-highlight');*/
	removeBackendErrorMessagesInline($("#guestMakePayment"));
	if (!$("#layawayPaymentAmountForm").valid()) {
		return false;
	}
	var amountDue = $("#outStandingAmt").html();
	amountDue = amountDue.replace('$', '');
	var layawayCustomerOrderId = $('#customerOrderNumber').html();
	var layawayCustomerName = $('#customerLayawayName').html();
	$.ajax({
		type: "POST",
		url: window.contextPath + "myaccount/layaway/intermediate/ajaxLayawayPaymentResponse.jsp",
		data: $("#layawayPaymentAmountForm").serialize() + '&formID=layawayPaymentAmountForm' + '&layawayAmountDue=' + amountDue + '&layawayOrderId=' + layawayCustomerOrderId + '&layawayCustomerName=' + layawayCustomerName,
		async: true,
		success: function (responseData) {
			if (responseData.indexOf('Following are the form errors:') > -1) {
				/*$('.layaway-header').before(responseData.split('Following are the form errors:')[1] );*/
				addBackendErrorsToForm(responseData, $("#layawayPaymentAmountForm"));
				//$(window).scrollTop(0);
			}
			else {
				var cardinalUpdateList = $(responseData).find('.cardinalUpdateList').html();
				var giftCardList = $(responseData).find('.giftCardList').html();
				$('.gift-layaway-card-table').replaceWith(giftCardList);
				$('#cardianlOrderDetails').replaceWith(cardinalUpdateList);
				$('#layawayPaymentAmount').attr('readonly', 'readonly');
				window.layawayOrderId = $('#cardianlOrderId').val();
				window.layawayPaymentAmount = $('#layawayPaymentAmount').val();
				$('#make-payment-submit-button').hide();
				$('#make-payment-edit-button').show();
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'myaccount/layaway/myAccountLayaway.jsp?sessionExpired=true';
			}
		},
		dataType: "html"
	});
});

function displayPaymentPage() {
	$.ajax({
		type: "POST",
		url: "/myaccount/layaway/layawayPaymentReceipt.jsp",
		cache: false,
		async: true,
		dataType: "html",
		success: function (responseData) {
			$('#layawayPaymentReceipt').html(responseData);
			$('#layawayPaymentReceipt').scrollTop(0);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'myaccount/layaway/myAccountLayaway.jsp?sessionExpired=true';
			}
		}
	});
}

$(document).on('click', '#make-payment-edit-button', function () {

	$.ajax({
		type: "POST",
		url: "/myaccount/layaway/exitPaymentOverlay.jsp",
		data: { formID: 'removePaymentGroupsFromOrder' },
		async: true,
		success: function (responseData) {
			$('#layawayExitPaymentModal').modal('show');
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'myaccount/layaway/myAccountLayaway.jsp?sessionExpired=true';
			}
		},
		dataType: "html"
	});

});

$(document).on('click', '#cancel-exit-process', function () {
	$('#layawayExitPaymentModal').modal('hide');
});

$(document).on('click', '#yes-exit-payment', function () {
	$('#layawayExitPaymentModal').modal('hide');
	$(".errorDisplay").remove();
	$.ajax({
		type: "POST",
		url: "/myaccount/layaway/intermediate/ajaxLayawayPaymentResponse.jsp",
		data: { formID: 'removePaymentGroupsFromOrder' },
		async: true,
		success: function (responseData) {
			$('#layawayPaymentAmount').removeAttr('readonly', 'readonly');
			$('#make-payment-submit-button').show();
			$('#make-payment-edit-button').hide();
			var giftCardList = $(responseData).find('.giftCardList').html();
			$('.gift-layaway-card-table').replaceWith(giftCardList);
			window.layawayPaymentAmount = "";
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'myaccount/layaway/myAccountLayaway.jsp?sessionExpired=true';
			}
		},
		dataType: "html"
	});
});


//Order Look up for Layaway orders
$(document).on('click', '.button-order-lookup', function () {
	$(".errorDisplay").remove();
	var layawayNumber = $('.layaway-order-number').val();
	if (layawayNumber == '' || layawayNumber.length < 13) {
		$('.layaway-order-header').eq(0).append('<span class="errorDisplay">Please enter your entire layaway order number</span>');
		return false;
	} else {
		$.ajax({
			type: "POST",
			url: "/myaccount/layaway/intermediate/ajaxLayawayPaymentResponse.jsp",
			data: $("#layawayOrderLookUpForm").serialize() + '&formID=layawayOrderLookUpForm',
			async: true,
			success: function (responseData) {

			},
			error: function (a, b, c) {
				console.log(a);
			},
			dataType: "html"
		});
	}
});


function onCountryChangeInPayment() {
	var selectCountry = $('#country').val();
	$.ajax({
		url: window.contextPath + 'myaccount/intermediate/ajaxIntermediateRequest.jsp?selectCountry=' + selectCountry + "&formID=changeCountry",
		dataType: 'html',
		success: function (data) {
			$('#state').html(data);
			if (selectCountry != 'US') {
				$('input[id="otherState"]').addClass('display-inlineblock');
				$(".creditCardOtherStateLabel").addClass('display-inlineblock');
			} else {
				$('input[id="otherState"]').removeClass('display-inlineblock');
				$(".creditCardOtherStateLabel").removeClass('display-inlineblock');
			}
		}
	});
}


function checkPaymentDetails() {

	var accountNumber = $('#ccnumber').val();
	var expirationMonth = $('#expirationMonth').val();
	var expirationYear = $('#expirationYear').val();
	var cardCode = $('#creditCardCVV').val();
	var nameOnAccount = $('#nameOnCard').val();

	addError('creditCardInfoApplicationForm');

	var v = parseInt(accountNumber.substr(0, 6));

	if (v == '604586') {
		if (accountNumber == '' || cardCode == '' || nameOnCard == '') {
			$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">Please enter complete card details</span>');
			return false;
		}
		expirationMonth = $('#expMonthForPLCC').val();
		expirationYear = $('#expYearForPLCC').val();
	} else {
		if (accountNumber == '' || expirationMonth == '' || expirationYear == '' || cardCode == '' || nameOnAccount == '') {
			$('.layaway-creditCard-error').eq(0).append('<span class="errorDisplay">Please enter complete card details</span>');
			$('.layaway-creditCard-error').scrollTop(0);
			return false;
		}
	}


	var d = new Date();
	var timeStamp = d.getTime();

	var currentMonth = d.getMonth() + 1;
	var currentYear = d.getFullYear();
	if (parseInt(expirationMonth) < currentMonth && parseInt(expirationYear) == currentYear) {
		$('.layaway-creditCard-error').eq(0).append('<span class="errorDisplay">We are sorry, the expiry date of the card should be a future date</span>');
		return false;
	}
	var orderAmt = parseInt($('#cardianlOrderAmount').val() * 100);
	if (isCardinalEnabled) {
		if (v == '604586') {
			getNONCEResponce();
			cardinalApi = 'creditCardCommitOrder';
			Radial.tokenize(accountNumber, tokenizeCallBackHandler);
			return false;
		} else {
			startTransaction();
		}
	} else {
		$('#cardinalToken').val($('#ccnumber').val());
		$('#cBinNum').val($('#ccnumber').val().substring(0, 6));
		$('#cLength').val($('#ccnumber').val().length);
		$("#layawayPaymentProcessingOverlay").modal('show');
		commitCreditCard();
		$("#layawayPaymentProcessingOverlay").modal('hide');
	}
}

function isNumber(evt) {
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (navigator.userAgent.indexOf("Firefox") > 0) {
		if (charCode == 46 || charCode == 8 || (charCode > 47 && charCode < 58) || evt.which == 0) {
			return true;
		}
		return false;
	}
	if (charCode == 46 || charCode == 8 || (charCode > 47 && charCode < 58)) {
		return true;
	}
	return false;
}
$(document).on("paste", "#layawayPaymentAmount", function () {
	return false;
})

function isZero(elm) {
	var value = $(elm).val();
	if (value) {
		var new_value = value + '';

		pos = new_value.indexOf('.');
		if (pos == -1) new_value = new_value + '.00';
		document.getElementById("layawayPaymentAmount").value = new_value;
	}
}
function termAndCondition(url) {
	popupWindow = window.open(
		url, 'popUpWindow', 'height=450,width=600,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}

$(document).on('click', '#LayawayLogoutNotYouId', function () {
	var clikedID = $(this).attr('id') || '';
	doSignOut(clikedID);
	signOutForTealium();
});

$(document).on('click', '#layaway-payment-page', function () {
	$('#account-and-payment').click();
});

$(document).on('click', '#layaway-tabs li a', function () {
	var title = $(this).text();
	$('#layaway-tab-display').text(title);
});

$(document).on('click', '#breadcrumb-layaway-header', function () {
	$('#layaway-tabs li a')[0].click();
});

$(document).on('click', '.backToOrderLookUp', function () {
	var loginStatus = $('#loginStatus').val();
	console.log(orderHistory);
	if (loginStatus == "false" && orderHistory.OrderList[0].Message != 'undefined' && orderHistory.OrderList[0].Message != 'NODATAFOUND') {
		$('#orderHistory').addClass('active');
	}
	$('#layaway-guest-account').addClass("active");
	$('#guestMakePayment').removeClass("active");
	$('#layawayPaymentReceipt').removeClass('active');
	$(window).scrollTop(0);
});

function layawayGlobalErrorDisplayNoRecordFound(error, errorText) {
	if (errorText.indexOf("Oops, there was an issue.") > -1) {
		errorText = errorText.trim().split("Oops, there was an issue.")[1];
	}
	else {
		errorText = errorText;
	}
	$("#layawayGlobalErrorDisplay").html('<div class="shopping-cart-error-state"><div class="shopping-cart-error-state-header row"><div class="error-state-exclamation-lg img-responsive col-md-2"></div><div class="shopping-cart-error-state-header-text col-md-10">Oops. There was an issue<div class="shopping-cart-error-state-subheader">' + errorText + '</div></div></div></div>');
}

function onSubmitLayawaySuggestedOverlay() {
	/*$('.errorDisplay').remove();*/
	removeBackendErrorMessagesInline($("#guestMakePayment"));
	$.ajax({
		type: "POST",
		async: true,
		url: "/myaccount/layaway/intermediate/ajaxLayawayPaymentResponse.jsp",
		data: $("#saveLayawayAddressFromSuggested").serialize() + '&formID=addLayawayAddFromSuggestedOverlay',
		success: function (data) {
			if (data.indexOf('Following are the form errors:') > -1 && !(data.indexOf('address-saved-success-from-ajax:orderReconciliationNeeded:') > -1)) {
				/*$('.layaway-payment-error').html( data.split('Following are the form errors:')[1] );*/
				addBackendErrorsToForm(data, $("#layawayPaymentAmountForm"));
			} else {
				$('.layaway-orders').removeClass('active');
				$('#layawayPaymentReceipt').addClass("active");
				$.ajax({
					type: "POST",
					url: "/myaccount/layaway/layawayPaymentReceipt.jsp",
					cache: false,
					async: true,
					dataType: "html",
					success: function (responseData) {
						$(window).scrollTop(0);
						$('#layawayPaymentReceipt').html(responseData);
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == '409') {
							var contextPath = $(".contextPath").val();
							location.href = contextPath + 'myaccount/layaway/myAccountLayaway.jsp?sessionExpired=true';
						}
					}
				});

			}
		},
		dataType: "html",
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				var contextPath = $(".contextPath").val();
				location.href = contextPath + 'myaccount/layaway/myAccountLayaway.jsp?sessionExpired=true';
			}
		}
	});
	return false;
}
function editUserEnteredLayawayAddress() {
	suggestedShippingAddressSelect('0');
	$('#layaway-page-suggested-address').modal('hide');
}

function suggestedShippingAddressSelect(id) {
	var count = $('input[name="valid-address"]:checked').val();
	if (typeof (count) != "undefined") {
		$("#suggestedAddress1Id").val($("#suggestedAddress1-" + count).val());//remove the parent form id																				
		$("#suggestedAddress2Id").val($("#suggestedAddress2-" + count).val());
		$("#suggestedCityId").val($("#suggestedCity-" + count).val());
		$("#suggestedStateId").val($("#suggestedState-" + count).val());
		$("#suggestedCountryId").val('US');
		$("#suggestedPostalCodeId").val($("#suggestedPostalCode-" + count).val());
		document.getElementById("continueWithSugestedAddr").disabled = false;
	}
}

function submitLayawayAddressFromOverlay() {
	suggestedShippingAddressSelect('1');
	$("#saveLayawayAddressFromSuggested").submit();
	$("html").removeClass("modal-open");
	$("body").css('padding-right', '');
	$('#layaway-page-suggested-address').modal('hide');
}

//cardinal TokenizationAndAuthentication call
function startTransaction() {
	$('.errorDisplay').remove();
	var accountNumber, expirationMonth, expirationYear, cardCode;
	cardCode = $('#creditCardCVV').val();
	accountNumber = $('#ccnumber').val();
	expirationMonth = $('#expirationMonth').val();
	expirationYear = $('#expirationYear').val();
	var nameOnAccount = $('#nameOnCard').val();
	var firstName = $('#firstName').val();
	var lastName = $('#lastName').val();
	var address1 = $('#address1').val();
	var address2 = $('#address2').val();
	var city = $('#city').val();
	var state = $('#state').val();
	var postalCode = $('#postalCode').val();
	var country = $('#country').val();
	var phoneNumber = $('#phoneNumber').val();
	var orderEmail = $('#orderEmail').val();
	var v = parseInt(accountNumber.substr(0, 6));
	if (v == '604586') {
		if (accountNumber == '' || cardCode == '' || nameOnAccount == '') {
			$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">Please enter complete card details</span>');
			return false;
		}
		expirationMonth = $('#expMonthForPLCC').val();
		expirationYear = $('#expYearForPLCC').val();
	} else {
		if (accountNumber == '' || expirationMonth == '' || expirationYear == '' || cardCode == '' || nameOnAccount == '') {
			$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">Please enter complete card details</span>');
			return false;
		}
	}

	var d = new Date();
	var timeStamp = d.getTime();

	var currentMonth = d.getMonth() + 1;
	var currentYear = d.getFullYear();
	if (parseInt(expirationMonth) < currentMonth && parseInt(expirationYear) == currentYear) {
		//$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">The expiration date you entered occurs in the past. Please re-enter the credit card number, expiration date and security code.</span>');
		addBackendErrorsToForm('Following are the form errors:The expiration date you entered occurs in the past. Please re-enter the credit card number, expiration date and security code.', $('#checkout-container'));
		return false;
	}

	var orderAmt = parseFloat($('#cardianlOrderAmount').val()).toFixed(2).replace('.', '');
	var customerIp = $.cookie("customerIpAddress");
	if (customerIp == undefined) {
		customerIp = "0:0:0:0:0:0:0:1";
	}
	//Get shipping address
	var shipAddressDetails = getLayawayShippingAddress();
	var shipAddress = shipAddressDetails.shipAddress;
	var RequestId = shipAddressDetails.cardinalRequestId;
	if (shipAddress == '' || shipAddress == undefined) {
		var jsonObject = {
			"shipAddressBA": {
				"firstName": firstName,
				"lastName": lastName,
				"address1": address1,
				"address2": address2,
				"city": city,
				"state": state,
				"countryCode": country,
				"postalCode": postalCode,
				"phone1": phoneNumber,
			}
		}
		shipAddress = jsonObject.shipAddressBA;
	}

	var jsonObject =
		{
			"cmpiRequestObject": {
				"RequestId": RequestId,
				"OrderNumber": $('#cardianlOrderId').val(),
				"CurrencyCode": $('#currencyCode').val(),
				"Amount": orderAmt,
				"CardExpMonth": expirationMonth,
				"CardExpYear": expirationYear,
				"CardNumber": accountNumber,
				"EMail": orderEmail,
				"BillingFirstName": firstName,
				"BillingLastName": lastName,
				"BillingAddress1": address1,
				"BillingAddress2": address2,
				"BillingCity": city,
				"BillingState": state,
				"BillingCountryCode": country,
				"BillingPostalCode": postalCode,
				"BillingPhone": phoneNumber,
				"IPAddress": customerIp,  // Client’s IP
				"ShippingFirstName": shipAddress.firstName,
				"ShippingLastName": shipAddress.lastName,
				"ShippingAddress1": shipAddress.address1,
				"ShippingAddress2": shipAddress.address2,
				"ShippingCity": shipAddress.city,
				"ShippingState": shipAddress.state,
				"ShippingCountryCode": shipAddress.countryCode,
				"ShippingPostalCode": shipAddress.postalCode,
				"ShippingPhone": shipAddress.phone1,
				"IPAddress": customerIp,
				"UserAgent": navigator.userAgent
			}

		}
	if (enableVerboseDebug) {
		console.log("calling getNONCEResponce() for accountNumber:" + accountNumber);
	}
	getNONCEResponce();
	if (enableVerboseDebug) {
		console.log("calling tokenizeAndAuthorize() for accountNumber:" + accountNumber);
	}

	if (enableIntegrationLog) {
		integrationLogCall('true', 'tokenizationAndAuthentication');
	}

	Radial.tokenizeAndAuthorize(accountNumber, jsonObject, tokenizeAndAuthorizeCallbackHandler);
	cardinalApi = "TokenizationAndAuthentication";
}

function integrationLogCall(entry, interfaceName) {
	var truIntegrationLoggingRestAPIURL = $("#truIntegrationLoggingRestAPIURL").val();
	var orderId = $("#orderId").val();
	var pushSite = $("#pushSiteId").val();
	$.ajax({
		url: truIntegrationLoggingRestAPIURL + "?orderId=" + orderId + "&pushSite=" + pushSite + "&entry=" + entry + "&interfaceName=" + interfaceName,
		type: "post",
		headers: {
			"X-APP-API_KEY": "apiKey",
			"X-APP-API_CHANNEL": "mobile",
			"Content-Type": "application/x-www-form-urlencoded"
		},
		dataType: "json",
		async: false,
		success: function (data) {
			console.log();
		}
	});
}

function getNONCEResponce() {
	var errorFlag = false;
	if (enableVerboseDebug) {
		console.log("Inside getNONCEResponce()");
	}
	$.ajax({
		url: "/checkout/common/nonceAjax.jsp",
		type: "POST",
		dataType: "json",
		async: false,
		cache: false,
		success: function (nonceData) {
			if (enableVerboseDebug) {
				console.log("getNONCEResponce() Radial Nonce:" + JSON.stringify(nonceData));
				console.log("getNONCEResponce() START Radial setup()");
			}
			if (typeof nonceData == 'object' && typeof nonceData.nonce != 'undefined' && typeof nonceData.jwt != 'undefined' && nonceData.nonce != '' && nonceData.radialPaymentErrorKey != '50002') {
				Radial.setup(nonceData.nonce, nonceData.jwt);
				if (enableVerboseDebug) {
					console.log("getNONCEResponce() END calling Radial setup()");
				}
				jwt = nonceData.jwt;
				nonceSetup = true;
				nonceRetryAttempts = 0;
				return false;
			}
			nonceRetryAttempts++;
			if (enableVerboseDebug) {
				console.log("Error in getNONCEResponce() and showing the Error radialPaymentErrorKey:" + nonceData.radialPaymentErrorKey);
			}
			if (nonceRetryAttempts == maxNonceRetryAttempts) {
				if (enableVerboseDebug) {
					console.log("Error in getNONCEResponce() and showing the Error msg");
				}
				nonceRetryAttempts = 0;
				nonceSetup = false;
				var radailPaymentErrorCode = nonceData.radialPaymentErrorKey;
				var radailPaymentKey = errorKeyJson[radailPaymentErrorCode];
				var radailPaymentMsg = errorKeyJson[radailPaymentKey];
				if ($('.layaway-template.container-fluid').length == 1) {
					$('.layaway-payment-error').eq(0).before('<span class="errorDisplay">' + radailPaymentMsg + '</span>');
				} else {
					$('#addOrEditCreditCard .tse-content').prepend('<span class="errorDisplay">' + radailPaymentMsg + '</span>');
					$('#addOrEditCreditCard .tse-scroll-content').scrollTop(0);
				}
				return false;
			}
			errorFlag = true;
		},
		error: function (e) {
			console.log(e);
		}
	});
	if (errorFlag) {
		if (enableVerboseDebug) {
			console.log("Error in getNONCEResponce() and Re_trying with Re_try attempts count is: " + nonceRetryAttempts);
		}
		getNONCEResponce();
	}
}

//Start : Tokenize method
function tokenizeCallBackHandler(data) {
	if (enableVerboseDebug) {
		console.log("tokenizeCallBackHandler() : Start***");
		console.log("tokenizeCallBackHandler() response response data:" + JSON.stringify(data));
	}
	var radailPaymentErrorCode;
	switch (data.ActionCode) {
		case "FAILURE":
			radailPaymentErrorCode = data.failure_code;
			break;
		case "ERROR":
			radailPaymentErrorCode = data.error_code;
			break;
	}
	//getting nonce again and re_try token
	if (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '50002' && getTokenRetryAttempts < maxGetTokenRetryAttempts) {
		getTokenRetryAttempts++;
		//Added  time out for defect=TSJ-6625
		setTimeout(function () { checkPaymentDetails() }, 4000);
		return false;
	}

	//re_try token
	if (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '50001' || radailPaymentErrorCode == '50003' && getTokenRetryAttempts < maxGetTokenRetryAttempts) {
		getTokenRetryAttempts++;
		//Added  time out for defect=TSJ-6625
		setTimeout(function () { checkPaymentDetails(); }, 4000);
		return false;
	}

	if (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '40001' || radailPaymentErrorCode == '40002' || radailPaymentErrorCode == '40003' || radailPaymentErrorCode == '40004' || radailPaymentErrorCode == '50001' || radailPaymentErrorCode == '50002' || radailPaymentErrorCode == '50003') {
		getTokenRetryAttempts = 0;
		$('.layaway-payment-error').eq(0).before('<span class="errorDisplay">' + radailPaymentMsg + '</span>');
		return false;
	}

	var truIntegrationLoggingRestAPIURL = $("#truIntegrationLoggingRestAPIURL").val();
	var orderId = $("#orderId").val();
	var pushSite = $("#pushSiteId").val();
	$.ajax({
		url: truIntegrationLoggingRestAPIURL + "?orderId=" + orderId + "&pushSite=" + pushSite + "&entry=false&interfaceName=tokenizationAndAuthentication",
		type: "post",
		headers: {
			"X-APP-API_KEY": "apiKey",
			"X-APP-API_CHANNEL": "mobile",
			"Content-Type": "application/x-www-form-urlencoded"
		},
		dataType: "json",
		async: false,
		success: function (data) {
			console.log();
		}
	});


	var token_number = data.account_token;

	if (token_number != undefined && token_number != '') {
		$('#cardinalToken').val(data.account_token);
		$('#cBinNum').val($('#ccnumber').val().substring(0, 6));
		$('#cLength').val($('#ccnumber').val().length);
		commitCreditCard();
		return false;
	}

}

//Start : TokenizeAndAuthorize method
function tokenizeAndAuthorizeCallbackHandler(data) {
	var radailPaymentErrorCode;
	switch (data.ActionCode) {
		case "FAILURE":
			radailPaymentErrorCode = data.failure_code;
			break;
		case "ERROR":
			radailPaymentErrorCode = data.error_code;
			break;
	}

	if (enableIntegrationLog) {
		integrationLogCall('false', 'tokenizationAndAuthentication');
	}

	if (enableVerboseDebug) {
		console.log("tokenizeAndAuthorizeCallbackHandler() response data:" + JSON.stringify(data));
	}

	var token_number = data.account_token;

	if (token_number != undefined && token_number != '') {
		$('#cardinalToken').val(data.account_token);
		$('#cBinNum').val($('#ccnumber').val().substring(0, 6));
		$('#cLength').val($('#ccnumber').val().length);
		commitCreditCard();
		return false;
	}

	if (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '40005' || radailPaymentErrorCode == '50005' || radailPaymentErrorCode == '50006') {
		$('#cardinalToken').val(data.account_token);
		$('#cBinNum').val($('#ccnumber').val().substring(0, 6));
		$('#cLength').val($('#ccnumber').val().length);
		commitCreditCard();
		return false;
	}

	//getting nonce again and re_try token
	if (radailPaymentErrorCode != undefined && (radailPaymentErrorCode == '50002' || radailPaymentErrorCode == '50004') && tokenizeAndAuthorizeRetryAttempts < maxTokenizeAndAuthorizeRetryAttempts) {
		tokenizeAndAuthorizeRetryAttempts++;
		//Added  time out for defect=TSJ-6625
		setTimeout(function () { startTransaction(); }, 4000);
		return false;
	}

	//re_try token
	if (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '50001' && tokenizeAndAuthorizeRetryAttempts < maxTokenizeAndAuthorizeRetryAttempts) {
		tokenizeAndAuthorizeRetryAttempts++;
		//Added  time out for defect=TSJ-6625
		setTimeout(function () { startTransaction(); }, 4000);
		return false;
	}

	if (radailPaymentErrorCode != undefined && radailPaymentErrorCode == '40001' || radailPaymentErrorCode == '40002' || radailPaymentErrorCode == '40003' || radailPaymentErrorCode == '40004' || radailPaymentErrorCode == '40006' || radailPaymentErrorCode == '50001' || radailPaymentErrorCode == '50002' || radailPaymentErrorCode == '50003' || radailPaymentErrorCode == '50004') {
		tokenizeAndAuthorizeRetryAttempts = 0;
		var radailPaymentKey = errorKeyJson[radailPaymentErrorCode];
		var radailPaymentMsg = errorKeyJson[radailPaymentKey];
		$('.layaway-payment-error').eq(0).before('<span class="errorDisplay">' + radailPaymentMsg + '</span>');
		return false;
	}
}

function getLayawayShippingAddress() {
	var contextPath = $(".contextPath").val();
	var respAddress;
	$.ajax({
		url: contextPath + "checkout/intermediate/getShippingAddressAjaxSubmit.jsp",
		type: "POST",
		async: false,
		cache: false,
		dataType: 'json',
		success: function (data) {
			respAddress = data;
		},
		error: function (e) {
			console.log(e);
		}
	});
	return respAddress;
}
/*Radial OMS details for order history details and show order details and cancel orders.*/
var totalNumberOfRecords;
var loadMorePageNumber;
var totalRecordsFlag = true;
var totalNumberOfRecordsDB;
var loadOrderHistoryFirstindex = 0;
var loadOrderHistorylastindex;
var defaultnoOfrecords;
var email;
var orderHistory = {};
var displayRecordsInLayaway = false;
$(document).on("click", ".order-history-left-col .row-load-more .more,.layaway-orders #layawayLoadMore.more", function () {
	var $loadMore = $('#orderHistoryLoadMore');
	if ((totalNumberOfRecords >= -4) && ($loadMore.html() === 'load more') && $(this).attr('id') != "layawayLoadMore") {
		loadMorePageNumber = parseInt($("#pageNumber").val());
		loadOrderHistoryDetails(false);
	}
	else if ($(this).attr('id') == "layawayLoadMore") {
		loadMorePageNumber = parseInt($("#layawayPageNumber").val());
		loadOrderHistoryDetails(true);
	}
});

function loadorderHistoryOrders(layawayPage) {
	customerId = $("#customerId").val();
	email = $("#email").val();
	loadMorePageNumber = $("#pageNumber").val() ? $("#pageNumber").val() : $("#layawayPageNumber").val();
	noOfRecordsPerPage = $("#noOfRecordsPerPage").val();
	loadOrderHistorylastindex = parseInt(noOfRecordsPerPage);
	defaultnoOfrecords = parseInt(noOfRecordsPerPage);
	pushSite = $("#pushSiteId").val();
	var omsOrderListRestAPIURL = $("#omsOrderListRestAPIURL").val();
	$.ajax({
		url: omsOrderListRestAPIURL + "?customerId=" + customerId + "&pageNumber=" + loadMorePageNumber + "&email=" + email + "&noOfRecordsPerPage=" + noOfRecordsPerPage + "&layawayPage=" + layawayPage + "&pushSite=" + pushSite,
		type: "post",
		dataType: "json",
		headers: {
			"X-APP-API_KEY": "apiKey",
			"X-APP-API_CHANNEL": "mobile",
			"Content-Type": "application/x-www-form-urlencoded"
		},
		async: true,
		success: function (data) {

			orderHistory = data.customerOrder;

			loadOrderHistoryDetails(layawayPage);
		}
	});
}
function loadOrderHistoryDetails(layawayPage) {
	noOfRecordsPerPage = $("#noOfRecordsPerPage").val();
	loadMorePageNumber = $("#pageNumber").val() ? $("#pageNumber").val() : $("#layawayPageNumber").val();
	var loadMorePageNumbers = parseInt(loadMorePageNumber) + 1;
	if (layawayPage) {
		$("#layawayPageNumber").val(loadMorePageNumbers);
		$('#layaway-guest-account').addClass("active");
		if (typeof orderHistory.OrderList[0].OrderNumber != 'undefined' && orderHistory.OrderList[0].OrderNumber != '') {
			displayRecordsInLayaway = true;
			$('#orderHistory').addClass('active');
		}
		else {
			$('#orderHistory').removeClass('active');
		}
	}
	else {
		$("#pageNumber").val(loadMorePageNumbers);
	}
	if (layawayPage) {
		if (typeof orderHistory.OrderList[0].Message !== "undefined" && orderHistory.OrderList[0].Message != "") {
			var ordersPerPage = orderHistory;
		}
		else {
			var ordersPerPage = orderHistory.OrderList.slice(loadOrderHistoryFirstindex, loadOrderHistorylastindex);
		}
	}
	else {
		if (typeof orderHistory.customerTransactionListResponse != 'undefined' && orderHistory.customerTransactionListResponse != '') {
			var ordersPerPage = orderHistory;
		}
		else {
			var ordersPerPage = orderHistory.CustomerLookupResponse.OrderList.Order.slice(loadOrderHistoryFirstindex, loadOrderHistorylastindex);
		}
	}
	var ordersDisplayPerPage = { 'orders': ordersPerPage };
	loadOrderHistoryFirstindex = parseInt(loadOrderHistoryFirstindex) + parseInt(noOfRecordsPerPage);
	loadOrderHistorylastindex = parseInt(loadOrderHistoryFirstindex) + parseInt(noOfRecordsPerPage);
	ajaxCallForStaticText(ordersDisplayPerPage);
	if (!totalRecordsFlag) {
		if (layawayPage) {
			totalNumberOfLayawayOrders = orderHistory.OrderList.length;
			totalNumberOfRecordsDB = orderHistory.OrderList.length;
		}
		else {
			if (typeof orderHistory.customerTransactionListResponse != 'undefined' && orderHistory.customerTransactionListResponse != '') {
				$(".my-account-order-history").find(".order-history-table-header").hide();

			}
			else {
				totalNumberOfLayawayOrders = orderHistory.CustomerLookupResponse.OrderList.Order.length;
				totalNumberOfRecordsDB = orderHistory.CustomerLookupResponse.OrderList.Order.length;
			}
		}
		totalNumberOfRecords = totalNumberOfRecordsDB - 5;
		if (typeof totalNumberOfRecordsDB == 'undefined' || totalNumberOfRecordsDB == '' || totalNumberOfRecordsDB < 4) {
			$('.my-account-order-history .orderHistoryViewAll').hide();
		}
		if (totalNumberOfRecordsDB > 4) {
			if ((totalNumberOfRecordsDB > loadOrderHistoryFirstindex)) {
				$(".row.row-load-more").removeClass('hide');
				$("#orderHistoryLoadMore").html("load more").removeClass("less").addClass('more');
				if (layawayPage) {
					$("#layawayLoadMore").html("load more").removeClass("less").addClass('more');
				}
			}
			else{
				var $loadMore = $('#orderHistoryLoadMore');
				$("#orderHistoryLoadMore").html('load less').removeClass('more').addClass("less");
				if (layawayPage) {
					$("#layawayLoadMore").html("load less").removeClass("more").addClass('less');
				}
			}
		}
		/*else if ((totalNumberOfRecordsDB > 10)) {
			$(".row.row-load-more").removeClass('hide');
			$("#orderHistoryLoadMore").html("load more").removeClass("less").addClass('more');
			if (layawayPage) {
				$("#layawayLoadMore").html("load more").removeClass("less").addClass('more');
			}
		}*/
	}
}
function ajaxCallForStaticText(orderHistory) {
	$.ajax({
		url: '/myaccount/StaticText.jsp',
		type: "post",
		dataType: "json",
		async: false,
		success: function (data) {
			orderDetails = data;
			updateOrdersInOrderHistory(orderHistory, orderDetails);
		}
	});
}
function updateOrdersInOrderHistory(orderHistory, orderDetails) {
	var orderHistoryJson = $.extend(orderHistory, orderDetails);
	if ($(".order-history-table-header").length > 0) {
		$('#myaccount-order-history-template').tmpl(orderHistoryJson).appendTo('.order-summary-block');
		totalRecordsFlag = false;
	} else if ($(".order-history-table").length > 0) {
		$('#order-history-template').tmpl(orderHistoryJson).appendTo('.order-history-table');
		if (!totalRecordsFlag) {
			totalNumberOfRecords = totalNumberOfRecords - 5;
		}
		if ((totalNumberOfRecords <= 0)) {
			if (totalNumberOfRecordsDB > 5) {
				var $loadMore = $('#orderHistoryLoadMore');
				$loadMore.html('load less').removeClass('more').addClass("less");

			}
		}
		totalRecordsFlag = false;
	} else if ($(".layaway-order-history-table-header.row").length > 0) {
		var layawayOrderHistoryTemplate = $('#layaway-order-history-template').tmpl(orderHistoryJson);
		$('.my-account-order-history .layaway-order-history-table-header.row').after(layawayOrderHistoryTemplate);
		if (!totalRecordsFlag) {
			totalNumberOfRecords = totalNumberOfRecords - 5;
		}
		if ((totalNumberOfRecords <= 0)) {
			if (totalNumberOfRecordsDB > 5) {
				var $loadMore = $('#orderHistoryLoadMore');
				$("#layawayLoadMore").html('load less').removeClass('more').addClass("less");

			}
		}
		totalRecordsFlag = false;
	}
}
function cancelOrder(obj) {
	if (obj == false) {
		$("#orderHistoryDetailModal").modal("hide");
	}
	else {
		var orderId = $(obj).parents("#orderHistoryCancelOrder").find(".cancelOrderNumber").val() || $("#orderHistoryDetailModal").find(".hiddenOrderNumber").val();
		var omsCancelOrderRestAPIURL = $("#omsCancelOrderRestAPIURL").val();
		pushSite = $("#pushSiteId").val();
		$.ajax({
			url: omsCancelOrderRestAPIURL + "?orderId=" + orderId + "&pushSite=" + pushSite + "&email=" + email,
			type: "post",
			headers: {
				"X-APP-API_KEY": "apiKey",
				"X-APP-API_CHANNEL": "mobile",
				"Content-Type": "application/x-www-form-urlencoded"
			},
			dataType: "json",
			async: false,
			success: function (data) {
				customerOrderDetailsResponse = data.customerOrder;
				var responseStatus = customerOrderDetailsResponse.OrderCancelResponse.ResponseStatus;
				if (responseStatus == "Success") {
					$(document).find('.successMessage').remove();
					$('.orderHistoryCancelOrder').removeClass("hide");
					$('.orderHistoryCancelOrderError').addClass("hide");
				}
				if (responseStatus == "ServiceDown") {
					$(document).find('.successMessage').remove();
					$('.orderHistoryCancelOrder').addClass("hide");
					$('.orderHistoryCancelOrderError').removeClass("hide");
				}
				/*if($(".my-account-sign-in-template").length>0){
					return false;
				}	*/
				totalRecordsFlag = true;
				$("#pageNumber").val(1);
				//cancelOrderHistoryJson = data;
				if ($(".order-history-table-header").length > 0) {
					$('.order-summary-block').children('.order-history-order.row').remove();
					//$('#myaccount-order-history-template').tmpl(orderHistoryJson).appendTo('.order-summary-block');
				} else if ($(".order-history-table").length > 0) {
					//$('#order-history-template').tmpl(orderHistoryJson).appendTo('.order-history-table');
					$('.order-history-table').children('.order-history-order.row').remove();

				} else if ($(".layaway-order-history-table-header.row").length > 0) {
					//var layawayOrderHistoryTemplate = $('#layaway-order-history-template').tmpl(orderHistoryJson);
					//$('.my-account-order-history .layaway-order-history-table-header.row').after(layawayOrderHistoryTemplate);
					//$('.order-summary-block').html("");
				}
				loadOrderHistoryFirstindex = 0;
				loadOrderHistorylastindex = 0;
				loadorderHistoryOrders(false);
			}
		});
		$("#orderHistoryCancelOrder").modal("hide");
		$("#orderHistoryDetailModal").modal("hide");
		omniCancelOrder();

	}
}
$(document).on('click', '.row-load-more .less', function () {
	totalNumberOfRecords = totalNumberOfRecordsDB - 5;
	$("#pageNumber").val(2);
	$(".order-history-table .order-history-order.row").slice(5).remove();
	$("#orderHistoryLoadMore").html("load more").removeClass("less").addClass('more');
	noOfRecordsPerPage = $("#noOfRecordsPerPage").val();
	loadOrderHistoryFirstindex = parseInt(noOfRecordsPerPage);
	loadOrderHistorylastindex = loadOrderHistoryFirstindex + parseInt(noOfRecordsPerPage);
})
$(document).on('click', '#layawayLoadMore.less', function () {
	totalNumberOfRecords = totalNumberOfRecordsDB - 5;
	$("#layawayPageNumber").val(2);
	$(".layaway-orders .order-history-order.layaway-orders").slice(5).remove();
	$("#layawayLoadMore").html("load more").removeClass("less").addClass('more');
	noOfRecordsPerPage = $("#noOfRecordsPerPage").val();
	loadOrderHistoryFirstindex = parseInt(noOfRecordsPerPage);
	loadOrderHistorylastindex = loadOrderHistoryFirstindex + parseInt(noOfRecordsPerPage);
})
/*function start to display order details in layaway page*/
$("body").on("click", ".orderHistoryDetailModal,.order-information,.guest-order-submit,.order-status button", function () {
	$("#layawayGlobalErrorDisplay").hide();
	if ($(this).attr("id") == "orderStatusSignIn") {
		if (!$("#truOrderStatusSignInForm").valid()) {
			$(".errorDisplay").css('display', 'none');
			return false;
		}
	} else if ($(this).attr("id") == "layawayGuestOrderNumberCheckup") {
		if (!$("#layawayGuestOrderNumberCheckupForm").valid()) {
			return false;
		}
	}
	if ($(this).hasClass("enter-button guest-order-submit layaway-order-num")) {
		if ($("#guest-order-number").val() == "" || $("#guest-order-number").val() == null) {
			if ($(this).parent().find('.errorDisplay').length > 0) {
				$(this).parent().find('.errorDisplay').remove();
			}
			bindServerErrorValidation("#guest-order-number", "Oops! Some information is missing. You must fill in the required fields");
			return false;
		}
	}

	removeBackendErrorMessagesInline($("#layawayGuestOrderNumberCheckupForm"));

	guestFlag = $(this).hasClass("guest-order-submit");
	layawaypage = $(this).hasClass("layaway-order-num");
	$('.orderError').hide();
	$('.orderNumber,.zipCode').removeClass('error-highlight');
	if ($(this).parents(".sign-in-columns-container").length > 0) {
		var orderId = $(".orderNumber").val();
	} else if ($(this).parents(".my-account-order-history").length > 0) {
		var orderId = $(this).attr('title');
	} else if ($(this).parents(".order-history-table").length > 0) {
		var orderId = $(this).attr('title');
	} else if ($(this).parents(".guest-account-details").length > 0) {
		var orderId = $(".guest-order-number").val();
	}
	var zipCode = $(".zipCode").val();
	if ($(this).parents(".sign-in-columns-container").length > 0 && (orderId == "" || zipCode == "" || orderId == undefined || zipCode == undefined)) {
		$('.orderError').text("Oops! Some information is missing. You must fill in the required fields").show();
		if (orderId == "" || orderId == undefined) $('.orderNumber').addClass('error-highlight');
		if (zipCode == "" || zipCode == undefined) $('.zipCode').addClass('error-highlight');
		if ((orderId == "" || orderId == undefined) && (zipCode == "" || zipCode == undefined)) $('.orderNumber,.zipCode').addClass('error-highlight');
		return false;
	}

	orderId = (orderId).trim();

	if (zipCode != undefined) {
		zipCode = (zipCode).trim();
	}
	else {
		zipCode = "";
	}
	var omsOrderDetailRestAPIURL = $("#omsOrderDetailRestAPIURL").val();
	pushSite = $("#pushSiteId").val();
	$.ajax({
		url: '/myaccount/StaticText.jsp',
		type: "post",
		dataType: "json",
		/*async : false,*/
		success: function (data) {
			orderDetails = data;
		},
		error: function (xhr, ajaxOptions, thrownError) {
			if (xhr.status == '409') {
				window.event.cancelBubble = true;
				var redirectionUrl = xhr.getResponseHeader('Referer');
				$(document).find("[data-toggle=modal]").each(function () {
					$(this).attr("data-target", "");
				});
				//setTimeout(function(){
				if (redirectionUrl != '' && redirectionUrl != null && redirectionUrl != 'undefined' && !($('#VisitorFirstNamecookieDIV').html() == 'my account')) {
					if (redirectionUrl.indexOf('?') > -1) {
						location.href = redirectionUrl + '&sessionExpired=true';
					} else {
						location.href = redirectionUrl + '?sessionExpired=true';
					}
				} else {
					var contextPath = $(".contextPath").val();
					location.href = contextPath + 'myaccount/myAccount.jsp?sessionExpired=true';
				}
				//},300);
			}
			return false;
		}
	})
	$.ajax({
		url: omsOrderDetailRestAPIURL + "?orderId=" + orderId + "&layawayPage=" + layawaypage + "&postalCode=" + zipCode + "&pushSite=" + pushSite,
		type: "post",
		headers: {
			"X-APP-API_KEY": "apiKey",
			"X-APP-API_CHANNEL": "mobile",
			"Content-Type": "application/x-www-form-urlencoded"
		},
		dataType: "json",
		/*async : false,*/
		beforeSend: function () {
			$('#truOrderStatusSignInForm').find('.errorDisplay').remove();
		},
		success: function (data) {
			var loginStatus = $('#loginStatus').val();
			if (loginStatus == 'true') {
				$('#billingInfoApplicationForm input,#creditCardInfoApplicationForm input,#layawayPaymentAmountForm input').val('');
			} else {
				$('#billingInfoApplicationForm input:not(#orderEmail),#creditCardInfoApplicationForm input,#layawayPaymentAmountForm input').val('');
			}
			$('#state,#expirationMonth,#expirationYear').prop('selectedIndex', 0);
			$(".display-card,#make-payment-edit-button").hide();
			$('#layawayPaymentAmount').removeAttr('readonly', 'readonly');
			$('#make-payment-submit-button').show();
			if (typeof data == 'string') {
				var data1 = $.parseJSON(data);
			}
			else {
				var data1 = data;
			}

			if (layawaypage) {
				var response1 = data1.customerOrder;
			} else {
				var response1 = data1;
			}
			customerOrderDetailsResponse = $.extend(orderDetails, response1);
			if ($(".order-history-table-header").length > 0) {
				if (typeof customerOrderDetailsResponse.customerOrder.CustomerOrderDetailResponse != "undefined") {
					var errorMessage = customerOrderDetailsResponse.customerOrder.CustomerOrderDetailResponse.messages.message.description;
					$('.my-account-order-history .messageFromService').remove();
					$('.my-account-order-history .order-history-header').after('<label class="error messageFromService">' + errorMessage + '</label>');
					return false;
				}
				else {
					$('.my-account-order-history .messageFromService').remove();
					$('.accountOrderHistoryDetailModalPopup .order-history-detail-modal').remove();
					$('#myaccount-order-history-details').tmpl(customerOrderDetailsResponse).appendTo('.accountOrderHistoryDetailModalPopup');
					$("#orderHistoryDetailModal").modal("show");
					setTimeout(function () {
						$(document).find('.accountOrderHistoryDetailModalPopup .tse-scrollable').TrackpadScrollEmulator({
							wrapContent: false,
							autoHide: false
						});
						//$('.accountOrderHistoryDetailModalPopup').find('.tse-scrollable').find('.tse-order-history-content').height(580);
					}, 600);
				}

			} else if ($(".order-history-table").length > 0) {
				$('.messageFromService').remove();
				if (typeof customerOrderDetailsResponse.customerOrder.CustomerOrderDetailResponse != "undefined") {
					var errorMessage = customerOrderDetailsResponse.customerOrder.CustomerOrderDetailResponse.messages.message.description;
					$('.order-history-header').after('<label class="error messageFromService">' + errorMessage + '</label>');
					return false;
				}
				$('.orderHistoryDetailModalPopup .order-history-detail-modal').remove();
				$('#order-history-details').tmpl(customerOrderDetailsResponse).appendTo('.orderHistoryDetailModalPopup');
				$("#orderHistoryDetailModal").modal("show");
				setTimeout(function () {
					$(document).find('.orderHistoryDetailModalPopup .tse-scrollable').TrackpadScrollEmulator({
						wrapContent: false,
						autoHide: false
					});
				}, 600);
			} else if ($(".layaway-order-history-table-header.row").length > 0) {
				if (typeof customerOrderDetailsResponse.OrderDetail != 'undefined' && customerOrderDetailsResponse.OrderDetail != "" && typeof customerOrderDetailsResponse.OrderDetail.Message == 'undefined') {
					layawayOrderInformationDetails = $('#layaway-order-information-details').tmpl(customerOrderDetailsResponse);
					layawayPaymentInformationDetailsLabel = $('#layaway-payment-information-details-label').tmpl(customerOrderDetailsResponse);
					layawayPaymentInformationDetailsValues = $('#layaway-payment-information-details-values').tmpl(customerOrderDetailsResponse);
					layawayOrderProductDetails = $('#layaway-order-product-details').tmpl(customerOrderDetailsResponse);

					$("#layawayGuestDetails .order-info").html(layawayOrderInformationDetails);
					$("#layawayGuestDetails .layawayPaymentInformationDetailsLabel").html(layawayPaymentInformationDetailsLabel);
					$("#layawayGuestDetails .layawayPaymentInformationDetailsValue").html(layawayPaymentInformationDetailsValues);
					$("#guestMakePayment .layawayOrderProduct").html(layawayOrderProductDetails);
					try {
						$('#giftCardInfoApplicationForm').load(window.contextPath + 'myaccount/layaway/layawayGiftCard.jsp');
					} catch (e) { }
					$('.layaway-orders').removeClass('active');
					$('#guestMakePayment').addClass("active");
					var layawayOrderLineStatus = $('.layawayOrderLineStatus').html();
					if (layawayOrderLineStatus == "Active" || layawayOrderLineStatus == "active" || layawayOrderLineStatus == "ACTIVE") {
						$("#layawayPaymentAmountForm, .enterPaymentDetails").show();
						$('#payment-section').show();
						if (guestFlag) {
							$("#guestMakePayment .layawayButtonCS").hide();
							$(".layaway-order-product .layaway-buttons").hide();
						}
						else {
							$(".layaway-order-product .layaway-buttons").hide();
						}
					}
				} else {
					var layawayNoOrderFoundMessage = orderDetails.customerOrderDetails.layawayNoOrderFoundMessage;
					var layawayOrderNumber = $("#order-number").length > 0 ? "#order-number" : "#guest-order-number";
					if (layawayOrderNumber == "#guest-order-number" && $(layawayOrderNumber).parent().find('.errorDisplay').length > 0) {
						$(layawayOrderNumber).parent().find('.errorDisplay').remove();
					}
					//bindServerErrorValidation(layawayOrderNumber,layawayNoOrderFoundMessage);
					/*layawayGlobalErrorDisplayNoRecordFound(layawayOrderNumber,layawayNoOrderFoundMessage);
					$("#layawayGlobalErrorDisplay").show();*/
					$('.forNoOrderFoundMessage').remove();
					$('#layawayGuestOrderNumberCheckupForm').prepend('<label class="error forNoOrderFoundMessage">' + layawayNoOrderFoundMessage + '</label>')
					//$(".orderError").empty().append(noOrderFoundMessage).show();
				}
			} else if ($(".my-account-sign-in-col.order-status").length > 0) {
				var canShowModal = false;
				var giftCardpayInStoreErrorMessgae = customerOrderDetailsResponse.customerOrderDetails.giftCardpayInStoreErrorMessgae;
				if (typeof customerOrderDetailsResponse.customerOrder.OrderHeader != "undefined") {
					email = customerOrderDetailsResponse.customerOrder.OrderHeader.BillingAddress.EmailAddress;
					if ((customerOrderDetailsResponse.customerOrder.OrderHeader.BillingAddress.PostalCode.toString().indexOf(zipCode) == 0 && zipCode.length >= 5)) {
						canShowModal = true;
					}
				} else {
					var noOrderFoundMessage = customerOrderDetailsResponse.customerOrder.CustomerOrderDetailResponse.messages.message.description;
					var zipCodeError = customerOrderDetailsResponse.customerOrder.CustomerOrderDetailResponse.messages.message.zipCodeError;
					if ((typeof (customerOrderDetailsResponse.customerOrder.CustomerOrderDetailResponse.messages.message)).toLowerCase() == "object") {
						if (zipCodeError == "true") {
							var zipCodeNotMatchMessage = customerOrderDetailsResponse.customerOrderDetails.zipCodeNotMatchMessage;
							bindServerErrorValidation("#zip-code", zipCodeNotMatchMessage);
						} else {
							var noOrderFoundMessage = customerOrderDetailsResponse.customerOrderDetails.noOrderFoundMessage;
							bindServerErrorValidation("#order-number", noOrderFoundMessage);
						}
					}
					else if (customerOrderDetailsResponse.customerOrder.CustomerOrderDetailResponse.messages.message.length >= 0) {
						$.each(customerOrderDetailsResponse.customerOrder.messages.message, function (i, val) {
							if (i == 0) {
								//$(".orderError").empty().append(noOrderFoundMessage).show();
								bindServerErrorValidation("#zip-code", noOrderFoundMessage);
							}
						});
					}
				}
				if (canShowModal) {
					$('.accountOrderHistoryDetailModalPopup .order-history-detail-modal').remove();
					$('#myaccount-order-history-details').tmpl(customerOrderDetailsResponse).appendTo('.accountOrderHistoryDetailModalPopup');
					$("#orderHistoryDetailModal").modal("show");
					setTimeout(function () {
						$(document).find('.accountOrderHistoryDetailModalPopup .tse-scrollable').TrackpadScrollEmulator({
							wrapContent: false,
							autoHide: false
						});
					}, 600);
				}

			}
		},
		error: function (e) {
			console.log(e)
		}
	});
})
/*function end to display order details in layaway page*/
