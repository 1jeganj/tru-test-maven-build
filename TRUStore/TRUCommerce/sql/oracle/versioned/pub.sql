-- TABLE TRU_SITE_CONFIGURATION
DROP TABLE TRU_SITE_CONFIGURATION;

CREATE TABLE TRU_SITE_CONFIGURATION(
	ID VARCHAR2(40) NOT NULL,
	ASSET_VERSION    NUMBER(19)        NOT NULL,
	THRESH_PRICE_PER NUMBER(19,2) NULL,
	THRESH_PRICE_AMOUNT NUMBER(19,2) NULL,
	ENABLE_ADDRESS_DOCTOR  NUMBER(1,0),
	enable_cart_cookie 	NUMBER(1,0),
	PRIMARY KEY(ID,ASSET_VERSION)
);

ALTER TABLE tru_site_configuration RENAME COLUMN enable_cart_cookie TO enable_persist_order;

-- TRU_PROMOTION
DROP TABLE TRU_PROMOTION;

CREATE TABLE TRU_PROMOTION(
	PROMOTION_ID VARCHAR2(40) NOT NULL,
  ASSET_VERSION    NUMBER(19)        NOT NULL,
	PROMO_DETAILS CLOB,
	TENDER_AMOUNT_LIMIT NUMBER(19,2),
	TENDER_TYPE NUMBER(1,0),
	VENDOR_FUNDED NUMBER(1,0),
	TAG_PROMOTION NUMBER(1,0),
	CAMPAIGN_ID VARCHAR2(40),
	PRIMARY KEY(PROMOTION_ID,ASSET_VERSION)
);

alter table tru_site_configuration add(enable_certona number(1)	NULL, enable_triad number(1)	NULL, promocode_zone number(1)	NULL, enable_sucn number(1)	NULL);

-- Start - Added for Maximum coupon limit - 27th Nov 2015 

ALTER TABLE TRU_SITE_CONFIGURATION ADD MAX_COUPON_LIMIT INT DEFAULT(8);

-- End  - Added for Maximum coupon limit - 27th Nov 2015 

-- Start - Added for Enable master and visa icons - 30th Nov 2015 

ALTER TABLE TRU_SITE_CONFIGURATION ADD ENABLE_MASTER_AND_VISA_ICONS NUMBER(1) DEFAULT 1;

-- End  -  Added for Enable master and visa icons - 30th Nov 2015 
-- Start - Added for Enable Inventory - 14th Nov 2015 

ALTER TABLE TRU_SITE_CONFIGURATION ADD ENABLE_INVENTORY NUMBER(1) DEFAULT 1;

-- End  -  Added for Enable Inventory - 14th Nov 2015 
