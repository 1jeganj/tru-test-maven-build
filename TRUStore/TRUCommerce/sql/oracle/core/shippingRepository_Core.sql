--drop table TRU_ZIPCODE_RESTRICTIONS;
--drop table TRU_SHIPMETHOD_REGION_RATES;
--drop table TRU_SHIP_RATES;
--drop table TRU_REGIONS;
--drop table TRU_SHIP_METH_NOT_ALLOWED;
--drop table TRU_SHIP_METH_ALLOWED;
--drop table TRU_SHIP_METHODS;
--drop table TRU_FREIGHT_SHIP_METHODS;
--drop table TRU_FREIGHT_CLASS;

CREATE TABLE TRU_ZIPCODE_RESTRICTIONS 
(
 			 SKU_ID VARCHAR2(20 BYTE) NOT NULL ,
 			 ZIP_CODE VARCHAR2(20 BYTE) NOT NULL , 
 			 ERROR_MESSAGE VARCHAR2(500 BYTE) , 
 			 CONSTRAINT TRU_ZIPCODE_RESTRICTIONS_PK PRIMARY KEY 
 			 (SKU_ID , 
 			 ZIP_CODE )
);


CREATE TABLE TRU_FREIGHT_CLASS (
                FREIGHT_CLASS_ID           varchar2(254)      NOT NULL,
                FREIGHT_CLASS_NAME    INTEGER               NULL,
                PRIMARY KEY(FREIGHT_CLASS_ID)
);
CREATE TABLE TRU_SHIP_METHODS (
                SHIP_METH_ID                   varchar2(254)      NOT NULL,
                SHIP_METH_CODE                            varchar2(254)      NOT NULL,
                SHIP_METH_NAME                           varchar2(254)      NOT NULL,
                SHIP_METH_DETAILS       varchar2(1000 BYTE)      NULL,
                SHIP_FULFILLMENT_MSG                varchar2(254)      NULL,
                IS_ACTIVE                             INTEGER               NULL,
                PRIMARY KEY(SHIP_METH_ID)
);
CREATE TABLE TRU_FREIGHT_SHIP_METHODS (
                FREIGHT_CLASS_ID           varchar2(254)      NOT NULL REFERENCES TRU_FREIGHT_CLASS(FREIGHT_CLASS_ID),
                shipping_method                varchar2(254)      NOT NULL REFERENCES TRU_SHIP_METHODS(SHIP_METH_ID),
                PRIMARY KEY(FREIGHT_CLASS_ID, shipping_method)
);

CREATE TABLE TRU_SHIP_METH_ALLOWED (
                SHIP_METH_ID                   varchar2(254)      NOT NULL REFERENCES TRU_SHIP_METHODS(SHIP_METH_ID),
                STATE_CODE                       varchar2(254)      NOT NULL,
                PRIMARY KEY(SHIP_METH_ID, STATE_CODE)
);

CREATE TABLE TRU_SHIP_METH_NOT_ALLOWED (
                SHIP_METH_ID                   varchar2(254)      NOT NULL REFERENCES TRU_SHIP_METHODS(SHIP_METH_ID),
                STATE_CODE                       varchar2(254)      NOT NULL,
                PRIMARY KEY(SHIP_METH_ID, STATE_CODE)
);

CREATE TABLE TRU_REGIONS (
                SHIP_REGION_ID                               varchar2(254)      NOT NULL,
                REGION_CODE                    varchar2(254)      NOT NULL,
                REGION_NAME                  varchar2(254)      NOT NULL,
                PRIMARY KEY(SHIP_REGION_ID)
);

CREATE TABLE TRU_SHIP_RATES (
                SHIP_RATE_ID                    varchar2(254)      NOT NULL,
                SHIP_RATE_NAME                             varchar2(254)      NULL,
                LOWER_ORDER_VALUE   number(28, 20)   NULL,
                UPPER_ORDER_VALUE    number(28, 20)   NULL,
                DESCRIPTION                      varchar2(254)      NULL,
                SHIP_PRICE                          number(28, 20)   NULL,
                PRIMARY KEY(SHIP_RATE_ID)
);

CREATE TABLE TRU_SHIPMETHOD_REGION_RATES (
                SHIP_REGION_ID                               varchar2(254)      NOT NULL,
                SHIP_METH_ID                   varchar2(254)      NOT NULL,
                SHIP_RATE_ID                    varchar2(254)      NOT NULL,
                PRIMARY KEY(SHIP_REGION_ID, SHIP_METH_ID, SHIP_RATE_ID)
);
