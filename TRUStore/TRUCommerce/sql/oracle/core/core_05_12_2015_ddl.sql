-- drop table tru_channel_ispu_sg;
-- drop table tru_channel_hardgood_sg;

CREATE TABLE tru_channel_ispu_sg (
	shipping_group_id 	varchar2(254)	NOT NULL REFERENCES dcspp_ship_group(shipping_group_id),
	channel_type 		varchar2(254)	NULL,
	channel_name 		varchar2(254)	NULL,
	channel_id 		varchar2(254)	NULL,
	channel_user_name 	varchar2(254)	NULL,
	channel_co_user_name 	varchar2(254)	NULL,
	PRIMARY KEY(shipping_group_id)
);

CREATE INDEX tru_channel_ispu_sg_shipping_group_idx ON tru_channel_ispu_sg(shipping_group_id);


CREATE TABLE tru_channel_hardgood_sg (
	shipping_group_id 	varchar2(254)	NOT NULL REFERENCES dcspp_ship_group(shipping_group_id),
	channel_type 		varchar2(254)	NULL,
	channel_name 		varchar2(254)	NULL,
	channel_id 		varchar2(254)	NULL,
	channel_user_name 	varchar2(254)	NULL,
	channel_co_user_name 	varchar2(254)	NULL,
	PRIMARY KEY(shipping_group_id)
);

CREATE INDEX tru_channel_hardgood_sg_shipping_group_idx ON tru_channel_hardgood_sg(shipping_group_id);