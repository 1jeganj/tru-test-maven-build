
-- TABLE TRU_SITE_CONFIGURATION
DROP TABLE TRU_SITE_CONFIGURATION;


CREATE TABLE tru_site_configuration (
	id 			varchar2(254)	NOT NULL REFERENCES site_configuration(id),
	thresh_price_per 	number(28, 20)	NULL,
	thresh_price_amount 	number(28, 20)	NULL,
	enable_address_doctor 	number(1)	NULL,
	enable_cart_cookie 	number(1)	NULL,
	CHECK (enable_address_doctor IN (0, 1)),
	CHECK (enable_cart_cookie IN (0, 1)),
	CONSTRAINT TRU_SITE_CONFIGURATION_P PRIMARY KEY(ID),
	CONSTRAINT TRU_SITE_CONFIGURATION_D_F FOREIGN KEY (ID) REFERENCES SITE_CONFIGURATION(ID)
);

ALTER TABLE tru_site_configuration RENAME COLUMN enable_cart_cookie TO enable_persist_order;

-- TRU_PROMOTION

CREATE TABLE TRU_PROMOTION(
	PROMOTION_ID VARCHAR2(40) NOT NULL,
	PROMO_DETAILS CLOB,
	TENDER_AMOUNT_LIMIT NUMBER(19,2),
	TENDER_TYPE NUMBER(1,0),
	VENDOR_FUNDED NUMBER(1,0),
	TAG_PROMOTION NUMBER(1,0),
	CAMPAIGN_ID VARCHAR2(40),
	CONSTRAINT TRU_PROMOTION_P PRIMARY KEY(PROMOTION_ID),
	CONSTRAINT TRU_PROMOTION_D_F FOREIGN KEY (PROMOTION_ID) REFERENCES DCS_PROMOTION(PROMOTION_ID)
);


alter table tru_site_configuration add(enable_certona number(1)	NULL, enable_triad number(1)	NULL, promocode_zone number(1)	NULL, enable_sucn number(1)	NULL);

-- Start - Added for Maximum coupon limit - 27th Nov 2015 

ALTER TABLE TRU_SITE_CONFIGURATION ADD MAX_COUPON_LIMIT INT DEFAULT(8);

-- End  - Added for Maximum coupon limit - 27th Nov 2015 

-- Start - Added for Enable master and visa icons - 30th Nov 2015 

ALTER TABLE TRU_SITE_CONFIGURATION ADD ENABLE_MASTER_AND_VISA_ICONS NUMBER(1) DEFAULT 1;

-- End  -  Added for Enable master and visa icons - 30th Nov 2015 

-- Start - Added for Enable Inventory - 14th Nov 2015 

ALTER TABLE TRU_SITE_CONFIGURATION ADD ENABLE_INVENTORY NUMBER(1) DEFAULT 1;

-- End  -  Added for Enable Inventory - 14th Nov 2015 
