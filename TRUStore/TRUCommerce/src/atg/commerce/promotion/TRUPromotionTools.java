
package atg.commerce.promotion;


import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Set;

import javax.sql.DataSource;
import javax.xml.ws.ProtocolException;

import atg.commerce.catalog.CatalogTools;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.PricingModelComparator;
import atg.commerce.pricing.ShippingPriceInfo;
import atg.commerce.util.NoLockNameException;
import atg.commerce.util.TransactionLockService;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.core.util.StringUtils;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.service.dynamo.LangLicense;
import atg.service.lockmanager.DeadlockException;
import atg.userprofiling.Profile;
import atg.userprofiling.email.TemplateEmailException;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.catalog.vo.ProductPromoInfoVO;
import com.tru.commerce.catalog.vo.SKUInfoVO;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.pricing.TRUPriceProperties;
import com.tru.commerce.pricing.TRUPricingModelProperties;
import com.tru.commerce.promotion.email.TRUPromotionEmailConfiguration;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRULockManager;
import com.tru.common.vo.TRUPromoContainer;
import com.tru.common.vo.TRUPromoVO;
import com.tru.common.vo.TRUPromotionInfoVO;
import com.tru.messaging.TRUBudgetPromoDisableMessageSource;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This class is to perform the promotion related business logic as mentioned below.
 * 
 * 1) To have the tender type promotion business logic. 2) Calling rest service to disable the promotion if the total tender limit
 * reaches. 3) Getting all shipping promotions.
 * 
 * @author PA
 * @version 1.0
 * 
 */
public class TRUPromotionTools extends PromotionTools {
	
	private static ResourceBundle sResourceBundle = LayeredResourceBundle.getBundle(TRUConstants.PROMOTION_RESOURCES, LangLicense.getLicensedDefault());
	
	/** This holds the reference for FAILED_HTTP_ERROR_CODE *. */
	public static final String FAILED_HTTP_ERROR_CODE = "Failed : HTTP error code : ";

	/** This holds the reference for TRUPriceProperties *. */
	private TRUPriceProperties mPriceProperties;

	/** This holds the reference for mTenderTypeRepository *. */
	private Repository mTenderTypeRepository;

	/** Holds reference for PromotionImportExportTools. */
	private PromotionImportExportTools mPromotionImportExportTools;

	/** This holds the reference for FreeShippingPromoTemplate *. */
	private String mFreeShippingPromoTemplate;

	/** This holds the reference for tru configuration. */
	private TRUConfiguration mConfiguration;

	/** This holds the reference for tru configuration. */
	private TRULockManager mLockManager;
	
	/** Property to hold mEmailConfiguration. */
	private TRUPromotionEmailConfiguration mEmailConfiguration;
	
	/** Property to hold mTemplateEmailSender. */
	private TemplateEmailSender mTemplateEmailSender;
	
	/** Property to hold mSetPromotionId. */
	private boolean mSetPromotionId;
	
	/**
	 * property to hold mMultiplePromoQuery
	 */
	private String mMultiplePromoQuery;
	
	/**
	 * property to hold mSubQuery
	 */
	private String mSubQuery;
	
	/**
	 * property to hold mSelectCondition
	 */
	private String mSelectCondition;
	
	/**
	 * property to hold dataSource
	 */
	private DataSource mDataSource;
	
	/**
	 * property to hold dataSource
	 */
	private String mTenderTypePromotionItemType;
	
	/** Holds reference for BudgetEmailMessageSource. 
 	 * 
 	 */
	private TRUBudgetPromoDisableMessageSource mBudgetPromoDisableMessageSource;
	/**
	 * property to hold mPromotionQuery
	 */
	private String mPromotionQuery;
	
	/**
	 * property to hold mNoOfItemsQuery
	 */
	private String mNoOfItemsQuery;
		
	/**
	 * This method is used to query the promotion repository to get all the promotion 
	 * which are enable and budget limit is true.
	 */
	public void sendBudgetPromoListInEmail() {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.sendBudgetPromoListInEmail() method");
		}
		RepositoryView PromotionView = null;
		QueryBuilder queryBuilder = null;
		QueryExpression promotionDisableConstant = null;
		QueryExpression promotionDisableProperty = null;
		RepositoryItem[] promotionItems = null;
		Date timeAsDate = null;
		final MutableRepository promotions = (MutableRepository) getPromotions();
		if (promotions != null) {
			timeAsDate = getCurrentDate().getTimeAsDate();
			final TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPricingModelProperties();
			try {
				PromotionView = promotions.getView(pricingModelProperties.getPromotionItemDescName());
				queryBuilder = PromotionView.getQueryBuilder();
				promotionDisableConstant = queryBuilder.createConstantQueryExpression(Boolean.TRUE);
				promotionDisableProperty = queryBuilder.createPropertyQueryExpression(pricingModelProperties.getEnabledPropertyName());
				Query tenderTypeQueryEnable = queryBuilder.createComparisonQuery(promotionDisableProperty, promotionDisableConstant,
						QueryBuilder.EQUALS);
				promotionDisableConstant = queryBuilder.createConstantQueryExpression(Boolean.TRUE);
				promotionDisableProperty = queryBuilder.createPropertyQueryExpression(pricingModelProperties.getTenderTypePropertyName());
				Query budgetTypeQuery = queryBuilder.createComparisonQuery(promotionDisableProperty, promotionDisableConstant,
						QueryBuilder.EQUALS);
				promotionDisableConstant = queryBuilder.createConstantQueryExpression(Boolean.TRUE);
				promotionDisableProperty = queryBuilder.createPropertyQueryExpression(pricingModelProperties.getTenderTypePromotionPropertyName());
				Query tenderTypeQuery = queryBuilder.createComparisonQuery(promotionDisableProperty, promotionDisableConstant,
						QueryBuilder.EQUALS);
				promotionDisableConstant = queryBuilder.createConstantQueryExpression(timeAsDate);
                promotionDisableProperty = queryBuilder.createPropertyQueryExpression(pricingModelProperties.getEndUsable());
                Query endUsableQuery = queryBuilder.createComparisonQuery(promotionDisableProperty, promotionDisableConstant,
                               QueryBuilder.GREATER_THAN_OR_EQUALS);
                Query isNullQueryResult = queryBuilder.createIsNullQuery(promotionDisableProperty);
 				Query[] endUsable = { endUsableQuery, isNullQueryResult};
                Query orQueryResult = queryBuilder.createOrQuery(endUsable);
				Query[] pieces = { tenderTypeQueryEnable, budgetTypeQuery ,tenderTypeQuery,orQueryResult};
				Query andQueryResult = queryBuilder.createAndQuery(pieces);
				promotionItems = PromotionView.executeQuery(andQueryResult);
				updateBudgetItemListToSendEmail(promotionItems);
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					vlogError("RepositoryException: While processing promotions with exception : {0} ", e);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUPromotionTools.sendBudgetPromoListInEmail() method");
		}
	}


	/**
	 * Update tender item to send email.
	 *
	 * @param pPromotionItems the promotion items
	 */
	private void updateBudgetItemListToSendEmail(RepositoryItem[] pPromotionItems) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.updateBudgetItemListToSendEmail() method");
		}
		String promoId = null;
		MutableRepositoryItem tenderTypeItem = null;
		List<RepositoryItem> newTenderItemList = new ArrayList<RepositoryItem>();
		List<RepositoryItem> tenderNotUsePromoList = new ArrayList<RepositoryItem>();
		if (pPromotionItems != null) {
			for (RepositoryItem promoItem : pPromotionItems) {
				promoId = promoItem.getRepositoryId();
				RepositoryItem[] tenderTypeItemRepo = getTenderTypeItem(promoId);
				if (tenderTypeItemRepo != null && tenderTypeItemRepo.length > TRUConstants.ZERO) {
					tenderTypeItem = (MutableRepositoryItem) tenderTypeItemRepo[TRUConstants.ZERO];
					newTenderItemList.add(tenderTypeItem);
				} else {
					tenderNotUsePromoList.add(promoItem);
				}
			}
			sendTenderPromotionDetailsAsEmail(newTenderItemList,tenderNotUsePromoList,Boolean.FALSE);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUPromotionTools.updateBudgetItemListToSendEmail() method");
		}
	}


	/**
	 * This method is used to iterate the tender type promotion adjustments and pass each adjustment and new array list to update.
	 * the promotion list based on business logic. If promotion list is not empty call restDisablePromoiotn method by passing the
	 * promotion list to disable the promotion.
	 * 
	 * @param pTenderTypePromotionsAdjustment
	 *            - the tender type promotions
	 */
	public void createOrUpdateTenderTypePromo(List<PricingAdjustment> pTenderTypePromotionsAdjustment) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.createOrUpdateTenderTypePromo() method");
		}
		final List<RepositoryItem> disablePromotionListItem = new ArrayList<RepositoryItem>();
		final List<String> disablePromotionList = new ArrayList<String>();
		String promotionId = null;
		if (pTenderTypePromotionsAdjustment != null) {
			RepositoryItem promotion = null;
			List<PricingAdjustment> pricingAdjustments = null;
			final Map<RepositoryItem, List<PricingAdjustment>> tenderAdjustmentMap = new HashMap<RepositoryItem, List<PricingAdjustment>>();
			for (PricingAdjustment pricingAdjustment : pTenderTypePromotionsAdjustment) {
				if (tenderAdjustmentMap != null && !tenderAdjustmentMap.isEmpty() && tenderAdjustmentMap.containsKey(promotion)) {
					pricingAdjustments = tenderAdjustmentMap.get(promotion);
					pricingAdjustments.add(pricingAdjustment);
					tenderAdjustmentMap.put(promotion, pricingAdjustments);
				} else {
					pricingAdjustments = new ArrayList<PricingAdjustment>();
					promotion = pricingAdjustment.getPricingModel();
					pricingAdjustments.add(pricingAdjustment);
					tenderAdjustmentMap.put(promotion, pricingAdjustments);
				}
			}
			// Update the tender item repository with tender type adjustments available in the order
			updateTenderTypePromotionItem(tenderAdjustmentMap, disablePromotionListItem);
			// Added promotionIds to list to disable the promotion
			if (disablePromotionListItem != null && !disablePromotionListItem.isEmpty()) {
				for (RepositoryItem tenderTypeItem : disablePromotionListItem) {
					if (tenderTypeItem != null) {
						promotionId = (String) tenderTypeItem.getPropertyValue(getPriceProperties().getPromotionIdPropertyName());
						if (!StringUtils.isEmpty(promotionId)) {
							disablePromotionList.add(promotionId);
						}
					}
				}
			}
			if (disablePromotionList != null && !disablePromotionList.isEmpty()) {
				if (isLoggingDebug()) {
					vlogDebug("Promotion list which are going to disable based on budget limit", disablePromotionList);
				}
				try {
					// posting message to qux instance to perform tender based calculation and if reaches threshold creating the BCC project with rest call
					getBudgetPromoDisableMessageSource().sendBudgetDisablePromoMessage(disablePromotionList);
				} catch (ProtocolException pExc) {
					if (isLoggingError()) {
						vlogError(pExc, "Protocol Exception in method TRUPromotionTools.createOrUpdateTenderTypePromo");
					}
				}
			} else {
				if (isLoggingDebug()) {
					logDebug("No tender type promotions to disable");
				}
			}
		} else {
			if (isLoggingDebug()) {
				logDebug("Tender type promoiton adjustments are empty");
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUPromotionTools.createOrUpdateTenderTypePromo() method");
		}
	}
	

	/**
	 * Send tender promotion details as email.
	 *
	 * @param pTenderItems the tender items
	 * @param pTenderNotUsePromoList 
	 * @param pFlagToHandleMessage 
	 */
	public void sendTenderPromotionDetailsAsEmail(List<RepositoryItem> pTenderItems, List<RepositoryItem> pTenderNotUsePromoList, boolean pFlagToHandleMessage) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.sendTenderPromotionDetailsAsEmail() method");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		if (pFlagToHandleMessage) {
			templateParameters.put(TRUConstants.MESSAGE_SUBJECT, getEmailConfiguration().getMessageSubject());
		} else {
			templateParameters.put(TRUConstants.MESSAGE_SUBJECT, getEmailConfiguration().getHourlyMessageSubject());
		}
		templateParameters.put(TRUConstants.TENDER_LIST, pTenderItems);
		templateParameters.put(TRUConstants.PROMOTION_LIST, pTenderNotUsePromoList);
		sendPromotionTenderDetailsSuccessEmail(templateParameters);
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUPromotionTools.sendTenderPromotionDetailsAsEmail() method");
		}
	}
  
	/**
	 * Send promotion tender details success email.
	 *
	 * @param pParams the params
	 */
	@SuppressWarnings("unchecked")
	private void sendPromotionTenderDetailsSuccessEmail(Map<String, Object> pParams) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.sendPromotionTenderDetailsSuccessEmail() method");
		}
		try {
			
			TemplateEmailInfoImpl emailInfo = getEmailConfiguration().getSuccessTemplateEmailInfo();
			if (emailInfo == null) {
				vlogError("emailInfo is NULL or EMPTY. Cannot Send email.");
				return;
			}
			clearEmailInfo(emailInfo);
			// Checking the emailInfo Parameters
			if (emailInfo.getTemplateParameters() != null) {
				emailInfo.getTemplateParameters().putAll(pParams);
			} else {
				emailInfo.setTemplateParameters(pParams);
			}
			emailInfo.setMessageSubject((String) pParams.get(TRUConstants.MESSAGE_SUBJECT));
			getTemplateEmailSender().sendEmailMessage(emailInfo, getEmailConfiguration().getRecipients());
		} catch (TemplateEmailException e) {
			vlogError("Exception occured while sending a success email : ", e);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUPromotionTools.sendPromotionTenderDetailsSuccessEmail() method");
		}
	}
	
	
	/**
	 * Clear email info.
	 *
	 * @param pEmailInfo the email info
	 */
	private void clearEmailInfo(TemplateEmailInfoImpl pEmailInfo) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.clearEmailInfo() method");
		}
		pEmailInfo.setTemplateParameters(null);
		pEmailInfo.setMessageAttachments(null);
		pEmailInfo.setMessageSubject(null);
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUPromotionTools.clearEmailInfo() method");
		}
	}
	
	

	/**
	 * This method is used to call disablePromotion method which is available in March module through rest URL by passing.
	 * promotion id which need to disable.
	 * 
	 * @param pDisablePromotionList
	 *            the disable promotion list
	 * @throws ProtocolException
	 *             - if any
	 */
	public void restDisablePromotion(List<String> pDisablePromotionList) throws ProtocolException {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.restDisablePromotion() method");
		}
		final StringBuffer promoIds = appendPipeToPromoList(pDisablePromotionList);
		try {
			if (getConfiguration() != null && !StringUtils.isEmpty(getConfiguration().getDisableRestServiceURL()) &&
					 promoIds != null) {
				if (isLoggingDebug()) {
					logDebug("Rest Configuration URL" + getConfiguration().getDisableRestServiceURL());
				}
				final URL url = new URL(getConfiguration().getDisableRestServiceURL() + promoIds.toString());
				final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod(TRUConstants.POST);
				conn.setRequestProperty(TRUConstants.CONTENT_TYPE, TRUConstants.APPLICATION_JSON);
				final OutputStream os = conn.getOutputStream();
				os.flush();
				if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
					throw new ProtocolException(FAILED_HTTP_ERROR_CODE + conn.getResponseCode());
				}
				conn.disconnect();
			}
		} catch (MalformedURLException e) {
			if (isLoggingError()) {
				vlogError("MalformedURLException: While processing MalformedURLException with exception : {0} ", e);
			}
		} catch (IOException e) {
			if (isLoggingError()) {
				vlogError("IOException: While processing IOException with exception : {0} ", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUPromotionTools.restDisablePromotion() method");
		}
	}

	/**
	 * This method is used to append the pipe for every promotion id.
	 * 
	 * @param pDisablePromotionList
	 *            the disable promotion list
	 * @return the string buffer
	 */
	private StringBuffer appendPipeToPromoList(List<String> pDisablePromotionList) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.appendPipeToPromoList() method");
		}
		final StringBuffer promoListIds = new StringBuffer();
		if (pDisablePromotionList != null && !pDisablePromotionList.isEmpty()) {
			for (String promotionId : pDisablePromotionList) {
				promoListIds.append(promotionId);
				promoListIds.append(TRUConstants.STR);
			}
			promoListIds.deleteCharAt(promoListIds.lastIndexOf(TRUConstants.STR));
			if (isLoggingDebug()) {
				logDebug("Promotion Id list" + promoListIds);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUPromotionTools.appendPipeToPromoList() method");
		}
		return promoListIds;
	}

	/**
	 * This method is used to create the budget type promotion and update the. properties which are available in promotion item.
	 * If already item is available it will update the item based on promotion item
	 * 
	 * @param pTenderAdjustmentMap
	 *            - the Pricing Adjustments Map
	 * @param pDisablePromotionList
	 *            the disable promotion list
	 * 
	 */
	private void updateTenderTypePromotionItem(Map<RepositoryItem, List<PricingAdjustment>> pTenderAdjustmentMap,
			List<RepositoryItem> pDisablePromotionList) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.getTenderTypePromotionItem() method");
		}
		RepositoryItem pricingModel = null;
		RepositoryItem[] tenderTypeQueryItems = null;
		double discountsValue = TRUConstants.DOUBLE_ZERO;
		double totalDiscountAmountTillDate = TRUConstants.DOUBLE_ZERO;
		double capitalAmount = TRUConstants.DOUBLE_ZERO;
		double updatedDiscountValue = TRUConstants.DOUBLE_ZERO;
		MutableRepositoryItem tenderTypeItem = null;
		String adjustmentDescription = null;
		TransactionLockService lockService = null;
		if (pTenderAdjustmentMap != null && !pTenderAdjustmentMap.isEmpty()) {
			for (Entry<RepositoryItem, List<PricingAdjustment>> entry : pTenderAdjustmentMap.entrySet()) {
				pricingModel = entry.getKey();
				List<PricingAdjustment> adjustments = entry.getValue();
				for (PricingAdjustment pricingAdjustment : adjustments) {
					discountsValue += -pricingAdjustment.getTotalAdjustment();
					adjustmentDescription = pricingAdjustment.getAdjustmentDescription();
				}
				if (isLoggingDebug()) {
					vlogDebug("Total Discount value for promtion : {0} is : {1}", pricingModel, discountsValue);
				}
				// Get the tender type item for update the values
				try {
					if (pricingModel != null) {
						tenderTypeQueryItems = getTenderTypeItem(pricingModel.getRepositoryId());
						if (tenderTypeQueryItems != null && tenderTypeQueryItems.length > 0) {
							lockService = getLockManager().acquireLock();
							tenderTypeItem = (MutableRepositoryItem) tenderTypeQueryItems[0];
							// update total amount of tender type if capital amount of
							// promotion and tender type amount is mismatch
							updateTotalAmountIfMisMatch(tenderTypeItem, pricingModel);
							if (tenderTypeItem != null) {
								totalDiscountAmountTillDate = (double) tenderTypeItem.getPropertyValue(getPriceProperties()
										.getTotalDiscountAmountTillDatePropertyName());
								capitalAmount = (double) tenderTypeItem.getPropertyValue(getPriceProperties()
										.getCapitalAmountPropertyName());
								if (totalDiscountAmountTillDate < capitalAmount) {
									updatedDiscountValue = totalDiscountAmountTillDate + discountsValue;
									if (updatedDiscountValue < capitalAmount) {
										tenderTypeItem.setPropertyValue(getPriceProperties()
												.getTotalDiscountAmountTillDatePropertyName(), updatedDiscountValue);
									} else if (updatedDiscountValue == capitalAmount) {
										tenderTypeItem.setPropertyValue(getPriceProperties()
												.getTotalDiscountAmountTillDatePropertyName(), updatedDiscountValue);
										pDisablePromotionList.add(tenderTypeItem);
										if (isLoggingDebug()) {
											vlogDebug(
													"Added the promotion : {0} to the disable list when updatedDiscount value is equal to capital value",
													pricingModel.getRepositoryId());
										}
									} else {
										tenderTypeItem.setPropertyValue(getPriceProperties()
												.getTotalDiscountAmountTillDatePropertyName(), updatedDiscountValue);
										pDisablePromotionList.add(tenderTypeItem);
										if (isLoggingDebug()) {
											vlogDebug(
													"Added the promotion : {0} to the disable list when updatedDiscount value is greater than capital value",
													pricingModel.getRepositoryId());
										}
									}
								}
							}
						} else {
							// create new tender type item and update the properties
							final MutableRepository tenderTypeRepository = (MutableRepository) getTenderTypeRepository();
							double tenderAmountLimit = (double) pricingModel.getPropertyValue(getPriceProperties().getTenderAmountLimitPropertyName());
							tenderTypeItem = (MutableRepositoryItem) tenderTypeRepository.createItem(getPriceProperties()
									.getTenderTypePromotionItemType());
							tenderTypeItem.setPropertyValue(getPriceProperties().getPromotionIdPropertyName(),
									pricingModel.getRepositoryId());
							tenderTypeItem.setPropertyValue(getPriceProperties().getPromotionTypePropertyName(),
									adjustmentDescription);
							tenderTypeItem.setPropertyValue(getPriceProperties().getCapitalAmountPropertyName(),
									tenderAmountLimit);
							tenderTypeItem.setPropertyValue(getPriceProperties().getTotalDiscountAmountTillDatePropertyName(),
									discountsValue);
							if (pricingModel.getPropertyValue(getPriceProperties().getDescription()) != null) {
								tenderTypeItem.setPropertyValue(getPriceProperties().getPromotionDescription(),
										pricingModel.getPropertyValue(getPriceProperties().getDescription()));
							} else {
								tenderTypeItem.setPropertyValue(getPriceProperties().getPromotionDescription(),
										pricingModel.getPropertyValue(getPriceProperties().getDisplayNamePropertyName()));
							}
								
							tenderTypeRepository.addItem(tenderTypeItem);
							if (discountsValue > tenderAmountLimit) {
								pDisablePromotionList.add(tenderTypeItem);
							}
							if (isLoggingDebug()) {
								vlogDebug("New Budget item is created with", tenderTypeItem.getRepositoryId());
							}
						}
					}
				} catch (RepositoryException e) {
					if (isLoggingError()) {
						vlogError("RepositoryException: While processing promotions with exception : {0} ", e);
					}
				} catch (NoLockNameException e) {
					if (isLoggingError()) {
						vlogError("NoLockNameException encountered for checkInventoryStatus.service():{0}", e);
					}
				} catch (DeadlockException e) {
					if (isLoggingError()) {
						vlogError("DeadlockException encountered for checkInventoryStatus.service():{0}", e);
					}
				} finally {
					if (lockService != null) {
						getLockManager().releaseLock(lockService);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUPromotionTools.getTenderTypePromotionItem() method");
		}
	}

	/**
	 * This method is used to update the the total amount if.
	 * 
	 * @param pTenderTypeQueryItems
	 *            the tender type query items
	 * @param pPricingModel
	 *            the pricing model
	 */
	private void updateTotalAmountIfMisMatch(MutableRepositoryItem pTenderTypeQueryItems, RepositoryItem pPricingModel) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.updateToatlAmountIfMisMatch() method");
		}
		double totalDiscountAmountTillDate = TRUConstants.DOUBLE_ZERO;
		double capitalAmount = TRUConstants.DOUBLE_ZERO;
		if (pPricingModel != null && pTenderTypeQueryItems != null && pPricingModel.getPropertyValue(getPriceProperties().getTenderAmountLimitPropertyName()) != null) {
			totalDiscountAmountTillDate = (double) pPricingModel.getPropertyValue(getPriceProperties()
					.getTenderAmountLimitPropertyName());
			capitalAmount = (double) pTenderTypeQueryItems.getPropertyValue(getPriceProperties().getCapitalAmountPropertyName());
			if (isLoggingDebug()) {
				logDebug("Total Discount value Till date in promotion item" + totalDiscountAmountTillDate);
				logDebug("Capital amount in Budget item" + capitalAmount);
			}
			if (totalDiscountAmountTillDate != capitalAmount) {
				pTenderTypeQueryItems.setPropertyValue(getPriceProperties().getCapitalAmountPropertyName(),
						totalDiscountAmountTillDate);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUPromotionTools.updateToatlAmountIfMisMatch() method");
		}
	}

	/**
	 * This method is used to query the Tender Type repository based on promotion id to get the tender type item.
	 * 
	 * @param pPromotionId
	 *            the promotion id
	 * @return the tender type item
	 */
	public RepositoryItem[] getTenderTypeItem(String pPromotionId) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.getTenderTypeItem() method");
		}
		RepositoryView tenderTypePromotionview = null;
		QueryBuilder queryBuilder = null;
		QueryExpression tenderTypeConstant = null;
		QueryExpression tenderTypeProperty = null;
		RepositoryItem[] tenderTypeQueryItems = null;
		try {
			final MutableRepository tenderTypeRepository = (MutableRepository) getTenderTypeRepository();
			if (tenderTypeRepository != null) {
				tenderTypePromotionview = tenderTypeRepository.getView(getPriceProperties().getTenderTypePromotionItemType());
				queryBuilder = tenderTypePromotionview.getQueryBuilder();
				tenderTypeConstant = queryBuilder.createConstantQueryExpression(pPromotionId);
				tenderTypeProperty = queryBuilder
						.createPropertyQueryExpression(getPriceProperties().getPromotionIdPropertyName());
				final Query tenderTypeQuery = queryBuilder.createComparisonQuery(tenderTypeProperty, tenderTypeConstant,
						QueryBuilder.EQUALS);
				tenderTypeQueryItems = tenderTypePromotionview.executeQuery(tenderTypeQuery);
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError("RepositoryException: While processing promotions with exception : {0} ", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUPromotionTools.getTenderTypeItem() method");
		}
		return tenderTypeQueryItems;
	}

	/**
	 * This method is used to get the adjustments from orderPriceInfo, ShippingPriceInfo and ItemPriceInfo to update the tender.
	 * type adjustment list.
	 * 
	 * @param pOrder
	 *            the order
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<PricingAdjustment> updateTenderTypeAdjustment(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.updateTenderTypeAdjustment() method");
		}
		ShippingPriceInfo shippingPriceInfo = null;
		ItemPriceInfo itemPriceInfo = null;
		List<ShippingGroup> shippingGroups = null;
		List<CommerceItem> commerceItems = null;
		OrderPriceInfo orderPriceInfo = null;
		List<PricingAdjustment> tenderTypeAdjustmentPromotions = null;
		if (pOrder != null) {
			tenderTypeAdjustmentPromotions = new ArrayList<PricingAdjustment>();
			orderPriceInfo = pOrder.getPriceInfo();
			if (orderPriceInfo != null) {
				tenderTypeAdjustmentPromotions = updatePromotionItem(orderPriceInfo.getAdjustments(),
						tenderTypeAdjustmentPromotions);
			}
			shippingGroups = (List<ShippingGroup>) pOrder.getShippingGroups();
			if (shippingGroups != null && !shippingGroups.isEmpty()) {
				for (ShippingGroup shippingGroup : shippingGroups) {
					shippingPriceInfo = shippingGroup.getPriceInfo();
					if (shippingPriceInfo != null) {
						tenderTypeAdjustmentPromotions = updatePromotionItem(shippingPriceInfo.getAdjustments(),
								tenderTypeAdjustmentPromotions);
					}
				}
			}
			commerceItems = (List<CommerceItem>) pOrder.getCommerceItems();
			if (commerceItems != null && !commerceItems.isEmpty()) {
				for (CommerceItem commerceItem : commerceItems) {
					itemPriceInfo = commerceItem.getPriceInfo();
					if (itemPriceInfo != null) {
						tenderTypeAdjustmentPromotions = updatePromotionItem(itemPriceInfo.getAdjustments(),
								tenderTypeAdjustmentPromotions);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUPromotionTools.updateTenderTypeAdjustment() method");
		}
		return tenderTypeAdjustmentPromotions;
	}

	/**
	 * This method is used to update the list TenderTypePromotion based on budget type property available in promotion Item if.
	 * true it will add if false it will ignore.
	 * 
	 * @param pAdjustments
	 *            the adjustments
	 * @param pTenderTypePromotions
	 *            the tender type promotions
	 * @return the list
	 */
	public List<PricingAdjustment> updatePromotionItem(List<PricingAdjustment> pAdjustments,
			List<PricingAdjustment> pTenderTypePromotions) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.updatePromotionItem() method");
		}
		RepositoryItem pricingModel = null;
		boolean promotionTenderFlag = Boolean.FALSE;
		boolean budgetFlag = Boolean.FALSE;
		if (pAdjustments != null && !pAdjustments.isEmpty()) {
			for (PricingAdjustment adjustment : pAdjustments) {
				if (adjustment != null) {
					pricingModel = adjustment.getPricingModel();
					if (pricingModel != null) {
						if (pricingModel.getPropertyValue(getPriceProperties().getTenderTypePropertyName()) != null && pricingModel.getPropertyValue(getPriceProperties().getTenderTypePromotionProperty()) != null) {
							budgetFlag = (Boolean) pricingModel.getPropertyValue(getPriceProperties()
									.getTenderTypePropertyName());
							promotionTenderFlag = (Boolean) pricingModel.getPropertyValue(getPriceProperties()
									.getTenderTypePromotionProperty());
							if (isLoggingDebug()) {
								vlogDebug("budgetFlag : {0}  tenderFlag : {1}", budgetFlag, promotionTenderFlag);
							}
						}
						if (budgetFlag && promotionTenderFlag) {
							pTenderTypePromotions.add(adjustment);
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUPromotionTools.updatePromotionItem() method");
		}
		return pTenderTypePromotions;
	}

	/**
	 * Method to get all the Shipping Promotion (Free Shipping Type) from Profile.
	 * 
	 * @param pProfile
	 *            - Profile
	 * @return - List of PromotionImportExportInfo Object.
	 */
	@SuppressWarnings("unchecked")
	public List<PromotionImportExportInfo> getShippingPromotions(Profile pProfile) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.getShippingPromotions() method");
			vlogDebug("pProfile : {0}", pProfile);
		}
		List<RepositoryItem> activeShippingPromo = null;
		List<PromotionImportExportInfo> promoInfo = null;
		if (pProfile == null) {
			vlogDebug("Profile is Null so returning with promoInfo : {0}", promoInfo);
			return promoInfo;
		}
		// Getting all Shipping Promotion from Shipping Pricing Models.
		activeShippingPromo = (List<RepositoryItem>) getPricingTools().getShippingPricingEngine().getPricingModels(pProfile);
		List<RepositoryItem> baseShippingPromo = null;
		if (activeShippingPromo != null && !activeShippingPromo.isEmpty()) {
			final TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPricingModelProperties();
			for (RepositoryItem promoItem : activeShippingPromo) {
				if (promoItem != null) {
					if (baseShippingPromo == null) {
						baseShippingPromo = new ArrayList<RepositoryItem>();
					}
					String templateType = (String) promoItem.getPropertyValue(pricingModelProperties.getTemplate());
					// Checking the Template type
					if ((!StringUtils.isBlank(templateType)) && templateType.contains(getFreeShippingPromoTemplate())) {
						baseShippingPromo.add(promoItem);
					}
				}
			}
			if (baseShippingPromo != null && !baseShippingPromo.isEmpty()) {
				// Shorting the Promotion based on the Higher Priority or Best
				// for user.
				Collections.sort(baseShippingPromo, new PricingModelComparator());
				final RepositoryItem[] promoRepoItem = baseShippingPromo.toArray(new RepositoryItem[baseShippingPromo.size()]);
				try {
					// Calling oOTB method to get the Promotion Information.
					promoInfo = getPromotionImportExportTools().processPromotions(promoRepoItem);
				} catch (PromotionException prmotionExp) {
					vlogError("TRUPromotionTools.getShippingPromotions() PromotionException occured:{0}", prmotionExp);
				}
			}

		}

		vlogDebug("Exiting from TRUPromotionTools.getShippingPromotions() method with baseShippingPromo : {0}", promoInfo);
		return promoInfo;
	}

	/**
	 * Method to get all Active Tag Promotion : tagPromotion property value: true Profile.
	 * 
	 * @param pProfile
	 *            - Profile
	 * @return - Set of RepositoryItem Object.
	 */
	@SuppressWarnings("unchecked")
	public Set<RepositoryItem> getAllActivePromotions(Profile pProfile) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.getAllActivePromotions() method");
			vlogDebug("pProfile : {0}", pProfile);
		}
		List<RepositoryItem> allActivePromotions = null;
		Set<RepositoryItem> allTagPromotions = null;
		if (pProfile == null) {
			vlogDebug("Profile is Null so returning with allTagPromotions : {0}", allTagPromotions);
			return allTagPromotions;
		}
		try {
			// Getting all Active Promotions
			allActivePromotions = (List<RepositoryItem>) getPromotions(pProfile.getRepositoryId());
			if (allActivePromotions != null && !allActivePromotions.isEmpty()) {
				final TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPricingModelProperties();
				for (RepositoryItem lPromoItem : allActivePromotions) {
					if (lPromoItem.getPropertyValue(pricingModelProperties.getTagPromotionPropertyName()) != null &&
							 (boolean) lPromoItem.getPropertyValue(pricingModelProperties.getTagPromotionPropertyName())) {
						if (allTagPromotions == null) {
							allTagPromotions = new HashSet<RepositoryItem>();
							allTagPromotions.add(lPromoItem);
						} else {
							allTagPromotions.add(lPromoItem);
						}
					}
				}
			}
		} catch (PromotionException exc) {
			vlogError("TRUPromotionTools.getAllActivePromotions() PromotionException occured:{0}", exc);
		}
		vlogDebug("Exiting from TRUPromotionTools.getAllActivePromotions() method with allTagPromotions : {0}", allTagPromotions);
		return allTagPromotions;
	}

	/**
	 * Method to get all Item Level Active Tag Promotion : tagPromotion property value: true Profile.
	 * 
	 * @param pProfile
	 *            - Profile
	 * @return - Set of RepositoryItem Object.
	 */
	@SuppressWarnings("unchecked")
	public Set<RepositoryItem> getAllItemLevelPromotions(Profile pProfile) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.getAllItemLevelPromotions() method");
			vlogDebug("pProfile : {0}", pProfile);
		}
		List<RepositoryItem> allActiveItemPromotions = null;
		Set<RepositoryItem> allTagPromotions = null;
		if (pProfile == null) {
			vlogDebug("Profile is Null so returning with allTagPromotions : {0}", allTagPromotions);
			return allTagPromotions;
		}
		// Getting all Item Promotions
		allActiveItemPromotions = (List<RepositoryItem>) getPricingTools().getItemPricingEngine().getPricingModels(pProfile);
		if (allActiveItemPromotions != null && !allActiveItemPromotions.isEmpty()) {
			TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPricingModelProperties();
			for (RepositoryItem lPromoItem : allActiveItemPromotions) {
				if (lPromoItem.getPropertyValue(pricingModelProperties.getTagPromotionPropertyName()) != null && 
						 (boolean) lPromoItem.getPropertyValue(pricingModelProperties.getTagPromotionPropertyName())) {
					if (allTagPromotions == null) {
						allTagPromotions = new HashSet<RepositoryItem>();
						allTagPromotions.add(lPromoItem);
					} else {
						allTagPromotions.add(lPromoItem);
					}
				}
			}
		}
		vlogDebug("Exiting from TRUPromotionTools.getAllItemLevelPromotions() method with allItemTagPromotions : {0}",
				allTagPromotions);
		return allTagPromotions;
	}

	/**
	 * Method to get all Order Level Active Tag Promotion : tagPromotion property value: true Profile.
	 * 
	 * @param pProfile
	 *            - Profile
	 * @return - Set of RepositoryItem Object.
	 */
	@SuppressWarnings("unchecked")
	public Set<RepositoryItem> getAllOrderPromotions(Profile pProfile) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.getAllOrderPromotions() method");
			vlogDebug("pProfile : {0}", pProfile);
		}
		List<RepositoryItem> allActiveOrderPromotions = null;
		Set<RepositoryItem> allTagPromotions = null;
		if (pProfile == null) {
			vlogDebug("Profile is Null so returning with allTagPromotions : {0}", allTagPromotions);
			return allTagPromotions;
		}
		// Getting all Order Promotions
		allActiveOrderPromotions = (List<RepositoryItem>) getPricingTools().getOrderPricingEngine().getPricingModels(pProfile);
		if (allActiveOrderPromotions != null && !allActiveOrderPromotions.isEmpty()) {
			final TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPricingModelProperties();
			for (RepositoryItem lPromoItem : allActiveOrderPromotions) {
				if (lPromoItem.getPropertyValue(pricingModelProperties.getTagPromotionPropertyName()) != null && 
						 (boolean) lPromoItem.getPropertyValue(pricingModelProperties.getTagPromotionPropertyName())) {
					if (allTagPromotions == null) {
						allTagPromotions = new HashSet<RepositoryItem>();
						allTagPromotions.add(lPromoItem);
					} else {
						allTagPromotions.add(lPromoItem);
					}
				}
			}
		}
		vlogDebug("Exiting from TRUPromotionTools.getAllOrderPromotions() method with allOrderTagPromotions : {0}",
				allTagPromotions);
		return allTagPromotions;

	}

	/**
	 * Method to get all Item Shipping Active Tag Promotion : tagPromotion property value: true Profile.
	 * 
	 * @param pProfile
	 *            - Profile
	 * @return - Set of RepositoryItem Object.
	 */
	@SuppressWarnings("unchecked")
	public Set<RepositoryItem> getAllShippingPromotions(Profile pProfile) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.getAllShippingPromotions() method");
			vlogDebug("pProfile : {0}", pProfile);
		}
		List<RepositoryItem> allActiveShippingPromotions = null;
		Set<RepositoryItem> allTagPromotions = null;
		if (pProfile == null) {
			vlogDebug("Profile is Null so returning with allTagPromotions : {0}", allTagPromotions);
			return allTagPromotions;
		}
		// Getting all Shipping Promotions
		allActiveShippingPromotions = (List<RepositoryItem>) getPricingTools().getShippingPricingEngine().getPricingModels(
				pProfile);
		if (allActiveShippingPromotions != null && !allActiveShippingPromotions.isEmpty()) {
			final TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPricingModelProperties();
			for (RepositoryItem lPromoItem : allActiveShippingPromotions) {
				if (lPromoItem.getPropertyValue(pricingModelProperties.getTagPromotionPropertyName()) != null && 
						 (boolean) lPromoItem.getPropertyValue(pricingModelProperties.getTagPromotionPropertyName())) {
					if (allTagPromotions == null) {
						allTagPromotions = new HashSet<RepositoryItem>();
						allTagPromotions.add(lPromoItem);
					} else {
						allTagPromotions.add(lPromoItem);
					}
				}
			}
		}
		vlogDebug("Exiting from TRUPromotionTools.getAllShippingPromotions() method with allShippingTagPromotions : {0}",
				allTagPromotions);
		return allTagPromotions;

	}

	/**
	 * Method to get the Repository Item for Provided ID.
	 * 
	 * @param pItemDesName
	 *            - String
	 * @param pId
	 *            - String
	 * @return promotionItem - Repository Item
	 * @throws RepositoryException
	 *             - If any
	 */
	public RepositoryItem getItemForId(String pItemDesName, String pId) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.getItem method");
			vlogDebug("pItemDesName : {0} pId : {1}", pItemDesName, pId);
		}
		RepositoryItem promotionItem = null;
		if (StringUtils.isBlank(pId) && StringUtils.isBlank(pItemDesName)) {
			return promotionItem;
		}
		promotionItem = getPromotions().getItem(pId, pItemDesName);
		if (isLoggingDebug()) {
			vlogDebug("Exiting from TRUPromotionTools.getItem method with Repository Item : {0}", promotionItem);
		}
		return promotionItem;
	}

	/**
	 * Returns the priceProperties.
	 * 
	 * @return the priceProperties
	 */
	public TRUPriceProperties getPriceProperties() {
		return mPriceProperties;
	}

	/**
	 * Sets/updates the priceProperties.
	 * 
	 * @param pPriceProperties
	 *            the priceProperties to set
	 */
	public void setPriceProperties(TRUPriceProperties pPriceProperties) {
		mPriceProperties = pPriceProperties;
	}

	/**
	 * Gets the tender type repository.
	 * 
	 * @return the tenderTypeRepository
	 */
	public Repository getTenderTypeRepository() {
		return mTenderTypeRepository;
	}

	/**
	 * Sets the tender type repository.
	 * 
	 * @param pTenderTypeRepository
	 *            the tenderTypeRepository to set
	 */
	public void setTenderTypeRepository(Repository pTenderTypeRepository) {
		mTenderTypeRepository = pTenderTypeRepository;
	}

	/**
	 * Returns the promotionImportExportTools.
	 * 
	 * @return the promotionImportExportTools
	 */
	public PromotionImportExportTools getPromotionImportExportTools() {
		return mPromotionImportExportTools;
	}

	/**
	 * Sets/updates the promotionImportExportTools.
	 * 
	 * @param pPromotionImportExportTools
	 *            the promotionImportExportTools to set
	 */
	public void setPromotionImportExportTools(PromotionImportExportTools pPromotionImportExportTools) {
		mPromotionImportExportTools = pPromotionImportExportTools;
	}

	/**
	 * Returns the freeShippingPromoTemplate.
	 * 
	 * @return the freeShippingPromoTemplate
	 */
	public String getFreeShippingPromoTemplate() {
		return mFreeShippingPromoTemplate;
	}

	/**
	 * Sets/updates the promotionImportExportTools.
	 * 
	 * @param pFreeShippingPromoTemplate
	 *            the freeShippingPromoTemplate to set
	 */
	public void setFreeShippingPromoTemplate(String pFreeShippingPromoTemplate) {
		this.mFreeShippingPromoTemplate = pFreeShippingPromoTemplate;
	}

	/**
	 * Gets the configuration.
	 * 
	 * @return the configuration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * Sets the configuration.
	 * 
	 * @param pConfiguration
	 *            the configuration to set
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		mConfiguration = pConfiguration;
	}

	/**
	 * @return the dataSource
	 */
	public DataSource getDataSource() {
		return mDataSource;
	}

	/**
	 * @param pDataSource
	 *            the dataSource to set
	 */
	public void setDataSource(DataSource pDataSource) {
		mDataSource = pDataSource;
	}
	
	/**
	 * @return the mMultiplePromoQuery
	 */
	public String getMultiplePromoQuery() {
		return mMultiplePromoQuery;
	}

	/**
	 * @param pMultiplePromoQuery the mMultiplePromoQuery to set
	 */
	public void setMultiplePromoQuery(String pMultiplePromoQuery) {
		this.mMultiplePromoQuery = pMultiplePromoQuery;
	}

	/**
	 * @return the mSubQuery
	 */
	public String getSubQuery() {
		return mSubQuery;
	}

	/**
	 * @param pSubQuery the mSubQuery to set
	 */
	public void setSubQuery(String pSubQuery) {
		this.mSubQuery = pSubQuery;
	}

	/**
	 * @return the mSelectCondition
	 */
	public String getSelectCondition() {
		return mSelectCondition;
	}

	/**
	 * @param pSelectCondition the mSelectCondition to set
	 */
	public void setSelectCondition(String pSelectCondition) {
		this.mSelectCondition = pSelectCondition;
	}
	
	/**
	 * Method to get all the Order Promotion applied in Order.
	 * 
	 * @param pPriceQuote
	 *            : OrderPriceInfo Object
	 * @return : List - List of Promotion Repository Item
	 */
	@SuppressWarnings("unchecked")
	public List<RepositoryItem> getPromotionFromOrderPriceInfo(OrderPriceInfo pPriceQuote) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: getPromotionFromOrderPriceInfo]");
			vlogDebug("pPriceQuote: {0}", pPriceQuote);
		}
		List<RepositoryItem> orderPromotionList = null;
		if (pPriceQuote == null) {
			return orderPromotionList;
		}
		List<PricingAdjustment> orderPricingAdj = null;
		RepositoryItem orderPromotion = null;
		if (pPriceQuote != null) {
			orderPricingAdj = pPriceQuote.getAdjustments();
			if (orderPricingAdj != null && !orderPricingAdj.isEmpty()) {
				for (PricingAdjustment lPricingAdj : orderPricingAdj) {
					orderPromotion = lPricingAdj.getPricingModel();
					if (orderPromotion != null) {
						if (orderPromotionList == null) {
							orderPromotionList = new ArrayList<RepositoryItem>();
						}
						orderPromotionList.add(orderPromotion);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPromotionTools  method: getPromotionFromOrderPriceInfo]");
			vlogDebug("Returning with orderPromotionList: {0}", orderPromotionList);
		}
		return orderPromotionList;
	}

	/**
	 * Method to get all the Shipping Promotion applied in Order.
	 * 
	 * @param pPriceQuote
	 *            : ShippingPriceInfo Object
	 * @return : List - List of Promotion Repository Item
	 */
	@SuppressWarnings("unchecked")
	public List<RepositoryItem> getPromotionFromShippingPriceInfo(ShippingPriceInfo pPriceQuote) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: getPromotionFromShippingPriceInfo]");
			vlogDebug("pPriceQuote: {0}", pPriceQuote);
		}
		List<RepositoryItem> shippingPromotionList = null;
		if (pPriceQuote == null) {
			return shippingPromotionList;
		}
		List<PricingAdjustment> shippingPricingAdj = null;
		RepositoryItem shippingPromotion = null;
		if (pPriceQuote != null) {
			shippingPricingAdj = pPriceQuote.getAdjustments();
			if (shippingPricingAdj != null && !shippingPricingAdj.isEmpty()) {
				for (PricingAdjustment lPricingAdj : shippingPricingAdj) {
					shippingPromotion = lPricingAdj.getPricingModel();
					if (shippingPromotion != null) {
						if (shippingPromotionList == null) {
							shippingPromotionList = new ArrayList<RepositoryItem>();
						}
						shippingPromotionList.add(shippingPromotion);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPromotionTools  method: getPromotionFromShippingPriceInfo]");
			vlogDebug("Returning with orderPromotionList: {0}", shippingPromotionList);
		}
		return shippingPromotionList;
	}

	/**
	 * Method to get all the Item Promotion applied in Order.
	 * 
	 * @param pPriceQuote
	 *            : ItemPriceInfo Object
	 * @return : List - List of Promotion Repository Item
	 */
	@SuppressWarnings("unchecked")
	public List<RepositoryItem> getPromotionFromItemPriceInfo(ItemPriceInfo pPriceQuote) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: getPromotionFromItemPriceInfo]");
			vlogDebug("pPriceQuote: {0}", pPriceQuote);
		}
		List<RepositoryItem> itemPromotionList = null;
		if (pPriceQuote == null) {
			return itemPromotionList;
		}
		List<PricingAdjustment> itemPricingAdj = null;
		RepositoryItem itemPromotion = null;
		if (pPriceQuote != null) {
			itemPricingAdj = pPriceQuote.getAdjustments();
			if (itemPricingAdj != null && !itemPricingAdj.isEmpty()) {
				for (PricingAdjustment lPricingAdj : itemPricingAdj) {
					itemPromotion = lPricingAdj.getPricingModel();
					if (itemPromotion != null) {
						if (itemPromotionList == null) {
							itemPromotionList = new ArrayList<RepositoryItem>();
						}
						itemPromotionList.add(itemPromotion);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPromotionTools  method: getPromotionFromItemPriceInfo]");
			vlogDebug("Returning with orderPromotionList: {0}", itemPromotionList);
		}
		return itemPromotionList;
	}

	/**
	 * Fetches the valid promotions for product.
	 * 
	 * @param pPromotionsIds
	 *            pPromotionsIds
	 * @return : List - List of Promotion Repository Item
	 */
	public List<RepositoryItem> getValidProductProductPromotions(List<String> pPromotionsIds) {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUPromotionTools.getValidProductProductPromotions :: START");
		}
		
		if (pPromotionsIds != null && !pPromotionsIds.isEmpty()) {
			final List<RepositoryItem> promoList = new ArrayList<RepositoryItem>();
			RepositoryItem promotionItem = null;
			final Repository promoRepository = getPromotions();
			final TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPricingModelProperties();
			if (promoRepository != null) {
				final Date now = getCurrentDate().getTimeAsDate();
				for (String lPromoId : pPromotionsIds) {
					try {
						int indexOf = lPromoId.indexOf(TRUCommerceConstants.PIPELINE);
						String promoId = lPromoId.substring(0,indexOf);
						promotionItem = getItemForId(pricingModelProperties.getPromotionItemDescName(), promoId);
						// checking promotion is expired or not. checking for promotion is started or not also
						if (promotionItem != null && !checkPromotionExpiration(promotionItem, now) && checkPromotionStartDate(promotionItem, now)) {
							promoList.add(promotionItem);
						}
					} catch (RepositoryException exc) {
						if (isLoggingError()) {
							vlogError("No Promotion Found for Promotion ID : {0}", lPromoId);
						}
					}
				}
			}
			List<RepositoryItem> list = new ArrayList<RepositoryItem>(promoList);
			getSortedList(list);
			return list;
		}
		if (isLoggingDebug()) {
			logDebug("Exiting method TRUPromotionTools.getValidProductProductPromotions :: END");
		}
		return null;
	}
	/**
	 * Fetches the valid promotions for sku.
	 * 
	 * @param pPromos - Promos
	 * @param pSkuInfoVO - SkuInfoVO
	 *        
	 *
	 */
	public void setSkuPromotions(SKUInfoVO pSkuInfoVO, List<String> pPromos) {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUPromotionTools.setSkuPromotions :: START");
		}
		List<ProductPromoInfoVO> productPromoInfoList = getSkuPromotionDetails(pPromos);
		if(productPromoInfoList!=null && !productPromoInfoList.isEmpty() ){
			pSkuInfoVO.setPromos(productPromoInfoList);
		}else{
			if (isLoggingDebug()) {
				logDebug("No Promotions Configured for SKU:"+pSkuInfoVO.getId());
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting method TRUPromotionTools.setSkuPromotions :: END");
		}
	}
	/**
	 * Fetches the valid promotions for sku.
	 * 
	 * @param pPromos - Promos
	 * @return ProductPromoInfoVO List        
	 *
	 */
	public List<ProductPromoInfoVO> getSkuPromotionDetails(List<String> pPromos) {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUPromotionTools.getSkuPromotionDetails:: START");
		}
		List<RepositoryItem> promoList=getValidProductProductPromotions(pPromos);
		ProductPromoInfoVO productPromoInfoVO = null;
		int promoCount = TRUConstants.SIZE_ONE;
		String promotionCountinPDP = getConfiguration().getPromotionCountinPDP();
		if (!StringUtils.isBlank(promotionCountinPDP)) {
			try {
				promoCount = Integer.parseInt(promotionCountinPDP);
			} catch (NumberFormatException nfe) {
				if (isLoggingError()) {
					vlogError("Provided Promotion Count {0} is not a Integer Value {1}: ", promotionCountinPDP, nfe);
				}
			}
		}
		final TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPricingModelProperties();
		List<ProductPromoInfoVO> productPromoInfoList= new ArrayList<ProductPromoInfoVO>();
		if (promoList != null && !promoList.isEmpty()) {
			for (RepositoryItem promotionItem : promoList) {
				if (promotionItem != null) {
					productPromoInfoVO= new ProductPromoInfoVO();
					boolean isWebDisplay = false;
					if(promotionItem != null && null!=promotionItem.getPropertyValue(pricingModelProperties.getWebStoreDisplayPropertyName())){
						isWebDisplay = (boolean)promotionItem.getPropertyValue(pricingModelProperties.getWebStoreDisplayPropertyName());
					}
					if (isWebDisplay) {
						if (isSetPromotionId() && StringUtils.isNotEmpty(promotionItem.getRepositoryId())) {
							productPromoInfoVO.setPromotionId(promotionItem.getRepositoryId().toString());	
						}
						productPromoInfoVO.setDetails((String) promotionItem.getPropertyValue(pricingModelProperties.getPromotionDetailsPropertyName()));
						String description =(String) promotionItem.getPropertyValue(pricingModelProperties.getDescription());
						if (StringUtils.isBlank(description)) {
							description =(String) promotionItem.getPropertyValue(pricingModelProperties.getDisplayName());
						}
						productPromoInfoVO.setDescription(description);
						Timestamp  endDate =(Timestamp) promotionItem.getPropertyValue(pricingModelProperties.getEndUsable());
						if (endDate != null) {
							productPromoInfoVO.setEndDate(getDateUsingTime(new java.util.Date(endDate.getTime())));
						}
						Timestamp  startDate =(Timestamp) promotionItem.getPropertyValue(pricingModelProperties.getEndUsable());
						if (startDate != null) {
							productPromoInfoVO.setStartDate(getDateUsingTime(new java.util.Date(startDate.getTime())));
						}
						productPromoInfoList.add(productPromoInfoVO);
					}
				}
			}
			if (productPromoInfoList != null && !productPromoInfoList.isEmpty()) {
				if (productPromoInfoList.size() > promoCount) {
					productPromoInfoList= productPromoInfoList.subList(TRUCommerceConstants.INT_ZERO, promoCount-TRUCommerceConstants.INT_ONE);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting method TRUPromotionTools.getSkuPromotionDetails :: END");
		}
		return productPromoInfoList;
		
	}
	/**
     * check begin Usable promotion start date.
     *
     * @param pEligiblePromos the promotion item
     * 
      * @return true, if successful
     */
     public boolean checkbeginUsable(String pEligiblePromos) {
            
            if (isLoggingDebug()) {
                   logDebug("Enter into [Class: TRUPromotionTools  method: checkbeginUsable]");
            }
            String[] splitArray = pEligiblePromos
                         .split(TRUCommerceConstants.PIPELINE_SEPERATOR);
            String promoDate = splitArray[TRUCommerceConstants.INT_TWO];
            Format feedFormatter = new SimpleDateFormat(
                         TRUCommerceConstants.DATE_FORMAT_PROMOTION);
            try {
                   Date date = ((DateFormat) feedFormatter).parse(promoDate);
                   Date todayDate = getCurrentDate().getTimeAsDate();
                   if ((date != null) && (date.before(todayDate) )) {
                         return Boolean.TRUE;
                   }
            } catch (ParseException parseEx) {
                   if(isLoggingError()){
                         logError(TRUCommerceConstants.PARSE_EXCEPTION, parseEx);
                   }
            }
            if (isLoggingDebug()) {
                   logDebug("Exit from [Class: TRUPromotionTools  method: checkbeginUsable]");
            }
            return Boolean.FALSE;
     }

	 
     /**
      * check  End Usabl promotion End date.
      *
      * @param pEndUsablePromo the promotion item
      * 
       * @return true, if successful
      */
      public boolean checkEndUsable(String pEndUsablePromo) {
             
             if (isLoggingDebug()) {
                    logDebug("Enter into [Class: TRUPromotionTools  method: checkEndUsable]");
             }
             String[] splitArray = pEndUsablePromo
                          .split(TRUCommerceConstants.PIPELINE_SEPERATOR);
             String promoDate = splitArray[TRUCommerceConstants.INT_THREE];
             Format feedFormatter = new SimpleDateFormat(
                          TRUCommerceConstants.DATE_FORMAT_PROMOTION, Locale.getDefault());
             try {
                    Date endUsable = ((DateFormat) feedFormatter).parse(promoDate);
                    Date todayDate = getCurrentDate().getTimeAsDate();
                    if ((endUsable != null) && (endUsable.after(todayDate))) {
                          return Boolean.TRUE;
                    }
             } catch (ParseException parseEx) {
                    if(isLoggingError()){
                          logError(TRUCommerceConstants.PARSE_EXCEPTION, parseEx);
                    }
             }
             if (isLoggingDebug()) {
                    logDebug("Exit from [Class: TRUPromotionTools  method: checkEndUsable]");
             }
             return Boolean.FALSE;
      }

	 /**
	 * Check promotion start date.
	 *
	 * @param pPromotionItem the promotion item
	 * @param pNow the now
	 * @return true, if successful
	 */
	public boolean checkPromotionStartDate(RepositoryItem pPromotionItem,
			Date pNow) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: checkPromotionStartDate]");
		}
	    if (getPricingModelProperties().getBeginUsable() != null)
	      {
	        java.util.Date beginUsable = (java.util.Date)pPromotionItem.getPropertyValue(getPricingModelProperties().getBeginUsable());
	        if(isLoggingDebug()){
	        	vlogDebug("Begin Usable date : {0}", beginUsable);
	        }
	        if(beginUsable == null){
	        	return Boolean.TRUE;
	        }
	        java.util.Date now = pNow;
	        if (pNow == null) {
	          now = getCurrentDate().getTimeAsDate();
	        } else {
	        	now = normalizeDate(pNow);
	        }
	        if(isLoggingDebug()){
	        	vlogDebug("Now date : {0}", now);
	        }
	        if (beginUsable instanceof java.sql.Date) {
	          now = normalizeDate(now);
	        }
	        if ((beginUsable != null) && (now.compareTo(beginUsable) > 0)) {
	        	return Boolean.TRUE;
	          }
	      }
	    if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPromotionTools  method: checkPromotionStartDate]");
		}
	      return Boolean.FALSE;
	 }
	
	/**
	 * checks Promotion Grant
	 * 
	 * @param pProfile
	 *            the profile
	 * @param pPromotion
	 *            the promotion
	 * @param pCoupon
	 *            the coupon
	 * @param pCheckProps
	 *            the check props
	 * @return true, if successful
	 * @throws PromotionException
	 *             the promotion exception
	 */
	/*@SuppressWarnings("rawtypes")
	@Override
	public boolean checkPromotionGrant(RepositoryItem pProfile, RepositoryItem pPromotion, RepositoryItem pCoupon, String[] pCheckProps)
			throws PromotionException {
		// Calling method to check ATG coupon and SUCN coupon applied for same promotion.
		boolean isATGPromotionExists=checkAtgpromtion(pProfile,pPromotion,pCoupon,pCheckProps);
		if(null!=pCoupon && (pCoupon.getRepositoryId().length()==TRUConstants.TWENTY || isATGPromotionExists)){
		if (pProfile == null) {
			String msg = ResourceUtils.getMsgResource(TRUConstants.NULL_PROFILE, TRUConstants.PROMOTION_RESOURCES, sResourceBundle);

			throw new PromotionException(TRUConstants.NULL_PROFILE, msg);
		}
		if (pPromotion == null) {
			String msg = ResourceUtils.getMsgResource(TRUConstants.NULL_PROMTION, TRUConstants.PROMOTION_RESOURCES, sResourceBundle);

			throw new PromotionException(TRUConstants.NULL_PROMTION, msg);
		}
		boolean allowMultiple = false;
		try {
			if (checkPromotionExpiration(pPromotion, getCurrentDate().getTimeAsDate())) {
				String[] msgArgs = { pPromotion.getRepositoryId(), pProfile.getRepositoryId() };

				String msg = ResourceUtils.getMsgResource(TRUConstants.EXPIRED_PROMOTION, TRUConstants.PROMOTION_RESOURCES, sResourceBundle,
						msgArgs);
				throw new PromotionException(TRUConstants.EXPIRED_PROMTION_RES, msg);
			}
			if ((pProfile.isTransient()) && (this.mGiveToAnonymousProfilesProperty != null)) {
				Boolean giveToAnonymous = (Boolean) pPromotion.getPropertyValue(getGiveToAnonymousProfilesProperty());
				if ((giveToAnonymous != null) && (!(giveToAnonymous.booleanValue()))) {
					String[] msgArgs = { pPromotion.getRepositoryId(), pProfile.getRepositoryId() };
					String msg = ResourceUtils.getMsgResource(TRUConstants.ANONY_PROFILE, TRUConstants.PROMOTION_RESOURCES,
							sResourceBundle, msgArgs);
					throw new PromotionException(TRUConstants.ANONY_PROFILE_RES, msg);
				}
			}
			allowMultiple = ((Boolean) pPromotion.getPropertyValue(getAllowMultipleProperty())).booleanValue();
		} catch (RemovedItemException rie) {
			if (isLoggingDebug()){
				logDebug("Getting exception while removing the promotion" + rie);
			}
			if (isLoggingWarning()) {
				Object[] params = { pPromotion.getRepositoryId(), pProfile.getRepositoryId() };
				logWarning(MessageFormat.format(sResourceBundle.getString(TRUConstants.PROMO_REMOVED), params));
			}
			String[] msgArgs = { pPromotion.getRepositoryId(), pProfile.getRepositoryId() };
			String msg = ResourceUtils.getMsgResource(TRUConstants.PROMO_REMOVED, TRUConstants.PROMOTION_RESOURCES, sResourceBundle,
					msgArgs);
			throw new PromotionException(TRUConstants.PROMO_REMOVED, msg);
		}
		if (!(allowMultiple)) {
			Collection promotionStatuses = getPromoStatuses(pProfile, pCheckProps);
			if (isLoggingDebug()) {
				logDebug(TRUConstants.EXISTING_PROM_FOR + pProfile.getRepositoryId() + "=" + promotionStatuses);
			}
			if (isPromotionInPromotionStatuses(pPromotion, promotionStatuses)) {
				if (pCoupon != null && isCouponInPromotionStatusesCoupons(pCoupon, promotionStatuses)) {
					return true;
				}// This is new added by me
				if (isLoggingDebug()) {
					logDebug(TRUConstants.PROMTION + pPromotion + TRUConstants.REJECTED_BECAUSE + pProfile.getRepositoryId()
							+TRUConstants.ACTIVE_PROMO_LIST);
				}
				String[] msgArgs = { pPromotion.getRepositoryId(), pProfile.getRepositoryId() };

				String msg = ResourceUtils.getMsgResource(TRUConstants.DUPLICATE_PROMOTION, TRUConstants.PROMOTION_RESOURCES,
						sResourceBundle, msgArgs);
				throw new DuplicatePromotionException(TRUConstants.DUPLICATE_PROMOTION_RES, msg);
			}
			Collection usedPromotions = (Collection) pProfile.getPropertyValue(getUsedPromotionsProperty());
			Iterator usedPromoIterator = usedPromotions.iterator();
			while (usedPromoIterator.hasNext()) {
				RepositoryItem promotion = (RepositoryItem) usedPromoIterator.next();
				if ((promotion != null) && (promotion.getRepositoryId().equals(pPromotion.getRepositoryId()))) {
					if (isLoggingDebug()) {
						logDebug(TRUConstants.PROMTION + pPromotion +TRUConstants.REJECTED_BECAUSE+TRUConstants.ACTIVE_PROMO_LIST);
					}
					String[] msgArgs = { pPromotion.getRepositoryId(), pProfile.getRepositoryId() };
					String msg = ResourceUtils.getMsgResource(TRUConstants.USED_PROMOTION, TRUConstants.PROMOTION_RESOURCES,
							sResourceBundle, msgArgs);
					throw new PromotionException(TRUConstants.USED_PROMOTION_RES, msg);
				}
			}
		}
		return true;
		}
		else{
			return super.checkPromotionGrant(pProfile, pPromotion, pCoupon, pCheckProps);
		}
	}*/
	  
	  /**
  	 * Check atgpromtion.
  	 *
  	 * @param pProfile the profile
  	 * @param pPromotion the promotion
  	 * @param pCoupon the coupon
  	 * @param pCheckProps the check props
  	 * @return true, if successful
  	 */
  	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean checkAtgpromtion(RepositoryItem pProfile, RepositoryItem pPromotion, RepositoryItem pCoupon,String[] pCheckProps) {
  		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: checkAtgpromtion]");
		}
  		Collection promotionStatuses = getPromoStatuses(pProfile, pCheckProps);
  		int numberOfAtgCoupon=0; 
  		List<RepositoryItem> coupons = new ArrayList<RepositoryItem>();
  		if(null!= promotionStatuses && isPromotionInPromotionStatuses(pPromotion, promotionStatuses)){
  			Iterator statusIterator = promotionStatuses.iterator();	
  			while (statusIterator.hasNext())
  		    {
  		    	RepositoryItem promotionStatus = (RepositoryItem)statusIterator.next();
  		    	RepositoryItem promotion =(RepositoryItem)(promotionStatus).getPropertyValue(getPromoStatusPromoProperty());
  		    	if (promotionStatus != null && promotion.getRepositoryId().equals(pPromotion.getRepositoryId()))
  		    	{
  		    			coupons.addAll((List<RepositoryItem>) promotionStatus.getPropertyValue(getPromoStatusCouponsPropertyName()));
  		    	}
  		    }
  		}
  		for(RepositoryItem coupon:coupons){
  			if(coupon.getRepositoryId().length()!=TRUConstants.TWENTY){
  				++numberOfAtgCoupon;
  			}
  		}
  		if(numberOfAtgCoupon>=TRUConstants.INTEGER_NUMBER_ONE){
  			return false;
  		}
  		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPromotionTools  method: checkAtgpromtion]");
		}
		return true;
	}


	/**
  	 * Checks if is coupon in promotion statuses coupons.
  	 *
  	 * @param pCoupon the coupon
  	 * @param pPromotionStatusesCoupons the promotion statuses coupons
  	 * @return true, if is coupon in promotion statuses coupons
  	 */
  	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean isCouponInPromotionStatusesCoupons(RepositoryItem pCoupon, Collection pPromotionStatusesCoupons)
	  {
  		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: isCouponInPromotionStatusesCoupons]");
		}
	    Iterator statusIterator = pPromotionStatusesCoupons.iterator();
	    List<RepositoryItem> coupons = new ArrayList<RepositoryItem>();
	    while (statusIterator.hasNext())
	    {
	    	RepositoryItem promotionStatus = (RepositoryItem)statusIterator.next();

	    	if (promotionStatus != null)
	    	{
	    			//it will return coupons associated with promtion statuses
	    			List<RepositoryItem> coupons1 = (List<RepositoryItem>) promotionStatus.getPropertyValue(getPromoStatusCouponsPropertyName());
	    			coupons.addAll(coupons1);
	    	}
	    }
	    if (coupons != null && !coupons.isEmpty()) {
  		  for (RepositoryItem coupon : coupons) {
  			  String couponId = (String) coupon.getRepositoryId();
  			  if ((pCoupon != null) && (!pCoupon.getRepositoryId().equals(couponId)))
  			  {
  				  return true;
  			  }
  		  }
  	  }
	    if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPromotionTools  method: isCouponInPromotionStatusesCoupons]");
		}
	    return false;
	  }
	  
	  /**
  	 * Gets the promo statuses coupons.
  	 *
  	 * @param pProfile the profile
  	 * @param pCheckProps the check props
  	 * @return the promo statuses coupons
  	 */
  	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection getPromoStatusesCoupons(RepositoryItem pProfile, String[] pCheckProps){
  		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: getPromoStatusesCoupons]");
		}
		  Collection promotionStatuses = new ArrayList();
		    for (int i = 0; i < pCheckProps.length; ++i) {
		      Object value = pProfile.getPropertyValue(pCheckProps[i]);
		      if (value instanceof Collection) {
		        promotionStatuses.addAll((Collection)value);
		      }
		    }
		    if (isLoggingDebug()) {
				logDebug("Exit from [Class: TRUPromotionTools  method: getPromoStatusesCoupons]");
			}
		    return promotionStatuses;
	  }

    /**
  	 * Normalize date.
  	 *
  	 * @param pDate the date
  	 * @return the java.util. date
  	 */
  	private java.util.Date normalizeDate(java.util.Date pDate)
      {
        Calendar cNow = Calendar.getInstance();
        cNow.setTime(pDate);
        cNow.clear(TRUConstants.THIRTEEN);
        cNow.clear(TRUConstants.FOURTEEN);

        return cNow.getTime();
      }
	
	/**
	 * Method wil give current time in YYYY/MM/DD HH:MI:SS format.
	 * @param pCurrentDate Date
	 * @return currentDate of String
	 */
	private String getDateUsingTime(Date pCurrentDate) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat(TRUCommerceConstants.PROMOTION_END_DATE_FORMAT,Locale.US);
	    String currentDate = dateFormat.format(pCurrentDate);
	    return currentDate;
	}
	/**
	 * Gets the mLockManager.
	 * 
	 * @return the mLockManager
	 */
	public TRULockManager getLockManager() {
		return mLockManager;
	}

	/**
	 * Sets the mLockManager.
	 * 
	 * @param pLockManager
	 *            the mLockManager to set
	 */
	public void setLockManager(TRULockManager pLockManager) {
		this.mLockManager = pLockManager;
	}
	
	/**
	 * Method to return the List of Excluded SKU Repository Items from Promotion.
	 * 
	 * @param pPromotion - Promotion Repository Item
	 * @return List - List of Excluded SKU Repository items.
	 */
	@SuppressWarnings("unchecked")
	public List<RepositoryItem> getExcludedSKUforPromo(RepositoryItem pPromotion){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: getExcludedSKUforPromo]");
			vlogDebug("pPromotion : {0}", pPromotion);
		}
		List<RepositoryItem> excludedSkuItem = null;
		if(pPromotion == null){
			return excludedSkuItem;
		}
		TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPricingModelProperties();
		excludedSkuItem = (List<RepositoryItem>) pPromotion.getPropertyValue(
				pricingModelProperties.getExcludedSkusPropertyName());
		if (isLoggingDebug()) {
			vlogDebug("List of Excluded SKU items : {0}", excludedSkuItem);
			logDebug("Exit from [Class: TRUPromotionTools  method: getExcludedSKUforPromo]");
		}
		return excludedSkuItem;
	}
	
	/**
	 * Method to return the List of Qualified SKU Repository Items for Promotion.
	 * 
	 * @param pPromotion - Promotion Repository Item
	 * @return List - List of Qualified SKU Repository items.
	 */
	@SuppressWarnings("unchecked")
	public List<RepositoryItem> getQualifiedSKUforPromo(RepositoryItem pPromotion){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: getQualifiedSKUforPromo]");
			vlogDebug("pPromotion : {0}", pPromotion);
		}
		List<RepositoryItem> qualifiedSKU = null;
		if(pPromotion == null){
			return qualifiedSKU;
		}
		TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPricingModelProperties();
		List<RepositoryItem> qualifiedSKUItems = (List<RepositoryItem>) pPromotion.getPropertyValue(
				pricingModelProperties.getQualifyingSkusPropertyName());
		if(qualifiedSKUItems == null || qualifiedSKUItems.isEmpty()){
			return qualifiedSKU;
		}
		qualifiedSKU = new ArrayList<RepositoryItem>();
		qualifiedSKU.addAll(qualifiedSKUItems);
		if (isLoggingDebug()) {
			vlogDebug("List of Qualified SKU items : {0}", qualifiedSKU);
			logDebug("Exit from [Class: TRUPromotionTools  method: getQualifiedSKUforPromo]");
		}
		return qualifiedSKU;
	}
	
	/**
	 * Method to return the List of Qualified SKU Ids.
	 * 
	 * @param pPromotion - Promotion Repository Item
	 * @return List - List of Bulk Qualified SKU ids.
	 */
	public List<String> getQualifyBulkSkuIds(RepositoryItem pPromotion){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: getQualifyBulkSkuIds]");
			vlogDebug("pPromotion : {0}", pPromotion);
		}
		if(pPromotion == null){
			return null;
		}
		TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPricingModelProperties();
		String bulkSkus = (String) pPromotion.getPropertyValue(pricingModelProperties.getBulkQualifyingSkusPropertyName());
		if (isLoggingDebug()) {
			vlogDebug("bulkSkus IDS : {0}", bulkSkus);
		}
		String[] skuIds = null;
		if(StringUtils.isNotBlank(bulkSkus)){
			skuIds = bulkSkus.split(TRUCommerceConstants.COMA_SYMBOL);
		}
		if(skuIds == null || skuIds.length <= TRUConstants.ZERO){
			return null;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPromotionTools  method: getQualifyBulkSkuIds]");
		}
		return Arrays.asList(skuIds);
	}
	
	/**
	 * Method to convert the SKU id to repository Items.
	 * 
	 * @param pBulkSkuString - List of SKU ids.
	 * @param pQualifiedSKUItems - List of Qualified SKU Repository items.
	 * @return List - List of SKU repository items.
	 */
	public List<RepositoryItem> convertStringToList(String pBulkSkuString, List<RepositoryItem> pQualifiedSKUItems) {
		List<RepositoryItem> qualifiedSKUItems=pQualifiedSKUItems;
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: convertStringToList]");
			vlogDebug("pBulkSkuString : {0}  pQualifiedSKUItems : {1}", pBulkSkuString, qualifiedSKUItems);
		}
		if(StringUtils.isBlank(pBulkSkuString)){
			return qualifiedSKUItems;
		}
		String[] split = pBulkSkuString.split(TRUCommerceConstants.COMA_SYMBOL);
		if(split == null || split.length <= TRUConstants.ZERO){
			return qualifiedSKUItems;
		}
		CatalogTools catalogTools = getPricingTools().getOrderManager().getCatalogTools();
		RepositoryItem skuItem = null;
		if(qualifiedSKUItems == null){
			qualifiedSKUItems = new ArrayList<RepositoryItem>();
		}
		for(String skuId : split){
			try {
				skuItem = catalogTools.findSKU(skuId);
				qualifiedSKUItems.add(skuItem);
			} catch (RepositoryException exc) {
				if (isLoggingError()) {
					vlogError("RepositoryException: while getting sku for sku id : {0} with exception : {1}",
							skuId,exc);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("List of Qualified SKU items : {0}", qualifiedSKUItems);
			logDebug("Exit from [Class: TRUPromotionTools  method: convertStringToList]");
		}
		return qualifiedSKUItems;
	}
	
	/**
	 * Check tender type promotion.
	 *
	 * @param pOrder the order
	 * @return the boolean
	 */
	@SuppressWarnings("unchecked")
	public Boolean checkTenderTypePromotion(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Entering into TenderBasedCheckDroplet.checkTenderTypePromotion() method");
		}
		ShippingPriceInfo shippingPriceInfo = null;
		ItemPriceInfo itemPriceInfo = null;
		List<ShippingGroup> shippingGroups = null;
		List<CommerceItem> commerceItems = null;
		OrderPriceInfo orderPriceInfo = null;
		Boolean tenderTypePromotion = Boolean.FALSE;
		orderPriceInfo = pOrder.getPriceInfo();
		if (orderPriceInfo != null) {
			tenderTypePromotion = isTenderTypePromotion(orderPriceInfo.getAdjustments());
			if (tenderTypePromotion) {
				return tenderTypePromotion;
			}
		}
		shippingGroups = (List<ShippingGroup>) pOrder.getShippingGroups();
		if (shippingGroups != null && !shippingGroups.isEmpty()) {
			for (ShippingGroup shippingGroup : shippingGroups) {
				shippingPriceInfo = shippingGroup.getPriceInfo();
				if (shippingPriceInfo != null) {
					tenderTypePromotion = isTenderTypePromotion(shippingPriceInfo.getAdjustments());
					if (tenderTypePromotion) {
						return tenderTypePromotion;
					}
				}
			}
		}
		commerceItems = (List<CommerceItem>) pOrder.getCommerceItems();
		if (commerceItems != null && !commerceItems.isEmpty()) {
			for (CommerceItem commerceItem : commerceItems) {
				itemPriceInfo = commerceItem.getPriceInfo();
				if (itemPriceInfo != null) {
					tenderTypePromotion = isTenderTypePromotion(itemPriceInfo.getAdjustments());
					if (tenderTypePromotion) {
						return tenderTypePromotion;
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TenderBasedCheckDroplet.checkTenderTypePromotion() method");
		}
		return tenderTypePromotion;
	}

	/**
	 * Checks if is tender type promotion.
	 *
	 * @param pAdjustments            the adjustments
	 * @return the list
	 */
	private Boolean isTenderTypePromotion(List<PricingAdjustment> pAdjustments) {
		if (isLoggingDebug()) {
			logDebug("Entering into TenderBasedCheckDroplet.isTenderTypePromotion() method");
		}
		RepositoryItem pricingModel = null;
		boolean promotionTenderType = Boolean.FALSE;
		if (pAdjustments != null && !pAdjustments.isEmpty()) {
			for (PricingAdjustment adjustment : pAdjustments) {
				if (adjustment != null) {
					pricingModel = adjustment.getPricingModel();
					if (pricingModel != null) {
						if (pricingModel.getPropertyValue(getPriceProperties().getTenderTypePromotionItemType()) != null) {
							promotionTenderType = (Boolean) pricingModel.getPropertyValue(getPriceProperties().getTenderTypePromotionItemType());
							if (isLoggingDebug()) {
								logDebug("Tender Type" + promotionTenderType);
							}
						}
						if (promotionTenderType) {
							return promotionTenderType;
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TenderBasedCheckDroplet.isTenderTypePromotion() method");
		}
		return promotionTenderType;
	}
	
	/**
	 * Revoke single use coupons from user.
	 *
	 * @param pSingleUseCoupons the single use coupons
	 * @param pProfile the profile
	 * @throws RepositoryException the repository exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void revokeSingleUseCouponsfromUser(List<RepositoryItem> pSingleUseCoupons, RepositoryItem pProfile) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Entering into revokeSingleUseCouponsfromUser method");
			vlogDebug("pSingleUseCoupons:{0} pProfile : {1}", pSingleUseCoupons,pProfile);
		}
	    List<RepositoryItem> activePromotions = (List<RepositoryItem>)pProfile.getPropertyValue(getActivePromotionsProperty());
	    List<RepositoryItem> removablePromotionStatus=new ArrayList<RepositoryItem>();
	    MutableRepositoryItem profile = getProfileRepository().getItemForUpdate(pProfile.getRepositoryId(), getProfileItemType());
	    Iterator statusIterator = activePromotions.iterator();	
		while (statusIterator.hasNext()){
			RepositoryItem promotionStatus = (RepositoryItem)statusIterator.next();
		    List<RepositoryItem> multicoupons=(List<RepositoryItem>) promotionStatus.getPropertyValue(getPromoStatusCouponsPropertyName());
		    for(RepositoryItem multiCoupon:multicoupons){
		    	if(pSingleUseCoupons.contains(multiCoupon)){
		    		removablePromotionStatus.add(promotionStatus);
  		    	}
  		    }
		}
		if(isLoggingDebug()){
			vlogDebug("removablePromotionStatus : {0}", removablePromotionStatus);
		}
		if(!removablePromotionStatus.isEmpty()){
			activePromotions.removeAll(removablePromotionStatus);
		}
		profile.setPropertyValue(getActivePromotionsProperty(), activePromotions);
		if (isLoggingDebug()) {
			logDebug("Exiting from revokeSingleUseCouponsfromUser method");
		}
	}
	/**
	 * @return the mEmailConfiguration
	 */
	public TRUPromotionEmailConfiguration getEmailConfiguration() {
		return mEmailConfiguration;
	}

	/**
	 * @param pEmailConfiguration the mEmailConfiguration to set
	 */
	public void setEmailConfiguration(
			TRUPromotionEmailConfiguration pEmailConfiguration) {
		this.mEmailConfiguration = pEmailConfiguration;
	}

	/**
	 * Gets the template email sender.
	 * 
	 * @return the templateEmailSender
	 */
	public TemplateEmailSender getTemplateEmailSender() {
		return mTemplateEmailSender;
	}

	/**
	 * Sets the template email sender.
	 * 
	 * @param pTemplateEmailSender
	 *            the mTemplateEmailSender to set
	 */
	public void setTemplateEmailSender(TemplateEmailSender pTemplateEmailSender) {
		mTemplateEmailSender = pTemplateEmailSender;
	}

	/**
	 * Does the sorting.
	 * 
	 *  @param pList the promotionItemList
	 * 
	 */
	public void getSortedList(List<RepositoryItem> pList) {
		Collections.sort(pList, new Comparator<RepositoryItem>(){
			public int compare(RepositoryItem pPriority1, RepositoryItem pPriority2) {
				Integer p01Item;
				Integer p02Item;
				if((Integer) pPriority1.getPropertyValue(((TRUPricingModelProperties) getPricingModelProperties()).getDisplayPriorityPropertyName())!=null){
					p01Item=(Integer) pPriority1.getPropertyValue(((TRUPricingModelProperties) getPricingModelProperties()).getDisplayPriorityPropertyName());
				}else{
					p01Item=(Integer) pPriority1.getPropertyValue(getPricingModelProperties().getPriority());
				}
				if((Integer) pPriority2.getPropertyValue(((TRUPricingModelProperties) getPricingModelProperties()).getDisplayPriorityPropertyName())!=null){
					p02Item=(Integer) pPriority2.getPropertyValue(((TRUPricingModelProperties) getPricingModelProperties()).getDisplayPriorityPropertyName());
				}else{
					p02Item=(Integer) pPriority2.getPropertyValue(getPricingModelProperties().getPriority());
				}
				return (p01Item.compareTo(p02Item));
			}
		});
		
	}

	/**
	 * This method will get all the promotion associated with skuId
	 * 
	 * @param pKeys
	 *            - SkuIdArray
	 * @return - Map of Object.
	 */
	@SuppressWarnings({ "unused" })
	public TRUPromotionInfoVO populateAllPromo(Object[] pKeys) {
		String skuId = null;
		Connection dbConnection = null;
		Date endUsableDate = null;
		Date beginUsableDate = null;
		PreparedStatement preparedStatement = null;
		List<String> skuIdList = new ArrayList<String>();
		List<String> promoList = new ArrayList<String>();
		TRUPromoVO promoObject = null;
		boolean validateEndUsable = false;
		TRUPromotionInfoVO promotionInfoVO = null;
		int promoCount = TRUConstants.SIZE_ONE;
		String promotionCountinPDP = getConfiguration().getPromotionCountinPDP();
		if (!StringUtils.isBlank(promotionCountinPDP)) {
			try {
				promoCount = Integer.parseInt(promotionCountinPDP);
			} catch (NumberFormatException nfe) {
				if (isLoggingError()) {
					vlogError("Provided Promotion Count {0} is not a Integer Value {1}: ", promotionCountinPDP, nfe);
				}
			}
		}
		Map<String, TRUPromoContainer> promoMap = new HashMap<String, TRUPromoContainer>();
		List<TRUPromoVO> promoSetObject = new ArrayList<TRUPromoVO>();
		String skuIdInput = (TRUCommerceConstants.EMPTY_SPACE + Arrays.asList(pKeys)).replaceAll(TRUCommerceConstants.REPLACE_PIPE, TRUCommerceConstants.BACK_SLASHE).replace(TRUCommerceConstants.COMMA_WITH_SPACE, TRUCommerceConstants.BACK_SLASHES);
		String query = getMultiplePromoQuery() + skuIdInput + getSubQuery() + skuIdInput + getSelectCondition();
		try {
			dbConnection = getDataSource().getConnection();
			preparedStatement = dbConnection.prepareStatement(query);
			ResultSet rs = preparedStatement.executeQuery();
			TRUPromoContainer promoContainer = null;
			while (rs.next()) {
				skuId = rs.getString(TRUCommerceConstants.SKU_ID_COLUMN);
				endUsableDate = rs.getTimestamp(TRUCommerceConstants.END_USABLE);
				beginUsableDate = rs.getTimestamp(TRUCommerceConstants.BEGIN_USABLE);
				validateEndUsable = validatePromo(beginUsableDate, endUsableDate);
					if (StringUtils.isNotEmpty(skuId) && validateEndUsable) {
						if (promoMap.get(skuId) != null) {
							promoContainer = (TRUPromoContainer) promoMap.get(skuId);
							promoSetObject = promoContainer.getPromotions();
							promoObject = new TRUPromoVO();
							promoObject.setPromoId(rs.getString(TRUCommerceConstants.PROMOTION_ID_COLUMN));
							promoObject.setPriority(rs.getInt(TRUCommerceConstants.PRIORITY));
							promoObject.setDetails(rs.getString(TRUCommerceConstants.DETAILS_COLUMN));
							if(StringUtils.isNotBlank(rs.getString(TRUCommerceConstants.DESCRIPTION_COLUMN))){
							promoObject.setDescription(rs.getString(TRUCommerceConstants.DESCRIPTION_COLUMN));
							}else{
								promoObject.setDescription(rs.getString(TRUCommerceConstants.DISPLAY_NAME_COLUMN));
							}
							promoSetObject.add(promoObject);
							promoContainer.setPromotions(promoSetObject);
						} else {
							promoContainer = new TRUPromoContainer();
							promoObject = new TRUPromoVO();
							promoSetObject = new ArrayList<TRUPromoVO>();
							promoObject.setPromoId(rs.getString(TRUCommerceConstants.PROMOTION_ID_COLUMN));
							promoObject.setPriority(rs.getInt(TRUCommerceConstants.PRIORITY));
							promoObject.setDetails(rs.getString(TRUCommerceConstants.DETAILS_COLUMN));
							if(StringUtils.isNotBlank(rs.getString(TRUCommerceConstants.DESCRIPTION_COLUMN))){
							promoObject.setDescription(rs.getString(TRUCommerceConstants.DESCRIPTION_COLUMN));
							}else{
								promoObject.setDescription(rs.getString(TRUCommerceConstants.DISPLAY_NAME_COLUMN));
							}
							promoSetObject.add(promoObject);
							promoContainer.setSkuId(skuId);
							promoContainer.setPromotions(promoSetObject);
						}
						promoMap.put(skuId, promoContainer);
					}
			}
			promotionInfoVO = new TRUPromotionInfoVO();
			List<TRUPromoContainer> promoListInfo  = new ArrayList<TRUPromoContainer>();
			List<TRUPromoVO> promoFilter = null;
			for (Entry<String, TRUPromoContainer> entry : promoMap.entrySet()) {
			    TRUPromoContainer value = (TRUPromoContainer)entry.getValue();
				if (value != null) {
					Collections.sort(value.getPromotions(),
							new Comparator<TRUPromoVO>() {
								public int compare(TRUPromoVO pKeyA, TRUPromoVO pKeyB) {
									if (pKeyA.getPriority() > pKeyB.getPriority()) {
										return TRUCommerceConstants.INT_ONE;
									} else {
										return TRUCommerceConstants.STRING_NOT_FOUND;
									}
								}
							});
						if (value.getPromotions().size() > promoCount) {
							promoFilter = value.getPromotions().subList(TRUCommerceConstants.INT_ZERO, promoCount-TRUCommerceConstants.INT_ONE);
							value.setPromotions(promoFilter);
						}	
			    	promoListInfo.add(value);
			    }
			}
			promotionInfoVO.setPromotionInfoList(promoListInfo);
		} catch (SQLException sQLException) {
			vlogError("SQLException : {0}", sQLException);
		} finally {
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException sQLException) {
					vlogError("SQLException : {0}", sQLException);
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException sQLException) {
					vlogError("SQLException : {0}", sQLException);
				}
			}

		}
		return promotionInfoVO;
	}
	/**
	 * Validate the beginUsableDate and endUsableDate for promotions
	 * @param pStartDate - StartDate
	 * @param pEndDate - EndDate
	 * @return activePromo
	 */
	public boolean validatePromo(Date pStartDate, Date pEndDate) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.validatePromo() method");
		}
		boolean activePromo = Boolean.FALSE;
		Date todayDate = getCurrentDate().getTimeAsDate();
		if (((pEndDate == null) || (pEndDate.after(todayDate))) && ((pStartDate == null) || (pStartDate.before(todayDate)))) {
			activePromo = true;
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUPromotionTools.validatePromo() method");
		}
		return activePromo;
	}
	/**
	 * This method is used to remove the tender type item in. tenderTypeRepository when promotions are disabled.
	 *
	 * @param pTenderItems the tender items
	 */
	public void removeTenderTypeItem(List<RepositoryItem> pTenderItems) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPromotionTools.removeTenderTypeItem() method");
		}
		try {
			List<String> tenderIds=new ArrayList<String>();
			final MutableRepository tenderTypeRepository = (MutableRepository) getTenderTypeRepository();
			if (pTenderItems != null && !pTenderItems.isEmpty()) {
				for (RepositoryItem tenderTypeItem : pTenderItems) {
					tenderIds.add(tenderTypeItem.getRepositoryId());
				}
			}
			if(!tenderIds.isEmpty()){
				for (String tenderId : tenderIds) {
					tenderTypeRepository.removeItem(tenderId, getTenderTypePromotionItemType());
					
				}
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError(e, "Repository Exception in method TRUPromotionTools.removeTenderTypeItem");
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUPromotionTools.removeTenderTypeItem() method");
		}
	}

	
	/**
	 * This method is extended to do the check for Tag Promotion.
	 * 
	 * @param pPromotion
	 *            - Repository Item(Promotion Status/Promotion)
	 * @param pNow
	 *   		  - Date Object
	 * @return - boolean.
	 */
	@Override
	public boolean checkPromotionExpiration(RepositoryItem pPromotion, Date pNow) {
		if (isLoggingDebug()) {
			logDebug("Entering into Exit from [Class: TRUPromotionTools  method: checkPromotionExpiration]");
			vlogDebug("pPromotion:{0} pNow : {1}", pPromotion,pNow);
		}
		boolean isPromotionExpired = Boolean.FALSE;
		TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPricingModelProperties();
		RepositoryItem promoStatus = null;
		try {
			if(pPromotion != null && pPromotion.getItemDescriptor().getItemDescriptorName().equals(getPromoStatusDescriptorName())){
				promoStatus = pPromotion;
				pPromotion = (RepositoryItem)promoStatus.getPropertyValue(getPromoStatusPromoProperty());
				Object tagPromo = pPromotion.getPropertyValue(pricingModelProperties.getTagPromotionPropertyName());
				if(tagPromo != null){
					isPromotionExpired = (boolean) tagPromo;
				}
				if(isLoggingDebug()){
					vlogDebug("pPromotion:{0} isPromotionExpired : {1}", pPromotion,isPromotionExpired);
				}
				if(isPromotionExpired){
					return isPromotionExpired;
				}
			}
		} catch (RepositoryException e) {
			if(isLoggingError()){
				vlogError("RepositoryException in TRUPromotionTools", e);
			}
		}
		isPromotionExpired= super.checkPromotionExpiration(pPromotion, pNow);
		if (isLoggingDebug()) {
			vlogDebug("isPromotionExpired:{0}", isPromotionExpired);
			logDebug("Exit from [Class: TRUPromotionTools  method: checkPromotionExpiration]");
		}
		return isPromotionExpired;
	}


	/**
	 * @return the tenderTypePromotionItemType
	 */
	public String getTenderTypePromotionItemType() {
		return mTenderTypePromotionItemType;
	}


	/**
	 * @param pTenderTypePromotionItemType the tenderTypePromotionItemType to set
	 */
	public void setTenderTypePromotionItemType(String pTenderTypePromotionItemType) {
		mTenderTypePromotionItemType = pTenderTypePromotionItemType;
	}


	/**
	 * @return the setPromotionId
	 */
	public boolean isSetPromotionId() {
		return mSetPromotionId;
	}


	/**
	 * @param pSetPromotionId the mSetPromotionId to set
	 */
	public void setSetPromotionId(boolean pSetPromotionId) {
		mSetPromotionId = pSetPromotionId;
	}
	
	/**
	 * Method to return the List of Excluded SKU Ids.
	 * 
	 * @param pPromotion - Promotion Repository Item
	 * @return List - List of Bulk Excluded SKU ids.
	 */
	public List<String> getExcludedBulkSkuIds(RepositoryItem pPromotion){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: getExcludedBulkSkuIds]");
			vlogDebug("pPromotion : {0}", pPromotion);
		}
		if(pPromotion == null){
			return null;
		}
		TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPricingModelProperties();
		String bulkSkus = (String) pPromotion.getPropertyValue(pricingModelProperties.getBulkExcludeSkusPropertyName());
		if (isLoggingDebug()) {
			vlogDebug("bulkSkus IDS : {0}", bulkSkus);
		}
		String[] skuIds = null;
		if(StringUtils.isNotBlank(bulkSkus)){
			skuIds = bulkSkus.split(TRUCommerceConstants.COMA_SYMBOL);
		}
		if(skuIds == null || skuIds.length <= TRUConstants.ZERO){
			return null;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPromotionTools  method: getExcludedBulkSkuIds]");
		}
		return Arrays.asList(skuIds);
	}
	
	
	/**
	 * Gets the qualified sk ufor promo from catalog.
	 *
	 * @param pOrder the order
	 * @param pPricingModel the pricing model
	 * @param pQualifiedSKUforPromo the qualified sk ufor promo
	 * @param pExcludedSKUforPromo the excluded sk ufor promo
	 * 
	 */
	@SuppressWarnings("unchecked")
	public void getQualifiedSKUforPromoFromCatalog(Order pOrder, RepositoryItem pPricingModel,List<RepositoryItem> pQualifiedSKUforPromo,
			List<RepositoryItem> pExcludedSKUforPromo) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: getQualifiedSKUforPromoFromCatalog]");
			vlogDebug("pPromotion : {0}", pPricingModel);
		}
		Set<RepositoryItem> itemsFromOrder = new HashSet<RepositoryItem>();
		List<CommerceItem> items = pOrder.getCommerceItems();
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getPricingTools().getOrderTools().getCatalogTools().getCatalogProperties();
		for (CommerceItem item : items) {
			if (item instanceof TRUDonationCommerceItem) {
				continue;
			}
			if (null != item.getAuxiliaryData().getCatalogRef()) {
				itemsFromOrder.add((RepositoryItem) item.getAuxiliaryData().getCatalogRef());
			}
		}
		TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPricingModelProperties();
		if ((null == pPricingModel.getPropertyValue(pricingModelProperties.getQualifyingProductsPropertyName())
				&& null == pPricingModel.getPropertyValue(pricingModelProperties.getQualifyingCategoriesPropertyName())
				&& null == pPricingModel.getPropertyValue(pricingModelProperties.getExcludedCategoriesPropertyName())
				&& null == pPricingModel.getPropertyValue(pricingModelProperties.getExcludedProductsPropertyName())
				&& null == pPricingModel.getPropertyValue(pricingModelProperties.getQualifyingSkusPropertyName())
				&& null == pPricingModel.getPropertyValue(pricingModelProperties.getExcludedSkusPropertyName())
				&& null == pPricingModel.getPropertyValue(pricingModelProperties.getBulkQualifyingSkusPropertyName())
				&& null == pPricingModel.getPropertyValue(pricingModelProperties.getBulkExcludeSkusPropertyName()))||(
				 ((List<RepositoryItem>) pPricingModel.getPropertyValue(pricingModelProperties.getQualifyingProductsPropertyName())).isEmpty()
				&& ((List<RepositoryItem>) pPricingModel.getPropertyValue(pricingModelProperties.getQualifyingCategoriesPropertyName())).isEmpty()
				&& ((List<RepositoryItem>) pPricingModel.getPropertyValue(pricingModelProperties.getExcludedCategoriesPropertyName())).isEmpty()
				&& ((List<RepositoryItem>) pPricingModel.getPropertyValue(pricingModelProperties.getExcludedProductsPropertyName())).isEmpty()
				&& ((List<RepositoryItem>) pPricingModel.getPropertyValue(pricingModelProperties.getQualifyingSkusPropertyName())).isEmpty()
				&& ((List<RepositoryItem>) pPricingModel.getPropertyValue(pricingModelProperties.getExcludedSkusPropertyName())).isEmpty()&&
				StringUtils.isBlank((String)pPricingModel.getPropertyValue(pricingModelProperties.getBulkExcludeSkusPropertyName()))&&
				StringUtils.isBlank((String)pPricingModel.getPropertyValue(pricingModelProperties.getBulkQualifyingSkusPropertyName()))
				)) {
			
			pQualifiedSKUforPromo.addAll(itemsFromOrder);
			if (isLoggingDebug()) {
				vlogDebug(" All the skus qualified for the promotion ::pPricingModel:{0}  pQualifiedSKUforPromo : {1}", pPricingModel ,pQualifiedSKUforPromo);
			}
			return;
		}
		for (RepositoryItem skuItem : itemsFromOrder) {
			List<String> promos = null;
			if (null != skuItem) {
				promos = (List<String>) skuItem.getPropertyValue(catalogProperties.getSkuQualifiedForPromos());
				boolean isPromotionAvailable = false;
				if (promos != null && !promos.isEmpty()) {
					for (String lPromoId : promos) {
						int indexOf = lPromoId.indexOf(TRUCommerceConstants.PIPELINE);
						String promoId = lPromoId.substring(0, indexOf);
						if (null != promoId && promoId.equals(pPricingModel.getRepositoryId())) {
							isPromotionAvailable = true;
							break;
						}
					}
					if (isPromotionAvailable) {
						pQualifiedSKUforPromo.add(skuItem);
					} else {
						pExcludedSKUforPromo.add(skuItem);
					}
				} else {
					pExcludedSKUforPromo.add(skuItem);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("Qualified and Excluded SKUs for  promotion ::  pPricingModel:{0}  pQualifiedSKUforPromo : {1} pExcludedSKUforPromo{2}", pPricingModel,pQualifiedSKUforPromo,pExcludedSKUforPromo);
			vlogDebug("Exiting from [Class: TRUPromotionTools  method: getQualifiedSKUforPromoFromCatalog]");
		}
	}
	
	/**
	 * Gets the promotion details for sku.
	 *
	 * @param pId the id
	 * @param pPromotionId the promotion id
	 * @param pIsProration the is proration
	 * @return the promotion details for sku
	 */
	public TRUPromoContainer getPromotionDetailsForSKU(String pId,String pPromotionId ,boolean pIsProration) {
		TRUPromoContainer promoContainer=null;
		List<TRUPromoVO> promoVOs=null;
		String[] promoSplit = null;
		TRUPromoVO promoObject = null;
		if(StringUtils.isNotEmpty(pId)){
			List<String> promoDetails=populatePromotionData(pId);
			if(null!= promoDetails&&promoDetails.size()>TRUCommerceConstants.INT_ZERO){
				promoContainer=new TRUPromoContainer();
				promoContainer.setSkuId(pId);
				List<String> eligiblePromos = new ArrayList<String>();
				List<String> beginUsablePromos = new ArrayList<String>();
				List<String> endUsablePromos = new ArrayList<String>();
				eligiblePromos.addAll(promoDetails);
				for(String eligiblePromo : eligiblePromos){
					boolean checkbeginUsable = checkbeginUsable(eligiblePromo);
					if(checkbeginUsable){
						beginUsablePromos.add(eligiblePromo);
					}
				}
				if (isLoggingDebug()) {
						vlogDebug("beginUsablePromos : {0}", beginUsablePromos.toString());
				}
				for(String endUsablePromo : beginUsablePromos){
					boolean checkEndUsable = checkEndUsable(endUsablePromo);
					if(checkEndUsable){
						endUsablePromos.add(endUsablePromo);
					}
				}
				if (isLoggingDebug()) {
					vlogDebug("endUsablePromos : {0}", endUsablePromos.toString());
			    }
				if (!endUsablePromos.isEmpty() && endUsablePromos.size() > TRUCommerceConstants.INT_ZERO) {
					promoVOs = new ArrayList<TRUPromoVO>();
					if (pPromotionId != null && !pPromotionId.isEmpty()) {
						for (String endUsablePromo : endUsablePromos) {
							promoSplit = endUsablePromo.split(TRUCommerceConstants.PIPELINE_SEPERATOR);
							if (pPromotionId.equals(promoSplit[TRUCommerceConstants.INT_ZERO])) {
								promoObject = new TRUPromoVO();
								promoObject.setPromoId(promoSplit[TRUCommerceConstants.INT_ZERO]);
								promoObject.setDetails(promoSplit[TRUCommerceConstants.INT_ONE]);
								promoObject.setStartDate(promoSplit[TRUCommerceConstants.INT_TWO]);
								promoObject.setEndDate(promoSplit[TRUCommerceConstants.INT_THREE]);
								promoObject.setPriority(Integer.parseInt(promoSplit[TRUCommerceConstants.INT_FOUR]));
								int webDisplay=Integer.parseInt(promoSplit[TRUCommerceConstants.INT_FIVE]);
								if(webDisplay==TRUCommerceConstants.INT_ONE||pIsProration){
									promoVOs.add(promoObject);
								}
							}
						}
					} else {
						for (String endUsablePromo : endUsablePromos) {
							promoSplit = endUsablePromo.split(TRUCommerceConstants.PIPELINE_SEPERATOR);
							promoObject = new TRUPromoVO();
							promoObject.setPromoId(promoSplit[TRUCommerceConstants.INT_ZERO]);
							promoObject.setDetails(promoSplit[TRUCommerceConstants.INT_ONE]);
							promoObject.setStartDate(promoSplit[TRUCommerceConstants.INT_TWO]);
							promoObject.setEndDate(promoSplit[TRUCommerceConstants.INT_THREE]);
							promoObject.setPriority(Integer.parseInt(promoSplit[TRUCommerceConstants.INT_FOUR]));
							int webDisplay=Integer.parseInt(promoSplit[TRUCommerceConstants.INT_FIVE]);
							if(webDisplay==TRUCommerceConstants.INT_ONE||pIsProration){
								promoVOs.add(promoObject);
							}
						}

					}
				}
				promoContainer.setPromotions(promoVOs);
				return promoContainer;
			}
			
		}
		return null;
		
	}
	
	/**
	 * Populate promotion data.
	 *
	 * @param pSkuId the sku id
	 * @return the list
	 */
	public List<String> populatePromotionData(String pSkuId){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUSkuPromotionDetails  method: populatePromotionData]");
		}
		List<String> promoitonDetails= null;
		if(StringUtils.isNotEmpty(pSkuId)){
			Connection connection = null;
			String query = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			String promotionData = null;
			int i=TRUCommerceConstants.INT_ONE;
			try {
				connection = getDataSource().getConnection();
				if (connection != null) {
					query = getPromotionQuery();
					if (!StringUtils.isEmpty(query)) {
						ps = connection.prepareStatement(query);
						ps.setString(TRUCommerceConstants.INT_ONE, pSkuId);
						if (ps != null) {
							rs = ps.executeQuery();
							if (rs != null) {
								promoitonDetails=new ArrayList<String>();
								while (rs.next()) {
									promotionData = rs.getString(i);
									if(promotionData != null){
										promoitonDetails.add(promotionData);
									}
								}
								return promoitonDetails;
							}
						}
					}
				}
			} catch (SQLException sQLException) {
				vlogError("SQLException : {0}", sQLException);
			} finally {
				try {
					if (ps != null) {
						ps.close();
					}
					if (connection != null) {
						connection.close();
					}
				} catch (SQLException sQLException) {
					vlogError("SQLException : {0}", sQLException);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit into [Class: TRUSkuPromotionDetails  method: populatePromotionData]");
		}
		return null;
	}
	
	/**
	 * This method is to get promotion Item
	 *
	 * @param pPromotionId - promotion Id
	 * @return promotionItem - Promotion Item
	 */
	public RepositoryItem getPromotionFromPromoId(String pPromotionId){
		RepositoryItem promotion = null;
		if(StringUtils.isBlank(pPromotionId)){
			return promotion;
		}
		try {
			promotion = getPromotions().getItem(pPromotionId, getBasePromotionItemType());
		} catch (RepositoryException e) {
			vlogError("RepositoryException : {0}", e);
		}
		return promotion;
	}
	
	/** This holds the reference for mLoadPricingEngine *. */
	private boolean mLoadPricingEngine;
	/**
	 * @return the loadPricingEngine
	 */
	public boolean isLoadPricingEngine() {
		return mLoadPricingEngine;
	}
	/**
	 * @param pLoadPricingEngine the loadPricingEngine to set
	 */
	public void setLoadPricingEngine(boolean pLoadPricingEngine) {
		mLoadPricingEngine = pLoadPricingEngine;
	}
	
	/**
	 * @return the budgetPromoDisableMessageSource
	 */
	public TRUBudgetPromoDisableMessageSource getBudgetPromoDisableMessageSource() {
		return mBudgetPromoDisableMessageSource;
	}

	/**
	 * @param pBudgetPromoDisableMessageSource the budgetPromoDisableMessageSource to set
	 */
	public void setBudgetPromoDisableMessageSource(TRUBudgetPromoDisableMessageSource pBudgetPromoDisableMessageSource) {
		mBudgetPromoDisableMessageSource = pBudgetPromoDisableMessageSource;
	}
	/**
	 * @return the promotionQuery
	 */
	public String getPromotionQuery() {
		return mPromotionQuery;
	}

	/**
	 * @param pPromotionQuery the promotionQuery to set
	 */
	public void setPromotionQuery(String pPromotionQuery) {
		mPromotionQuery = pPromotionQuery;
	}
	
	/**
	 * Method to remove the coupon from Profile.
	 * @param pMutProfile - MutableRepositoryItem Object
	 * @param pPromotion - RepositoryItem - Promotion Item.
	 * @param pRemoveAll - boolean.
	 * @param pCurrentCoupon - RepositoryItem - Coupon Repository Item.
	 * @return boolean.
	 */
	public boolean removeCoupon(MutableRepositoryItem pMutProfile,
			RepositoryItem pPromotion, boolean pRemoveAll, RepositoryItem pCurrentCoupon) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: removeCoupon]");
			vlogDebug("pMutProfile : {0} pPromotion : {1}", pMutProfile,pPromotion);
			vlogDebug("pRemoveAll : {0} pCurrentCoupon : {1}", pRemoveAll,pCurrentCoupon);
		}
		boolean result = false;
	    try {
	      result = revokeCoupon(pMutProfile, pPromotion, pRemoveAll,pCurrentCoupon);
	    }
	    catch (PromotionException pe) {
	      if(isLoggingError()){
	    	  vlogError("RepositoryException : {0}", pe);
	      }
	      return false;
	    }
	    if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPromotionTools  method: removeCoupon]");
		}
	    return result;
	}


	/**
	 * Method to remoke the coupon from Profile.
	 * @param pMutProfile - MutableRepositoryItem Object
	 * @param pPromotion - RepositoryItem - Promotion Item.
	 * @param pRemoveAll - boolean.
	 * @param pCurrentCoupon - RepositoryItem - Coupon Repository Item.
	 * @return boolean.
	 * @throws PromotionException - If any
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean revokeCoupon(MutableRepositoryItem pMutProfile,
			RepositoryItem pPromotion, boolean pRemoveAll,
			RepositoryItem pCurrentCoupon) throws PromotionException{
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: revokeCoupon]");
			vlogDebug("pMutProfile : {0} pPromotion : {1}", pMutProfile,pPromotion);
			vlogDebug("pRemoveAll : {0} pCurrentCoupon : {1}", pRemoveAll,pCurrentCoupon);
		}
		if (pMutProfile == null) {
			String msg = ResourceUtils.getMsgResource(TRUConstants.NULL_PROFILE, TRUConstants.PROMOTION_RESOURCES, sResourceBundle);
		    throw new PromotionException(TRUConstants.NULL_PROFILE, msg);
		}
		if (pPromotion == null) {
			String msg = ResourceUtils.getMsgResource(TRUConstants.NULL_PROMTION, TRUConstants.PROMOTION_RESOURCES, sResourceBundle);
			throw new PromotionException(TRUConstants.NULL_PROMTION, msg);
		}
		boolean result = false;
		Object property = pMutProfile.getPropertyValue(getActivePromotionsProperty());
		if ((property != null) && (property instanceof List)) {
			List promotions = (List)property;
		    ListIterator it = promotions.listIterator();
		    MutableRepositoryItem promoStatus = null;
		    RepositoryItem promo = null;
		    List<RepositoryItem> couponItems = null;
		    do{
		    	while(it.hasNext()){
		    		promoStatus = (MutableRepositoryItem)it.next();
		    		if(promoStatus == null){
		    			String[] msgArgs = { pMutProfile.getRepositoryId() };
			            if (isLoggingError()){
			            logError(ResourceUtils.getMsgResource("nullActivePromotion", "atg.commerce.promotion.PromotionResources", sResourceBundle, msgArgs));
			            }
		    		}
		    		promo = (RepositoryItem)promoStatus.getPropertyValue(getPromoStatusPromoProperty());
		    		if(promo == null){
		    			it.remove();
		    			continue;
		    		}
		    		couponItems = (List<RepositoryItem>) promoStatus.getPropertyValue(getPromoStatusCouponsPropertyName());
		    		if(couponItems != null && couponItems.contains(pCurrentCoupon)){
		    			it.remove();
		    			break;
		    		}
		    	}
		    	result = Boolean.TRUE;
		    }while(pRemoveAll);
		    if (result) {
		    	pMutProfile.setPropertyValue(getActivePromotionsProperty(), promotions);
		        sendPromotionRevokedEvent(pMutProfile, pPromotion);
		    }
		    else if (isLoggingDebug()) {
		    	logDebug("Promotion was not found in the " + getActivePromotionsProperty() + " of the profile");
		    }
		}
		else if (property == null) {
			if (isLoggingDebug()) {
		        logDebug("Promotion not removed because the profile's promotions property is null");
		    }
		}
		else if (isLoggingError()) {
			logError("The " + getActivePromotionsProperty() + " property of the profile is not null and is not a List");
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPromotionTools  method: revokeCoupon]");
		}
		return result;
	}

	/**
	 * Gets the number of items for promotion.
	 *
	 * @param pPromotionId the promotion id
	 * @param pProperty the property
	 * @return the number of items for promotion
	 */
	public String getTemplateValuesForPromotion(String pPromotionId ,String pProperty) {
		if (StringUtils.isNotBlank(pPromotionId)) {
			Connection connection = null;
			String query = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				connection = getDataSource().getConnection();
				if (connection != null) {
					query = getNoOfItemsQuery();
					if (!StringUtils.isEmpty(query)) {
						ps = connection.prepareStatement(query);
						ps.setString(TRUCommerceConstants.INT_ONE, pPromotionId);
						ps.setString(TRUCommerceConstants.INT_TWO, pProperty);
						if (ps != null) {
							rs = ps.executeQuery();
							if (rs != null) {
								while (rs.next()) {
									return rs.getString(TRUCommerceConstants.INT_ONE);
								}
							}
						}
					}
				}
			} catch (SQLException sQLException) {
				vlogError("SQLException : {0}", sQLException);
			} finally {
				try {
					if (ps != null) {
						ps.close();
					}
					if (connection != null) {
						connection.close();
					}
				} catch (SQLException sQLException) {
					vlogError("SQLException : {0}", sQLException);
				}
			}
		}
		return null;
	}


	/**
	 * Gets the no of items query.
	 *
	 * @return the no of items query
	 */
	public String getNoOfItemsQuery() {
		return mNoOfItemsQuery;
	}


	/**
	 * Sets the no of items query.
	 *
	 * @param pNoOfItemsQuery the new no of items query
	 */
	public void setNoOfItemsQuery(String pNoOfItemsQuery) {
		this.mNoOfItemsQuery = pNoOfItemsQuery;
	}
	
	/**
	 * Method has been overridden to add the flag if promo status is created for SUNC code.
	 *
	 * @param pProfile - Profile Repository Item
	 * @param pPromotion - Promotion Repository Item
	 * @param pNumUses - Integer Number of Uses
	 * @param pCoupon - Coupon Repository Item
	 * @return RepositoryItem - Promotion Status Repository Item
	 * @throws - Repository Exception if any.
	 */	
	@Override
	public RepositoryItem createPromotionStatus(RepositoryItem pProfile,
			RepositoryItem pPromotion, Integer pNumUses, RepositoryItem pCoupon)
			throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: createPromotionStatus]");
			vlogDebug("pProfile : {0}  pPromotion : {1}", pProfile,pPromotion);
			vlogDebug("pNumUses : {0}  pCoupon : {1}", pNumUses,pCoupon);
		}
		// Calling Super Method
		TRUPropertyManager propertyManager = ((TRUOrderManager)getPricingTools().getOrderManager()).getPropertyManager();
		// Calling super method to create the Promotion Status.
		MutableRepositoryItem promoStatus = (MutableRepositoryItem) super.createPromotionStatus(pProfile, pPromotion, pNumUses, pCoupon);
		// Start - Below logic to add boolean flag as true in status for Single used coupons.
		if(pProfile != null && pProfile.getPropertyValue(propertyManager.getSucnCodePropertyName()) != null){
			promoStatus.setPropertyValue(propertyManager.getAppliedCodePropertyName(), Boolean.TRUE);
			promoStatus.setPropertyValue(propertyManager.getAppliedCodeValuePropertyName(), 
					(String)pProfile.getPropertyValue(propertyManager.getSucnCodePropertyName()));
		}
		// END.
		if(isLoggingDebug()){
			vlogDebug("promoStatus : {0}", promoStatus);
			logDebug("END : TRUPromotionTools createPromotionStatus()");
		}
		return promoStatus;
	}


	/**
	 * Method is created to revoke the single used coupons.
	 * @return isValid - isValid
	 * @param pProfile - Profile Repository Item.
	 * @throws RepositoryException - If any
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean revokeSingleUseCoupons(RepositoryItem pProfile) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: revokeSingleUseCoupons]");
			vlogDebug("pProfile : {0}", pProfile);
		}
		boolean isValid = Boolean.FALSE;
		if(pProfile == null){
			return isValid;
		}
	    List<RepositoryItem> activePromotions = (List<RepositoryItem>)pProfile.getPropertyValue(getActivePromotionsProperty());
	    MutableRepositoryItem profile = getProfileRepository().getItemForUpdate(pProfile.getRepositoryId(), getProfileItemType());
	    if (isLoggingDebug()) {
			vlogDebug("activePromotions : {0}", activePromotions);
		}
	    TRUPropertyManager propertyManager = ((TRUOrderManager)getPricingTools().getOrderManager()).getPropertyManager();
	    List<RepositoryItem> removablePromotionStatus= null;
	    if(activePromotions != null && !activePromotions.isEmpty()){
	    	removablePromotionStatus = new ArrayList<RepositoryItem>();
	    	Iterator statusIterator = activePromotions.iterator();	
			while (statusIterator.hasNext()){
				RepositoryItem promotionStatus = (RepositoryItem)statusIterator.next();
			    if(promotionStatus.getPropertyValue(propertyManager.getAppliedCodePropertyName()) != null &&
			    		(boolean)promotionStatus.getPropertyValue(propertyManager.getAppliedCodePropertyName())){
			    	removablePromotionStatus.add(promotionStatus);
			    }
			}
	    }
	    if (isLoggingDebug()) {
			vlogDebug("removablePromotionStatus : {0}", removablePromotionStatus);
		}
		if(removablePromotionStatus != null && !removablePromotionStatus.isEmpty()){
			isValid = Boolean.TRUE;
			activePromotions.removeAll(removablePromotionStatus);
		}
		// Setting the Active promotion and sucn atg code property to profile.
		profile.setPropertyValue(getActivePromotionsProperty(), activePromotions);
		//profile.setPropertyValue(propertyManager.getSucnAtgCodesPropertyName(), null);
		if(isLoggingDebug()){
			logDebug("END : TRUPromotionTools revokeSingleUseCoupons()");
		}
		return isValid;
	}
	
	/**
	 * Method has been overridden check to check If promotion is applied
	 * multiple times for Single Used coupon then return false. 
	 *
	 * @param pPromotion - Promotion Repository Item.
	 *  @param pPromotionStatuses - Collection of Status Repository Item.
	 * @return boolean - true/false
	 */	
	@SuppressWarnings("rawtypes")
	@Override
	public boolean isPromotionInPromotionStatuses(RepositoryItem pPromotion,
			Collection pPromotionStatuses) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: isPromotionInPromotionStatuses]");
			vlogDebug("pPromotion : {0} pPromotionStatuses : {1}", pPromotion,pPromotionStatuses);
		}
		TRUPropertyManager propertyManager = ((TRUOrderManager)getPricingTools().getOrderManager()).getPropertyManager();
		Iterator statusIterator = pPromotionStatuses.iterator();
	    while (statusIterator.hasNext())
	    {
	      RepositoryItem promotionStatus = (RepositoryItem)statusIterator.next();
	      if (promotionStatus != null)
	      {
	    	// Added below condition to add the same coupon for loyalty coupon implementation.
	    	if(promotionStatus.getPropertyValue(propertyManager.getAppliedCodePropertyName())  != null &&
	    			(boolean)promotionStatus.getPropertyValue(propertyManager.getAppliedCodePropertyName())){
	    		return false;
	    	}
	    	// END.
	        RepositoryItem promotionStatusPromotion = (RepositoryItem)promotionStatus.getPropertyValue(getPromoStatusPromoProperty());
	        if ((promotionStatusPromotion != null) && (promotionStatusPromotion.getRepositoryId().equals(pPromotion.getRepositoryId())))
	        {
	          return true;
	        }
	      }
	    }
	    if(isLoggingDebug()){
			logDebug("END : TRUPromotionTools isPromotionInPromotionStatuses()");
		}
	    return false;
	}
	
	/**
	 * Method is created to revoke the single used coupons.
	 * 
	 * @param pProfile - Profile Repository Item.
	 * @param pOrderCoupons - Map of Order Coupons
	 * @return boolean - flag to check if scoupons are removed or not.
	 * @throws RepositoryException - If any
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean removeSUCNCouponFromProfile(RepositoryItem pProfile, Map<String,String> pOrderCoupons) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPromotionTools  method: revokeSingleUseCoupons]");
			vlogDebug("pProfile : {0} pOrderCoupons : {1}", pProfile,pOrderCoupons);
		}
		boolean isValid = Boolean.FALSE;
		if(pProfile == null){
			return isValid;
		}
	    List<RepositoryItem> activePromotions = (List<RepositoryItem>)pProfile.getPropertyValue(getActivePromotionsProperty());
	    MutableRepositoryItem profile = getProfileRepository().getItemForUpdate(pProfile.getRepositoryId(), getProfileItemType());
	    if (isLoggingDebug()) {
			vlogDebug("activePromotions : {0}", activePromotions);
		}
	    boolean isOrderCouponEmpty = Boolean.FALSE;
	    if(pOrderCoupons == null || pOrderCoupons.isEmpty()){
	    	isOrderCouponEmpty = Boolean.TRUE;
		}
	    TRUPropertyManager propertyManager = ((TRUOrderManager)getPricingTools().getOrderManager()).getPropertyManager();
	    List<RepositoryItem> removablePromotionStatus= null;
	    if(activePromotions != null && !activePromotions.isEmpty()){
	    	removablePromotionStatus = new ArrayList<RepositoryItem>();
	    	Iterator statusIterator = activePromotions.iterator();	
			while (statusIterator.hasNext()){
				RepositoryItem promotionStatus = (RepositoryItem)statusIterator.next();
				Object isCodeApplied = promotionStatus.getPropertyValue(propertyManager.getAppliedCodePropertyName());
				Object appliedCodeValue = promotionStatus.getPropertyValue(propertyManager.getAppliedCodeValuePropertyName());
				if (isLoggingDebug()) {
					vlogDebug("isCodeApplied : {0} appliedCodeValue : {1}", isCodeApplied,appliedCodeValue);
				}
				if(isCodeApplied == null || appliedCodeValue == null){
					continue;
				}
				if(isOrderCouponEmpty){
					if((boolean)isCodeApplied && StringUtils.isNotBlank((String)appliedCodeValue)){
						removablePromotionStatus.add(promotionStatus);
					}
				}else{
					if((boolean)isCodeApplied && !pOrderCoupons.containsKey((String)appliedCodeValue)){
						removablePromotionStatus.add(promotionStatus);
					}
				}
			}
	    }
	    if (isLoggingDebug()) {
			vlogDebug("removablePromotionStatus : {0}", removablePromotionStatus);
		}
		if(removablePromotionStatus != null && !removablePromotionStatus.isEmpty()){
			isValid = Boolean.TRUE;
			activePromotions.removeAll(removablePromotionStatus);
		}
		// Setting the Active promotion and sucn atg code property to profile.
		profile.setPropertyValue(getActivePromotionsProperty(), activePromotions);
		if(isLoggingDebug()){
			logDebug("END : TRUPromotionTools revokeSingleUseCoupons()");
		}
		return isValid;
	}
}
