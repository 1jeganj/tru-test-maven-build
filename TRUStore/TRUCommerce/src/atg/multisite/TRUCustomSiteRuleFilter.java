package atg.multisite;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.common.TRUConfiguration;


/**
 * The Class TRUCustomSiteRuleFilter.
 * @version 1.0
 * @author PA
 */
public class TRUCustomSiteRuleFilter extends GenericService implements SiteContextRuleFilter {

	/**  
	 * Property to hold mEnabled. 
	 * 
	 */
	private boolean mEnabled;

	/**
	 * This property hold reference for mEnableSessionStickySite.
	 */
	private boolean mEnableSessionStickySite;

	/**
	 * This property hold reference for mEnableSiteIdentificationViaCookie.
	 */
	private boolean mEnableSiteIdentificationViaCookie;

	/**  
	 * Property to hold mCatalogTools. 
	 * 
	 */
	private TRUCatalogTools mCatalogTools;

	/**
	 * Property to mSiteSessionManagerPath.
	 */
	private String mSiteSessionManagerPath;

	/**
	 * Property to mCollectionPageUrl.
	 */
	private String mCollectionPageUrl;

	/**
	 * Property to mProductIdQueryParameter.
	 */
	private String mProductIdQueryParameter;

	/**
	 * Property to mCategoryIdQueryParameter.
	 */
	private String mCategoryIdQueryParameter;

	/**
	 * Property to mCustomSiteQueryParameter.
	 */
	private String mCustomSiteQueryParameter;

	/**
	 * This property holds CatalogProperties variable mCatalogProperties.
	 */
	private TRUCatalogProperties mCatalogProperties;

	/** 
	 * property to hold tRUConfiguration. 
	 */
	private TRUConfiguration mTruConfiguration;

	/**
	 * Property to Hold siteSpecificDomainUrlMap.
	 */
	private Map<String, String> mSiteSpecificDomainUrlMap;
	
	/**  
	 * Property to hold mIgnoreRequestURIPattern. 
	 * 
	 */
	private Pattern mIgnoreRequestURIPattern;
	
	/**
	 * Gets the IgnoreRequestURIPattern.
	 *
	 * @return the mIgnoreRequestURIPattern
	 */
	public Pattern getIgnoreRequestURIPattern() {
		return mIgnoreRequestURIPattern;
	}

	/**
	 * Sets the Ignore Request URI Pattern.
	 *
	 * @param pIgnoreRequestURIPattern the Ignore Request URI Pattern
	 */
	public void setIgnoreRequestURIPattern(
			Pattern pIgnoreRequestURIPattern) {
		this.mIgnoreRequestURIPattern = pIgnoreRequestURIPattern;
	}

	/**
	 * Gets the SiteSpecificDomainUrlMap.
	 *
	 * @return the mSiteSpecificDomainUrlMap
	 */
	public Map<String, String> getSiteSpecificDomainUrlMap() {
		return mSiteSpecificDomainUrlMap;
	}

	/**
	 * Sets the Site Specific Domain Url Map.
	 *
	 * @param pSiteSpecificDomainUrlMap the Site Specific Domain Url Map
	 */
	public void setSiteSpecificDomainUrlMap(
			Map<String, String> pSiteSpecificDomainUrlMap) {
		this.mSiteSpecificDomainUrlMap = pSiteSpecificDomainUrlMap;
	}

	/**
	 * @return the mCatalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * @param pCatalogProperties the mCatalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		this.mCatalogProperties = pCatalogProperties;
	}

	/**
	 * Gets the SiteSessionManagerPath.
	 * @return the SiteSessionManagerPath
	 */
	public String getSiteSessionManagerPath() {
		return mSiteSessionManagerPath;
	}

	/**
	 * Sets the SiteSessionManagerPath.
	 * @param pSiteSessionManagerPath the SiteSessionManagerPath to set
	 */
	public void setSiteSessionManagerPath(String pSiteSessionManagerPath) {
		mSiteSessionManagerPath = pSiteSessionManagerPath;
	}

	/**
	 * Gets the CollectionPageUrl.
	 * @return the CollectionPageUrl
	 */
	public String getCollectionPageUrl() {
		return mCollectionPageUrl;
	}

	/**
	 * Sets the CollectionPageUrl.
	 * @param pCollectionPageUrl the CollectionPageUrl to set
	 */
	public void setCollectionPageUrl(String pCollectionPageUrl) {
		mCollectionPageUrl = pCollectionPageUrl;
	}

	/**
	 * Gets the ProductIdQueryParameter.
	 * @return the ProductIdQueryParameter
	 */
	public String getProductIdQueryParameter() {
		return mProductIdQueryParameter;
	}

	/**
	 * Sets the ProductIdQueryParameter.
	 * @param pProductIdQueryParameter the ProductIdQueryParameter to set
	 */
	public void setProductIdQueryParameter(String pProductIdQueryParameter) {
		mProductIdQueryParameter = pProductIdQueryParameter;
	}

	/**
	 * Gets the CategoryIdQueryParameter.
	 * @return the CategoryIdQueryParameter
	 */
	public String getCategoryIdQueryParameter() {
		return mCategoryIdQueryParameter;
	}

	/**
	 * Sets the CategoryIdQueryParameter.
	 * @param pCategoryIdQueryParameter the CategoryIdQueryParameter to set
	 */
	public void setCategoryIdQueryParameter(String pCategoryIdQueryParameter) {
		mCategoryIdQueryParameter = pCategoryIdQueryParameter;
	}

	/**
	 * Gets the CustomSiteQueryParameter.
	 * @return the CustomSiteQueryParameter
	 */
	public String getCustomSiteQueryParameter() {
		return mCustomSiteQueryParameter;
	}

	/**
	 * Sets the CustomSiteQueryParameter.
	 * @param pCustomSiteQueryParameter the CustomSiteQueryParameter to set
	 */
	public void setCustomSiteQueryParameter(String pCustomSiteQueryParameter) {
		mCustomSiteQueryParameter = pCustomSiteQueryParameter;
	}

	/**
	 * Sets the Enabled property.
	 *
	 * @param pEnabled the Enabled property to set
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	/**
	 * Gets the mEnabled.
	 *
	 * @return the mEnabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Gets the catalog tools.
	 *
	 * @return the truCatalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Sets the catalog tools.
	 *
	 * @param pCatalogTools the CatalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * Gets the mEnableSiteIdentificationViaCookie.
	 * 
	 * @return mEnableSiteIdentificationViaCookie
	 */
	public boolean isEnableSiteIdentificationViaCookie() {
		return mEnableSiteIdentificationViaCookie;
	}

	/**
	 * set the pEnableSiteIdentificationViaCookie.
	 *
	 * @param pEnableSiteIdentificationViaCookie the Enable Site Identification Via Cookie
	 */
	public void setEnableSiteIdentificationViaCookie(boolean pEnableSiteIdentificationViaCookie) {
		mEnableSiteIdentificationViaCookie = pEnableSiteIdentificationViaCookie;
	}


	/**
	 * Gets the mEnableSessionStickySite.
	 * 
	 * @return mEnableSessionStickySite
	 */
	public boolean isEnableSessionStickySite() {
		return mEnableSessionStickySite;
	}

	/**
	 * set the pEnableSessionStickySite.
	 *
	 * @param pEnableSessionStickySite the Enable Session Sticky Site
	 */
	public void setEnableSessionStickySite(boolean pEnableSessionStickySite) {
		mEnableSessionStickySite = pEnableSessionStickySite;
	}


	/**
	 * Gets the TRU configuration.
	 * 
	 * @return the tRUConfiguration
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * Sets the TRU configuration.
	 * 
	 * @param pTruConfiguration
	 *            the tRUConfiguration to set
	 */
	public void setTruConfiguration(TRUConfiguration pTruConfiguration) {
		mTruConfiguration = pTruConfiguration;
	}


	/**
	 * This filter() method will identify the site context and set it to the Site Session context.
	 * 
	 * @param pRequest
	 *            the Request parameter
	 * @param pSiteSessionManager
	 *             the Site Session Manager
	 * 
	 * @return the site id              
	 */
	public String filter(DynamoHttpServletRequest pRequest, SiteSessionManager pSiteSessionManager) {

		if (isLoggingDebug()) {
			vlogDebug("Start ::: TRUCustomSiteRuleFilter :: filter method ");
		}
		if ((!(isEnabled())) || pRequest == null) {
			return null;
		}
		
		SiteSessionManager lSiteSessionManager = pSiteSessionManager;
		String siteId = null;
		String sessionSiteId = null;
		String cookieSiteId = null;

		String akamaiSiteCookie = pRequest.getCookieParameter(getTruConfiguration().getAkamaiSiteParam());
		if(StringUtils.isNotEmpty(akamaiSiteCookie) && isEnableSiteIdentificationViaCookie()) {
			cookieSiteId = getTruConfiguration().getCustomParamToSiteIdMapping().get(akamaiSiteCookie);
		}

		if(lSiteSessionManager == null && isEnableSessionStickySite()) {
			lSiteSessionManager = (SiteSessionManager)pRequest.resolveName(getSiteSessionManagerPath());
			if(lSiteSessionManager != null) {
				sessionSiteId = lSiteSessionManager.getStickySiteId();
			}
		}

		if(StringUtils.isNotEmpty(cookieSiteId)) {
			sessionSiteId = cookieSiteId;
		}

		String forceSiteId = getCustomSiteId(pRequest, sessionSiteId);
		if(StringUtils.isNotEmpty(forceSiteId)) {
			siteId = forceSiteId;
		} else if(StringUtils.isNotEmpty(sessionSiteId)) {
			siteId = sessionSiteId;
		}

		if(StringUtils.isNotEmpty(siteId) && lSiteSessionManager != null && isEnableSessionStickySite()) {
			lSiteSessionManager.setStickySiteId(siteId);
		}

		if (isLoggingDebug()) {
			vlogDebug("The value of Site is :: {0}", siteId);
			vlogDebug("END ::: TRUCustomSiteRuleFilter :: filter method");
		}
		return siteId;
	}

	/**
	 * This getCustomSiteId() method will identify site id to be applied based on custom rules.
	 * 
	 * @param pRequest
	 *            the Request parameter
	 * @param pSessionSiteId
	 *             the site id thart is store din session
	 * 
	 * @return the site id              
	 */
	@SuppressWarnings("unchecked")
	public String getCustomSiteId(DynamoHttpServletRequest pRequest, String pSessionSiteId) {

		if (isLoggingDebug()) {
			vlogDebug("Start ::: TRUCustomSiteRuleFilter :: getCustomSiteId method ");
		}
		
		String lSessionSiteId = pSessionSiteId;
		String forceSiteId = null;
		Set<String> siteIdList = null;
		String categoryId = pRequest.getQueryParameter(getCategoryIdQueryParameter());
		String productId = pRequest.getQueryParameter(getProductIdQueryParameter());
		String urlSiteId = pRequest.getQueryParameter(getCustomSiteQueryParameter());
		String registryParam = pRequest.getQueryParameter(TRUCommerceConstants.REGISTRY_REQ);

		if(StringUtils.isNotEmpty(urlSiteId) && getTruConfiguration().getCustomParamToSiteIdMapping() != null && getTruConfiguration().getCustomParamToSiteIdMapping().containsKey(urlSiteId)) {
			lSessionSiteId = getTruConfiguration().getCustomParamToSiteIdMapping().get(urlSiteId);
		}

		try { 
			if(StringUtils.isNotEmpty(categoryId)) {
				RepositoryItem categoryItem = getCatalogTools().findCategory(categoryId);
				if(categoryItem != null) {
					if(StringUtils.isNotEmpty(registryParam) && registryParam.equals(TRUCommerceConstants.ONE_STR) && categoryItem.getItemDescriptor().getItemDescriptorName().equals(getCatalogProperties().getRegistryClassificationItemName())) {
						List<RepositoryItem> crossReferenceList = (List<RepositoryItem>) categoryItem.getPropertyValue(getCatalogProperties().getCrossReferencePropertyName());
						if(crossReferenceList != null && !crossReferenceList.isEmpty()){
							for(RepositoryItem classificationItem : crossReferenceList){
								siteIdList = (Set<String>) classificationItem.getPropertyValue(getCatalogProperties().getSiteIDPropertyName());
							}
						}
					} else {
						siteIdList = (Set<String>) categoryItem.getPropertyValue(getCatalogProperties().getSiteIDPropertyName());
					}
				}
			} else if(StringUtils.isNotEmpty(productId)) {
				String requestUri = pRequest.getRequestURI();
				if(!StringUtils.isEmpty(requestUri) && requestUri.equals(getCollectionPageUrl())) {
					RepositoryItem productItem = getCatalogTools().findProduct(productId);
					if(productItem != null) {
						siteIdList = (Set<String>) productItem.getPropertyValue(getCatalogProperties().getSiteIDPropertyName());
					}
				} else {
					RepositoryItem[] skuList = getCatalogTools().getSKUFromOnlinePID(productId);
					if(skuList != null && skuList.length > 0) {
						RepositoryItem skuItem = skuList[0];
						if(skuItem != null) {
							siteIdList = (Set<String>) skuItem.getPropertyValue(getCatalogProperties().getSiteIDPropertyName());
						}
					}
				}
			}
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				vlogError(repositoryException, "RepositoryException occured in TRUCustomSiteRuleFilter :: getCustomSiteId method");
			}
		}

		if(siteIdList != null && !siteIdList.isEmpty()) {
			if(StringUtils.isNotEmpty(lSessionSiteId) && siteIdList.contains(lSessionSiteId)) {
				forceSiteId = lSessionSiteId;
			} else {
				for (String siteId : siteIdList) {
					forceSiteId = siteId;
					break;
				}
			}
		} else {
			forceSiteId = getSiteInfoForStaticPages(pRequest, lSessionSiteId);
		}

		if (isLoggingDebug()) {
			vlogDebug("END ::: TRUCustomSiteRuleFilter :: getCustomSiteId method ");
		}

		return forceSiteId;
	}

	/**
	 * This getCustomSiteId() method will identify site id to be applied based on custom rules.
	 * 
	 * @param pRequest
	 *            the Request parameter
	 * @param pSessionSiteId
	 *             the site id that is store din session
	 * 
	 * @return the site id              
	 */
	@SuppressWarnings("unchecked")
	public String getSiteInfoForStaticPages(DynamoHttpServletRequest pRequest, String pSessionSiteId) {

		if (isLoggingDebug()) {
			vlogDebug("Start ::: TRUCustomSiteRuleFilter :: getSiteInfoForStaticPages method ");
		}
		RepositoryItem[] activeSites = null;
		String customSiteId = null;
		String requestURI = pRequest.getRequestURI();

		boolean isEndecaRequest = false;
		if (StringUtils.isNotEmpty(requestURI) && getIgnoreRequestURIPattern() != null && !(getIgnoreRequestURIPattern().matcher(requestURI).matches())) {
			isEndecaRequest = true;
		}
		
		if(isEndecaRequest) {
			try {
				activeSites = SiteManager.getSiteManager().getActiveSites();
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					vlogError(repositoryException, "RepositoryException occured in TRUCustomSiteRuleFilter :: getSiteInfoForStaticPages method");
				}
			}
		}
		
		if(activeSites != null && activeSites.length > 0) {
			for(int i = 0; i < activeSites.length; i++) {
				RepositoryItem site = activeSites[i];
				List<String> customSiteUrls = (List<String>) site.getPropertyValue(getCatalogProperties().getSiteSpecificStaticPageUrlPropertyName());
				if(StringUtils.isNotEmpty(customSiteId)) {
					break;
				}
				if(customSiteUrls != null && !customSiteUrls.isEmpty()) {
					for(String customUrl : customSiteUrls) {
						if(StringUtils.isNotEmpty(customUrl) && requestURI.contains(customUrl)) {
							customSiteId = site.getRepositoryId();
							break;
						}
					}
				}
			}
		}

		if(StringUtils.isEmpty(customSiteId) && StringUtils.isNotEmpty(pSessionSiteId)) {
			customSiteId = pSessionSiteId;
		}
		
		if (isLoggingDebug()) {
			vlogDebug("END ::: TRUCustomSiteRuleFilter :: getSiteInfoForStaticPages method ");
		}

		return customSiteId;
	}


}
