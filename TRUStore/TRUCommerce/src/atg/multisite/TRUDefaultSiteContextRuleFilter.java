package atg.multisite;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;


/**
 * The Class TRUDefaultSiteContextRuleFilter.
 * @version 1.0
 * @author PA
 */
public class TRUDefaultSiteContextRuleFilter extends DefaultSiteContextRuleFilter {

	/**
	 * This property hold reference for mEnableSessionStickySite.
	 */
	private boolean mEnableSessionStickySite;

	/**
	 * Property to mSiteSessionManagerPath.
	 */
	private String mSiteSessionManagerPath;

	/**
	 * Gets the SiteSessionManagerPath.
	 * @return the SiteSessionManagerPath
	 */
	public String getSiteSessionManagerPath() {
		return mSiteSessionManagerPath;
	}

	/**
	 * Sets the SiteSessionManagerPath.
	 * @param pSiteSessionManagerPath the SiteSessionManagerPath to set
	 */
	public void setSiteSessionManagerPath(String pSiteSessionManagerPath) {
		mSiteSessionManagerPath = pSiteSessionManagerPath;
	}

	/**
	 * Gets the mEnableSessionStickySite.
	 * 
	 * @return mEnableSessionStickySite
	 */
	public boolean isEnableSessionStickySite() {
		return mEnableSessionStickySite;
	}

	/**
	 * set the pEnableSessionStickySite.
	 *
	 * @param pEnableSessionStickySite the Enable Session Sticky Site
	 */
	public void setEnableSessionStickySite(boolean pEnableSessionStickySite) {
		mEnableSessionStickySite = pEnableSessionStickySite;
	}

	/**
	 * This filter() method will identify the site context and set it to the Site Session context.
	 * 
	 * @param pRequest
	 *            the Request parameter
	 * @param pSiteSessionManager
	 *             the Site Session Manager
	 * 
	 * @return the site id              
	 */
	public String filter(DynamoHttpServletRequest pRequest, SiteSessionManager pSiteSessionManager) {

		if (isLoggingDebug()) {
			vlogDebug("Start ::: TRUDefaultSiteContextRuleFilter :: filter method ");
		}

		String siteId = super.filter(pRequest, pSiteSessionManager);

		if(isEnableSessionStickySite() && StringUtils.isNotEmpty(siteId) && pSiteSessionManager == null) {
			pSiteSessionManager = (SiteSessionManager)pRequest.resolveName(getSiteSessionManagerPath());
			if(pSiteSessionManager != null) {
				pSiteSessionManager.setStickySiteId(siteId);
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("The value of Site is :: {0}", siteId);
			vlogDebug("END ::: TRUDefaultSiteContextRuleFilter :: filter method");
		}
		return siteId;
	}

}
