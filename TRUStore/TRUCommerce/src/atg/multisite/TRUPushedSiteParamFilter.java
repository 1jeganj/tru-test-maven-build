package atg.multisite;

import java.util.ArrayList;
import java.util.Set;

import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;

import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;

/**
 * The Class TRUPushedSiteParamFilter  used for resolving alternate site for PLP/PDP REST API calls .
 * 
 * @version 1.0
 * @author PA
 */
public class TRUPushedSiteParamFilter extends PushedSiteParamFilter {

	/**
	 * Property to hold mCatalogTools.
	 * 
	 */
	private TRUCatalogTools mCatalogTools;

	/**
	 * Property to mProductIdQueryParameter.
	 */
	private String mProductIdQueryParameter;

	/**
	 * Property to mCategoryIdQueryParameter.
	 */
	private String mCategoryIdQueryParameter;
	/**
	 * property to hold tRUConfiguration.
	 */
	private TRUConfiguration mTruConfiguration;

	/**
	 * Gets the TRU configuration.
	 * 
	 * @return the tRUConfiguration
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * Sets the TRU configuration.
	 * 
	 * @param pTruConfiguration
	 *            the tRUConfiguration to set
	 */
	public void setTruConfiguration(TRUConfiguration pTruConfiguration) {
		mTruConfiguration = pTruConfiguration;
	}

	/**
	 * This property holds CatalogProperties variable mCatalogProperties.
	 */
	private TRUCatalogProperties mCatalogProperties;

	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}

	/**
	 * property to hold ApiUrlList.
	 */
	private ArrayList<String> mApiUrlList;

	/**
	 * Gets the api url list.
	 * 
	 * @return the api url list
	 */
	public ArrayList<String> getApiUrlList() {
		return mApiUrlList;
	}

	/**
	 * Sets the api url list.
	 * 
	 * @param pApiUrlList
	 *            the new api url list
	 */
	public void setApiUrlList(ArrayList<String> pApiUrlList) {
		mApiUrlList = pApiUrlList;
	}

	/**
	 * Gets the ProductIdQueryParameter.
	 * 
	 * @return the ProductIdQueryParameter
	 */
	public String getProductIdQueryParameter() {
		return mProductIdQueryParameter;
	}

	/**
	 * Sets the ProductIdQueryParameter.
	 * 
	 * @param pProductIdQueryParameter
	 *            the ProductIdQueryParameter to set
	 */
	public void setProductIdQueryParameter(String pProductIdQueryParameter) {
		mProductIdQueryParameter = pProductIdQueryParameter;
	}

	/**
	 * Gets the CategoryIdQueryParameter.
	 * 
	 * @return the CategoryIdQueryParameter
	 */
	public String getCategoryIdQueryParameter() {
		return mCategoryIdQueryParameter;
	}

	/**
	 * Sets the CategoryIdQueryParameter.
	 * 
	 * @param pCategoryIdQueryParameter
	 *            the CategoryIdQueryParameter to set
	 */
	public void setCategoryIdQueryParameter(String pCategoryIdQueryParameter) {
		mCategoryIdQueryParameter = pCategoryIdQueryParameter;
	}

	/**
	 * Gets the catalog tools.
	 * 
	 * @return the truCatalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Sets the catalog tools.
	 * 
	 * @param pCatalogTools
	 *            the CatalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * This filter() method will identify the site context and set it to the
	 * Site Session context.
	 * 
	 * @param pRequest
	 *            the Request parameter
	 * @param pSiteSessionManager
	 *            the Site Session Manager
	 * 
	 * @return the site id
	 */
	public String filter(DynamoHttpServletRequest pRequest, SiteSessionManager pSiteSessionManager) {
		if (isLoggingDebug()) {
			vlogDebug("Start ::: TRUPushedSiteParamFilter :: filter method ");
		}
		if (getTruConfiguration().isEnableCustomSiteRuleFilter()) {
			if (isLoggingDebug()) {
				vlogDebug("Entered  ::: TRUPushedSiteParamFilter :: filter method  with isEnableCustomSiteRuleFilter true");
			}
			String contextName = pRequest.getContextPath();
			if (StringUtils.isNotBlank(contextName) && contextName.equals(TRUConstants.REST_CONTEXT_PATH)) {
				String channelType = (String) pRequest.getHeader(TRUConstants.API_CHANNEL);
				if (StringUtils.isNotBlank(channelType) && channelType.equalsIgnoreCase(TRUConstants.MOBILE)) {
					if (StringUtils.isNotBlank(pRequest.getRequestURI()) && getApiUrlList().contains(pRequest.getRequestURI())) {
						String siteId = null;
						siteId = super.filter(pRequest, pSiteSessionManager);
						if (StringUtils.isNotBlank(siteId)) {
							String alternateSiteId = getAlternateSiteId(pRequest, siteId);
							if (StringUtils.isNotEmpty(alternateSiteId)) {
								siteId = alternateSiteId;
							}
						}
						if (isLoggingDebug()) {
							vlogDebug("End ::: TRUPushedSiteParamFilter :: filter method ");
						}
						return siteId;
					} else {
						if (isLoggingDebug()) {
							vlogDebug("End ::: TRUPushedSiteParamFilter :: filter method ");
						}
						return super.filter(pRequest, pSiteSessionManager);
					}
				} else {
					if (isLoggingDebug()) {
						vlogDebug("End ::: TRUPushedSiteParamFilter :: filter method ");
					}
					return super.filter(pRequest, pSiteSessionManager);
				}

			} else {
				if (isLoggingDebug()) {
					vlogDebug("End ::: TRUPushedSiteParamFilter :: filter method ");
				}
				return super.filter(pRequest, pSiteSessionManager);
			}
		} else {
			if (isLoggingDebug()) {
				vlogDebug("End ::: TRUPushedSiteParamFilter :: filter method ");
			}
			return super.filter(pRequest, pSiteSessionManager);
		}

	}

	/**
	 * This getAlternateSiteId() method will identify site id to be applied
	 * based on custom rules.
	 * 
	 * @param pRequest
	 *            the Request parameter
	 * @param pSiteId
	 *            the site id thart is store din session
	 * 
	 * @return the site id
	 */
	@SuppressWarnings("unchecked")
	public String getAlternateSiteId(DynamoHttpServletRequest pRequest, String pSiteId) {
		if (isLoggingDebug()) {
			vlogDebug("Start ::: TRUPushedSiteParamFilter :: getAlternateSiteId method ");
		}
		Set<String> siteIdList = null;
		String actualSite = null;
		String categoryId = null;
		String filterString = null;
		String shopBy = null;
		if (getApiUrlList().get(0).equals(pRequest.getRequestURI()) || getApiUrlList().get(1).equals(pRequest.getRequestURI())) {
			shopBy = (String) pRequest.getQueryParameter(TRUConstants.SHOP_BY);
			if (StringUtils.isBlank(shopBy)) {
				shopBy = (String) pRequest.getParameter(TRUConstants.SHOP_BY);
			}
			if (StringUtils.isNotBlank(shopBy)) {
				return null;
			}
			filterString = pRequest.getQueryParameter(TRUConstants.FILTER_STRING);
			if (StringUtils.isBlank(filterString)) {
				filterString = (String) pRequest.getParameter(TRUConstants.FILTER_STRING);
			}
			if (StringUtils.isNotBlank(filterString)) {
				return null;
			}
			categoryId = pRequest.getQueryParameter(getCategoryIdQueryParameter());
			if (StringUtils.isBlank(categoryId)) {
				categoryId = (String) pRequest.getParameter(getCategoryIdQueryParameter());
			}
			try {
				if (StringUtils.isNotEmpty(categoryId)) {
					RepositoryItem categoryItem = getCatalogTools().findCategory(categoryId);
					if (categoryItem != null) {
						siteIdList = (Set<String>) categoryItem.getPropertyValue(getCatalogProperties().getSiteIDPropertyName());
						actualSite = getActualSite(siteIdList, pSiteId);
						return actualSite;
					}
				}
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					vlogError(repositoryException, "RepositoryException occured in TRUPushedSiteParamFilter :: getAlternateSiteId method");
				}
			}
		}
		if (getApiUrlList().get(2).equals(pRequest.getRequestURI())) {
			String productId = pRequest.getQueryParameter(getProductIdQueryParameter());
			if (StringUtils.isBlank(productId)) {
				productId = (String) pRequest.getParameter(getProductIdQueryParameter());
			}
			if (StringUtils.isNotEmpty(productId)) {
				RepositoryItem[] skuList = getCatalogTools().getSKUFromOnlinePID(productId);
				if (skuList != null && skuList.length > 0) {
					RepositoryItem skuItem = skuList[0];
					if (skuItem != null) {
						siteIdList = (Set<String>) skuItem.getPropertyValue(getCatalogProperties().getSiteIDPropertyName());
						actualSite = getActualSite(siteIdList, pSiteId);
					}
				}
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("END ::: TRUPushedSiteParamFilter :: getAlternateSiteId method ");
		}
		return actualSite;
	}

	/**
	 * This getActualSite() method will identify site id to be applied based on
	 * custom rules.
	 * 
	 * @param pSiteIdList
	 *            the Request parameter
	 * @param pCurrentSite
	 *            the site id thart is store din session
	 * 
	 * @return the site id
	 */
	private String getActualSite(Set<String> pSiteIdList, String pCurrentSite) {
		String actualSite = null;
		if (pSiteIdList != null && !pSiteIdList.isEmpty() && !pSiteIdList.contains(pCurrentSite)) {
			for (String siteId : pSiteIdList) {
				actualSite = siteId;
				if (isLoggingDebug()) {
					vlogDebug("Alternate Site is : ",actualSite);
				}
				break;
			}

		}
		return actualSite;

	}

}
