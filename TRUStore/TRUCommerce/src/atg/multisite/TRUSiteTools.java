package atg.multisite;

import java.util.List;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.userprofiling.ProfileThreadNamingPipelineServlet;

import com.tru.common.TRUIntegrationConfiguration;
import com.tru.integrations.TRUIntegrationPropertiesConfig;
import com.tru.logging.TRUIntegrationInfoLogger;

/** 
 * This will get CouponService, AddressDoctor, Epslon, HookLogic and PowerReviews BOOLEAN true false vale
 * and set it for dynamic integration enabling or disabling. Give the same names in BCC Site Repository 
 * which are CouponService, AddressDoctor, Epslon, HookLogic and PowerReviews.
 * Update above comment whenever new integrations are added 
 * @author PA
 * @version 1.0
 */

public class TRUSiteTools extends SiteTools {
	
	/** Holds the Constant of mTruIntegrationConfiguration. */
	private TRUIntegrationConfiguration mTruIntegrationConfiguration;

	/** Holds the Constant of mIntegrationPropertiesConfig. */
	private TRUIntegrationPropertiesConfig mIntegrationPropertiesConfig;	
	
	/** Property to hold mThreadNamingServlet *. */
	private ProfileThreadNamingPipelineServlet mThreadNamingServlet;
	
	/** Property to hold mIntegrationInfoLogger *. */
	private TRUIntegrationInfoLogger mIntegrationInfoLogger;
	
	/**
	 * @return the mIntegrationInfoLogger
	 */
	public TRUIntegrationInfoLogger getIntegrationInfoLogger() {
		return mIntegrationInfoLogger;
	}

	/**
	 * @param pIntegrationInfoLogger the mIntegrationInfoLogger to set
	 */
	public void setIntegrationInfoLogger(TRUIntegrationInfoLogger pIntegrationInfoLogger) {
		mIntegrationInfoLogger = pIntegrationInfoLogger;
	}
	
	/**
	 * @return the mThreadNamingServlet
	 */
	public ProfileThreadNamingPipelineServlet getThreadNamingServlet() {
		return mThreadNamingServlet;
	}

	/**
	 * @param pThreadNamingServlet the mThreadNamingServlet to set
	 */
	public void setThreadNamingServlet(ProfileThreadNamingPipelineServlet pThreadNamingServlet) {
		mThreadNamingServlet = pThreadNamingServlet;
	}
	/**
	 * Gets the integration properties config.
	 *
	 * @return the integrationPropertiesConfig
	 */
	public TRUIntegrationPropertiesConfig getIntegrationPropertiesConfig() {
		return mIntegrationPropertiesConfig;
	}

	/**
	 * Sets the integration properties config.
	 *
	 * @param pIntegrationPropertiesConfig the integrationPropertiesConfig to set
	 */
	public void setIntegrationPropertiesConfig(TRUIntegrationPropertiesConfig pIntegrationPropertiesConfig) {
		this.mIntegrationPropertiesConfig = pIntegrationPropertiesConfig;
	}

	/**
	 * Gets the tru integration configuration.
	 *
	 * @return the truIntegrationConfiguration
	 */
	public TRUIntegrationConfiguration getTruIntegrationConfiguration() {
		return mTruIntegrationConfiguration;
	}

	/**
	 * Sets the tru integration configuration.
	 *
	 * @param pTruIntegrationConfiguration the truIntegrationConfiguration to set
	 */
	public void setTruIntegrationConfiguration(TRUIntegrationConfiguration pTruIntegrationConfiguration) {
		mTruIntegrationConfiguration = pTruIntegrationConfiguration;
	}	
	
	/**
	 * This method will read the the service name and value from repository and update those value to TRUSOSIntegrationConfiguration.
	 *
	 * @param pSiteItem RepositoryItem
	 */
	@SuppressWarnings("unchecked")
	public void updateIntegrations(RepositoryItem pSiteItem) {
		if (isLoggingDebug()) {
			vlogDebug("Entering inside TRUSiteTools.updateIntegrations()");
		}
		if (pSiteItem == null) {
			if (isLoggingDebug()) {
				vlogDebug("Site item is empty -- >", pSiteItem);
			}
			return;
		}
		Boolean enabled = null;
		String integrationName = null;
		final List<RepositoryItem> storeIntegrations = (List<RepositoryItem>) pSiteItem
				.getPropertyValue(getIntegrationPropertiesConfig().getTruIntegrationsPropertyName());
		
		if (storeIntegrations != null && !storeIntegrations.isEmpty()) {
			for (RepositoryItem storeIntegration : storeIntegrations) {
				integrationName = (String) storeIntegration.getPropertyValue(getIntegrationPropertiesConfig()
						.getIntegrationNamePropertyName());
				if (StringUtils.isBlank(integrationName)) {
					continue;
				}
				enabled = (Boolean) storeIntegration.getPropertyValue(getIntegrationPropertiesConfig().getEnabledPropertyName());
				if (enabled == null) {
					continue;
				}
				if (isLoggingDebug()) {
					vlogDebug(">>>> Store IntegrationName:: " + integrationName + "  enabled:: " + enabled);
				}
				if (integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getCouponServiceIntegrationName())) {
					getTruIntegrationConfiguration().setEnableCouponService(enabled.booleanValue());
				} else if (integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getAddressDoctorIntegrationName())) {
					getTruIntegrationConfiguration().setEnableAddressDoctor(enabled.booleanValue());
				} else if (integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getEpslonIntegrationName())) {
					getTruIntegrationConfiguration().setEnableEpslon(enabled.booleanValue());
				} else if (integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getHookLogicIntegrationName())) {
					getTruIntegrationConfiguration().setEnableHookLogic(enabled.booleanValue());
				} else if (integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getPowerReviewsIntegrationName())) {
					getTruIntegrationConfiguration().setEnablePowerReviews(enabled.booleanValue());
				} else if (integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getTaxwareIntegrationName())) {
					getTruIntegrationConfiguration().setEnableTaxware(enabled.booleanValue());
				} else if (integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getCertonaIntegrationName())) {
					getTruIntegrationConfiguration().setEnableCertona(enabled.booleanValue());
				} else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getCardinalIntegrationName())) {
					getTruIntegrationConfiguration().setEnableCardinal(enabled.booleanValue());
				} else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getRadialCreditCardIntegrationName())) {
					getTruIntegrationConfiguration().setEnableRadialCreditCard(enabled.booleanValue());
				} else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getRadialGiftCardIntegrationName())) {
					getTruIntegrationConfiguration().setEnableRadialGiftCard(enabled.booleanValue());
				} else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getRadialPayPalIntegrationName())) {
					getTruIntegrationConfiguration().setEnableRadialPayPal(enabled.booleanValue());
				} else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getRadialApplePayIntegrationName())) {
					getTruIntegrationConfiguration().setEnableRadialApplePay(enabled.booleanValue());
				} else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getRadialSoftAllocationIntegrationName())) {
					getTruIntegrationConfiguration().setEnableRadialSoftAllocation(enabled.booleanValue());
				} else if(integrationName.equalsIgnoreCase(getIntegrationPropertiesConfig().getThreadNamingServlet())){
					getThreadNamingServlet().setEnabled(enabled.booleanValue());
					getIntegrationInfoLogger().setEnableThreadNaming(enabled.booleanValue());
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("Exiting from TRUSiteTools.updateIntegrations()");
		}
	}
}