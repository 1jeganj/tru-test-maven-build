package atg.vfs.switchable;

import java.io.File;

import atg.nucleus.Configuration;
import atg.nucleus.Nucleus;
import atg.nucleus.ServiceException;
import atg.nucleus.logging.ApplicationLogging;
import atg.service.scheduler.Schedule;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.vfs.journal.Journal;
import atg.vfs.journal.JournalException;

import com.tru.common.TRUConfiguration;

// TODO: Auto-generated Javadoc
/**
 * The Class TRUSlaveLocalFileSystem is extended to have custom logic to fix
 * multiple slave instances update.
 * 
 * @author Professional Access
 * @version 1.0 TRUSlaveLocalFileSystem extends the OOTB SlaveLocalFileSystem.
 */
public class TRUSlaveLocalFileSystem extends SlaveLocalFileSystem {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * property to hold RetainVersion.
	 */
	private int mLocalVFSVersion;

	/**
	 * property to hold RetainVersion.
	 */
	private boolean mConfigsAvailable;

	/**
	 * property to hold CatalogTools.
	 */
	private static TRUConfiguration mConfiguration;

	/** Constant to hold the TRUCONFIGURATION value. */
	public static final String TRU_CONFIGURATION = "/com/tru/common/TRUConfiguration";

	/** Constant to hold the SLASH value. */
	public static final String SLASH = "/";

	/** Constant to hold the DOT value. */
	public static final String DOT = ".";

	/**
	 * Instantiates a new TRU slave local file system.
	 * 
	 * @param pLogger
	 *            the logger
	 * @param pRootDir
	 *            the root dir
	 * @param pStatusFile
	 *            the status file
	 * @param pJournalDirectory
	 *            the journal directory
	 * @param pScheduler
	 *            the scheduler
	 * @param pSchedule
	 *            the schedule
	 * @param pJournalFileContent
	 *            the journal file content
	 * @param pLinksEnabled
	 *            the links enabled
	 * @param pCheckCase
	 *            the check case
	 * @param pExtraBooleanFlag
	 *            boolean flag
	 * @throws ServiceException
	 *             the service exception
	 */
	public TRUSlaveLocalFileSystem(ApplicationLogging pLogger, File pRootDir,
			File pStatusFile, File pJournalDirectory, Scheduler pScheduler,
			Schedule pSchedule, boolean pJournalFileContent,
			boolean pLinksEnabled, boolean pCheckCase, boolean pExtraBooleanFlag)
			throws ServiceException {
		super(pLogger, pRootDir, pStatusFile, pJournalDirectory, pScheduler,
				pSchedule, pJournalFileContent, pLinksEnabled, pCheckCase,
				pExtraBooleanFlag);
	}

	/**
	 * Overidded OOTB read status method to validate against nucleus.
	 * 
	 * @return l_bStatus, status
	 */
	public boolean readStatus() {
		if (this.mLogger.isLoggingDebug()) {
			this.mLogger
					.logDebug("Start readStatus in TRUSlaveLocalFileSystem");
		}
		boolean l_bStatus = false;
		l_bStatus = super.readStatus();
		if (this.mLogger.isLoggingDebug()) {
			this.mLogger.logDebug("Super Read Status : " + l_bStatus);
		}
		if (l_bStatus) {
			Nucleus nucleus = getScheduler().getNucleus();
			if (this.mLogger.isLoggingDebug()) {
				this.mLogger.logDebug("mLocalVFSVersion : " + mLocalVFSVersion
						+ " getUpdateVersion : " + getUpdateVersion());
			}
			if (mLocalVFSVersion != getUpdateVersion()) {
				l_bStatus = validateComponentInNucleus(nucleus);
				if (this.mLogger.isLoggingDebug()) {
					this.mLogger.logDebug("In Nucleus check  : " + l_bStatus);
				}
			}
		}
		if (this.mLogger.isLoggingDebug()) {
			this.mLogger.logDebug("End readStatus in TRUSlaveLocalFileSystem");
		}
		return l_bStatus;
	}

	/**
	 * This method is used to validate against version if changes calling handle
	 * update in the next schedule time.
	 * 
	 * @param pScheduler
	 *            the scheduler
	 * @param pJob
	 *            the scheduler job
	 */
	public void performScheduledTask(Scheduler pScheduler, ScheduledJob pJob) {
		if (this.mLogger.isLoggingDebug()) {
			this.mLogger
					.logDebug("Start performScheduledTask in TRUSlaveLocalFileSystem");
		}
		Nucleus nucleus = Nucleus.getGlobalNucleus();
		mConfiguration = (TRUConfiguration) nucleus
				.resolveName(TRU_CONFIGURATION);
		if (mConfiguration.isCustomSlaveEnable()) {
			boolean oneLive = getOneLive();
			boolean success = readStatus();
			if (this.mLogger.isLoggingDebug()) {
				this.mLogger.logDebug("oneLive : " + oneLive + " success : "
						+ success + " mLocalVFSVersion : " + mLocalVFSVersion
						+ " getUpdateVersion : " + this.getUpdateVersion());
			}
			if (!success) {
				return;
			}
			if ((oneLive != getOneLive())
					|| (mLocalVFSVersion != this.getUpdateVersion())) {
				if (this.mLogger.isLoggingDebug()) {
					this.mLogger
							.logDebug("calling handleUpdate as a result of status change");
				}
				handleUpdate();
				if (success) {
					if (this.mLogger.isLoggingDebug()) {
						this.mLogger
								.logDebug("After Handle update call  mLocalVFSVersion : "
										+ mLocalVFSVersion
										+ " getUpdateVersion : "
										+ getUpdateVersion());
					}
					mLocalVFSVersion = getUpdateVersion();
				}
			}
		} else {
			super.performScheduledTask(pScheduler, pJob);
		}
		if (this.mLogger.isLoggingDebug()) {
			this.mLogger
					.logDebug("End performScheduledTask in TRUSlaveLocalFileSystem");
		}
	}

	/**
	 * Validate component in nucleus.
	 * 
	 * @param pNucleus
	 *            the nucleus
	 * @return true, if successful
	 */
	private boolean validateComponentInNucleus(Nucleus pNucleus) {
		if (this.mLogger.isLoggingDebug()) {
			this.mLogger
					.logDebug("Start validateComponentInNucleus in TRUSlaveLocalFileSystem");
		}
		try {
			if ((isPerformGCOnUpdate()) && (Util.sIsWindows)) {
				System.gc();
			}
			this.mJournalDirectory = new File(
					this.mJournalDirectory.getAbsolutePath());
			this.mJournal = new Journal(this.mJournalDirectory,
					this.getJournalFileContent());
			Journal.EntryIterator it = this.mJournal.getEntryIterator();
			String repositoryGroupName = null;
			Configuration config = null;
			mConfigsAvailable = true;
			while (it.hasNext()) {
				Journal.Entry entry = it.nextEntry();
				repositoryGroupName = entry.getVirtualPath().getPath(SLASH);
				if (this.mLogger.isLoggingDebug()) {
					this.mLogger
							.logDebug("repositoryGroupName before index removed : "
									+ repositoryGroupName);
				}
				if (repositoryGroupName.contains(DOT)) {
					int indexOf = repositoryGroupName.indexOf(DOT);
					repositoryGroupName = repositoryGroupName.substring(0,
							indexOf);
				}
				if (this.mLogger.isLoggingDebug()) {
					this.mLogger.logDebug("repositoryGroupName : "
							+ repositoryGroupName);
				}
				config = pNucleus.getConfigurationFinder().findConfiguration(
						repositoryGroupName, false, null, null);
				if (this.mLogger.isLoggingDebug()) {
					this.mLogger.logDebug("In Nucleus : " + config);
				}
				if (null == config) {
					mConfigsAvailable = false;
					if (this.mLogger.isLoggingDebug()) {
						this.mLogger
								.logDebug("component is not available in nuclues so making retain version to : "
										+ mConfigsAvailable);
					}
					break;
				}
			}
		} finally {
			try {
				getJournal().close();
			} catch (JournalException exc) {
				if (this.mLogger.isLoggingError()) {
					this.mLogger.logError(exc);
				}
			}
		}
		if (this.mLogger.isLoggingDebug()) {
			this.mLogger
					.logDebug("End validateComponentInNucleus in TRUSlaveLocalFileSystem");
		}
		return mConfigsAvailable;
	}
}
