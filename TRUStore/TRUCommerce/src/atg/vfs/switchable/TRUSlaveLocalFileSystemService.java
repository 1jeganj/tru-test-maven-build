/**
 * 
 */
package atg.vfs.switchable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import atg.nucleus.ServiceException;
import atg.service.scheduler.Schedule;
import atg.service.scheduler.Scheduler;
import atg.vfs.VFSUpdateEventListener;
import atg.vfs.VirtualFile;
import atg.vfs.VirtualFileSystem;
import atg.vfs.VirtualFileSystemDescriptor;
import atg.vfs.VirtualPath;
import atg.vfs.journal.Journal;

import com.tru.common.TRUConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class TRUSlaveLocalFileSystemService.
 * 
 * @author PA
 */
public class TRUSlaveLocalFileSystemService extends SlaveLocalFileSystemService {

	/** The m wrapped vfs. */
	TRUSlaveLocalFileSystem mWrappedVFS = null;

	/**
	 * VirtualFileSystem.
	 * 
	 * @return mWrappedVFS
	 */
	public VirtualFileSystem getVirtualFileSystem() {
		return this.mWrappedVFS;
	}

	/**
	 * TRUSlaveLocalFileSystem.
	 * 
	 * @return mWrappedVFS
	 */
	public TRUSlaveLocalFileSystem getWrappedVFS() {
		return this.mWrappedVFS;
	}

	/** The m scheduler. */
	Scheduler mScheduler = null;

	/**
	 * setScheduler.
	 * 
	 * @param pScheduler
	 *            the scheduler
	 */
	public void setScheduler(Scheduler pScheduler) {
		if (this.mWrappedVFS == null) {
			this.mScheduler = pScheduler;
		} else {
			this.mWrappedVFS.setScheduler(pScheduler);
		}
	}

	/**
	 * getScheduler.
	 * 
	 * @return getScheduler
	 */
	public Scheduler getScheduler() {
		if (this.mWrappedVFS == null) {
			return this.mScheduler;
		}
		return this.mWrappedVFS.getScheduler();
	}

	/** The m schedule. */
	Schedule mSchedule = null;

	/**
	 * setSchedule.
	 * 
	 * @param pSchedule
	 *            the schedule
	 */
	public void setSchedule(Schedule pSchedule) {
		if (this.mWrappedVFS == null) {
			this.mSchedule = pSchedule;
		} else {
			this.mWrappedVFS.setSchedule(pSchedule);
		}
	}

	/**
	 * getSchedule.
	 * 
	 * @return mWrappedVFS.getSchedule
	 */
	public Schedule getSchedule() {
		if (this.mWrappedVFS == null) {
			return this.mSchedule;
		}
		return this.mWrappedVFS.getSchedule();
	}

	/**
	 * getRootDir.
	 * 
	 * @return mWrappedVFS.getRootDirectory
	 */
	public File getRootDir() {
		if (this.mWrappedVFS == null) {
			return this.mRootDir;
		}
		return this.mWrappedVFS.getRootDirectory();
	}

	/**
	 * setRootDir.
	 * 
	 * @param pRootDir
	 *            the rootDir
	 */
	public void setRootDir(File pRootDir) {
		if (this.mWrappedVFS == null) {
			this.mRootDir = pRootDir;
		} else {
			this.mWrappedVFS.setRootDirectory(pRootDir);
		}
	}

	/**
	 * getRootDirectory.
	 * 
	 * @return getRootDir
	 */
	public File getRootDirectory() {
		return getRootDir();
	}

	/**
	 * setRootDirectory.
	 * 
	 * @param pRootDir
	 *            the rootDir
	 */
	public void setRootDirectory(File pRootDir) {
		setRootDir(pRootDir);
	}

	/**
	 * getStatusFile.
	 * 
	 * @return mWrappedVFS.getStatusFile
	 */
	public File getStatusFile() {
		if (this.mWrappedVFS == null) {
			return this.mStatusFile;
		}
		return this.mWrappedVFS.getStatusFile();
	}

	/**
	 * setStatusFile.
	 * 
	 * @param pStatusFile
	 *            the statusFile
	 */
	public void setStatusFile(File pStatusFile) {
		if (this.mWrappedVFS == null) {
			this.mStatusFile = pStatusFile;
		} else {
			this.mWrappedVFS.setStatusFile(pStatusFile);
		}
	}

	/**
	 * getJournalDirectory.
	 * 
	 * @return mWrappedVFS.getJournalDirectory
	 */
	public File getJournalDirectory() {
		if (this.mWrappedVFS == null) {
			return this.mJournalDirectory;
		}
		return this.mWrappedVFS.getJournalDirectory();
	}

	/**
	 * setJournalDirectory.
	 * 
	 * @param pJournalDirectory
	 *            the journalDirectory
	 */
	public void setJournalDirectory(File pJournalDirectory) {
		if (this.mWrappedVFS == null) {
			this.mJournalDirectory = pJournalDirectory;
		} else {
			this.mWrappedVFS.setJournalDirectory(pJournalDirectory);
		}
	}

	/**
	 * getJournal.
	 * 
	 * @return mWrappedVFS.getJournal
	 */
	public Journal getJournal() {
		return this.mWrappedVFS.getJournal();
	}

	/** The m journal file content. */
	private boolean mJournalFileContent = false;

	/**
	 * setJournalFileContent.
	 * 
	 * @param pJournalFileContent
	 *            the journalFileContent
	 */
	public void setJournalFileContent(boolean pJournalFileContent) {
		if (this.mWrappedVFS == null) {
			this.mJournalFileContent = pJournalFileContent;
		} else {
			this.mWrappedVFS.setJournalFileContent(pJournalFileContent);
		}
	}

	/**
	 * getJournalFileContent.
	 * 
	 * @return mWrappedVFS.getJournalFileContent
	 */
	public boolean getJournalFileContent() {
		if (this.mWrappedVFS == null) {
			return this.mJournalFileContent;
		}
		return this.mWrappedVFS.getJournalFileContent();
	}

	/** The m check file name case. */
	boolean mCheckFileNameCase = false;

	/**
	 * setCheckFileNameCase.
	 * 
	 * @param pCheckFileNameCase
	 *            the checkFileNameCase
	 */
	public void setCheckFileNameCase(boolean pCheckFileNameCase) {
		if (this.mWrappedVFS == null) {
			this.mCheckFileNameCase = pCheckFileNameCase;
		} else {
			this.mWrappedVFS.setCheckFileNameCase(pCheckFileNameCase);
		}
	}

	/**
	 * getCheckFileNameCase.
	 * 
	 * @return mWrappedVFS.getCheckFileNameCase
	 */
	public boolean getCheckFileNameCase() {
		if (this.mWrappedVFS == null) {
			return this.mCheckFileNameCase;
		}
		return this.mWrappedVFS.getCheckFileNameCase();
	}

	/**
	 * setLinksEnabled.
	 * 
	 * @param pLinksEnabled
	 *            the linksEnabled
	 */
	public void setLinksEnabled(boolean pLinksEnabled) {
		if (this.mWrappedVFS == null) {
			this.mLinksEnabled = pLinksEnabled;
		} else {
			this.mWrappedVFS.setLinksEnabled(pLinksEnabled);
		}
	}

	/**
	 * getLinksEnabled.
	 * 
	 * @return mWrappedVFS.getLinksEnabled
	 */
	public boolean getLinksEnabled() {
		if (this.mWrappedVFS == null) {
			return this.mLinksEnabled;
		}
		return this.mWrappedVFS.getLinksEnabled();
	}

	/**
	 * doStartService.
	 * 
	 * @throws ServiceException
	 *             - ServiceException
	 */
	public void doStartService() throws ServiceException {
		this.mWrappedVFS = new TRUSlaveLocalFileSystem(this, this.mRootDir,
				this.mStatusFile, this.mJournalDirectory, this.mScheduler,
				this.mSchedule, this.mJournalFileContent, this.mLinksEnabled,
				this.mCheckFileNameCase, false);

		this.mWrappedVFS.setVFSName(getAbsoluteName());
		this.mWrappedVFS
				.setUpdateListeners((VFSUpdateEventListener[]) (VFSUpdateEventListener[]) this.mUpdateListeners
						.toArray(new VFSUpdateEventListener[this.mUpdateListeners
								.size()]));

		this.mWrappedVFS.initialize();
	}

	/**
	 * doStopService.
	 * 
	 * @throws ServiceException
	 *             - ServiceException
	 */
	public void doStopService() throws ServiceException {
		if (this.mWrappedVFS != null) {
			this.mWrappedVFS.halt();
		}
	}

	/**
	 * getOneLive.
	 * 
	 * @return mWrappedVFS.getOneLive
	 */
	public boolean getOneLive() {
		return this.mWrappedVFS.getOneLive();
	}

	/**
	 * handleUpdate.
	 */
	public synchronized void handleUpdate() {
		this.mWrappedVFS.handleUpdate();
	}

	/**
	 * deleteAllFiles.
	 * 
	 * @return true
	 */
	public boolean deleteAllFiles() {
		return true;
	}

	/**
	 * deleteAllBeforeDate.
	 * 
	 * @param pPurgeDate
	 *            the purgeDate
	 * @return true
	 */
	public boolean deleteAllBeforeDate(long pPurgeDate) {
		return true;
	}

	/**
	 * getVFSName.
	 * 
	 * @return mWrappedVFS.getVFSName
	 */
	public String getVFSName() {
		return this.mWrappedVFS.getVFSName();
	}

	/**
	 * getFile.
	 * 
	 * @param pPath
	 *            the path
	 * @return mWrappedVFS.getFile
	 */
	public VirtualFile getFile(String pPath) {
		return this.mWrappedVFS.getFile(pPath);
	}

	/**
	 * getFile.
	 * 
	 * @param pPath
	 *            the path
	 * @return mWrappedVFS.getFile
	 */
	public VirtualFile getFile(VirtualPath pPath) {
		return this.mWrappedVFS.getFile(pPath);
	}

	/**
	 * separator.
	 * 
	 * @return mWrappedVFS.separator
	 */
	public String separator() {
		return this.mWrappedVFS.separator();
	}

	/**
	 * getDescriptor.
	 * 
	 * @return mWrappedVFS.getDescriptor
	 */
	public VirtualFileSystemDescriptor getDescriptor() {
		return this.mWrappedVFS.getDescriptor();
	}

	/** The m update listeners. */
	private List mUpdateListeners = new ArrayList(TRUConstants.ONE);

	/**
	 * setUpdateListeners.
	 * 
	 * @param pListeners
	 *            the listeners
	 */
	public synchronized void setUpdateListeners(
			VFSUpdateEventListener[] pListeners) {
		if (this.mWrappedVFS != null) {
			this.mWrappedVFS.setUpdateListeners(pListeners);
		} else {
			for (int i = 0; (pListeners != null) && (i < pListeners.length); ++i) {
				this.mUpdateListeners.add(pListeners[i]);
			}
		}
	}

	/**
	 * addUpdateListener.
	 * 
	 * @param pListener
	 *            the listener
	 */
	public synchronized void addUpdateListener(VFSUpdateEventListener pListener) {
		if (this.mWrappedVFS != null) {
			this.mWrappedVFS.addUpdateListener(pListener);
		} else {
			this.mUpdateListeners.add(pListener);
		}
	}

	/**
	 * removeUpdateListener.
	 * 
	 * @param pListener
	 *            the listener
	 */
	public synchronized void removeUpdateListener(
			VFSUpdateEventListener pListener) {
		if (this.mWrappedVFS != null) {
			this.mWrappedVFS.removeUpdateListener(pListener);
		} else {
			this.mUpdateListeners.remove(pListener);
		}
	}

	/**
	 * VFSUpdateEventListener.
	 * 
	 * @return VFSUpdateEventListener
	 */
	public synchronized VFSUpdateEventListener[] getUpdateListeners() {
		if (this.mWrappedVFS != null) {
			return this.mWrappedVFS.getUpdateListeners();
		}

		VFSUpdateEventListener[] answer = new VFSUpdateEventListener[this.mUpdateListeners
				.size()];
		return ((VFSUpdateEventListener[]) (VFSUpdateEventListener[]) this.mUpdateListeners
				.toArray(answer));
	}
}
