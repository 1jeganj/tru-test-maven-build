package com.tru.servlet.pipeline;

import java.io.IOException;
import java.util.Enumeration;
import java.util.StringTokenizer;

import javax.servlet.ServletException;

import atg.multisite.SiteManager;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.InsertableServletImpl;

/**
 * This pipeline servlet performs switching between a secure sever and a
 * non-secure server. A list of secure paths and the enable property controls
 * the switching. The servlet is configured with a list of URL mappings; if the
 * URL being accessed is in the URL mapping, the request is passed off to the
 * secure server. The nonSecureHostName and secureHostname are default taken
 * from atg/dynamo/Configuration. These can be overwridden at the component
 * level.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUProtocolSwitchServlet extends InsertableServletImpl {

	/** Class version string. */
	public static final String CLASS_VERSION = "$Id: //hosting-blueprint/B2CBlueprint/version/11.1/EStore/src/atg/projects/store/servlet/pipeline/ProtocolSwitchServlet.java#2 $$Change: 877954 $";
	// -------------------------------------
	// Constants
	// -------------------------------------

	/**
	 * Secure protocol constant.
	 */
	public static final String SECURE_PROTOCOL = "https";

	/**
	 * Nonsecure protocol constant.
	 */
	public static final String NONSECURE_PROTOCOL = "http";

	/**
	 * The Constant POST.
	 */
	public static final String POST = "POST";

	/**
	 * The Constant COLON.
	 */
	public static final String COLON = ":";

	/**
	 * The Constant COLON_SLASH.
	 */
	public static final String COLON_SLASH = "://";

	/**
	 * The Constant ORIGIN_SCHEME_HEADER.
	 */
	public static final String ORIGIN_SCHEME_HEADER = "origin-scheme";

	/**
	 * The Constant SERVLET_EXCEPTION_CAN_NOT_SWITCH.
	 */
	public static final String SERVLET_EXCEPTION_CAN_NOT_SWITCH = "cannot switch on post";

	/**
	 * The Constant COLON_SLASH.
	 */
	public static final String HOST = "host";

	/** List of secure paths to protect. */
	protected String[] mSecureList = null;

	/** List of paths to ignore. */
	protected String[] mIgnoreList = null;

	/** Http port. */
	protected int mHttpPort;

	/** Https port. */
	protected int mHttpsPort;

	/** enable scheme detection from header. */
	protected boolean mEnableSchemeDetectionFromHeader = false;

	/** Enabled property. */
	protected boolean mEnabled = false;

	/** String to hold welcomeUrl. */
	private String mWelcomeUrl;

	/** String to hold mRootUrl. */
	private String mRootUrl;

	/**
	 * This property hold reference for mEnableHttps.
	 */
	private String mEnableHttps;

	/**
	 * Called in the pipeline when a request needs to be processed. The
	 * following conditions cause the request to be passed off to the next
	 * servlet in the pipeline: <li>If a protocol switch is not required
	 * Otherwise, a call is made to ProtocolSwitchURLGenerator to generate a URL
	 * for the protocol to be switched to by checking if the requested URI is in
	 * the secure list.
	 * 
	 * @param pRequest
	 *            The servlet's request
	 * @param pResponse
	 *            The servlet's response
	 * @exception ServletException
	 *                if an error occurred while processing the servlet request
	 * @exception IOException
	 *                if an error occurred while reading or writing the servlet
	 *                request
	 */
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws IOException,
			ServletException {
		if (isLoggingDebug()) {
			logDebug("START:: TRUProtocolSwitchServlet.service()");
		}
		RepositoryItem[] activeSites = null;
		boolean isEnableHttps = Boolean.FALSE;
		if (mEnabled) {
			// get the URI without the query string to check is it is in the
			// list of secure pages.
			String pathWithoutQueryString = pRequest.getRequestURI();
			// Also get the path w/ query string to be used in the redirect
			String path = pRequest.getRequestURIWithQueryString();
			if (path.contains(getWelcomeUrl())) {
				path = path.replace(getWelcomeUrl(), getRootUrl());
			}
			if (isCurrentPathInIgnoreList(pathWithoutQueryString)) {
				passRequest(pRequest, pResponse);
			} else {
				// check for HTTPS flag
				try {
					activeSites = SiteManager.getSiteManager().getActiveSites();
				} catch (RepositoryException repositoryException) {
					if (isLoggingError()) {
						vlogError(
								repositoryException,
								"RepositoryException occured in TRUCustomSiteRuleFilter :: getSiteInfoForStaticPages method");
					}
				}
				if (activeSites != null && activeSites.length > 0) {
					for (int i = 0; i < activeSites.length; i++) {
						RepositoryItem site = activeSites[i];
						isEnableHttps = (Boolean) site
								.getPropertyValue(getEnableHttps());
						break;
					}
				}
				boolean isSchemeSecure = isCurrentSchemeSecure(pRequest);
				boolean isSecureListUrl = isCurrentPathInSecureList(pathWithoutQueryString);
				if (!isSchemeSecure
						&& (isEnableHttps || (!isEnableHttps && isSecureListUrl))) {
					if (isLoggingDebug()) {
						logDebug("Redirecting this request to secure Url :: "
								+ path);
					}
					if (pRequest.getMethod().equals(POST)) {
						throw new ServletException(
								SERVLET_EXCEPTION_CAN_NOT_SWITCH);
					}
					String secureRedirectURL = getSecureUrl(path, pRequest);
					if (isLoggingDebug()) {
						logDebug("Page is secure but request is having non secure scheme.");
						logDebug("Redirecting to: " + secureRedirectURL);
						logDebug("Encoded URL: "
								+ pResponse
										.encodeRedirectURL(secureRedirectURL));
					}
					pResponse.sendRedirect(pResponse
							.encodeRedirectURL(secureRedirectURL));
				} else if (isSchemeSecure && !isEnableHttps && !isSecureListUrl) {
					if (isLoggingDebug()) {
						logDebug("Redirecting this request to non-secure Url :: "
								+ path);
					}
					/*
					 * if (pRequest.getMethod().equals(POST)) { throw new
					 * ServletException(SERVLET_EXCEPTION_CAN_NOT_SWITCH); }
					 */
					String nonSecureRedirectURL = getNonSecureUrl(path,
							pRequest);
					if (isLoggingDebug()) {
						logDebug("Page is non-secure but request is having secure scheme.");
						logDebug("Redirecting to: " + nonSecureRedirectURL);
						logDebug("Encoded URL: "
								+ pResponse
										.encodeRedirectURL(nonSecureRedirectURL));
					}
					pResponse.sendRedirect(pResponse
							.encodeRedirectURL(nonSecureRedirectURL));
				} else {
					passRequest(pRequest, pResponse);
				}
			}
		} else {
			passRequest(pRequest, pResponse);
		}

		if (isLoggingDebug()) {
			logDebug("END:: TRUProtocolSwitchServlet.service()");
		}
	}

	/**
	 * Method used check if the requested URI is part of the secure list. We
	 * assume that the secure list contains paths with the context root if
	 * needed. This allows us to protect servlets that aren't under the context
	 * root as well.
	 * 
	 * @param pPath
	 *            The requested URI without query parameters
	 * @return true if the requested URI is in the secure list, false otherwise.
	 */
	protected boolean isCurrentPathInSecureList(String pPath) {
		if (isLoggingDebug()) {
			logDebug("START:: TRUProtocolSwitchServlet.isCurrentPathInSecureList()");
		}
		boolean isSecure = false;
		if ((mSecureList != null) && (mSecureList.length > 0)) {
			for (int i = 0; i < mSecureList.length; i++) {
				if (isLoggingDebug()) {
					logDebug("matching " + mSecureList[i] + " against " + pPath);
				}
				if (pPath.contains(mSecureList[i])) {
					return true;
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUProtocolSwitchServlet.isCurrentPathInSecureList()");
		}
		return isSecure;
	}

	/**
	 * Method used check if the requested URI is part of the ignored list. We
	 * assume that the ignored list contains paths with the context root if
	 * needed. This allows us to protect servlets that aren't under the context
	 * root as well.
	 * 
	 * @param pPath
	 *            The requested URI without query parameters
	 * @return true if the requested URI is in the secure list, false otherwise.
	 */
	protected boolean isCurrentPathInIgnoreList(String pPath) {
		if (isLoggingDebug()) {
			logDebug("START:: TRUProtocolSwitchServlet.isCurrentPathInIgnoreList()");
		}
		boolean isIgnore = false;

		if ((mIgnoreList != null) && (mIgnoreList.length > 0)) {
			for (int i = 0; i < mIgnoreList.length; i++) {
				if (isLoggingDebug()) {
					logDebug("matching " + mIgnoreList[i] + " against " + pPath);
				}

				if (pPath.contains(mIgnoreList[i])) {
					return true;
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUProtocolSwitchServlet.isCurrentPathInIgnoreList()");
		}
		return isIgnore;
	}

	/**
	 * This method checks if current scheme is secure or not.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest
	 * @return mEnableHttps - boolean
	 */
	protected boolean isCurrentSchemeSecure(DynamoHttpServletRequest pRequest) {
		if (isLoggingDebug()) {
			logDebug("START:: TRUProtocolSwitchServlet.isCurrentSchemeSecure()");
		}

		boolean isSchemeSecure = false;
		/* JJ Added code to check origin scheme header and setting it */
		if (getEnableSchemeDetectionFromHeader()) {
			if (isLoggingDebug()) {
				Enumeration headerNames = pRequest.getHeaderNames();
				while (headerNames.hasMoreElements()) {
					String key = (String) headerNames.nextElement();
					String value = pRequest.getHeader(key);
					logDebug(key + " : " + value);
				}
			}

			String schemeFromHeader = pRequest.getHeader(ORIGIN_SCHEME_HEADER);
			if (isLoggingDebug()) {
				logDebug("scheme from header " + schemeFromHeader);
			}
			if ((schemeFromHeader != null)
					&& schemeFromHeader.equalsIgnoreCase(SECURE_PROTOCOL)) {
				if (isLoggingDebug()) {
					logDebug("setting scheme from header " + isSchemeSecure);
				}
				isSchemeSecure = true;
			}
		} else {
			if ((pRequest.getScheme() != null)
					&& pRequest.getScheme().equalsIgnoreCase(SECURE_PROTOCOL)) {
				isSchemeSecure = true;
			}
		}

		if (isLoggingDebug()) {
			logDebug("START:: TRUProtocolSwitchServlet.isCurrentSchemeSecure()");
		}
		return isSchemeSecure;
	}

	/**
	 * Build a Secure url. If passed /cmo/user/foo/, this will create
	 * https://localhost:8843/cmo/user/foo/ (if localhost is the configured
	 * hostname)
	 * 
	 * @param pUrl
	 *            - url to build full url for
	 * @return secure URL
	 * @param pRequest
	 *            - DynamoHttpServletRequest
	 */
	protected String getSecureUrl(String pUrl, DynamoHttpServletRequest pRequest) {
		if (isLoggingDebug()) {
			logDebug("START:: TRUProtocolSwitchServlet.getSecureUrl()");
		}
		StringBuffer buffer = new StringBuffer();
		buffer.append(SECURE_PROTOCOL);

		int httpsPort = getHttpsPort();
		buffer.append(COLON_SLASH);

		String host = pRequest.getHeader(HOST);

		if (host.contains(COLON)) {
			StringTokenizer tokenizer = new StringTokenizer(host, COLON);
			if (tokenizer.hasMoreElements()) {
				host = (String) tokenizer.nextElement();
			}
		} else {
			if (isLoggingDebug()) {
				logDebug("No need to append port number as application is accessed by domain name");
			}
			httpsPort = 0;
		}

		// XXX - make property
		buffer.append(host);

		if (httpsPort != 0) {
			buffer.append(COLON + httpsPort);
		}

		buffer.append(pUrl);
		if (isLoggingDebug()) {
			logDebug("END:: TRUProtocolSwitchServlet.getSecureUrl()");
		}
		return buffer.toString();
	}

	/**
	 * Build a nonSecure url. If passed /cmo/user/foo/, this will create
	 * http://localhost:8840/cmo/user/foo/ (if localhost is the configured
	 * hostname)
	 * 
	 * @param pUrl
	 *            - url to build full url for
	 * @param pRequest
	 *            - DynamoHttpServletRequest
	 * @return nonsecure URL
	 */
	protected String getNonSecureUrl(String pUrl,
			DynamoHttpServletRequest pRequest) {
		if (isLoggingDebug()) {
			logDebug("START:: TRUProtocolSwitchServlet.getNonSecureUrl()");
		}
		StringBuffer buffer = new StringBuffer();
		buffer.append(NONSECURE_PROTOCOL);

		int httpPort = getHttpPort();
		buffer.append(COLON_SLASH);

		String host = pRequest.getHeader(HOST);

		if (host.contains(COLON)) {
			StringTokenizer tokenizer = new StringTokenizer(host, COLON);
			if (tokenizer.hasMoreElements()) {
				host = (String) tokenizer.nextElement();
			}
		} else {
			if (isLoggingDebug()) {
				logDebug("No need to append port number as application is accessed by domain name");
			}
			httpPort = 0;
		}

		buffer.append(host);

		if (httpPort != 0) {
			buffer.append(COLON + httpPort);
		}

		buffer.append(pUrl);
		if (isLoggingDebug()) {
			logDebug("END:: TRUProtocolSwitchServlet.getNonSecureUrl()");
		}
		return buffer.toString();
	}

	/**
	 * @return https port.
	 */
	public int getHttpsPort() {
		return mHttpsPort;
	}

	/**
	 * @param pHttpsPort
	 *            - https port.
	 */
	public void setHttpsPort(int pHttpsPort) {
		mHttpsPort = pHttpsPort;
	}

	/**
	 * @return http port.
	 */
	public int getHttpPort() {
		return mHttpPort;
	}

	/**
	 * @param pHttpPort
	 *            - http port.
	 */
	public void setHttpPort(int pHttpPort) {
		mHttpPort = pHttpPort;
	}

	/**
	 * Set the secure list. This should normally be a directory, but can be the
	 * prefix to any portion of a url.
	 * 
	 * @param pSecureList
	 *            - array of protected urls
	 */
	public void setSecureList(String[] pSecureList) {
		mSecureList = pSecureList;
	}

	/**
	 * @return list of pages that should be rendered by the secure server.
	 */
	public String[] getSecureList() {
		return mSecureList;
	}

	/**
	 * @return the ignore list.
	 */
	public String[] getIgnoreList() {
		return mIgnoreList;
	}

	/**
	 * @param pIgnoreList
	 *            - the ignore list to set.
	 */
	public void setIgnoreList(String[] pIgnoreList) {
		mIgnoreList = pIgnoreList;
	}

	/**
	 * @return the enabled status.
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * @param pEnabled
	 *            - the enabled status to set.
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	/**
	 * @return the enabled status.
	 */
	public boolean getEnableSchemeDetectionFromHeader() {
		return mEnableSchemeDetectionFromHeader;
	}

	/**
	 * @param pEnabled
	 *            - the enabled status to set.
	 */
	public void setEnableSchemeDetectionFromHeader(boolean pEnabled) {
		mEnableSchemeDetectionFromHeader = pEnabled;
	}

	/**
	 * @return the welcomeUrl.
	 */
	public String getWelcomeUrl() {
		return mWelcomeUrl;
	}

	/**
	 * @param pWelcomeUrl
	 *            - the mWelcomeUrl to set.
	 */
	public void setWelcomeUrl(String pWelcomeUrl) {
		this.mWelcomeUrl = pWelcomeUrl;
	}

	/**
	 * @return the rootUrl.
	 */
	public String getRootUrl() {
		return mRootUrl;
	}

	/**
	 * @param pRootUrl
	 *            - the mRootUrl to set.
	 */
	public void setRootUrl(String pRootUrl) {
		this.mRootUrl = pRootUrl;
	}

	/**
	 * Gets the Enable Https.
	 * 
	 * @return the Enable Https.
	 */
	public String getEnableHttps() {
		return mEnableHttps;
	}

	/**
	 * Sets the Enable Https.
	 * 
	 * @param pEnableHttps
	 *            the Enable Https to set.
	 */
	public void setEnableHttps(String pEnableHttps) {
		this.mEnableHttps = pEnableHttps;
	}
}