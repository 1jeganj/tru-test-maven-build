package com.tru.servlet.pipeline;


import java.io.IOException;

import javax.servlet.ServletException;

import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import atg.userprofiling.ProfilePropertySetter;

import com.tru.common.vo.TRUSiteVO;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.utils.TRUGetSiteTypeUtil;

/**
 * This custom class is written to set profile properties siteStatus into the profile.
 * @author PA
 * @version 1.0
 */

public class TRUProfilePropertySetter extends ProfilePropertySetter {

	/**
	 * Hold the reference of the property propertyManager.
	 */
	private TRUPropertyManager mPropertyManager;
	/**
	 * Hold the reference of the property getSiteTypeUtil.
	 */
	private TRUGetSiteTypeUtil mGetSiteTypeUtil;
	
	/**
	 * @return the getSiteTypeUtil
	 */
	public TRUGetSiteTypeUtil getGetSiteTypeUtil() {
		return mGetSiteTypeUtil;
	}

	/**
	 * @param pGetSiteTypeUtil the getSiteTypeUtil to set
	 */
	public void setGetSiteTypeUtil(TRUGetSiteTypeUtil pGetSiteTypeUtil) {
		mGetSiteTypeUtil = pGetSiteTypeUtil;
	}

	/**
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}


	/**
	 * This OOTB method is used to set the siteStatus property into the profile.
	 * @param pProfile profile
	 * @param pRequest request
	 * @param pResponse response
	 * @return boolean value true
	 * @throws IOException - IOException
	 * @throws ServletException - ServletException
	 * @throws RepositoryException - RepositoryException
	 */
	public boolean setProperties(Profile pProfile, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException, RepositoryException {
		// Get Site Id
		String siteId = SiteContextManager.getCurrentSiteId();
		Site site = null;
		TRUSiteVO truSiteVO = null;
		String siteStatus = null;
		try {
			site = getGetSiteTypeUtil().getSite(siteId);
		} catch (SiteContextException siteContextExcep) {
			if(isLoggingError()) {
				vlogError("Getting SiteContextException while getting the site from repository :{0} ", siteContextExcep);
			}
		}
		truSiteVO = getGetSiteTypeUtil().getSiteInfo(pRequest, site);
		if(truSiteVO != null) {
			siteStatus = truSiteVO.getSiteStatus();
		}
		setProfileProperty(pProfile, getPropertyManager().getSiteStatusPropertyName(), siteStatus);
		return Boolean.TRUE;
	}

}

