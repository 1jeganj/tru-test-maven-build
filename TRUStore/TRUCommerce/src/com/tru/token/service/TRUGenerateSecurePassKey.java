/**
 * 
 */
package com.tru.token.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import atg.nucleus.GenericService;

import com.tru.commerce.TRUCommerceConstants;

/**
 * This custom class is written to generate the secure pass key for encryption and decryption
 * @author Pinky, Kushwaha
 *
 */
public class TRUGenerateSecurePassKey extends GenericService {

	/**
	 * hold to property passKey
	 */
	private String mPassKey;
	/**
	 * hold to property algorithmName
	 */
	private String mAlgorithmName;

	/**
	 * @return the passKey
	 */
	public String getPassKey() {
		return mPassKey;
	}

	/**
	 * @param pPassKey the passKey to set
	 */
	public void setPassKey(String pPassKey) {
		mPassKey = pPassKey;
	}

	/**
	 * @return the algorithmName
	 */
	public String getAlgorithmName() {
		return mAlgorithmName;
	}

	/**
	 * @param pAlgorithmName the algorithmName to set
	 */
	public void setAlgorithmName(String pAlgorithmName) {
		mAlgorithmName = pAlgorithmName;
	}

	/**
	 * This method used to generate secure pass key based the password.
	 * @return String object
	 */
	public String generateSecurePassKey() {

		if(isLoggingDebug()) {
			vlogDebug("TRUGenerateSecurePassKey:(generateSecurePassKey):STARTS");
		}
		String generatedPassword = null;
		try {
			// Create MessageDigest instance for MD5
			MessageDigest md = MessageDigest.getInstance(getAlgorithmName());
			//Add password bytes to digest
			md.update(getPassKey().getBytes());
			//Get the hash's bytes 
			byte[] bytes = md.digest();
			//This bytes[] has bytes in decimal format;
			//Convert it to hexadecimal format
			StringBuilder sb = new StringBuilder();
			for(int i = TRUCommerceConstants.INT_ZERO; i < bytes.length ;i++){
				sb.append(Integer.toString((bytes[i] & TRUCommerceConstants.INT_TWO_FIFTY_FIVE) + TRUCommerceConstants.INT_TWO_FIFTY_SIX, TRUCommerceConstants.INT_SIXITHEEN).substring(TRUCommerceConstants.INT_ONE));
			}
			//Get complete hashed password in hex format
			generatedPassword = sb.toString().toUpperCase();
		} 
		catch (NoSuchAlgorithmException noSuchAlgoExce){
			if(isLoggingError()) {
				logError("No such algorithm exception while generating the secure pass key using SHA-256 algorithm @Class::TRUGenerateSecurePassKey::@method::generateSecurePassKey()",noSuchAlgoExce);
			}
		}
		if(isLoggingDebug()) {
			vlogDebug("generated pass key :{0}", generatedPassword);
			vlogDebug("TRUGenerateSecurePassKey:(generateSecurePassKey):END");
		}
		return generatedPassword;
	}

}
