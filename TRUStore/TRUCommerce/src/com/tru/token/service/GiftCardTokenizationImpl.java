/**
 * GiftCardTokenizationImpl
 */
package com.tru.token.service;

import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.integrations.exception.TRUIntegrationException;
import com.tru.integrations.synchrony.util.AESCryptography;
import com.tru.security.service.TRUTokenization;

/**
 * This Custom class is written to tokenise the gift card number. 
 * @author Professional Access
 * @version 1.0
 *
 */
public class GiftCardTokenizationImpl extends GenericService implements TRUTokenization {

	private static final int _4 = 4;
	/**
	 * hold to property AESCryptography.
	 */
	private AESCryptography mAESCryptography;
	/**
	 * hold to property securityKey.
	 */
	private String mSecurityKey;

	/**
	 * @return the aESCryptography
	 */
	public AESCryptography getAESCryptography() {
		return mAESCryptography;
	}
	/**
	 * hold constant ENCRYPTED_CARD_NUMBER.
	 */
	private static final String ENCRYPTED_CARD_NUMBER = "encryptCardNumber";
	/**
	 * hold constant DECRYPTED_CARD_NUMBER.
	 */
	private static final String DECRYPTED_CARD_NUMBER = "decryptCardNumber";
	
	/**
	 * @param pAESCryptography the aESCryptography to set
	 */
	public void setAESCryptography(AESCryptography pAESCryptography) {
		mAESCryptography = pAESCryptography;
	}

	/**
	 * @return the securityKey
	 */
	public String getSecurityKey() {
		return mSecurityKey;
	}

	/**
	 * @param pSecurityKey the securityKey to set
	 */
	public void setSecurityKey(String pSecurityKey) {
		mSecurityKey = pSecurityKey;
	}

	
	/**
	 * This method is used to encrypt the gift card number.
	 * @param pGiftCardNumber giftCardNumber
	 * @return String object
	 * @throws TRUIntegrationException exception
	 * @throws RepositoryException exception
	 */
	public String encryptCardNumber(String pGiftCardNumber) throws TRUIntegrationException, RepositoryException {

		if (isLoggingDebug()) {
			vlogDebug("GiftCardTokenizationImpl.encryptCardNumber : START");
		}
		if(PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(ENCRYPTED_CARD_NUMBER);
		}
		String encryptedGiftCardNumber = null;
		if (pGiftCardNumber == null) {
			if (isLoggingDebug()) {
				vlogDebug("GiftCardTokenizationImpl.tokenizeCardNumber gift card number is null hence returning.");
			}
			return null;
		}
		if(pGiftCardNumber.length() > TRUCommerceConstants.INT_ZERO && pGiftCardNumber != null){

			if (isLoggingDebug()) {
				vlogDebug("Gift card to be encrypted : " + pGiftCardNumber.substring(_4, pGiftCardNumber.length() - _4));
			}
			encryptedGiftCardNumber = getAESCryptography().encrypt(pGiftCardNumber, getSecurityKey());
		}
		if (isLoggingDebug()) {
			vlogDebug("GiftCardTokenizationImpl.encryptCardNumber : END");
		}
		if(PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(ENCRYPTED_CARD_NUMBER);
		}
		return encryptedGiftCardNumber;

	}

	/**
	 * This method is used to decrypt the gift card number.
	 * @param pEncryptedGiftCardNumber - EncryptedGiftCardNumber
	 * @return decryptedGiftCardNumber - decrypted gift card number
	 * @throws TRUIntegrationException exception
	 * @throws RepositoryException exception
	 */
	public String decryptCardNumber(String pEncryptedGiftCardNumber) throws TRUIntegrationException, RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("GiftCardTokenizationImpl.decryptCardNumber : END");
		}
		if(PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(DECRYPTED_CARD_NUMBER);
		}
		if (isLoggingDebug()) {
			vlogDebug("PCITokenizationImpl.tokenizeCardNumber : START");
		}
		String decryptedGiftCardNumber = null;
		if (pEncryptedGiftCardNumber == null) {
			if (isLoggingDebug()) {
				vlogDebug("PCITokenizationImpl.tokenizeCardNumber gift card number is null hence returning.");
			}
			return null;
		}
		if(pEncryptedGiftCardNumber.length() >0 && pEncryptedGiftCardNumber != null){
			decryptedGiftCardNumber = getAESCryptography().decrypt(pEncryptedGiftCardNumber, getSecurityKey());
		}
		if (isLoggingDebug()) {
			vlogDebug("GiftCardTokenizationImpl.decryptCardNumber : END");
		}
		if(PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(DECRYPTED_CARD_NUMBER);
		}
		return decryptedGiftCardNumber;
	}
}
