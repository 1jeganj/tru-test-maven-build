package com.tru.service.dynamo;

import java.net.InetAddress;

import atg.service.dynamo.DAFConfiguration;

/**
 * The Class TRUDAFConfiguration includes ATGSLMHostNames and ATGSLMPort. This class extends ATG OOTB DAFConfiguration.
 *
 * @author Cerabus - Chee Loo (1looc)
 * @version 1.0
 *
 */

public class TRUDAFConfiguration extends DAFConfiguration {

        public InetAddress[] mATGSLMHostNames;
        
        /**
         * @return the mATGSLMHostNames
         */
        public InetAddress[] getATGSLMHostNames() {
                return mATGSLMHostNames;
        }
        
        /**
         * @param pATGSLMHostNames the pATGSLMHostNames to set
         */
        public void setATGSLMHostNames(InetAddress[] pATGSLMHostNames) {
                mATGSLMHostNames = pATGSLMHostNames;
        }
        
        protected int mATGSLMPort;
        
        /**
         * @return the mATGSLMPort
         */
        public int getATGSLMPort() {
                return mATGSLMPort;
        }
        
        /**
         * @param pATGSLMPort the pATGSLMPort to set
         */
        public void setATGSLMPort(int pATGSLMPort) {
                mATGSLMPort = pATGSLMPort;
        }
        
}

