package com.tru.preview;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.util.SchedulableDate;

import com.tru.userprofiling.TRUCookieManager;

/**
 * Class to set the future date to GlobalPromotionsDate Component
 */
public class TRUSchedulableDate extends SchedulableDate {
	
	/**
	 * Constant to hold mPromotionTools
	 */
	private TRUCookieManager mCookieManager;

	/**
	 * @return the cookieManager
	 */
	public TRUCookieManager getCookieManager() {
		return mCookieManager;
	}



	/**
	 * @param pCookieManager the cookieManager to set
	 */
	public void setCookieManager(TRUCookieManager pCookieManager) {
		mCookieManager = pCookieManager;
	}

	/**
	 * Handle perform schedule task
	 * 
	 * @param pScheduler
	 *            the scheduler
	 * @param pJob
	 *            the job
	 */
	@Override
	public void performScheduledTask(Scheduler pScheduler, ScheduledJob pJob) {
		if (isLoggingDebug()) {
			logDebug("Enter into TRUSchedulableDate/performScheduledTask method ");
		}
			performSetTimeAsDate();
		if (isLoggingDebug()) {
			logDebug("Exit From TRUSchedulableDate/performScheduledTask method ");
		}
	}
	
	/**
	 * Handle performSetTimeAsDate
	 * 
	 */
	public void performSetTimeAsDate(){
		if (isLoggingDebug()) {
			logDebug("Enter into TRUSchedulableDate/performSetTimeAsDate method ");
		}
		setTimeAsDate(getCookieManager().getCurrentDate().getTimeAsDate());
		if (isLoggingDebug()) {
			logDebug("End into TRUSchedulableDate/performSetTimeAsDate method ");
		}
	}
}
