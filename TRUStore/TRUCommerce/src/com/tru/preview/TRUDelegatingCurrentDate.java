package com.tru.preview;

import java.util.Date;

import atg.service.util.CurrentDate;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import atg.userprofiling.preview.PreviewTools;


/**
 * This class implements a brute force optional delegation of
 * calls to a separate, typically session scoped DateTime component.
 * If there is no request, or if shouldOverride returns null,
 * do not delegate but handle the method call ourself.
 *
 * <P>
 * @version $Id$
 **/

public class TRUDelegatingCurrentDate extends CurrentDate {

	
	/**
	 * Class version string from source code control system. 
	 */
	public static final String CLASS_VERSION = "$Id$"; 


	/** A constant used as a marker in our attribute value to avoid
	 * doing expensive work repeatedly.
	 */
	final static CurrentDate NULL_DELEGATE = new CurrentDate();
	
	// Fix for PMD Warning - AvoidHardcodingRule - START
	/**
	 * Constant for PROFILE_PATH
	 */
	public static final String PROFILE_PATH = "/atg/userprofiling/Profile";
	/**
	 * Constant for SESSION_CURRENT_DATE_PATH
	 */
	public static final String SESSION_CURRENT_DATE_PATH = "/com/tru/preview/SessionCurrentDate";
	/**
	 * Constant for CURRENT_OVERRIDE_REQUEST_PATH
	 */
	public static final String CURRENT_OVERRIDE_REQUEST_PATH = "atg.service.util.CurrentOverrideRequestAttribute.current";
	// Fix for PMD Warning - AvoidHardcodingRule - END
	/**
	 * it holds the value for pPreviewTools
	 */
	private PreviewTools mPreviewTools;

	/**
	 * Sets property previewTools
	 * @param pPreviewTools
	 * 			- pPreviewTools
	 */
	public void setPreviewTools(PreviewTools pPreviewTools) {
		mPreviewTools = pPreviewTools;
	}

	/**
	 * Returns property previewTools
	 * @return mPreviewTools
	 */
	public PreviewTools getPreviewTools() {
		return mPreviewTools;
	}

	/**
	 * it holds the value for mCurrentOverrideRequestAttributeName
	 */
	// Fix for PMD Warning - AvoidHardcodingRule - START
	// private String mCurrentOverrideRequestAttributeName =
		// "atg.service.util.CurrentOverrideRequestAttribute.current";
	private String mCurrentOverrideRequestAttributeName = CURRENT_OVERRIDE_REQUEST_PATH;
	// Fix for PMD Warning - AvoidHardcodingRule - END

	/**
	 * Sets property currentOverrideRequestAttributeName
	 * @param pCurrentOverrideRequestAttributeName
	 * 				- pCurrentOverrideRequestAttributeName
	 */
	public void setCurrentOverrideRequestAttributeName(String pCurrentOverrideRequestAttributeName) {
		mCurrentOverrideRequestAttributeName = pCurrentOverrideRequestAttributeName;
	}

	/**
	 * Returns property currentOverrideRequestAttributeName
	 * @return String
	 */
	public String getCurrentOverrideRequestAttributeName() {
		return mCurrentOverrideRequestAttributeName;
	}

	/**
	 * it holds the value for mDelegateCurrentDatePath
	 */
	// Fix for PMD Warning - AvoidHardcodingRule - START
	// private String mDelegateCurrentDatePath =
	private String mDelegateCurrentDatePath = SESSION_CURRENT_DATE_PATH;
	// Fix for PMD Warning - AvoidHardcodingRule - END
	/**
	 * Sets property delegateCurrentDatePath. This is the
	 * path of the CurrentDate to which we should delegate.
	 * @param pDelegateCurrentDatePath
	 * 			- pDelegateCurrentDatePath
	 */
	public void setDelegateCurrentDatePath(String pDelegateCurrentDatePath) {
		mDelegateCurrentDatePath = pDelegateCurrentDatePath;
	}

	/**
	 * Returns the path of the CurrentData to which we should delegate.
	 * @return String
	 */
	public String getDelegateCurrentDatePath() {
		return mDelegateCurrentDatePath;
	}
	/**
	 * it holds the value of mProfilePath
	 */
	// Fix for PMD Warning - AvoidHardcodingRule - START
	//private String mProfilePath = "/atg/userprofiling/Profile";
	private String mProfilePath = PROFILE_PATH;
	// Fix for PMD Warning - AvoidHardcodingRule - END

	/**
	 * Sets property profilePath
	 * @param pProfilePath
	 * 			- pProfilePath
	 */
	public void setProfilePath(String pProfilePath) {
		mProfilePath = pProfilePath;
	}

	/**
	 * Returns property profilePath 
	 * @return String
	 */
	public String getProfilePath() {
		return mProfilePath;
	}

	/**
	 * Return whether we should override the CurrentDate component for the
	 * specified request
	 * @param pRequest
	 * 			- pRequest
	 * @return boolean
	 */
	protected boolean shouldOverride(DynamoHttpServletRequest pRequest) {
		if (pRequest == null) {
			return false;
		}
		
		/**
		 * Performance Fix :  103- Staging date based preview.
		 * if previewFlag is true it returns true.
		 * previewFlag is set to true in KLSDatePreviewServlet
		 */
		TRUCurrentDate delegate=(TRUCurrentDate) pRequest.resolveName(getDelegateCurrentDatePath(), true);
		if(delegate.isPreviewFlag()){
			if(isLoggingDebug()){
				logDebug("Preview Flag set to TRUE in shouldOverride method");
			}
			return true;
		}
		return false;
	}

	/**
	 * Return the current delegate. Returning null means there
	 * is no current delegate.
	 * @return currentDate
	 */
	protected CurrentDate getDelegate() {
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		if (request == null) {
			// assume no override if no current request
			return null;
		}

		CurrentDate delegate =
			(CurrentDate)request.getAttribute(getCurrentOverrideRequestAttributeName());

		if(isLoggingDebug()){
			logDebug("delegate: "+delegate);
		}
		if (delegate == NULL_DELEGATE) {
			return null;
		}

		if (delegate == null) {

			if(isLoggingDebug()){
				logDebug("delegate is null");
			}
			// check whether we should override before we look at the attribute,
			// in case things have changed since we stashed the old delegate.
			if (shouldOverride(request)) {
				// resolve our delegate

				if(isLoggingDebug()){
					logDebug("shouldOverride is true");
				}
				delegate =
					(CurrentDate)request.resolveName(getDelegateCurrentDatePath());

				if(isLoggingDebug()){
					logDebug("setting delegate: "+delegate);
				}
			}
			if (delegate == null) {

				if(isLoggingDebug()){
					logDebug("Storing delegate as NULL_DELEGATE");
				}
				// remember we didn't find one, so we don't do the
				// same thing over and over.
				request.setAttribute(getCurrentOverrideRequestAttributeName(),
						NULL_DELEGATE);

			}
			else {

				if(isLoggingDebug()){
					logDebug("Storing delegate for later use");
				}
				// stash it away for later use
				request.setAttribute(getCurrentOverrideRequestAttributeName(),
						delegate);
			}

		}
		return delegate;
	}

	//-------------------------------------------------------
	// Override all public methods to go to delegate
	//-------------------------------------------------------

	@Override
	public void setMinimizeDateConstruction(boolean pMinimizeDateConstruction) {
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			delegate.setMinimizeDateConstruction(pMinimizeDateConstruction);
		}
		else {
			super.setMinimizeDateConstruction(pMinimizeDateConstruction);
		}
	}



	@Override
	public boolean isMinimizeDateConstruction() {
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.isMinimizeDateConstruction();
		}
		else {
			return super.isMinimizeDateConstruction();
		}
	}

	@Override
	public long getNextDate () {
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getNextDate();
		}
		else {
			return super.getNextDate();
		}
	}


	@Override
	public long getNextHour () {
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getNextHour();
		}
		else {
			return super.getNextHour();
		}
	}


	@Override
	public long getNextMinute () {
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getNextMinute();
		}
		else {
			return super.getNextMinute();
		}
	}


	@Override
	public long getNextSecond () {
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getNextSecond();
		}
		else {
			return super.getNextSecond();
		}
	}

	@Override
	public long getTime ()
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getTime();
		}
		else {
			return super.getTime();
		}
	}

	@Override
	public void setTime (long pTime)
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			delegate.setTime(pTime);
		}
		else {
			super.setTime(pTime);
		}
	}

	@Override
	public Date getTimeAsDate ()
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getTimeAsDate();
		}
		else {
			return super.getTimeAsDate();
		}
	}

	@Override
	public java.sql.Timestamp getTimeAsTimestamp () {
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getTimeAsTimestamp();
		}
		else {
			return super.getTimeAsTimestamp();
		}
	}

	@Override
	public void setTimeAsDate (Date pDate)
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			delegate.setTimeAsDate(pDate);
		}
		else {
			super.setTimeAsDate(pDate);
		}
	}


	@Override
	public int getYear () {
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getYear();
		}
		else {
			return super.getYear();
		}
	}

	@Override
	public String getMonthName ()
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getMonthName();
		}
		else {
			return super.getMonthName();
		}
	}

	@Override
	public String getShortMonthName ()
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getShortMonthName();
		}
		else {
			return super.getShortMonthName();
		}
	}


	@Override
	public int getDate ()
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getDate();
		}
		else {
			return super.getDate();
		}
	}


	@Override
	public Date getDateAsDate ()
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getDateAsDate();
		}
		else {
			return super.getDateAsDate();
		}
	}


	@Override
	public int getHour ()
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getHour();
		}
		else {
			return super.getHour();
		}
	}


	@Override
	public Date getHourAsDate ()
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getHourAsDate();
		}
		else {
			return super.getHourAsDate();
		}
	}


	@Override
	public int getMinute ()
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getMinute();
		}
		else {
			return super.getMinute();
		}
	}


	@Override
	public Date getMinuteAsDate ()
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getMinuteAsDate();
		}
		else {
			return super.getMinuteAsDate();
		}
	}


	@Override
	public int getSecond ()
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getSecond();
		}
		else {
			return super.getSecond();
		}
	}

	@Override
	public Date getSecondAsDate ()
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getSecondAsDate();
		}
		else {
			return super.getSecondAsDate();
		}
	}


	@Override
	public int getDayOfWeek ()
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getDayOfWeek();
		}
		else {
			return super.getDayOfWeek();
		}
	}


	@Override
	public String getDayOfWeekName ()
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getDayOfWeekName();
		}
		else {
			return super.getDayOfWeekName();
		}
	}


	@Override
	public String getShortDayOfWeekName ()
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getShortDayOfWeekName();
		}
		else {
			return super.getShortDayOfWeekName();
		}
	}



	@Override
	public int getDayOfWeekInMonth ()
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getDayOfWeekInMonth();
		}
		else {
			return super.getDayOfWeekInMonth();
		}
	}


	@Override
	public int getWeekOfMonth ()
	{
		CurrentDate delegate = getDelegate();
		if (delegate != null) {
			return delegate.getWeekOfMonth();
		}
		else {
			return super.getWeekOfMonth();
		}
	}

}
