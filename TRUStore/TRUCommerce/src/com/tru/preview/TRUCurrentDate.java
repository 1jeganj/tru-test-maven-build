package com.tru.preview;


import atg.service.util.CurrentDate;



 /**
 * <P>
 * @author PA
 * @version $Id$
 **/

@SuppressWarnings("serial")
public class TRUCurrentDate extends CurrentDate {
	/**
	 * previewFlag holds the boolean flag
	 */
	private boolean mPreviewFlag;
	
	/**
	 * It returns previewFlag
	 * @return mPreviewFlag
	 */
	public boolean isPreviewFlag() {
		return mPreviewFlag;
	}

	/**
	 * It sets the mPreviewFlag
	 *@param pPreviewFlag the mPreviewFlag to set
	 */
	public void setPreviewFlag(boolean pPreviewFlag) {
		this.mPreviewFlag = pPreviewFlag;
	}

	
}
