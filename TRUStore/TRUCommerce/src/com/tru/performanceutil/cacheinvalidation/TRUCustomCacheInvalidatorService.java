package com.tru.performanceutil.cacheinvalidation;

import java.util.Enumeration;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceMap;
import atg.repository.Repository;
import atg.repository.RepositoryImpl;

import com.tru.utils.TRUClassificationsTools;

/**
 * This class provides method to invalidate the cache components.
 * 
 * @author P.A
 *
 */
public class TRUCustomCacheInvalidatorService extends GenericService{

	/**
	 * mComponentMap
	 */
	private ServiceMap mComponentMap;
	/**
	 * mFlushCacheAllInstances
	 */
	private boolean mFlushCacheAllInstances;
	/**
	 * mRepository
	 */
	private Repository mRepository;
	

	/**
	 *<b>This Method will do following Operations:</b><BR>
	 * 1) It will fetch the components. 2) Flush the cache.
	 */
	public void invalidateCache() {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUCustomCacheInvalidatorService.invalidateCache method:");
		}
		if(isFlushCacheAllInstances()){
			RepositoryImpl repositoryImpl = (RepositoryImpl) getRepository();
			repositoryImpl.invalidateCaches();
		}
		if(getComponentMap() != null){
			Enumeration enumValues = getComponentMap().elements();
			while(enumValues.hasMoreElements()) {
				Object component = enumValues.nextElement();
				if(component != null) {
					if(component instanceof atg.service.cache.Cache) {
						atg.service.cache.Cache cacheComponent = (atg.service.cache.Cache)component;
						if(isLoggingDebug()) {
							logDebug("TRUCustomCacheInvalidatorService, trying to invoke flush on service cache " + cacheComponent);
						}
						cacheComponent.flush();
						if(isLoggingDebug()) {
							logDebug("TRUCustomCacheInvalidatorService, flush completed on " + cacheComponent);
						}
					} else if(component instanceof atg.droplet.Cache) {
						atg.droplet.Cache dropletCacheComp = (atg.droplet.Cache) component;
						if(isLoggingDebug()) {
							logDebug("trying to invoke flush on droplet cache" + dropletCacheComp);
						}
						dropletCacheComp.flushCache();
						if(isLoggingDebug()) {
							logDebug("flush completed with droplet cache" + dropletCacheComp);
						}
					}
					
					else if(component instanceof TRUClassificationsTools) {
						TRUClassificationsTools  classificationTools=(TRUClassificationsTools)component;
						if(isLoggingDebug()) {
							logDebug("trying to invoke flush on  TRUClassification Tools" + classificationTools);
						}
						classificationTools.flush();
						if(isLoggingDebug()) {
							logDebug("flush completed with TRUClassification Tools" + classificationTools);
						}
					}
					
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("exiting from TRUCustomCacheInvalidatorService.invalidateCache method:");
		}
	}

	/**
	 * @return the componentMap
	 */
	public ServiceMap getComponentMap() {
		return mComponentMap;
	}

	/**
	 * @param pComponentMap the componentMap to set
	 */
	public void setComponentMap(ServiceMap pComponentMap) {
		mComponentMap = pComponentMap;
	}

	/**
	 * @return the flushCacheAllInstances
	 */
	public boolean isFlushCacheAllInstances() {
		return mFlushCacheAllInstances;
	}

	/**
	 * @param pFlushCacheAllInstances the flushCacheAllInstances to set
	 */
	public void setFlushCacheAllInstances(boolean pFlushCacheAllInstances) {
		mFlushCacheAllInstances = pFlushCacheAllInstances;
	}

	/**
	 * @return the repository
	 */
	public Repository getRepository() {
		return mRepository;
	}

	/**
	 * @param pRepository the repository to set
	 */
	public void setRepository(Repository pRepository) {
		mRepository = pRepository;
	}
}

