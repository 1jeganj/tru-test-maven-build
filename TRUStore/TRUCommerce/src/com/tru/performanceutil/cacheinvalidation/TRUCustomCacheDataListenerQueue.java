package com.tru.performanceutil.cacheinvalidation;

import atg.service.datacollection.DataListenerQueue;

/**
 *  This Class  extends DataListenerQueue .
 *  
 * @author   : PA
 * @version  : 1.0
 * 
 * */
public class TRUCustomCacheDataListenerQueue extends DataListenerQueue
{
	/**
	 * property to hold TRUDataListener componant
	 */
	private TRUCustomCacheDataListener mDataListener;

	

	/**
	 * This method Starts the service
	 */
	public  void doStartService()
 {
		if (isLoggingDebug()) {
			logDebug("Entering TRUCustomCacheDataListenerQueue.doStartService method");
		}
		synchronized (this) {
			super.doStartService();
			addDataListener(getDataListener());
		}
		if (isLoggingDebug()) {
			logDebug("Exiting TRUCustomCacheDataListenerQueue.doStartService method");
		}
	}



	/**
	 * Gets the data listener.
	 *
	 * @return the data listener
	 */
	public TRUCustomCacheDataListener getDataListener() {
		return mDataListener;
	}



	/**
	 * Sets the data listener.
	 *
	 * @param pDataListener the new data listener
	 */
	public void setDataListener(TRUCustomCacheDataListener pDataListener) {
		this.mDataListener = pDataListener;
	}
	
}

