package com.tru.performanceutil.cacheinvalidation;

import atg.nucleus.GenericService;
import atg.service.datacollection.DataListener;


/**
 *  This Class is the Implementation for DataListener and extends GenericService .
 *  
 * @author   : PA
 * @version  : 1.0
 * 
 * */
public class TRUCustomCacheDataListener extends GenericService implements DataListener
{
	
	private TRUCustomCacheInvalidatorService mCustomCacheInvalidatorService;
	
	/**
	 * This method calls the logic for logging info 
	 * @param pParamObject the Object
	 */
	public void addDataItem(Object pParamObject) {
		if(isLoggingDebug()){
			logDebug("TRUCustomCacheDataListener.addDataItem method : "+ pParamObject);
		}
			getCustomCacheInvalidatorService().invalidateCache();
	}


	/**
	 * Gets the custom cache invalidator service.
	 *
	 * @return the custom cache invalidator service
	 */
	public TRUCustomCacheInvalidatorService getCustomCacheInvalidatorService() {
		return mCustomCacheInvalidatorService;
	}

	
	/**
	 * Sets the custom cache invalidator service.
	 *
	 * @param pCustomCacheInvalidatorService the new custom cache invalidator service
	 */
	public void setCustomCacheInvalidatorService(TRUCustomCacheInvalidatorService pCustomCacheInvalidatorService) {
		this.mCustomCacheInvalidatorService = pCustomCacheInvalidatorService;
	}
	
}

