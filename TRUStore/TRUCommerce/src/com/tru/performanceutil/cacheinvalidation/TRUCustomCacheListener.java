package com.tru.performanceutil.cacheinvalidation;

import atg.nucleus.GenericService;

/**
 *  This Class  extends GenericService and performs the cache flushing during deployment.
 *  
 * @author   : PA
 * @version  : 1.0
 * 
 * */
public class TRUCustomCacheListener extends GenericService
{
	/**
	 * Property to hold TRUDataListenerQueue Component
	 */
	private TRUCustomCacheDataListenerQueue mDataListenerQueue;


	
	/**
	 * Invoke cache flush.
	 */
	public void invokeCacheFlush(){
		if(isLoggingDebug()){
			logDebug("Inside TRUCustomCacheListener.sendMessage method ");
		}
		Object obj =new Object();
		sendMessage(obj);
		if(isLoggingDebug()){
			logDebug("Exiting TRUCustomCacheListener.sendMessage method ");
		}	
	}
	
	/**
	 * This method sends the Object message to the IamDataListenerQueue for Logging
	 * 
	 * @param pObject the Object
	 */
	public void sendMessage(Object pObject){
		if(isLoggingDebug()){
			logDebug("Inside TRUCustomCacheListener.sendMessage method ");
		}
		getDataListenerQueue().addDataItem(pObject);
		if(isLoggingDebug()){
			logDebug("Exiting TRUCustomCacheListener.sendMessage method ");
		}
	}



	/**
	 * Gets the data listener queue.
	 *
	 * @return the data listener queue
	 */
	public TRUCustomCacheDataListenerQueue getDataListenerQueue() {
		return mDataListenerQueue;
	}



	/**
	 * Sets the data listener queue.
	 *
	 * @param pDataListenerQueue the new data listener queue
	 */
	public void setDataListenerQueue(TRUCustomCacheDataListenerQueue pDataListenerQueue) {
		this.mDataListenerQueue = pDataListenerQueue;
	}
}

