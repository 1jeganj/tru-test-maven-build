package com.tru.cms;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.tru.commerce.TRUCommerceConstants;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

/**
 * The Class TRUDeleteIncrementalUpdateData.
 */
public class TRUDeleteIncrementalUpdateData extends GenericService {
	/**
	 * property to hold dataSource
	 */
	private DataSource mDataSource;
	
	/** The m delete query. */
	private String mDeleteQuery;
	
	private String mCountQuery;
	/**
	 * Populate promotion data.
	 *
	 * @param pSkuId the sku id
	 * @return the list
	 */
	public void deleteIncrementalData(int pThresholdLimit){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUDeleteIncrementalUpdateData  method: deleteIncrementalData]");
		}
			boolean isDeleteRequired=CheckWhetherThresholdExceeds(pThresholdLimit);
			if(!isDeleteRequired){
				return;
			}
			Connection connection = null;
			String query = null;
			PreparedStatement ps = null;
			try {
				connection = getDataSource().getConnection();
				if (connection != null) {
					query = getDeleteQuery();
					if (!StringUtils.isEmpty(query)) {
						ps = connection.prepareStatement(query);
						if (ps != null) {
							ps.executeQuery();
						}
					}
				}
			} catch (SQLException sQLException) {
				vlogError("SQLException : {0}", sQLException);
			} finally {
				try {
					if (ps != null) {
						ps.close();
					}
					if (connection != null) {
						connection.close();
					}
				} catch (SQLException sQLException) {
					vlogError("SQLException : {0}", sQLException);
				}
			}
		if (isLoggingDebug()) {
			logDebug("Exit into [Class: TRUDeleteIncrementalUpdateData  method: deleteIncrementalData]");
		}
	}
	
	
	/**
	 * Check whether threshold exceeds.
	 *
	 * @param pThresholdLimit the threshold limit
	 * @return true, if successful
	 */
	public boolean CheckWhetherThresholdExceeds(int pThresholdLimit){

		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUDeleteIncrementalUpdateData  method: CheckWhetherThresholdExceeds]");
		}
			Connection connection = null;
			String query = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			int count=0;
			try {
				connection = getDataSource().getConnection();
				if (connection != null) {
					query = getCountQuery();
					if (!StringUtils.isEmpty(query)) {
						ps = connection.prepareStatement(query);
						if (ps != null) {
							rs=ps.executeQuery();
							if (rs != null) {
								while (rs.next()) {
									count=rs.getInt(TRUCommerceConstants.INT_ONE);
									break;
								}
								if(count>pThresholdLimit){
									return true;
								}
							}
						}
					}
				}
			} catch (SQLException sQLException) {
				vlogError("SQLException : {0}", sQLException);
			} finally {
				try {
					if (ps != null) {
						ps.close();
					}
					if (connection != null) {
						connection.close();
					}
				} catch (SQLException sQLException) {
					vlogError("SQLException : {0}", sQLException);
				}
			}
		if (isLoggingDebug()) {
			logDebug("Exit into [Class: TRUDeleteIncrementalUpdateData  method: CheckWhetherThresholdExceeds]");
		}
	
		
		return false;
	}
	/**
	 * @return the dataSource
	 */
	public DataSource getDataSource() {
		return mDataSource;
	}

	/**
	 * @param pDataSource
	 *            the dataSource to set
	 */
	public void setDataSource(DataSource pDataSource) {
		mDataSource = pDataSource;
	}

	/**
	 * Gets the delete query.
	 *
	 * @return the delete query
	 */
	public String getDeleteQuery() {
		return mDeleteQuery;
	}

	/**
	 * Sets the delete query.
	 *
	 * @param pDeleteQuery the new delete query
	 */
	public void setDeleteQuery(String pDeleteQuery) {
		this.mDeleteQuery = pDeleteQuery;
	}

	public String getCountQuery() {
		return mCountQuery;
	}

	public void setCountQuery(String mCountQuery) {
		this.mCountQuery = mCountQuery;
	}

}
