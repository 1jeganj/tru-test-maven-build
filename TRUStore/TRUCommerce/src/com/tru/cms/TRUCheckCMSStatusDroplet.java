package com.tru.cms;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.catalog.StandardCatalogMaintenance;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.TRUConstants;

/**
 * The Class TRUCheckCMSStatusDroplet.
 */
public class TRUCheckCMSStatusDroplet extends DynamoServlet {
	
	/** The m catalog maintenance service. */
	private StandardCatalogMaintenance mCatalogMaintenanceService;

	/**
	 * Method to check the CMS status
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		boolean isLocked = getCatalogMaintenanceService().getClientLockManager().isLocallyLocked("CatalogMaintenance");
		pRequest.setParameter("isLocked", isLocked);
		pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);

	}

	/**
	 * Sets the catalog maintenance service.
	 * 
	 * @param pCatalogMaintenanceService
	 *            the new catalog maintenance service
	 */
	public void setCatalogMaintenanceService(StandardCatalogMaintenance pCatalogMaintenanceService) {
		this.mCatalogMaintenanceService = pCatalogMaintenanceService;
	}

	/**
	 * Gets the catalog maintenance service.
	 * 
	 * @return the catalog maintenance service
	 */
	public StandardCatalogMaintenance getCatalogMaintenanceService() {
		return this.mCatalogMaintenanceService;
	}
}
