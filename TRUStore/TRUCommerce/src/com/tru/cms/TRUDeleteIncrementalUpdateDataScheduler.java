package com.tru.cms;

import java.io.IOException;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

	/**
	 * The Class TRUDeleteIncrementalUpdateDataScheduler.
	 */
public class TRUDeleteIncrementalUpdateDataScheduler extends SingletonSchedulableService {
	
	/** The m enabled. */
	private boolean mEnabled;
	
	/** The m delete incremental update data. */
	private TRUDeleteIncrementalUpdateData mDeleteIncrementalUpdateData;
	
	/** The m threshold count. */
	private int mThresholdCount;
	/**
	 * To generate the data of the profile which is updated in last 15 mins.
	 * 
	 * @param pParamScheduler
	 *            the param scheduler
	 * @param pParamScheduledJob
	 *            the param scheduled job
	 */
	@Override
	public void doScheduledTask(Scheduler pParamScheduler, ScheduledJob pParamScheduledJob) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUDeleteIncrementalUpdateDataScheduler.doScheduledTask() method");
		}
		if (isEnabled()) {
			try {
				deleteIncrementalData();
			} catch (IOException e) {
				vlogError(
						"Exception occured while processing TRUDeleteIncrementalUpdateDataScheduler.doScheduledTask() method : ",
						e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUDeleteIncrementalUpdateDataScheduler.doScheduledTask() method");
		}
	}

	/**
	 * Generate profile data.
	 * 
	 * @throws IOException
	 */
	public void deleteIncrementalData() throws IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUDeleteIncrementalUpdateDataScheduler.deleteIncrementalData() method");
		}
		getDeleteIncrementalUpdateData().deleteIncrementalData(getThresholdCount());
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUDeleteIncrementalUpdateDataScheduler.deleteIncrementalData() method");
		}
	}

	/**
	 * Checks if is enabled.
	 * 
	 * @return true, if is enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets the enabled.
	 * 
	 * @param pEnabled
	 *            the new enabled
	 */
	public void setEnabled(boolean pEnabled) {
		this.mEnabled = pEnabled;
	}

	/**
	 * Gets the delete incremental update data.
	 *
	 * @return the delete incremental update data
	 */
	public TRUDeleteIncrementalUpdateData getDeleteIncrementalUpdateData() {
		return mDeleteIncrementalUpdateData;
	}

	/**
	 * Sets the delete incremental update data.
	 *
	 * @param mDeleteIncrementalUpdateData the new delete incremental update data
	 */
	public void setDeleteIncrementalUpdateData(TRUDeleteIncrementalUpdateData mDeleteIncrementalUpdateData) {
		this.mDeleteIncrementalUpdateData = mDeleteIncrementalUpdateData;
	}

	/**
	 * Gets the threshold count.
	 *
	 * @return the threshold count
	 */
	public int getThresholdCount() {
		return mThresholdCount;
	}

	/**
	 * Sets the threshold count.
	 *
	 * @param pThresholdCount the new threshold count
	 */
	public void setThresholdCount(int pThresholdCount) {
		this.mThresholdCount = pThresholdCount;
	}

}

