package com.tru.cms;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.catalog.RunServiceFormHandler;
import atg.service.jdbc.SwitchingDataSource;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.vfs.switchable.SwitchableLocalFileSystem;

// TODO: Auto-generated Javadoc
/**
 * Class will run cms activities.
 */
public class TRUCMSPerformFormHandler extends RunServiceFormHandler {
	
	/** The m switching data source. */
	private SwitchingDataSource mSwitchingDataSource;
	
	/**
	 * Gets the switching data source.
	 *
	 * @return the switching data source
	 */
	public SwitchingDataSource getSwitchingDataSource() {
		return mSwitchingDataSource;
	}

	/**
	 * Sets the switching data source.
	 *
	 * @param pSwitchingDataSource the new switching data source
	 */
	public void setSwitchingDataSource(SwitchingDataSource pSwitchingDataSource) {
		this.mSwitchingDataSource = pSwitchingDataSource;
	}

	/**
	 * Gets the config file system.
	 *
	 * @return the config file system
	 */
	public SwitchableLocalFileSystem getConfigFileSystem() {
		return mConfigFileSystem;
	}

	/**
	 * Sets the config file system.
	 *
	 * @param pConfigFileSystem the new config file system
	 */
	public void setConfigFileSystem(SwitchableLocalFileSystem pConfigFileSystem) {
		this.mConfigFileSystem = pConfigFileSystem;
	}

	/** The m config file system. */
	private SwitchableLocalFileSystem mConfigFileSystem;
	
	/** The cms error url. */
	private String cmsErrorURL;
	
	/** The cms success url. */
	private String cmsSuccessURL;
	
	/**
	 * Gets the cms error url.
	 *
	 * @return the cms error url
	 */
	public String getCmsErrorURL() {
		return cmsErrorURL;
	}

	/**
	 * Sets the cms error url.
	 *
	 * @param cmsErrorURL the new cms error url
	 */
	public void setCmsErrorURL(String cmsErrorURL) {
		this.cmsErrorURL = cmsErrorURL;
	}

	/**
	 * Gets the cms success url.
	 *
	 * @return the cms success url
	 */
	public String getCmsSuccessURL() {
		return cmsSuccessURL;
	}

	/**
	 * Sets the cms success url.
	 *
	 * @param cmsSuccessURL the new cms success url
	 */
	public void setCmsSuccessURL(String cmsSuccessURL) {
		this.cmsSuccessURL = cmsSuccessURL;
	}

	/** The m perform switch. */
	private boolean mPerformSwitch;
	
	
	/**
	 * Overridden method to perform switch and start the service
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @return true, if successful
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public boolean handleRunProcess(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCMSPerformFormHandler.RunProcess() ");
		}
		if(isPerformSwitch()){
			String CurrentDataSourceName = getSwitchingDataSource().getCurrentDataSourceName();
			if (isLoggingDebug()) {
				vlogDebug("CurrentDataSourceName for SwitchingDataSource Before CMS Activity :{0}", CurrentDataSourceName);
			}
			String LiveDataSourceName = getConfigFileSystem().getLiveDataStoreName();
			if (isLoggingDebug()) {
				vlogDebug("LiveDataStoreName for ConfigFileSystem Before CMS Activity:{0}", LiveDataSourceName);
			}
			try {
					getSwitchingDataSource().prepareSwitch();
					getSwitchingDataSource().performSwitch();
					String CurrentDataSourceName1 = getSwitchingDataSource().getCurrentDataSourceName();
					if (isLoggingDebug()) {
						vlogDebug("CurrentDataSourceName for SwitchingDataSource After switch datasource:{0}", CurrentDataSourceName1);
					}
					
					getConfigFileSystem().prepareSwitch();
					getConfigFileSystem().performSwitch();
					String LiveDataSourceName1 = getConfigFileSystem().getLiveDataStoreName();
					super.handleRunProcess(pRequest, pResponse);
					if (isLoggingDebug()) {
						vlogDebug("LiveDataStoreName for ConfigFileSystem After configfile swith:{0}", LiveDataSourceName1);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
		}
		else{
			super.handleRunProcess(pRequest, pResponse);			
		}
		
		
			
		if (isLoggingDebug()) {
			logDebug("End: TRUCMSPerformFormHandler.RunProcess() ");
		}
		return checkFormRedirect(getRunProcessSuccessURL(), getRunProcessErrorURL(), pRequest, pResponse);
	}

	

	/**
	 * Checks if is perform switch.
	 *
	 * @return true, if is perform switch
	 */
	public boolean isPerformSwitch() {
		return mPerformSwitch;
	}

	/**
	 * Sets the perform switch.
	 *
	 * @param pPerformSwitch the new perform switch
	 */
	public void setPerformSwitch(boolean pPerformSwitch) {
		this.mPerformSwitch = pPerformSwitch;
	}
	
}
