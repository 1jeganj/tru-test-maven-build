package com.tru.integrations;

import java.util.List;

import atg.nucleus.GenericService;

/**
 * Configuration for holding Integration names for both store and sos.
 * also specifies integration properties
 * @author PA
 * @version 1.0
 */
public class TRUIntegrationPropertiesConfig extends GenericService{
	/** property to hold mDefaultTRUSiteId. */
	private String mDefaultTRUSiteId;
	/** property to hold mCouponServiceIntegrationName. */
	private String mCouponServiceIntegrationName;
	/** property to hold mAddressDoctorIntegrationName. */
	private String mAddressDoctorIntegrationName;
	/** property to hold mEpslonIntegrationName. */
	private String mEpslonIntegrationName;
	/** property to hold mHookLogicIntegrationName. */
	private String mHookLogicIntegrationName;
	/** property to hold mPowerReviewsIntegrationName. */
	private String mPowerReviewsIntegrationName;
	/** property to hold mIntegrationNamePropertyName. */
	private String mIntegrationNamePropertyName;
	/** property to hold mEnabledPropertyName. */
	private String mEnabledPropertyName;
	/** property to hold mTruIntegrationsPropertyName. */
	private String mTruIntegrationsPropertyName;
	/** property to hold mTaxwareIntegrationName. */
	private String mTaxwareIntegrationName;
	/** property to hold mCertonaIntegrationName. */
	private String mCertonaIntegrationName;
	/** Holds mSosConfiguredURLs value. */
	private List<String> mSosConfiguredURLs;
	/** property to hold mCardinalIntegrationName. */
	private String mCardinalIntegrationName;
	/** property to hold  mRadialCreditCardIntegrationName. */
	private String mRadialCreditCardIntegrationName;
	/** property to hold mRadialSoftAllocationIntegrationName. */
	private String mRadialSoftAllocationIntegrationName;
	/** The Radial gift card integration name. */
	private String mRadialGiftCardIntegrationName;
	/** The Radial pay pal integration name. */
	private String mRadialPayPalIntegrationName;
	/** The Radial apple pay integration name. */
	private String mRadialApplePayIntegrationName;
	/** The Radial thread naming servlet name. */
	private String mThreadNamingServlet;
	
			
	/**
	 * @return the mThreadNamingServlet
	 */
	public String getThreadNamingServlet() {
		return mThreadNamingServlet;
	}
	/**
	 * @param pThreadNamingServlet the mThreadNamingServlet to set
	 */
	public void setThreadNamingServlet(String pThreadNamingServlet) {
		mThreadNamingServlet = pThreadNamingServlet;
	}
	/**
	 * @return the radialGiftCardIntegrationName
	 */
	public String getRadialGiftCardIntegrationName() {
		return mRadialGiftCardIntegrationName;
	}
	/**
	 * @param pRadialGiftCardIntegrationName the radialGiftCardIntegrationName to set
	 */
	public void setRadialGiftCardIntegrationName(
			String pRadialGiftCardIntegrationName) {
		mRadialGiftCardIntegrationName = pRadialGiftCardIntegrationName;
	}
	/**
	 * @return the radialPayPalIntegrationName
	 */
	public String getRadialPayPalIntegrationName() {
		return mRadialPayPalIntegrationName;
	}
	/**
	 * @param pRadialPayPalIntegrationName the radialPayPalIntegrationName to set
	 */
	public void setRadialPayPalIntegrationName(String pRadialPayPalIntegrationName) {
		mRadialPayPalIntegrationName = pRadialPayPalIntegrationName;
	}
	/**
	 * @return the radialApplePayIntegrationName
	 */
	public String getRadialApplePayIntegrationName() {
		return mRadialApplePayIntegrationName;
	}
	/**
	 * @param pRadialApplePayIntegrationName the radialApplePayIntegrationName to set
	 */
	public void setRadialApplePayIntegrationName(
			String pRadialApplePayIntegrationName) {
		mRadialApplePayIntegrationName = pRadialApplePayIntegrationName;
	}
	/**
	 * @return the radialCreditCardIntegrationName
	 */
	public String getRadialCreditCardIntegrationName() {
		return mRadialCreditCardIntegrationName;
	}
	/**
	 * @param pRadialCreditCardIntegrationName the radialCreditCardIntegrationName to set
	 */
	public void setRadialCreditCardIntegrationName(
			String pRadialCreditCardIntegrationName) {
		mRadialCreditCardIntegrationName = pRadialCreditCardIntegrationName;
	}
	/**
	 * @return the radialSoftAllocationIntegrationName
	 */
	public String getRadialSoftAllocationIntegrationName() {
		return mRadialSoftAllocationIntegrationName;
	}
	/**
	 * @param pRadialSoftAllocationIntegrationName the radialSoftAllocationIntegrationName to set
	 */
	public void setRadialSoftAllocationIntegrationName(
			String pRadialSoftAllocationIntegrationName) {
		mRadialSoftAllocationIntegrationName = pRadialSoftAllocationIntegrationName;
	}
	/**
	 * @return the truIntegrationsPropertyName
	 */
	public String getTruIntegrationsPropertyName() {
		return mTruIntegrationsPropertyName;
	}
	/**
	 * @param pTruIntegrationsPropertyName the truIntegrationsPropertyName to set
	 */
	public void setTruIntegrationsPropertyName(String pTruIntegrationsPropertyName) {
		this.mTruIntegrationsPropertyName = pTruIntegrationsPropertyName;
	}
	
	/**
	 * @return the defaultTRUSiteId
	 */
	public String getDefaultTRUSiteId() {
		return mDefaultTRUSiteId;
	}
	/**
	 * @param pDefaultTRUSiteId the defaultTRUSiteId to set
	 */
	public void setDefaultTRUSiteId(String pDefaultTRUSiteId) {
		mDefaultTRUSiteId = pDefaultTRUSiteId;
	}
	/**
	 * @return the couponServiceIntegrationName
	 */
	public String getCouponServiceIntegrationName() {
		return mCouponServiceIntegrationName;
	}
	/**
	 * @param pCouponServiceIntegrationName the couponServiceIntegrationName to set
	 */
	public void setCouponServiceIntegrationName(String pCouponServiceIntegrationName) {
		mCouponServiceIntegrationName = pCouponServiceIntegrationName;
	}
	/**
	 * @return the addressDoctorIntegrationName
	 */
	public String getAddressDoctorIntegrationName() {
		return mAddressDoctorIntegrationName;
	}
	/**
	 * @param pAddressDoctorIntegrationName the addressDoctorIntegrationName to set
	 */
	public void setAddressDoctorIntegrationName(String pAddressDoctorIntegrationName) {
		mAddressDoctorIntegrationName = pAddressDoctorIntegrationName;
	}
	/**
	 * @return the epslonIntegrationName
	 */
	public String getEpslonIntegrationName() {
		return mEpslonIntegrationName;
	}
	/**
	 * @param pEpslonIntegrationName the epslonIntegrationName to set
	 */
	public void setEpslonIntegrationName(String pEpslonIntegrationName) {
		mEpslonIntegrationName = pEpslonIntegrationName;
	}
	/**
	 * @return the hookLogicIntegrationName
	 */
	public String getHookLogicIntegrationName() {
		return mHookLogicIntegrationName;
	}
	/**
	 * @param pHookLogicIntegrationName the hookLogicIntegrationName to set
	 */
	public void setHookLogicIntegrationName(String pHookLogicIntegrationName) {
		mHookLogicIntegrationName = pHookLogicIntegrationName;
	}
	/**
	 * @return the powerReviewsIntegrationName
	 */
	public String getPowerReviewsIntegrationName() {
		return mPowerReviewsIntegrationName;
	}
	/**
	 * @param pPowerReviewsIntegrationName the powerReviewsIntegrationName to set
	 */
	public void setPowerReviewsIntegrationName(String pPowerReviewsIntegrationName) {
		mPowerReviewsIntegrationName = pPowerReviewsIntegrationName;
	}
	
	/**
	 * @return the integrationNamePropertyName
	 */
	public String getIntegrationNamePropertyName() {
		return mIntegrationNamePropertyName;
	}
	/**
	 * @param pIntegrationNamePropertyName the integrationNamePropertyName to set
	 */
	public void setIntegrationNamePropertyName(String pIntegrationNamePropertyName) {
		mIntegrationNamePropertyName = pIntegrationNamePropertyName;
	}
	/**
	 * @return the enabledPropertyName
	 */
	public String getEnabledPropertyName() {
		return mEnabledPropertyName;
	}
	/**
	 * @param pEnabledPropertyName the enabledPropertyName to set
	 */
	public void setEnabledPropertyName(String pEnabledPropertyName) {
		mEnabledPropertyName = pEnabledPropertyName;
	}
	
	/**
	 * @return the taxwareIntegrationName
	 */
	public String getTaxwareIntegrationName() {
		return mTaxwareIntegrationName;
	}
	/**
	 * @param pTaxwareIntegrationName the taxwareIntegrationName to set
	 */
	public void setTaxwareIntegrationName(String pTaxwareIntegrationName) {
		mTaxwareIntegrationName = pTaxwareIntegrationName;
	}
	
	/**
	 * @return the mSosConfiguredURLs - sosConfiguredURLs
	 */
	public List<String> getSosConfiguredURLs() {
		return mSosConfiguredURLs;
	}

	/**
	 * Sets the sos configured URLS.
	 *
	 * @param pSosConfiguredURLs the sos configured URLS
	 */
	public void setSosConfiguredURLs(List<String> pSosConfiguredURLs) {
		mSosConfiguredURLs = pSosConfiguredURLs;
	}	
	/**
	 * @return the certonaIntegrationName
	 */
	public String getCertonaIntegrationName() {
		return mCertonaIntegrationName;
	}
	/**
	 * @param pCertonaIntegrationName the certonaIntegrationName to set
	 */
	public void setCertonaIntegrationName(String pCertonaIntegrationName) {
		mCertonaIntegrationName = pCertonaIntegrationName;
	}
	/**
	 * @return the cardinalIntegrationName
	 */
	public String getCardinalIntegrationName() {
		return mCardinalIntegrationName;
	}
	/**
	 * @param pCardinalIntegrationName the cardinalIntegrationName to set
	 */
	public void setCardinalIntegrationName(String pCardinalIntegrationName) {
		mCardinalIntegrationName = pCardinalIntegrationName;
	}
	
}
