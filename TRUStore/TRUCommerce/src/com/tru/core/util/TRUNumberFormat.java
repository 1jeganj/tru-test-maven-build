package com.tru.core.util;

import atg.core.util.NumberFormat;

import com.tru.common.TRUConstants;

/**
 * The Class TRUNumberFormat.
 */
public class TRUNumberFormat extends NumberFormat {
	
	/**
	 * Format credit card number.
	 *
	 * @param pCreditCardNumber the credit card number
	 * @param pMaskCharacter the mask character
	 * @param pNumberCharactersUnmasked the number characters unmasked
	 * @param pGroupingSize the grouping size
	 * @return the string
	 */
	public static String formatCreditCardNumber(String pCreditCardNumber, String pMaskCharacter, int pNumberCharactersUnmasked, int pGroupingSize)
	  {
	    String maskCharacter = pMaskCharacter;
	    int numUnmasked = pNumberCharactersUnmasked;
	    int groupingSize = pGroupingSize;

	    StringBuffer creditCardNumber = new StringBuffer(pCreditCardNumber);
	    int sizeOfCreditCard = creditCardNumber.length();


	    if (sizeOfCreditCard < numUnmasked) {
	      numUnmasked = sizeOfCreditCard;
	    }
	    int numCharsToMask = sizeOfCreditCard - numUnmasked;

	    for (int i = TRUConstants.ZERO; i < numCharsToMask; ++i) {
	        creditCardNumber.replace(i, i + TRUConstants.SIZE_ONE, maskCharacter);
	    }
	    if (groupingSize >TRUConstants.ZERO) {
	      StringBuffer editCardNumber = new StringBuffer();
	      for (int i = TRUConstants.ZERO; i < sizeOfCreditCard; ++i) {
	        if (creditCardNumber.charAt(i) != TRUConstants.BLANK_CHARACTER) {
	          editCardNumber.append(creditCardNumber.charAt(i));
	        }
	      }
	      int sizeOfEditCard = editCardNumber.length();
	      for (int i = 0; i < sizeOfEditCard; ++i) {
	        if ((i + TRUConstants.SIZE_ONE) % groupingSize != TRUConstants.ZERO) {
	        	continue; 
	        }
	        editCardNumber.insert(sizeOfEditCard - TRUConstants.SIZE_ONE - i, TRUConstants.WHITE_SPACE);
	      }
	      return editCardNumber.toString();
	    }

	    return creditCardNumber.toString();
	  }
}
