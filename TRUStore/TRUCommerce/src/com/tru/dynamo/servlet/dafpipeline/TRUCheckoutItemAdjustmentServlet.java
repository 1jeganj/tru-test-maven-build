/**
 * 
 */
package com.tru.dynamo.servlet.dafpipeline;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.purchase.ShippingGroupMapContainer;
import atg.commerce.util.NoLockNameException;
import atg.commerce.util.TransactionLockService;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.naming.ComponentName;
import atg.repository.RepositoryItem;
import atg.service.lockmanager.DeadlockException;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.InsertableServletImpl;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUGiftCard;
import com.tru.commerce.order.TRUOrderHolder;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.purchase.TRUPurchaseProcessHelper;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.common.TRUConstants;
import com.tru.common.TRULockManager;
import com.tru.common.TRUUserSession;
import com.tru.userprofiling.TRUCookieManager;

/**
 * This Class is used to adjust the Order Billing and Payment details.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUCheckoutItemAdjustmentServlet extends InsertableServletImpl {

	/** The m purchase process helper. */
	private TRUPurchaseProcessHelper mPurchaseProcessHelper;
	/** property to Hold shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;
	/** Holds property mEnable . */
	private boolean mEnabled;

	/** property to Hold SHOPPING_CART_COMPONENT_NAME. */
	public static final ComponentName SHOPPING_CART_COMPONENT_NAME = ComponentName
			.getComponentName(TRUCommerceConstants.SHOPPING_CART_COMPONENT_NAME);

	/** The Constant TRU_SESSION_SCOPE_COMPONENT_NAME. */
	public static final  ComponentName TRU_SESSION_SCOPE_COMPONENT_NAME = ComponentName
			.getComponentName(TRUCommerceConstants.TRU_SESSION_SCOPE_COMPONENT_NAME);

	
	/** The Constant TRU_SESSION_SCOPE_COMPONENT_NAME. */
	public static final  ComponentName SHIPPINGGROUP_CONTAINER_SERVICE_COMPONENT_NAME = ComponentName
			.getComponentName(TRUCommerceConstants.SHIPPINGGROUP_CONTAINER_SERVICE_COMPONENT_NAME);
	
	/** This holds the reference for tru configuration. */
	private TRULockManager mLockManager;
	
	/**
	 * @return the lockManager
	 */
	public TRULockManager getLockManager() {
		return mLockManager;
	}

	/**
	 * @param pLockManager the lockManager to set
	 */
	public void setLockManager(TRULockManager pLockManager) {
		mLockManager = pLockManager;
	}
	
	/**
	 * This servlet is used to remove the items from the current order for which sku or product is soft deleted from the catalog.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ServletException
	 *             the servlet exception
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException,
	ServletException {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUCheckoutItemAdjustmentServlet service()");
		}
		if(!isEnabled()) {
			passRequest(pRequest, pResponse);
			if (isLoggingDebug()) {
				logDebug("Exit from TRUCheckoutItemAdjustmentServlet service() since isEnabled is false");
			}
		}else{
			TRUUserSession userSession = (TRUUserSession) pRequest.resolveName(TRU_SESSION_SCOPE_COMPONENT_NAME);
			Date userSessionStartTime = userSession.getUserSessionStartTime();
			TRUOrderManager  orderManager=(TRUOrderManager) getPurchaseProcessHelper().getOrderManager();
			if(userSessionStartTime == null) {
				userSession.setUserSessionStartTime(new Date());
			}
			TRUCookieManager cookieManager = (TRUCookieManager) getPurchaseProcessHelper().getProfileTools().getCookieManager();
			if(cookieManager.getCookieFromRequest(TRUConstants.CUST_IP_COOKIE, pRequest) == null){
				String trueClientIpAddress = orderManager.getTrueClientIpAddress(pRequest);
				cookieManager.generateCheckoutCookie(pRequest, pResponse, TRUConstants.CUST_IP_COOKIE,trueClientIpAddress);
			}

			String isCartPage = pRequest.getParameter(getShoppingCartUtils().getConfiguration().getShoppingCartParameter());
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUCheckoutItemAdjustmentServlet::@method::service() : Request URI {0}", pRequest.getRequestURI());
				vlogDebug("@Class::TRUCheckoutItemAdjustmentServlet::@method::service() : isCartPage {0}", isCartPage);
			}
			if (null != isCartPage && Boolean.TRUE.equals(Boolean.valueOf(isCartPage))) {
				TRUOrderHolder shoppingCart = (TRUOrderHolder) pRequest.resolveName(SHOPPING_CART_COMPONENT_NAME);
				ShippingGroupMapContainer shippingGroupMapContainerService = (ShippingGroupMapContainer) pRequest.resolveName(SHIPPINGGROUP_CONTAINER_SERVICE_COMPONENT_NAME);
				if (null != shoppingCart && null != shoppingCart.getCurrent()) {
					TRUOrderImpl order = (TRUOrderImpl) shoppingCart.getCurrent();
					if (order.getCommerceItemCount() > 0) {
						boolean rollback = false;
						TransactionDemarcation td = new TransactionDemarcation();
						TransactionLockService lockService = null;
						try {
							lockService = getLockManager().acquireLock();
							td.begin(getPurchaseProcessHelper().getTransactionManager(), TransactionDemarcation.REQUIRED);
							synchronized (order) {
								removeApplepayInfo(order);
								if(orderManager !=null){
									orderManager.updateOrderBillingAddress(shoppingCart, order,shippingGroupMapContainerService);
								}
								if(getShoppingCartUtils().getConfiguration().isTenderEligibilityCheck()){
									checkTenderTypePromotionEligibility(shoppingCart,
											order);
								}	
								if(((TRUOrderImpl)order).isRepriceRequiredOnCart()){
									getPurchaseProcessHelper().runProcessRepriceOrder(getPurchaseProcessHelper().getDeleteItemsFromOrderPricingOp(),
											order, null, null, shoppingCart.getProfile(), null, null);
									((TRUOrderImpl)order).setRepriceRequiredOnCart(Boolean.FALSE);
									getPurchaseProcessHelper().getOrderManager().updateOrder(order);				
								}
							}
						} catch (CommerceItemNotFoundException e) {
							if (isLoggingError()) {
								vlogError("CommerceItemNotFoundException in TRUCheckoutItemAdjustmentServlet.service()::", e);
							}
							rollback = true;
						} catch (InvalidParameterException e) {
							if (isLoggingError()) {
								vlogError("InvalidParameterException in TRUCheckoutItemAdjustmentServlet.service()::", e);
							}
							rollback = true;
						} catch (CommerceException e) {
							rollback = true;
							if (isLoggingError()) {
								vlogError("CommerceException in TRUCheckoutItemAdjustmentServlet.service()::", e);
							}
						} catch (NoLockNameException ne) {
							if (isLoggingError()) {
								vlogError("NoLockNameException in @method: TRUCheckoutItemAdjustmentServlet.service(): {0}", ne);
							}
						} catch (DeadlockException de) {
							if (isLoggingError()) {
								vlogError("DeadlockException in @method: TRUCheckoutItemAdjustmentServlet.service(): {0}", de);
							}
						} catch (RunProcessException re) {
							if (isLoggingError()) {
								vlogError("RunProcessException in @method: TRUCheckoutItemAdjustmentServlet.service(): {0}", re);
							}
						} catch (TransactionDemarcationException e) {
							rollback = true;
							if (isLoggingError()) {
								vlogError(" TransactionDemarcationException in TRUCheckoutItemAdjustmentServlet.service()::", e);
							}
						} finally {
							try {
								td.end(rollback);
							} catch (TransactionDemarcationException e) {
								if (isLoggingError()) {
									vlogError(
											" TransactionDemarcationException in @Class::TRUCheckoutItemAdjustmentServlet::@method::service() :",
											e);
								}
							}
							if (lockService != null) {
								getLockManager().releaseLock(lockService);
							}
						}

					}
				}
			}
			passRequest(pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting into TRUCheckoutItemAdjustmentServlet service()");
		}
	}
	
	/**
	 * <p>
	 * This method will remove applepay payment gropus
	 * </p>.
	 *
	 * @param pOrder the order
	 * @throws CommerceException - IF any
	 */
	private void removeApplepayInfo(TRUOrderImpl pOrder) throws CommerceException {
		if(pOrder.isApplepay()){
			TRUOrderManager  orderManager=(TRUOrderManager) getPurchaseProcessHelper().getOrderManager();
			orderManager.removeApplePayPamentGroup(pOrder);
			pOrder.setApplepay(Boolean.FALSE);
		}
		
	}

	/**
	 * <p>
	 * This method will update the tenderType promo eligibility if any CC is updated/removed from the profile.
	 * </p>
	 *
	 * @param pShoppingCart the shopping cart
	 * @param pOrder the order
	 */
	private void checkTenderTypePromotionEligibility(TRUOrderHolder pShoppingCart,
			TRUOrderImpl pOrder) {		
		TRUCreditCard orderCC = getCreditCardFromOrder(pOrder);
		if(!pShoppingCart.getProfile().isTransient() && orderCC != null /*&& StringUtils.isNotBlank(orderCC.getCreditCardType())*/){			
				String orderCCType = orderCC.getCreditCardType();
												
				String orderCCNo = orderCC.getCreditCardNumber();
				RepositoryItem profile = pShoppingCart.getProfile();										
				Map allCredictCards = (Map) profile.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getAllCreditCardsPropName()) ;				
				String defaultCCNickName = (String)profile.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getSelectedCCNickNamePropName());
				//((TRUCommercePropertyManager)getPurchaseProcessHelper().getCommercePropertyManager()).get
			
				//RepositoryItem defaultCreditCard = (RepositoryItem)profile.getPropertyValue("defaultCreditCard") ;
				boolean isTenderTypeFound = Boolean.FALSE;
				String profileCCNumber = TRUConstants.EMPTY;
				String profileCCType = TRUConstants.EMPTY;
				String nameOnCard = TRUConstants.EMPTY;
				RepositoryItem profileCCard = null;
				if(StringUtils.isNotBlank(defaultCCNickName) && allCredictCards!=null && !allCredictCards.isEmpty()){					
					profileCCard = (RepositoryItem) allCredictCards.get(defaultCCNickName);
					if(profileCCard !=null) {
						profileCCNumber = (String) profileCCard.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getCreditCardNumberPropName());	
						profileCCType = (String) profileCCard.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getCreditCardTypePropName());
						nameOnCard = (String) profileCCard.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getNameOnCardPropName());
						String expirationYear = (String) profileCCard.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getExpirationYearPropName());
						String expirationMonth = (String) profileCCard.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getExpirationMonthPropName());
						String expirationDayOfMonth = (String) profileCCard.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getExpirationDayOfMonthPropName());
						RepositoryItem billingAddress = (RepositoryItem) profileCCard.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getBillingAddressPropName());
						//If the order's CC matched with the selectedCC on profile
						if(profileCCType.equals(orderCCType) && orderCCNo.equals(profileCCNumber)){												
								isTenderTypeFound = Boolean.TRUE;												
						}else{
							//If the order's CC not matched with the selectedCC on profile,Copy the profileDefaultCC data to the order CC.
							orderCC.setCreditCardNumber(profileCCNumber);
							orderCC.setCreditCardType(profileCCType);
							orderCC.setNameOnCard(nameOnCard);
							orderCC.setExpirationYear(expirationYear);
							orderCC.setExpirationMonth(expirationMonth);
							orderCC.setExpirationDayOfMonth(expirationDayOfMonth);
							orderCC.setChanged(Boolean.TRUE);
							if(billingAddress!=null){
								try {
									OrderTools.copyAddress(billingAddress, orderCC.getBillingAddress());
								} catch (CommerceException e) {
									if(isLoggingError()){
										vlogError("In validateTenderTypePromotions while copyAddress {0}", e);
									}
								}
							}
							pOrder.setCreditCardType(profileCCType);
							pOrder.setRepriceRequiredOnCart(true);
							return;
						}							
					//	((TRUOrderImpl)pShoppingCart.getCurrent()).setCreditCardType(profileCCType);
					}
					}/*else{
					//TODO :Get the default address from profile and add to order
				}*/					
				if(!isTenderTypeFound){
					pOrder.setCreditCardType(null);
					pOrder.setRepriceRequiredOnCart(true);
				}
		}
	}


	/**
	 * Gets the shopping cart utils.
	 * 
	 * @return the shopping cart utils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * Sets the shopping cart utils.
	 * 
	 * @param pShoppingCartUtils
	 *            the new shopping cart utils
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		this.mShoppingCartUtils = pShoppingCartUtils;
	}

	/**
	 * Checks if is enabled.
	 *
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets the enabled.
	 *
	 * @param pEnabled the enabled to set
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	/**
	 * Gets the purchase process helper.
	 * 
	 * @return the purchase process helper
	 */
	public TRUPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}

	/**
	 * Sets the purchase process helper.
	 * 
	 * @param pPurchaseProcessHelper
	 *            the new purchase process helper
	 */
	public void setPurchaseProcessHelper(TRUPurchaseProcessHelper pPurchaseProcessHelper) {
		mPurchaseProcessHelper = pPurchaseProcessHelper;
	}

	/**
	 * Gets the credit card from order.
	 *
	 * @param pOrder the order
	 * @return the credit card from order
	 */
	private TRUCreditCard getCreditCardFromOrder(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCheckoutItemAdjustmentServlet::@method::getCreditCardFromOrder() : BEGIN");
		}
		TRUOrderImpl order =(TRUOrderImpl)pOrder;
		List<PaymentGroup> paymentGroups = order.getPaymentGroups();
        for(PaymentGroup pg : paymentGroups){
              if(pg instanceof TRUPayPal){
                    return null;
              }else if(pg instanceof TRUGiftCard){
                return null;
              }else if(pg instanceof TRUCreditCard ){
              	TRUCreditCard creditCard = (TRUCreditCard) pg;
              	return creditCard;
              }
        }
        if (isLoggingDebug()) {
			logDebug("@Class::TRUCheckoutItemAdjustmentServlet::@method::getCreditCardFromOrder() : END");
		}
        return null;
	}
}
