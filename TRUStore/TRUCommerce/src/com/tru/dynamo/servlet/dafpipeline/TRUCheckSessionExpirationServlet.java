 
package com.tru.dynamo.servlet.dafpipeline;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.multisite.SiteManager;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.InsertableServletImpl;

import com.tru.common.TRUConstants;
import com.tru.preview.TRUSchedulableDate;

/**
 * TRUCheckSessionExpirationServlet class to check if session has been expired
 * and redirect user to desired session expiration Url if session is expired.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUCheckSessionExpirationServlet extends InsertableServletImpl{

	/** property to hold my account expiration URL. */
	private String mMyAccountExpirationURL;
	
	/** property to hold secured URL list. */
	private List<String> mSecuredUrlList;
	
	/** property to hold myaccount header popover URL. */
    private String mMyAccountPopoverURL;
    
    /** property to hold enabled. */
    private boolean mEnabled;

    /**
 	 * @param sessionCheckForCheckout the enabled to set
 	 */
 	private boolean mSessionCheckForCheckout;

 	/**
 	 * @param mCheckoutExpirationURL the enabled to set
 	 */
 	private String mCheckoutExpirationURL;
 	
 	/**
 	 * @param mLayawayExpirationURL the enabled to set
 	 */
 	private String mLayawayExpirationURL;
 	
 	/**
 	 * @param sessionCheckForLayaway the enabled to set
 	 */
 	private boolean mSessionCheckForLayaway;
 	
 	/**
 	 * @param mCheckOurRequestURIs 
 	 */
 	private List<String> mCheckOurRequestURIs;
 	
 	/**
 	 * @param mLayawayRequestURIs 
 	 */
 	private List<String> mLayawayRequestURIs;
 	
 	/**
 	 * @param mFormIdsToSkip 
 	 */
 	private List<String> mFormIdsToSkip;
 	
	/**
	 * Constant to hold mGlobalPromotionDate
	 */
	private TRUSchedulableDate mGlobalPromotionDate;


	/**
	 * This method checks if session is expired.
	 * If session is expired and request type is AJAX, it sets 409 error code to response to handle redirection in AJAX.
	 * If session is expired and request type is non AJAX, it just redirects user to desired session expiration Url.
	 * @param pRequest - DynamoHttpServletRequest
	 * @param pResponse - DynamoHttpServletResponse
	 * @throws IOException IOException
	 * @throws ServletException ServletException
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException, ServletException {
		vlogDebug("Start: TRUCheckSessionExpiration.Service()");
		String requestUri = pRequest.getRequestURI();
		String formId = pRequest.getQueryParameter(TRUConstants.FORM_ID);
		String visitorFirstNamecookie = pRequest.getParameter(TRUConstants.VISITOR_FIRST_NAME_COOKIE);
		HttpSession session = pRequest.getSession(false);
		if (isEnabled() && (!pRequest.isRequestedSessionIdValid() || session.isNew()) && (requestUri != null && SiteContextManager.getCurrentSite() != null && 
				(formId == null || (getFormIdsToSkip() != null && !getFormIdsToSkip().contains(formId)))))  {
			getGlobalPromotionDate().getCookieManager().setNewSession(Boolean.TRUE);
			getGlobalPromotionDate().setTimeAsDate(new Date());
			Site site = SiteContextManager.getCurrentSite();
			vlogDebug("TRUCheckSessionExpirationServlet service() site {0}", site);
			vlogDebug("TRUCheckSessionExpirationServlet service() ProductionURLPropertyName {0}", getSiteManager().getProductionURLPropertyName());
			String lContextPath = (String) site.getPropertyValue(getSiteManager().getProductionURLPropertyName());
			if (getSecuredUrlList().contains(requestUri.trim())) {
				if (isSessionCheckForCheckout() && (getCheckOurRequestURIs().contains(requestUri))) {
					if (isAjax(pRequest)) {
						vlogDebug("Inside ajax request true block for checkout.");
						pResponse.setStatus(TRUConstants.SERVER_ERROR_CODE);
					} else {
						pResponse.sendLocalRedirect(lContextPath + getCheckoutExpirationURL() + TRUConstants.SESSION_EXPIRED_PARAMETER, pRequest);
					}
					return;
				} else if (isSessionCheckForLayaway() && (getLayawayRequestURIs().contains(requestUri))) {
					if (isAjax(pRequest)) {
						vlogDebug("Inside ajax request true block for layaway.");
						pResponse.setStatus(TRUConstants.SERVER_ERROR_CODE);
					} else {
						pResponse.sendLocalRedirect(lContextPath + getLayawayExpirationURL() + TRUConstants.SESSION_EXPIRED_PARAMETER, pRequest);
					}
					return;
				} else {
					if (isAjax(pRequest)) {
						vlogDebug("Inside ajax request true block for my account.");
						pResponse.setStatus(TRUConstants.SERVER_ERROR_CODE);
					} else {
						pResponse.sendLocalRedirect(lContextPath + getMyAccountExpirationURL() + TRUConstants.SESSION_EXPIRED_PARAMETER, pRequest);
					}
					return;
				}
			} else if (isAjax(pRequest) && requestUri.contains(getMyAccountPopoverURL()) && visitorFirstNamecookie != null && 
					(!visitorFirstNamecookie.equalsIgnoreCase(TRUConstants.MY_ACCOUNT) || pRequest.getHeader(TRUConstants.REFERER).endsWith(TRUConstants.ORDER_CONFIRMATION_PAGE))) {
				vlogDebug("Inside ajax request true block with my account pop over url.");
				pResponse.setStatus(TRUConstants.SERVER_ERROR_CODE);
				pResponse.setHeader(TRUConstants.REFERER, pRequest.getHeader(TRUConstants.REFERER));
				return;
			}
		} else {
			getGlobalPromotionDate().getCookieManager().setNewSession(Boolean.FALSE);
		}
		
		passRequest(pRequest, pResponse);
		vlogDebug("End: TRUCheckSessionExpiration.Service()");
	}
			
	
	/**
 	 * Checks if request type is ajax.
 	 *
 	 * @param pRequest the request
 	 * @return true, if is ajax
 	 */
 	private boolean isAjax(DynamoHttpServletRequest pRequest) {
 		return TRUConstants.XMLHTTPREQUEST.equals(pRequest.getHeader(TRUConstants.XREQUESTEDWITH));
	}
	
 	/**
	 * Returns my account Expiration URl.
	 * 
	 * @return mMyAccountExpirationURL
	 */
	public String getMyAccountExpirationURL() {

		return mMyAccountExpirationURL;
	}
	
	/**
	 * Sets my account Expiration URl.
	 * 
	 * @param pMyAccountExpirationURL the mMyAccountExpirationURL to set
	 */
	public void setMyAccountExpirationURL(String pMyAccountExpirationURL) {
		this.mMyAccountExpirationURL = pMyAccountExpirationURL;
	}

	/**
	 * @return the mSecuredUrlList
	 */
	public List<String> getSecuredUrlList() {
		return mSecuredUrlList;
	}

	/**
	 * @param pSecuredUrlList the mSecuredUrlList to set
	 */
	public void setSecuredUrlList(List<String> pSecuredUrlList) {
		this.mSecuredUrlList = pSecuredUrlList;
	}
	
	/**
     * @return the mMyAccountPopoverURL
     */
     public String getMyAccountPopoverURL() {
                     return mMyAccountPopoverURL;
     }

     /**
     * @param pMyAccountPopoverURL the mMyAccountPopoverURL to set
     */
     public void setMyAccountPopoverURL(String pMyAccountPopoverURL) {
                     this.mMyAccountPopoverURL = pMyAccountPopoverURL;
     }

 	/**
 	 * @return the enabled
 	 */
 	public boolean isEnabled() {
 		return mEnabled;
 	}

 	/**
 	 * @param pEnabled the enabled to set
 	 */
 	public void setEnabled(boolean pEnabled) {
 		mEnabled = pEnabled;
 	}
 	
 	
	/**
	 * @return CheckoutExpirationURL
	 */
	public String getCheckoutExpirationURL() {
		return mCheckoutExpirationURL;
	}


	/**
	 * @param pCheckoutExpirationURL - CheckoutExpirationURL
	 */
	public void setCheckoutExpirationURL(String pCheckoutExpirationURL) {
		this.mCheckoutExpirationURL = pCheckoutExpirationURL;
	}


	/**
	 * @return sessionCheckForCheckout
	 */
	public boolean isSessionCheckForCheckout() {
		return mSessionCheckForCheckout;
	}


	/**
	 * @param pSessionCheckForCheckout - SessionCheckForCheckout
	 */
	public void setSessionCheckForCheckout(boolean pSessionCheckForCheckout) {
		this.mSessionCheckForCheckout = pSessionCheckForCheckout;
	}
	
	/**
	 * @return sessionCheckForLayaway
	 */
	public boolean isSessionCheckForLayaway() {
		return mSessionCheckForLayaway;
	}

	/**
	 * @param pSessionCheckForLayaway - sessionCheckForLayaway
	 */
	public void setSessionCheckForLayaway(boolean pSessionCheckForLayaway) {
		this.mSessionCheckForLayaway = pSessionCheckForLayaway;
	}


	/**
	 * @return CheckOurRequestURIs
	 */
	public List<String> getCheckOurRequestURIs() {
		return mCheckOurRequestURIs;
	}


	/**
	 * @param pCheckOurRequestURIs - CheckOurRequestURIs
	 */
	public void setCheckOurRequestURIs(List<String> pCheckOurRequestURIs) {
		this.mCheckOurRequestURIs = pCheckOurRequestURIs;
	}
	
	/**
	 * @return layawayRequestURIs
	 */
	public List<String> getLayawayRequestURIs() {
		return mLayawayRequestURIs;
	}

	/**
	 * @param pLayawayRequestURIs - mLayawayRequestURIs
	 */
	public void setLayawayRequestURIs(List<String> pLayawayRequestURIs) {
		this.mLayawayRequestURIs = pLayawayRequestURIs;
	}
 	
	/**
	 * @return layawayExpirationURL
	 */
	public String getLayawayExpirationURL() {
		return mLayawayExpirationURL;
	}

	/**
	 * @param pLayawayExpirationURL - mLayawayExpirationURL
	 */
	public void setLayawayExpirationURL(String pLayawayExpirationURL) {
		this.mLayawayExpirationURL = pLayawayExpirationURL;
	}
	
	/** The m site manager. */
	private SiteManager mSiteManager;


	/**
	 * @return the siteManager
	 */
	public SiteManager getSiteManager() {
		return mSiteManager;
	}

	/**
	 * @param pSiteManager the siteManager to set
	 */
	public void setSiteManager(SiteManager pSiteManager) {
		mSiteManager = pSiteManager;
	}
	
	/**
	 * @return the formIdsToSkip
	 */
	public List<String> getFormIdsToSkip() {
		return mFormIdsToSkip;
	}

	/**
	 * @param pFormIdsToSkip the formIdsToSkip to set
	 */
	public void setFormIdsToSkip(List<String> pFormIdsToSkip) {
		this.mFormIdsToSkip = pFormIdsToSkip;
	}

	/**
	 * @return the globalPromotionDate
	 */
	public TRUSchedulableDate getGlobalPromotionDate() {
		return mGlobalPromotionDate;
	}


	/**
	 * @param pGlobalPromotionDate the globalPromotionDate to set
	 */
	public void setGlobalPromotionDate(TRUSchedulableDate pGlobalPromotionDate) {
		mGlobalPromotionDate = pGlobalPromotionDate;
	}
}