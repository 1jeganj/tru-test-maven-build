package com.tru.dynamo.servlet.dafpipeline;

import atg.commerce.order.OrderHolder;
import atg.nucleus.GenericService;
import atg.nucleus.naming.ComponentName;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.AccessController;
import atg.userprofiling.Profile;

/** 
 * TRUAccessController class for restricting the user to some pages based on certain conditions.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCheckoutAccessController extends GenericService implements AccessController {

	private ComponentName mShoppingCartName;
	
	/**
	 * holds shopping cart path.
	 */
	private String mShoppingCartPath;
	
	/** Property to hold the denied access url. */
	private String mDeniedUrl;
	
	/**
	 * @return the shoppingCartPath
	 */
	public String getShoppingCartPath() {
		return mShoppingCartPath;
	}

	/**
	 * @param pShoppingCartPath the shoppingCartPath to set
	 */
	public void setShoppingCartPath(String pShoppingCartPath) {
		mShoppingCartPath = pShoppingCartPath;
		if (this.mShoppingCartPath != null) {
			this.mShoppingCartName = ComponentName.getComponentName(this.mShoppingCartPath);
		} else {
			this.mShoppingCartName = null;
		}
	}


	
	/**
     * Checks if the user is allowed to access the requested URL.
     *
     * @param pProfile - current user
     * @param pRequest - DynamoHttpServletRequest
     * @return true if is explicitly logged in
     *         false otherwise
     */
	public boolean allowAccess(Profile pProfile, DynamoHttpServletRequest pRequest) {
		boolean checkaccess = false;
		checkaccess = !isRestricted(pRequest);
		return checkaccess;
	}

	/**
     * Checks if the request session is new.
     *
     * @param pRequest request
     * @return true if request session is new.
     *         false otherwise
     */
	private boolean isRestricted(DynamoHttpServletRequest pRequest) {
		boolean restrictAccess = false;
		
		if (pRequest.getSession().isNew()) {
			restrictAccess = true;
		} else {
			Object obj = pRequest.resolveName(this.mShoppingCartName);
			if (obj instanceof OrderHolder) {
				OrderHolder orderHolder = (OrderHolder) obj;
				if (orderHolder.isCurrentEmpty()) {
					restrictAccess = true;
				}
			}
		}
		
		return restrictAccess;
	}
	
	/**
	 * Gets the denied access Url for the specific profile.
	 * @param pProfile - current user
	 * @return deniedAccessUrl String
	 */
	public String getDeniedAccessURL(Profile pProfile) {
		String deniedAccessUrl = getDeniedUrl();
		return deniedAccessUrl;
	}
	
	/** getter method for denied url.
     *  @return the mDeniedUrl
     */
	public String getDeniedUrl() {
		return mDeniedUrl;
	}
	
	/**
     * @param pDeniedUrl the mDeniedUrl to set.
     */
	public void setDeniedUrl(String pDeniedUrl) {
		this.mDeniedUrl = pDeniedUrl;
	}
	
}
