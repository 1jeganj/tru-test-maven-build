package com.tru.dynamo.servlet.dafpipeline;

import atg.beans.PropertyNotFoundException;
import atg.nucleus.GenericService;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.AccessController;
import atg.userprofiling.Profile;

import com.tru.commerce.TRUCommerceConstants;

/** 
 * TRUAccessController class for restricting the user to some pages based on certain conditions.
 * @author Professional Access
 * @version 1.0
 */
public class TRUAccessController extends GenericService implements AccessController {

	/** Property to hold the denied access url. */
	private String mDeniedUrl;
	
	/**
     * Checks if the user is allowed to access the requested URL.
     *
     * @param pProfile - current user
     * @param pRequest - DynamoHttpServletRequest
     * @return true if is explicitly logged in
     *         false otherwise
     */
	public boolean allowAccess(Profile pProfile, DynamoHttpServletRequest pRequest) {
		boolean checkaccess = false;
		checkaccess = !isRestricted(pProfile);
		return checkaccess;
	}

	/**
     * Checks if the user's securityStatus is less than securityStatusLogin.
     *
     * @param pProfile - current user
     * @return true if is user's securityStatus is less than securityStatusLogin
     *         false otherwise
     */
	private boolean isRestricted(Profile pProfile){
		boolean restrictAccess = false;
		try{
		 if(pProfile != null){
			Integer securityStatus = pProfile.getProfileTools().getSecurityStatus(pProfile);
			if (securityStatus != null && securityStatus.intValue() < pProfile.getProfileTools().getPropertyManager().getSecurityStatusLogin()) {
                restrictAccess = true;
			}
		  }
		}
		catch(PropertyNotFoundException propEx){
			restrictAccess = false;
			vlogError("PropertyNotFoundException occured: ", propEx);
		}
		return restrictAccess;
	}
	
	/**
	 * Gets the denied access Url for the specific profile.
	 * @param pProfile - current user
	 * @return deniedAccessUrl String
	 */
	public String getDeniedAccessURL(Profile pProfile) {
		String deniedAccessUrl = TRUCommerceConstants.PATH_SEPERATOR + getDeniedUrl();
		return deniedAccessUrl;
	}
	
	/** getter method for denied url.
     *  @return the mDeniedUrl
     */
	public String getDeniedUrl() {
		return mDeniedUrl;
	}
	
	/**
     * @param pDeniedUrl the mDeniedUrl to set.
     */
	public void setDeniedUrl(String pDeniedUrl) {
		this.mDeniedUrl = pDeniedUrl;
	}
	
}
