package com.tru.dynamo.servlet.dafpipeline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.CommerceException;
import atg.commerce.inventory.InventoryException;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.util.NoLockNameException;
import atg.commerce.util.TransactionLockService;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.naming.ComponentName;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.lockmanager.DeadlockException;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.InsertableServletImpl;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUOrderHolder;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOutOfStockItem;
import com.tru.commerce.order.purchase.TRUAddCommerceItemInfo;
import com.tru.commerce.order.purchase.TRUPurchaseProcessHelper;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConstants;
import com.tru.common.TRULockManager;
import com.tru.common.TRUUserSession;
import com.tru.userprofiling.TRUCookieManager;

/**
 * This Class is used to adjust the cart items based on below scenarios.
 * 1. Handle when Items are soft deleted from BCC
 * 2. Handle Out of stock items in cart.
 * 3. Handle Inventory adjustments.
 * 4. Validate and add out of stock items back if they are available now.	  
 * 
 * @author PA
 * @version 1.0
 */
public class TRUCartItemAdjustmentServlet extends InsertableServletImpl {

	/** The m purchase process helper. */
	private TRUPurchaseProcessHelper mPurchaseProcessHelper;
	/** property to Hold shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;
	/** Holds property mEnable . */
	private boolean mEnabled;

	/** The Constant CART_AJAX_REQUEST. */
	private static final String CART_AJAX_REQUEST = "/cart/intermediate/ajaxIntermediateRequest.jsp";
	
	/** property to Hold SHOPPING_CART_COMPONENT_NAME. */
	public static final ComponentName SHOPPING_CART_COMPONENT_NAME = ComponentName
			.getComponentName(TRUCommerceConstants.SHOPPING_CART_COMPONENT_NAME);

	/** The Constant TRU_SESSION_SCOPE_COMPONENT_NAME. */
	public static final  ComponentName TRU_SESSION_SCOPE_COMPONENT_NAME = ComponentName
			.getComponentName(TRUCommerceConstants.TRU_SESSION_SCOPE_COMPONENT_NAME);

	/** This holds the reference for tru configuration. */
	private TRULockManager mLockManager;
	
	/**
	 * @return the lockManager
	 */
	public TRULockManager getLockManager() {
		return mLockManager;
	}

	/**
	 * @param pLockManager the lockManager to set
	 */
	public void setLockManager(TRULockManager pLockManager) {
		mLockManager = pLockManager;
	}


	/**
	 * This servlet is used to remove the items from the current order for which sku or product is soft deleted from the catalog.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ServletException
	 *             the servlet exception
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException,
			ServletException {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUCartItemAdjustmentServlet service()");
		}
		if(isEnabled()){
			String isCartPage = pRequest.getParameter(getShoppingCartUtils().getConfiguration().getShoppingCartParameter());
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUCartItemAdjustmentServlet::@method::service() : Request URI {0}", pRequest.getRequestURI());
				vlogDebug("@Class::TRUCartItemAdjustmentServlet::@method::service() : isCartPage {0}", isCartPage);
			}
			TransactionLockService lockService = null;
			if (null != isCartPage && Boolean.TRUE.equals(Boolean.valueOf(isCartPage))) {
				TRUOrderHolder shoppingCart = (TRUOrderHolder) pRequest.resolveName(SHOPPING_CART_COMPONENT_NAME);
				if (null != shoppingCart && null != shoppingCart.getCurrent()) {
					TRUOrderImpl order = (TRUOrderImpl) shoppingCart.getCurrent();
					boolean rollback = false;
					TransactionDemarcation td = new TransactionDemarcation();
					try {
						lockService = getLockManager().acquireLock();
						td.begin(getPurchaseProcessHelper().getTransactionManager(), TransactionDemarcation.REQUIRED);
						synchronized (order) {
							if(order.getHardgoodShippingGroupsCount()>0){
								List<TRUHardgoodShippingGroup> shipGrps=order.getOnlyHardgoodShippingGroups();
								for(TRUHardgoodShippingGroup shpGrp:shipGrps){
									shpGrp.setTransientState(TRUConstants.EMPTY_STRING);
								}
							}
							if (order.getCommerceItemCount() > 0 ||
									(null != ((TRUOrderImpl) order).getOutOfStockItems() && !((TRUOrderImpl) order).getOutOfStockItems().isEmpty())) {
								autoCorrectOrder(pRequest, shoppingCart, order);
								modifyCookie(pRequest, pResponse, shoppingCart, order);
							}
						}
					} catch (CommerceItemNotFoundException e) {
						if (isLoggingError()) {
							vlogError("CommerceItemNotFoundException in TRUCartItemAdjustmentServlet.service()::", e);
						}
						rollback = true;
					} catch (InvalidParameterException e) {
						if (isLoggingError()) {
							vlogError("InvalidParameterException in TRUCartItemAdjustmentServlet.service()::", e);
						}
						rollback = true;
					} catch (CommerceException e) {
						rollback = true;
						if (isLoggingError()) {
							vlogError("CommerceException in TRUCartItemAdjustmentServlet.service()::", e);
						}
					} catch (NoLockNameException ne) {
						if (isLoggingError()) {
							vlogError("NoLockNameException in @method: TRUCartItemAdjustmentServlet.service(): {0}", ne);
						}
					} catch (DeadlockException de) {
						if (isLoggingError()) {
							vlogError("DeadlockException in @method: TRUCartItemAdjustmentServlet.service(): {0}", de);
						}
					} catch (RepositoryException repoException) {
						if (isLoggingError()) {
							vlogError("RepositoryException in @method: TRUCartItemAdjustmentServlet.service(): {0}", repoException);
						}
					} catch (RunProcessException re) {
						if (isLoggingError()) {
							vlogError("RunProcessException in @method: TRUCartItemAdjustmentServlet.service(): {0}", re);
						}
					} catch (TransactionDemarcationException e) {
						rollback = true;
						if (isLoggingError()) {
							vlogError(" TransactionDemarcationException in TRUCartItemAdjustmentServlet.service()::", e);
						}
					} finally {
						try {
							if(td != null){
								td.end(rollback);
							}
						} catch (TransactionDemarcationException e) {
							if (isLoggingError()) {
								vlogError(
										" TransactionDemarcationException in @Class::TRUCartItemAdjustmentServlet::@method::service() :",
										e);
							}
						}
						if (lockService != null) {
							getLockManager().releaseLock(lockService);
						}
					}
				}
			} else if (pRequest.getRequestURI() != null && pRequest.getRequestURI().contains(CART_AJAX_REQUEST)) {
				TRUUserSession sessionComponent = (TRUUserSession) pRequest.resolveName(TRU_SESSION_SCOPE_COMPONENT_NAME);
				sessionComponent.getMultipleItemsInfoMessages().clear();
				sessionComponent.getItemInlineMessages().clear();
			}
			
		}
		passRequest(pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("Exiting into TRUCartItemAdjustmentServlet service()");
		}
	}
	

	/**
	 * Modify cookie.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @param pShoppingCart
	 *            the shopping cart
	 * @param pOrder
	 *            the order
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws CommerceException
	 *             the commerce exception
	 */
	private void modifyCookie(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse,
			TRUOrderHolder pShoppingCart, Order pOrder) throws ServletException, IOException, CommerceException {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUCartItemAdjustmentServlet modifyCookie() :: Start");
			vlogDebug("Entering into TRUCartItemAdjustmentServlet modifyCookie() :: pShoppingCart {0} -->, pOrder{1} -->",
					pShoppingCart, pOrder);
		}
		TRUProfileTools profileTools = getPurchaseProcessHelper().getProfileTools();
		if (profileTools.isAnonymousUser(pShoppingCart.getProfile())) {
			TRUCookieManager cookieManager = (TRUCookieManager) getPurchaseProcessHelper().getProfileTools().getCookieManager();
			cookieManager.createCartCookie(pOrder, pRequest, pResponse, TRUConstants.CART_COOKIE_NAME);

		} else {
			TRUCookieManager cookieManager = (TRUCookieManager) getPurchaseProcessHelper().getProfileTools().getCookieManager();
			cookieManager.createCartCookie(pOrder, pRequest, pResponse, TRUConstants.LOGGEDEIN_CART_COOKIE_NAME);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting into TRUCartItemAdjustmentServlet modifyCookie()");
		}
	}

	/**
	 * Auto correct order.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pShoppingCart
	 *            the shopping cart
	 * @param pOrder
	 *            the order
	 * @throws TransactionDemarcationException
	 *             the transaction demarcation exception
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws InventoryException
	 *             the inventory exception
	 * @throws CommerceException
	 *             the commerce exception
	 * @throws RunProcessException
	 *             the run process exception
	 */
	private void autoCorrectOrder(DynamoHttpServletRequest pRequest, TRUOrderHolder pShoppingCart, Order pOrder)
			throws TransactionDemarcationException, RepositoryException, InventoryException, CommerceException,
			RunProcessException {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUCartItemAdjustmentServlet autoCorrectOrder() :: Start");
			vlogDebug("Entering into TRUCartItemAdjustmentServlet autoCorrectOrder() :: pShoppingCart {0} -->, pOrder{1} -->",
					pShoppingCart, pOrder);
		}
		Set<String> removalItemList = null;
		Set<String> deletedSkuList = null;
		Set<String> outOfStockList = null;
		Set<String> excludeInvenotryCheckList = new HashSet<String>();
		TRUAddCommerceItemInfo[] inStockInfo = null;
		List<String> autoCorrectedList = null;
		getPurchaseProcessHelper().runProcessRepriceOrder(getPurchaseProcessHelper().getDeleteItemsFromOrderPricingOp(),
				pOrder, null, null, pShoppingCart.getProfile(), null, null);
		getPurchaseProcessHelper().getOrderManager().updateOrder(pOrder);
		if (null != ((TRUOrderImpl) pOrder).getOutOfStockItems() && !((TRUOrderImpl) pOrder).getOutOfStockItems().isEmpty()) {
			inStockInfo = getShoppingCartUtils().getInStockListFromOutOfStockItems(((TRUOrderImpl) pOrder).getOutOfStockItems(),
					getShoppingCartUtils().getLocationIdFromCookie(pRequest));
			if (null != inStockInfo && inStockInfo.length > TRUCommerceConstants.NUM_ZERO) {
				addItemsToOrderFromOutOfStockList(pOrder, inStockInfo, pShoppingCart.getProfile());
			}
		}
		removalItemList = getShoppingCartUtils().getTRUCartModifierHelper().getRemovalItemList(pOrder);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCartItemAdjustmentServlet::@method::autoCorrectOrder : removalItemList {0}", removalItemList);
		}
		if (null != removalItemList && !removalItemList.isEmpty()) {
			deletedSkuList = getPurchaseProcessHelper().removeItemsFromOrder(pOrder, removalItemList, pOrder.getProfileId());
			excludeInvenotryCheckList.addAll(deletedSkuList);
			if (null != deletedSkuList && !deletedSkuList.isEmpty()) {
				TRUUserSession sessionComponent = (TRUUserSession) pRequest.resolveName(TRU_SESSION_SCOPE_COMPONENT_NAME);
				sessionComponent.setDeletedSkuList(deletedSkuList);
			}
		}
		if (null != inStockInfo) {
			for (int i = 0; i < inStockInfo.length; i++) {
				excludeInvenotryCheckList.add(inStockInfo[i].getCatalogRefId());
			}
		}
		outOfStockList = getShoppingCartUtils().getOutOfStockItemList(excludeInvenotryCheckList, pOrder,
				getShoppingCartUtils().getLocationIdFromCookie(pRequest));
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCartItemAdjustmentServlet::@method::autoCorrectOrder : outOfStockList:  {0}", outOfStockList);
		}
		if (null != outOfStockList && !outOfStockList.isEmpty()) {
			getPurchaseProcessHelper().removeItemsFromOrder(pOrder, outOfStockList, pOrder.getProfileId());
		}
		autoCorrectedList = ((TRUOrderManager) getPurchaseProcessHelper().getOrderManager()).autoCorrectQuantity(pOrder, true);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCartItemAdjustmentServlet::@method::autoCorrectOrder :autoCorrectedList  {0}",
					autoCorrectedList);
		}
		if ((null != autoCorrectedList && !autoCorrectedList.isEmpty())	|| 
				(null != inStockInfo && inStockInfo.length > TRUCommerceConstants.NUM_ZERO)	||
				(null != outOfStockList && !outOfStockList.isEmpty()) ||
				(null != removalItemList && !removalItemList.isEmpty())) {
			getPurchaseProcessHelper().runProcessRepriceOrder(getPurchaseProcessHelper().getDeleteItemsFromOrderPricingOp(),
					pOrder, null, null, pShoppingCart.getProfile(), null, null);
			getPurchaseProcessHelper().getOrderManager().updateOrder(pOrder);
		}else{			
			if(((TRUOrderImpl)pOrder).isRepriceRequiredOnCart()){
				getPurchaseProcessHelper().runProcessRepriceOrder(getPurchaseProcessHelper().getDeleteItemsFromOrderPricingOp(),
						pOrder, null, null, pShoppingCart.getProfile(), null, null);
				((TRUOrderImpl)pOrder).setRepriceRequiredOnCart(Boolean.FALSE);
				getPurchaseProcessHelper().getOrderManager().updateOrder(pOrder);				
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting into TRUCartItemAdjustmentServlet modifyCookie()");
		}
	}

	/**
	 * Gets the shopping cart utils.
	 * 
	 * @return the shopping cart utils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * Sets the shopping cart utils.
	 * 
	 * @param pShoppingCartUtils
	 *            the new shopping cart utils
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		this.mShoppingCartUtils = pShoppingCartUtils;
	}

	/**
	 * Checks if is enabled.
	 *
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets the enabled.
	 *
	 * @param pEnabled the enabled to set
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	/**
	 * Gets the purchase process helper.
	 * 
	 * @return the purchase process helper
	 */
	public TRUPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}

	/**
	 * Sets the purchase process helper.
	 * 
	 * @param pPurchaseProcessHelper
	 *            the new purchase process helper
	 */
	public void setPurchaseProcessHelper(TRUPurchaseProcessHelper pPurchaseProcessHelper) {
		mPurchaseProcessHelper = pPurchaseProcessHelper;
	}

	/**
	 * Adds the items to order from out of stock list.
	 *
	 * @param pOrder the order
	 * @param pItemInfos the item info
	 * @param pProfile the profile
	 * @throws CommerceException the commerce exception
	 * @throws RepositoryException the repository exception
	 */
	private void addItemsToOrderFromOutOfStockList(Order pOrder, TRUAddCommerceItemInfo[] pItemInfos, RepositoryItem pProfile)
			throws CommerceException, RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUPurchaseProcessHelper::@method::addItemsToOrderFromOutOfStockList() : BEGIN pOrder :pItemInfos :pProfile {0} {1} {2}",
					pOrder, pItemInfos, pProfile);
		}
		List<TRUOutOfStockItem> outOfStockList = ((TRUOrderImpl) pOrder).getOutOfStockItems();
		List<TRUOutOfStockItem> updatedOutOfStockList = new ArrayList<TRUOutOfStockItem>(
				((TRUOrderImpl) pOrder).getOutOfStockItems());
		String locid = TRUConstants.EMPTY;
		String skuid = TRUConstants.EMPTY;
		TRUCommercePropertyManager propertyManager =  getPurchaseProcessHelper().getCommercePropertyManager() ;
		for (int i = TRUConstants.ZERO; i < pItemInfos.length; i++) {
			skuid = pItemInfos[i].getCatalogRefId();
			for (TRUOutOfStockItem outOfStockItem : outOfStockList) {
				locid = pItemInfos[i].getLocationId();
				if(StringUtils.isNotBlank(locid)){
				if (skuid.equals(outOfStockItem.getPropertyValue(propertyManager.getCatalogRefIdPropertyName())) 
						&& locid.equals(outOfStockItem.getPropertyValue(propertyManager.getLocationIdPropertyName())) ) {
					updatedOutOfStockList.remove(outOfStockItem);
					break;
				}} else {
					if (skuid.equals(outOfStockItem.getPropertyValue(propertyManager.getCatalogRefIdPropertyName()))) {
						updatedOutOfStockList.remove(outOfStockItem);
						break;
					}
				}
			}
		}
		locid = null;
		skuid = null;
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUPurchaseProcessHelper::@method::addItemsToOrderFromOutOfStockList() : old list : {0} : updated list : {1}",
					outOfStockList, updatedOutOfStockList);
		}
		((TRUOrderImpl) pOrder).setOutOfStockItems(updatedOutOfStockList);
		getPurchaseProcessHelper().createISPUShippingGroups(pOrder, pItemInfos);
		getPurchaseProcessHelper().addItemsToOrder(pOrder, (ShippingGroup) pOrder.getShippingGroups().get(0), pProfile,
				pItemInfos, null, null, null, null);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::addItemsToOrderFromOutOfStockList() : END ");
		}
	}
	
}
