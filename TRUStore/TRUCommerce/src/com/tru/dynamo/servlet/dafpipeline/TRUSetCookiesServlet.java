package com.tru.dynamo.servlet.dafpipeline;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import atg.core.util.StringUtils;
import atg.multisite.SiteContextManager;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.InsertableServletImpl;
import atg.servlet.wrappers.LazyServletOutputStream;
import atg.userprofiling.Profile;
import atg.userprofiling.PropertyManager;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.order.TRUOrderHolder;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * The class TRUSetCookiesServlet.
 * This class is used to generates the cookies.
 *
 * @author PA
 * @version 1.0
 */
public class TRUSetCookiesServlet extends InsertableServletImpl {

	/** The Constant Domain name */
	public static final String DOMAIN_NAME = ".toysrus.com";

	/** property to Hold shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;

	/** Property to hold  cookie path. */
	private String mCookiePath;

	/** Property to hold  cookie max age. */
	private int mCookieMaxAge;

	/** Property to hold  cookie secure. */
	private boolean mCookieSecure;
	/** Property to hold  cookie secure. */
	private boolean mEnabled;
	/**
	 * property to hold the mProfileCompnt.
	 */
	private String mProfileCompnt;

	/** 
	 * property to hold tRUConfiguration. 
	 */
	private TRUConfiguration mTruConfiguration;



	/** Property to hold property manager. */
	private TRUPropertyManager mPropertyManager;

	/**
	 * Gets the TRU configuration.
	 * 
	 * @return the tRUConfiguration
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * Sets the TRU configuration.
	 * 
	 * @param pTruConfiguration
	 *            the tRUConfiguration to set
	 */
	public void setTruConfiguration(TRUConfiguration pTruConfiguration) {
		mTruConfiguration = pTruConfiguration;
	}


	/**
	 * Gets the property manager.
	 *
	 * @return the property manager.
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 *
	 * @param pPropertyManager
	 *            the new property manager.
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * This property hold reference for mEnableSiteCookieCreation.
	 */
	private boolean mEnableSiteCookieCreation;

	/**
	 * Gets the mEnableSiteCookieCreation.
	 * 
	 * @return mEnableSiteCookieCreation
	 */
	public boolean isEnableSiteCookieCreation() {
		return mEnableSiteCookieCreation;
	}

	/**
	 * set the pEnableSiteCookieCreation.
	 *
	 * @param pEnableSiteCookieCreation the Enable Site Cookie Creation
	 */
	public void setEnableSiteCookieCreation(boolean pEnableSiteCookieCreation) {
		mEnableSiteCookieCreation = pEnableSiteCookieCreation;
	}


	/**
	 * This property hold reference for mEnableDefaultSiteIdCookieCreation.
	 */
	private boolean mEnableDefaultSiteIdCookieCreation;

	/**
	 * Gets the mEnableDefaultSiteIdCookieCreation.
	 * 
	 * @return mEnableDefaultSiteIdCookieCreation
	 */
	public boolean isEnableDefaultSiteIdCookieCreation() {
		return mEnableDefaultSiteIdCookieCreation;
	}

	/**
	 * set the pEnableDefaultSiteIdCookieCreation.
	 *
	 * @param pEnableDefaultSiteIdCookieCreation the Enable Default SiteId Cookie Creation
	 */
	public void setEnableDefaultSiteIdCookieCreation(boolean pEnableDefaultSiteIdCookieCreation) {
		mEnableDefaultSiteIdCookieCreation = pEnableDefaultSiteIdCookieCreation;
	}

	/**
	 * This servlet is used to generates the cookies with firstName, itemCount, freeShippingBar and
	 * setting into the response.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	@Override
	public void service(final DynamoHttpServletRequest pRequest,final DynamoHttpServletResponse pResponse) throws IOException,
	ServletException {

		if (isLoggingDebug()) {
			logDebug("Start: TRUSetCookiesServlet.service()");
		}
		if (isEnabled() && !(pResponse.getResponse() instanceof HttpServletResponseWrapper)) {
			HttpServletResponseWrapper l_oWrappedResponse = new HttpServletResponseWrapper((HttpServletResponse) pResponse.getResponse()) {

				protected void setCookies() {

					TRUOrderHolder shoppingCart = (TRUOrderHolder) pRequest.resolveName(TRUCommerceConstants.SHOPPING_CART_COMPONENT_NAME);
					//create desktop full site cookie for mobile
					String desktopSiteActive = pRequest.getQueryParameter(TRUConstants.DESKTOP_SITE_ACTIVE)	;
					boolean cookieExist = Boolean.FALSE ;
					if(pRequest.getCookies() != null){
						for (Cookie cookie : pRequest.getCookies()) {
							if (cookie.getName().equals(TRUConstants.DESKTOP_VIEW_COOKIE)){
								cookieExist = Boolean.TRUE;
								break;
							} 
						}
					}
					if(StringUtils.isNotBlank(desktopSiteActive) && !cookieExist){
						shoppingCart.getCookieManager().createCookieForDeskTopFullSite(pRequest, pResponse,desktopSiteActive);
					}
					shoppingCart.getCookieManager().createCookieForFreeShippingProgressBar(shoppingCart.getCurrent(),pRequest, pResponse);
					shoppingCart.getCookieManager().createCookieForCurrentTimeStamp(pRequest, pResponse);
					shoppingCart.getCookieManager().createCookieForOrderItemCount(shoppingCart.getCurrent(),shoppingCart.getProfile(),pRequest, pResponse);
					createCookieForFirstName(pRequest, pResponse);
					if(isEnableSiteCookieCreation()) {
						createCookieForAkamaiSiteRedirection(pRequest, pResponse);
					}
					shoppingCart.getCookieManager().createAlternateDomainCookie(pRequest, pResponse);
				}

				@Override
				public void flushBuffer() throws IOException {
					if (!isCommitted()) {
						setCookies();
					}
					super.flushBuffer();
				}

				@Override
				public PrintWriter getWriter() throws IOException {
					return new PrintWriter(super.getWriter()) {
						@Override
						public void flush() {
							if (!isCommitted()) {
								setCookies();
							}
							super.flush();

						}
					};
				}

				@Override
				public ServletOutputStream getOutputStream() throws IOException {
					// TODO Auto-generated method stub
					return new LazyServletOutputStream(this.getResponse()) {

						@Override
						public void flush() throws IOException {
							if (!isCommitted()) {
								setCookies();
							}
							super.flush();
						}

					};
				}
			};
			pResponse.setResponse(l_oWrappedResponse);
			passRequest(pRequest, pResponse);
		} else {
			passRequest(pRequest, pResponse);
		}

		if (isLoggingDebug()) {
			logDebug("End: TRUSetCookiesServlet.service()");
		}
	}


	/**
	 * Profile'FirstName: generating and removing cookie related to firstName based on below condition
	 * Before creating new cookies, first, it will check whether it is exist or not.
	 * @param pRequest  - the servlet's request
	 * @param pResponse - the servlet's response
	 */
	public void createCookieForAkamaiSiteRedirection(DynamoHttpServletRequest pRequest,	DynamoHttpServletResponse pResponse) {

		if (isLoggingDebug()) {
			logDebug("BEGIN::::: TRUSetCookiesServlet.createCookieForAkamaiSiteRedirection()");
		}
		boolean createCookie = Boolean.FALSE;

		String akamaiSiteCookie = pRequest.getCookieParameter(getTruConfiguration().getAkamaiSiteParam());
		String cookieSiteId = null;
		Map<String, String> siteIdMapping = getTruConfiguration().getCustomParamToSiteIdMapping();
		if(StringUtils.isNotEmpty(akamaiSiteCookie) && siteIdMapping != null && !siteIdMapping.isEmpty()) {
			cookieSiteId = siteIdMapping.get(akamaiSiteCookie);
			if (isLoggingDebug()) {
				vlogDebug("The value of Cookie Site Id set is ::: {0)", cookieSiteId);
			}
		}

		//Site site = SiteContextManager.getCurrentSite();
		String siteId = SiteContextManager.getCurrentSiteId();

		/*//Get the Maximum age of the Cookie from Site Level.
		int maxAge = 0;
		if(site != null && null != site.getPropertyValue(getPropertyManager().getPersistOrderDays())){
			maxAge = (int)site.getPropertyValue(getPropertyManager().getPersistOrderDays());
			if (isLoggingDebug()) {
				vlogDebug("Maximum age of the Cookie is ::: {0}", maxAge);
			}
		}*/

		if(StringUtils.isEmpty(cookieSiteId) && isEnableDefaultSiteIdCookieCreation()) {
			createCookie = Boolean.TRUE;
		} else if(StringUtils.isNotEmpty(cookieSiteId) && StringUtils.isNotEmpty(siteId) && !siteId.equals(cookieSiteId)) {
			createCookie = Boolean.TRUE;
		}

		if(createCookie && siteIdMapping != null && StringUtils.isNotEmpty(siteId)) {
			String cookieKey = null;
			for(Entry<String, String> entry : siteIdMapping.entrySet()) {
				cookieKey = entry.getKey();
				String cookieValue = entry.getValue();
				if(StringUtils.isNotEmpty(cookieValue) && siteId.equals(cookieValue)) {
					break;
				}
			}
			if(StringUtils.isNotEmpty(cookieKey)) {
				Cookie akamaiCookie = new Cookie(getTruConfiguration().getAkamaiSiteParam(), cookieKey);
				akamaiCookie.setPath(TRUConstants.PATH);
				akamaiCookie.setDomain(DOMAIN_NAME);
				akamaiCookie.setMaxAge(Integer.MAX_VALUE);

				if (isLoggingDebug()) {
					vlogDebug("Resetting the Site Cookie value to {0}. Previous value of cookie is {1}", cookieKey, akamaiSiteCookie);
				}
				pResponse.addCookie(akamaiCookie);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END::::: TRUSetCookiesServlet.createCookieForAkamaiSiteRedirection()");
		}
	}

	/**
	 * Profile'FirstName: generating and removing cookie related to firstName based on below condition
	 * Before creating new cookies, first, it will check whether it is exist or not.
	 * @param pRequest  - the servlet's request
	 * @param pResponse - the servlet's response
	 */
	public void createCookieForFirstName(DynamoHttpServletRequest pRequest,	DynamoHttpServletResponse pResponse) {

		if (isLoggingDebug()) {
			logDebug("Start: TRUSetCookiesServlet.createCookieForFirstName()");
		}

		Profile profile = (Profile) pRequest.resolveName(getProfileCompnt());
		PropertyManager lPropertyManager = profile.getProfileTools().getPropertyManager();
		String firstNameValue = (String) profile.getPropertyValue(lPropertyManager.getFirstNamePropertyName());
		String emailId=(String) profile.getPropertyValue(lPropertyManager.getEmailAddressPropertyName());
		if (StringUtils.isBlank(firstNameValue) && !StringUtils.isBlank(emailId)) {
			firstNameValue= emailId.split(TRUConstants.AT_SIGN)[TRUConstants.ZERO];
		}
		if (!StringUtils.isBlank(firstNameValue)  ) {
			String reqFirstName = pRequest.getCookieParameter(TRUConstants.PROFILE_FIRST_NAME);
			if ((reqFirstName != null) &&(firstNameValue.equals(reqFirstName))) {
				return;
			}else{
				generateCookie(TRUConstants.PROFILE_FIRST_NAME, firstNameValue, pResponse, false);
			}
		}else{
			expireFirstNameCookie(TRUConstants.PROFILE_FIRST_NAME, pResponse);
		}

		if (isLoggingDebug()) {
			logDebug("End: TRUSetCookiesServlet.createCookieForFirstName()");
		}
	}



	/**
	 * This method removes the cookies related to firstName.
	 *
	 * @param pFirstNameCookie the first name cookie
	 * @param pResponse - the servlet's response
	 */
	public void expireFirstNameCookie(String pFirstNameCookie,DynamoHttpServletResponse pResponse){

		if (isLoggingDebug()) {
			logDebug("Start: TRUSetCookiesServlet.removeFirstNameCookie()");
		}
		generateCookie(TRUConstants.PROFILE_FIRST_NAME, pFirstNameCookie,pResponse, true);


		if (isLoggingDebug()) {
			logDebug("End: TRUSetCookiesServlet.removeFirstNameCookie()");
		}
	}

	/**
	 * This method is initializing and removing the cookie values and setting into the response.
	 *
	 * @param pCookie the cookie
	 * @param pResponse the response
	 * @param pHaveToRemove the have to remove
	 */

	void initializeCookie(Cookie pCookie, DynamoHttpServletResponse pResponse, boolean pHaveToRemove)
	{
		if (pHaveToRemove != true) {
			pCookie.setMaxAge(TRUConstants.MAX_AGE_VALUE);
			pCookie.setPath(TRUConstants.PATH);
			pCookie.setSecure(isCookieSecure());
			pResponse.addCookie(pCookie);
		} else {
			pCookie.setMaxAge(TRUConstants.ZERO);
			//pCookie.setSecure(TRUConstants.COOKIE_SECURE_TRUE);
			//pCookie.setValue(TRUConstants.EMPTY);
			pCookie.setPath(TRUConstants.PATH);
			pResponse.addCookie(pCookie);
		}
	}

	/**
	 * This method is basically used to generate cookie.
	 *
	 * @param pCookieName -cookie Name
	 * @param pCookieValue -cookie Value
	 * @param pResponse the response
	 * @param pHaveToRemove the have to remove
	 */
	public void generateCookie(String pCookieName, String pCookieValue, DynamoHttpServletResponse pResponse, boolean pHaveToRemove)
	{
		Cookie cookie = null;
		if (!StringUtils.isBlank(pCookieName) && !StringUtils.isBlank(pCookieValue)){
			cookie= new Cookie(pCookieName, pCookieValue);
			initializeCookie(cookie, pResponse, pHaveToRemove );
		}


	}

	/**
	 * This method used to appending value using delimiter.
	 *
	 * @param pShortOfAmount the short of amount
	 * @param pPromotionAmount the promotion amount
	 * @return the string
	 */
	public String appendingValue(double pShortOfAmount, double pPromotionAmount) {
		StringBuilder sb = new StringBuilder();
		sb.append(String.valueOf(pShortOfAmount)).append(TRUConstants.PIPE_STRING).append(String.valueOf(pPromotionAmount));
		return sb.toString();
	}

	/**
	 * Gets the shopping cart utils.
	 * 
	 * @return the shopping cart utils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * Sets the shopping cart utils.
	 * 
	 * @param pShoppingCartUtils
	 *            the new shopping cart utils
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		this.mShoppingCartUtils = pShoppingCartUtils;
	}

	/**
	 * Gets the cookie path.
	 *
	 * @return the cookiePath
	 */
	public String getCookiePath() {
		return mCookiePath;
	}


	/**
	 * Sets the cookie path.
	 *
	 * @param pCookiePath the cookiePath to set
	 */
	public void setCookiePath(String pCookiePath) {
		this.mCookiePath = pCookiePath;
	}

	/**
	 * Gets the cookie max age.
	 *
	 * @return the cookieMaxAge
	 */
	public int getCookieMaxAge() {
		return mCookieMaxAge;
	}

	/**
	 * Sets the cookie max age.
	 *
	 * @param pCookieMaxAge the cookieMaxAge to set
	 */
	public void setCookieMaxAge(int pCookieMaxAge) {
		this.mCookieMaxAge = pCookieMaxAge;
	}

	/**
	 * Checks if is cookie secure.
	 *
	 * @return the cookieSecure
	 */
	public boolean isCookieSecure() {
		return mCookieSecure;
	}

	/**
	 * Sets the cookie secure.
	 *
	 * @param pCookieSecure the cookieSecure to set
	 */
	public void setCookieSecure(boolean pCookieSecure) {
		this.mCookieSecure = pCookieSecure;
	}

	/**
	 * Checks if is enabled.
	 *
	 * @return true, if is enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets the enabled.
	 *
	 * @param pEnabled the new enabled
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}
	/**
	 * @return the profileCompnt
	 */
	public String getProfileCompnt() {
		return mProfileCompnt;
	}

	/**
	 * @param pProfileCompnt the profileCompnt to set
	 */
	public void setProfileCompnt(String pProfileCompnt) {
		mProfileCompnt = pProfileCompnt;
	}

}
