package com.tru.dynamo.servlet.dafpipeline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.InsertableServletImpl;

import com.tru.commerce.order.purchase.TRUPurchaseProcessHelper;
import com.tru.userprofiling.TRUCookieManager;

/**
 * This is TRUAbandonCartServlet class.
 * @author PA
 * @version 1.0
 *
 */
public class TRUAbandonCartServlet extends InsertableServletImpl{
	/**
	 * property to hold mEnabled.
	 */
	private boolean mEnabled;
	/** 
	 * property to hold mPurchaseProcessHelper.
	 */
	private TRUPurchaseProcessHelper mPurchaseProcessHelper;
	
	/** The Ignore request list. */
	private List<String> mIgnoreRequestList = new ArrayList<String>();
	/**
	 * This servlet is used to save the cart items and date in the cookie.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException, ServletException {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUAbandonCartServlet service()");
		}
		if(isEnabled()){
			TRUCookieManager cookieManager = (TRUCookieManager) getPurchaseProcessHelper().getProfileTools().getCookieManager();
			if(cookieManager!=null && getIgnoreRequestList() != null && !getIgnoreRequestList().isEmpty()){
				boolean passRequest = true;
				for (String ignoreRequest : getIgnoreRequestList()) {
					if(pRequest.getRequestURI().contains(ignoreRequest)){
						passRequest = false;
					}
				}
				if(passRequest){
					((TRUCookieManager) cookieManager).createAbandonCartCookie(pRequest, pResponse);
				}
			}
		}
		passRequest(pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("Exting from TRUAbandonCartServlet service()");
		}
	}
	/**
	 * @return the mEnabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}
	/**
	 * @param pEnabled the mEnabled to set
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}
	/**
	 * @return the mPurchaseProcessHelper
	 */
	public TRUPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}
	/**
	 * @param pPurchaseProcessHelper the mPurchaseProcessHelper to set
	 */
	public void setPurchaseProcessHelper(TRUPurchaseProcessHelper pPurchaseProcessHelper) {
		mPurchaseProcessHelper = pPurchaseProcessHelper;
	}
	
	/**
	 * Gets the ignore request list.
	 *
	 * @return the ignore request list
	 */
	public List<String> getIgnoreRequestList() {
		return mIgnoreRequestList;
	}
	
	/**
	 * Sets the ignore request list.
	 *
	 * @param pIgnoreRequestList the new ignore request list
	 */
	public void setIgnoreRequestList(List<String> pIgnoreRequestList) {
		mIgnoreRequestList = pIgnoreRequestList;
	}
	
}
