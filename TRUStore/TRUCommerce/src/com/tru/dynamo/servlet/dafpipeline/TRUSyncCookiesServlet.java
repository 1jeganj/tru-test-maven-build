
/**
 * 
 */
package com.tru.dynamo.servlet.dafpipeline;


import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import atg.core.util.StringUtils;
import atg.multisite.SiteContextManager;
import atg.nucleus.naming.ComponentName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.InsertableServletImpl;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.common.TRUConstants;
import com.tru.common.TRUSession;
import com.tru.common.vo.TRUSiteVO;
import com.tru.userprofiling.TRUCookieManager;
import com.tru.utils.TRUGetSiteTypeUtil;

/**
 * The Class TRUSyncCookiesServlet.
 * This Class is used to sync the cookies between different sites.
 * @author PA
 * @version 1.0
 */
public class TRUSyncCookiesServlet extends InsertableServletImpl {
	
	/**
	 * property to hold TRUCookieManager.
	 */
	private TRUCookieManager mCookieManager;

	/** Holds property mEnable . */
	private boolean mEnabled;
	
	/** property to Hold TRU_SESSION_COMPONENT. */
	public final static ComponentName TRU_SESSION_COMPONENT = ComponentName.getComponentName("/com/tru/common/TRUSession");
	
	/** property to Hold mTruGetSiteTypeUtil. */
	private TRUGetSiteTypeUtil mTruGetSiteTypeUtil;
	
	/**
	 * @return the truGetSiteTypeUtil
	 */
	public TRUGetSiteTypeUtil getTruGetSiteTypeUtil() {
		return mTruGetSiteTypeUtil;
	}



	/**
	 * @param pTruGetSiteTypeUtil the truGetSiteTypeUtil to set
	 */
	public void setTruGetSiteTypeUtil(TRUGetSiteTypeUtil pTruGetSiteTypeUtil) {
		mTruGetSiteTypeUtil = pTruGetSiteTypeUtil;
	}



	/**
	 * This servlet is used to Sync cookie between sites.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException,
			ServletException {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUSyncCookiesServlet service()");
		}

		if (isEnabled()) {
			TRUSiteVO truSiteVO = null;
			if(getTruGetSiteTypeUtil() != null){
				truSiteVO =  getTruGetSiteTypeUtil().getSiteInfo(pRequest, SiteContextManager.getCurrentSite());
			}
			Cookie requestCookie = null;
			TRUSession truSession = (TRUSession) pRequest.resolveName(TRU_SESSION_COMPONENT);
			if (truSession != null) {
				Map<String, Cookie> truCookies = truSession.getTRUCookies();
				TRUCookieManager cookieManager = getCookieManager();
				if(truCookies != null && truSiteVO != null && StringUtils.isNotBlank(truSiteVO.getSiteStatus()) && truSiteVO.getSiteStatus().equals(TRUCommerceConstants.SOS)){
						if (isLoggingDebug()) {
							logDebug("inTRUSyncCookiesServlet service() : site is 'sos' so we are removing cart cookie");
						}
					truCookies.remove(cookieManager.getOrderCookieName());
					truCookies.remove(TRUConstants.LOGGEDEIN_CART_COOKIE_NAME);
				}
				if (truCookies != null) {
					for (String cookieName : truCookies.keySet()) {
						requestCookie = cookieManager.getCookieFromRequest(cookieName, pRequest);
						if (requestCookie == null) {
							if (isLoggingDebug()) {
								logDebug("Create new and copy Cookie values: " + cookieName);
							}
							cookieManager.copyCookie(pResponse, truCookies.get(cookieName));
						} else if (!requestCookie.getValue().equalsIgnoreCase(truCookies.get(cookieName).getValue())) {
							if (isLoggingDebug()) {
								logDebug("Session cookie name : " + cookieName);
								logDebug("Session cookie value : " + truCookies.get(cookieName).getValue());
							}
							cookieManager.copyCookie(pResponse, truCookies.get(cookieName));
						}
					}
				}
			}
		}
		passRequest(pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("Exiting into TRUSyncCookiesServlet service()");
		}
	}
	
	
	
	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * @param pEnabled the enabled to set
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}
	

	/**
	 * Gets the cookie manager.
	 *
	 * @return the cookie manager
	 */
	public TRUCookieManager getCookieManager() {
		return mCookieManager;
	}

	/**
	 * Sets the cookie manager.
	 *
	 * @param pCookieManager the new cookie manager
	 */
	public void setCookieManager(TRUCookieManager pCookieManager) {
		mCookieManager = pCookieManager;
	}

}
