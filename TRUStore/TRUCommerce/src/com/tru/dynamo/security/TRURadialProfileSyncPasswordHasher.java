package com.tru.dynamo.security;

import atg.nucleus.GenericService;
import atg.security.PasswordHasher;

import com.globalsportsinc.crypt.CryptKeeper;

/**
 * The Class TRURadialProfileSyncPasswordHasher.
 */
public class TRURadialProfileSyncPasswordHasher extends GenericService implements PasswordHasher {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The mPwd hasher component path. */
	private String mPwdHasherComponentPath;

	/**
	 * Sets the pwd hasher component path.
	 *
	 * @param pPwdHasherComponentPath the new pwd hasher component path
	 */
	public void setPwdHasherComponentPath(String pPwdHasherComponentPath) {
		this.mPwdHasherComponentPath = pPwdHasherComponentPath;
	}

	/**
	 * get Password Hasher ComponentPath
	 *
	 * @return the pwd hasher component path
	 */
	public String getPwdHasherComponentPath() {
		return this.mPwdHasherComponentPath;
	}

	/**
	 * encrypt Password.
	 *
	 * @param pPassowrd the passowrd
	 * @return the string
	 */
	@Override
	public String encryptPassword(String pPassowrd) {
		return CryptKeeper.encryptAndEncode(pPassowrd);
	}

	/**
	 * checkPassword.
	 *
	 * @param pLoginPassword the login password
	 * @param pEncryptedPassword the encrypted password
	 * @param pHashKey the hash key
	 * @return true, if successful
	 */
	@Override
	public boolean checkPassword(String pLoginPassword, String pEncryptedPassword, Object pHashKey) {
		 return pLoginPassword.equals(pEncryptedPassword);
	}

	/**
	 * hashPasswordForLogin.
	 *
	 * @param pPassowrd the passowrd
	 * @return the string
	 */
	@Override
	public String hashPasswordForLogin(String pPassowrd) {
		return CryptKeeper.encryptAndEncode(pPassowrd);
	}
	
	/**
	 * getPasswordHashKey.
	 *
	 * @return the password hash key
	 */
	@Override
	public Object getPasswordHashKey() {
		return null;
	}

	/**
	 * getLoginPasswordHasher.
	 *
	 * @return the login password hasher
	 */
	@Override
	public PasswordHasher getLoginPasswordHasher() {
		TRURadialProfileSyncPasswordHasher hasher=new TRURadialProfileSyncPasswordHasher();
		hasher.setPwdHasherComponentPath(getPwdHasherComponentPath());
		return hasher;
	}
	
	
}
