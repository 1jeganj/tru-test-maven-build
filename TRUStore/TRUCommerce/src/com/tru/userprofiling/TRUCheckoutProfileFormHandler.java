package com.tru.userprofiling;


import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import atg.commerce.CommerceException;
import atg.commerce.order.CreditCard;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.ContactInfo;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.globalsportsinc.crypt.CryptKeeper;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.purchase.TRUPurchaseProcessHelper;
import com.tru.commerce.profile.TRUProfileFormHandler;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.messaging.oms.TRUCustomerOrderUpdateSource;

/**
 * class TRUCheckoutProfileFormHandler extends TRUProfileFormHandler.
 * @author professional access
 * @version 1.0
 */
public class TRUCheckoutProfileFormHandler extends TRUProfileFormHandler {
	
	/** Property to Hold purchase process helper. */
	private TRUPurchaseProcessHelper mPurchaseProcessHelper;
	
	/**
	 * Holds orderUpdateSource
	 */
	private TRUCustomerOrderUpdateSource mOrderUpdateSource;
	
	/**
	 * This method handle creating new user from order confirmation page.
	 * @param  pRequest - DynamoHttpServletRequest
	 * @param pResponse - DynamoHttpServletResponse
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 * @return boolean -true/false
	 */
	@SuppressWarnings("unchecked")
	public boolean handleNewCustomer(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("TRUCheckoutProfileFormHandler.handleNewCustomer()method.STARTS");
		}
		String myHandleMethod = "TRUCheckoutProfileFormHandler.handleNewCustomer";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean lRollbackFlag = false;
		TransactionManager tm = getTransactionManager();
		TransactionDemarcation td = getTransactionDemarcation();
		if(rrm == null || rrm.isUniqueRequestEntry(myHandleMethod)) {
			try {
				td.begin(tm, TransactionDemarcation.REQUIRED);
				ContactInfo shippingAddress=null;
				TRUOrderImpl order = (TRUOrderImpl)getShoppingCart().getLast();
				String email = getPropertyManager().getEmailAddressPropertyName();
				String login = getPropertyManager().getLoginPropertyName();
				if(order != null){
					ShippingGroup sg = (ShippingGroup)order.getShippingGroups().get(0);
					if (sg instanceof TRUHardgoodShippingGroup) {
						shippingAddress = (ContactInfo)((TRUHardgoodShippingGroup) sg).getShippingAddress();
						setValueProperty(email,getValue().get(email));
						setValueProperty(login,getValue().get(login));
					}
					//This is for order update service call
					setOrderId(order.getId());
					super.handleCreate(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							logDebug("Redirecting due to form error in handleNewCustomer.");
						}
						return checkFormRedirect(getCreateSuccessURL(),getCreateErrorURL(), pRequest, pResponse);
					}
					int countOfHardgoodShippingGroup=0;
					List shippingGroups =order.getShippingGroups();
					Set<String> addressNickNames = new HashSet<String>();	
					final boolean isOrderOnlyDonationItems = getPurchaseProcessHelper().isOrderHavingOnlyDonationItems(order);
					if(!isOrderOnlyDonationItems){
						int shippingGroupSize = shippingGroups.size();
						for(int i=0;i<shippingGroupSize;i++){
							ShippingGroup shippingGroup=(ShippingGroup) shippingGroups.get(i);
							if (shippingGroup instanceof TRUHardgoodShippingGroup && !((TRUHardgoodShippingGroup) shippingGroup).isRegistryAddress()) {
								shippingAddress = (ContactInfo)((TRUHardgoodShippingGroup) shippingGroup).getShippingAddress();
								addressNickNames.add(((TRUHardgoodShippingGroup)shippingGroup).getNickName());
								boolean isDuplicateNickName = getProfileManager().validateDuplicateAddressNickname(getProfile(), ((TRUHardgoodShippingGroup)shippingGroup).getNickName());
								if(!isDuplicateNickName) {
									((TRUProfileTools) getProfileTools()).createProfileRepositorySecondaryAddress(getProfile(), ((TRUHardgoodShippingGroup)shippingGroup).getNickName(), shippingAddress);
									countOfHardgoodShippingGroup++;
									if(countOfHardgoodShippingGroup==TRUCheckoutConstants.INT_ONE){
										((TRUProfileTools) getProfileTools()).setProfileDefaultAddress((getProfileItem()));
									}else{
										vlogDebug("Shipping Group count is other than one");
									}
								}else{
									vlogDebug("isDuplicateNickName is true");
								}
							}
						}
					}
					//START: Checkout Billing Address CR changes
					TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
					List<PaymentGroup> listOfPaymentGroup = (List<PaymentGroup>) order.getPaymentGroups();
					TRUCreditCard creditCard = null;
					for(PaymentGroup pg : listOfPaymentGroup){
						if(pg instanceof CreditCard){
							creditCard = (TRUCreditCard)pg;
							break;
						}
					}
					if(creditCard instanceof CreditCard){
						Boolean isRetryAccept = creditCard.getPaymentRetryAccept();
						if( isRetryAccept != null && !isRetryAccept){
							String billingAddressNickName = order.getBillingAddressNickName();
							((TRUProfileTools) getProfileTools()).createProfileRepositorySecondaryAddress(getProfile(), billingAddressNickName, creditCard.getBillingAddress());
							RepositoryItem billingAddress = profileTools.getSecondaryAddressByNickName(getProfile(), billingAddressNickName);
							String creditCardNickName = profileTools.getUniqueCreditCardNickname(creditCard, getProfile(), null);
							profileTools.copyCreditCardToProfile(creditCard, getProfileItem(), creditCardNickName);
							profileTools.setCreditCardBillingAddress(creditCardNickName, billingAddress, getProfile());
							((MutableRepositoryItem)billingAddress).setPropertyValue(((TRUPropertyManager)profileTools.getPropertyManager()).getIsBillingAddressPropertyName(), Boolean.TRUE);
							profileTools.setProfileDefaultCard(getProfileItem());
							TRUOrderImpl currentOrder = (TRUOrderImpl)getShoppingCart().getCurrent();
							currentOrder.setBillingAddress(null);
						}
					}
					//END: Checkout Billing Address CR changes
					String rewardNumber = order.getRewardNumber();
					profileTools.updateProperty(((TRUPropertyManager)profileTools.getPropertyManager()).getRewardNumberPropertyName(), rewardNumber, getProfile());
					// setting the last updated time to profile 
					final String password = getStringValueProperty(getPropertyManager().getPasswordPropertyName()).trim();
					profileTools.updateProperty(((TRUPropertyManager)profileTools.getPropertyManager()).getRadialPasswordPropertyName(),CryptKeeper.encryptAndEncode(password), getProfile());
					profileTools.updateProperty(((TRUPropertyManager)profileTools.getPropertyManager()).getLastProfileUpdatePropertyName(), new Date(), getProfile());
					//This will be uncommented in R3
					/* if(getCustomerMasterImportSource() != null && !StringUtils.isBlank(rewardNumber)){
						 getCustomerMasterImportSource().sendCustomerMasterImport(getProfile(), null);
					 }*/
					if(getOrderUpdateSource() != null && order != null){
						getOrderUpdateSource().sendCustomerOrderUpdate(getProfile(), order);
					}
				}
			}catch (RepositoryException e) {
				lRollbackFlag=true;
				if (isLoggingError()) {
				vlogError("RepositoryException occured @Class::TRUCheckoutProfileFormHandler::@method::handleNewCustomer()", e);
				}
			}  catch (TransactionDemarcationException trDem) {
				lRollbackFlag = true;
				if (isLoggingError()) {
				vlogError("TransactionDemarcationException occured @Class::TRUCheckoutProfileFormHandler::@method::handleNewCustomer()", trDem);
				}
			} catch (CommerceException ce) {
				lRollbackFlag = true;
				if (isLoggingError()) {
					vlogError("CommerceException occured @Class::TRUCheckoutProfileFormHandler::@method::handleNewCustomer()", ce);
				}
			}finally {
				if (tm != null) {
					try {
						td.end(lRollbackFlag);
					} catch (TransactionDemarcationException tD) {
						if (isLoggingError()) {
						vlogError("TransactionDemarcationException @Class::TRUCheckoutProfileFormHandler::@method::handleNewCustomer()", tD);
						}
					}
				} 
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		return checkFormRedirect(getCreateSuccessURL(),getCreateErrorURL(), pRequest, pResponse);
	}

	/**
	 * @return the mPurchaseProcessHelper
	 */
	public TRUPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}

	/**
	 * @param pPurchaseProcessHelper the mPurchaseProcessHelper to set
	 */
	public void setPurchaseProcessHelper(TRUPurchaseProcessHelper pPurchaseProcessHelper) {
		this.mPurchaseProcessHelper = pPurchaseProcessHelper;
	}

	/**
	 * This is to get the value for orderUpdateSource
	 *
	 * @return the orderUpdateSource value
	 */
	public TRUCustomerOrderUpdateSource getOrderUpdateSource() {
		return mOrderUpdateSource;
	}

	/**
	 * This method to set the pOrderUpdateSource with orderUpdateSource
	 * 
	 * @param pOrderUpdateSource the orderUpdateSource to set
	 */
	public void setOrderUpdateSource(TRUCustomerOrderUpdateSource pOrderUpdateSource) {
		mOrderUpdateSource = pOrderUpdateSource;
	}


}