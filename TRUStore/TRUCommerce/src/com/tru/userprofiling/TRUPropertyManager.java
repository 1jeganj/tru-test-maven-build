package com.tru.userprofiling;

import atg.commerce.profile.CommercePropertyManager;

/**
 * extending property manager to add custom properties used in profile.
 * @author Professional Access
 * @version 1.0
 */
public class TRUPropertyManager extends CommercePropertyManager {
	
	/** Property to hold mItemInStorePickUp. */
	private String mShipToStoreEligible;
	
	/** Property to hold mItemInStorePickUp. */
	private String mItemInStorePickUp;
	
	/** Property to hold mNmwaItemDescPropertyName. */
	private String mNmwaItemDescPropertyName;
	
	/** Property to hold mNmwaIdPropertyName. */
	private String	mNmwaIdPropertyName;
	
	/** Property to hold NmwaEmailAddress. */
	private String	mNmwaEmailAddressPropertyName;
	
	/** Property to hold NmwaProductId. */
	private String	mNmwaProductIdPropertyName;
	
	/** Property to hold NmwaSkuId. */
	private String	mNmwaSkuIdPropertyName;
	
	/** Property to hold NmwaReqDate. */
	private String	mNmwaReqDatePropertyName;
	
	/** Property to hold NmwaEmailSentDate. */
	private String	mNmwaEmailSentDatePropertyName;
	
	/** Property to hold nmwaEmailSentFlag. */
	private String mNmwaEmailSentFlag;
	
	/** Property to hold siteStatusPropertyName. */
	private String mSiteStatusPropertyName;
	/**
	 * member variable to hold eciFlag property.
	 */
	private String mEciFlagPropertyName;
	/**
	 * member variable to hold cavv property.
	 */
	private String mCavvPropertyName;
	/**
	 * member variable to hold xid property.
	 */
	private String mXidPropertyName;
	/**
	 * member variable to hold confirm password property.
	 */
	private String mConfirmPasswordPropertyName;

	/**
	 * member variable to hold Reward Number property.
	 */
	private String mRewardNumberPropertyName;
	/**
	 * member variable to hold Nick name property.
	 */
	private String mNicknamePropertyName;
	
	/**
	 * member variable to hold old password property.
	 */
	private String mOldpasswordPropertyName;
	
	/**
	 * member variable to hold confirm password property.
	 */
	private String mConfirmpasswordPropertyName;
	/**
	 * member variable to hold cname on card property.
	 */
	private String mNameOnCardPropertyName;
	
	/**
	 * member variable to hold upper range property.
	 */
	private String mUpperRangePropertyName;
	
	/**
	 * member variable to hold lower range property.
	 */
	private String mLowerRangePropertyName;
	
	/**
	 * member variable to hold credit card type property.
	 */
	private String mCreditCardTypePropertyName;
	/**
	 * Member variable to hold card verification number.
	 */
	private String mCreditCardVerificationNumberPropertyName;

	/**
	 * Member variable to hold card verification number.
	 */
	private String mRecentlyViewedPropertyName;
	
		/** The m color upc level. */
	private String mColorUpcLevel;

	/** The m juvenile product size. */
	private String mJuvenileProductSize;

	/**
	 * Member variable to hold mGiftFinderURL.
	 */
	private String mGiftFinderURL;
	
	/** The Enable persist order. */
	private String mEnablePersistOrderPropertyName;
	
	/** The loyalty customer property name. */
	private String mLoyaltyCustomer;
	
	/** The mFreeShippingPromotionPropertyName. */
	private String mFreeShippingPromotionPropertyName;
	
	/** The mOrderLimitPropertyName. */
	private String mOrderLimitPropertyName;
	
	/** TMax coupon limit. */
	private String mMaxCouponLimitPropertyName;
	
	/** Holds the CardinalToken. */
	private String mCreditCardTokenPropertyName;
	/** Holds property for gift wrap comm item id. */
	private String mGiftWrapCommItemId;
	
	/** Holds property for gift wrap quantity. */
	private String mGiftWrapQuantity;
	
	/** Holds property for gift item descriptor name. */
	private String mGiftItemDescriptorName;
	
	/** Holds property for abandon cart threshold. */
	private String mAbandonCartThreshold;
	
	/** The Reminder count. */
	private String mReminderCount;
	
	/** Holds property for persist order days. */
	private String mPersistOrderDays;
	
	/** Holds property for mEnableAddressDoctorPropertyName. */
	private String mEnableAddressDoctorPropertyName;
	
	/** The Max gift card limit. */
	private String mMaxGiftCardLimitPropertyName;

	/** The Max gift card limit. */
	private String mReversalIdPropertyName;

	/** The mTypePropertyName. */
	private String mTypePropertyName;

	/** The mMessagePropertyName. */
	private String mMessagePropertyName;
	
	/** The mOmsErrorMessageItemName. */
	private String mOmsErrorMessageItemName;
	
	private String mRequestPropertyName;
	
	/** The mUser. */
	private String mUserItemName;
	
	/** The mTransactionTimePropertyName. */
	private String mTransactionTimePropertyName;
	
	
	/** The Selected cc nick name property name. */
	private String mSelectedCCNickNamePropertyName;
	
	/** The previous Shipping Address NickName  property name. */
	private String mPrevShippingAddressNickNamePropertyName;
	
	/** The mWord. */
	private String mWord;
	
	/** The mRecentlyViewedDate. */
	private String mRecentlyViewedDate;
	
	/** The mRecentlyViewedThreshold. */
	private String mRecentlyViewedThreshold;
	/** The mMigratedProfile. */
	private String mMigratedProfile;
	/** The mMigratedProfileResetPwd. */
	private String mMigratedProfileResetPwd;
	
	/** Holds StreetDate property name for SKU item. */
	private String mStreetDate;
	
	/** The Previous shipping address property name. */
	private String mPreviousShippingAddressPropertyName;
	
	/** Property to hold mSourceOfOrderPropertyName. */
	private String mSourceOfOrderPropertyName;
	
	/** Property to hold mLayawayItemDescriptorPropertyName. */
	private String mLayawayItemDescriptorPropertyName;
	
	/** Property to hold mErrorKeyPropertyName. */
	private String mErrorKeyPropertyName;
	
	/** Property to hold mDefaultErrorMessagePropertyName. */
	private String mDefaultErrorMessagePropertyName;
	
	/** Property to hold mOwnerIdPropertyName. */
	private String mOwnerIdPropertyName;
	
	/** Property to hold mLastActivityPropertyName. */
	private String mLastActivityPropertyName;
	
	/** Property to hold mOrderPropertyName. */
	private String mOrderPropertyName;
	
	/** Property to hold mPaymentStatusDescriptorName. */
	private String mPaymentStatusDescriptorName;
	
	/** Property to hold mPaymentStatusDescriptorName. */
	private String mAuthorizationStatusPropertyName;
	
	/** Property to hold mDebitStatusPropertyName. */
	private String mDebitStatusPropertyName;
	
	/** Property to hold mCreditStatusPropertyName. */
	private String mCreditStatusPropertyName;
	
	/** Property to hold mCreditCardStatusPropertyName. */
	private String mCreditCardStatusPropertyName;
	
	/** Property to hold mGiftCertificateStatusPropertyName. */
	private String mGiftCertificateStatusPropertyName;
	
	/** Property to hold mStoreCreditStatusPropertyName. */
	private String mStoreCreditStatusPropertyName;

	/** Property to hold mRetryCount. */
	private String mRetryCount;
	
	/** Property to hold mTransientRetryCount. */
	private String mTransientRetryCount;
	
	/** Property to hold mPaymentRetryCount. */
	private String mPaymentRetryCount;
	/** The Last profile update property name. */
	private String mLastProfileUpdatePropertyName;
	
	/** The Last radial user status property name. */
	private String mRadialUserPropertyName;
	
	/** The Last radial password property name. */
	private String mRadialPasswordPropertyName;
	
	/**Property name holder for to indicate whether the profile address is billing specific address or not */
	private String mIsBillingAddressPropertyName;
	
	/**Property name hold BillingAddressItemPropName */
	private String mBillingAddressItemPropName;
	
	/**Property name hold BillingAddressItemPropName */
	private String mOmsErrorMessagePropertyName;
	
	/**Property name hold EnablePriceFromATGPropertyName */
	private String mEnablePriceFromATGPropertyName;
	
	/**Property name hold RepositoryItemProperty */
	private String mRepositoryItemProperty;
	
	/**Property name hold BillingAddress */
	private String mBillingAddress;
	

	/** The Profile sync error messages property name. */
	private String mProfileSyncErrorMessagesPropertyName;
	
	
	/** The Profile id property name. */
	private String mProfileIdPropertyName;
	
	/** The Last name property name. */
	private String mLastNamePropertyName;
	
	/** The First name property name. */
	private String mFirstNamePropertyName;
	
	/** The Email property name. */
	private String mEmailPropertyName;
	

	/** The Registration date property name. */
	private String mRegistrationDatePropertyName;
	
	
	/** The Password property name. */
	private String mPasswordPropertyName;
	
	/** The Profile sync error message property name. */
	private String mProfileSyncErrorMessagePropertyName;
	
	/** The File name property name. */
	private String mFileNamePropertyName;
	
	/** The m shipping method property name. */
	private String mShippingMethodPropertyName;
	
	/**
	 * Gets the shipping method property name.
	 *
	 * @return the shipping method property name
	 */
	public String getShippingMethodPropertyName() {
		return mShippingMethodPropertyName;
	}
	
	/**
	 * Sets the shipping method property name.
	 *
	 * @param pShippingMethodPropertyName the new shipping method property name
	 */
	public void setShippingMethodPropertyName(String pShippingMethodPropertyName) {
		 mShippingMethodPropertyName = pShippingMethodPropertyName;
	}
	/**
	 * @return the repositoryItemProperty
	 */
	public String getRepositoryItemProperty() {
		return mRepositoryItemProperty;
	}
	/**
	 * @param pRepositoryItemProperty the repositoryItemProperty to set
	 */
	public void setRepositoryItemProperty(String pRepositoryItemProperty) {
		mRepositoryItemProperty = pRepositoryItemProperty;
	}
	/**
	 * @return the billingAddress
	 */
	public String getBillingAddress() {
		return mBillingAddress;
	}
	/**
	 * @param pBillingAddress the billingAddress to set
	 */
	public void setBillingAddress(String pBillingAddress) {
		mBillingAddress = pBillingAddress;
	}
	/**
	 * 
	 * @return mEnablePriceFromATGPropertyName
	 */
	public String getEnablePriceFromATGPropertyName() {
		return mEnablePriceFromATGPropertyName;
	}
	/**
	 * Sets the  enable price from atg property name.
	 *
	 * @param pEnablePriceFromATGPropertyName the new enable price from atg property name
	 */
	public void setEnablePriceFromATGPropertyName(String pEnablePriceFromATGPropertyName) {
		mEnablePriceFromATGPropertyName = pEnablePriceFromATGPropertyName;
	}

	/**
	 * 
	 * @return mOmsErrorMessagePropertyName
	 */
	public String getOmsErrorMessagePropertyName() {
		return mOmsErrorMessagePropertyName;
	}
	
	/**
	 * 
	 * @param pOmsErrorMessagePropertyName pOmsErrorMessagePropertyName
	 */
	public void setOmsErrorMessagePropertyName(String pOmsErrorMessagePropertyName) {
		this.mOmsErrorMessagePropertyName = pOmsErrorMessagePropertyName;
	}
	
	/**
	 * @return the mBillingAddressItemPropName
	 */
	public String getBillingAddressItemPropName() {
		return mBillingAddressItemPropName;
	}

	/**
	 * @param pBillingAddressItemPropName the pBillingAddressItemPropName to set
	 */
	public void setBillingAddressItemPropName(String pBillingAddressItemPropName) {
		this.mBillingAddressItemPropName = pBillingAddressItemPropName;
	}

	/**
	 * @return the paymentRetryCount
	 */
	public String getPaymentRetryCount() {
		return mPaymentRetryCount;
	}

	/**
	 * @param pPaymentRetryCount the paymentRetryCount to set
	 */
	public void setPaymentRetryCount(String pPaymentRetryCount) {
		mPaymentRetryCount = pPaymentRetryCount;
	}

	/**
	 * @return the transientRetryCount
	 */
	public String getTransientRetryCount() {
		return mTransientRetryCount;
	}

	/**
	 * @param pTransientRetryCount the transientRetryCount to set
	 */
	public void setTransientRetryCount(String pTransientRetryCount) {
		mTransientRetryCount = pTransientRetryCount;
	}

	/**
	 * @return the loyaltyCustomer
	 */
	public String getLoyaltyCustomer() {
		return mLoyaltyCustomer;
	}

	/**
	 * @param pLoyaltyCustomer the loyaltyCustomer to set
	 */
	public void setLoyaltyCustomer(String pLoyaltyCustomer) {
		mLoyaltyCustomer = pLoyaltyCustomer;
	}

	/**
	 * @return the retryCount
	 */
	public String getRetryCount() {
		return mRetryCount;
	}

	/**
	 * @param pRetryCount the retryCount to set
	 */
	public void setRetryCount(String pRetryCount) {
		mRetryCount = pRetryCount;
	}

	/**
	 * Gets the prev shipping address nick name property name.
	 *
	 * @return the prevShippingAddressNickNamePropertyName
	 */
	public String getPrevShippingAddressNickNamePropertyName() {
		return mPrevShippingAddressNickNamePropertyName;
	}

	/**
	 * Sets the prev shipping address nick name property name.
	 *
	 * @param pPrevShippingAddressNickNamePropertyName the prevShippingAddressNickNamePropertyName to set
	 */
	public void setPrevShippingAddressNickNamePropertyName(
			String pPrevShippingAddressNickNamePropertyName) {
		mPrevShippingAddressNickNamePropertyName = pPrevShippingAddressNickNamePropertyName;
	}

	/**
	 * Gets the reversal id property name.
	 *
	 * @return the reversalIdPropertyName
	 */
	public String getReversalIdPropertyName() {
		return mReversalIdPropertyName;
	}

	/**
	 * Sets the reversal id property name.
	 *
	 * @param pReversalIdPropertyName the reversalIdPropertyName to set
	 */
	public void setReversalIdPropertyName(String pReversalIdPropertyName) {
		mReversalIdPropertyName = pReversalIdPropertyName;
	}
	
	/**
	 * Gets the max gift card limit property name.
	 *
	 * @return the maxGiftCardLimitPropertyName
	 */
	public String getMaxGiftCardLimitPropertyName() {
		return mMaxGiftCardLimitPropertyName;
	}

	/**
	 * Sets the max gift card limit property name.
	 *
	 * @param pMaxGiftCardLimitPropertyName the maxGiftCardLimitPropertyName to set
	 */
	public void setMaxGiftCardLimitPropertyName(String pMaxGiftCardLimitPropertyName) {
		mMaxGiftCardLimitPropertyName = pMaxGiftCardLimitPropertyName;
	}

	/**
	 * Gets the reminder count.
	 *
	 * @return the reminder count
	 */
	public String getReminderCount() {
		return mReminderCount;
	}

	/**
	 * Sets the reminder count.
	 *
	 * @param pReminderCount the new reminder count
	 */
	public void setReminderCount(String pReminderCount) {
		mReminderCount = pReminderCount;
	}

	/**
	 * Gets the credit card token property name.
	 *
	 * @return the creditCardTokenPropertyName
	 */
	public String getCreditCardTokenPropertyName() {
		return mCreditCardTokenPropertyName;
	}

	/**
	 * Sets the credit card token property name.
	 *
	 * @param pCreditCardTokenPropertyName the creditCardTokenPropertyName to set
	 */
	public void setCreditCardTokenPropertyName(String pCreditCardTokenPropertyName) {
		mCreditCardTokenPropertyName = pCreditCardTokenPropertyName;
	}

	/**
	 * Gets the enable persist order property name.
	 *
	 * @return the enable persist order property name.
	 */
	public String getEnablePersistOrderPropertyName() {
		return mEnablePersistOrderPropertyName;
	}

	/**
	 * Sets the enable persist order property name.
	 *
	 * @param pEnablePersistOrderPropertyName the new enable persist order property name.
	 */
	public void setEnablePersistOrderPropertyName(String pEnablePersistOrderPropertyName) {
		mEnablePersistOrderPropertyName = pEnablePersistOrderPropertyName;
	}

	/**
	 * Gets the credit card verification number property name.
	 *
	 * @return the creditCardVerificationNumberPropertyName.
	 */
	
	public String getCreditCardVerificationNumberPropertyName() {
		return mCreditCardVerificationNumberPropertyName;
	}

	/**
	 * Sets the credit card verification number property name.
	 *
	 * @param pCreditCardVerificationNumberPropertyName the creditCardVerificationNumberPropertyName to set.
	 */
	public void setCreditCardVerificationNumberPropertyName(
			String pCreditCardVerificationNumberPropertyName) {
		mCreditCardVerificationNumberPropertyName = pCreditCardVerificationNumberPropertyName;
	}

	/**
	 * getter method for ConfirmPasswordPropertyName.
	 * @return mConfirmPasswordPropertyName
	 */
	public String getConfirmPasswordPropertyName() {
		return mConfirmPasswordPropertyName;
	}
	
	/**
	 * setter method for ConfirmPasswordPropertyName.
	 * @param pConfirmPasswordPropertyName ConfirmPasswordPropertyName
	 */
	public void setConfirmPasswordPropertyName(
			String pConfirmPasswordPropertyName) {
		this.mConfirmPasswordPropertyName = pConfirmPasswordPropertyName;
	}
	
	/**
	 * getter method for RewardNumberPropertyName.
	 * @return mRewardNumberPropertyName
	 */
	public String getRewardNumberPropertyName() {
		return mRewardNumberPropertyName;
	}

	/**
	 * setter method for RewardNumberPropertyName.
	 * @param pRewardNumberPropertyName RewardNumberPropertyName
	 */
	public void setRewardNumberPropertyName(String pRewardNumberPropertyName) {
		this.mRewardNumberPropertyName = pRewardNumberPropertyName;
	}

	/**
	 * getter method for NicknamePropertyName.
	 * @return mNicknamePropertyName
	 */
	public String getNicknamePropertyName() {
		return mNicknamePropertyName;
	}

	/**
	 * setter method for NicknamePropertyName.
	 * @param pNicknamePropertyName NicknamePropertyName
	 */
	public void setNicknamePropertyName(String pNicknamePropertyName) {
		this.mNicknamePropertyName = pNicknamePropertyName;
	}
	
	/**
	 * getter method for ConfirmpasswordPropertyName.
	 * @return mConfirmpasswordPropertyName
	 */
	
	public String getConfirmpasswordPropertyName() {
		return mConfirmpasswordPropertyName;
	}

	/**
	 * setter method for ConfirmpasswordPropertyName.
	 * @param pConfirmpasswordPropertyName ConfirmpasswordPropertyName
	 */
	
	public void setConfirmpasswordPropertyName(String pConfirmpasswordPropertyName) {
		this.mConfirmpasswordPropertyName = pConfirmpasswordPropertyName;
	}
	
	/**
	 * getter method for OldpasswordPropertyName.
	 * @return mOldpasswordPropertyName
	 */
	
	public String getOldpasswordPropertyName() {
		return mOldpasswordPropertyName;
	}

	/**
	 * setter method for OldpasswordPropertyName.
	 * @param pOldpasswordPropertyName OldpasswordPropertyName
	 */
	
	public void setOldpasswordPropertyName(String pOldpasswordPropertyName) {
		this.mOldpasswordPropertyName = pOldpasswordPropertyName;
	}

	/**
	 * Gets the name on card property name.
	 *
	 * @return the mNameOnCardPropertyName.
	 */
	public String getNameOnCardPropertyName() {
		return mNameOnCardPropertyName;
	}

	/**
	 * Sets the name on card property name.
	 *
	 * @param pNameOnCardPropertyName the mNameOnCardPropertyName to set.
	 */
	public void setNameOnCardPropertyName(String pNameOnCardPropertyName) {
		this.mNameOnCardPropertyName = pNameOnCardPropertyName;
	}

	/**
	 * Gets the upper range property name.
	 *
	 * @return the upperRangePropertyName.
	 */
	public String getUpperRangePropertyName() {
		return mUpperRangePropertyName;
	}

	/**
	 * Gets the gift finder url.
	 *
	 * @return the giftFinderURL
	 */
	public String getGiftFinderURL() {
		return mGiftFinderURL;
	}

	/**
	 * Sets the gift finder url.
	 *
	 * @param pGiftFinderURL the giftFinderURL to set
	 */
	public void setGiftFinderURL(String pGiftFinderURL) {
		mGiftFinderURL = pGiftFinderURL;
	}

	/**
	 * Sets the upper range property name.
	 *
	 * @param pUpperRangePropertyName the upperRangePropertyName to set.
	 */
	public void setUpperRangePropertyName(String pUpperRangePropertyName) {
		mUpperRangePropertyName = pUpperRangePropertyName;
	}

	/**
	 * Gets the lower range property name.
	 *
	 * @return the lowerRangePropertyName.
	 */
	public String getLowerRangePropertyName() {
		return mLowerRangePropertyName;
	}

	/**
	 * Sets the lower range property name.
	 *
	 * @param pLowerRangePropertyName the lowerRangePropertyName to set.
	 */
	public void setLowerRangePropertyName(String pLowerRangePropertyName) {
		mLowerRangePropertyName = pLowerRangePropertyName;
	}

	/**
	 * Gets the credit card type property name.
	 *
	 * @return the creditCardTypePropertyName.
	 */
	public String getCreditCardTypePropertyName() {
		return mCreditCardTypePropertyName;
	}

	/**
	 * Sets the credit card type property name.
	 *
	 * @param pCreditCardTypePropertyName the creditCardTypePropertyName to set.
	 */
	public void setCreditCardTypePropertyName(String pCreditCardTypePropertyName) {
		mCreditCardTypePropertyName = pCreditCardTypePropertyName;
	}

	/**
	 * Gets the recently viewed property name.
	 *
	 * @return the mRecentlyViewedPropertyName
	 */
	public String getRecentlyViewedPropertyName() {
		return mRecentlyViewedPropertyName;
	}

	/**
	 * Sets the recently viewed property name.
	 *
	 * @param pRecentlyViewedPropertyName the recentlyViewedPropertyName to set
	 */
	public void setRecentlyViewedPropertyName(String pRecentlyViewedPropertyName) {
		mRecentlyViewedPropertyName = pRecentlyViewedPropertyName;
	}
	/**
	 * Gets the color upc level.
	 *
	 * @return the color upc level
	 */
	public String getColorUpcLevel() {
		return mColorUpcLevel;
	}

	/**
	 * Sets the color upc level.
	 *
	 * @param pColorUpcLevel the new color upc level
	 */
	public void setColorUpcLevel(String pColorUpcLevel) {
		this.mColorUpcLevel = pColorUpcLevel;
	}

	/**
	 * Gets the juvenile product size.
	 *
	 * @return the juvenile product size
	 */
	public String getJuvenileProductSize() {
		return mJuvenileProductSize;
	}

	/**
	 * Sets the juvenile product size.
	 *
	 * @param pJuvenileProductSize the new juvenile product size
	 */
	public void setJuvenileProductSize(String pJuvenileProductSize) {
		this.mJuvenileProductSize = pJuvenileProductSize;
	}

	/**
	 * gets the loyalty customer property.
	 * @return loyalty customer
	 */
	public String getloyaltyCustomer() {
		return mLoyaltyCustomer;
	}

	/**
	 * sets the loyalty customer property.
	 * @param pLoyaltyCustomer loyalty customer
	 */
	public void setloyaltyCustomer(String pLoyaltyCustomer) {
		this.mLoyaltyCustomer = pLoyaltyCustomer;
	}

	/**
	 * Gets the free shipping promotion property name.
	 *
	 * @return the freeShippingPromotionPropertyName
	 */
	public String getFreeShippingPromotionPropertyName() {
		return mFreeShippingPromotionPropertyName;
	}

	/**
	 * Sets the free shipping promotion property name.
	 *
	 * @param pFreeShippingPromotionPropertyName the freeShippingPromotionPropertyName to set
	 */
	public void setFreeShippingPromotionPropertyName(
			String pFreeShippingPromotionPropertyName) {
		mFreeShippingPromotionPropertyName = pFreeShippingPromotionPropertyName;
	}

	/**
	 * Gets the order limit property name.
	 *
	 * @return the orderLimitPropertyName
	 */
	public String getOrderLimitPropertyName() {
		return mOrderLimitPropertyName;
	}

	/**
	 * Sets the order limit property name.
	 *
	 * @param pOrderLimitPropertyName the orderLimitPropertyName to set
	 */
	public void setOrderLimitPropertyName(String pOrderLimitPropertyName) {
		mOrderLimitPropertyName = pOrderLimitPropertyName;
	}

	/**
	 * gets the max coupon limit property.
	 *
	 * @return loyalty customer
	 */
	
	public String getMaxCouponLimitPropertyName() {
		return mMaxCouponLimitPropertyName;
	}
	
	/**
	 * sets the max coupon limit property.
	 *
	 * @param pMaxCouponLimitPropertyName the new max coupon limit property name
	 */
	
	public void setMaxCouponLimitPropertyName(String pMaxCouponLimitPropertyName) {
		this.mMaxCouponLimitPropertyName = pMaxCouponLimitPropertyName;
	}	
	
	/**
	 * Gets the gift wrap comm item id.
	 *
	 * @return the gift wrap comm item id
	 */
	public String getGiftWrapCommItemId() {
		return mGiftWrapCommItemId;
	}

	/**
	 * Sets the gift wrap comm item id.
	 *
	 * @param pGiftWrapCommItemId the new gift wrap comm item id
	 */
	public void setGiftWrapCommItemId(String pGiftWrapCommItemId) {
		this.mGiftWrapCommItemId = pGiftWrapCommItemId;
	}

	/**
	 * Gets the gift wrap quantity.
	 *
	 * @return the gift wrap quantity
	 */
	public String getGiftWrapQuantity() {
		return mGiftWrapQuantity;
	}

	/**
	 * Sets the gift wrap quantity.
	 *
	 * @param pGiftWrapQuantity the new gift wrap quantity
	 */
	public void setGiftWrapQuantity(String pGiftWrapQuantity) {
		this.mGiftWrapQuantity = pGiftWrapQuantity;
	}

	/**
	 * Gets the gift item descriptor name.
	 *
	 * @return the gift item descriptor name
	 */
	public String getGiftItemDescriptorName() {
		return mGiftItemDescriptorName;
	}

	/**
	 * Sets the gift item descriptor name.
	 *
	 * @param pGiftItemDescriptorName the new gift item descriptor name
	 */
	public void setGiftItemDescriptorName(String pGiftItemDescriptorName) {
		this.mGiftItemDescriptorName = pGiftItemDescriptorName;
	}

	/**
	 * Gets the abandon cart threshold.
	 *
	 * @return the abandon cart threshold
	 */
	public String getAbandonCartThreshold() {
		return mAbandonCartThreshold;
	}

	/**
	 * Sets the abandon cart threshold.
	 *
	 * @param pAbandonCartThreshold the new abandon cart threshold
	 */
	public void setAbandonCartThreshold(String pAbandonCartThreshold) {
		this.mAbandonCartThreshold = pAbandonCartThreshold;
	}	
	

	
		/**
		 * Gets the nmwa product id property name.
		 *
		 * @return the nmwaProductIdPropertyName
		 */
	public String getNmwaProductIdPropertyName() {
		return mNmwaProductIdPropertyName;
	}

	/**
	 * Sets the nmwa product id property name.
	 *
	 * @param pNmwaProductIdPropertyName the nmwaProductIdPropertyName to set
	 */
	public void setNmwaProductIdPropertyName(String pNmwaProductIdPropertyName) {
		mNmwaProductIdPropertyName = pNmwaProductIdPropertyName;
	}

	/**
	 * Gets the nmwa sku id property name.
	 *
	 * @return the nmwaSkuIdPropertyName
	 */
	public String getNmwaSkuIdPropertyName() {
		return mNmwaSkuIdPropertyName;
	}

	/**
	 * Sets the nmwa sku id property name.
	 *
	 * @param pNmwaSkuIdPropertyName the nmwaSkuIdPropertyName to set
	 */
	public void setNmwaSkuIdPropertyName(String pNmwaSkuIdPropertyName) {
		mNmwaSkuIdPropertyName = pNmwaSkuIdPropertyName;
	}

	/**
	 * Gets the nmwa req date property name.
	 *
	 * @return the nmwaReqDatePropertyName
	 */
	public String getNmwaReqDatePropertyName() {
		return mNmwaReqDatePropertyName;
	}

	/**
	 * Sets the nmwa req date property name.
	 *
	 * @param pNmwaReqDatePropertyName the nmwaReqDatePropertyName to set
	 */
	public void setNmwaReqDatePropertyName(String pNmwaReqDatePropertyName) {
		mNmwaReqDatePropertyName = pNmwaReqDatePropertyName;
	}

	/**
	 * Gets the nmwa email sent date property name.
	 *
	 * @return the nmwaEmailSentDatePropertyName
	 */
	public String getNmwaEmailSentDatePropertyName() {
		return mNmwaEmailSentDatePropertyName;
	}

	/**
	 * Sets the nmwa email sent date property name.
	 *
	 * @param pNmwaEmailSentDatePropertyName the nmwaEmailSentDatePropertyName to set
	 */
	public void setNmwaEmailSentDatePropertyName(
			String pNmwaEmailSentDatePropertyName) {
		mNmwaEmailSentDatePropertyName = pNmwaEmailSentDatePropertyName;
	}
	

	/**
	 * Gets the nmwa email address property name.
	 *
	 * @return the nmwaEmailAddressPropertyName
	 */
	public String getNmwaEmailAddressPropertyName() {
		return mNmwaEmailAddressPropertyName;
	}

	/**
	 * Sets the nmwa email address property name.
	 *
	 * @param pNmwaEmailAddressPropertyName the nmwaEmailAddressPropertyName to set
	 */
	public void setNmwaEmailAddressPropertyName(String pNmwaEmailAddressPropertyName) {
		mNmwaEmailAddressPropertyName = pNmwaEmailAddressPropertyName;
	}
    
	/**
	 * Gets the nmwa id property name.
	 *
	 * @return the nmwaIdPropertyName
	 */
	public String getNmwaIdPropertyName() {
		return mNmwaIdPropertyName;
	}

	/**
	 * Sets the nmwa id property name.
	 *
	 * @param pNmwaIdPropertyName the nmwaIdPropertyName to set
	 */
	public void setNmwaIdPropertyName(String pNmwaIdPropertyName) {
		mNmwaIdPropertyName = pNmwaIdPropertyName;
	}
	
	/**
	 * Gets the nmwa item desc property name.
	 *
	 * @return the nmwaItemDescPropertyName
	 */
	public String getNmwaItemDescPropertyName() {
		return mNmwaItemDescPropertyName;
	}

	/**
	 * Sets the nmwa item desc property name.
	 *
	 * @param pNmwaItemDescPropertyName the nmwaItemDescPropertyName to set
	 */
	public void setNmwaItemDescPropertyName(String pNmwaItemDescPropertyName) {
		mNmwaItemDescPropertyName = pNmwaItemDescPropertyName;
	}

	/**
	 * Gets the persist order days.
	 *
	 * @return the persist order days
	 */
	public String getPersistOrderDays() {
		return mPersistOrderDays;
	}

	/**
	 * Sets the persist order days.
	 *
	 * @param pPersistOrderDays the new persist order days
	 */
	public void setPersistOrderDays(String pPersistOrderDays) {
		this.mPersistOrderDays = pPersistOrderDays;
	}
	/**
	 * Gets the enableAddressDoctorPropertyName.
	 *
	 * @return the enableAddressDoctorPropertyName
	 */
	public String getEnableAddressDoctorPropertyName() {
		return mEnableAddressDoctorPropertyName;
	}

	/**
	 * Sets the enableAddressDoctorPropertyName.
	 *
	 * @param pEnableAddressDoctorPropertyName the enableAddressDoctorPropertyName
	 */
	public void setEnableAddressDoctorPropertyName(
			String pEnableAddressDoctorPropertyName) {
		mEnableAddressDoctorPropertyName = pEnableAddressDoctorPropertyName;
	}
	
	/**
	 * Gets the nmwa email sent flag.
	 *
	 * @return mNmwaEmailSentFlag
	 */
	public String getNmwaEmailSentFlag() {
		return mNmwaEmailSentFlag;
	 }
    
    /**
     * *.
     *
     * @param pNmwaEmailSentFlag the nmwaEmailSentFlag
     */
	public void setNmwaEmailSentFlag(String pNmwaEmailSentFlag) {
		this.mNmwaEmailSentFlag = pNmwaEmailSentFlag;
	}
	
	/**
	 * Gets the source of order property name.
	 *
	 * @return the sourceOfOrderPropertyName
	 */
	public String getSourceOfOrderPropertyName() {
		return mSourceOfOrderPropertyName;
	}

	/**
	 * Sets the source of order property name.
	 *
	 * @param pSourceOfOrderPropertyName the sourceOfOrderPropertyName to set
	 */
	public void setSourceOfOrderPropertyName(String pSourceOfOrderPropertyName) {
		mSourceOfOrderPropertyName = pSourceOfOrderPropertyName;
	}

	/**
	 * Gets the eci flag property name.
	 *
	 * @return the eciFlagPropertyName
	 */
	public String getEciFlagPropertyName() {
		return mEciFlagPropertyName;
	}

	/**
	 * Sets the eci flag property name.
	 *
	 * @param pEciFlagPropertyName the eciFlagPropertyName to set
	 */
	public void setEciFlagPropertyName(String pEciFlagPropertyName) {
		mEciFlagPropertyName = pEciFlagPropertyName;
	}

	/**
	 * Gets the cavv property name.
	 *
	 * @return the cavvPropertyName
	 */
	public String getCavvPropertyName() {
		return mCavvPropertyName;
	}

	/**
	 * Sets the cavv property name.
	 *
	 * @param pCavvPropertyName the cavvPropertyName to set
	 */
	public void setCavvPropertyName(String pCavvPropertyName) {
		mCavvPropertyName = pCavvPropertyName;
	}

	/**
	 * Gets the xid property name.
	 *
	 * @return the xidPropertyName
	 */
	public String getXidPropertyName() {
		return mXidPropertyName;
	}

	/**
	 * Sets the xid property name.
	 *
	 * @param pXidPropertyName the xidPropertyName to set
	 */
	public void setXidPropertyName(String pXidPropertyName) {
		mXidPropertyName = pXidPropertyName;
	}

	/**
	 * Gets the site status property name.
	 *
	 * @return the siteStatusPropertyName
	 */
	public String getSiteStatusPropertyName() {
		return mSiteStatusPropertyName;
	}

	/**
	 * Sets the site status property name.
	 *
	 * @param pSiteStatusPropertyName the siteStatusPropertyName to set
	 */
	public void setSiteStatusPropertyName(String pSiteStatusPropertyName) {
		mSiteStatusPropertyName = pSiteStatusPropertyName;
	}

	/**
	 * Gets the type property name.
	 *
	 * @return the mTypePropertyName
	 */
	public String getTypePropertyName() {
		return mTypePropertyName;
	}

	/**
	 * Sets the type property name.
	 *
	 * @param pTypePropertyName the mTypePropertyName to set
	 */
	public void setTypePropertyName(String pTypePropertyName) {
		this.mTypePropertyName = pTypePropertyName;
	}

	/**
	 * Gets the message property name.
	 *
	 * @return the mMessagePropertyName
	 */
	public String getMessagePropertyName() {
		return mMessagePropertyName;
	}

	/**
	 * Sets the message property name.
	 *
	 * @param pMessagePropertyName the mMessagePropertyName to set
	 */
	public void setMessagePropertyName(String pMessagePropertyName) {
		this.mMessagePropertyName = pMessagePropertyName;
	}

	/**
	 * Gets the oms error message item name.
	 *
	 * @return the mOmsErrorMessageItemName
	 */
	public String getOmsErrorMessageItemName() {
		return mOmsErrorMessageItemName;
	}

	/**
	 * Sets the oms error message item name.
	 *
	 * @param pOmsErrorMessageItemName the mOmsErrorMessageItemName to set
	 */
	public void setOmsErrorMessageItemName(String pOmsErrorMessageItemName) {
		this.mOmsErrorMessageItemName = pOmsErrorMessageItemName;
	}

	/**
	 * Gets the user item name.
	 *
	 * @return the mUserItemName
	 */
	public String getUserItemName() {
		return mUserItemName;
	}

	/**
	 * Sets the user item name.
	 *
	 * @param pUserItemName the mUserItemName to set
	 */
	public void setUserItemName(String pUserItemName) {
		this.mUserItemName = pUserItemName;
	}

	/**
	 * Gets the transaction time property name.
	 *
	 * @return the mTransactionTimePropertyName
	 */
	public String getTransactionTimePropertyName() {
		return mTransactionTimePropertyName;
	}

	/**
	 * Sets the transaction time property name.
	 *
	 * @param pTransactionTimePropertyName the mTransactionTimePropertyName to set
	 */
	public void setTransactionTimePropertyName(String pTransactionTimePropertyName) {
		this.mTransactionTimePropertyName = pTransactionTimePropertyName;
	}

	/**
	 * Gets the previous shipping address property name.
	 *
	 * @return the previous shipping address property name
	 */
	public String getPreviousShippingAddressPropertyName() {
		return mPreviousShippingAddressPropertyName;
	}

	/**
	 * Sets the previous shipping address property name.
	 *
	 * @param pPreviousShippingAddressPropertyName the mPreviousShippingAddressPropertyName to set
	 */
	public void setPreviousShippingAddressPropertyName(
			String pPreviousShippingAddressPropertyName) {
		this.mPreviousShippingAddressPropertyName = pPreviousShippingAddressPropertyName;
	}

	/**
	 * Gets the selected cc nick name property name.
	 *
	 * @return the selected cc nick name property name
	 */
	public String getSelectedCCNickNamePropertyName() {
		return mSelectedCCNickNamePropertyName;
	}

	/**
	 * Sets the selected cc nick name property name.
	 *
	 * @param pSelectedCCNickNamePropertyName the new selected cc nick name property name
	 */
	public void setSelectedCCNickNamePropertyName(String pSelectedCCNickNamePropertyName) {
		mSelectedCCNickNamePropertyName = pSelectedCCNickNamePropertyName;
	}

	/**
	 * Gets the word.
	 *
	 * @return the mWord
	 */
	public String getWord() {
		return mWord;
	}

	/**
	 * Sets the word.
	 *
	 * @param pWord the new word
	 */
	public void setWord(String pWord) {
		this.mWord = pWord;
	}

	/**
	 * Gets the recently viewed date.
	 *
	 * @return the mRecentlyViewedDate
	 */
	public String getRecentlyViewedDate() {
		return mRecentlyViewedDate;
	}

	/**
	 * Sets the recently viewed date.
	 *
	 * @param pRecentlyViewedDate the new recently viewed date
	 */
	public void setRecentlyViewedDate(String pRecentlyViewedDate) {
		this.mRecentlyViewedDate = pRecentlyViewedDate;
	}

	/**
	 * Gets the recently viewed threshold.
	 *
	 * @return the mRecentlyViewedThreshold
	 */
	public String getRecentlyViewedThreshold() {
		return mRecentlyViewedThreshold;
	}

	/**
	 * Sets the recently viewed threshold.
	 *
	 * @param pRecentlyViewedThreshold the new recently viewed threshold
	 */
	public void setRecentlyViewedThreshold(String pRecentlyViewedThreshold) {
		this.mRecentlyViewedThreshold = pRecentlyViewedThreshold;
	}

	/** Proerty to hold the previous shipping address for guest user. */
	private String mAnonymousPrevDefaultNickNamePropertyName;
	
	/**
	 * Gets the anonymous prev default nick name property name.
	 *
	 * @return the anonymous prev default nick name property name
	 */
	public String getAnonymousPrevDefaultNickNamePropertyName() {
		return mAnonymousPrevDefaultNickNamePropertyName;
	}

	/**
	 * Sets the anonymous prev default nick name property name.
	 *
	 * @param pAnonymousPrevDefaultNickNamePropertyName the new anonymous prev default nick name property name
	 */
	public void setAnonymousPrevDefaultNickNamePropertyName(String pAnonymousPrevDefaultNickNamePropertyName) {
		this.mAnonymousPrevDefaultNickNamePropertyName = pAnonymousPrevDefaultNickNamePropertyName;
	}
	
	/**
	 * Gets the item in store pick up.
	 *
	 * @return the item in store pick up
	 */
	public String getItemInStorePickUp() {
		return mItemInStorePickUp;
	}

	/**
	 * Sets the item in store pick up.
	 *
	 * @param pItemInStorePickUp the new item in store pick up
	 */
	public void setItemInStorePickUp(String pItemInStorePickUp) {
		this.mItemInStorePickUp = pItemInStorePickUp;
	}

	/**
	 * Gets the migrated profile.
	 *
	 * @return the mMigratedProfile
	 */
	public String getMigratedProfile() {
		return mMigratedProfile;
	}

	/**
	 * Sets the migrated profile.
	 *
	 * @param pMigratedProfile the MigratedProfile to set
	 */
	public void setMigratedProfile(String pMigratedProfile) {
		this.mMigratedProfile = pMigratedProfile;
	}

	/**
	 * Gets the migrated profile reset pwd.
	 *
	 * @return the mMigratedProfileResetPwd
	 */
	public String getMigratedProfileResetPwd() {
		return mMigratedProfileResetPwd;
	}

	/**
	 * Sets the migrated profile reset pwd.
	 *
	 * @param pMigratedProfileResetPwd the MigratedProfileResetPwd to set
	 */
	public void setMigratedProfileResetPwd(String pMigratedProfileResetPwd) {
		this.mMigratedProfileResetPwd = pMigratedProfileResetPwd;
	}
	
	/**
	 * Gets the street date.
	 *
	 * @return the streetDate
	 */
	public String getStreetDate() {
		return mStreetDate;
	}

	/**
	 * Sets the street date.
	 *
	 * @param pStreetDate the streetDate to set
	 */
	public void setStreetDate(String pStreetDate) {
		mStreetDate = pStreetDate;
	}

	/**
	 * @return the mLayawayItemDescriptorPropertyName
	 */
	public String getLayawayItemDescriptorPropertyName() {
		return mLayawayItemDescriptorPropertyName;
	}

	/**
	 * @param pLayawayItemDescriptorPropertyName the mLayawayItemDescriptorPropertyName to set
	 */
	public void setLayawayItemDescriptorPropertyName(
			String pLayawayItemDescriptorPropertyName) {
		this.mLayawayItemDescriptorPropertyName = pLayawayItemDescriptorPropertyName;
	}

	/**
	 * @return the mShipToStoreEligible
	 */
	public String getShipToStoreEligible() {
		return mShipToStoreEligible;
	}

	/**
	 * @param pShipToStoreEligible the pShipToStoreEligible to set
	 */
	public void setShipToStoreEligible(String pShipToStoreEligible) {
		this.mShipToStoreEligible = pShipToStoreEligible;
	}

	/**
	 * @return the mErrorKeyPropertyName
	 */
	public String getErrorKeyPropertyName() {
		return mErrorKeyPropertyName;
	}

	/**
	 * @param pErrorKeyPropertyName the mErrorKeyPropertyName to set
	 */
	public void setErrorKeyPropertyName(String pErrorKeyPropertyName) {
		this.mErrorKeyPropertyName = pErrorKeyPropertyName;
	}

	/**
	 * @return the mDefaultErrorMessagePropertyName
	 */
	public String getDefaultErrorMessagePropertyName() {
		return mDefaultErrorMessagePropertyName;
	}

	/**
	 * @param pDefaultErrorMessagePropertyName the mDefaultErrorMessagePropertyName to set
	 */
	public void setDefaultErrorMessagePropertyName(
			String pDefaultErrorMessagePropertyName) {
		this.mDefaultErrorMessagePropertyName = pDefaultErrorMessagePropertyName;
	}

	/**
	 * @return the mOwnerIdPropertyName
	 */
	public String getOwnerIdPropertyName() {
		return mOwnerIdPropertyName;
	}

	/**
	 * @param pOwnerIdPropertyName the mOwnerIdPropertyName to set
	 */
	public void setOwnerIdPropertyName(String pOwnerIdPropertyName) {
		this.mOwnerIdPropertyName = pOwnerIdPropertyName;
	}

	/**
	 * @return the mLastActivityPropertyName
	 */
	public String getLastActivityPropertyName() {
		return mLastActivityPropertyName;
	}

	/**
	 * @param pLastActivityPropertyName the mLastActivityPropertyName to set
	 */
	public void setLastActivityPropertyName(String pLastActivityPropertyName) {
		this.mLastActivityPropertyName = pLastActivityPropertyName;
	}

	/**
	 * @return the mOrderPropertyName
	 */
	public String getOrderPropertyName() {
		return mOrderPropertyName;
	}

	/**
	 * @param pOrderPropertyName the mOrderPropertyName to set
	 */
	public void setOrderPropertyName(String pOrderPropertyName) {
		this.mOrderPropertyName = pOrderPropertyName;
	}

	/**
	 * @return the mPaymentStatusDescriptorName
	 */
	public String getPaymentStatusDescriptorName() {
		return mPaymentStatusDescriptorName;
	}

	/**
	 * @param pPaymentStatusDescriptorName the mPaymentStatusDescriptorName to set
	 */
	public void setPaymentStatusDescriptorName(String pPaymentStatusDescriptorName) {
		this.mPaymentStatusDescriptorName = pPaymentStatusDescriptorName;
	}

	/**
	 * @return the mAuthorizationStatusPropertyName
	 */
	public String getAuthorizationStatusPropertyName() {
		return mAuthorizationStatusPropertyName;
	}

	/**
	 * @param pAuthorizationStatusPropertyName the mAuthorizationStatusPropertyName to set
	 */
	public void setAuthorizationStatusPropertyName(String pAuthorizationStatusPropertyName) {
		this.mAuthorizationStatusPropertyName = pAuthorizationStatusPropertyName;
	}

	/**
	 * @return the mDebitStatusPropertyName
	 */
	public String getDebitStatusPropertyName() {
		return mDebitStatusPropertyName;
	}

	/**
	 * @param pDebitStatusPropertyName the mDebitStatusPropertyName to set
	 */
	public void setDebitStatusPropertyName(String pDebitStatusPropertyName) {
		this.mDebitStatusPropertyName = pDebitStatusPropertyName;
	}

	/**
	 * @return the mCreditStatusPropertyName
	 */
	public String getCreditStatusPropertyName() {
		return mCreditStatusPropertyName;
	}

	/**
	 * @param pCreditStatusPropertyName the mCreditStatusPropertyName to set
	 */
	public void setCreditStatusPropertyName(String pCreditStatusPropertyName) {
		this.mCreditStatusPropertyName = pCreditStatusPropertyName;
	}

	/**
	 * @return the mCreditCardStatusPropertyName
	 */
	public String getCreditCardStatusPropertyName() {
		return mCreditCardStatusPropertyName;
	}

	/**
	 * @param pCreditCardStatusPropertyName the mCreditCardStatusPropertyName to set
	 */
	public void setCreditCardStatusPropertyName(String pCreditCardStatusPropertyName) {
		this.mCreditCardStatusPropertyName = pCreditCardStatusPropertyName;
	}

	/**
	 * @return the mGiftCertificateStatusPropertyName
	 */
	public String getGiftCertificateStatusPropertyName() {
		return mGiftCertificateStatusPropertyName;
	}

	/**
	 * @param pGiftCertificateStatusPropertyName the mGiftCertificateStatusPropertyName to set
	 */
	public void setGiftCertificateStatusPropertyName(String pGiftCertificateStatusPropertyName) {
		this.mGiftCertificateStatusPropertyName = pGiftCertificateStatusPropertyName;
	}

	/**
	 * @return the mStoreCreditStatusPropertyName
	 */
	public String getStoreCreditStatusPropertyName() {
		return mStoreCreditStatusPropertyName;
	}

	/**
	 * @param pStoreCreditStatusPropertyName the mStoreCreditStatusPropertyName to set
	 */
	public void setStoreCreditStatusPropertyName(String pStoreCreditStatusPropertyName) {
		this.mStoreCreditStatusPropertyName = pStoreCreditStatusPropertyName;
	}
	
	/** The billing address nick name. */
	private String mBillingAddressNickName;

	/**
	 * Gets the billing address nick name.
	 *
	 * @return the billing address nick name
	 */
	public String getBillingAddressNickName() {
		return mBillingAddressNickName;
	}

	/**
	 * Sets the billing address nick name.
	 *
	 * @param pBillingAddressNickName the new billing address nick name
	 */
	public void setBillingAddressNickName(String pBillingAddressNickName) {
		this.mBillingAddressNickName = pBillingAddressNickName;
	}
	
	/** The Time spent on site. */
	private String mTimeSpentOnSite;

	/**
	 * @return the timeSpentOnSite
	 */
	public String getTimeSpentOnSite() {
		return mTimeSpentOnSite;
	}

	/**
	 * @param pTimeSpentOnSite the timeSpentOnSite to set
	 */
	public void setTimeSpentOnSite(String pTimeSpentOnSite) {
		mTimeSpentOnSite = pTimeSpentOnSite;
	}
	
	/** The m display name property name. */
	private String mDisplayNamePropertyName ;

	/**
	 * Gets the display name property name.
	 *
	 * @return the display name property name
	 */
	public String getDisplayNamePropertyName() {
		return mDisplayNamePropertyName;
	}

	/**
	 * Sets the display name property name.
	 *
	 * @param pDisplayNamePropertyName the new display name property name
	 */
	public void setDisplayNamePropertyName(String pDisplayNamePropertyName) {
		this.mDisplayNamePropertyName = pDisplayNamePropertyName;
	}
	/**
	 * Gets the last profile update property name.
	 *
	 * @return the last profile update property name
	 */
	public String getLastProfileUpdatePropertyName() {
		return mLastProfileUpdatePropertyName;
	}

	/**
	 * Sets the last profile update property name.
	 *
	 * @param pLastProfileUpdatePropertyName the new last profile update property name
	 */
	public void setLastProfileUpdatePropertyName(
			String pLastProfileUpdatePropertyName) {
		mLastProfileUpdatePropertyName = pLastProfileUpdatePropertyName;
	}
	
	/**
	 * Gets the radial user property name.
	 *
	 * @return the radial user property name
	 */
	public String getRadialUserPropertyName() {
		return mRadialUserPropertyName;
	}
	
	/**
	 * Sets the radial user property name.
	 *
	 * @param pRadialUserPropertyName the new radial user property name
	 */
	public void setRadialUserPropertyName(String pRadialUserPropertyName) {
		this.mRadialUserPropertyName = pRadialUserPropertyName;
	}
	/**
	 * Gets the radial password property name.
	 *
	 * @return the getRadialPasswordPropertyName
	 */
	public String getRadialPasswordPropertyName() {
		return mRadialPasswordPropertyName;
	}
	/**
	 * @param pRadialPasswordPropertyName the mRadialPasswordPropertyName to set
	 */
	public void setRadialPasswordPropertyName(String pRadialPasswordPropertyName) {
		this.mRadialPasswordPropertyName = pRadialPasswordPropertyName;
	}



	/**
	 * Gets the checks if is billing address property name.
	 *
	 * @return the checks if is billing address property name
	 */
	public String getIsBillingAddressPropertyName() {
		return mIsBillingAddressPropertyName;
	}

	/**
	 * Sets the checks if is billing address property name.
	 *
	 * @param pIsBillingAddressPropertyName the new checks if is billing address property name
	 */
	public void setIsBillingAddressPropertyName(String pIsBillingAddressPropertyName) {
		this.mIsBillingAddressPropertyName = pIsBillingAddressPropertyName;
	}
	
	/** The Phone number property name. */
	private String mPhoneNumberPropertyName;

	/**
	 * @return the phoneNumberPropertyName
	 */
	public String getPhoneNumberPropertyName() {
		return mPhoneNumberPropertyName;
	}

	/**
	 * @param pPhoneNumberPropertyName the phoneNumberPropertyName to set
	 */
	public void setPhoneNumberPropertyName(String pPhoneNumberPropertyName) {
		mPhoneNumberPropertyName = pPhoneNumberPropertyName;
	}

	/** Property to hold mSucnCodePropertyName. */
	private String mSucnCodePropertyName;

	/** Property to hold mAppliedCode. */
	private String mAppliedCodePropertyName;

	/** Property to hold mAppliedCodeValue. */
	private String mAppliedCodeValuePropertyName;

	/**
	 * @return the sucnCodePropertyName
	 */
	public String getSucnCodePropertyName() {
		return mSucnCodePropertyName;
	}

	/**
	 * @param pSucnCodePropertyName the sucnCodePropertyName to set
	 */
	public void setSucnCodePropertyName(String pSucnCodePropertyName) {
		mSucnCodePropertyName = pSucnCodePropertyName;
	}

	/**
	 * @return the appliedCodePropertyName
	 */
	public String getAppliedCodePropertyName() {
		return mAppliedCodePropertyName;
	}

	/**
	 * @param pAppliedCodePropertyName the appliedCodePropertyName to set
	 */
	public void setAppliedCodePropertyName(String pAppliedCodePropertyName) {
		mAppliedCodePropertyName = pAppliedCodePropertyName;
	}

	/**
	 * @return the appliedCodeValuePropertyName
	 */
	public String getAppliedCodeValuePropertyName() {
		return mAppliedCodeValuePropertyName;
	}

	/**
	 * @param pAppliedCodeValuePropertyName the appliedCodeValuePropertyName to set
	 */
	public void setAppliedCodeValuePropertyName(String pAppliedCodeValuePropertyName) {
		mAppliedCodeValuePropertyName = pAppliedCodeValuePropertyName;
	}
	/**
	 * This method is to get profileSyncErrorMessagesPropertyName
	 *
	 * @return the profileSyncErrorMessagesPropertyName
	 */
	public String getProfileSyncErrorMessagesPropertyName() {
		return mProfileSyncErrorMessagesPropertyName;
	}
	/**
	 * This method sets profileSyncErrorMessagesPropertyName with pProfileSyncErrorMessagesPropertyName
	 *
	 * @param pProfileSyncErrorMessagesPropertyName the profileSyncErrorMessagesPropertyName to set
	 */
	public void setProfileSyncErrorMessagesPropertyName(
			String pProfileSyncErrorMessagesPropertyName) {
		mProfileSyncErrorMessagesPropertyName = pProfileSyncErrorMessagesPropertyName;
	}
	/**
	 * This method is to get profileIdPropertyName
	 *
	 * @return the profileIdPropertyName
	 */
	public String getProfileIdPropertyName() {
		return mProfileIdPropertyName;
	}
	/**
	 * This method sets profileIdPropertyName with pProfileIdPropertyName
	 *
	 * @param pProfileIdPropertyName the profileIdPropertyName to set
	 */
	public void setProfileIdPropertyName(String pProfileIdPropertyName) {
		mProfileIdPropertyName = pProfileIdPropertyName;
	}
	/**
	 * This method is to get lastNamePropertyName
	 *
	 * @return the lastNamePropertyName
	 */
	public String getLastNamePropertyName() {
		return mLastNamePropertyName;
	}
	/**
	 * This method sets lastNamePropertyName with pLastNamePropertyName
	 *
	 * @param pLastNamePropertyName the lastNamePropertyName to set
	 */
	public void setLastNamePropertyName(String pLastNamePropertyName) {
		mLastNamePropertyName = pLastNamePropertyName;
	}
	/**
	 * This method is to get firstNamePropertyName
	 *
	 * @return the firstNamePropertyName
	 */
	public String getFirstNamePropertyName() {
		return mFirstNamePropertyName;
	}
	/**
	 * This method sets firstNamePropertyName with pFirstNamePropertyName
	 *
	 * @param pFirstNamePropertyName the firstNamePropertyName to set
	 */
	public void setFirstNamePropertyName(String pFirstNamePropertyName) {
		mFirstNamePropertyName = pFirstNamePropertyName;
	}
	/**
	 * This method is to get emailPropertyName
	 *
	 * @return the emailPropertyName
	 */
	public String getEmailPropertyName() {
		return mEmailPropertyName;
	}
	/**
	 * This method sets emailPropertyName with pEmailPropertyName
	 *
	 * @param pEmailPropertyName the emailPropertyName to set
	 */
	public void setEmailPropertyName(String pEmailPropertyName) {
		mEmailPropertyName = pEmailPropertyName;
	}
	/**
	 * This method is to get registrationDatePropertyName
	 *
	 * @return the registrationDatePropertyName
	 */
	public String getRegistrationDatePropertyName() {
		return mRegistrationDatePropertyName;
	}
	/**
	 * This method sets registrationDatePropertyName with pRegistrationDatePropertyName
	 *
	 * @param pRegistrationDatePropertyName the registrationDatePropertyName to set
	 */
	public void setRegistrationDatePropertyName(String pRegistrationDatePropertyName) {
		mRegistrationDatePropertyName = pRegistrationDatePropertyName;
	}
	/**
	 * This method is to get passwordPropertyName
	 *
	 * @return the passwordPropertyName
	 */
	public String getPasswordPropertyName() {
		return mPasswordPropertyName;
	}
	/**
	 * This method sets passwordPropertyName with pPasswordPropertyName
	 *
	 * @param pPasswordPropertyName the passwordPropertyName to set
	 */
	public void setPasswordPropertyName(String pPasswordPropertyName) {
		mPasswordPropertyName = pPasswordPropertyName;
	}
	/**
	 * This method is to get profileSyncErrorMessagePropertyName
	 *
	 * @return the profileSyncErrorMessagePropertyName
	 */
	public String getProfileSyncErrorMessagePropertyName() {
		return mProfileSyncErrorMessagePropertyName;
	}
	/**
	 * This method sets profileSyncErrorMessagePropertyName with pProfileSyncErrorMessagePropertyName
	 *
	 * @param pProfileSyncErrorMessagePropertyName the profileSyncErrorMessagePropertyName to set
	 */
	public void setProfileSyncErrorMessagePropertyName(
			String pProfileSyncErrorMessagePropertyName) {
		mProfileSyncErrorMessagePropertyName = pProfileSyncErrorMessagePropertyName;
	}
	/**
	 * This method is to get fileNamePropertyName
	 *
	 * @return the fileNamePropertyName
	 */
	public String getFileNamePropertyName() {
		return mFileNamePropertyName;
	}
	/**
	 * This method sets fileNamePropertyName with pFileNamePropertyName
	 *
	 * @param pFileNamePropertyName the fileNamePropertyName to set
	 */
	public void setFileNamePropertyName(String pFileNamePropertyName) {
		mFileNamePropertyName = pFileNamePropertyName;
	}
	/**
	 * This method is to get requestPropertyName
	 *
	 * @return the requestPropertyName
	 */
	public String getRequestPropertyName() {
		return mRequestPropertyName;
	}
	/**
	 * This method sets requestPropertyName with pRequestPropertyName
	 *
	 * @param pRequestPropertyName the requestPropertyName to set
	 */
	public void setRequestPropertyName(String pRequestPropertyName) {
		mRequestPropertyName = pRequestPropertyName;
	}
	
	
}
