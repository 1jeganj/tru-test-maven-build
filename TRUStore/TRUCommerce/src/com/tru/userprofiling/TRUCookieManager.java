package com.tru.userprofiling;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.ShippingGroupRelationship;
import atg.commerce.pricing.PricingModelHolder;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.nucleus.naming.ComponentName;
import atg.repository.RemovedItemException;
import atg.repository.RepositoryItem;
import atg.service.pipeline.RunProcessException;
import atg.service.util.CurrentDate;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;
import atg.userprofiling.CookieManager;
import atg.userprofiling.Profile;
import atg.userprofiling.PropertyManager;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUChannelInStorePickupShippingGroup;
import com.tru.commerce.order.TRUCommerceItemImpl;
import com.tru.commerce.order.TRUCommerceItemManager;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderHolder;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.purchase.TRUPurchaseProcessHelper;
import com.tru.common.TRUConstants;
import com.tru.common.TRUSession;
import com.tru.security.encryption.GenericEncryptor;
import com.tru.utils.TRUGetSiteTypeUtil;

/**
 * Extending cookie manager OOTB class.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCookieManager extends CookieManager {
 /**
 * String to hold quote
 */
public static final String QUOTE = "&quot;";

public static final String FORWARD_SLASH = "\"";
	
	/**  Property to hold mSiteRepository. */
	private String mSiteRepository;

	/** Property to hold mDateCookieName. */
	private String mDateCookieName;

	/** Property to hold order cookie name. */
	private String mOrderCookieName;

	/** Property to hold order cookie path. */
	private String mOrderCookiePath;

	/** Property to hold order cookie secure. */
	private boolean mOrderCookieSecure;

	/** Property to hold order id encryption. */
	private boolean mOrderIdEncryption;

	/** Property to hold myStore cookie name. */
	private String mMyStoreCookieName;

	/** Property to hold myStore cookie path. */
	private String mMyStoreCookiePath;

	/** Property to hold myStore cookie max age. */
	private int mMyStoreCookieMaxAge;

	/** Property to hold myStore cookie secure. */
	private boolean mMyStoreCookieSecure;

	/** Property to hold myStore id encryption. */
	private boolean mMyStoreIdEncryption;
	
	/** Property to hold alternate domain cookie. */
	private boolean mAlternateDomainCookie;

	/** Property to hold generic encryptor. */
	private GenericEncryptor mGenericEncryptor;

	/** Property to hold Commerce item manager. */
	private TRUCommerceItemManager mCommerceItemManager;

	/** Property to hold property manager. */
	private TRUPropertyManager mPropertyManager;

	/** Property to hold current date. */
  	private CurrentDate mCurrentDate;

  	/**  Property to hold logged in user cart cookie. */
	private String mUserCartCookieName;

	/** property to Hold TRU_SESSION_COMPONENT. */
	public static final ComponentName TRU_SESSION_COMPONENT = ComponentName.getComponentName("/com/tru/common/TRUSession");

	/** The Constant USER_PRICING_MODELS_COMPONENT_NAME. */
	public static final ComponentName USER_PRICING_MODELS_COMPONENT_NAME = ComponentName.getComponentName(TRUCommerceConstants.USER_PRICING_MODELS_COMPONENT_NAME);

	/** The Purchase process helper. */
	private TRUPurchaseProcessHelper mPurchaseProcessHelper;


	/** The Invalidate order pricing op. */
	private String mInvalidateOrderPricingOp;

	/** The commerce item type for cookie map. */
	private Map<String,String> mCommerceItemTypeForCookieMap;

	/** The site id for cookie map. */
	private Map<String,String> mSiteIdForCookieMap;

	/** The cart cookie max size. */
	private int mCartCookieMaxSize;
	
	/** The m tru get site type util. */
	private TRUGetSiteTypeUtil mTRUGetSiteTypeUtil;

	/** property to Hold mNewSession.. */
	private boolean  mNewSession;
	
	
	/**
	 * Gets the site id for cookie map.
	 *
	 * @return the site id for cookie map.
	 */
	public Map<String, String> getSiteIdForCookieMap() {
		return mSiteIdForCookieMap;
	}

	/**
	 * Sets the site id for cookie map.
	 *
	 * @param pSiteIdForCookieMap the site id for cookie map.
	 */
	public void setSiteIdForCookieMap(Map<String, String> pSiteIdForCookieMap) {
		mSiteIdForCookieMap = pSiteIdForCookieMap;
	}

	/**
	 * Gets the cart cookie max size.
	 *
	 * @return the cart cookie max size.
	 */
	public int getCartCookieMaxSize() {
		return mCartCookieMaxSize;
	}

	/**
	 * Sets the cart cookie max size.
	 *
	 * @param pCartCookieMaxSize the new cart cookie max size.
	 */
	public void setCartCookieMaxSize(int pCartCookieMaxSize) {
		mCartCookieMaxSize = pCartCookieMaxSize;
	}

	/**
	 * Gets the commerce item type for cookie map.
	 *
	 * @return the commerce item type for cookie map.
	 */
	public Map<String, String> getCommerceItemTypeForCookieMap() {
		return mCommerceItemTypeForCookieMap;
	}

	/**
	 * Sets the commerce item type for cookie map.
	 *
	 * @param pCommerceItemTypeForCookieMap the commerce item type for cookie map.
	 */
	public void setCommerceItemTypeForCookieMap(Map<String, String> pCommerceItemTypeForCookieMap) {
		mCommerceItemTypeForCookieMap = pCommerceItemTypeForCookieMap;
	}

	/**
	 * Gets the purchase process helper.
	 *
	 * @return the purchase process helper.
	 */
	public TRUPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}

	/**
	 * Sets the purchase process helper.
	 *
	 * @param pPurchaseProcessHelper the new purchase process helper.
	 */
	public void setPurchaseProcessHelper(TRUPurchaseProcessHelper pPurchaseProcessHelper) {
		mPurchaseProcessHelper = pPurchaseProcessHelper;
	}

	/**
	 * Gets the invalidate order pricing op.
	 *
	 * @return the invalidate order pricing op.
	 */
	public String getInvalidateOrderPricingOp() {
		return mInvalidateOrderPricingOp;
	}

	/**
	 * Sets the invalidate order pricing op.
	 *
	 * @param pInvalidateOrderPricingOp the new invalidate order pricing op.
	 */
	public void setInvalidateOrderPricingOp(String pInvalidateOrderPricingOp) {
		mInvalidateOrderPricingOp = pInvalidateOrderPricingOp;
	}


	/**
	 * This method generates the cookie with email id in it after user is created.
	 * @param pProfile Profile.
	 */
	public void generateProfileNameCookie(Profile pProfile) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::generateProfileNameCookie() : BEGIN");
		}
		PropertyManager lPropertyManager = pProfile.getProfileTools().getPropertyManager();
		//get the email id from profile
		if(pProfile !=null){
			String lUserEmail = (String)pProfile.getPropertyValue(lPropertyManager.getEmailAddressPropertyName());
			Cookie lCreateUserCookie = new Cookie(TRUConstants.PROFILE_COOKIE_NAME, lUserEmail);
			lCreateUserCookie.setMaxAge(Integer.MAX_VALUE);
			DynamoHttpServletResponse response = ServletUtil.getCurrentResponse();
			DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
			lCreateUserCookie.setPath(TRUCommerceConstants.PATH_SEPERATOR);
			response.addCookie(lCreateUserCookie);
			addCookieToSession(request, lCreateUserCookie);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::generateProfileNameCookie() : END");
		}
	}

	/**
	 * This method expires the cookie with email id in it after user has logged out.
	 * @param pCookieName the cookie name.
	 */
	public void expireProfileNameCookie(String pCookieName) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::expireProfileNameCookie() : BEGIN");
		}
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		DynamoHttpServletResponse response = ServletUtil.getCurrentResponse();
		Cookie[] cookies = request.getCookies();
		Cookie cookie = null;
		if (cookies != null && null!=pCookieName) {
			for (Cookie tempCookie : cookies) {
				if (tempCookie.getName().equals(pCookieName)) {
					cookie = tempCookie;
					break;
				}
			}
			if (null != cookie) {
				cookie.setMaxAge(0);
				cookie.setSecure(true);
				cookie.setValue(TRUConstants.EMPTY);
				cookie.setPath(TRUConstants.PATH);
				response.addCookie(cookie);
				removeCookieFromSession(request, cookie.getName());
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::expireProfileNameCookie() : END");
		}
	}
	
	/**
	 * This method sets DPSLogout cookie after user has logged out for Tealium purpose.
	 * @param pCookieName the cookie name.
	 */
	public void setLogoutCookieForTealium(String pCookieName) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::setLogoutCookieForTealium() : BEGIN");
		}
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		DynamoHttpServletResponse response = ServletUtil.getCurrentResponse();
		Cookie logoutTealiumCookie = new Cookie(pCookieName, "true");
		logoutTealiumCookie.setMaxAge(Integer.MAX_VALUE);
		logoutTealiumCookie.setPath(TRUCommerceConstants.PATH_SEPERATOR);
		response.addCookie(logoutTealiumCookie);
		addCookieToSession(request, logoutTealiumCookie);
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::setLogoutCookieForTealium() : END");
		}
	}

	/**
	 * This method is used to store the cookie for not showing the email notification popup to user until a certain no. of days.
	 *
	 * @param pCookieName String.
	 * @param pCookieDescription String.
	 * @return cookie Cookie.
	 */
	public Cookie createLoginCookie(String pCookieName,String pCookieDescription) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::createLoginCookie() : BEGIN");
		}
		Cookie cookie;
		cookie = new Cookie(pCookieName, pCookieDescription);
		cookie.setPath(TRUCommerceConstants.PATH_SEPERATOR);
		cookie.setMaxAge(Integer.MAX_VALUE);
		cookie.setSecure(Boolean.FALSE);
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::createLoginCookie() : END");
		}
		return cookie;
	}

	/**
	 * Generate the cart cookie in well structured manner.
	 *
	 * @param pOrder            the order.
	 * @param pCookieName CookieName.
	 * @param pSite the site.
	 * @param pRequest the request.
	 * @param pResponse the response.
	 * @throws CommerceException             the commerce exception.
	 */
	private void generateCartCookie(Order pOrder, String pCookieName, Site pSite, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws CommerceException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::generateCartCookie() : BEGIN");
		}
		String cookieName =pCookieName;
		int maxAge=0;
		if(null!=pSite.getPropertyValue(getPropertyManager().getPersistOrderDays())){
			maxAge=(int)pSite.getPropertyValue(getPropertyManager().getPersistOrderDays());
		}
		List<ShippingGroupCommerceItemRelationship> shGrpCommItemRelShipList = getShippingGroupCommerceItemRelationships(pOrder);
		if (null != pOrder && null != shGrpCommItemRelShipList && !shGrpCommItemRelShipList.isEmpty()) {
			StringBuffer cookieString = new StringBuffer();
			int cookieCount =0;
			for (ShippingGroupCommerceItemRelationship shGrpCommItemRelShip : shGrpCommItemRelShipList) {
				if (null != shGrpCommItemRelShip && shGrpCommItemRelShip.getCommerceItem() instanceof TRUCommerceItemImpl && !(shGrpCommItemRelShip.getCommerceItem() instanceof TRUGiftWrapCommerceItem)) {
					TRUCommerceItemImpl truCommerceItem = (TRUCommerceItemImpl) shGrpCommItemRelShip.getCommerceItem();
					ShippingGroup shippingGroup = shGrpCommItemRelShip.getShippingGroup();
					cookieString.append(truCommerceItem.getCatalogRefId());
					cookieString.append(TRUConstants.PIPE_STRING);
					cookieString.append(truCommerceItem.getAuxiliaryData().getProductId());
					cookieString.append(TRUConstants.PIPE_STRING);
					if (shippingGroup instanceof TRUInStorePickupShippingGroup) {
						cookieString.append(((TRUInStorePickupShippingGroup) shippingGroup).getLocationId());
					} else if (shippingGroup instanceof TRUChannelInStorePickupShippingGroup) {
						cookieString.append(((TRUChannelInStorePickupShippingGroup) shippingGroup).getLocationId());
					} else {
						cookieString.append(TRUConstants.EMPTY);
					}
					cookieString.append(TRUConstants.PIPE_STRING);
					if (shippingGroup instanceof TRUChannelHardgoodShippingGroup) {
						setChannelHardGoodShipValuesToCartCookie(cookieString,shippingGroup);
					} else if (shippingGroup instanceof TRUChannelInStorePickupShippingGroup) {
						setChannelInStoreShipValuesToCartCookie(cookieString,shippingGroup);
					} else {
						cookieString.append(TRUConstants.EMPTY);
						cookieString.append(TRUConstants.PIPE_STRING);
						cookieString.append(TRUConstants.EMPTY);
						cookieString.append(TRUConstants.PIPE_STRING);
						cookieString.append(TRUConstants.EMPTY);
						cookieString.append(TRUConstants.PIPE_STRING);
						cookieString.append(TRUConstants.EMPTY);
						cookieString.append(TRUConstants.PIPE_STRING);
						cookieString.append(TRUConstants.EMPTY);
						cookieString.append(TRUConstants.PIPE_STRING);
						cookieString.append(TRUConstants.EMPTY);
						cookieString.append(TRUConstants.PIPE_STRING);
					}
					cookieString.append(shGrpCommItemRelShip.getQuantity());
					cookieString.append(TRUConstants.PIPE_STRING);
					if(truCommerceItem instanceof TRUDonationCommerceItem){
						cookieString.append(((TRUDonationCommerceItem) truCommerceItem).getDonationAmount());
						cookieString.append(TRUConstants.PIPE_STRING);
					}else{
						cookieString.append(TRUConstants.EMPTY);
						cookieString.append(TRUConstants.PIPE_STRING);
					}
					if(null != truCommerceItem.getAuxiliaryData()){
						cookieString
								.append(getSiteIdForCookieMap().get(truCommerceItem.getAuxiliaryData().getSiteId()));
						cookieString.append(TRUConstants.PIPE_STRING);
					}
					else{
						cookieString.append(TRUConstants.EMPTY);
						cookieString.append(TRUConstants.PIPE_STRING);
					}
					cookieString.append(getCommerceItemTypeForCookieMap().get(
							truCommerceItem.getCommerceItemClassType()));
					cookieString.append(TRUConstants.PIPE_STRING);
				}
				cookieString.append(TRUConstants.TILDA_STRING);
				if (isMaxSizeReached(cookieString.toString())) {
					if (isLoggingDebug()) {
						vlogDebug("@Class::TRUCookieManager::@method::MAX cookie size reached :", cookieCount);
					}
					if (cookieCount >= TRUCommerceConstants.INT_ONE) {
						cookieName = pCookieName + cookieCount;
					}
					addCartCookie(pRequest, pResponse, cookieName, maxAge, cookieString.toString());
					cookieCount++;
					cookieString = new StringBuffer();
				}
			}
			if(StringUtils.isNotEmpty(cookieString.toString())){
				if (cookieCount >= TRUCommerceConstants.INT_ONE) {
					cookieName = pCookieName + cookieCount;
				}
				addCartCookie(pRequest, pResponse, cookieName, maxAge, cookieString.toString());
			}
		}

		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::generateCartCookie() : END");
		}
	}

	/**
	 * Adds the cart cookie.
	 *
	 * @param pRequest the request.
	 * @param pResponse the response.
	 * @param pCookieName the cookie name.
	 * @param pMaxAge the max age.
	 * @param pCookieValue the cookie value.
	 */
	private void addCartCookie(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse,
			String pCookieName, int pMaxAge, String pCookieValue) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCookieManager::@method::generateCartCookie(), generated cookie name is :", pCookieName);
			vlogDebug("@Class::TRUCookieManager::@method::generateCartCookie(), generated cookie is :", pCookieValue);
		}
		Cookie cookie= new Cookie(pCookieName, pCookieValue);
		if (cookie != null) {
			cookie.setMaxAge(pMaxAge * TRUConstants.INT_SIXTY);
			cookie.setPath(getOrderCookiePath());
		}
		if (pResponse != null && cookie != null) {
			pResponse.addCookie(cookie);
		}
		addCookieToSession(pRequest, cookie);
	}

	/**
	 * Checks if is max size reached.
	 *
	 * @param pCookieString the pcookie string.
	 * @return true, if is max size reached.
	 */
	private boolean isMaxSizeReached(String pCookieString) {
		boolean maxSizeReached=false;
		try {
			byte[] utf8Bytes;
			utf8Bytes = pCookieString.getBytes(TRUCommerceConstants.UTF_ENCODE);
			if(utf8Bytes.length >= getCartCookieMaxSize()){
				maxSizeReached=true;
			}
		} catch (UnsupportedEncodingException encodingException) {
			if(isLoggingError()){
				logError("UnsupportedEncodingException in TRUCookieManager.isMaxSizeReached()", encodingException);
			}
		}
		return maxSizeReached;
	}

	/**
	 * Sets the channel in store ship values to cart cookie.
	 *
	 * @param pCookieString the cookie string.
	 * @param pShippingGroup the shipping group.
	 */
	private void setChannelInStoreShipValuesToCartCookie(StringBuffer pCookieString, ShippingGroup pShippingGroup) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::setChannelInStoreShipValuesToCartCookie() : BEGIN");
		}
		if (StringUtils.isBlank(((TRUChannelInStorePickupShippingGroup) pShippingGroup).getChannelId())) {
			pCookieString.append(TRUConstants.EMPTY);
		} else {
			pCookieString.append(((TRUChannelInStorePickupShippingGroup) pShippingGroup).getChannelId());
		}
		pCookieString.append(TRUConstants.PIPE_STRING);
		if (StringUtils.isBlank(((TRUChannelInStorePickupShippingGroup) pShippingGroup).getChannelType())) {
			pCookieString.append(TRUConstants.EMPTY);
		} else {
			pCookieString.append(((TRUChannelInStorePickupShippingGroup) pShippingGroup).getChannelType());
		}
		pCookieString.append(TRUConstants.PIPE_STRING);
		if (StringUtils.isBlank(((TRUChannelInStorePickupShippingGroup) pShippingGroup).getChannelUserName())) {
			pCookieString.append(TRUConstants.EMPTY);
		} else {
			pCookieString.append(((TRUChannelInStorePickupShippingGroup) pShippingGroup).getChannelUserName());
		}
		pCookieString.append(TRUConstants.PIPE_STRING);
		if (StringUtils.isBlank(((TRUChannelInStorePickupShippingGroup) pShippingGroup).getChannelCoUserName())) {
			pCookieString.append(TRUConstants.EMPTY);
		} else {
			pCookieString.append(((TRUChannelInStorePickupShippingGroup) pShippingGroup).getChannelCoUserName());
		}
		pCookieString.append(TRUConstants.PIPE_STRING);
		if (StringUtils.isBlank(((TRUChannelInStorePickupShippingGroup) pShippingGroup).getChannelName())) {
			pCookieString.append(TRUConstants.EMPTY);
		} else {
			pCookieString.append(((TRUChannelInStorePickupShippingGroup) pShippingGroup).getChannelName());
		}
		pCookieString.append(TRUConstants.PIPE_STRING);
		if (StringUtils.isBlank(((TRUChannelInStorePickupShippingGroup) pShippingGroup).getSourceChannel())) {
			pCookieString.append(TRUConstants.EMPTY);
		} else {
			pCookieString.append(((TRUChannelInStorePickupShippingGroup) pShippingGroup).getSourceChannel());
		}
		pCookieString.append(TRUConstants.PIPE_STRING);
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::setChannelInStoreShipValuesToCartCookie() : END");
		}
	}


	/**
	 * Sets the channel hard good ship values to cart cookie.
	 *
	 * @param pCookieString the cookie string.
	 * @param pShippingGroup the shipping group.
	 */
	private void setChannelHardGoodShipValuesToCartCookie(StringBuffer pCookieString, ShippingGroup pShippingGroup) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::setChannelHardGoodShipValuesToCartCookie() : BEGIN");
		}
		if (StringUtils.isBlank(((TRUChannelHardgoodShippingGroup) pShippingGroup).getChannelId())) {
			pCookieString.append(TRUConstants.EMPTY);
		} else {
			pCookieString.append(((TRUChannelHardgoodShippingGroup) pShippingGroup).getChannelId());
		}
		pCookieString.append(TRUConstants.PIPE_STRING);
		if (StringUtils.isBlank(((TRUChannelHardgoodShippingGroup) pShippingGroup).getChannelType())) {
			pCookieString.append(TRUConstants.EMPTY);
		} else {
			pCookieString.append(((TRUChannelHardgoodShippingGroup) pShippingGroup).getChannelType());
		}
		pCookieString.append(TRUConstants.PIPE_STRING);
		if (StringUtils.isBlank(((TRUChannelHardgoodShippingGroup) pShippingGroup).getChannelUserName())) {
			pCookieString.append(TRUConstants.EMPTY);
		} else {
			pCookieString.append(((TRUChannelHardgoodShippingGroup) pShippingGroup).getChannelUserName());
		}
		pCookieString.append(TRUConstants.PIPE_STRING);
		if (StringUtils.isBlank(((TRUChannelHardgoodShippingGroup) pShippingGroup).getChannelCoUserName())) {
			pCookieString.append(TRUConstants.EMPTY);
		} else {
			pCookieString.append(((TRUChannelHardgoodShippingGroup) pShippingGroup).getChannelCoUserName());
		}
		pCookieString.append(TRUConstants.PIPE_STRING);
		if (StringUtils.isBlank(((TRUChannelHardgoodShippingGroup) pShippingGroup).getChannelName())) {
			pCookieString.append(TRUConstants.EMPTY);
		} else {
			pCookieString.append(((TRUChannelHardgoodShippingGroup) pShippingGroup).getChannelName());
		}
		pCookieString.append(TRUConstants.PIPE_STRING);
		if (StringUtils.isBlank(((TRUChannelHardgoodShippingGroup) pShippingGroup).getSourceChannel())) {
			pCookieString.append(TRUConstants.EMPTY);
		} else {
			pCookieString.append(((TRUChannelHardgoodShippingGroup) pShippingGroup).getSourceChannel());
		}
		pCookieString.append(TRUConstants.PIPE_STRING);
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::setChannelHardGoodShipValuesToCartCookie() : END");
		}
	}

	/**
	 * Encrypts / decrypts the value based on the Inputs passed. For Decryption the return value is appended with the
	 * key encryptionIdentifier.
	 *
	 * @param pValue
	 *            - Value to be manipulated.
	 * @param pEncrypt
	 *            - To encrypt or Decrypt the Value.
	 * @return Returns Encrypted or Decrypted value based on the parameters passed.
	 */
	public String encryptDecrypt(String pValue, boolean pEncrypt) {
		if (isLoggingDebug()) {
			logDebug("TRUCookieManager.encryptDecrypt() : Entering the call for Encryption / DEcryption");
			vlogDebug("TRUCookieManager.encryptDecrypt() : Inputs Recieved: value : {0}, is Encryption : {1}", pValue, pEncrypt);
		}
		String returnValue = pValue;
		try {
			if (pEncrypt) {
				if (isLoggingDebug()) {
					logDebug("TRUCookieManager.encryptDecrypt() : Performing Encryption with Base64 Encoding");
				}
				returnValue = mGenericEncryptor.encrypt(pValue, true);
			} else {
				if (isLoggingDebug()) {
					logDebug("TRUCookieManager.encryptDecrypt() : Performing Decryption with Base64 Encoding");
				}
				returnValue = mGenericEncryptor.decrypt(pValue, true);
			}
		} catch (InvalidKeyException e) {
			if (isLoggingError()) {
				logError("InvalidKeyException in TRUCookieManager.encryptDecrypt()", e);
			}
		} catch (NoSuchPaddingException e) {
			if (isLoggingError()) {
				logError("NoSuchPaddingException in TRUCookieManager.encryptDecrypt()", e);
			}
		} catch (NoSuchAlgorithmException e) {
			if (isLoggingError()) {
				logError("NoSuchAlgorithmException in TRUCookieManager.encryptDecrypt()", e);
			}
		} catch (BadPaddingException e) {
			if (isLoggingError()) {
				logError("BadPaddingException in TRUCookieManager.encryptDecrypt()", e);
			}
		} catch (IllegalBlockSizeException e) {
			if (isLoggingError()) {
				logError("IllegalBlockSizeException in TRUCookieManager.encryptDecrypt()", e);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUCookieManager.encryptDecrypt() : Return Value is : {0}", returnValue);
		}
		return returnValue;
	}

	/**
	 * This method creates cart cookie to remember Transient or session based shopping bag information.
	 *
	 * @param pOrder            - Current Order.
	 * @param pRequest 			-the request.
	 * @param pResponse            - the servlet's response.
	 * @param pCookieName the cookie name.
	 * @exception ServletException                if an error occurred while processing the servlet request.
	 * @exception IOException                if an error occurred while reading or writing the servlet request.
	 * @throws CommerceException the commerce exception.
	 */
	public void createCartCookie(Order pOrder,DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse ,String pCookieName) throws ServletException,
			IOException, CommerceException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::createCartCookie() : BEGIN");
		}
		if(SiteContextManager.getCurrentSite() != null){
			Site site=SiteContextManager.getCurrentSite();
			Object enableCartCookie = site.getPropertyValue(getPropertyManager().getEnablePersistOrderPropertyName());
			if(enableCartCookie != null && (boolean)enableCartCookie){
				generateCartCookie(pOrder, pCookieName,site,pRequest, pResponse);
			}else{
				removeCartCookie(pRequest, pResponse);
				if (isLoggingDebug()) {
					logDebug("@Class::TRUCookieManager::@method::createCartCookie() : cart cookie is disabled");
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::createCartCookie() : END");
		}
	}

	/**
	 * Adds the cookie to session component.
	 *
	 * @param pRequest the request.
	 * @param pCookie the cookie.
	 */
	public void addCookieToSession(DynamoHttpServletRequest pRequest, Cookie pCookie) {
		if (pRequest != null && pCookie != null) {
			TRUSession truSession = (TRUSession) pRequest.resolveName(TRU_SESSION_COMPONENT);
			if (truSession != null) {
				Map<String, Cookie> truCookies = truSession.getTRUCookies();
				if (truCookies == null) {
					truCookies = new HashMap<String, Cookie>();
				}

				truCookies.put(pCookie.getName(), pCookie);
				truSession.setTRUCookies(truCookies);
			}
		}
	}

	/**
	 * Removes the cookie from session.
	 *
	 * @param pRequest the request.
	 * @param pCookieName the cookie name.
	 */
	public void removeCookieFromSession(DynamoHttpServletRequest pRequest, String pCookieName) {
		if (pRequest != null && pCookieName != null) {
			TRUSession truSession = (TRUSession) pRequest.resolveName(TRU_SESSION_COMPONENT);
			if (truSession != null) {
				Map<String, Cookie> truCookies = truSession.getTRUCookies();
				if (truCookies != null && truCookies.containsKey(pCookieName)) {
					truCookies.remove(pCookieName);
					truSession.setTRUCookies(truCookies);
				}
			}
		}
	}

	/**
	 * Adds the cookie with domain.
	 *
	 * @param pResponse the response.
	 * @param pCookie the cookie.
	 */
	public void copyCookie(DynamoHttpServletResponse pResponse,
			Cookie pCookie) {
		if (pResponse != null && pCookie != null) {
			Cookie newCookie = new Cookie(pCookie.getName(), pCookie.getValue());
			newCookie.setPath(pCookie.getPath());
			newCookie.setMaxAge(pCookie.getMaxAge());
			pResponse.addCookie(newCookie);
		}
	}

	/**
	 * This method removes the cookies related to number of items or quantities and sub-total.
	 *
	 * @param pRequest
	 *            - the servlet's request.
	 * @param pResponse
	 *            - the servlet's response.
	 */
	public void removeCartCookie(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::removeCartCookie() : BEGIN");
		}
		if (pResponse != null && pRequest != null) {
			removeAllCartCookies(pRequest, pResponse, getOrderCookieName());
			removeAllCartCookies(pRequest, pResponse, TRUConstants.LOGGEDEIN_CART_COOKIE_NAME);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::removeCartCookie() : END");
		}
	}

	/**
	 * Removes the all cart cookies.
	 *
	 * @param pRequest the request.
	 * @param pResponse the response.
	 * @param pCookieName the cookie name.
	 */
	private void removeAllCartCookies(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse,
			String pCookieName) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::removeAllCartCookies() : BEGIN");
			vlogDebug("@Class::TRUCookieManager::@method::removeAllCartCookies() : CookieName {0}:", pCookieName);
		}
		String cookieName = pCookieName;
		Cookie cartCookie = getCookieFromRequest(cookieName, pRequest);
		int cookieCount = TRUCommerceConstants.INT_ZERO;
		while (cartCookie != null) {
			cartCookie.setMaxAge(0);
			cartCookie.setPath(TRUConstants.FORWARD_SLASH);
			cartCookie.setValue(TRUConstants.EMPTY_STRING);
			pResponse.addCookie(cartCookie);
			removeCookieFromSession(pRequest, cartCookie.getName());
			if (cookieCount >= TRUCommerceConstants.INT_ONE) {
				cookieName = pCookieName + cookieCount;
			}
			cartCookie = getCookieFromRequest(cookieName, pRequest);
			cookieCount++;
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::removeAllCartCookies() : END");
		}
	}

	/**
	 * Gets the shipping group commerce item relationships.
	 *
	 * @param pOrder
	 *            the order.
	 * @return the shipping group commerce item relationships.
	 * @throws CommerceException
	 *             the commerce exception.
	 */
	private List getShippingGroupCommerceItemRelationships(Order pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::getShippingGroupCommerceItemRelationships() : BEGIN");
		}
		List<CommerceItemRelationship> ciRels = null;
		List<ShippingGroupRelationship> sgRels = null;
		ciRels = getCommerceItemManager().getAllCommerceItemRelationships(pOrder);
		sgRels = getCommerceItemManager().getShippingGroupManager()
				.getAllShippingGroupRelationships(pOrder);
		if (!ciRels.isEmpty() && !sgRels.isEmpty()) {
			ciRels.retainAll(sgRels);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::getShippingGroupCommerceItemRelationships() : END");
		}
		return ciRels;
	}

	/**
	 * This method creates myStore cookie to remember selected store as my favorite store.
	 *
	 * @param pStoreId            - selected StoreId.
	 * @param pRequest			  - DynamoHttpServletRequest.
	 * @param pResponse            - the servlet's response.
	 * @exception ServletException  -  if an error occurred while processing the servlet request.
	 * @exception IOException -      if an error occurred while reading or writing the servlet request.
	 */
	public void createMyStoreCookie(String pStoreId, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::createMyStoreCookie() : BEGIN");
		}
		Cookie newCookie = null;
		StringBuffer cookieString = new StringBuffer();
		cookieString.append(pStoreId);
		if (pRequest.getCookies() != null) {
			for (Cookie reqCookie : pRequest.getCookies()) {
				if (reqCookie != null && reqCookie.getName().equals(TRUCommerceConstants.MAKE_MYSTORE_COOKIE)) {
					reqCookie.setValue(pStoreId);
					reqCookie.setMaxAge(getMyStoreCookieMaxAge());
					reqCookie.setPath(getMyStoreCookiePath());
					reqCookie.setValue(encryptDecrypt(cookieString.toString(), false));
					reqCookie.setSecure(isMyStoreCookieSecure());
					if (pResponse != null && reqCookie != null) {
						pResponse.addCookie(reqCookie);
					}
					addCookieToSession(pRequest, reqCookie);
				} else {
					newCookie = new Cookie(getMyStoreCookieName(), cookieString.toString());
				}
				break;
			}
		} else {
			newCookie = new Cookie(getMyStoreCookieName(), cookieString.toString());
		}
		if (pResponse != null && newCookie != null) {
			newCookie.setMaxAge(getMyStoreCookieMaxAge());
			newCookie.setPath(getMyStoreCookiePath());
			newCookie.setValue(encryptDecrypt(cookieString.toString(), true));
			newCookie.setSecure(isMyStoreCookieSecure());
			pResponse.addCookie(newCookie);
			addCookieToSession(pRequest, newCookie);
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCookieManager::@method::createMyStoreCookie(), generated cookie is ", cookieString.toString());
			logDebug("@Class::TRUCookieManager::@method::createMyStoreCookie() : END");
		}
	}


	/**
	 * This method creates myStore cookie to remember selected store as my favorite store.
	 * @param pStoreName            - selected StoreId.
	 * @param pRequest			  - DynamoHttpServletRequest.
	 * @param pResponse            - the servlet's response.
	 * @exception ServletException  -  if an error occurred while processing the servlet request.
	 * @exception IOException -      if an error occurred while reading or writing the servlet request.
      */
	public void createMyStoreNameCookie(String pStoreName, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {


		if (isLoggingDebug()) {
			logDebug("Start: TRUCookieManager.createMyStoreNameCookie()");
		}
		
		String storeName = null;
		if(!StringUtils.isBlank(pStoreName))
		{
			storeName =  pStoreName.replaceAll(QUOTE,FORWARD_SLASH);
		}

	/*	String reqMyStoreNameCookie = pRequest.getCookieParameter(TRUCommerceConstants.MAKE_MYSTORENAME_COOKIE);
		if ((reqMyStoreNameCookie != null) &&(pStoreName.equals(reqMyStoreNameCookie))) {
		      return;
		 }else{*/
			 generateCookie(TRUCommerceConstants.MAKE_MYSTORENAME_COOKIE, storeName, pRequest, pResponse);
		/*}
		if (isLoggingDebug()) {
			logDebug("End: TRUCookieManager.createMyStoreAddressCookie()");
		}

			if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::createMyStoreCookie() : BEGIN");
		}
		Cookie newCookie = null;
		StringBuffer cookieString = new StringBuffer();
		cookieString.append(storeName);
		if (pRequest.getCookies() != null) {
			for (Cookie reqCookie : pRequest.getCookies()) {
				if (reqCookie != null && reqCookie.getName().equals(TRUCommerceConstants.MAKE_MYSTORENAME_COOKIE)) {
					reqCookie.setValue(storeName);
					reqCookie.setMaxAge(getMyStoreCookieMaxAge());
					reqCookie.setPath(getMyStoreCookiePath());
					reqCookie.setValue(encryptDecrypt(cookieString.toString(), false));
					reqCookie.setSecure(isMyStoreCookieSecure());
					if (pResponse != null && reqCookie != null) {
						pResponse.addCookie(reqCookie);
					}
					addCookieToSession(pRequest, reqCookie);
				} else {
					newCookie = new Cookie(TRUCommerceConstants.MAKE_MYSTORENAME_COOKIE, cookieString.toString());
				}
				break;
			}
		} else {
			newCookie = new Cookie(TRUCommerceConstants.MAKE_MYSTORENAME_COOKIE, cookieString.toString());
		}
		if (pResponse != null && newCookie != null) {
			newCookie.setMaxAge(getMyStoreCookieMaxAge());
			newCookie.setPath(getMyStoreCookiePath());


			pResponse.addCookie(newCookie);
			addCookieToSession(pRequest, newCookie);
		}*/
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCookieManager::@method::createMyStoreNameCookie(), generated cookie is ", storeName);
			logDebug("@Class::TRUCookieManager::@method::createMyStoreNameCookie() : END");
		}
	}

	/**
	 * This method is used to create abandon cart cookie to store current date in cookie.
	 *
	 * @param pRequest DynamoHttpServletRequest.
	 * @param pResponse DynamoHttpServletResponse.
	 */
	public void createAbandonCartCookie(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::createAbandonCartCookie() : BEGIN");
		}
		if (SiteContextManager.getCurrentSite() != null) {
			Object enableCartCookie = SiteContextManager.getCurrentSite().getPropertyValue(
					getPropertyManager().getEnablePersistOrderPropertyName());
			if (enableCartCookie != null && (boolean) enableCartCookie) {
				boolean abandonCartThreshold = false;
				long lastTimeStamp = TRUConstants.ZERO;
				long currentTimeStamp = TRUConstants.ZERO;
				if (null != getCurrentDate()) {
					currentTimeStamp = getCurrentDate().getTime() / TRUConstants.NUMBER_THOUSAND;
				}
				if (isLoggingDebug()) {
					vlogDebug("create abandon cart cookie : lastTimeStamp : {0}, currentTimeStamp {1}", lastTimeStamp, currentTimeStamp);
				}
				// Date Cookie
				if (pRequest.getCookies() != null && pRequest.getCookieParameter(getDateCookieName()) != null) {
					Cookie[] cookies = pRequest.getCookies();
					RepositoryItem siteItem = SiteContextManager.getCurrentSite();
					for (Cookie reqCookie : cookies) {
						if (reqCookie != null && reqCookie.getName().equals(getDateCookieName())) {
							String lastTime = reqCookie.getValue();
							if (StringUtils.isNotBlank(lastTime)) {
								lastTimeStamp = Long.parseLong(lastTime);
							}
							long threshold = currentTimeStamp - lastTimeStamp;
							if (siteItem.getPropertyValue(getPropertyManager().getAbandonCartThreshold()) != null
									&& threshold > Long.parseLong(siteItem
											.getPropertyValue(getPropertyManager().getAbandonCartThreshold())
											.toString().trim())) {
								abandonCartThreshold = true;
							}
							break;
						}
					}
				}
				setDateCookie(pRequest, pResponse, currentTimeStamp);
				setSiteReminderCountCookie(pRequest, pResponse);
				if (pRequest.getCookieParameter(TRUConstants.ITEM_COUNT_COOKIE) != null && 
						Integer.parseInt(pRequest.getCookieParameter(TRUConstants.ITEM_COUNT_COOKIE).trim()) > 0 && abandonCartThreshold) {
					abandonCartThreshold = true;
				}
				setWelcomeBackCookie(pRequest, pResponse, abandonCartThreshold);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::createAbandonCartCookie() : END");
		}
	}

	/**
	 * Sets the date cookie.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @param currentTimeStamp the current time stamp
	 */
	private void setDateCookie(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse,
			long currentTimeStamp) {
		Cookie dateCookie = ServletUtil.createCookie(getDateCookieName(), String.valueOf(currentTimeStamp));
		dateCookie.setMaxAge(getMyStoreCookieMaxAge());
		dateCookie.setPath(getMyStoreCookiePath());
		dateCookie.setSecure(isMyStoreCookieSecure());
		if (pResponse != null && dateCookie != null) {
			pResponse.addCookie(dateCookie);
			addCookieToSession(pRequest, dateCookie);
		}
	}

	/**
	 * Sets the site reminder count cookie.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 */
	private void setSiteReminderCountCookie(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		String siteReminderCountVal = TRUCommerceConstants.STRING_ZERO;
		Object siteReminderCount = SiteContextManager.getCurrentSite().getPropertyValue(
				getPropertyManager().getReminderCount());
		if (siteReminderCount != null) {
			siteReminderCountVal = Integer.toString((Integer)siteReminderCount);
		}
		Cookie reminderCount = ServletUtil.createCookie(TRUCommerceConstants.SITE_REMINDER_COUNT_COOKIE,
				siteReminderCountVal);
		reminderCount.setMaxAge(getMyStoreCookieMaxAge());
		reminderCount.setPath(getMyStoreCookiePath());
		reminderCount.setSecure(isMyStoreCookieSecure());
		if (pResponse != null) {
			pResponse.addCookie(reminderCount);
			addCookieToSession(pRequest, reminderCount);
		}
	}

	/**
	 * Creates the welcome back cookie.
	 *
	 * @param pRequest the DynamoHttpServletRequest.
	 * @param pResponse the DynamoHttpServletResponse.
	 * @param pResetValue the Boolean.
	 */
	private void setWelcomeBackCookie(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse,
			Boolean pResetValue) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::resetOrCreateWelcomeBackCookie() : BEGIN");
		}
			ServletUtil.setCreateHttpOnlyCookies(false);
			Cookie showWelcomeBack = ServletUtil.createCookie(TRUCommerceConstants.SHOW_WELCOME_BACK, String.valueOf(pResetValue));
			showWelcomeBack.setMaxAge(getMyStoreCookieMaxAge());
			showWelcomeBack.setPath(getMyStoreCookiePath());
			showWelcomeBack.setSecure(isMyStoreCookieSecure());
			if (pResponse != null) {
				pResponse.addCookie(showWelcomeBack);
				addCookieToSession(pRequest, showWelcomeBack);
			}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCookieManager::@method::resetOrCreateWelcomeBackCookie() : END");
		}
	}

	/**
	 * This method creates myStore Address cookie.
	 *
	 * @param pAddressValue the address value.
	 * @param pRequest the request.
	 * @param pResponse the response.
	 */
	public void createMyStoreAddressCookie(String pAddressValue, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse)  {

		if (isLoggingDebug()) {
			logDebug("Start: TRUCookieManager.createMyStoreAddressCookie()");
		}

		//String reqMyStoreAddressCookie = pRequest.getCookieParameter(TRUConstants.MY_STORE_ADDRESS_COOKIE);
		/*if ((reqMyStoreAddressCookie != null) &&(pAddressValue.equals(reqMyStoreAddressCookie))) {
		      return;
		 }else{*/
			 generateCookie(TRUConstants.MY_STORE_ADDRESS_COOKIE, pAddressValue, pRequest, pResponse);
		//}
		if (isLoggingDebug()) {
			logDebug("End: TRUCookieManager.createMyStoreAddressCookie()");
		}

	}

	/**
	 * This method creates myStore Address cookie.
	 *
	 * @param pFavStoreValue the address value.
	 * @param pRequest the request.
	 * @param pResponse the response.
	 */
	public void createFavStoreCookie(String pFavStoreValue, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse)  {

		if (isLoggingDebug()) {
			logDebug("Start: TRUCookieManager.createMyStoreAddressCookie()");
		}

		//String reqFavStoreCookie = pRequest.getCookieParameter(TRUConstants.FAV_STORE_COOKIE);
		/*if ((reqFavStoreCookie != null) &&(pFavStoreValue.equals(reqFavStoreCookie))) {
		      return;
		 }else{*/
			 generateCookie(TRUConstants.FAV_STORE_COOKIE, pFavStoreValue, pRequest, pResponse);
		//}
		if (isLoggingDebug()) {
			logDebug("End: TRUCookieManager.createMyStoreAddressCookie()");
		}

	}

	/**
	 * This method is basically used to generate cookie.
	 *
	 * @param pCookieName -cookie Name.
	 * @param pCookieValue -cookie Value.
	 * @param pRequest the request.
	 * @param pResponse the response.
	 */
	public void generateCookie(String pCookieName, String pCookieValue,	DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		Cookie cookie = null;
		if (!StringUtils.isBlank(pCookieName) && !StringUtils.isBlank(pCookieValue)) {
			cookie= new Cookie(pCookieName, pCookieValue);
			initializeCookie(cookie, pRequest, pResponse);
		}

	}

	/**
	 * This method is initializing the cookie values and setting into the response.
	 *
	 * @param pCookie - the cookie.
	 * @param pRequest - the request.
	 * @param pResponse the response.
	 */
	void initializeCookie(Cookie pCookie, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		
		if(pRequest.getParameter(TRUConstants.IS_TRUSESSION_COOKIE)!= null && pRequest.getParameter(TRUConstants.IS_TRUSESSION_COOKIE).equals(TRUConstants.TRUE)){
			pCookie.setMaxAge(TRUConstants.INT_MINUS_ONE);
		}else{
			pCookie.setMaxAge(TRUConstants.MAX_AGE_VALUE);
		}
		pCookie.setPath(TRUConstants.PATH);
		pCookie.setSecure(TRUConstants.COOKIE_SECURE_FALSE);
		pResponse.addCookie(pCookie);
		addCookieToSession(pRequest, pCookie);
	}

	/**
	 * Gets the cookie from request.
	 *
	 * @param pCookieName the cookie name.
	 * @param pRequest the request.
	 * @return the cookie from request.
	 */
	public Cookie getCookieFromRequest(String pCookieName, DynamoHttpServletRequest pRequest) {
		Cookie[] cookies = null;
		if (pRequest != null) {
			cookies = pRequest.getCookies();
		}
		if (null != cookies) {
			for (Cookie cookie : cookies) {
				if (cookie.getName() != null && cookie.getName().equals(pCookieName)) {
					return cookie;
				}
			}
		}
		return null;
	}

	/**
	 * This method creates the cookie related to itemCount.
	 * Before creating new cookies, first, it will check whether it is exist or not.
	 *
	 * @param pOrder the order.
	 * @param pRequest  - the servlet's request.
	 * @param pResponse - the servlet's response.
	 */
	public void createCookieForOrderItemCount(Order pOrder, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {

		if (isLoggingDebug()) {
			logDebug("Start: createCookieForOrderItemCount()");
		}

		String itemCountValue = String.valueOf(((TRUOrderImpl)pOrder).getTotalItemCount());
		String reqItemCount = pRequest.getCookieParameter(TRUConstants.ITEM_COUNT_COOKIE);
		if ((reqItemCount != null) && (itemCountValue.equals(reqItemCount))) {
		return;
		}else{
			generateCookie(TRUConstants.ITEM_COUNT_COOKIE, itemCountValue, pRequest, pResponse);
		}

		if (isLoggingDebug()) {
			logDebug("End: createCookieForOrderItemCount()");
		}
	}
	
	

	/**
	 * This method creates the cookie related to freeShippingBar.
	 * Before creating new cookies, first, it will check whether it is exist or not.
	 *
	 * @param pOrder the order.
	 * @param pRequest  - the servlet's request.
	 * @param pResponse - the servlet's response.
	 */
	public void createCookieForFreeShippingProgressBar(Order pOrder, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {

		if (isLoggingDebug()) {
			logDebug("Start: createCookieForFreeShippingProgressBar()");
		}
		TRUOrderHolder shoppingCart = (TRUOrderHolder) pRequest
				.resolveName(TRUCommerceConstants.SHOPPING_CART_COMPONENT_NAME);

		String freeShippingProgressBarValue = String.valueOf(appendingValue(
				shoppingCart.getAmountToQualifyPromotion(),
				shoppingCart.getShippingPromotionOrderLimit()));


		String reqFreeShippingBar = pRequest.getCookieParameter(TRUConstants.FREE_SHIPPING_PROGRESS_BAR_COOKIE);
		if ((reqFreeShippingBar != null) && (freeShippingProgressBarValue.equals(reqFreeShippingBar))) {
		      return;
		 }else{
			 generateCookie(TRUConstants.FREE_SHIPPING_PROGRESS_BAR_COOKIE, freeShippingProgressBarValue, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("End: createCookieForFreeShippingProgressBar()");
		}
	}

	/**
	 * This method used to appending value using delimiter.
	 *
	 * @param pShortOfAmount the short of amount.
	 * @param pPromotionAmount the promotion amount.
	 * @return the string.
	 */
	public String appendingValue(double pShortOfAmount, double pPromotionAmount) {
		StringBuilder sb = new StringBuilder();
		sb.append(String.valueOf(pShortOfAmount)).append(TRUConstants.PIPE_STRING).append(String.valueOf(pPromotionAmount));
		return sb.toString();
	}

	/**
	 * Initializing as zero.
	 *
	 * @param pCookie the cookie.
	 * @param pRequest the request.
	 * @param pResponse the response.
	 */
	public void initializingAsZero(String pCookie, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {
		Cookie cookie = new Cookie(pCookie, TRUCommerceConstants.STRING_ZERO);
		cookie.setMaxAge(TRUConstants.MAX_AGE_VALUE);
		cookie.setPath(TRUConstants.PATH);
		cookie.setSecure(TRUConstants.COOKIE_SECURE_FALSE);
		pResponse.addCookie(cookie);
		addCookieToSession(pRequest, cookie);

	}

	/**
	 * This method creates the cookie related to itemCount.
	 * Before creating new cookies, first, it will check whether it is exist or not.
	 *
	 * @param pOrder the order.
	 * @param pProfile the profile.
	 * @param pRequest  - the servlet's request.
	 * @param pResponse - the servlet's response.
	 */
	public void createCookieForOrderItemCount(Order pOrder, RepositoryItem pProfile, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {

		if (isLoggingDebug()) {
			logDebug("Start: createCookieForOrderItemCount()");
		}
		try {
			String itemCountValue = String.valueOf(((TRUOrderImpl) pOrder).getTotalItemCount());
			String reqItemCount = pRequest.getCookieParameter(TRUConstants.ITEM_COUNT_COOKIE);
			if ((reqItemCount != null) && (itemCountValue.equals(reqItemCount))) {
				return;
			} else {
				generateCookie(TRUConstants.ITEM_COUNT_COOKIE, itemCountValue, pRequest, pResponse);
			}
		} catch (RemovedItemException removedItemException) {
			if (isLoggingError()) {
				logError("RemovedItemException in @Class::TRUCookieManager::@method::createCookieForOrderItemCount()",
						removedItemException);
			}
			refreshOrder(pOrder, pProfile);
		}

		if (isLoggingDebug()) {
			logDebug("End: createCookieForOrderItemCount()");
		}
	}


	/**
	 * Refresh order.
	 *
	 * @param pOrder Order.
	 * @param pProfile Profile.
	 */
	public void refreshOrder(Order pOrder, RepositoryItem pProfile) {

		boolean rollbackFlag = Boolean.FALSE;
		TransactionDemarcation td = new TransactionDemarcation();

		synchronized (pOrder) {
			try {
				td.begin(getPurchaseProcessHelper().getTransactionManager(), TransactionDemarcation.REQUIRES_NEW);
				((TRUOrderImpl) pOrder).invalidateOrder();

				PricingModelHolder pricingModelHolder = (PricingModelHolder) ServletUtil.getCurrentRequest().resolveName(
						USER_PRICING_MODELS_COMPONENT_NAME);
				getPurchaseProcessHelper().runProcessRepriceOrder(getInvalidateOrderPricingOp(), pOrder, pricingModelHolder,
						getPurchaseProcessHelper().getOrderManager().getDefaultLocale(), pProfile, null, null);

			} catch (RunProcessException e) {
				rollbackFlag = Boolean.TRUE;
				if (isLoggingError()) {
					logError("RunProcessException in @Class::TRUCookieManager::@method::refreshOrder()", e);
				}
			} catch (TransactionDemarcationException e) {
				rollbackFlag = Boolean.TRUE;
				if (isLoggingError()) {
					logError("TransactionDemarcationException in @Class::TRUCookieManager::@method::refreshOrder()", e);
				}
			} finally {
				try {
					if (td != null) {
						td.end(rollbackFlag);
					}
				} catch (TransactionDemarcationException tde) {
					if (isLoggingError()) {
						logError("TransactionDemarcationException in @Class::TRUCookieManager::@method::refreshOrder()", tde);
					}

				}
			}

		}

	}

	/**
	 * Initializing as zero.
	 *
	 * @param pCookie Cookie.
	 * @param pResponse Response.
	 */
	public void initializingAsZero(String pCookie, DynamoHttpServletResponse pResponse) {
		Cookie cookie= new Cookie(pCookie, TRUConstants.DIGIT_ZERO);
		cookie.setMaxAge(TRUConstants.MAX_AGE_VALUE);
		cookie.setPath(TRUConstants.PATH);
		cookie.setSecure(TRUConstants.COOKIE_SECURE_FALSE);
		pResponse.addCookie(cookie);

	}

	/**
	 * This method creates the cookie related to current timestamp.
	 * 
	 *
	 * @param pRequest  - the servlet's request.
	 * @param pResponse - the servlet's response.
	 */
	public void createCookieForCurrentTimeStamp(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {

		if (isLoggingDebug()) {
			logDebug("Start: createCookieForCurrentTimeStamp()");
		}
			Calendar cal = Calendar.getInstance(); 
			Date date = cal.getTime(); 
			final String dateFormat = TRUCommerceConstants.SERVER_DATE_FORMAT;
			DateFormat formatter = new SimpleDateFormat(dateFormat,Locale.US); 
			formatter.setTimeZone(TimeZone.getTimeZone(TRUCheckoutConstants.TIME_ZONE));
			String parsedTimeStamp = formatter.format(date); 
            generateCookie(TRUConstants.CURRENT_SERVER_TIMESTAMP, parsedTimeStamp, pRequest, pResponse);
		
		if (isLoggingDebug()) {
			logDebug("End: createCookieForCurrentTimeStamp()");
		}
	}
	
	/**
	 * This method creates the cookie whether the navigation site is SOS or not.
	 * @param pRequest  - the servlet's request.
	 * @param pResponse - the servlet's response.
	 */
	public void createCookieForWebStoreOrSOS(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {

		if (isLoggingDebug()) {
			logDebug("Start: createCookieForWebStoreOrSOS()");
		}
		if(!pRequest.getContextPath().startsWith(TRUConstants.REST_CONTEXT_PATH)){
			Cookie sosCookie;
			if(pRequest.getServerName().startsWith(TRUConstants.AOS)){
				sosCookie = new Cookie(TRUConstants.SOS, TRUConstants.STRING_TRUE);
			}else{
				sosCookie = new Cookie(TRUConstants.SOS, TRUConstants.STRING_FALSE);
			}
			sosCookie.setMaxAge(Integer.MAX_VALUE);
			sosCookie.setPath(TRUConstants.PATH);
			sosCookie.setSecure(Boolean.FALSE);
			sosCookie.setDomain(TRUConstants.TOYSRUS_DOMAIN);
			pResponse.addCookie(sosCookie);
			addCookieToSession(pRequest, sosCookie);
		}
		if (isLoggingDebug()) {
			logDebug("End: createCookieForWebStoreOrSOS()");
		}
	}
	
	/**
	 * This method creates a cookie for deskTopView which is used by mobile to
	 * redirect to desktop full site from mobile.
	 * 
	 * @param pRequest - DynamoHttpServletRequest instance
	 * @param pResponse - DynamoHttpServletResponse instance
	 * @param  pDesktopSiteActive - pDesktopSiteActive can be true or false
	 */
	public void createCookieForDeskTopFullSite(
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse,String pDesktopSiteActive) {
		if (isLoggingDebug()) {
			logDebug("Start: createCookieForDeskTopFullSite()");
		}
		/*Site alternateSite = null;
		String siteUrl = null;
		String SiteId = SiteContextManager.getCurrentSiteId();
		try {
			if (StringUtils.isNotBlank(SiteId)
					&& SiteId.equalsIgnoreCase(TRUConstants.TOYSRUS_SITE)) {

				alternateSite = getTRUGetSiteTypeUtil().getSite(
						TRUConstants.BABYSRUS_SITE);

			} else if (StringUtils.isNotBlank(SiteId)
					&& SiteId.equalsIgnoreCase(TRUConstants.BABYSRUS_SITE)) {

				alternateSite = getTRUGetSiteTypeUtil().getSite(
						TRUConstants.TOYSRUS_SITE);

			}
		} catch (SiteContextException e) {
			vlogError(e, "Error getting site {0}", new Object[] { SiteId });
		}
		// get alternate site Url
		TRUSiteVO truSiteVO = getTRUGetSiteTypeUtil().getSiteInfo(pRequest,
				alternateSite);
		if (truSiteVO != null) {
			siteUrl = truSiteVO.getSiteURL();
		}*/
		// appending desktop view and alternateUrl
		//StringBuffer deskTopCookie = new StringBuffer();
		//deskTopCookie.append(pDesktopSiteActive);
		//deskTopCookie.append(TRUConstants.PIPE_STRING);
		//deskTopCookie.append(siteUrl);
		pRequest.setParameter(TRUConstants.IS_TRUSESSION_COOKIE, TRUConstants.TRUE);
		// creating cookie
		generateCookie(TRUConstants.DESKTOP_VIEW_COOKIE,
				pDesktopSiteActive, pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("End: createCookieForDeskTopFullSite()");
		}
	}
	
	/**
	 * This method creates the alternate domain cookie.
	 * @param pRequest  - the servlet's request.
	 * @param pResponse - the servlet's response.
	 */
	public void createAlternateDomainCookie(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {

		if (isLoggingDebug()) {
			logDebug("Start: createAlternateDomainCookie()");
		}
		if(isAlternateDomainCookie() && getTRUGetSiteTypeUtil() != null ){
				String alternateDomainUrl =  getTRUGetSiteTypeUtil().getSiteInfoForRegistry(pRequest);
				if(alternateDomainUrl!=null){
				generateCookie(TRUConstants.ALTERNATE_DOMAIN, alternateDomainUrl, pRequest, pResponse);
				}
		}
		if (isLoggingDebug()) {
			logDebug("End: createAlternateDomainCookie()");
		}
	}

	/**
	 * Generate checkout cookie.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @param pCookieName the cookie name
	 * @param pCookieValue the cookie value
	 */
	public void generateCheckoutCookie(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse ,String pCookieName,String pCookieValue){
		Site site = null;
		int maxAge=0;
		if(SiteContextManager.getCurrentSite() != null){
			site=SiteContextManager.getCurrentSite();
		}
		if(null != site && null != site.getPropertyValue(getPropertyManager().getPersistOrderDays())){
			maxAge=(int)site.getPropertyValue(getPropertyManager().getPersistOrderDays());
		}
		addCartCookie(pRequest, pResponse, pCookieName, maxAge, pCookieValue);
	}
	
	/**
	 * Gets the TRU get site type util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUGetSiteTypeUtil getTRUGetSiteTypeUtil() {
		return mTRUGetSiteTypeUtil;
	}



	/**
	 * Sets the TRU get site type util.
	 *
	 * @param pTRUGetSiteTypeUtil the new TRU get site type util
	 */
	public void setTRUGetSiteTypeUtil(TRUGetSiteTypeUtil pTRUGetSiteTypeUtil) {
		this.mTRUGetSiteTypeUtil = pTRUGetSiteTypeUtil;
	}
	
	
	/**
	 * Gets the order cookie name.
	 *
	 * @return the order cookie name.
	 */
	public String getOrderCookieName() {
		return mOrderCookieName;
	}

	/**
	 * Sets the order cookie name.
	 *
	 * @param pOrderCookieName
	 *            the new order cookie name.
	 */
	public void setOrderCookieName(String pOrderCookieName) {
		mOrderCookieName = pOrderCookieName;
	}

	/**
	 * Gets the order cookie path.
	 *
	 * @return the order cookie path.
	 */
	public String getOrderCookiePath() {
		return mOrderCookiePath;
	}

	/**
	 * Sets the order cookie path.
	 *
	 * @param pOrderCookiePath
	 *            the new order cookie path.
	 */
	public void setOrderCookiePath(String pOrderCookiePath) {
		mOrderCookiePath = pOrderCookiePath;
	}

	/**
	 * Checks if is order cookie secure.
	 *
	 * @return true, if is order cookie secure.
	 */
	public boolean isOrderCookieSecure() {
		return mOrderCookieSecure;
	}

	/**
	 * Sets the order cookie secure.
	 *
	 * @param pOrderCookieSecure
	 *            the new order cookie secure.
	 */
	public void setOrderCookieSecure(boolean pOrderCookieSecure) {
		mOrderCookieSecure = pOrderCookieSecure;
	}

	/**
	 * Checks if is order id encryption.
	 *
	 * @return true, if is order id encryption.
	 */
	public boolean isOrderIdEncryption() {
		return mOrderIdEncryption;
	}

	/**
	 * Sets the order id encryption.
	 *
	 * @param pOrderIdEncryption
	 *            the new order id encryption.
	 */
	public void setOrderIdEncryption(boolean pOrderIdEncryption) {
		mOrderIdEncryption = pOrderIdEncryption;
	}

	/**
	 * Gets the generic encryptor.
	 *
	 * @return the generic encryptor.
	 */
	public GenericEncryptor getGenericEncryptor() {
		return mGenericEncryptor;
	}

	/**
	 * Sets the generic encryptor.
	 *
	 * @param pGenericEncryptor
	 *            the new generic encryptor.
	 */
	public void setGenericEncryptor(GenericEncryptor pGenericEncryptor) {
		mGenericEncryptor = pGenericEncryptor;
	}

	/**
	 * Gets the my store cookie name.
	 *
	 * @return the myStoreCookieName.
	 */
	public String getMyStoreCookieName() {
		return mMyStoreCookieName;
	}

	/**
	 * Sets the my store cookie name.
	 *
	 * @param pMyStoreCookieName
	 *            the myStoreCookieName to set.
	 */
	public void setMyStoreCookieName(String pMyStoreCookieName) {
		this.mMyStoreCookieName = pMyStoreCookieName;
	}

	/**
	 * Gets the my store cookie path.
	 *
	 * @return the myStoreCookiePath.
	 */
	public String getMyStoreCookiePath() {
		return mMyStoreCookiePath;
	}

	/**
	 * Sets the my store cookie path.
	 *
	 * @param pMyStoreCookiePath
	 *            the myStoreCookiePath to set.
	 */
	public void setMyStoreCookiePath(String pMyStoreCookiePath) {
		this.mMyStoreCookiePath = pMyStoreCookiePath;
	}

	/**
	 * Gets the my store cookie max age.
	 *
	 * @return the myStoreCookieMaxAge.
	 */
	public int getMyStoreCookieMaxAge() {
		return mMyStoreCookieMaxAge;
	}

	/**
	 * Sets the my store cookie max age.
	 *
	 * @param pMyStoreCookieMaxAge
	 *            the myStoreCookieMaxAge to set.
	 */
	public void setMyStoreCookieMaxAge(int pMyStoreCookieMaxAge) {
		this.mMyStoreCookieMaxAge = pMyStoreCookieMaxAge;
	}

	/**
	 * Checks if is my store cookie secure.
	 *
	 * @return the myStoreCookieSecure.
	 */
	public boolean isMyStoreCookieSecure() {
		return mMyStoreCookieSecure;
	}

	/**
	 * Sets the my store cookie secure.
	 *
	 * @param pMyStoreCookieSecure the myStoreCookieSecure to set.
	 */
	public void setMyStoreCookieSecure(boolean pMyStoreCookieSecure) {
		this.mMyStoreCookieSecure = pMyStoreCookieSecure;
	}

	/**
	 * Checks if is my store id encryption.
	 *
	 * @return the myStoreIdEncryption.
	 */
	public boolean isMyStoreIdEncryption() {
		return mMyStoreIdEncryption;
	}
	
	/**
	 * check the alternate domain cookie
	 * 
	 * @return the mAlternateDomainCookie
	 */
	public boolean isAlternateDomainCookie() {
		return mAlternateDomainCookie;
	}

	/**
	 * sets the alternate domain cookie
	 * 
	 * @param pAlternateDomainCookie the alternateDomainCookie to set
	 */
	public void setAlternateDomainCookie(boolean pAlternateDomainCookie) {
		mAlternateDomainCookie = pAlternateDomainCookie;
	}

	/**
	 * Sets the my store id encryption.
	 *
	 * @param pMyStoreIdEncryption
	 *            the myStoreIdEncryption to set.
	 */
	public void setMyStoreIdEncryption(boolean pMyStoreIdEncryption) {
		this.mMyStoreIdEncryption = pMyStoreIdEncryption;
	}

	/**
	 * Gets the commerce item manager.
	 *
	 * @return the commerce item manager.
	 */
	public TRUCommerceItemManager getCommerceItemManager() {
		return mCommerceItemManager;
	}

	/**
	 * Sets the commerce item manager.
	 *
	 * @param pCommerceItemManager
	 *            the new commerce item manager.
	 */
	public void setCommerceItemManager(TRUCommerceItemManager pCommerceItemManager) {
		mCommerceItemManager = pCommerceItemManager;
	}

	/**
	 * Gets the property manager.
	 *
	 * @return the property manager.
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 *
	 * @param pPropertyManager
	 *            the new property manager.
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * Gets the date cookie name.
	 *
	 * @return the dateCookieName.
	 */
	public String getDateCookieName() {
		return mDateCookieName;
	}

	/**
	 * Sets the date cookie name.
	 *
	 * @param pDateCookieName
	 *            the dateCookieName to set.
	 */
	public void setDateCookieName(String pDateCookieName) {
		this.mDateCookieName = pDateCookieName;
	}

	/**
	 * Gets the site repository.
	 *
	 * @return the siteRepository.
	 */
	public String getSiteRepository() {
		return mSiteRepository;
	}

	/**
	 * Sets the site repository.
	 *
	 * @param pSiteRepository
	 *            the siteRepository to set.
	 */
	public void setSiteRepository(String pSiteRepository) {
		this.mSiteRepository = pSiteRepository;
	}

	/**
	 * Sets the current date.
	 *
	 * @param pCurrentDate
	 *            Sets the CurrentDate component.
	 */
	public void setCurrentDate(CurrentDate pCurrentDate) {
		mCurrentDate = pCurrentDate;
	}

	/**
	 * Gets the current date.
	 *
	 * @return currentDate.
	 */
	public CurrentDate getCurrentDate() {
		return mCurrentDate;
	}

	/**
	 * Gets the user cart cookie name.
	 *
	 * @return the user cart cookie name.
	 */
	public String getUserCartCookieName() {
		return mUserCartCookieName;
	}

	/**
	 * Sets the user cart cookie name.
	 *
	 * @param pUserCartCookieName
	 *            the new user cart cookie name.
	 */
	public void setUserCartCookieName(String pUserCartCookieName) {
		mUserCartCookieName = pUserCartCookieName;
	}

	/**
	 * @return the newSession
	 */
	public boolean isNewSession() {
		return mNewSession;
	}

	/**
	 * @param pNewSession the newSession to set
	 */
	public void setNewSession(boolean pNewSession) {
		mNewSession = pNewSession;
	}
}
