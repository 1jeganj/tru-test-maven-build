package com.tru.userprofiling;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import atg.apache.soap.encoding.soapenc.Base64;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.ForgotPasswordHandler;
import atg.userprofiling.PropertyManager;

import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.cml.SystemException;
import com.tru.common.cml.ValidationExceptions;
import com.tru.errorhandler.fhl.IErrorHandler;
import com.tru.messaging.TRUEpslonPasswordResetSource;
import com.tru.regex.EmailPasswordValidator;
import com.tru.utils.TRUIntegrationStatusUtil;
import com.tru.validator.fhl.IValidator;

/**
 * Extending forgot password handler.
 * @author Professional Access
 * @version 1.0
 */
public class TRUForgotPasswordHandler extends ForgotPasswordHandler{
	
	/**
	 * List to hold ERRORKEYS.
	 */
	private List<String> mErrorKeysList;
	/**
	 *  property to hold RepeatingRequestMonitor.
	 */
	private RepeatingRequestMonitor mRepeatingRequestMonitor;
	/**
	 *  property to hold SuccessErrorUrlMap.
	 */
	private Map<String, String> mSuccessErrorUrlMap;
	
	/**
	 * property to hold validator.
	 */
	private IValidator mValidator;
	
	/**
	 * property to hold error handler.
	 */
	private IErrorHandler mErrorHandler;
	
	/**
	 * ValidationExceptions object.
	 */
	private ValidationExceptions mVE = new ValidationExceptions();
    
    /**
   	 *  property to hold passwordResetSource.
   	 */
   	private TRUEpslonPasswordResetSource mPasswordResetSource;
   	
   	/**
 	 *  property to hold mTRUConfiguration.
 	 */
 	private TRUConfiguration mTRUConfiguration;
 	
 	/**
	 * Property to holds mEmailPasswordValidator.
	 */
	 private EmailPasswordValidator mEmailPasswordValidator;
	 /**
	 * Property to holds mForgotPasswordSuccessURL.
	 */
	 private String mForgotPasswordSuccessURL;
	 /**
	 * Property to holds mForgotPasswordErrorURL.
	 */
	 private String mForgotPasswordErrorURL;
	 /**
		 * Property to holds mResetPasswordURL.
	 */
	 private String mResetPasswordURL;
	 /**
		 * Property to holds mMyAccountURL.
	 */
	 private String mMyAccountURL;
	 /**
	 *  property to hold integrationStatusUtil.
	 */
	private TRUIntegrationStatusUtil mIntegrationStatusUtil;

	/**
	 * Custom method for handle forgot password.
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 * @return boolean
	 */
    public boolean handleForgotPassword(DynamoHttpServletRequest pRequest,
		      DynamoHttpServletResponse pResponse)
		  throws ServletException, IOException
		  {
	    	if(isLoggingDebug()){
				logDebug("Start: TRUForgotPasswordHandler.handleForgotPassword()");
			}
			RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
			String myHandleMethod = "TRUForgotPasswordHandler.handleForgotPassword";
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = getTransactionDemarcation();
			boolean lRollbackFlag = false;
			if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
				try {
					PropertyManager lPropertyManager = getProfileTools().getPropertyManager();
					String email = getStringValueProperty(lPropertyManager.getEmailAddressPropertyName()).toLowerCase().trim();
					setValueProperty(lPropertyManager.getEmailAddressPropertyName(), email);
					preForgotPassword(pRequest, pResponse);
					if(getFormError() || (tm == null)){
						return checkFormRedirect(getForgotPasswordSuccessURL(),getForgotPasswordErrorURL(), pRequest, pResponse);
					} else {
						td.begin(tm, TransactionDemarcation.REQUIRED);
						super.handleForgotPassword(pRequest, pResponse);
						String lResetPasswordURL = TRUConstants.EMPTY_STRING;
						String resetPwdURLWithoutParam = ((TRUProfileTools)getProfileTools()).generateURL(pRequest,getResetPasswordURL());
						String lEncryptedEmail = Base64.encode(email.getBytes());
						String serverTimeStamp = ((TRUProfileTools) getProfileTools()).getServerTimeStamp();
						String encodedTimeStamp = Base64.encode(serverTimeStamp.getBytes());
						if(resetPwdURLWithoutParam.contains(TRUConstants.QUESTION_MARK_STRING)){
							lResetPasswordURL = resetPwdURLWithoutParam + 
									TRUConstants.AMPERSAND_STRING + TRUConstants.EMAIL_PARAM_NAME + TRUConstants.EQUALS_SYMBOL_STRING + lEncryptedEmail
									+ TRUConstants.AMPERSAND_STRING + TRUConstants.TIMESTAMP_PARAM_NAME + TRUConstants.EQUALS_SYMBOL_STRING + encodedTimeStamp;
						} else {
							lResetPasswordURL = resetPwdURLWithoutParam + 
									TRUConstants.QUESTION_MARK_STRING + TRUConstants.EMAIL_PARAM_NAME + TRUConstants.EQUALS_SYMBOL_STRING + lEncryptedEmail
									+ TRUConstants.AMPERSAND_STRING + TRUConstants.TIMESTAMP_PARAM_NAME + TRUConstants.EQUALS_SYMBOL_STRING + encodedTimeStamp;
						}
						String lMyAccountURL = ((TRUProfileTools)getProfileTools()).generateURL(pRequest,getMyAccountURL());
						boolean isEpslonEnabled = Boolean.FALSE;
            			isEpslonEnabled = getIntegrationStatusUtil().getEpslonIntStatus(pRequest);
						if (!isEpslonEnabled) {
							if(isLoggingDebug()){
								logDebug("Epslon service is disabled");
							}
						}else{
								if (!StringUtils.isBlank(email) && getPasswordResetSource() != null) {
									try {
										getPasswordResetSource().sendPasswordResetEmail(email, lMyAccountURL, lResetPasswordURL );
									} catch (JMSException e) {
										vlogError("JMS EXCEPTION", e);
									}
								}else{
									if(isLoggingDebug()){
										logDebug("PasswordResetSource is NULL or Email Address is Empty");
									}
								}
							}
						}
					}
				  catch (TransactionDemarcationException e) {
					lRollbackFlag = true;
					vlogError("TransactionDemarcationException occured while handling forgot password with exception {0}", e);
				} finally {
					try {
						td.end(lRollbackFlag);
					} catch (TransactionDemarcationException tD) {
						vlogError("TransactionDemarcationException occured while handling forgot password with exception {0}", tD);
					}
					if (rrm != null) {
						rrm.removeRequestEntry(myHandleMethod);
					}
				}
			}
			if(isLoggingDebug()){
				logDebug("End: TRUForgotPasswordHandler.handleForgotPassword()");
			}
			return checkFormRedirect(getForgotPasswordSuccessURL(),getForgotPasswordErrorURL(), pRequest, pResponse);
		  }
	
	/**
	 * pre method to perform validations for handle forgot password.
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
    protected void preForgotPassword(DynamoHttpServletRequest pRequest,
		      DynamoHttpServletResponse pResponse)throws ServletException, IOException{
    	if(isLoggingDebug()){
			logDebug("Start: TRUForgotPasswordHandler.preFogotPassword()");
		}
		try {	
			PropertyManager lPropertyManager = getProfileTools().getPropertyManager();
			String email = getStringValueProperty(lPropertyManager.getEmailAddressPropertyName());
			getValidator().validate(TRUConstants.FORGOT_PASSWORD_FORM, this);
			if(StringUtils.isBlank(email)){
				mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_FORGOT_PASSWORD_MISSING_MANDATORY_INFO, null);   
				getErrorHandler().processException(mVE, this);
			}
			else{
				if(!getEmailPasswordValidator().validateEmail(email)){
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_FORGOT_PASSWORD_INVALID_EMAIL, null);   
					getErrorHandler().processException(mVE, this);
				}
				else if (!isProfileExists(email)){
					if(getErrorKeysList().contains(TRUErrorKeys.TRU_ERROR_ACCOUNT_EMAIL_INVALID_USER)){
						mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_EMAIL_INVALID_USER, null);   
						getErrorHandler().processException(mVE, this);
					}else{
						mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_FORGOT_PASSWORD_INVALID_USER, null);   
						getErrorHandler().processException(mVE, this);
					}
				}
			}
       }catch (ValidationExceptions ve) {
           	getErrorHandler().processException(ve, this);
       		vlogError("Validation exception occured: {0}", ve);
       }catch (SystemException se) {
    	   	getErrorHandler().processException(se, this);
       		vlogError("System exception occured: {0}", se);
       }
		if(isLoggingDebug()){
			logDebug("End: TRUForgotPasswordHandler.preFogotPassword()");
		}
	}
	
	/**
	 * This method checks if the email is already registered, if not form exception will be added.
	 * @param pEmail email
	 * @throws ServletException ServletException
	 * @return isProfileExists type boolean
	 */
    public boolean isProfileExists(String pEmail) throws ServletException{
    	boolean isProfileExists = true;
		RepositoryItem lUser = getProfileTools().getItemFromEmail(pEmail);
		if(lUser == null){
			isProfileExists = false;
		}
		return isProfileExists;
	}
	
	/**
	 * This method is used to get the error URL and Success URL from the map.
	 * @param pValue value of success or error url
	 * @return string for the url
	 */
	public String getSuccessOrErrorURL(String pValue){
		if(isLoggingDebug()){
			logDebug("Start: TRUProfileFormHandler.getSuccessOrErrorURL() ");
		}
		vlogDebug("Success or Error value: {0}", pValue);
		String url = null;
		if(StringUtils.isBlank(pValue)){
			vlogDebug("Success or Error URL value is null : {0}", pValue);
		} else{
			url = getSuccessErrorUrlMap().get(pValue);
		}
		if(isLoggingDebug()){
			logDebug("End: TRUProfileFormHandler.getSuccessOrErrorURL() ");
		}
		return url;
	}
	
	/**
	 * @return the mRepeatingRequestMonitor
	 */
	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	/**
	 * @param pRepeatingRequestMonitor the RepeatingRequestMonitor to set
	 */
	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		this.mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}
	
	/**
	 * @return the mSuccessErrorUrlMap
	 */
	public Map<String, String> getSuccessErrorUrlMap() {
		return mSuccessErrorUrlMap;
	}

	/**
	 * @param pSuccessErrorUrlMap the mSuccessErrorUrlMap to set
	 */
	public void setSuccessErrorUrlMap(Map<String, String> pSuccessErrorUrlMap) {
		this.mSuccessErrorUrlMap = pSuccessErrorUrlMap;
	}

	/**
	 * @return the mValidator
	 */
	public IValidator getValidator() {
		return mValidator;
	}

	/**
	 * @param pValidator the mValidator to set
	 */
	public void setValidator(IValidator pValidator) {
		this.mValidator = pValidator;
	}

	/**getter method for errorHandler.
	 * @return the mErrorHandler
	 */
	public IErrorHandler getErrorHandler() {
		return mErrorHandler;
	}

	/**setter method for errorHandler.
	 * @param pErrorHandler the mErrorHandler to set
	 */
	public void setErrorHandler(IErrorHandler pErrorHandler) {
		this.mErrorHandler = pErrorHandler;
	}
	
	/**getter method for email.
    *  @return the email
    */
    public String getEmail() {
    	return (String) (getValue()).get(getProfileTools().getPropertyManager().getEmailAddressPropertyName());
    }
    
    /**getter method for passwordResetSource.
	 * @return the passwordResetSource
	 */
	public TRUEpslonPasswordResetSource getPasswordResetSource() {
		return mPasswordResetSource;
	}

	/**setter method for passwordResetSource.
	 * @param pPasswordResetSource the passwordResetSource to set
	 */
	public void setPasswordResetSource(
			TRUEpslonPasswordResetSource pPasswordResetSource) {
		mPasswordResetSource = pPasswordResetSource;
	}

	/**getter method for mTRUConfiguration.
	 * @return the mTRUConfiguration
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * @param pTRUConfiguration the mTRUConfiguration to set
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		mTRUConfiguration = pTRUConfiguration;
	}
	 
	/**
	 * @return the emailPasswordValidator
	 */
	public EmailPasswordValidator getEmailPasswordValidator() {
		return mEmailPasswordValidator;
	}

	/**
	 * @param pEmailPasswordValidator the emailPasswordValidator to set
	 */
	public void setEmailPasswordValidator(EmailPasswordValidator pEmailPasswordValidator) {
		this.mEmailPasswordValidator = pEmailPasswordValidator;
	}

	/**
	 * 
	 * @return the mForgotPasswordSuccessURL
	 */
	public String getForgotPasswordSuccessURL() {
		return mForgotPasswordSuccessURL;
	}

	/**
	 * @param pForgotPasswordSuccessURL the mForgotPasswordSuccessURL to set
	 */
	public void setForgotPasswordSuccessURL(String pForgotPasswordSuccessURL) {
		this.mForgotPasswordSuccessURL = pForgotPasswordSuccessURL;
	}

	/**
	 * @return the mForgotPasswordErrorURL
	 */
	public String getForgotPasswordErrorURL() {
		return mForgotPasswordErrorURL;
	}

	/**
	 * @param pForgotPasswordErrorURL the mForgotPasswordErrorURL to set
	 */
	public void setForgotPasswordErrorURL(String pForgotPasswordErrorURL) {
		this.mForgotPasswordErrorURL = pForgotPasswordErrorURL;
	}
	/**
	 * @return the mResetPasswordURL
	 */
	public String getResetPasswordURL() {
		return mResetPasswordURL;
	}
	/**
	 * @param pResetPasswordURL the mResetPasswordURL to set
	 */
	public void setResetPasswordURL(String pResetPasswordURL) {
		this.mResetPasswordURL = pResetPasswordURL;
	}
	/**
	 * @return the mMyAccountURL
	 */
	public String getMyAccountURL() {
		return mMyAccountURL;
	}
	/**
	 * @param pMyAccountURL the mMyAccountURL to set
	 */
	public void setMyAccountURL(String pMyAccountURL) {
		this.mMyAccountURL = pMyAccountURL;
	}

	/**
	 * @return the integrationStatusUtil
	 */
	public TRUIntegrationStatusUtil getIntegrationStatusUtil() {
		return mIntegrationStatusUtil;
	}

	/**
	 * @param pIntegrationStatusUtil the integrationStatusUtil to set
	 */
	public void setIntegrationStatusUtil(TRUIntegrationStatusUtil pIntegrationStatusUtil) {
		mIntegrationStatusUtil = pIntegrationStatusUtil;
	}

	/**
	 * @return the errorKeysList
	 */
	public List<String> getErrorKeysList() {
		return mErrorKeysList;
	}

	/**
	 * @param pErrorKeysList the errorKeysList to set
	 */
	public void setErrorKeysList(List<String> pErrorKeysList) {
		mErrorKeysList = pErrorKeysList;
	}
	
}
