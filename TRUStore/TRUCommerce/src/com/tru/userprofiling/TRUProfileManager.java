package com.tru.userprofiling;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.ServletException;

import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;

import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConstants;
import com.tru.utils.TRUIntegrationStatusUtil;

/**
 * This class is a manager class which will be used to have the business logic methods or the profile repository related methods
 * in order to update or fetch information.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUProfileManager extends GenericService {

	/** The Constant SEQ. */
	static final AtomicInteger SEQ = new AtomicInteger();

	/** The Property manager. */
	private TRUPropertyManager mPropertyManager;

	/**
	 * property to hold Profile Tools.
	 */
	private TRUProfileTools mProfileTools;

	/**
	 * Validate address duplicate nickname.
	 * 
	 * @param pProfile
	 *            the profile RepositoryItem
	 * @param pNickName
	 *            the nickname
	 * @return boolean true if the name is duplicate
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean validateDuplicateAddressNickname(RepositoryItem pProfile, String pNickName) throws ServletException,
			IOException {
		vlogDebug("Start of TRUProfileManager.validateDuplicateAddressNickname()");
		boolean duplicateNickname = false;
		if (!StringUtils.isBlank(pNickName) && pProfile != null) {
			duplicateNickname = ((TRUProfileTools) getProfileTools()).isDuplicateAddressNickName(pProfile, pNickName);
		}
		vlogDebug("End of TRUProfileManager.validateDuplicateAddressNickname()");
		return duplicateNickname;
	}

	/**
	 * This method checks the card expiration.
	 * 
	 * @param pExpireMonth
	 *            - String
	 * @param pExpirationYear
	 *            - String
	 * @return boolean true if the card is expired
	 */
	public boolean validateCardExpiration(String pExpireMonth, String pExpirationYear) {
		vlogDebug("Start of TRUProfileManager.validateCardExpiration()");
		boolean isCardValid = false;
		if (!StringUtils.isBlank(pExpirationYear) && !StringUtils.isBlank(pExpireMonth)) {
			isCardValid = ((TRUProfileTools) getProfileTools()).isValidateCardExpiration(pExpireMonth, pExpirationYear);
		}
		vlogDebug("End of TRUProfileManager.validateCardExpiration()");
		return isCardValid;
	}

	/**
	 * Validate denali account num.
	 * 
	 * @param pRewardNumber
	 *            rewardNumber
	 * @return boolean
	 * @This method validates the reward membershipCard number.
	 */
	public boolean validateDenaliAccountNum(String pRewardNumber) {
		boolean isValid = false;
		int charPos = TRUConstants.ZERO;
		int evensum = TRUConstants.ZERO;
		int oddsum = TRUConstants.ZERO;
		int csum = TRUConstants.ZERO;
		int check = TRUConstants.ZERO;
		String rewardNumberSubStr = TRUConstants.EMPTY;

		if (pRewardNumber.length() == TRUConstants.THIRTEEN
				&& pRewardNumber.substring(TRUConstants.ZERO, TRUConstants.TWO).equals(TRUConstants.TWENTY_ONE)) {
			csum = Integer.parseInt(TRUConstants.DOUBLE_QUOTE + pRewardNumber.charAt(TRUConstants.TWELVE));
			if (pRewardNumber != null && pRewardNumber.length() == TRUConstants.THIRTEEN) {
				rewardNumberSubStr = pRewardNumber.substring(TRUConstants.ZERO, TRUConstants.TWELVE);
			}
			if (rewardNumberSubStr == null || rewardNumberSubStr.length() != TRUConstants.TWELVE) {
				return false;
			}

			for (int i = rewardNumberSubStr.length() - TRUConstants.SIZE_ONE; i >= TRUConstants.ZERO; i--) {
				charPos = ((rewardNumberSubStr.length() - i) - TRUConstants.SIZE_ONE);
				if ((rewardNumberSubStr.length() - i) % TRUConstants.TWO == TRUConstants.ZERO) {
					evensum = evensum + Integer.parseInt(TRUConstants.DOUBLE_QUOTE + rewardNumberSubStr.charAt(charPos));
				} else {
					oddsum = oddsum + Integer.parseInt(TRUConstants.DOUBLE_QUOTE + rewardNumberSubStr.charAt(charPos));
				}
			}
			check = TRUConstants.TEN - ((evensum * TRUConstants.THREE + oddsum) % TRUConstants.TEN);
			if (check >= TRUConstants.TEN) {
				check = TRUConstants.ZERO;
			}
			isValid = (csum == check);
		}
		return isValid;
	}

	/**
	 * getter method for profile tools.
	 * 
	 * @return mProfileTools
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * Gets the property manager.
	 * 
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 * 
	 * @param pPropertyManager
	 *            the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * setter method for profile tools.
	 * 
	 * @param pProfileTools
	 *            TRUProfileTools
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		this.mProfileTools = pProfileTools;
	}

	/**
	 * create the giftFinder map and store in DB.
	 * 
	 * @param pGiftFinderName
	 *            giftFinderName
	 * @param pGiftFinderNameURL
	 *            giftFinderNameURL
	 * @param pProfile
	 *            profile
	 */

	public void giftFinder(String pGiftFinderName, String pGiftFinderNameURL, Profile pProfile) {
		if (!pProfile.isTransient()) {
			@SuppressWarnings("unchecked")
			Map<String, String> giftMap = (Map<String, String>) pProfile
					.getPropertyValue(getPropertyManager().getGiftFinderURL());
			int nextval = TRUConstants.ONE;
			if (giftMap.isEmpty()) {
				giftMap = new HashMap<String, String>();
				nextval = TRUConstants.ONE;
			} else {
				for (String key : giftMap.keySet()) {
					if (key.contains(TRUConstants.GIFT_ID)) {
						int count = nextval + TRUConstants.ONE;
						nextval = count;
					}
				}
			}
			String uniqueId = TRUConstants.EMPTY;
			if (StringUtils.isBlank(pGiftFinderName) && !StringUtils.isBlank(pGiftFinderNameURL)) {
				uniqueId = TRUConstants.GIFT_ID + nextval;
				giftMap.put(uniqueId, pGiftFinderNameURL);
			} else {
				giftMap.put(pGiftFinderName, pGiftFinderNameURL);
			}

			pProfile.setPropertyValue(getPropertyManager().getGiftFinderURL(), giftMap);

		}
	}

	/**
	 * Next sequence id.
	 * 
	 * @param pSeq
	 *            the seq
	 * @return the int
	 */
	public static int nextSequenceId(AtomicInteger pSeq) {
		Integer nextVal = pSeq.incrementAndGet();
		return nextVal;
	}

	/**
	 * delete the giftFinder and delete from DB.
	 * 
	 * @param pGiftFinderName
	 *            giftFinderName
	 * @param pProfile
	 *            profile
	 */
	public void deleteGiftFinder(String pGiftFinderName, Profile pProfile) {
		if (!pProfile.isTransient()) {
			@SuppressWarnings("unchecked")
			final Map<String, String> giftMap = (Map<String, String>) pProfile.getPropertyValue(getPropertyManager()
					.getGiftFinderURL());
			if (!giftMap.isEmpty()) {
				giftMap.remove(pGiftFinderName);
				pProfile.setPropertyValue(getPropertyManager().getGiftFinderURL(), giftMap);
			}
		}
	}

	/**
	 * delete the giftFinder from Profile.
	 * 
	 * @param pProfile
	 *            profile
	 */
	public void clearGiftFinderFromProfile(Profile pProfile) {
		if (!pProfile.isTransient()) {
			@SuppressWarnings("unchecked")
			final Map<String, String> giftMap = (Map<String, String>) pProfile.getPropertyValue(getPropertyManager()
					.getGiftFinderURL());
			giftMap.clear();
			pProfile.setPropertyValue(getPropertyManager().getGiftFinderURL(), giftMap);
		}
	}

	/**
	 * This method is to check whether password and confirm password match.
	 * 
	 * @param pPassword
	 *            String
	 * @param pConfirmPassword
	 *            String
	 * @return boolean
	 * @throws ServletException
	 *             ServletException
	 */
	public boolean validatePasswordMatch(String pPassword, String pConfirmPassword) throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.validatePasswordMatch()");
		}
		boolean passwordMatch = true;
		if (!pPassword.equals(pConfirmPassword)) {
			passwordMatch = false;
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.validatePasswordMatch()");
		}
		return passwordMatch;
	}

	/**
	 * Populate contact info.
	 * 
	 * @param pContactInfo
	 *            - ContactInfo
	 * @param pAddressValue
	 *            AddressValue
	 * @return ContactInfo
	 */
	public ContactInfo populateContactInfo(ContactInfo pContactInfo, Map<String, String> pAddressValue) {
		if (isLoggingDebug()) {
			logDebug("Start of TRUProfileFormHandler/populateContactInfo method");
		}
		TRUPropertyManager propertyManager = getPropertyManager();

		pContactInfo.setAddress1(pAddressValue.get(propertyManager.getAddressLineOnePropertyName()));
		pContactInfo.setAddress2(pAddressValue.get(propertyManager.getAddressLineTwoPropertyName()));
		pContactInfo.setCity(pAddressValue.get(propertyManager.getAddressCityPropertyName()));
		pContactInfo.setState(pAddressValue.get(propertyManager.getAddressStatePropertyName()));
		pContactInfo.setFirstName(pAddressValue.get(propertyManager.getAddressFirstNamePropertyName()));
		pContactInfo.setLastName(pAddressValue.get(propertyManager.getAddressLastNamePropertyName()));
		pContactInfo.setCountry(pAddressValue.get(propertyManager.getAddressCountryPropertyName()));
		pContactInfo.setPhoneNumber(pAddressValue.get(propertyManager.getAddressPhoneNumberPropertyName()));
		pContactInfo.setPostalCode(pAddressValue.get(propertyManager.getAddressPostalCodePropertyName()));

		if (isLoggingDebug()) {
			logDebug("End of TRUProfileFormHandler/populateContactInfo method");
		}

		return pContactInfo;

	}

	/**
	 * This method is to check whether old password typed by user is valid.
	 * 
	 * @param pOldPassword
	 *            oldPassword
	 * @param pProfile
	 *            Profile
	 * @return boolean
	 * @throws ServletException
	 *             ServletException
	 */
	public boolean isValidOldPassword(String pOldPassword, Profile pProfile) throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.isValidOldPassword()");
		}
		boolean isValidOldPassword = true;
		String login = null;
		if (pProfile != null) {
			login = (String) pProfile.getPropertyValue(getPropertyManager().getLoginPropertyName());
		}
		if (!getProfileTools().isValidCredentials(login, pOldPassword)) {
			isValidOldPassword = false;
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.isValidOldPassword()");
		}
		return isValidOldPassword;
	}

	/**
	 * This method is for checking whether email and confirm email provided by user match.
	 * 
	 * @param pEmailAddress
	 *            emailAddress
	 * @param pConfirmEmail
	 *            confirmEmail
	 * @return boolean success/fail
	 */
	public boolean validateEmailMatch(String pEmailAddress, String pConfirmEmail) {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.validateEmailMatch()");
		}
		boolean isValid = false;
		if (pEmailAddress.equals(pConfirmEmail)) {
			isValid = true;
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.validateEmailMatch()");
		}
		return isValid;
	}

	/**
	 * property to hold integrationStatusUtil.
	 */
	private TRUIntegrationStatusUtil mIntegrationStatusUtil;

	/**
	 * Gets the integration status util.
	 * 
	 * @return the integrationStatusUtil
	 */
	public TRUIntegrationStatusUtil getIntegrationStatusUtil() {
		return mIntegrationStatusUtil;
	}

	/**
	 * Sets the integration status util.
	 * 
	 * @param pIntegrationStatusUtil
	 *            the integrationStatusUtil to set
	 */
	public void setIntegrationStatusUtil(TRUIntegrationStatusUtil pIntegrationStatusUtil) {
		mIntegrationStatusUtil = pIntegrationStatusUtil;
	}
}