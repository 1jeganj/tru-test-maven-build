package com.tru.cache;

import javax.jms.JMSException;
import javax.jms.Message;

import atg.dms.patchbay.MessageSink;
import atg.service.cache.Cache;

import com.tru.commerce.exception.TRUCommerceException;


/**
 * This class extends OOTB service Cache and implements patch bay systems message sink interface. This class is written
 * to hold the Sku pricing objects in cache.
 * 
 * 
 * This class implements OOTB Message Sink interface for flushing its cache content. Whenever a message is received in
 * receiveMessage() it triggers Cache Flush.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUSkuPricingCache extends Cache implements MessageSink{
	
	/**
	 * To hold serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Receive Message
	 *
	 * @param pParamString the param string
	 * @param pParamMessage the param message
	 * @throws JMSException the JMS exception
	 */
	@Override
	public void receiveMessage(String pParamString, Message pParamMessage) throws JMSException {
		if (isLoggingDebug()) {
			vlogDebug("TRUSkuPricingCache.receiveMessage method.. pParamString : {0},  "+ "pParamMessage : {1}", pParamString, pParamMessage);
		}
		flush();
		
	}
	
	/**
	 * This method is used to get the value form cache Object.
	 * 
	 * @param pKey - Value of key object.
	 * @return Object - return the Object values.
	 * @throws TRUCommerceException - to throw the TRUCommerceException.
	 */
	@Override
	public Object get(Object pKey) throws TRUCommerceException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUSkuPricingCache.get method.. pKey : {0}", pKey);
		}
		Object value = null;
		try {
			value = super.get(pKey);
		} catch (Exception exception) {
			throw new TRUCommerceException(exception);
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUSkuPricingCache.get method.. value : {0}", value);
		}
		return value;
	}

	/**
	 * This method is used to get the value form cache Object.
	 *
	 * @param pKeys the keys
	 * @return Object - return the Object values.
	 * @throws Exception the exception
	 */
	public Object[] get(Object[] pKeys) throws Exception {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUSkuPricingCache.get method.. pKey : {0}", pKeys);
		}
		Object[] values = null;
		values = super.get(pKeys);
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUSkuPricingCache.get method.. value : {0}", values);
		}
		return values;

	}

}
