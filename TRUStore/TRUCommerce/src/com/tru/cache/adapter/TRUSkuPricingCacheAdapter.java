package com.tru.cache.adapter;

import java.util.Map;

import atg.nucleus.GenericService;
import atg.service.cache.CacheAdapter;

import com.tru.commerce.exception.TRUCommerceException;
import com.tru.utils.TRUEndecaPriceUtils;

/**
 * This service is an implementation of CacheAdapter that caches sku pricing Objects for purpose of showing the pricing on
 * all the pages.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUSkuPricingCacheAdapter extends GenericService implements CacheAdapter{
	
	/** Property to hold is mEndecaUtils. */
	private TRUEndecaPriceUtils mEndecaUtils;
	/**
	 * @return the endecaUtils
	 */
	public TRUEndecaPriceUtils getEndecaUtils() {
		return mEndecaUtils;
	}
	/**
	 * @param pEndecaUtils the endecaUtils to set
	 */
	public void setEndecaUtils(TRUEndecaPriceUtils pEndecaUtils) {
		mEndecaUtils = pEndecaUtils;
	}
	
	/**
	 * This method invokes to get the sku price related information.
	 * 
	 * @param pKey - key
	 * @return Object - Object
	 * @throws TRUCommerceException - If exception occurs.
	 */
	@Override
	public Object getCacheElement(Object pKey) throws Exception {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUSkuPricingCacheAdapter.getCacheElement method.. pKey : {0}", pKey);
		}
		
		Map<String, Object> skuPriceMap = null;
		
		if(pKey instanceof String){
			String pSkuProds=(String)pKey;
			skuPriceMap = getEndecaUtils().getSKUPrice(pSkuProds);
		}
		
		return skuPriceMap;
	}

	/**
	 * Used to find the cache element size.
	 * 
	 * @param pParamObject1 - First object
	 * @param pParamObject2 - Second object
	 * @return int - Return integer value
	 */
	@Override
	public int getCacheElementSize(Object pParamObject1, Object pParamObject2) {
		return 0;
	}

	/**
	 * Used to find the cache key size.
	 * 
	 * @param pParamObject - Object
	 * @return int - Return integer value
	 */
	@Override
	public int getCacheKeySize(Object pParamObject) {
		return 0;
	}

	/**
	 * Used to remove cache element.
	 * 
	 * @param pParamObject1 - First object
	 * @param pParamObject2 - Second object
	 */
	@Override
	public void removeCacheElement(Object pParamObject1, Object pParamObject2) {
		return;
	}

	/**
	 * Get cache elements.
	 * 
	 * @param pParamArrayOfObject
	 *            the param array of object
	 * @return the cache elements
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public Object[] getCacheElements(Object[] pParamArrayOfObject) throws Exception {
		return null;
	}

}
