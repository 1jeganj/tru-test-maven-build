package com.tru.cache.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.service.cache.CacheAdapter;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogManager;
import com.tru.commerce.catalog.vo.CollectionProductInfoRequestVO;
import com.tru.commerce.catalog.vo.CollectionProductInfoVO;
import com.tru.commerce.exception.TRUCommerceException;

/**
 * This service is an implementation of CacheAdapter that caches product Objects for purpose of showing the content on product details page.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUCollectionProductCacheAdapter extends GenericService implements CacheAdapter {

	/**
	 * Holds the catalog manager.
	 */
	private TRUCatalogManager mCatalogManager;

	/**
	 * This method invokes to get the collection product related information.
	 * 
	 * @param pKey - Product request VO
	 * @return Object - Object retrieved from SEORepository.
	 * @throws TRUCommerceException - If exception occurs.
	 */
	@Override
	public Object getCacheElement(Object pKey) throws TRUCommerceException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUCollectionProductCacheAdapter.getCacheElement method.. pKey : {0}", pKey);
		}
		CollectionProductInfoVO collectionProductInfo = null;
		if (pKey instanceof CollectionProductInfoRequestVO) {
			CollectionProductInfoRequestVO requestVO = (CollectionProductInfoRequestVO) pKey;
			if (StringUtils.isBlank(requestVO.getCollectionId())) {
				return collectionProductInfo;
			}
			try {
				collectionProductInfo = getCatalogManager().createCollectionProductInfo(requestVO.getCollectionId(), false);
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					logError("RepositoryException in TRUProductCacheAdapter.getCacheElement method. ",
							repositoryException);
				}
			}
		}
		
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUCollectionProductCacheAdapter.getCacheElement method.. pKey : {0}", pKey);
		}
		return collectionProductInfo; 
	}

	/**
	 * This method invokes the ENDECA interface for querying results from ENDECA.
	 * 
	 * @param pKeys - Request Objects understood by ENDECA interface.
	 * @return Object[] - Objects retrieved from ENDECA interface.
	 * @throws TRUCommerceException - If exception occurs at the ENDECA end.
	 */
	@SuppressWarnings({"null", "unchecked"})
	@Override
	public Object[] getCacheElements(Object[] pKeys) throws TRUCommerceException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUCollectionProductCacheAdapter.getCacheElements method.. pKeys : {0}", pKeys);
		}

		List<Map<String, String>> seoMetaInfos = null;
		if (pKeys == null || pKeys.length < TRUCommerceConstants.INT_ONE) {
			if (isLoggingDebug()) {
				logDebug("Empty keys");
			}
			return seoMetaInfos.toArray();
		}

		Map<String, String> seoMetaInfo = null;
		for (Object key : pKeys) {
			seoMetaInfo = (Map<String, String> ) getCacheElement(key);
			if (seoMetaInfo != null && !seoMetaInfo.isEmpty()) {
				if (seoMetaInfos == null) {
					seoMetaInfos = new ArrayList<Map<String, String>>(pKeys.length);
				}
				seoMetaInfos.add(seoMetaInfo);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCollectionProductCacheAdapter.getCacheElements method..");
		}
		return seoMetaInfos.toArray();

	}

	/**
	 * Used to find the cache element size.
	 * 
	 * @param pParamObject1 - First object
	 * @param pParamObject2 - Second object
	 * @return int - Return integer value
	 */
	@Override
	public int getCacheElementSize(Object pParamObject1, Object pParamObject2) {
		return 0;
	}

	/**
	 * Used to find the cache key size.
	 * 
	 * @param pParamObject - Object
	 * @return int - Return integer value
	 */
	@Override
	public int getCacheKeySize(Object pParamObject) {
		return 0;
	}

	/**
	 * Used to remove cache element.
	 * 
	 * @param pParamObject1 - First object
	 * @param pParamObject2 - Second object
	 */
	@Override
	public void removeCacheElement(Object pParamObject1, Object pParamObject2) {
		return;
	}

	/**
	 * Returns the holds the catalog manager.
	 * 
	 * @return the catalogManager
	 */
	public TRUCatalogManager getCatalogManager() {
		return mCatalogManager;
	}

	/**
	 * Sets the holds the catalog manager.
	 * 
	 * @param pCatalogManager the catalogManager to set
	 */
	public void setCatalogManager(TRUCatalogManager pCatalogManager) {
		mCatalogManager = pCatalogManager;
	}
}
