package com.tru.cache.adapter;

import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.service.cache.CacheAdapter;

import com.tru.commerce.exception.TRUCommerceException;
import com.tru.common.vo.TRUPromoContainer;

/**
 * This service is an implementation of CacheAdapter that caches product Objects for purpose of showing the content on
 * product details page.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUPromotionCacheAdapter extends GenericService implements CacheAdapter {

	/**
	 * Holds the mPromotionTools.
	 */
	private TRUPromotionTools mPromotionTools;

	/**
	 * This method invokes to get the product related information.
	 * 
	 * @param pKey - Product request VO
	 * @return Object - Object retrieved from SEORepository.
	 * @throws TRUCommerceException - If exception occurs.
	 */
	@Override
	public Object getCacheElement(Object pKey) throws TRUCommerceException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUPromotionCacheAdapter.getCacheElement method.. pKey : {0}", pKey);
		}
		TRUPromoContainer promoContainer=null;
		if(pKey instanceof TRUPromoContainer){
			TRUPromoContainer truPromoContainer=(TRUPromoContainer)pKey;
			if (StringUtils.isBlank(truPromoContainer.getSkuId())) {
				return truPromoContainer;
			}
			promoContainer=getPromotionTools().getPromotionDetailsForSKU(truPromoContainer.getSkuId(),truPromoContainer.getPromotionId(),false);
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUPromotionCacheAdapter.getCacheElement method.. pKey : {0}", pKey);
		}
		return promoContainer;
	}


	/**
	 * Used to find the cache element size.
	 * 
	 * @param pParamObject1 - First object
	 * @param pParamObject2 - Second object
	 * @return int - Return integer value
	 */
	@Override
	public int getCacheElementSize(Object pParamObject1, Object pParamObject2) {
		return 0;
	}

	/**
	 * Used to find the cache key size.
	 * 
	 * @param pParamObject - Object
	 * @return int - Return integer value
	 */
	@Override
	public int getCacheKeySize(Object pParamObject) {
		return 0;
	}

	/**
	 * Used to remove cache element.
	 * 
	 * @param pParamObject1 - First object
	 * @param pParamObject2 - Second object
	 */
	@Override
	public void removeCacheElement(Object pParamObject1, Object pParamObject2) {
		return;
	}
	/**
	 * @return the mPromotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * @param pPromotionTools the mPromotionTools to set
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		this.mPromotionTools = pPromotionTools;
	}

	/**
	 * Get cache elements.
	 * 
	 * @param pParamArrayOfObject
	 *            the param array of object
	 * @return the cache elements
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public Object[] getCacheElements(Object[] pParamArrayOfObject) throws Exception {
		return null;
	}
}
