package com.tru.cache.adapter;

import atg.commerce.promotion.TRUPromotionTools;
import atg.nucleus.GenericService;
import atg.service.cache.CacheAdapter;

import com.tru.common.vo.TRUPromotionInfoVO;

/**
 * This service is an implementation of CacheAdapter that caches promotion Objects for purpose of showing the content on
 * product details page.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUPromoCacheAdapter extends GenericService implements CacheAdapter{

	/**
	 * Holds the mPromotionTools.
	 */
	private TRUPromotionTools mPromotionTools;
	
	/**
	 * This method invokes to get the promotion related information.
	 * 
	 * @param pKey - PromoInfoRequestVO request VO
	 * @return Object - Object retrieved SQL.
	 * @throws Exception - If exception occurs.
	 */
	@Override
	public Object getCacheElement(Object pKey) throws Exception {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUPromoCacheAdapter.getCacheElement method.. pKey : {0}", pKey);
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUPromoCacheAdapter.getCacheElement method");
		}
		return null;
	}
	/**
	 * Used to remove cache element.
	 * 
	 * @param pParamObject1 - First object
	 * @param pParamObject2 - Second object
	 */
	@Override
	public int getCacheElementSize(Object pParamObject1, Object pParamObject2) {
		return 0;
	}

	@Override
	public Object[] getCacheElements(Object[] pKeys) throws Exception {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUPromoCacheAdapter.getCacheElements method.. pKeys : {0}", pKeys);
		}
		TRUPromotionInfoVO promotionInfoVO = null;
		promotionInfoVO = getPromotionTools().populateAllPromo((Object[]) pKeys);
		if (isLoggingDebug()) {
			logDebug("END:: TRUPromoCacheAdapter.getCacheElements method..");
		}
		return promotionInfoVO.getPromotionInfoList().toArray();
	}
	/**
	 * Used to find the cache key size.
	 * 
	 * @param pObject - Object
	 * @return int - Return integer value
	 */
	@Override
	public int getCacheKeySize(Object pObject) {
		return 0;
	}
	/**
	 * Used to remove cache element.
	 * 
	 * @param pParamObject1 - First object
	 * @param pParamObject2 - Second object
	 */
	@Override
	public void removeCacheElement(Object pParamObject1, Object pParamObject2) {
	}

	/**
	 * @return the mPromotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * @param pPromotionTools the mPromotionTools to set
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		this.mPromotionTools = pPromotionTools;
	}

}
