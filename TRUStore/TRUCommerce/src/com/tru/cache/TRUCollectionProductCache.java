package com.tru.cache;

import javax.jms.JMSException;
import javax.jms.Message;

import atg.dms.patchbay.MessageSink;
import atg.service.cache.Cache;

import com.tru.commerce.exception.TRUCommerceException;

/**
 * This class extends OOTB service Cache and implements patch bay systems message sink interface. 
 * This class is written to hold the Product objects in cache.
 * 
 * This classes uses CacheAdapter for querying items from SEORepsoitry if not found in this cache. The Objects returned
 * by Cache Adapter are cached by this class so they can returned from cache with out invoking the repsoitry.
 * 
 * 
 * This class implements OOTB Message Sink interface for flushing its cache content. Whenever a message is received in
 * receiveMessage() it triggers Cache Flush.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUCollectionProductCache extends Cache implements MessageSink {

	/**
	 * To hold serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * This method is used to flush the cache.
	 * 
	 * @param pParamString - String value
	 * @param pParamMessage - value message.
	 * @throws JMSException - to throw the JMSException.
	 */
	@Override
	public void receiveMessage(String pParamString, Message pParamMessage) throws JMSException {
		if (isLoggingDebug()) {
			vlogDebug("TRUCollectionProductCache.receiveMessage method.. pParamString : {0},pParamMessage : {1}", pParamString,
					pParamMessage);
		}
		flush();
	}

	/**
	 * This method is used to get the value form cache Object.
	 * 
	 * @param pKey - Value of key object.
	 * @return Object - return the Object values.
	 * @throws TRUCommerceException - to throw the TRUCommerceException.
	 */
	@Override
	public Object get(Object pKey) throws TRUCommerceException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUCollectionProductCache.get method.. pKey : {0}", pKey);
		}
		Object value = null;
		try {
			value = super.get(pKey);
		} catch (Exception exception) {
			throw new TRUCommerceException(exception);
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUCollectionProductCache.get method.. value : {0}", value);
		}
		return value;
	}
}
