package com.tru.security.xssvalidation;

/**
 * This class contains XSSValidation constants.
 * 
 * @author PA
 * @version 1.0 
 * 
 */
public class TRUStoreXSSConstants {
	
	/**
	 * Constant to hold AMPRESEND_SYMBOL value.
	 */
	public static final String AMPRESEND_SYMBOL = "&";
	
	/**
	 * Constant to hold EMPTY_STRING value.
	 */
	public static final String EMPTY_STRING = "";
	
	/**
	 * Constant to hold EQUAL_SYMBOL value.
	 */
	public static final String EQUAL_SYMBOL = "=";
	
	/**
	 * Constant to hold SANITIZED value.
	 */
	public static final String SANITIZED = "sanitized";
	
	/** The Constant CHAR_DOT. */
	public static final String CHAR_DOT = ".";
}
