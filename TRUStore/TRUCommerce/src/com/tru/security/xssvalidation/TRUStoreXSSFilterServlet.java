package com.tru.security.xssvalidation;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;

import atg.nucleus.ServiceException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.InsertableServletImpl;
/**
 * The class is used to check Cross Site Scripting. Verifies the query params for malicious code and
 * encodes appropriately.
 * 
 * @author PA
 * @version 1.0 
 */
public class TRUStoreXSSFilterServlet extends InsertableServletImpl {

	/** Holds property mEnabled . */
	private boolean mEnabled;

	/** Holds property mStoreXSSValidator. */
	private TRUStoreXSSValidator mStoreXSSValidator;
	
	/** Holds property mPolicyFile . */
	private File mPolicyFile;
	
	/** The ignore parameters mIgnoreParametersList. */
	private List<String> mIgnoreParametersList;
	
	/** The m email user mEmailUserAgent. */
	private String mEmailUserAgent;
	/** The m email user USER_AGENT. */
	private static final String USER_AGENT="User-Agent";

	
	/**
	 * Gets the ignore parameters list.
	 *
	 * @return the ignore parameters list
	 */
	public List<String> getIgnoreParametersList() {
		return mIgnoreParametersList;
	}

	/**
	 * Sets the ignore parameters list.
	 *
	 * @param pIgnoreParametersList the new ignore parameters list
	 */
	public void setIgnoreParametersList(List<String> pIgnoreParametersList) {
		this.mIgnoreParametersList = pIgnoreParametersList;
	}

	/**
	 * @return the policyFile
	 */
	public File getPolicyFile() {
		return mPolicyFile;
	}

	/**
	 * @param pPolicyFile the policyFile to set
	 */
	public void setPolicyFile(File pPolicyFile) {
		this.mPolicyFile = pPolicyFile;
	}

	/**
	 * This method is overridden to output whether the pipeline is enabled or not.
	 * 
	 * @throws ServiceException
	 *             -ServiceException
	 */
	public void doStartService() throws ServiceException {
		if (isLoggingDebug()) {
			logDebug("Entering in to TRUStoreXSSFilterServlet doStartService()");
		}
		super.doStartService();
		if (isLoggingDebug()) {
			logDebug("TRUStoreXSSFilterServlet enabled: " + mEnabled);
		}
	}
	/**
	 * Overridden method verifies both parameters & cookies for malicious entry & encodes appropriately.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest.
	 * @param pResponse
	 *            - DynamoHttpServletResponse
	 * @throws IOException
	 *             - IOException
	 * @throws ServletException
	 *             - ServletException
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUStoreXSSFilterServlet service()");
		}
		String userAgent= pRequest.getHeader(USER_AGENT);
		
		if(isEnabled() && getPolicyFile() != null && userAgent!=null && !userAgent.contains(getEmailUserAgent())){
			cleanRequestParameters(pRequest);
		}
		passRequest(pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUStoreXSSFilterServlet service()");
		}
	}

	/**
	 * This method is used to clean all invalid characters i.e. XSS from the request parameter and set it back in the
	 * request.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest instance.
	 */
	protected void cleanRequestParameters(DynamoHttpServletRequest pRequest) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUStoreXSSFilterServlet cleanRequestParameters()");
		}
		String name = null;
		String val1 = null;
		String val2 = null;
		boolean isSanitized = false;
		StringBuffer queryString = new StringBuffer(TRUStoreXSSConstants.EMPTY_STRING);
		// Get all the params in the current request.
		Enumeration<String> params = pRequest.getParameterNames();
		
		if (params != null) {
			while (params.hasMoreElements()) {
				name = params.nextElement();
				//To handle null pointer if the parameter name is "."
				if(TRUStoreXSSConstants.CHAR_DOT.equalsIgnoreCase(name)){
					continue;
				}
				val1 = pRequest.getParameter(name);
				// Check for malicious code in the request
				if (val1 != null) {
					//To avoid clean up of certain parameters ex: third party params like tokens
					if (getIgnoreParametersList() != null && getIgnoreParametersList().contains(name)) {
						val2 = val1;
					} else {
						val2 = getStoreXSSValidator().cleanInputString(val1, getPolicyFile());
						if (isLoggingDebug()) {
							logDebug("Sanitized value: " + val2);
						}
					}
				    queryString = queryString.append(name).append(TRUStoreXSSConstants.EQUAL_SYMBOL).append(val2).append(TRUStoreXSSConstants.AMPRESEND_SYMBOL);
				  
					if (!isSanitized && !val1.equals(val2)) {
						isSanitized = true;
					}
					pRequest.setParameter(name.replace(TRUStoreXSSConstants.AMPRESEND_SYMBOL, TRUStoreXSSConstants.EMPTY_STRING), val2);
				}
			}
		}
		// If there is any changes in param values, then set the new querystring.
		if (isSanitized) {
		    pRequest.setQueryString(queryString.toString());
		}
		//Set parameter in the request to indicate that the incoming request was sanitized and encoded. 
		if (isLoggingDebug()) {
		    logDebug("Modified QueryString :" + pRequest.getQueryString());
			logDebug("Exiting from TRUStoreXSSFilterServlet cleanRequestParameters()");
		}
	}
	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * @param pEnabled the enabled to set
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	/**
	 * @return the storeXSSValidator
	 */
	public TRUStoreXSSValidator getStoreXSSValidator() {
		return mStoreXSSValidator;
	}
	/**
	 * @param pStoreXSSValidator the storeXSSValidator to set
	 */
	public void setStoreXSSValidator(TRUStoreXSSValidator pStoreXSSValidator) {
		mStoreXSSValidator = pStoreXSSValidator;
	}

	/**
	 * @return the mEmailUserAgent
	 */
	public String getEmailUserAgent() {
		return mEmailUserAgent;
	}
	
	/**
	 * @param pEmailUserAgent the EmailUserAgent to set
	 */
	public void setEmailUserAgent(String pEmailUserAgent) {
		mEmailUserAgent = pEmailUserAgent;
	}
}
