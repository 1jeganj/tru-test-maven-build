package com.tru.security.xssvalidation;

import java.io.File;

import org.owasp.validator.html.AntiSamy;
import org.owasp.validator.html.CleanResults;
import org.owasp.validator.html.Policy;
import org.owasp.validator.html.PolicyException;
import org.owasp.validator.html.ScanException;

import atg.nucleus.GenericService;

/**
 * This class uses OWASP Antisamy API to validate the InputString.
 * 
 * @author PA
 * @version 1.0 
 */
public class TRUStoreXSSValidator extends GenericService{

	/**
	 * This methods is used to validate the query params for malicious code such as javascript code 
	 * using OWASP AntiSamy project.
	 * 
	 * @param pInputString - params
	 * @param pPolicyFile file
	 * @return String - returns cleanString
	 */
	public String cleanInputString(String pInputString, File pPolicyFile) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUStoreXSSValidator cleanInputString()");
			logDebug("TRUStoreXSSValidator: pInputString before Cleaning::" +pInputString);
		}
		String cleanString = TRUStoreXSSConstants.EMPTY_STRING;
		
		Policy policy = null;
		try {
			policy = Policy.getInstance(pPolicyFile);
			AntiSamy antisamy = new AntiSamy();
			if(antisamy != null && policy != null){
				CleanResults cr = antisamy.scan(pInputString, policy, AntiSamy.SAX);
				cleanString = cr.getCleanHTML();
			}
			if (isLoggingDebug()) {
				logDebug("Sanitized InputString: "+cleanString);
			}
		} catch (PolicyException pe) {
			if(isLoggingError()){
				logError("PolicyException Ocurred:", pe);
			}
		}catch (ScanException se) {
			if(isLoggingError()){
				logError("ScanException Ocurred:", se);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUStoreXSSValidator cleanInputString()");
		}
		return cleanString;		
	}


}
