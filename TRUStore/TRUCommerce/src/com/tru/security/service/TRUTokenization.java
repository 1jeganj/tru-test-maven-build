
package com.tru.security.service;

import atg.repository.RepositoryException;

import com.tru.integrations.exception.TRUIntegrationException;

/**
 *  This interface is written to encrypt/decrypt the gift card number.
 *  @author PA
 *  @version 1.0
 *
 */
public interface TRUTokenization {

	/**
	 * This method is used to encrypt the gift card number.
	 * @param pGiftCardNumber - GiftCardNumber
	 * @throws TRUIntegrationException - TRUIntegrationException
	 * @throws RepositoryException  - RepositoryException 
	 * @return the encrypted CardNumber
	 */
	String encryptCardNumber(String pGiftCardNumber) throws TRUIntegrationException, RepositoryException;

	/**
	 * This method is used to decrypt the gift card number.
	 * @param pGiftCardNumber -GiftCardNumber
	 * @throws TRUIntegrationException - TRUIntegrationException
	 * @throws RepositoryException - RepositoryException
	 * @return the decrypted CardNumber
	 */
	String decryptCardNumber(String pGiftCardNumber) throws TRUIntegrationException, RepositoryException;

}

