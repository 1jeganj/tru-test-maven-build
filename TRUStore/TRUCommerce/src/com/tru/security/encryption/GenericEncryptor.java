package com.tru.security.encryption;

import java.io.Serializable;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import atg.core.util.Base64;
import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;

/**
 * Strong encryption scheme.
 * <p/>
 * Uses DESede encryption. and Password Based Encryption.
 * 
 * @author PA
 * @version 1.0
 * 
 */
public class GenericEncryptor extends GenericService implements Serializable {

	/** Constant for HEXA_C7. */
	private static final long HEXA_C7 = 0xc7;

	/** Constant for HEXA_73. */
	private static final long HEXA_73 = 0x73;

	/** Constant for HEXA_21. */
	private static final long HEXA_21 = 0x21;
	// Modified the constant Name to fix PMD
	/** Constant for HEXA_8C. */
	private static final long HEXA_8C = 0x8c;

	/** Constant for HEXA_7E. */
	private static final long HEXA_7E = 0x7e;

	/** Constant for HEXA_C8. */
	private static final long HEXA_C8 = 0xc8;

	/** Constant for HEXA_EE. */
	private static final long HEXA_EE = 0xee;

	/** Constant for HEXA_99. */
	private static final long HEXA_99 = 0x99;

	/** Constant for AMPERSAND. */
	private static final String AMPERSAND = "&";

	/** Constant for EQUAL_TO. */
	private static final String EQUAL_TO = "=";

	/** Constant for NUM_20. */
	private static final int NUM_20 = 20;

	/** Constant for serialVersionUID. */
	private static final long serialVersionUID = -2943563893631547781L;

	// Cipher objects encryption
	/** Holds the mCe. */
	private transient Cipher mCe;

	/** Holds the mCd. */
	private transient Cipher mCd;

	/** Holds the mProviderClassName. */
	private transient String mProviderClassName;

	/** Holds the mCiphorType. */
	private String mCiphorType;

	/** Holds the mPassword. */
	private String mPassword;

	/** Holds the mSalt. */
	private byte[] mSalt = { (byte) HEXA_C7, (byte) HEXA_73, (byte) HEXA_21, (byte) HEXA_8C, (byte) HEXA_7E,
			(byte) HEXA_C8, (byte) HEXA_EE, (byte) HEXA_99,};

	/** Holds the mCount. */
	private int mCount = NUM_20;

	/*
	 * public GenericEncryptor() { }
	 */

	/**
	 * This method Creates key and initializing cipher before starting component  .
	 * 
	 * @throws ServiceException
	 *             - if any exception occurs.
	 * 
	 */
	@Override
	public void doStartService() throws ServiceException {
		PBEParameterSpec pbeParameterSpec = new PBEParameterSpec(mSalt, mCount);

		PBEKeySpec pbeKeySpec = new PBEKeySpec(getPassword().toCharArray());
		try {
			SecretKeyFactory keyFac = SecretKeyFactory.getInstance(getCiphorType());
			SecretKey desKey = keyFac.generateSecret(pbeKeySpec);
			initCipher(desKey, getCiphorType(), pbeParameterSpec);
		} catch (InvalidKeyException ie) {
			vlogError("Unable to start cipher : @Class GenericEncryptor : @method : doStartService()::", ie);
			throw new ServiceException(ie);
		} catch (NoSuchAlgorithmException nsae) {
			vlogError("Unable to start cipher : @Class GenericEncryptor : @method : doStartService()::", nsae);
			throw new ServiceException(nsae);
		} catch (InvalidKeySpecException ike) {
			vlogError("Unable to start cipher : @Class GenericEncryptor : @method : doStartService()::", ike);
			throw new ServiceException(ike);
		} catch (NoSuchPaddingException nspe) {
			vlogError("Unable to start cipher : @Class GenericEncryptor : @method : doStartService()::", nspe);
			throw new ServiceException(nspe);
		} catch (InvalidAlgorithmParameterException iape) {
			vlogError("Unable to start cipher : @Class GenericEncryptor : @method : doStartService()::", iape);
			throw new ServiceException(iape);
		}
		super.doStartService();
	}

	/**
	 * Use to verify that the service has been started correctly. if the keyStore was loaded, this will return true.
	 * 
	 * @return True is key store was loaded.
	 */
	public boolean isEncrytionEnabled() {
		return (mCe != null && mCd != null);
	}

	/**
	 * Initializes the cipher.
	 * 
	 * @param pDesKey
	 *            the des key
	 * @param pType
	 *            the type
	 * @throws InvalidKeyException
	 *             the invalid key exception
	 * @throws NoSuchPaddingException
	 *             the no such padding exception
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 */
	public void initCipher(Key pDesKey, String pType) throws InvalidKeyException, NoSuchPaddingException,
			NoSuchAlgorithmException {
		// get cipher object for encryption type.
		mCe = Cipher.getInstance(pType);

		// initialize cipher for encryption, without supplying
		// any parameters.
		mCe.init(Cipher.ENCRYPT_MODE, pDesKey);

		// get cipher object for encryption type.
		mCd = Cipher.getInstance(pType);

		// initialize cipher for encryption, without supplying
		// any parameters.
		mCd.init(Cipher.DECRYPT_MODE, pDesKey);

	}

	/*
	 * Initializes the Cipher for Encryption and Decryption.
	 * 
	 * @param pDesKey Key
	 * 
	 * @param pType String
	 * 
	 * @param pPbeParameterSpec PBEParameterSpec
	 * 
	 * @throws InvalidKeyException
	 * 
	 * @throws NoSuchPaddingException
	 * 
	 * @throws NoSuchAlgorithmException
	 * 
	 * @throws InvalidAlgorithmParameterException
	 */
	/**
	 * Initializes the cipher.
	 * 
	 * @param pDesKey
	 *            the des key
	 * @param pType
	 *            the type
	 * @param pPbeParameterSpec
	 *            the pbe parameter spec
	 * @throws InvalidKeyException
	 *             the invalid key exception
	 * @throws NoSuchPaddingException
	 *             the no such padding exception
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 * @throws InvalidAlgorithmParameterException
	 *             the invalid algorithm parameter exception
	 */
	public void initCipher(Key pDesKey, String pType, PBEParameterSpec pPbeParameterSpec) throws InvalidKeyException,
			NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
		// get cipher object for encryption type.
		mCe = Cipher.getInstance(pType);

		// initialize cipher for encryption, without supplying
		// any parameters.
		mCe.init(Cipher.ENCRYPT_MODE, pDesKey, pPbeParameterSpec);

		// get cipher object for decryption type.
		mCd = Cipher.getInstance(pType);

		// initialize cipher for decryption, without supplying
		// any parameters.
		mCd.init(Cipher.DECRYPT_MODE, pDesKey, pPbeParameterSpec);
	}

	/**
	 * encrypt the given value *.
	 * 
	 * @param pValue
	 *            value to encrypt
	 * @return encrypted value
	 * @throws InvalidKeyException
	 *             the invalid key exception
	 * @throws NoSuchPaddingException
	 *             the no such padding exception
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 * @throws BadPaddingException
	 *             the bad padding exception
	 * @throws IllegalBlockSizeException
	 *             the illegal block size exception
	 */
	public String encrypt(String pValue) throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException,
			BadPaddingException, IllegalBlockSizeException {
		return encrypt(pValue, false);
	}

	/**
	 * encrypt the given value, if base64encoding is enabled, use base 64 encoding on the value before making it a
	 * string.
	 * 
	 * @param pValue
	 *            value to encrypt
	 * @param pBase64Encoding
	 *            base64 encode the encrypted bytes before creating a string.
	 * @return encrypted value
	 * @throws InvalidKeyException
	 *             the invalid key exception
	 * @throws NoSuchPaddingException
	 *             the no such padding exception
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 * @throws BadPaddingException
	 *             the bad padding exception
	 * @throws IllegalBlockSizeException
	 *             the illegal block size exception
	 */
	public String encrypt(String pValue, boolean pBase64Encoding) throws InvalidKeyException, NoSuchPaddingException,
			NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException {
		byte[] encodedValue = Base64.encodeToByteArray(pValue.getBytes());
		String encodedStringValue = new String(encodedValue);
		String encodedFinalValue = new String(encodedStringValue.replaceAll(EQUAL_TO, AMPERSAND));
		vlogDebug("value ::::::{0} ",pValue);
		vlogDebug("encodedValue ::::::{0} ",encodedValue);
		vlogDebug("encodedStringValue ::::::{0} ",encodedStringValue);
		vlogDebug("encodedFinalValue ::::::{0} ",encodedFinalValue);
		return encodedFinalValue;
	}

	/**
	 * Decrypt an encrypted value.
	 * 
	 * @param pValue
	 *            value to decrypt.
	 * @param pBase64Encoding
	 *            wheather base 64 encoding on the string was done
	 * @return un encrypted String
	 * @throws InvalidKeyException
	 *             the invalid key exception
	 * @throws NoSuchPaddingException
	 *             the no such padding exception
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 * @throws BadPaddingException
	 *             the bad padding exception
	 * @throws IllegalBlockSizeException
	 *             the illegal block size exception
	 */
	public String decrypt(String pValue, boolean pBase64Encoding) throws InvalidKeyException, NoSuchPaddingException,
			NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException {

		String valueToDecode = new String(pValue.replaceAll(AMPERSAND, EQUAL_TO));
		byte[] decodedValue = Base64.decodeToByteArray(valueToDecode);
		String decodedFinalValue = new String(decodedValue);
		vlogDebug("value ::::::" + pValue);
		vlogDebug("valueToDecode ::::::" + valueToDecode);
		vlogDebug("decodedValue ::::::" + decodedValue);
		vlogDebug("decodedFinalValue ::::::" + decodedFinalValue);
		return decodedFinalValue;
	}

	/**
	 * default to decryption without base 64 encoding.
	 * 
	 * @param pValue
	 *            the value
	 * @return the string
	 * @throws BadPaddingException
	 *             the bad padding exception
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 * @throws IllegalBlockSizeException
	 *             the illegal block size exception
	 * @throws InvalidKeyException
	 *             the invalid key exception
	 * @throws NoSuchPaddingException
	 *             the no such padding exception
	 */
	public String decrypt(String pValue) throws BadPaddingException, NoSuchAlgorithmException,
			IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException {
		return decrypt(pValue, false);
	}

	/**
	 * Sets the provider class name.
	 * 
	 * @param pProviderClassName
	 *            the new provider class name
	 */
	public void setProviderClassName(String pProviderClassName) {
		// Create instance of named provider class.
		try {
			Provider provider = (Provider) Class.forName(pProviderClassName).newInstance();
			Security.addProvider(provider);
		} catch (InstantiationException ie) {
			vlogError("Unable to start cipher : @Class GenericEncryptor : @method : setProviderClassName()::", ie);
		} catch (IllegalAccessException iae) {
			vlogError("Unable to start cipher : @Class GenericEncryptor : @method : setProviderClassName()::", iae);
		} catch (ClassNotFoundException cne) {
			vlogError("Unable to start cipher : @Class GenericEncryptor : @method : setProviderClassName()::", cne);
		}
		mProviderClassName = pProviderClassName;
	}

	/**
	 * Returns the provider class name.
	 * 
	 * @return the provider class name
	 */
	public String getProviderClassName() {
		return mProviderClassName;
	}

	/**
	 * Sets the ciphor type.
	 * 
	 * @param pCiphorType
	 *            the new ciphor type
	 */
	public void setCiphorType(String pCiphorType) {
		this.mCiphorType = pCiphorType;
	}

	/**
	 * Returns the ciphor type.
	 * 
	 * @return mCiphorType
	 */
	public String getCiphorType() {
		return mCiphorType;
	}

	/**
	 * Sets the password.
	 * 
	 * @param pPassword
	 *            the new password
	 */
	public void setPassword(String pPassword) {
		this.mPassword = pPassword;
	}

	/**
	 * Returns the password.
	 * 
	 * @return mPassword
	 */
	public String getPassword() {
		return mPassword;
	}

}