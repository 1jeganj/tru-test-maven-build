package com.tru.email.utils;

import java.io.File;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.nucleus.ServiceMap;
import atg.userprofiling.email.TemplateEmailException;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.email.conf.TRUBaseLineEndecaIndexEmailConfiguration;
import com.tru.email.conf.TRUParametericCSVEmailConfiguration;
import com.tru.email.conf.TRUParametericFeedEmailConfiguration;
import com.tru.email.conf.TRUPartialEndecaIndexEmailConfiguration;
import com.tru.email.conf.TRUSiteMapEmailConfiguration;

/**
 * This class extends OOTB DynamoServlet to Email operation.
 * @author Professional Access
 * @version 1.0
 */
public class TRUEmailServiceUtils extends GenericService {
	
	/** property to hold Email Site Configuration. */
	@SuppressWarnings("rawtypes")
	private ServiceMap mEmailSiteMapConfiguartion;

	/** Property to hold mTemplateEmailSender. */
	private TemplateEmailSender mTemplateEmailSender;
	
	/**
	 * Send site map success email.
	 * 
	 * @param pSiteMapType
	 *            the site map type
	 * @param pParams
	 *            the params
	 */
	public void sendSiteMapSuccessEmail(String pSiteMapType, Map<String, Object> pParams) {
		vlogDebug("Begin:@Class: TRUSiteEmailServiceUtils : @Method: sendSiteMapSuccessEmail()");
		TemplateEmailInfoImpl emailInfo = null;
		try {
			if(StringUtils.isEmpty(pSiteMapType)){
				return;
			}
			if(pSiteMapType.equalsIgnoreCase(TRUCommerceConstants.SITE_MAP)){
				TRUSiteMapEmailConfiguration truEmailConfiguration = (TRUSiteMapEmailConfiguration) getEmailSiteMapConfiguartion().get(pSiteMapType);
				emailInfo = truEmailConfiguration.getSuccessTemplateEmailInfo();
				setMessageContent(emailInfo,pParams);
				getTemplateEmailSender().sendEmailMessage(emailInfo, truEmailConfiguration.getRecipients());
			}else if(pSiteMapType.equalsIgnoreCase(TRUCommerceConstants.PARAMETRIC_CSV)){
				TRUParametericCSVEmailConfiguration truEmailConfiguration = (TRUParametericCSVEmailConfiguration) getEmailSiteMapConfiguartion().get(pSiteMapType);
				emailInfo = truEmailConfiguration.getSuccessTemplateEmailInfo();
				setMessageContent(emailInfo,pParams);
				getTemplateEmailSender().sendEmailMessage(emailInfo, truEmailConfiguration.getRecipients());
			}else if(pSiteMapType.equalsIgnoreCase(TRUCommerceConstants.PARAMETRIC_FEED)){
				TRUParametericFeedEmailConfiguration truEmailConfiguration = (TRUParametericFeedEmailConfiguration) getEmailSiteMapConfiguartion().get(pSiteMapType);
				emailInfo = truEmailConfiguration.getSuccessTemplateEmailInfo();
				setMessageContent(emailInfo,pParams);
				getTemplateEmailSender().sendEmailMessage(emailInfo, truEmailConfiguration.getRecipients());
			}else if(pSiteMapType.equalsIgnoreCase(TRUCommerceConstants.PARTIAL_ENDECA_INDEXING)){
				TRUPartialEndecaIndexEmailConfiguration truEmailConfiguration = (TRUPartialEndecaIndexEmailConfiguration) getEmailSiteMapConfiguartion().get(pSiteMapType);
				emailInfo = truEmailConfiguration.getSuccessTemplateEmailInfo();
				setMessageContent(emailInfo,pParams);
				getTemplateEmailSender().sendEmailMessage(emailInfo, truEmailConfiguration.getRecipients());
			}else if(pSiteMapType.equalsIgnoreCase(TRUCommerceConstants.BASELINE_ENDECA_INDEXING)){
				TRUBaseLineEndecaIndexEmailConfiguration truEmailConfiguration = (TRUBaseLineEndecaIndexEmailConfiguration) getEmailSiteMapConfiguartion().get(pSiteMapType);
				emailInfo = truEmailConfiguration.getSuccessTemplateEmailInfo();
				setMessageContent(emailInfo,pParams);
				getTemplateEmailSender().sendEmailMessage(emailInfo, truEmailConfiguration.getRecipients());
			}
	
		} catch (TemplateEmailException e) {
			vlogError("Exception occured while sending a success email : ", e);
		}
		vlogDebug("Begin:@Class: TRUSiteEmailServiceUtils : @Method: sendSiteMapSuccessEmail()");
	}
	/**
	 * Send site map failure email.
	 *
	 * @param pSiteMapType the site map type
	 * @param pParams the params
	 */
	public void sendSiteMapFailureEmail(String pSiteMapType, Map<String, Object> pParams) {
		vlogDebug("Begin:@Class: TRUSiteEmailServiceUtils : @Method: sendSiteMapFailureEmail()");
		TemplateEmailInfoImpl emailInfo = null;
		try {
			if(StringUtils.isEmpty(pSiteMapType)){
				return;
			}
			if(pSiteMapType.equalsIgnoreCase(TRUCommerceConstants.SITE_MAP)){
				TRUSiteMapEmailConfiguration truEmailConfiguration = (TRUSiteMapEmailConfiguration) getEmailSiteMapConfiguartion().get(pSiteMapType);
				emailInfo = truEmailConfiguration.getFailureTemplateEmailInfo();
				setMessageContent(emailInfo,pParams);
				getTemplateEmailSender().sendEmailMessage(emailInfo, truEmailConfiguration.getRecipients());
			}else if(pSiteMapType.equalsIgnoreCase(TRUCommerceConstants.PARAMETRIC_CSV)){
				TRUParametericCSVEmailConfiguration truEmailConfiguration = (TRUParametericCSVEmailConfiguration) getEmailSiteMapConfiguartion().get(pSiteMapType);
				emailInfo = truEmailConfiguration.getFailureTemplateEmailInfo();
				setMessageContent(emailInfo,pParams);
				getTemplateEmailSender().sendEmailMessage(emailInfo, truEmailConfiguration.getRecipients());
			}else if(pSiteMapType.equalsIgnoreCase(TRUCommerceConstants.PARAMETRIC_FEED)){
				TRUParametericFeedEmailConfiguration truEmailConfiguration = (TRUParametericFeedEmailConfiguration) getEmailSiteMapConfiguartion().get(pSiteMapType);
				emailInfo = truEmailConfiguration.getFailureTemplateEmailInfo();
				setMessageContent(emailInfo,pParams);
				getTemplateEmailSender().sendEmailMessage(emailInfo, truEmailConfiguration.getRecipients());
			}else if(pSiteMapType.equalsIgnoreCase(TRUCommerceConstants.PARTIAL_ENDECA_INDEXING)){
				TRUPartialEndecaIndexEmailConfiguration truEmailConfiguration = (TRUPartialEndecaIndexEmailConfiguration) getEmailSiteMapConfiguartion().get(pSiteMapType);
				emailInfo = truEmailConfiguration.getFailureTemplateEmailInfo();
				setMessageContent(emailInfo,pParams);
				getTemplateEmailSender().sendEmailMessage(emailInfo, truEmailConfiguration.getRecipients());
			}else if(pSiteMapType.equalsIgnoreCase(TRUCommerceConstants.BASELINE_ENDECA_INDEXING)){
				TRUBaseLineEndecaIndexEmailConfiguration truEmailConfiguration = (TRUBaseLineEndecaIndexEmailConfiguration) getEmailSiteMapConfiguartion().get(pSiteMapType);
				emailInfo = truEmailConfiguration.getFailureTemplateEmailInfo();
				setMessageContent(emailInfo,pParams);
				getTemplateEmailSender().sendEmailMessage(emailInfo, truEmailConfiguration.getRecipients());
			}
		} catch (TemplateEmailException e) {
			vlogError("Exception occured while sending a success email : ", e);
		}
		vlogDebug("Begin:@Class: TRUSiteEmailServiceUtils : @Method: sendSiteMapFailureEmail()");
	}
	
	/**
	 * Sets the message content.
	 *
	 * @param pEmailInfo the email info
	 * @param pParams the params
	 */
	@SuppressWarnings("unchecked")
	private void setMessageContent(TemplateEmailInfoImpl pEmailInfo,  Map<String, Object> pParams){
		if (pEmailInfo == null) {
			vlogError("emailInfo is NULL or EMPTY. Cannot Send email.");
			return;
		}
		clearEmailInfo(pEmailInfo);
		// Checking the emailInfo Parameters
		if (pEmailInfo.getTemplateParameters() != null) {
			pEmailInfo.getTemplateParameters().putAll(pParams);
		} else {
			pEmailInfo.setTemplateParameters(pParams);
		}
		pEmailInfo.setMessageSubject((String) pParams.get(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT));
	}
	
	/**
	 * Clear email info.
	 * 
	 * @param pEmailInfo
	 *            the email info
	 */
	public void clearEmailInfo(TemplateEmailInfoImpl pEmailInfo) {
		vlogDebug("Begin:@Class: TRUSiteEmailServiceUtils : @Method: clearEmailInfo()");
		pEmailInfo.setTemplateParameters(null);
		pEmailInfo.setMessageAttachments(null);
		pEmailInfo.setMessageSubject(null);
		vlogDebug("End:@Class: TRUSiteEmailServiceUtils: @Method: clearEmailInfo()");
	}
	
	
	/**
	 * Send site map processed with errors email.
	 * 
	 * @param pSiteMapType
	 *            the site map type
	 * @param pParams
	 *            the params
	 * @param pAttachementFiles
	 *            the attachement files
	 */
	@SuppressWarnings("unchecked")
	public void sendSiteMapProcessedWithErrorsEmail(String pSiteMapType, Map<String, Object> pParams, File[] pAttachementFiles) {
		vlogDebug("Begin:@Class: TRUSiteEmailServiceUtils : @Method: sendSiteMapProcessedWithErrorsEmail()");
		try {
			TRUSiteMapEmailConfiguration truSiteMapEmailConfiguration= (TRUSiteMapEmailConfiguration) getEmailSiteMapConfiguartion().get(pSiteMapType);
			TemplateEmailInfoImpl emailInfo = truSiteMapEmailConfiguration.getFailureTemplateEmailInfo();
			if (emailInfo == null) {
				vlogError("emailInfo is NULL or EMPTY. Cannot Send email.");
				return;
			}
			clearEmailInfo(emailInfo);
			// Checking the emailInfo Parameters
			if (emailInfo.getTemplateParameters() != null) {
				emailInfo.getTemplateParameters().putAll(pParams);
			} else {
				emailInfo.setTemplateParameters(pParams);
			}
			emailInfo.setMessageSubject((String) pParams.get(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT));
			if (pAttachementFiles != null) {

				emailInfo.setMessageAttachments(pAttachementFiles);
			}
			getTemplateEmailSender().sendEmailMessage(emailInfo, truSiteMapEmailConfiguration.getRecipients());
		} catch (TemplateEmailException e) {
			vlogError("Exception occured while sending a success email : ", e);
		}
		vlogDebug("End:@Class: TRUSiteEmailServiceUtils : @Method: sendSiteMapProcessedWithErrorsEmail()");
	}
	

	/**
	 * Gets the email site map configuartion.
	 *
	 * @return the emailSiteMapConfiguartion
	 */
	@SuppressWarnings("rawtypes")
	public ServiceMap getEmailSiteMapConfiguartion() {
		return mEmailSiteMapConfiguartion;
	}

	/**
	 * Sets the email site map configuartion.
	 *
	 * @param pEmailSiteMapConfiguartion the emailSiteMapConfiguartion to set
	 */
	@SuppressWarnings("rawtypes")
	public void setEmailSiteMapConfiguartion(ServiceMap pEmailSiteMapConfiguartion) {
		mEmailSiteMapConfiguartion = pEmailSiteMapConfiguartion;
	}

	/**
	 * Gets the template email sender.
	 *
	 * @return the templateEmailSender
	 */
	public TemplateEmailSender getTemplateEmailSender() {
		return mTemplateEmailSender;
	}

	/**
	 * Sets the template email sender.
	 *
	 * @param pTemplateEmailSender the templateEmailSender to set
	 */
	public void setTemplateEmailSender(TemplateEmailSender pTemplateEmailSender) {
		mTemplateEmailSender = pTemplateEmailSender;
	}
	
	

}
