package com.tru.email.conf;

import java.util.List;

import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailInfoImpl;

/**
 * The Class TRUParametericFeedEmailConfiguration.
 * @author Professional Access
 * @version 1.0
 */

public class TRUParametericFeedEmailConfiguration extends GenericService {
	/** Property to hold Failure Template Email Info. */
	private TemplateEmailInfoImpl mFailureTemplateEmailInfo;

	/** Property to hold Success Template Email Info. */
	private TemplateEmailInfoImpl mSuccessTemplateEmailInfo;
	 
	/** The m success ParametericFeed  email message. */
	private String mSuccessParametericFeedEmailMessage;
	
	/** The m success site map to ftp location message. *//*
	private String mSuccessSiteMapToFtpLocationMessage;*/
	
	/** The m failure exception message. */
	private String mFailureExceptionMessage;

	/** Property to hold Recipients. */
	private List<String> mRecipients;
	
	/** The m failure ParametericFeed email message. */
	private String mFailureParametericFeedEmailMessage;
	
	/** The m success ParametericFeed message. */
	private String mSuccParametericFeedMessage;

	/**
	 * Gets the recipients.
	 * 
	 * @return the recipients
	 */
	public List<String> getRecipients() {
		return mRecipients;
	}

	/**
	 * Sets the recipients.
	 * 
	 * @param pRecipients
	 *            the mRecipients to set
	 */
	public void setRecipients(List<String> pRecipients) {
		mRecipients = pRecipients;
	}

	/**
	 * Gets the failure template email info.
	 * 
	 * @return the failureTemplateEmailInfo
	 */
	public TemplateEmailInfoImpl getFailureTemplateEmailInfo() {
		return mFailureTemplateEmailInfo;
	}

	/**
	 * Sets the failure template email info.
	 * 
	 * @param pFailureTemplateEmailInfo
	 *            the mFailureTemplateEmailInfo to set
	 */
	public void setFailureTemplateEmailInfo(
			TemplateEmailInfoImpl pFailureTemplateEmailInfo) {
		mFailureTemplateEmailInfo = pFailureTemplateEmailInfo;
	}

	/**
	 * Gets the success template email info.
	 * 
	 * @return the successTemplateEmailInfo
	 */
	public TemplateEmailInfoImpl getSuccessTemplateEmailInfo() {
		return mSuccessTemplateEmailInfo;
	}

	/**
	 * Sets the success template email info.
	 * 
	 * @param pSuccessTemplateEmailInfo
	 *            the successTemplateEmailInfo to set
	 */
	public void setSuccessTemplateEmailInfo(
			TemplateEmailInfoImpl pSuccessTemplateEmailInfo) {
		mSuccessTemplateEmailInfo = pSuccessTemplateEmailInfo;
	}

	/**
	 * Gets the success ParametericFeed email message.
	 *
	 * @return the mSuccessParametericFeedEmailMessage
	 */
	public String getSuccessParametericFeedEmailMessage() {
		return mSuccessParametericFeedEmailMessage;
	}

	/**
	 * Sets the success ParametericFeed email message.
	 *
	 * @param pSuccessParametericFeedEmailMessage the mSuccessParametericFeedEmailMessagee to set
	 */
	public void setSuccessParametericFeedEmailMessage(
			String pSuccessParametericFeedEmailMessage) {
		this.mSuccessParametericFeedEmailMessage = pSuccessParametericFeedEmailMessage;
	}

/*	*//**
	 * Gets the success site map to ftp location message.
	 *
	 * @return the mSuccessSiteMapToFtpLocationMessage
	 *//*
	public String getSuccessSiteMapToFtpLocationMessage() {
		return mSuccessSiteMapToFtpLocationMessage;
	}

	*//**
	 * Sets the success site map to ftp location message.
	 *
	 * @param pSuccessSiteMapToFtpLocationMessage the mSuccessSiteMapToFtpLocationMessage to set
	 *//*
	public void setSuccessSiteMapToFtpLocationMessage(
			String pSuccessSiteMapToFtpLocationMessage) {
		this.mSuccessSiteMapToFtpLocationMessage = pSuccessSiteMapToFtpLocationMessage;
	}*/

	/**
	 * Gets the failure exception message.
	 *
	 * @return the mFailureExceptionMessage
	 */
	public String getFailureExceptionMessage() {
		return mFailureExceptionMessage;
	}

	/**
	 * Sets the failure exception message.
	 *
	 * @param pFailureExceptionMessage the mFailureExceptionMessage to set
	 */
	public void setFailureExceptionMessage(String pFailureExceptionMessage) {
		this.mFailureExceptionMessage = pFailureExceptionMessage;
	}

	/**
	 * @return the FailureParametericFeedEmailMessage
	 */
	public String getFailureParametericFeedEmailMessage() {
		return mFailureParametericFeedEmailMessage;
	}

	/**
	 * @param pFailureParametericFeedEmailMessage the FailureParametericFeedEmailMessage to set
	 */
	public void setFailureParametericFeedEmailMessage(
			String pFailureParametericFeedEmailMessage) {
		mFailureParametericFeedEmailMessage = pFailureParametericFeedEmailMessage;
	}

	/**
	 * @return the SuccParametericFeedMessage
	 */
	public String getSuccParametericFeedMessage() {
		return mSuccParametericFeedMessage;
	}

	/**
	 * @param pSuccParametericFeedMessage the SuccParametericFeedMessage to set
	 */
	public void setSuccParametericFeedMessage(String pSuccParametericFeedMessage) {
		mSuccParametericFeedMessage = pSuccParametericFeedMessage;
	}

}
