package com.tru.email.conf;

import java.util.List;

import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailInfoImpl;

/**
 * The Class TRUPartialEndecaIndexEmailConfiguration.
 * @author Professional Access
 * @version 1.0
 */

public class TRUPartialEndecaIndexEmailConfiguration extends GenericService {
	/** Property to hold Failure Template Email Info. */
	private TemplateEmailInfoImpl mFailureTemplateEmailInfo;

	/** Property to hold Success Template Email Info. */
	private TemplateEmailInfoImpl mSuccessTemplateEmailInfo;
	 
	/** The m success partial index  email message. */
	private String mSuccessPartialIndexEmailMessage;
	
/*	*//** The m success site map to ftp location message. *//*
	private String mSuccessSiteMapToFtpLocationMessage;*/
	
	/** The m failure exception message. */
	private String mFailureExceptionMessage;

	/** Property to hold Recipients. */
	private List<String> mRecipients;
	
	/** The m failure partial index email message. */
	private String mFailurePartialIndexEmailMessage;
	
	/** The m success partial index message. */
	private String mSuccPartialIndexMessage;

	/**
	 * Gets the recipients.
	 * 
	 * @return the recipients
	 */
	public List<String> getRecipients() {
		return mRecipients;
	}

	/**
	 * Sets the recipients.
	 * 
	 * @param pRecipients
	 *            the mRecipients to set
	 */
	public void setRecipients(List<String> pRecipients) {
		mRecipients = pRecipients;
	}

	/**
	 * Gets the failure template email info.
	 * 
	 * @return the failureTemplateEmailInfo
	 */
	public TemplateEmailInfoImpl getFailureTemplateEmailInfo() {
		return mFailureTemplateEmailInfo;
	}

	/**
	 * Sets the failure template email info.
	 * 
	 * @param pFailureTemplateEmailInfo
	 *            the mFailureTemplateEmailInfo to set
	 */
	public void setFailureTemplateEmailInfo(
			TemplateEmailInfoImpl pFailureTemplateEmailInfo) {
		mFailureTemplateEmailInfo = pFailureTemplateEmailInfo;
	}

	/**
	 * Gets the success template email info.
	 * 
	 * @return the successTemplateEmailInfo
	 */
	public TemplateEmailInfoImpl getSuccessTemplateEmailInfo() {
		return mSuccessTemplateEmailInfo;
	}

	/**
	 * Sets the success template email info.
	 * 
	 * @param pSuccessTemplateEmailInfo
	 *            the successTemplateEmailInfo to set
	 */
	public void setSuccessTemplateEmailInfo(
			TemplateEmailInfoImpl pSuccessTemplateEmailInfo) {
		mSuccessTemplateEmailInfo = pSuccessTemplateEmailInfo;
	}

	/**
	 * Gets the success partial index email message.
	 *
	 * @return the mSuccessPartialIndexEmailMessage
	 */
	public String getSuccessPartialIndexEmailMessage() {
		return mSuccessPartialIndexEmailMessage;
	}

	/**
	 * Sets the success partial index email message.
	 *
	 * @param pSuccessPartialIndexEmailMessage the mSuccessPartialIndexEmailMessage to set
	 */
	public void setSuccessPartialIndexEmailMessage(
			String pSuccessPartialIndexEmailMessage) {
		this.mSuccessPartialIndexEmailMessage = pSuccessPartialIndexEmailMessage;
	}

/*	*//**
	 * Gets the success site map to ftp location message.
	 *
	 * @return the mSuccessSiteMapToFtpLocationMessage
	 *//*
	public String getSuccessSiteMapToFtpLocationMessage() {
		return mSuccessSiteMapToFtpLocationMessage;
	}

	*//**
	 * Sets the success site map to ftp location message.
	 *
	 * @param pSuccessSiteMapToFtpLocationMessage the mSuccessSiteMapToFtpLocationMessage to set
	 *//*
	public void setSuccessSiteMapToFtpLocationMessage(
			String pSuccessSiteMapToFtpLocationMessage) {
		this.mSuccessSiteMapToFtpLocationMessage = pSuccessSiteMapToFtpLocationMessage;
	}*/

	/**
	 * Gets the failure exception message.
	 *
	 * @return the mFailureExceptionMessage
	 */
	public String getFailureExceptionMessage() {
		return mFailureExceptionMessage;
	}

	/**
	 * Sets the failure exception message.
	 *
	 * @param pFailureExceptionMessage the mFailureExceptionMessage to set
	 */
	public void setFailureExceptionMessage(String pFailureExceptionMessage) {
		this.mFailureExceptionMessage = pFailureExceptionMessage;
	}

	/**
	 * @return the FailurePartialIndexEmailMessage
	 */
	public String getFailureBaseLineIndexEmailMessage() {
		return mFailurePartialIndexEmailMessage;
	}

	/**
	 * @param pFailurePartialIndexEmailMessage the FailurePartialIndexEmailMessage to set
	 */
	public void setFailurePartialIndexEmailMessage(
			String pFailurePartialIndexEmailMessage) {
		mFailurePartialIndexEmailMessage = pFailurePartialIndexEmailMessage;
	}

	/**
	 * @return the SuccPartialIndexMessage
	 */
	public String getSuccPartialIndexMessage() {
		return mSuccPartialIndexMessage;
	}

	/**
	 * @param pSuccPartialIndexMessage the SuccPartialIndexMessage to set
	 */
	public void setSuccPartialIndexMessage(String pSuccPartialIndexMessage) {
		mSuccPartialIndexMessage = pSuccPartialIndexMessage;
	}

}
