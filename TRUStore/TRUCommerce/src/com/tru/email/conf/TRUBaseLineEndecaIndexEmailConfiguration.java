package com.tru.email.conf;

import java.util.List;

import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailInfoImpl;

/**
 * The Class TRUBaseLineEndecaIndexEmailConfiguration.
 * @author Professional Access
 * @version 1.0
 */

public class TRUBaseLineEndecaIndexEmailConfiguration extends GenericService {
	/** Property to hold Failure Template Email Info. */
	private TemplateEmailInfoImpl mFailureTemplateEmailInfo;

	/** Property to hold Success Template Email Info. */
	private TemplateEmailInfoImpl mSuccessTemplateEmailInfo;
	 
	/** The m success baseline index  email message. */
	private String mSuccessBaseLineIndexEmailMessage;
	
	/** The m success site map to ftp location message. *//*
	private String mSuccessSiteMapToFtpLocationMessage;*/
	
	/** The m failure exception message. */
	private String mFailureExceptionMessage;

	/** Property to hold Recipients. */
	private List<String> mRecipients;
	
	/** The m failure baseline index email message. */
	private String mFailureBaseLineIndexEmailMessage;
	
	/** The m success baseline index message. */
	private String mSuccBaseLineIndexMessage;

	/**
	 * Gets the recipients.
	 * 
	 * @return the recipients
	 */
	public List<String> getRecipients() {
		return mRecipients;
	}

	/**
	 * Sets the recipients.
	 * 
	 * @param pRecipients
	 *            the mRecipients to set
	 */
	public void setRecipients(List<String> pRecipients) {
		mRecipients = pRecipients;
	}

	/**
	 * Gets the failure template email info.
	 * 
	 * @return the failureTemplateEmailInfo
	 */
	public TemplateEmailInfoImpl getFailureTemplateEmailInfo() {
		return mFailureTemplateEmailInfo;
	}

	/**
	 * Sets the failure template email info.
	 * 
	 * @param pFailureTemplateEmailInfo
	 *            the mFailureTemplateEmailInfo to set
	 */
	public void setFailureTemplateEmailInfo(
			TemplateEmailInfoImpl pFailureTemplateEmailInfo) {
		mFailureTemplateEmailInfo = pFailureTemplateEmailInfo;
	}

	/**
	 * Gets the success template email info.
	 * 
	 * @return the successTemplateEmailInfo
	 */
	public TemplateEmailInfoImpl getSuccessTemplateEmailInfo() {
		return mSuccessTemplateEmailInfo;
	}

	/**
	 * Sets the success template email info.
	 * 
	 * @param pSuccessTemplateEmailInfo
	 *            the successTemplateEmailInfo to set
	 */
	public void setSuccessTemplateEmailInfo(
			TemplateEmailInfoImpl pSuccessTemplateEmailInfo) {
		mSuccessTemplateEmailInfo = pSuccessTemplateEmailInfo;
	}

	/**
	 * Gets the success baseline index email message.
	 *
	 * @return the mSuccessBaseLineIndexEmailMessage
	 */
	public String getSuccessBaseLineIndexEmailMessage() {
		return mSuccessBaseLineIndexEmailMessage;
	}

	/**
	 * Sets the success baseline index email message.
	 *
	 * @param pSuccessBaseLineIndexEmailMessage the mSuccessBaseLineIndexEmailMessage to set
	 */
	public void setSuccessBaseLineIndexEmailMessage(
			String pSuccessBaseLineIndexEmailMessage) {
		this.mSuccessBaseLineIndexEmailMessage = pSuccessBaseLineIndexEmailMessage;
	}

/*	*//**
	 * Gets the success site map to ftp location message.
	 *
	 * @return the mSuccessSiteMapToFtpLocationMessage
	 *//*
	public String getSuccessSiteMapToFtpLocationMessage() {
		return mSuccessSiteMapToFtpLocationMessage;
	}

	*//**
	 * Sets the success site map to ftp location message.
	 *
	 * @param pSuccessSiteMapToFtpLocationMessage the mSuccessSiteMapToFtpLocationMessage to set
	 *//*
	public void setSuccessSiteMapToFtpLocationMessage(
			String pSuccessSiteMapToFtpLocationMessage) {
		this.mSuccessSiteMapToFtpLocationMessage = pSuccessSiteMapToFtpLocationMessage;
	}*/

	/**
	 * Gets the failure exception message.
	 *
	 * @return the mFailureExceptionMessage
	 */
	public String getFailureExceptionMessage() {
		return mFailureExceptionMessage;
	}

	/**
	 * Sets the failure exception message.
	 *
	 * @param pFailureExceptionMessage the mFailureExceptionMessage to set
	 */
	public void setFailureExceptionMessage(String pFailureExceptionMessage) {
		this.mFailureExceptionMessage = pFailureExceptionMessage;
	}

	/**
	 * @return the FailureBaseLineIndexEmailMessage
	 */
	public String getFailureBaseLineIndexEmailMessage() {
		return mFailureBaseLineIndexEmailMessage;
	}

	/**
	 * @param pFailureBaseLineIndexEmailMessage the FailureBaseLineIndexEmailMessage to set
	 */
	public void setFailureBaseLineIndexEmailMessage(
			String pFailureBaseLineIndexEmailMessage) {
		mFailureBaseLineIndexEmailMessage = pFailureBaseLineIndexEmailMessage;
	}

	/**
	 * @return the SuccBaseLineIndexMessage
	 */
	public String getSuccBaseLineIndexMessage() {
		return mSuccBaseLineIndexMessage;
	}

	/**
	 * @param pSuccBaseLineIndexMessage the SuccBaseLineIndexMessage to set
	 */
	public void setSuccSiteMapMessage(String pSuccBaseLineIndexMessage) {
		mSuccBaseLineIndexMessage = pSuccBaseLineIndexMessage;
	}

}
