package com.tru.email.conf;

import java.util.List;

import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailInfoImpl;

/**
 * The Class TRUParametericCSVEmailConfiguration.
 * @author Professional Access
 * @version 1.0
 */

public class TRUParametericCSVEmailConfiguration extends GenericService {
	/** Property to hold Failure Template Email Info. */
	private TemplateEmailInfoImpl mFailureTemplateEmailInfo;

	/** Property to hold Success Template Email Info. */
	private TemplateEmailInfoImpl mSuccessTemplateEmailInfo;
	 
	/** The m success ParametericCSV  email message. */
	private String mSuccessParametericCSVEmailMessage;
	
	/** The m success site map to ftp location message. *//*
	private String mSuccessSiteMapToFtpLocationMessage;*/
	
	/** The m failure exception message. */
	private String mFailureExceptionMessage;

	/** Property to hold Recipients. */
	private List<String> mRecipients;
	
	/** The m failure ParametericCSV email message. */
	private String mFailureParametericCSVEmailMessage;
	
	/** The m success ParametericCSV message. */
	private String mSuccParametericCSVMessage;

	/**
	 * Gets the recipients.
	 * 
	 * @return the recipients
	 */
	public List<String> getRecipients() {
		return mRecipients;
	}

	/**
	 * Sets the recipients.
	 * 
	 * @param pRecipients
	 *            the mRecipients to set
	 */
	public void setRecipients(List<String> pRecipients) {
		mRecipients = pRecipients;
	}

	/**
	 * Gets the failure template email info.
	 * 
	 * @return the failureTemplateEmailInfo
	 */
	public TemplateEmailInfoImpl getFailureTemplateEmailInfo() {
		return mFailureTemplateEmailInfo;
	}

	/**
	 * Sets the failure template email info.
	 * 
	 * @param pFailureTemplateEmailInfo
	 *            the mFailureTemplateEmailInfo to set
	 */
	public void setFailureTemplateEmailInfo(
			TemplateEmailInfoImpl pFailureTemplateEmailInfo) {
		mFailureTemplateEmailInfo = pFailureTemplateEmailInfo;
	}

	/**
	 * Gets the success template email info.
	 * 
	 * @return the successTemplateEmailInfo
	 */
	public TemplateEmailInfoImpl getSuccessTemplateEmailInfo() {
		return mSuccessTemplateEmailInfo;
	}

	/**
	 * Sets the success template email info.
	 * 
	 * @param pSuccessTemplateEmailInfo
	 *            the successTemplateEmailInfo to set
	 */
	public void setSuccessTemplateEmailInfo(
			TemplateEmailInfoImpl pSuccessTemplateEmailInfo) {
		mSuccessTemplateEmailInfo = pSuccessTemplateEmailInfo;
	}

	/**
	 * Gets the success ParametericCSV email message.
	 *
	 * @return the mSuccessParametericCSVEmailMessage
	 */
	public String getSuccessParametericCSVEmailMessage() {
		return mSuccessParametericCSVEmailMessage;
	}

	/**
	 * Sets the success ParametericFeed email message.
	 *
	 * @param pSuccessParametericCSVEmailMessage the mSuccessParametericCSVEmailMessagee to set
	 */
	public void setSuccessParametericCSVEmailMessage(
			String pSuccessParametericCSVEmailMessage) {
		this.mSuccessParametericCSVEmailMessage = pSuccessParametericCSVEmailMessage;
	}

/*	*//**
	 * Gets the success site map to ftp location message.
	 *
	 * @return the mSuccessSiteMapToFtpLocationMessage
	 *//*
	public String getSuccessSiteMapToFtpLocationMessage() {
		return mSuccessSiteMapToFtpLocationMessage;
	}

	*//**
	 * Sets the success site map to ftp location message.
	 *
	 * @param pSuccessSiteMapToFtpLocationMessage the mSuccessSiteMapToFtpLocationMessage to set
	 *//*
	public void setSuccessSiteMapToFtpLocationMessage(
			String pSuccessSiteMapToFtpLocationMessage) {
		this.mSuccessSiteMapToFtpLocationMessage = pSuccessSiteMapToFtpLocationMessage;
	}*/

	/**
	 * Gets the failure exception message.
	 *
	 * @return the mFailureExceptionMessage
	 */
	public String getFailureExceptionMessage() {
		return mFailureExceptionMessage;
	}

	/**
	 * Sets the failure exception message.
	 *
	 * @param pFailureExceptionMessage the mFailureExceptionMessage to set
	 */
	public void setFailureExceptionMessage(String pFailureExceptionMessage) {
		this.mFailureExceptionMessage = pFailureExceptionMessage;
	}

	/**
	 * @return the FailureParametericCSVEmailMessage
	 */
	public String getFailureParametericCSVEmailMessage() {
		return mFailureParametericCSVEmailMessage;
	}

	/**
	 * @param pFailureParametericCSVEmailMessage the FailureParametericCSVEmailMessage to set
	 */
	public void setFailureParametericCSVEmailMessage(
			String pFailureParametericCSVEmailMessage) {
		mFailureParametericCSVEmailMessage = pFailureParametericCSVEmailMessage;
	}

	/**
	 * @return the SuccParametericCSVMessage
	 */
	public String getSuccParametericCSVMessage() {
		return mSuccParametericCSVMessage;
	}

	/**
	 * @param pSuccParametericCSVMessage the SuccParametericCSVMessage to set
	 */
	public void setSuccParametericCSVMessage(String pSuccParametericCSVMessage) {
		mSuccParametericCSVMessage = pSuccParametericCSVMessage;
	}

}
