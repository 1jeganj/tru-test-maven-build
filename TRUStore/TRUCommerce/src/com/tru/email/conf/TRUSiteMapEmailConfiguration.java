package com.tru.email.conf;

import java.util.List;

import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailInfoImpl;

/**
 * The Class TRUFeedEmailConfiguration.
 * @author Professional Access
 * @version 1.0
 */

public class TRUSiteMapEmailConfiguration extends GenericService {
	/** Property to hold Failure Template Email Info. */
	private TemplateEmailInfoImpl mFailureTemplateEmailInfo;

	/** Property to hold Success Template Email Info. */
	private TemplateEmailInfoImpl mSuccessTemplateEmailInfo;
	 
	/** The m success site map email message. */
	private String mSuccessSiteMapEmailMessage;
	
	/** The m success site map to ftp location message. */
	private String mSuccessSiteMapToFtpLocationMessage;
	
	/** The m failure exception message. */
	private String mFailureExceptionMessage;

	/** Property to hold Recipients. */
	private List<String> mRecipients;
	
	/** The m success site map email message. */
	private String mFailureSiteMapEmailMessage;
	
	/** The m success site map message. */
	private String mSuccSiteMapMessage;

	/**
	 * Gets the recipients.
	 * 
	 * @return the recipients
	 */
	public List<String> getRecipients() {
		return mRecipients;
	}

	/**
	 * Sets the recipients.
	 * 
	 * @param pRecipients
	 *            the mRecipients to set
	 */
	public void setRecipients(List<String> pRecipients) {
		mRecipients = pRecipients;
	}

	/**
	 * Gets the failure template email info.
	 * 
	 * @return the failureTemplateEmailInfo
	 */
	public TemplateEmailInfoImpl getFailureTemplateEmailInfo() {
		return mFailureTemplateEmailInfo;
	}

	/**
	 * Sets the failure template email info.
	 * 
	 * @param pFailureTemplateEmailInfo
	 *            the mFailureTemplateEmailInfo to set
	 */
	public void setFailureTemplateEmailInfo(
			TemplateEmailInfoImpl pFailureTemplateEmailInfo) {
		mFailureTemplateEmailInfo = pFailureTemplateEmailInfo;
	}

	/**
	 * Gets the success template email info.
	 * 
	 * @return the successTemplateEmailInfo
	 */
	public TemplateEmailInfoImpl getSuccessTemplateEmailInfo() {
		return mSuccessTemplateEmailInfo;
	}

	/**
	 * Sets the success template email info.
	 * 
	 * @param pSuccessTemplateEmailInfo
	 *            the successTemplateEmailInfo to set
	 */
	public void setSuccessTemplateEmailInfo(
			TemplateEmailInfoImpl pSuccessTemplateEmailInfo) {
		mSuccessTemplateEmailInfo = pSuccessTemplateEmailInfo;
	}

	/**
	 * Gets the success site map email message.
	 *
	 * @return the mSuccessSiteMapEmailMessage
	 */
	public String getSuccessSiteMapEmailMessage() {
		return mSuccessSiteMapEmailMessage;
	}

	/**
	 * Sets the success site map email message.
	 *
	 * @param pSuccessSiteMapEmailMessage the mSuccessSiteMapEmailMessage to set
	 */
	public void setSuccessSiteMapEmailMessage(
			String pSuccessSiteMapEmailMessage) {
		this.mSuccessSiteMapEmailMessage = pSuccessSiteMapEmailMessage;
	}

	/**
	 * Gets the success site map to ftp location message.
	 *
	 * @return the mSuccessSiteMapToFtpLocationMessage
	 */
	public String getSuccessSiteMapToFtpLocationMessage() {
		return mSuccessSiteMapToFtpLocationMessage;
	}

	/**
	 * Sets the success site map to ftp location message.
	 *
	 * @param pSuccessSiteMapToFtpLocationMessage the mSuccessSiteMapToFtpLocationMessage to set
	 */
	public void setSuccessSiteMapToFtpLocationMessage(
			String pSuccessSiteMapToFtpLocationMessage) {
		this.mSuccessSiteMapToFtpLocationMessage = pSuccessSiteMapToFtpLocationMessage;
	}

	/**
	 * Gets the failure exception message.
	 *
	 * @return the mFailureExceptionMessage
	 */
	public String getFailureExceptionMessage() {
		return mFailureExceptionMessage;
	}

	/**
	 * Sets the failure exception message.
	 *
	 * @param pFailureExceptionMessage the mFailureExceptionMessage to set
	 */
	public void setFailureExceptionMessage(String pFailureExceptionMessage) {
		this.mFailureExceptionMessage = pFailureExceptionMessage;
	}

	/**
	 * @return the failureSiteMapEmailMessage
	 */
	public String getFailureSiteMapEmailMessage() {
		return mFailureSiteMapEmailMessage;
	}

	/**
	 * @param pFailureSiteMapEmailMessage the failureSiteMapEmailMessage to set
	 */
	public void setFailureSiteMapEmailMessage(
			String pFailureSiteMapEmailMessage) {
		mFailureSiteMapEmailMessage = pFailureSiteMapEmailMessage;
	}

	/**
	 * @return the succSiteMapMessage
	 */
	public String getSuccSiteMapMessage() {
		return mSuccSiteMapMessage;
	}

	/**
	 * @param pSuccSiteMapMessage the succSiteMapMessage to set
	 */
	public void setSuccSiteMapMessage(String pSuccSiteMapMessage) {
		mSuccSiteMapMessage = pSuccSiteMapMessage;
	}

}
