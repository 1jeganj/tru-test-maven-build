package com.tru.scheduler;


import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

/**
 * This class is CategoryImageScheduler which will get used to schedule category image job.
 * @author PA
 * @version 1.0 
 * 
 */
public class CategoryImageScheduler extends SingletonSchedulableService{


	/** Property to hold mEnable */

	private boolean mEnable;
	/*
	 * Property to hold mSeoReportManager
	 */
	private CategoryImageManager mCategoryImageManager;

	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable
	 *            the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * <b>This method will invoke the task based on the ScheduledJob name at the
	 * scheduled time</b><br>
	 * .
	 * 
	 * @param pScheduler
	 *            - Scheduler
	 * @param pScheduledJob
	 *            - ScheduledJob
	 */
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: CategoryImageScheduler.doScheduledTask()");
		}
		if (isEnable()) {
			doCategoryImageJob();
		}
		if (isLoggingDebug()) {
			logDebug("Exit From :: CategoryImageScheduler.doScheduledTask()");
		}
	}

	/***
	 * This method invokes generateCategoryImages()
	 */
	public void doCategoryImageJob() {
		if (isLoggingDebug()) {
			logDebug("Entering into :: CategoryImageScheduler.doCategoryImageJob()");
		}
		getCategoryImageManager().generateCategoryImages();
		if (isLoggingDebug()) {
			logDebug("Exit from :: CategoryImageScheduler.doCategoryImageJob()");
		}
	}

	/**
	 * @return the mCategoryImageManager
	 */
	public CategoryImageManager getCategoryImageManager() {
		return mCategoryImageManager;
	}

	/**
	 * @param pCategoryImageManager the mCategoryImageManager to set
	 */
	public void setCategoryImageManager(CategoryImageManager pCategoryImageManager) {
		this.mCategoryImageManager = pCategoryImageManager;
	}
}
