package com.tru.scheduler;

/**
 * This is CategoryImageConstants class to hold constants.
 * @author PA
 * @version 1.0 
 * 
 */
public class CategoryImageConstants {
	/** The Constant INTERGER_NUMBER_ONE. */
	public static final int INTEGER_NUMBER_ONE = 1;
	/** The Constant CATEGORY_ID. */
	public static final String CATEGORY_ID = "CATEGORY_ID";
	/** The Constant IMAGE_URL */
	public static final String IMAGE_URL = "imageUrl";
	/** The Constant CategoryImageURL */
	public static final String CATEGORY_IMAGE_URL = "CategoryImageURL";
	/** The Constant defaultCatalog */
	public static final String DEFAULT_CATALOG = "defaultCatalog";
}
