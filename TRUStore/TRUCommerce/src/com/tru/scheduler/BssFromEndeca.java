package com.tru.scheduler;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.commerce.inventory.InventoryException;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.endeca.navigation.DimValIdList;
import com.endeca.navigation.ENEConnection;
import com.endeca.navigation.ENEQuery;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERec;
import com.endeca.navigation.ERecSortKey;
import com.endeca.navigation.ERecSortKeyList;
import com.endeca.navigation.HttpENEConnection;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.inventory.TRUCoherenceInventoryManager;
import com.tru.common.TRUConstants;
import com.tru.endeca.assembler.TRUIndexingConfiguration;

/**
 * This class is used to get best SKU seller from endeca.
 * @author PA
 * @version 1.0
 */

public class BssFromEndeca extends GenericService{
	/**
	 * property to hold mCoherenceInventoryManager.
	 */
	private TRUCoherenceInventoryManager mCoherenceInventoryManager;
	/**
	 * property to hold mDoInventoryCheck.
	 */
	private boolean mDoInventoryCheck;
	/**
	 * property to hold mDimValCacheTools.
	 */
    private DimensionValueCacheTools mDimValCacheTools;
    /**
     * property to hold mIndexingCofiguration.
     */
    private TRUIndexingConfiguration mIndexingCofiguration;
    
	/**
	 * This method will make coherence call to find inventory is available or not for the particular Best Seller SKU.
	 * @return bssInventory int
	 * @param pBss String
	 */
	public int getBSSInventory(String pBss){
		int bssInventory = 0;
		if (isLoggingDebug()) {
			logDebug("Begin method :: BssFromEndeca.getBSSInventory()");
		}
		try {
			bssInventory = getCoherenceInventoryManager().queryAvailabilityStatus(pBss);
		} catch (InventoryException invExe) {
			if (isLoggingError()) {
				logError("BssFromEndeca.getBSSInventory():: Inventory Exception occured while querying for inventory", invExe);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("bssInventory: {0} .",bssInventory);
			logDebug("End method :: BssFromEndeca.getBSSInventory()");
		}
		return bssInventory;
	}
	/**
	 * This method will make coherence call to find inventory is available or not for the particular Best Seller SKU 
	 * which is of type in store pickup.
	 * @param pBss String 
	 * @param pLocationIds List<String>
	 * @return bssInventory int
	 */
	 public List<String> getBSSInventory(String pBss,List<String> pLocationIds){
		 if (isLoggingDebug()) {
				logDebug("Begin method :: BssFromEndeca.getBSSInventory() Store Pickup");
			}
		List<String> inStockLocationIds = null;
		inStockLocationIds = getCoherenceInventoryManager().validateMultiStoreInventory(pBss, pLocationIds);
		if (isLoggingDebug()) {
			vlogDebug("inStockLocationIds: {0} .",inStockLocationIds);
			logDebug("End method :: BssFromEndeca.getBSSInventory() Store Pickup");
		}
		return inStockLocationIds;
	 }
    /**
     * This method will return the best seller sku image url.
     * @param pCatId String
     * @param pNavigationOffest Long
     * @param pEneConnection ENEConnection
     * @param pLocationIds List<String>
     * @return bssImageUrlMap Map<String, String>
     */
    public Map<String, String> getBestSellerSku(String pCatId, Long pNavigationOffest, ENEConnection pEneConnection, List<String> pLocationIds){
    	ENEQueryResults results =null;
    	if (isLoggingDebug()) {
    		logDebug("Begin method :: BssFromEndeca.getBestSellerSku()");
			vlogDebug("CatId: {0} .",pCatId);
    	}
    	Map <String, String> bssImageUrlMap = new LinkedHashMap<String, String>();
    	
    	String navId= getnavigationId(pCatId);
    	 try {
    		 if(pEneConnection != null && !StringUtils.isEmpty(navId)){
    			 results = pEneConnection.query(createNavigationQuery(navId , pNavigationOffest));
    		 }
			 if(results == null){
				return null;
			 }
			 for (Object result : results.getNavigation().getERecs()) {
	            final ERec eRec = (ERec) result;
	            String imageUrl = (String) eRec.getProperties().get(TRUConstants.SEARCH_IMAGE_URL);
	            String skuId = (String) eRec.getProperties().get(TRUConstants.SKU_REPOSITORYID);
	        	if(isDoInventoryCheck()){
	        		if (isLoggingDebug()) {
	            		logDebug("BssFromEndeca.getBestSellerSku() : Inventory check");
	        		}
					int bssInventoryStatus = getBSSInventory(skuId);
					if (bssInventoryStatus == TRUCommerceConstants.INT_IN_STOCK || bssInventoryStatus == TRUCommerceConstants.INT_PRE_ORDER
							|| bssInventoryStatus == TRUCommerceConstants.INT_LIMITED_STOCK) {
						if(StringUtils.isNotBlank(imageUrl)){
							bssImageUrlMap.put(skuId, imageUrl);
							break;
						}
					}
					else{
						List<String> inStoreLocationIds = null;
						inStoreLocationIds = getBSSInventory(skuId,pLocationIds);
						if(!inStoreLocationIds.isEmpty() && StringUtils.isNotBlank(imageUrl) ){
							bssImageUrlMap.put(skuId, imageUrl);
							break;
						}
					}
				}
				else{
					if(StringUtils.isNotBlank(imageUrl)){
						bssImageUrlMap.put(skuId, imageUrl);
						break;
					}
				}
			}
		} catch (ENEQueryException e) {
			if (isLoggingError()) {	
				logError(" BssFromEndeca.getBestSellerSku() : ENEQueryException :{0}", e);	
			}
		}
    	 if (isLoggingDebug()) {
    		vlogDebug("bssImageUrlMap: {0} .",bssImageUrlMap);
     		logDebug("End method :: BssFromEndeca.getBestSellerSku()");
     	}
    	return bssImageUrlMap;
    }
    
    /**
     * This method will create the navigation query.
     * @param pValue dimension value id
     * @param pNavigationOffest start index
     * @return query endecaquery
     */

	@SuppressWarnings("unchecked")
	private ENEQuery createNavigationQuery(final String pValue , Long pNavigationOffest) {
    	if (isLoggingDebug()) {
    		logDebug("Begin method :: BssFromEndeca.createNavigationQuery()");
    	}
    	
        final ENEQuery query = new ENEQuery();
        ERecSortKeyList erecKeyList= new ERecSortKeyList();
        ERecSortKey erecSortKey = new ERecSortKey(TRUConstants.BEST_SELLER_SORT, false); 
        erecKeyList.add(erecSortKey);
        
        final DimValIdList dimValIdList = new DimValIdList(pValue);
        query.setNavDescriptors(dimValIdList);
        query.setNavActiveSortKeys(erecKeyList);
        
        /*query.setNavSortKey(TRUConstants.BEST_SELLER_SORT);
        query.setNavSortOrder(TRUConstants.ONE);*/
        query.setNavERecsOffset(pNavigationOffest);
        if (isLoggingDebug()) {
    		logDebug("End method :: BssFromEndeca.createNavigationQuery()");
    	}
        return query;
    }

    /**
     * Creates the connection.
     *
     * @return eneConnection
     */
    public  ENEConnection createConnection() {
    	if(getIndexingCofiguration() == null){
    		return null;
    	}
    	if (isLoggingDebug()) {
    		logDebug("Begin method :: BssFromEndeca.createConnection()");
    	}
    	String mdexHostName = getIndexingCofiguration().getDefaultMdexHostName();
    	int mdexPort = getIndexingCofiguration().getDefaultMdexPort();
    	if (isLoggingDebug()) {
			vlogDebug("mdexHostName: {0} :: mdexPort: {1}",mdexHostName,mdexPort);
    	}
    	ENEConnection eneConnection = new HttpENEConnection(mdexHostName, mdexPort);
    	if (isLoggingDebug()) {
    		logDebug("End method :: BssFromEndeca.createConnection()");
    	}
        return eneConnection;
    }
    /**
     * This method will get the navigation id.
     * @param pCatId String
     * @return navigationId String
     */
    private String getnavigationId(String pCatId){
    	if (isLoggingDebug()) {
    		logDebug("Begin method :: BssFromEndeca.getnavigationId()");
    	}
    	String navigationId = null;
    	DimensionValueCacheTools dimValCacheTools = getDimValCacheTools();
    	if (!StringUtils.isBlank(pCatId) && dimValCacheTools != null) {
    		DimensionValueCacheObject cachedEntry  = dimValCacheTools.get(pCatId , null);
    			if(cachedEntry !=  null){
    				navigationId = cachedEntry.getDimvalId();
    			}
    	}
    	if (isLoggingDebug()) {
			vlogDebug("CatId: {0} :: Navigation Id :{1}.",pCatId,navigationId);
    		logDebug("End method :: BssFromEndeca.getnavigationId()");
    	}
    	return navigationId;
    }

	/**
	 * @return the dimValCacheTools.
	 */
	public DimensionValueCacheTools getDimValCacheTools() {
		return mDimValCacheTools;
	}

	/**
	 * @param pDimValCacheTools the dimValCacheTools to set.
	 */
	public void setDimValCacheTools(DimensionValueCacheTools pDimValCacheTools) {
		mDimValCacheTools = pDimValCacheTools;
	}

	/**
	 * @return the indexingCofiguration
	 */
	public TRUIndexingConfiguration getIndexingCofiguration() {
		return mIndexingCofiguration;
	}

	/**
	 * @param pIndexingCofiguration the indexingCofiguration to set.
	 */
	public void setIndexingCofiguration(TRUIndexingConfiguration pIndexingCofiguration) {
		mIndexingCofiguration = pIndexingCofiguration;
	}

	/**
	 * @return the doInventoryCheck.
	 */
	public boolean isDoInventoryCheck() {
		return mDoInventoryCheck;
	}

	/**
	 * @param pDoInventoryCheck the doInventoryCheck to set.
	 */
	public void setDoInventoryCheck(boolean pDoInventoryCheck) {
		mDoInventoryCheck = pDoInventoryCheck;
	}

	/**
	 * @return the coherenceInventoryManager.
	 */
	public TRUCoherenceInventoryManager getCoherenceInventoryManager() {
		return mCoherenceInventoryManager;
	}

	/**
	 * @param pCoherenceInventoryManager the coherenceInventoryManager to set.
	 */
	public void setCoherenceInventoryManager(TRUCoherenceInventoryManager pCoherenceInventoryManager) {
		mCoherenceInventoryManager = pCoherenceInventoryManager;
	}

}
