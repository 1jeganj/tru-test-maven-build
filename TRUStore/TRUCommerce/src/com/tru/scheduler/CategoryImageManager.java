package com.tru.scheduler;

import java.util.Calendar;
import java.util.Map;

import atg.multisite.SiteManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.utils.TRUClassificationsTools;

/**
 * This class is CategoryImageManager which used to generate category images.
 * @author PA
 * @version 1.0 
 * 
 */
public class CategoryImageManager extends GenericService{
	/**
	 * property to hold mCategoryImageTools
	 */
	private CategoryImageTools mCategoryImageTools;
	/**
	 * This property hold reference for TRUClassificationsTools.
	 */
	private TRUClassificationsTools mClassificationsTools;
	/**
	 * This method is used to insert category id and image url in the CategoryImageURLRepository
	 */
	public void generateCategoryImages(){
		if (isLoggingDebug()) {
			Calendar cal = Calendar.getInstance();
			logDebug("Entered into CategoryImageManager:generateCategoryImages() : " + cal.getTime().toString());
		}
		RepositoryItem[] sites = null;
		try {
			sites = SiteManager.getSiteManager().getActiveSites();
			Map<String,String> categoryImageUrl = getCategoryImageTools().setBestSellerSkuCatImage(sites);
			getCategoryImageTools().persistCategoryImageUrl(categoryImageUrl);
			getClassificationsTools().flush();
		}
		catch (RepositoryException repoExe) {
			if (isLoggingError()) {
				logError("RepositoryException occured while fetching the sites in CategoryImageManager:generateCategoryImages()", repoExe);
			}
		}
	}

	/**
	 * @return the mCategoryImageTools
	 */
	public CategoryImageTools getCategoryImageTools() {
		return mCategoryImageTools;
	}
	/**
	 * @param pCategoryImageTools the mCategoryImageTools to set
	 */
	public void setCategoryImageTools(CategoryImageTools pCategoryImageTools) {
		this.mCategoryImageTools = pCategoryImageTools;
	}
	/**
	 * Gets the TRUClassificationsTools.
	 *
	 * @return the TRUClassificationsTools
	 */

	public TRUClassificationsTools getClassificationsTools() {
		return mClassificationsTools;
	}

	/**
	 * Sets the TRUClassificationsTools.
	 *
	 * @param pClassificationsTools the TRUClassificationsTools to set
	 */

	public void setClassificationsTools(TRUClassificationsTools pClassificationsTools) {
		mClassificationsTools = pClassificationsTools;
	}
}
