package com.tru.scheduler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;

import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;
import com.endeca.navigation.ENEConnection;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.common.TRUConstants;

/**
 * This class is CategoryImageTools which will get used for category image transaction.
 * @author PA
 * @version 1.0 
 * 
 */
public class CategoryImageTools extends GenericService{
	
	/** property to hold mLocationRepository. */
	private Repository mLocationRepository;
	
	/** property to hold mCategoryImageUrlRepository. */
	private MutableRepository mCategoryImageUrlRepository;
	
	/** property to hold mBssFromEndeca. */
	private BssFromEndeca mBssFromEndeca;

	/** property to hold mCatalogProperties. */
	private TRUCatalogProperties mCatalogProperties;
	/** 
	 * The product catalog. 
	 */
	private MutableRepository mProductCatalog;
	
	/**
	 * This method will return all the location ids.
	 *
	 * @return locationIds List<String>
	 */
	public List<String> getLocationIds(){
		if (isLoggingDebug()) {
			logDebug("Begin method :: CategoryImageTools.getLocationIds()");
		}
		List<String> locationIds = null;
		Repository locationRepository = getLocationRepository();
		try {
			RepositoryItemDescriptor locationRepositoryDes = locationRepository.getItemDescriptor(getCatalogProperties().getLocationItemDescriptor());
			RepositoryView locationRepoView = locationRepositoryDes.getRepositoryView();
			QueryBuilder locRepoBuilder = locationRepoView.getQueryBuilder();
			Query locRepoQuery = locRepoBuilder.createUnconstrainedQuery();
			RepositoryItem[] allLocationItems = locationRepoView.executeQuery(locRepoQuery);
			if(allLocationItems == null){
				return null;
			}
				locationIds = new ArrayList<String>();
				for(RepositoryItem storeItem :allLocationItems) {
					if (storeItem != null) {
						String locationId = (String) storeItem.getPropertyValue(getCatalogProperties().getLocationId());
						if(!StringUtils.isEmpty(locationId)) {
							locationIds.add(locationId);
						}
					}
				}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("Repository Exception occured in method CategoryImageTools.getLocationIds()", e);
			}
		}
		if (isLoggingDebug()){
			vlogDebug("Location Id : {0} .",locationIds);
			logDebug("End method :: CategoryImageTools.getLocationIds()");
		}
		return locationIds;
	}
	
	/**
	 * This method will return the category image url for a particular category.
	 *
	 * @param pCategoryId category id
	 * @param pLocationIds list of location ids
	 * @return imageUrl String
	 */
	@SuppressWarnings("rawtypes")
	public String getImageUrlFromEndeca(String pCategoryId,List<String> pLocationIds){
		if (isLoggingDebug()) {
			logDebug("Begin method :: CategoryImageTools.getImageUrlFromEndeca()");
		}
		String imageUrl =null;
		long offset = TRUCommerceConstants.INT_ZERO;
		boolean recordExists = false;
		ENEConnection mdexConnection = getBssFromEndeca().createConnection();
		if(mdexConnection == null){
			if (isLoggingDebug()) {
				logDebug("CategoryImageTools.getImageUrlFromEndeca() : Unable to connect to Endeca box");
			}
			return null;
		}
		do{
				Map<String, String> bssImageUrlMap = getBssFromEndeca(pCategoryId,offset,mdexConnection,pLocationIds);
				if(bssImageUrlMap!=null && !bssImageUrlMap.isEmpty()){
					Iterator<Entry<String, String>> entries = bssImageUrlMap.entrySet().iterator();
					while(entries.hasNext()){
						Map.Entry entry = entries.next();
						imageUrl = (String) entry.getValue();
						if(StringUtils.isNotBlank(imageUrl)){
							recordExists = true;
							break;
						}
						offset=offset+TRUConstants.TEN;
					}
				}
				else{
					recordExists = true;
				}
		}while(!recordExists);
		if (isLoggingDebug()){
			vlogDebug("Image Url: {0} .",imageUrl);
			logDebug("End method :: CategoryImageTools.getImageUrlFromEndeca()");
		}
		return imageUrl;
	}
	
	/**
	 * This method will retrive the best seller sku from category id.
	 *
	 * @param pCatId String
	 * @param pOffset start index
	 * @param pEneConnection reference of ENEConnection
	 * @param pLocationIds list of location ids
	 * @return bssImageUrlMap Map<String, String>
	 */
	public Map<String, String> getBssFromEndeca(String pCatId, long pOffset, ENEConnection pEneConnection, List<String> pLocationIds){
		if (isLoggingDebug()) {
			logDebug("Begin method :: CategoryImageTools.getBssFromEndeca()");
		}
		
		Map<String, String> bssImageUrlMap = getBssFromEndeca().getBestSellerSku(pCatId, pOffset, pEneConnection, pLocationIds);
		
		if (isLoggingDebug()) {
			logDebug("End method :: CategoryImageTools.getBssFromEndeca()");
		}
		return bssImageUrlMap;
	}

	/**
	 * This method will return all the leaf categories of a root category.
	 *
	 * @param pSites list of site RepositoryItems
	 * @return categoryImageUrl map of type RepositoryItem
	 */
	public Map<String,String> setBestSellerSkuCatImage(RepositoryItem[] pSites){
		if (isLoggingDebug()) {
			logDebug("Begin method :: CategoryImageTools.setBestSellerSkuCatImage()");
		}
		Map<String,String> categoryImageUrl = new HashMap<String,String>();
		
		Set<String> categoryIdList = getAllCategoryId(pSites);
		List<String> locationIds = getLocationIds();
		
		for (String categoryId : categoryIdList) {
			String imageUrl = getImageUrlFromEndeca(categoryId,locationIds);
			categoryImageUrl.put(categoryId, imageUrl);
		}
		
		if (isLoggingDebug()) {
			vlogDebug("Category - ImageUrl: {0} .",categoryImageUrl);
			logDebug("End method :: CategoryImageTools.setBestSellerSkuCatImage()");
		}
		return categoryImageUrl;
	}
	
	/**
	 * Gets the all category id.
	 *
	 * @param pSites the sites
	 * @return the all category id
	 */
	private Set<String> getAllCategoryId(RepositoryItem[] pSites){
		if (isLoggingDebug()) {
			logDebug("Begin method :: CategoryImageTools.getAllCategoryId()");
		}
		Set<String> categoryIdList = new HashSet<String>();
		if(pSites == null){
			return null;
		}
		for(RepositoryItem site : pSites){
			if(site == null){
				if (isLoggingDebug()) {
					logDebug("End method :: CategoryImageTools.getAllCategoryId() : As no Site id found");
				}
				continue;
			}
			 try {
					RepositoryView view = getProductCatalog().getView(getCatalogProperties().getCategoryItemName());
					if(view == null){
						return null;
					}
					 QueryBuilder builder = view.getQueryBuilder();
		             QueryExpression categoryQuery  = builder.createPropertyQueryExpression(TRUConstants.SITE_IDS);
		             QueryExpression eachCategory = builder.createConstantQueryExpression(site.getRepositoryId());
		             Query categoryResult = builder.createComparisonQuery(categoryQuery, eachCategory, QueryBuilder.EQUALS);
		             RepositoryItem[] categoryIdItems = view.executeQuery(categoryResult);	
		             
		             if(categoryIdItems == null){
		            	 continue;
		             }
		             for (RepositoryItem repositoryItem : categoryIdItems) {
		            	 if(repositoryItem != null){
		            		 categoryIdList.add(repositoryItem.getRepositoryId());
		            	 }
					}
				} catch (RepositoryException e) {
					if (isLoggingError()) {
						logError("Repository Exception occured in method CategoryImageTools.getAllCategoryId()", e);
					}
				}
		}
		if (isLoggingDebug()) {
			vlogDebug("All Category id : {0} .",categoryIdList);
			logDebug("End method :: CategoryImageTools.getAllCategoryId()");
		}
		return categoryIdList;
	}
	
	/**
	 * This method will add or update the category image url in db.
	 *
	 * @param pCategoryImageUrl Map<String,String>
	 */
	@SuppressWarnings("rawtypes")
	public void persistCategoryImageUrl(Map<String,String> pCategoryImageUrl){
		if(isLoggingDebug()){
			logDebug("Enter into : CategoryImageTools and method : persistCategoryImageUrl ");
		}
		if(pCategoryImageUrl!=null && !pCategoryImageUrl.isEmpty()){
			Iterator<Entry<String, String>> entries = pCategoryImageUrl.entrySet().iterator();
			while(entries.hasNext()){
				MutableRepositoryItem repoItem = null;
				Map.Entry entry = (Map.Entry) entries.next();
				if(StringUtils.isNotBlank((String) entry.getValue())){
					try {
						repoItem = ((MutableRepository) getCategoryImageUrlRepository()).getItemForUpdate(entry.getKey().toString(), CategoryImageConstants.CATEGORY_IMAGE_URL);
						if(repoItem!=null){
							repoItem.setPropertyValue(getCatalogProperties().getProductImageUrl(), entry.getValue().toString());
							getCategoryImageUrlRepository().updateItem(repoItem);
						}
						else{
							repoItem = getCategoryImageUrlRepository().createItem(entry.getKey().toString(),CategoryImageConstants.CATEGORY_IMAGE_URL);
							repoItem.setPropertyValue(getCatalogProperties().getProductImageUrl(), entry.getValue().toString());
							getCategoryImageUrlRepository().addItem(repoItem);
						}
					} catch (RepositoryException e) {
						if (isLoggingError()) {
							logError("Repository Exception occured while querying CategoryImageUrlRepository ", e);
						}
					}
				}
			}
		}
		if (isLoggingInfo()) {
			logInfo("Completed Inserting values ");
		}
	}
	
	/**
	 * Gets the product catalog.
	 *
	 * @return the productCatalog
	 */
	public MutableRepository getProductCatalog() {
		return mProductCatalog;
	}

	/**
	 * Sets the product catalog.
	 *
	 * @param pProductCatalog the productCatalog to set
	 */
	public void setProductCatalog(MutableRepository pProductCatalog) {
		mProductCatalog = pProductCatalog;
	}
	
	/**
	 * Gets the bss from endeca.
	 *
	 * @return the bssFromEndeca
	 */
	public BssFromEndeca getBssFromEndeca() {
		return mBssFromEndeca;
	}
	
	/**
	 * Sets the bss from endeca.
	 *
	 * @param pBssFromEndeca the bssFromEndeca to set
	 */
	public void setBssFromEndeca(BssFromEndeca pBssFromEndeca) {
		mBssFromEndeca = pBssFromEndeca;
	}
	
	/**
	 * Gets the catalog properties.
	 *
	 * @return the catalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}
	
	/**
	 * Sets the catalog properties.
	 *
	 * @param pCatalogProperties the catalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}
	
	/**
	 * Gets the category image url repository.
	 *
	 * @return the categoryImageUrlRepository
	 */
	public MutableRepository getCategoryImageUrlRepository() {
		return mCategoryImageUrlRepository;
	}
	
	/**
	 * Sets the category image url repository.
	 *
	 * @param pCategoryImageUrlRepository the categoryImageUrlRepository to set
	 */
	public void setCategoryImageUrlRepository(
			MutableRepository pCategoryImageUrlRepository) {
		mCategoryImageUrlRepository = pCategoryImageUrlRepository;
	}
	
	/**
	 * Gets the location repository.
	 *
	 * @return the locationRepository
	 */
	public Repository getLocationRepository() {
		return mLocationRepository;
	}
	
	/**
	 * Sets the location repository.
	 *
	 * @param pLocationRepository the locationRepository to set
	 */
	public void setLocationRepository(Repository pLocationRepository) {
		mLocationRepository = pLocationRepository;
	}
}
