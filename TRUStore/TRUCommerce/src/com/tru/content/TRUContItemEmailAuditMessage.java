/**
 * 
 */
package com.tru.content;

import java.sql.Date;

/**
 * @author Professional Access
 * @version 1.0
 */
public class TRUContItemEmailAuditMessage {

	private String mEmailAuditId;

	private String mEmailSender;

	private String mEmailType;

	private Date mMessageData;

	private Date mStatus;

	private Date mMessageSendingResponse;

	private Date mCreatedTimestamp;

	/**
	 * @return the emailAuditId
	 */
	public String getEmailAuditId() {
		return mEmailAuditId;
	}

	/**
	 * @param pEmailAuditId
	 *            the emailAuditId to set
	 */
	public void setEmailAuditId(String pEmailAuditId) {
		mEmailAuditId = pEmailAuditId;
	}

	/**
	 * @return the emailSender
	 */
	public String getEmailSender() {
		return mEmailSender;
	}

	/**
	 * @param pEmailSender
	 *            the emailSender to set
	 */
	public void setEmailSender(String pEmailSender) {
		mEmailSender = pEmailSender;
	}

	/**
	 * @return the emailType
	 */
	public String getEmailType() {
		return mEmailType;
	}

	/**
	 * @param pEmailType
	 *            the emailType to set
	 */
	public void setEmailType(String pEmailType) {
		mEmailType = pEmailType;
	}

	/**
	 * @return the messageData
	 */
	public Date getMessageData() {
		return mMessageData;
	}

	/**
	 * @param pMessageData
	 *            the messageData to set
	 */
	public void setMessageData(Date pMessageData) {
		mMessageData = pMessageData;
	}

	/**
	 * @return the status
	 */
	public Date getStatus() {
		return mStatus;
	}

	/**
	 * @param pStatus
	 *            the status to set
	 */
	public void setStatus(Date pStatus) {
		mStatus = pStatus;
	}

	/**
	 * @return the messageSendingResponse
	 */
	public Date getMessageSendingResponse() {
		return mMessageSendingResponse;
	}

	/**
	 * @param pMessageSendingResponse
	 *            the messageSendingResponse to set
	 */
	public void setMessageSendingResponse(Date pMessageSendingResponse) {
		mMessageSendingResponse = pMessageSendingResponse;
	}

	/**
	 * @return the createdTimestamp
	 */
	public Date getCreatedTimestamp() {
		return mCreatedTimestamp;
	}

	/**
	 * @param pCreatedTimestamp
	 *            the createdTimestamp to set
	 */
	public void setCreatedTimestamp(Date pCreatedTimestamp) {
		mCreatedTimestamp = pCreatedTimestamp;
	}

}
