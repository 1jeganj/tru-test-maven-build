package com.tru.content;
import java.sql.Date;
/**
 * @author Professional Access
 * @version 1.0
 */
public class TRUContItemAuditMessage {
	/** The m tru get site type mOrderId. */
	private String mOrderId;
	/** The m tru get site type mMessageType. */
	private String mMessageType;
	/** The m tru get site type mMessageData. */
	private String mMessageData;
	/** The m tru get site type mAuditDate. */
	private Date mAuditDate;

	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * @param pOrderId
	 *            the orderId to set
	 */
	public void setOrderId(String pOrderId) {
		mOrderId = pOrderId;
	}

	/**
	 * @return the messageType
	 */
	public String getMessageType() {
		return mMessageType;
	}

	/**
	 * @param pMessageType
	 *            the messageType to set
	 */
	public void setMessageType(String pMessageType) {
		mMessageType = pMessageType;
	}

	/**
	 * @return the messageData
	 */
	public String getMessageData() {
		return mMessageData;
	}

	/**
	 * @param pMessageData
	 *            the messageData to set
	 */
	public void setMessageData(String pMessageData) {
		mMessageData = pMessageData;
	}

	/**
	 * @return the auditDate
	 */
	public Date getAuditDate() {
		return mAuditDate;
	}

	/**
	 * @param pAuditDate
	 *            the auditDate to set
	 */
	public void setAuditDate(Date pAuditDate) {
		mAuditDate = pAuditDate;
	}

}
