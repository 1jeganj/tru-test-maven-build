package com.tru.deployment.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.xml.ws.ProtocolException;

import atg.core.util.StringUtils;
import atg.deployment.agent.CatalogMaintenanceHelper;
import atg.deployment.agent.DeploymentAgent;
import atg.deployment.common.event.DeploymentEvent;
import atg.json.JSONException;
import atg.json.JSONObject;

import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;

/**
 * 
 */

/**
 * @author Professional Access
 *
 */
public class TRUCatalogMaintenanceHelper extends CatalogMaintenanceHelper {
	
	/** This holds the reference for FAILED_HTTP_ERROR_CODE *. */
	public static final String FAILED_HTTP_ERROR_CODE = "Failed : HTTP error code : ";
	/** property to hold mConfiguration boolean check. */
	private TRUConfiguration mConfiguration;
	
	/** (non-Javadoc)
	 * @see atg.deployment.agent.CatalogMaintenanceHelper#deploymentEvent(atg.deployment.common.event.DeploymentEvent)
	 */
	@Override
	public void deploymentEvent(DeploymentEvent pEvent) {
		DeploymentAgent agent = getDeploymentAgent();
		if (agent.isLoggingDebug()) {
			agent.logDebug("Entering into TRUCatalogMaintenanceHelper.deploymentEvent() method");
		}
		String[] projects = null;
		String projectName = null;
		boolean isRunCMS=false;
		if (getConfiguration().isValidateAgainstProjectId()) {
			projects = pEvent.getDeploymentProjectIDs();
			if (projects != null) {
				projectName = getProjectNameWithItRestCall(projects[0]);
				List<String> projectNames=getConfiguration().getCatalogFeedProjectNameList();
				for(String cmsProjectName:projectNames){
					if (StringUtils.isNotBlank(projectName)
							&& projectName.contains(cmsProjectName)) {
						isRunCMS=true;
						break;
					}
				}
				
				if (isRunCMS) {
					super.deploymentEvent(pEvent);
				} else {
					return;
				}
			} else {
				super.deploymentEvent(pEvent);
			}
		} else {
			super.deploymentEvent(pEvent);
		}
		if (agent.isLoggingDebug()) {
			agent.logDebug("Exiting from TRUCatalogMaintenanceHelper.deploymentEvent() method");
		}
	}
	
	/**
	 * This method is used to call projectNameFromID method which is available in March module through rest URL by passing.
	 * project id which need to get project Name.
	 * 
	 * @param pProjectId
	 *            the project id
	 */
	private String getProjectNameWithItRestCall(String pProjectId) {
		DeploymentAgent agent = getDeploymentAgent();
		if (agent.isLoggingDebug()) {
			agent.logDebug("Entering into TRUCatalogMaintenanceHelper.getProjectNameWithItRestCall() method");
		}
		String projectName = null; 
		try {
			if (getConfiguration() != null && !StringUtils.isEmpty(getConfiguration().getProjectNameWithIdRestServiceURL()) && !StringUtils.isEmpty(pProjectId)) {
				if (agent.isLoggingDebug()) {
					agent.logDebug("Rest Configuration URL" + getConfiguration().getProjectNameWithIdRestServiceURL());
				}
				final URL url = new URL(getConfiguration().getProjectNameWithIdRestServiceURL() + pProjectId.toString());
				if (agent.isLoggingDebug()) {
					agent.vlogDebug("pProjectId: {0} url : {1}", pProjectId,url);
				}
				final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod(TRUConstants.POST);
				conn.setRequestProperty(TRUConstants.CONTENT_TYPE, TRUConstants.APPLICATION_JSON);
				final OutputStream os = conn.getOutputStream();
				os.flush();
				conn.connect();
				BufferedReader responseBuffer = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				String output;
				while ((output = responseBuffer.readLine()) != null) {
					projectName = output;
				}
				if (StringUtils.isNotBlank(projectName)) {
					JSONObject jObject  = new JSONObject(projectName);
					projectName = (String) jObject.get(TRUConstants.PROJECTNAME);
					if (agent.isLoggingDebug()) {
						agent.vlogDebug("projectName: {0}", projectName);
					}
				}
				if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
					throw new ProtocolException(FAILED_HTTP_ERROR_CODE + conn.getResponseCode());
				}
				conn.disconnect();
			}
		} catch (MalformedURLException e) {
			if (agent.isLoggingError()) {
				agent.vlogError("MalformedURLException: While processing MalformedURLException with exception : {0} ", e);
			}
		} catch (IOException e) {
			if (agent.isLoggingError()) {
				agent.vlogError("IOException: While processing IOException with exception : {0} ", e);
			}
		}
		 catch (JSONException e) {
			 if (agent.isLoggingError()) {
					agent.vlogError("JSONException: While processing JSONException with exception : {0} ", e);
				}
			}
		if (agent.isLoggingDebug()) {
			agent.logDebug("Exiting from TRUCatalogMaintenanceHelper.getProjectNameWithItRestCall() method");
		}
		return projectName;
			
	}
	
	/**
	 * This method is to get configuration
	 *
	 * @return the configuration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * This method sets configuration with pConfiguration
	 *
	 * @param pConfiguration the configuration to set
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		this.mConfiguration = pConfiguration;
	}
}
