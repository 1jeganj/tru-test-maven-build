package com.tru.deployment.listener;

import atg.nucleus.GenericService;
import atg.service.datacollection.DataListener;


/**
 *  This Class is the Implementation for DataListener and extends GenericService .
 *  
 * @author   : PA
 * @version  : 1.0
 * 
 * */
public class TRUAkamaiCacheDataListener extends GenericService implements DataListener
{
	
	private TRUAkamaiCacheInvalidatorService mAkamaiCacheInvalidatorService;
	
	/**
	 * This method calls the logic for logging info 
	 * @param pParamObject the Object
	 */
	public void addDataItem(Object pParamObject) {
		if(isLoggingDebug()){
			logDebug("TRUCustomCacheDataListener.addDataItem method : "+ pParamObject);
		}
		getAkamaiCacheInvalidatorService().invalidateCache();
	}

	/**
	 * Gets the akamai cache invalidator service.
	 *
	 * @return the akamai cache invalidator service
	 */
	public TRUAkamaiCacheInvalidatorService getAkamaiCacheInvalidatorService() {
		return mAkamaiCacheInvalidatorService;
	}

	/**
	 * Sets the akamai cache invalidator service.
	 *
	 * @param pAkamaiCacheInvalidatorService the new akamai cache invalidator service
	 */
	public void setAkamaiCacheInvalidatorService(TRUAkamaiCacheInvalidatorService pAkamaiCacheInvalidatorService) {
		this.mAkamaiCacheInvalidatorService = pAkamaiCacheInvalidatorService;
	}


	
}

