package com.tru.deployment.listener;

import com.tru.integrations.akamai.scheduler.AkamaiCacheInvalidationJob;
import com.tru.integrations.exception.TRUIntegrationException;

import atg.nucleus.GenericService;

/**
 * This class provides method to invalidate the cache components.
 * 
 * @author P.A
 *
 */
public class TRUAkamaiCacheInvalidatorService extends GenericService{

	/**
	 * Property to hold mAkamaiCacheInvalidationJob.
	 */
	private AkamaiCacheInvalidationJob mAkamaiCacheInvalidationJob;
	

	/**
	 *<b>This Method will do following Operations:</b><BR>
	 * 1) It will fetch the components. 2) Flush the cache.
	 */
	public void invalidateCache() {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUAkamaiCacheInvalidatorService.invalidateCache method:");
		}
		try {
			getAkamaiCacheInvalidationJob().cacheInvalidation();
		} catch (TRUIntegrationException integrationException) {
			if (isLoggingError()) {
				vlogError(integrationException,"TRUIntegrationException occured in method deploymentEvent - For Akami Invalidate Cache");
			}
		}
		if (isLoggingDebug()) {
			logDebug("exiting from TRUAkamaiCacheInvalidatorService.invalidateCache method:");
		}
	}

	/**
	 * Gets the AkamaiCacheInvalidationJob
	 * 
	 * @return the AkamaiCacheInvalidationJob
	 */
	public AkamaiCacheInvalidationJob getAkamaiCacheInvalidationJob() {
		return mAkamaiCacheInvalidationJob;
	}

	/**
	 * Sets the AkamaiCacheInvalidationJob
	 * 
	 * @param pAkamaiCacheInvalidationJob the AkamaiCacheInvalidationJob to set
	 */
	public void setAkamaiCacheInvalidationJob(AkamaiCacheInvalidationJob pAkamaiCacheInvalidationJob) {
		mAkamaiCacheInvalidationJob = pAkamaiCacheInvalidationJob;
	}
}

