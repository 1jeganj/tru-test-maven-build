package com.tru.deployment.listener;

import atg.service.datacollection.DataListenerQueue;

/**
 *  This Class  extends DataListenerQueue .
 *  
 * @author   : PA
 * @version  : 1.0
 * 
 * */
public class TRUAkamaiCacheDataListenerQueue extends DataListenerQueue
{
	/**
	 * property to hold TRUDataListener component
	 */
	private TRUAkamaiCacheDataListener mDataListener;

	

	/**
	 * This method Starts the service
	 */
	public  void doStartService()
 {
		if (isLoggingDebug()) {
			logDebug("Entering TRUAkamaiCacheDataListenerQueue.doStartService method");
		}
		synchronized (this) {
			super.doStartService();
			addDataListener(getDataListener());
		}
		if (isLoggingDebug()) {
			logDebug("Exiting TRUAkamaiCacheDataListenerQueue.doStartService method");
		}
	}



	/**
	 * Gets the data listener.
	 *
	 * @return the data listener
	 */
	public TRUAkamaiCacheDataListener getDataListener() {
		return mDataListener;
	}



	/**
	 * Sets the data listener.
	 *
	 * @param pDataListener the new data listener
	 */
	public void setDataListener(TRUAkamaiCacheDataListener pDataListener) {
		this.mDataListener = pDataListener;
	}
	
}

