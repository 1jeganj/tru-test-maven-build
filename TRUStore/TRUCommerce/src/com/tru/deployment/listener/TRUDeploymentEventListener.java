package com.tru.deployment.listener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.ws.ProtocolException;

import atg.core.util.StringUtils;
import atg.deployment.common.event.DeploymentEvent;
import atg.deployment.common.event.DeploymentEventListener;
import atg.endeca.index.EndecaIndexingOutputConfig;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.multisite.SiteContextManager;
import atg.multisite.TRUSiteTools;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.cache.TRUCollectionProductCache;
import com.tru.cache.TRUProductCache;
import com.tru.cache.TRUPromotionCache;
import com.tru.commerce.catalog.custom.TRUPromotionUpdateService;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.ral.performanceutil.tol.cache.PARALATGCacheContainer;
import com.tru.utils.TRUClassificationsTools;

/**
 * This class implements OOTB DeploymentEventListener to invalidate TRUCollectionProductCache and TRUProductCache
 * Components based on the repository types available in deployed project.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUDeploymentEventListener extends GenericService implements DeploymentEventListener {

	//public static final String ATG_COMMERCE_CATALOG_PRODUCT_CATALOG = "/atg/commerce/catalog/ProductCatalog";

	/** This holds the reference for FAILED_HTTP_ERROR_CODE *. */
	public static final String FAILED_HTTP_ERROR_CODE = "Failed : HTTP error code : ";
	
	/** property to hold mActive boolean check. */
	private boolean mActive;
	
	/** property to hold TRUCollectionProductCache Component. */
	private TRUCollectionProductCache mTRUCollectionProductCache;
	
	/** property to hold PARALATGCacheContainer Component for Targater. */
	private PARALATGCacheContainer mTargaterCacheContainer;
	
	/** property to hold PARALATGCacheContainer Component for Assembler. */
	private PARALATGCacheContainer mAssemblerCacheContainer;
	
	/** property to hold TRUProductCache Component. */
	private TRUProductCache mTRUProductCache;
	
	/** property to hold TRUClassificationsTools Component. */
	private TRUClassificationsTools mTRUClassificationsTools;
	
	/** The m promotion cache. */
	private TRUPromotionCache mPromotionCache;
	
	/** The m deploy state. */
	private int mDeployState;
	
	/** DONE_ACTIVATING state	 */
	private int mDeployStateActivatingSwitch;
	/** mPromotionUpdateSercice	 */
	private TRUPromotionUpdateService mPromotionUpdateSercice;
	
	/** property to hold mAkamaiCacheInvalidationJobFlag boolean check. */
	private boolean mAkamaiCacheInvalidationJobFlag;
	
	/** property to hold mConfiguration boolean check. */
	private TRUConfiguration mConfiguration;
	
	/** The m switch incremental loading. */
	private boolean mSwitchIncrementalLoading;
	
	/** The m akamai cache listener. */
	private TRUAkamaiCacheListener mAkamaiCacheListener;
	
	/** The m product catalog output config. */
	private EndecaIndexingOutputConfig mProductCatalogOutputConfig;
	
	public EndecaIndexingOutputConfig getProductCatalogOutputConfig() {
		return mProductCatalogOutputConfig;
	}

	public void setProductCatalogOutputConfig(
			EndecaIndexingOutputConfig mProductCatalogOutputConfig) {
		this.mProductCatalogOutputConfig = mProductCatalogOutputConfig;
	}
	
	/**
	 * Gets the TRU classifications tools.
	 *
	 * @return the mTRUClassificationsTools
	 */
	public TRUClassificationsTools getTRUClassificationsTools() {
		return mTRUClassificationsTools;
	}

	/**
	 * Sets the TRU classifications tools.
	 *
	 * @param pTRUClassificationsTools the mTRUClassificationsTools to set
	 */

	public void setTRUClassificationsTools(
			TRUClassificationsTools pTRUClassificationsTools) {
		this.mTRUClassificationsTools = pTRUClassificationsTools;
	}


	/**
	 * Gets the TRU collection product cache.
	 *
	 * @return the tRUCollectionProductCache
	 */
	public TRUCollectionProductCache getTRUCollectionProductCache() {
		return mTRUCollectionProductCache;
	}


	/**
	 * Sets the TRU collection product cache.
	 *
	 * @param pTRUCollectionProductCache the tRUCollectionProductCache to set
	 */
	public void setTRUCollectionProductCache(
			TRUCollectionProductCache pTRUCollectionProductCache) {
		mTRUCollectionProductCache = pTRUCollectionProductCache;
	}


	/**
	 * Gets the TRU product cache.
	 *
	 * @return the tRUProductCache
	 */
	public TRUProductCache getTRUProductCache() {
		return mTRUProductCache;
	}


	/**
	 * Sets the TRU product cache.
	 *
	 * @param pTRUProductCache the tRUProductCache to set
	 */
	public void setTRUProductCache(TRUProductCache pTRUProductCache) {
		mTRUProductCache = pTRUProductCache;
	}
	
  /** 
	 * Property to hold repositories exclude while firing message. 
	 * 
	 */
	private List<Object> mExcludeRepositories;
	/** 
	 * Property to hold StaticContentMessageSource while firing message. 
	 * 
	 */

	/** Property to hold mSiteRepositoryPath */
	private String mSiteRepositoryPath;
	
	/**  Property to hold mSiteTools. */
	private TRUSiteTools mSiteTools;
	
	/** Holds pormo item type list	 */
	private List<String> mPromtionItemTypeList;
	
	
	/**
	 * Gets the site repository path.
	 *
	 * @return the siteRepositoryPath
	 */
	public String getSiteRepositoryPath() {
		return mSiteRepositoryPath;
	}

	/**
	 * Sets the site repository path.
	 *
	 * @param pSiteRepositoryPath the siteRepositoryPath to set
	 */
	public void setSiteRepositoryPath(String pSiteRepositoryPath) {
		mSiteRepositoryPath = pSiteRepositoryPath;
	}

	/**
	 * Gets the site tools.
	 *
	 * @return the siteTools
	 */
	public TRUSiteTools getSiteTools() {
		return mSiteTools;
	}

	/**
	 * Sets the site tools.
	 *
	 * @param pSiteTools the siteTools to set
	 */
	public void setSiteTools(TRUSiteTools pSiteTools) {
		mSiteTools = pSiteTools;
	}

	
	/**
	 * Gets the assembler cache container.
	 *
	 * @return the assembler cache container.
	 */
	public PARALATGCacheContainer getAssemblerCacheContainer() {
		return mAssemblerCacheContainer;
	}

	/**
	 * Sets the assembler cache container.
	 *
	 * @param pAssemblerCacheContainer the new assembler cache container.
	 */
	public void setAssemblerCacheContainer(
			PARALATGCacheContainer pAssemblerCacheContainer) {
		mAssemblerCacheContainer = pAssemblerCacheContainer;
	}

	
	
	/**
	 * This method has been overridden to invalidate the custom cache component
	 * TRUCollectionProductCache and TRUProductCache cache when any deployments have been done through
	 * BCC.
	 * 
	 * @param pDepEvent the Deployment Event
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void deploymentEvent(DeploymentEvent pDepEvent) {
		
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUDeploymnetEventListener.deploymentEvent method.. pDepEvent:{0}", pDepEvent);
		}
		String projectName = null;
		String[] projects = null;
		boolean isChangeInAkamaiFlag=false;
		if (pDepEvent == null || !isActive()) {
			if (isLoggingDebug()) {
				vlogDebug(" DeploymentEvent is :{0} , hence returning...", pDepEvent);
			}
			return;
		}
		if (isLoggingDebug()) {
			vlogDebug("NewState:{0}, isSwitchFirstApply:{1}  DeploymentProjectIDs:{2}", pDepEvent.
					getNewState(), pDepEvent.isSwitchFirstApply(), pDepEvent.getDeploymentProjectIDs());
		}
		if(getDeployState() == pDepEvent.getNewState() || getDeployStateActivatingSwitch() == pDepEvent.getNewState()){
			Map<String,Set<String>> affectedItemTypes = pDepEvent.getAffectedItemTypes();
			if(isLoggingDebug()){
				vlogDebug("Deployment event new state : {0}", pDepEvent.getNewState());
				vlogDebug("affectedItemTypes : {0}", affectedItemTypes);
			}
			if(affectedItemTypes != null && !affectedItemTypes.isEmpty()){
				String itemTypeAsKey = null;
				Iterator<String> iterator =  affectedItemTypes.keySet().iterator();
				while(iterator.hasNext()){
					itemTypeAsKey = iterator.next();
					if(!StringUtils.isBlank(itemTypeAsKey)){
						if(isLoggingInfo()) {
							logInfo("Repository::"+itemTypeAsKey);
						}
						if(itemTypeAsKey.equals(TRUConstants.ATG_COMMERCE_CATALOG_PRODUCT_CATALOG) && !getExcludeRepositories().contains(itemTypeAsKey)){
							if (getConfiguration().isValidateAgainstProjectId()) {
								projects = pDepEvent.getDeploymentProjectIDs();
								projectName = getProjectNameWithItRestCall(projects[0]);
							}
							Set<String> afftectedItems = affectedItemTypes.get(itemTypeAsKey);
							if(isLoggingDebug()){
								vlogDebug("afftectedItems : {0}", afftectedItems);
							}
							for (Iterator<String> iterator2 = afftectedItems.iterator(); iterator2.hasNext();) {
								String itemName = iterator2.next();
								if(getPromtionItemTypeList() != null && !getPromtionItemTypeList().isEmpty() && StringUtils.isNotBlank(itemName) && getPromtionItemTypeList().contains(itemName)){
									if(isLoggingDebug()){
										vlogDebug("Promotion Item type list : {0}", getPromtionItemTypeList());
										vlogDebug("Current item type from deployment : {0}", itemName);
									}
									if(isAkamaiCacheInvalidationJobFlag()&&getPromtionItemTypeList() != null && !getPromtionItemTypeList().isEmpty() && StringUtils.isNotBlank(itemName) && getPromtionItemTypeList().contains(itemName)){
										setAkamaiCacheInvalidationJobFlag(false);
										isChangeInAkamaiFlag=true;
									}
									if(isSwitchIncrementalLoading()){
										if(isLoggingDebug()){
											logDebug("updated Incremental Loading to false");
										}
										getProductCatalogOutputConfig().setEnableIncrementalLoading(Boolean.FALSE);
										getPromotionUpdateSercice().updatePromotions();
										getProductCatalogOutputConfig().setEnableIncrementalLoading(Boolean.TRUE);
										if(isLoggingDebug()){
											logDebug("updated Incremental Loading to true after promoiton update service call");
										}
										
									}else{
										if(isLoggingDebug()){
											logDebug("START: Calling getPromotionUpdateSercice().updatePromotions() ");
										}
										getPromotionUpdateSercice().updatePromotions();
										if(isLoggingDebug()){
											logDebug("END: Calling getPromotionUpdateSercice().updatePromotions() ");
										}
									}
								}
							}
							boolean isRunPromotionService=false;
							List<String> projectNames=getConfiguration().getCatalogFeedProjectNameList();
							for(String cmsProjectName:projectNames){
								if (StringUtils.isNotBlank(projectName)
										&& projectName.contains(cmsProjectName)) {
									isRunPromotionService=true;
									break;
								}
							}
							if(isRunPromotionService){
								if(isLoggingDebug()){
									logDebug("Catalog feed deployment : START: Calling getPromotionUpdateSercice().updatePromotions() ");
								}
								getPromotionUpdateSercice().updatePromotions();
								if(isLoggingDebug()){
									logDebug("Catalog feed deployment :  END: Calling getPromotionUpdateSercice().updatePromotions() ");
								}
							}
						}
					} 
				}
			}
		}
		
		if (getDeployState() == pDepEvent.getNewState()) {
			// that all datasources have switched and the server is beginning to deploy data to the second
			// datasource.
			if(isAkamaiCacheInvalidationJobFlag()) {
				if (isLoggingDebug()) {
					logDebug("START: Calling getAkamaiCacheInvalidationJob().cacheInvalidation() once deployment is completed");
				}
				getAkamaiCacheListener().invokeAkamaiCacheFlush();
				if (isLoggingDebug()) {
					logDebug("END: Calling getAkamaiCacheInvalidationJob().cacheInvalidation() once deployment is completed");
				}
			}
			if(isLoggingDebug()){
				logDebug("starts of getAffectedItemTypes");
			}
			Map<String,Set<String>> affectedRepositoriesFromThisDeployment = pDepEvent.getAffectedItemTypes();
			
			if(affectedRepositoriesFromThisDeployment != null && !affectedRepositoriesFromThisDeployment.isEmpty()){
				String repositoryPathAsKey = null;
				Iterator<String> iterator =  affectedRepositoriesFromThisDeployment.keySet().iterator();
				while(iterator.hasNext()){
					repositoryPathAsKey = iterator.next();
				   if(!StringUtils.isBlank(repositoryPathAsKey)){
					   if(isLoggingInfo()) {
					     logInfo("Repository::"+repositoryPathAsKey);
					   }
					  if(repositoryPathAsKey.equalsIgnoreCase(getSiteRepositoryPath())){
						 RepositoryItem siteItem = SiteContextManager.getCurrentSite();
						 if(siteItem == null) {
							try {
								siteItem = getSiteTools().getSiteManager().getSite(getSiteTools().getIntegrationPropertiesConfig().getDefaultTRUSiteId());
							} catch (RepositoryException pExce) {
								vlogError("Exception while getting site from site repository", pExce);
							}
						 }
					     if(isLoggingInfo()) {
					    	 logInfo("Repository::"+repositoryPathAsKey);					    	 
					     }	
					     getSiteTools().updateIntegrations(siteItem);
					  }
				   }				   
				}
			}
		}
		if(isChangeInAkamaiFlag){
			setAkamaiCacheInvalidationJobFlag(true);
		}
		if (isLoggingDebug()) {
			logDebug("TRUDeploymentEventListener (deploymentEvent) : END");
		}
	}

	/**
	 * This method is used to call projectNameFromID method which is available in March module through rest URL by passing.
	 * project id which need to get project Name.
	 * 
	 * @param pProjectId
	 *            the project id
	 */
	private String getProjectNameWithItRestCall(String pProjectId) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUDeploymentEventListener.getProjectNameWithItRestCall() method");
		}
		String projectName = null; 
		try {
			if (getConfiguration() != null && !StringUtils.isEmpty(getConfiguration().getProjectNameWithIdRestServiceURL()) && !StringUtils.isEmpty(pProjectId)) {
				if (isLoggingDebug()) {
					logDebug("Rest Configuration URL" + getConfiguration().getProjectNameWithIdRestServiceURL());
				}
				final URL url = new URL(getConfiguration().getProjectNameWithIdRestServiceURL() + pProjectId.toString());
				if (isLoggingDebug()) {
					vlogDebug("pProjectId: {0} url : {1}", pProjectId,url);
				}
				final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod(TRUConstants.POST);
				conn.setRequestProperty(TRUConstants.CONTENT_TYPE, TRUConstants.APPLICATION_JSON);
				final OutputStream os = conn.getOutputStream();
				os.flush();
				conn.connect();
				BufferedReader responseBuffer = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				String output;
				while ((output = responseBuffer.readLine()) != null) {
					projectName = output;
				}
				if (StringUtils.isNotBlank(projectName)) {
					JSONObject jObject  = new JSONObject(projectName);
					projectName = (String) jObject.get(TRUConstants.PROJECTNAME);
					if (isLoggingDebug()) {
						vlogDebug("pProjectId: {0}", projectName);
					}
				}
				if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
					throw new ProtocolException(FAILED_HTTP_ERROR_CODE + conn.getResponseCode());
				}
				conn.disconnect();
			}
		} catch (MalformedURLException e) {
			if (isLoggingError()) {
				vlogError("MalformedURLException: While processing MalformedURLException with exception : {0} ", e);
			}
		} catch (IOException e) {
			if (isLoggingError()) {
				vlogError("IOException: While processing IOException with exception : {0} ", e);
			}
		}
		catch (JSONException e) {
			if (isLoggingError()) {
				vlogError("JSONException: While processing JSONException with exception : {0} ", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUDeploymentEventListener.getProjectNameWithItRestCall() method");
		}
		return projectName;
			
	}

	/**
	 * Gets the active.
	 *
	 * @return the active
	 */
	public boolean isActive() {
		return mActive;
	}

	/**
	 * Sets the active.
	 *
	 * @param pActive the active to set
	 */
	public void setActive(boolean pActive) {
		mActive = pActive;
	}
	
	/**
	 * Gets the AkamaiCacheInvalidationJobFlag.
	 *
	 * @return the AkamaiCacheInvalidationJobFlag
	 */
	public boolean isAkamaiCacheInvalidationJobFlag() {
		return mAkamaiCacheInvalidationJobFlag;
	}


	/**
	 * Sets the AkamaiCacheInvalidationJobFlag.
	 *
	 * @param pAkamaiCacheInvalidationJobFlag the AkamaiCacheInvalidationJobFlag to set
	 */
	public void setAkamaiCacheInvalidationJobFlag(
			boolean pAkamaiCacheInvalidationJobFlag) {
		this.mAkamaiCacheInvalidationJobFlag = pAkamaiCacheInvalidationJobFlag;
	}
	
	/**
	 * Returns list of mapped repositories.
	 * 
	 * @return - mExcludeRepositories
	 */
	public List<Object> getExcludeRepositories() {
		return mExcludeRepositories;
	}
	
	/**
	 * Sets the repositories.
	 * 
	 * @param pExcludeRepositories - List<Object> 
	 */
	public void setExcludeRepositories(List<Object> pExcludeRepositories) {
		this.mExcludeRepositories = pExcludeRepositories;
	}

	/**
	 * Gets the targater cache container.
	 *
	 * @return the targater cache container
	 */
	public PARALATGCacheContainer getTargaterCacheContainer() {
		return mTargaterCacheContainer;
	}

	/**
	 * Sets the targater cache container.
	 *
	 * @param pTargaterCacheContainer the new targater cache container
	 */
	public void setTargaterCacheContainer(PARALATGCacheContainer pTargaterCacheContainer) {
		mTargaterCacheContainer = pTargaterCacheContainer;
	}
	/**
	 * Gets the promotion cache.
	 *
	 * @return the promotion cache
	 */
	public TRUPromotionCache getPromotionCache() {
		return mPromotionCache;
	}

	/**
	 * Sets the promotion cache.
	 *
	 * @param pPromotionCache the new promotion cache
	 */
	public void setPromotionCache(TRUPromotionCache pPromotionCache) {
		this.mPromotionCache = pPromotionCache;
	}

	/**
	 * Gets the deploy state.
	 *
	 * @return the deploy state
	 */
	public int getDeployState() {
		return mDeployState;
	}

	/**
	 * Sets the deploy state.
	 *
	 * @param pDeployState the new deploy state
	 */
	public void setDeployState(int pDeployState) {
		this.mDeployState = pDeployState;
	}

	/**
	 * This method is to get deployStateActivatingSwitch
	 *
	 * @return the deployStateActivatingSwitch
	 */
	public int getDeployStateActivatingSwitch() {
		return mDeployStateActivatingSwitch;
	}

	/**
	 * This method sets deployStateActivatingSwitch with pDeployStateActivatingSwitch
	 *
	 * @param pDeployStateActivatingSwitch the deployStateActivatingSwitch to set
	 */
	public void setDeployStateActivatingSwitch(int pDeployStateActivatingSwitch) {
		mDeployStateActivatingSwitch = pDeployStateActivatingSwitch;
	}

	/**
	 * This method is to get promtionItemTypeList
	 *
	 * @return the promtionItemTypeList
	 */
	public List<String> getPromtionItemTypeList() {
		return mPromtionItemTypeList;
	}

	/**
	 * This method sets promtionItemTypeList with pPromtionItemTypeList
	 *
	 * @param pPromtionItemTypeList the promtionItemTypeList to set
	 */
	public void setPromtionItemTypeList(List<String> pPromtionItemTypeList) {
		mPromtionItemTypeList = pPromtionItemTypeList;
	}

	/**
	 * This method is to get promotionUpdateSercice
	 *
	 * @return the promotionUpdateSercice
	 */
	public TRUPromotionUpdateService getPromotionUpdateSercice() {
		return mPromotionUpdateSercice;
	}

	/**
	 * This method sets promotionUpdateSercice with pPromotionUpdateSercice
	 *
	 * @param pPromotionUpdateSercice the promotionUpdateSercice to set
	 */
	public void setPromotionUpdateSercice(
			TRUPromotionUpdateService pPromotionUpdateSercice) {
		mPromotionUpdateSercice = pPromotionUpdateSercice;
	}	
	
	/**
	 * This method is to get configuration
	 *
	 * @return the configuration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * This method sets configuration with pConfiguration
	 *
	 * @param pConfiguration the configuration to set
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		this.mConfiguration = pConfiguration;
	}

	/**
	 * Checks if is switch incremental loading.
	 *
	 * @return true, if is switch incremental loading
	 */
	public boolean isSwitchIncrementalLoading() {
		return mSwitchIncrementalLoading;
	}

	/**
	 * Sets the switch incremental loading.
	 *
	 * @param pSwitchIncrementalLoading the new switch incremental loading
	 */
	public void setSwitchIncrementalLoading(boolean pSwitchIncrementalLoading) {
		this.mSwitchIncrementalLoading = pSwitchIncrementalLoading;
	}

	/**
	 * Gets the akamai cache listener.
	 *
	 * @return the akamai cache listener
	 */
	public TRUAkamaiCacheListener getAkamaiCacheListener() {
		return mAkamaiCacheListener;
	}

	/**
	 * Sets the akamai cache listener.
	 *
	 * @param pAkamaiCacheListener the new akamai cache listener
	 */
	public void setAkamaiCacheListener(TRUAkamaiCacheListener pAkamaiCacheListener) {
		this.mAkamaiCacheListener = pAkamaiCacheListener;
	}

}