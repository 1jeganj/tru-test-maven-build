package com.tru.deployment.listener;

import atg.nucleus.GenericService;

/**
 *  This Class  extends GenericService and performs the cache flushing during deployment.
 *  
 * @author   : PA
 * @version  : 1.0
 * 
 * */
public class TRUAkamaiCacheListener extends GenericService
{
	/**
	 * Property to hold TRUDataListenerQueue Component
	 */
	private TRUAkamaiCacheDataListenerQueue mDataListenerQueue;


	
	/**
	 * Invoke cache flush.
	 */
	public void invokeAkamaiCacheFlush(){
		if(isLoggingDebug()){
			logDebug("Inside TRUCustomCacheListener.sendMessage method ");
		}
		Object obj =new Object();
		sendMessage(obj);
		if(isLoggingDebug()){
			logDebug("Exiting TRUCustomCacheListener.sendMessage method ");
		}	
	}
	
	/**
	 * This method sends the Object message to the IamDataListenerQueue for Logging
	 * 
	 * @param pObject the Object
	 */
	public void sendMessage(Object pObject){
		if(isLoggingDebug()){
			logDebug("Inside TRUCustomCacheListener.sendMessage method ");
		}
		getDataListenerQueue().addDataItem(pObject);
		if(isLoggingDebug()){
			logDebug("Exiting TRUCustomCacheListener.sendMessage method ");
		}
	}



	/**
	 * Gets the data listener queue.
	 *
	 * @return the data listener queue
	 */
	public TRUAkamaiCacheDataListenerQueue getDataListenerQueue() {
		return mDataListenerQueue;
	}



	/**
	 * Sets the data listener queue.
	 *
	 * @param pDataListenerQueue the new data listener queue
	 */
	public void setDataListenerQueue(TRUAkamaiCacheDataListenerQueue pDataListenerQueue) {
		this.mDataListenerQueue = pDataListenerQueue;
	}
}

