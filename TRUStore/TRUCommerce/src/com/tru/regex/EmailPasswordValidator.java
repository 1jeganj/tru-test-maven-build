package com.tru.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**  EmailPasswordValidator class for validating email and password pattern.
 * @author Professional Access
 * @version 1.0
 */
public class EmailPasswordValidator {
	/**
	 *  property to hold Pattern object.
	 */
	private Pattern mPattern;
	/**
	 *  property to hold Matcher object.
	 */
	private Matcher mMatcher;
	/**
	 *  property to hold configurable emailPattern.
	 */
	private String mEmailPattern;
	/**
	 *  property to hold configurable passwordPattern.
	 */
	private String mPasswordPattern;
	/**
	 *  property to hold configurable zipCodePattern.
	 */
	private String mZipCodePattern;
	
	/**
	 * This method is for validating email pattern.
	 * @param pEmail String
	 * @return boolean
	 */
	public boolean validateEmail (String pEmail){
		mPattern = Pattern.compile(getEmailPattern());
		mMatcher = mPattern.matcher(pEmail);
		return mMatcher.matches();
	}
	
	/**
	 * This method is for validating password pattern.
	 * @param pPassword String
	 * @return boolean
	 */
	public boolean validatePassword (String pPassword){
		mPattern = Pattern.compile(getPasswordPattern());
		mMatcher = mPattern.matcher(pPassword);
		return mMatcher.matches();
	}
	
	/**
	 * This method is for validating US ZipCode codes.
	 * @param pZipCode String
	 * @return boolean
	 */
	public boolean validateZipCode (String pZipCode){
		mPattern = Pattern.compile(getZipCodePattern());
		mMatcher = mPattern.matcher(pZipCode);
		return mMatcher.matches();
	}
	
	/**
	 * @return the emailPattern
	 */
	public String getEmailPattern() {
		return mEmailPattern;
	}
	
	/**
	 * @param pEmailPattern the mEmailPattern to set
	 */
	public void setEmailPattern(String pEmailPattern) {
		this.mEmailPattern = pEmailPattern;
	}
	
	/**
	 * @return the passwordPattern
	 */
	public String getPasswordPattern() {
		return mPasswordPattern;
	}
	
	/**
	 * @param pPasswordPattern the mPasswordPattern to set
	 */
	public void setPasswordPattern(String pPasswordPattern) {
		this.mPasswordPattern = pPasswordPattern;
	}

	/**
	 * @return the zipCodePattern
	 */
	public String getZipCodePattern() {
		return mZipCodePattern;
	}

	/**
	 * @param pZipCodePattern the mZipCodePattern to set
	 */
	public void setZipCodePattern(String pZipCodePattern) {
		this.mZipCodePattern = pZipCodePattern;
	}
	
}
