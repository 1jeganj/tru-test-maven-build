package com.tru.contactus;

import java.util.HashMap;
import java.util.Map;

import atg.repository.RepositoryItem;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.common.TRUConstants;
import com.tru.integrations.exception.TRUIntegrationException;




/**
 * This class is overriding OOTB SingletonSchedulableService abstract class.
 * This class is used to get the all the failed OMS request, re-deliver them and delete the entry
 * from OMS Error Repository in case of get success response.
 * @author Professional Access
 * @version 1.0
 */
/**
 * @author Satish
 *
 */
public class TRUContactUsScheduler extends SingletonSchedulableService{

	/**
	 * Holds reference for mCustomerName
	 */
	private String mCustomerName;

	/**
	 * Holds reference for mTelephone
	 */
	private String mTelephone;

	/**
	 * Holds reference for mSubject
	 */
	private String mSubject;

	/**
	 * Holds reference for mEmail
	 */
	private String mEmail;
	/**
	 * Holds reference for mOrder
	 */
	private String mOrder;
	/**
	 * Holds reference for mQuestions
	 */
	private String mQuestions;


	/**
	 * Holds reference for mContactUsEmailSender
	 */
	private TRUContactUsEmailSender mContactUsEmailSender;

	/**
	 * @return the mContactUsEmailSender
	 */
	public TRUContactUsEmailSender getContactUsEmailSender() {
		return mContactUsEmailSender;
	}

	/**
	 * @param pContactUsEmailSender the epslonHelper to set
	 */
	public void setContactUsEmailSender(TRUContactUsEmailSender pContactUsEmailSender) {
		mContactUsEmailSender = pContactUsEmailSender;
	}

	/**
	 * Holds reference for mEnable.
	 */
	private boolean mEnable;


	/**
	 * Overriding OOTB method to re-deliver the failed Epsilon request.
	 *
	 * @param pParamScheduler - Scheduler Object
	 * @param pParamScheduledJob - ScheduledJob Object
	 *
	 */
	@Override
	public void doScheduledTask(Scheduler pParamScheduler,ScheduledJob pParamScheduledJob) {
		if(isLoggingDebug()){
			logDebug("TRUEpsilonRedeliveryScheduler :: doScheduledTask() method :: STARTS ");
		}
		if(isEnable()){
			try {
				repostContactUS();
			} catch (TRUIntegrationException e) {
				if(isLoggingError()) {
					logError(e.getMessage(), e);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("TRUEpsilonRedeliveryScheduler :: doScheduledTask() method :: ENDS ");
		}
	}

	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

/**
 * @throws TRUIntegrationException TRUIntegrationException
 */
public void repostContactUS() throws TRUIntegrationException{
	RepositoryItem[] failedContactMessageItems = getContactUsEmailSender().getContactMessages();
	if(failedContactMessageItems != null){
		for(RepositoryItem messageItem :failedContactMessageItems ){
			Map<String,Object> mapObject=new HashMap<String,Object>();
			mapObject.put(TRUConstants.CUSTOMER_NAME, (String) messageItem.getPropertyValue(getCustomerName()));
			mapObject.put(TRUConstants.TELEPHONE, (String) messageItem.getPropertyValue(getTelephone()));
			mapObject.put(TRUConstants.SUBJECT, (String) messageItem.getPropertyValue(getSubject()));
			mapObject.put(TRUConstants.EMAIL_ADDR,(String) messageItem.getPropertyValue(getEmail()));
			mapObject.put(TRUConstants.ORDER_NO, (String) messageItem.getPropertyValue(getOrder()));
			mapObject.put(TRUConstants.QUESTIONS, (String) messageItem.getPropertyValue(getQuestions()));
			boolean isPosted=getContactUsEmailSender().sendContactUsSuccessEmail(mapObject, true);
			String itemId = (String)messageItem.getRepositoryId();
			if(isPosted){
				getContactUsEmailSender().removeContactItem(itemId);
			}

			}
		}
	}

/**
 * @return the mCustomerName
 */
public String getCustomerName() {
	return mCustomerName;
}

/**
 * @param pCustomerName the mCustomerName to set
 */
public void setCustomerName(String pCustomerName) {
	this.mCustomerName = pCustomerName;
}

/**
 * @return the mTelephone
 */
public String getTelephone() {
	return mTelephone;
}

/**
 * @param pTelephone the mTelephone to set
 */
public void setTelephone(String pTelephone) {
	this.mTelephone = pTelephone;
}

/**
 * @return the mSubject
 */
public String getSubject() {
	return mSubject;
}

/**
 * @param pSubject the mSubject to set
 */
public void setSubject(String pSubject) {
	this.mSubject = pSubject;
}

/**
 * @return the mEmail
 */
public String getEmail() {
	return mEmail;
}

/**
 * @param pEmail the mEmail to set
 */
public void setEmail(String pEmail) {
	this.mEmail = pEmail;
}

/**
 * @return the mOrder
 */
public String getOrder() {
	return mOrder;
}

/**
 * @param pOrder the mOrder to set
 */
public void setOrder(String pOrder) {
	this.mOrder = pOrder;
}

/**
 * @return the mQuestions
 */
public String getQuestions() {
	return mQuestions;
}

/**
 * @param pQuestions the mQuestions to set
 */
public void setQuestions(String pQuestions) {
	this.mQuestions = pQuestions;
}

}
