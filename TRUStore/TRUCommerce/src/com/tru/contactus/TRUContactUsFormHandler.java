package com.tru.contactus;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.droplet.GenericFormHandler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.cml.ValidationExceptions;
import com.tru.errorhandler.fhl.IErrorHandler;
import com.tru.integrations.exception.TRUIntegrationException;
import com.tru.regex.EmailPasswordValidator;
/**
 * TRUContactUsFormHandler class for sending Contact Us Email.
 *
 * @author Professional Access
 * @version 1.0
 *
 */

public class TRUContactUsFormHandler extends GenericFormHandler{


	/**
	 * property to hold ValidationException.
	 */
	private ValidationExceptions mVE = new ValidationExceptions();

	/**
	 * property to hold the mErrorHandler.
	 */
	private IErrorHandler mErrorHandler;
	/**
	 * Holds the ContactUsEmailSender
	 */

	private TRUContactUsEmailSender mContactUsEmailSender;

	/**
	 * Holds the customerName
	 */
	private String mCustomerName;
	/**
	 * Holds the Telephone
	 */
	private String mTelephone;
	/**
	 * Holds the Subject
	 */
	private String mSubject;
	/**
	 * Holds the Email
	 */
	private String mEmail;
	/**
	 * Holds the OrderNo
	 */
	private String mOrderNo;
	/**
	 * Holds the QuestionsAndComments
	 */
	private String mQuestionsAndComments;

	/** property to hold mContactUsSuccessURL. */
	private String mContactUsSuccessURL;

	/** property to hold mContactUsErrorURL. */
	private String mContactUsErrorURL;


	private  EmailPasswordValidator mEmailPasswordValidator;
	/**
	 * Gets the email password validator.
	 *
	 * @return the emailPasswordValidator
	 */
	public EmailPasswordValidator getEmailPasswordValidator() {
		return mEmailPasswordValidator;
	}

	/**
	 * Sets the email password validator.
	 *
	 * @param pEmailPasswordValidator
	 *            the emailPasswordValidator to set
	 */
	public void setEmailPasswordValidator(EmailPasswordValidator pEmailPasswordValidator) {
		this.mEmailPasswordValidator = pEmailPasswordValidator;
	}


	/**
	 * Gets the error handler.
	 *
	 * @return the errorHandler
	 */
	public IErrorHandler getErrorHandler() {
		return mErrorHandler;
	}

	/**
	 * Sets the error handler.
	 *
	 * @param pErrorHandler
	 *            the errorHandler to set
	 */
	public void setErrorHandler(IErrorHandler pErrorHandler) {
		mErrorHandler = pErrorHandler;
	}

	/**
	 * @return the contactUsSuccessURL
	 */
	public String getContactUsSuccessURL() {
		return mContactUsSuccessURL;
	}
	/**
	 * @param pContactUsSuccessURL the contactUsSuccessURL to set
	 */
	public void setContactUsSuccessURL(String pContactUsSuccessURL) {
		mContactUsSuccessURL = pContactUsSuccessURL;
	}
	/**
	 * @return the contactUsErrorURL
	 */
	public String getContactUsErrorURL() {
		return mContactUsErrorURL;
	}
	/**
	 * @param pContactUsErrorURL the contactUsErrorURL to set
	 */
	public void setContactUsErrorURL(String pContactUsErrorURL) {
		mContactUsErrorURL = pContactUsErrorURL;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return mCustomerName;
	}
	/**
	 * @param pCustomerName the customerName to set
	 */
	public void setCustomerName(String pCustomerName) {
		mCustomerName = pCustomerName;
	}
	/**
	 * @return the mTelephone
	 */
	public String getTelephone() {
		return mTelephone;
	}
	/**
	 * @param pTelephone the mTelephone to set
	 */
	public void setTelephone(String pTelephone) {
		mTelephone = pTelephone;
	}
	/**
	 * @return the mSubject
	 */
	public String getSubject() {
		return mSubject;
	}
	/**
	 * @param pSubject the Subject to set
	 */
	public void setSubject(String pSubject) {
		mSubject = pSubject;
	}
	/**
	 * @return the mEmail
	 */
	public String getEmail() {
		return mEmail;
	}
	/**
	 * @param pEmail the mEmail to set
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}
	/**
	 * @return the mOrderNo
	 */
	public String getOrderNo() {
		return mOrderNo;
	}
	/**
	 * @param pOrderNo the mOrderNo to set
	 */
	public void setOrderNo(String pOrderNo) {
		mOrderNo = pOrderNo;
	}
	/**
	 * @return the QuestionsAndComments
	 */
	public String getQuestionsAndComments() {
		return mQuestionsAndComments;
	}
	/**
	 * @param pQuestionsAndComments the mQuestionsAndComments to set
	 */
	public void setQuestionsAndComments(String pQuestionsAndComments) {
		mQuestionsAndComments = pQuestionsAndComments;
	}

	/**
	 * @param pRequest pRequest
	 * @param pResponse pResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	protected void preContactUs(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
	IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUContactUsFormHandler.preContactUs()");
		}
		String customerName = getCustomerName();
		String telephone= getTelephone();
		String subject= getSubject();
		String email= getEmail();
		String questions= getQuestionsAndComments();
		if(StringUtils.isBlank(customerName) || StringUtils.isBlank(telephone) ||  StringUtils.isBlank(subject) ||
				StringUtils.isBlank(questions)|| StringUtils.isBlank(email)){
			mVE.addValidationError(TRUErrorKeys.TRU_ERROR_MANDATORY_FIELD_MISSING_MANDATORY_INFO, null);
			getErrorHandler().processException(mVE, this);
		}
		else if (email.length() < TRUConstants.SIX) {
			mVE.addValidationError(TRUErrorKeys.TRU_ERROR_INVALID_EMAIL_INFO, null);
			getErrorHandler().processException(mVE, this);
		} else if (!getEmailPasswordValidator().validateEmail(email)){
			mVE.addValidationError(TRUErrorKeys.TRU_ERROR_INVALID_EMAIL_INFO, null);
			getErrorHandler().processException(mVE, this);
		}
		if (isLoggingDebug()) {
		logDebug("End: TRUContactUsFormHandler.preContactUs()");
		}
	}
	/**
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean -true/false
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleContactUs(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,IOException
	{
		if(isLoggingDebug()){
			logDebug("TRUContactUsFormHandler :: handleContactUs() method :: STARTS ");
		}

		Map<String,Object> mapObject=new HashMap<String,Object>();

		try {

			preContactUs(pRequest, pResponse);
			if (!getFormError())
			{
				mapObject.put(TRUConstants.CUSTOMER_NAME, getCustomerName());
				mapObject.put(TRUConstants.TELEPHONE, getTelephone());
				mapObject.put(TRUConstants.SUBJECT, getSubject());
				mapObject.put(TRUConstants.EMAIL_ADDR, getEmail());
				mapObject.put(TRUConstants.ORDER_NO, getOrderNo());
				mapObject.put(TRUConstants.QUESTIONS, getQuestionsAndComments());
				getContactUsEmailSender().sendContactUsSuccessEmail(mapObject, false);
			}
		}catch (TRUIntegrationException e) {
			if(isLoggingError()) {
				logError(e.getMessage(), e);
			}
		}
		finally {
			if (isLoggingDebug()) {
				logDebug("Entered into finally block");
			}

		}

		if(isLoggingDebug()){
			logDebug("TRUContactUsFormHandler :: handleContactUs() method :: ENDS ");
		}
		return checkFormRedirect(getContactUsSuccessURL(), getContactUsErrorURL(), pRequest, pResponse);
	}

	/**
	 * @return ContactUsEmailSender
	 */
	public TRUContactUsEmailSender getContactUsEmailSender() {
		return mContactUsEmailSender;
	}
	/**
	 * @param pContactUsEmailSender ContactUsEmailSender to set
	 */
	public void setContactUsEmailSender(TRUContactUsEmailSender pContactUsEmailSender) {
		mContactUsEmailSender = pContactUsEmailSender;
	}


}
