package com.tru.contactus;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.transaction.TransactionManager;

import atg.adapter.gsa.GSARepository;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.userprofiling.email.TemplateEmailException;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;

import com.tru.common.TRUConstants;
import com.tru.integrations.common.TRUEpslonConfiguration;
import com.tru.integrations.exception.TRUIntegrationException;

/**
 * This class is used to send an Email on Contact Us Status to Users listed.
 * @version 1.0
 * @author Professional Access
 */
public class TRUContactUsEmailSender extends GenericService {

	/**
	 * Holds the property value of epslonConfiguration
	 */
	private TRUEpslonConfiguration mEpslonConfiguration;

	/**
	 * Holds the property value of ContactUs
	 */
	private TRUContactUsScheduler mContactUs;

	/**
	 * Holds the contactItemDescriptor
	 */
	private String mContactItemDescriptor;

	/**
	 * Holds the TransactionManager
	 */
	private TransactionManager mTransactionManager ;

	/**
	 * Holds reference for MessageContentRepository.
	 */
	private GSARepository mAuditRepository;


	/** Property to hold mTemplateEmailSender. */
	private TemplateEmailSender mTemplateEmailSender;

	/** Property to hold mContactEmailConfiguration. */
	private TRUContactUsEmailConfiguration mContactEmailConfiguration;

	/**
	 * This method used to send success email on Contact Us
	 * @param pMapObject configuration map
	 * @param pScheduler from Scheduler
	 * @throws TRUIntegrationException the TRUIntegrationException
	 * @return boolean postingStatus
	 */
	public boolean sendContactUsSuccessEmail(Map<String, Object> pMapObject,boolean pScheduler) throws TRUIntegrationException {
		if(isLoggingDebug()){
			logDebug("ContactUsEmailSender :: sendContactUsSuccessEmail() :: START");
		}
		TemplateEmailInfoImpl emailInfo = mContactEmailConfiguration.getContactSuccessEmailInfo();

		clearEmailInfo(emailInfo);

		if (emailInfo.getTemplateParameters() != null) {
		emailInfo.getTemplateParameters().putAll(pMapObject);
		} else {
			emailInfo.setTemplateParameters(pMapObject);
		}
		int postCount = 0;
		boolean postingStatus = TRUConstants.FALSE_FLAG;
		while((!postingStatus) && (postCount < TRUConstants.MAX_LIMIT)){
		try {
			emailInfo.setMessageSubject((String) pMapObject.get(TRUConstants.SUBJECT));
			emailInfo.setFillFromTemplate(!TRUConstants.FALSE_FLAG);
            getTemplateEmailSender().sendEmailMessage(emailInfo, mContactEmailConfiguration.getRecipients());
            postingStatus = !TRUConstants.FALSE_FLAG;
			} catch (TemplateEmailException e) {
				postingStatus = TRUConstants.FALSE_FLAG;
				postCount=postCount+TRUConstants._1;
				if(isLoggingError()) {
					logError(e.getMessage(), e);
				}
			}
			finally {
				if (isLoggingDebug()) {
					logDebug("Entered into finally block");
				}
				String customerName = null;
				String telephone= null;
				String subject= null;
				String email= null;
				String order= null;
				String questions= null;

			if((!postingStatus) && (postCount >= TRUConstants.MAX_LIMIT) && (!pScheduler)){
				Set mapSet = (Set) pMapObject.entrySet();
                Iterator mapIterator = mapSet.iterator();
                while (mapIterator.hasNext()) {
                        Map.Entry mapEntry = (Map.Entry) mapIterator.next();
                        // getKey Method of HashMap access a key of map
                        String keyValue = (String) mapEntry.getKey();
                        //getValue method returns corresponding key's value
                        String value = (String) mapEntry.getValue();
                        if(keyValue.equalsIgnoreCase(TRUConstants.CUSTOMER_NAME)){
                        	customerName=value;
                        }
                        if(keyValue.equalsIgnoreCase(TRUConstants.TELEPHONE)){
                        	telephone=value;
                        }
                        if(keyValue.equalsIgnoreCase(TRUConstants.SUBJECT)){
                        	subject=value;
                        }
                        if(keyValue.equalsIgnoreCase(TRUConstants.EMAIL_ADDR)){
                        	email=value;
                        }
                        if(keyValue.equalsIgnoreCase(TRUConstants.ORDER_NO)){
                        	order=value;
                        }
                        if(keyValue.equalsIgnoreCase(TRUConstants.QUESTIONS)){
                        	questions=value;
                        }

                }
                try {
                	updateContactUsItem(customerName,telephone,subject,email,order,questions);
                }catch(TransactionDemarcationException e){
                	if(isLoggingError()) {
    					logError(e.getMessage(), e);
    				}
                }
			}
		  }
		}
	return postingStatus;
	}

	/**
	 * This method used to clear email information
	 * @param pEmailInfo email template information
	 */
	public void clearEmailInfo(TemplateEmailInfoImpl pEmailInfo) {
		vlogDebug("Begin:@Class: ContactUsEmailSender : @Method: clearEmailInfo()");
		pEmailInfo.setTemplateParameters(null);
		pEmailInfo.setMessageAttachments(null);
		pEmailInfo.setMessageSubject(null);
		vlogDebug("End:@Class: ContactUsEmailSender : @Method: clearEmailInfo()");
	}

	/**
	 * This method used to send failure email
	 * @throws TRUIntegrationException integration exception
	 */
	public void sendContactUsFailureEmail() throws TRUIntegrationException {
		if(isLoggingDebug()){
			logDebug("ContactUsEmailSender :: sendContactUsFailureEmail() :: START");
		}
		TemplateEmailInfoImpl emailInfo = mContactEmailConfiguration.getContactFailureEmailInfo();
		try {
			getTemplateEmailSender().sendEmailMessage(emailInfo, mContactEmailConfiguration.getRecipients());
		} catch (TemplateEmailException e) {
			if(isLoggingError()) {
				logError(e.getMessage(), e);
			}
			throw new TRUIntegrationException(e);
		}
		if(isLoggingDebug()){
			logDebug("ContactUsEmailSender :: sendContactUsFailureEmail() :: END");
		}
	}

	/**
	 * @return the templateEmailSender
	 */
	public TemplateEmailSender getTemplateEmailSender() {
		return mTemplateEmailSender;
	}

	/**
	 * @param pTemplateEmailSender the templateEmailSender to set
	 */
	public void setTemplateEmailSender(TemplateEmailSender pTemplateEmailSender) {
		mTemplateEmailSender = pTemplateEmailSender;
	}


	/**
	 * @return the contactEmailConfiguration
	 */
	public TRUContactUsEmailConfiguration getContactEmailConfiguration() {
		return mContactEmailConfiguration;
	}

	/**
	 * @param pContactEmailConfiguration the contactEmailConfiguration to set
	 */
	public void setContactEmailConfiguration(
			TRUContactUsEmailConfiguration pContactEmailConfiguration) {
		mContactEmailConfiguration = pContactEmailConfiguration;
	}

	/**
	 * @return the mTransactionManager
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}
	/**
	 * @param pTransactionManager the mTransactionManager to set
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		this.mTransactionManager = pTransactionManager;
	}
	/**
	 * @return the mAuditRepository
	 */
	public GSARepository getAuditRepository() {
		return mAuditRepository;
	}
	/**
	 * @param pAuditRepository the mAuditRepository to set
	 */
	public void setAuditRepository(GSARepository pAuditRepository) {
		this.mAuditRepository = pAuditRepository;
	}
	/**
	 * @return the mEpslonConfiguration
	 */
	public TRUEpslonConfiguration getEpslonConfiguration() {
		return mEpslonConfiguration;
	}
	/**
	 * @param pEpslonConfiguration the mEpslonConfiguration to set
	 */
	public void setEpslonConfiguration(TRUEpslonConfiguration pEpslonConfiguration) {
		this.mEpslonConfiguration = pEpslonConfiguration;
	}
	/**
	 * @return the mcontactItemDescriptor
	 */
	public String getcontactItemDescriptor() {
		return mContactItemDescriptor;
	}
	/**
	 * @param pContactItemDescriptor the mcontactItemDescriptor to set
	 */
	public void setContactItemDescriptor(String pContactItemDescriptor) {
		this.mContactItemDescriptor = pContactItemDescriptor;
	}
	/**
	 * @param pCustomerName CustomerName
	 * @param pTelephone Telephone
	 * @param pSubject Subject
	 * @param pEmail Email
	 * @param pOrderNo OrderNo
	 * @param pQuestionsAndComments QuestionsAndComments
	 * @throws TransactionDemarcationException TransactionDemarcationException
	 */
	private void updateContactUsItem(String pCustomerName, String pTelephone, String pSubject, String pEmail, String pOrderNo, String pQuestionsAndComments) throws TransactionDemarcationException {

		final TransactionManager tm = getTransactionManager();

		TransactionDemarcation td = new TransactionDemarcation();
		boolean lRollbackFlag = TRUConstants.FALSE_FLAG;
		MutableRepository auditRepository = getAuditRepository();
		try {
			td.begin(tm, TransactionDemarcation.REQUIRED);

			MutableRepositoryItem transDetailsItem = auditRepository.createItem(getcontactItemDescriptor());
			transDetailsItem.setPropertyValue(getContactUs().getCustomerName(),pCustomerName);
			transDetailsItem.setPropertyValue(getContactUs().getTelephone(),pTelephone);
			transDetailsItem.setPropertyValue(getContactUs().getSubject(),pSubject);
			transDetailsItem.setPropertyValue(getContactUs().getEmail(),pEmail);
			transDetailsItem.setPropertyValue(getContactUs().getOrder(),pOrderNo);
			transDetailsItem.setPropertyValue(getContactUs().getQuestions(),pQuestionsAndComments);
			auditRepository.addItem(transDetailsItem);
		}
		catch(RepositoryException e1){
			lRollbackFlag=(!TRUConstants.FALSE_FLAG);
			if(isLoggingError()) {
				logError(e1.getMessage(), e1);
			}
		}finally{
			if (tm != null) {
				try {
					td.end(lRollbackFlag);
				} catch (TransactionDemarcationException tD) {
					vlogError("TransactionDemarcationException occured while adding contact us with exception ", tD);

				}
			}
		}


	}

	/**
	 * @return RepositoryItems
	 */
	public RepositoryItem[] getContactMessages() {

		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUContactUsEmailSender  method: getContactMessages]");

		}

		MutableRepository auditRepository = getAuditRepository();
		RepositoryItem[] auditMessageItems= null;
		try {
			RepositoryView autMessageView = auditRepository.getView(mContactItemDescriptor);
			QueryBuilder queryBuilder = autMessageView.getQueryBuilder();
			Query transactionTimeQuery = queryBuilder.createUnconstrainedQuery();
	        auditMessageItems=autMessageView.executeQuery(transactionTimeQuery);
		} catch (RepositoryException exc) {
			if(isLoggingError()){
				vlogError("RepositoryException : While view audit message items with exception : {0}", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUContactUsEmailSender  method: purgeOmsAuditMessages]");
		}
		return auditMessageItems;

	}

	/**
	 * @param pItemId Repository Id.
	 */
	public void removeContactItem(String pItemId) {

		try {
			MutableRepository auditRepository = getAuditRepository();
			auditRepository.removeItem(pItemId, getcontactItemDescriptor());
		} catch (RepositoryException e) {
			if(isLoggingError()) {
				logError(e.getMessage(), e);
			}
		}

	}

	/**
	 * @return the mContactUs
	 */
	public TRUContactUsScheduler getContactUs() {
		return mContactUs;
	}

	/**
	 * @param pContactUs the mContactUs to set
	 */
	public void setContactUs(TRUContactUsScheduler pContactUs) {
		mContactUs = pContactUs;
	}


}
