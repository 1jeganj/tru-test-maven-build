package com.tru.contactus;

import java.util.List;

import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailInfoImpl;

/**
 * The Class TRUContactUsEmailConfiguration.
 * @author Professional Access
 * @version 1.0
 */
public class TRUContactUsEmailConfiguration extends GenericService {

	/** Property to hold Success Template Email Info. */
	private TemplateEmailInfoImpl mContactSuccessEmailInfo;

	/** Property to hold Failure Template Email Info. */
	private TemplateEmailInfoImpl mContactFailureEmailInfo;

	/** Property to hold Recipients. */
	private List<String> mRecipients;

	/**
	 * @return the ContactSuccessEmailInfo
	 */
	public TemplateEmailInfoImpl getContactSuccessEmailInfo() {
		return mContactSuccessEmailInfo;
	}

	/**
	 * @param pContactSuccessEmailInfo the akamaiSuccessEmailInfo to set
	 */
	public void setContactSuccessEmailInfo(
			TemplateEmailInfoImpl pContactSuccessEmailInfo) {
		mContactSuccessEmailInfo = pContactSuccessEmailInfo;
	}

	/**
	 * @return the ContactFailureEmailInfo
	 */
	public TemplateEmailInfoImpl getContactFailureEmailInfo() {
		return mContactFailureEmailInfo;
	}

	/**
	 * @param pContactFailureEmailInfo the akamaiFailureEmailInfo to set
	 */
	public void setContactFailureEmailInfo(
			TemplateEmailInfoImpl pContactFailureEmailInfo) {
		mContactFailureEmailInfo = pContactFailureEmailInfo;
	}

	/**
	 * Gets the recipients.
	 *
	 * @return the recipients
	 */
	public List<String> getRecipients() {
		return mRecipients;
	}

	/**
	 * Sets the recipients.
	 *
	 * @param pRecipients
	 *            the mRecipients to set
	 */
	public void setRecipients(List<String> pRecipients) {
		mRecipients = pRecipients;
	}


}
