/*
 * @(#) TRUProductPageUtil.java 1.0 Nov 13, 2015
 * 
 * Copyright 2014 PA, Inc. All rights reserved. PA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.tru.endeca.utils;

import java.util.Set;

import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.multisite.SiteManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.common.TRUConstants;
import com.tru.common.vo.TRUSiteVO;
import com.tru.utils.TRUGetSiteTypeUtil;
import com.tru.utils.TRUSchemeDetectionUtil;



/**
 * The TRUProductPageUtil class will provide the SEO friendly
 * Product Detail page URLs when we pass the product Id.
 * 
 * @author Professional Access
 * @version 1.0
 * 
 */
public class TRUProductPageUtil extends GenericService {
	
	/**
	 * Property to mSiteDefaultCatalog.
	 */
	private String mSiteDefaultCatalog;
	/**
	 * Property to mSiteProductionURL.
	 */
	private String mSiteProductionURL;
	/**
	 * Property to mChildSKUs.
	 */
	private String mChildSKUs;
	/**
	 * Property to mSkuType.
	 */
	private String mSkuType;

	/**
	 * Property to Hold mPdpPageUrl.
	 */
	private String mPdpPageUrl;

	/**
	 * This property holds String variable mAggrERecKeyUrl.
	 */
	private String mAggrERecKeyUrl;

	/**
	 * This property holds mSiteManager.
	 */
	private SiteManager mSiteManager;
	/**
	 * This property hold reference for mCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;
	/**
	 *  The m tru get site type mTRUGetSiteTypeUtil. 
	 *  
	 */
	private TRUGetSiteTypeUtil mTRUGetSiteTypeUtil;

	/** Property Holds mSiteContextManager. */
	private SiteContextManager mSiteContextManager;
	/**
	 * This property holds CatalogProperties variable mCatalogProperties.
	 */
	private TRUCatalogProperties mCatalogProperties;
	
	/** The scheme detection util. */
	private TRUSchemeDetectionUtil mSchemeDetectionUtil;

	/**
	 * @return the siteManager
	 */
	public SiteManager getSiteManager() {
		return mSiteManager;
	}

	/**
	 * @param pSiteManager the siteManager to set
	 */
	public void setSiteManager(SiteManager pSiteManager) {
		mSiteManager = pSiteManager;
	}

	/**
	 * Returns the this property holds String variable aggrERecKeyUrl.
	 * 
	 * @return the aggrERecKeyUrl
	 */
	public String getAggrERecKeyUrl() {
		return mAggrERecKeyUrl;
	}

	/**
	 * Sets the this property holds String variable aggrERecKeyUrl.
	 * 
	 * @param pAggrERecKeyUrl
	 *            the aggrERecKeyUrl to set
	 */
	public void setAggrERecKeyUrl(String pAggrERecKeyUrl) {
		mAggrERecKeyUrl = pAggrERecKeyUrl;
	}

	/**
	 * Gets the catalogTools.
	 * 
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Sets the catalogTools.
	 * 
	 * @param pCatalogTools
	 *            the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}
	/**
	 * Returns Site Context Manager.
	 *
	 * @return the SiteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * sets Site Context Manager.
	 *
	 * @param pSiteContextManager the SiteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}
	/**
	 * @return the mCatalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}
	/**
	 * @param pCatalogProperties the mCatalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		this.mCatalogProperties = pCatalogProperties;
	}

	/**
	 * @return the pdpPageUrl
	 */
	public String getPdpPageUrl() {
		return mPdpPageUrl;
	}

	/**
	 * @param pPdpPageUrl the pdpPageUrl to set
	 */
	public void setPdpPageUrl(String pPdpPageUrl) {
		mPdpPageUrl = pPdpPageUrl;
	}

	/**
	 * @return the siteDefaultCatalog
	 */
	public String getSiteDefaultCatalog() {
		return mSiteDefaultCatalog;
	}

	/**
	 * @param pSiteDefaultCatalog the siteDefaultCatalog to set
	 */
	public void setSiteDefaultCatalog(String pSiteDefaultCatalog) {
		mSiteDefaultCatalog = pSiteDefaultCatalog;
	}

	/**
	 * @return the siteProductionURL
	 */
	public String getSiteProductionURL() {
		return mSiteProductionURL;
	}

	/**
	 * @param pSiteProductionURL the siteProductionURL to set
	 */
	public void setSiteProductionURL(String pSiteProductionURL) {
		mSiteProductionURL = pSiteProductionURL;
	}

	/**
	 * @return the childSKUs
	 */
	public String getChildSKUs() {
		return mChildSKUs;
	}

	/**
	 * @param pChildSKUs the childSKUs to set
	 */
	public void setChildSKUs(String pChildSKUs) {
		mChildSKUs = pChildSKUs;
	}

	/**
	 * @return the skuType
	 */
	public String getSkuType() {
		return mSkuType;
	}

	/**
	 * @param pSkuType the skuType to set
	 */
	public void setSkuType(String pSkuType) {
		mSkuType = pSkuType;
	}
	
	/**
	 * Gets the TRU get site type util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUGetSiteTypeUtil getTRUGetSiteTypeUtil() {
		return mTRUGetSiteTypeUtil;
	}



	/**
	 * Sets the TRU get site type util.
	 *
	 * @param pTRUGetSiteTypeUtil the new TRU get site type util
	 */
	public void setTRUGetSiteTypeUtil(TRUGetSiteTypeUtil pTRUGetSiteTypeUtil) {
		this.mTRUGetSiteTypeUtil = pTRUGetSiteTypeUtil;
	}
	
	/**
	 * Gets the scheme detection util.
	 * 
	 * @return the scheme detection util
	 */
	public TRUSchemeDetectionUtil getSchemeDetectionUtil() {
		return mSchemeDetectionUtil;
	}

	/**
	 * Sets the scheme detection util.
	 * 
	 * @param pSchemeDetectionUtil
	 *            the new scheme detection util
	 */
	public void setSchemeDetectionUtil(TRUSchemeDetectionUtil pSchemeDetectionUtil) {
		this.mSchemeDetectionUtil = pSchemeDetectionUtil;
	}


	/**
	 * Gets the product page url.
	 *
	 * @param pOnlinePID the product online id
	 * @param pSiteId the product site id
	 * @return the product page url
	 */
	public String constructPDPURLWithOnlinePID(String pOnlinePID, String pSiteId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN :: Inside TRUProductPageUtil.constructPDPURLWithOnlinePID() method ");
		}
		StringBuffer productUrl = new StringBuffer();
		String siteProductionUrl = null;

		if(!StringUtils.isEmpty(pOnlinePID)) {
			Site currentSite = null;
			
			try {
					currentSite = getSiteContextManager().getSite(pSiteId);
				
				if(currentSite != null) {
					DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
					TRUSiteVO truSiteVO = getTRUGetSiteTypeUtil().getSiteInfo(request, currentSite);
					if(truSiteVO != null ) {
						siteProductionUrl = truSiteVO.getSiteURL();
					}
				}
			} catch (SiteContextException siteContextException) {
				vlogError(siteContextException, "SiteContextException occured in TRUProductPageUtil.constructPDPURLWithOnlinePID");
			}
			

			if(!StringUtils.isEmpty(siteProductionUrl)) {
				if(!siteProductionUrl.equals(TRUCommerceConstants.SINGLE_FORWARD_SLASH)){
					productUrl.append(getSchemeDetectionUtil().getScheme());
					productUrl.append(siteProductionUrl);
				}
				productUrl.append(getPdpPageUrl()).append(getAggrERecKeyUrl());
				productUrl.append(TRUCommerceConstants.EQUAL).append(pOnlinePID);
			}
		}

		if (isLoggingDebug()) {
			logDebug("END :: Inside TRUProductPageUtil.constructPDPURLWithOnlinePID() method ");
		}
		return productUrl.toString();
	}

	
	/**
	 * Gets the product page siteId.
	 *
	 * @param pOnlinePID the product online id
	 * @param pSiteId the site id
	 * @return the product page url
	 */
	@SuppressWarnings("unchecked")
	public String getProductPageURL(String pOnlinePID, String pSiteId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN :: Inside TRUProductPageUtil.getProductPageURL overloaded method ");
		}
		String productUrl = null;
		String curentSiteId = pSiteId;
		if(StringUtils.isEmpty(pOnlinePID) || StringUtils.isEmpty(curentSiteId)) {
			return productUrl;
		}
		RepositoryItem[] skuList = getCatalogTools().getSKUFromOnlinePID(pOnlinePID);
		if (skuList == null || skuList.length <= TRUConstants.ZERO) {
			return productUrl;
		}
		RepositoryItem currentSku = skuList[0];
		if(currentSku != null) {
			RepositoryItem siteItem = null;
			try {
				siteItem = getSiteManager().getSite(curentSiteId);
				Set<String> siteIds = (Set<String>) currentSku.getPropertyValue(getCatalogProperties().getSiteIDPropertyName());
				if(siteIds != null && !siteIds.isEmpty() && siteItem != null) {
					if(siteIds.contains(siteItem.getRepositoryId())) {
						curentSiteId = siteItem.getRepositoryId();
					} else {
						for(String siteid : siteIds) {
							if(!StringUtils.isEmpty(siteid)) {
								curentSiteId = siteid;
								break;
							}
						}
					}
				}
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					vlogError(repositoryException, "Error in : @class TRUProductPageUtil method getProductPageURL(..)");
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("The ONline PID is : {0} and the Site Id is : {1}", pOnlinePID, curentSiteId);
			}
			productUrl = constructPDPURLWithOnlinePID(pOnlinePID, curentSiteId);
		}
		if (isLoggingDebug()) {
			vlogDebug("END :: Inside TRUProductPageUtil.getProductSite overloaded method and the PDP URL is {0}", productUrl);
		}
		return productUrl;

	}


	/**
	 * Gets the product page url.
	 *
	 * @param pOnlinePID the product  online id
	 * @return the product page url
	 */
	public String getProductPageURL(String pOnlinePID) {

		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUProductPageUtil.getProductPageURL(pProductId).");
		}
		String productUrl = null;
		if(!StringUtils.isEmpty(pOnlinePID)) {
			String currentSiteId = SiteContextManager.getCurrentSiteId();
			productUrl = getProductPageURL(pOnlinePID, currentSiteId);
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUProductPageUtil.getProductPageURL(pProductId).");
		}
		return productUrl;
	}
	
	/**
	 * Gets the product page siteId.
	 *
	 * @param pProductId the product id
	 * @param pSiteId the site id
	 * @return the product siteId
	 */
	@SuppressWarnings("unchecked")
	public String getProductSite(String pProductId, String pSiteId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN :: Inside TRUProductPageUtil.getProductSite overloaded method ");
		}
		String currentSiteId = pSiteId;
		try {
			RepositoryItem productItem = getCatalogTools().findProduct(pProductId);
			if(productItem == null) {
				return null;
			}
			RepositoryItem siteItem = getSiteManager().getSite(currentSiteId);
			if(productItem.getPropertyValue(getCatalogProperties().getSiteIDPropertyName()) != null) {
				Set<String> siteIds = (Set<String>) productItem.getPropertyValue(getCatalogProperties().getSiteIDPropertyName());
				if(siteIds != null && !siteIds.isEmpty()) {
					if(siteIds.contains(siteItem.getRepositoryId())) {
						currentSiteId = siteItem.getRepositoryId();
					} else {
						for(String siteid : siteIds) {
							currentSiteId = siteid;
						}
					}
				}
			}
		}catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				vlogError(repositoryException, "Error in : @class TRUProductPageUtil method getProductSite(..)");
			}
		}
		if (isLoggingDebug()) {
			logDebug("END :: Inside TRUProductPageUtil.getProductSite overloaded method ");
		}
		return currentSiteId;

	}
}
