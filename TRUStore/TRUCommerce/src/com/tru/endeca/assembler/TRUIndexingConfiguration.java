package com.tru.endeca.assembler;

import java.util.Map;
/**
 * This is the Bean class having all the Endeca Indexing Configuration details. 
 *  @version 1.0
 *  @author Professional Access
 */
public class TRUIndexingConfiguration {
	/**
	 * String holds BaseApplicationName.
	 */
	private String mBaseApplicationName;
	/**
	 * String holds DefaultLanguageForApplications.
	 */
	private String mDefaultLanguageForApplications;
	/**
	 * String holds WorkbenchHostName.
	 */
	private String mWorkbenchHostName;
	/**
	 * String holds WorkbenchPort.
	 */
	private int mWorkbenchPort;
	/**
	 * String holds DefaultMdexHostName.
	 */
	private String mDefaultMdexHostName;
	/**
	 * String holds DefaultMdexPort.
	 */
	private int mDefaultMdexPort;
	/**
	 * String holds CASHostName.
	 */
	private String mCASHostName;
	/**
	 * String holds CASPort.
	 */
	private int mCASPort;
	/**
	 * String holds EACHostName.
	 */
	private String mEACHostName;
	/**
	 * String holds EACPort.
	 */
	private int mEACPort;
	/**
	 * Map holds ApplicationKey To MdexHostAndPort.
	 */
	private Map<String, String> mApplicationKeyToMdexHostAndPort;
	/**
	 * Map holds Key To ApplicationName.
	 */
	private Map<String,String> mKeyToApplicationName;

	/**
	 * Holds the log server host name.
	 */
	private String mLogServerHostName;

	/**
	 * Holds the log server port.
	 */
	private Integer mLogServerPort;

	/**
	 * Returns the base application name.
	 * 
	 * @return the baseApplicationName
	 */
	public String getBaseApplicationName() {
		return mBaseApplicationName;
	}
	/**
	 * @param pBaseApplicationName the baseApplicationName to set
	 */
	public void setBaseApplicationName(String pBaseApplicationName) {
		mBaseApplicationName = pBaseApplicationName;
	}
	/**
	 * @return the defaultLanguageForApplications
	 */
	public String getDefaultLanguageForApplications() {
		return mDefaultLanguageForApplications;
	}
	/**
	 * @param pDefaultLanguageForApplications the defaultLanguageForApplications to set
	 */
	public void setDefaultLanguageForApplications(
			String pDefaultLanguageForApplications) {
		mDefaultLanguageForApplications = pDefaultLanguageForApplications;
	}
	/**
	 * @return the workbenchHostName
	 */
	public String getWorkbenchHostName() {
		return mWorkbenchHostName;
	}
	/**
	 * @param pWorkbenchHostName the workbenchHostName to set
	 */
	public void setWorkbenchHostName(String pWorkbenchHostName) {
		mWorkbenchHostName = pWorkbenchHostName;
	}
	/**
	 * @return the workbenchPort
	 */
	public int getWorkbenchPort() {
		return mWorkbenchPort;
	}
	/**
	 * @param pWorkbenchPort the workbenchPort to set
	 */
	public void setWorkbenchPort(int pWorkbenchPort) {
		mWorkbenchPort = pWorkbenchPort;
	}
	/**
	 * @return the defaultMdexHostName
	 */
	public String getDefaultMdexHostName() {
		return mDefaultMdexHostName;
	}
	/**
	 * @param pDefaultMdexHostName the defaultMdexHostName to set
	 */
	public void setDefaultMdexHostName(String pDefaultMdexHostName) {
		mDefaultMdexHostName = pDefaultMdexHostName;
	}
	/**
	 * @return the defaultMdexPort
	 */
	public int getDefaultMdexPort() {
		return mDefaultMdexPort;
	}
	/**
	 * @param pDefaultMdexPort the defaultMdexPort to set
	 */
	public void setDefaultMdexPort(int pDefaultMdexPort) {
		mDefaultMdexPort = pDefaultMdexPort;
	}
	/**
	 * @return the cASHostName
	 */
	public String getCASHostName() {
		return mCASHostName;
	}
	/**
	 * @param pCASHostName the cASHostName to set
	 */
	public void setCASHostName(String pCASHostName) {
		mCASHostName = pCASHostName;
	}
	/**
	 * @return the cASPort
	 */
	public int getCASPort() {
		return mCASPort;
	}
	/**
	 * @param pCASPort the cASPort to set
	 */
	public void setCASPort(int pCASPort) {
		mCASPort = pCASPort;
	}
	/**
	 * @return the eACHostName
	 */
	public String getEACHostName() {
		return mEACHostName;
	}
	/**
	 * @param pEACHostName the eACHostName to set
	 */
	public void setEACHostName(String pEACHostName) {
		mEACHostName = pEACHostName;
	}
	/**
	 * @return the eACPort
	 */
	public int getEACPort() {
		return mEACPort;
	}
	/**
	 * @param pEACPort the eACPort to set
	 */
	public void setEACPort(int pEACPort) {
		mEACPort = pEACPort;
	}
	/**
	 * @return the applicationKeyToMdexHostAndPort
	 */
	public Map<String, String> getApplicationKeyToMdexHostAndPort() {
		return mApplicationKeyToMdexHostAndPort;
	}
	/**
	 * @param pApplicationKeyToMdexHostAndPort the applicationKeyToMdexHostAndPort to set
	 */
	public void setApplicationKeyToMdexHostAndPort(
			Map<String, String> pApplicationKeyToMdexHostAndPort) {
		mApplicationKeyToMdexHostAndPort = pApplicationKeyToMdexHostAndPort;
	}
	/**
	 * @return the keyToApplicationName
	 */
	public Map<String, String> getKeyToApplicationName() {
		return mKeyToApplicationName;
	}
	/**
	 * @param pKeyToApplicationName the keyToApplicationName to set
	 */
	public void setKeyToApplicationName(Map<String, String> pKeyToApplicationName) {
		mKeyToApplicationName = pKeyToApplicationName;
	}

	/**
	 * Returns the log server host name.
	 * 
	 * @return the log server host name
	 */
	public String getLogServerHostName() {
		return this.mLogServerHostName;
	}

	/**
	 * Sets the log server host name.
	 * 
	 * @param pLogServerHostName
	 *            the new log server host name
	 */
	public void setLogServerHostName(String pLogServerHostName) {
		this.mLogServerHostName = pLogServerHostName;
	}

	/**
	 * Returns the log server port.
	 * 
	 * @return the log server port
	 */
	public Integer getLogServerPort() {
		return this.mLogServerPort;
	}

	/**
	 * Sets the log server port.
	 * 
	 * @param pLogServerPort
	 *            the new log server port
	 */
	public void setLogServerPort(Integer pLogServerPort) {
		this.mLogServerPort = pLogServerPort;
	}


}
