package com.tru.common;


/**
 * This class contains tru integration specific configurations.
 * 
 * @version 1.0 
 * @author PA
 * 
 */

public class TRUIntegrationConfiguration extends TRUIntegrationVO {
	/**  property to hold enableCertona. */
	private boolean mEnableCertona;
	
	/**  property to hold enableCardinal. */
	private boolean mEnableCardinal;
	
	/**
	 * @return the enableCertona.
	 */
	public boolean isEnableCertona() {
		return mEnableCertona;
	}

	/**
	 * @param pEnableCertona the enableCertona to set.
	 */
	public void setEnableCertona(boolean pEnableCertona) {
		mEnableCertona = pEnableCertona;
	}

	/**
	 * @return the enableCardinal
	 */
	public boolean isEnableCardinal() {
		return mEnableCardinal;
	}

	/**
	 * @param pEnableCardinal the enableCardinal to set
	 */
	public void setEnableCardinal(boolean pEnableCardinal) {
		mEnableCardinal = pEnableCardinal;
	}
	
}
