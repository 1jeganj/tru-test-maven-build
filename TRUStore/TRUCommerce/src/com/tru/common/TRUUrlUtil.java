package com.tru.common;

import javax.servlet.http.HttpServletRequest;

import atg.core.util.StringUtils;

import com.tru.commerce.TRUCommerceConstants;

/**
 * This is a Utility class which helps in 
 * construction of the URL at run time .
 * 
 * 
 * 
 * @author PA 
 *
 */
public class TRUUrlUtil {

	/**
	 * 
	 * This method takes the input the request and URI and query string and returns the 
	 * URL 
	 * 
	 * @param pRequest the pRequest
	 * @param pUri the Uri
	 * @param pIsPortNeeded the port
	 * @param pQuery query
	 * @return String  the uri
	 */
	public static String  getConstructedUrl(HttpServletRequest pRequest,String pUri , boolean pIsPortNeeded1,String pQuery) {
		StringBuffer url  = new StringBuffer();
		url.append(pRequest.getScheme()).append(TRUCommerceConstants.COLON_SYMBOL).append(TRUCommerceConstants.PATH_SEPERATOR);
		url.append(TRUCommerceConstants.PATH_SEPERATOR);
		url.append(pRequest.getServerName()).append(TRUCommerceConstants.COLON_SYMBOL);
		url.append(TRUCommerceConstants.PATH_SEPERATOR);
		if (!StringUtils.isEmpty(pUri))
		{
			url.append(pUri);
		}
		if (!StringUtils.isEmpty(pQuery)) {
			url.append(TRUCommerceConstants.QUESTION_MARK).append(pQuery);
		}
		return url.toString();
	}
	

	/**
	 * Gets the constructed url1.
	 *
	 * @param pRequest the p request
	 * @param schema the schema
	 * @param pUri the p uri
	 * @param pQuery the p query
	 * @return the constructed url1
	 */
	public static String  getConstructedUrl(HttpServletRequest pRequest, final String scheme ,final String pUri ,final String pQuery) {
		StringBuffer url  = new StringBuffer();
		//Adding Schema
		url.append(scheme);
		//
		url.append(pRequest.getServerName())/*.append(TRUCommerceConstants.COLON_SYMBOL)*/;
		url.append(TRUCommerceConstants.PATH_SEPERATOR);
		if (StringUtils.isNotBlank(pUri))
		{
			url.append(pUri);
		}
		if (StringUtils.isNotBlank(pQuery)) {
			url.append(TRUCommerceConstants.QUESTION_MARK).append(pQuery);
		}
		return url.toString();
	}
}
