/**
 * 
 */
package com.tru.common;

import atg.nucleus.GenericService;

import com.tru.integrations.TRUIntegrationPropertiesConfig;
import com.tru.utils.TRUGetSiteTypeUtil;

/**
 * This class contains tru integration specific configurations.
 * @version 1.0
 * @author PA
 *
 */
public class TRUIntegrationVO  extends GenericService{
	/** property to hold mEnableCouponService. */
	private boolean mEnableCouponService;

	/**  property to hold enableAddressDoctor. */
	private boolean mEnableAddressDoctor;

	/**  property to hold enableEpslon. */
	private boolean mEnableEpslon;

	/**  property to hold enableHookLogic. */
	private boolean mEnableHookLogic;

	/**  property to hold enablePowerReviews. */
	private boolean mEnablePowerReviews;

	/**  property to hold enableTaxware. */
	private boolean mEnableTaxware;
	
	/**  property to hold enableRadialSoftAllocation. */
	private boolean mEnableRadialSoftAllocation;
	
	/** The Enable radial credit card. */
	private boolean mEnableRadialCreditCard;
	
	/** The Enable radial gift card. */
	private boolean mEnableRadialGiftCard;
	
	/** The Enable radial pay pal. */
	private boolean mEnableRadialPayPal;
	
	/** The Enable radial apple pay. */
	private boolean mEnableRadialApplePay;
	
	/** The Site util. */
	private TRUGetSiteTypeUtil mSiteUtil;

	/** The Integration properties config. */
	private TRUIntegrationPropertiesConfig mIntegrationPropertiesConfig;

	
	/**
	 * Gets the integration properties config.
	 *
	 * @return the integration properties config
	 */
	public TRUIntegrationPropertiesConfig getIntegrationPropertiesConfig() {
		return mIntegrationPropertiesConfig;
	}

	/**
	 * Sets the integration properties config.
	 *
	 * @param pIntegrationPropertiesConfig the new integration properties config
	 */
	public void setIntegrationPropertiesConfig(TRUIntegrationPropertiesConfig pIntegrationPropertiesConfig) {
		mIntegrationPropertiesConfig = pIntegrationPropertiesConfig;
	}

	/**
	 * Gets the site util.
	 *
	 * @return the site util
	 */
	public TRUGetSiteTypeUtil getSiteUtil() {
		return mSiteUtil;
	}

	/**
	 * Sets the site util.
	 *
	 * @param pSiteUtil the new site util
	 */
	public void setSiteUtil(TRUGetSiteTypeUtil pSiteUtil) {
		mSiteUtil = pSiteUtil;
	}
	
	/**
	 * @return the enableRadialCreditCard
	 */
	public boolean isEnableRadialCreditCard() {
		return mEnableRadialCreditCard;
	}

	/**
	 * @param pEnableRadialCreditCard the enableRadialCreditCard to set
	 */
	public void setEnableRadialCreditCard(boolean pEnableRadialCreditCard) {
		mEnableRadialCreditCard = pEnableRadialCreditCard;
	}

	/**
	 * @return the enableRadialGiftCard
	 */
	public boolean isEnableRadialGiftCard() {
		return mEnableRadialGiftCard;
	}

	/**
	 * @param pEnableRadialGiftCard the enableRadialGiftCard to set
	 */
	public void setEnableRadialGiftCard(boolean pEnableRadialGiftCard) {
		mEnableRadialGiftCard = pEnableRadialGiftCard;
	}

	/**
	 * @return the enableRadialPayPal
	 */
	public boolean isEnableRadialPayPal() {
		return mEnableRadialPayPal;
	}

	/**
	 * @param pEnableRadialPayPal the enableRadialPayPal to set
	 */
	public void setEnableRadialPayPal(boolean pEnableRadialPayPal) {
		mEnableRadialPayPal = pEnableRadialPayPal;
	}

	/**
	 * @return the enableRadialApplePay
	 */
	public boolean isEnableRadialApplePay() {
		return mEnableRadialApplePay;
	}

	/**
	 * @param pEnableRadialApplePay the enableRadialApplePay to set
	 */
	public void setEnableRadialApplePay(boolean pEnableRadialApplePay) {
		mEnableRadialApplePay = pEnableRadialApplePay;
	}

	/**
	 * @return the enableRadialSoftAllocation
	 */
	public boolean isEnableRadialSoftAllocation() {
		return mEnableRadialSoftAllocation;
	}

	/**
	 * @param pEnableRadialSoftAllocation the enableRadialSoftAllocation to set
	 */
	public void setEnableRadialSoftAllocation(boolean pEnableRadialSoftAllocation) {
		mEnableRadialSoftAllocation = pEnableRadialSoftAllocation;
	}
	/**
	 * @return the enableTaxware
	 */
	public boolean isEnableTaxware() {
		return mEnableTaxware;
	}
	/**
	 * @param pEnableTaxware the enableTaxware to set
	 */
	public void setEnableTaxware(boolean pEnableTaxware) {
		mEnableTaxware = pEnableTaxware;
	}
	/**
	 * @return the enableCouponService
	 */
	public boolean isEnableCouponService() {
		return mEnableCouponService;
	}

	/**
	 * @param pEnableCouponService the enableCouponService to set
	 */
	public void setEnableCouponService(boolean pEnableCouponService) {
		mEnableCouponService = pEnableCouponService;
	}

	/**
	 * @return the enableAddressDoctor
	 */
	public boolean isEnableAddressDoctor() {
		return mEnableAddressDoctor;
	}

	/**
	 * @param pEnableAddressDoctor the enableAddressDoctor to set
	 */
	public void setEnableAddressDoctor(boolean pEnableAddressDoctor) {
		mEnableAddressDoctor = pEnableAddressDoctor;
	}

	/**
	 * @return the enableEpslon
	 */
	public boolean isEnableEpslon() {
		if(mSiteUtil != null && mIntegrationPropertiesConfig != null){
			return mSiteUtil.isStoreIntegrationSiteConfigEnabled(getIntegrationPropertiesConfig().getEpslonIntegrationName());
		}else{
		return mEnableEpslon;
		}
	}

	/**
	 * @param pEnableEpslon the enableEpslon to set
	 */
	public void setEnableEpslon(boolean pEnableEpslon) {
		mEnableEpslon = pEnableEpslon;
	}

	/**
	 * @return the enableHookLogic
	 */
	public boolean isEnableHookLogic() {
		return mEnableHookLogic;
	}

	/**
	 * @param pEnableHookLogic the enableHookLogic to set
	 */
	public void setEnableHookLogic(boolean pEnableHookLogic) {
		mEnableHookLogic = pEnableHookLogic;
	}

	/**
	 * @return the enablePowerReviews
	 */
	public boolean isEnablePowerReviews() {
		return mEnablePowerReviews;
	}

	/**
	 * @param pEnablePowerReviews the enablePowerReviews to set
	 */
	public void setEnablePowerReviews(boolean pEnablePowerReviews) {
		mEnablePowerReviews = pEnablePowerReviews;
	}

}
