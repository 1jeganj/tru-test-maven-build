package com.tru.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.core.util.ContactInfo;

/**
 * The class holds the TRUUserSession property getters and setters.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUUserSession {

	/** property: flag indicating whether gift options page will be displayed. */
	private boolean mShowMeGiftingOptions = false;
	
	
	/** The Qty adjusted sku list. */
	private List<String> mQtyAdjustedSkuList;
	
	/** The Deleted sku list. */
	private Set<String> mDeletedSkuList;
	
	/** The Removal item list. */
	private Set<String> mRemovalItemList;
	
	/** property: stores PAYPAL token & payer Id. */
	private Map<String,String> mTSPCardInfoMap;
	
	/** property: TSP Billing address info. */
	private ContactInfo mTSPBillingAddressMap;
	
	/** property: TSP Billing address edited. */
	private boolean mTSPEditBillingAddress = false;
	
	/** property: R Us card selected. */
	private boolean mRusCardSelected = false;
	
	/** property: flag indicating whether billing address will be same as shipping address. */
	private boolean mBillingAddressSameAsShippingAddress = false;
	
	/** property: input email address. */
	private String mInputEmail;
	
	/** The Multiple items info messages. */
	private List<String> mMultipleItemsInfoMessages;
	
	/** The Item inline messages. */
	private Map<String, String> mItemInlineMessages;
	
	/**
	 * Gets the item inline messages.
	 *
	 * @return the item inline messages
	 */
	public Map<String, String> getItemInlineMessages() {
		if(null == mItemInlineMessages) {
			mItemInlineMessages = new HashMap<String, String>();
		}
		return mItemInlineMessages;
	}

	/**
	 * Sets the item inline messages.
	 *
	 * @param pItemInlineMessages the item inline messages
	 */
	public void setItemInlineMessages(Map<String, String> pItemInlineMessages) {
		mItemInlineMessages = pItemInlineMessages;
	}
	/**
	 * Gets the multiple items info messages.
	 *
	 * @return the multiple items info messages
	 */
	public List<String> getMultipleItemsInfoMessages() {
		if(null == mMultipleItemsInfoMessages) {
			mMultipleItemsInfoMessages = new ArrayList<String>();
		}
		return mMultipleItemsInfoMessages;
	}

	/**
	 * Sets the multiple items info messages.
	 *
	 * @param pMultipleItemsInfoMessages the new multiple items info messages
	 */
	public void setMultipleItemsInfoMessages(List<String> pMultipleItemsInfoMessages) {
		mMultipleItemsInfoMessages = pMultipleItemsInfoMessages;
	}
	/**
	 * Checks if is billing address same as shipping address.
	 *
	 * @return the billingAddressSameAsShippingAddress
	 */
	public boolean isBillingAddressSameAsShippingAddress() {
		return mBillingAddressSameAsShippingAddress;
	}

	/**
	 * Sets the billing address same as shipping address.
	 *
	 * @param pBillingAddressSameAsShippingAddress the billingAddressSameAsShippingAddress to set
	 */
	public void setBillingAddressSameAsShippingAddress(
			boolean pBillingAddressSameAsShippingAddress) {
		mBillingAddressSameAsShippingAddress = pBillingAddressSameAsShippingAddress;
	}
	
	/**
	 * Checks if is show me gifting options.
	 *
	 * @return the showMeGiftingOptions
	 */
	public boolean isShowMeGiftingOptions() {
		return mShowMeGiftingOptions;
	}

	/**
	 * Sets the show me gifting options.
	 *
	 * @param pShowMeGiftingOptions the showMeGiftingOptions to set
	 */
	public void setShowMeGiftingOptions(boolean pShowMeGiftingOptions) {
		mShowMeGiftingOptions = pShowMeGiftingOptions;
	}
	
	/** property: indicates shipping mode. */
	private String mShippingMode;

	/**
	 * Gets the shipping mode.
	 *
	 * @return the shippingMode
	 */
	public String getShippingMode() {
		return mShippingMode;
	}

	/**
	 * Sets the shipping mode.
	 *
	 * @param pShippingMode the shippingMode to set
	 */
	public void setShippingMode(String pShippingMode) {
		mShippingMode = pShippingMode;
	}
	
	/** property: indicates whether order reconciled. */
	private boolean mOrderReconciled;

	/**
	 * Checks if is order reconciled.
	 *
	 * @return the orderReconciled
	 */
	public boolean isOrderReconciled() {
		return mOrderReconciled;
	}

	/**
	 * Sets the order reconciled.
	 *
	 * @param pOrderReconciled the orderReconciled to set
	 */
	public void setOrderReconciled(boolean pOrderReconciled) {
		mOrderReconciled = pOrderReconciled;
	}
	
	/** property: stores PAYPAL token & payer Id. */
	private Map<String,String> mPaypalPaymentDetailsMap;
	
	/**
	 * Gets the paypal payment details map.
	 *
	 * @return the paypalPaymentDetailsMap
	 */
	public Map<String, String> getPaypalPaymentDetailsMap() {
		return mPaypalPaymentDetailsMap;
	}

	/**
	 * Sets the paypal payment details map.
	 *
	 * @param pPaypalPaymentDetailsMap the paypalPaymentDetailsMap to set
	 */
	public void setPaypalPaymentDetailsMap(
			Map<String, String> pPaypalPaymentDetailsMap) {
		mPaypalPaymentDetailsMap = pPaypalPaymentDetailsMap;
	}
	
	/** The In store pick up details map. */
	private Map<String,String> mInStorePickUpDetailsMap;
	
	/**
	 * Gets the in store pick up details map.
	 *
	 * @return the inStorePickUpDetailsMap
	 */
	public Map<String, String> getInStorePickUpDetailsMap() {
		return mInStorePickUpDetailsMap;
	}

	/**
	 * Sets the in store pick up details map.
	 *
	 * @param pInStorePickUpDetailsMap the inStorePickUpDetailsMap to set
	 */
	public void setInStorePickUpDetailsMap(
			Map<String, String> pInStorePickUpDetailsMap) {
		mInStorePickUpDetailsMap = pInStorePickUpDetailsMap;
	}

	
	/**
	 * Gets the qty adjusted sku list.
	 *
	 * @return the qty adjusted sku list
	 */
	public List<String> getQtyAdjustedSkuList() {
		return mQtyAdjustedSkuList;
	}

	/**
	 * Sets the qty adjusted sku list.
	 *
	 * @param pQtyAdjustedSkuList the new qty adjusted sku list
	 */
	public void setQtyAdjustedSkuList(List<String> pQtyAdjustedSkuList) {
		mQtyAdjustedSkuList = pQtyAdjustedSkuList;
	}

	/**
	 * Gets the deleted sku list.
	 *
	 * @return the deleted sku list
	 */
	public Set<String> getDeletedSkuList() {
		return mDeletedSkuList;
	}

	/**
	 * Sets the deleted sku list.
	 *
	 * @param pDeletedSkuList the new deleted sku list
	 */
	public void setDeletedSkuList(Set<String> pDeletedSkuList) {
		mDeletedSkuList = pDeletedSkuList;
	}

	/**
	 * Gets the removal item list.
	 *
	 * @return the removal item list
	 */
	public Set<String> getRemovalItemList() {
		return mRemovalItemList;
	}

	/**
	 * Sets the removal item list.
	 *
	 * @param pRemovalItemList the new removal item list
	 */
	public void setRemovalItemList(Set<String> pRemovalItemList) {
		mRemovalItemList = pRemovalItemList;
	}

	/**
	 * Gets the TSP card details map.
	 *
	 * @return the tSPCardInfoMap
	 */
	public Map<String, String> getTSPCardInfoMap() {
		return mTSPCardInfoMap;
	}

	/**
	 * Sets the TSP card details map.
	 *
	 * @param pTSPCardInfoMap the mTSPCardInfoMap to set
	 */
	public void setTSPCardInfoMap(Map<String, String> pTSPCardInfoMap) {
		this.mTSPCardInfoMap = pTSPCardInfoMap;
	}

	/**
	 * @return the mTSPBillingAddressMap
	 */
	public ContactInfo getTSPBillingAddressMap() {
		return mTSPBillingAddressMap;
	}

	/**
	 * @param pTSPBillingAddressMap the mTSPBillingAddressMap to set
	 */
	public void setTSPBillingAddressMap(ContactInfo pTSPBillingAddressMap) {
		this.mTSPBillingAddressMap = pTSPBillingAddressMap;
	}

	/**
	 * @return the mTSPEditBillingAddress
	 */
	public boolean isTSPEditBillingAddress() {
		return mTSPEditBillingAddress;
	}

	/**
	 * @param pTSPEditBillingAddress the mTSPEditBillingAddress to set
	 */
	public void setTSPEditBillingAddress(boolean pTSPEditBillingAddress) {
		this.mTSPEditBillingAddress = pTSPEditBillingAddress;
	}

	/**
	 * @return the inputEmail
	 */
	public String getInputEmail() {
		return mInputEmail;
	}

	/**
	 * @param pInputEmail the inputEmail to set
	 */
	public void setInputEmail(String pInputEmail) {
		mInputEmail = pInputEmail;
	}

	/**
	 * @return the mRusCardSelected
	 */
	public boolean isRusCardSelected() {
		return mRusCardSelected;
	}

	/**
	 * @param pRusCardSelected the mRusCardSelected to set
	 */
	public void setRusCardSelected(boolean pRusCardSelected) {
		this.mRusCardSelected = pRusCardSelected;
	}
	
	/** The Sign in hidden. */
	private boolean mSignInHidden;

	/**
	 * @return the signInHidden
	 */
	public boolean isSignInHidden() {
		return mSignInHidden;
	}

	/**
	 * @param pSignInHidden the signInHidden to set
	 */
	public void setSignInHidden(boolean pSignInHidden) {
		mSignInHidden = pSignInHidden;
	}
	
	/** The User session start time. */
	private Date mUserSessionStartTime;

	/**
	 * @return the userSessionStartTime
	 */
	public Date getUserSessionStartTime() {
		return mUserSessionStartTime;
	}

	/**
	 * @param pUserSessionStartTime the userSessionStartTime to set
	 */
	public void setUserSessionStartTime(Date pUserSessionStartTime) {
		mUserSessionStartTime = pUserSessionStartTime;
	}
}