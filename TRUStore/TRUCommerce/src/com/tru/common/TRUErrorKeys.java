package com.tru.common;

/**
 * File to put all the error keys used to display all the error messages.
 * @author Professional Access
 * @version 1.0
 */
public class TRUErrorKeys {
	
	/*keys for registration flow starts*/	
	/** Constant for holding error when some mandatory information is missing for create account flow. */
	public static final String TRU_ERROR_ACCOUNT_REGISTRATION_MISSING_MANDATORY_INFO = "tru_error_account_create_miss_mand_info";
	
	/** Constant for holding error when email is given but password is missing for create account flow. */
	public static final String TRU_ERROR_ACCOUNT_REGISTRATION_MISSING_PASSWORD = "tru_error_account_create_missing_pass";
	
	/** Constant for holding error when password and confirm password mismatch. */
	public static final String TRU_ERROR_ACCOUNT_PASSWORD_MISMATCH = "tru_error_account_confirm_password_password_mismatch";
	
	/** Constant for holding error when user tries to create account with invalid email pattern. */
	public static final String TRU_ERROR_ACCOUNT_REGISTRATION_INVALID_EMAIL = "tru_error_account_invalid_email";
	
	/** Constant for holding error when user tries to create account with invalid password pattern and already registered */
	public static final String TRU_ERROR_ACCOUNT_PASSWORD_REGISTERED = "tru_error_account_password_registered";
	
	/** Constant for holding error when user tries to create account with invalid password pattern. */
	public static final String TRU_ERROR_ACCOUNT_INVALID_PASSWORD = "tru_error_account_invalid_password";
	
	/** Constant for holding error when user tries to create account with already existing email. */
	public static final String TRU_ERROR_ACCOUNT_ALREADY_REGISTERED = "tru_error_account_already_registered";	
	
	/** Constant for holding error when user tries to create account or login again when he/she is already logged in. */
	public static final String TRU_ERROR_ACCOUNT_RESTRICT_LOGIN = "tru_error_account_restrict_login";
	/*keys for registration flow ends*/
	
	/*keys for login flow starts*/	
	/** Constant for holding error when some mandatory information is missing for login flow. */
	public static final String TRU_ERROR_ACCOUNT_LOGIN_MISSING_MANDATORY_INFO = "tru_error_account_login_miss_mand_info";
	
	/** Constant for holding error when user tries to login with invalid email pattern. */
	public static final String TRU_ERROR_ACCOUNT_LOGIN_INVALID_EMAIL = "tru_error_account_login_invalid_email";
	
	/** Constant for holding error when user tries to login with an email address which is not registered. */
	public static final String TRU_ERROR_ACCOUNT_EMAIL_NOT_REGISTERED = "tru_error_account_email_not_registered";
	
	/** Constant for holding error when user tries to login with an email address which is not registered. */
	public static final String TRU_ERROR_ACCOUNT_USER_NOT_FOUND = "tru_error_account_user_not_found";
	
	/** Constant for holding error when user tries to login with correct email but wrong password. */
	public static final String TRU_ERROR_ACCOUNT_WRONG_PASSWORD = "tru_error_account_user_wrong_password";	
	/** Constant for holding error when user tries to login with correct email but wrong password. */
	public static final String TRU_ERROR_ACCOUNT_PASSWORD_WRONG_PASSWORD = "tru_error_account_password_wrongpassword";
	/*keys for login flow ends*/
	
	/*keys for change password flow starts*/	
	/** Constant for holding error when some mandatory information is missing for change password flow. */
	public static final String TRU_ERROR_ACCOUNT_CHANGE_PASSWORD_MISSING_MANDATORY_INFO = "tru_error_account_ch_passwrd_miss_info";
	
	/** Constant for holding error when old password is wrong for change password flow. */
	public static final String TRU_ERROR_ACCOUNT_OLD_PASSWORD_WRONG = "tru_error_account_current_password_old_password_wrong";	
	/*keys for change password flow ends*/
	
	/*keys for forgot password flow starts*/
	/** Constant for holding error when some mandatory information is missing for forgot password flow. */
	public static final String TRU_ERROR_ACCOUNT_FORGOT_PASSWORD_MISSING_MANDATORY_INFO = "tru_error_account_frgt_pass_miss_info";
	
	/** Constant for holding error when email is invalid for forgot password flow. */
	public static final String TRU_ERROR_ACCOUNT_FORGOT_PASSWORD_INVALID_EMAIL = "tru_error_account_forgot_invalid_email";
	
	/** Constant for holding error when email is invalid for forgot password flow. */
	public static final String TRU_ERROR_ACCOUNT_FORGOT_PASSWORD_INVALID_USER = "tru_error_account_forgot_invalid_user";	
	
	/** Constant for holding error when email is invalid for forgot password flow. */
	public static final String TRU_ERROR_ACCOUNT_EMAIL_INVALID_USER = "tru_error_account_email_invalid_user";
	/*keys for forgot password flow ends*/
	
	/*keys for reset password flow starts*/
	/** Constant for holding error when some mandatory information is missing for reset password flow. */
	public static final String TRU_ERROR_ACCOUNT_RES_PASS_MISSING_MANDATORY_INFO = "tru_error_account_res_pass_mand_info";
	
	/** Constant for holding error when user with the email is not found. */
    public static final String TRU_ERROR_ACCOUNT_EMAIL_NOT_FOUND = "tru_error_account_email_not_found";
	/*keys for reset password flow ends*/
	
	/*keys for update personal info flow starts*/
	/** Constant for holding error when some mandatory information is missing for reset password flow. */
	public static final String TRU_ERROR_ACCOUNT_CHANGE_PERSONAL_DET_MISSING_INFO = "tru_error_account_persnl_det_miss_info";	
	/*keys for update personal info flow ends*/
	
	/*keys for change email flow starts*/
	/** Constant for holding error when some mandatory information is missing for reset password flow. */
	public static final String TRU_ERROR_ACCOUNT_CHANGE_EMAIL_MISSING_INFO = "tru_error_account_chng_email_miss_info";
	
	/** Constant for holding error when user tries to change their email with already registered other users email.  */
	public static final String TRU_ERROR_ACCOUNT_EMAIL_CHANGE_INVALID_PATTERN = "tru_error_account_email_change_invalid";
	
	/** Constant for holding error when email and confirm email mismatch. */
	public static final String TRU_ERROR_ACCOUNT_EMAIL_MISMATCH = "tru_error_account_email_mismatch";
	
	/** Constant for holding error when user tries to change their email with already registered other users email. */
	public static final String TRU_ERROR_ACCOUNT_EMAIL_ALREADY_EXISTS = "tru_error_account_email_already_exists";	
	
	/** Constant for holding error when some wrong mobile service acount update request is coming. */
	public static final String TRU_ERROR_ACCOUNT_UPDATE_INVALID_REQUEST = "tru_error_account_update_invalid_request";	
	
	/*keys for change email flow ends*/
	
	/*keys for add/update reward number flow starts*/
	/** Constant for holding error when some mandatory information is missing for add/update reward number flow. */
	public static final String TRU_ERROR_ACCOUNT_REWARD_NUMBER_MISSING_INFO = "tru_error_account_rwrd_no_miss_info";
	
	/** Constant for holding error when user types invalid reward number. */
	public static final String TRU_ERROR_ACCOUNT_REWARD_NUMBER_INVALID = "tru_error_account_reward_number_invalid";	
	
	/** Constant for holding error when user types invalid reward number inline. */
	public static final String TRU_ERROR_ACCOUNT_MEMBERSHIPID_INVALID = "tru_error_account_enterMembershipID_invalid";	
	/*keys for add/update reward number flow ends*/
	
	/*keys for add/edit address in address book starts*/
	/** Constant for holding error when some mandatory information is missing for add/update address in manage address book flow. */
	public static final String TRU_ERROR_ACCOUNT_ADDRESS_MISSING_INFO = "tru_error_account_address_miss_info";
	
	/** Constant for holding error when nickname for the address provided by user is already existing. */
	public static final String TRU_ERROR_ACCOUNT_DUPLICATE_ADDR_NICKNAME = "tru_error_account_duplicate_addr_nckname";
	
	/** Constant for holding error when nickname for the address provided by user is already existing. */
	public static final String TRU_ERROR_ACCOUNT_NICKNAME_EXISTS = "tru_error_account_nickname_duplicate";
	
	/** Constant for holding error when nickname is empty. */
	public static final String TRU_ERROR_ACCOUNT_ADDR_EMPTY_NICKNAME = "tru_error_account_addr_empty_nickname";
	
	/** Constant for holding error when nickname for the address provided by user is not found. */
	public static final String TRU_ERROR_ACCOUNT_ADDR_INVALID_NICKNAME = "tru_error_account_addr_invalid_nickname";
	
	/** Constant for holding error when saved address not found for this account. */
	public static final String TRU_ERROR_ACCOUNT_NO_AVAILABLE_ADDR = "tru_error_account_no_available_address";
	
	/** Constant for holding error when some mandatory information is missing for add/update address in manage address book/taxware flow. */
	public static final String TRU_ERROR_TAXWARE_ADDRESS_NOGEOCODE_RESPONSE = "tru_error_taxware_address_nogeocode_response";
	
	/*keys for add/edit address in address book ends*/
	
	/*keys for add/edit credit card flow starts*/
	/** Constant for holding error when user tries to change their email with already registered other users email.  */
	public static final String TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO = "tru_error_account_card_miss_info";
	
	/** Constant for holding error when nickname for the address provided by user is already existing. */
	public static final String TRU_ERROR_ACCOUNT_CARD_DUPLICATE_ADDR_NICKNAME = "tru_error_account_cc_dupl_addr_nckname";
	
	/** Constant for holding error when user tries to change their email with already registered other users email. */
	public static final String TRU_ERROR_ACCOUNT_CARD_EXPIRED = "tru_error_account_card_expired";
	
	/** Constant for holding error when order has expired credit card. */
	public static final String TRU_ERROR_ORDER_CREDIT_CARD_EXPIRED = "tru_error_order_credit_card_expired";
	
	/** Constant for holding error when user enters improper length card. */
	public static final String TRU_ERROR_ACCOUNT_CARD_INVALID = "tru_error_account_card_invalid";
	
	/** Constant for holding error when user enters card number with invalid chars. */
	public static final String TRU_ERROR_CHECK_CARD_NUMBER_HAS_INVALID_CHARS = "tru_error_card_number_has_invalid_chars";
	
	/** Constant for holding error when user enters card number doesnt match type. */
	public static final String TRU_ERROR_CHECK_CARD_NUMBER_DOESNT_MATCH_TYPE = "tru_error_card_number_doesnt_match_type";
	
	/** Constant for holding error when user enters card length not valid. */
	public static final String TRU_ERROR_CHECK_LENGTH_NOT_VALID = "tru_error_length_not_valid";
	
	/** Constant for holding error when user enters card number not valid. */
	public static final String TRU_ERROR_CHECK_CARD_NUMBER_NOT_VALID = "tru_error_card_number_not_valid";
	
	/** Constant for holding error when user enters card information not valid. */
	public static final String TRU_ERROR_CHECK_CARD_INFO_NOT_VALID = "tru_error_card_info_not_valid";
	
	/** Constant for holding error when user enters improper expiry. */
	public static final String TRU_ERROR_CHECK_CARD_EXP_DATE_NOT_VALID = "tru_error_card_exp_date_not_valid";
	
	/** Constant for holding error when user enters improper card type. */
	public static final String TRU_ERROR_CHECK_CARD_TYPE_NOT_VALID = "tru_error_card_type_not_valid";	
	
	/*keys for add/edit credit card ends*/
	
	/*keys for registration flow starts*/	
	/** Constant for holding error when user not enter coupon code. */
	public static final String TRU_ERROR_CART_COUPON_EMPTY = "tru_error_cart_coupon_empty";
	
	/** Constant for holding error when user  enter duplicate coupon code. */
	public static final String TRU_ERROR_CART_COUPON_DUPLICATE = "tru_error_cart_coupon_duplicate";
	
	/** Constant for holding error when user  enter coupon code  but having some issue. */
	public static final String TRU_ERROR_CART_COUPON_PLEASE_TRY_AGAIN = "tru_error_cart_coupon_please_try_again";
	
	/** Constant for holding error when user  enter coupon code  but expired. */
	public static final String TRU_ERROR_CART_COUPON_EXPIRED = "tru_error_cart_coupon_expired";
	
	/** Constant for holding error when user  enter coupon code  but having some issue. */
	public static final String TRU_ERROR_CART_COUPON_NOT_FOUND = "tru_error_cart_coupon_not_found";
	
	/** Constant for holding error when user  enter coupon code  but having some issue. */
	public static final String TRU_ERROR_CHECKOUT_COUPON_NOT_FOUND = "tru_error_checkout_promoCode_not_found";
	/** Constant for holding error when user  enter coupon code  but having some issue. */
	public static final String TRU_ERROR_CHECKOUT_COUPON_EXPIRED = "tru_error_checkout_promoCode_coupon_expired";
	/** Constant for holding error when user  enter coupon code  but having some issue. */
	public static final String TRU_ERROR_CHECKOUT_PLEASE_ENTER_VALID_SUCN = "tru_error_checkout_promoCode_please_enter_valid_sucn";
	
	/** Constant for holding error when user  enter coupon code  but promotion is disabled. */
	public static final String TRU_ERROR_CART_COUPON_PROMO_DISABLED = "tru_error_cart_coupon_promotion_disabled";
	/*keys for add/edit credit card ends*/
	/*keys for tell a friend flow starts*/
	/** Constant for holding error when user  enter empty friendName. */
	public static final String TRU_ERROR_EMAILTOFRIEND_FRIENDNAME = "tru_error_emailtofriend_invalid_friendname";
	/** Constant for holding error when user   enter empty yourName. */
	public static final String TRU_ERROR_EMAILTOFRIEND_YOURNAME = "tru_error_emailtofriend_invalid_yourname";
	/** Constant for holding error when user   enter empty friendMail. */
	public static final String TRU_ERROR_EMAILTOFRIEND_FRIENDMAIL = "tru_error_emailtofriend_friendmail";
	/** Constant for holding error when user   enter empty yourMail. */
	public static final String TRU_ERROR_EMAILTOFRIEND_YOURMAIL = "tru_error_emailtofriend_yourmail";
	/** Constant for holding error when user   enter empty fields. */
	public static final String TRU_ERROR_EMAILTOFRIEND_MISS_INFO = "tru_error_emailtofriend_miss_info";
	/** Constant for holding error when user   enter invalid mail id. */
	public static final String TRU_ERROR_EMAILTOFRIEND_INVALID_MAIL = "tru_error_emailtofriend_invalid_mail";
	/** Constant for holding error when user  enters invalid values in all the fields. */
	public static final String TRU_ERROR_EMAILTOFRIEND_INVALID_FORMAT_ALL = "tru_error_emailtofriend_invalid_format_all";
	/*keys for tell a friend flow ends.*/
	
	/*keys for layaway flow starts*/
	/** Constant for holding error when user  enter empty adress. */
	public static final String TRU_ERROR_LAYAWAY_ADDRESS_MISS_INFO = "tru_error_layaway_address_miss_info";
	
	/** Constant for holding error when user  enter invalid giftcard. */
	public static final String TRU_ERROR_LAYAWAY_INVALID_GIFT_CARD = "tru_error_layaway_invalid_gift_card";
	
	/** Constant for holding error when user  enter invalid pin. */
	public static final String TRU_ERROR_LAYAWAY_INVALID_PIN = "tru_error_layaway_invalid_pin";
	
	/** Constant for holding error when user  enter empty payment. */
	public static final String TRU_ERROR_LAYAWAY_PAYMENT_MISS_INFO = "tru_error_layaway_payment_miss_info";
	
	/** Constant for holding error when user  enter Invalid info. */
	public static final String TRU_ERROR_LAYAWAY_INVALID_INFO = "tru_error_layaway_invalid_info";
	
	/** Constant for holding error when the card validation fails. */
	public static final String TRU_ERROR_LAYAWAY_VERIFY_CARD_INFO = "tru_error_layaway_verify_card_info";
	
	/** Constant for holding error when invalid payment is submitted. */
	public static final String TRU_ERROR_LAYAWAY_INVALID_PAYMENT_SUBMITTED = "tru_error_layaway_invalid_payment_submitted";
	
	/** Constant for holding error when user enter payment amount greater than due amount in layaway flow. */
	public static final String TRU_ERROR_LAYAWAY_LAYAWAYPAYMENTAMOUNT = "tru_error_layaway_layawaypaymentamount";
	
	/** Constant for holding error when user enter payment amount greater than due amount in layaway flow. */
	public static final String TRU_ERROR_LAYAWAY_INVALID_PAYMENT_AMOUNT = "tru_error_layaway_invalid_payment_amount";
	
	/** Constant for holding error when user  enter invalid order number. */
	public static final String TRU_ERROR_LAYAWAY_INVALID_ORDER_NUMBER = "tru_error_layaway_invalid_order_number";
	
	/** Constant for holding error when there is no order in account. */
	public static final String TRU_ERROR_LAYAWAY_NO_ORDER_IN_ACCOUNT = "tru_error_layaway_no_order_in_account";
	
	/** Constant for holding error when there is no payment in submitted. */
	public static final String TRU_ERROR_LAYAWAY_NO_PAYMENT_SUBMITTED = "tru_error_layaway_no_payment_submitted";
	
	/** Constant for holding error when there is no gift card payment in submitted. */
	public static final String TRU_ERROR_LAYAWAY_ENTER_REQUIRED_GIFT_CARD = "tru_error_layaway_enter_required_gift_card";
	/*keys for layaway flow ends*/
	
	/** Constant for holding error when user  enters a invalid email. */
	public static final String TRU_ERROR_CHECKOUT_INVALID_EMAIL = "tru_error_checkout_invalid_email";
	/** Constant for holding error when user  enters profane words in gift message. */
	public static final String TRU_ERROR_CHECKOUT_PROFANE_WORD = "tru_error_checkout_profane_word";
	/** Constant for holding error when user  enters invalid remove credit cartname. */
	public static final String REMOVE_CREDIT_CARD_NAME = "tru_error_remove_creditcart_invalid_name";
	

	/** Constant for holding error when user tries to apply promo code even after reaching max limit. */
	public static final String TRU_ERROR_COUPON_MAX_LIMIT = "tru_error_coupon_max_limit";
	
	/** Constant for holding error when user  enters invalid reward number in checkout flow. */
	public static final String TRU_ERROR_CHECKOUT_REWARD_NUMBER_INVALID = "tru_error_checkout_reward_number_invalid";
	
	/** Constant for holding error when user  miss entering instore pickup data. */
	public static final String TRU_ERROR_CHECKOUT_MISS_INFO_INSTORE = "tru_error_checkout_miss_info_instore";
	
	/*keys for add/edit credit card flow starts*/
	/** Constant holds the gift card number with empty data. */
	public static final String TRU_ERROR_CHECKOUT_EMPTY_GC_NUMBER_INFO = "tru_error_checkout_empty_gc_number_info";
	
	/** Constant holds the gift card pin with empty data */
	public static final String TRU_ERROR_CHECKOUT_EMPTY_GC_PIN_INFO = "tru_error_checkout_empty_gc_pin_info";
	/** Constant holds the gift card number entered is not numeric. */
	public static final String TRU_ERROR_INVALID_GIFTCARD_NUMBER_NUMERIC = "tru_error_invalid_giftcard_number_numeric";
	/** Constant holds the length of gift card number is not correct. */
	public static final String TRU_ERROR_INVALID_GIFTCARD_NUMBER_LENGTH = "tru_error_invalid_giftcard_number_length";
	/** Constant holds the gift card pin entered is not numeric. */
	public static final String TRU_ERROR_INVALID_GIFTCARD_PIN_NUMERIC = "tru_error_invalid_giftcard_pin_numeric";
	/** Constant holds the length of gift card pin is not correct. */
	public static final String TRU_ERROR_INVALID_GIFTCARD_PIN_LENGTH = "tru_error_invalid_giftcard_pin_length";
	/** Constant holds the payeezy service not available error. */
	public static final String TRU_PAYEEZY_SERVICE_NOT_AVAILABLE = "tru_payeezy_service_not_available";
	/** Constant holds the server error. */
	public static final String TRU_ERROR_SERVER_EXCEPTION = "tru_error_server_exception";
	
	/**Constant holds the SHIPPING_ECONOMY_RESTRICTION error key. */
	public static final String SHIPPING_ECONOMY_RESTRICTION = "shipping_economy_restriction";
	/**Constant holds the SHIPPING_FREIGHT_RESTRICTION error key. */
	public static final String SHIPPING_FREIGHT_RESTRICTION = "shipping_freight_restriction";
	
	/** Constant for holding error when user  enters invalid nick name remove shippingaddress. */
	public static final String REMOVE_SHIPPING_NICK_NAME = "tru_error_remove_shipping_invalid_name";

	/** Constant for holding error when user  enters invalid edit credit cartname. */
	public static final String EDIT_CREDIT_CARD_NAME = "tru_error_edit_creditcart_invalid_name";
	/** The Constant Confirm. */
	public static final String PREFERRED_METHOD_NOT_ELIGIBLE_FOR_ALL_MSG = "preferred_method_not_eligible_for_all_msg";

	/** The Constant ERR_OTHER_PAYMENT_OPTION_IS_REQUIRED. */
	public static final String ERR_OTHER_PAYMENT_OPTION_IS_REQUIRED = "ERR_OTHER_PAYMENT_OPTION_IS_REQUIRED";
	
	/** The Constant GIFT_CARDS_BALANCE_IS_LESS_THAN_ORDER_AMOUNT. */
	public static final String GIFT_CARDS_BALANCE_IS_LESS_THAN_ORDER_AMOUNT = "tru_error_gift_cards_balance_is_less_than_order_amount";
	
	/** Constant for holding error when user does not enter gift card number and pin. */
	public static final String TRU_ERROR_ENTER_GIFT_CARD_NUMBER_AND_PIN = "tru_error_enter_gift_card_number_and_pin";	
	/** Constant for holding error when user does not enter gift card number. */
	public static final String TRU_ERROR_ENTER_GIFT_CARD_NUMBER = "tru_error_enter_gift_card_number";
	/** Constant for holding error when user enters invalid gift card number. */
	public static final String TRU_ERROR_CHECKOUT_GCNUMBER_INVALID_CARDNUMBER = "tru_error_checkout_gcNumber_invalid_cardNumber";
	/** Constant for holding error when user enters invalid gift card number. */
	public static final String TRU_ERROR_ACCOUNT_GCNUMBER_INVALID = "tru_error_checkout_gcNumber_invalid";
	/** Constant for holding error when user enters gift card format. */
	public static final String TRU_ERROR_ENTER_GIFT_CARD_NUMBER_NUMERIC = "tru_error_enter_gift_card_number_numeric";
	/** Constant for holding error when user does not enter gift card pin. */
	public static final String TRU_ERROR_ENTER_GIFT_CARD_PIN = "tru_error_enter_gift_card_pin";
	/** Constant for holding error when user enters invalid pin. */
	public static final String TRU_ERROR_ENTER_VALID_GIFT_CARD_PIN = "tru_error_enter_valid_gift_card_pin";
	/** Constant for handling error when user enters invalid pin. */
	public static final String TRU_ERROR_ACCOUNT_PIN_INVALID = "tru_error_account_ean_invalid";
	/** Constant for holding error when user enters invalid pin format. */
	public static final String TRU_ERROR_ENTER_GIFT_CARD_PIN_NUMERIC = "tru_error_enter_gift_card_pin_numeric";
	/** Constant for holding error when user enters invalid pin format. */
	public static final String TRU_ERROR_INSUFFICIENT_ORDER_AMOUNT = "tru_error_insufficient_order_amount";
	/** Constant for holding error to request user to try again after some time. */
	public static final String TRU_ERROR_TRY_AGAIN_AFTER_SOME_TIME = "tru_error_try_again_after_some_time";
	/** Constant for holding error as error while processing the request. */
	public static final String TRU_ERROR_WHILE_PROCESSING_REQUEST = "tru_error_while_processing_request";
	/** Constant for holding error when user tries to enter more than 5 gift cards. */
	public static final String TRU_ERROR_ADD_MAXIMUM_FIVE_GIFT_CARDS = "tru_error_add_maximum_five_gift_cards";
	/** Constant for holding error when user tries to enter same gift card again. */
	public static final String TRU_ERROR_GIFT_CARD_ALREADY_APPLIED = "tru_error_gift_card_already_applied";
	/** Constant for holding error for gift card zero balance. */
	public static final String TRU_ERROR_GIFT_CARD_BALANCE_ZERO = "tru_error_gift_card_balance_zero";

	/** The Constant tru error gift card invalid pin. */
	public static final String TRU_ERROR_GIFT_CARD_INVALID_PIN = "tru_error_gift_card_invalid_pin";
	
	/** Constant for holding error of missing info. */
	public static final String TRU_ERROR_GIFT_OPTIONS_MISSING_INFO = "tru_error_gift_options_missing_info";
	
	/** Constant for holding error of missing info when required field is not passing from rest service. */
	public static final String TRU_ERROR_MISSING_INFO = "tru_error_missing_info";		
	
	/** Constant for holding error of captcha missing info. */
	public static final String TRU_ERROR_CAPTCHA_MISSING_INFO = "tru_error_captcha_not_entered";
	public static final String TRU_ERROR_CAPTCHA_INCORRECT_INFO ="tru_error_incorrect_capthcha";
	
	/** Constant for holding error when some mandatory information is missing for gift wrap in gifting flow. */
	public static final String TRU_ERROR_GIFT_WRAP_MISSING_INFO = "tru_error_gift_wrap_miss_info";
	
	/** Constant for holding error when some mandatory information is missing for gift wrap in gifting flow. */
	public static final String TRU_ERROR_NOTIFY_ME_MISSING_INFO = "tru_error_notify_me_miss_info";

	/**Constant holds the no_ship_method_selected error key ,If no ship method is selected on shipping page. */
	public static final String TRU_ERROR_SHIPPING_NO_SHIP_METHOD_SELECTED = "no_ship_method_selected";

	/**Constant holds the tru_error_checkout_no_preferred_ship_method error key, If selected ship method is not eligible for 
	 * all the cart items, Then the above error message will be displayed. */
	public static final String TRU_ERROR_SHIPPING_NO_PREFERRED_SHIP_METHOD="tru_error_checkout_no_preferred_ship_method";
	
	
	/**Constant holds the tru_error_shipping_address_miss_info error key, If shipping info is missed in shipping groups. */
	public static final String TRU_ERROR_SHIPPING_ADDRESS_MISSING_INFO="tru_error_shipping_address_missing_info";
	
	/**Constant holds the tru_error_checkout_no_ship_methods error key, If no eligible ship methods for the selected or entered address. */
	public static final String TRU_ERROR_SHIPPING_NO_SHIP_METHODS_MULTISHIP="tru_error_checkout_no_ship_methods_multiship";
	
	/**Constant holds the tru_error_checkout_no_ship_methods error key, If no eligible ship methods for the selected or entered address. */
	public static final String TRU_ERROR_SHIPPING_NO_SHIP_METHODS_FOUND="tru_error_checkout_no_ship_methods_found";
	
	/** The Constant error for recommendation status not in list. */
    public static final String TRU_ERROR_CHECKOUT_RECOMENDATION_STATUS_NOT_DEFINED_IN_LIST = "tru_error_checkout_recomendation_status_not_defined_in_list";

    /** The Constant error for address doctor - no connection. */
	public static final String TRU_ERROR_ADDRESS_DOCTOR_NO_CONNECTION = "tru_error_address_doctor_no_connection";
	
	/** The Constant error for account resent password. */
	public static final String TRU_RESENT_PASSWORD = "tru_error_account_resent_password";
	/** The Constant error for valid sucn coupon. */
	public static final String TRU_PLEASE_ENTER_VALID_SUCN = "tru_please_enter_valid_sucn";
	/** The Constant error for valid sucn coupon. */
	public static final String TRU_PLEASE_ENTER_VALID_COUPON_CODE = "tru_please_enter_valid_coupon_code";
	/** Constant for holding error when user enters invalid gift card number. */
	public static final String TRU_ERROR_ENTER_VALID_GIFT_CARD_NUMBER = "tru_error_enter_valid_gift_card_number";
	/** constant for response status ADDRESS_DOCTOR_PLACE_ORDER_ERROR*/
	public static final String TRU_ERROR_ADDRESS_DOCTOR_PLACE_ORDER = "tru_error_address_doctor_place_order";
	
	/**Constant holds the tru_error_ship_restriction_to_freight_class error key, If any items has shipping restriction. 
	 * Example : TRU_SHPA items having restriction for PO Box addresses*/
	public static final String TRU_ERROR_FREIGHT_CLASS_RESTRICTION_TO_ADDRESS="tru_error_freight_class_restriction_to_address";
	
	
	/**Constant holds the tru_error_economy_ship_method_restriction_to_address error key, If ship method has restriction to the shipping address. 
	 * Example : ECONOMY is not eligible for AK
	*/
	public static final String TRU_ERROR_ECONOMY_SHIP_METHOD_RESTRICTION_TO_ADDRESS="tru_error_economy_ship_method_restriction_to_address";
	
	/**Constant holds the tru_error_shptruck_ship_method_restriction_to_address error key, If ship method has restriction to the shipping address. 
	 * SHPTRUCK is not eligible for APO*/
	public static final String TRU_ERROR_SHPTRUCK_SHIP_METHOD_RESTRICTION_TO_ADDRESS="tru_error_shptruck_ship_method_restriction_to_address";
	
	/** The Constant TRU_AGREED_FINANCE_BUT_ORDER_IS_NOT_ELLIGIBLE. */
	public static final String TRU_AGREED_FINANCE_BUT_ORDER_IS_NOT_ELLIGIBLE="tru_error_order_not_elligible_for_finance";
	
	/** The Constant TRU_AGREED_FINANCE_BUT_CURRENT_ORDER_HAS_DIFF_FINANCING_TERM_VALUE. */
	public static final String TRU_AGREED_FINANCE_BUT_CURRENT_ORDER_HAS_DIFF_FINANCING_TERM_VALUE="tru_error_current_order_has_diff_finance_value";
	
	/** The Constant TRU_ERROR_MANDATORY_FIELD_MISSING_MANDATORY_INFO. */
	public static final String TRU_ERROR_MANDATORY_FIELD_MISSING_MANDATORY_INFO = "tru_error_mandatory_field_miss_info"; 
	
	/** The Constant TRU_ERROR_INVALID_EMAIL_INFO. */
	public static final String TRU_ERROR_INVALID_EMAIL_INFO = "tru_error_invalid_email_info";
	
	/** The Constant TRU_PAYPAL_GENERIC_ERR_KEY. */
	public static final String TRU_PAYPAL_GENERIC_ERR_KEY = "PAYPAL_GENERIC_ERR_KEY";
	
	/** The Constant TRU_PAYPAL_TIMEOUT_ERR_KEY for timeout error. */
	public static final String TRU_PAYPAL_TIMEOUT_ERR_KEY = "PAYPAL_TIME_OUT_ERR_KEY";
	
	/** The Constant TRU_PAYPAL_FAILURE_ERR_KEY for failure response. */
	public static final String TRU_PAYPAL_FAILURE_ERR_KEY = "PAYPAL_FAILURE_ERR_KEY";
	
	/** The Constant TRU_ERROR_APPLEPAY_FORM_MISS_INFO for failure response. */
	public static final String TRU_ERROR_APPLEPAY_FORM_MISS_INFO = "tru_error_applepay_Form_miss_info";
	
	/** The Constant TRU_ERROR_APPLEPAY_PERSONALEMAIL_MISS_INFO for failure response. */
	public static final String TRU_ERROR_APPLEPAY_PERSONALEMAIL_MISS_INFO = "tru_error_applepay_personalEmail_miss_info";
	
	/** The Constant TRU_ERROR_APPLEPAY_PERSONALPHONE_MISS_INFO for failure response. */
	public static final String TRU_ERROR_APPLEPAY_PERSONALPHONE_MISS_INFO = "tru_error_applepay_personalphone_miss_info";
	
	/** The Constant TRU_BILLING_ADDRESS_EMPTY for null billing address in order */
	public static final String TRU_BILLING_ADDRESS_EMPTY = "tru_billing_address_empty";
	/** The Constant TRU_BILLING_ADDRESS_NICKNAME_INVALID for invalid billing address nickname in order */
	public static final String TRU_BILLING_ADDRESS_NICKNAME_INVALID = "tru_billing_address_nickname_invalid";
	
	/** The Constant TRU_ERROR_CURRENT_ORDER_MAY_QUALIFY_FOR_SPECIAL_FINANCING. */
	public static final String TRU_ERROR_CURRENT_ORDER_MAY_QUALIFY_FOR_SPECIAL_FINANCING="tru_error_current_order_may_qualify_for_special_financing";
	
	/** The Constant TRU_ERROR_COMMON_CC_BIN_RANGES_NOT_AVAILABLE. */
	public static final String TRU_ERROR_COMMON_CC_BIN_RANGES_NOT_AVAILABLE  = "tru_error_common_cc_bin_ranges_not_available";
	
	 public static final String INVALID_OUTOFSTOCK_MSG = "tru_Inventory_outofstock_err_message"; 
	 
	 public static final String INVALID_OUTOFSTOCK_CART_MSG = "tru_Inventory_outofstock_cart_err_message"; 
}

