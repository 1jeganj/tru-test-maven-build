package com.tru.common;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import atg.nucleus.GenericService;

import com.tru.commerce.constants.TRUCheckoutConstants;

/**
 * The Class TRUConfiguration.
 * @author Professional Access
 * @version 1.0
 */
public class TRUConfiguration extends GenericService{
	
	/**  property to hold mEnableSkuPricingCache. */
	private boolean mEnableSkuPricingCache;
	
	/**  property to hold mEnableSiteCookieCreationInJS. */
	private boolean mEnableSiteCookieCreationInJS;
	
	/**  property to hold mAkamaiSiteParam. */
	private String mAkamaiSiteParam;
	
	/**  property to hold msosErrorURL. */
	private String mSosErrorUrl;
	
	/**  property to hold mDetailsURL. */
	private String mDetailsURL;

	/**  property to hold mSosBruNortonSealURL. */
	private String mSosBruNortonSealURL;

	/**  property to hold mSosTruNortonSealURL. */
	private String mSosTruNortonSealURL;

	/**  property to hold mEpsilonPixelURL. */
	private String mEpsilonPixelURL;

	/**  property to hold mShopLocalURL. */
	private String mShopLocalURL;

	/**  property to hold mDomainUrl. */
	private String mDomainUrl;

	/** property to hold mPortRequired. */
	private boolean mPortRequired;
	/** The  mLightboxConfigurableTime configurable time. */
	private String mLightboxConfigurableTime;

	/** The mLightboxsubmitConfigurableTime configurable time. */
	private String mLightboxsubmitConfigurableTime;

	/** The mLightboxdonotshowConfigurableTime configurable time. */
	private String mLightboxdonotshowConfigurableTime;

	/** property to hold display number of cards. */
	private String mDisplayNoOfCards;

	/**  property to hold enableAddressDoctor. */
	private boolean mEnableAddressDoctor;

	/**  property to hold enableEpslon. */

	private boolean mEnableEpslon;

	/** property to hold http. */
	private String mHTTP;

	/**  Holds store number. */
	private String mStoreNumber;

	/**  Holds English locale. */
	private String mEnglishLocale;

	/**  property to hold mOlsonSignUpURL. */
	private String mOlsonSignUpURL;

	/** property to hold OrderRepricingOption. */
	public String mOrderRepricingOption;

	/**  property to hold olsonLookUpURL. */
	private String mOlsonLookUpURL;
	/**  property to hold editPrefURL. */
	private String mEditPrefURL;

	/** Holds location id for ship to home. */
	private String mLocationIdForShipToHome;

	/** Holds mTruSkuTypes value. */
	private List<String> mTruSkuTypes;

	/** Holds mPriceOnCartValue value. */
	private String mPriceOnCartValue;

	/** The mregistry url. */
	private String mRegistryUrl;

	/** The mwishlist url. */
	private String mWishlistUrl;
	
	/** The Registry search url. */
	private String mRegistrySearchUrl;
	
	/** The Wishlist search url. */
	private String mWishlistSearchUrl;
	
	/** The Sos registry url. */
	private String mSosRegistryUrl;
	
	/** The Sos wishlist url. */
	private String mSosWishlistUrl;

	/** The myAccountRegistryUrl. */
	private String mMyAccountRegistryUrl;

	/** The myAccountWishlistUrl. */
	private String mMyAccountWishlistUrl;

	/** Holds mTruVideoGameEsrb value. */
	private List<String> mTruVideoGameEsrb;

	/** property to hold mEnableCouponService. */
	private boolean mEnableCouponService;

	/** Holds promotion count in pdp value. */
	private String mPromotionCountinPDP;

	/**
	 * Holds Shipping Page URL.
	 */
	private String mShippingPageURL;
	/**
	 * Holds Gift Options Page URL.
	 */
	private String mGiftOptionsPageURL;
	/** property to hold mEnableApplePayMockAddresses. */
	private boolean mEnableApplePayAddresses;

	/** Holds  mApplePayAddress1 */
	private String mApplePayAddress1;

	/** Holds mApplePayFirstName */
	private String mApplePayFirstName;
	/** Holds  mApplePayLastName */
	private String mApplePayLastName;
	/**
	 * Holds Payment Page URL.
	 */
	private String mPaymentPageURL;

	/**
	 * Holds Payment mImageServerURL.
	 */
	private String mImageServerURL;

	/**
	 * Holds Payment mImageServerFolderPath.
	 */
	private String mImageServerFolderPath;

	/**
	 * Holds property olsonLearnMoreURL.
	 */
	private String mOlsonLearnMoreURL;

	/** Property to hold m_MaxGiftCards. */
	private int mMaxGiftCards;

	/**
	 * property to hold max gift card length.
	 */
	private int mGiftCardNumberLength;

	/**
	 * property to hold max gift card length.
	 */
	private int mGiftCardPinLength;

	/**
	 * Holds Payment Page mLimitOfdaysToTellAProductNew.
	 */
	private int mLimitOfdaysToTellAProductNew;

	/** Holds disable promotion rest service URL value. */
	private String mDisableRestServiceURL;

	/** Holds Project Name rest service URL value. */
	private String mProjectNameWithIdRestServiceURL;
	
	/**
	 * Holds Search Dex URL.
	 */
	private String mSearchDexUrl;

	/**
	 * Holds  Akamai URL.
	 */
	private String mAkamaiUrl;

	/**
	 * Holds  SearchDex Footer Page.
	 */
	private String mSearchDexFooterPage;

	/**
	 * Holds  SearchDex Header Page.
	 */
	private String mSearchDexHeaderPage;

	/**  property to hold enableInlineSeo. */
	private boolean mEnableInlineSeo;

	/**  property to hold CheckoutTabUrls. */
	private Map<String, String> mCheckoutTabUrls;

	/**  property to hold CheckoutTabNumbers. */
	private Map<String, String> mCheckoutTabNumbers;

	/** Property to hold mPayPalExpChkoutSuccessUrl. */
	private String mPayPalExpChkoutSuccessUrl;

	/** Property to hold mPayPalExpChkoutCancelUrl. */
	private String mPayPalExpChkoutCancelUrl;

	/** Property to hold mCartCheckoutWithPayPalSuccessURL. */
	private String mCartCheckoutWithPayPalSuccessURL;

	/** Property to hold mCartCheckoutWithPayPalEURL. */
	private String mCartCheckoutWithPayPalErrorURL;

	/** Property to hold mCartCheckoutWithPayPalSuccessURL. */
	private String mCheckoutWithPayPalSuccessURL;

	/** Property to hold mCartCheckoutWithPayPalEURL. */
	private String mCheckoutWithPayPalErrorURL;


	/** Property to hold payPalExpiryTime. */
	private String mPayPalTokenExpiryTime;

	/**  property to hold mSizeSequenceMap. */
	private Map<String, String> mSizeSequenceMap;

	/**  property to hold mSortingEnabled. */
	private boolean mSortingEnabled;

	/**  property to hold mColorSequenceMap. */
	private Map<String, String> mColorSequenceMap;

	/**
	 * Holds pickup Page URL.
	 */
	private String mPickupPageURL;

	/** Property to hold mPayPalChkoutSuccessUrl. */

	private String mPayPalChkoutSuccessUrl;

	/** Property to hold mPayPalChkoutCancelUrl. */
	private String mPayPalChkoutCancelUrl;

	/**
	 * Property to hold mSixMonthFinancingLimit.
	 */
	private double mSixMonthFinancingLimit;

	/**
	 * Property to hold mTwelveMonthFinancingLimit.
	 */
	private double mTwelveMonthFinancingLimit;
	/**  property to hold displayPayPalPaymentTypeName. */
	public Map<String, String> mDisplayPayPalPaymentTypeName = new HashMap<String,String>();


	/** This flag holds the EDD flag value . */
	private boolean mEddEnabled;

	/** Hold the edd cutoff time. */
	private int mEddCutOffTime;

	/** Property to hold mHolidaysIgnored config value. */
	private boolean mHolidaysIgnored;

	/** Property to hold timeInTransitShipMethods . */
	private List<String> mTimeInTransitShipMethodCodes;

	/** Property to hold MerchantContactDettails . */
	private Map<String,String> mSDMerchangeContactInfo = new HashMap<String,String>();

	/** Property to hold DBA Name . */
	private Map<String,String> mSDDBAName;

	/** Property to hold Street . */
	private Map<String,String> mSDStreet;

	/** Property to hold City . */
	private Map<String,String> mSDCity;

	/** Property to hold Region . */
	private Map<String,String> mSDRegion;

	/** Property to hold Merchant Id . */
	private Map<String,String> mSDMid;

	/** Property to hold Merchant Category *. */
	private Map<String,String> mSDMcc;

	/** Property to hold PostalCode *. */
	private Map<String,String> mSDPostalCode;

	/** Property to hold TaxNumber *. */
	private Map<String,String> mTaxNumber;

	/** Property to hold TokenType *. */
	private Map<String,String> mTokenType;

	/** Property to hold mCreditCardTypeNamesForIntegration *. */
	private Map<String,String> mCreditCardTypeNamesForIntegration;

	/** Property to hold the currencyCodes string. */
	private Map<String,String> mCurrencyCodesString = new HashMap<String,String>();

	/** holds channelTypeMap *. */
	private Map<String, String> mChannelTypeMap;

	/** The Copy ship item rel properties. */
	private List<String> mCopyShipItemRelProperties;

	/** Holds power Reviews URL value. */
	private String mPowerReviewsURL;

	/** Holds power Reviews Full URL value. */

	/** Holds power Reviews AppLaunch URL value. */
	private String mPowerReviewsApiKey;


	/** The Charity donation redirect url. */
	private String mCharityDonationRedirectURL;

	/** Holds Recaptcha Script URL value. */
	private String mRecaptchaScriptURL;

	/** Holds Recaptcha NoScript URL value. */
	private String mRecaptchaNoScriptURL;

	/** Holds Recaptcha PublicKey value. */
	private String mRecaptchaPublicKey;

	/** Holds Recaptcha PrivateKey value. */
	private String mRecaptchaPrivateKey;
	
	/** Holds Recaptcha Secret value. */
	private String mRecaptchaSecretKey;
	
	/** Holds Recaptcha URL. */
	private String mRecaptchaURL;
	
	private String mHeaderparamForIpAddress;
	
	/**
	/**
	 * Holds the OMS Order Details Rest API URL.
	 */
	private String mOmsOrderDetailRestAPIURL;

	/**
	 * Holds the OMS Order List Rest API URL.
	 */
	private String mOmsOrderListRestAPIURL;
	/**
	 * Holds the OMS Cancel Order Rest API URL.
	 */
	private String mOmsCancelOrderRestAPIURL;
	
	/**
	 * Holds the Integration logging Rest API URL.
	 */
	private String mIntegrationLoggingRestAPIURL;

	/** The trucss path. */
	private String mTRUCSSPath;

	/** The trujs path. */
	private String mTRUJSPath;

	/** The tru image path. */
	private String mTRUImagePath;
	/** The mlog audit message into db. */
	private boolean mLogAuditMessageIntoDB;

	/** hold double property DistanceForNearByStoreSearch. */
	private double mDistanceForNearByStoreSearch;

	/** hold boolean property channelAvailability. */
	private String mChannelAvailabilityForBoth;

	/** hold boolean property mEnableCreditCardFullAuthFailure. */
	private boolean mEnableCreditCardFullAuthFailure;

	/** hold boolean property mFailedCreditCardAmt. */
	private String mFailedCreditCardAmt;

	/** hold boolean property mEnablePayPalFullAuthFailure. */
	private boolean mEnablePayPalFullAuthFailure;

	/** hold boolean property mFailedPayPalAmt. */
	private String mFailedPayPalAmt;

	/** hold boolean property mEnableGiftCardFullAuthFailure. */
	private boolean mEnableGiftCardFullAuthFailure;

	/** hold boolean property mFailedGiftCardAmt. */
	private String mFailedGiftCardAmt;

	/** hold boolean property mEnableCreditCardVoidFailure. */
	private boolean mEnableCreditCardVoidFailure;

	/**
	 * hold boolean property to  check inventory online.
	 */
	private boolean mCheckInventoryOnline;

	/**
	 * Holds the Gift Card Refund failure enable.
	 */
	private boolean mEnableGiftCardRefundFailure;

	/**
	 * Hold Gift Card Refund fail amount.
	 */
	private String mFailedGiftCardRefundAmt;

	/** Hold SoftDesDbaName. */
	private String mSoftDesDbaName;

	/** Hold SoftDesDbaName. */
	private String mSoftDesStreet;

	/** Hold SoftDesDbaName. */
	private String mSoftDesCity;

	/** Hold SoftDesDbaName. */
	private String mSoftDesRegion;

	/** Hold SoftDesDbaName. */
	private String mSoftDesMid;

	/** Hold SoftDesDbaName. */
	private String mSoftDesMcc;

	/** Hold SoftDesDbaName. */
	private String mSoftDesPostalCode;


	/** Holds shopping cart parameter. */
	private String mShoppingCartParameter;

	/** property to hold mFraudAcceptEmail. */
	private String mFraudAcceptEmail;

	/** property to hold mCsrEmailEnabled. */
	private boolean mCsrEmailEnabled;

	/** property to hold mCardCVV. */
	private String mCardCVV;

	/** The m enable sync cookie. */
	private boolean mEnableSyncCookie;

	/** property to hold mExpMonthAdjustmentForPLCC. */
	private String mExpMonthAdjustmentForPLCC;

	/** property to hold mExpYearAdjustmentForPLCC. */
	private String mExpYearAdjustmentForPLCC;

	/** property to hold cardNumberPLCC. */
	private String mCardNumberPLCC;

	/** property to hold mCardNumberRUSCOB. */
	private String mCardNumberRUSCOB;

	/** property to hold mCardNumberPRCOB. */
	private String mCardNumberPRCOB;

	/** property to hold mLevel2TaxNumber. */
	private String mLevel2TaxNumber;

	/** property to hold mServerTimeAdjustmentWithSynchrony. */
	private int mServerTimeAdjustmentWithSynchrony;

	/** property to hold mResetPasswordLinkExpiryTime. */
	private int mResetPasswordLinkExpiryTime;

	/** property to hold mEnableSearchDex. */
	private boolean mEnableSearchDex;

	/** property to hold inAuthURL. */
	private String mInAuthURL;

	/** property to hold inAuthKey. */
	private String mInAuthKey;

	/** Hold SoftDesDbaName. */
	private String mSoftDesCountryCode;

	/** Hold SoftDesDbaName. */
	private String mSoftDesMerchContactInfo;

	/** hold boolean property mFailedCreditCardVoidAmt. */
	private String mFailedCreditCardVoidAmt;

	/** hold boolean property mEnablePayPalVoidFailure. */
	private boolean mEnablePayPalVoidFailure;

	/** hold boolean property mFailedPayPalVoidAmt. */
	private String mFailedPayPalVoidAmt;

	/** hold boolean property mEnableGiftCardVoidFailure. */
	private boolean mEnableGiftCardVoidFailure;

	/** hold boolean property mFailedGiftCardVoidAmt. */
	private String mFailedGiftCardVoidAmt;

	/**
	 * Holds the OMS Order Details Service URL.
	 */
	private String mOmsOrderDetailServiceURL;

	/**
	 * Holds the OMS Order List Service URL.
	 */
	private String mOmsOrderListServiceURL;

	/**
	 * Holds the OMS Cancel Order Service URL.
	 */
	private String mOmsCancelOrderServiceURL;

	/**
	 * Holds the OMS Cancel Order Service URL.
	 */
	private String mOmsCustomerMasterServiceURL;

	/**
	 * Holds the OMS Customer Order Import Service URL.
	 */
	private String mOmsCustomerOrderImportServiceURL;

	/** holds address field properties *. */
	private List<String> mAddressFieldProperties;

	/**
	 * Holds nortonSealURL.
	 */
	private String mNortonSealURL;

	/**
	 * Holds mBruNortonSealURL.
	 */
	private String mBruNortonSealURL;

	/** Property to hold CountryCode *. */
	private Map<String,String> mSDCountryCode = new HashMap<String,String>();

	/** Property to hold TaxNumber *. */
	private Map<String,String> mSDTaxNumber = new HashMap<String,String>();

	/** Property to hold mPayPalChkoutFromOrderReviewSuccessUrl value. */
	private String mPayPalChkoutFromOrderReviewSuccessUrl;

	/** Property to hold mPayPalChkoutFromOrderReviewCancelUrl value. */
	private String mPayPalChkoutFromOrderReviewCancelUrl;

	/** Property to hold mPayPalReviewSuccessUrl value. */
	private String mPayPalReviewSuccessUrl;

	/** Property to hold mPayPalReviewErrorUrl value. */
	private String mPayPalReviewErrorUrl;

	/** Property to hold mCurrencyCode value. */
	private String mCurrencyCode;

	/** Property to hold  online only. */
	private String mOnlineOnly;

	/** Property to hold  in store only. */
	private String mInStoreOnly;

	/**  property to hold displayCreditCardTypeName. */
	public Map<String, String> mDisplayCreditCardTypeName = new HashMap<String,String>();

	/** property to hold mEnableSOS. */
	private boolean mEnableSOS;

	/**  property to hold mSourceOfOrderType. */
	private String mWebSourceOrderType;

	/**  property to hold mSosSourceOrderType. */
	private String mSosSourceOrderType;

	/**  property to hold mMobileSourceOrderType. */
	private String mMobileSourceOrderType;

	/**  property to hold mSsrSourceOrderType. */
	private String mCsrSourceOrderType;

	/** Holds mSosConfiguredURLs value. */
	private List<String> mSosConfiguredURLs;

	/** Switch for turn on/off pre sell item's EDD on review and confirmation page. */
	private boolean mEnablePreSellEDD;


	/** Holds the length of the billingNickName. */
	private int mBillingNickNameLength;


	/** Holds the length of the mPreSellDateFormat. */
	private String mPreSellDateFormat;

	/**
	 * This property hold reference for mAttrPropertyTypeProduct.
	 */
	private String mAttrPropertyTypeProduct;

	/**
	 * This property hold reference for mAttrPropertyTypeSku.
	 */
	private String mAttrPropertyTypeSku;
	/**
	 * This property hold reference for mAttrPropertyTypeDerivedSku.
	 */
	private String mAttrPropertyTypeDerivedSku;

	/**
	 * This property hold reference for mSosCookieNames.
	 */
	private List<String> mSosCookieNames;


	/**
	 * This property hold reference for MerchantGroupId.
	 */
	private String mMerchantGroupId;


	/**
	 * This property hold reference for powerReviews Site Id.
	 */
	private String mMerchantId;
	
	/**
	 * This property hold reference for synchrony card holder name.
	 */
	private String mDefaultSynchronyCardHolder;
	
	/**
	 * This property hold reference for visaURL.
	 */
	private String mVisaURL;
	
	/**
	 * This property hold reference for mastercardURL.
	 */
	private String mMastercardURL;
	
	
	/** This property days to purge single use coupon. */
	private int mDaysToPurgeSingleUseCoupon;
		
	
	/**This property hold reference for msosCookieredirectionURL. */
	private Map<String, String> mSosCookieredirectionURL;
	
	/**This property hold reference for defaultSOSStoreNumber. */
	private String mDefaultSOSStoreNumber;
	
	/**This property hold reference for mDefaultsosEmpNumber. */
	private String mDefaultsosEmpNumber;
	
	/** This property hold previewEnabled */
	private boolean mPreviewEnabled;
	/** This property hold previewPromoEnableInStaging */
	private boolean mPreviewPromoEnableInStaging;
	
	/** This property hold mRecaptchaTimeOut */
	private int mRecaptchaTimeOut;
	
	/** This property hold mRecaptchaRequestTimeout */
	private int mRecaptchaRequestTimeout;
	
	/** This property hold mRecaptchaProxyRequired */
	private boolean mRecaptchaProxyRequired;
	
	/** Holds storeLocatorGoogleMap URL value. */
	private String mStoreLocatorGoogleMapURL;
	
	/** Holds storeLocatorStaticGoogleMap URL value. */
	private String mStoreLocatorStaticGoogleMapURL;
	
	/** Holds mStoreLocatorGetDirectionsGoogleMap URL value. */
	private String mStoreLocatorGetDirectionsGoogleMapURL;
	
		/**
	 * Property to Hold CustomParamToSiteIdMapping.
	 */
	private Map<String, String> mCustomParamToSiteIdMapping;
	
	/** The Maximum item quantity. */
	private long mMaximumItemQuantity;
	/**
	 * Property to Hold mClientSecret.
	 */
	private String mClientSecret;
	/**
	 * Property to Hold mClientID.
	 */
	private String mClientID;
	
	/**
	 * Property to Hold CustomSlaveEnable.
	 */
	private boolean mCustomSlaveEnable;
	
	/**
	 * Property to Hold GoogleMapApiKey.
	 */
	private String mGoogleMapApiKey;
	
	/**
	 * Property to Hold enableCustomSiteRuleFilter.
	 */
	private boolean mEnableCustomSiteRuleFilter;
	
	/**
	 * property to hold mEnableGeoValidationfromClientId.
	 */
	private boolean mEnableGeoValidationfromClientId;
	
	/**
	 * property to hold mValidateAgainstProjectId.
	 */
	private boolean mValidateAgainstProjectId;
	
	/**
	 * Property to Hold CatalogFeedProjectName.
	 */
	private List<String> mCatalogFeedProjectNameList;
	
	
	/**
	 * Gets the enable custom site rule filter.
	 *
	 * @return the enable custom site rule filter
	 */
	public boolean isEnableCustomSiteRuleFilter() {
		return mEnableCustomSiteRuleFilter;
	}
	/**
	 * 
	 * @return the mIntegrationLoggingRestAPIURL
	 */
	public String getIntegrationLoggingRestAPIURL() {
		return mIntegrationLoggingRestAPIURL;
	}
	/**
	 * Sets the integrationLoggingRestAPIURL.
	 *
	 * @param pMIntegrationLoggingRestAPIURL String
	 */
	public void setIntegrationLoggingRestAPIURL(
			String pMIntegrationLoggingRestAPIURL) {
		mIntegrationLoggingRestAPIURL = pMIntegrationLoggingRestAPIURL;
	}

	/**
	 * Sets the enable custom site rule filter.
	 *
	 * @param pEnableCustomSiteRuleFilter the new enable custom site rule filter
	 */
	public void setEnableCustomSiteRuleFilter(boolean pEnableCustomSiteRuleFilter) {
		mEnableCustomSiteRuleFilter = pEnableCustomSiteRuleFilter;
	}

	/**
	 * 
	 * @return the mGoogleMapApiKey
	 */
	public String getGoogleMapApiKey() {
		return mGoogleMapApiKey;
	}

	/**
	 * @param pGoogleMapApiKey
	 *            the mGoogleMapApiKey to set
	 */
	public void setGoogleMapApiKey(String pGoogleMapApiKey) {
		this.mGoogleMapApiKey = pGoogleMapApiKey;
	}
	
	
	/**
	 * Gets the client secret.
	 *
	 * @return the client secret
	 */
	public String getClientSecret() {
		return mClientSecret;
	}

	/**
	 * Sets the client secret.
	 *
	 * @param pClientSecret the new client secret
	 */
	public void setClientSecret(String pClientSecret) {
		mClientSecret = pClientSecret;
	}

	/**
	 * Gets the client id.
	 *
	 * @return the client id
	 */
	public String getClientID() {
		return mClientID;
	}

	/**
	 * Sets the client id.
	 *
	 * @param pClientID the new client id
	 */
	public void setClientID(String pClientID) {
		mClientID = pClientID;
	}

	/**
	 * Gets the maximum item quantity.
	 *
	 * @return the maximum item quantity
	 */
	public long getMaximumItemQuantity() {
		return mMaximumItemQuantity;
	}

	/**
	 * Sets the maximum item quantity.
	 *
	 * @param pMaximumItemQuantity the new maximum item quantity
	 */
	public void setMaximumItemQuantity(long pMaximumItemQuantity) {
		mMaximumItemQuantity = pMaximumItemQuantity;
	}

	/**
	 * Returns the Custom Param To Site Id Mapping.
	 * 
	 * @return Map - CustomParamToSiteIdMapping
	 */
	public Map<String, String> getCustomParamToSiteIdMapping() {
		return mCustomParamToSiteIdMapping;
	}

	/**
	 * Sets the property for Custom Param To Site Id Mapping.
	 * 
	 * @param pCustomParamToSiteIdMapping
	 *            - Custom Param To Site Id Mapping
	 */
	public void setCustomParamToSiteIdMapping(Map<String, String> pCustomParamToSiteIdMapping) {
		mCustomParamToSiteIdMapping = pCustomParamToSiteIdMapping;
	}
	
	
	 /**
 	 * Gets the recaptcha time out.
 	 *
 	 * @return the recaptcha time out
 	 */
 	public int getRecaptchaTimeOut() {
		return mRecaptchaTimeOut;
	}

	/**
	 * Sets the recaptcha time out.
	 *
	 * @param pRecaptchaTimeOut the new recaptcha time out
	 */
	public void setRecaptchaTimeOut(int pRecaptchaTimeOut) {
		mRecaptchaTimeOut = pRecaptchaTimeOut;
	}

	/**
	 * Gets the recaptcha request timeout.
	 *
	 * @return the recaptcha request timeout
	 */
	public int getRecaptchaRequestTimeout() {
		return mRecaptchaRequestTimeout;
	}

	/**
	 * Sets the recaptcha request timeout.
	 *
	 * @param pRecaptchaRequestTimeout the new recaptcha request timeout
	 */
	public void setRecaptchaRequestTimeout(int pRecaptchaRequestTimeout) {
		mRecaptchaRequestTimeout = pRecaptchaRequestTimeout;
	}

	/**
	 * Checks if is recaptcha proxy required.
	 *
	 * @return true, if is recaptcha proxy required
	 */
	public boolean isRecaptchaProxyRequired() {
		return mRecaptchaProxyRequired;
	}

	/**
	 * Sets the recaptcha proxy required.
	 *
	 * @param pRecaptchaProxyRequired the new recaptcha proxy required
	 */
	public void setRecaptchaProxyRequired(boolean pRecaptchaProxyRequired) {
		mRecaptchaProxyRequired = pRecaptchaProxyRequired;
	}

	/**
	 * @return the previewEnabled
	 */	
	 public boolean isPreviewEnabled() {
		return mPreviewEnabled;
	}
	 
	/**
	 * @param pPreviewEnabled the previewEnabled to set
	 */

	public void setPreviewEnabled(boolean pPreviewEnabled) {
		mPreviewEnabled = pPreviewEnabled;
	}
	/**
 	 * Gets the visa url.
 	 *
 	 * @return the visaURL
 	 */
	public String getVisaURL() {
		return mVisaURL;
	}

	/**
	 * Sets the visa url.
	 *
	 * @param pVisaURL the visaURL to set
	 */
	public void setVisaURL(String pVisaURL) {
		mVisaURL = pVisaURL;
	}

	/**
	 * Gets the mastercard url.
	 *
	 * @return the mastercardURL
	 */
	public String getMastercardURL() {
		return mMastercardURL;
	}

	/**
	 * Sets the mastercard url.
	 *
	 * @param pMastercardURL the mastercardURL to set
	 */
	public void setMastercardURL(String pMastercardURL) {
		mMastercardURL = pMastercardURL;
	}

	/**
	 * Gets the attr property type derived sku.
	 *
	 * @return the attrPropertyTypeDerivedSku
	 */
	public String getAttrPropertyTypeDerivedSku() {
		return mAttrPropertyTypeDerivedSku;
	}

	/**
	 * Sets the attr property type derived sku.
	 *
	 * @param pAttrPropertyTypeDerivedSku the attrPropertyTypeDerivedSku to set
	 */
	public void setAttrPropertyTypeDerivedSku(String pAttrPropertyTypeDerivedSku) {
		mAttrPropertyTypeDerivedSku = pAttrPropertyTypeDerivedSku;
	}

	/**
	 * Gets the address field properties.
	 *
	 * @return the addressFieldProperties.
	 */
	public List<String> getAddressFieldProperties() {
		return mAddressFieldProperties;
	}

	/**
	 * Gets the akamai url.
	 *
	 * @return the akamaiUrl.
	 */
	public String getAkamaiUrl() {
		return mAkamaiUrl;
	}

	/**
	 * Gets the bru norton seal url.
	 *
	 * @return the mBruNortonSealURL String.
	 */

	public String getBruNortonSealURL() {
		return mBruNortonSealURL;
	}

	/**
	 * Returns the cardCVV.
	 *
	 * @return the cardCVV.
	 */

	public String getCardCVV() {
		return mCardCVV;
	}


	/**
	 * Gets the card number plcc.
	 *
	 * @return the cardNumberPLCC.
	 */
	public String getCardNumberPLCC() {
		return mCardNumberPLCC;
	}

	/**
	 * Gets the card number prcob.
	 *
	 * @return the cardNumberPRCOB.
	 */
	public String getCardNumberPRCOB() {
		return mCardNumberPRCOB;
	}

	/**
	 * Gets the card number ruscob.
	 *
	 * @return the cardNumberRUSCOB.
	 */
	public String getCardNumberRUSCOB() {
		return mCardNumberRUSCOB;
	}

	/**
	 * Gets the cart checkout with pay pal error url.
	 *
	 * @return the mCartCheckoutWithPayPalErrorURL.
	 */
	public String getCartCheckoutWithPayPalErrorURL() {
		return mCartCheckoutWithPayPalErrorURL;
	}

	/**
	 * Gets the cart checkout with pay pal success url.
	 *
	 * @return the mCartCheckoutWithPayPalSuccessURL.
	 */
	public String getCartCheckoutWithPayPalSuccessURL() {
		return mCartCheckoutWithPayPalSuccessURL;
	}

	/**
	 * Gets the channel availability.
	 *
	 * @return the channel availability.
	 */
	public String getChannelAvailabilityForBoth() {
		return mChannelAvailabilityForBoth;
	}

	/**
	 * Gets the channel type map.
	 *
	 * @return the channelTypeMap.
	 */
	public Map<String, String> getChannelTypeMap() {
		return mChannelTypeMap;
	}

	/**
	 * Gets the charity donation redirect url.
	 *
	 * @return the charity donation redirect url.
	 */
	public String getCharityDonationRedirectURL() {
		return mCharityDonationRedirectURL;
	}

	/**
	 * Gets the checkout tab numbers.
	 *
	 * @return the checkoutTabNumbers.
	 */
	public Map<String, String> getCheckoutTabNumbers() {
		return mCheckoutTabNumbers;
	}

	/**
	 * Gets the checkout tab urls.
	 *
	 * @return the checkoutTabUrls.
	 */
	public Map<String, String> getCheckoutTabUrls() {
		Map<String, String> checkoutTabs = new LinkedHashMap<String, String>();
		checkoutTabs.put(TRUCheckoutConstants.SHIPPING_TAB, mCheckoutTabUrls.get(TRUCheckoutConstants.SHIPPING_TAB));
		checkoutTabs.put(TRUCheckoutConstants.GIFTING_TAB, mCheckoutTabUrls.get(TRUCheckoutConstants.GIFTING_TAB));
		checkoutTabs.put(TRUCheckoutConstants.PICKUP_TAB, mCheckoutTabUrls.get(TRUCheckoutConstants.PICKUP_TAB));
		checkoutTabs.put(TRUCheckoutConstants.PAYMENT_TAB, mCheckoutTabUrls.get(TRUCheckoutConstants.PAYMENT_TAB));
		checkoutTabs.put(TRUCheckoutConstants.REVIEW_TAB, mCheckoutTabUrls.get(TRUCheckoutConstants.REVIEW_TAB));
		return checkoutTabs;
	}

	/**
	 * Gets the checkout with pay pal error url.
	 *
	 * @return the mCheckoutWithPayPalErrorURL.
	 */
	public String getCheckoutWithPayPalErrorURL() {
		return mCheckoutWithPayPalErrorURL;
	}

	/**
	 * Gets the checkout with pay pal success url.
	 *
	 * @return the mCheckoutWithPayPalSuccessURL.
	 */
	public String getCheckoutWithPayPalSuccessURL() {
		return mCheckoutWithPayPalSuccessURL;
	}

	/**
	 * Gets the color sequence map.
	 *
	 * @return the colorSequenceMap.
	 */
	public Map<String, String> getColorSequenceMap() {
		return mColorSequenceMap;
	}

	/* @return the copyShipItemRelProperties
	 */
	/**
	 * Gets the copy ship item rel properties.
	 *
	 * @return the copy ship item rel properties.
	 */
	public List<String> getCopyShipItemRelProperties() {
		return mCopyShipItemRelProperties;
	}

	/**
	 * Gets the credit card type names for integration.
	 *
	 * @return the creditCardTypeNamesForIntegration.
	 */
	public Map<String, String> getCreditCardTypeNamesForIntegration() {
		return mCreditCardTypeNamesForIntegration;
	}

	/**
	 * Gets the csr source order type.
	 *
	 * @return the csrSourceOrderType.
	 */
	public String getCsrSourceOrderType() {
		return mCsrSourceOrderType;
	}

	/**
	 * Gets the currency code.
	 *
	 * @return the currencyCode.
	 */
	public String getCurrencyCode() {
		return mCurrencyCode;
	}

	/**
	 * Gets the currency codes string.
	 *
	 * @return the currencyCodesString.
	 */
	public Map<String, String> getCurrencyCodesString() {
		return mCurrencyCodesString;
	}

	/**
	 * Gets the disable rest service url.
	 *
	 * @return the disableRestServiceURL.
	 */
	public String getDisableRestServiceURL() {
		return mDisableRestServiceURL;
	}

	/**
	 * Gets the display credit card type name.
	 *
	 * @return the displayCreditCardTypeName.
	 */
	public Map<String, String> getDisplayCreditCardTypeName() {
		return mDisplayCreditCardTypeName;
	}

	/**
	 * Gets the display no of cards.
	 * @return the mDisplayNoOfCards.
	 */
	public String getDisplayNoOfCards() {
		return mDisplayNoOfCards;
	}

	/**
	 * Gets the display pay pal payment type name.
	 *
	 * @return the displayPayPalPaymentTypeName.
	 */
	public Map<String, String> getDisplayPayPalPaymentTypeName() {
		return mDisplayPayPalPaymentTypeName;
	}

	/** Gets the distance for near by store search.
	 *
	 * @return the distance for near by store search.
	 */
	public double getDistanceForNearByStoreSearch() {
		return mDistanceForNearByStoreSearch;
	}

	/**
	 * Gets the domain url.
	 *
	 * @return the domainUrl.
	 */
	public String getDomainUrl() {
		return mDomainUrl;
	}

	/**
	 * Gets the edd cut off time.
	 *
	 * @return the eddCutOffTime.
	 */
	public int getEddCutOffTime() {
		return mEddCutOffTime;
	}

	/**
	 * Gets the edits the pref url.
	 * @return the editPrefURL.
	 */
	public String getEditPrefURL() {
		return mEditPrefURL;
	}

	/**
	 * Returns the englishLocale.
	 * @return the englishLocale.
	 */
	public String getEnglishLocale() {
		return mEnglishLocale;
	}

	/**
	 * Gets the epsilon pixel url.
	 *
	 * @return epsilonPixelURL.
	 */
	public String getEpsilonPixelURL() {
		return mEpsilonPixelURL;
	}

	/**
	 * Gets the exp month adjustment for plcc.
	 *
	 * @return the mExpMonthAdjustmentForPLCC - expMonthAdjustmentForPLCC.
	 */
	public String getExpMonthAdjustmentForPLCC() {
		return mExpMonthAdjustmentForPLCC;
	}

	/**
	 * Gets the exp year adjustment for plcc.
	 *
	 * @return the mExpYearAdjustmentForPLCC - expYearAdjustmentForPLCC.
	 */
	public String getExpYearAdjustmentForPLCC() {
		return mExpYearAdjustmentForPLCC;
	}

	/**
	 * Gets the failed credit card amt.
	 *
	 * @return the failedCreditCardAmt.
	 */
	public String getFailedCreditCardAmt() {
		return mFailedCreditCardAmt;
	}

	/**
	 * Gets the failed credit card void amt.
	 *
	 * @return the failedCreditCardVoidAmt.
	 */
	public String getFailedCreditCardVoidAmt() {
		return mFailedCreditCardVoidAmt;
	}

	/**
	 * Gets the failed gift card amt.
	 *
	 * @return the failedGiftCardAmt.
	 */
	public String getFailedGiftCardAmt() {
		return mFailedGiftCardAmt;
	}

	/**
	 * Gets the failed gift card refund amt.
	 *
	 * @return the failedGiftCardRefundAmt.
	 */
	public String getFailedGiftCardRefundAmt() {
		return mFailedGiftCardRefundAmt;
	}

	/**
	 * Gets the failed gift card void amt.
	 *
	 * @return the failedGiftCardVoidAmt.
	 */
	public String getFailedGiftCardVoidAmt() {
		return mFailedGiftCardVoidAmt;
	}

	/**
	 * Gets the failed pay pal amt.
	 *
	 * @return the failedPayPalAmt.
	 */
	public String getFailedPayPalAmt() {
		return mFailedPayPalAmt;
	}

	/**
	 * Gets the failed pay pal void amt.
	 *
	 * @return the failedPayPalVoidAmt.
	 */
	public String getFailedPayPalVoidAmt() {
		return mFailedPayPalVoidAmt;
	}

	/**
	 * Returns the fraudAcceptEmail.
	 *
	 * @return the fraudAcceptEmail.
	 */

	public String getFraudAcceptEmail() {
		return mFraudAcceptEmail;
	}

	/**
	 * Gets the gift card number length.
	 *
	 * @return the giftCardNumberLength.
	 */
	public int getGiftCardNumberLength() {
		return mGiftCardNumberLength;
	}

	/**
	 * Gets the gift card pin length.
	 *
	 * @return the giftCardPinLength.
	 */
	public int getGiftCardPinLength() {
		return mGiftCardPinLength;
	}

	/**
	 * Gets the gift options page url.
	 *
	 * @return the giftOptionsPageURL.
	 */
	public String getGiftOptionsPageURL() {
		return mGiftOptionsPageURL;
	}

	/**
	 * Gets the http.
	 * @return the mHTTP.
	 */
	public String getHTTP() {
		return mHTTP;
	}
	/**
	 * Gets the image server folder path.
	 *
	 * @return the imageServerFolderPath.
	 */
	public String getImageServerFolderPath() {
		return mImageServerFolderPath;
	}


	/**
	 * Gets the image server url.
	 *
	 * @return the imageServerURL..
	 */
	public String getImageServerURL() {
		return mImageServerURL;
	}

	/**
	 * Gets the in auth key.
	 *
	 * @return the inAuthKey..
	 */
	public String getInAuthKey() {
		return mInAuthKey;
	}

	/**
	 * Gets the in auth url.
	 *
	 * @return the inAuthURL.
	 */
	public String getInAuthURL() {
		return mInAuthURL;
	}

	/**
	 * Gets the in store only.
	 *
	 * @return the in store only.
	 */
	public String getInStoreOnly() {
		return mInStoreOnly;
	}

	/**
	 * Gets the level2 tax number.
	 *
	 * @return the level2TaxNumber.
	 */
	public String getLevel2TaxNumber() {
		return mLevel2TaxNumber;
	}

	/**
	 * Gets the lightbox configurable time.
	 * @return the mLightboxConfigurableTime.
	 */
	public String getLightboxConfigurableTime() {
		return mLightboxConfigurableTime;
	}

	/**
	 * Gets the lightboxdonotshow configurable time.
	 * @return the lightboxdonotshow configurable time.
	 */
	public String getLightboxdonotshowConfigurableTime() {
		return mLightboxdonotshowConfigurableTime;
	}

	/**
	 * Gets the lightboxsubmit configurable time.
	 * @return the lightboxsubmit configurable time.
	 */
	public String getLightboxsubmitConfigurableTime() {
		return mLightboxsubmitConfigurableTime;
	}

	/**
	 * Gets the limit ofdays to tell a product new.
	 *
	 * @return the limitOfdaysToTellAProductNew.
	 */
	public int getLimitOfdaysToTellAProductNew() {
		return mLimitOfdaysToTellAProductNew;
	}

	/**
	 * Gets the location id for ship to home.
	 * @return the location id for ship to home.
	 */
	public String getLocationIdForShipToHome() {
		return mLocationIdForShipToHome;
	}

	/**
	 * Gets the max gift cards.
	 *
	 * @return the maxGiftCards.
	 */
	public int getMaxGiftCards() {
		return mMaxGiftCards;
	}

	/**
	 * Gets the mobile source order type.
	 *
	 * @return the mobileSourceOrderType.
	 */
	public String getMobileSourceOrderType() {
		return mMobileSourceOrderType;
	}
	/**
	 * Gets the myAccountRegistryUrl.
	 * @return the mMyAccountRegistryUrl.
	 */
	public String getMyAccountRegistryUrl() {
		return mMyAccountRegistryUrl;
	}

	/**
	 * Gets the myAccountWishlistUrl.
	 * @return the mMyAccountWishlistUrl.
	 */
	public String getMyAccountWishlistUrl() {
		return mMyAccountWishlistUrl;
	}

	/**
	 * Gets the norton seal url.
	 *
	 * @return the mNortonSealURL String.
	 */

	public String getNortonSealURL() {
		return mNortonSealURL;
	}

	/**
	 * This method is used to get olsonLearnMoreURL.
	 * @return olsonLearnMoreURL String.
	 */
	public String getOlsonLearnMoreURL() {
		return mOlsonLearnMoreURL;
	}

	/**
	 * Gets the olson look up url.
	 * @return the olsonLookUpURL.
	 */
	public String getOlsonLookUpURL() {
		return mOlsonLookUpURL;
	}

	/**
	 * Gets the olson sign up url.
	 * @return the olsonSignUpURL.
	 */
	public String getOlsonSignUpURL() {
		return mOlsonSignUpURL;
	}

	/**
	 * Gets the oms cancel order rest apiurl.
	 *
	 * @return the omsCancelOrderRestAPIURL.
	 */
	public String getOmsCancelOrderRestAPIURL() {
		return mOmsCancelOrderRestAPIURL;
	}

	/**
	 * Gets the oms cancel order service url.
	 *
	 * @return the mOmsCancelOrderServiceURL.
	 */
	public String getOmsCancelOrderServiceURL() {
		return mOmsCancelOrderServiceURL;
	}

	/**
	 * Gets the oms customer master service url.
	 *
	 * @return the mOmsCustomerMasterServiceURL.
	 */

	public String getOmsCustomerMasterServiceURL() {
		return mOmsCustomerMasterServiceURL;
	}

	/**
	 * Gets the oms customer order import service url.
	 *
	 * @return the mOmsCustomerOrderImportServiceURL.
	 */

	public String getOmsCustomerOrderImportServiceURL() {
		return mOmsCustomerOrderImportServiceURL;
	}
	/**
	 * Gets the oms order detail rest apiurl.
	 *
	 * @return the omsOrderDetailRestAPIURL.
	 */
	public String getOmsOrderDetailRestAPIURL() {
		return mOmsOrderDetailRestAPIURL;
	}

	/**
	 * Gets the oms order detail service url.
	 *
	 * @return the mOmsOrderDetailServiceURL.
	 */
	public String getOmsOrderDetailServiceURL() {
		return mOmsOrderDetailServiceURL;
	}

	/**
	 * Gets the oms order list rest apiurl.
	 *
	 * @return the omsOrderListRestAPIURL.
	 */
	public String getOmsOrderListRestAPIURL() {
		return mOmsOrderListRestAPIURL;
	}

	/**
	 * Gets the oms order list service url.
	 *
	 * @return the mOmsOrderListServiceURL.
	 */
	public String getOmsOrderListServiceURL() {
		return mOmsOrderListServiceURL;
	}

	/**
	 * Gets the online only.
	 *
	 * @return the online only.
	 */
	public String getOnlineOnly() {
		return mOnlineOnly;
	}

	/**
	 * Gets the order repricing option.
	 * @return the orderRepricingOption.
	 */
	public String getOrderRepricingOption() {
		return mOrderRepricingOption;
	}

	/**
	 * Gets the payment page url.
	 *
	 * @return the paymentPageURL.
	 */
	public String getPaymentPageURL() {
		return mPaymentPageURL;
	}

	/**
	 * Gets the pay pal chkout cancel url.
	 *
	 * @return payPalChkoutCancelUrl.
	 */
	public String getPayPalChkoutCancelUrl() {
		return mPayPalChkoutCancelUrl;
	}

	/**
	 * Gets the pay pal chkout from order review cancel url.
	 *
	 * @return the mPayPalChkoutFromOrderReviewCancelUrl.
	 */
	public String getPayPalChkoutFromOrderReviewCancelUrl() {
		return mPayPalChkoutFromOrderReviewCancelUrl;
	}

	/**
	 * Gets the pay pal chkout from order review success url.
	 *
	 * @return the mPayPalChkoutFromOrderReviewSuccessUrl.
	 */
	public String getPayPalChkoutFromOrderReviewSuccessUrl() {
		return mPayPalChkoutFromOrderReviewSuccessUrl;
	}

	/**
	 * Gets the pay pal chkout success url.
	 *
	 * @return payPalChkoutSuccessUrl.
	 */
	public String getPayPalChkoutSuccessUrl() {
		return mPayPalChkoutSuccessUrl;
	}

	/**
	 * Gets the pay pal exp chkout cancel url.
	 *
	 * @return the payPalExpChkoutCancelUrl.
	 */
	public String getPayPalExpChkoutCancelUrl() {
		return mPayPalExpChkoutCancelUrl;
	}

	/**
	 * Gets the pay pal exp chkout success url.
	 *
	 * @return the payPalExpChkoutSuccessUrl.
	 */
	public String getPayPalExpChkoutSuccessUrl() {
		return mPayPalExpChkoutSuccessUrl;
	}

	/**
	 * Gets the pay pal review error url.
	 *
	 * @return the mPayPalReviewErrorUrl.
	 */
	public String getPayPalReviewErrorUrl() {
		return mPayPalReviewErrorUrl;
	}

	/**
	 * Gets the pay pal review success url.
	 *
	 * @return the mPayPalReviewSuccessUrl.
	 */
	public String getPayPalReviewSuccessUrl() {
		return mPayPalReviewSuccessUrl;
	}

	/**
	 * Gets the pay pal token expiry time.
	 *
	 * @return the payPalTokenExpiryTime.
	 */
	public String getPayPalTokenExpiryTime() {
		return mPayPalTokenExpiryTime;
	}

	/**
	 * Gets the pickup page url.
	 *
	 * @return pickup page url.
	 */
	public String getPickupPageURL() {
		return mPickupPageURL;
	}

	/**
	 * Gets the power reviews app launch url.
	 *
	 * @return the mPowerReviewsURL.
	 */
	public String getPowerReviewsApiKey() {
		return mPowerReviewsApiKey;

	/**
	 * Gets the power reviews full url.
	 *
	 * @return the mPowerReviewsFullURL.
	 */
	}

	/**
	 * Gets the power reviews url.
	 *
	 * @return the mPowerReviewsURL.
	 */
	public String getPowerReviewsURL() {
		return mPowerReviewsURL;
	}

	/**
	 * Returns the priceOnCartValue.
	 * @return the priceOnCartValue.
	 */
	public String getPriceOnCartValue() {
		return mPriceOnCartValue;
	}

	/**
	 * Returns the promotionCountinPDP.
	 *
	 * @return the promotionCountinPDP.
	 */
	public String getPromotionCountinPDP() {
		return mPromotionCountinPDP;
	}

	/**
	 * Gets the recaptcha no script url.
	 *
	 * @return the recaptchaNoScriptURL.
	 */
	public String getRecaptchaNoScriptURL() {
		return mRecaptchaNoScriptURL;
	}

	/**
	 * Gets the recaptcha private key.
	 *
	 * @return the recaptchaPrivateKey.
	 */
	public String getRecaptchaPrivateKey() {
		return mRecaptchaPrivateKey;
	}
	
	/**
	 * Gets the recaptcha secret key.
	 *
	 * @return the recaptchaSecretKey.
	 */
	public String getRecaptchaSecretKey() {
		return mRecaptchaSecretKey;
	}

	/**
	 * Gets the recaptcha public key.
	 *
	 * @return the recaptchaPublicKey.
	 */
	public String getRecaptchaPublicKey() {
		return mRecaptchaPublicKey;
	}

	/**
	 * Gets the recaptcha script url.
	 *
	 * @return the recaptchaScriptURL.
	 */
	public String getRecaptchaScriptURL() {
		return mRecaptchaScriptURL;
	}

	/**
	 * Gets the registry url.
	 * @return the registry url.
	 */
	public String getRegistryUrl() {
		return mRegistryUrl;
	}

	/**
	 * Gets the reset password link expiry time.
	 *
	 * @return the mResetPasswordLinkExpiryTime - resetPasswordLinkExpiryTime.
	 */
	public int getResetPasswordLinkExpiryTime() {
		return mResetPasswordLinkExpiryTime;
	}

	/**
	 * Gets the sD city.
	 *
	 * @return the sDCity.
	 */
	public Map<String, String> getSDCity() {
		return mSDCity;
	}

	/**
	 * Gets the sD country code.
	 *
	 * @return the sDCountryCode.
	 */
	public Map<String, String> getSDCountryCode() {
		return mSDCountryCode;
	}

	/**
	 * Gets the sDDBA name.
	 *
	 * @return the sDDBAName.
	 */
	public Map<String, String> getSDDBAName() {
		return mSDDBAName;
	}

	/**
	 * Gets the sD mcc.
	 *
	 * @return the sDMcc.
	 */
	public Map<String, String> getSDMcc() {
		return mSDMcc;
	}

	/**
	 * Gets the sD merchange contact info.
	 *
	 * @return the sDMerchangeContactEmail.
	 */
	public Map<String, String> getSDMerchangeContactInfo() {
		return mSDMerchangeContactInfo;
	}

	/**
	 * Gets the sD mid.
	 *
	 * @return the sDMid.
	 */
	public Map<String, String> getSDMid() {
		return mSDMid;
	}

	/**
	 * Gets the sD postal code.
	 *
	 * @return the sDPostalCode.
	 */
	public Map<String, String> getSDPostalCode() {
		return mSDPostalCode;
	}

	/**
	 * Gets the sD region.
	 *
	 * @return the sDRegion.
	 */
	public Map<String, String> getSDRegion() {
		return mSDRegion;
	}

	/**
	 * Gets the sD street.
	 *
	 * @return the sDStreet.
	 */
	public Map<String, String> getSDStreet() {
		return mSDStreet;
	}

	/**
	 * Gets the sD tax number.
	 *
	 * @return the sDTaxNumber.
	 */
	public Map<String, String> getSDTaxNumber() {
		return mSDTaxNumber;
	}
	/**
	 * Gets the search dex footer page.
	 *
	 * @return the searchDexFooterPage.
	 */
	public String getSearchDexFooterPage() {
		return mSearchDexFooterPage;
	}

	/**
	 * Gets the search dex header page.
	 *
	 * @return the searchDexHeaderPage.
	 */
	public String getSearchDexHeaderPage() {
		return mSearchDexHeaderPage;
	}

	/**
	 * Gets the search dex url.
	 *
	 * @return the searchDexUrl.
	 */
	public String getSearchDexUrl() {
		return mSearchDexUrl;
	}

	/**
	 * Gets the server time adjustment with synchrony.
	 *
	 * @return the mServerTimeAdjustmentWithSynchrony - serverTimeAdjustmentWithSynchrony.
	 */
	public int getServerTimeAdjustmentWithSynchrony() {
		return mServerTimeAdjustmentWithSynchrony;
	}

	/**
	 * Gets the shipping page url.
	 *
	 * @return the shippingPageURL.
	 */
	public String getShippingPageURL() {
		return mShippingPageURL;
	}

	/**
	 * Gets the shop local url.
	 *
	 * @return the shopLocalURL.
	 */
	public String getShopLocalURL() {
		return mShopLocalURL;
	}

	/**
	 * Gets the shopping cart parameter.
	 *
	 * @return the shopping cart parameter.
	 */
	public String getShoppingCartParameter() {
		return mShoppingCartParameter;
	}

	/**
	 * Gets the six month financing limit.
	 *
	 * @return the sixMonthFinancingLimit.
	 */
	public double getSixMonthFinancingLimit() {
		return mSixMonthFinancingLimit;
	}

	/**
	 * Gets the size sequence map.
	 *
	 * @return the sizeSequenceMap.
	 */
	public Map<String, String> getSizeSequenceMap() {
		return mSizeSequenceMap;
	}

	/**
	 * Gets the soft des city.
	 *
	 * @return the softDesCity.
	 */
	public String getSoftDesCity() {
		return mSoftDesCity;
	}

	/**
	 * Gets the soft des country code.
	 *
	 * @return the softDesCountryCode.
	 */
	public String getSoftDesCountryCode() {
		return mSoftDesCountryCode;
	}

	/**
	 * Gets the soft des dba name.
	 *
	 * @return the softDesDbaName.
	 */
	public String getSoftDesDbaName() {
		return mSoftDesDbaName;
	}


	/**
	 * Gets the soft des mcc.
	 *
	 * @return the softDesMcc.
	 */
	public String getSoftDesMcc() {
		return mSoftDesMcc;
	}

	/**
	 * Gets the soft des merch contact info.
	 *
	 * @return the softDesMerchContactInfo.
	 */
	public String getSoftDesMerchContactInfo() {
		return mSoftDesMerchContactInfo;
	}

	/**
	 * Gets the soft des mid.
	 *
	 * @return the softDesMid.
	 */
	public String getSoftDesMid() {
		return mSoftDesMid;
	}

	/**
	 * Gets the soft des postal code.
	 *
	 * @return the softDesPostalCode.
	 */
	public String getSoftDesPostalCode() {
		return mSoftDesPostalCode;
	}

	/**
	 * Gets the soft des region.
	 *
	 * @return the softDesRegion.
	 */
	public String getSoftDesRegion() {
		return mSoftDesRegion;
	}

	/**
	 * Gets the soft des street.
	 *
	 * @return the softDesStreet.
	 */
	public String getSoftDesStreet() {
		return mSoftDesStreet;
	}

	/**
	 * Gets the sos configured ur ls.
	 *
	 * @return the mSosConfiguredURLs - sosConfiguredURLs.
	 */
	public List<String> getSosConfiguredURLs() {
		return mSosConfiguredURLs;
	}

	/**
	 * Gets the sos source order type.
	 *
	 * @return the sosSourceOrderType.
	 */
	public String getSosSourceOrderType() {
		return mSosSourceOrderType;
	}


	/**
	 * Returns the storeNumber.
	 * @return the storeNumber.
	 */
	public String getStoreNumber() {
		return mStoreNumber;
	}

	/**
	 * Gets the tax number.
	 *
	 * @return the taxNumber.
	 */
	public Map<String, String> getTaxNumber() {
		return mTaxNumber;
	}

	/**
	 * Gets the time in transit ship method codes.
	 *
	 * @return the timeInTransitShipMethodCodes.
	 */
	public List<String> getTimeInTransitShipMethodCodes() {
		return mTimeInTransitShipMethodCodes;
	}

	/**
	 * Gets the token type.
	 *
	 * @return the tokenType.
	 */
	public Map<String, String> getTokenType() {
		return mTokenType;
	}


	/**
	 * Gets the tRUCSS path.
	 *
	 * @return the tRUCSS path.
	 */
	public String getTRUCSSPath() {
		return mTRUCSSPath;
	}

	/**
	 * Gets the tRU image path.
	 *
	 * @return the tRU image path.
	 */
	public String getTRUImagePath() {
		return mTRUImagePath;
	}

	/**
	 * Gets the tRUJS path.
	 *
	 * @return the tRUJS path.
	 */
	public String getTRUJSPath() {
		return mTRUJSPath;
	}

	/**
	 * Gets the tru sku types.
	 * @return the truSkuTypes.
	 */
	public List<String> getTruSkuTypes() {
		return mTruSkuTypes;
	}

	/**
	 * Gets the tru video game esrb.
	 *
	 * @return the mTruVideoGameEsrb.
	 */
	public List<String> getTruVideoGameEsrb() {
		return mTruVideoGameEsrb;
	}

	/**
	 * Gets the twelve month financing limit.
	 *
	 * @return the twelveMonthFinancingLimit.
	 */
	public double getTwelveMonthFinancingLimit() {
		return mTwelveMonthFinancingLimit;
	}

	/**
	 * Gets the web source order type.
	 *
	 * @return the webSourceOrderType.
	 */
	public String getWebSourceOrderType() {
		return mWebSourceOrderType;
	}

	/**
	 * Gets the wishlist url.
	 * @return the wishlist url.
	 */
	public String getWishlistUrl() {
		return mWishlistUrl;
	}
	/**
	 * Gets the registry search url.
	 *
	 * @return the registry search url
	 */
	public String getRegistrySearchUrl() {
		return mRegistrySearchUrl;
	}

	/**
	 * Sets the registry search url.
	 *
	 * @param pRegistrySearchUrl the new registry search url
	 */
	public void setRegistrySearchUrl(String pRegistrySearchUrl) {
		mRegistrySearchUrl = pRegistrySearchUrl;
	}

	/**
	 * Gets the wishlist search url.
	 *
	 * @return the wishlist search url
	 */
	public String getWishlistSearchUrl() {
		return mWishlistSearchUrl;
	}

	/**
	 * Sets the wishlist search url.
	 *
	 * @param pWishlistSearchUrl the new wishlist search url
	 */
	public void setWishlistSearchUrl(String pWishlistSearchUrl) {
		mWishlistSearchUrl = pWishlistSearchUrl;
	}

	/**
	 * Gets the sos registry url.
	 *
	 * @return the sos registry url
	 */
	public String getSosRegistryUrl() {
		return mSosRegistryUrl;
	}

	/**
	 * Sets the sos registry url.
	 *
	 * @param pSosRegistryUrl the new sos registry url
	 */
	public void setSosRegistryUrl(String pSosRegistryUrl) {
		mSosRegistryUrl = pSosRegistryUrl;
	}

	/**
	 * Gets the sos wishlist url.
	 *
	 * @return the sos wishlist url
	 */
	public String getSosWishlistUrl() {
		return mSosWishlistUrl;
	}

	/**
	 * Sets the sos wishlist url.
	 *
	 * @param pSosWishlistUrl the new sos wishlist url
	 */
	public void setSosWishlistUrl(String pSosWishlistUrl) {
		mSosWishlistUrl = pSosWishlistUrl;
	}

	/**
	 * Checks if is check inventory online.
	 *
	 * @return true, if is check inventory online.
	 */
	public boolean isCheckInventoryOnline() {
		return mCheckInventoryOnline;
	}

	/**
	 * Returns the csrEmailEnabled.
	 *
	 * @return the csrEmailEnabled.
	 */

	public boolean isCsrEmailEnabled() {
		return mCsrEmailEnabled;
	}

	/**
	 * Checks if is edd enabled.
	 *
	 * @return the eddEnabled.
	 */
	public boolean isEddEnabled() {
		return mEddEnabled;
	}

	/**
	 * Checks if is enable address doctor.
	 * @return the enableAddressDoctor.
	 */
	public boolean isEnableAddressDoctor() {
		return mEnableAddressDoctor;
	}

	/**
	 * Checks if is enable coupon service.
	 *
	 * @return the enableCouponService.
	 */
	public boolean isEnableCouponService() {
		return mEnableCouponService;
	}

	/**
	 * Checks if is enable credit card full auth failure.
	 *
	 * @return the enableCreditCardFullAuthFailure.
	 */
	public boolean isEnableCreditCardFullAuthFailure() {
		return mEnableCreditCardFullAuthFailure;
	}

	/**
	 * Checks if is enable credit card void failure.
	 *
	 * @return the enableCreditCardVoidFailure.
	 */
	public boolean isEnableCreditCardVoidFailure() {
		return mEnableCreditCardVoidFailure;
	}

	/**
	 * Checks if is enable epslon.
	 * @return the enableEpslon.
	 */
	public boolean isEnableEpslon() {
		return mEnableEpslon;
	}

	/**
	 * Checks if is enable gift card full auth failure.
	 *
	 * @return the enableGiftCardFullAuthFailure.
	 */
	public boolean isEnableGiftCardFullAuthFailure() {
		return mEnableGiftCardFullAuthFailure;
	}

	/**
	 * Checks if is enable gift card refund failure.
	 *
	 * @return the enableGiftCardRefundFailure.
	 */
	public boolean isEnableGiftCardRefundFailure() {
		return mEnableGiftCardRefundFailure;
	}

	/**
	 * Checks if is enable gift card void failure.
	 *
	 * @return the enableGiftCardVoidFailure.
	 */
	public boolean isEnableGiftCardVoidFailure() {
		return mEnableGiftCardVoidFailure;
	}

	/**
	 * Checks if is enable inline seo.
	 *
	 * @return the mEnableInlineSeo.
	 */
	public boolean isEnableInlineSeo() {
		return mEnableInlineSeo;
	}

	/**
	 * Checks if is enable pay pal full auth failure.
	 *
	 * @return the enablePayPalFullAuthFailure.
	 */
	public boolean isEnablePayPalFullAuthFailure() {
		return mEnablePayPalFullAuthFailure;
	}

	/**
	 * Checks if is enable pay pal void failure.
	 *
	 * @return the enablePayPalVoidFailure.
	 */
	public boolean isEnablePayPalVoidFailure() {
		return mEnablePayPalVoidFailure;
	}

	/**
	 * Checks if is enable pre sell edd.
	 *
	 * @return EnablePreSellEDD
	 */
	public boolean isEnablePreSellEDD() {
		return mEnablePreSellEDD;
	}

	/**
	 * Gets the enableSearchDex flag value.
	 *
	 * @return the mEnableSearchDex - enableSearchDex.
	 */
	public boolean isEnableSearchDex() {
		return mEnableSearchDex;
	}

	/**
	 * Checks if is enable sos.
	 *
	 * @return mEnableSOS - mEnableSOS.
	 */
	public boolean isEnableSOS() {
		return mEnableSOS;
	}

	/**
	 * Checks if is m enable sync cookie.
	 *
	 * @return true, if is m enable sync cookie.
	 */
	public boolean isEnableSyncCookie() {
		return mEnableSyncCookie;
	}

	/**
	 * Checks if is holidays ignored.
	 *
	 * @return holidaysIgnored.
	 */
	public boolean isHolidaysIgnored() {
		return mHolidaysIgnored;
	}

	/**
	 * Checks if is log audit message into db.
	 *
	 * @return the logAuditMessageIntoDB.
	 */
	public boolean isLogAuditMessageIntoDB() {
		return mLogAuditMessageIntoDB;
	}

	/**
	 * Checks if is port required.
	 *
	 * @return the portRequired.
	 */
	public boolean isPortRequired() {
		return mPortRequired;
	}

	/**
	 * Checks if is sorting enabled.
	 *
	 * @return the sortingEnabled.
	 */
	public boolean isSortingEnabled() {
		return mSortingEnabled;
	}

	/**
	 * Sets the address field properties.
	 *
	 * @param pAddressFieldProperties the addressFieldProperties to set.
	 */
	public void setAddressFieldProperties(List<String> pAddressFieldProperties) {
		mAddressFieldProperties = pAddressFieldProperties;
	}

	/**
	 * Sets the akamai url.
	 *
	 * @param pAkamaiUrl the akamaiUrl to set.
	 */
	public void setAkamaiUrl(String pAkamaiUrl) {
		mAkamaiUrl = pAkamaiUrl;
	}

	/**
	 * Sets the bru norton seal url.
	 *
	 * @param pBruNortonSealURL the mBruNortonSealURL to set.
	 */
	public void setBruNortonSealURL(String pBruNortonSealURL) {
		mBruNortonSealURL = pBruNortonSealURL;
	}

	/**
	 * Sets/updates the cardCVV.
	 *
	 * @param pCardCVV
	 *            the cardCVV to set.
	 */

	public void setCardCVV(String pCardCVV) {
		mCardCVV = pCardCVV;
	}


	/**
	 * Sets the card number plcc.
	 *
	 * @param pCardNumberPLCC the cardNumberPLCC to set.
	 */
	public void setCardNumberPLCC(String pCardNumberPLCC) {
		mCardNumberPLCC = pCardNumberPLCC;
	}

	/**
	 * Sets the card number prcob.
	 *
	 * @param pCardNumberPRCOB the cardNumberPRCOB to set.
	 */
	public void setCardNumberPRCOB(String pCardNumberPRCOB) {
		this.mCardNumberPRCOB = pCardNumberPRCOB;
	}

	/**
	 * Sets the card number ruscob.
	 *
	 * @param pCardNumberRUSCOB the cardNumberRUSCOB to set.
	 */
	public void setCardNumberRUSCOB(String pCardNumberRUSCOB) {
		this.mCardNumberRUSCOB = pCardNumberRUSCOB;
	}

	/**
	 * Sets the cart checkout with pay pal error url.
	 *
	 * @param pCartCheckoutWithPayPalErrorURL the mCartCheckoutWithPayPalErrorURL to set.
	 */
	public void setCartCheckoutWithPayPalErrorURL(
			String pCartCheckoutWithPayPalErrorURL) {
		mCartCheckoutWithPayPalErrorURL = pCartCheckoutWithPayPalErrorURL;
	}

	/**
	 * Sets the cart checkout with pay pal success url.
	 *
	 * @param pCartCheckoutWithPayPalSuccessURL the mCartCheckoutWithPayPalSuccessURL to set.
	 */
	public void setCartCheckoutWithPayPalSuccessURL(
			String pCartCheckoutWithPayPalSuccessURL) {
		mCartCheckoutWithPayPalSuccessURL = pCartCheckoutWithPayPalSuccessURL;
	}

	/**
	 * Sets the channel availability.
	 *
	 * @param pChannelAvailabilityForBoth the new channel availability.
	 */
	public void setChannelAvailabilityForBoth(String pChannelAvailabilityForBoth) {
		mChannelAvailabilityForBoth = pChannelAvailabilityForBoth;
	}

	/**
	 * Sets the channel type map.
	 *
	 * @param pChannelTypeMap the channelTypeMap to set.
	 */
	public void setChannelTypeMap(Map<String, String> pChannelTypeMap) {
		mChannelTypeMap = pChannelTypeMap;
	}

	/**
	 * Sets the charity donation redirect url.
	 *
	 * @param pCharityDonationRedirectURL the new charity donation redirect url.
	 */
	public void setCharityDonationRedirectURL(String pCharityDonationRedirectURL) {
		mCharityDonationRedirectURL = pCharityDonationRedirectURL;
	}

	/**
	 * Sets the check inventory online.
	 *
	 * @param pCheckInventoryOnline the new check inventory online.
	 */
	public void setCheckInventoryOnline(boolean pCheckInventoryOnline) {
		mCheckInventoryOnline = pCheckInventoryOnline;
	}

	/**
	 * Sets the checkout tab numbers.
	 *
	 * @param pCheckoutTabNumbers the checkoutTabNumbers to set.
	 */
	public void setCheckoutTabNumbers(Map<String, String> pCheckoutTabNumbers) {
		mCheckoutTabNumbers = pCheckoutTabNumbers;
	}
	/**
	 * Sets the checkout tab urls.
	 *
	 * @param pCheckoutTabUrls the checkoutTabUrls to set.
	 */
	public void setCheckoutTabUrls(Map<String, String> pCheckoutTabUrls) {
		mCheckoutTabUrls = pCheckoutTabUrls;
	}
	/**
	 * Sets the checkout with pay pal error url.
	 *
	 * @param pCheckoutWithPayPalErrorURL the mCheckoutWithPayPalErrorURL to set.
	 */
	public void setCheckoutWithPayPalErrorURL(String pCheckoutWithPayPalErrorURL) {
		mCheckoutWithPayPalErrorURL = pCheckoutWithPayPalErrorURL;
	}

	/**
	 * Sets the checkout with pay pal success url.
	 *
	 * @param pCheckoutWithPayPalSuccessURL the mCheckoutWithPayPalSuccessURL to set.
	 */
	public void setCheckoutWithPayPalSuccessURL(
			String pCheckoutWithPayPalSuccessURL) {
		mCheckoutWithPayPalSuccessURL = pCheckoutWithPayPalSuccessURL;
	}

	/**
	 * Sets the color sequence map.
	 *
	 * @param pColorSequenceMap the colorSequenceMap to set.
	 */
	public void setColorSequenceMap(Map<String, String> pColorSequenceMap) {
		mColorSequenceMap = pColorSequenceMap;
	}

	/**
	 * Sets the copy ship item rel properties.
	 *
	 * @param pCopyShipItemRelProperties the copyShipItemRelProperties to set.
	 */
	public void setCopyShipItemRelProperties(List<String> pCopyShipItemRelProperties) {
		mCopyShipItemRelProperties = pCopyShipItemRelProperties;
	}

	/**
	 * Sets the credit card type names for integration.
	 *
	 * @param pCreditCardTypeNamesForIntegration the creditCardTypeNamesForIntegration to set.
	 */
	public void setCreditCardTypeNamesForIntegration(
			Map<String, String> pCreditCardTypeNamesForIntegration) {
		mCreditCardTypeNamesForIntegration = pCreditCardTypeNamesForIntegration;
	}

	/**
	 * Sets/updates the csrEmailEnabled.
	 *
	 * @param pCsrEmailEnabled
	 *            the csrEmailEnabled to set.
	 */

	public void setCsrEmailEnabled(boolean pCsrEmailEnabled) {
		mCsrEmailEnabled = pCsrEmailEnabled;
	}

	/**
	 * Sets the csr source order type.
	 *
	 * @param pCsrSourceOrderType the csrSourceOrderType to set.
	 */
	public void setCsrSourceOrderType(String pCsrSourceOrderType) {
		mCsrSourceOrderType = pCsrSourceOrderType;
	}

	/**
	 * Sets the currency code.
	 *
	 * @param pCurrencyCode the currencyCode to set.
	 */
	public void setCurrencyCode(String pCurrencyCode) {
		mCurrencyCode = pCurrencyCode;
	}

	/**
	 * Sets the currency codes string.
	 *
	 * @param pCurrencyCodesString the currencyCodesString to set.
	 */
	public void setCurrencyCodesString(Map<String, String> pCurrencyCodesString) {
		mCurrencyCodesString = pCurrencyCodesString;
	}

	/**
	 * Sets the disable rest service url.
	 *
	 * @param pDisableRestServiceURL the disableRestServiceURL to set.
	 */
	public void setDisableRestServiceURL(String pDisableRestServiceURL) {
		mDisableRestServiceURL = pDisableRestServiceURL;
	}

	/**
	 * Sets the display credit card type name.
	 *
	 * @param pDisplayCreditCardTypeName the displayCreditCardTypeName to set.
	 */
	public void setDisplayCreditCardTypeName(
			Map<String, String> pDisplayCreditCardTypeName) {
		mDisplayCreditCardTypeName = pDisplayCreditCardTypeName;
	}


	/**
	 * Sets the display no of cards.
	 * @param pDisplayNoOfCards the mDisplayNoOfCards to set.
	 */
	public void setDisplayNoOfCards(String pDisplayNoOfCards) {
		mDisplayNoOfCards = pDisplayNoOfCards;
	}

	/**
	 * Sets the display pay pal payment type name.
	 *
	 * @param pDisplayPayPalPaymentTypeName the displayPayPalPaymentTypeName to set.
	 */
	public void setDisplayPayPalPaymentTypeName(Map<String, String> pDisplayPayPalPaymentTypeName) {
		mDisplayPayPalPaymentTypeName = pDisplayPayPalPaymentTypeName;
	}

	/**
	 * Sets the distance for near by store search.
	 *
	 * @param pDistanceForNearByStoreSearch the new distance for near by store search.
	 */
	public void setDistanceForNearByStoreSearch(
			double pDistanceForNearByStoreSearch) {
		mDistanceForNearByStoreSearch = pDistanceForNearByStoreSearch;
	}

	/**
	 * Sets the domain url.
	 *
	 * @param pDomainUrl the domainUrl to set.
	 */
	public void setDomainUrl(String pDomainUrl) {
		mDomainUrl = pDomainUrl;
	}

	/**
	 * Sets the edd cut off time.
	 *
	 * @param pEddCutOffTime the eddCutOffTime to set.
	 */
	public void setEddCutOffTime(int pEddCutOffTime) {
		mEddCutOffTime = pEddCutOffTime;
	}

	/**
	 * Sets the edd enabled.
	 *
	 * @param pEddEnabled the eddEnabled to set.
	 */
	public void setEddEnabled(boolean pEddEnabled) {
		mEddEnabled = pEddEnabled;
	}

	/**
	 * Sets the edits the pref url.
	 * @param pEditPrefURL the editPrefURL to set.
	 */
	public void setEditPrefURL(String pEditPrefURL) {
		mEditPrefURL = pEditPrefURL;
	}

	/**
	 * Sets the enable address doctor.
	 * @param pEnableAddressDoctor the enableAddressDoctor to set.
	 */
	public void setEnableAddressDoctor(boolean pEnableAddressDoctor) {
		mEnableAddressDoctor = pEnableAddressDoctor;
	}

	/**
	 * Sets the enable coupon service.
	 *
	 * @param pEnableCouponService the enableCouponService to set.
	 */
	public void setEnableCouponService(boolean pEnableCouponService) {
		mEnableCouponService = pEnableCouponService;
	}

	/**
	 * Sets the enable credit card full auth failure.
	 *
	 * @param pEnableCreditCardFullAuthFailure the enableCreditCardFullAuthFailure to set.
	 */
	public void setEnableCreditCardFullAuthFailure(
			boolean pEnableCreditCardFullAuthFailure) {
		mEnableCreditCardFullAuthFailure = pEnableCreditCardFullAuthFailure;
	}

	/**
	 * Sets the enable credit card void failure.
	 *
	 * @param pEnableCreditCardVoidFailure the enableCreditCardVoidFailure to set.
	 */
	public void setEnableCreditCardVoidFailure(boolean pEnableCreditCardVoidFailure) {
		mEnableCreditCardVoidFailure = pEnableCreditCardVoidFailure;
	}

	/**
	 * Sets the enable epslon.
	 * @param pEnableEpslon the enableEpslon to set.
	 */
	public void setEnableEpslon(boolean pEnableEpslon) {
		mEnableEpslon = pEnableEpslon;
	}

	/**
	 * Sets the enable gift card full auth failure.
	 *
	 * @param pEnableGiftCardFullAuthFailure the enableGiftCardFullAuthFailure to set.
	 */
	public void setEnableGiftCardFullAuthFailure(
			boolean pEnableGiftCardFullAuthFailure) {
		mEnableGiftCardFullAuthFailure = pEnableGiftCardFullAuthFailure;
	}

	/**
	 * Sets the enable gift card refund failure.
	 *
	 * @param pEnableGiftCardRefundFailure the enableGiftCardRefundFailure to set.
	 */
	public void setEnableGiftCardRefundFailure(boolean pEnableGiftCardRefundFailure) {
		mEnableGiftCardRefundFailure = pEnableGiftCardRefundFailure;
	}

	/**
	 * Sets the enable gift card void failure.
	 *
	 * @param pEnableGiftCardVoidFailure the enableGiftCardVoidFailure to set.
	 */
	public void setEnableGiftCardVoidFailure(boolean pEnableGiftCardVoidFailure) {
		mEnableGiftCardVoidFailure = pEnableGiftCardVoidFailure;
	}

	/**
	 * Sets the enable inline seo.
	 *
	 * @param pEnableInlineSeo the new enable inline seo.
	 */
	public void setEnableInlineSeo(boolean pEnableInlineSeo) {
		mEnableInlineSeo = pEnableInlineSeo;
	}

	/**
	 * Sets the enable pay pal full auth failure.
	 *
	 * @param pEnablePayPalFullAuthFailure the enablePayPalFullAuthFailure to set.
	 */
	public void setEnablePayPalFullAuthFailure(boolean pEnablePayPalFullAuthFailure) {
		mEnablePayPalFullAuthFailure = pEnablePayPalFullAuthFailure;
	}

	/**
	 * Sets the enable pay pal void failure.
	 *
	 * @param pEnablePayPalVoidFailure the enablePayPalVoidFailure to set.
	 */
	public void setEnablePayPalVoidFailure(boolean pEnablePayPalVoidFailure) {
		mEnablePayPalVoidFailure = pEnablePayPalVoidFailure;
	}

	/**
	 * Sets the enable pre sell edd.
	 *
	 * @param pEnablePreSellEDD - EnablePreSellEDD
	 */
	public void setEnablePreSellEDD(boolean pEnablePreSellEDD ) {
		this.mEnablePreSellEDD = pEnablePreSellEDD ;
	}

	/**
	 * Sets the enableSearchDex flag value.
	 *
	 * @param pEnableSearchDex the enableSearchDex.
	 */
	public void setEnableSearchDex(boolean pEnableSearchDex) {
		mEnableSearchDex = pEnableSearchDex;
	}

	/**
	 * Sets the enable sos.
	 *
	 * @param pEnableSOS the EnableSOS to set.
	 */
	public void setEnableSOS(boolean pEnableSOS) {
		mEnableSOS = pEnableSOS;
	}

	/**
	 * Sets the m enable sync cookie.
	 *
	 * @param pEnableSyncCookie the new m enable sync cookie.
	 */
	public void setEnableSyncCookie(boolean pEnableSyncCookie) {
		this.mEnableSyncCookie = pEnableSyncCookie;
	}

	/**
	 * Sets/updates the englishLocale.
	 * @param pEnglishLocale the englishLocale to set.
	 */
	public void setEnglishLocale(String pEnglishLocale) {
		mEnglishLocale = pEnglishLocale;
	}

	/**
	 * Sets the epsilonPixelURL.
	 *
	 * @param pEpsilonPixelURL the pEpsilonPixelURL.
	 */
	public void setEpsilonPixelURL(String pEpsilonPixelURL) {
		mEpsilonPixelURL = pEpsilonPixelURL;
	}

	/**
	 * Sets the expMonthAdjustmentForPLCC.
	 *
	 * @param pExpMonthAdjustmentForPLCC the expMonthAdjustmentForPLCC.
	 */
	public void setExpMonthAdjustmentForPLCC(String pExpMonthAdjustmentForPLCC) {
		this.mExpMonthAdjustmentForPLCC = pExpMonthAdjustmentForPLCC;
	}

	/**
	 * Sets the expYearAdjustmentForPLCC.
	 *
	 * @param pExpYearAdjustmentForPLCC the expYearAdjustmentForPLCC.
	 */
	public void setExpYearAdjustmentForPLCC(String pExpYearAdjustmentForPLCC) {
		this.mExpYearAdjustmentForPLCC = pExpYearAdjustmentForPLCC;
	}

	/**
	 * Sets the failed credit card amt.
	 *
	 * @param pFailedCreditCardAmt the failedCreditCardAmt to set.
	 */
	public void setFailedCreditCardAmt(String pFailedCreditCardAmt) {
		mFailedCreditCardAmt = pFailedCreditCardAmt;
	}

	/**
	 * Sets the failed credit card void amt.
	 *
	 * @param pFailedCreditCardVoidAmt the failedCreditCardVoidAmt to set.
	 */
	public void setFailedCreditCardVoidAmt(String pFailedCreditCardVoidAmt) {
		mFailedCreditCardVoidAmt = pFailedCreditCardVoidAmt;
	}

	/**
	 * Sets the failed gift card amt.
	 *
	 * @param pFailedGiftCardAmt the failedGiftCardAmt to set.
	 */
	public void setFailedGiftCardAmt(String pFailedGiftCardAmt) {
		mFailedGiftCardAmt = pFailedGiftCardAmt;
	}

	/**
	 * Sets the failed gift card refund amt.
	 *
	 * @param pFailedGiftCardRefundAmt the failedGiftCardRefundAmt to set.
	 */
	public void setFailedGiftCardRefundAmt(String pFailedGiftCardRefundAmt) {
		mFailedGiftCardRefundAmt = pFailedGiftCardRefundAmt;
	}

	/**
	 * Sets the failed gift card void amt.
	 *
	 * @param pFailedGiftCardVoidAmt the failedGiftCardVoidAmt to set.
	 */
	public void setFailedGiftCardVoidAmt(String pFailedGiftCardVoidAmt) {
		mFailedGiftCardVoidAmt = pFailedGiftCardVoidAmt;
	}

	/**
	 * Sets the failed pay pal amt.
	 *
	 * @param pFailedPayPalAmt the failedPayPalAmt to set.
	 */
	public void setFailedPayPalAmt(String pFailedPayPalAmt) {
		mFailedPayPalAmt = pFailedPayPalAmt;
	}

	/**
	 * Sets the failed pay pal void amt.
	 *
	 * @param pFailedPayPalVoidAmt the failedPayPalVoidAmt to set.
	 */
	public void setFailedPayPalVoidAmt(String pFailedPayPalVoidAmt) {
		mFailedPayPalVoidAmt = pFailedPayPalVoidAmt;
	}

	/**
	 * Sets/updates the fraudAcceptEmail.
	 *
	 * @param pFraudAcceptEmail
	 *            the fraudAcceptEmail to set.
	 */

	public void setFraudAcceptEmail(String pFraudAcceptEmail) {
		mFraudAcceptEmail = pFraudAcceptEmail;
	}

	/**
	 * Sets the gift card number length.
	 *
	 * @param pGiftCardNumberLength the giftCardNumberLength to set.
	 */
	public void setGiftCardNumberLength(int pGiftCardNumberLength) {
		mGiftCardNumberLength = pGiftCardNumberLength;
	}

	/**
	 * Sets the gift card pin length.
	 *
	 * @param pGiftCardPinLength the giftCardPinLength to set.
	 */
	public void setGiftCardPinLength(int pGiftCardPinLength) {
		mGiftCardPinLength = pGiftCardPinLength;
	}

	/**
	 * Sets the gift options page url.
	 *
	 * @param pGiftOptionsPageURL the giftOptionsPageURL to set.
	 */
	public void setGiftOptionsPageURL(String pGiftOptionsPageURL) {
		mGiftOptionsPageURL = pGiftOptionsPageURL;
	}

	/**
	 * Sets the holidays ignored.
	 *
	 * @param pHolidaysIgnored holidaysIgnored.
	 */
	public void setHolidaysIgnored(boolean pHolidaysIgnored) {
		mHolidaysIgnored = pHolidaysIgnored;
	}

	/**
	 * Sets the http.
	 * @param pHTTP the mHTTP to set.
	 */
	public void setHTTP(String pHTTP) {
		mHTTP = pHTTP;
	}

	/**
	 * Sets the image server folder path.
	 *
	 * @param pImageServerFolderPath the imageServerFolderPath to set.
	 */
	public void setImageServerFolderPath(String pImageServerFolderPath) {
		mImageServerFolderPath = pImageServerFolderPath;
	}

	/**
	 * Sets the image server url.
	 *
	 * @param pImageServerURL the imageServerURL to set.
	 */
	public void setImageServerURL(String pImageServerURL) {
		mImageServerURL = pImageServerURL;
	}

	/**
	 * Sets the in auth key.
	 *
	 * @param pInAuthKey the inAuthKey to set.
	 */
	public void setInAuthKey(String pInAuthKey) {
		mInAuthKey = pInAuthKey;
	}

	/**
	 * Sets the in auth url.
	 *
	 * @param pInAuthURL the inAuthURL to set.
	 */
	public void setInAuthURL(String pInAuthURL) {
		mInAuthURL = pInAuthURL;
	}

	/**
	 * Sets the in store only.
	 *
	 * @param pInStoreOnly the new in store only.
	 */
	public void setInStoreOnly(String pInStoreOnly) {
		mInStoreOnly = pInStoreOnly;
	}

	/**
	 * Sets the level2 tax number.
	 *
	 * @param pLevel2TaxNumber the level2TaxNumber to set.
	 */
	public void setLevel2TaxNumber(String pLevel2TaxNumber) {
		mLevel2TaxNumber = pLevel2TaxNumber;
	}

	/**
	 * Sets the lightbox configurable time.
	 * @param pLightboxConfigurableTime the mLightboxConfigurableTime to set.
	 */
	public void setLightboxConfigurableTime(String pLightboxConfigurableTime) {
		mLightboxConfigurableTime = pLightboxConfigurableTime;
	}

	/**
	 * Sets the lightboxdonotshow configurable time.
	 * @param pLightboxdonotshowConfigurableTime the new lightboxdonotshow configurable time.
	 */
	public void setLightboxdonotshowConfigurableTime(
			String pLightboxdonotshowConfigurableTime) {
		mLightboxdonotshowConfigurableTime = pLightboxdonotshowConfigurableTime;
	}

	/**
	 * Sets the lightboxsubmit configurable time.
	 * @param pLightboxsubmitConfigurableTime the new lightboxsubmit configurable time.
	 */
	public void setLightboxsubmitConfigurableTime(
			String pLightboxsubmitConfigurableTime) {
		mLightboxsubmitConfigurableTime = pLightboxsubmitConfigurableTime;
	}

	/**
	 * Sets the limit ofdays to tell a product new.
	 *
	 * @param pLimitOfdaysToTellAProductNew the limitOfdaysToTellAProductNew to set.
	 */
	public void setLimitOfdaysToTellAProductNew(int pLimitOfdaysToTellAProductNew) {
		mLimitOfdaysToTellAProductNew = pLimitOfdaysToTellAProductNew;
	}

	/**
	 * Sets the location id for ship to home.
	 * @param pLocationIdForShipToHome the new location id for ship to home.
	 */
	public void setLocationIdForShipToHome(String pLocationIdForShipToHome) {
		mLocationIdForShipToHome = pLocationIdForShipToHome;
	}

	/**
	 * Sets the log audit message into db.
	 *
	 * @param pLogAuditMessageIntoDB the logAuditMessageIntoDB to set.
	 */
	public void setLogAuditMessageIntoDB(boolean pLogAuditMessageIntoDB) {
		mLogAuditMessageIntoDB = pLogAuditMessageIntoDB;
	}

	/**
	 * Sets the max gift cards.
	 *
	 * @param pMaxGiftCards the maxGiftCards to set.
	 */
	public void setMaxGiftCards(int pMaxGiftCards) {
		mMaxGiftCards = pMaxGiftCards;
	}

	/**
	 * Sets the mobile source order type.
	 *
	 * @param pMobileSourceOrderType the mobileSourceOrderType to set.
	 */
	public void setMobileSourceOrderType(String pMobileSourceOrderType) {
		mMobileSourceOrderType = pMobileSourceOrderType;
	}

	/**
	 * Sets the myAccountRegistryUrl.
	 * @param pMyAccountRegistryUrl the mMyAccountRegistryUrl to set.
	 */
	public void setMyAccountRegistryUrl(String pMyAccountRegistryUrl) {
		mMyAccountRegistryUrl = pMyAccountRegistryUrl;
	}

	/**
	 * Sets the myAccountWishlistUrl.
	 * @param pMyAccountWishlistUrl the mMyAccountWishlistUrl to set.
	 */
	public void setMyAccountWishlistUrl(String pMyAccountWishlistUrl) {
		mMyAccountWishlistUrl = pMyAccountWishlistUrl;
	}

	/**
	 * Sets the norton seal url.
	 *
	 * @param pNortonSealURL the mnortonSealURL to set.
	 */
	public void setNortonSealURL(String pNortonSealURL) {
		mNortonSealURL = pNortonSealURL;
	}
	/**
	 *This method is used to set olsonLearnMoreURL.
	 *@param pOlsonLearnMoreURL String.
	 */
	public void setOlsonLearnMoreURL(String pOlsonLearnMoreURL) {
		mOlsonLearnMoreURL = pOlsonLearnMoreURL;
	}

	/**
	 * Sets the olson look up url.
	 * @param pOlsonLookUpURL the olsonLookUpURL to set.
	 */
	public void setOlsonLookUpURL(String pOlsonLookUpURL) {
		mOlsonLookUpURL = pOlsonLookUpURL;
	}

	/**
	 * Sets the olson sign up url.
	 * @param pOlsonSignUpURL the olsonSignUpURL to set.
	 */
	public void setOlsonSignUpURL(String pOlsonSignUpURL) {
		mOlsonSignUpURL = pOlsonSignUpURL;
	}

	/**
	 * Sets the oms cancel order rest apiurl.
	 *
	 * @param pOmsCancelOrderRestAPIURL the mOmsCancelOrderRestAPIURL to set.
	 */
	public void setOmsCancelOrderRestAPIURL(String pOmsCancelOrderRestAPIURL) {
		mOmsCancelOrderRestAPIURL = pOmsCancelOrderRestAPIURL;
	}

	/**
	 * Sets the oms cancel order service url.
	 *
	 * @param pOmsCancelOrderServiceURL the mOmsCancelOrderServiceURL to set.
	 */
	public void setOmsCancelOrderServiceURL(String pOmsCancelOrderServiceURL) {
		mOmsCancelOrderServiceURL = pOmsCancelOrderServiceURL;
	}

	/**
	 * Sets the oms customer master service url.
	 *
	 * @param pOmsCustomerMasterServiceURL the mOmsCustomerMasterServiceURL to set.
	 */

	public void setOmsCustomerMasterServiceURL(String pOmsCustomerMasterServiceURL) {
		mOmsCustomerMasterServiceURL = pOmsCustomerMasterServiceURL;
	}

	/**
	 * Sets the oms customer order import service url.
	 *
	 * @param pOmsCustomerOrderImportServiceURL the mOmsCustomerOrderImportServiceURL to set.
	 */

	public void setOmsCustomerOrderImportServiceURL(String pOmsCustomerOrderImportServiceURL) {
		mOmsCustomerOrderImportServiceURL = pOmsCustomerOrderImportServiceURL;
	}

	/**
	 * Sets the oms order detail rest apiurl.
	 *
	 * @param pOmsOrderDetailRestAPIURL the mOmsOrderDetailRestAPIURL to set.
	 */
	public void setOmsOrderDetailRestAPIURL(String pOmsOrderDetailRestAPIURL) {
		mOmsOrderDetailRestAPIURL = pOmsOrderDetailRestAPIURL;
	}

	/**
	 * Sets the oms order detail service url.
	 *
	 * @param pOmsOrderDetailServiceURL the mOmsOrderDetailServiceURL to set.
	 */
	public void setOmsOrderDetailServiceURL(String pOmsOrderDetailServiceURL) {
		mOmsOrderDetailServiceURL = pOmsOrderDetailServiceURL;
	}

	/**
	 * Sets the oms order list rest apiurl.
	 *
	 * @param pOmsOrderListRestAPIURL the mOmsOrderListRestAPIURL to set.
	 */
	public void setOmsOrderListRestAPIURL(String pOmsOrderListRestAPIURL) {
		mOmsOrderListRestAPIURL = pOmsOrderListRestAPIURL;
	}

	/**
	 * Sets the oms order list service url.
	 *
	 * @param pOmsOrderListServiceURL the mOmsOrderListServiceURL to set.
	 */
	public void setOmsOrderListServiceURL(String pOmsOrderListServiceURL) {
		mOmsOrderListServiceURL = pOmsOrderListServiceURL;
	}

	/**
	 * Sets the online only.
	 *
	 * @param pOnlineOnly the new online only.
	 */
	public void setOnlineOnly(String pOnlineOnly) {
		mOnlineOnly = pOnlineOnly;
	}

	/**
	 * Sets the order repricing option.
	 * @param pOrderRepricingOptionp the orderRepricingOption to set.
	 */
	public void setOrderRepricingOption(String pOrderRepricingOptionp) {
		mOrderRepricingOption = pOrderRepricingOptionp;
	}

	/**
	 * Sets the payment page url.
	 *
	 * @param pPaymentPageURL the paymentPageURL to set.
	 */
	public void setPaymentPageURL(String pPaymentPageURL) {
		mPaymentPageURL = pPaymentPageURL;
	}

	/**
	 * Sets the pay pal chkout cancel url.
	 *
	 * @param pPayPalChkoutCancelUrl the pPayPalChkoutCancelUrl to set.
	 */
	public void setPayPalChkoutCancelUrl(String pPayPalChkoutCancelUrl) {
		mPayPalChkoutCancelUrl = pPayPalChkoutCancelUrl;
	}

	/**
	 * Sets the pay pal chkout from order review cancel url.
	 *
	 * @param pPayPalChkoutFromOrderReviewCancelUrl the pPayPalChkoutFromOrderReviewCancelUrl to set.
	 */
	public void setPayPalChkoutFromOrderReviewCancelUrl(
			String pPayPalChkoutFromOrderReviewCancelUrl) {
		mPayPalChkoutFromOrderReviewCancelUrl = pPayPalChkoutFromOrderReviewCancelUrl;
	}

	/**
	 * Sets the pay pal chkout from order review success url.
	 *
	 * @param pPayPalChkoutFromOrderReviewSuccessUrl the pPayPalChkoutFromOrderReviewSuccessUrl to set.
	 */
	public void setPayPalChkoutFromOrderReviewSuccessUrl(
			String pPayPalChkoutFromOrderReviewSuccessUrl) {
		mPayPalChkoutFromOrderReviewSuccessUrl = pPayPalChkoutFromOrderReviewSuccessUrl;
	}

	/**
	 * Sets the pay pal chkout success url.
	 *
	 * @param pPayPalChkoutSuccessUrl the pPayPalChkoutSuccessUrl to set.
	 */
	public void setPayPalChkoutSuccessUrl(String pPayPalChkoutSuccessUrl) {
		mPayPalChkoutSuccessUrl = pPayPalChkoutSuccessUrl;
	}

	/**
	 * Sets the pay pal exp chkout cancel url.
	 *
	 * @param pPayPalExpChkoutCancelUrl the payPalExpChkoutCancelUrl to set.
	 */
	public void setPayPalExpChkoutCancelUrl(String pPayPalExpChkoutCancelUrl) {
		mPayPalExpChkoutCancelUrl = pPayPalExpChkoutCancelUrl;
	}

	/**
	 * Sets the pay pal exp chkout success url.
	 *
	 * @param pPayPalExpChkoutSuccessUrl the payPalExpChkoutSuccessUrl to set.
	 */
	public void setPayPalExpChkoutSuccessUrl(String pPayPalExpChkoutSuccessUrl) {
		mPayPalExpChkoutSuccessUrl = pPayPalExpChkoutSuccessUrl;
	}

	/**
	 * Sets the pay pal review error url.
	 *
	 * @param pPayPalReviewErrorUrl the new pay pal review error url.
	 */
	public void setPayPalReviewErrorUrl(String pPayPalReviewErrorUrl) {
		mPayPalReviewErrorUrl = pPayPalReviewErrorUrl;
	}

	/**
	 * Sets the pay pal review success url.
	 *
	 * @param pPayPalReviewSuccessUrl the mPayPalReviewSuccessUrl to set.
	 */
	public void setPayPalReviewSuccessUrl(String pPayPalReviewSuccessUrl) {
		mPayPalReviewSuccessUrl = pPayPalReviewSuccessUrl;
	}

	/**
	 * Sets the pay pal token expiry time.
	 *
	 * @param pPayPalTokenExpiryTime the payPalTokenExpiryTime to set.
	 */
	public void setPayPalTokenExpiryTime(String pPayPalTokenExpiryTime) {
		mPayPalTokenExpiryTime = pPayPalTokenExpiryTime;
	}

	/**
	 * Sets the pickup page url.
	 *
	 * @param pPickupPageURL pickup page url.
	 */
	public void setPickupPageURL(String pPickupPageURL) {
		mPickupPageURL = pPickupPageURL;
	}

	/**
	 * Sets the port required.
	 *
	 * @param pPortRequired the portRequired to set.
	 */
	public void setPortRequired(boolean pPortRequired) {
		mPortRequired = pPortRequired;
	}

	/**
	 * Sets the power reviews app launch url.
	 *
	 * @param pPowerReviewsApiKey the new power reviews app launch url.
	 */
	public void setPowerReviewsApiKey(String pPowerReviewsApiKey) {
		mPowerReviewsApiKey = pPowerReviewsApiKey;

	/**
	 * Sets the power reviews full url.
	 *
	 * @param pPowerReviewsFullURL the new power reviews full url.
	 */
	}

	/**
	 * Sets the power reviews url.
	 *
	 * @param pPowerReviewsURL the powerReviewsURL to set.
	 */
	public void setPowerReviewsURL(String pPowerReviewsURL) {
		mPowerReviewsURL = pPowerReviewsURL;
	}

	/**
	 * Sets/updates the priceOnCartValue.
	 * @param pPriceOnCartValue the priceOnCartValue to set.
	 */
	public void setPriceOnCartValue(String pPriceOnCartValue) {
		mPriceOnCartValue = pPriceOnCartValue;
	}

	/**
	 * Sets/updates the promotionCountinPDP.
	 *
	 * @param pPromotionCountinPDP the promotionCountinPDP to set.
	 */
	public void setPromotionCountinPDP(String pPromotionCountinPDP) {
		mPromotionCountinPDP = pPromotionCountinPDP;
	}

	/**
	 * Sets the recaptcha no script url.
	 *
	 * @param pRecaptchaNoScriptURL the recaptchaNoScriptURL to set.
	 */
	public void setRecaptchaNoScriptURL(String pRecaptchaNoScriptURL) {
		mRecaptchaNoScriptURL = pRecaptchaNoScriptURL;
	}

	/**
	 * Sets the recaptcha private key.
	 *
	 * @param pRecaptchaPrivateKey the recaptchaPrivateKey to set.
	 */
	public void setRecaptchaPrivateKey(String pRecaptchaPrivateKey) {
		mRecaptchaPrivateKey = pRecaptchaPrivateKey;
	}

	/**
	 * Sets the recaptcha secret key.
	 *
	 * @param pRecaptchaSecretKey the recaptchaSecretKey to set.
	 */
	public void setRecaptchaSecretKey(String pRecaptchaSecretKey) {
		mRecaptchaSecretKey = pRecaptchaSecretKey;
	}
	
	/**
	 * Sets the recaptcha public key.
	 *
	 * @param pRecaptchaPublicKey the recaptchaPublicKey to set.
	 */
	public void setRecaptchaPublicKey(String pRecaptchaPublicKey) {
		mRecaptchaPublicKey = pRecaptchaPublicKey;
	}

	/**
	 * Sets the recaptcha script url.
	 *
	 * @param pRecaptchaScriptURL the recaptchaScriptURL to set.
	 */
	public void setRecaptchaScriptURL(String pRecaptchaScriptURL) {
		mRecaptchaScriptURL = pRecaptchaScriptURL;
	}

	/**
	 * Sets the registry url.
	 * @param pRegistryUrl the new registry url.
	 */
	public void setRegistryUrl(String pRegistryUrl) {
		mRegistryUrl = pRegistryUrl;
	}

	/**
	 * Sets the resetPasswordLinkExpiryTime.
	 *
	 * @param pResetPasswordLinkExpiryTime the resetPasswordLinkExpiryTime.
	 */
	public void setResetPasswordLinkExpiryTime(int pResetPasswordLinkExpiryTime) {
		this.mResetPasswordLinkExpiryTime = pResetPasswordLinkExpiryTime;
	}
	/**
	 * Sets the sd city.
	 *
	 * @param pSDCity the sDCity to set.
	 */
	public void setSDCity(Map<String, String> pSDCity) {
		mSDCity = pSDCity;
	}

	/**
	 * Sets the sd country code.
	 *
	 * @param pSDCountryCode the sDCountryCode to set.
	 */
	public void setSDCountryCode(Map<String, String> pSDCountryCode) {
		mSDCountryCode = pSDCountryCode;
	}
	/**
	 * Sets the sddba name.
	 *
	 * @param pSDDBAName the sDDBAName to set.
	 */
	public void setSDDBAName(Map<String, String> pSDDBAName) {
		mSDDBAName = pSDDBAName;
	}
	/**
	 * Sets the sd mcc.
	 *
	 * @param pSDMcc the sDMcc to set.
	 */
	public void setSDMcc(Map<String, String> pSDMcc) {
		mSDMcc = pSDMcc;
	}
	/**
	 * Sets the sd merchange contact info.
	 *
	 * @param pSDMerchangeContactInfo the sDMerchangeContactEmail to set.
	 */
	public void setSDMerchangeContactInfo(
			Map<String, String> pSDMerchangeContactInfo) {
		mSDMerchangeContactInfo = pSDMerchangeContactInfo;
	}

	/**
	 * Sets the sd mid.
	 *
	 * @param pSDMid the sDMid to set.
	 */
	public void setSDMid(Map<String, String> pSDMid) {
		mSDMid = pSDMid;
	}

	/**
	 * Sets the sd postal code.
	 *
	 * @param pSDPostalCode the sDPostalCode to set.
	 */
	public void setSDPostalCode(Map<String, String> pSDPostalCode) {
		mSDPostalCode = pSDPostalCode;
	}

	/**
	 * Sets the sd region.
	 *
	 * @param pSDRegion the sDRegion to set.
	 */
	public void setSDRegion(Map<String, String> pSDRegion) {
		mSDRegion = pSDRegion;
	}

	/**
	 * Sets the sd street.
	 *
	 * @param pSDStreet the sDStreet to set.
	 */
	public void setSDStreet(Map<String, String> pSDStreet) {
		mSDStreet = pSDStreet;
	}

	/**
	 * Sets the sd tax number.
	 *
	 * @param pSDTaxNumber the sDTaxNumber to set.
	 */
	public void setSDTaxNumber(Map<String, String> pSDTaxNumber) {
		mSDTaxNumber = pSDTaxNumber;
	}

	/**
	 * Sets the search dex footer page.
	 *
	 * @param pSearchDexFooterPage the searchDexFooterPage to set.
	 */
	public void setSearchDexFooterPage(String pSearchDexFooterPage) {
		mSearchDexFooterPage = pSearchDexFooterPage;
	}

	/**
	 * Sets the search dex header page.
	 *
	 * @param pSearchDexHeaderPage the searchDexHeaderPage to set.
	 */
	public void setSearchDexHeaderPage(String pSearchDexHeaderPage) {
		mSearchDexHeaderPage = pSearchDexHeaderPage;
	}

	/**
	 * Sets the search dex url.
	 *
	 * @param pSearchDexUrl the searchDexUrl to set.
	 */
	public void setSearchDexUrl(String pSearchDexUrl) {
		mSearchDexUrl = pSearchDexUrl;
	}


	/**
	 * Sets the serverTimeAdjustmentWithSynchrony.
	 *
	 * @param pServerTimeAdjustmentWithSynchrony the serverTimeAdjustmentWithSynchrony.
	 */
	public void setServerTimeAdjustmentWithSynchrony(int pServerTimeAdjustmentWithSynchrony) {
		this.mServerTimeAdjustmentWithSynchrony = pServerTimeAdjustmentWithSynchrony;
	}

	/**
	 * Sets the shipping page url.
	 *
	 * @param pShippingPageURL the shippingPageURL to set.
	 */
	public void setShippingPageURL(String pShippingPageURL) {
		mShippingPageURL = pShippingPageURL;
	}

	/**
	 * Sets the shop local url.
	 *
	 * @param pShopLocalURL the shopLocalURL to set.
	 */
	public void setShopLocalURL(String pShopLocalURL) {
		mShopLocalURL = pShopLocalURL;
	}

	/**
	 * Sets the shopping cart parameter.
	 *
	 * @param pShoppingCartParameter the new shopping cart parameter.
	 */
	public void setShoppingCartParameter(String pShoppingCartParameter) {
		mShoppingCartParameter = pShoppingCartParameter;
	}

	/**
	 * Sets the six month financing limit.
	 *
	 * @param pSixMonthFinancingLimit the sixMonthFinancingLimit to set.
	 */
	public void setSixMonthFinancingLimit(double pSixMonthFinancingLimit) {
		mSixMonthFinancingLimit = pSixMonthFinancingLimit;
	}

	/**
	 * Sets the size sequence map.
	 *
	 * @param pSizeSequenceMap the sizeSequenceMap to set.
	 */
	public void setSizeSequenceMap(Map<String, String> pSizeSequenceMap) {
		mSizeSequenceMap = pSizeSequenceMap;
	}

	/**
	 * Sets the soft des city.
	 *
	 * @param pSoftDesCity the softDesCity to set.
	 */
	public void setSoftDesCity(String pSoftDesCity) {
		mSoftDesCity = pSoftDesCity;
	}

	/**
	 * Sets the soft des country code.
	 *
	 * @param pSoftDesCountryCode the softDesCountryCode to set.
	 */
	public void setSoftDesCountryCode(String pSoftDesCountryCode) {
		mSoftDesCountryCode = pSoftDesCountryCode;
	}

	/**
	 * Sets the soft des dba name.
	 *
	 * @param pSoftDesDbaName the softDesDbaName to set.
	 */
	public void setSoftDesDbaName(String pSoftDesDbaName) {
		mSoftDesDbaName = pSoftDesDbaName;
	}

	/**
	 * Sets the soft des mcc.
	 *
	 * @param pSoftDesMcc the softDesMcc to set.
	 */
	public void setSoftDesMcc(String pSoftDesMcc) {
		mSoftDesMcc = pSoftDesMcc;
	}

	/**
	 * Sets the soft des merch contact info.
	 *
	 * @param pSoftDesMerchContactInfo the softDesMerchContactInfo to set.
	 */
	public void setSoftDesMerchContactInfo(String pSoftDesMerchContactInfo) {
		mSoftDesMerchContactInfo = pSoftDesMerchContactInfo;
	}

	/**
	 * Sets the soft des mid.
	 *
	 * @param pSoftDesMid the softDesMid to set.
	 */
	public void setSoftDesMid(String pSoftDesMid) {
		mSoftDesMid = pSoftDesMid;
	}

	/**
	 * Sets the soft des postal code.
	 *
	 * @param pSoftDesPostalCode the softDesPostalCode to set.
	 */
	public void setSoftDesPostalCode(String pSoftDesPostalCode) {
		mSoftDesPostalCode = pSoftDesPostalCode;
	}

	/**
	 * Sets the soft des region.
	 *
	 * @param pSoftDesRegion the softDesRegion to set.
	 */
	public void setSoftDesRegion(String pSoftDesRegion) {
		mSoftDesRegion = pSoftDesRegion;
	}

	/**
	 * Sets the soft des street.
	 *
	 * @param pSoftDesStreet the softDesStreet to set.
	 */
	public void setSoftDesStreet(String pSoftDesStreet) {
		mSoftDesStreet = pSoftDesStreet;
	}

	/**
	 * Sets the sorting enabled.
	 *
	 * @param pSortingEnabled the sortingEnabled to set.
	 */
	public void setSortingEnabled(boolean pSortingEnabled) {
		mSortingEnabled = pSortingEnabled;
	}

	/**
	 * Sets the sos configured URLS.
	 *
	 * @param pSosConfiguredURLs the sos configured URLS.
	 */
	public void setSosConfiguredURLs(List<String> pSosConfiguredURLs) {
		mSosConfiguredURLs = pSosConfiguredURLs;
	}

	/**
	 * Sets the sos source order type.
	 *
	 * @param pSosSourceOrderType the sosSourceOrderType to set.
	 */
	public void setSosSourceOrderType(String pSosSourceOrderType) {
		mSosSourceOrderType = pSosSourceOrderType;
	}

	/**
	 * Sets/updates the storeNumber.
	 * @param pStoreNumber the storeNumber to set.
	 */
	public void setStoreNumber(String pStoreNumber) {
		mStoreNumber = pStoreNumber;
	}

	/**
	 * Sets the tax number.
	 *
	 * @param pTaxNumber the taxNumber to set.
	 */
	public void setTaxNumber(Map<String, String> pTaxNumber) {
		mTaxNumber = pTaxNumber;
	}

	/**
	 * Sets the time in transit ship method codes.
	 *
	 * @param pTimeInTransitShipMethodCodes the timeInTransitShipMethodCodes to set.
	 */
	public void setTimeInTransitShipMethodCodes(
			List<String> pTimeInTransitShipMethodCodes) {
		mTimeInTransitShipMethodCodes = pTimeInTransitShipMethodCodes;
	}

	/**
	 * Sets the token type.
	 *
	 * @param pTokenType the tokenType to set.
	 */
	public void setTokenType(Map<String, String> pTokenType) {
		mTokenType = pTokenType;
	}
	/**
	 * Sets the tRUCSS path.
	 *
	 * @param pTRUCSSPath the new tRUCSS path.
	 */
	public void setTRUCSSPath(String pTRUCSSPath) {
		this.mTRUCSSPath = pTRUCSSPath;
	}

	/**
	 * Sets the tRU image path.
	 *
	 * @param pTRUImagePath the new tRU image path.
	 */
	public void setTRUImagePath(String pTRUImagePath) {
		this.mTRUImagePath = pTRUImagePath;
	}

	/**
	 * Sets the tRUJS path.
	 *
	 * @param pTRUJSPath the new tRUJS path.
	 */
	public void setTRUJSPath(String pTRUJSPath) {
		this.mTRUJSPath = pTRUJSPath;
	}

	/**
	 * Sets the tru sku types.
	 * @param pTruSkuTypes the truSkuTypes to set.
	 */
	public void setTruSkuTypes(List<String> pTruSkuTypes) {
		mTruSkuTypes = pTruSkuTypes;
	}

	/**
	 * Sets the tru video game esrb.
	 *
	 * @param pTruVideoGameEsrb the mTruVideoGameEsrb to set.
	 */
	public void setTruVideoGameEsrb(List<String> pTruVideoGameEsrb) {
		mTruVideoGameEsrb = pTruVideoGameEsrb;
	}

	/**
	 * Sets the twelve month financing limit.
	 *
	 * @param pTwelveMonthFinancingLimit the twelveMonthFinancingLimit to set.
	 */
	public void setTwelveMonthFinancingLimit(double pTwelveMonthFinancingLimit) {
		mTwelveMonthFinancingLimit = pTwelveMonthFinancingLimit;
	}

	/**
	 * Sets the web source order type.
	 *
	 * @param pWebSourceOrderType the webSourceOrderType to set.
	 */
	public void setWebSourceOrderType(String pWebSourceOrderType) {
		mWebSourceOrderType = pWebSourceOrderType;
	}

	/**
	 * Sets the wishlist url.
	 * @param pWishlistUrl the new wishlist url.
	 */
	public void setWishlistUrl(String pWishlistUrl) {
		mWishlistUrl = pWishlistUrl;
	}

	/**
	 * Gets the billingNickNameLength.
	 *
	 * @return mBillingNickNameLength
	 */
	public int getBillingNickNameLength() {
		return mBillingNickNameLength;
	}

	/**
	 * Sets the billing nick name length.
	 *
	 * @param pBillingNickNameLength the billingNickNameLength to set
	 */
	public void setBillingNickNameLength(int pBillingNickNameLength) {
		this.mBillingNickNameLength = pBillingNickNameLength;
	}

	/** The Soft des city name. */
	private String mSoftDesCityName;

	/**
	 * @return the softDesCityName
	 */
	public String getSoftDesCityName() {
		return mSoftDesCityName;
	}

	/**
	 * @param pSoftDesCityName the softDesCityName to set
	 */
	public void setSoftDesCityName(String pSoftDesCityName) {
		mSoftDesCityName = pSoftDesCityName;
	}
	
	/**
	 * Gets the preSellDateFormat.
	 *
	 * @return mPreSellDateFormat
	 */
	public String getPreSellDateFormat() {
		return mPreSellDateFormat;
	}

	/**
	 * Sets the pre sell date format.
	 *
	 * @param pPreSellDateFormat the preSellDateFormat to set
	 */
	public void setPreSellDateFormat(String pPreSellDateFormat) {
		this.mPreSellDateFormat = pPreSellDateFormat;
	}

	/**
	 * Gets the attr property type product.
	 *
	 * @return the attrPropertyTypeProduct
	 */
	public String getAttrPropertyTypeProduct() {
		return mAttrPropertyTypeProduct;
	}

	/**
	 * Sets the attr property type product.
	 *
	 * @param pAttrPropertyTypeProduct the attrPropertyTypeProduct to set
	 */
	public void setAttrPropertyTypeProduct(String pAttrPropertyTypeProduct) {
		mAttrPropertyTypeProduct = pAttrPropertyTypeProduct;
	}

	/**
	 * Gets the attr property type sku.
	 *
	 * @return the attrPropertyTypeSku
	 */
	public String getAttrPropertyTypeSku() {
		return mAttrPropertyTypeSku;
	}

	/**
	 * Sets the attr property type sku.
	 *
	 * @param pAttrPropertyTypeSku the attrPropertyTypeSku to set
	 */
	public void setAttrPropertyTypeSku(String pAttrPropertyTypeSku) {
		mAttrPropertyTypeSku = pAttrPropertyTypeSku;
	}
	
	/**
	 * Gets the sos bru norton seal url.
	 *
	 * @return the sosBruNortonSealURL
	 */
	public String getSosBruNortonSealURL() {
		return mSosBruNortonSealURL;
	}
	
	/**
	 * Sets the sos bru norton seal url.
	 *
	 * @param pSosBruNortonSealURL the sosBruNortonSealURL to set
	 */
	public void setSosBruNortonSealURL(String pSosBruNortonSealURL) {
		mSosBruNortonSealURL = pSosBruNortonSealURL;
	}
	/**
	 *
	 * @return the DetailsURL
	 */
	public String getDetailsURL() {
		return mDetailsURL;
	}
	
	/**
	 *
	 * @param pDetailsURL the DetailsURL to set
	 */
	public void setDetailsURL(String pDetailsURL) {
		mDetailsURL = pDetailsURL;
	}
	/**
	 * Gets the sos tru norton seal url.
	 *
	 * @return the sosTruNortonSealURL
	 */
	public String getSosTruNortonSealURL() {
		return mSosTruNortonSealURL;
	}
	
	/**
	 * Sets the sos tru norton seal url.
	 *
	 * @param pSosTruNortonSealURL the sosTruNortonSealURL to set
	 */
	public void setSosTruNortonSealURL(String pSosTruNortonSealURL) {
		mSosTruNortonSealURL = pSosTruNortonSealURL;
	}

	/**
	 * Gets the sos cookie names.
	 *
	 * @return the sosCookieNames
	 */
	public List<String> getSosCookieNames() {
		return mSosCookieNames;
	}

	/**
	 * Sets the sos cookie names.
	 *
	 * @param pSosCookieNames the sosCookieNames to set
	 */
	public void setSosCookieNames(List<String> pSosCookieNames) {
		mSosCookieNames = pSosCookieNames;
	}

	/**
	 * Gets the merchant group id.
	 *
	 * @return the mMerchantGroupId
	 */
	public String getMerchantGroupId() {
		return mMerchantGroupId;
	}

	/**
	 * Sets the merchant group id.
	 *
	 * @param pMerchantGroupId the MerchantGroupId to set
	 */
	public void setMerchantGroupId(String pMerchantGroupId) {
		this.mMerchantGroupId = pMerchantGroupId;
	}

	/**
	 * Gets the default synchrony card holder.
	 *
	 * @return the mDefaultSynchronyCardHolder
	 */
	public String getDefaultSynchronyCardHolder() {
		return mDefaultSynchronyCardHolder;
	}

	/**
	 * Sets the default synchrony card holder.
	 *
	 * @param pDefaultSynchronyCardHolder the mDefaultSynchronyCardHolder to set
	 */
	public void setDefaultSynchronyCardHolder(
			String pDefaultSynchronyCardHolder) {
		this.mDefaultSynchronyCardHolder = pDefaultSynchronyCardHolder;
	}

	/**
	 * Gets the days to purge single use coupon.
	 *
	 * @return the days to purge single use coupon
	 */
	public int getDaysToPurgeSingleUseCoupon() {
		return mDaysToPurgeSingleUseCoupon;
	}

	/**
	 * Sets the days to purge single use coupon.
	 *
	 * @param pDaysToPurgeSingleUseCoupon the new days to purge single use coupon
	 */
	public void setDaysToPurgeSingleUseCoupon(int pDaysToPurgeSingleUseCoupon) {
		this.mDaysToPurgeSingleUseCoupon = pDaysToPurgeSingleUseCoupon;
	}

	/**
	 * Gets the default sos store number.
	 *
	 * @return the default sos store number
	 */
	public String getDefaultSOSStoreNumber() {
		return mDefaultSOSStoreNumber;
	}

	/**
	 * Sets the default sos store number.
	 *
	 * @param pDefaultSOSStoreNumber the new default sos store number
	 */
	public void setDefaultSOSStoreNumber(String pDefaultSOSStoreNumber) {
		mDefaultSOSStoreNumber = pDefaultSOSStoreNumber;
	}

	/**
	 * Gets the defaultsos emp number.
	 *
	 * @return the defaultsos emp number
	 */
	public String getDefaultsosEmpNumber() {
		return mDefaultsosEmpNumber;
	}

	/**
	 * Sets the defaultsos emp number.
	 *
	 * @param pDefaultsosEmpNumber the new defaultsos emp number
	 */
	public void setDefaultsosEmpNumber(String pDefaultsosEmpNumber) {
		mDefaultsosEmpNumber = pDefaultsosEmpNumber;
	}

	/**
	 * Gets the sos cookieredirection url.
	 *
	 * @return the sos cookieredirection url
	 */
	public Map<String, String> getSosCookieredirectionURL() {
		return mSosCookieredirectionURL;
	}

	/**
	 * Sets the sos cookieredirection url.
	 *
	 * @param pSosCookieredirectionURL the m sos cookieredirection url
	 */
	
	
	public void setSosCookieredirectionURL(Map<String, String> pSosCookieredirectionURL) {
		this.mSosCookieredirectionURL = pSosCookieredirectionURL;
	}

	/**
	 * Gets the sos mSosErrorUrl url.
	 *
	 * @return the sos mSosErrorUrl url
	 */
	public String getSosErrorUrl() {
		return mSosErrorUrl;
	}

	/**
	 * Sets the sos cookieredirection url.
	 *
	 * @param pSosErrorUrl the m sos cookieredirection url
	 */
	public void setSosErrorUrl(String pSosErrorUrl) {
		mSosErrorUrl = pSosErrorUrl;
	}
	
	/** The Test address doctor. */
	private boolean mTestAddressDoctor;
	
	/** The m tender eligibility check. */
	private boolean mTenderEligibilityCheck;

	/**
	 * Checks if is tender eligibility check.
	 *
	 * @return true, if is tender eligibility check
	 */
	public boolean isTenderEligibilityCheck() {
		return mTenderEligibilityCheck;
	}

	/**
	 * Sets the tender eligibility check.
	 *
	 * @param pTenderEligibilityCheck the new tender eligibility check
	 */
	public void setTenderEligibilityCheck(boolean pTenderEligibilityCheck) {
		this.mTenderEligibilityCheck = pTenderEligibilityCheck;
	}

	/**
	 * @return the previewPromoEnableInStaging
	 */
	public boolean isPreviewPromoEnableInStaging() {
		return mPreviewPromoEnableInStaging;
	}

	/**
	 * @param pPreviewPromoEnableInStaging the previewPromoEnableInStaging to set
	 */
	public void setPreviewPromoEnableInStaging(boolean pPreviewPromoEnableInStaging) {
		mPreviewPromoEnableInStaging = pPreviewPromoEnableInStaging;
	}

	/**
	 * Gets the recaptcha url.
	 *
	 * @return mRecaptchaURL
	 */
	public String getRecaptchaURL() {
		return mRecaptchaURL;
	}

	/**
	 * Sets the recaptcha url.
	 *
	 * @param pRecaptchaURL - String
	 */
	public void setRecaptchaURL(String pRecaptchaURL) {
		mRecaptchaURL = pRecaptchaURL;
	}

	/**
	 * @return the mStoreLocatorGoogleMapURL
	 */
	public String getStoreLocatorGoogleMapURL() {
		return mStoreLocatorGoogleMapURL;
	}

	/**
	 * Sets the store locator google map URL.
	 *
	 * @param pStoreLocatorGoogleMapURL the new store locator google map URL
	 */
	public void setStoreLocatorGoogleMapURL(String pStoreLocatorGoogleMapURL) {
		this.mStoreLocatorGoogleMapURL = pStoreLocatorGoogleMapURL;
	}

	/**
	 * @return the mTestAddressDoctor
	 */
	public boolean isTestAddressDoctor() {
		return mTestAddressDoctor;
	}

	/**
	 * @param pTestAddressDoctor the pTestAddressDoctor to set
	 */
	public void setTestAddressDoctor(boolean pTestAddressDoctor) {
		this.mTestAddressDoctor = pTestAddressDoctor;
	}

	/**
	 * @return the enableApplePayMockAddresses
	 */
	public boolean isEnableApplePayAddresses() {
		return mEnableApplePayAddresses;
	}

	/**
	 * @param pEnableApplePayAddresses the enableApplePayAddresses to set
	 */
	public void setEnableApplePayAddresses(boolean pEnableApplePayAddresses) {
		mEnableApplePayAddresses = pEnableApplePayAddresses;
	}

	/**
	 * @return the applePayAddress1
	 */
	public String getApplePayAddress1() {
		return mApplePayAddress1;
	}

	/**
	 * @param pApplePayAddress1 the applePayAddress1 to set
	 */
	public void setApplePayAddress1(String pApplePayAddress1) {
		mApplePayAddress1 = pApplePayAddress1;
	}

	/**
	 * @return the applePayFirstName
	 */
	public String getApplePayFirstName() {
		return mApplePayFirstName;
	}

	/**
	 * @param pApplePayFirstName the applePayFirstName to set
	 */
	public void setApplePayFirstName(String pApplePayFirstName) {
		mApplePayFirstName = pApplePayFirstName;
	}

	/**
	 * @return the applePayLastName
	 */
	public String getApplePayLastName() {
		return mApplePayLastName;
	}

	/**
	 * @param pApplePayLastName the applePayLastName to set
	 */
	public void setApplePayLastName(String pApplePayLastName) {
		mApplePayLastName = pApplePayLastName;
	}

	/**
	 * @return the mHeaderparamForIpAddress
	 */
	public String getHeaderparamForIpAddress() {
		return mHeaderparamForIpAddress;
	}

	/**
	 * Sets the headerparam for ip address.
	 *
	 * @param pHeaderparamForIpAddress the new headerparam for ip address
	 */
	public void setHeaderparamForIpAddress(String pHeaderparamForIpAddress) {
		this.mHeaderparamForIpAddress = pHeaderparamForIpAddress;
	}

	/**
	 * @return the mStoreLocatorStaticGoogleMapURL
	 */
	public String getStoreLocatorStaticGoogleMapURL() {
		return mStoreLocatorStaticGoogleMapURL;
	}

	/**
	 * @param pStoreLocatorStaticGoogleMapURL the mStoreLocatorStaticGoogleMapURL to set
	 */
	public void setStoreLocatorStaticGoogleMapURL(
			String pStoreLocatorStaticGoogleMapURL) {
		this.mStoreLocatorStaticGoogleMapURL = pStoreLocatorStaticGoogleMapURL;
	}

	/**
	 * @return the akamaiSiteParam
	 */
	public String getAkamaiSiteParam() {
		return mAkamaiSiteParam;
	}

	/**
	 * @param pAkamaiSiteParam the akamaiSiteParam to set
	 */
	public void setAkamaiSiteParam(String pAkamaiSiteParam) {
		mAkamaiSiteParam = pAkamaiSiteParam;
	}
	
	
	/** The m save billing address. */
	private boolean mSaveBillingAddress;

	/**
	 * Checks if is save billing address.
	 *
	 * @return the mSaveBillingAddress
	 */
	public boolean isSaveBillingAddress() {
		return mSaveBillingAddress;
	}

	/**
	 * Sets the save billing address.
	 *
	 * @param pSaveBillingAddress the new save billing address
	 */
	public void setSaveBillingAddress(boolean pSaveBillingAddress) {
		this.mSaveBillingAddress = pSaveBillingAddress;
	}

	/**
	 * @return the mStoreLocatorGetDirectionsGoogleMapURL
	 */
	public String getStoreLocatorGetDirectionsGoogleMapURL() {
		return mStoreLocatorGetDirectionsGoogleMapURL;
	}

	/**
	 * @param pStoreLocatorGetDirectionsGoogleMapURL the mStoreLocatorGetDirectionsGoogleMapURL to set
	 */
	public void setStoreLocatorGetDirectionsGoogleMapURL(
			String pStoreLocatorGetDirectionsGoogleMapURL) {
		this.mStoreLocatorGetDirectionsGoogleMapURL = pStoreLocatorGetDirectionsGoogleMapURL;
	}
	/**
	 * @return the mMerchantId
	 */
	public String getMerchantId() {
		return mMerchantId;
	}
	/**
	 * @param pMerchantId the mMerchantId to set
	 */
	public void setMerchantId(String pMerchantId) {
		this.mMerchantId = pMerchantId;
	}

	/**
	 * @return the mEnableSkuPricingCache
	 */
	public boolean isEnableSkuPricingCache() {
		return mEnableSkuPricingCache;
	}

	/**
	 * @param pEnableSkuPricingCache the mEnableSkuPricingCache to set
	 */
	public void setEnableSkuPricingCache(boolean pEnableSkuPricingCache) {
		this.mEnableSkuPricingCache = pEnableSkuPricingCache;
	}
	
	/**
	 * @return the mEnableSiteCookieCreationInJS
	 */
	public boolean isEnableSiteCookieCreationInJS() {
		return mEnableSiteCookieCreationInJS;
	}

	/**
	 * @param pEnableSiteCookieCreationInJS the mEnableSiteCookieCreationInJS to set
	 */
	public void setEnableSiteCookieCreationInJS(boolean pEnableSiteCookieCreationInJS) {
		this.mEnableSiteCookieCreationInJS = pEnableSiteCookieCreationInJS;
	}

	/** The Append schema for synchrony return url. */
	private boolean mAppendSchemaForSynchronyReturnUrl;

	/**
	 * @return the appendSchemaForSynchronyReturnUrl
	 */
	public boolean isAppendSchemaForSynchronyReturnUrl() {
		return mAppendSchemaForSynchronyReturnUrl;
	}

	/**
	 * @param pAppendSchemaForSynchronyReturnUrl the appendSchemaForSynchronyReturnUrl to set
	 */
	public void setAppendSchemaForSynchronyReturnUrl(
			boolean pAppendSchemaForSynchronyReturnUrl) {
		mAppendSchemaForSynchronyReturnUrl = pAppendSchemaForSynchronyReturnUrl;
	}
	
	/**
	 * Checks if is CustomSlaveEnable required.
	 *
	 * @return true, if is CustomSlaveEnable required
	 */
	public boolean isCustomSlaveEnable() {
		return mCustomSlaveEnable;
	}

	/**
	 * Sets the CustomSlaveEnable required.
	 *
	 * @param pCustomSlaveEnable the CustomSlaveEnable required
	 */
	public void setCustomSlaveEnable(boolean pCustomSlaveEnable) {
		mCustomSlaveEnable = pCustomSlaveEnable;
	}

	/**
	 * @return the mEnableGeoValidationfromClientId
	 */
	public boolean isEnableGeoValidationfromClientId() {
		return mEnableGeoValidationfromClientId;
	}

	/**
	 * @param pEnableGeoValidationfromClientId the mEnableGeoValidationfromClientId to set
	 */
	public void setEnableGeoValidationfromClientId(boolean pEnableGeoValidationfromClientId) {
		mEnableGeoValidationfromClientId = pEnableGeoValidationfromClientId;
	}
	
	
	
	/**
	 * @return the mProjectNameWithIdRestServiceURL
	 */
	public String getProjectNameWithIdRestServiceURL() {
		return mProjectNameWithIdRestServiceURL;
	}

	/**
	 * @param pProjectNameWithIdRestServiceURL the mProjectNameWithIdRestServiceURL to set
	 */
	public void setProjectNameWithIdRestServiceURL(
			String pProjectNameWithIdRestServiceURL) {
		this.mProjectNameWithIdRestServiceURL = pProjectNameWithIdRestServiceURL;
	}
	
	/**
	 * @return the mValidateAgainstProjectId
	 */
	public boolean isValidateAgainstProjectId() {
		return mValidateAgainstProjectId;
	}

	/**
	 * @param pValidateAgainstProjectId the mValidateAgainstProjectId to set
	 */
	public void setValidateAgainstProjectId(boolean pValidateAgainstProjectId) {
		this.mValidateAgainstProjectId = pValidateAgainstProjectId;
	}
	
	/**
	 * Gets the catalog feed project name list.
	 *
	 * @return the catalog feed project name list
	 */
	public List<String> getCatalogFeedProjectNameList() {
		return mCatalogFeedProjectNameList;
	}
	
	/**
	 * Sets the catalog feed project name list.
	 *
	 * @param pCatalogFeedProjectNameList the new catalog feed project name list
	 */
	public void setCatalogFeedProjectNameList(List<String> pCatalogFeedProjectNameList) {
		this.mCatalogFeedProjectNameList = pCatalogFeedProjectNameList;
	}
}