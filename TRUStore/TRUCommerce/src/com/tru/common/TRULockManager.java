package com.tru.common;

import java.util.concurrent.ConcurrentHashMap;

import com.tru.common.TRUGenerateDebugId.LockDebugId;
import com.tru.common.TRUGenerateDebugId.LockDebugIdManager;

import atg.commerce.util.NoLockNameException;
import atg.commerce.util.TransactionLockFactory;
import atg.commerce.util.TransactionLockService;
import atg.nucleus.GenericService;
import atg.service.lockmanager.DeadlockException;
import atg.service.lockmanager.LockManagerException;


/**
 *  This class consists of the methods to acquire and release locks on the profile
 *  
 * @author Professional Access
 * @version 1.0
 */
public class TRULockManager extends GenericService {

	/** Property to hold TransactionLockFactory.*/
	private TransactionLockFactory mTransactionLockFactory;
	private boolean mEnableLockdebug;

	public static LockDebugIdManager DEBUG_MANAGER = LockDebugIdManager.MANAGER;
	
	public static ConcurrentHashMap<LockDebugId, Integer> lockDebugIdMap = new ConcurrentHashMap<>();

	/**
	 * This method gets transactionLockFactory.
	 * @return the transactionLockFactory
	 */
	public TransactionLockFactory getTransactionLockFactory() {
		return mTransactionLockFactory;
	}

	/**
	 * This method sets transactionLockFactory.
	 * @param pTransactionLockFactory the transactionLockFactory to set
	 */
	public void setTransactionLockFactory(TransactionLockFactory pTransactionLockFactory) {
		mTransactionLockFactory = pTransactionLockFactory;
	}	

	/**
	 * This method acquires lock for current profile.
	 * @return the lock service.
	 * @throws NoLockNameException - the NoLockNameException.
	 * @throws DeadlockException - the DeadlockException.
	 */
	public TransactionLockService acquireLock() throws NoLockNameException, DeadlockException {

		if (isLoggingDebug()) {
			logDebug("START TRULockManager : acquireLock");
		}

		TransactionLockFactory tlf = getTransactionLockFactory();
		
		TransactionLockService lockService = null;
		if (tlf != null) {
			lockService = tlf.getServiceInstance(this);
		} else if (isLoggingWarning()) {
			logWarning("Transaction is missing");
		}
		
		if (lockService != null) {
			if (isLoggingDebug()) {
				logDebug("Lock Name : " + lockService.getLockName());
			}
			
			if (isEnableLockdebug())
				DEBUG_MANAGER.acquireLock(this, lockService);
			else 
				lockService.acquireTransactionLock();
		}
		
		if (isLoggingDebug()) {
	
			logDebug("END TRULockManager : acquireLock");
		}
		return lockService;
	}

	/**
	 * This method releases lock for current profile.
	 * @param pLockService - the lockService which needs to be released.
	 */
	public void releaseLock(TransactionLockService pLockService) {
		if (isLoggingDebug()) {
			logDebug("START TRULockManager : releaseLock");
		}
		
		try {
			if (pLockService != null) {
				pLockService.releaseTransactionLock();
				if (isEnableLockdebug())
					DEBUG_MANAGER.releaseLock(this, pLockService);
				else 
					pLockService.releaseTransactionLock();
				
			} 
		} catch (LockManagerException lme) {
			if (isLoggingError()) {
				logError("LockManagerException in releaseLock", lme);
			}
		} 
		if (isLoggingDebug()) {
			logDebug("END TRULockManager : releaseLock");
		}
	}

	public boolean isEnableLockdebug() {
		return mEnableLockdebug;
	}

	public void setEnableLockdebug(boolean pEnableLockdebug) {
		mEnableLockdebug = pEnableLockdebug;
	}

	
}
