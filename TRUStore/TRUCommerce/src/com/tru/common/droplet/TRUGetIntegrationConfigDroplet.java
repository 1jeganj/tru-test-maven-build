package com.tru.common.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.TRUConstants;
import com.tru.common.TRUIntegrationConfiguration;


/** This droplet is used to read all the service integration value.
 * @author Professional Access
 * @version 1.0
 */
public class TRUGetIntegrationConfigDroplet extends DynamoServlet {
	
	/**
	 * Holds the property value of mTruIntegrationConfiguration.
	 */	
	private TRUIntegrationConfiguration mTruIntegrationConfiguration;

	/**
	 * @return the truIntegrationConfiguration.
	 */
	public TRUIntegrationConfiguration getTruIntegrationConfiguration() {
		return mTruIntegrationConfiguration;
	}

	/**
	 * @param pTruIntegrationConfiguration the truIntegrationConfiguration to set.
	 */
	public void setTruIntegrationConfiguration(TRUIntegrationConfiguration pTruIntegrationConfiguration) {
		mTruIntegrationConfiguration = pTruIntegrationConfiguration;
	}

	/**
	 * This method is used to get the sos user details and populate in the header sections.
	 * 
	 * @param pRequest
	 *            the request.
	 * @param pResponse
	 *            the response.
	 * @throws ServletException
	 *             the servlet exception.
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */

	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUSOSGetIntegrationConfigDroplet.service :: START");
		}
		pRequest.setParameter(TRUConstants.IS_ENABLE_ADDRESS_DOCTOR, getTruIntegrationConfiguration().isEnableAddressDoctor());			
		pRequest.setParameter(TRUConstants.IS_ENABLE_COUPON_SERVICE, getTruIntegrationConfiguration().isEnableCouponService());		
		pRequest.setParameter(TRUConstants.IS_ENABLE_EPSLON, getTruIntegrationConfiguration().isEnableEpslon());		
		pRequest.setParameter(TRUConstants.IS_ENABLE_HOOKLOGIC, getTruIntegrationConfiguration().isEnableHookLogic());
		pRequest.setParameter(TRUConstants.IS_ENABLE_POWER_REVIEWS, getTruIntegrationConfiguration().isEnablePowerReviews());		
		pRequest.serviceLocalParameter(TRUConstants.OUTPUT_OPARAM, pRequest, pResponse);		
		if (isLoggingDebug()) {
			logDebug("Exiting  method TRUSOSGetIntegrationConfigDroplet.service :: END");
		}
	}	
}
