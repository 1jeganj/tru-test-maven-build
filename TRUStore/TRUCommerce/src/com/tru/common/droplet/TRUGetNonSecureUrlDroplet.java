/*
 * @(#)TRUGetNonSecureUrlDroplet.java
 *
 * Copyright PA, Inc. All rights reserved.
 * PA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.tru.common.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.service.dynamo.Configuration;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.TRUStoreConfiguration;
import com.tru.utils.SecureURLUtils;


/**
 * This droplet is used to get secure url.
 *
 * @author PA.
 * @version 1.0
 *
 */
public class TRUGetNonSecureUrlDroplet extends DynamoServlet {

	/**
	 * Constant for OUTPUT.
	 */
	public static final ParameterName OUTPUT = ParameterName.getParameterName("output");

	/**
	 * Constant for EMPTY.
	 */
	public static final ParameterName EMPTY = ParameterName.getParameterName("empty");

	/**
	 * Constant for REDIRECTION_PATH.
	 */
	public static final ParameterName URL_PATH = ParameterName.getParameterName("urlPath");

	/**
	 * Constant for REDIRECTION_URL.
	 */
	public static final String NON_SECURE_URL = "nonSecureUrl";

	/**
	 * Constant for HOST_NAME.
	 */
	public static final ParameterName HOST_NAME = ParameterName.getParameterName("hostName");

	/**
	 * Holds the configuration.
	 */
	private Configuration mConfiguration;

	/**
	 * Holds the tru store configuration.
	 */
	private TRUStoreConfiguration mTruStoreConfiguration;

	/**
	 * Returns the holds the configuration.
	 * 
	 * @return the configuration.
	 */
	public Configuration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * Sets the holds the configuration.
	 * 
	 * @param pConfiguration
	 *            the configuration to set.
	 */
	public void setConfiguration(Configuration pConfiguration) {
		mConfiguration = pConfiguration;
	}

	/**
	 * Returns the holds the tru store configuration.
	 * 
	 * @return the truStoreConfiguration.
	 */
	public TRUStoreConfiguration getTruStoreConfiguration() {
		return mTruStoreConfiguration;
	}

	/**
	 * Sets the holds the tru store configuration.
	 * 
	 * @param pTruStoreConfiguration
	 *            the truStoreConfiguration to set.
	 */
	public void setTruStoreConfiguration(TRUStoreConfiguration pTruStoreConfiguration) {
		mTruStoreConfiguration = pTruStoreConfiguration;
	}

	/**
	 * This method used get the non secure url for a given path.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest.
	 * @param pResponse
	 *            DynamoHttpServletResponse.
	 * @throws ServletException
	 *             if any servlet exception occurs.
	 * @throws IOException
	 *             if any i/o Exception occurs.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("START::TRUGetNonSecureUrlDroplet ::service");
		}
		String urlPath = (String) pRequest.getLocalParameter(URL_PATH);
		String hostName = (String) pRequest.getLocalParameter(HOST_NAME);
		String nonSecureUrl = null;
		if (!StringUtils.isBlank(urlPath) && !StringUtils.isBlank(hostName)) {
			nonSecureUrl =
					SecureURLUtils.nonSecureURL(
							hostName,
							getConfiguration().getHttpPort(),
							urlPath,
							false);
		}
		if (isLoggingDebug()) {
			logDebug("END::TRUGetNonSecureUrlDroplet ::service");
			logDebug("Redirectional URL::" + nonSecureUrl);
		}
		if (!StringUtils.isBlank(nonSecureUrl)) {
			pRequest.setParameter(NON_SECURE_URL, nonSecureUrl);
			pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
		} else {
			pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
		}

	}

}
