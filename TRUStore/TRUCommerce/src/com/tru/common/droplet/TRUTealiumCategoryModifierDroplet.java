package com.tru.common.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.TRUConstants;

/**
 * This class holds the business logic for constructing the breadcrumb .
 * 
 * @author Professional Access
 * @version 1.0
 */

public class TRUTealiumCategoryModifierDroplet extends DynamoServlet {	

	
	
	/**
     * This method will get the category Name  and modify it by removing spaces and special characters and convert it to lower case.
     * @param pRequest - DynamoHttpServletRequest
	 * @param pResponse - DynamoHttpServletResponse
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException 
     */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if(isLoggingDebug()){
			logDebug("TRUTealiumCategoryModifierDroplet.service(DynamoHttpServletRequest, DynamoHttpServletResponse) :: Starts");
		}
		String catName = (String) pRequest.getLocalParameter(TRUConstants.CATETORY_NAME);
		String pageName = (String) pRequest.getLocalParameter(TRUConstants.PAGE_NAME);
		if(isLoggingDebug()){
			logDebug("categoryName in TRUTealiumCategoryModifierDroplet.service() :: "+catName);
		}
		
		if(!StringUtils.isBlank(catName) && catName.contains(TRUConstants.CHARACTERS_THEMES)){
			catName = catName.replaceAll(TRUConstants.FORWARD_SLASH,TRUConstants.EMPTY);
			catName = TRUConstants.FORWARD_SLASH.concat(catName);
		}
		if(!StringUtils.isBlank(catName)){
			try {				
					String catNameWithoutSpecialChars = catName.replaceAll(TRUConstants.SPECIAL_CHAR_REG_EX,TRUConstants.EMPTY);
					String catNameWithoutSpaces = StringUtils.removeWhiteSpace(catNameWithoutSpecialChars);
					String catNameWithLowerCase = StringUtils.toLowerCase(catNameWithoutSpaces);
					
					String[] actualBreadCrumbs = catNameWithLowerCase.split(TRUConstants.FORWARD_SLASH);
					String breadCrumbLevels=TRUConstants.EMPTY;
					
					if(actualBreadCrumbs!=null && actualBreadCrumbs.length>TRUConstants.INTEGER_NUMBER_ONE)
					{
					int breadCrumbLength = actualBreadCrumbs.length;
					int maxBreadCrumbLength= TRUConstants.SIX;
						for (int i=1;i<breadCrumbLength;i++)
						{
							Integer intIndex= i;
							String index = intIndex.toString();
							breadCrumbLevels = breadCrumbLevels.concat(TRUConstants.LEVEL_STRING).concat(index).
									concat(TRUConstants.EQUALS_SYMBOL_STRING).concat(actualBreadCrumbs[i]).concat(TRUConstants.AMPERSAND_STRING);
						}
						for (int i=breadCrumbLength;i<maxBreadCrumbLength;i++){
							
							Integer intIndex1 = i;
							String index1 = intIndex1.toString();
							if((i==breadCrumbLength) && (pageName!=null && (!pageName.isEmpty()))){
								breadCrumbLevels = breadCrumbLevels.concat(TRUConstants.LEVEL_STRING).concat(index1).
										concat(TRUConstants.EQUALS_SYMBOL_STRING).concat(pageName).concat(TRUConstants.AMPERSAND_STRING);
								}else{
									breadCrumbLevels = breadCrumbLevels.concat(TRUConstants.LEVEL_STRING).concat(index1).
											concat(TRUConstants.EQUALS_SYMBOL_STRING).concat(TRUConstants.NULL_STRING).
											concat(TRUConstants.AMPERSAND_STRING);
							}	
						}
						breadCrumbLevels = breadCrumbLevels.concat(TRUConstants.LEVEL_STRING).concat(TRUConstants.STRING_SIX).
								concat(TRUConstants.EQUALS_SYMBOL_STRING).concat(TRUConstants.NULL_STRING);						
					}					
					pRequest.setParameter(TRUConstants.MODIFIED_CATEGORY, catNameWithLowerCase);
					pRequest.setParameter(TRUConstants.BREADCRUMB_LEVELS, breadCrumbLevels);
					pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);								

			} catch (NumberFormatException exp) {
				if(isLoggingError()){
					logError("NumberFormatException in TRUTealiumCategoryModifierDroplet.service()", exp);
				}
			}
		}
		
		if(isLoggingDebug()){
			logDebug("TRUTealiumCategoryModifierDroplet.service(DynamoHttpServletRequest, DynamoHttpServletResponse) :: Ends");
		}
		
	}

}
