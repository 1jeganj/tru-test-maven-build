package com.tru.common.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;

import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.TRUConstants;

/** Display MonthAndYear Droplet for calculating next 20 years and setting to array.
 * @author Professional Access
 * @version 1.0
 */
public class DisplayMonthAndYearDroplet extends DynamoServlet {
	
	/**
	 * Holds the property value of month List.
	 */	
	public static final String MONTH_LIST = "monthList";
	
	/**
	 * Holds the property value of YEAR List.
	 */
	public static final String YEAR_LIST= "yearList";
	
	/**
	 * Constant to hold output.
	 */
	public static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
	
	/**
	 *  property to hold month list.
	 */
	
	private List<String> mMonthList;
	
	/**
	 * This method calculates the next 20 years and sets it to an array.
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
		    throws ServletException, IOException {
		vlogDebug("DisplayMonthAndYearDroplet Service() : STARTS");
		Calendar lCurrentCalender = Calendar.getInstance();   // Gets the current date and time
		int lCurrentYear = lCurrentCalender.get(Calendar.YEAR); 
		int lFutureYear = lCurrentYear + TRUConstants.TWENTY;
		List<Integer> lYearList = new ArrayList<Integer>();
		for (int i = lCurrentYear; i < lFutureYear; i++) {
			lYearList.add(i);
		}
		pRequest.setParameter(YEAR_LIST, lYearList);
		pRequest.setParameter(MONTH_LIST, getMonthList());
		pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest,pResponse);
		vlogDebug("DisplayMonthAndYearDroplet Service() : ENDS");
	}

	/** getter method for month list.
	 *  @return the mMonthList
	 */
	public List<String> getMonthList() {
		return mMonthList;
	}

	/** setter method for month list.
	 *  @param pMonthList the mMonthList to set
	 */
	public void setMonthList(List<String> pMonthList) {
		this.mMonthList = pMonthList;
	}
}
