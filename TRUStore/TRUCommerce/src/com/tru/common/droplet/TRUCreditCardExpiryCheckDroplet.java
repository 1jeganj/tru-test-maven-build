package com.tru.common.droplet;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;

import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.common.TRUConstants;

/**
 * This droplet checks if the credit card is expired or not.
 * @author Professional Access
 * @version	1.0
 */
public class TRUCreditCardExpiryCheckDroplet extends DynamoServlet {
		
	

	/**
	 * Constant to hold true oparam.
	 */
	public static final ParameterName TRUE_OPARAM = ParameterName.getParameterName("true");
	
	/**
	 * Constant to hold false oparam.
	 */
	public static final ParameterName FALSE_OPARAM = ParameterName.getParameterName("false");
	/**
	 * This method adds the servive parameter true/false based on the credit card is expired or not.
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			    throws ServletException, IOException {
		if (isLoggingDebug()) {	
		vlogDebug("TRUCreditCardExpiryCheckDroplet Service() : STARTS");
		}
			Calendar lCurrentCalender = Calendar.getInstance();   // Gets the current date and time
			int lCurrentYear = lCurrentCalender.get(Calendar.YEAR); 
			int lCurrentMonth = lCurrentCalender.get(Calendar.MONTH) + TRUCheckoutConstants.INT_ONE;
			int lCreditCardMonth = 0;
			String lMonth = (String) pRequest.getLocalParameter(TRUConstants.MONTH);
			if(null != lMonth){
				lCreditCardMonth = Integer.parseInt(lMonth);
			}
			int lCreditCardYear = 0;
			String lYear = (String) pRequest.getLocalParameter(TRUConstants.YEAR);
			if(null != lYear){
				lCreditCardYear = Integer.parseInt(lYear);
			}
			if(lCreditCardYear < lCurrentYear || ((lCreditCardYear == lCurrentYear) && (lCreditCardMonth < lCurrentMonth))){
				pRequest.serviceLocalParameter(TRUE_OPARAM, pRequest,pResponse);
			}else{
				pRequest.serviceLocalParameter(FALSE_OPARAM, pRequest,pResponse);
			}
			if (isLoggingDebug()) {
			vlogDebug("TRUCreditCardExpiryCheckDroplet Service() : ENDS");
			}
		}
}
