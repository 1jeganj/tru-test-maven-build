package com.tru.common.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;

import atg.adapter.gsa.ChangeAwareMap;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;

/**
 * This droplet display the last added cards in my account landing page.
 * @author Professional Access.
 * @version	1.0 .
 */
public class TRUCardDisplayDroplet extends DynamoServlet {
	/**
	 * holds output.
	 */
	public static final ParameterName OUTPUT = ParameterName.getParameterName(TRUConstants.OUTPUT);
	/**
	 * holds PROFILE_CREDIT_CARDS.
	 */
	private static final ParameterName PROFILE_CREDIT_CARDS = ParameterName.getParameterName(TRUConstants.CREDIT_CARDS);
	/**
	 * holds DEFAULT_CREDIT_CARD.
	 */
	private static final ParameterName DEFAULT_CREDIT_CARD = ParameterName.getParameterName(TRUConstants.DEFAULT_CREDIT_CARD);
	/**
	 * holds DEFAULT_CARD_EXISTS.
	 */
	private static final ParameterName DEFAULT_CARD_EXISTS = ParameterName.getParameterName(TRUConstants.DEFAULT_CARD_EXISTS);
	/**
	 * property to hold mTRUConfiguration.
	 */
	private TRUConfiguration mTRUConfiguration;

	/**
	 * This method adds the credit cards to be displayed in servive parameter .
	 * @param pRequest DynamoHttpServletRequest.
	 * @param pResponse DynamoHttpServletResponse.
	 * @throws ServletException this method is used to handle ServletException .
	 * @throws IOException this method is used to handle IOException.
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		@SuppressWarnings("unchecked")
		Map <String,RepositoryItem> creditCardsMap = (ChangeAwareMap) pRequest.getLocalParameter(PROFILE_CREDIT_CARDS);
		List<RepositoryItem> creditCards = new ArrayList<RepositoryItem>();
		RepositoryItem defaultCreditCard = (RepositoryItem)pRequest.getLocalParameter(DEFAULT_CREDIT_CARD);
		List <RepositoryItem> displayCreditCards = new ArrayList<RepositoryItem>();
		boolean defaultCardExists = Boolean.parseBoolean((String) pRequest.getLocalParameter(DEFAULT_CARD_EXISTS));
		Iterator<Entry<String, RepositoryItem>> entries = creditCardsMap.entrySet().iterator();
		while (entries.hasNext()) {
		    @SuppressWarnings("rawtypes")
			Map.Entry entry = (Map.Entry) entries.next();
		    creditCards.add((RepositoryItem) entry.getValue());
		}
		String cardsDisplayNumber = getTRUConfiguration().getDisplayNoOfCards();
		int cardsNumber = Integer.parseInt(cardsDisplayNumber);
		if(defaultCardExists){
			cardsNumber = cardsNumber-TRUConstants.SIZE_ONE;
		}
		int i=0;
		Collections.sort(creditCards, Collections.reverseOrder());
		for(RepositoryItem creditCard:creditCards){
			if((defaultCreditCard !=null && creditCard.getRepositoryId() != defaultCreditCard.getRepositoryId()) || (defaultCreditCard == null)){
				if(i == cardsNumber){
					break;
				} else{
					displayCreditCards.add(creditCard);
					i++;
				}
			}
		}
		
		pRequest.setParameter(TRUConstants.ELEMENT,displayCreditCards);
		pRequest.serviceParameter(OUTPUT, pRequest, pResponse);
	}

	/**
	 * @return the mTRUConfiguration.
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * @param pTRUConfiguration the mTRUConfiguration to set.
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		this.mTRUConfiguration = pTRUConfiguration;
	}

}
