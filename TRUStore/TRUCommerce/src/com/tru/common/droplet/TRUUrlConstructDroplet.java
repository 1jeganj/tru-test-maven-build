/*
 * @(#)TRUUrlConstructDroplet.java
 *
 * Copyright PA, Inc. All rights reserved.
 * PA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.tru.common.droplet;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.common.TRUConfiguration;


/** This droplet generates the absolute URL for the given relative URL.
 * @author Professional Access
 * @version 1.0
 */

public class TRUUrlConstructDroplet extends DynamoServlet {

	/**
	 * Constant for OUTPUT.
	 */
	public static final ParameterName OUTPUT = ParameterName.getParameterName("output");
	
	/**
	 * Nonsecure protocol constant.
	 */
	public static final String NONSECURE_PROTOCOL = "http";
	
	/**
	 * Secure protocol constant.
	 */
	public static final String SECURE_PROTOCOL = "https";
	
	/**
	 * Https Port constant.
	 */
	
	public static final String HTTP_PORT ="80";
	
	/**
	 * HTTPS Port constant.
	 */
	
	public static final String HTTPS_PORT ="443";
	
	/**
	 * Constant for EMPTY.
	 */
	public static final ParameterName EMPTY = ParameterName.getParameterName("empty");

	/**
	 * Constant for  ORDER_LIST.
	 */
	public static final ParameterName ORDER_LIST_PARAM = ParameterName.getParameterName("orderListParam");

	/**
	 * Constant for  ORDER_DETAIL.
	 */
	public static final ParameterName ORDER_DETAIL_PARAM = ParameterName.getParameterName("orderDetailParam");

	/**
	 * Constant for  ORDER_CANCEL.
	 */
	public static final ParameterName ORDER_CANCEL_PARAM = ParameterName.getParameterName("orderCancelParam");
	
	/**
	 * Constant for  INTEGRATION_LOGGING_PARAM.
	 */
	public static final ParameterName INTEGRATION_LOGGING_PARAM = ParameterName.getParameterName("integrationLoggingParam");

	/**
	 * The Constant ORIGIN_SCHEME_HEADER.
	 */
	public static final String ORIGIN_SCHEME_HEADER = "origin-scheme";
	
	/**
	 * Constant for  ORDER_LIST.
	 */
	public static final String ORDER_LIST = "orderList";

	/**
	 * Constant for  ORDER_DETAIL.
	 */
	public static final String ORDER_DETAIL ="orderDetail";

	/**
	 * Constant for  ORDER_CANCEL.
	 */
	public static final String ORDER_CANCEL = "orderCancel";
	
	/**
	 * Constant for  INTEGRATION_LOGGING.
	 */
	public static final String INTEGRATION_LOGGING = "integrationLogging";

	/**
	 * Constant for REDIRECTION_URL.
	 */
	public static final String ORDER_LIST_URL = "orderListURL";

	/**
	 * Constant for REDIRECTION_URL.
	 */
	public static final String ORDER_DETAIL_URL = "orderDetailURL";

	/**
	 * Constant for REDIRECTION_URL.
	 */
	public static final String ORDER_CANCEL_URL = "orderCancelURL";
	
	/**
	 * Constant for INTEGRATION_LOGGING_URL.
	 */
	public static final String INTEGRATION_LOGGING_URL = "integrationLoggingURL";

	/** enable scheme detection from header */
	protected  Boolean mEnableSchemeDetectionFromHeader;

	/**
	 * Holds the tru  configuration.
	 */
	private TRUConfiguration mTruConfiguration;
	
	/** property to hold mPortRequired. */
	private boolean mPortRequired;
	
	
	/**
	 * Checks if is port required.
	 *
	 * @return the portRequired.
	 */
	public boolean isPortRequired() {
		return mPortRequired;
	}
	
	/**
	 * Sets the port required.
	 *
	 * @param pPortRequired the portRequired to set.
	 */
	public void setPortRequired(boolean pPortRequired) {
		mPortRequired = pPortRequired;
	}


	/**
	 * Returns the holds the tru configuration.
	 * 
	 * @return the truConfiguration
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * Sets the holds the tru configuration.
	 * 
	 * @param pTruConfiguration
	 *            the truConfiguration to set
	 */
	public void setTruConfiguration(TRUConfiguration pTruConfiguration) {
		mTruConfiguration = pTruConfiguration;
	}
	
	
	/**
	 * @return the enabled status.
	 */
	public  Boolean getEnableSchemeDetectionFromHeader() {
		return mEnableSchemeDetectionFromHeader;
	}

	/**
	 * @param pEnabled - the enabled status to set.
	 */
	public void setEnableSchemeDetectionFromHeader(Boolean pEnabled) {
		mEnableSchemeDetectionFromHeader = pEnabled;
	}
	
	/**
	 * This method used get the absolute url for the given input parameter
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             if any servlet exception occurs
	 * @throws IOException
	 *             if any i/o Exception occurs.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("START::TRUUrlConstructorDroplet ::service");
		}
		String orderList = (String) pRequest.getLocalParameter(ORDER_LIST_PARAM);
		String orderDetail = (String) pRequest.getLocalParameter(ORDER_DETAIL_PARAM);
		String orderCancel = (String) pRequest.getLocalParameter(ORDER_CANCEL_PARAM);
		String integrationLogging = (String) pRequest.getLocalParameter(INTEGRATION_LOGGING_PARAM);
		
		if (!StringUtils.isBlank(orderList) && orderList.equalsIgnoreCase(ORDER_LIST)) {
			String orderListURL = getConstructedUrl(pRequest, getTruConfiguration().getOmsOrderListRestAPIURL(), null);
			if(!StringUtils.isBlank(orderListURL)){
				if (isLoggingDebug()) {
					logDebug("Order List URL::" + orderListURL);
				}
				pRequest.setParameter(ORDER_LIST_URL,orderListURL);
			}
		}

		if (!StringUtils.isBlank(orderDetail) && orderDetail.equalsIgnoreCase(ORDER_DETAIL)) {
			String orderDetailURL = getConstructedUrl(pRequest, getTruConfiguration().getOmsOrderDetailRestAPIURL(), null);
			if(!StringUtils.isBlank(orderDetailURL)){
				if (isLoggingDebug()) {
					logDebug("Order Detail URL::" + orderDetailURL);
				}
				pRequest.setParameter(ORDER_DETAIL_URL,orderDetailURL);
			}
		}

		if (!StringUtils.isBlank(orderCancel) && orderCancel.equalsIgnoreCase(ORDER_CANCEL)) {
			String orderCancelURL = getConstructedUrl(pRequest, getTruConfiguration().getOmsCancelOrderRestAPIURL(), null);
			if(!StringUtils.isBlank(orderCancelURL)){
				if (isLoggingDebug()) {
					logDebug("Order Cancel URL::" + orderCancelURL);
				}
				pRequest.setParameter(ORDER_CANCEL_URL,orderCancelURL);
			}
		}
		
		if (!StringUtils.isBlank(integrationLogging) && integrationLogging.equalsIgnoreCase(INTEGRATION_LOGGING)) {
			String integrationLoggingURL = getConstructedUrl(pRequest, getTruConfiguration().getIntegrationLoggingRestAPIURL(), null);
			if(!StringUtils.isBlank(integrationLoggingURL)){
				if (isLoggingDebug()) {
					logDebug("Integration logging URL::" + integrationLoggingURL);
				}
				pRequest.setParameter(INTEGRATION_LOGGING_URL,integrationLoggingURL);
			}
		}
		pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);

	}
	
	
	/**
	 * 
	 * This method takes the input the request and URI and query string and returns the 
	 * URL 
	 * 
	 * @param pRequest the pRequest
	 * @param pUri the Uri
	 * @param pQuery query
	 * @return String  the uri
	 */
	public String getConstructedUrl(HttpServletRequest pRequest,String pUri ,String pQuery)
	
	{
		
		StringBuffer url  = new StringBuffer();
		// Determine what the current scheme is, HTTP or HTTPS
		boolean isSchemeSecure = Boolean.FALSE;
		String portNumber=HTTP_PORT;
		
		if (isLoggingDebug()) {
			logDebug("Scheme is " + pRequest.getScheme());
		}
		
		if(getEnableSchemeDetectionFromHeader()){
				Enumeration headerNames = pRequest.getHeaderNames();
				while (headerNames.hasMoreElements()) {
					String key = (String) headerNames.nextElement();
					String value = pRequest.getHeader(key);
					if(isLoggingDebug()){
						logDebug(key + " : " + value);
					}
				}
				
		
			String schemeFromHeader = pRequest.getHeader(ORIGIN_SCHEME_HEADER);
			if(isLoggingDebug()){
				logDebug("scheme from header " + schemeFromHeader);
			}
			if((schemeFromHeader != null) && schemeFromHeader.equalsIgnoreCase(SECURE_PROTOCOL)){
				if(isLoggingDebug()){
					logDebug("setting scheme from header " + schemeFromHeader);
				}
				isSchemeSecure = Boolean.TRUE;
				portNumber=HTTPS_PORT;
			}
		}else{	

			if ((pRequest.getScheme() != null) && pRequest.getScheme().equalsIgnoreCase(SECURE_PROTOCOL)) {
				if(isLoggingDebug()){
					logDebug("setting scheme  " + pRequest.getScheme());
				}
				isSchemeSecure = Boolean.TRUE;
				portNumber=HTTPS_PORT;
			}
		}
		if(isLoggingDebug()){
			logDebug("isSchemeSecure value is " + isSchemeSecure);
		}
		
		if(isSchemeSecure){
			url.append(SECURE_PROTOCOL).append(TRUCommerceConstants.COLON_SYMBOL).append(TRUCommerceConstants.PATH_SEPERATOR).append(TRUCommerceConstants.PATH_SEPERATOR);
		}else{
			url.append(NONSECURE_PROTOCOL).append(TRUCommerceConstants.COLON_SYMBOL).append(TRUCommerceConstants.PATH_SEPERATOR).append(TRUCommerceConstants.PATH_SEPERATOR);
		}
		if(isPortRequired()){
			url.append(pRequest.getServerName()).append(TRUCommerceConstants.COLON_SYMBOL).append(portNumber);			
		}
		else{
			url.append(pRequest.getServerName());
		}
		
		url.append(TRUCommerceConstants.PATH_SEPERATOR);
		
		if (!StringUtils.isEmpty(pUri))
		{
			url.append(pUri);
		}
		
		if (!StringUtils.isEmpty(pQuery))
		{
			url.append(TRUCommerceConstants.QUESTION_MARK).append(pQuery);
			
		}
	
		return url.toString();
	}

}
