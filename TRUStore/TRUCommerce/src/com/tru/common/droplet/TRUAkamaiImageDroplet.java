
package com.tru.common.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.utils.TRUSchemeDetectionUtil;
/**
 * The class.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUAkamaiImageDroplet  extends DynamoServlet {
	
	
	/**
	 * property to hold number of mTRUConfiguration.
	 */
	private TRUConfiguration mTRUConfiguration;
	

	/** Constant for OUTPUT. */

	public static final String OUTPUT = TRUConstants.OUTPUT;
	
	/** Constant for IMAGE_NAME. */
	private static final String IMAGE_NAME = "imageName";
	
	/** Constant for IMAGE_HEIGHT. */
	private static final String IMAGE_HEIGHT = "imageHeight";
	
	/** Constant for IMAGE_WIDTH. */
	private static final String IMAGE_WIDTH = "imageWidth";
	
	/** Constant for AKAMAI_URL. */
	private static final String AKAMAI_URL = "akamaiUrl";
	
	/** The scheme detection util. */
	private TRUSchemeDetectionUtil mSchemeDetectionUtil;

	//private static final String DEFAULT_IMAGE_NAME = "http://truimgdev.toysrus.com/product/images/noImage.jpg";
	

	/**
	 * @param pRequest DynamoHttpServletRequest.
	 * @param pResponse DynamoHttpServletResponse.
	 * @throws ServletException ServletException.
	 * @throws IOException IOException.
	 */

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
		String imageName = null;
		if(pRequest.getLocalParameter(IMAGE_NAME) != null) {
			imageName =  (String) pRequest.getLocalParameter(IMAGE_NAME).toString();
		} else {
			imageName = getSchemeDetectionUtil().getScheme() + TRUConstants.DEFAULT_IMAGE_NAME;
		}
		String imageHeight =  (String)pRequest.getLocalParameter(IMAGE_HEIGHT);
		String imageWidth =  (String)pRequest.getLocalParameter(IMAGE_WIDTH);
		
		String imageUrl = imageName+TRUConstants.QUESTION_MARK_STRING+TRUConstants.FIT+
				TRUConstants.EQUALS_SYMBOL_STRING+TRUConstants.INSIDE+TRUConstants.PIPE_STRING+imageWidth+TRUConstants.COLON_STRING+imageHeight;
		
		pRequest.setParameter(AKAMAI_URL,imageUrl);
		pRequest.serviceParameter(OUTPUT, pRequest, pResponse);
	}
	
	/**
	 * @return the mTRUConfiguration.
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * @param pTRUConfiguration the mTRUConfiguration to set.
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		this.mTRUConfiguration = pTRUConfiguration;
	}

	/**
	 * Gets the scheme detection util.
	 * 
	 * @return the scheme detection util
	 */
	public TRUSchemeDetectionUtil getSchemeDetectionUtil() {
		return mSchemeDetectionUtil;
	}

	/**
	 * Sets the scheme detection util.
	 * 
	 * @param pSchemeDetectionUtil
	 *            the new scheme detection util
	 */
	public void setSchemeDetectionUtil(TRUSchemeDetectionUtil pSchemeDetectionUtil) {
		this.mSchemeDetectionUtil = pSchemeDetectionUtil;
	}
	
}
