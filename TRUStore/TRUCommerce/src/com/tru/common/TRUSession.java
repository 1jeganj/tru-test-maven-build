package com.tru.common;

import java.util.Map;

import javax.servlet.http.Cookie;

import atg.nucleus.GenericService;

/**
 * The class holds the TRUSession property getters and setters.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUSession extends GenericService {

	/** The m tru mTRUCookies. */
	private Map<String, Cookie> mTRUCookies;

	/**
	 * Gets the tRU cookies.
	 * 
	 * @return the tRU cookies
	 */
	public Map<String, Cookie> getTRUCookies() {
		return mTRUCookies;
	}

	/**
	 * Sets the tru cookies.
	 * 
	 * @param pTRUCookies
	 *            the m tru cookies
	 */
	public void setTRUCookies(Map<String, Cookie> pTRUCookies) {
		this.mTRUCookies = pTRUCookies;
	}
}
