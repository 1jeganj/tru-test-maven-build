package com.tru.common;

import atg.nucleus.naming.ParameterName;

/**
 * This class holds all the constants.
 * @author Professional Access
 * @version 1.0
 */
public class TRUConstants {
	/** Constant for CHECKOUT. */
	public static final String CHECKOUT = "checkout";
	/** Constant for ENGLISH. */
	public static final String ENGLISH = "en";
	/** Constant for ABCD. */
	public static final String ABCD ="abcd";
	/** Constant for PRODUCT_DESCRIPTION. */
	public static final String PRODUCT_DESCRIPTION = "productDescription";
	/** Constant for SKN_ORIGIN. */
	public static final String SKN_ORIGIN = "sknOrigin";
	/** Constant for UID. */
	public static final String UID = "uid";
	/** Constant for UPC. */
	public static final String UPC ="upc";
	/** Constant for SKN. */
	public static final String SKN = "skn";
	/** Constant for COLOR. */
	public static final String COLOR = "color";
	/** Constant for SIZE. */
	public static final String SIZE = "size";
	/** Constant for SIZE. */
	public static final String REQUESTED_QTY = "requestedQty";

	/** Constant for RESPONSE. */
	public static final String RESPONSE = "response";
	/** Constant for PROFILE_COOKIE_NAME. */
	public static final String PROFILE_COOKIE_NAME = "VisitorUsaFullName";
	
	/** Constant for DPS_LOGOUT_COOKIE. */
	public static final String DPS_LOGOUT_COOKIE = "DPSLogout";

	/** Constant for CART_COOKIE_NAME. */
	public static final String CART_COOKIE_NAME = "cartCookie";

	/** Constant for CART_COOKIE_NAME. */
	public static final String LOGGEDEIN_CART_COOKIE_NAME = "loggedinCartCookie";
	/**
	Constant for holding the PRODUCT_COMPARE_COOKIE.
    */
	public static final String PRODUCT_COMPARE_COOKIE = "productCompareCookie";

	/** Constant for EMPTY. */
	public static final String EMPTY = "";

	/** Constant for SEARCH_IMAGE_URL. */
	public static final String SEARCH_IMAGE_URL = "searchImageUrl";

	/** Constant for SKU_REPOSITORYID. */
	public static final String SKU_REPOSITORYID = "sku.repositoryId";

	/** Constant for BEST_SELLER_SORT. */
	public static final String BEST_SELLER_SORT="bestSellerSort";
	/** Constant for address nick name. */
	public static final String ADDRESS_NICK_NAME = "nickName";

	/** Constant for ADDRESS_CLASS_NAME. */
	public static final String ADDRESS_CLASS_NAME = "atg.core.util.ContactInfo";

	/** Constant for credit card nick name. */
	public static final String CREDIT_CARD_NICK_NAME = ADDRESS_NICK_NAME;

	/** Constant for changePassword. */
	public static final String MY_ACCOUNT_CHANGE_PASSWORD_FORM = "changePasswordForm";

	/** Constant for resetPasswordForm. */
	public static final String MY_ACCOUNT_RESET_PASSWORD_FORM = "resetPasswordForm";

	/** Constant for updatePersonalInfoForm. */
	public static final String MY_ACCOUNT_UPDATE_PERSONAL_INFO_FORM = "updatePersonalInfoForm";

	/** Constant for updateEmailAddressForm. */
	public static final String MY_ACCOUNT_UPDATE_EMAIL_FORM = "updateEmailAddressForm";

	/** Constant for add/update rewards card form. */
	public static final String ADD_UPDATE_REWARDS_CARD_FORM= "addUpdateRewardsCardForm";

	/** constant for int 13. */
	public static final int THIRTEEN = 13;

	/** constant for 1. */
	public static final int SIZE_ONE = 1;

	/** Constant for PREV_SHIPPINGADDRESS_NICKNAME */
	public static final String PREV_SHIPPINGADDRESS_NICKNAME = "prevShippingAddressNickName";

	/** property to hold one. */
	public static final int ONE = 1;

	/** Constant for forgotPasswordForm. */
	public static final String FORGOT_PASSWORD_FORM = "forgotPasswordForm";
		/**
	 * Constant for holding add at the time of population secondary address.
	 */
	public static final String ADD = "add";

	/** property to hold ATTRIBUTE_NAME */
	public static final String ATTRIBUTE_NAME = "attributeName";
	/**
	 * property to hold credit card token for temporary purpose, will be removed.
	 */
	public static final String CREDIT_CARD_TOKEN = "creditCardToken";
	/**
	 * property to hold credit card token for temporary purpose, will be removed.
	 */
	public static final String CREDIT_CARD_NUMBER = "creditCardNumber";

	/**
	 * property to hold synchrony card holder name.
	 */
	public static final String CARD_HOLDER_NAME = "cardHolderName";

	/**
	 * property to hold length of card.
	 */
	public static final String CARD_LENGTH = "cardLength";
	/**
	 * property to hold type of card.
	 */
	public static final String CARD_TYPE = "cardType";
	/**
	 * property to hold type of card undefined.
	 */
	public static final String CARD_TYPE_UNDEFINED = "undefined";
	/**
	 * property to hold credit cardtype.
	 */
	public static final String CREDIT_CARD_TYPE = "creditCardType";

	/** Constant for add address form name.*/
	public static final String MY_ACCOUNT_EDIT_ADDRESS_FORM = "myAccountEditAddressForm";


	/** constant for int 20. */
	public static final int TWENTY = 20;


	/** constant for question mark as string. */
	public static final String QUESTION_MARK_STRING = "?";

	/** constant for email param for forgot password. */
	public static final String EMAIL_PARAM_NAME = "pe";

	/** constant for timestamp param for forgot password. */
	public static final String TIMESTAMP_PARAM_NAME = "pt";

	/** constant for equals symbol as string. */
	public static final String EQUALS_SYMBOL_STRING = "=";

	/** constant for ":". */
	public static final String COLON_STRING = ":";

	/** constant for response status CORRECTED.*/
	public static final String ADDRESS_DOCTOR_STATUS_PROCESS_STATUS = "ProcessStatus";

	/** constant for response status ADDRESS_DOCTOR_STATUS_ADDRESS_DOCTOR_MESSAGE.*/
	public static final String ADDRESS_DOCTOR_STATUS_ADDRESS_DOCTOR_MESSAGE = "AddressDoctorMessage";

	/** constant for response status ADDRESS_DOCTOR_RESULTS.*/
	public static final String ADDRESS_DOCTOR_RESULTS = "results";

	/** constant for response status ADDRESS_DOCTOR_CONTENT.*/
	public static final String ADDRESS_DOCTOR_CONTENT = "content";

	/** constant for response status ADDRESS_DOCTOR_COUNT.*/
	public static final String ADDRESS_DOCTOR_COUNT = "count";

	/** constant for response status CORRECTED.*/
	public static final String ADDRESS_DOCTOR_STATUS_CORRECTED = "CORRECTED";

	/** constant for response status VERIFIED.*/
	public static final String ADDRESS_DOCTOR_STATUS_VERIFIED = "VERIFIED";

	/** constant for response status VALIDATION ERROR*/
	public static final String ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR = "VALIDATION ERROR";
	
	/** constant for response status VALIDATION ERROR*/
	public static final String ADDRESS_DOCTOR_STATUS_CONNECTION_ERROR = "CONNECTION ERROR";

	/** constant for path="/". */
	public static final String PATH = "/";

	/** Constant for add address form name.*/
	public static final String MY_ACCOUNT_ADD_ADDRESS_FORM = "myAccountAddAddressForm";

	/** Constant for add credit card with address form name.*/
	public static final String MY_ACCOUNT_ADD_CARD_WITH_ADDR_FORM = "addCreditCardWithAddress";

	/** Constant for add credit card without address form name.*/
	public static final String MY_ACCOUNT_ADD_CARD_WITHOUT_ADDR_FORM = "addCreditCardWithoutAddress";

	/** The Constant PIPE_STRING. */
	public static final String PIPE_STRING = "|";

	/** The Constant PIPE_STRING. */
	public static final char PIPE_STRING_CHAR = '|';

	/** The Constant AMPERSAND_STRING. */
	public static final String AMPERSAND_STRING = "&";

	/** The Constant DOT. */
	public static final String DOT = ".";
	
	/**
	 * Constant for OUT PARAM.
	 */
	public static final String OUTPUT_PARAM = "output";

	/**  Constant for expiration month. */
	public static final String EXPIRATION_MONTH = "expirationMonth";

	/**  Constant for expiration year. */
	public static final String EXPIRATION_YEAR = "expirationYear";

	/** Constant to hold month.*/
	public static final String MONTH = "month";

	/** Constant to Hold YEAR.*/
	public static final String YEAR= "year";

	/** Constant to hold number 36.*/
	public static final String THIRTY_SIX = "36";

	/** Constant to hold MASTERCARD.*/
	public static final String MASTERCARD = "masterCard";

	/** Constant to hold DISCOVER card.*/
	public static final String DISCOVER = "discover";

	/** Constant to hold VISA card.*/
	public static final String VISA = "visa";

	/** Constant to hold AMEX card.*/
	public static final String AMEX = "americanExpress";

	/** Constant to hold BINRange.*/
	public static final String ITEM_DESCRIPTOR_BIN_RANGE = "BINRange";

	/** Constant to hold lower bin range.*/
	public static final String LOWER_RANGE = "lowerRange";

	/** Constant to hold UPPER bin range.*/
	public static final String UPPER_RANGE = "upperRange";

	/** Constant to hold FORWARD_SLASH.*/
	public static final String FORWARD_SLASH = "/";

	/** Constant to hold HYPHEN.*/
	public static final String HYPHEN = "-";

	/** Constant to hold PHONE_NUMBER.*/
	public static final String PHONE_NUMBER = "phoneNumber";

	/** constant to hold ID.*/
	public static final String ID = "ID";

	/** constant to hold "DeliveryAddressLine". */
	public static final String DELIVERY_ADDRESS_LINE = "DeliveryAddressLine";

	/** constant to hold "SubBuilding". */
	public static final String SUB_BUILIDING = "SubBuilding";

	/** constant to hold "Locality". */
	public static final String LOCALITY = "Locality";

	/** constant to hold "Province". */
	public static final String PROVINCE = "Province";

	/** constant to hold "Country". */
	public static final String COUNTRY = "Country";

	/** constant to hold "PostalCode". */
	public static final String POSTAL_CODE = "PostalCode";

	/** constant to hold "fullAddress". */
	public static final String FULL_ADDRESS = "fullAddress";

	/** constant for int 14. */
	public static final int FOURTEEN = 14;

	/** constant for int 16. */
	public static final int SIXTEEN = 16;

	/** constant for int 0. */
	public static final int ZERO = 0;

	/** constant for int 6. */
	public static final int SIX = 6;

	/** Constant to hold FALSE. */
	public static final String FALSE = "false";

	/** Constant to hold ADDRESS_VALIDATED.*/
	public static final String ADDRESS_VALIDATED = "addressValidated";

	/** Constant to hold POSTAL_CODE.*/
	public static final String POSTAL_CODE_ADDRESS = "postalCode";

	/** Constant to hold number 21.*/
	public static final String TWENTY_ONE = "21";

	/** Constant to hold number. ""*/
	public static final String DOUBLE_QUOTE = "";

	/** constant for int 12. */
	public static final int TWELVE = 12;

	/** constant for int 2. */
	public static final int TWO = 2;

	/** constant for int 3. */
	public static final int THREE = 3;

	/** constant for int 10. */
	public static final int TEN = 10;

	/** The Constant ELEVEN. */
	public static final int ELEVEN = 11;

	/**  Constant to hold TRUE. */
	public static final String TRUE = "true";

	/** Constant to hold TRU_PROFILE_FORM_HANDLER.*/
	public static final String TRU_PROFILE_FORM_HANDLER = "TRUProfileFormHandler";

	/** Constant to hold HANDLE_UPDATE_CREDIT_CARD.*/
	public static final String HANDLE_UPDATE_CREDIT_CARD = "handleUpdateCreditCard";

	/** Constant to hold ELEMENT.*/
	public static final String ELEMENT = "element";

	/** Constant to hold OUTPUT.*/
	public static final String OUTPUT = "output";

	/** Constant to hold CREDIT_CARDS.*/
	public static final String CREDIT_CARDS = "creditCards";

	/** Constant to hold DEFAULT_CREDIT_CARD.*/
	public static final String DEFAULT_CREDIT_CARD = "defaultCreditCard";

	/** Constant to hold DEFAULT_CARD_EXISTS.*/
	public static final String DEFAULT_CARD_EXISTS = "defaultCardExists";

	/** The Constant EMPTY_STRING. */
	public static final String EMPTY_STRING = "";

	/** The Constant CHANNEL_TYPE. */
	public static final String CHANNEL_TYPE = "channelType";

	/** The Constant CHANNEL_ID. */
	public static final String CHANNEL_ID = "channelId";

	/** The Constant CHANNEL_USER_NAME. */
	public static final String CHANNEL_USER_NAME = "channelUserName";

	/** Constant to hold ://. */
	public static final String URL_SEPARATOR = "://";

	/** Static property to hold ':'. */
	public static final String COLON = ":";

	/** The Constant CHANNEL_CO_USER_NAME. */
	public static final String CHANNEL_CO_USER_NAME = "channelCoUserName";

	/** The Constant CHANNEL_NAME. */
	public static final String CHANNEL_NAME = "channelName";

	/** The Constant CHANNEL_NAME. */
	public static final String SOURCE_CHANNEL = "sourceChannel";
	
	/** The Constant REGISTRANT_ID. */
	public static final String REGISTRANT_ID = "registrantId";

	/** The Constant JUVENILE_PRODUCT_SIZE. */
	public static final String JUVENILE_PRODUCT_SIZE = "juvenileProductSize";

	/** The Constant COLOR_UPC_LEVEL. */
	public static final String COLOR_UPC_LEVEL = "colorUpcLevel";

	/** The Constant IS_GIFT_WRAP. */
	public static final String IS_GIFT_WRAP = "isGiftWrap";

	/** The Constant TILDA_STRING. */
	public static final String TILDA_STRING = "~";

	/** The Constant BACKSLASH_TILDA_STRING. */
	public static final String BACKSLASH_TILDA_STRING = "\\~";

	/** Holds SKU ID. */
	public static final String SKU_ID = "skuId";

	/** Holds Product ID. */
	public static final String PRODUCT_ID = "productId";

	/** Holds Sale Price. */
	public static final String SALE_PRICE = "salePrice";

	/** Holds List Price. */
	public static final String LIST_PRICE = "listPrice";

	/** Holds Save Percentage. */
	public static final String SAVED_PERCENTAGE = "savedPercentage";

	/**
	 * Holds Saved Amount.
	 */
	public static final String SAVED_AMOUNT = "savedAmount";

	/**  Holds the empty oparam. */
	public static final String EMPTY_OPARAM = "empty";

	/**  Holds the true oparam. */
	public static final String OUTPUT_OPARAM_TRUE = "true";

	/**  Holds the output oparam. */
	public static final String OUTPUT_OPARAM_FALSE = "false";

	/**
	 * property to hold first detailed item price info 1.
	 */
	public static final int FIRST_DETIALED_ITEM_PRICE_INFO = 1;

	/**
	 * property to hold empty detailed item price info 0.
	 */
	public static final int EMTPY_DETIALED_ITEM_PRICE_INFOS = 0;

	/**
	 * property to hold empty price adjustments 0.
	 */
	public static final int EMTPY_ADJUSTMENTS = 0;

	/**
	 * Holds double value zero.
	 */
	public static final double DOUBLE_ZERO = 0.0;

	/** property to hold one. */
	public static final int INTEGER_NUMBER_ZERO = 0;
	/** property to hold one. */
	public static final int INTEGER_NUMBER_ONE = 1;

	/** property to hold two. */
	public static final int INTEGER_NUMBER_TWO = 2;

	/** This holds english locale. */
	public static final String LOCALE_EN_US = "en_US";

	/** This holds under score. */
	public static final String UNDER_SCORE = "_";

	/** property to hold element. */
	public final static String TRU_PROPERTY_RESOURCES = "com.tru.commerce.TRUPropertyNameResources";

	/** property to hold element. */
	public static final String MSG_ERROR_UNABLE_TO_LOADRESOURCE = "ERROR: Unable to load resource";

	/** Static property to hold white space. */
	public static final String WHITE_SPACE = " ";

	/** Holds the property to check whether the sku added to order. */
	public static final String IS_SKU_ADDED_TO_ORDER = "isSkuAddedToOrder";

	/** The Constant LONG_ZERO. */
	public static final Long LONG_ZERO = 0L;

	/** The Constant FOUR. */
	public static final int FOUR = 4;

	/** The Constant FIVE. */
	public static final int FIVE = 5;

	/** The Constant SEVEN. */
	public static final int SEVEN = 7;

	/** The Constant EIGHT. */
	public static final int EIGHT = 8;

	/** The Constant NINE. */
	public static final int NINE = 9;

	/** The Constant MINUS_ONE. */
	public static final int INT_MINUS_ONE = -1;

	/** The Constant INT_THOUSAND. */
	public static final int INT_THOUSAND = 1000;

	/** The Constant INT_TWENTY_FOUR. */
	public static final int INT_TWENTY_FOUR = 24;

	/** The Constant INT_SIXTY. */
	public static final int INT_SIXTY = 60;


	/**  Constant to hold United States. */
	public static final String UNITED_STATES = "US";

	/** The Constant OTHER_STATE. */
	public static final String OTHER_STATE = "otherState";

	/** The Constant NA. */
	public static final String NA = "NA";

	/** Constant to hold USA.*/
	public static final String USA = "USA";

	/** Constant to hold true bilingAddressFieldVisible.*/
	public static final String BILLING_ADDRESS_VISIBLE = "bilingAddressFieldVisible";
	/** property to hold element. */
	public static final long LONG_ONE = 1L;

	/**  Constant to hold address1. */
	public static final String ADDRESS_1 = "address1";

	/**  Constant to hold address2. */
	public static final String ADDRESS_2 = "address2";

	/**  Constant to hold city. */
	public static final String CITY = "city";

	/**  Constant to hold state. */
	public static final String STATE = "state";

	/**  Constant to hold STATE_ADDRESS. */
	public static final String STATE_ADDRESS = "stateAddress";

	/**  Constant to hold postal code. */
	public static final String POSTAL_CODE_FOR_ADD = "postalCode";

	/**  Constant to hold postal code. */
	public static final String COUNTRY_TO_ADD = "country";
	/** Hold list of Adjustments. */
	public static final String ADJUSTMENTS = "adjustments";

	/** Hold list of promotions. */
	public static final String PROMOTIONS = "promotions";

	/**
	 * Hold rank one promotion.
	 */
	public static final String RANK_ONE_PROMOTION = "rankOnePromotion";

	/**
	 * Hold promotion count.
	 */
	public static final String PROMOTION_COUNT = "promotionCount";

	/** The Constant 100. */
	public static final int HUNDERED = 100;

	/**  Constant to hold STATE_AK. */
	public static final String STATE_AK = "AK";

	/**  Constant to hold STATE_HI. */
	public static final String STATE_HI ="HI";

	/** Hold  AK_HI_MAP. */
	public static final String AK_HI_MAP= "akHiMap";

	/** Hold   LOWER_48_MAP. */
	public static final String   LOWER_48_MAP = "lower48Map";

	/** Hold   f. */
	public static final String F="F";

	/** Hold   CHILD_SKUS. */
	public static final String  CHILD_SKUS ="childSkus";

	/**
	 * Hold discount_value.
	 */
	public static final String DISCOUNT_VALUE_KEY = "discount_value";
	/**
	 * Hold spend_value.
	 */
	public static final String SPEND_VALUE_KEY = "spend_value";
	/**
	 * Hold remainingAmount.
	 */
	public static final String REMAINING_AMOUNT_OPARAM = "remainingAmountOparam";
	/**
	 * Hold freeShipping.
	 */
	public static final String FREE_SHIPPING_PARAM = "freeShippingParam";
	/**
	 * Hold spend_value.
	 */
	public static final String SPEND_VALUE_PARAM = "spendValue";
	/**
	 * Hold remainingAmount.
	 */
	public static final String REMAINING_AMOUNT_PARAM = "remainingAmount";
	/**
	 * Hold profile.
	 */
	public static final String PROFILE = "profile";
	
	/** The Constant RADIAL_PAYMENT. */
	public static final String RADIAL_PAYMENT = "radialPayment";
	
	/** The Constant ENTRY. */
	public static final String ENTRY = "entry";
	/**
	 * Hold orderTotal.
	 */
	public static final String ORDER_TOTAL = "orderTotal";
	/**
	 * Hold discount value.
	 */
	public static final String DISCOUNT_VALUE = "0";

	/**  Constant for credit card cvv year. */
	public static final String CREDIT_CARD_CVV = "creditCardVerificationNumber";

	/**  Constant for single shipping. */
	public static final String SINGLE_SHIPPING = "singleShipping";

	/**  Constant to hold HANDLE_INITIALIZE_CREDIT_CARD. */
	public static final String HANDLE_INITIALIZE_CREDIT_CARD = "handleInitializeCreditCard";

	/**  Constant to hold HANDLE_ENSURE_PAYMENT_GROUP. */
	public static final String HANDLE_ENSURE_PAYMENT_GROUP = "TRUBillingInfoFormHandler.handleEnsurePaymentGroup";

	/**  Constant to hold profanityWords. */
	public static final String PROFANITY_WORDS = "profanityWords";

	/**  Constant to hold ALL. */
	public static final String ALL_PROMO = "allPromo";

	/**  Constant to hold ITEM. */
	public static final String ITEM_PROMO = "itemPromo";

	/**  Constant to hold ORDER. */
	public static final String ORDER_PROMO = "orderPromo";

	/**  Constant to hold SHIPPING. */
	public static final String SHIPPING_PROMO = "shippingPromo";

	/**  Constant to hold TAG_PROMOS. */
	public static final String TAG_PROMOS = "tagPromos";

	/** constant to hold sessionExpired=true parameter for construction redirection URL */
	public static final String SESSION_EXPIRED_PARAMETER = "?sessionExpired=true";

	/** Constant to hold APPLICATION_JSON. */
	public static final String APPLICATION_JSON = "application/json";

	/** Constant to hold CONTENT_TYPE. */
	public static final String CONTENT_TYPE = "Content-Type";

	/** Constant to hold POST. */
	public static final String POST = "POST";

	/** Constant to hold STR. */
	public static final String STR = "|";

	/** Constant to hold word. */
	public static final String WORD = "word";

	/**  Constant to hold RESIZE. */
	public static final String RESIZE = "resize";

	/**  Constant to hold FIT. */
	public static final String FIT = "fit";

	/**  Constant to hold INSIDE. */
	public static final String INSIDE = "inside";

	/**  Constant to hold Not Applied. */
	public static final String NOT_APPLIED = "Not Applied";

	/** constant to hold server error code in case of session expired for AJAX request. */
	public static final String PRODUCTION_URL="productionURL";

	/** Constant to hold XMLHTTPREQUEST.*/
	public static final String XMLHTTPREQUEST = "XMLHttpRequest";

	/** Constant to hold XREQUESTEDWITH.*/
	public static final String XREQUESTEDWITH = "X-Requested-With";

	/** constant to hold server error code in case of session expired for AJAX request. */
	public static final Integer SERVER_ERROR_CODE=409;

	/** Hold   f. */
	public static final String S="S";
	/** added this constant for holding order billing address*/
	public static final String ORDER_BILLING_ADDRESS = "orderBillingAddress";

	/** added this constant for holding order nonMerchSKU*/
	public static final String NON_MERCH_SKU = "nonMerchSKU";


	/** The Constant for SITE_CONFIGURATION */
	public static final String SITE_CONFIGURATION="siteConfiguration";

	/**
	 * The Constant for ADDRESS1_PARAM which was used in TRUShippingPricingEngine
	 */
	public static final String ADDRESS1_PARAM="Address1";

	/**
	 * The Constant for STATE_PARAM which was used in TRUShippingPricingEngine
	 */
	public static final String STATE_PARAM="State";

	/**
	 *
	 */
	public static final String ORDER_PARAM="Order";

	public static final String DAYS=" Days";

	public static final String IFAN="-";
	/**
	 * This constant is for getting the reward no from profile
	 */
	public static final String REWARD_NO = "rewardNo";
	/**
	 * Constant to hold the value of SALE.
	 */
	public static final String SALE = "Sale";
	/**
	 * Constant to hold the value of SALE.
	 */
	public static final String ORDER = "Order";

	/**
	 * Constant to hold the value of token.
	 */
	public static final String PAYPAL_TOKEN = "token";
	/**
	 * Constant to hold the value of token.
	 */
	public static final String DIGIT_ZERO = "0";
	/**
	 *
	 */
	public static final String EDD_TEXT="ready in ";

	/**
	 * Constant to hold the value of GIFT.
	 */
	public static final String GIFT = "Gift";

	/**
	 * Constant to hold the value of BALANCE_INQUIRY.
	 */
	public static final String BALANCE_INQUIRY = "balance_inquiry";

	/**
	 * Constant to hold the value of VALUELINK.
	 */
	public static final String VALUELINK = "valuelink";

	/**
	 * Constant to hold the value of token.
	 */
	public static final String DIGIT_ONE = "1";
	/**
	 * Constant to hold the value of TRUShippingManager.
	 */
	public static final String TRU_SHIPPING_MANAGER = "TRUShippingManager";
	/**
	 * Constant to hold the value of TRUShippingTools.
	 */
	public static final String TRU_SHIPPING_TOOLS = "TRUShippingTools";
	/**
	 * Constant to hold the value of GIFT_ID.
	 */
	public static final String GIFT_ID = "giftid";
	/** Constant for INT_ONE */
	public static final String CHAR_DOT = ".";

	/** Constant for USER_SESSION */
	public static final String USER_SESSION = "userSession";

	/** Constant for PAYPAL_ORDER */
	public static final String PAYPAL_ORDER = "order";
	/**
	 * Constant to hold the value of shiptocountrycode.
	 */
	public static final String SHIP_TO_COUNTRY_CODE = "US";

	/** Constant for PROFILE_COOKIE_NAME. */
	public static final String PROFILE_FIRST_NAME = "VisitorFirstName";

	/** Constant for PROFILE_COOKIE_NAME. */
	public static final String ITEM_COUNT_COOKIE = "itemCountCookie";

	/** The Constant REMINDER_COUNT_COOKIE. */
	public static final String REMINDER_COUNT_COOKIE = "reminderCountCookie";

	/** Constant for FREE_SHIPPING_PROGRESS_BAR. */
	public static final String FREE_SHIPPING_PROGRESS_BAR_COOKIE = "freeShippingProgressBarCookie";

	/** Constant for CURRENT_SERVER_TIMESTAMP. */
	public static final String CURRENT_SERVER_TIMESTAMP = "currentTimeStamp";

	/** Constant for COOKIE_SECURE_TRUE. */
	public static final boolean COOKIE_SECURE_TRUE = true;

	/** Constant for COOKIE_SECURE_FALSE. */
	public static final boolean COOKIE_SECURE_FALSE = false;

	/** Constant for MY_STORE_ADDRESS_COOKIE. */
	public static final String MY_STORE_ADDRESS_COOKIE = "myStoreAddressCookie";


	/** The Constant FAV_STORE_COOKIE. */
	public static final String FAV_STORE_COOKIE = "favStore";

	/** Constant for MAX_AGE_VALUE. */
	public static final int MAX_AGE_VALUE = 2592000;

	/** Constant for @ AT_SIGN. */
	public static final String AT_SIGN = "@";

	/** Constant for @ TYPE_AHEAD_COLLECTION. */
	public static final String TYPE_AHEAD_COLLECTION = "/content/ToysRUs/TypeAheadCollection";

	/** constant to hold referer */
    public static final String REFERER = "Referer";
    /** constant to hold errorMessage */
    public static final String ERROR_MESSAGE ="errorMessage";

    /** constant to hold STORE_OUT_OF_STOCK */
    public static final String STORE_OUT_OF_STOCK = "storelocator.itemOutOfStock";

    /** constant to hold DOUBLE_ZERO_STRING */
    public static final String DOUBLE_ZERO_STRING ="0.0";

    /** constant to hold DOUBLE_ZERO_STRING */
    public static final String CHAR_PATTERN ="[^a-z0-9 ]";

    /**
     * Constant for ADDRESS_PARAM
     */
    public static final String ADDRESS_PARAM="Address";
    /** constant for 31. */
	public static final int INTEGER_NUMBER_THIRTY_ONE = 31;

	/** The Constant THOUSAND. */
	public static final int NUMBER_THOUSAND=1000;

	/** constant to hold INIT_BASED_ON_LAST_ORDER */
	public static final String INIT_BASED_ON_LAST_ORDER = "initBasedOnLastOrder";

	/**
	 * Constant for holding the ECONOMY's ship method code.
	 */
	 public static final String ECONOMY_SHIP_METHOD_CODE="GROUND";
 /**
	 * Constant for holding the STANDARD's ship method code.
	 */
	 public static final String STANDARD_SHIP_METHOD_CODE="TRU_Std_Gnd";
 /**
	 * Constant for holding the EXPIDATED's ship method code.
	 */
	 public static final String EXPIDATED_SHIP_METHOD_CODE="ANY_2Day";
 /**
	 * Constant for holding the EXPRESS's ship method code.
	 */
	 public static final String EXPRESS_SHIP_METHOD_CODE="1DAY" ;
 /**
	 * Constant for holding the FREIGHTCARRIER's ship method code.
	 */
	 public static final String FREIGHT_SHIP_METHOD_CODE="TRUCK";
 /**
	 * Constant for holding the USPS's ship method code.
	 */
	 public static final String USPS_SHIP_METHOD_CODE="USPS_PRIORITY";

	 /**
		 * Constant for holding the USPS's ship method code.
		 */
	public static final String TRU_SHPA="TRU_SHPA";
		 /**
			 * Constant for holding the error.
			 */
	public static final String ERROR="error";
/**
* Constant for holding the message.
*/
	public static final String MESSAGE="message";
 /**
* Constant for holding the currentOrder.
 */
 public static final String CURRENT_ORDER="currentOrder";
 /**
* Constant for holding the SHIPPING_GROUP_MAP_CONTAINER.
*/
 public static final String SHIPPING_GROUP_MAP_CONTAINER="shippingGroupMapContainer";
/**
* Constant for holding the ADDRESS1.
*/
public static final String ADDRESS1="address1";
/**
* Constant for holding the PROFILE_ID.
 */
public static final String PROFILE_ID="id";
/**
 * Constant for holding the FNAME.
*/
public static final String FNAME="Essex County";
/**
* Constant for holding the LNAME.
*/
public static final String LNAME="Veterans Courthouse";
/**
* Constant for holding the ADDR1.
*/
public static final String ADDR1="50 West Market Street";
/**
* Constant for holding the ADDR2.
*/
public static final String ADDR2="";
/**
* Constant for holding the CITY_NY.
*/
public static final String CITY_NY="Newark";
/**
* Constant for holding the STATE_NJ.
*/
public static final String STATE_NY="NJ";
/**
* Constant for holding the ADDRESS_POSTAL_CODE.
*/
public static final String ADDRESS_POSTAL_CODE="07102";
/**
* Constant for holding the ADDRESS_PHONE_NUMBER.
*/
public static final String ADDRESS_PHONE_NUMBER="9737769300";
/**
* Constant for holding the ADDRESS_EMAIL.
*/
public static final String ADDRESS_EMAIL="TRU1001@GMAIL.COM";
/**
* Constant for holding the REGISTANT_ID1.
*/
public static final String REGISTANT_ID1="117704926";
/**
* Constant for holding the COFNAME.
*/
public static final String COFNAME="COFNAME";
/**
* Constant for holding the COLNAME.
*/
public static final String COLNAME="COLNAME";
/**
* Constant for holding the REGISTANT_ID2.
*/
public static final String REGISTANT_ID2="117704927";
/**
* Constant for holding the GIFT_CARD.
*/
public static final String GIFT_CARD="giftCard";

/**  Constant to hold HANDLE_EMAIL_TO_FRIEND. */
public static final String HANDLE_EMAIL_TO_FRIEND = "handleEmailToFriend";
/**  Constant to hold HANDLE_NOTIFYME. */
public static final String HANDLE_NOTIFYME = "handleNotifyMe";


/**
 * Constant for holding a flag to get the ship methods based on new shipping address aor saved address.
 */
public static final String IS_NEW_ADDRESS_FORM="isNewAddressForm";
/**
* Constant for holding the CARDINAL_JWT.
*//*
public static final String CARDINAL_JWT="cardinalJwt";*/
/**
* Constant for holding the PAYLOAD.
*/
public static final String PAYLOAD="Payload";
/**
* Constant for holding the PAYMENT.
*/
public static final String PAYMENT="Payment";
/**
* Constant for holding the EXTENDED_DATA.
*/
public static final String EXTENDED_DATA="ExtendedData";
/**
* Constant for holding the CAVV.
*/
public static final String CAVV="CAVV";
/**
* Constant for holding the ECI_FLAG.
*/
public static final String ECI_FLAG="ECIFlag";
/**
* Constant for holding the PARES_STATUS.
*/
public static final String PARES_STATUS="PAResStatus";
/**
* Constant for holding the SIGNATURE_VERIFICATION.
*/
public static final String SIGNATURE_VERIFICATION="SignatureVerification";
/**
* Constant for holding the XID.
*/
public static final String XID="XID";
/**
* Constant for holding the ENROLLED.
*/
public static final String ENROLLED="Enrolled";
/**
* Constant for holding the TOKEN.
*/
public static final String TOKEN="Token";
/**
* Constant for holding the REASON_CODE.
*/
public static final String REASON_CODE="ReasonCode";
/**
* Constant for holding the REASON_DESCRIPTION.
*/
public static final String REASON_DESCRIPTION="ReasonDescription";
/**
* Constant for holding the ACTION_CODE.
*/
public static final String ACTION_CODE="ActionCode";
/**
* Constant for holding the ERROR_NUMBER.
*/
public static final String ERROR_NUMBER="ErrorNumber";
/**
* Constant for holding the ERROR_DESCRIPTION.
*/
public static final String ERROR_DESCRIPTION="ErrorDescription";

/**
 * Constant to hold SYNCHRONY_DUMMY_PARAM
 */
public static final String SYNCHRONY_DUMMY_PARAM = "?dummy=dummy";

/**
	Constant for holding the REGULAR_SKU.
*/
public static final String REGULAR_SKU = "regularSKU";
/**
	Constant for holding the FALSE_FLAG.
*/
public static final boolean FALSE_FLAG = false;
/**
Constant for holding the PAYMENT_ENQUIRY.
*/
public static final String LAYAWAY_PAYMENT = "LayawayPayment";
/**
Constant for holding the COMMA.
*/
public static final String COMMA = ",";
/** Constant for SINGLE_QUOTE. */
public static final String SINGLE_QUOTE = "'";
/** Constant for BACKSLASH_SINGLE_QUOTE. */
public static final String BACKSLASH_SINGLE_QUOTE = "\\'";

/** Constant for characters/themes catgory name. */
public static final String CHARACTERS_THEMES = "characters/themes";

/**
Constant for holding the TRANSACTION_DATE.
*/
public static final String TRANSACTION_DATE = "Transaction Date";
/**
Constant for holding the TRANSACTION_TOTAL_AMOUNT.
*/
public static final String TRANSACTION_TOTAL_AMOUNT = "Transaction Total Amount";
/**
Constant for holding the NEXT_PAYMENT_AMOUNT.
*/
public static final String NEXT_PAYMENT_AMOUNT = "Next Payment Amount";
/**
Constant for holding the REMAINING_AMOUNT.
*/
public static final String REMAINING_AMOUNT = "Remaining Amount";
/**
Constant for holding the NEXT_PAYMENT_DUE.
*/
public static final String NEXT_PAYMENT_DUE = "Next Payment Due";

/**
 * Constant to hold PayPal
 */
public static final String PAYPAL = "PayPal";
/** Constant for SPLIT_SYMBOL_COMA */
public static final String SPLIT_SYMBOL_COMA = ",";
/**
 * Constant for Allow INT_ZERO
 */

public static final int INT_ZERO = 0;
/**
 * property to hold SPLITE_PRODUCTIDS
 */
public static final String SPLIT_PIPE = "\\|";

/**
 * property to hold TRUE
 */
public static final String STRING_TRUE = "true";

/**
 * property to hold TRUE
 */
public static final String STRING_FALSE = "false";
/**
/**
 * property to hold level string
 */
public final static String LEVEL_STRING = "level";
/**
 * property to hold null string
 */
public final static String NULL_STRING = "null";
/**
 * property to hold null string
 */
public final static String SPECIAL_CHAR_REG_EX="[^\\w\\s\\\\/]";

/**
 *  Property to hold  modifiedCategory.
 */
public final static String MODIFIED_CATEGORY = "modifiedCategory";

/**
 *  Property to hold  breadCrumbLevels.
 */
public final static String BREADCRUMB_LEVELS = "breadCrumbLevels";

/** Property to hold categoryName. */
public static final String CATETORY_NAME = "categoryName";

/** Property to hold  pageName. */
public static final String PAGE_NAME = "pageName";

/**
Constant for holding the RESPONCE_DATE.
*/
public static final String RESPONCE_DATE = "MM/dd/yy";
/**
Constant for holding the LAYAWAY_DATE_DISPLAY.
*/
public static final String LAYAWAY_DATE_DISPLAY = "MM-dd-yyyy";
/**
Constant for holding the ORDER_HISTORY_DATE_DISPLAY.
*/
public static final String ORDER_HISTORY_DATE_DISPLAY = "MMMMMMMMMMMMM dd, yyyy";

/** Constant to hold IS_ENABLE_ADDRESS_DOCTOR. */
public static final String IS_ENABLE_ADDRESS_DOCTOR = "isEnableAddressDoctor";
/** Constant to hold IS_ENABLE_COUPON_SERVICE. */
public static final String IS_ENABLE_COUPON_SERVICE = "isEnableCouponService";
/** Constant to hold IS_ENABLE_EPSLON. */
public static final String IS_ENABLE_EPSLON = "isEnableEpslon";
/** Constant to hold IS_ENABLE_HOOKLOGIC. */
public static final String IS_ENABLE_HOOKLOGIC = "isEnableHookLogic";
/** Constant to hold IS_ENABLE_POWER_REVIEWS. */
public static final String IS_ENABLE_POWER_REVIEWS = "isEnablePowerReviews";
/** Constant to hold empty. */
public static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");


/** Property to hold handleRemoveCoupon. */
public static final String HANDLE_REMOVE_COUPON = "handleRemoveCoupon";
/** Property to hold handleRemoveCoupon. */
public static final String HANDLE_CLAIM_COUPON = "handleClaimCoupon";

/** Property to hold NO_COLORSIZE_AVAILABLE. */
public static final String NO_COLORSIZE_AVAILABLE = "noColorSizeAvailable";

/** Property to hold CURRENCY_USD. */
public static final String CURRENCY_USD = "USD";

/** The Constant Is_SOS. */

public static final String IS_SOS = "isSOS";

/** The Constant Is_SOS_VALUE. */

public static final String IS_SOS_VALUE = "true";

/** The Constant Is_SOS_VALUE. */
public static final String HANDLER_NAME_RECNTLY_VIEVWED = "TRUProfileFormHandler.handleClearRecentViewedProducts";
/** Property to hold ORDER_RESOURCES. */
public static final String ORDER_RESOURCES = "atg.commerce.order.OrderResources";
/** Property to hold USER_MESSAGES. */
public static final String USER_MESSAGES = "atg.commerce.order.UserMessages";
/** Property to hold INVALID_SHIP_GRP_PARAM. */
public static final String INVALID_SHIP_GRP_PARAM = "InvalidShippingGroupParameter";
/** Property to hold INVALID_ORDER_PARAM. */
public static final String INVALID_ORDER_PARAM = "InvalidOrderParameter";
/** Property to hold PRICING_CONTEXT. */
public static final String PRICING_CONTEXT="pricingContext";
/** Property to hold COUPON_CODE_UPDATE. */
public static final String COUPON_CODE_UPDATE="couponUpdateInOrder";
/** Property to hold LIST_OF_PROMOTIONS. */
public static final String LIST_OF_PROMOTIONS="listOfPromotions";
/** Property to hold removedCouponCode. */
public static final String REMOVED_COUPON_CODE="removedCouponCode";
/** Property to hold EXTRA_PARAMETERS. */
public static final String EXTRA_PARAMETERS="ExtraParameters";

/** Property to hold CREDIT_CARD_TYPE_REPOSITORY_EXCEPTION. */
public static final String CREDIT_CARD_TYPE_REPOSITORY_EXCEPTION = " Card type is not available for this Card number, please verify once ";

/** Property to hold ATG_COMMERCE_CATALOG_PRODUCT_CATALOG. */
public static final String ATG_COMMERCE_CATALOG_PRODUCT_CATALOG = "/atg/commerce/catalog/ProductCatalog";

/** Property to hold DEFAULT_IMAGE_NAME. */
public static final String DEFAULT_IMAGE_NAME = "truimgdev.toysrus.com/product/images/noImage.jpg";

/** Property to hold HOST. */
public static final String HOST = "host";


/** Property to hold LASTACCESSED_ID. */
public static final String LASTACCESSED_ID = "lastAccessedID";

/** Property to hold IS_REGISTRY. */
public static final String IS_REGISTRY = "isRegistry";

/** Property to hold REGISTRY_SUCCESS. */
public static final String REGISTRY_SUCCESS = "success";

/** Property to hold REGISTRY_ERROR. */
public static final String REGISTRY_ERROR = "error";

/** Property to hold IS_WISHLIST. */
public static final String IS_WISHLIST = "isWishlist";

/** Property to hold REGISTRY_NUMBER. */
public static final String REGISTRY_NUMBER = "registryNumber";

/** Property to hold PRODUCT_DETAILS. */
public static final String PRODUCT_DETAILS = "productDetails";

/** Property to hold REQUEST_TYPE. */
public static final String REQUEST_TYPE = "requestType";

/** Property to hold REGISTRY_DETAILS. */
public static final String REGISTRY_DETAILS = "registryDetails";

/** Property to hold REGISTRY_RESPONSE. */
public static final String REGISTRY_RESPONSE = "registryResponse";

/** Property to hold MESSAGE_ID. */
public static final String MESSAGE_ID = "messageId";

/** Property to hold REGISTRY_TYPE. */
public static final String REGISTRY_TYPE = "registryType";


/** Property to hold DEFAULT_COLOR. */
public static final String DEFAULT_COLOR = "defaultColor";

/** Property to hold DEFAULT_SIZE. */
public static final String DEFAULT_SIZE = "defaultSize";

/** Property to hold DEFAULT_SKN_ORIGIN. */
public static final String DEFAULT_SKN_ORIGIN = "defaultSknOrigin";

/** Property to hold ITEM_PURCHASED. */
public static final String ITEM_PURCHASED = "itemPurchased";

/** Property to hold ITEM_REQUESTED. */
public static final String ITEM_REQUESTED = "itemRequested";

/** Property to hold NON_SPECIFIC_INDICATOR. */
public static final String NON_SPECIFIC_INDICATOR = "nonSpecificIndicator";

/** Property to hold PURCHASE_UPDATE_INDICATOR. */
public static final String PURCHASE_UPDATE_INDICATOR = "purchaseUpdateIndicator";

/** Property to hold SOURCE. */
public static final String SOURCE = "source";


/** Property to hold LOWER48. */
public static final String LOWER48 = "LOWER48";

/** Property to hold LOWER48_PO_BOX. */
public static final String LOWER48_PO_BOX = "LOWER48_PO_BOX";

/** Property to hold AK_HI. */
public static final String AK_HI = "AK_HI";

/** Property to hold AK_HI_PO_BOX. */
public static final String AK_HI_PO_BOX = "AK_HI_PO_BOX";

/** Property to hold US_TERRITORY. */
public static final String US_TERRITORY = "US_TERRITORY";

/** Property to hold US_TERRITORY_PO_BOX. */
public static final String US_TERRITORY_PO_BOX = "US_TERRITORY_PO_BOX";

/** Property to hold APO_FPO. */
public static final String APO_FPO = "APO_FPO";

/** Property to hold COMMIT_ORDER. */
public static final String COMMIT_ORDER = "commitOrder";


/** Property to hold COMMERCE_REL_ID. */
public static final String USER_LOCALE = "UserLocale";


/** Property to hold COMMERCE_REL_ID. */
public static final String SHIP_GRP_ID = "shipGrpId";

/** Property to hold COMMIT_ORDER. */
public static final String COMMERCE_REL_ID = "commerceRelId";

/** Property to hold GIFT_WRAP_SKU_ID. */
public static final String GIFT_WRAP_SKU_ID = "GiftWrapSkuId";

/** Property to hold GIFT_WRAP_PROD_ID. */
public static final String GIFT_WRAP_PROD_ID = "GiftWrapProductId";


/** Property to hold PRE_GIFT_WRAP_SKU_ID. */
public static final String PRE_GIFT_WRAP_SKU_ID = "PreviousGiftWrapSkuId";

/** Property to hold IS_GIFT_WRAP_SELECTED. */
public static final String IS_GIFT_WRAP_SELECTED="isGiftWrapSelected";

/** Property to hold PRODUCT_NAME. */
public static final String PRODUCT_NAME = "productName";

/** Property to hold PRODUCT_NAMES. */
public static final String PRODUCT_NAMES = "productNames";

/** The Constant TIMESTAMP_FORMAT_FOR_IMPORT. */
public static final String TIMESTAMP_FORMAT_FOR_IMPORT= "yyyy-MM-dd hh:mm:ss";


public static final String CUSTOMER_MASTER_IMPORT = "CustomerMasterImport";


/** The Constant WEBSTORE. */
public static final String WEBSTORE= "Web store";

/** The Constant LETTER_G. */
public static final String LETTER_G= "G";

/** The Constant LETTER_M. */
public static final String LETTER_M= "M";

/** The Constant SELECTED_CC_NICKNAME. */
public static final String SELECTED_CC_NICKNAME= "selectedCCNickName";

/** The Constant RECENTLY_VIEWED_DATE. */
public static final String RECENTLY_VIEWED_DATE= "recentlyViewedDate";

/** The Constant RECENTLY_VIEWED_THRESHOLD. */
public static final String RECENTLY_VIEWED_THRESHOLD= "recentlyViewedThreshold";

/**
 * Constant for addToRegistryCookie.
 **/
public static final String ADDTOREGISTRY_COOKIE = "addToRegistryCookie";

/**
 * Constant for domainNamePrefix.
 **/
public static final String DOMAINNAME_PREFIX = ".babiesrus.com";

/**
 * Constant for addToWishListCookie.
 **/
public static final String ADDTOWISHLIST_COOKIE = "addToWishListCookie";

/**
 * Constant for CVALUE.
 **/
public static final String CVALUE = "cvalue";

/**
 * Constant for PAGENAME.
 **/
public static final String PAGENAME = "pagename";

/** Constant for RETRIEVE_REGISTRY_COOKIE. */
public static final String RETRIEVE_REGISTRY_COOKIE = "retrieveRegistryCookie";

/** The Constant UNITED STATES. */
public static final String FULL_UNITED_STATES= "UNITED STATES";
/** The Constant _0. */
public static final int _0 = 0;
/** The Constant _1. */
public static final int _1 = 1;

/** The Constant _2. */
public static final int _2 = 2;


/** The Constant String 6. */
public static final String STRING_SIX= "6";

/** The Constant for Address Doctor - connection error */
public static final String CONNECTION_ERROR = "CONNECTION_ERROR";

/**
 * The Constant for please try again error
 */
public static final String  PLEASE_TRY_AGAIN_ERROR = "Please try again";

/**
 * The Constant for DEFAULT_SHIPPING_ADDRESSCHANGED
 */
public static final String DEFAULT_SHIPPING_ADDRESS_CHANGED = "DefaultShippingAddressChanged";
/** The Constant SHIPP_ADDRESS. */
public static final String SHIPP_ADDRESS = "shippAddress";
/** The Constant LOWER48_FIXEDSURCHARGE. */
public static final Object LOWER48_FIXEDSURCHARGE = "lower48FixedSurcharge";
/** The Constant ALPHABET_S. */
public static final String ALPHABET_S = "S";
/** The Constant ALPHABET_F. */
public static final String ALPHABET_F = "F";

/** The Constant DOLLER. */
public static final String DOLLER = "$";

/** The Constant DOUBLE_DECIMAL. */
public static final String DOUBLE_DECIMAL = "0.00";

/** The Constant LAYAWAY_DATE_RESPONSE_DISPLAY. */
public static final String LAYAWAY_DATE_RESPONSE_DISPLAY = "dd-MM-yyyy";

/**
 * property to hold salePriceList.
 */
public static final String SALE_PRICE_LIST = "salePriceList";

/**
 * property to hold priceList.
 */
public static final String PRICE_LIST = "priceList";

/**
 * property to hold HANDLE_REWARD_NO.
 */
public static final String HANDLE_REWARD_NO = "handleInitRewardNumberInOrder";

/** The Constant NICKNAME MAX LENGTH. */
public static final int NICKNAME_MAX_LENGTH = 40;

/** The Constant NICKNAME MIN LENGTH. */
public static final int NICKNAME_MIN_LENGTH = 0;

/** The Constant FORM_ID. */
public static final String FORM_ID = "formID";

/** The Constant VISITOR_FIRST_NAME_COOKIE. */
public static final String VISITOR_FIRST_NAME_COOKIE = "visitorFirstNamecookie";

/** The Constant ORDER_CONFIRMATION_PAGE. */
public static final String ORDER_CONFIRMATION_PAGE = "orderConfirmation.jsp";

/** The Constant MY_ACCOUNT. */
public static final String MY_ACCOUNT = "my account";

/** The Constant SOS_EMP_NUMBER. */

public static final String SOS_EMP_NUMBER = "sosEmpNumber";
/** The Constant SOS_EMP_NAME. */

public static final String SOS_EMP_NAME = "sosEmpName";
/** The Constant SOS_STORE_NUMBER. */

public static final String SOS_STORE_NUMBER = "sosStoreNumber";

/** The Constant AOS. */
public static final String AOS = "aos";

/** The Constant SOS. */
public static final String SOS = "sos";

/** The Constant TOYSRUS_DOMAIN. */
public static final String TOYSRUS_DOMAIN = ".toysrus.com";

/** The Constant ALTERNATE_DOMAIN. */
public static final String ALTERNATE_DOMAIN = "alternateDomain";

/** The Constant BABIESRUS_DOMAIN. */
public static final String BABIESRUS_DOMAIN = ".babiesrus.com";

/** property to hold the REST_CONTEXT_PATH. */
public static final String REST_CONTEXT_PATH = "/rest";

/** The Constant IS_SOS_REQUEST. */
public static final String IS_SOS_REQUEST="isSOSRequest";

/**
 * The Constant EDD_SHIP_MIN
 */
public static final String EDD_SHIP_MIN="EDD_SHIP_MIN";

/**
 * The Constant EDD_SHIP_MAX
 */
public static final String  EDD_SHIP_MAX="EDD_SHIP_MAX";

/**
 * The Constant REPRICE_ORDER
 */
public static final String  REPRICE_ORDER = "repriceOrder";

/** The Constant NEXT_LINE. */
public static final String NEXT_LINE = "\n";
/** The Constant SKU_ID_EQUALS. */
public static final String SKU_ID_EQUALS = "SkuId=";
/** The Constant COMMA_STORE_ID_EQUALS. */
public static final String COMMA_STORE_ID_EQUALS = ",storeId=";
/** The Constant COMMA_HAND_QTY_EQUALS. */
public static final String COMMA_HAND_QTY_EQUALS = ",onHandQty=";
/** The Constant COMMA_DELIVERY_EQUALS. */
public static final String COMMA_DELIVERY_EQUALS = ",delivery=";


/** The Constant DESKTOP_VIEW_COOKIE. */

public static final String DESKTOP_VIEW_COOKIE = "desktopView";
/** The Constant DESKTOP_SITE_ACTIVE. */

public static final String DESKTOP_SITE_ACTIVE = "desktopSiteActive";

/**
*  The Constant to hold TOYSRUS_SITE
*/
public static final String TOYSRUS_SITE= "ToysRUs";

/**
*  The Constant to hold BABYSRUS_SITE
*/
public static final String BABYSRUS_SITE= "BabyRUs";

/**
 * The Constant to hold IS_TRUSESSION_COOKIE
 */
public static final String IS_TRUSESSION_COOKIE = "isTRUSessCookie";

/**
*  The Constant to hold TAXWARE
*/
public static final String TAXWARE = "taxware";

/**
*  The Constant to hold HTTP_STRING
*/
public static final String HTTP_STRING = "http://";

/** The Constant REPOSITORY_ITEM. */
public static final String REPOSITORY_ITEM = "repositoryItem";

/** The Constant MESSAGE_SUBJECT. */
public static final String MESSAGE_SUBJECT = "messageSubject";

/** The Constant PROMOTION_LIST. */
public static final String TENDER_LIST = "TenderList";

/** The Constant PROMOTION_LIST. */
public static final String PROMOTION_LIST = "PromotionList";

/** The Constant REQUEST_ORIGIN. */
public static final String REQUEST_ORIGIN = "origin";
/**
 * Constant for API_CHANNEL.
 **/
public static final String API_CHANNEL = "X-APP-API_CHANNEL";

/** The Constant WISHLIST. */
public static final String WISHLIST_CHANNEL_TYPE="wishlist";

/** The Constant REGISTRY_CHANNEL_TYPE. */
public static final String REGISTRY_CHANNEL_TYPE="registry";
/** The Constant TARGET_ITEM. */
public static final String TARGET_ITEM = "TARGET";
/** The Constant QUALIFIED_ITEM. */
public static final String QUALIFIED_ITEM = "QUALIFY";
public static final String PROMOTION_RESOURCES="atg.commerce.promotion.PromotionResources";

/** The Constant NULL_PROFILE. */
public static final String NULL_PROFILE="nullProfile";

/** The Constant NULL_PROMTION. */
public static final String NULL_PROMTION="nullPromotion";

/** The Constant EXPIRED_PROMOTION. */
public static final String EXPIRED_PROMOTION="expiredPromotion";

/** The Constant EXPIRED_PROMTION_RES. */
public static final String EXPIRED_PROMTION_RES="expiredPromotionUserResource";

/** The Constant ANONY_PROFILE. */
public static final String ANONY_PROFILE="anonymousProfile";

/** The Constant ANONY_PROFILE_RES. */
public static final String ANONY_PROFILE_RES="anonymousProfileUserResource";

/** The Constant PROMO_REMOVED. */
public static final String PROMO_REMOVED="promotionRemoved";

/** The Constant EXISTING_PROM_FOR. */
public static final String EXISTING_PROM_FOR="existing promotions for ";

/** The Constant REJECTED_BECAUSE. */
public static final String REJECTED_BECAUSE=" rejected because user ";

/** The Constant ACTIVE_PROMO_LIST. */
public static final String ACTIVE_PROMO_LIST=" has it in the active promotions list";

/** The Constant PROMTION. */
public static final String PROMTION="promotion ";

/** The Constant DUPLICATE_PROMOTION. */
public static final String DUPLICATE_PROMOTION="duplicatePromotion";

/** The Constant DUPLICATE_PROMOTION_RES. */
public static final String DUPLICATE_PROMOTION_RES="duplicatePromotionUserResource";

/** The Constant USED_PROMOTION. */
public static final String USED_PROMOTION="usedPromotion";

/** The Constant USED_PROMOTION_RES. */
public static final String USED_PROMOTION_RES="usedPromotionUserResource";

/** Constants for Contact US ***/

/** The Constant CUSTOMER_NAME. */
public static final String CUSTOMER_NAME="customerName";

/** The Constant TELEPHONE. */
public static final String TELEPHONE="telephone";

/** The Constant SUBJECT. */
public static final String SUBJECT="subject";

/** The Constant EMAIL_ADDR. */
public static final String EMAIL_ADDR="email";

/** The Constant ORDER_NO. */
public static final String ORDER_NO="order";

/** The Constant QUESTIONS. */
public static final String QUESTIONS="questions";

/** The Constant QUESTIONS. */
public static final String CONTACT_US="Contact US Email";

/** The Constant MAX LIMIT. */
public static final int MAX_LIMIT=3;

/** The Constant ORDER_ID_PREPEND. */
public static final String ORDER_ID_PREPEND = "COI-";

/**   Constant for SITE_IDS. */
public static final String SITE_IDS = "siteIds";

/** The Constant PROF_ID. */
public static final String PROF_ID = "Profile Id:{0}.";

/** The Constant CLAIMING_COUPON_NOTALLOWED. */
public static final String CLAIMING_COUPON_NOTALLOWED = "claimingCouponNotAllowed";

/** The Constant NOCOUPONUSES. */
public static final String NO_COUPON_USES = "noCouponUses";

/** The Constant PROMOTION_PROPERTY. */
public static final String PROMOTION_PROPERTY = "Promotion property name:{0}.";

/** The Constant MULTIPLE_PROMOTIONS. */
public static final String MULTIPLE_PROMOTIONS = "Multiple promotions for this coupon.";

/** The Constant PROMOTION_DESCRIPTOR. */
public static final String PROMOTION_DESCRIPTOR="Promotion descriptor name:{0}.";

/** The Constant PROMOTION_TO_BE_GRANTED. */
public static final String PROMOTION_TO_BE_GRANTED="Promotion to be granted:";

/** The Constant NO_COUPON_PROMOTIONS. */
public static final String NO_COUPON_PROMOTIONS="noCouponPromotions";

/** The Constant TSP_CARD_NUMBER. */
public static final String TSP_CARD_NUMBER="tspCardNumber";

/** The Constant DAYS_IN_MILLISECONDS. */
public static final int DAYS_IN_MILLISECONDS=1000 * 60 * 60 * 24;

/** The Constant IS_CSC_REQUEST. */
public static final String IS_CSC_REQUEST = "isCSCRequest";


/** The Constant EXPIRATION_DAY_OF_MONTH. */
public static final String EXPIRATION_DAY_OF_MONTH="expirationDayOfMonth";

/** The Constant BILLING_ADDRESS. */
public static final String BILLING_ADDRESS="billingAddress";

/** The Constant NAME_ON_CARD. */
public static final String NAME_ON_CARD = "nameOnCard";

/** The Constant ALL_CREDIT_CARDS. */
public static final String ALL_CREDIT_CARDS = "allCreditCards";

/** Holds Collection ID. */
public static final String COLLECTION_ID = "collectionId";


/** Holds Collection . */
public static final String COLLECTION = "Collection";
/** Holds PDPNme . */
public static final String PDP = "pdp";

/** Property to hold couponId. */
public static final String COUPON_ID = "couponId";

/** Property to hold MESSAGE_VALUE. */
public static final String MESSAGE_VALUE = "messageValue";

/** Property to hold REPORT_TYPE. */
public static final String REPORT_TYPE = "reportType";

/** Property to hold accept. */
public static final String ACCEPT = "accept";

/** Property to hold application/x-www-form-urlencoded. */
public static final String APPLICATION_FORM_URLENCODED = "application/x-www-form-urlencoded";

/** Property to hold remoteip. */
public static final String REMOTE_IP = "remoteip";

/** Property to hold secret. */
public static final String SECRET = "secret";

/** The Constant INVENTORY_MAP. */
public static final String INVENTORY_MAP = "inventorymap";
/** The Constant ADD_TO_CART_DETAIL_MAP. */
public static final String ADD_TO_CART_DETAIL_MAP = "addToCartDetailMap";

/** The Constant ONE_STR. */
public static final String ONE_STR = "1";

/** Property to hold PIPE_DELIMETER. */
public static final String PIPE_DELIMETER = "\\|";
/** Property to hold PROMOTION_QUALIFIER_ITEM_LABEL. */
public static final String PROMOTION_QUALIFIER_ITEM_LABEL = "qual";
/** Property to hold PROMOTION_TARGETER_ITEM_LABEL. */
public static final String PROMOTION_TARGETER_ITEM_LABEL = "tar";

/** Property to hold PURCHASE_PROCESS_RESOURCES. */
public static final String PURCHASE_PROCESS_RESOURCES = "atg.commerce.order.purchase.PurchaseProcessResources";

/** Property to hold ERROR_REPRICING_ORDER_AFTER_ADD_ITEM. */
public static final String ERROR_REPRICING_ORDER_AFTER_ADD_ITEM = "errorRepricingOrderAfterAddItem";

/** Constant to hold BLANK_CHARACTER */
public static final char BLANK_CHARACTER = ' ';

/**
 * The constant to hold APPLICABLE_SHIP_METHODS
 */
public static final String VALID_SHIP_METHODS = "validShipMethods";
/**
 * The constant to hold APPLE_BUY_FLOW
 */
public static final String  APPLE_BUY_FLOW= "apBuyNowFlow";
/**
 * The constant to hold APPLE_BUY_NOW
 */
public static final String  APPLE_BUY_NOW= "applePayBuyNow";

/** Property to hold MAILABILITY_SCORE. */
public static final String MAILABILITY_SCORE = "MailabilityScore";

/** Property to hold ELEMENT_STATUS_RESULT. */
public static final String ELEMENT_STATUS_RESULT = "ElementResultStatus";

/** Property to hold AD_PROCESS_STATUS. */
public static final String AD_PROCESS_STATUS = "ADProcessStatus";

/** Property to hold DERIVED_STATUS. */
public static final String DERIVED_STATUS = "DerivedStatus";

/** Property to hold NO. */
public static final String NO = "NO";

/** Property to hold NO. */
public static final String YES = "YES";
/** Property to hold PREFERRED_SHIP_METHOD. */

public static final String PREFERRED_SHIP_METHOD = "selectedShippingMethod";
/** Property to hold NUMERIC_FOUR. */
public static final int NUMERIC_FOUR = 4;

/** The Constant NEXT. */
public static final String NEXT ="next";

/** The Constant NUMBER. */
public static final String NUMBER ="number";
/**
 * Constant for SESSION_ID.
 **/
public static final String SESSION_ID = "sessionID";
/**
 * Constant for visitor_id.
 **/
public static final String VISITOR_ID = "visitorID";
/**
 * Constant for LOGIN_STATUS.
 **/
public static final String LOGIN_STATUS = "loginStatus";
/** 
 * The Constant CUST_STATUS_COOKIE.
 *  */
public static final String CUST_STATUS_COOKIE="custStatusCookie";

/** 
 * The Constant CUST_IP_COOKIE.
 *  */
public static final String CUST_IP_COOKIE="customerIpAddress";
/** 
 * The Constant PARTIAL_MATCH.
 *  */
public static final String PARTIAL_MATCH="partialMatch";

/** 
 * The Constant ADDRESS_COMPONENTS.
 *  */
public static final String ADDRESS_COMPONENTS="addressComponents";
/** 
 * The Constant LOCALITY_ADDRESS_VAL.
 *  */
public static final String LOCALITY_ADDRESS_VAL="LOCALITY";
/** 
 * The Constant ADMINISTRATIVE_AREA_LEVEL_1.
 *  */
public static final String ADMINISTRATIVE_AREA_LEVEL_1="ADMINISTRATIVE_AREA_LEVEL_1";
/** 
 * The Constant ERROR_CODE.
 *  */
public static final String ERROR_CODE="errorcode";
/** 
 * The Constant COUNTRY_ADDRESS_VAL.
 *  */
public static final String COUNTRY_ADDRESS_VAL="COUNTRY";
/** 
 * The Constant SHORT_NAME.
 *  */
public static final String SHORT_NAME="shortName";
/** 
 * The Constant LONG_NAME.
 *  */
public static final String LONG_NAME="longName";

/** 
 * The Constant POSTAL_CODE_ADDRESS_VAL.
 *  */
public static final String POSTAL_CODE_ADDRESS_VAL="POSTAL_CODE";

/** 
 * The Constant TYPES.
 *  */
public static final String TYPES="types";
/** 
 * The Constant ERROR_VALUE.
 *  */
public static final String ERROR_VALUE="errorValue";
/**
 * Constant for OUT PARAM.
 */
public static final String INVALID_COMBINATION = "INVALID_SHIPPING_CITY_STATE_POSTAL_COMBINATION";
/**
 * Constant for OUT REQUIRED_FIELD_MESSAGE.
 */
public static final String REQUIRED_FIELD_MESSAGE = "Please enter all the required parameters.";

/** Property to hold REGISTRY_SERVICE. */
public static final String REGISTRY_SERVICE = "registryService";

/** Property to hold REGISTRY_SERVICE. */
public static final String WISHLIST_SERVICE = "wishlistService";

/**
 * Constant for ORDER_ID.
 */
public static final String ORDER_ID = "orderId";

/**
 * Constant for USER_ID.
 */
public static final String USER_ID = "userId";

/**
 * Constant for TOKEN_SMALL.
 */
public static final String TOKEN_SMALL = "token";

/** Property to hold ADD_REGISTRY. */
public static final String ADD_REGISTRY = "addRegistry";

/** Property to hold GET_WISHLIST_DETAILS. */
public static final String GET_WISHLIST_DETAILS = "getWishlistDetails";


/** The Constant holds the STR_REPPONSE_CODE_FOR_LOG_MONITORING. */
public static final String STR_REPPONSE_CODE_FOR_LOG_MONITORING ="ERROR";


/** The Constant holds the STR_ERROR_VALUE_FOR_LOG_MONITORING. */
public static final String STR_ERROR_VALUE_FOR_LOG_MONITORING ="Connection timeout,Response code=500";

/** The Constant holds the AddWishlist. */
public static final String ADD_WISHLIST ="addWishlist";

/** The Constant holds the PRICE_MAP. */
public static final String PRICE_MAP ="priceMap";

/** The Constant holds the PRICEMAP_JSON. */
public static final String PRICEMAP_JSON ="priceMapJSON";

/** The Constant holds the COPIED_DURING_LOGIN. */
public static final String COPIED_DURING_LOGIN ="copiedDuringLogin";

/** The Constant holds the REVOKED_DURING_LOGIN. */
public static final String REVOKED_DURING_LOGIN ="revokedDuringLogin";

/**
 * Constant to hold HANDLE_GET_NONCE.
*/
public static final String HANDLE_GET_NONCE = "handleGetNONCE";

/**
 * Constant to hold HANDLE_RETAIN_PICKUP_DETAILS.
*/
public static final String HANDLE_RETAIN_PICKUP_DETAILS = "handleRetainPickUpDetails";

/**
 * Constant for RESULT_CODE_FAILED.
 */
public static final String  RESULT_CODE_FAILED = "FAILED";
/**
 * Constant for NEXTVAL.
 */
public static final String NEXTVAL = "NEXTVAL";

/**
 * Constant for mobile.
 */
public static final String MOBILE = "mobile";

/**
 * Constant for RADIAL_OMS.
 */
public static final String RADIAL_OMS = "radialOMS";

/**
 * Constant for LAYAWAY_ORDER_HISTORY.
 */
public static final String LAYAWAY_ORDER_HISTORY = "layawayOrderHistory";

/**
 * Constant for LAYAWAY_ORDER_HISTORY.
 */
public static final String LAYAWAY_ORDER_DETAIL = "layawayOrderDetail";

/**
 * Constant for shopBy.
 */
public static final String SHOP_BY = "shopBy";
/**
 * Constant for filterString.
 */
public static final String FILTER_STRING = "filterString";

/** The Constant STR_CART_PAGE. */
public static final String STR_CART_PAGE = "Cart";

/** Property to hold  page. */
public static final String PAGE = "page";

/**
* Constant for holding the PAYMENT.
*/
public static final String SHIPPING ="Shipping";

/**
Constant for holding the Layaway.
*/
public static final String LAYAWAY = "Layaway";

/**
Constant for holding the MyAccount.
*/
public static final String MY_ACCOUNT_PAGE = "MyAccount";
/**
Constant for holding the CUSTOMER_PROFILE.
*/
public static final CharSequence CUSTOMER_PROFILE = "CustomerProfile";
/**
Constant for holding the SPLIT_UNDERSCORE.
*/
public static final String SPLIT_UNDERSCORE = "\\_";

/**
Constant for holding the INT_FIVE.
*/
public static final int INT_FIVE = 5;

/** The Constant UPDATE_ADDRESS. */
public static final String UPDATE_ADDRESS  = "UpdateAddress";

/** The Constant ADD_ADDRESS. */
public static final String ADD_ADDRESS = "AddAddress";

/** The Constant TASK_NAME. */
public static final String TASK_NAME = "Task Name";
/** The Constant DISCOUNT_STRUCTURE. */
public static final String DISCOUNT_STRUCTURE = "discountStructure";

/** The Constant END_DISCOUNT_STRUCTURE. */
public static final String END_DISCOUNT_STRUCTURE = "endDiscountStructure";

/** The Constant BULK. */
public static final String BULK = "bulk";

/** The Constant PROJECTNAME. */
public static final String PROJECTNAME = "projectName";
}