package com.tru.common;

import java.util.HashMap;
import java.util.Map;

import atg.nucleus.GenericService;

/**
 * This class is used to hold the configurable tealium input parameters.
 * @author PA
 * @version 1.0
 */
public class TRUTealiumConfiguration extends GenericService{

	/**
	 * Holds the property value of mOrderSubtotal.
	 */
	private String mOrderSubtotal;
	/**
	 * Holds the property value of mOrderPromotionName.
	 */
	private String mOrderPromotionName;

	/**
	 * Holds the property value of mOrderInternalCampaignPage.
	 */
	private String mOrderInternalCampaignPage;
	/**
	 * Holds the property value of mOrderOrsoCode.
	 */
	private String mOrderOrsoCode;
	/**
	 * Holds the property value of mOrderOutOfStockBySku.
	 */
	private String mOrderOutOfStockBySku;
	/**
	 * Holds the property value of mOrderProductCollection.
	 */
	private String mOrderProductCollection;
	/**
	 * Holds the property value of mOrderOasBreadcrumb.
	 */
	private String mOrderOasBreadcrumb;
	/**
	 * Holds the property value of mOrderOasTaxonomy.
	 */
	private String mOrderOasTaxonomy;
	/**
	 * Holds the property value of mOrderOasSize.
	 */
	private String mOrderOasSize;

	/**
	 * Holds the property value of mOrderProductBrand.
	 */
	private String mOrderProductBrand;
	/**
	 * Holds the property value of mOrderProductCategory.
	 */
	private String mOrderProductCategory;
	/**
	 * Holds the property value of mOrderProductId.
	 */
	private String mOrderProductId;
	/**
	 * Holds the property value of mOrderProductName.
	 */
	private String mOrderProductName;
	/**
	 * Holds the property value of mOrderProductUnitPrice.
	 */
	private String mOrderProductUnitPrice;
	/**
	 * Holds the property value of mOrderProductListPrice.
	 */
	private String mOrderProductListPrice;
	/**
	 * Holds the property value of mOrderProductSku.
	 */
	private String mOrderProductSku;
	/**
	 * Holds the property value of mOrderProductSubcategory.
	 */
	private String mOrderProductSubcategory;
	/**
	 * Holds the property value of mOrderProductFindingMethod.
	 */
	private String mOrderProductFindingMethod;
	/**
	 * Holds the property value of mOrderProductDiscount.
	 */
	private String mOrderProductDiscount;
	/**
	 * Holds the property value of mOrderProdImageUrl.
	 */
	private String mOrderProdImageUrl;
	/**
	 * Holds the property value of mOrderProductQuantity.
	 */
	private String mOrderProductQuantity;
	/**
	 * Holds the property value of mOrderCurrency.
	 */
	private String mOrderCurrency;
	/**
	 * Holds the property value of mOrderCouponType.
	 */
	private String mOrderCouponType;
	/**
	 * Holds the property value of mOrderType.
	 */
	private String mOrderType;
	/**
	 * Holds the property value of mOrderStore.
	 */
	private String mOrderStore;
	/**
	 * Holds the property value of mOrderTax.
	 */
	private String mOrderTax;
	/**
	 * Holds the property value of mOrderDefaultShipType.
	 */
	private String mOrderDefaultShipType;
	/**
	 * Holds the property value of mOrderShippingSurcharge.
	 */
	private String mOrderShippingSurcharge;
	/**
	 * Holds the property value of mOrderShippingType.
	 */
	private String mOrderShippingType;
	/**
	 * Holds the property value of mOrderShippingCountry.
	 */
	private String mOrderShippingCountry;
	/**
	 * Holds the property value of mOrderShippingDiscount.
	 */
	private String mOrderShippingDiscount;
	/**
	 * Holds the property value of mOrderShipping.
	 */
	private String mOrderShipping;
	/**
	 * Holds the property value of mOrderPaymentAmount.
	 */
	private String mOrderPaymentAmount;
	/**
	 * Holds the property value of mOrderCouponCode.
	 */
	private String mOrderCouponCode;
	/**
	 * Holds the property value of mOrderGiftWrap.
	 */
	private String mOrderGiftWrap;
	/**
	 * Holds the property value of mOrderIspu.
	 */
	private String mOrderIspu;
	/**
	 * Holds the property value of mOrderPaymentType.
	 */
	private String mOrderPaymentType;
	/**
	 * Holds the property value of mOrderID.
	 */
	private String mOrderID;
	/**
	 * Holds the property value of mOrderTotal.
	 */
	private String mOrderTotal;
	/**
	 * Holds the property value of mOrderDiscount.
	 */
	private String mOrderDiscount;
	/**
	 * Holds the property value of mOrderFulfillmentChannel.
	 */
	private String mOrderFulfillmentChannel;
	/**
	 * Holds the property value of mOrderProductMerchCategory.
	 */
	private String mOrderProductMerchCategory;
	/**
	 * Holds the property value of mOrderErrorCode.
	 */
	private String mOrderErrorCode;
	/**
	 * Holds the property value of mOrderErrorMessage.
	 */
	private String mOrderErrorMessage;

	/**
	 * Holds the property value of mSearchAB.
	 */
	private String mSearchAB ;
	/**
	 * Holds the property value of mSearchCID.
	 */
	private String mSearchCID ;
	/**
	 * Holds the property value of mSearchOrsoCode.
	 */
	private String mSearchOrsoCode ;
	/**
	 * Holds the property value of mSearchIspuSource.
	 */
	private String mSearchIspuSource ;
	/**
	 * Holds the property value of mSearchRefinementType.
	 */
	private String mSearchRefinementType ;
	/**
	 * Holds the property value of mSearchRecent.
	 */
	private String mSearchRecent ;
	/**
	 * Holds the property value of mSearchSuccess.
	 */
	private String mSearchSuccess ;
	/**
	 * Holds the property value of mSearchSynonym.
	 */
	private String mSearchSynonym ;
	/**
	 * Holds the property value of mSearchTest.
	 */
	private String mSearchTest ;
	/**
	 * Holds the property value of mSearchRefinementValue.
	 */
	private String mSearchRefinementValue ;
	/**
	 * Holds the property value of mSearchIntCampaignPage.
	 */
	private String mSearchIntCampaignPage ;

	/**
	 * Holds the property value of PageName.
	 */
	private String mSearchPageName ;

	/**
	 * Holds the property value of PageType.
	 */
	private String mSearchPageType ;
	/**
	 * Holds the property value of searchNoResultsPageName.
	 */
	private String mSearchNoResultsPageName ;

	/**
	 * Holds the property value of customerStatus.
	 */
	private String mCustomerStatus ;
	/**
	 * Holds the property value of categoryPageName.
	 */
	private String mCategoryPageName;
	/**
	 * Holds the property value of categoryPageType.
	 */
	private String mCategoryPageType;
	/**
	 * Holds the property value of categoryPageCategory.
	 */
	private String mCategoryPageCategory;
	/**
	 * Holds the property value of categorySiteSection.
	 */
	private String mCategorySiteSection ;
	/**
	 * Holds the property value of browserId.
	 */
	private String mBrowserId ;
	/**
	 * Holds the property value of categoryProductCompared.
	 */
	private String mCategoryProductCompared ;
	/**
	 * Holds the property value of deviceType.
	 */
	private String mDeviceType ;
	/**
	 * Holds the property value of categoryOASBreadcrumb.
	 */
	private String mCategoryOASBreadcrumb;
	/**
	 * Holds the property value of categoryOASTaxonamy.
	 */
	private String mCategoryOASTaxonamy ;
	/**
	 * Holds the property value of categoryOASSizes.
	 */
	private String mCategoryOASSizes ;


	/**
	 * Holds the property value of searchOASBreadcrumb.
	 */
	private String mSearchOASBreadcrumb;
	/**
	 * Holds the property value of searchOASTaxonamy.
	 */
	private String mSearchOASTaxonamy ;
	/**
	 * Holds the property value of searchOASSizes.
	 */
	private String mSearchOASSizes ;

	/**
	 * Holds the property value of searchFinderSortGroup.
	 */
	private String mSearchFinderSortGroup ;

	/**
	 * Holds the property value of homePageName.
	 */
	private String mHomePageName;
	/**
	 * Holds the property value of homePageType.
	 */
	private String mHomePageType;

	/**
	 * Holds the property value of homeOASBreadcrumb.
	 */
	private String mHomeOASBreadcrumb;
	/**
	 * Holds the property value of homeOASTaxonamy.
	 */
	private String mHomeOASTaxonamy ;
	/**
	 * Holds the property value of homeOASSizes.
	 */
	private String mHomeOASSizes ;

	/**
	 * Holds the property value of subCategoryPageName.
	 */
	private String mSubCategoryPageName;
	/**
	 * Holds the property value of subCategoryPageType.
	 */
	private String mSubCategoryPageType;
	/**
	 * Holds the property value of subCategoryPageCategory.
	 */
	private String mPageSubCategory;
	/**
	 * Holds the property value of subCategorySiteSection.
	 */
	private String mSubCategorySiteSection ;

	/**
	 * Holds the property value of subCategoryOASBreadcrumb.
	 */
	private String mSubCategoryOASBreadcrumb;
	/**
	 * Holds the property value of subCategoryOASTaxonamy.
	 */
	private String mSubCategoryOASTaxonamy ;
	/**
	 * Holds the property value of subCategoryOASSizes.
	 */
	private String mSubCategoryOASSizes ;

	/**
	 * Holds the property value of familyPageName.
	 */
	private String mFamilyPageName;
	/**
	 * Holds the property value of familyPageType.
	 */
	private String mFamilyPageType;

	/**
	 * Holds the property value of familyOASBreadcrumb.
	 */
	private String mFamilyOASBreadcrumb;
	/**
	 * Holds the property value of familyOASTaxonamy.
	 */
	private String mFamilyOASTaxonamy ;
	/**
	 * Holds the property value of familyOASSizes.
	 */
	private String mFamilyOASSizes ;

	/**
	 * Holds the property value of pDPOasBreadcrumb.
	 */
	private String mPDPOasBreadcrumb;
	/**
	 * Holds the property value of pDPOasTaxonomy.
	 */
	private String mPDPOasTaxonomy;
	/**
	 * Holds the property value of pDPOasSize.
	 */
	private String mPDPOasSize;
	/**
	 * Holds the property value of pDPFinderSortGroup.
	 */
	private String mPDPFinderSortGroup;
	/**
	 * Holds the property value of pDPPageCategory.
	 */
	private String mPDPPageCategory;
	/**
	 * Holds the property value of pDPPageSubcategory.
	 */
	private String mPDPPageSubcategory;
	/**
	 * Holds the property value of pDPProductBrand.
	 */
	private String mPDPProductBrand;
	/**
	 * Holds the property value of pDPProductCategory.
	 */
	private String mPDPProductCategory;
	/**
	 * Holds the property value of pDPProductId.
	 */
	private String mPDPProductId;
	/**
	 * Holds the property value of pDPProductName.
	 */
	private String mPDPProductName;
	/**
	 * Holds the property value of pDPProductUnitPrice.
	 */
	private String mPDPProductUnitPrice;
	/**
	 * Holds the property value of pDPProductListPrice.
	 */
	private String mPDPProductListPrice;
	/**
	 * Holds the property value of pDPProductSku.
	 */
	private String mPDPProductSku;
	/**
	 * Holds the property value of pDPProductSubcategory.
	 */
	private String mPDPProductSubcategory;
	/**
	 * Holds the property value of pDPProductFindingMethod.
	 */
	private String mPDPProductFindingMethod;
	/**
	 * Holds the property value of pDPProductDiscount.
	 */
	private String mPDPProductDiscount;
	/**
	 * Holds the property value of pDPAb.
	 */
	private String mPDPAb;
	/**
	 * Holds the property value of pDPCid.
	 */
	private String mPDPCid;
	/**
	 * Holds the property value of pDPImageIcon.
	 */
	private String mPDPImageIcon;
	/**
	 * Holds the property value of pDPInternalCampaignPage.
	 */
	private String mPDPInternalCampaignPage;
	/**
	 * Holds the property value of pDPOrsoCode.
	 */
	private String mPDPOrsoCode;
	/**
	 * Holds the property value of pDPOutOfStockBySku.
	 */
	private String mPDPOutOfStockBySku;
	/**
	 * Holds the property value of pDPProductCollection.
	 */
	private String mPDPProductCollection;
	/**
	 * Holds the property value of pDPProdMerchCategory.
	 */
	private String mPDPProdMerchCategory;
	/**
	 * Holds the property value of pDPProductPathing.
	 */
	private String mPDPProductPathing;
	/**
	 * Holds the property value of pDPRegistryOrigin.
	 */
	private String mPDPRegistryOrigin;
	/**
	 * Holds the property value of pDPStrikedPricePage.
	 */
	private String mPDPStrikedPricePage;
	/**
	 * Holds the property value of pDPStrikedPricePageTitle.
	 */
	private String mPDPStrikedPricePageTitle;
	/**
	 * Holds the property value of pDPXsellProduct.
	 */
	private String mPDPXsellProduct;
	/**
	 * Holds the property value of pDPIspuSource.
	 */
	private String mPDPIspuSource;

	/**
	 * Holds the property value of pDPProdImageUrl.
	 */
	private String mPDPProdImageUrl;

	/**
	 * Holds the property value of pDPSiteSection.
	 */
	private String mPDPSiteSection;

	/**
	 * Holds the property value of pdpPageName.
	 */
	private String mPDPPageName;

	/**
	 * Holds the property value of pdpPageType.
	 */
	private String mPDPPageType;

	/**
	 * Holds the property value of accountPageName.
	 */
	private String mAccountPageName;
	/**
	 * Holds the property value of accountPageType.
	 */
	private String mAccountPageType;
	/**
	 * Holds the property value of accountAb.
	 */
	private String mAccountAb;
	/**
	 * Holds the property value of accountCid.
	 */
	private String mAccountCid;
	/**
	 * Holds the property value of accountOrsoCode.
	 */
	private String mAccountOrsoCode;

	/**
	 * Holds the property value of accountInternalCampaignPage.
	 */
	private String mAccountInternalCampaignPage;

	/**
	 * Holds the property value of accountInternalCampaignPage.
	 */
	private String mAccountIspuSource;

	/**
	 * Holds the property value of tealium URL.
	 */
	private String mTealiumURL;

	/**
	 * Holds the property value of tealium sync URL.
	 */
	private String mTealiumSyncURL;

	/**
	 * Holds the property value of customer City.
	 */
	private String mCustomerCity;
	/**
	 * Holds the property value of customer Country.
	 */
	private String mCustomerCountry;
	/**
	 * Holds the property value of customer Email.
	 */
	private String mCustomerEmail;
	/**
	 * Holds the property value of customer Id.
	 */
	private String mCustomerId;
	/**
	 * Holds the property value of customer Type.
	 */
	private String mCustomerType;
	/**
	 * Holds the property value of customer State.
	 */
	private String mCustomerState;
	/**
	 * Holds the property value of customer Zip.
	 */
	private String mCustomerZip;
	/**
	 * Holds the property value of customer DOB.
	 */
	private String mCustomerDob;

	/**
	 * Holds the property value of ab.
	 */
	private String mAb;
	/**
	 * Holds the property value of cid.
	 */
	private String mCid;
	/**
	 * Holds the property value of internal Campaign Page.
	 */
	private String mInternalCampaignPage;
	/**
	 * Holds the property value of Orson Code.
	 */
	private String mOrsoCode;
	/**
	 * Holds the property value of isp Source.
	 */
	private String mIspuSource;
	/**
	 * Holds the property value of site Code.
	 */
	private String mBabySiteCode;

	/**
	 * Holds the property value of productMerchandisingCategory.
	 */
	private String mCheckoutProductMerchandisingCategory ;
	/**
	 * Holds the property value of promotionName.
	 */
	private String mCheckoutPromotionName ;
	/**
	 * Holds the property value of stsRevenue.
	 */
	private String mCheckoutStsRevenue ;

	/**
	 * Holds the property value of PageName.
	 */
	private String mGiftFinderPageName;
	/**
	 * Holds the property value of PageType.
	 */
	private String mGiftFinderPageType;
	/**
	 * Holds the property value of giftFinderFinderSortGroup.
	 */
	private String mGiftFinderFinderSortGroup;
	/**
	 * Holds the property value of giftFinderFinderType.
	 */
	private String mGiftFinderFinderType;

	/**
	 * Holds the property value of PageName.
	 */
	private String mQuickviewPageName;
	/**
	 * Holds the property value of PageType.
	 */
	private String mQuickviewPageType;
	/**
	 * Holds the property value of ProductCategory.
	 */
	private String mQuickviewProductCategory;
	/**
	 * Holds the property value of ProductId.
	 */
	private String mQuickviewProductId;
	/**
	 * Holds the property value of ProductName.
	 */
	private String mQuickviewProductName;
	/**
	 * Holds the property value of ProductUnitPrice.
	 */
	private String mQuickviewProductUnitPrice;
	/**
	 * Holds the property value of ProductSku.
	 */
	private String mQuickviewProductSku;
	/**
	 * Holds the property value of ProductCollection.
	 */
	private String mQuickviewProductCollection;
	/**
	 * Holds the property value of ProductMerchandisingCategory.
	 */
	private String mQuickviewProductMerchandisingCategory;
	/**
	 * Holds the property value of RegistryOrigin.
	 */
	private String mQuickviewRegistryOrigin;
	/**
	 * Holds the property value of StrikedPricePage.
	 */
	private String mQuickviewStrikedPricePage;
	/**
	 * Holds the property value of ProductBrand.
	 */
	private String mQuickviewProductBrand;
	/**
	 * Holds the property value of ProductImageUrl.
	 */
	private String mQuickviewProductImageUrl;
	/**
	 * Holds the property value of ProductListPrice.
	 */
	private String mQuickviewProductListPrice;
	/**
	 * Holds the property value of ProductSubcategory.
	 */
	private String mQuickviewProductSubcategory;
	/**
	 * Holds the property value of ProductFindingMethod.
	 */
	private String mQuickviewProductFindingMethod;
	/**
	 * Holds the property value of ProductDiscount.
	 */
	private String mQuickviewProductDiscount;
	/**
	 * Holds the property value of InternalCampaignPage.
	 */
	private String mQuickviewInternalCampaignPage;
	/**
	 * Holds the property value of OutOfStockBySku.
	 */
	private String mQuickviewOutOfStockBySku;

	/**
	 * Holds the property value of cartOasBreadcrumb.
	 */
	private String mCartOasBreadcrumb;
	/**
	 * Holds the property value of cartOasTaxonomy.
	 */
	private String mCartOasTaxonomy;
	/**
	 * Holds the property value of cartOasSize.
	 */
	private String mCartOasSize;
	/**
	 * Holds the property value of cartFinderSortGroup.
	 */
	private String mCartFinderSortGroup;
	/**
	 * Holds the property value of cartPageCategory.
	 */
	private String mCartPageCategory;
	/**
	 * Holds the property value of cartPageSubcategory.
	 */
	private String mCartPageSubcategory;
	/**
	 * Holds the property value of cartProductBrand.
	 */
	private String mCartProductBrand;
	/**
	 * Holds the property value of cartProductCategory.
	 */
	private String mCartProductCategory;
	/**
	 * Holds the property value of cartProductId.
	 */
	private String mCartProductId;
	/**
	 * Holds the property value of cartProductName.
	 */
	private String mCartProductName;
	/**
	 * Holds the property value of cartProductUnitPrice.
	 */
	private String mCartProductUnitPrice;
	/**
	 * Holds the property value of cartProductListPrice.
	 */
	private String mCartProductListPrice;
	/**
	 * Holds the property value of cartProductSku.
	 */
	private String mCartProductSku;
	/**
	 * Holds the property value of cartProductSubcategory.
	 */
	private String mCartProductSubcategory;
	/**
	 * Holds the property value of cartProductFindingMethod.
	 */
	private String mCartProductFindingMethod;
	/**
	 * Holds the property value of cartProductDiscount.
	 */
	private String mCartProductDiscount;
	/**
	 * Holds the property value of cartAb.
	 */
	private String mCartAb;
	/**
	 * Holds the property value of cartCid.
	 */
	private String mCartCid;
	/**
	 * Holds the property value of cartImageIcon.
	 */
	private String mCartImageIcon;
	/**
	 * Holds the property value of cartInternalCampaignPage.
	 */
	private String mCartInternalCampaignPage;
	/**
	 * Holds the property value of cartOrsoCode.
	 */
	private String mCartOrsoCode;
	/**
	 * Holds the property value of cartOutOfStockBySku.
	 */
	private String mCartOutOfStockBySku;
	/**
	 * Holds the property value of cartProductCollection.
	 */
	private String mCartProductCollection;
	/**
	 * Holds the property value of cartProductPathing.
	 */
	private String mCartProductPathing;
	/**
	 * Holds the property value of cartRegistryOrigin.
	 */
	private String mCartRegistryOrigin;
	/**
	 * Holds the property value of cartXsellProduct.
	 */
	private String mCartXsellProduct;
	/**
	 * Holds the property value of cartIspuSource.
	 */
	private String mCartIspuSource;

	/**
	 * Holds the property value of cartProdImageUrl.
	 */
	private String mCartProdImageUrl;

	/**
	 * Holds the property value of cartSiteSection.
	 */
	private String mCartSiteSection;

	/**
	 * Holds the property value of mCartProductQuantity.
	 */
	private String mCartProductQuantity ;
	/**
	 * Holds the property value of mCartPromotionName.
	 */
	private String mCartPromotionName ;

	/**
	 * Holds the property value of mCartEconomyShipping.
	 */
	private String mCartEconomyShipping ;
	/**
	 * Holds the property value of mCartStsRevenue.
	 */
	private String mCartStsRevenue ;
	/**
	 * Holds the property value of mCartOrderSubtotal.
	 */
	private String mCartOrderSubtotal ;
	/**
	 * Holds the property value of mCartOrderCurrency.
	 */
	private String mCartOrderCurrency ;

	/**
	 * Holds the property value of checkoutPageName.
	 */
	private String mCheckoutPageName ;
	/**
	 * Holds the property value of reviewPageName.
	 */
	private String mReviewPageName ;
	/**
	 * Holds the property value of reviewPageName.
	 */
	private String mPaymentPageName ;
	
	/**
	 * Holds the property value of checkoutPageType.
	 */
	private String mCheckoutPageType ;
	/**
	 * Holds the property value of quickviewStrikedPricePageTitle.
	 */
	private String mQuickviewStrikedPricePageTitle;
	/**
	 * Holds the property value of storeLocatorLandingPageName.
	 */
	private String mStoreLocatorLandingPageName;
	/**
	 * Holds the property value of storeLocatorLandingPageType.
	 */
	private String mStoreLocatorLandingPageType;

	/**
	 * Holds the property value of shopbyPageName.
	 */
	private String mShopbyPageName;
	/**
	 * Holds the property value of shopbyPageType.
	 */
	private String mShopbyPageType;
	/**
	 * Holds the property value of cartPageName.
	 */
	private String mCartPageName;
	/**
	 * Holds the property value of cartPageType.
	 */
	private String mCartPageType;
	/**
	 * Holds the property value of site Code.
	 */
	private String mToysSiteCode;

	/**
	 * Holds the property value of partner name.
	 */
	private String mBabyPartnerName;

	/**
	 * Holds the property value of partner name.
	 */
	private String mToysPartnerName;


	/**
	 * Holds the property value of Order History Page.
	 */
	private String mOrderHistoryPage;


	/**
	 * Holds the property value of OrderHistory Page Name.
	 */
	private String mOrderHistoryPageName;
	
	
	/**
	 *  Holds the property value of displayCreditCardTypeName.
	 */
	public Map<String, String> mDisplayCreditCardTypeName = new HashMap<String,String>();

	/**
	 * Gets the tealium sync url.
	 *
	 * @return the tealiumSyncURL.
	 */
	public String getTealiumSyncURL() {
		return mTealiumSyncURL;
	}

	/**
	 * Sets the tealium sync url.
	 *
	 * @param pTealiumSyncURL the tealiumSyncURL to set.
	 */
	public void setTealiumSyncURL(String pTealiumSyncURL) {
		mTealiumSyncURL = pTealiumSyncURL;
	}

	/**
	 * Gets the customer status.
	 *
	 * @return the customerStatus
	 */
	public String getCustomerStatus() {
		return mCustomerStatus;
	}

	/**
	 * Sets the customer status.
	 *
	 * @param pCustomerStatus the customerStatus to set
	 */
	public void setCustomerStatus(String pCustomerStatus) {
		mCustomerStatus = pCustomerStatus;
	}

	/**
	 * Gets the category page name.
	 *
	 * @return the categoryPageName
	 */
	public String getCategoryPageName() {
		return mCategoryPageName;
	}

	/**
	 * Sets the category page name.
	 *
	 * @param pCategoryPageName the categoryPageName to set
	 */
	public void setCategoryPageName(String pCategoryPageName) {
		mCategoryPageName = pCategoryPageName;
	}

	/**
	 * Gets the category page type.
	 *
	 * @return the categoryPageType
	 */
	public String getCategoryPageType() {
		return mCategoryPageType;
	}

	/**
	 * Sets the category page type.
	 *
	 * @param pCategoryPageType the categoryPageType to set
	 */
	public void setCategoryPageType(String pCategoryPageType) {
		mCategoryPageType = pCategoryPageType;
	}

	/**
	 * Gets the category page category.
	 *
	 * @return the categoryPageCategory
	 */
	public String getCategoryPageCategory() {
		return mCategoryPageCategory;
	}

	/**
	 * Sets the category page category.
	 *
	 * @param pCategoryPageCategory the categoryPageCategory to set
	 */
	public void setCategoryPageCategory(String pCategoryPageCategory) {
		mCategoryPageCategory = pCategoryPageCategory;
	}

	/**
	 * Gets the category site section.
	 *
	 * @return the categorySiteSection
	 */
	public String getCategorySiteSection() {
		return mCategorySiteSection;
	}

	/**
	 * Sets the category site section.
	 *
	 * @param pCategorySiteSection the categorySiteSection to set
	 */
	public void setCategorySiteSection(String pCategorySiteSection) {
		mCategorySiteSection = pCategorySiteSection;
	}

	/**
	 * Gets the browser id.
	 *
	 * @return the browserId
	 */
	public String getBrowserId() {
		return mBrowserId;
	}

	/**
	 * Sets the browser id.
	 *
	 * @param pBrowserId the browserId to set
	 */
	public void setBrowserId(String pBrowserId) {
		mBrowserId = pBrowserId;
	}

	/**
	 * Gets the category product compared.
	 *
	 * @return the categoryProductCompared
	 */
	public String getCategoryProductCompared() {
		return mCategoryProductCompared;
	}

	/**
	 * Sets the category product compared.
	 *
	 * @param pCategoryProductCompared the categoryProductCompared to set
	 */
	public void setCategoryProductCompared(String pCategoryProductCompared) {
		mCategoryProductCompared = pCategoryProductCompared;
	}

	/**
	 * Gets the device type.
	 *
	 * @return the deviceType
	 */
	public String getDeviceType() {
		return mDeviceType;
	}

	/**
	 * Sets the device type.
	 *
	 * @param pDeviceType the deviceType to set
	 */
	public void setDeviceType(String pDeviceType) {
		mDeviceType = pDeviceType;
	}

	/**
	 * Gets the category oas breadcrumb.
	 *
	 * @return the categoryOASBreadcrumb
	 */
	public String getCategoryOASBreadcrumb() {
		return mCategoryOASBreadcrumb;
	}

	/**
	 * Sets the category oas breadcrumb.
	 *
	 * @param pCategoryOASBreadcrumb the categoryOASBreadcrumb to set
	 */
	public void setCategoryOASBreadcrumb(String pCategoryOASBreadcrumb) {
		mCategoryOASBreadcrumb = pCategoryOASBreadcrumb;
	}

	/**
	 * Gets the category oas taxonamy.
	 *
	 * @return the categoryOASTaxonamy
	 */
	public String getCategoryOASTaxonamy() {
		return mCategoryOASTaxonamy;
	}

	/**
	 * Sets the category oas taxonamy.
	 *
	 * @param pCategoryOASTaxonamy the categoryOASTaxonamy to set
	 */
	public void setCategoryOASTaxonamy(String pCategoryOASTaxonamy) {
		mCategoryOASTaxonamy = pCategoryOASTaxonamy;
	}

	/**
	 * Gets the category oas sizes.
	 *
	 * @return the categoryOASSizes
	 */
	public String getCategoryOASSizes() {
		return mCategoryOASSizes;
	}

	/**
	 * Sets the category oas sizes.
	 *
	 * @param pCategoryOASSizes the categoryOASSizes to set
	 */
	public void setCategoryOASSizes(String pCategoryOASSizes) {
		mCategoryOASSizes = pCategoryOASSizes;
	}
	/**
	 * This method is used to get searchOASBreadcrumb.
	 * @return searchOASBreadcrumb String
	 */
	public String getSearchOASBreadcrumb() {
		return mSearchOASBreadcrumb;
	}
	/**
	 *This method is used to set searchOASBreadcrumb.
	 *@param pSearchOASBreadcrumb String
	 */
	public void setSearchOASBreadcrumb(String pSearchOASBreadcrumb) {
		mSearchOASBreadcrumb = pSearchOASBreadcrumb;
	}
	/**
	 * This method is used to get searchOASTaxonamy.
	 * @return searchOASTaxonamy String
	 */
	public String getSearchOASTaxonamy() {
		return mSearchOASTaxonamy;
	}
	/**
	 *This method is used to set searchOASTaxonamy.
	 *@param pSearchOASTaxonamy String
	 */
	public void setSearchOASTaxonamy(String pSearchOASTaxonamy) {
		mSearchOASTaxonamy = pSearchOASTaxonamy;
	}
	/**
	 * This method is used to get searchOASSizes.
	 * @return searchOASSizes String
	 */
	public String getSearchOASSizes() {
		return mSearchOASSizes;
	}
	/**
	 *This method is used to set searchOASSizes.
	 *@param pSearchOASSizes String
	 */
	public void setSearchOASSizes(String pSearchOASSizes) {
		mSearchOASSizes = pSearchOASSizes;
	}
	/**
	 * This method is used to get searchFinderSortGroup.
	 * @return searchFinderSortGroup String
	 */
	public String getSearchFinderSortGroup() {
		return mSearchFinderSortGroup;
	}
	/**
	 *This method is used to set searchFinderSortGroup.
	 *@param pSearchFinderSortGroup String
	 */
	public void setSearchFinderSortGroup(String pSearchFinderSortGroup) {
		mSearchFinderSortGroup = pSearchFinderSortGroup;
	}

	/**
	 * Gets the home page name.
	 *
	 * @return the homePageName
	 */
	public String getHomePageName() {
		return mHomePageName;
	}

	/**
	 * Sets the home page name.
	 *
	 * @param pHomePageName the homePageName to set
	 */
	public void setHomePageName(String pHomePageName) {
		mHomePageName = pHomePageName;
	}

	/**
	 * Gets the home page type.
	 *
	 * @return the homePageType
	 */
	public String getHomePageType() {
		return mHomePageType;
	}

	/**
	 * Sets the home page type.
	 *
	 * @param pHomePageType the homePageType to set
	 */
	public void setHomePageType(String pHomePageType) {
		mHomePageType = pHomePageType;
	}

	/**
	 * Gets the home oas breadcrumb.
	 *
	 * @return the homeOASBreadcrumb
	 */
	public String getHomeOASBreadcrumb() {
		return mHomeOASBreadcrumb;
	}

	/**
	 * Sets the home oas breadcrumb.
	 *
	 * @param pHomeOASBreadcrumb the homeOASBreadcrumb to set
	 */
	public void setHomeOASBreadcrumb(String pHomeOASBreadcrumb) {
		mHomeOASBreadcrumb = pHomeOASBreadcrumb;
	}

	/**
	 * Gets the home oas taxonamy.
	 *
	 * @return the homeOASTaxonamy
	 */
	public String getHomeOASTaxonamy() {
		return mHomeOASTaxonamy;
	}

	/**
	 * Sets the home oas taxonamy.
	 *
	 * @param pHomeOASTaxonamy the homeOASTaxonamy to set
	 */
	public void setHomeOASTaxonamy(String pHomeOASTaxonamy) {
		mHomeOASTaxonamy = pHomeOASTaxonamy;
	}

	/**
	 * Gets the home oas sizes.
	 *
	 * @return the homeOASSizes
	 */
	public String getHomeOASSizes() {
		return mHomeOASSizes;
	}

	/**
	 * Sets the home oas sizes.
	 *
	 * @param pHomeOASSizes the homeOASSizes to set
	 */
	public void setHomeOASSizes(String pHomeOASSizes) {
		mHomeOASSizes = pHomeOASSizes;
	}

	/**
	 * Gets the sub category page name.
	 *
	 * @return the subCategoryPageName
	 */
	public String getSubCategoryPageName() {
		return mSubCategoryPageName;
	}

	/**
	 * Sets the sub category page name.
	 *
	 * @param pSubCategoryPageName the subCategoryPageName to set
	 */
	public void setSubCategoryPageName(String pSubCategoryPageName) {
		mSubCategoryPageName = pSubCategoryPageName;
	}

	/**
	 * Gets the sub category page type.
	 *
	 * @return the subCategoryPageType
	 */
	public String getSubCategoryPageType() {
		return mSubCategoryPageType;
	}

	/**
	 * Sets the sub category page type.
	 *
	 * @param pSubCategoryPageType the subCategoryPageType to set
	 */
	public void setSubCategoryPageType(String pSubCategoryPageType) {
		mSubCategoryPageType = pSubCategoryPageType;
	}

	/**
	 * Gets the page sub category.
	 *
	 * @return the pageSubCategory
	 */
	public String getPageSubCategory() {
		return mPageSubCategory;
	}

	/**
	 * Sets the page sub category.
	 *
	 * @param pPageSubCategory the pageSubCategory to set
	 */
	public void setPageSubCategory(String pPageSubCategory) {
		mPageSubCategory = pPageSubCategory;
	}

	/**
	 * Gets the sub category site section.
	 *
	 * @return the subCategorySiteSection
	 */
	public String getSubCategorySiteSection() {
		return mSubCategorySiteSection;
	}

	/**
	 * Sets the sub category site section.
	 *
	 * @param pSubCategorySiteSection the subCategorySiteSection to set
	 */
	public void setSubCategorySiteSection(String pSubCategorySiteSection) {
		mSubCategorySiteSection = pSubCategorySiteSection;
	}

	/**
	 * Gets the sub category oas breadcrumb.
	 *
	 * @return the subCategoryOASBreadcrumb
	 */
	public String getSubCategoryOASBreadcrumb() {
		return mSubCategoryOASBreadcrumb;
	}

	/**
	 * Sets the sub category oas breadcrumb.
	 *
	 * @param pSubCategoryOASBreadcrumb the subCategoryOASBreadcrumb to set
	 */
	public void setSubCategoryOASBreadcrumb(String pSubCategoryOASBreadcrumb) {
		mSubCategoryOASBreadcrumb = pSubCategoryOASBreadcrumb;
	}

	/**
	 * Gets the sub category oas taxonamy.
	 *
	 * @return the subCategoryOASTaxonamy
	 */
	public String getSubCategoryOASTaxonamy() {
		return mSubCategoryOASTaxonamy;
	}

	/**
	 * Sets the sub category oas taxonamy.
	 *
	 * @param pSubCategoryOASTaxonamy the subCategoryOASTaxonamy to set
	 */
	public void setSubCategoryOASTaxonamy(String pSubCategoryOASTaxonamy) {
		mSubCategoryOASTaxonamy = pSubCategoryOASTaxonamy;
	}

	/**
	 * Gets the sub category oas sizes.
	 *
	 * @return the subCategoryOASSizes
	 */
	public String getSubCategoryOASSizes() {
		return mSubCategoryOASSizes;
	}

	/**
	 * Sets the sub category oas sizes.
	 *
	 * @param pSubCategoryOASSizes the subCategoryOASSizes to set
	 */
	public void setSubCategoryOASSizes(String pSubCategoryOASSizes) {
		mSubCategoryOASSizes = pSubCategoryOASSizes;
	}

	/**
	 * Gets the family page name.
	 *
	 * @return the familyPageName
	 */
	public String getFamilyPageName() {
		return mFamilyPageName;
	}

	/**
	 * Sets the family page name.
	 *
	 * @param pFamilyPageName the familyPageName to set
	 */
	public void setFamilyPageName(String pFamilyPageName) {
		mFamilyPageName = pFamilyPageName;
	}

	/**
	 * Gets the family page type.
	 *
	 * @return the familyPageType
	 */
	public String getFamilyPageType() {
		return mFamilyPageType;
	}

	/**
	 * Sets the family page type.
	 *
	 * @param pFamilyPageType the familyPageType to set
	 */
	public void setFamilyPageType(String pFamilyPageType) {
		mFamilyPageType = pFamilyPageType;
	}

	/**
	 * Gets the family oas breadcrumb.
	 *
	 * @return the familyOASBreadcrumb
	 */
	public String getFamilyOASBreadcrumb() {
		return mFamilyOASBreadcrumb;
	}

	/**
	 * Sets the family oas breadcrumb.
	 *
	 * @param pFamilyOASBreadcrumb the familyOASBreadcrumb to set
	 */
	public void setFamilyOASBreadcrumb(String pFamilyOASBreadcrumb) {
		mFamilyOASBreadcrumb = pFamilyOASBreadcrumb;
	}

	/**
	 * Gets the family oas taxonamy.
	 *
	 * @return the familyOASTaxonamy
	 */
	public String getFamilyOASTaxonamy() {
		return mFamilyOASTaxonamy;
	}

	/**
	 * Sets the family oas taxonamy.
	 *
	 * @param pFamilyOASTaxonamy the familyOASTaxonamy to set
	 */
	public void setFamilyOASTaxonamy(String pFamilyOASTaxonamy) {
		mFamilyOASTaxonamy = pFamilyOASTaxonamy;
	}

	/**
	 * Gets the family oas sizes.
	 *
	 * @return the familyOASSizes
	 */
	public String getFamilyOASSizes() {
		return mFamilyOASSizes;
	}

	/**
	 * Sets the family oas sizes.
	 *
	 * @param pFamilyOASSizes the familyOASSizes to set
	 */
	public void setFamilyOASSizes(String pFamilyOASSizes) {
		mFamilyOASSizes = pFamilyOASSizes;
	}
	/**
	 * This method is used to get pDPOasBreadcrumb.
	 * @return pDPOasBreadcrumb String
	 */
	public String getPDPOasBreadcrumb() {
		return mPDPOasBreadcrumb;
	}
	/**
	 *This method is used to set pDPOasBreadcrumb.
	 *@param pPDPOasBreadcrumb String
	 */
	public void setPDPOasBreadcrumb(String pPDPOasBreadcrumb) {
		mPDPOasBreadcrumb = pPDPOasBreadcrumb;
	}
	/**
	 * This method is used to get pDPOasTaxonomy.
	 * @return pDPOasTaxonomy String
	 */
	public String getPDPOasTaxonomy() {
		return mPDPOasTaxonomy;
	}
	/**
	 *This method is used to set pDPOasTaxonomy.
	 *@param pPDPOasTaxonomy String
	 */
	public void setPDPOasTaxonomy(String pPDPOasTaxonomy) {
		mPDPOasTaxonomy = pPDPOasTaxonomy;
	}
	/**
	 * This method is used to get pDPOasSize.
	 * @return pDPOasSize String
	 */
	public String getPDPOasSize() {
		return mPDPOasSize;
	}
	/**
	 *This method is used to set pDPOasSize.
	 *@param pPDPOasSize String
	 */
	public void setPDPOasSize(String pPDPOasSize) {
		mPDPOasSize = pPDPOasSize;
	}
	/**
	 * This method is used to get pDPFinderSortGroup.
	 * @return pDPFinderSortGroup String
	 */
	public String getPDPFinderSortGroup() {
		return mPDPFinderSortGroup;
	}
	/**
	 *This method is used to set pDPFinderSortGroup.
	 *@param pPDPFinderSortGroup String
	 */
	public void setPDPFinderSortGroup(String pPDPFinderSortGroup) {
		mPDPFinderSortGroup = pPDPFinderSortGroup;
	}
	/**
	 * This method is used to get pDPPageCategory.
	 * @return pDPPageCategory String
	 */
	public String getPDPPageCategory() {
		return mPDPPageCategory;
	}
	/**
	 *This method is used to set pDPPageCategory.
	 *@param pPDPPageCategory String
	 */
	public void setPDPPageCategory(String pPDPPageCategory) {
		mPDPPageCategory = pPDPPageCategory;
	}
	/**
	 * This method is used to get pDPPageSubcategory.
	 * @return pDPPageSubcategory String
	 */
	public String getPDPPageSubcategory() {
		return mPDPPageSubcategory;
	}
	/**
	 *This method is used to set pDPPageSubcategory.
	 *@param pPDPPageSubcategory String
	 */
	public void setPDPPageSubcategory(String pPDPPageSubcategory) {
		mPDPPageSubcategory = pPDPPageSubcategory;
	}
	/**
	 * This method is used to get pDPProductBrand.
	 * @return pDPProductBrand String
	 */
	public String getPDPProductBrand() {
		return mPDPProductBrand;
	}
	/**
	 *This method is used to set pDPProductBrand.
	 *@param pPDPProductBrand String
	 */
	public void setPDPProductBrand(String pPDPProductBrand) {
		mPDPProductBrand = pPDPProductBrand;
	}
	/**
	 * This method is used to get pDPProductCategory.
	 * @return pDPProductCategory String
	 */
	public String getPDPProductCategory() {
		return mPDPProductCategory;
	}
	/**
	 *This method is used to set pDPProductCategory.
	 *@param pPDPProductCategory String
	 */
	public void setPDPProductCategory(String pPDPProductCategory) {
		mPDPProductCategory = pPDPProductCategory;
	}
	/**
	 * This method is used to get pDPProductId.
	 * @return pDPProductId String
	 */
	public String getPDPProductId() {
		return mPDPProductId;
	}
	/**
	 *This method is used to set pDPProductId.
	 *@param pPDPProductId String
	 */
	public void setPDPProductId(String pPDPProductId) {
		mPDPProductId = pPDPProductId;
	}
	/**
	 * This method is used to get pDPProductName.
	 * @return pDPProductName String
	 */
	public String getPDPProductName() {
		return mPDPProductName;
	}
	/**
	 *This method is used to set pDPProductName.
	 *@param pPDPProductName String
	 */
	public void setPDPProductName(String pPDPProductName) {
		mPDPProductName = pPDPProductName;
	}
	/**
	 * This method is used to get pDPProductUnitPrice.
	 * @return pDPProductUnitPrice String
	 */
	public String getPDPProductUnitPrice() {
		return mPDPProductUnitPrice;
	}
	/**
	 *This method is used to set pDPProductUnitPrice.
	 *@param pPDPProductUnitPrice String
	 */
	public void setPDPProductUnitPrice(String pPDPProductUnitPrice) {
		mPDPProductUnitPrice = pPDPProductUnitPrice;
	}
	/**
	 * This method is used to get pDPProductListPrice.
	 * @return pDPProductListPrice String
	 */
	public String getPDPProductListPrice() {
		return mPDPProductListPrice;
	}
	/**
	 *This method is used to set pDPProductListPrice.
	 *@param pPDPProductListPrice String
	 */
	public void setPDPProductListPrice(String pPDPProductListPrice) {
		mPDPProductListPrice = pPDPProductListPrice;
	}
	/**
	 * This method is used to get pDPProductSku.
	 * @return pDPProductSku String
	 */
	public String getPDPProductSku() {
		return mPDPProductSku;
	}
	/**
	 *This method is used to set pDPProductSku.
	 *@param pPDPProductSku String
	 */
	public void setPDPProductSku(String pPDPProductSku) {
		mPDPProductSku = pPDPProductSku;
	}
	/**
	 * This method is used to get pDPProductSubcategory.
	 * @return pDPProductSubcategory String
	 */
	public String getPDPProductSubcategory() {
		return mPDPProductSubcategory;
	}
	/**
	 *This method is used to set pDPProductSubcategory.
	 *@param pPDPProductSubcategory String
	 */
	public void setPDPProductSubcategory(String pPDPProductSubcategory) {
		mPDPProductSubcategory = pPDPProductSubcategory;
	}
	/**
	 * This method is used to get pDPProductFindingMethod.
	 * @return pDPProductFindingMethod String
	 */
	public String getPDPProductFindingMethod() {
		return mPDPProductFindingMethod;
	}
	/**
	 *This method is used to set pDPProductFindingMethod.
	 *@param pPDPProductFindingMethod String
	 */
	public void setPDPProductFindingMethod(String pPDPProductFindingMethod) {
		mPDPProductFindingMethod = pPDPProductFindingMethod;
	}
	/**
	 * This method is used to get pDPProductDiscount.
	 * @return pDPProductDiscount String
	 */
	public String getPDPProductDiscount() {
		return mPDPProductDiscount;
	}
	/**
	 *This method is used to set pDPProductDiscount.
	 *@param pPDPProductDiscount String
	 */
	public void setPDPProductDiscount(String pPDPProductDiscount) {
		mPDPProductDiscount = pPDPProductDiscount;
	}
	/**
	 * This method is used to get pDPAb.
	 * @return pDPAb String
	 */
	public String getPDPAb() {
		return mPDPAb;
	}
	/**
	 *This method is used to set pDPAb.
	 *@param pPDPAb String
	 */
	public void setPDPAb(String pPDPAb) {
		mPDPAb = pPDPAb;
	}
	/**
	 * This method is used to get pDPCid.
	 * @return pDPCid String
	 */
	public String getPDPCid() {
		return mPDPCid;
	}
	/**
	 *This method is used to set pDPCid.
	 *@param pPDPCid String
	 */
	public void setPDPCid(String pPDPCid) {
		mPDPCid = pPDPCid;
	}
	/**
	 * This method is used to get pDPImageIcon.
	 * @return pDPImageIcon String
	 */
	public String getPDPImageIcon() {
		return mPDPImageIcon;
	}
	/**
	 *This method is used to set pDPImageIcon.
	 *@param pPDPImageIcon String
	 */
	public void setPDPImageIcon(String pPDPImageIcon) {
		mPDPImageIcon = pPDPImageIcon;
	}
	/**
	 * This method is used to get pDPInternalCampaignPage.
	 * @return pDPInternalCampaignPage String
	 */
	public String getPDPInternalCampaignPage() {
		return mPDPInternalCampaignPage;
	}
	/**
	 *This method is used to set pDPInternalCampaignPage.
	 *@param pPDPInternalCampaignPage String
	 */
	public void setPDPInternalCampaignPage(String pPDPInternalCampaignPage) {
		mPDPInternalCampaignPage = pPDPInternalCampaignPage;
	}
	/**
	 * This method is used to get pDPOrsoCode.
	 * @return pDPOrsoCode String
	 */
	public String getPDPOrsoCode() {
		return mPDPOrsoCode;
	}
	/**
	 *This method is used to set pDPOrsoCode.
	 *@param pPDPOrsoCode String
	 */
	public void setPDPOrsoCode(String pPDPOrsoCode) {
		mPDPOrsoCode = pPDPOrsoCode;
	}
	/**
	 * This method is used to get pDPOutOfStockBySku.
	 * @return pDPOutOfStockBySku String
	 */
	public String getPDPOutOfStockBySku() {
		return mPDPOutOfStockBySku;
	}
	/**
	 *This method is used to set pDPOutOfStockBySku.
	 *@param pPDPOutOfStockBySku String
	 */
	public void setPDPOutOfStockBySku(String pPDPOutOfStockBySku) {
		mPDPOutOfStockBySku = pPDPOutOfStockBySku;
	}
	/**
	 * This method is used to get pDPProductCollection.
	 * @return pDPProductCollection String
	 */
	public String getPDPProductCollection() {
		return mPDPProductCollection;
	}
	/**
	 *This method is used to set pDPProductCollection.
	 *@param pPDPProductCollection String
	 */
	public void setPDPProductCollection(String pPDPProductCollection) {
		mPDPProductCollection = pPDPProductCollection;
	}
	/**
	 * This method is used to get pDPProdMerchCategory.
	 * @return pDPProdMerchCategory String
	 */
	public String getPDPProdMerchCategory() {
		return mPDPProdMerchCategory;
	}
	/**
	 *This method is used to set pDPProdMerchCategory.
	 *@param pPDPProdMerchCategory String
	 */
	public void setPDPProdMerchCategory(String pPDPProdMerchCategory) {
		mPDPProdMerchCategory = pPDPProdMerchCategory;
	}
	/**
	 * This method is used to get pDPProductPathing.
	 * @return pDPProductPathing String
	 */
	public String getPDPProductPathing() {
		return mPDPProductPathing;
	}
	/**
	 *This method is used to set pDPProductPathing.
	 *@param pPDPProductPathing String
	 */
	public void setPDPProductPathing(String pPDPProductPathing) {
		mPDPProductPathing = pPDPProductPathing;
	}
	/**
	 * This method is used to get pDPRegistryOrigin.
	 * @return pDPRegistryOrigin String
	 */
	public String getPDPRegistryOrigin() {
		return mPDPRegistryOrigin;
	}
	/**
	 *This method is used to set pDPRegistryOrigin.
	 *@param pPDPRegistryOrigin String
	 */
	public void setPDPRegistryOrigin(String pPDPRegistryOrigin) {
		mPDPRegistryOrigin = pPDPRegistryOrigin;
	}
	/**
	 * This method is used to get pDPStrikedPricePage.
	 * @return pDPStrikedPricePage String
	 */
	public String getPDPStrikedPricePage() {
		return mPDPStrikedPricePage;
	}
	/**
	 *This method is used to set pDPStrikedPricePage.
	 *@param pPDPStrikedPricePage String
	 */
	public void setPDPStrikedPricePage(String pPDPStrikedPricePage) {
		mPDPStrikedPricePage = pPDPStrikedPricePage;
	}
	/**
	 * This method is used to get pDPStrikedPricePageTitle.
	 * @return pDPStrikedPricePageTitle String
	 */
	public String getPDPStrikedPricePageTitle() {
		return mPDPStrikedPricePageTitle;
	}
	/**
	 *This method is used to set pDPStrikedPricePageTitle.
	 *@param pPDPStrikedPricePageTitle String
	 */
	public void setPDPStrikedPricePageTitle(String pPDPStrikedPricePageTitle) {
		mPDPStrikedPricePageTitle = pPDPStrikedPricePageTitle;
	}
	/**
	 * This method is used to get pDPXsellProduct.
	 * @return pDPXsellProduct String
	 */
	public String getPDPXsellProduct() {
		return mPDPXsellProduct;
	}
	/**
	 *This method is used to set pDPXsellProduct.
	 *@param pPDPXsellProduct String
	 */
	public void setPDPXsellProduct(String pPDPXsellProduct) {
		mPDPXsellProduct = pPDPXsellProduct;
	}
	/**
	 * This method is used to get pDPIspuSource.
	 * @return pDPIspuSource String
	 */
	public String getPDPIspuSource() {
		return mPDPIspuSource;
	}
	/**
	 *This method is used to set pDPIspuSource.
	 *@param pPDPIspuSource String
	 */
	public void setPDPIspuSource(String pPDPIspuSource) {
		mPDPIspuSource = pPDPIspuSource;
	}
	/**
	 * This method is used to get pDPProdImageUrl.
	 * @return pDPProdImageUrl String
	 */
	public String getPDPProdImageUrl() {
		return mPDPProdImageUrl;
	}
	/**
	 *This method is used to set pDPProdImageUrl.
	 *@param pPDPProdImageUrl String
	 */
	public void setPDPProdImageUrl(String pPDPProdImageUrl) {
		mPDPProdImageUrl = pPDPProdImageUrl;
	}
	/**
	 * This method is used to get pDPSiteSection.
	 * @return pDPSiteSection String
	 */
	public String getPDPSiteSection() {
		return mPDPSiteSection;
	}
	/**
	 *This method is used to set pDPSiteSection.
	 *@param pPDPSiteSection String
	 */
	public void setPDPSiteSection(String pPDPSiteSection) {
		mPDPSiteSection = pPDPSiteSection;
	}
	/**
	 * This method is used to get accountAb.
	 * @return accountAb String
	 */
	public String getAccountAb() {
		return mAccountAb;
	}
	/**
	 *This method is used to set accountAb.
	 *@param pAccountAb String
	 */
	public void setAccountAb(String pAccountAb) {
		mAccountAb = pAccountAb;
	}
	/**
	 * This method is used to get accountCid.
	 * @return accountCid String
	 */
	public String getAccountCid() {
		return mAccountCid;
	}
	/**
	 *This method is used to set accountCid.
	 *@param pAccountCid String
	 */
	public void setAccountCid(String pAccountCid) {
		mAccountCid = pAccountCid;
	}
	/**
	 * This method is used to get accountOrsoCode.
	 * @return accountOrsoCode String
	 */
	public String getAccountOrsoCode() {
		return mAccountOrsoCode;
	}
	/**
	 *This method is used to set accountOrsoCode.
	 *@param pAccountOrsoCode String
	 */
	public void setAccountOrsoCode(String pAccountOrsoCode) {
		mAccountOrsoCode = pAccountOrsoCode;
	}
	/**
	 * This method is used to get accountInternalCampaignPage.
	 * @return accountInternalCampaignPage String
	 */
	public String getAccountInternalCampaignPage() {
		return mAccountInternalCampaignPage;
	}
	/**
	 *This method is used to set accountInternalCampaignPage.
	 *@param pAccountInternalCampaignPage String
	 */
	public void setAccountInternalCampaignPage(String pAccountInternalCampaignPage) {
		mAccountInternalCampaignPage = pAccountInternalCampaignPage;
	}
	/**
	 * This method is used to get accountIspuSource.
	 * @return accountIspuSource String
	 */
	public String getAccountIspuSource() {
		return mAccountIspuSource;
	}
	/**
	 *This method is used to set accountIspuSource.
	 *@param pAccountIspuSource String
	 */
	public void setAccountIspuSource(String pAccountIspuSource) {
		mAccountIspuSource = pAccountIspuSource;
	}
	/**
	 * This method is used to get tealiumURL.
	 * @return tealiumURL String
	 */
	public String getTealiumURL() {
		return mTealiumURL;
	}
	/**
	 * This method is used to set tealiumURL.
	 * @param pTealiumURL String
	 */
	public void setTealiumURL(String pTealiumURL) {
		mTealiumURL = pTealiumURL;
	}
	/**
	 * This method is used to get searchAB.
	 * @return searchAB String
	 */
	public String getSearchAB() {
		return mSearchAB;
	}
	/**
	 *This method is used to set searchAB.
	 *@param pSearchAB String
	 */
	public void setSearchAB(String pSearchAB) {
		mSearchAB = pSearchAB;
	}
	/**
	 * This method is used to get searchCID.
	 * @return searchCID String
	 */
	public String getSearchCID() {
		return mSearchCID;
	}
	/**
	 *This method is used to set searchCID.
	 *@param pSearchCID String
	 */
	public void setSearchCID(String pSearchCID) {
		mSearchCID = pSearchCID;
	}
	/**
	 * This method is used to get searchOrsoCode.
	 * @return searchOrsoCode String
	 */
	public String getSearchOrsoCode() {
		return mSearchOrsoCode;
	}
	/**
	 *This method is used to set searchOrsoCode.
	 *@param pSearchOrsoCode String
	 */
	public void setSearchOrsoCode(String pSearchOrsoCode) {
		mSearchOrsoCode = pSearchOrsoCode;
	}
	/**
	 * This method is used to get searchIspuSource.
	 * @return searchIspuSource String
	 */
	public String getSearchIspuSource() {
		return mSearchIspuSource;
	}
	/**
	 *This method is used to set searchIspuSource.
	 *@param pSearchIspuSource String
	 */
	public void setSearchIspuSource(String pSearchIspuSource) {
		mSearchIspuSource = pSearchIspuSource;
	}
	/**
	 * This method is used to get searchRefinementType.
	 * @return searchRefinementType String
	 */
	public String getSearchRefinementType() {
		return mSearchRefinementType;
	}
	/**
	 *This method is used to set searchRefinementType.
	 *@param pSearchRefinementType String
	 */
	public void setSearchRefinementType(String pSearchRefinementType) {
		mSearchRefinementType = pSearchRefinementType;
	}
	/**
	 * This method is used to get searchRecent.
	 * @return searchRecent String
	 */
	public String getSearchRecent() {
		return mSearchRecent;
	}
	/**
	 *This method is used to set searchRecent.
	 *@param pSearchRecent String
	 */
	public void setSearchRecent(String pSearchRecent) {
		mSearchRecent = pSearchRecent;
	}
	/**
	 * This method is used to get searchSuccess.
	 * @return searchSuccess String
	 */
	public String getSearchSuccess() {
		return mSearchSuccess;
	}
	/**
	 *This method is used to set searchSuccess.
	 *@param pSearchSuccess String
	 */
	public void setSearchSuccess(String pSearchSuccess) {
		mSearchSuccess = pSearchSuccess;
	}
	/**
	 * This method is used to get searchSynonym.
	 * @return searchSynonym String
	 */
	public String getSearchSynonym() {
		return mSearchSynonym;
	}
	/**
	 *This method is used to set searchSynonym.
	 *@param pSearchSynonym String
	 */
	public void setSearchSynonym(String pSearchSynonym) {
		mSearchSynonym = pSearchSynonym;
	}
	/**
	 * This method is used to get searchTest.
	 * @return searchTest String
	 */
	public String getSearchTest() {
		return mSearchTest;
	}
	/**
	 *This method is used to set searchTest.
	 *@param pSearchTest String
	 */
	public void setSearchTest(String pSearchTest) {
		mSearchTest = pSearchTest;
	}
	/**
	 * This method is used to get searchRefinementValue.
	 * @return searchRefinementValue String
	 */
	public String getSearchRefinementValue() {
		return mSearchRefinementValue;
	}
	/**
	 *This method is used to set searchRefinementValue.
	 *@param pSearchRefinementValue String
	 */
	public void setSearchRefinementValue(String pSearchRefinementValue) {
		mSearchRefinementValue = pSearchRefinementValue;
	}
	/**
	 * This method is used to get searchIntCampaignPage.
	 * @return searchIntCampaignPage String
	 */
	public String getSearchIntCampaignPage() {
		return mSearchIntCampaignPage;
	}
	/**
	 *This method is used to set searchIntCampaignPage.
	 *@param pSearchIntCampaignPage String
	 */
	public void setSearchIntCampaignPage(String pSearchIntCampaignPage) {
		mSearchIntCampaignPage = pSearchIntCampaignPage;
	}

	/**
	 * Gets the customer city.
	 *
	 * @return the customerCity
	 */
	public String getCustomerCity() {
		return mCustomerCity;
	}

	/**
	 * Sets the customer city.
	 *
	 * @param pCustomerCity the customerCity to set
	 */
	public void setCustomerCity(String pCustomerCity) {
		mCustomerCity = pCustomerCity;
	}

	/**
	 * Gets the customer country.
	 *
	 * @return the customerCountry
	 */
	public String getCustomerCountry() {
		return mCustomerCountry;
	}

	/**
	 * Sets the customer country.
	 *
	 * @param pCustomerCountry the customerCountry to set
	 */
	public void setCustomerCountry(String pCustomerCountry) {
		mCustomerCountry = pCustomerCountry;
	}

	/**
	 * Gets the customer email.
	 *
	 * @return the customerEmail
	 */
	public String getCustomerEmail() {
		return mCustomerEmail;
	}

	/**
	 * Sets the customer email.
	 *
	 * @param pCustomerEmail the customerEmail to set
	 */
	public void setCustomerEmail(String pCustomerEmail) {
		mCustomerEmail = pCustomerEmail;
	}

	/**
	 * Gets the customer id.
	 *
	 * @return the customerId
	 */
	public String getCustomerId() {
		return mCustomerId;
	}

	/**
	 * Sets the customer id.
	 *
	 * @param pCustomerId the customerId to set
	 */
	public void setCustomerId(String pCustomerId) {
		mCustomerId = pCustomerId;
	}

	/**
	 * Gets the customer type.
	 *
	 * @return the customerType
	 */
	public String getCustomerType() {
		return mCustomerType;
	}

	/**
	 * Sets the customer type.
	 *
	 * @param pCustomerType the customerType to set
	 */
	public void setCustomerType(String pCustomerType) {
		mCustomerType = pCustomerType;
	}

	/**
	 * Gets the customer state.
	 *
	 * @return the customerState
	 */
	public String getCustomerState() {
		return mCustomerState;
	}

	/**
	 * Sets the customer state.
	 *
	 * @param pCustomerState the customerState to set
	 */
	public void setCustomerState(String pCustomerState) {
		mCustomerState = pCustomerState;
	}

	/**
	 * Gets the customer zip.
	 *
	 * @return the customerZip
	 */
	public String getCustomerZip() {
		return mCustomerZip;
	}

	/**
	 * Sets the customer zip.
	 *
	 * @param pCustomerZip the customerZip to set
	 */
	public void setCustomerZip(String pCustomerZip) {
		mCustomerZip = pCustomerZip;
	}

	/**
	 * Gets the customer dob.
	 *
	 * @return the customerDob
	 */
	public String getCustomerDob() {
		return mCustomerDob;
	}

	/**
	 * Sets the customer dob.
	 *
	 * @param pCustomerDob the customerDob to set
	 */
	public void setCustomerDob(String pCustomerDob) {
		mCustomerDob = pCustomerDob;
	}

	/**
	 * Gets the ab.
	 *
	 * @return the ab
	 */
	public String getAb() {
		return mAb;
	}

	/**
	 * Sets the ab.
	 *
	 * @param pAb the ab to set
	 */
	public void setAb(String pAb) {
		mAb = pAb;
	}

	/**
	 * Gets the cid.
	 *
	 * @return the cid
	 */
	public String getCid() {
		return mCid;
	}

	/**
	 * Sets the cid.
	 *
	 * @param pCid the cid to set
	 */
	public void setCid(String pCid) {
		mCid = pCid;
	}

	/**
	 * Gets the internal campaign page.
	 *
	 * @return the internalCampaignPage
	 */
	public String getInternalCampaignPage() {
		return mInternalCampaignPage;
	}

	/**
	 * Sets the internal campaign page.
	 *
	 * @param pInternalCampaignPage the internalCampaignPage to set
	 */
	public void setInternalCampaignPage(String pInternalCampaignPage) {
		mInternalCampaignPage = pInternalCampaignPage;
	}

	/**
	 * Gets the orso code.
	 *
	 * @return the orsoCode
	 */
	public String getOrsoCode() {
		return mOrsoCode;
	}

	/**
	 * Sets the orso code.
	 *
	 * @param pOrsoCode the orsoCode to set
	 */
	public void setOrsoCode(String pOrsoCode) {
		mOrsoCode = pOrsoCode;
	}

	/**
	 * Gets the ispu source.
	 *
	 * @return the ispuSource
	 */
	public String getIspuSource() {
		return mIspuSource;
	}

	/**
	 * Sets the ispu source.
	 *
	 * @param pIspuSource the ispuSource to set
	 */
	public void setIspuSource(String pIspuSource) {
		mIspuSource = pIspuSource;
	}

	/**
	 * Gets the checkout product merchandising category.
	 *
	 * @return the checkoutProductMerchandisingCategory
	 */
	public String getCheckoutProductMerchandisingCategory() {
		return mCheckoutProductMerchandisingCategory;
	}

	/**
	 * Sets the checkout product merchandising category.
	 *
	 * @param pCheckoutProductMerchandisingCategory the checkoutProductMerchandisingCategory to set
	 */
	public void setCheckoutProductMerchandisingCategory(
			String pCheckoutProductMerchandisingCategory) {
		mCheckoutProductMerchandisingCategory = pCheckoutProductMerchandisingCategory;
	}

	/**
	 * Gets the checkout promotion name.
	 *
	 * @return the checkoutPromotionName
	 */
	public String getCheckoutPromotionName() {
		return mCheckoutPromotionName;
	}

	/**
	 * Sets the checkout promotion name.
	 *
	 * @param pCheckoutPromotionName the checkoutPromotionName to set
	 */
	public void setCheckoutPromotionName(String pCheckoutPromotionName) {
		mCheckoutPromotionName = pCheckoutPromotionName;
	}

	/**
	 * Gets the checkout sts revenue.
	 *
	 * @return the checkoutStsRevenue
	 */
	public String getCheckoutStsRevenue() {
		return mCheckoutStsRevenue;
	}

	/**
	 * Sets the checkout sts revenue.
	 *
	 * @param pCheckoutStsRevenue the checkoutStsRevenue to set
	 */
	public void setCheckoutStsRevenue(String pCheckoutStsRevenue) {
		mCheckoutStsRevenue = pCheckoutStsRevenue;
	}

	/**
	 * Gets the gift finder page name.
	 *
	 * @return the giftFinderPageName
	 */
	public String getGiftFinderPageName() {
		return mGiftFinderPageName;
	}

	/**
	 * Sets the gift finder page name.
	 *
	 * @param pGiftFinderPageName the giftFinderPageName to set
	 */
	public void setGiftFinderPageName(String pGiftFinderPageName) {
		mGiftFinderPageName = pGiftFinderPageName;
	}

	/**
	 * Gets the gift finder page type.
	 *
	 * @return the giftFinderPageType
	 */
	public String getGiftFinderPageType() {
		return mGiftFinderPageType;
	}

	/**
	 * Sets the gift finder page type.
	 *
	 * @param pGiftFinderPageType the giftFinderPageType to set
	 */
	public void setGiftFinderPageType(String pGiftFinderPageType) {
		mGiftFinderPageType = pGiftFinderPageType;
	}

	/**
	 * Gets the gift finder finder sort group.
	 *
	 * @return the giftFinderFinderSortGroup
	 */
	public String getGiftFinderFinderSortGroup() {
		return mGiftFinderFinderSortGroup;
	}

	/**
	 * Sets the gift finder finder sort group.
	 *
	 * @param pGiftFinderFinderSortGroup the giftFinderFinderSortGroup to set
	 */
	public void setGiftFinderFinderSortGroup(String pGiftFinderFinderSortGroup) {
		mGiftFinderFinderSortGroup = pGiftFinderFinderSortGroup;
	}

	/**
	 * Gets the gift finder finder type.
	 *
	 * @return the giftFinderFinderType
	 */
	public String getGiftFinderFinderType() {
		return mGiftFinderFinderType;
	}

	/**
	 * Sets the gift finder finder type.
	 *
	 * @param pGiftFinderFinderType the giftFinderFinderType to set
	 */
	public void setGiftFinderFinderType(String pGiftFinderFinderType) {
		mGiftFinderFinderType = pGiftFinderFinderType;
	}

	/**
	 * Gets the baby site code.
	 *
	 * @return the babySiteCode
	 */
	public String getBabySiteCode() {
		return mBabySiteCode;
	}

	/**
	 * Sets the baby site code.
	 *
	 * @param pBabySiteCode the babySiteCode to set
	 */
	public void setBabySiteCode(String pBabySiteCode) {
		mBabySiteCode = pBabySiteCode;
	}

	/**
	 * Gets the quickview page name.
	 *
	 * @return the quickviewPageName
	 */
	public String getQuickviewPageName() {
		return mQuickviewPageName;
	}

	/**
	 * Sets the quickview page name.
	 *
	 * @param pQuickviewPageName the quickviewPageName to set
	 */
	public void setQuickviewPageName(String pQuickviewPageName) {
		mQuickviewPageName = pQuickviewPageName;
	}

	/**
	 * Gets the quickview page type.
	 *
	 * @return the quickviewPageType
	 */
	public String getQuickviewPageType() {
		return mQuickviewPageType;
	}

	/**
	 * Sets the quickview page type.
	 *
	 * @param pQuickviewPageType the quickviewPageType to set
	 */
	public void setQuickviewPageType(String pQuickviewPageType) {
		mQuickviewPageType = pQuickviewPageType;
	}

	/**
	 * Gets the quickview product category.
	 *
	 * @return the quickviewProductCategory
	 */
	public String getQuickviewProductCategory() {
		return mQuickviewProductCategory;
	}

	/**
	 * Sets the quickview product category.
	 *
	 * @param pQuickviewProductCategory the quickviewProductCategory to set
	 */
	public void setQuickviewProductCategory(String pQuickviewProductCategory) {
		mQuickviewProductCategory = pQuickviewProductCategory;
	}

	/**
	 * Gets the quickview product id.
	 *
	 * @return the quickviewProductId
	 */
	public String getQuickviewProductId() {
		return mQuickviewProductId;
	}

	/**
	 * Sets the quickview product id.
	 *
	 * @param pQuickviewProductId the quickviewProductId to set
	 */
	public void setQuickviewProductId(String pQuickviewProductId) {
		mQuickviewProductId = pQuickviewProductId;
	}

	/**
	 * Gets the quickview product name.
	 *
	 * @return the quickviewProductName
	 */
	public String getQuickviewProductName() {
		return mQuickviewProductName;
	}

	/**
	 * Sets the quickview product name.
	 *
	 * @param pQuickviewProductName the quickviewProductName to set
	 */
	public void setQuickviewProductName(String pQuickviewProductName) {
		mQuickviewProductName = pQuickviewProductName;
	}

	/**
	 * Gets the quickview product unit price.
	 *
	 * @return the quickviewProductUnitPrice
	 */
	public String getQuickviewProductUnitPrice() {
		return mQuickviewProductUnitPrice;
	}

	/**
	 * Sets the quickview product unit price.
	 *
	 * @param pQuickviewProductUnitPrice the quickviewProductUnitPrice to set
	 */
	public void setQuickviewProductUnitPrice(String pQuickviewProductUnitPrice) {
		mQuickviewProductUnitPrice = pQuickviewProductUnitPrice;
	}

	/**
	 * Gets the quickview product sku.
	 *
	 * @return the quickviewProductSku
	 */
	public String getQuickviewProductSku() {
		return mQuickviewProductSku;
	}

	/**
	 * Sets the quickview product sku.
	 *
	 * @param pQuickviewProductSku the quickviewProductSku to set
	 */
	public void setQuickviewProductSku(String pQuickviewProductSku) {
		mQuickviewProductSku = pQuickviewProductSku;
	}

	/**
	 * Gets the quickview product collection.
	 *
	 * @return the quickviewProductCollection
	 */
	public String getQuickviewProductCollection() {
		return mQuickviewProductCollection;
	}

	/**
	 * Sets the quickview product collection.
	 *
	 * @param pQuickviewProductCollection the quickviewProductCollection to set
	 */
	public void setQuickviewProductCollection(String pQuickviewProductCollection) {
		mQuickviewProductCollection = pQuickviewProductCollection;
	}

	/**
	 * Gets the quickview product merchandising category.
	 *
	 * @return the quickviewProductMerchandisingCategory
	 */
	public String getQuickviewProductMerchandisingCategory() {
		return mQuickviewProductMerchandisingCategory;
	}

	/**
	 * Sets the quickview product merchandising category.
	 *
	 * @param pQuickviewProductMerchandisingCategory the quickviewProductMerchandisingCategory to set
	 */
	public void setQuickviewProductMerchandisingCategory(
			String pQuickviewProductMerchandisingCategory) {
		mQuickviewProductMerchandisingCategory = pQuickviewProductMerchandisingCategory;
	}

	/**
	 * Gets the quickview registry origin.
	 *
	 * @return the quickviewRegistryOrigin
	 */
	public String getQuickviewRegistryOrigin() {
		return mQuickviewRegistryOrigin;
	}

	/**
	 * Sets the quickview registry origin.
	 *
	 * @param pQuickviewRegistryOrigin the quickviewRegistryOrigin to set
	 */
	public void setQuickviewRegistryOrigin(String pQuickviewRegistryOrigin) {
		mQuickviewRegistryOrigin = pQuickviewRegistryOrigin;
	}

	/**
	 * Gets the quickview striked price page.
	 *
	 * @return the quickviewStrikedPricePage
	 */
	public String getQuickviewStrikedPricePage() {
		return mQuickviewStrikedPricePage;
	}

	/**
	 * Sets the quickview striked price page.
	 *
	 * @param pQuickviewStrikedPricePage the quickviewStrikedPricePage to set
	 */
	public void setQuickviewStrikedPricePage(String pQuickviewStrikedPricePage) {
		mQuickviewStrikedPricePage = pQuickviewStrikedPricePage;
	}

	/**
	 * Gets the quickview product brand.
	 *
	 * @return the quickviewProductBrand
	 */
	public String getQuickviewProductBrand() {
		return mQuickviewProductBrand;
	}

	/**
	 * Sets the quickview product brand.
	 *
	 * @param pQuickviewProductBrand the quickviewProductBrand to set
	 */
	public void setQuickviewProductBrand(String pQuickviewProductBrand) {
		mQuickviewProductBrand = pQuickviewProductBrand;
	}

	/**
	 * Gets the quickview product image url.
	 *
	 * @return the quickviewProductImageUrl
	 */
	public String getQuickviewProductImageUrl() {
		return mQuickviewProductImageUrl;
	}

	/**
	 * Sets the quickview product image url.
	 *
	 * @param pQuickviewProductImageUrl the quickviewProductImageUrl to set
	 */
	public void setQuickviewProductImageUrl(String pQuickviewProductImageUrl) {
		mQuickviewProductImageUrl = pQuickviewProductImageUrl;
	}

	/**
	 * Gets the quickview product list price.
	 *
	 * @return the quickviewProductListPrice
	 */
	public String getQuickviewProductListPrice() {
		return mQuickviewProductListPrice;
	}

	/**
	 * Sets the quickview product list price.
	 *
	 * @param pQuickviewProductListPrice the quickviewProductListPrice to set
	 */
	public void setQuickviewProductListPrice(String pQuickviewProductListPrice) {
		mQuickviewProductListPrice = pQuickviewProductListPrice;
	}

	/**
	 * Gets the quickview product subcategory.
	 *
	 * @return the quickviewProductSubcategory
	 */
	public String getQuickviewProductSubcategory() {
		return mQuickviewProductSubcategory;
	}

	/**
	 * Sets the quickview product subcategory.
	 *
	 * @param pQuickviewProductSubcategory the quickviewProductSubcategory to set
	 */
	public void setQuickviewProductSubcategory(String pQuickviewProductSubcategory) {
		mQuickviewProductSubcategory = pQuickviewProductSubcategory;
	}

	/**
	 * Gets the quickview product finding method.
	 *
	 * @return the quickviewProductFindingMethod
	 */
	public String getQuickviewProductFindingMethod() {
		return mQuickviewProductFindingMethod;
	}

	/**
	 * Sets the quickview product finding method.
	 *
	 * @param pQuickviewProductFindingMethod the quickviewProductFindingMethod to set
	 */
	public void setQuickviewProductFindingMethod(
			String pQuickviewProductFindingMethod) {
		mQuickviewProductFindingMethod = pQuickviewProductFindingMethod;
	}

	/**
	 * Gets the quickview product discount.
	 *
	 * @return the quickviewProductDiscount
	 */
	public String getQuickviewProductDiscount() {
		return mQuickviewProductDiscount;
	}

	/**
	 * Sets the quickview product discount.
	 *
	 * @param pQuickviewProductDiscount the quickviewProductDiscount to set
	 */
	public void setQuickviewProductDiscount(String pQuickviewProductDiscount) {
		mQuickviewProductDiscount = pQuickviewProductDiscount;
	}

	/**
	 * Gets the quickview internal campaign page.
	 *
	 * @return the quickviewInternalCampaignPage
	 */
	public String getQuickviewInternalCampaignPage() {
		return mQuickviewInternalCampaignPage;
	}

	/**
	 * Sets the quickview internal campaign page.
	 *
	 * @param pQuickviewInternalCampaignPage the quickviewInternalCampaignPage to set
	 */
	public void setQuickviewInternalCampaignPage(
			String pQuickviewInternalCampaignPage) {
		mQuickviewInternalCampaignPage = pQuickviewInternalCampaignPage;
	}

	/**
	 * Gets the quickview out of stock by sku.
	 *
	 * @return the quickviewOutOfStockBySku
	 */
	public String getQuickviewOutOfStockBySku() {
		return mQuickviewOutOfStockBySku;
	}

	/**
	 * Sets the quickview out of stock by sku.
	 *
	 * @param pQuickviewOutOfStockBySku the quickviewOutOfStockBySku to set
	 */
	public void setQuickviewOutOfStockBySku(String pQuickviewOutOfStockBySku) {
		mQuickviewOutOfStockBySku = pQuickviewOutOfStockBySku;
	}

	/**
	 * Gets the cart oas breadcrumb.
	 *
	 * @return the cartOasBreadcrumb
	 */
	public String getCartOasBreadcrumb() {
		return mCartOasBreadcrumb;
	}

	/**
	 * Sets the cart oas breadcrumb.
	 *
	 * @param pCartOasBreadcrumb the cartOasBreadcrumb to set
	 */
	public void setCartOasBreadcrumb(String pCartOasBreadcrumb) {
		mCartOasBreadcrumb = pCartOasBreadcrumb;
	}

	/**
	 * Gets the cart oas taxonomy.
	 *
	 * @return the cartOasTaxonomy
	 */
	public String getCartOasTaxonomy() {
		return mCartOasTaxonomy;
	}

	/**
	 * Sets the cart oas taxonomy.
	 *
	 * @param pCartOasTaxonomy the cartOasTaxonomy to set
	 */
	public void setCartOasTaxonomy(String pCartOasTaxonomy) {
		mCartOasTaxonomy = pCartOasTaxonomy;
	}

	/**
	 * Gets the cart oas size.
	 *
	 * @return the cartOasSize
	 */
	public String getCartOasSize() {
		return mCartOasSize;
	}

	/**
	 * Sets the cart oas size.
	 *
	 * @param pCartOasSize the cartOasSize to set
	 */
	public void setCartOasSize(String pCartOasSize) {
		mCartOasSize = pCartOasSize;
	}

	/**
	 * Gets the cart finder sort group.
	 *
	 * @return the cartFinderSortGroup
	 */
	public String getCartFinderSortGroup() {
		return mCartFinderSortGroup;
	}

	/**
	 * Sets the cart finder sort group.
	 *
	 * @param pCartFinderSortGroup the cartFinderSortGroup to set
	 */
	public void setCartFinderSortGroup(String pCartFinderSortGroup) {
		mCartFinderSortGroup = pCartFinderSortGroup;
	}

	/**
	 * Gets the cart page category.
	 *
	 * @return the cartPageCategory
	 */
	public String getCartPageCategory() {
		return mCartPageCategory;
	}

	/**
	 * Sets the cart page category.
	 *
	 * @param pCartPageCategory the cartPageCategory to set
	 */
	public void setCartPageCategory(String pCartPageCategory) {
		mCartPageCategory = pCartPageCategory;
	}

	/**
	 * Gets the cart page subcategory.
	 *
	 * @return the cartPageSubcategory
	 */
	public String getCartPageSubcategory() {
		return mCartPageSubcategory;
	}

	/**
	 * Sets the cart page subcategory.
	 *
	 * @param pCartPageSubcategory the cartPageSubcategory to set
	 */
	public void setCartPageSubcategory(String pCartPageSubcategory) {
		mCartPageSubcategory = pCartPageSubcategory;
	}

	/**
	 * Gets the cart product brand.
	 *
	 * @return the cartProductBrand
	 */
	public String getCartProductBrand() {
		return mCartProductBrand;
	}

	/**
	 * Sets the cart product brand.
	 *
	 * @param pCartProductBrand the cartProductBrand to set
	 */
	public void setCartProductBrand(String pCartProductBrand) {
		mCartProductBrand = pCartProductBrand;
	}

	/**
	 * Gets the cart product category.
	 *
	 * @return the cartProductCategory
	 */
	public String getCartProductCategory() {
		return mCartProductCategory;
	}

	/**
	 * Sets the cart product category.
	 *
	 * @param pCartProductCategory the cartProductCategory to set
	 */
	public void setCartProductCategory(String pCartProductCategory) {
		mCartProductCategory = pCartProductCategory;
	}

	/**
	 * Gets the cart product id.
	 *
	 * @return the cartProductId
	 */
	public String getCartProductId() {
		return mCartProductId;
	}

	/**
	 * Sets the cart product id.
	 *
	 * @param pCartProductId the cartProductId to set
	 */
	public void setCartProductId(String pCartProductId) {
		mCartProductId = pCartProductId;
	}

	/**
	 * Gets the cart product name.
	 *
	 * @return the cartProductName
	 */
	public String getCartProductName() {
		return mCartProductName;
	}

	/**
	 * Sets the cart product name.
	 *
	 * @param pCartProductName the cartProductName to set
	 */
	public void setCartProductName(String pCartProductName) {
		mCartProductName = pCartProductName;
	}

	/**
	 * Gets the cart product unit price.
	 *
	 * @return the cartProductUnitPrice
	 */
	public String getCartProductUnitPrice() {
		return mCartProductUnitPrice;
	}

	/**
	 * Sets the cart product unit price.
	 *
	 * @param pCartProductUnitPrice the cartProductUnitPrice to set
	 */
	public void setCartProductUnitPrice(String pCartProductUnitPrice) {
		mCartProductUnitPrice = pCartProductUnitPrice;
	}

	/**
	 * Gets the cart product list price.
	 *
	 * @return the cartProductListPrice
	 */
	public String getCartProductListPrice() {
		return mCartProductListPrice;
	}

	/**
	 * Sets the cart product list price.
	 *
	 * @param pCartProductListPrice the cartProductListPrice to set
	 */
	public void setCartProductListPrice(String pCartProductListPrice) {
		mCartProductListPrice = pCartProductListPrice;
	}

	/**
	 * Gets the cart product sku.
	 *
	 * @return the cartProductSku
	 */
	public String getCartProductSku() {
		return mCartProductSku;
	}

	/**
	 * Sets the cart product sku.
	 *
	 * @param pCartProductSku the cartProductSku to set
	 */
	public void setCartProductSku(String pCartProductSku) {
		mCartProductSku = pCartProductSku;
	}

	/**
	 * Gets the cart product subcategory.
	 *
	 * @return the cartProductSubcategory
	 */
	public String getCartProductSubcategory() {
		return mCartProductSubcategory;
	}

	/**
	 * Sets the cart product subcategory.
	 *
	 * @param pCartProductSubcategory the cartProductSubcategory to set
	 */
	public void setCartProductSubcategory(String pCartProductSubcategory) {
		mCartProductSubcategory = pCartProductSubcategory;
	}

	/**
	 * Gets the cart product finding method.
	 *
	 * @return the cartProductFindingMethod
	 */
	public String getCartProductFindingMethod() {
		return mCartProductFindingMethod;
	}

	/**
	 * Sets the cart product finding method.
	 *
	 * @param pCartProductFindingMethod the cartProductFindingMethod to set
	 */
	public void setCartProductFindingMethod(String pCartProductFindingMethod) {
		mCartProductFindingMethod = pCartProductFindingMethod;
	}

	/**
	 * Gets the cart product discount.
	 *
	 * @return the cartProductDiscount
	 */
	public String getCartProductDiscount() {
		return mCartProductDiscount;
	}

	/**
	 * Sets the cart product discount.
	 *
	 * @param pCartProductDiscount the cartProductDiscount to set
	 */
	public void setCartProductDiscount(String pCartProductDiscount) {
		mCartProductDiscount = pCartProductDiscount;
	}

	/**
	 * Gets the cart ab.
	 *
	 * @return the cartAb
	 */
	public String getCartAb() {
		return mCartAb;
	}

	/**
	 * Sets the cart ab.
	 *
	 * @param pCartAb the cartAb to set
	 */
	public void setCartAb(String pCartAb) {
		mCartAb = pCartAb;
	}

	/**
	 * Gets the cart cid.
	 *
	 * @return the cartCid
	 */
	public String getCartCid() {
		return mCartCid;
	}

	/**
	 * Sets the cart cid.
	 *
	 * @param pCartCid the cartCid to set
	 */
	public void setCartCid(String pCartCid) {
		mCartCid = pCartCid;
	}

	/**
	 * Gets the cart image icon.
	 *
	 * @return the cartImageIcon
	 */
	public String getCartImageIcon() {
		return mCartImageIcon;
	}

	/**
	 * Sets the cart image icon.
	 *
	 * @param pCartImageIcon the cartImageIcon to set
	 */
	public void setCartImageIcon(String pCartImageIcon) {
		mCartImageIcon = pCartImageIcon;
	}

	/**
	 * Gets the cart internal campaign page.
	 *
	 * @return the cartInternalCampaignPage
	 */
	public String getCartInternalCampaignPage() {
		return mCartInternalCampaignPage;
	}

	/**
	 * Sets the cart internal campaign page.
	 *
	 * @param pCartInternalCampaignPage the cartInternalCampaignPage to set
	 */
	public void setCartInternalCampaignPage(String pCartInternalCampaignPage) {
		mCartInternalCampaignPage = pCartInternalCampaignPage;
	}

	/**
	 * Gets the cart orso code.
	 *
	 * @return the cartOrsoCode
	 */
	public String getCartOrsoCode() {
		return mCartOrsoCode;
	}

	/**
	 * Sets the cart orso code.
	 *
	 * @param pCartOrsoCode the cartOrsoCode to set
	 */
	public void setCartOrsoCode(String pCartOrsoCode) {
		mCartOrsoCode = pCartOrsoCode;
	}

	/**
	 * Gets the cart out of stock by sku.
	 *
	 * @return the cartOutOfStockBySku
	 */
	public String getCartOutOfStockBySku() {
		return mCartOutOfStockBySku;
	}

	/**
	 * Sets the cart out of stock by sku.
	 *
	 * @param pCartOutOfStockBySku the cartOutOfStockBySku to set
	 */
	public void setCartOutOfStockBySku(String pCartOutOfStockBySku) {
		mCartOutOfStockBySku = pCartOutOfStockBySku;
	}

	/**
	 * Gets the cart product collection.
	 *
	 * @return the cartProductCollection
	 */
	public String getCartProductCollection() {
		return mCartProductCollection;
	}

	/**
	 * Sets the cart product collection.
	 *
	 * @param pCartProductCollection the cartProductCollection to set
	 */
	public void setCartProductCollection(String pCartProductCollection) {
		mCartProductCollection = pCartProductCollection;
	}

	/**
	 * Gets the cart product pathing.
	 *
	 * @return the cartProductPathing
	 */
	public String getCartProductPathing() {
		return mCartProductPathing;
	}

	/**
	 * Sets the cart product pathing.
	 *
	 * @param pCartProductPathing the cartProductPathing to set
	 */
	public void setCartProductPathing(String pCartProductPathing) {
		mCartProductPathing = pCartProductPathing;
	}

	/**
	 * Gets the cart registry origin.
	 *
	 * @return the cartRegistryOrigin
	 */
	public String getCartRegistryOrigin() {
		return mCartRegistryOrigin;
	}

	/**
	 * Sets the cart registry origin.
	 *
	 * @param pCartRegistryOrigin the cartRegistryOrigin to set
	 */
	public void setCartRegistryOrigin(String pCartRegistryOrigin) {
		mCartRegistryOrigin = pCartRegistryOrigin;
	}

	/**
	 * Gets the cart xsell product.
	 *
	 * @return the cartXsellProduct
	 */
	public String getCartXsellProduct() {
		return mCartXsellProduct;
	}

	/**
	 * Sets the cart xsell product.
	 *
	 * @param pCartXsellProduct the cartXsellProduct to set
	 */
	public void setCartXsellProduct(String pCartXsellProduct) {
		mCartXsellProduct = pCartXsellProduct;
	}

	/**
	 * Gets the cart ispu source.
	 *
	 * @return the cartIspuSource
	 */
	public String getCartIspuSource() {
		return mCartIspuSource;
	}

	/**
	 * Sets the cart ispu source.
	 *
	 * @param pCartIspuSource the cartIspuSource to set
	 */
	public void setCartIspuSource(String pCartIspuSource) {
		mCartIspuSource = pCartIspuSource;
	}

	/**
	 * Gets the cart prod image url.
	 *
	 * @return the cartProdImageUrl
	 */
	public String getCartProdImageUrl() {
		return mCartProdImageUrl;
	}

	/**
	 * Sets the cart prod image url.
	 *
	 * @param pCartProdImageUrl the cartProdImageUrl to set
	 */
	public void setCartProdImageUrl(String pCartProdImageUrl) {
		mCartProdImageUrl = pCartProdImageUrl;
	}

	/**
	 * Gets the cart site section.
	 *
	 * @return the cartSiteSection
	 */
	public String getCartSiteSection() {
		return mCartSiteSection;
	}

	/**
	 * Sets the cart site section.
	 *
	 * @param pCartSiteSection the cartSiteSection to set
	 */
	public void setCartSiteSection(String pCartSiteSection) {
		mCartSiteSection = pCartSiteSection;
	}

	/**
	 * Gets the cart product quantity.
	 *
	 * @return the cartProductQuantity
	 */
	public String getCartProductQuantity() {
		return mCartProductQuantity;
	}

	/**
	 * Sets the cart product quantity.
	 *
	 * @param pCartProductQuantity the cartProductQuantity to set
	 */
	public void setCartProductQuantity(String pCartProductQuantity) {
		mCartProductQuantity = pCartProductQuantity;
	}

	/**
	 * Gets the cart promotion name.
	 *
	 * @return the cartPromotionName
	 */
	public String getCartPromotionName() {
		return mCartPromotionName;
	}

	/**
	 * Sets the cart promotion name.
	 *
	 * @param pCartPromotionName the cartPromotionName to set
	 */
	public void setCartPromotionName(String pCartPromotionName) {
		mCartPromotionName = pCartPromotionName;
	}

	/**
	 * Gets the cart economy shipping.
	 *
	 * @return the cartEconomyShipping
	 */
	public String getCartEconomyShipping() {
		return mCartEconomyShipping;
	}

	/**
	 * Sets the cart economy shipping.
	 *
	 * @param pCartEconomyShipping the cartEconomyShipping to set
	 */
	public void setCartEconomyShipping(String pCartEconomyShipping) {
		mCartEconomyShipping = pCartEconomyShipping;
	}

	/**
	 * Gets the cart sts revenue.
	 *
	 * @return the cartStsRevenue
	 */
	public String getCartStsRevenue() {
		return mCartStsRevenue;
	}

	/**
	 * Sets the cart sts revenue.
	 *
	 * @param pCartStsRevenue the cartStsRevenue to set
	 */
	public void setCartStsRevenue(String pCartStsRevenue) {
		mCartStsRevenue = pCartStsRevenue;
	}

	/**
	 * Gets the cart order subtotal.
	 *
	 * @return the cartOrderSubtotal
	 */
	public String getCartOrderSubtotal() {
		return mCartOrderSubtotal;
	}

	/**
	 * Sets the cart order subtotal.
	 *
	 * @param pCartOrderSubtotal the cartOrderSubtotal to set
	 */
	public void setCartOrderSubtotal(String pCartOrderSubtotal) {
		mCartOrderSubtotal = pCartOrderSubtotal;
	}

	/**
	 * Gets the cart order currency.
	 *
	 * @return the cartOrderCurrency
	 */
	public String getCartOrderCurrency() {
		return mCartOrderCurrency;
	}

	/**
	 * Sets the cart order currency.
	 *
	 * @param pCartOrderCurrency the cartOrderCurrency to set
	 */
	public void setCartOrderCurrency(String pCartOrderCurrency) {
		mCartOrderCurrency = pCartOrderCurrency;
	}

	/**
	 * Gets the order product brand.
	 *
	 * @return the orderProductBrand
	 */
	public String getOrderProductBrand() {
		return mOrderProductBrand;
	}

	/**
	 * Sets the order product brand.
	 *
	 * @param pOrderProductBrand the orderProductBrand to set
	 */
	public void setOrderProductBrand(String pOrderProductBrand) {
		mOrderProductBrand = pOrderProductBrand;
	}

	/**
	 * Gets the order product category.
	 *
	 * @return the orderProductCategory
	 */
	public String getOrderProductCategory() {
		return mOrderProductCategory;
	}

	/**
	 * Sets the order product category.
	 *
	 * @param pOrderProductCategory the orderProductCategory to set
	 */
	public void setOrderProductCategory(String pOrderProductCategory) {
		mOrderProductCategory = pOrderProductCategory;
	}

	/**
	 * Gets the order product id.
	 *
	 * @return the orderProductId
	 */
	public String getOrderProductId() {
		return mOrderProductId;
	}

	/**
	 * Sets the order product id.
	 *
	 * @param pOrderProductId the orderProductId to set
	 */
	public void setOrderProductId(String pOrderProductId) {
		mOrderProductId = pOrderProductId;
	}

	/**
	 * Gets the order product name.
	 *
	 * @return the orderProductName
	 */
	public String getOrderProductName() {
		return mOrderProductName;
	}

	/**
	 * Sets the order product name.
	 *
	 * @param pOrderProductName the orderProductName to set
	 */
	public void setOrderProductName(String pOrderProductName) {
		mOrderProductName = pOrderProductName;
	}

	/**
	 * Gets the order product unit price.
	 *
	 * @return the orderProductUnitPrice
	 */
	public String getOrderProductUnitPrice() {
		return mOrderProductUnitPrice;
	}

	/**
	 * Sets the order product unit price.
	 *
	 * @param pOrderProductUnitPrice the orderProductUnitPrice to set
	 */
	public void setOrderProductUnitPrice(String pOrderProductUnitPrice) {
		mOrderProductUnitPrice = pOrderProductUnitPrice;
	}

	/**
	 * Gets the order product list price.
	 *
	 * @return the orderProductListPrice
	 */
	public String getOrderProductListPrice() {
		return mOrderProductListPrice;
	}

	/**
	 * Sets the order product list price.
	 *
	 * @param pOrderProductListPrice the orderProductListPrice to set
	 */
	public void setOrderProductListPrice(String pOrderProductListPrice) {
		mOrderProductListPrice = pOrderProductListPrice;
	}

	/**
	 * Gets the order product sku.
	 *
	 * @return the orderProductSku
	 */
	public String getOrderProductSku() {
		return mOrderProductSku;
	}

	/**
	 * Sets the order product sku.
	 *
	 * @param pOrderProductSku the orderProductSku to set
	 */
	public void setOrderProductSku(String pOrderProductSku) {
		mOrderProductSku = pOrderProductSku;
	}

	/**
	 * Gets the order product subcategory.
	 *
	 * @return the orderProductSubcategory
	 */
	public String getOrderProductSubcategory() {
		return mOrderProductSubcategory;
	}

	/**
	 * Sets the order product subcategory.
	 *
	 * @param pOrderProductSubcategory the orderProductSubcategory to set
	 */
	public void setOrderProductSubcategory(String pOrderProductSubcategory) {
		mOrderProductSubcategory = pOrderProductSubcategory;
	}

	/**
	 * Gets the order product finding method.
	 *
	 * @return the orderProductFindingMethod
	 */
	public String getOrderProductFindingMethod() {
		return mOrderProductFindingMethod;
	}

	/**
	 * Sets the order product finding method.
	 *
	 * @param pOrderProductFindingMethod the orderProductFindingMethod to set
	 */
	public void setOrderProductFindingMethod(String pOrderProductFindingMethod) {
		mOrderProductFindingMethod = pOrderProductFindingMethod;
	}

	/**
	 * Gets the order product discount.
	 *
	 * @return the orderProductDiscount
	 */
	public String getOrderProductDiscount() {
		return mOrderProductDiscount;
	}

	/**
	 * Sets the order product discount.
	 *
	 * @param pOrderProductDiscount the orderProductDiscount to set
	 */
	public void setOrderProductDiscount(String pOrderProductDiscount) {
		mOrderProductDiscount = pOrderProductDiscount;
	}

	/**
	 * Gets the order prod image url.
	 *
	 * @return the orderProdImageUrl
	 */
	public String getOrderProdImageUrl() {
		return mOrderProdImageUrl;
	}

	/**
	 * Sets the order prod image url.
	 *
	 * @param pOrderProdImageUrl the orderProdImageUrl to set
	 */
	public void setOrderProdImageUrl(String pOrderProdImageUrl) {
		mOrderProdImageUrl = pOrderProdImageUrl;
	}

	/**
	 * Gets the order product quantity.
	 *
	 * @return the orderProductQuantity
	 */
	public String getOrderProductQuantity() {
		return mOrderProductQuantity;
	}

	/**
	 * Sets the order product quantity.
	 *
	 * @param pOrderProductQuantity the orderProductQuantity to set
	 */
	public void setOrderProductQuantity(String pOrderProductQuantity) {
		mOrderProductQuantity = pOrderProductQuantity;
	}

	/**
	 * Gets the order currency.
	 *
	 * @return the orderCurrency
	 */
	public String getOrderCurrency() {
		return mOrderCurrency;
	}

	/**
	 * Sets the order currency.
	 *
	 * @param pOrderCurrency the orderCurrency to set
	 */
	public void setOrderCurrency(String pOrderCurrency) {
		mOrderCurrency = pOrderCurrency;
	}

	/**
	 * Gets the order coupon type.
	 *
	 * @return the orderCouponType
	 */
	public String getOrderCouponType() {
		return mOrderCouponType;
	}

	/**
	 * Sets the order coupon type.
	 *
	 * @param pOrderCouponType the orderCouponType to set
	 */
	public void setOrderCouponType(String pOrderCouponType) {
		mOrderCouponType = pOrderCouponType;
	}

	/**
	 * Gets the order type.
	 *
	 * @return the orderType
	 */
	public String getOrderType() {
		return mOrderType;
	}

	/**
	 * Sets the order type.
	 *
	 * @param pOrderType the orderType to set
	 */
	public void setOrderType(String pOrderType) {
		mOrderType = pOrderType;
	}

	/**
	 * Gets the order store.
	 *
	 * @return the orderStore
	 */
	public String getOrderStore() {
		return mOrderStore;
	}

	/**
	 * Sets the order store.
	 *
	 * @param pOrderStore the orderStore to set
	 */
	public void setOrderStore(String pOrderStore) {
		mOrderStore = pOrderStore;
	}

	/**
	 * Gets the order tax.
	 *
	 * @return the orderTax
	 */
	public String getOrderTax() {
		return mOrderTax;
	}

	/**
	 * Sets the order tax.
	 *
	 * @param pOrderTax the orderTax to set
	 */
	public void setOrderTax(String pOrderTax) {
		mOrderTax = pOrderTax;
	}

	/**
	 * Gets the order default ship type.
	 *
	 * @return the orderDefaultShipType
	 */
	public String getOrderDefaultShipType() {
		return mOrderDefaultShipType;
	}

	/**
	 * Sets the order default ship type.
	 *
	 * @param pOrderDefaultShipType the orderDefaultShipType to set
	 */
	public void setOrderDefaultShipType(String pOrderDefaultShipType) {
		mOrderDefaultShipType = pOrderDefaultShipType;
	}

	/**
	 * Gets the order shipping surcharge.
	 *
	 * @return the orderShippingSurcharge
	 */
	public String getOrderShippingSurcharge() {
		return mOrderShippingSurcharge;
	}

	/**
	 * Sets the order shipping surcharge.
	 *
	 * @param pOrderShippingSurcharge the orderShippingSurcharge to set
	 */
	public void setOrderShippingSurcharge(String pOrderShippingSurcharge) {
		mOrderShippingSurcharge = pOrderShippingSurcharge;
	}

	/**
	 * Gets the order shipping type.
	 *
	 * @return the orderShippingType
	 */
	public String getOrderShippingType() {
		return mOrderShippingType;
	}

	/**
	 * Sets the order shipping type.
	 *
	 * @param pOrderShippingType the orderShippingType to set
	 */
	public void setOrderShippingType(String pOrderShippingType) {
		mOrderShippingType = pOrderShippingType;
	}

	/**
	 * Gets the order shipping country.
	 *
	 * @return the orderShippingCountry
	 */
	public String getOrderShippingCountry() {
		return mOrderShippingCountry;
	}

	/**
	 * Sets the order shipping country.
	 *
	 * @param pOrderShippingCountry the orderShippingCountry to set
	 */
	public void setOrderShippingCountry(String pOrderShippingCountry) {
		mOrderShippingCountry = pOrderShippingCountry;
	}

	/**
	 * Gets the order shipping discount.
	 *
	 * @return the mOrderShippingDiscount
	 */
	public String getOrderShippingDiscount() {
		return mOrderShippingDiscount;
	}

	/**
	 * Sets the order shipping discount.
	 *
	 * @param pOrderShippingDiscount the orderShippingDiscount to set
	 */
	public void setOrderShippingDiscount(String pOrderShippingDiscount) {
		mOrderShippingDiscount = pOrderShippingDiscount;
	}

	/**
	 * Gets the order shipping.
	 *
	 * @return the orderShipping
	 */
	public String getOrderShipping() {
		return mOrderShipping;
	}

	/**
	 * Sets the order shipping.
	 *
	 * @param pOrderShipping the orderShipping to set
	 */
	public void setOrderShipping(String pOrderShipping) {
		mOrderShipping = pOrderShipping;
	}

	/**
	 * Gets the order payment amount.
	 *
	 * @return the orderPaymentAmount
	 */
	public String getOrderPaymentAmount() {
		return mOrderPaymentAmount;
	}

	/**
	 * Sets the order payment amount.
	 *
	 * @param pOrderPaymentAmount the orderPaymentAmount to set
	 */
	public void setOrderPaymentAmount(String pOrderPaymentAmount) {
		mOrderPaymentAmount = pOrderPaymentAmount;
	}

	/**
	 * Gets the order coupon code.
	 *
	 * @return the orderCouponCode
	 */
	public String getOrderCouponCode() {
		return mOrderCouponCode;
	}

	/**
	 * Sets the order coupon code.
	 *
	 * @param pOrderCouponCode the orderCouponCode to set
	 */
	public void setOrderCouponCode(String pOrderCouponCode) {
		mOrderCouponCode = pOrderCouponCode;
	}

	/**
	 * Gets the order gift wrap.
	 *
	 * @return the orderGiftWrap
	 */
	public String getOrderGiftWrap() {
		return mOrderGiftWrap;
	}

	/**
	 * Sets the order gift wrap.
	 *
	 * @param pOrderGiftWrap the orderGiftWrap to set
	 */
	public void setOrderGiftWrap(String pOrderGiftWrap) {
		mOrderGiftWrap = pOrderGiftWrap;
	}

	/**
	 * Gets the order ispu.
	 *
	 * @return the orderIspu
	 */
	public String getOrderIspu() {
		return mOrderIspu;
	}

	/**
	 * Sets the order ispu.
	 *
	 * @param pOrderIspu the orderIspu to set
	 */
	public void setOrderIspu(String pOrderIspu) {
		mOrderIspu = pOrderIspu;
	}

	/**
	 * Gets the order payment type.
	 *
	 * @return the orderPaymentType
	 */
	public String getOrderPaymentType() {
		return mOrderPaymentType;
	}

	/**
	 * Sets the order payment type.
	 *
	 * @param pOrderPaymentType the orderPaymentType to set
	 */
	public void setOrderPaymentType(String pOrderPaymentType) {
		mOrderPaymentType = pOrderPaymentType;
	}

	/**
	 * Gets the order id.
	 *
	 * @return the orderID
	 */
	public String getOrderID() {
		return mOrderID;
	}

	/**
	 * Sets the order id.
	 *
	 * @param pOrderID the orderID to set
	 */
	public void setOrderID(String pOrderID) {
		mOrderID = pOrderID;
	}

	/**
	 * Gets the order total.
	 *
	 * @return the orderTotal
	 */
	public String getOrderTotal() {
		return mOrderTotal;
	}

	/**
	 * Sets the order total.
	 *
	 * @param pOrderTotal the orderTotal to set
	 */
	public void setOrderTotal(String pOrderTotal) {
		mOrderTotal = pOrderTotal;
	}

	/**
	 * Gets the order discount.
	 *
	 * @return the orderDiscount
	 */
	public String getOrderDiscount() {
		return mOrderDiscount;
	}

	/**
	 * Sets the order discount.
	 *
	 * @param pOrderDiscount the orderDiscount to set
	 */
	public void setOrderDiscount(String pOrderDiscount) {
		mOrderDiscount = pOrderDiscount;
	}

	/**
	 * Gets the order fulfillment channel.
	 *
	 * @return the orderFulfillmentChannel
	 */
	public String getOrderFulfillmentChannel() {
		return mOrderFulfillmentChannel;
	}

	/**
	 * Sets the order fulfillment channel.
	 *
	 * @param pOrderFulfillmentChannel the orderFulfillmentChannel to set
	 */
	public void setOrderFulfillmentChannel(String pOrderFulfillmentChannel) {
		mOrderFulfillmentChannel = pOrderFulfillmentChannel;
	}

	/**
	 * Gets the order product merch category.
	 *
	 * @return the orderProductMerchCategory
	 */
	public String getOrderProductMerchCategory() {
		return mOrderProductMerchCategory;
	}

	/**
	 * Sets the order product merch category.
	 *
	 * @param pOrderProductMerchCategory the orderProductMerchCategory to set
	 */
	public void setOrderProductMerchCategory(String pOrderProductMerchCategory) {
		mOrderProductMerchCategory = pOrderProductMerchCategory;
	}

	/**
	 * Gets the order error code.
	 *
	 * @return the orderErrorCode
	 */
	public String getOrderErrorCode() {
		return mOrderErrorCode;
	}

	/**
	 * Sets the order error code.
	 *
	 * @param pOrderErrorCode the orderErrorCode to set
	 */
	public void setOrderErrorCode(String pOrderErrorCode) {
		mOrderErrorCode = pOrderErrorCode;
	}

	/**
	 * Gets the order error message.
	 *
	 * @return the orderErrorMessage
	 */
	public String getOrderErrorMessage() {
		return mOrderErrorMessage;
	}

	/**
	 * Sets the order error message.
	 *
	 * @param pOrderErrorMessage the orderErrorMessage to set
	 */
	public void setOrderErrorMessage(String pOrderErrorMessage) {
		mOrderErrorMessage = pOrderErrorMessage;
	}

	/**
	 * Gets the order oas breadcrumb.
	 *
	 * @return the orderOasBreadcrumb
	 */
	public String getOrderOasBreadcrumb() {
		return mOrderOasBreadcrumb;
	}

	/**
	 * Sets the order oas breadcrumb.
	 *
	 * @param pOrderOasBreadcrumb the orderOasBreadcrumb to set
	 */
	public void setOrderOasBreadcrumb(String pOrderOasBreadcrumb) {
		mOrderOasBreadcrumb = pOrderOasBreadcrumb;
	}

	/**
	 * Gets the order oas taxonomy.
	 *
	 * @return the orderOasTaxonomy
	 */
	public String getOrderOasTaxonomy() {
		return mOrderOasTaxonomy;
	}

	/**
	 * Sets the order oas taxonomy.
	 *
	 * @param pOrderOasTaxonomy the orderOasTaxonomy to set
	 */
	public void setOrderOasTaxonomy(String pOrderOasTaxonomy) {
		mOrderOasTaxonomy = pOrderOasTaxonomy;
	}

	/**
	 * Gets the order oas size.
	 *
	 * @return the orderOasSize
	 */
	public String getOrderOasSize() {
		return mOrderOasSize;
	}

	/**
	 * Sets the order oas size.
	 *
	 * @param pOrderOasSize the orderOasSize to set
	 */
	public void setOrderOasSize(String pOrderOasSize) {
		mOrderOasSize = pOrderOasSize;
	}

	/**
	 * Gets the order internal campaign page.
	 *
	 * @return the orderInternalCampaignPage
	 */
	public String getOrderInternalCampaignPage() {
		return mOrderInternalCampaignPage;
	}

	/**
	 * Sets the order internal campaign page.
	 *
	 * @param pOrderInternalCampaignPage the orderInternalCampaignPage to set
	 */
	public void setOrderInternalCampaignPage(String pOrderInternalCampaignPage) {
		mOrderInternalCampaignPage = pOrderInternalCampaignPage;
	}

	/**
	 * Gets the order orso code.
	 *
	 * @return the orderOrsoCode
	 */
	public String getOrderOrsoCode() {
		return mOrderOrsoCode;
	}

	/**
	 * Sets the order orso code.
	 *
	 * @param pOrderOrsoCode the orderOrsoCode to set
	 */
	public void setOrderOrsoCode(String pOrderOrsoCode) {
		mOrderOrsoCode = pOrderOrsoCode;
	}

	/**
	 * Gets the order out of stock by sku.
	 *
	 * @return the orderOutOfStockBySku
	 */
	public String getOrderOutOfStockBySku() {
		return mOrderOutOfStockBySku;
	}

	/**
	 * Sets the order out of stock by sku.
	 *
	 * @param pOrderOutOfStockBySku the orderOutOfStockBySku to set
	 */
	public void setOrderOutOfStockBySku(String pOrderOutOfStockBySku) {
		mOrderOutOfStockBySku = pOrderOutOfStockBySku;
	}

	/**
	 * Gets the order product collection.
	 *
	 * @return the orderProductCollection
	 */
	public String getOrderProductCollection() {
		return mOrderProductCollection;
	}

	/**
	 * Sets the order product collection.
	 *
	 * @param pOrderProductCollection the orderProductCollection to set
	 */
	public void setOrderProductCollection(String pOrderProductCollection) {
		mOrderProductCollection = pOrderProductCollection;
	}

	/**
	 * Gets the order promotion name.
	 *
	 * @return the orderPromotionName
	 */
	public String getOrderPromotionName() {
		return mOrderPromotionName;
	}

	/**
	 * Sets the order promotion name.
	 *
	 * @param pOrderPromotionName the orderPromotionName to set
	 */
	public void setOrderPromotionName(String pOrderPromotionName) {
		mOrderPromotionName = pOrderPromotionName;
	}

	/**
	 * Gets the order subtotal.
	 *
	 * @return the orderSubtotal
	 */
	public String getOrderSubtotal() {
		return mOrderSubtotal;
	}

	/**
	 * Sets the order subtotal.
	 *
	 * @param pOrderSubtotal the orderSubtotal to set
	 */
	public void setOrderSubtotal(String pOrderSubtotal) {
		mOrderSubtotal = pOrderSubtotal;
	}

	/**
	 * Gets the PDP page name.
	 *
	 * @return the pDPPageName
	 */
     public String getPDPPageName() {
            return mPDPPageName;
     }

     /**
      * Sets the PDP page name.
      *
      * @param pPDPPageName the pDPPageName to set
      */
     public void setPDPPageName(String pPDPPageName) {
            mPDPPageName = pPDPPageName;
     }

     /**
      * Gets the PDP page type.
      *
      * @return the pDPPageType
      */
     public String getPDPPageType() {
            return mPDPPageType;
     }

     /**
      * Sets the PDP page type.
      *
      * @param pPDPPageType the pDPPageType to set
      */
     public void setPDPPageType(String pPDPPageType) {
            mPDPPageType = pPDPPageType;
     }

     /**
      * Gets the search page name.
      *
      * @return the searchPageName
      */
     public String getSearchPageName() {
            return mSearchPageName;
     }

     /**
      * Sets the search page name.
      *
      * @param pSearchPageName the searchPageName to set
      */
     public void setSearchPageName(String pSearchPageName) {
            mSearchPageName = pSearchPageName;
     }

     /**
      * Gets the search page type.
      *
      * @return the searchPageType
      */
     public String getSearchPageType() {
            return mSearchPageType;
     }

     /**
      * Sets the search page type.
      *
      * @param pSearchPageType the searchPageType to set
      */
     public void setSearchPageType(String pSearchPageType) {
            mSearchPageType = pSearchPageType;
     }

     /**
      * Gets the account page name.
      *
      * @return the accountPageName
      */
     public String getAccountPageName() {
            return mAccountPageName;
     }

     /**
      * Sets the account page name.
      *
      * @param pAccountPageName the accountPageName to set
      */
     public void setAccountPageName(String pAccountPageName) {
            mAccountPageName = pAccountPageName;
     }

     /**
      * Gets the account page type.
      *
      * @return the accountPageType
      */
     public String getAccountPageType() {
            return mAccountPageType;
     }

     /**
      * Sets the account page type.
      *
      * @param pAccountPageType the accountPageType to set
      */
     public void setAccountPageType(String pAccountPageType) {
            mAccountPageType = pAccountPageType;
     }

     /**
      * Gets the search no results page name.
      *
      * @return the searchNoResultsPageName
      */
     public String getSearchNoResultsPageName() {
            return mSearchNoResultsPageName;
     }

     /**
      * Sets the search no results page name.
      *
      * @param pSearchNoResultsPageName the searchNoResultsPageName to set
      */
     public void setSearchNoResultsPageName(String pSearchNoResultsPageName) {
            mSearchNoResultsPageName = pSearchNoResultsPageName;
     }

	/**
	 * Gets the checkout page name.
	 *
	 * @return the checkoutPageName
	 */
	public String getCheckoutPageName() {
		return mCheckoutPageName;
	}

	/**
	 * Sets the checkout page name.
	 *
	 * @param pCheckoutPageName the checkoutPageName to set
	 */
	public void setCheckoutPageName(String pCheckoutPageName) {
		mCheckoutPageName = pCheckoutPageName;
	}

	/**
	 * Gets the checkout page type.
	 *
	 * @return the checkoutPageType
	 */
	public String getCheckoutPageType() {
		return mCheckoutPageType;
	}

	/**
	 * Sets the checkout page type.
	 *
	 * @param pCheckoutPageType the checkoutPageType to set
	 */
	public void setCheckoutPageType(String pCheckoutPageType) {
		mCheckoutPageType = pCheckoutPageType;
	}

	/**
	 * Gets the quickview striked price page title.
	 *
	 * @return the quickviewStrikedPricePageTitle
	 */
	public String getQuickviewStrikedPricePageTitle() {
		return mQuickviewStrikedPricePageTitle;
	}

	/**
	 * Sets the quickview striked price page title.
	 *
	 * @param pQuickviewStrikedPricePageTitle the quickviewStrikedPricePageTitle to set
	 */
	public void setQuickviewStrikedPricePageTitle(
			String pQuickviewStrikedPricePageTitle) {
		mQuickviewStrikedPricePageTitle = pQuickviewStrikedPricePageTitle;
	}

	/**
	 * Gets the store locator landing page name.
	 *
	 * @return the storeLocatorLandingPageName
	 */
	public String getStoreLocatorLandingPageName() {
		return mStoreLocatorLandingPageName;
	}

	/**
	 * Sets the store locator landing page name.
	 *
	 * @param pStoreLocatorLandingPageName the storeLocatorLandingPageName to set
	 */
	public void setStoreLocatorLandingPageName(String pStoreLocatorLandingPageName) {
		mStoreLocatorLandingPageName = pStoreLocatorLandingPageName;
	}

	/**
	 * Gets the store locator landing page type.
	 *
	 * @return the storeLocatorLandingPageType
	 */
	public String getStoreLocatorLandingPageType() {
		return mStoreLocatorLandingPageType;
	}

	/**
	 * Sets the store locator landing page type.
	 *
	 * @param pStoreLocatorLandingPageType the storeLocatorLandingPageType to set
	 */
	public void setStoreLocatorLandingPageType(String pStoreLocatorLandingPageType) {
		mStoreLocatorLandingPageType = pStoreLocatorLandingPageType;
	}

	/**
	 * Gets the shopby page name.
	 *
	 * @return the shopbyPageName
	 */
	public String getShopbyPageName() {
		return mShopbyPageName;
	}

	/**
	 * Sets the shopby page name.
	 *
	 * @param pShopbyPageName the shopbyPageName to set
	 */
	public void setShopbyPageName(String pShopbyPageName) {
		mShopbyPageName = pShopbyPageName;
	}

	/**
	 * Gets the shopby page type.
	 *
	 * @return the shopbyPageType
	 */
	public String getShopbyPageType() {
		return mShopbyPageType;
	}

	/**
	 * Sets the shopby page type.
	 *
	 * @param pShopbyPageType the shopbyPageType to set
	 */
	public void setShopbyPageType(String pShopbyPageType) {
		mShopbyPageType = pShopbyPageType;
	}

	/**
	 * Gets the cart page name.
	 *
	 * @return the cartPageName
	 */
	public String getCartPageName() {
		return mCartPageName;
	}

	/**
	 * Sets the cart page name.
	 *
	 * @param pCartPageName the cartPageName to set
	 */
	public void setCartPageName(String pCartPageName) {
		mCartPageName = pCartPageName;
	}

	/**
	 * Gets the cart page type.
	 *
	 * @return the cartPageType
	 */
	public String getCartPageType() {
		return mCartPageType;
	}

	/**
	 * Sets the cart page type.
	 *
	 * @param pCartPageType the cartPageType to set
	 */
	public void setCartPageType(String pCartPageType) {
		mCartPageType = pCartPageType;
	}

	/**
	 * Gets the toys site code.
	 *
	 * @return the toysSiteCode
	 */
	public String getToysSiteCode() {
		return mToysSiteCode;
	}

	/**
	 * Sets the toys site code.
	 *
	 * @param pToysSiteCode the toysSiteCode to set
	 */
	public void setToysSiteCode(String pToysSiteCode) {
		mToysSiteCode = pToysSiteCode;
	}

	/**
	 * Gets the baby partner name.
	 *
	 * @return the mBabyPartnerName
	 */
	public String getBabyPartnerName() {
		return mBabyPartnerName;
	}

	/**
	 * Sets the baby partner name.
	 *
	 * @param pBabyPartnerName the babyPartnerName to set
	 */
	public void setBabyPartnerName(String pBabyPartnerName) {
		this.mBabyPartnerName = pBabyPartnerName;
	}

	/**
	 * Gets the toys partner name.
	 *
	 * @return the mToysPartnerName
	 */
	public String getToysPartnerName() {
		return mToysPartnerName;
	}

	/**
	 * Sets the toys partner name.
	 *
	 * @param pToysPartnerName the toysPartnerName to set
	 */
	public void setToysPartnerName(String pToysPartnerName) {
		this.mToysPartnerName = pToysPartnerName;
	}

	/**
	 * Gets the toys partner name.
	 * @return the mOrderHistoryPageName
	 */
	public String getOrderHistoryPageName() {
		return mOrderHistoryPageName;
	}

	/**
	 * Sets the Order History PageName
	 * @param pOrderHistoryPageName the OrderHistoryPageName to set
	 */
	public void setOrderHistoryPageName(String pOrderHistoryPageName) {
		this.mOrderHistoryPageName = pOrderHistoryPageName;
	}

	/**
	 * Sets the Order History PageName
	 * @return the mOrderHistoryPage
	 */
	public String getOrderHistoryPage() {
		return mOrderHistoryPage;
	}

	/**
	 * Sets the Order History Page
	 * @param pOrderHistoryPage the OrderHistoryPage to set
	 */
	public void setOrderHistoryPage(String pOrderHistoryPage) {
		this.mOrderHistoryPage = pOrderHistoryPage;
	}

	/**
	 * @return the displayCreditCardTypeName
	 */
	public Map<String, String> getDisplayCreditCardTypeName() {
		return mDisplayCreditCardTypeName;
	}

	/**
	 * @param pDisplayCreditCardTypeName the displayCreditCardTypeName to set
	 */
	public void setDisplayCreditCardTypeName(
			Map<String, String> pDisplayCreditCardTypeName) {
		mDisplayCreditCardTypeName = pDisplayCreditCardTypeName;
	}

	/**
	 * @return the reviewPageName
	 */
	public String getReviewPageName() {
		return mReviewPageName;
	}

	/**
	 * @param pReviewPageName the reviewPageName to set
	 */
	public void setReviewPageName(String pReviewPageName) {
		mReviewPageName = pReviewPageName;
	}

	/**
	 * @return the paymentPageName
	 */
	public String getPaymentPageName() {
		return mPaymentPageName;
	}

	/**
	 * @param pPaymentPageName the paymentPageName to set
	 */
	public void setPaymentPageName(String pPaymentPageName) {
		mPaymentPageName = pPaymentPageName;
	}
}
