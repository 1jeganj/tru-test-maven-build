package com.tru.common;

/**
 * This is a bean class to hold classification configuration properties.
 * @author PA
 * @version 1.0 
 * 
 */
public class TRUClassificationConfiguration {
	/**
	 * Property to Hold mType PropertyName.
	 */
	private String mType;
	
	/**
	 * Property to Hold ClassificationIsDeleted PropertyName.
	 */
	private String	mClassificationIsDeleted;
	/**
	 * Property to Hold ClassificationDisplayStatus PropertyName.
	 */
	private String	mClassificationDisplayStatus;
	/**
	 * Property to Hold ClassificationDisplayOrder PropertyName.
	 */
	private String	mClassificationDisplayOrder;

	/**
	 * Property to Hold ClassificationUserTypeId PropertyName.
	 */
	private String	mClassificationUserTypeId;
	/**
	 * Property to Hold mClassificationLongDescription PropertyName.
	 */
	private String	mClassificationLongDescription;
	
	/**
	 * Property to Hold ClassificationRrelatedMediaContent PropertyName.
	 */
	private String	mClassificationRrelatedMediaContent;
	
	/**
	 * Property to Hold MediaName PropertyName.
	 */
	private String	mMediaName;
	/**
	 * Property to Hold MediaType PropertyName.
	 */
	private String	mMediaType;
	
	/**
	 * Property to Hold MediaUrl PropertyName.
	 */
	private String	mMediaUrl;
	
	/**
	 * Property to Hold MediaHtmlContent PropertyName.
	 */
	private String	mMediaHtmlContent;
	/**
	 * Property to Hold mCategoryImageKey .
	 */
	private String mCategoryImageKey;
	
	/**
	 * Property to Hold mClassificationCrossRefDisplayName .
	 */
	private String mClassificationCrossRefDisplayName;
	
	/**
	 * Property to Hold mClassificationCrossRefDisplayOrder .
	 */
	private String mClassificationCrossRefDisplayOrder;
	/**
	 * @return the classificationCrossRefDisplayName
	 */
	public String getClassificationCrossRefDisplayName() {
		return mClassificationCrossRefDisplayName;
	}
	/**
	 * @param pClassificationCrossRefDisplayName the classificationCrossRefDisplayName to set
	 */
	public void setClassificationCrossRefDisplayName(String pClassificationCrossRefDisplayName) {
		this.mClassificationCrossRefDisplayName = pClassificationCrossRefDisplayName;
	}
	/**
	 * @return the classificationCrossRefDisplayOrder
	 */
	public String getClassificationCrossRefDisplayOrder() {
		return mClassificationCrossRefDisplayOrder;
	}
	/**
	 * @param pClassificationCrossRefDisplayOrder the classificationCrossRefDisplayOrder to set
	 */
	public void setClassificationCrossRefDisplayOrder(String pClassificationCrossRefDisplayOrder) {
		this.mClassificationCrossRefDisplayOrder = pClassificationCrossRefDisplayOrder;
	}
	/**
	 * @return the classificationDisplayOrder
	 */
	public String getClassificationDisplayOrder() {
		return mClassificationDisplayOrder;
	}
	/**
	 * @param pClassificationDisplayOrder the classificationDisplayOrder to set
	 */
	public void setClassificationDisplayOrder(String pClassificationDisplayOrder) {
		mClassificationDisplayOrder = pClassificationDisplayOrder;
	}
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return mType;
	}
	
	/**
	 * Sets the type.
	 *
	 * @param pType the new type
	 */
	public void setType(String pType) {
		this.mType = pType;
	}
	
	/**
	 * Gets the classification is deleted.
	 *
	 * @return the classificationIsDeleted
	 */
	public String getClassificationIsDeleted() {
		return mClassificationIsDeleted;
	}
	
	/**
	 * Sets the classification is deleted.
	 *
	 * @param pClassificationIsDeleted the classificationIsDeleted to set
	 */
	public void setClassificationIsDeleted(String pClassificationIsDeleted) {
		mClassificationIsDeleted = pClassificationIsDeleted;
	}
	
	/**
	 * Gets the classification display status.
	 *
	 * @return the classificationDisplayStatus
	 */
	public String getClassificationDisplayStatus() {
		return mClassificationDisplayStatus;
	}
	
	/**
	 * Sets the classification display status.
	 *
	 * @param pClassificationDisplayStatus the classificationDisplayStatus to set
	 */
	public void setClassificationDisplayStatus(String pClassificationDisplayStatus) {
		mClassificationDisplayStatus = pClassificationDisplayStatus;
	}
	
	/**
	 * Gets the classification user type id.
	 *
	 * @return the classificationUserTypeId
	 */
	public String getClassificationUserTypeId() {
		return mClassificationUserTypeId;
	}
	
	/**
	 * Sets the classification user type id.
	 *
	 * @param pClassificationUserTypeId the classificationUserTypeId to set
	 */
	public void setClassificationUserTypeId(String pClassificationUserTypeId) {
		mClassificationUserTypeId = pClassificationUserTypeId;
	}
	
	/**
	 * Gets the classification long description.
	 *
	 * @return the classification long description
	 */
	public String getClassificationLongDescription() {
		return mClassificationLongDescription;
	}
	
	/**
	 * Sets the classification long description.
	 *
	 * @param pClassificationLongDescription the new classification long description
	 */
	public void setClassificationLongDescription(
			String pClassificationLongDescription) {
		this.mClassificationLongDescription = pClassificationLongDescription;
	}
	
	/**
	 * Gets the classification rrelated media content.
	 *
	 * @return the classificationRrelatedMediaContent
	 */
	public String getClassificationRrelatedMediaContent() {
		return mClassificationRrelatedMediaContent;
	}
	
	/**
	 * Sets the classification rrelated media content.
	 *
	 * @param pClassificationRrelatedMediaContent the classificationRrelatedMediaContent to set
	 */
	public void setClassificationRrelatedMediaContent(
			String pClassificationRrelatedMediaContent) {
		mClassificationRrelatedMediaContent = pClassificationRrelatedMediaContent;
	}
	
	/**
	 * Gets the media name.
	 *
	 * @return the mediaName
	 */
	public String getMediaName() {
		return mMediaName;
	}
	
	/**
	 * Sets the media name.
	 *
	 * @param pMediaName the mediaName to set
	 */
	public void setMediaName(String pMediaName) {
		mMediaName = pMediaName;
	}
	
	/**
	 * Gets the media type.
	 *
	 * @return the mediaType
	 */
	public String getMediaType() {
		return mMediaType;
	}
	
	/**
	 * Sets the media type.
	 *
	 * @param pMediaType the mediaType to set
	 */
	public void setMediaType(String pMediaType) {
		mMediaType = pMediaType;
	}
	
	/**
	 * Gets the media url.
	 *
	 * @return the mediaUrl
	 */
	public String getMediaUrl() {
		return mMediaUrl;
	}
	
	/**
	 * Sets the media url.
	 *
	 * @param pMediaUrl the mediaUrl to set
	 */
	public void setMediaUrl(String pMediaUrl) {
		mMediaUrl = pMediaUrl;
	}
	
	/**
	 * Gets the media html content.
	 *
	 * @return the mediaHtmlContent
	 */
	public String getMediaHtmlContent() {
		return mMediaHtmlContent;
	}
	
	/**
	 * Sets the media html content.
	 *
	 * @param pMediaHtmlContent the mediaHtmlContent to set
	 */
	public void setMediaHtmlContent(String pMediaHtmlContent) {
		mMediaHtmlContent = pMediaHtmlContent;
	}
	
	/**
	 * Gets the category image key.
	 *
	 * @return the category image key
	 */
	public String getCategoryImageKey() {
		return mCategoryImageKey;
	}
	
	/**
	 * Sets the category image key.
	 *
	 * @param pCategoryImageKey the new category image key
	 */
	public void setCategoryImageKey(String pCategoryImageKey) {
		mCategoryImageKey = pCategoryImageKey;
	}
}
