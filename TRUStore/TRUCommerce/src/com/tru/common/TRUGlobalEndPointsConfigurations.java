package com.tru.common;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class TRUGlobalEndPointsConfigurations.
 *  
 *@author Professional Access.
 *@version 1.0.
 *
 */
public class TRUGlobalEndPointsConfigurations {
	
	/** Holds the property value of mTealiumURL. */
	private String mTealiumURL;
	
	/** Holds the property value of mTealiumSyncURL. */
	private String mTealiumSyncURL;
	
	/** Holds the property value of mAddressDoctorURL. */
	private String mAddressDoctorURL;
	
	/** Holds the property value of mESBServer. */
	private String mESBServer;
	
	/** Holds the property value of mESBUser.*/
	private String mESBUser;
	
	/** Holds the property value of mESBPassword. */
	private String mESBPassword;
	
	/** Holds the property value of mCouponLookupServiceURL. */
	private String mCouponLookupServiceURL;
	
	/** Holds the property value of mCouponServiceURL. */
	private String mCouponServiceURL;
	
	/** Holds the property value of mOrderCreateServiceURL. */
	private String mOrderCreateServiceURL;
	
	/** Holds the property value of mOrderLookupServiceURL. */
	private String mOrderLookupServiceURL;
	
	/**Holds the property value of mOrderDetailsLookupServiceURL. */
	private String mOrderDetailsLookupServiceURL;
	
	/** Holds the property value of mOrderCancelServiceURL. */
	private String mOrderCancelServiceURL;
	
	/** Holds the property value of mApiKeyValue.*/
	private String mApiKeyValue;
	
	/** Holds the property value of mWsdlLocation.*/
	private String mWsdlLocation;
	
	/** Holds the property value of mRadialTaxQuoteURI.*/
	private String mRadialTaxQuoteURI;
	
	/** Holds the property value of mRadialApiKey. */
	private String mRadialApiKey;
	
	/** Holds the property value of mPdpRegistryUrl. */
	private String mPdpRegistryUrl;
	
	/** Holds the property value of mTealiumURL. */
	private String mPdpWishlistUrl;
	
	/** Holds the property value of mPdpChecklistUrl.*/
	private String mPdpChecklistUrl;
	
	/** Holds the property value of mViewWishListUrl. */
	private String mViewWishListUrl;
	
	/** Holds the property value of mBackToWishListUrl. */
	private String mBackToWishListUrl;
	
	/** Holds the property value of mViewRegistryUrl. */
	private String mViewRegistryUrl;
	
	/** Holds the property value of mBackToAnonymousRegistryURL.*/
	private String mBackToAnonymousRegistryURL;
	
	/** Holds the property value of mBackToAnonymousWishlistURL. */
	private String mBackToAnonymousWishlistURL;
	
	/** Holds the property value of mCreateWishListUrl. */
	private String mCreateWishListUrl;
	
	/** Holds the property value of mPdpRegistryInvokeURL. */
	private String mPdpRegistryInvokeURL;
	
	/** Holds the property value of mPdpWishlistInvokeURL.*/
	private String mPdpWishlistInvokeURL;
	
	/** Holds the property value of mMerchantGroupId. */
	private String mMerchantGroupId;
	
	/** Holds the property value of mMerchantId. */
	private String mMerchantId;
	
	/** Holds the property value of mPowerReviewsApiKey. */
	private String mPowerReviewsApiKey;
	
	/** Holds the property value of mPowerReviewsURL. */
	private String mPowerReviewsURL;
	
	/** Holds the property value of mStoreLocatorGoogleMapURL. */
	private String mStoreLocatorGoogleMapURL;
	
	/** Holds the property value of mStoreLocatorStaticGoogleMapURL. */
	private String mStoreLocatorStaticGoogleMapURL;
	
	/** Holds the property value of mStoreLocatorGetDirectionsGoogleMapURL. */
	private String mStoreLocatorGetDirectionsGoogleMapURL;
		
	/** Holds the property value of mSummaryDataXmlURL. */
	private String mSummaryDataXmlURL;
	
	/** Holds the property value of mServiceHost. */
	private String mServiceHost;
	
	/** The nonce service host. */
	private String mNonceServiceHost;
	
	/** The nonce resource path. */
	private String mNonceResourcepath;
	
	/** Holds the property value of mNortonSealURL. */
	private String mNortonSealURL;
	
	/** Holds the property value of mBruNortonSealURL. */
	private String mBruNortonSealURL;
	
	/**
	 * Gets the TealiumURL.
	 *
	 * @return the TealiumURL
	 */
	public String getTealiumURL() {
		return mTealiumURL;
	}

	/**
	 * Gets the StoreLocatorGetDirectionsGoogleMapURL.
	 *
	 * @return the mStoreLocatorGetDirectionsGoogleMapURL
	 */
	public String getStoreLocatorGetDirectionsGoogleMapURL() {
		return mStoreLocatorGetDirectionsGoogleMapURL;
	}

	/**
	 * Sets the mStoreLocatorGetDirectionsGoogleMapURL.
	 *
	 * @param pStoreLocatorGetDirectionsGoogleMapURL the new store locator get directions google map url
	 */
	public void setStoreLocatorGetDirectionsGoogleMapURL(
			String pStoreLocatorGetDirectionsGoogleMapURL) {
		this.mStoreLocatorGetDirectionsGoogleMapURL = pStoreLocatorGetDirectionsGoogleMapURL;
	}

	/**
	 * Sets the mTealiumURL.
	 *
	 * @param pTealiumURL the new TealiumURL
	 */
	public void setTealiumURL(String pTealiumURL) {
		this.mTealiumURL = pTealiumURL;
	}

	/**
	 * Gets the TealiumSyncURL.
	 *
	 * @return the mTealiumSyncURL
	 */
	public String getTealiumSyncURL() {
		return mTealiumSyncURL;
	}

	/**
	 * Sets the mTealiumSyncURL.
	 *
	 * @param pTealiumSyncURL the new TealiumSyncURL
	 */
	public void setTealiumSyncURL(String pTealiumSyncURL) {
		this.mTealiumSyncURL = pTealiumSyncURL;
	}

	/**
	 * Gets the mAddressDoctorURL.
	 *
	 * @return the mAddressDoctorURL
	 */
	public String getAddressDoctorURL() {
		return mAddressDoctorURL;
	}

	/**
	 * Sets the mAddressDoctorURL.
	 *
	 * @param pAddressDoctorURL the new AddressDoctorURL
	 */
	public void setAddressDoctorURL(String pAddressDoctorURL) {
		this.mAddressDoctorURL = pAddressDoctorURL;
	}

	/**
	 * Gets the m esb server.
	 *
	 * @return the m esb server
	 */
	public String getESBServer() {
		return mESBServer;
	}

	/**
	 * Sets the m esb server.
	 *
	 * @param pESBServer the new ESB server
	 */
	public void setESBServer(String pESBServer) {
		this.mESBServer = pESBServer;
	}

	/**
	 * Gets the m esb user.
	 *
	 * @return the m esb user
	 */
	public String getESBUser() {
		return mESBUser;
	}

	/**
	 * Sets the m esb user.
	 *
	 * @param pESBUser the new ESB user
	 */
	public void setESBUser(String pESBUser) {
		this.mESBUser = pESBUser;
	}

	/**
	 * Gets the m esb password.
	 *
	 * @return the m esb password
	 */
	public String getESBPassword() {
		return mESBPassword;
	}

	/**
	 * Sets the m esb password.
	 *
	 * @param pESBPassword the new ESB password
	 */
	public void setESBPassword(String pESBPassword) {
		this.mESBPassword = pESBPassword;
	}

	/**
	 * Gets the m coupon lookup service url.
	 *
	 * @return the m coupon lookup service url
	 */
	public String getCouponLookupServiceURL() {
		return mCouponLookupServiceURL;
	}

	/**
	 * Sets the m coupon lookup service url.
	 *
	 * @param pCouponLookupServiceURL the new coupon lookup service url
	 */
	public void setCouponLookupServiceURL(String pCouponLookupServiceURL) {
		this.mCouponLookupServiceURL = pCouponLookupServiceURL;
	}

	/**
	 * Gets the m coupon service url.
	 *
	 * @return the m coupon service url
	 */
	public String getCouponServiceURL() {
		return mCouponServiceURL;
	}

	/**
	 * Sets the m coupon service url.
	 *
	 * @param pCouponServiceURL the new coupon service url
	 */
	public void setCouponServiceURL(String pCouponServiceURL) {
		this.mCouponServiceURL = pCouponServiceURL;
	}

	/**
	 * Gets the m order create service url.
	 *
	 * @return the m order create service url
	 */
	public String getOrderCreateServiceURL() {
		return mOrderCreateServiceURL;
	}

	/**
	 * Sets the m order create service url.
	 *
	 * @param pOrderCreateServiceURL the new order create service url
	 */
	public void setOrderCreateServiceURL(String pOrderCreateServiceURL) {
		this.mOrderCreateServiceURL = pOrderCreateServiceURL;
	}

	/**
	 * Gets the m order lookup service url.
	 *
	 * @return the m order lookup service url
	 */
	public String getOrderLookupServiceURL() {
		return mOrderLookupServiceURL;
	}

	/**
	 * Sets the m order lookup service url.
	 *
	 * @param pOrderLookupServiceURL the new order lookup service url
	 */
	public void setOrderLookupServiceURL(String pOrderLookupServiceURL) {
		this.mOrderLookupServiceURL = pOrderLookupServiceURL;
	}

	/**
	 * Gets the m order details lookup service url.
	 *
	 * @return the m order details lookup service url
	 */
	public String getOrderDetailsLookupServiceURL() {
		return mOrderDetailsLookupServiceURL;
	}

	/**
	 * Sets the m order details lookup service url.
	 *
	 * @param pOrderDetailsLookupServiceURL the new order details lookup service url
	 */
	public void setOrderDetailsLookupServiceURL(
			String pOrderDetailsLookupServiceURL) {
		this.mOrderDetailsLookupServiceURL = pOrderDetailsLookupServiceURL;
	}

	/**
	 * Gets the m order cancel service url.
	 *
	 * @return the m order cancel service url
	 */
	public String getOrderCancelServiceURL() {
		return mOrderCancelServiceURL;
	}

	/**
	 * Sets the m order cancel service url.
	 *
	 * @param pOrderCancelServiceURL the new order cancel service url
	 */
	public void setOrderCancelServiceURL(String pOrderCancelServiceURL) {
		this.mOrderCancelServiceURL = pOrderCancelServiceURL;
	}

	/**
	 * Gets the m api key value.
	 *
	 * @return the m api key value
	 */
	public String getApiKeyValue() {
		return mApiKeyValue;
	}

	/**
	 * Sets the m api key value.
	 *
	 * @param pApiKeyValue the new api key value
	 */
	public void setApiKeyValue(String pApiKeyValue) {
		this.mApiKeyValue = pApiKeyValue;
	}

	/**
	 * Gets the m wsdl location.
	 *
	 * @return the m wsdl location
	 */
	public String getWsdlLocation() {
		return mWsdlLocation;
	}

	/**
	 * Sets the m wsdl location.
	 *
	 * @param pWsdlLocation the new wsdl location
	 */
	public void setWsdlLocation(String pWsdlLocation) {
		this.mWsdlLocation = pWsdlLocation;
	}

	/**
	 * Gets the m radial tax quote uri.
	 *
	 * @return the m radial tax quote uri
	 */
	public String getRadialTaxQuoteURI() {
		return mRadialTaxQuoteURI;
	}

	/**
	 * Sets the m radial tax quote uri.
	 *
	 * @param pRadialTaxQuoteURI the new radial tax quote uri
	 */
	public void setRadialTaxQuoteURI(String pRadialTaxQuoteURI) {
		this.mRadialTaxQuoteURI = pRadialTaxQuoteURI;
	}

	/**
	 * Gets the m radial api key.
	 *
	 * @return the m radial api key
	 */
	public String getRadialApiKey() {
		return mRadialApiKey;
	}

	/**
	 * Sets the m radial api key.
	 *
	 * @param pRadialApiKey the new radial api key
	 */
	public void setRadialApiKey(String pRadialApiKey) {
		this.mRadialApiKey = pRadialApiKey;
	}

	/**
	 * Gets the m pdp registry url.
	 *
	 * @return the m pdp registry url
	 */
	public String getPdpRegistryUrl() {
		return mPdpRegistryUrl;
	}

	/**
	 * Sets the m pdp registry url.
	 *
	 * @param pPdpRegistryUrl the new pdp registry url
	 */
	public void setPdpRegistryUrl(String pPdpRegistryUrl) {
		this.mPdpRegistryUrl = pPdpRegistryUrl;
	}

	/**
	 * Gets the m pdp wishlist url.
	 *
	 * @return the m pdp wishlist url
	 */
	public String getPdpWishlistUrl() {
		return mPdpWishlistUrl;
	}

	/**
	 * Sets the m pdp wishlist url.
	 *
	 * @param pPdpWishlistUrl the new pdp wishlist url
	 */
	public void setPdpWishlistUrl(String pPdpWishlistUrl) {
		this.mPdpWishlistUrl = pPdpWishlistUrl;
	}

	/**
	 * Gets the m pdp checklist url.
	 *
	 * @return the m pdp checklist url
	 */
	public String getPdpChecklistUrl() {
		return mPdpChecklistUrl;
	}

	/**
	 * Sets the m pdp checklist url.
	 *
	 * @param pPdpChecklistUrl the new pdp checklist url
	 */
	public void setPdpChecklistUrl(String pPdpChecklistUrl) {
		this.mPdpChecklistUrl = pPdpChecklistUrl;
	}

	/**
	 * Gets the m view wish list url.
	 *
	 * @return the m view wish list url
	 */
	public String getViewWishListUrl() {
		return mViewWishListUrl;
	}

	/**
	 * Sets the m view wish list url.
	 *
	 * @param pViewWishListUrl the new view wish list url
	 */
	public void setViewWishListUrl(String pViewWishListUrl) {
		this.mViewWishListUrl = pViewWishListUrl;
	}

	/**
	 * Gets the m back to wish list url.
	 *
	 * @return the m back to wish list url
	 */
	public String getBackToWishListUrl() {
		return mBackToWishListUrl;
	}

	/**
	 * Sets the m back to wish list url.
	 *
	 * @param pBackToWishListUrl the new back to wish list url
	 */
	public void setBackToWishListUrl(String pBackToWishListUrl) {
		this.mBackToWishListUrl = pBackToWishListUrl;
	}

	/**
	 * Gets the m view registry url.
	 *
	 * @return the m view registry url
	 */
	public String getViewRegistryUrl() {
		return mViewRegistryUrl;
	}

	/**
	 * Sets the m view registry url.
	 *
	 * @param pViewRegistryUrl the new view registry url
	 */
	public void setViewRegistryUrl(String pViewRegistryUrl) {
		this.mViewRegistryUrl = pViewRegistryUrl;
	}

	/**
	 * Gets the m back to anonymous registry url.
	 *
	 * @return the m back to anonymous registry url
	 */
	public String getBackToAnonymousRegistryURL() {
		return mBackToAnonymousRegistryURL;
	}

	/**
	 * Sets the m back to anonymous registry url.
	 *
	 * @param pBackToAnonymousRegistryURL the new back to anonymous registry url
	 */
	public void setBackToAnonymousRegistryURL(String pBackToAnonymousRegistryURL) {
		this.mBackToAnonymousRegistryURL = pBackToAnonymousRegistryURL;
	}

	/**
	 * Gets the m back to anonymous wishlist url.
	 *
	 * @return the m back to anonymous wishlist url
	 */
	public String getBackToAnonymousWishlistURL() {
		return mBackToAnonymousWishlistURL;
	}

	/**
	 * Sets the m back to anonymous wishlist url.
	 *
	 * @param pBackToAnonymousWishlistURL the new back to anonymous wishlist url
	 */
	public void setBackToAnonymousWishlistURL(String pBackToAnonymousWishlistURL) {
		this.mBackToAnonymousWishlistURL = pBackToAnonymousWishlistURL;
	}

	/**
	 * Gets the m create wish list url.
	 *
	 * @return the m create wish list url
	 */
	public String getCreateWishListUrl() {
		return mCreateWishListUrl;
	}

	/**
	 * Sets the m create wish list url.
	 *
	 * @param pCreateWishListUrl the new creates the wish list url
	 */
	public void setCreateWishListUrl(String pCreateWishListUrl) {
		this.mCreateWishListUrl = pCreateWishListUrl;
	}

	/**
	 * Gets the m pdp registry invoke url.
	 *
	 * @return the m pdp registry invoke url
	 */
	public String getPdpRegistryInvokeURL() {
		return mPdpRegistryInvokeURL;
	}

	/**
	 * Sets the m pdp registry invoke url.
	 *
	 * @param pPdpRegistryInvokeURL the new pdp registry invoke url
	 */
	public void setPdpRegistryInvokeURL(String pPdpRegistryInvokeURL) {
		this.mPdpRegistryInvokeURL = pPdpRegistryInvokeURL;
	}

	/**
	 * Gets the m pdp wishlist invoke url.
	 *
	 * @return the m pdp wishlist invoke url
	 */
	public String getPdpWishlistInvokeURL() {
		return mPdpWishlistInvokeURL;
	}

	/**
	 * Sets the m pdp wishlist invoke url.
	 *
	 * @param pPdpWishlistInvokeURL the new pdp wishlist invoke url
	 */
	public void setPdpWishlistInvokeURL(String pPdpWishlistInvokeURL) {
		this.mPdpWishlistInvokeURL = pPdpWishlistInvokeURL;
	}

	/**
	 * Gets the m merchant group id.
	 *
	 * @return the m merchant group id
	 */
	public String getMerchantGroupId() {
		return mMerchantGroupId;
	}

	/**
	 * Sets the m merchant group id.
	 *
	 * @param pMerchantGroupId the new merchant group id
	 */
	public void setMerchantGroupId(String pMerchantGroupId) {
		this.mMerchantGroupId = pMerchantGroupId;
	}

	/**
	 * Gets the m merchant id.
	 *
	 * @return the m merchant id
	 */
	public String getMerchantId() {
		return mMerchantId;
	}

	/**
	 * Sets the m merchant id.
	 *
	 * @param pMerchantId the new merchant id
	 */
	public void setMerchantId(String pMerchantId) {
		this.mMerchantId = pMerchantId;
	}

	/**
	 * Gets the m power reviews api key.
	 *
	 * @return the m power reviews api key
	 */
	public String getPowerReviewsApiKey() {
		return mPowerReviewsApiKey;
	}

	/**
	 * Sets the m power reviews api key.
	 *
	 * @param pPowerReviewsApiKey the new power reviews api key
	 */
	public void setPowerReviewsApiKey(String pPowerReviewsApiKey) {
		this.mPowerReviewsApiKey = pPowerReviewsApiKey;
	}

	/**
	 * Gets the m power reviews url.
	 *
	 * @return the m power reviews url
	 */
	public String getPowerReviewsURL() {
		return mPowerReviewsURL;
	}

	/**
	 * Sets the m power reviews url.
	 *
	 * @param pPowerReviewsURL the new power reviews url
	 */
	public void setPowerReviewsURL(String pPowerReviewsURL) {
		this.mPowerReviewsURL = pPowerReviewsURL;
	}

	/**
	 * Gets the m store locator google map url.
	 *
	 * @return the m store locator google map url
	 */
	public String getStoreLocatorGoogleMapURL() {
		return mStoreLocatorGoogleMapURL;
	}

	/**
	 * Sets the m store locator google map url.
	 *
	 * @param pStoreLocatorGoogleMapURL the new store locator google map url
	 */
	public void setStoreLocatorGoogleMapURL(String pStoreLocatorGoogleMapURL) {
		this.mStoreLocatorGoogleMapURL = pStoreLocatorGoogleMapURL;
	}

	/**
	 * Gets the m store locator static google map url.
	 *
	 * @return the m store locator static google map url
	 */
	public String getStoreLocatorStaticGoogleMapURL() {
		return mStoreLocatorStaticGoogleMapURL;
	}

	/**
	 * Sets the m store locator static google map url.
	 *
	 * @param pStoreLocatorStaticGoogleMapURL the new store locator static google map url
	 */
	public void setStoreLocatorStaticGoogleMapURL(
			String pStoreLocatorStaticGoogleMapURL) {
		this.mStoreLocatorStaticGoogleMapURL = pStoreLocatorStaticGoogleMapURL;
	}
	
	/**
	 * Gets the summary data xml url.
	 *
	 * @return the summary data xml url
	 */
	public String getSummaryDataXmlURL() {
		return mSummaryDataXmlURL;
	}

	/**
	 * Sets the summary data xml url.
	 *
	 * @param pSummaryDataXmlURL the new summary data xml url
	 */
	public void setSummaryDataXmlURL(String pSummaryDataXmlURL) {
		this.mSummaryDataXmlURL = pSummaryDataXmlURL;
	}

	/**
	 * Gets the service host.
	 *
	 * @return the service host
	 */
	public String getServiceHost() {
		return mServiceHost;
	}

	/**
	 * Sets the service host.
	 *
	 * @param pServiceHost the new service host
	 */
	public void setServiceHost(String pServiceHost) {
		this.mServiceHost = pServiceHost;
	}
	
	/** The m service resource path. */
	private Map<String, String> mServiceResourcePath= new HashMap<String, String>();
	
	/**
	 * Gets the service resource path.
	 *
	 * @return the service resource path
	 */
	public Map<String, String> getServiceResourcePath() {
		return mServiceResourcePath;
	}

	/**
	 * Sets the service resource path.
	 *
	 * @param pServiceResourcePath the m service resource path
	 */
	public void setServiceResourcePath(Map<String, String> pServiceResourcePath) {
		this.mServiceResourcePath = pServiceResourcePath;
	}
	
	/**
	 * @return the nonceServiceHost
	 */
	public String getNonceServiceHost() {
		return mNonceServiceHost;
	}

	/**
	 * @param pNonceServiceHost the nonceServiceHost to set
	 */
	public void setNonceServiceHost(String pNonceServiceHost) {
		mNonceServiceHost = pNonceServiceHost;
	}

	/**
	 * @return the nonceResourcepath
	 */
	public String getNonceResourcepath() {
		return mNonceResourcepath;
	}

	/**
	 * @param pNonceResourcepath the nonceResourcepath to set
	 */
	public void setNonceResourcepath(String pNonceResourcepath) {
		mNonceResourcepath = pNonceResourcepath;
	}
	
	/**
	 * @return the nortonSealURL
	 */
	public String getNortonSealURL() {
		return mNortonSealURL;
	}
	
	/**
	 * @param pNortonSealURL the nortonSealURL to set
	 */
	public void setNortonSealURL(String pNortonSealURL) {
		this.mNortonSealURL = pNortonSealURL;
	}
	
	/**
	 * @return the bruNortonSealURL
	 */
	public String getBruNortonSealURL() {
		return mBruNortonSealURL;
	}
	
	/**
	 * @param pBruNortonSealURL the bruNortonSealURL to set
	 */
	public void setBruNortonSealURL(String pBruNortonSealURL) {
		this.mBruNortonSealURL = pBruNortonSealURL;
	}

	
}
