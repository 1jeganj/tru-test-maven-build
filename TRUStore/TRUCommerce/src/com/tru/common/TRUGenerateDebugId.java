/**
 * 
 */
package com.tru.common;

import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import atg.commerce.util.NoLockNameException;
import atg.commerce.util.TransactionLockService;
import atg.service.lockmanager.DeadlockException;
import atg.service.lockmanager.LockManagerException;

/**
 * @author 
 *
 */

public class TRUGenerateDebugId {

private static Random random = new Random();	

public static class LockDebugId {

		private TRULockManager truLockManager;
		
		private int id;
		private long millis;
		private int count;
		
		private String lockName;
		
		private LockDebugId(String lockName, TRULockManager truLockManager, int id,	 long millis, int count) {
		  this.lockName = lockName;
		  
	      this.count = count;
	      this.millis = millis;
	      this.id = id;
	      
	      this.truLockManager = truLockManager;
		}
		
		public String toString() {
			return "[LockDEbugId][name=" + lockName + "][" + truLockManager + "][LockDebugId][" + id + "." + millis + "." + count + "]";
		}
	}

    private static HashMap<String, LockDebugId> lockDebugIdMap = new HashMap<>();
	
	public static class LockDebugIdManager {
		
		public static LockDebugIdManager MANAGER = new LockDebugIdManager();
		private long lastTime;
		private int count;
		
		private LockDebugIdManager() {
			
		}
		
		private static ConcurrentHashMap<String, LockDebugId> lockDebugIdMap = new ConcurrentHashMap<>();
		
		public void acquireLock(TRULockManager lockManager, TransactionLockService transactionLockService ) throws DeadlockException, NoLockNameException{
			long currentTime = System.currentTimeMillis();
			int localCount = 0;
			
			synchronized (this) {
			  if (lastTime  == currentTime ) count ++;
			  else count = 0;
			  
			  localCount = count;
			  lastTime = currentTime;
			
			  String lockName = transactionLockService.getLockName();
			  LockDebugId lockDebugId = lockDebugIdMap.get(lockName);
			  if (lockDebugId != null) new Exception("Coding or Product Error for lockName=" + lockName).printStackTrace();
			
			  lockDebugId = new LockDebugId(lockName, lockManager, random.nextInt(Integer.MAX_VALUE), currentTime, localCount);
			  lockDebugIdMap.put(lockName, lockDebugId);
			  System.out.println("Lock ID **** "+lockDebugIdMap.get(lockName));

				transactionLockService.acquireTransactionLock();
		
		    }
			
		}
		
		public void releaseLock(TRULockManager lockManager, TransactionLockService transactionLockService) throws LockManagerException {
			synchronized(this) {
				String lockName = transactionLockService.getLockName();
				transactionLockService.releaseTransactionLock();
				System.out.println("Lock ID **** "+lockDebugIdMap.get(lockName));
				lockDebugIdMap.remove(lockName);
			}
		}
	}

	
	LockDebugIdManager MANAGER = LockDebugIdManager.MANAGER;

}

