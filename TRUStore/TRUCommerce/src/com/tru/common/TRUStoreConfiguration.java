package com.tru.common;

import java.util.List;
import java.util.Map;

import atg.nucleus.GenericService;

/**
 * The class holds the TRUStoreConfiguration property getters and setters.
 *
 * @author PA
 * @version 1.0
 */

public class TRUStoreConfiguration extends GenericService {

	/**
	 * This property hold reference for maximumCategoryLevel.
	 */
	private int mMaximumCategoryLevel;
	/**
	 * Property to hold MegaMenuMaxAllowedRootCategories.
	 */
	private int mMegaMenuMaxAllowedRootCategories;

	/**
	 * Property to hold mCollectionUrl.
	 */
	private String mCollectionUrl;
	/**
	 * Property to hold mEmailLength.
	 */
	private int mEmailLength;

	/**
	 * Property to hold mMegaMenuMaxAllowedLevelOneCategories.
	 */
	private int mMegaMenuMaxAllowedLevelOneCategories;

	/**
	 * Property to hold mMegaMenuMaxAllowedLevelTwoCategories.
	 */
	private int mMegaMenuMaxAllowedLevelTwoCategories;

	/**
	 * Property to hold WishlistRegistryType.
	 */

	private String mWishlistRegistryType;
	
	/**
	 * Property to determine whether server port would be present or not.
	 */
	private boolean mServerPortRequired;

	/**
	 * Property to hold ProductIdsParameterName.
	 */
	private String mProductIdsParameterName;

	/**
	 * Property to hold ProductIdParameterName.
	 */
	private String mProductIdParameterName;


	/**
	 * Property to hold SkuIdParameterName.
	 */
	private String mSkuIdParameterName;

	/**
	 * Property to hold EnableProductFeatureSorting.
	 */
	private boolean mEnableProductFeatureSorting;

	/**
	 * Property to hold EnableCompareImageURL.
	 */
	private boolean mEnableCompareImageURL;

	/**
	 * Property to hold EnableSuppressInSearch.
	 */
	private boolean mEnableSuppressInSearch;
	
	/**
	 * Property to hold EnableInvodoFlag.
	 */
	private boolean mEnableInvodoFlag;


	/**
	 * Property to hold EnableSuppressInSearch.
	 */
	private boolean mEnableRegistryAJAX;

	/**
	 * Property to hold mRegistryWishlistkeyMap.
	 */
	private Map<String, String> mRegistryWishlistkeyMap;
	
	/**
	 * Property to hold mPromotionalStickerDisplayTextMap.
	 */
	private Map<String, String> mPromotionalStickerDisplayTextMap;
	/**
	 * Property to hold CategoryIdParameterName;.
	 */
	private String mCategoryIdParameterName;

	/** The m profile migration file path. */
	private String mProfileMigrationFilePath;
	/**
	 * Property to hold mPdpRegistryUrl.
	 */
	private String mPdpRegistryUrl;
	/**
	 * Property to hold mPdpWishlistUrl.
	 */
	private String mPdpWishlistUrl;

	/**
	 * Property to hold mToysRUsSiteId.
	 */
	private String mToysRUsSiteId;

	/**
	 * Property to hold mBabiesRUsSiteId.
	 */
	private String mBabiesRUsSiteId;

	/**
	 * Property to hold mUserStatus.
	 */
	private boolean mUserStatus;
	/**
	 * Property to hold mPdpRegistryInvokeURL.
	 */
	private String mPdpRegistryInvokeURL;
	/**
	 * Property to hold mPdpWishlistInvokeURL.
	 */
	private String mPdpWishlistInvokeURL;
	/**
	 * Property to hold mStaticAssetsVersion.
	 */
	private String mStaticAssetsVersion;
	/**
	 * Property to hold mPdpChecklistUrl.
	 */
	private String mPdpChecklistUrl;

	/**
	 * Property to hold mViewWishListUrl.
	 */
	private String mViewWishListUrl;

	/**
	 * Property to hold mViewRegistryUrl.
	 */
	private String mViewRegistryUrl;

	/**
	 * Property to hold mBackToWishListUrl.
	 */
	private String mBackToWishListUrl;

	/**
	 * Property to hold mBackToAnonymousRegistryURL.
	 */
	private String mBackToAnonymousRegistryURL;

	/**
	 * Property to hold mBackToAnonymousWishlistURL.
	 */
	private String mBackToAnonymousWishlistURL;

	/**
	 * Property to hold mCreateWishListUrl.
	 */
	private String mCreateWishListUrl;


	/**  Property to hold storeAvailableMessage. */
	  private String mStoreAvailableMessage;

	  /**  Property to hold stylizedItem. */
	  private String mStylizedItem;

	  /**  Property to hold mStoreAvailableMessageS2s. */
	  private String mStoreAvailableMessageS2s;


	  /**  Property to hold storeNotAvailableMessage. */
	  private String mStoreNotAvailableMessage;

	 /**
	 * Property to hold image size for shipping page.
	 */
	private Map<String, String> mShippingImageSizeMap;

	 /**
	   * Property to hold image size for checkout common fragment .
	  */
	private Map<String, String> mCheckoutCommonImageSizeMap;


	 /** Property to hold image size for gift wrapped item. */
	private Map<String, String> mGiftWrappedItemImageSizeMap;

	 /** Property to hold image size for gift wrap eligible item. */
	private Map<String, String> mGiftWrapEligibleItemImageSizeMap;

	 /** Property to hold image size for gift wrap non eligible item. */

	private Map<String, String> mGiftWrapNonEligibleItemImageSizeMap;

	 /** Property to hold image size for product on PDP page. */

	private Map<String, String> mPdpAkamaiImageResizeMap;

	 /** Property to hold image size for product on PDP page. */

	private Map<String, String> mResultListAkamaiImageResizeMap;

	 /** Property to hold image size for collection product. */

	private Map<String, String> mCollectionAkamaiImageResizeMap;

	/** Property to hold image size for quick view product. */

	private Map<String, String> mQuickViewAkamaiImageResizeMap;

	/** Property to hold image size for related product. */

	private String mRelatedProductImageBlock;


	/** Property to hold image size for related product. */

	private Map<String, String> mGridViewHomePageAkamaiImageResizeMap;

	/** Property to hold image url for related Category. */

	private Map<String, String> mCategoryImageResizeMap;
	/**
	 * Property to hold mVersionaingFlag.
	 */
	private boolean mVersioningFlag;
	
	
	/**
	 * Property to hold mEnableJSDebug.
	 */
	private boolean mEnableJSDebug;
	
	
	
	/**
	 * Property to hold mRegistryFlag.
	 */
	private boolean mRegistryFlag;

	/**
	 * @return the mEnableJSDebug
	 * 
	 * 	 */

	
	public boolean isEnableJSDebug() {
		return mEnableJSDebug;
	}

	/**
	 * @param mEnableJSDebug the mEnableJSDebug to set
	 */
	public void setEnableJSDebug(boolean pEnableJSDebug) {
		this.mEnableJSDebug = pEnableJSDebug;
	}

	/**
	 * Property to hold mProductTitleFlag.
	 */
	private boolean mProductTitleFlag;


	/**
	 * Property to hold mEnableRegistryModalFlag.
	 */
	private boolean mEnableRegistryModalFlag;

	/**
	 * Property to hold mInventoryStatus.
	 */
	private boolean mInventoryStatus;

	/**
	 * Property to hold mPopulateShippingMessage.
	 */
	private boolean mPopulateShippingMessage;

	/**
	 * Property to hold mChangeAddTocartButtonFromOverlay.
	 */
	private boolean mChangeAddTocartButtonFromOverlay;

	/**
	 * Property to hold mCanonicalImageUrl.
	 */
	private boolean mCanonicalImageUrl;
	/**
	 * Property to hold mRegistrySwapFlag.
	 */
	private boolean mRegistrySwapFlag;
	/**
	 * Property to hold mBatteryQuantity.
	 */
	private int mBatteryQuantity;
	

	/** Property to hold image static URL for size chart. */
	private String mPdpSizechartImageStaticPath;

	/** Property to hold mRmsSKNOrigin. */
	private int mRmsSKNOrigin;

	/**
	 * Property to hold mWishlistFlag.
	 */
	private boolean mWishlistFlag;
	/** Holds the MaxAllowedProductToCompare. */
	private int mMaxAllowedProductToCompare;

	/** Holds the MinAllowedProductToCompare. */
	private int mMinAllowedProductToCompare;
	/**
	 * This property hold reference for UrlPrefix.
	 */
	private String mUrlPrefix;

	/**
	 * This property hold reference for mNoImageUrl.
	 */
	private String mNoImageUrl;

	/**
	 * This property hold reference for imageUrlParameterName for compareProduct overlay for setting cookies.
	 */
	private String mImageUrlParameterName;

	/**
	 * This property hold reference for mAkamaiNoImageURL.
	 */
	private String mAkamaiNoImageURL;

	/** The Price feed message from. */
	private String mSiteMapMessageFrom;

	/** The Price feed message to. */
	private List<String> mSiteMapMessageTo;


	/** The m footer configuration js. */
	private String mFooterConfigurationJS;

	/** The m footer configuration css. */
	private String mFooterConfigurationCSS;

	/** The m header configuration js. */
	private String mHeaderConfigurationJS;

	/** The m header configuration css. */
	private String mHeaderConfigurationCSS;
	
	/** The Base Line Index message from. */
	private String mBaseLineIndexMessageFrom;

	/** The Base Line Index  message to. */
	private List<String> mBaseLineIndexMessageTo;
	
	/** The Partial index message from. */
	private String mPartialIndexMessageFrom;

	/** The Partial index message to. */
	private List<String> mPartialIndexMessageTo;
	
	
	/** The ParametricFeed message from. */
	private String mParametericFeedMessageFrom;

	/** The ParametricFeed  message to. */
	private List<String> mParametericFeedMessageTo;
	
	/** The ParametricCSV message from. */
	private String mParametricCSVMessageFrom;

	/** The ParametricCSV message to. */
	private List<String> mParametericCSVMessageTo;
	/** The m targeterpath check. */
	private boolean mTargeterpathCheck;
	
	/** The m plpPathCheck check. */
	private boolean mPlpPathCheck;

	/**
	 * Checks if is RegistrySwapFlag flag.
	 *
	 * @return the RegistrySwapFlag
	 */
	public boolean isRegistrySwapFlag() {
		return mRegistrySwapFlag;
	}

	/**
	 * Sets the RegistrySwapFlag flag.
	 *
	 * @param pRegistrySwapFlag the RegistrySwapFlag to set
	 */
	public void setRegistrySwapFlag(boolean pRegistrySwapFlag) {
		mRegistrySwapFlag = pRegistrySwapFlag;
	}

	/**
	 * Gets the Akamai no Image URL.
	 *
	 * @return the Akamai no Image URL
	 */
	public String getAkamaiNoImageURL() {
		return mAkamaiNoImageURL;
	}

	/**
	 * Sets the Akamai no Image URL.
	 *
	 * @param pAkamaiNoImageURL the Akamai no Image URL to set
	 */
	public void setAkamaiNoImageURL(String pAkamaiNoImageURL) {
		mAkamaiNoImageURL = pAkamaiNoImageURL;
	}
	/**
	 * the EnableProductFeatureSorting flag.
	 *
	 * @return the EnableProductFeatureSorting flag
	 */
	public boolean isEnableProductFeatureSorting() {
		return mEnableProductFeatureSorting;
	}

	/**
	 * Sets the EnableProductFeatureSorting flag.
	 *
	 * @param pEnableProductFeatureSorting the ProductFeature  to sort
	 */
	public void setEnableProductFeatureSorting(boolean pEnableProductFeatureSorting) {
		mEnableProductFeatureSorting = pEnableProductFeatureSorting;
	}

	/**
	 * the EnableInvodoFlag flag.
	 *
	 * @return the EnableInvodoFlag flag
	 */
	public boolean isEnableInvodoFlag() {
		return mEnableInvodoFlag;
	}

	/**
	 * Sets the EnableInvodoFlag flag.
	 *
	 * @param pEnableInvodoFlag the EnableInvodoFlag  to set
	 */
	public void setEnableInvodoFlag(boolean pEnableInvodoFlag) {
		mEnableInvodoFlag = pEnableInvodoFlag;
	}
	
	/**
	 * the EnableSuppressInSearch flag.
	 *
	 * @return the EnableSuppressInSearch flag
	 */
	public boolean isEnableSuppressInSearch() {
		return mEnableSuppressInSearch;
	}

	/**
	 * Sets the EnableSuppressInSearch flag.
	 *
	 * @param pEnableSuppressInSearch the EnableSuppressInSearch  to set
	 */
	public void setEnableSuppressInSearch(boolean pEnableSuppressInSearch) {
		mEnableSuppressInSearch = pEnableSuppressInSearch;
	}

	/**
	 * the EnableRegistryAJAX flag.
	 *
	 * @return the EnableRegistryAJAX flag
	 */
	public boolean isEnableRegistryAJAX() {
		return mEnableRegistryAJAX;
	}

	/**
	 * Sets the EnableRegistryAJAX flag.
	 *
	 * @param pEnableRegistryAJAX the EnableRegistryAJAX  to set
	 */
	public void setEnableRegistryAJAX(boolean pEnableRegistryAJAX) {
		mEnableRegistryAJAX = pEnableRegistryAJAX;
	}

	/**
	 * the EnableCompareImageURL flag.
	 *
	 * @return the EnableCompareImageURL flag
	 */
	public boolean isEnableCompareImageURL() {
		return mEnableCompareImageURL;
	}

	/**
	 * Sets the EnableCompareImageURL flag.
	 *
	 * @param pEnableCompareImageURL the CompareImageURL  to set
	 */
	public void setEnableCompareImageURL(boolean pEnableCompareImageURL) {
		mEnableCompareImageURL = pEnableCompareImageURL;
	}


	/**
	 * Gets the category image resize map.
	 *
	 * @return the category image resize map
	 */
	public Map<String, String> getCategoryImageResizeMap() {
		return mCategoryImageResizeMap;
	}

	/**
	 * Sets the category image resize map.
	 *
	 * @param pCategoryImageResizeMap the category image resize map
	 */
	public void setCategoryImageResizeMap(Map<String, String> pCategoryImageResizeMap) {
		mCategoryImageResizeMap = pCategoryImageResizeMap;
	}
	/**
	 * Gets the related product image block.
	 *
	 * @return the mRelatedProductImageBlock
	 */
	public String getRelatedProductImageBlock() {
		return mRelatedProductImageBlock;
	}

	/**
	 * Sets the related product image block.
	 *
	 * @param pRelatedProductImageBlock the mRelatedProductImageBlock to set
	 */
	public void setRelatedProductImageBlock(String pRelatedProductImageBlock) {
		this.mRelatedProductImageBlock = pRelatedProductImageBlock;
	}

	/**
	 * Gets the quick view akamai image resize map.
	 *
	 * @return the mPdpAkamaiImageResizeMap
	 */
	public Map<String, String> getQuickViewAkamaiImageResizeMap() {
		return mQuickViewAkamaiImageResizeMap;
	}

	/**
	 * Sets the quick view akamai image resize map.
	 *
	 * @param pQuickViewAkamaiImageResizeMap the quick view akamai image resize map
	 */
	public void setQuickViewAkamaiImageResizeMap(Map<String, String> pQuickViewAkamaiImageResizeMap) {
		mQuickViewAkamaiImageResizeMap = pQuickViewAkamaiImageResizeMap;
	}

	/**
	 * Gets the pdp akamai image resize map.
	 *
	 * @return the mPdpAkamaiImageResizeMap
	 */
	public Map<String, String> getPdpAkamaiImageResizeMap() {
		return mPdpAkamaiImageResizeMap;
	}

	/**
	 * Sets the pdp akamai image resize map.
	 *
	 * @param pPdpAkamaiImageResizeMap the mPdpAkamaiImageResizeMap to set
	 */

	public void setPdpAkamaiImageResizeMap(Map<String, String> pPdpAkamaiImageResizeMap) {
		mPdpAkamaiImageResizeMap = pPdpAkamaiImageResizeMap;
	}

	/**
	 * Gets the collection akamai image resize map.
	 *
	 * @return the mCollectionAkamaiImageResizeMap
	 */

	public Map<String, String> getCollectionAkamaiImageResizeMap() {
		return mCollectionAkamaiImageResizeMap;
	}

	/**
	 * Sets the collection akamai image resize map.
	 *
	 * @param pCollectionAkamaiImageResizeMap the mCollectionAkamaiImageResizeMap to set
	 */

	public void setCollectionAkamaiImageResizeMap(Map<String, String> pCollectionAkamaiImageResizeMap) {
		mCollectionAkamaiImageResizeMap = pCollectionAkamaiImageResizeMap;
	}

	/**
	 * Gets the gift wrap non eligible item image size map.
	 *
	 * @return the mGiftWrapNonEligibleItemImageSizeMap
	 */

	public Map<String, String> getGiftWrapNonEligibleItemImageSizeMap() {
		return mGiftWrapNonEligibleItemImageSizeMap;
	}

	/**
	 * Sets the gift wrap non eligible item image size map.
	 *
	 * @param pGiftWrapNonEligibleItemImageSizeMap the mGiftWrapNonEligibleItemImageSizeMap to set
	 */

	public void setGiftWrapNonEligibleItemImageSizeMap(
			Map<String, String> pGiftWrapNonEligibleItemImageSizeMap) {
		mGiftWrapNonEligibleItemImageSizeMap = pGiftWrapNonEligibleItemImageSizeMap;
	}

	/**
	 * Gets the gift wrap eligible item image size map.
	 *
	 * @return the mGiftWrapEligibleItemImageSizeMap
	 */

	public Map<String, String> getGiftWrapEligibleItemImageSizeMap() {
		return mGiftWrapEligibleItemImageSizeMap;
	}

	/**
	 * Sets the gift wrap eligible item image size map.
	 *
	 * @param pGiftWrapEligibleItemImageSizeMap the mGiftWrapEligibleItemImageSizeMap to set
	 */
	public void setGiftWrapEligibleItemImageSizeMap(
			Map<String, String> pGiftWrapEligibleItemImageSizeMap) {
		mGiftWrapEligibleItemImageSizeMap = pGiftWrapEligibleItemImageSizeMap;
	}

	/**
	 * Gets the gift wrapped item image size map.
	 *
	 * @return the mGiftWrappedItemImageSizeMap
	 */

	public Map<String, String> getGiftWrappedItemImageSizeMap() {
		return mGiftWrappedItemImageSizeMap;
	}

	/**
	 * Sets the gift wrapped item image size map.
	 *
	 * @param pGiftWrappedItemImageSizeMap the mGiftWrappedItemImageSizeMap to set
	 */
	public void setGiftWrappedItemImageSizeMap(
			Map<String, String> pGiftWrappedItemImageSizeMap) {
		mGiftWrappedItemImageSizeMap = pGiftWrappedItemImageSizeMap;
	}

	/**
	 * Gets the checkout common image size map.
	 *
	 * @return the mCheckoutCommonImageSizeMap
	 */

	public Map<String, String> getCheckoutCommonImageSizeMap() {
		return mCheckoutCommonImageSizeMap;
	}

	/**
	 * Sets the checkout common image size map.
	 * @param pCheckoutCommonImageSizeMap the checkout common image size map
	 */
	public void setCheckoutCommonImageSizeMap(
			Map<String, String> pCheckoutCommonImageSizeMap) {
		mCheckoutCommonImageSizeMap = pCheckoutCommonImageSizeMap;
	}
	/**
	 * Gets the maxlength allowed for email feild.
	 * @return the mEmailLength
	 */

	public int getEmailLength() {
		return mEmailLength;
	}
	/**
	 * Sets the maxlength allowed for email feild.
	 *
	 * @param pEmailLength the EmailLength to set
	 */
	public void setEmailLength(int pEmailLength) {
		mEmailLength = pEmailLength;
	}
	/**
	 * Gets the shipping image size map.
	 *
	 * @return the mShippingImageSizeMap
	 */

	public Map<String, String> getShippingImageSizeMap() {
		return mShippingImageSizeMap;
	}

	/**
	 * Sets the shipping image size map.
	 *
	 * @param pShippingImageSizeMap the mShippingImageSizeMap to set
	 */

	public void setShippingImageSizeMap(Map<String, String> pShippingImageSizeMap) {
		mShippingImageSizeMap = pShippingImageSizeMap;
	}

	/**
	 * Gets the store available message.
	 *
	 * @return the storeAvailableMessage
	 */
	public String getStoreAvailableMessage() {
		return mStoreAvailableMessage;
	}

	/**
	 * Sets the store available message.
	 *
	 * @param pStoreAvailableMessage the storeAvailableMessage to set
	 */
	public void setStoreAvailableMessage(String pStoreAvailableMessage) {
		mStoreAvailableMessage = pStoreAvailableMessage;
	}

	/**
	 * Gets the store not available message.
	 *
	 * @return the storeNotAvailableMessage
	 */
	public String getStoreNotAvailableMessage() {
		return mStoreNotAvailableMessage;
	}

	/**
	 * Sets the store not available message.
	 *
	 * @param pStoreNotAvailableMessage the storeNotAvailableMessage to set
	 */
	public void setStoreNotAvailableMessage(String pStoreNotAvailableMessage) {
		mStoreNotAvailableMessage = pStoreNotAvailableMessage;
	}


	/**
	 * Gets the categoryIdParameterName.
	 *
	 * @return the categoryIdParameterName
	 */
	public String getCategoryIdParameterName() {
		return mCategoryIdParameterName;
	}

	/**
	 * Gets the pdp checklist url.
	 *
	 * @return the pdpChecklistUrl
	 */
	public String getPdpChecklistUrl() {
		return mPdpChecklistUrl;
	}

	/**
	 * Sets the pdp checklist url.
	 *
	 * @param pPdpChecklistUrl the pdpChecklistUrl to set
	 */
	public void setPdpChecklistUrl(String pPdpChecklistUrl) {
		mPdpChecklistUrl = pPdpChecklistUrl;
	}

	/**
	 * Gets the view wish list url.
	 *
	 * @return the viewWishListUrl
	 */
	public String getViewWishListUrl() {
		return mViewWishListUrl;
	}

	/**
	 * Gets the back to wishlist url.
	 *
	 * @return the backToWishListUrl
	 */
	public String getBackToWishListUrl() {
		return mBackToWishListUrl;
	}

	/**
	 * Sets the back to wishlist url.
	 *
	 * @param pBackToWishListUrl the backToWishListUrl to set
	 */
	public void setBackToWishListUrl(String pBackToWishListUrl) {
		mBackToWishListUrl = pBackToWishListUrl;
	}

	/**
	 * Gets the back to anonymous registry url.
	 *
	 * @return the backToAnonymousRegistryURL
	 */
	public String getBackToAnonymousRegistryURL() {
		return mBackToAnonymousRegistryURL;
	}

	/**
	 * Sets the back to anonymous registry url.
	 *
	 * @param pBackToAnonymousRegistryURL the backToAnonymousRegistryURL to set
	 */
	public void setBackToAnonymousRegistryURL(String pBackToAnonymousRegistryURL) {
		mBackToAnonymousRegistryURL = pBackToAnonymousRegistryURL;
	}

	/**
	 * Gets the back to anonymous wishlist url.
	 *
	 * @return the backToAnonymousWishlistURL
	 */
	public String getBackToAnonymousWishlistURL() {
		return mBackToAnonymousWishlistURL;
	}

	/**
	 * Sets the back to anonymous wishlist url.
	 *
	 * @param pBackToAnonymousWishlistURL the backToAnonymousWishlistURL to set
	 */
	public void setBackToAnonymousWishlistURL(String pBackToAnonymousWishlistURL) {
		mBackToAnonymousWishlistURL = pBackToAnonymousWishlistURL;
	}

	/**
	 * Sets the view wish list url.
	 *
	 * @param pViewWishListUrl the viewWishListUrl to set
	 */
	public void setViewWishListUrl(String pViewWishListUrl) {
		mViewWishListUrl = pViewWishListUrl;
	}

	/**
	 * Gets the view registry url.
	 *
	 * @return the mViewRegistryUrl
	 */
	public String getViewRegistryUrl() {
		return mViewRegistryUrl;
	}

	/**
	 * Sets the view registry url.
	 *
	 * @param pViewRegistryUrl the viewRegistryUrl to set
	 */
	public void setViewRegistryUrl(String pViewRegistryUrl) {
		mViewRegistryUrl = pViewRegistryUrl;
	}



	/**
	 * Gets the maxAllowedProductToCompare.
	 *
	 * @return the maxAllowedProductToCompare
	 */
	public int getMaxAllowedProductToCompare() {
		return mMaxAllowedProductToCompare;
	}

	/**
	 * Sets the maxAllowedProductToCompare.
	 *
	 * @param pMaxAllowedProductToCompare the maxAllowedProductToCompare to set
	 */
	public void setMaxAllowedProductToCompare(int pMaxAllowedProductToCompare) {
		mMaxAllowedProductToCompare = pMaxAllowedProductToCompare;
	}


	/**
	 * Sets the categoryIdParameterName.
	 *
	 * @param pCategoryIdParameterName the categoryIdParameterName to set
	 */
	public void setCategoryIdParameterName(String pCategoryIdParameterName) {
		mCategoryIdParameterName = pCategoryIdParameterName;
	}

	/**
	 * Gets the productIdParameterName.
	 *
	 * @return the productIdParameterName
	 */
	public String getProductIdParameterName() {
		return mProductIdParameterName;
	}

	/**
	 * Sets the skuIdParameterName.
	 *
	 * @param pSkuIdParameterName the skuIdParameterName to set
	 */
	public void setSkuIdParameterName(String pSkuIdParameterName) {
		mSkuIdParameterName = pSkuIdParameterName;
	}

	/**
	 * Gets the skuIdParameterName.
	 *
	 * @return the skuIdParameterName
	 */
	public String getSkuIdParameterName() {
		return mSkuIdParameterName;
	}

	/**
	 * Sets the productIdParameterName.
	 *
	 * @param pProductIdParameterName the productIdParameterName to set
	 */
	public void setProductIdParameterName(String pProductIdParameterName) {
		mProductIdParameterName = pProductIdParameterName;
	}

	/**
	 * Gets the productIdsParameterName.
	 *
	 * @return the productIdsParameterName
	 */
	public String getProductIdsParameterName() {
		return mProductIdsParameterName;
	}

	/**
	 * Sets the productIdsParameterName.
	 *
	 * @param pProductIdsParameterName the productIdsParameterName to set
	 */
	public void setProductIdsParameterName(String pProductIdsParameterName) {
		mProductIdsParameterName = pProductIdsParameterName;
	}

	/**
	 * Gets the megaMenuMaxAllowedRootCategories.
	 *
	 * @return the megaMenuMaxAllowedRootCategories
	 */
	public int getMegaMenuMaxAllowedRootCategories() {
		return mMegaMenuMaxAllowedRootCategories;
	}

	/**
	 * Sets the megaMenuMaxAllowedRootCategories.
	 *
	 * @param pMegaMenuMaxAllowedRootCategories the megaMenuMaxAllowedRootCategories to set
	 */
	public void setMegaMenuMaxAllowedRootCategories(int pMegaMenuMaxAllowedRootCategories) {
		mMegaMenuMaxAllowedRootCategories = pMegaMenuMaxAllowedRootCategories;
	}

	/**
	 * Gets the megaMenuMaxAllowedLevelOneCategories.
	 *
	 * @return the megaMenuMaxAllowedLevelOneCategories
	 */
	public int getMegaMenuMaxAllowedLevelOneCategories() {
		return mMegaMenuMaxAllowedLevelOneCategories;
	}

	/**
	 * Gets the pdp registry url.
	 *
	 * @return the pdpRegistryUrl
	 */
	public String getPdpRegistryUrl() {
		return mPdpRegistryUrl;
	}

	/**
	 * Sets the pdp registry url.
	 *
	 * @param pPdpRegistryUrl the pdpRegistryUrl to set
	 */
	public void setPdpRegistryUrl(String pPdpRegistryUrl) {
		mPdpRegistryUrl = pPdpRegistryUrl;
	}

	/**
	 * Sets the megaMenuMaxAllowedLevelOneCategories.
	 *
	 * @param pMegaMenuMaxAllowedLevelOneCategories the megaMenuMaxAllowedLevelOneCategories to set
	 */
	public void setMegaMenuMaxAllowedLevelOneCategories(int pMegaMenuMaxAllowedLevelOneCategories) {
		mMegaMenuMaxAllowedLevelOneCategories = pMegaMenuMaxAllowedLevelOneCategories;
	}

	/**
	 * Gets the pdp wishlist url.
	 *
	 * @return the pdpWishlistUrl
	 */
	public String getPdpWishlistUrl() {
		return mPdpWishlistUrl;
	}

	/**
	 * Sets the pdp wishlist url.
	 *
	 * @param pPdpWishlistUrl the pdpWishlistUrl to set
	 */
	public void setPdpWishlistUrl(String pPdpWishlistUrl) {
		mPdpWishlistUrl = pPdpWishlistUrl;
	}

	/**
	 * Gets the megaMenuMaxAllowedLevelTwoCategories.
	 *
	 * @return the megaMenuMaxAllowedLevelTwoCategories
	 */
	public int getMegaMenuMaxAllowedLevelTwoCategories() {
		return mMegaMenuMaxAllowedLevelTwoCategories;
	}

	/**
	 * Checks if is user status.
	 *
	 * @return the userStatus
	 */
	public boolean isUserStatus() {
		return mUserStatus;
	}

	/**
	 * Sets the user status.
	 *
	 * @param pUserStatus the userStatus to set
	 */
	public void setUserStatus(boolean pUserStatus) {
		mUserStatus = pUserStatus;
	}

	/**
	 * Checks if it product title flag.
	 *
	 * @return the productTitleFlag
	 */
	public boolean isProductTitleFlag() {
		return mProductTitleFlag;
	}

	/**
	 * Sets the productTitleFlag.
	 *
	 * @param pProductTitleFlag the productTitleFlag to set
	 */
	public void setProductTitleFlag(boolean pProductTitleFlag) {
		mProductTitleFlag = pProductTitleFlag;
	}

	/**
	 * Sets the enableRegistryModalFlag.
	 *
	 * @return the enableRegistryModalFlag
	 */
	public boolean isEnableRegistryModalFlag() {
		return mEnableRegistryModalFlag;
	}

	/**
	 * @param pEnableRegistryModalFlag the enableRegistryModalFlag to set
	 */
	public void setEnableRegistryModalFlag(boolean pEnableRegistryModalFlag) {
		mEnableRegistryModalFlag = pEnableRegistryModalFlag;
	}

	/**
	 * Gets the registry wishlistkey map.
	 *
	 * @return the registryWishlistkeyMap
	 */
	public Map<String, String> getRegistryWishlistkeyMap() {
		return mRegistryWishlistkeyMap;
	}
	/**
	 * Gets the  PromotionalStickerDisplayTextMap map.
	 *
	 * @return the promotionalStickerDisplayTextMap
	 */
	public Map<String, String> getPromotionalStickerDisplayTextMap() {
		return mPromotionalStickerDisplayTextMap;
	}
	/**
	 * Checks if is registry flag.
	 *
	 * @return the registryFlag
	 */
	public boolean isRegistryFlag() {
		return mRegistryFlag;
	}

	/**
	 * Sets the registry flag.
	 *
	 * @param pRegistryFlag the registryFlag to set
	 */
	public void setRegistryFlag(boolean pRegistryFlag) {
		mRegistryFlag = pRegistryFlag;
	}

	/**
	 * Checks if is wishlist flag.
	 *
	 * @return the wishlistFlag
	 */
	public boolean isWishlistFlag() {
		return mWishlistFlag;
	}

	/**
	 * Sets the wishlist flag.
	 *
	 * @param pWishlistFlag the wishlistFlag to set
	 */
	public void setWishlistFlag(boolean pWishlistFlag) {
		mWishlistFlag = pWishlistFlag;
	}


	/**
	 * Checks if is inventoryStatus.
	 *
	 * @return the inventoryStatus
	 */
	public boolean isInventoryStatus() {
		return mInventoryStatus;
	}

	/**
	 * sets the inventoryStatus.
	 *
	 * @param pInventoryStatus the inventoryStatus to set
	 */
	public void setInventoryStatus(boolean pInventoryStatus) {
		mInventoryStatus = pInventoryStatus;
	}

	/**
	 * Checks if is populateShippingMessage.
	 *
	 * @return the populateShippingMessage
	 */
	public boolean isPopulateShippingMessage() {
		return mPopulateShippingMessage;
	}

	/**
	 * sets the populateShippingMessage.
	 *
	 * @param pPopulateShippingMessage the populateShippingMessage to set
	 */
	public void setPopulateShippingMessage(boolean pPopulateShippingMessage) {
		mPopulateShippingMessage = pPopulateShippingMessage;
	}

	/**
	 * Checks if is changeAddTocartButtonFromOverlay.
	 *
	 * @return the changeAddTocartButtonFromOverlay
	 */
	public boolean isChangeAddTocartButtonFromOverlay() {
		return mChangeAddTocartButtonFromOverlay;
	}

	/**
	 * sets the changeAddTocartButtonFromOverlay.
	 *
	 * @param pChangeAddTocartButtonFromOverlay the changeAddTocartButtonFromOverlay to set
	 */
	public void setChangeAddTocartButtonFromOverlay(boolean pChangeAddTocartButtonFromOverlay) {
		mChangeAddTocartButtonFromOverlay = pChangeAddTocartButtonFromOverlay;
	}


	/**
	 * Checks if is canonicalImageUrl.
	 *
	 * @return the canonicalImageUrl
	 */
	public boolean isCanonicalImageUrl() {
		return mCanonicalImageUrl;
	}

	/**
	 * sets the canonicalImageUrl.
	 *
	 * @param pCanonicalImageUrl the canonicalImageUrl to set
	 */
	public void setCanonicalImageUrl(boolean pCanonicalImageUrl) {
		mCanonicalImageUrl = pCanonicalImageUrl;
	}

	/**
	 * Sets the registry wishlistkey map.
	 *
	 * @param pRegistryWishlistkeyMap the registryWishlistkeyMap to set
	 */
	public void setRegistryWishlistkeyMap(
			Map<String, String> pRegistryWishlistkeyMap) {
		mRegistryWishlistkeyMap = pRegistryWishlistkeyMap;
	}
	/**
	 * Sets the  PromotionalStickerDisplayTextMap map.
	 *
	 * @param pPromotionalStickerDisplayTextMap the promotionalStickerDisplayText to set
	 */
	public void setPromotionalStickerDisplayTextMap(
			Map<String, String> pPromotionalStickerDisplayTextMap) {
		mPromotionalStickerDisplayTextMap = pPromotionalStickerDisplayTextMap;
	}
	/**
	 * Sets the megaMenuMaxAllowedLevelTwoCategories.
	 *
	 * @param pMegaMenuMaxAllowedLevelTwoCategories the megaMenuMaxAllowedLevelTwoCategories to set
	 */
	public void setMegaMenuMaxAllowedLevelTwoCategories(int pMegaMenuMaxAllowedLevelTwoCategories) {
		mMegaMenuMaxAllowedLevelTwoCategories = pMegaMenuMaxAllowedLevelTwoCategories;
	}

	/**
	 * Gets the pdp registry invoke url.
	 *
	 * @return the pdpRegistryInvokeURL
	 */
	public String getPdpRegistryInvokeURL() {
		return mPdpRegistryInvokeURL;
	}

	/**
	 * Sets the pdp registry invoke url.
	 *
	 * @param pPdpRegistryInvokeURL the pdpRegistryInvokeURL to set
	 */
	public void setPdpRegistryInvokeURL(String pPdpRegistryInvokeURL) {
		mPdpRegistryInvokeURL = pPdpRegistryInvokeURL;
	}

	/**
	 * Gets the minAllowedProductToCompare.
	 *
	 * @return the minAllowedProductToCompare
	 */
	public int getMinAllowedProductToCompare() {
		return mMinAllowedProductToCompare;
	}

	/**
	 * Sets the minAllowedProductToCompare.
	 *
	 * @param pMinAllowedProductToCompare the minAllowedProductToCompare to set
	 */
	public void setMinAllowedProductToCompare(int pMinAllowedProductToCompare) {
		mMinAllowedProductToCompare = pMinAllowedProductToCompare;
	}


	/**
	 * Gets the serverPortRequired.
	 *
	 * @return the serverPortRequired
	 */
	public boolean isServerPortRequired() {
		return mServerPortRequired;
	}

	/**
	 * Sets the serverPortRequired.
	 *
	 * @param pServerPortRequired the serverPortRequired to set
	 */
	public void setServerPortRequired(boolean pServerPortRequired) {
		mServerPortRequired = pServerPortRequired;
	}

	/**
	 * Gets the url prefix.
	 *
	 * @return the url prefix
	 */
	public String getUrlPrefix() {
		return mUrlPrefix;
	}

	/**
	 * Sets the url prefix.
	 *
	 * @param pUrlPrefix the new url prefix
	 */
	public void setUrlPrefix(String pUrlPrefix) {
		this.mUrlPrefix = pUrlPrefix;
	}
	/**
	 * Gets the url ProfileMigrationFilePath.
	 *
	 * @return the url ProfileMigrationFilePath
	 */
	public String getProfileMigrationFilePath() {
		return mProfileMigrationFilePath;
	}

	/**
	 * Sets the pProfileMigrationFilePath.
	 *
	 * @param pProfileMigrationFilePath the ProfileMigrationFilePath
	 */

	public void setProfileMigrationFilePath(String pProfileMigrationFilePath) {
		this.mProfileMigrationFilePath = pProfileMigrationFilePath;
	}
	/**
	 * Gets the url StaticAssetsVersion.
	 *
	 * @return the url StaticAssetsVersion
	 */
	public String getStaticAssetsVersion() {
		return mStaticAssetsVersion;
	}

	/**
	 * Sets the pStaticAssetsVersion.
	 *
	 * @param pStaticAssetsVersion the StaticAssetsVersion
	 */
	public void setStaticAssetsVersion(String pStaticAssetsVersion) {
		mStaticAssetsVersion = pStaticAssetsVersion;
	}
	/**
	 * Gets the url VersionaingFlag.
	 *
	 * @return the url VersionaingFlag
	 */
	public boolean isVersioningFlag() {
		return mVersioningFlag;
	}

	/**
	 * Sets the pVersionaingFlag.
	 *
	 * @param pVersioningFlag the VersionaingFlag
	 */
	public void setVersioningFlag(boolean pVersioningFlag) {
		mVersioningFlag = pVersioningFlag;
	}

	/**
	 * Gets the pdpWishlistInvokeURL.
	 *
	 * @return the pdpWishlistInvokeURL
	 */
	public String getPdpWishlistInvokeURL() {
		return mPdpWishlistInvokeURL;
	}

	/**
	 * Sets the pdpWishlistInvokeURL.
	 *
	 * @param pPdpWishlistInvokeURL the new pdp wishlist invoke url
	 */
	public void setPdpWishlistInvokeURL(String pPdpWishlistInvokeURL) {
		mPdpWishlistInvokeURL = pPdpWishlistInvokeURL;
	}

	/**
	 * Gets the wishlist registry type.
	 *
	 * @return the wishlist registry type
	 */
	public String getWishlistRegistryType() {
		return mWishlistRegistryType;
	}

	/**
	 * Sets the WishlistRegistryType.
	 *
	 * @param pWishlistRegistryType the new wishlist registry type
	 */
	public void setWishlistRegistryType(String pWishlistRegistryType) {
		mWishlistRegistryType = pWishlistRegistryType;
	}

	/**
	 * Gets the creates the wish list url.
	 *
	 * @return the createWishListUrl
	 */
	public String getCreateWishListUrl() {
		return mCreateWishListUrl;
	}

	/**
	 * Sets the creates the wish list url.
	 *
	 * @param pCreateWishListUrl the createWishListUrl to set
	 */
	public void setCreateWishListUrl(String pCreateWishListUrl) {
		mCreateWishListUrl = pCreateWishListUrl;
	}
	/**
	 * Gets the resultListAkamaiImageResizeMapL.
	 *
	 * @return the resultListAkamaiImageResizeMapL
	 */
	public Map<String, String> getResultListAkamaiImageResizeMap() {
		return mResultListAkamaiImageResizeMap;
	}

	/**
	 * Sets the resultListAkamaiImageResizeMapL.
	 *
	 * @param pResultListAkamaiImageResizeMap the result list akamai image resize map
	 */
	public void setResultListAkamaiImageResizeMap(
			Map<String, String> pResultListAkamaiImageResizeMap) {
		mResultListAkamaiImageResizeMap = pResultListAkamaiImageResizeMap;
	}


	/**
	 * Gets the grid view home page akamai image resize map.
	 *
	 * @return the grid view home page akamai image resize map
	 */
	public Map<String, String> getGridViewHomePageAkamaiImageResizeMap() {
		return mGridViewHomePageAkamaiImageResizeMap;
	}

	/**
	 * Sets the grid view home page akamai image resize map.
	 *
	 * @param pGridViewHomePageAkamaiImageResizeMap the grid view home page akamai image resize map
	 */
	public void setGridViewHomePageAkamaiImageResizeMap(
			Map<String, String> pGridViewHomePageAkamaiImageResizeMap) {
		mGridViewHomePageAkamaiImageResizeMap = pGridViewHomePageAkamaiImageResizeMap;
	}

	/**
	 * Gets the toys r us site id.
	 *
	 * @return the toysRUsSiteId
	 */
	public String getToysRUsSiteId() {
		return mToysRUsSiteId;
	}

	/**
	 * Sets the toys r us site id.
	 *
	 * @param pToysRUsSiteId the toysRUsSiteId to set
	 */
	public void setToysRUsSiteId(String pToysRUsSiteId) {
		mToysRUsSiteId = pToysRUsSiteId;
	}

	/**
	 * Gets the babies r us site id.
	 *
	 * @return the babiesRUsSiteId
	 */
	public String getBabiesRUsSiteId() {
		return mBabiesRUsSiteId;
	}

	/**
	 * Sets the babies r us site id.
	 *
	 * @param pBabiesRUsSiteId the babiesRUsSiteId to set
	 */
	public void setBabiesRUsSiteId(String pBabiesRUsSiteId) {
		mBabiesRUsSiteId = pBabiesRUsSiteId;
	}

	/**
	 * Gets the store available message s2s.
	 *
	 * @return the storeAvailableMessageS2s
	 */
	public String getStoreAvailableMessageS2s() {
		return mStoreAvailableMessageS2s;
	}

	/**
	 * Sets the store available message s2s.
	 *
	 * @param pStoreAvailableMessageS2s the storeAvailableMessageS2s to set
	 */
	public void setStoreAvailableMessageS2s(String pStoreAvailableMessageS2s) {
		mStoreAvailableMessageS2s = pStoreAvailableMessageS2s;
	}

	/**
	 * Gets the collection url.
	 *
	 * @return the collectionUrl
	 */
	public String getCollectionUrl() {
		return mCollectionUrl;
	}

	/**
	 * Sets the collection url.
	 *
	 * @param pCollectionUrl the collectionUrl to set
	 */
	public void setCollectionUrl(String pCollectionUrl) {
		mCollectionUrl = pCollectionUrl;
	}

	/**
	 * @return the pdpSizechartImageStaticPath
	 */
	public String getPdpSizechartImageStaticPath() {
		return mPdpSizechartImageStaticPath;
	}

	/**
	 * @param pPdpSizechartImageStaticPath the pdpSizechartImageStaticPath to set
	 */
	public void setPdpSizechartImageStaticPath(String pPdpSizechartImageStaticPath) {
		mPdpSizechartImageStaticPath = pPdpSizechartImageStaticPath;
	}

	/**
	 * @return the rmsSKNOrigin
	 */
	public int getRmsSKNOrigin() {
		return mRmsSKNOrigin;
	}

	/**
	 * @param pRmsSKNOrigin the rmsSKNOrigin to set
	 */
	public void setRmsSKNOrigin(int pRmsSKNOrigin) {
		mRmsSKNOrigin = pRmsSKNOrigin;
	}

	/**
	 * @return the batteryQuantity
	 */
	public int getBatteryQuantity() {
		return mBatteryQuantity;
	}

	/**
	 * @param pBatteryQuantity the batteryQuantity to set
	 */
	public void setBatteryQuantity(int pBatteryQuantity) {
		mBatteryQuantity = pBatteryQuantity;
	}

	/**
	 * @return the noImageUrl
	 */
	public String getNoImageUrl() {
		return mNoImageUrl;
	}

	/**
	 * @param pNoImageUrl the noImageUrl to set
	 */
	public void setNoImageUrl(String pNoImageUrl) {
		mNoImageUrl = pNoImageUrl;
	}
	/**
	 * @return the ImageUrlParameterName
	 */
	public String getImageUrlParameterName() {
		return mImageUrlParameterName;
	}

	/**
	 * @param pImageUrlParameterName the imageUrlParameterName to set cookies
	 */
	public void setImageUrlParameterName(String pImageUrlParameterName) {
		mImageUrlParameterName = pImageUrlParameterName;
	}
	/**
	 * @param pStylizedItem the StylizedItem to set
	 */
	public void setStylizedItem(String pStylizedItem) {
		mStylizedItem = pStylizedItem;
	}
	/**
	 * @return the StylizedItem
	 */
	public String getStylizedItem() {
		return mStylizedItem;
	}

	/**
	 * @return the siteMapMessageFrom
	 */
	public String getSiteMapMessageFrom() {
		return mSiteMapMessageFrom;
	}

	/**
	 * @param pSiteMapMessageFrom the siteMapMessageFrom to set
	 */
	public void setSiteMapMessageFrom(String pSiteMapMessageFrom) {
		mSiteMapMessageFrom = pSiteMapMessageFrom;
	}

	/**
	 *
	 * @return the msiteMapMessageTo
	 */
	public List<String> getSiteMapMessageTo() {
		return mSiteMapMessageTo;
	}

	/**
	 * @param pSiteMapMessageTo the msiteMapMessageTo to set
	 */
	public void setSiteMapMessageTo(List<String> pSiteMapMessageTo) {
		mSiteMapMessageTo = pSiteMapMessageTo;
	}

	/**
	 * @return the footerConfigurationJS
	 */
	public String getFooterConfigurationJS() {
		return mFooterConfigurationJS;
	}

	/**
	 * @param pFooterConfigurationJS the footerConfigurationJS to set
	 */
	public void setFooterConfigurationJS(String pFooterConfigurationJS) {
		mFooterConfigurationJS = pFooterConfigurationJS;
	}

	/**
	 * @return the footerConfigurationCSS
	 */
	public String getFooterConfigurationCSS() {
		return mFooterConfigurationCSS;
	}

	/**
	 * @param pFooterConfigurationCSS the footerConfigurationCSS to set
	 */
	public void setFooterConfigurationCSS(String pFooterConfigurationCSS) {
		mFooterConfigurationCSS = pFooterConfigurationCSS;
	}

	/**
	 * @return the headerConfigurationJS
	 */
	public String getHeaderConfigurationJS() {
		return mHeaderConfigurationJS;
	}

	/**
	 * @param pHeaderConfigurationJS the headerConfigurationJS to set
	 */
	public void setHeaderConfigurationJS(String pHeaderConfigurationJS) {
		mHeaderConfigurationJS = pHeaderConfigurationJS;
	}

	/**
	 * @return the headerConfigurationCSS
	 */
	public String getHeaderConfigurationCSS() {
		return mHeaderConfigurationCSS;
	}

	/**
	 * @param pHeaderConfigurationCSS the headerConfigurationCSS to set
	 */
	public void setHeaderConfigurationCSS(String pHeaderConfigurationCSS) {
		mHeaderConfigurationCSS = pHeaderConfigurationCSS;
	}
	/**
	 * Returns the this property hold reference for MaximumCategoryLevel.
	 * 
	 * @return the MaximumCategoryLevel
	 */
	public int getMaximumCategoryLevel() {
		return mMaximumCategoryLevel;
	}

	/**
	 * Sets the this property hold reference for MaximumCategoryLevel.
	 * 
	 * @param pMaximumCategoryLevel the MaximumCategoryLevel to set
	 */
	public void setMaximumCategoryLevel(int pMaximumCategoryLevel) {
		mMaximumCategoryLevel = pMaximumCategoryLevel;
	}

	/**
	 * @return the baseLineIndexMessageFrom
	 */
	public String getBaseLineIndexMessageFrom() {
		return mBaseLineIndexMessageFrom;
	}

	/**
	 * @param pBaseLineIndexMessageFrom the baseLineIndexMessageFrom to set
	 */
	public void setBaseLineIndexMessageFrom(String pBaseLineIndexMessageFrom) {
		mBaseLineIndexMessageFrom = pBaseLineIndexMessageFrom;
	}

	/**
	 * @return the baseLineIndexMessageTo
	 */
	public List<String> getBaseLineIndexMessageTo() {
		return mBaseLineIndexMessageTo;
	}

	/**
	 * @param pBaseLineIndexMessageTo the baseLineIndexMessageTo to set
	 */
	public void setBaseLineIndexMessageTo(List<String> pBaseLineIndexMessageTo) {
		mBaseLineIndexMessageTo = pBaseLineIndexMessageTo;
	}

	/**
	 * @return the partialIndexMessageFrom
	 */
	public String getPartialIndexMessageFrom() {
		return mPartialIndexMessageFrom;
	}

	/**
	 * @param pPartialIndexMessageFrom the partialIndexMessageFrom to set
	 */
	public void setPartialIndexMessageFrom(String pPartialIndexMessageFrom) {
		mPartialIndexMessageFrom = pPartialIndexMessageFrom;
	}

	/**
	 * @return the PartialIndexMessageTo
	 */
	public List<String> getPartialIndexMessageTo() {
		return mPartialIndexMessageTo;
	}

	/**
	 * @param pPartialIndexMessageTo the PartialIndexMessageTo to set
	 */
	public void setPartialIndexMessageTo(List<String> pPartialIndexMessageTo) {
		mPartialIndexMessageTo = pPartialIndexMessageTo;
	}

	/**
	 * @return the parametericFeedMessageFrom
	 */
	public String getParametericFeedMessageFrom() {
		return mParametericFeedMessageFrom;
	}

	/**
	 * @param pParametericFeedMessageFrom the parametericFeedMessageFrom to set
	 */
	public void setParametericFeedMessageFrom(
			String pParametericFeedMessageFrom) {
		mParametericFeedMessageFrom = pParametericFeedMessageFrom;
	}

	/**
	 * @return the parametericFeedMessageTo
	 */
	public List<String> getParametericFeedMessageTo() {
		return mParametericFeedMessageTo;
	}

	/**
	 * @param pParametericFeedMessageTo the parametericFeedMessageTo to set
	 */
	public void setParametericFeedMessageTo(List<String> pParametericFeedMessageTo) {
		mParametericFeedMessageTo = pParametericFeedMessageTo;
	}

	/**
	 * @return the parametricCSVMessageFrom
	 */
	public String getParametricCSVMessageFrom() {
		return mParametricCSVMessageFrom;
	}

	/**
	 * @param pParametricCSVMessageFrom the parametricCSVMessageFrom to set
	 */
	public void setParametricCSVMessageFrom(String pParametricCSVMessageFrom) {
		mParametricCSVMessageFrom = pParametricCSVMessageFrom;
	}

	/**
	 * @return the parametericCSVMessageTo
	 */
	public List<String> getParametericCSVMessageTo() {
		return mParametericCSVMessageTo;
	}

	/**
	 * @param pParametericCSVMessageTo the parametericCSVMessageTo to set
	 */
	public void setParametericCSVMessageTo(List<String> pParametericCSVMessageTo) {
		mParametericCSVMessageTo = pParametericCSVMessageTo;
	}
	/**
	 * @return the targeterpathCheck
	 */
	public boolean isTargeterpathCheck() {
		return mTargeterpathCheck;
	}

	/**
	 * @param pTargeterpathCheck the targeterpathCheck to set
	 */
	public void setTargeterpathCheck(boolean pTargeterpathCheck) {
		mTargeterpathCheck = pTargeterpathCheck;
	}

	/**
	 * @return the plpPathCheck
	 */
	public boolean isPlpPathCheck() {
		return mPlpPathCheck;
	}

	/**
	 * @param pPlpPathCheck the plpPathCheck to set
	 */
	public void setPlpPathCheck(boolean pPlpPathCheck) {
		mPlpPathCheck = pPlpPathCheck;
	}
}
