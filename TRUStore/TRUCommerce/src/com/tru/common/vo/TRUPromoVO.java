/**
 * 
 */
package com.tru.common.vo;

/**
 * The TRUPromoVO class is used to store the promotion properties.
 *  @author Professional Access
 *  @version 1.0
 */
public class TRUPromoVO {
	
	/**
	 * To Hold mPromoId
	 */
	private String mPromoId;
	
	/**
	 * To Hold mPromoName
	 */
	private String mDetails;
	
	/**
	 * To Hold mStartDate
	 */
	private String mStartDate;
	
	/**
	 * To Hold mEndDate
	 */
	private String mEndDate;
	
	/**
	 * To Hold mPriority
	 */
	private int mPriority;
	
	/**
	 * To Hold mDescription
	 */
	private String mDescription;

	/**
	 * @return the mPromoId
	 */
	public String getPromoId() {
		return mPromoId;
	}

	/**
	 * @param pPromoId the mPromoId to set
	 */
	public void setPromoId(String pPromoId) {
		this.mPromoId = pPromoId;
	}

	

	/**
	 * @return the mDetails
	 */
	public String getDetails() {
		return mDetails;
	}

	/**
	 * @param pDetails the mDetails to set
	 */
	public void setDetails(String pDetails) {
		this.mDetails = pDetails;
	}

	/**
	 * @return the mStartDate
	 */
	public String getStartDate() {
		return mStartDate;
	}

	/**
	 * @param pStartDate the mStartDate to set
	 */
	public void setStartDate(String pStartDate) {
		this.mStartDate = pStartDate;
	}

	/**
	 * @return the mEndDate
	 */
	public String getEndDate() {
		return mEndDate;
	}

	/**
	 * @param pEndDate the mEndDate to set
	 */
	public void setEndDate(String pEndDate) {
		this.mEndDate = pEndDate;
	}

	/**
	 * @return the mPriority
	 */
	public int getPriority() {
		return mPriority;
	}

	/**
	 * @param pPriority the mPriority to set
	 */
	public void setPriority(int pPriority) {
		this.mPriority = pPriority;
	}

	/**
	 * @return the mDescription
	 */
	public String getDescription() {
		return mDescription;
	}

	/**
	 * @param pDescription the mDescription to set
	 */
	public void setDescription(String pDescription) {
		this.mDescription = pDescription;
	}
	

}
