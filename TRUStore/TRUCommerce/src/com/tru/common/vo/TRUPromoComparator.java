package com.tru.common.vo;

import java.util.Comparator;

import com.tru.commerce.TRUCommerceConstants;

/**
 * This class is used to Sort Set by Value.
 * @author PA
 * @version 1.0
 */
public class TRUPromoComparator implements Comparator<TRUPromoVO>{

	/**
	 * Sort Set obj by Value. 
	 * 
	 * @param pKeyA - Key A
	 *           
	 * @param pKeyB - Key B
	 * @return integer
	 */
	@Override
	public int compare(TRUPromoVO pKeyA, TRUPromoVO pKeyB) {
		if (pKeyA.getPriority() > pKeyB.getPriority()) {
			return TRUCommerceConstants.INT_ONE;
		} else {
			return TRUCommerceConstants.STRING_NOT_FOUND;
		}
	}

}
