package com.tru.common.vo;

import java.io.Serializable;

import com.tru.common.TRUConstants;

/**
 * The TRUStoreItemVO class is used to store the store item details which are used to populate Json Data mainly.
 *
 * @author PA
 * @version 1.0
 */
public class TRUStoreItemVO implements Serializable {
	
	 /**  Property To Hold serialVersionUID. */
     private static final long serialVersionUID = 1L;
     
     /**  Property To Hold mStoreItemRepositoryId. */
     private String mStoreItemRepositoryId;
     
     /** Property to hold mInventoryItemStatus. */
     private String mInventoryItemStatus;

	/** Property To Hold mOnHandQty. */
     private double mOnHandQty;

	/**  Property To Hold mDelivery. */
     private String mDelivery;
     
     /**  Property To Hold mSkuId. */
     private String mSkuId;
     
     /**  Property To Hold mWareHouseInventory. */
     private boolean mWareHouseInventoryFlag;
    
     /**
      * Gets the sku id.
      *
      * @return the mSkuId
      */
	public String getSkuId() {
		return mSkuId;
	}
	
	/**
	 * Sets the sku id.
	 *
	 * @param pSkuId the mSkuId to set
	 */
	public void setSkuId(String pSkuId) {
		mSkuId = pSkuId;
	}
	
	/**
	 * Gets the store item repository id.
	 *
	 * @return the mStoreItemRepositoryId
	 */
	public String getStoreItemRepositoryId() {
		return mStoreItemRepositoryId;
	}
	
	/**
	 * Sets the store item repository id.
	 *
	 * @param pStoreItemRepositoryId the mStoreItemRepositoryId to set
	 */
	public void setStoreItemRepositoryId(String pStoreItemRepositoryId) {
		mStoreItemRepositoryId = pStoreItemRepositoryId;
	}
	
    
    /**
     * Gets the inventory item status.
     *
     * @return the inventory item status
     */
    public String getInventoryItemStatus() {
		return mInventoryItemStatus;
	}

	/**
	 * Sets the inventory item status.
	 *
	 * @param pInventoryItemStatus the new inventory item status
	 */
	public void setInventoryItemStatus(String pInventoryItemStatus) {
		this.mInventoryItemStatus = pInventoryItemStatus;
	}
	
	/**
	 * Gets the on hand qty.
	 *
	 * @return the mOnHandQty
	 */
	public double getOnHandQty() {
		return mOnHandQty;
	}
	
	/**
	 * Sets the on hand qty.
	 *
	 * @param pOnHandQty the mOnHandQty to set
	 */
	public void setOnHandQty(double pOnHandQty) {
		mOnHandQty = pOnHandQty;
	}
	
	/**
	 * Gets the delivery.
	 *
	 * @return the mDelivery
	 */
	public String getDelivery() {
		return mDelivery;
	}
	
	/**
	 * Sets the delivery.
	 *
	 * @param pDelivery the mDelivery to set
	 */
	public void setDelivery(String pDelivery) {
		mDelivery = pDelivery;
	}
	
	/**
	 * This method is used to display the all the members data.
	 * @return storeItemVOBuilder.toString()
	 */
	public String toString(){
		StringBuilder storeItemVOBuilder = new StringBuilder();
		storeItemVOBuilder.append(TRUConstants.NEXT_LINE);
		
		storeItemVOBuilder.append(TRUConstants.SKU_ID_EQUALS);
		storeItemVOBuilder.append(getSkuId());
		
		storeItemVOBuilder.append(TRUConstants.COMMA_STORE_ID_EQUALS);
		storeItemVOBuilder.append(getStoreItemRepositoryId());
		
		storeItemVOBuilder.append(TRUConstants.COMMA_HAND_QTY_EQUALS);
		storeItemVOBuilder.append(getOnHandQty());
		
		storeItemVOBuilder.append(TRUConstants.COMMA_DELIVERY_EQUALS);
		storeItemVOBuilder.append(getDelivery());
		
		
		return storeItemVOBuilder.toString();
	}

	/**
	 * Checks if is ware house inventory flag.
	 *
	 * @return the mWareHouseInventoryFlag
	 */
	public boolean isWareHouseInventoryFlag() {
		return mWareHouseInventoryFlag;
	}

	/**
	 * Sets the ware house inventory flag.
	 *
	 * @param pWareHouseInventoryFlag the WareHouseInventoryFlag to set
	 */
	public void setWareHouseInventoryFlag(boolean pWareHouseInventoryFlag) {
		this.mWareHouseInventoryFlag = pWareHouseInventoryFlag;
	}
	
}
