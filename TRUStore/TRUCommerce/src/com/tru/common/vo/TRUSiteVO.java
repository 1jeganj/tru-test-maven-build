package com.tru.common.vo;

/**
 * The TRUSiteVO class is used to store the site item details.
 *  @author Professional Access
 *  @version 1.0
 */
public class TRUSiteVO {
	
	/** Property to hold mSiteURL. */
	private String mSiteURL;
	
	/** Property to hold mSiteStatus. */
	private String mSiteStatus;

	/**
	 * Gets the site url.
	 *
	 * @return the site url
	 */
	public String getSiteURL() {
		return mSiteURL;
	}

	/**
	 * Sets the site url.
	 *
	 * @param pSiteURL the new site url
	 */
	public void setSiteURL(String pSiteURL) {
		this.mSiteURL = pSiteURL;
	}

	/**
	 * Gets the site status.
	 *
	 * @return the site status
	 */
	public String getSiteStatus() {
		return mSiteStatus;
	}

	/**
	 * Sets the site status.
	 *
	 * @param pSiteStatus the new site status
	 */
	public void setSiteStatus(String pSiteStatus) {
		this.mSiteStatus = pSiteStatus;
	}

}
