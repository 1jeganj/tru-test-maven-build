package com.tru.common.vo;
/**
 * The class is used to store the article content details for the specified content repository Article name.
 * @author Professional Access
 * @version 1.0
 */
public class TRUContentVO {
	
	/** Property To Hold ArticleBody. */
	private String mArticleBody;
	
	/** Property To Hold SeoTitleText. */	
	private String mSeoTitleText;
	
	/** Property To Hold SeoMetaDesc. */	
	private String mSeoMetaDesc;
	
	/** Property To Hold SeoMetaKeyword. */
	private String mSeoMetaKeyword;
	
	/** Property To Hold SeoAltDesc. */
	private String mSeoAltDesc;
	
	/**
	 * @return the articleBody
	 */
	public String getArticleBody() {
		return mArticleBody;
	}
	/**
	 * @param pArticleBody the ArticleBody to set
	 */
	public void setArticleBody(String pArticleBody) {
		this.mArticleBody = pArticleBody;
	}
	
	/**
	 * @return the seoTitleText
	 */
	public String getSeoTitleText() {
		return mSeoTitleText;
	}
	/**
	 * @param pSeoTitleText the seoTitleText to set
	 */
	public void setSeoTitleText(String pSeoTitleText) {
		this.mSeoTitleText = pSeoTitleText;
	}
	
	/**
	 * @return the seoMetaDesc
	 */
	public String getSeoMetaDesc() {
		return mSeoMetaDesc;
	}
	/**
	 * @param pSeoMetaDesc the seoMetaDesc to set
	 */
	public void setSeoMetaDesc(String pSeoMetaDesc) {
		this.mSeoMetaDesc = pSeoMetaDesc;
	}
	
	/**
	 * @return the seoMetaKeyword
	 */
	public String getSeoMetaKeyword() {
		return mSeoMetaKeyword;
	}
	/**
	 * @param pSeoMetaKeyword the seoMetaKeyword to set
	 */
	public void setSeoMetaKeyword(String pSeoMetaKeyword) {
		this.mSeoMetaKeyword = pSeoMetaKeyword;
	}
	
	/**
	 * @return the mSeoAltDesc
	 */
	public String getSeoAltDesc() {
		return mSeoAltDesc;
	}
	/**
	 * @param pSeoAltDesc the seoAltDesc to set
	 */
	public void setSeoAltDesc(String pSeoAltDesc) {
		this.mSeoAltDesc = pSeoAltDesc;
	}
}