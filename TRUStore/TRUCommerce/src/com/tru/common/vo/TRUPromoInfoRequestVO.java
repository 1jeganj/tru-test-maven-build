package com.tru.common.vo;

import java.util.Arrays;

/**
 * The Class TRUPromoInfoRequestVO.
 * @author Professional Access
 * @version 1.0
 */
public class TRUPromoInfoRequestVO {

	/**
	 * Property to hold product id.
	 */
	public String[] mSkuIds;

	/**
	 * @return the skuIds
	 */
	public String[] getSkuIds() {
		return mSkuIds;
	}

	/**
	 * @param pSkuIds the skuIds to set
	 */
	public void setSkuIds(String[] pSkuIds) {
		mSkuIds = pSkuIds;
	}
	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return int - Memory value
	 */
	@Override
	public int hashCode() {
		 return Arrays.hashCode(mSkuIds);
	}
	/**
	 * Over-ridden the OOTB equals method.
	 * 
	 * @param pObject - Object to compare
	 * @return boolean - Return true if both object content are equal otherwise false
	 */
	@Override
	public boolean equals(Object pObject) {
		if (this == pObject) {
			return true;
		}
		if (pObject == null || getClass() != pObject.getClass()) {
			return false;
		}
		if (getClass() != pObject.getClass()) {
			return false;
		}
		TRUPromoInfoRequestVO infoRequestObject = (TRUPromoInfoRequestVO) pObject;
		if (!Arrays.equals(mSkuIds, infoRequestObject.getSkuIds())){
			return false;
		}
		return true;
	}
}
