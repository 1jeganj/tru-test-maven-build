package com.tru.common.vo;

import java.util.List;

import com.tru.commerce.TRUCommerceConstants;

/**
 * The TRUPromoContainer class is used to store the promotion properties.
 *  @author Professional Access
 *  @version 1.0
 */
public class TRUPromoContainer {
	/**
	 * To Hold mSkuId
	 */
	private String mSkuId;
	/**
	 * To Hold mPromotions
	 */
	private List<TRUPromoVO> mPromotions;
	
	/**
	 * To Hold mPromoId
	 */
	private String mPromotionId;
	/**
	 * @return the mSkuId
	 */
	public String getSkuId() {
		return mSkuId;
	}
	/**
	 * @param pSkuId the mSkuId to set
	 */
	public void setSkuId(String pSkuId) {
		this.mSkuId = pSkuId;
	}
	/**
	 * @return the promotions
	 */
	public List<TRUPromoVO> getPromotions() {
		return mPromotions;
	}
	/**
	 * @param pPromotions the promotions to set
	 */
	public void setPromotions(List<TRUPromoVO> pPromotions) {
		mPromotions = pPromotions;
	}
	
	/**
	 * Gets the promotion id.
	 *
	 * @return the promotion id
	 */
	public String getPromotionId() {
		return mPromotionId;
	}
	
	/**
	 * Sets the promotion id.
	 *
	 * @param pPromotionId the new promotion id
	 */
	public void setPromotionId(String pPromotionId) {
		this.mPromotionId = pPromotionId;
	}
		
	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return int - Memory value
	 */
	@Override
	public int hashCode() {
		final int prime = TRUCommerceConstants.HASH_CODE_PRIME;
		int result = TRUCommerceConstants.ONE;
		result = prime * result + (mSkuId == null ? 0 : mSkuId.hashCode());
		return result;
	}

	/**
	 * Over-ridden the OOTB equals method.
	 * 
	 * @param pObject - Object to compare
	 * @return boolean - Return true if both object content are equal otherwise false
	 */
	@Override
	public boolean equals(Object pObject) {
		if (this == pObject) {
			return true;
		}
		if (pObject == null) {
			return false;
		}
		if (getClass() != pObject.getClass()) {
			return false;
		}
		TRUPromoContainer other = (TRUPromoContainer) pObject;
		
		if (mSkuId == null) {
			if (other.mSkuId != null) {
				return false;
			}
		} else if (!mSkuId.equals(other.mSkuId)) {
			return false;
		}
		return true;
	}
}
