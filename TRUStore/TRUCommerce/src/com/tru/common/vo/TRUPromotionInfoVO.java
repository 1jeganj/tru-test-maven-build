package com.tru.common.vo;

import java.util.List;

/**
 * The Class TRUPromotionInfoVO.
 * @author Professional Access
 * @version 1.0
 */
public class TRUPromotionInfoVO {
	/**
	 * To Hold promotionInfoList
	 */
	private List<TRUPromoContainer> mPromotionInfoList;

	/**
	 * @return the promotionInfoList
	 */
	public List<TRUPromoContainer> getPromotionInfoList() {
		return mPromotionInfoList;
	}

	/**
	 * @param pPromotionInfoList the promotionInfoList to set
	 */
	public void setPromotionInfoList(List<TRUPromoContainer> pPromotionInfoList) {
		mPromotionInfoList = pPromotionInfoList;
	}
}
