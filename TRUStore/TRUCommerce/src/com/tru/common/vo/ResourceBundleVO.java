package com.tru.common.vo;


/**
 * This is a bean class to hold resource bundle key value pair.
 * @author PA
 * @version 1.0 
 * 
 */

public class ResourceBundleVO {
	
	/**  
	 *  Property to hold mKey.
	 */	
	private String mKey;
	
	/**  
	 *  Property to hold The mMessage.
	 */	
	private String mMessage;
	
	/**
	 * @return the mKey
	 */
	public String getKey() {
		return mKey;
	}
	
	/**
	 * @param pKey the mKey to set
	 */
	public void setKey(String pKey) {
		mKey = pKey;
	}
	
	/**
	 * @return the mMessage
	 */
	public String getMessage() {
		return mMessage;
	}
	
	/**
	 * @param pMessage the mMessage to set
	 */
	public void setMessage(String pMessage) {
		mMessage = pMessage;
	}
}
