package com.tru.common.vo;

/**
 * The Class TRUAddressDoctorVO.
 * @author Professional Access
 * @version 1.0
 */
public class TRUAddressDoctorVO {

	/**
	 *  property to hold mailabilityScore.
	 */
	private String mMailabilityScore;
	
	/**
	 *  property to hold elementResultStatus.
	 */
	private String mElementResultStatus;
	
	/**
	 *  property to hold Address1.
	 */
	private String mAddress1;
	/**
	 *  property to hold Address2.
	 */
	private String mAddress2;
	/**
	 *  property to hold City.
	 */
	private String mCity;
	/**
	 *  property to hold State.
	 */
	private String mState;
	/**
	 *  property to hold PostalCode.
	 */
	private String mPostalCode;
	/**
	 *  property to hold Country.
	 */
	private String mCountry;
	/**
	 *  property to hold FullAddressName.
	 */
	private String mFullAddressName;
	/**
	 *  property to hold Count.
	 */
	private int mCount;
	
	/**
	 * @return the address1.
	 */
	public String getAddress1() {
		return mAddress1;
	}

	/**
	 * @param pAddress1 the address1 to set.
	 */
	public void setAddress1(String pAddress1) {
		mAddress1 = pAddress1;
	}

	/**
	 * @return the address2.
	 */
	public String getAddress2() {
		return mAddress2;
	}

	/**
	 * @param pAddress2 the address2 to set.
	 */
	public void setAddress2(String pAddress2) {
		mAddress2 = pAddress2;
	}

	/**
	 * @return the city.
	 */
	public String getCity() {
		return mCity;
	}

	/**
	 * @param pCity the city to set.
	 */
	public void setCity(String pCity) {
		mCity = pCity;
	}

	/**
	 * @return the state.
	 */
	public String getState() {
		return mState;
	}

	/**
	 * @param pState the state to set.
	 */
	public void setState(String pState) {
		mState = pState;
	}

	/**
	 * @return the postalCode.
	 */
	public String getPostalCode() {
		return mPostalCode;
	}

	/**
	 * @param pPostalCode the postalCode to set.
	 */
	public void setPostalCode(String pPostalCode) {
		mPostalCode = pPostalCode;
	}

	/**
	 * @return the country.
	 */
	public String getCountry() {
		return mCountry;
	}

	/**
	 * @param pCountry the country to set.
	 */
	public void setCountry(String pCountry) {
		mCountry = pCountry;
	}

	/**
	 * @return the fullAddressName.
	 */
	public String getFullAddressName() {
		return mFullAddressName;
	}

	/**
	 * @param pFullAddressName the fullAddressName to set.
	 */
	public void setFullAddressName(String pFullAddressName) {
		mFullAddressName = pFullAddressName;
	}

	/**
	 * @return the count.
	 */
	public int getCount() {
		return mCount;
	}

	/**
	 * @param pCount the count to set.
	 */
	public void setCount(int pCount) {
		mCount = pCount;
	}

	/**
	 * @return the mailabilityScore.
	 */
	public String getMailabilityScore() {
		return mMailabilityScore;
	}

	/**
	 * @param pMailabilityScore the mMailabilityScore to set.
	 */
	public void setMailabilityScore(String pMailabilityScore) {
		mMailabilityScore = pMailabilityScore;
	}

	/**
	 * @return the elementResultStatus.
	 */
	public String getElementResultStatus() {
		return mElementResultStatus;
	}

	/**
	 * @param pElementResultStatus the mElementResultStatus to set.
	 */
	public void setElementResultStatus(String pElementResultStatus) {
		mElementResultStatus = pElementResultStatus;
	}
}
