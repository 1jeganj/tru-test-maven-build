package com.tru.common.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class TRUAddressDoctorResponseVO.
 * @author Professional Access
 * @version 1.0
 */
public class TRUAddressDoctorResponseVO {

	/**
	 *  property to hold adProcessStatus.
	 */
	private String mAdProcessStatus;
	
	/**
	 *  property to hold derivedStatus.
	 */
	private String mDerivedStatus;
	
	/**
	 *  property to hold list of AddressDoctorVOs.
	 */
	private List<TRUAddressDoctorVO> mAddressDoctorVO = new ArrayList<TRUAddressDoctorVO>();
	/**
	 *  property to hold ProcessStatus.
	 */
	private String mProcessStatus;
	/**
	 *  property to hold AddressDoctorMessage.
	 */
	private String mAddressDoctorMessage;

	/**
	 *  property to hold AddressRecognized.
	 */
	private boolean mAddressRecognized = Boolean.TRUE;

	/**
	 * @return the addressRecognized.
	 */
	public boolean isAddressRecognized() {
		return mAddressRecognized;
	}

	/**
	 * @param pAddressRecognized the addressRecognized to set.
	 */
	public void setAddressRecognized(boolean pAddressRecognized) {
		mAddressRecognized = pAddressRecognized;
	}

	/**
	 * @return the addressDoctorVO.
	 */
	public List<TRUAddressDoctorVO> getAddressDoctorVO() {
		return mAddressDoctorVO;
	}

	/**
	 * @param pAddressDoctorVO the addressDoctorVO to set.
	 */
	public void setAddressDoctorVO(List<TRUAddressDoctorVO> pAddressDoctorVO) {
		mAddressDoctorVO = pAddressDoctorVO;
	}

	/**
	 * @return the processStatus.
	 */
	public String getProcessStatus() {
		return mProcessStatus;
	}

	/**
	 * @param pProcessStatus the processStatus to set.
	 */
	public void setProcessStatus(String pProcessStatus) {
		mProcessStatus = pProcessStatus;
	}

	/**
	 * @return the addressDoctorMessage.
	 */
	public String getAddressDoctorMessage() {
		return mAddressDoctorMessage;
	}

	/**
	 * @param pAddressDoctorMessage the addressDoctorMessage to set.
	 */
	public void setAddressDoctorMessage(String pAddressDoctorMessage) {
		mAddressDoctorMessage = pAddressDoctorMessage;
	}
	
	/**
	 * @return the adProcessStatus.
	 */
	public String getAdProcessStatus() {
		return mAdProcessStatus;
	}

	/**
	 * @param pAdProcessStatus the mAdProcessStatus to set.
	 */
	public void setAdProcessStatus(String pAdProcessStatus) {
		mAdProcessStatus = pAdProcessStatus;
	}

	/**
	 * @return the derivedStatus.
	 */
	public String getDerivedStatus() {
		return mDerivedStatus;
	}

	/**
	 * @param pDerivedStatus the mDerivedStatus to set.
	 */
	public void setDerivedStatus(String pDerivedStatus) {
		mDerivedStatus = pDerivedStatus;
	}
}
