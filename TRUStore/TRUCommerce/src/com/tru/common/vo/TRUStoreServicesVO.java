package com.tru.common.vo;

import java.io.Serializable;

/**
 * The TRUStoreServicesVO class is used to store the service details for the corresponding Store item.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUStoreServicesVO implements Serializable {
	
	/** Property To Hold serialVersionUID. */
    private static final long serialVersionUID = 2L;
    
    /** Property To Hold mServiceName. */
	private String mServiceName;
	
	/** Property To Hold mServiceDesc. */
	private String mServiceDesc;
	
	/** Property To Hold mLearnMore. */
	private String mLearnMore;

	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return mServiceName;
	}

	/**
	 * @param pServiceName the serviceName to set
	 */
	public void setServiceName(String pServiceName) {
		mServiceName = pServiceName;
	}

	/**
	 * @return the serviceDesc
	 */
	public String getServiceDesc() {
		return mServiceDesc;
	}

	/**
	 * @param pServiceDesc the serviceDesc to set
	 */
	public void setServiceDesc(String pServiceDesc) {
		mServiceDesc = pServiceDesc;
	}

	/**
	 * @return the learnMore
	 */
	public String getLearnMore() {
		return mLearnMore;
	}

	/**
	 * @param pLearnMore the learnMore to set
	 */
	public void setLearnMore(String pLearnMore) {
		mLearnMore = pLearnMore;
	}
	
	
	

}
