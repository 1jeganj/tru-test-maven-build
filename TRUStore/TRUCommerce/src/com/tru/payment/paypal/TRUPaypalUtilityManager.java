package com.tru.payment.paypal;

import java.beans.IntrospectionException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupManager;
import atg.commerce.order.purchase.PaymentGroupMapContainer;
import atg.commerce.order.purchase.ShippingGroupMapContainer;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUChannelInStorePickupShippingGroup;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUPaymentGroupManager;
import com.tru.commerce.order.TRUShippingGroupManager;
import com.tru.commerce.order.TRUShippingManager;
import com.tru.commerce.order.purchase.TRUPaymentGroupContainerService;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.TRUIntegrationConfiguration;
import com.tru.common.TRUSOSIntegrationConfig;
import com.tru.common.TRUUserSession;
import com.tru.common.cml.SystemException;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;
import com.tru.radial.commerce.payment.paypal.TRUPaypalConstants;
import com.tru.radial.commerce.payment.paypal.util.TRUPayPalConstants;
import com.tru.radial.commerce.payment.paypal.util.TRUPayPalException;
import com.tru.radial.common.TRURadialConfiguration;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.common.beans.TRURadialError;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.radial.payment.paypal.PayPalAdaptor;
import com.tru.radial.payment.paypal.bean.GetExpressCheckOutRequest;
import com.tru.radial.payment.paypal.bean.GetExpressCheckoutResponse;
import com.tru.radial.payment.paypal.bean.PayPalResponse;
import com.tru.radial.payment.paypal.bean.SetExpressCheckoutRequest;
import com.tru.radial.payment.paypal.bean.SetExpressCheckoutResponse;
import com.tru.userprofiling.TRUPropertyManager;


/**
 * This Manager class interact to integration layer to invoke PayPal API payment service calls like setExpressCheckout,getExpressCheckoutDetails etc.
 * 
 * @author PA
 *
 */
public class TRUPaypalUtilityManager extends GenericService{

	private static final String US_LOCALE = "en_US";
	/**
	 * Property to hold instance of ProfileTools
	 */
	private TRUShippingManager	mShippingManager;
	/**
	 * Property to hold instance of ProfileTools
	 */
	private TRUProfileTools mProfileTools;
	/**
	 * Property to hold instance of PropertyManager
	 */	
	private TRUPropertyManager mPropertyManager;
	/**
	 * Property to hold instance of PayPalAdaptor
	 */
	PayPalAdaptor  mPayPalAdaptor;

	/**
	 * Property to hold reference to OrderManager
	 */
	private TRUOrderManager mOrderManager;

	/** Property to hold paymentGroupManager;. */
	private TRUPaymentGroupManager mPaymentGroupManager;

	/** property to hold addressClassName instance. */
	private String mAddressClassName;

	

	/** Property to Hold Error handler manager. */
	private DefaultErrorHandlerManager mErrorHandlerManager;

	/**
	 * This property hold reference for TRUConfiguration.
	 */
	private TRUConfiguration mTruConfiguration;	
	
	/** The Pricing tools. */
	private TRUPricingTools mPricingTools;
	
	/** The m radialconfig. */
	private TRURadialConfiguration mRadialconfig;
	
	/**
	 * Gets the radialconfig.
	 *
	 * @return the mRadialconfig
	 */
	public TRURadialConfiguration getRadialconfig() {
		return mRadialconfig;
	}

	/**
	 * Sets the radialconfig.
	 *
	 * @param pRadialconfig the new radialconfig
	 */
	public void setRadialconfig(TRURadialConfiguration pRadialconfig) {
		mRadialconfig = pRadialconfig;
	}

	/**
	 * @return the pricingTools
	 */
	public TRUPricingTools getPricingTools() {
		return mPricingTools;
	}

	/**
	 * @param pPricingTools the pricingTools to set
	 */
	public void setPricingTools(TRUPricingTools pPricingTools) {
		mPricingTools = pPricingTools;
	}

	/**
	 * Gets the address class name.
	 * 
	 * @return the addressClassName
	 */
	public String getAddressClassName() {
		return mAddressClassName;
	}

	/**
	 * Sets the address class name.
	 * 
	 * @param pAddressClassName
	 *            the AddressClassName to set
	 */
	public void setAddressClassName(String pAddressClassName) {
		mAddressClassName = pAddressClassName;
	}

	/**
	 * 	
	 * @return mPayPalAdaptor instance of PayPalAdaptor
	 */
	public PayPalAdaptor getPayPalAdaptor() {
		return mPayPalAdaptor;
	}

	/**
	 * @param pPayPalAdaptor the PayPalAdaptor to set
	 */
	public void setPayPalAdaptor(PayPalAdaptor pPayPalAdaptor) {
		this.mPayPalAdaptor = pPayPalAdaptor;
	}

	/**
	 * @return the orderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}
	
	/**
	 * @param pOrderManager the orderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		this.mOrderManager = pOrderManager;
	}

	/**
	 * @return the paymentGroupManager
	 */
	public TRUPaymentGroupManager getPaymentGroupManager() {
		return mPaymentGroupManager;
	}

	/**
	 * @param pPaymentGroupManager the paymentGroupManager to set
	 */
	public void setPaymentGroupManager(TRUPaymentGroupManager pPaymentGroupManager) {
		this.mPaymentGroupManager = pPaymentGroupManager;
	}

	/**
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * @return the profileTools
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * @param pProfileTools the profileTools to set
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}
	/**
	 * @return the shippingManager
	 */
	public TRUShippingManager getShippingManager() {
		return mShippingManager;
	}

	/**
	 * @param pShippingManager the shippingManager to set
	 */
	public void setShippingManager(TRUShippingManager pShippingManager) {
		mShippingManager = pShippingManager;
	}


	/**
	 * This method populates request parameters for SetExpressCheckout API Call. 
	 * @param pLocale
	 * 			- the user locale
	 * @param pOrder
	 * 			- the current Order
	 * @param pSetExpCheckoutRequest
	 * 			- the SetExpCheckoutRequest object
	 * @param pCancelURL
	 * 			- the PayPal Cancel URL
	 * @param pReturnURL
	 * 			- the PayPal return URL
	 * @param pProfile the user Profile
	 * @param pPaymentGroupContainerService PaymentGroupMapContainer
	 * @throws CommerceException CommerceException
	 */
	public void populateRequestAttributes(String pLocale, Order pOrder, SetExpressCheckoutRequest pSetExpCheckoutRequest,
			String pCancelURL,String pReturnURL, RepositoryItem pProfile, PaymentGroupMapContainer pPaymentGroupContainerService) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug(" Start ofthe TRUPaypalUtilityManager.populateRequestAttributes() method ");
		}
		pSetExpCheckoutRequest.setOrderId(pOrder.getId());	
		pSetExpCheckoutRequest.setPageName(((TRUOrderImpl)pOrder).getCurrentTab());
		pSetExpCheckoutRequest.setAmount(pOrder.getPriceInfo().getAmount());
		pSetExpCheckoutRequest.setCancelURL(pCancelURL);		
		pSetExpCheckoutRequest.setReturnURL(pReturnURL);		
		if(!StringUtils.isBlank(pLocale)){
			/*String[] lSplit = pLocale.split(TRUConstants.UNDER_SCORE);
			if(lSplit != null && lSplit.length > TRUConstants.SIZE_ONE){
				pSetExpCheckoutRequest.setLocaleCode(lSplit[TRUConstants.SIZE_ONE]);
			}*/
			pSetExpCheckoutRequest.setLocaleCode(pLocale);
		}else{
			pSetExpCheckoutRequest.setLocaleCode(US_LOCALE);
		}
		//here populating Order level Information 
		populateOrderInfo(pOrder,pSetExpCheckoutRequest,pProfile, pPaymentGroupContainerService);
		if (isLoggingDebug()) {
			vlogDebug(" end ofthe TRUPaypalUtilityManager.populateRequestAttributes() method ");
		}
	}


	/**
	 * Populating the Order Information.
	 * @param pOrder
	 * 			- the current Order
	 * @param pSetExpCheckoutRequest
	 * 			- The SetExpCheckoutRequest object
	 * @param pProfile the user Profile
	 * @param pPaymentGroupContainerService PaymentGroupMapContainer
	 * @throws CommerceException CommerceException
	 */
	public void populateOrderInfo(Order pOrder, SetExpressCheckoutRequest pSetExpCheckoutRequest, RepositoryItem pProfile, PaymentGroupMapContainer pPaymentGroupContainerService) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START : populateOrderInfo method of the TRUPayPalUtilityManager");
		}
		HardgoodShippingGroup shippingGroup = null;
		double lOrderAmount = TRUConstants.DOUBLE_ZERO;
		// get order amount
			if(pOrder != null && pOrder.getPriceInfo() != null) {
				lOrderAmount = getPaymentGroupManager().getRemainingOrderAmount(pOrder);
				pSetExpCheckoutRequest.setAmount(getPricingTools().round(lOrderAmount));
			}
			if(pOrder.getShippingGroups() != null && !pOrder.getShippingGroups().isEmpty()) {
				shippingGroup = getShippingGroupForPayPal(pOrder);		
				if(shippingGroup instanceof TRUChannelHardgoodShippingGroup){
					pSetExpCheckoutRequest.setAddOverRide(String.valueOf(TRUConstants._0));
					pSetExpCheckoutRequest.setShippingAddress(((TRUChannelHardgoodShippingGroup)shippingGroup).getShippingAddress());
					TRUChannelHardgoodShippingGroup channelHardgoodShippingGroup = ((TRUChannelHardgoodShippingGroup)shippingGroup);
					String shipToName  =channelHardgoodShippingGroup.getNickName() ;
					if(channelHardgoodShippingGroup.getShippingAddress()!=null  && StringUtils.isNotBlank(channelHardgoodShippingGroup.getShippingAddress().getFirstName())){
						shipToName = channelHardgoodShippingGroup.getShippingAddress().getFirstName()+TRUConstants.EMPTY+channelHardgoodShippingGroup.getShippingAddress().getLastName();
					}
					pSetExpCheckoutRequest.setShipToName(shipToName);					
				}else if(shippingGroup instanceof TRUHardgoodShippingGroup){
					pSetExpCheckoutRequest.setAddOverRide(String.valueOf(TRUConstants._1));
					if(((TRUHardgoodShippingGroup)shippingGroup).isRegistryAddress()){
						pSetExpCheckoutRequest.setAddOverRide(String.valueOf(TRUConstants._0));
					}
					pSetExpCheckoutRequest.setShippingAddress(((TRUHardgoodShippingGroup)shippingGroup).getShippingAddress());
					TRUHardgoodShippingGroup hardgoodShippingGroup = ((TRUHardgoodShippingGroup)shippingGroup);
					String shipToName  =hardgoodShippingGroup.getNickName() ;
					if(hardgoodShippingGroup.getShippingAddress()!=null  && StringUtils.isNotBlank(hardgoodShippingGroup.getShippingAddress().getFirstName())){
						shipToName = hardgoodShippingGroup.getShippingAddress().getFirstName()+TRUConstants.WHITE_SPACE+hardgoodShippingGroup.getShippingAddress().getLastName();
					}
					pSetExpCheckoutRequest.setShipToName(shipToName);
				}else{
					pSetExpCheckoutRequest.setNoShippingAddressDisplay(String.valueOf(TRUConstants._2));
					if(isLoggingDebug()){
						vlogDebug("TRUPaypalUtiliyManger.populateOrderInfo getting the shippingGroup is null");
					}				
				}
			}			
		if (isLoggingDebug()) {
			vlogDebug("END : populateOrderInfo method of the TRUPayPalUtilityManager");
		}
	}

	/**
	 * This method will return the shipping group with highest price.
	 * @param pOrder - pShippingGroupsList
	 * @return shippingGroup1
	 */
	public HardgoodShippingGroup shippingGroupForPayPalRequest(Order pOrder){
		if (isLoggingDebug()) {
			vlogDebug("START : TRUPaypalUtilityManager.shippingGroupForPayPalRequest");
		}
		HardgoodShippingGroup shippingGroup = null;
		List<ShippingGroup> shippingGroupList = pOrder.getShippingGroups();
		boolean shippingGroupRegistrant = false;
		Double maxShippingAmount = TRUConstants.DOUBLE_ZERO;
		HardgoodShippingGroup shipGropForPaypal =null;
		if(shippingGroupList != null && !shippingGroupList.isEmpty()){
			for(ShippingGroup shippingGroup1 :shippingGroupList){
				if(shippingGroup1 instanceof TRUChannelHardgoodShippingGroup || shippingGroup1 instanceof TRUChannelInStorePickupShippingGroup ){
					shippingGroupRegistrant = true;
					if(shippingGroup1 instanceof TRUChannelHardgoodShippingGroup){
						shipGropForPaypal = (HardgoodShippingGroup) shippingGroup1;
					}
					break;
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("TRUPaypalUtilityManager.shippingGroupForPayPalRequest shippingGroupRegistrant::"+ shippingGroupRegistrant);
			}
			if(shippingGroupList.size() == TRUConstants.INTEGER_NUMBER_ONE && shippingGroupRegistrant){
				if (isLoggingDebug()) {
					vlogDebug("TRUPaypalUtilityManager.shippingGroupForPayPalRequest having single shipping address and  having Registrant address ::Start");
				}
				ShippingGroup shippingGroup1 =shippingGroupList.get(TRUConstants.INTEGER_NUMBER_ZERO);
				if(shippingGroup1 instanceof HardgoodShippingGroup){
					HardgoodShippingGroup hgShippingGroup = (HardgoodShippingGroup) shippingGroup1;
					if (isLoggingDebug()) {
						vlogDebug("TRUPaypalUtilityManager.shippingGroupForPayPalRequest having single shipping address and  having Registrant address ::End"+hgShippingGroup.getShippingAddress());
					}
					return hgShippingGroup;
				}
			} else if(shippingGroupList.size() > TRUConstants.INTEGER_NUMBER_ONE && !shippingGroupRegistrant) {
				for(ShippingGroup shippingGroup1 :shippingGroupList){
					double shippingGroupAmount = 0;
					if(shippingGroup1 instanceof TRUHardgoodShippingGroup){						
						TRUHardgoodShippingGroup hgShippingGroup = (TRUHardgoodShippingGroup) shippingGroup1;
						shippingGroupAmount = hgShippingGroup.getShippingPrice();
						CommerceItemRelationship commerceItemRel = null;
						for (Object commerceItemRelObj : hgShippingGroup.getCommerceItemRelationships())
						{
							commerceItemRel = (CommerceItemRelationship)commerceItemRelObj;
							if(commerceItemRel.getCommerceItem().getPriceInfo() != null){
								shippingGroupAmount+=commerceItemRel.getCommerceItem().getPriceInfo().getAmount();
							}
						}
						if(shippingGroupAmount > maxShippingAmount){
							if (isLoggingDebug()) {
								vlogDebug("TRUPaypalUtilityManager.shippingGroupForPayPalRequest having multiple shipping address,highest shipping group price and no Registrant address::Start");
							}
							maxShippingAmount= roundTwoPlaceDecimal(shippingGroupAmount, TRUConstants.TWO);
							shippingGroup = hgShippingGroup;
							if (isLoggingDebug()) {
								vlogDebug("TRUPaypalUtilityManager.shippingGroupForPayPalRequest having multiple shipping address,highest shipping group price and no Registrant address ::maxShippingAmount"+maxShippingAmount+" the Shipping Address::"+shippingGroup.getShippingAddress());
							}
						}
					}
				}
				return shippingGroup;
			} else if(pOrder.getPaymentGroups().size() == TRUConstants.INTEGER_NUMBER_ONE && pOrder.getShippingGroups().size() == TRUConstants.INTEGER_NUMBER_ONE && !shippingGroupRegistrant){
				if (isLoggingDebug()) {
					vlogDebug("TRUPaypalUtilityManager.shippingGroupForPayPalRequest having one shipping address,billing address and no Registrant address :: Start");
				}
				if(pOrder.getShippingGroups().get(TRUConstants.INTEGER_NUMBER_ZERO) instanceof HardgoodShippingGroup) {
					shippingGroup = (HardgoodShippingGroup) pOrder.getShippingGroups().get(TRUConstants.INTEGER_NUMBER_ZERO);
					if (isLoggingDebug()) {
						vlogDebug("TRUPaypalUtilityManager.shippingGroupForPayPalRequest having one shipping address,billing address and no Registrant address :: End" + shippingGroup.getShippingAddress());
					}
				}
				return shippingGroup;
			} else if(shippingGroupList.size() > TRUConstants.INTEGER_NUMBER_ZERO && shippingGroupRegistrant){				
			//	shippingGroup = getShippingGroupRegistrant(pOrder);
				shippingGroup = shipGropForPaypal;
			} else {
				if(pOrder.getShippingGroups().get(TRUConstants.INTEGER_NUMBER_ZERO) instanceof TRUHardgoodShippingGroup){
				shippingGroup = (HardgoodShippingGroup) pOrder.getShippingGroups().get(TRUConstants.INTEGER_NUMBER_ZERO);
				}				
			}
			if (isLoggingDebug()) {
				vlogDebug("END : TRUPaypalUtilityManager.shippingGroupForPayPalRequest");
			}
		}
		return shippingGroup;
	}

	/**
	 * This method will return shippingGroupForPayPalRequest having multiple shipping address and a registrant Address
	 * @param pOrder order
	 * @return shippingGroup 
	 */
	public HardgoodShippingGroup getShippingGroupRegistrant(Order pOrder){
		if (isLoggingDebug()) {
			vlogDebug("TRUPaypalUtilityManager.shippingGroupForPayPalRequest.getShippingGroupRegistrant having multiple shipping address and a registrant Address :: Start");
		}
		HardgoodShippingGroup shippingGroup = new HardgoodShippingGroup();
		TRUPayPal paymentGroup = (TRUPayPal) getOrderManager().fetchPayPalPaymentGroupForOrder(pOrder);
		if (isLoggingDebug()) {
			vlogDebug("TRUPaypalUtilityManager.shippingGroupForPayPalRequest.getShippingGroupRegistrant payemnt group : " + paymentGroup);
		}
		ContactInfo billAddress = new ContactInfo();
		ContactInfo shipAddress= new ContactInfo();
		if(paymentGroup != null) {
			billAddress = (ContactInfo) paymentGroup.getBillingAddress();
			if(StringUtils.isNotEmpty(billAddress.getAddress1())){
				shipAddress.setAddress1(billAddress.getAddress1());
			}
			if(StringUtils.isNotEmpty(billAddress.getAddress1())){
				shipAddress.setCity(billAddress.getCity());
			}
			if(StringUtils.isNotEmpty(billAddress.getCountry())){
				shipAddress.setCountry(billAddress.getCountry());
			}
			if(StringUtils.isNotEmpty(billAddress.getFirstName())){
				shipAddress.setFirstName(billAddress.getFirstName());
			}
			if(StringUtils.isNotEmpty(billAddress.getLastName())){
				shipAddress.setLastName(billAddress.getLastName());
			}
			if(StringUtils.isNotEmpty(billAddress.getState())){
				shipAddress.setState(billAddress.getState());
			}
			if(StringUtils.isNotEmpty(billAddress.getPostalCode())){
				shipAddress.setPostalCode(billAddress.getPostalCode());
			}
			if(StringUtils.isNotEmpty(billAddress.getPhoneNumber())){
				shipAddress.setPhoneNumber(billAddress.getPhoneNumber());
			}
			shippingGroup.setShippingAddress(shipAddress);
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUPaypalUtilityManager.shippingGroupForPayPalRequest having multiple shipping address and a registrant Address :: End"+shipAddress);
		}
		return shippingGroup;
	
	}


	/**
	 * This method will populate the payerId in PayPal Payment Group.
	 *
	 * @param pOrder   current order
	 * @param pShippingGroupMapContainer the shipping group map container
	 * @param pGetExpressCheckoutResponse getExpressCheckoutResponse
	 * @throws CommerceException commerceException
	 */
	public void updatePayPalDetails(Order pOrder,ShippingGroupMapContainer pShippingGroupMapContainer, GetExpressCheckoutResponse pGetExpressCheckoutResponse) throws CommerceException{
		if(isLoggingDebug()) {
			vlogDebug("Entering into  the TRUPayPalUtilityManager.updatePayPalDetails()");
		}
		ContactInfo contactInfo =  pGetExpressCheckoutResponse.getBillingAddress(); 
		String payerCountry = pGetExpressCheckoutResponse.getPayerCountry();
		String payerStatus = pGetExpressCheckoutResponse.getPayerStatus();
		String payerId = pGetExpressCheckoutResponse.getPayerId();
		/*String firstName = pGetExpressCheckoutResponse.getFirstName();
		String lastName = pGetExpressCheckoutResponse.getLastName();
		String middleName = pGetExpressCheckoutResponse.getMiddlename();
		String payPalEmail = pGetExpressCheckoutResponse.getEmail();
		String payerCountry = pGetExpressCheckoutResponse.getPayerCountry();
		String payerStatus = pGetExpressCheckoutResponse.getPayerStatus();
		String payerPhNo = pGetExpressCheckoutResponse.getPhoneNum();*/
		if (isLoggingDebug()) {
			vlogDebug("The payer id is" +payerId );
			vlogDebug("The payer firstName is" +contactInfo.getFirstName() );
			vlogDebug("The payer middleName is" +contactInfo.getMiddleName() );
			vlogDebug("The payer lastName is" +contactInfo.getLastName() );
			vlogDebug("The payer email is" +contactInfo.getEmail() );
		}
		if (pOrder == null || StringUtils.isBlank(payerId)) {
			return;
		}
		PaymentGroup paymentGroup = ((TRUOrderManager)getOrderManager()).fetchPayPalPaymentGroupForOrder(pOrder);
		if(paymentGroup instanceof TRUPayPal){
			((TRUPayPal)paymentGroup).setPayerId(payerId);			
			//ContactInfo contactInfo = new ContactInfo();			
			//getOrderManager().getOrderTools().copyAddress(pGetExpressCheckoutResponse.getBillingAddress(), contactInfo);			
			((TRUPayPal)paymentGroup).setBillingAddress(pGetExpressCheckoutResponse.getBillingAddress());
			((TRUPayPal)paymentGroup).setFirstName(contactInfo.getFirstName());
			((TRUPayPal)paymentGroup).setMiddleName(contactInfo.getMiddleName());
			((TRUPayPal)paymentGroup).setLastName(contactInfo.getLastName());
			((TRUPayPal)paymentGroup).setEmail(contactInfo.getEmail());
			((TRUPayPal)paymentGroup).setPayerStatus(payerStatus);
			((TRUPayPal)paymentGroup).setPayerCountry(payerCountry);			
			//((TRUPayPal)paymentGroup).setTransactionTimestamp(pGetExpressCheckoutResponse.getTimeStamp());
			((TRUPayPal)paymentGroup).setPayerPhoneNumber(contactInfo.getPhoneNumber());			
			Address paypalBillingAddress = ((TRUPayPal)paymentGroup).getBillingAddress();			
			ContactInfo orderBillingAddress  = new ContactInfo();
			OrderTools.copyAddress(paypalBillingAddress, orderBillingAddress);
			orderBillingAddress.setPhoneNumber(contactInfo.getPhoneNumber());
			//Adding the billing address to the credit card. 
			((TRUOrderImpl)pOrder).setBillingAddress(orderBillingAddress);
			String billingAddressNickName = contactInfo.getFirstName()+TRUConstants.WHITE_SPACE +contactInfo.getLastName()+TRUCheckoutConstants.STRING_BA;								
			((TRUOrderImpl)pOrder).setBillingAddressNickName(billingAddressNickName);	
			
			ShippingGroupManager shippingGroupManager = ((TRUOrderManager)getOrderManager()).getShippingGroupManager();
			TRUHardgoodShippingGroup billingAddressShippingGroup = (TRUHardgoodShippingGroup)shippingGroupManager.createShippingGroup();
			billingAddressShippingGroup.setShippingAddress(orderBillingAddress);
			billingAddressShippingGroup.setBillingAddress(Boolean.TRUE);
			pShippingGroupMapContainer.addShippingGroup(billingAddressNickName, billingAddressShippingGroup);
		}
		if(pOrder instanceof TRUOrderImpl) {
			//((TRUOrderImpl) pOrder).setEmail(payPalEmail);
			((TRUOrderImpl) pOrder).setPayPalOrder(true);
		}
		((TRUOrderManager)getOrderManager()).updatePayPalOrderDetails(pOrder);
		if (isLoggingDebug()) {
			vlogDebug("Exiting from TRUOrderManager.updatePayPalDetails() menthod");
		}		
	}


	/**
	 * This method will populate the payerId in PayPal Payment Group
	 * 
	 * @param pOrder   current order
	 * @param pPayerId payerId which comes from PayPal site
	 * @return boolean
	 * @throws CommerceException 
	 * 
	 */
	public boolean updatePayPalDetails(Order pOrder, String pPayerId) throws CommerceException{

		if (isLoggingDebug()) {
			vlogDebug("Entering into TRUOrderManager.updatePayPalDetails() menthod");
			vlogDebug("The payer id is" +pPayerId );
		}
		boolean isValidToken = false;
		if (pOrder == null || StringUtils.isBlank(pPayerId)) {
			return isValidToken;
		}
		synchronized (pOrder) {
			PaymentGroup paymentGroup = getOrderManager().fetchPayPalPaymentGroupForOrder(pOrder);
			if(paymentGroup instanceof TRUPayPal){
				((TRUPayPal)paymentGroup).setPayerId(pPayerId);
			}
			//((TRUOrderImpl)pOrder).setPaypalPayerId(pPayerId);
			getOrderManager().updateOrder(pOrder);
			isValidToken = true;
		}
		if (isLoggingDebug()) {
			vlogDebug("Exiting from TRUOrderManager.updatePayPalDetails() menthod");
		}
		return isValidToken;
	}

	/**
	 * This method call SetExpressCheckout API call and returns token 
	 *
	 * @param pPaymentGroupContainerService the payment group container service
	 * @param pLocale the locale
	 * @param pOrder the order
	 * @param pCancelURL the cancel url
	 * @param pReturnURL the return url
	 * @param pProfile the profile
	 * @param pAddOverRide the add over ride
	 * @return the sets the express checkout response
	 * @throws TRUPayPalException the TRU pay pal exception
	 * @throws SystemException the system exception
	 * @throws CommerceException the commerce exception
	 */
	public SetExpressCheckoutResponse payPalSetExpressCheckout(PaymentGroupMapContainer pPaymentGroupContainerService
			, String pLocale,Order pOrder,String pCancelURL,String pReturnURL, RepositoryItem pProfile
			, boolean pAddOverRide) throws TRUPayPalException, SystemException, CommerceException {
		vlogDebug(" START of TRUPaypalUtilityManager. payPalSetExpressCheckout method");		
		SetExpressCheckoutRequest setExpCheckoutRequest= new SetExpressCheckoutRequest();
		if(!enableRadialPayPal()) {
			SetExpressCheckoutResponse setExpCheckoutResponse = new SetExpressCheckoutResponse();
			setExpCheckoutResponse.setResponseCode(TRUPayPalConstants.DISABLED);
			TRURadialError error = new TRURadialError(TRUPayPalConstants.DISABLED, TRUPayPalConstants.EMPTY_STRING,TRUPayPalConstants.EMPTY_STRING);
			setExpCheckoutResponse.setRadialError(error);
			return setExpCheckoutResponse;
		}
		// populating request  parameters for SetExpressCheckout API call
		populateRequestAttributes(pLocale, pOrder, setExpCheckoutRequest, pCancelURL, pReturnURL,pProfile, pPaymentGroupContainerService);
		//setExpCheckoutRequest.setAddOverRide(pAddOverRide);
		//calling SetExpressCheckout API call
		setExpCheckoutRequest.setStanderdPayPal(((TRUOrderImpl)pOrder).isStanderdPayPal());
		setExpCheckoutRequest.setExpressPayPal(((TRUOrderImpl)pOrder).isExpressPayPal());	
		/*if(isEnableNoShippingAddressDisplay()){
			setExpCheckoutRequest.setNoShippingAddressDisplay(String.valueOf(TRUConstants._1));
		}else{
			setExpCheckoutRequest.setNoShippingAddressDisplay(String.valueOf(TRUConstants._0));
		}*/
		SetExpressCheckoutResponse setExpCheckoutResponse = 
				(SetExpressCheckoutResponse)getPayPalAdaptor().setExpressCheckout(setExpCheckoutRequest);
		if (isLoggingDebug()) {
			vlogDebug(" end of the TRUPaypalUtilityManager.payPalSetExpressCheckout() method");
		}		
		return setExpCheckoutResponse;
	}

	/**
	 * This method will check whether the PayPal token is valid one or not
	 *
	 * @param pOrder the order
	 * @param pProfile the profile
	 * @param pPageName the page name
	 * @param pShippingGroupMapContainer the shipping group map container
	 * @param pPaymentGroupContainerService the payment group container service
	 * @return the gets the express checkout response
	 * @throws TRUPayPalException the TRU pay pal exception
	 * @throws SystemException the system exception
	 * @throws InstantiationException the instantiation exception
	 * @throws IllegalAccessException the illegal access exception
	 * @throws ClassNotFoundException the class not found exception
	 * @throws IntrospectionException the introspection exception
	 * @throws CommerceException the commerce exception
	 */
	public GetExpressCheckoutResponse payPalGetExpressCheckout(Order pOrder, RepositoryItem pProfile, String pPageName, 
			ShippingGroupMapContainer pShippingGroupMapContainer, PaymentGroupMapContainer pPaymentGroupContainerService 
					/*Map<String,String>  pValidationMap*/) 
					throws TRUPayPalException, SystemException, InstantiationException, IllegalAccessException, 
					ClassNotFoundException, IntrospectionException, CommerceException {		 
		if(isLoggingDebug()) {
			vlogDebug("Entering into  the TRUPayPalUtilityManager.payPalGetExpressCheckout()");
		}		 
		if(pOrder == null){
			if(isLoggingError()) {
				logError("Order is null @Class:::TRUPaypalUtiliyManger::@method::GetExpressCheckoutResponse()");
			}	
			return null;
		}
		if(pOrder != null && pOrder.getPriceInfo() == null ) {
			if(isLoggingError()) {
				logError("Order Price Info is null @Class:::TRUPaypalUtiliyManger::@method::GetExpressCheckoutResponse()");
			}	
			return null;
		}
		if(pOrder != null && pOrder.getPriceInfo() != null 
				&& StringUtils.isBlank(pOrder.getPriceInfo().getCurrencyCode())) {
			if(isLoggingError()) {
				logError("CurrencyCode is null @Class:::TRUPaypalUtiliyManger::@method::GetExpressCheckoutResponse()");
			}	
			return null;
		}		 
		GetExpressCheckoutResponse getExpCheckoutResponse = null;
		ContactInfo billingAddress = null;
		// fetch PAYPAL payment group
		TRUPayPal payPalPG = getOrderManager().fetchPayPalPaymentGroupForOrder(pOrder);

		if(payPalPG != null && !StringUtils.isBlank(payPalPG.getToken())) {
			if(!enableRadialPayPal()) {
				getExpCheckoutResponse = new GetExpressCheckoutResponse();
				getExpCheckoutResponse.setResponseCode(TRUPayPalConstants.DISABLED);
				TRURadialError error = new TRURadialError(TRUPayPalConstants.DISABLED, TRUPayPalConstants.EMPTY_STRING,TRUPayPalConstants.EMPTY_STRING);
				getExpCheckoutResponse.setRadialError(error);
				return getExpCheckoutResponse;
			}
			GetExpressCheckOutRequest getExpCheckoutRequest= new GetExpressCheckOutRequest();
			getExpCheckoutRequest.setOrderId(pOrder.getId());
			getExpCheckoutRequest.setPageName(((TRUOrderImpl)pOrder).getCurrentTab());
			getExpCheckoutRequest.setTokenId(payPalPG.getToken());
			getExpCheckoutRequest.setCurrencyCode(pOrder.getPriceInfo().getCurrencyCode());
			getExpCheckoutResponse = (GetExpressCheckoutResponse)getPayPalAdaptor().getExpressCheckout(getExpCheckoutRequest);
			//success GetExpress checkout Response
			if(getExpCheckoutResponse != null && getExpCheckoutResponse.isSuccess() 
					&& !StringUtils.isBlank(getExpCheckoutResponse.getPayerId())) {				
				billingAddress = getExpCheckoutResponse.getBillingAddress();	
				//billingAddress = null;
				if(billingAddress == null  || StringUtils.isBlank(billingAddress.getAddress1())){
					if(isLoggingInfo()){
						vlogInfo("ProfileId:{0}|OrderId:{1}|Email:{2}|Token:{3}|API:Paypal.GetExpress|Reason:Billing address is empty or some fields are missed in it's response",
								pOrder.getProfileId(),pOrder.getId(),((TRUOrderImpl)pOrder).getEmail(),payPalPG.getToken());
					}
					((TRUOrderImpl)pOrder).setCurrentTab(TRUCheckoutConstants.PAYMENT_TAB);
					throw new SystemException(TRUErrorKeys.TRU_ERROR_SHIPPING_ADDRESS_MISSING_INFO);
				}
				// updating order based on the PAYPAL response
		//	synchronized (pOrder) {
				ContactInfo shippingAddress = (ContactInfo)getExpCheckoutResponse.getShippingAddress();	
				
				if(shippingAddress == null  || StringUtils.isBlank(shippingAddress.getAddress1())){
					if(isLoggingInfo()){
						vlogInfo("ProfileId:{0}|OrderId:{1}|Email:{2}|Token:{3}|API:Paypal.GetExpress|Reason:Shipping address is empty or some fields are missed in it's response",
								pOrder.getProfileId(),pOrder.getId(),((TRUOrderImpl)pOrder).getEmail(),payPalPG.getToken());
					}
					((TRUOrderImpl)pOrder).setCurrentTab(TRUCheckoutConstants.PAYMENT_TAB);
					throw new SystemException(TRUErrorKeys.TRU_ERROR_SHIPPING_ADDRESS_MISSING_INFO);
				}
				
				if(StringUtils.isBlank(((TRUOrderImpl) pOrder).getEmail())) {
					((TRUOrderImpl) pOrder).setEmail(shippingAddress.getEmail());
				} else if(!pProfile.isTransient()) {
					Object profileEmail = pProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());
					if(profileEmail != null) {
						((TRUOrderImpl)pOrder).setEmail((String)pProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName()));
					} else {
						((TRUOrderImpl) pOrder).setEmail(shippingAddress.getEmail());
					}
				}
				if(TRUPayPalConstants.CART_PAGE_NAME.equals(pPageName)) {
					// Populating shipping address received from payPal site
					getOrderManager().addPayPalShippingAddressToOrder(pOrder, pProfile, shippingAddress, pShippingGroupMapContainer,getExpCheckoutResponse.getShipToName());
					((TRUShippingGroupManager)getOrderManager().getShippingGroupManager()).validateShippingMethodAndPrice(pOrder);
				}
				if(billingAddress != null && pPaymentGroupContainerService != null && StringUtils.isNotBlank(billingAddress.getAddress1()) ) {
					((TRUPaymentGroupContainerService)pPaymentGroupContainerService).setBillingAddress(billingAddress);
					// Populating billing address received from payPal site and payerId
					updatePayPalDetails(pOrder, pShippingGroupMapContainer, getExpCheckoutResponse);
				}
				//getOrderManager().updateOrder(pOrder);
		//	}
			}
			if(isLoggingDebug()) {
				vlogDebug("Exit from the TRUPayPalUtilityManager.payPalGetExpressCheckout()");
			}
		}
		return getExpCheckoutResponse;
	}


	/**
	 * <Description> This method is used to fetch the error code from response
	 * and add the error message to add for exception.
	 *
	 * @param pPalResponse            - PayPalResponse {@link PayPalResponse}
	 * @param pFormHandler            - GenericFormHandler {@link GenericFormHandler}
	 */
	public void addPayPalErrorMessage(IRadialResponse pPalResponse, GenericFormHandler pFormHandler) {
		if (isLoggingDebug()) {
			vlogDebug("START addPayPalErrorMessage method");
		}			
		String payPalErrMessage = TRUErrorKeys.TRU_PAYPAL_GENERIC_ERR_KEY;
		String errorKey = TRUErrorKeys.TRU_PAYPAL_GENERIC_ERR_KEY;
		if(pPalResponse != null && pPalResponse.getRadialError() != null && StringUtils.isNotBlank(pPalResponse.getRadialError().getErrorCode())){
			String radialErrorCode = pPalResponse.getRadialError().getErrorCode();
			Map<String, String> paypalErrorCodesMap = getRadialconfig().getRadialInvalidPaypalErrorCodes();
			if (null != paypalErrorCodesMap && !paypalErrorCodesMap.isEmpty() && paypalErrorCodesMap.containsKey(radialErrorCode)) {
				errorKey = getRadialconfig().getRadialInvalidPaypalErrorCodes().get(radialErrorCode);
			}
		}
		try {
			payPalErrMessage =getErrorHandlerManager().getErrorMessage(errorKey);
		} catch (SystemException e) {
			if(isLoggingError()){
				logError("Error occured while getting error message for "+errorKey, e);
			}
		}
		pFormHandler.addFormException(new DropletException(payPalErrMessage));
		if (isLoggingDebug()) {
			vlogDebug("payPalErrKey {0} ,  payPalErrMessage {1}" , errorKey ,payPalErrMessage);
			vlogDebug("END addPayPalErrorMessage method");
		}
	}

	/**
	 * @return the mErrorHandlerManager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * @param pErrorHandlerManager the pErrorHandlerManager to set
	 */
	public void setErrorHandlerManager(DefaultErrorHandlerManager pErrorHandlerManager) {
		this.mErrorHandlerManager = pErrorHandlerManager;
	}
	
	/**
	 * <Description> This method is used to check whether pay pal token valid or
	 * not.
	 * 
	 * @param pUserSession
	 *            - TRUUserSession
	 * @return true/false
	 */
	public boolean getPayPalToken(TRUUserSession pUserSession) {
		if (isLoggingDebug()) {
			vlogDebug("START getPayPalToken method");
		}
		boolean isTokenValid = false;
		String payPalToken = null;
		String tokenTimeStamp = null;
		String payPalPayerId = null;
		long diff =0;
		// fetch PAYPAL details map from session
		Map<String, String> paypalPaymentDetailsMap = pUserSession.getPaypalPaymentDetailsMap();
		if(paypalPaymentDetailsMap != null && !paypalPaymentDetailsMap.isEmpty()) {
			payPalToken = paypalPaymentDetailsMap.get(TRUPayPalConstants.PAYPAL_TOKEN);
			payPalPayerId = paypalPaymentDetailsMap.get(TRUPayPalConstants.PAYER_ID);
			tokenTimeStamp = paypalPaymentDetailsMap.get(TRUPayPalConstants.PAYPAL_TOKEN_TIMESTAMP);
			if(!StringUtils.isBlank(payPalToken) && !StringUtils.isBlank(tokenTimeStamp) && !StringUtils.isBlank(payPalPayerId)) {
				DateFormat formatter = null ;
				Date tokenDate = null;
				formatter = new SimpleDateFormat(TRUPaypalConstants.ORDER_DATE_FORMAT);
				try {
					tokenDate = (Date)formatter.parse(tokenTimeStamp);
				} catch (ParseException pE) {
					if (isLoggingError()) {
						logError("ParseException in isPaypalTokenAlreadyCaptured " +
								"while parsing string @Class:::TRUPaypalUtiliyManger::@method::getPayPalToken()"+tokenTimeStamp,pE);
					}
				}
				if(tokenDate != null) {
					diff = getOrderManager().getDateDiff(tokenDate, new Date(), TimeUnit.MINUTES);
				}
				if( diff <= Long.valueOf(getTruConfiguration().getPayPalTokenExpiryTime())) {
					if (isLoggingDebug()) {
						vlogDebug("Paypal Token captured has already expired, need to authenticate again.");
					}
					isTokenValid = true;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END getPayPalToken method");
		}
		return isTokenValid;
	}

	/**
	 * @return the mTruConfiguration
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * @param pTruConfiguration the pTruConfiguration to set
	 */
	public void setTruConfiguration(TRUConfiguration pTruConfiguration) {
		this.mTruConfiguration = pTruConfiguration;
	}
	
	/**
	 * This method is used to update the billing address in pay pal PG from
	 * payment group container service.
	 * 
	 * @param pOrder
	 *            - Order Object.
	 * @param pPayerId
	 *            - PayerId
	 * @param pPgContainerBillingAddress
	 *            - Billing Address.
	 * @return - true/false
	 * @throws CommerceException
	 *             - If any {@link CommerceException}
	 */
	public boolean updateBillingAddressInPayPalPG(Order pOrder, String pPayerId, ContactInfo pPgContainerBillingAddress) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("Entering into TRUPaypalUtilityManager.updateBillingAddressInPayPalPG() menthod");
			vlogDebug("The payer id is" +pPayerId );
		}
		boolean isValidToken = false;
		if (pOrder == null || StringUtils.isBlank(pPayerId)) {
			return isValidToken;
		}
		synchronized (pOrder) {
			PaymentGroup paymentGroup = getOrderManager().fetchPayPalPaymentGroupForOrder(pOrder);
			if(paymentGroup instanceof TRUPayPal){
				((TRUPayPal)paymentGroup).setPayerId(pPayerId);
				((TRUPayPal)paymentGroup).setBillingAddress(pPgContainerBillingAddress);
				((TRUOrderImpl) pOrder).setPayPalOrder(true);
			}
			//((TRUOrderImpl)pOrder).setPaypalPayerId(pPayerId);
			getOrderManager().updateOrder(pOrder);
			isValidToken = true;
		}
		if (isLoggingDebug()) {
			vlogDebug("Exiting from TRUPaypalUtilityManager.updateBillingAddressInPayPalPG() menthod");
		}
		return isValidToken;
	}
	
	/**
	 * this method is used to round off the decimal value with two place
	 * @param pNumber the number
	 * @param pRoundingDecimalPlaces the roundingDecimalPlaces
	 * @return BigDecimal Object
	 */
	private double roundTwoPlaceDecimal(double pNumber, int pRoundingDecimalPlaces) {
		if(isLoggingDebug()) {
			vlogDebug("(roundTwoPlaceDecimal): pNumber:{0}",pNumber);
		}
		BigDecimal bd = new BigDecimal(Double.toString(pNumber));
		bd = bd.setScale(pRoundingDecimalPlaces, TRUTaxwareConstant.NUMERIC_FOUR);
		if(isLoggingDebug()) {
			vlogDebug("(roundTwoPlaceDecimal): bid=g decimal:{0}",bd);
		}
		return bd.doubleValue();
	}
	
	/** The Enable No Shipping Address Display. */
	private boolean mEnableNoShippingAddressDisplay;
	
	/**
	 * @return the enableNoShippingAddressDisplay
	 */
	public boolean isEnableNoShippingAddressDisplay() {
		return mEnableNoShippingAddressDisplay;
	}
	
	/**
	 * @param pEnableNoShippingAddressDisplay the enableNoShippingAddressDisplay to set
	 */
	public void setEnableNoShippingAddressDisplay(
			boolean pEnableNoShippingAddressDisplay) {
		this.mEnableNoShippingAddressDisplay = pEnableNoShippingAddressDisplay;
	}
	
	/**
	 * hold boolean property integrationConfig.
	 */
	private TRUIntegrationConfiguration mIntegrationConfig;
	
	/** The m site context mSiteContextManager. */
	private SiteContextManager mSiteContextManager;

	/**
	 * @return the siteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * @param pSiteContextManager the siteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}

	/** The mSosIntegrationConfiguration. */
	private TRUSOSIntegrationConfig mSosIntegrationConfiguration;

	/**
	 * @return the sosIntegrationConfiguration
	 */
	public TRUSOSIntegrationConfig getSosIntegrationConfiguration() {
		return mSosIntegrationConfiguration;
	}

	/**
	 * @param pSosIntegrationConfiguration the sosIntegrationConfiguration to set
	 */
	public void setSosIntegrationConfiguration(
			TRUSOSIntegrationConfig pSosIntegrationConfiguration) {
		mSosIntegrationConfiguration = pSosIntegrationConfiguration;
	}

	/**
	 * @return the integrationConfig
	 */
	public TRUIntegrationConfiguration getIntegrationConfig() {
		return mIntegrationConfig;
	}

	/**
	 * @param pIntegrationConfig the integrationConfig to set
	 */
	public void setIntegrationConfig(TRUIntegrationConfiguration pIntegrationConfig) {
		mIntegrationConfig = pIntegrationConfig;
	}
	
	/**
	 * Enable RadialGiftCard
	 *
	 * @return true, if successful
	 */
	private boolean enableRadialPayPal() {
		if (isLoggingDebug()) {
			vlogDebug("@class:TRUPaypalUtilityManager/enableRadialPayPal method start");
		}
		boolean lEnabled = Boolean.FALSE;
		Site site=null;
		site=SiteContextManager.getCurrentSite();
		if(site != null && site.getId().equalsIgnoreCase(TRUCommerceConstants.SOS)) {
			lEnabled = getSosIntegrationConfiguration().isEnableRadialPayPal();
			if (isLoggingDebug()) {
				vlogDebug("@class:TRUPaypalUtilityManager/enableRadialPayPal is  radial PayPal  service enabled in SOS : {0} ",lEnabled);
			}
		} else{
			lEnabled = getIntegrationConfig().isEnableRadialPayPal();
			if (isLoggingDebug()) {
				vlogDebug("@class:TRUPaypalUtilityManager/enableRadialPayPal is  radial PayPal  service enabled in store : {0} ",lEnabled);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@class:TRUPaypalUtilityManager/enableRadialPayPal method end");
		}
		return lEnabled;
	}
	
	/**
	 * This method will return the shipping group with highest price.
	 * @param pOrder - pShippingGroupsList
	 * @return shippingGroup1
	 */
	public HardgoodShippingGroup getShippingGroupForPayPal(Order pOrder){
		if (isLoggingDebug()) {
			vlogDebug("START : TRUPaypalUtilityManager.getShippingGroupForPayPal()");
		}		
		Map<Double,TRUHardgoodShippingGroup> hgsgMap=new TreeMap<Double,TRUHardgoodShippingGroup>(new DescComparator());
		List<Double> hgsgList = new ArrayList<Double>();		
		Map<Double,TRUChannelHardgoodShippingGroup> chgsgMap=new TreeMap<Double,TRUChannelHardgoodShippingGroup>(new DescComparator());
		List<Double> chhgsgList = new ArrayList<Double>();	
		
		HardgoodShippingGroup shippingGroup = null;
		List<ShippingGroup> shippingGroupList = pOrder.getShippingGroups();
		if(shippingGroupList != null && !shippingGroupList.isEmpty()){
			if(shippingGroupList.size() >= TRUConstants.INTEGER_NUMBER_ONE) {
				for(ShippingGroup shippingGroup1 :shippingGroupList){
					double shippingGroupAmount = 0;
					if(shippingGroup1 instanceof TRUHardgoodShippingGroup){						
						TRUHardgoodShippingGroup hgShippingGroup = (TRUHardgoodShippingGroup) shippingGroup1;
						shippingGroupAmount = getAmountByShippingGroup(hgShippingGroup);						
						if(shippingGroup1 instanceof TRUChannelHardgoodShippingGroup){
							chgsgMap.put(shippingGroupAmount, (TRUChannelHardgoodShippingGroup)shippingGroup1);
							chhgsgList.add(shippingGroupAmount);
						}else{
							hgsgMap.put(shippingGroupAmount, (TRUHardgoodShippingGroup)shippingGroup1);
							hgsgList.add(shippingGroupAmount);
						}
					}
				}
			} 
			if (isLoggingDebug()) {
				vlogDebug("END : TRUPaypalUtilityManager.getShippingGroupForPayPal()");
			}
		}
		if(!chgsgMap.isEmpty() && !chhgsgList.isEmpty()){
			//Sorting foe getting the highest amount value first
			Collections.sort(chhgsgList,new DescComparator());
			for(Double amountKey : chhgsgList){
				shippingGroup = chgsgMap.get(amountKey);
				Address shippingAddress = ((TRUChannelHardgoodShippingGroup)shippingGroup).getShippingAddress() ;
				if(shippingAddress != null && StringUtils.isNotBlank(shippingAddress.getFirstName())){
					if (isLoggingDebug()) {
						vlogDebug("Great value is TRUChannelHardgoodShippingGroup : {0} and Nickname : {1}",
								shippingGroup.getId() ,((TRUChannelHardgoodShippingGroup)shippingGroup).getNickName() );
					}
					break;
				}
			}			
		}else if(!hgsgMap.isEmpty() && !hgsgList.isEmpty()){
			//Sorting foe getting the highest amount value first
			Collections.sort(hgsgList,new DescComparator());		
			for(Double amountKey : hgsgList){
				shippingGroup = hgsgMap.get(amountKey);
				Address shippingAddress = ((TRUHardgoodShippingGroup)shippingGroup).getShippingAddress() ;
				if(shippingAddress != null && StringUtils.isNotBlank(shippingAddress.getFirstName())){
					if (isLoggingDebug()) {
						vlogDebug("Great value shippingGroup is TRUHardgoodShippingGroup: {0} and Nickname : {1}",
								shippingGroup.getId(),((TRUHardgoodShippingGroup)shippingGroup).getNickName());
					}
					break;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End : TRUPaypalUtilityManager.getShippingGroupForPayPal()");
		}	
		return shippingGroup;
	}

	/**
	 * The Class DescComparator.
	 */
	class DescComparator implements Comparator<Double>{
		/**
		 * @param o1 Double
		 * @param o2 Double
		 * @return compare result
		 */
        @Override
        public int compare(Double o1, Double o2) {
            return o2.compareTo(o1);
        }

    }
	
	
	/**
	 * Gets the amount by shipping group.
	 *
	 * @param pHGShippingGroup the hG shipping group
	 * @return the amount by shipping group
	 */
	private double getAmountByShippingGroup(TRUHardgoodShippingGroup pHGShippingGroup) {
		double shippingGroupAmount = pHGShippingGroup.getShippingPrice();
		CommerceItemRelationship commerceItemRel = null;
		for (Object commerceItemRelObj : pHGShippingGroup.getCommerceItemRelationships())
		{
			commerceItemRel = (CommerceItemRelationship)commerceItemRelObj;
			if(!(commerceItemRel instanceof TRUDonationCommerceItem) &&  commerceItemRel.getCommerceItem().getPriceInfo() != null){
				shippingGroupAmount+=commerceItemRel.getCommerceItem().getPriceInfo().getAmount();
			}
		}
		return shippingGroupAmount;
	}

}
