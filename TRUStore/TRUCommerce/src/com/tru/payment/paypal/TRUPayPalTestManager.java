package com.tru.payment.paypal;

import java.util.ArrayList;
import java.util.List;

import atg.core.util.Address;
import atg.nucleus.GenericService;

import com.tru.common.cml.SystemException;
import com.tru.radial.payment.paypal.PayPalAdaptor;
import com.tru.radial.payment.paypal.bean.DoAuthorizationPaymentRequest;
import com.tru.radial.payment.paypal.bean.DoAuthorizationPaymentResponse;
import com.tru.radial.payment.paypal.bean.GetExpressCheckOutRequest;
import com.tru.radial.payment.paypal.bean.GetExpressCheckoutResponse;
import com.tru.radial.payment.paypal.bean.PaymentDetails;
import com.tru.radial.payment.paypal.bean.SetExpressCheckoutRequest;
import com.tru.radial.payment.paypal.bean.SetExpressCheckoutResponse;


/**
 * This is a Test Manager class interact to integration layer for testing PayPal API payment service calls like setExpressCheckout,getExpressCheckoutDetails etc.
 * 
 * @author PA
 *
 */
public class TRUPayPalTestManager extends GenericService{
	
	/**
	 * Property to hold instance of PayPalAdaptor
	 */
	PayPalAdaptor  mPayPalAdaptor;
	/**
	 * Property to hold Informational note about this void that is displayed to the buyer in email
	 */
	private String mNote;
	/**
	 * Property to hold Original authorization ID specifying the authorization the order ID.
	 */
	private String mAuthorizationId;
	/**
	 * Property to hold mTransactionId
	 */
	private String mTransactionId;
	/**
	 * 	
	 * @return mPayPalAdaptor instance of PayPalAdaptor
	 */
	public PayPalAdaptor getPayPalAdaptor() {
		return mPayPalAdaptor;
	}
	
	/**
	 * @param pPayPalAdaptor the PayPalAdaptor to set
	 */
	public void setPayPalAdaptor(PayPalAdaptor pPayPalAdaptor) {
		this.mPayPalAdaptor = pPayPalAdaptor;
	}
	/**
	 * Property to hold mAmount
	 */
	private double mAmount;
	/**
	 * Property to hold mCurrencyCode
	 */
	private String mCurrencyCode;
	/**
	 * Property to hold mPaymentAction
	 */
	private String mPaymentAction;
	/**
	 * Property to hold mReturnURL
	 */
	private String mReturnURL;
	/**
	 * Property to hold mCancelURL
	 */
	private String mCancelURL;
	/**
	 * Property to hold mTokenId
	 */
	private String mTokenId;
	/**
	 * Property to hold mShipToName
	 */
	private String mShipToName;
	/**
	 * Property to hold mShipToStreet
	 */
	private String mShipToStreet;
	/**
	 * Property to hold mShipToStreet2
	 */
	private String mShipToStreet2;
	/**
	 * Property to hold mShipToCity
	 */
	private String mShipToCity;
	/**
	 * Property to hold mShipToState
	 */
	private String mShipToState;
	/**
	 * Property to hold mShipToZip
	 */
	private String mShipToZip;
	/**
	 * Property to hold mShipToCountry
	 */
	private String mShipToCountry;
	/**
	 * Property to hold mShipToPhoneNo
	 */
	private String mShipToPhoneNo;
	/**
	 * Property to hold mIpAddress
	 */
	private String mIpAddress;
	
	/**
	 * @return the authorizationId
	 */
	public String getAuthorizationId() {
		return mAuthorizationId;
	}

	/**
	 * @param pAuthorizationId the authorizationId to set
	 */
	public void setAuthorizationId(String pAuthorizationId) {
		mAuthorizationId = pAuthorizationId;
	}

	/**
	 * @return the Note
	 */
	public String getNote() {
		return mNote;
	}

	/**
	 * @param pNote the Note to set
	 */
	public void setNote(String pNote) {
		mNote = pNote;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return mTransactionId;
	}

	/**
	 * @param pTransactionId the transactionId to set
	 */
	public void setTransactionId(String pTransactionId) {
		mTransactionId = pTransactionId;
	}
	/**
	 * @return the amount
	 */
	public double getAmount() {
		return mAmount;
	}
	/**
	 * @param pAmount the amount to set
	 */
	public void setAmount(double pAmount) {
		mAmount = pAmount;
	}
	/**
	 * @return the shipToName
	 */
	public String getShipToName() {
		return mShipToName;
	}

	/**
	 * @param pShipToName the shipToName to set
	 */
	public void setShipToName(String pShipToName) {
		mShipToName = pShipToName;
	}

	/**
	 * @return the shipToStreet
	 */
	public String getShipToStreet() {
		return mShipToStreet;
	}

	/**
	 * @param pShipToStreet the shipToStreet to set
	 */
	public void setShipToStreet(String pShipToStreet) {
		mShipToStreet = pShipToStreet;
	}

	/**
	 * @return the shipToStreet2
	 */
	public String getShipToStreet2() {
		return mShipToStreet2;
	}

	/**
	 * @param pShipToStreet2 the shipToStreet2 to set
	 */
	public void setShipToStreet2(String pShipToStreet2) {
		mShipToStreet2 = pShipToStreet2;
	}

	/**
	 * @return the shipToCity
	 */
	public String getShipToCity() {
		return mShipToCity;
	}

	/**
	 * @param pShipToCity the shipToCity to set
	 */
	public void setShipToCity(String pShipToCity) {
		mShipToCity = pShipToCity;
	}

	/**
	 * @return the shipToState
	 */
	public String getShipToState() {
		return mShipToState;
	}

	/**
	 * @param pShipToState the shipToState to set
	 */
	public void setShipToState(String pShipToState) {
		mShipToState = pShipToState;
	}

	/**
	 * @return the shipToZip
	 */
	public String getShipToZip() {
		return mShipToZip;
	}

	/**
	 * @param pShipToZip the shipToZip to set
	 */
	public void setShipToZip(String pShipToZip) {
		mShipToZip = pShipToZip;
	}

	/**
	 * @return the shipToCountry
	 */
	public String getShipToCountry() {
		return mShipToCountry;
	}

	/**
	 * @param pShipToCountry the shipToCountry to set
	 */
	public void setShipToCountry(String pShipToCountry) {
		mShipToCountry = pShipToCountry;
	}

	/**
	 * @return the shipToPhoneNo
	 */
	public String getShipToPhoneNo() {
		return mShipToPhoneNo;
	}

	/**
	 * @param pShipToPhoneNo the shipToPhoneNo to set
	 */
	public void setShipToPhoneNo(String pShipToPhoneNo) {
		mShipToPhoneNo = pShipToPhoneNo;
	}

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return mIpAddress;
	}

	/**
	 * @param pIpAddress the ipAddress to set
	 */
	public void setIpAddress(String pIpAddress) {
		mIpAddress = pIpAddress;
	}
	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return mCurrencyCode;
	}
	/**
	 * @param pCurrencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String pCurrencyCode) {
		mCurrencyCode = pCurrencyCode;
	}
	/**
	 * @return the paymentAction
	 */
	public String getPaymentAction() {
		return mPaymentAction;
	}
	/**
	 * @param pPaymentAction the paymentAction to set
	 */
	public void setPaymentAction(String pPaymentAction) {
		mPaymentAction = pPaymentAction;
	}
	/**
	 * @return the cancelURL
	 */
	public String getCancelURL() {
		return mCancelURL;
	}
	/**
	 * @param pCancelURL the cancelURL to set
	 */
	public void setCancelURL(String pCancelURL) {
		mCancelURL = pCancelURL;
	}
	/**
	 * @return the returnURL
	 */
	public String getReturnURL() {
		return mReturnURL;
	}
	/**
	 * @param pReturnURL the ReturnURL to set
	 */
	public void setReturnURL(String pReturnURL) {
		mReturnURL = pReturnURL;
	}
	/**
	 * @return the tokenId
	 */
	public String getTokenId() {
		return mTokenId;
	}
	/**
	 * @param pTokenId the tokenId to set
	 */
	public void setTokenId(String pTokenId) {
		mTokenId = pTokenId;
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
	//Method for Test PayPal Payment SetExprsessCheckout  Call	
	/**
	 * SetExprsessCheckout
	 * @return reposne the setexpresscheckout response
	 */
	public SetExpressCheckoutResponse invokeSetExprsessCheckout(){
		SetExpressCheckoutRequest createTokenRequest = new SetExpressCheckoutRequest();
		PaymentDetails paymentRequest = new PaymentDetails();     
		paymentRequest.setAmt(Double.toString(getAmount()));
		paymentRequest.setCurrencyCode(getCurrencyCode());
		paymentRequest.setPaymentAction(getPaymentAction());
		List<PaymentDetails> paymentRequestList=new ArrayList<PaymentDetails>();
		paymentRequestList.add(paymentRequest);
		createTokenRequest.setPaymentRequestList(paymentRequestList);     
		createTokenRequest.setCancelURL(getCancelURL());
		createTokenRequest.setReturnURL(getReturnURL());
		SetExpressCheckoutResponse response = null;
		try {
			if(isLoggingDebug()){
				vlogDebug("TRUPayPalTestManager.invokeSetExprsessCheckout() "+createTokenRequest);
			}
			response = (SetExpressCheckoutResponse) getPayPalAdaptor().setExpressCheckout(createTokenRequest);
		} catch (SystemException e) {
			if(isLoggingError()){
				logError("SystemException in TRUPayPalTestManager.invokeSetExprsessCheckout()",e);
			}
		}
		if(isLoggingDebug()){
			vlogDebug("TRUPayPalTestManager.invokeSetExprsessCheckout() response::"+response);
		}
        return response;
	}
	//Method for Test PayPal Payment GetExpressCheckoutDetails  Call	
	/**
	 * GetExpressCheckoutDetails
	 */
	public void sampleExprsessCheckout(){
		GetExpressCheckOutRequest getExpressCheckOutRequest = new GetExpressCheckOutRequest();
		PaymentDetails paymentRequest = new PaymentDetails();

		paymentRequest.setAmt(Double.toString(getAmount()));
		paymentRequest.setCurrencyCode(getCurrencyCode());
		paymentRequest.setPaymentAction(getPaymentAction());
		List<PaymentDetails> paymentRequestList=new ArrayList<PaymentDetails>();
		paymentRequestList.add(paymentRequest);
		getExpressCheckOutRequest.setPaymentRequestList(paymentRequestList);
		getExpressCheckOutRequest.setCancelURL(getCancelURL());
		getExpressCheckOutRequest.setReturnURL(getReturnURL());
		getExpressCheckOutRequest.setTokenId(getTokenId());
		GetExpressCheckoutResponse response = null;
		try {
			if(isLoggingDebug()){
				vlogDebug("TRUPayPalTestManager.sampleExprsessCheckout() createTokenRequest:: "+getExpressCheckOutRequest);
			}
			response = (GetExpressCheckoutResponse) getPayPalAdaptor().getExpressCheckout(getExpressCheckOutRequest);
			if(isLoggingDebug()){
				vlogDebug("TRUPayPalTestManager.sampleExprsessCheckout() response "+response);
			}
		} catch (SystemException e) {
			if(isLoggingError()){
				logError("SystemException in TRUPayPalTestManager.sampleExprsessCheckout()",e);
			}
		}


	}
	/**
	 * DoAuthorization
	 */
	//Method for Test PayPal Payment DoAuthorization  Call	
	public void invokeDoAuthorization(){
		DoAuthorizationPaymentRequest paymentRequest = new DoAuthorizationPaymentRequest();
		paymentRequest.setTransactionId(getTransactionId());
		paymentRequest.setAmount(getAmount());
		paymentRequest.setIpAddress(getIpAddress());
		Address shippingAddress=new Address();
		shippingAddress.setCity(getShipToCity());
		shippingAddress.setState(getShipToState());
		shippingAddress.setCountry(getShipToCountry());
		shippingAddress.setPostalCode(getShipToZip());
		shippingAddress.setAddress1(getShipToStreet());
		shippingAddress.setAddress2(getShipToStreet2());
		shippingAddress.setPostalCode(getShipToZip());
		shippingAddress.setFirstName(getShipToName());
		paymentRequest.setShippingAddress(shippingAddress);
		try {
			if(isLoggingDebug()){
				vlogDebug("TRUPayPalTestManager.invokeDoAuthorization() respaymentRequestponse "+paymentRequest);
			}
			DoAuthorizationPaymentResponse doAuthorizationPaymentResponse=(DoAuthorizationPaymentResponse)getPayPalAdaptor().doAuthorizaton(paymentRequest);
			if(isLoggingDebug()){
					vlogDebug("TRUPayPalTestManager.invokeDoAuthorization() response "+doAuthorizationPaymentResponse);
			}
		} catch (SystemException e) {
			if(isLoggingError()){
				logError("SystemException in TRUPayPalTestManager.invokeDoAuthorization()",e);
			}
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
