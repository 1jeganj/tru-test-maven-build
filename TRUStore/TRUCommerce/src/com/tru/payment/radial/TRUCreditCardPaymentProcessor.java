package com.tru.payment.radial;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;

import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.payment.TRUPaymentTools;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardInfo;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardStatus;
import com.tru.logging.TRUIntegrationInfoLogger;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialIntegrationException;
import com.tru.radial.exception.TRURadialResponseBuilderException;
import com.tru.radial.integration.util.TRURadialConstants;
import com.tru.radial.payment.CreditCardAuthReplyType;
import com.tru.radial.payment.CreditCardAuthRequestType;
import com.tru.radial.payment.FaultResponseType;
import com.tru.radial.payment.PLCCAuthReplyType;
import com.tru.radial.payment.PLCCAuthRequestType;
import com.tru.radial.payment.PaymentAccountUniqueIdType;
import com.tru.radial.payment.PaymentContextType;
import com.tru.radial.payment.ValidateCardReplyType;
import com.tru.radial.payment.creditcard.bean.CreditCardAuthorizationRequest;
import com.tru.radial.payment.creditcard.bean.CreditCardAuthorizationResponse;
import com.tru.radial.payment.creditcard.bean.CreditCardValidationRequest;
import com.tru.radial.payment.creditcard.bean.CreditCardValidationResponse;
import com.tru.radial.payment.creditcard.bean.PLCCCardAuthorizationRequest;
import com.tru.radial.payment.creditcard.bean.PLCCCardAuthorizationResponse;
import com.tru.radial.payment.creditcard.bean.TRURadialGetNonceBuilder;
import com.tru.radial.payment.creditcard.bean.TRURadialGetNonceRequest;
import com.tru.radial.payment.creditcard.bean.TRURadialGetNonceResponse;
import com.tru.radial.payment.creditcard.builder.CreditCardAuthorizationBuilder;
import com.tru.radial.payment.creditcard.builder.CreditCardValidationBuilder;
import com.tru.radial.payment.creditcard.builder.PLCCCardAuthorizationBuilder;
import com.tru.radial.service.RadialGatewayResponse;
import com.tru.radial.service.TRURadialBaseProcessor;
import com.tru.radial.validation.TRURadialXSDValidator;

/**
 * The Class CreditCardPaymentProcessor.
 */
public class TRUCreditCardPaymentProcessor extends TRURadialBaseProcessor {
	
	/** The Constant GIFTCARD_REDEEM_END_PONT_NAME. */
	private static final String PLCC_AUTH = "plccAuth";
	
	/** The Constant GIFTCARD_REDEEM_END_PONT_NAME. */
	private static final String CREDIT_CARD_AUTH = "creditcardAuth";
	
	/** The Constant VALIDATE_CARD. */
	private static final String VALIDATE_CARD = "validateCard";
	
	
	private TRUIntegrationInfoLogger mIntegrationInfoLogger;
	
	/**
	 * Holds component for PaymentTools.
	 */
	private TRUPaymentTools mPaymentTools;
	
	/**
	 * @return the mIntegrationInfoLogger
	 */
	public TRUIntegrationInfoLogger getIntegrationInfoLogger() {
		return mIntegrationInfoLogger;
	}

	/**
	 * @param pIntegrationInfoLogger the mIntegrationInfoLogger to set
	 */
	public void setIntegrationInfoLogger(TRUIntegrationInfoLogger pIntegrationInfoLogger) {
		mIntegrationInfoLogger = pIntegrationInfoLogger;
	}
	
	/**
	 * Card authorization.
	 *
	 * @param pCreditCardInfo the credit card info
	 * @param pCreditCardStatus the credit card status
	 * @throws TRURadialIntegrationException the TRU radial integration exception
	 */
	public void cardAuthorization(TRUCreditCardInfo pCreditCardInfo, TRUCreditCardStatus pCreditCardStatus) throws TRURadialIntegrationException{
		if (isLoggingDebug()) {
			vlogDebug("TRUCreditCardPaymentProcessor  (CardAuthorization) : BEGIN");
			vlogDebug("TRUCreditCardInfo :{0} TRUCreditCardStatus :{1}",pCreditCardInfo,pCreditCardStatus);
		}
		if (pCreditCardInfo != null && pCreditCardStatus != null) {

			if (isLoggingDebug()) {
				vlogDebug("pCreditCardInfo " + pCreditCardInfo);
			}
			// Invoking the tender call and capturing the response.
			vlogDebug("CreditCardType " + pCreditCardInfo.getCreditCardType());
			if (pCreditCardInfo.getCreditCardType().equalsIgnoreCase(TRURadialConstants.PLCC)) {
				plccCardAuthorization(pCreditCardInfo, pCreditCardStatus);
			} else {
				creditCardAuthorization(pCreditCardInfo, pCreditCardStatus);
			}

		}
		if (isLoggingDebug()) {
			vlogDebug("TRUCreditCardPaymentProcessor  (CardAuthorization) : End");
		}
	}
	
	/**
	 * Credit card authorization.
	 *
	 * @param pCreditCardInfo the credit card info
	 * @param pCreditCardStatus the credit card status
	 * @throws TRURadialIntegrationException the TRU radial integration exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void creditCardAuthorization(TRUCreditCardInfo pCreditCardInfo, TRUCreditCardStatus pCreditCardStatus) throws TRURadialIntegrationException {
		if (isLoggingDebug()) {
			vlogDebug("TRUCreditCardPaymentProcessor  (creditCardAuthorization) : BEGIN");
			vlogDebug("TRUCreditCardInfo :{0} TRUCreditCardStatus :{1}",pCreditCardInfo,pCreditCardStatus);
		}
		if (pCreditCardInfo != null && pCreditCardStatus != null) {
			if (isLoggingDebug()) {
				vlogDebug("pCreditCardInfo " + pCreditCardInfo);
			}
			// Invoking the tender call and capturing the response.
			vlogDebug("CreditCardType " + pCreditCardInfo.getCreditCardType());
			String responseXML = null;
			CreditCardAuthorizationBuilder authorizationBuilder = new CreditCardAuthorizationBuilder();
			CreditCardAuthorizationRequest authorizationRequest = new CreditCardAuthorizationRequest();
			CreditCardAuthorizationResponse authorizationResponse = new CreditCardAuthorizationResponse();
			long starTime = 0;
			long endTime = 0;
			String pageName = null;
			if (pCreditCardInfo.getOrder() instanceof TRUOrderImpl) { 
				pageName = ((TRUOrderImpl)pCreditCardInfo.getOrder()).getCurrentTab();
			}
			try {
				// populating request bean
				authorizationRequest.setAmount(pCreditCardInfo.getAmount());
				authorizationRequest.setBillingAddress(pCreditCardInfo.getBillingAddress());
				authorizationRequest.setCreditCardNumber(pCreditCardInfo.getCreditCardNumber());
				authorizationRequest.setMerchantRefNumber(pCreditCardInfo.getMerchantRefNumber());
				authorizationRequest.setExpirationMonth(pCreditCardInfo.getExpirationMonth());
				authorizationRequest.setExpirationYear(pCreditCardInfo.getExpirationYear());
				authorizationRequest.setEmail(pCreditCardInfo.getEmail());
				long unId = getPaymentTools().getUidGenerator().getPaymentId(); 
				DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
				String trueClientIpAddress = getPaymentTools().getOrderManager().getTrueClientIpAddress(request);
				authorizationRequest.setUniqueId(unId);
				authorizationRequest.setTrueClientIpAddress(trueClientIpAddress);
				authorizationRequest.setSchemaVersion(getRadialconfig().getSchemaVersion());
				authorizationRequest.setCvvNum(pCreditCardInfo.getCvv());
				setUserShippingDetails(pCreditCardInfo,authorizationRequest);
				authorizationRequest.setRetryCount(pCreditCardInfo.getRetryCount());
				
				if(pCreditCardInfo.getRetryCount() > IRadialConstants.INT_ZERO) {
					String previousResponseCode = pCreditCardInfo.getPreviousResponseCode();
					if(StringUtils.isNotBlank(previousResponseCode)) {
						previousResponseCode = previousResponseCode.toLowerCase();
						if(getRadialconfig().getResponseCodesToCorrectCVVOrAVSError().contains(previousResponseCode)) {
							authorizationRequest.setRequestToCorrectCVVOrAVSError(true);
						} else {
							authorizationRequest.setRequestToCorrectCVVOrAVSError(false);
						}
					} else {
						authorizationRequest.setRequestToCorrectCVVOrAVSError(false);
					}
				} else {
					authorizationRequest.setRequestToCorrectCVVOrAVSError(false);
				}
				
				// Build request
				authorizationBuilder.buildRequest(authorizationRequest);
				//It is needed for OMS, so setting back to pass.
				pCreditCardInfo.setTransactionId(authorizationRequest.getTransactionId());

				//Transforming to XML string
				String requestXML = transformToXML(authorizationBuilder.getRequestObject(), IRadialConstants.PACKAGE_NAME);

				JAXBElement<CreditCardAuthRequestType> creditCarAuthReqType = authorizationBuilder.getRequestObject();
				CreditCardAuthRequestType creditType = creditCarAuthReqType.getValue();
				List<JAXBElement<?>> listOfElements = creditType.getContent();

				for(JAXBElement jaxbElement : listOfElements){
					if(jaxbElement.getDeclaredType().getName().equals(IRadialConstants.PAYMENT_CONTEXT_PACKAGE)){
						PaymentAccountUniqueIdType paymentContAccId =  ((PaymentContextType) jaxbElement.getValue()).getPaymentAccountUniqueId();
						String accountNumber = paymentContAccId.getValue();
						String encrCardNum = getEncryptedCardNumber(accountNumber);
						paymentContAccId.setValue(encrCardNum);
					}

					if(jaxbElement.getName().getLocalPart().toString().equalsIgnoreCase(IRadialConstants.CARD_SEC_CODE)){
						String actualCVV = (String) jaxbElement.getValue();
						String encrCvv = getEncryptedCVVNumber(actualCVV);
						jaxbElement.setValue(encrCvv);
					}
				}

				String requestXML1 = transformToXML(creditCarAuthReqType, IRadialConstants.PACKAGE_NAME);
				requestXML1 = authorizationBuilder.formatXML(requestXML1);
				if(isLoggingDebug()){
					vlogDebug("{0}",requestXML1);
				}

				boolean validateXMLWithXSD = false;
				//validate xml with xsd
				if (isXsdValidationRequired()) {
					validateXMLWithXSD = TRURadialXSDValidator.validateXMLWithXSD(requestXML, getXSDFilenames(CREDIT_CARD_AUTH));
				}	

				getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
						getIntegrationInfoLogger().getInterfaceNameMap().get(CREDIT_CARD_AUTH),pCreditCardInfo.getOrder().getId(), null);
				starTime = Calendar.getInstance().getTimeInMillis();
				RadialGatewayResponse response = executeRequest(requestXML, pCreditCardInfo.getCreditCardType(), getRadialconfig().getMaxReTry());
				endTime = Calendar.getInstance().getTimeInMillis();

				if(null != response){
					responseXML =authorizationBuilder.formatXML(response.getResponseBody());
					getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
							getIntegrationInfoLogger().getInterfaceNameMap().get(CREDIT_CARD_AUTH),pCreditCardInfo.getOrder().getId(), null);
				}
				if(isLoggingDebug()){			
					if(null != response){
						vlogDebug("Is validateXMLWithXSD success ? {0}",validateXMLWithXSD);
						vlogDebug("{0}",responseXML);
					}else{
						vlogDebug("RadialGatewayResponse is null for {0}",CREDIT_CARD_AUTH);
					}			
				}
				//Add request and response to repository
				super.auditReqResToDB(pCreditCardInfo.getMerchantRefNumber(), CREDIT_CARD_AUTH, requestXML1, responseXML);
				if(response == null){	
					//This is for if not response from service,Then treat as timeout and create error code for front end purpose.
					authorizationBuilder.buildTimeOutResponse(authorizationResponse);
					String errorMessage = null;
					if (authorizationResponse != null && authorizationResponse.getRadialError() != null) {
						errorMessage = authorizationResponse.getRadialError().getErrorMessage();
					}
					alertLogger(pCreditCardInfo.getCreditCardType(),String.valueOf(endTime-starTime),authorizationResponse.getResponseCode(),errorMessage, pageName,IRadialConstants.STR_AUTHORISE);
				}else if(response.getStatusCode() != IRadialConstants.HTTP_SUCCESS_STATE  && !response.getResponseBody().isEmpty() ) {
					authorizationBuilder.setFaultResponseType(transformToObject(response.getResponseBody(), FaultResponseType.class));
					authorizationBuilder.buildFaultResponse(authorizationResponse);
					String errorMessage = null;
					String responseCode = null;
					if (authorizationResponse != null && authorizationResponse.getRadialError() != null) {
						errorMessage = authorizationResponse.getRadialError().getErrorMessage();
						responseCode = authorizationResponse.getRadialError().getErrorCode();
					}
					alertLogger(pCreditCardInfo.getCreditCardType(), String.valueOf(endTime-starTime),responseCode,errorMessage, pageName,IRadialConstants.STR_AUTHORISE);
				} else if (!response.getResponseBody().isEmpty() ) {
					authorizationBuilder.setCreditCardAuthReplyType(transformToObject(response.getResponseBody(), CreditCardAuthReplyType.class));
					authorizationBuilder.buildResponse(authorizationResponse);
					if (!IRadialConstants.CREDIT_CARD_AUTH_RESPONSE.equalsIgnoreCase(authorizationResponse.getResponseCode())) {
						String errorMessage = null;
						if (authorizationResponse != null && authorizationResponse.getRadialError() != null) {
							errorMessage = authorizationResponse.getRadialError().getErrorMessage();
						}
						alertLogger(pCreditCardInfo.getCreditCardType(),String.valueOf(endTime-starTime),authorizationResponse.getResponseCode(),errorMessage,pageName,IRadialConstants.STR_AUTHORISE);
					}
					pCreditCardStatus.setAuthResponseCode(authorizationResponse.getAuthResponseCode());
					pCreditCardStatus.setBankAuthCode(authorizationResponse.getBankAuthCode());
					pCreditCardStatus.setCvvCode(authorizationResponse.getCvv2Code());
					pCreditCardStatus.setAvsCode(authorizationResponse.getAvsCode());
				} 
				pCreditCardStatus.setResponseCode(authorizationResponse.getResponseCode());
			} catch (TRURadialIntegrationException exe) {
				endTime = Calendar.getInstance().getTimeInMillis();
				alertLogger(pCreditCardInfo.getCreditCardType(),String.valueOf(endTime-starTime),getExceptionErrorCode(),exe.getErrorMessage(),pageName,IRadialConstants.STR_AUTHORISE);
				throw exe;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUCreditCardPaymentProcessor  (creditCardAuthorization) : End");
		}
	}
	
	
	/**
	 * PLCC card authorization.
	 *
	 * @param pCreditCardInfo the credit card info
	 * @param pCreditCardStatus the credit card status
	 * @throws TRURadialIntegrationException the TRU radial integration exception
	 */
	public void plccCardAuthorization(TRUCreditCardInfo pCreditCardInfo, TRUCreditCardStatus pCreditCardStatus) throws TRURadialIntegrationException{
		if (isLoggingDebug()) {
			vlogDebug("TRUCreditCardPaymentProcessor  (PLCCCardAuthorization) : BEGIN");
			vlogDebug("TRUCreditCardInfo :{0} TRUCreditCardStatus :{1}",pCreditCardInfo,pCreditCardStatus);
		}
		if (pCreditCardInfo != null && pCreditCardStatus != null) {

			if (isLoggingDebug()) {
				vlogDebug("pCreditCardInfo " + pCreditCardInfo);
			}
			// Invoking the tender call and capturing the response.
			vlogDebug("CreditCardType " + pCreditCardInfo.getCreditCardType());
			String responseXML = null;
			PLCCCardAuthorizationBuilder authorizationBuilder = new PLCCCardAuthorizationBuilder();
			PLCCCardAuthorizationRequest authorizationRequest = new PLCCCardAuthorizationRequest();
			PLCCCardAuthorizationResponse authorizationResponse = new PLCCCardAuthorizationResponse();
			long starTime = 0;
			long endTime = 0;
			String pageName = null;
			if (pCreditCardInfo.getOrder() instanceof TRUOrderImpl) { 
				pageName = ((TRUOrderImpl)pCreditCardInfo.getOrder()).getCurrentTab();
			}
			try {
				// populating request bean
				authorizationRequest.setAmount(pCreditCardInfo.getAmount());
				authorizationRequest.setBillingAddress(pCreditCardInfo.getBillingAddress());
				authorizationRequest.setCreditCardNumber(pCreditCardInfo.getCreditCardNumber());
				authorizationRequest.setMerchantRefNumber(pCreditCardInfo.getMerchantRefNumber());
				authorizationRequest.setCvvNumber(pCreditCardInfo.getCvv());
				authorizationRequest.setRetryCount(pCreditCardInfo.getRetryCount());
				//authorizationRequest.setTransactionId(pCreditCardInfo.getTransactionId());
				long unId = getPaymentTools().getUidGenerator().getPaymentId(); 
				authorizationRequest.setUniqueId(unId);
				if(pCreditCardInfo.getRetryCount() > IRadialConstants.INT_ZERO) {
					String previousResponseCode = pCreditCardInfo.getPreviousResponseCode();
					if(StringUtils.isNotBlank(previousResponseCode)) {
						previousResponseCode = previousResponseCode.toLowerCase();
						if(getRadialconfig().getResponseCodesToCorrectCVVOrAVSError().contains(previousResponseCode)) {
							authorizationRequest.setRequestToCorrectCVVOrAVSError(true);
						} else {
							authorizationRequest.setRequestToCorrectCVVOrAVSError(false);
						}
					} else {
						authorizationRequest.setRequestToCorrectCVVOrAVSError(false);
					}
				} else {
					authorizationRequest.setRequestToCorrectCVVOrAVSError(false);
				}
				// Build request
				authorizationBuilder.buildRequest(authorizationRequest);
				//It is needed for OMS, so setting back to pass.
				pCreditCardInfo.setTransactionId(authorizationRequest.getTransactionId());
				//Transforming to XML string
				String requestXML = transformToXML(authorizationBuilder.getRequestObject(), IRadialConstants.PACKAGE_NAME);

				JAXBElement<PLCCAuthRequestType> plccAuthRequestType = authorizationBuilder.getRequestObject();

				PLCCAuthRequestType creditType = plccAuthRequestType.getValue();
				PaymentContextType paymentContextType = creditType.getPaymentContext();
				String cardNum  = paymentContextType.getPaymentAccountUniqueId().getValue();
				//Encrypting the Card Number for logging purpose	
				if(!StringUtils.isBlank(cardNum)){
					cardNum = getEncryptedCardNumber(cardNum);
					paymentContextType.getPaymentAccountUniqueId().setValue(cardNum);
					creditType.setPaymentContext(paymentContextType);
				}
				//Encrypting the CVV for logging purpose				
				String cvvNum = creditType.getCardSecurityCode();
				if(!StringUtils.isBlank(cvvNum)){
					cvvNum = getEncryptedCVVNumber(cvvNum);
					creditType.setCardSecurityCode(cvvNum);
				}
				String requestXML1 = transformToXML(plccAuthRequestType, IRadialConstants.PACKAGE_NAME);
				requestXML1 = authorizationBuilder.formatXML(requestXML1);
				if(isLoggingDebug()){
					vlogDebug("{0}",requestXML1);
				}

				boolean validateXMLWithXSD = false;
				//validate xml with xsd
				if (isXsdValidationRequired()) {
					validateXMLWithXSD = TRURadialXSDValidator.validateXMLWithXSD(requestXML, getXSDFilenames(PLCC_AUTH));
				}	
				starTime = Calendar.getInstance().getTimeInMillis();
				getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
						getIntegrationInfoLogger().getInterfaceNameMap().get(PLCC_AUTH),pCreditCardInfo.getOrder().getId(), null);
				RadialGatewayResponse response = executeRequest(requestXML, pCreditCardInfo.getCreditCardType(), getRadialconfig().getMaxReTry());
				endTime = Calendar.getInstance().getTimeInMillis();
				if(null != response){
					responseXML =authorizationBuilder.formatXML(response.getResponseBody());
					getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
							getIntegrationInfoLogger().getInterfaceNameMap().get(PLCC_AUTH),pCreditCardInfo.getOrder().getId(), null);
				}
				if(isLoggingDebug()){			
					if(null != response){
						vlogDebug("Is validateXMLWithXSD success ? {0}",validateXMLWithXSD);
						vlogDebug("{0}",responseXML);
					}else{
						vlogDebug("RadialGatewayResponse is null for {0}",PLCC_AUTH);
					}			
				}
				//Add request and response to repository
				super.auditReqResToDB(pCreditCardInfo.getMerchantRefNumber(), PLCC_AUTH, requestXML1, responseXML);
				if(response == null){	
					//This is for if not response from service,Then treat as timeout and create error code for front end purpose.
					authorizationBuilder.buildTimeOutResponse(authorizationResponse);
					String errorMessage = null;
					if (authorizationResponse != null && authorizationResponse.getRadialError() != null) {
						errorMessage = authorizationResponse.getRadialError().getErrorMessage();
					}
					alertLogger(pCreditCardInfo.getCreditCardType(), String.valueOf(endTime-starTime),authorizationResponse.getResponseCode(),errorMessage,pageName,IRadialConstants.STR_PLCC_AUTHORISE);
				}else if(response.getStatusCode() != IRadialConstants.HTTP_SUCCESS_STATE  && !response.getResponseBody().isEmpty() ) {
					authorizationBuilder.setFaultResponseType(transformToObject(response.getResponseBody(), FaultResponseType.class));
					authorizationBuilder.buildFaultResponse(authorizationResponse);
					String errorMessage = null;
					String responseCode = null;
					if (authorizationResponse != null && authorizationResponse.getRadialError() != null) {
						errorMessage = authorizationResponse.getRadialError().getErrorMessage();
						responseCode = authorizationResponse.getRadialError().getErrorCode();
					}
					alertLogger(pCreditCardInfo.getCreditCardType(), String.valueOf(endTime-starTime),responseCode,errorMessage,pageName,IRadialConstants.STR_PLCC_AUTHORISE);
				} else if (!response.getResponseBody().isEmpty() ) {
					authorizationBuilder.setPLCCAuthReplyType(transformToObject(response.getResponseBody(), PLCCAuthReplyType.class));
					authorizationBuilder.buildResponse(authorizationResponse);
					if (!IRadialConstants.CREDIT_CARD_AUTH_RESPONSE.equalsIgnoreCase(authorizationResponse.getResponseCode())) {
						String errorMessage = null;
						if (authorizationResponse != null && authorizationResponse.getRadialError() != null) {
							errorMessage = authorizationResponse.getRadialError().getErrorMessage();
						}
						alertLogger(pCreditCardInfo.getCreditCardType(), String.valueOf(endTime-starTime),authorizationResponse.getResponseCode(),errorMessage,pageName,IRadialConstants.STR_PLCC_AUTHORISE);
					}
					pCreditCardStatus.setAuthResponseCode(authorizationResponse.getAuthResponseCode());
					pCreditCardStatus.setBankAuthCode(authorizationResponse.getBankAuthCode());
					pCreditCardStatus.setCvvCode(authorizationResponse.getCvv2Code());
					pCreditCardStatus.setAvsCode(authorizationResponse.getAvsCode());
					pCreditCardStatus.setAvsDescriptiveResult(authorizationResponse.getAvsDescriptiveResult());
				} 
				pCreditCardStatus.setResponseCode(authorizationResponse.getResponseCode());
			} catch (TRURadialIntegrationException exe) {
				endTime = Calendar.getInstance().getTimeInMillis();
				alertLogger(pCreditCardInfo.getCreditCardType(),String.valueOf(endTime-starTime),getExceptionErrorCode(),exe.getErrorMessage(),pageName,IRadialConstants.STR_PLCC_AUTHORISE);
				throw exe;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUCreditCardPaymentProcessor  (PLCCCardAuthorization) : End");
		}
	}
	
	/**
	 * Sets the user shipping details.
	 *
	 * @param pCreditCardInfo the credit card info
	 * @param pCreditCardAuthorizationRequest the credit card authorization request
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void setUserShippingDetails(TRUCreditCardInfo pCreditCardInfo, CreditCardAuthorizationRequest pCreditCardAuthorizationRequest ){
		if (isLoggingDebug()) {
			logDebug("TRUCreditCardPaymentProcessor  (setUserShippingDetails) : START");
		}
		
		String shipToFirstName = TRURadialConstants.EMPTY;
		String shipToLastName = TRURadialConstants.EMPTY;
		String shipToPhone = TRURadialConstants.EMPTY;
		String shipAddress1 = TRURadialConstants.EMPTY;
		String shipAddress2 = TRURadialConstants.EMPTY;
		String shipCity = TRURadialConstants.EMPTY;
		String shipCountry = TRURadialConstants.EMPTY;
		String shipState = TRURadialConstants.EMPTY;
		String shipPostalCode = TRURadialConstants.EMPTY;
		Order order= null;
		order = pCreditCardInfo.getOrder();
		if(order != null){
		String orderType = order.getOrderClassType();
		if(orderType.equals(TRURadialConstants.LAYAWAY_ORDER_TYPE)){
			TRULayawayOrderImpl layAwayOrder = (TRULayawayOrderImpl)order;
			ContactInfo contactInfo = (ContactInfo) pCreditCardInfo.getBillingAddress();
			shipToFirstName = TRURadialConstants.TEST1;
			shipToLastName =  TRURadialConstants.TEST2;
			shipToFirstName = layAwayOrder.getFirstName();
			shipToLastName = layAwayOrder.getLastName();
			shipAddress1 =  pCreditCardInfo.getBillingAddress().getAddress1();
			shipAddress2 = pCreditCardInfo.getBillingAddress().getAddress2();
			shipCity =  pCreditCardInfo.getBillingAddress().getCity();
			shipCountry =  pCreditCardInfo.getBillingAddress().getCountry();
			shipState =  pCreditCardInfo.getBillingAddress().getState();
			shipPostalCode = pCreditCardInfo.getBillingAddress().getPostalCode();
			shipToPhone = contactInfo.getPhoneNumber();
		}
		if(!orderType.equals(TRURadialConstants.LAYAWAY_ORDER_TYPE)){
		List<ShippingGroup> shippingGroups = order.getShippingGroups();
		Iterator it = shippingGroups.iterator();
		int shipGroupCount = TRURadialConstants.INT_ZERO;
		if(it.hasNext() && shipGroupCount < TRURadialConstants.INT_ONE){
			ShippingGroup sg = (ShippingGroup) it.next();
			
			if(sg instanceof HardgoodShippingGroup){
				shipToFirstName = ((HardgoodShippingGroup) sg).getShippingAddress().getFirstName();
				shipToLastName = ((HardgoodShippingGroup) sg).getShippingAddress().getLastName();
				shipAddress1 = ((HardgoodShippingGroup) sg).getShippingAddress().getAddress1();
				shipAddress2 = ((HardgoodShippingGroup) sg).getShippingAddress().getAddress2();
				shipCity = ((HardgoodShippingGroup) sg).getShippingAddress().getCity();
				shipCountry = ((HardgoodShippingGroup) sg).getShippingAddress().getCountry();
				shipState = ((HardgoodShippingGroup) sg).getShippingAddress().getState();
				shipPostalCode = ((HardgoodShippingGroup) sg).getShippingAddress().getPostalCode();
				ContactInfo contactInfo =(ContactInfo) ((HardgoodShippingGroup) sg).getShippingAddress();
				shipToPhone = contactInfo.getPhoneNumber();
				shipGroupCount++;
			}
			
			if(sg instanceof InStorePickupShippingGroup){
				
				if(!StringUtils.isBlank(((TRUInStorePickupShippingGroup) sg).getFirstName())){
					shipToFirstName = ((TRUInStorePickupShippingGroup) sg).getFirstName();
				}else{
					shipToFirstName = ((TRUInStorePickupShippingGroup) sg).getAltLastName();
				 }
				if(!StringUtils.isBlank(((TRUInStorePickupShippingGroup) sg).getLastName())){
					shipToLastName = ((TRUInStorePickupShippingGroup) sg).getLastName();
				}else{
					shipToLastName = ((TRUInStorePickupShippingGroup) sg).getAltLastName();
				}
				if(!StringUtils.isBlank(((TRUInStorePickupShippingGroup) sg).getPhoneNumber())){
					shipToPhone = ((TRUInStorePickupShippingGroup) sg).getPhoneNumber();
				}
				else{
					shipToFirstName = ((TRUInStorePickupShippingGroup) sg).getAltPhoneNumber();
				}
				shipAddress1 =  pCreditCardInfo.getBillingAddress().getAddress1();
				shipAddress2 = pCreditCardInfo.getBillingAddress().getAddress2();
				shipCity =  pCreditCardInfo.getBillingAddress().getCity();
				shipCountry =  pCreditCardInfo.getBillingAddress().getCountry();
				shipState =  pCreditCardInfo.getBillingAddress().getState();
				shipPostalCode = pCreditCardInfo.getBillingAddress().getPostalCode();
				ContactInfo contactInfo = (ContactInfo) pCreditCardInfo.getBillingAddress();
				shipToPhone = contactInfo.getPhoneNumber();
				shipGroupCount++;
			}
		  }
		 }
		}
		
		if(shipToFirstName == null && shipToLastName == null){
			//This change done only for Donation item in cart.
			shipToFirstName = pCreditCardInfo.getBillingAddress().getFirstName();
			shipToLastName = pCreditCardInfo.getBillingAddress().getLastName();
			shipAddress1 = pCreditCardInfo.getBillingAddress().getAddress1();
			shipAddress2 = pCreditCardInfo.getBillingAddress().getAddress2();
			shipCity = pCreditCardInfo.getBillingAddress().getCity();
			shipCountry = pCreditCardInfo.getBillingAddress().getCountry();
			shipState = pCreditCardInfo.getBillingAddress().getState();
			shipPostalCode = pCreditCardInfo.getBillingAddress().getPostalCode();
			shipToPhone = ((ContactInfo)pCreditCardInfo.getBillingAddress()).getPhoneNumber();
		}
		pCreditCardAuthorizationRequest.setShipAddress1(shipAddress1);
		pCreditCardAuthorizationRequest.setShipCity(shipCity);
		pCreditCardAuthorizationRequest.setShipAddress2(shipAddress2);
		pCreditCardAuthorizationRequest.setShipState(shipState);
		pCreditCardAuthorizationRequest.setShipCountry(shipCountry);
		pCreditCardAuthorizationRequest.setShipPostalCode(shipPostalCode);
		pCreditCardAuthorizationRequest.setShipToFirstName(shipToFirstName);
		pCreditCardAuthorizationRequest.setShipToLastName(shipToLastName);
		pCreditCardAuthorizationRequest.setShipToPhone(shipToPhone);
		if (isLoggingDebug()) {
			logDebug("TRUCreditCardPaymentProcessor  (setUserShippingDetails) : END");
		}
	}
	
	/**
	 * This method invokes the creditCardValidation service call.
	 *
	 * @param pCreditCardInfo - TRUCreditCardInfo
	 * @param pCreditCardStatus - TRUCreditCardStatus
	 * @throws TRURadialIntegrationException the TRU radial integration exception
	 */
	public void validateCreditCard(TRUCreditCardInfo pCreditCardInfo, TRUCreditCardStatus pCreditCardStatus) throws TRURadialIntegrationException {
		if (isLoggingDebug()) {
			vlogDebug("TRUCreditCardPaymentProcessor  (validateCreditCard) : BEGIN");
			vlogDebug("TRUCreditCardInfo :{0} TRUCreditCardStatus :{1}", pCreditCardInfo, pCreditCardStatus);
		}
		if (pCreditCardInfo != null && pCreditCardStatus != null) {
			if (isLoggingDebug()) {
				vlogDebug("pCreditCardInfo " + pCreditCardInfo);
			}
			
			String responseXML = null;
			// Invoking the tender call and capturing the response.
			CreditCardValidationBuilder validationBuilder = new CreditCardValidationBuilder();
			CreditCardValidationRequest validationRequest = new CreditCardValidationRequest();
			CreditCardValidationResponse validationResponse = new CreditCardValidationResponse();
			long starTime = 0;
			long endTime = 0;
			String pageName = TRUCommerceConstants.MYACCOUNT;
			// Populating Request
			try {
				validationRequest.setCreditCardNumber(pCreditCardInfo.getCreditCardNumber());
				validationRequest.setCreditCardType(pCreditCardInfo.getCreditCardType());
				validationRequest.setMerchantRefNumber(pCreditCardInfo.getMerchantRefNumber());
				validationRequest.setExpirationYear(pCreditCardInfo.getExpirationYear());
				validationRequest.setExpirationMonth(pCreditCardInfo.getExpirationMonth());
				validationRequest.setCvv(pCreditCardInfo.getCvv());
				validationRequest.setBillingAddress(pCreditCardInfo.getBillingAddress());
				long unId = getPaymentTools().getUidGenerator().getPaymentId(); 
				DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
				String trueClientIpAddress = getPaymentTools().getOrderManager().getTrueClientIpAddress(request);
				validationRequest.setUniqueId(unId);
				validationRequest.setTrueClientIpAddress(trueClientIpAddress);
				//Build the Request
				validationBuilder.buildRequest(validationRequest);
				//Transforming to XML string
				String requestXML = transformToXML(validationBuilder.getRequestObject(), IRadialConstants.PACKAGE_NAME);

				// encrypting credit card number and cvv for logging purpose only for PCI compliance
				validationRequest.setCreditCardNumber(getEncryptedCardNumber(validationRequest.getCreditCardNumber()));
				validationRequest.setCvv(getEncryptedCVVNumber(validationRequest.getCvv()));
				validationBuilder.buildRequest(validationRequest);

				String requestXMLForLogging = transformToXML(validationBuilder.getRequestObject(), IRadialConstants.PACKAGE_NAME);
				requestXMLForLogging = validationBuilder.formatXML(requestXMLForLogging);
				if(isLoggingDebug()){
					vlogDebug("{0}", requestXMLForLogging);
				}
				
				boolean validateXMLWithXSD = false;
				//validate xml with xsd
				if (isXsdValidationRequired()) {
					validateXMLWithXSD = TRURadialXSDValidator.validateXMLWithXSD(requestXML, getXSDFilenames(VALIDATE_CARD));
				}	
				String operation = pCreditCardInfo.getCreditCardType()+IRadialConstants.VALIDATE;
				starTime = Calendar.getInstance().getTimeInMillis();
				String orderId = null;
				if(pCreditCardInfo.getOrder() != null){
					orderId = pCreditCardInfo.getOrder().getId();
				}
				getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
						getIntegrationInfoLogger().getInterfaceNameMap().get(VALIDATE_CARD), orderId, pCreditCardInfo.getMerchantRefNumber());
				RadialGatewayResponse response = executeRequest(requestXML, operation, getRadialconfig().getMaxReTry());
				endTime = Calendar.getInstance().getTimeInMillis();
				if(null != response){
					responseXML =validationBuilder.formatXML(response.getResponseBody());
					getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
							getIntegrationInfoLogger().getInterfaceNameMap().get(VALIDATE_CARD), orderId, pCreditCardInfo.getMerchantRefNumber());
				}
				if(isLoggingDebug()){			
					if(null != response){
						vlogDebug("Is validateXMLWithXSD success ? {0}",validateXMLWithXSD);
						vlogDebug("{0}",responseXML);
					}else{
						vlogDebug("RadialGatewayResponse is null for {0}",VALIDATE_CARD);
					}			
				}
				//Add request and response to repository
				super.auditReqResToDB(pCreditCardInfo.getMerchantRefNumber(), VALIDATE_CARD, requestXMLForLogging, responseXML);
				if(response == null){	
					 //This is for if not response from service,Then treat as timeout and create error code for front end purpose.
					validationBuilder.buildTimeOutResponse(validationResponse);
					 //This is for if not response from service,Then treat as timeout and create error code for front end purpose.
					String errorMessage = null;
					if (validationResponse != null && validationResponse.getRadialError() != null) {
						errorMessage = validationResponse.getRadialError().getErrorMessage();
					}
					alertLogger(pCreditCardInfo.getCreditCardType(), String.valueOf(endTime-starTime),validationResponse.getResponseCode(),errorMessage,pageName,IRadialConstants.VALIDATE);
				}else if(response.getStatusCode() != IRadialConstants.HTTP_SUCCESS_STATE  && !response.getResponseBody().isEmpty() ) {
					validationBuilder.setFaultResponseType(transformToObject(response.getResponseBody(), FaultResponseType.class));
					validationBuilder.buildFaultResponse(validationResponse);
					String errorMessage = null;
					String responseCode = null;
					if (validationResponse != null && validationResponse.getRadialError() != null) {
						errorMessage = validationResponse.getRadialError().getErrorMessage();
						responseCode = validationResponse.getRadialError().getErrorCode();
					}
					alertLogger(pCreditCardInfo.getCreditCardType(), String.valueOf(endTime-starTime),responseCode,errorMessage,pageName,IRadialConstants.VALIDATE);
				} else if (!response.getResponseBody().isEmpty() ) {
					validationBuilder.setValidateCardReplyType(transformToObject(response.getResponseBody(), ValidateCardReplyType.class));
					validationBuilder.buildResponse(validationResponse);
					if (!IRadialConstants.VALIDATE_CC_SUCCESS_CODE.equalsIgnoreCase(validationResponse.getResponseCode())) {
						String errorMessage = null;
						if (validationResponse != null && validationResponse.getRadialError() != null) {
							errorMessage = validationResponse.getRadialError().getErrorMessage();
						}
						alertLogger(pCreditCardInfo.getCreditCardType(), String.valueOf(endTime-starTime),validationResponse.getResponseCode(),errorMessage,pageName,IRadialConstants.VALIDATE);
					}
					pCreditCardStatus.setCreditCardToken(validationResponse.getCreditCardToken());
				} 
				pCreditCardStatus.setResponseCode(validationResponse.getResponseCode());
			} catch (TRURadialIntegrationException exe) {
				endTime = Calendar.getInstance().getTimeInMillis();
				alertLogger(pCreditCardInfo.getCreditCardType(),String.valueOf(endTime-starTime),getExceptionErrorCode(),exe.getErrorMessage(),pageName,IRadialConstants.VALIDATE);
				throw exe;
			}			
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUCreditCardPaymentProcessor  (validateCreditCard) : End");
		}
	}
	
	/**
	 * Gets the radial nonce details.
	 *
	 * @param pRadialResponse IRadialResponse
	 * @param pProfile RepositoryItem
	 * @param pOrder pOrder
	 */
	public void getRadialNonceDetails(IRadialResponse pRadialResponse, Order pOrder, RepositoryItem pProfile) {
		if (isLoggingDebug()) {
			vlogDebug("TRUCreditCardPaymentProcessor  (getRadialNonceDetails) : START");
		}
		TRURadialGetNonceBuilder radialGetNonceBuilder = new TRURadialGetNonceBuilder();
		TRURadialGetNonceResponse radialGetNonceResponse = (TRURadialGetNonceResponse) pRadialResponse;
		TRURadialGetNonceRequest radialGetNonceRequest = (TRURadialGetNonceRequest) constructRadialGetNonceRequest();
		radialGetNonceBuilder.buildRequest(radialGetNonceRequest);
		String  requestXML = radialGetNonceRequest.getRequestString();
		long starTime=0;
		long endTime=0;
		String pageName = null;
		if (pOrder instanceof TRUOrderImpl) { 
			pageName = ((TRUOrderImpl)pOrder).getCurrentTab();
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUCreditCardPaymentProcessor  (getRadialNonceDetails):: Nonce Request{0}",requestXML);
		}
		try {
			starTime = Calendar.getInstance().getTimeInMillis();
			getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), getIntegrationInfoLogger().getInterfaceNameMap().get(IRadialConstants.NONCE_OPRATION), pOrder.getId(), pProfile.getRepositoryId());
			RadialGatewayResponse response = executeRequest(requestXML, IRadialConstants.NONCE_OPRATION, 0);
			endTime = Calendar.getInstance().getTimeInMillis();
			if(response == null){	
				//This is for if not response from service,Then treat as timeout and create error code for front end purpose.
				alertLogger(TRUCheckoutConstants.NONCE, String.valueOf(endTime-starTime),IRadialConstants.RADIAL_TIMEOUT_RESPONSE_CODE,IRadialConstants.RADIAL_TIMEOUT_RESPONSE_MESSAGE,pageName,TRUCheckoutConstants.NONCE);
			}
			if(response !=null){
				radialGetNonceBuilder.setRadialGetNonceResponse(response.getResponseBody());
				getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
						getIntegrationInfoLogger().getInterfaceNameMap().get(IRadialConstants.NONCE_OPRATION), pOrder.getId(), pProfile.getRepositoryId());
				if (isLoggingDebug()) {
					vlogDebug("TRUCreditCardPaymentProcessor  (getRadialNonceDetails):: Nonce Responce{0}",response.getResponseBody());
				}
			}
			radialGetNonceBuilder.buildResponse(radialGetNonceResponse);
		} catch (TRURadialIntegrationException | TRURadialResponseBuilderException e) {
			endTime = Calendar.getInstance().getTimeInMillis();
			try {
				radialGetNonceBuilder.buildResponse(radialGetNonceResponse);
			} catch (TRURadialResponseBuilderException e1) {
				if(isLoggingError()){
					vlogError("TRURadialResponseBuilderException  at TRUCreditCardPaymentProcessor/(getRadialNonceDetails) {0}", e1);
				}
			}
			alertLogger(TRUCheckoutConstants.NONCE, String.valueOf(endTime-starTime),IRadialConstants.RADIAL_TIMEOUT_RESPONSE_CODE,IRadialConstants.RADIAL_TIMEOUT_RESPONSE_MESSAGE,pageName,TRUCheckoutConstants.NONCE);
			if (isLoggingError()) {
				logError("Exception in TRUCreditCardPaymentProcessor::getRadialNonceDetails()", e);
			}
		}
	}
		
	/**
	 * Construct radial get nonce request.
	 *
	 * @return the i radial request
	 */
	private IRadialRequest constructRadialGetNonceRequest() {
		JSONObject user=new JSONObject();
		try {
			Map<String, String> nonceRequestParameters = getRadialconfig().getNonceRequestParameters();
			for (String requestParam : nonceRequestParameters.keySet()) {
				user.put(requestParam, nonceRequestParameters.get(requestParam));
			}
		} catch (JSONException e) {
			if (isLoggingError()) {
				logError("JSONException in @Class::TRURadialPaymentProcessor::@method::getRadialNonceDetails()", e);
			}
		}
		String jsonData = user.toString();
		TRURadialGetNonceRequest radialNonceRequest = new TRURadialGetNonceRequest();
		radialNonceRequest.setRequestString(jsonData);
		if (isLoggingDebug()) {
			vlogDebug("TRURadialPaymentProcessor  (constructRadialGetNonceRequest) : END");
		}
		return radialNonceRequest;
	}

	/**
	 * Gets the payment tools.
	 *
	 * @return the mPaymentTools
	 */
	public TRUPaymentTools getPaymentTools() {
		return mPaymentTools;
	}

	/**
	 * Sets the payment tools.
	 *
	 * @param pPaymentTools the new payment tools
	 */
	public void setPaymentTools(TRUPaymentTools pPaymentTools) {
		mPaymentTools = pPaymentTools;
	}
	
	/**
	 * This method logs the alerts in case of down scenarios.
	 *
	 * @param pOperationName - String
	 * @param pTimeTaken - String
	 * @param pResponceCode the responce code
	 * @param pError the error
	 * @param pPageName the page name
	 * @param pServiceType the service type
	 */
	public void alertLogger(String pOperationName,String pTimeTaken,String pResponceCode,String pError,String pPageName,String pServiceType){
		if (isLoggingDebug()) {
			logDebug("TRUCreditCardPaymentProcessor  (alertLogger) Start");
		}
		super.alertLogger(IRadialConstants.STR_CREDIT_CARD_PROCESSOR, pServiceType, pOperationName, pTimeTaken, pResponceCode, pError,pPageName);
		if (isLoggingDebug()) {
			logDebug("TRUCreditCardPaymentProcessor  (alertLogger) End");
		}
	}

}
