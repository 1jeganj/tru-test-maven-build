package com.tru.payment.radial;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.xml.bind.JAXBElement;

import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardInfo;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardStatus;
import com.tru.common.TRUIntegrationConfiguration;
import com.tru.common.TRUSOSIntegrationConfig;
import com.tru.logging.TRUIntegrationInfoLogger;
import com.tru.radial.commerce.payment.giftcard.GiftCardBalanceRequestBuilder;
import com.tru.radial.commerce.payment.giftcard.GiftCardReedemRequestBuilder;
import com.tru.radial.commerce.payment.giftcard.GiftCardVoidRequestBuilder;
import com.tru.radial.commerce.payment.paypal.util.TRUPayPalConstants;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialIntegrationException;
import com.tru.radial.payment.FaultResponseType;
import com.tru.radial.payment.PaymentAccountUniqueIdType;
import com.tru.radial.payment.StoredValueBalanceReplyType;
import com.tru.radial.payment.StoredValueBalanceRequestType;
import com.tru.radial.payment.StoredValueRedeemReplyType;
import com.tru.radial.payment.StoredValueRedeemRequestType;
import com.tru.radial.payment.StoredValueRedeemVoidReplyType;
import com.tru.radial.payment.StoredValueRedeemVoidRequestType;
import com.tru.radial.payment.giftcard.bean.GiftCardBalanceRequest;
import com.tru.radial.payment.giftcard.bean.GiftCardBalanceResponse;
import com.tru.radial.payment.giftcard.bean.GiftCardReedemRequest;
import com.tru.radial.payment.giftcard.bean.GiftCardReedemResponse;
import com.tru.radial.payment.giftcard.bean.GiftCardVoidRequest;
import com.tru.radial.payment.giftcard.bean.GiftCardVoidResponse;
import com.tru.radial.service.RadialGatewayResponse;
import com.tru.radial.service.TRURadialBaseProcessor;
import com.tru.radial.validation.TRURadialXSDValidator;

/**
 * The Class TRUGiftCardPaymentProcessor.
 * This class holds the business logics for Gift card balance enquiry and gift card redeem services. 
 */
public class TRUGiftCardPaymentProcessor extends TRURadialBaseProcessor {

	/** The Constant GIFTCARD_BALANCE_END_POINT. */
	private static final String GIFTCARD_BALANCE_END_POINT = "giftcardBalance";

	/** The Constant GIFTCARD_REDEEM_END_PONT_NAME. */
	private static final String GIFTCARD_REDEEM_END_PONT_NAME = "giftcardRedeem";

	/** The Constant GIFTCARD_VOID_REDEEM_PONT_NAME. */
	private static final String GIFTCARD_VOID_REDEEM_PONT_NAME  = "giftcardvoidRedeem";
	
	private TRUIntegrationInfoLogger mIntegrationInfoLogger;
	
	/**
	 * @return the mIntegrationInfoLogger
	 */
	public TRUIntegrationInfoLogger getIntegrationInfoLogger() {
		return mIntegrationInfoLogger;
	}

	/**
	 * @param pIntegrationInfoLogger the mIntegrationInfoLogger to set
	 */
	public void setIntegrationInfoLogger(TRUIntegrationInfoLogger pIntegrationInfoLogger) {
		mIntegrationInfoLogger = pIntegrationInfoLogger;
	}
		
	/**
	 * Method invokes the radial gift card balance Inquiry service call.
	 *
	 * @param pGiftCardInfo - TRUGiftCardInfo
	 * @param pGiftCardStatus - TRUGiftCardStatus
	 * @throws TRURadialIntegrationException the tRU radial integration exception
	 */
	public void giftCardBalanceInquiry(TRUGiftCardInfo pGiftCardInfo, TRUGiftCardStatus pGiftCardStatus) throws  TRURadialIntegrationException {
		if (isLoggingDebug()) {
			vlogDebug("TRUGiftCardPaymentProcessor  (giftCardBalanceInquiry) : BEGIN");
			vlogDebug("TRUGiftCardInfo :{0} TRUGiftCardStatus :{1}",pGiftCardInfo,pGiftCardStatus);
		}
		if (pGiftCardInfo != null && pGiftCardStatus != null) {		
			if(!enableRadialGiftCard()) {
				pGiftCardStatus.setResponseCode(TRUPayPalConstants.DISABLED);
				pGiftCardStatus.setBalanceAmount(new BigDecimal(TRUCheckoutConstants.DOUBLE_ZERO));
				vlogDebug("Radial GiftCard Service is Disabled Now");
				return;
			}
			String orderId = pGiftCardInfo.getOrder().getId();
			String emailAddress = null;
			if(pGiftCardInfo.getOrder() instanceof TRUOrderImpl) {
				 emailAddress = ((TRUOrderImpl)pGiftCardInfo.getOrder()).getEmail();
			} else if(pGiftCardInfo.getOrder() instanceof TRULayawayOrderImpl) {
				 emailAddress = ((TRULayawayOrderImpl)pGiftCardInfo.getOrder()).getEmail();
			}
			String apiName = GIFTCARD_BALANCE_END_POINT;
			String responseXML = null;
			IRadialResponse giftCardBalanceResponse = new GiftCardBalanceResponse();
			GiftCardBalanceRequestBuilder balanceRequestBuilder=new GiftCardBalanceRequestBuilder();
			//Create the radial request VO
			GiftCardBalanceRequest balanceRequest = new GiftCardBalanceRequest();
			long starTime = 0;
			long endTime = 0;
			String pageName = null;
			if (pGiftCardInfo.getOrder() instanceof TRUOrderImpl) { 
				pageName = ((TRUOrderImpl)pGiftCardInfo.getOrder()).getCurrentTab();
			}
			try {
				balanceRequest.setGiftCardNumber(pGiftCardInfo.getGiftCardNumber());
				balanceRequest.setPin(pGiftCardInfo.getPin());
				balanceRequestBuilder.buildRequest(balanceRequest);
				//do transform to xml
				String requestXML = transformToXML(balanceRequestBuilder.getRequestObject(), IRadialConstants.PACKAGE_NAME);
				// Added for logging the encrypted card number and pin

				JAXBElement<StoredValueBalanceRequestType> giftCardAuthReqType = balanceRequestBuilder.getRequestObject();
				StoredValueBalanceRequestType giftType = giftCardAuthReqType.getValue();

				PaymentAccountUniqueIdType paymentAccountUniqueIdType = giftType.getPaymentAccountUniqueId();
				String giftCardNumber = paymentAccountUniqueIdType.getValue();
				if(!StringUtils.isBlank(giftCardNumber)){
					giftCardNumber = getEncryptedCardNumber(giftCardNumber);
					paymentAccountUniqueIdType.setValue(giftCardNumber);
					giftType.setPaymentAccountUniqueId(paymentAccountUniqueIdType);
				}
				String giftCardPin = giftType.getPin();
				if(!StringUtils.isBlank(giftCardPin)){
					giftCardPin = getEncryptedCVVNumber(giftCardPin);
					giftType.setPin(giftCardPin);
				}
				String requestXML1 = transformToXML(giftCardAuthReqType, IRadialConstants.PACKAGE_NAME);
				requestXML1 = balanceRequestBuilder.formatXML(requestXML1);
				if(isLoggingDebug()){
					vlogDebug("RadialGiftCardRequest :{0}", requestXML1);
				}
				
				boolean validateXMLWithXSD = false;
				//validate xml with xsd
				if (isXsdValidationRequired()) {
					validateXMLWithXSD = TRURadialXSDValidator.validateXMLWithXSD(requestXML, getXSDFilenames(GIFTCARD_BALANCE_END_POINT));
				}		
				
				starTime = Calendar.getInstance().getTimeInMillis();
				getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
						getIntegrationInfoLogger().getInterfaceNameMap().get(GIFTCARD_BALANCE_END_POINT), pGiftCardInfo.getOrder().getId(), pGiftCardInfo.getProfileId());
				RadialGatewayResponse response = executeRequest(requestXML, GIFTCARD_BALANCE_END_POINT, getRadialconfig().getMaxReTry());
				endTime = Calendar.getInstance().getTimeInMillis();

				if(null != response){
					responseXML =balanceRequestBuilder.formatXML(response.getResponseBody());
					getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
							getIntegrationInfoLogger().getInterfaceNameMap().get(GIFTCARD_BALANCE_END_POINT), pGiftCardInfo.getOrder().getId(), pGiftCardInfo.getProfileId());
				}
				if(isLoggingDebug()){			
					if(null != response){
						vlogDebug("Is validateXMLWithXSD success ? {0}",validateXMLWithXSD);
						vlogDebug("{0}",responseXML);
					}else{
						vlogDebug("RadialGatewayResponse is null for {0}",GIFTCARD_BALANCE_END_POINT);
					}			
				 }
				//Add request and response to repository
				super.auditReqResToDB(orderId, GIFTCARD_BALANCE_END_POINT, requestXML1, responseXML);
				if(response == null){
					((GiftCardBalanceResponse)giftCardBalanceResponse).setSuccess(Boolean.FALSE);
					((GiftCardBalanceResponse)giftCardBalanceResponse).setTimeoutReponse(Boolean.TRUE);
					populateErrorParameters(requestXML1,null,orderId, emailAddress, apiName);
					//Map<String, String> extra = new ConcurrentHashMap<String, String>();
					alertLogger(apiName, String.valueOf(endTime-starTime),IRadialConstants.RADIAL_TIMEOUT_RESPONSE_CODE,IRadialConstants.RADIAL_TIMEOUT_RESPONSE_MESSAGE,pageName,IRadialConstants.STR_GIFTCARD_INQUIRY);
				}else if(response.getStatusCode() != IRadialConstants.HTTP_SUCCESS_STATE  && !response.getResponseBody().isEmpty() ) {
					balanceRequestBuilder.setFaultResponseType(transformToObject(response.getResponseBody(), FaultResponseType.class));
					balanceRequestBuilder.buildFaultResponse(giftCardBalanceResponse);
					populateErrorParameters(requestXML1,response.getResponseBody(),orderId, emailAddress, apiName);
					String responseCode = null;
					String errorMessage = null;
					if (giftCardBalanceResponse != null && giftCardBalanceResponse.getRadialError() != null) {
						responseCode = giftCardBalanceResponse.getRadialError().getErrorCode();
						errorMessage = giftCardBalanceResponse.getRadialError().getErrorMessage();
					}
					alertLogger(apiName, String.valueOf(endTime-starTime),responseCode,errorMessage,pageName,IRadialConstants.STR_GIFTCARD_INQUIRY);
				} else if (!response.getResponseBody().isEmpty() ) {
					balanceRequestBuilder.setGiftCardBalanceResponse(transformToObject(response.getResponseBody(), StoredValueBalanceReplyType.class));
					balanceRequestBuilder.buildResponse(giftCardBalanceResponse);
					if(!giftCardBalanceResponse.getResponseCode().equalsIgnoreCase(IRadialConstants.RADIAL_SUCCESS_RESPONSE_CODE)){
						populateErrorParameters(requestXML1,response.getResponseBody(),orderId, emailAddress, apiName);
						String errorMessage = null;
						if (giftCardBalanceResponse != null && giftCardBalanceResponse.getRadialError() != null) {
							errorMessage = giftCardBalanceResponse.getRadialError().getErrorMessage();
						}
						alertLogger(apiName,String.valueOf(endTime-starTime),giftCardBalanceResponse.getResponseCode(),errorMessage,pageName,IRadialConstants.STR_GIFTCARD_INQUIRY);
					}
				} 
				pGiftCardStatus.setResponseCode(giftCardBalanceResponse.getResponseCode());
				pGiftCardStatus.setBalanceAmount(((GiftCardBalanceResponse)giftCardBalanceResponse).getBalanceAmount());
			} catch (TRURadialIntegrationException exe) {
				endTime = Calendar.getInstance().getTimeInMillis();
				alertLogger(apiName,String.valueOf(endTime-starTime),getExceptionErrorCode(),exe.getErrorMessage(),pageName,IRadialConstants.STR_GIFTCARD_INQUIRY);
				throw exe;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUGiftCardPaymentProcessor  (giftCardBalanceInquiry) : End");
		}		
	}

	
	
	/**
	 * Method invokes the Payeezy's balance Inquiry service call.
	 *
	 * @param pGiftCardInfo - TRUGiftCardInfo
	 * @param pGiftCardStatus - TRUGiftCardStatus
	 * @throws TRURadialIntegrationException the TRU radial integration exception
	 */
	public void giftCardRedeem(TRUGiftCardInfo pGiftCardInfo, TRUGiftCardStatus pGiftCardStatus) throws  TRURadialIntegrationException  {
		if (isLoggingDebug()) {
			vlogDebug("TRUGiftCardPaymentProcessor  (giftCardRedeem) : BEGIN");
			vlogDebug("TRUGiftCardInfo :{0} TRUGiftCardStatus :{1}",pGiftCardInfo,pGiftCardStatus);
		}
		if (pGiftCardInfo != null && pGiftCardStatus != null) {			
			if(!enableRadialGiftCard()) {
				pGiftCardStatus.setResponseCode(TRUPayPalConstants.DISABLED);
				vlogDebug("Radial GiftCard Service is Disabled Now");
				return;
			}
			String orderId = pGiftCardInfo.getOrderId();
			
			String emailAddress = null;
			if(pGiftCardInfo.getOrder() instanceof TRUOrderImpl) {
				 emailAddress = ((TRUOrderImpl)pGiftCardInfo.getOrder()).getEmail();
			} else if(pGiftCardInfo.getOrder() instanceof TRULayawayOrderImpl) {
				 emailAddress = ((TRULayawayOrderImpl)pGiftCardInfo.getOrder()).getEmail();
			}
			
			String apiName = GIFTCARD_REDEEM_END_PONT_NAME;
			String responseXML = null;
			GiftCardReedemResponse giftCardReedemResponse = new GiftCardReedemResponse();
			GiftCardReedemRequestBuilder reedemRequestBuilder = new GiftCardReedemRequestBuilder();
			
			//Create the radial request VO
			GiftCardReedemRequest reedemRequest = new GiftCardReedemRequest();
			long starTime = 0;
			long endTime = 0;
			String pageName = null;
			if (pGiftCardInfo.getOrder() instanceof TRUOrderImpl) { 
				pageName = ((TRUOrderImpl)pGiftCardInfo.getOrder()).getCurrentTab();
			}
			try {
				reedemRequest.setOrderId(pGiftCardInfo.getOrderId());
				reedemRequest.setGiftCardNumber(pGiftCardInfo.getGiftCardNumber());
				reedemRequest.setPin(pGiftCardInfo.getPin());			
				reedemRequest.setTransactionId(pGiftCardInfo.getTransactionId());
				reedemRequest.setAmount(pGiftCardInfo.getAmount());
				reedemRequestBuilder.buildRequest(reedemRequest);
				//Transforming to XML string
				String requestXML = transformToXML(reedemRequestBuilder.getRequestObject(), IRadialConstants.PACKAGE_NAME);
				// Added for logging the encrypted card number and pin
				JAXBElement<StoredValueRedeemRequestType> giftCardAuthReqType = reedemRequestBuilder.getRequestObject();
				StoredValueRedeemRequestType giftType = giftCardAuthReqType.getValue();

				PaymentAccountUniqueIdType paymentAccountUniqueIdType = giftType.getPaymentContext().getPaymentAccountUniqueId();
				String giftCardNumber = paymentAccountUniqueIdType.getValue();
				if(!StringUtils.isBlank(giftCardNumber)){
					giftCardNumber = getEncryptedCardNumber(giftCardNumber);
					paymentAccountUniqueIdType.setValue(giftCardNumber);
					giftType.getPaymentContext().setPaymentAccountUniqueId(paymentAccountUniqueIdType);
				}
				String giftCardPin = giftType.getPin();
				if(!StringUtils.isBlank(giftCardPin)){
					giftCardPin = getEncryptedCVVNumber(giftCardPin);
					giftType.setPin(giftCardPin);
				}
				String requestXML1 = transformToXML(giftCardAuthReqType, IRadialConstants.PACKAGE_NAME);
				requestXML1 = reedemRequestBuilder.formatXML(requestXML1);
				if(isLoggingDebug()){
					vlogDebug("{0}", requestXML1);
				}
				boolean validateXMLWithXSD = false;
				//validate xml with xsd
				if (isXsdValidationRequired()) {
					validateXMLWithXSD = TRURadialXSDValidator.validateXMLWithXSD(requestXML, getXSDFilenames(GIFTCARD_REDEEM_END_PONT_NAME));
				}
				starTime = Calendar.getInstance().getTimeInMillis();
				getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
						getIntegrationInfoLogger().getInterfaceNameMap().get(GIFTCARD_REDEEM_END_PONT_NAME), pGiftCardInfo.getOrder().getId(),pGiftCardInfo.getProfileId());
				RadialGatewayResponse response = executeRequest(requestXML, GIFTCARD_REDEEM_END_PONT_NAME, getRadialconfig().getMaxReTry());
				endTime = Calendar.getInstance().getTimeInMillis();

				if(null != response){
					responseXML = reedemRequestBuilder.formatXML(response.getResponseBody());
					getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
							getIntegrationInfoLogger().getInterfaceNameMap().get(GIFTCARD_REDEEM_END_PONT_NAME), pGiftCardInfo.getOrder().getId(), pGiftCardInfo.getProfileId());
				}
				if(isLoggingDebug()){			
					if(null != response){
						vlogDebug("Is validateXMLWithXSD success ? {0}",validateXMLWithXSD);
						vlogDebug("{0}",responseXML);
					}else{
						vlogDebug("RadialGatewayResponse is null for {0}",GIFTCARD_REDEEM_END_PONT_NAME);
					}			
				}
				//Add request and response to repository
				super.auditReqResToDB(orderId, GIFTCARD_REDEEM_END_PONT_NAME, requestXML1, responseXML);
				if(response == null){
					giftCardReedemResponse.setSuccess(Boolean.FALSE);
					giftCardReedemResponse.setTimeoutReponse(Boolean.TRUE);
					populateErrorParameters(requestXML1,null,orderId, emailAddress, apiName);
					alertLogger(apiName, String.valueOf(endTime-starTime),IRadialConstants.RADIAL_TIMEOUT_RESPONSE_CODE,IRadialConstants.RADIAL_TIMEOUT_RESPONSE_MESSAGE,pageName,GIFTCARD_REDEEM_END_PONT_NAME);
				} else if(response.getStatusCode() != IRadialConstants.HTTP_SUCCESS_STATE  && !response.getResponseBody().isEmpty() ) {
					vlogDebug("Setting The Fault Response (giftCardRedeem)");
					reedemRequestBuilder.setFaultResponseType(transformToObject(response.getResponseBody(), FaultResponseType.class));
					reedemRequestBuilder.buildFaultResponse(giftCardReedemResponse);
					populateErrorParameters(requestXML1,response.getResponseBody(),orderId, emailAddress, apiName);
					String responseCode = null;
					String errorMessage = null;
					if (giftCardReedemResponse != null && giftCardReedemResponse.getRadialError() != null) {
						responseCode = giftCardReedemResponse.getRadialError().getErrorCode();
						errorMessage = giftCardReedemResponse.getRadialError().getErrorMessage();
					}
					alertLogger(apiName, String.valueOf(endTime-starTime),responseCode,errorMessage,pageName,GIFTCARD_REDEEM_END_PONT_NAME);
				} else if (!response.getResponseBody().isEmpty() ) {
					reedemRequestBuilder.setRedeemReplyType(transformToObject(response.getResponseBody(), StoredValueRedeemReplyType.class));
					reedemRequestBuilder.buildResponse(giftCardReedemResponse);
					pGiftCardStatus.setResponseCode(giftCardReedemResponse.getResponseCode());
					pGiftCardStatus.setBalanceAmount(giftCardReedemResponse.getBalanceAmount());
					if(!giftCardReedemResponse.getResponseCode().equalsIgnoreCase(IRadialConstants.RADIAL_SUCCESS_RESPONSE_CODE)){
						populateErrorParameters(requestXML1,response.getResponseBody(),orderId, emailAddress, apiName);
						String errorMessage = null;
						if (giftCardReedemResponse != null && giftCardReedemResponse.getRadialError() != null) {
							errorMessage = giftCardReedemResponse.getRadialError().getErrorMessage();
						}
						alertLogger(apiName,String.valueOf(endTime-starTime),giftCardReedemResponse.getResponseCode(),errorMessage,pageName,GIFTCARD_REDEEM_END_PONT_NAME);
					}
				}
			} catch (TRURadialIntegrationException exe) {
				endTime = Calendar.getInstance().getTimeInMillis();
				alertLogger(apiName,String.valueOf(endTime-starTime),getExceptionErrorCode(),exe.getErrorMessage(),pageName,GIFTCARD_REDEEM_END_PONT_NAME);
				throw exe;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUGiftCardPaymentProcessor  (giftCardRedeem) : End");
		}
	}
	
	/**
	 * Method invokes the radial StoredValueRedeemVoid API call.
	 * @param pGiftCardInfo - TRUGiftCardInfo
	 * @param pGiftCardStatus - TRUGiftCardStatus
	 * @throws TRURadialIntegrationException - PaymentIntegrationException
	 */
	public void giftCardVoidRedeem(TRUGiftCardInfo pGiftCardInfo, TRUGiftCardStatus pGiftCardStatus) throws TRURadialIntegrationException {
		if (isLoggingDebug()) {
			vlogDebug("TRUGiftCardPaymentProcessor  (giftCardVoidRedeem) : BEGIN");
			vlogDebug("TRUGiftCardInfo :{0} TRUGiftCardStatus :{1}",pGiftCardInfo,pGiftCardStatus);
		}
		
		if (pGiftCardInfo != null && pGiftCardStatus != null) {
			if(!enableRadialGiftCard()) {
				pGiftCardStatus.setResponseCode(TRUPayPalConstants.DISABLED);
				vlogDebug("Radial GiftCard Service is Disabled Now");
				return;
			}
			if (isLoggingDebug()) {
				vlogDebug("pGiftCardInfo " + pGiftCardInfo);
			}
			String orderId = pGiftCardInfo.getOrderId();
			String emailAddress = null;
			if(pGiftCardInfo.getOrder() instanceof TRUOrderImpl) {
				 emailAddress = ((TRUOrderImpl)pGiftCardInfo.getOrder()).getEmail();
			} else if(pGiftCardInfo.getOrder() instanceof TRULayawayOrderImpl) {
				 emailAddress = ((TRULayawayOrderImpl)pGiftCardInfo.getOrder()).getEmail();
			}
			
			String apiName = GIFTCARD_VOID_REDEEM_PONT_NAME;
			String responseXML = null;
			GiftCardVoidRequest giftCardVoidRequest = new GiftCardVoidRequest();
			GiftCardVoidResponse giftCardVoidResponse = new GiftCardVoidResponse();
			GiftCardVoidRequestBuilder requestBuilder = new GiftCardVoidRequestBuilder();
			long starTime = 0;
			long endTime = 0;
			String pageName = null;
			if (pGiftCardInfo.getOrder() instanceof TRUOrderImpl) { 
				pageName = ((TRUOrderImpl)pGiftCardInfo.getOrder()).getCurrentTab();
			}
			try {
				// populating the Request
				giftCardVoidRequest.setAmount(pGiftCardInfo.getAmount());
				giftCardVoidRequest.setGiftCardNumber(pGiftCardInfo.getGiftCardNumber());
				giftCardVoidRequest.setOrderId(pGiftCardInfo.getOrderId());
				giftCardVoidRequest.setPin(pGiftCardInfo.getPin());
				giftCardVoidRequest.setTransactionId(pGiftCardInfo.getTransactionId());
				// build the request
				requestBuilder.buildRequest(giftCardVoidRequest);
				// Generating xml string
				String requestXML = transformToXML(requestBuilder.getRequestObject(), IRadialConstants.PACKAGE_NAME);
				// Added for logging the encrypted card number and pin


				JAXBElement<StoredValueRedeemVoidRequestType> giftCardAuthReqType = requestBuilder.getRequestObject();
				StoredValueRedeemVoidRequestType giftType = giftCardAuthReqType.getValue();
				PaymentAccountUniqueIdType paymentAccountUniqueIdType = giftType.getPaymentContext().getPaymentAccountUniqueId();
				String giftCardNumber = paymentAccountUniqueIdType.getValue();
				if(!StringUtils.isBlank(giftCardNumber)){
					giftCardNumber = getEncryptedCardNumber(giftCardNumber);
					paymentAccountUniqueIdType.setValue(giftCardNumber);
					giftType.getPaymentContext().setPaymentAccountUniqueId(paymentAccountUniqueIdType);
				}
				String giftCardPin = giftType.getPin();
				if(!StringUtils.isBlank(giftCardPin)){
					giftCardPin = getEncryptedCVVNumber(giftCardPin);
					giftType.setPin(giftCardPin);
				}
				String requestXML1 = transformToXML(giftCardAuthReqType, IRadialConstants.PACKAGE_NAME);
				requestXML1 = requestBuilder.formatXML(requestXML1);
				if(isLoggingDebug()) {
					vlogDebug("{0}", requestXML1);
				}

				boolean validateXMLWithXSD = false;
				//validate xml with xsd
				if (isXsdValidationRequired()) {
					validateXMLWithXSD = TRURadialXSDValidator.validateXMLWithXSD(requestXML, getXSDFilenames(GIFTCARD_VOID_REDEEM_PONT_NAME));
				}
				starTime = Calendar.getInstance().getTimeInMillis();
				getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
						getIntegrationInfoLogger().getInterfaceNameMap().get(GIFTCARD_VOID_REDEEM_PONT_NAME), pGiftCardInfo.getOrder().getId(),pGiftCardInfo.getProfileId());
				RadialGatewayResponse response = executeRequest(requestXML, GIFTCARD_VOID_REDEEM_PONT_NAME, getRadialconfig().getMaxReTry());
				endTime = Calendar.getInstance().getTimeInMillis();

				if(null != response){
					responseXML = requestBuilder.formatXML(response.getResponseBody());
					getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
							getIntegrationInfoLogger().getInterfaceNameMap().get(GIFTCARD_VOID_REDEEM_PONT_NAME), pGiftCardInfo.getOrder().getId(), pGiftCardInfo.getProfileId());
				}
				// Biuld response xml
				if(isLoggingDebug()){			
					if(null != response){
						vlogDebug("Is validateXMLWithXSD success ? {0}",validateXMLWithXSD);
						vlogDebug("{0}",responseXML);
					}else{
						vlogDebug("RadialGatewayResponse is null for {0}",GIFTCARD_VOID_REDEEM_PONT_NAME);
					}			
				}
				//Add request and response to repository
				super.auditReqResToDB(orderId, GIFTCARD_VOID_REDEEM_PONT_NAME, requestXML1, responseXML);
				if (null == response) {
					giftCardVoidResponse.setSuccess(Boolean.FALSE);
					giftCardVoidResponse.setTimeoutReponse(Boolean.FALSE);
					populateErrorParameters(requestXML1,null,orderId, emailAddress, apiName);
					alertLogger(apiName, String.valueOf(endTime-starTime),IRadialConstants.RADIAL_TIMEOUT_RESPONSE_CODE,IRadialConstants.RADIAL_TIMEOUT_RESPONSE_MESSAGE,pageName,GIFTCARD_VOID_REDEEM_PONT_NAME);
				} else if(response.getStatusCode() != IRadialConstants.HTTP_SUCCESS_STATE  && !response.getResponseBody().isEmpty() ) {
					vlogDebug("Setting The Fault Response (giftCardVoidRedeem)");
					requestBuilder.setFaultResponseType(transformToObject(response.getResponseBody(), FaultResponseType.class));
					requestBuilder.buildFaultResponse(giftCardVoidResponse);
					populateErrorParameters(requestXML1,response.getResponseBody(),orderId, emailAddress, apiName);
					String responseCode = null;
					String errorMessage = null;
					if (giftCardVoidResponse != null && giftCardVoidResponse.getRadialError() != null) {
						responseCode = giftCardVoidResponse.getRadialError().getErrorCode();
						errorMessage = giftCardVoidResponse.getRadialError().getErrorMessage();
					}
					alertLogger(apiName, String.valueOf(endTime-starTime),responseCode,errorMessage,pageName,GIFTCARD_VOID_REDEEM_PONT_NAME);
				} else if (!response.getResponseBody().isEmpty() ) {
					requestBuilder.setStoredValueRedeemVoidReplyType(transformToObject(response.getResponseBody(), StoredValueRedeemVoidReplyType.class));
					requestBuilder.buildResponse(giftCardVoidResponse);
					if(!giftCardVoidResponse.getResponseCode().equalsIgnoreCase(IRadialConstants.RADIAL_SUCCESS_RESPONSE_CODE)){
						populateErrorParameters(requestXML1,response.getResponseBody(),orderId, emailAddress, apiName);
						String errorMessage = null;
						if (giftCardVoidResponse != null && giftCardVoidResponse.getRadialError() != null) {
							errorMessage = giftCardVoidResponse.getRadialError().getErrorMessage();
						}
						alertLogger(apiName,String.valueOf(endTime-starTime),giftCardVoidResponse.getResponseCode(),errorMessage,pageName,GIFTCARD_VOID_REDEEM_PONT_NAME);
					}
				}
				pGiftCardStatus.setResponseCode(giftCardVoidResponse.getResponseCode());
			} catch (TRURadialIntegrationException exe) {
				endTime = Calendar.getInstance().getTimeInMillis();
				alertLogger(apiName,String.valueOf(endTime-starTime),getExceptionErrorCode(),exe.getErrorMessage(),pageName,GIFTCARD_VOID_REDEEM_PONT_NAME);
				throw exe;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUGiftCardPaymentProcessor  (giftCardVoidRedeem) : End");
		}
	}
	
	/**
	 * This method logs the alerts in case of down scenarios.
	 *
	 * @param pOperationName - String
	 * @param pTimeTaken - String
	 * @param pResponseCode the response code
	 * @param pError the error
	 * @param pPageName the page name
	 * @param pServiceType the service type
	 */
	public void alertLogger(String pOperationName,String pTimeTaken,String pResponseCode,String pError,String pPageName,String pServiceType){
		if (isLoggingDebug()) {
			logDebug("TRUGiftCardPaymentProcessor  (alertLogger) Start");
		}
		super.alertLogger(IRadialConstants.STR_GIFTCARD_PROCESSOR, pServiceType, pOperationName, pTimeTaken, pResponseCode, pError, pPageName);
		if (isLoggingDebug()) {
			logDebug("TRUGiftCardPaymentProcessor  (alertLogger) End");
		}
	}
	
	/**
	 * hold boolean property integrationConfig.
	 */
	private TRUIntegrationConfiguration mIntegrationConfig;
	
	/** The m site context mSiteContextManager. */
	private SiteContextManager mSiteContextManager;

	/**
	 * @return the siteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * @param pSiteContextManager the siteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}

	/** The mSosIntegrationConfiguration. */
	private TRUSOSIntegrationConfig mSosIntegrationConfiguration;

	/**
	 * @return the sosIntegrationConfiguration
	 */
	public TRUSOSIntegrationConfig getSosIntegrationConfiguration() {
		return mSosIntegrationConfiguration;
	}

	/**
	 * @param pSosIntegrationConfiguration the sosIntegrationConfiguration to set
	 */
	public void setSosIntegrationConfiguration(
			TRUSOSIntegrationConfig pSosIntegrationConfiguration) {
		mSosIntegrationConfiguration = pSosIntegrationConfiguration;
	}

	/**
	 * @return the integrationConfig
	 */
	public TRUIntegrationConfiguration getIntegrationConfig() {
		return mIntegrationConfig;
	}

	/**
	 * @param pIntegrationConfig the integrationConfig to set
	 */
	public void setIntegrationConfig(TRUIntegrationConfiguration pIntegrationConfig) {
		mIntegrationConfig = pIntegrationConfig;
	}
	
	/**
	 * Enable RadialGiftCard
	 *
	 * @return true, if successful
	 */
	private boolean enableRadialGiftCard() {
		if (isLoggingDebug()) {
			vlogDebug("@class:TRUGiftCardPaymentProcessor/enableRadialGiftCard method start");
		}
		boolean lEnabled = Boolean.FALSE;
		Site site=null;
		site=SiteContextManager.getCurrentSite();
		if(site != null && site.getId().equalsIgnoreCase(TRUCommerceConstants.SOS)) {
			lEnabled = getSosIntegrationConfiguration().isEnableRadialGiftCard();
			if (isLoggingDebug()) {
				vlogDebug("@class:TRUGiftCardPaymentProcessor/enableRadialGiftCard is  radial GiftCard  service enabled in SOS : {0} ",lEnabled);
			}
		} else{
			lEnabled = getIntegrationConfig().isEnableRadialGiftCard();
			if (isLoggingDebug()) {
				vlogDebug("@class:TRUGiftCardPaymentProcessor/enableRadialGiftCard is  radial GiftCard  service enabled in store : {0} ",lEnabled);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@class:TRUGiftCardPaymentProcessor/enableRadialGiftCard method end");
		}
		return lEnabled;
	}
}
