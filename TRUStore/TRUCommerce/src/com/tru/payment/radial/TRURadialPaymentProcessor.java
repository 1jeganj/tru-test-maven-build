package com.tru.payment.radial;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.core.util.StringUtils;

import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUCommerceItemImpl;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.payment.TRUPaymentTools;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardInfo;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardStatus;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialIntegrationException;
import com.tru.radial.integration.exception.TRUPaymentIntegrationException;
import com.tru.radial.integration.inventory.bean.RadialInventoryRequest;
import com.tru.radial.integration.inventory.bean.RadialInventoryResponse;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.radial.payment.FaultResponseType;
import com.tru.radial.payment.PaymentContextType;
import com.tru.radial.payment.TermsAndConditionsReplyType;
import com.tru.radial.payment.TermsAndConditionsRequestType;
import com.tru.radial.payment.creditcard.builder.PromoFinanceBuilder;
import com.tru.radial.payment.promo.bean.PromoFinanceRequest;
import com.tru.radial.payment.promo.bean.PromoFinanceResponse;
import com.tru.radial.service.RadialGatewayResponse;
import com.tru.radial.service.TRURadialBaseProcessor;


/**
 * This class holds all the business logics related to payment
 * 
 * @author PA
 * @version 1.0
 *
 */
public class TRURadialPaymentProcessor extends TRURadialBaseProcessor {
	
	/**
	 * Holds component for PaymentTools.
	 */
	private TRUPaymentTools mPaymentTools;
	/**
	
	/** Property holds the mApplePayTransactionId */
	private String mApplePayTransactionId;
	
	/** Property holds the mEphemeralPublicKey */
	private String mEphemeralPublicKey;
	
	/** Property holds the mPublicKeyHash */
	private String mPublicKeyHash;
	
	/** Property holds the mVersion */
	private String mVersion;

	/** Property holds the mData */
	private String mData;
	
	/** Property holds the mSignature */
	private String mSignature;
	
	/**
	 * @return the applePayTransactionId
	 */
	public String getApplePayTransactionId() {
		return mApplePayTransactionId;
	}

	/**
	 * @param pApplePayTransactionId the applePayTransactionId to set
	 */
	public void setApplePayTransactionId(String pApplePayTransactionId) {
		mApplePayTransactionId = pApplePayTransactionId;
	}

	/**
	 * @return the ephemeralPublicKey
	 */
	public String getEphemeralPublicKey() {
		return mEphemeralPublicKey;
	}

	/**
	 * @param pEphemeralPublicKey the ephemeralPublicKey to set
	 */
	public void setEphemeralPublicKey(String pEphemeralPublicKey) {
		mEphemeralPublicKey = pEphemeralPublicKey;
	}

	/**
	 * @return the publicKeyHash
	 */
	public String getPublicKeyHash() {
		return mPublicKeyHash;
	}

	/**
	 * @param pPublicKeyHash the publicKeyHash to set
	 */
	public void setPublicKeyHash(String pPublicKeyHash) {
		mPublicKeyHash = pPublicKeyHash;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return mVersion;
	}

	/**
	 * @param pVersion the version to set
	 */
	public void setVersion(String pVersion) {
		mVersion = pVersion;
	}

	/**
	 * @return the data
	 */
	public String getData() {
		return mData;
	}

	/**
	 * @param pData the data to set
	 */
	public void setData(String pData) {
		mData = pData;
	}

	/**
	 * @return the signature
	 */
	public String getSignature() {
		return mSignature;
	}

	/**
	 * @param pSignature the signature to set
	 */
	public void setSignature(String pSignature) {
		mSignature = pSignature;
	}

	/**
	 * @return the paymentTools
	 */
	public TRUPaymentTools getPaymentTools() {
		return mPaymentTools;
	}

	/**
	 * @param pPaymentTools the paymentTools to set
	 */
	public void setPaymentTools(TRUPaymentTools pPaymentTools) {
		mPaymentTools = pPaymentTools;
	}


	/**
	 * Construct radial soft allocations request.
	 *
	 * @param pOrder the order
	 * @return the radial inventory request
	 */
	@SuppressWarnings(TRUTaxwareConstant.SUPPRESS_WARNING_UNCHECKED)
	public RadialInventoryRequest constructRadialSoftAllocationsRequest(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::constructRadialSoftAllocationsRequest() : BEGIN ");
		}
		Map<String, Long> allocationsItems = new HashMap<String, Long>();
		final List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		if (shippingGroups == null || shippingGroups.isEmpty()) {
			return null;
		}
		for (ShippingGroup shippingGroup : shippingGroups) {
			if (shippingGroup instanceof TRUHardgoodShippingGroup || shippingGroup instanceof TRUChannelHardgoodShippingGroup) {
				final List<CommerceItemRelationship> ciRels = shippingGroup.getCommerceItemRelationships();
				for (CommerceItemRelationship commerceItemRelationship : ciRels) {
					final TRUShippingGroupCommerceItemRelationship truSgCiR = (TRUShippingGroupCommerceItemRelationship)commerceItemRelationship;
					if(truSgCiR.getCommerceItem() instanceof TRUGiftWrapCommerceItem || truSgCiR.getCommerceItem() instanceof TRUDonationCommerceItem) {
						continue;
					}
					String catalogRefId = truSgCiR.getCommerceItem().getCatalogRefId();
					if(null == allocationsItems.get(catalogRefId)) {
						allocationsItems.put(catalogRefId, truSgCiR.getQuantity());
					} else {
						allocationsItems.put(catalogRefId, allocationsItems.get(catalogRefId)+truSgCiR.getQuantity());
					}
				}
			}
		}
		RadialInventoryRequest radialInventoryRequest = new RadialInventoryRequest();
		radialInventoryRequest.setOrderId(pOrder.getId());
		radialInventoryRequest.setAllocationRequest(allocationsItems);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::constructRadialSoftAllocationsRequest() : END ");
		}
		return radialInventoryRequest;
	}
	


	/**
	 * Update radial soft allocations response.
	 *
	 * @param pOrder the order
	 * @param pRadialSoftAllocations the radial soft allocations
	 */
	@SuppressWarnings(TRUTaxwareConstant.SUPPRESS_WARNING_UNCHECKED)
	public void updateRadialSoftAllocationsResponse(Order pOrder, IRadialResponse pRadialSoftAllocations) {
		RadialInventoryResponse lRadialInventoryResponse = (RadialInventoryResponse) pRadialSoftAllocations;
		if(lRadialInventoryResponse.isTransactionSuccess()) {
			TRUOrderImpl lOrder = (TRUOrderImpl) pOrder;
			lOrder.setReservationId(lRadialInventoryResponse.getReservationId().toString());
			Map<String, BigInteger> allocationResponse = lRadialInventoryResponse.getAllocationResponse();
			for (String catalogRefId : allocationResponse.keySet()) {
				List<CommerceItem> commerceItemsByCatalogRefId;
				try {
					if(isLoggingDebug()) {
						vlogDebug("updateRadialSoftAllocationsResponse() calling getCommerceItemsByCatalogRefId() for :: skuId:{0}", catalogRefId);
					}
					commerceItemsByCatalogRefId = pOrder.getCommerceItemsByCatalogRefId(catalogRefId);
					if(isLoggingDebug()){
						vlogDebug("updateRadialSoftAllocationsResponse():: skuId:{0} commerceItemsByCatalogRefId : {1}", catalogRefId, commerceItemsByCatalogRefId);
					}
					if(commerceItemsByCatalogRefId == null || commerceItemsByCatalogRefId.isEmpty()){
						continue;
					}
					for(CommerceItem cItem : commerceItemsByCatalogRefId) {
						TRUCommerceItemImpl lTRUCommerceItemImpl = (TRUCommerceItemImpl) cItem;
						if(allocationResponse.get(catalogRefId) != null) {
							lTRUCommerceItemImpl.setSoftAmountAllocated(allocationResponse.get(catalogRefId).longValue());
						}
						if(lRadialInventoryResponse.getReservationId() != null) {
							lTRUCommerceItemImpl.setReservationId(lRadialInventoryResponse.getReservationId().toString());
						}
					}
				} catch (CommerceItemNotFoundException ce) {
					vlogError("CommerceItemNotFoundException in TRURadialPaymentProcessor.updateRadialSoftAllocationsResponse():: {0}", ce);
				} catch (InvalidParameterException ie) {
					vlogError("InvalidParameterException in TRURadialPaymentProcessor.updateRadialSoftAllocationsResponse():: {0}", ie);
				}
			}
		}
	}
	
	/**
	 * Method invokes the promoFinanceAuthorization service call.
	 * @param pCreditCardInfo - TRUCreditCardInfo
	 * @param pCreditCardStatus - TRUCreditCardStatus
	 * @throws TRUPaymentIntegrationException - PaymentIntegrationException
	 * @throws
	 */
	public void promoFinanceAuthorization(TRUCreditCardInfo pCreditCardInfo, TRUCreditCardStatus pCreditCardStatus) throws TRUPaymentIntegrationException{
		if (isLoggingDebug()) {
			vlogDebug("TRURadialPaymentProcessor  (promoFinanceAuthorization) : BEGIN");
			vlogDebug("TRUCreditCardInfo :{0} TRUCreditCardStatus :{1}",pCreditCardInfo,pCreditCardStatus);
		}
		if (pCreditCardInfo != null && pCreditCardStatus != null) {
			if (isLoggingDebug()) {
				vlogDebug("pCreditCardInfo " + pCreditCardInfo);
			}
			String responseXML = null;
			PromoFinanceRequest promoFinanceRequest = new PromoFinanceRequest();
			PromoFinanceResponse promoFinanceResponse = new PromoFinanceResponse();
			PromoFinanceBuilder promoFinanceBuilder = new PromoFinanceBuilder();
			RadialGatewayResponse response = null;
			//Setting the request
			BigDecimal financeCharge = new BigDecimal(getRadialconfig().getFinancingCharge());
			BigDecimal promoCharge = financeCharge.setScale(BigDecimal.ROUND_CEILING, BigDecimal.ROUND_CEILING);
			promoFinanceRequest.setAmount(promoCharge.doubleValue());
			promoFinanceRequest.setBillingAddress(pCreditCardInfo.getBillingAddress());
			promoFinanceRequest.setCreditCardNumber(pCreditCardInfo.getCreditCardNumber());
			promoFinanceRequest.setCvvNum(pCreditCardInfo.getCvv());
			promoFinanceRequest.setEmail(pCreditCardInfo.getEmail());
			promoFinanceRequest.setExpirationMonth(pCreditCardInfo.getExpirationMonth());
			promoFinanceRequest.setExpirationYear(pCreditCardInfo.getExpirationYear());
			promoFinanceRequest.setMerchantRefNumber(pCreditCardInfo.getMerchantRefNumber());
			promoFinanceRequest.setRetryCount(pCreditCardInfo.getRetryCount());
			promoFinanceRequest.setSchemaVersion(getRadialconfig().getSchemaVersion());
			long unId = getPaymentTools().getUidGenerator().getPaymentId(); 
			promoFinanceRequest.setUniqueId(unId);
			
			promoFinanceBuilder.buildRequest(promoFinanceRequest);
			
			String requestXML = transformToXML(promoFinanceBuilder.getRequestObject(), IRadialConstants.PACKAGE_NAME);
			
			String operation = pCreditCardInfo.getCreditCardType().concat(IRadialConstants.PROMO_OPERATION);
			try{
				response = executeRequest(requestXML, operation, getRadialconfig().getMaxReTry());

				JAXBElement<TermsAndConditionsRequestType> termsAndConditionsRequest = promoFinanceBuilder.getRequestObject();
				if(isLoggingDebug()){
					TermsAndConditionsRequestType promoRequest = termsAndConditionsRequest.getValue();
					PaymentContextType paymentContextType = promoRequest.getPaymentContext();
					String cardNumber = paymentContextType.getPaymentAccountUniqueId().getValue();
					if(!StringUtils.isBlank(cardNumber)){
						cardNumber = getEncryptedCardNumber(cardNumber);
						paymentContextType.getPaymentAccountUniqueId().setValue(cardNumber);
						promoRequest.setPaymentContext(paymentContextType);
						//termsAndConditionsRequest.setValue(promoRequest);
					}

					String cvvNum = promoRequest.getCardSecurityCode();
					if(!StringUtils.isBlank(cvvNum)){
						cvvNum = getEncryptedCVVNumber(cvvNum);
						promoRequest.setCardSecurityCode(cvvNum);
						//termsAndConditionsRequest.setValue(promoRequest);
					}

					String requestXML1 = transformToXML(termsAndConditionsRequest, IRadialConstants.PACKAGE_NAME);
					vlogDebug("PromoFinannceRequest :{0}",requestXML1);
				}


				if(null != response){
					responseXML = promoFinanceBuilder.formatXML(response.getResponseBody());
				}
				if(isLoggingDebug()){			
					if(null != response){
						vlogDebug("PromoFinance Response ? ");
						vlogDebug("{0}",responseXML);
					}else{
						vlogDebug("RadialGatewayResponse is null for PromoFinance");
					}			
				}
				if(response == null){	
					//This is for if not response from service,Then treat as timeout and create error code for front end purpose.
					promoFinanceBuilder.buildTimeOutResponse(promoFinanceResponse);
				}else if(response.getStatusCode() != IRadialConstants.HTTP_SUCCESS_STATE  && !response.getResponseBody().isEmpty() ) {
					promoFinanceBuilder.setFaultResponseType(transformToObject(response.getResponseBody(), FaultResponseType.class));
					promoFinanceBuilder.buildFaultResponse(promoFinanceResponse);
					pCreditCardStatus.setErrorMessage(promoFinanceResponse.getRadialError().getErrorMessage());

				} else if (!response.getResponseBody().isEmpty()) {
					promoFinanceBuilder.setTermsAndConditionsReplyType(transformToObject(response.getResponseBody(), TermsAndConditionsReplyType.class));
					promoFinanceBuilder.buildResponse(promoFinanceResponse);
					String responseCode = IRadialConstants.EMPTY_STRING;
					if(promoFinanceResponse.getResponseCode() != null){
						responseCode = promoFinanceResponse.getResponseCode().toString() ;}
					String termsAndConditionText = promoFinanceResponse.getTermsAndConditionText();
					String promoFinanceRate = promoFinanceResponse.getPromoFinanceRate();
					vlogDebug(" mResponseCode :{0} termsAndConditionText :{1} promoFinanceRate :{2}",responseCode,termsAndConditionText,promoFinanceRate);
					pCreditCardStatus.setResponseCode(responseCode);
					pCreditCardStatus.setPromoText(termsAndConditionText);
					pCreditCardStatus.setPromoFinanceRate(promoFinanceRate);
				} 
				pCreditCardStatus.setResponseCode(promoFinanceResponse.getResponseCode());
			}
			catch(TRURadialIntegrationException exe){
				vlogError("TRURadialIntegrationException at TRURadialPaymentProcessor/creditCardAuthorizationRequest method :{0}",exe);
				throw new TRUPaymentIntegrationException(exe);
			}
			
		 }
		if (isLoggingDebug()) {
			vlogDebug("TRURadialPaymentProcessor  (promoFinanceAuthorization) : End");
		}
	}

}

