/*
 * @(#) SecureURLUtils.java 1.0
 * 
 * Copyright PA, Inc. All rights reserved. PA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.tru.utils;

import atg.core.util.StringUtils;

import com.tru.common.TRUConstants;

/**
 * This class is used to construct the secure page URL for the given non secure URL.
 * @author PA
 * @version 1.0
 * 
 */
public class SecureURLUtils {

	/**
	 * This constant holds the secure protocol.
	 */
	public static final String SECURE_PROTOCOL = "https";

	/**
	 * Constant for NON_SECURE_PROTOCOL.
	 */
	public static final String NON_SECURE_PROTOCOL = "http";
	
	/**
	 * default constructor.
	 */
	
	private SecureURLUtils() {

	}

	/**
	 * This method used to construct the secure url for the given url.
	 * @param pHostName - String
	 * @param pPort - int
	 * @param pUrl - String
	 * @param pPortRequired - boolean
	 * @return String - the secure URL
	 */
	public static String secureURL(String pHostName, int pPort, String pUrl, boolean pPortRequired){
		String redirectUrl = null;
		StringBuilder sb = new StringBuilder();
		if (!StringUtils.isBlank(pHostName)) {
			String scheme = SECURE_PROTOCOL;
			if (!StringUtils.isBlank(scheme)) {
				sb.append(scheme);
			}
			sb.append(TRUConstants.URL_SEPARATOR);
			if (!StringUtils.isBlank(pHostName)) {
				sb.append(pHostName);
			}
			if (pPortRequired) {
				sb.append(TRUConstants.COLON);
				sb.append(pPort);
			}
		}
		if (!StringUtils.isBlank(pUrl)) {
			sb.append(pUrl);
		}
		redirectUrl = sb.toString();
		return redirectUrl;
	}

	/**
	 * This method used to construct the secure url for the given url.
	 * 
	 * @param pHostName
	 *            - String
	 * @param pPort
	 *            - int
	 * @param pUrl
	 *            - String
	 * @param pPortRequired
	 *            - boolean
	 * @return String - the secure URL
	 */
	public static String nonSecureURL(String pHostName, int pPort, String pUrl, boolean pPortRequired) {
		String nonSecureUrl = null;
		StringBuilder nonSecureUrlBuilder = new StringBuilder();
		if (!StringUtils.isBlank(pHostName)) {
			String scheme = NON_SECURE_PROTOCOL;
			if (!StringUtils.isBlank(scheme)) {
				nonSecureUrlBuilder.append(scheme);
			}
			nonSecureUrlBuilder.append(TRUConstants.URL_SEPARATOR);
			if (!StringUtils.isBlank(pHostName)) {
				nonSecureUrlBuilder.append(pHostName);
			}
			if (pPortRequired) {
				nonSecureUrlBuilder.append(TRUConstants.COLON);
				nonSecureUrlBuilder.append(pPort);
			}
		}
		if (!StringUtils.isBlank(pUrl)) {
			nonSecureUrlBuilder.append(pUrl);
		}
		nonSecureUrl = nonSecureUrlBuilder.toString();
		return nonSecureUrl;
	}
}
