package com.tru.utils;

import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.servlet.DynamoHttpServletRequest;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.common.TRUIntegrationConfiguration;
import com.tru.common.TRUSOSIntegrationConfig;
import com.tru.common.vo.TRUSiteVO;


/**
 * The Class checks the integration status for site and returns the status * 
 * @author PA
 * @version 1.0
 *
 */
public class TRUIntegrationStatusUtil extends GenericService {
	
	/** The mStoreIntegrationConfiguration. */
	private TRUIntegrationConfiguration mStoreIntegrationConfiguration;
	
	/** The mSosIntegrationConfiguration. */
	private TRUSOSIntegrationConfig mSosIntegrationConfiguration;
	
	/** The mSiteTypeUtil. */
	private TRUGetSiteTypeUtil mSiteTypeUtil;

	/**
	 * @return the storeIntegrationConfiguration
	 */
	public TRUIntegrationConfiguration getStoreIntegrationConfiguration() {
		return mStoreIntegrationConfiguration;
	}

	/**
	 * @param pStoreIntegrationConfiguration the storeIntegrationConfiguration to set
	 */
	public void setStoreIntegrationConfiguration(TRUIntegrationConfiguration pStoreIntegrationConfiguration) {
		mStoreIntegrationConfiguration = pStoreIntegrationConfiguration;
	}

	/**
	 * @return the sosIntegrationConfiguration
	 */
	public TRUSOSIntegrationConfig getSosIntegrationConfiguration() {
		return mSosIntegrationConfiguration;
	}

	/**
	 * @param pSosIntegrationConfiguration the sosIntegrationConfiguration to set
	 */
	public void setSosIntegrationConfiguration(TRUSOSIntegrationConfig pSosIntegrationConfiguration) {
		mSosIntegrationConfiguration = pSosIntegrationConfiguration;
	}
	
	/**
	 * @return the siteTypeUtil
	 */
	public TRUGetSiteTypeUtil getSiteTypeUtil() {
		return mSiteTypeUtil;
	}

	/**
	 * @param pSiteTypeUtil the siteTypeUtil to set
	 */
	public void setSiteTypeUtil(TRUGetSiteTypeUtil pSiteTypeUtil) {
		mSiteTypeUtil = pSiteTypeUtil;
	}

	/**
	 * This method returns the Address Doctor integrations status based current request is for sos or store
	 * @param pRequest the request
	 * @return lEnabled - address doctor integration is enable or not
	 */
	public boolean getAddressDoctorIntStatus(DynamoHttpServletRequest pRequest){
		if (isLoggingDebug()) {
			logDebug("START : TRUIntegrationStatusUtil getAddressDoctorIntStatus method");
		}
		boolean lEnabled = Boolean.FALSE;
		Site lSite = SiteContextManager.getCurrentSite();
		TRUSiteVO truSiteVO = getSiteTypeUtil().getSiteInfo(pRequest,lSite);
		if(null == truSiteVO){
			return lEnabled; 
		}
		if(TRUCommerceConstants.SOS.equals(truSiteVO.getSiteStatus())){			
			lEnabled = getSosIntegrationConfiguration().isEnableAddressDoctor();
			if (isLoggingDebug()) {
				vlogDebug(" Address doctor service enabled in SOS : {0} ",lEnabled);
			}
		} else {
			lEnabled = getStoreIntegrationConfiguration().isEnableAddressDoctor();
			if (isLoggingDebug()) {
				vlogDebug(" Address doctor service enabled : {0}  ",lEnabled);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END : TRUIntegrationStatusUtil getAddressDoctorIntStatus method");
		}
		return lEnabled;
	}
	
	/**
	 * This method returns the Coupon service integrations status based current request is for sos or store
	 * @param pRequest the request
	 * @return the site info
	 */
	public boolean getCouponServiceIntStatus(DynamoHttpServletRequest pRequest){
		if (isLoggingDebug()) {
			logDebug("START : TRUIntegrationStatusUtil getCouponServiceIntStatus method");
		}
		boolean lEnabled = Boolean.FALSE;
		Site lSite = SiteContextManager.getCurrentSite();
		TRUSiteVO truSiteVO = getSiteTypeUtil().getSiteInfo(pRequest,lSite);
		if(null == truSiteVO){
			return lEnabled; 
		}
		if(TRUCommerceConstants.SOS.equals(truSiteVO.getSiteStatus())){			
			lEnabled = getSosIntegrationConfiguration().isEnableCouponService();
			if (isLoggingDebug()) {
				vlogDebug("Coupon Service enabled in SOS : {0} ",lEnabled);
			}
		} else {
			lEnabled = getStoreIntegrationConfiguration().isEnableCouponService();
			if (isLoggingDebug()) {
				vlogDebug("Coupon Service enabled : {0}  ",lEnabled);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END : TRUIntegrationStatusUtil getCouponServiceIntStatus method");
		}
		return lEnabled;
	}
	
	/**
	 * This method returns the Epslon integrations status based current request is for sos or store
	 * @param pRequest the request
	 * @return the site info
	 */
	public boolean getEpslonIntStatus(DynamoHttpServletRequest pRequest){
		if (isLoggingDebug()) {
			logDebug("START : TRUIntegrationStatusUtil getEpslonIntStatus method");
		}
		boolean lEnabled = Boolean.FALSE;
		Site lSite = SiteContextManager.getCurrentSite();
		TRUSiteVO truSiteVO = getSiteTypeUtil().getSiteInfo(pRequest,lSite);
		if(null == truSiteVO){
			return lEnabled; 
		}
		if(TRUCommerceConstants.SOS.equals(truSiteVO.getSiteStatus())){			
			lEnabled = getSosIntegrationConfiguration().isEnableEpslon();
			if (isLoggingDebug()) {
				vlogDebug("Epslon enabled in SOS : {0} ",lEnabled);
			}
		} else {
			lEnabled = getStoreIntegrationConfiguration().isEnableEpslon();
			if (isLoggingDebug()) {
				vlogDebug("Epslon enabled : {0}  ",lEnabled);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END : TRUIntegrationStatusUtil getEpslonIntStatus method");
		}
		return lEnabled;
	}
	
}
