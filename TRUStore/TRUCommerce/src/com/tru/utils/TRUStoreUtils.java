package com.tru.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import atg.core.util.StringUtils;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.vo.ProductInfoVO;
import com.tru.commerce.catalog.vo.SKUInfoVO;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.locations.TRUStoreLocatorTools;
import com.tru.common.TRUConstants;
import com.tru.common.TRUStoreConfiguration;
import com.tru.common.tol.IResourceBundleTools;

/**
 * This is a utility class for store locator.
 * @author PA
 * @version 1.0 
 * 
 */
public class TRUStoreUtils extends GenericService{
	
	/** Property to hold is mStoreLocatorTools. */
	private TRUStoreLocatorTools mStoreLocatorTools;
	
	/** Property to hold is mStoreConfig. */
	private TRUStoreConfiguration mStoreConfig;
	
	/** Property to hold is mCatalogTools. */
	private TRUCatalogTools mCatalogTools;
	
	/** Holds the constant of IResourceBundleTools. */
	private IResourceBundleTools mResourceBundleTools;
	
	/**
	 * Gets the resource bundle tools.
	 * 
	 * @return the resourceBundleTools
	 */
	public IResourceBundleTools getResourceBundleTools() {
		return mResourceBundleTools;
	}

	/**
	 * Sets the resource bundle tools.
	 * 
	 * @param pResourceBundleTools
	 *            the resourceBundleTools to set
	 */
	public void setResourceBundleTools(IResourceBundleTools pResourceBundleTools) {
		mResourceBundleTools = pResourceBundleTools;
	}
	
	/**
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * @return the storeConfig
	 */
	public TRUStoreConfiguration getStoreConfig() {
		return mStoreConfig;
	}

	/**
	 * @param pStoreConfig the storeConfig to set
	 */
	public void setStoreConfig(TRUStoreConfiguration pStoreConfig) {
		mStoreConfig = pStoreConfig;
	}

	/**
	 * @return the storeLocatorTools
	 */
	public TRUStoreLocatorTools getStoreLocatorTools() {
		return mStoreLocatorTools;
	}

	/**
	 * @param pStoreLocatorTools the storeLocatorTools to set
	 */
	public void setStoreLocatorTools(TRUStoreLocatorTools pStoreLocatorTools) {
		mStoreLocatorTools = pStoreLocatorTools;
	}

	/**
	 * This method is used to get the StoreId from cookie.
	 * @param pCookieValue - CookieValue
	 * @return storeId
	 */
	public String getStoreIdFromCoockie(String pCookieValue){
		String storeId=null;
		 if(!StringUtils.isBlank(pCookieValue)){
     		String[] favStoreArry = StringUtils.splitStringAtCharacter(pCookieValue, TRUConstants.PIPE_STRING_CHAR);
     		if(favStoreArry != null && favStoreArry.length >= TRUConstants.TWO){
     			storeId = favStoreArry[TRUConstants.INTEGER_NUMBER_ONE];
     		}
     	}
		 return storeId;
	}

	/**
	 * This method is to get the store available message for a sku.
	 * @param pStoreId - store id
	 * @param pSkuInfoVO - SKU info VO
	 * @param pProductInfo - product info VO
	 * @return storeMessagemap - map of available store
	 */
	@SuppressWarnings("rawtypes")
	public Map getStoreAvailableMessage(String pStoreId, SKUInfoVO pSkuInfoVO,ProductInfoVO pProductInfo) {
		vlogDebug("BEGIN: TRUStoreUtils.getStoreAvailableMessage pStoreId", pStoreId );
		  TRUStoreLocatorTools locatorTools = ((TRUStoreLocatorTools)getStoreLocatorTools());
		  Map<String,String> storeMessagemap= new HashMap<String,String>();
		  String storeAvailMsg=null;
		  String storeAvailMsg1=null;
		  String key=null;
		  String key1=null;
		  if(pProductInfo == null || pProductInfo.getDefaultSKU() == null){
			  return storeMessagemap;
		  }
		  String ispu =pProductInfo.getDefaultSKU().getIspu();
		  String s2s =pProductInfo.getDefaultSKU().getS2s();
		  String variantStatus= pProductInfo.getColorSizeVariantsAvailableStatus();
		  boolean presellable= pSkuInfoVO.isPresellable();
		  Map<String,String> inventoryMap= new HashMap<>();
		  String siteId= SiteContextManager.getCurrentSiteId();
		  if(pSkuInfoVO != null && !presellable &&   TRUCommerceConstants.NO_COLOR_SIZE_AVAILABLE.equals(variantStatus)){
		  inventoryMap = getCatalogTools().populateInventory(pSkuInfoVO.getId(), pStoreId, inventoryMap);
		  vlogDebug("TRUStoreUtils.getStoreAvailableMessage ispu{0},s2s{1}", ispu,s2s );
		  if(StringUtils.isNotBlank(pStoreId)){
	      	//Get the store item
	      	RepositoryItem storeRepoItem = locatorTools.getStoreItem(pStoreId);
	      	String address1 = (String)locatorTools.getPropertyValue(storeRepoItem,locatorTools.getLocationPropertyManager().getAddress1PropertyName());
	      	
	      	String chainCode = (String)locatorTools.getPropertyValue(storeRepoItem,locatorTools.getLocationPropertyManager().getChainCodePropertyName());
	      	 if(StringUtils.isNotBlank(chainCode)){
	      	String chianName = locatorTools.getToysMap().get(chainCode.trim());
	      	
	      	
	      	//Prepare the store availability message
	      	if(!StringUtils.isBlank(address1)){
	      		String inventoryStatus=inventoryMap.get(TRUCommerceConstants.INVENTORY_STATUS_STORE);
	      		if(TRUCommerceConstants.IN_STOCK.equals(inventoryStatus) && !(TRUCommerceConstants.NO.equals(ispu) && TRUCommerceConstants.NO.equals(s2s))){
	      			if(TRUCommerceConstants.YES.equals(ispu)){
	      				key=TRUCommerceConstants.STORE_MESSAGE_PDP_5;
	      					key1=TRUCommerceConstants.STORE_MESSAGE_NEW_PDP_5;
	      			}else{
	      					key=TRUCommerceConstants.STORE_MESSAGE_PDP_6;
	      					key1=TRUCommerceConstants.STORE_MESSAGE_NEW_PDP_6;
	      				}
	      		}else if((TRUCommerceConstants.YES.equals(ispu) && TRUCommerceConstants.NO.equals(s2s)) || (TRUCommerceConstants.NO.equals(ispu) && TRUCommerceConstants.NO.equals(s2s))){
	      					key=TRUCommerceConstants.STORE_MESSAGE_PDP_7;
	      					key1=TRUCommerceConstants.STORE_MESSAGE_NEW_PDP_7;
	      				}
	      		 else if(TRUCommerceConstants.YES.equals(s2s)){
							key=TRUCommerceConstants.STORE_MESSAGE_PDP_6;
							key1=TRUCommerceConstants.STORE_MESSAGE_NEW_PDP_6;
	      		    	}
	      		
	      		 try {
					  storeAvailMsg = getResourceBundleTools().getKeyValue(key,siteId);
					  storeAvailMsg1 = key1;
				  } catch (RepositoryException e) {
						vlogError("TRUStoreUtils.getStoreAvailableMessage Error", e);
					}
				  if(storeAvailMsg == null || StringUtils.isBlank(storeAvailMsg)){
					  storeAvailMsg = key;
				  }
				  if(storeAvailMsg1 == null || StringUtils.isBlank(storeAvailMsg1)){
					  storeAvailMsg1 = key1;
				  }
	      		StringBuilder builder = new StringBuilder();
	      		builder.append(storeAvailMsg);
	      		builder.append(TRUCommerceConstants.SPACE);
		      	builder.append(chianName).append(TRUCheckoutConstants.COMMA_SEPERATOR).append(address1);
		      	storeAvailMsg=builder.toString();
		      	StringBuilder storeAvailBuilder = new StringBuilder();
		      	storeAvailBuilder.append(storeAvailMsg1);
		      	storeAvailBuilder.append(TRUCommerceConstants.PIPELINE);
		      	storeAvailBuilder.append(TRUCommerceConstants.AT).append(TRUCommerceConstants.SPACE);
		      	storeAvailBuilder.append(chianName).append(TRUCheckoutConstants.COMMA_SEPERATOR).append(address1);
		      	storeAvailMsg1=storeAvailBuilder.toString();
	      	}
	      }
		  }else{
			  //String inventoryStatusOnline=inventoryMap.get(TRUCommerceConstants.INVENTORY_STATUS_ONLINE);
			 
			  if(TRUCommerceConstants.YES.equals(ispu) && TRUCommerceConstants.YES.equals(s2s)){
				  key=TRUCommerceConstants.STORE_MESSAGE_PDP_1;
			  } else if(TRUCommerceConstants.YES.equals(ispu) && TRUCommerceConstants.NO.equals(s2s)){
				  key=TRUCommerceConstants.STORE_MESSAGE_PDP_2;
			  }else if(TRUCommerceConstants.NO.equals(ispu) && TRUCommerceConstants.YES.equals(s2s)){
				  key=TRUCommerceConstants.STORE_MESSAGE_PDP_3;
			  }
			  else{
				  key=TRUCommerceConstants.STORE_MESSAGE_PDP_4;
			  }
			  try {
				  storeAvailMsg = getResourceBundleTools().getKeyValue(key,siteId);
			  } catch (RepositoryException e) {
					vlogError("TRUStoreUtils.getStoreAvailableMessage Error", e);
				}
			  if(storeAvailMsg == null || StringUtils.isBlank(storeAvailMsg)){
				  storeAvailMsg = key;
			  }
		  }
		  vlogDebug("END: TRUStoreUtils.getStoreAvailableMessage storeAvailMsg", storeAvailMsg );
	}
		  storeMessagemap.put(TRUCommerceConstants.SKU_ID_PDP, pSkuInfoVO.getId());
		  storeMessagemap.put(TRUCommerceConstants.STORE_ID_PDP, pStoreId);
		  storeMessagemap.put(TRUCommerceConstants.INVENTORY_STORE_MESSAGE, storeAvailMsg);
		  if(storeAvailMsg1!=null){
			  storeMessagemap.put(TRUCommerceConstants.INVENTORY_STORE_MESSAGE1, storeAvailMsg1);
			  vlogDebug("TRUStoreUtils.getStoreAvailableMessage storeAvailMsg1", storeAvailMsg1 );
		  }
		  vlogDebug("END: TRUStoreUtils.getStoreAvailableMessage storeMessagemap", storeMessagemap );
		return storeMessagemap;
	}
	
	/**
	 * To find name have special character or not.
	 * @param pName - String.
	 * @return Matcher - Matcher.
	 */
	public boolean containsSpecialCharacter(String pName) {
		final Pattern p = Pattern.compile(TRUCommerceConstants.CHAR_PATTERN, Pattern.CASE_INSENSITIVE);
		final Matcher m = p.matcher(pName);
		return m.find();
	}
}
