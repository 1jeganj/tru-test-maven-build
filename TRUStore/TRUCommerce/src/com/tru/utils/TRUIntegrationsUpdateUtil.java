package com.tru.utils;

import atg.multisite.SiteContextManager;
import atg.multisite.TRUSiteTools;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

/**
 * Extending GenericService class.This class is called during server startup and update the sos integration service value.
 * @author Professional Access
 * @version 1.0
 */
public class TRUIntegrationsUpdateUtil extends GenericService {
	/** Property to hold mSiteTools. */
	private TRUSiteTools mSiteTools;
	
	/**
	 * @return the siteTools
	 */
	public TRUSiteTools getSiteTools() {
		return mSiteTools;
	}

	/**
	 * @param pSiteTools the siteTools to set
	 */
	public void setSiteTools(TRUSiteTools pSiteTools) {
		mSiteTools = pSiteTools;
	}

	/** @see atg.nucleus.GenericService#doStartService()
	 */
	@Override
	public void doStartService() {
		if(isLoggingDebug()) {
			logDebug("Entering into TRUIntegrationsUpdateUtil.doStartService()");
		}
		RepositoryItem siteItem = SiteContextManager.getCurrentSite();
		if(siteItem == null) {
			try {
				siteItem = getSiteTools().getSiteManager().getSite(getSiteTools().getIntegrationPropertiesConfig().getDefaultTRUSiteId());
			} catch (RepositoryException pExce) {
				vlogError("Exception while getting site from site repository", pExce);
			}
		}
		getSiteTools().updateIntegrations(siteItem);
		if(isLoggingDebug()) {
			logDebug("exiting TRUIntegrationsUpdateUtil.doStartService()");
		}
	}
}
