package com.tru.utils;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.endeca.utils.TRUProductPageUtil;

/**
 * Droplet to generate PDP Url.
 * @author PA
 * @version 1.0
 */
public class TRUPdpURLDroplet extends DynamoServlet {
	/**
	 *  Holds the mProductPageUrl.
	 */
	private String mProductPageUrl;

	/**
	 *  Holds the mProductPageUtil.
	 */
	private TRUProductPageUtil mProductPageUtil;


	/**
	 * @return the productPageUtil
	 */
	public TRUProductPageUtil getProductPageUtil() {
		return mProductPageUtil;
	}


	/**
	 * @param pProductPageUtil the productPageUtil to set
	 */
	public void setProductPageUtil(TRUProductPageUtil pProductPageUtil) {
		mProductPageUtil = pProductPageUtil;
	}

	/**
	 * Used to generate the PDP URL.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for pResourcePath
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUPdpURLDroplet.service method..");
		}

		String siteId = (String) pRequest.getLocalParameter(TRUCommerceConstants.SITE_ID);
		String productId = (String) pRequest.getLocalParameter(TRUCommerceConstants.PRODUCT_ID);
		
		String productPageUrl = null;
		if(!StringUtils.isEmpty(productId)) {
			if(!StringUtils.isEmpty(siteId)) {
				productPageUrl = getProductPageUtil().getProductPageURL(productId,siteId);
			} else {
				productPageUrl = getProductPageUtil().getProductPageURL(productId);
			}
		}else{
			productPageUrl= TRUCommerceConstants.HASH;
		}
		if(!StringUtils.isEmpty(productPageUrl)) {
			pRequest.setParameter(getProductPageUrl(), productPageUrl);
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		} else {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUPdpURLDroplet.service method..");
		}
	}


	/**
	 * @return the productPageUrl
	 */
	public String getProductPageUrl() {
		return mProductPageUrl;
	}


	/**
	 * @param pProductPageUrl the productPageUrl to set
	 */
	public void setProductPageUrl(String pProductPageUrl) {
		mProductPageUrl = pProductPageUrl;
	}

}
