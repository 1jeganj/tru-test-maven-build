package com.tru.utils;

import java.util.Enumeration;

import atg.nucleus.GenericService;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.tru.common.TRUConstants;

/**
 * This is a utility class for store locator.
 * 
 * @author PA
 * @version 1.0
 * 
 */
public class TRUSchemeDetectionUtil extends GenericService {

	/**
	 * The Constant ORIGIN_SCHEME_HEADER.
	 */
	public static final String ORIGIN_SCHEME_HEADER = "origin-scheme";

	/**
	 * Non Secure protocol constant.
	 */
	public static final String NON_SECURE_PROTOCOL = "http";

	/**
	 * Secure protocol constant.
	 */
	public static final String SECURE_PROTOCOL = "https";

	/**
	 * scheme constant.
	 */
	public static final String SCHEME = "scheme";

	/**
	 * enable scheme detection from header
	 */
	protected Boolean mEnableSchemeDetectionFromHeader;

	/**
	 * This method used to check origin scheme header.
	 * 
	 * @return scheme - scheme
	 */
	public String getScheme() {
		if (isLoggingDebug()) {
			logDebug("START:: TRUSchemeDetectionUtil.getScheme()");
		}
		DynamoHttpServletRequest currentRequest = ServletUtil.getCurrentRequest();
		if (currentRequest != null) {
			if (getEnableSchemeDetectionFromHeader()) {
				if (isLoggingDebug()) {
					Enumeration headerNames = currentRequest.getHeaderNames();
					while (headerNames.hasMoreElements()) {
						String key = (String) headerNames.nextElement();
						String value = currentRequest.getHeader(key);
						logDebug(key + " : " + value);
					}
				}
				String schemeFromHeader = currentRequest
						.getHeader(ORIGIN_SCHEME_HEADER);
				if (isLoggingDebug()) {
					logDebug("scheme from header " + schemeFromHeader);
				}
				if ((schemeFromHeader != null)
						&& schemeFromHeader.equalsIgnoreCase(SECURE_PROTOCOL)) {
					if (isLoggingDebug()) {
						logDebug("setting scheme from header " + schemeFromHeader);
					}
					return schemeFromHeader + TRUConstants.URL_SEPARATOR;
				}
			} else {
				if ((currentRequest.getScheme() != null)
						&& currentRequest.getScheme().equalsIgnoreCase(
								SECURE_PROTOCOL)) {
					if (isLoggingDebug()) {
						logDebug("setting scheme from Request "
								+ currentRequest.getScheme());
					}
					return currentRequest.getScheme() + TRUConstants.URL_SEPARATOR;
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUSchemeDetectionUtil.getScheme()");
		}
		return NON_SECURE_PROTOCOL + TRUConstants.URL_SEPARATOR;
	}

	/**
	 * @return the enabled status.
	 */
	public Boolean getEnableSchemeDetectionFromHeader() {
		return mEnableSchemeDetectionFromHeader;
	}

	/**
	 * @param pEnabled
	 *            - the enabled status to set.
	 */
	public void setEnableSchemeDetectionFromHeader(Boolean pEnabled) {
		mEnableSchemeDetectionFromHeader = pEnabled;
	}

}
