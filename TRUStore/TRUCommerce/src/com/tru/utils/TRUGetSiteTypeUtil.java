package com.tru.utils;

import java.util.List;

import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUStoreConfiguration;
import com.tru.common.vo.TRUSiteVO;
import com.tru.endeca.utils.TRUProductPageUtil;
import com.tru.integrations.TRUIntegrationPropertiesConfig;


/**
 * The Class TRUGetSiteTypeUtil determine the current request is for sos or store
 * and also set the site URL according to that.
 * @author PA
 * @version 1.0
 *
 */
public class TRUGetSiteTypeUtil extends GenericService{

	/** The m site context mSiteContextManager. */
	private SiteContextManager mSiteContextManager;

	/** The m tru mTRUConfiguration. */
	private TRUConfiguration mTRUConfiguration;

	/** The m trusos integration properties mTRUIntegrationPropertiesConfig. */
	private TRUIntegrationPropertiesConfig mTRUIntegrationPropertiesConfig;
    
	/**  Holds the mCatalogProperties */
	private TRUCatalogProperties mCatalogProperties;
	/**
	 *  Holds the mProductPageUtil.
	 */
	private TRUProductPageUtil mProductPageUtil;
	
	private TRUStoreConfiguration mConfiguration;
	/**
	 * This method determine the current request is for sos or store
	 * and also set the site URL according to that.
	 *
	 * @param pRequest the request
	 * @param pSite the site
	 * @return the site info
	 */
	public TRUSiteVO getSiteInfo(DynamoHttpServletRequest pRequest, Site pSite){
		vlogDebug("===BEGIN===:: TRUGetSiteTypeUtil.getSiteInfo() method:: ");

		String siteUrl = null;
		TRUSiteVO truSiteVO = null;
		boolean previewEnabled=getTRUConfiguration().isPreviewEnabled();
		vlogDebug("===BEGIN===:: TRUGetSiteTypeUtil.pRequest:: "+pRequest);
		if(pSite != null){
			vlogDebug("Found site {0}", new Object[] { pSite.getId() });
			String[] siteUrls = (String[]) pSite.getPropertyValue(getCatalogProperties().getAdditionalProductionURLs());
			List<String> sosConfiguredUrl = getTRUIntegrationPropertiesConfig().getSosConfiguredURLs();
			
			vlogDebug("===BEGIN===:: TRUGetSiteTypeUtil.getTRUConfiguration() :: "+getTRUConfiguration());
			vlogDebug("===BEGIN===:: TRUGetSiteTypeUtil.siteUrls :: "+siteUrls);
			vlogDebug("===BEGIN===:: TRUGetSiteTypeUtil.sosConfiguredUrl :: "+sosConfiguredUrl);
			
			boolean aosFlag = false;
			List<String> sosCookieNames = getTRUConfiguration().getSosCookieNames();
			if(pRequest!=null&&pRequest.getContextPath().startsWith(TRUCommerceConstants.REST_CONTEXT_PATH)){
				for(String sosCookieName:sosCookieNames){			
					if(StringUtils.isNotEmpty(pRequest.getCookieParameter(sosCookieName))||(Boolean.parseBoolean(pRequest.getParameter(TRUCommerceConstants.SOS)))){
						aosFlag = true;
						break;
					}
				}
			}
			
			truSiteVO = new TRUSiteVO();
			
			if (siteUrls != null && siteUrls.length > 0) {
				if (getTRUConfiguration().isEnableSOS() 
						&& siteUrls != null && pRequest!=null
						&& !StringUtils.isBlank(pRequest.getServerName()) 
						&& sosConfiguredUrl != null 
						&& !sosConfiguredUrl.isEmpty() 
						&& (sosConfiguredUrl.contains(pRequest.getServerName())||aosFlag)
						) {
					if(previewEnabled){
						if (siteUrls.length > TRUCommerceConstants.INT_THREE) {
							siteUrl = siteUrls[TRUCommerceConstants.INT_THREE];
							truSiteVO.setSiteStatus(TRUCommerceConstants.SOS);
							truSiteVO.setSiteURL(siteUrl);
						} else {
							siteUrl = siteUrls[TRUCommerceConstants.INT_ZERO];
							truSiteVO.setSiteStatus(TRUCommerceConstants.STORE);
							truSiteVO.setSiteURL(siteUrl);
						}
					}
					else{
					if (siteUrls.length > TRUCommerceConstants.INT_ONE) {
						siteUrl = siteUrls[TRUCommerceConstants.INT_ONE];
						truSiteVO.setSiteStatus(TRUCommerceConstants.SOS);
						truSiteVO.setSiteURL(siteUrl);
					} else {
						siteUrl = siteUrls[TRUCommerceConstants.INT_ZERO];
						truSiteVO.setSiteStatus(TRUCommerceConstants.STORE);
						truSiteVO.setSiteURL(siteUrl);
					}
				}
				} else {
					if(previewEnabled && siteUrls.length > TRUCommerceConstants.INT_TWO ){
						siteUrl = siteUrls[TRUCommerceConstants.INT_TWO];
						truSiteVO.setSiteStatus(TRUCommerceConstants.STORE);
						truSiteVO.setSiteURL(siteUrl);
					}
					else{
					siteUrl = siteUrls[TRUCommerceConstants.INT_ZERO];
					truSiteVO.setSiteStatus(TRUCommerceConstants.STORE);
					truSiteVO.setSiteURL(siteUrl);
					}
				}
				vlogDebug("site staus {0} , site url{1} ", truSiteVO.getSiteStatus(), siteUrl);
			}

		}
		vlogDebug("===END===:: TRUGetSiteTypeUtil.getSiteInfo() method:: ");
		return truSiteVO;
	}

	/**
	 * This method determine the site info for setting in registry cookie.
	 * 
	 * @param pRequest -DynamoHttpServletRequest
	 * @return siteUrl- the site info
	 */
	public String getSiteInfoForRegistry(DynamoHttpServletRequest pRequest){
		if (isLoggingDebug()) {
			logDebug("Start: TRUGetSiteTypeUtil.getSiteInfoForRegistry()");
		}
		String siteUrl = null;
		Site site =null;
		try {
			site = getAlternateSite(SiteContextManager.getCurrentSite());
		} catch (SiteContextException e) {
			if (isLoggingError()) {
				logError("Site Context Exception:{0}",e);
			}
		}
		if(site != null){
			if (isLoggingDebug()) {
				logDebug("END: TRUGetSiteTypeUtil.getSiteInfoForRegistry()");
			}
			String[] siteUrls = (String[]) site.getPropertyValue(getCatalogProperties().getAdditionalProductionURLs());
			List<String> sosConfiguredUrl = getTRUIntegrationPropertiesConfig().getSosConfiguredURLs();
			if (siteUrls != null && siteUrls.length > 0) {
				if (getTRUConfiguration().isEnableSOS() && 
						siteUrls != null && 
						!StringUtils.isBlank(pRequest.getServerName()) && 
						sosConfiguredUrl != null && 
						 !sosConfiguredUrl.isEmpty() && 
						(sosConfiguredUrl.contains(pRequest.getServerName()))
						) {
					if (siteUrls.length > TRUCommerceConstants.INT_ONE) {
						siteUrl = siteUrls[TRUCommerceConstants.INT_ONE];
						return siteUrl;
					} else {
						siteUrl = siteUrls[TRUCommerceConstants.INT_ZERO];
						return siteUrl;
					}
				} else {
					siteUrl = siteUrls[TRUCommerceConstants.INT_ZERO];
					return siteUrl;
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END: TRUGetSiteTypeUtil.getSiteInfoForRegistry()");
		}
		return siteUrl;
	}

	/**
	 * This method is used to get the Alternate Site.
	 * 
	 * @param pSite -site
	 * @return site -Site
	 * @throws SiteContextException -siteContextException
	 */
	private Site getAlternateSite(Site pSite) throws SiteContextException {
		if (isLoggingDebug()) {
			vlogDebug("START:: TRUGetSiteTypeUtil.getAlternateSite : {0}" );
		}
		Site site = null;
		if (pSite != null && pSite.getId().equals(getConfiguration().getToysRUsSiteId())) {
			Object siteId = getConfiguration().getBabiesRUsSiteId();
			site = getSiteContextManager().getSite(siteId.toString());
			if (isLoggingDebug()) {
			vlogDebug("Found site {0}", new Object[] { site.getId() });
			}
		}else if(pSite != null && pSite.getId().equals(getConfiguration().getBabiesRUsSiteId())){
			Object siteId = getConfiguration().getToysRUsSiteId();
			site = getSiteContextManager().getSite(siteId.toString());
			if (isLoggingDebug()) {
			vlogDebug("Found site {0}", new Object[] { site.getId() });
			}
		}
		else if(pSite!=null &&  pSite.toString()!=null){
			site = getSiteContextManager().getSite(pSite.toString());
			if (isLoggingDebug()) {
				vlogDebug("Found site {0}", new Object[] { site.getId() });
				}
			}
		
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUGetSiteTypeUtil.getAlternateSite : {0}" );
		}
		return site;
	}
	
	
	/**
	 * Checks if is store integration site config enabled.
	 *
	 * @param pSite the site
	 * @param pString the string
	 * @return true, if is store integration site config enabled
	 */
	@SuppressWarnings("unchecked")
	public boolean isStoreIntegrationSiteConfigEnabled(String pString){
		if (isLoggingDebug()) {
			vlogDebug("START:: TRUGetSiteTypeUtil.isStoreIntegrationSiteConfigEnabled");
		}
		Site pSite = SiteContextManager.getCurrentSite();
		if(pSite != null && !StringUtils.isBlank(pString)){
			
			List<RepositoryItem> itemList = (List<RepositoryItem>)pSite.getPropertyValue(getTRUIntegrationPropertiesConfig().getTruIntegrationsPropertyName());
			for(RepositoryItem item : itemList){
				String integName = (String)item.getPropertyValue(getTRUIntegrationPropertiesConfig().getIntegrationNamePropertyName());
				if(pString.equals(integName)){
					return (Boolean)item.getPropertyValue(getTRUIntegrationPropertiesConfig().getEnabledPropertyName());
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUGetSiteTypeUtil.isStoreIntegrationSiteConfigEnabled");
		}
		return false;
	}

	/**
	 * Gets the site.
	 *
	 * @param pSiteId the site id
	 * @return site the site object
	 * @throws SiteContextException the site context exception
	 */
	public Site getSite(Object pSiteId) throws SiteContextException{
		vlogDebug("===BEGIN===:: TRUGetSiteTypeUtil.getSite() method:: ");
		Site site = null;

		if (pSiteId != null) {
			site = getSiteContextManager().getSite(pSiteId.toString());
			vlogDebug("Found site {0}", new Object[] { site.getId() });
		}
		vlogDebug("===END===:: TRUGetSiteTypeUtil.getSite() method:: ");
		return site;
	}
	/**
	 * This method will generate PDP URL for mobile service
	 * @param pProductId - the ProductId
	 * @return pdpURLWithOnlinePID
	 * @throws SiteContextException the site context exception
	 */
	public String getPDPURL(String pProductId) throws SiteContextException {
		vlogDebug("===BEGIN===:: TRUGetSiteTypeUtil.getPDPURL() method:: ");
		String pdpURLWithOnlinePID = null;
		Site site = SiteContextManager.getCurrentSite();
		pdpURLWithOnlinePID = getProductPageUtil().getProductPageURL(pProductId,site.toString());
		return pdpURLWithOnlinePID;
	}
	
	/**
	 * @return the mConfiguration.
	 */
	public TRUStoreConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * @param pConfiguration the mConfiguration to set.
	 */
	public void setConfiguration(TRUStoreConfiguration pConfiguration) {
		this.mConfiguration = pConfiguration;
	}

	/**
	 * Gets the TRU configuration.
	 * @return mTRUConfiguration the TRU configuration
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * Sets the TRU configuration.
	 * @param pTRUConfiguration the new TRU configuration
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		mTRUConfiguration = pTRUConfiguration;
	}

	/**
	 * Gets the site context manager.
	 * @return mSiteContextManager the site context manager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * Sets the site context manager.
	 * @param pSiteContextManager the new site context manager
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}

	/**
	 * Gets the TRU integration properties config
	 * @return mTRUIntegrationPropertiesConfig the TRU integration properties config
	 */
	public TRUIntegrationPropertiesConfig getTRUIntegrationPropertiesConfig() {
		return mTRUIntegrationPropertiesConfig;
	}

	/**
	 * Sets the TRU integration properties config
	 * @param pTRUIntegrationPropertiesConfig the new TRU integration properties config
	 */
	public void setTRUIntegrationPropertiesConfig(
			TRUIntegrationPropertiesConfig pTRUIntegrationPropertiesConfig) {
		mTRUIntegrationPropertiesConfig = pTRUIntegrationPropertiesConfig;
	}

	/**
	 * @return mProductPageUtil
	 */
	public TRUProductPageUtil getProductPageUtil() {
		return mProductPageUtil;
	}

	/**
	 * @param pProductPageUtil the ProductPageUtil to set.
	 */
	public void setProductPageUtil(TRUProductPageUtil pProductPageUtil) {
		mProductPageUtil = pProductPageUtil;
	}

	/**
	 * @return mCatalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * @param pCatalogProperties the CatalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}


}
