package com.tru.utils;

import java.util.Comparator;
import java.util.Map;

import com.tru.common.TRUConstants;

/**
 * This class is used to Sort Map by Value.
 * @author PA
 * @version 1.0
 */
public class TRUValueComparator implements Comparator<Object> {
	/**
	 * map. 
	 */
	Map<String, Long> mMap;
	
	/**
	 * Constructor.
	 * @param pMap - Map
	 */
	public TRUValueComparator(Map<String, Long> pMap){
		this.mMap = pMap;
	}
	
	/**
	 * Sort Map obj by Value. 
	 * 
	 * @param pKeyA - Key A
	 *           
	 * @param pKeyB - Key B
	 * @return integer
	 */
	@Override
	public int compare(Object pKeyA, Object pKeyB){
		if (mMap.get(pKeyA) >= mMap.get(pKeyB)) {
			return TRUConstants.INT_MINUS_ONE;
		} else {
			return TRUConstants.ONE;
		} 
	}
}
