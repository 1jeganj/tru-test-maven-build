package com.tru.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;

import com.tru.cache.TRUSkuPricingCache;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.exception.TRUCommerceException;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.TRUConstants;

/**
 * This is a utility class for Endeca Price.
 * @author PA
 * @version 1.0 
 * 
 */
public class TRUEndecaPriceUtils extends GenericService{
	
	/** Property to hold is mCatalogTools. */
	private TRUCatalogTools mCatalogTools;
	/** This holds the reference for TRUPricingTools. */
	private TRUPricingTools mPricingTools;
	/**  This holds the reference for mCatalogProperties. */
	private TRUCatalogProperties mCatalogProperties;

	/** The m sku pricing cache. */
	private TRUSkuPricingCache mSkuPricingCache;
	
	/**
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * @return the pricingTools
	 */
	public TRUPricingTools getPricingTools() {
		return mPricingTools;
	}
	/**
	 * @param pPricingTools the pricingTools to set
	 */
	public void setPricingTools(TRUPricingTools pPricingTools) {
		mPricingTools = pPricingTools;
	}
	/**
	 * @return the catalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}
	/**
	 * @param pCatalogProperties the catalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}

	/**
	 * Method to generate the price Map for Endeca Pages.
	 * 
	 * @param pSkuProds - String Array - SKU and Product Array
	 * @return - Map - Price Map 
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Map<String, Object>> generatePriceMapForEndeca(
			String[] pSkuProds) {
		if(isLoggingDebug()){
			logDebug("Enter into [Class: TRUEndecaPriceUtils  method: generatePriceMapForEndeca]");
			logDebug("pSkuProds:::" + pSkuProds);
		}
		if(pSkuProds != null && pSkuProds.length <= TRUConstants.ZERO){
			return null;
		}
		
		Map<String, Map<String, Object>> priceMap = null;
		for(String l_skuProd : pSkuProds){
			String[] sku_prd = l_skuProd.split(TRUCommerceConstants.COLON_SYMBOL);
			if(priceMap == null){
				priceMap = new HashMap<String, Map<String, Object>>();
			}
			Map<String, Object> skuPriceMap = null;
			
			if(getPricingTools().getTruConfiguration().isEnableSkuPricingCache()){
				try {
					skuPriceMap	=  (Map<String, Object>) getSkuPricingCache().get(l_skuProd);
				} catch (TRUCommerceException e) {
					if(isLoggingError()){
						vlogError("RepositoryException in TRUEndecaPriceUtils", e);
					}
				}
			}else{
				skuPriceMap	= getSKUPrice(l_skuProd);
			}
			
			if(isLoggingDebug()){
				vlogDebug("skuPriceMap:{0} for sku_prd : {1}", skuPriceMap,sku_prd);
			}
			if(skuPriceMap != null && !skuPriceMap.isEmpty()){
				priceMap.put(sku_prd[TRUConstants.ZERO],skuPriceMap);
			}
		}
		if(isLoggingDebug()){
			vlogDebug("priceMap:{0}", priceMap);
			logDebug("Exiting from [Class: TRUEndecaPriceUtils  method: generatePriceMapForEndeca]");
		}
		return priceMap;
	}
	
	/**
	 * Method to get the Price for sku and price. 
	 * @param pSkuProd - String Array
	 * @return Map - Price Map
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getSKUPrice(String pSkuProd) {
		if(isLoggingDebug()){
			logDebug("Enter into [Class: TRUEndecaPriceUtils  method: getSKUPrice]");
			logDebug("pSkuProd:::" + pSkuProd);
		}
		
		String[] lSkuProdArray = pSkuProd.split(TRUCommerceConstants.COLON_SYMBOL);
		
		Map<String, Object> priceMap = null;
		if(lSkuProdArray != null && lSkuProdArray.length < TRUConstants.TWO){
			return priceMap;
		}
		String parentSkuId = lSkuProdArray[TRUConstants.ZERO];
		String productId = lSkuProdArray[TRUConstants.ONE];
		RepositoryItem skuItem = null;
		RepositoryItem prodItem = null;
		RepositoryItemDescriptor collectionProduct = null;
		List<RepositoryItem> skuList = null;
		boolean isCollectionProduct = false;
		try {
			skuItem = getCatalogTools().findSKU(parentSkuId);
			prodItem = getCatalogTools().findProduct(productId);
			if(isLoggingDebug()){
				vlogDebug("skuItem:{0} prodItem : {1}", skuItem,prodItem);
			}
			if(prodItem == null || skuItem == null){
				return priceMap;
			}
			List<String> styleDimensions = (List<String>) prodItem.getPropertyValue(getCatalogProperties().getProductStyleDimension());
			collectionProduct = prodItem.getItemDescriptor();
			if(isLoggingDebug()){
				vlogDebug("styleDimensions:{0} collectionProduct : {1}", styleDimensions,collectionProduct);
			}
			if (TRUCommerceConstants.COLLECTION_PRODUCT.equals(collectionProduct.getItemDescriptorName())) {
				isCollectionProduct = true;
			}
			boolean isStylizedProduct = checkForStylizedProduct(styleDimensions);
			Site currentSite = SiteContextManager.getCurrentSite();
			skuList = (List<RepositoryItem>) prodItem.getPropertyValue(getCatalogProperties().getChildSKUsPropertyName());
			if(isCollectionProduct)	{
				skuList = (List<RepositoryItem>) prodItem.getPropertyValue(getCatalogProperties().getCollectionSkus());
			}
			if(isLoggingDebug()){
				vlogDebug("isStylizedProduct:{0} currentSite : {1} skuList : {2}", isStylizedProduct,currentSite,skuList);
			}
			if(skuList == null || skuList.isEmpty() || currentSite == null) {
				return priceMap;
			}
			if(isCollectionProduct) {
				// Calling method to get the Price map for Collection/Stylized products.
				priceMap = getPriceMapForStylisedItems(parentSkuId, skuList, productId, currentSite);
			} else if(skuList.size() == TRUConstants.INTEGER_NUMBER_ONE || !isStylizedProduct) {
				// Calling method to get the Price map for regular products.
				priceMap = getPriceMapForNonStylisedItems(parentSkuId, productId, currentSite);
			} else  {
				// Calling method to get the Price map for Collection/Stylized products.
				priceMap = getPriceMapForStylisedItems(parentSkuId, skuList, productId, currentSite);
			}
			
			priceMap.put(TRUCommerceConstants.IS_STYLIZED, isStylizedProduct);
			priceMap.put(TRUCommerceConstants.IS_COLLECTION_PRODUCT, isCollectionProduct);

			if(isCollectionProduct && skuList != null && !skuList.isEmpty()){
				priceMap.put(TRUCommerceConstants.COLLECTION_SKU_LISTSIZE, skuList.size());
			}
			
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError("RepositoryException : {0}", e);
			}
		}
		if(isLoggingDebug()){
			vlogDebug("priceMap:{0}", priceMap);
			logDebug("Exiting from [Class: TRUEndecaPriceUtils  method: getSKUPrice]");
		}
		return priceMap;
	}
	
	/**
	 * Method to check whether product is stylized or regular.
	 * 
	 * @param pStyleDimensions - List - List of Dimension value
	 * @return boolean
	 */
	public boolean checkForStylizedProduct(List<String> pStyleDimensions) {
		if(isLoggingDebug()){
			logDebug("Enter into [Class: TRUEndecaPriceUtils  method: checkForStylizedProduct]");
			vlogDebug("pStyleDimensions : {0}", pStyleDimensions);
		}
		if(!pStyleDimensions.isEmpty()) {
			Set<String> setStyleDimensions=  new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
			setStyleDimensions.addAll(pStyleDimensions);
			if(setStyleDimensions.contains(TRUCommerceConstants.COLOR) || setStyleDimensions.contains(TRUCommerceConstants.SIZE)) {
				if (isLoggingDebug()) {
					logDebug("The product is a Stylized product.");
				}
				return true;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END::: TRUActivePricePropertyAccessor.checkForStylizedProduct method");
		}
		if (isLoggingDebug()) {
			logDebug("The product is a Non Stylized product.");
			logDebug("Exiting from [Class: TRUEndecaPriceUtils  method: checkForStylizedProduct]");
		}
		return false;
	}
	
	/**
	 * @param pParentSkuId - String - Default SKU Id
	 * @param pSkuList - List - List of all sku repository items
	 * @param pProductId - String - product Id.
	 * @param pSite - Site object
	 * @return Map - Price Map for Stylized Item.
	 */
	private Map<String, Object> getPriceMapForStylisedItems(String pParentSkuId, List<RepositoryItem> pSkuList, String pProductId, Site pSite) {
		if(isLoggingDebug()){
			logDebug("Enter into [Class: TRUEndecaPriceUtils  method: getPriceMapForStylisedItems]");
			vlogDebug("pParentSkuId : {0} pSkuList : {1} pProductId : {2} pSite : {3}", pParentSkuId,pSkuList,pProductId,pSite);
		}
		Map<String, Object> priceMap = new HashMap<String, Object>();
		double salePrice=TRUConstants.DOUBLE_ZERO;
		double listPrice=TRUConstants.DOUBLE_ZERO;
		List<Double> allListPriceList = new ArrayList<Double>();
		List<Double> allSalePriceList = new ArrayList<Double>();
		double minlist=TRUConstants.DOUBLE_ZERO;
		double maxlist=TRUConstants.DOUBLE_ZERO;
		double minsale=TRUConstants.DOUBLE_ZERO;
		double maxsale=TRUConstants.DOUBLE_ZERO;
		priceMap.put(TRUCommerceConstants.HAS_PRICE_RANGE, TRUCommerceConstants.TRUE_STRING);
		for (RepositoryItem sku : pSkuList) {
			String skuId = sku.getRepositoryId();
			//boolean isActiveSku = getTRUEndecaUtils().isActiveSku(sku);
			if (!StringUtils.isEmpty(skuId) && !StringUtils.isEmpty(pProductId) && pSite != null) {
				// List price
				listPrice = (double) getPricingTools().getListPriceForSKU(skuId, pProductId, pSite);
				if(listPrice > TRUConstants.DOUBLE_ZERO) {
					allListPriceList.add(listPrice);
				}
				// Sale price
				salePrice = (double) getPricingTools().getSalePriceForSKU(skuId, pProductId, pSite);
				if(salePrice > TRUConstants.DOUBLE_ZERO) {
					allSalePriceList.add(salePrice);
				}
				if(skuId.equals(pParentSkuId)) {
					boolean calculateSavings = getPricingTools().validateThresholdPrices(pSite, listPrice, salePrice);
					if (calculateSavings) {
						priceMap.put(TRUCommerceConstants.SKU_ACTIVEPRICE,salePrice);
					} else {
						priceMap.put(TRUCommerceConstants.SKU_ACTIVEPRICE,listPrice); 
					}
				}
				if (isLoggingDebug()) {
					vlogDebug("For the Sku Id : {0} --- listPrice is : {1} and saleprice is : {2}" , skuId, listPrice, salePrice);
				}
			}
		}

		if (!allListPriceList.isEmpty()) {
			Collections.sort(allListPriceList);
			minlist=allListPriceList.get(TRUConstants.ZERO);
			maxlist=allListPriceList.get(allListPriceList.size() - TRUCommerceConstants.INT_ONE);
			if (isLoggingDebug()) {
				vlogDebug("allListPriceList after sorting: {0} minlist : {1} maxlist : {2}", 
						allListPriceList,minlist,maxlist);
			}
		}

		if (!allSalePriceList.isEmpty()) {
			Collections.sort(allSalePriceList);
			minsale=allSalePriceList.get(TRUConstants.ZERO);
			maxsale=allSalePriceList.get(allSalePriceList.size() - TRUCommerceConstants.INT_ONE);
			if (isLoggingDebug()) {
				vlogDebug("allSalePriceList after sorting: {0} minsale : {1} maxsale : {2}", 
						allSalePriceList,minsale,maxsale);
			}
		}
		if(minlist > TRUConstants.DOUBLE_ZERO && maxlist > TRUConstants.DOUBLE_ZERO){
			priceMap.put(TRUCommerceConstants.IS_PRICE_EXISTS,TRUCommerceConstants.TRUE_STRING); 
			if(minsale <= minlist && maxsale <= maxlist) {
				priceMap.put(TRUCommerceConstants.MIN_PRICE, minsale);
				priceMap.put(TRUCommerceConstants.MAX_PRICE, maxsale);
				priceMap.put(TRUCommerceConstants.PRICE_ASCENDING_SORT_PARAMETER, minsale);
				priceMap.put(TRUCommerceConstants.PRICE_DESCENDING_SORT_PARAMETER, maxsale);
			} else {
				priceMap.put(TRUCommerceConstants.MIN_PRICE, minlist);
				priceMap.put(TRUCommerceConstants.MAX_PRICE, maxlist);
				priceMap.put(TRUCommerceConstants.PRICE_ASCENDING_SORT_PARAMETER, minlist);
				priceMap.put(TRUCommerceConstants.PRICE_DESCENDING_SORT_PARAMETER, maxlist);
			}
		}else{
			priceMap.put(TRUCommerceConstants.IS_PRICE_EXISTS,TRUCommerceConstants.FALSE_STRING); 
		}
		if(isLoggingDebug()){
			vlogDebug("priceMap:{0}", priceMap);
			logDebug("Exiting from [Class: TRUEndecaPriceUtils  method: getPriceMapForStylisedItems]");
		}
		return priceMap;
	}
	
	/**
	 * @param pParentSkuId - String - Default SKU Id.
	 * @param pProductId - String - Default Product Id
	 * @param pSite - Site Object
	 * @return Map - Price Map for Stylized Item.
	 */
	private Map<String, Object> getPriceMapForNonStylisedItems(String pParentSkuId, String pProductId, Site pSite) {
		if(isLoggingDebug()){
			logDebug("Enter into [Class: TRUEndecaPriceUtils  method: getPriceMapForNonStylisedItems]");
			vlogDebug("pParentSkuId : {0} pProductId : {1} pSite : {2}", pParentSkuId,pProductId,pSite);
		}
		Map<String, Object> priceMap = new HashMap<String, Object>();
		double salePrice = TRUConstants.DOUBLE_ZERO;
		double listPrice=TRUConstants.DOUBLE_ZERO;

		priceMap.put(TRUCommerceConstants.HAS_PRICE_RANGE, TRUCommerceConstants.FALSE_STRING);

		listPrice = (double) getPricingTools().getListPriceForSKU(pParentSkuId, pProductId, pSite);
		if(listPrice > TRUConstants.DOUBLE_ZERO) {
			priceMap.put(TRUCommerceConstants.SKU_LISTPRICE, listPrice);
		}

		salePrice = (double) getPricingTools().getSalePriceForSKU(pParentSkuId, pProductId, pSite);
		if(salePrice > TRUConstants.DOUBLE_ZERO) {
			priceMap.put(TRUCommerceConstants.SKU_SALEPRICE, salePrice);
		}

		boolean calculateSavings = getPricingTools().validateThresholdPrices(pSite, listPrice, salePrice);
		if (isLoggingDebug()) {
			vlogDebug("listPrice for sku id : {0} and saleprice for sku id : {1}  and Strike Thorugh Price : {2}" ,
					listPrice, salePrice, calculateSavings);
		}
		priceMap.put(TRUCommerceConstants.STRIKE_THROUGH, calculateSavings);
		Map<String, Double> savingsMap = getPricingTools().calculateSavings(listPrice, salePrice);

		if (calculateSavings && savingsMap != null) {
			priceMap.put(TRUConstants.SAVED_AMOUNT, savingsMap.get(TRUConstants.SAVED_AMOUNT));
			priceMap.put(TRUConstants.SAVED_PERCENTAGE, savingsMap.get(TRUConstants.SAVED_PERCENTAGE));
		} 

		if(salePrice > TRUConstants.DOUBLE_ZERO && (listPrice > salePrice||listPrice <=TRUConstants.DOUBLE_ZERO)) {
			priceMap.put(TRUCommerceConstants.PRICE_ASCENDING_SORT_PARAMETER, salePrice);
			priceMap.put(TRUCommerceConstants.PRICE_DESCENDING_SORT_PARAMETER, salePrice);
			priceMap.put(TRUCommerceConstants.IS_PRICE_EXISTS,TRUCommerceConstants.TRUE_STRING);
			priceMap.put(TRUCommerceConstants.SKU_ACTIVEPRICE,salePrice);
		}else {
			priceMap.put(TRUCommerceConstants.PRICE_ASCENDING_SORT_PARAMETER, listPrice);
			priceMap.put(TRUCommerceConstants.PRICE_DESCENDING_SORT_PARAMETER, listPrice);
			priceMap.put(TRUCommerceConstants.SKU_ACTIVEPRICE,listPrice); 
			if(listPrice == TRUConstants.DOUBLE_ZERO){
				priceMap.put(TRUCommerceConstants.IS_PRICE_EXISTS,TRUCommerceConstants.FALSE_STRING); 
			}else{
				priceMap.put(TRUCommerceConstants.IS_PRICE_EXISTS,TRUCommerceConstants.TRUE_STRING);
			}
		}
		if(isLoggingDebug()){
			vlogDebug("priceMap:{0}", priceMap);
			logDebug("Exiting from [Class: TRUEndecaPriceUtils  method: getPriceMapForNonStylisedItems]");
		}
		return priceMap;
	}

	/**
	 * @return the mSkuPricingCache
	 */
	public TRUSkuPricingCache getSkuPricingCache() {
		return mSkuPricingCache;
	}

	/**
	 * @param pSkuPricingCache the mSkuPricingCache to set
	 */
	public void setSkuPricingCache(TRUSkuPricingCache pSkuPricingCache) {
		this.mSkuPricingCache = pSkuPricingCache;
	}
	
	/**
	 * Method to generate the price Map for Endeca Pages.
	 * 
	 * @param pSkuProds
	 *            - String Array - SKU and Product Array
	 * @param pSkuId
	 *            the sku id
	 * @param pProductId
	 *            the product id
	 * @return - Map - Price Map
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getPriceDetails(String pSkuProds, String pSkuId, String pProductId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUEndecaPriceUtils  method: getPriceDetails]");
			logDebug("pSkuProds:::" + pSkuProds);
		}
		if (StringUtils.isEmpty(pSkuProds)) {
			return null;
		}
		Map<String, Object> skuPriceMap = null;
		if (getPricingTools().getTruConfiguration().isEnableSkuPricingCache()) {
			try {
				skuPriceMap = (Map<String, Object>) getSkuPricingCache().get(pSkuProds);
			} catch (TRUCommerceException e) {
				if (isLoggingError()) {
					vlogError("RepositoryException in TRUEndecaPriceUtils", e);
				}
			}
		} else {
			Site currentSite = SiteContextManager.getCurrentSite();
			skuPriceMap = getPriceMapForNonStylisedItems(pSkuId, pProductId, currentSite);
		}
		if (isLoggingDebug()) {
			vlogDebug("skuPriceMap:{0} for sku_prd : {1}", skuPriceMap);
		}

		if (isLoggingDebug()) {
			vlogDebug("priceMap:{0}", skuPriceMap);
			logDebug("Exiting from [Class: TRUEndecaPriceUtils  method: getPriceDetails]");
		}
		return skuPriceMap;
	}
}
