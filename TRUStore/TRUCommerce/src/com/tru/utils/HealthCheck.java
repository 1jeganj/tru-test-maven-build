
package com.tru.utils;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import atg.nucleus.GenericService;
import atg.service.jdbc.WatcherDataSource;

/**
 * Purpose of the class is to perform a check on the database connection
 * performs a query - select 1 from dual
 * returns FAILURE by default, unless successfully queried and closed statements/connection
 * @author lphan
 *
 */
public class HealthCheck extends GenericService {
	
	/**
	 * Constant for RESULT_CODE_FAILED.
	 */
	public static final String  RESULT_CODE_FAILED = "FAILED";
	
	/**
	 * Constant for RESULT_CODE_OK.
	 */
	public static final String  RESULT_CODE_OK = "OK";
	
	/**
	 * Constant for SQL_SELECT_DUAL_STMT.
	 */
	public static final String  SQL_SELECT_DUAL_STMT = "Select 1 from dual";
	
	/**
	 * property for mDataSource.
	 */
	WatcherDataSource mDataSource = null;
	
	/**
	 * @param pDataSource for pDataSource
	 */
	public void setDataSource(WatcherDataSource pDataSource) {
		mDataSource = pDataSource;
	}
	
	/**
	 * @return mDataSource
	 */
	public WatcherDataSource getDataSource() {
		return mDataSource; 
	}

	
	/**
	 * GetStatus method to check DB connection status.
	 * @return Result Code
	 */
	public String getStatus() {
		
		// the following code checks for db connection
		Connection conn = null;
		String resultCode = RESULT_CODE_FAILED;  //by default
		try {
			conn = mDataSource.getConnection();
			Statement stmt = conn.createStatement();
			stmt.execute (SQL_SELECT_DUAL_STMT);
			try {
				stmt.close();
			} catch (SQLException sqe) {
				if(isLoggingError()){
					logError("Unable to close healthcheck stmt connections: ", sqe);
				}
			} finally {
				conn.close();
				resultCode = RESULT_CODE_OK;
			}
		} catch (SQLException e) {
			resultCode = RESULT_CODE_FAILED;
			if(isLoggingError()){
				logError("Unable to close healthcheck db connections: ", e);
			}
		} 
		return resultCode;
	}
	
	
	/**
	 * setStatus method.
	 */
	public void setStatus() {
		if(isLoggingDebug()){
			logDebug("Entering to HealthCheck class : setStatus method. ");
		}
		if(isLoggingDebug()){
			logDebug("Exiting from HealthCheck class : setStatus method. ");
		}
	}
}
