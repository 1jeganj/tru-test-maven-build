package com.tru.utils.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.google.gson.Gson;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.common.TRUConstants;
import com.tru.integrations.registry.beans.Error;
import com.tru.integrations.registry.beans.ItemInfoRequest;
import com.tru.integrations.registry.beans.Locale;
import com.tru.integrations.registry.beans.Product;
import com.tru.integrations.registry.beans.RegistryResponse;
import com.tru.integrations.registry.beans.Validation;
import com.tru.integrations.registry.exception.RegistryIntegrationException;
import com.tru.integrations.registry.service.RegistryServiceImpl;

/**
 * This is a class for testing Registry serivces
 *
 */
public class TRUPDPRegistryTestDroplet extends DynamoServlet {
	
	private RegistryServiceImpl mRegistryServiceImpl;
	
	private String mRegistryId;
	
	private String mRegistryType;
	private boolean mGetMethod;
	
	private Map mProductDetails;
	
	/**
	 * Property to hold deletedFlag.
	 */
	
	private String mDeletedFlag;

	
	/**
	 * @return the deletedFlag
	 */
	public String getDeletedFlag() {
		return mDeletedFlag;
	}

	/**
	 * @param pDeletedFlag the deletedFlag to set
	 */
	public void setDeletedFlag(String pDeletedFlag) {
		mDeletedFlag = pDeletedFlag;
	}
	
	/**
	 * @return the productDetails
	 */
	public Map getProductDetails() {
		return mProductDetails;
	}

	/**
	 * @param pProductDetails the productDetails to set
	 */
	public void setProductDetails(Map pProductDetails) {
		mProductDetails = pProductDetails;
	}

	/**
	 * @return the getMethod
	 */
	public boolean isGetMethod() {
		return mGetMethod;
	}

	/**
	 * @param pGetMethod the getMethod to set
	 */
	public void setGetMethod(boolean pGetMethod) {
		mGetMethod = pGetMethod;
	}

	/**
	 * @return the registryId
	 */
	public String getRegistryId() {
		return mRegistryId;
	}

	/**
	 * @param pRegistryId the registryId to set
	 */
	public void setRegistryId(String pRegistryId) {
		mRegistryId = pRegistryId;
	}

	/**
	 * @return the registryType
	 */
	public String getRegistryType() {
		return mRegistryType;
	}

	/**
	 * @param pRegistryType the registryType to set
	 */
	public void setRegistryType(String pRegistryType) {
		mRegistryType = pRegistryType;
	}

	/**
	 * @return the registryServiceImpl
	 */
	public RegistryServiceImpl getRegistryServiceImpl() {
		return mRegistryServiceImpl;
	}

	/**
	 * @param pRegistryServiceImpl the registryServiceImpl to set
	 */
	public void setRegistryServiceImpl(RegistryServiceImpl pRegistryServiceImpl) {
		mRegistryServiceImpl = pRegistryServiceImpl;
	}


	/**
	 * This method will get the product related information required for registry.
	 * 
	 * @param pRequest - reference to DynamoHttpServletRequest object.
	 * @param pResponse - reference to DynamoHttpServletResponse object.
	 * @throws ServletException - if any exception occurs in servicing the request.
	 * @throws IOException - if any exception occurs while writing the response to output stream.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUPDPRegistryTest.service method.. ");
		}
		RegistryResponse registryResponse=null;
		Gson gson =new Gson();
		Validation validation= new Validation();
		Error error=new Error();
	
			try {
				if(isGetMethod()){
					registryResponse=getWishlistItems();
				vlogDebug("TRUPDPRegistryTest.service method:: call Get Method",registryResponse);
				}else{
					registryResponse=testAddItemToRegistry();
					vlogDebug("TRUPDPRegistryTest.service method:: call Add Method",registryResponse);
				}
			} catch (RegistryIntegrationException e) {
				vlogError("Error TRUPDPRegistryTest.Service ", e);
			}
			if(registryResponse==null){
				vlogError("Error TRUPDPRegistryTest.Service Respionse is null");
				registryResponse= new RegistryResponse();
				//registryResponse.setRegistryNumber(registryNumber);
				registryResponse.setValidation(validation);
				registryResponse.getValidation().setError(error);
				registryResponse.getValidation().getError().setInformational(TRUConstants.REGISTRY_ERROR);
			}
			String response=gson.toJson(registryResponse);
			pRequest.setParameter(TRUConstants.RESPONSE, response);
			pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
			if (isLoggingDebug()) {
				logDebug("END:: TRUPDPRegistryTest.service method.. ");
			}
	}
	
	/**
	 * This method test Registry Service.
	 * 
	 * @return RegistryResponse of response
	 * @throws RegistryIntegrationException  of RegistryIntegrationException.
	 */
	public RegistryResponse getWishlistItems() throws  RegistryIntegrationException {
		if (isLoggingDebug()) {
			vlogDebug("RegistryTest :: addItemToRegistry :: STARTS");
		}
		RegistryResponse response=mRegistryServiceImpl.getWishlistItems(mRegistryId, mDeletedFlag);
		if (isLoggingDebug()) {
			vlogDebug("RegistryTest :: addItemToRegistry :: ENDS");
		}
		return response;
	}
	
	/**
	 * This method is use to update Registry items.
	 * 
	 * @return response.
	 * @throws RegistryIntegrationException of RegistryIntegrationException.
	 */
	public RegistryResponse testAddItemToRegistry() throws  RegistryIntegrationException {
		if (isLoggingDebug()) {
			vlogDebug("RegistryTest :: addItemToRegistry :: STARTS");
		}
		RegistryResponse response=mRegistryServiceImpl.addItemToRegistry(createAddItemRequest());
		if (isLoggingDebug()) {
			vlogDebug("RegistryTest :: addItemToRegistry :: ENDS");
		}
		return response;
	}
	
	/**
	 * This method gives ItemInfoRequest object
	 * @return itemInfo of ItemInfoRequest
	 */
	private ItemInfoRequest createAddItemRequest(){
		if (isLoggingDebug()) {
			vlogDebug("RegistryTest :: createAddItemRequest :: STARTS");
		}
		ItemInfoRequest itemInfo = new ItemInfoRequest();
		itemInfo.setRegistryNumber(getRegistryId());
		itemInfo.setRegistryType(getRegistryType());
		List<Product> products = new ArrayList<Product>();
		Product product = new Product();
		Map productDetails=getProductDetails();
		if(productDetails!=null){
		product.setSkn((String) productDetails.get(TRUConstants.SKN));
		product.setUpc((String) productDetails.get(TRUConstants.UPC));
		product.setUid((String) productDetails.get(TRUConstants.UID));
		product.setColor((String) productDetails.get(TRUCommerceConstants.COLOR));
		product.setSize((String) productDetails.get(TRUCommerceConstants.SIZE));
		product.setSknOrigin((String) productDetails.get(TRUConstants.SKN_ORIGIN));
		product.setNonSpecificIndicator((String) productDetails.get(TRUConstants.NON_SPECIFIC_INDICATOR));
		product.setItemRequested((String) productDetails.get(TRUConstants.ITEM_REQUESTED));
		product.setItemPurchased((String) productDetails.get(TRUConstants.ITEM_PURCHASED));
		product.setPurchaseUpdateIndicator((String) productDetails.get(TRUConstants.PURCHASE_UPDATE_INDICATOR));
		product.setProductDescription((String) productDetails.get(TRUConstants.PRODUCT_DESCRIPTION));
		}
		products.add(product);
		itemInfo.setProduct(products);
		itemInfo.setMessageId(TRUConstants.ABCD);
		Locale locale = new Locale();
		locale.setCountry(TRUConstants.UNITED_STATES);
		locale.setLanguage(TRUConstants.ENGLISH);
		itemInfo.setLocale(locale);
		if (isLoggingDebug()) {
			vlogDebug("RegistryTest :: createAddItemRequest :: ENDS");
		}
		return itemInfo;
	}

}
