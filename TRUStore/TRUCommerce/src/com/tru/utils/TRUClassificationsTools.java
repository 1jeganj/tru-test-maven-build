/*
 * @(#) TRUClassificationsTools.java
 * 
 * Copyright 2016 PA, Inc. All rights reserved. PA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.tru.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.commerce.catalog.custom.CatalogProperties;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.repository.MutableRepository;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.vo.ClassificationUtilKeyVO;
import com.tru.commerce.catalog.vo.TRUClassificationMediaVO;
import com.tru.commerce.catalog.vo.TRUClassificationVO;
import com.tru.common.TRUClassificationConfiguration;
import com.tru.common.TRUStoreConfiguration;


/**
 * This class will execute at server startup ,to get the all classification ( which display status is ACTIVE and isDeleted is false)
 * and associated properties for all the Active site.
 *
 * @author PA
 * @version 1.0
 */
public class TRUClassificationsTools extends GenericService {


	/** 
	 * The product catalog. 
	 */
	private MutableRepository mProductCatalog;

	/** 
	 * The endeca Configuration constant.
	 */
	private TRUClassificationConfiguration mClassificationConfiguration;

	/** 
	 * The List of Root Categories for Site.
	 */
	private Map<String, List<String>> mRootCategoryForSite;

	/** 
	 * The AllClassificationMap.
	 */
	private Map<ClassificationUtilKeyVO, TRUClassificationVO> mAllClassificationMap;

	/**
	 * property to hold mCategoryImageURLRepository.
	 */
	private Repository mCategoryImageURLRepository;
	
	/**
	 * property to hold mImageUrl.
	 */
	private String mImageUrl;
	
	/**
	 * This property hold reference for CatalogProperties.
	 */
	private CatalogProperties mCatalogProperties;
		
	/**
	 * This property hold reference for mTRUStoreConfiguration.
	 */
	private TRUStoreConfiguration mTRUStoreConfiguration;
	

	/** The m classification with condition flag. */
	private boolean mClassificationWithConditionFlag;

	/**
	 * Gets the category image url repository.
	 *
	 * @return the category image url repository
	 */
	public Repository getCategoryImageURLRepository() {
		return mCategoryImageURLRepository;
	}

	/**
	 * Sets the category image url repository.
	 *
	 * @param pCategoryImageURLRepository the new category image url repository
	 */
	public void setCategoryImageURLRepository(
			Repository pCategoryImageURLRepository) {
		mCategoryImageURLRepository = pCategoryImageURLRepository;
	}

	/**
	 * Gets the root category for site.
	 *
	 * @return the RootCategoryForSite.
	 */
	public Map<String, List<String>> getRootCategoryForSite() {
		return mRootCategoryForSite;
	}

	/**
	 * Sets the root category for site.
	 *
	 * @param pRootCategoryForSite the Map to set
	 */
	public void setRootCategoryForSite(Map<String, List<String>> pRootCategoryForSite) {
		mRootCategoryForSite = pRootCategoryForSite;
	}

	/**
	 * Gets the all classification map.
	 *
	 * @return the AllClassificationMap
	 */
	public Map<ClassificationUtilKeyVO, TRUClassificationVO> getAllClassificationMap() {
		return mAllClassificationMap;
	}

	/**
	 * Sets the all classification map.
	 *
	 * @param pAllClassificationMap the AllClassificationMap to set
	 */
	public void setAllClassificationMap(Map<ClassificationUtilKeyVO, TRUClassificationVO> pAllClassificationMap) {
		mAllClassificationMap = pAllClassificationMap;
	}

	/**
	 * Gets the classification configuration.
	 *
	 * @return the classification configuration
	 */
	public TRUClassificationConfiguration getClassificationConfiguration() {
		return mClassificationConfiguration;
	}

	/**
	 * Sets the classification configuration.
	 *
	 * @param pClassificationConfiguration the new classification configuration
	 */
	public void setClassificationConfiguration(
			TRUClassificationConfiguration pClassificationConfiguration) {
		this.mClassificationConfiguration = pClassificationConfiguration;
	}
	/**
	 * Gets the product catalog.
	 *
	 * @return the productCatalog
	 */
	public MutableRepository getProductCatalog() {
		return mProductCatalog;
	}

	/**
	 * Sets the product catalog.
	 *
	 * @param pProductCatalog the productCatalog to set
	 */
	public void setProductCatalog(MutableRepository pProductCatalog) {
		mProductCatalog = pProductCatalog;
	}

	/**
	 * Returns the this property hold reference for CatalogProperties.
	 * 
	 * @return the catalogProperties
	 */
	public CatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the this property hold reference for CatalogProperties.
	 * 
	 * @param pCatalogProperties the catalogProperties to set
	 */
	public void setCatalogProperties(CatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}


	/**
	 * Checks if is classification with condition flag.
	 *
	 * @return the classificationWithConditionFlag
	 */
	public boolean isClassificationWithConditionFlag() {
		return mClassificationWithConditionFlag;
	}

	/**
	 * Sets the classification with condition flag.
	 *
	 * @param pClassificationWithConditionFlag the classificationWithConditionFlag to set
	 */
	public void setClassificationWithConditionFlag(
			boolean pClassificationWithConditionFlag) {
		mClassificationWithConditionFlag = pClassificationWithConditionFlag;
	}

	/**
	 * Gets the image url.
	 *
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return mImageUrl;
	}

	/**
	 * Sets the image url.
	 *
	 * @param pImageUrl the imageUrl to set
	 */
	public void setImageUrl(String pImageUrl) {
		mImageUrl = pImageUrl;
	}
	
	/**
	 * Called after the service has been created and create security token.
	 *
	 * @exception ServiceException
	 *                if the service had a problem starting up
	 */
	public void doStartService() throws ServiceException {
		if (isLoggingDebug()){
			logDebug("BEGIN:: TRUClassificationsTools.doStartService()");
		}
		super.doStartService();
		utilClassificationMapData();
		if (isLoggingDebug()){
			logDebug("END:: TRUClassificationsTools.doStartService()");
		}
	}

	/**
	 * The reset method will be called to flush the Cache.
	 *
	 */
	public void flush() {
		if (isLoggingDebug()){
			logDebug("BEGIN:: TRUClassificationsTools.reset()");
		}
		utilClassificationMapData();
		if (isLoggingDebug()){
			logDebug("END:: TRUClassificationsTools.reset()");
		}
	}


	/**
	 * The utilClassificationMapData method will set the AllClassification map with the 
	 * Map of All Classifications.
	 *
	 */
	private void utilClassificationMapData() {
		if (isLoggingDebug()){
			logDebug("BEGIN:: TRUClassificationsTools.utilClassificationMapData()");
		}

		List<RepositoryItem> allRootCategories = getAllRootCategories();
		Map<ClassificationUtilKeyVO, TRUClassificationVO> allClassifications = new HashMap<ClassificationUtilKeyVO, TRUClassificationVO>();
		if(allRootCategories != null && !allRootCategories.isEmpty()) {
			for(RepositoryItem rootCategory : allRootCategories) {

				TRUClassificationVO classificationVO = createClassificationVOForCategory(rootCategory);
				ClassificationUtilKeyVO classificationKey = new ClassificationUtilKeyVO();
				classificationKey.setClassificationId(rootCategory.getRepositoryId());
				classificationKey.setRootCategoryId(rootCategory.getRepositoryId());
				classificationKey.setLevel(TRUCommerceConstants.INT_ZERO);
				if(classificationKey != null && classificationVO != null) {
					if (isLoggingDebug()){
						vlogDebug("Adding the Root Category with ID {0} to the allClassifications Map"
								,rootCategory.getRepositoryId());
					}
					allClassifications.put(classificationKey, classificationVO);
				}

				getAllClassifications(allClassifications, rootCategory, rootCategory.getRepositoryId(), TRUCommerceConstants.INT_ONE);
			}
		}
		setAllClassificationMap(allClassifications);
		if (isLoggingDebug()){
			logDebug("END:: TRUClassificationsTools.utilClassificationMapData()");
		}
	}

	/**
	 * The getAllRootCategories method will provide all the Root Categories by querying the db to get all
	 * Catalogs and getting the Root Categories from each of the Catalogs.
	 * 
	 * @return  rootCategoryItemList
	 *            - List of Root Category Repository Items.
	 * 
	 */

	@SuppressWarnings("unchecked")
	private List<RepositoryItem> getAllRootCategories() {

		if (isLoggingDebug()){
			logDebug("BEGIN:: TRUClassificationsTools.getAllRootCategories()");
		}
		RepositoryItem[] catalogItems= null;
		List<RepositoryItem> rootCategoryItemList = new ArrayList<RepositoryItem>();
		Map<String,List<String>> rootCategoriesMappingForSite = new HashMap<String, List<String>>();
		try {
			Repository productCatalogRepository= getProductCatalog();
			RepositoryItemDescriptor catalogDescritor = productCatalogRepository.getItemDescriptor(getCatalogProperties()
					.getCatalogItemName());
			RepositoryView catalogRepositoryView = catalogDescritor.getRepositoryView();
			if (catalogRepositoryView != null ) {
				QueryBuilder catalogQueryBuilder = catalogRepositoryView.getQueryBuilder();
				Query query = catalogQueryBuilder.createUnconstrainedQuery();
				catalogItems = catalogRepositoryView.executeQuery(query);
			}
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				vlogError(repositoryException,"Repository Exception occured while fetching the list of Catalogs "+
						 "TRUClassificationsTools.getAllRootCategories()");		
			}
		}

		if(catalogItems != null && catalogItems.length > TRUCommerceConstants.INT_ZERO) {
			for(RepositoryItem catalogItem : catalogItems) {

				Set<String> siteIds = (Set<String>) catalogItem.getPropertyValue(getCatalogProperties().getSitesPropertyName());

				Set<RepositoryItem> rootCategories = (Set<RepositoryItem>) catalogItem.getPropertyValue(getCatalogProperties()
						.getAllRootCategoriesPropertyName());
				if(!rootCategories.isEmpty()) {
					List<String> rootCategoryIdList = new ArrayList<String>();
					for(RepositoryItem rootCategory : rootCategories) {
						rootCategoryItemList.add(rootCategory);
						rootCategoryIdList.add(rootCategory.getRepositoryId());
						if (isLoggingDebug()){
							vlogDebug("Added the Root Category with Id : {0} to the list of Root Categories."
									,rootCategory.getRepositoryId());
						}
					}
					if(siteIds != null && !siteIds.isEmpty() && !rootCategoryIdList.isEmpty()) {
						for(String siteId : siteIds) {
							rootCategoriesMappingForSite.put(siteId, rootCategoryIdList);
						}
					}
				}
			}
		}
		if(rootCategoriesMappingForSite != null && !rootCategoriesMappingForSite.isEmpty()) {
			setRootCategoryForSite(rootCategoriesMappingForSite);
		}
		if (isLoggingDebug()){
			logDebug("END:: TRUClassificationsTools.getAllRootCategories()");
		}
		return rootCategoryItemList;
	}


	/**
	 * The getAllClassificationMap method will call recursively to get all the All the Classifications
	 * and set it to the All Classification map.
	 *
	 * @param pAllClassifications the all classifications
	 * @param pCurrentParentCategory the current parent category
	 * @param pRootCategoryId the root category id
	 * @param pLevel the level
	 */

	@SuppressWarnings("unchecked")
	private void getAllClassifications(Map<ClassificationUtilKeyVO, TRUClassificationVO> pAllClassifications, 
			RepositoryItem pCurrentParentCategory, String pRootCategoryId, int pLevel) {

		if (isLoggingDebug()){
			logDebug("BEGIN:: TRUClassificationsTools.getAllClassifications()");
		}
		String parentCategoryId = pCurrentParentCategory.getRepositoryId();
		if(pCurrentParentCategory != null && pLevel <= getTRUStoreConfiguration().getMaximumCategoryLevel()) {
			List<RepositoryItem> childCategories = (List<RepositoryItem>) pCurrentParentCategory.getPropertyValue(getCatalogProperties()
					.getFixedChildCategoriesPropertyName());
			if(childCategories == null || childCategories.isEmpty()) {
				return;
			}
			String type=null;
			for(RepositoryItem childCategory : childCategories) {
				type= (String)childCategory.getPropertyValue(getClassificationConfiguration().getType());
				if(!StringUtils.isBlank(type) && 
						(type.equalsIgnoreCase(TRUCommerceConstants.CLASSIFICATION) ||
								type.equalsIgnoreCase(TRUCommerceConstants.REGISTRY_CLASSIFICATION))){
					TRUClassificationVO classificationVO = createClassificationVO(childCategory, parentCategoryId);
					ClassificationUtilKeyVO classificationKey = new ClassificationUtilKeyVO();
					classificationKey.setParentClassificationId(parentCategoryId);
					classificationKey.setClassificationId(childCategory.getRepositoryId());
					classificationKey.setRootCategoryId(pRootCategoryId);
					classificationKey.setLevel(pLevel);
					if(classificationVO != null && classificationKey != null) {
						if (isLoggingDebug()){
							vlogDebug("Adding the Classification with ID {0} to the allClassifications Map at " +
									"level {1} with Root Category Id {2}",childCategory.getRepositoryId(), pLevel, pRootCategoryId);
						}
						pAllClassifications.put(classificationKey, classificationVO);
						getAllClassifications(pAllClassifications, childCategory, pRootCategoryId, pLevel+TRUCommerceConstants.INT_ONE);
					}
				}

			}
		}
		if (isLoggingDebug()){
			logDebug("END:: TRUClassificationsTools.getAllClassifications()");
		}
	}

	/**
	 * Creates the classification vo from the Category Repository Item.
	 *
	 * @param pCategoryItem the classification item
	 * @return the tRU classification vo
	 */
	@SuppressWarnings("unchecked")
	private TRUClassificationVO createClassificationVOForCategory(RepositoryItem pCategoryItem)
	{
		if (isLoggingDebug()){
			logDebug("BEGIN:: TRUClassificationsTools.createClassificationVOForCategory()");
		}
		TRUClassificationVO truClassificationVO = new TRUClassificationVO();
		truClassificationVO.setClaasificationId(pCategoryItem.getRepositoryId());
		String classificationName =(String) pCategoryItem.getItemDisplayName();
		List<RepositoryItem> childClassficationList =(List<RepositoryItem>) pCategoryItem.getPropertyValue(
				getCatalogProperties().getFixedChildCategoriesPropertyName());

		List<String> childClassficationIds = new ArrayList<String>();

		for (RepositoryItem childClassfication : childClassficationList) {
			String childClassficationId = childClassfication.getRepositoryId();
			childClassficationIds.add(childClassficationId);
		}

		truClassificationVO.setClaasificationName(classificationName);
		truClassificationVO.setChildClassifications(childClassficationIds);
		if (isLoggingDebug()){
			vlogDebug("END:: TRUClassificationsTools.createClassificationVOForCategory(), truClassificationVO::: {0}",truClassificationVO);
		}
		return truClassificationVO;
	}



	/**
	 * Creates the classification vo.
	 *
	 * @param pClassificationItem the classification item
	 * @param pParentCategoryId the parent category id
	 * @return the tRU classification vo
	 */
	@SuppressWarnings("unchecked")
	private TRUClassificationVO createClassificationVO(RepositoryItem pClassificationItem, String pParentCategoryId) {
		if (isLoggingDebug()){
			logDebug("BEGIN:: TRUClassificationsTools.createClassificationVO()");
		}
		boolean dispalyStatusCheck = false;
		String categoryImageURL = null;
		TRUClassificationVO truClassificationVO = new TRUClassificationVO();
		truClassificationVO.setClaasificationId(pClassificationItem.getRepositoryId());
		Map<String,TRUClassificationMediaVO> relatedMediaMap = new HashMap<String,TRUClassificationMediaVO>();
		
		Map<String, String> crossRefDisplayNameMap =(Map<String, String>) pClassificationItem.getPropertyValue(getClassificationConfiguration().getClassificationCrossRefDisplayName());
		
		Map<String,String> crossRefDisplayOrderMap=(Map<String, String>)pClassificationItem.getPropertyValue(getClassificationConfiguration().getClassificationCrossRefDisplayOrder());
		String crossRefDisplay=crossRefDisplayNameMap.get(pParentCategoryId);
		String crossRefDisplayOrder=crossRefDisplayOrderMap.get(pParentCategoryId);
		String classificationName=null;
		int displayOrder = TRUCommerceConstants.INT_9999;
		if(StringUtils.isNotEmpty(crossRefDisplay)) {
			classificationName = crossRefDisplay;
		} else {
			classificationName = (String) pClassificationItem.getItemDisplayName();
		}
		
		if(crossRefDisplayOrder!=null && !crossRefDisplayOrder.isEmpty()){
			displayOrder=Integer.parseInt(crossRefDisplayOrder);
		}else{
			Integer displayOrderObj = 	(Integer)pClassificationItem.getPropertyValue(getClassificationConfiguration().getClassificationDisplayOrder());
			if(displayOrderObj != null)	{
				displayOrder = displayOrderObj.intValue();
			}
		}
	
		List<RepositoryItem> childClassficationList =(List<RepositoryItem>) pClassificationItem.getPropertyValue(
				getCatalogProperties().getFixedChildCategoriesPropertyName());

		List<String> childClassficationIds = new ArrayList<String>();

		for (RepositoryItem childClassfication : childClassficationList) {
			String childClassficationId = childClassfication.getRepositoryId();
			childClassficationIds.add(childClassficationId);
		}

		String displayStatus = (String) pClassificationItem.getPropertyValue(
				getClassificationConfiguration().getClassificationDisplayStatus());
		boolean isDeleted = (Boolean) pClassificationItem.getPropertyValue(getClassificationConfiguration().getClassificationIsDeleted());

		//Begin:: Added for Classification Display Order
		/*Integer displayOrderObj = 	(Integer)pClassificationItem.getPropertyValue(
				getClassificationConfiguration().getClassificationDisplayOrder());*/
		//int displayOrder = TRUCommerceConstants.INT_9999;
		/*if(displayOrderObj != null)	{
			displayOrder = displayOrderObj.intValue();
		}*/
		//End:: Added for Classification Display Order

		if(!StringUtils.isEmpty(displayStatus)){
			dispalyStatusCheck = (displayStatus.equalsIgnoreCase(TRUCommerceConstants.HIDDEN));
		}

		if(isClassificationWithConditionFlag() && (isDeleted || dispalyStatusCheck)) {
			return null;
		}
		String classificationUserType = (String) pClassificationItem.getPropertyValue(
				getClassificationConfiguration().getClassificationUserTypeId());
		String classificationLongDesc = (String) pClassificationItem.getPropertyValue(
				getClassificationConfiguration().getClassificationLongDescription());

		//Populate the Media Content For the Classification
		Map<String,RepositoryItem> relatedMediaContents = (Map<String, RepositoryItem>) pClassificationItem.getPropertyValue(
				getClassificationConfiguration().getClassificationRrelatedMediaContent());
		if(relatedMediaContents != null) {
			Set<String> relatedMediaContentKeys = relatedMediaContents.keySet();
			if(relatedMediaContentKeys != null && !relatedMediaContentKeys.isEmpty()) {
				for (String relatedMediaContentKey : relatedMediaContentKeys) {
						RepositoryItem relatedMediaContent = relatedMediaContents.get(relatedMediaContentKey);
						TRUClassificationMediaVO mediaVO = createClassificationMediaVO(relatedMediaContent);
						if(mediaVO != null) {
							relatedMediaMap.put(relatedMediaContentKey, mediaVO);
					}
				}
			}
		}

		//Populate the Category Image Url
		if(pClassificationItem != null && pClassificationItem.getRepositoryId() != null) {
			categoryImageURL = (String)getCategoryImageUrl(pClassificationItem.getRepositoryId());
		}
		if(!StringUtils.isEmpty(categoryImageURL)) {
			truClassificationVO.setClassificationImageUrl(categoryImageURL);
		}
		if(StringUtils.isNotBlank(displayStatus)) {
			truClassificationVO.setClassificationDisplayStatus(displayStatus);
		}
		truClassificationVO.setClaasificationName(classificationName);
		truClassificationVO.setChildClassifications(childClassficationIds);
		truClassificationVO.setClassificationLongDesc(classificationLongDesc);
		truClassificationVO.setUserType(classificationUserType);
		truClassificationVO.setRelatedMediaContent(relatedMediaMap);
		truClassificationVO.setClassificationDisplayOrder(displayOrder);
		if (isLoggingDebug()){
			vlogDebug("END:: TRUClassificationsTools.createClassificationVO(), truClassificationVO::: {0}",truClassificationVO);
		}
		return truClassificationVO;
	}


	/**
	 * Creates the classification media vo.
	 *
	 * @param pRealtedMediaContentItem the realted media content item
	 * @return the tRU classification media vo
	 */
	private TRUClassificationMediaVO createClassificationMediaVO(RepositoryItem pRealtedMediaContentItem)
	{
		if (isLoggingDebug()){
			logDebug("BEGIN:: TRUClassificationsTools.createClassificationMediaVO()");
		}
		
		if(pRealtedMediaContentItem != null) {
			TRUClassificationMediaVO mediaVO = new TRUClassificationMediaVO();
			String mediaId = pRealtedMediaContentItem.getRepositoryId();
			String mediaName = (String) pRealtedMediaContentItem.getPropertyValue(getClassificationConfiguration().getMediaName());
			String mediaType = (String) pRealtedMediaContentItem.getPropertyValue(getClassificationConfiguration().getMediaType());
			String mediaURL = (String) pRealtedMediaContentItem.getPropertyValue(getClassificationConfiguration().getMediaUrl());
			String mediaHTMLContent = (String) pRealtedMediaContentItem.getPropertyValue(
					getClassificationConfiguration().getMediaHtmlContent());
			mediaVO.setMediaId(mediaId);
			mediaVO.setMediaName(mediaName);
			mediaVO.setMediaType(mediaType);
			mediaVO.setMediaURL(mediaURL);
			mediaVO.setMediaHTMLContent(mediaHTMLContent);
			if (isLoggingDebug()){
				vlogDebug("TRUClassificationsTools.createClassificationMediaVO(), mediaVO::: {0}",mediaVO);
			}
			return mediaVO;
		}
		
		if (isLoggingDebug()){
			logDebug("END:: TRUClassificationsTools.createClassificationMediaVO(), mediaVO::: ");
		}
		return null;
		
	}

	/**
	 * Return the Root Category Id associated with the Site Id.
	 * @param pSiteId the String
	 *
	 * @return the rootCategoryId
	 */
	public String getRootCategoryIdForSite(String pSiteId) {
		String rootCategoryId = null;
		if(getRootCategoryForSite() != null && !StringUtils.isEmpty(pSiteId)) {
			List<String> rootCategoryIds = getRootCategoryForSite().get(pSiteId);
			if(rootCategoryIds != null && !rootCategoryIds.isEmpty()) {
				rootCategoryId = rootCategoryIds.get(TRUCommerceConstants.INT_ZERO);
			}
		}
		return rootCategoryId;
	}

	/**
	 * Return the Root Category image Url associated with the Category Id.
	 * @param pRepositoryId the String
	 *
	 * @return the catImageUrl
	 */
	public String getCategoryImageUrl(String pRepositoryId) {
		if (isLoggingDebug()){
			logDebug("BEGIN:: TRUClassificationsTools.getCategoryImageUrl(-)");
		}
		String catImageUrl = null;
		RepositoryItem [] items = null;
		if(!StringUtils.isEmpty(pRepositoryId)) {
			try {
				RepositoryView view = getCategoryImageURLRepository().getView(TRUCommerceConstants.CATEGORY_IMAGE_URL);
				RqlStatement statement = RqlStatement.parseRqlStatement(TRUCommerceConstants.CATEGORYID_EQUALS_QUESTION_ZERO);
				Object params[] = new Object[TRUCommerceConstants.INT_ONE];
				params[TRUCommerceConstants.INT_ZERO] = pRepositoryId;
				if(statement != null) {
					items = statement.executeQuery (view, params);
				}
				if(items != null) {
					for (RepositoryItem repositoryItem : items) {
						catImageUrl = (String)repositoryItem.getPropertyValue(getImageUrl());
					}
				}
			} catch (RepositoryException e) {
					vlogError(e, "RepositoryException occured in TRUClassificationsTools.getCategoryImageUrl()");
			}
		}
		if (isLoggingDebug()){
			logDebug("END:: TRUClassificationsTools.getCategoryImageUrl(), catImageUrl::: "+catImageUrl);
		}
		return catImageUrl;
	}

	/**
	 * @return the tRUStoreConfiguration
	 */
	public TRUStoreConfiguration getTRUStoreConfiguration() {
		return mTRUStoreConfiguration;
	}

	/**
	 * @param pTRUStoreConfiguration the tRUStoreConfiguration to set
	 */
	public void setTRUStoreConfiguration(TRUStoreConfiguration pTRUStoreConfiguration) {
		mTRUStoreConfiguration = pTRUStoreConfiguration;
	}


}

