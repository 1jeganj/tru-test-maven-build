package com.tru.commerce.cart.utils;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import atg.commerce.CommerceException;
import atg.commerce.inventory.InventoryException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemManager;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.cart.helper.TRUCartModifierHelper;
import com.tru.commerce.cart.vo.TRUCartItemDetailVO;
import com.tru.commerce.cart.vo.TRUStoreVO;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.inventory.TRUCoherenceInventoryManager;
import com.tru.commerce.inventory.TRUInventoryInfo;
import com.tru.commerce.locations.TRUStoreLocatorTools;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUChannelInStorePickupShippingGroup;
import com.tru.commerce.order.TRUCommerceItemImpl;
import com.tru.commerce.order.TRUCommerceItemMetaInfo;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUOutOfStockItem;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.purchase.TRUAddCommerceItemInfo;
import com.tru.commerce.order.purchase.TRUShippingProcessHelper;
import com.tru.commerce.order.vo.TRUGiftItemInfo;
import com.tru.commerce.order.vo.TRUGiftOptionsInfo;
import com.tru.commerce.order.vo.TRUGiftWrapItemInfo;
import com.tru.commerce.order.vo.TRUShipGroupKey;
import com.tru.commerce.order.vo.TRUShipmentVO;
import com.tru.commerce.order.vo.TRUShippingDestinationVO;
import com.tru.commerce.order.vo.TRUStorePickUpInfo;
import com.tru.commerce.pricing.TRUItemPriceInfo;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.commerce.pricing.TRUShipMethodVO;
import com.tru.commerce.pricing.TRUShippingPriceInfo;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.cml.SystemException;
import com.tru.common.vo.TRUStoreItemVO;
import com.tru.endeca.utils.TRUProductPageUtil;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.utils.TRUGetSiteTypeUtil;

/**
 * The Class TRUShoppingCartUtils.
 *
 * @author PA
 * @version 1.0
 */
public class TRUShoppingCartUtils extends GenericService {

	
	/** Holds the mLocationTools. */
	
	private TRUStoreLocatorTools mLocationTools;
	
	/** Property To Hold commerce item manager. */
	private CommerceItemManager mCommerceItemManager;

	/** Property To property manager. */
	private TRUCommercePropertyManager mPropertyManager;
	/** Property To mCoherenceInventoryManager . */
	private TRUCoherenceInventoryManager mCoherenceInventoryManager;

	/** Property to Hold Error handler manager. */
	private DefaultErrorHandlerManager mErrorHandlerManager;

	/** property: Reference to the ShippingProcessHelper component. */
	private TRUShippingProcessHelper mShippingHelper;

	/** Property To Hold location repository. */
	private Repository mLocationRepository;

	/** Holds reference for TRUCatalogProperties. */
	private TRUCatalogProperties mCatalogProperties;

	/** Property to hold is TRUConfiguration. */
	private TRUConfiguration mConfiguration;

	/** The TRU cart modifier helper. */
	private TRUCartModifierHelper mTRUCartModifierHelper;
	
	/** Holds the mSiteUtil. */
	private TRUGetSiteTypeUtil mSiteUtil;

	/** The m product page util. */
	private TRUProductPageUtil mProductPageUtil;
	
	/**
	 * This method is used to get locationTools.
	 * @return locationTools TRUStoreLocatorTools
	 */
	public TRUStoreLocatorTools getLocationTools() {
		return mLocationTools;
	}

	/**
	 *This method is used to set locationTools.
	 *@param pLocationTools TRUStoreLocatorTools
	 */
	public void setLocationTools(TRUStoreLocatorTools pLocationTools) {
		mLocationTools = pLocationTools;
	}

	/**
	 * Gets the catalog properties.
	 * 
	 * @return the catalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the catalog properties.
	 * 
	 * @param pCatalogProperties
	 *            the catalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}

	/**
	 * Gets the coherence inventory manager.
	 * 
	 * @return the coherence inventory manager
	 */
	public TRUCoherenceInventoryManager getCoherenceInventoryManager() {
		return mCoherenceInventoryManager;
	}

	/**
	 * Sets the coherence inventory manager.
	 * 
	 * @param pCoherenceInventoryManager
	 *            the CoherenceInventoryManager to set
	 */
	public void setCoherenceInventoryManager(TRUCoherenceInventoryManager pCoherenceInventoryManager) {
		mCoherenceInventoryManager = pCoherenceInventoryManager;
	}

	/**
	 * Gets the shipping helper.
	 * 
	 * @return the shippingHelper
	 */
	public TRUShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * Sets the shipping helper.
	 * 
	 * @param pShippingHelper
	 *            the shippingHelper to set
	 */
	public void setShippingHelper(TRUShippingProcessHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}

	/**
	 * Builds the shopping cart v os. This method is used to iterate the items in order and build respective vo for individual
	 * item.
	 *
	 * @param pOrder            the order
	 * @param pFromOverlay            the from overlay
	 * @param pIsAPIRequest            the is api request
	 * @param pCookieLocationId            the cookie location id
	 * @param pIsCSCRequest the is csc request
	 * @return the item detail v os by relation ships
	 */
	@SuppressWarnings("rawtypes")
	public List<TRUCartItemDetailVO> getItemDetailVOsByRelationShips(Order pOrder, String pFromOverlay, boolean pIsAPIRequest,
			String pCookieLocationId, boolean pIsCSCRequest) {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUShoppingCartUtils.getItemDetailVOsByRelationShips :: START");
		}
		final List<TRUCartItemDetailVO> shoppingCartList = new ArrayList<TRUCartItemDetailVO>();
		final List<TRUCartItemDetailVO> outOfStockItemList = new ArrayList<TRUCartItemDetailVO>();
		final List<TRUCartItemDetailVO> donationItemList = new ArrayList<TRUCartItemDetailVO>();
		List sgCiRels = null;
		TRUCartItemDetailVO cartItemDetailVO = null;
		final Map<String, Long> skuIdAndLocIdMap = new HashMap<String, Long>();
		final List<TRUInventoryInfo> inventoryRequest = new ArrayList<TRUInventoryInfo>();
		List<TRUInventoryInfo> inventoryResponse = null;
		try {
			sgCiRels = getTRUCartModifierHelper().getShippingGroupCommerceItemRelationships(pOrder);
			if ((sgCiRels != null && !sgCiRels.isEmpty()) ||
					 (null != ((TRUOrderImpl) pOrder).getOutOfStockItems() && !((TRUOrderImpl) pOrder).getOutOfStockItems()
							.isEmpty())) {
				final int sgCiRelsCount = sgCiRels.size();
				for (int i = TRUCommerceConstants.INT_ZERO; i < sgCiRelsCount; ++i) {
					final TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) sgCiRels.get(i);
					// To avoid the gift wrap items display part
					final CommerceItem commerceItem = sgCiRel.getCommerceItem();
					if (commerceItem instanceof TRUDonationCommerceItem) {
						donationItemList.add(getDonationItemsVO((TRUDonationCommerceItem)commerceItem));
					} else if (!(commerceItem instanceof TRUGiftWrapCommerceItem) && sgCiRel.getMetaInfo() != null
							&& !sgCiRel.getMetaInfo().isEmpty()) {
						final ListIterator<TRUCommerceItemMetaInfo> listIterator = sgCiRel.getMetaInfo().listIterator(sgCiRel.getMetaInfo().size());
						while (listIterator.hasPrevious()){ 
							cartItemDetailVO = getTRUCartModifierHelper().createIndividualItemDetailVOByRelatioShip(
									sgCiRel,pOrder, listIterator.previous());
							if(pIsAPIRequest){
								boolean shipInOrigContainerInfo=getShipInOrigContainerInfo(cartItemDetailVO);
								cartItemDetailVO.setShipInOrigContainer(shipInOrigContainerInfo);
								getBrandName(cartItemDetailVO);
							}
							setGiftwrapDetailsToVo(pIsAPIRequest, cartItemDetailVO, skuIdAndLocIdMap,inventoryRequest, sgCiRel);
							shoppingCartList.add(cartItemDetailVO);
						}
					}
					else{
						if (isLoggingDebug()) {
							vlogDebug("@Class::TRUShoppingCartUtils::@method::getItemDetailVOsByRelationShips() "
									+ ": order containing items with no metaInfo {0} ::", pOrder.getId());
						}
					}
				}
				inventoryResponse = getCoherenceInventoryManager().validateBulkStockLevel(inventoryRequest);
				if (isLoggingDebug()) {
					logDebug("@Class::TRUShoppingCartUtils::@method::getItemDetailVOsByRelationShips() : " + inventoryResponse);
				}
				
				Set<String> skuIdSet = getTRUCartModifierHelper().buildSkuSetForInventoryCheck(skuIdAndLocIdMap,
						inventoryRequest, inventoryResponse);
				
				for (TRUInventoryInfo cartRequest : inventoryRequest) {
					if(cartRequest.getLocationId() != null){
						RepositoryItem skuItem = getTRUCartModifierHelper().findSkuItem(cartRequest.getCatalogRefId());
						if(skuItem != null && isISPUItemInOOS(skuItem)){
							skuIdSet.add(cartRequest.getCatalogRefId() + TRUCommerceConstants.UNDER_SCORE + cartRequest.getLocationId());
						}
					}
				}
				if (!((TRUOrderImpl) pOrder).getOutOfStockItems().isEmpty() && !pIsCSCRequest) {
					outOfStockItemList.addAll(getTRUCartModifierHelper().createIndividualItemForOutOfStock(pOrder));
				}
				if (!shoppingCartList.isEmpty()) {
					addItemVoDetailsToList(pIsAPIRequest, pCookieLocationId, shoppingCartList, skuIdSet);
				}
				if ((null == pFromOverlay || (null != pFromOverlay && pFromOverlay
						.equalsIgnoreCase(TRUCommerceConstants.UNDEFINED))) && !outOfStockItemList.isEmpty()) {
					shoppingCartList.addAll(outOfStockItemList);
				}
				if (donationItemList != null && !donationItemList.isEmpty()) {
					shoppingCartList.addAll(donationItemList);
				}
			}
		} catch (CommerceException e) {
			if (isLoggingError()) {
				logError("Commerce Exception in TRUShoppingCartUtils.getItemDetailVOsByRelationShips", e);
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("Repository Exception in TRUShoppingCartUtils.getItemDetailVOsByRelationShips", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting method TRUShoppingCartUtils.getItemDetailVOsByRelationShips :: END");
		}

		return shoppingCartList;
	}
	
	/**
	 * This method is used to set ShipInOrigContainer flag  to VO
	 * 
	 * object and adding VO details to CommerceItem List.
	 * @param pCartItemDetailVO -the cartItemDetailVO Object
	 * @return shipInOrigContainer - shipInOrigContainer
	 */
	private boolean  getShipInOrigContainerInfo(TRUCartItemDetailVO pCartItemDetailVO) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::setShipInOrigContainer() : START");
		}
		Boolean shipInOrigContainer = Boolean.FALSE;
		if(pCartItemDetailVO !=null && pCartItemDetailVO.getSkuItem()!= null && pCartItemDetailVO.getSkuItem().getPropertyValue(getCatalogProperties().getShipInOrigContainer()) !=null){
			
			shipInOrigContainer=  (Boolean) pCartItemDetailVO.getSkuItem().getPropertyValue(getCatalogProperties().getShipInOrigContainer());
		}
		
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::setShipInOrigContainer() : END");
			
		}
		return shipInOrigContainer;
	}

	/**
	 * This method is used to set the commerceItem inventory details to VO
	 * object and adding VO details to CommerceItem List.
	 * 
	 * @param pIsAPIRequest - the boolean value of pIsAPIRequest
	 * @param pCookieLocationId -the cookie location id
	 * @param pShoppingCartList - the shoppingCartList value
	 * @param pSkuIdSet - the value of skuIdSet
	 */
	private void addItemVoDetailsToList(boolean pIsAPIRequest,
			String pCookieLocationId,
			final List<TRUCartItemDetailVO> pShoppingCartList,
			final Set<String> pSkuIdSet) {
		
		final Iterator<TRUCartItemDetailVO> listIterator = pShoppingCartList.iterator();
		while (listIterator.hasNext()){
			final TRUCartItemDetailVO itemDetailVO = listIterator.next();
			if ((itemDetailVO.getLocationId() != null && pSkuIdSet.contains(itemDetailVO.getSkuId()
					+ TRUCommerceConstants.UNDER_SCORE + itemDetailVO.getLocationId())) ||
					 itemDetailVO.getLocationId() == null && pSkuIdSet.contains(
							itemDetailVO.getSkuId()) && !TRUCommerceConstants.PRE_ORDER.equals(itemDetailVO.getInventoryStatus())) {
				getTRUCartModifierHelper().setInventoryInfoMessageToItemVO(itemDetailVO, pCookieLocationId);
			}
			if (itemDetailVO.getInventoryStatus() == null ||
					 !itemDetailVO.getInventoryStatus().equals(TRUCommerceConstants.PRE_ORDER)) {
				itemDetailVO.setInventoryStatus(TRUCommerceConstants.IN_STOCK);
			}
			if (isLoggingDebug()) {
				logDebug("@Class::TRUShoppingCartUtils::@method::getItemDetailVOsByRelationShips() itemDetailVO and inventory is:  {0}"
						+ itemDetailVO.getInventoryStatus());
			}
			if (pIsAPIRequest) {
				final Site site = SiteContextManager.getCurrentSite();
				final String pdpUrl = getPDPURL(
						(String) itemDetailVO.getSkuItem().getPropertyValue(
								getCatalogProperties().getOnlinePID()), site);
				if (StringUtils.isNotBlank(pdpUrl)) {
					itemDetailVO.setItemURL(pdpUrl);
				}
			}
		}
	}

	/**
	 * This method is used to set the Gift Wrap item details to CartItemVO
	 * Object.
	 * 
	 * @param pIsAPIRequest
	 *            the boolean value of pIsAPIRequest
	 * @param pCartItemDetailVO
	 *            the cartItemDetailVO Object
	 * @param pSkuIdAndLocIdMap
	 *            the Map of SKU and LocId
	 * @param pInventoryRequest
	 *            the List of inventory Request
	 * @param pSgCiRel
	 *            the Object of SGAndCI Relationship
	 */
	private void setGiftwrapDetailsToVo(boolean pIsAPIRequest,
			TRUCartItemDetailVO pCartItemDetailVO,
			final Map<String, Long> pSkuIdAndLocIdMap,
			final List<TRUInventoryInfo> pInventoryRequest,
			final TRUShippingGroupCommerceItemRelationship pSgCiRel) {
		
		if (StringUtils.isNotBlank(pCartItemDetailVO.getLocationId())
				&& !pSkuIdAndLocIdMap.containsKey(pCartItemDetailVO.getSkuId()
						+ TRUCommerceConstants.UNDER_SCORE + pCartItemDetailVO.getLocationId())) {
			final TRUInventoryInfo inveReq = new TRUInventoryInfo(pCartItemDetailVO.getSkuId(),
					pCartItemDetailVO.getQuantity(), pCartItemDetailVO.getLocationId());
			pInventoryRequest.add(inveReq);
			pSkuIdAndLocIdMap.put(pCartItemDetailVO.getSkuId() + TRUCommerceConstants.UNDER_SCORE
					+ pCartItemDetailVO.getLocationId(), pCartItemDetailVO.getQuantity());
		} else if (!pSkuIdAndLocIdMap.containsKey(pCartItemDetailVO.getSkuId())) {
			final TRUInventoryInfo inveReq = new TRUInventoryInfo(pCartItemDetailVO.getSkuId(),
					pCartItemDetailVO.getQuantity(), null);
			pInventoryRequest.add(inveReq);
			pSkuIdAndLocIdMap.put(pCartItemDetailVO.getSkuId(), pCartItemDetailVO.getQuantity());
		}
		if (pIsAPIRequest) {
			// setting first promotion
			TRUCommerceItemImpl item = (TRUCommerceItemImpl) pSgCiRel.getCommerceItem();
			RepositoryItem skuItem = (RepositoryItem) item.getAuxiliaryData().getCatalogRef();
			if (skuItem != null) {
				TRUPromotionTools promotionTools = (TRUPromotionTools) getCommerceItemManager()
						.getOrderTools().getProfileTools().getPromotionTools();
				@SuppressWarnings("unchecked")
				List<String> promotionsIds = (List<String>) skuItem.getPropertyValue(getCatalogProperties()
						.getSkuQualifiedForPromos());
				List<RepositoryItem> promoList = promotionTools
						.getValidProductProductPromotions(promotionsIds);
				if (promoList != null && !promoList.isEmpty()) {
					pCartItemDetailVO.setProductPromotion(promoList.get(TRUConstants.ZERO));
				}
			}
			if (StringUtils.isNotBlank(pCartItemDetailVO.getLocationId())) {
				getTRUCartModifierHelper().setInStorePickUpDetails(pCartItemDetailVO);
			}
		}
	}

	/**
	 * Gets the inventory status.
	 * 
	 * @param pSkuId
	 *            the sku id
	 * @param pLocationId
	 *            the location id
	 * @return the inventory status
	 */
	public int getInventoryStatus(String pSkuId, String pLocationId) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::getInventoryStatus() : START");
			vlogDebug("SKU ID : {0}, Location ID: {1}", pSkuId, pLocationId);
		}
		int status = TRUCommerceConstants.INT_ZERO;
		try {
			if (!StringUtils.isBlank(pSkuId) && !StringUtils.isBlank(pLocationId) &&
					 !pLocationId.equalsIgnoreCase(TRUCommerceConstants.ONLINE)) {
				// status = getInventoryManager().queryAvailabilityStatus(pSkuId, pLocationId);
				status = getCoherenceInventoryManager().queryAvailabilityStatus(pSkuId, pLocationId);
			} else if ((!StringUtils.isBlank(pSkuId) && !StringUtils.isBlank(pLocationId) && pLocationId
					.equalsIgnoreCase(TRUCommerceConstants.ONLINE)) ||
					 (!StringUtils.isBlank(pSkuId) && getPropertyManager().getLocationIdForShipToHome().equalsIgnoreCase(
							pLocationId))) {
				// status = getInventoryManager().queryAvailabilityStatus(pSkuId,
				// getPropertyManager().getLocationIdForShipToHome());
				status = getCoherenceInventoryManager().queryAvailabilityStatus(pSkuId);
			}
			if (status == TRUCommerceConstants.INT_OUT_OF_STOCK && !StringUtils.isBlank(pLocationId) &&
					 !pLocationId.equalsIgnoreCase(TRUCommerceConstants.ONLINE)) {
				status = getCoherenceInventoryManager().queryAvailabilityStatus(pSkuId,
						Long.toString(getTRUCartModifierHelper().getWareHouseLocationId(pLocationId).longValue()));

			}
		} catch (RepositoryException repEx) {
			if (isLoggingError()) {
				logError("Repository Exception  in TRUShoppingCartUtils.getInventoryStatus", repEx);
			}
		} catch (InventoryException inventoryException) {
			if (isLoggingError()) {
				logError("Inventory Exception  in TRUShoppingCartUtils.getInventoryStatus", inventoryException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::getInventoryStatus() : END : Inventory Status -->" + status);
		}
		return status;
	}

	/**
	 * Gets the item inventory.
	 * 
	 * @param pSkuId
	 *            the sku id
	 * @param pLocationId
	 *            the location id
	 * @param pInventoryStatus
	 *            the inventory status
	 * @return the item inventory
	 */
	public long getItemInventory(String pSkuId, String pLocationId, int pInventoryStatus) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::getItemInventory() : START");
			vlogDebug("SKU ID : {0}, Location ID: {1}, Inventory Status : {2}", pSkuId, pLocationId);
		}
		long itemInventory = TRUCommerceConstants.INT_ZERO;
		try {
			if (!StringUtils.isBlank(pSkuId) && !StringUtils.isBlank(pLocationId) &&
					 !pLocationId.equalsIgnoreCase(getPropertyManager().getLocationIdForShipToHome())) {

				itemInventory = getCoherenceInventoryManager().queryStockLevel(pSkuId, pLocationId);
				if (itemInventory <= TRUCommerceConstants.INT_ZERO) {
					itemInventory = getCoherenceInventoryManager().queryStockLevel(pSkuId,
							Long.toString(Math.round(getTRUCartModifierHelper().getWareHouseLocationId(pLocationId))));
				}
			} else if ((!StringUtils.isBlank(pSkuId) && StringUtils.isBlank(pLocationId)) ||
					 (!StringUtils.isBlank(pSkuId) && getPropertyManager().getLocationIdForShipToHome().equalsIgnoreCase(
							pLocationId))) {
				itemInventory = getCoherenceInventoryManager().queryStockLevel(pSkuId);

			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException @getItemInventory in TRUShoppingCartUtils class", e);
			}
		} catch (InventoryException e) {
			if (isLoggingError()) {
				logError("InventoryException @getItemInventory in TRUShoppingCartUtils class", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Item Inventory -->" + itemInventory);
			logDebug("@Class::TRUShoppingCartUtils::@method::getItemInventory() : END");
		}
		return itemInventory;
	}

	/**
	 * Gets the prucahse limit.
	 * 
	 * @param pSkuId
	 *            the sku id
	 * @return the prucahse limit
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws NumberFormatException
	 *             the number format exception
	 */
	public long getCustomerPurchaseLimit(String pSkuId) throws RepositoryException, NumberFormatException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::getCustomerPurchaseLimit() : START : SKU ID -->" + pSkuId);
		}
		String customerLimit = null;
		long customerLimitQty = TRUCommerceConstants.INT_ZERO;
		RepositoryItem skuItem = null;
		if (!StringUtils.isBlank(pSkuId)) {
			skuItem = getTRUCartModifierHelper().findSkuItem(pSkuId);
			if (skuItem != null) {
				customerLimit = (String) skuItem.getPropertyValue(getPropertyManager().getCustomerPurchaseLimit());
				if (customerLimit != null) {
					customerLimitQty = (long) Long.parseLong(customerLimit);
				} else {
					customerLimitQty = getConfiguration().getMaximumItemQuantity();
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::getCustomerPurchaseLimit() : END : Customer Limit Qty -->"
					+ customerLimitQty);
		}
		return customerLimitQty;
	}

	/**
	 * Gets the item qty in cart.
	 * 
	 * @param pSkuId
	 *            the sku id
	 * @param pOrder
	 *            the order
	 * @return the item qty in cart
	 */
	@SuppressWarnings("unchecked")
	public long getItemQtyInCart(String pSkuId, Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::getItemQtyInCart() : START");
			vlogDebug("SKU ID : {0}, Order : {1}", pSkuId, pOrder);
		}
		long cartQty = TRUCommerceConstants.INT_ZERO;
		if (!StringUtils.isBlank(pSkuId)) {
			List<CommerceItem> commerceItems = null;
			commerceItems = pOrder.getCommerceItems();
			if (null != commerceItems && !commerceItems.isEmpty()) {
				for (CommerceItem commerceItem : commerceItems) {
					if (commerceItem.getCatalogRefId().equalsIgnoreCase(pSkuId)) {
						cartQty = commerceItem.getQuantity();
						break;
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::getItemQtyInCart() : END : Cart Qty -->" + cartQty);
		}
		return cartQty;
	}

	/**
	 * Gets the item qty in cart by rel ship.
	 * 
	 * @param pSkuId
	 *            the sku id
	 * @param pLocationId
	 *            the location id
	 * @param pOrder
	 *            the order
	 * @return the item qty in cart by rel ship
	 */
	public long getItemQtyInCartByRelShip(String pSkuId, String pLocationId, Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::getItemQtyInCartByRelShip() : START");
			vlogDebug("Sku Id: {0}, Location Id : {1}, Order : {2}", pSkuId, pLocationId, pOrder);
		}
		long cartQty = TRUCommerceConstants.INT_ZERO;
		CommerceItem commerceItemExist = null;
		final List<CommerceItem> commerceItems = pOrder.getCommerceItems();
		if (null != commerceItems && !commerceItems.isEmpty()) {
			for (CommerceItem commerceItem : commerceItems) {
				if (commerceItem.getCatalogRefId().equalsIgnoreCase(pSkuId)) {
					commerceItemExist = commerceItem;
					break;
				}
			}
		}
		if (null != commerceItemExist) {
			final List<ShippingGroupCommerceItemRelationship> ciSGRelationShips = commerceItemExist.getShippingGroupRelationships();
			for (ShippingGroupCommerceItemRelationship sGCItemRelationship : ciSGRelationShips) {
				if (!StringUtils.isBlank(pLocationId) && pLocationId.equals(getPropertyManager().getLocationIdForShipToHome())) {
					if (sGCItemRelationship.getShippingGroup() instanceof TRUHardgoodShippingGroup
							|| sGCItemRelationship.getShippingGroup() instanceof TRUChannelHardgoodShippingGroup) {
						cartQty = cartQty + sGCItemRelationship.getQuantity();
					}
				} else {
					if (sGCItemRelationship.getShippingGroup() instanceof TRUInStorePickupShippingGroup) {
						final TRUInStorePickupShippingGroup iSPUSG = (TRUInStorePickupShippingGroup) sGCItemRelationship
								.getShippingGroup();
						if (!StringUtils.isBlank(pLocationId) && pLocationId.equals(iSPUSG.getLocationId())) {
							cartQty = cartQty + sGCItemRelationship.getQuantity();
						}
					} else if (sGCItemRelationship.getShippingGroup() instanceof TRUChannelInStorePickupShippingGroup) {
						final TRUChannelInStorePickupShippingGroup iSPUSG = (TRUChannelInStorePickupShippingGroup) sGCItemRelationship
								.getShippingGroup();
						if (pLocationId.equals(iSPUSG.getLocationId())) {
							cartQty = cartQty + sGCItemRelationship.getQuantity();
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::getItemQtyInCartByRelShip() : END : Cart Qty -->" + cartQty);
		}
		return cartQty;
	}

	/**
	 * Gets the shipping surcharge.
	 * 
	 * @param pOrder
	 *            the order
	 * @return the shipping surcharge
	 */
	@SuppressWarnings("unchecked")
	public double getShippingSurcharge(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::getShippingSurcharge() : START : Order -->" + pOrder);
		}
		double shippingSurcharge = TRUCommerceConstants.DOUBLE_ZERO;
		List<CommerceItem> items = null;
		items = pOrder.getCommerceItems();
		if (items != null && !items.isEmpty()) {
			for (CommerceItem item : items) {
				if (item.getPriceInfo() instanceof TRUItemPriceInfo) {
					shippingSurcharge += ((TRUItemPriceInfo) item.getPriceInfo()).getItemShippingSurcharge();
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::getShippingSurcharge() : END : Shipping Surhcarge -->"
					+ shippingSurcharge);
		}
		return shippingSurcharge;
	}

	/**
	 * Gets the display name for sku.
	 * 
	 * @param pSkuId
	 *            the sku id
	 * @return the display name for sku
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public String getDisplayNameForSku(String pSkuId) throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShoppingCartUtils::@method::getDisplayNameForSku() : START : SKU ID -->", pSkuId);
		}
		RepositoryItem skuitem = null;
		String displayName = null;
		if (!StringUtils.isBlank(pSkuId)) {
			skuitem = getTRUCartModifierHelper().findSkuItem(pSkuId);
			if (null != skuitem) {
				displayName = skuitem.getPropertyValue(getPropertyManager().getDisplayName()).toString();
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShoppingCartUtils::@method::getDisplayNameForSku() : END : Display Name -->", displayName);
		}
		return displayName;
	}

	/**
	 * Builds the TRUGiftOptionsInfo vos. This iterates all Shipping Groups to populate giftOptionsDetails map based.
	 * 
	 * @param pOrder
	 *            - the order
	 * @return Map - key Shipping Group Id and Value is TRUGiftOptionsInfo obj
	 */
	@SuppressWarnings("unchecked")
	public Map<String, TRUGiftOptionsInfo> getGiftOptionsDetails(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUShoppingCartUtils.getGiftOptionsDetails :: START");
			vlogDebug("INPUT PARAMS getGiftOptionsDetails method pOrder: {0}", pOrder);
		}
		if (pOrder == null) {
			return null;
		}
		final Map<String, TRUGiftOptionsInfo> giftOptionsDetails = new HashMap<String, TRUGiftOptionsInfo>();
		try {
			final List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
			if (shippingGroups != null && !shippingGroups.isEmpty()) {
				for (ShippingGroup shippingGroup : shippingGroups) {
					if (shippingGroup instanceof TRUHardgoodShippingGroup) {
						// giftOptionsDetails.put(shippingGroup.getId(), populateGiftOptionInfo(shippingGroup, pOrder));
						populateGiftOptionInfo(shippingGroup, pOrder, giftOptionsDetails);
					}
				}
			}
		} catch (CommerceException e) {
			if (isLoggingError()) {
				logError("Commerce Exception in TRUShoppingCartUtils.getGiftOptionsDetails", e);
			}
		} catch (CloneNotSupportedException e) {
			if (isLoggingError()) {
				logError("CloneNotSupportedException in TRUShoppingCartUtils.getGiftOptionsDetails", e);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END TRUShoppingCartUtils.getGiftOptionsDetails method and returns giftOptionsDetails size: {0}",
					giftOptionsDetails.size());
		}
		return giftOptionsDetails;
	}

	/**
	 * Populate item detail TRUGiftOptionsInfo vo based on the shipping group.
	 * 
	 * @param pShippingGroup
	 *            the shipping group
	 * @param pOrder
	 *            the order
	 * @param pGiftOptionsDetails
	 *            the gift options details
	 * @throws CommerceItemNotFoundException
	 *             the commerce item not found exception
	 * @throws InvalidParameterException
	 *             the invalid parameter exception
	 * @throws CloneNotSupportedException
	 *             the clone not supported exception
	 */
	@SuppressWarnings("unchecked")
	public void populateGiftOptionInfo(ShippingGroup pShippingGroup, Order pOrder,
			Map<String, TRUGiftOptionsInfo> pGiftOptionsDetails) throws CommerceItemNotFoundException, InvalidParameterException,
			CloneNotSupportedException {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUShoppingCartUtils.populateGiftOptionInfo :: START");
			vlogDebug("INPUT PARAMS populateGiftOptionInfo method pShippingGroup: {0} pOrder: {1}", pShippingGroup, pOrder);
		}
		if (pShippingGroup == null || pOrder == null) {
			return;
		}
		TRUGiftOptionsInfo giftOptionsInfo = null;
		Map<String, TRUGiftItemInfo> eligibleGiftItems = null;
		Map<String, TRUGiftItemInfo> giftWrappedItems = null;
		Map<String, TRUGiftItemInfo> nonEligibleGiftItems = null;
		TRUHardgoodShippingGroup shippingGroup = null;
		if (pShippingGroup instanceof TRUHardgoodShippingGroup) {
			shippingGroup = (TRUHardgoodShippingGroup) pShippingGroup;
		}
		if (StringUtils.isNotBlank(shippingGroup.getNickName()) && pGiftOptionsDetails.get(shippingGroup.getNickName()) != null) {
			giftOptionsInfo = pGiftOptionsDetails.get(shippingGroup.getNickName());
			eligibleGiftItems = giftOptionsInfo.getEligibleGiftItems();
			giftWrappedItems = giftOptionsInfo.getGiftWrappedItems();
			nonEligibleGiftItems = giftOptionsInfo.getNonEligibleGiftItems();
			giftOptionsInfo.setGiftOptionId(giftOptionsInfo.getGiftOptionId() + TRUCommerceConstants.UNDER_SCORE
					+ shippingGroup.getId());
		} else {
			giftOptionsInfo = new TRUGiftOptionsInfo();
			eligibleGiftItems = new HashMap<String, TRUGiftItemInfo>();
			giftWrappedItems = new HashMap<String, TRUGiftItemInfo>();
			nonEligibleGiftItems = new HashMap<String, TRUGiftItemInfo>();
			giftOptionsInfo.setGiftOptionId(shippingGroup.getId());
			giftOptionsInfo.setGiftReceipt(shippingGroup.isGiftReceipt());
			giftOptionsInfo.setGiftWrapMessage(shippingGroup.getGiftWrapMessage());
			giftOptionsInfo.setShippingAddress(shippingGroup.getShippingAddress());
			giftOptionsInfo.setRegistryAddress(shippingGroup.isRegistryAddress());
			if (StringUtils.isNotBlank(shippingGroup.getNickName())) {
				pGiftOptionsDetails.put(shippingGroup.getNickName(), giftOptionsInfo);
			}
		}
		final List<CommerceItemRelationship> ciRels = pShippingGroup.getCommerceItemRelationships();
		for (CommerceItemRelationship commerceItemRelationship : ciRels) {
			final TRUShippingGroupCommerceItemRelationship truSgCiR = (TRUShippingGroupCommerceItemRelationship) commerceItemRelationship;
			if (truSgCiR.getCommerceItem() != null) {
				final TRUCommerceItemImpl commerceItem = (TRUCommerceItemImpl) truSgCiR.getCommerceItem();
				if (commerceItem instanceof TRUGiftWrapCommerceItem || commerceItem instanceof TRUDonationCommerceItem) {
					continue;
				}
				populateTRUGiftItemInfo(shippingGroup, pOrder, truSgCiR, eligibleGiftItems, giftWrappedItems,
						nonEligibleGiftItems);
			}
		}
		giftOptionsInfo.setEligibleGiftItems(eligibleGiftItems);
		giftOptionsInfo.setNonEligibleGiftItems(nonEligibleGiftItems);
		giftOptionsInfo.setGiftWrappedItems(giftWrappedItems);
		if (isLoggingDebug()) {
			vlogDebug("END TRUShoppingCartUtils.getItemDetailVOsByRelationShips returns giftOptionsInfo: {0}", giftOptionsInfo);
		}
	}

	/**
	 * Populate item detail TRUGiftItemInfo vo based on the TRUShippingGroupCommerceItemRelationship.
	 * 
	 * @param pShippingGroup
	 *            - ShippingGroup obj
	 * @param pOrder
	 *            - Order Obj
	 * @param pTruSgCiR
	 *            - TRUShippingGroupCommerceItemRelationship obj
	 * @param pEligibleGiftItems
	 *            - map of eligible Gift Items
	 * @param pGiftWrappedItems
	 *            - Map of Gift wrapped Items
	 * @param pNonEligibleGiftItems
	 *            - Map of Eligible Gift Items
	 * @throws CommerceItemNotFoundException
	 *             - If any
	 * @throws InvalidParameterException
	 *             - If any
	 * @throws CloneNotSupportedException
	 *             - If any
	 */
	@SuppressWarnings("unchecked")
	private void populateTRUGiftItemInfo(ShippingGroup pShippingGroup, Order pOrder,
			TRUShippingGroupCommerceItemRelationship pTruSgCiR, Map<String, TRUGiftItemInfo> pEligibleGiftItems,
			Map<String, TRUGiftItemInfo> pGiftWrappedItems, Map<String, TRUGiftItemInfo> pNonEligibleGiftItems)
			throws CommerceItemNotFoundException, InvalidParameterException, CloneNotSupportedException {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUShoppingCartUtils.populateTRUGiftItemInfo :: START");
			vlogDebug("INPUT PARAMS populateTRUGiftItemInfo method pShippingGroup: {0} pOrder: {1}", pShippingGroup, pOrder);
		}
		if (pShippingGroup == null || pOrder == null || pTruSgCiR == null || pTruSgCiR.getCommerceItem() == null) {
			vlogDebug("END TRUShoppingCartUtils.populateTRUGiftItemInfo returns null");
			return;
		}
		RepositoryItem skuItem = null;
		String canBeGiftWrapped = null;
		StringBuilder registryName = new StringBuilder();
		int itemQtyInCart = 0;
		List<String> styleDimensions = null;
		final TRUCommerceItemImpl commerceItem = (TRUCommerceItemImpl) pTruSgCiR.getCommerceItem();
		skuItem = (RepositoryItem) commerceItem.getAuxiliaryData().getCatalogRef();
		if (skuItem != null) {
			itemQtyInCart = (int) pTruSgCiR.getQuantity();
			canBeGiftWrapped = (String) skuItem.getPropertyValue(getPropertyManager().getCanBeGiftWrapped());
		}
		final TRUGiftItemInfo giftItemInfo = new TRUGiftItemInfo();
		// Inserting the basic info to VO
		giftItemInfo.setShippingGroupId(pShippingGroup.getId());
		giftItemInfo.setCommItemRelationshipId(pTruSgCiR.getId());
		giftItemInfo.setParentCommerceId(commerceItem.getId());
		giftItemInfo.setProductId(commerceItem.getAuxiliaryData().getProductId());
		giftItemInfo.setProductDisplayName((String) skuItem.getPropertyValue(getPropertyManager().getDisplayName()));
		giftItemInfo.setParentQuantity(itemQtyInCart);
		giftItemInfo.setSkuId(commerceItem.getCatalogRefId());
		//START: TUW-52942
		if (pShippingGroup instanceof TRUChannelHardgoodShippingGroup) {
			TRUChannelHardgoodShippingGroup hgSg = (TRUChannelHardgoodShippingGroup) pShippingGroup;
			registryName.append(getTRUCartModifierHelper().getCamelCaseString(hgSg.getChannelUserName()));
			if (!StringUtils.isBlank(hgSg.getChannelCoUserName())) {
				registryName.append(TRUCommerceConstants.SPACE).append(TRUCommerceConstants.AND).append(TRUCommerceConstants.SPACE)
						.append(getTRUCartModifierHelper().getCamelCaseString(hgSg.getChannelCoUserName())).append(TRUCommerceConstants.TAIL_OF_REGISTRY_NAME);
			}else {
				registryName.append(TRUCommerceConstants.TAIL_OF_REGISTRY_NAME).append(TRUCommerceConstants.SPACE);
			}
			if (!StringUtils.isBlank(hgSg.getChannelType()) && hgSg.getChannelType().equals(getPropertyManager().getWishlist())) {
				registryName.append(TRUCommerceConstants.SPACE).append(getPropertyManager().getWishlist());
			} else if (!StringUtils.isBlank(hgSg.getChannelType())&& hgSg.getChannelType().equals(getPropertyManager().getRegistry())) {
				registryName.append(TRUCommerceConstants.SPACE).append(getPropertyManager().getRegistry());
			}
			if (!StringUtils.isBlank(registryName.toString())) {
				giftItemInfo.setChannel(registryName.toString());
			}
		}
		//END: TUW-52942
		RepositoryItem productItem = (RepositoryItem) commerceItem.getAuxiliaryData().getProductRef();
		if(productItem != null){
			styleDimensions = (List<String>) (productItem.getPropertyValue(getPropertyManager()
					.getStyleDimensionsPropertyName()));
		}
		if (styleDimensions != null && !styleDimensions.isEmpty()) {
			if (null != skuItem.getPropertyValue(getPropertyManager().getColorUpcLevel())) {
				RepositoryItem clorItem = (RepositoryItem) skuItem.getPropertyValue(getPropertyManager().getColorUpcLevel());
				String colorCode = String.valueOf(clorItem.getPropertyValue(getPropertyManager().getColorNamePropertyName()));
				giftItemInfo.setProductColor(String.valueOf(colorCode.toLowerCase()));
			}
			if (null != skuItem.getPropertyValue(getPropertyManager().getJuvenileProductSize())) {
				RepositoryItem sizeItem = (RepositoryItem) skuItem.getPropertyValue(getPropertyManager().getJuvenileProductSize());
				String size = String.valueOf(sizeItem.getPropertyValue(getPropertyManager().getSizeDescriptionPropertyName()));
				giftItemInfo.setProductSize(String.valueOf(size.toUpperCase()));
			}
		}

		if (skuItem != null) {
			giftItemInfo.setProductImage((String) skuItem.getPropertyValue(getPropertyManager().getPrimaryImage()));
		}
		giftItemInfo.setQuantity(TRUCommerceConstants.ONE);
		// If for eligible for GW and else for non else
		if (!StringUtils.isBlank(canBeGiftWrapped) && canBeGiftWrapped.equals(TRUCommerceConstants.YES)) {
			setGiftWrapDetails(pOrder, pTruSgCiR, pEligibleGiftItems,
					pGiftWrappedItems, itemQtyInCart, commerceItem,
					giftItemInfo);
		} else {
			giftItemInfo.setGiftWrapEligible(false);
			giftItemInfo.setGiftItem(false);
			for (int i = 0; i < itemQtyInCart; i++) {
				final TRUGiftItemInfo splitGiftItemInfo = (TRUGiftItemInfo) giftItemInfo.clone();
				pNonEligibleGiftItems.put(commerceItem.getId() + TRUCommerceConstants.UNDER_SCORE + pTruSgCiR.getId()
						+ TRUCommerceConstants.UNDER_SCORE + i, splitGiftItemInfo);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END TRUShoppingCartUtils.populateTRUGiftItemInfo");
		}
	}
	
	/**
	 * This method is used to set the details to the GiftItemInfo Object.
	 * 
	 * @param pOrder
	 *            Order object
	 * @param pTruSgCiR
	 *            SGAndCI RelationShip Object
	 * @param pEligibleGiftItems
	 *            Eligible GiftItems Map
	 * @param pGiftWrappedItems
	 *            GiftWrapped Items Map
	 * @param pItemQtyInCart
	 *            cartItems Quantity
	 * @param pCommerceItem
	 *            the CommerceItem Object
	 * @param pGiftItemInfo
	 *            the giftItemInfo Object
	 * @throws CommerceItemNotFoundException
	 *             - If any
	 * @throws InvalidParameterException
	 *             - If any
	 * @throws CloneNotSupportedException
	 *             - If any
	 */
	private void setGiftWrapDetails(Order pOrder,
			TRUShippingGroupCommerceItemRelationship pTruSgCiR,
			Map<String, TRUGiftItemInfo> pEligibleGiftItems,
			Map<String, TRUGiftItemInfo> pGiftWrappedItems, int pItemQtyInCart,
			final TRUCommerceItemImpl pCommerceItem,
			final TRUGiftItemInfo pGiftItemInfo)
			throws CommerceItemNotFoundException, InvalidParameterException,
			CloneNotSupportedException {
		pGiftItemInfo.setGiftWrapEligible(true);
		// If for already Gift wrap item and else for not
		if (pTruSgCiR.getIsGiftItem()) {
			pGiftItemInfo.setGiftItem(true);
			final List<TRUGiftWrapItemInfo> wrapItemDetails = new ArrayList<TRUGiftWrapItemInfo>();

			final List<RepositoryItem> getGiftItemInfo = pTruSgCiR.getGiftItemInfo();
			// Taking all Gift wrap items and adding it to wrapItemDetails to assign to each Main Comm Item
			if (getGiftItemInfo != null && !getGiftItemInfo.isEmpty()) {
				for (RepositoryItem giftItem : getGiftItemInfo) {
					setGiftWrapItemDetails(pOrder, wrapItemDetails, giftItem);
				}
			}
			// Assigning the Gift warp details (wrapItemDetails) to each Main Comm Item with Qty 1
			for (int i = 0; i < pItemQtyInCart; i++) {
				final TRUGiftItemInfo splitGiftItemInfo = (TRUGiftItemInfo) pGiftItemInfo.clone();
				if (wrapItemDetails.size() > i) {
					splitGiftItemInfo.setWrapCommerceItemId(wrapItemDetails.get(i).getWrapCommerceItemId());
					splitGiftItemInfo.setWrapProductId(wrapItemDetails.get(i).getWrapProductId());
					splitGiftItemInfo.setWrapSkuId(wrapItemDetails.get(i).getWrapSkuId());
				} else {
					splitGiftItemInfo.setWrapCommerceItemId(null);
					splitGiftItemInfo.setWrapProductId(null);
					splitGiftItemInfo.setWrapSkuId(null);
				}
				pGiftWrappedItems.put(pCommerceItem.getId() + TRUCommerceConstants.UNDER_SCORE + pTruSgCiR.getId()
						+ TRUCommerceConstants.UNDER_SCORE + i, splitGiftItemInfo);
			}
		} else {
			// Assigning the Gift warp details (wrapItemDetails) to each Main Comm Item with Qty 1
			pGiftItemInfo.setGiftItem(false);
			for (int i = 0; i < pItemQtyInCart; i++) {
				final TRUGiftItemInfo splitGiftItemInfo = (TRUGiftItemInfo) pGiftItemInfo.clone();
				pEligibleGiftItems.put(pCommerceItem.getId() + TRUCommerceConstants.UNDER_SCORE + pTruSgCiR.getId()
						+ TRUCommerceConstants.UNDER_SCORE + i, splitGiftItemInfo);
			}
		}
	}
	
	/**
	 * This method is used to set the GiftWrap commerceItem details to
	 * GiftWrapItemInfo Object.
	 * 
	 * @param pOrder
	 *            Order object
	 * @param pWrapItemDetails
	 *            GiftWrapItemInfo details
	 * @param pGiftItem
	 *            Repository of GiftItem
	 * @throws CommerceItemNotFoundException
	 *             -If Any
	 * @throws InvalidParameterException
	 *             -If Any
	 */
	private void setGiftWrapItemDetails(Order pOrder,
			final List<TRUGiftWrapItemInfo> pWrapItemDetails,
			RepositoryItem pGiftItem) throws CommerceItemNotFoundException,
			InvalidParameterException {
		if (pGiftItem.getPropertyValue(getPropertyManager().getGiftWrapCommItemId()) != null) {
			final String truGiftWrapCommerceItemId = (String) pGiftItem.getPropertyValue(getPropertyManager()
					.getGiftWrapCommItemId());
			final long wrapCommerceItemQty = (long) pGiftItem.getPropertyValue(getPropertyManager()
					.getGiftWrapQuantity());
			final TRUGiftWrapCommerceItem truGiftWrapCommerceItem = (TRUGiftWrapCommerceItem) pOrder
					.getCommerceItem(truGiftWrapCommerceItemId);
			for (int i = 0; i < wrapCommerceItemQty; i++) {
				final TRUGiftWrapItemInfo wrapItemInfo = new TRUGiftWrapItemInfo();
				wrapItemInfo.setWrapQuantity(TRUCommerceConstants.ONE);
				wrapItemInfo.setWrapCommerceItemId(
						truGiftWrapCommerceItem.getRepositoryItem().getRepositoryId());
				wrapItemInfo.setWrapProductId(truGiftWrapCommerceItem.getAuxiliaryData().getProductId());
				wrapItemInfo.setWrapSkuId(truGiftWrapCommerceItem.getCatalogRefId());
				pWrapItemDetails.add(wrapItemInfo);
			}
		}
	}

	/**
	 * Checks if is order has gift wrapped item.
	 * 
	 * @param pOrder
	 *            the order
	 * @return the boolean
	 */
	@SuppressWarnings("unchecked")
	public boolean isOrderHasGiftWrappedItem(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUShoppingCartUtils.isOrderHasGiftWrappedItem :: START");
		}
		final List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		if (shippingGroups != null && !shippingGroups.isEmpty()) {
			for (ShippingGroup shippingGroup : shippingGroups) {
				if (shippingGroup instanceof TRUHardgoodShippingGroup || shippingGroup instanceof TRUChannelHardgoodShippingGroup) {
					final List<CommerceItemRelationship> ciRels = shippingGroup.getCommerceItemRelationships();
					for (CommerceItemRelationship commerceItemRelationship : ciRels) {
						final TRUShippingGroupCommerceItemRelationship truSgCiR = (TRUShippingGroupCommerceItemRelationship)commerceItemRelationship;
						if (truSgCiR.getIsGiftItem()) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	/**
	 * Gets the out of stock item list.
	 * 
	 * @param pDeletedList
	 *            the deleted list
	 * @param pOrder
	 *            the order
	 * @param pCookieLocationId
	 *            the cookie location id
	 * @return the out of stock item list
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public Set<String> getOutOfStockItemList(Set<String> pDeletedList, Order pOrder, String pCookieLocationId)
			throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug(
					"Entering into method TRUShoppingCartUtils.getOutOfStockItemList ::"
							+ " START --> DeletedList, pOrder, pCookieLocationId {0} {1} {2}",
					pDeletedList, pOrder, pCookieLocationId);
		}
		Set<String> outOfStockItemList = new HashSet<String>();
		List<TRUShippingGroupCommerceItemRelationship> sgCiRels = null;
		final List<TRUInventoryInfo> cartInventoryRequest = new ArrayList<TRUInventoryInfo>();
		List<TRUInventoryInfo> inventoryResponse = null;
		final Map<String, Long> skuIdAndLocIdMap = new HashMap<String, Long>();
		Set<String> skuIdSet = new HashSet<String>();
		try {
			sgCiRels = getTRUCartModifierHelper().getShippingGroupCommerceItemRelationships(pOrder);
			if ((sgCiRels != null && !sgCiRels.isEmpty())) {
				final int sgCiRelsCount = sgCiRels.size();
				for (int i = TRUCommerceConstants.INT_ZERO; i < sgCiRelsCount; ++i) {
					final TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) sgCiRels.get(i);
					ShippingGroup sg = ((ShippingGroupCommerceItemRelationship) sgCiRel).getShippingGroup();
					final TRUCommerceItemImpl item = (TRUCommerceItemImpl) sgCiRel.getCommerceItem();
					if (!pDeletedList.contains(item.getCatalogRefId())) {
						if (item instanceof TRUGiftWrapCommerceItem || item instanceof TRUDonationCommerceItem) {
							continue;
						}
						String backOrderStatus = getBackOrderStatus(item.getCatalogRefId());
						if ((sg instanceof TRUHardgoodShippingGroup || sg instanceof TRUChannelHardgoodShippingGroup) &&
								 !skuIdAndLocIdMap.containsKey(item.getCatalogRefId())) {
							if(backOrderStatus != null && backOrderStatus.equalsIgnoreCase(TRUCommerceConstants.N)){
								final List<CommerceItem> cItems = pOrder.getCommerceItemsByCatalogRefId(item.getCatalogRefId());
								for (CommerceItem commerceItem : cItems) {
									outOfStockItemList.add(commerceItem.getId());
								}
							}else{
								final TRUInventoryInfo inveReq = new TRUInventoryInfo(
										item.getCatalogRefId(), sgCiRel.getQuantity(), null);
								cartInventoryRequest.add(inveReq);
								skuIdAndLocIdMap.put(item.getCatalogRefId(), sgCiRel.getQuantity());
							}
						} else if ((sg instanceof TRUInStorePickupShippingGroup || sg instanceof TRUChannelInStorePickupShippingGroup)) {
							final TRUInStorePickupShippingGroup ispu = (TRUInStorePickupShippingGroup) sg;
							if (!skuIdAndLocIdMap.containsKey(item.getCatalogRefId() + TRUCommerceConstants.UNDER_SCORE
									+ ispu.getLocationId())) {
								if(backOrderStatus != null && backOrderStatus.equalsIgnoreCase(TRUCommerceConstants.N)){
									final List<CommerceItem> cItems = pOrder.getCommerceItemsByCatalogRefId(item.getCatalogRefId());
									for (CommerceItem commerceItem : cItems) {
										outOfStockItemList.add(commerceItem.getId());
									}
								}else{
									final TRUInventoryInfo inveReq = new TRUInventoryInfo(
											item.getCatalogRefId(), sgCiRel.getQuantity(),ispu.getLocationId());
									cartInventoryRequest.add(inveReq);
									skuIdAndLocIdMap.put(
											item.getCatalogRefId() + TRUCommerceConstants.UNDER_SCORE + ispu.getLocationId(),
											sgCiRel.getQuantity());
								}
							}
						}
					}
				}
				if (!cartInventoryRequest.isEmpty()) {
					inventoryResponse = getCoherenceInventoryManager().validateBulkStockLevel(cartInventoryRequest);
				} else {
					if(outOfStockItemList != null && outOfStockItemList.isEmpty()){
						return outOfStockItemList;
					}
				}
				if (isLoggingDebug()) {
					vlogDebug("@Class::TRUShoppingCartUtils::@method::getOutOfStockItemList() :  {0}", inventoryResponse);
				}
				long stockCount = TRUCommerceConstants.INT_ZERO;
				
				for (TRUInventoryInfo cartRequest : cartInventoryRequest) {
					if(cartRequest.getLocationId() != null){
						stockCount=getCoherenceInventoryManager().queryStockLevel(cartRequest.getCatalogRefId());
						if (stockCount <= TRUCommerceConstants.INT_ZERO) {
							RepositoryItem skuItem = getTRUCartModifierHelper().findSkuItem(cartRequest.getCatalogRefId());							
							if(skuItem != null && isISPUItemInOOS(skuItem)){
								final List<CommerceItem> cItems = pOrder.getCommerceItemsByCatalogRefId(cartRequest.getCatalogRefId());
								for (CommerceItem commerceItem : cItems) {
									outOfStockItemList.add(commerceItem.getId());
								}
							}														
						}
					}
				}
				
				skuIdSet = getTRUCartModifierHelper().buildSkuSetForInventoryCheck(skuIdAndLocIdMap, cartInventoryRequest,
						inventoryResponse);
				for (String skuId : skuIdSet) {
					addDetailsToOutOfStockList(pOrder, pCookieLocationId, outOfStockItemList, skuId);
				}
				if (isLoggingDebug()) {
					vlogDebug("@Class::TRUShoppingCartUtils::@method::getOutOfStockItemList() :  {0}", outOfStockItemList);
				}
				((TRUOrderTools) getCommerceItemManager().getOrderTools()).generateOutOfStockItems(outOfStockItemList, pOrder);
			}
		} catch (CommerceException e) {
			if (isLoggingError()) {
				logError("Commerce Exception in TRUShoppingCartUtils.getOutOfStockItemList", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting into method TRUShoppingCartUtils.getOutOfStockItemList :: END");
		}
		return outOfStockItemList;
	}
	
	/**
	 * Gets the back order status.
	 *
	 * @param pSkuId the sku id
	 * @return the back order status
	 * @throws RepositoryException the repository exception
	 */
	public String getBackOrderStatus(String pSkuId) throws RepositoryException{
		String backOrderStatus = null;
		RepositoryItem skuItem = getTRUCartModifierHelper().findSkuItem(pSkuId);
		Date currentDate = new Date();
		if (skuItem != null) {
			Date streetDate = (Date) skuItem.getPropertyValue(getPropertyManager()
					.getStreetDate());
			if (streetDate != null && currentDate.before(streetDate)) {
				backOrderStatus = (String) skuItem.getPropertyValue(getPropertyManager()
						.getBackOrderStatus());
				return backOrderStatus;
			}
		}
		return backOrderStatus;
	}
	
	/**
	 * This method is used to add OutofStock CommerceItem details to List
	 * Object.
	 * 
	 * @param pOrder
	 *            Order Object
	 * @param pCookieLocationId
	 *            Value of CookieLocationId
	 * @param pOutOfStockItemList
	 *            List Of OutOfStockItems
	 * @param pSkuId
	 *            Value of SkuId
	 * @throws CommerceItemNotFoundException
	 *             -If Any
	 * @throws InvalidParameterException
	 *             -If Any
	 * @throws InventoryException
	 *             -If Any
	 * @throws RepositoryException
	 *             -If Any
	 */
	private void addDetailsToOutOfStockList(Order pOrder,
			String pCookieLocationId, Set<String> pOutOfStockItemList,
			String pSkuId) throws CommerceItemNotFoundException,
			InvalidParameterException, InventoryException, RepositoryException {
		String skuId = pSkuId;
		if(skuId.contains(TRUCommerceConstants.UNDER_SCORE)){
			skuId = skuId.substring(TRUCommerceConstants.INT_ZERO,skuId.indexOf(TRUCommerceConstants.UNDER_SCORE));
		}
		final List<CommerceItem> cItems = pOrder.getCommerceItemsByCatalogRefId(skuId);
		for (CommerceItem cItem : cItems) {
			if(!pOutOfStockItemList.contains(cItem.getId())){
				final List<TRUShippingGroupCommerceItemRelationship> skuRels = cItem.getShippingGroupRelationships();
				for (TRUShippingGroupCommerceItemRelationship skuRel : skuRels) {
					final ShippingGroup sg = ((ShippingGroupCommerceItemRelationship) skuRel).getShippingGroup();
					if (sg instanceof TRUHardgoodShippingGroup) {
						long stockCount = TRUCommerceConstants.INT_ZERO;
						stockCount=getCoherenceInventoryManager().queryStockLevel(skuId);
						if(stockCount<=TRUCommerceConstants.INT_ZERO){
							stockCount = getTRUCartModifierHelper().getInventoryBasedOnChannelAvailability(
									cItem.getCatalogRefId(), cItem.getAuxiliaryData().getProductId(),
									cItem.getAuxiliaryData().getSiteId(), null, pCookieLocationId);
						}
						if (stockCount <= TRUCommerceConstants.INT_ZERO) {
							pOutOfStockItemList.add(cItem.getId());
						} else if (pOutOfStockItemList.contains(cItem.getId())) {
							pOutOfStockItemList.remove(cItem.getId());
						}
					} else if (sg instanceof TRUInStorePickupShippingGroup) {
						final InStorePickupShippingGroup ispu = (InStorePickupShippingGroup) sg;
						long stockCount = TRUCommerceConstants.INT_ZERO;
						stockCount=getCoherenceInventoryManager().queryStockLevel(skuId,ispu.getLocationId());
						if(stockCount<= TRUCommerceConstants.INT_ZERO){
							stockCount = getTRUCartModifierHelper().getInventoryBasedOnChannelAvailability(
									cItem.getCatalogRefId(), cItem.getAuxiliaryData().getProductId(),
									cItem.getAuxiliaryData().getSiteId(), ispu.getLocationId(), pCookieLocationId);
						}
						if (stockCount<= TRUCommerceConstants.INT_ZERO) {
							pOutOfStockItemList.add(cItem.getId());
						} else if (pOutOfStockItemList.contains(cItem.getId())) {
							pOutOfStockItemList.remove(cItem.getId());
						}
					}
				}
			}
			RepositoryItem skuItem =  (RepositoryItem) cItem.getAuxiliaryData().getCatalogRef();
			Date streetDate = (Date) skuItem.getPropertyValue(getPropertyManager().getStreetDate());
			Date currentDate = new Date();
			if (streetDate != null && currentDate.before(streetDate)) {
				String backOrderStatus = (String) skuItem.getPropertyValue(getPropertyManager().getBackOrderStatus());
				if(backOrderStatus != null && backOrderStatus.equalsIgnoreCase(TRUCommerceConstants.STRING_R)){
					pOutOfStockItemList.remove(cItem.getId());
				}
			}
		}
	}

	/**
	 * This method will determines, if sku id is ispu eligible and instock.
	 *
	 * @param pSkuId - SkuId
	 * @param pLocationId - LocationId
	 * @return true, if successful
	 */
	public boolean checkItemInStock(String pSkuId,String pLocationId) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::checkItemInStock() : START");
			vlogDebug("SKU ID : {0}, Location ID: {1}", pSkuId, pLocationId);
		}
		String s2s = null;
		RepositoryItem skuItem = null;
		if (StringUtils.isNotBlank(pSkuId)) {
			try {
				skuItem = getTRUCartModifierHelper().findSkuItem(pSkuId);
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError(" RepositoryException in @Class::TRUShoppingCartUtils::@method::checkItemInStock() :", e);
				}
			}
		}
		if(skuItem!=null){
			Boolean s2sValue=(Boolean) skuItem.getPropertyValue(getPropertyManager().getShipToStoreEligible());
			//check for ShipToStoreEligible flag from SKU
			if(s2sValue!=null && s2sValue.booleanValue())
			{
				s2s=TRUCommerceConstants.YES;
			}else{
				s2s=TRUCommerceConstants.NO;
			}
			//check for inStorePickUp flag from SKU
			String ispu=(String) skuItem.getPropertyValue(getPropertyManager().getItemInStorePickUp());
			if(TRUCommerceConstants.YES.equalsIgnoreCase(ispu) || TRUCommerceConstants.YES_STRING.equalsIgnoreCase(ispu) || TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(ispu)){
				ispu=TRUCommerceConstants.YES;
			}else{
				ispu=TRUCommerceConstants.NO;
			}
			int status = TRUCommerceConstants.INT_ZERO;
			if((s2s.equals(TRUCommerceConstants.YES) ||ispu.equals(TRUCommerceConstants.YES)) && StringUtils.isNotBlank(pLocationId)&& pLocationId.equalsIgnoreCase(TRUCommerceConstants.ONLINE)){
				try {
					status = getCoherenceInventoryManager().queryAvailabilityStatus(pSkuId);
				} catch (InventoryException e) {
					if (isLoggingError()) {
						logError(" RepositoryException in @Class::TRUShoppingCartUtils::@method::checkItemInStock() :", e);
					}
				}
				if(status == TRUCommerceConstants.INT_IN_STOCK || status == TRUCommerceConstants.INT_LIMITED_STOCK || status == TRUCommerceConstants.INT_PRE_ORDER ){
					return true;
				}
			}else{
				return true;
			}
			if (isLoggingDebug()) {
				vlogDebug("Exiting - TRUShoppingCartUtils :: checkItemInStock method to determine if the commerce Item is eligible for ISPU or not.");					
			}
		}
		return false;
	}
	
	/**
	 * Gets the out of stock shippin group Relationship item list.
	 * 
	 * @param pDeletedList
	 *            the deleted list
	 * @param pOrder
	 *            the order
	 * @param pCookieLocationId
	 *            the cookie location id
	 * @return the out of stock item list
	 * @throws RepositoryException
	 *             the repository exception
	 */
	@SuppressWarnings("unchecked")
	public Set<String> getOutOfStockSGRelItemList(Set<String> pDeletedList, Order pOrder, String pCookieLocationId)
			throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug(
					"Entering into method TRUShoppingCartUtils.getOutOfStockSGRelItemList :: "
							+ "START --> DeletedList, pOrder, pCookieLocationId {0} {1} {2}",
					pDeletedList, pOrder, pCookieLocationId);
		}
		final Set<String> outOfStockShipRelItemList = new HashSet<String>();
		List<TRUShippingGroupCommerceItemRelationship> sgCiRels = null;
		final List<TRUInventoryInfo> cartInventoryRequest = new ArrayList<TRUInventoryInfo>();
		List<TRUInventoryInfo> inventoryResponse = null;
		final Map<String, Long> skuIdAndLocIdMap = new HashMap<String, Long>();
		Set<TRUInventoryInfo> inventoryInfoSet = new HashSet<TRUInventoryInfo>();
		try {
			sgCiRels = getTRUCartModifierHelper().getShippingGroupCommerceItemRelationships(pOrder);
			if ((sgCiRels != null && !sgCiRels.isEmpty())) {
				final int sgCiRelsCount = sgCiRels.size();
				for (int i = TRUCommerceConstants.INT_ZERO; i < sgCiRelsCount; ++i) {
					final TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) sgCiRels
							.get(i);
					final ShippingGroup sg = ((ShippingGroupCommerceItemRelationship) sgCiRel).getShippingGroup();
					final TRUCommerceItemImpl item = (TRUCommerceItemImpl) sgCiRel.getCommerceItem();					
					if (!pDeletedList.contains(item.getCatalogRefId())) {
						if (item instanceof TRUGiftWrapCommerceItem || item instanceof TRUDonationCommerceItem) {
							continue;
						}
						if (isLoggingDebug()) {
							vlogDebug("@Class::TRUShoppingCartUtils::@method::getOutOfStockItemList() shippingGroupType:  {0}", sg);
						}
						if ((sg instanceof TRUHardgoodShippingGroup || sg instanceof TRUChannelHardgoodShippingGroup) &&
								 !skuIdAndLocIdMap.containsKey(item.getCatalogRefId())) {
							final TRUInventoryInfo inveReq = new TRUInventoryInfo(item.getCatalogRefId(), sgCiRel.getQuantity(),
									null);
							cartInventoryRequest.add(inveReq);
							skuIdAndLocIdMap.put(item.getCatalogRefId(), sgCiRel.getQuantity());
						} else if ((sg instanceof TRUInStorePickupShippingGroup || sg instanceof TRUChannelInStorePickupShippingGroup)) {
							final TRUInStorePickupShippingGroup ispu = (TRUInStorePickupShippingGroup) sg;
							//Start :MVP8 changes for checking the storeEligible flag before placing the order 
							RepositoryItem skuItem=(RepositoryItem) item.getAuxiliaryData().getCatalogRef();							
							if(isISPUItemInOOS(skuItem)){
								outOfStockShipRelItemList.add(sgCiRel.getId());
							//End :MVP8 changes for checking the storeEligible flag before placing the order 
							}else{
								final long locationInv = getCoherenceInventoryManager().queryStockLevel(item.getCatalogRefId(),
										ispu.getLocationId());
								ispu.setWarhouseLocationCode(Long.toString(getTRUCartModifierHelper().getWareHouseLocationId(ispu.getLocationId()).longValue()));
								if (isLoggingDebug()) {
									vlogDebug("locationInv : {0} WarhouseLocationCode : {1}", locationInv,ispu.getWarhouseLocationCode());
								}
								
								//Start :For Defect - TUW-75495
								RepositoryItem storeItem = null;
								String wareHouseMsg= TRUCommerceConstants.EMPTY_STRING;
								storeItem = getLocationRepository().getItem(ispu.getLocationId(), getPropertyManager().getStoreItemDescriptorName());
								Map<String,TRUStoreItemVO> storeItemMap= getLocationTools().getOnHandQtyMap(Arrays.asList(storeItem), item.getCatalogRefId());
								if(!storeItemMap.isEmpty() && storeItem != null){
								wareHouseMsg = getLocationTools().getStoreHoursMsg(storeItem,storeItemMap.get(storeItem.getRepositoryId()));
								}
								sgCiRel.setWarehouseMessage(wareHouseMsg);
								
								//End :For Defect - TUW-75495
								if (locationInv <= 0 && ispu.getWarhouseLocationCode() != null) {
									final long wareHouseInv = getCoherenceInventoryManager().queryStockLevel(item.getCatalogRefId(),
											ispu.getWarhouseLocationCode());
									if (isLoggingDebug()) {
										vlogDebug("@Class::TRUShoppingCartUtils::@method::getOutOfStockItemList() wareHouseInv:  {0}", wareHouseInv);
									}
									if (wareHouseInv > 0) {
										sgCiRel.setWarehousePickup(Boolean.TRUE);
									}else{
										sgCiRel.setWarehousePickup(Boolean.FALSE);
									}
								}
								if (!skuIdAndLocIdMap.containsKey(item.getCatalogRefId() + TRUCommerceConstants.UNDER_SCORE
										+ ispu.getLocationId())) {
									final TRUInventoryInfo inveReq = new TRUInventoryInfo(item.getCatalogRefId(),
											sgCiRel.getQuantity(), ispu.getLocationId());
									cartInventoryRequest.add(inveReq);
									skuIdAndLocIdMap.put(
											item.getCatalogRefId() + TRUCommerceConstants.UNDER_SCORE + ispu.getLocationId(),
											sgCiRel.getQuantity());
								}
							}							
						}
					}
				}
				if (!cartInventoryRequest.isEmpty()) {
					inventoryResponse = getCoherenceInventoryManager().validateBulkStockLevel(cartInventoryRequest);
				}/* else {
					return outOfStockShipRelItemList;
				}*/
				if (isLoggingDebug()) {
					vlogDebug("@Class::TRUShoppingCartUtils::@method::getOutOfStockItemList() :  {0}", inventoryResponse);
				}
				inventoryInfoSet = getTRUCartModifierHelper().buildInventoryInfoSetForInventoryCheck(skuIdAndLocIdMap,
						cartInventoryRequest, inventoryResponse);
				for (TRUInventoryInfo inventoryInfo : inventoryInfoSet) {
					addDetailsToOutOfStockShipRelList(pOrder, outOfStockShipRelItemList, inventoryInfo);
				}
				if (isLoggingDebug()) {
					vlogDebug(
							"@Class::TRUShoppingCartUtils::@method::getOutOfStockSGRelItemList() outOfStockShipRelItemList:  {0}",
							outOfStockShipRelItemList);
				}
				((TRUOrderTools) getCommerceItemManager().getOrderTools()).generateOutOfStockItemsBySGRelIds(
						outOfStockShipRelItemList, pOrder);
			}
		} catch (CommerceException e) {
			if (isLoggingError()) {
				logError("Commerce Exception in TRUShoppingCartUtils.getOutOfStockSGRelItemList", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting into method TRUShoppingCartUtils.getOutOfStockSGRelItemList :: END");
		}
		return outOfStockShipRelItemList;
	}

	/**
	 * This method is used to add details to OutOfStockRelList Item.
	 * 
	 * @param pOrder
	 *            Order Object
	 * @param pOutOfStockShipRelItemList
	 *            list of outOfStockShipRelItem
	 * @param pInventoryInfo
	 *            Object of TRUInventoryInfo
	 * @throws CommerceException
	 *             -If Any
	 */
	private void addDetailsToOutOfStockShipRelList(Order pOrder,
			final Set<String> pOutOfStockShipRelItemList,
			TRUInventoryInfo pInventoryInfo) throws CommerceException {
		final List<TRUShippingGroupCommerceItemRelationship> sgrels = getTRUCartModifierHelper()
				.getShippingGroupCommerceItemRelationships(pOrder);
		if (!sgrels.isEmpty()) {
			for (TRUShippingGroupCommerceItemRelationship sgrel : sgrels) {
				final ShippingGroup invSG = sgrel.getShippingGroup();
				final TRUCommerceItemImpl cItem = (TRUCommerceItemImpl) sgrel.getCommerceItem();
				if (pInventoryInfo.getLocationId() == null &&
						 (invSG instanceof TRUHardgoodShippingGroup || invSG instanceof TRUChannelHardgoodShippingGroup) &&
						 cItem.getCatalogRefId().equals(pInventoryInfo.getCatalogRefId())) {
					pOutOfStockShipRelItemList.add(sgrel.getId());
				} else if (pInventoryInfo.getLocationId() != null &&
						 (invSG instanceof TRUInStorePickupShippingGroup || invSG instanceof TRUChannelInStorePickupShippingGroup) &&
						 cItem.getCatalogRefId().equals(pInventoryInfo.getCatalogRefId())) {
					final TRUInStorePickupShippingGroup ispsg = (TRUInStorePickupShippingGroup) invSG;
					if (pInventoryInfo.getLocationId().equals(ispsg.getWarhouseLocationCode())) {
						pOutOfStockShipRelItemList.add(sgrel.getId());
					}
				}
			}
		}
	}
	
	
	/**
	 * Gets the in stock list from out of stock items.
	 *
	 * @param pOutOfStockItemsList the out of stock items list
	 * @param pCookieLocationId            the cookie location id
	 * @return the in stock list from out of stock items
	 * @throws RepositoryException             the repository exception
	 * @throws InventoryException             the inventory exception
	 */
	public TRUAddCommerceItemInfo[] getInStockListFromOutOfStockItems(List<TRUOutOfStockItem> pOutOfStockItemsList,
			String pCookieLocationId) throws RepositoryException, InventoryException {
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUShoppingCartUtils::@method::getInStockListFromOutOfStockItems() : START :"
							+ " pOutOfStockItems ,pCookieLocationId --> {0} {1} ",
					pOutOfStockItemsList, pCookieLocationId);
		}
		final Set<TRUOutOfStockItem> inStockList = new HashSet<TRUOutOfStockItem>();
		int status = TRUCommerceConstants.INT_ZERO;
		TRUAddCommerceItemInfo[] itemInfoForTheAddItems = null;
		for (TRUOutOfStockItem outOfStockItem : pOutOfStockItemsList) {
			if (null != outOfStockItem.getRepositoryItem() && StringUtils.isNotBlank(outOfStockItem.getId()) &&
					 null != outOfStockItem.getPropertyValue(getPropertyManager().getCatalogRefIdPropertyName())) {
				final String skuId = (String) outOfStockItem.getPropertyValue(getPropertyManager().getCatalogRefIdPropertyName());
				final String productId = (String) outOfStockItem.getPropertyValue(getPropertyManager().getProductIdPropertyName());
				final String locationId = (String) outOfStockItem.getPropertyValue(getPropertyManager().getLocationIdPropertyName());
				final String siteId = (String) outOfStockItem.getPropertyValue(getPropertyManager().getSiteIdPropertyName());
				if (!StringUtils.isBlank(skuId) && StringUtils.isNotBlank(productId)) {
					String backOrderStatus = getBackOrderStatus(skuId);
					if (!StringUtils.isBlank(locationId)) {
						if (backOrderStatus != null && !backOrderStatus.equalsIgnoreCase(TRUCommerceConstants.R)) {
							status = TRUCommerceConstants.INT_PRE_ORDER;
						} else if (backOrderStatus != null && !backOrderStatus.equalsIgnoreCase(TRUCommerceConstants.N)) {
							status = TRUCommerceConstants.INT_OUT_OF_STOCK;
						} else {
							status = getCoherenceInventoryManager().queryAvailabilityStatus(skuId, locationId);
						}
						if (status == TRUCommerceConstants.INT_OUT_OF_STOCK || status == TRUCommerceConstants.ZERO) {
							long wareHouseCount = TRUCommerceConstants.INT_ZERO;
							wareHouseCount = getCoherenceInventoryManager().queryStockLevel(skuId,
									Long.toString(Math.round(getTRUCartModifierHelper().getWareHouseLocationId(
											locationId))));
							if (wareHouseCount > TRUCommerceConstants.ZERO) {
								status = TRUCommerceConstants.INT_IN_STOCK;
							}
						}
					} else {
						if (backOrderStatus != null && !backOrderStatus.equalsIgnoreCase(TRUCommerceConstants.R)) {
							status = TRUCommerceConstants.INT_PRE_ORDER;
						} else if (backOrderStatus != null && !backOrderStatus.equalsIgnoreCase(TRUCommerceConstants.N)) {
							status = TRUCommerceConstants.INT_OUT_OF_STOCK;
						} else {
							status = getCoherenceInventoryManager().queryAvailabilityStatus(skuId);
						}
					}
					if (status == TRUCommerceConstants.INT_OUT_OF_STOCK || status == TRUCommerceConstants.ZERO) {
						long stockCount = getTRUCartModifierHelper().getInventoryBasedOnChannelAvailability(skuId, productId,
								siteId, locationId, pCookieLocationId);
						if (stockCount > TRUCommerceConstants.NUM_ZERO) {
							inStockList.add(outOfStockItem);
						}
					} else {
						inStockList.add(outOfStockItem);
					}
					if(backOrderStatus != null && backOrderStatus.equalsIgnoreCase(TRUCommerceConstants.STRING_R)){
						inStockList.add(outOfStockItem);
					}
				}
			}
		}
		if (!inStockList.isEmpty()) {
			itemInfoForTheAddItems = getItemInfoForTheAddItems(inStockList);
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShoppingCartUtils::@method::getInStockListFromOutOfStockItems() : END : {0} ", inStockList);
		}
		return itemInfoForTheAddItems;
	}

	/**
	 * Gets the item info for the add items.
	 * 
	 * @param pOutOfStockItems
	 *            the out of stock items
	 * @return the item info for the add items
	 */
	public TRUAddCommerceItemInfo[] getItemInfoForTheAddItems(Set<TRUOutOfStockItem> pOutOfStockItems) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShoppingCartUtils::@method::getItemInfoForTheAddItems() : START : pOutOfStockItems -->",
					pOutOfStockItems);
		}
		final List<TRUAddCommerceItemInfo> itemInfos = new ArrayList<TRUAddCommerceItemInfo>();
		for (TRUOutOfStockItem outOfStockItem : pOutOfStockItems) {
			if (null != outOfStockItem &&
					 null != outOfStockItem.getRepositoryItem() && StringUtils.isNotBlank(outOfStockItem.getId()) &&
					 null != outOfStockItem.getPropertyValue(getPropertyManager().getCatalogRefIdPropertyName()) &&
					 null != outOfStockItem.getPropertyValue(getPropertyManager().getProductIdPropertyName()) &&
					 null != outOfStockItem.getPropertyValue(getPropertyManager().getQuantityPropertyName())) {
				final TRUAddCommerceItemInfo itemInfo = new TRUAddCommerceItemInfo();
				final String skuId = (String) outOfStockItem.getPropertyValue(getPropertyManager().getCatalogRefIdPropertyName());
				final String prodId = (String) outOfStockItem.getPropertyValue(getPropertyManager().getProductIdPropertyName());
				final long qty = (long) outOfStockItem.getPropertyValue(getPropertyManager().getQuantityPropertyName());
				itemInfo.setCatalogRefId(skuId);
				itemInfo.setProductId(prodId);
				itemInfo.setQuantity(qty);
				itemInfo.setSiteId((String) outOfStockItem.getPropertyValue(getPropertyManager().getSiteIdPropertyName()));
				itemInfo.setCommerceItemType(getCommerceItemManager().getOrderTools().getDefaultCommerceItemType());
				if (null != outOfStockItem.getPropertyValue(getPropertyManager().getLocationIdPropertyName())) {
					itemInfo.setLocationId((String) outOfStockItem.getPropertyValue(getPropertyManager()
							.getLocationIdPropertyName()));
				}
				if (null != outOfStockItem.getPropertyValue(getPropertyManager().getChannelId())) {
					itemInfo.setChannelId((String) outOfStockItem.getPropertyValue(getPropertyManager().getChannelId()));
				}
				if (null != outOfStockItem.getPropertyValue(getPropertyManager().getChannelType())) {
					itemInfo.setChannelType((String) outOfStockItem.getPropertyValue(getPropertyManager().getChannelType()));
				}
				if (null != outOfStockItem.getPropertyValue(getPropertyManager().getChannelName())) {
					itemInfo.setChannelName((String) outOfStockItem.getPropertyValue(getPropertyManager().getChannelName()));
				}
				if (null != outOfStockItem.getPropertyValue(getPropertyManager().getChannelUserName())) {
					itemInfo.setChannelUserName((String) outOfStockItem.getPropertyValue(getPropertyManager()
							.getChannelUserName()));
				}
				if (null != outOfStockItem.getPropertyValue(getPropertyManager().getChannelCoUserName())) {
					itemInfo.setChannelCoUserName((String) outOfStockItem.getPropertyValue(getPropertyManager()
							.getChannelCoUserName()));
				}
				if (null != outOfStockItem.getPropertyValue(getPropertyManager().getSourceChannel())) {
					itemInfo.setSourceChannel((String) outOfStockItem.getPropertyValue(getPropertyManager()
							.getSourceChannel()));
				}

				itemInfos.add(itemInfo);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::getItemInfoForTheAddItems() : END ");
		}
		return itemInfos.toArray(new TRUAddCommerceItemInfo[itemInfos.size()]);
	}

	/**
	 * Gets the qty adjusted info from merge.
	 * 
	 * @param pAdjustedSkuList
	 *            the adjusted sku list
	 * @return the qty adjusted info from merge
	 */
	public List<String> getQtyAdjustedInfoFromMerge(Map<String, String> pAdjustedSkuList) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShoppingCartUtils::@method::getQtyAdjustedInfoFromMerge() : START : pAdjustedSkuList -->",
					pAdjustedSkuList);
		}
		final List<String> adjustedItemList = new ArrayList<String>();
		try {
			final String inventoryInfoMessage = getErrorHandlerManager().getErrorMessage(
					TRUCommerceConstants.TRU_SHOPPINGCART_QTY_ADJUSTED_IN_MERGE_WITH_INVENTORY_LIMIT);
			final String customerLimitInfoMessage = getErrorHandlerManager().getErrorMessage(
					TRUCommerceConstants.TRU_SHOPPINGCART_QTY_ADJUSTED_IN_MERGE_WITH_PURCHASE_LIMIT);
			String errorMessage = null;
			RepositoryItem skuItem = null;
			String skuDisplayName = null;
			String value = null;
			final Iterator entries = pAdjustedSkuList.entrySet().iterator();
			while (entries.hasNext()) {
				final Map.Entry entry = (Map.Entry) entries.next();
				skuItem = getTRUCartModifierHelper().findSkuItem((String) entry.getKey());
				skuDisplayName = (String) skuItem.getPropertyValue(getPropertyManager().getDisplayName());
				value = (String) entry.getValue();
				if (value.contains(TRUCommerceConstants.TRU_SHOPPINGCART_AUTOCORRECT_WITH_INVENTORY)) {
					errorMessage = MessageFormat.format(inventoryInfoMessage, skuDisplayName);
				} else {
					errorMessage = MessageFormat.format(customerLimitInfoMessage, skuDisplayName);
				}
				adjustedItemList.add(errorMessage);
			}

		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError(" RepositoryException in @Class::TRUShoppingCartUtils::@method::getRemovedItemMessages() :", e);
			}
		} catch (SystemException e) {
			if (isLoggingError()) {
				logError(" SystemException in @Class::TRUShoppingCartUtils::@method::getRemovedItemMessages() :", e);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShoppingCartUtils::@method::getQtyAdjustedInfoFromMerge() : END : adjustedItemList -->",
					adjustedItemList);
		}
		return adjustedItemList;
	}

	/**
	 * Gets the location id from cookie.
	 * 
	 * @param pRequest
	 *            the request
	 * @return the location id from cookie
	 */
	public String getLocationIdFromCookie(DynamoHttpServletRequest pRequest) {
		String locationIdParam = null;
		final String favStore = pRequest.getCookieParameter(TRUCheckoutConstants.FAV_STORE);
		if (!StringUtils.isBlank(favStore)) {
			final String[] favStoreArry = StringUtils.splitStringAtCharacter(favStore, TRUCheckoutConstants.CHAR_OR);
			if (favStoreArry != null && favStoreArry.length >= TRUCheckoutConstants.INDEX_TWO) {
				locationIdParam = favStoreArry[TRUCheckoutConstants.INT_ONE];
			}
		}
		return locationIdParam;
	}

	/**
	 * Sets the commerce item manager.
	 * 
	 * @param pCommerceItemManager
	 *            the new commerce item manager
	 */
	public void setCommerceItemManager(CommerceItemManager pCommerceItemManager) {
		mCommerceItemManager = pCommerceItemManager;
	}

	/**
	 * Gets the commerce item manager.
	 * 
	 * @return the commerce item manager
	 */
	public CommerceItemManager getCommerceItemManager() {
		return mCommerceItemManager;
	}

	/**
	 * Gets the property manager.
	 * 
	 * @return the property manager
	 */
	public TRUCommercePropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 * 
	 * @param pPropertyManager
	 *            the new property manager
	 */
	public void setPropertyManager(TRUCommercePropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * Gets the error handler manager.
	 * 
	 * @return the error handler manager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * Sets the error handler manager.
	 * 
	 * @param pErrorHandlerManager
	 *            the new error handler manager
	 */
	public void setErrorHandlerManager(DefaultErrorHandlerManager pErrorHandlerManager) {
		mErrorHandlerManager = pErrorHandlerManager;
	}

	/**
	 * Gets the location repository.
	 * 
	 * @return the mLocationRepository
	 */
	public Repository getLocationRepository() {
		return mLocationRepository;
	}

	/**
	 * Gets the shipping destination vo by relationship.
	 *
	 * @param pShippingDestinationVOs            the shipping destination v os
	 * @param pSgCiRel            the sg ci rel
	 * @param pOrder            the order
	 * @param pProfile            the profile
	 * @param pPage            the page
	 * @param pIsAPIRequest            the is api request
	 * @param pCartItemDetailVOMap            the cart item detail vo map
	 */
	public void getShippingDestinationVOByRelationship(Map<String, TRUShippingDestinationVO> pShippingDestinationVOs,
			TRUShippingGroupCommerceItemRelationship pSgCiRel, TRUOrderImpl pOrder, RepositoryItem pProfile, String pPage,
			boolean pIsAPIRequest, Map<String, TRUCartItemDetailVO> pCartItemDetailVOMap) {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUShoppingCartUtils.getShippingDestinationVOByRelationship :: START");
			vlogDebug(
					"INPUT VALUES: getShippingDestinationVOByRelationship pShippingDestinationVOs: {0} pSgCiRel: {1} pOrder: {2} pPage: {3}",
					pShippingDestinationVOs, pSgCiRel, pOrder, pPage);
		}
		final ShippingGroup shippingGroup = pSgCiRel.getShippingGroup();
		if (shippingGroup instanceof TRUHardgoodShippingGroup) {
			final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
			String nickName = hardgoodShippingGroup.getNickName();

			// When no address and ship to multi address - nickname will be null
			if (StringUtils.isBlank(nickName)) {
				nickName = pSgCiRel.getId();
			}
			TRUShippingDestinationVO shippingDestinationVO = null;
			if (pShippingDestinationVOs.get(nickName) != null) {
				shippingDestinationVO = pShippingDestinationVOs.get(nickName);
			} else {
				shippingDestinationVO = new TRUShippingDestinationVO();
				shippingDestinationVO.setAddress(hardgoodShippingGroup.getShippingAddress());
				shippingDestinationVO.setRegistryAddress(hardgoodShippingGroup.isRegistryAddress());
				shippingDestinationVO.setShippingGroupType(hardgoodShippingGroup.getShippingGroupClassType());
				shippingDestinationVO.setShipToName(hardgoodShippingGroup.getShipToName());
				pShippingDestinationVOs.put(nickName, shippingDestinationVO);
			}
			shippingDestinationVO.setItemsCount(shippingDestinationVO.getItemsCount() + pSgCiRel.getQuantity());
			updateDetailsWithShippingDestinationVO(shippingDestinationVO, pSgCiRel, pOrder, pProfile, pPage, pIsAPIRequest,
					pCartItemDetailVOMap);
		}
		// populate the Vo for instore pickup shipping group
		if (shippingGroup instanceof TRUInStorePickupShippingGroup && !pPage.equalsIgnoreCase(TRUCommerceConstants.SHIPPING)) {
			final TRUInStorePickupShippingGroup instoreShippingGroup = (TRUInStorePickupShippingGroup) shippingGroup;
			final String locationId = instoreShippingGroup.getLocationId();
			final String shippingGroupType = instoreShippingGroup.getShippingGroupClassType();
			final String key = locationId + TRUConstants.UNDER_SCORE + shippingGroupType;
			TRUShippingDestinationVO inStoreDestinationVO = null;
			if (pShippingDestinationVOs.get(key) != null) {
				inStoreDestinationVO = pShippingDestinationVOs.get(key);
			} else {
				inStoreDestinationVO = new TRUShippingDestinationVO();
				inStoreDestinationVO.setShippingGroupType(instoreShippingGroup.getShippingGroupClassType());
				pShippingDestinationVOs.put(key, inStoreDestinationVO);
			}
			inStoreDestinationVO.setItemsCount(inStoreDestinationVO.getItemsCount() + pSgCiRel.getQuantity());
			updateDetailsWithShippingDestinationVO(inStoreDestinationVO, pSgCiRel, pOrder, pProfile, pPage, pIsAPIRequest,
					pCartItemDetailVOMap);
		}
		if (isLoggingDebug()) {
			logDebug("END:TRUShoppingCartUtils.getShippingDestinationVOByRelationship method");
		}
	}

	/**
	 * Update details with shipping destination vo.
	 * 
	 * @param pShippingDestinationVO
	 *            the shipping destination vo
	 * @param pSgRel
	 *            the sg rel
	 * @param pOrder
	 *            the order
	 * @param pProfile
	 *            the profile
	 * @param pPage
	 *            the page
	 * @param pIsAPIRequest
	 *            the is api request
	 * @param pCartItemDetailVOMap
	 *            the cart item detail vo map
	 */
	private void updateDetailsWithShippingDestinationVO(TRUShippingDestinationVO pShippingDestinationVO,
			TRUShippingGroupCommerceItemRelationship pSgRel, Order pOrder, RepositoryItem pProfile, String pPage,
			boolean pIsAPIRequest, Map<String, TRUCartItemDetailVO> pCartItemDetailVOMap) {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUShoppingCartUtils.updateDetailsWithShippingDestinationVO :: START");
			vlogDebug(
					"INPUT VALUES: updateDetailsWithShippingDestinationVO pShippingDestinationVO: {0} pSgRel: {1} pOrder: {2} pPage: {3}",
					pShippingDestinationVO, pSgRel, pOrder, pPage);
		}
		if (pShippingDestinationVO != null && pOrder != null && pSgRel != null) {
			final List<TRUCartItemDetailVO> shoppingCartList = new ArrayList<TRUCartItemDetailVO>();
			final List<TRUCartItemDetailVO> outOfStockItemList = new ArrayList<TRUCartItemDetailVO>();
			final CommerceItem cItem = pSgRel.getCommerceItem();
			try {
				// TODO Need to check with BA about this as part of Shipping page
				if (cItem instanceof TRUDonationCommerceItem &&
						 !pPage.equalsIgnoreCase(TRUCommerceConstants.SHIPPING)) {
					final TRUCartItemDetailVO cartItemDetailVO = getDonationItemsVO((TRUDonationCommerceItem) cItem);
					shoppingCartList.add(cartItemDetailVO);
				}
				if (pPage.equalsIgnoreCase(TRUCommerceConstants.SHIPPING)) {
					final List<TRUCommerceItemMetaInfo> metaInfos = pSgRel.getMetaInfo();
					for (TRUCommerceItemMetaInfo metaInfo : metaInfos) {
						setDetailsToCartItemVo(pSgRel, pOrder, pIsAPIRequest,
								pCartItemDetailVOMap, shoppingCartList,
								outOfStockItemList, metaInfo);
					}
				} else {
					final List<TRUCommerceItemMetaInfo> metaInfos = pSgRel.getMetaInfo();
					int i= TRUCommerceConstants.INT_ZERO;
					for (TRUCommerceItemMetaInfo metaInfo : metaInfos) {
						final TRUCartItemDetailVO cartItemDetailVO = getTRUCartModifierHelper()
								.createIndividualItemDetailVOByRelatioShip(pSgRel, pOrder, metaInfo);
						if (pIsAPIRequest) {
							final Site site = SiteContextManager.getCurrentSite();
							
							
						getBrandName(cartItemDetailVO);
								
							
							final String pdpUrl = getPDPURL(
									(String) cartItemDetailVO.getSkuItem().getPropertyValue(
											getCatalogProperties().getOnlinePID()), site);
							if (StringUtils.isNotBlank(pdpUrl)) {
								cartItemDetailVO.setItemURL(pdpUrl);
							}
						}
						if(pPage.equalsIgnoreCase(TRUCommerceConstants.REVIEW_PAGE_NAME)){
							cartItemDetailVO.setUniqueId(pSgRel.getId() + TRUConstants.UNDER_SCORE
									+ metaInfo.getId() + TRUConstants.UNDER_SCORE + i++);
						}
						if (!StringUtils.isBlank(cartItemDetailVO.getInventoryStatus()) &&
								 cartItemDetailVO.getInventoryStatus().equalsIgnoreCase(
										TRUCommerceConstants.OUT_OF_STOCK)) {
							outOfStockItemList.add(cartItemDetailVO);
						} else {
							shoppingCartList.add(cartItemDetailVO);
						}
					}
				}
			} catch (CommerceItemNotFoundException commerceItemNotFoundException) {
				vlogError(
						"CommerceItemNotFoundException in TRUDisplayOrderDetailsDroplet.updateDetailsWithShippingDestinationVO()::",
						commerceItemNotFoundException);
			} catch (InvalidParameterException invalidParameterException) {
				vlogError("InvalidParameterException in TRUDisplayOrderDetailsDroplet.removeItemsFromOrder()::",
						invalidParameterException);
			}
			updateShippingDestinationVO(pOrder, pProfile, pSgRel, pShippingDestinationVO, shoppingCartList,
					outOfStockItemList, pIsAPIRequest);
		}
		if (isLoggingDebug()) {
			logDebug("END:TRUShoppingCartUtils.updateDetailsWithShippingDestinationVO method");
		}
	}

	/**
	 * This method is used to GET BRAND NAME
	 * 
	 * @param cartItemDetailVO
	 * @throws InventoryException
	 */
	public void getBrandName( TRUCartItemDetailVO cartItemDetailVO) {
		
		if (isLoggingDebug()) {
			logDebug("END:TRUShoppingCartUtils.getBrandName method");
		}
		RepositoryItem productItem = cartItemDetailVO.getProductItem();
		List<RepositoryItem> instockSKUs =null;
		try {
			List<RepositoryItem> childSKUs = (List<RepositoryItem>) productItem.getPropertyValue(getCatalogProperties().getChildSKUsPropertyName());
			List<Long> stockLevelList = new ArrayList<Long>();
			Map<Long, RepositoryItem> inStockToItemMap = new HashMap<Long, RepositoryItem>();
			instockSKUs = getProductPageUtil().getCatalogTools().getAllInstockItems(childSKUs, true, null);
			if(instockSKUs == null || instockSKUs.isEmpty()) {
			instockSKUs = getProductPageUtil().getCatalogTools().getAllInstockItems(childSKUs, false, null);	
			}
			for (RepositoryItem instckSKU : instockSKUs) {
				long stockLevel = getCoherenceInventoryManager().queryStockLevel(instckSKU.getRepositoryId());
				stockLevelList.add(stockLevel);
				inStockToItemMap.put(stockLevel, instckSKU);
			}
			RepositoryItem defaultSku = null;
			long maxinventory = TRUCommerceConstants.INT_ZERO;
			if (stockLevelList != null && !stockLevelList.isEmpty()) {
				if (stockLevelList.contains(TRUCommerceConstants.LONG_MINUS_ONE)) {
					defaultSku = inStockToItemMap.get(TRUCommerceConstants.LONG_MINUS_ONE);
				} else {
					maxinventory = Collections.max(stockLevelList);
					defaultSku = inStockToItemMap.get(maxinventory);
				}
			}
			String brandName = TRUCommerceConstants.EMPTY_SPACE;
			if(null != defaultSku && defaultSku.getPropertyValue(getCatalogProperties().getProductBrandNamePrimary()) != null) {
				brandName = (String) defaultSku.getPropertyValue(getCatalogProperties().getProductBrandNamePrimary());	
			}
			if (StringUtils.isNotBlank(brandName)) {
				cartItemDetailVO.setBrandName(brandName);
			}
		} catch (InventoryException inventoryException) {
			vlogError("InventoryException in TRUShoppingCartUtils.getBrandName()::",
					inventoryException);
		}
		
		if (isLoggingDebug()) {
			logDebug("END:TRUShoppingCartUtils.getBrandName method");
		}
	}

	/**
	 * This method is used to add ShippingGroup details to CartItemVO Object.
	 * 
	 * @param pSgRel
	 *            The ShippingGroupCommerceItemRelationship Object
	 * @param pOrder
	 *            The Order object
	 * @param pIsAPIRequest
	 *            boolean value of IsAPIRequest
	 * @param pCartItemDetailVOMap
	 *            TRUCartItemDetailVO Map Object
	 * @param pShoppingCartList
	 *            List of TRUCartItemDetailVO
	 * @param pOutOfStockItemList
	 *            List of outOfStockItemList
	 * @param pMetaInfo
	 *            Object of TRUCommerceItemMetaInfo
	 * @throws CommerceItemNotFoundException
	 *             -If Any
	 * @throws InvalidParameterException
	 *             -If Any
	 */
	private void setDetailsToCartItemVo(
			TRUShippingGroupCommerceItemRelationship pSgRel, Order pOrder,
			boolean pIsAPIRequest,
			Map<String, TRUCartItemDetailVO> pCartItemDetailVOMap,
			final List<TRUCartItemDetailVO> pShoppingCartList,
			final List<TRUCartItemDetailVO> pOutOfStockItemList,
			TRUCommerceItemMetaInfo pMetaInfo)
			throws CommerceItemNotFoundException, InvalidParameterException {
		final ShippingGroup shippingGroup = pSgRel.getShippingGroup();
		final TRUCommercePropertyManager propertyManager = getPropertyManager();
		final long metaInfoQty = (long) pMetaInfo.getPropertyValue(propertyManager.getQuantityPropertyName());
		for (int i = TRUCommerceConstants.INT_ZERO; i < metaInfoQty; i++) {
			final TRUCartItemDetailVO cartItemDetailVO = getTRUCartModifierHelper()
					.createIndividualItemDetailVOByRelatioShip(pSgRel, pOrder, pMetaInfo);
			cartItemDetailVO.setParentQuantity(pSgRel.getQuantity());
			cartItemDetailVO.setQuantity(metaInfoQty);
			cartItemDetailVO.setShipGroupId(shippingGroup.getId());
			cartItemDetailVO.setSelectedShippingMethod(shippingGroup.getShippingMethod());
			if (shippingGroup instanceof TRUHardgoodShippingGroup) {
				final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
				cartItemDetailVO.setShippingGroupName(hardgoodShippingGroup.getNickName());
				final TRUShippingPriceInfo priceInfo = (TRUShippingPriceInfo) hardgoodShippingGroup.getPriceInfo();
				if (priceInfo != null) {
					if(!hardgoodShippingGroup.isMarkerShippingGroup()) {
						cartItemDetailVO.setSelectedShippingMethodPrice(hardgoodShippingGroup.getActualAmount());
					} else {
						cartItemDetailVO.setSelectedShippingMethodPrice(priceInfo.getAmount());
					}
				} else {
					cartItemDetailVO.setSelectedShippingMethodPrice(hardgoodShippingGroup.getShippingPrice());
				}
			}
			if (pIsAPIRequest) {
				final Site site = SiteContextManager.getCurrentSite();
				getBrandName(cartItemDetailVO);
				final String pdpUrl = getPDPURL(
						(String) cartItemDetailVO.getSkuItem().getPropertyValue(
								getCatalogProperties().getOnlinePID()), site);
				if (StringUtils.isNotBlank(pdpUrl)) {
					cartItemDetailVO.setItemURL(pdpUrl);
				}
			}
			if (metaInfoQty == TRUCheckoutConstants.INT_ONE) {
				cartItemDetailVO.setTotalAmount((double) pMetaInfo.getPropertyValue(getPropertyManager()
						.getPricePropertyName()));
			} else {
				cartItemDetailVO.setTotalAmount((double) pMetaInfo.getPropertyValue(propertyManager
						.getUnitPricePropertyName()));
			}
			if (!StringUtils.isBlank(cartItemDetailVO.getInventoryStatus()) &&
					 cartItemDetailVO.getInventoryStatus().equalsIgnoreCase(
							TRUCommerceConstants.OUT_OF_STOCK)) {
				pOutOfStockItemList.add(cartItemDetailVO);
			} else {
				cartItemDetailVO.setUniqueId(pSgRel.getId() + TRUConstants.UNDER_SCORE
						+ pMetaInfo.getId() + TRUConstants.UNDER_SCORE + i);
				pShoppingCartList.add(cartItemDetailVO);
				pCartItemDetailVOMap.put(pSgRel.getId() + TRUConstants.UNDER_SCORE + pMetaInfo.getId()
						+ TRUConstants.UNDER_SCORE + i, cartItemDetailVO);
			}
		}
	}

	/**
	 * Update shipping destination vo.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pProfile
	 *            the profile
	 * @param pSgRel
	 *            the sg rel
	 * @param pShippingDestinationVO
	 *            the shipping destination vo
	 * @param pShoppingCartList
	 *            the shopping cart list
	 * @param pOutOfStockItemList
	 *            the out of stock item list
	 * @param pIsAPIRequest
	 *            the is api request
	 */
	private void updateShippingDestinationVO(Order pOrder, RepositoryItem pProfile,
			TRUShippingGroupCommerceItemRelationship pSgRel, TRUShippingDestinationVO pShippingDestinationVO,
			List<TRUCartItemDetailVO> pShoppingCartList, List<TRUCartItemDetailVO> pOutOfStockItemList, boolean pIsAPIRequest) {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUShoppingCartUtils.updateShippingDestinationVO :: START");
			vlogDebug(
					"INPUT VALUES: updateShippingDestinationVO pOrder: "
					+ "{0} pShippingGroup: {1} pSgRel: {2} pShippingDestinationVO: {3} pShoppingCartList: {4} pOutOfStockItemList: {5} ",
					pOrder, pSgRel, pShippingDestinationVO, pShoppingCartList, pOutOfStockItemList);
		}
		final ShippingGroup shippingGroup = pSgRel.getShippingGroup();
		if (shippingGroup instanceof TRUHardgoodShippingGroup) {
			final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;

			final String freightClass = getShippingHelper().getPurchaseProcessHelper().getShippingManager().getFreightClassFromCI(pSgRel.getCommerceItem());
			final String region = getShippingHelper().getPurchaseProcessHelper().getShippingManager().getShippingRegion(pShippingDestinationVO.getAddress());
			final List<TRUShipMethodVO> shipMethodVOs = getShippingHelper().getPurchaseProcessHelper().getShippingManager().getShipMethods(freightClass, region,
					pOrder, true);
			final TRUShipGroupKey shipGroupKey = getShippingHelper().findShipGroupKey(pOrder, pSgRel);

			if (pShippingDestinationVO.getShipmentVOMap().get(shipGroupKey.toString()) == null) {
				final TRUShipmentVO shipmentVO = new TRUShipmentVO();
				pShippingDestinationVO.getShipmentVOMap().put(shipGroupKey.toString(), shipmentVO);

				shipmentVO.setShipMethodVOs(shipMethodVOs);

				shipmentVO.setShippingGroup(hardgoodShippingGroup);
				shipmentVO.setSelectedShippingMethod(hardgoodShippingGroup.getShippingMethod());
				shipmentVO.setEstimateShipWindowMin(pSgRel.getEstimateShipWindowMin());
				shipmentVO.setEstimateShipWindowMax(pSgRel.getEstimateShipWindowMax());

				shipmentVO.setEstimateMinDate(getRelativeDate(pSgRel.getEstimateShipWindowMin()));
				shipmentVO.setEstimateMaxDate(getRelativeDate(pSgRel.getEstimateShipWindowMax()));

				double shippingPrice = TRUCommerceConstants.DOUBLE_ZERO;
				if (hardgoodShippingGroup.getPriceInfo() != null) {
					if(!hardgoodShippingGroup.isMarkerShippingGroup()) {
						shippingPrice = hardgoodShippingGroup.getActualAmount();
					} else {
						shippingPrice = hardgoodShippingGroup.getPriceInfo().getAmount();
					}
				}
				if(pSgRel.getStreetDate()!=null){
					shipmentVO.setStreetDate(pSgRel.getStreetDate());
				}
				shipmentVO.setSelectedShippingPrice(shippingPrice);
			}
			pShippingDestinationVO.getShipmentVOMap().get(shipGroupKey.toString()).getCartItemDetailVOs()
					.addAll(pShoppingCartList);
			pShippingDestinationVO.getShipmentVOMap().get(shipGroupKey.toString()).getCartOOSItemDetailVOs()
					.addAll(pOutOfStockItemList);
		} else if (shippingGroup instanceof TRUInStorePickupShippingGroup) {
			// setting the vo in case of instore pickup shipping group
			final TRUInStorePickupShippingGroup inStorePickupShippingGroup = (TRUInStorePickupShippingGroup) shippingGroup;
			final String loCationId = inStorePickupShippingGroup.getLocationId();
			final TRUShipGroupKey shipGroupKey = getShippingHelper().findShipGroupKey(pOrder, pSgRel);
			if (pShippingDestinationVO.getShipmentVOMap().get(shipGroupKey.toString()) == null) {
				pShippingDestinationVO.getShipmentVOMap().put(shipGroupKey.toString(), new TRUShipmentVO());
			}

			final TRUStorePickUpInfo storePickUpInfo = getStorePickUpInfo(pOrder, pProfile, inStorePickupShippingGroup);
			pShippingDestinationVO.getShipmentVOMap().get(shipGroupKey.toString()).setStorePickUpInfo(storePickUpInfo);

			// setting the store information in TRUStoreVo if the request is from Rest API
			if (StringUtils.isNotBlank(loCationId) && pIsAPIRequest) {
				final TRUStoreVO storeVo = new TRUStoreVO();
				populateStoreInfo(loCationId, storeVo);
				pShippingDestinationVO.getShipmentVOMap().get(shipGroupKey.toString()).setStoreDetails(storeVo);
			}
			pShippingDestinationVO.getShipmentVOMap().get(shipGroupKey.toString()).getCartItemDetailVOs()
					.addAll(pShoppingCartList);
			pShippingDestinationVO.getShipmentVOMap().get(shipGroupKey.toString()).getCartOOSItemDetailVOs()
					.addAll(pOutOfStockItemList);
			pShippingDestinationVO.getShipmentVOMap().get(shipGroupKey.toString()).setShippingGroup(inStorePickupShippingGroup);
		}
		if (isLoggingDebug()) {
			logDebug("END:TRUShoppingCartUtils.updateShippingDestinationVO method");
		}
	}

	/**
	 * Gets the store pick up info.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pProfile
	 *            the profile
	 * @param pShippingGroup
	 *            the shipping group
	 * @return the store pick up info
	 */
	public TRUStorePickUpInfo getStorePickUpInfo(Order pOrder, RepositoryItem pProfile, InStorePickupShippingGroup pShippingGroup) {

		final TRUOrderManager orderManager = (TRUOrderManager) getShippingHelper().getPurchaseProcessHelper().getOrderManager();
		final TRUProfileTools profileTools = (TRUProfileTools) orderManager.getOrderTools().getProfileTools();
		final TRUPropertyManager propertyManager = (TRUPropertyManager) profileTools.getPropertyManager();
		final boolean isAnonymousUser = profileTools.isAnonymousUser(pProfile);

		final TRUStorePickUpInfo storePickUpInfo = new TRUStorePickUpInfo();
		if (pShippingGroup instanceof InStorePickupShippingGroup) {
			final TRUInStorePickupShippingGroup inStorePickupShippingGroup = (TRUInStorePickupShippingGroup) pShippingGroup;
			try {
				if (inStorePickupShippingGroup != null) {
					storePickUpInfo.setShippingGroupId(inStorePickupShippingGroup.getId());
					storePickUpInfo.setShippingGroupType(inStorePickupShippingGroup.getShippingGroupClassType());
					if (StringUtils.isNotBlank(inStorePickupShippingGroup.getFirstName())) {
						storePickUpInfo.setFirstName(inStorePickupShippingGroup.getFirstName());
						storePickUpInfo.setLastName(inStorePickupShippingGroup.getLastName());
						storePickUpInfo.setPhoneNumber(inStorePickupShippingGroup.getPhoneNumber());
						storePickUpInfo.setEmail(inStorePickupShippingGroup.getEmail());

						if (inStorePickupShippingGroup.isAltAddrRequired()) {
							storePickUpInfo.setAlternateInfoRequired(inStorePickupShippingGroup.isAltAddrRequired());
							storePickUpInfo.setAlternateFirstName(inStorePickupShippingGroup.getAltFirstName());
							storePickUpInfo.setAlternateLastName(inStorePickupShippingGroup.getAltLastName());
							storePickUpInfo.setAlternatePhoneNumber(inStorePickupShippingGroup.getAltPhoneNumber());
							storePickUpInfo.setAlternateEmail(inStorePickupShippingGroup.getAltEmail());
						}
					} else {
						if (!isAnonymousUser) {
							storePickUpInfo.setFirstName((String) pProfile.getPropertyValue(propertyManager
									.getFirstNamePropertyName()));
							storePickUpInfo.setLastName((String) pProfile.getPropertyValue(propertyManager
									.getLastNamePropertyName()));
							storePickUpInfo.setEmail((String) pProfile.getPropertyValue(propertyManager
									.getEmailAddressPropertyName()));
						} else if(((TRUOrderImpl)pOrder).getHardgoodShippingGroupsCount() > TRUConstants._0) {
							final TRUHardgoodShippingGroup hardgoodShippingGroup = getShippingHelper().getPurchaseProcessHelper().getFirstHardgoodShippingGroup(
									pOrder);
							if (hardgoodShippingGroup != null && hardgoodShippingGroup.getShippingAddress() != null) {
								final ContactInfo contactInfo = (ContactInfo) hardgoodShippingGroup.getShippingAddress();
								if (StringUtils.isNotBlank(contactInfo.getFirstName())) {
									storePickUpInfo.setFirstName(contactInfo.getFirstName());
								}
								if (StringUtils.isNotBlank(contactInfo.getLastName())) {
									storePickUpInfo.setLastName(contactInfo.getLastName());
								}
								if (StringUtils.isNotBlank(contactInfo.getPhoneNumber())) {
									storePickUpInfo.setPhoneNumber(contactInfo.getPhoneNumber());
								}
								if (StringUtils.isNotBlank(contactInfo.getEmail())) {
									storePickUpInfo.setEmail(contactInfo.getEmail());
								}
							}
						}
					}
				}
			} catch (CommerceException comExc) {
				if (isLoggingError()) {
					logError("CommerceException in TRUInStorePickUpFormHandler.setShippingGroupIdForDisplay", comExc);
				}
			}
		}

		return storePickUpInfo;
	}

	/**
	 * create DonationItemsVO obj.
	 * 
	 * @param pCommerceItem
	 *            the commerce item
	 * @return the donation items vo
	 */
	public TRUCartItemDetailVO getDonationItemsVO(TRUDonationCommerceItem pCommerceItem) {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUShoppingCartUtils.getDonationItemsVO :: START");
			vlogDebug("INPUT VALUES: getDonationItemsVO pCommerceItem: {0}", pCommerceItem);
		}
		final TRUCartItemDetailVO itemDetailVO = new TRUCartItemDetailVO();
		final Double donationAmount = ((TRUDonationCommerceItem) pCommerceItem).getDonationAmount();
		itemDetailVO.setDonationAmount(donationAmount);
		itemDetailVO.setDonationItem(true);
		itemDetailVO.setCommerceItemId(pCommerceItem.getId());
		final RepositoryItem skuItem = (RepositoryItem) ((TRUDonationCommerceItem) pCommerceItem).getAuxiliaryData().getCatalogRef();
		if (skuItem != null) {
			String displayName = (String) skuItem.getPropertyValue(getPropertyManager().getDisplayNamePropertyName());
			itemDetailVO.setDonationItemDisplayName(displayName);
			String primaryImage = (String) skuItem.getPropertyValue(getPropertyManager().getPrimaryImage());
			if(StringUtils.isNotBlank(primaryImage)){
				itemDetailVO.setDonationItemImageUrl(primaryImage);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:TRUShoppingCartUtils.getDonationItemsVO method");
		}
		return itemDetailVO;
	}

	/**
	 * Populate store info.
	 * 
	 * @param pLocationId
	 *            the location id
	 * @param pStoreVo
	 *            the store vo
	 * @return the TRU store vo
	 */
	private TRUStoreVO populateStoreInfo(String pLocationId, TRUStoreVO pStoreVo) {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUShoppingCartUtils.populateStoreInfo :: START");
			vlogDebug("INPUT VALUES: locationId: {0}", pLocationId);
		}
		try {
			final RepositoryItem item = (RepositoryItem) getLocationRepository().getItem(pLocationId,
					getPropertyManager().getStoreItemDescriptorName());
			if (item != null) {
				pStoreVo.setStoreId((String) item.getPropertyValue(getPropertyManager().getLocationIdPropertyName()));
				pStoreVo.setStoreName((String) item.getPropertyValue(getPropertyManager().getStoreName()));
				pStoreVo.setCity((String) item.getPropertyValue(getPropertyManager().getCityName()));
				pStoreVo.setAddress1((String) item.getPropertyValue(getPropertyManager().getStoreAddress1()));
				pStoreVo.setStateAddress((String) item.getPropertyValue(getPropertyManager().getStoreStateAddress()));
				pStoreVo.setPostalCode((String) item.getPropertyValue(getPropertyManager().getStorePostalCode()));
				pStoreVo.setPhoneNumber((String) item.getPropertyValue(getPropertyManager().getStorePhoneNumber()));
			}
		} catch (RepositoryException repositoryException) {
			vlogError("RepositoryException in TRUShoppingCartUtils.populateStoreInfo::", repositoryException);
		}
		if (isLoggingDebug()) {
			logDebug("END:TRUShoppingCartUtils.populateStoreInfo method");
		}
		return pStoreVo;

	}

	/**
	 * Sets the location repository.
	 * 
	 * @param pLocationRepository
	 *            the mLocationRepository to set
	 */
	public void setLocationRepository(Repository pLocationRepository) {
		mLocationRepository = pLocationRepository;
	}

	/**
	 * returns date in fomat DD/MM/YY *.
	 * 
	 * @param pDays
	 *            the days
	 * @return the relative date
	 */
	private Date getRelativeDate(int pDays) {
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DATE, pDays);
		//DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
		return now.getTime();
		//return df.format(now.getTime());
	}

	/**
	 * Gets the configuration.
	 * 
	 * @return the configuration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * Sets the configuration.
	 * 
	 * @param pConfiguration
	 *            the new configuration
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		this.mConfiguration = pConfiguration;
	}

	/**
	 * Gets the TRU cart modifier helper.
	 * 
	 * @return the TRU cart modifier helper
	 */
	public TRUCartModifierHelper getTRUCartModifierHelper() {
		return mTRUCartModifierHelper;
	}

	/**
	 * Sets the TRU cart modifier helper.
	 * 
	 * @param pTRUCartModifierHelper
	 *            the new TRU cart modifier helper
	 */
	public void setTRUCartModifierHelper(TRUCartModifierHelper pTRUCartModifierHelper) {
		this.mTRUCartModifierHelper = pTRUCartModifierHelper;
	}
	
	/**
	 * Gets the site util.
	 *
	 * @return the mSiteUtil
	 */
	public TRUGetSiteTypeUtil getSiteUtil() {
		return mSiteUtil;
	}

	/**
	 * Sets the site util.
	 *
	 * @param pSiteUtil the mSiteUtil to set
	 */
	public void setSiteUtil(TRUGetSiteTypeUtil pSiteUtil) {
		this.mSiteUtil = pSiteUtil;
	}
	
	/**
	 * Gets the hook logic details.
	 *
	 * @param pOrder TRUOrderImpl
	 * @return hookLogicDetails
	 * Map
	 */
	public Map<String, String> getHookLogicDetails(TRUOrderImpl pOrder){
		final Map<String, String> hookLogicDetails = new HashMap<String, String>();
		StringBuffer skuId = new StringBuffer();
		StringBuffer parentSkuId = new StringBuffer();
		StringBuffer quantity = new StringBuffer();
		StringBuffer price = new StringBuffer();
		StringBuffer regularPrice = new StringBuffer();
		final List commerceItems = pOrder.getCommerceItems();
		if(commerceItems.size()<TRUCommerceConstants.INT_ONE){
			return hookLogicDetails;
		}
		for (Object cItem : commerceItems) {
			CommerceItem commerceItem = (CommerceItem) cItem;
			skuId.append(commerceItem.getCatalogRefId()).append(TRUCommerceConstants.PIPELINE);
			parentSkuId.append(commerceItem.getAuxiliaryData().getProductId()).append(TRUCommerceConstants.PIPELINE);
			quantity.append(commerceItem.getQuantity()).append(TRUCommerceConstants.PIPELINE);
			price.append(commerceItem.getPriceInfo().getSalePrice()).append(TRUCommerceConstants.PIPELINE);
			regularPrice.append(commerceItem.getPriceInfo().getListPrice()).append(TRUCommerceConstants.PIPELINE);		
			
		}
		skuId=skuId.deleteCharAt(skuId.length()-TRUCommerceConstants.INT_ONE);
		parentSkuId=parentSkuId.deleteCharAt(parentSkuId.length()-TRUCommerceConstants.INT_ONE);
		quantity=quantity.deleteCharAt(quantity.length()-TRUCommerceConstants.INT_ONE);
		price=price.deleteCharAt(price.length()-TRUCommerceConstants.INT_ONE);
		regularPrice=regularPrice.deleteCharAt(regularPrice.length()-TRUCommerceConstants.INT_ONE);
		final double orderTotal = pOrder.getPriceInfo().getRawSubtotal();
		final String orderSubTotal = orderTotal + TRUCommerceConstants.EMPTY_STRING;
		hookLogicDetails.put(TRUCommerceConstants.SKU_STRING, skuId.toString());
		hookLogicDetails.put(TRUCommerceConstants.PARENT_SKU_STRING, parentSkuId.toString());
		hookLogicDetails.put(TRUCommerceConstants.QUANTITY_STRING, quantity.toString());
		hookLogicDetails.put(TRUCommerceConstants.PRICE, price.toString());
		hookLogicDetails.put(TRUCommerceConstants.REGULAR_PRICE, regularPrice.toString());
		hookLogicDetails.put(TRUCommerceConstants.ORDER_SUB_TOTAL, orderSubTotal);
		hookLogicDetails.put(TRUCommerceConstants.ORDER_ID, pOrder.getId());
		
		return hookLogicDetails;
	}
	/**
	 * This method will return the PDP page URL based on the productId and siteId.
	 * @param pProductId productId
	 * @param pSiteId siteId
	 * @return string
	 */
	protected String getPDPURL(String pProductId, Site pSiteId) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::getPDPURL() : START");
		}
		String productPageUrl = null;
		if (!StringUtils.isEmpty(pProductId) && pSiteId != null) {
			if (StringUtils.isNotEmpty(pSiteId.toString())) {
				productPageUrl = getProductPageUtil().getProductPageURL(pProductId, pSiteId.toString());
				return productPageUrl;
			} else {
				productPageUrl = getProductPageUtil().getProductPageURL(pProductId);
				return productPageUrl;
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::getPDPURL() : END");
		}
		return productPageUrl;
	}
	
	/**
	 * Gets the gift wrap comm item id.
	 *
	 * @param pMetaInfo the meta info
	 * @return the gift wrap comm item id
	 */
	public String getGiftWrapCommItemId (TRUCommerceItemMetaInfo pMetaInfo) {
		String giftWrapCommId = null;
		RepositoryItem item = (RepositoryItem) pMetaInfo.getPropertyValue(getPropertyManager().getGiftWrapItemPropertyName());
		if(null != item) {
			giftWrapCommId = item.getRepositoryId();
		}
		return giftWrapCommId;
	}
	/**
	 * Get the current server time.
	 * @return the TimeStamp
	 */
	public String getCurrentServerTime() {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::getCurrentServerTime() : START");
		}
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();
		final String dateFormat = TRUCommerceConstants.SERVER_DATE_FORMAT;
		DateFormat formatter = new SimpleDateFormat(dateFormat, Locale.US);
		formatter.setTimeZone(TimeZone.getTimeZone(TRUCheckoutConstants.TIME_ZONE));
		String parsedTimeStamp = formatter.format(date);
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::getCurrentServerTime() : END");
		}
		if (StringUtils.isNotBlank(parsedTimeStamp)) {
			return parsedTimeStamp;
		}
		return null;
	}
	/**
	 * Gets the product page util.
	 *
	 * @return the mProductPageUtil
	 */
	public TRUProductPageUtil getProductPageUtil() {
		return mProductPageUtil;
	}

	/**
	 * Sets the product page util.
	 *
	 * @param pProductPageUtil the mProductPageUtil to set
	 */
	public void setProductPageUtil(TRUProductPageUtil pProductPageUtil) {
		this.mProductPageUtil = pProductPageUtil;
	}

	/**
	 * Calculate excluded skus amount.
	 *
	 * @param pExcludedSKUsforPromo the excluded sk usfor promo
	 * @param pOrder the order
	 * @param pQualifySKUsforPromo the qualify sk usfor promo
	 * @param pQualifyBulkSkuids the qualify bulk skuids
	 * @param pExcludedBulkSkuids - the excluded bulk skuids
	 * @return the double
	 */
	@SuppressWarnings("unchecked")
	public double calculateExcludedSkusAmount(List<RepositoryItem> pExcludedSKUsforPromo, Order pOrder,List<RepositoryItem> pQualifySKUsforPromo
			, List<String> pQualifyBulkSkuids, List<String> pExcludedBulkSkuids) {
		Double excludedItemsAmount = TRUConstants.DOUBLE_ZERO;
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::calculateExcludedSkusAmount() : START");
			vlogDebug("INPUT VALUES for calculateExcludedSkusAmount :: pExcludedSkuItems: {0},pOrder : {1}", pExcludedSKUsforPromo, pOrder);
		}
		List<CommerceItem> citems = pOrder.getCommerceItems();
		for (CommerceItem item : citems) {
			if(item instanceof TRUDonationCommerceItem){
				excludedItemsAmount = Double.valueOf(excludedItemsAmount.doubleValue() + item.getPriceInfo().getAmount());
				continue;
			}
			if (pExcludedSKUsforPromo != null && pExcludedSKUsforPromo.contains(item.getAuxiliaryData().getCatalogRef())) {
				excludedItemsAmount = Double.valueOf(excludedItemsAmount.doubleValue() + item.getPriceInfo().getAmount());
				continue;
			}
			if (pExcludedBulkSkuids != null && pExcludedBulkSkuids.contains(item.getCatalogRefId())) {
				excludedItemsAmount = Double.valueOf(excludedItemsAmount.doubleValue() + item.getPriceInfo().getAmount());
				continue;
			}
			if ((pQualifySKUsforPromo == null || pQualifySKUsforPromo.isEmpty()) &&
			(pQualifyBulkSkuids == null || pQualifyBulkSkuids.isEmpty())) {
				continue;
			}
			if ((pQualifySKUsforPromo == null || !pQualifySKUsforPromo.contains(item.getAuxiliaryData().getCatalogRef()))
			&& (pQualifyBulkSkuids == null || !pQualifyBulkSkuids.contains(item.getCatalogRefId()))) {
				excludedItemsAmount = Double.valueOf(excludedItemsAmount.doubleValue() + item.getPriceInfo().getAmount());
			}
		}
		// Calling method to get the BPP items.
		excludedItemsAmount += getPricingTools().getBPPItemsAmount(pOrder);
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShoppingCartUtils::@method::calculateExcludedSkusAmount() : END");
		}
		return excludedItemsAmount;
	}
	
	
	/**
	 * Checks if is iSPU item in oos.
	 *
	 * @param pSkuItem the sku item
	 * @return true, if is iSPU item in oos
	 */
	private boolean isISPUItemInOOS(final RepositoryItem pSkuItem){
		if(isLoggingDebug()){
			logDebug("isISPUItemInOOS method START");
		}
		boolean isOOSItem=Boolean.FALSE;
		if (pSkuItem != null) {
			Boolean s2s = Boolean.FALSE;
			Boolean v_InStorePickup = null;
			Boolean s2sValue = (Boolean) pSkuItem.getPropertyValue(getPropertyManager()
					.getShipToStoreEligible());
			if (s2sValue != null && s2sValue.booleanValue()) {
				s2s = Boolean.TRUE;
			} 
			String inStorePickup = (String) pSkuItem.getPropertyValue(getPropertyManager().getItemInStorePickUp());
			if (TRUCommerceConstants.YES.equalsIgnoreCase(inStorePickup)
					|| TRUCommerceConstants.YES_STRING.equalsIgnoreCase(inStorePickup)
					|| TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(inStorePickup)) {
				v_InStorePickup = Boolean.TRUE;
			} else {
				v_InStorePickup = Boolean.FALSE;
			}
			if (!s2s &&!v_InStorePickup) {
				isOOSItem=Boolean.TRUE;
			}
		}
		if(isLoggingDebug()){
			vlogDebug("Is pSku{0} eligible for ISPU ?  isOOSItem {1}", pSkuItem.getRepositoryId(),isOOSItem );
		}
		return isOOSItem;
	}
	
	/** Property To mPricingTools . */
	private TRUPricingTools mPricingTools;
	/**
	 * @return the pricingTools
	 */
	public TRUPricingTools getPricingTools() {
		return mPricingTools;
	}

	/**
	 * @param pPricingTools the pricingTools to set
	 */
	public void setPricingTools(TRUPricingTools pPricingTools) {
		mPricingTools = pPricingTools;
	}
	
}
