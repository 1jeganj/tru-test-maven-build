package com.tru.commerce.cart.helper;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.commerce.CommerceException;
import atg.commerce.inventory.InventoryException;
import atg.commerce.locations.CoordinateManager;
import atg.commerce.locations.GeoLocatorService;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemManager;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.Relationship;
import atg.commerce.order.RelationshipNotFoundException;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.promotion.GWPMarkerManager;
import atg.commerce.promotion.TRUPromotionTools;
import atg.commerce.states.OrderStates;
import atg.core.util.StringUtils;
import atg.markers.MarkerException;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.cart.vo.TRUBPPItemDetailVO;
import com.tru.commerce.cart.vo.TRUCartItemDetailVO;
import com.tru.commerce.cart.vo.TRUStoreVO;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.inventory.TRUCoherenceInventoryManager;
import com.tru.commerce.inventory.TRUInventoryInfo;
import com.tru.commerce.locations.TRUStoreLocatorTools;
import com.tru.commerce.order.TRUBppItemInfo;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUChannelInStorePickupShippingGroup;
import com.tru.commerce.order.TRUCommerceItemImpl;
import com.tru.commerce.order.TRUCommerceItemMetaInfo;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUOutOfStockItem;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.pricing.TRUItemPriceInfo;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.cml.SystemException;
import com.tru.common.vo.TRUStoreItemVO;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;

/**
 * The Class TRUCartModifierHelper.
 */
/**
 * @author PA
 * @version 1.0
 */
public class TRUCartModifierHelper extends GenericService{
	
	/** Property To Hold OrderStates. */
	private OrderStates mOrderStates;
	
	/** Property To Hold commerce item manager. */
	private CommerceItemManager mCommerceItemManager;

	/** Property To property manager. */
	private TRUCommercePropertyManager mPropertyManager;
	
	/** Property to Hold Error handler manager. */
	private DefaultErrorHandlerManager mErrorHandlerManager;
	
	/** Property To mCoherenceInventoryManager . */
	private TRUCoherenceInventoryManager mCoherenceInventoryManager;
	
	/**  Property To Hold location repository. */
	private Repository mLocationRepository;
	
	/** Holds reference for geo locator service. */
	private GeoLocatorService mGeoLocatorService;

	/** Holds reference for coordinate manager. */
	private CoordinateManager mCoordinateManager;
	
	/** Property to hold is TRUConfiguration. */
	private TRUConfiguration mConfiguration;
	
	/** Holds the mLocationTools. */
	private TRUStoreLocatorTools mLocationTools;
	
	/** property to Hold shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;
	
	
	
	   
	/**
	 * Gets the shipping group commerce item relationships.
	 * 
	 * @param pOrder
	 *            the order.
	 * @return the shipping group commerce item relationships.
	 * @throws CommerceException
	 *             the commerce exception.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List getShippingGroupCommerceItemRelationships(Order pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::getShippingGroupCommerceItemRelationships() : START : Order --> "	+ pOrder);
		}
		List<TRUShippingGroupCommerceItemRelationship> rels=new ArrayList<TRUShippingGroupCommerceItemRelationship>();
		List<CommerceItem> cItems=pOrder.getCommerceItems();
		List<CommerceItem> tmpList = new ArrayList<CommerceItem>(cItems); 
	    Collections.reverse(tmpList);
		for(CommerceItem cItem:tmpList){
			rels.addAll(cItem.getShippingGroupRelationships());
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCartModifierHelper::@method::getShippingGroupCommerceItemRelationships() : END");
		}
		return rels;
	}
	
	
	/**
	 * Creates the individual item for out of stock.
	 * 
	 * @param pOrder
	 *            the order.
	 * @return the list.
	 */
	@SuppressWarnings("unchecked")
	public List<TRUCartItemDetailVO> createIndividualItemForOutOfStock(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::createIndividualItemForOutOfStock() : START: Order --> {0}" + pOrder);
		}
		RepositoryItem skuItem = null;
		String infoMessage = null;
		double salePrice = TRUCommerceConstants.DOUBLE_ZERO;
		double listPrice = TRUCommerceConstants.DOUBLE_ZERO;
		double savedAmount = TRUCommerceConstants.DOUBLE_ZERO;
		double savedPercentage = TRUCommerceConstants.DOUBLE_ZERO;
		List<TRUCartItemDetailVO> outOfStockVOs = new ArrayList<TRUCartItemDetailVO>();
		List<TRUOutOfStockItem> outOfStockItems = ((TRUOrderImpl) pOrder).getOutOfStockItems();
		TRUPricingTools prcingTools = (TRUPricingTools) getCommerceItemManager().getOrderTools().getProfileTools()
				.getPricingTools();
		for (TRUOutOfStockItem item : outOfStockItems) {
			StringBuilder registryName = new StringBuilder();
			try {
				TRUCartItemDetailVO itemDetailVO = new TRUCartItemDetailVO();
				String skuId = (String) item.getPropertyValue(getPropertyManager().getCatalogRefIdPropertyName());
				skuItem = findSkuItem(skuId);
				String channelType = null;
				String prodId = (String) item.getPropertyValue(getPropertyManager().getProductIdPropertyName());
				if(item.getPropertyValue(getPropertyManager().getSalePricePropertyName()) != null) {
					salePrice = (double) item.getPropertyValue(getPropertyManager().getSalePricePropertyName());	
				}
				if(item.getPropertyValue(getPropertyManager().getListPricePropertyName()) != null) {
					listPrice = (double) item.getPropertyValue(getPropertyManager().getListPricePropertyName());	
				}
				if(item.getPropertyValue(getPropertyManager().getTotalSavedAmount())!=null) {
					savedAmount = (double) item.getPropertyValue(getPropertyManager().getTotalSavedAmount());	
				}
				if(item.getPropertyValue(getPropertyManager().getTotalSavedPercentage())!=null) {
					savedPercentage = (double) item.getPropertyValue(getPropertyManager().getTotalSavedPercentage());	
				}
				if (null != item.getPropertyValue(getPropertyManager().getChannelUserName())) {
					registryName.append((String) item.getPropertyValue(getPropertyManager().getChannelUserName())).append(
							TRUCommerceConstants.SPACE);
				}
				if (null != item.getPropertyValue(getPropertyManager().getChannelCoUserName())) {
					registryName.append(TRUCommerceConstants.AND).append(TRUCommerceConstants.SPACE)
							.append((String) item.getPropertyValue(getPropertyManager().getChannelCoUserName()));
				}
				if (null != item.getPropertyValue(getPropertyManager().getChannelType())) {
					channelType = (String) item.getPropertyValue(getPropertyManager().getChannelType());
					if (!StringUtils.isBlank(channelType) && getPropertyManager().getWishlist().equals(channelType)) {
						registryName.append(TRUCommerceConstants.SPACE).append(getPropertyManager().getWishlist());
					} else if (!StringUtils.isBlank(channelType) && getPropertyManager().getRegistry().equals(channelType)) {
						registryName.append(TRUCommerceConstants.SPACE).append(getPropertyManager().getRegistry());
					}
				}
				if (null != registryName && registryName.length() > TRUConstants.ZERO) {
					itemDetailVO.setChannel(registryName.toString());
				}
				itemDetailVO.setSkuItem(skuItem);
				itemDetailVO.setId(item.getId());
				itemDetailVO.setSkuId(skuId);
				itemDetailVO.setProductId(prodId);
				itemDetailVO.setQuantity(TRUConstants.ZERO);
				itemDetailVO.setPrice(prcingTools.round(listPrice));
				itemDetailVO.setSalePrice(prcingTools.round(salePrice));
				itemDetailVO.setTotalAmount(TRUConstants.DOUBLE_ZERO);
				if(skuItem.getPropertyValue(getPropertyManager().getPrimaryImage()) != null){
					itemDetailVO.setProductImage((String)skuItem.getPropertyValue(getPropertyManager().getPrimaryImage()));
				}
				if (skuItem.getPropertyValue(getPropertyManager().getNotiFyMe()) != null) {
					itemDetailVO.setNotifyMe((String) skuItem.getPropertyValue(getPropertyManager().getNotiFyMe()));
				}
				if (skuItem != null && null != skuItem.getPropertyValue(getPropertyManager().getLongDescription())) {
					itemDetailVO.setSKUDescription((String) skuItem.getPropertyValue(getPropertyManager().getLongDescription()));
				}
				if (savedAmount > TRUConstants.DOUBLE_ZERO) {
					itemDetailVO.setDisplayStrikeThrough(true);
					itemDetailVO.setSavedAmount(prcingTools.round(savedAmount));
					itemDetailVO.setSavedPercentage(prcingTools.round(savedPercentage));
				} else {
					itemDetailVO.setDisplayStrikeThrough(false);
				}
				itemDetailVO.setDisplayName((String) skuItem.getPropertyValue(getPropertyManager().getDisplayName()));
				itemDetailVO.setInventoryStatus(TRUCommerceConstants.OUT_OF_STOCK);
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.TRU_SHOPPINGCART_NO_INVENTORY_INFO_MESSAGE_KEY);
				infoMessage = MessageFormat.format(infoMessage, itemDetailVO.getDisplayName(),itemDetailVO.getDisplayName());
				RepositoryItem productItem = getCommerceItemManager().getOrderTools().getCatalogTools().findProduct(prodId);
				itemDetailVO.setProductItem(productItem);
				List<String> styleDimensions = (List<String>) (productItem.getPropertyValue(getPropertyManager()
						.getStyleDimensionsPropertyName()));
				if (styleDimensions != null && !styleDimensions.isEmpty()) {
					String colorCode = null;
					String size = null;
					if (null != skuItem.getPropertyValue(getPropertyManager().getColorUpcLevel())) {
						RepositoryItem clorItem = (RepositoryItem) skuItem.getPropertyValue(getPropertyManager()
								.getColorUpcLevel());
						colorCode = String.valueOf(clorItem.getPropertyValue(getPropertyManager()
								.getColorNamePropertyName())).toLowerCase();
					}
					if (null != skuItem.getPropertyValue(getPropertyManager().getJuvenileProductSize())) {
						RepositoryItem sizeItem = (RepositoryItem) skuItem.getPropertyValue(getPropertyManager()
								.getJuvenileProductSize());
						size = String.valueOf(sizeItem.getPropertyValue(getPropertyManager()
								.getSizeDescriptionPropertyName())).toUpperCase();
					}
					boolean colorFlag = Boolean.FALSE;
					boolean sizeFlag = Boolean.FALSE;
					for (String styleDimension : styleDimensions) {
						if (styleDimension.equalsIgnoreCase(TRUCommerceConstants.COLOR)) {
							colorFlag = Boolean.TRUE;
						}
						if (styleDimension.equalsIgnoreCase(TRUCommerceConstants.SIZE)) {
							sizeFlag = Boolean.TRUE;
						}
					}
					if (colorFlag && sizeFlag) {
						itemDetailVO.setColor(colorCode);
						itemDetailVO.setSize(size);
					} else if (colorFlag && !sizeFlag) {
						itemDetailVO.setColor(colorCode);
					} else if (!colorFlag && sizeFlag) {
						itemDetailVO.setSize(size);
					}
				}
				itemDetailVO.setInventoryInfoMessage(infoMessage);
				outOfStockVOs.add(itemDetailVO);
			} catch (RepositoryException e) {
				vlogError(" RepositoryException in @Class::TRUCartModifierHelper::@method::createIndividualItemForOutOfStock() :",
						e);
			} catch (SystemException e) {
				vlogError(" SystemException in @Class::TRUCartModifierHelper::@method::createIndividualItemForOutOfStock() :", e);
			}

		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::createIndividualItemForOutOfStock() : END");
		}
		return outOfStockVOs;
	}
	
	/**
	 * Find sku item.
	 * 
	 * @param pSkuId
	 *            the sku id.
	 * @return the repository item.
	 * @throws RepositoryException
	 *             the repository exception.
	 */
	public RepositoryItem findSkuItem(String pSkuId) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::findSkuItem() : START : SKU ID -->" + pSkuId);
		}
		RepositoryItem skuItem = null;
		if (!StringUtils.isBlank(pSkuId)) {
			skuItem = getCommerceItemManager().getOrderTools().getCatalogTools().findSKU(pSkuId);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::findSkuItem() : END : SKU Item -->" + skuItem);
		}
		return skuItem;

	}
	
	/**
	 * Gets the removed item messages for whose sku or product is soft deleted from the catalog.
	 * 
	 * @param pDeletedSkuList
	 *            the deleted sku list.
	 * @return the removed item messages.
	 */
	public List<String> getRemovedItemMessages(Set<String> pDeletedSkuList) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCartModifierHelper::@method::getRemovedItemMessages() : START : pDeletedSkuList -->",
					pDeletedSkuList);
		}
		List<String> removeItemList = new ArrayList<String>();
		Iterator<String> skuList = pDeletedSkuList.iterator();
		try {
			String infoMessage = getErrorHandlerManager().getErrorMessage(
					TRUCommerceConstants.TRU_SHOPPINGCART_REMOVED_IETM_FROM_BCC);
			while (skuList.hasNext()) {
				String errorMessage = null;
				RepositoryItem skuItem = findSkuItem(skuList.next());
				String name = (String) skuItem.getPropertyValue(getPropertyManager().getDisplayName());
				errorMessage = MessageFormat.format(infoMessage, name, name);
				removeItemList.add(errorMessage);
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError(" RepositoryException in @Class::TRUCartModifierHelper::@method::getRemovedItemMessages() :", e);
			}
		} catch (SystemException e) {
			if (isLoggingError()) {
				logError(" SystemException in @Class::TRUCartModifierHelper::@method::getRemovedItemMessages() :", e);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCartModifierHelper::@method::getRemovedItemMessages() : END : removeItemList -->",
					removeItemList);
		}
		return removeItemList;
	}
	
	/**
	 * This method is used to get the list of location ids.
	 * 
	 * @param pStoreItems
	 *            - store item.
	 * @return locationIds - location ids.
	 */
	public List<String> getLocationIds(RepositoryItem[] pStoreItems) {
		if (isLoggingDebug()) {
			logDebug("Start : TRUStoreLocatorTools.getLocationIds ()");
		}
		List<String> locationIds = null;
		if (pStoreItems != null) {
			locationIds = new ArrayList<String>();
			for (RepositoryItem storeItem : pStoreItems) {
				if (storeItem != null) {
					String locationId = (String) storeItem.getPropertyValue(getPropertyManager().getLocationIdPropertyName());
					if (!StringUtils.isEmpty(locationId)) {
						locationIds.add(locationId);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End : TRUStoreLocatorTools.getLocationIds ()");
		}
		return locationIds;
	}
	
	/**
	 * Sets the street date to item vo.
	 * 
	 * @param pItemDetailVO
	 *            the item detail vo.
	 * @param pSkuItem
	 *            the sku item.
	 */
	private void setStreetDateToItemVO(TRUCartItemDetailVO pItemDetailVO, RepositoryItem pSkuItem) {
		Date streetDate = (Date) pSkuItem.getPropertyValue(getPropertyManager().getStreetDate());
		SimpleDateFormat sdf = new SimpleDateFormat(TRUCommerceConstants.DATE_FORMAT, getCommerceItemManager().getOrderTools()
				.getProfileTools().getLocaleService().getLocale());
		Date currentDate = new Date();
		String formattedDate = null;
		if (streetDate != null && currentDate.before(streetDate)) {
			formattedDate = sdf.format(streetDate);
			pItemDetailVO.setStreetDate(formattedDate);
			pItemDetailVO.setInventoryStatus(TRUCommerceConstants.PRE_ORDER);
		}
	}
	
	
	/**
	 * Sets the bpp item details.
	 *
	 * @param pItemDetailVO the item detail vo.
	 * @param pSkuItem the sku item.
	 * @param pSgCiR the sg ci r.
	 * @param pItemPriceInfo the item price info
	 */
	public void setBPPItemDetails(TRUCartItemDetailVO pItemDetailVO, RepositoryItem pSkuItem,
			TRUShippingGroupCommerceItemRelationship pSgCiR, TRUItemPriceInfo pItemPriceInfo) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::setBPPItemDetails() : START");
			vlogDebug("TRUCartItemDetailVO : {0},pSkuItem : {1},TRUShippingGroupCommerceItemRelationship : {2}", pItemDetailVO,
					pSkuItem, pSgCiR);
		}
		Object bppEligible = pSkuItem.getPropertyValue(getPropertyManager().getBppEligiblePropertyName());
		if (bppEligible != null && (boolean) bppEligible) {
			RepositoryItem[] bppItems = null;
			List<TRUBPPItemDetailVO> bppItemsList = null;
			TRUBPPItemDetailVO bppItemDetailVO = null;
			Object bppName = pSkuItem.getPropertyValue(getPropertyManager().getBppNamePropertyName());
			/*RepositoryItem bppSKUItem = ((TRUCatalogTools) getCommerceItemManager().getOrderTools().getCatalogTools())
					.getDonationOrBPPSKUItem(TRUCommerceConstants.BPP_SKU_TYPE);*/
			vlogDebug("bppName : {0} :  bppEligible : {2}", bppName, bppEligible);
			if (null != bppName) {
				if (StringUtils.isNotBlank((String) bppName)) {
					bppItems = ((TRUCatalogTools) getCommerceItemManager().getOrderTools().getCatalogTools())
							.getBppItemsBasedOnBppName((String) bppName, pItemPriceInfo.getSalePrice());
				}
				try {
					if (null != bppItems && bppItems.length > 0) {
						bppItemsList = new ArrayList<TRUBPPItemDetailVO>();
						TRUBppItemInfo bppItemInfo = pSgCiR.getBppItemInfo();
						String selectedBppItemId = null;
						if (null != bppItemInfo && StringUtils.isNotBlank(bppItemInfo.getId())&& StringUtils.isNotBlank(bppItemInfo.getBppItemId())) {
							selectedBppItemId = bppItemInfo.getBppItemId();
							pItemDetailVO.setBppItemInfoId(bppItemInfo.getId());
						}
						for (RepositoryItem bppItem : bppItems) {
							bppItemDetailVO = new TRUBPPItemDetailVO();
							bppItemDetailVO.setBppItemId(bppItem.getRepositoryId());
							Object cartDescription = bppItem.getPropertyValue(getPropertyManager().getBppCartDescriptionPropertyName());
							if (null != cartDescription) {
								bppItemDetailVO.setBppPlan((String) cartDescription);
							}
							Object retail = bppItem.getPropertyValue(getPropertyManager().getBppRetailPropertyName());
							if (null != retail) {
								bppItemDetailVO.setBppRetail((double) retail);
							}
							Object term = bppItem.getPropertyValue(getPropertyManager().getBppTermPropertyName());
							if (null != term) {
								bppItemDetailVO.setBppTerm((String) term);
							}
							if (StringUtils.isNotBlank(selectedBppItemId)) {
								if (selectedBppItemId.equalsIgnoreCase(bppItem.getRepositoryId())) {
									bppItemDetailVO.setPreSelected(Boolean.TRUE);
								} else {
									bppItemDetailVO.setPreSelected(Boolean.FALSE);
								}
							}
							Object bppSKUId = bppItem.getPropertyValue(getPropertyManager().getBppSKUPropertyName());
							if(bppSKUId != null){
								RepositoryItem skuItem = findSkuItem((String)bppSKUId);
								if(skuItem != null){
									String subType = (String) skuItem.getPropertyValue(getPropertyManager().getSubTypePropertyName());
									if(subType != null && TRUCommerceConstants.BPP_SKU_TYPE.equalsIgnoreCase(subType)){
										bppItemDetailVO.setBppSkuId((String) bppSKUId);
										bppItemsList.add(bppItemDetailVO);
									}
								}
							}
						}
					}
					if (null != bppItemsList && !bppItemsList.isEmpty()) {
						String skuId= null;
						if (bppItemsList.size() == TRUCommerceConstants.ONE) {
							pItemDetailVO.setBppItemId(bppItemsList.get(TRUCommerceConstants.NUM_ZERO).getBppItemId());
							skuId = bppItemsList.get(TRUCommerceConstants.NUM_ZERO).getBppSkuId();
						}else if(bppItemsList.size() > TRUCommerceConstants.ONE){
							for (TRUBPPItemDetailVO trubppItemDetailVO : bppItemsList) {
								if(trubppItemDetailVO.isPreSelected()){
									skuId = trubppItemDetailVO.getBppSkuId();
								}
							}
						}
						if(skuId != null){
							RepositoryItem skuItem = findSkuItem(skuId);
							String primaryImage = (String) skuItem.getPropertyValue(getPropertyManager().getPrimaryImage());
							if(StringUtils.isNotBlank(primaryImage)){
								pItemDetailVO.setBppSkuImageUrl(primaryImage);
							}
						}
						pItemDetailVO.setBppItemList(bppItemsList);
						pItemDetailVO.setBppItemCount(bppItemsList.size());
					}
				} catch (RepositoryException repositoryException) {
					if (isLoggingError()) {
						logError("Exception in TRUCartModifierHelper.setBPPItemDetails", repositoryException);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::setBPPItemDetails() : END");
		}
	}
	
	/**
	 * Sets the gift wrap details.
	 * 
	 * @param pItemDetailVO
	 *            the item detail vo.
	 * @param pOrder
	 *            the order.
	 * @param pMetaInfo
	 *            the meta info.
	 * @throws CommerceItemNotFoundException
	 *             the commerce item not found exception.
	 * @throws InvalidParameterException
	 *             the invalid parameter exception.
	 */
	private void setGiftWrapDetails(TRUCartItemDetailVO pItemDetailVO, Order pOrder, TRUCommerceItemMetaInfo pMetaInfo)
			throws CommerceItemNotFoundException, InvalidParameterException {
		RepositoryItem gwCommerceItem = (RepositoryItem) pMetaInfo.getPropertyValue(getPropertyManager()
				.getGiftWrapItemPropertyName());
		TRUGiftWrapCommerceItem truGiftWrapCommerceItem = (TRUGiftWrapCommerceItem) pOrder.getCommerceItem(gwCommerceItem
				.getRepositoryId());

		pItemDetailVO.setWrapCommerceItemId(truGiftWrapCommerceItem.getId());
		pItemDetailVO.setWrapProductId(truGiftWrapCommerceItem.getAuxiliaryData().getProductId());
		pItemDetailVO.setWrapSkuId(truGiftWrapCommerceItem.getCatalogRefId());
		pItemDetailVO.setWrapQuantity((int)truGiftWrapCommerceItem.getQuantity());
		RepositoryItem wrapSkuItem = (RepositoryItem) truGiftWrapCommerceItem.getAuxiliaryData().getCatalogRef();
		if (wrapSkuItem != null) {
			pItemDetailVO.setWrapDisplayName((String) wrapSkuItem.getPropertyValue(getPropertyManager().getDisplayName()));
		}
	}
	
	
	/**
	 * Sets the channel availability details.
	 * @param pOrder Order
	 * 
	 * @param pSgCiR TRUShippingGroupCommerceItemRelationship
	 * 
	 * @param pItemDetailVO
	 *            the item detail vo.
	 * @param pSkuItem
	 *            the sku item.
	 * @param pItem
	 *            the item.
	 */
	private void setChannelAvailabilityDetails(Order pOrder,TRUShippingGroupCommerceItemRelationship pSgCiR ,TRUCartItemDetailVO pItemDetailVO, RepositoryItem pSkuItem,
			TRUCommerceItemImpl pItem) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::setChannelAvailabilityDetails() : START");
			vlogDebug("@Class::TRUCartModifierHelper::@method::setChannelAvailabilityDetails() : pItemDetailVO-->{0} pSkuItem-->{1}  pItem-->{2}",pItemDetailVO,pSkuItem,pItem);
		}
		String ispu = null;
		long stockLevel=TRUCommerceConstants.ZERO;
		RepositoryItem productItem = (RepositoryItem) pItem.getAuxiliaryData().getProductRef();
		if(productItem == null) {
			return;
		}
		List<String> styleDimensions = (List<String>) (productItem.getPropertyValue(getPropertyManager()
				.getStyleDimensionsPropertyName()));
		Boolean s2sValue=(Boolean) pSkuItem.getPropertyValue(getPropertyManager().getShipToStoreEligible());
		String skuChannelAvailability = (String) pSkuItem.getPropertyValue(getPropertyManager().getChannelAvailability());
		String s2s=null;
		if(s2sValue!=null && s2sValue.booleanValue())
		{
			s2s=TRUCommerceConstants.YES;
		}else{
			s2s=TRUCommerceConstants.NO;
		}
		ispu=(String) pSkuItem
				.getPropertyValue(getPropertyManager().getItemInStorePickUp());
		if(TRUCommerceConstants.YES.equalsIgnoreCase(ispu) || TRUCommerceConstants.YES_STRING.equalsIgnoreCase(ispu) || TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(ispu)){
			ispu=TRUCommerceConstants.YES;
		}else{
			ispu=TRUCommerceConstants.NO;
		}
		try {
			Date currentDate = new Date();
			if (pSkuItem != null) {
				Date streetDate = (Date) pSkuItem.getPropertyValue(getShoppingCartUtils().getPropertyManager()
						.getStreetDate());
				String backOrderStatus = (String) pSkuItem.getPropertyValue(getShoppingCartUtils().getPropertyManager()
						.getBackOrderStatus());
				if (streetDate != null && currentDate.before(streetDate)) {
					if (backOrderStatus != null && backOrderStatus.equalsIgnoreCase(TRUCommerceConstants.R)) {
						stockLevel = TRUCommerceConstants.TEST_MODE_STOCK_LEVEL;
					} else {
						stockLevel = getCoherenceInventoryManager().queryStockLevel(pItemDetailVO.getSkuId());
					}
				} else {
					stockLevel = getCoherenceInventoryManager().queryStockLevel(pItemDetailVO.getSkuId());
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUCartModifierHelper::@method::setChannelAvailabilityDetails() : stockLevel-->{0} ",stockLevel);
			}
			if(StringUtils.isNotBlank(pItemDetailVO.getLocationId())){
				
				RepositoryItem storeItem = null;
				storeItem = getLocationRepository().getItem(pItemDetailVO.getLocationId(), getPropertyManager().getStoreItemDescriptorName());
				Map<String,TRUStoreItemVO> storeItemMap=getLocationTools().getOnHandQtyMap(Arrays.asList(storeItem), pItemDetailVO.getSkuId());
				String wareHouseMsg=getLocationTools().getStoreHoursMsg(storeItem,storeItemMap.get(storeItem.getRepositoryId()));
				
				if (storeItemMap != null && !storeItemMap.isEmpty()) {
					pItemDetailVO.setStoreInventoryStatus(storeItemMap.get(storeItem.getRepositoryId()).getInventoryItemStatus());
				}
				if(OrderStates.SUBMITTED.equalsIgnoreCase(getOrderStates().getStateString(pOrder.getState()))){
					pItemDetailVO.setWarehouseMessage(pSgCiR.getWarehouseMessage());
				}
				else{
					pItemDetailVO.setWarehouseMessage(wareHouseMsg);
				}
				if(TRUCommerceConstants.NO.equalsIgnoreCase(s2s) && TRUCommerceConstants.NO.equalsIgnoreCase(ispu)){
					pItemDetailVO.setChannelAvailability(getConfiguration().getOnlineOnly());		
				}else if(getConfiguration().getInStoreOnly().equalsIgnoreCase(skuChannelAvailability)){
					pItemDetailVO.setChannelAvailability(getConfiguration().getInStoreOnly());		
				}
				else{
					pItemDetailVO.setChannelAvailability(getConfiguration().getChannelAvailabilityForBoth());		
				}
			}
			else{
				if((s2s.equals(TRUCommerceConstants.YES) ||ispu.equals(TRUCommerceConstants.YES))){
					pItemDetailVO.setChannelAvailability(getConfiguration().getChannelAvailabilityForBoth());
				}
				else{
					pItemDetailVO.setChannelAvailability(getConfiguration().getOnlineOnly());		
				}
				
			}
			if (isLoggingDebug()&& pItemDetailVO.getChannelAvailability()!=null) {
				vlogDebug("@Class::TRUCartModifierHelper::@method::setChannelAvailabilityDetails() : ChannelAvailability-->{0} ",pItemDetailVO.getChannelAvailability());
			}
		}
		catch (InventoryException inventoryException) {
			if (isLoggingError()) {
				vlogError("InventoryException in TRUCartModifierHelper.setChannelAvailabilityDetails for order {0} and details {1}",pOrder.getId(), inventoryException);
			}
		
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				vlogError("RepositoryException in TRUCartModifierHelper.setChannelAvailabilityDetails for order {0} and details {1}",pOrder.getId(), repositoryException);
			}
		}
		if (styleDimensions != null && !styleDimensions.isEmpty()) {
			pItemDetailVO.setInStorePickUpAvailable(Boolean.FALSE);
			String colorCode = null;
			String size = null;
			if (null != pSkuItem.getPropertyValue(getPropertyManager().getColorUpcLevel())) {
				RepositoryItem clorItem = (RepositoryItem) pSkuItem.getPropertyValue(getPropertyManager()
						.getColorUpcLevel());
				colorCode = String.valueOf(clorItem.getPropertyValue(getPropertyManager().getColorNamePropertyName())).toLowerCase();
			}
			if (null != pSkuItem.getPropertyValue(getPropertyManager().getJuvenileProductSize())) {
				RepositoryItem sizeItem = (RepositoryItem) pSkuItem.getPropertyValue(getPropertyManager()
						.getJuvenileProductSize());
				size = String.valueOf(sizeItem.getPropertyValue(getPropertyManager().getSizeDescriptionPropertyName())).toUpperCase();
			}
			boolean colorFlag = Boolean.FALSE;
			boolean sizeFlag = Boolean.FALSE;
			for (String styleDimension : styleDimensions) {
				if (styleDimension.equalsIgnoreCase(TRUCommerceConstants.COLOR)) {
					colorFlag = Boolean.TRUE;
				}
				if (styleDimension.equalsIgnoreCase(TRUCommerceConstants.SIZE)) {
					sizeFlag = Boolean.TRUE;
				}
			}
			if (colorFlag && sizeFlag) {
				pItemDetailVO.setColor(colorCode);
				pItemDetailVO.setSize(size);
			} else if (colorFlag && !sizeFlag) {
				pItemDetailVO.setColor(colorCode);
			} else if (!colorFlag && sizeFlag) {
				pItemDetailVO.setSize(size);
			}
		} else if (TRUCommerceConstants.NO.equals(s2s) && TRUCommerceConstants.NO.equals(ispu)) {
			pItemDetailVO.setInStorePickUpAvailable(Boolean.FALSE);
		} else {
			pItemDetailVO.setInStorePickUpAvailable(Boolean.TRUE);
		}
		//for fixing TUMOB-10292 adding back below 2 properties 
		pItemDetailVO.setItemInStorePickUp(ispu);
		pItemDetailVO.setShipFromStoreEligible(s2s);
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::setChannelAvailabilityDetails() : END");
		}
	}
	
	/**
	 * Sets the meta info details to cart vo.
	 *
	 * @param pSgCiR the sg ci r.
	 * @param pItemDetailVO the item detail vo.
	 * @param pMetaInfo the meta info.
	 * @param pItem the item.
	 * @param pPrcingTools the prcing tools.
	 * @param pItemPriceInfo the item price info.
	 */
	private void setMetaInfoDetailsToCartVO(TRUShippingGroupCommerceItemRelationship pSgCiR, TRUCartItemDetailVO pItemDetailVO,
			TRUCommerceItemMetaInfo pMetaInfo, TRUCommerceItemImpl pItem, TRUPricingTools pPrcingTools, TRUItemPriceInfo pItemPriceInfo) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::setMetaInfoDetailsToCartVO() : START");
			vlogDebug("@Class::TRUCartModifierHelper::@method::setMetaInfoDetailsToCartVO() : pSgCiR-->{0} pItem-->{1}  pMetaInfo-->{2}  pItemPriceInfo-->{3} ", pSgCiR,pItem,pMetaInfo,pItemPriceInfo);
			vlogDebug("@Class::TRUCartModifierHelper::@method::setMetaInfoDetailsToCartVO() : pMetaInfo-->{0}", pMetaInfo);
		}
		String promoId=null;
		List<PricingAdjustment> pricingAdj=null;
		double totalSaving = TRUConstants.DOUBLE_ZERO;
		double totalSavingPercentage = TRUConstants.DOUBLE_ZERO;
		double amount = TRUConstants.DOUBLE_ZERO;
		boolean isadjusted = Boolean.FALSE;
		long quantity = TRUConstants.ZERO;

		if (pMetaInfo.getPropertyValue(getPropertyManager().getMetaInfoIdPropertyName()) != null) {
			pItemDetailVO
					.setMetaInfoId((String) pMetaInfo.getPropertyValue(getPropertyManager().getMetaInfoIdPropertyName()));
		}
		if (pMetaInfo.getPropertyValue(getPropertyManager().getQuantityPropertyName()) != null) {
			quantity = (long) pMetaInfo.getPropertyValue(getPropertyManager().getQuantityPropertyName());
		}
		if (pMetaInfo.getPropertyValue(getPropertyManager().getPricePropertyName()) != null) {
			amount = (double) pMetaInfo.getPropertyValue(getPropertyManager().getPricePropertyName());
		}
		if (pMetaInfo.getPropertyValue(getPropertyManager().getTotalSavedAmount()) != null) {
			totalSaving = (double) pMetaInfo.getPropertyValue(getPropertyManager().getTotalSavedAmount());
		}
		if (pMetaInfo.getPropertyValue(getPropertyManager().getTotalSavedPercentage()) != null) {
			if(isLoggingDebug()){
				vlogDebug("@Class::TRUCartModifierHelper::@method::setMetaInfoDetailsToCartVO() : meta info-->{0}",
						pMetaInfo.getPropertyValue(getPropertyManager().getTotalSavedPercentage()));
			}
			totalSavingPercentage = (double) pMetaInfo.getPropertyValue(getPropertyManager().getTotalSavedPercentage());
		}
		if (pMetaInfo.getPropertyValue(getPropertyManager().getIsAdjustedPropertyName()) != null) {
			isadjusted = (Boolean) pMetaInfo.getPropertyValue(getPropertyManager().getIsAdjustedPropertyName());
			if (isadjusted && pItemPriceInfo != null && pItemPriceInfo.getAdjustments() != null&& !pItemPriceInfo.getAdjustments().isEmpty()) {
				promoId = (String) pMetaInfo.getPropertyValue(getPropertyManager().getPromotionIdPropertyName());
				if(isLoggingDebug()){
					vlogDebug("@Class::TRUCartModifierHelper::@method::setMetaInfoDetailsToCartVO() : promoId-->{0}", promoId);
				}
				pricingAdj = pItemPriceInfo.getAdjustments();
				for (PricingAdjustment priceAdj : pricingAdj) {
					if ((!StringUtils.isBlank(promoId)) && priceAdj.getPricingModel() != null&& promoId.equalsIgnoreCase(priceAdj.getPricingModel().getRepositoryId())) {
						pItemDetailVO.setPricingAdjustments(priceAdj.getPricingModel());
						break;
					}
				}
			}
		}
		pItemDetailVO.setProductItem((RepositoryItem) pItem.getAuxiliaryData().getProductRef());
		pItemDetailVO.setTotalAmount(pPrcingTools.round(amount));
		pItemDetailVO.setQuantity(quantity);
		pItemDetailVO.setSavedAmount(pPrcingTools.round(totalSaving));
		if(totalSaving>TRUConstants.DOUBLE_ZERO){
			double discountedUnitPrice = TRUConstants.DOUBLE_ZERO;
			discountedUnitPrice=amount/quantity;
			pItemDetailVO.setSalePrice(pPrcingTools.round(discountedUnitPrice));
		}
		else{
			pItemDetailVO.setSalePrice(pItemPriceInfo.getSalePrice());
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::setMetaInfoDetailsToCartVO() : END");
		}
		if(isLoggingDebug()){
			vlogDebug("@Class::TRUCartModifierHelper::@method::setMetaInfoDetailsToCartVO() : totalSavingPercentage-->{0}", totalSavingPercentage);
		}
		pItemDetailVO.setSavedPercentage(pPrcingTools.round(totalSavingPercentage));
		if (pSgCiR.getMetaInfo().size() > TRUConstants.SIZE_ONE) {
			pItemDetailVO.setFromMetaInfo(Boolean.TRUE);
		}
	
			pItemDetailVO.setAdjusted(isadjusted);
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::setMetaInfoDetailsToCartVO() : END");
		}
	}
	
	
	/**
	 * Sets the sku details to item vo.
	 * 
	 * @param pSgCiR
	 *            the sg ci r.
	 * @param pItemDetailVO
	 *            the item detail vo.
	 * @param pSkuItem
	 *            the sku item.
	 */
	private void setSkuDetailsToItemVO(TRUShippingGroupCommerceItemRelationship pSgCiR, TRUCartItemDetailVO pItemDetailVO,
			RepositoryItem pSkuItem) {
		String customerLimit;
		customerLimit = (String) pSkuItem.getPropertyValue(getPropertyManager().getCustomerPurchaseLimit());
		RepositoryItem[] itemMarkers=null;
		GWPMarkerManager manager = getGwpMarkerManager();
		if (customerLimit != null) {
			pItemDetailVO.setCustomerPurchaseLimit((long) Integer.parseInt(customerLimit));
		} else {
			pItemDetailVO.setCustomerPurchaseLimit(TRUCommerceConstants.ZERO);
		}
		long itemQtyInCart = pSgCiR.getCommerceItem().getQuantity();
		pItemDetailVO.setItemQtyInCart(itemQtyInCart);
		pItemDetailVO.setRelItemQtyInCart(pSgCiR.getQuantity());
		pItemDetailVO.setDisplayName((String) pSkuItem.getPropertyValue(getPropertyManager().getDisplayName()));
		String shipWindowMin = (String) pSkuItem.getPropertyValue(getPropertyManager().getShipWindowMin());
		String shipWindowMax = (String) pSkuItem.getPropertyValue(getPropertyManager().getShipWindowMax());
		pItemDetailVO.setShipWindowMin(!StringUtils.isBlank(shipWindowMin) ? shipWindowMin : TRUCommerceConstants.STRING_ZERO);
		pItemDetailVO.setShipWindowMax(!StringUtils.isBlank(shipWindowMax) ? shipWindowMax : TRUCommerceConstants.STRING_ZERO);
		pItemDetailVO.setProductImage((String) pSkuItem.getPropertyValue(getPropertyManager().getPrimaryImage()));
		try {
			itemMarkers = manager.getItemMarkers((TRUCommerceItemImpl) pSgCiR.getCommerceItem(), null, TRUCommerceConstants.STRING_NOT_FOUND);
			if(itemMarkers!=null && (null != pSkuItem.getPropertyValue(getPropertyManager().getUnCartable()) && Boolean.TRUE.equals(pSkuItem
					.getPropertyValue(getPropertyManager().getUnCartable())))){
				pItemDetailVO.setGWPPromotionItem(Boolean.TRUE);
			}else{
				pItemDetailVO.setGWPPromotionItem(Boolean.FALSE);
			}
		} catch (MarkerException markerException) {
			if (isLoggingError()) {
				vlogError("MarkerException in TRUCartModifierHelper.populateItemDetailVO", markerException);
			}
		}
	}
	
	
	/**
	 * Sets the channel details.
	 * 
	 * @param pItemDetailVO
	 *            the item detail vo.
	 * @param pShippingGrp
	 *            the shippingGroup.
	 */
	private void setChannelDetails(TRUCartItemDetailVO pItemDetailVO,ShippingGroup pShippingGrp) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::setChannelDetails() : START");
			vlogDebug("TRUCartItemDetailVO : {0}, pRegistryName : {1}, ShippingGroup : {2}", pItemDetailVO,
					pShippingGrp);
		}
		StringBuffer registryName=new StringBuffer();
		if (pShippingGrp instanceof TRUChannelHardgoodShippingGroup) {
			TRUChannelHardgoodShippingGroup hgSg = (TRUChannelHardgoodShippingGroup) pShippingGrp;
			registryName.append(getCamelCaseString(hgSg.getChannelUserName())).append(TRUCommerceConstants.SPACE);
			if (!StringUtils.isBlank(hgSg.getChannelCoUserName())) {
				registryName.append(TRUCommerceConstants.AND).append(TRUCommerceConstants.SPACE)
						.append(getCamelCaseString(hgSg.getChannelCoUserName())).append(TRUCommerceConstants.TAIL_OF_REGISTRY_NAME);
			}else{
				registryName.append(TRUCommerceConstants.TAIL_OF_REGISTRY_NAME);
			}
			if (!StringUtils.isBlank(hgSg.getChannelType())) {
				registryName.append(TRUCommerceConstants.SPACE).append(hgSg.getChannelType());
				pItemDetailVO.setChannelType(hgSg.getChannelType());
				pItemDetailVO.setChannelId(hgSg.getChannelId());
				pItemDetailVO.setChannelUserName(hgSg.getChannelUserName().toLowerCase());
				if (!StringUtils.isBlank(hgSg.getChannelCoUserName())) {
					pItemDetailVO.setChannelCoUserName(hgSg.getChannelCoUserName().toLowerCase());
				}
			}
			if (!StringUtils.isBlank(registryName.toString())) {
				pItemDetailVO.setChannel(registryName.toString());
			}
		} else if (pShippingGrp instanceof TRUChannelInStorePickupShippingGroup) {
			TRUChannelInStorePickupShippingGroup ispuSg = (TRUChannelInStorePickupShippingGroup) pShippingGrp;
			registryName.append(getCamelCaseString(ispuSg.getChannelUserName())).append(TRUCommerceConstants.SPACE);
			if (!StringUtils.isBlank(ispuSg.getChannelCoUserName())) {
				registryName.append(TRUCommerceConstants.AND).append(TRUCommerceConstants.SPACE)
						.append(getCamelCaseString(ispuSg.getChannelCoUserName())).append(TRUCommerceConstants.TAIL_OF_REGISTRY_NAME);
			}else{
				registryName.append(TRUCommerceConstants.TAIL_OF_REGISTRY_NAME);
			}
			registryName.append(TRUCommerceConstants.TAIL_OF_REGISTRY_NAME);
			if (!StringUtils.isBlank(ispuSg.getChannelType())&& ispuSg.getChannelType().equals(getPropertyManager().getWishlist())) {
				registryName.append(TRUCommerceConstants.SPACE).append(getPropertyManager().getWishlist());
				pItemDetailVO.setChannelType(getPropertyManager().getWishlist());
				pItemDetailVO.setChannelId(ispuSg.getChannelId());
				pItemDetailVO.setChannelUserName(ispuSg.getChannelUserName().toLowerCase());
				if (!StringUtils.isBlank(ispuSg.getChannelCoUserName())) {
				pItemDetailVO.setChannelCoUserName(ispuSg.getChannelCoUserName().toLowerCase());
				}
			} else if (!StringUtils.isBlank(ispuSg.getChannelType())&& ispuSg.getChannelType().equals(getPropertyManager().getRegistry())) {
				registryName.append(TRUCommerceConstants.SPACE).append(getPropertyManager().getRegistry());
				pItemDetailVO.setChannelType(getPropertyManager().getRegistry());
				pItemDetailVO.setChannelId(ispuSg.getChannelId());
				pItemDetailVO.setChannelUserName(ispuSg.getChannelUserName().toLowerCase());
				if (!StringUtils.isBlank(ispuSg.getChannelCoUserName())) {
				pItemDetailVO.setChannelCoUserName(ispuSg.getChannelCoUserName().toLowerCase());
				}
			}
			if (!StringUtils.isBlank(registryName.toString())) {
				pItemDetailVO.setChannel(registryName.toString());
			}
		}
		registryName=null;
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::setChannelDetails() : END");
		}
	}
	
	
	/**
	 * Populate item detail vo.
	 *
	 * @param pSgCiR            the sg ci r.
	 * @param pItemDetailVO            the item detail vo.
	 * @param pOrder            the order.
	 * @param pMetaInfo            the meta info.
	 * @return the TRU cart item detail vo
	 * @throws CommerceItemNotFoundException             - while getting the Invalid Commerce Item.
	 * @throws InvalidParameterException             - while getting the Invalid Commerce Item.
	 */
	public TRUCartItemDetailVO populateItemDetailVO(TRUShippingGroupCommerceItemRelationship pSgCiR,
			TRUCartItemDetailVO pItemDetailVO, Order pOrder, TRUCommerceItemMetaInfo pMetaInfo) throws CommerceItemNotFoundException,
			InvalidParameterException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::populateItemDetailVO() : START");
			vlogDebug("TRUShippingGroupCommerceItemRelationship : {0},pItemDetailVO : {1},pOrder : {2},pMetaInfo : {3}", pSgCiR,
					pItemDetailVO, pOrder, pMetaInfo);
		}
		RepositoryItem skuItem = null;
		String canBeGiftWrapped = null;
		TRUCommerceItemImpl item = (TRUCommerceItemImpl) pSgCiR.getCommerceItem();
		TRUPricingTools prcingTools = (TRUPricingTools) getCommerceItemManager().getOrderTools().getProfileTools()
				.getPricingTools();
		ShippingGroup sg = pSgCiR.getShippingGroup();
		TRUItemPriceInfo itemPriceInfo = (TRUItemPriceInfo) item.getPriceInfo();
		skuItem = (RepositoryItem) item.getAuxiliaryData().getCatalogRef();
		pItemDetailVO.setSkuItem(skuItem);
		pItemDetailVO.setSiteId(item.getAuxiliaryData().getSiteId());
		pItemDetailVO.setId(pSgCiR.getId());
		pItemDetailVO.setSkuId(item.getCatalogRefId());
		pItemDetailVO.setProductId(item.getAuxiliaryData().getProductId());
		if (skuItem.getPropertyValue(getPropertyManager().getNotiFyMe()) != null) {
			pItemDetailVO.setNotifyMe((String) skuItem.getPropertyValue(getPropertyManager().getNotiFyMe()));
		}
		if ((sg instanceof TRUHardgoodShippingGroup || sg instanceof TRUChannelHardgoodShippingGroup) && itemPriceInfo != null) {
			pItemDetailVO.setItemShippingSurcharge(itemPriceInfo.getItemShippingSurcharge());
		}
		if (pMetaInfo != null) {
			setMetaInfoDetailsToCartVO(pSgCiR, pItemDetailVO, pMetaInfo, item, prcingTools, itemPriceInfo);
		}
		setPriceDetails(pItemDetailVO, itemPriceInfo);
		pItemDetailVO.setGiftWrap(pSgCiR.getIsGiftItem());
		pItemDetailVO.setCommerceItemId(item.getId());
		
		if (skuItem != null) {
			setSkuDetailsToItemVO(pSgCiR, pItemDetailVO, skuItem);
		}
		if (pSgCiR.getShippingGroup() instanceof TRUChannelHardgoodShippingGroup
				|| pSgCiR.getShippingGroup() instanceof TRUChannelInStorePickupShippingGroup) {
			setChannelDetails(pItemDetailVO, pSgCiR.getShippingGroup());
		}
		if (sg != null) {
			if (sg instanceof TRUHardgoodShippingGroup || sg instanceof TRUChannelHardgoodShippingGroup) {
				String type = (String) skuItem.getPropertyValue(getPropertyManager().getTypePropertyName());
				if (skuItem != null && !TRUConstants.NON_MERCH_SKU.equalsIgnoreCase(type)) {
					canBeGiftWrapped = (String) skuItem.getPropertyValue(getPropertyManager().getCanBeGiftWrapped());
					if (!StringUtils.isBlank(canBeGiftWrapped) && canBeGiftWrapped.equals(TRUCommerceConstants.YES)) {
						pItemDetailVO.setGiftWrappable(Boolean.TRUE);
					}
				} else {
					pItemDetailVO.setGiftWrappable(Boolean.FALSE);
				}
				pItemDetailVO.setInfoMsgLocationId(getPropertyManager().getLocationIdForShipToHome());
			} else if (sg instanceof TRUInStorePickupShippingGroup) {
				pItemDetailVO.setLocationId(((TRUInStorePickupShippingGroup) sg).getLocationId());
				pItemDetailVO.setInfoMsgLocationId(((TRUInStorePickupShippingGroup) sg).getLocationId());
				pItemDetailVO.setWarehouseLocationId(((TRUInStorePickupShippingGroup) sg).getWarhouseLocationCode());
			} else if (sg instanceof TRUChannelInStorePickupShippingGroup) {
				pItemDetailVO.setLocationId(((TRUChannelInStorePickupShippingGroup) sg).getLocationId());
				pItemDetailVO.setInfoMsgLocationId(((TRUChannelInStorePickupShippingGroup) sg).getLocationId());
				pItemDetailVO.setWarehouseLocationId(((TRUChannelInStorePickupShippingGroup) sg).getWarhouseLocationCode());
			}
		}
		
		setChannelAvailabilityDetails(pOrder,pSgCiR,pItemDetailVO, skuItem, item);
		setStreetDateToItemVO(pItemDetailVO, skuItem);
		pItemDetailVO.setShipGroupId(sg.getId());
		setPromotionItemDetails(itemPriceInfo,pItemDetailVO,pMetaInfo);
		// Update Gift wrap information to VO obj
		if (pMetaInfo != null && pMetaInfo.getPropertyValue(getPropertyManager().getGiftWrapItemPropertyName()) != null) {
			setGiftWrapDetails(pItemDetailVO, pOrder, pMetaInfo);
		}

		// set BPP item details to VO object
		setBPPItemDetails(pItemDetailVO, skuItem, pSgCiR, itemPriceInfo);
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::populateItemDetailVO() : END");
		}
		return pItemDetailVO;
	}

	
	/**
	 * Sets the promotion item details.
	 *
	 * @param pItemPriceInfo the item price info
	 * @param pItemDetailVO the item detail vo
	 * @param pMetaInfo the meta info
	 */	
	private void setPromotionItemDetails(TRUItemPriceInfo pItemPriceInfo,
			TRUCartItemDetailVO pItemDetailVO,TRUCommerceItemMetaInfo pMetaInfo) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::setPromotionItemDetails() : START");
			vlogDebug("ItemPriceInfo : {0},pItemDetailVO : {1},pMetaInfo : {2}", pItemPriceInfo,
					pItemDetailVO,  pMetaInfo);
		}
		RepositoryItem pricingModel = null;
		TRUCartItemDetailVO itemDetailVO = pItemDetailVO;
		TRUPromotionTools promotionTools= (TRUPromotionTools)getCommerceItemManager().getOrderTools().getProfileTools().getPromotionTools();
		String promoId=(String)pMetaInfo.getPropertyValue(getPropertyManager().getPromotionIdPropertyName());
		if (null != promoId) {
			try {
				pricingModel = promotionTools.getItemForId(promotionTools.getBasePromotionItemType(), promoId);
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					logError("repositoryException in TRUCartModifierHelper.setPromotionItemDetails", repositoryException);
				}
			}

		}
		if(pricingModel != null && pMetaInfo.getPropertyValue(getPropertyManager().getIsAdjustedPropertyName()) == Boolean.TRUE){
			
			if((String)pricingModel.getPropertyValue(getPropertyManager().getDescriptionPropertyName())!=null){
				itemDetailVO.setPromotionItemDescription((String)pricingModel.getPropertyValue(getPropertyManager().getDescriptionPropertyName()));
			}else{
				itemDetailVO.setPromotionItemDescription((String)pricingModel.getPropertyValue(getPropertyManager().getDisplayNamePropertyName()));
			}
			if(null!=pricingModel.getPropertyValue(getPropertyManager().getPromotionDetailsPropertyName())){
				itemDetailVO.setPromotionDetails((String)pricingModel.getPropertyValue(getPropertyManager().getPromotionDetailsPropertyName()));
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::setPromotionItemDetails() : END");
		}
	}


	/**
	 * Sets the price details.
	 * 
	 * @param pItemDetailVO
	 *            the item detail vo.
	 * @param pItemPriceInfo
	 *            the item price info.
	 */
	private void setPriceDetails(TRUCartItemDetailVO pItemDetailVO, TRUItemPriceInfo pItemPriceInfo) {
		if (pItemPriceInfo != null) {
			pItemDetailVO.setOnSale(pItemPriceInfo.isOnSale());
			pItemDetailVO.setPrice(pItemPriceInfo.getListPrice());
			//pItemDetailVO.setSalePrice(pItemPriceInfo.getSalePrice());
			pItemDetailVO.setDisplayStrikeThrough(pItemPriceInfo.isDisplayStrikeThroughPrice());
			pItemDetailVO.setDisplayStrikeThroughPrice(pItemPriceInfo.isDisplayStrikeThroughPrice());
		}
	}
	
	
	
	/**
	 * Builds the sku set for inventory check.
	 *
	 * @param pSkuIdAndLocIdMap the sku id and loc id map.
	 * @param pInventoryRequest the inventory request.
	 * @param pInventoryResponse the inventory response.
	 * @return the sets the.
	 * @throws RepositoryException the repository exception.
	 */
	public Set<String> buildSkuSetForInventoryCheck(Map<String, Long> pSkuIdAndLocIdMap, List<TRUInventoryInfo> pInventoryRequest,
			List<TRUInventoryInfo> pInventoryResponse) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Entering method TRUShoppingCartUtils.buildSkuSetForInventoryCheck :: END");
			vlogDebug("@Class::TRUShoppingCartUtils::@method::buildSkuSetForInventoryCheck()  pSkuIdAndLocIdMap -->{0} pInventoryRequest--> {1}pInventoryResponse-->{2}"
					,pSkuIdAndLocIdMap,pInventoryRequest,pInventoryResponse);
		}
		List<TRUInventoryInfo> cartInventoryRequestWeb = new ArrayList<TRUInventoryInfo>();
		List<TRUInventoryInfo> inventoryResponseWeb = null;
		Set<String> skuIdSet = new HashSet<String>();
		Map<String,String> skuIdAndWareHouseMap=new HashMap<String, String>();
		if (pInventoryResponse == null || pInventoryResponse.isEmpty()) {
			for (TRUInventoryInfo invRes : pInventoryRequest) {
				if (invRes.getLocationId() != null) {
					String wareHouseId=Long.toString(getWareHouseLocationId(invRes.getLocationId()).longValue());
					TRUInventoryInfo inveReq = new TRUInventoryInfo(invRes.getCatalogRefId(),
							(long) pSkuIdAndLocIdMap.get(invRes.getCatalogRefId() + TRUCommerceConstants.UNDER_SCORE + invRes.getLocationId()), wareHouseId,invRes.getLocationId());
					skuIdAndWareHouseMap.put(invRes.getCatalogRefId() + TRUCommerceConstants.UNDER_SCORE + wareHouseId+ TRUCommerceConstants.UNDER_SCORE+ invRes.getLocationId() , invRes.getLocationId());
					cartInventoryRequestWeb.add(inveReq);
				} else {
					skuIdSet.add(invRes.getCatalogRefId());
				}
			}
		} else {
			for (TRUInventoryInfo invRes : pInventoryResponse) {
				if (invRes.getLocationId() != null && invRes.getQuantity() <= 0) {
					String wareHouseId=Long.toString(getWareHouseLocationId(invRes.getLocationId()).longValue());
					TRUInventoryInfo inveReq = new TRUInventoryInfo(invRes.getCatalogRefId(),
							(long) pSkuIdAndLocIdMap.get(invRes.getCatalogRefId() + TRUCommerceConstants.UNDER_SCORE+ invRes.getLocationId()), wareHouseId,invRes.getLocationId());
					skuIdAndWareHouseMap.put(invRes.getCatalogRefId() + TRUCommerceConstants.UNDER_SCORE + wareHouseId + TRUCommerceConstants.UNDER_SCORE+ invRes.getLocationId() , invRes.getLocationId());
					cartInventoryRequestWeb.add(inveReq);
				} else if (invRes.getQuantity() <= 0) {
						skuIdSet.add(invRes.getCatalogRefId());
				}
			}
		}
		if (cartInventoryRequestWeb != null && !cartInventoryRequestWeb.isEmpty()) {
			inventoryResponseWeb = getCoherenceInventoryManager().validateBulkStockLevel(cartInventoryRequestWeb);
			if (inventoryResponseWeb == null || inventoryResponseWeb.isEmpty()) {
				for (TRUInventoryInfo invReqWeb : cartInventoryRequestWeb) {
					if (invReqWeb.getQuantity() <= 0) {
						if (invReqWeb.getLocationId() != null) {
							skuIdSet.add(invReqWeb.getCatalogRefId() + TRUCommerceConstants.UNDER_SCORE + skuIdAndWareHouseMap.get(invReqWeb.getCatalogRefId() + TRUCommerceConstants.UNDER_SCORE	+invReqWeb.getLocationId()+TRUCommerceConstants.UNDER_SCORE	+invReqWeb.getOriginalLocationId()));
						} else {
							skuIdSet.add(invReqWeb.getCatalogRefId());
						}
					}
				}
			} else {
				for (TRUInventoryInfo invRes : inventoryResponseWeb) {
					if (invRes.getQuantity() <= 0) {
						if (invRes.getLocationId() != null) {
							skuIdSet.add(invRes.getCatalogRefId() + TRUCommerceConstants.UNDER_SCORE + skuIdAndWareHouseMap.get(invRes.getCatalogRefId() + TRUCommerceConstants.UNDER_SCORE	+invRes.getLocationId()+TRUCommerceConstants.UNDER_SCORE +invRes.getOriginalLocationId()));
						} else {
							skuIdSet.add(invRes.getCatalogRefId());
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShoppingCartUtils::@method::buildSkuSetForInventoryCheck   skuIdSet -->{0}"
					,skuIdSet);
			logDebug("Exiting method TRUShoppingCartUtils.buildSkuSetForInventoryCheck :: END");
		}
		return skuIdSet;
	}
	
	
	
	/**
	 * Builds the inventory info set for inventory check.
	 *
	 * @param pSkuIdAndLocIdMap the sku id and loc id map.
	 * @param pInventoryRequest the inventory request.
	 * @param pInventoryResponse the inventory response.
	 * @return the sets the.
	 * @throws RepositoryException the repository exception.
	 * @throws RelationshipNotFoundException the relationship not found exception.
	 * @throws InvalidParameterException the invalid parameter exception.
	 */
	public Set<TRUInventoryInfo> buildInventoryInfoSetForInventoryCheck(Map<String, Long> pSkuIdAndLocIdMap, List<TRUInventoryInfo> pInventoryRequest,
			List<TRUInventoryInfo> pInventoryResponse) throws RepositoryException, RelationshipNotFoundException, InvalidParameterException {
		if (isLoggingDebug()) {
			logDebug("Entering method TRUShoppingCartUtils.buildSkuSetForInventoryCheck :: END");
			vlogDebug("@Class::TRUShoppingCartUtils::@method::buildSkuSetForInventoryCheck()  pSkuIdAndLocIdMap -->{0} pInventoryRequest--> {1}pInventoryResponse-->{2}"
					,pSkuIdAndLocIdMap,pInventoryRequest,pInventoryResponse);
		}
		List<TRUInventoryInfo> cartInventoryRequestWeb = new ArrayList<TRUInventoryInfo>();
		List<TRUInventoryInfo> inventoryResponseWeb = null;
		Set<TRUInventoryInfo> inventoryInfoSet = new HashSet<TRUInventoryInfo>();
		if (pInventoryResponse == null || pInventoryResponse.isEmpty()) {
			for (TRUInventoryInfo invRes : pInventoryRequest) {
				if (invRes.getLocationId() != null) {
					TRUInventoryInfo inveReq = new TRUInventoryInfo(invRes.getCatalogRefId(),
							(long) pSkuIdAndLocIdMap.get(invRes.getCatalogRefId() + TRUCommerceConstants.UNDER_SCORE+ invRes.getLocationId()), Long.toString(getWareHouseLocationId(
									invRes.getLocationId()).longValue()));
					cartInventoryRequestWeb.add(inveReq);
				} else {
					inventoryInfoSet.add(invRes);
				}
			}
		} else {
			for (TRUInventoryInfo invRes : pInventoryResponse) {
				if (invRes.getLocationId() != null && invRes.getQuantity() <= 0) {
					TRUInventoryInfo inveReq = new TRUInventoryInfo(invRes.getCatalogRefId(),
							(long) pSkuIdAndLocIdMap.get(invRes.getCatalogRefId() + TRUCommerceConstants.UNDER_SCORE+ invRes.getLocationId()), Long.toString(getWareHouseLocationId(
									invRes.getLocationId()).longValue()));
					cartInventoryRequestWeb.add(inveReq);
				} else if (invRes.getQuantity() <= 0) {
					inventoryInfoSet.add(invRes);
				}
			}
		}
		if (cartInventoryRequestWeb != null && !cartInventoryRequestWeb.isEmpty()) {
			inventoryResponseWeb = getCoherenceInventoryManager().validateBulkStockLevel(cartInventoryRequestWeb);
			if (inventoryResponseWeb == null || inventoryResponseWeb.isEmpty()) {
				for (TRUInventoryInfo invRes : inventoryResponseWeb) {
					if (invRes.getQuantity() <= 0) {
						inventoryInfoSet.add(invRes);
					}
				}
			} else {
				for (TRUInventoryInfo invRes : inventoryResponseWeb) {
					if (invRes.getQuantity() <= 0) {
						inventoryInfoSet.add(invRes);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShoppingCartUtils::@method::buildSkuSetForInventoryCheck   inventoryInfoSet -->{0}"
					,inventoryInfoSet);
			logDebug("Exiting method TRUShoppingCartUtils.buildSkuSetForInventoryCheck :: END");
		}
		return inventoryInfoSet;
	}
	
	/**
	 * Gets the ware house location id.
	 * 
	 * @param pLocationId
	 *            the location id.
	 * @return the ware house location id.
	 * @throws RepositoryException
	 *             the repository exception.
	 */
	public Double getWareHouseLocationId(String pLocationId) throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("START:: TRUCartModifierHelper.getWareHouseLocationId :: START, LocationId", pLocationId);
		}
		RepositoryItem storeItem = null;
		Double wareHouseCode = null;
		if (pLocationId != null) {
			storeItem = getLocationRepository().getItem(pLocationId, getPropertyManager().getStoreItemDescriptorName());
		}
		if (null != storeItem && null != storeItem.getPropertyValue(getPropertyManager().getWarehouseLocationCode())) {
			wareHouseCode = (Double) storeItem.getPropertyValue(getPropertyManager().getWarehouseLocationCode());
		}
		if (isLoggingDebug()) {
			logDebug("START:: TRUCartModifierHelper.getWareHouseLocationId :: END");
		}
		return wareHouseCode;
	}
	
	/**
	 * Builds the individual shopping cart vo. This method is used to build respective vo for individual item.
	 *
	 * @param pShippingGroupCommerceItemRelationship            the shipping group commerce item relationship.
	 * @param pOrder            the order.
	 * @param pMetaInfo            the meta info.
	 * @return the TRU item detail vo.
	 * @throws CommerceItemNotFoundException             - while getting the Invalid Commerce Item.
	 * @throws InvalidParameterException             - while getting the Invalid Commerce Item.
	 */
	public TRUCartItemDetailVO createIndividualItemDetailVOByRelatioShip(
			TRUShippingGroupCommerceItemRelationship pShippingGroupCommerceItemRelationship, Order pOrder,
			TRUCommerceItemMetaInfo pMetaInfo) throws CommerceItemNotFoundException, InvalidParameterException {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUCartModifierHelper.createIndividualItemDetailVOByRelatioShip :: START");
		}
		TRUShippingGroupCommerceItemRelationship sgCiR = null;
		if (pShippingGroupCommerceItemRelationship == null) {
			return null;
		} else {
			sgCiR = pShippingGroupCommerceItemRelationship;
		}
		TRUCartItemDetailVO itemDetailVO = new TRUCartItemDetailVO();
		itemDetailVO =populateItemDetailVO(sgCiR, itemDetailVO, pOrder, pMetaInfo);
		if (isLoggingDebug()) {
			logDebug("Exiting method TRUCartModifierHelper.buildIndividualShoppingCartVO :: END");
		}
		return itemDetailVO;
	}
	
	
	/**
	 * Sets the inventory info message to item vo.
	 * 
	 * @param pItemDetailVO
	 *            the item detail vo.
	 * @param pCookieLocationId
	 *            the cookie location id.
	 */
	public void setInventoryInfoMessageToItemVO(TRUCartItemDetailVO pItemDetailVO, String pCookieLocationId) {
		long items;
		try {
			String infoMessage = null;
			items = getInventoryBasedOnChannelAvailability(pItemDetailVO.getSkuId(), pItemDetailVO.getProductId(),
					pItemDetailVO.getSiteId(), pItemDetailVO.getLocationId(), pCookieLocationId);
			if (items > 0) {
				if (StringUtils.isBlank(pItemDetailVO.getLocationId())) {
					infoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.TRU_SHOPPINGCART_NO_ONLINE_INVENTORY_INFO_MESSAGE_KEY);
				}else if(StringUtils.isNotBlank(pItemDetailVO.getLocationId()) && pItemDetailVO.getChannelAvailability().equalsIgnoreCase(getConfiguration().getOnlineOnly())){
					infoMessage = TRUCommerceConstants.TRU_SHOPPINGCART_UNAVAILABLE_IN_STORE_PICKUP_INFO_MESSAGE_KEY;
				}else if (StringUtils.isNotBlank(pItemDetailVO.getLocationId())) {
					infoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.TRU_SHOPPINGCART_NO_INVENTORY_AT_SELECTED_STORE_INFO_MESSAGE_KEY);
				}
				infoMessage = MessageFormat.format(infoMessage, pItemDetailVO.getDisplayName());
				pItemDetailVO.setInventoryInfoMessage(infoMessage);
			}
		} catch (SystemException systemException) {
			if (isLoggingError()) {
				logError("Exception in TRUCartModifierHelper.populateItemDetailVO", systemException);
			}
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				logError("Exception in TRUCartModifierHelper.populateItemDetailVO", repositoryException);
			}
		}
	}
	
	
	/**
	 * Gets the inventory based on channel availability.
	 * 
	 * @param pSkuId
	 *            the sku id.
	 * @param pProductId
	 *            the product id.
	 * @param pSiteId
	 *            the site id.
	 * @param pLocationId
	 *            the location id.
	 * @param pCookieLocationId
	 *            the cookie location id.
	 * @return the inventory based on channel availability.
	 * @throws RepositoryException
	 *             the repository exception.
	 */
	public long getInventoryBasedOnChannelAvailability(String pSkuId, String pProductId, String pSiteId, String pLocationId,
			String pCookieLocationId) throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUCartModifierHelper::@method::getInStockListFromOutOfStockItems() : START : pOutOfStockItems --> {0}{1}{2}{3}{4}",
					pSkuId, pProductId, pSiteId, pLocationId, pCookieLocationId);
		}
		TRUCatalogTools catalogTools = (TRUCatalogTools) getCommerceItemManager().getOrderTools().getCatalogTools();
		RepositoryItem skuItem = catalogTools.findSKU(pSkuId);
		if(skuItem == null){
			if(isLoggingWarning()){
				vlogWarning("SKuItem is null for pSkuId {0}",pSkuId);
			}
		}
		Boolean s2sValue = null;
		if(skuItem != null){
			s2sValue = (Boolean) skuItem.getPropertyValue(getPropertyManager().getShipToStoreEligible());
		}
		String shipFromStoreEligible = null;
		if (s2sValue != null && s2sValue.booleanValue()) {
			shipFromStoreEligible = TRUCommerceConstants.YES;
		} else {
			shipFromStoreEligible = TRUCommerceConstants.NO;
		}
		String channelAvailability = null;
		long stockCount = TRUCommerceConstants.ZERO;
		try {
			String itemInStorePickUp = null;
			String skuChannelAvailability = null;
			Date streetDate = null;
			if(skuItem != null){
				itemInStorePickUp = (String) skuItem.getPropertyValue(getPropertyManager().getItemInStorePickUp());
				skuChannelAvailability = (String) skuItem.getPropertyValue(getPropertyManager().getChannelAvailability());
				streetDate = (Date) skuItem.getPropertyValue(getPropertyManager().getStreetDate());
			}
			if (TRUCommerceConstants.YES.equalsIgnoreCase(itemInStorePickUp)
					|| TRUCommerceConstants.YES_STRING.equalsIgnoreCase(itemInStorePickUp)
					|| TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(itemInStorePickUp)) {
				itemInStorePickUp = TRUCommerceConstants.YES;
			} else {
				itemInStorePickUp = TRUCommerceConstants.NO;
			}
			stockCount = getCoherenceInventoryManager().queryStockLevel(pSkuId);
			if (StringUtils.isNotBlank(pLocationId)) {
				if (getConfiguration().getInStoreOnly().equalsIgnoreCase(skuChannelAvailability) ){
					channelAvailability = getConfiguration().getInStoreOnly();
				} else {
					channelAvailability = getConfiguration().getChannelAvailabilityForBoth();
				}
			} else {
				if (shipFromStoreEligible.equals(TRUCommerceConstants.YES) || itemInStorePickUp.equals(TRUCommerceConstants.YES)) {
					channelAvailability = getConfiguration().getChannelAvailabilityForBoth();
				} else {
					channelAvailability = getConfiguration().getOnlineOnly();
				}
			}
			Date currentDate = new Date();
			if ((StringUtils.isNotBlank(channelAvailability) && channelAvailability.equals(getConfiguration().getOnlineOnly())) || (streetDate != null && currentDate.before(streetDate))) {
				return stockCount;
			} else if (StringUtils.isNotBlank(channelAvailability)
					&& channelAvailability.equals(getConfiguration().getInStoreOnly())) {
				RepositoryItem storeItem = getLocationRepository().getItem(pLocationId,
						getPropertyManager().getStoreItemDescriptorName());
				String postalCode = null;
				if (null != storeItem && null != storeItem.getPropertyValue(getPropertyManager().getStorePostalCode())) {
					postalCode = (String) storeItem.getPropertyValue(getPropertyManager().getStorePostalCode());
				}
				List<String> siteIds = Arrays.asList(pSiteId);
				RepositoryItem[] stores = getCoordinateManager().getNearest(getGeoLocatorService().getGeoLocation(postalCode),
						getConfiguration().getDistanceForNearByStoreSearch(), siteIds);
				if (null != stores && stores.length > TRUCommerceConstants.INT_ZERO) {
					List<String> locationIds = getLocationIds(stores);
					if (null != locationIds && !locationIds.isEmpty()) {
						stockCount = getCoherenceInventoryManager().validateMultiStoreInventory(pSkuId, locationIds).size();
					}
				}
			} else {
				stockCount = getStockCountForStoreAndOnline(pSkuId, pSiteId, pLocationId, pCookieLocationId, stockCount);
			}
		} catch (InventoryException e) {
			if (isLoggingError()) {
				logError(
						" InventoryException in @Class::TRUCartModifierHelper::@method::getInventoryBasedOnChannelAvailability() :",
						e);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCartModifierHelper::@method::getInStockListFromOutOfStockItems() :stockCount--> {0}",
					stockCount);
		}
		return stockCount;

	}


	/**
	 * Gets the stock count for store and online.
	 *
	 * @param pSkuId the sku id
	 * @param pSiteId the site id
	 * @param pLocationId the location id
	 * @param pCookieLocationId the cookie location id
	 * @param pStockCount the stock count
	 * @return the stock count for store and online
	 * @throws RepositoryException the repository exception
	 */
	private long getStockCountForStoreAndOnline(String pSkuId, String pSiteId, String pLocationId, String pCookieLocationId,
			long pStockCount) throws RepositoryException {
		long stockCount = pStockCount;
		String locId = null;
		if (StringUtils.isNotBlank(pLocationId)) {
			locId = pLocationId;
		} else {
			locId = pCookieLocationId;
		}
		if (locId != null) {
			RepositoryItem storeItem = getLocationRepository().getItem(locId,
					getPropertyManager().getStoreItemDescriptorName());
			String postalCode = null;
			List<String> locationIds = null;
			if (null != storeItem && null != storeItem.getPropertyValue(getPropertyManager().getStorePostalCode())) {
				postalCode = (String) storeItem.getPropertyValue(getPropertyManager().getStorePostalCode());
			}
			List<String> siteIds = Arrays.asList(pSiteId);
			RepositoryItem[] stores = getCoordinateManager().getNearest(
					getGeoLocatorService().getGeoLocation(postalCode),
					getConfiguration().getDistanceForNearByStoreSearch(), siteIds);
			if (null != stores && stores.length > TRUCommerceConstants.INT_ZERO) {
				locationIds = getLocationIds(stores);
			}
			if (null != locationIds && !locationIds.isEmpty()) {
				if (stockCount > TRUCommerceConstants.INT_ZERO) {
					stockCount = getCoherenceInventoryManager().validateMultiStoreInventory(pSkuId, locationIds).size()
							+ TRUCommerceConstants.INT_ONE;
				} else {
					stockCount = getCoherenceInventoryManager().validateMultiStoreInventory(pSkuId, locationIds).size();
				}
			}
		} else {
			List<String> locationIds = getLocationTools().getLocationIdsForAllStores();
			if (stockCount > TRUCommerceConstants.INT_ZERO) {
				stockCount = getCoherenceInventoryManager().validateMultiStoreInventory(pSkuId, locationIds).size()
						+ TRUCommerceConstants.INT_ONE;
			} else {
				stockCount = getCoherenceInventoryManager().validateMultiStoreInventory(pSkuId, locationIds).size();
			}
		}
		return stockCount;
	}
	
	/**
	 * Gets the removal item list to for whose sku or product is soft deleted from the catalog.
	 * 
	 * @param pOrder
	 *            the order.
	 * @return the removal item list.
	 */
	public Set<String> getRemovalItemList(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("Entering into method TRUCartModifierHelper.getRemovalItemList :: START pOrder {0}", pOrder);
		}
		Set<String> removalItemList = new HashSet<String>();
		List<CommerceItem> items = pOrder.getCommerceItems();
		RepositoryItem[] itemMarkers = null;
		for (CommerceItem commerceItem : items) {
			try {
				if(commerceItem instanceof TRUDonationCommerceItem){
					RepositoryItem skuItem = (RepositoryItem) commerceItem.getAuxiliaryData().getCatalogRef();
					String subType = (String) skuItem.getPropertyValue(getPropertyManager().getSubTypePropertyName());
					if(!TRUCommerceConstants.DONATION.equalsIgnoreCase(subType)){
						removalItemList.add(commerceItem.getId());
					}
				}
				else if (commerceItem instanceof TRUCommerceItemImpl && !(commerceItem instanceof TRUGiftWrapCommerceItem)) {
					TRUCommerceItemImpl item = (TRUCommerceItemImpl) commerceItem;
					RepositoryItem skuItem = findSkuItem(item.getCatalogRefId());
					GWPMarkerManager manager = getGwpMarkerManager();
					itemMarkers = manager.getItemMarkers(item, null, TRUCommerceConstants.STRING_NOT_FOUND);
				    if (itemMarkers == null && (null != skuItem
							.getPropertyValue(getPropertyManager().getUnCartable()) && Boolean.TRUE.equals(skuItem
							.getPropertyValue(getPropertyManager().getUnCartable())))) {
				    	   removalItemList.add(item.getId());
					}
					if (null != skuItem && ((null != skuItem.getPropertyValue(getPropertyManager().getIsDeleted()) && Boolean.TRUE
									.equals(skuItem.getPropertyValue(getPropertyManager().getIsDeleted()))) || (null != skuItem
									.getPropertyValue(getPropertyManager().getWebDisplayFlag())&& Boolean.FALSE.equals(skuItem.getPropertyValue(getPropertyManager().getWebDisplayFlag()))))) {
						removalItemList.add(item.getId());
					}
					if (null != skuItem && Boolean.FALSE.equals(skuItem.getPropertyValue(getPropertyManager().getSuperDisplayFlag()))) {
						removalItemList.add(item.getId());
					}
					if(item.getPriceInfo() == null || (!item.getPriceInfo().isDiscounted() && item.getPriceInfo().getAmount() <= 0)){
						removalItemList.add(item.getId());
					}
					TRUCatalogTools catalogTools = (TRUCatalogTools) getShoppingCartUtils().getPricingTools().getCatalogTools();
					if(catalogTools.isZeroPriceAvailable(item.getAuxiliaryData().getProductId(), item.getCatalogRefId())) {
						if (isLoggingDebug()) {
							vlogDebug("isZeroPriceAvailable is true for SkuID {0}", item.getCatalogRefId());
						}
						removalItemList.add(item.getId());
					}
				}
			} catch (MarkerException markerExe) {
				vlogError(" MarkerException in @Class::TRUCartModifierHelper::@method::getRemovalItemList() :", markerExe);
			} catch (RepositoryException e) {
				vlogError(" RepositoryException in @Class::TRUCartModifierHelper::@method::getRemovalItemList() :", e);
			}

		}
		if (isLoggingDebug()) {
			vlogDebug("EXITING into method TRUCartModifierHelper.getRemovalItemList :: END removalItemList {0}", removalItemList);
		}
		return removalItemList;
	}
	
	/**
	 * Find product.
	 * 
	 * @param pProductId
	 *            the product id.
	 * @return the repository item.
	 * @throws RepositoryException
	 *             the repository exception.
	 */
	public RepositoryItem findProduct(String pProductId) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::findProduct() : START : Product ID -->" + pProductId);
		}
		RepositoryItem productItem = null;
		if (!StringUtils.isBlank(pProductId)) {
			productItem = getCommerceItemManager().getOrderTools().getCatalogTools().findProduct(pProductId);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::findProduct() : END : productItem -->" + productItem);
		}
		return productItem;

	}
	
	/**
	 * Gets the item by relation ship id.
	 * 
	 * @param pOrder
	 *            the order.
	 * @param pRelationShipId
	 *            the relation ship id.
	 * @param pMetaInfoId
	 *            the meta info id.
	 * @return the item by relation ship id.
	 * @throws CommerceItemNotFoundException
	 *             the commerce item not found exception.
	 */
	public TRUCartItemDetailVO getItemByRelationShipId(Order pOrder, String pRelationShipId, String pMetaInfoId)
			throws CommerceItemNotFoundException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::getItemByRelationShipId() : START");
			vlogDebug("Order : {0}, Relation Ship ID: {1}, Meta Info ID: {2}", pOrder, pRelationShipId, pMetaInfoId);
		}
		TRUCartItemDetailVO itemDetailVO = new TRUCartItemDetailVO();
		TRUCartItemDetailVO populateItemDetailVO = null;
		try {
			Relationship relationship = pOrder.getRelationship(pRelationShipId);
			if (relationship instanceof TRUShippingGroupCommerceItemRelationship) {
				List<TRUCommerceItemMetaInfo> metaInfos = ((TRUShippingGroupCommerceItemRelationship) relationship).getMetaInfo();
				TRUCommerceItemMetaInfo metaInfoItem = null;
				if (null != metaInfos && !metaInfos.isEmpty()) {
					if (metaInfos.size() == TRUConstants.ONE) {
						metaInfoItem = metaInfos.get((TRUConstants.ZERO));
					} else {
						for (TRUCommerceItemMetaInfo item : metaInfos) {
							if (String.valueOf(item.getPropertyValue(getPropertyManager().getMetaInfoIdPropertyName()))
									.equalsIgnoreCase(pMetaInfoId)) {
								metaInfoItem = item;
								break;
							}
						}
					}
					populateItemDetailVO = populateItemDetailVO((TRUShippingGroupCommerceItemRelationship) relationship,
							itemDetailVO, pOrder, metaInfoItem);
				}
			}

		} catch (RelationshipNotFoundException rnfe) {
			if (isLoggingError()) {
				logError("RelationshipNotFoundException Exception  in TRUCartModifierHelper.getItemByRelationShipId", rnfe);
			}
		} catch (InvalidParameterException ipe) {
			if (isLoggingError()) {
				logError("InvalidParameterException Exception  in TRUCartModifierHelper.getItemByRelationShipId", ipe);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierHelper::@method::getItemByRelationShipId() : END");
		}
		return populateItemDetailVO;
	}
	
	/**
	 * Sets the in store pick up details.
	 * 
	 * @param pCartItemDetailVO
	 *            the new in store pick up details.
	 */
	public void setInStorePickUpDetails(TRUCartItemDetailVO pCartItemDetailVO) {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUCartModifierHelper.setStoreDetails :: START");
		}
		TRUStoreVO storeVo = new TRUStoreVO();
		try {
			RepositoryItem item = (RepositoryItem) getLocationRepository().getItem(pCartItemDetailVO.getLocationId(),
					getPropertyManager().getStoreItemDescriptorName());
			if (item != null) {
				storeVo.setStoreId((String) item.getPropertyValue(getPropertyManager().getLocationIdPropertyName()));
				storeVo.setStoreName((String) item.getPropertyValue(getPropertyManager().getStoreName()));
				storeVo.setCity((String) item.getPropertyValue(getPropertyManager().getCityName()));
				storeVo.setAddress1((String) item.getPropertyValue(getPropertyManager().getStoreAddress1()));
				storeVo.setStateAddress((String) item.getPropertyValue(getPropertyManager().getStoreStateAddress()));
				storeVo.setPostalCode((String) item.getPropertyValue(getPropertyManager().getStorePostalCode()));
				pCartItemDetailVO.setStoreDetail(storeVo);
			}
		} catch (RepositoryException repExp) {
			vlogError(" RepositoryException :{0}", repExp);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting into method TRUCartModifierHelper.setStoreDetails :: END");
		}
	}
	
	
	/**
	 * Gets the out of stock item list by location id.
	 * 
	 * @param pDeletedList
	 *            the deleted list.
	 * @param pOrder
	 *            the order.
	 * @return the out of stock item list by location id.
	 */
	public Set<String> getOutOfStockItemListByLocationId(Set<String> pDeletedList, Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUCartModifierHelper.getOutOfStockItemListByLocationId :: START");
		}
		Set<String> outOfStockItemList = new HashSet<String>();
		List sgCiRels = null;
		int inventoryStatus = TRUCommerceConstants.INT_ZERO;
		int wareHouseStatus = TRUCommerceConstants.INT_ZERO;
		try {
			sgCiRels = getShippingGroupCommerceItemRelationships(pOrder);
			if ((sgCiRels != null && !sgCiRels.isEmpty())) {
				int sgCiRelsCount = sgCiRels.size();
				for (int i = TRUCommerceConstants.INT_ZERO; i < sgCiRelsCount; ++i) {
					ShippingGroup sg = ((ShippingGroupCommerceItemRelationship) sgCiRels.get(i)).getShippingGroup();
					TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) sgCiRels.get(i);
					TRUCommerceItemImpl item = (TRUCommerceItemImpl) sgCiRel.getCommerceItem();
					if (item instanceof TRUGiftWrapCommerceItem || item instanceof TRUDonationCommerceItem) {
						continue;
					}
					if (!StringUtils.isBlank(item.getCatalogRefId())&& (pDeletedList == null || (pDeletedList != null && !pDeletedList.contains(item.getCatalogRefId()))) && (outOfStockItemList == null || (outOfStockItemList != null && !outOfStockItemList.contains(item
									.getCatalogRefId())))) {
						if (sg instanceof TRUHardgoodShippingGroup || sg instanceof TRUChannelHardgoodShippingGroup) {
							inventoryStatus = getCoherenceInventoryManager().queryAvailabilityStatus(item.getCatalogRefId());
						} else if (sg instanceof TRUInStorePickupShippingGroup) {
							inventoryStatus = getCoherenceInventoryManager().queryAvailabilityStatus(item.getCatalogRefId(),
									((TRUInStorePickupShippingGroup) sg).getLocationId());
						} else if (sg instanceof TRUChannelInStorePickupShippingGroup) {
							inventoryStatus = getCoherenceInventoryManager().queryAvailabilityStatus(item.getCatalogRefId(),
									((TRUChannelInStorePickupShippingGroup) sg).getLocationId());
						}
						if ((sg instanceof TRUChannelInStorePickupShippingGroup || sg instanceof TRUInStorePickupShippingGroup) && inventoryStatus == TRUCommerceConstants.INT_OUT_OF_STOCK) {
							wareHouseStatus = getCoherenceInventoryManager().queryAvailabilityStatus(item.getCatalogRefId(),
									((TRUInStorePickupShippingGroup) sg).getWarhouseLocationCode());
						}
						if (wareHouseStatus == TRUCommerceConstants.INT_IN_STOCK) {
							 ((TRUShippingGroupCommerceItemRelationship) sgCiRels).setWarehousePickup(Boolean.TRUE);
							inventoryStatus = TRUCommerceConstants.INT_IN_STOCK;
							wareHouseStatus = TRUCommerceConstants.INT_ZERO;
						}
						if (inventoryStatus == TRUCommerceConstants.INT_OUT_OF_STOCK
								|| inventoryStatus == TRUCommerceConstants.INT_ZERO) {
							outOfStockItemList.add(item.getId());
						}
					}
				}
				((TRUOrderTools) getCommerceItemManager().getOrderTools()).generateOutOfStockItems(outOfStockItemList, pOrder);
			}
		} catch (CommerceException e) {
			if (isLoggingError()) {
				logError("Commerce Exception in TRUCartModifierHelper.getOutOfStockItemListByLocationId", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting into method TRUCartModifierHelper.getOutOfStockItemListByLocationId :: END");
		}
		return outOfStockItemList;
	}
	
	/**
	 * Populate store info.
	 * 
	 * @param pLocationId
	 *            the location id.
	 * @param pStoreVo
	 *            the store vo.
	 * @return the TRU store vo.
	 */
	public TRUStoreVO populateStoreInfo(String pLocationId, TRUStoreVO pStoreVo) {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUCartModifierHelper.populateStoreInfo :: START");
			vlogDebug("INPUT VALUES: locationId: {0}", pLocationId);
		}
		try {
			RepositoryItem item = (RepositoryItem) getLocationRepository().getItem(pLocationId,
					getPropertyManager().getStoreItemDescriptorName());
			if (item != null) {
				pStoreVo.setStoreId((String) item.getPropertyValue(getPropertyManager().getLocationIdPropertyName()));
				pStoreVo.setStoreName((String) item.getPropertyValue(getPropertyManager().getStoreName()));
				pStoreVo.setCity((String) item.getPropertyValue(getPropertyManager().getCityName()));
				pStoreVo.setAddress1((String) item.getPropertyValue(getPropertyManager().getStoreAddress1()));
				pStoreVo.setStateAddress((String) item.getPropertyValue(getPropertyManager().getStoreStateAddress()));
				pStoreVo.setPostalCode((String) item.getPropertyValue(getPropertyManager().getStorePostalCode()));
				pStoreVo.setPhoneNumber((String) item.getPropertyValue(getPropertyManager().getStorePhoneNumber()));
			}
		} catch (RepositoryException repositoryException) {
			vlogError("RepositoryException in TRUCartModifierHelper.populateStoreInfo::", repositoryException);
		}
		if (isLoggingDebug()) {
			logDebug("END:TRUCartModifierHelper.populateStoreInfo method");
		}
		return pStoreVo;

	}
	
	/**
	 * Sets the commerce item manager.
	 * 
	 * @param pCommerceItemManager
	 *            the new commerce item manager.
	 */
	public void setCommerceItemManager(CommerceItemManager pCommerceItemManager) {
		mCommerceItemManager = pCommerceItemManager;
	}

	/**
	 * Gets the commerce item manager.
	 * 
	 * @return the commerce item manager.
	 */
	public CommerceItemManager getCommerceItemManager() {
		return mCommerceItemManager;
	}

	/**
	 * Gets the property manager.
	 * 
	 * @return the property manager.
	 */
	public TRUCommercePropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 * 
	 * @param pPropertyManager
	 *            the new property manager.
	 */
	public void setPropertyManager(TRUCommercePropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}
	
	
	/**
	 * Gets the error handler manager.
	 * 
	 * @return the error handler manager.
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * Sets the error handler manager.
	 * 
	 * @param pErrorHandlerManager
	 *            the new error handler manager.
	 */
	public void setErrorHandlerManager(DefaultErrorHandlerManager pErrorHandlerManager) {
		mErrorHandlerManager = pErrorHandlerManager;
	}
	
	
	/**
	 * Gets the coherence inventory manager.
	 * 
	 * @return the coherence inventory manager.
	 */
	public TRUCoherenceInventoryManager getCoherenceInventoryManager() {
		return mCoherenceInventoryManager;
	}

	/**
	 * Sets the coherence inventory manager.
	 * 
	 * @param pCoherenceInventoryManager
	 *            the CoherenceInventoryManager to set.
	 */
	public void setCoherenceInventoryManager(TRUCoherenceInventoryManager pCoherenceInventoryManager) {
		mCoherenceInventoryManager = pCoherenceInventoryManager;
	}
	
	/**
	 * Gets the location repository.
	 * 
	 * @return the mLocationRepository.
	 */
	public Repository getLocationRepository() {
		return mLocationRepository;
	}
	
	/**
	 * Sets the location repository.
	 * 
	 * @param pLocationRepository
	 *            the mLocationRepository to set.
	 */
	public void setLocationRepository(Repository pLocationRepository) {
		mLocationRepository = pLocationRepository;
	}
	
	
	/**
	 * Gets the geo locator service.
	 * 
	 * @return the geo locator service.
	 */
	public GeoLocatorService getGeoLocatorService() {
		return this.mGeoLocatorService;
	}

	/**
	 * Sets the geo locator service.
	 * 
	 * @param pGeoLocatorService
	 *            the new geo locator service.
	 */
	public void setGeoLocatorService(GeoLocatorService pGeoLocatorService) {
		this.mGeoLocatorService = pGeoLocatorService;
	}

	/**
	 * Gets the coordinate manager.
	 * 
	 * @return the coordinate manager.
	 */
	public CoordinateManager getCoordinateManager() {
		return this.mCoordinateManager;
	}

	/**
	 * Sets the coordinate manager.
	 * 
	 * @param pCoordinateManager
	 *            the new coordinate manager.
	 */
	public void setCoordinateManager(CoordinateManager pCoordinateManager) {
		this.mCoordinateManager = pCoordinateManager;
	}

	/**
	 * Gets the configuration.
	 * 
	 * @return the configuration.
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * Sets the configuration.
	 * 
	 * @param pConfiguration
	 *            the new configuration.
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		this.mConfiguration = pConfiguration;
	}
	
	  
  /**
   * Gets the location tools.
   *
   * @return the locationTools.
   */
	public TRUStoreLocatorTools getLocationTools() {
		return mLocationTools;
	}
	
	/**
	 * Sets the location tools.
	 *
	 * @param pLocationTools            the locationTools to set.
	 */
	public void setLocationTools(TRUStoreLocatorTools pLocationTools) {
		this.mLocationTools = pLocationTools;
	}	
	
	/** The Gwp marker manager. */
	private GWPMarkerManager mGwpMarkerManager;
	
	/**
	 * Gets the gwp marker manager.
	 *
	 * @return the gwp marker manager
	 */
	public GWPMarkerManager getGwpMarkerManager()
	 {
	    return mGwpMarkerManager;
	 }
	  
	/**
	 * Sets the gwp marker manager.
	 *
	 * @param pGwpMarkerManager the new gwp marker manager
	 */
	public void setGwpMarkerManager(GWPMarkerManager pGwpMarkerManager)
	 {
	    mGwpMarkerManager = pGwpMarkerManager;
	 }
	/**
	 * Gets the shopping cart utils.
	 * 
	 * @return the shopping cart utils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * Sets the shopping cart utils.
	 * 
	 * @param pShoppingCartUtils
	 *            the new shopping cart utils
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		this.mShoppingCartUtils = pShoppingCartUtils;
	}
	
	/**
	 * This method is used to get orderStates.
	 * @return mOrderStates String
	 */
	public OrderStates getOrderStates() {
		return mOrderStates;
	}


	/**
	 *This method is used to set orderStates.
	 *@param pOrderStates String
	 */
	public void setOrderStates(OrderStates pOrderStates) {
		mOrderStates = pOrderStates;
	}


	/**
	 * Gets the camel case string.
	 *
	 * @param pValue the value
	 * @return the camel case string
	 */
	public String getCamelCaseString(String pValue) {
		if(isLoggingDebug()){
			vlogDebug("Start getCamelCaseString() and input param {0}", pValue);
		}
		StringBuffer finalResult = new StringBuffer();		
		StringBuffer stringBuffer=null;
		try {			
			if (StringUtils.isNotBlank(pValue)) {
				String array[] = pValue.split(TRUConstants.WHITE_SPACE);
				if (array != null && array.length > TRUConstants.ONE) {
					for (String ele : array) {
						if ( StringUtils.isNotBlank(ele) &&  ele.trim().length() != TRUConstants._0) {
							stringBuffer=new StringBuffer();
							stringBuffer.append(ele.substring(TRUConstants._0, TRUConstants._1).toUpperCase()).
							append(ele.substring(TRUConstants._1).toLowerCase());
							/*String res = ele.substring(TRUConstants._0, TRUConstants._1).toUpperCase()
									+ ele.substring(TRUConstants._1).toLowerCase();*/
							finalResult = finalResult.append(TRUConstants.WHITE_SPACE).append(stringBuffer);
						}
					}
				} else {
					finalResult = new StringBuffer(pValue.substring(TRUConstants._0, TRUConstants._1).toUpperCase()).append(pValue.substring(TRUConstants._1).toLowerCase());
				}
			}
		} catch (Exception e) {
			if(isLoggingError()){
				logError("Exception at getCamelCaseString()",e);
			}
			finalResult= new StringBuffer(pValue);
		}finally{
			if(isLoggingDebug()){
				vlogDebug("End  getCamelCaseString() and output param {0}", finalResult);
			}
		}
		return finalResult.toString();
	}
	
	/**
	 * This method return
	 * 
	 * @param pOrder - Order
	 * @return - Set<String>
	 */
	public Set<String> removeZeroPricelItems(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("Entering into method TRUCartModifierHelper.removeZeroPricelItemList :: START pOrder {0}",pOrder);
		}
		Set<String> removalItemList = new HashSet<String>();
		List<CommerceItem> items = pOrder.getCommerceItems();
		for (CommerceItem commerceItem : items) {
			if (commerceItem instanceof TRUCommerceItemImpl) {
				TRUCommerceItemImpl item = (TRUCommerceItemImpl) commerceItem;
				if (item.getPriceInfo() == null || (!item.getPriceInfo().isDiscounted() && item.getPriceInfo().getAmount() <= 0)) {
					removalItemList.add(item.getId());
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("EXITING into method TRUCartModifierHelper.removeZeroPricelItemList :: END removalItemList {0}",removalItemList);
		}
		return removalItemList;
	}
}


