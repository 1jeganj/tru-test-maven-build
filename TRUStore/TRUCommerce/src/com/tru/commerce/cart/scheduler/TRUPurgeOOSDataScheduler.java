package com.tru.commerce.cart.scheduler;

import atg.repository.RepositoryException;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.commerce.order.TRUOrderTools;

/**
 * @author PA
 * 
 * The Class TRUPurgeOOSDataScheduler.
 *
 */
public class TRUPurgeOOSDataScheduler extends SingletonSchedulableService {
	
	/** property to hold OrderTools. */
	private TRUOrderTools mOrderTools;
	
	/** The Enabled. */
	private boolean mEnabled;
	
	/**
	 * Scheduler is used to purge the out of stock items where orders are in SUBMITTED state and 
	 * submitted date is earlier yesterday date and out of stock items property is not null
	 * 
	 * @param pScheduler - pScheduler
	 * @param pScheduledJob - pScheduledJob
	 * 
	 */
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurgeOOSDataScheduler::@method::performScheduledTask() : START");
		}
		
		try {
			if(isEnabled()){
				getOrderTools().getPurgeOOSOrders();
			}
			
		} catch (RepositoryException repExc) {				
			if ( isLoggingError() ) {
				logError("RepositoryException while fetch the items TRUOrderTools.getPurgeOOSOrders() ",repExc);
			}
		}
		
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurgeOOSDataScheduler::@method::performScheduledTask() : END");
		}
	}

	/**
	 * Gets the order tools.
	 *
	 * @return the order tools
	 */
	public TRUOrderTools getOrderTools() {
		return mOrderTools;
	}

	/**
	 * Sets the order tools.
	 *
	 * @param pOrderTools the new order tools
	 */
	public void setOrderTools(TRUOrderTools pOrderTools) {
		mOrderTools = pOrderTools;
	}

	/**
	 * Checks if is enabled.
	 *
	 * @return true, if is enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets the enabled.
	 *
	 * @param pEnabled the new enabled
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}
	
	
}
