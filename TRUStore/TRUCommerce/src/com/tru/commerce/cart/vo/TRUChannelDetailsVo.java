package com.tru.commerce.cart.vo;

import java.util.List;

/**
 * The Class TRUChannelDetailsVo.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUChannelDetailsVo {
	
	/** The m channel type. */
	private String mChannelType;
	
	/** The m channel name. */
	private String mChannelName;

	/** The m channel id. */
	private String mChannelId;

	/** The m channel user name. */
	private String mChannelUserName;

	/** The Channel co user name. */
	private String mChannelCoUserName;
	
	/** The Source channel. */
	private String mSourceChannel;
	
	/** Registrant details. **/
	private List<TRURegistrantDetailsVO> mRegistrantDetailsVOs;

	/**
	 * Gets the source channel.
	 *
	 * @return the source channel
	 */
	public String getSourceChannel() {
		return mSourceChannel;
	}

	/**
	 * Sets the source channel.
	 *
	 * @param pSourceChannel the new source channel
	 */
	public void setSourceChannel(String pSourceChannel) {
		mSourceChannel = pSourceChannel;
	}

	/**
	 * Gets the channel type.
	 *
	 * @return the channel type.
	 */
	public String getChannelType() {
		return mChannelType;
	}

	/**
	 * Sets the channel type.
	 *
	 * @param pChannelType the new channel type.
	 */
	public void setChannelType(String pChannelType) {
		mChannelType = pChannelType;
	}

	/**
	 * Gets the channel name.
	 *
	 * @return the channel name.
	 */
	public String getChannelName() {
		return mChannelName;
	}

	/**
	 * Sets the channel name.
	 *
	 * @param pChannelName the new channel name.
	 */
	public void setChannelName(String pChannelName) {
		mChannelName = pChannelName;
	}

	/**
	 * Gets the channel id.
	 *
	 * @return the channel id.
	 */
	public String getChannelId() {
		return mChannelId;
	}

	/**
	 * Sets the channel id.
	 *
	 * @param pChannelId the new channel id.
	 */
	public void setChannelId(String pChannelId) {
		mChannelId = pChannelId;
	}

	/**
	 * Gets the channel user name.
	 *
	 * @return the channel user name.
	 */
	public String getChannelUserName() {
		return mChannelUserName;
	}

	/**
	 * Sets the channel user name.
	 *
	 * @param pChannelUserName the new channel user name.
	 */
	public void setChannelUserName(String pChannelUserName) {
		mChannelUserName = pChannelUserName;
	}

	/**
	 * Gets the channel co user name.
	 *
	 * @return the channel co user name.
	 */
	public String getChannelCoUserName() {
		return mChannelCoUserName;
	}

	/**
	 * Sets the channel co user name.
	 *
	 * @param pChannelCoUserName the new channel co user name.
	 */
	public void setChannelCoUserName(String pChannelCoUserName) {
		mChannelCoUserName = pChannelCoUserName;
	}

	/**
	 * Gets the registrant details v os.
	 *
	 * @return the registrantDetailsVOs.
	 */
	public List<TRURegistrantDetailsVO> getRegistrantDetailsVOs() {
		return mRegistrantDetailsVOs;
	}

	/**
	 * Sets the registrant details v os.
	 *
	 * @param pRegistrantDetailsVOs the registrantDetailsVOs to set.
	 */
	public void setRegistrantDetailsVOs(
			List<TRURegistrantDetailsVO> pRegistrantDetailsVOs) {
		mRegistrantDetailsVOs = pRegistrantDetailsVOs;
	}
}
