/*
 * 
 */
package com.tru.commerce.cart.vo;

import java.util.List;

import atg.repository.RepositoryItem;

/**
 * The Class TRUCartItemDetailVO.
 * @author PA
 * @version 1.0
 */
public class TRUCartItemDetailVO {
	
	/** Property To Hold mOnSale. */
	private boolean mOnSale;

	/** Property To Hold Id. */
	private String mId;

	/** Property To Hold size. */
	private String mSize;

	/** Property To Hold color. */
	private String mColor;

	/** Property To Hold quantity. */
	private long mQuantity;

	/** Property To Hold quantity. */
	private long mCustomerPurchaseLimit;

	/** Property To Hold price. */
	private double mPrice;

	/** Property To Hold sku id. */
	private String mSkuId;

	/** Property To Hold product id. */
	private String mProductId;

	/** Property To Hold display name. */
	private String mDisplayName;

	/** Property To Hold price. */
	private double mTotalAmount;

	/** The m channel. */
	private String mChannel;

	/** The m channel id. */
	private String mChannelId;

	/** The m channel user name. */
	private String mChannelUserName;

	/** The Channel co user name. */
	private String mChannelCoUserName;

	/** The Is gift wrappable. */
	private boolean mGiftWrappable;

	/** The Selected for gift wrap. */
	private boolean mSelectedForGiftWrap;

	/** The Shipping group name. */
	private String mShippingGroupName;

	/** The Warehouse message. */
	private String mWarehouseMessage;

	/** The Location id. */
	private String mLocationId;

	/** The Commerce item id. */
	private String mCommerceItemId;

	/** Property To Hold in store pick up available. */
	private boolean mInStorePickUpAvailable;

	/** Saved Amount. */
	private double mSavedAmount;

	/** Saved percentage. */
	private double mSavedPercentage;

	/** Item shipping surcharge. */
	private double mItemShippingSurcharge;

	/** Display strike through price. */
	private boolean mDisplayStrikeThroughPrice;

	/** The m inventory status. */
	private String mInventoryStatus;

	/** The m gift wrap. */
	private boolean mGiftWrap;

	/** The Pricing Adjustments. */
	private RepositoryItem mPricingAdjustments;

	/** The Sale price. */
	private double mSalePrice;

	/** The Cannel availability. */
	private String mChannelAvailability;

	/** The m channel type. */
	private String mChannelType;

	/** The m ship window max. */
	private String mShipWindowMax;

	/** The m ship window min. */
	private String mShipWindowMin;

	/** The m ProductImage. */
	private String mProductImage;

	/** The m item qty in cart. */
	private long mItemQtyInCart;

	/** Property To Hold from meta info. */
	private boolean mFromMetaInfo;

	/** The m item qty in cart. */
	private long mRelItemQtyInCart;

	/** The m item price strike through in cart. */
	private boolean mDisplayStrikeThrough;

	/** The Invntory info message. */
	private String mInventoryInfoMessage;

	/** Property To Hold adjusted. */
	private boolean mAdjusted;

	/** Property To Hold StreetDate in SKU item. */
	private String mStreetDate;

	/** Property To Hold Wrap ProductId. */
	private String mWrapProductId;

	/** Property To Hold Wrap Sku Id. */
	private String mWrapSkuId;

	/** Property To Hold Wrap Display Name. */
	private String mWrapDisplayName;

	/** Property To Hold Wrap Commerce Item Id. */
	private String mWrapCommerceItemId;

	/** Property To Hold Wrap Quantity. */
	private int mWrapQuantity;

	/** Property To Hold Ship Group Id. */
	private String mShipGroupId;

	/** Property To Hold Meta Info Id. */
	private String mMetaInfoId;
	
	/** Property To ProductItem. */
	private RepositoryItem  mProductItem;
	
	/** Property To Hold mIteamURL. */
	private String mItemURL;
	/** The Donation amount. */
	private Double mDonationAmount;
	
	/** The Donation item. */
	private boolean mDonationItem;
	
	/** The Donation item display name. */
	private String mDonationItemDisplayName;
	
	/** The Donation item image url. */
	private String mDonationItemImageUrl;
	
	/** Holds property for  sku description. */
	private String mSKUDescription;
	
	/** Property To hold Bpp item list. */
	private List<TRUBPPItemDetailVO> mBppItemList;
	
	/** Property To hold Bpp sku image. */
	private String mBppSkuImageUrl;
	
	/** Property To hold Bpp item count. */
	private int mBppItemCount;
	
	/** Property To hold Bpp item Info Id. */
	private String mBppItemInfoId;
	
	/** Property To hold Bpp sku Id. */
	private String mBppSkuId;
	
	/** Property To hold Bpp item Id. */
	private String mBppItemId;

	/** The Site id. */
	private String mSiteId;
	
	/** The Site mUniqueId. */
	private String mUniqueId;
	
	/** The Site mStoreDetail. */
	private TRUStoreVO mStoreDetail;
	
	/** The Warehouse location id. */
	private String mWarehouseLocationId;
	
	/** The Info msg location id. */
	private String mInfoMsgLocationId;

	/** Property To ProductItem. */
	private RepositoryItem  mProductPromotion;
	
	/** The sku item. */
	private RepositoryItem mSkuItem;
	/**
	 * Property to hold mNotifyMe.
	 */
	private String mNotifyMe;
	/** Property To Hold quantity. */
	private long mParentQuantity;
	/** property: holds selectedShippingMethod. */
	private String mSelectedShippingMethod;
	
	/** property: holds selectedShippingMethodPrice. */
	private double mSelectedShippingMethodPrice;
	/** Holds property for item in store pick up. */
	private String mItemInStorePickUp;
	
	/** Holds property for ship from store eligible. */
	private String mShipFromStoreEligible;
	
/** Holds property Promotion Item Description. */
	private String mPromotionItemDescription;
	/** Holds property Promotion Details. */
	private String mPromotionDetails;
	
	/** Holds property mGWPPromotionItem . */
	private Boolean mGWPPromotionItem;
	
	/** Holds property mStoreInventoryStatus . */
	private String mStoreInventoryStatus;
	/** Property To hold mCategoryName. */
	private String mCategoryName;
	
	/** Property To hold SubCategory Names list. */
	private List<String> mSubCategoryNames;
	
	/** Property To hold mBrandName. */
	private String mBrandName;
	
	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return mCategoryName;
	}

	/**
	 * @param pCategoryName the categoryName to set
	 */
	public void setCategoryName(String pCategoryName) {
		mCategoryName = pCategoryName;
	}
	/**
	 * @return the subCategoryNames
	 */
	public List<String> getSubCategoryNames() {
		return mSubCategoryNames;
	}

	/**
	 * @param pSubCategoryNames the subCategoryNames to set
	 */
	public void setSubCategoryNames(List<String> pSubCategoryNames) {
		mSubCategoryNames = pSubCategoryNames;
	}

	/**
	 * @return the gWPPromotionItem
	 */
	public Boolean getGWPPromotionItem() {
		return mGWPPromotionItem;
	}

	/**
	 * @param pGWPPromotionItem the gWPPromotionItem to set
	 */
	public void setGWPPromotionItem(Boolean pGWPPromotionItem) {
		mGWPPromotionItem = pGWPPromotionItem;
	}

	/**
	 * Gets the Promotion Item Description
	 *
	 * @return the PromotionItemDescription
	 */
	public String getPromotionItemDescription() {
		return mPromotionItemDescription;
	}
	
	/**
	 * Sets the  Promotion Item Description.
	 *
	 * @param pPromotionItemDescription the promotionItemDescription to set
	 */
	public void setPromotionItemDescription(String pPromotionItemDescription) {
		  mPromotionItemDescription = pPromotionItemDescription;
	}

	/**
	
	/** Holds property for sShipInOrigContainer. */
	private boolean mShipInOrigContainer;
	
	/**
	 * @return the shipInOrigContainer
	 */
	public boolean isShipInOrigContainer() {
		return mShipInOrigContainer;
	}

	/**
	 * @param pShipInOrigContainer the shipInOrigContainer to set
	 */
	public void setShipInOrigContainer(boolean pShipInOrigContainer) {
		mShipInOrigContainer = pShipInOrigContainer;
	}

	/**
	 * Gets the item in store pick up.
	 *
	 * @return the itemInStorePickUp
	 */
	public String getItemInStorePickUp() {
		return mItemInStorePickUp;
	}

	/**
	 * Sets the item in store pick up.
	 *
	 * @param pItemInStorePickUp the itemInStorePickUp to set
	 */
	public void setItemInStorePickUp(String pItemInStorePickUp) {
		mItemInStorePickUp = pItemInStorePickUp;
	}

	/**
	 * Gets the ship from store eligible.
	 *
	 * @return the shipFromStoreEligible
	 */
	public String getShipFromStoreEligible() {
		return mShipFromStoreEligible;
	}

	/**
	 * Sets the ship from store eligible.
	 *
	 * @param pShipFromStoreEligible the shipFromStoreEligible to set
	 */
	public void setShipFromStoreEligible(String pShipFromStoreEligible) {
		mShipFromStoreEligible = pShipFromStoreEligible;
	}

	/**
	 * Gets the sku item.
	 *
	 * @return the sku item
	 */
	public RepositoryItem getSkuItem() {
		return mSkuItem;
	}

	/**
	 * Sets the sku item.
	 *
	 * @param pSkuItem the new sku item
	 */
	public void setSkuItem(RepositoryItem pSkuItem) {
		mSkuItem = pSkuItem;
	}

	/**
	 * Gets the product promotion.
	 *
	 * @return the productPromotion
	 */
	public RepositoryItem getProductPromotion() {
		return mProductPromotion;
	}

	/**
	 * Sets the product promotion.
	 *
	 * @param pProductPromotion the productPromotion to set
	 */
	public void setProductPromotion(RepositoryItem pProductPromotion) {
		mProductPromotion = pProductPromotion;
	}

	/**
	 * Gets the warehouse location id.
	 *
	 * @return the warehouse location id
	 */
	public String getWarehouseLocationId() {
		return mWarehouseLocationId;
	}

	/**
	 * Sets the warehouse location id.
	 *
	 * @param pWarehouseLocationId the new warehouse location id
	 */
	public void setWarehouseLocationId(String pWarehouseLocationId) {
		mWarehouseLocationId = pWarehouseLocationId;
	}

	/**
	 * Gets the unique id.
	 *
	 * @return the uniqueId
	 */
	public String getUniqueId() {
		return mUniqueId;
	}

	/**
	 * 
	 * Sets the unique id.
	 *
	 * @param pUniqueId the uniqueId to set
	 */
	public void setUniqueId(String pUniqueId) {
		mUniqueId = pUniqueId;
	}

	/**

	 * Gets the donation item display name.
	 *
	 * @return the donation item display name.
	 */
	public String getDonationItemDisplayName() {
		return mDonationItemDisplayName;
	}

	/**
	 * Sets the donation item display name.
	 *
	 * @param pDonationItemDisplayName the new donation item display name.
	 */
	public void setDonationItemDisplayName(String pDonationItemDisplayName) {
		mDonationItemDisplayName = pDonationItemDisplayName;
	}

	/**
	 * Gets the donation item image url.
	 *
	 * @return the donation item image url.
	 */
	public String getDonationItemImageUrl() {
		return mDonationItemImageUrl;
	}

	/**
	 * Sets the donation item image url.
	 *
	 * @param pDonationItemImageUrl the new donation item image url.
	 */
	public void setDonationItemImageUrl(String pDonationItemImageUrl) {
		mDonationItemImageUrl = pDonationItemImageUrl;
	}

	/**
	 * Checks if is donation item.
	 *
	 * @return true, if is donation item.
	 */
	public boolean isDonationItem() {
		return mDonationItem;
	}

	/**
	 * Sets the donation item.
	 *
	 * @param pDonationItem the new donation item.
	 */
	public void setDonationItem(boolean pDonationItem) {
		mDonationItem = pDonationItem;
	}

	/**
	 * Gets the donation amount.
	 *
	 * @return the donation amount.
	 */
	public Double getDonationAmount() {
		return mDonationAmount;
	}

	/**
	 * Sets the donation amount.
	 *
	 * @param pDonationAmount the new donation amount.
	 */
	public void setDonationAmount(Double pDonationAmount) {
		mDonationAmount = pDonationAmount;
	}
	
	/**
	 * Gets the selected shipping method.
	 *
	 * @return the selectedShippingMethod.
	 */
	public String getSelectedShippingMethod() {
		return mSelectedShippingMethod;
	}

	/**
	 * Sets the selected shipping method.
	 *
	 * @param pSelectedShippingMethod the selectedShippingMethod to set.
	 */
	public void setSelectedShippingMethod(String pSelectedShippingMethod) {
		mSelectedShippingMethod = pSelectedShippingMethod;
	}

	/**
	 * Gets the selected shipping method price.
	 *
	 * @return the selectedShippingMethodPrice
	 */
	public double getSelectedShippingMethodPrice() {
		return mSelectedShippingMethodPrice;
	}

	/**
	 * 
	 * Sets the selected shipping method price.
	 *
	 * @param pSelectedShippingMethodPrice the selectedShippingMethodPrice to set
	 */
	public void setSelectedShippingMethodPrice(double pSelectedShippingMethodPrice) {
		mSelectedShippingMethodPrice = pSelectedShippingMethodPrice;
	}

	/**
	 * Gets the site id.
	 *
	 * @return the siteId
	 */
	public String getSiteId() {
		return mSiteId;
	}

	/**
	 * Sets the site id.
	 *
	 * @param pSiteId the new site id
	 */
	public void setSiteId(String pSiteId) {
		mSiteId = pSiteId;
	}

	/**

	 * Gets the channel type.
	 * 
	 * @return the channel type
	 */
	public String getChannelType() {
		return mChannelType;
	}

	/**
	 * Sets the channel type.
	 * 
	 * @param pChannelType
	 *            the new channel type
	 */
	public void setChannelType(String pChannelType) {
		mChannelType = pChannelType;
	}

	/**
	 * Gets the size.
	 * 
	 * @return the size
	 */
	public String getSize() {
		return mSize;
	}

	/**
	 * Sets the size.
	 * 
	 * @param pSize
	 *            the new size
	 */
	public void setSize(String pSize) {
		mSize = pSize;
	}

	/**
	 * Gets the color.
	 * 
	 * @return the color
	 */
	public String getColor() {
		return mColor;
	}

	/**
	 * Sets the color.
	 * 
	 * @param pColor
	 *            the new color
	 */
	public void setColor(String pColor) {
		mColor = pColor;
	}

	/**
	 * Gets the quantity.
	 * 
	 * @return the quantity
	 */
	public long getQuantity() {
		return mQuantity;
	}

	/**
	 * Sets the quantity.
	 * 
	 * @param pQuantity
	 *            the new quantity
	 */
	public void setQuantity(long pQuantity) {
		mQuantity = pQuantity;
	}

	/**
	 * Gets the price.
	 * 
	 * @return the price
	 */
	public double getPrice() {
		return mPrice;
	}

	/**
	 * Sets the price.
	 * 
	 * @param pPrice
	 *            the new price
	 */
	public void setPrice(double pPrice) {
		mPrice = pPrice;
	}

	/**
	 * Gets the sku id.
	 * 
	 * @return the sku id
	 */
	public String getSkuId() {
		return mSkuId;
	}

	/**
	 * Sets the sku id.
	 * 
	 * @param pSkuId
	 *            the new sku id
	 */
	public void setSkuId(String pSkuId) {
		mSkuId = pSkuId;
	}

	/**
	 * Gets the product id.
	 * 
	 * @return the product id
	 */
	public String getProductId() {
		return mProductId;
	}

	/**
	 * Sets the product id.
	 * 
	 * @param pProductId
	 *            the new product id
	 */
	public void setProductId(String pProductId) {
		mProductId = pProductId;
	}

	/**
	 * Gets the display name.
	 * 
	 * @return the display name
	 */
	public String getDisplayName() {
		return mDisplayName;
	}

	/**
	 * Sets the display name.
	 * 
	 * @param pDisplayName
	 *            the new display name
	 */
	public void setDisplayName(String pDisplayName) {
		mDisplayName = pDisplayName;
	}

	/**
	 * Gets the total amount.
	 * 
	 * @return the total amount
	 */
	public double getTotalAmount() {
		return mTotalAmount;
	}

	/**
	 * Sets the total amount.
	 * 
	 * @param pTotalAmount
	 *            the new total amount
	 */
	public void setTotalAmount(double pTotalAmount) {
		mTotalAmount = pTotalAmount;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return mId;
	}

	/**
	 * Sets the id.
	 * 
	 * @param pId
	 *            the new id
	 */
	public void setId(String pId) {
		mId = pId;
	}

	/**
	 * Gets the customer purchase limit.
	 * 
	 * @return the customer purchase limit
	 */
	public long getCustomerPurchaseLimit() {
		return mCustomerPurchaseLimit;
	}

	/**
	 * Sets the customer purchase limit.
	 * 
	 * @param pCustomerPurchaseLimit
	 *            the new customer purchase limit
	 */
	public void setCustomerPurchaseLimit(long pCustomerPurchaseLimit) {
		mCustomerPurchaseLimit = pCustomerPurchaseLimit;
	}

	/**
	 * Gets the channel.
	 * 
	 * @return the channel
	 */
	public String getChannel() {
		return mChannel;
	}

	/**
	 * Sets the channel.
	 * 
	 * @param pChannel
	 *            the new channel
	 */
	public void setChannel(String pChannel) {
		mChannel = pChannel;
	}

	/**
	 * Gets the channel id.
	 * 
	 * @return the channel id
	 */
	public String getChannelId() {
		return mChannelId;
	}

	/**
	 * Sets the channel id.
	 * 
	 * @param pChannelId
	 *            the new channel id
	 */
	public void setChannelId(String pChannelId) {
		mChannelId = pChannelId;
	}

	/**
	 * Gets the channel user name.
	 * 
	 * @return the channel user name
	 */
	public String getChannelUserName() {
		return mChannelUserName;
	}

	/**
	 * Sets the channel user name.
	 * 
	 * @param pChannelUserName
	 *            the new channel user name
	 */
	public void setChannelUserName(String pChannelUserName) {
		mChannelUserName = pChannelUserName;
	}

	/**
	 * Gets the channel co user name.
	 * 
	 * @return the channel co user name
	 */
	public String getChannelCoUserName() {
		return mChannelCoUserName;
	}

	/**
	 * Sets the channel co user name.
	 * 
	 * @param pChannelCoUserName
	 *            the new channel co user name
	 */
	public void setChannelCoUserName(String pChannelCoUserName) {
		mChannelCoUserName = pChannelCoUserName;
	}

	/**
	 * Checks if is selected for gift wrap.
	 * 
	 * @return true, if is selected for gift wrap
	 */
	public boolean isSelectedForGiftWrap() {
		return mSelectedForGiftWrap;
	}

	/**
	 * Sets the selected for gift wrap.
	 * 
	 * @param pSelectedForGiftWrap
	 *            the new selected for gift wrap
	 */
	public void setSelectedForGiftWrap(boolean pSelectedForGiftWrap) {
		mSelectedForGiftWrap = pSelectedForGiftWrap;
	}

	/**
	 * Gets the shipping group name.
	 * 
	 * @return the shipping group name
	 */
	public String getShippingGroupName() {
		return mShippingGroupName;
	}

	/**
	 * Sets the shipping group name.
	 * 
	 * @param pShippingGroupName
	 *            the new shipping group name
	 */
	public void setShippingGroupName(String pShippingGroupName) {
		mShippingGroupName = pShippingGroupName;
	}

	/**
	 * Checks if is gift wrappable.
	 * 
	 * @return true, if is gift wrappable
	 */
	public boolean isGiftWrappable() {
		return mGiftWrappable;
	}

	/**
	 * Sets the gift wrappable.
	 * 
	 * @param pGiftWrappable
	 *            the new gift wrappable
	 */
	public void setGiftWrappable(boolean pGiftWrappable) {
		mGiftWrappable = pGiftWrappable;
	}

	/**
	 * Gets the warehouse message.
	 * 
	 * @return the warehouse message
	 */
	public String getWarehouseMessage() {
		return mWarehouseMessage;
	}

	/**
	 * Sets the warehouse message.
	 * 
	 * @param pWarehouseMessage
	 *            the new warehouse message
	 */
	public void setWarehouseMessage(String pWarehouseMessage) {
		mWarehouseMessage = pWarehouseMessage;
	}

	/**
	 * Gets the location id.
	 * 
	 * @return the location id
	 */
	public String getLocationId() {
		return mLocationId;
	}

	/**
	 * Sets the location id.
	 * 
	 * @param pLocationId
	 *            the new location id
	 */
	public void setLocationId(String pLocationId) {
		mLocationId = pLocationId;
	}

	/**
	 * Gets the commerce item id.
	 * 
	 * @return the commerce item id
	 */
	public String getCommerceItemId() {
		return mCommerceItemId;
	}

	/**
	 * Sets the commerce item id.
	 * 
	 * @param pCommerceItemId
	 *            the new commerce item id
	 */
	public void setCommerceItemId(String pCommerceItemId) {
		mCommerceItemId = pCommerceItemId;
	}

	/**
	 * Checks if is in store pick up available.
	 * 
	 * @return true, if is in store pick up available
	 */
	public boolean isInStorePickUpAvailable() {
		return mInStorePickUpAvailable;
	}

	/**
	 * Sets the in store pick up available.
	 * 
	 * @param pInStorePickUpAvailable
	 *            the new in store pick up available
	 */
	public void setInStorePickUpAvailable(boolean pInStorePickUpAvailable) {
		mInStorePickUpAvailable = pInStorePickUpAvailable;
	}

	/**
	 * Returns the savedAmount.
	 * 
	 * @return the savedAmount
	 */
	public double getSavedAmount() {
		return mSavedAmount;
	}

	/**
	 * Sets/updates the savedAmount.
	 * 
	 * @param pSavedAmount
	 *            the savedAmount to set
	 */
	public void setSavedAmount(double pSavedAmount) {
		mSavedAmount = pSavedAmount;
	}

	/**
	 * Returns the savedPercentage.
	 * 
	 * @return the savedPercentage
	 */
	public double getSavedPercentage() {
		return mSavedPercentage;
	}

	/**
	 * Sets/updates the savedPercentage.
	 * 
	 * @param pSavedPercentage
	 *            the savedPercentage to set
	 */
	public void setSavedPercentage(double pSavedPercentage) {
		mSavedPercentage = pSavedPercentage;
	}

	/**
	 * Returns the itemShippingSurcharge.
	 * 
	 * @return the itemShippingSurcharge
	 */
	public double getItemShippingSurcharge() {
		return mItemShippingSurcharge;
	}

	/**
	 * Sets/updates the itemShippingSurcharge.
	 * 
	 * @param pItemShippingSurcharge
	 *            the itemShippingSurcharge to set
	 */
	public void setItemShippingSurcharge(double pItemShippingSurcharge) {
		mItemShippingSurcharge = pItemShippingSurcharge;
	}

	/**
	 * Returns the displayStrikeThroughPrice.
	 * 
	 * @return the displayStrikeThroughPrice
	 */
	public boolean isDisplayStrikeThroughPrice() {
		return mDisplayStrikeThroughPrice;
	}

	/**
	 * Sets/updates the displayStrikeThroughPrice.
	 * 
	 * @param pDisplayStrikeThroughPrice
	 *            the displayStrikeThroughPrice to set
	 */
	public void setDisplayStrikeThroughPrice(boolean pDisplayStrikeThroughPrice) {
		mDisplayStrikeThroughPrice = pDisplayStrikeThroughPrice;
	}

	/**
	 * Gets the inventory status.
	 * 
	 * @return the inventory status
	 */
	public String getInventoryStatus() {
		return mInventoryStatus;
	}

	/**
	 * Sets the inventory status.
	 * 
	 * @param pInventoryStatus
	 *            the new inventory status
	 */
	public void setInventoryStatus(String pInventoryStatus) {
		mInventoryStatus = pInventoryStatus;
	}

	/**
	 * Checks if is gift wrap.
	 * 
	 * @return true, if is gift wrap
	 */
	public boolean isGiftWrap() {
		return mGiftWrap;
	}

	/**
	 * Sets the gift wrap.
	 * 
	 * @param pGiftWrap
	 *            the new gift wrap
	 */
	public void setGiftWrap(boolean pGiftWrap) {
		mGiftWrap = pGiftWrap;
	}

	/**
	 * Returns the salePrice.
	 * 
	 * @return the salePrice
	 */
	public double getSalePrice() {
		return mSalePrice;
	}

	/**
	 * Sets/updates the salePrice.
	 * 
	 * @param pSalePrice
	 *            the salePrice to set
	 */
	public void setSalePrice(double pSalePrice) {
		mSalePrice = pSalePrice;
	}

	/**
	 * Gets the channel availability.
	 * 
	 * @return the channel availability
	 */
	public String getChannelAvailability() {
		return mChannelAvailability;
	}

	/**
	 * Sets the channel availability.
	 * 
	 * @param pChannelAvailability
	 *            the new channel availability
	 */
	public void setChannelAvailability(String pChannelAvailability) {
		mChannelAvailability = pChannelAvailability;
	}

	/**
	 * Gets the ship window max.
	 * 
	 * @return the ship window max
	 */
	public String getShipWindowMax() {
		return mShipWindowMax;
	}

	/**
	 * Sets the ship window max.
	 * 
	 * @param pShipWindowMax
	 *            the new ship window max
	 */
	public void setShipWindowMax(String pShipWindowMax) {
		mShipWindowMax = pShipWindowMax;
	}

	/**
	 * Gets the ship window min.
	 * 
	 * @return the ship window min
	 */
	public String getShipWindowMin() {
		return mShipWindowMin;
	}

	/**
	 * Sets the ship window min.
	 * 
	 * @param pShipWindowMin
	 *            the new ship window min
	 */
	public void setShipWindowMin(String pShipWindowMin) {
		mShipWindowMin = pShipWindowMin;
	}

	/**
	 * Gets the product image.
	 * 
	 * @return the mProductImage
	 */
	public String getProductImage() {
		return mProductImage;
	}

	/**
	 * Sets the product image.
	 * 
	 * @param pProductImage
	 *            the new product image
	 */
	public void setProductImage(String pProductImage) {
		this.mProductImage = pProductImage;
	}

	/**
	 * Gets the item qty in cart.
	 * 
	 * @return the item qty in cart
	 */
	public long getItemQtyInCart() {
		return mItemQtyInCart;
	}

	/**
	 * Sets the item qty in cart.
	 * 
	 * @param pItemQtyInCart
	 *            the new item qty in cart
	 */
	public void setItemQtyInCart(long pItemQtyInCart) {
		mItemQtyInCart = pItemQtyInCart;
	}

	/**
	 * Checks if is from meta info.
	 * 
	 * @return true, if is from meta info
	 */
	public boolean isFromMetaInfo() {
		return mFromMetaInfo;
	}

	/**
	 * Sets the from meta info.
	 * 
	 * @param pFromMetaInfo
	 *            the new from meta info
	 */
	public void setFromMetaInfo(boolean pFromMetaInfo) {
		mFromMetaInfo = pFromMetaInfo;
	}

	/**
	 * Gets the rel item qty in cart.
	 * 
	 * @return the rel item qty in cart
	 */
	public long getRelItemQtyInCart() {
		return mRelItemQtyInCart;
	}

	/**
	 * Sets the rel item qty in cart.
	 * 
	 * @param pRelItemQtyInCart
	 *            the new rel item qty in cart
	 */
	public void setRelItemQtyInCart(long pRelItemQtyInCart) {
		mRelItemQtyInCart = pRelItemQtyInCart;
	}

	/**
	 * Checks if is display strike through.
	 * 
	 * @return true, if is display strike through
	 */
	public boolean isDisplayStrikeThrough() {
		return mDisplayStrikeThrough;
	}

	/**
	 * Sets the display strike through.
	 * 
	 * @param pDisplayStrikeThrough
	 *            the new display strike through
	 */
	public void setDisplayStrikeThrough(boolean pDisplayStrikeThrough) {
		mDisplayStrikeThrough = pDisplayStrikeThrough;
	}
	
	/**
	 * Gets the inventory info message.
	 * @return the inventoryInfoMessage
	 */
	public String getInventoryInfoMessage() {
		return mInventoryInfoMessage;
	}

	/**
	 * Sets the inventory info message.
	 * @param pInventoryInfoMessage the inventoryInfoMessage to set
	 */
	public void setInventoryInfoMessage(String pInventoryInfoMessage) {
		mInventoryInfoMessage = pInventoryInfoMessage;
	}

	/**
	 * Checks if is adjusted.
	 * 
	 * @return true, if is adjusted
	 */
	public boolean isAdjusted() {
		return mAdjusted;
	}

	/**
	 * Sets the adjusted.
	 * 
	 * @param pAdjusted
	 *            the new adjusted
	 */
	public void setAdjusted(boolean pAdjusted) {
		mAdjusted = pAdjusted;
	}

	/**
	 * Gets the street date.
	 * 
	 * @return the street date
	 */
	public String getStreetDate() {
		return mStreetDate;
	}

	/**
	 * Sets the street date.
	 * 
	 * @param pStreetDate
	 *            the new street date
	 */
	public void setStreetDate(String pStreetDate) {
		mStreetDate = pStreetDate;
	}

	/**
	 * Gets the wrap product id.
	 *
	 * @return the wrapProductId
	 */
	public String getWrapProductId() {
		return mWrapProductId;
	}

	/**
	 * Sets the wrap product id.
	 *
	 * @param pWrapProductId            the wrapProductId to set
	 */
	public void setWrapProductId(String pWrapProductId) {
		mWrapProductId = pWrapProductId;
	}

	/**
	 * Gets the wrap sku id.
	 *
	 * @return the wrapSkuId
	 */
	public String getWrapSkuId() {
		return mWrapSkuId;
	}

	/**
	 * Sets the wrap sku id.
	 *
	 * @param pWrapSkuId            the wrapSkuId to set
	 */
	public void setWrapSkuId(String pWrapSkuId) {
		mWrapSkuId = pWrapSkuId;
	}

	/**
	 * Gets the wrap display name.
	 *
	 * @return the wrapDisplayName
	 */
	public String getWrapDisplayName() {
		return mWrapDisplayName;
	}

	/**
	 * Sets the wrap display name.
	 *
	 * @param pWrapDisplayName            the wrapDisplayName to set
	 */
	public void setWrapDisplayName(String pWrapDisplayName) {
		mWrapDisplayName = pWrapDisplayName;
	}

	/**
	 * Gets the wrap commerce item id.
	 *
	 * @return the wrapCommerceItemId
	 */
	public String getWrapCommerceItemId() {
		return mWrapCommerceItemId;
	}

	/**
	 * Sets the wrap commerce item id.
	 *
	 * @param pWrapCommerceItemId            the wrapCommerceItemId to set
	 */
	public void setWrapCommerceItemId(String pWrapCommerceItemId) {
		mWrapCommerceItemId = pWrapCommerceItemId;
	}

	/**
	 * Gets the wrap quantity.
	 *
	 * @return the wrapQuantity
	 */
	public int getWrapQuantity() {
		return mWrapQuantity;
	}

	/**
	 * Sets the wrap quantity.
	 *
	 * @param pWrapQuantity            the wrapQuantity to set
	 */
	public void setWrapQuantity(int pWrapQuantity) {
		mWrapQuantity = pWrapQuantity;
	}

	/**
	 * Gets the ship group id.
	 *
	 * @return the shipGroupId
	 */
	public String getShipGroupId() {
		return mShipGroupId;
	}

	/**
	 * Sets the ship group id.
	 *
	 * @param pShipGroupId            the shipGroupId to set
	 */
	public void setShipGroupId(String pShipGroupId) {
		mShipGroupId = pShipGroupId;
	}

	/**
	 * Gets the meta info id.
	 *
	 * @return the meta info id
	 */
	public String getMetaInfoId() {
		return mMetaInfoId;
	}

	/**
	 * Sets the meta info id.
	 *
	 * @param pMetaInfoId the new meta info id
	 */
	public void setMetaInfoId(String pMetaInfoId) {
		mMetaInfoId = pMetaInfoId;
	}

	/**
	 * Gets the pricing adjustments.
	 *
	 * @return the pricingAdjustments
	 */
	public RepositoryItem getPricingAdjustments() {
		return mPricingAdjustments;
	}

	/**
	 * Sets the pricing adjustments.
	 *
	 * @param pPricingAdjustments the pricingAdjustments to set
	 */
	public void setPricingAdjustments(RepositoryItem pPricingAdjustments) {
		mPricingAdjustments = pPricingAdjustments;
	}

	/**
	 * Gets the product item.
	 *
	 * @return the productItem
	 */
	public RepositoryItem getProductItem() {
		return mProductItem;
	}

	/**
	 * Sets the product item.
	 *
	 * @param pProductItem the productItem to set
	 */
	public void setProductItem(RepositoryItem pProductItem) {
		mProductItem = pProductItem;
	}

	/**
	 * Gets the item url.
	 *
	 * @return the mItemURL
	 */
	public String getItemURL() {
		return mItemURL;
	}

	/**
	 * Sets the item url.
	 *
	 * @param pItemURL the new item url
	 */
	public void setItemURL(String pItemURL) {
		this.mItemURL = pItemURL;
	}
	/**
	 * Gets the SKU description.
	 *
	 * @return the SKU description
	 */
	public String getSKUDescription() {
		return mSKUDescription;
	}

	/**
	 * Sets the SKU description.
	 *
	 * @param pSKUDescription the new SKU description
	 */
	public void setSKUDescription(String pSKUDescription) {
		this.mSKUDescription = pSKUDescription;
	}

	/**
	 * Gets the bpp item list.
	 *
	 * @return the bpp item list
	 */
	public List<TRUBPPItemDetailVO> getBppItemList() {
		return mBppItemList;
	}

	/**
	 * Sets the bpp item list.
	 *
	 * @param pBppItemList the new bpp item list
	 */
	public void setBppItemList(List<TRUBPPItemDetailVO> pBppItemList) {
		mBppItemList = pBppItemList;
	}

	/**
	 * Gets the bpp sku image url.
	 *
	 * @return the bpp sku image url
	 */
	public String getBppSkuImageUrl() {
		return mBppSkuImageUrl;
	}

	/**
	 * Sets the bpp sku image url.
	 *
	 * @param pBppSkuImageUrl the new bpp sku image url
	 */
	public void setBppSkuImageUrl(String pBppSkuImageUrl) {
		mBppSkuImageUrl = pBppSkuImageUrl;
	}

	/**
	 * Gets the bpp item count.
	 *
	 * @return the bpp item count
	 */
	public int getBppItemCount() {
		return mBppItemCount;
	}

	/**
	 * Sets the bpp item count.
	 *
	 * @param pBppItemCount the new bpp item count
	 */
	public void setBppItemCount(int pBppItemCount) {
		mBppItemCount = pBppItemCount;
	}

	/**
	 * Gets the bpp item info id.
	 *
	 * @return the bpp item info id
	 */
	public String getBppItemInfoId() {
		return mBppItemInfoId;
	}

	/**
	 * Sets the bpp item info id.
	 *
	 * @param pBppItemInfoId the new bpp item info id
	 */
	public void setBppItemInfoId(String pBppItemInfoId) {
		mBppItemInfoId = pBppItemInfoId;
	}

	/**
	 * Gets the bpp sku id.
	 *
	 * @return the bpp sku id
	 */
	public String getBppSkuId() {
		return mBppSkuId;
	}

	/**
	 * Sets the bpp sku id.
	 *
	 * @param pBppSkuId the new bpp sku id
	 */
	public void setBppSkuId(String pBppSkuId) {
		mBppSkuId = pBppSkuId;
	}

	/**
	 * Gets the bpp item id.
	 *
	 * @return the bpp item id
	 */
	public String getBppItemId() {
		return mBppItemId;
	}

	/**
	 * Sets the bpp item id.
	 *
	 * @param pBppItemId the new bpp item id
	 */
	public void setBppItemId(String pBppItemId) {
		mBppItemId = pBppItemId;
	}

	/**
	 * Gets the store detail.
	 *
	 * @return the mStoreDetail
	 */
	public TRUStoreVO getStoreDetail() {
		return mStoreDetail;
	}

	/**
	 * Sets the store detail.
	 *
	 * @param pStoreDetail the mStoreDetail to set
	 */
	public void setStoreDetail(TRUStoreVO pStoreDetail) {
		this.mStoreDetail = pStoreDetail;
	}

	/**
	 * Gets the notify me.
	 *
	 * @return the mNotifyMe
	 */
	public String getNotifyMe() {
		return mNotifyMe;
	}

	/**
	 * Sets the notify me.
	 *
	 * @param pNotifyMe the mNotifyMe to set
	 */
	public void setNotifyMe(String pNotifyMe) {
		this.mNotifyMe = pNotifyMe;
	}

	

	/**
	 * Gets the parent quantity.
	 *
	 * @return the parentQuantity
	 */
	public long getParentQuantity() {
		return mParentQuantity;
	}

	/**
	 * Sets the parent quantity.
	 *
	 * @param pParentQuantity the parentQuantity to set
	 */
	public void setParentQuantity(long pParentQuantity) {
		mParentQuantity = pParentQuantity;
	}

	/**
	 * Gets the info msg location id.
	 *
	 * @return the info msg location id
	 */
	public String getInfoMsgLocationId() {
		return mInfoMsgLocationId;
	}

	
	 
	/**
	 * Sets the info msg location id.
	 *
	 * @param pInfoMsgLocationId the new info msg location id
	 */
	public void setInfoMsgLocationId(String pInfoMsgLocationId) {
		mInfoMsgLocationId = pInfoMsgLocationId;
	}

	/**
	 * @return the mOnSale
	 */
	public boolean isOnSale() {
		return mOnSale;
	}

	/**
	 * @param pOnSale the onSale to set
	 */
	public void setOnSale(boolean pOnSale) {
		this.mOnSale = pOnSale;
	}

	/**
	 * Gets the promotion details.
	 *
	 * @return the promotion details
	 */
	public String getPromotionDetails() {
		return mPromotionDetails;
	}

	/**
	 * Sets the promotion details.
	 *
	 * @param pPromotionDetails the new promotion details
	 */
	public void setPromotionDetails(String pPromotionDetails) {
		this.mPromotionDetails = pPromotionDetails;
	}

	/**
	 * @return the mStoreInventoryStatus
	 */
	public String getStoreInventoryStatus() {
		return mStoreInventoryStatus;
	}

	/**
	 * @param pStoreInventoryStatus the mStoreInventoryStatus to set
	 */
	public void setStoreInventoryStatus(String pStoreInventoryStatus) {
		this.mStoreInventoryStatus = pStoreInventoryStatus;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TRUCartItemDetailVO [mSkuId=" + mSkuId + ", mTotalAmount="
				+ mTotalAmount + ", mChannel=" + mChannel
				+ ", mShippingGroupName=" + mShippingGroupName + ", mSkuItem="
				+ mSkuItem + ", mSelectedShippingMethod="
				+ mSelectedShippingMethod + ", mSelectedShippingMethodPrice="
				+ mSelectedShippingMethodPrice + "]";
	}

	/**
	 * @return the mBrandName
	 */
	public String getBrandName() {
		return mBrandName;
	}

	/**
	 * @param pBrandName the mBrandName to set
	 */
	public void setBrandName(String pBrandName) {
		mBrandName = pBrandName;
	}
	
}
	
