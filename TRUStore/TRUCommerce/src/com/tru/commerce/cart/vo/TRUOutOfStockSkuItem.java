/**
 * 
 */
package com.tru.commerce.cart.vo;

import java.util.List;

/**
 * The Class TRUOutOfStockSku.
 * @author PA
 * @version 1.0
 */
public class TRUOutOfStockSkuItem {
	
	/** Property To Hold pSkuIdList. */
	private List<String> mSkuIdList;
	
	/** Property To Hold pInventoryInfoMessage. */
    private String mInventoryInfoMessage;
    
    /** Property To Hold mRelationShipId. */
    private List<String> mRelationShipId;

	/**
	 * @return the mSkuIdList
	 */
	public List<String> getSkuIdList() {
		return mSkuIdList;
	}

	/**
	 * @param pSkuIdList the mSkuIdList to set
	 */
	public void setSkuIdList(List<String> pSkuIdList) {
		this.mSkuIdList = pSkuIdList;
	}

	/**
	 * @return the mInventoryInfoMessage
	 */
	public String getInventoryInfoMessage() {
		return mInventoryInfoMessage;
	}

	/**
	 * @param pInventoryInfoMessage the mInventoryInfoMessage to set
	 */
	public void setInventoryInfoMessage(String pInventoryInfoMessage) {
		this.mInventoryInfoMessage = pInventoryInfoMessage;
	}

	/**
	 * @return the mRelationShipId
	 */
	public List<String> getRelationShipId() {
		return mRelationShipId;
	}

	/**
	 * @param pRelationShipId the mRelationShipId to set
	 */
	public void setRelationShipId(List<String> pRelationShipId) {
		this.mRelationShipId = pRelationShipId;
	}
	
}
