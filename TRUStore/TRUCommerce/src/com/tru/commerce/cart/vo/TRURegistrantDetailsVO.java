package com.tru.commerce.cart.vo;

import atg.core.util.ContactInfo;

/**
 * 
 * @author PA
 * @version 1.0
 *
 */
public class TRURegistrantDetailsVO {
	
	/** holds registrant id. */
	private String mRegistrantId;

	/** holds registrant address. */
	private ContactInfo mAddress;

	/**
	 * @return the registrantId.
	 */
	public String getRegistrantId() {
		return mRegistrantId;
	}

	/**
	 * @param pRegistrantId the registrantId to set.
	 */
	public void setRegistrantId(String pRegistrantId) {
		mRegistrantId = pRegistrantId;
	}

	/**
	 * @return the address.
	 */
	public ContactInfo getAddress() {
		return mAddress;
	}

	/**
	 * @param pAddress the address to set.
	 */
	public void setAddress(ContactInfo pAddress) {
		mAddress = pAddress;
	}
	
	
}
