/**
 * 
 */
package com.tru.commerce.cart.vo;

/**
 * The Class TRUStoreVO.
 * @author PA
 * @version 1.0
 */
public class TRUStoreVO {
	
	
	/** The m store id. */
	private String mStoreId;
	/** Property To Hold StoreName. */
	private String mStoreName;
	/** Property To Hold PostalCode. */
	private String mPostalCode;
	/** Property To Hold StreetAddress. */
	private String mStateAddress;
	/** Property To Hold PhoneNumber. */
	private String mPhoneNumber;
	/** Property To Hold City. */
	private String mCity;
	/** Property To Hold Address1. */
	private String mAddress1;
	
	
	/**
	 * Gets the store id.
	 *
	 * @return the store id
	 */
	public String getStoreId() {
		return mStoreId;
	}
	
	
	/**
	 * Sets the store id.
	 *
	 * @param pStoreId the new store id
	 */
	public void setStoreId(String pStoreId) {
		this.mStoreId = pStoreId;
	}
	
	
	/**
	 * Gets the store name.
	 *
	 * @return the store name
	 */
	public String getStoreName() {
		return mStoreName;
	}
	
	
	/**
	 * Sets the store name.
	 *
	 * @param pStoreName the new store name
	 */
	public void setStoreName(String pStoreName) {
		this.mStoreName = pStoreName;
	}
	
	/**
	 * Gets the postal code.
	 *
	 * @return the mPostalCode
	 */
	public String getPostalCode() {
		return mPostalCode;
	}
	
	/**
	 * Sets the postal code.
	 *
	 * @param pPostalCode is the postalCode of store
	 */
	public void setPostalCode(String pPostalCode) {
		this.mPostalCode = pPostalCode;
	}
	
	/**
	 * Gets the state address.
	 *
	 * @return the mstateAddress
	 */
	public String getStateAddress() {
		return mStateAddress;
	}
	
	/**
	 * Sets the state address.
	 *
	 * @param pStateAddress is the State Address of the store
	 */
	public void setStateAddress(String pStateAddress) {
		this.mStateAddress = pStateAddress;
	}
	
	/**
	 * Gets the phone number.
	 *
	 * @return the mPhoneNumber
	 */
	public String getPhoneNumber() {
		return mPhoneNumber;
	}
	
	/**
	 * Sets the phone number.
	 *
	 * @param pPhoneNumber is the store phoneNumber
	 */
	public void setPhoneNumber(String pPhoneNumber) {
		this.mPhoneNumber = pPhoneNumber;
	}
	
	
	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return mCity;
	}
	
	/**
	 * Sets the city.
	 *
	 * @param pCity is the city of the Store
	 */
	public void setCity(String pCity) {
		this.mCity = pCity;
	}
	
	/**
	 * Gets the address1.
	 *
	 * @return the mAddress1
	 */
	public String getAddress1() {
		return mAddress1;
	}
	
	/**
	 * Sets the address1.
	 *
	 * @param pAddress1 is the address of the store
	 */
	public void setAddress1(String pAddress1) {
		this.mAddress1 = pAddress1;
	}
}
