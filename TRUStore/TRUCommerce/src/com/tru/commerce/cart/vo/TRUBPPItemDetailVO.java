package com.tru.commerce.cart.vo;


/**
 * The Class TRUBPPItemDetailVO.
 * @author PA.
 * @version 1.0
 */
public class TRUBPPItemDetailVO {

	/** Property To hold Bpp Item Info Id. */
	private String mBppItemInfoId;

	/** Property To hold Bpp Item Id. */
	private String mBppItemId;

	/** Property To hold Bpp Sku Id. */
	private String mBppSkuId;
	
	/** Property To hold Bpp Parent Sku Id. */
	private String mBppParentSkuId;
	
	/** Property To hold Bpp Relationship Id. */
	private String mBppRelId;
	
	/** Property To hold Bpp Plan. */
	private String mBppPlan;
	
	/** Property To hold mPreSelected. */
	private boolean mPreSelected;
	
	/** Property To hold Bpp retail. */
	private double mBppRetail;
	
	/** Property To hold Bpp term. */
	private String mBppTerm;

	/**
	 * Gets the bpp item info id.
	 *
	 * @return the bpp item info id
	 */
	public String getBppItemInfoId() {
		return mBppItemInfoId;
	}

	/**
	 * Sets the bpp item info id.
	 *
	 * @param pBppItemInfoId the new bpp item info id
	 */
	public void setBppItemInfoId(String pBppItemInfoId) {
		mBppItemInfoId = pBppItemInfoId;
	}

	/**
	 * Gets the bpp item id.
	 *
	 * @return the bpp item id
	 */
	public String getBppItemId() {
		return mBppItemId;
	}

	/**
	 * Sets the bpp item id.
	 *
	 * @param pBppItemId the new bpp item id
	 */
	public void setBppItemId(String pBppItemId) {
		mBppItemId = pBppItemId;
	}

	/**
	 * Gets the bpp sku id.
	 *
	 * @return the bpp sku id
	 */
	public String getBppSkuId() {
		return mBppSkuId;
	}

	/**
	 * Sets the bpp sku id.
	 *
	 * @param pBppSkuId the new bpp sku id
	 */
	public void setBppSkuId(String pBppSkuId) {
		mBppSkuId = pBppSkuId;
	}

	/**
	 * Gets the bpp parent sku id.
	 *
	 * @return the bpp parent sku id
	 */
	public String getBppParentSkuId() {
		return mBppParentSkuId;
	}

	/**
	 * Sets the bpp parent sku id.
	 *
	 * @param pBppParentSkuId the new bpp parent sku id
	 */
	public void setBppParentSkuId(String pBppParentSkuId) {
		mBppParentSkuId = pBppParentSkuId;
	}

	/**
	 * Gets the bpp rel id.
	 *
	 * @return the bpp rel id
	 */
	public String getBppRelId() {
		return mBppRelId;
	}

	/**
	 * Sets the bpp rel id.
	 *
	 * @param pBppRelId the new bpp rel id
	 */
	public void setBppRelId(String pBppRelId) {
		mBppRelId = pBppRelId;
	}

	/**
	 * Gets the bpp plan.
	 *
	 * @return the bpp plan
	 */
	public String getBppPlan() {
		return mBppPlan;
	}

	/**
	 * Sets the bpp plan.
	 *
	 * @param pBppPlan the new bpp plan
	 */
	public void setBppPlan(String pBppPlan) {
		mBppPlan = pBppPlan;
	}

	/**
	 * Checks if is pre selected.
	 *
	 * @return true, if is pre selected
	 */
	public boolean isPreSelected() {
		return mPreSelected;
	}

	/**
	 * Sets the pre selected.
	 *
	 * @param pPreSelected the new pre selected
	 */
	public void setPreSelected(boolean pPreSelected) {
		mPreSelected = pPreSelected;
	}

	/**
	 * Gets the bpp retail.
	 *
	 * @return the bpp retail
	 */
	public double getBppRetail() {
		return mBppRetail;
	}

	/**
	 * Sets the bpp retail.
	 *
	 * @param pBppRetail the new bpp retail
	 */
	public void setBppRetail(double pBppRetail) {
		mBppRetail = pBppRetail;
	}

	/**
	 * Gets the bpp term.
	 *
	 * @return the bpp term
	 */
	public String getBppTerm() {
		return mBppTerm;
	}

	/**
	 * Sets the bpp term.
	 *
	 * @param pBppTerm the new bpp term
	 */
	public void setBppTerm(String pBppTerm) {
		mBppTerm = pBppTerm;
	}


}
