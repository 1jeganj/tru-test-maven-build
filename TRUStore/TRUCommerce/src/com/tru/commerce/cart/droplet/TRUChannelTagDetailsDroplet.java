package com.tru.commerce.cart.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUChannelInStorePickupShippingGroup;
import com.tru.common.TRUConfiguration;

/**
 * The Class TRUChannelTagDetailsDroplet.
 * @author PA
 * @version 1.0
 */
public class TRUChannelTagDetailsDroplet extends DynamoServlet {

	/** Property To property manager. */
	private TRUCommercePropertyManager mPropertyManager;

	/** Property to hold is mConfiguration. */
	private TRUConfiguration mConfiguration;

	/**
	 * Gets the configuration.
	 *
	 * @return the configuration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * Sets the configuration.
	 *
	 * @param pConfiguration the new configuration
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		mConfiguration = pConfiguration;
	}

	/**
	 * Gets the property manager.
	 * 
	 * @return the property manager
	 */
	public TRUCommercePropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 * 
	 * @param pPropertyManager
	 *            the new property manager
	 */
	public void setPropertyManager(TRUCommercePropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * This method is used to get the channel((Registry/Wishlist)) tag details.
	 * Like channel display name and corresponding URL
	 * which we need to display in cart header.
	 *
	 * @param pRequest            the request
	 * @param pResponse            the response
	 * @throws ServletException             the servlet exception
	 * @throws IOException             Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if(isLoggingDebug()){
			logDebug("Entering into method TRUChannelTagDetailsDroplet.service :: START");
		}
		
		final Order orderObj = (Order) pRequest.getObjectParameter(TRUCommerceConstants.PARAM_ORDER);
		
		vlogDebug("TRUChannelTagDetailsDroplet.service :: Order --> {0}", orderObj);
		
		if (null != orderObj && orderObj.getCommerceItems() != null && !orderObj.getCommerceItems().isEmpty()) {
			final List<CommerceItem> commerceItemList = orderObj.getCommerceItems();
			final List<CommerceItem> finalCommerceItemList = new ArrayList<CommerceItem>();
			final ListIterator<CommerceItem> listIterator = commerceItemList.listIterator(commerceItemList.size());
			while (listIterator.hasPrevious()) {
				finalCommerceItemList.add(listIterator.previous());
			}
			Set<String> registrySet = new HashSet<String>();
			Set<String> wishlistSet = new HashSet<String>();
			String lastRegistryUserName = null;
			String lastWishlitsUserName = null;
			for (CommerceItem commerceItem : finalCommerceItemList) {
				final List<ShippingGroupCommerceItemRelationship> cmSgRelList = commerceItem.getShippingGroupRelationships();
				if (null != cmSgRelList && !cmSgRelList.isEmpty()) {
					for (ShippingGroupCommerceItemRelationship sgcir : cmSgRelList) {
						final ShippingGroup sg = sgcir.getShippingGroup();
						if (sg instanceof TRUChannelHardgoodShippingGroup) {
							final TRUChannelHardgoodShippingGroup hgSg = (TRUChannelHardgoodShippingGroup) sg;
							if (null != hgSg.getChannelType() && null != hgSg.getChannelId() &&
									hgSg.getChannelType().equals(getPropertyManager().getRegistry())) {
								registrySet.add(hgSg.getChannelId());
								lastRegistryUserName = hgSg.getChannelUserName().toLowerCase();
							} else if (null != hgSg.getChannelType() && null != hgSg.getChannelId() &&
									hgSg.getChannelType().equals(getPropertyManager().getWishlist())) {
								wishlistSet.add(hgSg.getChannelId());
								lastWishlitsUserName = hgSg.getChannelUserName().toLowerCase();
							}
						} else if (sg instanceof TRUChannelInStorePickupShippingGroup) {
							final TRUChannelInStorePickupShippingGroup inStoreSg = (TRUChannelInStorePickupShippingGroup) sg;
							if (null != inStoreSg.getChannelType() && null != inStoreSg.getChannelId() &&
									inStoreSg.getChannelType().equals(getPropertyManager().getRegistry())) {
								registrySet.add(inStoreSg.getChannelId());
								lastRegistryUserName = inStoreSg.getChannelUserName().toLowerCase();
							} else if (null != inStoreSg.getChannelType()&& null != inStoreSg.getChannelId() &&
									inStoreSg.getChannelType().equals(getPropertyManager().getWishlist())) {
								wishlistSet.add(inStoreSg.getChannelId());
								lastWishlitsUserName = inStoreSg.getChannelUserName().toLowerCase();
							}
						}
					}
				}
			}
			setRequestParameters(pRequest, registrySet, wishlistSet, lastRegistryUserName, lastWishlitsUserName);
		}
		pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		if(isLoggingDebug()){
			logDebug("Exiting  method TRUChannelTagDetailsDroplet.service :: END");
		}
	}

	/**
	 * Sets the request parameters.
	 *
	 * @param pRequest the request
	 * @param pRegistrySet the registry set
	 * @param pWishlistSet the wishlist set
	 * @param pLastRegistryUserName the last registry user name
	 * @param pLastWishlitsUserName the last wishlits user name
	 */
	private void setRequestParameters(DynamoHttpServletRequest pRequest, Set<String> pRegistrySet,
			Set<String> pWishlistSet, String pLastRegistryUserName, String pLastWishlitsUserName) {
		Cookie[] cookies = pRequest.getCookies();
		Cookie cookie = null;
		if (cookies != null) {
			for (Cookie tempCookie : cookies) {
				if (tempCookie.getName().equals(TRUCommerceConstants.SOS_COOKIE_NAME)) {
					cookie = tempCookie;
					break;
				}
			}
		}
		if(pRegistrySet != null && !pRegistrySet.isEmpty()){
			if(cookie != null){
				if( pRegistrySet.size() > TRUCommerceConstants.INT_ONE){
					pRequest.setParameter(TRUCommerceConstants.REGISTRY_URL, getConfiguration().getRegistrySearchUrl());
				}else{
					for (String channelId : pRegistrySet) {
						StringBuffer sb = new StringBuffer(getConfiguration().getSosRegistryUrl());
						StringBuffer finalUrl = sb.append(channelId).append(TRUCommerceConstants.REGISTRY_TYPE);
						pRequest.setParameter(TRUCommerceConstants.REGISTRY_USER_NAME, pLastRegistryUserName);
						pRequest.setParameter(TRUCommerceConstants.REGISTRY_URL, finalUrl);
					}
				}
			}else{
				for (String channelId : pRegistrySet) {
					StringBuffer sb = new StringBuffer(getConfiguration().getRegistryUrl());
					pRequest.setParameter(TRUCommerceConstants.REGISTRY_USER_NAME, pLastRegistryUserName);
					pRequest.setParameter(TRUCommerceConstants.REGISTRY_URL, sb.append(channelId));
				}
			}
		}
		if(pWishlistSet != null && !pWishlistSet.isEmpty()){
			if(cookie != null){
				if( pWishlistSet.size() > TRUCommerceConstants.INT_ONE){
					pRequest.setParameter(TRUCommerceConstants.WISHLIST_URL, getConfiguration().getWishlistSearchUrl());
				}else{
					for (String channelId : pWishlistSet) {
						StringBuffer sb = new StringBuffer(getConfiguration().getSosWishlistUrl());
						StringBuffer finalUrl = sb.append(channelId).append(TRUCommerceConstants.WISHLIST_TYPE);
						pRequest.setParameter(TRUCommerceConstants.WISHLIST_USER_NAME, pLastWishlitsUserName);
						pRequest.setParameter(TRUCommerceConstants.WISHLIST_URL, finalUrl);
					}
				}
			}else{
				for (String channelId : pWishlistSet) {
					StringBuffer sb = new StringBuffer(getConfiguration().getWishlistUrl());
					pRequest.setParameter(TRUCommerceConstants.WISHLIST_USER_NAME, pLastWishlitsUserName);
					pRequest.setParameter(TRUCommerceConstants.WISHLIST_URL, sb.append(channelId));
				}
			}
		}
	}
}