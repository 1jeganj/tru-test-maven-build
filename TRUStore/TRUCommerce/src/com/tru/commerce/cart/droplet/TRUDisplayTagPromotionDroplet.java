package com.tru.commerce.cart.droplet;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

import com.tru.common.TRUConstants;

/**
 * TRUDisplayTagPromotionDroplet extends the OOTB DynamoServlet.
 * This Droplet will be used to display Tag Promotion Descriptions.
 * 
 * <p>
 * Input Parameters:
 * <ul>
 * <li>profile - Profile Object.
 * <li>all - String - all.
 * <li>item - String - item.
 * <li>order - String - order.
 * <li>shipping - String - shipping.
 * </ul>
 * Output tagPromos.
 * <ul>
 * <li>tagPromos - Return all or Only Item or Shipping or Order level promotions.
 * </ul>
 * <p>
 * Open Parameters:
 * <ul>
 * <li>output. 
 * <li>empty.
 * </ul>
 * @author PA
 * @version 1.0
 *
 */
public class TRUDisplayTagPromotionDroplet extends DynamoServlet{
	
	/**property to hold promotionTools. */
	private TRUPromotionTools mPromotionTools;
	
	
	/**
	 * This method overrides the OOTB service method to display Tag Promotion Descriptions.
	 * 
	 * @param pRequest
	 *            reference to DynamoHttpServletRequest.
	 * @param pResponse
	 *            reference to DynamoHttpServletResponse.
	 * @throws IOException - 
	 * 			 an error occurred reading data from the request or writing data to the response.
	 * @throws ServletException - 
	 * 			an application specific error occurred processing this request.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Start of TRUDisplayTagPromotionDroplet : service()");
		}
		final Profile profile =(Profile) pRequest.getObjectParameter(TRUConstants.PROFILE);
		final String allPromotion = (String) pRequest.getObjectParameter(TRUConstants.ALL_PROMO);
		final String itemLevelPromotions = (String) pRequest.getObjectParameter(TRUConstants.ITEM_PROMO);
		final String orderPromotions = (String) pRequest.getObjectParameter(TRUConstants.ORDER_PROMO);
		final String shippingPromotions = (String) pRequest.getObjectParameter(TRUConstants.SHIPPING_PROMO);
		Set<RepositoryItem> allTagPromotions = null;
		Set<RepositoryItem> itemTagPromotions = null;
		Set<RepositoryItem> orderTagPromotions = null;
		Set<RepositoryItem> shippingTagPromotions = null;
		if((!StringUtils.isBlank(allPromotion)) && allPromotion.equalsIgnoreCase(TRUConstants.ALL_PROMO)){
			// calling promotion tools method to get the all Active Tag Promotions
			allTagPromotions = getPromotionTools().getAllActivePromotions(profile);
		}else{
			allTagPromotions = new HashSet<RepositoryItem>();
			if((!StringUtils.isBlank(itemLevelPromotions)) && itemLevelPromotions.equalsIgnoreCase(TRUConstants.ITEM_PROMO) && 
					getPromotionTools().getAllItemLevelPromotions(profile) != null){
				// calling promotion tools method to get the all Item Level Active Tag Promotions
				itemTagPromotions = getPromotionTools().getAllItemLevelPromotions(profile);
					allTagPromotions.addAll(itemTagPromotions);
			}
			if((!StringUtils.isBlank(orderPromotions)) && orderPromotions.equalsIgnoreCase(TRUConstants.ORDER_PROMO) && 
					getPromotionTools().getAllOrderPromotions(profile) != null){
				// calling promotion tools method to get the all Order Level Active Tag Promotions
				orderTagPromotions = getPromotionTools().getAllOrderPromotions(profile);
					allTagPromotions.addAll(orderTagPromotions);
			}
			if((!StringUtils.isBlank(shippingPromotions)) && shippingPromotions.equalsIgnoreCase(TRUConstants.SHIPPING_PROMO) &&
					getPromotionTools().getAllShippingPromotions(profile) != null ){
				// calling promotion tools method to get the all Shipping Level Active Tag Promotions
				shippingTagPromotions = getPromotionTools().getAllShippingPromotions(profile);
				allTagPromotions.addAll(shippingTagPromotions);
			}
			
		}
		if(allTagPromotions != null && !allTagPromotions.isEmpty()){
			pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
			pRequest.setParameter(TRUConstants.TAG_PROMOS, allTagPromotions);
		}else{
			pRequest.serviceLocalParameter(TRUConstants.EMPTY_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("Ending of TRUDisplayTagPromotionDroplet : service()");
		}
		
	}

	/**
	 * Gets the promotion tools.
	 *
	 * @return the promotionTools.
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}
	/**
	 * Sets the promotion tools.
	 *
	 * @param pPromotionTools the promotionTools to set.
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		this.mPromotionTools = pPromotionTools;
	}
	
	
}
