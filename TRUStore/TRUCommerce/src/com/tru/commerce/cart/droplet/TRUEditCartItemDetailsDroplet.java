/**
 * 
 */
package com.tru.commerce.cart.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.Order;
import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.cart.vo.TRUCartItemDetailVO;
import com.tru.endeca.utils.TRUProductPageUtil;

/**
 * The Class TRUEditCartItemDetailsDroplet.
 *
 * @author PA
 * @version 1.0
 */
public class TRUEditCartItemDetailsDroplet extends DynamoServlet {

	/** property to Hold shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;

	/** The m product page util. */
	private TRUProductPageUtil mProductPageUtil;
	/**
	 * This method is used to get the current order as input and iterates all the commerce items present in the current.
	 * order and send the items in list of shopping cart vo's.
	 * 
	 * @param pRequest
	 *            the request.
	 * @param pResponse
	 *            the response.
	 * @throws ServletException
	 *             the servlet exception.
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUEditCartItemDetailsDroplet.service :: START");
		}
		boolean isAPIRequest = false;
		if (pRequest.getContextPath().equals(TRUCommerceConstants.REST_CONTEXT_PATH)) {
			isAPIRequest = true;
		}
		final Order order = (Order) pRequest.getObjectParameter(TRUCommerceConstants.PARAM_ORDER);
		final String relationShipId = (String) pRequest.getLocalParameter(TRUCommerceConstants.RELATIONSHIP_ID);
		final String metaInfoId = (String) pRequest.getLocalParameter(TRUCommerceConstants.METAINFO_ID);
		if ((order != null && !order.getCommerceItems().isEmpty()) && StringUtils.isNotBlank(relationShipId)) {
			try {
				if (isAPIRequest) {
					TRUCartItemDetailVO cartItem = getShoppingCartUtils().getTRUCartModifierHelper().getItemByRelationShipId(order,
							relationShipId, metaInfoId);
					populateItemURL(cartItem);
					pRequest.setParameter(TRUCommerceConstants.ITEM,cartItem);	
				} else {
					pRequest.setParameter(TRUCommerceConstants.ITEM,getShoppingCartUtils().getTRUCartModifierHelper().getItemByRelationShipId(order, 
							relationShipId, metaInfoId));	
				}
				
			} catch (CommerceItemNotFoundException e) {
				vlogError("CommerceItemNotFoundException in TRUEditCartItemDetailsDroplet.service()::" , e);
			}
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
			if (isLoggingDebug()) {
				logDebug("TRUEditCartItemDetailsDroplet.service :: ");
			}
		} else {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting  method TRUEditCartItemDetailsDroplet.service :: END");
		}
	}
	
	/**
	 * This method should populate PDP page URL for mobile service.
	 * @param pCartItem cartItem.
	 */
	protected void populateItemURL(TRUCartItemDetailVO pCartItem) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUEditCartItemDetailsDroplet::@method::populateItemURL() : START");
		}
		Site site = SiteContextManager.getCurrentSite();
		if (pCartItem != null) {
			String pdpUrl = getPDPURL(
					(String) pCartItem.getSkuItem().getPropertyValue(
							getShoppingCartUtils().getCatalogProperties().getOnlinePID()), site);
			if (!StringUtils.isEmpty(pdpUrl)) {
				pCartItem.setItemURL(pdpUrl);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUEditCartItemDetailsDroplet::@method::populateItemURL() : END");
		}
	}
	/**
	 * This method will return the PDP page URL based on the productId and siteId.
	 * @param pProductId productId.
	 * @param pSiteId siteId.
	 * @return string.
	 */
	protected String getPDPURL(String pProductId, Site pSiteId) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUEditCartItemDetailsDroplet::@method::getPDPURL() : START");
		}
		String productPageUrl = null;
		if (!StringUtils.isEmpty(pProductId) && pSiteId != null) {
			if (StringUtils.isNotEmpty(pSiteId.toString())) {
				productPageUrl = getProductPageUtil().getProductPageURL(pProductId, pSiteId.toString());
			} else {
				productPageUrl = getProductPageUtil().getProductPageURL(pProductId);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUEditCartItemDetailsDroplet::@method::getPDPURL() : END");
		}
		return productPageUrl;
	}
	/**
	 * Gets the shopping cart utils.
	 * 
	 * @return the shopping cart utils.
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * Sets the shopping cart utils.
	 * 
	 * @param pShoppingCartUtils
	 *            the new shopping cart utils.
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		this.mShoppingCartUtils = pShoppingCartUtils;
	}

	/**
	 * @return the mProductPageUtil.
	 */
	public TRUProductPageUtil getProductPageUtil() {
		return mProductPageUtil;
	}

	/**
	 * @param pProductPageUtil
	 *         the new productPageUtil.
	 */
	public void setProductPageUtil(TRUProductPageUtil pProductPageUtil) {
		this.mProductPageUtil = pProductPageUtil;
	}

}
