package com.tru.commerce.cart.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.vo.TRUCartItemDetailVO;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;

/**
 * This class is TRUShopLocalDroplet.
 * @author Professional Access
 * @version 1.0
 */
public class TRUShopLocalDroplet extends DynamoServlet{
	
	/** Property to hold mCatalogTools. */
	private TRUCatalogTools mCatalogTools;
	
	/** Property to hold mCatalogProperties. */
	private TRUCatalogProperties mCatalogProperties;
	
	/**
	 * This method generate the items added from shoplocal and returns shoplocallist.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse the response
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		String requestURI = pRequest.getRequestURI();
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShopLocalDroplet::@method::service() : BEGIN");
			vlogDebug("@Class::TRUShopLocalDroplet::@method::service() rquest URI:", requestURI);
		}
		if (requestURI != null) {
			List<TRUCartItemDetailVO> shopLocalList = new ArrayList<TRUCartItemDetailVO>();
			for(int i=1;i<=pRequest.getParameterMap().size();i++){
				String prod = TRUCommerceConstants.PROD+i;
				String qty = TRUCommerceConstants.QTY+i;
				if(pRequest.getParameterMap().get(prod)!=null && pRequest.getParameterMap().get(qty)!=null){
					TRUCartItemDetailVO shopLocalItemInfo = new TRUCartItemDetailVO();
					String [] pidtemp = (String[]) pRequest.getParameterMap().get(prod);
					String onlinePID = pidtemp[0].trim();
					String [] qtytemp = (String[]) pRequest.getParameterMap().get(qty);
					String quantity = qtytemp[0].trim();
					setDetailsToCartItem(shopLocalItemInfo, onlinePID,
							quantity);
					shopLocalItemInfo.setLocationId(TRUCommerceConstants.EMPTY_STRING);
					shopLocalList.add(shopLocalItemInfo);
				}
			}
			pRequest.setParameter(TRUCommerceConstants.SHOP_LOCAL_LIST, shopLocalList);
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShopLocalDroplet::@method::service() : END");
		}
	}
	
	/**
	 * Setting the ProductId and Quantity details to the TRUCartItemDetaisVO Object. 
	 *
	 * @param pShopLocalItemInfo TRUCartItemDetailVO Object
	 * @param pOnlinePID the online pid
	 * @param pQuantity the Quantity of Item
	 */
	private void setDetailsToCartItem(TRUCartItemDetailVO pShopLocalItemInfo,
			String pOnlinePID, String pQuantity) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShopLocalDroplet::@method::setDetailsToCartItem()  Product Id and SKU Id :",
					pOnlinePID, pQuantity);
		}
		TRUCatalogTools catalogTools=(TRUCatalogTools) getCatalogTools();
		if(StringUtils.isNotBlank(pOnlinePID)){
			pShopLocalItemInfo.setProductId(catalogTools.getProductIdFromOnlinePID(pOnlinePID));
		}
		if(StringUtils.isNotBlank(pQuantity) && pQuantity instanceof String){
			pShopLocalItemInfo.setQuantity(Long.parseLong((String)pQuantity));
		}
			pShopLocalItemInfo.setSkuId(catalogTools.getSkuIdFromOnlinePID(pOnlinePID));
	}
	
	/**
	 * Gets the catalog tools.
	 *
	 * @return the mCatalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}
	
	/**
	 * Sets the catalog tools.
	 *
	 * @param pCatalogTools the new catalog tools
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		this.mCatalogTools = pCatalogTools;
	}
	
	/**
	 * Gets the catalog properties.
	 *
	 * @return the mCatalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}
	
	/**
	 * Sets the catalog properties.
	 *
	 * @param pCatalogProperties the mCatalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		this.mCatalogProperties = pCatalogProperties;
	}
}
