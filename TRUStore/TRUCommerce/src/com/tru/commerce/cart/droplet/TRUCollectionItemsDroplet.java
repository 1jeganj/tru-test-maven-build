package com.tru.commerce.cart.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.vo.TRUCartItemDetailVO;
import com.tru.common.TRUConstants;

/**
 * The Class TRUCollectionItemsDroplet to process the collection details sent.
 * from Collection PDP page.
 * @author PA
 * @version 1.0
 */
public class TRUCollectionItemsDroplet extends DynamoServlet{
	
	/** The Constant DEFAULT_STRING. */
	public static final String PIPE_STRING = "\\|";
	
	/**
	 * This method is used to have the collection items.
	 *
	 * @param pRequest DynamoHttpServletRequest.
	 * @param pResponse the response.
	 * @throws ServletException ServletException.
	 * @throws IOException IOException.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCollectionItemsDroplet::@method::service() : BEGIN");
		}
		final String collection = (String) pRequest.getLocalParameter(TRUCommerceConstants.COLLECTION);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCollectionItemsDroplet::@method::service() : collection --> ", collection);
		}
		final List<TRUCartItemDetailVO> collectionItemInfoList = generateCollectionList(collection);
		if (collectionItemInfoList == null) {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		} else {
			pRequest.setParameter(TRUCommerceConstants.COLLECTION, collectionItemInfoList);
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
			if (isLoggingDebug()) {
				logDebug("TRUCollectionItemsDroplet.service :: ");
			}

		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCollectionItemsDroplet::@method::service() : END");
		}
	}
	
	/**
	 * This method generate the collection list that has been selected by the user.
	 *
	 * @param pCollection - collection of items selected by user in string format.
	 * @return collectionItemInfoList - collection of items selected by user from PDP.
	 */
	public List<TRUCartItemDetailVO> generateCollectionList(String pCollection) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCollectionItemsDroplet::@method::generateItemInfos() : BEGIN");
		}
		final List<TRUCartItemDetailVO> collectionItemInfoList = new ArrayList<TRUCartItemDetailVO>();
		if (StringUtils.isNotEmpty(pCollection)) {
			final String[] productInfos = pCollection.split(TRUConstants.TILDA_STRING);
			if (productInfos != null && productInfos.length > 0) {
				for (String productInfoItem : productInfos) {
					if (StringUtils.isNotEmpty(productInfoItem)) {
						final String[] productInfo = productInfoItem.split(PIPE_STRING);
						if (productInfo != null && productInfo.length >= TRUConstants.THREE) {
							final TRUCartItemDetailVO collectionItemInfo = new TRUCartItemDetailVO();
							collectionItemInfo.setProductId(productInfo[TRUConstants.ZERO]);
							if (StringUtils.isNotBlank(productInfo[TRUConstants.ONE])) {
								collectionItemInfo.setLocationId(productInfo[TRUConstants.ONE]);
							} else {
								collectionItemInfo.setLocationId(null);
							}
							if (StringUtils.isNotEmpty(productInfo[TRUConstants.TWO])) {
								collectionItemInfo.setQuantity(Long.parseLong(productInfo[TRUConstants.TWO]));
							}
							collectionItemInfo.setSkuId(productInfo[TRUConstants.THREE]);
							if (isLoggingDebug()) {
								vlogDebug(
										"@Class::TRUCollectionItemsDroplet::@method::generateItemInfos() collectionItemInfo is: ",
										collectionItemInfo);
							}
							collectionItemInfoList.add(collectionItemInfo);
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCollectionItemsDroplet::@method::generateItemInfos() : END");
		}
		return collectionItemInfoList;
	}
}
