package com.tru.commerce.cart.droplet;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.promotion.PromotionImportExportInfo;
import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.TRUConstants;

/**
 * TRUFreeShippingProgressBarDroplet extends the OOTB DynamoServlet.
 * This Droplet will be used to display the Shipping Progression Bar on shopping cart and mini cart page.
 * 
 * <p>
 * Input Parameters:
 * <ul>
 * <li>profile - Profile Object.
 * <li>orderTotal - Double -orderTotal value
 * </ul>
 * Output Parameters
 * <ul>
 * <li>remainingAmount
 * <li>spendValue 
 * <li>freeShippingParam 
 * </ul>
 * <p>
 * Open Parameters:
 * <ul>
 * <li>output 
 * <li>empty
 * </ul>
 * 
 * @author PA
 * @version 1.0
 */
public class TRUFreeShippingProgressBarDroplet extends DynamoServlet{
	
	/**property to hold promotionTools. */
	private TRUPromotionTools mPromotionTools;
	
	/**
	 * This method overrides the OOTB service method to display the shipping progress bar on cart and mini cart page.
	 * 
	 * @param pRequest
	 *            reference to DynamoHttpServletRequest
	 * @param pResponse
	 *            reference to DynamoHttpServletResponse
	 * @throws IOException - 
	 * 			 an error occurred reading data from the request or writing data to the response.
	 * @throws ServletException - 
	 * 			an application specific error occurred processing this request.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start of TRUFreeShippingProgressBarDroplet : service()");
		}
		final Profile profile =(Profile) pRequest.getObjectParameter(TRUConstants.PROFILE);
		final Double odrerTotal = (Double) pRequest.getObjectParameter(TRUConstants.ORDER_TOTAL);
		List<PromotionImportExportInfo> shippingPromotions = null;
		if(profile != null && odrerTotal != null){
			if (isLoggingDebug()) {
				logDebug("Exiting from of TRUFreeShippingProgressBarDroplet service method because Profile or odrerTotal null");
			}
			// Method to get all shipping promotions
			shippingPromotions = getPromotionTools().getShippingPromotions(profile);
		}
		if(shippingPromotions != null && !shippingPromotions.isEmpty()){
			String discountValue = null;
			String spendValue = null;
			for(PromotionImportExportInfo info : shippingPromotions){
				final Map<String, String> values = info.getTemplateValues();
				if(values != null && !values.isEmpty()){
					discountValue = values.get(TRUConstants.DISCOUNT_VALUE_KEY);
					spendValue = values.get(TRUConstants.SPEND_VALUE_KEY);
					final TRUPricingTools pricingTool = (TRUPricingTools) getPromotionTools().getPricingTools();
					if((!StringUtils.isBlank(discountValue)) && (!StringUtils.isBlank(spendValue)) && 
							discountValue.equalsIgnoreCase(TRUConstants.DISCOUNT_VALUE)){
						final double lSpendValue = pricingTool.round(Double.parseDouble(spendValue));
						final double lDiff = pricingTool.round(lSpendValue-odrerTotal.doubleValue());
						pRequest.setParameter(TRUConstants.FREE_SHIPPING_PARAM, Boolean.FALSE);
						pRequest.setParameter(TRUConstants.REMAINING_AMOUNT_PARAM, TRUConstants.DOUBLE_ZERO);
						if(lDiff > TRUConstants.ZERO){
							pRequest.setParameter(TRUConstants.REMAINING_AMOUNT_PARAM, lDiff);
						}else{
							pRequest.setParameter(TRUConstants.FREE_SHIPPING_PARAM, Boolean.TRUE);
						}
						pRequest.setParameter(TRUConstants.SPEND_VALUE_PARAM, lSpendValue);
						pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
						break;
					}
				}
			}
		}else{
			pRequest.serviceLocalParameter(TRUConstants.EMPTY_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("End of TRUFreeShippingProgressBarDroplet : service()");
		}
	}
	/**
	 * Gets the promotion tools.
	 *
	 * @return the promotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}
	/**
	 * Sets the promotion tools.
	 *
	 * @param pPromotionTools the promotionTools to set
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		this.mPromotionTools = pPromotionTools;
	}
	
}
