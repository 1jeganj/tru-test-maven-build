package com.tru.commerce.cart.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;

/**
 * The Class TRUGetDonationSKUILookupDroplet.
 *
 * @author PA
 * @version 1.0
 */
public class TRUDonationSKULookupDroplet extends DynamoServlet {
	
	/** The Catalog tools. */
	private TRUCatalogTools mCatalogTools;
	
	/**
	 * Gets the catalog tools.
	 *
	 * @return the catalog tools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}


	/**
	 * Sets the catalog tools.
	 *
	 * @param pCatalogTools the new catalog tools
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}


	/**
	 * This method is used to get the donation skuItem from repository.
	 *
	 * @param pRequest            the request
	 * @param pResponse            the response
	 * @throws ServletException             the servlet exception
	 * @throws IOException             Signals that an I/O exception has occurred.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUGetDonationSKUILookupDroplet::@method::service() : BEGIN");
		}
		if(getCatalogTools() != null && getCatalogTools().getDonationOrBPPSKUItem(TRUCommerceConstants.DONATION) != null){
			final RepositoryItem donationSKUItem = getCatalogTools().getDonationOrBPPSKUItem(TRUCommerceConstants.DONATION);
			pRequest.setParameter(TRUCommerceConstants.DONATION_SKU_ITEM, donationSKUItem);
				final Set<RepositoryItem> parentProducts = getCatalogTools().getParentProducts(donationSKUItem);
				if(parentProducts!= null && !parentProducts.isEmpty()){
					final List<RepositoryItem> parentProductsList = new ArrayList<RepositoryItem>(parentProducts);
					pRequest.setParameter(TRUCommerceConstants.DONATION_PRODUCT_ITEM, parentProductsList.get(TRUCommerceConstants.INT_ZERO));
				}
		}
		pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("@Class::TRUGetDonationSKUILookupDroplet::@method::service() : END");
		}
	}
}
