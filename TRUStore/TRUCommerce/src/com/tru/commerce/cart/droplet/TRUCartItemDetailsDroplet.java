/**
 * 
 */
package com.tru.commerce.cart.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.order.Order;
import atg.nucleus.naming.ComponentName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.cart.vo.TRUCartItemDetailVO;
import com.tru.commerce.cart.vo.TRUOutOfStockSkuItem;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.common.TRUConstants;
import com.tru.common.TRUUserSession;
import com.tru.common.cml.SystemException;


/**
 * This class is used to get all the items from the order.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUCartItemDetailsDroplet extends DynamoServlet {

	/** property to Hold shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;
	
	/** property to Hold mEnableOutOfStock. */
	private boolean mEnableOutOfStock;
	/** The Constant TRU_SESSION_SCOPE_COMPONENT_NAME. */
	public static final ComponentName TRU_SESSION_SCOPE_COMPONENT_NAME = ComponentName
			.getComponentName(TRUCommerceConstants.TRU_SESSION_SCOPE_COMPONENT_NAME);

	/**
	 * This method is used to get the current order as input and iterates all the commerce items present in the current
	 * order and send the items in list of shopping cart vo's.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUCartItemDetailsDroplet.service :: START");
		}
		String fromOverlay = null;
		boolean isAPIRequest = false;
		Object isCSCRequestObj = null;
		boolean isCSCRequest = false;
		if (pRequest.getContextPath().equals(TRUCommerceConstants.REST_CONTEXT_PATH)) {
			isAPIRequest = true;
		}
		isCSCRequestObj = pRequest.getLocalParameter(TRUConstants.IS_CSC_REQUEST);
		if(isCSCRequestObj != null && isCSCRequestObj.equals(TRUCommerceConstants.TRUE_FLAG)){
			isCSCRequest = true;
		}
		final Order order = (Order) pRequest.getObjectParameter(TRUCommerceConstants.PARAM_ORDER);
		TRUUserSession sessionComponent = (TRUUserSession) pRequest.resolveName(TRU_SESSION_SCOPE_COMPONENT_NAME);
		if (null != order && null != sessionComponent && null != sessionComponent.getDeletedSkuList()) {
		final Set<String> deletedSkuList = (Set<String>) sessionComponent.getDeletedSkuList();
			if (!deletedSkuList.isEmpty()) {
				pRequest.setParameter(TRUCommerceConstants.ITEMS_REMOVED_INFO_MESSAGE, getShoppingCartUtils().getTRUCartModifierHelper()
						.getRemovedItemMessages(deletedSkuList));
				sessionComponent.setDeletedSkuList(null);
			}
		}
		if (null != order && null != sessionComponent && null != sessionComponent.getQtyAdjustedSkuList()
				&& !((List<String>) sessionComponent.getQtyAdjustedSkuList()).isEmpty()) {
			pRequest.setParameter(TRUCommerceConstants.ITEMS_QTY_ADJUSTED_INFO_MESSAGE,
					(List<String>) sessionComponent.getQtyAdjustedSkuList());
			sessionComponent.setQtyAdjustedSkuList(null);

		}
		if (null != pRequest.getObjectParameter(TRUCommerceConstants.FROM_OVERLAY)) {
			fromOverlay = (String) pRequest.getObjectParameter(TRUCommerceConstants.FROM_OVERLAY);
		}
		if (order != null
				&& (!order.getCommerceItems().isEmpty() || (null != ((TRUOrderImpl) order).getOutOfStockItems() && !((TRUOrderImpl) order)
						.getOutOfStockItems().isEmpty()))) {
			
				List<TRUCartItemDetailVO> cartItem = getShoppingCartUtils().getItemDetailVOsByRelationShips(order, fromOverlay,
						isAPIRequest, getShoppingCartUtils().getLocationIdFromCookie(pRequest), isCSCRequest);
				pRequest.setParameter(TRUCommerceConstants.ITEMS, cartItem);
				if ((isAPIRequest && isEnableOutOfStock()) && getOutOfStockItems(cartItem) != null) {
					pRequest.setParameter(TRUCommerceConstants.OUTOFSTOCKINFO, getOutOfStockItems(cartItem));
			}
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
			if (isLoggingDebug()) {
				vlogDebug("TRUCartItemDetailsDroplet.service :: ");
			}
		} else {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		}

		if (isLoggingDebug()) {
			logDebug("Exiting  method TRUCartItemDetailsDroplet.service :: END");
		}
	}
	
	/**
	 * This method will add all the outofStockItems
	 * @param pCartItem - CartItem
	 * @return outOfStockSkuItem 
	 */
		public TRUOutOfStockSkuItem getOutOfStockItems(List<TRUCartItemDetailVO> pCartItem) {
			if (isLoggingDebug()) {
				logDebug("Entering into method TRUCartItemDetailsDroplet.getOutOfStockItems :: START");
			}
			List<String> skuIds = new ArrayList<String>();
			String infoMessage = null;
			TRUOutOfStockSkuItem outOfStockSkuItem = null;
			for (TRUCartItemDetailVO cartItems : pCartItem) {
				if (cartItems.getInventoryStatus().equalsIgnoreCase(TRUCommerceConstants.OUT_OF_STOCK)) {
					skuIds.add(cartItems.getSkuId());
				}
			}
			if (skuIds != null && !skuIds.isEmpty()) {
				outOfStockSkuItem = new TRUOutOfStockSkuItem();
				outOfStockSkuItem.setSkuIdList(skuIds);
				try {
					infoMessage = getShoppingCartUtils().getErrorHandlerManager().getErrorMessage(TRUCommerceConstants.TRU_SHOPPINGCART_NO_INVENTORY_INFO_MESSAGE_KEY);
					outOfStockSkuItem.setInventoryInfoMessage(infoMessage);
				} catch (SystemException e) {
					if (isLoggingError()) {
						logError("SystemException in TRUCartItemDetailsDroplet.getOutOfStockItems", e);
					}
				}
			}
			if (isLoggingDebug()) {
				logDebug("Exiting  method TRUCartItemDetailsDroplet.getOutOfStockItems :: END");
			}
			return outOfStockSkuItem;
		}
	/**
	 * Gets the shopping cart utils.
	 * 
	 * @return the shopping cart utils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * Sets the shopping cart utils.
	 * 
	 * @param pShoppingCartUtils
	 *            the new shopping cart utils
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		this.mShoppingCartUtils = pShoppingCartUtils;
	}

	/**
	 * @return the mEnableOutOfStock
	 */
	public boolean isEnableOutOfStock() {
		return mEnableOutOfStock;
	}

	/**
	 * @param pEnableOutOfStock the mEnableOutOfStock to set
	 */
	public void setEnableOutOfStock(boolean pEnableOutOfStock) {
		this.mEnableOutOfStock = pEnableOutOfStock;
	}
	
}
