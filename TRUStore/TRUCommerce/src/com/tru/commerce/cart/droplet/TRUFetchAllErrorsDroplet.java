package com.tru.commerce.cart.droplet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;

import atg.json.JSONException;
import atg.json.JSONObject;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.tol.DefaultResourceBundleTools;
import com.tru.storelocator.cml.StoreLocatorConstants;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * Droplet fetches all the errors from the repository.
 * @author PA
 * @version 1.0
 */
public class TRUFetchAllErrorsDroplet extends DynamoServlet {
	/**
	 * holds ErrorHandlerTools object .
	 */
	private DefaultResourceBundleTools mErrorHandlerTools;
	
	/** The Configuration.mPropertyManager. */
	private TRUPropertyManager mPropertyManager;

	/**
	 * @return the errorHandlerTools
	 */
	public DefaultResourceBundleTools getErrorHandlerTools() {
		return mErrorHandlerTools;
	}

	/**
	 * Holds the property value of radialInvalidCardErrorCodes.
	 */
	private Map<String, String> mRadialInvalidCardErrorCodes;
	
	/**
	 * @return the radialInvalidCardErrorCodes
	 */
	public Map<String, String> getRadialInvalidCardErrorCodes() {
		return mRadialInvalidCardErrorCodes;
	}

	/**
	 * @param pRadialInvalidCardErrorCodes the radialInvalidCardErrorCodes to set
	 */
	public void setRadialInvalidCardErrorCodes(
			Map<String, String> pRadialInvalidCardErrorCodes) {
		mRadialInvalidCardErrorCodes = pRadialInvalidCardErrorCodes;
	}

	/**
	 * @param pErrorHandlerTools
	 *            the errorHandlerTools to set
	 */
	public void setErrorHandlerTools(
			DefaultResourceBundleTools pErrorHandlerTools) {
		mErrorHandlerTools = pErrorHandlerTools;
	}
	
	/**
	 * @return the mPropertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager the mPropertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		this.mPropertyManager = pPropertyManager;
	}

	/**
	 * Method fetches all the error keys and populates in the JSON object.
	 * @param pReq - pReq
	 * @param pRes - pRes
	 * @throws IOException - IOException.
	 * @throws ServletException - ServletException
	 */
	public void service(DynamoHttpServletRequest pReq,DynamoHttpServletResponse pRes) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("TRUFetchAllErrorsDroplet.service method :: START");
		}
		try {
			DefaultResourceBundleTools errTools = getErrorHandlerTools();

			RepositoryItemDescriptor resourceBundleDesc = errTools.getRepository().getItemDescriptor(errTools.getPropertyManager()
									.getRepositoryItemDescPropertyName());
			RepositoryView bundleRepoView = resourceBundleDesc.getRepositoryView();
			QueryBuilder queryBuilder = bundleRepoView.getQueryBuilder();
			Query resourceBundleQuery = queryBuilder.createUnconstrainedQuery();
			RepositoryItem[] errors = bundleRepoView.executeQuery(resourceBundleQuery);
			Map radialInvalidCardErrorCodes = getRadialInvalidCardErrorCodes();

			JSONObject obj = new JSONObject();

			for (RepositoryItem item : errors) {
				obj.put(item.getPropertyValue(getPropertyManager().getErrorKeyPropertyName()).toString(),item.getPropertyValue(getPropertyManager().getDefaultErrorMessagePropertyName()));
			}
			if(!radialInvalidCardErrorCodes.isEmpty()){
				for(Object radialInvalidCardErrorKey :radialInvalidCardErrorCodes.keySet()){
					obj.put(radialInvalidCardErrorKey.toString(),radialInvalidCardErrorCodes.get(radialInvalidCardErrorKey));
				}
			}
			pRes.setContentType(StoreLocatorConstants.APPLICATIONORJSON);

			PrintWriter out = pRes.getWriter();
			out.print(obj.toString());
			out.flush();

			if (isLoggingDebug()) {
				vlogDebug(" ErrorKeys :{0}", obj.toString());
			}
		} catch (JSONException jsonExp) {
			vlogError("TRUFetchAllErrorsDroplet.service Exception :{0}", jsonExp);
		} catch (RepositoryException repExp) {
			vlogError("TRUFetchAllErrorsDroplet.service Exception :{0}", repExp);
		}
		if (isLoggingDebug()) {
			logDebug("TRUFetchAllErrorsDroplet.service method :: End");
		}
	}
}
