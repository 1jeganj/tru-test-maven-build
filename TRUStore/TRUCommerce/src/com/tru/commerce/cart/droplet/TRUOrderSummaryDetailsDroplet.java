package com.tru.commerce.cart.droplet;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.ServletException;

import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUChannelInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.common.TRUConstants;

/**
 * The Class TRUOrderSummaryDetailsDroplet.
 *
 * @author Professional Access.
 * @version 1.0
 */
public class TRUOrderSummaryDetailsDroplet extends DynamoServlet {
	/** property to Hold shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;
	
	/**  property to Hold OrderManager object. */
	private TRUOrderManager mOrderManager;
	
	/**
	 * This method is used to get order summary details for the cart.
	 *
	 * @param pRequest            the request.
	 * @param pResponse            the response.
	 * @throws ServletException             the servlet exception.
	 * @throws IOException             Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderSummaryDetailsDroplet::@method::service() : BEGIN");
		}
		Order order = (Order) pRequest.getObjectParameter(TRUCommerceConstants.PARAM_ORDER);
		Profile profile=(Profile)pRequest.getObjectParameter(TRUCommerceConstants.PROFILE);
		boolean showPaypalCheckOutFlag=Boolean.TRUE;
		boolean serverTime = Boolean.FALSE;
		String serverTimeStamp = null;
		double giftCardAmtApplied = ((TRUOrderTools)(getOrderManager().getOrderTools())).getGiftCardAppliedAmount(order);
		double balanceAmount = TRUConstants.DOUBLE_ZERO;
		String serverTimeFlag = (String) pRequest.getObjectParameter(TRUCommerceConstants.SERVER_TIME_FLAG);
		serverTime = Boolean.parseBoolean(serverTimeFlag);
		if(order.getPriceInfo() != null){
			DecimalFormat df = new DecimalFormat(TRUConstants.DOUBLE_DECIMAL);
			String s_OrderAmount = df.format(order.getPriceInfo().getTotal());
			balanceAmount = Double.parseDouble(s_OrderAmount) - giftCardAmtApplied;
		}
		if(balanceAmount < TRUConstants.DOUBLE_ZERO){
			balanceAmount = TRUConstants.DOUBLE_ZERO;
		}
		if (serverTime) {
			serverTimeStamp = getShoppingCartUtils().getCurrentServerTime();
			if (StringUtils.isNotBlank(serverTimeStamp)) {
				pRequest.setParameter(TRUCommerceConstants.CURRENT_SERVER_TIME, serverTimeStamp);
				pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
			}
		}
		boolean isMaxCouponLimtReached=false;
		if(null!=profile){
			try {
				// Modified for defect TUW-64607
				//isMaxCouponLimtReached=getOrderManager().checkMaxCouponLimit(profile);
				isMaxCouponLimtReached = getOrderManager().checkOrderMaxCouponLimit(order);
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					vlogError("RepositoryException occured TRUOrderSummaryDetailsDroplet.service", e);
				}
			}
		}
		pRequest.setParameter(TRUCommerceConstants.BALANCE_AMOUNT, balanceAmount);
		pRequest.setParameter(TRUCommerceConstants.GIFT_CARD_APPLIED_AMOUNT, giftCardAmtApplied);
		pRequest.setParameter(TRUCommerceConstants.MAX_COUPON_LIMIT, isMaxCouponLimtReached);
		if (order != null && order.getCommerceItems().isEmpty()) {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		} else {
			List<ShippingGroup> sgs = order.getShippingGroups();
			for (ShippingGroup shippingGroup : sgs) {
				if (balanceAmount == TRUConstants.DOUBLE_ZERO || shippingGroup instanceof TRUChannelHardgoodShippingGroup || shippingGroup instanceof TRUChannelInStorePickupShippingGroup ) {
					showPaypalCheckOutFlag = Boolean.FALSE;
					break;
				}
			}
			pRequest.setParameter(TRUCommerceConstants.ITEM_COUNT, ((TRUOrderImpl)order).getTotalItemCount());
			pRequest.setParameter(TRUCommerceConstants.SHOW_PAYPAL_CHECKOUT_FLAG, showPaypalCheckOutFlag);
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		}

		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderSummaryDetailsDroplet::@method::service() : END");
		}
	}
	/**
	 * Gets the shopping cart utils.
	 * 
	 * @return the shopping cart utils.
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * Sets the shopping cart utils.
	 * 
	 * @param pShoppingCartUtils
	 *            the new shopping cart utils.
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		this.mShoppingCartUtils = pShoppingCartUtils;
	}

	/**
	 * Gets the OrderManager.
	 *
	 * @return the OrderManager.
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * Sets the OrderManager.
	 *
	 * @param pOrderManager the new order manager.
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}



}
