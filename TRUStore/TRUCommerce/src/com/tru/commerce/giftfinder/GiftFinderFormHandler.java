package com.tru.commerce.giftfinder;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import atg.core.util.StringUtils;
import atg.droplet.GenericFormHandler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.userprofiling.TRUProfileManager;

/**
 * The Class GiftFinderFormHandler.
 * 
 * @author PA
 * @version 1.0
 *
 */
public class GiftFinderFormHandler extends GenericFormHandler {
	/**
	 * property to hold the mGiftFinderName.
	 */
	private String mGiftFinderName;
	/**
	 * property to hold the mGiftFinderNameURL.
	 */
	private String mGiftFinderNameURL;
	/**
	 * property to hold the mProfileManager.
	 */

	private TRUProfileManager mProfileManager;
	/**
	 * property to hold the mProfile.
	 */
	private Profile mProfile;

	/**
	 * @return the profileManager
	 */
	public TRUProfileManager getProfileManager() {
		return mProfileManager;
	}

	/**
	 * @return the giftFinderName
	 */
	public String getGiftFinderName() {
		return mGiftFinderName;
	}

	/**
	 * @param pGiftFinderName
	 *            the giftFinderName to set
	 */
	public void setGiftFinderName(String pGiftFinderName) {
		mGiftFinderName = pGiftFinderName;
	}

	/**
	 * @return the giftFinderNameURL
	 */
	public String getGiftFinderNameURL() {
		return mGiftFinderNameURL;
	}

	/**
	 * @return the profile
	 */
	public Profile getProfile() {
		return mProfile;
	}

	/**
	 * @param pProfile
	 *            the profile to set
	 */
	public void setProfile(Profile pProfile) {
		mProfile = pProfile;
	}

	/**
	 * @param pGiftFinderNameURL
	 *            the giftFinderNameURL to set
	 */
	public void setGiftFinderNameURL(String pGiftFinderNameURL) {
		mGiftFinderNameURL = pGiftFinderNameURL;
	}

	/**
	 * @param pProfileManager
	 *            the profileManager to set
	 */
	public void setProfileManager(TRUProfileManager pProfileManager) {
		mProfileManager = pProfileManager;
	}

	/**
	 * Handle GiftFinder for the user.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @return true, if successful
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */

	public boolean handleGiftFinder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {

		String count = pRequest
				.getCookieParameter(TRUCheckoutConstants.COUNT_COOKIE);
		boolean createcookieflag = true;

		Cookie[] allCookie = pRequest.getCookies();
		for (Cookie test : allCookie) {
			if (test != null) {
				String as = test.getName();
				String test1 = TRUCheckoutConstants.GIFT_FINDER;
				if (as.contains(test1)) {
					String s2 = pRequest.getCookieParameter(as);
					if (getGiftFinderNameURL().equalsIgnoreCase(s2)) {
						createcookieflag = false;
					}

				}
			}
		}

		Cookie[] cookie = null;

		if (getProfile().isTransient() && createcookieflag) {
			cookie = createGiftFinderCookie(getGiftFinderName(),
					getGiftFinderNameURL(), count);
		}
		if (cookie != null) {

			for (Cookie ck : cookie) {

				if (ck != null) {

					pResponse.addCookie(ck);
				}

			}
		}

		if (!StringUtils.isBlank(getGiftFinderNameURL())&& getProfile() != null) {
			getProfileManager().giftFinder(getGiftFinderName(),
					getGiftFinderNameURL(), getProfile());
		}
		return true;
	}

	/**
	 * create gift finder cookie.
	 * 
	 * @param pName
	 *            the name
	 * @param pUrl
	 *            the url
	 * @param pCount
	 *            the count
	 * @return ck, if successful
	 */
	public Cookie[] createGiftFinderCookie(String pName, String pUrl,
			String pCount)

	{
		String count = pCount;
		String name = pName;
		Cookie giftcountcookie = null;
		Cookie cookie = null;
		StringBuffer cookieNameBuffer = new StringBuffer();
		if (StringUtils.isEmpty(name)) {
			if (count == null) {
				count = TRUCommerceConstants.STRING_ZERO;

			}
			int coockieCount = Integer.parseInt(count.trim());

			coockieCount = ++coockieCount;
			cookieNameBuffer.append(TRUCommerceConstants.GIFT_FINDER);
			cookieNameBuffer.append(TRUCommerceConstants.GT_FINDER);
			cookieNameBuffer.append(coockieCount);
			//name = TRUCommerceConstants.GIFT_FINDER+ TRUCommerceConstants.GT_FINDER + coockieCount;
			name=cookieNameBuffer.toString();
			cookie = new Cookie(name, pUrl);
			giftcountcookie = new Cookie(TRUCheckoutConstants.COUNT_COOKIE, Integer.toString(coockieCount));

		} else {
			cookieNameBuffer.append(TRUCommerceConstants.GIFT_FINDER);
			cookieNameBuffer.append(name);
			//name = TRUCommerceConstants.GIFT_FINDER + name;
			name=cookieNameBuffer.toString();
			cookie = new Cookie(name, pUrl);
		}

		cookie.setMaxAge(Integer.MAX_VALUE);
		cookie.setPath(TRUCommerceConstants.PATH_SEPERATOR);

		Cookie[] ck = new Cookie[TRUCommerceConstants.INT_TWO];

		if (cookie != null) {
			ck[0] = cookie;
		}
		if (giftcountcookie != null) {
			ck[TRUCommerceConstants.INT_ONE] = giftcountcookie;
		}

		return ck;
	}

	/**
	 * Handle Delete giftFinder for the user.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @return true, if successful
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public boolean handleDelete(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {

			if (!getProfile().isTransient()&& !(StringUtils.isBlank(getGiftFinderName()))&& getProfile() != null) {
				getProfileManager().deleteGiftFinder(getGiftFinderName(),
						getProfile());
			}


		if (getProfile().isTransient()) {

			String currentCookie =(String)pRequest.getLocalParameter(TRUCommerceConstants.FINDER_KEY);

			Cookie[] cookies = pRequest.getCookies();
			StringBuffer cookieBuffer = new StringBuffer();
			cookieBuffer.append(TRUCommerceConstants.GIFT_FINDER);
			cookieBuffer.append(currentCookie);
			//currentCookie = TRUCommerceConstants.GIFT_FINDER + currentCookie;
			currentCookie =cookieBuffer.toString();
			for (int i = 0; i < cookies.length; ++i) {
				if (cookies[i].getName().equals(currentCookie)) {

					cookies[i].setValue(TRUCommerceConstants.EMPTY_STRING);
					cookies[i].setPath(TRUCommerceConstants.PATH_SEPERATOR);
					cookies[i].setMaxAge(0);
					pResponse.addCookie(cookies[i]);
					break;
				}
			}

		}
		return true;
	}

	/**
	 * Handle DeleteAll giftFinder for the user.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @return true, if successful
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public boolean handleClearAll(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (getProfile() != null) {
			getProfileManager().clearGiftFinderFromProfile(getProfile());
		}

		if (getProfile().isTransient()) {
			Cookie[] allCookie = pRequest.getCookies();
			for (Cookie test : allCookie) {
				if (test != null) {
					String as = test.getName();
					String test1 = TRUCommerceConstants.GIFT_FINDER;
					if (as.contains(test1)) {
						test.setPath(TRUCheckoutConstants.SLASH_SYMBOL);
						test.setMaxAge(0);
						pResponse.addCookie(test);
					}

				}

			}
		}
		return true;
	}

}
