package com.tru.commerce.email;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jms.JMSException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import atg.adapter.gsa.GSARepository;
import atg.commerce.CommerceException;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.multisite.SiteContextManager;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.cml.SystemException;
import com.tru.common.cml.ValidationExceptions;
import com.tru.errorhandler.fhl.IErrorHandler;
import com.tru.messaging.TRUEpsilonNotifyEmailSource;
import com.tru.messaging.TRUEpslonEmailAFriendSource;
import com.tru.regex.EmailPasswordValidator;
import com.tru.userprofiling.TRUCookieManager;
import com.tru.utils.TRUIntegrationStatusUtil;
import com.tru.validator.fhl.IValidator;
/**
 * This form handler supports the below functionalities.
 *  to send an email to a friend for referring a product.
 *  to help customer for email subscription to subscribe for notify the customer when a product is available.
 *  @author Professional Access
 *  @version 1.0
 */
public class TRUEmailNotificationFormHandler extends GenericFormHandler {
	
	/**
	 * Holds the property value of AT.
	 */  
	private static final String ACKNOWLEDGEMENT= "acknowledgment";
	/**
	 * property to hold the  mCatalogProperty.
	 */
	private TRUCatalogProperties mCatalogProperty;
	
	/**
	 * property to hold the  mRepeatingRequestMonitor.
	 */
	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	/**
	 * property to hold the  mEmailNotificationManager.
	 */
	private TRUEmailNotificationManager mEmailNotificationManager;

	/**
	 * property to hold the mFriendName.
	 */
	private String mFriendName;
	/**
	 * property to hold the mSiteID.
	 */
	private String mSiteID;
	/**
	 * property to hold the mFriendsEmail.
	 */
	private String mFriendsEmail;
	/**
	 * property to hold the mYourName.
	 */
	private String mYourName;
	/**
	 * property to hold the mYourMail.
	 */
	private String mYourMail;
	/**
	 * property to hold the mPersonalMessage.
	 */
	private String mPersonalMessage;
	/**
	 * property to hold the mDisplayName.
	 */
	private String mDisplayName;
	/**
	 * property to hold the mNMWAEmailSentDate.
	 */
	private Date mNMWAEmailSentDate;
	/**
	 * property to hold the mNMWAReqDate.
	 */
	private Date mNMWAReqDate;
	/**
	 * property to hold the mSkuId.
	 */
	private String mSkuId;
	/**
	 * property to hold the mOnlinePID
	 */
	private String mOnlinePID;
	/**
	 * property to hold the mProductId.
	 */
	private String mProductId;
	/**
	 * property to hold the mPdpPageURL.
	 */
	private String mPdpPageURL;
	/**
	 * property to hold the mNotifyMeMailId.
	 */
	private String mNotifyMeMailId;
	/**
	 * property to hold the mHelpURLName.
	 */
	private String mHelpURLName;
	/**
	 * property to hold the mHelpURLValue.
	 */
	private String mHelpURLValue;
	/**
	 * property to hold the mItemDescription.
	 */
	private String mItemDescription;
	/**
	 * property to hold the mSpecialOffer.
	 */
	private boolean mSendSpecialofferToMail;
	/**
	 * property to hold the mEpslonEmailAFriendSource.
	 */
	private TRUEpslonEmailAFriendSource mEpslonEmailAFriendSource;
	/**
	 * property to hold the mEpsilonNotifyEmailSource.
	 */
	private TRUEpsilonNotifyEmailSource mEpsilonNotifyEmailSource;
	/**
	 * Property holds NMWA repository.
	 */

	private Repository mNMWARepository;
	/**
	 * property to hold the mErrorHandler.
	 */
	private IErrorHandler mErrorHandler;
	/**
	 * property to hold the mValidator.
	 */
	private IValidator mValidator;
	/**
	 * property to hold ValidationException.
	 */
	private ValidationExceptions mVe = new ValidationExceptions();

	/**
	 * property to hold emailPasswordValidator.
	 */
	private EmailPasswordValidator mEmailPasswordValidator;
	
	/**
	 * property to hold the mChallengeField.
	 */
	private String mChallengeField;
	
	/**
	 * property to hold the mAcknowledge.
	 */
	private String mAcknowledgeProperty;
	/**
	 * property to hold the mResponseField.
	 */
	private String mResponseField;
	
	/**
	 *  property to hold integrationStatusUtil.
	 */
	private TRUIntegrationStatusUtil mIntegrationStatusUtil;
	/**
	 * property to hold email.
	 */
	private String mEmail;
	/** The store configuration. */
	private TRUConfiguration mStoreConfiguration;
	/**
	 * Holds the TRU profile manager.
	 */
	private TRUCookieManager mCookieManager;
	/**
	 * property to hold mCommonSuccessURL.
	 */
	private String mCommonSuccessURL;
	/**
	 * property to hold mCommonErrorURL.
	 */
	private String mCommonErrorURL;
	/**
	 * Instance variable to hold mRestService.
	 */
	private boolean mRestService;
	

	/**
	 * Property to hold mCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;
	/**
	 * @return the mCatalogTools.
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}
	/**
	 * @param pCatalogTools the mCatalogTools to set.
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		this.mCatalogTools = pCatalogTools;
	}
	
	/**
	 * Gets the challenge field.
	 *
	 * @return the ChallengeField.
	 */
	public String getChallengeField() {
		return mChallengeField;
	}
	
	/**
	 * Sets the challenge field.
	 *
	 * @param pChallengeField the ChallengeField to set.
	 */
	public void setChallengeField(String pChallengeField) {
		mChallengeField = pChallengeField;
	}
	
	/**
	 * Gets the response field.
	 *
	 * @return the ResponseField.
	 */
	public String getResponseField() {
		return mResponseField;
	}
	
	/**
	 * Sets the response field.
	 *
	 * @param pResponseField the ResponseField to set.
	 */
	public void setResponseField(String pResponseField) {
		mResponseField = pResponseField;
	}
	

	/**
	 * Gets the email password validator.
	 *
	 * @return the emailPasswordValidator.
	 */
	public EmailPasswordValidator getEmailPasswordValidator() {
		return mEmailPasswordValidator;
	}

	/**
	 * Sets the email password validator.
	 *
	 * @param pEmailPasswordValidator the emailPasswordValidator to set.
	 */
	public void setEmailPasswordValidator(
			EmailPasswordValidator pEmailPasswordValidator) {
		mEmailPasswordValidator = pEmailPasswordValidator;
	}
	
	/**
	 * Gets the email.
	 *
	 * @return the email.
	 */
	public String getEmail() {
		return mEmail;
	}

	/**
	 * Sets the email.
	 *
	 * @param pEmail            the email to set.
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}

	/**
	 * Gets the help url value.
	 *
	 * @return the helpURLValue.
	 */
	public String getHelpURLValue() {
		return mHelpURLValue;
	}

	/**
	 * Sets the help url value.
	 *
	 * @param pHelpURLValue the helpURLValue to set.
	 */
	public void setHelpURLValue(String pHelpURLValue) {
		mHelpURLValue = pHelpURLValue;
	}

	/**
	 * Gets the item description.
	 *
	 * @return the itemDescription.
	 */
	public String getItemDescription() {
		return mItemDescription;
	}

	/**
	 * Sets the item description.
	 *
	 * @param pItemDescription the itemDescription to set.
	 */
	public void setItemDescription(String pItemDescription) {
		mItemDescription = pItemDescription;
	}

	/**
	 * Gets the repeating request monitor.
	 *
	 * @return the repeatingRequestMonitor.
	 */
	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	/**
	 * Sets the repeating request monitor.
	 *
	 * @param pRepeatingRequestMonitor the repeatingRequestMonitor to set.
	 */
	public void setRepeatingRequestMonitor(
			RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	/**
	 * Gets the store configuration.
	 * 
	 * @return the store configuration.
	 */
	public TRUConfiguration getStoreConfiguration() {
		return mStoreConfiguration;
	}

	/**
	 * Sets the store configuration.
	 * 
	 * @param pStoreConfiguration
	 *            the new store configuration.
	 */
	public void setStoreConfiguration(TRUConfiguration pStoreConfiguration) {
		this.mStoreConfiguration = pStoreConfiguration;
	}

	/**
	 * Gets the friend name.
	 *
	 * @return the friendName.
	 */
	public String getFriendName() {
		return mFriendName;
	}


	/**
	 * Sets the friend name.
	 *
	 * @param pFriendName the friendName to set.
	 */
	public void setFriendName(String pFriendName) {
		mFriendName = pFriendName;
	}

	/**
	 * Gets the friends email.
	 *
	 * @return the friendsEmail.
	 */
	public String getFriendsEmail() {
		return mFriendsEmail;
	}

	/**
	 * Gets the epsilon notify email source.
	 *
	 * @return the epsilonNotifyEmailSource.
	 */
	public TRUEpsilonNotifyEmailSource getEpsilonNotifyEmailSource() {
		return mEpsilonNotifyEmailSource;
	}

	/**
	 * Sets the epsilon notify email source.
	 *
	 * @param pEpsilonNotifyEmailSource the epsilonNotifyEmailSource to set.
	 */
	public void setEpsilonNotifyEmailSource(
			TRUEpsilonNotifyEmailSource pEpsilonNotifyEmailSource) {
		mEpsilonNotifyEmailSource = pEpsilonNotifyEmailSource;
	}

	/**
	 * Gets the notify me mail id.
	 *
	 * @return the notifyMeMailId.
	 */
	public String getNotifyMeMailId() {
		return mNotifyMeMailId;
	}

	/**
	 * Sets the notify me mail id.
	 *
	 * @param pNotifyMeMailId the notifyMeMailId to set.
	 */
	public void setNotifyMeMailId(String pNotifyMeMailId) {
		mNotifyMeMailId = pNotifyMeMailId;
	}

	/**
	 * Checks if is send specialoffer to mail.
	 *
	 * @return the sendSpecialofferToMail.
	 */
	public boolean isSendSpecialofferToMail() {
		return mSendSpecialofferToMail;
	}

	/**
	 * Sets the send specialoffer to mail.
	 *
	 * @param pSendSpecialofferToMail the sendSpecialofferToMail to set.
	 */
	public void setSendSpecialofferToMail(boolean pSendSpecialofferToMail) {
		mSendSpecialofferToMail = pSendSpecialofferToMail;
	}

	/**
	 * Sets the friends email.
	 *
	 * @param pFriendsEmail the friendsEmail to set.
	 */
	public void setFriendsEmail(String pFriendsEmail) {
		mFriendsEmail = pFriendsEmail;
	}

	/**
	 * Gets the your name.
	 *
	 * @return the yourName.
	 */
	public String getYourName() {
		return mYourName;
	}

	/**
	 * Gets the display name.
	 *
	 * @return the displayName.
	 */
	public String getDisplayName() {
		return mDisplayName;
	}

	/**
	 * Gets the product id.
	 *
	 * @return the productId.
	 */
	public String getProductId() {
		return mProductId;
	}

	/**
	 * Sets the product id.
	 *
	 * @param pProductId the productId to set.
	 */
	public void setProductId(String pProductId) {
		mProductId = pProductId;
	}

	/**
	 * Gets the pdp page url.
	 *
	 * @return the pdpPageURL.
	 */
	public String getPdpPageURL() {
		return mPdpPageURL;
	}

	/**
	 * Sets the pdp page url.
	 *
	 * @param pPdpPageURL the pdpPageURL to set.
	 */
	public void setPdpPageURL(String pPdpPageURL) {
		pPdpPageURL=pPdpPageURL.replace("&amp;","&");
		mPdpPageURL = pPdpPageURL;
	}

	/**
	 * Gets the validator.
	 *
	 * @return the validator.
	 */
	public IValidator getValidator() {
		return mValidator;
	}

	/**
	 * Sets the validator.
	 *
	 * @param pValidator the validator to set.
	 */
	public void setValidator(IValidator pValidator) {
		mValidator = pValidator;
	}

	/**
	 * Sets the display name.
	 *
	 * @param pDisplayName the displayName to set.
	 */
	public void setDisplayName(String pDisplayName) {
		mDisplayName = pDisplayName;
	}

	/**
	 * Gets the sku id.
	 *
	 * @return the skuId.
	 */
	public String getSkuId() {
		return mSkuId;
	}
	

	/**
	 * Gets the online id.
	 *
	 * @return the onlinePID.
	 */
	public String getOnlinePID() {
		return mOnlinePID;
	}

	
	/**
	 * Gets the help url name.
	 *
	 * @return the helpURLName.
	 */
	public String getHelpURLName() {
		return mHelpURLName;
	}

	/**
	 * Sets the help url name.
	 *
	 * @param pHelpURLName the helpURLName to set.
	 */
	public void setHelpURLName(String pHelpURLName) {
		mHelpURLName = pHelpURLName;
	}
	
	/**
	 * Sets the onlinePid.
	 *
	 * @param pOnlinePID the onlinePID to set.
	 */
	public void setOnlinePID(String pOnlinePID) {
		mOnlinePID = pOnlinePID;
	}



	/**
	 * Sets the sku id.
	 *
	 * @param pSkuId the skuId to set.
	 */
	public void setSkuId(String pSkuId) {
		mSkuId = pSkuId;
	}

	/**
	 * Sets the your name.
	 *
	 * @param pYourName the yourName to set.
	 */
	public void setYourName(String pYourName) {
		mYourName = pYourName;
	}

	/**
	 * Gets the your mail.
	 *
	 * @return the yourMail.
	 */
	public String getYourMail() {
		return mYourMail;
	}

	/**
	 * Sets the your mail.
	 *
	 * @param pYourMail the yourMail to set.
	 */
	public void setYourMail(String pYourMail) {
		mYourMail = pYourMail;
	}

	/**
	 * Gets the epslon email a friend source.
	 *
	 * @return the epslonEmailAFriendSource.
	 */
	public TRUEpslonEmailAFriendSource getEpslonEmailAFriendSource() {
		return mEpslonEmailAFriendSource;
	}

	/**
	 * Sets the epslon email a friend source.
	 *
	 * @param pEpslonEmailAFriendSource the epslonEmailAFriendSource to set.
	 */
	public void setEpslonEmailAFriendSource(
			TRUEpslonEmailAFriendSource pEpslonEmailAFriendSource) {
		mEpslonEmailAFriendSource = pEpslonEmailAFriendSource;
	}

	/**
	 * Gets the error handler.
	 *
	 * @return the errorHandler.
	 */
	public IErrorHandler getErrorHandler() {
		return mErrorHandler;
	}

	/**
	 * Sets the error handler.
	 *
	 * @param pErrorHandler the errorHandler to set.
	 */
	public void setErrorHandler(IErrorHandler pErrorHandler) {
		mErrorHandler = pErrorHandler;
	}
	
	/**
	 * Gets the email notification manager.
	 *
	 * @return the email notification manager
	 */
	public TRUEmailNotificationManager getEmailNotificationManager() {
		return mEmailNotificationManager;
	}

	/**
	 * Gets the personal message.
	 *
	 * @return the personalMessage.
	 */
	public String getPersonalMessage() {
		return mPersonalMessage;
	}

	/**
	 * Sets the personal message.
	 *
	 * @param pPersonalMessage the personalMessage to set.
	 */
	public void setPersonalMessage(String pPersonalMessage) {
		mPersonalMessage = pPersonalMessage;
	}

	
	/**
	 * Gets the cookie manager.
	 *
	 * @return the cookieManager
	 */
	public TRUCookieManager getCookieManager() {
		return mCookieManager;
	}

	/**
	 * Sets the cookie manager.
	 *
	 * @param pCookieManager the mCookieManager to set.
	 */
	public void setCookieManager(TRUCookieManager pCookieManager) {
		mCookieManager = pCookieManager;
	}

	/**
	 * Gets the NMWA repository.
	 *
	 * @return the nMWARepository.
	 */
	public Repository getNMWARepository() {
		return mNMWARepository;
	}

	/**
	 * Sets the NMWA repository.
	 *
	 * @param pNMWARepository the nMWARepository to set.
	 */
	public void setNMWARepository(Repository pNMWARepository) {
		mNMWARepository = pNMWARepository;
	}

	/**
	 * Gets the site id.
	 *
	 * @return the siteID.
	 */
	public String getSiteID() {
		return mSiteID;
	}

	/**
	 * Sets the site id.
	 *
	 * @param pSiteID the siteID to set.
	 */
	public void setSiteID(String pSiteID) {
		mSiteID = pSiteID;
	}

	/**
	 * Gets the NMWA req date.
	 *
	 * @return the nMWAReqDate.
	 */
	public Date getNMWAReqDate() {
		return mNMWAReqDate;
	}

	/**
	 * Sets the NMWA req date.
	 *
	 * @param pNMWAReqDate the nMWAReqDate to set.
	 */
	public void setNMWAReqDate(Date pNMWAReqDate) {
		mNMWAReqDate = pNMWAReqDate;
	}

	/**
	 * Sets the email notification manager.
	 *
	 * @param pEmailNotificationManager the new email notification manager
	 */
	public void setEmailNotificationManager(
			TRUEmailNotificationManager pEmailNotificationManager) {
		mEmailNotificationManager = pEmailNotificationManager;
	}


	/**
	 * Gets the NMWA email sent date.
	 *
	 * @return the nMWAEmailSentDate.
	 */
	public Date getNMWAEmailSentDate() {
		return mNMWAEmailSentDate;
	}

	/**
	 * Sets the NMWA email sent date.
	 *
	 * @param pNMWAEmailSentDate the nMWAEmailSentDate to set.
	 */
	public void setNMWAEmailSentDate(Date pNMWAEmailSentDate) {
		mNMWAEmailSentDate = pNMWAEmailSentDate;
	}

	/**
	 * Gets the catalog property.
	 *
	 * @return the catalogProperty.
	 */
	public TRUCatalogProperties getCatalogProperty() {
		return mCatalogProperty;
	}

	/**
	 * Sets the catalog property.
	 *
	 * @param pCatalogProperty the catalogProperty to set.
	 */
	public void setCatalogProperty(TRUCatalogProperties pCatalogProperty) {
		mCatalogProperty = pCatalogProperty;
	}
	
	
	
	/**
	 * Checks if is rest service.
	 *
	 * @return the mRestService.
	 */
	public boolean isRestService() {
		return mRestService;
	}
	
	/**
	 * Sets the rest service.
	 *
	 * @param pRestService the mRestService to set.
	 */
	public void setRestService(boolean pRestService) {
		this.mRestService = pRestService;
	}
	
	/**
	 * Gets the common success url.
	 *
	 * @return the commonSuccessURL.
	 */
	public String getCommonSuccessURL() {
		return mCommonSuccessURL;
	}

	/**
	 * Sets the common success url.
	 *
	 * @param pCommonSuccessURL            the commonSuccessURL to set.
	 */
	public void setCommonSuccessURL(String pCommonSuccessURL) {
		this.mCommonSuccessURL = pCommonSuccessURL;
	}

	/**
	 * Gets the common error url.
	 *
	 * @return the commonErrorURL.
	 */
	public String getCommonErrorURL() {
		return mCommonErrorURL;
	}

	/**
	 * Sets the common error url.
	 *
	 * @param pCommonErrorURL            the commonErrorURL to set.
	 */
	public void setCommonErrorURL(String pCommonErrorURL) {
		this.mCommonErrorURL = pCommonErrorURL;
	}


	
	
	
	

	/**
	 * Gets the acknowledge property.
	 *
	 * @return the productURl.
	 */



	/*
	 * public void preSendNewsLetter(DynamoHttpServletRequest pRequest,
	 * DynamoHttpServletResponse pResponse) throws ServletException, IOException
	 * { if (isLoggingDebug()) {
	 * logDebug("TRUEmailNotificationFormHandler.preSendNewsLetter method.."); }
	 * String formName = getValue().get(TRUProfileConstants.FORM_NAME);
	 * setEmail(getValue().get(TRUEmailNotificationConstants.TO_ADDRESS)); try {
	 * getValidator().validate(formName, this); } catch (SystemException
	 * systemException) { if (isLoggingError()) { logError(
	 * "SystemException occured in TRUEmailNotificationFormHandler.preSendNewsLetter(): "
	 * , systemException); } getErrorHandler().processException(systemException,
	 * this); } catch (ValidationExceptions validationExceptions) { if
	 * (isLoggingError()) { logError(
	 * "ValidationExceptions occured in TRUEmailNotificationFormHandler.preSendNewsLetter(): "
	 * , validationExceptions); }
	 * getErrorHandler().processException(validationExceptions, this, formName);
	 * return; } }
	 */
	/**
	 * @return the AcknowledgeProperty.
	 */
	public String getAcknowledgeProperty() {
		return mAcknowledgeProperty;
	}
	
	/**
	 * Sets the acknowledge property.
	 *
	 * @param pAcknowledgeProperty the acknowledgeProperty  to set.
	 */
	public void setAcknowledgeProperty(String pAcknowledgeProperty) {
		this.mAcknowledgeProperty = pAcknowledgeProperty;
	}
	/**
	 * PostSendProductToFriendEmail this method used for creating cookie post subscribing for Email or Cancelling Subscription .
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest instance.
	 * @param pResponse
	 *            - DynamoHttpServletResponse instance.
	 * @throws ServletException
	 *             - if an error occurs while processing the servlet.
	 * @throws IOException
	 *             - if an error occurs while reading and writing the servlet.
	 *             request
	 */
	public void postSendNewsLetter(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		vlogDebug("TRUEmailNotificationFormHandler.postSendNewsLetter method..");
		createSubscriptionCookie(pRequest, pResponse, TRUCommerceConstants.SUBMIT);

	}

	/**
	 * Creates the subscription cookie to prevent user getting subscription pop up second time.
	 * @param pRequest the request.
	 * @param pResponse the response.
	 * @param pSubmit String.
	 */
	public void createSubscriptionCookie(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse, String pSubmit) {
		vlogDebug("Entered TRUEmailNotificationFormHandler.createSubscriptionCookie method..");

		String xpirationDate = null;

		final SimpleDateFormat sdf = new SimpleDateFormat(
				TRUCommerceConstants.DATE_FORMAT1);
		final Calendar c = Calendar.getInstance();
		c.setTime(new Date());

		// Now use today date adding new logic for site level.

		if (TRUCommerceConstants.SUBMIT.equalsIgnoreCase(pSubmit) && SiteContextManager.getCurrentSite() != null && getStoreConfiguration().getLightboxsubmitConfigurableTime()!=null) {
			c.add(Calendar.DATE, (Integer)SiteContextManager.getCurrentSite().getPropertyValue(
					getStoreConfiguration().getLightboxsubmitConfigurableTime()));
			 // Adding store date
			// days

		} else if (TRUCommerceConstants.DO_NOT_SHOW.equalsIgnoreCase(pSubmit) && SiteContextManager.getCurrentSite() != null &&  getStoreConfiguration().getLightboxdonotshowConfigurableTime()!=null) {
			
		  c.add(Calendar.DATE, (Integer)SiteContextManager.getCurrentSite().getPropertyValue(
				  getStoreConfiguration().getLightboxdonotshowConfigurableTime()));
		}

		else if (TRUCommerceConstants.CANCEL.equalsIgnoreCase(pSubmit) && SiteContextManager.getCurrentSite() != null && getStoreConfiguration().getLightboxConfigurableTime()!=null) {
			
		c.add(Calendar.DATE, (Integer)SiteContextManager.getCurrentSite().getPropertyValue(
				getStoreConfiguration().getLightboxConfigurableTime()));
			
		}// Adding store date days

		xpirationDate = sdf.format(c.getTime());

		final Cookie cookie = getCookieManager().createLoginCookie(
				TRUCommerceConstants.LIGHT_BOX_COOKIE_EXPIRATION_DATE,
				xpirationDate);
		if (cookie != null) {
			pResponse.addCookie(cookie);
			getCookieManager().addCookieToSession(pRequest, cookie);
		}

		vlogDebug("Exiting TRUEmailNotificationFormHandler.createSubscriptionCookie method..");
	}

	/**
	 * This method is used to subscribe customer for newsletters.
	 * It also creates the cookie for the current user and prevent any further occurences of the newsletter sign up pop up.
	 * 
	 * @param pRequest
	 *            the request.
	 * @param pResponse
	 *            the respons.
	 * @return true, if successful.
	 * @throws ServletException
	 *             the servlet exception.
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws CommerceException
	 *             the commerce exception.
	 * @throws SystemException
	 *             the system exception.
	 */
	public boolean handleSendNewsLetter(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException, SystemException {
		vlogDebug("BEGIN :: TRUEmailNotificationFormHandler.handleSendNewsLetter method..");

		if (!getFormError()) {
			try {
				// getEmailNotificationManager().sendNewsLetter(getValue());
				postSendNewsLetter(pRequest, pResponse);
			} catch (ServletException serviceException) {
				if(isLoggingError())
				{
					logError("FinancialServicesException occured in handleSendNewsLetter method.. :", serviceException);
				}
			}
		}

		vlogDebug("END:: EmailNotificationFormHandler.handleSendNewsLetter method..");

		return false;

	}

	/**
	 * This method will be used to cancel subscription pop up by the user.
	 * It internally calls the createSubscriptionCookie method which creates a
	 * cookie for the current user using the JSSESION ID and prevent any further
	 * occurences of the pop up.
	 * 
	 * @param pRequest
	 *            the request.
	 * @param pResponse
	 *            the response.
	 * @return the boolean.
	 * @throws ServletException
	 *             if the request could not be handled.
	 * @throws IOException
	 *             signals that an I/O exception has occurred.
	 * @throws SystemException
	 *             if any SystemException exception occurs.
	 */
	public Boolean handleCancelSubscriptionPopUp(
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, SystemException {
		vlogDebug("Entered :: TRUEmailNotificationFormHandler.handleCancelSubscriptionPopUp method..");

		createSubscriptionCookie(pRequest, pResponse, TRUCommerceConstants.CANCEL);

		/*
		 * if (rrm != null) {
		 * rrm.removeRequestEntry("CANCEL_SUBSCRIPTION_POP_UP"); }
		 */

		vlogDebug("Exited :: TRUEmailNotificationFormHandler.handleCancelSubscriptionPopUp method..");
		return false;
	}

	/**
	 * This method is used to do not show subscription popup.
	 * 
	 * @param pRequest
	 *            the request.
	 * @param pResponse
	 *            the respons.
	 * @return true, if successful.
	 * @throws ServletException
	 *             the servlet exception.
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws SystemException
	 *             the system exception.
	 */
	public Boolean handleDonotShowSubscriptionPopUp(
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, SystemException {
		vlogDebug("Entered :: TRUEmailNotificationFormHandler.handleCancelSubscriptionPopUp method..");

		createSubscriptionCookie(pRequest, pResponse, TRUCommerceConstants.DO_NOT_SHOW);

		/*
		 * if (rrm != null) {
		 * rrm.removeRequestEntry("CANCEL_SUBSCRIPTION_POP_UP"); }
		 */

		vlogDebug("Exited :: TRUEmailNotificationFormHandler.handleCancelSubscriptionPopUp method..");
		return false;

	}
	/**
	 * This method will be used to handle EmailToFriend.
	 * 
	 * @param pRequest
	 *            the request.
	 * @param pResponse
	 *            the response.
	 * @return the boolean.
	 * @throws ServletException
	 *             if the request could not be handled.
	 * @throws IOException
	 *             signals that an I/O exception has occurred.
	 */
	public boolean handleEmailToFriend(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Entered :: TRUEmailNotificationFormHandler.handleEmailToFriend method..");
		}
		try {
			getValidator().validate(TRUCommerceConstants.EMAIL_TO_FRIEND, this);
			if (StringUtils.isBlank(getFriendName()) && StringUtils.isBlank(getFriendsEmail()) && StringUtils.isBlank(getYourName())&&
					StringUtils.isBlank(getYourMail()) && StringUtils.isBlank(getPersonalMessage())) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_EMAILTOFRIEND_MISS_INFO, null);
				getErrorHandler().processException(mVe, this);
			} 
			else if ((containsSpecialCharacter(getFriendName())) && (!getEmailPasswordValidator().validateEmail(getFriendsEmail())) 
                    && (containsSpecialCharacter(getYourName())) && (!getEmailPasswordValidator().validateEmail(getYourMail()))) {
             mVe.addValidationError(TRUErrorKeys.TRU_ERROR_EMAILTOFRIEND_INVALID_FORMAT_ALL, null);
             getErrorHandler().processException(mVe, this);
          }

			
			else if (StringUtils.isBlank(getFriendName()) || containsSpecialCharacter(getFriendName())) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_EMAILTOFRIEND_FRIENDNAME, null);
				getErrorHandler().processException(mVe, this);
			}

			else if (StringUtils.isBlank(getFriendsEmail())) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_EMAILTOFRIEND_FRIENDMAIL, null);
				getErrorHandler().processException(mVe, this);

			} else if (!getEmailPasswordValidator().validateEmail(getFriendsEmail())) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_EMAILTOFRIEND_INVALID_MAIL, null);
				getErrorHandler().processException(mVe, this);

			} else if (StringUtils.isBlank(getYourName())) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_EMAILTOFRIEND_YOURNAME, null);
				getErrorHandler().processException(mVe, this);

			} else if (StringUtils.isBlank(getYourMail())) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_EMAILTOFRIEND_YOURMAIL, null);
				getErrorHandler().processException(mVe, this);

			} else if (!getEmailPasswordValidator().validateEmail(getYourMail())) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_EMAILTOFRIEND_INVALID_MAIL, null);
				getErrorHandler().processException(mVe, this);
			} else if (StringUtils.isBlank(getResponseField())) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CAPTCHA_MISSING_INFO, null);
				getErrorHandler().processException(mVe, this);
			} else {
				String remoteAddr = pRequest.getRemoteAddr();
				String response = getResponseField();
				
				boolean valid = getEmailNotificationManager().validateRecaptcha(response, remoteAddr);

				if (isLoggingDebug()) {
					logDebug("Captcha Response:::" + valid);
				}
/*
 *				ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
				reCaptcha.setPrivateKey(getStoreConfiguration().getRecaptchaPrivateKey());
				String challenge = getChallengeField();
				ReCaptchaResponse reCaptchaResponse;
*/				try {
					//reCaptchaResponse = reCaptcha.checkAnswer(remoteAddr, challenge, uresponse);
					
					if (!valid) {
						mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CAPTCHA_INCORRECT_INFO, null);
						getErrorHandler().processException(mVe, this);
					} else {
						try {
							boolean isEpslonEnabled = Boolean.FALSE;
							isEpslonEnabled = getIntegrationStatusUtil().getEpslonIntStatus(pRequest);
							if (isEpslonEnabled) {
								getEpslonEmailAFriendSource().emailToAFriend(getYourName(), getFriendName(), getYourMail(),
										getFriendsEmail(), getSkuId(), getPdpPageURL(), getDisplayName());
							} else {
								if (isLoggingDebug()) {
									logDebug("::: Epslon service is disabled OR service is down :::");
								}
							}

						} catch (JMSException e) {
							if (isLoggingError()) {
								logError("JMSException occured in Epslon services", e);
							}
						}
					}
				} catch (Exception e) {
					if (isLoggingError()) {
						logError("Not able to reach captcha server:", e);
					}
				}

			}

		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			if (isLoggingError()) {
				logError("System exception occured:", se);
			}
		} catch (ValidationExceptions e) {
			getErrorHandler().processException(mVe, this);
			if (isLoggingError()) {
				logError("Validation exception occured:", e);
			}
		}

		if (isLoggingDebug()) {
			logDebug("Exited :: TRUEmailNotificationFormHandler.handleEmailToFriend method..");
		}
		return true;

	}
		
	
	/**
	 * To find name have special character or not.
	 * @param pName name.
	 * @return boolean.
	 */
	public boolean containsSpecialCharacter(String pName) {
		final Pattern p = Pattern.compile(TRUConstants.CHAR_PATTERN, Pattern.CASE_INSENSITIVE);
		final Matcher m = p.matcher(pName);
		return m.find();
	}
	/**
	 * This method will be used to handle NotifyMe mail if the items in outofstock.
	 * 
	 * @param pRequest
	 *            the request.
	 * @param pResponse
	 *            the response.
	 * @return the boolean.
	 * @throws ServletException
	 *             if the request could not be handled.
	 * @throws IOException
	 *             signals that an I/O exception has occurred.
	 */
	public boolean handleNotifyMe(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Entered :: TRUEmailNotificationFormHandler.handleNotifyMe method..");
		}
		/* get current date time with Date() */
		Date currentDate = new Date();

		if (StringUtils.isBlank(getNotifyMeMailId())) {
			isSendSpecialofferToMail();
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_EMAILTOFRIEND_YOURMAIL, null);
			getErrorHandler().processException(mVe, this);
		} else if (!getEmailPasswordValidator().validateEmail(getNotifyMeMailId())) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_EMAILTOFRIEND_INVALID_MAIL, null);
			getErrorHandler().processException(mVe, this);
		} else if (StringUtils.isBlank(getSkuId()) || StringUtils.isBlank(getProductId())) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_NOTIFY_ME_MISSING_INFO, null);
			getErrorHandler().processException(mVe, this);
		} else {
			try {

				final MutableRepositoryItem nmwaRepositoryItem = ((GSARepository) getNMWARepository()).createItem(getCatalogProperty().getNmwaAlertPropertyName());
				nmwaRepositoryItem.setPropertyValue(getCatalogProperty().getEmailAddressPropertyName(), getNotifyMeMailId());
				nmwaRepositoryItem.setPropertyValue(getCatalogProperty().getProductIdPropertyName(), getProductId());
				nmwaRepositoryItem.setPropertyValue(getCatalogProperty().getSkuIdPropertyName(), getSkuId());
				nmwaRepositoryItem.setPropertyValue(getCatalogProperty().getNMWAReqDatePropertyName(), getNMWAReqDate());
				nmwaRepositoryItem.setPropertyValue(getCatalogProperty().getNMWAReqDatePropertyName(), new Timestamp(currentDate.getTime()));
				String onlinePID = getCatalogTools().getOnlinePIDFromSKUId(getSkuId());
				if (isRestService() && StringUtils.isNotBlank(onlinePID)  ) {
					nmwaRepositoryItem.setPropertyValue(getCatalogProperty().getOnlinePID(), onlinePID);
					
				} else {
					if(!StringUtils.isBlank(getOnlinePID()))
					nmwaRepositoryItem.setPropertyValue(getCatalogProperty().getOnlinePID(), getOnlinePID());
					else
					nmwaRepositoryItem.setPropertyValue(getCatalogProperty().getOnlinePID(), onlinePID);	
				}
				((GSARepository) getNMWARepository()).addItem(nmwaRepositoryItem);

				final String notifyEmailFlag = ACKNOWLEDGEMENT;
				if (isLoggingDebug()) {
					logDebug("Begin::EpsilonNotifyEmailSource method calling start..");
				}
				boolean isEpslonEnabled = Boolean.FALSE;
				isEpslonEnabled = getIntegrationStatusUtil().getEpslonIntStatus(pRequest);
				if (isEpslonEnabled) {
					getEpsilonNotifyEmailSource().sendNotificationEmail(getItemDescription(), getDisplayName(), getSkuId(),
							getPdpPageURL(), getHelpURLName(), getHelpURLValue(), getNotifyMeMailId(), notifyEmailFlag);
				} else {
					if (isLoggingDebug()) {
						logDebug("::: Epslon service is disabled OR service is down :::");
					}
				}
				if (isLoggingDebug()) {
					logDebug("END::EpsilonNotifyEmailSource method calling End");
				}

			} catch (JMSException e) {
				if (isLoggingError()) {
					logError("JMSException occured", e);
				}
				addFormException(new DropletException(TRUCommerceConstants.PLEASE_TRY_AGAIN));
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError("RepositoryException occured", e);
				}
				addFormException(new DropletException(TRUCommerceConstants.PLEASE_TRY_AGAIN));
			}
			if (isLoggingDebug()) {
				logDebug("End of handleNotifyMe method");
			}
		}

		if (getFormError()) {
			return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
		}
		if (!getFormError() && isRestService()) {
			return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
		}
		return true;

	}
	
	/**
	 * Gets the integration status util.
	 *
	 * @return the integrationStatusUtil
	 */
	public TRUIntegrationStatusUtil getIntegrationStatusUtil() {
		return mIntegrationStatusUtil;
	}
	
	/**
	 * Sets the integration status util.
	 *
	 * @param pIntegrationStatusUtil the integrationStatusUtil to set
	 */
	public void setIntegrationStatusUtil(TRUIntegrationStatusUtil pIntegrationStatusUtil) {
		mIntegrationStatusUtil = pIntegrationStatusUtil;
	}

}


