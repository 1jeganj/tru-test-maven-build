package com.tru.commerce.email;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.integrations.exception.TRUIntegrationException;
import com.tru.integrations.http.client.TRUHTTPClient;

/**
 * This acts as the buisness layer and supports all functionalities of TRUEmailNotificationFormHandler class.
 * It puts the email requests into the queue of integration layer for all types of email subscription.
 * 
 * @author PA
 * @version 1.0
 */


public class TRUEmailNotificationManager extends GenericService {


	/**
	 * Holds the profile tools.
	 */
	private TRUProfileTools mProfileTools;
	
	/** The store configuration. */
	private TRUConfiguration mStoreConfiguration;
	
	/**
	 * Instance variable to hold mTRUHTTPClient.
	 */
	private TRUHTTPClient mHttpClient;
	
	/**
	 * Gets the http client.
	 *
	 * @return mHttpClient
	 */
	public TRUHTTPClient getHttpClient() {
		return mHttpClient;
	}

	/**
	 * Sets the TRUHTTPclient.
	 *
	 * @param pHttpClient - http client
	 */
	public void setHttpClient(TRUHTTPClient pHttpClient) {
		mHttpClient = pHttpClient;
	}

	
	/**
	 * Gets the store configuration.
	 * 
	 * @return the store configuration.
	 */
	public TRUConfiguration getStoreConfiguration() {
		return mStoreConfiguration;
	}

	/**
	 * Sets the store configuration.
	 * 
	 * @param pStoreConfiguration
	 *            the new store configuration.
	 */
	public void setStoreConfiguration(TRUConfiguration pStoreConfiguration) {
		this.mStoreConfiguration = pStoreConfiguration;
	}


	/**
	 * Gets the profile tools.
	 *
	 * @return the mProfileTools.
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * Sets the profile tools.
	 *
	 * @param pProfileTools the profileTools to set
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	/**
	 * Gets the query string for recaptcha.
	 *
	 * @param pRecaptchaSecretKey the recaptcha secret key
	 * @param pResponseKey the response key
	 * @param pRemoteIp the remote ip
	 * @return the query string for recaptcha
	 */
	private String getQueryStringForRecaptcha(String pRecaptchaSecretKey,String pResponseKey,String pRemoteIp){
		if (isLoggingDebug()) {
			logDebug("Entered :: TRUEmailNotificationManager.getQueryStringForRecaptcha method..");
		}
    	StringBuilder sb = new StringBuilder();
    	sb.append(TRUConstants.SECRET);
    	sb.append(TRUConstants.EQUALS_SYMBOL_STRING);
    	sb.append(pRecaptchaSecretKey);
    	sb.append(TRUConstants.AMPERSAND_STRING);
    	sb.append(TRUConstants.RESPONSE);
    	sb.append(TRUConstants.EQUALS_SYMBOL_STRING);
    	sb.append(pResponseKey);
    	sb.append(TRUConstants.AMPERSAND_STRING);
    	sb.append(TRUConstants.REMOTE_IP);
    	sb.append(TRUConstants.EQUALS_SYMBOL_STRING);
    	sb.append(pRemoteIp);
    	if (isLoggingDebug()) {
			logDebug("Exited :: TRUEmailNotificationManager.getQueryStringForRecaptcha method..");
		}
    	return sb.toString();
	}
	
	/**
	 * Validate recaptcha.
	 *
	 * @param pResponseKey the response key
	 * @param pRemoteIp the remote ip
	 * @return true, if successful
	 */
	public boolean validateRecaptcha(String pResponseKey, String pRemoteIp) {
		if (isLoggingDebug()) {
			logDebug("Entered :: TRUEmailNotificationManager.validateRecaptcha method..");
		}
		String response = null;
		Map<String, String> headers = new HashMap<String, String>();
		headers.put(TRUConstants.ACCEPT,TRUConstants.APPLICATION_FORM_URLENCODED);
		headers.put(TRUConstants.CONTENT_TYPE,TRUConstants.APPLICATION_FORM_URLENCODED);
		String recaptchaSecretKey = getStoreConfiguration()
				.getRecaptchaSecretKey();
		String queryString = getQueryStringForRecaptcha(recaptchaSecretKey,
				pResponseKey, pRemoteIp);

		try {
			response = getHttpClient().executeHttpPost(getStoreConfiguration().getRecaptchaURL(), queryString,headers, false,
					getStoreConfiguration().getRecaptchaTimeOut(),
					getStoreConfiguration().getRecaptchaRequestTimeout(),
					getStoreConfiguration().isRecaptchaProxyRequired());
			if (!StringUtils.isBlank(response)) {
				ObjectMapper mapper = new ObjectMapper();
				Map<String, String> responseDataMap = new HashMap<String, String>();
				responseDataMap = mapper.readValue(response,new TypeReference<Map<String, String>>() {});
				return Boolean.parseBoolean(responseDataMap.get(TRUConstants.REGISTRY_SUCCESS));
			}
		} catch (TRUIntegrationException e) {
			if (isLoggingError()) {
				logError("TRUIntegrationException while sending request for validate captcha",e);
			}
			return false;
		} catch (JsonGenerationException e) {
			if (isLoggingError()) {
				logError("JsonGenerationException while sending request for validate captcha",e);
			}
			return false;
		} catch (JsonMappingException e) {
			if (isLoggingError()) {
				logError("JsonMappingException while sending request for validate captcha",e);
			}
			return false;
		} catch (IOException e) {
			if (isLoggingError()) {
				logError("IOException while sending request for validate captcha",e);
			}
			return false;
		}
		if (isLoggingDebug()) {
			logDebug("Exited :: TRUEmailNotificationManager.validateRecaptcha method..");
		}
		return false;
	}


}