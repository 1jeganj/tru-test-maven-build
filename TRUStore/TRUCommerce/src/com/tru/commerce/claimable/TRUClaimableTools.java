package com.tru.commerce.claimable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.commerce.claimable.ClaimableException;
import atg.commerce.claimable.ClaimableTools;
import atg.commerce.order.Order;
import atg.core.util.StringUtils;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.pricing.TRUPricingModelProperties;
import com.tru.common.TRUConstants;

/**
 * This class is overriding the OOTB ClaimableTools.
 * 
 * This class is used to interact with the Claimable Repository.
 * @author Professional Access.
 * @version 1.0
 * 
 */
public class TRUClaimableTools extends ClaimableTools{
	
	/**
	 * Property to hold DerivedDisplayNamePropertyName.
	 */
	private String mDerivedDisplayNamePropertyName;
	/**
	 * Property to hold mSucnEnabledPropertyName.
	 */
	private String mSucnEnabledPropertyName;
	
	/**  Property to hold single use item descriptor name. */
	private String mSingleUseItemDescriptorName;
	/**
	 * @return the derivedDisplayNamePropertyName.
	 */
	public String getDerivedDisplayNamePropertyName() {
		return mDerivedDisplayNamePropertyName;
	}

	/**
	 * @param pDerivedDisplayNamePropertyName the derivedDisplayNamePropertyName to set.
	 */
	public void setDerivedDisplayNamePropertyName(
			String pDerivedDisplayNamePropertyName) {
		mDerivedDisplayNamePropertyName = pDerivedDisplayNamePropertyName;
	}


	/**
	 * This method is used to get the Coupon repository items associated to the Promotion.
	 * 
	 * @param pAllPromoItem : List - List of Promotion Repository Item.
	 * @return List : List of Coupon Repository Item.
	 */
	public List<RepositoryItem> getAllCouponItemFormPromotion(List<RepositoryItem> pAllPromoItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUClaimableTools  method: getAllCouponItemFormPromotion]");
			vlogDebug("pAllPromoItem:{0}", pAllPromoItem);
		}
		List<RepositoryItem> allCouponItems = null;
		List<RepositoryItem> batchCouponItems = null;
		if(pAllPromoItem == null || pAllPromoItem.isEmpty()){
			return allCouponItems;
		}
		allCouponItems = new ArrayList<RepositoryItem>();
		TRUPricingModelProperties pricingModelProperties = getPricingModelProperties();
		for(RepositoryItem lPromoItem : pAllPromoItem){
			RepositoryItem[] couponsForPromotion;
			try {
				// Calling method to get all the coupon items associated to promotion.
				couponsForPromotion = getCouponsForPromotion(lPromoItem.getRepositoryId());
				if(couponsForPromotion == null || couponsForPromotion.length <= TRUConstants.INT_ZERO){
					continue;
				}
				for(RepositoryItem repoItem : couponsForPromotion){
					if(repoItem.getItemDescriptor() != null && 
							pricingModelProperties.getCouponBatchItemDescName().equals(
									repoItem.getItemDescriptor().getItemDescriptorName())){
						batchCouponItems=getBatchCouponsForPrmotion(repoItem.getRepositoryId());
					}else{
						allCouponItems.add(repoItem);
						continue;
					}
					if(batchCouponItems != null && batchCouponItems.size()>TRUConstants.INT_ZERO){
						allCouponItems.addAll(batchCouponItems);
					}
				}
			} catch (ClaimableException exc) {
				if(isLoggingError()){
					vlogError("ClaimableException: @Method getAllCouponItemFormPromotion in class TRUClaimableTools: {0}" , exc);
				}
			} catch (RepositoryException exc) {
				if(isLoggingError()){
					vlogError("RepositoryException: @Method getAllCouponItemFormPromotion in class TRUClaimableTools: {0}" , exc);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUClaimableTools  method: getAllCouponItemFormPromotion]");
			vlogDebug("pAllPromoItem:{0}", allCouponItems);
		}
		return allCouponItems;
	}
	
	/**
	 * This method is used to get the all coupon id from Coupon repository Item.
	 * 
	 * @param pAllCouponItems : List - List of Coupon Repository Items.
	 * @return List : List of Coupon Id.
	 */
	public List<String> getAllCouponIds(
			List<RepositoryItem> pAllCouponItems) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUClaimableTools  method: getAllCouponIds]");
			vlogDebug("pAllCouponItems:{0}", pAllCouponItems);
		}
		List<String> allCouponIds = null;
		if(pAllCouponItems == null || pAllCouponItems.isEmpty()){
			return allCouponIds;
		}
		allCouponIds = new ArrayList<String>();
		String couponId = null;
		for(RepositoryItem lCouponItem : pAllCouponItems){
			couponId = lCouponItem.getRepositoryId();
			allCouponIds.add(couponId);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUClaimableTools  method: getAllCouponIds]");
			vlogDebug("pAllPromoItem:{0}", allCouponIds);
		}
		return allCouponIds;
	}
	
	/**
	 * This method is to check whether the coupon is sucn enabled.
	 * @param pTenderdCouponCode - Coupon Id.
	 * @return isSucnEnabled - TRUE/FALSE.
	 */
	public boolean checkSucnEnabled(String pTenderdCouponCode) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUClaimableTools  method: checkSucnEnabled]");
			vlogDebug("pTenderdCouponCode:{0}", pTenderdCouponCode);
		}
		boolean isSucnEnabled = Boolean.FALSE;
		if(StringUtils.isNotBlank(pTenderdCouponCode)){
		RepositoryItem claimableItem = null;
		try {
			claimableItem = getClaimableItem(pTenderdCouponCode);
		} catch (RepositoryException exc) {
			if(isLoggingError()){
				vlogError("RepositoryException : while getting coupon item with Id : {0} and exception is : {1}", pTenderdCouponCode, exc);
			}
		}
		if(claimableItem != null && claimableItem.getPropertyValue(getSucnEnabledPropertyName()) != null){
			isSucnEnabled = (boolean) claimableItem.getPropertyValue(getSucnEnabledPropertyName());
		}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUClaimableTools  method: checkSucnEnabled]");
			vlogDebug("isSucnEnabled:{0}", isSucnEnabled);
		}
		return isSucnEnabled;
	}
	
	/**
	 * Creates the coupon and associate promotion.
	 *
	 * @param pTenderedCouponCode the tendered coupon code
	 * @param pClaimableCoupon the claimable coupon
	 * @throws ClaimableException the claimable exception
	 * @throws RepositoryException the repository exception
	 */
	@SuppressWarnings("unchecked")
	public void createCouponAndAssociatePromotion(String pTenderedCouponCode, RepositoryItem pClaimableCoupon)
			throws ClaimableException, RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUClaimableTools  method: createCouponAndAssociatePromotion]");
			vlogDebug("pTenderdCouponCode:{0},pClaimableCoupon {1}", pTenderedCouponCode,pClaimableCoupon);
		}
		MutableRepositoryItem coupon=null;
		if(null!=pClaimableCoupon){
			Set<RepositoryItem> promotionItems=(Set<RepositoryItem>)pClaimableCoupon.getPropertyValue(getPromotionsPropertyName());
			coupon=(MutableRepositoryItem)createClaimableItem(pTenderedCouponCode, getSingleUseItemDescriptorName(), Boolean.TRUE);
			coupon.setPropertyValue(getDisplayNamePropertyName(), pTenderedCouponCode);
			coupon.setPropertyValue(getPromotionsPropertyName(), promotionItems);
			coupon.setPropertyValue(getMaxUsesPropertyName(), TRUConstants.INTEGER_NUMBER_ONE);
			MutableRepository mutRep=(MutableRepository)getClaimableRepository();
			mutRep.addItem(coupon);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUClaimableTools  method: createCouponAndAssociatePromotion]");
		}
	}
	
	/**
	 * Update coupon uses.
	 *
	 * @param pCurrentCoupon the current coupon
	 * @throws RepositoryException the repository exception
	 */
	public void updateCouponUses(RepositoryItem pCurrentCoupon) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUClaimableTools  method: updateCouponUses]");
			vlogDebug("pCurrentCoupon:{0},", pCurrentCoupon);
		}
		if(null!=pCurrentCoupon.getPropertyValue(getUsesPropertyName())){
			int uses=(int)pCurrentCoupon.getPropertyValue(getUsesPropertyName());
			MutableRepository mutRep=(MutableRepository)getClaimableRepository();
			MutableRepositoryItem couponItem =mutRep.getItemForUpdate(pCurrentCoupon.getRepositoryId(), getCouponItemDescriptorName());
			if(uses>TRUCommerceConstants.ZERO){
				couponItem.setPropertyValue(getUsesPropertyName(),uses-TRUConstants.INTEGER_NUMBER_ONE);
			}
			mutRep.updateItem(couponItem);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUClaimableTools  method: updateCouponUses]");
		}
	}
	
	
	/**
	 * Purge single use coupons.
	 *
	 * @param pDays the days
	 */
	public void purgeSingleUseCoupons(int pDays) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUClaimableTools  method: purgeSingleUseCoupons]");
			vlogDebug("pDays:{0},", pDays);
		}
		Set<String> removableCoupons = new HashSet<String>();
		try {
			Repository claimableRepository = getClaimableRepository();
			RepositoryView repoView = claimableRepository.getView(getClaimableItemDescriptorName());
			QueryBuilder queryBuilder = repoView.getQueryBuilder();
			QueryExpression maxUsesExpression = queryBuilder.createPropertyQueryExpression(getMaxUsesPropertyName());
			QueryExpression maxUses = queryBuilder.createConstantQueryExpression(TRUConstants.INTEGER_NUMBER_ONE);
			Query maxUsesQuery = queryBuilder.createComparisonQuery(maxUsesExpression, maxUses, QueryBuilder.EQUALS);
			QueryExpression usesExpression = queryBuilder.createPropertyQueryExpression(getUsesPropertyName());
			QueryExpression uses = queryBuilder.createConstantQueryExpression(TRUConstants.INTEGER_NUMBER_ONE);
			Query usesQuery = queryBuilder.createComparisonQuery(usesExpression, uses, QueryBuilder.EQUALS);
 		    Query[] andQuery = { maxUsesQuery,usesQuery };
			Query couponQuery = queryBuilder.createAndQuery(andQuery);
			RepositoryItem[] couponItems = repoView.executeQuery(couponQuery);
			if (null != couponItems && couponItems.length > TRUConstants.ZERO) {
				for (RepositoryItem item : couponItems) {
					Date lastModifiedDate = (Date) item.getPropertyValue(getLastModifiedPropertyName());
					Calendar currentDate = Calendar.getInstance();
					if (item.getRepositoryId().length()==TRUConstants.TWENTY&&currentDate.getTimeInMillis() - lastModifiedDate.getTime() > TRUConstants.ZERO) {
						long days = (currentDate.getTimeInMillis() - lastModifiedDate.getTime()) / (TRUConstants.DAYS_IN_MILLISECONDS);
						if (days >= pDays) {
							removableCoupons.add(item.getRepositoryId());
						}
					}
				}
			}
			if (!removableCoupons.isEmpty()) {
				for (String couponId : removableCoupons) {
					((MutableRepository) claimableRepository).removeItem(couponId, getCouponItemDescriptorName());
				}
			}

		} catch (RepositoryException e) {
			if(isLoggingError()){
				vlogError("RepositoryException : while fetching single use coupons", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUClaimableTools  method: purgeSingleUseCoupons]");
		}
	}
	
	/**
	 * Update single use coupon uses.
	 *
	 * @param pOrder the order
	 * @return the list
	 * @throws RepositoryException the repository exception
	 */
	public List<RepositoryItem> updateSingleUseCouponUses(Order pOrder) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUClaimableTools  method: updateSingleUseCouponUses]");
			vlogDebug("pOrder:{0},", pOrder);
		}
		List<RepositoryItem> singleUseCoupons=new ArrayList<RepositoryItem>();
			if(null!=pOrder){
				TRUOrderImpl order=(TRUOrderImpl)pOrder;
				Map<String,String> singleUseCoupon=order.getSingleUseCoupon();
				if(null!=singleUseCoupon&&!singleUseCoupon.isEmpty()){
					for(Map.Entry<String, String> entrySet : singleUseCoupon.entrySet()){
						if(!StringUtils.isBlank(entrySet.getKey())&&entrySet.getKey().length()==TRUConstants.TWENTY){
							RepositoryItem item=getClaimableItem(entrySet.getKey());
							singleUseCoupons.add(item);
							
						if (item != null) {
							synchronized (item) {
								int uses = 0;
								if (null != item.getPropertyValue(getUsesPropertyName())) {
									uses = (int) item.getPropertyValue(getUsesPropertyName());
								}
								MutableRepository mutRep = (MutableRepository) getClaimableRepository();
								MutableRepositoryItem couponItem = mutRep.getItemForUpdate(item.getRepositoryId(),
										getCouponItemDescriptorName());
								couponItem.setPropertyValue(getUsesPropertyName(), uses+TRUConstants.INTEGER_NUMBER_ONE);
								mutRep.updateItem(couponItem);
							}
						}
								
							}
						}
					}
				}
			if (isLoggingDebug()) {
				logDebug("Exit from [Class: TRUClaimableTools  method: updateSingleUseCouponUses]");
			}
	   return singleUseCoupons;
	}
	/**
	 * This method returns the sucnEnabledPropertyName value.
	 *
	 * @return the sucnEnabledPropertyName.
	 */
	public String getSucnEnabledPropertyName() {
		return mSucnEnabledPropertyName;
	}


	/**
	 * This method sets the sucnEnabledPropertyName with parameter value pSucnEnabledPropertyName.
	 *
	 * @param pSucnEnabledPropertyName the sucnEnabledPropertyName to set.
	 */
	public void setSucnEnabledPropertyName(String pSucnEnabledPropertyName) {
		mSucnEnabledPropertyName = pSucnEnabledPropertyName;
	}
	
	/**
	 * This method will return the Coupon associated to Batch Coupon.
	 * @param pRepositoryId - String - Coupon Batch Id.
	 * @return - List - List of Coupon Repository Items.
	 */
	public List<RepositoryItem> getBatchCouponsForPrmotion(String pRepositoryId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUClaimableTools  method: getBatchCouponsForPrmotion]");
			vlogDebug("Coupon Batch repositoryId:{0},", pRepositoryId);
		}
		try {
			TRUPricingModelProperties pricingModelProperties = getPricingModelProperties();
			Repository claimableRepository = getClaimableRepository();
			RepositoryView view = claimableRepository.getView(pricingModelProperties.getBatchPromoClaimItemDescName());
			RqlStatement statement = RqlStatement.parseRqlStatement(getCouponBatchQuery());
			Object params[] = new Object[TRUCheckoutConstants._1];
			params[0] = new String(pRepositoryId);
			RepositoryItem [] items =statement.executeQuery (view, params);
			if(null!=items){
				return Arrays.asList(items);
			}

		} catch (RepositoryException e) {
			if(isLoggingError()){
				vlogError("RepositoryException in TRUClaimableTools", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUClaimableTools  method: getBatchCouponsForPrmotion]");
		}
		return null;
	}
	/**
	 * Gets the single use item descriptor name.
	 *
	 * @return the single use item descriptor name
	 */
	public String getSingleUseItemDescriptorName() {
		return mSingleUseItemDescriptorName;
	}


	/**
	 * Sets the single use item descriptor name.
	 *
	 * @param pSingleUseItemDescriptorName the new single use item descriptor name
	 */
	public void setSingleUseItemDescriptorName(String pSingleUseItemDescriptorName) {
		this.mSingleUseItemDescriptorName = pSingleUseItemDescriptorName;
	}


	
	/**
	 * Property to hold PricingModelProperties.
	 */
	private TRUPricingModelProperties mPricingModelProperties;
	/**
	 * @return the pricingModelProperties
	 */
	public TRUPricingModelProperties getPricingModelProperties() {
		return mPricingModelProperties;
	}

	/**
	 * @param pPricingModelProperties the pricingModelProperties to set
	 */
	public void setPricingModelProperties(TRUPricingModelProperties pPricingModelProperties) {
		mPricingModelProperties = pPricingModelProperties;
	}
	/**
	 * Property to hold CouponBatchQuery.
	 */
	private String mCouponBatchQuery;
	/**
	 * @return the couponBatchQuery
	 */
	public String getCouponBatchQuery() {
		return mCouponBatchQuery;
	}

	/**
	 * @param pCouponBatchQuery the couponBatchQuery to set
	 */
	public void setCouponBatchQuery(String pCouponBatchQuery) {
		mCouponBatchQuery = pCouponBatchQuery;
	}
	
	/**
	 * Get Single Used Coupons.
	 *
	 * @param pOrder the order
	 * @return the list
	 * @throws RepositoryException the repository exception
	 */
	public List<RepositoryItem> getSingleUsedCoupon(Order pOrder) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUClaimableTools  method: getSingleUsedCoupon]");
			vlogDebug("pOrder:{0}", pOrder);
		}
		List<RepositoryItem> singleUseCoupons = null;
		if(pOrder == null){
			return singleUseCoupons;
		}
		TRUOrderImpl order=(TRUOrderImpl)pOrder;
		Map<String,String> singleUseCoupon=order.getSingleUseCoupon();
		if(singleUseCoupon == null || singleUseCoupon.isEmpty()){
			return singleUseCoupons;
		}
		singleUseCoupons = new ArrayList<RepositoryItem>();
		for(Map.Entry<String, String> entrySet : singleUseCoupon.entrySet()){
			if(StringUtils.isNotBlank(entrySet.getKey()) && entrySet.getKey().length()==TRUConstants.TWENTY){
				RepositoryItem item=getClaimableItem(entrySet.getValue());
				singleUseCoupons.add(item);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("singleUseCoupons:{0}", singleUseCoupons);
			logDebug("Exit from [Class: TRUClaimableTools  method: getSingleUsedCoupon]");
		}
	   return singleUseCoupons;
	}
}
