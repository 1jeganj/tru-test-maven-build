package com.tru.commerce.claimable.scheduler;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.commerce.claimable.TRUClaimableTools;
import com.tru.common.TRUConfiguration;

/**
 * The Class TRUPurgeSingleUseCouponScehduler.
 * This class is used to purge the single use coupons after some period of time
 */
public class TRUPurgeSingleUseCouponScehduler extends SingletonSchedulableService {
	/**
	 * Holds reference for mEnable
	 */
	private boolean mEnable;
	
	/** The m claimable tools. */
	private TRUClaimableTools mClaimableTools;
	
	/** The m configuration. */
	private TRUConfiguration mConfiguration;
	
	
	@Override
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {if (isLoggingDebug()) {
		logDebug("TRUPurgeSingleUseCouponScehduler :: doScheduledTask() method :: STARTS ");
	}
	if (isEnable()) {
		purgeSingleUseCoupons();
	}
	if (isLoggingDebug()) {
		logDebug("TRUPurgeSingleUseCouponScehduler :: doScheduledTask() method :: END ");
	}}
	
	/**
	 * Purge single use coupons.
	 */
	public void purgeSingleUseCoupons() {
		int noOfDays=getConfiguration().getDaysToPurgeSingleUseCoupon();
		if(noOfDays>0){
			getClaimableTools().purgeSingleUseCoupons(noOfDays);
		}
	}
	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable
	 *            the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}
	
	/**
	 * Gets the claimable tools.
	 *
	 * @return the claimable tools
	 */
	public TRUClaimableTools getClaimableTools() {
		return mClaimableTools;
	}
	
	/**
	 * Sets the claimable tools.
	 *
	 * @param pClaimableTools the new claimable tools
	 */
	public void setClaimableTools(TRUClaimableTools pClaimableTools) {
		this.mClaimableTools = pClaimableTools;
	}
	
	/**
	 * Gets the configuration.
	 *
	 * @return the configuration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}
	
	/**
	 * Sets the configuration.
	 *
	 * @param pConfiguration the new configuration
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		this.mConfiguration = pConfiguration;
	}
}
