package com.tru.commerce;

import atg.adapter.gsa.query.Clause;
import atg.nucleus.GenericService;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;

import com.tru.common.vo.TRUContentVO;
/**
 * This class is used perform the actual repository operations related to content repository.
 * @author Professional Access
 * @version 1.0
 */
public class TRUContentTools extends GenericService {
	
	/** Property to hold commerce property mCommercePropertyManager. */
	private TRUCommercePropertyManager mCommercePropertyManager;
	
	/**
	 * Gets the commerce property manager.
	 * 
	 * @return the commerce property manager
	 */
	public TRUCommercePropertyManager getCommercePropertyManager() {
		return mCommercePropertyManager;
	}

	/**
	 * Sets the commerce property manager.
	 * 
	 * @param pCommercePropertyManager
	 *            the new commerce property manager
	 */
	public void setCommercePropertyManager(TRUCommercePropertyManager pCommercePropertyManager) {
		mCommercePropertyManager = pCommercePropertyManager;
	}
	
	private Repository mContentRepository;
	
	/**
	 * Gets the content repository.
	 *
	 * @return the mContentRepository
	 */
	public Repository getContentRepository() {
		return mContentRepository;
	}

	/**
	 * Sets the content repository.
	 *
	 * @param pContentRepository the new content repository
	 */
	public void setContentRepository(Repository pContentRepository) {
		this.mContentRepository = pContentRepository;}
	
	/**
	 * This method Fetches the BCC configured static content making use of endeca EM based on the key provided.
	 * @param pArticleName		- article name
	 * @return contentVO		- contentVO
	 */
	public TRUContentVO fetchContent(String pArticleName) {		
		if(isLoggingDebug()){
			logDebug("::: TRUContentTools.fetchContent Start :::");
		}
	    TRUContentVO contentVO = null;
		String articleBody = null;
		String seoTitleText=null;
		String seoMetaDesc=null;
		String seoMetaKeyword=null;
		String seoAltDesc=null;
		RepositoryItem articleItem=null;
		try {
			//do repository call to get the values
			RepositoryView repoView= getContentRepository().getView(getCommercePropertyManager().getArticleItemDescriptorName());
			QueryBuilder queryBuilder = repoView.getQueryBuilder();
			QueryExpression articleKey = queryBuilder.createPropertyQueryExpression(getCommercePropertyManager().getArticleName());
			QueryExpression  articleValue= queryBuilder.createConstantQueryExpression(pArticleName);
            Query artcleQuery=queryBuilder.createComparisonQuery(articleKey, articleValue, QueryBuilder.EQUALS);
	            if(artcleQuery instanceof Clause){
					((Clause) artcleQuery).setContextFilterApplied(true);
				}	
            	//get articleItems
            	RepositoryItem[] articleItems = repoView.executeQuery(artcleQuery); 	
			    if(articleItems != null){
	                articleItem=articleItems[0];
	                articleBody = (String) articleItem.getPropertyValue(getCommercePropertyManager().getArticleBodyPropertyName());
	      			seoTitleText = (String) articleItem.getPropertyValue(getCommercePropertyManager().getSeoTitleTextPropertyName());
	      			seoMetaDesc = (String) articleItem.getPropertyValue(getCommercePropertyManager().getSeoMetaDescPropertyName());
	      			seoMetaKeyword = (String) articleItem.getPropertyValue(getCommercePropertyManager().getSeoMetaKeywordPropertyName());
	      			seoAltDesc = (String) articleItem.getPropertyValue(getCommercePropertyManager().getSeoAltDescPropertyName());
	      			contentVO = new TRUContentVO();
	      			//populate vo object
	      			contentVO.setArticleBody(articleBody);
	      			contentVO.setSeoAltDesc(seoAltDesc);
	      			contentVO.setSeoMetaDesc(seoMetaDesc);
	      			contentVO.setSeoMetaKeyword(seoMetaKeyword);
	      			contentVO.setSeoTitleText(seoTitleText);	      			
	            }			
			} catch (RepositoryException e) {
				if(isLoggingError()){
					logError("TRUContentTools.fetchContent -> getting article item",e);
				}
			}
			if(isLoggingDebug()){
				logDebug("::: TRUContentTools.fetchContent End :::");
			}
			return contentVO ;			  
	 	}
	}
