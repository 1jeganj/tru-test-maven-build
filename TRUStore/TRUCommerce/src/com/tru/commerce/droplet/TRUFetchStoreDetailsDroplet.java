/**
 * 
 */
package com.tru.commerce.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.constants.TRUCheckoutConstants;



/** This class used to fetch the store details.
 * @author PA
 * @version 1.0
 * 
 */
public class TRUFetchStoreDetailsDroplet extends DynamoServlet {

	/** The m location repository. */
	private Repository mLocationRepository;

	/**
	 * Gets the location repository.
	 *
	 * @return the locationRepository
	 */
	public Repository getLocationRepository() {
		return mLocationRepository;
	}

	/**
	 * Sets the location repository.
	 *
	 * @param pLocationRepository the locationRepository to set
	 */
	public void setLocationRepository(Repository pLocationRepository) {
		mLocationRepository = pLocationRepository;
	}
	
	/**
	 * Service.
	 *
	 * @param pReq DynamoHttpServletRequest
	 * @param pRes DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void service(DynamoHttpServletRequest pReq,DynamoHttpServletResponse pRes) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN::TRUFetchStoreDetailsDroplet.service method Begins");
		}
		final String storeOrLocationID = (String)pReq.getLocalParameter(TRUCheckoutConstants.PROP_ID);
		final String itemDescriptorName = (String)pReq.getLocalParameter(TRUCheckoutConstants.PROP_ITEM_DESCRIPTOR);
		final String elementName =  (String)pReq.getLocalParameter(TRUCheckoutConstants.ELEMENT_NAME);
		try {
			final RepositoryItem item = (RepositoryItem)getLocationRepository().getItem(storeOrLocationID, itemDescriptorName);
			if(item != null){
				pReq.setParameter(elementName, item);
				pReq.serviceParameter(TRUCheckoutConstants.OPARAM_OUTPUT, pReq, pRes);
			}else{
				pReq.serviceParameter(TRUCheckoutConstants.OPARAM_EMPTY, pReq, pRes);
			}
		} catch (RepositoryException repExp) {
			if (isLoggingError()) {	
				logError(" RepositoryException :{0}", repExp);	
			}	
		}
		if (isLoggingDebug()) {
			logDebug("END::TRUMyStoreDroplet.service method Ends");
		}
	}
}
