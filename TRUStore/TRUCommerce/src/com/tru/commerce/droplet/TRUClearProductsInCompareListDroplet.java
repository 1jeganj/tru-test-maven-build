package com.tru.commerce.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.comparison.TRUProductComparisonList;

/**
 * This droplet will check if the user has navigated to a different category. 
 * It gets the request parameter(categoryId) and checks against the productList's currentCategoryId. 
 * If the categoryId passed is not same as the currentCategoryId, then it clears the product comparison list 
 * and sets the current category id as the category id passed.
 * If the categoryId passed is null, then it sets the current category Id to the category id passed.
 * 
 * <br>
 * <br>
 * <b>Input Parameters</b><br>
 * &nbsp;&nbsp;&nbsp;<code>productList</code> - ProductComparisonList<br>
 * &nbsp;&nbsp;&nbsp;<code>categoryId</code> - String<br>
 * 
 * <b>Output Parameters</b><br>
 * &nbsp;&nbsp;&nbsp;<b>No output Parameters available</b><br>
 * 
 * <b>Open Parameters</b><br>
 * &nbsp;&nbsp;&nbsp;<b>No open Parameters available</b><br>
 * 
 * <br>
 * <b>Usage:</b><br>
 * &lt;dspel:droplet name="/com/tru/commerce/droplet/TRUClearProductsInCompareListDroplet"&gt;<br>
 * &lt;dspel:param name="categoryId" param="categoryId"/&gt;<br>
 * &lt;dspel:param name="productList" bean="/atg/commerce/catalog/comparison/ProductList" /&gt;<br>
 * &lt;/dspel:droplet&gt;
 * 
 * @author Professional Access.
 * @version 1.0
 */
public class TRUClearProductsInCompareListDroplet extends DynamoServlet {

	/**
	 * This method will check if the user has navigated to a different category. 
	 * It gets the request parameter(categoryId) and checks against the productList.
	 * If thecategoryId passed is not same as the currentCategoryId, then it clears the
	 * product comparison list and sets the current category id as the category id passed.
	 * If the categoryId passed is null, then it sets the current category Id to the category id passed.
	 * 
	 * @param pRequest -  reference to DynamoHttpServletRequest object.
	 * @param pResponse -  reference to DynamoHttpServletResponse object.
	 * @throws ServletException -  if any exception occurs in servicing the request.
	 * @throws IOException - if any exception occurs while writing the response to output stream.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUClearProductsInCompareListDroplet.service method.. ");
		}
		final TRUProductComparisonList productList =
				(TRUProductComparisonList) pRequest.getObjectParameter(TRUCommerceConstants.PRODUCT_LIST);
		final String categoryId = (String) pRequest.getLocalParameter(TRUCommerceConstants.CATEGORY_ID);
		if(isLoggingDebug()) {
			vlogDebug("Product list: {0} category ID : {1}", productList, categoryId);
		}
		if (productList != null) {
			if (!StringUtils.isBlank(categoryId) && !categoryId.equals(productList.getCurrentCategoryId())) {
				productList.clear();
				productList.setCurrentCategoryId(categoryId);
			} else {
				productList.setCurrentCategoryId(categoryId);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUClearProductsInCompareListDroplet.service method.. ");
		}
	}
}
