package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.google.gson.Gson;
import com.tru.cache.TRUProductCache;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogManager;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.catalog.vo.BatteryProductInfoVO;
import com.tru.commerce.catalog.vo.ProductInfoRequestVO;
import com.tru.commerce.catalog.vo.ProductInfoVO;
import com.tru.commerce.catalog.vo.SKUInfoVO;
import com.tru.commerce.exception.TRUCommerceException;
import com.tru.commerce.pricing.TRUPricingModelProperties;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.utils.TRUStoreUtils;


/**
 * This class used to render product related information in PDP page.
 * 
 * @author PA
 * @version 1.0
 * 
 */
public class TRUDisplayProductInfoDroplet extends DynamoServlet {

	/**
	 * Property to hold TRUCatalogManager.
	 */
	private TRUCatalogManager mCatalogManager;

	/**
	 * Property to hold TRUProductCache.
	 */
	private TRUProductCache mProductCache;
	
	/**
	 * This property hold reference of DimensionValueCacheTools.
	 */
	private DimensionValueCacheTools mDimensionValueCacheTools;
	
	/** This holds the reference for TRUPricingTools. */
	private TRUPricingTools mPricingTools;
	
	/**
	 * Holds reference for TRUConfiguration.
	 */
	private TRUConfiguration mTruConfiguration;
	
	/**property to hold promotionTools. */
	private TRUPromotionTools mPromotionTools;
	
	/**property to hold mCatalogProperties. */
	private TRUCatalogProperties mCatalogProperties;
	
	/**
	 * Property to hold TRUCatalogManager.
	 */
	private TRUStoreUtils mStoreUtils;
	
	/**
	 * Gets the store utils.
	 *
	 * @return the storeUtils
	 */
	public TRUStoreUtils getStoreUtils() {
		return mStoreUtils;
	}

	/**
	 * Sets the store utils.
	 *
	 * @param pStoreUtils the storeUtils to set
	 */
	public void setStoreUtils(TRUStoreUtils pStoreUtils) {
		mStoreUtils = pStoreUtils;
	}

	/**
	 * Gets the catalog properties.
	 *
	 * @return the mCatalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the catalog properties.
	 *
	 * @param pCatalogProperties the mCatalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		this.mCatalogProperties = pCatalogProperties;
	}

	/**
	 * Gets the tru configuration.
	 *
	 * @return the mTruConfiguration
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * Sets the tru configuration.
	 *
	 * @param pTruConfiguration the mTruConfiguration to set
	 */
	public void setTruConfiguration(TRUConfiguration pTruConfiguration) {
		this.mTruConfiguration = pTruConfiguration;
	}

	/**
	 * Gets the promotion tools.
	 *
	 * @return the mPromotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * Sets the promotion tools.
	 *
	 * @param pPromotionTools the mPromotionTools to set
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		this.mPromotionTools = pPromotionTools;
	}

	/**
	 * Gets the pricing tools.
	 *
	 * @return the mPricingTools
	 */
	public TRUPricingTools getPricingTools() {
		return mPricingTools;
	}

	/**
	 * Sets the pricing tools.
	 *
	 * @param pPricingTools the mPricingTools to set
	 */
	public void setPricingTools(TRUPricingTools pPricingTools) {
		this.mPricingTools = pPricingTools;
	}

	/**
	 * This method will get the product related information required to be populated into PDP page.
	 * 
	 * @param pRequest - reference to DynamoHttpServletRequest object.
	 * @param pResponse - reference to DynamoHttpServletResponse object.
	 * @throws ServletException - if any exception occurs in servicing the request.
	 * @throws IOException - if any exception occurs while writing the response to output stream.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUDisplayProductInfoDroplet.service method.. ");
		}
		String productIds = (String) pRequest.getLocalParameter(TRUCommerceConstants.PRODUCT_IDS);
		ProductInfoVO productInfo = null;
		Map combineStringMap=new HashMap<>();		
		if(productIds!=null && StringUtils.isNotBlank(productIds)){
		String[] ids=productIds.split(TRUCommerceConstants.COMA_SYMBOL);
		final String cookieValue = pRequest.getCookieParameter(TRUCommerceConstants.FAV_STORE);
		
			for (String productId : ids) {
			// Creating request VO to get the item from the cache object
			final ProductInfoRequestVO requestVO = new ProductInfoRequestVO();
			requestVO.setProductId(productId);
			Map ajaxDetails=new HashMap<>();
			try {
				// Getting the product related information from the cache
				productInfo = (ProductInfoVO) getProductCache().get(requestVO);
		if (isLoggingDebug()) {
			vlogDebug("productId : {0}", productId);
		}
				if (productInfo != null) {
					List<Map> inventoryPriceList=populatePriceAndInventoryInformation(productInfo,cookieValue);
					List<Map> storeAvailableMessageMap=populateStoreAvailabilityMessages(productInfo,cookieValue);
					List promotionDetails=fetchPromotionDetails(productInfo.getProductId());
					if(productInfo.getChildSKUsList()!=null  && !productInfo.getChildSKUsList().isEmpty()){
						List<Map> batteryInventoryPriceList=populateBatteryPriceAndInventoryInformation(productInfo,cookieValue);
						if(batteryInventoryPriceList!=null && !batteryInventoryPriceList.isEmpty()){
							ajaxDetails.put(TRUCommerceConstants.BATTERY_INVENTORY_AND_PRICE_DETAILS, batteryInventoryPriceList);
							}
						}
					ajaxDetails.put(TRUCommerceConstants.INVENTORY_AND_PRICE_DETAILS, inventoryPriceList);
					ajaxDetails.put(TRUCommerceConstants.PROMOTION_DETAILS_PDP, promotionDetails);
					ajaxDetails.put(TRUCommerceConstants.STORE_AVAILABLE_MESSAGES, storeAvailableMessageMap);
					combineStringMap.put(productInfo.getProductId(), ajaxDetails);	
				}
			
			
		} catch (TRUCommerceException commerceException) {
			if (isLoggingError()) {	
				logError("TRUCommerceException in TRUDisplayProductInfoDroplet.service method..", commerceException);
			}
		}
		}
			Gson gson=new Gson();
			String combineStringMapJson=gson.toJson(combineStringMap);
			pRequest.setParameter(TRUCommerceConstants.COMBINE_STRING_MAP_JSON, combineStringMapJson);
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		
		}
		if (productInfo==null ||(productInfo!=null && !productInfo.isActive())) {
			// If product is empty rendering empty output parameter
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUDisplayProductInfoDroplet.service method.." );
		}
	}
	
	/**
	 * Populate store availability messages.
	 *
	 * @param pProductInfo -pProductInfo
	 * @param pCookieValue - pCookieValue
	 * @return availableMessageList - availableMessageList
	 */
	public List<Map> populateStoreAvailabilityMessages(ProductInfoVO pProductInfo, String pCookieValue) {
		vlogDebug("END:: TRUDisplayProductInfoDroplet.populateStoreAvailabilityMessages method..pCookieValue::" + pCookieValue
				+ ", pProductInfo" + pProductInfo);
		final TRUStoreUtils storeUtils = (TRUStoreUtils) getStoreUtils();
		final String storeId = storeUtils.getStoreIdFromCoockie(pCookieValue);
		List<SKUInfoVO> activeChildSkuList = pProductInfo.getChildSKUsList();
		//Site site = SiteContextManager.getCurrentSite();
		List<Map> availableMessageList = new ArrayList<Map>();
		// String productId=pProductInfo.getProductId();
		if (activeChildSkuList != null && !activeChildSkuList.isEmpty()) {
			for (SKUInfoVO skuInfoVO : activeChildSkuList) {
				Map storemessageMap = storeUtils.getStoreAvailableMessage(storeId, skuInfoVO, pProductInfo);
				availableMessageList.add(storemessageMap);
			}
		}
		vlogDebug("END:: TRUDisplayProductInfoDroplet.populateStoreAvailabilityMessages method.. availableMessageList::"
				+ availableMessageList);
		return availableMessageList;
	}

	/**
	 * To fetch the promotion details from.
	 * @param pProductId -pProductId
	 * @return promotionDetails - promotionDetails
	 */
	public List fetchPromotionDetails(String pProductId) {
		final TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		List promotionDetailsList = new ArrayList<>();
		RepositoryItem productItem = null;
		List<String> promos = null;
		int promoCount = TRUConstants.SIZE_ONE;
		String promotionCountinPDP = getTruConfiguration().getPromotionCountinPDP();
		if (!StringUtils.isBlank(promotionCountinPDP)) {
			try {
				promoCount = Integer.parseInt(promotionCountinPDP);
			} catch (NumberFormatException nfe) {
				if (isLoggingError()) {
					vlogError("Provided Promotion Count {0} is not a Integer Value {1}: ", promotionCountinPDP, nfe);
				}
			}
		}

		try {
			productItem = getCatalogManager().getCatalogTools().findProduct(pProductId);
			if (productItem == null) {
				return promotionDetailsList;
			}
			List<RepositoryItem> childSkus = (List<RepositoryItem>) productItem.getPropertyValue(getCatalogProperties()
					.getChildSKUsPropertyName());
			if (childSkus == null || childSkus.isEmpty()) {
				return promotionDetailsList;
			}
			for (RepositoryItem skuItem : childSkus) {
				Map promotionDetails = new HashMap<>();
				promos = (List<String>) skuItem.getPropertyValue(getCatalogProperties().getSkuQualifiedForPromos());
				if (promos != null && !promos.isEmpty()) {
					List<RepositoryItem> list = new ArrayList<RepositoryItem>();
					RepositoryItem promotionItem = null;
					TRUPromotionTools promotionTools = getPromotionTools();
					Repository promoRepository = promotionTools.getPromotions();
					TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPromotionTools()
							.getPricingModelProperties();
					if (promoRepository != null) {
						Date now = getPromotionTools().getCurrentDate().getTimeAsDate();
						for (String lPromoId : promos) {
							try {
								int indexOf = lPromoId.indexOf(TRUCommerceConstants.PIPELINE);
								String promoId = lPromoId.substring(0, indexOf);
								promotionItem = promotionTools.getItemForId(pricingModelProperties.getPromotionItemDescName(), promoId);
								boolean isWebDisplay=false;
								if(promotionItem != null && null!=promotionItem.getPropertyValue(pricingModelProperties.getWebStoreDisplayPropertyName())){
									isWebDisplay=(boolean)promotionItem.getPropertyValue(pricingModelProperties.getWebStoreDisplayPropertyName());
								}
								// checking promotion is expired or not
								if (promotionItem != null && !promotionTools.checkPromotionExpiration(promotionItem, now) && getPromotionTools().checkPromotionStartDate(promotionItem, now) && isWebDisplay) {
									list.add(promotionItem);
								}
							} catch (RepositoryException exc) {
								if (isLoggingError()) {
									vlogError("No Promotion Found for Promotion ID : {0}", lPromoId);
								}
							}
						}
					}
					List<RepositoryItem> promoList = new ArrayList<RepositoryItem>(list);
					Collections.sort(promoList, new Comparator<RepositoryItem>(){
						public int compare(RepositoryItem pPriority1, RepositoryItem pPriority2) {
								Integer p01Item;
								Integer p02Item;
								if((Integer) pPriority1.getPropertyValue(((TRUPricingModelProperties) getPromotionTools().getPricingModelProperties()).getDisplayPriorityPropertyName())!=null){
									p01Item=(Integer) pPriority1.getPropertyValue(((TRUPricingModelProperties) getPromotionTools().getPricingModelProperties()).getDisplayPriorityPropertyName());
								}else{
									p01Item=(Integer) pPriority1.getPropertyValue(((TRUPricingModelProperties) getPromotionTools().getPricingModelProperties()).getPriority());
								}
								if((Integer) pPriority2.getPropertyValue(((TRUPricingModelProperties) getPromotionTools().getPricingModelProperties()).getDisplayPriorityPropertyName())!=null){
									p02Item=(Integer) pPriority2.getPropertyValue(((TRUPricingModelProperties) getPromotionTools().getPricingModelProperties()).getDisplayPriorityPropertyName());
								}else{
									p02Item=(Integer) pPriority2.getPropertyValue(((TRUPricingModelProperties) getPromotionTools().getPricingModelProperties()).getPriority());
								}
								return (p01Item.compareTo(p02Item));
							}
						});
					if (promoList != null && !promoList.isEmpty()) {
						if(promoList.size() > promoCount){
							promoList= promoList.subList(TRUCommerceConstants.INT_ZERO, promoCount-TRUCommerceConstants.INT_ONE);
						}
						int lengthOfList = promoList.size();
						promotionDetails.put(TRUConstants.PROMOTION_COUNT, lengthOfList);
						String promotionName = null;
						List otherPromotionDetailsList = new ArrayList<>();
						for (int i = 0; i < lengthOfList; i++) {
							Map tempMap = new HashMap<>();
							String _Temp = (String) promoList.get(i).getPropertyValue(catalogProperties.getDescription());
							if (_Temp != null) {
								promotionName = (String) promoList.get(i).getPropertyValue(catalogProperties.getDescription());
							} else {
								promotionName = (String) promoList.get(i).getPropertyValue(catalogProperties.getProductDisplayNamePropertyName());
							}
							tempMap.put(TRUCommerceConstants.PROMO_NAME, promotionName);
							tempMap.put(TRUCommerceConstants.PROMO_ID, promoList.get(i).getRepositoryId());
							if (promoList.get(i).getPropertyValue(catalogProperties.getPromotionDetails()) != null) {
								tempMap.put(TRUCommerceConstants.PROMO_DETAILS,
										promoList.get(i).getPropertyValue(catalogProperties.getPromotionDetails()).toString());
							}
							otherPromotionDetailsList.add(tempMap);
						}
						promotionDetails.put(TRUConstants.PROMOTIONS, otherPromotionDetailsList);
						promotionDetails.put(TRUConstants.SKU_ID, skuItem.getRepositoryId());
					}
					promotionDetailsList.add(promotionDetails);
				}
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("TRUCommerceException in TRUDisplayProductInfoDroplet.fetchPromotionDetails method..", e);
			}
		}
		return promotionDetailsList;
	}

	/**
	 * To populate all the inventory and price information.
	 * @param pProductInfo - pProductInfo
	 * @param pCookieValue - pCookieValue
	 * @return skuInventoryAndPriceMap - skuInventoryAndPriceMap
	 */
	public List<Map> populatePriceAndInventoryInformation(ProductInfoVO pProductInfo,String pCookieValue) {
		vlogDebug("BEGIN:: TRUDisplayProductInfoDroplet.populatePriceAndInventoryInformation method.." );
		List<Map> skuInventoryAndPriceMap= new ArrayList<>();
		final TRUStoreUtils storeUtils = new TRUStoreUtils();
		final String storeId = storeUtils.getStoreIdFromCoockie(pCookieValue);
		List<SKUInfoVO> activeChildSkuList= pProductInfo.getChildSKUsList();
		Site site= SiteContextManager.getCurrentSite();
		
		String productId=pProductInfo.getProductId();
		if(activeChildSkuList!=null && !activeChildSkuList.isEmpty()){
			for (SKUInfoVO skuInfoVO : activeChildSkuList) {
				Map inventoryAndPricemap=new HashMap<>();
				String skuId=skuInfoVO.getId();
				inventoryAndPricemap.put(TRUCommerceConstants.SKU_ID_PDP, skuId);
				double listPrice = getPricingTools().getListPriceForSKU(skuId, productId, site);
				double salePrice = getPricingTools().getSalePriceForSKU(skuId, productId, site);
				vlogDebug("List Price for SKU : {0} is : {1}", skuId, listPrice);
				vlogDebug("Sale Price for SKU : {0} is : {1}", skuId, salePrice);
				if(listPrice != TRUConstants.DOUBLE_ZERO && salePrice != TRUConstants.DOUBLE_ZERO){
					inventoryAndPricemap.put(TRUCommerceConstants.LIST_PRICE, listPrice);
					inventoryAndPricemap.put(TRUCommerceConstants.SALE_PRICE, salePrice);
					boolean calculateSavings = getPricingTools().validateThresholdPrices(site, listPrice, salePrice);
					if (calculateSavings) {
						Map<String, Double> savingsMap = getPricingTools()
								.calculateSavings(listPrice, salePrice);
						if (savingsMap != null) {
							inventoryAndPricemap.put(TRUConstants.SAVED_AMOUNT, savingsMap.get(TRUConstants.SAVED_AMOUNT));
							inventoryAndPricemap.put(TRUConstants.SAVED_PERCENTAGE, savingsMap.get(TRUConstants.SAVED_PERCENTAGE));
							vlogDebug("saving amount for SKU : {0} is : {1}", skuId, savingsMap.get(TRUConstants.SAVED_AMOUNT));
							vlogDebug("saving percentage for SKU : {0} is : {1}", skuId, savingsMap.get(TRUConstants.SAVED_PERCENTAGE));
						}

					}
			}
				inventoryAndPricemap=getCatalogManager().getCatalogTools().populateInventory(skuId,storeId, inventoryAndPricemap);
				skuInventoryAndPriceMap.add(inventoryAndPricemap);
		}
	 }
		vlogDebug("END:: TRUDisplayProductInfoDroplet.populatePriceAndInventoryInformation method.." );		
		return skuInventoryAndPriceMap;
	}
	
	/**
	 * To populate all the battery inventory and price information.
	 * @param pProductInfo - pProductInfo
	 * @param pCookieValue - pCookieValue
	 * @return skuInventoryAndPriceMap - skuInventoryAndPriceMap
	 */
	public List<Map> populateBatteryPriceAndInventoryInformation(ProductInfoVO pProductInfo,String pCookieValue) {
		vlogDebug("BEGIN:: TRUDisplayProductInfoDroplet.populateBatteryPriceAndInventoryInformation method.." );
		List<Map> batterySkuInventoryAndPriceMap= new ArrayList<>();
		final TRUStoreUtils storeUtils = new TRUStoreUtils();
		final String storeId = storeUtils.getStoreIdFromCoockie(pCookieValue);
		List<SKUInfoVO> skuInfoVOs= pProductInfo.getChildSKUsList();
		if(skuInfoVOs!=null && !skuInfoVOs.isEmpty()){
			for(SKUInfoVO skuInfoVO: skuInfoVOs){
				Site site= SiteContextManager.getCurrentSite();
				String productId=pProductInfo.getProductId();
				List<BatteryProductInfoVO>batterySkuList=skuInfoVO.getBatteryProducts();
				if(batterySkuList!=null && !batterySkuList.isEmpty()){
					for (BatteryProductInfoVO batteryInfoVO : batterySkuList) {
						Map inventoryAndPricemap=new HashMap<>();
						String skuId=batteryInfoVO.getSkuId();
						inventoryAndPricemap.put(TRUCommerceConstants.SKU_ID_PDP, skuId);
						double listPrice = getPricingTools().getListPriceForSKU(skuId, productId, site);
						double salePrice = getPricingTools().getSalePriceForSKU(skuId, productId, site);
						vlogDebug("Battery List Price for SKU : {0} is : {1}", skuId, listPrice);
						vlogDebug("Battery Sale Price for SKU : {0} is : {1}", skuId, salePrice);
						if(listPrice != TRUConstants.DOUBLE_ZERO && salePrice != TRUConstants.DOUBLE_ZERO){
							inventoryAndPricemap.put(TRUCommerceConstants.LIST_PRICE, listPrice);
							inventoryAndPricemap.put(TRUCommerceConstants.SALE_PRICE, salePrice);
						}
						inventoryAndPricemap=getCatalogManager().getCatalogTools().populateInventory(skuId,storeId, inventoryAndPricemap);
						batterySkuInventoryAndPriceMap.add(inventoryAndPricemap);
				}
			 }
			}
		}
		vlogDebug("END:: TRUDisplayProductInfoDroplet.populateBatteryPriceAndInventoryInformation method.." );		
		return batterySkuInventoryAndPriceMap;
	}


	/**
	 * Gets the productCache.
	 * 
	 * @return the productCache
	 */
	public TRUProductCache getProductCache() {
		return mProductCache;
	}

	/**
	 * Sets the productCache.
	 * 
	 * @param pProductCache the productCache to set
	 */
	public void setProductCache(TRUProductCache pProductCache) {
		mProductCache = pProductCache;
	}
	
	/**
	 * Gets the dimensionValueCacheTools.
	 * 
	 * @return the dimensionValueCacheTools
	 */
	public DimensionValueCacheTools getDimensionValueCacheTools() {
		return mDimensionValueCacheTools;
	}

	/**
	 * Sets the dimensionValueCacheTools.
	 * 
	 * @param pDimensionValueCacheTools the dimensionValueCacheTools to set
	 */
	public void setDimensionValueCacheTools(DimensionValueCacheTools pDimensionValueCacheTools) {
		mDimensionValueCacheTools = pDimensionValueCacheTools;
	}
	
	/**
	 * Gets the catalogManager.
	 * 
	 * @return the catalogManager
	 */
	public TRUCatalogManager getCatalogManager() {
		return mCatalogManager;
	}

	/**
	 * Sets the catalogManager.
	 * 
	 * @param pCatalogManager the catalogManager to set
	 */
	public void setCatalogManager(TRUCatalogManager pCatalogManager) {
		mCatalogManager = pCatalogManager;
	}
}
