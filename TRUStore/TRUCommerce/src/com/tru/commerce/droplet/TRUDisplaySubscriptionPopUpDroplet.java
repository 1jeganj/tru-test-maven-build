package com.tru.commerce.droplet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;

/**
 * The class TRUDisplaySubscriptionPopUpDroplet.
 * This droplet is used to check if subscription pop up needs to be displayed into the home page based on if the 
 * Subscription Cookie is present in the browser.
 * 
 *         <b>Output Parameters</b><br>
 *         &nbsp;&nbsp;&nbsp;<code>element</code> - Boolean<br>
 * 
 *         <b>Open Parameters</b><br>
 *         &nbsp;&nbsp;&nbsp;<b>output - If static Content is found in the
 *         repository.</b>
 * 
 * <br>
 *         <b>Usage:</b><br>
 *         &lt;dspel:droplet
 *         name="/com/tfg/commerce/droplet/TRUDisplaySubscriptionPopUpDroplet"
 *         &gt;<br>
 *         &lt;dspel:oparam name="output"&gt;<br>
 *         &lt;/dspel:oparam&gt;<br>
 *         &lt;/dspel:droplet&gt;
 *
 * @author PA
 * @version 1.0
 */

public class TRUDisplaySubscriptionPopUpDroplet extends DynamoServlet {

	/**
	 * Constant for OUTPUT_OPEN_PARAMETER.
	 */
	public static final ParameterName OUTPUT_OPEN_PARAMETER = ParameterName
			.getParameterName("output");

	/**
	 * This method is used to get the cookies from the browser and checks if it contains subscriptionCookie. 
	 * If it contains the same, it would return true which means user has 
	 * already subscribed to newsletter and no more subscription pop up is required. 
	 * However if the cookie subscriptionCookie is not present, it would 
	 * return false which will display the subscription pop up in the home page.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             if the request could not be handled
	 * @throws IOException
	 *             signals that an I/O exception has occurred.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		final String [] cookiesDescription = pRequest
				.getCookieParameterValues(TRUCommerceConstants.LIGHT_BOX_COOKIE_EXPIRATION_DATE);
		Boolean isSubscriptionPopUpRequired = true;
		vlogDebug("Entered service method of TRUDisplaySubscriptionPopUpDroplet..");

		if (cookiesDescription != null && cookiesDescription.length > 0 && !StringUtils.isBlank(cookiesDescription[0])) {
			final String cookieDescription = cookiesDescription[0];
			vlogDebug("Subscription Cookie value stored in the browser : {0} ",	cookieDescription);
			/*SimpleDateFormat sdf = new SimpleDateFormat(TRUCommerceConstants.DATE_FORMAT1);*/
			final SimpleDateFormat sdf = new SimpleDateFormat(TRUCommerceConstants.DATE_FORMAT1, Locale.US);
			final Calendar c = Calendar.getInstance();
			final String today = sdf.format(c.getTime());
			/*SimpleDateFormat format = new SimpleDateFormat(TRUCommerceConstants.DATE_FORMAT1);*/
			final SimpleDateFormat format = new SimpleDateFormat(TRUCommerceConstants.DATE_FORMAT1,Locale.US);
			Date date1 = null;
			Date date2 = null;
			try {
				date1 = format.parse(cookieDescription);
				date2 = format.parse(today);

				vlogDebug("Subscription Cookie has been created by date........{0}",date1);
				vlogDebug("today date is ..........{0}",date2);
			} catch (ParseException e) {
				vlogError(e,"exception occured in TRUDisplaySubscriptionPopUpDroplet.service() while date parsing");

			}

			if (date1.compareTo(date2) >= 0) {
				vlogDebug("date is compared and value is 0 or +ve...........making isSubscriptionPopUpRequired false");
				isSubscriptionPopUpRequired = false;
			}
			vlogDebug("Subscription Cookie has been created by submitting subscription form..");

		}
		pRequest.setParameter(TRUCommerceConstants.ELEMENT,isSubscriptionPopUpRequired);
		pRequest.serviceParameter(OUTPUT_OPEN_PARAMETER, pRequest, pResponse);
		vlogDebug("Exiting service method of TRUDisplaySubscriptionPopUpDroplet...Subscription Pop up required : {0} ", isSubscriptionPopUpRequired);
	}
}
