package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.repository.RepositoryException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.TRURecentlyViewedTools;
import com.tru.commerce.catalog.vo.CollectionProductInfoVO;
import com.tru.commerce.catalog.vo.ProductInfoVO;
import com.tru.commerce.catalog.vo.SKUInfoVO;
import com.tru.endeca.utils.TRUProductPageUtil;
import com.tru.userprofiling.TRUCookieManager;

/**
 * This droplet is being to used keep track of all the visited product for recently viewed section.
 * 
 * @author Professional Access
 * @version 1.0
 * <br>
 * <br>
 * <b>Input Parameters</b><br>
 * &nbsp;&nbsp;&nbsp;<code>productId</code> - String<br>
 * &nbsp;&nbsp;&nbsp;<code>profile</code> - Profile<br>

 */
public class TRURecentlyViewedHistoryCollectorDroplet extends DynamoServlet {

	/** Property to hold Recently Viewed Tools. */
	private TRURecentlyViewedTools mRecentlyViewedTools;

	/**  Holds the mProductPageUtil. */
	private TRUProductPageUtil mProductPageUtil;

	/** property to hold TRUCookieManager. */
	private TRUCookieManager mCookieManager;

	/** property to hold TRUCatalogTools. */

	private TRUCatalogTools mCatalogTools;


	/**
	 * This method gets the product's, profile details and adds the information into users recently viewed profile
	 * section. It internally calls addRecentlyViewedProduct method of TRURecentlyViewedTools to achieve the same.
	 * 
	 * @param pRequest - Reference to DynamoHttpServletRequest
	 * @param pResponse - Reference to DynamoHttpServletResponse
	 * @throws ServletException - throws ServletException
	 * @throws IOException - throws IOException
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRURecentlyViewedHistoryCollectorDroplet.service method..");
		}

		final String productId = (String) pRequest.getObjectParameter(TRUCommerceConstants.PRODUCT_INFO);
		final Profile profile = (Profile) pRequest.getObjectParameter(TRUCommerceConstants.PROFILE);
		final String productIdsForPromo = (String) pRequest.getObjectParameter(TRUCommerceConstants.PRODUCT_IDS_FOR_PROMO);
		final String onlinePID = (String) pRequest.getObjectParameter(TRUCommerceConstants.ONLINE_PID);
		final String collectionId = (String) pRequest.getLocalParameter(TRUCommerceConstants.COLLECTION_ID);
		ProductInfoVO productVO = null;
		CollectionProductInfoVO collectionProductVO = null;
		String viewedItemsDetails = null;
		try {
			if(productIdsForPromo!= null) {
				if (isLoggingDebug()) {
					logDebug("productIdsListForPromo ::::" + productIdsForPromo);
				}
				List<String> productIdsListForPromo = Arrays.asList(productIdsForPromo.split(TRUCommerceConstants.COMMA));
				final JSONObject json = new JSONObject();
				for (Iterator<String> iterator = productIdsListForPromo.iterator(); iterator.hasNext();) {
					String id = (String) iterator.next();
					ProductInfoVO prodVO = getRecentlyViewedTools().getProductVo(id, onlinePID);
					SKUInfoVO defaultSku = prodVO.getDefaultSKU();
					try {
						if(defaultSku != null){
							getRecentlyViewedTools().setOneRankPromotion(id, json, defaultSku.getId());
						}
					} catch (JSONException jsonException) {
						if (isLoggingError()) {
							logError("JsonException in TRURecentlyViewedHistoryCollectorDroplet.service method..",
									jsonException);
						}
					}
				}
				pRequest.setParameter(TRUCommerceConstants.PROMOTION_DETAILS, json.toString());
				pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);

			} else if(profile != null) {

				if(profile.isTransient()) {
					if(StringUtils.isNotEmpty(collectionId)) {
						collectionProductVO = getRecentlyViewedTools().getCollectionVo(collectionId);
						if(collectionProductVO  != null) {
							viewedItemsDetails = getRecentlyViewedTools().getCollectionViewedItemDetail(collectionProductVO);
						}
					} else if(StringUtils.isNotEmpty(onlinePID) && StringUtils.isNotEmpty(productId)) {
						productVO = getRecentlyViewedTools().getProductVo(productId, onlinePID);
						if(productVO != null){
							viewedItemsDetails = getRecentlyViewedTools().getViewedItemDetail(productVO);
						}
					}
				} else {
					if(StringUtils.isNotEmpty(collectionId)) {
						getRecentlyViewedTools().addRecentlyViewedProduct(profile, collectionId);
					} else if(StringUtils.isNotEmpty(onlinePID) && StringUtils.isNotEmpty(productId)) {
						getRecentlyViewedTools().addRecentlyViewedProduct(profile, onlinePID);  
					}

					List <String> jsonList = getRecentlyViewedTools().getRecentlyViewedItemsList(profile);
					if(jsonList != null && !jsonList.isEmpty()){
						Collections.reverse(jsonList);

						String[] jsonArr = jsonList.toArray(new String[jsonList.size()]);
						viewedItemsDetails = Arrays.toString(jsonArr);
					}
				}
				pRequest.setParameter(TRUCommerceConstants.RECENT_VIEWED_ITEMS_PARAM, viewedItemsDetails);
				pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);

			}

		}
		catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				logError("RepositoryException in TRURecentlyViewedHistoryCollectorDroplet.service method..",
						repositoryException);
			}
		}

		if (isLoggingDebug()) {
			logDebug("END:: TRURecentlyViewedHistoryCollectorDroplet.service method..");
		}
	}


	/**
	 * Gets the recentlyViewedTools.
	 *
	 * @return the recentlyViewedTools
	 */
	public TRURecentlyViewedTools getRecentlyViewedTools() {
		return mRecentlyViewedTools;
	}

	/**
	 * Sets the recentlyViewedTools.
	 *
	 * @param pRecentlyViewedTools the recentlyViewedTools to set
	 */
	public void setRecentlyViewedTools(TRURecentlyViewedTools pRecentlyViewedTools) {
		mRecentlyViewedTools = pRecentlyViewedTools;
	}

	/**
	 * Gets the cookie manager.
	 *
	 * @return the cookie manager
	 */
	public TRUCookieManager getCookieManager() {
		return mCookieManager;
	}

	/**
	 * Sets the cookie manager.
	 *
	 * @param pCookieManager the new cookie manager
	 */
	public void setCookieManager(TRUCookieManager pCookieManager) {
		mCookieManager = pCookieManager;
	}

	/**
	 * Gets the product page util.
	 *
	 * @return the productPageUtil
	 */
	public TRUProductPageUtil getProductPageUtil() {
		return mProductPageUtil;
	}


	/**
	 * Sets the product page util.
	 *
	 * @param pProductPageUtil the productPageUtil to set
	 */
	public void setProductPageUtil(TRUProductPageUtil pProductPageUtil) {
		mProductPageUtil = pProductPageUtil;
	}


	/**
	 * Gets the catalog tools.
	 *
	 * @return the catalog tools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Sets the catalog tools.
	 *
	 * @param pCatalogTools the new catalog tools
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}


}
