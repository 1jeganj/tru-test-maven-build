/**
 * 
 */
package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.google.gson.Gson;
import com.tru.common.TRUConstants;
import com.tru.common.TRUStoreConfiguration;
import com.tru.integrations.registry.beans.Error;
import com.tru.integrations.registry.beans.RegistryResponse;
import com.tru.integrations.registry.beans.Validation;
import com.tru.integrations.registry.exception.RegistryIntegrationException;
import com.tru.integrations.registry.service.RegistryService;
import com.tru.integrations.registry.util.TRURegistryConstant;
import com.tru.logging.TRUAlertLogger;


/**
 * This droplet is used to get PDP wishlist info.
 * @author Professional Access.
 * @version 1.0
 */
public class TRUPDPWishlistInfoDroplet extends DynamoServlet {

	private TRUStoreConfiguration mConfiguration;
	private RegistryService mRegistryService;
	
	/** The mAlertLogger */
	private TRUAlertLogger mAlertLogger;
	/**
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
	
	/**
	 * Property to hold deletedFlag.
	 */
	
	private String mDeletedFlag;

	
	/**
	 * @return the deletedFlag.
	 */
	public String getDeletedFlag() {
		return mDeletedFlag;
	}

	/**
	 * @param pDeletedFlag the deletedFlag to set.
	 */
	public void setDeletedFlag(String pDeletedFlag) {
		mDeletedFlag = pDeletedFlag;
	}
	
	
	/**
	 * @return the mRegistryService.
	 */
	public RegistryService getRegistryService() {
		return mRegistryService;
	}

	/**
	 * @param pRegistryService the mRegistryService to set.
	 */
	public void setRegistryService(RegistryService pRegistryService) {
		this.mRegistryService = pRegistryService;
	}

	/**
	 * @return the mConfiguration.
	 */
	public TRUStoreConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * @param pConfiguration the mConfiguration to set.
	 */
	public void setConfiguration(TRUStoreConfiguration pConfiguration) {
		this.mConfiguration = pConfiguration;
	}

	/**
	 * This method will get the product related information required for resistry.
	 * 
	 * @param pRequest - reference to DynamoHttpServletRequest object.
	 * @param pResponse - reference to DynamoHttpServletResponse object.
	 * @throws ServletException - if any exception occurs in servicing the request.
	 * @throws IOException - if any exception occurs while writing the response to output stream.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUPDPWishlistInfoDroplet.service method.. ");
		}
		String registryNumber = (String) pRequest.getLocalParameter(TRUConstants.REGISTRY_NUMBER);
		String pageName = (String) pRequest.getLocalParameter(TRUConstants.PAGE_NAME);
		if (isLoggingDebug()) {
			logDebug("RegistryNumber: "+registryNumber);
		}
		Gson gson =new Gson();
		Validation validation= new Validation();
		Error error=new Error();
		RegistryResponse registryResponse=null;
		
		if(registryNumber!=null){
			try {
				registryResponse = getRegistryService().getWishlistItems(registryNumber, mDeletedFlag);
			} catch (RegistryIntegrationException e) {
				vlogError("ERROR: TRUPDPWishlistInfoDroplet.service", e);
				if(registryResponse==null){
					registryResponse= new RegistryResponse();
						registryResponse.setRegistryNumber(registryNumber);
						registryResponse.setValidation(validation);
						registryResponse.getValidation().setError(error);
						registryResponse.getValidation().getError().setInformational(TRUConstants.REGISTRY_ERROR);
						Map<String, String> extra = new ConcurrentHashMap<String, String>();
						if(pageName!=null){
						  extra.put(TRUConstants.PAGE_NAME,pageName);
						}
						if(registryResponse.getResponseStatus()!=null){
							extra.put(TRUConstants.STR_REPPONSE_CODE_FOR_LOG_MONITORING,TRUConstants.STR_ERROR_VALUE_FOR_LOG_MONITORING);
						}else{
							extra.put(TRUConstants.STR_REPPONSE_CODE_FOR_LOG_MONITORING,TRUConstants.STR_ERROR_VALUE_FOR_LOG_MONITORING);
						}
						getAlertLogger().logFailure(TRUConstants.WISHLIST_SERVICE, TRUConstants.GET_WISHLIST_DETAILS, extra);
					}
				}
		}
		if(registryResponse != null && registryResponse.getResponseStatus() != null && TRURegistryConstant.RESPONSE_STATUS_SUCCESS.equalsIgnoreCase(registryResponse.getResponseStatus().getIndicator())) {
			Map<String, String> extra = new ConcurrentHashMap<String, String>();
			if(pageName!=null){
			   extra.put(TRUConstants.PAGE_NAME,pageName);
			}
			if(registryResponse.getResponseStatus()!=null){
				extra.put(TRUConstants.STR_REPPONSE_CODE_FOR_LOG_MONITORING,TRUConstants.STR_ERROR_VALUE_FOR_LOG_MONITORING);
			}else{
				extra.put(TRUConstants.STR_REPPONSE_CODE_FOR_LOG_MONITORING,TRUConstants.STR_ERROR_VALUE_FOR_LOG_MONITORING);
			}
			getAlertLogger().logFailure(TRUConstants.WISHLIST_SERVICE, TRUConstants.GET_WISHLIST_DETAILS, extra);
		} 
		String jsonResponse=gson.toJson(registryResponse);
		if (isLoggingDebug()) {
			logDebug("jsonResponse. "+jsonResponse);
		}
		pRequest.setParameter(TRUConstants.REGISTRY_RESPONSE, jsonResponse);
		pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
		
		if (isLoggingDebug()) {
			logDebug("END:: TRUPDPWishlistInfoDroplet.service method.. ");
		}
	}

	

	
}


