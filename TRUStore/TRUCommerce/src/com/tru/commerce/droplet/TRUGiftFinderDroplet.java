package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.Enumeration;
import java.util.LinkedHashMap;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;

/**
 * The class TRUGiftFinderDroplet.
 * This droplet is used to check if subscription pop up needs to be displayed into the home page based 
 * on if the Subscription Cookie is present in the browser.
 * 
 *         <b>Output Parameters</b><br>
 *         &nbsp;&nbsp;&nbsp;<code>element</code> - Boolean<br>
 * 
 *         <b>Open Parameters</b><br>
 *         &nbsp;&nbsp;&nbsp;<b>output - If static Content is found in the
 *         repository.</b>
 * 
 * <br>
 *         <b>Usage:</b><br>
 *         &lt;dspel:droplet
 *         name="/com/tfg/commerce/droplet/TRUDisplaySubscriptionPopUpDroplet"
 *         &gt;<br>
 *         &lt;dspel:oparam name="output"&gt;<br>
 *         &lt;/dspel:oparam&gt;<br>
 *         &lt;/dspel:droplet&gt;
 *         
 *         
 * @author PA
 * @version 1.0
 */

public class TRUGiftFinderDroplet extends DynamoServlet {

	/**
	 * Constant for OUTPUT_OPEN_PARAMETER.
	 */
	public static final ParameterName OUTPUT_OPEN_PARAMETER = ParameterName
			.getParameterName("output");

	/**
	 * This method is used to get the cookies from the browser and checks if it contains subscriptionCookie. 
	 * If it contains the same, it would return true which means user has already subscribed to newsletter
	 * and no more subscription pop up is required. However if the cookie subscriptionCookie is not present, 
	 * it would return false which will display the subscription pop up in the home page.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             if the request could not be handled
	 * @throws IOException
	 *             signals that an I/O exception has occurred.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		final Cookie cookiesDescription[] = pRequest.getCookies();
		final LinkedHashMap giftfinderMap = new LinkedHashMap();
		if (cookiesDescription != null && cookiesDescription.length > 0) {
			final Enumeration names = pRequest.getCookieParameterNames();
			while (names.hasMoreElements()) {
				final String findernamename = (String) names.nextElement();
				if (findernamename.contains(TRUCheckoutConstants.GIFT_FINDER)) {
					final String[] cookiename = findernamename.split(TRUCheckoutConstants.GIFT_FINDER);
					final String[] s1 = pRequest.getCookieParameterValues(findernamename);

					if (s1.length >= TRUCheckoutConstants.INT_ONE && cookiename.length > 0) {
						giftfinderMap.put(cookiename[TRUCheckoutConstants.INT_ONE], s1[s1.length - TRUCheckoutConstants.INT_ONE]);
					}
				}
			}
		}
		pRequest.setParameter(TRUCommerceConstants.ELEMENT, giftfinderMap);
		pRequest.serviceParameter(OUTPUT_OPEN_PARAMETER, pRequest, pResponse);

	}
}
