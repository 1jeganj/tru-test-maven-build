package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.cache.TRUCollectionProductCache;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.vo.CollectionProductInfoRequestVO;
import com.tru.commerce.catalog.vo.CollectionProductInfoVO;
import com.tru.commerce.catalog.vo.ProductInfoVO;
import com.tru.commerce.catalog.vo.SKUInfoVO;
import com.tru.commerce.exception.TRUCommerceException;


/**
 * This class used to render collection product related information in PDP page.
 * 
 * @version 1.0
 * @author Professional Access.
 * 
 */
public class TRUCollectionLineItemInfoLookupDroplet extends DynamoServlet {

	/**
	 * Property to hold TRUCollectionProductCache.
	 */
	private TRUCollectionProductCache mCollectionProductCache;
	
	/**
	 * This property hold reference of DimensionValueCacheTools.
	 */
	private DimensionValueCacheTools mDimensionValueCacheTools;

	/**
	 * This method will get the product related information required to be populated into PDP page.
	 * 
	 * @param pRequest - reference to DynamoHttpServletRequest object.
	 * @param pResponse - reference to DynamoHttpServletResponse object.
	 * @throws ServletException - if any exception occurs in servicing the request.
	 * @throws IOException - if any exception occurs while writing the response to output stream.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCollectionLineItemInfoLookupDroplet.service method.. ");
		}
		Object collectionObj = pRequest.getLocalParameter(TRUCommerceConstants.COLLECTION_ID);
		String collectionId =  null;
		if(collectionObj!=null){
			collectionId =collectionObj.toString();
		}
		Object productIdObj = pRequest.getLocalParameter(TRUCommerceConstants.PRODUCT_ID_1);
		String productId =  null;
		if(productIdObj!=null){
			productId =productIdObj.toString();
		}
		Object skuIdObj = pRequest.getLocalParameter(TRUCommerceConstants.SKU_ID);
		String skuId =  null;
		if(skuIdObj!=null){
			skuId =skuIdObj.toString();
		}
		ProductInfoVO productVo=null;
		if (isLoggingDebug()) {
			vlogDebug("productId : {0}", collectionId);
		}
		//boolean isCollectionProductAvailable = false;
		CollectionProductInfoVO collectionProductInfo = null;
		if (!StringUtils.isBlank(collectionId)) {
			// Creating request VO to get the item from the cache object
			CollectionProductInfoRequestVO requestVO = new CollectionProductInfoRequestVO();
			requestVO.setCollectionId(collectionId);
			
			try {
				// Getting the collection product related information from the cache
				collectionProductInfo = (CollectionProductInfoVO) getCollectionProductCache().get(requestVO);
				if (collectionProductInfo != null && productId!=null) {
					if (isLoggingDebug()) {
						vlogDebug("brand :",collectionProductInfo.getCollectionId());
					}
					productVo=getProductVoFromColectionVo(collectionProductInfo,productId);	
					if(productVo!=null){
						List<SKUInfoVO> activeSkus=productVo.getChildSKUsList();
						if(activeSkus == null || activeSkus.isEmpty()){
							pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
							return;
						}
						for (SKUInfoVO skuVO : activeSkus) {
							if(skuVO.getId().equalsIgnoreCase(skuId)){
								productVo.setDefaultSKU(skuVO);
							}
						}
						
					pRequest.setParameter(TRUCommerceConstants.COLLECTION_LINE_ITEM_INFO, productVo);
					pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
					}else{
						pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
					}
					//isCollectionProductAvailable =true;
				}
			} catch (TRUCommerceException commerceException) {
				if (isLoggingError()) {	
					logError("TRUCommerceException in TRUCollectionLineItemInfoLookupDroplet.service method..", commerceException);
				}
			}
		}
		if (collectionProductInfo==null || productId==null) {
			// If product is empty rendering empty output parameter
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUCollectionLineItemInfoLookupDroplet.service method.. {0}");
		}
	}
	/**
	 * This method will check whether selected product is listed in collection or not.
	 * @param pCollectionProductInfo - CollectionProductInfoVO.
	 * @param pProductId - String.
	 * @return productVo - ProductInfoVO.
	 */
	public ProductInfoVO getProductVoFromColectionVo(CollectionProductInfoVO pCollectionProductInfo, String pProductId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCollectionLineItemInfoLookupDroplet.getProductVoFromColectionVo method.. ");
		}
		List<ProductInfoVO> productVos=null;
		ProductInfoVO productVo=null;
		productVos=pCollectionProductInfo.getActiveProductInfoList();
		if(productVos!=null && !productVos.isEmpty())
			{
				for (ProductInfoVO productInfoVO : productVos) {
					if(productInfoVO.getProductId().equalsIgnoreCase(pProductId)){
						productVo=productInfoVO;
						break;
					}
				}
			}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCollectionLineItemInfoLookupDroplet.getProductVoFromColectionVo method.. :: productVo::"+productVo);
		}
		return productVo;
	}

	/**
	 * Gets the collectionProductCache.
	 * 
	 * @return the collectionProductCache.
	 */
	public TRUCollectionProductCache getCollectionProductCache() {
		return mCollectionProductCache;
	}

	/**
	 * Sets the collectionProductCache.
	 * 
	 * @param pCollectionProductCache the collectionProductCache to set.
	 */
	public void setCollectionProductCache(TRUCollectionProductCache pCollectionProductCache) {
		mCollectionProductCache = pCollectionProductCache;
	}
	
	/**
	 * Gets the dimensionValueCacheTools.
	 * 
	 * @return the dimensionValueCacheTools.
	 */
	public DimensionValueCacheTools getDimensionValueCacheTools() {
		return mDimensionValueCacheTools;
	}

	/**
	 * Sets the dimensionValueCacheTools.
	 * 
	 * @param pDimensionValueCacheTools the dimensionValueCacheTools to set.
	 */
	public void setDimensionValueCacheTools(DimensionValueCacheTools pDimensionValueCacheTools) {
		mDimensionValueCacheTools = pDimensionValueCacheTools;
	}

}
