package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import atg.service.response.output.JSONWriterOutputCustomizer;
import atg.service.response.output.OutputException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.endeca.infront.assembler.ContentItem;


/**
 * This class used to convert content item into json droplet.
 * 
 * @version 1.0
 * @author Professional Access.
 * 
 */

public class TRUConvertContentItemToJSONDroplet extends DynamoServlet{
	
	/**
	 * hold constant CONTENT_ITEMS.
	 */
	private static final String CONTENT_ITEMS = "contentItems";
	/**
	 * hold to property jsonOutPutCustomizer.
	 */
	private JSONWriterOutputCustomizer mJsonOutPutCustomizer =null;
	
	/**
	 * @return the jsonOutPutCustomizer.
	 */
	public JSONWriterOutputCustomizer getJsonOutPutCustomizer() {
		return mJsonOutPutCustomizer;
	}

	/**
	 * @param pJsonOutPutCustomizer the jsonOutPutCustomizer to set.
	 */
	public void setJsonOutPutCustomizer(
			JSONWriterOutputCustomizer pJsonOutPutCustomizer) {
		mJsonOutPutCustomizer = pJsonOutPutCustomizer;
	}

	/**
	 * Convert contentItem map object to JSON object. 
	 * 
	 * @param pRequest
	 *            - holds the reference for DynamoHttpServletRequest.
	 * @param pResponse
	 *            - holds the reference for DynamoHttpServletResponse.
	 * @throws ServletException
	 *             - ServletException.
	 * @throws IOException
	 *             - IOException.
	 */
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Entered the Class:[TRUConvertContentItemToJSON] Method:[service] ");
		}
		//Get content object
		ContentItem objConentItem = (ContentItem) pRequest.getObjectParameter(CONTENT_ITEMS);
		
		if (objConentItem != null && getJsonOutPutCustomizer()!= null){
				Map options = new HashMap();
				try {
				   Object jsonData =    getJsonOutPutCustomizer().generateResponseAsJSONWriter(objConentItem,options); 
				   if (isLoggingDebug()) {
						logDebug("jsonData ::"+jsonData);
					}
			} catch (OutputException e) {
				if (isLoggingError()) {
					logError("OutputException in TRUConvertContentItemToJSON.service method ",e);
				}
			}                                                                          
			
		}
		
		if (isLoggingDebug()) {
			logDebug("Exit the Class:[TRUConvertContentItemToJSON] Method:[service]");
		}
	}
}
