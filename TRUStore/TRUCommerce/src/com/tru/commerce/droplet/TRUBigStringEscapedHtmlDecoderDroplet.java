package com.tru.commerce.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.endeca.infront.shaded.org.apache.commons.lang.StringEscapeUtils;
import com.tru.commerce.TRUCommerceConstants;

/**
 * This droplet is used to decode the given data into escape html.
 * 
 * <br>
 * <br>
 * <b>Input Parameters.</b><br>
 * &nbsp;&nbsp;&nbsp;<code>data</code> - Data to be encoded.<br>
 * 
 * <b>Output Parameters.</b><br>
 * &nbsp;&nbsp;&nbsp;<code>enocdeData</code> - Encoded data.<br>
 * 
 * <b>Open Parameters</b><br>
 * &nbsp;&nbsp;&nbsp;<b>output - If the requested data not empty.</b> <br>
 * &nbsp;&nbsp;&nbsp;<b>empty - If the requested data is empty.</b>
 * 
 * <br>
 * <b>Usage:</b><br>
 * &lt;dspel:droplet name="/com/tru/commerce/droplet/TRUBigStringEscapedHtmlDecoderDroplet"&gt;<br>
 * &lt;dspel:param name="data" param="data"/&gt;<br>
 * &lt;dspel:oparam name="output"&gt;<br>
 * &lt;/dspel:oparam&gt;<br>
 * &lt;dspel:oparam name="empty"&gt;<br>
 * &lt;/dspel:oparam&gt;<br>
 * &lt;/dspel:droplet&gt;
 * @author PA.
 * @version 1.0
 */
public class TRUBigStringEscapedHtmlDecoderDroplet extends DynamoServlet {

	/**
	 * This method will get the data from the JSP as a parameter and it will unescapeHtml.
	 * 
	 * @param pRequest - reference to DynamoHttpServletRequest object.
	 * @param pResponse - reference to DynamoHttpServletResponse object.
	 * @throws ServletException - if any exception occurs in servicing the request.
	 * @throws IOException - if any exception occurs while writing the response to output stream.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		String data= (String)pRequest.getLocalParameter(TRUCommerceConstants.DATA);
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUBigStringEscapedHtmlDecoderDroplet.service method..  Data : {0}", data);
		}
		if (!StringUtils.isBlank(data)) {
		String escapeHtml = StringEscapeUtils.unescapeHtml(data);
	       escapeHtml = escapeHtml.replaceAll(TRUCommerceConstants.GT, TRUCommerceConstants.GT_SYMBOL);
	       escapeHtml = escapeHtml.replaceAll(TRUCommerceConstants.LT, TRUCommerceConstants.LT_SYMBOL);
	       escapeHtml = escapeHtml.replace(TRUCommerceConstants.REG_SYMBOL, TRUCommerceConstants.REG_ENTITY);
	       if (isLoggingDebug()) {
				vlogDebug("escapeHtml : {0}", escapeHtml);
			}
		pRequest.setParameter(TRUCommerceConstants.ENCODE_DATA, escapeHtml);
		pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		}
		else {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUBigStringEscapedHtmlDecoderDroplet.service method.. ");
		}
	}
}
