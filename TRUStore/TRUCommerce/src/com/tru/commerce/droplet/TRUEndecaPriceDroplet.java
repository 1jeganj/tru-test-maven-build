package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.google.gson.Gson;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.common.TRUConstants;
import com.tru.utils.TRUEndecaPriceUtils;
/**
 * TRUEndecaPriceDroplet droplet will be used to display the Price on endeca pages.
 * 
 * Droplet will use to fetch the data from Collection/Regular/Variant products.
 * 
 * @author Professional Access.
 * @version 1.0
 * 
 */
public class TRUEndecaPriceDroplet extends DynamoServlet{
	
	/** Property to hold is mEndecaUtils. */
	private TRUEndecaPriceUtils mEndecaUtils;
	/**
	 * @return the endecaUtils
	 */
	public TRUEndecaPriceUtils getEndecaUtils() {
		return mEndecaUtils;
	}
	/**
	 * @param pEndecaUtils the endecaUtils to set
	 */
	public void setEndecaUtils(TRUEndecaPriceUtils pEndecaUtils) {
		mEndecaUtils = pEndecaUtils;
	}
	
	/**
	 * This method populates the prices to map.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest.
	 * @param pResponse
	 *            - DynamoHttpServletResponse.
	 * @throws ServletException -ServletException.
	 * @throws IOException - IOException.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUEndecaPriceDroplet  method: service]");
		}
		String skuProdString = (String) pRequest.getLocalParameter(TRUCommerceConstants.SKU_STRING);
		if(isLoggingDebug()){
			vlogDebug("Input parameter value : {0}", skuProdString);
		}
		if(StringUtils.isBlank(skuProdString)){
			pRequest.serviceLocalParameter(TRUConstants.EMPTY_OPARAM, pRequest, pResponse);
			return;
		}
		String[] skuProds = skuProdString.split(TRUCommerceConstants.SPLIT_PIPE);
		if(isLoggingDebug()){
			logDebug("SKU-Product Array :::" + skuProds);
		}
		if(skuProds != null && skuProds.length <= TRUConstants.ZERO){
			pRequest.serviceLocalParameter(TRUConstants.EMPTY_OPARAM, pRequest, pResponse);
			return;
		}
		// Calling method to fetch the prices
		Map<String, Map<String, Object>> priceMap = getEndecaUtils().generatePriceMapForEndeca(skuProds);
		Gson gson = new Gson();
		String formatedString = null;
		if(priceMap != null && !priceMap.isEmpty()){
			formatedString = gson.toJson(priceMap);
		}
		if(priceMap == null || priceMap.isEmpty()){
			pRequest.serviceLocalParameter(TRUConstants.EMPTY_OPARAM, pRequest, pResponse);
		}else{
			pRequest.setParameter(TRUConstants.PRICE_MAP, priceMap);
			pRequest.setParameter(TRUConstants.PRICEMAP_JSON, formatedString);
			pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUEndecaPriceDroplet  method: service]");
		}
	}
}
