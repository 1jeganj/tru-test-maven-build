
package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.common.TRUStoreConfiguration;

/**
 * This droplet is used to construct List of Map object which has product features from the products available in the productList component.
 * This map which will be used to display the Product Comparison table.
 * 
 * Input parameters - 1. productComparisonList - List of product repository items productComparisonList
 * 
 * Output parameters - 1. attriMap - This Map containing product keyFeatures to display in comparison page 2.productList
 * - List of product for comparison page.
 * 
 * open parameters - 1. error - if Parameter:productComparisonList is null 2. output - to render displays product all
 * the features and services in comparison page.
 * 
 * <br>
 * <br>
 * <b>Input Parameters</b><br>
 * &nbsp;&nbsp;&nbsp;<code>productList</code> - ProductComparisonList<br>
 * &nbsp;&nbsp;&nbsp;<code>categoryId</code> - String<br>
 * 
 * <b>Output Parameters</b><br>
 * &nbsp;&nbsp;&nbsp;<b>No output Parameters available</b><br>
 * 
 * <b>Open Parameters</b><br>
 * &nbsp;&nbsp;&nbsp;<b>No open Parameters available</b><br>
 * 
 * <br>
 * <b>Usage:</b><br>
 * &lt;dspel:droplet name="/com/tru/commerce/droplet/TRUCompareProductFeatureDroplet"&gt;<br>
 * &lt;dspel:param name="productList" bean="/atg/commerce/catalog/comparison/ProductList" /&gt;<br>
 * &lt;/dspel:droplet&gt;
 * @version 1.0
 * @author Professional Access
 * 
 */
public class TRUCompareProductFeatureDroplet extends DynamoServlet {

	/**
	 * PRODUCT_COMPARISON_LIST parameter name.
	 */
	private static final ParameterName PRODUCT_COMPARISON_LIST = ParameterName
			.getParameterName("activeProductList");

	/**
	 * Property to hold mCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;

	/**
	 * Property to hold mAttributeName.
	 */
	private String mAttributeName;

	/**
	 * Property to hold StoreConfiguration.
	 */
	private TRUStoreConfiguration mStoreConfiguration;

	/**
	 * Gets the storeConfiguration.
	 * 
	 * @return the storeConfiguration.
	 */
	public TRUStoreConfiguration getStoreConfiguration() {
		return mStoreConfiguration;
	}

	/**
	 * Sets the storeConfiguration.
	 * 
	 * @param pStoreConfiguration
	 *            the storeConfiguration to set.
	 */
	public void setStoreConfiguration(TRUStoreConfiguration pStoreConfiguration) {
		mStoreConfiguration = pStoreConfiguration;
	}

	/**
	 * This method is used to get the attributeName.
	 * 
	 * @return the attributeName.
	 */
	public String getAttributeName() {
		return mAttributeName;
	}

	/**
	 * This method is used to set the attributeName.
	 * 
	 * @param pAttributeName
	 *            the attributeName to set.
	 */
	public void setAttributeName(String pAttributeName) {
		mAttributeName = pAttributeName;
	}

	/**
	 * Getter of catalogTools.
	 * 
	 * @return the catalogTools.
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Setter of catalogTools.
	 * 
	 * @param pCatalogTools
	 *            the catalogTools to set.
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * This method is used to construct List of Map object which has product features from the products available in the productList component. 
	 * This map which will be used to display the Product Comparison table.
	 * 
	 * @param pRequest
	 *            - reference to DynamoHttpServletRequest object.
	 * @param pResponse
	 *            - reference to DynamoHttpServletResponse object.
	 * @throws ServletException
	 *             - if any exception occurs in servicing the request.
	 * @throws IOException
	 *             - if any exception occurs while writing the response to output stream.
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUCompareProductFeatureDroplet.service method.. ");
		}
		final List productList = (List) pRequest.getObjectParameter(PRODUCT_COMPARISON_LIST);
		String categoryId = (String)pRequest.getLocalParameter(TRUCommerceConstants.CATEGORY_ID);
		if (isLoggingDebug()) {
			vlogDebug("List of Products selected for comparision from User {0}", productList);
		}
		if (productList == null || productList.isEmpty()) {
			pRequest.serviceLocalParameter(TRUCommerceConstants.ERROR_OPARAM, pRequest, pResponse);
			return;
		}

		if (isLoggingDebug()) {
			vlogDebug("User Selected Product Items {0}", productList);
		}
		if (productList == null || productList.size() < getStoreConfiguration().getMinAllowedProductToCompare()|| 
				productList.size() > getStoreConfiguration().getMaxAllowedProductToCompare()) {
			pRequest.serviceLocalParameter(TRUCommerceConstants.ERROR_OPARAM, pRequest, pResponse);
			if (isLoggingDebug()) {
				vlogDebug("Error:Comparison product list is less than or equal to 1");
			}
			return;
		}
		// getting attributes and value map for compare list
		if(StringUtils.isNotBlank(categoryId)){
		Set<RepositoryItem> classificationAttributeNamesList=getCatalogTools().getClassificationAttributeNames(categoryId);
		final Map attributeAndValueMap = getCatalogTools().createAttributeAndValueMap(productList,classificationAttributeNamesList);
		if (attributeAndValueMap != null) {
			pRequest.setParameter(TRUCommerceConstants.PRODUCT_ATTRIBUTE_VALUES,attributeAndValueMap.get(TRUCommerceConstants.PRODUCT_ATTRIBUTE_VALUES));
			pRequest.setParameter(TRUCommerceConstants.ATTRIBUTE_MAP, attributeAndValueMap.get(TRUCommerceConstants.ATTRIBUTE_MAP));
			pRequest.setParameter(TRUCommerceConstants.ATTRIBUTE_GROUP_ITEMS, attributeAndValueMap.get(TRUCommerceConstants.ATTRIBUTE_GROUP_ITEMS));
			pRequest.setParameter(TRUCommerceConstants.ATTRIBUTE_MAP_FOREACH_PRODUCT, attributeAndValueMap.get(TRUCommerceConstants.ATTRIBUTE_MAP_FOREACH_PRODUCT));
			pRequest.setParameter(TRUCommerceConstants.SET_OF_ATTRIBUTENAME, attributeAndValueMap.get(TRUCommerceConstants.SET_OF_ATTRIBUTENAME));
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		}
		pRequest.setParameter(TRUCommerceConstants.PRODUCT_LIST_NAME, productList);
		}

		if (isLoggingDebug()) {
			vlogDebug("END:: TRUCompareProductFeatureDroplet.service method.. ");
		}
	}
}
