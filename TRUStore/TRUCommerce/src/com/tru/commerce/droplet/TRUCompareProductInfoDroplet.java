
package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogManager;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.vo.CompareProductInfoVO;
import com.tru.commerce.catalog.vo.ProductInfoVO;
import com.tru.commerce.catalog.vo.SKUInfoVO;
import com.tru.common.TRUStoreConfiguration;
import com.tru.utils.TRUStoreUtils;

/**
 * This droplet is used to construct List of Map object which has product features from the products available in the
 * productList component. This map which will be used to display the Product Comparison table.
 * 
 * Input parameters - 1. productComparisonList - List of product repository items productComparisonList
 * 
 * Output parameters - 1. attriMap - This Map containing product keyFeatures to display in comparison page 2.productList
 * - List of product for comparison page.
 * 
 * open parameters - 1. error - if Parameter:productComparisonList is null 2. output - to render displays product all
 * the features and services in comparison page.
 * 
 * <br>
 * <br>
 * <b>Input Parameters</b><br>
 * &nbsp;&nbsp;&nbsp;<code>productList</code> - ProductComparisonList<br>
 * &nbsp;&nbsp;&nbsp;<code>categoryId</code> - String<br>
 * 
 * <b>Output Parameters</b><br>
 * &nbsp;&nbsp;&nbsp;<b>No output Parameters available</b><br>
 * 
 * <b>Open Parameters</b><br>
 * &nbsp;&nbsp;&nbsp;<b>No open Parameters available</b><br>
 * 
 * <br>
 * <b>Usage:</b><br>
 * &lt;dspel:droplet name="/com/tru/commerce/droplet/TRUCompareProductInfoDroplet"&gt;<br>
 * &lt;dspel:param name="productList" bean="/atg/commerce/catalog/comparison/ProductList" /&gt;<br>
 * &lt;/dspel:droplet&gt;
 * 
 * @author Professional Access
 * @version 1.0
 * 
 */
public class TRUCompareProductInfoDroplet extends DynamoServlet {

	/**
	 * PRODUCT_COMPARISON_LIST parameter name.
	 */
	private static final ParameterName PRODUCT_COMPARISON_LIST = ParameterName
			.getParameterName("productComparisonList");

	/**
	 * Property to hold mCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;

	/**
	 * Property to hold mAttributeName.
	 */
	private String mAttributeName;

	/**
	 * Property to hold StoreConfiguration.
	 */
	private TRUStoreConfiguration mStoreConfiguration;
	/**
	 * Property to hold TRUCatalogManager.
	 */
	private TRUCatalogManager mCatalogManager;
	
	/**
	 * Gets the storeConfiguration.
	 * 
	 * @return the storeConfiguration.
	 */
	public TRUStoreConfiguration getStoreConfiguration() {
		return mStoreConfiguration;
	}

	/**
	 * Sets the storeConfiguration.
	 * 
	 * @param pStoreConfiguration
	 *            the storeConfiguration to set.
	 */
	public void setStoreConfiguration(TRUStoreConfiguration pStoreConfiguration) {
		mStoreConfiguration = pStoreConfiguration;
	}

	/**
	 * This method is used to get the attributeName.
	 * 
	 * @return the attributeName.
	 */
	public String getAttributeName() {
		return mAttributeName;
	}

	/**
	 * This method is used to set the attributeName.
	 * 
	 * @param pAttributeName
	 *            the attributeName to set.
	 */
	public void setAttributeName(String pAttributeName) {
		mAttributeName = pAttributeName;
	}

	/**
	 * Getter of catalogTools.
	 * 
	 * @return the catalogTools.
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Setter of catalogTools.
	 * 
	 * @param pCatalogTools
	 *            the catalogTools to set.
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * @return the TRUCatalogManager.
	 */
	public TRUCatalogManager getCatalogManager() {
		return mCatalogManager;
	}

	/**
	 * @param pCatalogManager the TRUCatalogManager to set.
	 */
	public void setCatalogManager(TRUCatalogManager pCatalogManager) {
		mCatalogManager = pCatalogManager;
	}

	/**
	 * This method is used to construct List of Map object which has product features from the products available in the productList component. 
	 * This map which will be used to display the Product Comparison table.
	 * 
	 * @param pRequest
	 *            - reference to DynamoHttpServletRequest object.
	 * @param pResponse
	 *            - reference to DynamoHttpServletResponse object.
	 * @throws ServletException
	 *             - if any exception occurs in servicing the request.
	 * @throws IOException
	 *             - if any exception occurs while writing the response to output stream.
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked", "null", "unused" })
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUCompareProductInfoDroplet.service method.. ");
		}
		
		List<RepositoryItem> activeProductList = new ArrayList<RepositoryItem>();
		//List<Map<RepositoryItem,RepositoryItem>> listofCompareproduct=null;
		List<ProductInfoVO> productInfoList=null;
		Map<String,String> productSkumap= new HashMap<String,String>();
		CompareProductInfoVO compareProductInfoVO=null;
		final String cookieValue = pRequest.getCookieParameter(TRUCommerceConstants.FAV_STORE);
		final List productComparisonList = (List) pRequest.getObjectParameter(PRODUCT_COMPARISON_LIST);
		String categoryId = (String)pRequest.getLocalParameter(TRUCommerceConstants.CATEGORY_ID);
		
		if (isLoggingDebug()) {
			vlogDebug("List of Products selected for comparision from User {0}", productComparisonList);
		}
			// getting ACTIVE product items from comparison list
		 	activeProductList = getCatalogTools().getProductItems(productComparisonList,productSkumap,categoryId);
			if (productComparisonList==null || productComparisonList.isEmpty()  || activeProductList==null || activeProductList.isEmpty()) {
				pRequest.serviceLocalParameter(TRUCommerceConstants.ERROR_OPARAM, pRequest, pResponse);
				return;
			}
		if ( !productComparisonList.isEmpty()  &&  !activeProductList.isEmpty()) {
			compareProductInfoVO = getCatalogTools().createCompareProductInfo(activeProductList,Boolean.TRUE);
			if(compareProductInfoVO!=null){
				final TRUStoreUtils storeUtils= new TRUStoreUtils();
				final List<ProductInfoVO> productInfos = compareProductInfoVO.getProductInfoList();
				 final String storeId=storeUtils.getStoreIdFromCoockie(cookieValue);
				 productInfoList = new ArrayList<ProductInfoVO>();
				 List compareAtrributeList=  new ArrayList<>();
				 List compareAtrributeNameList=  new ArrayList<>();
				 if(productInfos != null && !productInfos.isEmpty()){
					 int infoslength=productInfos.size();
				 for(int i=0;i<infoslength;i++)
					{
						SKUInfoVO defaultSku = getCatalogManager().getCatalogTools().getDefaultSkuInfoAndPopulateInvetoryInformation(
								storeId, productInfos.get(i));
					     String key=productInfos.get(i).getProductId()+TRUCommerceConstants.UNDER_SCORE+i;
					     String addedSkuId=productSkumap.get(key);
					     if(StringUtils.isNotBlank(addedSkuId) &&  
					    		 TRUCommerceConstants.NO_COLOR_SIZE_AVAILABLE.equals(productInfos.get(i).getColorSizeVariantsAvailableStatus())){
					    	 defaultSku= getCatalogManager().getCatalogTools().findDefaultSkuFormSkuId(productInfos.get(i), addedSkuId);
					     }
					     productInfos.get(i).setDefaultSKU(defaultSku);
						 productInfoList.add(productInfos.get(i));
							List classificationAttributeNamesList = getCatalogTools().getAllClassificationAttributeNamesList(categoryId);
							RepositoryItem activeprodItem=null;
							for (RepositoryItem activeProduct : activeProductList) {
								if(activeProduct.getRepositoryId().equals(productInfos.get(i).getProductId())){
									activeprodItem=activeProduct;
									break;
								}
							}
						 Map valueMap= getCatalogTools().getCompareableProperties(activeprodItem,defaultSku,classificationAttributeNamesList);
						 if (isLoggingDebug()) {
								vlogDebug("List of map selected for comparision, valueMap::", valueMap);
							}
						 if(valueMap != null && !valueMap.isEmpty()){
						 compareAtrributeList.add(valueMap.get(TRUCommerceConstants.PRODUCT_ATTRIBUTE_NAME_VALUE_MAP)); 
						 if(compareAtrributeNameList == null || compareAtrributeNameList.isEmpty()){
							 compareAtrributeNameList= (List) valueMap.get(TRUCommerceConstants.SET_OF_ATTRIBUTENAME);
						 }
						 }
					}
				 }
				 if(productInfoList != null && !productInfoList.isEmpty())
				 {
					 compareProductInfoVO.setProductInfoList(productInfoList);
				 }
				 if(compareAtrributeList != null && !compareAtrributeList.isEmpty()){
					 pRequest.setParameter(TRUCommerceConstants.COMPARE_ATTRIBUTE_LIST, compareAtrributeList);
				 }
				 if(compareAtrributeNameList != null && !compareAtrributeNameList.isEmpty()){
					 pRequest.setParameter(TRUCommerceConstants.SET_OF_ATTRIBUTENAME, compareAtrributeNameList);
				 }
			pRequest.setParameter(TRUCommerceConstants.COMPARE_PRODUCT_INFO, compareProductInfoVO);
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
			}else{
				pRequest.serviceLocalParameter(TRUCommerceConstants.ERROR_OPARAM, pRequest, pResponse);
				return;
			}
		}
		
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUCompareProductInfoDroplet.service method.. ");
		}
	}	
}
