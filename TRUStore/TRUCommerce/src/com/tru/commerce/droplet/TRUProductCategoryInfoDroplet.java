package com.tru.commerce.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;


/**
 * This class used to render product related information in PDP page.
 * 
 * @author Professional Access.
 * @version 1.0
 * 
 */
public class TRUProductCategoryInfoDroplet extends DynamoServlet {

	
	/**
	 * This property hold reference for TRUCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;
	
	/** Property to hold mCatalogProperties. */
	private TRUCatalogProperties mCatalogProperties;
	
	
	/**
	 * Gets the catalogTools.
	 * 
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Sets the catalogTools.
	 * 
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * Gets the catalog properties.
	 *
	 * @return the mCatalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}
	
	/**
	 * Sets the catalog properties.
	 *
	 * @param pCatalogProperties the mCatalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		this.mCatalogProperties = pCatalogProperties;
	}
	
	/**
	 * This method will get the product category related information required to be populated tealium utag parameters.
	 * 
	 * @param pRequest - reference to DynamoHttpServletRequest object.
	 * @param pResponse - reference to DynamoHttpServletResponse object.
	 * @throws ServletException - if any exception occurs in servicing the request.
	 * @throws IOException - if any exception occurs while writing the response to output stream.
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUProductCategoryInfoDroplet.service method.. ");
		}
		Object productIdObj = pRequest.getLocalParameter(TRUCommerceConstants.PRODUCT_ID);
		String selectedSkuId = (String) pRequest.getLocalParameter(TRUCommerceConstants.SELECTED_SKUID_PDP);
		String productCategoryName =  TRUCommerceConstants.EMPTY_SPACE;
		String productId =  TRUCommerceConstants.EMPTY_SPACE;
		String onlinePid =  TRUCommerceConstants.EMPTY_SPACE;
		String subCategoryName = TRUCommerceConstants.EMPTY_SPACE;
		if (isLoggingDebug()) {
			logDebug("TRUProductCategoryInfoDroplet.service method.. selectedSkuId-->" + selectedSkuId);
		}
		if(productIdObj!=null){
			productId =productIdObj.toString();
		}
		if(selectedSkuId != null){
			onlinePid = getCatalogTools().getOnlinePIDFromSKUId(selectedSkuId);
		}
		
		productCategoryName = getCatalogTools().getProductCategoryName(productId);
		subCategoryName = getCatalogTools().getProductSubCategory(productId,productCategoryName);
		pRequest.setParameter(TRUCommerceConstants.PRODUCT_CATEGORY_NAME, productCategoryName);
		pRequest.setParameter(TRUCommerceConstants.PRODUCT_SUBCAT_NAME, subCategoryName);
		pRequest.setParameter(TRUCommerceConstants.ONLINEPID_SKU, onlinePid);
		
        pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		if (productId==null) {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUProductCategoryInfoDroplet.service method.. ");
		}
	}

}
