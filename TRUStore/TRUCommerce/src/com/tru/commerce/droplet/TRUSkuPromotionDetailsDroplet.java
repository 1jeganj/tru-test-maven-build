package com.tru.commerce.droplet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.sql.DataSource;

import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.StringUtils;
import atg.repository.Repository;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.google.gson.Gson;
import com.tru.cache.TRUPromoCache;
import com.tru.cache.TRUPromotionCache;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.exception.TRUCommerceException;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.vo.TRUPromoContainer;
import com.tru.common.vo.TRUPromoInfoRequestVO;
import com.tru.common.vo.TRUPromoVO;
import com.tru.common.vo.TRUPromotionInfoVO;

/**
 * This class will get the promotion details from the sku ids
 * that are supplied from jsp.
 */
public class TRUSkuPromotionDetailsDroplet extends DynamoServlet {
	
	/**
	 * To Hold mCatalogReposiory
	 */
	private Repository mCatalogReposiory;
	
	/** The test mode. */
	private boolean mTestMode;
	
	/** The m promotion id. */
	private String mPromotionId;
	
	/**
	 * property to hold dataSource
	 */
	private DataSource mDataSource;
	
	/**
	 * property to hold mPromotionQuery
	 */
	private String mPromotionQuery;
	
	/**
	 * property to hold mPromoCache
	 */
	private TRUPromoCache mPromoCache;
	
	/**
	 * property to hold mPromoCacheEnable
	 */
	private boolean mPromoCacheEnable;
	
	/** The m promotion cache. */
	private TRUPromotionCache mPromotionCache;
	
	/**
	 * Holds reference for TRUConfiguration.
	 */
	private TRUConfiguration mTruConfiguration;
	
	/**
	 * @return the promoCacheEnable
	 */
	public boolean isPromoCacheEnable() {
		return mPromoCacheEnable;
	}

	/**
	 * @param pPromoCacheEnable the mPromoCacheEnable to set
	 */
	public void setPromoCacheEnable(boolean pPromoCacheEnable) {
		mPromoCacheEnable = pPromoCacheEnable;
	}

	/**
	 * @return the promotionQuery
	 */
	public String getPromotionQuery() {
		return mPromotionQuery;
	}

	/**
	 * @param pPromotionQuery the promotionQuery to set
	 */
	public void setPromotionQuery(String pPromotionQuery) {
		mPromotionQuery = pPromotionQuery;
	}

	/**
	 * Gets the promotion id.
	 *
	 * @return the promotionId
	 */
	public String getPromotionId() {
		return mPromotionId;
	}

	/**
	 * Sets the promotion id.
	 *
	 * @param pPromotionId the promotionId to set
	 */
	public void setPromotionId(String pPromotionId) {
		mPromotionId = pPromotionId;
	}

	/**
	 * To Hold mDummySkuList
	 */
	private String[] mDummySkuList;
	
	/** The m enable. */
	private boolean mEnable;
	
	/**
	 * Checks if is enable.
	 *
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * Gets the dummy sku list.
	 *
	 * @return the dummySkuList
	 */
	public String[] getDummySkuList() {
		return mDummySkuList;
	}

	/**
	 * Sets the dummy sku list.
	 *
	 * @param pDummySkuList the dummySkuList to set
	 */
	public void setDummySkuList(String[] pDummySkuList) {
		mDummySkuList = pDummySkuList;
	}


	/** To Hold mEligiblePromos. */
	private List<String> mEligiblePromos;
	
	/**
	 * Gets the eligible promos.
	 *
	 * @return the eligiblePromos
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<String> getEligiblePromos() {
		if (this.mEligiblePromos == null) {
			this.mEligiblePromos = new ArrayList();
		}
		return mEligiblePromos;
	}

	/**
	 * Sets the eligible promos.
	 *
	 * @param pEligiblePromos the eligiblePromos to set
	 */
	public void setEligiblePromos(List<String> pEligiblePromos) {
		mEligiblePromos = pEligiblePromos;
	}


	/** To Hold mCatalogProperties. */
	private TRUCatalogProperties mCatalogProperties;
	
	/** The m promotion tools. */
	private TRUPromotionTools mPromotionTools;

	/**
	 * Gets the promotion tools.
	 *
	 * @return the promotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * Sets the promotion tools.
	 *
	 * @param pPromotionTools the promotionTools to set
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		mPromotionTools = pPromotionTools;
	}

	/**
	 * Gets the catalog properties.
	 *
	 * @return the catalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the catalog properties.
	 *
	 * @param pCatalogProperties the catalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}

	/**
	 * Gets the catalog reposiory.
	 *
	 * @return the catalogReposiory
	 */
	public Repository getCatalogReposiory() {
		return mCatalogReposiory;
	}
	
	/**
	 * Checks if is test mode.
	 *
	 * @return true, if is test mode
	 */
	public boolean isTestMode() {
		return mTestMode;
	}

	/**
	 * Sets the test mode.
	 *
	 * @param pTestMode the new test mode
	 */
	public void setTestMode(boolean pTestMode) {
		this.mTestMode = pTestMode;
	}

	/**
	 * Sets the catalog reposiory.
	 *
	 * @param pCatalogReposiory            the catalogReposiory to set
	 */
	public void setCatalogReposiory(Repository pCatalogReposiory) {
		mCatalogReposiory = pCatalogReposiory;
	}
	
	/**
	 * @return the dataSource
	 */
	public DataSource getDataSource() {
		return mDataSource;
	}

	/**
	 * @param pDataSource
	 *            the dataSource to set
	 */
	public void setDataSource(DataSource pDataSource) {
		mDataSource = pDataSource;
	}
	
	/**
	 * @return the promoCache
	 */
	public TRUPromoCache getPromoCache() {
		return mPromoCache;
	}

	/**
	 * @param pPromoCache the promoCache to set
	 */
	public void setPromoCache(TRUPromoCache pPromoCache) {
		mPromoCache = pPromoCache;
	}
	
	

	/**
	 * Service method to fetch the promotion details for the SKU
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(this.getClass().getName(),
					TRUCommerceConstants.SKU_PROMOTION_DROPLET);
		}
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUSkuPromotionDetails;  method: service]");
			vlogDebug("Request : {0},response : {1}", pRequest,
					pResponse);
		}
		String[] skuIds= null;	
		String formatedString = null;
		boolean allPromo = Boolean.parseBoolean((String)pRequest.getLocalParameter(TRUCommerceConstants.ALL_SKUS_PROMO)) ;
		if (allPromo) {
			String skuId = null;
			TRUPromotionInfoVO promoInfo = null;
			skuId = ((String) pRequest.getLocalParameter(TRUCommerceConstants.SKU_STRING));
			Object[] promotionObject = null;
			TRUPromoInfoRequestVO skuInfoRequestVO = new TRUPromoInfoRequestVO();
			if (StringUtils.isNotEmpty(skuId)) {
				skuIds = skuId.split(TRUCommerceConstants.PIPELINE_SEPERATOR);
				skuInfoRequestVO.setSkuIds(skuIds);
				try {
					if (isPromoCacheEnable()) {
						promotionObject = (Object[]) getPromoCache().get(skuInfoRequestVO.getSkuIds());	
					} else {
						promoInfo = getPromotionTools().populateAllPromo(skuIds);
					}
				} catch (Exception exception) {
					vlogError("Exception : {0}", exception);
				} 
			}
			if (promotionObject != null) {
				pRequest.setParameter(TRUCommerceConstants.PROMOTION_INFO, promotionObject);
			}
			if (promoInfo != null) {
				pRequest.setParameter(TRUCommerceConstants.PROMOTION_INFO, promoInfo.getPromotionInfoList());
			}
			if (promotionObject == null && promoInfo == null) {
				pRequest.serviceLocalParameter(TRUConstants.EMPTY_OPARAM, pRequest, pResponse);
				return;	
			}
		}
		if (isEnable() && !allPromo) {
			String skuId = null;
			String promotionId = null;
			if (pRequest.getLocalParameter(TRUCommerceConstants.SKU_STRING) != null) {
				skuId = ((String) pRequest.getLocalParameter(TRUCommerceConstants.SKU_STRING));
				skuIds = skuId.split(TRUCommerceConstants.PIPELINE_SEPERATOR);
			}
			if (pRequest.getLocalParameter(TRUCommerceConstants.PROMOTION_ID) != null) {
				promotionId = ((String) pRequest.getLocalParameter(TRUCommerceConstants.PROMOTION_ID));
			}
			if (isLoggingDebug()) {
				vlogDebug("promotionId : {0},skuIds : {1}", promotionId, skuIds);
			}
			List<TRUPromoVO> promotionsList = null;
			List<Object> promotionDetailList = new ArrayList<Object>();
			Map<Object, Object> promotionDetailsObjMap = null;
			Map<Object, Object> promotionDetailObjMap = new HashMap<Object, Object>();
			Set<String> skuItems = new HashSet<String>();
			if (null != skuIds && skuIds.length >TRUCommerceConstants.INT_ZERO) {
				skuItems.addAll(Arrays.asList(skuIds));
			}
			if (skuItems != null && skuItems.size() > TRUCommerceConstants.INT_ZERO) {
				try {
					for (String skuItem : skuItems) {
						TRUPromoContainer truPromoContainer = null;
						promotionDetailsObjMap = new HashMap<Object, Object>();
						TRUPromoContainer promoContainer = new TRUPromoContainer();
						promoContainer.setSkuId(skuItem);
						if (StringUtils.isNotEmpty(promotionId)) {
							promoContainer.setPromotionId(promotionId);
						}
						if (!getTruConfiguration().isPreviewPromoEnableInStaging()) {
							truPromoContainer = (TRUPromoContainer) getPromotionCache().get(promoContainer);
						} else {
							truPromoContainer = getPromotionTools().getPromotionDetailsForSKU(skuItem,promotionId,false);
						}
						if(null!=truPromoContainer&&null!=truPromoContainer.getPromotions()){
							List<TRUPromoVO> promotions =new ArrayList<TRUPromoVO>();
							promotions.addAll(truPromoContainer.getPromotions());
							if(promotions.size()>TRUCommerceConstants.INT_ZERO){
								promotionsList = new ArrayList<TRUPromoVO>();
								if(promotions.size()==TRUCommerceConstants.INT_ONE){
									promotionsList.addAll(promotions);
								}
								else{
									List<TRUPromoVO> list = new ArrayList<TRUPromoVO>(promotions);
									Collections.sort(list, new Comparator<TRUPromoVO>() {
										public int compare(TRUPromoVO pPriorityItem1, TRUPromoVO pPriorityItem2) {
											int priority1= pPriorityItem1.getPriority();
											int priority2= pPriorityItem2.getPriority();
											 return Integer.valueOf(priority1).compareTo(priority2);
										}
									});
									promotionsList.add(list.get(TRUCommerceConstants.INT_ZERO));
								}
							}
							promotionDetailsObjMap.put(getCatalogProperties().getSkuIdPropertyName(), truPromoContainer.getSkuId());
							promotionDetailsObjMap.put(TRUCommerceConstants.PROMOTIONS, promotionsList);
							promotionDetailList.add(promotionDetailsObjMap);
						}
					}
				} catch (TRUCommerceException e) {
					if(isLoggingError()){
						vlogError("RepositoryException in TRUPromotionTools", e);
					}
				}
				promotionDetailObjMap.put(TRUCommerceConstants.PROMOTION_DETAILS, promotionDetailList);
				Gson gson = new Gson();
				formatedString = gson.toJson(promotionDetailObjMap);
				
				pRequest.setParameter(TRUCommerceConstants.PROMOTION_DISPLAY_MAP, promotionDetailObjMap);
				pRequest.setParameter(TRUCommerceConstants.PROMOTION_DISPLAY, formatedString);
				pRequest.setParameter(TRUCommerceConstants.PROMOTION_INFO, promotionDetailList);
				pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
				if (isLoggingDebug()) {
					vlogDebug("formatedString : {0},promotionDetailObjMap : {1}", formatedString, promotionDetailObjMap);
				}
			}
		}
		if (isEnable() && allPromo) {
			pRequest.serviceLocalParameter(TRUConstants.ALL_PROMO, pRequest,pResponse);
		}
		if (!allPromo && StringUtils.isEmpty(formatedString)) {
			pRequest.serviceLocalParameter(TRUConstants.EMPTY_OPARAM, pRequest, pResponse);
			if(isLoggingDebug()){
				logDebug("test mode is false");
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit into [Class: TRUSkuPromotionDetails  method: service]");
		}
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(this.getClass().getName(),
					TRUCommerceConstants.SKU_PROMOTION_DROPLET);
		}
	}
	
	/**
	 * Populate promotion data.
	 *
	 * @param pPromotionData the promotion data
	 * @param pSkuItems the sku items
	 * @param pSkuIds the sku ids
	 */
	public void populatePromotionData(Map<String, List<String>> pPromotionData, Set<String> pSkuItems, String[] pSkuIds){
		
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUSkuPromotionDetails  method: populatePromotionData]");
		}
		String[] skuList=null;
		if(isTestMode()){
			skuList=getDummySkuList();
		}else{
			skuList=pSkuIds;
		}
		if(skuList!=null && skuList.length > TRUCommerceConstants.INT_ZERO){
		for(String skuid:skuList){
			Connection connection = null;
			String query = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			List<String> promoitonDetails=null;
			String promotionData = null;
			int i=TRUCommerceConstants.INT_ONE;
			try {
				connection = getDataSource().getConnection();
				if (connection != null) {
					query = getPromotionQuery();
					if (!StringUtils.isEmpty(query)) {
						ps = connection.prepareStatement(query);
						ps.setString(TRUCommerceConstants.INT_ONE, skuid);
						if (ps != null) {
							rs = ps.executeQuery();
							if (rs != null) {
								promoitonDetails=new ArrayList<String>();
								while (rs.next()) {
									promotionData = rs.getString(i);
									if(promotionData != null){
										promoitonDetails.add(promotionData);
									}
								}
								pPromotionData.put(skuid, promoitonDetails);
								pSkuItems.add(skuid);
							}
						}
					}
				}
			} catch (SQLException sQLException) {
				vlogError("SQLException : {0}", sQLException);
			} finally {
				try {
					if (ps != null) {
						ps.close();
					}
					if (connection != null) {
						connection.close();
					}
				} catch (SQLException sQLException) {
					vlogError("SQLException : {0}", sQLException);
				}
			}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit into [Class: TRUSkuPromotionDetails  method: populatePromotionData]");
		}
	}

	/**
	 * Gets the promotion cache.
	 *
	 * @return the promotion cache
	 */
	public TRUPromotionCache getPromotionCache() {
		return mPromotionCache;
	}

	/**
	 * Sets the promotion cache.
	 *
	 * @param pPromotionCache the new promotion cache
	 */
	public void setPromotionCache(TRUPromotionCache pPromotionCache) {
		this.mPromotionCache = pPromotionCache;
	}	
	
	/**
	 * Gets the tru configuration.
	 *
	 * @return the mTruConfiguration
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * Sets the tru configuration.
	 *
	 * @param pTruConfiguration the mTruConfiguration to set
	 */
	public void setTruConfiguration(TRUConfiguration pTruConfiguration) {
		this.mTruConfiguration = pTruConfiguration;
	}
}
