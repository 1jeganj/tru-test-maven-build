/*
 * Copyright (C) 2014, Professional Access Pvt Ltd. All Rights Reserved. No use,
 * copying or distribution of this work may be made except in accordance with a
 * valid license agreement from PA, India. This notice must be included on all
 * copies, modifications and derivatives of this work.
 */
package com.tru.commerce.droplet;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.endeca.infront.cartridge.model.Refinement;
import com.endeca.infront.cartridge.model.RefinementBreadcrumb;
import com.tru.commerce.TRUCommerceConstants;

/**
 * This droplet performs the following tasks:.
 * This Droplet is used to highlight the selected refinements.
 * Input Parameters:
 * refinements - represents the Refinements
 * dimensionName - represents the Dimension Name
 * Open Parameters:
 * selected
 * nonselected
 * Output Parameters:
 * selectedRefinement - represents Selected Refinement
 * refinement - represents Refinement
 * 
 * @author PA
 * @version 1.0
 */
public class TRUFacetSelectionsDroplet extends DynamoServlet {

	/**
	 * This service method used to highlight the selected refinements.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest instance
	 * @param pResponse
	 *            - DynamoHttpServletResponse instance
	 * @throws ServletException
	 *             - if an error occurs while processing the servlet
	 * @throws IOException
	 *             - if an error occurs while reading and writing the servlet
	 *             request
	 */
	@SuppressWarnings("unchecked")
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("DLXFacetSelectionsDroplet  (service) : BEGIN");
		}
		// holds the input parameter Refinement Crumbs
		List<RefinementBreadcrumb> refinementCrumbs = (List<RefinementBreadcrumb>) pRequest
				.getObjectParameter(TRUCommerceConstants.REFINEMENT_CRUMBS);
		if (refinementCrumbs == null) {
			refinementCrumbs = new ArrayList<RefinementBreadcrumb>();
		}

		// holds the input parameter Refinement menu
		List<Refinement> refinements = (List<Refinement>) pRequest.getObjectParameter(TRUCommerceConstants.REFINEMENTS);
		if (refinements == null) {
			refinements = new ArrayList<Refinement>();
		}

		// holds the input parameter Dimension Name
		final String dimensionName = (String) pRequest.getLocalParameter(TRUCommerceConstants.DIMENSION_NAME);
		if (isLoggingDebug()) {
			vlogDebug("Refinement Menu is ........[{0}]....Dimension Name is...........[{1}]", refinements, dimensionName);
		}

		if (refinements != null && refinementCrumbs != null) {

			final Iterator<Refinement> refinementIterator = refinements.iterator();
			final Iterator<RefinementBreadcrumb> crumbIterator = refinementCrumbs.iterator();

			if (!refinements.isEmpty()) {
				while (refinementIterator.hasNext()) {
					final Refinement refinement = refinementIterator.next();
					if (refinement != null) {
						while (crumbIterator.hasNext()) {
							RefinementBreadcrumb refinementCrumb = crumbIterator.next();
							if (refinementCrumb != null && refinementCrumb.getDimensionName().equals(dimensionName)) {
								if (isLoggingDebug()) {
									vlogDebug("selected refinement...{0}", refinementCrumb.getLabel());
								}
								pRequest.setParameter(TRUCommerceConstants.SELECTED_REFINEMENT, refinementCrumb);
								pRequest.serviceLocalParameter(TRUCommerceConstants.SELECTED, pRequest, pResponse);
							}
						}
					}
					if (refinement != null) {
						if (isLoggingDebug()) {
							vlogDebug("nonselected refinement...{0}", refinement.getLabel());
						}
						pRequest.setParameter(TRUCommerceConstants.REFINEMENT, refinement);
						pRequest.serviceLocalParameter(TRUCommerceConstants.NON_SELECTED, pRequest, pResponse);
					}
				}
			} /*
			 * else { if (isLoggingDebug()) { vlogDebug(
			 * "Refinement Menu is Empty for the Dimension {0}. So get the refinement list from Refinement BreadCrumbs"
			 * ,dimensionName); } while (crumbIterator.hasNext()) {
			 * RefinementBreadcrumb refinementCrumb = crumbIterator.next();
			 * if(refinementCrumb != null &&
			 * refinementCrumb.getDimensionName().equals(dimensionName)){ if
			 * (isLoggingDebug()) {
			 * vlogDebug("selected refinement...{0}",refinementCrumb
			 * .getLabel()); }
			 * pRequest.setParameter(DLXEndecaConstants.SELECTED_REFINEMENT,
			 * refinementCrumb);
			 * pRequest.serviceLocalParameter(DLXEndecaConstants.SELECTED,
			 * pRequest, pResponse); } } }
			 */
		} else {
			if (isLoggingDebug()) {
				logDebug("Either Refinement Menu or RefinementCrumbs is null. So rendering the Empty Oparam");
			}
			pRequest.serviceParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("DLXFacetSelectionsDroplet  (service) : END");
		}
	}
}
