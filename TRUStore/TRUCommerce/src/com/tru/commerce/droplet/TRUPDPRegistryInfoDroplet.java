/**
 * 
 */
package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.google.gson.Gson;
import com.tru.common.TRUConstants;
import com.tru.common.TRUStoreConfiguration;
import com.tru.integrations.registry.beans.Categories;
import com.tru.integrations.registry.beans.Error;
import com.tru.integrations.registry.beans.ItemInfoRequest;
import com.tru.integrations.registry.beans.Locale;
import com.tru.integrations.registry.beans.Product;
import com.tru.integrations.registry.beans.RegistryResponse;
import com.tru.integrations.registry.beans.Validation;
import com.tru.integrations.registry.exception.RegistryIntegrationException;
import com.tru.integrations.registry.service.RegistryService;
import com.tru.integrations.registry.util.TRURegistryConstant;
import com.tru.logging.TRUAlertLogger;

/**
 * This droplet is used to get PDP registry info.
 * @author Professional Access.
 * @version 1.0
 */
public class TRUPDPRegistryInfoDroplet extends DynamoServlet {

	private TRUStoreConfiguration mConfiguration;
	private RegistryService mRegistryService;
	
	/** The mAlertLogger */
	private TRUAlertLogger mAlertLogger;
	/**
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
	
	/**
	 * @return the mRegistryService.
	 */
	public RegistryService getRegistryService() {
		return mRegistryService;
	}

	/**
	 * @param pRegistryService the mRegistryService to set.
	 */
	public void setRegistryService(RegistryService pRegistryService) {
		this.mRegistryService = pRegistryService;
	}

	/**
	 * @return the mConfiguration.
	 */
	public TRUStoreConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * @param pConfiguration the mConfiguration to set.
	 */
	public void setConfiguration(TRUStoreConfiguration pConfiguration) {
		this.mConfiguration = pConfiguration;
	}

	/**
	 * This method will get the product related information required for resistry.
	 * 
	 * @param pRequest - reference to DynamoHttpServletRequest object.
	 * @param pResponse - reference to DynamoHttpServletResponse object.
	 * @throws ServletException - if any exception occurs in servicing the request.
	 * @throws IOException - if any exception occurs while writing the response to output stream.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUPDPRegistryInfoDroplet.service method.. ");
		}
		String isWishlist = (String) pRequest.getLocalParameter(TRUConstants.IS_WISHLIST);
		String lastAccessedID = (String) pRequest.getLocalParameter(TRUConstants.LASTACCESSED_ID);
		String registryNumber = (String) pRequest.getLocalParameter(TRUConstants.REGISTRY_NUMBER);
		String productDetails = (String) pRequest.getLocalParameter(TRUConstants.PRODUCT_DETAILS);
		String requestType = (String) pRequest.getLocalParameter(TRUConstants.REQUEST_TYPE);
		String pageName = (String) pRequest.getLocalParameter(TRUConstants.PAGE_NAME);
		ItemInfoRequest itemInfoRequest=populateRegistryRequest(lastAccessedID,productDetails,isWishlist);
		Gson gson =new Gson();
		Validation validation= new Validation();
		Error error=new Error();
		String itemInfoJson=gson.toJson(itemInfoRequest);
		if (isLoggingDebug()) {
			logDebug("ItemInfoJson. "+itemInfoJson);
		}
		RegistryResponse registryResponse=null;
		try {
			registryResponse = getRegistryService().addItemToRegistry(itemInfoRequest);
			
		} catch (RegistryIntegrationException e) {
			vlogError("ERROR: TRUPDPRegistryInfoDroplet.service", e);
			if(registryResponse==null){
				registryResponse= new RegistryResponse();
				registryResponse.setRegistryNumber(registryNumber);
				registryResponse.setValidation(validation);
				registryResponse.getValidation().setError(error);
				registryResponse.getValidation().getError().setInformational(TRUConstants.REGISTRY_ERROR);
				Map<String, String> extra = new ConcurrentHashMap<String, String>();
				if(pageName!=null){
				extra.put(TRUConstants.PAGE_NAME,pageName);
				}
				if(registryResponse.getResponseStatus()!=null){
				     extra.put(TRUConstants.STR_REPPONSE_CODE_FOR_LOG_MONITORING,TRUConstants.STR_ERROR_VALUE_FOR_LOG_MONITORING);
				}else{
					extra.put(TRUConstants.STR_REPPONSE_CODE_FOR_LOG_MONITORING,TRUConstants.STR_ERROR_VALUE_FOR_LOG_MONITORING);
				}
				getAlertLogger().logFailure(TRUConstants.REGISTRY_SERVICE, requestType, extra);
				
			}
			/*pRequest.setParameter(TRUConstants.REGISTRY_ERROR, e.getMessage());
			pRequest.serviceLocalParameter(TRUConstants.ERROR, pRequest, pResponse);
			return;*/
		}
		if (registryResponse != null && registryResponse.getRegistryDetail() != null && registryResponse.getRegistryDetail().getResponseStatus() != null && registryResponse.getRegistryDetail().getResponseStatus().getIndicator() !=null && TRURegistryConstant.RESPONSE_STATUS_SUCCESS.equalsIgnoreCase(registryResponse.getRegistryDetail().getResponseStatus().getIndicator())) {			
			Map<String, String> extra = new ConcurrentHashMap<String, String>();
			if(pageName!=null){
			   extra.put(TRUConstants.PAGE_NAME,pageName);
			}
			if(registryResponse.getResponseStatus()!=null){
				extra.put(TRUConstants.STR_REPPONSE_CODE_FOR_LOG_MONITORING,TRUConstants.STR_ERROR_VALUE_FOR_LOG_MONITORING);
			}else{
				extra.put(TRUConstants.STR_REPPONSE_CODE_FOR_LOG_MONITORING,TRUConstants.STR_ERROR_VALUE_FOR_LOG_MONITORING);
			}
			getAlertLogger().logFailure(TRUConstants.REGISTRY_SERVICE, requestType, extra);
		}
		String jsonResponse=gson.toJson(registryResponse);
		if (isLoggingDebug()) {
			logDebug("jsonResponse. "+jsonResponse);
		}
		pRequest.setParameter(TRUConstants.REGISTRY_RESPONSE, jsonResponse);
		pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
		
		if (isLoggingDebug()) {
			logDebug("END:: TRUPDPRegistryInfoDroplet.service method.. ");
		}
	}

	

	/**
	 * This method is used to set the ItemInfoRequest Object.
	 * 
	 * @param pLastAccessedID - reference to lastAccessedID.
	 * @param pProductDetails - reference to productDetails.
	 * @param pIsWishList - reference to isWishList.
	 * @return itemInfoRequest - reference to itemInfoRequest.
	 */
	private ItemInfoRequest populateRegistryRequest(String pLastAccessedID,String pProductDetails,String pIsWishList) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUPDPRegistryInfoDroplet.populateRegistryRequest method.. ");
		}
		List<Product> productInfoList= new ArrayList<Product>();
		ItemInfoRequest itemInfoRequest = new ItemInfoRequest();
		TRUStoreConfiguration storeCofiguration= getConfiguration();
		Map wishListMap=null;
		if(storeCofiguration!=null && storeCofiguration.getRegistryWishlistkeyMap()!=null){
		//registryWishlistkeyMap=registryType\=B,source\=W,country\=US,language\=en,defaultColor\=0,defaultSize\=0,defaultSknOrigin\=0,nonSpecificIndicator\=0,itemRequested\=0,itemPurchased\=0,purchaseUpdateIndicator\=1,messageId\=0,deletedFlag\=N,timeout=\5000
		wishListMap=storeCofiguration.getRegistryWishlistkeyMap();
		String[] productInfos = pProductDetails.split(TRUConstants.SPLIT_SYMBOL_COMA);
		if (productInfos != null && productInfos.length > TRUConstants.INT_ZERO) {
			for (String productInfoItem : productInfos) {
				if (StringUtils.isNotBlank(productInfoItem)) {
					String[] productProperties = productInfoItem.split(TRUConstants.SPLIT_PIPE);
					if (productProperties != null && productProperties.length >= TRUConstants.THREE) {
						Product tRUPDPProduct = new Product();
						populateProductInformationVO(productProperties,tRUPDPProduct,wishListMap);
						productInfoList.add(tRUPDPProduct);
					}
			}
		}
		}
		}
		itemInfoRequest.setProduct(productInfoList);
		itemInfoRequest.setRegistryNumber(pLastAccessedID);
		
		if(wishListMap!=null){
				if(pIsWishList!=null && pIsWishList.equalsIgnoreCase(TRUConstants.STRING_TRUE)){
				itemInfoRequest.setRegistryType((String)storeCofiguration.getWishlistRegistryType());
				}else if(pIsWishList!=null && pIsWishList.equalsIgnoreCase(TRUConstants.STRING_FALSE)){
					itemInfoRequest.setRegistryType((String) wishListMap.get(TRUConstants.REGISTRY_TYPE));
				}
				else{
				itemInfoRequest.setRegistryType((String)storeCofiguration.getWishlistRegistryType());
				}
			itemInfoRequest.setMessageId((String) wishListMap.get(TRUConstants.MESSAGE_ID));
		}
		Locale locale = new Locale();
		locale.setCountry(TRURegistryConstant.COUNTRY_US);
		locale.setLanguage(TRURegistryConstant.LANGUAGE_EN);
		itemInfoRequest.setLocale(locale);
		if (isLoggingDebug()) {
			logDebug("END:: TRUPDPRegistryInfoDroplet.populateRegistryRequest method.. ");
		}
		return itemInfoRequest;
	}
	
	/**
	 * This method is used to populate the productInfo related VO.
	 * 
	 * @param pProductProperties - reference to productProperties.
	 * @param pProduct - reference to product.
	 * @param pWishListMap - reference to wishListMap.
	 * @return pProduct - reference to product.
	 */
	private Product populateProductInformationVO(String[] pProductProperties,
			Product pProduct, Map pWishListMap) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUPDPRegistryInfoDroplet.populateProductInformationVO method.. ");
		}
		pProduct.setSkn(pProductProperties[TRUConstants.ONE]);
		pProduct.setUpc(pProductProperties[TRUConstants.TWO]);
		pProduct.setUid(pProductProperties[TRUConstants.THREE]);
		if(StringUtils.isNotBlank(pProductProperties[TRUConstants.FOUR])){
			pProduct.setColor((pProductProperties[TRUConstants.FOUR]));
		}else{
			pProduct.setColor((String) pWishListMap.get(TRUConstants.DEFAULT_COLOR));
		}
		if(StringUtils.isNotBlank(pProductProperties[TRUConstants.FIVE])){
			pProduct.setSize(pProductProperties[TRUConstants.FIVE]);
		}else{
			pProduct.setColor((String) pWishListMap.get(TRUConstants.DEFAULT_SIZE));
		}
		
		if(StringUtils.isNotBlank(pProductProperties[TRUConstants.SIX])){
			pProduct.setSknOrigin(pProductProperties[TRUConstants.SIX]);
		}else{
			pProduct.setColor((String) pWishListMap.get(TRUConstants.DEFAULT_SKN_ORIGIN));
		}
		pProduct.setProductDescription(TRUConstants.WHITE_SPACE);
		List<Categories> categoriesList=new ArrayList<Categories>();
		Categories category= new Categories();
		category.setCategoryId(TRUConstants.WHITE_SPACE);
		category.setSubcategoryId(TRUConstants.WHITE_SPACE);
		categoriesList.add(category);
		pProduct.setCategories(categoriesList);
		if(pWishListMap!=null){
			pProduct.setItemPurchased((String) pWishListMap.get(TRUConstants.ITEM_PURCHASED));
			if(StringUtils.isNotBlank(pProductProperties[TRUConstants.SEVEN])){
				pProduct.setItemRequested(pProductProperties[TRUConstants.SEVEN]);
			}else{
				pProduct.setItemRequested((String) pWishListMap.get(TRUConstants.ITEM_REQUESTED));
			}
			pProduct.setNonSpecificIndicator((String) pWishListMap.get(TRUConstants.NON_SPECIFIC_INDICATOR));
			pProduct.setPurchaseUpdateIndicator((String) pWishListMap.get(TRUConstants.PURCHASE_UPDATE_INDICATOR));
			pProduct.setSource((String) pWishListMap.get(TRUConstants.SOURCE));
		}
		//pTRUPDPProduct.setReferenceRegistryIndicator("S");
		if (isLoggingDebug()) {
			logDebug("END:: TRUPDPRegistryInfoDroplet.populateProductInformationVO method.. ");
		}
		return pProduct;
	}
}


