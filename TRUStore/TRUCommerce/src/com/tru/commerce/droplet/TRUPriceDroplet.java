/**
 * TRUPriceDroplet.java
 */
package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.pricing.priceLists.PriceDroplet;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.TRUConstants;

/**
 * TRUPriceDroplet droplet overridden to display the prices of product. Intraday repository will be queried first to get.
 * the price for sku. If there is no price from Intraday repository then PriceLists repository will be queried to get
 * price for sku.
 * 
 * @author Professional Access.
 * @version 1.0
 * 
 */
public class TRUPriceDroplet extends PriceDroplet {

	/** This holds the reference for TRUPricingTools. */
	private TRUPricingTools mPricingTools;

	/**
	 * This method populates the prices to bean.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest.
	 * @param pResponse
	 *            - DynamoHttpServletResponse.
	 * @throws ServletException -ServletException.
	 * @throws IOException - IOException.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPriceDroplet  method: service]");
		}
		double listPrice = TRUConstants.DOUBLE_ZERO;
		double salePrice = TRUConstants.DOUBLE_ZERO;
		Site site = SiteContextManager.getCurrentSite();
		//Need to get from jsp
		String skuId = (String) pRequest.getLocalParameter(TRUConstants.SKU_ID);
		//Need to get from jsp
		String productId = (String) pRequest.getLocalParameter(TRUConstants.PRODUCT_ID);

		// Get prices for SKU		
		listPrice = getPricingTools().getListPriceForSKU(skuId, productId, site);
		salePrice = getPricingTools().getSalePriceForSKU(skuId, productId, site);
		vlogDebug("List Price for SKU : {0} is : {1}", skuId, listPrice);
		vlogDebug("Sale Price for SKU : {0} is : {1}", skuId, salePrice);
		if(listPrice != TRUConstants.DOUBLE_ZERO && salePrice != TRUConstants.DOUBLE_ZERO){
			// This method will validates whether strike through price need to be displayed or not.
			boolean calculateSavings = getPricingTools().validateThresholdPrices(site, listPrice, salePrice);
			// This method will calculate the Savings and updates the values in bean.
			if (calculateSavings) {
				Map<String, Double> savingsMap = getPricingTools().calculateSavings(listPrice, salePrice);
				pRequest.setParameter(TRUConstants.SAVED_AMOUNT, savingsMap.get(TRUConstants.SAVED_AMOUNT));
				pRequest.setParameter(TRUConstants.SAVED_PERCENTAGE, savingsMap.get(TRUConstants.SAVED_PERCENTAGE));
				pRequest.setParameter(TRUConstants.LIST_PRICE, listPrice);
				pRequest.setParameter(TRUConstants.SALE_PRICE, salePrice);
				pRequest.serviceLocalParameter(TRUConstants.OUTPUT_OPARAM_TRUE, pRequest, pResponse);
			} else {
				pRequest.setParameter(TRUConstants.LIST_PRICE, listPrice);
				pRequest.setParameter(TRUConstants.SALE_PRICE, salePrice);
				pRequest.serviceLocalParameter(TRUConstants.OUTPUT_OPARAM_FALSE, pRequest, pResponse);
			}
		}else{
			pRequest.setParameter(TRUConstants.LIST_PRICE, listPrice);
			pRequest.setParameter(TRUConstants.SALE_PRICE, salePrice);
			pRequest.serviceLocalParameter(TRUConstants.EMPTY_OPARAM, pRequest, pResponse);
		}

		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPriceDroplet  method: service]");
		}
	}

	/**
	 * This method will return pricingTools.
	 *
	 * @return the pricingTools.
	 */
	public TRUPricingTools getPricingTools() {
		return mPricingTools;
	}

	/**
	 * This method will set mpricingTools with pPricingTools.
	 *
	 * @param pPricingTools the mpricingTools to set.
	 */
	public void setPricingTools(TRUPricingTools pPricingTools) {
		mPricingTools = pPricingTools;
	}

}
