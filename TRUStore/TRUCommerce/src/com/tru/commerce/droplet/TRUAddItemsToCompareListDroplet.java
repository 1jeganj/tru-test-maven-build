
package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.comparison.TRUProductComparisonList;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.common.TRUConstants;
import com.tru.common.TRUStoreConfiguration;


/**
 * <p>
 * This droplet will be invoked when user add or remove product to compare list or comes through a book mark to the product comparison page. In the jsp, this
 * will be invoked if the product list is empty.This droplet will add the Product Ids from the request parameter to the
 * ProductList component. Also it sets the category Id(passed as request parameter) to the productList's current
 * category id.
 * </p>
 * <b>Input Parameters</b><br>
 * &nbsp;&nbsp;&nbsp;<code>productList</code> - ProductComparisonList.<br>
 * &nbsp;&nbsp;&nbsp;<code>categoryId</code> - String.<br>
 * &nbsp;&nbsp;&nbsp;<code>productIds</code> - String.<br>
 * 
 * <b>Output Parameters.</b><br>
 * &nbsp;&nbsp;&nbsp;<b>No output Parameters available.</b><br>
 * 
 * <b>Open Parameters.</b><br>
 * &nbsp;&nbsp;&nbsp;<b>No open Parameters available.</b><br>
 * 
 * <br>
 * <b>Usage:</b><br>
 * &lt;dspel:droplet name="/com/tru/commerce/droplet/TRUAddItemsToCompareListDroplet"&gt;<br>
 * &lt;dspel:param name="productList" bean="/atg/commerce/catalog/comparison/ProductList" /&gt;<br>
 * &lt;/dspel:droplet&gt;
 * @author PA
 * @version 1.0
 */
public class TRUAddItemsToCompareListDroplet extends DynamoServlet {

	/**
	 * Property to hold StoreConfiguration.
	 */
	private TRUStoreConfiguration mStoreConfiguration;

	/**
	 * Property to hold mCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;
	
	/**
	 * Property to hold mProductComparisonList.
	 */
	private TRUProductComparisonList mProductComparisonList;
	/**
	 * 
	 * @return mProductComparisonList.
	 */
	public TRUProductComparisonList getProductComparisonList() {
		return mProductComparisonList;
	}
	
	/**
	 * Sets the productComparisonList.
	 * 
	 * @param pProductComparisonList
	 *            the productComparisonList to set.
	 */
	public void setProductComparisonList(TRUProductComparisonList pProductComparisonList) {
		this.mProductComparisonList = pProductComparisonList;
	}

	/**
	 * Gets the storeConfiguration.
	 * 
	 * @return the storeConfiguration.
	 */
	public TRUStoreConfiguration getStoreConfiguration() {
		return mStoreConfiguration;
	}

	/**
	 * Sets the storeConfiguration.
	 * 
	 * @param pStoreConfiguration
	 *            the storeConfiguration to set.
	 */
	public void setStoreConfiguration(TRUStoreConfiguration pStoreConfiguration) {
		mStoreConfiguration = pStoreConfiguration;
	}

	/**
	 * Getter of catalogTools.
	 * 
	 * @return the catalogTools.
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Setter of catalogTools.
	 * 
	 * @param pCatalogTools
	 *            the catalogTools to set.
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * This method will get the parameters productIds, categoryId and ProductList. It then splits the productIds by pipe
	 * delimiter and then invokes addProductsToCompareList() method to add the products to the comparison list. <br>
	 * 
	 * @param pRequest
	 *            - Holds the DynamoHttpServletRequest.
	 * @param pResponse
	 *            - Holds the DynamoHttpServletResponse.
	 * @throws ServletException
	 *             - ServletException if an error occurs.
	 * @throws IOException
	 *             - IOException if an error occurs.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUAddItemsToCompareListDroplet.service method.. ");
		}
		// getting productId parameter from jsp or browser url
		String productId = (String) pRequest.getLocalParameter(getStoreConfiguration().getProductIdParameterName());
		String skuId = (String) pRequest.getLocalParameter(getStoreConfiguration().getSkuIdParameterName());
		// getting categoryId parameter from jsp or browser url
		String categoryId =(String) pRequest.getLocalParameter(getStoreConfiguration().getCategoryIdParameterName());
		String imageURL =(String) pRequest.getLocalParameter(getStoreConfiguration().getImageUrlParameterName());
		String stylizedItem=(String) pRequest.getLocalParameter(getStoreConfiguration().getStylizedItem());
		TRUProductComparisonList productComparisonList =(TRUProductComparisonList) pRequest.getObjectParameter(TRUCommerceConstants.PRODUCT_LIST);
		String requestType = (String) pRequest.getLocalParameter(TRUCommerceConstants.REQUEST_TYPE);
		TRUCatalogProperties catalogProperties= (TRUCatalogProperties) getCatalogTools().getCatalogProperties();
		if (isLoggingDebug()) {
			vlogDebug("productId : {0}, categoryId : {1}, requestType{2}", productId, categoryId, requestType);
		}
		if (!StringUtils.isBlank(productId) && !StringUtils.isBlank(categoryId) && productComparisonList != null) {
			int listSize = productComparisonList.size();
			List productList= productComparisonList.getItems();
			RepositoryItem category=null;
			Object bean = null;
			if (isLoggingDebug()) {
				vlogDebug("listSize {0}: ", listSize);
			}
			int count= TRUCommerceConstants.INT_ZERO;
			for (int index = 0; index < listSize; index++) {
				// Fetching the bean from the pProductItems.
				bean = productList.get(index);
				
					try {
						category = (RepositoryItem) DynamicBeans
								.getSubPropertyValue(bean,catalogProperties.getCategoryItemName());
						if(category  != null && category.getRepositoryId().equals(categoryId)){
							count++;
						}
					} catch (PropertyNotFoundException e) {
						vlogError("Error:Comparison product list:", e);
					}
			}			
			if (count >= TRUCommerceConstants.INT_FOUR && !TRUCommerceConstants.REMOVE_REQUEST_TYPE.equals(requestType)) {
				if (isLoggingDebug()) {
					vlogDebug("Error:Comparison product list has more than 3 items");
				}
				pRequest.serviceLocalParameter(TRUCommerceConstants.ERROR_OPARAM, pRequest, pResponse);
				return;
			}
			// calls method to add product to compare list if request type is addProduct
			if (productComparisonList.size() == 0 && TRUCommerceConstants.ADD_REQUEST_TYPE.equals(requestType)) {
				addProductsToCompareList(productId, productComparisonList,categoryId,  skuId ,imageURL,stylizedItem, pRequest, pResponse);
			} else if (TRUCommerceConstants.ADD_REQUEST_TYPE.equals(requestType)) {
				// calls method to add product to compare list from url if comparison list size is 0
				addProductsToCompareList(productId, productComparisonList, categoryId,  skuId ,imageURL,stylizedItem, pRequest, pResponse);
			}
			else if (TRUCommerceConstants.REMOVE_REQUEST_TYPE.equals(requestType)) {
				// calls method to remove product to compare list if request type is removeProduct
				removeProductsfromCompareList(productId, productComparisonList, categoryId,  skuId,stylizedItem, pRequest, pResponse);
			}
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUAddItemsToCompareListDroplet.service method.. ");
		}
	}

	/**
	 * This method will remove the given product id from the CompareList.
	 * 
	 * @param pProductId - reference to productId.
	 * @param pProductComparisonList - reference to productComparisonList.
	 * @param pCategoryId - reference to categoryId.
	 * @param pSkuId - reference to skuId.
	 * @param pStylizedItem - reference to stylizedItem.
	 * @param pRequest - reference to request.
	 * @param pResponse - reference to response.
	 */
	private void removeProductsfromCompareList(String pProductId,
			TRUProductComparisonList pProductComparisonList,
			String pCategoryId,  String pSkuId,String pStylizedItem,DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUAddItemsToCompareListDroplet.removeProductsfromCompareList method..");
			logDebug("BEGIN:: TRUAddItemsToCompareListDroplet.removeProductsfromCompareList method..pStylizedItem"+pStylizedItem);
		}
		String compareProductCookie;
		String newCompareProductCookie=TRUConstants.EMPTY;
		Cookie createcompareProductCookie;
		try {
			// used to remove product from comparison list
			if(pStylizedItem!=null && pStylizedItem.equalsIgnoreCase(TRUConstants.TRUE) && getStoreConfiguration().isEnableCompareImageURL()){
				pProductComparisonList.remove(pProductId, pCategoryId,null, null);
			}else {
				pProductComparisonList.remove(pProductId, pCategoryId, pSkuId, null);
			}
			compareProductCookie = pRequest.getCookieParameter(TRUConstants.PRODUCT_COMPARE_COOKIE+TRUCommerceConstants.UNDER_SCORE+pCategoryId);
			if (compareProductCookie != null) {
				if(compareProductCookie.contains(TRUConstants.TILDA_STRING)){
					String[] SplitCookie = compareProductCookie.split(TRUConstants.BACKSLASH_TILDA_STRING);
					String[] pairs;
					int arryLength= SplitCookie.length;
					for (int index = 0; index < arryLength; index++) {
						pairs = SplitCookie[index].split(TRUConstants.SPLIT_PIPE);
						StringBuffer compareProductCookieBuffer=new StringBuffer();
						if (!pSkuId.equals(pairs[TRUConstants.TWO])) {
							compareProductCookieBuffer.append(newCompareProductCookie);
							if(newCompareProductCookie != TRUConstants.EMPTY || StringUtils.isNotBlank(newCompareProductCookie)){
								compareProductCookieBuffer.append(TRUConstants.TILDA_STRING);
							}
							compareProductCookieBuffer.append(pairs[TRUConstants.ZERO]);
							compareProductCookieBuffer.append(TRUConstants.PIPE_STRING);
							compareProductCookieBuffer.append(pairs[TRUConstants.ONE]);
							compareProductCookieBuffer.append(TRUConstants.PIPE_STRING);
							compareProductCookieBuffer.append(pairs[TRUConstants.TWO]);
							compareProductCookieBuffer.append(TRUConstants.PIPE_STRING);
							compareProductCookieBuffer.append(pairs[TRUConstants.THREE]);
							compareProductCookieBuffer.append(TRUConstants.PIPE_STRING);
							compareProductCookieBuffer.append(pairs[TRUConstants.FOUR]);
							newCompareProductCookie = compareProductCookieBuffer.toString();
						}/*else{
							
						}*/
					}
					// newCompareProductCookie = newCompareProductCookie.substring(TRUConstants.ZERO, newCompareProductCookie.length()-TRUConstants.ONE);
				}
				createcompareProductCookie = new Cookie(TRUConstants.PRODUCT_COMPARE_COOKIE+TRUCommerceConstants.UNDER_SCORE+pCategoryId,newCompareProductCookie);
				createcompareProductCookie.setMaxAge(TRUConstants.INT_MINUS_ONE);
				createcompareProductCookie.setPath(TRUConstants.PATH);
				createcompareProductCookie.setSecure(TRUConstants.FALSE_FLAG);
				pResponse.addCookie(createcompareProductCookie);
			}
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				logError("Error while removing selected product from the CompareList ",repositoryException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUAddItemsToCompareListDroplet.removeProductsfromCompareList method..");
		}
	}

	/**
	 * This method will add the given product id to the CompareList.
	 * 
	 * @param pProductId - reference to productId.
	 * @param pProductComparisonList - reference to productComparisonList.
	 * @param pCategoryID - reference to categoryID.
	 * @param pSkuId - reference to skuId.
	 * @param pImageURL - reference to imageURL.
	 * @param pStylizedItem - reference to stylizedItem.
	 * @param pRequest - reference to request.
	 * @param pResponse - reference to response.
	 */
	private void addProductsToCompareList(String pProductId,
			TRUProductComparisonList pProductComparisonList,
			String pCategoryID,  String pSkuId,String pImageURL,String pStylizedItem,
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUAddItemsToCompareListDroplet.addProductsToCompareList method..");
		}
		// used to set current category to comparison list
		if (!StringUtils.isBlank(pCategoryID)) {
			pProductComparisonList.setCurrentCategoryId(pCategoryID);
		}
		String compareProduct;
		String compareProductCookie;
		Cookie createcompareProductCookie = null;

		if (isLoggingDebug()) {
			vlogDebug("ProductId : {0}", pProductId);
		}
		try {
			// used to add product from comparison list. fromUrl will be true in
			// case of book marked compare page
			// url
			// we are checking if that product item is active ,then only the
			// product id is added to compare list
			if (pProductComparisonList != null && !pProductComparisonList.contains(pProductId,pCategoryID, pSkuId, null)) {
				if (isLoggingDebug()) {
					logDebug("List Size before adding.."+pProductComparisonList.size());
				}
				List<RepositoryItem> activeSkuList = new ArrayList<RepositoryItem>();
				List<RepositoryItem> skuList = new ArrayList<RepositoryItem>();
				RepositoryItem skuItem = getCatalogTools().findSKU(pSkuId);
				if (skuItem != null) {
					skuList.add(skuItem);
				}
				activeSkuList = getCatalogTools().getActiveChildSKUs(skuList,Boolean.TRUE,TRUCommerceConstants.TYPE_PRODUCT,null,pProductId);
				if(pStylizedItem!=null && pStylizedItem.equalsIgnoreCase(TRUConstants.TRUE) && activeSkuList != null && !activeSkuList.isEmpty() && getStoreConfiguration().isEnableCompareImageURL() ){
					pProductComparisonList.add(pProductId, pCategoryID, null,null);
				}
				else if (activeSkuList != null && !activeSkuList.isEmpty()) {
					pProductComparisonList.add(pProductId, pCategoryID, pSkuId,null);
				}
				if (isLoggingDebug()) {
					logDebug("List Size after adding.."+pProductComparisonList.size());
				}
			}
			StringBuffer compareProductBuffer= new StringBuffer();
			compareProductBuffer.append(pProductId);
			compareProductBuffer.append(TRUConstants.PIPE_STRING);
			compareProductBuffer.append(pCategoryID);
			compareProductBuffer.append(TRUConstants.PIPE_STRING);		
			compareProductBuffer.append(pSkuId);
			compareProductBuffer.append(TRUConstants.PIPE_STRING);
			if(pImageURL!=null ){
				compareProductBuffer.append(pImageURL);
			}else {
				compareProductBuffer.append(getStoreConfiguration().getAkamaiNoImageURL());
			}
			compareProductBuffer.append(TRUConstants.PIPE_STRING);
			if(pStylizedItem!=null){
				compareProductBuffer.append(pStylizedItem);
			}else{
				compareProductBuffer.append(Boolean.FALSE);
			}
			compareProduct = compareProductBuffer.toString();
			compareProductCookie = pRequest.getCookieParameter(TRUConstants.PRODUCT_COMPARE_COOKIE+TRUCommerceConstants.UNDER_SCORE+pCategoryID);
			
			if (!StringUtils.isBlank(compareProductCookie) && !compareProductCookie.contains(compareProduct)) {
				StringBuffer compareProductCookieBuffer= new StringBuffer();
				compareProductCookieBuffer.append(compareProductCookie);
				compareProductCookieBuffer.append(TRUConstants.TILDA_STRING);
				compareProductCookieBuffer.append(compareProduct);
					compareProductCookie =  compareProductCookieBuffer.toString();
					createcompareProductCookie = new Cookie(TRUConstants.PRODUCT_COMPARE_COOKIE+TRUCommerceConstants.UNDER_SCORE+pCategoryID,compareProductCookie);
			} else {
				createcompareProductCookie = new Cookie(TRUConstants.PRODUCT_COMPARE_COOKIE+TRUCommerceConstants.UNDER_SCORE+pCategoryID,compareProduct);
			}
			if(createcompareProductCookie instanceof Cookie){
				createcompareProductCookie.setMaxAge(TRUConstants.INT_MINUS_ONE);
				createcompareProductCookie.setPath(TRUConstants.PATH);
				createcompareProductCookie.setSecure(TRUConstants.FALSE_FLAG);
				pResponse.addCookie(createcompareProductCookie);
			}
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				logError("Error while adding selected product to the CompareList ",repositoryException);
			}
		}

		if (isLoggingDebug()) {
			logDebug("END:: TRUAddItemsToCompareListDroplet.addProductsToCompareList method..");
		}
	}


}
