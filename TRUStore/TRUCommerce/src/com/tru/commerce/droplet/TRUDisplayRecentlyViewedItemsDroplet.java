/*
 * @(#)TRUDisplayRecentlyViewedItemsDroplet.java	
 *
 * Copyright 2015 PA, Inc. All rights reserved.
 * PA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

import com.tru.cache.TRUProductCache;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.vo.ProductInfoRequestVO;
import com.tru.commerce.catalog.vo.ProductInfoVO;
import com.tru.commerce.exception.TRUCommerceException;
import com.tru.userprofiling.TRUPropertyManager;



/**
 * This class used to render recently viewed items.
 * 
 *
 * <br>
 * <br>
 * <b>Input Parameters.</b><br>
 * &nbsp;&nbsp;&nbsp;<code>profile</code> - Profile<br>
 * 
 * <b>Output Parameters.</b><br>
 * &nbsp;&nbsp;&nbsp;<code>productInfo</code> - ProductInfoVO<br>
 * 
 * <b>Open Parameters.</b><br>
 * &nbsp;&nbsp;&nbsp;<b>output - If user has viewed some product.</b>
 * &nbsp;&nbsp;&nbsp;<b>empty - If user has not viewed any product.</b>
 * 
 * <br>
 * <b>Usage:</b><br>
 * &lt;dspel:droplet name="/com/tru/commerce/droplet/TRUDisplayRecentlyViewedItemsDroplet"&gt;<br>
 * &lt;dspel:param name="profile" &gt;<br>
 * &lt;dspel:oparam name="output"&gt;<br>
 * &lt;dspel:oparam name="empty"&gt;<br>
 * &lt;/dspel:oparam&gt;<br>
 * &lt;/dspel:droplet&gt;
 * 
 * @author PA
 * @version 1.0
 */
public class TRUDisplayRecentlyViewedItemsDroplet extends DynamoServlet {

	/**
	 * Property to hold TRUProductCache.
	 */
	private TRUProductCache mProductCache;
	
	/**
	 * Property to hold PropertyManager.
	 */
	private TRUPropertyManager mPropertyManager;

	/**
	 * This method is used to render recently viewed items.
	 * 
	 * @param pRequest - Reference to DynamoHttpServletRequest.
	 * @param pResponse - Reference to DynamoHttpServletResponse.
	 * @throws ServletException - throws ServletException.
	 * @throws IOException - throws IOException.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
 throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUDisplayRecentlyViewedItemsDroplet.service method..");
		}
		final Profile profile = (Profile) pRequest.getObjectParameter(TRUCommerceConstants.PROFILE);
		boolean isRecentlyViewedItemsAvailable = false;
		// Fetching available items from the profile object
		@SuppressWarnings("unchecked")
		final List<String> siteRecentlyViewedProductIds = (List<String>) profile.getPropertyValue(getPropertyManager()
				.getRecentlyViewedPropertyName());

		if (siteRecentlyViewedProductIds != null && !siteRecentlyViewedProductIds.isEmpty()) {
			if (isLoggingDebug()) {
				vlogDebug("siteRecentlyViewedProductIds : {0}", siteRecentlyViewedProductIds);
			}
			// Creating request VO to get the item from the cache object
			Collections.reverse(siteRecentlyViewedProductIds);
			ProductInfoRequestVO requestVO = null;
			ProductInfoVO productInfo = null;
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_START, pRequest, pResponse);
			for (String recentlyViewedProductId : siteRecentlyViewedProductIds) {
				requestVO = new ProductInfoRequestVO();
				requestVO.setProductId(recentlyViewedProductId);
				// Getting the product related information from the cache
				try {
					productInfo = (ProductInfoVO) getProductCache().get(requestVO);
					if (productInfo != null) {
						pRequest.setParameter(TRUCommerceConstants.PRODUCT_INFO, productInfo);
						pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
						isRecentlyViewedItemsAvailable = true;
					}
				} catch (TRUCommerceException commerceException) {
					if (isLoggingError()) {
						logError("TRUCommerceException in TRUDisplayRecentlyViewedItemsDroplet.service method.", commerceException);
					}
				}
			}
		}

		if (!isRecentlyViewedItemsAvailable) {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUDisplayRecentlyViewedItemsDroplet.service method. recentlyViewedItemsAvailable : {0}",
					isRecentlyViewedItemsAvailable);
		}
	}

	/**
	 * Gets the productCache.
	 * 
	 * @return the productCache.
	 */
	public TRUProductCache getProductCache() {
		return mProductCache;
	}

	/**
	 * Sets the productCache.
	 * 
	 * @param pProductCache the productCache to set.
	 */
	public void setProductCache(TRUProductCache pProductCache) {
		mProductCache = pProductCache;
	}

	/**
	 * Gets the property manager.
	 *
	 * @return the propertyManager.
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 *
	 * @param pPropertyManager the propertyManager to set.
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}
	
	
}
