package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.cache.TRUCollectionProductCache;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogManager;
import com.tru.commerce.catalog.vo.CollectionProductInfoRequestVO;
import com.tru.commerce.catalog.vo.CollectionProductInfoVO;
import com.tru.commerce.catalog.vo.ProductInfoJsonVO;
import com.tru.commerce.catalog.vo.ProductInfoVO;
import com.tru.commerce.catalog.vo.SKUInfoVO;
import com.tru.commerce.collection.TRUCollectionProductUtils;
import com.tru.commerce.exception.TRUCommerceException;
import com.tru.utils.TRUStoreUtils;

/**
/**
 * This class used to render collection product related information in PDP page.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUCollectionProductInfoLookupDroplet extends DynamoServlet {

	/**
	 * Property to hold TRUCollectionProductCache.
	 */
	private TRUCollectionProductCache mCollectionProductCache;
	
	/**
	 * This property hold reference of DimensionValueCacheTools.
	 */
	private DimensionValueCacheTools mDimensionValueCacheTools;
	
	/**
	 * Property to hold TRUCatalogManager.
	 */
	private TRUCatalogManager mCatalogManager;
	
	/**
	 * Property to hold TRUCatalogManager
	 */
	private TRUCollectionProductUtils mCollectionUtils;
	
	
	/**
	 * This method will get the product related information required to be populated into PDP page.
	 * 
	 * @param pRequest - reference to DynamoHttpServletRequest object.
	 * @param pResponse - reference to DynamoHttpServletResponse object.
	 * @throws ServletException - if any exception occurs in servicing the request.
	 * @throws IOException - if any exception occurs while writing the response to output stream.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCollectionProductInfoLookupDroplet.service method.. ");
		}
		Object collectionObj = pRequest.getLocalParameter(TRUCommerceConstants.COLLECTION_ID);
		String cookieValue = pRequest.getCookieParameter(TRUCommerceConstants.FAV_STORE);
		String collectionId =  null;
		List<ProductInfoVO> productInfoList=null;
		Map productInfoJsonMap=null;
		if(collectionObj!=null){
			collectionId =collectionObj.toString();
		}
		if (isLoggingDebug()) {
			vlogDebug("CollectionId :", collectionId);
		}
		boolean isCollectionProductInfoAvailable = false;
		boolean isCollectionProductInfoJsonAvailable = false;
		CollectionProductInfoVO collectionProductInfo = null;
		if (!StringUtils.isBlank(collectionId)) {
			// Creating request VO to get the item from the cache object
			CollectionProductInfoRequestVO requestVO = new CollectionProductInfoRequestVO();
			requestVO.setCollectionId(collectionId);
			
			try {
				// Getting the collection product related information from the cache
 				collectionProductInfo = (CollectionProductInfoVO) getCollectionProductCache().get(requestVO);
 				if (collectionProductInfo == null ||collectionProductInfo.getActiveProductInfoList()==null) {
					// If product is empty rendering empty output parameter
					pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
					return;
				}
					if (isLoggingDebug()) {
						vlogDebug("CollectionProductInfo.getCollectionId():::",collectionProductInfo.getCollectionId());
					}
					TRUStoreUtils storeUtils= new TRUStoreUtils();
					List<ProductInfoVO> productInfos = collectionProductInfo.getActiveProductInfoList();
					String storeId=storeUtils.getStoreIdFromCoockie(cookieValue);
					 productInfoList = new ArrayList<ProductInfoVO>();
					 productInfoJsonMap=new HashMap<>();
					
					if (productInfos != null) {
 						for (ProductInfoVO productInfo : productInfos) {
  							if (productInfo.getChildSKUsList() != null && !productInfo.getChildSKUsList().isEmpty()) {
 								List<SKUInfoVO> childSkus= productInfo.getChildSKUsList();
								String productType = productInfo.getColorSizeVariantsAvailableStatus();
 								if(childSkus != null && !childSkus.isEmpty() && childSkus.size() > TRUCommerceConstants.ONE && 
										productType.equals(TRUCommerceConstants.NO_COLOR_SIZE_AVAILABLE)){
								for (SKUInfoVO childSku : childSkus) {
									ProductInfoVO productVo = getCollectionUtils().populateAssortementProductInfo(productInfo);
									productVo.setDefaultSKU(childSku);
									productInfoList.add(productVo);	
								}
								}else{
								SKUInfoVO defaultSku = getCatalogManager().getCatalogTools().getDefaultSkuInfoAndPopulateInvetoryInformation(storeId, productInfo);
								productInfo.setDefaultSKU(defaultSku);
 								productInfoList.add(productInfo);
								}								 
							}							
  						}
						for (ProductInfoVO tempProductInfoVO : productInfoList) {
							ProductInfoJsonVO productInfoJsonVO =null;
							productInfoJsonVO = getCatalogManager().getCatalogTools().populateProductJsonVO(tempProductInfoVO);
							if (productInfoJsonVO != null) {
								String productVoJson = getCatalogManager().getCatalogTools().convertVOToJsonObject(productInfoJsonVO);
								 if (isLoggingDebug()) {
									 vlogDebug("ProductVoJson::",productVoJson);
								}
								productInfoJsonMap.put(tempProductInfoVO.getProductId(),productVoJson);
							}
						}
					}
					 if(productInfoList != null && !productInfoList.isEmpty())
					 {
						 collectionProductInfo.setProductInfoList(productInfoList);
						 isCollectionProductInfoAvailable =true;
						if (isLoggingDebug()) {
						 vlogDebug("isCollectionProductInfoAvailable:",isCollectionProductInfoAvailable);
						}
					 }
					 if(productInfoJsonMap != null && !productInfoJsonMap.isEmpty())
					 {
						 collectionProductInfo.setProductInfoJsonMap(productInfoJsonMap);
						 isCollectionProductInfoJsonAvailable =true;
						 if (isLoggingDebug()) {
							 vlogDebug("isCollectionProductInfoJsonAvailable:",isCollectionProductInfoJsonAvailable);
						}
					 }
					pRequest.setParameter(TRUCommerceConstants.COLLECTION_PRODUCT_INFO, collectionProductInfo);
					pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
				
			} catch (TRUCommerceException commerceException) {
				if (isLoggingError()) {	
					logError("TRUCommerceException in TRUProductInfoLookupDroplet.service method..", commerceException);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUCollectionProductInfoLookupDroplet.service method.. {0}",isCollectionProductInfoAvailable);
		}
	}

	/**
	 * Gets the collectionProductCache
	 * 
	 * @return the collectionProductCache
	 */
	public TRUCollectionProductCache getCollectionProductCache() {
		return mCollectionProductCache;
	}

	/**
	 * Sets the collectionProductCache
	 * 
	 * @param pCollectionProductCache the collectionProductCache to set
	 */
	public void setCollectionProductCache(TRUCollectionProductCache pCollectionProductCache) {
		mCollectionProductCache = pCollectionProductCache;
	}
	
	/**
	 * Gets the dimensionValueCacheTools
	 * 
	 * @return the dimensionValueCacheTools
	 */
	public DimensionValueCacheTools getDimensionValueCacheTools() {
		return mDimensionValueCacheTools;
	}

	/**
	 * Sets the dimensionValueCacheTools
	 * 
	 * @param pDimensionValueCacheTools the dimensionValueCacheTools to set
	 */
	public void setDimensionValueCacheTools(DimensionValueCacheTools pDimensionValueCacheTools) {
		mDimensionValueCacheTools = pDimensionValueCacheTools;
	}

	/**
	 * @return the TRUCatalogManager
	 */
	public TRUCatalogManager getCatalogManager() {
		return mCatalogManager;
	}

	/**
	 * @param pCatalogManager the TRUCatalogManager to set
	 */
	public void setCatalogManager(TRUCatalogManager pCatalogManager) {
		mCatalogManager = pCatalogManager;
	}

	/**
	 * @return the collectionUtils
	 */
	public TRUCollectionProductUtils getCollectionUtils() {
		return mCollectionUtils;
	}

	/**
	 * @param pCollectionUtils the collectionUtils to set
	 */
	public void setCollectionUtils(TRUCollectionProductUtils pCollectionUtils) {
		mCollectionUtils = pCollectionUtils;
	}
	
	
}
