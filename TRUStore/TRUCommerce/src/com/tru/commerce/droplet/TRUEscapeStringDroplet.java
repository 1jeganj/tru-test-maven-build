/*
 * @(#)TRUEscapeStringDroplet.java	1.0
 *
 * Copyright PA, Inc. All rights reserved.
 * PA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.tru.commerce.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.endeca.infront.shaded.org.apache.commons.lang.StringEscapeUtils;
import com.tru.commerce.TRUCommerceConstants;


/**
 * This droplet is used to escape the data.
 * 
 * <br>
 * <br>
 * <b>Input Parameters</b><br>
 * &nbsp;&nbsp;&nbsp;<code>data</code> - Data to be escaped<br>
 * 
 * <b>Output Parameters</b><br>
 * &nbsp;&nbsp;&nbsp;<code>enocdeData</code> - Escaped data<br>
 * 
 * <b>Open Parameters</b><br>
 * &nbsp;&nbsp;&nbsp;<b>output - If the requested data not empty</b> <br>
 * &nbsp;&nbsp;&nbsp;<b>empty - If the requested data is empty.</b>
 * 
 * <br>
 * <b>Usage:</b><br>
 * &lt;dspel:droplet name="/com/TRU/commerce/droplet/TRUEscapeStringDroplet"&gt;<br>
 * &lt;dspel:param name="data" param="data"/&gt;<br>
 * &lt;dspel:oparam name="output"&gt;<br>
 * &lt;/dspel:oparam&gt;<br>
 * &lt;dspel:oparam name="empty"&gt;<br>
 * &lt;/dspel:oparam&gt;<br>
 * &lt;/dspel:droplet&gt;
 * 
 * @author PA
 * @version 1.0
 */
public class TRUEscapeStringDroplet extends DynamoServlet {

	/**
	 * This method will get the data from the JSP as a parameter and it will escape the special character.
	 * 
	 * @param pRequest - reference to DynamoHttpServletRequest object.
	 * @param pResponse - reference to DynamoHttpServletResponse object.
	 * @throws ServletException - if any exception occurs in servicing the request.
	 * @throws IOException - if any exception occurs while writing the response to output stream.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException,
			IOException {
		final String data = (String) pRequest.getLocalParameter(TRUCommerceConstants.DATA);
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUEscapeStringDroplet.service method..  Data : {0}", data);
		}
		if (!StringUtils.isBlank(data)) {
			final String escapeData =StringEscapeUtils.escapeJava(data);
			if (isLoggingDebug()) {
				vlogDebug("escapeData : {0}", escapeData);
			}
			pRequest.setParameter(TRUCommerceConstants.ESCAPE_DATA, escapeData);
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		} else {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUEscapeStringDroplet.service method.. ");
		}
	}
}
