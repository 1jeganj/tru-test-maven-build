package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;

import atg.commerce.order.Order;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUUserSession;
import com.tru.radial.commerce.payment.paypal.TRUPaypalConstants;
import com.tru.radial.commerce.payment.paypal.util.TRUPayPalConstants;
/**
 * This class is TRUCheckPaypalExpressCheckoutDroplet.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCheckPaypalExpressCheckoutDroplet extends DynamoServlet {
	/**
	 * holds USER_SESSION.
	 */
	private static final ParameterName USER_SESSION = ParameterName.getParameterName(TRUConstants.USER_SESSION);
	
	/**
	 * holds ORDER.
	 */
	private static final ParameterName ORDER = ParameterName.getParameterName(TRUConstants.PAYPAL_ORDER);
	
	/** Hold TRUOrderManager Instance {@link TRUOrderManager}.**/
	private TRUOrderManager mOrderManager;
	
	/**
	 * This property hold reference for TRUConfiguration.
	 */
	private TRUConfiguration mTruConfiguration;


	/**
	 * This method is used to check the paypal token as well payer id in user session.
	 * 
	 * @param pRequest
	 *            - reference to DynamoHttpServletRequest object.
	 * @param pResponse
	 *            - reference to DynamoHttpServletResponse object.
	 * @throws ServletException
	 *             - if any exception occurs in servicing the request.
	 * @throws IOException
	 *             - if any exception occurs while writing the response to output stream.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCheckPaypalExpressCheckoutDroplet.service method.. ");
		}
	boolean isAlreadyToken = false;
		final TRUUserSession userSession = (TRUUserSession) pRequest.getObjectParameter(USER_SESSION);
		final TRUOrderImpl order = (TRUOrderImpl) pRequest.getObjectParameter(ORDER);
		if(userSession == null || order == null) {
			if (isLoggingDebug()) {
				logDebug("userSession is null and/or Order is null");
			}
			return;
		}
		isAlreadyToken = isPaypalTokenAlreadyCaptured(userSession,order);
		
		pRequest.setParameter(TRUPaypalConstants.PAYPAL_TOKEN_ALREADY_TAKEN, isAlreadyToken);
		pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);

		if (isLoggingDebug()) {
			logDebug("END:: TRUCheckPaypalExpressCheckoutDroplet.service method.. ");
		}
	}
	
	/**
	 * This method will check if PAYPAL Token already captured. 
	 *
	 * @param pUserSession userSession.
	 * @param pOrderImpl Order
	 * @return isAlreadyToken if PAYPAL Token already captured.
	 * @throws ServletException ServletException.
	 * @throws IOException IOException.
	 */
	private boolean isPaypalTokenAlreadyCaptured(TRUUserSession pUserSession, Order pOrderImpl) throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start of TRUCheckPaypalExpressCheckoutDroplet : isPaypalTokenAlreadyCaptured() method ");
		}
		final String myHandleMethod = TRUPayPalConstants.PAYPAL_DROPLET_CHECK_EXPRESS_CHECKOUT;
		if (PerformanceMonitor.isEnabled()){
			PerformanceMonitor.startOperation(myHandleMethod);
		}
		boolean isAlreadyToken = false;
		String payPalToken = null;
		String payPalPayerId = null;
		/*String tokenTimeStamp = null;
		final TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();*/
		// fetch PAYPAL details map from session
		final Map<String, String> paypalPaymentDetailsMap = pUserSession.getPaypalPaymentDetailsMap();
		if (paypalPaymentDetailsMap == null || paypalPaymentDetailsMap.isEmpty()) {
			return false;
		}
		payPalToken = paypalPaymentDetailsMap.get(TRUPayPalConstants.PAYPAL_TOKEN);
		payPalPayerId = paypalPaymentDetailsMap.get(TRUPayPalConstants.PAYER_ID);
		//tokenTimeStamp = paypalPaymentDetailsMap.get(TRUPayPalConstants.PAYPAL_TOKEN_TIMESTAMP);
		/*if (!StringUtils.isBlank(payPalToken) && !StringUtils.isBlank(tokenTimeStamp)) {
			DateFormat formatter = null;
			Date tokenDate = null;
			formatter = new SimpleDateFormat(TRUPaypalConstants.ORDER_DATE_FORMAT,Locale.US);
			try {
				tokenDate = (Date) formatter.parse(tokenTimeStamp);
			} catch (ParseException pE) {
				if (isLoggingError()) {
					logError("ParseException in isPaypalTokenAlreadyCaptured " + "while parsing string" + tokenTimeStamp, pE);
				}
			}
			if (tokenDate == null) {
				return false;
			}
			final long diff = orderManager.getDateDiff(tokenDate, new Date(), TimeUnit.MINUTES);
			if (diff > Long.valueOf(getTruConfiguration().getPayPalTokenExpiryTime())) {
				if (isLoggingDebug()) {
					logDebug("Paypal Token captured has already expired, need to authenticate again.");
				}
				return false;
			}
			if(!StringUtils.isBlank(payPalPayerId)) {
				isAlreadyToken = true;
				//Start TUW-56572
				((TRUOrderImpl)pOrderImpl).setPaypalToken(payPalToken);
				//End TUW-56572
			}
		}*/
		if(!StringUtils.isBlank(payPalPayerId)) {
			isAlreadyToken = true;
			//Start TUW-56572
			((TRUOrderImpl)pOrderImpl).setPaypalToken(payPalToken);
			//End TUW-56572
		}
		if (PerformanceMonitor.isEnabled()){
			PerformanceMonitor.endOperation(myHandleMethod);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCheckPaypalExpressCheckoutDroplet::@method::isPaypalTokenAlreadyCaptured() : END");
		}
		return isAlreadyToken;
	}

	/**
	 * Get a diff between two dates.
	 * @param pDate1 the oldest date.
	 * @param pDate2 the newest date.
	 * @param pTimeUnit the unit in which you want the diff.
	 * @return the diff value, in the provided unit.
	 */
	public static long getDateDiff(Date pDate1, Date pDate2, TimeUnit pTimeUnit) {
	   final long diffInMillies = pDate2.getTime() - pDate1.getTime();
	    return pTimeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Gets the order manager.
	 *
	 * @return the mOrderManager.
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * Sets the order manager.
	 *
	 * @param pOrderManager the pOrderManager to set.
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		this.mOrderManager = pOrderManager;
	}

	/**
	 * Gets the tru configuration.
	 *
	 * @return the mTruConfiguration.
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * Sets the tru configuration.
	 *
	 * @param pTruConfiguration the pTruConfiguration to set.
	 */
	public void setTruConfiguration(TRUConfiguration pTruConfiguration) {
		this.mTruConfiguration = pTruConfiguration;
	}	
}
