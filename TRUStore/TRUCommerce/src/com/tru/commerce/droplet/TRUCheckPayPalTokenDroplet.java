package com.tru.commerce.droplet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;

import atg.commerce.order.processor.BillingAddrValidator;
import atg.commerce.order.processor.BillingAddrValidatorImpl;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUUserSession;
import com.tru.radial.commerce.payment.paypal.TRUPaypalConstants;
import com.tru.radial.commerce.payment.paypal.util.TRUPayPalConstants;
/**
 * This class is TRUCheckPayPalTokenDroplet.
 * @author PA.
 * @version 1.0
 */
public class TRUCheckPayPalTokenDroplet extends DynamoServlet {
	
	/**
	 * holds USER_SESSION.
	 */
	private static final ParameterName USER_SESSION = ParameterName.getParameterName(TRUConstants.USER_SESSION);

	/**
	 * holds ORDER.
	 */
	private static final ParameterName ORDER = ParameterName.getParameterName(TRUConstants.PAYPAL_ORDER);
	
	/** Hold TRUOrderManager Instance {@link TRUOrderManager}.**/
	private TRUOrderManager mOrderManager;
	
	/**
	 * This property hold reference for TRUConfiguration.
	 */
	private TRUConfiguration mTruConfiguration;
	
	/**
	 * property to hold mBillingAddressValidator.
	 */
	private BillingAddrValidator mBillingAddressValidator;
	
	/**
	 * Gets the billing address validator.
	 *
	 * @return the mBillingAddressValidator.
	 */	
	public BillingAddrValidator getBillingAddressValidator() {
		return mBillingAddressValidator;
	}

	/**
	 * Sets the billing address validator.
	 *
	 * @param pBillingAddressValidator the mBillingAddressValidator to set.
	 */	
	public void setBillingAddressValidator(BillingAddrValidator pBillingAddressValidator) {
		this.mBillingAddressValidator = pBillingAddressValidator;
	}


	/**
	 * This method is used to check the paypal token as well payer id in user session.
	 * 
	 * @param pRequest
	 *            - reference to DynamoHttpServletRequest object.
	 * @param pResponse
	 *            - reference to DynamoHttpServletResponse object.
	 * @throws ServletException
	 *             - if any exception occurs in servicing the request.
	 * @throws IOException
	 *             - if any exception occurs while writing the response to output stream.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCheckPayPalTokenDroplet.service method.. ");
		}
		boolean isAlreadyToken = false;
		final TRUUserSession userSession = (TRUUserSession) pRequest.getObjectParameter(USER_SESSION);
		final 	TRUOrderImpl order = (TRUOrderImpl) pRequest.getObjectParameter(ORDER);
		if (userSession != null && order != null) {
			isAlreadyToken = isPaypalTokenAlreadyCaptured(userSession, order);
		}
		pRequest.setParameter(TRUPaypalConstants.PAYPAL_TOKEN_ALREADY_TAKEN, isAlreadyToken);
		pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);

		if (isLoggingDebug()) {
			logDebug("END:: TRUCheckPayPalTokenDroplet.service method.. ");
		}
	}
	
	/**
	 * This method will check if PAYPAL Token already captured. 
	 * @param pUserSession userSession.
	 * @param pOrder order.
	 * @throws ServletException ServletException.
	 * @throws IOException IOException.
	 * @return isAlreadyToken if PAYPAL Token already captured. 
	 */

	private boolean isPaypalTokenAlreadyCaptured(TRUUserSession pUserSession, TRUOrderImpl pOrder) throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUCheckPayPalTokenDroplet : isPaypalTokenAlreadyCaptured() method ");
		}
		boolean isAlreadyToken = false;
		String payPalToken = null;
		String payPalPayerId = null;
		String tokenTimeStamp = null;
		
		//final TRUOrderManager orderManager = (TRUOrderManager)getOrderManager();
		// fetch PAYPAL details map from session
		final Map<String, String> paypalPaymentDetailsMap = pUserSession.getPaypalPaymentDetailsMap();
		if(paypalPaymentDetailsMap == null || paypalPaymentDetailsMap.isEmpty()) {
			return false;
		}
		payPalToken = paypalPaymentDetailsMap.get(TRUPayPalConstants.PAYPAL_TOKEN);
		payPalPayerId = paypalPaymentDetailsMap.get(TRUPayPalConstants.PAYER_ID);
		tokenTimeStamp = paypalPaymentDetailsMap.get(TRUPayPalConstants.PAYPAL_TOKEN_TIMESTAMP);
		 isAlreadyToken = paypalTokenCapture(payPalToken,payPalPayerId,tokenTimeStamp,pOrder);
		if (isLoggingDebug()) {
			vlogDebug("End of TRUCheckPayPalTokenDroplet : isPaypalTokenAlreadyCaptured() method ");
		}
		return isAlreadyToken;
	}
	
	
	/**
	 * @param pPayPalToken - PaypalToken
	 * @param pPayPalPayerId - PayPalPayerId
	 * @param pTokenTimeStamp - TokenTimeStamp
	 * @param pOrder - Order
	 * @return Boolean - IsAlreadyToken
	 */
	private boolean paypalTokenCapture(String pPayPalToken, String pPayPalPayerId,
			String pTokenTimeStamp,TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUCheckPayPalTokenDroplet : paypalTokenCapture() method ");
		}
		boolean isAlreadyToken = false;
		long diff =0;
		final TRUOrderManager orderManager = (TRUOrderManager)getOrderManager();
		
		if(!StringUtils.isBlank(pPayPalToken) && !StringUtils.isBlank(pPayPalPayerId) && !StringUtils.isBlank(pTokenTimeStamp)) {
			DateFormat formatter = null ;
			Date tokenDate = null;
			formatter = new SimpleDateFormat(TRUPaypalConstants.ORDER_DATE_FORMAT,Locale.US);
			try {
				tokenDate = (Date)formatter.parse(pTokenTimeStamp);
			} catch (ParseException pE) {
				if (isLoggingError()) {
					logError("ParseException in isPaypalTokenAlreadyCaptured " +
							"while parsing string"+pTokenTimeStamp,pE);
				}
			}
			if(tokenDate == null) {
				return false;
			}
			diff = orderManager.getDateDiff(tokenDate, new Date(), TimeUnit.MINUTES);
			if( diff > Long.valueOf(getTruConfiguration().getPayPalTokenExpiryTime())) {
				if (isLoggingDebug()) {
					logDebug("Paypal Token captured has already expired, need to authenticate again.");
				}
				return false;
			}
			isAlreadyToken = isAlreadyTokenExist(orderManager, pOrder ,pPayPalToken ,pPayPalPayerId);	
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUCheckPayPalTokenDroplet : paypalTokenCapture() method ");
		}
		return isAlreadyToken;		
		
	}

	/**
	 * Get a diff between two dates.
	 * @param pDate1 the oldest date.
	 * @param pDate2 the newest date.
	 * @param pTimeUnit the unit in which you want the diff.
	 * @return the diff value, in the provided unit.
	 */
	public static long getDateDiff(Date pDate1, Date pDate2, TimeUnit pTimeUnit) {
	    final long diffInMillies = pDate2.getTime() - pDate1.getTime();
	    return pTimeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Gets the order manager.
	 *
	 * @return the mOrderManager.
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * Sets the order manager.
	 *
	 * @param pOrderManager the pOrderManager to set.
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		this.mOrderManager = pOrderManager;
	}

	/**
	 * Gets the tru configuration.
	 *
	 * @return the mTruConfiguration.
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * Sets the tru configuration.
	 *
	 * @param pTruConfiguration the pTruConfiguration to set.
	 */
	public void setTruConfiguration(TRUConfiguration pTruConfiguration) {
		this.mTruConfiguration = pTruConfiguration;
	}
	
	/**
	 * This method will check if Token already Exist.
	 *
	 * @param pOrderManager order manager.
	 * @param pOrder order.
	 * @param pPayPalToken the pay pal token
	 * @param pPayPalPayerId the pay pal payer id
	 * @return lIsAlreadyToken if PAYPAL Token already exist.
	 */
	private boolean isAlreadyTokenExist(TRUOrderManager pOrderManager,TRUOrderImpl pOrder ,String pPayPalToken ,String pPayPalPayerId){
		// fetch PAYPAL payment group
		boolean lIsAlreadyToken = Boolean.FALSE;
		final TRUPayPal payPalPG = pOrderManager.fetchPayPalPaymentGroupForOrder(pOrder);
		if (payPalPG != null && getBillingAddressValidator() != null) {
			final BillingAddrValidator bav = getBillingAddressValidator();
			if (bav instanceof BillingAddrValidatorImpl) {
				final Collection addressCollection = ((BillingAddrValidatorImpl)bav).validateAddress(payPalPG.getBillingAddress());
				if (addressCollection.isEmpty()) {
					if (StringUtils.isEmpty(payPalPG.getToken())) {
						payPalPG.setToken(pPayPalToken);
					}
					if (StringUtils.isEmpty(payPalPG.getPayerId())) {
						payPalPG.setPayerId(pPayPalPayerId);
					}
					lIsAlreadyToken = true;
				}
			}
		}
		return lIsAlreadyToken;
	}
}
