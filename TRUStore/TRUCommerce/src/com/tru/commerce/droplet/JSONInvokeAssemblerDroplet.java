/**
 * 
 */
package com.tru.commerce.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.endeca.assembler.AssemblerTools;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.endeca.infront.assembler.AssemblerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.ContentSlotConfig;
import com.endeca.infront.serialization.JsonSerializer;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.common.TRUConstants;

/**
 * This droplet is used to get the assembled content based on the visual type ahead collection sent as input param.
 * The Class JSONInvokeAssemblerDroplet.
 *
 * @author PA
 * @version 1.0
 */
public class JSONInvokeAssemblerDroplet extends DynamoServlet{
	/** The m assembler tools. */
	private AssemblerTools mAssemblerTools;


	
	/**
	 * Gets the assembler tools.
	 *
	 * @return the assembler tools
	 */
	public AssemblerTools getAssemblerTools() {
		return mAssemblerTools;
	}

	/**
	 * Sets the assembler tools.
	 *
	 * @param pAssemblerTools the new assembler tools
	 */
	public void setAssemblerTools(AssemblerTools pAssemblerTools) {
		mAssemblerTools = pAssemblerTools;
	}

	/**
	 * Takes param as visual type ahead collection and assembles the content to be sent.
	 * 
	 * @param pRequest
	 *            - holds the reference for DynamoHttpServletRequest
	 * @param pResponse
	 *            - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException
	 *             - ServletException
	 * @throws IOException
	 *             - IOException
	 */
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Entered the Class:[JSONInvokeAssemblerDroplet] Method:[service] ");
		}
		/* Set the visual type ahead collection*/
		String contentCollection = TRUConstants.TYPE_AHEAD_COLLECTION;
		/*Create a empty content*/
		ContentItem contentItem = new ContentSlotConfig(contentCollection, TRUConstants.THREE);

		
		if(contentItem != null){
			ContentItem responseContentItem = null;
			try {
				responseContentItem = getAssemblerTools().invokeAssembler(contentItem);
				if (isLoggingDebug()) {
					logDebug("Class:[JSONInvokeAssemblerDroplet] Method:[service] responseContentItem:"+responseContentItem);
				}
				if(responseContentItem != null){
					pResponse.setContentType(TRUConstants.APPLICATION_JSON); 
					pResponse.setCharacterEncoding(TRUCommerceConstants.UTF_ENCODE);
					(new JsonSerializer(pResponse.getWriter())).write(responseContentItem);
				}
			} catch (AssemblerException assemblerException) {
				if (isLoggingError()) {
					logError("AssemblerException assemblerException:: ", assemblerException);
				}
			}
		}
	}
}