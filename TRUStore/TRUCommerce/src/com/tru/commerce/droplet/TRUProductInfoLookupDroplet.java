package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.cache.TRUProductCache;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogManager;
import com.tru.commerce.catalog.vo.ProductInfoJsonVO;
import com.tru.commerce.catalog.vo.ProductInfoRequestVO;
import com.tru.commerce.catalog.vo.ProductInfoVO;
import com.tru.commerce.catalog.vo.SKUInfoVO;
import com.tru.commerce.exception.TRUCommerceException;
import com.tru.common.TRUConstants;
import com.tru.utils.TRUStoreUtils;

/**
/**
 * This class used to render product related information in PDP page.
 * 
 * @author Professional Access.
 * @version 1.0
 * 
 */
public class TRUProductInfoLookupDroplet extends DynamoServlet {

	

	/**
	 * Property to hold TRUProductCache.
	 */
	private TRUProductCache mProductCache;
	
	/**
	 * This property hold reference of DimensionValueCacheTools.
	 */
	private DimensionValueCacheTools mDimensionValueCacheTools;
	
	/**
	 * Property to hold TRUCatalogManager.
	 */
	private TRUCatalogManager mCatalogManager;
	
	/**
	 * Property to hold mOnlinePidEnable.
	 */
	private boolean mOnlinePidEnable;
	
	/**
	 * Property to hold mEnableBrandURL.
	 */
	private boolean mEnableBrandURL;
	
	/**
	 * Property to hold mStoreUtils.
	 */
	private TRUStoreUtils mStoreUtils;
	

	/**
	 * This method will get the product related information required to be populated into PDP page.
	 * 
	 * @param pRequest - reference to DynamoHttpServletRequest object.
	 * @param pResponse - reference to DynamoHttpServletResponse object.
	 * @throws ServletException - if any exception occurs in servicing the request.
	 * @throws IOException - if any exception occurs while writing the response to output stream.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUProductInfoLookupDroplet.service method.. ");
		}
		String cookieValue = pRequest.getCookieParameter(TRUCommerceConstants.FAV_STORE);
		String onlinePid = (String) pRequest.getLocalParameter(TRUCommerceConstants.ONLINEPID);
		Object productIdObj = pRequest.getLocalParameter(TRUCommerceConstants.PRODUCT_ID);
		String selectedColor = (String) pRequest.getLocalParameter(TRUCommerceConstants.SELECTED_COLOR_PDP);
		String selectedSize = (String) pRequest.getLocalParameter(TRUCommerceConstants.SELECTED_SIZE_PDP);
		String selectedSkuId = (String) pRequest.getLocalParameter(TRUCommerceConstants.SELECTED_SKUID_PDP);
		String productId =  null;
		if(productIdObj!=null){
			productId =productIdObj.toString();
		}
		if (isLoggingDebug()) {
			vlogDebug("productId : {0} onlinePid::{1}", productId,onlinePid);
		}
		boolean isProductAvailable = false;
		ProductInfoVO productInfo = null;
		if(StringUtils.isBlank(productId) && StringUtils.isNotBlank(onlinePid)){
			productId=getCatalogManager().getCatalogTools().getProductIdFromOnlinePID(onlinePid);
		}
		if (!StringUtils.isBlank(productId)) {
			// Creating request VO to get the item from the cache object
			ProductInfoRequestVO requestVO = new ProductInfoRequestVO();
			requestVO.setProductId(productId);
			try {
				// Getting the product related information from the cache
				productInfo = (ProductInfoVO) getProductCache().get(requestVO);
				if (productInfo != null && productInfo.isActive()) {
					//TRUStoreUtils storeUtils = new TRUStoreUtils();
					 String storeId = getStoreUtils().getStoreIdFromCoockie(cookieValue);
					 String productType=productInfo.getColorSizeVariantsAvailableStatus();
					 SKUInfoVO defaultSku= getCatalogManager().getCatalogTools().getDefaultSkuInfoAndPopulateInvetoryInformation(storeId, productInfo);
					 if(isOnlinePidEnable()){
						 List<SKUInfoVO> childSkus=productInfo.getChildSKUsList();
						 if(StringUtils.isNotBlank(onlinePid) && TRUCommerceConstants.NO_COLOR_SIZE_AVAILABLE.equalsIgnoreCase(productType) && childSkus!=null && childSkus.size()>TRUConstants._1){
				    	 defaultSku=getCatalogManager().getCatalogTools().findDefaultSkuUsingOnlinePID(onlinePid,childSkus);
				     }
					 }
					 if((StringUtils.isNotEmpty(selectedColor) ||
							 StringUtils.isNotEmpty(selectedSize))
							 && StringUtils.isNotEmpty(selectedSkuId)
							 && !TRUCommerceConstants.NO_COLOR_SIZE_AVAILABLE.equalsIgnoreCase(productType)){
						 defaultSku=getCatalogManager().getCatalogTools().getDefaultFromSelectedSku(productInfo,selectedSkuId);
					 }					 
					if(defaultSku!=null){
						productInfo.setDefaultSKU(defaultSku);
						if(isEnableBrandURL()){
							if (isLoggingDebug()) {
								vlogDebug("brand :",productInfo.getDefaultSKU().getBrandName());
							}
							if (!StringUtils.isBlank(productInfo.getDefaultSKU().getBrandName()) ) {
								String brandPageURL = getBrandPageUrl(productInfo.getDefaultSKU().getBrandName());
								if(!StringUtils.isBlank(brandPageURL)){
									productInfo.getDefaultSKU().setBrandLandingURL(brandPageURL);
								}
							}
						}
						 ProductInfoJsonVO productInfoJsonVO = getCatalogManager().getCatalogTools().populateProductJsonVO(productInfo);
						 if(productInfoJsonVO!=null){
								String productVoJson =getCatalogManager().getCatalogTools().convertVOToJsonObject(productInfoJsonVO);
								pRequest.setParameter(TRUCommerceConstants.PRODUCTVO_JSON, productVoJson);
								if (isLoggingDebug()) {
									vlogDebug("TRUProductInfoLookupDroplet.service :::productVoJson::.. {0}", productVoJson);
								}
							}
					pRequest.setParameter(TRUCommerceConstants.PRODUCT_INFO, productInfo);
					pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
					isProductAvailable =true;
					}else{
						pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
					}
					
				}
			} catch (TRUCommerceException commerceException) {
				if (isLoggingError()) {	
					logError("TRUCommerceException in TRUProductInfoLookupDroplet.service method..", commerceException);
				}
			}
		}
		if (productInfo==null ||(productInfo!=null && !productInfo.isActive())) {
			// If product is empty rendering empty output parameter
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
			if (isLoggingDebug()) {
				vlogDebug("TRUProductInfoLookupDroplet.service method...Setting empty oparam");
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUProductInfoLookupDroplet.service method..isProductAvailable {0}", isProductAvailable);
		}
	}

	

	/**
	 * Gets the productCache.
	 * 
	 * @return the productCache.
	 */
	public TRUProductCache getProductCache() {
		return mProductCache;
	}

	/**
	 * Sets the productCache.
	 * 
	 * @param pProductCache the productCache to set.
	 */
	public void setProductCache(TRUProductCache pProductCache) {
		mProductCache = pProductCache;
	}
	/**
	 * Gets the enableBrandURL.
	 * 
	 * @return the mEnableBrandURL.
	 */
	public boolean isEnableBrandURL() {
		return mEnableBrandURL;
	}

	/**
	 * Sets the enableBrandURL.
	 * 
	 * @param pEnableBrandURL the enableBrandURL to set.
	 */
	public void setEnableBrandURL(boolean pEnableBrandURL) {
		mEnableBrandURL = pEnableBrandURL;
	}


	
	/**
	 * Gets the dimensionValueCacheTools.
	 * 
	 * @return the dimensionValueCacheTools.
	 */
	public DimensionValueCacheTools getDimensionValueCacheTools() {
		return mDimensionValueCacheTools;
	}

	/**
	 * Sets the dimensionValueCacheTools.
	 * 
	 * @param pDimensionValueCacheTools the dimensionValueCacheTools to set.
	 */
	public void setDimensionValueCacheTools(DimensionValueCacheTools pDimensionValueCacheTools) {
		mDimensionValueCacheTools = pDimensionValueCacheTools;
	}
	
	/**
	 * This method takes brand name as parameter and returns the brand url.
	 * 
	 * @param pBrandName - the brand name parameter.
	 * @return brandUrl - the brand Url.
	 */
	public String getBrandPageUrl(String pBrandName) {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUProductInfoLookupDroplet.getBrandPageUrl method.. {0}", pBrandName);
		}
		String brandUrl = null;
		if (!StringUtils.isBlank(pBrandName)) {
			DimensionValueCacheObject cache = null;
			StringBuffer brandKey = new StringBuffer();
			brandKey.append(TRUCommerceConstants.PDP_BRAND);
			brandKey.append(TRUCommerceConstants.UNDER_SCORE);
			brandKey.append(pBrandName);
			if (brandKey != null) {
				cache = getDimensionValueCacheTools().get(brandKey.toString(), null);
				if (cache != null) {
					brandUrl = cache.getUrl();
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUProductInfoLookupDroplet.getBrandPageUrl method.. {0}", brandUrl);
		}
		return brandUrl;
	}

	/**
	 * @return the TRUCatalogManager.
	 */
	public TRUCatalogManager getCatalogManager() {
		return mCatalogManager;
	}

	/**
	 * @param pCatalogManager the TRUCatalogManager to set.
	 */
	public void setCatalogManager(TRUCatalogManager pCatalogManager) {
		mCatalogManager = pCatalogManager;
	}

	
	/**
	 * @return the onlinePidEnable.
	 */
	public boolean isOnlinePidEnable() {
		return mOnlinePidEnable;
	}



	/**
	 * @param pOnlinePidEnable the onlinePidEnable to set.
	 */
	public void setOnlinePidEnable(boolean pOnlinePidEnable) {
		mOnlinePidEnable = pOnlinePidEnable;
	}



	/**
	 * @return the mStoreUtils
	 */
	public TRUStoreUtils getStoreUtils() {
		return mStoreUtils;
	}



	/**
	 * @param pStoreUtils the mStoreUtils to set
	 */
	public void setStoreUtils(TRUStoreUtils pStoreUtils) {
		this.mStoreUtils = pStoreUtils;
	}


}
