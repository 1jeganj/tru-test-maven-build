package com.tru.commerce.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.order.Order;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.pricing.TRUPriceProperties;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.TRUConstants;

/**
 * This TRUHidePriceDroplet is used to display the prices for products whose priceDisplay property value is 'Price on Cart'.
 *  when it is Added to cart otherwise price will not be displayed.
 * 
 * @author Professional Access.
 * @version 1.0
 * 
 */
public class TRUHidePriceDroplet extends DynamoServlet {

	/**
	 * Holds reference for TRUPricingTools.
	 */
	private TRUPricingTools mPricingTools;

	/**
	 * This method is to check whether the added displaying product is added to cart or not.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest.
	 * @param pResponse
	 *            - DynamoHttpServletResponse.
	 * @throws ServletException - servlet exception.
	 * @throws IOException - IO exception. 
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		// Need to get from jsp
		String skuId = null;
		String productId = null;
		TRUPriceProperties priceProperties = getPricingTools().getPriceProperties();
		Object skuObject = pRequest.getLocalParameter(priceProperties.getSKUIdPropertyName());
		if(skuObject != null) {
			skuId = skuObject.toString();
		}
		Object productObject = pRequest.getLocalParameter(priceProperties.getProductIdPropertyName());
		if(skuObject != null) {
			productId = productObject.toString();
		}
		Order order = (Order) pRequest.getLocalParameter(priceProperties.getOrderPropertyName());
		//check whether the passed(current PDP) product id is available in order or not
		boolean isAdded = getPricingTools().compareCommItemWithSkuId(order, skuId, productId);
		if (isAdded) {
			pRequest.setParameter(TRUConstants.IS_SKU_ADDED_TO_ORDER, isAdded);
			pRequest.serviceLocalParameter(TRUConstants.OUTPUT_OPARAM_TRUE, pRequest, pResponse);
		} else {
			pRequest.setParameter(TRUConstants.IS_SKU_ADDED_TO_ORDER, isAdded);
			pRequest.serviceLocalParameter(TRUConstants.OUTPUT_OPARAM_FALSE, pRequest, pResponse);
		}
	}
	
	/**
	 * This method will return pricingTools.
	 *
	 * @return the pricingTools.
	 */
	public TRUPricingTools getPricingTools() {
		return mPricingTools;
	}
	/**
	 * This method will set mpricingTools with pPricingTools.
	 *
	 * @param pPricingTools the mpricingTools to set.
	 */
	public void setPricingTools(TRUPricingTools pPricingTools) {
		mPricingTools = pPricingTools;
	}

}
