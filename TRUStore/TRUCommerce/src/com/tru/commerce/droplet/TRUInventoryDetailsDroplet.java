package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.google.gson.Gson;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogManager;
import com.tru.commerce.inventory.TRUInventoryInfo;
import com.tru.commerce.locations.TRUStoreLocatorTools;
import com.tru.common.TRUConstants;
import com.tru.storelocator.tol.PARALStoreLocatorTools;

/**
 * The Class TRUInventoryDetailsDroplet.
 */
public class TRUInventoryDetailsDroplet extends DynamoServlet {

	/** The m catalog manager. */
	private TRUCatalogManager mCatalogManager;

	/**
	 *  Holds the mStoreLocatorTools.
	 */
	private PARALStoreLocatorTools mStoreLocatorTools; 



	/**
	 * Gets the catalog manager.
	 * 
	 * @return the catalog manager
	 */
	public TRUCatalogManager getCatalogManager() {
		return mCatalogManager;
	}

	/**
	 * Sets the catalog manager.
	 * 
	 * @param pCatalogManager
	 *            the new catalog manager
	 */
	public void setCatalogManager(TRUCatalogManager pCatalogManager) {
		mCatalogManager = pCatalogManager;
	}
	/**
	 * @return the storeLocatorTools
	 */
	public PARALStoreLocatorTools getStoreLocatorTools() {
		return mStoreLocatorTools;
	}

	/**
	 * @param pStoreLocatorTools the mStoreLocatorTools to set
	 */
	public void setStoreLocatorTools(PARALStoreLocatorTools pStoreLocatorTools) {
		this.mStoreLocatorTools = pStoreLocatorTools;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * atg.servlet.DynamoServlet#service(atg.servlet.DynamoHttpServletRequest,
	 * atg.servlet.DynamoHttpServletResponse)
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {

		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(this.getClass().getName(),
					TRUCommerceConstants.SKU_PROMOTION_DROPLET);
		}
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUInventoryDetailsDroplet;  method: service]");
			vlogDebug("Request : {0},response : {1}", pRequest, pResponse);
		}
		String skuId = null;
		Map<String, TRUInventoryInfo> inventoryMap = new HashMap<String, TRUInventoryInfo>();
		Map<String, TRUInventoryInfo> storeinventoryMap = new HashMap<String, TRUInventoryInfo>();
		Map<String, String> addToCartDetailMap = new HashMap<String, String>();
		final String cookieValue = pRequest.getCookieParameter(TRUCommerceConstants.FAV_STORE);
		final String storeId = getStoreIdFromCoockie(cookieValue);
		Double storeItemWareHouseLocationId =null;
		String storeItemWareHouseLocationIdAsString=null;
		if (StringUtils.isNotBlank(storeId)){
			TRUStoreLocatorTools locatorTools = ((TRUStoreLocatorTools)getStoreLocatorTools());
			RepositoryItem storeItem = locatorTools.getStoreItem(storeId);
			storeItemWareHouseLocationId   = (Double)locatorTools.getPropertyValue(storeItem, locatorTools.getLocationPropertyManager().getWarehouseLocationCodePropertyName());
			if(storeItemWareHouseLocationId !=null)      
			{
				storeItemWareHouseLocationIdAsString =Long.toString(storeItemWareHouseLocationId.longValue());
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit into [Class: TRUInventoryDetailsDroplet  method: storeId]"+storeId);
		}

		skuId = ((String) pRequest.getLocalParameter(TRUCommerceConstants.SKU_STRING));
		String skuStoreEligibleArray = (String) pRequest.getParameter(TRUCommerceConstants.SKU_STORE_ELIGIBLE_ARRAY);		
		Map<String,String> SkuStoreMap = new HashMap<String,String>();
		List<String> skuIdList = new ArrayList<String>();

		if(StringUtils.isNotEmpty(skuStoreEligibleArray)){
			String[] skuStoreArray = skuStoreEligibleArray.split(TRUCommerceConstants.PIPELINE_SEPERATOR);
			for (String skuStoreVal : skuStoreArray) {
				String[] split = skuStoreVal.split(TRUCommerceConstants.COLON_SYMBOL);

				skuIdList.add(split[TRUConstants.ZERO]);
				if(split.length == TRUConstants.TWO){
					SkuStoreMap.put(split[TRUConstants.ZERO], split[TRUConstants.ONE]);
				}
			}
		}
		if (StringUtils.isNotEmpty(skuId) && !SkuStoreMap.isEmpty()) {
					List<String> skuList = new ArrayList<String>();
						for (String ids : skuIdList) {
					  	String flagvalue = SkuStoreMap.get(ids);
						String[] splitforchannelavilable=null;
						boolean inStoreOnly=false;
						if (StringUtils.isNotBlank(flagvalue)){
							splitforchannelavilable = flagvalue.split(TRUCommerceConstants.DOT_SEPERATOR);
						}
						if (splitforchannelavilable!=null && splitforchannelavilable.length>TRUConstants.TWO)
						{
							if (TRUCommerceConstants.IN_STORE_ONLY.equalsIgnoreCase(splitforchannelavilable[TRUConstants.TWO]))
							{
								inStoreOnly=true;
							}
				
                           }
						if (! inStoreOnly)
						{
							skuList.add(ids);
						}
													}
			
			try{
				inventoryMap = getCatalogManager().getCatalogTools().getCoherenceInventoryManager().queryBulkS2HStockLevelMap(skuList);
			}
			catch(Exception e)
			{
				if (isLoggingError()) {
					vlogError("inventory Exception : ", e);
				}
			}
			List<String> storeList=new ArrayList<String>();
			for (String id : skuIdList) {
				String value = SkuStoreMap.get(id);
				String[] split=null;
				if (StringUtils.isNotBlank(value)){
					split = value.split(TRUCommerceConstants.DOT_SEPERATOR);
				}
				boolean s2sflag=false;
				boolean ispuFlag=false;
				if (split!=null)
				{

					if (TRUCommerceConstants.YES_STRING.equalsIgnoreCase(split[TRUConstants.ZERO]) || TRUCommerceConstants.YES.equalsIgnoreCase(split[TRUConstants.ZERO])||TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(split[TRUConstants.ONE])||TRUConstants.ONE_STR.equalsIgnoreCase(split[TRUConstants.ZERO]))

					{
						s2sflag=true;
					}
					if (TRUCommerceConstants.YES.equalsIgnoreCase(split[TRUConstants.ONE]) || TRUCommerceConstants.YES_STRING.equalsIgnoreCase(split[TRUConstants.ONE]) ||TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(split[TRUConstants.ONE]))
					{
						ispuFlag=true;
					}

				}
				if(inventoryMap.containsKey(id) && inventoryMap.get(id).getAtcStatus() !=  TRUCommerceConstants.COHERENCE_OUT_OF_STOCK ) {
					addToCartDetailMap.put(id,TRUCommerceConstants.INVENTORY_AVAILABLE);
				} else if((!s2sflag && !ispuFlag)) {
					addToCartDetailMap.put(id,TRUCommerceConstants.DISABLE);
				} else if(StringUtils.isEmpty(storeId)) {

					//open show store
					addToCartDetailMap.put(id,TRUCommerceConstants.STORE_AVAILABLE);
				} else {
					//create new list
					//add only this sku id to that list
					storeList.add(id);

					if (isLoggingDebug()) {
						vlogDebug("printing storlist{0}",storeList);
					}

				}
			}
			List<String> storeSkuList = new ArrayList<String>();
			if (!storeList.isEmpty())
			{
				storeSkuList.addAll(storeList);
				try{
					storeinventoryMap=getCatalogManager().getCatalogTools().getCoherenceInventoryManager().queryBulkStoreStockLevelMap(storeList,storeId,storeItemWareHouseLocationIdAsString);
				}
				catch(Exception e)
				{
					if (isLoggingError()) {
						vlogError("inventory Exception : ", e);
					}
				}
			}
			if (storeinventoryMap!=null && ! storeSkuList.isEmpty()){

				if (isLoggingDebug()) {
					logDebug("entering  into store inventry iteration");
				}
				int  statuscode=TRUCommerceConstants.COHERENCE_OUT_OF_STOCK;
				Iterator itr=	storeSkuList.iterator();
							while (itr.hasNext()){
			
								Object objNext = itr.next();
								if(objNext != null){	
									if (!storeinventoryMap.isEmpty()){
										TRUInventoryInfo storeObject = storeinventoryMap.get(objNext);
										if (isLoggingDebug()) {
											vlogDebug("store object{0}",storeObject);
										}
			
										if(storeObject != null){
											
											//inventoryMap.get(id).getAtcStatus() !=  TRUCommerceConstants.COHERENCE_OUT_OF_STOCK
											statuscode = storeObject.getAtcStatus();
			
											if (isLoggingDebug()) {
												vlogDebug("statuscode{0}",statuscode);
											}
										}
									}
									if (statuscode != TRUCommerceConstants.COHERENCE_OUT_OF_STOCK){
										addToCartDetailMap.put(objNext.toString(), TRUCommerceConstants.STORE_INVENTORY_AVAILABLE);
									}
									else {
										addToCartDetailMap.put(objNext.toString(),TRUCommerceConstants.DISABLE);
									}
								}
							}


				if (isLoggingDebug()) {
					logDebug("exiting from   store inventry iteration");
				}

			}
			if (!addToCartDetailMap.isEmpty()) {
				Gson gson = new Gson();
				String addToCartDetailStr = gson.toJson(addToCartDetailMap);
				pRequest.setParameter(TRUConstants.ADD_TO_CART_DETAIL_MAP,addToCartDetailStr);
				pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest,pResponse);
			}
		}

		if (isLoggingDebug()) {
			logDebug("Exit into [Class: TRUInventoryDetailsDroplet  method: service]");
		}
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(this.getClass().getName(),
					TRUCommerceConstants.SKU_PROMOTION_DROPLET);
		}
	}

	/**
	 * @param pCookieValue String
	 * @return the storeId
	 */
	public String getStoreIdFromCoockie(String pCookieValue){
		String storeId=null;
		if(!StringUtils.isBlank(pCookieValue)){
			String[] favStoreArry = StringUtils.splitStringAtCharacter(pCookieValue, TRUConstants.PIPE_STRING_CHAR);
			if(favStoreArry != null && favStoreArry.length >= TRUConstants.TWO){
				storeId = favStoreArry[TRUConstants.INTEGER_NUMBER_ONE];
			}
		}
		return storeId;
	}

}
