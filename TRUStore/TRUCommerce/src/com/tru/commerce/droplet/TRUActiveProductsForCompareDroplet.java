
package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;

/**
 * This droplet is used to get the active product from the supplied list.
 * 
 * Input parameters - 1. productComparisonList - List of product repository items productComparisonList.
 * 
 * Output parameters - 1. activeProductList - list of active products. <br>
 * <br>
 * <b>Input Parameters</b><br>
 * &nbsp;&nbsp;&nbsp;<code>productList</code> - ProductComparisonList.<br>
 * &nbsp;&nbsp;&nbsp;<code>categoryId</code> - String.<br>
 * 
 * <b>Output Parameters</b><br>
 * &nbsp;&nbsp;&nbsp;<b>No output Parameters available.</b><br>
 * 
 * <br>
 * <b>Usage:</b><br>
 * &lt;dspel:droplet name="/com/tru/commerce/droplet/TRUActiveProductsForCompareDroplet"&gt;<br>
 * &lt;dspel:param name="productComparisonList" bean="/atg/commerce/catalog/comparison/ProductList" /&gt;<br>
 * &lt;/dspel:droplet&gt;
 * @author PA
 * @version 1.0
 */
public class TRUActiveProductsForCompareDroplet extends DynamoServlet {

	/**
	 * PRODUCT_COMPARISON_LIST parameter name.
	 */
	private static final ParameterName PRODUCT_COMPARISON_LIST = ParameterName
			.getParameterName("productComparisonList");

	/**
	 * Property to hold mCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;

	/**
	 * Getter of catalogTools.
	 * 
	 * @return the mCatalogTools.
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Setter of catalogTools.
	 * 
	 * @param pCatalogTools
	 *            the mCatalogTools to set.
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * This method is used to get the active product from the supplied list.
	 * 
	 * @param pRequest
	 *            - reference to DynamoHttpServletRequest object.
	 * @param pResponse
	 *            - reference to DynamoHttpServletResponse object.
	 * @throws ServletException
	 *             - if any exception occurs in servicing the request.
	 * @throws IOException
	 *             - if any exception occurs while writing the response to output stream.
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUActiveProductsForCompareDroplet.service method.. ");
		}
		List<RepositoryItem> activeProductList = null;
		Map<String, String> productIdSkuItemMap = new HashMap<String, String>();
		List productComparisonList = (List) pRequest.getObjectParameter(PRODUCT_COMPARISON_LIST);
		String categoryId = (String)pRequest.getLocalParameter(TRUCommerceConstants.CATEGORY_ID);
		if (isLoggingDebug()) {
			vlogDebug("List of Products selected for comparision from User {0}", productComparisonList);
		}
		// getting ACTIVE product items from comparison list
		activeProductList = getCatalogTools().getProductItems(productComparisonList,productIdSkuItemMap,categoryId);
		if (isLoggingDebug()) {
			vlogDebug("User Selected Product Items {0}", activeProductList);
		}
		pRequest.setParameter(TRUCommerceConstants.ACTIVE_PRODUCT_LIST, activeProductList);
		pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);

		if (isLoggingDebug()) {
			logDebug("END:: TRUActiveProductsForCompareDroplet.service method.. ");
		}
	}
}
