/*
 * @(#)TRUItemLevelPromotionDroplet.java	1.0 Nov 2, 2015
 *
 * Copyright (C) 2015, Professional Access (A division of Zensar
 * Technologies Limited).  All Rights Reserved. No use, copying or
 * distribution of this work may be made except in accordance with a
 * valid license agreement from PA, India. This notice must be included
 * on all copies, modifications and derivatives of this work.
 */
package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.StringUtils;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.pricing.TRUPricingModelProperties;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;


/**
 * This TRUItemLevelPromotionDroplet.
 *
 * @author Professional Access
 * @version 1.0
 *
 */
public class TRUItemLevelPromotionDroplet extends DynamoServlet {

	/**
	 * Holds reference for TRUConfiguration.
	 */
	private TRUConfiguration mTruConfiguration;
	
	/**property to hold promotionTools.
	 */
	private TRUPromotionTools mPromotionTools;
	
	/**
	 * Service.
	 *
	 * @param pRequest DynamoHttpServletRequest.
	 * @param pResponse DynamoHttpServletResponse.
	 * @throws ServletException ServletException.
	 * @throws IOException IOException.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
	IOException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUItemLevelPromotionDroplet  method: service]");
		}
		
		String promotionCountinPDP = getTruConfiguration().getPromotionCountinPDP();
		int promoCount = TRUConstants.SIZE_ONE;
		Set<String> promotionsIds = null;
		if(pRequest.getLocalParameter(TRUConstants.ADJUSTMENTS) != null){
			promotionsIds = new HashSet<String>((List<String>) pRequest.getLocalParameter(TRUConstants.ADJUSTMENTS));
		}
		if(isLoggingDebug()){
			vlogDebug("Promotion Data from Request is : {0} ", promotionsIds);
			vlogDebug("Number of promotions to be displayed in PDP is : {0} ", promotionCountinPDP);
		}
		if(!StringUtils.isBlank(promotionCountinPDP)){
			try{
				promoCount = Integer.parseInt(promotionCountinPDP);
			}catch(NumberFormatException nfe){
				if(isLoggingError()){
					vlogError("Provided Promotion Count {0} is not a Integer Value {1}: ", promotionCountinPDP,nfe);
				}
			}
		}
		if (promotionsIds != null && !promotionsIds.isEmpty()) {
			List<RepositoryItem> promoListExp = new ArrayList<RepositoryItem>();
			List<RepositoryItem> promoList = new ArrayList<RepositoryItem>();
			RepositoryItem promotionItem = null;
			TRUPromotionTools promotionTools = getPromotionTools();
			Repository promoRepository = promotionTools.getPromotions();
			 TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPromotionTools().getPricingModelProperties();
			if(promoRepository != null){
				 Date now = getPromotionTools().getCurrentDate().getTimeAsDate();
				for(String lPromoId : promotionsIds){
					try {
						int indexOf = lPromoId.indexOf(TRUCommerceConstants.PIPELINE);
						String promoId = lPromoId.substring(0,indexOf);
						promotionItem = promotionTools.getItemForId(pricingModelProperties.getPromotionItemDescName(), promoId);
						boolean isWebDisplay=false;
						if(promotionItem != null && null!=promotionItem.getPropertyValue(pricingModelProperties.getWebStoreDisplayPropertyName())){
							isWebDisplay=(boolean)promotionItem.getPropertyValue(pricingModelProperties.getWebStoreDisplayPropertyName());
						}
						//checking promotion is expired or not
						if(promotionItem != null && !promotionTools.checkPromotionExpiration(promotionItem, now) && isWebDisplay){
							promoListExp.add(promotionItem);
							if(isLoggingDebug()){
								vlogDebug("Pormotion Item : {0} is valid", promotionItem);
							}
						}
					} catch (RepositoryException exc) {
						if(isLoggingError()){
							vlogError("No Promotion Found for Promotion ID : {0}",lPromoId);
						}
					}
				}
				for(RepositoryItem lPromoItem : promoListExp){
						//checking promotion is started or not
						if(lPromoItem != null && getPromotionTools().checkPromotionStartDate(lPromoItem, now)){
							promoList.add(lPromoItem);
							if(isLoggingDebug()){
								vlogDebug("Pormotion Item : {0} is valid", lPromoItem);
							}
						}
				}
			}
			List<RepositoryItem> list = new ArrayList<RepositoryItem>(promoList);
			promotionTools.getSortedList(list);
			if (list != null && !list.isEmpty() && list.size() > promoCount) {
				pRequest.setParameter(TRUConstants.RANK_ONE_PROMOTION, list.get(TRUConstants.ZERO));
				if(isLoggingDebug()){
					vlogDebug("Rank One promotion is  : {0}", list.get(TRUConstants.ZERO));
				}
				list = list.subList(TRUConstants.INTEGER_NUMBER_ONE, promoCount);
				pRequest.setParameter(TRUConstants.PROMOTION_COUNT, list.size());
				pRequest.setParameter(TRUConstants.PROMOTIONS, list);
				if(isLoggingDebug()){
					vlogDebug("Special offers to be displayed in PDP is : {0}", list);
				}
				pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
			} else if (list != null && !list.isEmpty()){
				pRequest.setParameter(TRUConstants.RANK_ONE_PROMOTION, list.get(TRUConstants.ZERO));
				if(isLoggingDebug()){
					vlogDebug("Rank One promotion is  : {0}", list.get(TRUConstants.ZERO));
				}
				list.remove(TRUConstants.ZERO);
				pRequest.setParameter(TRUConstants.PROMOTION_COUNT, list.size());
				pRequest.setParameter(TRUConstants.PROMOTIONS, list);
				if(isLoggingDebug()){
					vlogDebug("Special offers to be displayed in PDP is : {0}", list);
				}
				pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
			}
		} else {
			pRequest.serviceLocalParameter(TRUConstants.EMPTY_OPARAM, pRequest, pResponse);
			if(isLoggingDebug()){
				logDebug("Promotion list is emplty");
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUItemLevelPromotionDroplet  method: service]");
		}
	}
	

	      



	/**
	 * Returns the truConfiguration.
	 * 
	 * @return the truConfiguration.
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * Sets/updates the truConfiguration.
	 * 
	 * @param pTruConfiguration
	 *            the truConfiguration to set.
	 */
	public void setTruConfiguration(TRUConfiguration pTruConfiguration) {
		mTruConfiguration = pTruConfiguration;
	}
	/**
	 * Gets the promotion tools.
	 *
	 * @return the promotionTools.
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}
	/**
	 * Sets the promotion tools.
	 *
	 * @param pPromotionTools the promotionTools to set.
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		this.mPromotionTools = pPromotionTools;
	}
}
