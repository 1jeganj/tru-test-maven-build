package com.tru.commerce.droplet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.common.TRUConstants;
import com.tru.common.TRUStoreConfiguration;
import com.tru.userprofiling.TRUCookieManager;
/**
 * <p>
 * This droplet will be invoked to  remove product compare cookiee. 
 * </p>
 * <b>Input Parameters</b><br>
 * 
 * &nbsp;&nbsp;&nbsp;<code>categoryId</code> - String.<br>
 * 
 * 
 * <b>Output Parameters.</b><br>
 * &nbsp;&nbsp;&nbsp;<b>No output Parameters available.</b><br>
 * 
 * <b>Open Parameters.</b><br>
 * &nbsp;&nbsp;&nbsp;<b>No open Parameters available.</b><br>
 * 
 * <br>
 * <b>Usage:</b><br>
 * &lt;dspel:droplet name="/com/tru/commerce/droplet/TRURemoveCompareCookieDroplet"&gt;<br>
 * &lt;/dspel:droplet&gt;
 * @author PA
 * @version 1.0
 */

public class TRURemoveCompareCookieDroplet extends DynamoServlet {
	
	/**
	 * Property to hold StoreConfiguration.
	 */
	private TRUStoreConfiguration mStoreConfiguration;
	
	/**
	 * property to hold TRUCookieManager.
	 */
	private TRUCookieManager mCookieManager;


	
	/**
	 * Gets the storeConfiguration.
	 * 
	 * @return the storeConfiguration.
	 */
	public TRUStoreConfiguration getStoreConfiguration() {
		return mStoreConfiguration;
	}

	/**
	 * Sets the storeConfiguration.
	 * 
	 * @param pStoreConfiguration
	 *            the storeConfiguration to set.
	 */
	public void setStoreConfiguration(TRUStoreConfiguration pStoreConfiguration) {
		mStoreConfiguration = pStoreConfiguration;
	}
	
	/**
	 * Gets the cookie manager.
	 * 
	 * @return the mTRUCookieManager
	 */
	public TRUCookieManager getCookieManager() {
		return mCookieManager;
	}

	/**
	 * Sets the cookie manager.
	 * 
	 * @param pCookieManager
	 *            the mCookieManager to set
	 */
	public void setCookieManager(TRUCookieManager pCookieManager) {
		this.mCookieManager = pCookieManager;
	}

	/**
	 * This method will get the parameter categoryId. It then search for the cookie based on  category id and remove the cookie
	 * 
	 * @param pRequest
	 *            - Holds the DynamoHttpServletRequest.
	 * @param pResponse
	 *            - Holds the DynamoHttpServletResponse.
	 * @throws ServletException
	 *             - ServletException if an error occurs.
	 * @throws IOException
	 *             - IOException if an error occurs.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRURemoveCompareCookieDroplet.service method.. ");
		}
			// getting categoryId parameter from jsp
				String compareCookieprefix=TRUCommerceConstants.EMPTY_SPACE;
				String categoryId =(String) pRequest.getLocalParameter(getStoreConfiguration().getCategoryIdParameterName());
				if(!StringUtils.isEmpty(categoryId)){
					compareCookieprefix = TRUConstants.PRODUCT_COMPARE_COOKIE+TRUCommerceConstants.UNDER_SCORE+categoryId;
				}
				//removing the  compare Cookie
				Cookie compareCookie =getCookieManager().getCookieFromRequest(compareCookieprefix, pRequest);
				if(compareCookie != null) {
					compareCookie.setMaxAge(0);
					compareCookie.setPath(TRUConstants.FORWARD_SLASH);
					compareCookie.setValue(TRUConstants.EMPTY_STRING);
					pResponse.addCookie(compareCookie);
					getCookieManager().removeCookieFromSession(pRequest, compareCookie.getName());
				}
				pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
				
		if (isLoggingDebug()) {
			vlogDebug("END:: TRURemoveCompareCookieDroplet.service method.. ");
		}
	}


}
