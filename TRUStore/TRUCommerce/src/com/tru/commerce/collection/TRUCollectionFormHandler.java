package com.tru.commerce.collection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.GenericFormHandler;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.common.TRUConstants;

/**
 * The class TRUCollectionFormHandler.
 * This form handler supports the following functionality : This is to used to create a collection string for add to cart /add to registry/wishlist functionality.
 * 
 *  @author PA
 *  @version 1.0
 */
public class TRUCollectionFormHandler extends GenericFormHandler {

	/** The Constant HANDLE_CREATE_COLLECTION_STRING. */
	private static final String HANDLE_CREATE_COLLECTION_STRING = "handleCreateCollectionString";
	
	/** The Constant HANDLE_CREATE_REGISTRY_STRING. */
	private static final String HANDLE_CREATE_REGISTRY_STRING = "handleCreateRegistryString";
	/**
	 * to keep quantity.
	 */
	private String mQuantity;
	
	/**
	 * to keep mProductId.
	 */
	private String mProductId;
	
	/**
	 * to keep mSkuId.
	 */
	private String mSkuId;
	
	/**
	 * to keep mLocationId.
	 */
	private String mLocationId;
	/**
	 * to keep mUtils.
	 */
	private TRUCollectionProductUtils mUtils;
	
	/**
	 * to keep mFinalString.
	 */
	private String mFinalString;
	
	/**
	 * to keep mFinalRegistryString.
	 */
	private String mFinalRegistryString;
	/**
	 * to keep mExistingString.
	 */
	private String mExistingString;
	/**
	 * to keep count of collection items.
	 */
	private int mItemsCount;
	
	/**
	 * to keep count of registry items.
	 */
	private int mItemsRegistryCount;
	/**
	 * to keep inventoryStatus.
	 */
	private String mInventoryStatus;
	
	/**
	 * to keep ispu.
	 */
	private String mIspu;
	
	/**
	 * to keep s2s.
	 */
	private String mS2s;
	
	/**
	 * to keep storeInventoryStatus.
	 */
	private String mStoreInventoryStatus;
	/**
	 * holds the RepeatingRequestMonitor object.
	 */
	private RepeatingRequestMonitor mRepeatingRequestMonitor;
	/**
	 * property to hold mTransactionManager.
	 */
	private TransactionManager mTransactionManager;

	/**
	 * to keep mActiveSkulength.
	 */
	private int mActiveSkulength;
	
	/**
	 * to keep mProductType.
	 */
	private String mProductType;
	

	/**
	 * This method will be used to handle quantity string.
	 * 
	 * @param pRequest
	 *            the request.
	 * @param pResponse
	 *            the response.
	 * @return the boolean.
	 * @throws ServletException
	 *             if the request could not be handled.
	 * @throws IOException
	 *             signals that an I/O exception has occurred.
	 */
	public boolean handleCreateCollectionString(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse)
			throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Entered :: TRUCollectionFormHandler.handleCreateCollectionString method..");
		}  
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = new TransactionDemarcation();
		try {
			if (getRepeatingRequestMonitor() != null && getRepeatingRequestMonitor().isUniqueRequestEntry(HANDLE_CREATE_COLLECTION_STRING)) {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
		final String productId=getProductId();
		final String skuId=getSkuId();
		final String inventoryStatus=getInventoryStatus();
		final String storeInventoryStatus=getStoreInventoryStatus();
		final String ispu=getIspu();
		final String s2s=getS2s();
		String locationId=TRUCommerceConstants.EMPTY_STRING;
		if(getLocationId()!=null && !getLocationId().isEmpty()){
			locationId=getLocationId();
		}
		final String quantity=getQuantity();
		StringBuilder combineStringUtils=new StringBuilder();
		combineStringUtils=combineStringUtils.append(productId);
		combineStringUtils.append(TRUCommerceConstants.PIPELINE);
		combineStringUtils=combineStringUtils.append(locationId);
		combineStringUtils.append(TRUCommerceConstants.PIPELINE);
		combineStringUtils=combineStringUtils.append(quantity);
		combineStringUtils.append(TRUCommerceConstants.PIPELINE);
		combineStringUtils=combineStringUtils.append(skuId);
		String combineString = combineStringUtils.toString();
		List<String> combineList = getUtils().getItemList();
		if(combineList==null)
		{
			combineList=new ArrayList<String>();
		}
		if(stringContainsItemFromList(productId,skuId,combineList)){
			final String existingString=getExistingString();
			removeExistingStringFromList(existingString,combineList);
		 }
		if(quantity!=null && Integer.parseInt(quantity)!=TRUCommerceConstants.INT_ZERO && (TRUCommerceConstants.EMPTY_STRING.equals(locationId)	&& TRUCommerceConstants.IN_STOCK.equals(inventoryStatus)) ||(!TRUCommerceConstants.EMPTY_STRING.equals(locationId) && !(TRUCommerceConstants.NO.equals(ispu) && TRUCommerceConstants.NO.equals(s2s)) &&(TRUCommerceConstants.IN_STOCK.equals(inventoryStatus) || TRUCommerceConstants.IN_STOCK.equals(storeInventoryStatus))) || 
				(!TRUCommerceConstants.EMPTY_STRING.equals(locationId) && TRUCommerceConstants.NO.equals(ispu) && TRUCommerceConstants.NO.equals(s2s) && TRUCommerceConstants.IN_STOCK.equals(inventoryStatus))){
		combineList.add(combineString);
		}
		getUtils().setItemList(combineList);
		if(combineList!=null && !combineList.isEmpty()){
			final int itemsCount = combineList.size();
			if(combineList.size()== TRUCommerceConstants.INT_ONE){
				final String combineFullString=combineList.get(TRUCommerceConstants.INT_ZERO);
				setFinalString(combineFullString);
			}else{
				StringBuffer combineFinalString = new StringBuffer();
				for (String elementString : combineList) {
					combineFinalString.append(TRUConstants.TILDA_STRING+elementString);
				}
				combineFinalString.deleteCharAt(0);
				setFinalString(combineFinalString.toString());
				}
			  setItemsCount(itemsCount);
			}
	   }
     }catch (TransactionDemarcationException e) {
			if (isLoggingError()) {
				logError("TransactionDemarcationException in handleCreateCollectionString: ", e);
			}
		} finally {
			if(isLoggingDebug()){
				logDebug("removeRequestEntry of RepeatingRequestMonitor in handleCreateCollectionString:");
			}
			if (getRepeatingRequestMonitor() != null) {
				getRepeatingRequestMonitor().removeRequestEntry(HANDLE_CREATE_COLLECTION_STRING);
			}
			try {
				if (tm != null) {
					td.end();
				}
			} catch (TransactionDemarcationException e) {
				if (isLoggingError()) {
					logError(
							"TransactionDemarcationException in handleCreateCollectionString: ",
							e);
				}
			}
			if (isLoggingDebug()) {
				logDebug("Exited :: TRUCollectionFormHandler.handleCreateCollectionString method..");
			}
		}
		return true;
	}
	
	/**
	 * This method will be used to handle quantity string for registry.
	 * 
	 * @param pRequest
	 *            the request.
	 * @param pResponse
	 *            the response.
	 * @return the boolean.
	 * @throws ServletException
	 *             if the request could not be handled.
	 * @throws IOException
	 *             signals that an I/O exception has occurred.
	 */
	public boolean handleCreateRegistryString(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse)
			throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Entered :: TRUCollectionFormHandler.handleCreateRegistryString method..");
		}  
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = new TransactionDemarcation();
		try {
			if (getRepeatingRequestMonitor() != null && getRepeatingRequestMonitor().isUniqueRequestEntry(HANDLE_CREATE_REGISTRY_STRING)) {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
		final String productId=getProductId();
		final String skuId=getSkuId();
		String locationId=TRUCommerceConstants.EMPTY_STRING;
		if(getLocationId()!=null && !getLocationId().isEmpty()){
			locationId=getLocationId();
		}
		final String quantity=getQuantity();
		StringBuilder combineRegistryStringUtils=new StringBuilder();
		combineRegistryStringUtils=combineRegistryStringUtils.append(productId);
		combineRegistryStringUtils.append(TRUCommerceConstants.PIPELINE);
		combineRegistryStringUtils=combineRegistryStringUtils.append(locationId);
		combineRegistryStringUtils.append(TRUCommerceConstants.PIPELINE);
		combineRegistryStringUtils=combineRegistryStringUtils.append(quantity);
		combineRegistryStringUtils.append(TRUCommerceConstants.PIPELINE);
		combineRegistryStringUtils=combineRegistryStringUtils.append(skuId);
		String combineString = combineRegistryStringUtils.toString();
		List<String> combineRegistryList = getUtils().getItemRegistryList();
		if(combineRegistryList==null)
		{
			combineRegistryList=new ArrayList<String>();
		}
		if(stringContainsItemFromList(productId,skuId,combineRegistryList)){
			final String existingString=getExistingString();
			removeExistingStringFromList(existingString,combineRegistryList);
		 }
		if(quantity!=null && Integer.parseInt(quantity)!=TRUCommerceConstants.INT_ZERO){
			combineRegistryList.add(combineString);
		}
		getUtils().setItemRegistryList(combineRegistryList);
		if(combineRegistryList!=null && !combineRegistryList.isEmpty()){
			final int itemsRegistryCount = combineRegistryList.size();
			if(combineRegistryList.size()== TRUCommerceConstants.INT_ONE){
				final String combineFullRegistryString=combineRegistryList.get(TRUCommerceConstants.INT_ZERO);
				setFinalRegistryString(combineFullRegistryString);
			}else{
				StringBuffer combineFinalRegistryString = new StringBuffer();
				for (String registryElementString : combineRegistryList) {
					combineFinalRegistryString.append(TRUConstants.TILDA_STRING+registryElementString);
				}
				combineFinalRegistryString.deleteCharAt(0);
				setFinalRegistryString(combineFinalRegistryString.toString());
				}
			  setItemsRegistryCount(itemsRegistryCount);
			}
	   }
     }catch (TransactionDemarcationException e) {
			if (isLoggingError()) {
				logError("TransactionDemarcationException in handleCreateRegistryString: ", e);
			}
		} finally {
			if(isLoggingDebug()){
				logDebug("removeRequestEntry of RepeatingRequestMonitor in handleCreateRegistryString:");
			}
			if (getRepeatingRequestMonitor() != null) {
				getRepeatingRequestMonitor().removeRequestEntry(HANDLE_CREATE_REGISTRY_STRING);
			}
			try {
				if (tm != null) {
					td.end();
				}
			} catch (TransactionDemarcationException e) {
				if (isLoggingError()) {
					logError("TransactionDemarcationException in handleCreateRegistryString: ",e);
				}
			}
			if (isLoggingDebug()) {
				logDebug("Exited :: TRUCollectionFormHandler.handleCreateRegistryString method..");
			}
		}
		return true;
	}
	
	/**
	 * To remove the existing string from list.
	 * @param pExistingString - ExistingString.
	 * @param pCombineList - combineList.
	 * @return boolean - boolean.
	 */
	public  boolean removeExistingStringFromList(String pExistingString ,List<String> pCombineList)
	{
		if (isLoggingDebug()) {
			logDebug("START :: TRUCollectionFormHandler.removeExistingStringFromList method..");
		}
		if(pExistingString != null && !pExistingString.isEmpty() ){
			   int index=pCombineList.indexOf(pExistingString);
			    if(index!=TRUCommerceConstants.STRING_NOT_FOUND){
			    	pCombineList.remove(index);
			 }
			}
		if (isLoggingDebug()) {
			logDebug("Exited :: TRUCollectionFormHandler.removeExistingStringFromList method..");
		}
		return true;
	}

	/**
	 * To make sure list have any part of string element or not.
	 * 
	 * @param pInputString - inputString
	 * @param pSkuId - skuId
	 * @param pItems - items
	 * @return boolean - boolean
	 */
	public boolean stringContainsItemFromList(String pInputString,String pSkuId, List<String> pItems)
	{
		if (isLoggingDebug()) {
			logDebug("START :: TRUCollectionFormHandler.stringContainsItemFromList method..");
		}
		for (String stringElement : pItems) {
			if(checkValidation(pInputString,pSkuId,stringElement)){	
				setExistingString(stringElement);
				return true;
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exited :: TRUCollectionFormHandler.stringContainsItemFromList method..");
		}
	    return false;
	}
	
	/**
	 * This method is used to check when the string should replace.
	 * 
	 * @param pInputString - productid
	 * @param pSkuId - SkuId
	 * @param pItem - add to cart String
	 * @return boolean - checkValidation
	 */
	public  boolean checkValidation(String pInputString,String pSkuId, String pItem)
	{
		if (isLoggingDebug()) {
			logDebug("START :: TRUCollectionFormHandler.checkValidation method..");
		}
		if(TRUCommerceConstants.NO_COLOR_SIZE_AVAILABLE.equals(getProductType()) && 
					pItem.contains(pInputString) && TRUCommerceConstants.INT_ONE == getActiveSkulength()){
			return true;
		}else if(TRUCommerceConstants.NO_COLOR_SIZE_AVAILABLE.equals(getProductType()) && 
					pItem.contains(pInputString) &&  getActiveSkulength() > TRUCommerceConstants.INT_ONE && pItem.contains(pSkuId)){
			return true;	
		}else if(!TRUCommerceConstants.NO_COLOR_SIZE_AVAILABLE.equals(getProductType()) && 
					pItem.contains(pInputString)){
			return true;
		}else{
			return false;
		}
	}
	/**
	 * To flush the collection session object.
	 * @param pRequest - DynamoHttpServletRequest.
	 * @param pResponse - DynamoHttpServletResponse.
	 * @return boolean - boolean.
	 * @throws ServletException - ServletException.
	 * @throws IOException - IOException.
	 */
	public boolean handleFlushCollectionString(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) 
			throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Begin :: TRUCollectionFormHandler.handleFlushCollectionString method..");
		}
		if(getUtils().getItemList()!=null && !getUtils().getItemList().isEmpty()){
			getUtils().getItemList().clear();
		}
		if (isLoggingDebug()) {
			logDebug("End :: TRUCollectionFormHandler.handleFlushCollectionString method..");
		}
		return true;
	}
	
	/**
	 * To flush the registry session object.
	 * @param pRequest - DynamoHttpServletRequest.
	 * @param pResponse - DynamoHttpServletResponse.
	 * @return boolean - boolean.
	 * @throws ServletException - ServletException.
	 * @throws IOException - IOException.
	 */
	public boolean handleFlushRegistryString(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) 
			throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Begin :: TRUCollectionFormHandler.handleFlushRegistryString method..");
		}
		if(getUtils().getItemRegistryList()!=null && !getUtils().getItemRegistryList().isEmpty()){
			getUtils().getItemRegistryList().clear();
		}
		if (isLoggingDebug()) {
			logDebug("End :: TRUCollectionFormHandler.handleFlushRegistryString method..");
		}
		return true;
	}
	
/**
 * To find name have special character or not.
 * @param pName - String.
 * @return Matcher - Matcher.
 */
	public boolean containsSpecialCharacter(String pName) {
		final Pattern p = Pattern.compile(TRUCommerceConstants.CHAR_PATTERN, Pattern.CASE_INSENSITIVE);
		final Matcher m = p.matcher(pName);
		return m.find();
	}

/**
 * Gets the quantity.
 *
 * @return the quantity.
 */
public String getQuantity() {
	return mQuantity;
}

/**
 * Sets the quantity.
 *
 * @param pQuantity the quantity to set.
 */
public void setQuantity(String pQuantity) {
	mQuantity = pQuantity;
}

/**
 * Gets the product id.
 *
 * @return the productId.
 */
public String getProductId() {
	return mProductId;
}

/**
 * Sets the product id.
 *
 * @param pProductId the productId to set.
 */
public void setProductId(String pProductId) {
	mProductId = pProductId;
}

/**
 * Gets the sku id.
 *
 * @return the skuId.
 */
public String getSkuId() {
	return mSkuId;
}

/**
 * Sets the sku id.
 *
 * @param pSkuId the skuId to set.
 */
public void setSkuId(String pSkuId) {
	mSkuId = pSkuId;
}

/**
 * Gets the location id.
 *
 * @return the locationId.
 */
public String getLocationId() {
	return mLocationId;
}

/**
 * Sets the location id.
 *
 * @param pLocationId the locationId to set.
 */
public void setLocationId(String pLocationId) {
	mLocationId = pLocationId;
}

/**
 * Gets the utils.
 *
 * @return the utils.
 */
public TRUCollectionProductUtils getUtils() {
	return mUtils;
}

/**
 * Sets the utils.
 *
 * @param pUtils the utils to set.
 */
public void setUtils(TRUCollectionProductUtils pUtils) {
	this.mUtils = pUtils;
}

/**
 * Gets the final string.
 *
 * @return the finalString.
 */
public String getFinalString() {
	return mFinalString;
}

/**
 * Sets the final string.
 *
 * @param pFinalString the finalString to set.
 */
public void setFinalString(String pFinalString) {
	mFinalString = pFinalString;
}

/**
 * Gets the final registry string.
 *
 * @return the finalString.
 */
public String getFinalRegistryString() {
	return mFinalRegistryString;
}

/**
 * Sets the final registry string.
 *
 * @param pFinalRegistryString the finalRegistryString to set.
 */
public void setFinalRegistryString(String pFinalRegistryString) {
	mFinalRegistryString = pFinalRegistryString;
}

/**
 * Gets the existing string.
 *
 * @return the String.
 */
public String getExistingString() {
	return mExistingString;
}

/**
 * Sets the existing string.
 *
 * @param pExistingString the String to set.
 */
public void setExistingString(String pExistingString) {
	mExistingString = pExistingString;
}

/**
 * Gets the items count.
 *
 * @return the String.
 */
public int getItemsCount() {
	return mItemsCount;
}

/**
 * Sets the items count.
 *
 * @param pItemsCount the String to set.
 */
public void setItemsCount(int pItemsCount) {
	mItemsCount = pItemsCount;
}

/**
 * Gets the items registry count.
 *
 * @return the String.
 */
public int getItemsRegistryCount() {
	return mItemsRegistryCount;
}

/**
 * Sets the items registry  count.
 *
 * @param pItemsRegistryCount the String to set.
 */
public void setItemsRegistryCount(int pItemsRegistryCount) {
	mItemsRegistryCount = pItemsRegistryCount;
}

/**
 * Gets the inventory status.
 *
 * @return the String.
 */
public String getInventoryStatus() {
	return mInventoryStatus;
}

/**
 * Sets the inventory status.
 *
 * @param pInventoryStatus the String to set.
 */
public void setInventoryStatus(String pInventoryStatus) {
	mInventoryStatus = pInventoryStatus;
}

/**
 * Gets the ispu.
 *
 * @return the String.
 */
public String getIspu() {
	return mIspu;
}

/**
 * Sets the ispu.
 *
 * @param pIspu the String to set.
 */
public void setIspu(String pIspu) {
	mIspu = pIspu;
}

/**
 * Gets the ispu.
 *
 * @return the String.
 */
public String getS2s() {
	return mS2s;
}

/**
 * Sets the S2s.
 *
 * @param pS2s the String to set.
 */
public void setS2s(String pS2s) {
	mS2s = pS2s;
}

/**
 * Gets the store inventory status.
 *
 * @return the String.
 */
public String getStoreInventoryStatus() {
	return mStoreInventoryStatus;
}

/**
 * Sets the store inventory status.
 *
 * @param pStoreInventoryStatus the String to set.
 */
public void setStoreInventoryStatus(String pStoreInventoryStatus) {
	mStoreInventoryStatus = pStoreInventoryStatus;
}

/**
 * Gets the repeating request monitor.
 *
 * @return the RepeatingRequestMonitor.
 */
public RepeatingRequestMonitor getRepeatingRequestMonitor() {
	return mRepeatingRequestMonitor;
}

/**
 * Sets the repeating request monitor.
 *
 * @param pRepeatingRequestMonitor the RepeatingRequestMonitor to set.
 */
public void setRepeatingRequestMonitor(
		RepeatingRequestMonitor pRepeatingRequestMonitor) {
	mRepeatingRequestMonitor = pRepeatingRequestMonitor;
}

/**
 * Gets the transaction manager.
 *
 * @return the TransactionManager.
 */
public TransactionManager getTransactionManager() {
	return mTransactionManager;
}

/**
 * Sets the transaction manager.
 *
 * @param pTransactionManager the TransactionManager to set.
 */
public void setTransactionManager(TransactionManager pTransactionManager) {
	mTransactionManager = pTransactionManager;
}

/**
 * @return the activeSkulength.
 */
public int getActiveSkulength() {
	return mActiveSkulength;
}

/**
 * @param pActiveSkulength the activeSkulength to set.
 */
public void setActiveSkulength(int pActiveSkulength) {
	mActiveSkulength = pActiveSkulength;
}

/**
 * @return the productType.
 */
public String getProductType() {
	return mProductType;
}

/**
 * @param pProductType the productType to set.
 */
public void setProductType(String pProductType) {
	mProductType = pProductType;
}


}
