package com.tru.commerce.collection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.core.util.StringUtils;
import atg.nucleus.Nucleus;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import atg.nucleus.naming.ComponentName;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.common.TRUConstants;


/**
 * This  property descriptor class return the child sku which belongs to current site and collection sku .
 * @author PA
 * @version 1.0 
 */
public class TRUChildSkuPropertyDescriptor extends RepositoryPropertyDescriptor {

	/**
	 * Serial Version Id
	 */
	private static final long serialVersionUID = 3441321679320758207L;

	/**
	 *  Property to hold logging. 
	 */
	private static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(TRUChildSkuPropertyDescriptor.class);

	/** 
	 * property to Hold CATALOG_PROPERTIES_COMPONENT_NAME. 
	 */
	public static final ComponentName CATALOG_PROPERTIES_COMPONENT_NAME = ComponentName
			.getComponentName(TRUCommerceConstants.CATALOG_PROPERTIES_COMPONENT_NAME);

	Nucleus nucleus = Nucleus.getGlobalNucleus();
	TRUCatalogProperties catalogProperties= (TRUCatalogProperties)nucleus.resolveName(CATALOG_PROPERTIES_COMPONENT_NAME);


	/**
	 * This method return the child sku which belongs to current site and collection sku.
	 *
	 * @param pItem 
	 * 			- the item
	 * @param pValue 
	 * 			- the Value
	 * @return Object
	 * 			 - the collectionSkus
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {

		if (mLogging.isLoggingDebug()) {   
			mLogging.logDebug("BEGIN :: TRUChildSkuPropertyDescriptor.getPropertyValue method..");
		} 

		List<RepositoryItem> collectionSkus= new ArrayList<RepositoryItem>();

		if(null != pItem && StringUtils.isNotEmpty(pItem.getRepositoryId())) {

			String repositoryId = pItem.getRepositoryId();
			String[] split = repositoryId.split(TRUConstants.COLON);
			
			boolean isActiveCollection = false;
			
			String displayStatus = (String) pItem.getPropertyValue(catalogProperties.getDisplayStatusPropertyName());
			if(StringUtils.isEmpty(displayStatus) || !displayStatus.equalsIgnoreCase(TRUCommerceConstants.DISPLAY_STATUS_HIDDEN)) {
				isActiveCollection = true;
			}
			
			if(isActiveCollection && split != null && split.length >= TRUCommerceConstants.INT_ONE && StringUtils.isNotEmpty(split[TRUCommerceConstants.INT_ZERO])) {

				String collectionId = split[TRUCommerceConstants.INT_ZERO];
				Set<String> collectionSiteSet = (Set<String>)pItem.getPropertyValue(catalogProperties.getSiteIDPropertyName());
				List<RepositoryItem>  collectionChildSkus =  new ArrayList<RepositoryItem>();
				List<RepositoryItem> childProducts = (List<RepositoryItem>)pItem.getPropertyValue(catalogProperties.getChildProductsPropertyName());
				List<String> onlinePIDForSkus = new ArrayList<String>();
				
				for (RepositoryItem childProduct : childProducts) {
					List<RepositoryItem> childSKUs = (List<RepositoryItem>) childProduct.getPropertyValue(catalogProperties.getChildSKUsPropertyName());

					for (RepositoryItem childSKU : childSKUs) {
						Map onlineCollectionName = (Map) childSKU.getPropertyValue(catalogProperties.getOnlineCollectionName());
						String onlinePID = (String) childSKU.getPropertyValue(catalogProperties.getOnlinePID());
						Set keySet = onlineCollectionName.keySet();
						if (keySet!= null && !keySet.isEmpty() && keySet.contains(collectionId) && isActiveSku(childSKU) && onlinePIDForSkus != null && !onlinePIDForSkus.contains(onlinePID)) {
							collectionChildSkus.add(childSKU);
							onlinePIDForSkus.add(onlinePID);
						}
					}
				}

				if(collectionChildSkus != null && collectionChildSkus.size() > TRUCommerceConstants.INT_ONE) {
					collectionSkus = getCollectionSkus(collectionChildSkus,collectionSiteSet);
				}
			}
		}
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("END :: TRUChildSkuPropertyDescriptor.getPropertyValue method..");
		}
		return collectionSkus;
	}


	/**
	 * This method is used to retrive the collectionSkuList.
	 *
	 * @param pSKUList the SKU list
	 * @param pSite the site
	 * @return the collection skus
	 */
	@SuppressWarnings("unchecked")
	public List<RepositoryItem> getCollectionSkus(List<RepositoryItem> pSKUList , Set<String> pSite) 

	{
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("BEGIN :: TRUChildSkuPropertyDescriptor.getCollectionSkus method..");
		}
		List<RepositoryItem> singleskulist = new ArrayList<RepositoryItem>();

		for(RepositoryItem childSKU : pSKUList) {
			Set<String> skuSiteSet = (Set<String>) childSKU.getPropertyValue(catalogProperties.getSiteIDPropertyName());
			if (skuSiteSet.equals(pSite)) {
				singleskulist.add(childSKU);
				break;
			}
		}
		if(singleskulist.isEmpty()) {
			singleskulist.add(pSKUList.get(TRUCommerceConstants.INT_ZERO));
		}
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("END :: TRUChildSkuPropertyDescriptor.getCollectionSkus method..");
		}
		return singleskulist;

	}

	/**
	 * This method is use to chek the active sku.
	 * @param pChildSku
	 * 				- the childSku
	 * @return isActive
	 * 			- returns the isActive as true or false
	 */
	private boolean isActiveSku(RepositoryItem pChildSku) {

		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("BEGIN :: TRUChildSkuPropertyDescriptor.isActiveSku method..");
		}
		boolean isActive = false;
		if(pChildSku == null) {
			return isActive;
		}

		boolean isDeletedFlag =	(boolean)pChildSku.getPropertyValue(catalogProperties.getIsSkuDeleted());
		boolean isSupressInSearchFlag =	(boolean)pChildSku.getPropertyValue(catalogProperties.getSupressInSearch());
		boolean isWebDisplayFlag = (boolean)pChildSku.getPropertyValue(catalogProperties.getWebDisplayFlag());
		String onlinePID = (String) pChildSku.getPropertyValue(catalogProperties.getOnlinePID());
		boolean superDisplayFlag = false;
		if(pChildSku.getPropertyValue(catalogProperties.getSuperDisplayFlag()) == null) {
			superDisplayFlag = true;
		} else {
			superDisplayFlag = (boolean) pChildSku.getPropertyValue(catalogProperties.getSuperDisplayFlag());
		}

		if(!isDeletedFlag && isWebDisplayFlag && !StringUtils.isEmpty(onlinePID) && superDisplayFlag && !isSupressInSearchFlag) {
			isActive = true;

		}
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("END :: TRUChildSkuPropertyDescriptor.isActiveSku method..");
		}
		return isActive;

	}
}
