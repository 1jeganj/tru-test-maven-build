package com.tru.commerce.collection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.core.util.StringUtils;
import atg.nucleus.Nucleus;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import atg.nucleus.naming.ComponentName;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.common.TRUConstants;

/**
 * This  property descriptor class return the list of collection sku's.
 * @author PA
 * @version 1.0 
 */
public class TRUCollectionSkuPropertyDescriptor extends RepositoryPropertyDescriptor {

	/**
	 * Serial Version Id
	 */
	private static final long serialVersionUID = -2828383228910827291L;

	/** 
	 * Property to hold logging. 
	 */
	private static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(TRUCollectionSkuPropertyDescriptor.class);

	/** 
	 * Property to Hold CATALOG_PROPERTIES_COMPONENT_NAME. 
	 */
	public static final ComponentName CATALOG_PROPERTIES_COMPONENT_NAME = ComponentName
			.getComponentName(TRUCommerceConstants.CATALOG_PROPERTIES_COMPONENT_NAME);

	/**
	 * This method return return the list of collection sku's.
	 *
	 * @param pItem 
	 * 			- the item
	 * @param pValue 
	 * 			- the Value
	 * @return Object 
	 * 			- the collectionSkus
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {

		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("BEGIN :: TRUCollectionSkuPropertyDescriptor.getPropertyValue method..");
		}
		
		Nucleus nucleus = Nucleus.getGlobalNucleus();
		TRUCatalogProperties catalogProperties=(TRUCatalogProperties)nucleus.resolveName(CATALOG_PROPERTIES_COMPONENT_NAME);
		List<RepositoryItem> collectionSkus= new ArrayList<RepositoryItem>();
		
		if(null != pItem && StringUtils.isNotEmpty(pItem.getRepositoryId())) {
			
			String repositoryId = pItem.getRepositoryId();
			String[] split = repositoryId.split(TRUConstants.COLON);
			
			if(StringUtils.isNotBlank(split[0])){

				String collectionId = split[0];

				List<RepositoryItem> childProducts = (List<RepositoryItem>)pItem.getPropertyValue(catalogProperties.getChildProductsPropertyName());
				for (RepositoryItem childProduct : childProducts) {

					List<RepositoryItem> childSKUs = (List<RepositoryItem>) childProduct.getPropertyValue(catalogProperties.getChildSKUsPropertyName());
					for (RepositoryItem childSKU : childSKUs) {

						Map onlineCollectionName = (Map) childSKU.getPropertyValue(catalogProperties.getOnlineCollectionName());
						if(onlineCollectionName != null) {
							Set keySet = onlineCollectionName.keySet();
							if (keySet!= null && !keySet.isEmpty() && keySet.contains(collectionId)) {
								collectionSkus.add(childSKU);
							}
						}
					}
				}
			}
		}
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("END :: TRUCollectionSkuPropertyDescriptor.getPropertyValue method..");
		}
		return collectionSkus;
	}
}
