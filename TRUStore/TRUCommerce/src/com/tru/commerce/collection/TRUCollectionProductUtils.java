package com.tru.commerce.collection;

import java.util.List;

import atg.nucleus.GenericService;

import com.tru.commerce.catalog.vo.ProductInfoVO;
/**
 * This class holds collection product Items.
 * @version 1.0
 * @author Professional Access.
 */
public class TRUCollectionProductUtils extends GenericService {
	/**
	 * Property to hold mAddToCartString.
	 */
	private String mAddToCartString;
	/**
	 * Property to hold mItemList.
	 */
	private List<String> mItemList;
	
	/**
	 * Property to hold mItemRegistryList.
	 */
	private List<String> mItemRegistryList;

	/**
	 * @return the addToCartString.
	 */
	public String getAddToCartString() {
		return mAddToCartString;
	}

	/**
	 * @param pAddToCartString the addToCartString to set.
	 */
	public void setAddToCartString(String pAddToCartString) {
		mAddToCartString = pAddToCartString;
	}

	/**
	 * @return the itemList.
	 */
	public List<String> getItemList() {
		return mItemList;
	}

	/**
	 * @param pItemRegistryList the itemRegistryList to set.
	 */
	public void setItemRegistryList(List<String> pItemRegistryList) {
		mItemRegistryList = pItemRegistryList;
	}
	
	/**
	 * @return the itemRegistryList.
	 */
	public List<String> getItemRegistryList() {
		return mItemRegistryList;
	}

	/**
	 * @param pItemList the itemList to set.
	 */
	public void setItemList(List<String> pItemList) {
		mItemList = pItemList;
	}

	/**
	 * This will populate a new assortment Item.
	 * 
	 * @param pProductInfo - pProductInfo
	 * @return productVo- productVo
	 */
	public ProductInfoVO populateAssortementProductInfo(
			ProductInfoVO pProductInfo) {
		if(isLoggingDebug()){
			logDebug("BEGIN: TRUCollectionProductUtils, populateAssortementProductInfo method::: pProductInfo:: "+pProductInfo);
		}
		ProductInfoVO productVo= new ProductInfoVO();
		productVo.setActive(pProductInfo.isActive());
		productVo.setActiveSKUsList(pProductInfo.getActiveSKUsList());
		productVo.setBatteryIncluded(pProductInfo.getBatteryIncluded());
		productVo.setBatteryRequired(pProductInfo.getBatteryRequired());
	 //	productVo.setBrand(pProductInfo.getBrand());
	//	productVo.setBrandLandingURL(pProductInfo.getBrandLandingURL());
	//	productVo.setBrandName(pProductInfo.getBrandName());
		productVo.setBreadcrumb(pProductInfo.getBreadcrumb());
	//	productVo.setChannelAvailability(pProductInfo.getChannelAvailability());
		productVo.setChildSKUsList(pProductInfo.getChildSKUsList());
		productVo.setColorCodeList(pProductInfo.getColorCodeList());
		productVo.setColorCodeToSwatchImageMap(pProductInfo.getColorCodeToSwatchImageMap());
		productVo.setColorSizes(pProductInfo.getColorSizes());
		productVo.setColorSizeVariantsAvailableStatus(pProductInfo.getColorSizeVariantsAvailableStatus());
		productVo.setColorSkusMap(pProductInfo.getColorSkusMap());
		productVo.setColorSwatchImageList(pProductInfo.getColorSwatchImageList());
		productVo.setComparableMap(pProductInfo.getComparableMap());
		productVo.setDefaultColorId(pProductInfo.getDefaultColorId());
		productVo.setDescription(pProductInfo.getDescription());
		productVo.setDisplayable(pProductInfo.isDisplayable());
		productVo.setDisplayName(pProductInfo.getDisplayName());
		productVo.setEwasteSurchargeSku(pProductInfo.getEwasteSurchargeSku());
		productVo.setHaveRelateditems(pProductInfo.isHaveRelateditems());
		productVo.setInActiveSKUsList(pProductInfo.getInActiveSKUsList());
		productVo.setInStockSKUsList(pProductInfo.getInStockSKUsList());
	//	productVo.setIspu(pProductInfo.getIspu());
		productVo.setJuvenileSizesList(pProductInfo.getJuvenileSizesList());
		productVo.setLocale(pProductInfo.getLocale());
		productVo.setManufacturerStyleNumber(pProductInfo.getManufacturerStyleNumber());
		productVo.setModel(pProductInfo.getModel());
		productVo.setOnlinePid(pProductInfo.getOnlinePid());
		productVo.setOutofStockSKUsList(pProductInfo.getOutofStockSKUsList());
		productVo.setPdpURL(pProductInfo.getPdpURL());
		productVo.setProductId(pProductInfo.getProductId());
		productVo.setProductStatus(pProductInfo.getProductStatus());
		productVo.setProductType(pProductInfo.getProductType());
		productVo.setRusItemNumber(pProductInfo.getRusItemNumber());
		productVo.setSearchable(pProductInfo.isSearchable());
		productVo.setSelectedColorSKUsList(pProductInfo.getSelectedColorSKUsList());
		productVo.setSelectedSizeSKUsList(pProductInfo.getSelectedSizeSKUsList());
		productVo.setSeoDescription(pProductInfo.getSeoDescription());
		productVo.setSeoDisplayName(pProductInfo.getSeoDisplayName());
		productVo.setSeoKeywords(pProductInfo.getSeoKeywords());
		productVo.setSeoTitle(pProductInfo.getSeoTitle());
		productVo.setSfs(pProductInfo.getSfs());
		productVo.setSiteId(pProductInfo.getSiteId());
		productVo.setSizeChart(pProductInfo.getSizeChart());
		productVo.setSizeChartName(pProductInfo.getSizeChartName());
		productVo.setSizeChartURL(pProductInfo.getSizeChartURL());
		productVo.setSizeSkusMap(pProductInfo.getSizeSkusMap());
		productVo.setThingsToKnow(pProductInfo.getThingsToKnow());
		productVo.setVendorsProductDemoUrl(pProductInfo.getVendorsProductDemoUrl());
		productVo.setVideoURL(pProductInfo.getVideoURL());
		productVo.setWhyWeloveIt(pProductInfo.getWhyWeloveIt());
		if(isLoggingDebug()){
			logDebug("END: TRUCollectionProductUtils, populateAssortementProductInfo method::: productVo:: "+productVo);
		}
		return productVo;
	}
	


}
