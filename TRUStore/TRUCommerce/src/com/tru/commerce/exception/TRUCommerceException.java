package com.tru.commerce.exception;

import atg.core.exception.ContainerException;

/**
 * This exception class used to throw commerce related exceptions.
 * 
 * @author PA
 * @version 1.0
 */

public class TRUCommerceException extends ContainerException {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * This method is used for get the exception details.
	 * 
	 * @param pMessage - String
	 * @param pSourceException - Throwable
	 */
	public TRUCommerceException(String pMessage, Throwable pSourceException) {
		super(pMessage, pSourceException);
	}

	/**
	 * This method is used for get the exception details.
	 * 
	 * @param pMessage - String
	 */
	public TRUCommerceException(String pMessage) {
		super(pMessage);
	}

	/**
	 * This method is used for get the exception details.
	 * 
	 * @param pSourceException - Throwable
	 */
	public TRUCommerceException(Throwable pSourceException) {
		super(pSourceException);
	}
}
