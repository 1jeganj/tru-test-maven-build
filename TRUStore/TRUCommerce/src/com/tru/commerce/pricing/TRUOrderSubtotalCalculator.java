/**
 * TRUOrderSubtotalCalculator.java
 */
package com.tru.commerce.pricing;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.OrderSubtotalCalculator;
import atg.commerce.pricing.PricingException;
import atg.repository.RepositoryItem;

/**
 * TRUOrderSubtotalCalculator extends to OrderSubtotalCalculator.
 * @author Professional Access.
 * @version 1.0
 */
public class TRUOrderSubtotalCalculator extends OrderSubtotalCalculator {

	/**
	 * This method will update the all order prices with sum of item prices.
	 * 
	 * @param pPriceQuote
	 *            - Order Price Info.
	 * @param pOrder
	 *            - Order.
	 * @param pPricingModel
	 *            - Pricing model item.
	 * @param pLocale
	 *            - Current Locale.
	 * @param pProfile
	 *            - Profile.
	 * @param pExtraParameters
	 *            - Map of extra parametes.
	 *@throws PricingException - if any.           
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	@Override
	public void priceOrder(OrderPriceInfo pPriceQuote, Order pOrder,
			RepositoryItem pPricingModel, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters)
					throws PricingException {
		if (isLoggingDebug()) {
			vlogDebug("Enter into [Class: TRUOrderSubtotalCalculator  method: priceOrder]");
			vlogDebug(
					"Order price info : {0} Order : {1} Pricing Model : {2} Locale : {3} Profile : {4} Extra parameters : {5}",
					pPriceQuote,
					pOrder,
					pPricingModel,
					pLocale,
					pProfile,
					pExtraParameters);
		}
		//calling super method to update the Order Price Info values.
		super.priceOrder(pPriceQuote, pOrder, pPricingModel, pLocale, pProfile,
				pExtraParameters);
		List<CommerceItem> commerceItems = pOrder.getCommerceItems();
		TRUOrderPriceInfo orderPriceInfo = (TRUOrderPriceInfo) pPriceQuote;
		TRUPricingTools pricingTools = (TRUPricingTools)getPricingTools();
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		// Calling method to update the custom properties values to Order Pricing Info Object.
		pricingTools.calculateAllOrderPrices(orderPriceInfo, commerceItems ,shippingGroups);
		if (isLoggingDebug()) {
			vlogDebug("Exit from [Class: TRUOrderSubtotalCalculator  method: priceOrder]");
		}
	}

}
