package com.tru.commerce.pricing;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.OrderSubtotalCalculator;
import atg.commerce.pricing.PricingException;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.common.TRUConstants;
/**
 *TRUGiftWrapOrderCalculator extend the OOTB OrderSubtotalCalculator.
 *
 *Gift Wrap Prices are not part of Order Subtotal so adjusting the Order Subtotal and Order Total Prices.
 *@author PA.
 *@version 1.0.
 *
 */
public class TRUGiftWrapOrderCalculator extends OrderSubtotalCalculator{
	
	/**
	 * This method will update the Order Raw Total and Order Tolal Prices.
	 * 
	 * @param pPriceQuote
	 *            - Order Price Info
	 * @param pOrder
	 *            - Order
	 * @param pPricingModel
	 *            - Pricing model item
	 * @param pLocale
	 *            - Current Locale
	 * @param pProfile
	 *            - Profile
	 * @param pExtraParameters
	 *            - Map of extra parametes
	 *@throws PricingException - if any           
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	@Override
	public void priceOrder(OrderPriceInfo pPriceQuote, Order pOrder,
			RepositoryItem pPricingModel, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters)
					throws PricingException {
		if (isLoggingDebug()) {
			vlogDebug("Enter into [Class: TRUGiftWrapOrderCalculator  method: priceOrder]");
			vlogDebug(
					"Order price info : {0} Order : {1} Pricing Model : {2} Locale : {3} Profile : {4} Extra parameters : {5}",
					pPriceQuote,
					pOrder,
					pPricingModel,
					pLocale,
					pProfile,
					pExtraParameters);
		}
		List<CommerceItem> commerceItems = pOrder.getCommerceItems();
		TRUOrderImpl orderImpl = (TRUOrderImpl) pOrder;
		TRUOrderPriceInfo orderPriceInfo = (TRUOrderPriceInfo) pPriceQuote;
		// Setting false as a default value
		orderImpl.setGWItemInOrder(Boolean.FALSE);
		if(orderPriceInfo == null || commerceItems == null || commerceItems.isEmpty()){
			return;
		}
		double giftWrapAmount = TRUConstants.DOUBLE_ZERO;
		double giftWraplistPrice = TRUConstants.DOUBLE_ZERO;
		double giftWrapSavings = TRUConstants.DOUBLE_ZERO;
		double orderAmount = orderPriceInfo.getAmount();
		double totalSavings = TRUConstants.DOUBLE_ZERO;
		double totalListPrice = TRUConstants.DOUBLE_ZERO;
		for(CommerceItem lCommerceItem : commerceItems){
			if(lCommerceItem instanceof TRUGiftWrapCommerceItem){
				if(!orderImpl.isGWItemInOrder()){
					// Setting true if order contains GiftWrap Item
					orderImpl.setGWItemInOrder(Boolean.TRUE);
				}
				TRUItemPriceInfo itemPriceInfo = (TRUItemPriceInfo) lCommerceItem.getPriceInfo();
				// Getting the Gift Wpap Item amount and Raw Amount
				giftWrapAmount += itemPriceInfo.getAmount();
				giftWrapSavings += itemPriceInfo.getTotalSavings();
				if(itemPriceInfo.isDisplayStrikeThroughPrice()){
					giftWraplistPrice += itemPriceInfo.getListPrice();
				}else {
					giftWraplistPrice += itemPriceInfo.getSalePrice();
				}
			}
		}
		if(isLoggingDebug()){
			vlogDebug("Total Saved Amount from Odrer with GiftWrap Prices: {0}", orderPriceInfo.getTotalSavedAmount());
			vlogDebug("Total List Price from Odrer with GiftWrap Prices: {0}", orderPriceInfo.getTotalListPrice());
			vlogDebug("Total Gift Wrap Amount from Odrer: {0}", giftWrapAmount);
			vlogDebug("Total GiftWrap List Price from Odrer : {0}", giftWraplistPrice);
			vlogDebug("Total GiftWrap Saving from Odrer : {0}", giftWrapSavings);
		}
		if(orderImpl.isGWItemInOrder()){
			totalSavings = orderPriceInfo.getTotalSavedAmount()-giftWrapSavings;
			totalListPrice = orderPriceInfo.getTotalListPrice() - giftWraplistPrice;
			orderPriceInfo.setTotalItemLevelSavings(getPricingTools().round(totalSavings));
			if(totalListPrice == TRUConstants.DOUBLE_ZERO){
				orderPriceInfo.setTotalItemLevelSavingsPercentage(TRUConstants.DOUBLE_ZERO);
			}else{
				orderPriceInfo.setTotalItemLevelSavingsPercentage(
						getPricingTools().round((totalSavings/totalListPrice)*TRUCommerceConstants.INT_HUNDRED));
			}
			if(isLoggingDebug()){
				vlogDebug("Total Saved Amount from Odrer without GiftWrap Prices: {0}", totalSavings);
				vlogDebug("Total List Price from Odrer without GiftWrap Prices: {0}", totalListPrice);
				
			}
			// Adjusting the Order Price Info amount and raw amount
			orderAmount -= giftWrapAmount;
			// After adjusting the amount and raw amount of order setting back to order price info objetc.
			orderPriceInfo.setAmount(orderAmount);
			orderPriceInfo.setRawSubtotal(orderAmount);
			orderPriceInfo.setGiftWrapPrice(giftWrapAmount);
		} else{
			totalSavings = orderPriceInfo.getTotalSavedAmount();
			totalListPrice = orderPriceInfo.getTotalListPrice();
			orderPriceInfo.setTotalItemLevelSavings(totalSavings);
			if(totalListPrice == TRUConstants.DOUBLE_ZERO){
				orderPriceInfo.setTotalItemLevelSavingsPercentage(TRUConstants.DOUBLE_ZERO);
			}else{
				orderPriceInfo.setTotalItemLevelSavingsPercentage(
						getPricingTools().round((totalSavings/totalListPrice)*TRUCommerceConstants.INT_HUNDRED));
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("Exit from [Class: TRUGiftWrapOrderCalculator  method: priceOrder]");
		}
	}
}
