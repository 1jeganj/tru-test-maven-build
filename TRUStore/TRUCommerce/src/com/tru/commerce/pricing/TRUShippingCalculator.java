/**
 * TRUShippingCalculator.java
 */
package com.tru.commerce.pricing;

import java.util.Locale;
import java.util.Map;

import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.Constants;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.ShippingCalculatorImpl;
import atg.commerce.pricing.ShippingPriceInfo;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.order.purchase.TRUShippingProcessHelper;
import com.tru.common.TRUConstants;

/**
 * The Class TRUShippingCalculator.
 *
 * @author Professional Access
 * @version 1.0
 * 
 * This calculator is to update the shipping prices into shipping price info object. 
 * Shipping price will be updated using freight logic and shipping surcharge will updated from product.
 */
public class TRUShippingCalculator extends ShippingCalculatorImpl {

	/**  holds ShippingProcessHelper component instance *. */
	private TRUShippingProcessHelper mShippingHelper;
	
	/**
	 * Gets the shipping helper.
	 *
	 * @return the shippingHelper
	 */
	public TRUShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}


	/**
	 * Sets the shipping helper.
	 *
	 * @param pShippingHelper the shippingHelper to set.
	 */
	public void setShippingHelper(TRUShippingProcessHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}

	/**
	 * This method is overridden to update the shipping price info values like
	 * raw amount, remaining shipping amount and shipping surcharge.
	 * @param pOrder - Order
	 * @param pPriceQuote - ShippingPriceInfo
	 * @param pShippingGroup - ShippingGroup
	 * @param pPricingModel - Pricing model repository item
	 * @param pLocale - Locale
	 * @param pProfile - Profile RepositoryItem
	 * @param pExtraParameters - Map of extra parameters
	 * @throws PricingException - if any exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void priceShippingGroup(Order pOrder, ShippingPriceInfo pPriceQuote,
			ShippingGroup pShippingGroup, RepositoryItem pPricingModel,
			Locale pLocale, RepositoryItem pProfile, Map pExtraParameters)
					throws PricingException {
		if (isLoggingDebug()) {
			vlogDebug("Enter into [Class: TRUShippingCalculator  method: priceShippingGroup]");
			vlogDebug(
					"pOrder : {0} ShippingPriceInfo : {1} pShippingGroup : {2} pPricingModel : {3} "
							+ " pLocale : {4} pProfile : {5} pExtraParameters : {6}",
					pOrder, pPriceQuote, pShippingGroup, pPricingModel,
					pLocale, pProfile, pExtraParameters);
		}
		
		// updating the least shipping method and its price if shipping group doesn't have
		if (pExtraParameters != null && pExtraParameters.get(TRUCommerceConstants.UPDATE_SHIP_METHOD_PRICE) != null) {
			final boolean updateShippingMethodAndPrice = (boolean) pExtraParameters.get(TRUCommerceConstants.UPDATE_SHIP_METHOD_PRICE);
			if (updateShippingMethodAndPrice /*&& (!(((TRUOrderImpl)pOrder).isOrderIsInMultiShip() || ((TRUOrderImpl)pOrder).isOrderMandateForMultiShipping()) )*/) {
				getShippingHelper().updateShippingMethodAndPrice(pOrder);
			}
		}		
		final TRUPricingTools pricingTools = (TRUPricingTools) getPricingTools();
		final TRUShippingPriceInfo shippingPriceInfo = (TRUShippingPriceInfo) pPriceQuote;
		
		//update shipping price info with values
		pricingTools.updateShippingPriceInfo(
				pShippingGroup.getCommerceItemRelationships(),
				shippingPriceInfo,
				pShippingGroup,
				pOrder);
		//Price shipping group price info
		pPriceQuote.getAdjustments().add(new PricingAdjustment(Constants.SHIPPING_PRICE_ADJUSTMENT_DESCRIPTION, 
				null, shippingPriceInfo.getAmount(), TRUConstants.LONG_ONE));
		
		// Setting the shipping price info to the Order Price Info Object
		pricingTools.priceShippingInOrderPriceInfo(pOrder, pShippingGroup, shippingPriceInfo);
		if (isLoggingDebug()) {
			vlogDebug("Exit from [Class: TRUShippingCalculator  method: priceShippingGroup]");
		}
	}

}
