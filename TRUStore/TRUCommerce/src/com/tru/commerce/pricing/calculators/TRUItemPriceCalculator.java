package com.tru.commerce.pricing.calculators;

import java.util.Locale;
import java.util.Map;

import atg.commerce.order.CommerceItem;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.priceLists.ItemPriceCalculator;
import atg.repository.RepositoryItem;

import com.tru.commerce.order.TRUDonationCommerceItem;


/**
 * This TRUItemPriceCalculator is a pre calculator to update the pricing of the Commerce Item.
 * @author Professional Access.
 * @version 1.0
 */
public class TRUItemPriceCalculator extends ItemPriceCalculator {
	
	/**
	 * Overriding the OOTB method to skip the calculation for Donation Type of commerce item.
	 * 
	 * @param pPriceQuote - Item price info.
	 * @param pItem - Commerce Item.
	 * @param pPricingModel - Pricing Model.
	 * @param pLocale - Current Locale.
	 * @param pProfile - Profile.
	 * @param pExtraParameters - Map of extra parameters.
	 * @throws PricingException - if any .
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void priceItem(ItemPriceInfo pPriceQuote, CommerceItem pItem, RepositoryItem pPricingModel, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters) throws PricingException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUItemPriceCalculator  method: priceItem]");
			vlogDebug(
					"Item price info : {0} Commerce Item : {1} Pricing Model : {2} Locale : {3} Profile : {4} Extra parameters : {5}",
					pPriceQuote,
					pItem,
					pPricingModel,
					pLocale,
					pProfile,
					pExtraParameters);
		}
		if(!(pItem instanceof TRUDonationCommerceItem)){
			try {
				super.priceItem(pPriceQuote, pItem, pPricingModel, pLocale, pProfile,
						pExtraParameters);
			} catch (PricingException prExc) {
				if (isLoggingError()) {
					vlogError(
							"PricingException:  While calculating  pricing adjustments for item : {0} with product id : {1} and exception is : {2}",
							pItem,
							pItem.getAuxiliaryData().getProductId(),
							prExc);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUItemPriceCalculator  method: priceItem]");
		}
	}
	
}
