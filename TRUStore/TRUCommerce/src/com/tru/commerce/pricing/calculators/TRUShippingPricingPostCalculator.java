package com.tru.commerce.pricing.calculators;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.ShippingCalculatorImpl;
import atg.commerce.pricing.ShippingPriceInfo;
import atg.commerce.promotion.TRUPromotionTools;
import atg.repository.RepositoryItem;

import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.common.TRUConstants;

/**
 * @author Professional Access
 * @version 1.0
 * This class is overriding the OOTB ShippingCalculatorImpl.
 * 
 * This calculator is used to update the get the Applied Promotion and set the
 * applied and not applied coupon codes in Order.
 * 
 */
public class TRUShippingPricingPostCalculator extends ShippingCalculatorImpl{
	

	/**
	 * This method will update the all the item prices to item price into object and adds the price info to Shipping Group.
	 * 
	 * @param pOrder
	 *            - Order Object
	 * @param pPriceQuote
	 *            - ShippingPriceInfo
	 * @param pShippingGroup
	 *            - ShippingGroup
	 * @param pPricingModel
	 *            - Promotions
	 * @param pLocale
	 *            - Locale
	 * @param pProfile
	 *            - Profile
	 * @param pExtraParameters
	 *            - Map of extra parameters
	 *@throws PricingException
	 *			-if any          
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void priceShippingGroup(Order pOrder, ShippingPriceInfo pPriceQuote,
			ShippingGroup pShippingGroup, RepositoryItem pPricingModel,
			Locale pLocale, RepositoryItem pProfile, Map pExtraParameters)
			throws PricingException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUShippingPricingPostCalculator  method: priceShippingGroup]");
			vlogDebug("pProfile: {0} pOrder : {1} pPriceQuote : {2}",pProfile,pOrder,pPriceQuote);
		}
		if(pProfile == null || !(pOrder instanceof TRUOrderImpl)){
			return;
		}
		TRUPromotionTools promotionTools = (TRUPromotionTools) ((TRUOrderManager)getPricingTools().getOrderManager()).getPromotionTools();
		// Calling method to get all the applied shipping promotion
		List<RepositoryItem> shippingPromotionList = promotionTools.getPromotionFromShippingPriceInfo(pPriceQuote);
		if(shippingPromotionList != null && !shippingPromotionList.isEmpty()){
			List<RepositoryItem> allPromo= (List<RepositoryItem>) pExtraParameters.get(TRUConstants.LIST_OF_PROMOTIONS);
			if(allPromo == null){
				allPromo = new ArrayList<RepositoryItem>();
			}
			allPromo.addAll(shippingPromotionList);
			pExtraParameters.put(TRUConstants.LIST_OF_PROMOTIONS, allPromo);
		}
		// Added code to calculate the Item and Order Discount Share.
		//TRUPricingTools pricingTools = (TRUPricingTools)getPricingTools();
		//pricingTools.calculateItemDiscountShare(pOrder, pExtraParameters);
		//pricingTools.calculateOrderDiscountShare(pOrder);
		// END.
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUShippingPricingPostCalculator  method: priceShippingGroup]");
		}
	}
}
