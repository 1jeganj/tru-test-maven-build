package com.tru.commerce.pricing.calculators;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import atg.commerce.order.CommerceItem;
import atg.commerce.pricing.Constants;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.priceLists.ItemPriceCalculator;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.common.TRUConstants;


/**
 * This TRUDonationItemPriceCalculator is a pre calculator to update donation amount.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUDonationItemPriceCalculator extends ItemPriceCalculator {

	
	/**
	 * Price item.
	 *
	 * @param pPriceQuote - Item price info.
	 * @param pItem - Commerce Item.
	 * @param pPricingModel - Pricing Model.
	 * @param pLocale - Current Locale.
	 * @param pProfile - Profile.
	 * @param pExtraParameters - Map of extra parameters.
	 * @throws PricingException - if any .
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	@Override
	public void priceItem(ItemPriceInfo pPriceQuote, CommerceItem pItem, RepositoryItem pPricingModel, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters) throws PricingException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUItemPricingPostCalculator  method: priceItem]");
			vlogDebug( "Item price info : {0} Commerce Item : {1} Pricing Model : {2} Locale : {3} Profile : {4} Extra parameters : {5}", pPriceQuote, pItem, pPricingModel, pLocale, pProfile, pExtraParameters);
		}
		if(pItem instanceof TRUDonationCommerceItem){
			Double donationAmount = TRUConstants.DOUBLE_ZERO;
			Object donationAmt = ((TRUDonationCommerceItem) pItem).getPropertyValue(TRUCommerceConstants.DONATION_AMOUNT);
			double itemAdjustAmount = pPriceQuote.getAmount() - donationAmount;
			if(donationAmt instanceof Double){
				donationAmount = Double.parseDouble(donationAmt.toString());
				pPriceQuote.setListPrice(donationAmount);
				pPriceQuote.setSalePrice(donationAmount);
				pPriceQuote.setAmount(donationAmount);
				pPriceQuote.setRawTotalPrice(donationAmount);
				//Creating Adjustments and detailed priceInfo
				List  detailsList = pPriceQuote.getCurrentPriceDetails();
				if (detailsList == null) {
					detailsList = new ArrayList(TRUConstants.INTEGER_NUMBER_ONE);
				}
				List newDetails = getPricingTools().getDetailedItemPriceTools().createInitialDetailedItemPriceInfos(donationAmount, pPriceQuote, pItem, pPricingModel, pLocale, pProfile, pExtraParameters, Constants.LIST_PRICE_ADJUSTMENT_DESCRIPTION);
				detailsList.addAll(newDetails);
				List adjustments = pPriceQuote.getAdjustments();
				if (adjustments.size() > TRUConstants.INT_ZERO) {
					adjustments.clear();
				}
				long quantity = pItem.getQuantity();
				double adjustAmount = donationAmount * quantity;
				PricingAdjustment adjustment = new PricingAdjustment(Constants.LIST_PRICE_ADJUSTMENT_DESCRIPTION, null, getPricingTools().round(adjustAmount), quantity);
				adjustments.add(adjustment);
				PricingAdjustment itemAdjustment = new PricingAdjustment(Constants.SALE_PRICE_ADJUSTMENT_DESCRIPTION, pPricingModel, getPricingTools().round(itemAdjustAmount), quantity);
				adjustments.add(itemAdjustment);
				pPriceQuote.getAdjustments().addAll(adjustments);
				if (detailsList == null) {
					throw new PricingException(MessageFormat.format(Constants.ITEM_NOT_LIST_PRICED, new Object[] { pItem.getId() }));
				}
				getPricingTools().getDetailedItemPriceTools().assignSalePriceToDetails(detailsList, donationAmount, pPriceQuote, pItem, pPricingModel, pLocale, pProfile, pExtraParameters, Constants.SALE_PRICE_ADJUSTMENT_DESCRIPTION);
				pPriceQuote.setCurrentPriceDetails(detailsList);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUItemPricingPostCalculator  method: priceItem]");
		}
	}
}
