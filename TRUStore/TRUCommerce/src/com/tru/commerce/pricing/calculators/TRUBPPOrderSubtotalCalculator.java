package com.tru.commerce.pricing.calculators;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.OrderSubtotalCalculator;
import atg.commerce.pricing.PricingException;
import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.order.TRUBppItemInfo;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.pricing.TRUOrderPriceInfo;
import com.tru.common.TRUConstants;

/**
 *TRUBPPOrderSubtotalCalculator extend the OOTB OrderSubtotalCalculator.
 *Adds the BPP item prices to Order Subtotal and Order Total Prices.
 * @author PA.
 * @version 1.0 .
 *
 * @author Professional Access.
 * @version 1.0
 * 
 */
public class TRUBPPOrderSubtotalCalculator extends OrderSubtotalCalculator{
	
	/**
	 * Holds the reference for TRUCommercePropertyManager
	 */
	private TRUCommercePropertyManager mCommercePropertyManager;
	
	/**
	 * This method will update the Order Raw Total and Order Total Prices including BPP item prices.
	 * 
	 * @param pPriceQuote
	 *            - Order Price Info
	 * @param pOrder
	 *            - Order
	 * @param pPricingModel
	 *            - Pricing model item
	 * @param pLocale
	 *            - Current Locale
	 * @param pProfile
	 *            - Profile
	 * @param pExtraParameters
	 *            - Map of extra parameters
	 *@throws PricingException - if any           
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	@Override
	public void priceOrder(OrderPriceInfo pPriceQuote, Order pOrder,
			RepositoryItem pPricingModel, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters)
					throws PricingException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBPPOrderSubtotalCalculator  method: priceOrder]");
			vlogDebug(
					"Order price info : {0} Order : {1} Pricing Model : {2} Locale : {3} Profile : {4} Extra parameters : {5}",
					pPriceQuote,
					pOrder,
					pPricingModel,
					pLocale,
					pProfile,
					pExtraParameters);
		}
		final List<CommerceItem> commerceItems = pOrder.getCommerceItems();
		final TRUOrderPriceInfo orderPriceInfo = (TRUOrderPriceInfo) pPriceQuote;
		if(orderPriceInfo == null || commerceItems == null || commerceItems.isEmpty()){
			return;
		}
		List<TRUShippingGroupCommerceItemRelationship> shipItemRels = null;
		TRUBppItemInfo bppItemInfo = null;
		double bppPrice = TRUConstants.DOUBLE_ZERO;
		double shipItemRelQty = TRUConstants.LONG_ZERO;
		double orderAmount = orderPriceInfo.getAmount();
		double rawSubtotal = orderPriceInfo.getRawSubtotal();
		if(isLoggingDebug()){
			vlogDebug("orderAmount : {0} rawSubtotal : {1} ", orderAmount, rawSubtotal);
		}
		for(CommerceItem lCommerceItem : commerceItems){
			shipItemRels = lCommerceItem.getShippingGroupRelationships();
			if(shipItemRels != null && !shipItemRels.isEmpty()){
				for (final Iterator iterator = shipItemRels.iterator(); iterator.hasNext();) {
					final TRUShippingGroupCommerceItemRelationship shipItemRel = (TRUShippingGroupCommerceItemRelationship) iterator.next();
					bppItemInfo = shipItemRel.getBppItemInfo();
					if(isLoggingDebug()){
						vlogDebug("BPP Item : {0} Commerce Item : {1} Ship Item Rel : {2}", bppItemInfo, lCommerceItem, shipItemRel);
					}
					if(bppItemInfo != null && StringUtils.isNotBlank(bppItemInfo.getId())){
						shipItemRelQty = shipItemRel.getQuantity();
						bppPrice = bppItemInfo.getBppPrice() * shipItemRelQty;
						rawSubtotal += bppPrice;
						orderAmount += bppPrice;
					}
					if(isLoggingDebug()){
						vlogDebug("Order Amount : {0} Raw Subtotal : {1} BPP Price : {2}", orderAmount, rawSubtotal, bppPrice);
					}
				}
			}
		}
		orderPriceInfo.setAmount(orderAmount);
		orderPriceInfo.setRawSubtotal(rawSubtotal);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBPPOrderSubtotalCalculator  method: priceOrder]");
		}
	}
	
	/**
	 * @return the commercePropertyManager
	 */
	public TRUCommercePropertyManager getCommercePropertyManager() {
		return mCommercePropertyManager;
	}

	/**
	 * @param pCommercePropertyManager the commercePropertyManager to set
	 */
	public void setCommercePropertyManager(
			TRUCommercePropertyManager pCommercePropertyManager) {
		mCommercePropertyManager = pCommercePropertyManager;
	}
}
