package com.tru.commerce.pricing.calculators;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.PricingContext;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.priceLists.ItemPriceCalculator;
import atg.commerce.promotion.TRUPromotionTools;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.TRUConstants;


/**
 * This TRUItemPricingPostCalculator is a post calculator to update the shipping group commerce item relationship with.
 * meta info containing the details quantity, price based on the item level promotion applied.
 *  @author Professional Access
 *  @version 1.0
 */
public class TRUItemPricingPostCalculator extends ItemPriceCalculator {

	
	/**
	 * 
	 * @param pPriceQuote - Item price info
	 * @param pItem - Commerce Item
	 * @param pPricingModel - Pricing Model
	 * @param pLocale - Current Locale
	 * @param pProfile - Profile
	 * @param pExtraParameters - Map of extra parameters
	 * @throws PricingException - if any 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void priceItem(ItemPriceInfo pPriceQuote, CommerceItem pItem, RepositoryItem pPricingModel, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters) throws PricingException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUItemPricingPostCalculator  method: priceItem]");
			vlogDebug(
					"Item price info : {0} Commerce Item : {1} Pricing Model : {2} Locale : {3} Profile : {4} Extra parameters : {5}",
					pPriceQuote,
					pItem,
					pPricingModel,
					pLocale,
					pProfile,
					pExtraParameters);
		}
		try {
			Order order = null;
			if(pExtraParameters != null && !pExtraParameters.isEmpty()){
				PricingContext context = (PricingContext) pExtraParameters.get(TRUConstants.PRICING_CONTEXT);
				order = context.getOrder();
			}
			if(isLoggingDebug()){
				vlogDebug("order: {0}",order);
			}
			if(pProfile == null || !(order instanceof TRUOrderImpl)){
				return;
			}
			TRUPromotionTools promotionTools = (TRUPromotionTools) ((TRUOrderManager)getPricingTools().getOrderManager()).getPromotionTools();
			// Calling method to get all the applied item promotion
			List<RepositoryItem> itemPromotionList = promotionTools.getPromotionFromItemPriceInfo(pPriceQuote);
			if(itemPromotionList != null && !itemPromotionList.isEmpty()){
				List<RepositoryItem> allPromo= (List<RepositoryItem>) pExtraParameters.get(TRUConstants.LIST_OF_PROMOTIONS);
				if(allPromo == null){
					allPromo = new ArrayList<RepositoryItem>();
				}
				allPromo.addAll(itemPromotionList);
				pExtraParameters.put(TRUConstants.LIST_OF_PROMOTIONS, allPromo);
			}
			// Calling method to update the meta info
			((TRUPricingTools)getPricingTools()).updateMetaInfo(pPriceQuote, pItem, pProfile);
		} catch (RepositoryException e) {
			vlogError(" RepositoryException in @Class::TRUItemPricingPostCalculator::@method::priceItem() :", e);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUItemPricingPostCalculator  method: priceItem]");
		}
	}
	
}
