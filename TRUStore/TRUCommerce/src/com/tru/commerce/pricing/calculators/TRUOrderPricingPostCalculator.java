package com.tru.commerce.pricing.calculators;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import atg.commerce.order.Order;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.OrderSubtotalCalculator;
import atg.commerce.pricing.PricingException;
import atg.commerce.promotion.TRUPromotionTools;
import atg.repository.RepositoryItem;

import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.common.TRUConstants;

/**
 * This class is overriding the OOTB OrderSubtotalCalculator.
 * 
 * This calculator is used to update the get the Applied Promotion and set the
 * applied and not applied coupon codes in Order.
 *  @author PA
 *  @version 1.0
 * 
 */
public class TRUOrderPricingPostCalculator extends OrderSubtotalCalculator{
	
	/**
	 * This method will update the all the item prices to item price into object and adds the price info to Order.
	 * 
	 * @param pPriceQuote
	 *            - ShippingPriceInfo.
	 *  @param pOrder
	 *            - Order Object.
	 * @param pPricingModel
	 *            - Promotions.
	 * @param pLocale
	 *            - Locale.
	 * @param pProfile
	 *            - Profile.
	 * @param pExtraParameters
	 *            - Map of extra parameters.
	 *@throws PricingException
	 *			-if any .         
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void priceOrder(OrderPriceInfo pPriceQuote, Order pOrder,
			RepositoryItem pPricingModel, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters)
			throws PricingException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOrderPricingPostCalculator  method: priceOrder]");
			vlogDebug("pProfile: {0} pOrder : {1} pPriceQuote : {2}",pProfile,pOrder,pPriceQuote);
		}
		if(pProfile == null || !(pOrder instanceof TRUOrderImpl)){
			return;
		}
		TRUPromotionTools promotionTools = (TRUPromotionTools) ((TRUOrderManager)getOrderManager()).getPromotionTools();
		// Calling method to get all the applied order promotion
		List<RepositoryItem> orderPromotionList = promotionTools.getPromotionFromOrderPriceInfo(pPriceQuote);
		if(orderPromotionList != null && !orderPromotionList.isEmpty()){
			List<RepositoryItem> allPromo= (List<RepositoryItem>) pExtraParameters.get(TRUConstants.LIST_OF_PROMOTIONS);
			if(allPromo == null){
				allPromo = new ArrayList<RepositoryItem>();
			}
			allPromo.addAll(orderPromotionList);
			pExtraParameters.put(TRUConstants.LIST_OF_PROMOTIONS, allPromo);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOrderPricingPostCalculator  method: priceOrder]");
		}
	}
}
