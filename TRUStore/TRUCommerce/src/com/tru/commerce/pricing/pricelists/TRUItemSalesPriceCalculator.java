/**
 * TRUItemPriceCalculator.java
 */
package com.tru.commerce.pricing.pricelists;

import java.util.Locale;
import java.util.Map;

import atg.commerce.order.CommerceItem;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.priceLists.ItemSalesPriceCalculator;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.pricing.TRUItemPriceInfo;
import com.tru.commerce.pricing.TRUPricingTools;

/**
 * This class is overriding the OOTB ItemSalesPriceCalculator.
 * This Calculator is to update the item price info with item prices.
 * 
 * This calculator is used to update the get the Applied Promotion and set the
 * applied and not applied coupon codes in Order.
 *  @author Professional Access.
 *  @version 1.0
 */
public class TRUItemSalesPriceCalculator extends ItemSalesPriceCalculator {

	/**
	 * This method will update the all the item prices to item price into object and adds the price info to commerce item.
	 * 
	 * @param pPrice
	 *            - Price Item.
	 * @param pPriceQuote
	 *            - ItemPriceInfo.
	 * @param pItem
	 *            - CommerceItem.
	 * @param pPricingModel
	 *            - Promotions.
	 * @param pLocale
	 *            - Locale.
	 * @param pProfile
	 *            - Profile.
	 * @param pExtraParameters
	 *            - Map of extra parameters.
	 *@throws PricingException
	 *			-if any.        
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void priceItem(RepositoryItem pPrice, ItemPriceInfo pPriceQuote,
			CommerceItem pItem, RepositoryItem pPricingModel, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters)
					throws PricingException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUItemSalesPriceCalculator  method: priceItem]");
			vlogDebug(
					"Price Item: {0} Item price info : {1} Commerce Item : {2} Pricing Model : {3} Locale : {4} Profile : {5} Extra parameters : {6}",
					pPrice,
					pPriceQuote,
					pItem,
					pPricingModel,
					pLocale,
					pProfile,
					pExtraParameters);
		}
		String dealIdPropertName = ((TRUPricingTools)getPricingTools()).getPriceProperties().getDealIDPropertyName();
		((TRUItemPriceInfo)pPriceQuote).setDealID((String)pPrice.getPropertyValue(dealIdPropertName));
		// Calling OOTB method to set the OOTB price to the Item Price Info Object.
 		
		
		try {
			if(!(pItem instanceof TRUDonationCommerceItem)){
				super.priceItem(pPrice, pPriceQuote, pItem, pPricingModel, pLocale, pProfile, pExtraParameters);
				((TRUPricingTools) getPricingTools()).priceItem(pPriceQuote,pItem,pPricingModel,pLocale,pProfile,pExtraParameters);
			}
		} catch (RepositoryException repExc) {
			if (isLoggingError()) {
				vlogError("RepositoryException:  While updating item price info with exception : {0}", repExc);
			}
		} catch (PricingException prExc) {
			if (isLoggingError()) {
				vlogError(
						"PricingException:  While calculating  pricing adjustments for item : {0} with product id : {1} and exception is : {2}",
						pItem,
						pItem.getAuxiliaryData().getProductId(),
						prExc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUItemSalesPriceCalculator  method: priceItem]");
		}
	}
}
