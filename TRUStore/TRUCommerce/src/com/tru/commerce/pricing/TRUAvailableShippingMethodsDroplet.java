package com.tru.commerce.pricing;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.purchase.TRUShippingGroupContainerService;
import com.tru.commerce.order.purchase.TRUShippingProcessHelper;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;
/**
 * The class TRUAvailableShippingMethodsDroplet extends DynamoServlet.
 * @author Professional Access
 * @version 1.0
 */
public class TRUAvailableShippingMethodsDroplet extends DynamoServlet {
	

	private static final String AVAILABLE_SHIPPING_METHODS = "availableShippingMethods";
	private static final String PREFERRED_SHIP_METHOD = "selectedShippingMethod";
	private static final ParameterName OUTPUT = ParameterName.getParameterName("output");
	private static final ParameterName ERROR = ParameterName.getParameterName("error");
	
	

	/**
	 * holds mShippingPricingEngine.
	 */
	private TRUShippingPricingEngine mShippingPricingEngine;
	
	/**
	 * @return the shippingPricingEngine.
	 */
	public TRUShippingPricingEngine getShippingPricingEngine() {
		return mShippingPricingEngine;
	}


	/**
	 * @param pShippingPricingEngine the shippingPricingEngine to set.
	 */
	public void setShippingPricingEngine(TRUShippingPricingEngine pShippingPricingEngine) {
		this.mShippingPricingEngine = pShippingPricingEngine;
	}


	/**
	 * @param pRequest DynamoHttpServletRequest.
	 * @param pResponse DynamoHttpServletResponse.
	 * @throws ServletException ServletException.
	 * @throws IOException IOException.
	 */
	@SuppressWarnings("unchecked")
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {				
		Map<String, Object> inputParamsMap=createInputParamsMap(pRequest);
		if(inputParamsMap.get(TRUConstants.ORDER_PARAM)==null){
			pRequest.setParameter(TRUConstants.ERROR,TRUConstants.TRUE );
			pRequest.serviceLocalParameter(ERROR, pRequest, pResponse);	
			return;
		}	
		TRUShippingPricingEngine shippingPricingEngine = (TRUShippingPricingEngine) getShippingPricingEngine();
		TRUShippingProcessHelper shippingHelper = shippingPricingEngine.getShippingHelper();
		List<TRUShipMethodVO> availableShippingMethods=null;	
		TRUHardgoodShippingGroup existingShippingGroup=null;
		Order lOrder=(Order)inputParamsMap.get(TRUConstants.ORDER_PARAM);
		try {		
			existingShippingGroup = shippingHelper.getPurchaseProcessHelper().getFirstHardgoodShippingGroup(lOrder);
			inputParamsMap.put(TRUConstants.ADDRESS_PARAM,existingShippingGroup.getShippingAddress());
			availableShippingMethods = shippingPricingEngine.getAvailableMethods(null, null, null, null, inputParamsMap);				
		} catch (CommerceException e) {
			if(isLoggingError()){
				logError("CommerceException ",e);
			}
		}
		if(availableShippingMethods==null || availableShippingMethods.isEmpty()){
			if (isLoggingInfo()) {
				vlogInfo("No shipmethods found for the order {0}",lOrder.getId());
			}	
			pRequest.setParameter(TRUConstants.MESSAGE,getErrorMessage(TRUErrorKeys.TRU_ERROR_SHIPPING_NO_SHIP_METHODS_FOUND) );
			pRequest.serviceLocalParameter(ERROR, pRequest, pResponse);	
		}else{
			pRequest.setParameter(AVAILABLE_SHIPPING_METHODS, availableShippingMethods);
			pRequest.setParameter(PREFERRED_SHIP_METHOD, existingShippingGroup.getShippingMethod());
			pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
		}						
		if(isLoggingDebug()){
			vlogDebug("End TRUAvailableShippingMethodsDroplet.service()");
		}	
	}//End of service()


/**
 * 
 * @param pRequest DynamoHttpServletRequest.
 * @return map.
 */
	private Map<String, Object> createInputParamsMap(DynamoHttpServletRequest pRequest) {
		Order order=(Order) pRequest.getObjectParameter(TRUConstants.CURRENT_ORDER);
		TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService)
				pRequest.getObjectParameter(TRUConstants.SHIPPING_GROUP_MAP_CONTAINER);
		String state= (String) pRequest.getLocalParameter(TRUConstants.STATE);
		String address1= (String) pRequest.getLocalParameter(TRUConstants.ADDRESS1);
		String isNewAddressForm= (String) pRequest.getLocalParameter(TRUConstants.IS_NEW_ADDRESS_FORM);	
		String city= (String) pRequest.getLocalParameter(TRUConstants.CITY);
		Map<String, Object> map=new HashMap<String, Object>();
		map.put(TRUConstants.ORDER_PARAM,order);
		map.put(TRUConstants.SHIPPING_GROUP_MAP_CONTAINER, shippingGroupMapContainer);
		map.put(TRUConstants.STATE_PARAM,state);
		map.put(TRUConstants.CITY,city);
		map.put(TRUConstants.ADDRESS1_PARAM,address1);	
		map.put(TRUConstants.IS_NEW_ADDRESS_FORM,isNewAddressForm);			
		if(isLoggingDebug()){			
			vlogDebug("Input Param's Map\t"+map);
		}
		return map;
	}
	
	/** Property to Hold Error handler manager. */
	private DefaultErrorHandlerManager mErrorHandlerManager;
	/**getter for errorHandlerManager.
	 * 
	 * @return mErrorHandlerManager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}
	
	/**
	 * Sets the errorHandlerManager.
	 * 
	 * @param pErrorHandlerManager
	 *            the mErrorHandlerManager to set.
	 */
	public void setErrorHandlerManager(DefaultErrorHandlerManager pErrorHandlerManager) {
		this.mErrorHandlerManager = pErrorHandlerManager;
	}
	
	/**
 	 * <p>
 	 * This method will get the error message for givem error key.
 	 * </p>
 	 * @param pErrorKey - ErrorKey.
 	 * @return errorMessage - errorMessage.
 	 *
 	 */
 	private String getErrorMessage(String pErrorKey){
 		String errorMessage=pErrorKey;
 		try {
			 errorMessage = getErrorHandlerManager().getErrorMessage(pErrorKey);
		} catch (com.tru.common.cml.SystemException e) {
			if(isLoggingError()){
				logError("com.tru.common.cml.SystemException \t:"+e,e);
			}
		}
 		return errorMessage;
 	}
}
