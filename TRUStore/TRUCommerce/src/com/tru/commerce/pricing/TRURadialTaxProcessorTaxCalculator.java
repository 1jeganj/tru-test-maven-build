package com.tru.commerce.pricing;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.datatype.DatatypeConfigurationException;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.TaxPriceInfo;
import atg.commerce.pricing.TaxProcessorTaxCalculator;
import atg.core.util.Address;
import atg.core.util.StringUtils;
import atg.payment.tax.TaxRequestInfo;
import atg.payment.tax.TaxRequestInfoImpl;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.service.util.CurrentDate;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.common.TRUIntegrationConfiguration;
import com.tru.logging.TRUAlertLogger;
import com.tru.logging.TRUIntegrationInfoLogger;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.integration.taxware.RadialTaxWareStatus;
import com.tru.radial.integration.taxware.TRURadialTaxwareProcessor;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.radial.util.TRURadialServiceRequestMonitor;
import com.tru.radial.util.TRUServiceMonitorRequest;

/**
 * The Class TRURadialTaxProcessorTaxCalculator.
 * @author Professional Access
 */
public class TRURadialTaxProcessorTaxCalculator extends TaxProcessorTaxCalculator {

	/** The Taxware request monitor. */
	private TRURadialServiceRequestMonitor mTaxwareRequestMonitor;

	/** The Radial taxware utils. */
	private TRURadialTaxwareUtils mRadialTaxwareUtils;
	
	/** The Radial taxware processor. */
	private TRURadialTaxwareProcessor mRadialTaxwareProcessor;

	/** The Alert logger. */
	private TRUAlertLogger mAlertLogger;
	
	/** The Current date. */
	private CurrentDate mCurrentDate;
	
	/** The mIntegrationInfoLogger. */
	private TRUIntegrationInfoLogger mIntegrationInfoLogger;
	
	/**
	 * @return the mIntegrationInfoLogger
	 */
	public TRUIntegrationInfoLogger getIntegrationInfoLogger() {
		return mIntegrationInfoLogger;
	}

	/**
	 * @param pIntegrationInfoLogger the mIntegrationInfoLogger to set
	 */
	public void setIntegrationInfoLogger(TRUIntegrationInfoLogger pIntegrationInfoLogger) {
		mIntegrationInfoLogger = pIntegrationInfoLogger;
	}
	
	/**
	 * @return the radialTaxwareUtils
	 */
	public TRURadialTaxwareUtils getRadialTaxwareUtils() {
		return mRadialTaxwareUtils;
	}

	/**
	 * @param pRadialTaxwareUtils the radialTaxwareUtils to set
	 */
	public void setRadialTaxwareUtils(TRURadialTaxwareUtils pRadialTaxwareUtils) {
		mRadialTaxwareUtils = pRadialTaxwareUtils;
	}

	/**
	 * @return the radialTaxwareProcessor
	 */
	public TRURadialTaxwareProcessor getRadialTaxwareProcessor() {
		return mRadialTaxwareProcessor;
	}

	/**
	 * @param pRadialTaxwareProcessor the radialTaxwareProcessor to set
	 */
	public void setRadialTaxwareProcessor(
			TRURadialTaxwareProcessor pRadialTaxwareProcessor) {
		mRadialTaxwareProcessor = pRadialTaxwareProcessor;
	}

	/**
	 * hold boolean property integrationConfig.
	 */
	private TRUIntegrationConfiguration mIntegrationConfig;

	/**
	 * @return the integrationConfig
	 */
	public TRUIntegrationConfiguration getIntegrationConfig() {
		return mIntegrationConfig;
	}

	/**
	 * @param pIntegrationConfig the integrationConfig to set
	 */
	public void setIntegrationConfig(TRUIntegrationConfiguration pIntegrationConfig) {
		mIntegrationConfig = pIntegrationConfig;
	}

	/**
	 * Calculate tax using radial Tax Quote service.
	 *
	 * @param pTaxInfo the tax info
	 * @param pPriceQuote the price quote
	 * @param pOrder the order
	 * @param pPricingModel the pricing model
	 * @param pLocale the locale
	 * @param pProfile the profile
	 * @param pExtraParameters the extra parameters
	 * @throws PricingException the pricing exception
	 */
	@SuppressWarnings({ "rawtypes"})
	protected void calculateTax(TaxRequestInfo pTaxInfo, TaxPriceInfo pPriceQuote, Order pOrder, RepositoryItem pPricingModel,
			Locale pLocale, RepositoryItem pProfile, Map pExtraParameters) throws PricingException {
		if (isLoggingDebug()) {
			vlogDebug("START:: TRURadialTaxProcessorTaxCalculator.calculateTax()");
		}
		RadialTaxWareStatus status = null;
		TRUOrderImpl order = (TRUOrderImpl) pOrder;
		double taxAmount = TRUTaxwareConstant.DOUBLE_ZERO;
		TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pPriceQuote;
		TRUOrderPriceInfo orderPriceInfo = (TRUOrderPriceInfo) pOrder.getPriceInfo();
		String currentTab = order.getCurrentTab();
		if (StringUtils.isEmpty(currentTab)) {
			currentTab = order.getCurrentTabInCSC();
		}
		if (StringUtils.isEmpty(currentTab)) {
			currentTab = TRUCommerceConstants.EMPTY_STRING;
		}
		String methodIdentifier = TRUCheckoutConstants.TRU_RADIAL_TAX_CALCULATOR_CALCULATE_TAX;
		long startTime = Calendar.getInstance().getTimeInMillis();
		try {
			if (PerformanceMonitor.isEnabled()) {
				PerformanceMonitor
				.startOperation(
						this.getClass().getName(),
						methodIdentifier);
			}
			String constructTaxQuoteRequest = getRadialTaxwareUtils().constructTaxQuoteRequest(pTaxInfo);
			long endTime = Calendar.getInstance().getTimeInMillis();
			if(constructTaxQuoteRequest != null) {
				getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_TAX), getIntegrationInfoLogger().getInterfaceNameMap().get(IRadialConstants.RADIAL_TAX), pTaxInfo.getOrder().getId(), pProfile.getRepositoryId());
				status = getRadialTaxwareProcessor().calculateRadialTaxQuote(constructTaxQuoteRequest);
				DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
				StringBuffer requestedSessionId = new StringBuffer(request.getRequestedSessionId());
				String source = requestedSessionId.append(TRUCheckoutConstants.DOUBLE_COLON).append(order.getId()).toString();
				String requestClientIpAddress = ((TRUOrderManager)getOrderManager()).getTrueClientIpAddress(request);
				getTaxwareRequestMonitor().addRequestEntry(new TRUServiceMonitorRequest(requestClientIpAddress, source, order.getId(), currentTab));
			}
			
			
			if (status == null) {
				if (isLoggingDebug()) {
					vlogDebug("TRURadialTaxProcessorTaxCalculator.calculateTax() returns null");
				}
				//order.setTaxwareErroMessage("Taxware Error: Please try again");
				return;
			}
			if (status.getTransactionSuccess() == Boolean.FALSE) {
				if (status.getRadialTaxFaultDetails() != null) {
					taxPriceInfo.setRadialTaxFaultDetails(status.getRadialTaxFaultDetails());
				}
				//order.setTaxwareErroMessage("Taxware Error: Please try again");
				Map<String, String> extra = new ConcurrentHashMap<String, String>();
				extra.put(IRadialConstants.STR_PAGE_NAME, currentTab);
				//extra.put(IRadialConstants.START_TIME, String.valueOf(startTime));
				extra.put(IRadialConstants.STR_REPPONSE_CODE, String.valueOf(status.getConnectionStatus()));
				extra.put(IRadialConstants.STR_TIME_TAKEN, new StringBuffer().append(String.valueOf(endTime-startTime)).
						append(TRUCommerceConstants.SPACE).append(TRUCommerceConstants.STR_MILLISECONDS).toString());
				getAlertLogger().logFailure(IRadialConstants.STR_TAX_PROCESSOR, IRadialConstants.STR_CALCULATE_TAX, extra);
			}
			if (status.getTransactionSuccess() == Boolean.TRUE) {
				getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_TAX), getIntegrationInfoLogger().getInterfaceNameMap().get(IRadialConstants.RADIAL_TAX), pTaxInfo.getOrder().getId(), pProfile.getRepositoryId());
				taxPriceInfo.setRadialTaxTransactionId(status.getTransactionId());
				if(status.getRadialTaxPriceInfos() != null) {
					taxPriceInfo.setRadialTaxPriceInfos(status.getRadialTaxPriceInfos());
				}
			}
			taxPriceInfo.setTaxRequestTimestamp(status.getTransactionTimestamp());
			taxAmount = getPricingTools().round(status.getAmount());
			taxPriceInfo.setAmount(taxAmount);
			taxPriceInfo.setEstimatedSalesTax(getPricingTools().round(status.getEstimatedSalesTax()));
			taxPriceInfo.setEstimatedIslandTax(getPricingTools().round(status.getEstimatedIslandTax()));
			taxPriceInfo.setEstimatedLocalTax(getPricingTools().round(status.getEstimatedLocalTax()));
			orderPriceInfo.setTax(taxAmount);
			
		}catch (DatatypeConfigurationException dataTypeExce) {
			if(isLoggingError()) {
				vlogError("TRURadialTaxProcessorTaxCalculator.calculateTax(): {0}",dataTypeExce);
			}
			//order.setTaxwareErroMessage("Taxware Error: Please try again");
		} finally{
			if (PerformanceMonitor.isEnabled()) {
				PerformanceMonitor
				.endOperation(
						this.getClass().getName(), methodIdentifier);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END: TRURadialTaxProcessorTaxCalculator and tax amount:{0}", status.getAmount());
		}
	}



	/**
	 * Override Price tax api to calculate price using Radial Tax Quote Service.
	 *
	 * @param pPriceQuote the price quote
	 * @param pOrder the order
	 * @param pPricingModel the pricing model
	 * @param pLocale the locale
	 * @param pProfile the profile
	 * @param pExtraParameters the extra parameters
	 * @throws PricingException the pricing exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void priceTax(TaxPriceInfo pPriceQuote, Order pOrder, RepositoryItem pPricingModel, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters)
					throws PricingException {

		if (isLoggingDebug()) {
			vlogDebug("START:: TRURadialTaxProcessorTaxCalculator.priceTax()");
		}
		// Added code to calculate the Item and Order Discount Share.
		TRUPricingTools pricingTools = (TRUPricingTools)getPricingTools();
		pricingTools.updateCouponAndShareDetails(pOrder, pExtraParameters);
		//pricingTools.calculateItemDiscountShare(pOrder, pExtraParameters);
		//pricingTools.calculateOrderDiscountShare(pOrder);
		//END.
		TaxRequestInfoImpl tri = new TaxRequestInfoImpl();
		tri.setOrder(pOrder);
		tri.setOrderId(pOrder.getId());
		//Tax Down Scenario
		if(!getIntegrationConfig().isEnableTaxware()) {
			TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pPriceQuote;
			taxPriceInfo.setAmountIsFinal(Boolean.TRUE);
			taxPriceInfo.setTaxRequestTimestamp(getCurrentDate().getTimeAsTimestamp());
			return;
		}
		Address shippingAddress = null;
		String storeLocationId = null;
		boolean isTaxClacRequired=false;
		if (null != pExtraParameters.get(TRUTaxwareConstant.CALC_TAX)) {
			isTaxClacRequired = (boolean)pExtraParameters.get(TRUTaxwareConstant.CALC_TAX);
		} else if(null != pExtraParameters.get(TRUTaxwareConstant.CALCULATE_TAX)) {
			isTaxClacRequired = (boolean)pExtraParameters.get(TRUTaxwareConstant.CALCULATE_TAX);
		} else if(null != pExtraParameters.get(TRUTaxwareConstant.CAL_TAX)) {
			isTaxClacRequired = (boolean)pExtraParameters.get(TRUTaxwareConstant.CAL_TAX);
		} else if(((TRUOrderImpl)pOrder).isRepriceRequiredOnCart()) {
			isTaxClacRequired = true;
			((TRUOrderImpl)pOrder).setRepriceRequiredOnCart(false);
		}
		if (isLoggingDebug()) {
			vlogDebug("TRURadialTaxProcessorTaxCalculator.priceTax() isTaxClacRequired: {0}", isTaxClacRequired);
		}
		if(isTaxClacRequired) {
			isTaxClacRequired = Boolean.FALSE;
			tri.setOrder(pOrder);
			tri.setOrderId(pOrder.getId());
			List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
			ShippingGroup group = null;
			Iterator groupIterator = shippingGroups.iterator();
			while (groupIterator.hasNext()) {
				shippingAddress = null;
				storeLocationId = null;
				group = (ShippingGroup) groupIterator.next();
				if (!(group.getCommerceItemRelationshipCount() > TRUTaxwareConstant.NUMERIC_ZERO)) {
					continue;
				}
				if(group instanceof TRUHardgoodShippingGroup) {
					shippingAddress = ((TRUHardgoodShippingGroup) group).getShippingAddress();
				}
				if(group instanceof TRUInStorePickupShippingGroup) {
					storeLocationId = ((TRUInStorePickupShippingGroup) group).getLocationId();
				}
				if((shippingAddress != null && shippingAddress.getPostalCode() != null) || StringUtils.isNotBlank(storeLocationId) ) {
					isTaxClacRequired = Boolean.TRUE;
					break;
				}
			}
			boolean orderHasOnlydonationItems = orderHasOnlydonationItems(pOrder);
			if (isLoggingDebug()) {
				vlogDebug("TRURadialTaxProcessorTaxCalculator.priceTax() isTaxClacRequired:{0} orderHasOnlydonationItems: {1}", isTaxClacRequired, orderHasOnlydonationItems);
			}
			if (isTaxClacRequired || (!isTaxClacRequired && orderHasOnlydonationItems)) {
				calculateTax(tri, pPriceQuote, pOrder, pPricingModel, pLocale, pProfile, pExtraParameters);
			}
		}
	}

	/**
	 * This method is used to check the order for donation item. If order has only donation item it will return the true boolean value.
	 * @param pOrder the order
	 * @return boolean value
	 */
	@SuppressWarnings("unchecked")
	boolean orderHasOnlydonationItems(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("END:: TRURadialTaxProcessorTaxCalculator.orderHasOnlydonationItems()");
		}
		List<CommerceItem> commerceItems = pOrder.getCommerceItems();
		for (CommerceItem commerceItem : commerceItems) {
			if (!(commerceItem instanceof TRUDonationCommerceItem)) {
				return false;
			}
		}
		if(((TRUOrderImpl)pOrder).getBillingAddress() != null) {
			return true;
		}
		return false;
	}

	/**
	 * @return the currentDate
	 */
	public CurrentDate getCurrentDate() {
		return mCurrentDate;
	}

	/**
	 * @param pCurrentDate the currentDate to set
	 */
	public void setCurrentDate(CurrentDate pCurrentDate) {
		mCurrentDate = pCurrentDate;
	}

	/**
	 * @return the mTaxwareRequestMonitor
	 */
	public TRURadialServiceRequestMonitor getTaxwareRequestMonitor() {
		return mTaxwareRequestMonitor;
	}

	/**
	 * Sets the taxware request monitor.
	 *
	 * @param pTaxwareRequestMonitor the new taxware request monitor
	 */
	public void setTaxwareRequestMonitor(TRURadialServiceRequestMonitor pTaxwareRequestMonitor) {
		this.mTaxwareRequestMonitor = pTaxwareRequestMonitor;
	}

	/**
	 * Gets the alert logger.
	 *
	 * @return the alert logger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the new alert logger
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
}
