package com.tru.commerce.pricing;

import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.ShippingGroupNotFoundException;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.pricing.PricingTools;
import atg.commerce.pricing.priceLists.PriceListException;
import atg.commerce.promotion.GWPMarkerManager;
import atg.commerce.promotion.PromotionTools;
import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.Address;
import atg.core.util.StringUtils;
import atg.markers.MarkerException;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.claimable.TRUClaimableTools;
import com.tru.commerce.order.TRUBppItemInfo;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.vo.TRUPromoContainer;
import com.tru.common.vo.TRUPromoVO;
import com.tru.userprofiling.TRUPropertyManager;


/**
 * This class is to perform the pricing related business logic.
 * @author Professional Access.
 * @version 1.0
 *
 */
public class TRUPricingTools extends PricingTools {

	/**
	 * Holds reference for TRUCatalogProperties.
	 */
	private TRUCatalogProperties mCatalogProperties;
	
	/** This holds reference for TRUConfiguration. */
	public TRUConfiguration mTruConfiguration;

	/** This holds the reference for TRUPriceProperties. **/
	private TRUPriceProperties mPriceProperties;

	/** This holds the Default PriceList Property Name. **/
	private String mDefaultPriceListPropertyName;

	/** This holds the Default Sale PriceList Property Name. **/
	private String mDefaultSalePriceListPropertyName;
	
	/** This holds the Default Sale mPricingModelProperties.**/
	private TRUPricingModelProperties mPricingModelProperties;
	
	
	/** The tru commerce property manager. */
	private TRUCommercePropertyManager mTRUCommercePropertyManager;

	private final static String ALL_SHP_GRP_ITEMS_TOTAL_KEY = "ALL_SHP_GRP_ITEMS_TOTAL";

	/**
	 * This method is to get the list/sale for SKU.
	 * @param pSkuId - SKU ID.
	 * @param pProductId - Product ID.
	 * @param pSite - Site
	 * @param pListPrice - true/false.
	 * @param pSalePrice - true/false.
	 * @return priceItem - list or sale price item.
	 */
	public RepositoryItem getPriceForSKU(String pSkuId, String pProductId, Site pSite, boolean pListPrice,
			boolean pSalePrice) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPricingTools  method: getPriceForSKU]");
			vlogDebug("SKU Id : {0} ", pSkuId);
		}
		RepositoryItem priceItem = null;
		RepositoryItem priceList = null;
		if (pSite != null && pListPrice) {
			priceList =
					(RepositoryItem) pSite.getPropertyValue(getDefaultPriceListPropertyName());
			vlogDebug("Default price list for site : {0} is  : {1}", pSite, priceList);
		} else if (pSite != null && pSalePrice) {
			priceList =
					(RepositoryItem) pSite.getPropertyValue(getDefaultSalePriceListPropertyName());
			vlogDebug("Default sale price list for site : {0} is  : {1}", pSite, priceList);
		}

		try {
			priceItem = getPriceListManager().getPrice(priceList, pProductId, pSkuId);
		} catch (PriceListException exc) {
			if (isLoggingError()) {
				vlogError(
						"PriceListException: while getting price for SKU : {0} Product : {1} with exception : {2}",
						pSkuId,
						pProductId,
						exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: getPriceForSKU]");
		}
		return priceItem;
	}
	
	/**
	 * This method is to update the total list and sale prices to item price info object and calculates the pricing.
	 * adjustments.
	 * 
	 * @param pPriceQuote
	 *            - Item price info.
	 * @param pItem
	 *            - Commerce Item.
	 * @param pPricingModel
	 *            - Pricing Model.
	 * @param pLocale
	 *            - Current Locale.
	 * @param pProfile
	 *            - Profile.
	 * @param pExtraParameters
	 *            - Map of extra parameters.
	 * @throws RepositoryException
	 *             - if any.
	 * @throws PricingException
	 *             - if any.
	 */
	@SuppressWarnings({"rawtypes"})
	public void priceItem(ItemPriceInfo pPriceQuote,
			CommerceItem pItem, RepositoryItem pPricingModel, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters) throws RepositoryException, PricingException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPricingTools  method: priceItem]");
			vlogDebug(
					"Item price info : {0} Commerce Item : {1} Pricing Model : {2} Locale : {3} Profile : {4} Extra parameters : {5}",
					pPriceQuote,
					pItem,
					pPricingModel,
					pLocale,
					pProfile,
					pExtraParameters);
		}
		TRUItemPriceInfo itemPriceInfo = (TRUItemPriceInfo) pPriceQuote;
		
		// Updates prices to item price info
		updateItemPriceInfo(itemPriceInfo, pItem);
		
		//Update Shipping Surcharge value to the Item Price Info Object.
		updateShippingSurchargeAndEWastePrice(itemPriceInfo,pItem ,pExtraParameters);				
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: priceItem]");
		}
	}

	/**
	 * This method is to Update the Shipping Surcharge value to the Item Price Info Object.
	 * 
	 * @param pItemPriceInfo : TRUItemPriceInfo Object.
	 * @param pItem : CommerceItem Object.
	 * @param pExtraParameters : pExtraParameters
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void updateShippingSurchargeAndEWastePrice(TRUItemPriceInfo pItemPriceInfo,
			CommerceItem pItem,Map pExtraParameters) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPricingTools  method: updateShippingSurchargePrice]");
			vlogDebug("Item price info : {0} pItemPriceInfo : {1} pItem",pItemPriceInfo,pItem);
		}
		if(pItemPriceInfo == null || pItem == null){
			return;
		}
		RepositoryItem skuItem = (RepositoryItem) pItem.getAuxiliaryData().getCatalogRef();
		RepositoryItem prodItem = (RepositoryItem) pItem.getAuxiliaryData().getProductRef();
		//String shipMethod = null;
		TRUCatalogProperties catalogProperties = getCatalogProperties();
		List<TRUShippingGroupCommerceItemRelationship> shipGroupRels = pItem.getShippingGroupRelationships();
		String type = (String) skuItem.getPropertyValue(catalogProperties.getTypePropertyName());
		if(shipGroupRels != null && !shipGroupRels.isEmpty()  && !type.equalsIgnoreCase(TRUConstants.NON_MERCH_SKU)){
			Map<String, String> akhiOrLowe48ShipPrice = null;
			String akhiShippingId = null;
			String lower48ShippingId = null;
			String shippingSurcharge = null;
			String eWasteSurchargeState = TRUConstants.DOUBLE_ZERO_STRING;
			double shippingSurchargeDouble = TRUConstants.DOUBLE_ZERO;
			double eWastePrice = TRUConstants.DOUBLE_ZERO;
			boolean isCompleteShippingAddress=false;
			for(TRUShippingGroupCommerceItemRelationship lShiRep : shipGroupRels){
				ShippingGroup shippingGroup = lShiRep.getShippingGroup();
				if(shippingGroup instanceof TRUHardgoodShippingGroup || shippingGroup instanceof TRUInStorePickupShippingGroup){
					String state=TRUConstants.EMPTY_STRING;
					String shipMethod = TRUConstants.EMPTY_STRING;
					TRUHardgoodShippingGroup hardGoodShipGrp = null;
					TRUInStorePickupShippingGroup instorePickupShipGrp = null;
					if(shippingGroup instanceof TRUHardgoodShippingGroup){
						hardGoodShipGrp = (TRUHardgoodShippingGroup)shippingGroup;
						shipMethod = hardGoodShipGrp.getShippingMethod();					
						Address address= hardGoodShipGrp.getShippingAddress();
						if (address != null && StringUtils.isNotBlank(address.getFirstName()) &&
								StringUtils.isNotBlank(address.getLastName()) &&
								StringUtils.isNotBlank(address.getAddress1()) &&
								StringUtils.isNotBlank(address.getCity()) &&
								StringUtils.isNotBlank(address.getState()) 
								) {
							state=address.getState();
							isCompleteShippingAddress=true;
						}else{						
							state=((TRUHardgoodShippingGroup)shippingGroup).getTransientState();
						}
					} else if (shippingGroup instanceof TRUInStorePickupShippingGroup){
						instorePickupShipGrp =  (TRUInStorePickupShippingGroup)shippingGroup;
						state = instorePickupShipGrp.getEwasteState();
					}
					
					if(!StringUtils.isBlank(state) ){
						if (state.equalsIgnoreCase(TRUConstants.STATE_AK) || state.equalsIgnoreCase(TRUConstants.STATE_HI)) {
							akhiShippingId = getShippingMethodAndAKAHIdMap().get(shipMethod);
							akhiOrLowe48ShipPrice = (Map<String, String>) skuItem.getPropertyValue(catalogProperties.getAkhiMapPropertyName());
							shippingSurcharge = akhiOrLowe48ShipPrice.get(akhiShippingId);
						}else {
							lower48ShippingId = getShippingMethodAndLowe48IdMap().get(shipMethod);
							akhiOrLowe48ShipPrice = (Map<String, String>) skuItem.getPropertyValue(catalogProperties.getLower48MapPropertyName());
							shippingSurcharge = akhiOrLowe48ShipPrice.get(lower48ShippingId);
						}
						if(!StringUtils.isBlank(shippingSurcharge) && !shippingSurcharge.equalsIgnoreCase(TRUConstants.F) &&
								!shippingSurcharge.equalsIgnoreCase(TRUConstants.S)){
							lShiRep.setSurcharge(round(Double.valueOf(shippingSurcharge)*lShiRep.getQuantity()));
							shippingSurchargeDouble += Double.valueOf(shippingSurcharge)*lShiRep.getQuantity();
						}else{
							lShiRep.setSurcharge(TRUConstants.DOUBLE_ZERO);
						}
						if(prodItem !=null){
							eWasteSurchargeState = (String) prodItem.getPropertyValue(catalogProperties.getEwasteSurchargeStatePropertyName());
						}
						if (isLoggingDebug()) {
							vlogDebug("Class: TRUPricingTools  method: updateShippingSurchargePrice] :prodItem {0}  : eWasteSurchargeState{1}",prodItem,eWasteSurchargeState);
						}
						if((!StringUtils.isBlank(eWasteSurchargeState)) && eWasteSurchargeState.equalsIgnoreCase(state)){
							// Method to get the EWate Feed for EWaste SKU.
							eWastePrice += round((getEWastePrice(prodItem)*lShiRep.getQuantity()));
							lShiRep.setE911Fees(eWastePrice);
						} else{
							lShiRep.setE911Fees(TRUConstants.DOUBLE_ZERO);
						}
					}else{
						lower48ShippingId = getShippingMethodAndLowe48IdMap().get(shipMethod);
						akhiOrLowe48ShipPrice = (Map<String, String>) skuItem.getPropertyValue(catalogProperties.getLower48MapPropertyName());
						//Calling method to get the default shipping surcharge
						if(StringUtils.isNotBlank(lower48ShippingId) && akhiOrLowe48ShipPrice != null && !akhiOrLowe48ShipPrice.isEmpty()){
							shippingSurcharge = akhiOrLowe48ShipPrice.get(lower48ShippingId);
						}
						//shippingSurcharge = getDefaultShipSurcharge(akhiOrLowe48ShipPrice);
						if(!StringUtils.isBlank(shippingSurcharge) && 
								!shippingSurcharge.equalsIgnoreCase(TRUConstants.F) && !shippingSurcharge.equalsIgnoreCase(TRUConstants.S)){
							shippingSurchargeDouble += Double.valueOf(shippingSurcharge)*lShiRep.getQuantity();
							lShiRep.setSurcharge(shippingSurchargeDouble);
						}else{
							lShiRep.setSurcharge(TRUConstants.DOUBLE_ZERO);
						}
					}
				}else{
					lShiRep.setSurcharge(TRUConstants.DOUBLE_ZERO);
					lShiRep.setE911Fees(TRUConstants.DOUBLE_ZERO);
				}
			}
			pItemPriceInfo.setItemShippingSurcharge(round(shippingSurchargeDouble));
			pItemPriceInfo.setEwasteFees(round(eWastePrice));
			
			
			if(!isCompleteShippingAddress &&
					pExtraParameters!=null && 
					pExtraParameters.get(TRUConstants.ORDER)!=null  &&
					pItemPriceInfo.getEwasteFees()>TRUConstants.DOUBLE_ZERO
					){
				TRUOrderImpl orderImpl=(TRUOrderImpl) pExtraParameters.get(TRUConstants.ORDER);
				orderImpl.setRepriceRequiredOnCart(Boolean.TRUE);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: updateShippingSurchargePrice]");
		}

	}

	/**
	 * Method to get The EWate Price (List Price) of EWaste SKU item.
	 * 
	 * @param pProdItem - Repository Item.
	 * @return double : EWate Price for EWaste SKU Item.
	 */
	public double getEWastePrice(RepositoryItem pProdItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPricingTools  method: getEWastePrice]");
			vlogDebug("Product Item : {0} pProdItem",pProdItem);
		}
		double eWastePrice = TRUConstants.DOUBLE_ZERO;
		if(pProdItem == null){
			vlogDebug("EWates Product Item is NUll,EWaste Price : {0} eWastePrice",eWastePrice);
			return eWastePrice;
		}
		TRUCatalogProperties catalogProperties = getCatalogProperties();
		String eWasteSurchargeProdId = (String) pProdItem.getPropertyValue(catalogProperties.getEwasteSurchargeSkuPropertyName());
		if(StringUtils.isBlank(eWasteSurchargeProdId)){
			vlogDebug("EWate Product Id is NUll,EWaste Price : {0} eWastePrice",eWastePrice);
			return eWastePrice;
		}
		TRUCatalogTools catalogTools = (TRUCatalogTools) getCatalogTools();
		Site site = SiteContextManager.getCurrentSite();
		try {
			// Getting Repository Item for EWaste Product ID
			RepositoryItem eWasteProductItem = catalogTools.findProduct(eWasteSurchargeProdId);
			// Getting Product of EWaste SKU
			RepositoryItem eWasteSKUItem = catalogTools.getDefaultChildSku(eWasteProductItem);
			if(eWasteSKUItem != null && eWasteProductItem != null){
				// Calling method to get the List Price Repository Item of EWaste sku and EWaste Product.
				eWastePrice = getListPriceForSKU(eWasteSKUItem.getRepositoryId(),eWasteProductItem.getRepositoryId(), site);
			}
			
		} catch (RepositoryException repExc) {
			if (isLoggingError()) {
				vlogError("RepositoryException:  While getting sku or product item : {0}", repExc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: getEWastePrice]");
			vlogDebug("EWaste Price : {0} eWastePrice",eWastePrice);
		}
		return eWastePrice;
		
	}

	/**
	 * This method updates the list and sale prices to item price info by getting intraday prices.
	 * 
	 * @param pItemPriceInfo
	 *            - Item price info.
	 * @param pItem
	 *            - Commerce Item.
	 * @throws RepositoryException
	 *             - if any.
	 */
	private void updateItemPriceInfo(TRUItemPriceInfo pItemPriceInfo, CommerceItem pItem) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: updateItemPriceInfo]");
		}
		Site site = SiteContextManager.getCurrentSite();
		long quantity = pItem.getQuantity();
		boolean validateThresholdPrices =
				validateThresholdPrices(site, pItemPriceInfo.getListPrice(), pItemPriceInfo.getSalePrice());
		pItemPriceInfo.setDisplayStrikeThroughPrice(validateThresholdPrices);
		pItemPriceInfo.setTotalSalePrice(pItemPriceInfo.getSalePrice()*pItem.getQuantity());
		if (validateThresholdPrices) {
			Map<String, Double> savingsMap =
					calculateSavings(pItemPriceInfo.getListPrice() * quantity, pItemPriceInfo.getSalePrice() * quantity);
			pItemPriceInfo.setSavedAmount(savingsMap.get(TRUConstants.SAVED_AMOUNT));
			pItemPriceInfo.setSavedPercentage(savingsMap.get(TRUConstants.SAVED_PERCENTAGE));
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: updateItemPriceInfo]");
		}
	}

	/**
	 * This method is to calculate the saved amount and save percentages in case of any difference in the list and sale.
	 * prices.
	 * 
	 * @param pListPrice - list price.
	 * @param pSalePrice - sale prices.
	 *  @return Map - savings.
	 */
	public Map<String, Double> calculateSavings(double pListPrice, double pSalePrice) {
		if (isLoggingDebug()) {
			logDebug("Entered into [Class: TRUPricingTools  method: calculateSavings]");
			vlogDebug("pListPrice : {0} pSalePrice : {1}", pListPrice, pSalePrice);
		}
		Map<String, Double> savingsMap = new HashMap<String, Double>();
		double savedAmount = pListPrice - pSalePrice;
		double savedPercentage=TRUConstants.DOUBLE_ZERO;
		if(pListPrice!= TRUConstants.DOUBLE_ZERO){
			savedPercentage = (pListPrice - pSalePrice) / pListPrice * TRUConstants.HUNDERED;
		}
		vlogDebug("Saved amount : {0} Saved Perecentage : {1}", savedAmount, savedPercentage);
		savingsMap.put(TRUConstants.SAVED_AMOUNT, round(savedAmount));
		savingsMap.put(TRUConstants.SAVED_PERCENTAGE, round(savedPercentage));
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: calculateSavings]");
		}
		return savingsMap;
	}

	/**
	 * This method is to validate the threshold prices. Which is used to display the strike through prices.
	 * 
	 * @param pSite
	 *            - Site.
	 * @param pListPrice - list price.
	 * @param pSalePrice - sale prices.
	 * @return boolean - true or false base on thresholdAmount and thresholdPersentage .
	 */
	public boolean validateThresholdPrices(Site pSite,
			double pListPrice, double pSalePrice) {
		if (isLoggingDebug()) {
			logDebug("Entered into [Class: TRUPricingTools  method: validateThresholdPrices]");
			vlogDebug("Site : {0}  pListPrice : {1} pSalePrice : {2}", pSite, pListPrice, pSalePrice);
		}
		TRUPriceProperties priceProperties = getPriceProperties();
		double thresholdPricePercentage = TRUConstants.DOUBLE_ZERO;
		double thresholdPriceAmount = TRUConstants.DOUBLE_ZERO;
		if (pSite != null) {
			if(pSite.getPropertyValue(priceProperties.getThresholdPricePercentagePropertyName()) == null &&
					pSite.getPropertyValue(priceProperties.getThresholdPriceAmountPropertyName()) == null){
				return Boolean.FALSE;
			}
			if (pSite.getPropertyValue(priceProperties.getThresholdPricePercentagePropertyName()) != null) {
				thresholdPricePercentage =
						(double) pSite.getPropertyValue(priceProperties.getThresholdPricePercentagePropertyName());
			}
			if (pSite.getPropertyValue(priceProperties.getThresholdPriceAmountPropertyName()) != null) {
				thresholdPriceAmount =
						(double) pSite.getPropertyValue(priceProperties.getThresholdPriceAmountPropertyName());
			}
			vlogDebug(
					"Site level Threshold Price Percentage : {0} Threshold Price Amount : {1}",
					thresholdPricePercentage,
					thresholdPriceAmount);
		}
		if (pListPrice != TRUConstants.DOUBLE_ZERO && pSalePrice != TRUConstants.DOUBLE_ZERO) {
			double diffPercentage = round((pListPrice - pSalePrice) / pListPrice * TRUConstants.HUNDERED);
			double diffAmount = round(pListPrice - pSalePrice);
			if (diffPercentage >= thresholdPricePercentage || diffAmount > thresholdPriceAmount) {
				return Boolean.TRUE;
			} else {
				return Boolean.FALSE;
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: validateThresholdPrices]");
		}
		return Boolean.FALSE;
	}

	/**
	 * This method is to update the order price info with sum of all item price info prices by iterating the commerce.
	 * items in the order.
	 * 
	 * @param pOrderPriceInfo
	 *            - Order price info.
	 * @param pCommerceItems
	 *            - Commerce items in the order.
	 * @param pShippingGroups .
	 */
	@SuppressWarnings("unchecked")
	public void calculateAllOrderPrices(TRUOrderPriceInfo pOrderPriceInfo,
			List<CommerceItem> pCommerceItems, List<ShippingGroup> pShippingGroups) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPricingTools  method: calculateAllOrderPrices]");
			vlogDebug("OrderPriceInfo : {0} CommerceItems : {1}", pOrderPriceInfo, pCommerceItems);
		}
		double totalSavedAmount = TRUConstants.DOUBLE_ZERO;
		double totalSavePercentage = TRUConstants.DOUBLE_ZERO;
		double eWatseFee = TRUConstants.DOUBLE_ZERO;
		double totaltPrice = TRUConstants.DOUBLE_ZERO;
		double giftWrapPrice = TRUConstants.DOUBLE_ZERO;
		double bppPrice = TRUConstants.DOUBLE_ZERO;
		double shipItemRelQty = TRUConstants.LONG_ZERO;
		List<TRUShippingGroupCommerceItemRelationship> shipItemRels = null;
		TRUBppItemInfo bppItemInfo = null;
		//double totalShippingSurcharge = TRUConstants.DOUBLE_ZERO;
		// Iterate through the commerce items and update orderPriceInfo's item related price components
		TRUItemPriceInfo itemPriceInfo = null;
		for (CommerceItem commerceItem : pCommerceItems) {
			itemPriceInfo = (TRUItemPriceInfo) commerceItem.getPriceInfo();
			if (itemPriceInfo != null) {
				if(itemPriceInfo.isDisplayStrikeThroughPrice()){
					totaltPrice += itemPriceInfo.getListPrice() * commerceItem.getQuantity();
				}else {
					totaltPrice += itemPriceInfo.getSalePrice() * commerceItem.getQuantity();
				}
				

				shipItemRels = commerceItem.getShippingGroupRelationships();
				if(shipItemRels != null && !shipItemRels.isEmpty()){
					for (@SuppressWarnings("rawtypes")
					Iterator iterator = shipItemRels.iterator(); iterator.hasNext();) {
						TRUShippingGroupCommerceItemRelationship shipItemRel = (TRUShippingGroupCommerceItemRelationship) iterator.next();
						bppItemInfo = shipItemRel.getBppItemInfo();
						if(isLoggingDebug()){
							vlogDebug("BPP Item : {0} Commerce Item : {1} Ship Item Rel : {2}", bppItemInfo, commerceItem, shipItemRel);
						}
						if(bppItemInfo != null && StringUtils.isNotBlank(bppItemInfo.getId())){
							shipItemRelQty = shipItemRel.getQuantity();
							bppPrice = bppItemInfo.getBppPrice() * shipItemRelQty;
							totaltPrice += bppPrice;
						}
						if(isLoggingDebug()){
							vlogDebug("totaltPrice : {0} BPP Price : {1}", totaltPrice, bppPrice);
						}
					}
				}
			
				
				
				
				totalSavedAmount += round(itemPriceInfo.getTotalSavings());
				eWatseFee +=round(itemPriceInfo.getEwasteFees());
			}
		}
		pOrderPriceInfo.setTotalListPrice(totaltPrice);
		pOrderPriceInfo.setEwasteFees(eWatseFee);
		vlogDebug(
				"totalSavedAmount : {0} totalSavePercentage : {1}  ",
				totalSavedAmount,
				totalSavePercentage);
		// Set all the prices in the order price info
		pOrderPriceInfo.setTotalSavedAmount(round(totalSavedAmount));
		pOrderPriceInfo.setTotalSavedPercentage(round(totalSavePercentage));
		pOrderPriceInfo.setGiftWrapPrice(giftWrapPrice);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: calculateAllOrderPrices]");
		}
	}

	/**
	 * This method is to update the shipping prices into shipping price info. Shipping price will updated using Freight.
	 * logic. Shipping surcharge will updated from the product.
	 * 
	 * @param pCommerceItemRelationships
	 *            - ShippingGroupCommerceItemRelationship.
	 * @param pShippingPriceInfo
	 *            - TRUShippingPriceInfo.
	 * @param pShippingGroup
	 *            - ShippingGroup.
	 * @param pOrder
	 *            - Order.
	 */
	public void updateShippingPriceInfo(List<TRUShippingGroupCommerceItemRelationship> pCommerceItemRelationships,
			TRUShippingPriceInfo pShippingPriceInfo,
			ShippingGroup pShippingGroup, Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPricingTools  method: updateShippingPriceInfo]");
		}
		double shippingPrice = TRUConstants.DOUBLE_ZERO;
		double shipSurcharge = TRUConstants.DOUBLE_ZERO;
		boolean isOnlyDonationItem = Boolean.TRUE;
		if(pCommerceItemRelationships != null && !pCommerceItemRelationships.isEmpty()){
			CommerceItem cItem = null;
			for(TRUShippingGroupCommerceItemRelationship sgCommItemRel : pCommerceItemRelationships){
				shipSurcharge += sgCommItemRel.getSurcharge();
				cItem = sgCommItemRel.getCommerceItem();
				if(cItem != null && !(cItem instanceof TRUDonationCommerceItem) && isOnlyDonationItem){
					isOnlyDonationItem = Boolean.FALSE;
				}
			}
			pShippingPriceInfo.setShippingSurcharge(round(shipSurcharge));
		}
		// Dependency form Checkout Team for ship rate price.
		if (pShippingGroup instanceof TRUHardgoodShippingGroup && !isOnlyDonationItem) {
			TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) pShippingGroup;
			shippingPrice = hardgoodShippingGroup.getShippingPrice();
			}
		// Updating the Amount and RawShipping Charges to Shipping Price Info Object. Need to checkout BRD to update the shipping amount based on the Qty
		//pShippingPriceInfo.setAmount(round(shippingPrice));
		
		if(pShippingGroup instanceof TRUHardgoodShippingGroup) {
			if(((TRUHardgoodShippingGroup)pShippingGroup).isMarkerShippingGroup()) {
				pShippingPriceInfo.setRawShipping(round(shippingPrice));
				pShippingPriceInfo.setAmount(round(shippingPrice));
			} else {
				pShippingPriceInfo.setRawShipping(TRUConstants.DOUBLE_ZERO);
				pShippingPriceInfo.setAmount(TRUConstants.DOUBLE_ZERO);
			}
		} else {
			pShippingPriceInfo.setRawShipping(round(shippingPrice));
			pShippingPriceInfo.setAmount(round(shippingPrice));
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: updateShippingPriceInfo]");
		}
	}

	/**
	 * This method will iterate all the commerce items in the order and checks whether the product/sku id is already.
	 * added to the order or not. If already added then return true else false will be returned.
	 * 
	 * @param pOrder
	 *            - Current order.
	 * @param pSkuId
	 *            - SKU id.
	 * @param pProductId
	 *            - Product id.
	 * @return boolean - returns true or false .
	 */
	@SuppressWarnings("unchecked")
	public boolean compareCommItemWithSkuId(Order pOrder, String pSkuId, String pProductId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPricingTools  method: compareCommItemWithSkuId]");
			vlogDebug("Order : {0} : SKU Id : {1} Product Id : {2}", pOrder, pSkuId, pProductId);
		}
		String catalogRefId = null;
		if (pOrder != null && pSkuId != null) {
			List<CommerceItem> commerceItems = pOrder.getCommerceItems();
			for (CommerceItem commerceItem : commerceItems) {
				catalogRefId = commerceItem.getCatalogRefId();
				if (catalogRefId.equals(pSkuId)) {
					vlogDebug("SKU : {0} already added to the current order : {1}", pSkuId, pOrder);
					return Boolean.TRUE;
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: compareCommItemWithSkuId]");
		}
		return Boolean.FALSE;
	}

	/**
	 * This method will return the value.
	 * 
	 * @param pPropertyName
	 *            property name to be fetched.
	 * @param pItem
	 *            repository item from which value is to be fetched.
	 * @return returns the object.
	 * 
	 */
	public Object getValueForProperty(String pPropertyName, RepositoryItem pItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPricingTools  method: getValueForProperty]");
			vlogDebug("pPropertyName : {0} and Item is : {1}", pPropertyName, pItem);
		}
		Object value = null;

		if (pItem != null) {
			String propertyName = pPropertyName;
			value = pItem.getPropertyValue(propertyName);
		}
		vlogDebug("Value for the pPropertyName : {0} is : {1} Item is : {2}", pPropertyName, value, pItem);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: getValueForProperty]");
		}
		return value;
	}

	/**
	 * This method returns the price items for child SKUs of product.
	 * 
	 * @param pProductItem
	 *            - Product item.
	 * @return priceItems - List of Price items.
	 */
	@SuppressWarnings("unchecked")
	public List<RepositoryItem> getPricesForChildSKUs(RepositoryItem pProductItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPricingTools  method: getPricesForChildSKUs]");
			vlogDebug("Product Item : {0} ", pProductItem);
		}
		RepositoryItem priceItem = null;
		List<RepositoryItem> priceItems = null;
		// Site site = SiteContextManager.getCurrentSite();
		if (pProductItem != null) {
			List<RepositoryItem> childSKUs = (List<RepositoryItem>) 
					pProductItem.getPropertyValue(getCatalogProperties().getChildSkusPropertyName());
			if (childSKUs != null && !childSKUs.isEmpty()) {
				priceItems = new ArrayList<RepositoryItem>();
				for (RepositoryItem childSKU : childSKUs) {
					// priceItem = getPriceForSKU(childSKU.getRepositoryId(), pProductItem.getRepositoryId(), site);
					vlogDebug("Price for SKU : {0} is : {1}", childSKU, priceItem);
					priceItems.add(priceItem);
				}
			}
		} else {
			if (isLoggingDebug()) {
				logDebug("Product item is null");
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: getPricesForChildSKUs]");
		}
		return priceItems;
	}

	/**
	 * This method will create/update the commerce item meta info item with the details of shipping group commerce item.
	 * relationship.
	 * 
	 * @param pPriceQuote
	 *            - ItemPriceinfo.
	 * @param pItem
	 *            - CommerceItem.
	 * @param pProfile
	 *            - Profile.
	 * @throws RepositoryException - if any.
	 */
	@SuppressWarnings("unchecked")
	public void updateMetaInfo(ItemPriceInfo pPriceQuote, CommerceItem pItem, RepositoryItem pProfile) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPricingTools  method: getPricesForChildSKUs]");
			vlogDebug("Item price info : {0} CommerceItem : {1} Profile : {2}", pPriceQuote, pItem, pProfile);
		}
		long shipItemRelQty = TRUConstants.LONG_ZERO;
		double salePrice = TRUConstants.DOUBLE_ZERO;
		String promotionId=null;
		TRUOrderTools orderTools = ((TRUOrderTools) getOrderTools());
		TRUItemPriceInfo priceQuote = (TRUItemPriceInfo) pPriceQuote;
		salePrice = priceQuote.getSalePrice();
		TRUShippingGroupCommerceItemRelationship shippingGroupCommerceItemRelationship = null;
		List<ShippingGroupCommerceItemRelationship> shippingGroupRelationships =
				pItem.getShippingGroupRelationships();
		Map<String,String> relAndDiscountMap = null;
		int shippingGroupRelationshipCount = pItem.getShippingGroupRelationshipCount();
		if (shippingGroupRelationships != null && !shippingGroupRelationships.isEmpty()) {
			double lAdjustedAmount = TRUConstants.DOUBLE_ZERO;
			double adjPossitiveVal = TRUConstants.DOUBLE_ZERO;
			if(shippingGroupRelationshipCount > TRUConstants._1){
				relAndDiscountMap = new HashMap<String, String>();
				for (Iterator<PricingAdjustment> iterator = pPriceQuote.getAdjustments().iterator(); iterator.hasNext();) {
					PricingAdjustment pricingAdjustment = iterator.next();
					if (pricingAdjustment.getPricingModel() != null){
						lAdjustedAmount = pricingAdjustment.getTotalAdjustment();
						adjPossitiveVal = -lAdjustedAmount;
						promotionId = pricingAdjustment.getPricingModel().getRepositoryId();
					}
				}
				for(ShippingGroupCommerceItemRelationship shipItemRel1 : shippingGroupRelationships){
					long shipIteRel1Qty = shipItemRel1.getQuantity();
					if(adjPossitiveVal > salePrice*shipIteRel1Qty){
						relAndDiscountMap.put(shipItemRel1.getId(),round(salePrice*shipIteRel1Qty)+TRUConstants.PIPE_STRING+promotionId);
						adjPossitiveVal -= salePrice*shipIteRel1Qty;
					} else if(adjPossitiveVal == salePrice*shipIteRel1Qty){
						relAndDiscountMap.put(shipItemRel1.getId(),round(salePrice*shipIteRel1Qty)+TRUConstants.PIPE_STRING+promotionId);
						adjPossitiveVal -= salePrice*shipIteRel1Qty;
					}else{
						relAndDiscountMap.put(shipItemRel1.getId(),round(adjPossitiveVal)+TRUConstants.PIPE_STRING+promotionId);
						adjPossitiveVal = TRUConstants.DOUBLE_ZERO;
					}
				}
			}
		}
		for(ShippingGroupCommerceItemRelationship shipItemRel : shippingGroupRelationships){
			shippingGroupCommerceItemRelationship = (TRUShippingGroupCommerceItemRelationship) shipItemRel;
			if(shippingGroupCommerceItemRelationship != null && 
					!(shippingGroupCommerceItemRelationship.getCommerceItem() instanceof TRUGiftWrapCommerceItem) &&
					!(shippingGroupCommerceItemRelationship.getCommerceItem() instanceof TRUDonationCommerceItem)){
				shipItemRelQty = shippingGroupCommerceItemRelationship.getQuantity();
				orderTools.createOrUpdateCommItemMetaInfo(shippingGroupCommerceItemRelationship, salePrice,
						TRUConstants.LONG_ZERO, shipItemRelQty, TRUConstants.DOUBLE_ZERO,priceQuote.getListPrice(), priceQuote, relAndDiscountMap);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: getPricesForChildSKUs]");
		}
	}

	/**
	 * This method will return the list for the SKU.
	 * @param pSkuId - SKU ID to get Price.
	 * @param pProductId - Product ID to get Price.
	 * @param pSite - Site.
	 * @return listPrice - List price item.
	 */
	public double getListPriceForSKU(String pSkuId, String pProductId,
			Site pSite) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPricingTools  method: getListPriceForSKU]");
			vlogDebug("SKU Id : {0} ", pSkuId);
		}
		RepositoryItem priceItem = null;
		RepositoryItem priceList = null;
		double listPrice = TRUConstants.DOUBLE_ZERO;
		if (pSite != null && pSite.getRepositoryId()!=null) {
			priceList =
					(RepositoryItem) pSite.getPropertyValue(getDefaultPriceListPropertyName());
			vlogDebug("Default list price list for site : {0} is  : {1}", pSite, priceList);
		}
		try {
			if(priceList != null && pSkuId != null){
				priceItem = getPriceListManager().getPrice(priceList, pProductId, pSkuId);
			}else{
				vlogDebug("Price list : {0} pProductId  : {1} pSkuId : {2}", priceList, pProductId, pSkuId);
			}
			if(priceItem != null){
				listPrice =(double) getValueForProperty(getPriceProperties().getListPricePropertyName(),priceItem);
			}
		} catch (PriceListException exc) {
			if (isLoggingError()) {
				vlogError(
						"PriceListException: while getting price for SKU : {0} Product : {1} with exception : {2}",
						pSkuId,
						pProductId,
						exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: getListPriceForSKU]");
		}
		return listPrice;
	}

	/**
	 * This method will return the sale for the SKU.
	 * @param pSkuId - SKU ID to get Price.
	 * @param pProductId - Product ID to get Price.
	 * @param pSite - Site.
	 * @return listPrice - Sale price item.
	 */
	public double getSalePriceForSKU(String pSkuId, String pProductId,
			Site pSite) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPricingTools  method: getSalePriceForSKU]");
			vlogDebug("SKU Id : {0} ", pSkuId);
		}
		RepositoryItem priceItem = null;
		RepositoryItem priceList = null;
		double salePrice = TRUConstants.DOUBLE_ZERO;
		if (pSite != null && pSite.getRepositoryId() != null) {
			priceList =
					(RepositoryItem) pSite.getPropertyValue(getDefaultSalePriceListPropertyName());
			vlogDebug("Default sale price list for site : {0} is  : {1}", pSite, priceList);
		}
		try {
			if(priceList != null && pSkuId != null){
				priceItem = getPriceListManager().getPrice(priceList, pProductId, pSkuId);
			}else{
				vlogDebug("Price list : {0} pProductId  : {1} pSkuId : {2}", priceList, pProductId, pSkuId);
			}
			
			if(priceItem != null){
				salePrice =(double) getValueForProperty(getPriceProperties().getListPricePropertyName(),priceItem);
			}
		} catch (PriceListException exc) {
			if (isLoggingError()) {
				vlogError(
						"PriceListException: while getting price for SKU : {0} Product : {1} with exception : {2}",
						pSkuId,
						pProductId,
						exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: getSalePriceForSKU]");
		}
		return salePrice;
	}
	
	/**
	 * This method will update the shipping prices in order level.
	 * @param pOrder - order instance.
	 * @param pShippingPriceInfo - the ShippingPriceInfo .
	 * @param pShippingGroup - the ShippingGroup.
	 */
	public void priceShippingInOrderPriceInfo(Order pOrder,
			ShippingGroup pShippingGroup, TRUShippingPriceInfo pShippingPriceInfo) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPricingTools  method: priceShippingInOrderPriceInfo]");
			vlogDebug(
					"pOrder : {0} ShippingPriceInfo : {1} pShippingGroup : {2} shippingPriceInfo",pOrder,
					pShippingGroup,pShippingPriceInfo);
		}
		double shipRate = TRUConstants.DOUBLE_ZERO;
		double shippingSurcharge = TRUConstants.DOUBLE_ZERO;
		double totalRawShipping = TRUConstants.DOUBLE_ZERO;
		// Updating the Amount and RawShipping Charges to Shipping Price Info Object. Need to checkout BRD to update the shipping amount based on the Qty
		if(pShippingGroup != null && pOrder != null && pShippingPriceInfo != null){
			TRUOrderPriceInfo  odrerPriceInfo = (TRUOrderPriceInfo) pOrder.getPriceInfo();
			if(pShippingGroup != null && pShippingPriceInfo != null && odrerPriceInfo != null){
				// Getting Value price from Order Price Info
				shipRate += odrerPriceInfo.getShipping();
				shippingSurcharge +=odrerPriceInfo.getTotalShippingSurcharge();
				totalRawShipping = odrerPriceInfo.getTotalRawShipping();
				// Getting Value price from Shipping Price Info
				shipRate += pShippingPriceInfo.getAmount();
				shippingSurcharge += pShippingPriceInfo.getShippingSurcharge();
				totalRawShipping += pShippingPriceInfo.getRawShipping();
				// Setting Price Value to Order Price Info
				odrerPriceInfo.setTotalRawShipping(round(totalRawShipping));
				odrerPriceInfo.setShipping(round(shipRate));
				odrerPriceInfo.setTotalShippingSurcharge(round(shippingSurcharge));
			}
			
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: priceShippingInOrderPriceInfo]");
		}
		
	}
	
	/**
	 * Method to get the default shipping surcharge from sku's lower48 Map.
	 * @param pAkhiOrLowe48ShipPrice - Map.
	 * @return String : Shipping Surcharge.
	 */
	public String getDefaultShipSurcharge(Map<String, String> pAkhiOrLowe48ShipPrice) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPricingTools  method: getDefaultShipSurcharge]");
			vlogDebug("pAkhiOrLowe48ShipPrice : {0}",pAkhiOrLowe48ShipPrice);
		}
		String shippingSurcharge = null;
		if(pAkhiOrLowe48ShipPrice == null || pAkhiOrLowe48ShipPrice.isEmpty()){
			return shippingSurcharge;
		}
		if(pAkhiOrLowe48ShipPrice.containsKey(TRUConstants.LOWER48_FIXEDSURCHARGE) && 
				(TRUConstants.ALPHABET_S.equalsIgnoreCase(pAkhiOrLowe48ShipPrice.get(TRUConstants.LOWER48_FIXEDSURCHARGE))
				|| TRUConstants.ALPHABET_F.equalsIgnoreCase(pAkhiOrLowe48ShipPrice.get(TRUConstants.LOWER48_FIXEDSURCHARGE)))){
			pAkhiOrLowe48ShipPrice.remove(TRUConstants.LOWER48_FIXEDSURCHARGE);
		}
		Set<Entry<String, String>> set = pAkhiOrLowe48ShipPrice.entrySet();
		List<Entry<String, String>> list = new ArrayList<Entry<String, String>>(set);
		//Logic to sort map based on Values
		Collections.sort( list, new Comparator<Map.Entry<String, String>>(){
		    public int compare( Map.Entry<String, String> pO1, Map.Entry<String, String> pO2 ){
		    	if( Double.valueOf(pO1.getValue()) != null &&  Double.valueOf(pO2.getValue()) != null){
            		return ( Double.valueOf(pO1.getValue()) ).compareTo( Double.valueOf(pO2.getValue()) );
            	}else{
            		return TRUConstants.INTEGER_NUMBER_ONE;
            	}
		    }
		 } );
		if(list != null && !list.isEmpty()){
			shippingSurcharge =  ((Map.Entry<String, String>)list.get(0)).getValue();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: getDefaultShipSurcharge]");
			vlogDebug("Returning with Shipping Surcharge Value : {0}",shippingSurcharge);
		}
		return shippingSurcharge;
	}
	
	/**
	 * This method is to trim.
	 * @param pAmount - Amount.
	 * @return TrimmedAmount - Returns amount.
	 */
	public String getDotTrimmedAmount(double pAmount){
		if (isLoggingDebug()) {
			logDebug("START from [Class: TRUPricingTools  method: getTrimmedAmount]");
			vlogDebug(" : {0}",pAmount);
		}
		DecimalFormat df = new DecimalFormat(TRUConstants.DOUBLE_DECIMAL);
		String formate = df.format(pAmount);
		double finalValue = Double.parseDouble(formate) ;
		String doubleAmount = Double.toString(finalValue); 
	//	String doubleAmount = Double.toString(pAmount);
		
		int amountIndex = doubleAmount.indexOf(TRUConstants.CHAR_DOT);
		if (isLoggingDebug()) {
			vlogDebug("  amountIndex : {0}",amountIndex);
		}
		String TrimmedAmount = doubleAmount.replace(TRUConstants.CHAR_DOT,TRUConstants.EMPTY);
		if (isLoggingDebug()) {
			vlogDebug("  TrimmedAmount : {0}",TrimmedAmount);
		}
		if((amountIndex + TRUConstants.ONE) == TrimmedAmount.length()){
			TrimmedAmount = new StringBuilder().append(TrimmedAmount).append(TRUConstants.DIGIT_ZERO).toString();
			if (isLoggingDebug()) {
				vlogDebug("  TrimmedAmount inside if : {0}",TrimmedAmount);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: getTrimmedAmount]");
			vlogDebug("  : {0}",TrimmedAmount);
		}
		
		return TrimmedAmount;
	}
	
	

	
	/**
	 * Holds ShippingMethodAndAKAHIdMap.
	 */
	private Map<String,String> mShippingMethodAndAKAHIdMap;
	/**
	 * Holds ShippingMethodAndLowe48IdMap.
	 */
	private Map<String,String> mShippingMethodAndLowe48IdMap;
	/**
	 * This method will return mShippingMethodAndAKAHIdMap.
	 *
	 * @return the mShippingMethodAndAKAHIdMap
	 */
	public Map<String, String> getShippingMethodAndAKAHIdMap() {
		return mShippingMethodAndAKAHIdMap;
	}
	/**
	 * This method will set mShippingMethodAndAKAHIdMap with pShippingMethodAndAKAHIdMap.
	 * @param pShippingMethodAndAKAHIdMap the mShippingMethodAndAKAHIdMap to set.
	 */
	public void setShippingMethodAndAKAHIdMap(
			Map<String, String> pShippingMethodAndAKAHIdMap) {
		this.mShippingMethodAndAKAHIdMap = pShippingMethodAndAKAHIdMap;
	}
	/**
	 * This method will return mShippingMethodAndLowe48IdMap.
	 *
	 * @return the mShippingMethodAndLowe48IdMap.
	 */
	public Map<String, String> getShippingMethodAndLowe48IdMap() {
		return mShippingMethodAndLowe48IdMap;
	}
	/**
	 * This method will set mShippingMethodAndLowe48IdMap with pShippingMethodAndLowe48IdMap.
	 * @param pShippingMethodAndLowe48IdMap the mShippingMethodAndLowe48IdMap to set.
	 */
	public void setShippingMethodAndLowe48IdMap(
			Map<String, String> pShippingMethodAndLowe48IdMap) {
		mShippingMethodAndLowe48IdMap = pShippingMethodAndLowe48IdMap;
	}
	/**
	 * This method will return mCatalogProperties.
	 *
	 * @return the mCatalogProperties.
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}
	/**
	 * This method will set mCatalogProperties with pCatalogProperties.
	 * @param pCatalogProperties the mCatalogProperties to set.
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}
	/**
	 * This method will return mTruConfiguration.
	 *
	 * @return the mTruConfiguration.
	 */

	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * This method will set mTruConfiguration with pTruConfiguration.
	 * @param pTruConfiguration the mTruConfiguration to set.
	 */
	public void setTruConfiguration(TRUConfiguration pTruConfiguration) {
		mTruConfiguration = pTruConfiguration;
	}
	/**
	 * This method will return mPriceProperties.
	 *
	 * @return the mPriceProperties.
	 */

	public TRUPriceProperties getPriceProperties() {
		return mPriceProperties;
	}
	/**
	 * This method will set mPriceProperties with pPriceProperties.
	 * @param pPriceProperties the mPriceProperties to set.
	 */
	public void setPriceProperties(TRUPriceProperties pPriceProperties) {
		mPriceProperties = pPriceProperties;
	}
	/**
	 * This method will return mDefaultPriceListPropertyName.
	 *
	 * @return the mDefaultPriceListPropertyName.
	 */
	public String getDefaultPriceListPropertyName() {
		return mDefaultPriceListPropertyName;
	}
	/**
	 * This method will set mDefaultPriceListPropertyName with pDefaultPriceListPropertyName.
	 * @param pDefaultPriceListPropertyName the mDefaultPriceListPropertyName to set.
	 */

	public void setDefaultPriceListPropertyName(String pDefaultPriceListPropertyName) {
		mDefaultPriceListPropertyName = pDefaultPriceListPropertyName;
	}
	/**
	 * This method will return mDefaultSalePriceListPropertyName.
	 *
	 * @return the mDefaultSalePriceListPropertyName.
	 */
	public String getDefaultSalePriceListPropertyName() {
		return mDefaultSalePriceListPropertyName;
	}
	/**
	 * This method will set mDefaultSalePriceListPropertyName with pDefaultSalePriceListPropertyName.
	 * @param pDefaultSalePriceListPropertyName the mDefaultSalePriceListPropertyName to set.
	 */

	public void setDefaultSalePriceListPropertyName(
			String pDefaultSalePriceListPropertyName) {
		mDefaultSalePriceListPropertyName = pDefaultSalePriceListPropertyName;
	}
	/**
	 * This method will return mPricingModelProperties.
	 *
	 * @return the mPricingModelProperties.
	 */
	public TRUPricingModelProperties getPricingModelProperties() {
		return mPricingModelProperties;
	}
	/**
	 * This method will set mPricingModelProperties with pPricingModelProperties.
	 * @param pPricingModelProperties the mPricingModelProperties to set.
	 */
	public void setPricingModelProperties(TRUPricingModelProperties pPricingModelProperties) {
		mPricingModelProperties = pPricingModelProperties;
	}

	/**
	 * Gets the TRU commerce property manager.
	 *
	 * @return the TRU commerce property manager
	 */
	public TRUCommercePropertyManager getTRUCommercePropertyManager() {
		return mTRUCommercePropertyManager;
	}

	/**
	 * Sets the TRU commerce property manager.
	 *
	 * @param pTRUCommercePropertyManager the new TRU commerce property manager.
	 */
	public void setTRUCommercePropertyManager(
			TRUCommercePropertyManager pTRUCommercePropertyManager) {
		mTRUCommercePropertyManager = pTRUCommercePropertyManager;
	}

	
	/**
	 * This method is overridden to call the update the Applied and not applied coupon.
	 * codes in Order after each re-price order.
	 * 
	 * @param pPricingOperation : String - Pricing Operation.
	 * @param pOrder : Order Object.
	 * @param pPricingModels : PricingModelHolder.
	 * @param pLocale : Locale.
	 * @param pProfile : RepositoryItem.
	 * @param pExtraParameters : Map.
	 * @throws PricingException : PricingException if any.
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void performPricingOperation(String pPricingOperation, Order pOrder,
			PricingModelHolder pPricingModels, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters)
			throws PricingException {
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.performPricingOperation()methods.STARTS");
		}
		TRUOrderImpl orderImpl =(TRUOrderImpl)pOrder;
		
		List<TRUHardgoodShippingGroup> allHGShpGrps = orderImpl.getAllHardgoodShippingGroups();
		List<TRUHardgoodShippingGroup> allHGShpGrps2 = orderImpl.getAllHardgoodShippingGroups();
		
		Map<String,Set<String>> shippingIdsMapByUniqueKey = new ConcurrentHashMap<>();
		Map<String,TRUHardgoodShippingGroup> shipGrpMapContaner = new ConcurrentHashMap<>();
		String key = null;
		Set<String> values = null;
		boolean matchFound = false;
		List<String> alreadyFiltered = new ArrayList<String>();
		for(TRUHardgoodShippingGroup shippingGroup : allHGShpGrps){
			if(alreadyFiltered.contains(shippingGroup.getId())) {
				continue;
			}
			matchFound = false;
			shippingGroup.setMarkerShippingGroup(false);
			shippingGroup.setDependencyShippingGruoupIds(new ArrayList<String>());
			shippingGroup.getDependencyShippingGruoupIds().add(shippingGroup.getId());
			alreadyFiltered.add(shippingGroup.getId());
			for(TRUHardgoodShippingGroup shippingGroup2 : allHGShpGrps2) {
				if(!shippingGroup.getId().equals(shippingGroup2.getId()) &&
						StringUtils.isNotBlank(shippingGroup.getNickName()) &&
						StringUtils.isNotBlank(shippingGroup.getShippingMethod()) &&
						shippingGroup.getNickName().equals(shippingGroup2.getNickName()) && 
						shippingGroup.getShippingMethod().equals(shippingGroup2.getShippingMethod())){					
					key = shippingGroup.getNickName()+TRUConstants.HYPHEN+shippingGroup2.getShippingMethod();					
					values = shippingIdsMapByUniqueKey.get(key);
					if(values == null){
						values = new HashSet<String>();
					}
					values.add(shippingGroup2.getId());
					shippingIdsMapByUniqueKey.put(key, values);
					matchFound = true;
					shippingGroup.setMarkerShippingGroup(true);
					if(shippingGroup.getDependencyShippingGruoupIds() == null) {
						shippingGroup.setDependencyShippingGruoupIds(new ArrayList<String>());
					}
					if(!shippingGroup.getDependencyShippingGruoupIds().contains(shippingGroup2.getId())) {
						shippingGroup.getDependencyShippingGruoupIds().add(shippingGroup2.getId());
					}
					shippingGroup2.setMarkerShippingGroup(false);
					if(shippingGroup2.getDependencyShippingGruoupIds() == null) {
						shippingGroup2.setDependencyShippingGruoupIds(new ArrayList<String>());
					}
					if(!shippingGroup2.getDependencyShippingGruoupIds().contains(shippingGroup.getId())) {
						shippingGroup2.getDependencyShippingGruoupIds().add(shippingGroup.getId());
					}
					alreadyFiltered.add(shippingGroup2.getId());
					//shippingGroup2.setShippingPrice(0);
				}
			}
			if(!matchFound) {
				key = shippingGroup.getNickName()+TRUConstants.HYPHEN+shippingGroup.getShippingMethod();	
				values = new HashSet<String>();
				values.add(shippingGroup.getId());
				shippingIdsMapByUniqueKey.put(key, values);
				shippingGroup.setMarkerShippingGroup(true);
			}			
			shipGrpMapContaner.put(shippingGroup.getId(),shippingGroup);					
			values = new HashSet<String>();	
			key=null;
		}
		
		// Calling OOTB Methods
		super.performPricingOperation(pPricingOperation, pOrder, pPricingModels,
				pLocale, pProfile, pExtraParameters);
		if(pExtraParameters == null || pExtraParameters.isEmpty() || pOrder == null){
			return;
		}
		
		List<TRUHardgoodShippingGroup> allHGShpGrps3 = orderImpl.getAllHardgoodShippingGroups();
		for(TRUHardgoodShippingGroup shippingGroup : allHGShpGrps3){
			if(!shippingGroup.isMarkerShippingGroup()) {
				String shipId = shippingGroup.getDependencyShippingGruoupIds().get(0);
				ShippingGroup shippingGroup2;
				try {
					shippingGroup2 = pOrder.getShippingGroup(shipId);
					shippingGroup.setActualAmount(((TRUHardgoodShippingGroup)shippingGroup2).getPriceInfo().getAmount());
				} catch (ShippingGroupNotFoundException se) {
					vlogError("ShippingGroupNotFoundException TRUPricingTools.performPricingOperation() : {0}", se);
				} catch (InvalidParameterException ie) {
					vlogError("InvalidParameterException TRUPricingTools.performPricingOperation() : {0}", ie);
				}
			}
		}
		
		// Calling method to set the Coupon Info in Order.
		updateCouponInfoInOrder(pExtraParameters, pOrder);
		//updateCouponInfoInOrder(pExtraParameters,pOrder);
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.performPricingOperation() methods.END");
		}
	}

	/**
	 * Method to calculate the Order Discount Share.
	 * 
	 * @param pOrder - Order Object
	 * @param pProfileCouponStatus 
	 */
	@SuppressWarnings("unchecked")
	public void calculateOrderDiscountShare(Order pOrder, List<RepositoryItem> pProfileCouponStatus) {
		if(isLoggingDebug()){
			logDebug("Enter into [Class: TRUPricingTools  method: calculateOrderDiscountShare()]");
		}
		if(pOrder == null){
			return;
		}
		OrderPriceInfo priceInfo = pOrder.getPriceInfo();
		if (priceInfo == null) {
			return;
		}
		List<PricingAdjustment> adjustments = priceInfo.getAdjustments();
		if (adjustments == null || adjustments.isEmpty()) {
			return;
		}
		RepositoryItem promotion = null;
		TRUPromotionTools promotionTools = (TRUPromotionTools) getOrderTools().getProfileTools().getPromotionTools();
		TRUOrderTools orderTools = (TRUOrderTools) getOrderTools();
		Map<String, String> singleUseCoupon = ((TRUOrderImpl)pOrder).getSingleUseCoupon();
		Map<String, String> forProration = new HashMap<String, String>();
		forProration.putAll(singleUseCoupon);
		for (PricingAdjustment priceAdj : adjustments) {
			promotion = priceAdj.getPricingModel();
			if(promotion == null){
				continue;
			}
			String singleUsedCoupon = null;
			List<RepositoryItem> qualifiedSKUforPromo=new ArrayList<RepositoryItem>();
			List<RepositoryItem> excludedSKUforPromo=new ArrayList<RepositoryItem>();
			promotionTools.getQualifiedSKUforPromoFromCatalog(pOrder,promotion,qualifiedSKUforPromo,excludedSKUforPromo);
			List<String> qualifyBulkSkuIds = promotionTools.getQualifyBulkSkuIds(promotion);
			List<String> excludedBulkSkuIds = promotionTools.getExcludedBulkSkuIds(promotion);
			if (isLoggingDebug()) {
				vlogDebug("qualifiedSKUforPromo : {0} qualifyBulkSkuIds : {1}", qualifiedSKUforPromo,qualifyBulkSkuIds);
				vlogDebug("excludedSKUforPromo : {0} excludedBulkSkuIds : {1}", excludedSKUforPromo,excludedBulkSkuIds);
			}
			singleUsedCoupon = getCoupon(pProfileCouponStatus,promotion,forProration);
			// Calling method to get the eligible items for discount share.
			List<CommerceItem> eligibleitemForOrderDis = orderTools.getEligibleitemForOrderDis(excludedSKUforPromo,
					qualifiedSKUforPromo, qualifyBulkSkuIds,excludedBulkSkuIds, pOrder);
			// Calling method to update the discount share to order info. 
			updateOrderDiscountShare(eligibleitemForOrderDis, promotion, pOrder,priceAdj,singleUsedCoupon);
		}
		if(isLoggingDebug()){
			logDebug("Exiting from [Class: TRUPricingTools  method: calculateOrderDiscountShare]");
		}
	}
	
	/**
	 * Method is used to update the order discount share in ItemPriceInfo.
	 * 
	 * @param pCommerceItems - List of Commerce Items.
	 * @param pPricingModel - Promotion RepositoryItem Item.
	 * @param pOrder - Order Object
	 * @param pPriceAdj - PricingAdjustment Object
	 * @param pSingleUsedCoupon - String Single Used Coupon 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void updateOrderDiscountShare(List<CommerceItem> pCommerceItems,
			RepositoryItem pPricingModel, Order pOrder,
			PricingAdjustment pPriceAdj, String pSingleUsedCoupon) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPricingTools  method: updateOrderDiscountShare]");
			vlogDebug("pCommerceItem : {0} pPricingModel : {1}", pCommerceItems,pPricingModel);
			vlogDebug("pOrder : {0}", pOrder);
		}
		TRUOrderTools orderTools = (TRUOrderTools) getOrderTools();
		if(pCommerceItems == null || pCommerceItems.isEmpty()){
			return;
		}
		// calling method to get the raw sub total of qualified items.
		double rawSubtotal = orderTools.getOrderToatal(pCommerceItems);
		double discountAmountToShare = -pPriceAdj.getAdjustment();
		if(discountAmountToShare <=  TRUConstants.ZERO){
			return;
		}
		double shareSoFar = TRUConstants.DOUBLE_ZERO;
		TRUItemPriceInfo priceInfo = null;
		for(CommerceItem ci : pCommerceItems){
			priceInfo = (TRUItemPriceInfo) ci.getPriceInfo();
			Map<String,String> truItemDiscountShare = priceInfo.getTruItemDiscountShare();
			double proratedAmount = TRUConstants.DOUBLE_ZERO;
			String promoID = pPricingModel.getRepositoryId();
			String value = null;
			if(truItemDiscountShare != null){
				for(Map.Entry<String,String> entry : truItemDiscountShare.entrySet()){
					value = entry.getValue();
					if(StringUtils.isNotBlank(value)){
						proratedAmount += Double.valueOf((value.split(TRUConstants.PIPE_DELIMETER))[TRUConstants.INT_ZERO]);
					}
				}
			}
			Map<String,Double> truOrderDiscountShare = priceInfo.getTruOrderDiscountShare();
			if(truOrderDiscountShare != null){
				for(Map.Entry<String,Double> entry : truOrderDiscountShare.entrySet()){
					if(StringUtils.isBlank(entry.getKey()) || entry.getKey().equalsIgnoreCase(promoID+TRUConstants.PIPE_STRING+pSingleUsedCoupon)){
						continue;
					}
					proratedAmount += entry.getValue();
				}
			}
			double discountShare = round((discountAmountToShare*((priceInfo.getSalePrice()*ci.getQuantity())-proratedAmount))/rawSubtotal);
			shareSoFar += round(discountShare);
			if(pCommerceItems.indexOf(ci)+TRUConstants.INTEGER_NUMBER_ONE == pCommerceItems.size()){
				discountShare += discountAmountToShare-shareSoFar;
			}
			Map newOrderShare = priceInfo.getTruOrderDiscountShare();
			double oldShare = TRUConstants.DOUBLE_ZERO;
			if(newOrderShare.get(promoID+TRUConstants.PIPE_STRING+pSingleUsedCoupon) != null){
				oldShare = (double) newOrderShare.get(promoID+TRUConstants.PIPE_STRING+pSingleUsedCoupon);
			}
			newOrderShare.put(promoID+TRUConstants.PIPE_STRING+pSingleUsedCoupon, round(discountShare+oldShare));
			priceInfo.setTruOrderDiscountShare(newOrderShare);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPricingTools  method: updateOrderDiscountShare]");
		}
	}

	/**
	 * Method to set the Coupon Info in Order.
	 * 
	 * @param pExtraParameters : Map
	 * @param pOrder : Order Object
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void updateCouponInfoInOrder(Map pExtraParameters, Order pOrder) {
		if(isLoggingDebug()){
			logDebug("Enter into [Class: TRUPricingTools  method: updateCouponInfoInOrder]");
			vlogDebug("pExtraParameters:{0} pOrder : {1}", pExtraParameters,pOrder);
		}
		if(pExtraParameters == null || pExtraParameters.isEmpty() || pOrder == null){
			return;
		}
		TRUOrderImpl orderImpl = null;
		if(!(pOrder instanceof TRUOrderImpl)){
			return;
		}
		orderImpl = (TRUOrderImpl) pOrder;
		TRUClaimableTools claimableTools = (TRUClaimableTools) ((TRUOrderManager)getOrderManager()).
				getClaimableManager().getClaimableTools();
		Map<String, String> appliedCodeFromOrder = orderImpl.getSingleUseCoupon();
		Map<String, String> notAppliedCodeFromOrder = orderImpl.getNotAppliedCouponCode();
		List<RepositoryItem> allPromo = (List<RepositoryItem>) pExtraParameters.get(TRUConstants.LIST_OF_PROMOTIONS);
		String removedCouponCode = (String) pExtraParameters.get(TRUConstants.REMOVED_COUPON_CODE);
		TRUPromotionTools promotionTools = (TRUPromotionTools) getOrderTools().getProfileTools().getPromotionTools();
		List<RepositoryItem> allCouponItems = null;
		List<String> allCouponIds = null;
		if(isLoggingDebug()){
			vlogDebug("allPromo:{0} removedCouponCode : {1} appliedCodeFromOrder : {2} notAppliedCodeFromOrder :{3}"
					, allPromo,removedCouponCode,appliedCodeFromOrder,notAppliedCodeFromOrder);
		}
		if(allPromo != null && !allPromo.isEmpty()){
			allCouponItems = claimableTools.getAllCouponItemFormPromotion(allPromo);
			allCouponIds = claimableTools.getAllCouponIds(allCouponItems);
			if(isLoggingDebug()){
				vlogDebug("allCouponItems:{0} allCouponIds : {1}", allCouponItems,allCouponIds);
			}
		}
		Map<String, String> couponCodeFromOrder = null;
		if(appliedCodeFromOrder != null && !appliedCodeFromOrder.isEmpty()){
			for(Map.Entry<String, String> entrySet : appliedCodeFromOrder.entrySet()){
				if(couponCodeFromOrder == null){
					couponCodeFromOrder = new HashMap<String, String>();
				}
				couponCodeFromOrder.put(entrySet.getKey(), entrySet.getValue());
			}
		}
		if(notAppliedCodeFromOrder != null && !notAppliedCodeFromOrder.isEmpty()){
			for(Map.Entry<String, String> entrySet : notAppliedCodeFromOrder.entrySet()){
				if(couponCodeFromOrder == null){
					couponCodeFromOrder = new HashMap<String, String>();
				}
				couponCodeFromOrder.put(entrySet.getKey(), entrySet.getValue());
			}
		}
		List<RepositoryItem> coupons = null;
		List<RepositoryItem> activePromotions = null;
		List<String> twentyDigitCode = null;
		TRUPricingModelProperties pricingModelProperties = getPricingModelProperties();
		TRUPropertyManager propertyManager = ((TRUOrderManager)getOrderManager()).getPropertyManager();
		try {
			RepositoryItem profile = getProfile(orderImpl.getProfileId());
			if(isLoggingDebug()){
				vlogDebug("profile:{0}", profile);
			}
			if(profile != null){
				twentyDigitCode = new ArrayList<String>();
				activePromotions = (List<RepositoryItem>) profile.getPropertyValue(promotionTools.getActivePromotionsProperty());
				if(isLoggingDebug()){
					vlogDebug("activePromotions:{0}", activePromotions);
				}	
				Map<String, String> notAppliedPromoName = null;
				Map<String, String> appliedPromoName = null;
				if(activePromotions != null && !activePromotions.isEmpty()){
					notAppliedPromoName = new HashMap<String, String>();
					appliedPromoName = new HashMap<String, String>();
					RepositoryItem promotion = null;
					Object sucnCode = null;
					for (RepositoryItem promotionStatus : activePromotions) {
						sucnCode = promotionStatus.getPropertyValue(propertyManager.getAppliedCodeValuePropertyName());
						if(sucnCode != null){
							twentyDigitCode.add((String)sucnCode);
						}
						coupons = (List<RepositoryItem>) promotionStatus.getPropertyValue(promotionTools.getPromoStatusCouponsPropertyName());
						promotion = (RepositoryItem) promotionStatus.getPropertyValue(promotionTools.getBasePromotionItemType());
						if(isLoggingDebug()){
							vlogDebug("coupons:{0}", coupons);
						}
						if (coupons != null && !coupons.isEmpty()) {
							for (RepositoryItem coupon : coupons) {
								if(allPromo != null && allPromo.contains(promotion)){
									appliedPromoName.put(coupon.getRepositoryId(),
											(String)promotion.getPropertyValue(pricingModelProperties.getDisplayName()));
								}else{
									notAppliedPromoName.put(coupon.getRepositoryId(),
											(String)promotion.getPropertyValue(pricingModelProperties.getDisplayName()));
								}
								if(couponCodeFromOrder == null){
									couponCodeFromOrder = new HashMap<String, String>();
								}
								if(!couponCodeFromOrder.containsValue(coupon.getRepositoryId())){
									if(sucnCode != null){
										couponCodeFromOrder.put((String)sucnCode, coupon.getRepositoryId());
									}else{
										couponCodeFromOrder.put(coupon.getRepositoryId(), coupon.getRepositoryId());
									}
								}
							}
						}
					}
				}
				orderImpl.setNotAppliedPromoNames(notAppliedPromoName);
				orderImpl.setAppliedPromoNames(appliedPromoName);
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError("RepositoryException : {0}", e);
			}
		}
		if(isLoggingDebug()){
			vlogDebug("Total Available Coupon Ids in Order:{0}", couponCodeFromOrder);
		}
		Map<String, String> appliedPromoCodes = null;
		Map<String, String> notAppliedPromoCodes = null;
		if(couponCodeFromOrder != null && !couponCodeFromOrder.isEmpty()){
			for(Map.Entry<String, String> lCouponId : couponCodeFromOrder.entrySet()){
				if(allCouponIds != null && allCouponIds.contains(lCouponId.getValue())){
					if(appliedPromoCodes == null){
						appliedPromoCodes = new HashMap<String, String>();
					}
					appliedPromoCodes.put(lCouponId.getKey(),lCouponId.getValue());
				}else{
					if(notAppliedPromoCodes == null){
						notAppliedPromoCodes = new HashMap<String, String>();
					}
					notAppliedPromoCodes.put(lCouponId.getKey(),lCouponId.getValue());
				}
			}
		}
		
		if(isLoggingDebug()){
			vlogDebug("Final Applied Code to Order :{0} and Not Applied Coupon Code", appliedPromoCodes,notAppliedPromoCodes);
		}
		if((appliedPromoCodes != null && !appliedPromoCodes.isEmpty()) && StringUtils.isNotBlank(removedCouponCode)
				&& appliedPromoCodes.containsValue(removedCouponCode)){
			for(Map.Entry<String, String> lCouponId : appliedPromoCodes.entrySet()){
				if(lCouponId.getValue().equals(removedCouponCode)){
					if(twentyDigitCode != null && twentyDigitCode.contains(lCouponId.getKey())){
						continue;
					}else{
						appliedPromoCodes.remove(lCouponId.getKey());
						break;
					}
				}
			}
		}
		orderImpl.setSingleUseCoupon(appliedPromoCodes);
		if((notAppliedPromoCodes != null && !notAppliedPromoCodes.isEmpty()) && StringUtils.isNotBlank(removedCouponCode)
				&& notAppliedPromoCodes.containsValue(removedCouponCode)){
			for(Map.Entry<String, String> lCouponId : notAppliedPromoCodes.entrySet()){
				if(lCouponId.getValue().equals(removedCouponCode)){
					if(twentyDigitCode != null && twentyDigitCode.contains(lCouponId.getKey())){
						continue;
					}else{
						notAppliedPromoCodes.remove(lCouponId.getKey());
						break;
					}
				}
			}
		}
		orderImpl.setNotAppliedCouponCode(notAppliedPromoCodes);
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.updateCouponInfoInOrder()methods.END");
		}
	}
	
	/**
	 * This method is used to get the Target and Qualified Items for Promotions and call the method to prorate it.
	 * 
	 * @param pOrder - Order Object
	 * @param pExtraParameters - Map Object
	 * @param pProfileCouponStatus 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void calculateItemDiscountShare(Order pOrder, Map pExtraParameters, List<RepositoryItem> pProfileCouponStatus) {
		if(isLoggingDebug()){
			logDebug("Enter into [Class: TRUPricingTools  method: calculateItemDiscountShare]");
			vlogDebug("pExtraParameters:{0} pOrder : {1}", pExtraParameters,pOrder);
		}
		if(pOrder == null || pExtraParameters == null){
			return;
		}
		Map<String, Map<String, Long>> targetItem = null;
		if(pExtraParameters.get(TRUConstants.TARGET_ITEM) != null){
			targetItem = (Map<String, Map<String, Long>>) pExtraParameters.get(TRUConstants.TARGET_ITEM);
		}
		if(isLoggingDebug()){
			vlogDebug("targetItem : {0}", targetItem);
		}
		Map<String, Map<String, Long>> qualifyItem = null;
		if(pExtraParameters.get(TRUConstants.QUALIFIED_ITEM) != null){
			qualifyItem = (Map<String, Map<String, Long>>) pExtraParameters.get(TRUConstants.QUALIFIED_ITEM);
		}
		if(isLoggingDebug()){
			vlogDebug("qualifyItem : {0}", qualifyItem);
		}
		if(targetItem == null || targetItem.isEmpty()){
			return;
		}
		String promoId = null;
		Map<String, Long> qualifiedItemAndQty = null;
		Map<String, Long> targetedItemAndQty = null;
		Map<String, String> singleUseCoupon = ((TRUOrderImpl)pOrder).getSingleUseCoupon();
		Map<String, String> forProration = new HashMap<String, String>();
		forProration.putAll(singleUseCoupon);
		for(Map.Entry<String, Map<String, Long>> entry : targetItem.entrySet()){
			promoId = entry.getKey();
			targetedItemAndQty = entry.getValue();
			if(isLoggingDebug()){
				vlogDebug("targetedItemAndQty : {0} promoId : {1}", targetedItemAndQty,promoId);
			}
			if(targetedItemAndQty == null || targetedItemAndQty.isEmpty() || StringUtils.isBlank(promoId)){
				continue;
			}
			if(qualifyItem != null){
				qualifiedItemAndQty = qualifyItem.get(promoId);
			}
			if(isLoggingDebug()){
				vlogDebug("qualifiedItemAndQty : {0}", qualifiedItemAndQty);
			}
			// Calling method to check for gift wrap promotion.
			boolean isGiftWrapPromo = chekIfGiftWrapPromotion(targetedItemAndQty,pOrder);
			if(qualifiedItemAndQty == null || qualifiedItemAndQty.isEmpty() || isGiftWrapPromo){
				// Calling method to set the promotion sharing only in targeted Item, if Qualifier is null or empty.
				setItemDiscountOnTargetItems(targetedItemAndQty,promoId,pOrder,pProfileCouponStatus,forProration);
			}else{
				// Calling method to set the promotion sharing on target and qualifier items.
				setItemDiscountOnTargetAndQulItems(qualifiedItemAndQty,targetedItemAndQty,promoId,pOrder,pProfileCouponStatus,forProration);
			}
		}
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.calculateItemDiscountShare() methods.END");
		}
	}
	
	/**
	 * Method to share the discount only on Targeted Items.
	 * 
	 * @param pTargetedItemAndQty - Map - Qualified Item and Quantity Map
	 * @param pPromoId - String - Promotion Id
	 * @param pOrder - Order Object
	 * @param pForProration 
	 * @param pProfileCouponStatus 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void setItemDiscountOnTargetItems(
			Map<String, Long> pTargetedItemAndQty, String pPromoId, Order pOrder, List<RepositoryItem> pProfileCouponStatus,
			Map<String, String> pForProration) {
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.setItemDiscountOnTargetItems() methods.STARTS");
			vlogDebug("pTargetedItemAndQty:{0} pOrder : {1} pPromoId : {2}", pTargetedItemAndQty,pOrder,pPromoId);
		}
		if(pTargetedItemAndQty == null || pTargetedItemAndQty.isEmpty() || pOrder == null ||
				StringUtils.isBlank(pPromoId)){
			return;
		}
		String skuId = null;
		long qty = TRUConstants.LONG_ZERO;
		for(Map.Entry<String, Long> qulEntry : pTargetedItemAndQty.entrySet()){
			skuId = qulEntry.getKey();
			qty = qulEntry.getValue();
			try {
				List<CommerceItem> commerceItemsByCatalogRefId = pOrder.getCommerceItemsByCatalogRefId(skuId);
				if(isLoggingDebug()){
					vlogDebug("skuId:{0} commerceItemsByCatalogRefId : {1}", skuId,commerceItemsByCatalogRefId);
				}
				if(commerceItemsByCatalogRefId == null || commerceItemsByCatalogRefId.isEmpty()){
					continue;
				}
				TRUItemPriceInfo itemPriceInfo = null;
				for(CommerceItem cItem : commerceItemsByCatalogRefId){
					itemPriceInfo = (TRUItemPriceInfo) cItem.getPriceInfo();
					List<PricingAdjustment> adjustments = itemPriceInfo.getAdjustments();
					if(adjustments == null || adjustments.isEmpty()){
						continue;
					}
					RepositoryItem promotion = null;
					String singleUsedCoupon = null;
					for(PricingAdjustment priceAdj : adjustments){
						promotion = priceAdj.getPricingModel();
						if(promotion == null){
							continue;
						}
						singleUsedCoupon = getCoupon(pProfileCouponStatus, promotion,pForProration);
						double oldShare = TRUConstants.DOUBLE_ZERO;
						String value = null;
						if(pPromoId.equals(promotion.getRepositoryId())){
							Map truItemDiscountShare = itemPriceInfo.getTruItemDiscountShare();
							if(truItemDiscountShare.get(pPromoId+TRUConstants.PIPE_STRING+singleUsedCoupon+TRUConstants.PIPE_STRING+TRUConstants.PROMOTION_TARGETER_ITEM_LABEL) != null){
								value = (String)truItemDiscountShare.get(pPromoId+TRUConstants.PIPE_STRING+singleUsedCoupon+TRUConstants.PIPE_STRING+TRUConstants.PROMOTION_TARGETER_ITEM_LABEL);
							}
							if(StringUtils.isNotBlank(value)){
								oldShare = Double.valueOf((value.split(TRUConstants.PIPE_DELIMETER))[TRUConstants.INT_ZERO]);
							}
							truItemDiscountShare.put(pPromoId+TRUConstants.PIPE_STRING+singleUsedCoupon+TRUConstants.PIPE_STRING+TRUConstants.PROMOTION_TARGETER_ITEM_LABEL,-priceAdj.getTotalAdjustment()+oldShare+TRUConstants.PIPE_STRING+qty);
							itemPriceInfo.setTruItemDiscountShare(truItemDiscountShare);
						}
					}
				}
			} catch (CommerceItemNotFoundException e) {
				if (isLoggingError()) {
					vlogError("CommerceItemNotFoundException : {0}", e);
				}
			} catch (InvalidParameterException e) {
				if (isLoggingError()) {
					vlogError("InvalidParameterException : {0}", e);
				}
			}
		}
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.setItemDiscountOnTargetItems() methods.END");
		}
	}

	/**
	 * Method is used to set the item discount on qualified and targeted items.
	 * 
	 * @param pQualifiedItemAndQty - Map - Qualified Items and quantity Map
	 * @param pTargetedItemAndQty - - Map - Target Items and quantity Map
	 * @param pPromoId - String - Promotion ID
	 * @param pOrder - Order Object
	 * @param pForProration 
	 * @param pProfileCouponStatus 
	 */
	public void setItemDiscountOnTargetAndQulItems(
			Map<String, Long> pQualifiedItemAndQty,
			Map<String, Long> pTargetedItemAndQty, String pPromoId, Order pOrder, 
			List<RepositoryItem> pProfileCouponStatus, Map<String, String> pForProration) {
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.setItemDiscountOnTargetAndQulItems() methods.STARTS");
			vlogDebug("pQualifiedItemAndQty:{0} pTargetedItemAndQty : {1}", pQualifiedItemAndQty,pTargetedItemAndQty);
			vlogDebug("pPromoId:{0} pOrder : {1}", pPromoId,pOrder);
		}
		if(pQualifiedItemAndQty == null || pQualifiedItemAndQty.isEmpty() || pTargetedItemAndQty == null ||
				pTargetedItemAndQty.isEmpty() || pOrder == null || StringUtils.isBlank(pPromoId)){
			return;
		}
		TRUPromotionTools promotionTools = (TRUPromotionTools) getOrderTools().getProfileTools().getPromotionTools();
		RepositoryItem promotionFromPromoId = promotionTools.getPromotionFromPromoId(pPromoId);
		String singleUsedCoupon = getCoupon(pProfileCouponStatus, promotionFromPromoId,pForProration);
		// Calling method to get the total of the qualified items.
		double totalAmount = getQualifiedItemAmount(pQualifiedItemAndQty,pOrder,pPromoId,singleUsedCoupon);
		// Calling method to populate the discount share on qualified and targeted items info object.
		populateItemDiscountShare(pQualifiedItemAndQty,pTargetedItemAndQty,pPromoId,pOrder,totalAmount,singleUsedCoupon);
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.setItemDiscountOnTargetAndQulItems() methods.END");
		}
	}
	
	/**
	 * Method to get the Qualified Item amount.
	 * 
	 * @param pQualifiedItemAndQty - Map - Qualified Items and quantity Map
	 * @param pOrder - Order Object
	 * @param pPromoId - String - Promotion ID
	 * @param pSingleUsedCoupon 
	 * @return double - Sum of all qualified item amount.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private double getQualifiedItemAmount(Map<String, Long> pQualifiedItemAndQty, Order pOrder, 
			String pPromoId, String pSingleUsedCoupon) {
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.getQualifiedItemAmount() methods.STARTS");
			vlogDebug("pQualifiedItemAndQty:{0} pOrder : {1} pPromoId : {2}", pQualifiedItemAndQty,pOrder,pPromoId);
		}
		double qualItemsAmount = TRUConstants.DOUBLE_ZERO;
		if(pQualifiedItemAndQty == null || pQualifiedItemAndQty.isEmpty() || pOrder == null){
			return qualItemsAmount;
		}
		String skuId = null;
		long qty = TRUConstants.LONG_ZERO;
		List<CommerceItem> cItems = null;
		for(Map.Entry<String, Long> entry : pQualifiedItemAndQty.entrySet()){
			skuId = entry.getKey();
			qty = entry.getValue();
			try {
				cItems = pOrder.getCommerceItemsByCatalogRefId(skuId);
				if(isLoggingDebug()){
					vlogDebug("skuId:{0} qty : {1} cItems : {2}", skuId,qty,cItems);
				}
				if(cItems == null || cItems.isEmpty()){
					continue;
				}
				TRUItemPriceInfo info = null;
				for(CommerceItem cItem : cItems){
					if(cItem.getPriceInfo() == null){
						continue;
					}
					info = (TRUItemPriceInfo) cItem.getPriceInfo();
					Map truItemDiscountShare = info.getTruItemDiscountShare();
					double discountShare = TRUConstants.DOUBLE_ZERO;
					if(truItemDiscountShare != null && !truItemDiscountShare.isEmpty()){
						Iterator it = truItemDiscountShare.entrySet().iterator();
				    	while (it.hasNext()) {
				    		Map.Entry discountMap  = (Map.Entry)it.next();
				    		String value = null;
							if(discountMap.getValue() != null && discountMap.getValue() instanceof String && 
									!discountMap.getKey().equals(pPromoId+TRUConstants.PIPE_STRING+pSingleUsedCoupon+TRUConstants.PIPE_STRING+TRUConstants.PROMOTION_QUALIFIER_ITEM_LABEL)){
								value = ((String)discountMap.getValue());
							}
							if(StringUtils.isNotBlank(value)){
								discountShare += Double.valueOf((value.split(TRUConstants.PIPE_DELIMETER))[TRUConstants.INT_ZERO]);
							}
						}
					}
					qualItemsAmount += info.getSalePrice()*qty-discountShare;
				}
			} catch (CommerceItemNotFoundException e) {
				if (isLoggingError()) {
					vlogError("CommerceItemNotFoundException : {0}", e);
				}
			} catch (InvalidParameterException e) {
				if (isLoggingError()) {
					vlogError("InvalidParameterException : {0}", e);
				}
			}
		}
		if(isLoggingDebug()){
			vlogDebug("Returning with qualItemsAmount", qualItemsAmount);
			vlogDebug("TRUPricingTools.getQualifiedItemAmount() methods.END");
		}
		return qualItemsAmount;
	}

	/**
	 * Method to populate the discount share on Item Indo object.
	 * 
	 * @param pQualifiedItemAndQty - Map - Qualified Items and quantity Map
	 * @param pTargetedItemAndQty - Map - Target Items and quantity Map
	 * @param pPromoId - String - Promotion Id
	 * @param pOrder - Order Object
	 * @param pTotalAmount - double - Total Amount of Qualified Items.
	 * @param pSingleUsedCoupon 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void populateItemDiscountShare(Map<String, Long> pQualifiedItemAndQty,Map<String, Long> pTargetedItemAndQty,
			String pPromoId, Order pOrder, double pTotalAmount, String pSingleUsedCoupon) {
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.populateItemDiscountShare() methods.STARTS");
			vlogDebug("pQualifiedItemAndQty : {0} pTargetedItemAndQty : {1}", pQualifiedItemAndQty,pTargetedItemAndQty);
			vlogDebug("pPromoId:{0} pOrder : {1} pTotalAmount : {2}", pPromoId,pOrder,pTotalAmount);
		}
		if(pTargetedItemAndQty == null || pTargetedItemAndQty.isEmpty() || pOrder == null || StringUtils.isBlank(pPromoId)){
			return;
		}
		String skuId = null;
		Long qty = null;
		for(Map.Entry<String, Long> entry : pTargetedItemAndQty.entrySet()){
			skuId = entry.getKey();
			if(StringUtils.isBlank(skuId)){
				continue;
			}
			qty = entry.getValue();
			try {
				List<CommerceItem> cItems = pOrder.getCommerceItemsByCatalogRefId(skuId);
				if(isLoggingDebug()){
					vlogDebug("skuId:{0} qty : {1} cItems : {2}", skuId,qty,cItems);
				}
				if(cItems == null || cItems.isEmpty()){
					continue;
				}
				double itemPrice = TRUConstants.DOUBLE_ZERO;
				double discountPrice = TRUConstants.DOUBLE_ZERO;
				List<PricingAdjustment> adjustList = null;
				TRUItemPriceInfo itemPriceInfo = null;
				for(CommerceItem cItem : cItems){
					itemPriceInfo = (TRUItemPriceInfo) cItem.getPriceInfo();
					if(itemPriceInfo== null){
						continue;
					}
					adjustList = itemPriceInfo.getAdjustments();
					if(adjustList == null || adjustList.isEmpty()){
						continue;
					}
					boolean isPromotionMatch = Boolean.FALSE;
					for(PricingAdjustment adj : adjustList){
						if(adj == null || adj.getPricingModel() == null){
							continue;
						}
						if(pPromoId.equals(adj.getPricingModel().getRepositoryId())){
							discountPrice = -adj.getTotalAdjustment();
							isPromotionMatch = Boolean.TRUE;
							break;
						}
					}
					if(!isPromotionMatch){
						continue;
					}
					Map truItemDiscountShare = itemPriceInfo.getTruItemDiscountShare();
					double discountShare = TRUConstants.DOUBLE_ZERO;
					if(truItemDiscountShare != null && !truItemDiscountShare.isEmpty()){
						Iterator it = truItemDiscountShare.entrySet().iterator();
				    	while (it.hasNext()) {
				    		String value = null;
				    		Map.Entry discountMap  = (Map.Entry)it.next();
							if(discountMap.getValue() != null && discountMap.getValue() instanceof String && 
									!discountMap.getKey().equals(pPromoId+TRUConstants.PIPE_STRING+pSingleUsedCoupon+TRUConstants.PIPE_STRING+TRUConstants.PROMOTION_TARGETER_ITEM_LABEL)){
								value = ((String)discountMap.getValue());
							}
							if(StringUtils.isNotBlank(value)){
								discountShare += Double.valueOf((value.split(TRUConstants.PIPE_DELIMETER))[TRUConstants.INT_ZERO]);
							}
						}
					}
					itemPrice = itemPriceInfo.getSalePrice()*qty-discountShare;
					// Calling method to prorate the share and set it in info object.
					prorateItemDiscountShare(itemPrice,discountPrice,pTotalAmount,pOrder,pQualifiedItemAndQty,
							cItem.getPriceInfo(),pPromoId,pSingleUsedCoupon,qty);
				}
			} catch (CommerceItemNotFoundException e) {
				if (isLoggingError()) {
					vlogError("CommerceItemNotFoundException : {0}", e);
				}
			} catch (InvalidParameterException e) {
				if (isLoggingError()) {
					vlogError("InvalidParameterException : {0}", e);
				}
			}
		}
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.populateItemDiscountShare() methods.END");
		}
	}

	/**
	 * Method to prorate the discount share and update the item info.
	 * 
	 * @param pItemPrice - double - Target Item Price.
	 * @param pDiscountPrice - double - Discount Amount.
	 * @param pTotalAmount - double - Total qualified items amount.
	 * @param pOrder - Order Object.
	 * @param pQualifiedItemAndQty - Map - Qualified Items and quantity Map
	 * @param pTargetItemInfo - ItemPriceInfo Object.
	 * @param pPromoId - String - Promotion ID.
	 * @param pSingleUsedCoupon 
	 * @param pTargetItemQty 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void prorateItemDiscountShare(double pItemPrice,double pDiscountPrice,
			double pTotalAmount, Order pOrder, Map<String, Long> pQualifiedItemAndQty,
			ItemPriceInfo pTargetItemInfo, String pPromoId, String pSingleUsedCoupon, Long pTargetItemQty) {
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.prorateItemDiscountShare() methods.STARTS");
			vlogDebug("pQualifiedItemAndQty : {0} pTargetItemInfo : {1}", pQualifiedItemAndQty,pTargetItemInfo);
			vlogDebug("pItemPrice:{0} pDiscountPrice : {1} pTotalAmount : {2}", pItemPrice,pDiscountPrice,pTotalAmount);
			vlogDebug("pOrder : {0} pPromoId : {1}", pOrder,pPromoId);
		}
		if(pItemPrice+pTotalAmount == TRUConstants.DOUBLE_ZERO || pDiscountPrice == TRUConstants.DOUBLE_ZERO){
			return;
		}
		double discountSoFar = TRUConstants.DOUBLE_ZERO;
		double tarOldShare = TRUConstants.DOUBLE_ZERO;
		// Prorating the discount share on targeted item.
		double tarItemShare = round(pItemPrice*pDiscountPrice/(pItemPrice+pTotalAmount));
		discountSoFar += tarItemShare;
		Map tarItemDiscountShare = ((TRUItemPriceInfo)pTargetItemInfo).getTruItemDiscountShare();
		String value = null;
		if(tarItemDiscountShare.get(pPromoId+TRUConstants.PIPE_STRING+pSingleUsedCoupon+TRUConstants.PIPE_STRING+TRUConstants.PROMOTION_TARGETER_ITEM_LABEL) != null){
			value = (String) tarItemDiscountShare.get(pPromoId+TRUConstants.PIPE_STRING+pSingleUsedCoupon+TRUConstants.PIPE_STRING+TRUConstants.PROMOTION_TARGETER_ITEM_LABEL);
		}
		if(StringUtils.isNotBlank(value)){
			tarOldShare = Double.valueOf((value.split(TRUConstants.PIPE_DELIMETER))[0]);
		}
		tarItemDiscountShare.put(pPromoId+TRUConstants.PIPE_STRING+pSingleUsedCoupon+TRUConstants.PIPE_STRING+TRUConstants.PROMOTION_TARGETER_ITEM_LABEL, round(tarItemShare+tarOldShare)+TRUConstants.PIPE_STRING+pTargetItemQty);
		((TRUItemPriceInfo)pTargetItemInfo).setTruItemDiscountShare(tarItemDiscountShare);
		if(pQualifiedItemAndQty == null || pQualifiedItemAndQty.isEmpty() || pOrder == null){
			return;
		}
		String skuId = null;
		Long qty = TRUConstants.LONG_ZERO;
		List<CommerceItem> commerceItemsByCatalogRefId = null;
		TRUItemPriceInfo priceInfo = null;
		int i =TRUConstants.ZERO;
		boolean isLastItem = Boolean.FALSE;
		for(Map.Entry<String,Long> entry : pQualifiedItemAndQty.entrySet()){
			skuId = entry.getKey();
			if(StringUtils.isBlank(skuId)){
				continue;
			}
			qty = entry.getValue();
			i++;
			try {
				commerceItemsByCatalogRefId = pOrder.getCommerceItemsByCatalogRefId(skuId);
				if(commerceItemsByCatalogRefId == null || commerceItemsByCatalogRefId.isEmpty()){
					continue;
				}
				if(pQualifiedItemAndQty.size() == i){
					isLastItem = Boolean.TRUE;
				}
				for(CommerceItem cItem : commerceItemsByCatalogRefId){
					priceInfo = (TRUItemPriceInfo) cItem.getPriceInfo();
					Map truItemDiscountShare = priceInfo.getTruItemDiscountShare();
					double discountShare = TRUConstants.DOUBLE_ZERO;
					String strShare = null;
					if(truItemDiscountShare != null && !truItemDiscountShare.isEmpty()){
						Iterator it = truItemDiscountShare.entrySet().iterator();
				    	while (it.hasNext()) {
				    		Map.Entry discountMap  = (Map.Entry)it.next();
							if(discountMap.getValue() != null && discountMap.getValue() instanceof String && 
									!discountMap.getKey().equals(pPromoId+TRUConstants.PIPE_STRING+pSingleUsedCoupon+TRUConstants.PIPE_STRING+TRUConstants.PROMOTION_QUALIFIER_ITEM_LABEL)){
								strShare = ((String)discountMap.getValue());
							}
							if(StringUtils.isNotBlank(strShare)){
								discountShare += Double.valueOf((strShare.split(TRUConstants.PIPE_DELIMETER))[0]);
							}
						}
					}
					double qulDiscountShare = round(((priceInfo.getSalePrice()*qty-discountShare)*pDiscountPrice)/(pItemPrice+pTotalAmount));
					discountSoFar +=qulDiscountShare;
					if(isLastItem &&
							commerceItemsByCatalogRefId.indexOf(cItem)+TRUConstants.INTEGER_NUMBER_ONE == commerceItemsByCatalogRefId.size()){
						qulDiscountShare += pDiscountPrice-discountSoFar;
					}
					Map qulItemDisShare = priceInfo.getTruItemDiscountShare();
					double qulOldShare = TRUConstants.DOUBLE_ZERO;
					String oldValue = null;
					if(qulItemDisShare.get(pPromoId+TRUConstants.PIPE_STRING+pSingleUsedCoupon+TRUConstants.PIPE_STRING+TRUConstants.PROMOTION_QUALIFIER_ITEM_LABEL) != null){
						oldValue = (String) qulItemDisShare.get(pPromoId+TRUConstants.PIPE_STRING+pSingleUsedCoupon+TRUConstants.PIPE_STRING+TRUConstants.PROMOTION_QUALIFIER_ITEM_LABEL);
					}
					if(StringUtils.isNotBlank(oldValue)){
						qulOldShare = Double.valueOf((oldValue.split(TRUConstants.PIPE_DELIMETER))[0]);
					}
					qulItemDisShare.put(pPromoId+TRUConstants.PIPE_STRING+pSingleUsedCoupon+TRUConstants.PIPE_STRING+TRUConstants.PROMOTION_QUALIFIER_ITEM_LABEL, round(qulDiscountShare+qulOldShare)+TRUConstants.PIPE_STRING+qty);
					priceInfo.setTruItemDiscountShare(qulItemDisShare);
				}
			} catch (CommerceItemNotFoundException e) {
				if (isLoggingError()) {
					vlogError("CommerceItemNotFoundException : {0}", e);
				}
			} catch (InvalidParameterException e) {
				if (isLoggingError()) {
					vlogError("InvalidParameterException : {0}", e);
				}
			}
		}
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.prorateItemDiscountShare() methods.END");
		}
	}
	
	/**
	 * Method is used to get the all BPP items amount from the Order.
	 * @param pOrder - Order Object.
	 * @return double - Sum of all the BPP item Price.
	 */
	@SuppressWarnings("unchecked")
	public Double getBPPItemsAmount(Order pOrder) {
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.getBPPItemsAmount() methods.STARTS");
			vlogDebug("pOrder : {0}", pOrder);
		}
		Double bppAmount = TRUConstants.DOUBLE_ZERO;
		if(pOrder == null){
			return bppAmount;
		}
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		if(shippingGroups == null || shippingGroups.isEmpty()){
			return bppAmount;
		}
		List<TRUShippingGroupCommerceItemRelationship> commerceItemRelationships = null;
		TRUBppItemInfo bppItemInfo = null;
		for(ShippingGroup sg : shippingGroups){
			commerceItemRelationships = sg.getCommerceItemRelationships();
			if(commerceItemRelationships == null || commerceItemRelationships.isEmpty()){
				continue;
			}
			for(TRUShippingGroupCommerceItemRelationship rel : commerceItemRelationships){
				bppItemInfo = rel.getBppItemInfo();
				if(bppItemInfo != null && StringUtils.isNotBlank(bppItemInfo.getId())){
					bppAmount += bppItemInfo.getBppPrice()*rel.getQuantity();
				}
			}
		}
		if(isLoggingDebug()){
			vlogDebug("returning with bppAmount : {0}", bppAmount);
			vlogDebug("TRUPricingTools.getBPPItemsAmount() methods.END");
		}
		return round(bppAmount);
	}
	
	/**
	 * Method to get all the Promotion Status from Profile.
	 * 
	 * @param pProfileId - String - Profile Id.
	 * @return List -List of Repository Items.
	 */
	@SuppressWarnings("unchecked")
	public List<RepositoryItem> getProfileCouponStatus(String pProfileId) {
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.getProfileCouponStatus() methods.STARTS");
			vlogDebug("pProfileId : {0}", pProfileId);
		}
		List<RepositoryItem> statuses = null;
		if(StringUtils.isBlank(pProfileId)){
			return statuses;
		}
		Repository profileRepository = getOrderTools().getProfileRepository();
		getOrderTools();
		PromotionTools promotionTools = ((TRUOrderManager)getOrderManager()).getPromotionTools();
		try {
			RepositoryItem profile = profileRepository.getItem(pProfileId, getOrderTools().getDefaultProfileType());
			if(profile != null && profile.getPropertyValue(promotionTools.getActivePromotionsProperty()) != null){
				statuses = new ArrayList<RepositoryItem>((List<RepositoryItem>)profile.getPropertyValue(
						promotionTools.getActivePromotionsProperty()));
			}
		} catch (RepositoryException e) {
			vlogError("RepositoryException : {0}", e);
		}
		if(isLoggingDebug()){
			vlogDebug("returning with statuses : {0}", statuses);
			vlogDebug("TRUPricingTools.getProfileCouponStatus() methods.END");
		}
		return statuses;
	}
	
	/**
	 * @param pProfileCouponStatus - List of Promotion Status Repository Items.
	 * @param pPromotion - Repository Item
	 * @param pForProration - Map
	 * @return String - Coupon Id.
	 */
	@SuppressWarnings("unchecked")
	private String getCoupon(List<RepositoryItem> pProfileCouponStatus,
			RepositoryItem pPromotion, Map<String, String> pForProration) {
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.getCoupon() methods.STARTS");
			vlogDebug("pProfileCouponStatus : {0} pPromotion : {1}", pProfileCouponStatus,pPromotion);
			vlogDebug("pForProration : {0}", pForProration);
		}
		String singleUsedCoupon = null;
		if(pProfileCouponStatus == null || pProfileCouponStatus.isEmpty() || pPromotion == null){
			return singleUsedCoupon;
		}
		PromotionTools promotionTools = ((TRUOrderManager)getOrderManager()).getPromotionTools();
		List<RepositoryItem> statusCoupons = null;
		for (RepositoryItem status : pProfileCouponStatus) {
			RepositoryItem promoStatusPromotion = null;
			if(status.getPropertyValue(promotionTools.getPromoStatusPromoProperty()) != null){
				promoStatusPromotion = (RepositoryItem)status.getPropertyValue(promotionTools.getPromoStatusPromoProperty());
			}
			if(promoStatusPromotion == null){
				continue;
			}
			if(pPromotion.getRepositoryId().equals(promoStatusPromotion.getRepositoryId()) && 
					status.getPropertyValue(promotionTools.getPromoStatusCouponsPropertyName()) != null){
				statusCoupons = (List<RepositoryItem>)status.getPropertyValue(promotionTools.getPromoStatusCouponsPropertyName());
			}
			boolean isCouponMatched = Boolean.FALSE;
			if(statusCoupons != null && !statusCoupons.isEmpty() && pForProration != null && !pForProration.isEmpty()){
				for(RepositoryItem couponItem : statusCoupons){
					for(Iterator<Map.Entry<String, String>> it = pForProration.entrySet().iterator(); it.hasNext(); ) {
						Map.Entry<String, String> entry = it.next();
						if(StringUtils.isNotBlank(entry.getValue()) && entry.getValue().equals(couponItem.getRepositoryId())){
							singleUsedCoupon = entry.getKey();
						    isCouponMatched = Boolean.TRUE;
						    break;
						}
					}
					if(isCouponMatched){
						break;
					}
				}
				if(isCouponMatched){
					break;
				}
			}
		}
		if(isLoggingDebug()){
			vlogDebug("returning with singleUsedCoupon : {0}", singleUsedCoupon);
			vlogDebug("TRUPricingTools.getCoupon() methods.END");
		}
		return singleUsedCoupon;
	}

	/**
	 * Method to invoke the method to prorate the Item and Order Discount Shares.
	 * 
	 * @param pOrder - Order Object
	 * @param pExtraParameters - Map - Extra parameter.
	 */
	@SuppressWarnings("rawtypes")
	public void updateCouponAndShareDetails(Order pOrder, Map pExtraParameters) {
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.updateCouponAndShareDetails() methods.STARTS");
			vlogDebug("pOrder : {0} pExtraParameters : {1}", pOrder,pExtraParameters);
		}
		if(isEnableGWPProration()){
			buildGWPDetails(pOrder,pExtraParameters);
		}
		buildTieredPromotionDetails(pOrder,pExtraParameters);
		// Calling method to get all the Promotion Status from Profile.
		List<RepositoryItem> profileCouponStatus = getProfileCouponStatus(pOrder.getProfileId());
		// Calling method to prorate the Item Discount Share.
		calculateItemDiscountShare(pOrder, pExtraParameters,profileCouponStatus);
		// Calling method to prorate the Order Discount Share.
		calculateOrderDiscountShare(pOrder,profileCouponStatus);
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.updateCouponAndShareDetails() methods.END");
		}
	}
	
	/**
	 * Builds the tiered promotion details.
	 *
	 * @param pOrder the order
	 * @param pExtraParameters the extra parameters
	 */
	@SuppressWarnings({ "rawtypes", "unchecked"})
	public void buildTieredPromotionDetails(Order pOrder, Map pExtraParameters) {
		if(isLoggingDebug()){
			vlogDebug("TRUPricingTools.buildTieredPromotionDetails() methods.STARTS");
			vlogDebug("pOrder : {0} pExtraParameters : {1}", pOrder,pExtraParameters);
		}
		Map<String, Map<String, Long>> targetItem = null;
		targetItem = (Map<String, Map<String, Long>>) pExtraParameters.get(TRUConstants.TARGET_ITEM);
		if (targetItem == null) {
			targetItem = new HashMap<String, Map<String, Long>>();
		}
		List<CommerceItem> items = pOrder.getCommerceItems();
		String tieredBasePromo=((TRUOrderTools)getOrderTools()).getTieredPriceBreakTemplate();
		String advancedPromo=getTRUCommercePropertyManager().getAdvancedItemTemplatePropertyName();
		TRUItemPriceInfo itemPriceInfo = null;
		
			for (CommerceItem cItem : items) {
				if(cItem instanceof TRUDonationCommerceItem ||cItem instanceof TRUGiftWrapCommerceItem){
					continue;
				}
				itemPriceInfo = (TRUItemPriceInfo) cItem.getPriceInfo();
				if(itemPriceInfo==null){
					continue;
				}
				List<PricingAdjustment> adjustments = itemPriceInfo.getAdjustments();
				if(adjustments == null || adjustments.isEmpty()){
					continue;
				}
				RepositoryItem promotion=null;
				for(PricingAdjustment priceAdj : adjustments){
					promotion = priceAdj.getPricingModel();
					if(promotion == null){
						continue;
					}
					Map<String, Long> targetItemQtyMap = new HashMap<String, Long>();
					String templateValue =(String) promotion.getPropertyValue(getPricingModelProperties().getTemplate());
					
					if(StringUtils.isNotBlank(templateValue)&&(tieredBasePromo.equals(templateValue)||(advancedPromo.equals(templateValue) && isTieredPromotion(promotion)) )){
						long qty =priceAdj.getQuantityAdjusted();
						targetItemQtyMap.put(cItem.getCatalogRefId(), qty);
						targetItem.put(promotion.getRepositoryId(), targetItemQtyMap);
					}
				}
			}
			pExtraParameters.put(TRUConstants.TARGET_ITEM, targetItem);
			if(isLoggingDebug()){
				vlogDebug("TRUPricingTools.buildTieredPromotionDetails() methods.END");
			}
	}
	
	
	/**
	 * Checks if is tiered promotion.
	 *
	 * @param pItem the item
	 * @return true, if is tiered promotion
	 */
	public boolean isTieredPromotion(RepositoryItem pItem){
		TRUPromotionTools promotionTools = (TRUPromotionTools) getOrderTools().getProfileTools().getPromotionTools();
		String startDiscountStructure = promotionTools.getTemplateValuesForPromotion(pItem.getRepositoryId(),TRUConstants.DISCOUNT_STRUCTURE);
		String endDiscountStructure = promotionTools.getTemplateValuesForPromotion(pItem.getRepositoryId(),TRUConstants.END_DISCOUNT_STRUCTURE);
		String discountStructure=null;
		if(StringUtils.isNotBlank(startDiscountStructure)&& StringUtils.isNotBlank(endDiscountStructure)){
			discountStructure=startDiscountStructure.concat(endDiscountStructure);
			if(StringUtils.isNotBlank(discountStructure)){
				String calculatorType=getCalculatorType(discountStructure);
				if(StringUtils.isNotBlank(calculatorType)&& TRUConstants.BULK.equals(calculatorType)){
					return true;
				}
			}
		}
		
		return false;
	}
	/**
	 * Builds the gwp details.
	 *
	 * @param pOrder the order
	 * @param pExtraParameters the extra parameters
	 */
	@SuppressWarnings({ "rawtypes", "unchecked"})
	public void buildGWPDetails(Order pOrder, Map pExtraParameters) {
		Map<String, Map<String, Long>> targetItem = null;
		Map<String, Map<String, Long>> qualifyItem = null;
		targetItem = (Map<String, Map<String, Long>>) pExtraParameters.get(TRUConstants.TARGET_ITEM);
		if (targetItem == null) {
			targetItem = new HashMap<String, Map<String, Long>>();
		}
		qualifyItem = (Map<String, Map<String, Long>>) pExtraParameters.get(TRUConstants.QUALIFIED_ITEM);
		if (qualifyItem == null) {
			qualifyItem = new HashMap<String, Map<String, Long>>();
		}
		List<CommerceItem> items = pOrder.getCommerceItems();
		Map<String, Long> gwpSkuItemsMap = new HashMap<String, Long>();
		Map<String, String> gwpSkuPromoMap = new HashMap<String, String>();
		TRUPromotionTools promotionTools = (TRUPromotionTools) getOrderTools().getProfileTools().getPromotionTools();
		try {
			for (CommerceItem item : items) {
				if(item instanceof TRUDonationCommerceItem ||item instanceof TRUGiftWrapCommerceItem){
					continue;
				}
				RepositoryItem[] itemMarkers = null;
				GWPMarkerManager manager = getGwpManager().getGwpMarkerManager();
				itemMarkers = manager.getItemMarkers(item, null, TRUCommerceConstants.STRING_NOT_FOUND);
				if (null != itemMarkers) {
					String promotionId = manager.getPromotionId(itemMarkers[TRUConstants.ZERO]);
					String property = promotionTools.getTemplateValuesForPromotion(promotionId, getTRUCommercePropertyManager().getNoOfItemsColumnName());
					long noOfItems = TRUConstants.LONG_ZERO;
					if(StringUtils.isNotBlank(property)){
						try {
							noOfItems = Long.parseLong(property);
						} catch (NumberFormatException e) {
							noOfItems = TRUConstants.LONG_ZERO;
						}
					}
					else{
						noOfItems = TRUConstants.LONG_ZERO;
					}
					if (noOfItems == TRUConstants.ZERO) {
						RepositoryItem promotion = promotionTools.lookupPromotion(promotionId);
						if (promotion != null
								&& null != promotion.getPropertyValue(getPricingModelProperties().getTemplate())) {
							String propertyValue = (String) promotion.getPropertyValue(getPricingModelProperties()
									.getTemplate());
							if (getTRUCommercePropertyManager().getAdvancedItemTemplatePropertyName().equals(propertyValue)) {
								String qualifier = promotionTools
										.getTemplateValuesForPromotion(promotionId, getTRUCommercePropertyManager().getQualifierColumnName());
								String evaluationLimitValue = promotionTools
										.getTemplateValuesForPromotion(promotionId, getTRUCommercePropertyManager().getEvaluationLimitValueColumnName());
								long gwpTimes = TRUConstants.LONG_ONE;
								if(StringUtils.isNotBlank(evaluationLimitValue)){
									
									try {
										gwpTimes = Long.parseLong(evaluationLimitValue);
									} catch (NumberFormatException e) {
										gwpTimes = TRUConstants.LONG_ONE;
									}
								}
								if (qualifier != null) {
									if (!gwpSkuItemsMap.containsKey(item.getCatalogRefId())) {
										long gwpNoOfItems = getGwpNoOfItems(qualifier);
										if(gwpTimes>TRUConstants.LONG_ONE){
											gwpNoOfItems=gwpNoOfItems*item.getQuantity();
										}
										gwpSkuItemsMap.put(item.getCatalogRefId(), gwpNoOfItems);
										gwpSkuPromoMap.put(item.getCatalogRefId(), promotionId);
									}
								}
							} else {
								gwpSkuItemsMap.put(item.getCatalogRefId(), noOfItems);
								gwpSkuPromoMap.put(item.getCatalogRefId(), promotionId);
							}

						}
					} else {
						gwpSkuItemsMap.put(item.getCatalogRefId(), noOfItems);
						gwpSkuPromoMap.put(item.getCatalogRefId(), promotionId);
					}

				}
			}
			if(isLoggingDebug()){
				vlogDebug("gwpSkuItemsMap : {0} gwpSkuPromoMap : {1}", gwpSkuItemsMap,gwpSkuPromoMap);
			}
			if (!gwpSkuItemsMap.isEmpty()) {
				for (Map.Entry<String, String> entry : gwpSkuPromoMap.entrySet()) {
					long promoQty = gwpSkuItemsMap.get(entry.getKey());
					String promoid = entry.getValue();
					Map<String, Long> qualifyItemQtyMap = new HashMap<String, Long>();
					for (CommerceItem item : items) {
						if(item instanceof TRUDonationCommerceItem ||item instanceof TRUGiftWrapCommerceItem){
							continue;
						}
						String skuid = item.getCatalogRefId();
						long qty = item.getQuantity();

						if (!gwpSkuItemsMap.containsKey(skuid)) {
							if (promoQty == TRUConstants.ZERO) {
								qualifyItemQtyMap.put(skuid, qty);
								qualifyItem.put(entry.getValue(), qualifyItemQtyMap);
								continue;
							} else {
								TRUPromoContainer container = promotionTools.getPromotionDetailsForSKU(skuid, null,true);
								if (null != container && null != container.getPromotions()
										&& !container.getPromotions().isEmpty()) {
									for (TRUPromoVO vo : container.getPromotions()) {
										if (vo.getPromoId().equals(promoid)) {
											List<PricingAdjustment> adjustments = item.getPriceInfo().getAdjustments();
											for (PricingAdjustment adjustment : adjustments) {
												RepositoryItem promotion = adjustment.getPricingModel();
												if (null != promotion) {
													qty -= adjustment.getQuantityAdjusted();
												}
											}
											if (qty >= promoQty) {
												qualifyItemQtyMap.put(skuid, promoQty);
												qualifyItem.put(entry.getValue(), qualifyItemQtyMap);
												break;
											} else {
												qualifyItemQtyMap.put(skuid, qty);
												promoQty -= qty;
											}
										}

									}
								}
							}
						}
					}
					pExtraParameters.put(TRUConstants.QUALIFIED_ITEM, qualifyItem);
				}
			}
			if (!gwpSkuItemsMap.isEmpty()) {
				for (Map.Entry<String, String> entry : gwpSkuPromoMap.entrySet()) {
					Map<String, Long> targetItemQtyMap = new HashMap<String, Long>();
					for (CommerceItem item : items) {
						if(item instanceof TRUDonationCommerceItem ||item instanceof TRUGiftWrapCommerceItem){
							continue;
						}
						String skuid = item.getCatalogRefId();
						if (gwpSkuItemsMap.containsKey(skuid)) {
						long qty=item.getQuantity();
						item.getPriceInfo().getQuantityDiscounted();
						if(item.getQuantity()>item.getPriceInfo().getQuantityDiscounted()){
							qty=qty-item.getPriceInfo().getQuantityDiscounted();
						}
						targetItemQtyMap.put(skuid, qty);
						targetItem.put(entry.getValue(), targetItemQtyMap);}
					}
				}
				pExtraParameters.put(TRUConstants.TARGET_ITEM, targetItem);
			}
			if(isLoggingDebug()){
				vlogDebug("targetItem : {0} qualifyItem : {1}", targetItem,qualifyItem);
			}
		} catch (MarkerException e) {
			vlogError("MarkerException : {0}", e);
		} catch (RepositoryException e) {
			vlogError("RepositoryException : {0}", e);
		}
	}

	/**
	 * Gets the gwp no of items.
	 *
	 * @param pQualifier the qualifier
	 * @return the gwp no of items
	 */
	private long getGwpNoOfItems(String pQualifier) {
		if (isLoggingDebug()) {
			logDebug("Entering getGwpNoOfItems method of TRUPricingTools....");
		}
		DocumentBuilder docBuilder = null;
		NodeList nodeListStringValue = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		long numberOfItems = TRUConstants.LONG_ZERO;
		try {
			docBuilder = dbf.newDocumentBuilder();
			InputSource inputSource = new InputSource();
			inputSource.setCharacterStream(new StringReader(pQualifier));
			Document doc = docBuilder.parse(inputSource);
			nodeListStringValue = doc.getDocumentElement().getElementsByTagName(TRUConstants.NEXT);
			if (nodeListStringValue != null) {
				for (int i = 0; i < nodeListStringValue.getLength(); i++) {
					Element line = (Element) nodeListStringValue.item(i);
					try {
						numberOfItems += Long.parseLong(line.getAttribute(TRUConstants.NUMBER));
					} catch (NumberFormatException e) {
						numberOfItems = TRUConstants.LONG_ZERO;
					}

				}
			}

		} catch (SAXException e) {
			if (isLoggingError()) {
				vlogError("TRUPricingTools.getGwpNoOfItems,IOException occured:{0}", e);
			}
		} catch (IOException e) {
			if (isLoggingError()) {
				vlogError("TRUPricingTools.getGwpNoOfItems,IOException occured:{0}", e);
			}
		} catch (ParserConfigurationException e) {
			if (isLoggingError()) {
				vlogError("TRUPricingTools.getGwpNoOfItems,IOException occured:{0}", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting getGwpNoOfItems method of TRUPricingTools....");
		}
		return numberOfItems;
	}
	
	
	
	/**
	 * Gets the calculator type.
	 *
	 * @param pDiscountStructure the discount structure
	 * @return the calculator type
	 */
	private String getCalculatorType(String pDiscountStructure) {
		if (isLoggingDebug()) {
			logDebug("Entering getCalculatorType method of TRUPricingTools....");
		}
		String type=null;
		DocumentBuilder docBuilder = null;
		String nodeListStringValue = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			docBuilder = dbf.newDocumentBuilder();
			InputSource inputSource = new InputSource();
			inputSource.setCharacterStream(new StringReader(pDiscountStructure));
			Document doc = docBuilder.parse(inputSource);
			nodeListStringValue = doc.getDocumentElement().getAttribute("calculator-type");
			if (nodeListStringValue != null) {
				return nodeListStringValue;
			}

		} catch (SAXException e) {
			if (isLoggingError()) {
				vlogError("TRUPricingTools.getCalculatorType,IOException occured:{0}", e);
			}
		} catch (IOException e) {
			if (isLoggingError()) {
				vlogError("TRUPricingTools.getCalculatorType,IOException occured:{0}", e);
			}
		} catch (ParserConfigurationException e) {
			if (isLoggingError()) {
				vlogError("TRUPricingTools.getCalculatorType,IOException occured:{0}", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting getCalculatorType method of TRUPricingTools....");
		}
		return type;
	}
	/**
	 * Holds reference for mEnableGWPProration.
	 */
	private boolean mEnableGWPProration;
	/**
	 * This method will return mEnableGWPProration.
	 *
	 * @return the mEnableGWPProration.
	 */
	public boolean isEnableGWPProration() {
		return mEnableGWPProration;
	}
	/**
	 * This method will set mEnableGWPProration with pEnableGWPProration.
	 * @param pEnableGWPProration the mEnableGWPProration to set.
	 */
	public void setEnableGWPProration(boolean pEnableGWPProration) {
		mEnableGWPProration = pEnableGWPProration;
	}
	
	/**
	 * Method is to check whether promotion is applied on Gift Wrap item or not.
	 * 
	 * @param pTargetedItemAndQty - Map of Target Items sku ids and quantity.
	 * @param pOrder - Order Object
	 * @return boolean - True if promotion applied on Gift Wrap Items.
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	private boolean chekIfGiftWrapPromotion(Map<String, Long> pTargetedItemAndQty, Order pOrder) {
		if(isLoggingDebug()){
			logDebug("Enter into [Class: TRUPricingTools  method: chekIfGiftWrapPromotion]");
			vlogDebug("pTargetedItemAndQty:{0} pOrder : {1}", pTargetedItemAndQty,pOrder);
		}
		boolean isGiftWrapPromo = Boolean.FALSE;
		if(pTargetedItemAndQty == null || pTargetedItemAndQty.isEmpty() || pOrder == null){
			return isGiftWrapPromo;
		}
		String skuId = null;
		long qty = TRUConstants.LONG_ZERO;
		for(Map.Entry<String, Long> qulEntry : pTargetedItemAndQty.entrySet()){
			skuId = qulEntry.getKey();
			qty = qulEntry.getValue();
			try {
				List<CommerceItem> commerceItemsByCatalogRefId = pOrder.getCommerceItemsByCatalogRefId(skuId);
				if(isLoggingDebug()){
					vlogDebug("skuId:{0} commerceItemsByCatalogRefId : {1}", skuId,commerceItemsByCatalogRefId);
				}
				if(commerceItemsByCatalogRefId == null || commerceItemsByCatalogRefId.isEmpty()){
					continue;
				}
				TRUItemPriceInfo itemPriceInfo = null;
				for(CommerceItem cItem : commerceItemsByCatalogRefId){
					if(cItem instanceof TRUGiftWrapCommerceItem){
						if(isLoggingDebug()){
							vlogDebug("isGiftWrapPromo:{0}", isGiftWrapPromo);
						}
						isGiftWrapPromo = Boolean.TRUE;
						break;
					}
				}
			}catch (CommerceItemNotFoundException e) {
				if (isLoggingError()) {
					vlogError("CommerceItemNotFoundException : {0}", e);
				}
			} catch (InvalidParameterException e) {
				if (isLoggingError()) {
					vlogError("InvalidParameterException : {0}", e);
				}
			}
			if(isGiftWrapPromo){
				break;
			}
		}
		if(isLoggingDebug()){
			vlogDebug("isGiftWrapPromo:{0}", isGiftWrapPromo);
			vlogDebug("TRUPricingTools.chekIfGiftWrapPromotion() methods.END");
		}
		return isGiftWrapPromo;
	}
}
