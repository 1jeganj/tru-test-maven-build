package com.tru.commerce.pricing;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import atg.adapter.gsa.ChangeAwareSet;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.ShippingGroupNotFoundException;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.ShippingPriceInfo;
import atg.core.util.Address;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.payment.tax.TaxRequestInfo;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.order.TRUBppItemInfo;
import com.tru.commerce.order.TRUCommerceItemImpl;
import com.tru.commerce.order.TRUCommerceItemMetaInfo;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.radial.taxquote.DestinationTargetType;
import com.tru.radial.taxquote.Discount2;
import com.tru.radial.taxquote.FeeListRequest2;
import com.tru.radial.taxquote.FeeRequest2;
import com.tru.radial.taxquote.GiftPackaging2;
import com.tru.radial.taxquote.ISOCurrencyCodeType;
import com.tru.radial.taxquote.MailingAddressType2;
import com.tru.radial.taxquote.MerchandisePriceGroup2;
import com.tru.radial.taxquote.ObjectFactory;
import com.tru.radial.taxquote.OrderItem2;
import com.tru.radial.taxquote.OrderItemList2;
import com.tru.radial.taxquote.OrderItemPricing2;
import com.tru.radial.taxquote.Origins;
import com.tru.radial.taxquote.PersonNameType;
import com.tru.radial.taxquote.PriceGroup2;
import com.tru.radial.taxquote.PromotionalDiscounts2;
import com.tru.radial.taxquote.ShipGroupType2;
import com.tru.radial.taxquote.TaxDutyPhysicalAddress;
import com.tru.radial.taxquote.TaxDutyQuoteRequest;
import com.tru.radial.taxquote.TdfDestinationsType;
import com.tru.radial.taxquote.TdfShippingRequestType;
import com.tru.radial.taxquote.TdfShippingRequestType.ShipGroups;

/**
 * This class implements the multiple items requests sent to Taxware, where items are represented
 * by the shipping destination group objects passed in TaxRequestInfo.
 * 
 * @author Professional Access
 * 
 */
public class TRURadialTaxwareUtils extends GenericService {
	
	public static final String PROMOTION_QUALIFIER_ITEM_LABEL = "qual";

	public static final String PROMOTION_TARGETER_ITEM_LABEL = "tar";

	/** Holds the pipe delimetre. */
	public static final String PIPE_DELIMETRE = "\\|";

	/** Property To property manager. */
	private TRUCommercePropertyManager mPropertyManager;

	/** The Pricing tools. */
	private TRUPricingTools mPricingTools;

	/** Property To Hold location repository. */
	private Repository mLocationRepository;
	
	/** Property To Hold product repository. */
	private Repository mProductRepository;

	/**
	 * @return the productRepository
	 */
	public Repository getProductRepository() {
		return mProductRepository;
	}

	/**
	 * @param pProductRepository the productRepository to set
	 */
	public void setProductRepository(Repository pProductRepository) {
		mProductRepository = pProductRepository;
	}

	/**
	 * @return the locationRepository
	 */
	public Repository getLocationRepository() {
		return mLocationRepository;
	}

	/**
	 * @param pLocationRepository the locationRepository to set
	 */
	public void setLocationRepository(Repository pLocationRepository) {
		mLocationRepository = pLocationRepository;
	}

	/**
	 * @return the propertyManager
	 */
	public TRUCommercePropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUCommercePropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * @return the pricingTools
	 */
	public TRUPricingTools getPricingTools() {
		return mPricingTools;
	}

	/**
	 * @param pPricingTools the pricingTools to set
	 */
	public void setPricingTools(TRUPricingTools pPricingTools) {
		mPricingTools = pPricingTools;
	}

	/** The Configuration. */
	private TRUConfiguration mConfiguration;
	
	/**
	 * @return the configuration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * @param pConfiguration the configuration to set
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		mConfiguration = pConfiguration;
	}

	/**
	 * Construct tax quote request.
	 *
	 * @param pCcinfo the ccinfo
	 * @return the string
	 * @throws DatatypeConfigurationException the datatype configuration exception
	 */
	public  String constructTaxQuoteRequest(TaxRequestInfo pCcinfo) throws DatatypeConfigurationException{
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructTaxQuoteRequest()");
		}
		String taxQuoteRequestXML = null;
		JAXBElement<TaxDutyQuoteRequest> taxDutyQuoteRequest = populateTaxDutyQuoteRequest(pCcinfo);
		//Marshaling the Request
		if(taxDutyQuoteRequest == null) {
			return null;
		}
		taxQuoteRequestXML = marshalXML(taxDutyQuoteRequest, TRUTaxwareConstant.RADIAL_TAXWARE_STUB_PACKAGE);
		return taxQuoteRequestXML;
	}

	/**
	 * Populate tax duty quote request.
	 *
	 * @param pCcinfo the ccinfo
	 * @return the JAXB element
	 * @throws DatatypeConfigurationException the datatype configuration exception
	 */
	public  JAXBElement<TaxDutyQuoteRequest> populateTaxDutyQuoteRequest(TaxRequestInfo pCcinfo) throws DatatypeConfigurationException {
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.populateTaxDutyQuoteRequest()");
		}
		ObjectFactory objectFactory = new ObjectFactory();
		TaxDutyQuoteRequest taxDutyQuoteRequest = objectFactory.createTaxDutyQuoteRequest();
		constructTaxDutyQuoteRequest(pCcinfo, objectFactory, taxDutyQuoteRequest);
		if(taxDutyQuoteRequest.getShipping() == null) {
			return null;
		}
		JAXBElement<TaxDutyQuoteRequest> taxDutyQuoteRequestJaxB = objectFactory.createTaxDutyQuoteRequest(taxDutyQuoteRequest);
		return taxDutyQuoteRequestJaxB;
	}

	/**
	 * Construct tax duty quote request.
	 *
	 * @param pCcinfo the ccinfo
	 * @param pObjectFactory the object factory
	 * @param pTaxDutyQuoteRequest the tax duty quote request
	 */
	private  void constructTaxDutyQuoteRequest(TaxRequestInfo pCcinfo, ObjectFactory pObjectFactory, TaxDutyQuoteRequest pTaxDutyQuoteRequest) {
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructTaxDutyQuoteRequest()");
		}
		TRUOrderImpl order = (TRUOrderImpl)pCcinfo.getOrder();
		pTaxDutyQuoteRequest.setCurrency(ISOCurrencyCodeType.USD);
		pTaxDutyQuoteRequest.setVATInclusivePricing(Boolean.FALSE);
		TdfShippingRequestType createTdfShippingRequestType = pObjectFactory.createTdfShippingRequestType();
		TdfDestinationsType createTdfDestinationsType = constructRequestDestinations(pObjectFactory, order);
		createTdfShippingRequestType.setDestinations(createTdfDestinationsType);

		DestinationTargetType billingInformation = pObjectFactory.createDestinationTargetType();
		MailingAddressType2 requestMailingAddressById = getRequestMailingAddressById(createTdfDestinationsType, TRUTaxwareConstant.BILLING_MAILINGADDRESS_ID);
		if(requestMailingAddressById != null) {
			billingInformation.setRef(requestMailingAddressById);
		}
		pTaxDutyQuoteRequest.setBillingInformation(billingInformation);

		ShipGroups shipGroups =  constructRequestShipGroups(pObjectFactory, createTdfDestinationsType, order);
		if(!shipGroups.getShipGroup().isEmpty()) {
			createTdfShippingRequestType.setShipGroups(shipGroups);
			pTaxDutyQuoteRequest.setShipping(createTdfShippingRequestType);
		}
	}

	/**
	 * Construct request ship groups.
	 *
	 * @param pObjectFactory the object factory
	 * @param pCreateTdfDestinationsType the create tdf destinations type
	 * @param pOrderImpl the order impl
	 * @return the ship groups
	 */
	@SuppressWarnings({ TRUTaxwareConstant.SUPPRESS_WARNING_UNCHECKED, TRUTaxwareConstant.SUPPRESS_WARNING_RAWTYPES })
	private  ShipGroups constructRequestShipGroups(ObjectFactory pObjectFactory, TdfDestinationsType pCreateTdfDestinationsType, TRUOrderImpl pOrderImpl){
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructRequestShipGroups()");
		}
		ShipGroups shipGroups =  pObjectFactory.createTdfShippingRequestTypeShipGroups();
		List<ShipGroupType2> shipGroup = shipGroups.getShipGroup();
		Map<String, Double> shippingChargeMap = prepareShippingChargeMap(pOrderImpl);
		Map<String, Double> shippingDiscountShareMap = prepareShippingDiscountShareMap(pOrderImpl);
		Map<String, Double> orderDiscountShareMap = prepareOrderDiscountShareMap(pOrderImpl);
		List<ShippingGroup> shippingGroups = pOrderImpl.getShippingGroups();
		ShippingGroup group = null;
		Iterator groupIterator = shippingGroups.iterator();
		while (groupIterator.hasNext()) {
			group = (ShippingGroup) groupIterator.next();
			if (!(group.getCommerceItemRelationshipCount() > TRUTaxwareConstant.NUMERIC_ZERO)) {
				continue;
			}
			if(group instanceof TRUHardgoodShippingGroup) {
				Address shippingAddress = ((TRUHardgoodShippingGroup)group).getShippingAddress();
				boolean taxForDonationItem = false;
				List commerceItemRelationships = ((TRUHardgoodShippingGroup)group).getCommerceItemRelationships();
				if(commerceItemRelationships != null && !commerceItemRelationships.isEmpty() && commerceItemRelationships.size() == TRUTaxwareConstant.NUMERIC_ONE) {
					for (Object object : commerceItemRelationships) {
						ShippingGroupCommerceItemRelationship sgCIR = (ShippingGroupCommerceItemRelationship) object;
						if(sgCIR.getCommerceItem() instanceof  TRUDonationCommerceItem && pOrderImpl.getBillingAddress() != null) {
							taxForDonationItem = true;
							break;
						}
					}
				}
				if(StringUtils.isNotBlank(shippingAddress.getFirstName()) || taxForDonationItem) {
					TRUHardgoodShippingGroup truHGSgroup = (TRUHardgoodShippingGroup) group;
					if(!truHGSgroup.isMarkerShippingGroup()) {
						continue;
					}
					ShipGroupType2 createShipGroupType2 = pObjectFactory.createShipGroupType2();
					createShipGroupType2.setId(TRUTaxwareConstant.SHIPGROUPTYPE2_ID+group.getId());
					DestinationTargetType shippingInformation = pObjectFactory.createDestinationTargetType();
					if(getRequestMailingAddressById(pCreateTdfDestinationsType, group.getId()) == null && taxForDonationItem) {
						shippingInformation.setRef(getRequestMailingAddressById(pCreateTdfDestinationsType, TRUTaxwareConstant.BILLING_MAILINGADDRESS_ID));
					} else {
						shippingInformation.setRef(getRequestMailingAddressById(pCreateTdfDestinationsType, group.getId()));
					}
					createShipGroupType2.setDestinationTarget(shippingInformation);
					createShipGroupType2 = constructRequestS2HOrderItemList2(pObjectFactory, pOrderImpl, truHGSgroup, pCreateTdfDestinationsType, 
							shippingChargeMap, shippingDiscountShareMap, orderDiscountShareMap, createShipGroupType2);
					if(createShipGroupType2.getItems() != null && !(createShipGroupType2.getItems().getOrderItem().isEmpty())){
					shipGroup.add(createShipGroupType2);
					}
				}
			}
			if(group instanceof TRUInStorePickupShippingGroup) {
				String locationId = ((TRUInStorePickupShippingGroup)group).getLocationId();
				if(StringUtils.isNotBlank(locationId)) {
					ShipGroupType2 createShipGroupType2 = pObjectFactory.createShipGroupType2();
					createShipGroupType2.setId(TRUTaxwareConstant.SHIPGROUPTYPE2_ID+group.getId());

					DestinationTargetType shippingInformation = pObjectFactory.createDestinationTargetType();
					shippingInformation.setRef(getRequestMailingAddressById(pCreateTdfDestinationsType, group.getId()));
					createShipGroupType2.setDestinationTarget(shippingInformation);
					OrderItemList2 orderItemList2 = constructRequestISPUOrderItemList2(pObjectFactory, group, pCreateTdfDestinationsType, orderDiscountShareMap);
					createShipGroupType2.setItems(orderItemList2);
					shipGroup.add(createShipGroupType2);
				}
			}
		}
		return shipGroups;
	}

	/**
	 * Construct request ispu order item list2.
	 *
	 * @param pObjectFactory the object factory
	 * @param pGroup the group
	 * @param pCreateTdfDestinationsType the create tdf destinations type
	 * @param pOrderDiscountShareMap 
	 * @return the order item list2
	 */
	@SuppressWarnings(TRUTaxwareConstant.SUPPRESS_WARNING_UNCHECKED)
	private  OrderItemList2 constructRequestISPUOrderItemList2(ObjectFactory pObjectFactory, ShippingGroup pGroup, TdfDestinationsType pCreateTdfDestinationsType, Map<String, Double> pOrderDiscountShareMap){
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructRequestISPUOrderItemList2()");
		}
		TRUInStorePickupShippingGroup inStorePickupShippingGroup = (TRUInStorePickupShippingGroup) pGroup;
		List<ShippingGroupCommerceItemRelationship> commerceItemRelationships = inStorePickupShippingGroup.getCommerceItemRelationships();
		if (commerceItemRelationships == null || commerceItemRelationships.isEmpty())  {
			return null;
		}
		OrderItemList2 orderItemList2 = pObjectFactory.createOrderItemList2();
		int lineNumber = 0;
		for (ShippingGroupCommerceItemRelationship commerceItemRelationship : commerceItemRelationships) {
			final TRUShippingGroupCommerceItemRelationship shippingGroupCommerceItemRelationship = (TRUShippingGroupCommerceItemRelationship) commerceItemRelationship;
			// To avoid the gift wrap items display part
			if (shippingGroupCommerceItemRelationship.getMetaInfo() != null
					&& !shippingGroupCommerceItemRelationship.getMetaInfo().isEmpty()) {
				final ListIterator<TRUCommerceItemMetaInfo> listIterator = shippingGroupCommerceItemRelationship.
						getMetaInfo().listIterator(shippingGroupCommerceItemRelationship.getMetaInfo().size());
				while (listIterator.hasPrevious()){ 
					lineNumber = lineNumber+TRUConstants.INTEGER_NUMBER_ONE;
					TRUCommerceItemMetaInfo metaInfo = listIterator.previous();
					OrderItem2 orderItem2 = constructRequestISPUOrderItem2(pObjectFactory, shippingGroupCommerceItemRelationship, metaInfo, pCreateTdfDestinationsType, lineNumber, pOrderDiscountShareMap); 
					orderItemList2.getOrderItem().add(orderItem2);
					
					TRUBppItemInfo bppItemInfo = shippingGroupCommerceItemRelationship.getBppItemInfo();
					if (null != bppItemInfo && StringUtils.isNotBlank(bppItemInfo.getId())&& StringUtils.isNotBlank(bppItemInfo.getBppItemId())) {
						lineNumber = lineNumber+TRUConstants.INTEGER_NUMBER_ONE;
						OrderItem2 constructRequestBPPOrderItem2 = constructRequestBPPOrderItem2(pObjectFactory, 
								shippingGroupCommerceItemRelationship, metaInfo, bppItemInfo, pCreateTdfDestinationsType, lineNumber);
						orderItemList2.getOrderItem().add(constructRequestBPPOrderItem2);
					}
				}
			}
		}
		return orderItemList2;
	}

	/**
	 * Construct request s2 h order item list2.
	 *
	 * @param pObjectFactory the object factory
	 * @param pOrderImpl the order impl
	 * @param pGroup the group
	 * @param pCreateTdfDestinationsType the create tdf destinations type
	 * @param pShippingChargeMap the shipping charge map
	 * @param pShippingDiscountShareMap the shipping discount share map
	 * @param pOrderDiscountShareMap the order discount share map
	 * @param pCreateShipGroupType2 the create ship group type2
	 * @return the order item list2
	 */
	@SuppressWarnings(TRUTaxwareConstant.SUPPRESS_WARNING_UNCHECKED)
	private  ShipGroupType2 constructRequestS2HOrderItemList2(ObjectFactory pObjectFactory, TRUOrderImpl pOrderImpl, 
			ShippingGroup pGroup, TdfDestinationsType pCreateTdfDestinationsType, 
			Map<String, Double> pShippingChargeMap, Map<String, Double> pShippingDiscountShareMap, Map<String, Double> pOrderDiscountShareMap, 
			ShipGroupType2 pCreateShipGroupType2) {
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructRequestS2HOrderItemList2()");
		}
		List<String> dependencyShippingGruoupIds = ((TRUHardgoodShippingGroup)pGroup).getDependencyShippingGruoupIds();
		OrderItemList2 orderItemList2 = pObjectFactory.createOrderItemList2();
		int lineNumber = 0;
		for (String shipGroupId : dependencyShippingGruoupIds) {
			try {
				ShippingGroup shippingGroup = pOrderImpl.getShippingGroup(shipGroupId);
				TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
				List<ShippingGroupCommerceItemRelationship> commerceItemRelationships = hardgoodShippingGroup.getCommerceItemRelationships();
				if (commerceItemRelationships == null || commerceItemRelationships.isEmpty())  {
					return null;
				}
				TRUCommerceItemMetaInfo metaInfo = null;
				for (ShippingGroupCommerceItemRelationship commerceItemRelationship : commerceItemRelationships) {
					final TRUShippingGroupCommerceItemRelationship shippingGroupCommerceItemRelationship = (TRUShippingGroupCommerceItemRelationship) commerceItemRelationship;
					final CommerceItem commerceItem = shippingGroupCommerceItemRelationship.getCommerceItem();
					if(shippingGroupCommerceItemRelationship.getMetaInfo() != null && (!shippingGroupCommerceItemRelationship.getMetaInfo().isEmpty())){
						vlogDebug("shippingGroupCommerceItemRelationship id :{0}", shippingGroupCommerceItemRelationship.getId());
						vlogDebug("commerceItem id {0}", shippingGroupCommerceItemRelationship.getCommerceItemId());
						vlogDebug("MetaInfo from CommerceItemRelationship :{0}",shippingGroupCommerceItemRelationship.getMetaInfo());
				       }
					else{
						vlogDebug("shippingGroupCommerceItemRelationship id :{0}", shippingGroupCommerceItemRelationship.getId());
						vlogDebug("commerceItem id {0}", shippingGroupCommerceItemRelationship.getCommerceItemId());
						vlogDebug("MetaInfo from CommerceItemRelationship is NULL");
					  }
					
					if (commerceItem instanceof TRUDonationCommerceItem) {
						lineNumber = lineNumber+TRUConstants.INTEGER_NUMBER_ONE;
						OrderItem2 orderItem2 = constructRequestDonationOrderItem2(pObjectFactory, 
								shippingGroupCommerceItemRelationship, lineNumber, pCreateTdfDestinationsType, pOrderDiscountShareMap);
						orderItemList2.getOrderItem().add(orderItem2);
					} else if (!(commerceItem instanceof TRUGiftWrapCommerceItem) && shippingGroupCommerceItemRelationship.getMetaInfo() != null
							&& !shippingGroupCommerceItemRelationship.getMetaInfo().isEmpty()) {
						final ListIterator<TRUCommerceItemMetaInfo> listIterator = shippingGroupCommerceItemRelationship.
								getMetaInfo().listIterator(shippingGroupCommerceItemRelationship.getMetaInfo().size());
						while (listIterator.hasPrevious()) {
							lineNumber = lineNumber+TRUConstants.INTEGER_NUMBER_ONE;
							metaInfo = listIterator.previous();
							vlogDebug("MetaInfo :{0}",metaInfo);
							OrderItem2 orderItem2 = constructRequestS2HOrderItem2(pObjectFactory, pOrderImpl,
									shippingGroupCommerceItemRelationship, metaInfo, lineNumber, pCreateTdfDestinationsType,
									pShippingChargeMap, pShippingDiscountShareMap, pOrderDiscountShareMap, 
									((TRUHardgoodShippingGroup)pGroup).getPriceInfo());
							orderItemList2.getOrderItem().add(orderItem2);
							
							TRUBppItemInfo bppItemInfo = shippingGroupCommerceItemRelationship.getBppItemInfo();
							if (null != bppItemInfo && StringUtils.isNotBlank(bppItemInfo.getId())&& StringUtils.isNotBlank(bppItemInfo.getBppItemId())) {
								lineNumber = lineNumber+TRUConstants.INTEGER_NUMBER_ONE;
								OrderItem2 constructRequestBPPOrderItem2 = constructRequestBPPOrderItem2(pObjectFactory, 
										shippingGroupCommerceItemRelationship, metaInfo, bppItemInfo, pCreateTdfDestinationsType, lineNumber);
								orderItemList2.getOrderItem().add(constructRequestBPPOrderItem2);
							}
						}
					}

				}
				pCreateShipGroupType2.setItems(orderItemList2);
			} catch (ShippingGroupNotFoundException se) {
				vlogError("ShippingGroupNotFoundException in TRURadialTaxwareUtils.constructRequestS2HOrderItemList2() {0}", se);
			} catch (InvalidParameterException ie) {
				vlogError("InvalidParameterException in TRURadialTaxwareUtilsconstructRequestS2HOrderItemList2 {0}", ie);
			}
		}
		return pCreateShipGroupType2;
	}

	/**
	 * Construct request bpp order item2.
	 *
	 * @param pObjectFactory the object factory
	 * @param pSGCIRel the SGCI rel
	 * @param pMetaInfo - MetaInfo 
	 * @param pBPPItemInfo the BPP item info
	 * @param pCreateTdfDestinationsType the create tdf destinations type
	 * @param pLineNumber the line number
	 * @return the order item2
	 */
	private OrderItem2 constructRequestBPPOrderItem2(ObjectFactory pObjectFactory, TRUShippingGroupCommerceItemRelationship pSGCIRel,
			TRUCommerceItemMetaInfo pMetaInfo, TRUBppItemInfo pBPPItemInfo, TdfDestinationsType pCreateTdfDestinationsType, int pLineNumber){
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructRequestBPPOrderItem2()");
		}
		//long quantity = pSGCIRel.getQuantity();
		long metaInfoItemquantity = TRUConstants.ZERO;
		String metaInfoId = null;
		if (pMetaInfo.getPropertyValue(getPropertyManager().getMetaInfoIdPropertyName()) != null) {
			metaInfoId = (String) pMetaInfo.getPropertyValue(getPropertyManager().getMetaInfoIdPropertyName());
		}
		if (pMetaInfo.getPropertyValue(getPropertyManager().getQuantityPropertyName()) != null) {
			metaInfoItemquantity = (long) pMetaInfo.getPropertyValue(getPropertyManager().getQuantityPropertyName());
		}
		
		OrderItem2 orderItem2 = pObjectFactory.createOrderItem2();
		orderItem2.setLineNumber(String.valueOf(pLineNumber));
		orderItem2.setItemId(pBPPItemInfo.getBppSkuId());
		orderItem2.setItemDesc(metaInfoId+TRUConstants.UNDER_SCORE+pBPPItemInfo.getBppItemId());
		
		Origins origins = pObjectFactory.createOrigins();

		TaxDutyPhysicalAddress createAdminOriginTaxDutyPhysicalAddress = constructRequestAdminOrigin(pObjectFactory);
		origins.setAdminOrigin(createAdminOriginTaxDutyPhysicalAddress);

		MailingAddressType2 mailingAddressType2 =
				getRequestMailingAddressById(pCreateTdfDestinationsType, pSGCIRel.getShippingGroupId());
		
		TaxDutyPhysicalAddress createShippingOriginTaxDutyPhysicalAddress = mailingAddressType2.getAddress();
		origins.setShippingOrigin(createShippingOriginTaxDutyPhysicalAddress);

		origins.setManufacturingCountryCode(pObjectFactory.createOriginsManufacturingCountryCode(TRUTaxwareConstant.COUNTRYCODE_US));

		orderItem2.setOrigins(origins);
		orderItem2.setQuantity((int) metaInfoItemquantity);

		OrderItemPricing2  orderItemPricing2 = pObjectFactory.createOrderItemPricing2();
		MerchandisePriceGroup2 merchandisePriceGroup2 = pObjectFactory.createMerchandisePriceGroup2();
		merchandisePriceGroup2.setAmount(BigDecimal.valueOf(getPricingTools().round(pBPPItemInfo.getBppPrice()*metaInfoItemquantity)));
		
		RepositoryItem bppItem = null;
		try {
			bppItem = getProductRepository().getItem(pBPPItemInfo.getBppSkuId(),getPropertyManager().getSkuItem());
			ChangeAwareSet listOfProducts = (ChangeAwareSet) bppItem.getPropertyValue(getPropertyManager().getParentProducts());
			for (Object productObj : listOfProducts) {
				RepositoryItem product = (RepositoryItem) productObj;
				merchandisePriceGroup2.setTaxClass((String) product.getPropertyValue(getPropertyManager().getTaxCodePropertyName()));
				break;
			}
		} catch (RepositoryException repExp) {
			vlogError("RepositoryException in TRURadialTaxwareUtils {0}", repExp);
		}
		merchandisePriceGroup2.setUnitPrice(BigDecimal.valueOf(getPricingTools().round(pBPPItemInfo.getBppPrice())));
		orderItemPricing2.setMerchandise(merchandisePriceGroup2);
		orderItem2.setPricing(orderItemPricing2);
		return orderItem2;
	} 

	/**
	 * Construct request donation order item2.
	 *
	 * @param pObjectFactory the object factory
	 * @param pSGCIRel the SGCI rel
	 * @param pLineNumber the line number
	 * @param pCreateTdfDestinationsType the create tdf destinations type
	 * @param pOrderDiscountShareMap 
	 * @return the order item2
	 */
	private OrderItem2 constructRequestDonationOrderItem2(ObjectFactory pObjectFactory,
			TRUShippingGroupCommerceItemRelationship pSGCIRel, int pLineNumber, TdfDestinationsType pCreateTdfDestinationsType, Map<String, Double> pOrderDiscountShareMap){
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructRequestDonationOrderItem2()");
		}
		CommerceItem commerceItem = pSGCIRel.getCommerceItem();
		TRUItemPriceInfo commerceItempriceInfo = (TRUItemPriceInfo) commerceItem.getPriceInfo();
		long quantity = commerceItem.getQuantity();

		OrderItem2 orderItem2 = pObjectFactory.createOrderItem2();
		orderItem2.setLineNumber(String.valueOf(pLineNumber));
		orderItem2.setItemId(pSGCIRel.getCommerceItem().getCatalogRefId());
		orderItem2.setItemDesc(commerceItem.getId());
		
		Origins origins = pObjectFactory.createOrigins();

		TaxDutyPhysicalAddress createAdminOriginTaxDutyPhysicalAddress = constructRequestAdminOrigin(pObjectFactory);
		origins.setAdminOrigin(createAdminOriginTaxDutyPhysicalAddress);

		MailingAddressType2 mailingAddressType2 =
				getRequestMailingAddressById(pCreateTdfDestinationsType, pSGCIRel.getShippingGroupId());
		if(mailingAddressType2 == null) {
			mailingAddressType2 = getRequestMailingAddressById(pCreateTdfDestinationsType, TRUTaxwareConstant.BILLING_MAILINGADDRESS_ID);
		}
		if(mailingAddressType2 != null) {
			TaxDutyPhysicalAddress createShippingOriginTaxDutyPhysicalAddress = mailingAddressType2.getAddress();
			origins.setShippingOrigin(createShippingOriginTaxDutyPhysicalAddress);
			
			origins.setManufacturingCountryCode(pObjectFactory.createOriginsManufacturingCountryCode(TRUTaxwareConstant.COUNTRYCODE_US));
			
			orderItem2.setOrigins(origins);
			orderItem2.setQuantity((int) quantity);
			
			OrderItemPricing2  orderItemPricing2 = pObjectFactory.createOrderItemPricing2();
			
			MerchandisePriceGroup2 merchandisePriceGroup2 = constructRequestMerchandisePriceGroup2(pObjectFactory, (TRUCommerceItemImpl)commerceItem, commerceItempriceInfo, quantity, null, pOrderDiscountShareMap);
			orderItemPricing2.setMerchandise(merchandisePriceGroup2);
			orderItem2.setPricing(orderItemPricing2);
		}
		return orderItem2;
	}

	/**
	 * Construct request s2 h order item2.
	 *
	 * @param pObjectFactory the object factory
	 * @param pOrderImpl the order impl
	 * @param pShippingGroupCommerceItemRelationship the shipping group commerce item relationship
	 * @param pMetaInfo the meta info
	 * @param pLineNumber the line number
	 * @param pCreateTdfDestinationsType the create tdf destinations type
	 * @param pShippingChargeMap the shipping charge map
	 * @param pShippingDiscountShareMap the shipping discount share map
	 * @param pOrderDiscountShareMap the order discount share map
	 * @param pShippingPriceInfo the shipping price info
	 * @return the order item2
	 */
	private OrderItem2 constructRequestS2HOrderItem2(ObjectFactory pObjectFactory, TRUOrderImpl pOrderImpl,
			TRUShippingGroupCommerceItemRelationship pShippingGroupCommerceItemRelationship, 
			TRUCommerceItemMetaInfo pMetaInfo, int pLineNumber, TdfDestinationsType pCreateTdfDestinationsType,
			Map<String, Double> pShippingChargeMap, Map<String, Double> pShippingDiscountShareMap, Map<String, Double> pOrderDiscountShareMap, 
			ShippingPriceInfo pShippingPriceInfo){
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructRequestS2HOrderItem2()");
		}
		CommerceItem commerceItem = pShippingGroupCommerceItemRelationship.getCommerceItem();
		TRUItemPriceInfo commerceItempriceInfo = (TRUItemPriceInfo) commerceItem.getPriceInfo();
		long metaInfoItemquantity = TRUConstants.ZERO;
		String metaInfoId = null;
		if (pMetaInfo.getPropertyValue(getPropertyManager().getMetaInfoIdPropertyName()) != null) {
			metaInfoId = (String) pMetaInfo.getPropertyValue(getPropertyManager().getMetaInfoIdPropertyName());
		}
		if (pMetaInfo.getPropertyValue(getPropertyManager().getQuantityPropertyName()) != null) {
			metaInfoItemquantity = (long) pMetaInfo.getPropertyValue(getPropertyManager().getQuantityPropertyName());
		}

		RepositoryItem productItem = (RepositoryItem) commerceItem.getAuxiliaryData().getProductRef();
		OrderItem2 orderItem2 = pObjectFactory.createOrderItem2();
		orderItem2.setLineNumber(String.valueOf(pLineNumber));
		orderItem2.setItemId(pShippingGroupCommerceItemRelationship.getCommerceItem().getCatalogRefId());
		orderItem2.setItemDesc(metaInfoId);
		
		Origins origins = pObjectFactory.createOrigins();

		TaxDutyPhysicalAddress createAdminOriginTaxDutyPhysicalAddress = constructRequestAdminOrigin(pObjectFactory);
		origins.setAdminOrigin(createAdminOriginTaxDutyPhysicalAddress);

		MailingAddressType2 mailingAddressType2 =
				getRequestMailingAddressById(pCreateTdfDestinationsType, pShippingGroupCommerceItemRelationship.getShippingGroupId());
		TaxDutyPhysicalAddress createShippingOriginTaxDutyPhysicalAddress = mailingAddressType2.getAddress();
		origins.setShippingOrigin(createShippingOriginTaxDutyPhysicalAddress);

		origins.setManufacturingCountryCode(pObjectFactory.createOriginsManufacturingCountryCode(TRUTaxwareConstant.COUNTRYCODE_US));

		orderItem2.setOrigins(origins);
		orderItem2.setQuantity((int) metaInfoItemquantity);

		OrderItemPricing2  orderItemPricing2 = pObjectFactory.createOrderItemPricing2();

		MerchandisePriceGroup2 merchandisePriceGroup2 = constructRequestMerchandisePriceGroup2(pObjectFactory, (TRUCommerceItemImpl)commerceItem, commerceItempriceInfo, metaInfoItemquantity, pMetaInfo, pOrderDiscountShareMap);
		orderItemPricing2.setMerchandise(merchandisePriceGroup2);
		FeeListRequest2 feeListRequest2 = constructRequestFeeListRequest2(pObjectFactory, productItem, metaInfoItemquantity, mailingAddressType2.getAddress().getMainDivision());
		if(feeListRequest2 != null) {
			orderItemPricing2.setFees(feeListRequest2);
		}

		PriceGroup2 shippingPriceGroup2 = constructRequestShippingPriceGroup2(pObjectFactory,
				pShippingGroupCommerceItemRelationship, metaInfoItemquantity, metaInfoId, pShippingChargeMap, pShippingDiscountShareMap,
				pShippingPriceInfo);
		orderItemPricing2.setShipping(shippingPriceGroup2);

		if (pMetaInfo.getPropertyValue(getPropertyManager().getGiftWrapItemPropertyName()) != null) {
			GiftPackaging2 giftPackaging2 = constructRequestGiftPackaging2(pObjectFactory, pOrderImpl, pMetaInfo);
			orderItem2.setGifting(giftPackaging2);
		}
		orderItem2.setPricing(orderItemPricing2);
		return orderItem2;
	}

	/**
	 * Construct request fee list request2.
	 *
	 * @param pObjectFactory the object factory
	 * @param pProdItem the prod item
	 * @param pQuantity the quantity
	 * @param pState the state
	 * @return the fee list request2
	 */
	private FeeListRequest2 constructRequestFeeListRequest2(ObjectFactory pObjectFactory, RepositoryItem pProdItem, long pQuantity, String pState) {
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructRequestFeeListRequest2()");
		}
		//E Waste Fee
		String eWasteSurchargeState = TRUTaxwareConstant.EMPTY_STRING;
		FeeListRequest2 createFeeListRequest2 = pObjectFactory.createFeeListRequest2();
		try {
			String ewasteSurchargeSku = null;
			if(pProdItem != null){
			 eWasteSurchargeState = (String) pProdItem.getPropertyValue(getPricingTools().getCatalogProperties().getEwasteSurchargeStatePropertyName());
			 if(!StringUtils.isBlank(eWasteSurchargeState) && eWasteSurchargeState.equals(pState)) {
					ewasteSurchargeSku = (String) pProdItem.getPropertyValue(getPricingTools().getCatalogProperties().getEwasteSurchargeSkuPropertyName());
				}
			}
			if(StringUtils.isNotBlank(ewasteSurchargeSku)) {
				String eWasteSurchargeProdId = null;
				RepositoryItem eWasteProductItem = null;
				if(pProdItem != null) {
					eWasteSurchargeProdId = (String) pProdItem.getPropertyValue(getPricingTools().getCatalogProperties().getEwasteSurchargeSkuPropertyName());
				}
				if(eWasteSurchargeProdId == null) {
					if (isLoggingDebug()) {
						vlogDebug("EXIT: TRURadialTaxwareUtils.constructRequestFeeListRequest2() eWasteSurchargeProdId is null");
					}
					return null;
				}
				eWasteProductItem = getPricingTools().getCatalogTools().findProduct(eWasteSurchargeProdId);
				if(eWasteProductItem == null) {
					if (isLoggingDebug()) {
						vlogDebug("EXIT: TRURadialTaxwareUtils.constructRequestFeeListRequest2() eWasteProductItem is null");
					}
					return null;
				}
				double eWastePrice = TRUTaxwareConstant.DECIMAL_ZERO;
				eWastePrice = getPricingTools().getEWastePrice(pProdItem);
				PriceGroup2 paramPriceGroup2 = pObjectFactory.createPriceGroup2();
				paramPriceGroup2.setAmount(BigDecimal.valueOf(getPricingTools().round(eWastePrice*pQuantity)));
				paramPriceGroup2.setTaxClass((String)eWasteProductItem.getPropertyValue(getPropertyManager().getTaxCodePropertyName()));

				// Getting Repository Item for EWaste Product ID
				// Getting Product of EWaste SKU
				if(eWasteProductItem != null) {
					RepositoryItem eWasteSKUItem = ((TRUCatalogTools)getPricingTools().getCatalogTools()).getDefaultChildSku(eWasteProductItem);
					if(eWasteSKUItem != null) {
						FeeRequest2 createFeeRequest2 = pObjectFactory.createFeeRequest2();
						createFeeRequest2.setFeeType(TRUTaxwareConstant.FEETYPE_EWASTE);
						createFeeRequest2.setDescription(eWasteSKUItem.getItemDisplayName());
						createFeeRequest2.setFeeId(eWasteSKUItem.getRepositoryId());
						createFeeRequest2.setCharge(paramPriceGroup2);
						createFeeListRequest2.getFee().add(createFeeRequest2);
					}
				}
			}
		} catch (RepositoryException repositoryExce) {
			if(isLoggingError()) {
				vlogError("RepositoryException in TRURadialTaxwareUtils {0}", repositoryExce);
			}
		}
		if(createFeeListRequest2.getFee().isEmpty()) {
			return null;
		}
		return createFeeListRequest2;
	}

	/**
	 * Construct request ispu order item2.
	 *
	 * @param pObjectFactory the object factory
	 * @param pShippingGroupCommerceItemRelationship the shipping group commerce item relationship
	 * @param pMetaInfo the meta info
	 * @param pCreateTdfDestinationsType the create tdf destinations type
	 * @param pLineNumber the line number
	 * @param pOrderDiscountShareMap 
	 * @return the order item2
	 */
	private OrderItem2 constructRequestISPUOrderItem2(ObjectFactory pObjectFactory,  
			TRUShippingGroupCommerceItemRelationship pShippingGroupCommerceItemRelationship, 
			TRUCommerceItemMetaInfo pMetaInfo, TdfDestinationsType pCreateTdfDestinationsType, int pLineNumber, Map<String, Double> pOrderDiscountShareMap){
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructRequestISPUOrderItem2()");
		}
		CommerceItem commerceItem = pShippingGroupCommerceItemRelationship.getCommerceItem();
		TRUItemPriceInfo commerceItempriceInfo = (TRUItemPriceInfo) commerceItem.getPriceInfo();
		long metaInfoItemquantity = TRUConstants.ZERO;
		String metaInfoId = null;
		if (pMetaInfo.getPropertyValue(getPropertyManager().getMetaInfoIdPropertyName()) != null) {
			metaInfoId = (String) pMetaInfo.getPropertyValue(getPropertyManager().getMetaInfoIdPropertyName());
		}
		if (pMetaInfo.getPropertyValue(getPropertyManager().getQuantityPropertyName()) != null) {
			metaInfoItemquantity = (long) pMetaInfo.getPropertyValue(getPropertyManager().getQuantityPropertyName());
		}

		RepositoryItem productItem = (RepositoryItem) commerceItem.getAuxiliaryData().getProductRef();
		OrderItem2 orderItem2 = pObjectFactory.createOrderItem2();
		orderItem2.setLineNumber(String.valueOf(pLineNumber));
		orderItem2.setItemId(pShippingGroupCommerceItemRelationship.getCommerceItem().getCatalogRefId());
		orderItem2.setItemDesc(metaInfoId);
		
		Origins origins = pObjectFactory.createOrigins();

		TaxDutyPhysicalAddress createAdminOriginTaxDutyPhysicalAddress = constructRequestAdminOrigin(pObjectFactory);
		origins.setAdminOrigin(createAdminOriginTaxDutyPhysicalAddress);

		MailingAddressType2 mailingAddressType2 =
				getRequestMailingAddressById(pCreateTdfDestinationsType, pShippingGroupCommerceItemRelationship.getShippingGroupId());
		
		TaxDutyPhysicalAddress createShippingOriginTaxDutyPhysicalAddress = mailingAddressType2.getAddress();
		origins.setShippingOrigin(createShippingOriginTaxDutyPhysicalAddress);

		origins.setManufacturingCountryCode(pObjectFactory.createOriginsManufacturingCountryCode(TRUTaxwareConstant.COUNTRYCODE_US));

		orderItem2.setOrigins(origins);
		orderItem2.setQuantity((int) metaInfoItemquantity);

		OrderItemPricing2  orderItemPricing2 = pObjectFactory.createOrderItemPricing2();

		MerchandisePriceGroup2 merchandisePriceGroup2 = constructRequestMerchandisePriceGroup2(pObjectFactory, (TRUCommerceItemImpl)commerceItem, commerceItempriceInfo, 
				metaInfoItemquantity, pMetaInfo, pOrderDiscountShareMap);
		//Start :: TUW-65382
		if(productItem != null && productItem.getPropertyValue(getPropertyManager().getTaxCodePropertyName()) != null) {
			merchandisePriceGroup2.setTaxClass((String)productItem.getPropertyValue(getPropertyManager().getTaxCodePropertyName()));
			//End :: TUW-65382
		}
		orderItemPricing2.setMerchandise(merchandisePriceGroup2);
		FeeListRequest2 feeListRequest2 = constructRequestFeeListRequest2(pObjectFactory, productItem, metaInfoItemquantity, mailingAddressType2.getAddress().getMainDivision());
		if(feeListRequest2 != null) {
			orderItemPricing2.setFees(feeListRequest2);
		}

		orderItem2.setPricing(orderItemPricing2);
		return orderItem2;
	}

	/**
	 * Construct request merchandise price group2.
	 *
	 * @param pObjectFactory the object factory
	 * @param pCommerceItemImpl the commerce item impl
	 * @param pCommerceItempriceInfo the commerce itemprice info
	 * @param pMetaInfoItemquantity the meta info itemquantity
	 * @param pMetaInfo  - Meta Info
	 * @param pOrderDiscountShareMap 
	 * @return the merchandise price group2
	 */
	private MerchandisePriceGroup2 constructRequestMerchandisePriceGroup2(ObjectFactory pObjectFactory, TRUCommerceItemImpl pCommerceItemImpl,
			TRUItemPriceInfo pCommerceItempriceInfo, long pMetaInfoItemquantity, TRUCommerceItemMetaInfo pMetaInfo, Map<String, Double> pOrderDiscountShareMap){
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructRequestMerchandisePriceGroup2()");
		}
		long merchTaxAppliedQuantity = pCommerceItemImpl.getTaxAppliedQuantity();
		BigDecimal merchTaxAppliedAmount = pCommerceItemImpl.getTaxAppliedAmount();
		BigDecimal merchTaxAmount = new BigDecimal(TRUConstants.INT_ZERO);
		RepositoryItem productItem = (RepositoryItem) pCommerceItemImpl.getAuxiliaryData().getProductRef();
		if(pCommerceItemImpl.getQuantity() == merchTaxAppliedQuantity + pMetaInfoItemquantity){
			BigDecimal totalMerchAmount = BigDecimal.valueOf(getPricingTools().round(pCommerceItempriceInfo.getSalePrice()*pCommerceItemImpl.getQuantity()));
			merchTaxAmount = totalMerchAmount.subtract(merchTaxAppliedAmount);
			pCommerceItemImpl.setTaxAppliedAmount(new BigDecimal(TRUConstants.INT_ZERO));
			pCommerceItemImpl.setTaxAppliedQuantity(TRUConstants.INT_ZERO);
		} else {
			merchTaxAmount = BigDecimal.valueOf(getPricingTools().round(pCommerceItempriceInfo.getSalePrice()*pMetaInfoItemquantity));
			merchTaxAppliedAmount = merchTaxAppliedAmount.add(merchTaxAmount);
			pCommerceItemImpl.setTaxAppliedAmount(merchTaxAppliedAmount);
			merchTaxAppliedQuantity = merchTaxAppliedQuantity+pMetaInfoItemquantity;
			pCommerceItemImpl.setTaxAppliedQuantity(merchTaxAppliedQuantity);
		}

		MerchandisePriceGroup2 merchandisePriceGroup2 = pObjectFactory.createMerchandisePriceGroup2();
		merchandisePriceGroup2.setAmount(BigDecimal.valueOf(getPricingTools().round(pCommerceItempriceInfo.getSalePrice()*pMetaInfoItemquantity)));

		//Start :: TUW-65382
		if(productItem != null && productItem.getPropertyValue(getPropertyManager().getTaxCodePropertyName()) != null) {
			merchandisePriceGroup2.setTaxClass((String)productItem.getPropertyValue(getPropertyManager().getTaxCodePropertyName()));
		}
		//End :: TUW-65382

		PromotionalDiscounts2 promotionalDiscounts2 = pObjectFactory.createPromotionalDiscounts2();

		if(pOrderDiscountShareMap != null && !pOrderDiscountShareMap.isEmpty() && pMetaInfo != null) {
			Iterator<String> truOrderDiscountShareIterator = pOrderDiscountShareMap.keySet().iterator();
			while(truOrderDiscountShareIterator.hasNext()){
				String key = (String) truOrderDiscountShareIterator.next();
				double discShare = pOrderDiscountShareMap.get(key);
				String[] keySplit = key.split(PIPE_DELIMETRE);
				String metaInfoId = keySplit[TRUConstants.INT_ZERO];
				String promotionId = keySplit[TRUConstants.INTEGER_NUMBER_ONE];
				if(metaInfoId.equals(pMetaInfo.getId())){
					Discount2 discount2 = pObjectFactory.createDiscount2();
					if(promotionId.length() > TRUConstants.TWELVE) {
						discount2.setId(promotionId.substring(promotionId.length()-TRUConstants.TWELVE));
					} else {
						discount2.setId(promotionId);
					}
					discount2.setAmount(BigDecimal.valueOf(discShare));
					promotionalDiscounts2.getDiscount().add(discount2);
				}
			}
		}

		getItemDiscountShare(pObjectFactory, pMetaInfo, pCommerceItemImpl, promotionalDiscounts2);

		if(promotionalDiscounts2.getDiscount().size() > TRUConstants.INT_ZERO) {
			merchandisePriceGroup2.setPromotionalDiscounts(promotionalDiscounts2);
		}
		merchandisePriceGroup2.setUnitPrice(BigDecimal.valueOf(getPricingTools().round(pCommerceItempriceInfo.getSalePrice())));
		return merchandisePriceGroup2;
	}

	/**
	 * Construct request shipping price group2.
	 *
	 * @param pObjectFactory the object factory
	 * @param pShippingGroupCommerceItemRelationship the shipping group commerce item relationship
	 * @param pMetaInfoItemquantity the meta info itemquantity
	 * @param pMetaInfoId the meta info id
	 * @param pShippingChargeMap the shipping charge map
	 * @param pShippingDiscountShareMap the shipping discount share map
	 * @param pShippingPriceInfo the shipping price info
	 * @return the price group2
	 */
	@SuppressWarnings("unchecked")
	private PriceGroup2 constructRequestShippingPriceGroup2(ObjectFactory pObjectFactory,
			TRUShippingGroupCommerceItemRelationship pShippingGroupCommerceItemRelationship, long pMetaInfoItemquantity,
			String pMetaInfoId, Map<String, Double> pShippingChargeMap, Map<String, Double> pShippingDiscountShareMap, 
			ShippingPriceInfo pShippingPriceInfo){
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructRequestShippingPriceGroup2()");
		}
		double totalSurcharge = pShippingGroupCommerceItemRelationship.getSurcharge();
		double surcharge = TRUConstants.DOUBLE_ZERO;
		if(totalSurcharge > TRUConstants.DOUBLE_ZERO) {
			surcharge = (totalSurcharge/pShippingGroupCommerceItemRelationship.getQuantity())*pMetaInfoItemquantity;
		}
		PriceGroup2 shippingPriceGroup2 = pObjectFactory.createPriceGroup2();
		shippingPriceGroup2.setAmount(BigDecimal.valueOf(getPricingTools().round(pShippingChargeMap.get(pMetaInfoId)+surcharge)));
		
		PromotionalDiscounts2 shippingPromotionalDiscounts2 = pObjectFactory.createPromotionalDiscounts2();
		if(pShippingDiscountShareMap != null && !pShippingDiscountShareMap.isEmpty()){
			List<PricingAdjustment> shippingPricingAdjustment = pShippingPriceInfo.getAdjustments();
			if(shippingPricingAdjustment != null && !shippingPricingAdjustment.isEmpty()){
				for (PricingAdjustment pricingAdjustment2 : shippingPricingAdjustment) {
					if(pricingAdjustment2.getTotalAdjustment() < TRUConstants.INT_ZERO && pricingAdjustment2.getPricingModel() != null) {
						String promoId = pricingAdjustment2.getPricingModel().getRepositoryId();
						Double double1 = pShippingDiscountShareMap.get(pMetaInfoId+TRUConstants.PIPE_STRING+promoId);
						if(double1 != null) {
							Discount2 shippingDiscount2 = pObjectFactory.createDiscount2();
							if(promoId.length() > TRUConstants.TWELVE) {
								shippingDiscount2.setId(promoId.substring(promoId.length()-TRUConstants.TWELVE));
							} else {
								shippingDiscount2.setId(promoId);
							}
							shippingDiscount2.setAmount(BigDecimal.valueOf(double1));
							shippingPromotionalDiscounts2.getDiscount().add(shippingDiscount2);
						}
					}
				}
			}
		}
		if(shippingPromotionalDiscounts2.getDiscount().size() > TRUConstants.INT_ZERO) {
			shippingPriceGroup2.setPromotionalDiscounts(shippingPromotionalDiscounts2);
		}
		return shippingPriceGroup2;
	}

	/**
	 * Construct request gift packaging2.
	 *
	 * @param pObjectFactory the object factory
	 * @param pOrderImpl the order impl
	 * @param pMetaInfo the meta info
	 * @return the gift packaging2
	 */
	@SuppressWarnings(TRUTaxwareConstant.SUPPRESS_WARNING_UNCHECKED)
	private GiftPackaging2 constructRequestGiftPackaging2(ObjectFactory pObjectFactory, TRUOrderImpl pOrderImpl, TRUCommerceItemMetaInfo pMetaInfo ){
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructRequestGiftPackaging2()");
		}
		RepositoryItem giftItem = (RepositoryItem) pMetaInfo.getPropertyValue(getPropertyManager()
				.getGiftWrapItemPropertyName());
		GiftPackaging2 giftPackaging2 = pObjectFactory.createGiftPackaging2();
		try {
			TRUGiftWrapCommerceItem truGiftWrapCommerceItem = (TRUGiftWrapCommerceItem) pOrderImpl
					.getCommerceItem(giftItem.getRepositoryId());
			long truGiftWrapMIQty = (long) pMetaInfo.getPropertyValue(getPropertyManager().getQuantityPropertyName());
			long truGiftWrapCIQty = (long) truGiftWrapCommerceItem.getQuantity();
			ItemPriceInfo gwCommerceItempriceInfo = truGiftWrapCommerceItem.getPriceInfo();
			giftPackaging2.setItemId(truGiftWrapCommerceItem.getCatalogRefId());
			RepositoryItem product = (RepositoryItem)truGiftWrapCommerceItem.getAuxiliaryData().getProductRef();
			
			giftPackaging2.setItemDesc(giftItem.getRepositoryId()+TRUTaxwareConstant.GIFTPACKAGING2_ITEMID);
			MerchandisePriceGroup2 gwMerchandisePriceGroup2 = pObjectFactory.createMerchandisePriceGroup2();
			gwMerchandisePriceGroup2.setAmount(BigDecimal.valueOf(getPricingTools().round(gwCommerceItempriceInfo.getSalePrice()*truGiftWrapMIQty)));
			gwMerchandisePriceGroup2.setUnitPrice(BigDecimal.valueOf(getPricingTools().round(gwCommerceItempriceInfo.getSalePrice())));
			gwMerchandisePriceGroup2.setTaxClass((String)product.getPropertyValue(getPropertyManager().getTaxCodePropertyName()));
			List<PricingAdjustment> gwPricingAdjustment = gwCommerceItempriceInfo.getAdjustments();
			if(gwPricingAdjustment != null && !gwPricingAdjustment.isEmpty()){
				PromotionalDiscounts2 gwPromotionalDiscounts2 = pObjectFactory.createPromotionalDiscounts2();
				for (PricingAdjustment gwPricingAdjustment2 : gwPricingAdjustment) {
					if(gwPricingAdjustment2.getTotalAdjustment() < TRUConstants.INT_ZERO && gwPricingAdjustment2.getPricingModel() != null) {
						Discount2 gwDiscount2 = pObjectFactory.createDiscount2();
						String promoId = gwPricingAdjustment2.getPricingModel().getRepositoryId();
						if(promoId.length() > TRUConstants.TWELVE) {
							gwDiscount2.setId(promoId.substring(promoId.length()-TRUConstants.TWELVE));
						} else {
							gwDiscount2.setId(promoId);
						}
						gwDiscount2.setAmount(BigDecimal.valueOf(getPricingTools().round((-gwPricingAdjustment2.getTotalAdjustment()/truGiftWrapCIQty))*truGiftWrapMIQty));
						gwPromotionalDiscounts2.getDiscount().add(gwDiscount2);
					}
				}
				if(gwPromotionalDiscounts2.getDiscount().size() > TRUConstants.INT_ZERO) {
					gwMerchandisePriceGroup2.setPromotionalDiscounts(gwPromotionalDiscounts2);
				}
			}
			giftPackaging2.setPricing(gwMerchandisePriceGroup2);
		} catch (CommerceItemNotFoundException e) {
			if(isLoggingError()) {
				vlogError("CommerceItemNotFoundException in TRURadialTaxwareUtils {0}", e);
			}
		} catch (InvalidParameterException e) {
			if(isLoggingError()) {
				vlogError("InvalidParameterException in TRURadialTaxwareUtils {0}", e);
			}
		}
		return giftPackaging2; 
	}

	/**
	 * Marshal xml.
	 *
	 * @param pJAXBObject the JAXB object
	 * @param pPackage the package
	 * @return the string
	 */
	public String marshalXML(Object pJAXBObject, String pPackage) {
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.marshalXML()");
		}
		String xmlRequest = TRUConstants.EMPTY_STRING;
		if(pJAXBObject == null){
			return xmlRequest;
		}
		StringWriter sw = new StringWriter();
		try {
			JAXBContext	context = JAXBContext.newInstance(pPackage);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(TRUTaxwareConstant.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.marshal(pJAXBObject, sw);
			xmlRequest = sw.toString();
			sw.close();
		} catch (JAXBException exc) {
			if(isLoggingError()) {
				vlogError("JAXBException in TRURadialTaxwareUtils {0}", exc);
			}
		} catch (IOException exc) {
			if(isLoggingError()) {
				vlogError("IOException in TRURadialTaxwareUtils {0}", exc);
			}
		}
		return xmlRequest;
	}

	/**
	 * Unmarshal xml.
	 *
	 * @param pConnection the connection
	 * @param pPackage the package
	 * @return the object
	 */
	public Object unMarshalXML(HttpURLConnection pConnection, String pPackage) {
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.unMarshalXML()");
		}
		Object unmarshalObj = null;
		InputStream inputStream = null;
		try{
			inputStream = pConnection.getInputStream();
			JAXBContext jaxbContext = JAXBContext.newInstance(pPackage);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			unmarshalObj = jaxbUnmarshaller.unmarshal(inputStream);
		} catch (JAXBException exc) {
			if(isLoggingError()) {
				vlogError("JAXBException in TRURadialTaxwareUtils {0}", exc);
			}
		} catch (IOException exc) {
			if(isLoggingError()) {
				vlogError("IOException in TRURadialTaxwareUtils {0}", exc);
			}
		}finally{
			if(inputStream != null){
				try {
					inputStream.close();
				} catch (IOException exc) {
					if(isLoggingError()) {
						vlogError("IOException in TRURadialTaxwareUtils {0}", exc);
					}
				}
			}
		}
		return unmarshalObj;
	}

	/**
	 * Format xml.
	 *
	 * @param pInput the input
	 * @return the string
	 */
	public String formatXML(String pInput) {
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.formatXML()");
		}
		StringWriter sw = new StringWriter();  
		try {
			Document doc = DocumentHelper.parseText(pInput);  
			OutputFormat format = OutputFormat.createPrettyPrint();  
			format.setIndent(true);
			format.setIndentSize(TRUTaxwareConstant.NUMERIC_THREE);
			XMLWriter xw = new XMLWriter(sw, format);  
			xw.write(doc);
		} catch (IOException e) {
			if(isLoggingError()) {
				vlogError("IOException in TRURadialTaxwareUtils {0}", e);
			}
		} catch (DocumentException e) {
			if(isLoggingError()) {
				vlogError("DocumentException in TRURadialTaxwareUtils {0}", e);
			}
		}  
		return sw.toString();
	}

	/**
	 * Construct request admin origin.
	 *
	 * @param pObjectFactory the object factory
	 * @return the tax duty physical address
	 */
	private TaxDutyPhysicalAddress constructRequestAdminOrigin(ObjectFactory pObjectFactory) {
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructRequestAdminOrigin()");
		}
		TaxDutyPhysicalAddress createAdminOriginTaxDutyPhysicalAddress = pObjectFactory.createTaxDutyPhysicalAddress();
		createAdminOriginTaxDutyPhysicalAddress.setLine1(getConfiguration().getSoftDesStreet());
		createAdminOriginTaxDutyPhysicalAddress.setCity(getConfiguration().getSoftDesCityName());
		createAdminOriginTaxDutyPhysicalAddress.setMainDivision(getConfiguration().getSoftDesRegion());
		createAdminOriginTaxDutyPhysicalAddress.setCountryCode(getConfiguration().getSoftDesCountryCode());
		createAdminOriginTaxDutyPhysicalAddress.setPostalCode(getConfiguration().getSoftDesPostalCode());
		return createAdminOriginTaxDutyPhysicalAddress;
	}

	/**
	 * Construct request destinations.
	 *
	 * @param pObjectFactory the object factory
	 * @param pOrderImpl the order impl
	 * @return the tdf destinations type
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private TdfDestinationsType constructRequestDestinations(ObjectFactory pObjectFactory, TRUOrderImpl pOrderImpl) {
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructRequestDestinations()");
		}
		TdfDestinationsType createTdfDestinationsType = pObjectFactory.createTdfDestinationsType();
		MailingAddressType2 createBillingMailingAddress = constructRequestBillingAddress(pObjectFactory, pOrderImpl);
		if(createBillingMailingAddress != null) {
			createTdfDestinationsType.getMailingAddress().add(createBillingMailingAddress);
		}

		List<ShippingGroup> shippingGroups = pOrderImpl.getShippingGroups();
		ShippingGroup group = null;
		Iterator groupIterator = shippingGroups.iterator();
		MailingAddressType2 mailingAddressType2 = null;
		while (groupIterator.hasNext()) {
			group = (ShippingGroup) groupIterator.next();
			if (!(group.getCommerceItemRelationshipCount() > TRUTaxwareConstant.NUMERIC_ZERO)) {
				continue;
			}
			if(group instanceof TRUHardgoodShippingGroup) {
				mailingAddressType2 = constructRequestShippingAddress(pObjectFactory, group);
				if(mailingAddressType2 != null && null != mailingAddressType2.getAddress()) {
					createTdfDestinationsType.getMailingAddress().add(mailingAddressType2);
				}
			}
			if(group instanceof TRUInStorePickupShippingGroup) {
				mailingAddressType2 = constructRequestStoreAddress(pObjectFactory, group);
				createTdfDestinationsType.getMailingAddress().add(mailingAddressType2);
			}
		}

		return createTdfDestinationsType;
	}

	/**
	 * Construct request store address.
	 *
	 * @param pObjectFactory the object factory
	 * @param pShippingGroup the shipping group
	 * @return the mailing address type2
	 */
	private MailingAddressType2 constructRequestStoreAddress(ObjectFactory pObjectFactory, ShippingGroup pShippingGroup) {
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructRequestStoreAddress()");
		}
		TRUInStorePickupShippingGroup inStorePickupShippingGroup = (TRUInStorePickupShippingGroup) pShippingGroup;
		String storeLocationId = ((TRUInStorePickupShippingGroup) pShippingGroup).getLocationId();
		MailingAddressType2 createStoreMailingAddress = pObjectFactory.createMailingAddressType2();
		createStoreMailingAddress = pObjectFactory.createMailingAddressType2();
		createStoreMailingAddress.setId(inStorePickupShippingGroup.getId());
		if(StringUtils.isNotBlank(storeLocationId)) {
			PersonNameType createBillingPersonNameType = pObjectFactory.createPersonNameType();
			if(inStorePickupShippingGroup.getFirstName() != null) {
				createBillingPersonNameType.setFirstName(inStorePickupShippingGroup.getFirstName());
			} else {
				createBillingPersonNameType.setFirstName(TRUConstants.EMPTY_STRING);
			}
			if(inStorePickupShippingGroup.getLastName() != null) {
				createBillingPersonNameType.setLastName(inStorePickupShippingGroup.getLastName());
			} else {
				createBillingPersonNameType.setLastName(TRUConstants.EMPTY_STRING);
			}
			createStoreMailingAddress.setPersonName(createBillingPersonNameType);
			TaxDutyPhysicalAddress createBillingTaxDutyPhysicalAddress = pObjectFactory.createTaxDutyPhysicalAddress();

			try {
				RepositoryItem item = (RepositoryItem) getLocationRepository().getItem(storeLocationId,
						getPropertyManager().getStoreItemDescriptorName());
				if (item != null) {
					createBillingTaxDutyPhysicalAddress.setLine1((String) item.getPropertyValue(getPropertyManager().getStoreAddress1()));
					createBillingTaxDutyPhysicalAddress.setCity((String) item.getPropertyValue(getPropertyManager().getCityName()));
					createBillingTaxDutyPhysicalAddress.setMainDivision((String) item.getPropertyValue(getPropertyManager().getStoreStateAddress()));
					createBillingTaxDutyPhysicalAddress.setCountryCode(TRUTaxwareConstant.COUNTRYCODE_US);
					createBillingTaxDutyPhysicalAddress.setPostalCode((String) item.getPropertyValue(getPropertyManager().getStorePostalCode()));
				}

			} catch (RepositoryException e) {
				if(isLoggingError()) {
					vlogError("RepositoryException in TRURadialTaxwareUtils {0}", e);
				}
			}
			createStoreMailingAddress.setAddress(createBillingTaxDutyPhysicalAddress);
		}
		return createStoreMailingAddress;
	}

	/**
	 * Construct request shipping address.
	 *
	 * @param pObjectFactory the object factory
	 * @param pShippingGroup the shipping group
	 * @return the mailing address type2
	 */
	private MailingAddressType2 constructRequestShippingAddress(ObjectFactory pObjectFactory, ShippingGroup pShippingGroup) {
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructRequestShippingAddress()");
		}
		Address shippingAddress = ((TRUHardgoodShippingGroup)pShippingGroup).getShippingAddress();
		MailingAddressType2 createShippingMailingAddress = pObjectFactory.createMailingAddressType2();
		createShippingMailingAddress = pObjectFactory.createMailingAddressType2();
		createShippingMailingAddress.setId(pShippingGroup.getId());
		if(StringUtils.isNotBlank(shippingAddress.getFirstName())) {
			PersonNameType createBillingPersonNameType = pObjectFactory.createPersonNameType();
			createBillingPersonNameType.setFirstName(shippingAddress.getFirstName());
			createBillingPersonNameType.setLastName(shippingAddress.getLastName());
			createShippingMailingAddress.setPersonName(createBillingPersonNameType);
			TaxDutyPhysicalAddress createBillingTaxDutyPhysicalAddress = pObjectFactory.createTaxDutyPhysicalAddress();
			createBillingTaxDutyPhysicalAddress.setLine1(shippingAddress.getAddress1());
			createBillingTaxDutyPhysicalAddress.setCity(shippingAddress.getCity());
			createBillingTaxDutyPhysicalAddress.setMainDivision(shippingAddress.getState());
			createBillingTaxDutyPhysicalAddress.setCountryCode(shippingAddress.getCountry());
			createBillingTaxDutyPhysicalAddress.setPostalCode(shippingAddress.getPostalCode());
			createShippingMailingAddress.setAddress(createBillingTaxDutyPhysicalAddress);
		} 
		return createShippingMailingAddress;
	}


	/**
	 * Construct request billing address.
	 *
	 * @param pObjectFactory the object factory
	 * @param pOrderImpl the order impl
	 * @return the mailing address type2
	 */
	private MailingAddressType2 constructRequestBillingAddress(ObjectFactory pObjectFactory, TRUOrderImpl pOrderImpl) {
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.constructRequestBillingAddress()");
		}
		MailingAddressType2 createBillingMailingAddress = null;
		Address billingAddress = pOrderImpl.getBillingAddress();
		if(billingAddress != null && StringUtils.isNotBlank(billingAddress.getFirstName())) {
			createBillingMailingAddress = pObjectFactory.createMailingAddressType2();
			createBillingMailingAddress.setId(TRUTaxwareConstant.BILLING_MAILINGADDRESS_ID);
			PersonNameType createBillingPersonNameType = pObjectFactory.createPersonNameType();
			createBillingPersonNameType.setFirstName(billingAddress.getFirstName());
			createBillingPersonNameType.setLastName(billingAddress.getLastName());
			createBillingMailingAddress.setPersonName(createBillingPersonNameType);
			TaxDutyPhysicalAddress createBillingTaxDutyPhysicalAddress = pObjectFactory.createTaxDutyPhysicalAddress();
			createBillingTaxDutyPhysicalAddress.setLine1(billingAddress.getAddress1());
			createBillingTaxDutyPhysicalAddress.setCity(billingAddress.getCity());
			createBillingTaxDutyPhysicalAddress.setMainDivision(billingAddress.getState());
			createBillingTaxDutyPhysicalAddress.setCountryCode(billingAddress.getCountry());
			createBillingTaxDutyPhysicalAddress.setPostalCode(billingAddress.getPostalCode());
			createBillingMailingAddress.setAddress(createBillingTaxDutyPhysicalAddress);
			if (isLoggingDebug()) {
				vlogDebug("TRURadialTaxwareUtils.constructRequestBillingAddress() id: {0}", TRUTaxwareConstant.BILLING_MAILINGADDRESS_ID);
			}
		}
		return createBillingMailingAddress;
	}

	/**
	 * Gets the request mailing address by id.
	 *
	 * @param pCreateTdfDestinationsType the create tdf destinations type
	 * @param pId the id
	 * @return the request mailing address by id
	 */
	private  MailingAddressType2 getRequestMailingAddressById(TdfDestinationsType pCreateTdfDestinationsType, String pId) {
		if (isLoggingDebug()) {
			vlogDebug("START: TRURadialTaxwareUtils.getRequestMailingAddressById()");
		}
		List<MailingAddressType2> mailingAddress = pCreateTdfDestinationsType.getMailingAddress();
		for (MailingAddressType2 mailingAddressType2 : mailingAddress) {
			if(mailingAddressType2.getId().equalsIgnoreCase(pId)){
				return mailingAddressType2;
			}
		}
		return null;
	}
	
	/** The Order repository. */
	private Repository mOrderRepository;
	  
	/**
	 * @return the orderRepository
	 */
	public Repository getOrderRepository() {
		return mOrderRepository;
	}

	/**
	 * @param pOrderRepository the orderRepository to set
	 */
	public void setOrderRepository(Repository pOrderRepository) {
		mOrderRepository = pOrderRepository;
	}
	  
	/**
	 * This method is to prepare the map of shipping charge share for each item
	 *
	 * @param pOrder
	 *            - Order
	 * @return shippingChargeShareMap - shipping charge share map
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Double> prepareShippingChargeMap(TRUOrderImpl pOrder){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialTaxwareUtils method: prepareShippingChargeMap() ");
		}
		List<TRUCommerceItemMetaInfo> metaInfoList = null;
		double shipGrpCITotal = TRUConstants.DOUBLE_ZERO;
		double unitPrice = TRUConstants.DOUBLE_ZERO;
		long metaInfoQty = TRUConstants.LONG_ZERO;
		Map<String, Double> shippingChargeShareMap = new HashMap<String, Double>();
		TRUCommercePropertyManager commercePropertyManager = getPropertyManager();
		TRUPricingTools pricingTools = getPricingTools();
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		for (ShippingGroup shippingGroup : shippingGroups) {
			shipGrpCITotal = TRUConstants.DOUBLE_ZERO;
			if(shippingGroup instanceof TRUHardgoodShippingGroup){
				metaInfoList = new ArrayList<TRUCommerceItemMetaInfo>();
				TRUHardgoodShippingGroup hardgoodSG = (TRUHardgoodShippingGroup) shippingGroup;
				if(!hardgoodSG.isMarkerShippingGroup()) {
					continue;
				}
				ShippingPriceInfo priceInfo = hardgoodSG.getPriceInfo();
				double amount = priceInfo.getRawShipping();
				List<TRUShippingGroupCommerceItemRelationship> shipItemRels = new ArrayList<TRUShippingGroupCommerceItemRelationship>();
				List<String> dependencyShippingGruoupIds = hardgoodSG.getDependencyShippingGruoupIds();
				for (String shipID : dependencyShippingGruoupIds) {
					ShippingGroup shippingGroup2;
						try {
							shippingGroup2 = pOrder.getShippingGroup(shipID);
							shipItemRels.addAll(shippingGroup2.getCommerceItemRelationships());
						} catch (ShippingGroupNotFoundException se) {
							vlogError("ShippingGroupNotFoundException in TRURadialTaxwareUtils.prepareShippingChargeMap() {0}", se);
						} catch (InvalidParameterException ie) {
							vlogError("InvalidParameterException in TRURadialTaxwareUtils.prepareShippingChargeMap() {0}", ie);
						}
				}
				for (TRUShippingGroupCommerceItemRelationship shipItemRel : shipItemRels) {
					if(!(shipItemRel.getCommerceItem() instanceof TRUDonationCommerceItem|| shipItemRel.getCommerceItem() instanceof TRUGiftWrapCommerceItem)){
					List<TRUCommerceItemMetaInfo> metaInfos = shipItemRel.getMetaInfo();
					if(metaInfos != null && !metaInfos.isEmpty()){
						metaInfoList.addAll(metaInfos);
					}
					shipGrpCITotal += shipItemRel.getQuantity()*shipItemRel.getCommerceItem().getPriceInfo().getSalePrice();
					}
				}
				if(metaInfoList != null && !metaInfoList.isEmpty()){
					double totalShippingCharge = TRUConstants.DOUBLE_ZERO;
					double shippingChargeShare = TRUConstants.DOUBLE_ZERO;
					for (TRUCommerceItemMetaInfo metaInfo : metaInfoList) {
						unitPrice = (double) metaInfo.getPropertyValue(commercePropertyManager.getUnitPricePropertyName());
						metaInfoQty = (long) metaInfo.getPropertyValue(commercePropertyManager.getQuantityPropertyName());
						if(metaInfoList.indexOf(metaInfo)+TRUConstants.INTEGER_NUMBER_ONE == metaInfoList.size()){
							shippingChargeShare = amount - totalShippingCharge;
						}else{
							shippingChargeShare = amount*unitPrice*metaInfoQty/shipGrpCITotal;
						}
						shippingChargeShareMap.put(metaInfo.getId(), pricingTools.round(shippingChargeShare));
						totalShippingCharge += pricingTools.round(shippingChargeShare);
					}
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialTaxwareUtils method: prepareShippingChargeMap()");
		}
		return shippingChargeShareMap;
	}
	
	/**
	 * This method is to prepare the map of shipping discount share for each
	 * item.
	 *
	 * @param pOrderImpl
	 *            - Order
	 * @return shippingDiscountShareMap - Shipping discount share map for each
	 *         order item
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Double> prepareShippingDiscountShareMap(TRUOrderImpl pOrderImpl){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialTaxwareUtils method: prepareShippingDiscountShareMap()");
		}
		List<TRUCommerceItemMetaInfo> metaInfoList = null;
		double shipGrpCITotal = TRUConstants.DOUBLE_ZERO;
		double unitPrice = TRUConstants.DOUBLE_ZERO;
		long metaInfoQty = TRUConstants.LONG_ZERO;
		Map<String, Double> shippingDiscountShareMap = new HashMap<String, Double>();
		TRUPricingTools pricingTools = getPricingTools();
		if(pOrderImpl != null){
			TRUCommercePropertyManager commercePropertyManager = getPropertyManager();
			List<ShippingGroup> shippingGroups = pOrderImpl.getShippingGroups();
			for (ShippingGroup shippingGroup : shippingGroups) {
				shipGrpCITotal = TRUConstants.DOUBLE_ZERO;
				if(shippingGroup instanceof TRUHardgoodShippingGroup){
					TRUHardgoodShippingGroup hardgoodSG = (TRUHardgoodShippingGroup) shippingGroup;
					if(!hardgoodSG.isMarkerShippingGroup()) {
						continue;
					}
					metaInfoList = new ArrayList<TRUCommerceItemMetaInfo>();
					ShippingPriceInfo priceInfo = hardgoodSG.getPriceInfo();
					List<PricingAdjustment> adjustments = priceInfo.getAdjustments();
					if(adjustments != null && !adjustments.isEmpty()){
						List<String> dependencyShippingGruoupIds = hardgoodSG.getDependencyShippingGruoupIds();
						for (String shipGroupID : dependencyShippingGruoupIds) {
							try {
								ShippingGroup shippingGroup2 = pOrderImpl.getShippingGroup(shipGroupID);
								List<TRUShippingGroupCommerceItemRelationship> shipItemRels = shippingGroup2.getCommerceItemRelationships();
								for (TRUShippingGroupCommerceItemRelationship shipItemRel : shipItemRels) {
								if(!(shipItemRel.getCommerceItem() instanceof TRUDonationCommerceItem|| shipItemRel.getCommerceItem() instanceof TRUGiftWrapCommerceItem)){
									List<TRUCommerceItemMetaInfo> metaInfos = shipItemRel.getMetaInfo();
									metaInfoList.addAll(metaInfos);
									shipGrpCITotal += shipItemRel.getQuantity()*shipItemRel.getCommerceItem().getPriceInfo().getSalePrice();
									}
								}
							} catch (ShippingGroupNotFoundException se) {
								vlogError("ShippingGroupNotFoundException in TRURadialTaxwareUtils.prepareShippingDiscountShareMap() {0}", se);
							} catch (InvalidParameterException ie) {
								vlogError("InvalidParameterException in TRURadialTaxwareUtils.prepareShippingDiscountShareMap() {0}", ie);
							}
						}
						for (PricingAdjustment pricingAdjustment : adjustments) {
							if(pricingAdjustment.getPricingModel() != null){
								double discountAmount = -pricingAdjustment.getTotalAdjustment();
								String promoId = pricingAdjustment.getPricingModel().getRepositoryId();
								if(metaInfoList != null && !metaInfoList.isEmpty()){
									double totalShippingDiscShare = TRUConstants.DOUBLE_ZERO;
									double shippingDiscShare = TRUConstants.DOUBLE_ZERO;
									for (TRUCommerceItemMetaInfo metaInfo : metaInfoList) {
										unitPrice = (double) metaInfo.getPropertyValue(commercePropertyManager.getUnitPricePropertyName());
										metaInfoQty = (long) metaInfo.getPropertyValue(commercePropertyManager.getQuantityPropertyName());
										if(metaInfoList.indexOf(metaInfo)+TRUConstants.INTEGER_NUMBER_ONE == metaInfoList.size()){
											shippingDiscShare = discountAmount - totalShippingDiscShare;
										}else{
											shippingDiscShare = discountAmount*unitPrice*metaInfoQty/shipGrpCITotal;
										}
										shippingDiscountShareMap.put(metaInfo.getId()+TRUConstants.PIPE_STRING+promoId, pricingTools.round(shippingDiscShare));
										totalShippingDiscShare += pricingTools.round(shippingDiscShare);
									}
								}
							}
						}
					}
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialTaxwareUtils method: prepareShippingDiscountShareMap()");
		}
		return shippingDiscountShareMap;
	}
	
	/**
	 * This method is to prepare order discount share map for each metainfo form
	 * Prepare order discount share map.
	 *
	 * @param pOrder the order
	 * @return the map
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Double> prepareOrderDiscountShareMap(TRUOrderImpl pOrder){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialTaxwareUtils method: populateOrderDiscountShare()");
		}
		Map<String, Double> truOrderDiscountShare = null;
		TRUItemPriceInfo itemPriceInfo = null;
		List<TRUCommerceItemMetaInfo> metaInfoList = null;
		double commerceItemTotal = TRUConstants.DOUBLE_ZERO;
		double unitPrice = TRUConstants.DOUBLE_ZERO;
		long metaInfoQty = TRUConstants.LONG_ZERO;
		Map<String, Double> orderDiscountShareMap = new HashMap<String, Double>();
		TRUCommercePropertyManager commercePropertyManager = getPropertyManager();
		TRUPricingTools pricingTools = getPricingTools();
		if(pOrder != null){
		List<CommerceItem> commerceItems = pOrder.getCommerceItems();
		for (CommerceItem commerceItem : commerceItems) {
			metaInfoList = new ArrayList<TRUCommerceItemMetaInfo>();
			itemPriceInfo = (TRUItemPriceInfo) commerceItem.getPriceInfo();
			commerceItemTotal = itemPriceInfo.getRawTotalPrice();
			truOrderDiscountShare = itemPriceInfo.getTruOrderDiscountShare();
			List<TRUShippingGroupCommerceItemRelationship> shipItemRels = commerceItem.getShippingGroupRelationships();
			for (TRUShippingGroupCommerceItemRelationship shipItemRel : shipItemRels) {
				List<TRUCommerceItemMetaInfo> metaInfos = shipItemRel.getMetaInfo();
				metaInfoList.addAll(metaInfos);
			}
			if(metaInfoList != null && !metaInfoList.isEmpty() && truOrderDiscountShare != null && !truOrderDiscountShare.isEmpty()){
				Iterator<String> truOrderDiscountShareIterator = truOrderDiscountShare.keySet().iterator();
				while(truOrderDiscountShareIterator.hasNext()){
					String key = (String) truOrderDiscountShareIterator.next();
					double value = truOrderDiscountShare.get(key);
					double totalDiscountShare = TRUConstants.DOUBLE_ZERO;
					double discShare = TRUConstants.DOUBLE_ZERO;
					for (TRUCommerceItemMetaInfo metaInfo : metaInfoList) {
						unitPrice = (double) metaInfo.getPropertyValue(commercePropertyManager.getUnitPricePropertyName());
						metaInfoQty = (long) metaInfo.getPropertyValue(commercePropertyManager.getQuantityPropertyName());
						if(metaInfoList.indexOf(metaInfo)+TRUConstants.INTEGER_NUMBER_ONE == metaInfoList.size()){
							discShare = value - totalDiscountShare;
						}else{
							discShare = value*unitPrice*metaInfoQty/commerceItemTotal;
						}
						orderDiscountShareMap.put(metaInfo.getId()+TRUConstants.PIPE_STRING+key, pricingTools.round(discShare));
						totalDiscountShare += discShare;
					}
				}
			}
		}
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialTaxwareUtils method: populateOrderDiscountShare()");
		}
		return orderDiscountShareMap;
	}
	
	/**
	 * This method is to populate the discount details from Item discount share
	 * map.
	 *
	 * @param pObjectFactory            - ObjectFactory
	 * @param pMetaInfo            - TRUCommerceItemMetaInfo
	 * @param pCommItem            - CommerceItem
	 * @param pDiscounts            - PromotionalDiscounts
	 */
	@SuppressWarnings("unchecked")
	public void getItemDiscountShare(ObjectFactory pObjectFactory, TRUCommerceItemMetaInfo pMetaInfo, CommerceItem pCommItem,
			PromotionalDiscounts2 pDiscounts){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialTaxwareUtils method: getOrderItemDiscountShare()");
		}
		if(pMetaInfo == null) {
			return;
		}
		TRUItemPriceInfo priceInfo = (TRUItemPriceInfo) pCommItem.getPriceInfo();
		Map<String, String> truItemDiscountShare = priceInfo.getTruItemDiscountShare();
		String metaInfoPromoId = (String) pMetaInfo.getPropertyValue(getPropertyManager().getPromotionIdPropertyName());
		boolean isAdjusted = (boolean) pMetaInfo.getPropertyValue(getPropertyManager().getIsAdjustedPropertyName());
		long qty = (long) pMetaInfo.getPropertyValue(getPropertyManager().getQuantityPropertyName());
		double unitPrice = (double) pMetaInfo.getPropertyValue(getPropertyManager().getUnitPricePropertyName());
		double price = (double) pMetaInfo.getPropertyValue(getPropertyManager().getPricePropertyName());
		if(truItemDiscountShare != null && !truItemDiscountShare.isEmpty()){
			Iterator<String> itemDiscountShareIterator = truItemDiscountShare.keySet().iterator();
			while(itemDiscountShareIterator.hasNext()){
				String key = (String) itemDiscountShareIterator.next();
				String value = truItemDiscountShare.get(key);
				String[] keySplit = key.split(PIPE_DELIMETRE);
				String promoId = keySplit[TRUConstants.INT_ZERO];
				String qualOrTar = keySplit[TRUConstants.INTEGER_NUMBER_TWO];
				if(!StringUtils.isBlank(key)){
					if(!StringUtils.isBlank(metaInfoPromoId) && !StringUtils.isBlank(promoId) && promoId.equals(metaInfoPromoId) && isAdjusted && qualOrTar.equals(PROMOTION_TARGETER_ITEM_LABEL)){
						if(!StringUtils.isBlank(value)){
							String[] valueSplit = value.split(PIPE_DELIMETRE);
							if(!StringUtils.isBlank(valueSplit[0])){
								double discShare = Double.parseDouble(valueSplit[0]);
								if(discShare > unitPrice && price == TRUConstants.DOUBLE_ZERO){
									discShare =  unitPrice;
								} else if(discShare > unitPrice && unitPrice*qty - price > TRUConstants.DOUBLE_ZERO){
									discShare = unitPrice*qty - price;
								}
								Discount2 discount2 = pObjectFactory.createDiscount2();
								if(promoId.length() > TRUConstants.TWELVE) {
									discount2.setId(promoId.substring(promoId.length()-TRUConstants.TWELVE));
								} else {
									discount2.setId(promoId);
								}
								discount2.setAmount(BigDecimal.valueOf(getPricingTools().round(discShare)));
								pDiscounts.getDiscount().add(discount2);
							}
						}
					}else if(!StringUtils.isBlank(promoId) && StringUtils.isBlank(metaInfoPromoId) && qualOrTar.equals(PROMOTION_QUALIFIER_ITEM_LABEL) && !isAdjusted && !StringUtils.isBlank(value)){
							String[] valueSplit = value.split(PIPE_DELIMETRE);
							double discShare = TRUConstants.DOUBLE_ZERO;
							if(!StringUtils.isBlank(valueSplit[0])){
								discShare = Double.parseDouble(valueSplit[0]);
								if(discShare > unitPrice && unitPrice*qty == price){
									discShare = TRUConstants.DOUBLE_ZERO;
								}
								//Populate the promotion details
								if(discShare > TRUConstants.DOUBLE_ZERO){
									Discount2 discount2 = pObjectFactory.createDiscount2();
									if(promoId.length() > TRUConstants.TWELVE) {
										discount2.setId(promoId.substring(promoId.length()-TRUConstants.TWELVE));
									} else {
										discount2.setId(promoId);
									}
									discount2.setAmount(BigDecimal.valueOf(getPricingTools().round(discShare)));
									pDiscounts.getDiscount().add(discount2);
							}
						}
					}
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialTaxwareUtils method: getOrderItemDiscountShare()");
		}
	}
	
}
