package com.tru.commerce.pricing;

import java.util.Comparator;

import com.tru.common.TRUConstants;

/**
 * This class is to compare the shipping methods.
 * @author Professional Access
 * @version 1.0
 */
public class TRUShipMethodComparator implements Comparator<TRUShipMethodVO> {

	/**
	 * This method compares the shipping method price.
	 * @param pO1 - TRUShipMethodVO.
	 * @param pO2 - TRUShipMethodVO.
	 * @return l_int - Returns integer value.
	 */
	@Override
	public int compare(TRUShipMethodVO pO1, TRUShipMethodVO pO2) {
		int compareVal = TRUConstants.INT_MINUS_ONE;
		if (pO1.getShippingPrice() == pO2.getShippingPrice()) {
			compareVal = TRUConstants.INTEGER_NUMBER_ZERO;
		} else if (pO1.getShippingPrice() > pO2.getShippingPrice()) {
			compareVal = TRUConstants.INTEGER_NUMBER_ONE;
		}
		return compareVal;
	}

}
