/**
 * TRUPriceProperties.java
 */
package com.tru.commerce.pricing;

import atg.nucleus.GenericService;

/**
 * This class is to hold the Price properties.
 * 
 * @author Professional Access.
 * @version 1.0 
 */
public class TRUPriceProperties extends GenericService {

	/** This property holds country code property name.*/
	private String mCountryCodePropertyName;

	/** This property holds market code property name.*/
	private String mMarketCodePropertyName;

	/** This property holds store number property name.*/
	private String mStoreNumberPropertyName;

	/** This property holds our price property name. */
	private String mSalePricePropertyName;

	/** This property holds SKU Id property name. */
	private String mSKUIdPropertyName;

	/** This property holds List price property name. */
	private String mListPricePropertyName;

	/** This property holds sale price property name. */
	private String mLocalePropetryName;

	/** This property holds sale price property name. */
	private String mPriceIdPropertyName;

	/** This property holds sale price property name. */
	private String mPriceListIdPropertyName;

	/** This property holds sale price property name. */
	private String mDisplayNamePropertyName;

	/** This property holds Threshold Price Percentage property name. */
	private String mThresholdPricePercentagePropertyName;

	/** This property holds Threshold Price Amount property name. */
	private String mThresholdPriceAmountPropertyName;

	/** This property holds Intrday price item descriptor name. */
	private String mIntraDayPriceItemDescriptorName;

	/** This property holds Intrday price start date property name. */
	private String mIntradayPriceStartDatePropertyName;

	/** This property holds Intrday end date property name. */
	private String mIntradayPriceEndDatePropertyName;

	/** This property holds Intrday price start time property name. */
	private String mIntradayPriceStartTimePropertyName;

	/** This property holds Intrday price end time property name. */
	private String mIntradayPriceEndTimePropertyName;

	/** This property holds Intrday price date format. */
	private String mIntradayPriceDateFormat;

	/** This property holds Intrday price time format. */
	private String mIntradayPriceTimeFormat;

	/** This property holds product id property name. */
	private String mProductIdPropertyName;

	/** This property holds order property name. */
	private String mOrderPropertyName;
	
	/** This property holds tenderType property name. */
	private String mTenderTypePropertyName;
	
	/** This property holds total discount amount till date property name. */
	private String mTotalDiscountAmountTillDatePropertyName;
	
	/** This property holds capital amount property name. */
	private String mCapitalAmountPropertyName;
	
	/** This property holds tender Type promotion property name. */
	private String mTenderTypePromotionItemType;
	
	/** This property holds promotion id property name. */
	private String mPromotionIdPropertyName;
	
	/** This property holds tenderType property name. */
	private String mPromotionTypePropertyName;
	
	/** This property holds tender amount limit property name. */
	private String mTenderAmountLimitPropertyName;
	
	/** This property mProductionURLPropertyName.*/
	private String mProductionURLPropertyName;
	
	/** This property mBasePriceList.*/
	private String mBasePriceList;
	
	/** This property holds mDealIDPropertyName */
	private String mDealIDPropertyName;
	
	/** This property holds mDescription */
	private String mDescription;
	
	/** This property holds mPromotionDescription */
	private String mPromotionDescription;
	
	/** This property holds mTenderTypePromotionProperty */
	private String mTenderTypePromotionProperty;
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return mDescription;
	}
	/**
	 * @param pDescription the description to set
	 */
	public void setDescription(String pDescription) {
		mDescription = pDescription;
	}
	/**
	 * @return the promotionDescription
	 */
	public String getPromotionDescription() {
		return mPromotionDescription;
	}
	/**
	 * @param pPromotionDescription the promotionDescription to set
	 */
	public void setPromotionDescription(String pPromotionDescription) {
		mPromotionDescription = pPromotionDescription;
	}
	/**
	 * This method will return countryCodePropertyName.
	 *
	 * @return the countryCodePropertyName.
	 */
	public String getCountryCodePropertyName() {
		return mCountryCodePropertyName;
	}
	/**
	 * This method will set mcountryCodePropertyName with pCountryCodePropertyName.
	 *
	 * @param pCountryCodePropertyName the mcountryCodePropertyName to set.
	 */
	public void setCountryCodePropertyName(String pCountryCodePropertyName) {
		mCountryCodePropertyName = pCountryCodePropertyName;
	}
	/**
	 * This method will return marketCodePropertyName.
	 *
	 * @return the marketCodePropertyName.
	 */
	public String getMarketCodePropertyName() {
		return mMarketCodePropertyName;
	}
	/**
	 * This method will set mmarketCodePropertyName with pMarketCodePropertyName.
	 *
	 * @param pMarketCodePropertyName the mmarketCodePropertyName to set.
	 */
	public void setMarketCodePropertyName(String pMarketCodePropertyName) {
		mMarketCodePropertyName = pMarketCodePropertyName;
	}
	/**
	 * This method will return storeNumberPropertyName.
	 *
	 * @return the storeNumberPropertyName.
	 */
	public String getStoreNumberPropertyName() {
		return mStoreNumberPropertyName;
	}
	/**
	 * This method will set mstoreNumberPropertyName with pStoreNumberPropertyName.
	 *
	 * @param pStoreNumberPropertyName the mstoreNumberPropertyName to set.
	 */
	public void setStoreNumberPropertyName(String pStoreNumberPropertyName) {
		mStoreNumberPropertyName = pStoreNumberPropertyName;
	}

	/**
	 * Returns the listPricePropertyName.
	 * 
	 * @return the listPricePropertyName.
	 */
	public String getListPricePropertyName() {
		return mListPricePropertyName;
	}

	/**
	 * Sets/updates the listPricePropertyName.
	 * 
	 * @param pListPricePropertyName
	 *            the listPricePropertyName to set.
	 */
	public void setListPricePropertyName(String pListPricePropertyName) {
		mListPricePropertyName = pListPricePropertyName;
	}

	/**
	 * Returns the localePropetryName.
	 * 
	 * @return the localePropetryName.
	 */
	public String getLocalePropetryName() {
		return mLocalePropetryName;
	}

	/**
	 * Sets/updates the localePropetryName.
	 * 
	 * @param pLocalePropetryName
	 *            the localePropetryName to set.
	 */
	public void setLocalePropetryName(String pLocalePropetryName) {
		mLocalePropetryName = pLocalePropetryName;
	}

	/**
	 * Returns the priceIdPropertyName.
	 * 
	 * @return the priceIdPropertyName.
	 */
	public String getPriceIdPropertyName() {
		return mPriceIdPropertyName;
	}

	/**
	 * Sets/updates the priceIdPropertyName.
	 * 
	 * @param pPriceIdPropertyName
	 *            the priceIdPropertyName to set.
	 */
	public void setPriceIdPropertyName(String pPriceIdPropertyName) {
		mPriceIdPropertyName = pPriceIdPropertyName;
	}

	/**
	 * Returns the priceListIdPropertyName.
	 * 
	 * @return the priceListIdPropertyName.
	 */
	public String getPriceListIdPropertyName() {
		return mPriceListIdPropertyName;
	}

	/**
	 * Sets/updates the priceListIdPropertyName.
	 * 
	 * @param pPriceListIdPropertyName
	 *            the priceListIdPropertyName to set.
	 */
	public void setPriceListIdPropertyName(String pPriceListIdPropertyName) {
		mPriceListIdPropertyName = pPriceListIdPropertyName;
	}

	/**
	 * Returns the displayNamePropertyName.
	 * 
	 * @return the displayNamePropertyName.
	 */
	public String getDisplayNamePropertyName() {
		return mDisplayNamePropertyName;
	}

	/**
	 * Sets/updates the displayNamePropertyName.
	 * 
	 * @param pDisplayNamePropertyName
	 *            the displayNamePropertyName to set.
	 */
	public void setDisplayNamePropertyName(String pDisplayNamePropertyName) {
		mDisplayNamePropertyName = pDisplayNamePropertyName;
	}

	/**
	 * Returns the sKUIdPropertyName.
	 * 
	 * @return the sKUIdPropertyName.
	 */
	public String getSKUIdPropertyName() {
		return mSKUIdPropertyName;
	}

	/**
	 * Sets/updates the sKUIdPropertyName.
	 * 
	 * @param pSKUIdPropertyName
	 *            the sKUIdPropertyName to set.
	 */
	public void setSKUIdPropertyName(String pSKUIdPropertyName) {
		mSKUIdPropertyName = pSKUIdPropertyName;
	}

	/**
	 * Returns the thresholdPricePercentagePropertyName.
	 * 
	 * @return the thresholdPricePercentagePropertyName.
	 */
	public String getThresholdPricePercentagePropertyName() {
		return mThresholdPricePercentagePropertyName;
	}

	/**
	 * Sets/updates the thresholdPricePercentagePropertyName.
	 * 
	 * @param pThresholdPricePercentagePropertyName
	 *            the thresholdPricePercentagePropertyName to set.
	 */
	public void setThresholdPricePercentagePropertyName(String pThresholdPricePercentagePropertyName) {
		mThresholdPricePercentagePropertyName = pThresholdPricePercentagePropertyName;
	}

	/**
	 * Returns the thresholdPriceAmountPropertyName.
	 * 
	 * @return the thresholdPriceAmountPropertyName.
	 */
	public String getThresholdPriceAmountPropertyName() {
		return mThresholdPriceAmountPropertyName;
	}

	/**
	 * Sets/updates the thresholdPriceAmountPropertyName.
	 * 
	 * @param pThresholdPriceAmountPropertyName
	 *            the thresholdPriceAmountPropertyName to set.
	 */
	public void setThresholdPriceAmountPropertyName(String pThresholdPriceAmountPropertyName) {
		mThresholdPriceAmountPropertyName = pThresholdPriceAmountPropertyName;
	}

	/**
	 * Returns the intraDayPriceItemDescriptorName.
	 * 
	 * @return the intraDayPriceItemDescriptorName.
	 */
	public String getIntraDayPriceItemDescriptorName() {
		return mIntraDayPriceItemDescriptorName;
	}

	/**
	 * Sets/updates the intraDayPriceItemDescriptorName.
	 * 
	 * @param pIntraDayPriceItemDescriptorName
	 *            the intraDayPriceItemDescriptorName to set.
	 */
	public void setIntraDayPriceItemDescriptorName(String pIntraDayPriceItemDescriptorName) {
		mIntraDayPriceItemDescriptorName = pIntraDayPriceItemDescriptorName;
	}

	/**
	 * Returns the intradayPriceStartDatePropertyName.
	 * 
	 * @return the intradayPriceStartDatePropertyName.
	 */
	public String getIntradayPriceStartDatePropertyName() {
		return mIntradayPriceStartDatePropertyName;
	}

	/**
	 * Sets/updates the intradayPriceStartDatePropertyName.
	 * 
	 * @param pIntradayPriceStartDatePropertyName
	 *            the intradayPriceStartDatePropertyName to set.
	 */
	public void setIntradayPriceStartDatePropertyName(String pIntradayPriceStartDatePropertyName) {
		mIntradayPriceStartDatePropertyName = pIntradayPriceStartDatePropertyName;
	}

	/**
	 * Returns the intradayPriceEndDatePropertyName.
	 * 
	 * @return the intradayPriceEndDatePropertyName.
	 */
	public String getIntradayPriceEndDatePropertyName() {
		return mIntradayPriceEndDatePropertyName;
	}

	/**
	 * Sets/updates the intradayPriceEndDatePropertyName.
	 * 
	 * @param pIntradayPriceEndDatePropertyName
	 *            the intradayPriceEndDatePropertyName to set.
	 */
	public void setIntradayPriceEndDatePropertyName(String pIntradayPriceEndDatePropertyName) {
		mIntradayPriceEndDatePropertyName = pIntradayPriceEndDatePropertyName;
	}

	/**
	 * Returns the intradayPriceStartTimePropertyName.
	 * 
	 * @return the intradayPriceStartTimePropertyName.
	 */
	public String getIntradayPriceStartTimePropertyName() {
		return mIntradayPriceStartTimePropertyName;
	}

	/**
	 * Sets/updates the intradayPriceStartTimePropertyName.
	 * 
	 * @param pIntradayPriceStartTimePropertyName
	 *            the intradayPriceStartTimePropertyName to set.
	 */
	public void setIntradayPriceStartTimePropertyName(String pIntradayPriceStartTimePropertyName) {
		mIntradayPriceStartTimePropertyName = pIntradayPriceStartTimePropertyName;
	}

	/**
	 * Returns the intradayPriceEndTimePropertyName.
	 * 
	 * @return the intradayPriceEndTimePropertyName.
	 */
	public String getIntradayPriceEndTimePropertyName() {
		return mIntradayPriceEndTimePropertyName;
	}

	/**
	 * Sets/updates the intradayPriceEndTimePropertyName.
	 * 
	 * @param pIntradayPriceEndTimePropertyName
	 *            the intradayPriceEndTimePropertyName to set.
	 */
	public void setIntradayPriceEndTimePropertyName(String pIntradayPriceEndTimePropertyName) {
		mIntradayPriceEndTimePropertyName = pIntradayPriceEndTimePropertyName;
	}

	/**
	 * Returns the intradayPriceDateFormat.
	 * 
	 * @return the intradayPriceDateFormat.
	 */
	public String getIntradayPriceDateFormat() {
		return mIntradayPriceDateFormat;
	}

	/**
	 * Sets/updates the intradayPriceDateFormat.
	 * 
	 * @param pIntradayPriceDateFormat
	 *            the intradayPriceDateFormat to set.
	 */
	public void setIntradayPriceDateFormat(String pIntradayPriceDateFormat) {
		mIntradayPriceDateFormat = pIntradayPriceDateFormat;
	}

	/**
	 * Returns the intradayPriceTimeFormat.
	 * 
	 * @return the intradayPriceTimeFormat.
	 */
	public String getIntradayPriceTimeFormat() {
		return mIntradayPriceTimeFormat;
	}

	/**
	 * Sets/updates the intradayPriceTimeFormat.
	 * 
	 * @param pIntradayPriceTimeFormat
	 *            the intradayPriceTimeFormat to set.
	 */
	public void setIntradayPriceTimeFormat(String pIntradayPriceTimeFormat) {
		mIntradayPriceTimeFormat = pIntradayPriceTimeFormat;
	}

	/**
	 * Returns the productIdPropertyName.
	 * 
	 * @return the productIdPropertyName.
	 */
	public String getProductIdPropertyName() {
		return mProductIdPropertyName;
	}

	/**
	 * Sets/updates the productIdPropertyName.
	 * 
	 * @param pProductIdPropertyName
	 *            the productIdPropertyName to set.
	 */
	public void setProductIdPropertyName(String pProductIdPropertyName) {
		mProductIdPropertyName = pProductIdPropertyName;
	}

	/**
	 * Returns the orderPropertyName.
	 * 
	 * @return the orderPropertyName.
	 */
	public String getOrderPropertyName() {
		return mOrderPropertyName;
	}

	/**
	 * Sets/updates the orderPropertyName.
	 * 
	 * @param pOrderPropertyName
	 *            the orderPropertyName to set.
	 */
	public void setOrderPropertyName(String pOrderPropertyName) {
		mOrderPropertyName = pOrderPropertyName;
	}

	/**
	 * Returns the salePricePropertyName.
	 * 
	 * @return the salePricePropertyName.
	 */
	public String getSalePricePropertyName() {
		return mSalePricePropertyName;
	}

	/**
	 * Sets/updates the salePricePropertyName.
	 * 
	 * @param pSalePricePropertyName
	 *            the salePricePropertyName to set.
	 */
	public void setSalePricePropertyName(String pSalePricePropertyName) {
		mSalePricePropertyName = pSalePricePropertyName;
	}
	/**
	 * @return the tenderTypePropertyName.
	 */
	public String getTenderTypePropertyName() {
		return mTenderTypePropertyName;
	}
	/**
	 * @param pTenderTypePropertyName the tenderTypePropertyName to set.
	 */
	public void setTenderTypePropertyName(String pTenderTypePropertyName) {
		mTenderTypePropertyName = pTenderTypePropertyName;
	}
	/**
	 * @return the totalDiscountAmountTillDatePropertyName.
	 */
	public String getTotalDiscountAmountTillDatePropertyName() {
		return mTotalDiscountAmountTillDatePropertyName;
	}
	/**
	 * @param pTotalDiscountAmountTillDatePropertyName the totalDiscountAmountTillDatePropertyName to set.
	 */
	public void setTotalDiscountAmountTillDatePropertyName(String pTotalDiscountAmountTillDatePropertyName) {
		mTotalDiscountAmountTillDatePropertyName = pTotalDiscountAmountTillDatePropertyName;
	}
	/**
	 * @return the capitalAmountPropertyName.
	 */
	public String getCapitalAmountPropertyName() {
		return mCapitalAmountPropertyName;
	}
	/**
	 * @param pCapitalAmountPropertyName the capitalAmountPropertyName to set.
	 */
	public void setCapitalAmountPropertyName(String pCapitalAmountPropertyName) {
		mCapitalAmountPropertyName = pCapitalAmountPropertyName;
	}
	/**
	 * @return the tenderTypePromotionItemType.
	 */
	public String getTenderTypePromotionItemType() {
		return mTenderTypePromotionItemType;
	}
	/**
	 * @param pTenderTypePromotionItemType the tenderTypePromotionItemType to set.
	 */
	public void setTenderTypePromotionItemType(String pTenderTypePromotionItemType) {
		mTenderTypePromotionItemType = pTenderTypePromotionItemType;
	}
	/**
	 * @return the promotionIdPropertyName.
	 */
	public String getPromotionIdPropertyName() {
		return mPromotionIdPropertyName;
	}
	/**
	 * @param pPromotionIdPropertyName the promotionIdPropertyName to set.
	 */
	public void setPromotionIdPropertyName(String pPromotionIdPropertyName) {
		mPromotionIdPropertyName = pPromotionIdPropertyName;
	}
	/**
	 * @return the promotionTypePropertyName.
	 */
	public String getPromotionTypePropertyName() {
		return mPromotionTypePropertyName;
	}
	/**
	 * @param pPromotionTypePropertyName the promotionTypePropertyName to set.
	 */
	public void setPromotionTypePropertyName(String pPromotionTypePropertyName) {
		mPromotionTypePropertyName = pPromotionTypePropertyName;
	}
	/**
	 * @return the tenderAmountLimitPropertyName.
	 */
	public String getTenderAmountLimitPropertyName() {
		return mTenderAmountLimitPropertyName;
	}
	/**
	 * @param pTenderAmountLimitPropertyName the tenderAmountLimitPropertyName to set.
	 */
	public void setTenderAmountLimitPropertyName(String pTenderAmountLimitPropertyName) {
		mTenderAmountLimitPropertyName = pTenderAmountLimitPropertyName;
	}
	/**
	 * @return the productionURLPropertyName.
	 */
	public String getProductionURLPropertyName() {
		return mProductionURLPropertyName;
	}
	/**
	 * @param pProductionURLPropertyName the productionURLPropertyName to set.
	 */
	public void setProductionURLPropertyName(String pProductionURLPropertyName) {
		mProductionURLPropertyName = pProductionURLPropertyName;
	}
	/**
	 * @return the mBasePriceList
	 */
	public String getBasePriceList() {
		return mBasePriceList;
	}
	/**
	 * @param pBasePriceList the mBasePriceList to set
	 */
	public void setBasePriceList(String pBasePriceList) {
		this.mBasePriceList = pBasePriceList;
	}
	/**
	 * This method will return dealIDPropertyName.
	 * 
	 * @return the dealIDPropertyName.
	 */
	public String getDealIDPropertyName() {
		return mDealIDPropertyName;
	}
	
	/**
	 * This method will set mDealIDPropertyName with pDealIDPropertyName.
	 * 
	 * @param pDealIDPropertyName the mDealIDPropertyName to set.
	 */
	public void setDealIDPropertyName(String pDealIDPropertyName) {
		this.mDealIDPropertyName = pDealIDPropertyName;
	}
	/**
	 * @return the tenderTypePromotionProperty
	 */
	public String getTenderTypePromotionProperty() {
		return mTenderTypePromotionProperty;
	}
	/**
	 * @param pTenderTypePromotionProperty the tenderTypePromotionProperty to set
	 */
	public void setTenderTypePromotionProperty(String pTenderTypePromotionProperty) {
		mTenderTypePromotionProperty = pTenderTypePromotionProperty;
	}

}