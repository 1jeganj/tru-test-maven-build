/*
 * @(#)TRUTaxPriceInfo.java	1.0 Oct 14, 2015
 *
 */
package com.tru.commerce.pricing;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import atg.commerce.pricing.TaxPriceInfo;


/**
 * This price info object holds the custom tax price info properties.
 * @author Professional Access
 * @version 1.0
 */
public class TRUTaxPriceInfo extends TaxPriceInfo {

	/**
	 * Holds the default serial version id.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Hold the item type.
	 */
	private String mItemType;

	/**
	 * Hold the Item or tax prod id.
	 */
	private String mItemOrTaxProdId;

	/**
	 * Hold the Item or State Tax Amount.
	 */
	private double mStateTaxAmount;

	/**
	 * Hold the Item or County Tax Amount.
	 */
	private double mCountyTaxAmount;

	/**
	 * Hold the Item or City Tax Amount.
	 */
	private double mCityTaxAmount;

	/**
	 * Hold the Item or Secondary State Tax Amount.
	 */
	private double mSecondaryStateTaxAmount;

	/**
	 * Hold the Item or Secondary County Tax Amount.
	 */
	private double mSecondaryCountyTaxAmount;

	/**
	 * Hold the Item or Secondary City Tax Amount.
	 */
	private double mSecondaryCityTaxAmount;
	
	/**
	 * Property to hold shipItemRelationsTaxPriceInfos.
	 */
	private Map mShipItemRelationsTaxPriceInfos;
	/**
	 * property to hold taxCity.
	 */
	private String mTaxCity;
	/**
	 * property to hold taxState.
	 */
	private String mTaxState;
	/**
	 * property to hold taxCounty.
	 */
	private String mTaxCounty;
	/**
	 * property to hold taxSecondoryState.
	 */
	private String mTaxSecondoryState;
	/**
	 * property to hold taxSecondoryCity.
	 */
	private String mTaxSecondoryCity;
	/**
	 * property to hold taxSecondoryCounty.
	 */
	private String mTaxSecondoryCounty;
	/**
	 * Hold the Item or County Tax Amount.
	 */
	private double mTaxRate;
	/**
	 * Hold the Item or taxRequestTime.
	 */
	private Date mTaxRequestTimestamp;
	/**
	 * Hold the Item or lineItemType.
	 */
	private String mLineItemType;
	
	/**
	 * Returns the itemType.
	 * 
	 * @return the itemType
	 */
	public String getItemType() {
		return mItemType;
	}

	/**
	 * Sets/updates the itemType.
	 * 
	 * @param pItemType
	 *            the itemType to set
	 */
	public void setItemType(String pItemType) {
		mItemType = pItemType;
	}

	/**
	 * Returns the itemOrTaxProdId.
	 * 
	 * @return the itemOrTaxProdId
	 */
	public String getItemOrTaxProdId() {
		return mItemOrTaxProdId;
	}

	/**
	 * Sets/updates the itemOrTaxProdId.
	 * 
	 * @param pItemOrTaxProdId
	 *            the itemOrTaxProdId to set
	 */
	public void setItemOrTaxProdId(String pItemOrTaxProdId) {
		mItemOrTaxProdId = pItemOrTaxProdId;
	}

	/**
	 * Returns the stateTaxAmount.
	 * 
	 * @return the stateTaxAmount
	 */
	public double getStateTaxAmount() {
		return mStateTaxAmount;
	}

	/**
	 * Sets/updates the stateTaxAmount.
	 * 
	 * @param pStateTaxAmount
	 *            the stateTaxAmount to set
	 */
	public void setStateTaxAmount(double pStateTaxAmount) {
		mStateTaxAmount = pStateTaxAmount;
	}

	/**
	 * Returns the countyTaxAmount.
	 * 
	 * @return the countyTaxAmount
	 */
	public double getCountyTaxAmount() {
		return mCountyTaxAmount;
	}

	/**
	 * Sets/updates the countyTaxAmount.
	 * 
	 * @param pCountyTaxAmount
	 *            the countyTaxAmount to set
	 */
	public void setCountyTaxAmount(double pCountyTaxAmount) {
		mCountyTaxAmount = pCountyTaxAmount;
	}

	/**
	 * Returns the cityTaxAmount.
	 * 
	 * @return the cityTaxAmount
	 */
	public double getCityTaxAmount() {
		return mCityTaxAmount;
	}

	/**
	 * Sets/updates the cityTaxAmount.
	 * 
	 * @param pCityTaxAmount
	 *            the cityTaxAmount to set
	 */
	public void setCityTaxAmount(double pCityTaxAmount) {
		mCityTaxAmount = pCityTaxAmount;
	}

	/**
	 * Returns the secondaryStateTaxAmount.
	 * 
	 * @return the secondaryStateTaxAmount
	 */
	public double getSecondaryStateTaxAmount() {
		return mSecondaryStateTaxAmount;
	}

	/**
	 * Sets/updates the secondaryStateTaxAmount.
	 * 
	 * @param pSecondaryStateTaxAmount
	 *            the secondaryStateTaxAmount to set
	 */
	public void setSecondaryStateTaxAmount(double pSecondaryStateTaxAmount) {
		mSecondaryStateTaxAmount = pSecondaryStateTaxAmount;
	}

	/**
	 * Returns the secondaryCountyTaxAmount.
	 * 
	 * @return the secondaryCountyTaxAmount
	 */
	public double getSecondaryCountyTaxAmount() {
		return mSecondaryCountyTaxAmount;
	}

	/**
	 * Sets/updates the secondaryCountyTaxAmount.
	 * 
	 * @param pSecondaryCountyTaxAmount
	 *            the secondaryCountyTaxAmount to set
	 */
	public void setSecondaryCountyTaxAmount(double pSecondaryCountyTaxAmount) {
		mSecondaryCountyTaxAmount = pSecondaryCountyTaxAmount;
	}

	/**
	 * Returns the secondaryCityTaxAmount.
	 * 
	 * @return the secondaryCityTaxAmount
	 */
	public double getSecondaryCityTaxAmount() {
		return mSecondaryCityTaxAmount;
	}

	/**
	 * Sets/updates the secondaryCityTaxAmount.
	 * 
	 * @param pSecondaryCityTaxAmount
	 *            the secondaryCityTaxAmount to set
	 */
	public void setSecondaryCityTaxAmount(double pSecondaryCityTaxAmount) {
		mSecondaryCityTaxAmount = pSecondaryCityTaxAmount;
	}

	/**
	 * Gets the ship item relations tax price infos.
	 *
	 * @return the shipItemRelationsTaxPriceInfos
	 */
	public Map getShipItemRelationsTaxPriceInfos() {
        if(this.mShipItemRelationsTaxPriceInfos == null){
            this.mShipItemRelationsTaxPriceInfos = new HashMap();
     }
		return mShipItemRelationsTaxPriceInfos;
	}

	/**
	 * Sets the ship item relations tax price infos.
	 *
	 * @param pShipItemRelationsTaxPriceInfos the shipItemRelationsTaxPriceInfos to set
	 */
	public void setShipItemRelationsTaxPriceInfos(
			Map pShipItemRelationsTaxPriceInfos) {
		mShipItemRelationsTaxPriceInfos = pShipItemRelationsTaxPriceInfos;
	}

	/**
	 * Gets the tax city.
	 *
	 * @return the taxCity
	 */
	public String getTaxCity() {
		return mTaxCity;
	}

	/**
	 * Sets the tax city.
	 *
	 * @param pTaxCity the taxCity to set
	 */
	public void setTaxCity(String pTaxCity) {
		mTaxCity = pTaxCity;
	}

	/**
	 * Gets the tax state.
	 *
	 * @return the taxState
	 */
	public String getTaxState() {
		return mTaxState;
	}

	/**
	 * Sets the tax state.
	 *
	 * @param pTaxState the taxState to set
	 */
	public void setTaxState(String pTaxState) {
		mTaxState = pTaxState;
	}

	/**
	 * Gets the tax county.
	 *
	 * @return the taxCounty
	 */
	public String getTaxCounty() {
		return mTaxCounty;
	}

	/**
	 * Sets the tax county.
	 *
	 * @param pTaxCounty the taxCounty to set
	 */
	public void setTaxCounty(String pTaxCounty) {
		mTaxCounty = pTaxCounty;
	}

	/**
	 * Gets the tax secondory state.
	 *
	 * @return the taxSecondoryState
	 */
	public String getTaxSecondoryState() {
		return mTaxSecondoryState;
	}

	/**
	 * Sets the tax secondory state.
	 *
	 * @param pTaxSecondoryState the taxSecondoryState to set
	 */
	public void setTaxSecondoryState(String pTaxSecondoryState) {
		mTaxSecondoryState = pTaxSecondoryState;
	}

	/**
	 * Gets the tax secondory city.
	 *
	 * @return the taxSecondoryCity
	 */
	public String getTaxSecondoryCity() {
		return mTaxSecondoryCity;
	}

	/**
	 * Sets the tax secondory city.
	 *
	 * @param pTaxSecondoryCity the taxSecondoryCity to set
	 */
	public void setTaxSecondoryCity(String pTaxSecondoryCity) {
		mTaxSecondoryCity = pTaxSecondoryCity;
	}

	/**
	 * Gets the tax secondory county.
	 *
	 * @return the taxSecondoryCounty
	 */
	public String getTaxSecondoryCounty() {
		return mTaxSecondoryCounty;
	}

	/**
	 * Sets the tax secondory county.
	 *
	 * @param pTaxSecondoryCounty the taxSecondoryCounty to set
	 */
	public void setTaxSecondoryCounty(String pTaxSecondoryCounty) {
		mTaxSecondoryCounty = pTaxSecondoryCounty;
	}

	/**
	 * Gets the tax rate.
	 *
	 * @return the taxRate
	 */
	public double getTaxRate() {
		return mTaxRate;
	}

	/**
	 * Sets the tax rate.
	 *
	 * @param pTaxRate the taxRate to set
	 */
	public void setTaxRate(double pTaxRate) {
		mTaxRate = pTaxRate;
	}
	
	/**
	 * Gets the line item type.
	 *
	 * @return the lineItemType
	 */
	public String getLineItemType() {
		return mLineItemType;
	}

	/**
	 * Sets the line item type.
	 *
	 * @param pLineItemType the lineItemType to set
	 */
	public void setLineItemType(String pLineItemType) {
		mLineItemType = pLineItemType;
	}

	/**
	 * Gets the tax request timestamp.
	 *
	 * @return the taxRequestTimestamp
	 */
	public Date getTaxRequestTimestamp() {
		return mTaxRequestTimestamp;
	}

	/**
	 * Sets the tax request timestamp.
	 *
	 * @param pTaxRequestTimestamp the taxRequestTimestamp to set
	 */
	public void setTaxRequestTimestamp(Date pTaxRequestTimestamp) {
		mTaxRequestTimestamp = pTaxRequestTimestamp;
	}
	/**
	 * property to hold stateTaxRate.
	 */
	private double mStateTaxRate;
	/**
	 * property to hold taxwareSystemDown.
	 */
	private boolean mTaxwareSystemDown = Boolean.FALSE;
	/**
	 * property to hold countyTaxRate.
	 */
	private double mCountyTaxRate;
	/**
	 * property to hold cityTaxRate.
	 */
	private double mCityTaxRate;
	/**
	 * property to hold secondaryStateTaxRate.
	 */
	private double mSecondaryStateTaxRate;
	/**
	 * property to hold secondaryCountyTaxRate.
	 */
	private double mSecondaryCountyTaxRate;
	/**
	 * property to hold secondaryCityTaxRate.
	 */
	private double mSecondaryCityTaxRate;
	/**
	 * property to hold stateBasisAmount.
	 */
	private double mStateBasisAmount;
	/**
	 * property to hold countyBasisAmount.
	 */
	private double mCountyBasisAmount;
	/**
	 * property to hold cityBasisAmount.
	 */
	private double mCityBasisAmount;
	/**
	 * property to hold secondaryStateBasisAmount.
	 */
	private double mSecondaryStateBasisAmount;
	/**
	 * property to hold SecondaryCountyBasisAmount.
	 */
	private double mSecondaryCountyBasisAmount;
	/**
	 * property to hold secondaryCityBasisAmount.
	 */
	private double mSecondaryCityBasisAmount;


	/**
	 * @return the taxwareSystemDown
	 */
	public boolean isTaxwareSystemDown() {
		return mTaxwareSystemDown;
	}

	/**
	 * @param pTaxwareSystemDown the taxwareSystemDown to set
	 */
	public void setTaxwareSystemDown(boolean pTaxwareSystemDown) {
		mTaxwareSystemDown = pTaxwareSystemDown;
	}

	/**
	 * @return the stateTaxRate
	 */
	public double getStateTaxRate() {
		return mStateTaxRate;
	}

	/**
	 * @param pStateTaxRate the stateTaxRate to set
	 */
	public void setStateTaxRate(double pStateTaxRate) {
		mStateTaxRate = pStateTaxRate;
	}

	/**
	 * @return the countyTaxRate
	 */
	public double getCountyTaxRate() {
		return mCountyTaxRate;
	}

	/**
	 * @param pCountyTaxRate the countyTaxRate to set
	 */
	public void setCountyTaxRate(double pCountyTaxRate) {
		mCountyTaxRate = pCountyTaxRate;
	}

	/**
	 * @return the cityTaxRate
	 */
	public double getCityTaxRate() {
		return mCityTaxRate;
	}

	/**
	 * @param pCityTaxRate the cityTaxRate to set
	 */
	public void setCityTaxRate(double pCityTaxRate) {
		mCityTaxRate = pCityTaxRate;
	}

	/**
	 * @return the secondaryStateTaxRate
	 */
	public double getSecondaryStateTaxRate() {
		return mSecondaryStateTaxRate;
	}

	/**
	 * @param pSecondaryStateTaxRate the secondaryStateTaxRate to set
	 */
	public void setSecondaryStateTaxRate(double pSecondaryStateTaxRate) {
		mSecondaryStateTaxRate = pSecondaryStateTaxRate;
	}

	/**
	 * @return the secondaryCountyTaxRate
	 */
	public double getSecondaryCountyTaxRate() {
		return mSecondaryCountyTaxRate;
	}

	/**
	 * @param pSecondaryCountyTaxRate the secondaryCountyTaxRate to set
	 */
	public void setSecondaryCountyTaxRate(double pSecondaryCountyTaxRate) {
		mSecondaryCountyTaxRate = pSecondaryCountyTaxRate;
	}

	/**
	 * @return the secondaryCityTaxRate
	 */
	public double getSecondaryCityTaxRate() {
		return mSecondaryCityTaxRate;
	}

	/**
	 * @param pSecondaryCityTaxRate the secondaryCityTaxRate to set
	 */
	public void setSecondaryCityTaxRate(double pSecondaryCityTaxRate) {
		mSecondaryCityTaxRate = pSecondaryCityTaxRate;
	}

	/**
	 * @return the stateBasisAmount
	 */
	public double getStateBasisAmount() {
		return mStateBasisAmount;
	}

	/**
	 * @param pStateBasisAmount the stateBasisAmount to set
	 */
	public void setStateBasisAmount(double pStateBasisAmount) {
		mStateBasisAmount = pStateBasisAmount;
	}

	/**
	 * @return the countyBasisAmount
	 */
	public double getCountyBasisAmount() {
		return mCountyBasisAmount;
	}

	/**
	 * @param pCountyBasisAmount the countyBasisAmount to set
	 */
	public void setCountyBasisAmount(double pCountyBasisAmount) {
		mCountyBasisAmount = pCountyBasisAmount;
	}

	/**
	 * @return the cityBasisAmount
	 */
	public double getCityBasisAmount() {
		return mCityBasisAmount;
	}

	/**
	 * @param pCityBasisAmount the cityBasisAmount to set
	 */
	public void setCityBasisAmount(double pCityBasisAmount) {
		mCityBasisAmount = pCityBasisAmount;
	}

	/**
	 * @return the secondaryStateBasisAmount
	 */
	public double getSecondaryStateBasisAmount() {
		return mSecondaryStateBasisAmount;
	}

	/**
	 * @param pSecondaryStateBasisAmount the secondaryStateBasisAmount to set
	 */
	public void setSecondaryStateBasisAmount(double pSecondaryStateBasisAmount) {
		mSecondaryStateBasisAmount = pSecondaryStateBasisAmount;
	}

	/**
	 * @return the secondaryCountyBasisAmount
	 */
	public double getSecondaryCountyBasisAmount() {
		return mSecondaryCountyBasisAmount;
	}

	/**
	 * @param pSecondaryCountyBasisAmount the secondaryCountyBasisAmount to set
	 */
	public void setSecondaryCountyBasisAmount(double pSecondaryCountyBasisAmount) {
		mSecondaryCountyBasisAmount = pSecondaryCountyBasisAmount;
	}

	/**
	 * @return the secondaryCityBasisAmount
	 */
	public double getSecondaryCityBasisAmount() {
		return mSecondaryCityBasisAmount;
	}

	/**
	 * @param pSecondaryCityBasisAmount the secondaryCityBasisAmount to set
	 */
	public void setSecondaryCityBasisAmount(double pSecondaryCityBasisAmount) {
		mSecondaryCityBasisAmount = pSecondaryCityBasisAmount;
	}
	
	/**
	 * Property to hold radialTaxPriceInfos.
	 */
	private Map<String, String> mRadialTaxPriceInfos;
	
    /**
	 * @return the radialTaxPriceInfos
	 */
	public Map<String, String> getRadialTaxPriceInfos() {
		return mRadialTaxPriceInfos;
	}
	/**
	 * @param pRadialTaxPriceInfos the radialTaxPriceInfos to set
	 */
	public void setRadialTaxPriceInfos(Map<String, String> pRadialTaxPriceInfos) {
		mRadialTaxPriceInfos = pRadialTaxPriceInfos;
	}
	
	/** The Radial tax transaction id. */
	private String  mRadialTaxTransactionId;
	
	/** The Radial tax fault details. */
	private String  mRadialTaxFaultDetails;

	/**
	 * @return the radialTaxTransactionId
	 */
	public String getRadialTaxTransactionId() {
		return mRadialTaxTransactionId;
	}

	/**
	 * @param pRadialTaxTransactionId the radialTaxTransactionId to set
	 */
	public void setRadialTaxTransactionId(String pRadialTaxTransactionId) {
		mRadialTaxTransactionId = pRadialTaxTransactionId;
	}

	/**
	 * @return the radialTaxFaultDetails
	 */
	public String getRadialTaxFaultDetails() {
		return mRadialTaxFaultDetails;
	}

	/**
	 * @param pRadialTaxFaultDetails the radialTaxFaultDetails to set
	 */
	public void setRadialTaxFaultDetails(String pRadialTaxFaultDetails) {
		mRadialTaxFaultDetails = pRadialTaxFaultDetails;
	}
	
	/** The Estimated sales tax. */
	private double mEstimatedSalesTax;
	
	/** The Estimated local tax. */
	private double mEstimatedLocalTax;
	
	/** The Estimated island tax. */
	private double mEstimatedIslandTax;

	/**
	 * @return the estimatedSalesTax
	 */
	public double getEstimatedSalesTax() {
		return mEstimatedSalesTax;
	}
	/**
	 * @param pEstimatedSalesTax the estimatedSalesTax to set
	 */
	public void setEstimatedSalesTax(double pEstimatedSalesTax) {
		mEstimatedSalesTax = pEstimatedSalesTax;
	}
	/**
	 * @return the estimatedLocalTax
	 */
	public double getEstimatedLocalTax() {
		return mEstimatedLocalTax;
	}
	/**
	 * @param pEstimatedLocalTax the estimatedLocalTax to set
	 */
	public void setEstimatedLocalTax(double pEstimatedLocalTax) {
		mEstimatedLocalTax = pEstimatedLocalTax;
	}
	/**
	 * @return the estimatedIslandTax
	 */
	public double getEstimatedIslandTax() {
		return mEstimatedIslandTax;
	}
	/**
	 * @param pEstimatedIslandTax the estimatedIslandTax to set
	 */
	public void setEstimatedIslandTax(double pEstimatedIslandTax) {
		mEstimatedIslandTax = pEstimatedIslandTax;
	}
	
}
