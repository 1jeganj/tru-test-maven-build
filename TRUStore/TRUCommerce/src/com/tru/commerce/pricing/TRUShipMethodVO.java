package com.tru.commerce.pricing;

import java.io.Serializable;
import java.util.Set;

import com.tru.common.TRUConstants;

/**
 * The Class TRUShipMethodVO.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUShipMethodVO implements Serializable {

	/**  serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/**  mShipMethId. */
	private String mShipMethId;
	
	/**  mShipMethodCode. */
	private String mShipMethodCode;
	
	/**  mShipMethodName. */
	private String mShipMethodName;
	
	/**  mShippingPrice. */
	private double mShippingPrice;
	
	/**  mDetails. */
	private String mDetails;
	
	/**  mRegionCode. */
	private String mRegionCode;
	
	/**  mDelivertTimeLine. */
	private String mDelivertTimeLine;
	
	/**  mNonQualifiedAreas. */
	private Set<String> mNonQualifiedAreas;
	
	/**  mErrorMessage. */
	private String mErrorMessage;
	
	/**  mFreightClasses. */
	private Set<String> mFreightClasses;
	
	/**  mShipRegionVO. */	
	private TRUShipRegionVO mShipRegionVO;
	
	
	/**
	 * TRUShipMethodVO Constructor.
	 */
	public TRUShipMethodVO() {
		super();
	}
	
	/**
	 * TRUShipMethodVO Constructor.
	 * @param pShipMethodId - Shipping method ID.
	 * @param pShipMethodCode2 - Shipping method Code.
	 * @param pShipMethodName2 - Shipping method Name.
	 * @param pDetails2 - Details.
	 */
	public TRUShipMethodVO(String pShipMethodId, String pShipMethodCode2,
			String pShipMethodName2, String pDetails2) {
		this.mShipMethId=pShipMethodId;
		this.mShipMethodCode = pShipMethodCode2;
		this.mShipMethodName = pShipMethodName2;
		this.mDetails=pDetails2;
	}
	
	/**
	 * Gets the ship method code.
	 *
	 * @return the shipMethodCode.
	 */
	public String getShipMethodCode() {
		return mShipMethodCode;
	}
	
	/**
	 * Sets the ship method code.
	 *
	 * @param pShipMethodCode the shipMethodCode to set.
	 */
	public void setShipMethodCode(String pShipMethodCode) {
		this.mShipMethodCode = pShipMethodCode;
	}
	
	/**
	 * Gets the ship method name.
	 *
	 * @return the shipMethodName.
	 */
	public String getShipMethodName() {
		return mShipMethodName;
	}
	
	/**
	 * Sets the ship method name.
	 *
	 * @param pShipMethodName the shipMethodName to set.
	 */
	public void setShipMethodName(String pShipMethodName) {
		this.mShipMethodName = pShipMethodName;
	}
	
	/**
	 * Gets the shipping price.
	 *
	 * @return the shippingPrice.
	 */
	public double getShippingPrice() {
		return mShippingPrice;
	}
	
	/**
	 * Sets the shipping price.
	 *
	 * @param pShippingPrice the shippingPrice to set.
	 */
	public void setShippingPrice(double pShippingPrice) {
		this.mShippingPrice = pShippingPrice;
	}
	
	/**
	 * Gets the ship meth id.
	 *
	 * @return the shipMethId.
	 */
	public String getShipMethId() {
		return mShipMethId;
	}
	
	/**
	 * Sets the ship meth id.
	 *
	 * @param pShipMethId the shipMethId to set.
	 */
	public void setShipMethId(String pShipMethId) {
		this.mShipMethId = pShipMethId;
	}
	

	/**
	 * Gets the region code.
	 *
	 * @return the regionCode.
	 */
	public String getRegionCode() {
		return mRegionCode;
	}
	
	/**
	 * Sets the region code.
	 *
	 * @param pRegionCode the regionCode to set.
	 */
	public void setRegionCode(String pRegionCode) {
		this.mRegionCode = pRegionCode;
	}
	
	/**
	 * Gets the details.
	 *
	 * @return the details.
	 */	
	public String getDetails() {
		return mDetails;
	}
	
	/**
	 * Sets the details.
	 *
	 * @param pDetails the details to set.
	 */
	public void setDetails(String pDetails) {
		this.mDetails = pDetails;
	}
	
	/**
	 * Gets the delivert time line.
	 *
	 * @return the delivertTimeLine.
	 */
	public String getDelivertTimeLine() {
		return mDelivertTimeLine;
	}
	
	/**
	 * Sets the delivert time line.
	 *
	 * @param pDelivertTimeLine the delivertTimeLine to set.
	 */
	public void setDelivertTimeLine(String pDelivertTimeLine) {
		this.mDelivertTimeLine = pDelivertTimeLine;
	}
	
	/**
	 * Gets the ship region vo.
	 *
	 * @return the shipRegionVO.
	 */
	public TRUShipRegionVO getShipRegionVO() {
		return mShipRegionVO;
	}
	
	/**
	 * Sets the ship region vo.
	 *
	 * @param pShipRegionVO the shipRegionVO to set.
	 */
	public void setShipRegionVO(TRUShipRegionVO pShipRegionVO) {
		this.mShipRegionVO = pShipRegionVO;
	}

	/**
	 * Gets the non qualified areas.
	 *
	 * @return the nonQualifiedAreas.
	 */
	public Set<String> getNonQualifiedAreas() {
		return mNonQualifiedAreas;
	}
	
	/**
	 * Sets the non qualified areas.
	 *
	 * @param pNonQualifiedAreas the nonQualifiedAreas to set.
	 */
	public void setNonQualifiedAreas(Set<String> pNonQualifiedAreas) {
		this.mNonQualifiedAreas = pNonQualifiedAreas;
	}
	
	/**
	 * Gets the error message.
	 *
	 * @return the errorMessage.
	 */
	public String getErrorMessage() {
		return mErrorMessage;
	}
	
	/**
	 * Sets the error message.
	 *
	 * @param pErrorMessage the errorMessage to set.
	 */
	public void setErrorMessage(String pErrorMessage) {
		this.mErrorMessage = pErrorMessage;
	}
	
	/**
	 * Gets the freight classes.
	 *
	 * @return the freightClasses.
	 */
	public Set<String> getFreightClasses() {
		return mFreightClasses;
	}
	
	/**
	 * Sets the freight classes.
	 *
	 * @param pFreightClasses the freightClasses to set.
	 */
	public void setFreightClasses(Set<String> pFreightClasses) {
		this.mFreightClasses = pFreightClasses;
	}
	
	/**
	 * This method returns the hash code.
	 * @return result - Integer.
	 */
	@Override
	public int hashCode() {
		int prime = TRUConstants.INTEGER_NUMBER_THIRTY_ONE;
		int result = TRUConstants.INTEGER_NUMBER_ONE;
		result = prime * result + ((mShipMethodCode == null) ? TRUConstants.INTEGER_NUMBER_ZERO : mShipMethodCode.hashCode());
		return result;
	}

	/**
	 * This method is to check equals.
	 *@param pObj - Object.
	 *@return boolean.
	 */
	@Override
	public boolean equals(Object pObj) {
		if (this == pObj)
		{
			return true;
		}
		if (pObj == null)
		{
			return false;
		}
		if (getClass() != pObj.getClass())
		{
			return false;
		}
		TRUShipMethodVO other = (TRUShipMethodVO) pObj;
		if (mShipMethodCode == null) {
			if (other.mShipMethodCode != null){
				return false;
			}
		} else if (!mShipMethodCode.equals(other.mShipMethodCode)){
			return false;
		}
		return true;
	}

	/**
	 * This method is to convert to String.
	 * @return string - String.
	 */
	@Override
	public String toString() {
		return "TRUShipMethodVO [Code=" + mShipMethodCode + ","	+ " Name=" + mShipMethodName + ", RegionCode=" +
				mRegionCode + " Price"+mShippingPrice+"]\n";
	}
	
	
}
