package com.tru.commerce.pricing;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.Constants;
import atg.commerce.pricing.DetailedItemPriceInfo;
import atg.commerce.pricing.FilteredCommerceItem;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.PricingContext;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.Qualifier;
import atg.commerce.pricing.definition.MatchingObject;
import atg.commerce.promotion.TRUPromotionTools;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.common.TRUConstants;

/**
 * The Class TRUQualifier.
 *
 * @author Professional Access
 * @version 1.0
 * TRUQualifier extends the OOTB Qualifier.
 * 
 * This Qualify class will be used to qualify the promotion.
 */
public class TRUQualifier extends Qualifier{
	
	/** Constant for DISCOUNTABLE_MAP. */
	public static final String DISCOUNTABLE_MAP = "discountableMap";
	
	/**
	 * Holds reference of mPromotionId.
	 */
	private String mPromotionId;
	/**
	 * Holds reference of mQualifiedItem Map.
	 */
	private Map<String, Long> mQualifiedItem;

	/**
	 * @return the qualifiedItem
	 */
	public Map<String, Long> getQualifiedItem() {
		if(mQualifiedItem == null){
			mQualifiedItem = new HashMap<String, Long>();
		}
		return mQualifiedItem;
	}

	/**
	 * @param pQualifiedItem the qualifiedItem to set
	 */
	public void setQualifiedItem(Map<String, Long> pQualifiedItem) {
		mQualifiedItem = pQualifiedItem;
	}

	/**
	 * @return the promotionId
	 */
	public String getPromotionId() {
		return mPromotionId;
	}

	/**
	 * @param pPromotionId the promotionId to set
	 */
	public void setPromotionId(String pPromotionId) {
		mPromotionId = pPromotionId;
	}

	

	/**
	 * This method has been overridden to qualify the order by excluding items from order level promotions.
	 * 
	 * @param pPricingContext - the PricingContext
	 * @param pExtraParametersMap - Map of ExtraParameters
	 * @throws PricingException - the PricingException
	 * @return the MatchingObject
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public MatchingObject findQualifyingOrder(PricingContext pPricingContext,
			Map pExtraParametersMap) throws PricingException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUQualifier  method: findQualifyingOrder]");
			vlogDebug("pPricingContext : {0} pExtraParametersMap : {1}", pPricingContext,pExtraParametersMap);
		}
		if ((pPricingContext == null) || (pPricingContext.getOrder() == null)) {
			throw new PricingException(Constants.NO_ORDER);
		}
		if (pPricingContext.getPricingModel() == null) {
			throw new PricingException(MessageFormat.format(Constants.NO_PRICING_MODEL, new Object[TRUConstants.INT_ZERO]));
		}
		OrderPriceInfo orderPriceInfo = null;
		if(pPricingContext.getOrderPriceQuote() != null){
			orderPriceInfo = pPricingContext.getOrderPriceQuote();
		}
		RepositoryItem pricingModel = pPricingContext.getPricingModel();
		TRUPromotionTools promotionTools = (TRUPromotionTools)getPromotionTools();
		MatchingObject matchingObject = null;
		Order order = pPricingContext.getOrder();
		boolean isPromoItem = Boolean.FALSE;
		try {
			isPromoItem = promotionTools.isPromotionItem(pricingModel);
		} catch (IllegalArgumentException iaexcp) {
			if (isLoggingError()) {
				vlogError("IllegalArgumentException: while checking promotion item pricingModel : {0} with exception : {1}",
						pricingModel,iaexcp);
			}
		} catch (RepositoryException repoExcp) {
			if (isLoggingError()) {
				vlogError("RepositoryException: while checking promotion item pricingModel : {0} with exception : {1}",
						pricingModel,repoExcp);
			}
		}
		if(isPromoItem){
			// Getting Qualifier SKU from promotion.
			//List<RepositoryItem> qualifiedSKUforPromo = promotionTools.getQualifiedSKUforPromo(pricingModel);
			List<RepositoryItem> qualifiedSKUforPromo=new ArrayList<RepositoryItem>();
			List<RepositoryItem> excludedSKUforPromo=new ArrayList<RepositoryItem>();
			promotionTools.getQualifiedSKUforPromoFromCatalog(order,pricingModel,qualifiedSKUforPromo,excludedSKUforPromo);
			// Getting Qualifier Bulk SKU from promotion.
			List<String> qualifyBulkSkuIds = promotionTools.getQualifyBulkSkuIds(pricingModel);
			// Getting Excluded SKU from promotion.
			//List<RepositoryItem> excludedSKUforPromo = promotionTools.getExcludedSKUforPromo(pricingModel);
			// Getting Excluded Bulk SKU from promotion.
			List<String> excludedBulkSkuIds = promotionTools.getExcludedBulkSkuIds(pricingModel);
			// Old Order Amount
			Double originalAmount = orderPriceInfo.getAmount();
			if(isLoggingDebug()){
				vlogDebug("Order Actual Amount : {0}", originalAmount);
			}
			// Method to get the Discountable Amount.
			double discountableAmount = getDiscountableTotal(pPricingContext.getOrder(),pExtraParametersMap,pPricingContext,
					qualifiedSKUforPromo,qualifyBulkSkuIds,excludedSKUforPromo,excludedBulkSkuIds);
			if(isLoggingDebug()){
				vlogDebug("discountableAmount : {0}", discountableAmount);
			}
			//List items = new ArrayList(pPricingContext.getItems());
			// Calling method to update the Filter Item Details.
			//updateFilterItemDetails(pPricingContext,qualifiedSKUforPromo,qualifyBulkSkuIds,excludedSKUforPromo);
			if(discountableAmount > TRUConstants.DOUBLE_ZERO){
				if(discountableAmount < originalAmount){
					orderPriceInfo.setAmount(discountableAmount);
				}else{
					orderPriceInfo.setAmount(originalAmount);
				}
				pPricingContext.setOrderPriceQuote(orderPriceInfo);
				// Calling Super Method.
				matchingObject =  super.findQualifyingOrder(pPricingContext, pExtraParametersMap);
				// Setting back the Original Amount.
				orderPriceInfo.setAmount(originalAmount);
				// Setting back the Original Filter Item reference.
				//pPricingContext.setItems(items);
				// Setting back the Order Price Info Object.
				pPricingContext.setOrderPriceQuote(orderPriceInfo);
			}
		}else{
			// Calling Super Method.
			matchingObject =  super.findQualifyingOrder(pPricingContext, pExtraParametersMap);
		}
		if(isLoggingDebug()){
			vlogDebug("Qualified Order Item : {0}", matchingObject);
			logDebug("END : TRUQualifier findQualifyingOrder()");
		}
		return matchingObject;
	}

	/**
	 * Method to get the nonDiscountable amount.
	 * 
	 * @param pOrder - Order Object
	 * @param pExtraParametersMap - Map
	 * @param pPricingContext - PricingContext Object
	 * @param pQualifiedSKUforPromo - List of Qualified SKU Repository Items.
	 * @param pQualifyBulkSkuIds - List of Qualified SKU ids.
	 * @param pExcludedSKUforPromo - List of Excluded SKU Repository Items.
	 * @param pExcludedBulkSkuIds -List of Excluded SKU ids.
	 * @return double - nonDiscountable amount.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public double getNonDiscountableTotal(Order pOrder, Map pExtraParametersMap,PricingContext pPricingContext,
			List<RepositoryItem> pQualifiedSKUforPromo, List<String> pQualifyBulkSkuIds, 
			List<RepositoryItem> pExcludedSKUforPromo, List<String> pExcludedBulkSkuIds) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUQualifier  method: getNonDiscountableTotal]");
			vlogDebug("pOrder : {0} pExtraParametersMap : {1} pPricingContext : {2}", 
					pOrder,pExtraParametersMap,pPricingContext);
			vlogDebug("pQualifiedSKUforPromo : {0} pQualifyBulkSkuIds : {1} pExcludedSKUforPromo : {2}", 
					pQualifiedSKUforPromo,pQualifyBulkSkuIds,pExcludedSKUforPromo);
		}
		Double nonDiscountableTotal = TRUConstants.DOUBLE_ZERO;
		if(pOrder == null || pPricingContext == null || pPricingContext.getItems() == null){
			if(isLoggingDebug()){
				vlogDebug("nonDiscountableTotal : {0}", nonDiscountableTotal);
				logDebug("END : TRUQualifier getNonDiscountableTotal()");
			}
			return nonDiscountableTotal;
		}
		List<CommerceItem> commerceItems = pOrder.getCommerceItems();
		if(commerceItems == null || commerceItems.isEmpty()){
			if(isLoggingDebug()){
				vlogDebug("nonDiscountableTotal : {0}", nonDiscountableTotal);
				logDebug("END : TRUQualifier getNonDiscountableTotal()");
			}
			return nonDiscountableTotal;
		}
		FilteredCommerceItem commerceItem = null;
		for (Object obj : pPricingContext.getItems()) {
			commerceItem = (FilteredCommerceItem)obj;
			// Calling method to check item is eligible for Proration or not.
			((TRUItemPriceInfo)commerceItem.getPriceInfo()).setExcludedForOrderDiscount(
					isItemExcludedForOrderDisccount(commerceItem,pQualifiedSKUforPromo,
							pQualifyBulkSkuIds,pExcludedSKUforPromo,pExcludedBulkSkuIds));
		}
		CommerceItem ci = null;
		String productId = null;
		String skuId = null;
		String keyToMap = TRUConstants.EMPTY;
		TRUItemPriceInfo itemPriceInfo = null;
		Iterator itemIterator = commerceItems.iterator();
		while (itemIterator.hasNext()) {
			ci = (CommerceItem)itemIterator.next();
			skuId = ci.getCatalogRefId();
			productId = ci.getAuxiliaryData().getProductId();
			keyToMap = productId+skuId;
			itemPriceInfo = (TRUItemPriceInfo) ci.getPriceInfo();
			if(itemPriceInfo == null){
				continue;
			}
			if(itemPriceInfo.isExcludedForOrderDiscount()){
				nonDiscountableTotal = Double.valueOf(nonDiscountableTotal.doubleValue() + itemPriceInfo.getAmount());
			}else if (pExtraParametersMap != null) {
		        Map previousOrder = (Map)pExtraParametersMap.get(DISCOUNTABLE_MAP);
		        if (previousOrder != null) {
		          if ((previousOrder.containsKey(keyToMap)) && 
		            (!(((Boolean)previousOrder.get(keyToMap)).booleanValue()))) {
		            nonDiscountableTotal = Double.valueOf(nonDiscountableTotal.doubleValue() + itemPriceInfo.getAmount());
		          }
		        }
		        else if (!(ci.getPriceInfo().getDiscountable())) {
		            nonDiscountableTotal = Double.valueOf(nonDiscountableTotal.doubleValue() + itemPriceInfo.getAmount());
		        }
		   }else if (!(ci.getPriceInfo().getDiscountable())) {
			   nonDiscountableTotal = Double.valueOf(nonDiscountableTotal.doubleValue() + itemPriceInfo.getAmount());
		   }
		}
		TRUPricingTools pricingTools = (TRUPricingTools) getPricingTools();
		// Calling method to get the BPP amount.
		nonDiscountableTotal = nonDiscountableTotal+pricingTools.getBPPItemsAmount(pOrder);
		if(isLoggingDebug()){
			vlogDebug("nonDiscountableTotal : {0}", nonDiscountableTotal);
			logDebug("END : TRUQualifier getNonDiscountableTotal()");
		}
		return nonDiscountableTotal;
	}

	/**
	 * Method to find item is excluded from Item Discount or not.
	 * 
	 * @param pCommerceItem - CommerceItem Object
	 * @param pQualifiedSKUforPromo - List of Qualified SKU Repository Items.
	 * @param pQualifyBulkSkuIds - List of Qualified SKU ids.
	 * @param pExcludedSKUforPromo - List of Excluded SKU Repository Items.
	 * @param pExcludedBulkSkuIds - List of Excluded SKU ids
	 * @return boolean.
	 */
	private boolean isItemExcludedForOrderDisccount(FilteredCommerceItem pCommerceItem,List<RepositoryItem> pQualifiedSKUforPromo,
			List<String> pQualifyBulkSkuIds, List<RepositoryItem> pExcludedSKUforPromo, List<String> pExcludedBulkSkuIds) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUQualifier  method: isItemExcludedForOrderDisccount]");
			vlogDebug("pCommerceItem : {0}", pCommerceItem);
			vlogDebug("pQualifiedSKUforPromo : {0} pQualifyBulkSkuIds : {1} pExcludedSKUforPromo : {2}", 
					pQualifiedSKUforPromo,pQualifyBulkSkuIds,pExcludedSKUforPromo);
		}
		boolean isItemExcluded = Boolean.FALSE;
		if(pCommerceItem == null && isLoggingDebug()){
				vlogDebug("TRUQualifier.isItemExcluded : {0}", isItemExcluded);
		}
		CommerceItem wrappedItem = pCommerceItem.getWrappedItem();
		String skuId = pCommerceItem.getCatalogRefId();
		RepositoryItem skuRepoItem = (RepositoryItem) pCommerceItem.getAuxiliaryData().getCatalogRef();
		if(wrappedItem instanceof TRUDonationCommerceItem || wrappedItem instanceof TRUGiftWrapCommerceItem){
			if(isLoggingDebug()){
				vlogDebug("TRUQualifier.isItemExcluded : {0}", Boolean.TRUE);
			}
			return true;
		}
		if(pExcludedSKUforPromo != null && pExcludedSKUforPromo.contains(skuRepoItem)){
			if(isLoggingDebug()){
				vlogDebug("TRUQualifier.isItemExcluded : {0}", Boolean.TRUE);
			}
			return true;
	    }
		if(pExcludedBulkSkuIds != null && pExcludedBulkSkuIds.contains(skuId)){
			if(isLoggingDebug()){
				vlogDebug("TRUQualifier.isItemExcluded : {0}", Boolean.TRUE);
			}
			return true;
	    }
		if((pQualifiedSKUforPromo == null|| pQualifiedSKUforPromo.isEmpty()) && 
	    		  (pQualifyBulkSkuIds == null || pQualifyBulkSkuIds.isEmpty())){
			if(isLoggingDebug()){
				vlogDebug("TRUQualifierisItemExcluded : {0}", Boolean.FALSE);
			}
			return false;
	    }
	    if((pQualifiedSKUforPromo == null || !pQualifiedSKUforPromo.contains(skuRepoItem))
	    		&& (pQualifyBulkSkuIds == null || !pQualifyBulkSkuIds.contains(skuId))){
	    	if(isLoggingDebug()){
				vlogDebug("TRUQualifier.isItemExcluded : {0}", Boolean.TRUE);
			}
	    	return true;
	    }
	    if(isLoggingDebug()){
			vlogDebug("isItemExcluded : {0}", isItemExcluded);
			logDebug("END : TRUQualifier isItemExcludedForOrderDisccount()");
		}
		return isItemExcluded;
	}

	/**
	 * Method is used to update for info object for "Spend in X get Order Discount".
	 * 
	 * @param pPricingContext - PricingContext Object.
	 * @param pQualifiedSKUforPromo - List of Qualified SKU Repository Items.
	 * @param pQualifyBulkSkuIds - List of Qualified SKU ids.
	 * @param pExcludedSKUforPromo - List of Excluded SKU Repository Items.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void updateFilterItemDetails(PricingContext pPricingContext,
			List<RepositoryItem> pQualifiedSKUforPromo,
			List<String> pQualifyBulkSkuIds,
			List<RepositoryItem> pExcludedSKUforPromo) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUQualifier  method: updateFilterItemDetails]");
			vlogDebug("pPricingContext : {0}", pPricingContext);
			vlogDebug("pQualifiedSKUforPromo : {0} pQualifyBulkSkuIds : {1} pExcludedSKUforPromo : {2}", 
					pQualifiedSKUforPromo,pQualifyBulkSkuIds,pExcludedSKUforPromo);
		}
		if(pPricingContext == null || pPricingContext.getItems() == null || pPricingContext.getItems().isEmpty()){
			return;
		}
		List<CommerceItem> newItem = new ArrayList(pPricingContext.getItems());
		TRUItemPriceInfo itemPriceInfo = null;
		for(CommerceItem item : newItem){
			itemPriceInfo = (TRUItemPriceInfo) item.getPriceInfo();
			Map truOrderDiscountShare = itemPriceInfo.getTruOrderDiscountShare();
			double orderShare = TRUConstants.DOUBLE_ZERO;
			double newAmount = TRUConstants.DOUBLE_ZERO;
			if(truOrderDiscountShare != null && !truOrderDiscountShare.isEmpty()){
				Iterator it = truOrderDiscountShare.entrySet().iterator();
				 while (it.hasNext()) {
					 Map.Entry entry  = (Map.Entry)it.next();
					 if(entry.getValue() instanceof Double){
						 orderShare += ((Double)entry.getValue());
					 }
				 }
			}
			newAmount = Double.valueOf(itemPriceInfo.getAmount() - orderShare);
			if(orderShare > TRUConstants.DOUBLE_ZERO){
		    	try {
		    		List detailItem = new ArrayList(itemPriceInfo.getCurrentPriceDetailsSortedByAmount(getDetailSortOrder()));
					Iterator detailsIterator = detailItem.iterator();
					while (detailsIterator.hasNext()) {
						DetailedItemPriceInfo dpi = (DetailedItemPriceInfo)detailsIterator.next();
						dpi.setAmount(newAmount);
					}
					itemPriceInfo.setCurrentPriceDetails(detailItem);
					item.setPriceInfo(itemPriceInfo);
				} catch (PricingException e) {
					if (isLoggingError()) {
						vlogError("PricingException : {0}", e);
					}
				}
		    }
			
		}
		if(newItem != null && !newItem.isEmpty()){
			pPricingContext.setItems(newItem);
		}
		if(isLoggingDebug()){
			logDebug("END : TRUQualifier updateFilterItemDetails()");
		}
	}
	
	/**
	 * Overridden the OOTB method to get the Qualified Item and quantity in Map.
	 * 
	 * @param pPricingContext - PricingContext Object
	 * @param pExtraParametersMap - Map Object
	 * @param pFilteredQualifierItems - List - List of filtered commerce items.
	 * @return Object - Matching Object
	 * @throws - PricingException
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected Object evaluateQualifier(PricingContext pPricingContext,
			Map pExtraParametersMap, List pFilteredQualifierItems)
			throws PricingException {
		if(isLoggingDebug()){
			logDebug("START : TRUQualifier evaluateQualifier()");
		}
		//Calling Super Method.
		Object matchingObject = super.evaluateQualifier(pPricingContext, pExtraParametersMap, pFilteredQualifierItems);
		if(isLoggingDebug()){
			vlogDebug("matchingObject : {0}", matchingObject);
		}
		if(!(matchingObject instanceof Collection)){
			return matchingObject;
		}
		Map<String, Long> qualifiedItem = getQualifiedItem();
		for (Object object : (Collection)matchingObject) {
			if(!(object instanceof MatchingObject)){
				continue;
			}
			MatchingObject matchObj = (MatchingObject)object;
			if(matchObj != null && matchObj.getMatchingObject() instanceof FilteredCommerceItem){
				qualifiedItem.put(((FilteredCommerceItem)matchObj.getMatchingObject()).getCatalogRefId(), matchObj.getQuantity());
				setPromotionId(pPricingContext.getPricingModel().getRepositoryId());
			}
		}
		if(qualifiedItem != null && !qualifiedItem.isEmpty()){
			setQualifiedItem(qualifiedItem);
		}
		if(isLoggingDebug()){
			logDebug("END : TRUQualifier evaluateQualifier()");
		}
		return matchingObject;
	}
	
	/**
	 * Overridden the OOTB method to get the Target Item and qualified item quantity in Map.
	 * 
	 * @param pPricingContext - PricingContext Object
	 * @param pExtraParametersMap - Map Object
	 * @param pFilteredTargetItems - List - List of filtered commerce items.
	 * @return Object - Matching Object
	 * @throws - PricingException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked"})
	@Override
	protected Object evaluateTarget(PricingContext pPricingContext,Map pExtraParametersMap, List pFilteredTargetItems)
			throws PricingException {
		if (isLoggingDebug()) {
			logDebug("START : TRUQualifier evaluateTarget()");
		}
		TRUOrderTools orderTools = (TRUOrderTools) getPromotionTools().getPricingTools().getOrderTools();
		String tieredBasePromo=orderTools.getTieredPriceBreakTemplate();
		String advancedPromo=((TRUPricingTools)getPricingTools()).getTRUCommercePropertyManager().getAdvancedItemTemplatePropertyName();
		String propertyValue = (String) pPricingContext.getPricingModel().getPropertyValue(getPricingModelProperties().getTemplate());
		Map<String, Map<String, Long>> targetItem = null;
		Map<String, Long> target = null;
		Map<String, Map<String, Long>> qualifyItem = null;
		Map<String, Long> qualify = null;
		// Calling Super Method.
		Object matchingObject = super.evaluateTarget(pPricingContext,pExtraParametersMap, pFilteredTargetItems);
		if(isLoggingDebug()){
			vlogDebug("matchingObject : {0}", matchingObject);
		}
		if(!(matchingObject instanceof Collection)){
			return matchingObject;
		}
		boolean isTieredPromotion=false;
		if(null!=advancedPromo &&null!=propertyValue&& advancedPromo.equals(propertyValue)){
			isTieredPromotion=((TRUPricingTools)getPricingTools()).isTieredPromotion(pPricingContext.getPricingModel());
		}
		if((null!=tieredBasePromo &&null!=propertyValue&& tieredBasePromo.equals(propertyValue))||isTieredPromotion){
			return matchingObject;
		}
		int i = TRUConstants.ZERO;
		for (Object object : (Collection) matchingObject) {
			if (!(object instanceof MatchingObject)) {
				continue;
			}
			MatchingObject matchObj = (MatchingObject) object;
			if (matchObj == null || !(matchObj.getMatchingObject() instanceof FilteredCommerceItem)) {
				continue;
			}
			targetItem = (Map<String, Map<String, Long>>) pExtraParametersMap.get(TRUConstants.TARGET_ITEM);
			if (targetItem == null) {
				targetItem = new HashMap<String, Map<String, Long>>();
			}
			target = targetItem.get(pPricingContext.getPricingModel().getRepositoryId());
			if (target == null) {
				target = new HashMap<String, Long>();
			}
			Long qty = target.get(((FilteredCommerceItem) matchObj.getMatchingObject()).getCatalogRefId());
			if (qty == null) {
				qty = matchObj.getQuantity();
			} else {
				qty = qty + matchObj.getQuantity();
			}
				target.put(((FilteredCommerceItem) matchObj.getMatchingObject()).getCatalogRefId(), qty);
				targetItem.put(pPricingContext.getPricingModel().getRepositoryId(), target);
			if(isLoggingDebug()){
				vlogDebug("targetItem : {0}", targetItem);
			}
			pExtraParametersMap.put(TRUConstants.TARGET_ITEM,targetItem);
			if (((Collection) matchingObject).size() == i+ TRUConstants.INTEGER_NUMBER_ONE &&
					pPricingContext.getPricingModel().getRepositoryId().equals(getPromotionId())) {
				qualifyItem = (Map<String, Map<String, Long>>) pExtraParametersMap.get(TRUConstants.QUALIFIED_ITEM);
				if (qualifyItem == null) {
					qualifyItem = new HashMap<String, Map<String, Long>>();
				}
				qualify = qualifyItem.get(pPricingContext.getPricingModel().getRepositoryId());
				if (qualify == null) {
					qualify = new HashMap<String, Long>();
				}
				Long qualifyQty = TRUConstants.LONG_ZERO;
				for (Map.Entry<String, Long> entry : getQualifiedItem().entrySet()) {
					qualifyQty = qualify.get(entry.getKey());
					if (qualifyQty == null) {
						qualifyQty = entry.getValue();
					} else {
						qualifyQty = qualifyQty + entry.getValue();
					}
					qualify.put(entry.getKey(), qualifyQty);
				}
				qualifyItem.put(getPromotionId(), qualify);
				if(isLoggingDebug()){
					vlogDebug("qualifyItem : {0}", qualifyItem);
				}
				pExtraParametersMap.put(TRUConstants.QUALIFIED_ITEM,qualifyItem);
			}
			i++;
		}
		setQualifiedItem(null);
		if (isLoggingDebug()) {
			logDebug("END : TRUQualifier evaluateTarget()");
		}
		return matchingObject;
	}
	
	/**
	 * Holds reference of mPromotionId.
	 */
	private List<Integer> mPromotionType;
	/**
	 * Holds reference of mOtherItemType.
	 */
	private List<String> mOtherItemType;

	/**
	 * @return the promotionType
	 */
	public List<Integer> getPromotionType() {
		return mPromotionType;
	}

	/**
	 * @param pPromotionType the promotionType to set
	 */
	public void setPromotionType(List<Integer> pPromotionType) {
		mPromotionType = pPromotionType;
	}

	/**
	 * @return the otherItemType
	 */
	public List<String> getOtherItemType() {
		return mOtherItemType;
	}

	/**
	 * @param pOtherItemType the otherItemType to set
	 */
	public void setOtherItemType(List<String> pOtherItemType) {
		mOtherItemType = pOtherItemType;
	}
	
	/**
	 * This method has been overridden to qualify the Shipping promotions by excluding items 
	 * amount from the Order(Only for Spend  X get Shipping Discount).
	 * 
	 * @param pPricingContext - the PricingContext
	 * @param pExtraParametersMap - Map of ExtraParameters
	 * @throws PricingException - the PricingException
	 * @return the MatchingObject
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public MatchingObject findQualifyingShipping(
			PricingContext pPricingContext, Map pExtraParametersMap)
			throws PricingException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUQualifier  method: findQualifyingShipping]");
			vlogDebug("pPricingContext : {0} pExtraParametersMap : {1}", pPricingContext,pExtraParametersMap);
		}
		if ((pPricingContext == null) || (pPricingContext.getShippingGroup() == null)) {
	        throw new PricingException(Constants.NO_SHIPPING);
	     }
		if (pPricingContext.getPricingModel() == null) {
			throw new PricingException(MessageFormat.format(Constants.NO_PRICING_MODEL, new Object[TRUConstants.INT_ZERO]));
		}
		OrderPriceInfo orderPriceQuote = pPricingContext.getOrderPriceQuote();
		if(orderPriceQuote == null){
			return super.findQualifyingShipping(pPricingContext, pExtraParametersMap);
		}
		MatchingObject matchObject = null;
		TRUPromotionTools promotionTools = (TRUPromotionTools) getPromotionTools();
		TRUOrderTools orderTools = (TRUOrderTools) promotionTools.getPricingTools().getOrderTools();
		ShippingGroup shippingGroup = pPricingContext.getShippingGroup();
		// Added check to fix the defect - TUW-64676
		if(shippingGroup instanceof TRUInStorePickupShippingGroup){
			return matchObject;
		}
		RepositoryItem pricingModel = pPricingContext.getPricingModel();
		Order order = pPricingContext.getOrder();
		// Added check to fix the defect - TSJ-9293
		boolean isOnlyDonationItem=false;
		List<CommerceItem> items=order.getCommerceItems();
		if(items.size()>TRUConstants.INT_ZERO&&items.size()==TRUConstants.INTEGER_NUMBER_ONE){
			CommerceItem item=items.get(TRUConstants.INT_ZERO);
			if(item instanceof TRUDonationCommerceItem){
				isOnlyDonationItem=true;
			}
		}
		if(isOnlyDonationItem){
			return matchObject;
		}
		boolean isPromoItem = Boolean.FALSE;
		try {
			isPromoItem = promotionTools.isPromotionItem(pricingModel);
		} catch (IllegalArgumentException iaexcp) {
			if (isLoggingError()) {
				vlogError("IllegalArgumentException: while checking promotion item pricingModel : {0} with exception : {1}",
						pricingModel,iaexcp);
			}
		} catch (RepositoryException repoExcp) {
			if (isLoggingError()) {
				vlogError("RepositoryException: while checking promotion item pricingModel : {0} with exception : {1}",
						pricingModel,repoExcp);
			}
		}
		if(isPromoItem){
			// Getting Qualifier SKU from promotion.
			List<RepositoryItem> qualifiedSKUforPromo=new ArrayList<RepositoryItem>();
			List<RepositoryItem> excludedSKUforPromo=new ArrayList<RepositoryItem>();
			promotionTools.getQualifiedSKUforPromoFromCatalog(order,pricingModel,qualifiedSKUforPromo,excludedSKUforPromo);
			// Getting Qualifier Bulk SKU from promotion.
			List<String> qualifyBulkSkuIds = promotionTools.getQualifyBulkSkuIds(pricingModel);
			// Getting Excluded SKU from promotion.
			//List<RepositoryItem> excludedSKUforPromo = promotionTools.getExcludedSKUforPromo(pricingModel);
			// Getting Excluded Bulk SKU from promotion.
			List<String> excludedBulkSkuIds = promotionTools.getExcludedBulkSkuIds(pricingModel);
			// Getting non eligible items amount.
			//promotionTools.getIncludedSkusFromProductsAndCategoreis(order, pricingModel, qualifiedSKUforPromo);
			Double nonEligibleItemAmount =  orderTools.getNonEligibleItemsAmount(order,shippingGroup,qualifiedSKUforPromo,
					qualifyBulkSkuIds,excludedSKUforPromo,excludedBulkSkuIds);
			if(nonEligibleItemAmount <= 0){
				matchObject = super.findQualifyingShipping(pPricingContext, pExtraParametersMap);
			}else{
				Double orderOriginalAmt = orderPriceQuote.getAmount();
				orderPriceQuote.setAmount(orderOriginalAmt-nonEligibleItemAmount);
				pPricingContext.setOrderPriceQuote(orderPriceQuote);
				matchObject = super.findQualifyingShipping(pPricingContext, pExtraParametersMap);
				orderPriceQuote.setAmount(orderOriginalAmt);
				pPricingContext.setOrderPriceQuote(orderPriceQuote);
			}
		}else{
			matchObject = super.findQualifyingShipping(pPricingContext, pExtraParametersMap);
		}
		if(isLoggingDebug()){
			vlogDebug("Qualified Order Item : {0}", matchObject);
			logDebug("END : TRUQualifier findQualifyingShipping()");
		}
		return matchObject;
	}
	
	/**
	 * Method to get the Discountable amount.
	 * 
	 * @param pOrder - Order Object
	 * @param pExtraParametersMap - Map
	 * @param pPricingContext - PricingContext Object
	 * @param pQualifiedSKUforPromo - List of Qualified SKU Repository Items.
	 * @param pQualifyBulkSkuIds - List of Qualified SKU ids.
	 * @param pExcludedSKUforPromo - List of Excluded SKU Repository Items.
	 * @param pExcludedBulkSkuIds -List of Excluded SKU ids.
	 * @return double - discountable amount.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private double getDiscountableTotal(Order pOrder, Map pExtraParametersMap,
			PricingContext pPricingContext,
			List<RepositoryItem> pQualifiedSKUforPromo,
			List<String> pQualifyBulkSkuIds,
			List<RepositoryItem> pExcludedSKUforPromo, List<String> pExcludedBulkSkuIds) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUQualifier  method: getDiscountableTotal]");
			vlogDebug("pOrder : {0} pExtraParametersMap : {1} pPricingContext : {2} pExcludedBulkSkuIds : {3}", 
					pOrder,pExtraParametersMap,pPricingContext,pExcludedBulkSkuIds);
			vlogDebug("pQualifiedSKUforPromo : {0} pQualifyBulkSkuIds : {1} pExcludedSKUforPromo : {2}", 
					pQualifiedSKUforPromo,pQualifyBulkSkuIds,pExcludedSKUforPromo);
		}
		Double discountableTotal = TRUConstants.DOUBLE_ZERO;
		if(pOrder == null || pPricingContext == null || pPricingContext.getItems() == null){
			if(isLoggingDebug()){
				vlogDebug("nonDiscountableTotal : {0}", discountableTotal);
				logDebug("END : TRUQualifier getNonDiscountableTotal()");
			}
			return discountableTotal;
		}
		List<CommerceItem> commerceItems = pOrder.getCommerceItems();
		if(commerceItems == null || commerceItems.isEmpty()){
			if(isLoggingDebug()){
				vlogDebug("nonDiscountableTotal : {0}", discountableTotal);
				logDebug("END : TRUQualifier getNonDiscountableTotal()");
			}
			return discountableTotal;
		}
		FilteredCommerceItem commerceItem = null;
		for (Object obj : pPricingContext.getItems()) {
			commerceItem = (FilteredCommerceItem)obj;
			// Calling method to check item is eligible for Proration or not.
			((TRUItemPriceInfo)commerceItem.getPriceInfo()).setExcludedForOrderDiscount(
					isItemExcludedForOrderDisccount(commerceItem,pQualifiedSKUforPromo,
							pQualifyBulkSkuIds,pExcludedSKUforPromo,pExcludedBulkSkuIds));
		}
		CommerceItem ci = null;
		TRUItemPriceInfo itemPriceInfo = null;
		Iterator itemIterator = commerceItems.iterator();
		while (itemIterator.hasNext()) {
			ci = (CommerceItem)itemIterator.next();
			itemPriceInfo = (TRUItemPriceInfo) ci.getPriceInfo();
			if(itemPriceInfo == null){
				continue;
			}
			if(!itemPriceInfo.isExcludedForOrderDiscount()){
				discountableTotal = Double.valueOf(discountableTotal.doubleValue() + itemPriceInfo.getAmount());
			}
		}
		if(isLoggingDebug()){
			vlogDebug("nonDiscountableTotal : {0}", discountableTotal);
			logDebug("END : TRUQualifier getDiscountableTotal()");
		}
		return discountableTotal;
	}
}
