package com.tru.commerce.pricing;

import java.util.Iterator;

import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.ShippingPriceInfo;
import atg.repository.RepositoryItem;

import com.tru.common.TRUConstants;

/**
 * @author Professional Access
 * @version 1.0
 * This TRUShippingPriceInfo extended to add custom properties to ShippingPriceInfo.
 */
public class TRUShippingPriceInfo extends ShippingPriceInfo {

	/** serialVersionUID of type long. */
	private static final long serialVersionUID = 1L;

	/**
	 * Holds shipping surcharge for shipping group.
	 */
	private double mShippingSurcharge;

	/**
	 * This method will return mshippingSurcharge.
	 *
	 * @return the mshippingSurcharge
	 */
	public double getShippingSurcharge() {
		return mShippingSurcharge;
	}

	/**
	 * This method will set mshippingSurcharge with pShippingSurcharge.
	 *
	 * @param pShippingSurcharge the mshippingSurcharge to set
	 */
	public void setShippingSurcharge(double pShippingSurcharge) {
		mShippingSurcharge = pShippingSurcharge;
	}
	
	/**
	 * This method will return the total adjusted amount through promotions.
	 * 
	 * @return discounts - Discounted amount through promotions.
	 */
	@SuppressWarnings("unchecked")
	public double getDiscountedAmount() {
		double discounts = TRUConstants.DOUBLE_ZERO;
		if (getAdjustments() != null && !getAdjustments().isEmpty()) {
			for (
					Iterator<PricingAdjustment> iterator = getAdjustments().iterator(); iterator.hasNext();) {
				PricingAdjustment pricingAdjustment = iterator.next();
				if (pricingAdjustment.getPricingModel() != null) {
					discounts += pricingAdjustment.getTotalAdjustment();
				}
			}
		}
		return -discounts;
	}
	
	/**
	 * This method will return the total adjusted amount through each promotions.
	 * @param pPricingModel  - Promotion
	 * 
	 * @return discounts - Discounted amount through promotions.
	 */
	@SuppressWarnings("unchecked")
	public double getDiscountedAmountForPromotion(RepositoryItem pPricingModel) {
		double discounts = TRUConstants.DOUBLE_ZERO;
		if (getAdjustments() != null && !getAdjustments().isEmpty() && pPricingModel != null) {
			String promotionID = pPricingModel.getRepositoryId();
			for (Iterator<PricingAdjustment> iterator = getAdjustments().iterator(); iterator.hasNext();) {
				PricingAdjustment pricingAdjustment = iterator.next();
				if (pricingAdjustment.getPricingModel() != null && promotionID.equals(pricingAdjustment.getPricingModel().getRepositoryId())) {
					discounts += pricingAdjustment.getTotalAdjustment();
				}
			}
		}
		return -discounts;
	}

}
