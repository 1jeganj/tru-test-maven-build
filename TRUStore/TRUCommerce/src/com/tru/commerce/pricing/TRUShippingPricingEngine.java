package com.tru.commerce.pricing;

import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.ShippingPricingEngineImpl;
import atg.core.util.Address;
import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;

import com.tru.commerce.order.TRUShippingManager;
import com.tru.commerce.order.purchase.TRUShippingProcessHelper;
import com.tru.commerce.order.utils.TRURegionConstants;
import com.tru.common.TRUConstants;

/**
 * The Class TRUShippingPricingEngine.
 *
 * @author Professional Access
 * @version 1.0
 * 
 * This class is overridden to get the available shipping methods from shipping
 * repository.
 */
public class TRUShippingPricingEngine extends ShippingPricingEngineImpl {
	
	/** shippingProcessHelper. */
	private TRUShippingProcessHelper mShippingHelper;
	
	/**
	 * Gets the shipping helper.
	 *
	 * @return the shippingHelper
	 */
	public TRUShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * Sets the shipping helper.
	 *
	 * @param pShippingHelper the shippingHelper to set
	 */
	public void setShippingHelper(
			TRUShippingProcessHelper pShippingHelper) {
		this.mShippingHelper = pShippingHelper;
	}
	
	/**
	 * This methods gets the available shipping method based on cart items and retrieve the prices based on the region.
	 * This logic would be invoked in the following cases.
	 * 1 . Loading the shipping page with no address/saved address.
	 * 2 . When customer changes address1,state form values on shipping page.
	 * @param pShipment shipment
	 * @param pPricingModels pricingModels
	 * @param pLocale locale
	 * @param pProfile profile 
	 * @param pExtraParameters extraParameters 
	 * @return list
	 * @throws PricingException PricingException
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public List getAvailableMethods(ShippingGroup pShipment, Collection pPricingModels, Locale pLocale, RepositoryItem pProfile,
			Map pExtraParameters) throws PricingException {
		if (isLoggingDebug()) {
			logDebug("Start TRUShippingPricingEngine.getAvailableMethods(");
		}
		final TRUShippingManager shippingManager = getShippingHelper().getPurchaseProcessHelper().getShippingManager();
		final Order order = (Order) pExtraParameters.get(TRUConstants.ORDER_PARAM);
		final String state = (String) pExtraParameters.get(TRUConstants.STATE_PARAM);
		final String address1 = (String) pExtraParameters.get(TRUConstants.ADDRESS1_PARAM);
		final String city = (String) pExtraParameters.get(TRUConstants.CITY);
		String region = TRURegionConstants.LOWER48.getRegion();

		String isNewAddressForm = (String) pExtraParameters.get(TRUConstants.IS_NEW_ADDRESS_FORM);

		if (StringUtils.isNotBlank(isNewAddressForm) && TRUConstants.TRUE.equalsIgnoreCase(isNewAddressForm)) {
			final Address address = new Address();
			address.setAddress1(address1);
			address.setState(state);
			address.setCity(city);
			region = shippingManager.getShippingRegion(address);
		} else {
			region = shippingManager.getShippingRegion((Address) pExtraParameters.get(TRUConstants.ADDRESS_PARAM));
		}		
		List<TRUShipMethodVO> applicableShipMethods = shippingManager.getShippingMethods(order,region,false);
		if (isLoggingDebug()) {
			vlogInfo("TRUShippingPricingEngine.getAvailableMethods():applicableShipMethods \n {0}", applicableShipMethods);
			vlogDebug("Shipmethod retrieved for {0}" , region);
			logDebug("End TRUShippingPricingEngine.getAvailableMethods()");
		}		
		return applicableShipMethods;
	}
	
}
