package com.tru.commerce.pricing;

import atg.commerce.pricing.OrderPriceInfo;

import com.tru.common.TRUConstants;

/**
 * This TRUOrderPriceInfo extended to add custom properties to OrderPriceInfo.
 *  @author PA
 *  @version 1.0
 */
public class TRUOrderPriceInfo extends OrderPriceInfo {

	/**
	 * serialVersionUID of type long.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Holds total saved amount.
	 */
	private double mTotalSavedAmount;

	/**
	 * Holds total saved percentage.
	 */
	private double mTotalSavedPercentage;

	/**
	 * Holds total shipping surcharge.
	 */
	private double mTotalShippingSurcharge;
	
	/** mEwasteFees Fees. */
	private double mEwasteFees;
	
	
	/** Total items list price Fees. */
	private double mTotalListPrice;
	
	/** Gift Wrap Amount. */
	private double mGiftWrapPrice;
	
	/** Item level Savings. */
	private double mTotalItemLevelSavings;
	
	/** Item level Savings Percentage. */
	private double mTotalItemLevelSavingsPercentage;
	/** Item level Savings Percentage. */
	private double mTotalRawShipping;

	
	/**
	 * This method will return mTotalRawShipping.
	 *
	 * @return the mTotalRawShipping.
	 */
	public double getTotalRawShipping() {
		return mTotalRawShipping;
	}
	/**
	 * This method will set mTotalRawShipping with pTotalRawShipping.
	 *
	 * @param pTotalRawShipping the mTotalRawShipping to set.
	 */
	public void setTotalRawShipping(double pTotalRawShipping) {
		this.mTotalRawShipping = pTotalRawShipping;
	}

	/**
	 * This method will return totalSavedAmount.
	 *
	 * @return the totalSavedAmount.
	 */
	public double getTotalSavedAmount() {
		return mTotalSavedAmount;
	}

	/**
	 * This method will set mtotalSavedAmount with pTotalSavedAmount.
	 *
	 * @param pTotalSavedAmount the mtotalSavedAmount to set.
	 */
	public void setTotalSavedAmount(double pTotalSavedAmount) {
		mTotalSavedAmount = pTotalSavedAmount;
	}

	/**
	 * This method will return totalSavedPercentage.
	 *
	 * @return the totalSavedPercentage.
	 */
	public double getTotalSavedPercentage() {
		return mTotalSavedPercentage;
	}

	/**
	 * This method will set mtotalSavedPercentage with pTotalSavedPercentage.
	 *
	 * @param pTotalSavedPercentage the mtotalSavedPercentage to set.
	 */
	public void setTotalSavedPercentage(double pTotalSavedPercentage) {
		mTotalSavedPercentage = pTotalSavedPercentage;
	}

	/**
	 * This method will return totalShippingSurcharge.
	 *
	 * @return the totalShippingSurcharge.
	 */
	public double getTotalShippingSurcharge() {
		return mTotalShippingSurcharge;
	}

	/**
	 * This method will set mtotalShippingSurcharge with pTotalShippingSurcharge.
	 *
	 * @param pTotalShippingSurcharge the mtotalShippingSurcharge to set.
	 */
	public void setTotalShippingSurcharge(double pTotalShippingSurcharge) {
		mTotalShippingSurcharge = pTotalShippingSurcharge;
	}

	/**
	 * This method gets the order Total amount.
	 * 
	 * @return the order Total amount.
	 */
	@Override
	public double getTotal() {
		return getAmount() + getShipping() + getTotalShippingSurcharge() + getTax() + getEwasteFees()+ getGiftWrapPrice();
	}

	/**
	 * This method gets the order Total savings by including discount amount.
	 * 
	 * @return the order total savings amount.
	 */
	public double getTotalSavings() {
		return getTotalSavedAmount() + getDiscountAmount();
	}

	/**
	 * This method gets the order Total savings percentage.
	 * 
	 * @return the order total savings percentage.
	 */
	public double getTotalSavingsPercentage(){
		double calculateSaving=getTotalListPrice()+getTotalRawShipping()+ getTotalShippingSurcharge() + 
				getTax() + getEwasteFees() + getGiftWrapPrice();
		if (calculateSaving > TRUConstants.ZERO) {
			return Math.round((getTotalSavings() / (getTotalListPrice()+getTotalRawShipping()+ getTotalShippingSurcharge() +
					getTax() + getEwasteFees() + getGiftWrapPrice())) * TRUConstants.HUNDERED);	
		}
		return TRUConstants.ZERO;
	}
	
	/**
	 * This method will return mTotalListPrice.
	 *
	 * @return the mTotalListPrice.
	 */
	public double getTotalListPrice() {
		return mTotalListPrice;
	}
	/**
	 * This method will set mTotalListPrice with pTotalListPrice.
	 * @param pTotalListPrice the mTotalListPrice to set.
	 */
	public void setTotalListPrice(double pTotalListPrice) {
		mTotalListPrice = pTotalListPrice;
	}
	/**
	 * This method will return mGiftWrapPrice.
	 *
	 * @return the mGiftWrapPrice.
	 */
	public double getGiftWrapPrice() {
		return mGiftWrapPrice;
	}
	/**
	 * This method will set mGiftWrapPrice with pGiftWrapPrice.
	 * @param pGiftWrapPrice the mGiftWrapPrice to set.
	 */
	public void setGiftWrapPrice(double pGiftWrapPrice) {
		mGiftWrapPrice = pGiftWrapPrice;
	}

	/**
	 * @return the totalItemLevelSavings.
	 */
	public double getTotalItemLevelSavings() {
		return mTotalItemLevelSavings;
	}

	/**
	 * @param pTotalItemLevelSavings the totalItemLevelSavings to set.
	 */
	public void setTotalItemLevelSavings(double pTotalItemLevelSavings) {
		mTotalItemLevelSavings = pTotalItemLevelSavings;
	}

	/**
	 * @return the totalItemLevelSavingsPercentage.
	 */
	public double getTotalItemLevelSavingsPercentage() {
		return mTotalItemLevelSavingsPercentage;
	}

	/**
	 * @param pTotalItemLevelSavingsPercentage the totalItemLevelSavingsPercentage to set.
	 */
	public void setTotalItemLevelSavingsPercentage(
			double pTotalItemLevelSavingsPercentage) {
		mTotalItemLevelSavingsPercentage = pTotalItemLevelSavingsPercentage;
	}
	/**
	 * This method returns the ewasteFees value.
	 *
	 * @return the ewasteFees.
	 */
	public double getEwasteFees() {
		return mEwasteFees;
	}
	/**
	 * This method sets the ewasteFees with parameter value pEwasteFees.
	 *
	 * @param pEwasteFees the ewasteFees to set.
	 */
	public void setEwasteFees(double pEwasteFees) {
		mEwasteFees = pEwasteFees;
	}
}
