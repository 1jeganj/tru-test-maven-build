package com.tru.commerce.pricing;

import java.util.Iterator;
import java.util.List;

import atg.commerce.pricing.OrderPricingEngineImpl;
import atg.commerce.pricing.PricingException;
import atg.repository.RepositoryItem;
/**
 * The Class TRUOrderPricingEngineImpl.
 *
 * @author Professional Access
 * @version 1.0
 * 
 * This class is overridden to write the custom logic to met the TRU requirement.
 */
public class TRUOrderPricingEngineImpl extends OrderPricingEngineImpl{
	
	/**
	 * Overriding the OOTB method to skip the tag type promotion.
	 * 
	 * @return List - List of Global Promotion.
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected List findGlobalPromotions() {
		if (isLoggingDebug()) {
			logDebug("Start TRUOrderPricingEngineImpl.findGlobalPromotions(");
		}
		List allGlobalPromo = null;
		// Calling OOTB method to load the all global promotion.
		allGlobalPromo = super.findGlobalPromotions();
		if (isLoggingDebug()) {
			logDebug("All Order Promotion before removing TagPromo:" + allGlobalPromo);
		}
		if(allGlobalPromo == null || allGlobalPromo.isEmpty() ){
			return allGlobalPromo;
		}
		// Calling method to remove the tag promotion.
		allGlobalPromo = removeTagPromotion(allGlobalPromo);
		if (isLoggingDebug()) {
			logDebug("All Order Promotion :" + allGlobalPromo);
			logDebug("End TRUOrderPricingEngineImpl.findGlobalPromotions()");
		}
		return allGlobalPromo;
	}
	
	/**
	 * Overriding the OOTB method to skip the tag type promotion.
	 * @param pProfile - Profile Object
	 * @return List - List of User Promotion.
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public List getUserOnlyPromotions(RepositoryItem pProfile)
			throws PricingException {
		if (isLoggingDebug()) {
			logDebug("Start TRUOrderPricingEngineImpl.getUserOnlyPromotions(");
		}
		List userOnlyPromo = null;
		// Calling OOTB method to load the all global promotion.
		userOnlyPromo = super.getUserOnlyPromotions(pProfile);
		if (isLoggingDebug()) {
			logDebug("All User Order Promotion before removing TagPromo:" + userOnlyPromo);
		}
		if(userOnlyPromo == null || userOnlyPromo.isEmpty() ){
			return userOnlyPromo;
		}
		// Calling method to remove the tag promotion.
		userOnlyPromo = removeTagPromotion(userOnlyPromo);
		if (isLoggingDebug()) {
			logDebug("All User Order Promotion :" + userOnlyPromo);
			logDebug("End TRUOrderPricingEngineImpl.getUserOnlyPromotions()");
		}
		return userOnlyPromo;
	}
	
	/**
	 * Method to remove the tag type of promotion from list of all promotion.
	 * 
	 * @param pPromoList - List of Promotion Items.
	 * @return List - List of All Promotion except tag promotions.
	 */
	@SuppressWarnings("rawtypes")
	private List removeTagPromotion(List pPromoList) {
		if (isLoggingDebug()) {
			logDebug("Start TRUOrderPricingEngineImpl.removeTagPromotion(");
			logDebug("Promotion List Before Removing tag Promotion :" + pPromoList);
		}
		if(pPromoList == null || pPromoList.isEmpty() ){
			return pPromoList;
		}
		RepositoryItem promotionItem = null;
		TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties)getPricingModelProperties();
		for(Iterator iterator = pPromoList.iterator() ; iterator.hasNext();){
			Object obj = iterator.next();
			boolean isTagPromotion = Boolean.FALSE;
			if(obj instanceof RepositoryItem){
				promotionItem = (RepositoryItem) obj;
				if(promotionItem.getPropertyValue(pricingModelProperties.getTagPromotionPropertyName()) == null){
					continue;
				}
				isTagPromotion = (boolean) promotionItem.getPropertyValue(pricingModelProperties.getTagPromotionPropertyName());
				if(isLoggingDebug()){
					vlogDebug("promotionItem : {0} isTagPromotion : {1}" ,promotionItem,  isTagPromotion);
				}
				if(isTagPromotion){
					iterator.remove();
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Promotion List After Removing tag Promotion :" + pPromoList);
			logDebug("End TRUOrderPricingEngineImpl.removeTagPromotion()");
		}
		return pPromoList;
	}

	
	
}
