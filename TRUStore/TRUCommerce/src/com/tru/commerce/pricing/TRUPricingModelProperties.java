package com.tru.commerce.pricing;

import atg.commerce.pricing.PricingModelProperties;

/**
 *TRUPricingModelProperties extends PricingModelProperties.
 * @author Professional Access.
 * @version 1.0
 *
 */
public class TRUPricingModelProperties extends PricingModelProperties{
	/**
	 * Property to hold mPromotionDetailsPropertyName.
	 */
	private String mPromotionDetailsPropertyName;
	/**
	 * Property to hold mTenderAmountLimitPropertyName.
	 */
	private String mTenderAmountLimitPropertyName;
	/**
	 * Property to hold mTenderTypePropertyName.
	 */
	private String mTenderTypePropertyName;
	/**
	 * Property to hold mVendorFundedPropertyName.
	 */
	private String mVendorFundedPropertyName;
	/**
	 * Property to hold mTagPromotionPropertyName.
	 */
	private String mTagPromotionPropertyName;
	/**
	 * Property to hold mPromotionItemDescName.
	 */
	private String mPromotionItemDescName;
	/**
	 * Property to hold mCampaignIdPropertyName.
	 */
	private String mCampaignIdPropertyName;
	/**
	 * Property to hold mPromotionItemDescName.
	 */
	private String mPromotionPropertyName;
	/**
	 * Property to hold mDisplayPriorityPropertyName.
	 */
	private String mDisplayPriorityPropertyName;
	/**
	 * Property to hold mEnabledPropertyName.
	 */
	private String mEnabledPropertyName;
	/**
	 * Property to hold mTenderTypePromotionPropertyName.
	 */
	private String mTenderTypePromotionPropertyName;
	
	/** The m web store display property name. */
	private String mWebStoreDisplayPropertyName;
	
	/**
	 * @return the displayPriorityPropertyName
	 */
	public String getDisplayPriorityPropertyName() {
		return mDisplayPriorityPropertyName;
	}
	/**
	 * @param pDisplayPriorityPropertyName the displayPriorityPropertyName to set
	 */
	public void setDisplayPriorityPropertyName(String pDisplayPriorityPropertyName) {
		mDisplayPriorityPropertyName = pDisplayPriorityPropertyName;
	}
	/**
	 * @return the promotionPropertyName.
	 */
	public String getPromotionPropertyName() {
		return mPromotionPropertyName;
	}
	/**
	 * @param pPromotionPropertyName the promotionPropertyName to set.
	 */
	public void setPromotionPropertyName(String pPromotionPropertyName) {
		mPromotionPropertyName = pPromotionPropertyName;
	}
	/**
	 * Gets the PromotionDetailsPropertyName.
	 * @return the PromotionDetailsPropertyName.
	 */
	public String getPromotionDetailsPropertyName() {
		return mPromotionDetailsPropertyName;
	}
	/**
	 * Sets the PromotionDetailsPropertyName.
	 * @param pPromotionDetailsPropertyName the PromotionDetailsPropertyName to set.
	 */
	public void setPromotionDetailsPropertyName(
			String pPromotionDetailsPropertyName) {
		this.mPromotionDetailsPropertyName = pPromotionDetailsPropertyName;
	}
	/**
	 * Gets the TenderAmountLimitPropertyName.
	 * @return the TenderAmountLimitPropertyName.
	 */
	public String getTenderAmountLimitPropertyName() {
		return mTenderAmountLimitPropertyName;
	}
	/**
	 * Sets the TenderAmountLimitPropertyName.
	 * @param pTenderAmountLimitPropertyName the TenderAmountLimitPropertyName to set.
	 */
	public void setTenderAmountLimitPropertyName(
			String pTenderAmountLimitPropertyName) {
		this.mTenderAmountLimitPropertyName = pTenderAmountLimitPropertyName;
	}
	/**
	 * Gets the TenderTypePropertyName.
	 * @return the TenderTypePropertyName.
	 */
	public String getTenderTypePropertyName() {
		return mTenderTypePropertyName;
	}
	/**
	 * Sets the TenderTypePropertyName.
	 * @param pTenderTypePropertyName the TenderTypePropertyName to set.
	 */
	public void setTenderTypePropertyName(String pTenderTypePropertyName) {
		this.mTenderTypePropertyName = pTenderTypePropertyName;
	}
	/**
	 * Gets the VendorFundedPropertyName.
	 * @return the VendorFundedPropertyName.
	 */
	public String getVendorFundedPropertyName() {
		return mVendorFundedPropertyName;
	}
	/**
	 * Sets the VendorFundedPropertyName.
	 * @param pVendorFundedPropertyName the VendorFundedPropertyName to set.
	 */
	public void setVendorFundedPropertyName(String pVendorFundedPropertyName) {
		this.mVendorFundedPropertyName = pVendorFundedPropertyName;
	}
	/**
	 * Gets the TagPromotionPropertyName.
	 * @return the TagPromotionPropertyName.
	 */
	public String getTagPromotionPropertyName() {
		return mTagPromotionPropertyName;
	}
	/**
	 * Sets the TagPromotionPropertyName.
	 * @param pTagPromotionPropertyName the TagPromotionPropertyName to set.
	 */
	public void setTagPromotionPropertyName(String pTagPromotionPropertyName) {
		mTagPromotionPropertyName = pTagPromotionPropertyName;
	}
	/**
	 * Gets the PromotionItemDescName.
	 * @return the PromotionItemDescName.
	 */
	public String getPromotionItemDescName() {
		return mPromotionItemDescName;
	}
	/**
	 * Sets the PromotionItemDescName.
	 * @param pPromotionItemDescName the PromotionItemDescName to set.
	 */
	public void setPromotionItemDescName(String pPromotionItemDescName) {
		mPromotionItemDescName = pPromotionItemDescName;
	}
	/**
	 * @return the campaignIdPropertyName.
	 */
	public String getCampaignIdPropertyName() {
		return mCampaignIdPropertyName;
	}
	/**
	 * @param pCampaignIdPropertyName the campaignIdPropertyName to set.
	 */
	public void setCampaignIdPropertyName(String pCampaignIdPropertyName) {
		mCampaignIdPropertyName = pCampaignIdPropertyName;
	}
	/**
	 * @return the enabledPropertyName
	 */
	public String getEnabledPropertyName() {
		return mEnabledPropertyName;
	}
	/**
	 * @param pEnabledPropertyName the enabledPropertyName to set
	 */
	public void setEnabledPropertyName(String pEnabledPropertyName) {
		mEnabledPropertyName = pEnabledPropertyName;
	}
	/**
	 * Property to hold mQualifyingSkusPropertyName.
	 */
	private String mQualifyingSkusPropertyName;
	/**
	 * Property to hold mQualifyingProductsPropertyName.
	 */
	private String mQualifyingProductsPropertyName;
	/**
	 * Property to hold mQualifyingCategoriesPropertyName.
	 */
	private String mQualifyingCategoriesPropertyName;
	/**
	 * Property to hold mExcludedSkusPropertyName.
	 */
	private String mExcludedSkusPropertyName;
	/**
	 * Property to hold mExcludedProductsPropertyName.
	 */
	private String mExcludedProductsPropertyName;
	/**
	 * Property to hold mExcludedCategoriesPropertyName.
	 */
	private String mExcludedCategoriesPropertyName;
	/**
	 * Property to hold mBulkQualifyingSkusPropertyName.
	 */
	private String mBulkQualifyingSkusPropertyName;
	/**
	 * @return the qualifyingSkusPropertyName
	 */
	public String getQualifyingSkusPropertyName() {
		return mQualifyingSkusPropertyName;
	}
	/**
	 * @param pQualifyingSkusPropertyName the qualifyingSkusPropertyName to set
	 */
	public void setQualifyingSkusPropertyName(String pQualifyingSkusPropertyName) {
		mQualifyingSkusPropertyName = pQualifyingSkusPropertyName;
	}
	/**
	 * @return the qualifyingProductsPropertyName
	 */
	public String getQualifyingProductsPropertyName() {
		return mQualifyingProductsPropertyName;
	}
	/**
	 * @param pQualifyingProductsPropertyName the qualifyingProductsPropertyName to set
	 */
	public void setQualifyingProductsPropertyName(
			String pQualifyingProductsPropertyName) {
		mQualifyingProductsPropertyName = pQualifyingProductsPropertyName;
	}
	/**
	 * @return the qualifyingCategoriesPropertyName
	 */
	public String getQualifyingCategoriesPropertyName() {
		return mQualifyingCategoriesPropertyName;
	}
	/**
	 * @param pQualifyingCategoriesPropertyName the qualifyingCategoriesPropertyName to set
	 */
	public void setQualifyingCategoriesPropertyName(
			String pQualifyingCategoriesPropertyName) {
		mQualifyingCategoriesPropertyName = pQualifyingCategoriesPropertyName;
	}
	/**
	 * @return the excludedSkusPropertyName
	 */
	public String getExcludedSkusPropertyName() {
		return mExcludedSkusPropertyName;
	}
	/**
	 * @param pExcludedSkusPropertyName the excludedSkusPropertyName to set
	 */
	public void setExcludedSkusPropertyName(String pExcludedSkusPropertyName) {
		mExcludedSkusPropertyName = pExcludedSkusPropertyName;
	}
	/**
	 * @return the excludedProductsPropertyName
	 */
	public String getExcludedProductsPropertyName() {
		return mExcludedProductsPropertyName;
	}
	/**
	 * @param pExcludedProductsPropertyName the excludedProductsPropertyName to set
	 */
	public void setExcludedProductsPropertyName(String pExcludedProductsPropertyName) {
		mExcludedProductsPropertyName = pExcludedProductsPropertyName;
	}
	/**
	 * @return the excludedCategoriesPropertyName
	 */
	public String getExcludedCategoriesPropertyName() {
		return mExcludedCategoriesPropertyName;
	}
	/**
	 * @param pExcludedCategoriesPropertyName the excludedCategoriesPropertyName to set
	 */
	public void setExcludedCategoriesPropertyName(
			String pExcludedCategoriesPropertyName) {
		mExcludedCategoriesPropertyName = pExcludedCategoriesPropertyName;
	}
	/**
	 * @return the bulkQualifyingSkusPropertyName
	 */
	public String getBulkQualifyingSkusPropertyName() {
		return mBulkQualifyingSkusPropertyName;
	}
	/**
	 * @param pBulkQualifyingSkusPropertyName the bulkQualifyingSkusPropertyName to set
	 */
	public void setBulkQualifyingSkusPropertyName(
			String pBulkQualifyingSkusPropertyName) {
		mBulkQualifyingSkusPropertyName = pBulkQualifyingSkusPropertyName;
	}
	/**
	 * Property to hold mShipMethodPropertyName.
	 */
	private String mShipMethodPropertyName;
	/**
	 * @return the shipMethodPropertyName
	 */
	public String getShipMethodPropertyName() {
		return mShipMethodPropertyName;
	}
	/**
	 * @param pShipMethodPropertyName the shipMethodPropertyName to set
	 */
	public void setShipMethodPropertyName(String pShipMethodPropertyName) {
		mShipMethodPropertyName = pShipMethodPropertyName;
	}
	/**
	 * Property to hold mCouponBatchItemDescName.
	 */
	private String mCouponBatchItemDescName;
	/**
	 * Property to hold mBatchPromoClaimItemDescName.
	 */
	private String mBatchPromoClaimItemDescName;
	/**
	 * @return the couponBatchItemDescName
	 */
	public String getCouponBatchItemDescName() {
		return mCouponBatchItemDescName;
	}
	/**
	 * @param pCouponBatchItemDescName the couponBatchItemDescName to set
	 */
	public void setCouponBatchItemDescName(String pCouponBatchItemDescName) {
		mCouponBatchItemDescName = pCouponBatchItemDescName;
	}
	/**
	 * @return the batchPromoClaimItemDescName
	 */
	public String getBatchPromoClaimItemDescName() {
		return mBatchPromoClaimItemDescName;
	}
	/**
	 * @param pBatchPromoClaimItemDescName the batchPromoClaimItemDescName to set
	 */
	public void setBatchPromoClaimItemDescName(String pBatchPromoClaimItemDescName) {
		mBatchPromoClaimItemDescName = pBatchPromoClaimItemDescName;
	}
	
	/**
	 * Property to hold bulkExcludeSkusPropertyName.
	 */
	private String mBulkExcludeSkusPropertyName;
	/**
	 * @return the bulkExcludeSkusPropertyName
	 */
	public String getBulkExcludeSkusPropertyName() {
		return mBulkExcludeSkusPropertyName;
	}
	/**
	 * @param pBulkExcludeSkusPropertyName the bulkExcludeSkusPropertyName to set
	 */
	public void setBulkExcludeSkusPropertyName(String pBulkExcludeSkusPropertyName) {
		mBulkExcludeSkusPropertyName = pBulkExcludeSkusPropertyName;
	}
	/**
	 * @return the tenderTypePromotionPropertyName
	 */
	public String getTenderTypePromotionPropertyName() {
		return mTenderTypePromotionPropertyName;
	}
	/**
	 * @param pTenderTypePromotionPropertyName the tenderTypePromotionPropertyName to set
	 */
	public void setTenderTypePromotionPropertyName(String pTenderTypePromotionPropertyName) {
		mTenderTypePromotionPropertyName = pTenderTypePromotionPropertyName;
	}
	
	/**
	 * Gets the web store display property name.
	 *
	 * @return the web store display property name
	 */
	public String getWebStoreDisplayPropertyName() {
		return mWebStoreDisplayPropertyName;
	}
	
	/**
	 * Sets the web store display property name.
	 *
	 * @param pWebStoreDisplayPropertyName the new web store display property name
	 */
	public void setWebStoreDisplayPropertyName(String pWebStoreDisplayPropertyName) {
		this.mWebStoreDisplayPropertyName = pWebStoreDisplayPropertyName;
	}
	
}
