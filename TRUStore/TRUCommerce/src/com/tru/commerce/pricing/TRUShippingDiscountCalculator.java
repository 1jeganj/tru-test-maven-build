package com.tru.commerce.pricing;

import java.util.Locale;
import java.util.Map;

import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.ShippingDiscountCalculator;
import atg.commerce.pricing.ShippingPriceInfo;
import atg.repository.RepositoryItem;

import com.tru.common.TRUConstants;

/**
 * The Class TRUShippingDiscountCalculator.
 *
 * @author Professional Access
 * @version 1.0
 * TRUShippingDiscountCalculator extends the OOTB ShippingDiscountCalculator.
 * 
 * This Calculator is to update the total saving to order price info Object
 */
public class TRUShippingDiscountCalculator extends ShippingDiscountCalculator{
	
	/**
	 * This method will update the total saving amount to the order price info object.
	 * 
	 * @param pOrder
	 *            - Order Object
	 * @param pPriceQuote
	 *            - ShippingPriceInfo
	 * @param pShipment
	 *            - ShippingGroup
	 * @param pPricingModel
	 *            - Promotions
	 * @param pLocale
	 *            - Locale
	 * @param pProfile
	 *            - Profile
	 * @param pExtraParameters
	 *            - Map of extra parameters
	 * @throws PricingException - if any
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void priceShippingGroup(Order pOrder, ShippingPriceInfo pPriceQuote,
			ShippingGroup pShipment, RepositoryItem pPricingModel,
			Locale pLocale, RepositoryItem pProfile, Map pExtraParameters)
			throws PricingException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUShippingDiscountCalculator  method: priceShippingGroup]");
			vlogDebug(
					"pOrder: {0} Shipping price info : {1} ShippingGroup : {2} Pricing Model : {3} Locale : {4} Profile : {5} Extra parameters : {6}",
					pOrder,
					pPriceQuote,
					pShipment,
					pPricingModel,
					pLocale,
					pProfile,
					pExtraParameters);
		}
		// Calling OOTB method
		super.priceShippingGroup(pOrder, pPriceQuote, pShipment, pPricingModel,
				pLocale, pProfile, pExtraParameters);
		
		if(pOrder != null && pShipment != null){
			double totalSavedAmount = TRUConstants.DOUBLE_ZERO;
			final TRUOrderPriceInfo orderPriceInfo = (TRUOrderPriceInfo) pOrder.getPriceInfo();
			final TRUShippingPriceInfo shipPriceInfo = (TRUShippingPriceInfo) pPriceQuote;
			if(orderPriceInfo != null && shipPriceInfo != null){
				totalSavedAmount = orderPriceInfo.getTotalSavedAmount();
				totalSavedAmount += shipPriceInfo.getDiscountedAmountForPromotion(pPricingModel);
				//Setting the total saved amount to the order price info
				orderPriceInfo.setTotalSavedAmount(totalSavedAmount);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUShippingDiscountCalculator  method: priceShippingGroup]");
		}
	}
}
