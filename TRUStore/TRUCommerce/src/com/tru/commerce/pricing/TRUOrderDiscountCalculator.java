package com.tru.commerce.pricing;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.pricing.OrderDiscountCalculator;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.PricingException;
import atg.commerce.promotion.TRUPromotionTools;
import atg.repository.RepositoryItem;

import com.tru.common.TRUConstants;

/**
 * The Class TRUOrderDiscountCalculator.
 *
 * @author Professional Access
 * @version 1.0
 * TRUOrderDiscountCalculator extends the OOTB OrderDiscountCalculator.
 * 
 * This Calculator is to update the total saving to order price info Object
 */
public class TRUOrderDiscountCalculator extends OrderDiscountCalculator{
	
	/** Constant for DISCOUNTABLE_MAP. */
	public static final String DISCOUNTABLE_MAP = "discountableMap";
	/**
	 * Holds reference of List of RepositoryItem.
	 */
	private List<RepositoryItem> mQualifySKUsforPromo; 
	/**
	 * Holds reference of List of RepositoryItem.
	 */
	private List<RepositoryItem> mExcludedSKUsforPromo; 
	/**
	 * Holds reference of List of SKU ids.
	 */
	private List<String> mQualifyBulkSkuIds;

	/**
	 * @return the qualifySKUsforPromo
	 */
	public List<RepositoryItem> getQualifySKUsforPromo() {
		return mQualifySKUsforPromo;
	}

	/**
	 * @param pQualifySKUsforPromo the qualifySKUsforPromo to set
	 */
	public void setQualifySKUsforPromo(List<RepositoryItem> pQualifySKUsforPromo) {
		mQualifySKUsforPromo = pQualifySKUsforPromo;
	}

	/**
	 * @return the excludedSKUsforPromo
	 */
	public List<RepositoryItem> getExcludedSKUsforPromo() {
		return mExcludedSKUsforPromo;
	}

	/**
	 * @param pExcludedSKUsforPromo the excludedSKUsforPromo to set
	 */
	public void setExcludedSKUsforPromo(List<RepositoryItem> pExcludedSKUsforPromo) {
		mExcludedSKUsforPromo = pExcludedSKUsforPromo;
	}

	/**
	 * @return the qualifyBulkSkuIds
	 */
	public List<String> getQualifyBulkSkuIds() {
		return mQualifyBulkSkuIds;
	}

	/**
	 * @param pQualifyBulkSkuIds the qualifyBulkSkuIds to set
	 */
	public void setQualifyBulkSkuIds(List<String> pQualifyBulkSkuIds) {
		mQualifyBulkSkuIds = pQualifyBulkSkuIds;
	}

	/**
	 * This method will used to update the order price info object.
	 * 
	 * @param pPriceQuote
	 *            - OrderPriceInfo.
	 * @param pOrder
	 *            - Order.
	 * @param pPricingModel
	 *            - Promotions.
	 * @param pLocale
	 *            - Locale.
	 * @param pProfile
	 *            - Profile.
	 * @param pExtraParameters
	 *            - Map of extra parameters.
	 *@throws PricingException
	 *			-if any.        
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void priceOrder(OrderPriceInfo pPriceQuote, Order pOrder,
			RepositoryItem pPricingModel, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters)
			throws PricingException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOrderDiscountCalculator  method: priceOrder]");
			vlogDebug("pPriceQuote : {0} pOrder : {1} pPricingModel : {2}", pPriceQuote,pOrder,pPricingModel);
			vlogDebug("pProfile : {0} pExtraParameters : {1} pLocale : {2}", pProfile,pExtraParameters,pLocale);
		}
		TRUPromotionTools promotionTools = (TRUPromotionTools) getPromotionTools();
		// Setting the Qualify SKU Items
		setQualifySKUsforPromo(promotionTools.getQualifiedSKUforPromo(pPricingModel));
		// Setting the Excluded SKU Items
		setExcludedSKUsforPromo(promotionTools.getExcludedSKUforPromo(pPricingModel));
		// Setting the Qualify SKU Ids.
		setQualifyBulkSkuIds(promotionTools.getQualifyBulkSkuIds(pPricingModel));
		// Calling Super Method.
		setOldOrderAmt(pPriceQuote.getAmount());
		super.priceOrder(pPriceQuote, pOrder, pPricingModel, pLocale, pProfile,
				pExtraParameters);
		// Setting back to null;
		setQualifySKUsforPromo(null);
		setExcludedSKUsforPromo(null);
		setQualifyBulkSkuIds(null);
		setOldOrderAmt(TRUConstants.DOUBLE_ZERO);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOrderDiscountCalculator  method: priceOrder]");
		}
	}
	
	/**
	 * This method will used to get the non discountable amount..
	 * 
	 * @param pOrder
	 *            - Order.
	 * @param pExtraParametersMap
	 *            - Map of extra parameters.  
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public double getNonDiscountableTotal(Order pOrder, Map pExtraParametersMap) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOrderDiscountCalculator  method: getNonDiscountableTotal]");
			vlogDebug("pOrder : {0} pExtraParametersMap : {1}", pOrder,pExtraParametersMap);
		}
		Double nonDiscountableTotal = Double.valueOf(TRUConstants.DOUBLE_ZERO);
		if(pOrder == null && isLoggingDebug()){
			if (isLoggingDebug()) {
				vlogDebug("TRUOrderDiscountCalculator.nonDiscountableTotal : {0}", nonDiscountableTotal);
			}
			return nonDiscountableTotal;
		}
		double oldOrderAmt = getOldOrderAmt();
		Double discountableTotal = Double.valueOf(TRUConstants.DOUBLE_ZERO);
		// Getting all the commerce items from Order.
	    List commerceItems = pOrder.getCommerceItems();
	    // Iterating all the commerce Items.
	    Iterator itemIterator = commerceItems.iterator();
	    TRUItemPriceInfo priceInfo = null;
	    while (itemIterator.hasNext()) {
	      String keyToMap = TRUConstants.EMPTY;
	      CommerceItem ci = (CommerceItem)itemIterator.next();
	      String productId = ci.getAuxiliaryData().getProductId();
	      String skuId = ci.getCatalogRefId();
	      keyToMap = productId + skuId;
	      priceInfo = (TRUItemPriceInfo) ci.getPriceInfo();
	      if (pExtraParametersMap != null) {
	        Map previousOrder = (Map)pExtraParametersMap.get(DISCOUNTABLE_MAP);
	        if (previousOrder != null) {
	          if ((previousOrder.containsKey(keyToMap)) && 
	            (!(((Boolean)previousOrder.get(keyToMap)).booleanValue()))) {
	            nonDiscountableTotal = Double.valueOf(nonDiscountableTotal.doubleValue() + priceInfo.getAmount());
	            continue;
	          }
	        }
	        else if (!(ci.getPriceInfo().getDiscountable())) {
	            nonDiscountableTotal = Double.valueOf(nonDiscountableTotal.doubleValue() + priceInfo.getAmount());
	            continue;
	        }
	      }
	      else if (!(ci.getPriceInfo().getDiscountable())) {
	        nonDiscountableTotal = Double.valueOf(nonDiscountableTotal.doubleValue() + priceInfo.getAmount());
	        continue;
	      }
	      if(!priceInfo.isExcludedForOrderDiscount()){
				discountableTotal = Double.valueOf(discountableTotal.doubleValue() + priceInfo.getAmount());
		  }
	    }
	    TRUPricingTools pricingTools = (TRUPricingTools) getPricingTools();
	    // Calling Method to exclude the BPP amount from discount calculation.
	    nonDiscountableTotal = nonDiscountableTotal+pricingTools.getBPPItemsAmount(pOrder);
	    double returnValue = TRUConstants.DOUBLE_ZERO;
	    if(oldOrderAmt >= discountableTotal){
	    	returnValue = oldOrderAmt-discountableTotal;
	    }else{
	    	returnValue = TRUConstants.DOUBLE_ZERO;
	    }
	    if (isLoggingDebug()) {
			vlogDebug("returnValue : {0}", returnValue);
			logDebug("Exit from [Class: TRUOrderDiscountCalculator  method: getNonDiscountableTotal]");
		}
	    return returnValue;
	}
	
	/**
	 * Holds reference of Old Order Amount before Discount.
	 */
	private double mOldOrderAmt;

	/**
	 * @return the oldOrderAmt
	 */
	public double getOldOrderAmt() {
		return mOldOrderAmt;
	}

	/**
	 * @param pOldOrderAmt the oldOrderAmt to set
	 */
	public void setOldOrderAmt(double pOldOrderAmt) {
		mOldOrderAmt = pOldOrderAmt;
	}
	
}
