package com.tru.commerce.pricing;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * This class overridden to get the available shipping methods.
 * @author PA.
 * @version 1.0
 */
public class TRUAvailableShippingMethodsForMultiShipDroplet extends DynamoServlet {
	/**
	 * This method is to get available shipping methods.
	 * @param pReq - DynamoHttpServletRequest.
	 * @param pRes - DynamoHttpServletResponse.
	 * @throws ServletException - if any.
	 * @throws IOException - if any.
	 */
	@Override
	public void service(DynamoHttpServletRequest pReq,
			DynamoHttpServletResponse pRes) throws ServletException, IOException {
		super.service(pReq, pRes);
	}
}
