package com.tru.commerce.pricing;

/**
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUShipRegionVO {

	/** The m region name. */
	private String mRegionName;
	
	/** The m order cut off. */
	private double mOrderCutOff;
	
	/** The m ship min price. */
	private double mShipMinPrice;
	
	/** The m ship max price. */
	private double mShipMaxPrice;
	
	/** The m delivery min days. */
	private int mDeliveryMinDays;
	
	/** The m delivery max days. */
	private int mDeliveryMaxDays;
	
	/** The m time in transit min days. */
	private int mTimeInTransitMinDays;
	
	/** The m time in transit max days. */
	private int mTimeInTransitMaxDays;
	
	/**
	 * Gets the region name.
	 *
	 * @return the regionName
	 */
	public String getRegionName() {
		return mRegionName;
	}
	
	/**
	 * Sets the region name.
	 *
	 * @param pRegionName the regionName to set
	 */
	public void setRegionName(String pRegionName) {
		this.mRegionName = pRegionName;
	}
	
	/**
	 * Gets the order cut off.
	 *
	 * @return the orderCutOff
	 */
	public double getOrderCutOff() {
		return mOrderCutOff;
	}
	
	/**
	 * Sets the order cut off.
	 *
	 * @param pOrderCutOff the orderCutOff to set
	 */
	public void setOrderCutOff(double pOrderCutOff) {
		this.mOrderCutOff = pOrderCutOff;
	}
	
	/**
	 * Gets the ship min price.
	 *
	 * @return the shipMinPrice
	 */
	public double getShipMinPrice() {
		return mShipMinPrice;
	}
	
	/**
	 * Sets the ship min price.
	 *
	 * @param pShipMinPrice the shipMinPrice to set
	 */
	public void setShipMinPrice(double pShipMinPrice) {
		this.mShipMinPrice = pShipMinPrice;
	}
	
	/**
	 * Gets the ship max price.
	 *
	 * @return the shipMaxPrice
	 */
	public double getShipMaxPrice() {
		return mShipMaxPrice;
	}
	
	/**
	 * Sets the ship max price.
	 *
	 * @param pShipMaxPrice the shipMaxPrice to set
	 */
	public void setShipMaxPrice(double pShipMaxPrice) {
		this.mShipMaxPrice = pShipMaxPrice;
	}
	
	/**
	 * Gets the delivery min days.
	 *
	 * @return the deliveryMinDays
	 */
	public int getDeliveryMinDays() {
		return mDeliveryMinDays;
	}
	
	/**
	 * Sets the delivery min days.
	 *
	 * @param pDeliveryMinDays the deliveryMinDays to set
	 */
	public void setDeliveryMinDays(int pDeliveryMinDays) {
		this.mDeliveryMinDays = pDeliveryMinDays;
	}
	
	/**
	 * Gets the delivery max days.
	 *
	 * @return the deliveryMaxDays
	 */
	public int getDeliveryMaxDays() {
		return mDeliveryMaxDays;
	}
	
	/**
	 * Sets the delivery max days.
	 *
	 * @param pDeliveryMaxDays the deliveryMaxDays to set
	 */
	public void setDeliveryMaxDays(int pDeliveryMaxDays) {
		this.mDeliveryMaxDays = pDeliveryMaxDays;
	}	
	
	/**
	 * Gets the time in transit min days.
	 *
	 * @return the timeInTransitMinDays
	 */
	public int getTimeInTransitMinDays() {
		return mTimeInTransitMinDays;
	}
	
	/**
	 * Sets the time in transit min days.
	 *
	 * @param pTimeInTransitMinDays the timeInTransitMinDays to set
	 */
	public void setTimeInTransitMinDays(int pTimeInTransitMinDays) {
		mTimeInTransitMinDays = pTimeInTransitMinDays;
	}
	
	/**
	 * Gets the time in transit max days.
	 *
	 * @return the timeInTransitMaxDays
	 */
	public int getTimeInTransitMaxDays() {
		return mTimeInTransitMaxDays;
	}
	
	/**
	 * Sets the time in transit max days.
	 *
	 * @param pTimeInTransitMaxDays the timeInTransitMaxDays to set
	 */
	public void setTimeInTransitMaxDays(int pTimeInTransitMaxDays) {
		mTimeInTransitMaxDays = pTimeInTransitMaxDays;
	}
	
	/**
	 * This method return converted value in String.
	 * @return string - String
	 */
	@Override
	public String toString() {
		return "TRUShipRegionVO [mRegionName=" + mRegionName 
				+ ", mOrderCutOff=" + mOrderCutOff + ", mShipMinPrice=" + mShipMinPrice + ", mShipMaxPrice=" + mShipMaxPrice + ", mDeliveryMinDays=" + mDeliveryMinDays +", mDeliveryMaxDays=" + mDeliveryMaxDays +", mTimeInTransitMinDays=" + mTimeInTransitMinDays +", mTimeInTransitMaxDays=" + mTimeInTransitMaxDays + "]";
	}
	
}
