package com.tru.commerce.pricing;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.PricingAdjustment;

import com.tru.common.TRUConstants;

/**
 * @author Professional Access.
 * @version 1.0
 * This TRUItemPriceInfo extends OOTB ItemPriceInfo to update the TRU custom property values to item price info.
 * 
 */
public class TRUItemPriceInfo extends ItemPriceInfo {

	/**
	 * Default serial version id.
	 */
	private static final long serialVersionUID = 1L;

	/** Saved Amount. */
	private double mSavedAmount;

	/** Saved percentage. */
	private double mSavedPercentage;

	/** Item shipping surcharge .*/
	private double mItemShippingSurcharge;

	/** Display strike through price. */
	private boolean mDisplayStrikeThroughPrice;

	/** eWaste/e911/bottleRecycle Fees.. */
	private double mFees;
	
	/** mEWasteFees Fees. */
	private double mEwasteFees;
	
	/** mTotalSalePrice Fees. */
	private double mTotalSalePrice; 
	
	/** Item DealId.  */
	private String mDealID;
	
	
	/**
	 * Gets the deal id.
	 *
	 * @return the deal id
	 */
	public String getDealID() {
		return mDealID;
	}

	/**
	 * Sets the deal id.
	 *
	 * @param pDealID the new deal id
	 */
	public void setDealID(String pDealID) {
		this.mDealID = pDealID;
	}

	
	/**
	 * Returns the savedAmount.
	 * 
	 * @return the savedAmount.
	 */
	public double getSavedAmount() {
		return mSavedAmount;
	}

	/**
	 * Sets/updates the savedAmount.
	 * 
	 * @param pSavedAmount
	 *            the savedAmount to set.
	 */
	public void setSavedAmount(double pSavedAmount) {
		mSavedAmount = pSavedAmount;
	}

	/**
	 * Returns the savedPercentage.
	 * 
	 * @return the savedPercentage.
	 */
	public double getSavedPercentage() {
		return mSavedPercentage;
	}

	/**
	 * Sets/updates the savedPercentage.
	 * 
	 * @param pSavedPercentage
	 *            the savedPercentage to set.
	 */
	public void setSavedPercentage(double pSavedPercentage) {
		mSavedPercentage = pSavedPercentage;
	}

	/**
	 * Returns the itemShippingSurcharge.
	 * 
	 * @return the itemShippingSurcharge.
	 */
	public double getItemShippingSurcharge() {
		return mItemShippingSurcharge;
	}

	/**
	 * Sets/updates the itemShippingSurcharge.
	 * 
	 * @param pItemShippingSurcharge
	 *            the itemShippingSurcharge to set.
	 */
	public void setItemShippingSurcharge(double pItemShippingSurcharge) {
		mItemShippingSurcharge = pItemShippingSurcharge;
	}

	/**
	 * Returns the displayStrikeThroughPrice.
	 * 
	 * @return the displayStrikeThroughPrice.
	 */
	public boolean isDisplayStrikeThroughPrice() {
		return mDisplayStrikeThroughPrice;
	}

	/**
	 * Sets/updates the displayStrikeThroughPrice.
	 * 
	 * @param pDisplayStrikeThroughPrice
	 *            the displayStrikeThroughPrice to set.
	 */
	public void setDisplayStrikeThroughPrice(boolean pDisplayStrikeThroughPrice) {
		mDisplayStrikeThroughPrice = pDisplayStrikeThroughPrice;
	}

	/**
	 * This method will return the total adjusted amount through promotions.
	 * 
	 * @return discounts - Discounted amount through promotions.
	 */
	@SuppressWarnings("unchecked")
	public double getDiscountedAmount() {
		double discounts = TRUConstants.DOUBLE_ZERO;
		if (getAdjustments() != null && !getAdjustments().isEmpty()) {
			for (
					Iterator<PricingAdjustment> iterator = getAdjustments().iterator(); iterator.hasNext();) {
				PricingAdjustment pricingAdjustment = iterator.next();
				if (pricingAdjustment.getPricingModel() != null) {
					discounts += pricingAdjustment.getTotalAdjustment();
				}
			}
		}
		return discounts;
	}
	

	/**
	 * This method will return the total amount saved including promotional discount amount.
	 * 
	 * @return savings - Saved amount.
	 */
	public double getTotalSavings() {
		return getSavedAmount() - getDiscountedAmount();
	}

	/**
	 * This method returns the total saved amount percentage. 
	 * Which includes the total item level savings(price down in list and sale price) and promotional discount.
	 * 
	 * @return savedPercentage - Amount save percentage.
	 */
	public double getTotalSavingsPercentage() {
		double savingPercenatge = TRUConstants.DOUBLE_ZERO;
		if(isDisplayStrikeThroughPrice()){
			savingPercenatge = getTotalSavings() / getRawTotalPrice() * TRUConstants.HUNDERED;
		}else{
			savingPercenatge= getTotalSavings() / getTotalSalePrice() * TRUConstants.HUNDERED;
		}
		return Math.round(savingPercenatge);
	}
	

	/**
	 * Returns the fees.
	 * 
	 * @return the fees.
	 */
	public double getFees() {
		return mFees;
	}

	/**
	 * Sets/updates the fees.
	 * 
	 * @param pFees
	 *            the fees to set.
	 */
	public void setFees(double pFees) {
		mFees = pFees;
	}
	
	/**
	 * This method will return mTotalSalePrice.
	 *
	 * @return the mTotalSalePrice.
	 */
	public double getTotalSalePrice() {
		return mTotalSalePrice;
	}
	/**
	 * This method will set mTotalSalePrice with pTotalSalePrice.
	 * @param pTotalSalePrice the mTotalSalePrice to set.
	 */

	public void setTotalSalePrice(double pTotalSalePrice) {
		mTotalSalePrice = pTotalSalePrice;
	}

	/**
	 * This method returns the ewasteFees value.
	 *
	 * @return the ewasteFees.
	 */
	public double getEwasteFees() {
		return mEwasteFees;
	}

	/**
	 * This method sets the ewasteFees with parameter value pEwasteFees.
	 *
	 * @param pEwasteFees the ewasteFees to set.
	 */
	public void setEwasteFees(double pEwasteFees) {
		mEwasteFees = pEwasteFees;
	}
	/**
	 * Property to hold truOrderDiscountShare.
	 */
	@SuppressWarnings("rawtypes")
	private Map mTruOrderDiscountShare;

	/**
	 * @return the truOrderDiscountShare
	 */
	@SuppressWarnings("rawtypes")
	public Map getTruOrderDiscountShare() {
		if(mTruOrderDiscountShare == null){
			mTruOrderDiscountShare = new HashMap();
		}
		return mTruOrderDiscountShare;
	}
	/**
	 * Property to hold truItemDiscountShare.
	 */
	@SuppressWarnings("rawtypes")
	private Map mTruItemDiscountShare;
	
	/**
	 * @return the truItemDiscountShare
	 */
	@SuppressWarnings("rawtypes")
	public Map getTruItemDiscountShare() {
		if(mTruItemDiscountShare == null){
			mTruItemDiscountShare = new HashMap();
		}
		return mTruItemDiscountShare;
	}

	/**
	 * @param pTruItemDiscountShare the truItemDiscountShare to set
	 */
	@SuppressWarnings("rawtypes")
	public void setTruItemDiscountShare(Map pTruItemDiscountShare) {
		mTruItemDiscountShare = pTruItemDiscountShare;
	}

	/**
	 * @param pTruOrderDiscountShare the truOrderDiscountShare to set
	 */
	@SuppressWarnings("rawtypes")
	public void setTruOrderDiscountShare(Map pTruOrderDiscountShare) {
		mTruOrderDiscountShare = pTruOrderDiscountShare;
	}
	/**
	 * Property to hold excludedForOrderDiscount;
	 */
	private boolean mExcludedForOrderDiscount;
	/**
	 * Gets the excludedForOrderDiscount.
	 * @return the excludedForOrderDiscount
	 */
	public boolean isExcludedForOrderDiscount() {
		return mExcludedForOrderDiscount;
	}
	/**
	 * Sets the excludedForOrderDiscount.
	 * @param pExcludedForOrderDiscount the excludedForOrderDiscount to set
	 */
	public void setExcludedForOrderDiscount(boolean pExcludedForOrderDiscount) {
		mExcludedForOrderDiscount = pExcludedForOrderDiscount;
	}
}
