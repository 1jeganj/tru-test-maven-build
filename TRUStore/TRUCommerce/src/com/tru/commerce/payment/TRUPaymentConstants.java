package com.tru.commerce.payment;


/**
 * @author Professional Access.
 * @version 1.0
 * 
 * class TRUPaymentConstants .
 */
public class TRUPaymentConstants {
	
	/**
	 * Constant to hold  invalid order exception string.
	 */
	public static final String INVALID_ORDER_PARAMETER_EXCEPTION = "Order is null";
	
	/**
	 * Constant to hold Invalid Reversal amount string. 
	 */
	public static final String INVALID_REVERSAL_AMOUNT = "Invalid Reversal Amount";
	
	/**
	 * Constant to hold double for 0.00.
	 */
	public static final double DOUBLE_INITIAL_VALUE = 0.00;
	
	/**
	 * Constant to hold the double zero value.
	 */
	public static final double DOUBLE_ZERO_VALUE = 0.0;
	
	/**
	 * Constant for holding AUTHORIZE_STATE_VALUE.
	 */
	public static final String AUTHORIZE_STATE_VALUE = "authorized";
	/**
	 * Constant for NullItemPriceInfo.
	 */
	public static final String NULL_ITEM_PRICE_INFO = "NullItemPriceInfo";
	/**
	 * Constant for NullShippingPriceInfo.
	 */
	public static final String NULL_SHIPPING_PRICE_INFO = "NullShippingPriceInfo";
	/**
	 * Constant for NullTaxPriceInfo.
	 */
	public static final String NULL_TAX_PRICE_INFO = "NullTaxPriceInfo";
	/**
	 * Constant for NullOrderPriceInfo.
	 */
	public static final String NULL_ORDER_PRICE_INFO = "NullOrderPriceInfo";
	
	/**
	 * Constant for String ERR_GC_INSUFFICIENT_BALANCE .
	 */
	public static final String ERR_GC_INSUFFICIENT_BALANCE = "ERR_GC_INSUFFICIENT_BALANCE";
	
	/**
	 * Constant  to hold String ERROR_GIFT_CARD_VALIDATION.
	 */
	public static final String ERROR_GIFT_CARD_VALIDATION = "tru_error_checkout_giftcard_validation";
	
	/**
	 * Constant to hold AJB_SERVICE_NOT_AVAILABLE.
	 */
	public static final String PAYEEZY_SERVICE_NOT_AVAILABLE = "tru_error_checkout_payeezy_service_not_available";
	
	/**
	 * Constant to hold GIFT_CARD.
	 */
	public static final String GIFT_CARD = "giftCard";
	
	/**
	 * Constant to hold CODE 100.
	 */
	public static final String CODE_100 = "100";
	
	/**
	 * Constant to hold CODE 00.
	 */
	public static final String CODE_00 = "00";
	
	/**
	 * Constant to hold CODE 253.
	 */
	public static final String CODE_253 = "253";
	
	/**
	 * Constant to hold CODE 842.
	 */
	public static final String CODE_842 = "842";
	
	/**
	 * Constant to hold CREDIT_CARD_OBJECT_NULL.
	 */
	public static final String INVENTORY_STATUS_RETRY = "RETRY";
	
	/**
	 * Constant to hold CREDIT_CARD_OBJECT_NULL.
	 */
	public static final String INVENTORY_STATUS_PROCESSED = "PROCESSED";
	
	/**
	 * Constant to hold integer zero.
	 */
	public static final int NUMERIC_ZERO = 0;
	
	/**
	 * Constant to hold CREDIT_CARD_OBJECT_NULL.
	 */
	public static final String INVENTORY_STATUS_NEW = "NEW";
	
	/**
	 * Constant  to hold name for item descriptor failedReverseAdjustInventory.
	 *//*
	public static final String FAILED_REV_AUTH_ITEM_DESC_NAME = "failedReverseAuthorization";
*/
	/**
	 * Constant to hold the String orderId.
	 */
	public static final String ORDER_ID = "orderId";
	
	/**
	 * Constant  to hold name for item descriptor invalid information.
	 */
	public static final String TRU_ERROR_INVALID_INFO = "Invalid information";
	
	/**
	 * Constant  to hold String id.
	 */
	public static final String ID = "id";
	
	/**
	 * Constant  to hold numeric one.
	 */
	public static final int NUMERIC_ONE = 1;
	/**
	 * Constant to String P.
	 */
	public static final String STRING_P = "P";
	
	/**
	 * Constant to hold String web store.
	 */
	public static final String WEB_STORE = "web store";
	
	/**
	 * Constant to hold String O.
	 */
	public static final String STRING_O = "O";
	
	/**
	 * Constant to hold String contactCenter.
	 */
	public static final String CONTACT_CENTER = "contactCenter";
	
	/**
	 * Constant to hold String purchase.
	 */
	public static final String PURCHASE = "purchase";
	
	/**
	 * Constant to hold String valuelink.
	 */
	public static final String VALUE_LINK = "valuelink";

	/**
	 * Constant to hold String purchase_score.
	 */
	public static final String PURCHASE_SCORE = "purchase_score";	
	/**
	 * Constant to hold String purchase_score.
	 */
	public static final String THREEDS_TYPE = "D";
	
	/** The Constant PROPERTY_STATUS. 
	 */
	public static final String PROPERTY_STATUS = "status";
	
	/**
	 * Constant holds the billing address item's class package.
	 */
	public static final String CONT_BILL_ADDRESS_CLASS_PAC = "atg.commerce.order.RepositoryContactInfo";
}
