package com.tru.commerce.payment.util;

/**
 * This is created for the Integration Exception.
 * @author Professional Access.
 * @version 1.0
 */
public class TRUIntegrationException extends Exception {
	
	/**
	 * Version Identifier Serial Version UID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor used to create a Exception.
	 * 
	 * @param pMessage message
	 */
	public TRUIntegrationException(String pMessage) {

		super(pMessage);
	}

	/**
	 * Constructor used to create a Exception.
	 * 
	 * @param pException exception
	 */
	public TRUIntegrationException(Exception pException) {

		super(pException);
	}

	/**
	 * Constructor used to create a Exception.
	 * 
	 * @param pThrowable throwable
	 */
	public TRUIntegrationException(Throwable pThrowable) {

		super(pThrowable);
	}
	
	/**
	 * Constructor used to create a Exception.
	 * @param pMessage mTRUCouponFormHandleressage 
	 * @param  pExceptionexception
	 * @param pException - Exception
	 */
	public TRUIntegrationException(String pMessage, Exception pException) {

		super(pMessage, pException);
	}

}
