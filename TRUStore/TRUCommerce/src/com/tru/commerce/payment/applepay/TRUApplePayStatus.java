package com.tru.commerce.payment.applepay;

import java.util.Date;

import com.tru.commerce.order.payment.TRUPaymentStatus;

/**
 * The Class TRUApplePayStatus.
 */
public class TRUApplePayStatus extends TRUPaymentStatus{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** holds the mCvv2Code. */
	private String mCvvCode;

	/** holds the mTenderType. */
	private String mTenderType;
	
	/**
	 * holds the mBankAuthCode
	 */
	private String mBankAuthCode;
	
	/**
	 * holds the mAuthResponseCode
	 */
	private String mAuthResponseCode;
	
	/**
	 * @return the authResponseCode
	 */
	public String getAuthResponseCode() {
		return mAuthResponseCode;
	}

	/**
	 * @param pAuthResponseCode the authResponseCode to set
	 */
	public void setAuthResponseCode(String pAuthResponseCode) {
		mAuthResponseCode = pAuthResponseCode;
	}

	/**
	 * @return the bankAuthCode
	 */
	public String getBankAuthCode() {
		return mBankAuthCode;
	}

	/**
	 * @param pBankAuthCode the bankAuthCode to set
	 */
	public void setBankAuthCode(String pBankAuthCode) {
		mBankAuthCode = pBankAuthCode;
	}

	
	/**
	 * @return the tenderType
	 */
	public String getTenderType() {
		return mTenderType;
	}

	/**
	 * @param pTenderType the tenderType to set
	 */
	public void setTenderType(String pTenderType) {
		mTenderType = pTenderType;
	}

	/**
	 * Gets the cvv2 code.
	 *
	 * @return the String
	 */
	public String getCvvCode() {
		return mCvvCode;
	}

	/**
	 * Sets the cvv2 code.
	 *
	 * @param pCvv2Code the cvv2Code to set
	 */
	public void setCvvCode(String pCvv2Code) {
		mCvvCode = pCvv2Code;
	}
	/**
	 * holds the mAuthorizationExpiration.
	 */
	private Date mAuthorizationExpiration ;
	/**
	 * Gets the authorization expiration.
	 *
	 * @return the mAuthorizationExpiration
	 */
	public Date getAuthorizationExpiration() {
		return this.mAuthorizationExpiration;
	}

	/**
	 * Sets the authorization expiration.
	 *
	 * @param pAuthorizationExpiration the mAuthorizationExpiration to set
	 */
	public void setAuthorizationExpiration(Date pAuthorizationExpiration) {
		this.mAuthorizationExpiration = pAuthorizationExpiration;
	}

	/** holds the mAvsCode. */
	private String mAvsCode;
	
	/**
	 * @return the avsCode
	 */
	public String getAvsCode() {
		return mAvsCode;
	}

	/**
	 * @param pAvsCode the avsCode to set
	 */
	public void setAvsCode(String pAvsCode) {
		mAvsCode = pAvsCode;
	}
	/**
	 * holds the mAvsDescriptiveResult.
	 */
	private String mAvsDescriptiveResult;
	/**
	 * Gets the avs descriptive result.
	 *
	 * @return the mAvsDescriptiveResult
	 */
	public String getAvsDescriptiveResult() {
		return this.mAvsDescriptiveResult;
	}

	/**
	 * Sets the avs descriptive result.
	 *
	 * @param pAvsDescriptiveResult  the mAvsDescriptiveResult to set
	 */
	public void setAvsDescriptiveResult(String pAvsDescriptiveResult) {
		this.mAvsDescriptiveResult = pAvsDescriptiveResult;
	}
	
	/**
	 * holds the mApplePayTransactionId
	 */
	private String mApplePayTransactionId;
	
	/**
	 * @return the mApplePayTransactionId
	 */
	public String getApplePayTransactionId() {
		return mApplePayTransactionId;
	}

	/**
	 * @param pApplePayTransactionId -String
	 */
	public void setApplePayTransactionId(String pApplePayTransactionId) {
		mApplePayTransactionId = pApplePayTransactionId;
	}


}
