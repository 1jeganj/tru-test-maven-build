package com.tru.commerce.payment.applepay;

import atg.commerce.CommerceException;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroupImpl;
import atg.commerce.order.RepositoryAddress;
import atg.commerce.order.RepositoryContactInfo;
import atg.core.util.Address;

import com.tru.commerce.order.TRUPropertyNameConstants;

/**
 * The Class TRUApplePay.
 */
public class TRUApplePay extends PaymentGroupImpl {

	/**
	 * To hold serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * property TRANSACTION_ID.
	 */
	private static final String PROP_NAME_TRANSACTION_ID = "transactionId";
	
	/**
     * The constant to hold the RESPONSE_CODE.
     */
	private static final String RESPONSE_CODE = "responseCode";

	/**
	 * TransactionId for the order transaction.
	 * 
	 * @return token
	 */
	/**
	 * Initially set the mBillingAddress.
	 */
	private Address mBillingAddress;
	 /**
	  * This method sets the Shipping Address.
	  * @return billingAddress 
	  */
   public Address getBillingAddress()
   {
     return this.mBillingAddress;
   }
   /**
	 * @param pBillingAddress the billingAddress to set
    * @throws CommerceException  CommerceException
	 */
   public void setBillingAddress(Address pBillingAddress) throws CommerceException
   {
     if ((pBillingAddress instanceof RepositoryContactInfo) || (pBillingAddress instanceof RepositoryAddress) || (pBillingAddress == null))


     {
       if (this.mBillingAddress != null){
       	this.mBillingAddress.deleteObservers();
       }
       this.mBillingAddress = pBillingAddress;
       this.mBillingAddress.addObserver(this);
     }
     else
     {
       try
       {
         if (this.mBillingAddress == null){
       	  this.mBillingAddress = (Address)pBillingAddress.getClass().newInstance();
         }
         OrderTools.copyAddress(pBillingAddress, this.mBillingAddress);
       }
       catch (InstantiationException ie) {
         throw new CommerceException(ie.getMessage(),ie);
       }
       catch (IllegalAccessException iae) {
         throw new CommerceException(iae.getMessage(),iae);
       }
       
     }
     setSaveAllProperties(true);
    }
	
	/**
	 * Gets the transaction id.
	 *
	 * @return the transaction id
	 */
	public String getApplePayTransactionId() {
		return (String) getPropertyValue(TRUPropertyNameConstants.APPLEPAY_TRANSACTION_ID);
	}

	/**
	 * Sets the transaction id.
	 *
	 * @param pTransactionId the new transaction id
	 */
	public void setApplePayTransactionId(String pTransactionId) {
		setPropertyValue(TRUPropertyNameConstants.APPLEPAY_TRANSACTION_ID,
				pTransactionId);
	}

	/**
	 * @return ephemeral_publickey
	 */
	public String getEphemeralPublicKey() {
		return (String) getPropertyValue(TRUPropertyNameConstants.APPLEPAY_EPHEMERAL_PUBLICKEY);
	}

	/**
	 * Sets the ephemeral public key.
	 *
	 * @param pEphemeralPublicKey the new ephemeral public key
	 */
	public void setEphemeralPublicKey(String pEphemeralPublicKey) {
		setPropertyValue(TRUPropertyNameConstants.APPLEPAY_EPHEMERAL_PUBLICKEY,
				pEphemeralPublicKey);
	}
	/**
	 * @return ephemeral_publickey_hash
	 */
	public String getPublickeyHash() {
		return (String) getPropertyValue(TRUPropertyNameConstants.APPLEPAY_PUBLICKEY_HASH);
	}

	/**
	 * Sets the publickey hash.
	 *
	 * @param pPublickeyHash the new publickey hash
	 */
	public void setPublickeyHash(String pPublickeyHash) {
		setPropertyValue(TRUPropertyNameConstants.APPLEPAY_PUBLICKEY_HASH,pPublickeyHash);
	}
	/**
	 * @return Version
	 */
	public String getVersion() {
		return (String) getPropertyValue(TRUPropertyNameConstants.APPLEPAY_VERSION);
	}

	/**
	 * @param pVersion
	 *            - String
	 */
	public void setVersion(String pVersion) {
		setPropertyValue(TRUPropertyNameConstants.APPLEPAY_VERSION,pVersion);
	}
	/**
	 * @return Data
	 */
	public String getData() {
		return (String) getPropertyValue(TRUPropertyNameConstants.APPLEPAY_DATA);
	}

	/**
	 * @param pData
	 *            - String
	 */
	public void setData(String pData) {
		setPropertyValue(TRUPropertyNameConstants.APPLEPAY_DATA,pData);
	}
	/**
	 * @return signature
	 */
	public String getSignature() {
		return (String) getPropertyValue(TRUPropertyNameConstants.APPLEPAY_SIGNATURE);
	}

	/**
	 * @param pSignature
	 *            - String
	 */
	public void setSignature(String pSignature) {
		setPropertyValue(TRUPropertyNameConstants.APPLEPAY_SIGNATURE,pSignature);
	}
	/**
	 * @return Order id
	 */
	public String getOrderid() {
		return (String) getPropertyValue(TRUPropertyNameConstants.APPLEPAY_ORDERID);
	}

	/**
	 * @param pOrderid
	 *            - String
	 */
	public void setOrderid(String pOrderid) {
		setPropertyValue(TRUPropertyNameConstants.APPLEPAY_ORDERID,pOrderid);
	}
	
	 /**
 	 * Gets the transaction id.
 	 *
 	 * @return the Transaction Id.
 	 */
    public String getTransactionId() {
        return (String)getPropertyValue(PROP_NAME_TRANSACTION_ID);
    }

    /**
     * Sets the transaction id.
     *
     * @param pTransactionId the cardHolderName to set
     */
    public void setTransactionId(String pTransactionId) {
        setPropertyValue(PROP_NAME_TRANSACTION_ID, pTransactionId);
    }
    
    /**
   	 * Sets the ResponseCode.
   	 * 
   	 * @param pResponseCode
   	 *            - the errorNumber
   	 */
   	 public void setResponseCode(String pResponseCode) {
   		setPropertyValue(RESPONSE_CODE, pResponseCode);
   	 }
   	 
   	 /**
    	 * Gets the Response code.
    	 *
    	 * @return the String.
    	 */
   	 public String getResponseCode() {
   		return (String) getPropertyValue(RESPONSE_CODE);
   	 }
}
