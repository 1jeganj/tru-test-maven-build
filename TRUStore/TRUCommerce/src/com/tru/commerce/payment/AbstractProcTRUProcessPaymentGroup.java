package com.tru.commerce.payment;

import java.util.List;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.commerce.payment.processor.ProcProcessPaymentGroup;
import atg.payment.PaymentStatus;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;
import atg.service.pipeline.RunProcessException;

import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUPipeLineConstants;

/**
 * @author ORACLE
 * @version 1.0
 */
public abstract class AbstractProcTRUProcessPaymentGroup extends ProcProcessPaymentGroup
implements PipelineProcessor {
	public static final int SUCCESS = 1;
	/**
	 * Method invokeProcessorAction.
	 * @param pResult -- PipelineResult
	 * @param pParam --Object
	 * @return Integer.
	 * @throws RunProcessException	-	if any.
	 */
	public int runProcess(Object pParam, PipelineResult  pResult) throws RunProcessException{
		if(isLoggingDebug()){
			logDebug("Method body");
		}
		Boolean isReverse = Boolean.FALSE;
		PaymentManagerPipelineArgs params = (PaymentManagerPipelineArgs) pParam;
		Object action = (Object)params.get(TRUPipeLineConstants.REVERSE_ACTION);
		if (action != null) {
			isReverse = (Boolean)action;
		}
		try {
			if (!isReverse) {
				invokeProcessorAction(params.getAction(), params);	
			} else {
				invokeReverseProcessor(params);
			}
		} catch (CommerceException e) {
			if(isLoggingError()){
				vlogError("CommerceException of ProcTRUProcessPaymentGroup.runProcess,OrderId:{0} ProfileId:{1} CommerceException:{2}",
						params.getOrder().getId(),params.getOrder().getProfileId(),e);
			}
			pResult.addError(TRUPipeLineConstants.PROCESS_PG_FAILED, e);
			return TRUPipeLineConstants.NUMERIC_ZERO;
		}
		return TRUPipeLineConstants.NUMERIC_ONE;
	}
	
	/**
	 * @param pParams - PaymentManagerPipelineArgs
	 * @throws CommerceException - CommerceException
	 */
	protected void invokeReverseProcessor(PaymentManagerPipelineArgs pParams) throws CommerceException {
		PaymentStatus status = null;
		status = reverseAuthorizePaymentGroup(pParams);
		pParams.setPaymentStatus(status);
	}

	/**
	 * Method invokeProcessorAction.
	 * @param pParams -- PaymentManagerPipelineArgs
	 * @return null 
	 * @throws CommerceException - CommerceException
	 */
	public abstract PaymentStatus reverseAuthorizePaymentGroup(PaymentManagerPipelineArgs pParams) throws CommerceException ;

	/**
	 * @return int[]
	 */
	public int[] getRetCodes()
	{
		int[] retCodes = { TRUPipeLineConstants.NUMERIC_ONE };
		return retCodes;
	}
	
	/**
	 * This method is overridden for decrease the autharise amount for creditcard while modifying the order.
	 * @param pParams
	 * 			- Pipeline arguments.
	 * @return status
	 * @throws CommerceException - CommerceException
	 */
	public PaymentStatus decreaseAuthorizationForPaymentGroup(PaymentManagerPipelineArgs pParams)
     throws CommerceException
     {
		if (isLoggingDebug()) { 
			vlogDebug("ProcTRUProcessPaymentGroup.decreaseAuthorizationForPaymentGroup() method. START"); 
		}
		PaymentStatus status = null;
		PaymentGroup pg = pParams.getPaymentGroup();
		int psListSize = 0;
		List paymentStatusList = null;
		Order order = pParams.getOrder();
		TRUPaymentManager paymentManager = (TRUPaymentManager)pParams.getPaymentManager(); 
		if (isLoggingDebug()) { 
			vlogDebug("ProcTRUProcessPaymentGroup.decreaseAuthorizationForPaymentGroup :: PaymentGroup : {0}", pg);
		}
		if(pg != null && pg.getAmountAuthorized() > TRUPaymentConstants.DOUBLE_INITIAL_VALUE && pg.getAmount() > 
			TRUPaymentConstants.DOUBLE_INITIAL_VALUE){
			double revarsalAmount = pParams.getAmount();
			if(order != null  && revarsalAmount > TRUPaymentConstants.DOUBLE_INITIAL_VALUE && pg instanceof TRUCreditCard) {
					TRUCreditCard creditCard = (TRUCreditCard) pg;
					creditCard.setReversalAmount(revarsalAmount);
					paymentManager.reverseAuthorize(order, pg, revarsalAmount);
					paymentStatusList = pg.getAuthorizationStatus();
					if (isLoggingDebug()) { 
						vlogDebug("PaymentGroup paymentStatusList : {0}", paymentStatusList);
					}
					if(paymentStatusList != null){
						psListSize = paymentStatusList.size();
					}
					if(pg.getAuthorizationStatus() != null && pg.getAuthorizationStatus().size() > psListSize){
						if (isLoggingDebug()) { 
							vlogDebug("PaymentGroup paymentStatus : {0}", pg.getAuthorizationStatus().get(psListSize));
						}
						status = (PaymentStatus) pg.getAuthorizationStatus().get(psListSize);
					}
			}
		}
		if (isLoggingDebug()) { 
			vlogDebug("ProcTRUProcessPaymentGroup.decreaseAuthorizationForPaymentGroup() method. END"); 
		}
		return status;
   }
	
}