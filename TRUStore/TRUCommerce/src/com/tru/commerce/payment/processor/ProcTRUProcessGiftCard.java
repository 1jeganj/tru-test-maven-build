package com.tru.commerce.payment.processor;

import java.util.List;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.payment.PaymentStatus;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUGiftCard;
import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardInfo;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardStatus;
import com.tru.commerce.payment.AbstractProcTRUProcessPaymentGroup;
import com.tru.commerce.payment.TRUPaymentManager;
import com.tru.radial.exception.TRURadialIntegrationException;
/**
 * This class is used to authorize the gift card payment group and reverse authorization of gift card payment group.
 * @author PA
 * @version 1.0
 */
public class ProcTRUProcessGiftCard extends AbstractProcTRUProcessPaymentGroup {
	
	/**
	 * variable to hold mPaymentManager reference.
	 */
	private TRUPaymentManager mPaymentManager;
	
	/**
	 * This method authorizes the payment group.
	 * 
	 * @param pParams -- PaymentManagerPipelineArgs
	 * @return giftCardStatus -- gift card status
	 */
	public PaymentStatus authorizePaymentGroup(PaymentManagerPipelineArgs  pParams){
		if (isLoggingDebug()) {
			vlogDebug(" ProcTRUProcessGiftCard .(authorizePaymentGroup) Method : BEGIN ");
		}
		TRUGiftCardStatus giftCardStatus = null;
		Order order = pParams.getOrder();

		try {
			TRUGiftCardInfo giftCardInfo = (TRUGiftCardInfo)pParams.getPaymentInfo();
			//Authorizes, get the gift card balance and makes the gift card payment.
			giftCardInfo.setOrder(order);
			giftCardStatus = getPaymentManager().getGiftCardPaymentProcessor().giftCardRedeem(giftCardInfo);
			
			if(giftCardStatus.getTransactionSuccess()){
				((TRUGiftCard)pParams.getPaymentGroup()).setTransactionId(giftCardStatus.getTransactionId());
			}
			if (isLoggingDebug()) {
				vlogDebug(" TRUGiftCardInfo :{0},TRUGiftCardStatus :{1}",giftCardInfo,giftCardStatus);
			}
			
		} catch (ClassCastException cce) {
			String profileId = null;
			if (order instanceof TRUOrderImpl) {
				profileId = ((TRUOrderImpl) pParams.getOrder()).getProfileId();
			} else if (order instanceof TRULayawayOrderImpl) {
				profileId = ((TRULayawayOrderImpl) pParams.getOrder()).getProfileId();
			}
			if (isLoggingError()) {
				vlogError("ProcTRUProcessGiftCard.authorizePaymentGroup() ,orderId:{0} profileId:{1} className:{2} ClassCastException:{3}",
						order.getId(), profileId, pParams.getPaymentInfo().getClass().getName());
			}
			throw cce;
		}
		if (isLoggingDebug()) {
			vlogDebug(" ProcTRUProcessGiftCard .(authorizePaymentGroup) Method : END ");
		}
		return giftCardStatus;
	}
    
	/**
	 * This method reverse authorizes the payment group.
	 * 
	 * @param pParams -- PaymentManagerPipelineArgs
	 * @return PaymentStatus - PaymentStatus
	 * @throws CommerceException -- CommerceException
	 */
	public PaymentStatus reverseAuthorizePaymentGroup(PaymentManagerPipelineArgs pParams) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug(" ProcTRUProcessGiftCard .(reverseAuthorizePaymentGroup) Method : BEGIN ");
		}
		
		TRUGiftCardStatus paymentStatus = null;
		TRUGiftCardInfo giftCardInfo = null;
		TRUGiftCardStatus lastStatus = null;
		TRUGiftCardProcessor giftCardProcessor = (TRUGiftCardProcessor)getPaymentManager().getGiftCardPaymentProcessor();
		try {
			if (pParams != null) {
				giftCardInfo = (TRUGiftCardInfo)pParams.getPaymentInfo();
				TRUGiftCard giftCardPG = (TRUGiftCard)pParams.getPaymentGroup();
				List<PaymentStatus> authorizationStatus = giftCardPG.getAuthorizationStatus();
				for(PaymentStatus status : authorizationStatus){
					if(status instanceof TRUGiftCardStatus ){
						paymentStatus = (TRUGiftCardStatus)status;
						if(giftCardPG.getTransactionId().equalsIgnoreCase(paymentStatus.getTransactionId()) && paymentStatus.getTransactionSuccess()){
							lastStatus = paymentStatus;
							break;
						}
					}
				}
				if (isLoggingDebug()) {
					vlogDebug(" Payment Status :{0} ",lastStatus);
				}
				if(lastStatus != null){
					paymentStatus = new TRUGiftCardStatus();
					giftCardInfo.setTransactionId(lastStatus.getTransactionId());
					giftCardInfo.setAmount(giftCardPG.getReversalAmount());
					paymentStatus = giftCardProcessor.reverseAuthorize(giftCardInfo);
				}
			}
			if (isLoggingDebug()) {
				vlogDebug(" TRUGiftCardInfo :{0},TRUGiftCardStatus :{1}",giftCardInfo,paymentStatus);
			}
			
		}catch (TRURadialIntegrationException paymentIntExp) {
			if (isLoggingError()) {
				vlogError("ProcTRUProcessGiftCard.reverseAuthorizePaymentGroup()", paymentIntExp);
			}
			paymentStatus.setTransactionSuccess(false);
			paymentStatus.setErrorMessage(TRUCheckoutConstants.TRU_ERROR_CHECKOUT_CREDIT_GENERAL_ERROR);
		}
		if (isLoggingDebug()) {
			vlogDebug(" ProcTRUProcessGiftCard .(reverseAuthorizePaymentGroup) Method : END ");
		}
		return paymentStatus;
	}

	/**
	 * This method is used to credits the amount to the payment group.
	 * 
	 * @param pParamPaymentManagerPipelineArgs -- PaymentManagerPipelineArgs
	 * @throws CommerceException -- CommerceException
	 * @throws UnsupportedOperationException -- UnsupportedOperationException
	 * @return PaymentStatus - PaymentStatus
	 */
	public PaymentStatus creditPaymentGroup(
			PaymentManagerPipelineArgs pParamPaymentManagerPipelineArgs)
			throws CommerceException,UnsupportedOperationException {
		 throw new UnsupportedOperationException();
	}

	/**
	 * This method debits amount from the payment group.
	 * 
	 * @param pParamPaymentManagerPipelineArgs -- PaymentManagerPipelineArgs
	 * @return giftCardStatus -- gift card status
	 * @throws CommerceException -- CommerceException
	 */
	public PaymentStatus debitPaymentGroup(
			PaymentManagerPipelineArgs pParamPaymentManagerPipelineArgs)
			throws CommerceException {
		 throw new UnsupportedOperationException();
	}
	
	/**
	 * @param pPaymentManager the PaymentManager to set
	 */
	public void setPaymentManager(TRUPaymentManager pPaymentManager) {
		this.mPaymentManager = pPaymentManager;
	}

	/**
	 * @return the PaymentManager
	 */
	public TRUPaymentManager getPaymentManager() {
		return mPaymentManager;
	}

}