
package com.tru.commerce.payment.processor;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.payment.PaymentException;
import atg.commerce.payment.PaymentManager;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.core.util.StringUtils;
import atg.payment.PaymentStatus;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.payment.AbstractProcTRUProcessPaymentGroup;
import com.tru.commerce.payment.TRUPayPalPaymentInfo;
import com.tru.commerce.payment.TRUPaymentManager;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.commerce.payment.paypal.TRUPayPalStatus;
import com.tru.radial.commerce.payment.paypal.TRUPaypalConstants;
import com.tru.radial.commerce.payment.paypal.util.TRUPayPalException;
/**
 * The PayPal payment processor will do the following tasks.
 *  i) Payment Authorization.
 * @author PA
 * @version 1.0
 *
 */
public class TRUProcProcessPayPal extends AbstractProcTRUProcessPaymentGroup {
	
	/**
	 * Holds the PaymentManager.
	 */
	private PaymentManager mPaymentManager;
	/**
	 * Holds the Configuration.
	 */
	/** property to hold Negative testing. */
	private boolean mEnableNegativeTesting;
	/** property to hold negative testing for authorization. */
	//private boolean mEnableFailAuthorization;
	/** property to hold Negative testing for fraud.*/
	//private boolean mEnableFailFraud;
	/** property to hold Negative testing for paypal do authorization.*/
	private boolean mEnableFailPaypal;
	//TODO :: Start This has to remove once the TRU confirms on the DO void flow.
	/** property to hold to enable DoVoid in case of BAMS fails.*/
	private boolean mDoVoid;

	/**
	 * @return the doVoid
	 */
	public boolean isDoVoid() {
		return mDoVoid;
	}

	/**
	 * @param pDoVoid the doVoid to set
	 */
	public void setDoVoid(boolean pDoVoid) {
		mDoVoid = pDoVoid;
	}
	//TODO :: End This has to remove once the TRU confirms on the DO void flow.

	/**
	 * @return the enableFailPaypal
	 */
	public boolean isEnableFailPaypal() {
		return mEnableFailPaypal;
	}


	/**
	 * @param pEnableFailPaypal the enableFailPaypal to set
	 */
	public void setEnableFailPaypal(boolean pEnableFailPaypal) {
		mEnableFailPaypal = pEnableFailPaypal;
	}


	/**
	 * @return the enableNegativeTesting
	 */
	public boolean isEnableNegativeTesting() {
		return mEnableNegativeTesting;
	}


	/**
	 * @param pEnableNegativeTesting the enableNegativeTesting to set
	 */
	public void setEnableNegativeTesting(boolean pEnableNegativeTesting) {
		mEnableNegativeTesting = pEnableNegativeTesting;
	}

	
	/**
	 * Getting the PaymentManager.
	 * @return the PaymentManager
	 */
	public PaymentManager getPaymentManager() {
		return mPaymentManager;
	}

	/**
	 * Setting the value for the mPaymentManager.
	 * @param pPaymentManager
	 * 			- the PaymentManager
	 */
	public void setPaymentManager(PaymentManager pPaymentManager) {
		this.mPaymentManager = pPaymentManager;
	}	
	
	/**
	 * This method is used to authorize the paypal  payment group, and return.
	 * the TRUPayPal payment group status.
	 * 
	 * @param pPaymentManagerPipelineArgs			-pPaymentmanagerpipelineargs
	 * @return PaymentStatus								-status	
	 * @throws CommerceException					-CommerceException
	 */
	@Override
	public PaymentStatus authorizePaymentGroup(PaymentManagerPipelineArgs pPaymentManagerPipelineArgs)
			throws CommerceException {
		if(isLoggingDebug()){
			vlogDebug("Start of TRUProcProcessPayPal.authorizePaymentGroup() method");
		}
		
		TRUPayPalPaymentInfo lpayPalPaymentInfo = (TRUPayPalPaymentInfo) pPaymentManagerPipelineArgs.getPaymentInfo();
		Order order = pPaymentManagerPipelineArgs.getOrder();
		TRUPayPalStatus lPayPalStatus = new TRUPayPalStatus();
		Number lPayPalAmount = null;
		NumberFormat lNbrFrmt = NumberFormat.getNumberInstance();
		lNbrFrmt.setMaximumFractionDigits(TRUPaypalConstants.MAXIMUM_FRACTIONALDIGITS);
		try {
			lPayPalAmount = lNbrFrmt.parse(lpayPalPaymentInfo.getAmount());
		} catch (ParseException e) {
			if(isLoggingError()){
				logError("ParseException while processing paypal payment @Class:::TRUProcProcessPayPal::@method::authorizePaymentGroup()", e);
			}
			throw new CommerceException(e);
		}
		if (lPayPalAmount.doubleValue() <= TRUPaypalConstants.ORDERAMT_ZERO) {
			lPayPalStatus.setTransactionId(TRUPaypalConstants.ZERO_AUTH_TRANSACTION_ID);
			lPayPalStatus.setAmount(TRUPaypalConstants.ORDER_ZEROAMT);
			lPayPalStatus.setTransactionSuccess(true);
			lPayPalStatus.setErrorMessage(TRUPaypalConstants.BLANK_STRING);
			lPayPalStatus.setTransactionTimestamp(new Date());
			return lPayPalStatus;
		}
		String lTransactionId = null;
		//String lCoRelationId = null;
		PaymentGroup lPaymentGroup = null;
		//String lCoTransactionTag = null;
		try {
			//populating transactional currency code of order
			if(order instanceof TRUOrderImpl){
				lpayPalPaymentInfo.setCurrencyCode(order.getPriceInfo().getCurrencyCode());
				lpayPalPaymentInfo.setPageName(((TRUOrderImpl)order).getCurrentTab());
			}
			//delegating  authorization to PayPal processor
			((TRUPaymentManager)getPaymentManager()).getPayPalProcessor().authorize(lpayPalPaymentInfo,lPayPalStatus);
			if(lPayPalStatus != null && lPayPalStatus.getTransactionSuccess()) {
				lTransactionId = lPayPalStatus.getTransactionId();
				//lCoTransactionTag = lPayPalStatus.getTransactionTag();
				lPaymentGroup = pPaymentManagerPipelineArgs.getPaymentGroup();
				if (lPaymentGroup instanceof TRUPayPal) {
					((TRUPayPal) lPaymentGroup).setPayPalTransactionId(lTransactionId);
					//((TRUPayPal) lPaymentGroup).setPayPalCorRelationId(lCoRelationId);
					((TRUPayPal)lPaymentGroup).setTransactionId(lPayPalStatus.getTransactionId());
					//((TRUPayPal) lPaymentGroup).setExtraProperties(extraParametersMap);
				}
				lPaymentGroup.setSubmittedDate(new Date());
			}
			
			//TODO : Start This needs to be remove once the negative scenario testing done.			
			/*if(isEnableNegativeTesting()){				
				lPayPalStatus.setTransactionSuccess(false);
				}else{
				lPayPalStatus.setTransactionSuccess(true);
				}		*/	
			//TODO : End This needs to be remove once the negative scenario testing done.			
		} catch (TRUPayPalException pPe) {
			lPayPalStatus.setTransactionSuccess(false);
			lPayPalStatus.setErrorMessage(pPe.getMessage());
			if (isLoggingError()) {
				logError("TRUPayPalException Exception occured in TRUProcProcessPayPal.authorizePaymentGroup: @Class:::TRUProcProcessPayPal::@method::authorizePaymentGroup()" +
						"While calling DOExpressCheckout API with Order= " + order, pPe);
			}
			//throw new PaymentException(TRUPaypalConstants.PAYPAL_PAYMENT_AUTH_ERROR, pPe);
		}
		if(isLoggingDebug()){
			vlogDebug("End of TRUProcProcessPayPal.authorizePaymentGroup() method");
		}
		return lPayPalStatus;
	}
	
	/**
	 * This method reverse authorizes the payment group.
	 * @param pParams -- PaymentManagerPipelineArgs
	 * @return PaymentStatus - PaymentStatus
	 * @throws CommerceException -- CommerceException
	 */
	public PaymentStatus reverseAuthorizePaymentGroup(PaymentManagerPipelineArgs pParams) throws CommerceException {
		
		if(isLoggingDebug()){
			vlogDebug("Start of TRUProcProcessPayPal.reverseAuthorizePaymentGroup() method");
		}
		
		TRUPayPalPaymentInfo lpayPalPaymentInfo = (TRUPayPalPaymentInfo) pParams.getPaymentInfo();
		//Order order = pParams.getOrder();
		//TRUPayPalStatus lPayPalStatus = (TRUPayPalStatus) pParams.getPaymentStatus();
		PaymentGroup pg = pParams.getPaymentGroup();
		Number lPayPalAmount = null;
		NumberFormat lNbrFrmt = NumberFormat.getNumberInstance();
		lNbrFrmt.setMaximumFractionDigits(TRUPaypalConstants.MAXIMUM_FRACTIONALDIGITS);
		try {
			lPayPalAmount = lNbrFrmt.parse(lpayPalPaymentInfo.getAmount());
		} catch (ParseException e) {
			if(isLoggingError()){
				logError("ParseException exception occurs while processing reversal paypal payment @Class:::TRUProcProcessPayPal::@method::reverseAuthorizePaymentGroup()", e);
			}
			throw new CommerceException(e);
		}
		TRUPayPalStatus lastStatus = null;
				
		List<PaymentStatus> authorizationStatus = pg.getAuthorizationStatus();
		for(PaymentStatus status : authorizationStatus){
				if(status instanceof TRUPayPalStatus && ((TRUPayPal)pg).getTransactionId().equalsIgnoreCase(((TRUPayPalStatus)status).getTransactionId()) && 
						status.getTransactionSuccess() && TRUCheckoutConstants.AUTHORIZE_SCORE.equalsIgnoreCase(((TRUPayPalStatus)status).getTransactionType())){
					lastStatus = (TRUPayPalStatus)status;
					break;
				}
		}

		TRUPayPalStatus paypalStatus = new TRUPayPalStatus();
		
		if (lastStatus == null || lPayPalAmount.doubleValue() <= TRUPaypalConstants.ORDERAMT_ZERO) {
			paypalStatus.setTransactionId(TRUPaypalConstants.ZERO_AUTH_TRANSACTION_ID);
			paypalStatus.setAmount(TRUPaypalConstants.ORDER_ZEROAMT);
			paypalStatus.setTransactionSuccess(true);
			paypalStatus.setErrorMessage(TRUPaypalConstants.BLANK_STRING);
			paypalStatus.setTransactionTimestamp(new Date());
			return paypalStatus;
		}
		
		/*try {
			//populating transactional currency code of order
			if(order instanceof TRUOrderImpl){
				lpayPalPaymentInfo.setCurrencyCode(order.getPriceInfo().getCurrencyCode());
			}
			
			if(lastStatus.getTransactionSuccess() && (!lastStatus.isFullAuthSuccess() && !lastStatus.isFraud())){
				paypalStatus.setTransactionId(lastStatus.getTransactionId());
				((TRUPaymentManager)getPaymentManager()).getPayPalProcessor().reverseAuthorize(lpayPalPaymentInfo,paypalStatus);			
			}
			//TODO :: Start This has to remove once the TRU confirms on the DO void flow.
			if(isDoVoid() && lastStatus.getTransactionSuccess()){
				paypalStatus.setTransactionTag(lastStatus.getTransactionTag());
				paypalStatus.setTransactionId(lastStatus.getTransactionId());
				((TRUPaymentManager)getPaymentManager()).getPayPalProcessor().reverseAuthorize(lpayPalPaymentInfo,paypalStatus);			
			}
			//TODO :: End This has to remove once the TRU confirms on the DO void flow.
		} catch (TRUPayPalException pPe) {
			paypalStatus.setTransactionSuccess(false);
			paypalStatus.setErrorMessage(pPe.getMessage());
			if (isLoggingError()) {
				logError("TRUPayPalException Exception occured in TRUProcProcessPayPal.reverseAuthorizePaymentGroup: " +
						"While calling doVoid API with Order= " + order, pPe);
			}
			throw new PaymentException(TRUPaypalConstants.PAYPAL_PAYMENT_AUTH_ERROR, pPe);
		}*/
		if(isLoggingDebug()){
			vlogDebug("End of TRUProcProcessPayPal.reverseAuthorizePaymentGroup() method");
		}
		return paypalStatus;
	}
	
	/**
	 * This method is used to credit the paypal  payment group, and return.
	 * the TRUPayPal payment group status.
	 * 
	 * @param pPaymentManagerPipelineArgs			-pPaymentmanagerpipelineargs
	 * @return PaymentStatus								-status	
	 * @throws CommerceException					-CommerceException
	 */
	@Override
	public PaymentStatus creditPaymentGroup(PaymentManagerPipelineArgs pPaymentManagerPipelineArgs)
			throws CommerceException {
		if(isLoggingDebug()){
			vlogDebug("Start of TRUProcProcessPayPal.creditPaymentGroup() method");
		}
		
		TRUPayPalPaymentInfo lpayPalPaymentInfo = (TRUPayPalPaymentInfo) pPaymentManagerPipelineArgs.getPaymentInfo();
		Order lOrder = pPaymentManagerPipelineArgs.getOrder();
		//String lRequestId = (String) pPaymentManagerPipelineArgs.get(TRUPaypalConstants.REQUEST_ID);
		String lComments = (String) pPaymentManagerPipelineArgs.get(TRUPaypalConstants.COMMENTS);		
		TRUPayPalStatus lPayPalStatus = new TRUPayPalStatus();
	    if(!StringUtils.isBlank(lComments)){
	    	lpayPalPaymentInfo.setComments(lComments);
		}
	   /* if(!StringUtils.isBlank(lRequestId)){
	    	lPayPalStatus.setReturnRequestId(lRequestId);
		}	    
	    */if(lOrder instanceof TRUOrderImpl){
	    	lpayPalPaymentInfo.setCurrencyCode(TRUPaypalConstants.USD);
	    }		
		String lTransactionId = null;
		//String lCoRelationId = null;
		try {
			lPayPalStatus = ((TRUPaymentManager)getPaymentManager()).getPayPalProcessor().credit(lpayPalPaymentInfo, lPayPalStatus);
			if(lPayPalStatus != null){
				lTransactionId = lpayPalPaymentInfo.getTransactionId();
				PaymentGroup lPaymentGroup = pPaymentManagerPipelineArgs.getPaymentGroup();
				if (lPaymentGroup instanceof TRUPayPal) {
					((TRUPayPal) lPaymentGroup).setPayPalTransactionId(lTransactionId);
					//((TRUPayPal) lPaymentGroup).setPayPalCorRelationId(lCoRelationId);

				}
				lPaymentGroup.setSubmittedDate(new Date());
				//return lLastDebitStatus;
			}
		} catch (TRUPayPalException pPe) {
			lPayPalStatus.setTransactionSuccess(false);
			lPayPalStatus.setErrorMessage(pPe.getMessage());
			if (isLoggingError()) {
				logError("TRUPayPalException Exception occured in TRUProcProcessPayPal.creditPaymentGroup: " +
					"While calling DOExpressCheckout API with Order= " + lOrder, pPe);
			}
			throw new PaymentException(TRUPaypalConstants.PAYPAL_PAYMENT_AUTH_ERROR, pPe);
		}
		if(isLoggingDebug()){
			vlogDebug("End of TRUProcProcessPayPal.creditPaymentGroup() method");
		}
		return lPayPalStatus;
	}

	/**
	 * This method is used to debit the paypal  payment group, and return.
	 * the TRUPayPal payment group status.
	 * 
	 * @param pPaymentManagerPipelineArgs			-pPaymentmanagerpipelineargs
	 * @return PaymentStatus								-status	
	 * @throws CommerceException					-CommerceException
	 */
	@Override
	public PaymentStatus debitPaymentGroup(PaymentManagerPipelineArgs pPaymentManagerPipelineArgs)
			throws CommerceException {
		if(isLoggingDebug()){
			vlogDebug("Start of TRUProcProcessPayPal.debitPaymentGroup() method");
		}
		TRUPayPalPaymentInfo lpayPalPaymentInfo = (TRUPayPalPaymentInfo) pPaymentManagerPipelineArgs.getPaymentInfo();
		Order lOrder = pPaymentManagerPipelineArgs.getOrder();
		TRUPayPalStatus lPayPalStatus = new TRUPayPalStatus();
		lPayPalStatus.setTransactionId(lpayPalPaymentInfo.getTransactionId());
		PaymentGroup pg = pPaymentManagerPipelineArgs.getPaymentGroup();
		PaymentStatus authStatus = pPaymentManagerPipelineArgs.getPaymentManager().getLastAuthorizationStatus(pg);
		if (authStatus == null || TRUPaypalConstants.ZERO_AUTH_TRANSACTION_ID.equals(authStatus.getTransactionId())) {
			lPayPalStatus.setTransactionId(TRUPaypalConstants.ZERO_DEBIT_TRANSACTION_ID);
			lPayPalStatus.setAmount(TRUPaypalConstants.ORDER_ZEROAMT);
			lPayPalStatus.setTransactionSuccess(true);
			lPayPalStatus.setErrorMessage(TRUPaypalConstants.BLANK_STRING);
			lPayPalStatus.setTransactionTimestamp(new Date());
			return lPayPalStatus;
		}
		
		String lTransactionId = null;
		//String lCoRelationId = null;
		try {
			lPayPalStatus = ((TRUPaymentManager)getPaymentManager()).getPayPalProcessor().debit(lpayPalPaymentInfo);
			if(lPayPalStatus != null){
				lTransactionId = lPayPalStatus.getTransactionId();
				PaymentGroup lPaymentGroup = pPaymentManagerPipelineArgs.getPaymentGroup();
				if (lPaymentGroup instanceof TRUPayPal) {
					((TRUPayPal) lPaymentGroup).setPayPalTransactionId(lTransactionId);
					//((TRUPayPal) lPaymentGroup).setPayPalCorRelationId(lCoRelationId);
				}
			}
		} catch (TRUPayPalException pPe) {
			lPayPalStatus.setTransactionSuccess(false);
			lPayPalStatus.setErrorMessage(pPe.getMessage());
			if (isLoggingError()) {
				logError("TRUPayPalException Exception occured in TRUProcProcessPayPal.debitPaymentGroup: " +
					"While calling DOExpressCheckout API with Order= " + lOrder, pPe);
			}
			throw new PaymentException(TRUPaypalConstants.PAYPAL_PAYMENT_AUTH_ERROR, pPe);
		}
		if(isLoggingDebug()){
			vlogDebug("End of TRUProcProcessPayPal.debitPaymentGroup() method");
		}
		return lPayPalStatus;
	}
}