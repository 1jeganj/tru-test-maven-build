package com.tru.commerce.payment.processor;

import java.math.BigDecimal;

import atg.commerce.order.Order;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.core.util.ContactInfo;
import atg.nucleus.GenericService;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.commerce.order.payment.applepay.TRUApplePayInfo;
import com.tru.commerce.payment.applepay.TRUApplePay;
import com.tru.common.TRUConfiguration;

/**
 * The Class ProcTRUCreateApplePayInfo.
 */
public class ProcTRUCreateApplePayInfo extends GenericService implements PipelineProcessor{
	/**
	 * Property holds the Constant Success Code.
	 */
	private static final int SUCCESS = 1;
	/**
	 * Property holds the Constant TEST.
	 */
	//private static final String TEST = TRUPipeLineConstants.TEST;
	/**
	 * Property holds the gift card info.
	 */
	private String mApplePayInfoClass;
	
	/**
	 * Property to hold the TRUConfiguration.
	 */
	private TRUConfiguration mTruConfiguration;

	/**
	 * Gets the tru configuration.
	 *
	 * @return the tRUConfiguration
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * Sets the tru configuration.
	 *
	 * @param pTRUConfiguration the tRUConfiguration to set
	 */
	public void setTruConfiguration(TRUConfiguration pTRUConfiguration) {
		mTruConfiguration = pTRUConfiguration;
	}
	
	/**
	 * Gets the apple pay info class.
	 *
	 * @return String
	 */ 
	public String getApplePayInfoClass()
	{
		return this.mApplePayInfoClass;
	}

	/**
	 * Sets the apple pay info class.
	 *
	 * @param pApplePayInfoClass - String
	 */
	public void setApplePayInfoClass(String pApplePayInfoClass)
	{
		this.mApplePayInfoClass = pApplePayInfoClass;
	}
	
	/**
	 *  ProcTRUCreateApplePayInfo.
	 */
	public ProcTRUCreateApplePayInfo()
	{
		this.mApplePayInfoClass = TRUPipeLineConstants.APPLE_PAY_INFO_CLASS;
	}
	
	/**
	 * Adds the data to apple pay info.
	 *
	 * @param pOrder - Order
	 * @param pPaymentGroup - TRUApplePayPG
	 * @param pAmount - double
	 * @param pParams - PaymentManagerPipelineArgs
	 * @param pApplePayInfo - TRUApplePayInfo
	 */
	protected void addDataToApplePayInfo(Order pOrder, TRUApplePay pPaymentGroup, double pAmount, PaymentManagerPipelineArgs pParams, TRUApplePayInfo pApplePayInfo)
	{
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUCreateApplePayInfo.addDataToApplePayInfo :STARTS");
		}
		TRUOrderImpl order = null;

		if (pOrder instanceof TRUOrderImpl) {

			order = (TRUOrderImpl)pOrder;

		}

		pApplePayInfo.setAmount(new BigDecimal(pPaymentGroup.getAmount()));

		pApplePayInfo.setCurrencyCode(pPaymentGroup.getCurrencyCode());
		
		pApplePayInfo.setOrderId(order.getId());
		
		pApplePayInfo.setCustomerIPAddress(order.getCustomerIPAddress());

		pApplePayInfo.setCustomerEmail(order.getEmail());
//		if (order.getIpAddress() != null) {
//			pApplePayInfo.setCustomerIPAddress(order.getIpAddress());	
//		}
		//Billing Address
		if(order.getBillingAddress()!=null){
			ContactInfo billingAddress = (ContactInfo)order.getBillingAddress();
			pApplePayInfo.setBillingFirstName(billingAddress.getFirstName());
			pApplePayInfo.setBillingLastName(billingAddress.getLastName());
			pApplePayInfo.setBillingPhoneNo(billingAddress.getPhoneNumber());

			pApplePayInfo.setBillingAddressLine1(billingAddress.getAddress1());
			pApplePayInfo.setBillingAddressLine2(billingAddress.getAddress2());
			pApplePayInfo.setBillingAddressLine3(billingAddress.getAddress3());

			pApplePayInfo.setBillingAddressState(billingAddress.getState());
			pApplePayInfo.setBillingAddressCity(billingAddress.getCity());
			pApplePayInfo.setBillingAddressPostalCode(billingAddress.getPostalCode());
			pApplePayInfo.setBillingAddressCountryCode(billingAddress.getCountry());
		}
		//Shipping Address
		TRUHardgoodShippingGroup shippingGroup = (TRUHardgoodShippingGroup)order.getShippingGroups().get(0);

		ContactInfo shippingAddress = (ContactInfo)shippingGroup.getShippingAddress();

		pApplePayInfo.setShippingFirstName(shippingAddress.getFirstName());
		pApplePayInfo.setShippingLastName(shippingAddress.getLastName());
		pApplePayInfo.setShippingPhoneNo(shippingAddress.getPhoneNumber());

		pApplePayInfo.setShippingAddressLine1(shippingAddress.getAddress1());
		pApplePayInfo.setShippingAddressLine2(shippingAddress.getAddress2());
		pApplePayInfo.setShippingAddressLine3(shippingAddress.getAddress3());

		pApplePayInfo.setShippingState(shippingAddress.getState());
		pApplePayInfo.setShippingCity(shippingAddress.getCity());
		pApplePayInfo.setShippingPostalCode(shippingAddress.getPostalCode());
		pApplePayInfo.setShippingCountryCode(shippingAddress.getCountry());

		pApplePayInfo.setApplePayTransactionId(pPaymentGroup.getApplePayTransactionId());
		pApplePayInfo.setEphemeralPublicKey(pPaymentGroup.getEphemeralPublicKey());
		pApplePayInfo.setPublicKeyHash(pPaymentGroup.getPublickeyHash());
		pApplePayInfo.setData(pPaymentGroup.getData());
		pApplePayInfo.setSignature(pPaymentGroup.getSignature());
		pApplePayInfo.setVersion(pPaymentGroup.getVersion());

		if (isLoggingDebug()) {
			vlogDebug("ProcTRUCreateApplePayInfo.addDataToApplePayInfo : ENDS");
		}
	}

	/**
	 * Gets the apple pay info.
	 *
	 * @return TRUApplePayInfo
	 * @throws InstantiationException - InstantiationException
	 * @throws IllegalAccessException - IllegalAccessException
	 * @throws ClassNotFoundException - IllegalAccessException
	 */
	protected TRUApplePayInfo getApplePayInfo() 
	throws InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		if (isLoggingDebug()) {
			vlogDebug("Making a new instance of type: " + getApplePayInfoClass());
		}
		TRUApplePayInfo gcInfo = (TRUApplePayInfo)Class.forName(getApplePayInfoClass()).newInstance();

		return gcInfo;
	}

	/**
	 * Run process.
	 *
	 * @param pParam - Object
	 * @param pResult - PipelineResult
	 * @return Integer
	 * @throws InstantiationException - InstantiationException
	 * @throws IllegalAccessException - IllegalAccessException
	 * @throws ClassNotFoundException the class not found exception
	 */
	public int runProcess(Object pParam, PipelineResult pResult) 
			throws InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUCreateApplePayInfo.runProcess :STARTS");
		}
		PaymentManagerPipelineArgs params = (PaymentManagerPipelineArgs)pParam;
		Order order = params.getOrder();
		TRUApplePay ApplePay = (TRUApplePay)params.getPaymentGroup();
		double amount = params.getAmount();

		TRUApplePayInfo ApplePayInfo = getApplePayInfo();

		if (isLoggingDebug()) {
			vlogDebug("Params :{0},Order : {1},ApplePay : {2},Amount : {3}",params,order,ApplePay,amount);
		}
		addDataToApplePayInfo(order, ApplePay, amount, params, ApplePayInfo);

		if (isLoggingDebug()) {
			vlogDebug("Putting ApplePayInfo object into pipeline: " + ApplePayInfo.toString());
		}
		params.setPaymentInfo(ApplePayInfo);

		if (isLoggingDebug()) {
			vlogDebug("ProcTRUCreateApplePayInfo.runProcess :ENDS");
		}
		return SUCCESS;
	}

	/**
	 * Gets the ret codes.
	 *
	 * @return int[]
	 */
	public int[] getRetCodes()
	{
		int[] retCodes = { SUCCESS };
		return retCodes;
	}

}
