package com.tru.commerce.payment.processor;

import com.tru.commerce.payment.TRUPayPalPaymentInfo;
import com.tru.commerce.payment.paypal.TRUPayPalStatus;
import com.tru.radial.commerce.payment.paypal.util.TRUPayPalException;


/**
 * TRUPayPalProcessor is interface for paypalProcessor.
 * 
 * @author Professional Access
 * @version 1.0
 * 
 */
public interface TRUPayPalProcessor {

	/**
	 * Authorize the amount in PayPal Payment Group.
	 *
	 * @param pPayPalInfo the GameSaverInfo reference which contains
	 *        all the authorization data
	 * @param pPaymentStatus the payment status
	 * 
	 *@throws   TRUPayPalException the TRUPaypalexception
	 */
	void authorize(TRUPayPalPaymentInfo pPayPalInfo,TRUPayPalStatus pPaymentStatus) throws TRUPayPalException;
	
	
	/**
	 * Debit the amount in PayPal payment Group.
	 *
	 * @param pPayPalInfo the PayPalInfo reference which contains
	 *        all the debit data
	 * @return a PayPalStatus object detailing the results of the debit
	 * @throws TRUPayPalException - TRUPayPalException
	 */
	TRUPayPalStatus debit(TRUPayPalPaymentInfo pPayPalInfo) throws TRUPayPalException;

	
	/**
	 * Debit the amount in PayPal after authorization.
	 *
	 * @param pPayPalInfo the PayPalInfo reference which contains
	 *        all the debit data
	 * @param pStatus the PayPalStatus object which contains
	 *        information about the transaction. This should be the object
	 *        which was returned from authorize().
	 * @return a PayPalStatus object detailing the results of the debit
     * @throws TRUPayPalException the TRUPaypalexception
	 */
	TRUPayPalStatus debit(TRUPayPalPaymentInfo pPayPalInfo,
			TRUPayPalStatus pStatus) throws TRUPayPalException;

	
	/**
	 * Credit the amount in PayPal payment Group.
	 *
	 * @param pPayPalInfo the PayPalInfo reference which contains
	 *        all the debit data
	 *@param pPayPalStatus the paypalStatus
	 *@return a PayPalStatus object detailing the results of the debit
	 *@throws TRUPayPalException - TRUPayPalException
	 */	
	TRUPayPalStatus credit(TRUPayPalPaymentInfo pPayPalInfo, 
			TRUPayPalStatus pPayPalStatus) throws TRUPayPalException;

	/**
	 * Credit the amount in PayPal payment Group outside the context of an Order.
	 *
	 * @param pPayPalInfo the PayPalInfo reference which contains
	 *        all the credit data
	 * @return a PayPalStatus object detailing the results of the
	 *        credit
	 *@throws TRUPayPalException the TRUPaypalexception
	 */
	TRUPayPalStatus credit(TRUPayPalPaymentInfo pPayPalInfo) throws TRUPayPalException;


}
