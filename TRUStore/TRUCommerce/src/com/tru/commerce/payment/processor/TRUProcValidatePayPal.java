
package com.tru.commerce.payment.processor;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.processor.BillingAddrValidator;
import atg.commerce.order.processor.ValidatePaymentGroupPipelineArgs;
import atg.core.i18n.LayeredResourceBundle;
import atg.nucleus.GenericService;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.radial.commerce.payment.paypal.TRUPaypalConstants;

/**
 * This  class validates PayPal payment group's billing address.
 * 
 * @author Professional Access 
 * @version 1.0
 */
public class TRUProcValidatePayPal extends GenericService implements PipelineProcessor
{
	/**
	 * This property holds sUserResourceBundle.
	 */
	private static ResourceBundle sUserResourceBundle = LayeredResourceBundle.getBundle(TRUPaypalConstants.USER_MESSAGE_BUNDLE, Locale.getDefault());

	/**
	 * This property holds the success code.
	 */
	private static final  int SUCCESS_CODE = TRUPaypalConstants.ONE;
	/**
	 * This property holds the return codes.
	 */
	private static final int[] RETURN_CODES = { SUCCESS_CODE };
	
	/**
	 * property to hold mLoggingIdentifier.
	 */
	  private  String mLoggingIdentifier;
	/**
	 * property to hold mBillingAddressValidator.
	 */
	private BillingAddrValidator mBillingAddressValidator;
	
	/**
	 * Default constructor.
	 */
	public TRUProcValidatePayPal()
	{
		this.mLoggingIdentifier = TRUPaypalConstants.PAYPAL_LOGGING_IDENTIFIER;
	}

	/**
	 * @param pLoggingIdentifier the mLoggingIdentifier to set
	 */
	public void setLoggingIdentifier(String pLoggingIdentifier)
	{
		this.mLoggingIdentifier = pLoggingIdentifier;
	}

	/**
	 * Gets the loggingIdentifier.
	 * 
	 * @return the LoggingIdentifier
	 */
	public String getLoggingIdentifier()
	{
		return this.mLoggingIdentifier;
	}

	/**
	 * Sets the BillingAddressValidator.
	 * 
	 * @param pBillingAddressValidator the mBillingAddressValidator to set
	 */
	public void setBillingAddressValidator(BillingAddrValidator pBillingAddressValidator)
	{
		this.mBillingAddressValidator = pBillingAddressValidator;
	}

	/**
	 * Gets the BilingAddressValidatory.
	 * 
	 * @return the bBllingAddressValidator
	 */
	public BillingAddrValidator getBillingAddressValidator() {
		return this.mBillingAddressValidator;
	}

	/**
	 * This method will give return codes.
	 * 
	 * 
	 * @return retrun code
	 */
	@Override
	public int[] getRetCodes() {
		return RETURN_CODES;
	}

	/**
	 * This method will do  PayPal billing address validation.
	 * 
	 * @param pParamObject the paramObject
	 * @param pParamPipelineResult - the paramPipelineResult
	 * @return success  or failure
	 * @throws InvalidParameterException occurs while validating payment group
	 */
	@Override
	public int runProcess(Object pParamObject, PipelineResult pParamPipelineResult)
	throws  InvalidParameterException  {
		if (isLoggingDebug()) {
			vlogDebug("Start of  TRUProcValidatePayPal.runProcess() method");
		}

		ValidatePaymentGroupPipelineArgs args = (ValidatePaymentGroupPipelineArgs) pParamObject;
		PaymentGroup paymentGroup = args.getPaymentGroup();
		Locale locale = args.getLocale();
		ResourceBundle resourceBundle;
		if (locale == null){
			resourceBundle = sUserResourceBundle;
		}
		else {
			resourceBundle = LayeredResourceBundle.getBundle(TRUCheckoutConstants.USER_MESSAGE_BUNDLE, locale);
		}
		if (paymentGroup == null) {
			throw new InvalidParameterException(TRUPaypalConstants.INVALIDPAYMENTGROUP_PARAMETER);
		}
		if (!(paymentGroup instanceof TRUPayPal)) {
			throw new InvalidParameterException(TRUPaypalConstants.INVALIDPAYMENTGROUP_PARAMETER);
		}		

		if (isLoggingDebug()) {
			vlogDebug("Validating one PayPal of type " + paymentGroup.getPaymentGroupClassType());
		}

		validatePayPalBillingAddress((TRUPayPal)paymentGroup, pParamPipelineResult, resourceBundle);

		if (isLoggingDebug()) {
			vlogDebug("End of  TRUProcValidatePayPal.runProcess() method");
		}
		return SUCCESS_CODE;
	}
	
	/**
	 * This method will invoke the validateBillingAddress method to validate billing address.
	 * 
	 * @param pPayPal the paramObject
	 * @param pResult - the paramPipelineResult
	 * @param pResourceBundle - the resourceBundle	 
	 */
	
	protected void validatePayPalBillingAddress(TRUPayPal pPayPal, PipelineResult pResult, ResourceBundle pResourceBundle){
		if (isLoggingDebug()) {
			vlogDebug("Entering into TRUProcValidatePayPal.validatePayPalBillingAddress() method");
		}
		BillingAddrValidator bav = getBillingAddressValidator();
		bav.validateBillingAddress(pPayPal.getBillingAddress(), pPayPal.getId(), pResult, pResourceBundle);
		if (isLoggingDebug()) {
			vlogDebug("End of  TRUProcValidatePayPal.validatePayPalBillingAddress) method");
		}
	}

	/**
	 * THis method will add error to map of the pipelienResult other wise it will create map , add error to this map and  add this map to piplelistResult. 
	 * 
	 * @param pResult - the paramPipelineResult
	 * @param pKey - the thekey
	 * @param pId - the payment groupId
	 * @param pError  -- the error value
	 */
	
	protected void addHashedError(PipelineResult pResult, String pKey, String pId, Object pError){
		Object error = pResult.getError(pKey);
		if (error == null) {
			Map map = new HashMap(TRUPaypalConstants.DIGIT_FIVE);
			pResult.addError(pKey, map);
			map.put(pId, pError);
		}
		else if (error instanceof Map) {
			Map map = (Map)error;
			map.put(pId, pError);
		}
	}
}