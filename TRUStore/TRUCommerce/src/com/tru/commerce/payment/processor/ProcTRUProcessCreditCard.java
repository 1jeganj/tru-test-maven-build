package com.tru.commerce.payment.processor;
import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.payment.PaymentStatus;

import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardInfo;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardStatus;
import com.tru.commerce.payment.AbstractProcTRUProcessPaymentGroup;
import com.tru.commerce.payment.TRUPaymentManager;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * Class extends ProcTRUProcessPaymentGroup.
 * @author ORACLE
 * @version 1.0
 */
public class ProcTRUProcessCreditCard extends AbstractProcTRUProcessPaymentGroup {

	/**
	 * Property holds the PaymentManager.
	 */
	private TRUPaymentManager mPaymentManager;
	/*
	 * Property to hold the TRUConfiguration.
	 */
	private TRUConfiguration mTruConfiguration;
	
	/** Member variable to hold propery manager. */
	private TRUPropertyManager mPropertyManager;

	/**
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * @return the tRUConfiguration
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * @param pTRUConfiguration the tRUConfiguration to set
	 */
	public void setTruConfiguration(TRUConfiguration pTRUConfiguration) {
		mTruConfiguration = pTRUConfiguration;
	}
	
	/**
	 * This method validate the existing payment Group is authorize payment Group or not.
	 * 
	 * @param pParams - PaymentManagerPipelineArgs.
	 * @return PaymentStatus -- PaymentStatus
	 * @throws CommerceException -- if any 
	 */
	public PaymentStatus authorizePaymentGroup(PaymentManagerPipelineArgs pParams) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUProcessCreditCard - authorizePaymentGroupMethod");
		}
		Order order = (Order) pParams.getOrder();
		TRUCreditCardInfo creditCardInfo = (TRUCreditCardInfo) pParams.getPaymentInfo();
		TRUCreditCardStatus paymentStatus = null;
		// check cardinal properties to decide 3DS or full auth call
		paymentStatus = ((TRUCreditCardProcessor)getPaymentManager().getCreditCardProcessor()).fullAuthorization(creditCardInfo);
		
		if(paymentStatus.getTransactionSuccess()){
			((TRUCreditCard)pParams.getPaymentGroup()).setTransactionId(creditCardInfo.getTransactionId());
			if(order instanceof TRUOrderImpl){
				((TRUCreditCard)pParams.getPaymentGroup()).setRetryCount(((TRUOrderImpl)order).getRetryCount());
			}
			if(order instanceof TRULayawayOrderImpl){
				((TRUCreditCard)pParams.getPaymentGroup()).setRetryCount(((TRULayawayOrderImpl)order).getRetryCount());
			}
			if(paymentStatus.isRetryAcceptOrder()){
				((TRUCreditCard)pParams.getPaymentGroup()).setPaymentRetryAccept(true);
			}
			((TRUCreditCard)pParams.getPaymentGroup()).setResponseCode(paymentStatus.getResponseCode());
		} else {
			if(order instanceof TRUOrderImpl){
				((TRUOrderImpl)order).setRetryCount(((TRUOrderImpl)order).getRetryCount()+TRUConstants.INTEGER_NUMBER_ONE);
				((TRUOrderImpl)order).setPreviousResponseCode(paymentStatus.getResponseCode());
				((TRUOrderImpl)order).setPreviousCreditCardNumber(creditCardInfo.getCreditCardNumber());
			}
			if(order instanceof TRULayawayOrderImpl){
				((TRULayawayOrderImpl)order).setRetryCount(((TRULayawayOrderImpl)order).getRetryCount()+TRUConstants.INTEGER_NUMBER_ONE);
				((TRULayawayOrderImpl)order).setPreviousResponseCode(paymentStatus.getResponseCode());
				((TRULayawayOrderImpl)order).setPreviousCreditCardNumber(creditCardInfo.getCreditCardNumber());
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("Before invoking authorize method of credit card processor");
		}
		return paymentStatus;
	}
	/**
	 * Method reverseAuthorizePaymentGroup.
	 * 
	 * @param pParams -- PaymentManagerPipelineArgs
	 * @return PaymentStatus -- PaymentStatus
	 */
	public PaymentStatus reverseAuthorizePaymentGroup(PaymentManagerPipelineArgs pParams){
		
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUProcessCreditCard - authorizePaymentGroupMethod");
		}
		if (isLoggingDebug()) {
			vlogDebug("Before invoking authorize method of credit card processor");
		}
		return null;
	}

	/**
	 * @param pParam -- PaymentManagerPipeline Arguments
	 * @return PaymentStatus -- payment status
	 * @exception CommerceException -- if any
	 */
	public PaymentStatus creditPaymentGroup(PaymentManagerPipelineArgs pParam) throws CommerceException {
		 throw new UnsupportedOperationException();
	}

	/**
	 * @param  pArgs - PaymentManagerPipelineArgs
	 * @return PaymentStatus -- payment status
	 * @throws CommerceException -- if any
	 */
	public PaymentStatus debitPaymentGroup(PaymentManagerPipelineArgs pArgs) throws CommerceException {
		 throw new UnsupportedOperationException();
	}

	/**
	 * @param pPaymentManager the mPaymentManager to set
	 */
	public void setPaymentManager(TRUPaymentManager pPaymentManager) {
		this.mPaymentManager = pPaymentManager;
	}

	/**
	 * @return the mPaymentManager
	 */
	public TRUPaymentManager getPaymentManager() {
		return mPaymentManager;
	}


}