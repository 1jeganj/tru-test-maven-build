
package com.tru.commerce.payment.processor;

import java.beans.IntrospectionException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.ShippingGroup;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.service.idgen.IdGeneratorException;
import atg.userprofiling.address.AddressTools;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.payment.TRUPayPalPaymentInfo;
import com.tru.commerce.payment.paypal.TRUPayPalStatus;
import com.tru.common.TRUConstants;
import com.tru.common.TRUIntegrationConfiguration;
import com.tru.common.TRUSOSIntegrationConfig;
import com.tru.common.cml.SystemException;
import com.tru.radial.commerce.payment.paypal.TRUPaypalConstants;
import com.tru.radial.commerce.payment.paypal.util.TRUPayPalException;
import com.tru.radial.integration.util.TRUUIDGenerator;
import com.tru.radial.payment.paypal.PayPalAdaptor;
import com.tru.radial.payment.paypal.bean.AuthorizationInfo;
import com.tru.radial.payment.paypal.bean.DoAuthorizationPaymentRequest;
import com.tru.radial.payment.paypal.bean.DoAuthorizationPaymentResponse;
import com.tru.radial.payment.paypal.bean.DoExpressCheckoutPaymentRequest;
import com.tru.radial.payment.paypal.bean.DoExpressCheckoutPaymentResponse;
import com.tru.radial.payment.paypal.bean.LineItem;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This class will call the DOExcpress checkout.
 * Payment Processor Impl.
 * @author PA
 * @version 1.0
 *
 */
public class TRUPayPalProcessorImpl extends GenericService implements TRUPayPalProcessor {

	/**
	 * Property to hold instance of PayPalAdaptor.
	 */
	private PayPalAdaptor  mPayPalAdaptor;
	
	/** property to hold addressClassName instance. */
	private String mAddressClassName;
	
	/** property to hold propertyManager instance. */
	private TRUPropertyManager mPropertyManager;
	
	/**
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}
	
	/**
	 * Gets the address class name.
	 * 
	 * @return the addressClassName
	 */
	public String getAddressClassName() {
		return mAddressClassName;
	}

	/**
	 * Sets the address class name.
	 * 
	 * @param pAddressClassName
	 *            the AddressClassName to set
	 */
	public void setAddressClassName(String pAddressClassName) {
		mAddressClassName = pAddressClassName;
	}
	
	/**
	 * 	
	 * @return mPayPalAdaptor instance of PayPalAdaptor
	 */
	public PayPalAdaptor getPayPalAdaptor() {
		return mPayPalAdaptor;
	}
	
	/**
	 * @param pPayPalAdaptor the PayPalAdaptor to set
	 */
	public void setPayPalAdaptor(PayPalAdaptor pPayPalAdaptor) {
		this.mPayPalAdaptor = pPayPalAdaptor;
	}

	/**
	 * This method will do AUTH of PAYPAL payment group.
	 * 
	 * @param pPayPalInfo the paypalInfo
	 * @param pPaymentStatus the payment status
	 * @throws TRUPayPalException while TRUPayPalException
	 * 
	 */
	@Override
	public void authorize(TRUPayPalPaymentInfo pPayPalInfo,TRUPayPalStatus pPaymentStatus) throws TRUPayPalException {
		if(isLoggingDebug()){
			vlogDebug("Start of TRUPayPalProcessorImpl.authorize() method");
		}
		DoExpressCheckoutPaymentResponse lDoExpressResponse = null;
		DoExpressCheckoutPaymentRequest lDoExpRequest= new DoExpressCheckoutPaymentRequest();
		populateDoExpressCheckoutRequestAttributes(pPayPalInfo,lDoExpRequest);
		TRUPayPalStatus lPaymentStatus = pPaymentStatus;
		try {
			if(!enableRadialPayPal()) {
				throw new TRUPayPalException(TRUPaypalConstants.TRU_ERROR_RADIAL_PAYPAL_DISABLED);
			}
			lDoExpressResponse = (DoExpressCheckoutPaymentResponse) getPayPalAdaptor().doExpressCheckoutPayment(lDoExpRequest);
			if(lDoExpressResponse != null) {
				// populate do express checkout response to lPaymentStatus
				lPaymentStatus = populateDoExpressCheckoutResponseAttributes(lPaymentStatus, lDoExpressResponse);
				if(lDoExpressResponse.isSuccess()) {
					// PAYPAL AUTH request
					DoAuthorizationPaymentRequest lDoAuthRequest= new DoAuthorizationPaymentRequest();
					lDoAuthRequest.setTransactionId(lPaymentStatus.getTransactionId());
					lDoAuthRequest.setRequestId(lDoExpRequest.getRequestId());
					populateDoAuthCheckoutRequestAttributes(pPayPalInfo, lDoAuthRequest);
					DoAuthorizationPaymentResponse lDoAuthResponse = (DoAuthorizationPaymentResponse) getPayPalAdaptor().doAuthorizaton(lDoAuthRequest);
					if(lDoAuthResponse != null) {
						populateDoAuthCheckoutResponseAttributes(lPaymentStatus, lDoAuthResponse);
						// populate do AUTH checkout PAYPAL response	in lPaymentStatus
						if(lDoAuthResponse.isSuccess() && StringUtils.isNotBlank(pPayPalInfo.getAmount())){
							double amount=Double.parseDouble(pPayPalInfo.getAmount());
							lPaymentStatus.setAmount(amount);
						}else{
							if(lDoAuthResponse.isTimeOutResponse()){
								throw new TRUPayPalException(lDoAuthResponse.getRadialError().getErrorCode());
							}else{
								throw new TRUPayPalException(TRUPaypalConstants.PAYPAL_PAYMENT_AUTH_ERROR);
							}							
						}						
					} 
				}else{
					throw new TRUPayPalException(TRUPaypalConstants.PAYPAL_PAYMENT_AUTH_ERROR);
				}	
			} 
		} catch (SystemException sysE) {
			throw new TRUPayPalException(TRUPaypalConstants.PAYPAL_PAYMENT_AUTH_ERROR,sysE);
		}
		if(isLoggingDebug()){
			vlogDebug("End of TRUPayPalProcessorImpl.authorize() method");
		}		
	}
	
	/**
	 * No implementation is required.
	 * 
	 * @param pPayPalInfo the paypalInfo
	 * @param pStatus the payPalStatus
	 * @throws TRUPayPalException while PAYPAL Exception
	 * @return TRUPayPalStatus
	 */
	@Override
	public TRUPayPalStatus debit(TRUPayPalPaymentInfo pPayPalInfo, 
			TRUPayPalStatus pStatus) throws TRUPayPalException {
		if(isLoggingDebug()){
			vlogDebug("Start of TRUPayPalProcessorImpl.debit() method");
		}
		TRUPayPalStatus lPayPalStatus = pStatus;	
		if(isLoggingDebug()){
			vlogDebug("End of TRUPayPalProcessorImpl.debit() method");
		}
		return lPayPalStatus;
	}
	
	/**
	 * This method will do PayPal refund activity.
	 * 
	 * @param pPayPalInfo the paypalInfo
	 * @param pPayPalStatus the paypalStatus
	 * @throws TRUPayPalException while paypalException
	 * @return  TRUPayPalStatus
	 */
	@Override
	public TRUPayPalStatus credit(TRUPayPalPaymentInfo pPayPalInfo, 
			TRUPayPalStatus pPayPalStatus) throws TRUPayPalException {
		if(isLoggingDebug()){
			vlogDebug("Start of TRUPayPalProcessorImpl.credit() method");
		}
		TRUPayPalStatus lPayPalStatus = new TRUPayPalStatus();
				
		if(isLoggingDebug()){
			vlogDebug("End of TRUPayPalProcessorImpl.credit() method");
		}
		return lPayPalStatus;	
	}
	
	/**
	 * No implementation is required.
	 * 
	 * @param pPayPalInfo the paypalInfo
	 * @throws TRUPayPalException while paypal Exception
	 * @return  TRUPayPalStatus
	 */	
	@Override
	public TRUPayPalStatus debit(TRUPayPalPaymentInfo pPayPalInfo) throws TRUPayPalException {
		if(isLoggingDebug()){
			vlogDebug("Start of TRUPayPalProcessorImpl.debit() method");
		}		
		TRUPayPalStatus lPayPalStatus = new TRUPayPalStatus();	
		if(isLoggingDebug()){
			vlogDebug("End of TRUPayPalProcessorImpl.debit() method");
		}
		return lPayPalStatus;
	}

	/**
	 * This method will do PayPal refund activity.
	 * 
	 * @param pPayPalInfo the paypalInfo
	 * @return  TRUPayPalStatus
	 */
	@Override
	public TRUPayPalStatus credit(TRUPayPalPaymentInfo pPayPalInfo) {
		if(isLoggingDebug()){
			vlogDebug("Start of TRUPayPalProcessorImpl.credit() method");
		}		
		TRUPayPalStatus lPayPalStatus = new TRUPayPalStatus();	
		if(isLoggingDebug()){
			vlogDebug("End of TRUPayPalProcessorImpl.credit() method");
		}
		return lPayPalStatus;
	}	
	

	/**
	 * This method populates request for DoExpressCheckout API call.
	 * @param pPayPalInfo -holds PayPalInfo object
	 * @param pDoExpRequest -holds  PayPalDoExpCheckoutPaymentRequest object 
	 */
	private void populateDoExpressCheckoutRequestAttributes(TRUPayPalPaymentInfo pPayPalInfo, 
			DoExpressCheckoutPaymentRequest pDoExpRequest){
		if(isLoggingDebug()) {
			vlogDebug("Start of TRUPayPalProcessorImpl.populateDoExpressCheckoutRequestAttributes() method");
		}			
		TRUOrderImpl orderImpl=(TRUOrderImpl)pPayPalInfo.getOrder();		
		NumberFormat lNbrFrmt = NumberFormat.getNumberInstance();
		lNbrFrmt.setMaximumFractionDigits(TRUPaypalConstants.MAXIMUM_FRACTIONALDIGITS);		
		pDoExpRequest.setOrderId(pPayPalInfo.getOrderId());
		pDoExpRequest.setPageName(pPayPalInfo.getPageName());
		pDoExpRequest.setTokenId(pPayPalInfo.getPayPalToken());
		pDoExpRequest.setPayerId(pPayPalInfo.getPayerId());		
		pDoExpRequest.setAmount(Double.valueOf(pPayPalInfo.getAmount()));
	//	pDoExpRequest.setItemsAmount(Double.parseDouble(lNbrFrmt.format(amount-shipping)));
		pDoExpRequest.setItemsAmount(pPayPalInfo.getOrder().getPriceInfo().getRawSubtotal());
		pDoExpRequest.setCurrencyCode(pPayPalInfo.getCurrencyCode());
		try {
			pDoExpRequest.setRequestId(getUidGenerator().getIdGenerator().generateStringId());
		} catch (IdGeneratorException e) {
			if(isLoggingError()){
				vlogError("createDoExpressCheckoutRequest():IdGeneratorException while generating requestingId and exception details {0}",e);
			}
		}		
		//pDoExpCheckoutPaymentRequest.setPaymentAction(pPayPalInfo.getPaymentAction());
		pDoExpRequest.setShippingCost(new BigDecimal(lNbrFrmt.format(Double.parseDouble(pPayPalInfo.getShippingAmount()))));		
		pDoExpRequest.setTaxAmount(new BigDecimal(lNbrFrmt.format(pPayPalInfo.getOrder().getPriceInfo().getTax())));
		pDoExpRequest.setStanderdPayPal(((TRUOrderImpl)pPayPalInfo.getOrder()).isStanderdPayPal());
		pDoExpRequest.setExpressPayPal(((TRUOrderImpl)pPayPalInfo.getOrder()).isExpressPayPal());
		pDoExpRequest.setShipToName(pPayPalInfo.getShipToName());
		List<LineItem> lineItems=new ArrayList<LineItem>();
		LineItem lineItem=new LineItem();		
		List<CommerceItem> items=  orderImpl.getCommerceItems();
		Integer itemSize = items.size();
		for(int i=TRUConstants._0 ; i<itemSize ; i++){
			lineItem=new LineItem();
			RepositoryItem skuItem= (RepositoryItem) items.get(i).getAuxiliaryData().getCatalogRef();
			String name= (String)skuItem.getPropertyValue(getPropertyManager().getDisplayNamePropertyName());
			int qty = (int) items.get(i).getQuantity();
			double unitPrice = items.get(i).getPriceInfo().getSalePrice();
			lineItem.setName(name);
			lineItem.setQty(qty);
			lineItem.setSeqNo(i);
			lineItem.setUnitPrice(unitPrice);
			lineItems.add(lineItem);
		}
		pDoExpRequest.setLineItems(lineItems);		
		
		List<ShippingGroup> shippingGroupList = pPayPalInfo.getOrder().getShippingGroups();
		if (shippingGroupList != null && !shippingGroupList.isEmpty()) {
			for (ShippingGroup shippingGroup : shippingGroupList) {
				if (shippingGroup instanceof TRUInStorePickupShippingGroup) {
					pDoExpRequest.setPickUpStoreId(((TRUInStorePickupShippingGroup)shippingGroup).getLocationId());
					break;
				}
			}
		}
		Map<String,String> shippingAddressMap = populateShippingInfo(pPayPalInfo);
		if(shippingAddressMap != null) {
			ContactInfo shippingAddress = null;
			try {
				shippingAddress = (ContactInfo) AddressTools.createAddressFromMap(shippingAddressMap, getAddressClassName());
			} catch (InstantiationException instantiationException) {
				if (isLoggingError()) {
					logError("Exception in  @Class::TRUPayPalProcessorImpl::@method::populateDoExpressCheckoutRequestAttributes() while adding shipping address", instantiationException);
				}
			} catch (IllegalAccessException illegalAccessException) {
				if (isLoggingError()) {
					logError("Exception in @Class::TRUPayPalProcessorImpl::@method::populateDoExpressCheckoutRequestAttributes() while adding shipping address", illegalAccessException);
				}
			} catch (ClassNotFoundException classNotFoundException) {
				if (isLoggingError()) {
					logError("Exception in @Class::TRUPayPalProcessorImpl::@method::populateDoExpressCheckoutRequestAttributes() while adding shipping address", classNotFoundException);
				}
			}catch (IntrospectionException introspectionException) {
				if (isLoggingError()) {
					logError("Exception in @Class::TRUPayPalProcessorImpl::@method::populateDoExpressCheckoutRequestAttributes() while adding shipping address", introspectionException);
				}
			}
			if(shippingAddress != null) {
				pDoExpRequest.setShippingAddress(shippingAddress);
			}
		}
		if(isLoggingDebug()){
			vlogDebug("End of TRUPayPalProcessorImpl.populateDoExpressCheckoutRequestAttributes() method");
		}
	}
	
	/**
	 * This method populates request for DoExpressCheckout API call.
	 * @param pPayPalInfo -holds PayPalInfo object
	 * @param pDoAuthRequest -holds  PayPalDoExpCheckoutPaymentRequest object 
	 */
	private void populateDoAuthCheckoutRequestAttributes(TRUPayPalPaymentInfo pPayPalInfo, 
			DoAuthorizationPaymentRequest pDoAuthRequest){
		if(isLoggingDebug()){
			vlogDebug("Start of TRUPayPalProcessorImpl.populateDoAuthCheckoutRequestAttributes() method");
		}		
		pDoAuthRequest.setOrderId(pPayPalInfo.getOrderId());
		pDoAuthRequest.setPageName(pPayPalInfo.getPageName());
		pDoAuthRequest.setAmount(Double.valueOf(pPayPalInfo.getAmount()));
		pDoAuthRequest.setCurrencyCode(pPayPalInfo.getCurrencyCode());
		pDoAuthRequest.setPaymentAction(pPayPalInfo.getPaymentAction());
		pDoAuthRequest.setIpAddress(pPayPalInfo.getBuyerIpAddress());
		//pDoAuthCheckoutPaymentRequest.setTransactionId(lTransactionId);
		//populate shipping address
		Map<String,String> shippingAddressMap = populateShippingInfo(pPayPalInfo);
		if(shippingAddressMap != null) {
			ContactInfo shippingAddress = null;
			try {
				shippingAddress = (ContactInfo) AddressTools.createAddressFromMap(shippingAddressMap, getAddressClassName());
			} catch (InstantiationException instantiationException) {
				if (isLoggingError()) {
					logError("Exception in @Class::TRUPayPalProcessorImpl::@method::populateDoAuthCheckoutRequestAttributes() while adding shipping address", instantiationException);
				}
			} catch (IllegalAccessException illegalAccessException) {
				if (isLoggingError()) {
					logError("Exception in @Class::TRUPayPalProcessorImpl::@method::populateDoAuthCheckoutRequestAttributes() while adding shipping address", illegalAccessException);
				}
			} catch (ClassNotFoundException classNotFoundException) {
				if (isLoggingError()) {
					logError("Exception in @Class::TRUPayPalProcessorImpl::@method::populateDoAuthCheckoutRequestAttributes() while adding shipping address", classNotFoundException);
				}
			}catch (IntrospectionException introspectionException) {
				if (isLoggingError()) {
					logError("Exception in @Class::TRUPayPalProcessorImpl::@method::populateDoAuthCheckoutRequestAttributes() while adding shipping address", introspectionException);
				}
			}
			if(shippingAddress != null) {
				pDoAuthRequest.setShippingAddress(shippingAddress);
			}
		}
		if(isLoggingDebug()){
			vlogDebug("End of TRUPayPalProcessorImpl.populateDoAuthCheckoutRequestAttributes() method");
		}
	}
	
	/**
	 * This method populates request for DoVoid API call.
	 * @param pPayPalInfo -holds PayPalInfo object
	 * @param pDoVoidPaymentRequest -holds  PayPalDoVoidPaymentRequest object 
	 * @param pStatus -holds  TRUPayPalStatus object
	 */
	/*private void populateDoVoidRequestAttributes(TRUPayPalPaymentInfo pPayPalInfo, 
			DoVoidRequest pDoVoidPaymentRequest,TRUPayPalStatus pStatus){
		if(isLoggingDebug()){
			vlogDebug("Start of TRUPayPalProcessorImpl.populateDoVoidRequestAttributes() method");
		}
		pDoVoidPaymentRequest.setAuthorizationId(pStatus.getTransactionId());
		pDoVoidPaymentRequest.setSiteId(pPayPalInfo.getSiteId());
		pDoVoidPaymentRequest.setNote(pPayPalInfo.getPaymentAction());
		if(isLoggingDebug()){
			vlogDebug("End of TRUPayPalProcessorImpl.populateDoVoidRequestAttributes() method");
		}
	}*/
	
	/**
	 * Populating the shipping Information.
	 * @param pPayPalInfo the GetExpressCheckoutResponse
	 * 			
	 * @return shippingInfo map
	 */
	public Map<String,String> populateShippingInfo(TRUPayPalPaymentInfo pPayPalInfo){
		if (isLoggingDebug()) {
			vlogDebug("START : populateShippingInfo method of the TRUPayPalProcessorImpl");
		}	 
		if(pPayPalInfo == null){
			return null;
		}
		Map<String,String> shippingAddressMap = new HashMap<String,String>();
		if(!StringUtils.isBlank(pPayPalInfo.getShipToFirstName())){
			shippingAddressMap.put(getPropertyManager().getAddressFirstNamePropertyName(), pPayPalInfo.getShipToFirstName());
		}
		if(!StringUtils.isBlank(pPayPalInfo.getShipToFirstName())){
			shippingAddressMap.put(getPropertyManager().getAddressLastNamePropertyName(), pPayPalInfo.getShipToFirstName());
		}
		if(!StringUtils.isBlank(pPayPalInfo.getShipToStreet1())){
			shippingAddressMap.put(getPropertyManager().getAddressLineOnePropertyName(), pPayPalInfo.getShipToStreet1());
		}
		if(!StringUtils.isBlank(pPayPalInfo.getShipToStreet2())){
			shippingAddressMap.put(getPropertyManager().getAddressLineTwoPropertyName(), pPayPalInfo.getShipToStreet2());
		}
		if(!StringUtils.isBlank(pPayPalInfo.getShipToCity())){
			shippingAddressMap.put(getPropertyManager().getAddressCityPropertyName(), pPayPalInfo.getShipToCity());
		}
		if(!StringUtils.isBlank(pPayPalInfo.getShipToState())){
			shippingAddressMap.put(getPropertyManager().getAddressStatePropertyName(), pPayPalInfo.getShipToState());
		}
		if(!StringUtils.isBlank(pPayPalInfo.getCountryCode())){
			shippingAddressMap.put(getPropertyManager().getAddressCountryPropertyName(), pPayPalInfo.getCountryCode());
		}
		//TODO Need to get country code Dynamically 
		pPayPalInfo.setCountryCode(TRUConstants.UNITED_STATES);
		if(!StringUtils.isBlank(pPayPalInfo.getShipToZip())){
			shippingAddressMap.put(getPropertyManager().getAddressPostalCodePropertyName(), pPayPalInfo.getShipToZip());
		}
		if(!StringUtils.isBlank(pPayPalInfo.getShipToPhoneNumber())){
			shippingAddressMap.put(getPropertyManager().getAddressPhoneNumberPropertyName(), pPayPalInfo.getShipToPhoneNumber());
		}
		
		if (isLoggingDebug()) {
			vlogDebug("END : populateShippingInfo method of the TRUPayPalProcessorImpl");
		}
		return shippingAddressMap;
	}
	
	/**
	 * This method populates response from DoExpressCheckout API call to PayPalStatus.
	 * @param pPayPalStatus -holds PayPalStatus object
	 * @param pDoExpResponse -holds  PayPalDoExpCheckoutPaymentResponse object
	 * @return TRUPayPalStatus - holds payPalStatus object
	 */
	private TRUPayPalStatus populateDoExpressCheckoutResponseAttributes(TRUPayPalStatus pPayPalStatus, 
			DoExpressCheckoutPaymentResponse pDoExpResponse){
		if(isLoggingDebug()){
			vlogDebug("Start of TRUPayPalProcessorImpl.populateDoExpressCheckoutResponseAttributes() method");
		}
		pPayPalStatus.setResponseCode(pDoExpResponse.getResponseCode());
		pPayPalStatus.setTransactionSuccess(pDoExpResponse.isSuccess());
		pPayPalStatus.setTransactionId(pDoExpResponse.getTranscationId());
		pPayPalStatus.setTransactionTimestamp(parseDate(pDoExpResponse.getTimeStamp()));		
		if(!StringUtils.isBlank(String.valueOf(pDoExpResponse.getAmount()))){
			pPayPalStatus.setAmount(Double.valueOf(pDoExpResponse.getAmount()));
		}
		if(!pDoExpResponse.isSuccess()){
			if(null != pDoExpResponse.getRadialError()){
				pPayPalStatus.setErrorMessage(pDoExpResponse.getRadialError().getErrorMessage());
				pPayPalStatus.setPayPalAuthShortErrorMessage(pDoExpResponse.getRadialError().getErrorMessage());
				pPayPalStatus.setPayPalAuthErrorCode(pDoExpResponse.getRadialError().getErrorCode());
			}						
			pPayPalStatus.setRadialError(pDoExpResponse.getRadialError());
		}
		if(pDoExpResponse.getPaymentInfoResp()!=null){
			pPayPalStatus.setPayPalPaymentStatus(pDoExpResponse.getPaymentInfoResp().getPaymentStatus());
			pPayPalStatus.setPayPalPaymentPendingReason(pDoExpResponse.getPaymentInfoResp().getPendingReasons());
			pPayPalStatus.setPayPalPaymentReasonCode(pDoExpResponse.getPaymentInfoResp().getReasonCode());
		}		
				
		if(isLoggingDebug()){
			vlogDebug("End of TRUPayPalProcessorImpl.populateDoExpressCheckoutResponseAttributes() method");
		}
		return pPayPalStatus;
	}
	
	/**
	 * This method populates response from DoAuthCheckout API call to PayPalStatus.
	 * @param pPayPalStatus -holds PayPalStatus object
	 * @param pDoAuthCheckoutResponse -holds  DoAuthorizationPaymentResponse object
	 *
	 */
	private void populateDoAuthCheckoutResponseAttributes(TRUPayPalStatus pPayPalStatus, 
			DoAuthorizationPaymentResponse pDoAuthCheckoutResponse){
		if(isLoggingDebug()){
			vlogDebug("Start of TRUPayPalProcessorImpl.populateDoExpressCheckoutResponseAttributes() method");
		}
		pPayPalStatus.setResponseCode(pDoAuthCheckoutResponse.getResponseCode());
		pPayPalStatus.setPayPalAuthResponseCode(pDoAuthCheckoutResponse.getResponseCode() );
		pPayPalStatus.setTransactionSuccess(pDoAuthCheckoutResponse.isSuccess());
		pPayPalStatus.setTransactionTimestamp(parseDate(pDoAuthCheckoutResponse.getTimeStamp()));
		
		AuthorizationInfo authorizationInfo= pDoAuthCheckoutResponse.getAuthorizationInfo();		
		if(authorizationInfo!=null){
			pPayPalStatus.setPayPalAuthPendingReason(authorizationInfo.getPendingReason());
			pPayPalStatus.setPayPalAuthStatus(authorizationInfo.getPaymentStatus());			
			pPayPalStatus.setPayPalAuthReasonCode(authorizationInfo.getReasonCode());						
		}				
		/*List<PayPalError> palErrors = pDoAuthCheckoutPaymentResponse.getErrors();
		pPayPalStatus.setErrors(palErrors);*/
		if(!pDoAuthCheckoutResponse.isSuccess()){			
			if(null!=pDoAuthCheckoutResponse.getRadialError()){
				pPayPalStatus.setErrorMessage(pDoAuthCheckoutResponse.getRadialError().getErrorMessage());
				pPayPalStatus.setPayPalAuthShortErrorMessage(pDoAuthCheckoutResponse.getRadialError().getErrorMessage());
				pPayPalStatus.setPayPalAuthErrorCode(pDoAuthCheckoutResponse.getRadialError().getErrorCode());
			}				
			pPayPalStatus.setRadialError(pDoAuthCheckoutResponse.getRadialError());			
		}
			
		if(isLoggingDebug()){
			vlogDebug("End of TRUPayPalProcessorImpl.populateDoExpressCheckoutResponseAttributes() method");
		}
	}
	
	/**
	 * This method populates response from DoVoid API call to PayPalStatus.
	 * @param pPayPalStatus -holds PayPalStatus object
	 * @param pDoVoidPaymentResponse -holds  DoVoidPaymentResponse object
	 * @return TRUPayPalStatus - holds payPalStatus object
	 */
	/*private TRUPayPalStatus populateDoVoidResponseAttributes(TRUPayPalStatus pPayPalStatus, 
			DoVoidResponse pDoVoidPaymentResponse){
		if(isLoggingDebug()){
			vlogDebug("Start of TRUPayPalProcessorImpl.populateDoVoidResponseAttributes() method");
		}
		//pPayPalStatus.setSuccess(pDoVoidPaymentResponse.isSuccess());
		pPayPalStatus.setTransactionSuccess(pDoVoidPaymentResponse.isSuccess());
		pPayPalStatus.setTransactionId(pDoVoidPaymentResponse.getAuthorizationId());		
		//pPayPalStatus.setErrorCode(pDoAuthCheckoutPaymentResponse.getErrorCode());
		pPayPalStatus.setErrorMessage(pDoVoidPaymentResponse.getStatusMessage());		
		if(isLoggingDebug()){
			vlogDebug("End of TRUPayPalProcessorImpl.populateDoVoidResponseAttributes() method");
		}
		return pPayPalStatus;
	}*/
	
	
	/** The m uid generator. */
	private TRUUIDGenerator mUidGenerator;
	
	/**
	 * Gets the uid generator.
	 *
	 * @return the uid generator
	 */
	public TRUUIDGenerator getUidGenerator() {
		return mUidGenerator;
	}
	
	/**
	 * Sets the uid generator.
	 *
	 * @param pUidGenerator the new uid generator
	 */
	public void setUidGenerator(TRUUIDGenerator pUidGenerator) {
		this.mUidGenerator = pUidGenerator;
	}

	/**
	 * Parses the date.
	 *
	 * @param pTimeStamp the time stamp
	 * @return the date
	 */
	private Date parseDate(String pTimeStamp){
		Date date = null;
		if(!StringUtils.isBlank(pTimeStamp)){
			DateFormat formatter = null ;			
			formatter = new SimpleDateFormat(TRUPaypalConstants.ORDER_DATE_FORMAT);
			try {
				date = (Date)formatter.parse(pTimeStamp);
			} catch (ParseException e) {
				if (isLoggingError()) {
					logError("ParseException in parseDate " +
							"while parsing string @Class::TRUPayPalProcessorImpl::@method::parseDate()"+pTimeStamp,e);
				}
			}			
		}	
		return date;
	}
	
	/**
	 * hold boolean property integrationConfig.
	 */
	private TRUIntegrationConfiguration mIntegrationConfig;
	
	/** The m site context mSiteContextManager. */
	private SiteContextManager mSiteContextManager;

	/**
	 * @return the siteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * @param pSiteContextManager the siteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}

	/** The mSosIntegrationConfiguration. */
	private TRUSOSIntegrationConfig mSosIntegrationConfiguration;

	/**
	 * @return the sosIntegrationConfiguration
	 */
	public TRUSOSIntegrationConfig getSosIntegrationConfiguration() {
		return mSosIntegrationConfiguration;
	}

	/**
	 * @param pSosIntegrationConfiguration the sosIntegrationConfiguration to set
	 */
	public void setSosIntegrationConfiguration(
			TRUSOSIntegrationConfig pSosIntegrationConfiguration) {
		mSosIntegrationConfiguration = pSosIntegrationConfiguration;
	}

	/**
	 * @return the integrationConfig
	 */
	public TRUIntegrationConfiguration getIntegrationConfig() {
		return mIntegrationConfig;
	}

	/**
	 * @param pIntegrationConfig the integrationConfig to set
	 */
	public void setIntegrationConfig(TRUIntegrationConfiguration pIntegrationConfig) {
		mIntegrationConfig = pIntegrationConfig;
	}
	
	/**
	 * Enable RadialGiftCard
	 *
	 * @return true, if successful
	 */
	private boolean enableRadialPayPal() {
		if (isLoggingDebug()) {
			vlogDebug("@class:TRUPaypalUtilityManager/enableRadialPayPal method start");
		}
		boolean lEnabled = Boolean.FALSE;
		Site site=null;
		site=SiteContextManager.getCurrentSite();
		if(site != null && site.getId().equalsIgnoreCase(TRUCommerceConstants.SOS)) {
			lEnabled = getSosIntegrationConfiguration().isEnableRadialPayPal();
			if (isLoggingDebug()) {
				vlogDebug("@class:TRUPaypalUtilityManager/enableRadialPayPal is  radial PayPal  service enabled in SOS : {0} ",lEnabled);
			}
		} else{
			lEnabled = getIntegrationConfig().isEnableRadialPayPal();
			if (isLoggingDebug()) {
				vlogDebug("@class:TRUPaypalUtilityManager/enableRadialPayPal is  radial PayPal  service enabled in store : {0} ",lEnabled);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@class:TRUPaypalUtilityManager/enableRadialPayPal method end");
		}
		return lEnabled;
	}
}