package com.tru.commerce.payment.processor;

import java.util.Date;

import atg.nucleus.logging.ApplicationLoggingImpl;

import com.tru.commerce.order.payment.TRUPaymentTools;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardInfo;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardStatus;
import com.tru.common.TRUErrorKeys;
import com.tru.common.TRUStoreConfiguration;
import com.tru.radial.exception.TRURadialIntegrationException;

/**
 * This class overrides the ApplicationLoggingImpl.
 * It is  used to authorize the payment and  Reverses authorization with AJB service.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUGiftCardProcessor extends ApplicationLoggingImpl {

	/**
	 * Property to hold the PaymentTools.
	 */
	private TRUPaymentTools mPaymentTools;
	/**
	 * Property to hold the StoreConfig.
	 */
	private TRUStoreConfiguration mStoreConfig;
	/**
	 * Property to hold the payeezyEnabled flag.
	 */
	private boolean mPayeezyEnabled;

	/**
	 * property to hold Negative testing.
	 */
	private boolean mEnableNegativeTesting;
	/**
	 * property to hold negative testing for authorization.
	 */
	private boolean mEnableFailAuthorization;
	/**
	 * property to hold Negative testing for fraud.
	 */
	private boolean mEnableFailFraud;

	/**
	 * Checks if is enable negative testing.
	 * 
	 * @return the enableNegativeTesting
	 */
	public boolean isEnableNegativeTesting() {
		return mEnableNegativeTesting;
	}

	/**
	 * Sets the enable negative testing.
	 * 
	 * @param pEnableNegativeTesting
	 *            the enableNegativeTesting to set
	 */
	public void setEnableNegativeTesting(boolean pEnableNegativeTesting) {
		mEnableNegativeTesting = pEnableNegativeTesting;
	}

	/**
	 * Checks if is enable fail authorization.
	 * 
	 * @return the enableFailAuthorization
	 */
	public boolean isEnableFailAuthorization() {
		return mEnableFailAuthorization;
	}

	/**
	 * Sets the enable fail authorization.
	 * 
	 * @param pEnableFailAuthorization
	 *            the enableFailAuthorization to set
	 */
	public void setEnableFailAuthorization(boolean pEnableFailAuthorization) {
		mEnableFailAuthorization = pEnableFailAuthorization;
	}

	/**
	 * Checks if is enable fail fraud.
	 * 
	 * @return the enableFailFraud
	 */
	public boolean isEnableFailFraud() {
		return mEnableFailFraud;
	}

	/**
	 * Sets the enable fail fraud.
	 * 
	 * @param pEnableFailFraud
	 *            the enableFailFraud to set
	 */
	public void setEnableFailFraud(boolean pEnableFailFraud) {
		mEnableFailFraud = pEnableFailFraud;
	}

	/**
	 * Checks if is payeezy enabled.
	 * 
	 * @return the payeezyEnabled
	 */
	public boolean isPayeezyEnabled() {
		return mPayeezyEnabled;
	}

	/**
	 * Sets the payeezy enabled.
	 * 
	 * @param pPayeezyEnabled
	 *            the payeezyEnabled to set
	 */
	public void setPayeezyEnabled(boolean pPayeezyEnabled) {
		mPayeezyEnabled = pPayeezyEnabled;
	}

	/**
	 * Gets the payment tools.
	 * 
	 * @return the paymentTools component.
	 */
	public TRUPaymentTools getPaymentTools() {
		return mPaymentTools;
	}

	/**
	 * Sets the payment tools.
	 * 
	 * @param pPaymentTools
	 *            the paymentTools to set
	 */
	public void setPaymentTools(TRUPaymentTools pPaymentTools) {
		mPaymentTools = pPaymentTools;
	}

	/**
	 * Gets the store config.
	 * 
	 * @return the storeConfig
	 */
	public TRUStoreConfiguration getStoreConfig() {
		return mStoreConfig;
	}

	/**
	 * Sets the store config.
	 * 
	 * @param pStoreConfig
	 *            the storeConfig to set
	 */
	public void setStoreConfig(TRUStoreConfiguration pStoreConfig) {
		mStoreConfig = pStoreConfig;
	}

	/**
	 * Reverses authorization. This method should also handle the error codes
	 *
	 * @param pGiftCardInfo            the GiftCardInfo reference which contains all the
	 *            authorization data
	 * @return a GiftCardStatus object detailing the results of the
	 *         authorization
	 * @throws TRURadialIntegrationException the TRU radial integration exception
	 */
	public TRUGiftCardStatus reverseAuthorize(TRUGiftCardInfo pGiftCardInfo) throws TRURadialIntegrationException {

		if (isLoggingDebug()) {
			vlogDebug("In Reverse Authorization method of gift card processor");
		}
		final TRUGiftCardStatus giftCardStatus = new TRUGiftCardStatus();
		if (isLoggingDebug()) {
			vlogDebug("Gift card processor - Before invoking rev auth payment method webservice ");
		}

		getPaymentTools().giftCardVoidRedeem(pGiftCardInfo, giftCardStatus);
		
		String responseCode = giftCardStatus.getResponseCode();
		
		if(responseCode != null){
			responseCode = responseCode.toLowerCase();
		}
		if (isLoggingDebug()) {
			logDebug("giftCardStatus.getResponseCode() "+ giftCardStatus.getResponseCode());
		}
		if(responseCode != null){
			if (getPaymentTools().getGiftCardRespSuccessCodes().contains(responseCode)) {
				giftCardStatus.setTransactionSuccess(true);
				giftCardStatus.setTransactionTimestamp(new Date());
				giftCardStatus.setTransactionId(pGiftCardInfo.getTransactionId());
			} else {
				giftCardStatus.setTransactionSuccess(false);
				giftCardStatus.setErrorMessage(getPaymentTools().getRedeemVoidResponseCodes().get(responseCode));
				giftCardStatus.setTransactionTimestamp(new Date());
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("In Reverse Authorization method of gift card processor with " + "gift card status"
					+ giftCardStatus.getTransactionSuccess());
		}
		return giftCardStatus;

	}
	/**
	 * This method will validate the gift card payment group.
	 * 
	 * @param pGiftCardInfo
	 *            the Gift Card Info to validate.
	 * @return TRUGiftCardStatus - gift card status.
	 * 
	 */
	public TRUGiftCardStatus giftCardRedeem(TRUGiftCardInfo pGiftCardInfo) {
		if (isLoggingDebug()) {
			vlogDebug("TRUGiftCardProcessor/giftCardRedeem method start");
			vlogDebug("TRUGiftCardInfo :{0}", pGiftCardInfo);
		}
		final TRUGiftCardStatus giftCardStatus = new TRUGiftCardStatus();
		final TRUGiftCardInfo giftCardInfo = (TRUGiftCardInfo) pGiftCardInfo;
		try {
			//Invoking the balance inquiry service.
			getPaymentTools().giftCardRedeem(giftCardInfo, giftCardStatus);
			String responseCode = giftCardStatus.getResponseCode();
			if(responseCode != null){
				responseCode = responseCode.toLowerCase();
			}
			if (isLoggingDebug()) {
				logDebug("giftCardStatus.getResponseCode() "+ giftCardStatus.getResponseCode());
			}
			if(responseCode != null){
				if (getPaymentTools().getGiftCardRespSuccessCodes().contains(responseCode)) {
					giftCardStatus.setTransactionSuccess(true);
					giftCardStatus.setTransactionTimestamp(new Date());
					giftCardStatus.setTransactionId(pGiftCardInfo.getTransactionId());
				} else {
					giftCardStatus.setTransactionSuccess(false);
					giftCardStatus.setErrorMessage(getPaymentTools().getFailureGiftCardRedeemResponseCodes().get(responseCode));
					giftCardStatus.setTransactionTimestamp(new Date());
				}
			}
		} catch (TRURadialIntegrationException payIntExp) {
			vlogError("TRURadialIntegrationException at TRUGiftCardProcessor/giftCardRedeem method :{0}",
					payIntExp);
			giftCardStatus.setTransactionSuccess(false);
			giftCardStatus.setErrorMessage(TRUErrorKeys.TRU_ERROR_TRY_AGAIN_AFTER_SOME_TIME);
		}
		vlogDebug("In Authorization method of gift card processor with credit card status :{0} ErrorMessage :{1}",
					giftCardStatus.getTransactionSuccess(),giftCardStatus.getErrorMessage());
		vlogDebug("TRUGiftCardProcessor/giftCardRedeem method End");
		return giftCardStatus;
	}
	
}