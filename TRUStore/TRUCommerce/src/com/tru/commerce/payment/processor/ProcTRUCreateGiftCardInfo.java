package com.tru.commerce.payment.processor;

import atg.commerce.order.Order;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.nucleus.GenericService;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.order.TRUGiftCard;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardInfo;
import com.tru.common.TRUConfiguration;

/**
 * This method implements OOTB PipelineProcessor.
 * @author PA
 * @version 1.0
 */
public class ProcTRUCreateGiftCardInfo extends GenericService	implements PipelineProcessor{
	/**
	 * Property holds the Constant Success Code.
	 */
	private static final int SUCCESS = 1;
	/**
	 * Property holds the Constant TEST.
	 */
	private static final String TEST= TRUPipeLineConstants.TEST;
	/**
	 * Property holds the gift card info.
	 */
	private String mGiftCardInfoClass;
	
	/**
	 * Property to hold the TRUConfiguration.
	 */
	private TRUConfiguration mTruConfiguration;

	/**
	 * @return the tRUConfiguration
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * @param pTRUConfiguration the tRUConfiguration to set
	 */
	public void setTruConfiguration(TRUConfiguration pTRUConfiguration) {
		mTruConfiguration = pTRUConfiguration;
	}
	/**
	 * @return String
	 */ 
	public String getGiftCardInfoClass()
	{
		return this.mGiftCardInfoClass;
	}

	/**
	 * @param pGiftCardInfoClass - String
	 */
	public void setGiftCardInfoClass(String pGiftCardInfoClass)
	{
		this.mGiftCardInfoClass = pGiftCardInfoClass;
	}
	/**
	 *  ProcTRUCreateGiftCardInfo
	 */
	public ProcTRUCreateGiftCardInfo()
	{
		this.mGiftCardInfoClass = TRUPipeLineConstants.GIFTCARD_INFO_CLASS;
	}
	/**
	 * @param pOrder - Order
	 * @param pPaymentGroup - TRUGiftCardPG
	 * @param pAmount - double
	 * @param pParams - PaymentManagerPipelineArgs
	 * @param pGiftCardInfo - TRUGiftCardInfo
	 */
	protected void addDataToGiftCardInfo(Order pOrder, TRUGiftCard pPaymentGroup, double pAmount, PaymentManagerPipelineArgs pParams, TRUGiftCardInfo pGiftCardInfo)
	{
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUCreateGiftCardInfo.addDataToGiftCardInfo :STARTS");
		}
		pGiftCardInfo.setAmount(pPaymentGroup.getAmount());		
		pGiftCardInfo.setGiftCardNumber(pPaymentGroup.getGiftCardNumber());
		String emailId = null;
		if(pOrder instanceof TRUOrderImpl){
			emailId  = ((TRUOrderImpl)pOrder).getEmail();
		}
		//TODO : Start : Needs to remove once the order email sets propertly.
		if(emailId == null){
			emailId = TEST;
		}
		//TODO : Start : Needs to remove once the order email sets propertly.
		pGiftCardInfo.setCardHolderName(emailId);
		pGiftCardInfo.setCurrencyCode(pPaymentGroup.getCurrencyCode());
		pGiftCardInfo.setPin(pPaymentGroup.getGiftCardPin());		
		pGiftCardInfo.setOrderId(pOrder.getId());
		pGiftCardInfo.setPaymentId(pPaymentGroup.getId());
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUCreateGiftCardInfo.addDataToGiftCardInfo : ENDS");
		}
	}

	/**
	 * @return TRUGiftCardInfo
	 * @throws InstantiationException - InstantiationException
	 * @throws IllegalAccessException - IllegalAccessException
	 * @throws ClassNotFoundException - IllegalAccessException
	 */
	protected TRUGiftCardInfo getGiftCardInfo() 
	throws InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		if (isLoggingDebug()) {
			vlogDebug("Making a new instance of type: " + getGiftCardInfoClass());
		}
		TRUGiftCardInfo gcInfo = (TRUGiftCardInfo)Class.forName(getGiftCardInfoClass()).newInstance();

		return gcInfo;
	}

	/**
	 * @param pParam - Object
	 * @param pResult - PipelineResult
	 * @return Integer
	 * @throws InstantiationException - InstantiationException
	 * @throws IllegalAccessException - IllegalAccessException
	 * @throws  ClassNotFoundException - ClassNotFoundException
	 */
	public int runProcess(Object pParam, PipelineResult pResult)
	throws InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUCreateGiftCardInfo.runProcess :STARTS");
		}
		PaymentManagerPipelineArgs params = (PaymentManagerPipelineArgs)pParam;
		Order order = params.getOrder();
		TRUGiftCard giftCard = (TRUGiftCard)params.getPaymentGroup();
		double amount = params.getAmount();

		TRUGiftCardInfo giftCardInfo = getGiftCardInfo();

		if (isLoggingDebug()) {
			vlogDebug("Params :{0},Order : {1},GiftCard : {2},Amount : {3}",params,order,giftCard,amount);
		}
		addDataToGiftCardInfo(order, giftCard, amount, params, giftCardInfo);

		if (isLoggingDebug()) {
			vlogDebug("Putting GiftCardInfo object into pipeline: " + giftCardInfo.toString());
		}
		params.setPaymentInfo(giftCardInfo);

		if (isLoggingDebug()) {
			vlogDebug("ProcTRUCreateGiftCardInfo.runProcess :ENDS");
		}
		return SUCCESS;
	}

	/**
	 * @return int[]
	 */
	public int[] getRetCodes()
	{
		int[] retCodes = { SUCCESS };
		return retCodes;
	}
}