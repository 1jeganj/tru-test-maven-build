package com.tru.commerce.payment.processor;

import java.math.BigDecimal;

import atg.nucleus.logging.ApplicationLoggingImpl;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.payment.TRUPaymentTools;
import com.tru.commerce.order.payment.applepay.IApplePayProcessor;
import com.tru.commerce.order.payment.applepay.TRUApplePayInfo;
import com.tru.commerce.payment.applepay.TRUApplePayStatus;
import com.tru.common.TRUErrorKeys;
import com.tru.radial.exception.TRURadialIntegrationException;
import com.tru.radial.integration.util.TRUUIDGenerator;
import com.tru.radial.payment.applepay.TRURadialApplePayProcessor;
import com.tru.radial.payment.applepay.bean.TRURadialApplePayRequest;
import com.tru.radial.payment.applepay.bean.TRURadialApplePayResponse;

/**
 * The Class TRUApplePayProcessor.
 */
public class TRUApplePayProcessor extends ApplicationLoggingImpl implements IApplePayProcessor {
	
	private TRUUIDGenerator mUidGenerator;
	
	private TRURadialApplePayProcessor mRadialApplePayService;
	
	/** property to hold PaymentTools .
	 */
	private TRUPaymentTools mPaymentTools;
	
	/**
	 * @return the paymentTools
	 */
	public TRUPaymentTools getPaymentTools() {
		return mPaymentTools;
	}

	/**
	 * @param pPaymentTools the paymentTools to set
	 */
	public void setPaymentTools(TRUPaymentTools pPaymentTools) {
		mPaymentTools = pPaymentTools;
	}


	/**
	 * This method will validate the Apple Pay Status payment group.
	 *
	 * @param pApplePayInfo the apple pay info
	 * @return TRUGiftCardStatus - gift card status.
	 */
	public TRUApplePayStatus authorize(TRUApplePayInfo pApplePayInfo) {
		if (isLoggingDebug()) {
			vlogDebug("TRUApplePayProcessor/authorize method start");
			vlogDebug("TRUApplePayInfo :{0}", pApplePayInfo);
		}
		final TRUApplePayStatus applePayStatus = new TRUApplePayStatus();
		final TRURadialApplePayRequest radialAppleRequest = new TRURadialApplePayRequest();
		try {
			//BeanUtils.copyProperties(pApplePayInfo, radialAppleRequest);

			radialAppleRequest.setApplePayTransactionId(pApplePayInfo.getApplePayTransactionId());
			radialAppleRequest.setEphemeralPublicKey(pApplePayInfo.getEphemeralPublicKey());
			radialAppleRequest.setPublicKeyHash(pApplePayInfo.getPublicKeyHash());
			radialAppleRequest.setData(pApplePayInfo.getData());
			radialAppleRequest.setSignature(pApplePayInfo.getSignature());
			radialAppleRequest.setVersion(pApplePayInfo.getVersion());

			
			String paymentId = Long.toString(getUidGenerator().getPaymentId());
			String orderId = pApplePayInfo.getOrderId();
			/*String requestId = paymentId.concat(orderId);*/
			radialAppleRequest.setRequestId(paymentId);
			
			radialAppleRequest.setOrderId(orderId);
			
			radialAppleRequest.setCustomerEmail(pApplePayInfo.getCustomerEmail());
			
			radialAppleRequest.setCustomerIPAddress(pApplePayInfo.getCustomerIPAddress());
			
			radialAppleRequest.setBillingFirstName(pApplePayInfo.getBillingFirstName());
			radialAppleRequest.setBillingLastName(pApplePayInfo.getBillingLastName());
			radialAppleRequest.setBillingAddressCity(pApplePayInfo.getBillingAddressCity());
			radialAppleRequest.setBillingAddressCountryCode(pApplePayInfo.getBillingAddressCountryCode());
			radialAppleRequest.setBillingAddressLine1(pApplePayInfo.getBillingAddressLine1());
			radialAppleRequest.setBillingAddressLine2(pApplePayInfo.getBillingAddressLine2());
			radialAppleRequest.setBillingAddressPostalCode(pApplePayInfo.getBillingAddressPostalCode());
			radialAppleRequest.setBillingAddressState(pApplePayInfo.getBillingAddressState());
			radialAppleRequest.setBillingPhoneNo(pApplePayInfo.getBillingPhoneNo());
			
			radialAppleRequest.setShippingFirstName(pApplePayInfo.getShippingFirstName());
			radialAppleRequest.setShippingLastName(pApplePayInfo.getShippingLastName());
			radialAppleRequest.setShippingCity(pApplePayInfo.getShippingCity());
			radialAppleRequest.setShippingCountryCode(pApplePayInfo.getShippingCountryCode());
			radialAppleRequest.setShippingAddressLine1(pApplePayInfo.getShippingAddressLine1());
			radialAppleRequest.setShippingAddressLine2(pApplePayInfo.getShippingAddressLine2());
			radialAppleRequest.setShippingPostalCode(pApplePayInfo.getShippingPostalCode());
			radialAppleRequest.setShippingState(pApplePayInfo.getShippingState());
			radialAppleRequest.setShippingPhoneNo(pApplePayInfo.getShippingPhoneNo());

			TRURadialApplePayResponse applepayAuthroization = (TRURadialApplePayResponse)mRadialApplePayService.applepayAuthroization(radialAppleRequest);
			// populate do AUTH checkout apple pay  response	
			if(applepayAuthroization != null && applepayAuthroization.getAmountAuthorized() != null){
				BigDecimal amountAuthorized = applepayAuthroization.getAmountAuthorized();
				applePayStatus.setAmount(amountAuthorized.doubleValue());
			}
			String responseCode = applepayAuthroization.getResponseCode();
			if(responseCode != null){
				responseCode = responseCode.toLowerCase();
				applePayStatus.setTransactionId(paymentId);
				applePayStatus.setApplePayTransactionId(applepayAuthroization.getPaymentAccountUniqueId());
				applePayStatus.setResponseCode(applepayAuthroization.getResponseCode());
				applePayStatus.setCvvCode(applepayAuthroization.getCVV2ResponseCode());
				applePayStatus.setAvsCode(applepayAuthroization.getAVSResponseCode());
				applePayStatus.setBankAuthCode(applepayAuthroization.getBankAuthorizationCode());
				applePayStatus.setTenderType((applepayAuthroization.getTenderType()));
			}
			if (responseCode != null) {
				if (getPaymentTools().getCreditCardRespSuccessCodes().contains(responseCode)) {
					applePayStatus.setTransactionSuccess(true);
					applePayStatus.setTransactionId(applepayAuthroization.getPaymentAccountUniqueId());
				} else if (getPaymentTools().getCreditCardRespManualRetrySuccessCodes().contains(responseCode)) {
					applePayStatus.setTransactionSuccess(false);
					applePayStatus.setErrorMessage(getPaymentTools().getFailureResponseCodes().get(responseCode));
				}else {
					applePayStatus.setTransactionSuccess(false);
					applePayStatus.setErrorMessage(TRUCheckoutConstants.TRU_ERROR_CHECKOUT_CREDIT_GENERAL_ERROR);
				} 
			}
			else {
				applePayStatus.setTransactionSuccess(false);
				applePayStatus.setErrorMessage(TRUCheckoutConstants.TRU_ERROR_CHECKOUT_CREDIT_GENERAL_ERROR);
			}
		} catch (TRURadialIntegrationException payIntExp) {
			vlogError("PaymentIntegrationException at TRUGiftCardProcessor/giftCardRedeem method :{0}",payIntExp);
			applePayStatus.setTransactionSuccess(false);
			applePayStatus.setErrorMessage(TRUErrorKeys.TRU_ERROR_TRY_AGAIN_AFTER_SOME_TIME);
		}
		return applePayStatus;
	}


	@Override
	public TRUApplePayStatus reverseAuthorization(
			TRUApplePayInfo pApplePayInfo, TRUApplePayStatus pApplePayStatus) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the mRadialApplePayService
	 */
	public TRURadialApplePayProcessor getRadialApplePayService() {
		return mRadialApplePayService;
	}

	/**
	 * Sets the radial apple pay service.
	 *
	 * @param pRadialApplePayService the new radial apple pay service
	 */
	public void setRadialApplePayService(TRURadialApplePayProcessor pRadialApplePayService) {
		this.mRadialApplePayService = pRadialApplePayService;
	}


	/**
	 * @return the mUidGenerator
	 */
	public TRUUIDGenerator getUidGenerator() {
		return mUidGenerator;
	}


	/**
	 * Sets the uid generator.
	 *
	 * @param pUidGenerator the new uid generator
	 */
	public void setUidGenerator(TRUUIDGenerator pUidGenerator) {
		this.mUidGenerator = pUidGenerator;
	}

}
