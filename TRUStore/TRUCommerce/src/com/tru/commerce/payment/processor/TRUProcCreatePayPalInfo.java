package com.tru.commerce.payment.processor;

import java.text.NumberFormat;

import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderImpl;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.payment.TRUPayPalPaymentInfo;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.payment.paypal.TRUPaypalUtilityManager;
import com.tru.radial.commerce.payment.paypal.TRUPaypalConstants;

/**
 * This class  will populate the data from TRUPayment group to paypalStatus.
 * the  TRUProcCreatePayPalInfo  
 * @author PA
 * @version 1.0
 */
public class TRUProcCreatePayPalInfo extends GenericService implements PipelineProcessor {
	
	/**
	 * property to hold OrderManager.
	 */
	private TRUOrderManager mOrderManager;
	/**
	 * property CLIENT_IP.
	 *//*	
	private static final String CLIENT_IP = "True-Client-IP";*/
	/**
	 * The possible return value for this processor is 1.
	 */
	private static final int SUCCESS = 1;

	
	/**
	 * The possible return value for this processor is 0.
	 */
	private static final int FAILURE = 0;
	/**
	 * property TRUPaypalUtilityManager class.
	 */	
	private TRUPaypalUtilityManager mPaypalUtilityManager;
	/**
	 * property paypal info class.
	 */	
	private String mPayPalInfoClass;
	/**
	 * property I pAddress Enable.
	 */	
	private boolean mIpAddressEnable;
	
	/**
	 * This method is used to get orderManager.
	 * @return orderManager String
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 *This method is used to set orderManager.
	 *@param pOrderManager - OrderManager String
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	/**
	 * @return the ipAddressEnable
	 */
	public boolean isIpAddressEnable() {
		return mIpAddressEnable;
	}

	/**
	 * @param pIpAddressEnable the ipAddressEnable to set
	 */
	public void setIpAddressEnable(boolean pIpAddressEnable) {
		mIpAddressEnable = pIpAddressEnable;
	}

	/**
	 * @return the payPalInfoClass
	 */
	public String getPayPalInfoClass() {
		return mPayPalInfoClass;
	}

	/**
	 * @param pPayPalInfoClass the payPalInfoClass to set
	 */
	public void setPayPalInfoClass(String pPayPalInfoClass) {
		mPayPalInfoClass = pPayPalInfoClass;
	}
	/**
	 * @return the retCodes
	 */
	@Override
	public int[] getRetCodes() {
		int[] lRetCodes = {SUCCESS};
		return lRetCodes;
	}
	
	/**
	 * @return the paypalUtilityManager
	 */
	public TRUPaypalUtilityManager getPaypalUtilityManager() {
		return mPaypalUtilityManager;
	}

	/**
	 * @param pPaypalUtilityManager the paypalUtilityManager to set
	 */
	public void setPaypalUtilityManager(
			TRUPaypalUtilityManager pPaypalUtilityManager) {
		mPaypalUtilityManager = pPaypalUtilityManager;
	}

	/**
	 * This method will populate the required data to PayPalInfo object from PayPal PaymentGroup.
	 * 
	 * @param pParamObject the paramObject to set
	 * @param pParamPipelineResult the paramPipelineResult
	 * @return success or failure
	 * @throws InstantiationException 	-InstantiationException
	 * @throws IllegalAccessException - IllegalAccessException
	 *  @throws InvalidParameterException - InvalidParameterException
	 * @throws ClassNotFoundException - ClassNotFoundException
	 */
	@Override
	public int runProcess(Object pParamObject, PipelineResult pParamPipelineResult)
			throws InstantiationException, IllegalAccessException, InvalidParameterException, ClassNotFoundException {
		if(isLoggingDebug()) {
			vlogDebug("Entering to the runprocess of the ProcCreatePayPalInfo ");
		}		
		if(pParamObject == null){
			return  FAILURE;
		}		
		PaymentManagerPipelineArgs lParams = (PaymentManagerPipelineArgs) pParamObject;
		OrderImpl order = (OrderImpl) lParams.getOrder();
		TRUPayPal lPayPal = (TRUPayPal) lParams.getPaymentGroup();
		double amount = lPayPal.getAmount();
		//create and populate PayPalinfo class.
		TRUPayPalPaymentInfo lPayPalInfo = getPayPalInfo();
		try {
			populatePayPalInfo(order, lPayPal, amount, lParams, lPayPalInfo);
		} catch (RepositoryException exception) {
			if(isLoggingError()) {
				logError("Repository Exception : @Class::TRUProcCreatePayPalInfo::@method::runProcess() While getting the ProfileItem or getting providerCode. " + " Order = " + order, exception);
			}
		}
		if(isLoggingDebug()) {
			vlogDebug("populating PayPalInfo Object into pipeline");			
		}
		lParams.setPaymentInfo(lPayPalInfo);
		/*List<TRUPayPalStatus> ListOfPaypalStatus = (List<TRUPayPalStatus>)lPayPal.getAuthorizationStatus();
		for(TRUPayPalStatus paypalStatus :  ListOfPaypalStatus){
			if(paypalStatus.getTransactionSuccess()){
				lParams.setPaymentStatus(paypalStatus);
				break;
			}
		}*/
		if(isLoggingDebug()) {
			vlogDebug("Exting to the runprocess of the ProcCreatePayPalInfo ");
		}		
		return SUCCESS;
	}

	/**
	 * This method will populate the PayPalInfo object with the data. 
	 * 
	 * @param pOrder the order
	 * @param pPayPal the paypal
	 * @param pAmount the amount
	 * @param pParams the params
	 * @param pPayPalInfo the paypal
	 * @throws RepositoryException the repositoryException
	 */
	public void populatePayPalInfo(Order pOrder, TRUPayPal pPayPal, double pAmount, 
			PaymentManagerPipelineArgs pParams, TRUPayPalPaymentInfo pPayPalInfo) throws RepositoryException {
		if(isLoggingDebug()) {
			vlogDebug("Entering to the populatePayPalInfo of the ProcCreatePayPalInfo ");
		}		
		if(pPayPalInfo == null || pOrder == null || pPayPal == null ){			
			if(isLoggingDebug()) {
				vlogDebug("order or paypalInfo or paypal payment group or ");
			}
			return ;
		}
		NumberFormat lNbrFrmt = NumberFormat.getNumberInstance();
		lNbrFrmt.setMaximumFractionDigits(TRUPaypalConstants.MAXIMUM_FRACTIONALDIGITS);
		pPayPalInfo.setOrder(pOrder);
		pPayPalInfo.setOrderId(pOrder.getId());
		pPayPalInfo.setPayerId(pPayPal.getPayerId());
		pPayPalInfo.setSiteId(pOrder.getSiteId());
		pPayPalInfo.setPayPalToken(pPayPal.getToken());		
		pPayPalInfo.setPaymentAction(TRUPaypalConstants.PAYPAL_PAYMENT_ACTION_ORDER);
		pPayPalInfo.setCurrencyCode(pPayPal.getCurrencyCode());	
		//pPayPalInfo.setTransactionId(pPayPal.getPayPalTransactionId());//instead of below	
		
		String ipAddress = null;
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		if(request != null && getOrderManager() != null) {
			ipAddress = getOrderManager().getTrueClientIpAddress(request);
			if(isLoggingDebug()){
				logDebug(" IP Address ::" +ipAddress);
			}
		}
		if(pOrder instanceof TRUOrderImpl) {
			pPayPalInfo.setEmail(((TRUOrderImpl)pOrder).getEmail());
		}
		
		pPayPalInfo.setBuyerIpAddress(ipAddress);
		//populating shipping info
		ContactInfo shippingAddress = null;	
		HardgoodShippingGroup shippingGroup =  getPaypalUtilityManager().getShippingGroupForPayPal(pOrder);
			if (shippingGroup != null && shippingGroup.getShippingAddress() != null) {
				shippingAddress = (ContactInfo) shippingGroup.getShippingAddress();
				if(shippingAddress != null && shippingAddress.getFirstName() != null){
					StringBuilder firstName=  new StringBuilder( shippingAddress.getFirstName());
					if(!StringUtils.isBlank(shippingAddress.getLastName())){
						firstName.append(TRUCheckoutConstants.SPACE);
						firstName.append(shippingAddress.getLastName());
					}					
					pPayPalInfo.setShipToFirstName(firstName.toString());				
					pPayPalInfo.setShipToStreet1(shippingAddress.getAddress1());
					pPayPalInfo.setShipToStreet2(shippingAddress.getAddress2());
					pPayPalInfo.setShipToCity(shippingAddress.getCity());
					pPayPalInfo.setShipToState(shippingAddress.getState());					
					pPayPalInfo.setShipToZip(shippingAddress.getPostalCode());
					pPayPalInfo.setShipToPhoneNumber(shippingAddress.getPhoneNumber());
					//TODO Need to get country code Dynamically 
					if(shippingAddress.getCountry().equals(TRUPaypalConstants.UNITED_STATES)){
						pPayPalInfo.setCountryCode(TRUPaypalConstants.UNITED_STATES);
					} else{
						pPayPalInfo.setCountryCode(TRUPaypalConstants.UNITED_STATES);
					}					
					if(StringUtils.isNotBlank(((TRUHardgoodShippingGroup)shippingGroup).getShipToName())){
						pPayPalInfo.setShipToName(((TRUHardgoodShippingGroup)shippingGroup).getShipToName());
					}else{
						pPayPalInfo.setShipToName(((TRUHardgoodShippingGroup)shippingGroup).getNickName());
					}
					pPayPalInfo.setShipToPhoneNumber(shippingAddress.getPhoneNumber());
				}				
			}
		//setting order amount
		if(pOrder!= null && pOrder.getPriceInfo()!= null){
			Double shippingAmt = pOrder.getPriceInfo().getShipping();
			Double amountString = pAmount;
			pPayPalInfo.setAmount(amountString.toString());
			pPayPalInfo.setShippingAmount(shippingAmt.toString());
		}
		//setting billing address
		pPayPalInfo.setBillingAddress(pPayPal.getBillingAddress());
		if(isLoggingDebug()) {
			vlogDebug("Exting from the populatePayPalInfo of the ProcCreatePayPalInfo ");
		}	
	}

	/**
	 * This method will create the PayPalInfo object.
	 *  
	 * @return TRUPayPalPaymentInfo the paypal PaymentInfo
	 * @throws InstantiationException  while processing  paypal PaymentInfo
	 * @throws IllegalAccessException while processing  paypal PaymentInfo
	 * @throws ClassNotFoundException while processing  paypal PaymentInfo
	 */
	public TRUPayPalPaymentInfo getPayPalInfo() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		if(isLoggingDebug()) {
			vlogDebug("Making new instance of type : " + getPayPalInfoClass());
		}
		TRUPayPalPaymentInfo payPalInfo = (TRUPayPalPaymentInfo) Class.forName(getPayPalInfoClass()).newInstance();
		return payPalInfo;
	}

}
