package com.tru.commerce.payment.processor;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.payment.PaymentStatus;

import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.payment.TRUPaymentTools;
import com.tru.commerce.order.payment.applepay.IApplePayProcessor;
import com.tru.commerce.order.payment.applepay.TRUApplePayInfo;
import com.tru.commerce.payment.AbstractProcTRUProcessPaymentGroup;
import com.tru.commerce.payment.TRUPaymentManager;
import com.tru.commerce.payment.applepay.TRUApplePay;
import com.tru.commerce.payment.applepay.TRUApplePayStatus;
/**
 * This class is used to authorize the gift card payment group and reverse authorization of gift card payment group.
 * @author PA
 * @version 1.0
 */
public class ProcTRUProcessApplePay extends AbstractProcTRUProcessPaymentGroup {
	
	/**
	 * variable to hold mPaymentManager reference.
	 */
	private TRUPaymentManager mPaymentManager;
	
	/**
	 * This method authorizes the payment group.
	 * 
	 * @param pParams -- PaymentManagerPipelineArgs
	 * @return giftCardStatus -- gift card status
	 */
	public PaymentStatus authorizePaymentGroup(PaymentManagerPipelineArgs  pParams){
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUProcessApplePay .(authorizePaymentGroup) Method : BEGIN ");
		}
		TRUApplePayStatus applePayStatus = null;
		Order order = pParams.getOrder();

		try {
			Object paymentInfo = pParams.getPaymentInfo();
			if (paymentInfo == null) {
				if (isLoggingDebug()) {
					vlogDebug(" TRUApplePayInfo is null :{0}",paymentInfo);
				}
				return null;
			}
			TRUApplePayInfo applePayInfo = (TRUApplePayInfo)pParams.getPaymentInfo();
			
			IApplePayProcessor applePayProcessor = getPaymentManager().getApplePayProcessor();
			if (applePayProcessor == null) {
				if (isLoggingDebug()) {
					vlogDebug(" applePayProcessor is null :{0}",applePayProcessor);
				}
				return null;
			}
			TRUPaymentTools paymentTools = getPaymentManager().getPaymentTools();
			
			if (paymentTools == null) {
				if (isLoggingDebug()) {
					vlogDebug(" paymentTools is null :{0}",paymentTools);
				}
				return null;
			}	
			applePayStatus = getPaymentManager().getApplePayProcessor().authorize(applePayInfo);
			//long paymentId = getPaymentManager().getPaymentTools().getUidGenerator().getPaymentId();
			
			if(applePayStatus.getTransactionSuccess()){
				((TRUApplePay)pParams.getPaymentGroup()).setApplePayTransactionId(applePayStatus.getApplePayTransactionId());
				((TRUApplePay)pParams.getPaymentGroup()).setTransactionId(applePayStatus.getTransactionId());
				((TRUApplePay)pParams.getPaymentGroup()).setResponseCode(applePayStatus.getResponseCode());
			}
			if (isLoggingDebug()) {
				vlogDebug(" TRUApplePay :{0},TRUApplePay :{1}",applePayInfo,applePayStatus);
			}
		} catch (ClassCastException cce) {
			String profileId = null;
			if (order instanceof TRUOrderImpl) {
				profileId = ((TRUOrderImpl) pParams.getOrder()).getProfileId();
			} else if (order instanceof TRULayawayOrderImpl) {
				profileId = ((TRULayawayOrderImpl) pParams.getOrder()).getProfileId();
			}
			if (isLoggingError()) {
				vlogError("TRUApplePay.authorizePaymentGroup() ,orderId:{0} profileId:{1} className:{2} ClassCastException:{3}",
						order.getId(), profileId, pParams.getPaymentInfo().getClass().getName());
			}
			throw cce;
		}
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUProcessApplePay .(authorizePaymentGroup) Method : END ");
		}
		return applePayStatus;
	}
    
	/**
	 * This method is used to credits the amount to the payment group.
	 * 
	 * @param pParamPaymentManagerPipelineArgs -- PaymentManagerPipelineArgs
	 * @throws CommerceException -- CommerceException
	 * @throws UnsupportedOperationException -- UnsupportedOperationException
	 * @return PaymentStatus - PaymentStatus
	 */
	public PaymentStatus creditPaymentGroup(
			PaymentManagerPipelineArgs pParamPaymentManagerPipelineArgs)
			throws CommerceException,UnsupportedOperationException {
		 throw new UnsupportedOperationException();
	}

	/**
	 * This method debits amount from the payment group.
	 * 
	 * @param pParamPaymentManagerPipelineArgs -- PaymentManagerPipelineArgs
	 * @return giftCardStatus -- gift card status
	 * @throws CommerceException -- CommerceException
	 */
	public PaymentStatus debitPaymentGroup(
			PaymentManagerPipelineArgs pParamPaymentManagerPipelineArgs)
			throws CommerceException {
		 throw new UnsupportedOperationException();
	}
	
	/**
	 * @param pPaymentManager the PaymentManager to set
	 */
	public void setPaymentManager(TRUPaymentManager pPaymentManager) {
		this.mPaymentManager = pPaymentManager;
	}

	/**
	 * @return the PaymentManager
	 */
	public TRUPaymentManager getPaymentManager() {
		return mPaymentManager;
	}

	@Override
	public PaymentStatus reverseAuthorizePaymentGroup(
			PaymentManagerPipelineArgs pParams) throws CommerceException {
		// TODO Auto-generated method stub
		return null;
	}

}