package com.tru.commerce.payment.processor;
import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.payment.creditcard.CreditCardInfo;
import atg.payment.creditcard.CreditCardProcessor;
import atg.payment.creditcard.CreditCardStatus;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.payment.TRUPaymentTools;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardInfo;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardStatus;
import com.tru.common.TRUConstants;
import com.tru.common.TRUIntegrationConfiguration;
import com.tru.common.TRUSOSIntegrationConfig;
import com.tru.radial.integration.exception.TRUPaymentIntegrationException;
import com.tru.radial.integration.util.TRURadialConstants;

/**
 * This class implements OOTB CreditCardProcessor and it invokes the Sale transaction for the credit card.
 * @author PA
 * @version 1.0
 */
public class TRUCreditCardProcessor extends ApplicationLoggingImpl implements CreditCardProcessor  {

	/** property to hold PaymentTools .
	 */
	private TRUPaymentTools mPaymentTools;
	
	/** property to hold OrderTools.
	 */
	private TRUOrderTools mOrderTools;
	/** property to hold Negative testing.
	 */
	private boolean mEnableNegativeTesting;
	/** property to hold negative testing for authorization.
	 */
	private boolean mEnableFailAuthorization;
	/** property to hold Negative testing for fraud.
	 * 
	 */
	private boolean mEnableFailFraud;
	
	/**
	 * hold boolean property integrationConfig.
	 */
	private TRUIntegrationConfiguration mIntegrationConfig;
	
	/** The m site context mSiteContextManager. */
	private SiteContextManager mSiteContextManager;

	/**
	 * @return the siteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * @param pSiteContextManager the siteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}

	/** The mSosIntegrationConfiguration. */
	private TRUSOSIntegrationConfig mSosIntegrationConfiguration;

	/**
	 * @return the sosIntegrationConfiguration
	 */
	public TRUSOSIntegrationConfig getSosIntegrationConfiguration() {
		return mSosIntegrationConfiguration;
	}

	/**
	 * @param pSosIntegrationConfiguration the sosIntegrationConfiguration to set
	 */
	public void setSosIntegrationConfiguration(
			TRUSOSIntegrationConfig pSosIntegrationConfiguration) {
		mSosIntegrationConfiguration = pSosIntegrationConfiguration;
	}

	/**
	 * @return the integrationConfig
	 */
	public TRUIntegrationConfiguration getIntegrationConfig() {
		return mIntegrationConfig;
	}

	/**
	 * @param pIntegrationConfig the integrationConfig to set
	 */
	public void setIntegrationConfig(TRUIntegrationConfiguration pIntegrationConfig) {
		mIntegrationConfig = pIntegrationConfig;
	}
	/**
	 * @return the enableNegativeTesting
	 */
	public boolean isEnableNegativeTesting() {
		return mEnableNegativeTesting;
	}


	/**
	 * @param pEnableNegativeTesting the enableNegativeTesting to set
	 */
	public void setEnableNegativeTesting(boolean pEnableNegativeTesting) {
		mEnableNegativeTesting = pEnableNegativeTesting;
	}


	/**
	 * @return the enableFailAuthorization
	 */
	public boolean isEnableFailAuthorization() {
		return mEnableFailAuthorization;
	}


	/**
	 * @param pEnableFailAuthorization the enableFailAuthorization to set
	 */
	public void setEnableFailAuthorization(boolean pEnableFailAuthorization) {
		mEnableFailAuthorization = pEnableFailAuthorization;
	}


	/**
	 * @return the enableFailFraud
	 */
	public boolean isEnableFailFraud() {
		return mEnableFailFraud;
	}


	/**
	 * @param pEnableFailFraud the enableFailFraud to set
	 */
	public void setEnableFailFraud(boolean pEnableFailFraud) {
		mEnableFailFraud = pEnableFailFraud;
	}


	/**
	 * This method will validate the credit card payment group.
	 * @param pCreditCardInfo the credit Card Info to validate.
	 * 
	 * @return credit card status.
	 */
	public TRUCreditCardStatus authorize(CreditCardInfo pCreditCardInfo) {
		if (isLoggingDebug()) {
			vlogDebug("TRUCreditCardProcessor/authorize method start");
			vlogDebug("CreditCardInfo :{0}", pCreditCardInfo);
		}
	
		//TRUCreditCardInfo creditCardInfo = (TRUCreditCardInfo) pCreditCardInfo;
		
		TRUCreditCardStatus creditCardStatus = new TRUCreditCardStatus();
		
		if (isLoggingDebug()) {
			vlogDebug("In Authorization method of credit card processor with credit card status" + creditCardStatus.getTransactionSuccess());
			vlogDebug("TRUCreditCardProcessor/authorize method End");
		}
		
		return creditCardStatus;
	}
	
	
	/**
	 * This method will validate the credit card payment group.
	 * 
	 * @param pCreditCardInfo the credit Card Info to validate. 
	 * @return credit card status.
	 * 
	 */
	public TRUCreditCardStatus fullAuthorization(CreditCardInfo pCreditCardInfo) {
		vlogDebug("TRUCreditCardProcessor/fullAuthorization method start");
		vlogDebug("CreditCardInfo :{0}", pCreditCardInfo);
		TRUCreditCardStatus creditCardStatus = new TRUCreditCardStatus();
		TRUCreditCardInfo creditCardInfo = (TRUCreditCardInfo) pCreditCardInfo;
		int paymentProcessortimeoutRetry = TRUConstants._1;
		int retryAttemptsCount = creditCardInfo.getRetryCount();
		int creditCardRetryCount = 0;
		RepositoryItem siteItem = null;
		boolean lEnabledRadialPayment = enableRadialCreditCard();
		if(lEnabledRadialPayment){
			try {
				siteItem = getPaymentTools().getSiteContextManager().getSite(pCreditCardInfo.getOrder().getSiteId());
				creditCardRetryCount = (int)siteItem.getPropertyValue(getPaymentTools().
						getOrderManager().getPropertyManager().getPaymentRetryCount());
			} catch (SiteContextException siteContextException) {
				vlogError("RepositoryException at TRUCreditCardProcessor/fullAuthorization method :{0}",siteContextException);
			}
			getPaymentTools().fullAuthorization(creditCardInfo, creditCardStatus);
			String responseCode = creditCardStatus.getResponseCode();
			if(responseCode != null){
				responseCode = responseCode.toLowerCase();
			}
			if(responseCode != null && responseCode.equalsIgnoreCase(getPaymentTools().getPaymentProcessorTimeout())) {
				vlogDebug("PaymentProcessor Timeout Retrycount :{0}===========", paymentProcessortimeoutRetry);
				while(paymentProcessortimeoutRetry < creditCardRetryCount) {
					++ paymentProcessortimeoutRetry;
					vlogDebug("PaymentProcessor Timeout Retrycount :{0}===========", paymentProcessortimeoutRetry);
					getPaymentTools().fullAuthorization(creditCardInfo,creditCardStatus);
					responseCode = creditCardStatus.getResponseCode();
					if (responseCode != null) {
						responseCode = responseCode.toLowerCase();
						if (!responseCode.equalsIgnoreCase(getPaymentTools().getPaymentProcessorTimeout())) {
							break;
						}
					}
				}
			}
			if (responseCode != null) {
				if(getPaymentTools().getCreditCardRespSuccessCodes().contains(responseCode)) {
					creditCardStatus.setTransactionSuccess(true);
					creditCardStatus.setTransactionId(creditCardInfo.getTransactionId());
				} else if (StringUtils.isNotBlank(creditCardInfo.getCvv()) && 
						getPaymentTools().getSynchronyAcceptCVV().equalsIgnoreCase(creditCardInfo.getCvv()) && 
						StringUtils.isNotBlank(pCreditCardInfo.getCreditCardType()) && 
						getPaymentTools().getSynchronyCardTypes().contains(pCreditCardInfo.getCreditCardType()) && 
						getPaymentTools().getCreditCardRespSynchronyAcceptCodes().contains(responseCode)) {
					creditCardStatus.setTransactionSuccess(true);
					creditCardStatus.setTransactionId(creditCardInfo.getTransactionId());
				} else if(responseCode.equalsIgnoreCase(getPaymentTools().getPaymentProcessorTimeout())) {
					boolean notTokenized= StringUtils.isNumericOnly(creditCardInfo.getCreditCardNumber());
					if(!notTokenized){
					vlogDebug("accepting order  with after PaymentProcessor Timeout Retrycount  :{0} ======",paymentProcessortimeoutRetry);
					creditCardStatus.setTransactionSuccess(true);
					creditCardStatus.setTransactionId(creditCardInfo.getTransactionId());
					}else{
						vlogDebug("Rejecting order as card is not Tokenized after PaymentProcessor Timeout Retrycount   :{0} ======",paymentProcessortimeoutRetry);
						creditCardStatus.setTransactionSuccess(false);
						creditCardStatus.setErrorMessage(TRUCheckoutConstants.TRU_ERROR_CHECKOUT_CREDIT_GENERAL_ERROR);
					}
				} else if(getPaymentTools().getCreditCardRespManualRetrySuccessCodes().contains(responseCode)){
					if(retryAttemptsCount >= creditCardRetryCount){
						creditCardStatus.setTransactionSuccess(true);
						creditCardStatus.setRetryAcceptOrder(true);
					}else{
						creditCardStatus.setTransactionSuccess(false);
						creditCardStatus.setErrorMessage(getPaymentTools().getFailureResponseCodes().get(responseCode));
					}
				}else{
					creditCardStatus.setTransactionSuccess(false);
					creditCardStatus.setErrorMessage(TRUCheckoutConstants.TRU_ERROR_CHECKOUT_CREDIT_GENERAL_ERROR);
				}
			}else{
				creditCardStatus.setTransactionSuccess(false);
				creditCardStatus.setErrorMessage(TRUCheckoutConstants.TRU_ERROR_CHECKOUT_CREDIT_GENERAL_ERROR);
			}

			vlogDebug("In Authorization method of credit card processor with credit card status :{0} ErrorMessage :{1} retryCount :{2}",creditCardStatus.getTransactionSuccess(),creditCardStatus.getErrorMessage(),retryAttemptsCount);
		} else {
			creditCardStatus.setTransactionSuccess(false);
			creditCardStatus.setErrorMessage(TRUCheckoutConstants.TRU_ERROR_RADIAL_CREDIT_CARD_DISABLED);
			vlogDebug("Radial CreditCard Service is Disabled Now");
		}
		vlogDebug("TRUCreditCardProcessor/fullAuthorization method End");
		return creditCardStatus;
	}
	
	/**
	 * This method will validate the credit card payment group.
	 * 
	 * @param pCreditCardInfo the credit Card Info to validate. 
	 * @return credit card status.
	 * 
	 */
	public TRUCreditCardStatus zeroAuthorization(CreditCardInfo pCreditCardInfo) {
		vlogDebug("TRUCreditCardProcessor/zeroAuthorization method start");
		vlogDebug("CreditCardInfo :{0}", pCreditCardInfo);
		TRUCreditCardStatus creditCardStatus = new TRUCreditCardStatus();
		if(!enableRadialCreditCard()) {
			creditCardStatus.setTransactionSuccess(true);
			vlogDebug("Radial Service is Disabled Now");
			return creditCardStatus;
		}

		TRUCreditCardInfo creditCardInfo = (TRUCreditCardInfo) pCreditCardInfo;
		int retryCount = TRUConstants.ZERO;
		int retryLimit = TRUConstants.ZERO;
		RepositoryItem siteItem = null;
		String responseCode = TRUConstants.EMPTY_STRING;

		try {
			siteItem = getPaymentTools().getSiteContextManager().getSite(creditCardInfo.getSiteId());
			retryLimit = (int)siteItem.getPropertyValue(getPaymentTools().getOrderManager().getPropertyManager().
					getPaymentRetryCount());
		} catch (SiteContextException siteContextException) {
			vlogError("RepositoryException at TRUCreditCardProcessor/zeroAuthorization method :{0}", siteContextException);
			creditCardStatus.setTransactionSuccess(false);
			creditCardStatus.setErrorMessage(TRUCheckoutConstants.TRU_ERROR_CHECKOUT_CREDIT_GENERAL_ERROR);
		}

		while(retryCount < retryLimit){
			tryZeroAuthorization(creditCardInfo, creditCardStatus);
			responseCode = creditCardStatus.getResponseCode();
			++ retryCount;
			if (responseCode != null){
				responseCode = responseCode.toLowerCase();
				if (!(getPaymentTools().getValidateCreditCardRetryCodes().contains(responseCode))){
					break;
				}
			}
		}

		if (responseCode != null) {
			if (getPaymentTools().getValidateCreditCardSuccessCodes().contains(responseCode)) {
				creditCardStatus.setTransactionSuccess(true);
			} else if (getPaymentTools().getValidateCreditCardRetryCodes().contains(responseCode)){
				creditCardStatus.setTransactionSuccess(false);
				creditCardStatus.setErrorMessage(getPaymentTools().getValidateCreditCardRetryErrorKey());
			} else {
				creditCardStatus.setTransactionSuccess(false);
				creditCardStatus.setErrorMessage(getPaymentTools().getValidateCreditCardErrorCodes().get(responseCode));
			}
		}else{
			creditCardStatus.setTransactionSuccess(false);
			creditCardStatus.setErrorMessage(TRUCheckoutConstants.TRU_ERROR_CHECKOUT_CREDIT_GENERAL_ERROR);
		}

		vlogDebug("In Zero Authorization method of credit card processor with credit card status :{0} ErrorMessage :{1}", 
				creditCardStatus.getTransactionSuccess(), creditCardStatus.getErrorMessage());
		vlogDebug("TRUCreditCardProcessor/zeroAuthorization method End");
		return creditCardStatus;
	}
	
	/**
	 * This method will validate the credit card payment group.
	 * 
	 * @param pCreditCardInfo the credit Card Info to validate.
	 * @param pCreditCardStatus the credit card status.
	 * 
	 */
	public void tryZeroAuthorization(TRUCreditCardInfo pCreditCardInfo, TRUCreditCardStatus pCreditCardStatus) {
		try {
			getPaymentTools().zeroAuthorization(pCreditCardInfo, pCreditCardStatus);
		} catch (TRUPaymentIntegrationException paymentIntegrationExp) {
			vlogError("PaymentIntegrationException at TRUCreditCardProcessor/zeroAuthorization method :{0}", paymentIntegrationExp);
			pCreditCardStatus.setTransactionSuccess(false);
			pCreditCardStatus.setErrorMessage(TRUCheckoutConstants.TRU_ERROR_CHECKOUT_CREDIT_GENERAL_ERROR);
		}
	}
	
	/**
	 * This method will call the BAMS pre-Auth API.
	 * @param pCreditCardInfo - CreditCardInfo
	 * @return TRUCreditCardStatus - TRUCreditCardStatus
	 */
	public TRUCreditCardStatus preAuthorization(CreditCardInfo pCreditCardInfo) {
		if (isLoggingDebug()) {
			vlogDebug("TRUCreditCardProcessor/preAuthorization method start");
			vlogDebug("CreditCardInfo :{0}", pCreditCardInfo);
		}
		TRUCreditCardStatus creditCardStatus = new TRUCreditCardStatus();
		TRUCreditCardInfo creditCardInfo = (TRUCreditCardInfo) pCreditCardInfo;
		if(!enableRadialCreditCard()) {
			creditCardStatus.setErrorMessage(TRUCheckoutConstants.TRU_ERROR_CHECKOUT_CREDIT_GENERAL_ERROR);
			creditCardStatus.setTransactionSuccess(false);
			vlogDebug("Radial Service is Disabled Now");
			return creditCardStatus;
		}
		try {
			getPaymentTools().promoFinancingAuthorization(creditCardInfo,creditCardStatus);
			if (creditCardStatus.getResponseCode() != null && creditCardStatus.getResponseCode().equalsIgnoreCase(
					TRURadialConstants.PROMO_CODE_SUCCESS) || creditCardStatus.getResponseCode().equalsIgnoreCase(
							TRURadialConstants.PROMO_CODE_FAIL) || creditCardStatus.getResponseCode().equalsIgnoreCase(
									TRURadialConstants.PROMO_CODE_TIMEOUT)) {
				creditCardStatus.setTransactionSuccess(true);
			}
		}
		catch(TRUPaymentIntegrationException exe){
			vlogError("PaymentIntegrationException at TRUCreditCardProcessor/preAuthorization method :{0}",exe);
			creditCardStatus.setErrorMessage(TRUCheckoutConstants.TRU_ERROR_CHECKOUT_CREDIT_GENERAL_ERROR);
			creditCardStatus.setTransactionSuccess(false);
		} 
		if (isLoggingDebug()) {
			vlogDebug("In pre Authorization method of credit card processor with credit card status :{0} ErrorMessage :{1}",
					creditCardStatus.getResponseCode(), creditCardStatus.getErrorMessage());
			vlogDebug("TRUCreditCardProcessor/preAuthorization method End");
		}
		return creditCardStatus;
	}

	
	/**
     * Debit the amount on the credit card after authorization.
     * This method is unsupported as authorize method takes care of making payment.
     *
     * @param pCreditCardInfo the CreditCardInfo reference which contains all the debit data
     * @param pStatus the CreditCardStatus object which contains information about the transaction. This
     *                should be the object which was returned from authorize().
     * @return a CreditCardStatus object detailing the results of the debit
     */
    public CreditCardStatus debit(CreditCardInfo pCreditCardInfo, CreditCardStatus pStatus) {
        throw new UnsupportedOperationException();
    }

    /**
     * Credit the amount on the credit card after debiting.
     * This method is unsupported as authorize method takes care of making payment.
     *
     * @param pCreditCardInfo the CreditCardInfo reference which contains all the credit data
     * @param pStatus the CreditCardStatus object which contains information about the transaction. This
     *                should be the object which was returned from debit().
     * @return a CreditCardStatus object detailing the results of the credit
     */
    public CreditCardStatus credit(CreditCardInfo pCreditCardInfo, CreditCardStatus pStatus) {
        throw new UnsupportedOperationException();
    }

    /**
     * Credit the amount on the credit card with as a new order.
     * This method is unsupported as authorize method takes care of making payment.
     *
     * @param pCreditCardInfo the CreditCardInfo reference which contains all the credit data
     * @return a CreditCardStatus object detailing the results of the credit
     */
    public CreditCardStatus credit(CreditCardInfo pCreditCardInfo) {
        throw new UnsupportedOperationException();
    }
	
	/**
	 * @return the paymentTools
	 */
	public TRUPaymentTools getPaymentTools() {
		return mPaymentTools;
	}


	/**
	 * @param pPaymentTools the paymentTools to set
	 */
	public void setPaymentTools(TRUPaymentTools pPaymentTools) {
		mPaymentTools = pPaymentTools;
	}


	/**
	 * @return the orderTools
	 */
	public TRUOrderTools getOrderTools() {
		return mOrderTools;
	}


	/**
	 * @param pOrderTools the orderTools to set
	 */
	public void setOrderTools(TRUOrderTools pOrderTools) {
		mOrderTools = pOrderTools;
	}

	/**
	 * Enable RadialCreditCard
	 *
	 * @return true, if successful
	 */
	private boolean enableRadialCreditCard() {
		if (isLoggingDebug()) {
			vlogDebug("@class:TRUCreditCardProcessor/enableRadialCreditCard method start");
		}
		boolean lEnabled = Boolean.FALSE;
		Site site=null;
		site=SiteContextManager.getCurrentSite();
		if(site != null && site.getId().equalsIgnoreCase(TRUCommerceConstants.SOS)) {
			lEnabled = getSosIntegrationConfiguration().isEnableRadialCreditCard();
			if (isLoggingDebug()) {
				vlogDebug("@class:TRUCreditCardProcessor/enableRadialCreditCard is  radial payment  service enabled in SOS : {0} ",lEnabled);
			}
		} else{
			lEnabled = getIntegrationConfig().isEnableRadialCreditCard();
			if (isLoggingDebug()) {
				vlogDebug("@class:TRUCreditCardProcessor/enableRadialCreditCard is  radial payment  service enabled in store : {0} ",lEnabled);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@class:TRUCreditCardProcessor/enableRadialCreditCard method end");
		}
		return lEnabled;
	}
}
