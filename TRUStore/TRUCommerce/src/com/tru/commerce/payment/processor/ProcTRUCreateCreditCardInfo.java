package com.tru.commerce.payment.processor;

import atg.commerce.order.Order;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.nucleus.GenericService;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardInfo;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This method implements OOTB PipelineProcessor.
 * @author ORACLE
 * @version 1.0
 */
public class ProcTRUCreateCreditCardInfo extends GenericService	implements PipelineProcessor{
	
	/**
	 * Property holds the constant for success.
	 */
	private static final int SUCCESS = 1;
	/**
	 * Property holds the Credit card info.
	 */
	private String mCreditCardInfoClass;
	
	/**
	 * Property to hold the TRUConfiguration.
	 */
	private TRUConfiguration mTruConfiguration;
	
	/**
	 * Property to hold the TRUPropertyManager.
	 */
	private TRUPropertyManager mPropertyManager;

	/**
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * @return the tRUConfiguration
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * @param pTRUConfiguration the tRUConfiguration to set
	 */
	public void setTruConfiguration(TRUConfiguration pTRUConfiguration) {
		mTruConfiguration = pTRUConfiguration;
	}
	/**
	 * @return String
	 */ 
	public String getCreditCardInfoClass()
	{
		return this.mCreditCardInfoClass;
	}
	
	/**
	 * @param pCreditCardInfoClass - String
	 */
	public void setCreditCardInfoClass(String pCreditCardInfoClass)
	{
		this.mCreditCardInfoClass = pCreditCardInfoClass;
	}
	/**
	 *  ProcTRUCreateCreditCardInfo.
	 */
	public ProcTRUCreateCreditCardInfo()
	{
		this.mCreditCardInfoClass = TRUPipeLineConstants.CREDITCARD_INFO_CLASS;
	}

	/**
	 * @param pOrder - Order
	 * @param pPaymentGroup - TRUCreditCardPG
	 * @param pAmount - double
	 * @param pParams - PaymentManagerPipelineArgs
	 * @param pCreditCardInfo - TRUCreditCardInfo
	 */
	protected void addDataToCreditCardInfo(Order pOrder, TRUCreditCard pPaymentGroup, double pAmount, PaymentManagerPipelineArgs pParams, 
			TRUCreditCardInfo pCreditCardInfo)
	{
		
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUCreateCreditCardInfo.addDataToCreditCardInfo :STARTS");
		}
		
		//Below line is need to correct. Expected format is "1012" for $10.12
		
		pCreditCardInfo.setAmount(pPaymentGroup.getAmount());
		
		pCreditCardInfo.setCreditCardNumber(pPaymentGroup.getCreditCardNumber());
		
		pCreditCardInfo.setCreditCardType(getTruConfiguration().getCreditCardTypeNamesForIntegration().
				get(pPaymentGroup.getCreditCardType()));
		
		pCreditCardInfo.setCvv(pPaymentGroup.getCardVerificationNumber());
		
		pCreditCardInfo.setCurrencyCode(pPaymentGroup.getCurrencyCode());
		
		pCreditCardInfo.setCardHolderName(pPaymentGroup.getNameOnCard());
		
		if(pOrder instanceof TRUOrderImpl) {
			pCreditCardInfo.setEmail(((TRUOrderImpl)pOrder).getEmail());
		} else if(pOrder instanceof TRULayawayOrderImpl) {
			pCreditCardInfo.setEmail(((TRULayawayOrderImpl)pOrder).getEmail());
		}
		
		pCreditCardInfo.setExpirationMonth(pPaymentGroup.getExpirationMonth());
		
		pCreditCardInfo.setExpirationYear(pPaymentGroup.getExpirationYear());

		StringBuilder expirationDate = new StringBuilder().append(pPaymentGroup.getExpirationMonth()).append(pPaymentGroup.getExpirationYear()); 
		
		pCreditCardInfo.setExpirationDate(expirationDate.toString());
		
		pCreditCardInfo.setBillingAddress(pPaymentGroup.getBillingAddress());
		
		String siteId = pOrder.getSiteId();
		pCreditCardInfo.setSDDbaName(getTruConfiguration().getSDDBAName().get(siteId)+pOrder.getId());
		
		pCreditCardInfo.setSDStreet(getTruConfiguration().getSDStreet().get(siteId));
		
		pCreditCardInfo.setSDCity(getTruConfiguration().getSDCity().get(siteId));
		
		pCreditCardInfo.setSDRegion(getTruConfiguration().getSDRegion().get(siteId));
		
		pCreditCardInfo.setSDMid(getTruConfiguration().getSDMid().get(siteId));

		pCreditCardInfo.setSDMcc(getTruConfiguration().getSDMcc().get(siteId));

		pCreditCardInfo.setSDPostalCode(getTruConfiguration().getSDPostalCode().get(siteId));

		pCreditCardInfo.setSDCountryCode(getTruConfiguration().getSDCountryCode().get(siteId));
		
		pCreditCardInfo.setSDMerchantContactInfo(getTruConfiguration().getSDMerchangeContactInfo().get(siteId));

		if(pOrder instanceof TRUOrderImpl){
			pCreditCardInfo.setTaxAmount(pOrder.getTaxPriceInfo().getAmount());
		} else if(pOrder instanceof TRULayawayOrderImpl) {
			pCreditCardInfo.setTaxAmount(TRUConstants.DOUBLE_ZERO);
		}
		
		pCreditCardInfo.setTaxNumber(getTruConfiguration().getTaxNumber().get(siteId));
		
		pCreditCardInfo.setTokenType(getTruConfiguration().getTokenType().get(siteId));
		
		pCreditCardInfo.setCavv(pPaymentGroup.getCavv());
		pCreditCardInfo.setEciFlag(pPaymentGroup.getEciFlag());
		pCreditCardInfo.setPaResStatus(pPaymentGroup.getPaResStatus());
		pCreditCardInfo.setSignatureVerification(pPaymentGroup.getSignatureVerification());
		pCreditCardInfo.setXid(pPaymentGroup.getXid());
		pCreditCardInfo.setEnrolled(pPaymentGroup.getEnrolled());
		pCreditCardInfo.setActionCode(pPaymentGroup.getActionCode());
		pCreditCardInfo.setMerchantRefNumber(pOrder.getId());
		pCreditCardInfo.setOrder(pOrder);
		
		if(pOrder instanceof TRUOrderImpl){
			pCreditCardInfo.setPreviousResponseCode(((TRUOrderImpl)pOrder).getPreviousResponseCode());
			pCreditCardInfo.setRetryCount((int) ((TRUOrderImpl)pOrder).getRetryCount());
		}
		if(pOrder instanceof TRULayawayOrderImpl){
			pCreditCardInfo.setPreviousResponseCode(((TRULayawayOrderImpl)pOrder).getPreviousResponseCode());
			pCreditCardInfo.setRetryCount((int) ((TRULayawayOrderImpl)pOrder).getRetryCount());
		}
		
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUCreateCreditCardInfo.addDataToCreditCardInfo : ENDS");
		}
	}

	/**
	 * @return TRUCreditCardInfo
	 * @throws InstantiationException - InstantiationException
	 * @throws IllegalAccessException - IllegalAccessException
	 * @throws ClassNotFoundException - IllegalAccessException
	 */
	protected TRUCreditCardInfo getCreditCardInfo() 
	throws InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		
		if (isLoggingDebug()) {
			vlogDebug("Making a new instance of type: " + getCreditCardInfoClass());
		}
		TRUCreditCardInfo cci = (TRUCreditCardInfo)Class.forName(getCreditCardInfoClass()).newInstance();

		return cci;
	}

	/**
	 * @param pParam - Object
	 * @param pResult - PipelineResult
	 * @return Integer
	 * @throws InstantiationException - InstantiationException
	 * @throws IllegalAccessException - IllegalAccessException
	 * @throws  ClassNotFoundException - ClassNotFoundException
	 */
	public int runProcess(Object pParam, PipelineResult pResult)
	throws InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUCreateCreditCardInfo.runProcess :STARTS");
		}
		PaymentManagerPipelineArgs params = (PaymentManagerPipelineArgs)pParam;
		Order order = params.getOrder();
		TRUCreditCard creditCard = (TRUCreditCard)params.getPaymentGroup();
		double amount = params.getAmount();

		TRUCreditCardInfo creditCardInfo = getCreditCardInfo();

		if (isLoggingDebug()) {
			vlogDebug("Params :{0},Order : {1},CreditCard : {2},Amount : {3}",params,order,creditCard,amount);
		}
		addDataToCreditCardInfo(order, creditCard, amount, params, creditCardInfo);

		if (isLoggingDebug()) {
			vlogDebug("Putting CreditCardInfo object into pipeline: " + creditCardInfo.toString());
		}
		params.setPaymentInfo(creditCardInfo);

		if (isLoggingDebug()) {
			vlogDebug("ProcTRUCreateCreditCardInfo.runProcess :ENDS");
		}
		return SUCCESS;
	}

	/**
	 * @return int[]
	 */
	public int[] getRetCodes()
	{
		int[] retCodes = { SUCCESS };
		return retCodes;
	}
}