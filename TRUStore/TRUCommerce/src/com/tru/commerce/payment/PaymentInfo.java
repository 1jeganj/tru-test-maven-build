package com.tru.commerce.payment;

import java.io.Serializable;


/**
 * @author Professional access.
 * @version 1.0
 */
public interface PaymentInfo extends Serializable {
	
	/**
	 * @return OrderID
	 */
	String getOrderId();

}
