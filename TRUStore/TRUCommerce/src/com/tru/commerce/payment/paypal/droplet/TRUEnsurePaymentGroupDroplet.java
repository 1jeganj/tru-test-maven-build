
package com.tru.commerce.payment.paypal.droplet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import atg.commerce.CommerceException;
import atg.commerce.order.CreditCard;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.payment.paypal.TRUPayPal;




/**
 * This droplet will check if no payment group is assigned to order then create and assign a credit card payment group to order.
 * 
 * @author PA 
 * @version 1.0
 */
public class TRUEnsurePaymentGroupDroplet extends DynamoServlet {

	/** Input parameter name ORDER. */
	private static final ParameterName ORDER = ParameterName.getParameterName("order");

	/**
	 * TRUOrder manager.
	 */
	private TRUOrderManager mOrderManager;

	/**
	 * Transaction manager.
	 */
	private TransactionManager mTransactionManager;

	/**
	 * Gets the order manager.
	 *
	 * @return the payment group manager.
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * Sets the order manager.
	 *
	 * @param pOrderManager            - the payment group manager.
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	/**
	 * Gets the transaction manager.
	 *
	 * @return the transaction manager.
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	/**
	 * Sets the transaction manager.
	 *
	 * @param pTransactionManager            - the transactional manager.
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}

	/**
	 * <p>
	 * If the credit card payment group is empty, this droplet will create one
	 * and add it to the order. Wrapped in a transaction since we're making
	 * order modifications.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value
	 * @exception ServletException
	 *                if an error occurs
	 * @exception IOException
	 *                if an error occurs
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Ensuring order contains credit card pg");
		}

		final Object orderObject = pRequest.getObjectParameter(ORDER);

		if ((orderObject == null) || !(orderObject instanceof Order)) {
			if (isLoggingDebug()) {
				vlogDebug("Bad order parameter passed: null=" + (orderObject == null) + ". If null=false, then wrong object type.");
			}
			return;
		}
		final Order order = (Order) orderObject;
		CreditCard ccPayG = null;

		try {
			final TransactionDemarcation td = new TransactionDemarcation();

			try {
				td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);

				ccPayG = ((TRUOrderTools) getOrderManager().getOrderTools()).getCreditCard(order);

				if (ccPayG == null) {
					if (isLoggingDebug()) {
						vlogDebug("Creating credit card pg, current one is null.");
					}

					try {
						final List<PaymentGroup> paymentGroups = order.getPaymentGroups();
						final int numPayGroups = order.getPaymentGroupCount();

						PaymentGroup pg = null;

						for (int i = 0; i < numPayGroups; ++i) {
							pg = (PaymentGroup) paymentGroups.get(i);

							if (pg instanceof TRUPayPal) {
								return;
							}
						}
						final CreditCard newCard = (CreditCard) ((TRUOrderTools) getOrderManager().getOrderTools())
								.createPaymentGroup(TRUCheckoutConstants.CREDITCARD);
						order.addPaymentGroup(newCard);
						getOrderManager().updateOrder(order);

					} catch (CommerceException ce) {
						if (isLoggingError()) {
							logError("Couldn't create new cc group @Class::TRUEnsurePaymentGroupDroplet@method::service()", ce);
						}
					}
				} else {
					if (isLoggingDebug()) {
						vlogDebug("Credit card pg is not null, no need to create.");
						vlogDebug("Billing address is null: " + (ccPayG.getBillingAddress() == null));
					}
				}
			} finally {
				td.end();
			}
		} catch (TransactionDemarcationException e) {
			if (isLoggingError()) {
				logError("Transaction error @Class::TRUEnsurePaymentGroupDroplet@method::service()", e);
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("cc from orderTools is null: " + (ccPayG == null));
		}
	}
}
