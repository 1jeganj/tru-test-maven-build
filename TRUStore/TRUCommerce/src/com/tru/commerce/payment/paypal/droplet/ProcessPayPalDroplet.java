package com.tru.commerce.payment.paypal.droplet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.core.util.Address;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.common.TRUConstants;
import com.tru.common.cml.SystemException;
import com.tru.payment.paypal.TRUPaypalUtilityManager;
import com.tru.radial.payment.paypal.PayPalAdaptor;
import com.tru.radial.payment.paypal.bean.GetExpressCheckOutRequest;
import com.tru.radial.payment.paypal.bean.GetExpressCheckoutResponse;

/**
 * This droplet will call the GetExpressCheckout API call, get response and populate the response to appropriate objects .
 * 
 * @author Professional Access
 * @version 1.0
 */
public class ProcessPayPalDroplet extends DynamoServlet {
	
	/** property to hold mAddressClassName instance. */
	private String mAddressClassName;
	
	/** Hold's the instance of  TransactionManager.*/
	private TransactionManager mTransactionManager;
	
	/**
	 * Property to hold reference to TRUOrderTools.
	 */
	private TRUOrderTools mOrderTools;
	
	/**
	 * Property to hold reference to order manager.
	 */
	private TRUOrderManager mOrderManager;
	
	/** property to Hold payPal utility manager. */
	private TRUPaypalUtilityManager mPayPalUtilityManager;
	
	/**
	 * Property to hold instance of PayPalAdaptor.
	 */
	private PayPalAdaptor  mPayPalAdaptor;
	
	/**
	 * @return the orderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * @param pOrderManager the orderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		this.mOrderManager = pOrderManager;
	}
	
	/**
	 * @return the orderTools
	 */
	public TRUOrderTools getOrderTools() {
		return mOrderTools;
	}

	/**
	 * @param pOrderTools the orderTools to set
	 */
	public void setOrderTools(TRUOrderTools pOrderTools) {
		this.mOrderTools = pOrderTools;
	}
	
	/**
	 * Gets the address class name.
	 * 
	 * @return the addressClassName
	 */
	public String getAddressClassName() {
		return mAddressClassName;
	}

	/**
	 * Sets the address class name.
	 * 
	 * @param pAddressClassName
	 *            the AddressClassName to set
	 */
	public void setAddressClassName(String pAddressClassName) {
		mAddressClassName = pAddressClassName;
	}
	
	/**
	 * @return the mTransactionManager
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	/**
	 * @param pTransactionManager the pTransactionManager to set
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		this.mTransactionManager = pTransactionManager;
	}
	
	/**
	 * Gets the PayPal Utility Manager.
	 * 
	 * @return the PayPal Utility Manager.
	 */
	public TRUPaypalUtilityManager getPayPalUtilityManager() {
		return mPayPalUtilityManager;
	}

	/**
	 * @param pPayPalUtilityManager the PayPalUtilityManager to set
	 */
	public void setPayPalUtilityManager(TRUPaypalUtilityManager pPayPalUtilityManager) {
		this.mPayPalUtilityManager = pPayPalUtilityManager;
	}
	
	/**
	 * 	
	 * @return mPayPalAdaptor instance of PayPalAdaptor
	 */
	public PayPalAdaptor getPayPalAdaptor() {
		return mPayPalAdaptor;
	}
	
	/**
	 * @param pPayPalAdaptor the PayPalAdaptor to set
	 */
	public void setPayPalAdaptor(PayPalAdaptor pPayPalAdaptor) {
		this.mPayPalAdaptor = pPayPalAdaptor;
	}
	
	/**
	 * This method   call the GetExpressCehckout payPal call, get response and populate the response to appropriate objects. 
     * 
	 * 
	 * @param pRequest   of type DynamoHttpServletRequest
	 * @param pResponse  of type DynamoHttpServletResponse
	 * @throws IOException    IO Exception
	 * @throws ServletException    ServletException
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
	throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start of ProcessPayPalDroplet service method");
		}		
		String token = (String)pRequest.getLocalParameter(TRUConstants.PAYPAL_TOKEN);
		Object orderObject = pRequest.getObjectParameter(TRUConstants.ORDER_PARAM);
		Order order = (Order)orderObject;
		boolean rollBack =false;
		//Map<String,String> shippingAddressMap = null;
		//Map<String,String> billingAddressMap = null;
		TransactionManager transactionManager = null;
		TransactionDemarcation transactionDemarcation  = null;		
		if(!StringUtils.isBlank(token)){
			GetExpressCheckoutResponse getExpCheckoutResponse = null;
			GetExpressCheckOutRequest getExpCheckoutRequest= new GetExpressCheckOutRequest();
			getExpCheckoutRequest.setOrderId(order.getId());
			getExpCheckoutRequest.setPageName(((TRUOrderImpl)order).getCurrentTab());
			getExpCheckoutRequest.setTokenId(token);		
			try{
				   getExpCheckoutResponse = (GetExpressCheckoutResponse) getPayPalAdaptor().getExpressCheckout(getExpCheckoutRequest);				
				  // shippingAddressMap = getPayPalUtilityManager().populateShippingInfo(getExpCheckoutResponse);
				 //  billingAddressMap = getPayPalUtilityManager().populateBillingInfo(getExpCheckoutResponse);
				   transactionManager = getTransactionManager();
				   transactionDemarcation = new TransactionDemarcation();	
			     	//begin the transaction
				   if (transactionManager != null) {
					   transactionDemarcation.begin(transactionManager, TransactionDemarcation.REQUIRED);
				   }
				   synchronized (order) {
					   if(getExpCheckoutResponse!=null){
						  // Address shippingAddress = AddressTools.createAddressFromMap(shippingAddressMap, getAddressClassName());
						   Address shippingAddress = getExpCheckoutResponse.getShippingAddress();
						   // Populating shipping address received from payPal site
						   getOrderTools().addShippingAddress(order, shippingAddress);
					 //  }
					   //if(billingAddressMap!=null){
						   //Address billingAddress = AddressTools.createAddressFromMap(billingAddressMap, getAddressClassName());
						   Address billingAddress = getExpCheckoutResponse.getBillingAddress();
						   // Populating billing address received from payPal site and payerId
						   getOrderTools().updatePayPalDetails(order, billingAddress, getExpCheckoutResponse.getPayerId());
					   }
					   getOrderManager().updateOrder(order);
				   }			
			}catch (CommerceException e) {
				rollBack = true;
				if (isLoggingError()) {
					logError("CommerceException in ProcessPayPalDroplet.service method  for order Id : " + order.getId(), e);
				}				
			}
			/*catch (InstantiationException ie) {
				rollBack = true;
				if(isLoggingError()){
					logError("Getting InstantiationException in ProcessPayPalDroplet.service()",ie);
				}
			}
			catch(IllegalAccessException ie){
				if(isLoggingError()){
					logError("Getting IllegalAccessException in ProcessPayPalDroplet.service()",ie);
				}

			}catch (ClassNotFoundException ce) {
				rollBack = true;
				if(isLoggingError()){
					logError("Getting ClassNotFoundException in ProcessPayPalDroplet.service()",ce);
				}
			}catch (IntrospectionException ie) {
				rollBack = true;
				if(isLoggingError()){
					logError("Getting IntrospectionException in ProcessPayPalDroplet.service",ie);
				}
			}*/
			catch (TransactionDemarcationException tde) {
				rollBack = true;
				if (isLoggingError()) {
					logError("TransactionDemarcationException occured during ProcessPayPalDroplet.service: ",tde);
				}
			} catch (SystemException se) {
				rollBack = true;
				if (isLoggingError()) {
					logError("SystemException occured during ProcessPayPalDroplet.service: ",se);
				}
			}finally {
				try {
					if (transactionManager != null) {
						transactionDemarcation.end(rollBack);
					}
				} catch (TransactionDemarcationException tde) {
					if (isLoggingError()) {
						logError("TransactionDemarcationException occured during ProcessPayPalDroplet.service: ",tde);
					}
				}
			}
		}	
		if (isLoggingDebug()) {
			logDebug("End of ProcessPayPalDroplet service method");
		}
	}
}