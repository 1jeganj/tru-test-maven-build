
package com.tru.commerce.payment.paypal;

import com.tru.commerce.order.payment.TRUPaymentStatus;
import com.tru.radial.common.beans.TRURadialError;

/**
 * This class  defines paypal payment status.This class redefined as part of JDA integration for holding the paypal ransaction status.
 * If the transaction is SUCCESS,paymentStatus value will be available and persisted.Inother cases remaining values will be populated.
 * @author PA
 * @version 1.0
 */
public class TRUPayPalStatus extends TRUPaymentStatus {
	
	/**
	 * This property hold serialVersionUid.
	 */
	private static final long serialVersionUID = 1L;
	
	
		
	/** The m pay pal response code. */
	private String mPayPalAuthResponseCode;
	
	/** The m pay pal auth status. */
	private String mPayPalAuthStatus;
	
	/** The m pay pal auth pending reason. */
	private String mPayPalAuthPendingReason;
	
	/** The m pay pal auth reason code. */
	private String mPayPalAuthReasonCode;
	
	/** The m pay pal short error message. */
	private String mPayPalAuthShortErrorMessage;
	
	/** The m pay pal error code. */
	private String mPayPalAuthErrorCode;


	/** The m pay pal payment status. */
	private String mPayPalPaymentStatus;
	
	/** The m pay pal payment pending reason. */
	private String mPayPalPaymentPendingReason;
	
	/** The m pay pal payment reason code. */
	private String mPayPalPaymentReasonCode;
	
	


	/**
	 * Gets the pay pal auth status.
	 *
	 * @return the pay pal auth status
	 */
	public String getPayPalAuthStatus() {
		return mPayPalAuthStatus;
	}

	/**
	 * Sets the pay pal auth status.
	 *
	 * @param pPayPalAuthStatus the new pay pal auth status
	 */
	public void setPayPalAuthStatus(String pPayPalAuthStatus) {
		this.mPayPalAuthStatus = pPayPalAuthStatus;
	}

	/**
	 * Gets the pay pal auth pending reason.
	 *
	 * @return the pay pal auth pending reason
	 */
	public String getPayPalAuthPendingReason() {
		return mPayPalAuthPendingReason;
	}

	/**
	 * Sets the pay pal auth pending reason.
	 *
	 * @param pPayPalAuthPendingReason the new pay pal auth pending reason
	 */
	public void setPayPalAuthPendingReason(String pPayPalAuthPendingReason) {
		this.mPayPalAuthPendingReason = pPayPalAuthPendingReason;
	}

	/**
	 * Gets the pay pal auth reason code.
	 *
	 * @return the pay pal auth reason code
	 */
	public String getPayPalAuthReasonCode() {
		return mPayPalAuthReasonCode;
	}

	/**
	 * Sets the pay pal auth reason code.
	 *
	 * @param pPayPalAuthReasonCode the new pay pal auth reason code
	 */
	public void setPayPalAuthReasonCode(String pPayPalAuthReasonCode) {
		this.mPayPalAuthReasonCode = pPayPalAuthReasonCode;
	}

	

	/**
	 * Gets the pay pal payment status from DoExpressResponse.
	 *
	 * @return the pay pal payment status
	 */
	public String getPayPalPaymentStatus() {
		return mPayPalPaymentStatus;
	}

	/**
	 * Sets the pay pal payment status.
	 *
	 * @param pPayPalPaymentStatus the new pay pal payment status
	 */
	public void setPayPalPaymentStatus(String pPayPalPaymentStatus) {
		this.mPayPalPaymentStatus = pPayPalPaymentStatus;
	}

	/**
	 * Gets the pay pal payment pending reason.
	 *
	 * @return the pay pal payment pending reason
	 */
	public String getPayPalPaymentPendingReason() {
		return mPayPalPaymentPendingReason;
	}

	/**
	 * Sets the pay pal payment pending reason from the from DoExpressResponse.
	 *
	 * @param pPayPalPaymentPendingReason the new pay pal payment pending reason
	 */
	public void setPayPalPaymentPendingReason(String pPayPalPaymentPendingReason) {
		this.mPayPalPaymentPendingReason = pPayPalPaymentPendingReason;
	}

	/**
	 * Gets the pay pal payment reason code from DoExpressResponse.
	 *
	 * @return the pay pal payment reason code
	 */
	public String getPayPalPaymentReasonCode() {
		return mPayPalPaymentReasonCode;
	}

	/**
	 * Sets the pay pal payment reason code.
	 *
	 * @param pPayPalPaymentReasonCode the new pay pal payment reason code
	 */
	public void setPayPalPaymentReasonCode(String pPayPalPaymentReasonCode) {
		this.mPayPalPaymentReasonCode = pPayPalPaymentReasonCode;
	}

	/**
	 * Gets the pay pal auth response code.
	 *
	 * @return the pay pal auth response code
	 */
	public String getPayPalAuthResponseCode() {
		return mPayPalAuthResponseCode;
	}

	/**
	 * Sets the pay pal auth response code.
	 *
	 * @param pPayPalAuthResponseCode the new pay pal auth response code
	 */
	public void setPayPalAuthResponseCode(String pPayPalAuthResponseCode) {
		this.mPayPalAuthResponseCode = pPayPalAuthResponseCode;
	}

	/**
	 * Gets the pay pal auth short error message.
	 *
	 * @return the pay pal auth short error message
	 */
	public String getPayPalAuthShortErrorMessage() {
		return mPayPalAuthShortErrorMessage;
	}

	/**
	 * Sets the pay pal auth short error message.
	 *
	 * @param pPayPalAuthShortErrorMessage the new pay pal auth short error message
	 */
	public void setPayPalAuthShortErrorMessage(String pPayPalAuthShortErrorMessage) {
		this.mPayPalAuthShortErrorMessage = pPayPalAuthShortErrorMessage;
	}

	/**
	 * Gets the pay pal auth error code.
	 *
	 * @return the pay pal auth error code
	 */
	public String getPayPalAuthErrorCode() {
		return mPayPalAuthErrorCode;
	}

	/**
	 * Sets the pay pal auth error code.
	 *
	 * @param pPayPalAuthErrorCode the new pay pal auth error code
	 */
	public void setPayPalAuthErrorCode(String pPayPalAuthErrorCode) {
		this.mPayPalAuthErrorCode = pPayPalAuthErrorCode;
	}

	/** The m radial error. */
	private TRURadialError mRadialError;
	/**
	 * Gets the radial error.
	 *
	 * @return the mRadialError
	 */
	public TRURadialError getRadialError() {
		return mRadialError;
	}

	/**
	 * Sets the radial error.
	 *
	 * @param pRadialError the new radial error
	 */
	public void setRadialError(TRURadialError pRadialError) {
		this.mRadialError = pRadialError;
	}

	
		
}