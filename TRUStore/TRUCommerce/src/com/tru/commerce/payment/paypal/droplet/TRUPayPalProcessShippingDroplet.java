package com.tru.commerce.payment.paypal.droplet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.processor.ShippingAddrValidator;
import atg.commerce.order.processor.ShippingAddrValidatorImpl;
import atg.commerce.order.purchase.ShippingGroupContainerService;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.purchase.TRUShippingGroupContainerService;
import com.tru.radial.commerce.payment.paypal.util.TRUPayPalConstants;

/**
 * This DROPLET fetch the shipping details captured via PAYPAL.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUPayPalProcessShippingDroplet extends DynamoServlet {
		/**Property to hold ShippingAddrValidator. */
	  private ShippingAddrValidator mShippingAddressValidator;
	
	 /** Input parameter name ORDER. */
	  public static final ParameterName ORDER = ParameterName.getParameterName(TRUPayPalConstants.ORDER_PARAM);
	
	  /** Input parameter name SHIPPINGGROUPCONTAINER. */
	  public static final ParameterName SHIPPINGGROUPCONTAINER = ParameterName.getParameterName(TRUPayPalConstants.SHIPPINGGROUPCONTAINER_PARAM);

	
	/**
	 * @return the shippingAddressValidator
	 */
	public ShippingAddrValidator getShippingAddressValidator() {
		return mShippingAddressValidator;
	}

	/**
	 * @param pShippingAddressValidator the shippingAddressValidator to set
	 */
	public void setShippingAddressValidator(
			ShippingAddrValidator pShippingAddressValidator) {
		mShippingAddressValidator = pShippingAddressValidator;
	}

	/**
	 * This method gets the shipping details associated with PAYPAL shipping group
     * 
	 * 
	 * @param pRequest   of type DynamoHttpServletRequest
	 * @param pResponse  of type DynamoHttpServletResponse
	 * @throws IOException    IO Exception
	 * @throws ServletException    ServletException
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
	throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUPayPalProcessShippingDroplet service method");
		}
		Address defaultAddress = null;
		ShippingGroup defaultShippingGroup = null;
		Order order = (Order)pRequest.getObjectParameter(ORDER);
		ShippingGroupContainerService shippingContainerService = (ShippingGroupContainerService)
				pRequest.getObjectParameter(SHIPPINGGROUPCONTAINER);
		if(order == null || shippingContainerService == null) {
			if (isLoggingDebug()) {
				vlogDebug("Bad order parameter passed: null="+(order == null) +
						"and/or Bad shippingContainerService passed: null="+(shippingContainerService == null));
			}
			return;
		}
		Map shippingGroupMap = shippingContainerService.getShippingGroupMap();
		String defaultShippingGroupName = shippingContainerService.getDefaultShippingGroupName();
		if(shippingGroupMap != null && !StringUtils.isBlank(defaultShippingGroupName) && !shippingGroupMap.isEmpty()) {
			defaultShippingGroup = (ShippingGroup) shippingGroupMap.get(defaultShippingGroupName);
			if(defaultShippingGroup instanceof TRUHardgoodShippingGroup) {
				defaultAddress = ((TRUHardgoodShippingGroup)defaultShippingGroup).getShippingAddress();
				if (isLoggingDebug()) {
					vlogDebug("default Address in shipping group exists" + defaultAddress);
				}
				if(defaultAddress instanceof ContactInfo) {
					ShippingAddrValidator sav = getShippingAddressValidator();
					if (sav instanceof ShippingAddrValidatorImpl && !((ShippingAddrValidatorImpl)sav).validateAddress(defaultAddress).isEmpty()) {
							defaultAddress = ((TRUShippingGroupContainerService)shippingContainerService).getShippingAddress();
					}
				}
				pRequest.setParameter(TRUPayPalConstants.DEFAULT_ADDRESS_PARAM, defaultAddress);
			}
		}
		
		if (isLoggingDebug()) {
			vlogDebug("End of TRUPayPalProcessShippingDroplet service method");
		}
		pRequest.serviceLocalParameter(TRUCheckoutConstants.OPARAM_OUTPUT, pRequest, pResponse);
	}
}