package com.tru.commerce.payment.paypal;

import java.sql.Timestamp;

import atg.commerce.CommerceException;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroupImpl;
import atg.commerce.order.RepositoryAddress;
import atg.commerce.order.RepositoryContactInfo;
import atg.core.util.Address;

import com.tru.commerce.order.TRUPropertyNameConstants;

/** This is the payment group for PayPal payment method.
 * 
 * @author PA
 * @version 1.0
 */

public class TRUPayPal extends PaymentGroupImpl {
	
	/**
	 * To hold serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	 /**
     * Constant  to hold double zero.
	 */
	private static final double DOUBLE_ZERO = 0.0;
	
	
	 /**
     * property TRANSACTION_ID.
     */
    private static final String PROP_NAME_TRANSACTION_ID = "transactionId";
	
	/**
	 * Initially set the mBillingAddress.
	 */
	private Address mBillingAddress;
	 
	 /**
	  * property PROP_REVERSED_AMOUNT.
	  */
	private static final String PROP_REVERSED_AMOUNT = "reversedAmount";
    
    /**
     * @return the Transaction Id.
     */
    public String getTransactionId() {
        return (String)getPropertyValue(PROP_NAME_TRANSACTION_ID);
    }

    /**
     * @param pTransactionId the cardHolderName to set
     */
    public void setTransactionId(String pTransactionId) {
        setPropertyValue(PROP_NAME_TRANSACTION_ID, pTransactionId);
    }
	
	/**
      * token for the order transaction.
      * @return token
     */
    public String getToken() {
        return (String)getPropertyValue(TRUPropertyNameConstants.PAYPAL_TOKEN);
    }
    
    /**
     * @param pToken - String
     */    
    public void setToken(String pToken) {
        setPropertyValue(TRUPropertyNameConstants.PAYPAL_TOKEN, pToken);
    }

    /**
     * Payer Id of payPal user.
     * @return payerId
     */
    public String getPayerId() {
        return (String)getPropertyValue(TRUPropertyNameConstants.PAYPAL_PAYER_ID);
    }
    
    /**
     * @param pPayerId - String
     */
    public void setPayerId(String pPayerId) {
        setPropertyValue(TRUPropertyNameConstants.PAYPAL_PAYER_ID, pPayerId);
    }
    
	 /**
	 * @return the payerStatus
	 */
	public String getPayerStatus() {
		return (String)getPropertyValue(TRUPropertyNameConstants.PP_PAYER_STATUS);
	}
	
	/**
	 * @param pPayerStatus the payerStatus to set
	 */
	public void setPayerStatus(String pPayerStatus) {
		setPropertyValue(TRUPropertyNameConstants.PP_PAYER_STATUS, pPayerStatus);
	}
	
	
    /**
	 * @return String
	 */
	public String getFirstName() {
		return (String)getPropertyValue(TRUPropertyNameConstants.PP_FIRST_NAME);
	}
	
	/**
	 * 
	 * @param pFirstName - FirstName
	 */
	public void setFirstName(String pFirstName) {
		setPropertyValue(TRUPropertyNameConstants.PP_FIRST_NAME, pFirstName);
	}
	
	/**
	 * @return String
	 */
	public String getLastName() {
		return (String)getPropertyValue(TRUPropertyNameConstants.PP_LAST_NAME);
	}
	
	/**
	 * 
	 * @param pLastName - LastName
	 */
	public void setLastName(String pLastName) {
		setPropertyValue(TRUPropertyNameConstants.PP_LAST_NAME, pLastName);
	}
	
	/**
	 * @return String
	 */
	public String getMiddleName() {
		return (String)getPropertyValue(TRUPropertyNameConstants.PP_MIDDLE_NAME);
	}
	
	/**
	 * 
	 * @param pMiddleName - MiddleName
	 */
	public void setMiddleName(String pMiddleName) {
		setPropertyValue(TRUPropertyNameConstants.PP_MIDDLE_NAME, pMiddleName);
	}
	
	/**
	 * @return String
	 */
	public String getEmail() {
		return (String)getPropertyValue(TRUPropertyNameConstants.PP_EMAIL);
	}
	
	/**
	 * 
	 * @param pEmail - Email
	 */
	public void setEmail(String pEmail) {
		setPropertyValue(TRUPropertyNameConstants.PP_EMAIL, pEmail);
	}
	/**
	 * 
	 * @param pPayPalCorRelationId
	 *            the pPayPalCorRelationId to set
	 */
	public void setPayPalCorRelationId(String pPayPalCorRelationId){
		setPropertyValue(TRUPropertyNameConstants.PAYPAL_CORRELATIONID,pPayPalCorRelationId);
	}

	/**
	 * 
	 * @return the payPalCorRelationId
	 */
	public String getPayPalCorRelationId(){
		return (String)getPropertyValue(TRUPropertyNameConstants.PAYPAL_CORRELATIONID);
	}
	
	/**
	 * 
	 * @param pPayPalAck
	 *            the pPayPalAck to set
	 */
	public void setPayPalAck(String pPayPalAck){
		setPropertyValue(TRUPropertyNameConstants.PAYPAL_ACK,pPayPalAck);
	}

	/**
	 * 
	 * @return the payPalAck Name
	 */
	public String getPayPalAck(){
		return (String)getPropertyValue(TRUPropertyNameConstants.PAYPAL_ACK);
	}
	
	/**
	 * 
	 * @param pPayPalTransactionId
	 *            the pPayPalTransactionId to set
	 */
	public void setPayPalTransactionId(String pPayPalTransactionId){
		setPropertyValue(TRUPropertyNameConstants.PAYPAL_TRANSACTIONID,pPayPalTransactionId);
	}

	/**
	 * 
	 * @return the payPalTransactionId Name
	 */
	public String getPayPalTransactionId(){
		return (String)getPropertyValue(TRUPropertyNameConstants.PAYPAL_TRANSACTIONID);
	}
	
   	/**
   	 * @return the transactionTimestamp
   	 */
   	public Timestamp getTransactionTimestamp() {
   		return (Timestamp)getPropertyValue(TRUPropertyNameConstants.PAYPAL_TRANSACTION_TIMESTAMP);
   	}

   	/**
   	 * @param pPayPalGetExpTransactionTimestamp
   	 *            the payPalGetExpTransactionTimestamp to set
   	 */
   	public void setTransactionTimestamp(
   			Timestamp pPayPalGetExpTransactionTimestamp) {
   		setPropertyValue(TRUPropertyNameConstants.PAYPAL_TRANSACTION_TIMESTAMP,pPayPalGetExpTransactionTimestamp);
   	}
   	
    /**
	  * This method sets the Shipping Address.
	  * @return billingAddress 
	  */
    public Address getBillingAddress()
    {
      return this.mBillingAddress;
    }
    /**
	 * @param pBillingAddress the billingAddress to set
     * @throws CommerceException  CommerceException
	 */
    public void setBillingAddress(Address pBillingAddress) throws CommerceException
    {
      if ((pBillingAddress instanceof RepositoryContactInfo) || (pBillingAddress instanceof RepositoryAddress) || (pBillingAddress == null))


      {
        if (this.mBillingAddress != null){
        	this.mBillingAddress.deleteObservers();
        }
        this.mBillingAddress = pBillingAddress;
        this.mBillingAddress.addObserver(this);
      }
      else
      {
        try
        {
          if (this.mBillingAddress == null){
        	  this.mBillingAddress = (Address)pBillingAddress.getClass().newInstance();
          }
          OrderTools.copyAddress(pBillingAddress, this.mBillingAddress);
        }
        catch (InstantiationException ie) {
          throw new CommerceException(ie.getMessage(),ie);
        }
        catch (IllegalAccessException iae) {
          throw new CommerceException(iae.getMessage(),iae);
        }
        
      }
      setSaveAllProperties(true);
    }
    
    /**
     * @return the reversalAmount
     */
    public double getReversalAmount() {
        Object revrsalAmount = (Object) getPropertyValue(PROP_REVERSED_AMOUNT);
        if (revrsalAmount != null) {
            return ((Double)revrsalAmount).doubleValue();
        } else {
            return DOUBLE_ZERO;
        }
    }    
    /**
     * @param pReversalAmount the reversalAmount to set
     */
    public void setReversalAmount(double pReversalAmount) {
        setPropertyValue(PROP_REVERSED_AMOUNT, pReversalAmount);
    }

	/**
	 * Gets the payer country.
	 *
	 * @return the payer country
	 */
	public String getPayerCountry() {		
		return (String)getPropertyValue(TRUPropertyNameConstants.PROP_NAME_PAYER_COUNTRY);
	}

	/**
	 * Sets the payer country.
	 *
	 * @param pPayerCountry the new payer country
	 */
	public void setPayerCountry(String pPayerCountry) {
		setPropertyValue(TRUPropertyNameConstants.PROP_NAME_PAYER_COUNTRY, pPayerCountry);
	}
    

	/**
	 * Gets the phone no.
	 *
	 * @return the phone no
	 */
	public String getPayerPhoneNumber() {
		return (String)getPropertyValue(TRUPropertyNameConstants.PP_PHONE_NUMBER);
	}

	/**
	 * Sets the phone no.
	 *
	 * @param pPhoneNo the new phone no
	 */
	public void setPayerPhoneNumber(String pPhoneNo) {
		setPropertyValue(TRUPropertyNameConstants.PP_PHONE_NUMBER, pPhoneNo);
	}
    
    
}
