
package com.tru.commerce.payment;

import atg.commerce.order.Order;
import atg.core.util.Address;
import atg.nucleus.GenericService;

/** 
 * This class hold the PAYPAL payment information.
 * 
 * @author Professional Access
 * @version 1.0
 * 
 */
public class TRUPayPalPaymentInfo extends GenericService {
	/**
	 * This Property holds the mShippingAmount.
	 */
	private String mShippingAmount;
	/**
	 * This Property holds the mBuyerIpAddress.
	 */
	private String mBuyerIpAddress;
	/**
	 * This Property holds the mAmount.
	 */
	private String mAmount;
	/**
	 * This Property holds the countryCode.
	 */
	private String mCountryCode;	
	
	/**
	 * This Property holds the currencyCode.
	 */
	private String mCurrencyCode;
	
	/**
	 * This Property holds the player Id.
	 */
	private String mPayerId;
	
	/**
	 * This Property holds the paymentAction.
	 */
	private String mPaymentAction;

	/**
	 * This Property holds the PayPalToken.
	 */
	private String mPayPalToken;
	/**
	 * This Property holds the ShiptoFirstName.
	 */
	private String mShipToFirstName;
	
	/**
	 * This Property holds the shipToStreet1.
	 */
	private String mShipToStreet1;
	
	/**
	 * This Property holds the shipToStreet2.
	 */
	private String mShipToStreet2;
	
	/**
	 * This Property holds the shipToState.
	 */
	private String mShipToState;	
	
	/**
	 * This Property holds the shipToCity.
	 */
	private String mShipToCity;
	
	/**
	 * This Property holds the shipToZip.
	 */
	private String mShipToZip;
	/**
	 * This Property holds the shipToPhoneNumber.
	 */
	private String mShipToPhoneNumber;	
	
	/**
	 * This Property holds the Transaction Id.
	 */
	private String mTransactionId;
	
	/**
	 * This Property holds the mTRUOrderId.
	 */
	private String mTRUOrderId;	
	
	/**
	 * @holds the Order Id
	 */
	private String mOrderId;
	
	/**
	 * This Property holds the comments.
	 */
	private String mComments;
	
	/**
	 * This Property holds the siteId.
	 */
	private String mSiteId;
	
	/**
	 * This Property holds the order.
	 */
	private Order mOrder;
	
	/**
	 * This Property holds the billing address.
	 */
	private Address mBillingAddress;
	
	/**
	 * 
	 */
	private String mEmail;
	
	/** The m page name. */
	private String mPageName;
	
	/**
	 * @return the mPageName
	 */
	public String getPageName() {
		return mPageName;
	}

	/**
	 * Sets the page name.
	 *
	 * @param pPageName the new page name
	 */
	public void setPageName(String pPageName) {
		mPageName = pPageName;
	}
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return mEmail;
	}
	/**
	 * @param pEmail the email to set
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}
	
	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return mSiteId;
	}
	/**
	 * @param pSiteId the SiteId to set
	 */
	public void setSiteId(String pSiteId) {
		this.mSiteId = pSiteId;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return mComments;
	}
	/**
	 * @param pComments the comments to set
	 */
	public void setComments(String pComments) {
		this.mComments = pComments;
	}
	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * @param pOrderId the orderId to set
	 */
	public void setOrderId(String pOrderId) {
		mOrderId = pOrderId;
	}	
	
	/**
	 *Gets the  mTRUOrderId.
	 *@return the mTRUOrderId
	 */
	public String getTRUOrderId() {
		return mTRUOrderId;
	}
	/**
	 * Sets the mTRUOrderId.
	 * @param pTRUOrderId the mTRUOrderId to set
	 */
	public void setTRUOrderId(String pTRUOrderId) {
		this.mTRUOrderId = pTRUOrderId;
	}
	/**
	 * @return the mAmount
	 */
	public String getAmount() {
		return mAmount;
	}

	/**
	 * @param pAmount the mAmount to set
	 */
	public void setAmount(String pAmount) {
		this.mAmount = pAmount;
	}


	/**
	 * @return the mCountryCode
	 */
	public String getCountryCode() {
		return mCountryCode;
	}

	/**
	 * @param pCountryCode the mCountryCode to set
	 */
	public void setCountryCode(String pCountryCode) {
		this.mCountryCode = pCountryCode;
	}
	/**
	 * @return the mCurrencyCode
	 */
	public String getCurrencyCode() {
		return mCurrencyCode;
	}

	/**
	 * @param pCurrencyCode the mCurrencyCode to set
	 */
	public void setCurrencyCode(String pCurrencyCode) {
		this.mCurrencyCode = pCurrencyCode;
	}
	/**
	 * @return the playerId
	 */
	public String getPayerId() {
		return mPayerId;
	}

	/**
	 * @param pPlayerId the playerId to set
	 */
	public void setPayerId(String pPlayerId) {
		mPayerId = pPlayerId;
	}
	/**
	 * @return the mPaymentAction
	 */
	public String getPaymentAction() {
		return mPaymentAction;
	}

	/**
	 * @param pPaymentAction the pPaymentAction to set
	 */
	public void setPaymentAction(String pPaymentAction) {
		this.mPaymentAction = pPaymentAction;
	}
	/**
	 * @return the payPalToken
	 */
	public String getPayPalToken() {
		return mPayPalToken;
	}

	/**
	 * @param pPayPalToken the payPalToken to set
	 */
	public void setPayPalToken(String pPayPalToken) {
		mPayPalToken = pPayPalToken;
	}
	/**
	 * Gets the  mShipToFirstName.
	 *@return the mShipToFirstName
	 */
	public String getShipToFirstName() {
		return mShipToFirstName;
	}

	/**
	 * Sets the mShipToFirstName.
	 * @param pShipToFirstName the mShipToFirstName to set
	 */
	public void setShipToFirstName(String pShipToFirstName) {
		mShipToFirstName = pShipToFirstName;
	}
	/**
	 *Gets the  mShipToStreet1.
	 *@return the mShipToStreet1
	 */
	public String getShipToStreet1() {
		return mShipToStreet1;
	}

	/**
	 * Sets the mShipToStreet1.
	 * @param pShipToStreet1 the mShipToStreet1 to set
	 */
	public void setShipToStreet1(String pShipToStreet1) {
		mShipToStreet1 = pShipToStreet1;
	}
	/**
	 *Gets the  mShipToStreet2.
	 *@return the mShipToStreet2
	 */
	public String getShipToStreet2() {
		return mShipToStreet2;
	}

	/**
	 * Sets the mShipToStreet2.
	 * @param pShipToStreet2 the mShipToStreet2 to set
	 */
	public void setShipToStreet2(String pShipToStreet2) {
		mShipToStreet2 = pShipToStreet2;
	}
	/**
	 *Gets the  mShipToState.
	 *@return the mShipToState
	 */
	public String getShipToState() {
		return mShipToState;
	}

	/**
	 * Sets the mShipToState.
	 * @param pShipToState the mShipToState to set
	 */
	public void setShipToState(String pShipToState) {
		mShipToState = pShipToState;
	}

	/**
	 *Gets the  mShipToCity.
	 *@return the mShipToCity
	 */
	public String getShipToCity() {
		return mShipToCity;
	}
	/**
	 * Sets the mShipToCity.
	 * @param pShipToCity the mShipToCity to set
	 */
	public void setShipToCity(String pShipToCity) {
		mShipToCity = pShipToCity;
	}

	/**
	 *Gets the  mShipToZip.
	 *@return the mShipToZip
	 */
	public String getShipToZip() {
		return mShipToZip;
	}

	/**
	 * Sets the mShipToZip.
	 * @param pShipToZip the mShipToZip to set
	 */
	public void setShipToZip(String pShipToZip) {
		mShipToZip = pShipToZip;
	}

	/**
	 *Gets the  mShipToPhoneNumber.
	 *@return the mShipToPhoneNumber
	 */
	public String getShipToPhoneNumber() {
		return mShipToPhoneNumber;
	}

	/**
	 * Sets the mShipToPhoneNumber.
	 * @param pShipToPhoneNumber the mShipToPhoneNumber to set
	 */
	public void setShipToPhoneNumber(String pShipToPhoneNumber) {
		mShipToPhoneNumber = pShipToPhoneNumber;
	}
	/**
	 * @return the mTransactionId
	 */
	public String getTransactionId() {
		return mTransactionId;
	}

	/**
	 * @param pTransactionId the mTransactionId to set
	 */
	public void setTransactionId(String pTransactionId) {
		this.mTransactionId = pTransactionId;
	}
	/**
	 * @return the buyerIpAddress
	 */
	public String getBuyerIpAddress() {
		return mBuyerIpAddress;
	}
	/**
	 * @param pBuyerIpAddress the buyerIpAddress to set
	 */
	public void setBuyerIpAddress(String pBuyerIpAddress) {
		mBuyerIpAddress = pBuyerIpAddress;
	}
	/**
	 * @return the shippingAmount
	 */
	public String getShippingAmount() {
		return mShippingAmount;
	}
	/**
	 * @param pShippingAmount the shippingAmount to set
	 */
	public void setShippingAmount(String pShippingAmount) {
		mShippingAmount = pShippingAmount;
	}
	/**
	 * @return the order
	 */
	public Order getOrder() {
		return mOrder;
	}
	/**
	 * @param pOrder the order to set
	 */
	public void setOrder(Order pOrder) {
		mOrder = pOrder;
	}
	/**
	 * @return the billingAddress
	 */
	public Address getBillingAddress() {
		return mBillingAddress;
	}
	/**
	 * @param pBillingAddress the billingAddress to set
	 */
	public void setBillingAddress(Address pBillingAddress) {
		mBillingAddress = pBillingAddress;
	}
	
	/**
	 * 
	 */
	private String mShipToName;

	/**
	 * 
	 * @return the shipToName
	 */
	public String getShipToName() {
		return mShipToName;
	}
	
	/**
	 * Sets the ship to name.
	 *
	 * @param pShipToName the new ship to name
	 */
	public void setShipToName(String pShipToName) {
		this.mShipToName = pShipToName;
	}
	
	
}