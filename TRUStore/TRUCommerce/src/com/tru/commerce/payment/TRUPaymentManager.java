package com.tru.commerce.payment;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;

import atg.commerce.CommerceException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.payment.Constants;
import atg.commerce.payment.PaymentException;
import atg.commerce.payment.PaymentManager;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.payment.PaymentStatus;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUGiftCard;
import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.commerce.order.payment.TRUPaymentTools;
import com.tru.commerce.order.payment.applepay.IApplePayProcessor;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardStatus;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardInfo;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardStatus;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.commerce.payment.paypal.TRUPayPalStatus;
import com.tru.commerce.payment.processor.TRUGiftCardProcessor;
import com.tru.commerce.payment.processor.TRUPayPalProcessor;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.radial.exception.TRURadialIntegrationException;


/**
 * This class is extended to add methods for various tender types.
 * 
 * @author Professional Access
 * @version 1.0
 * 
 */
public class TRUPaymentManager extends PaymentManager {

	/**
	 * Property to hold PaymentTools.
	 */
	private TRUPaymentTools mPaymentTools;

	/**
	 * Property to hold ReversePaymentGroupToChainNameMap.
	 */
	private Properties mReversePaymentGroupToChainNameMap; 
	
	/**
	 * Property to hold Gift Card Processor.
	 */
	private TRUGiftCardProcessor mGiftCardPaymentProcessor;
	
	
	/**reference for paypalProcessor.
	 */
	private TRUPayPalProcessor mPayPalProcessor;
	
	/**reference for paypalProcessor.
	 */
	private IApplePayProcessor mApplePayProcessor;
	
	/**
	 * @return the applePayProcessor
	 */
	public IApplePayProcessor getApplePayProcessor() {
		return mApplePayProcessor;
	}


	/**
	 * @param pApplePayProcessor the applePayProcessor to set
	 */
	public void setApplePayProcessor(IApplePayProcessor pApplePayProcessor) {
		mApplePayProcessor = pApplePayProcessor;
	}




	/**
	 * This method has been overridden to set the order of payment t if multiple payments
	 * exist in the order. This is required if order contains credit card and gift card payments
	 * first gift card then credit card should be authorized.
	 * 
	 * @param pOrder - the order
	 * @param pPaymentGroups - the list of payment groups in order
	 * @throws CommerceException - if any CommerceException exists
	 * @return list - the list of payment groups
	 */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public List authorize(Order pOrder, List pPaymentGroups) throws CommerceException {
    	if (isLoggingDebug()) {
    		vlogDebug("TRUPaymentManager.authorize() method.STARTS");
    		vlogDebug("TRUPaymentManager.authorize() method , pOrder:{0} pPaymentGroups:{1}", pOrder, pPaymentGroups);
    	}
    	if (pPaymentGroups == null) {
    		throw new InvalidParameterException(Constants.INVALID_PAYMENT_GROUP_LIST);
    	}
    	String paymentGroupId = null;
    	List<String> failedGroups = new ArrayList(pPaymentGroups.size());
    	List<PaymentGroup> orderedPaymentGroups = getReorganizePaymentGroups(pPaymentGroups);

    	try {
    		for (PaymentGroup paymentGroup : orderedPaymentGroups) {
    			if (isLoggingDebug()) {
    				vlogDebug("TRUPaymentManager.authorize() method , paymentGroup:{0}", paymentGroup);
    			}
    			paymentGroupId = paymentGroup.getId();
    			if (pOrder != null && paymentGroup.getAmount() > TRUPaymentConstants.DOUBLE_INITIAL_VALUE) {
    				authorize(pOrder, paymentGroup);    				
    				PaymentStatus lastPaymentStatus = getLastAuthorizationStatus(paymentGroup);
    				if(lastPaymentStatus instanceof TRUPayPalStatus && !lastPaymentStatus.getTransactionSuccess()){
    					failedGroups.add(paymentGroup.getId());
    					break;
    				}
    				if(lastPaymentStatus instanceof TRUCreditCardStatus && !lastPaymentStatus.getTransactionSuccess()){
    					failedGroups.add(paymentGroup.getId());
    					break;
    				}
    				if(lastPaymentStatus instanceof TRUGiftCardStatus && !lastPaymentStatus.getTransactionSuccess()){
    					failedGroups.add(paymentGroup.getId());
    					break;
    				}
    			}
    		}
    	} catch (CommerceException e) {
    		failedGroups.add(paymentGroupId);
    		if (isLoggingDebug()) {
    			vlogDebug("CommerceException for the payment id - " + paymentGroupId + ", " + e);
    		}
    		if (isLoggingError()) {
				logError("CommerceException @Class:::TRUPaymentManager::@method::authorize()" ,e);
			}
    	}
    	if (isLoggingDebug()) {
    		vlogDebug("TRUPaymentManager.authorize() method , failedGroups:{0} ", failedGroups);
    		vlogDebug("TRUPaymentManager.authorize() method.END");
    	}
    	return failedGroups;
    }
	
    
	
	
	/**
	 * This method reverse authorizes the payment groups within the order and adds the payment
	 * group to the  failedGroups(List). 
	 * 
	 * @param pPaymentGroups - list of the PaymentGroups in the order.
	 * @param pOrder - Order.
	 * @return List - returns the list of authorization failed payment groups.
	 */    
	public List<String> reverseAuthorize(Order pOrder, List<PaymentGroup> pPaymentGroups){
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentManager.reverseAuthorize() method.STARTS");
			vlogDebug("TRUPaymentManager.reverseAuthorize() method , Order:{0} PaymentGroups:{1}", pOrder, pPaymentGroups);
		}
		List<String> failedGroups = null;
		if (pOrder != null && pPaymentGroups != null && !pPaymentGroups.isEmpty()) {
			failedGroups = new ArrayList<String>(pPaymentGroups.size());
			List<PaymentGroup> reorganizePaymentGroups = getReorganizePaymentGroups(pPaymentGroups);
			if (reorganizePaymentGroups != null && !reorganizePaymentGroups.isEmpty()) {
				for (PaymentGroup paymentGroup : reorganizePaymentGroups) {
					try {
						doReverseAuthorize(pOrder, paymentGroup);
					} catch (CommerceException commEx) {
						failedGroups.add(paymentGroup.getId());
						if(isLoggingError()){
							vlogError("Commerce Exception caught in TRUPaymentManager.reverseAuthorize(), OrderId:{0} profileId:{1} Exception:{2}", pOrder.getId(),pOrder.getProfileId(),commEx);
						}
					}
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("TRUPaymentManager.reverseAuthorize() method returning, failedGroups:{0}", failedGroups);
				vlogDebug("TRUPaymentManager.reverseAuthorize() method.END");
			}	
		}
		return failedGroups;
	}

	/**
	 * @param pOrder - order object
	 * @param pPaymentGroup -paymentGroup object
	 * @throws CommerceException -CommerceException 
	 */
	private void doReverseAuthorize(Order pOrder, PaymentGroup pPaymentGroup)
			throws CommerceException {
		double revarsalAmount = TRUConstants.DOUBLE_ZERO;
		if(pOrder != null){			
			if (pPaymentGroup instanceof TRUCreditCard) {
				TRUCreditCard creditCard = (TRUCreditCard) pPaymentGroup;
				revarsalAmount = creditCard.getReversalAmount();
			}if (pPaymentGroup instanceof TRUPayPal) {
				TRUPayPal payPal = (TRUPayPal) pPaymentGroup;
				revarsalAmount = payPal.getReversalAmount();
			}if (pPaymentGroup instanceof TRUGiftCard) {
				TRUGiftCard giftCard = (TRUGiftCard) pPaymentGroup;
				revarsalAmount = giftCard.getReversalAmount();
			}if (revarsalAmount > TRUPaymentConstants.DOUBLE_INITIAL_VALUE ) {
				reverseAuthorize(pOrder, pPaymentGroup);
			}
		}
	}
	
	/**
	 * This method is used to invoke the reverse authorize method with the
	 * payment group.
	 * 
	 * @param pPaymentGroup - the PaymentGroup in the order.
	 * @param pOrder - Order.
	 * @throws CommerceException - CommerceException.
	 */
	public void reverseAuthorize(Order pOrder, PaymentGroup pPaymentGroup) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentManager.reverseAuthorize() method.STARTS");
			vlogDebug("TRUPaymentManager.reverseAuthorize() method , Order:{0} PaymentGroup:{1}", pOrder,pPaymentGroup);
		}
		double revarsalAmount = TRUConstants.DOUBLE_ZERO;
		if(pOrder != null && pPaymentGroup != null){
			if (pPaymentGroup instanceof TRUCreditCard) {
				revarsalAmount = ((TRUCreditCard) pPaymentGroup).getReversalAmount();
			}
			if (pPaymentGroup instanceof TRUGiftCard) {
				revarsalAmount = ((TRUGiftCard) pPaymentGroup).getReversalAmount();
			}
			if (pPaymentGroup instanceof TRUPayPal) {
				revarsalAmount = ((TRUPayPal) pPaymentGroup).getReversalAmount();
			}
			reverseAuthorize(pOrder, pPaymentGroup, revarsalAmount);
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentManager.reverseAuthorize() method.END");
		}
	}
	
	/**
	 * This method is used to invoke the reverse authorization chain for
	 * the corresponding payment group.
	 * 
	 * @param pPaymentGroup - the PaymentGroup in the order.
	 * @param pOrder - Order.
	 * @param pAmount - Amount.
	 * @throws CommerceException - CommerceException.
	 */
	public void reverseAuthorize(Order pOrder, PaymentGroup pPaymentGroup, double pAmount) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentManager.reverseAuthorize() method.STARTS");
			vlogDebug("TRUPaymentManager.reverseAuthorize() method , Order:{0} Amount:{1} PaymentGroup:{1}", pOrder,pAmount,pPaymentGroup);
		}
		PaymentStatus status = null;
		
		if(pOrder == null){
			throw new InvalidParameterException(TRUPaymentConstants.INVALID_ORDER_PARAMETER_EXCEPTION);
		}
		if(pPaymentGroup == null){
			createErrorPaymentStatus(0, pPaymentGroup, pAmount, Constants.INVALID_PAYMENT_GROUP);
			throw new InvalidParameterException(Constants.INVALID_PAYMENT_GROUP);
		}
		if(pAmount <= TRUPaymentConstants.DOUBLE_INITIAL_VALUE){
			if(isLoggingError()){
				vlogError("TRUPaymentManager.reverseAuthorizeInvalid, Reversal amount :{0} for OrderId :{1} and PaymentGroupId :{2} ProfileId:{3}",pAmount,pOrder.getId(),pPaymentGroup.getId(),pOrder.getProfileId());
			}
			throw new InvalidParameterException(TRUPaymentConstants.INVALID_REVERSAL_AMOUNT);
		}
		try {
			preProcessReverseAuthorize(pPaymentGroup,pAmount);
			// Get the reverse authorization chain name.
			String chainName = getReverseAuthorizationChainName(pPaymentGroup);
			PaymentManagerPipelineArgs args = new PaymentManagerPipelineArgs();
			args.setOrder(pOrder);
			args.setPaymentManager(this);
			args.setPaymentGroup(pPaymentGroup);
			args.setAmount(pAmount);
			args.put(TRUPipeLineConstants.REVERSE_ACTION, Boolean.TRUE);
			runProcessorChain(chainName, args);
			status = args.getPaymentStatus();
			
			/*if(!status.getTransactionSuccess()){
				throw new CommerceException(TRUCheckoutConstants.VOID_TRANSACTION_FAILED);
			}*/
		} catch (CommerceException commEx) {
			if (isLoggingError()) {
				vlogError("Commerce Exception caught in @Class:::TRUPaymentManager::@method::reverseAuthorize() method, Order Id:{0} profileId:{1} Exception:{2}",pOrder.getId(),pOrder.getProfileId(),commEx);
			}
			 createErrorPaymentStatus(TRUConstants.ZERO, pPaymentGroup, pAmount, TRUCheckoutConstants.REVERSE_AUTHORIZE + commEx.getMessage());
			throw commEx;
		}
		postProcessReverseAuthorize(pPaymentGroup, status, pAmount);
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentManager.reverseAuthorize() method.END");
		}
	}
	
	/**
	 * 
	 * 
	 * @param pPaymentGroup - PaymentGroup
	 * @param pReversalAmount reversalAmount
	 * @throws CommerceException CommerceException
	 */
	private void preProcessReverseAuthorize(PaymentGroup pPaymentGroup,double pReversalAmount) throws CommerceException{
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentManager.preProcessReverseAuthorize() method.STARTS");
		}
		if(pReversalAmount > pPaymentGroup.getAmountAuthorized()){
			String msg = MessageFormat.format(TRUCheckoutConstants.ATTEMPT_TO_REVERSE_MORE_THAN_AUTHORIZE_AMT,new Object[] { Double.toString(pReversalAmount),
					pPaymentGroup.getId(), });
			createErrorPaymentStatus(0, pPaymentGroup, pReversalAmount, msg);
			throw new PaymentException(msg);
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentManager.preProcessReverseAuthorize() method.End");
		}
	}
	
	/**
	 * This method is used called after the handleProcessAuthorize.
	 *
	 *  @param pPaymentGroup - Payment Group
	 *  @param pStatus - status
	 *  @param pAmount - Amount
	 *  @throws CommerceException - CommerceException
	 */
	protected void postProcessReverseAuthorize(PaymentGroup pPaymentGroup, PaymentStatus pStatus, double pAmount)
			throws CommerceException{
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentManager.postProcessReverseAuthorize() method.STARTS");
			vlogDebug("PaymentGroup :{0} PaymentStatus :{1}",pPaymentGroup,pStatus);
		}
		if (pPaymentGroup != null && pStatus != null) {
			pPaymentGroup.addAuthorizationStatus(pStatus);
			if(isLoggingDebug()){
				vlogDebug("Reverse Authorized PaymentGroup:{0} transactionId:{1} transactionSuccess:{2} Error Message:{3} TransactionTimestamp:{4}"
					,pPaymentGroup.getId(),pStatus.getTransactionId(), pStatus.getTransactionSuccess(), pStatus.getErrorMessage() ,pStatus.getTransactionTimestamp());
				}
		if (pStatus instanceof TRUGiftCardStatus && pStatus.getTransactionSuccess() ) {
				double amountAuthorized = pPaymentGroup.getAmountAuthorized();
				if (isLoggingDebug()) {
					vlogDebug("TRUPaymentManager.postProcessReverseAuthorize()...TRUGiftCardStatus...amountAuthorized :{0} pAmount :{1}", amountAuthorized, pAmount);
				}
				amountAuthorized = getPricingTools().round(amountAuthorized - pAmount);
				pPaymentGroup.setAmountAuthorized(amountAuthorized);
				((TRUGiftCard)pPaymentGroup).setReversalAmount(TRUPaymentConstants.DOUBLE_INITIAL_VALUE);
				//int state = StateDefinitions.PAYMENTGROUPSTATES.getStateValue(TRUCheckoutConstants.STATE_INITIAL);
				pPaymentGroup.setState(0);
			}else if (pStatus instanceof TRUCreditCardStatus && pStatus.getTransactionSuccess()) {
				double amountAuthorized = pPaymentGroup.getAmountAuthorized();
				if (isLoggingDebug()) {
					vlogDebug("TRUPaymentManager.postProcessReverseAuthorize()...TRUCreditCardStatusImpl...amountAuthorized :{0} pAmount :{1}", amountAuthorized, pAmount);
				}
				amountAuthorized = getPricingTools().round(amountAuthorized - pAmount);
				pPaymentGroup.setAmountAuthorized(amountAuthorized);
				((TRUCreditCard)pPaymentGroup).setReversalAmount(TRUPaymentConstants.DOUBLE_INITIAL_VALUE);
				//int state = StateDefinitions.PAYMENTGROUPSTATES.getStateValue(TRUCheckoutConstants.STATE_INITIAL);
				pPaymentGroup.setState(0);
				
			}else if (pStatus instanceof TRUPayPalStatus && pStatus.getTransactionSuccess()) {
				double amountAuthorized = pPaymentGroup.getAmountAuthorized();
				if (isLoggingDebug()) {
					vlogDebug("TRUPaymentManager.postProcessReverseAuthorize()...TRUPayPalStatus...amountAuthorized :{0} pAmount :{1}", amountAuthorized, pAmount);
				}
				amountAuthorized = getPricingTools().round(amountAuthorized - pAmount);
				pPaymentGroup.setAmountAuthorized(amountAuthorized);
				((TRUPayPal)pPaymentGroup).setReversalAmount(TRUPaymentConstants.DOUBLE_INITIAL_VALUE);
				//int state = StateDefinitions.PAYMENTGROUPSTATES.getStateValue(TRUCheckoutConstants.STATE_INITIAL);
				pPaymentGroup.setState(0);
				
			}else {
				if (isLoggingError()) {
				vlogError("ReverseAuthorization transaction failed :  @Class:::TRUPaymentManager::@method::postProcessReverseAuthorize() PaymentGroup :{0} PaymentStatus :{1}",pPaymentGroup,pStatus);
				}
				throw new PaymentException(MessageFormat.format(TRUCheckoutConstants.REV_AUTH_FAILURE, new Object[] { pPaymentGroup.getId(), pStatus.getErrorMessage() }));
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentManager.postProcessAuthorize() method.END");
		}
	}

	
	/**
	 * This method is used to get the reverse authorization name for a given
	 * payment group.
	 * 
	 * @param pPaymentGroup - PaymentGroup.
	 * @return String - Chain name of the payment group.
	 * @throws CommerceException - CommerceException.
	 */
	public String getReverseAuthorizationChainName(PaymentGroup pPaymentGroup) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentManager.getReverseAuthorizationChainName() method.STARTS");
			vlogDebug("TRUPaymentManager.getReverseAuthorizationChainName() method , PaymentGroup:{0}", pPaymentGroup);
		}
		if (pPaymentGroup == null) {
			throw new CommerceException(Constants.MISSING_PAYMENT_GROUP);
		}
		String paymentlaGroupClassName = pPaymentGroup.getClass().getName();
		String chainName = (String)getReversePaymentGroupToChainNameMap().get(paymentlaGroupClassName);
		if (chainName == null) {
			if (isLoggingDebug()) {
				vlogDebug("TRUPaymentManager.getReverseAuthorizationChainName() method.END with unknown payment group type exception being thrown");
			}
			throw new CommerceException(Constants.UNKNOWN_PAYMENT_GROUP_TYPE);
		} else {
			if (isLoggingDebug()) {
				vlogDebug("TRUPaymentManager.getReverseAuthorizationChainName() method , chainName:{0}", chainName);
				vlogDebug("TRUPaymentManager.getReverseAuthorizationChainName() method.END");
			}
			return chainName;
		}
	}
	
	/**
	 * This method is used to reorganize the payments such that while authorizing the order,
	 * validation will be in the specific order.
	 * The order of payment validation is as follows:
	 *  a. KCC.
	 *  b. giftCard.
	 *  c. credit card.
	 *  @param pPaymentGroups - Payment Groups
	 *  @return PaymentGroupList -  reorganized payment groups
	 *
	 */
	public List<PaymentGroup> getReorganizePaymentGroups(List<PaymentGroup> pPaymentGroups) {
		if(isLoggingDebug()){
			vlogDebug("TRUPaymentManager.getReorganizePaymentGroups() method.STARTS");
			vlogDebug("TRUPaymentManager.getReorganizePaymentGroups() method,pPaymentGroups:{0} ",pPaymentGroups);
		}
		List<PaymentGroup> reorganizePaymentGroups = null;
		if (pPaymentGroups != null) {
			List<PaymentGroup> paymentGroups = pPaymentGroups;
			reorganizePaymentGroups = new ArrayList<PaymentGroup>(paymentGroups.size());
			List<PaymentGroup> giftCardPayment = new ArrayList<PaymentGroup>();
			List<PaymentGroup> creditCardPayment = new ArrayList<PaymentGroup>();
			if (paymentGroups != null && paymentGroups.size() > TRUConstants.INTEGER_NUMBER_ONE) {
				for (PaymentGroup paymentGroup : pPaymentGroups) {
					if (isLoggingDebug()) {
						vlogDebug("TRUPaymentManager.getReorganizePaymentGroups() method,paymentGroup:{0} ",paymentGroup);
					}
					 if (paymentGroup instanceof TRUGiftCard) {
						giftCardPayment.add(paymentGroup);
					} else if (paymentGroup instanceof TRUCreditCard || paymentGroup instanceof TRUPayPal ) {
						creditCardPayment.add(paymentGroup);
					}
				}
				if (!giftCardPayment.isEmpty()) {
					reorganizePaymentGroups.addAll(giftCardPayment);
				}
				if (!creditCardPayment.isEmpty()) {
					reorganizePaymentGroups.addAll(creditCardPayment);
				}
			} else {
				reorganizePaymentGroups = paymentGroups;
			}
		}
		if(isLoggingDebug()){
			vlogDebug("TRUPaymentManager.getReorganizePaymentGroups() method,orderedPaymentGroups:{0} ", reorganizePaymentGroups);
			vlogDebug("TRUPaymentManager.getReorganizePaymentGroups() method.END");
		}
		return reorganizePaymentGroups;
	}
	
	/**
	 * This method is used to return the latest tender transaction PaymentStatus object.
	 * @param pPaymentStatusList paymentStatusList
	 * @return paymentStatus
	 */	
	public PaymentStatus getLatestPayStatusObject(List<PaymentStatus> pPaymentStatusList) {
		List<PaymentStatus> filteredList = new ArrayList<PaymentStatus>();
		if(pPaymentStatusList != null && pPaymentStatusList.size() >TRUConstants.INTEGER_NUMBER_ONE){
			Collections.sort(pPaymentStatusList,new Comparator<PaymentStatus>() {
			@Override
			public int compare(PaymentStatus pO1,PaymentStatus pO2) {
				return pO1.getTransactionTimestamp().compareTo(pO2.getTransactionTimestamp());
				}
			});
			filteredList.addAll(pPaymentStatusList);
			return filteredList.get(filteredList.size() - TRUConstants.INTEGER_NUMBER_ONE);
		}
		return null;
	}
	/**
	 * @return the reversePaymentGroupToChainNameMap
	 */
	public Properties getReversePaymentGroupToChainNameMap() {
		return mReversePaymentGroupToChainNameMap;
	}
	/**
	 * @param pReversePaymentGroupToChainNameMap the reversePaymentGroupToChainNameMap to set
	 */
	public void setReversePaymentGroupToChainNameMap(
			Properties pReversePaymentGroupToChainNameMap) {
		this.mReversePaymentGroupToChainNameMap = pReversePaymentGroupToChainNameMap;
	}
	

	/**
	 * Gets the mPayPalProcessor.
	 * @return the mPayPalProcessor
	 */
	public TRUPayPalProcessor getPayPalProcessor() {
		return mPayPalProcessor;
	}

	/**
	 *Sets  the mPayPalProcessor.
	 * @param pPayPalProcessor the mPaypalProcessor to set
	 */
	public void setPayPalProcessor(TRUPayPalProcessor pPayPalProcessor) {
		mPayPalProcessor = pPayPalProcessor;
	}
	
	/**
	 * @return the paymentTools
	 */
	public TRUPaymentTools getPaymentTools() {
		return mPaymentTools;
	}

	/**
	 * @param pPaymentTools the paymentTools to set
	 */
	public void setPaymentTools(TRUPaymentTools pPaymentTools) {
		mPaymentTools = pPaymentTools;
	}
	
	/**
	 * This method is used to clear the Gift Card info.
	 * 
	 * @param pGiftCardInfo
	 *            The Gift Card Info Object
	 * @return TRUGiftCardStatus - TRUGiftCardStatus
	 */
	public TRUGiftCardStatus getGiftCardBalance(TRUGiftCardInfo pGiftCardInfo)  {
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentManager.getGiftCardBalance() method.STARTS");
			vlogDebug("TRUPaymentManager.reverseAuthorize() method , GiftCardInfo:{0}", pGiftCardInfo);
		}
		TRUGiftCardStatus gcStatus = null;
		if (pGiftCardInfo != null) {			
			try {
				gcStatus = getPaymentTools().getGiftCardbalance(pGiftCardInfo);
			} catch (TRURadialIntegrationException paymentExp) {
				vlogError("PaymentIntegrationException at TRUPaymentManager/getGiftCardBalance method :{0}",
						paymentExp);
				gcStatus = new TRUGiftCardStatus();
				gcStatus.setTransactionSuccess(false);
				gcStatus.setErrorMessage(TRUErrorKeys.TRU_ERROR_TRY_AGAIN_AFTER_SOME_TIME);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentManager.getGiftCardBalance() method.END");
		}
		return gcStatus;
	}


	/**
	 * @return the mGiftCardPaymentProcessor
	 */
	public TRUGiftCardProcessor getGiftCardPaymentProcessor() {
		return mGiftCardPaymentProcessor;
	}


	/**
	 * @param pGiftCardPaymentProcessor the pGiftCardPaymentProcessor to set
	 */
	public void setGiftCardPaymentProcessor(
			TRUGiftCardProcessor pGiftCardPaymentProcessor) {
		this.mGiftCardPaymentProcessor = pGiftCardPaymentProcessor;
	}


}
