package com.tru.commerce.order;
/**
 * This class.
 * @author Professional Access
 * @version 1.0
 */
public class TRUPaymentServiceConstants {
	
	
	/**
	 * Constant holds the ERR_UNABLE_TO_PROCESS.
	 */
	public static final String ERR_UNABLE_TO_PROCESS = "ERR_UNABLE_TO_PROCESS";
	
	/**
	 * Constant holds the ERR_UNABLE_TO_PROCESS.
	 */
	public static final String ERR_SERVER_EXCEPTION = "ERR_SERVER_EXCEPTION";

}
