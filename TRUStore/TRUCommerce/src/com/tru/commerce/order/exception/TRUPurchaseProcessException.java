package com.tru.commerce.order.exception;

/**
 * The custom exception which will be used at all PayPal operations.
 * @author PA
 * @version 1.0
 */

public class TRUPurchaseProcessException extends Exception {

	static final long serialVersionUID = 0L;
	/**
	 * Overridden Constructor.
	 */
	public TRUPurchaseProcessException() {
		super();
	}
	/**
	 * @param pMessage the message
	 * @param pNestedException the nestedException
	 */
	public TRUPurchaseProcessException(String pMessage, Throwable pNestedException) {
		super(pMessage, pNestedException);
	}
	/**
	 * @param pMessage the message
	 */
	public TRUPurchaseProcessException(String pMessage) {
		super(pMessage);
	}
	/**
	 * @param pNestedException the nestedException
	 */
	public TRUPurchaseProcessException(Throwable pNestedException) {
		super(pNestedException);
	}

}