package com.tru.commerce.order;

import atg.commerce.order.PaymentGroupImpl;

/**
 * This class is an implementation of Gift card  payment information.
 * It represents all the information which designates a gift card payment.
 * @author PA
 * @version 1.0
 */
public class TRUGiftCard extends PaymentGroupImpl  {
	
	 /**
    * property TRANSACTION_ID.
    */
    private static final String PROP_NAME_TRANSACTION_ID = "transactionId";
	/**
	 * Used to hold serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
    /**
     * property reversedAmount.
     */
    private static final String REVERSED_AMOUNT = "reversedAmount";
	/**
	 * Constant  to hold double zero.
	 */
	private static final double DOUBLE_ZERO = 0.0;

	
	 /**
 	 * Gets the transaction id.
 	 *
 	 * @return the Transaction Id.
 	 */
    public String getTransactionId() {
        return (String)getPropertyValue(PROP_NAME_TRANSACTION_ID);
    }

    /**
     * Sets the transaction id.
     *
     * @param pTransactionId the cardHolderName to set
     */
    public void setTransactionId(String pTransactionId) {
        setPropertyValue(PROP_NAME_TRANSACTION_ID, pTransactionId);
    }
    
	/**
	 * This is the method to get the value of the property giftCardNumber.
	 *
	 * @return Returns the collectionId.
	 */
	
	public String getGiftCardNumber() {
		return (String)getPropertyValue(TRUPropertyNameConstants.GIFT_CARD_NUMBER);
	}
	/**
	 * This method sets the value of the property giftCardNumber. 
	 *
	 * @param pGiftCardNumber The collectionId to set.
	 */
	public void setGiftCardNumber(String pGiftCardNumber) {
		this.setPropertyValue(TRUPropertyNameConstants.GIFT_CARD_NUMBER, pGiftCardNumber);
	} 
	/**
	 * This is the method to get the value of the property giftCardPin.
	 * @return Returns the giftCardPin.
	 */
	public String getGiftCardPin() {
		return (String) getPropertyValue(TRUPropertyNameConstants.GIFT_CARD_PIN);
	}
	/**
	 * This method sets the value of the property giftCardPin. 
	 * @param pGiftCardPin The giftCardPin to set.
	 */
	public void setGiftCardPin(String pGiftCardPin) {
		this.setPropertyValue(TRUPropertyNameConstants.GIFT_CARD_PIN, pGiftCardPin);
	} 
	
	
	/**
	 * This is the method to get the value of the property currentBalance.
	 * @return Returns the currentBalance.
	 */
	public double getCurrentBalance() {
		Double currentBalance = (Double)getPropertyValue(TRUPropertyNameConstants.CURRENT_BALANCE);
	    return (currentBalance == null) ? DOUBLE_ZERO : currentBalance.doubleValue();
	}

	/**
	 * This method sets the value of the property current balance. 
	 * @param pCurrentBalance to set.
	 */
	public void setCurrentBalance(double pCurrentBalance) {
		if (getCurrentBalance() != pCurrentBalance)
		{
		      setPropertyValue(TRUPropertyNameConstants.CURRENT_BALANCE, new Double(pCurrentBalance));
		}
	} 
	
	/**
     * @return the reversalAmount
     */
    public double getReversalAmount() {
        final Object loRevrsalAmount = (Object) getPropertyValue(REVERSED_AMOUNT);
        if (loRevrsalAmount != null) {
            return ((Double)loRevrsalAmount).doubleValue();
        } else {
            return DOUBLE_ZERO;
        }
    }    
    /**
     * @param pReversalAmount the reversalAmount to set
     */
    public void setReversalAmount(double pReversalAmount) {
        setPropertyValue(REVERSED_AMOUNT, pReversalAmount);
    }
    
    /**
	 * This is the method to get the value of the property remainingBalance.
	 * @return Returns the remainingBalance.
	 */
	public Double getRemainingBalance() {
		final Double remainingBalance = (Double)getPropertyValue(TRUPropertyNameConstants.REMAINING_BALANCE);
	    return (remainingBalance == null) ? DOUBLE_ZERO : remainingBalance.doubleValue();
	}
	
	/**
	 * This method sets the value of the property remainingBalance. 
	 * @param pRemainingBalance The remainingBalance to set.
	 */
	public void setRemainingBalance(Double pRemainingBalance) {
		if (getRemainingBalance() != pRemainingBalance)
		{
		      setPropertyValue(TRUPropertyNameConstants.REMAINING_BALANCE, new Double(pRemainingBalance));
		}
	}
    

}