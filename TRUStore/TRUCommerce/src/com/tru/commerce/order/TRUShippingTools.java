package com.tru.commerce.order;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import atg.nucleus.GenericService;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;

import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.common.TRUConstants;

/**
 * This class is TRUShippingTools.
 *
 * @author PA
 * @version 1.0
 */
public class TRUShippingTools extends GenericService{

	
	/** mShippingRepository. */
	private Repository mShippingRepository;
	
	/** "freightClass" item name. */
	private String mFreightClassItemName;
	
	/** "shipMethodRegionRate" item name. */
	private String mShipMethodRegionRateItemName;
	
	/** mPropertyManager. */
	private TRUCommercePropertyManager mPropertyManager;
	
	/** "zipCodeRestrictions" item name. */
	private String mZipCodeRestrictionsItemName;
	
	/** The m ship method item name. */
	private String mShipMethodItemName;
	
	
	/** The m holiday list item name. */
	private String mHolidayListItemName;
	
	
	/** The m us time zone. */
	private String mUSTimeZone;
	
	
	
	
	/**
	 * Gets the shipping repository.
	 *
	 * @return shippingRepository
	 */
	public Repository getShippingRepository() {
		return mShippingRepository;
	}
	
	/**
	 * Sets the shipping repository.
	 *
	 * @param pShippingRepository the shippingRepository to set
	 */
	public void setShippingRepository(Repository pShippingRepository) {
		this.mShippingRepository = pShippingRepository;
	}
	
	/**
	 * Gets the freight class item name.
	 *
	 * @return freightClassItemName
	 */
	public String getFreightClassItemName() {
		return mFreightClassItemName;
	}
	
	/**
	 * Sets the freight class item name.
	 *
	 * @param pFreightClassItemName the freightClassItemName to set
	 */
	public void setFreightClassItemName(String pFreightClassItemName) {
		this.mFreightClassItemName = pFreightClassItemName;
	}
	
	
	/**
	 * Gets the ship method region rate item name.
	 *
	 * @return shipMethodRegionRateItemName
	 */
	public String getShipMethodRegionRateItemName() {
		return mShipMethodRegionRateItemName;
	}
	
	/**
	 * Sets the ship method region rate item name.
	 *
	 * @param pShipMethodRegionRateItemName the shipMethodRegionRateItemName to set
	 */
	public void setShipMethodRegionRateItemName(String pShipMethodRegionRateItemName) {
		this.mShipMethodRegionRateItemName = pShipMethodRegionRateItemName;
	}
	
	/**
	 * Gets the zip code restrictions item name.
	 *
	 * @return zipCodeRestrictionsItemName
	 */
	public String getZipCodeRestrictionsItemName() {
		return mZipCodeRestrictionsItemName;
	}
	
	/**
	 * Sets the zip code restrictions item name.
	 *
	 * @param pZipCodeRestrictionsItemName the zipCodeRestrictionsItemName to set
	 */
	public void setZipCodeRestrictionsItemName(String pZipCodeRestrictionsItemName) {
		this.mZipCodeRestrictionsItemName = pZipCodeRestrictionsItemName;
	}
	
	/**
	 * getter for propertyManager.
	 *
	 * @return propertyManager
	 */
	public TRUCommercePropertyManager getPropertyManager() {
		return mPropertyManager;
	}
	
	/**
	 * propertyManager.
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUCommercePropertyManager pPropertyManager) {
		this.mPropertyManager = pPropertyManager;
	}
	
	/**
	 * Gets the uS time zone.
	 *
	 * @return uSTimeZone
	 */
	public String getUSTimeZone() {
		return mUSTimeZone;
	}
	
	/**
	 * setter for USTimeZone.
	 *
	 * @param pUSTimeZone the USTimeZone to set
	 */
	public void setUSTimeZone(String pUSTimeZone) {
		mUSTimeZone = pUSTimeZone;
	}
		
	

	


	/**
	 * Gets the ship method item name.
	 *
	 * @return shipMethodItemName
	 */
	public String getShipMethodItemName() {
		return mShipMethodItemName;
	}
	
	/**
	 * Sets the ship method item name.
	 *
	 * @param pShipMethodItemName the shipMethodItemName to set
	 */
	public void setShipMethodItemName(String pShipMethodItemName) {
		this.mShipMethodItemName = pShipMethodItemName;
	}
	
	/**
	 * Gets the holiday list item name.
	 *
	 * @return holidayListItemName
	 */
	public String getHolidayListItemName() {
		return mHolidayListItemName;
	}
	
	/**
	 * Sets the holiday list item name.
	 *
	 * @param pHolidayListItemName the holidayListItemName to set
	 */
	public void setHolidayListItemName(String pHolidayListItemName) {
		this.mHolidayListItemName = pHolidayListItemName;
	}
	/**
	 * <p>
	 * 	It will get all the active shipmethods where key is the freightClass and value will be the list of applicable ship methods. 
	 * <p>
	 * @param pFreightClassesSet freightClassesSet
	 * @return items
	 */
	public RepositoryItem[] getShipMethodsByFreighs(final Set<String> pFreightClassesSet){
		if(isLoggingDebug()){
			vlogDebug("Start TRUShippingTools.getShipMethodsByFreighs()");
		}
		RepositoryItem[] items=null;
		
		try {
			final RepositoryView view = getShippingRepository().getView(getShipMethodItemName());
			final QueryBuilder b = view.getQueryBuilder();			
			final QueryExpression propertyExp=b.createPropertyQueryExpression(getPropertyManager().getEligibleFreightClassesPropertyName() );				
			final QueryExpression theValueExpression = 
			    b.createConstantQueryExpression(pFreightClassesSet);
			final Query theQuery = 
			    b.createIncludesAnyQuery(theValueExpression, propertyExp);						
			items= view.executeQuery(theQuery);
			if(items!=null && items.length>TRUConstants.ZERO){
				for(int i=0;i<items.length;i++){
					if(isLoggingDebug()){
						vlogDebug("Ship Method Item \t::"+items[i]);
					}
				}
				
			}
		} catch (RepositoryException e) {
			if(isLoggingError()){
				logError("Error : @Class::TRUShippingTools::@method::getShipMethodsByFreighs() ",e);
			}
		}finally{
			if(isLoggingDebug()){
				vlogDebug("Resulted Items \t:"+items);
				vlogDebug("End TRUShippingTools.getShipMethodsByFreighs()");
			}
		}		
		return items;
	}
	
	/**
	 * This Method is to query the repository to get the active holidays list.
	 * @return RepositoryItem[] - Returns the items returned by querying the holidayList item descriptor
	 * @throws RepositoryException - Encountered while doing repository query
	 */
	public RepositoryItem[] getHolidayList() throws RepositoryException{
		if(isLoggingDebug()){
			vlogDebug("TRUShippingTools.getHolidayList() method.STARTS");
		}
		RepositoryItem[] items = null;
		final RepositoryView view = getShippingRepository().getView(getHolidayListItemName());
		final QueryBuilder builder= view.getQueryBuilder();
		
		final QueryExpression isActivePropertyExpr = builder.createPropertyQueryExpression(getPropertyManager().getIsActivePropertyName());
		
		final QueryExpression isActiveValueExpr = builder.createConstantQueryExpression(Boolean.TRUE);
		final Query compareQuery = builder.createComparisonQuery(isActivePropertyExpr, isActiveValueExpr,
				QueryBuilder.EQUALS);				
		items = view.executeQuery(compareQuery);		
		if(isLoggingDebug()){
			if(items != null && items.length > 0){
				 vlogDebug("TRUShippingTools.getHolidayList() method ,items:{0}",(Object)items);
			}else {
				vlogDebug("Returned items is null or empty in TRUShippingTools.getHolidayList() method");
			}
			vlogDebug("TRUShippingTools.getHolidayList() method.END");
		}
		return items;
	}
	
	
	/**
	 * Gets the ship method details.
	 *
	 * @param pShipMethodCode the ship method code
	 * @return the ship method details
	 */
	public RepositoryItem getShipMethodDetails(String pShipMethodCode) {
		if (isLoggingDebug()) {
			vlogDebug("Start TRUShippingTools.getShipMethodItemByCode()");
		}
		RepositoryItem[] items = null;
		RepositoryItem shipMethodItem = null;
		try {
			RepositoryView view = getShippingRepository().getView(getShipMethodItemName());
			final QueryBuilder builder = view.getQueryBuilder();
			final QueryExpression shipMethodPropertyExpr = builder.createPropertyQueryExpression(getPropertyManager()
					.getShipMethodCodePropertyName());
			final QueryExpression shipMethodValueExpr = builder.createConstantQueryExpression(pShipMethodCode);
			final Query compareQuery = builder.createComparisonQuery(shipMethodPropertyExpr, shipMethodValueExpr,
					QueryBuilder.EQUALS);
			items = view.executeQuery(compareQuery);
			if (items != null && items.length > 0) {
				shipMethodItem = items[0];
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("Error : TRUShippingTools.getShipRatesByRegionCode() ", e);
			}
			return shipMethodItem;
		} finally {
			if (isLoggingDebug()) {
				vlogDebug("Resulted ShipMethodRegionRateItems \t:" + items);
				vlogDebug("End TRUShippingTools.getShipMethodItemByCode()");
			}
		}
		return shipMethodItem;
	}
	
	
	
	/**
	 * Gets the non qualified regions by ship method.
	 *
	 * @param pShipMethodCode the ship method code
	 * @return the non qualified regions by ship method
	 */
	@SuppressWarnings("unchecked")
	public Set<String> getNonQualifiedRegionsByShipMethod(String pShipMethodCode) {
		final RepositoryItem shipMethodItem = getShipMethodDetails(pShipMethodCode);
		if (shipMethodItem != null) {
			return (Set<String>) shipMethodItem.getPropertyValue(getPropertyManager().getNonQualifiedStateCodesPropertyName());
		}
		return null;
	}
	
	
	/**
	 * <p>
	 * This method is used to get the holiday List.
	 * </p>
	 * @param pStartDate - the startDate
	 * @param pEndaDate - the item
	 * @throws RepositoryException - repositoryException 
	 * @return items - RepositoryItem[]
	 */
	public RepositoryItem[] getHolidayInBtnEDDRange(Date pStartDate,Date pEndaDate) throws RepositoryException{
		if(isLoggingDebug()){
			vlogDebug("TRUShippingTools.getHolidayList() method.STARTS");
		}
		
		RepositoryItem[] items = null;
		final RepositoryView view = getShippingRepository().getView(getHolidayListItemName());
		final QueryBuilder queryBuilder= view.getQueryBuilder();
		QueryExpression expression1=	queryBuilder.createPropertyQueryExpression(getPropertyManager().getHolidayDatePropertyName());
		QueryExpression startDateExpression=	queryBuilder.createConstantQueryExpression(pStartDate);
		QueryExpression expression2=	queryBuilder.createPropertyQueryExpression(getPropertyManager().getHolidayDatePropertyName());
		QueryExpression endDateExpression=	queryBuilder.createConstantQueryExpression(pEndaDate);
		final Query query1 = queryBuilder.createComparisonQuery(expression1, startDateExpression, QueryBuilder.GREATER_THAN_OR_EQUALS);
		final Query query2 = queryBuilder.createComparisonQuery(expression2, endDateExpression, QueryBuilder.LESS_THAN_OR_EQUALS);
		
		Query[] pieces = { query1, query2 };
		
		 Query andQuery = queryBuilder.createAndQuery(pieces);
		 
		items = view.executeQuery(andQuery);
		
		if(isLoggingDebug()){
			if(items != null && items.length > 0){
				 vlogDebug("TRUShippingTools.getHolidayList() method ,items:{0}",(Object)items);
			}else {
				vlogDebug("Returned items is null or empty in TRUShippingTools.getHolidayList() method");
			}
			vlogDebug("TRUShippingTools.getHolidayList() method.END");
		}
		return items;
	}

	/**
	 * Check zip code restriction.
	 *
	 * @param pSkuId the sku id
	 * @param pZipCode the zip code
	 * @return the string
	 */
	public String checkSKUZipCodeRestriction(String pSkuId, String pZipCode) {
		if (isLoggingDebug()) {
			vlogDebug("Start TRUShippingTools.checkSKUZipCodeRestriction() for sku {0} and pZipCode{1}" , pSkuId , pZipCode);
		}
		String errorMsg = TRUConstants.EMPTY;
		try {
			final Repository repository = getShippingRepository();
			final RepositoryView view = repository.getView(getZipCodeRestrictionsItemName());
			final QueryBuilder b = view.getQueryBuilder();

			final QueryExpression skuIdpropertyExp = b.createPropertyQueryExpression(getPropertyManager().getSkuIdPropertyName());
			final QueryExpression skuIdValueExp = b.createConstantQueryExpression(pSkuId.trim());

			final QueryExpression zipCodepropertyExp = b.createPropertyQueryExpression(getPropertyManager().getRestrictedZipCodesPropertyName());
			Set<String> zipCodeSet=new HashSet<String>(TRUConstants._1);
			zipCodeSet.add(pZipCode.trim());
			final QueryExpression zipCodeValueExp = b.createConstantQueryExpression(zipCodeSet);			
			final Query skuIdQuery_1 = b.createComparisonQuery(skuIdpropertyExp, skuIdValueExp, QueryBuilder.EQUALS);						
			final Query zipCodeQuery_2 = b.createIncludesAnyQuery(zipCodeValueExp,zipCodepropertyExp);

			final Query query = b.createAndQuery(new Query[] { skuIdQuery_1, zipCodeQuery_2 });

			final RepositoryItem[] items = view.executeQuery(query);
			if (items != null && items.length >= TRUConstants.SIZE_ONE) {
				errorMsg = (String) items[0].getPropertyValue(getPropertyManager().getErrorMessagePropertyName());
			}

		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("Error at TRUShippingTools.checkSKUZipCodeRestriction() and RepositoryException", e);
			}
		} finally {
			if (isLoggingDebug()) {
				vlogDebug("End TRUShippingTools.checkSKUZipCodeRestriction() for sku {0} and pZipCode{1}" , pSkuId , pZipCode);
			}
		}
		return errorMsg;
	}
	
	/**
     * This method is to get the common list of shipMethods for the unique FreightClass set passed as input in case of Apple pay
     * @param pFreightClassesSet -pFreightClassesSet 
      * @return shipMethodArray - array of common ship methods for a set of freight classes 
      */
     public RepositoryItem[] getShipMethodsByFreights(
                   final Set<String> pFreightClassesSet) {
            if (isLoggingDebug()) {
                   vlogDebug("Start TRUShippingTools.getShipMethodsByFreighs()");
            }
            RepositoryItem[] items = null;
            Iterator<String> iterator = pFreightClassesSet.iterator();
            ArrayList<RepositoryItem[]> shipMethodsList = new ArrayList<RepositoryItem[]>();
            ArrayList<RepositoryItem> firstShipMethodList = new ArrayList<RepositoryItem>();
            RepositoryItem[] shipMethodArray = null;
            try {
                   while (iterator.hasNext()) {
                         final RepositoryView view = getShippingRepository().getView(getShipMethodItemName());
                         final QueryBuilder b = view.getQueryBuilder();
                         final QueryExpression propertyExp = b.createPropertyQueryExpression(getPropertyManager().getEligibleFreightClassesPropertyName());
                         final QueryExpression theValueExpression = b.createConstantQueryExpression(iterator.next());
                         final Query theQuery = b.createComparisonQuery(propertyExp,
                                       theValueExpression, QueryBuilder.EQUALS);
                         items = view.executeQuery(theQuery);

                         if (items != null && items.length > TRUConstants.ZERO) {
                                for (int i = 0; i < items.length; i++) {
                                       if (isLoggingDebug()) {
                                              vlogDebug("Ship Method Item \t::" + items[i]);
                                       }
                                }

                         }
                         shipMethodsList.add(items);
                   }

                   if (shipMethodsList != null && !shipMethodsList.isEmpty()) {
                         RepositoryItem[] firstShipMethodArray = shipMethodsList.get(0);
                         firstShipMethodList.addAll(Arrays.asList(firstShipMethodArray));
                         for (RepositoryItem[] repoItem : shipMethodsList) {
                                firstShipMethodList.retainAll(Arrays.asList(repoItem));
                         }
                         if (firstShipMethodList != null && !firstShipMethodList.isEmpty()) {
                                shipMethodArray = new RepositoryItem[firstShipMethodList.size()];
                                for (int i = 0; i < firstShipMethodList.size(); i++) {

                                       shipMethodArray[i] = firstShipMethodList.get(i);
                                }
                         }

                         if (isLoggingDebug()) {
                                logDebug("Common ship method list is ::::"
                                              + firstShipMethodList);
                         }
                   }

            } catch (RepositoryException e) {
                   if (isLoggingError()) {
                         logError(
                                       "Error : @Class::TRUShippingTools::@method::getShipMethodsByFreighs() ",
                                       e);
                   }
            } finally {
                   if (isLoggingDebug()) {
                         vlogDebug("Resulted Items \t:" + items);
                         vlogDebug("End TRUShippingTools.getShipMethodsByFreighs()");
                   }
            }
            return shipMethodArray;
     }
}
