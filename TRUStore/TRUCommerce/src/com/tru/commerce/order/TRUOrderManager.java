/*
 * 
 */
package com.tru.commerce.order;

import java.beans.IntrospectionException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import javax.transaction.TransactionManager;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.claimable.ClaimableManager;
import atg.commerce.inventory.InventoryException;
import atg.commerce.order.ChangedProperties;
import atg.commerce.order.CommerceIdentifier;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.CreditCard;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InStorePayment;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupOrderRelationship;
import atg.commerce.order.PipelineConstants;
import atg.commerce.order.Relationship;
import atg.commerce.order.RelationshipNotFoundException;
import atg.commerce.order.RelationshipTypes;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.SimpleOrderManager;
import atg.commerce.order.purchase.PaymentGroupMapContainer;
import atg.commerce.order.purchase.ShippingGroupMapContainer;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.PricingTools;
import atg.commerce.promotion.PromotionTools;
import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.ResourceUtils;
import atg.core.util.StringUtils;
import atg.multisite.SiteContextManager;
import atg.nucleus.naming.ComponentName;
import atg.payment.PaymentStatus;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.pipeline.PipelineResult;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import atg.userprofiling.address.AddressTools;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.inventory.TRUCoherenceInventoryManager;
import com.tru.commerce.inventory.TRUInventoryConstants;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardStatus;
import com.tru.commerce.order.purchase.TRUApplePayForm;
import com.tru.commerce.order.purchase.TRUPaymentGroupContainerService;
import com.tru.commerce.order.purchase.TRUPurchaseProcessHelper;
import com.tru.commerce.order.purchase.TRUShippingGroupContainerService;
import com.tru.commerce.order.purchase.TRUShippingProcessHelper;
import com.tru.commerce.order.vo.TRUStorePickUpInfo;
import com.tru.commerce.payment.TRUPaymentConstants;
import com.tru.commerce.payment.TRUPaymentManager;
import com.tru.commerce.payment.applepay.TRUApplePay;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.commerce.pricing.TRUOrderPriceInfo;
import com.tru.commerce.pricing.TRUShipMethodVO;
import com.tru.commerce.pricing.TRUShippingPricingEngine;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUUserSession;
import com.tru.integrations.exception.TRUIntegrationException;
import com.tru.integrations.synchrony.bean.SynchronySDPRequestBean;
import com.tru.integrations.synchrony.bean.SynchronySDPResponseBean;
import com.tru.integrations.synchrony.service.Synchrony;
import com.tru.radial.commerce.payment.paypal.TRUPaypalConstants;
import com.tru.radial.commerce.payment.paypal.util.TRUPayPalConstants;
import com.tru.radial.integration.util.TRURadialConstants;
import com.tru.shipping.registry.TRURegistryServiceProcessor;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * The Class TRUOrderManager will take of all order related
 * payment and order merge and repository operations.
 * 
 *  @author PA
 *  @version 1.0
 */
public class TRUOrderManager extends SimpleOrderManager {

	
	/** The Constant LAST_NAME. */
	private static final String LAST_NAME = "LASTNAME";

	/** The Constant FIRST_NAME. */
	private static final String FIRST_NAME = "FIRSTNAME";

	/** The Constant TRU_SESSION_SCOPE_COMPONENT_NAME. */
	public final static ComponentName TRU_SESSION_SCOPE_COMPONENT_NAME = ComponentName.getComponentName(TRUCommerceConstants.TRU_SESSION_SCOPE_COMPONENT_NAME);
	
	/**
	 * Property to hold OrderResources.
	 */
	static final String MY_RESOURCE_NAME = "atg.commerce.order.OrderResources";
	
	/**
	 * Property to hold ClaimableManager.
	 */
	private ClaimableManager mClaimableManager;

	/**
	 * Property to hosld PromotionTools.
	 */
	private PromotionTools mPromotionTools;

	/** Member variable to hold propery manager. */
	private TRUPropertyManager mPropertyManager;

	/** property to hold mSiteRepository. */
	private Repository mSiteRepository;
	
	/** Constant for SPLIT_SYMBOL_COMA. */
	public static final String SPLIT_SYMBOL_COMA = ",";

	/**
	 * property to hold mSynchrony.
	 */
	private Synchrony mSynchrony;

	/** Property to hold the Payment Manager. */
	private TRUPaymentManager mPaymentManager;

	/**
	 * property for adjustPaymentGroupsChainId pipeline Chain Id.
	 */
	private String mAdjustPaymentGroupsChainId;
	
	/**
	 * property for mUpdateLayawayOrderChainId pipeline Chain Id.
	 */
	private String mUpdateLayawayOrderChainId;

	/**
	 * Variable to hold the failed Reverse Authorize create PropertyNames.
	 */
	private List<String> mFailedReverseAuthCreatePropertyName;

	/**
	 * Variable to hold the failed Reverse Authorize update PropertyNames.
	 */
	private List<String> mFailedReverseAuthPropertyName;
	
	/**
	 * property for configuration.
	 */
	private TRUConfiguration mConfiguration;
	
	/** Property To mCoherenceInventoryManager . */
	private TRUCoherenceInventoryManager mCoherenceInventoryManager; 
	
	/**
	 * property to hold shipping process helper instance
	 */
	private TRUShippingProcessHelper mShippingHelper;
	
	/** holds TRURegistryServiceProcessor instance **/
	private TRURegistryServiceProcessor mRegistryServiceProcessor;
	/**
	 * Reference to the Transaction Manager.
	 */
	private TransactionManager mTransactionManager;

	private Repository mMessageContentRepository;
	
	/** The m purchase process helper. */
	private TRUPurchaseProcessHelper mPurchaseProcessHelper;
	
	private String mOrderIdToLoad;
	
	/**
	 * @return the orderIdToLoad
	 */
	public String getOrderIdToLoad() {
		return mOrderIdToLoad;
	}

	/**
	 * @param pOrderIdToLoad the orderIdToLoad to set
	 */
	public void setOrderIdToLoad(String pOrderIdToLoad) {
		mOrderIdToLoad = pOrderIdToLoad;
	}

	/**
	 * @return the purchaseProcessHelper
	 */
	public TRUPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}

	/**
	 * @param pPurchaseProcessHelper the purchaseProcessHelper to set
	 */
	public void setPurchaseProcessHelper(
			TRUPurchaseProcessHelper pPurchaseProcessHelper) {
		mPurchaseProcessHelper = pPurchaseProcessHelper;
	}

	/**
	 * Gets the failedReverseAuthPropertyName.
	 * 
	 * @return the mFailedReverseAuthPropertyName
	 */
	public List<String> getFailedReverseAuthPropertyName() {
		return mFailedReverseAuthPropertyName;
	}

	/**
	 * Sets the failedReverseAuthPropertyName.
	 * 
	 * @param pFailedReverseAuthPropertyName the mFailedReverseAuthPropertyName to set
	 */
	public void setFailedReverseAuthPropertyName(
			List<String> pFailedReverseAuthPropertyName) {
		mFailedReverseAuthPropertyName = pFailedReverseAuthPropertyName;
	}

	/**
	 * Gets the coherence inventory manager.
	 *
	 * @return the coherence inventory manager
	 */
	public TRUCoherenceInventoryManager getCoherenceInventoryManager() {
		return mCoherenceInventoryManager;
	}

	/**
	 * Sets the coherence inventory manager.
	 *
	 * @param pCoherenceInventoryManager the CoherenceInventoryManager to set
	 */
	public void setCoherenceInventoryManager(
			TRUCoherenceInventoryManager pCoherenceInventoryManager) {
		mCoherenceInventoryManager = pCoherenceInventoryManager;
	} 
	/**
	 * Gets the site repository.
	 *
	 * @return the mSiteRepository
	 */

	/**
	 * @return the shippingHelper
	 */
	public TRUShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * @param pShippingHelper the shippingHelper to set
	 */
	public void setShippingHelper(TRUShippingProcessHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}

	/**
	 * @return the paymentManager
	 */
	public TRUPaymentManager getPaymentManager() {
		return mPaymentManager;
	}

	/**
	 * @param pPaymentManager the paymentManager to set
	 */
	public void setPaymentManager(TRUPaymentManager pPaymentManager) {
		mPaymentManager = pPaymentManager;
	}

	/**
	 * @return the adjustPaymentGroupsChainId
	 */
	public String getAdjustPaymentGroupsChainId() {
		return mAdjustPaymentGroupsChainId;
	}
	
	/**
	 * Sets the adjust payment groups chain id.
	 *
	 * @param pAdjustPaymentGroupsChainId the new adjust payment groups chain id
	 */
	public void setAdjustPaymentGroupsChainId(String pAdjustPaymentGroupsChainId) {
		this.mAdjustPaymentGroupsChainId = pAdjustPaymentGroupsChainId;
	}
	
	/**
	 * @return the mUpdateLayawayOrderChainId
	 */
	public String getUpdateLayawayOrderChainId() {
		return mUpdateLayawayOrderChainId;
	}

	/**
	 * Sets the update layaway order chain id.
	 *
	 * @param pUpdateLayawayOrderChainId the new adjust payment groups chain id
	 */
	public void setUpdateLayawayOrderChainId(String pUpdateLayawayOrderChainId) {
		this.mUpdateLayawayOrderChainId = pUpdateLayawayOrderChainId;
	}

	/**
	 * Gets the failedReverseAuthCreatePropertyName
	 * 
	 * @return the mFailedReverseAuthCreatePropertyName
	 */
	public List<String> getFailedReverseAuthCreatePropertyName() {
		return mFailedReverseAuthCreatePropertyName;
	}

	/**
	 * Sets the mFailedReverseAuthCreatePropertyName
	 * 
	 * @param pFailedReverseAuthCreatePropertyName the mFailedReverseAuthCreatePropertyName to set
	 */
	public void setFailedReverseAuthCreatePropertyName(
			List<String> pFailedReverseAuthCreatePropertyName) {
		mFailedReverseAuthCreatePropertyName = pFailedReverseAuthCreatePropertyName;
	}

	/**
	 * @return the registryServiceProcessor
	 */
	public TRURegistryServiceProcessor getRegistryServiceProcessor() {
		return mRegistryServiceProcessor;
	}

	/**
	 * @param pRegistryServiceProcessor the registryServiceProcessor to set
	 */
	public void setRegistryServiceProcessor(
			TRURegistryServiceProcessor pRegistryServiceProcessor) {
		mRegistryServiceProcessor = pRegistryServiceProcessor;
	}
	
	/**
	 * @return the configuration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * @param pConfiguration the configuration to set
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		mConfiguration = pConfiguration;
	}

	/**
	 * mergeOrders method is overridden and set RemoveSourceOrder as false Assigning the relationships for unassigned quantity in
	 * the commerce items.
	 * 
	 * @param pSrcOrder
	 *            the src order
	 * @param pDestOrder
	 *            the dest order
	 * @throws CommerceException
	 *             the commerce exception
	 */
	@SuppressWarnings("unchecked")
	public void mergeOrders(Order pSrcOrder, Order pDestOrder) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::mergeOrders() : START");
		}
		pDestOrder.getTransientData().put(TRUCheckoutConstants.IS_MERGE_ORDER, true);
		clearAnyGiftOptions(pSrcOrder);
		long preDestTotalItemCount = ((TRUOrderImpl)pDestOrder).getTotalItemCount();
		String currentTab = ((TRUOrderImpl)pSrcOrder).getCurrentTab();				
		//CR CHANGES FOR SUCN COUPON CODE
		// Calling method to remove old single used coupon before merging with new one.
		Map<String, String> singleUseCoupon = removeOldSingleUsedCoupons(pDestOrder);
		Map<String, String> notAppliedCoupon = removeOldNotAppliedCoupons(pDestOrder);
		if(singleUseCoupon != null){
			singleUseCoupon.putAll(((TRUOrderImpl) pSrcOrder).getSingleUseCoupon());
			((TRUOrderImpl) pDestOrder).setSingleUseCoupon(singleUseCoupon);
		}else{
			((TRUOrderImpl) pDestOrder).setSingleUseCoupon(((TRUOrderImpl) pSrcOrder).getSingleUseCoupon());
		}
		if(notAppliedCoupon != null){
			notAppliedCoupon.putAll(((TRUOrderImpl) pSrcOrder).getNotAppliedCouponCode());
			((TRUOrderImpl) pDestOrder).setNotAppliedCouponCode(notAppliedCoupon);
		}else{
			((TRUOrderImpl) pDestOrder).setNotAppliedCouponCode(((TRUOrderImpl) pSrcOrder).getNotAppliedCouponCode());
		}
		// RemoveSourceOrder flag set as false in the last parameter in below method.
		super.mergeOrders(pSrcOrder, pDestOrder, isCreateSGRelationshipsOnMerge(), Boolean.FALSE);
		Long sgCiQty = TRUConstants.LONG_ZERO;
		List<CommerceItem> commerceItemsList = pDestOrder.getCommerceItems();
		if (null != commerceItemsList && !commerceItemsList.isEmpty()) {
			for (CommerceItem destCommerceItem : commerceItemsList) {
				if (null != destCommerceItem) {
					List<ShippingGroupCommerceItemRelationship> destCiSgRelationShips = destCommerceItem
							.getShippingGroupRelationships();
					sgCiQty = TRUConstants.LONG_ZERO;
					// Summing the quantities in relationships objects of commerce item.
					if (null != destCiSgRelationShips && !destCiSgRelationShips.isEmpty()) {
						for (ShippingGroupCommerceItemRelationship shippingGroupCommerceItemRelationship : destCiSgRelationShips) {
							sgCiQty = sgCiQty + shippingGroupCommerceItemRelationship.getQuantity();
						}
					}
					// if commerce item quantity is greater than sum of the quantity of relationship objects then some
					// quantity in commerce item is not assigned to shipping group.
					if (destCommerceItem.getQuantity() > sgCiQty) {
						addMissedItemQuantityToShippingGroup(pSrcOrder, pDestOrder, destCommerceItem);
					}
				}
			}
		}
		copyBPPItemsFromSrcToDestOrder(pSrcOrder, pDestOrder);
		autoCorrectQuantity(pDestOrder, false);
		setDonationQuantity(pDestOrder);
		if(!pSrcOrder.isTransient()){
			removeOrder(pSrcOrder.getId());
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::mergeOrders() : removing source order");
		}
		synchronized (pDestOrder) {
			//To show Merge cart overlay
			if (preDestTotalItemCount > TRUConstants.ZERO){
				pDestOrder.getTransientData().put(TRUCheckoutConstants.SHOW_MERGE_CART_OVERLAY, true);
			}
			boolean isSrcOrderHasOnlyISPUItems = false;
			if (StringUtils.isNotBlank(currentTab) && TRUCheckoutConstants.PICKUP_TAB.equalsIgnoreCase(currentTab)) {
				isSrcOrderHasOnlyISPUItems = getShippingHelper().getPurchaseProcessHelper().isOrderHavingOnlyInstorePickupItems(pDestOrder);
			}
			//To redirect to shipping tab from pickup tab
			if (!isSrcOrderHasOnlyISPUItems && ((TRUOrderImpl)pDestOrder).getShipToHomeItemsCount() > TRUConstants.ZERO) {
				pDestOrder.getTransientData().put(TRUCheckoutConstants.REDIRECT_TO_SHIPPING, true);
			}
			getShippingHelper().updateShippingMethodAndPrice(pDestOrder);
			updateOrder(pDestOrder);
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::mergeOrders() : END");
		}
	}
	/**
	 * Copy bpp items from src to dest order.
	 *
	 * @param pSrcOrder the src order
	 * @param pDestOrder the dest order
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void copyBPPItemsFromSrcToDestOrder(Order pSrcOrder, Order pDestOrder) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderManager::@method::copyBPPItemsFromSrcToDestOrder() : START");
		}
		List srcCIList = pSrcOrder.getCommerceItems();
		List destCIList = pDestOrder.getCommerceItems();
		int i = TRUCommerceConstants.INT_ZERO;
		List<String> bppSKUIdsList = new ArrayList<String>();
		List<CommerceItem> bppCIsList = new ArrayList<CommerceItem>();
		for (int ciListSize = srcCIList.size(); i < ciListSize; ++i) {
			CommerceItem srcCi = (CommerceItem) srcCIList.get(i);
			List<TRUShippingGroupCommerceItemRelationship> sgclirList = srcCi.getShippingGroupRelationships();
			for (TRUShippingGroupCommerceItemRelationship sgcir : sgclirList) {
				TRUBppItemInfo bppItemInfo = sgcir.getBppItemInfo();
				if (bppItemInfo != null && bppItemInfo.getRepositoryItem() != null) {
					bppSKUIdsList.add(srcCi.getCatalogRefId());
					bppCIsList.add(srcCi);
					break;
				}
			}
		}
		int j = TRUCommerceConstants.INT_ZERO;
		for (int ciListSize = destCIList.size(); j < ciListSize; ++j) {
			CommerceItem destCi = (CommerceItem) destCIList.get(j);
			if(bppSKUIdsList != null && !bppSKUIdsList.isEmpty() && bppSKUIdsList.contains(destCi.getCatalogRefId()) ){
				for (CommerceItem srcCi : bppCIsList) {
					if(srcCi.getCatalogRefId().equalsIgnoreCase(destCi.getCatalogRefId())){
						
						List<TRUShippingGroupCommerceItemRelationship> srcsgclirList = srcCi.getShippingGroupRelationships();
						List<TRUShippingGroupCommerceItemRelationship> destsgclirList = destCi.getShippingGroupRelationships();
						Iterator<TRUShippingGroupCommerceItemRelationship> itr1 = srcsgclirList.iterator();
						Iterator<TRUShippingGroupCommerceItemRelationship> itr2 = destsgclirList.iterator();
						
						while (itr1.hasNext()) {
							TRUShippingGroupCommerceItemRelationship srcsgcir = itr1.next();
							while (itr2.hasNext()) {
								TRUShippingGroupCommerceItemRelationship destsgcir = itr2.next();
								ShippingGroup srcShippingGroup = srcsgcir.getShippingGroup();
								ShippingGroup destShippingGroup = destsgcir.getShippingGroup();
								try {
									if(srcShippingGroup.getShippingGroupClassType().equals(destShippingGroup.getShippingGroupClassType()) ){
										if (srcShippingGroup instanceof TRUChannelHardgoodShippingGroup
												&& destShippingGroup instanceof TRUChannelHardgoodShippingGroup
												&& ((TRUChannelHardgoodShippingGroup) srcShippingGroup).getChannelId() != null &&((TRUChannelHardgoodShippingGroup) srcShippingGroup).getChannelId().equals(
														((TRUChannelHardgoodShippingGroup) destShippingGroup).getChannelId())) {
												((TRUOrderTools) getOrderTools()).addBppItemToOrder(pDestOrder,destsgcir.getId(),srcsgcir.getBppItemInfo().getBppSkuId(),srcsgcir.getBppItemInfo().getBppItemId());
										}else if(srcShippingGroup instanceof TRUHardgoodShippingGroup && destShippingGroup instanceof TRUHardgoodShippingGroup){
											((TRUOrderTools) getOrderTools()).addBppItemToOrder(pDestOrder,destsgcir.getId(),srcsgcir.getBppItemInfo().getBppSkuId(),srcsgcir.getBppItemInfo().getBppItemId());
										}else if(srcShippingGroup instanceof TRUChannelInStorePickupShippingGroup && destShippingGroup instanceof TRUChannelInStorePickupShippingGroup
												&& ((TRUChannelInStorePickupShippingGroup) srcShippingGroup).getLocationId() != null && ((TRUChannelInStorePickupShippingGroup) srcShippingGroup).getLocationId().equals(
														((TRUChannelInStorePickupShippingGroup) destShippingGroup).getLocationId()) && ((TRUChannelInStorePickupShippingGroup) srcShippingGroup).getChannelId() != null &&
														((TRUChannelInStorePickupShippingGroup) srcShippingGroup).getChannelId().equals(((TRUChannelInStorePickupShippingGroup) destShippingGroup).getChannelId())){
											((TRUOrderTools) getOrderTools()).addBppItemToOrder(pDestOrder,destsgcir.getId(),srcsgcir.getBppItemInfo().getBppSkuId(),srcsgcir.getBppItemInfo().getBppItemId());
										}else if(srcShippingGroup instanceof TRUInStorePickupShippingGroup && destShippingGroup instanceof TRUInStorePickupShippingGroup
												&& ((TRUInStorePickupShippingGroup) srcShippingGroup).getLocationId() != null && ((TRUInStorePickupShippingGroup) srcShippingGroup).getLocationId().equals(
														((TRUInStorePickupShippingGroup) destShippingGroup).getLocationId())){
											((TRUOrderTools) getOrderTools()).addBppItemToOrder(pDestOrder,destsgcir.getId(),srcsgcir.getBppItemInfo().getBppSkuId(),srcsgcir.getBppItemInfo().getBppItemId());
										}
									}
								} catch (RelationshipNotFoundException rnfe) {
									if(isLoggingError()){
										logError("RelationshipNotFoundException in handleAddUpdateBppItem : ", rnfe);
									}
								} catch (InvalidParameterException ipe) {
									if(isLoggingError()){
										logError("InvalidParameterException in handleAddUpdateBppItem : ", ipe);
									}
								} catch (RepositoryException re) {
									if(isLoggingError()){
										logError("RepositoryException in handleAddUpdateBppItem : ", re);
									}
								} 
							}
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderManager::@method::copyBPPItemsFromSrcToDestOrder() : END");
		}
	}
	/**
	 * compareHardgoodShippingGroups method is overridden .
	 *
	 * @param pSrcShippingGroup the src shipping group
	 * @param pDestShippingGroup the dest shipping group
	 * @return true, if successful
	 */
	
	public boolean compareHardgoodShippingGroups(ShippingGroup pSrcShippingGroup, ShippingGroup pDestShippingGroup)
	  {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::compareHardgoodShippingGroups() : START");
		}
		boolean found = true;
		if (!(pSrcShippingGroup.getShippingGroupClassType().equalsIgnoreCase(pDestShippingGroup.getShippingGroupClassType()))) {
			found = false;
		} else if (pSrcShippingGroup instanceof TRUChannelHardgoodShippingGroup
				&& pDestShippingGroup instanceof TRUChannelHardgoodShippingGroup
				&& ((TRUChannelHardgoodShippingGroup) pSrcShippingGroup).getChannelId() != null &&(!((TRUChannelHardgoodShippingGroup) pSrcShippingGroup).getChannelId().equals(
						((TRUChannelHardgoodShippingGroup) pDestShippingGroup).getChannelId()))) {
				found = false;
		}else{
			if(isEnableTransientShippingInfo()){
				found = true;
			}else{
				found = super.compareHardgoodShippingGroups(pSrcShippingGroup, pDestShippingGroup);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::compareHardgoodShippingGroups() : END");
		}
		return found;
	  }
	
	/**
	 * This method is used to update the coherence inventory cache after placing
	 * order.
	 *
	 * @param pOrder the order
	 * @param pQtyAdjustedSkuList 
	 * @return the int
	 * @throws InventoryException the inventory exception
	 */
	public int updateInventory(Order pOrder, Map<String, String> pQtyAdjustedSkuList) throws InventoryException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::UpdateInventory() : START");
		}
		List<CommerceItem> listCommerceItems = null;
		String locationId = null;
		int status=TRUCommerceConstants.INT_ZERO; 
		long quantity = TRUCommerceConstants.INT_ZERO;
		InStorePickupShippingGroup srcItemIsPkSg = null;
		List<ShippingGroupCommerceItemRelationship> relItems = null;
		listCommerceItems = pOrder.getCommerceItems();
		for (CommerceItem commItem : listCommerceItems) {
			relItems = commItem.getShippingGroupRelationships();
			if (!(commItem instanceof TRUGiftWrapCommerceItem) && !(commItem instanceof TRUDonationCommerceItem)) {
				for (ShippingGroupCommerceItemRelationship relationItem : relItems) {
					quantity = relationItem.getQuantity();
					ShippingGroup sg = relationItem.getShippingGroup();
					if (sg instanceof TRUHardgoodShippingGroup || sg instanceof TRUChannelHardgoodShippingGroup) {
						status=getCoherenceInventoryManager().purchase(
								commItem.getCatalogRefId(), quantity);
					} else if (sg instanceof TRUInStorePickupShippingGroup || sg instanceof TRUChannelInStorePickupShippingGroup) {
						srcItemIsPkSg = (InStorePickupShippingGroup) relationItem
								.getShippingGroup();
						locationId = srcItemIsPkSg.getLocationId();
						if(((TRUShippingGroupCommerceItemRelationship)relationItem).isWarehousePickup()){
							locationId=((TRUInStorePickupShippingGroup)srcItemIsPkSg).getWarhouseLocationCode();
						}
						status=getCoherenceInventoryManager().purchase(
								commItem.getCatalogRefId(), quantity,
								locationId);
						vlogDebug("@Class::TRUOrderManager::@method::UpdateInventory(), catalogRefId:{0} and status: {1}", commItem.getCatalogRefId(), status);
					}
					if (status != TRUInventoryConstants.INVENTORY_STATUS_SUCCEED) {
						pQtyAdjustedSkuList.put(commItem.getCatalogRefId(), TRUCommerceConstants.TRU_SHOPPINGCART_AUTOCORRECT_WITH_INVENTORY);
					}
				}
			} else{
				status=TRUCommerceConstants.INT_ONE;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::UpdateInventory() : END");
		}
		return status;
	}

	/**
	 * Adds the missed item quantity to shipping group.
	 * 
	 * @param pSrcOrder
	 *            the src order
	 * @param pDestOrder
	 *            the dest order
	 * @param pDestCommerceItem
	 *            the dest commerce item
	 * @throws CommerceException
	 *             the commerce exception
	 */
	private void addMissedItemQuantityToShippingGroup(Order pSrcOrder, Order pDestOrder, CommerceItem pDestCommerceItem)
			throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::addMissedItemQuantityToShippingGroup() : START");
		}
		// finding the destination commerce item in source order.
		CommerceItem matchedItem = findMatchingItem(pSrcOrder, pDestCommerceItem);
		if (null != matchedItem) {
			// Constructing a map with loactionId as key and quantity as parameter.
			Map shipGpQtymap = findmatchedSgCiRelSp(matchedItem, pDestCommerceItem);
			if (!shipGpQtymap.isEmpty()) {
				Iterator entries = shipGpQtymap.entrySet().iterator();
				while (entries.hasNext()) {
					Map.Entry entry = (Map.Entry) entries.next();
					String locationID = (String) entry.getKey();
					Long quantity = (Long) entry.getValue();
					// get the InstorePickup ShippingGroup for that locationId.
					ShippingGroup matchedShGrp = findIspuShGrpInOrder(locationID, pDestOrder);
					if (null != matchedShGrp) {
						// Assigning relationship with shipping group.
						addItemQuantityToShippingGroup(pDestOrder, pDestCommerceItem.getId(), matchedShGrp.getId(), quantity);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::addMissedItemQuantityToShippingGroup() : END");
		}
	}

	/**
	 * Get the remaining Amount from the order.
	 * 
	 * @param pOrder
	 *            -TRUOrderImpl object
	 * @return orderReminingAmount - double value
	 */
	public double getOrderRemaningAmount(TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.getOrderRemaningAmount() method.STARTS");
		}
		double orderReminingAmount = TRUCheckoutConstants.DOUBLE_ZERO;
		if (pOrder != null) {
			TRUOrderImpl order = pOrder;
			PricingTools pricingTools = getOrderTools().getProfileTools().getPricingTools();
			double orderTotal = TRUPaymentConstants.DOUBLE_ZERO_VALUE;
			if (order.getPriceInfo() != null) {
				orderTotal = order.getPriceInfo().getTotal();
				if (isLoggingDebug()) {
					vlogDebug("TRUOrderManager (getOrderRemaningAmount), orderTotal:{0}", orderTotal);
				}
			}

			double usedAmount = getPaymentGroupsTotalAppliedAmount(pOrder);

			if (isLoggingDebug()) {
				vlogDebug("TRUOrderManager (getOrderRemaningAmount), usedAmount:{0}", usedAmount);
			}
			if (orderTotal >= usedAmount) {
				orderReminingAmount = pricingTools.round(orderTotal - usedAmount);
			}
			if (isLoggingDebug()) {
				vlogDebug("TRUOrderManager (getOrderRemaningAmount), orderReminingAmount:{0}", orderReminingAmount);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.getOrderRemaningAmount() method.END");
		}
		return orderReminingAmount;
	}

	/**
	 * This method is used to get payment groups applied amount.
	 * 
	 * @param pOrder
	 *            - Order
	 * @return double - amount.
	 */
	@SuppressWarnings("unchecked")
	public double getPaymentGroupsTotalAppliedAmount(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.getPaymentGroupsTotalAppliedAmount() method.START");
		}
		double totalAmtApplied = TRUCheckoutConstants.DOUBLE_ZERO;
		if (pOrder != null && pOrder.getPaymentGroupRelationshipCount() > TRUCheckoutConstants.DOUBLE_ZERO) {
			double amountAppliedOnPG = TRUCheckoutConstants.DOUBLE_ZERO;
			List<PaymentGroup> groups = pOrder.getPaymentGroups();
			if (groups != null) {
				for (final PaymentGroup paymentGroup : groups) {
					if (isLoggingDebug()) {
						vlogDebug("TRUOrderManager (getOrderRemaningAmount), pgOrderRel:{0},paymentGroup:{1}", paymentGroup,
								paymentGroup);
					}
					if (paymentGroup != null && TRUCheckoutConstants.GIFTCARD.equalsIgnoreCase(paymentGroup.getPaymentGroupClassType())) {
						if (paymentGroup instanceof TRUGiftCard) {
							amountAppliedOnPG = ((TRUGiftCard) paymentGroup).getAmount();
						}
						totalAmtApplied += amountAppliedOnPG;
						if (isLoggingDebug()) {
							vlogDebug("Current Store Credit PaymentGroup  Amount: {0}", totalAmtApplied);
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.getPaymentGroupsTotalAppliedAmount() method.END");
		}
		return totalAmtApplied;
	}

	/**
	 * Sets the donation quantity.
	 * 
	 * @param pDestOrder
	 *            the new donation quantity
	 */
	public void setDonationQuantity(Order pDestOrder) {
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.setDonationQuantity() method.START");
		}
		List<CommerceItem> destCommerceItemsList = pDestOrder.getCommerceItems();
		if (destCommerceItemsList != null && !destCommerceItemsList.isEmpty()) {
			for (CommerceItem destCommerceItem : destCommerceItemsList) {
				if (destCommerceItem instanceof TRUDonationCommerceItem) {
					List<ShippingGroupCommerceItemRelationship> destCiSgRelationShips = destCommerceItem
							.getShippingGroupRelationships();
					if (destCiSgRelationShips != null && !destCiSgRelationShips.isEmpty()) {
						for (ShippingGroupCommerceItemRelationship sgcir : destCiSgRelationShips) {
							try {
								getCommerceItemManager().removeItemQuantityFromShippingGroup(pDestOrder,
										destCommerceItem.getId(), sgcir.getShippingGroup().getId(), sgcir.getQuantity());
								getCommerceItemManager().addItemQuantityToShippingGroup(pDestOrder, destCommerceItem.getId(),
										sgcir.getShippingGroup().getId(), TRUCheckoutConstants.INT_ONE);
								destCommerceItem.setQuantity(TRUCheckoutConstants.INT_ONE);
							} catch (CommerceException commerceException) {
								if (isLoggingError()) {
									logError("CommerceException in @Class::TRUOrderManager:::@method::setDonationQuantity()", commerceException);
								}
							}

						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.setDonationQuantity() method.END");
		}
	}

	/**
	 * This method is used to find the matching destination item in the source order.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pItem
	 *            the item
	 * @return the commerce item
	 * @throws CommerceException
	 *             the commerce exception
	 */
	public CommerceItem findMatchingItem(Order pOrder, CommerceItem pItem) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::findMatchingItem() : START");
		}
		CommerceItem commerceItem = getCommerceItemManager().findMatchingItem(pOrder, pItem);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::findMatchingItem() : END");
		}
		return commerceItem;
	}

	/**
	 * findmatchedSgCiRelSp is used to identify the missing relation ships in source and destination commerce items.
	 * 
	 * @param pSrcItem
	 *            the src item
	 * @param pDestItem
	 *            the dest item
	 * @return the map
	 */
	public Map findmatchedSgCiRelSp(CommerceItem pSrcItem, CommerceItem pDestItem) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::findmatchedSgCiRelSp() : START");
		}
		InStorePickupShippingGroup srcItemIsPkSg = null;
		InStorePickupShippingGroup destItemIsPkSg = null;
		List<ShippingGroupCommerceItemRelationship> srcItemSgRelShipsList = pSrcItem.getShippingGroupRelationships();
		List<ShippingGroupCommerceItemRelationship> destItemSgRelShipsList = pDestItem.getShippingGroupRelationships();
		Map<String, Long> shipGpQtymap = new HashMap<String, Long>();

		if (destItemSgRelShipsList.isEmpty()) {
			for (ShippingGroupCommerceItemRelationship srcItemSgRelShip : srcItemSgRelShipsList) {
				if (null != srcItemSgRelShip && srcItemSgRelShip.getShippingGroup() instanceof TRUChannelInStorePickupShippingGroup) {
					srcItemIsPkSg = (InStorePickupShippingGroup) srcItemSgRelShip.getShippingGroup();
					shipGpQtymap.put(srcItemIsPkSg.getLocationId(), srcItemSgRelShip.getQuantity());

				}
				else if (null != srcItemSgRelShip && srcItemSgRelShip.getShippingGroup() instanceof TRUInStorePickupShippingGroup) {
					srcItemIsPkSg = (InStorePickupShippingGroup) srcItemSgRelShip.getShippingGroup();
					shipGpQtymap.put(srcItemIsPkSg.getLocationId(), srcItemSgRelShip.getQuantity());

				}
			}
		} else if (null != srcItemSgRelShipsList && null != destItemSgRelShipsList && !srcItemSgRelShipsList.isEmpty()) {
			for (ShippingGroupCommerceItemRelationship srcItemSgRelShip : srcItemSgRelShipsList) {
				if (null != srcItemSgRelShip && srcItemSgRelShip.getShippingGroup() instanceof TRUChannelInStorePickupShippingGroup) {
					srcItemIsPkSg = (InStorePickupShippingGroup) srcItemSgRelShip.getShippingGroup();
					for (ShippingGroupCommerceItemRelationship destItemSgRelShip : destItemSgRelShipsList) {
						if (null != destItemSgRelShip
								&& destItemSgRelShip.getShippingGroup() instanceof TRUChannelInStorePickupShippingGroup) {
							destItemIsPkSg = (InStorePickupShippingGroup) destItemSgRelShip.getShippingGroup();
							if (srcItemIsPkSg.getLocationId().equals(destItemIsPkSg.getLocationId())) {
								shipGpQtymap.put(srcItemIsPkSg.getLocationId(), srcItemSgRelShip.getQuantity());
							}
						}
					}

				}
				else if (null != srcItemSgRelShip && srcItemSgRelShip.getShippingGroup() instanceof TRUInStorePickupShippingGroup) {
					srcItemIsPkSg = (InStorePickupShippingGroup) srcItemSgRelShip.getShippingGroup();
					for (ShippingGroupCommerceItemRelationship destItemSgRelShip : destItemSgRelShipsList) {
						if (null != destItemSgRelShip && destItemSgRelShip.getShippingGroup() instanceof TRUInStorePickupShippingGroup) {
							destItemIsPkSg = (InStorePickupShippingGroup) destItemSgRelShip.getShippingGroup();
							if (srcItemIsPkSg.getLocationId().equals(destItemIsPkSg.getLocationId())) {
								shipGpQtymap.put(srcItemIsPkSg.getLocationId(), srcItemSgRelShip.getQuantity());
							}
						}
					}

				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::findmatchedSgCiRelSp() : END");
		}
		return shipGpQtymap;
	}

	/**
	 * findIspuShGrpInOrder is used to get the InstorPickup Shipping group in the order based on location id.
	 * 
	 * @param pLocationId
	 *            the location id
	 * @param pOrder
	 *            the order
	 * @return the shipping group
	 */
	@SuppressWarnings("unchecked")
	public ShippingGroup findIspuShGrpInOrder(String pLocationId, Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::findIspuShGrpInOrder() : START");
		}
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		if (null != shippingGroups && !shippingGroups.isEmpty()) {
			for (ShippingGroup shippingGroup : shippingGroups) {
				if (shippingGroup instanceof TRUInStorePickupShippingGroup && 
				pLocationId.equals(((TRUInStorePickupShippingGroup) shippingGroup).getLocationId())) {
					if (isLoggingDebug()) {
						vlogDebug("@Class::TRUOrderManager::@method::findIspuShGrpInOrder() : END");
					}
					return shippingGroup;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::findIspuShGrpInOrder() : END");
		}
		return null;
	}

	/**
	 * returns order credit card.
	 * Order will hold only one credit card all the time.
	 * 
	 * @param pOrder - Holds the order
	 * @return CreditCard - Holds the credit card
	 */
	@SuppressWarnings("unchecked")
	public CreditCard getCreditCard(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getCreditCard() : START");
		}
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		for (PaymentGroup pg : paymentGroups) {
			if (pg instanceof CreditCard) {
				return (CreditCard) pg;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getCreditCard() : END");
		}
		return null;
	}

	/**
	 * Ensures credit card payment group in order.
	 * 
	 * @param pOrder - Holds the order
	 * @param pPaymentGroupMapContainer - paymentGroupMapContainer
	 * @return containerCreditCard the CreditCard
	 */
	@SuppressWarnings("unchecked")
	public CreditCard getDefaultCreditCard(Order pOrder, PaymentGroupMapContainer pPaymentGroupMapContainer) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::ensureDefaultCreditCard() : START");
		}

		TRUCreditCard containerCreditCard = null;
		String defaultCreditCardName = pPaymentGroupMapContainer.getDefaultPaymentGroupName();
		if (StringUtils.isNotBlank(defaultCreditCardName)) {
			containerCreditCard = (TRUCreditCard) pPaymentGroupMapContainer.getPaymentGroup(defaultCreditCardName);
			return containerCreditCard;
		} else {
			// if there is no default credit card, get first card and update to order
			Map<String, PaymentGroup> paymentGroups = pPaymentGroupMapContainer.getPaymentGroupMap();
			if (paymentGroups != null && !paymentGroups.isEmpty()) {
				Set<String> creditCardNames = paymentGroups.keySet();
				for (String creditCardName : creditCardNames) {
					if (paymentGroups.get(creditCardName) instanceof TRUCreditCard)
					{
						containerCreditCard = (TRUCreditCard) paymentGroups.get(creditCardName);
						if (containerCreditCard != null) {
							pPaymentGroupMapContainer.setDefaultPaymentGroupName(creditCardName);
							return containerCreditCard;
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::ensureDefaultCreditCard() : END");
		}
		return null;
	}
	
	/**
	 * This method returns current order's credit card nick name.
	 * 
	 * @param pCreditCard - TRUCreditCard
	 * @param pPaymentGroupMapContainer - paymentGroupMapContainer
	 * @return nickName - String
	 */
	@SuppressWarnings("unchecked")
	public String getOrderCreditCardNickName(TRUCreditCard pCreditCard, PaymentGroupMapContainer pPaymentGroupMapContainer) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getOrderCreditCardNickName() : START");
		}
		String nickName = null;
		TRUCreditCard containerCreditCard = null;
		Map<String, PaymentGroup> paymentGroups = pPaymentGroupMapContainer.getPaymentGroupMap();
		if (paymentGroups != null && !paymentGroups.isEmpty()) {
			Set<String> creditCardNames = paymentGroups.keySet();
			for (String creditCardName : creditCardNames) {
				if (paymentGroups.get(creditCardName) instanceof TRUCreditCard)
				{
					containerCreditCard = (TRUCreditCard) paymentGroups.get(creditCardName);
					if (containerCreditCard.getId() == pCreditCard.getId()) {
						nickName = creditCardName;
						break;
					}
				}
			}
		}
		
		return nickName;
	}

	/**
	 * Ensures in-store payment group in order.
	 * 
	 * @param pOrder - Holds the order
	 * @throws CommerceException 
	 */
	@SuppressWarnings("unchecked")
	public void ensureInStorePayment(Order pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::ensureInStorePayment() : START");
		}

		List<PaymentGroup> exclusionPaymentGroups = new ArrayList<PaymentGroup>();

		boolean ensureInStorePayment = Boolean.FALSE;
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		for (PaymentGroup paymentGroup : paymentGroups) {
			if (paymentGroup instanceof InStorePayment) {
				ensureInStorePayment = true;
				exclusionPaymentGroups.add(paymentGroup);
			}
		}
		getPaymentGroupManager().removeAllPaymentGroupsFromOrder(pOrder, exclusionPaymentGroups);

		if (!ensureInStorePayment) {
			InStorePayment inStorePayment = (InStorePayment) getPaymentGroupManager().createPaymentGroup(TRUCheckoutConstants.IN_STORE_PAYMENT);
			getPaymentGroupManager().addPaymentGroupToOrder(pOrder, inStorePayment);
			addRemainingOrderAmountToPaymentGroup(pOrder, inStorePayment.getId());
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::ensureInStorePayment() : END");
		}
	}

	/**
	 * Ensures credit card payment group in order.
	 * @param pOrder - Holds the order
	 * @throws CommerceException 
	 */
	@SuppressWarnings("unchecked")
	public void ensureCreditCard(Order pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderManager::@method::ensureCreditCard() : START");
		}
		List<PaymentGroup> exclusionPaymentGroups = new ArrayList<PaymentGroup>();

		boolean ensureCreditCard = Boolean.FALSE;
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		for (PaymentGroup paymentGroup : paymentGroups) {
			if (paymentGroup instanceof CreditCard) {
				ensureCreditCard = true;

				// By default order will have default credit card with no relationship.
				// Hence ensuring order remaining amount relationship for credit card if relationship doesn't exist with order.
				CreditCard creditCard = (CreditCard) paymentGroup;
				addRemainingOrderAmountToPaymentGroup(pOrder, creditCard.getId());

				exclusionPaymentGroups.add(paymentGroup);
			}
			if (paymentGroup instanceof TRUGiftCard) {
				exclusionPaymentGroups.add(paymentGroup);
			}
		}
		getPaymentGroupManager().removeAllPaymentGroupsFromOrder(pOrder, exclusionPaymentGroups);

		if (!ensureCreditCard) {
			CreditCard creditCard = (CreditCard) getPaymentGroupManager().createPaymentGroup();
			getPaymentGroupManager().addPaymentGroupToOrder(pOrder, creditCard);
			addRemainingOrderAmountToPaymentGroup(pOrder, creditCard.getId());
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::ensureCreditCard() : END");
		}
	}

	/**
	 * Ensures pay pal payment group in order.
	 * @param pOrder - Holds the order
	 * @throws CommerceException 
	 */
	@SuppressWarnings("unchecked")
	public void ensurePayPal(Order pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::ensureCreditCard() : START");
		}

		List<PaymentGroup> exclusionPaymentGroups = new ArrayList<PaymentGroup>();

		boolean ensurePayPal = Boolean.FALSE;
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		for (PaymentGroup paymentGroup : paymentGroups) {
			if (paymentGroup instanceof TRUPayPal) {
				ensurePayPal = true;
				exclusionPaymentGroups.add(paymentGroup);
			}
			if (paymentGroup instanceof TRUGiftCard) {
				exclusionPaymentGroups.add(paymentGroup);
			}
		}
		getPaymentGroupManager().removeAllPaymentGroupsFromOrder(pOrder, exclusionPaymentGroups);

		if (!ensurePayPal) {
			TRUPayPal payPal = (TRUPayPal) getPaymentGroupManager().createPaymentGroup(TRUCheckoutConstants.PAYPAL_CARD);
			getPaymentGroupManager().addPaymentGroupToOrder(pOrder, payPal);
			addRemainingOrderAmountToPaymentGroup(pOrder, payPal.getId());
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::ensureCreditCard() : END");
		}
	}
	
	/**
	 * This method calculates a coupon code used by the order specified. It looks up an owner of the order (i.e. profile) and.
	 * iterates over its active promotion statuses. It calculates all coupons linked by these statuses. First coupon with proper
	 * site ID (i.e. from the shared cart site group) will be returned.
	 * 
	 * @param pProfile
	 *            profile
	 * @param pTenderdCouponCode - Applied coupon code
	 * @param pIsSingleUseCoupon 
	 * @param pSUCNCoupon 
	 * @return valid - returns true/false.
	 * @throws CommerceException
	 *             if something goes wrong.
	 */
	@SuppressWarnings("unchecked")
	public boolean getCouponCode(Profile pProfile, String pTenderdCouponCode, boolean pIsSingleUseCoupon, String pSUCNCoupon) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getCouponCode() : START");
		}
		boolean isValid = Boolean.TRUE;
		// Get useful managers and tools.
		ClaimableManager claimableManager = getClaimableManager();
		PromotionTools promotionTools = getPromotionTools();

		List<RepositoryItem> activePromotions = null;
		if (pProfile != null) {
			// Take its active promotions and iterate over them.
			activePromotions = (List<RepositoryItem>) pProfile.getPropertyValue(promotionTools.getActivePromotionsProperty());
		}
		if(pIsSingleUseCoupon){
			// Added logic to validate for the SUCN Coupons.
			return validateSingleUseCoupon(activePromotions,pSUCNCoupon);
		}else{
			if (activePromotions != null && !activePromotions.isEmpty()) {
				List<RepositoryItem> coupons = null;
				for (RepositoryItem promotionStatus : activePromotions) {
					// Each promotionStatus contains a list of coupons that
					// activated a promotion. Inspect these coupons.
					if (promotionStatus != null) {
						coupons = (List<RepositoryItem>) promotionStatus.getPropertyValue(promotionTools
								.getPromoStatusCouponsPropertyName());
					}
					if (coupons != null && !coupons.isEmpty()) {
						for (RepositoryItem coupon : coupons) {
							// Proper coupon? I.e. does it came from a shared cart site
							// group?
							if (coupon != null && claimableManager.checkCouponSite(coupon)) {
								// True, return this coupon code.
								String couponId = (String) coupon.getPropertyValue(claimableManager.getClaimableTools()
										.getIdPropertyName());
								if(StringUtils.isNotBlank(couponId) && couponId.equals(pTenderdCouponCode)){
									return Boolean.FALSE;
								}
							}
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getCouponCode() : END");
		}
		// Can't find proper coupon, return nothing.
		return isValid;

	}

	/**
	 * Checks if is any credit card field empty.
	 * 
	 * @param pCreditCardMap
	 *            - creditCardMap
	 * @return boolean
	 */
	public boolean isAnyCreditCardFieldEmpty(Map<String, String> pCreditCardMap) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::isAnyCreditCardFieldEmpty() : START");
		}
		TRUPropertyManager cpmgr = ((TRUOrderManager) getOrderManager()).getPropertyManager();
		String ccNum = null;
		String ccExpMonth = null;
		String ccExpYear = null;
		String ccNameOnCard = null;
		String ccCVV = null;
		String ccCardinalToken = null;
		String creditCardType = null;
		String cardLength = null;
		String cardType = null;
		if (pCreditCardMap != null) {
			ccNum = (String) pCreditCardMap.get(cpmgr.getCreditCardNumberPropertyName());
			ccCardinalToken = (String) pCreditCardMap.get(cpmgr.getCreditCardTokenPropertyName());
			ccExpMonth = (String) pCreditCardMap.get(cpmgr.getCreditCardExpirationMonthPropertyName());
			ccExpYear = (String) pCreditCardMap.get(cpmgr.getCreditCardExpirationYearPropertyName());
			ccNameOnCard = (String) pCreditCardMap.get(cpmgr.getNameOnCardPropertyName());
			ccCVV = (String) pCreditCardMap.get(cpmgr.getCreditCardVerificationNumberPropertyName());
			cardLength = (String) pCreditCardMap.get(TRUConstants.CARD_LENGTH);
			cardType = (String) pCreditCardMap.get(TRUConstants.CARD_TYPE);
		}
		if(!StringUtils.isBlank(cardType) && !TRUConstants.CARD_TYPE_UNDEFINED.equalsIgnoreCase(cardType)){
			creditCardType = cardType;
			pCreditCardMap.put(cpmgr.getCreditCardTypePropertyName(), creditCardType );
			return false;
		}
		if (StringUtils.isNotBlank(ccNum) && StringUtils.isNotBlank(cardLength)) {
			 creditCardType = ((TRUProfileTools) getOrderTools().getProfileTools()).getCreditCardTypeFromRepository(ccNum,cardLength);
			pCreditCardMap.put(cpmgr.getCreditCardTypePropertyName(), creditCardType );
		} else {
			return TRUConstants.COOKIE_SECURE_TRUE;
		}
		
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::isAnyCreditCardFieldEmpty() {0} : END", creditCardType);
		}
		if (StringUtils.isNotBlank(creditCardType) && creditCardType.equals(TRUCheckoutConstants.RUS_PRIVATE_LABEL_CARD)) {
			return StringUtils.isBlank(ccNum) || StringUtils.isBlank(ccNameOnCard) ||
					StringUtils.isBlank(ccCVV)|| StringUtils.isBlank(ccCardinalToken);
		} else {
			return StringUtils.isBlank(ccNum) || StringUtils.isBlank(ccExpMonth) || 
					StringUtils.isBlank(ccExpYear)|| StringUtils.isBlank(ccNameOnCard) || 
					StringUtils.isBlank(ccCVV) || StringUtils.isBlank(ccCardinalToken);
		}
	}

	/**
	 * Checks if is any address field empty.
	 * 
	 * @param pAddress
	 *            address
	 * @return boolean
	 */
	public boolean isAnyAddressFieldEmpty(ContactInfo pAddress) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::isAnyAddressFieldEmpty() : START");
		}
		String firstName = null;
		String lastName = null;
		String country = null;
		String address1 = null;
		String city = null;
		String state = null;
		String postalCode = null;
		String phoneNumber = null;
		if (pAddress != null) {
			firstName = pAddress.getFirstName();
			lastName = pAddress.getLastName();
			country = pAddress.getCountry();
			address1 = pAddress.getAddress1();
			city = pAddress.getCity();
			state = pAddress.getState();
			postalCode = pAddress.getPostalCode();
			phoneNumber = pAddress.getPhoneNumber();
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::isAnyAddressFieldEmpty() : END");
		}
		return StringUtils.isBlank(firstName) || StringUtils.isBlank(lastName) || StringUtils.isBlank(country)||
				StringUtils.isBlank(address1) || StringUtils.isBlank(city) || StringUtils.isBlank(state)|| 
				StringUtils.isBlank(postalCode) || StringUtils.isBlank(phoneNumber);
	}

	/**
	 * Method to Update the Card Type Selected in Payment page to Order property creditCardType.
	 * 
	 * @param pOrder
	 *            the Order
	 * @param pCreditCardType
	 *            the Credit Card Type
	 */
	@SuppressWarnings("unchecked")
	public void updateCardDataInOrder(Order pOrder, String pCreditCardType) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::updateCardDataInOrder() : START");
		}
		if (pOrder != null) {
			TRUOrderImpl orderImpl = (TRUOrderImpl) pOrder;
			orderImpl.setCreditCardType(pCreditCardType);
			//check if PG as giftCard then set creditCardType as null for not to apply tender promotion.
			setCCTypeNullIfPGAsGC(orderImpl);
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::updateCardDataInOrder() : END");
		}

	}

	/**
	 * Method to Update the Card Type to null if Payment group as Gift Card.
	 * 
	 * @param pOrderImpl
	 *            the Order
	 */
	@SuppressWarnings("unchecked")
	public void setCCTypeNullIfPGAsGC(TRUOrderImpl pOrderImpl) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::setCCTypeNullIfPGAsGC() : START");
		}
		if (pOrderImpl != null) {
			List<PaymentGroup> paymentGroups = pOrderImpl.getPaymentGroups();
			for (PaymentGroup paymentGroup : paymentGroups) {
				if (paymentGroup instanceof TRUGiftCard) {
					pOrderImpl.setCreditCardType(null);
					}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::setCCTypeNullIfPGAsGC() : END");
		}
	}

	/**
	 * Auto correct quantity.
	 * 
	 * @param pOrder
	 *            - Order obj
	 * @param pIsGiftWrapAdjust
	 *            - IsGiftWrapAdjust
	 * @return List CommerceItem cItems
	 * @throws CommerceException
	 *             the commerce exception
	 */
	public List<String> autoCorrectQuantity(Order pOrder, boolean pIsGiftWrapAdjust) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::updateCardDataInOrder() : START");
		}
		List<CommerceItem> cItems = null;
		List<String> qtyAdjustedList = null;
		cItems = pOrder.getCommerceItems();
		Map<String, String> qtyAdjustedSkuList = new HashMap<String, String>();
		try {
			for (CommerceItem item : cItems) {
				if (null != item && !(item instanceof TRUGiftWrapCommerceItem) && !(item instanceof TRUDonationCommerceItem)) {
					autoCorrectQuantityWithPurchaseLimit(pOrder, qtyAdjustedSkuList, item, pIsGiftWrapAdjust);
					autoCorrectQuantityWithInventory(pOrder, qtyAdjustedSkuList, item, pIsGiftWrapAdjust);
				}
			}

			
			if (!qtyAdjustedSkuList.isEmpty()) {
				TRUUserSession sessionComponent= (TRUUserSession)ServletUtil.getCurrentRequest().resolveName(TRU_SESSION_SCOPE_COMPONENT_NAME);
				qtyAdjustedList = getShoppingCartUtils().getQtyAdjustedInfoFromMerge(qtyAdjustedSkuList);
				sessionComponent.setQtyAdjustedSkuList(qtyAdjustedList);
			}
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				logError("RepositoryException in @Class::TRUOrderManager::@method::autoCorrectQuantity()", repositoryException);
			}
			throw new CommerceException(repositoryException);
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::updateCardDataInOrder() : END");
		}
		return qtyAdjustedList;
	}

	/**
	 * This method is used to auto correct the item and relationship quantity with customer purchase limit while merging the orders.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pQtyAdjustedSkuList
	 *            the qty adjusted sku list
	 * @param pItem
	 *            the item
	 * @param pIsGiftWrapAdjust
	 *            - IsGiftWrapAdjust
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws CommerceException
	 *             the commerce exception
	 */
	public void autoCorrectQuantityWithPurchaseLimit(Order pOrder, Map<String, String> pQtyAdjustedSkuList, CommerceItem pItem,
			boolean pIsGiftWrapAdjust) throws RepositoryException, CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::autoCorrectQuantityWithPurchaseLimit() : START");
		}
		String skuId = pItem.getCatalogRefId();
		long customerPurchaseLimt = getShoppingCartUtils().getCustomerPurchaseLimit(skuId);
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUOrderManager::@method::autoCorrectQuantityWithPurchaseLimit() : customerPurchaseLimt :{0} , SKUiD is :{1}",
					customerPurchaseLimt, skuId);
		}
		if (pItem.getQuantity() > customerPurchaseLimt) {
			List<ShippingGroupCommerceItemRelationship> sgcirs = pItem.getShippingGroupRelationships();
			List<String> removableRelationships = new ArrayList<String>();
			long quantity = 0;
			boolean setQtyZerosFlag = false;
			for (ShippingGroupCommerceItemRelationship sgcir : sgcirs) {
				if (!setQtyZerosFlag) {
					if ((quantity + sgcir.getQuantity()) > customerPurchaseLimt) {
						long allowedQty = customerPurchaseLimt - quantity;
						if (allowedQty < sgcir.getQuantity() && pIsGiftWrapAdjust && sgcir instanceof TRUShippingGroupCommerceItemRelationship) {
							removeGiftWrapInfoFromRelationShip(pOrder, sgcir);
						}
						pItem.setQuantity(pItem.getQuantity() - (sgcir.getQuantity() - allowedQty));
						if(allowedQty == TRUCommerceConstants.INT_ZERO){
							removableRelationships.add(sgcir.getId());
						}else{
							sgcir.setQuantity(allowedQty);
						}
						setQtyZerosFlag = true;
					} else {
						quantity += sgcir.getQuantity();
						if (pIsGiftWrapAdjust && sgcir instanceof TRUShippingGroupCommerceItemRelationship) {
							removeGiftWrapInfoFromRelationShip(pOrder, sgcir);
						}
					}
				} else {
					pItem.setQuantity(pItem.getQuantity() - sgcir.getQuantity());
					if (isLoggingDebug()) {
						vlogDebug(
								"@Class::TRUOrderManager::@method::autoCorrectQuantityWithPurchaseLimit() : removed sgciRel id is {0}",
								sgcir.getId());
					}
					removableRelationships.add(sgcir.getId());
				}
				if (setQtyZerosFlag) {
					pQtyAdjustedSkuList.put(skuId, TRUCommerceConstants.TRU_SHOPPINGCART_AUTOCORRECT_WITH_PURCHASE_LIMIT);
				}
			}
			if(removableRelationships != null && !removableRelationships.isEmpty()){
				for (String relationShipId : removableRelationships) {
					ShippingGroupCommerceItemRelationship relationship = (ShippingGroupCommerceItemRelationship) pOrder.getRelationship(relationShipId);
					if(relationship != null){
						getCommerceItemManager().removeItemQuantityFromShippingGroup(pOrder,
								relationship.getCommerceItem().getId(), relationship.getShippingGroup().getId(),
								relationship.getQuantity());
					}
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUOrderManager::@method::autoCorrectQuantityWithPurchaseLimit() : pQtyAdjustedSkuList :{0} ",
						pQtyAdjustedSkuList);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::autoCorrectQuantityWithPurchaseLimit() : END");
		}
	}

	/**
	 * This method is used to auto correct the item and relationship quantity with inventory limit while merging the orders.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pQtyAdjustedSkuList
	 *            the qty adjusted sku list
	 * @param pItem
	 *            the item
	 * @param pIsGiftWrapAdjust
	 *            - IsGiftWrapAdjust
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws CommerceException
	 *             the commerce exception
	 */
	public void autoCorrectQuantityWithInventory(Order pOrder, Map<String, String> pQtyAdjustedSkuList, CommerceItem pItem,
			boolean pIsGiftWrapAdjust) throws RepositoryException, CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::autoCorrectQuantityWithInventory() : START");
		}
		String skuId = pItem.getCatalogRefId();
		List<ShippingGroupCommerceItemRelationship> sgcirs = pItem.getShippingGroupRelationships();
		long inventoryQty = TRUCommerceConstants.LONG_ZERO;
		Set<String> locationIdsSet = generateLocationIdSet(sgcirs);
		for (String locationId : locationIdsSet) {
			inventoryQty = getAllowedQuantity(skuId, locationId);
			if(inventoryQty>TRUCommerceConstants.NUM_ZERO){
				Set<ShippingGroupCommerceItemRelationship> locationRelationShips = getRelationsShipsForTheLocation(locationId,
						sgcirs);
				long quantity = 0;
				for (ShippingGroupCommerceItemRelationship sgciRelationship : locationRelationShips) {
					if ((quantity + sgciRelationship.getQuantity()) > inventoryQty) {
						long allowedQty = inventoryQty - quantity;
						if (allowedQty < sgciRelationship.getQuantity() && pIsGiftWrapAdjust &&
								sgciRelationship instanceof TRUShippingGroupCommerceItemRelationship) {
							removeGiftWrapInfoFromRelationShip(pOrder, sgciRelationship);
						}
						pItem.setQuantity(pItem.getQuantity() - (sgciRelationship.getQuantity() - allowedQty));
						sgciRelationship.setQuantity(allowedQty);
						pQtyAdjustedSkuList.put(skuId, TRUCommerceConstants.TRU_SHOPPINGCART_AUTOCORRECT_WITH_INVENTORY);
						break;
					} else {
						quantity += sgciRelationship.getQuantity();
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::autoCorrectQuantityWithInventory() : END");
		}
	}

	/**
	 * Removes the gift wrap info from relation ship.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pSgcir
	 *            the sgcir
	 * @throws CommerceException
	 *             the commerce exception
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public void removeGiftWrapInfoFromRelationShip(Order pOrder, ShippingGroupCommerceItemRelationship pSgcir)
			throws CommerceException, RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("Start :: TRUOrderManager.removeGiftWrapInfoFromRelationShip method..");
		}
		TRUShippingGroupCommerceItemRelationship trusgcir = (TRUShippingGroupCommerceItemRelationship) pSgcir;
		List<RepositoryItem> giftItemInfo = trusgcir.getGiftItemInfo();
		if (trusgcir.getIsGiftItem()) {
			Set<String> giftItemIdSet = new HashSet<String>();
			for (RepositoryItem giftItem : giftItemInfo) {
				giftItemIdSet.add(giftItem.getRepositoryId());
				if (giftItem.getPropertyValue(getPropertyManager().getGiftWrapCommItemId()) != null) {
					CommerceItem commerceItem = pOrder.getCommerceItem((String) giftItem.getPropertyValue(getPropertyManager()
							.getGiftWrapCommItemId()));
					long qty = (Long) giftItem.getPropertyValue(getPropertyManager().getGiftWrapQuantity());
					// null!=commerceItem&&
					if (commerceItem instanceof TRUGiftWrapCommerceItem && commerceItem.getQuantity() - qty > 0) {
						commerceItem.setQuantity(commerceItem.getQuantity() - qty);
					} else {
						getCommerceItemManager().removeItemFromOrder(pOrder, commerceItem.getId());
					}
				}
			}
			if (!giftItemIdSet.isEmpty()) {
				Iterator<String> giftIterator = giftItemIdSet.iterator();
				while (giftIterator.hasNext()) {
					((MutableRepository) getOrderTools().getOrderRepository()).removeItem(giftIterator.next(),
							getPropertyManager().getGiftItemDescriptorName());
				}
			}
			trusgcir.setGiftItemInfo(null);
			trusgcir.setIsGiftItem(Boolean.FALSE);
		}
		if (isLoggingDebug()) {
			vlogDebug("End :: TRUOrderManager.removeGiftWrapInfoFromRelationShip method..");
		}
	}

	/**
	 * Update in store data.
	 * 
	 * @param pOrder
	 *            order
	 * @param pInStorePickupShippingGroup
	 *            the in store pickup shipping group
	 * @param pStorePickUpInfo
	 *            the store pick up info
	 * @throws CommerceException
	 *             the commerce exception
	 */
	public void updateInStoreData(Order pOrder, InStorePickupShippingGroup pInStorePickupShippingGroup,
			TRUStorePickUpInfo pStorePickUpInfo) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::updateInStoreData() : START");
		}

		if (pInStorePickupShippingGroup == null || pStorePickUpInfo == null) {
			throw new CommerceException(TRUCheckoutConstants.NO_INSTORE_PICKUP);
		}

		TRUOrderTools orderTools = (TRUOrderTools) getOrderTools();
		if (pInStorePickupShippingGroup instanceof TRUInStorePickupShippingGroup) {
			TRUInStorePickupShippingGroup inStorePickupShippingGroup = (TRUInStorePickupShippingGroup) pInStorePickupShippingGroup;
			orderTools.updateInStoreData(pOrder, inStorePickupShippingGroup, pStorePickUpInfo);
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::updateInStoreData() : END");
		}
	}

	/**
	 * This method calculates a coupon code used by the order specified. It looks up an owner of the order (i.e. profile) and
	 * iterates over its active promotion statuses. It calculates all coupons linked by these statuses.
	 * 
	 * @param pProfile
	 *            profile
	 * 
	 * @return boolean true/false - reached max limit or not
	 * @throws RepositoryException
	 *             RepositoryException
	 */

	@SuppressWarnings("unchecked")
	public boolean checkMaxCouponLimit(Profile pProfile) throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::checkMaxCouponLimit() : START");
		}
		PromotionTools promotionTools = getPromotionTools();
		List<RepositoryItem> activePromotions = null;
		List<RepositoryItem> appliedCoupons = new ArrayList<RepositoryItem>();
		if (pProfile != null) {
			activePromotions = (List<RepositoryItem>) pProfile.getPropertyValue(promotionTools.getActivePromotionsProperty());
		}
		List<RepositoryItem> coupons = null;
		if (activePromotions != null && !activePromotions.isEmpty()) {
			for (RepositoryItem promotionStatus : activePromotions) {
				if (promotionStatus != null) {
					coupons = (List<RepositoryItem>) promotionStatus.getPropertyValue(promotionTools
							.getPromoStatusCouponsPropertyName());
					appliedCoupons.addAll(coupons);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::checkMaxCouponLimit() : END");
		}
		if (appliedCoupons != null && getCouponMaxLimit() > 0 && appliedCoupons.size() >= getCouponMaxLimit()) {
			return true;
		}
		return false;
	}

	/**
	 * This method is used to retrieve Coupon Max Limit based on site id.
	 * 
	 * @return int
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public int getCouponMaxLimit() throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getCouponMaxLimit() : START");
		}
		int couponMaxLimit = 0;
		if (SiteContextManager.getCurrentSite() != null && SiteContextManager.getCurrentSite().getPropertyValue(getPropertyManager().getMaxCouponLimitPropertyName()) != null) {
			couponMaxLimit = (int)SiteContextManager.getCurrentSite().getPropertyValue(getPropertyManager().getMaxCouponLimitPropertyName());
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getCouponMaxLimit() : END");
		}
		return couponMaxLimit;
	}

	/**
	 * Adds the shipping address.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pProfile
	 *            the profile
	 * @param pShippingAddress
	 *            the shipping address
	 * @param pShippingGroupMapContainer
	 *            the shipping group map container
	 * @throws CommerceException
	 *             the commerce exception
	 */
	@SuppressWarnings("unchecked")
	public void addShippingAddress(Order pOrder, RepositoryItem pProfile, ContactInfo pShippingAddress,
			ShippingGroupMapContainer pShippingGroupMapContainer) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::addShippingAddress() : START");
		}
		if(StringUtils.isBlank(((TRUOrderImpl) pOrder).getEmail())) {
			((TRUOrderImpl) pOrder).setEmail(pShippingAddress.getEmail());
		} else if(!pProfile.isTransient()) {
			((TRUOrderImpl)pOrder).setEmail((String)pProfile.getPropertyValue(getOrderTools().getProfileTools()
					.getPropertyManager().getEmailAddressPropertyName()));
		}
		List<ShippingGroup> shippingGroupList = pOrder.getShippingGroups();
		if (shippingGroupList != null && !shippingGroupList.isEmpty()) {
			for (ShippingGroup shippingGroup : shippingGroupList) {
				if (shippingGroup instanceof TRUHardgoodShippingGroup) {
					TRUHardgoodShippingGroup truHardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
					if(truHardgoodShippingGroup.getShippingAddress() != null && 
							StringUtils.isNotBlank(truHardgoodShippingGroup.getShippingAddress().getFirstName())){
						continue;
					}
					String nickName = pShippingAddress.getFirstName() + TRUPaypalConstants.SPACE_STRING	+ pShippingAddress.getLastName();
					truHardgoodShippingGroup.setNickName(nickName);
					truHardgoodShippingGroup.setShippingAddress(pShippingAddress);
					// set the shipping group in container
					pShippingGroupMapContainer.setDefaultShippingGroupName(nickName);
					((TRUShippingGroupContainerService) pShippingGroupMapContainer).setSelectedAddressNickName(nickName);

					// Put the new shipping group in the container.
					pShippingGroupMapContainer.addShippingGroup(nickName, truHardgoodShippingGroup);
					((TRUShippingGroupContainerService)pShippingGroupMapContainer).setShippingAddress(pShippingAddress);
					// ((TRUOrderTools)getOrderTools()).saveAddressToAddressBook(pProfile, (Address)pShippingAddress, nickName);

					// setting cheapest shipping method
					String cheapestShippingMethod = null;
					String regionCode = null;
					double shippingPrice = TRUCommerceConstants.DOUBLE_ZERO;
					HashMap<String, Object> map = new HashMap<String, Object>();
					List<TRUShipMethodVO> availableShippingMethods = null;
					map.put(TRUPayPalConstants.ORDER_PARAM, pOrder);
					map.put(TRUPayPalConstants.SHIPPINGGROUPCONTAINER_PARAM, pShippingGroupMapContainer);
					map.put(TRUPayPalConstants.STATE_PARAM, pShippingAddress.getState());
					map.put(TRUPayPalConstants.ADDRESS1_PARAM, pShippingAddress.getAddress1());
					map.put(TRUPayPalConstants.ADDRESS_PARAM, pShippingAddress);
					TRUShippingPricingEngine shippingPricingEngine = (TRUShippingPricingEngine) getPromotionTools()
							.getPricingTools().getShippingPricingEngine();
					if (shippingPricingEngine != null) {
						try {
							availableShippingMethods = shippingPricingEngine.getAvailableMethods(null, null, null, null, map);
						} catch (PricingException pE) {
							if (isLoggingError()) {
								logError("PricingException in pricing shipping method @Class::TRUOrderManager:::@method::addShippingAddress()", pE);
							}
						}
						if (availableShippingMethods != null && !availableShippingMethods.isEmpty()) {
							cheapestShippingMethod = availableShippingMethods.get(0).getShipMethodCode();
							regionCode = availableShippingMethods.get(0).getRegionCode();
							shippingPrice = availableShippingMethods.get(0).getShippingPrice();
							truHardgoodShippingGroup.setShippingMethod(cheapestShippingMethod);
							truHardgoodShippingGroup.setRegionCode(regionCode);
							truHardgoodShippingGroup.setShippingPrice(shippingPrice);
						}
					}
					// setting shipping as active tab
					((TRUOrderImpl) pOrder).getActiveTabs().put(TRUCheckoutConstants.SHIPPING_TAB, Boolean.TRUE);
				} else if (shippingGroup instanceof TRUInStorePickupShippingGroup) {
					TRUInStorePickupShippingGroup truInStorePickupShippingGroup = (TRUInStorePickupShippingGroup) shippingGroup;
					if(StringUtils.isNotBlank(truInStorePickupShippingGroup.getFirstName()) || 
							StringUtils.isNotBlank(truInStorePickupShippingGroup.getPhoneNumber())){
						continue;
					}
					// PRE-Populating pick up step fields.
					truInStorePickupShippingGroup.setFirstName(pShippingAddress.getFirstName());
					truInStorePickupShippingGroup.setLastName(pShippingAddress.getLastName());
					truInStorePickupShippingGroup.setPhoneNumber(pShippingAddress.getPhoneNumber());
					truInStorePickupShippingGroup.setEmail(pShippingAddress.getEmail());
					// pick up step is also complete now
					((TRUOrderImpl) pOrder).getActiveTabs().put(TRUCheckoutConstants.PICKUP_TAB, Boolean.TRUE);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::addShippingAddress() : END");
		}
	}

	/**
	 * Get a diff between two dates.
	 * 
	 * @param pDate1
	 *            the oldest date
	 * @param pDate2
	 *            the newest date
	 * @param pTimeUnit
	 *            the unit in which you want the diff
	 * @return the diff value, in the provided unit
	 */
	public long getDateDiff(Date pDate1, Date pDate2, TimeUnit pTimeUnit) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getDateDiff() : START");
		}
		TimeZone tz = TimeZone.getDefault();
		Date ret = new Date(pDate2.getTime() - tz.getRawOffset());
		// if we are now in DST, back off by the delta. Note that we are checking the GMT date, this is the KEY.
		if (tz.inDaylightTime(ret)) {
			Date dstDate = new Date(ret.getTime() - tz.getDSTSavings());

			// check to make sure we have not crossed back into standard time
			// this happens when we are on the cusp of DST (7pm the day before the change for PDT)
			if (tz.inDaylightTime(dstDate)) {
				ret = dstDate;
			}
		}
		long diffInMillies = ret.getTime() - pDate1.getTime();
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getDateDiff() : END");
		}
		return pTimeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * This method will populate the tabs details while PayPal Order.
	 *
	 * @param pOrder the order
	 */
	public void updatePayPalOrderDetails(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::updatePayPalOrderDetails() : START");
		}
		if (!(pOrder instanceof TRUOrderImpl)) {
			return;
		}

		if (pOrder instanceof TRUOrderImpl) {
			((TRUOrderImpl) pOrder).setActiveTabs(null);
			// ((TRUOrderImpl)pOrder).setPaypalPayerId(pPayerId);
			((TRUOrderImpl) pOrder).getActiveTabs().put(TRUCheckoutConstants.REVIEW_TAB, Boolean.TRUE);
			((TRUOrderImpl) pOrder).getActiveTabs().put(TRUCheckoutConstants.PAYMENT_TAB, Boolean.TRUE);
			((TRUOrderImpl) pOrder).setCurrentTab(TRUCheckoutConstants.REVIEW_TAB);
			List<CommerceItem> listOfCommItems = pOrder.getCommerceItems();
			boolean isOrderOnlyInStoreItems = true;
			boolean isOrderOnlyDonationItem = true;
			boolean isOrderHavingGiftItem = false;
			boolean isOrderHavingPickupItem = false;
			for (CommerceItem item : listOfCommItems) {
				if (!(item instanceof TRUDonationCommerceItem)) {
					isOrderOnlyDonationItem = false;
				}
				List<ShippingGroupCommerceItemRelationship> listOfRelations = item.getShippingGroupRelationships();
				for (ShippingGroupCommerceItemRelationship shipItem : listOfRelations) {
					if (shipItem.getShippingGroup() instanceof TRUInStorePickupShippingGroup) {
						isOrderHavingPickupItem = true;
					} else {
						isOrderOnlyInStoreItems = false;
					}
					if (((TRUShippingGroupCommerceItemRelationship) shipItem).getIsGiftItem()) {
						isOrderHavingGiftItem = true;
					}
				}
			}
			if (isOrderHavingGiftItem) {
				((TRUOrderImpl) pOrder).getActiveTabs().put(TRUCheckoutConstants.GIFTING_TAB, Boolean.TRUE);
				((TRUOrderImpl) pOrder).setCurrentTab(TRUCheckoutConstants.GIFTING_TAB);
			}
			if (isOrderHavingPickupItem) {
				((TRUOrderImpl) pOrder).getActiveTabs().put(TRUCheckoutConstants.PICKUP_TAB, Boolean.TRUE);
			}
			if (!isOrderOnlyInStoreItems && !isOrderOnlyDonationItem) {
				((TRUOrderImpl) pOrder).getActiveTabs().put(TRUCheckoutConstants.SHIPPING_TAB, Boolean.TRUE);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::updatePayPalOrderDetails() : END");
		}
	}

	/**
	 * This method will populate the PAYPAL Token in PayPal Order.
	 * 
	 * @param pOrder
	 *            current order
	 * @param pPaypalToken
	 *            pPaypalToken which comes from PayPal site
	 */
	public void updatePayPalTokenOrderDetails(Order pOrder, String pPaypalToken) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::updatePayPalTokenOrderDetails() : START");
		}
		if (pOrder == null || StringUtils.isBlank(pPaypalToken)) {
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUOrderManager::@method::updatePayPalTokenOrderDetails() : ENDED with pPaypalToken is {0}", pPaypalToken);
			}
			return;
		}
		if (pOrder instanceof TRUOrderImpl) {
			((TRUOrderImpl) pOrder).setPaypalToken(pPaypalToken);
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::updatePayPalTokenOrderDetails() : END");
		}
	}

	/**
	 * This method will return the payment group of type Paypal from the list of payment groups for an order.
	 * 
	 * @param pOrder
	 *            current order
	 * @return TRUPayPal payPalPG
	 */
	@SuppressWarnings("unchecked")
	public TRUPayPal fetchPayPalPaymentGroupForOrder(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::fetchPayPalPaymentGroupForOrder() : START");
		}
		TRUPayPal payPalPG = null;
		if (pOrder == null) {
			return payPalPG;
		}
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		for (PaymentGroup paymentGroup : paymentGroups) {
			if (paymentGroup instanceof TRUPayPal) {
				if (isLoggingDebug()) {
					vlogDebug("@Class::TRUOrderManager::@method::fetchPayPalPaymentGroupForOrder() : payPal payment group already added to order : "+
							paymentGroup.toString());
					vlogDebug("@Class::TRUOrderManager::@method::fetchPayPalPaymentGroupForOrder() : END");
				}
				return (TRUPayPal) paymentGroup;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::fetchPayPalPaymentGroupForOrder() : No payPal payment group already added to order");
			vlogDebug("@Class::TRUOrderManager::@method::fetchPayPalPaymentGroupForOrder() : END");
		}
		return payPalPG;
	}
	
	/**
	 * This method will return the payment group of type apple pay from the list of payment groups for an order.
	 * 
	 * @param pOrder
	 *            current order
	 * @return TRUPayPal payPalPG
	 */
	@SuppressWarnings("unchecked")
	public TRUApplePay getApplePayPGForOrder(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getApplePayPGForOrder() : START");
		}
		TRUApplePay applepay = null;
		if (pOrder == null) {
			return applepay;
		}
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		if (paymentGroups !=null && !paymentGroups.isEmpty()) {
			for (PaymentGroup paymentGroup : paymentGroups) {
				if (paymentGroup instanceof TRUApplePay) {
					if (isLoggingDebug()) {
						vlogDebug("@Class::TRUOrderManager::@method::getApplePayPGForOrder() : applepay payment group already added to order : "+
								paymentGroup.toString());
						vlogDebug("@Class::TRUOrderManager::@method::getApplePayPGForOrder() : END");
					}
					return (TRUApplePay) paymentGroup;
				}
			}
		} 
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::fetchPayPalPaymentGroupForOrder() : No payPal payment group already added to order");
			vlogDebug("@Class::TRUOrderManager::@method::fetchPayPalPaymentGroupForOrder() : END");
		}
		return applepay;
	}
	
	/**
	 * This method is used to apply apply payment group to order;.
	 *
	 * @param pOrder the order
	 * @param pAppleRequest the apple request
	 * @throws CommerceException the commerce exception
	 */
	@SuppressWarnings("unchecked")
	public void applyApplePayPG(TRUOrderImpl pOrder, TRUApplePayForm pAppleRequest) throws CommerceException  {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderManager::@method::applyApplePayPG()");
			vlogDebug("@Class::TRUOrderManager::@method::applyApplePayPG() pOrder{0} appleRequest{1}::",pOrder,pAppleRequest );
		}
		boolean exists = false;
		if (pOrder == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(TRUPipeLineConstants.INVALID_ORDER_PARAMETER, MY_RESOURCE_NAME, sResourceBundle));
		}
		TRUApplePay applePayPG = getApplePayPGForOrder(pOrder);
		if (applePayPG == null) {
			applePayPG = (TRUApplePay)getPaymentGroupManager().createPaymentGroup(TRUCheckoutConstants.APPLE_PAY);
			//add payment group to order
			getPaymentGroupManager().addPaymentGroupToOrder(pOrder, applePayPG);
		}
		
		double orderRemaningAmount = getOrderRemaningAmount(pOrder);
		List<Relationship> paymentRelationships = pOrder.getPaymentGroupRelationships();
		for (Relationship relationship : paymentRelationships) {
			if (relationship.getRelationshipType()== RelationshipTypes.ORDERAMOUNTREMAINING) {
				exists = true;
				if (isLoggingDebug()) {
					logDebug("@Class::TRUOrderManager::@method::applyApplePayPG() -> ORDERAMOUNTREMAINING is exists");
				}
				break;
			}
			
		}
		
		if(!exists) {
			if (isLoggingDebug()) {
				logDebug("@Class::TRUOrderManager::@method::applyApplePayPG() -> addRemainingOrderAmountToPaymentGroup");
			}
			addRemainingOrderAmountToPaymentGroup(pOrder, applePayPG.getId());
		}
		applePayPG.setAmount(orderRemaningAmount);
		//applePayPG.setBillingAddress(pAppleRequest.getBillingAddress());
		String CurrencyCode = getConfiguration().getCurrencyCodesString().get(pOrder.getSiteId());
		if (StringUtils.isBlank(CurrencyCode)) {
			applePayPG.setCurrencyCode(TRUPaypalConstants.USD);
		} else {
			applePayPG.setCurrencyCode(CurrencyCode);
		}
		applePayPG.setData(pAppleRequest.getData());
		applePayPG.setEphemeralPublicKey(pAppleRequest.getEphemeralPublicKey());
		applePayPG.setPublickeyHash(pAppleRequest.getPublicKeyHash());
		applePayPG.setOrderid(pOrder.getId());
		applePayPG.setSignature(pAppleRequest.getSignature());
		applePayPG.setApplePayTransactionId(pAppleRequest.getApplePayTransactionId());
		applePayPG.setVersion(pAppleRequest.getVersion());
		
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderManager::@method::applyApplePayPG() : END");
		}
	}
	
	

	/**
	 * This method will create a new payment group of the type payPal and add it to the current order.
	 * 
	 * @param pOrder
	 *            current order
	 * @param pPaymentGroupMapContainer
	 *            pPaymentGroupMapContainer
	 * @param pPayPalToken
	 *            paypalToen returned by payPal site
	 * @throws CommerceException
	 *             CommerceException
	 */
	public void addPayPalPaymentGroupToOrder(Order pOrder, PaymentGroupMapContainer pPaymentGroupMapContainer, String pPayPalToken)
			throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::addPayPalPaymentGroupToOrder() : START");
		}
		if (pOrder == null || pPaymentGroupMapContainer == null || StringUtils.isBlank(pPayPalToken)) {
			return;
		}
		TRUPaymentGroupManager paymentGroupManager = (TRUPaymentGroupManager) getPaymentGroupManager();
		double payPalAmt = paymentGroupManager.getRemainingOrderAmount(pOrder);
		TRUPayPal payPalPG = fetchPayPalPaymentGroupForOrder(pOrder);
		if (payPalPG == null) {
			payPalPG = (TRUPayPal) pPaymentGroupMapContainer.getPaymentGroup(TRUPropertyNameConstants.PAYPAL_PAYMENT);
			if (payPalPG == null) {
				payPalPG = (TRUPayPal) getPaymentGroupManager().createPaymentGroup(TRUPropertyNameConstants.PAYPAL_PAYMENT);
				if(payPalPG != null) {
					pPaymentGroupMapContainer.addPaymentGroup(TRUPropertyNameConstants.PAYPAL_PAYMENT, payPalPG);
					paymentGroupManager.addPaymentGroupToOrder(pOrder, payPalPG);
				}
			} else {
				paymentGroupManager.addPaymentGroupToOrder(pOrder, payPalPG);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::addPayPalPaymentGroupToOrder() : PaypalPG is:"+payPalPG);
		}
		if(payPalPG != null) {
			payPalPG.setAmount(payPalAmt);
			payPalPG.setToken(pPayPalToken);
			addRemainingOrderAmountToPaymentGroup(pOrder, payPalPG.getId());
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::addPayPalPaymentGroupToOrder() : END");
		}
	}

	/**
	 * This method will create a new payment group of the type payPal and add it to the current order.
	 * 
	 * @param pOrder
	 *            current order
	 * @param pPaymentGroupMapContainer
	 *            pPaymentGroupMapContainer
	 * @param pPayPalToken
	 *            paypalToen returned by payPal site
	 * @param pProfile
	 *            the profile
	 * @throws CommerceException
	 *             CommerceException
	 */
	public void addUpdatePayPalPaymentGroupToOrder(Order pOrder, PaymentGroupMapContainer pPaymentGroupMapContainer,
			String pPayPalToken, RepositoryItem pProfile) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::addUpdatePayPalPaymentGroupToOrder() : START");
		}
		if (pOrder == null && pPaymentGroupMapContainer == null && !StringUtils.isBlank(pPayPalToken) && pProfile == null) {
			return;
		}
		RepositoryItem proifleBillingAddress = (RepositoryItem) pProfile.getPropertyValue(getPropertyManager()
				.getBillingAddressPropertyName());
		TRUPaymentGroupManager paymentGroupManager = (TRUPaymentGroupManager) getPaymentGroupManager();
		double payPalAmt = paymentGroupManager.getRemainingOrderAmount(pOrder);
		TRUPayPal payPalPG = fetchPayPalPaymentGroupForOrder(pOrder);
		if (payPalPG == null) {
			payPalPG = (TRUPayPal) pPaymentGroupMapContainer.getPaymentGroup(TRUPropertyNameConstants.PAYPAL_PAYMENT);
			if (payPalPG == null) {
				payPalPG = (TRUPayPal) getPaymentGroupManager().createPaymentGroup(TRUPropertyNameConstants.PAYPAL_PAYMENT);
				if(payPalPG != null) {
					pPaymentGroupMapContainer.addPaymentGroup(TRUPropertyNameConstants.PAYPAL_PAYMENT, payPalPG);
					paymentGroupManager.addPaymentGroupToOrder(pOrder, payPalPG);
				}
			} else {
				paymentGroupManager.addPaymentGroupToOrder(pOrder, payPalPG);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::addPayPalPaymentGroupToOrder() : PaypalPG is: {0}", payPalPG);
		}
		if(payPalPG != null) {
			payPalPG.setAmount(payPalAmt);
			payPalPG.setToken(pPayPalToken);
			if (proifleBillingAddress != null) {
				Address address = payPalPG.getBillingAddress();
				OrderTools.copyAddress(proifleBillingAddress, address);
			}
			addRemainingOrderAmountToPaymentGroup(pOrder, payPalPG.getId());
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::addUpdatePayPalPaymentGroupToOrder() : END");
		}
	}

	/**
	 * This method will return the payment group of type Paypal from the list of payment groups for an order.
	 * 
	 * @param pOrder
	 *            current order
	 * @return TRUPayPal payPalPG
	 */
	@SuppressWarnings("unchecked")
	public TRUCreditCard fetchCreditCardPaymentGroupForOrder(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderManager::@method::fetchCreditCardPaymentGroupForOrder() : START");
		}
		TRUCreditCard creditCardPG = null;
		if (pOrder == null) {
			return creditCardPG;
		}
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		for (PaymentGroup paymentGroup : paymentGroups) {
			if (paymentGroup instanceof CreditCard) {
				if (isLoggingDebug()) {
					vlogDebug("@Class::TRUOrderManager::@method::fetchCreditCardPaymentGroupForOrder() payPal payment group already added to order :"+
							 paymentGroup.toString());
					logDebug("@Class::TRUOrderManager::@method::fetchCreditCardPaymentGroupForOrder() : END");
				}
				return (TRUCreditCard) paymentGroup;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::fetchCreditCardPaymentGroupForOrder() No payPal payment group already added to order");
			vlogDebug("@Class::TRUOrderManager::@method::fetchCreditCardPaymentGroupForOrder() : END");
		}
		return creditCardPG;
	}

	/**
	 * <p>
	 * getShipGroupByNickNameAndShipMethod.
	 * </p>
	 * .
	 * 
	 * @param pOrder
	 *            - Order
	 * @param pNickName
	 *            nickName
	 * @param pShippingMethod
	 *            shippingMethod
	 * @return hardgoodShippingGroup
	 */
	public TRUHardgoodShippingGroup getShipGroupByNickNameAndShipMethod(Order pOrder, String pNickName, String pShippingMethod) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getShipGroupByNickNameAndShipMethod() : START");
			vlogDebug("INPUT PARAMS: getShipGroupByNickNameAndShipMethod pOrder: {0} pNickName: {1} pShippingMethod: {2}", pOrder,
					pNickName, pShippingMethod);
		}
		if (pOrder == null || pShippingMethod == null) {
			return null;
		}
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		for (ShippingGroup sg : shippingGroups) {
			if (sg instanceof TRUChannelHardgoodShippingGroup) {
				TRUChannelHardgoodShippingGroup hardgoodShippingGroup = (TRUChannelHardgoodShippingGroup) sg;
				if (StringUtils.isBlank(pNickName)
						&& hardgoodShippingGroup.getShippingMethod()
								.equalsIgnoreCase(pShippingMethod)) {
					return hardgoodShippingGroup;
				}else if (StringUtils.isNotBlank(pNickName) &&
						StringUtils.isNotBlank(hardgoodShippingGroup.getNickName()) && 
						hardgoodShippingGroup.getNickName().equalsIgnoreCase(
						pNickName)
						&& hardgoodShippingGroup.getShippingMethod()
								.equalsIgnoreCase(pShippingMethod)) {
					return hardgoodShippingGroup;
				}

			} else if (sg instanceof TRUHardgoodShippingGroup) {
				TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) sg;
				if (StringUtils.isBlank(pNickName)
						&& hardgoodShippingGroup.getShippingMethod()
								.equalsIgnoreCase(pShippingMethod)) {
					return hardgoodShippingGroup;
				}else if (StringUtils.isNotBlank(pNickName) && pNickName.equalsIgnoreCase(hardgoodShippingGroup.getNickName())
						&& hardgoodShippingGroup.getShippingMethod()
								.equalsIgnoreCase(pShippingMethod)) {
					return hardgoodShippingGroup;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getShipGroupByNickNameAndShipMethod() : END");
		}
		return null;
	}

	/**
	 * <p>
	 * getShipGroupByNickName.
	 * </p>
	 * .
	 * 
	 * @param pOrder
	 *            - Order
	 * @param pNickName
	 *            nickName
	 * @return hardgoodShippingGroup
	 */
	public TRUHardgoodShippingGroup getShipGroupByNickName(Order pOrder, String pNickName) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getShipGroupByNickName() : START");
			vlogDebug(
					"@Class::TRUOrderManager::@method::getShipGroupByNickName() INPUT PARAMS: getShipGroupByNickName pOrder: {0} pNickName: {1} ",
					pOrder, pNickName);
		}
		if (pOrder == null || pNickName == null) {
			return null;
		}
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		for (ShippingGroup sg : shippingGroups) {
			if (sg instanceof TRUHardgoodShippingGroup && !(sg instanceof TRUChannelHardgoodShippingGroup)) {
				TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) sg;
				if (hardgoodShippingGroup != null &&
						hardgoodShippingGroup.getNickName() != null &&
						hardgoodShippingGroup.getNickName().equalsIgnoreCase(pNickName)) {
					return hardgoodShippingGroup;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getShipGroupByNickName() : END");
		}
		return null;
	}

	/**
	 * <p>
	 * getShipGroupsByNickName.
	 * </p>
	 * .
	 * 
	 * @param pOrder
	 *            the order
	 * @param pNickName
	 *            the nick name
	 * @return List TRUHardgoodShippingGroup - repective SG's
	 */
	public List<TRUHardgoodShippingGroup> getShipGroupsByNickName(Order pOrder, String pNickName) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getShipGroupsByNickName() : START");
			vlogDebug(
					"@Class::TRUOrderManager::@method::getShipGroupsByNickName() INPUT PARAMS: getShipGroupsByNickName pOrder: {0}" + 
							" pNickName: {1} ", pOrder, pNickName);
		}
		if (pOrder == null || pNickName == null) {
			return null;
		}
		List<TRUHardgoodShippingGroup> shipGroups = new ArrayList<TRUHardgoodShippingGroup>();
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		for (ShippingGroup sg : shippingGroups) {
			if (sg instanceof TRUHardgoodShippingGroup) {
				TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) sg;
				if (hardgoodShippingGroup.getNickName()!=null && !(hardgoodShippingGroup instanceof TRUChannelHardgoodShippingGroup) && hardgoodShippingGroup.getNickName().equalsIgnoreCase(pNickName)) {
					shipGroups.add(hardgoodShippingGroup);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getShipGroupsByNickName() : END");
		}
		return shipGroups;
	}

	/**
	 * This method returns SynchronySDPRequestBean after populating all available properties from order.
	 * 
	 * @param pOrder
	 *            order
	 * @param pUrl
	 *            url
	 * @return SynchronySDPRequestBean
	 * @throws ParseException
	 *             the parse exception
	 */
	public SynchronySDPRequestBean createSDPObject(Order pOrder, String pUrl) throws ParseException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::createSDPObject() : START");
		}
		SynchronySDPRequestBean bean = new SynchronySDPRequestBean();
		updateBillingOrShippingAddress(bean, pOrder);
		Double orderTotal = getPromotionTools().getPricingTools().round(((TRUOrderPriceInfo)pOrder.getPriceInfo()).getTotal());
		String orderTotalAsString = getParsedAmount(orderTotal);
		bean.setIntialTransactionAmt(orderTotalAsString);
		if (((TRUOrderImpl) pOrder).getEmail() == null) {
			bean.setEmail(TRUCheckoutConstants.EMPTY_STRING);
		} else {
			bean.setEmail(((TRUOrderImpl) pOrder).getEmail());
		}
		if (((TRUOrderImpl) pOrder).getRewardNumber() == null) {
			bean.setMemberNumber(TRUCheckoutConstants.EMPTY_STRING);
		} else {
			bean.setMemberNumber(((TRUOrderImpl) pOrder).getRewardNumber());
		}

		bean.setPromoCodeAD(TRUCheckoutConstants.NULL);
		bean.setPromoCodePOC(TRUCheckoutConstants.NULL);
		bean.setRU(pUrl);

		bean.setDateTimeStamp(getParsedTimeStamp(TRUCheckoutConstants.DATE_FORMAT));
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::createSDPObject() : END");
		}
		return bean;
	}

	/**
	 * This method is used to parse double order amount value required 15 digit format for Synchrony.
	 * 
	 * @param pAmount
	 *            Double
	 * @return String
	 */
	public String getParsedAmount(Double pAmount) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getParsedAmount() : START");
		}
		String parsedAmount = String.valueOf(pAmount);
		parsedAmount = parsedAmount.replace(TRUCheckoutConstants.CHAR_DOT, TRUCheckoutConstants.EMPTY_STRING);
		parsedAmount = String.format(TRUCheckoutConstants.FORMAT_FOURTEEN_DIGIT, Integer.parseInt(parsedAmount));
		parsedAmount = parsedAmount.concat(TRUCheckoutConstants.CHAR_PLUS);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getParsedAmount() : END");
		}
		return parsedAmount;
	}

	/**
	 * This method is used to get time stamp in desired format in String.
	 *
	 * @param pDateFormat
	 *            String
	 * @return String
	 */
	public String getParsedTimeStamp(String pDateFormat) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getParsedTimeStamp() : START");
		}
		String parsedTimeStamp = null;
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.SECOND, getConfiguration().getServerTimeAdjustmentWithSynchrony());
		Date date = cal.getTime();
		DateFormat formatter = new SimpleDateFormat(pDateFormat,Locale.US);
		formatter.setTimeZone(TimeZone.getTimeZone(TRUCheckoutConstants.TIME_ZONE));
		parsedTimeStamp = formatter.format(date);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getParsedTimeStamp() : END");
		}
		return parsedTimeStamp;
	}

	/**
	 * This method gets required parameters from order and constructs SynchronySDPRequestBean. After the SynchronySDPRequestBean
	 * construction is done, it passes the bean to prepareSynchronyURL method of Synchrony and gets the URL.
	 * 
	 * @param pOrder
	 *            order
	 * @param pUrl
	 *            prl
	 * @return synchronyUrl - String
	 */
	public String getSynchronyUrl(Order pOrder, String pUrl) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getSynchronyUrl() : START");
		}
		String synchronyUrl = null;
		try {
			SynchronySDPRequestBean synchronySDPRequestBean = createSDPObject(pOrder, pUrl);
			synchronyUrl = getSynchrony().prepareSynchronyURL(synchronySDPRequestBean);
		} catch (TRUIntegrationException intExc) {
			if (isLoggingError()) {
				logError("TRUIntegrationException in  @Class::TRUOrderManager:::@method::getSynchronyUrl()", intExc);
			}
		} catch (ParseException parExc) {
			if (isLoggingError()) {
				logError("ParseException in  @Class::TRUOrderManager:::@method::getSynchronyUrl()", parExc);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getSynchronyUrl() : END");
		}
		return synchronyUrl;
	}

	/**
	 * This method is used to passes SDP parameter from payment page landing url after user lands back to payment page from
	 * Synchrony site. And it gets back the SynchronySDPResponseBean object populated with response data which is used for
	 * populating temporary credit card info on the payment page.
	 * 
	 * @param pResponseSDPParam
	 *            the response sdp param
	 * @return SynchronySDPResponseBean
	 */
	public SynchronySDPResponseBean getSynchronyResponseBean(String pResponseSDPParam) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getSynchronyResponseBean() : START");
		}
		SynchronySDPResponseBean SynchronySDPResponseObj = null;
		try {
			SynchronySDPResponseObj = getSynchrony().parseResponseSDP(pResponseSDPParam);
		} catch (TRUIntegrationException intExc) {
			if (isLoggingError()) {
				logError("TRUIntegrationException in getSynchronyResponseBean", intExc);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getSynchronyResponseBean() : END");
		}
		return SynchronySDPResponseObj;
	}

	/**
	 * This method will create a new payment group of the type payPal and add it to the current order.
	 * 
	 * @param pOrder
	 *            current order
	 * @param pPaymentGroupMapContainer
	 *            pPaymentGroupMapContainer
	 * @param pProfile
	 *            the profile
	 * @throws CommerceException
	 *             CommerceException
	 */
	public void addUpdatePayPalPaymentGroupToOrder(Order pOrder, PaymentGroupMapContainer pPaymentGroupMapContainer,
			RepositoryItem pProfile) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::addUpdatePayPalPaymentGroupToOrder() : START");
		}
		if (pOrder == null && pPaymentGroupMapContainer == null && pProfile == null) {
			return;
		}
		 Address profileBillingAddress = null;
		RepositoryItem profileBillingAddressItem = (RepositoryItem) pProfile.getPropertyValue(getPropertyManager()
				.getBillingAddressPropertyName());
		if(profileBillingAddressItem != null) {
			profileBillingAddress= new Address();
			OrderTools.copyAddress(profileBillingAddressItem, profileBillingAddress);
		}
		TRUPaymentGroupManager paymentGroupManager = (TRUPaymentGroupManager) getPaymentGroupManager();
		double payPalAmt = paymentGroupManager.getRemainingOrderAmount(pOrder);
		TRUPayPal payPalPG = fetchPayPalPaymentGroupForOrder(pOrder);
		if (payPalPG == null) {
			payPalPG = (TRUPayPal) pPaymentGroupMapContainer.getPaymentGroup(TRUPropertyNameConstants.PAYPAL_PAYMENT);
			if (payPalPG == null) {
				payPalPG = (TRUPayPal) getPaymentGroupManager().createPaymentGroup(TRUPropertyNameConstants.PAYPAL_PAYMENT);
				if(payPalPG != null) {
					pPaymentGroupMapContainer.addPaymentGroup(TRUPropertyNameConstants.PAYPAL_PAYMENT, payPalPG);
					paymentGroupManager.addPaymentGroupToOrder(pOrder, payPalPG);
				}
			} else {
				paymentGroupManager.addPaymentGroupToOrder(pOrder, payPalPG);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::addPayPalPaymentGroupToOrder() : PaypalPG is: {0}", payPalPG);
		}
		if(payPalPG != null) {
			payPalPG.setAmount(payPalAmt);
			if (profileBillingAddress == null) {
				profileBillingAddress = ((TRUPaymentGroupContainerService)pPaymentGroupMapContainer).getBillingAddress();
				if(profileBillingAddress != null) {
					payPalPG.setBillingAddress(profileBillingAddress);
				}
			} else {
				payPalPG.setBillingAddress(profileBillingAddress);
			}
			addRemainingOrderAmountToPaymentGroup(pOrder, payPalPG.getId());
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::addUpdatePayPalPaymentGroupToOrder() : END");
		}
	}

	/**
	 * Removes the pay pal payment group to order.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pPaymentGroupMapContainer
	 *            the payment group map container
	 * @throws CommerceException
	 *             the commerce exception
	 */
	public void createCreditCardPaymentGroupToOrder(Order pOrder, PaymentGroupMapContainer pPaymentGroupMapContainer)
			throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::removePayPalPaymentGroupToOrder() : START");
		}
		if (pOrder == null && pPaymentGroupMapContainer == null) {
			return;
		}
		TRUPaymentGroupManager paymentGroupManager = (TRUPaymentGroupManager) getPaymentGroupManager();
		double payPalAmt = paymentGroupManager.getRemainingOrderAmount(pOrder);
		TRUCreditCard creditCardPG = fetchCreditCardPaymentGroupForOrder(pOrder);
		if (creditCardPG == null) {
			creditCardPG = (TRUCreditCard) pPaymentGroupMapContainer.getPaymentGroup(TRUCommerceConstants.CREDIT_CARD);
			if (creditCardPG == null) {
				creditCardPG = (TRUCreditCard) getPaymentGroupManager().createPaymentGroup(TRUCommerceConstants.CREDIT_CARD);
				if(creditCardPG != null) {
					pPaymentGroupMapContainer.addPaymentGroup(TRUCommerceConstants.CREDIT_CARD, creditCardPG);
					paymentGroupManager.addPaymentGroupToOrder(pOrder, creditCardPG);
				}
			} else {
				paymentGroupManager.addPaymentGroupToOrder(pOrder, creditCardPG);
			}
		}
		if (creditCardPG != null) {
			creditCardPG.setAmount(payPalAmt);
		}
		addRemainingOrderAmountToPaymentGroup(pOrder, creditCardPG.getId());
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::removePayPalPaymentGroupToOrder() : END");
		}
	}

	/**
	 * This method used to generate a set with location ids in the ShippingGroupCommmerceitem Relationships in the order.
	 * 
	 * @param pSgcirs
	 *            the sgcirs
	 * @return the sets the
	 */
	public Set<String> generateLocationIdSet(List<ShippingGroupCommerceItemRelationship> pSgcirs) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::generateLocationIdSet() : START");
		}
		Set<String> locationIdSet = new HashSet<String>();
		String locationIdForS2H = getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome();
		for (ShippingGroupCommerceItemRelationship sgcir : pSgcirs) {

			if (sgcir.getShippingGroup() instanceof TRUHardgoodShippingGroup || 
					sgcir.getShippingGroup() instanceof TRUChannelHardgoodShippingGroup) {

				if (!locationIdSet.contains(locationIdForS2H)) {
					locationIdSet.add(locationIdForS2H);
				}

			} else if (sgcir.getShippingGroup() instanceof TRUInStorePickupShippingGroup) {
				TRUInStorePickupShippingGroup storePickupShippingGroup = (TRUInStorePickupShippingGroup) sgcir.getShippingGroup();
				if (!locationIdSet.contains(storePickupShippingGroup.getLocationId())) {
					locationIdSet.add(storePickupShippingGroup.getLocationId());
				}
			}

			else if (sgcir.getShippingGroup() instanceof TRUChannelInStorePickupShippingGroup) {
				TRUChannelInStorePickupShippingGroup channelStorePickupShippingGroup = (TRUChannelInStorePickupShippingGroup) sgcir
						.getShippingGroup();
				if (!locationIdSet.contains(channelStorePickupShippingGroup.getLocationId())) {
					locationIdSet.add(channelStorePickupShippingGroup.getLocationId());
				}
			}

		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::generateLocationIdSet() : END");
		}
		return locationIdSet;
	}

	/**
	 * This method is used to get the relationships for that location.
	 * 
	 * @param pLocationId
	 *            the location id
	 * @param pSgcirs
	 *            the sgcirs
	 * @return the relations ships for the location
	 */
	public Set<ShippingGroupCommerceItemRelationship> getRelationsShipsForTheLocation(String pLocationId,
			List<ShippingGroupCommerceItemRelationship> pSgcirs) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getRelationsShipsForTheLocation() : START");
		}
		HashSet<ShippingGroupCommerceItemRelationship> sgSet = new LinkedHashSet<ShippingGroupCommerceItemRelationship>();
		for (ShippingGroupCommerceItemRelationship sgcir : pSgcirs) {
			ShippingGroup sg = sgcir.getShippingGroup();
			if (sg instanceof HardgoodShippingGroup	&&
					pLocationId.equals(getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome())) {
				sgSet.add(sgcir);
			} else if (sg instanceof TRUInStorePickupShippingGroup &&
					pLocationId.equals(((TRUInStorePickupShippingGroup) sg).getLocationId())) {
				sgSet.add(sgcir);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getRelationsShipsForTheLocation() : END");
		}
		return sgSet;

	}

	/**
	 * This method is used to get the allowed inventory quantity for that item based on location.
	 * 
	 * @param pSkuId
	 *            the sku id
	 * @param pLocationId
	 *            the location id
	 * @return the allowed quantity
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public long getAllowedQuantity(String pSkuId, String pLocationId) throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getAllowedQuantity() : START");
		}
		long inventoryQty = TRUCommerceConstants.LONG_ZERO;
		int invenotryStatus = getShoppingCartUtils().getInventoryStatus(pSkuId, pLocationId);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getAllowedQuantity() : invenotryStatus : {0}", invenotryStatus);
		}
		if (invenotryStatus == TRUCommerceConstants.INT_IN_STOCK) {

			inventoryQty = getShoppingCartUtils().getItemInventory(pSkuId, pLocationId, invenotryStatus);

		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getAllowedQuantity() : inventoryQty : {0}", inventoryQty);
			vlogDebug("@Class::TRUOrderManager::@method::getAllowedQuantity() : END");
		}
		return inventoryQty;

	}

	/**
	 * If Show me gift Options value is false, clear all Gift Options details.
	 * 
	 * @param pOrder
	 *            the order
	 * @throws CommerceException
	 *             the commerce exception
	 */
	@SuppressWarnings("unchecked")
	public void clearAnyGiftOptions(Order pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::clearAnyGiftOptions() : START");
		}
		List<String> wrapItemIds = new ArrayList<String>();
		// Clear all Gift Options details
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		if (shippingGroups != null && !shippingGroups.isEmpty()) {
			for (ShippingGroup shippingGroup : shippingGroups) {
				synchronized (shippingGroup) {
					if (shippingGroup instanceof TRUHardgoodShippingGroup || 
							shippingGroup instanceof TRUChannelHardgoodShippingGroup) {

						if (shippingGroup instanceof TRUHardgoodShippingGroup) {
							((TRUHardgoodShippingGroup) shippingGroup).setGiftReceipt(false);
							((TRUHardgoodShippingGroup) shippingGroup).setGiftWrapMessage(null);
						} else if (shippingGroup instanceof TRUChannelHardgoodShippingGroup) {
							((TRUChannelHardgoodShippingGroup) shippingGroup).setGiftReceipt(false);
							((TRUChannelHardgoodShippingGroup) shippingGroup).setGiftWrapMessage(null);
						}

						List<CommerceItemRelationship> ciRels = shippingGroup.getCommerceItemRelationships();
						for (CommerceItemRelationship commerceItemRelationship : ciRels) {
							TRUShippingGroupCommerceItemRelationship truSgCiR = (TRUShippingGroupCommerceItemRelationship) commerceItemRelationship;
							if (truSgCiR.getIsGiftItem()) {
								List<RepositoryItem> getGiftItemInfo = truSgCiR.getGiftItemInfo();
								if (getGiftItemInfo != null && !getGiftItemInfo.isEmpty()) {
									for (RepositoryItem giftItem : getGiftItemInfo) {
										if (giftItem.getPropertyValue(getPropertyManager().getGiftWrapCommItemId()) != null) {
											wrapItemIds.add((String) giftItem.getPropertyValue(getPropertyManager()
													.getGiftWrapCommItemId()));
										}
									}
								}
								// Remove the Gift Wrap commerceItem and assign null
								truSgCiR.setIsGiftItem(false);
								truSgCiR.setGiftItemInfo(null);
							}
						}
					}
				}
			}
		}
		for (String giftWrapItemId : wrapItemIds) {
			getCommerceItemManager().removeItemFromOrder(pOrder, giftWrapItemId);
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUOrderManager::@method::clearAnyGiftOptions() : Removed giftWrapItemId :{0}", giftWrapItemId);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::clearAnyGiftOptions() : END");
		}
	}

	/**
	 * Gets the site repository.
	 * 
	 * @return the mSiteRepository
	 */

	public Repository getSiteRepository() {
		return mSiteRepository;
	}

	/**
	 * Sets the site repository.
	 * 
	 * @param pSiteRepository
	 *            the mSiteRepository to set
	 */

	public void setSiteRepository(Repository pSiteRepository) {
		this.mSiteRepository = pSiteRepository;
	}

	/**
	 * Gets the property manager.
	 * 
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 * 
	 * @param pPropertyManager
	 *            the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * This method will set PromotionTools with pPromotionTools.
	 * 
	 * @param pPromotionTools
	 *            -Promotion Tools.
	 */
	public void setPromotionTools(PromotionTools pPromotionTools) {
		this.mPromotionTools = pPromotionTools;
	}

	/**
	 * This property contains PromotionTools object.
	 * 
	 * @return PromotionTools
	 */
	public PromotionTools getPromotionTools() {
		return this.mPromotionTools;
	}

	/**
	 * This property contains ClaimableManager object.
	 * 
	 * @return ClaimableManager
	 */
	public ClaimableManager getClaimableManager() {
		return mClaimableManager;
	}

	/**
	 * This method will set ClaimableManager with pClaimableManager.
	 * 
	 * 
	 * @param pClaimableManager
	 *            the ClaimableManager to set
	 */
	public void setClaimableManager(ClaimableManager pClaimableManager) {
		mClaimableManager = pClaimableManager;
	}

	/** The Shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;

	/**
	 * Gets the shopping cart utils.
	 * 
	 * @return the shopping cart utils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * Sets the shopping cart utils.
	 * 
	 * @param pShoppingCartUtils
	 *            the new shopping cart utils
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		mShoppingCartUtils = pShoppingCartUtils;
	}

	/**
	 * Gets the synchrony.
	 * 
	 * @return Synchrony
	 */
	public Synchrony getSynchrony() {
		return mSynchrony;
	}

	/**
	 * Sets the synchrony.
	 * 
	 * @param pSynchrony
	 *            the mSynchrony to set
	 */
	public void setSynchrony(Synchrony pSynchrony) {
		this.mSynchrony = pSynchrony;
	}

	/**
	 * This method return will the boolean value either true and false. If it is true means cart is eligible for placing order with pay in store.
	 * 
	 * @param pOrder Order
	 * @return isPayInstore boolean
	 */
	public boolean isOrderEligibleForPayInStore(Order pOrder) {
		boolean isPayInstore = Boolean.TRUE;
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.isOrderEligibleForPayInStore method.STARTS");
			vlogDebug("TRUOrderManager.isOrderEligibleForPayInStore ,pOrder:{0}", pOrder);
			vlogDebug("TRUOrderManager.isOrderEligibleForPayInStore ,isPayInstore:{0}", isPayInstore);
		}
		if (pOrder != null) {
			List<TRUCommerceItemImpl> commItems = pOrder.getCommerceItems();
			List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
			List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
			Date currentDate = new Date();
			for (CommerceItem commerceItem : commItems) {
				//Start :: Commented out for defect TUW-55599
				/*if (commerceItem instanceof TRUDonationCommerceItem) {
					isPayInstore = Boolean.FALSE;
					break;
				}else */
				//End :: Commented out for defect TUW-55599
				if(commerceItem instanceof TRUCommerceItemImpl){
					//TUW-50518 : Start changes. 
					RepositoryItem skuItem = (RepositoryItem) commerceItem.getAuxiliaryData().getCatalogRef();
					Date streetDate = (Date) skuItem.getPropertyValue(getPropertyManager().getStreetDate());
					if (streetDate != null && currentDate.before(streetDate)) {
						isPayInstore = Boolean.FALSE;
						break;
					}
					//TUW-50518 : End changes. 
				}
			}
			for (ShippingGroup sg : shippingGroups) {
				if (sg instanceof TRUInStorePickupShippingGroup) {
					isPayInstore = Boolean.FALSE;
					break;
				}
			}
			for (PaymentGroup pg : paymentGroups) {
				if (pg instanceof TRUGiftCard) {
					isPayInstore = Boolean.FALSE;
					break;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.isOrderEligibleForPayInStore method.ENDS");
			vlogDebug("TRUOrderManager.isOrderEligibleForPayInStore ,isPayInstore:{0}", isPayInstore);
		}
		return isPayInstore;
	}

	/**
	 * This method check if order has payment group - inStorePayment.
	 * 
	 * @param pOrder Order
	 * @return inStorePayment boolean
	 */
	public boolean isPayGroupInStorePayment(Order pOrder) {
		boolean inStorePayment = Boolean.FALSE;
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.isPayGroupInStorePayment method.STARTS");
			vlogDebug("TRUOrderManager.isPayGroupInStorePayment ,pOrder:{0}", pOrder);
			vlogDebug("TRUOrderManager.isPayGroupInStorePayment ,inStorePayment:{0}", inStorePayment);
		}
		if (pOrder != null) {
			List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
			for (PaymentGroup pg : paymentGroups) {
				if (pg instanceof InStorePayment) {
					inStorePayment = Boolean.TRUE;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.isPayGroupInStorePayment method.ENDS");
			vlogDebug("TRUOrderManager.isPayGroupInStorePayment ,inStorePayment:{0}", inStorePayment);
		}
		return inStorePayment;
	}

	/**
	 * This method check if order has payment group - payPal.
	 * 
	 * @param pOrder Order
	 * @return inStorePayment boolean
	 */
	public boolean isPayGroupPayPal(Order pOrder) {
		boolean payPal = Boolean.FALSE;
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.isPayGroupPayPal method.STARTS");
			vlogDebug("TRUOrderManager.isPayGroupPayPal, pOrder:{0}", pOrder);
			vlogDebug("TRUOrderManager.isPayGroupPayPal, payPal:{0}", payPal);
		}
		if (pOrder != null) {
			List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
			for (PaymentGroup pg : paymentGroups) {
				if (pg instanceof TRUPayPal) {
					payPal = Boolean.TRUE;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.isPayGroupPayPal method.ENDS");
			vlogDebug("TRUOrderManager.isPayGroupPayPal, payPal:{0}", payPal);
		}
		return payPal;
	}

	/**
	 * This method check if order has shipping group - TRUInStorePickupShippingGroup.
	 * 
	 * @param pOrder Order
	 * @return isInStorePickupShippingGroup boolean
	 */
	public boolean isShipGroupInStorePickup(Order pOrder) {
		boolean isInStorePickupShippingGroup = Boolean.FALSE;
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.isShipGroupInStorePickup method.STARTS");
			vlogDebug("TRUOrderManager.isShipGroupInStorePickup ,pOrder:{0}", pOrder);
			vlogDebug("TRUOrderManager.isShipGroupInStorePickup ,isInStorePickupShippingGroup:{0}", isInStorePickupShippingGroup);
		}
		if (pOrder != null) {
			List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
			for (ShippingGroup sg : shippingGroups) {
				if (sg instanceof TRUInStorePickupShippingGroup) {
					isInStorePickupShippingGroup = Boolean.TRUE;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.isInStorePickupShippingGroup method.ENDS");
			vlogDebug("TRUOrderManager.isInStorePickupShippingGroup ,isInStorePickupShippingGroup:{0}",
					isInStorePickupShippingGroup);
		}
		return isInStorePickupShippingGroup;
	}

	
	/**
	 * This method is used to create a new order of type TRULayawayOrderImpl.
	 *
	 * @param pProfileId the profile id
	 * @return TRULayawayOrderImpl
	 * @throws CommerceException the commerce exception
	 */
	public TRULayawayOrderImpl createLayawayOrder(String pProfileId) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.createLayawayOrder() :: method :: STARTS");
		}
		TRUOrderTools orderTools = (TRUOrderTools) getOrderTools();
		TRUPaymentGroupManager paymentGroupManager = (TRUPaymentGroupManager) getPaymentGroupManager();
		TRULayawayOrderImpl layawayOrder = orderTools.createLayawayOrder();

		layawayOrder.setProfileId(pProfileId);
		layawayOrder.setSiteId(SiteContextManager.getCurrentSiteId());
		long now = getCurrentDate().getTime();
		
		layawayOrder.setCreationTime(now);
		layawayOrder.setSubmittedDate(now);

		// Start - adding default credit card to Order
		TRUCreditCard creditCard = (TRUCreditCard) paymentGroupManager.createPaymentGroup();
		paymentGroupManager.addPaymentGroupToOrder(layawayOrder, creditCard);
		addRemainingOrderAmountToPaymentGroup(layawayOrder, creditCard.getId());
		// End - adding default credit card to Order
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.createLayawayOrder() :: methods :: END");
		}
		return layawayOrder;
	}

	/**
	 * This method is used to add layaway order to repository which will be called from ProcAddLayawayOrderToRepository pipeline processor.
	 *
	 * @param pOrder the order
	 * @throws CommerceException the commerce exception
	 */
	@SuppressWarnings("rawtypes")
	public void addLayawayOrder(TRULayawayOrderImpl pOrder) throws CommerceException
	{
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.addLayawayOrder method.STARTS");
		}
		TRUOrderTools orderTools = (TRUOrderTools) getOrderTools();
		MutableRepository mutRep = (MutableRepository) orderTools.getOrderRepository();
		MutableRepositoryItem mutItem = null;
		RepositoryItem returnedItem = null;
		Iterator iter = null;
		CommerceIdentifier ci;
		try {
			for (iter = pOrder.getPaymentGroups().iterator(); iter.hasNext();) {
				ci = (CommerceIdentifier) iter.next();

				mutItem = null;
				if (ci instanceof ChangedProperties) {
					mutItem = ((ChangedProperties) ci).getRepositoryItem();
				}
				if (mutItem == null) {
					mutItem = mutRep
							.getItemForUpdate(ci.getId(), orderTools.getMappedItemDescriptorName(ci.getClass().getName()));

					if (ci instanceof ChangedProperties) {
						((ChangedProperties) ci).setRepositoryItem(mutItem);
					}
				}
				returnedItem = mutRep.addItem(mutItem);
				DynamicBeans.setPropertyValue(ci, TRUCheckoutConstants.PROP_ID, returnedItem.getRepositoryId());
			}

			for (iter = pOrder.getRelationships().iterator(); iter.hasNext();) {
				try {
					ci = (CommerceIdentifier) iter.next();

					mutItem = null;
					if (ci instanceof ChangedProperties) {
						mutItem = ((ChangedProperties) ci).getRepositoryItem();
					}
					if (mutItem == null) {
						mutItem = mutRep.getItemForUpdate(ci.getId(),
								orderTools.getMappedItemDescriptorName(ci.getClass().getName()));

						if (ci instanceof ChangedProperties) {
							((ChangedProperties) ci).setRepositoryItem(mutItem);
						}
					}
					returnedItem = mutRep.addItem(mutItem);
					DynamicBeans.setPropertyValue(ci, TRUCheckoutConstants.PROP_ID, returnedItem.getRepositoryId());
				} catch (RepositoryException e) {
					throw new CommerceException(e);
				} catch (PropertyNotFoundException e) {
					throw new CommerceException(e);
				}
			}

			if (pOrder instanceof ChangedProperties) {
				mutItem = ((ChangedProperties) pOrder).getRepositoryItem();
			}
			if (mutItem == null) {
				mutItem = mutRep.getItemForUpdate(pOrder.getId(),
						orderTools.getMappedItemDescriptorName(pOrder.getClass().getName()));

				if (pOrder instanceof ChangedProperties) {
					((ChangedProperties) pOrder).setRepositoryItem(mutItem);
				}
			}
			returnedItem = mutRep.addItem(mutItem);
			DynamicBeans.setPropertyValue(pOrder, TRUCheckoutConstants.PROP_LAYAWAY_ORDER_ID, returnedItem.getRepositoryId());

			if (isLoggingInfo() && null != pOrder.getId()) {
				vlogInfo("Layaway order placed with id: {0}", pOrder.getId());
			}

		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException in @Class TRUOrderManager.adjustPaymentGroups()", e);
			}
			throw new CommerceException(e);
		} catch (PropertyNotFoundException e) {
			if (isLoggingError()) {
				logError("PropertyNotFoundException in @Class TRUOrderManager.adjustPaymentGroups()", e);
			}
			throw new CommerceException(e);
		}

		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.addLayawayOrder method.ENDS");
		}
		
		updateLayawayOrder(pOrder);
	}
	
	/**
	 * this method updates layaway order.
	 * @param pOrder TRULayawayOrderImpl
	 * @throws CommerceException CommerceException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void updateLayawayOrder(TRULayawayOrderImpl pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.updateLayawayOrder method.STARTS");
		}

		if (pOrder == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(TRUPipeLineConstants.INVALID_ORDER_PARAMETER, MY_RESOURCE_NAME, sResourceBundle));
		}
		
		if (isTransactionMarkedAsRollBack()) {
			if (pOrder instanceof TRULayawayOrderImpl) {
				((TRULayawayOrderImpl) pOrder).invalidateLayawayOrder();
			}
			if (isLoggingDebug()) {
				logDebug("Skipping updateLayawayOrder because transaction marked as roll back");
			}
			return;
		}
		
		Map map = new HashMap(TRUConstants.THIRTEEN);
		map.put(PipelineConstants.ORDERMANAGER, this);
	    map.put(PipelineConstants.ORDER, pOrder);
	    map.put(PipelineConstants.CHANGED, Boolean.FALSE);
	    
	    try
	    {
	      map.put(PipelineConstants.ORDERREPOSITORY, ((TRUOrderTools)getOrderTools()).getOrderRepository());
	      ((TRUOrderTools)getOrderTools()).registerSynchronization(pOrder);
	      getPipelineManager().runProcess(getUpdateLayawayOrderChainId(), map);
	    }
	    catch (RunProcessException e)
	    {
	    	if (isLoggingError()) {
				logError("CommerceException in @Class TRUOrderManager.updateLayawayOrder()", e);
			}
			throw new CommerceException(e);
	    }
	    finally {
	      if ((pOrder instanceof TRULayawayOrderImpl) && (isTransactionMarkedAsRollBack())) {
	        ((TRULayawayOrderImpl)pOrder).invalidateLayawayOrder();
	      }
	    }

		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.updateLayawayOrder method.ENDS");
		}
	}

	/**
	 * adjustPaymentGroups method .
	 *
	 * @param pOrder the order
	 * @return the pipeline result
	 * @throws CommerceException the commerce exception
	 */	
	public PipelineResult adjustPaymentGroups(Order pOrder) throws CommerceException									
	{
		return adjustPaymentGroups(pOrder, getAdjustPaymentGroupsChainId());
	}

	/**
	 * adjustPaymentGroups method .
	 *
	 * @param pOrder the order
	 * @param pAdjustOrderPGsChainId the adjust order p gs chain id
	 * @return the pipeline result
	 * @throws CommerceException the commerce exception
	 */	
	public PipelineResult adjustPaymentGroups(Order pOrder, String pAdjustOrderPGsChainId) throws CommerceException
	{
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.adjustPaymentGroups()**********START**********");
		}
		HashMap map = new HashMap();
		map.put(TRUConstants.ORDER_PARAM, pOrder);
		PipelineResult result;
		try {
			result = getPipelineManager().runProcess(pAdjustOrderPGsChainId, map);
		} catch (RunProcessException e) {
			if (isLoggingError()) {
				logError("CommerceException in @Class TRUOrderManager.adjustPaymentGroups()", e);
			}
			throw new CommerceException(e);
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.adjustPaymentGroups()**********END**********");
		}
		return result;
	}

	/**
	 * @return the messageContentRepository
	 */
	public Repository getMessageContentRepository() {
		return mMessageContentRepository;
	}

	/**
	 * @param pMessageContentRepository the messageContentRepository to set
	 */
	public void setMessageContentRepository(Repository pMessageContentRepository) {
		mMessageContentRepository = pMessageContentRepository;
	}

	/**
	 * @return the transactionManager
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	/**
	 * @param pTransactionManager the transactionManager to set
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}


	/**
	 * Verifies the given credit card is exists in order.
	 *
	 * @param pOrder the order
	 * @param pCreditCard the credit card
	 * @return true, if is credit card exists in order
	 */
	public boolean isCreditCardExistsInOrder(Order pOrder, CreditCard pCreditCard) {
		if (isLoggingDebug()) {
			vlogDebug("@Class :: TRUOrderManager.isCreditCardExistsInOrder() :: method :: START");
		}
		TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
		TRUOrderTools orderTools = (TRUOrderTools) getOrderTools();
		TRUConfiguration configuration = orderTools.getConfiguration();

		TRUOrderImpl order = (TRUOrderImpl) pOrder;
		TRUCreditCard creditCard = (TRUCreditCard) pCreditCard;
		if (creditCard == null || order == null) {
			return Boolean.FALSE;
		}

		String creditCardNumber = creditCard.getCreditCardNumber();
		String expirationMonth = creditCard.getExpirationMonth();
		String expirationYear = creditCard.getExpirationYear();

		TRUCreditCard orderCreditCard = (TRUCreditCard) orderManager.getCreditCard(order);
		if (orderCreditCard != null && StringUtils.isNotBlank(orderCreditCard.getCreditCardNumber()) &&
				StringUtils.isNotBlank(orderCreditCard.getExpirationMonth()) &&
				StringUtils.isNotBlank(orderCreditCard.getExpirationYear())) {

			String orderCreditCardNumber = orderCreditCard.getCreditCardNumber();
			String orderCreditCardExpirationMonth = orderCreditCard.getExpirationMonth();
			String orderCreditCardExpirationYear = orderCreditCard.getExpirationYear();

			boolean matched = Boolean.TRUE;
			if (!orderCreditCardNumber.equalsIgnoreCase(creditCardNumber)) {
				matched = Boolean.FALSE;
			}
			if (matched && !orderCreditCardExpirationMonth.equalsIgnoreCase(expirationMonth)) {
				matched = Boolean.FALSE;
			}
			if (matched && !orderCreditCardExpirationYear.equalsIgnoreCase(expirationYear)) {
				matched = Boolean.FALSE;
			}
			if (matched) {
				try {
					Address billingAddress = creditCard.getBillingAddress();
					Address orderCreditCardBillingAddress = orderCreditCard.getBillingAddress();
					matched = AddressTools.compareObjects(orderCreditCardBillingAddress, billingAddress,
							configuration.getAddressFieldProperties(), null);
				} catch (IntrospectionException e) {
					matched = Boolean.FALSE;
					if(isLoggingError()){
						logError("IntrospectionException in @Class :: TRUOrderManager.isCreditCardExistsInOrder() :: method ::",e);
					}
				}
			}
			return matched;
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class :: TRUOrderManager.isCreditCardExistsInOrder() :: method :: END");
		}
		return Boolean.FALSE;
	}

	/**
	 * Update registry item.
	 *
	 * @param pOrderHolder the order holder
	 */
	public void updateRegistryItem(OrderHolder pOrderHolder) {
		if (isLoggingDebug()) {
			vlogDebug("@Class :: TRUOrderManager.updateRegistryItem() :: method :: START");
		}
		TRURegistryServiceProcessor registryServiceProcessor = getRegistryServiceProcessor();
		TRUOrderHolder orderHolder = (TRUOrderHolder) pOrderHolder;

		Order lastOrder = orderHolder.getLast();
		if (lastOrder != null) {
			Map<String, List<ShippingGroup>> registryDetails = getRegistryDetailsFromOrder(lastOrder);
			if(registryDetails != null && !registryDetails.isEmpty() && true) {
				for (String channelId : registryDetails.keySet()) {
					registryServiceProcessor.updateRegistryItemByShippingGroups(lastOrder, registryDetails.get(channelId));
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class :: TRUOrderManager.updateRegistryItem() :: method :: END");
		}
	}

	/**
	 * Gets the registry details from order.
	 *
	 * @param pOrder - TRUOrderManager
	 * @return the registry details from order
	 */
	private Map<String, List<ShippingGroup>> getRegistryDetailsFromOrder(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("@Class :: TRUOrderManager.updateRegistryItem() :: method :: START");
		}
		Map<String, List<ShippingGroup>> registryDetails = new HashMap<String, List<ShippingGroup>>();
		String channelId = null;
		if (pOrder != null) {
			List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
			if (shippingGroups != null && !shippingGroups.isEmpty()) {
				for (ShippingGroup shippingGroup : shippingGroups) {
					channelId = null;
					if(shippingGroup instanceof TRUChannelHardgoodShippingGroup){
						channelId = ((TRUChannelHardgoodShippingGroup)shippingGroup).getChannelId();
					} else if(shippingGroup instanceof TRUChannelInStorePickupShippingGroup){
						channelId = ((TRUChannelInStorePickupShippingGroup)shippingGroup).getChannelId();
					}
					if(channelId == null) {
						continue;
					}
					if(registryDetails.get(channelId) == null) {
						List<ShippingGroup> list = new ArrayList<ShippingGroup>();
						list.add(shippingGroup);
						registryDetails.put(channelId, list);
					} else {
						registryDetails.get(channelId).add(shippingGroup);
					}
				}
			}
			
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class :: TRUOrderManager.updateRegistryItem() :: method :: END");
		}
		return registryDetails;
	}
	
	/**
	 * This method is used to remove all gift card payment groups from order.
	 * @param pOrder Order
	 * @throws CommerceException CommerceException
	 * @throws RepositoryException RepositoryException
	 */
	@SuppressWarnings("unchecked")
	public void removeAllGiftCardFromOrder(Order pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class :: TRUOrderManager.removeAllGiftCardFromOrder() :: method :: START");
		}
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		for (PaymentGroup pg : paymentGroups) {
			if (pg instanceof TRUGiftCard) {
				List<Relationship> paymentRelationships = pOrder.getPaymentGroupRelationships();
				for(Relationship paymentRelationship : paymentRelationships) {
					if (paymentRelationship instanceof PaymentGroupOrderRelationship) {
						String relpayId = ((PaymentGroupOrderRelationship)paymentRelationship).getPaymentGroup().getId();
						if (relpayId.equals(pg.getId())) {
							pOrder.removeRelationship(paymentRelationship.getId());
							pOrder.removePaymentGroup(pg.getId());
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class :: TRUOrderManager.removeAllGiftCardFromOrder() :: method :: END");
		}
	}

	/**
	 * Gets the gift card max limit.
	 *
	 * @return the gift card max limit
	 * @throws RepositoryException the repository exception
	 */
	public int getGiftCardMaxLimit() throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getGiftCardMaxLimit() : START");
		}
		int giftCardMaxLimit = 0;
		if (SiteContextManager.getCurrentSite() != null) {
			giftCardMaxLimit = (int)SiteContextManager.getCurrentSite().getPropertyValue(getPropertyManager().getMaxGiftCardLimitPropertyName());
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getGiftCardMaxLimit() : END");
		}
		return giftCardMaxLimit;
	}
	
	/**
	 * Ensure in store payment group.
	 *
	 * @param pOrder the order
	 * @throws CommerceException the commerce exception
	 */
	@SuppressWarnings("unchecked")
	public void ensureInStorePaymentGroup(Order pOrder) throws CommerceException{
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::ensureInStorePaymentGroup() : START");
		}
		// Check for InstorePayment Group is exist or not
		PaymentGroup inStorePaymentGroup = null;
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		for (PaymentGroup paymentGroup : paymentGroups) {
			if (paymentGroup instanceof InStorePayment) {
				inStorePaymentGroup = paymentGroup;
				break;
			}
		}
		if(inStorePaymentGroup != null) {
			boolean payInStoreEligible = ((TRUOrderManager) getOrderManager()).isOrderEligibleForPayInStore(pOrder);
			//Remove Pay In Store Payment Group if it is not eligible
			if(!payInStoreEligible) {
				getPaymentGroupManager().removePaymentGroupFromOrder(pOrder, inStorePaymentGroup.getId());
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::ensureInStorePaymentGroup() : END");
		}
	}

	/**
	 * Update instore shipping group pick up info.
	 *
	 * @param pOldShippingGroup the old shipping group
	 * @param pCurrentShippingGroup the current shipping group
	 */
	public void updateInstoreShippingGroupPickUpInfo(
			TRUInStorePickupShippingGroup pOldShippingGroup,
			TRUInStorePickupShippingGroup pCurrentShippingGroup) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::updateInstoreShippingGroupPickUpInfo() : START");
		}
		if (pOldShippingGroup == null || pCurrentShippingGroup == null ) {
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUOrderManager::@method::updateInstoreShippingGroupPickUpInfo() : END with null");
			}
			return;
		}
		if(StringUtils.isBlank(pCurrentShippingGroup.getEmail()) && StringUtils.isNotBlank(pOldShippingGroup.getEmail())) {
			pCurrentShippingGroup.setFirstName(pOldShippingGroup.getFirstName());
			pCurrentShippingGroup.setLastName(pOldShippingGroup.getLastName());
			pCurrentShippingGroup.setEmail(pOldShippingGroup.getEmail());
			pCurrentShippingGroup.setPhoneNumber(pOldShippingGroup.getPhoneNumber());
		}
		if(pOldShippingGroup.isAltAddrRequired() && StringUtils.isBlank(pCurrentShippingGroup.getAltEmail()) && 
				StringUtils.isNotBlank(pOldShippingGroup.getAltEmail())) {
			pCurrentShippingGroup.setAltAddrRequired(pOldShippingGroup.isAltAddrRequired());
			pCurrentShippingGroup.setAltFirstName(pOldShippingGroup.getAltFirstName());
			pCurrentShippingGroup.setAltLastName(pOldShippingGroup.getAltLastName());
			pCurrentShippingGroup.setAltEmail(pOldShippingGroup.getAltEmail());
			pCurrentShippingGroup.setAltPhoneNumber(pOldShippingGroup.getAltPhoneNumber());
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::updateInstoreShippingGroupPickUpInfo() : END");
		}
	}
	
	/*@SuppressWarnings("unchecked")
	public TRUHardgoodShippingGroup findOrAddShippingGroupByNicknameAndShipMethod(Order pOrder, String pNickName, String pShippingMethod,ShippingGroupMapContainer pShippingGroupMapContainer){
		TRUHardgoodShippingGroup hardgoodShippingGroup=getShipGroupByNickNameAndShipMethod(pOrder, pNickName, pShippingMethod);
		if(hardgoodShippingGroup==null){
		// Create new shipping group 
			hardgoodShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupManager().createShippingGroup();
			getShippingGroupManager().addShippingGroupToOrder(pOrder, hardgoodShippingGroup);			
			TRUHardgoodShippingGroup containerShippingGroup = (TRUHardgoodShippingGroup) pShippingGroupMapContainer.getShippingGroup(pNickName);
			if (containerShippingGroup != null) {
				OrderTools.copyAddress(containerShippingGroup.getShippingAddress(), hardgoodShippingGroup.getShippingAddress());
			}
			hardgoodShippingGroup.setNickName(pNickName);
		}		
		return hardgoodShippingGroup;
	}*/
	/**
	 * Update billing address to SDP Object.
	 *
	 * @param pBean the SDP Bean
	 * @param pOrder the current order
	 */
	private void updateBillingOrShippingAddress(SynchronySDPRequestBean pBean, Order pOrder){
		Address shippingAddress = null;		
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
				if(!shippingGroups.isEmpty()) {                    
		             for (ShippingGroup shippingGroup : shippingGroups) {
								if(shippingGroup instanceof TRUHardgoodShippingGroup){
									TRUHardgoodShippingGroup sg = (TRUHardgoodShippingGroup) shippingGroup;
									shippingAddress = sg.getShippingAddress();
									break;
								}
		             		}
					}
		
		Address billingAddress = null;
		TRUUserSession sessionComponent= (TRUUserSession)ServletUtil.getCurrentRequest().resolveName(TRU_SESSION_SCOPE_COMPONENT_NAME);
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		for (PaymentGroup pg : paymentGroups) {
			if((pg instanceof CreditCard) && (((CreditCard) pg).getBillingAddress().getAddress1() != null)){
				billingAddress = ((CreditCard) pg).getBillingAddress();
			}
		}
		if (billingAddress != null) {
			pBean.setFirstName(billingAddress.getFirstName());
			if (billingAddress.getMiddleName() == null) {
				pBean.setMiddleInit(TRUCheckoutConstants.EMPTY_STRING);
			} else {
				pBean.setMiddleInit(billingAddress.getMiddleName());
			}
			pBean.setLastName(billingAddress.getLastName());
			pBean.setHomeAddress(billingAddress.getAddress1());
			if(billingAddress.getAddress2() == null){
				pBean.setHomeAddress2(TRUCheckoutConstants.EMPTY_STRING);
			} else {
				pBean.setHomeAddress2(billingAddress.getAddress2());
			}
			pBean.setCity(billingAddress.getCity());
			pBean.setState(billingAddress.getState());
			if(billingAddress.getPostalCode() == null){
				pBean.setZipCode(TRUCheckoutConstants.EMPTY_STRING);
			} else {
				pBean.setZipCode(billingAddress.getPostalCode().substring(TRUCommerceConstants.INT_ZERO, TRUCommerceConstants.INT_FIVE));
			}
		} else if(billingAddress == null && sessionComponent.isBillingAddressSameAsShippingAddress() && shippingAddress != null){
			pBean.setFirstName(shippingAddress.getFirstName());
			if (shippingAddress.getMiddleName() == null) {
				pBean.setMiddleInit(TRUCheckoutConstants.EMPTY_STRING);
			} else {
				pBean.setMiddleInit(shippingAddress.getMiddleName());
			}
			pBean.setLastName(shippingAddress.getLastName());
			pBean.setHomeAddress(shippingAddress.getAddress1());
			if(shippingAddress.getAddress2() == null){
				pBean.setHomeAddress2(TRUCheckoutConstants.EMPTY_STRING);
			} else {
				pBean.setHomeAddress2(shippingAddress.getAddress2());
			}
			pBean.setCity(shippingAddress.getCity());
			pBean.setState(shippingAddress.getState());
			if(shippingAddress.getPostalCode() == null){
				pBean.setZipCode(TRUCheckoutConstants.EMPTY_STRING);
			} else {
				pBean.setZipCode(shippingAddress.getPostalCode().substring(TRUCommerceConstants.INT_ZERO, TRUCommerceConstants.INT_FIVE));
			}
		} else {
			pBean.setFirstName(TRUCheckoutConstants.EMPTY_STRING);
			pBean.setMiddleInit(TRUCheckoutConstants.EMPTY_STRING);
			pBean.setLastName(TRUCheckoutConstants.EMPTY_STRING);
			pBean.setHomeAddress(TRUCheckoutConstants.EMPTY_STRING);
			pBean.setHomeAddress2(TRUCheckoutConstants.EMPTY_STRING);
			pBean.setCity(TRUCheckoutConstants.EMPTY_STRING);
			pBean.setState(TRUCheckoutConstants.EMPTY_STRING);
			pBean.setZipCode(TRUCheckoutConstants.EMPTY_STRING);
		}
	}
	
	
	/**
	 * Gets the ship group by nick name and ship method on change of the shipping address in multi ship page.
	 *
	 * @param pOrder the order
	 * @param pNickName the nick name
	 * @param pOldShippingGroup the old shipping group
	 * @return the ship group by nick name and ship method
	 */
	public TRUHardgoodShippingGroup getShipGroupByNickNameAndShipMethod(Order pOrder, String pNickName, TRUHardgoodShippingGroup pOldShippingGroup) {
		String shippingMethod = pOldShippingGroup.getShippingMethod();
		String channelId = TRUConstants.EMPTY ;
		String channelType = TRUConstants.EMPTY ;
		String shippingGrpTypeClass = pOldShippingGroup.getShippingGroupClassType();
		if(pOldShippingGroup instanceof TRUChannelHardgoodShippingGroup){
			channelType = ((TRUChannelHardgoodShippingGroup)pOldShippingGroup).getChannelType() ;
			channelId = ((TRUChannelHardgoodShippingGroup)pOldShippingGroup).getChannelId() ;
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getShipGroupByNickNameAndShipMethod() : START");
			vlogDebug("INPUT PARAMS: getShipGroupByNickNameAndShipMethod pOrder: {0} pNickName: {1} pShippingMethod: {2} ", pOrder,
					pNickName, shippingMethod);
		}
		if (pOrder == null || pNickName == null || shippingMethod == null ) {
			return null;
		}
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		for (ShippingGroup sg : shippingGroups) {	
			if (sg instanceof TRUChannelHardgoodShippingGroup) {
				TRUChannelHardgoodShippingGroup hardgoodShippingGroup = (TRUChannelHardgoodShippingGroup) sg;
				String nickName=hardgoodShippingGroup.getNickName();
				if (channelType.equalsIgnoreCase(hardgoodShippingGroup.getChannelId()) &&
						channelId.equalsIgnoreCase(hardgoodShippingGroup.getChannelId()) &&
						StringUtils.isNotBlank(nickName) && nickName.equalsIgnoreCase(pNickName)&&
						hardgoodShippingGroup.getShippingMethod().equalsIgnoreCase(shippingMethod)) {
					return hardgoodShippingGroup;
				}
			}else if (sg instanceof TRUHardgoodShippingGroup) {
				TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) sg;
				String nickName=hardgoodShippingGroup.getNickName();
				if (StringUtils.isNotBlank(nickName) && nickName.equalsIgnoreCase(pNickName)			&&
						hardgoodShippingGroup.getShippingMethod().equalsIgnoreCase(shippingMethod) &&
						shippingGrpTypeClass.equalsIgnoreCase(hardgoodShippingGroup.getShippingGroupClassType())) {
					return hardgoodShippingGroup;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getShipGroupByNickNameAndShipMethod() : END");
		}
		return null;
	}

	
	
	/**
	 * <p>
	 * This method will be invoked when change ship method triggered on multi ship page and searches for any matched shipping group based on the below properties.
	 * <li>ShippingGroupClassType</li>
	 * <li>channelType</li>
	 * <li>channelId</li>
	 * <li>pSelectedShipMethod</li>
	 * </p>
	 * If any matched shipping group found based on above criteria that will be returned.
	 * @param pOrder the order
	 * @param pOldShippingGroup the old shipping group
	 * @param pSelectedShipMethod the selected ship method
	 * @return the ship group by nick name and ship method
	 */
	public TRUHardgoodShippingGroup getShipGroupByNickNameAndShipMethod(Order pOrder, TRUHardgoodShippingGroup pOldShippingGroup,String pSelectedShipMethod) {
		String channelId = TRUConstants.EMPTY ;
		String channelType = TRUConstants.EMPTY ;
		String selectedNickName = pOldShippingGroup.getNickName();
		String shippingGrpTypeClass = pOldShippingGroup.getShippingGroupClassType();
		String shippingGrpNickName = TRUConstants.EMPTY;
		if(pOldShippingGroup instanceof TRUChannelHardgoodShippingGroup){
			channelType = ((TRUChannelHardgoodShippingGroup)pOldShippingGroup).getChannelType() ;
			channelId = ((TRUChannelHardgoodShippingGroup)pOldShippingGroup).getChannelId() ;
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getShipGroupByNickNameAndShipMethod() : START");
			vlogDebug("INPUT PARAMS: getShipGroupByNickNameAndShipMethod pOrder: {0} pShippingMethod: {1} ", pOrder,
					pSelectedShipMethod);
		}
		if (pOrder == null || pSelectedShipMethod == null ) {
			return null;
		}
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		for (ShippingGroup sg : shippingGroups) {	
			if (sg instanceof TRUChannelHardgoodShippingGroup) {
				TRUChannelHardgoodShippingGroup hardgoodShippingGroup = (TRUChannelHardgoodShippingGroup) sg;
				shippingGrpNickName=hardgoodShippingGroup.getNickName();
				if (channelType.equalsIgnoreCase(hardgoodShippingGroup.getChannelType()) &&
						channelId.equalsIgnoreCase(hardgoodShippingGroup.getChannelId()) &&
						StringUtils.isNotBlank(shippingGrpNickName) && shippingGrpNickName.equalsIgnoreCase(selectedNickName)&&
						hardgoodShippingGroup.getShippingMethod().equalsIgnoreCase(pSelectedShipMethod)) {
					return hardgoodShippingGroup;
				}
			}else if (sg instanceof TRUHardgoodShippingGroup) {
				TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) sg;
				shippingGrpNickName=hardgoodShippingGroup.getNickName();
				if (StringUtils.isBlank(shippingGrpNickName)){
					if (hardgoodShippingGroup.getShippingMethod().equalsIgnoreCase(pSelectedShipMethod) &&
							shippingGrpTypeClass.equalsIgnoreCase(hardgoodShippingGroup.getShippingGroupClassType())) {
						return hardgoodShippingGroup;
					}
				}else{
					if (shippingGrpNickName.equalsIgnoreCase(selectedNickName)			&&
							hardgoodShippingGroup.getShippingMethod().equalsIgnoreCase(pSelectedShipMethod) &&
							shippingGrpTypeClass.equalsIgnoreCase(hardgoodShippingGroup.getShippingGroupClassType())) {
						return hardgoodShippingGroup;
					}
				}
				
			}
			shippingGrpNickName = TRUConstants.EMPTY;
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getShipGroupByNickNameAndShipMethod() : END");
		}
		return null;
	}
	/**
	 * The method is overridden to save the custom item info like out of stock and BPP item.
	 * 
	 *
	 * @param pOrder the order
	 * @throws CommerceException the commerce exception
	 */
	@Override
	public void addOrder(Order pOrder) throws CommerceException {
		if(isLoggingDebug()){
			logDebug("@Class::TRUOrderManager::@method::addOrder() : START");
		}
		if (pOrder != null && pOrder.isTransient()) {
			OrderTools orderTools = getOrderTools();
			MutableRepository mutRep = (MutableRepository) orderTools.getOrderRepository();
			saveBPPItemInfo(pOrder, orderTools, mutRep);
			saveOutOfStockItem(pOrder, orderTools, mutRep);
		}
		super.addOrder(pOrder);
		if(isLoggingDebug()){
			logDebug("@Class::TRUOrderManager::@method::addOrder() : END");
		}
	}

	/**
	 * Save bpp item info.
	 *
	 * @param pOrder the order
	 * @param pOrderTools the order tools
	 * @param pMutRep the mut rep
	 * @throws CommerceException the commerce exception
	 */
	public void saveBPPItemInfo(Order pOrder, OrderTools pOrderTools, MutableRepository pMutRep) throws CommerceException {
		if(isLoggingDebug()){
			logDebug("@Class::TRUOrderManager::@method::saveBPPItemInfo() : START");
		}
		MutableRepositoryItem mutItem;
		CommerceIdentifier ci = null;
		List<TRUShippingGroupCommerceItemRelationship> sgCiRels = new ArrayList<TRUShippingGroupCommerceItemRelationship>();
		List<CommerceItem> cItems = pOrder.getCommerceItems();
		for (CommerceItem cItem : cItems) {
			sgCiRels.addAll(cItem.getShippingGroupRelationships());
		}
		if ((sgCiRels != null && !sgCiRels.isEmpty())) {
			for (TRUShippingGroupCommerceItemRelationship sgciRel : sgCiRels) {
				mutItem = null;
				try {
					if (null != sgciRel.getBppItemInfo().getRepositoryItem() && null != sgciRel.getBppItemInfo().getRepositoryItem().getRepositoryId()) {
						ci = (CommerceIdentifier) sgciRel.getBppItemInfo();
						if (ci instanceof ChangedProperties) {
							mutItem = ((ChangedProperties) ci).getRepositoryItem();
						}
						if (mutItem == null) {
							mutItem = pMutRep
									.getItemForUpdate(ci.getId(), pOrderTools.getMappedItemDescriptorName(ci.getClass().getName()));

							if (ci instanceof ChangedProperties) {
								((ChangedProperties) ci).setRepositoryItem(mutItem);
							}
						}
						RepositoryItem returnedItem = pMutRep.addItem(mutItem);
						if(isLoggingDebug()){
							logDebug("@Class::TRUOrderManager::@method::saveBPPItemInfo() : Returned Item is: " +returnedItem);
						}
						DynamicBeans.setPropertyValue(ci, TRUCommerceConstants.ID, returnedItem.getRepositoryId());
					}
				} catch (RepositoryException e) {
					throw new CommerceException(e);
				} catch (PropertyNotFoundException e) {
					throw new CommerceException(e);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("@Class::TRUOrderManager::@method::saveBPPItemInfo() : END");
		}
	}

	/**
	 * Save out of stock item.
	 *
	 * @param pOrder the order
	 * @param pOrderTools the order tools
	 * @param pMutRep the mut rep
	 * @throws CommerceException the commerce exception
	 */
	public void saveOutOfStockItem(Order pOrder, OrderTools pOrderTools, MutableRepository pMutRep) throws CommerceException {
		if(isLoggingDebug()){
			logDebug("@Class::TRUOrderManager::@method::saveOutOfStockItem() : START");
		}
		MutableRepositoryItem mutItem;
		CommerceIdentifier ci = null;
		List<TRUOutOfStockItem> outOfStockItems = ((TRUOrderImpl) pOrder).getOutOfStockItems();
		if (null != outOfStockItems && !outOfStockItems.isEmpty()) {
			for (TRUOutOfStockItem outOfStockItem : outOfStockItems) {
				try {
					ci = (CommerceIdentifier) outOfStockItem;

					mutItem = null;
					if (ci instanceof ChangedProperties) {
						mutItem = ((ChangedProperties) ci).getRepositoryItem();
					}
					if (mutItem == null) {
						mutItem = pMutRep.getItemForUpdate(ci.getId(), pOrderTools.getMappedItemDescriptorName(ci.getClass().getName()));

						if (ci instanceof ChangedProperties) {
							((ChangedProperties) ci).setRepositoryItem(mutItem);
						}
					}
					RepositoryItem returnedItem = pMutRep.addItem(mutItem);
					if(isLoggingDebug()){
						logDebug("@Class::TRUOrderManager::@method::saveOutOfStockItem() : Returned Item is: "+returnedItem);
					}
					DynamicBeans.setPropertyValue(ci, TRUCommerceConstants.ID, returnedItem.getRepositoryId());
				} catch (RepositoryException e) {
					throw new CommerceException(e);
				} catch (PropertyNotFoundException e) {
					throw new CommerceException(e);

				}
			}
		}
		if(isLoggingDebug()){
			logDebug("@Class::TRUOrderManager::@method::saveOutOfStockItem() : END");
		}
	}
	
	/**
	 * This method generates nick name for billing address if during edit/add credit card billing address nickname is already present.
	 * @param pAddressInputFields ContactInfo
	 * @param pShippingGroups Map<String, ShippingGroup>
	 * @return generatedNickName String
	 */
	public String generateUniqueNickNameForAddress(ContactInfo pAddressInputFields, Map<String, ShippingGroup> pShippingGroups){
		String generatedNickName = TRUCommerceConstants.EMPTY_STRING;
		String firstName = pAddressInputFields.getFirstName();
		String lastName = pAddressInputFields.getLastName();
		String customNickName = firstName + TRUConstants.WHITE_SPACE + lastName;
		Set<String> addressNickNames = new HashSet<String>();	
		for (String nickName : pShippingGroups.keySet()) {
			ShippingGroup shippingGroup = pShippingGroups.get(nickName);
			if (shippingGroup instanceof TRUHardgoodShippingGroup && !(shippingGroup instanceof TRUChannelHardgoodShippingGroup)) {
				TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
				if (hardgoodShippingGroup != null && hardgoodShippingGroup.getNickName() != null){
					addressNickNames.add(hardgoodShippingGroup.getNickName());
				}
			}
		}
		TRUProfileTools profileTools = (TRUProfileTools) getOrderTools().getProfileTools();
		generatedNickName = profileTools.getUniqueAddressNickname(pAddressInputFields, addressNickNames, customNickName);
		return generatedNickName;
	}
	
	/** The enable transient shipping info. */
	private boolean mEnableTransientShippingInfo;
	
	
	/**
	 * @return the enableTransientShippingInfo
	 */
	public boolean isEnableTransientShippingInfo() {
		return mEnableTransientShippingInfo;
	}

	/**
	 * @param pEnableTransientShippingInfo the enableTransientShippingInfo to set
	 */
	public void setEnableTransientShippingInfo(boolean pEnableTransientShippingInfo) {
		this.mEnableTransientShippingInfo = pEnableTransientShippingInfo;
	}
	
	
	/**
	 * Update order with fraud parameters.
	 *
	 * @param pRequest            the request
	 * @param pOrder            the order
	 * @param pProfile            the profile
	 * @param pDeviceId            the device id
	 * @param pUserSessionStartTime            the user session start time
	 * @param pIsRestService the is rest service
	 * @param pBrowserAcceptEncoding the browser accept encoding
	 */
	public void updateOrderWithFraudParameters(DynamoHttpServletRequest pRequest, TRUOrderImpl pOrder, RepositoryItem pProfile, String pDeviceId,
			Date pUserSessionStartTime, boolean pIsRestService, String pBrowserAcceptEncoding) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderManager::@method::updateOrderWithFraudParameters() : START");
		}
		if (StringUtils.isNotBlank(pDeviceId)) {
			int deviceIdLenth = pDeviceId.length();
			if (deviceIdLenth > TRUCheckoutConstants.INT_FOUR_ZERO_ZERO_ZERO) {
				pOrder.setDeviceID(pDeviceId.substring(TRUCheckoutConstants.INT_ZERO, TRUCheckoutConstants.INT_FOUR_ZERO_ZERO_ZERO));
			} else {
				pOrder.setDeviceID(pDeviceId);
			}
		}
		if (pIsRestService) {
			updateOrderFraudParametersForRest(pRequest, pOrder);

		} else {

			pOrder.setBrowserID(pRequest.getHeader(TRUCheckoutConstants.USER_AGENT));
			pOrder.setBrowserSessionId(pRequest.getCookieParameter(TRUCheckoutConstants.TRUSESSIONID));
			pOrder.setBrowserConnection(pRequest.getHeader(TRUCheckoutConstants.CONNECTION));
			pOrder.setBrowserAccept(pRequest.getHeader(TRUCheckoutConstants.ACCEPT));
			if (isLoggingDebug()) {
				vlogDebug("updateOrderWithFraudParameters() pBrowserAcceptEncoding {0}", pBrowserAcceptEncoding);
			}
			pOrder.setBrowserAcceptEncoding(pBrowserAcceptEncoding);
			pOrder.setBrowserAcceptCharset(pRequest.getHeader(TRUCheckoutConstants.CONTENT_TYPE));
			pOrder.setBrowserIdLanguageCode(pRequest.getHeader(TRUCheckoutConstants.ACCEPT_LANGUAGE));
			String cookie = pRequest.getHeader(TRUCheckoutConstants.COOKIE);
			if (StringUtils.isNotBlank(cookie)) {
				int cookieLenth = cookie.length();
				if (cookieLenth > TRUCheckoutConstants.INT_FOUR_ZERO_ZERO_ZERO) {
					pOrder.setBrowserCookie(cookie.substring(TRUCheckoutConstants.INT_ZERO, TRUCheckoutConstants.INT_FOUR_ZERO_ZERO_ZERO));
				} else {
					pOrder.setBrowserCookie(cookie);
				}
			}
			pOrder.setBrowserReferer(pRequest.getHeader(TRUCheckoutConstants.REFERER));
			pOrder.setHostName(pRequest.getServerName());

			String clientIp = getTrueClientIpAddress(pRequest);
			if (isLoggingDebug()) {
				vlogDebug("Request.getHeader(True-Client-IP) :{0}", clientIp);
			}
			
			if (StringUtils.isNotBlank(clientIp)) {
				pOrder.setCustomerIPAddress(clientIp);
			}
			try {
				Date currentDate = new Date();
				SimpleDateFormat dateFormat = new SimpleDateFormat(TRUCheckoutConstants.HH_MM_SS);
				Date startTime = dateFormat.parse(dateFormat.format(pUserSessionStartTime));
				Date currentTime = dateFormat.parse(dateFormat.format(currentDate));
				long duration = currentTime.getTime() - startTime.getTime();
				pOrder.setTimeSpentOnSite(formatDuration(duration));
			} catch (ParseException e) {
				if (isLoggingError()) {
					vlogError("ParseException in handleAddUpdateBppItem : {0}", e);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderManager::@method::updateOrderWithFraudParameters() : END");
		}
	}
	
	/**
	 * Format duration.
	 *
	 * @param pDuration the duration
	 * @return the string
	 */
	private static String formatDuration(long pDuration) {
	    long hours = TimeUnit.MILLISECONDS.toHours(pDuration);
	    long minutes = TimeUnit.MILLISECONDS.toMinutes(pDuration) % TRUCheckoutConstants.INT_SIX_ZERO;
	    long seconds = TimeUnit.MILLISECONDS.toSeconds(pDuration) % TRUCheckoutConstants.INT_SIX_ZERO;
	    return String.format(TRUCheckoutConstants.TIME_SPENT_ONSITE_FORMAT, hours, minutes, seconds);
	}
	
	/**
	 * Update order with fraud parameters.
	 *
	 * @param pRequest the request
	 * @param pOrder the order
	 * @param pProfile the profile
	 * @param pDeviceId the device id
	 * @param pUserSessionStartTime the user session start time
	 * @param pBrowserAcceptEncoding -  BrowserAcceptEncoding
	 * @param pIsRestService -  Is Rest Service
	 */
	public void updateLayawayOrderWithFraudParameters(DynamoHttpServletRequest pRequest, TRULayawayOrderImpl pOrder, 
			RepositoryItem pProfile, String pDeviceId, Date pUserSessionStartTime,String pBrowserAcceptEncoding,boolean pIsRestService){
		if(isLoggingDebug()){
			logDebug("@Class::TRUOrderManager::@method::updateLayawayOrderWithFraudParameters() : START");
		}
		if(StringUtils.isNotBlank(pDeviceId)) {
			int deviceIdLenth = pDeviceId.length();
			if(deviceIdLenth > TRUCheckoutConstants.INT_FOUR_ZERO_ZERO_ZERO) {
				pOrder.setDeviceID(pDeviceId.substring(TRUCheckoutConstants.INT_ZERO, TRUCheckoutConstants.INT_FOUR_ZERO_ZERO_ZERO));
			} else {
				pOrder.setDeviceID(pDeviceId);
			}
		}
		
		if (pIsRestService) {
			updateLayawayOrderFraudParametersForRest(pRequest, pOrder);

		}else{
		pOrder.setBrowserID(pRequest.getHeader(TRUCheckoutConstants.USER_AGENT));
		pOrder.setBrowserSessionId(pRequest.getCookieParameter(TRUCheckoutConstants.TRUSESSIONID));
		pOrder.setBrowserConnection(pRequest.getHeader(TRUCheckoutConstants.CONNECTION));
		pOrder.setBrowserAccept(pRequest.getHeader(TRUCheckoutConstants.ACCEPT));
		if (isLoggingDebug()) {
			vlogDebug("updateLayawayOrderWithFraudParameters() pBrowserAcceptEncoding {0}", pBrowserAcceptEncoding);
		}
		pOrder.setBrowserAcceptEncoding(pBrowserAcceptEncoding);
		pOrder.setBrowserAcceptCharset(pRequest.getHeader(TRUCheckoutConstants.CONTENT_TYPE));
		pOrder.setBrowserIdLanguageCode(pRequest.getHeader(TRUCheckoutConstants.ACCEPT_LANGUAGE));

		String cookie = pRequest.getHeader(TRUCheckoutConstants.COOKIE);
		if(StringUtils.isNotBlank(cookie)) {
			int cookieLenth = cookie.length();
			if(cookieLenth > TRUCheckoutConstants.INT_FOUR_ZERO_ZERO_ZERO) {
				pOrder.setBrowserCookie(cookie.substring(TRUCheckoutConstants.INT_ZERO, TRUCheckoutConstants.INT_FOUR_ZERO_ZERO_ZERO));
			} else {
				pOrder.setBrowserCookie(cookie);
			}
		}

		pOrder.setBrowserReferer(pRequest.getHeader(TRUCheckoutConstants.REFERER));
		pOrder.setHostName(pRequest.getRemoteHost());

		String clientIp = getTrueClientIpAddress(pRequest);
		if (isLoggingDebug()) {
			vlogDebug("Request.getHeader(True-Client-IP) :{0}", clientIp);
		}
		
		pOrder.setCustomerIPAddress(clientIp);

		try {
			Date currentDate = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat(TRUCheckoutConstants.HH_MM_SS);
			Date startTime = dateFormat.parse(dateFormat.format(pUserSessionStartTime));
			Date currentTime = dateFormat.parse(dateFormat.format(currentDate));
			long duration = currentTime.getTime() - startTime.getTime();
			pOrder.setTimeSpentOnSite(formatDuration(duration));
		} catch (ParseException e) {
			if(isLoggingError()){
				vlogError("ParseException in updateLayawayOrderWithFraudParameters : {0}", e);
			}
		}
		if(isLoggingDebug()){
			logDebug("@Class::TRUOrderManager::@method::updateLayawayOrderWithFraudParameters() : END");
		}
		}
	}

	/**
	 * Gets the ship group by nick name and ship method.
	 *
	 * @param pOrder the order
	 * @param pNickName the nick name
	 * @param pShippingMethod the shipping method
	 * @param pShippingGrpClassType the shipping grp class type
	 * @return the ship group by nick name and ship method
	 */
	public TRUHardgoodShippingGroup getShipGroupByNickNameAndShipMethod(Order pOrder, String pNickName, String pShippingMethod,String pShippingGrpClassType) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getShipGroupByNickNameAndShipMethod() : START");
			vlogDebug("INPUT PARAMS: getShipGroupByNickNameAndShipMethod pOrder: {0} pNickName: {1} pShippingMethod: {2} and pShippingGrpClassType: {3}", pOrder,
					pNickName, pShippingMethod,pShippingGrpClassType);
		}
		if (pOrder == null || pNickName == null || pShippingMethod == null || pShippingGrpClassType ==null) {
			return null;
		}
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		for (ShippingGroup sg : shippingGroups) {			
			if (sg instanceof TRUHardgoodShippingGroup) {
				TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) sg;
				String nickName=hardgoodShippingGroup.getNickName();
				if (pShippingGrpClassType.equalsIgnoreCase(hardgoodShippingGroup.getShippingGroupClassType()) &&
						StringUtils.isNotBlank(nickName) && nickName.equalsIgnoreCase(pNickName)			&&
						hardgoodShippingGroup.getShippingMethod().equalsIgnoreCase(pShippingMethod)) {
					return hardgoodShippingGroup;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getShipGroupByNickNameAndShipMethod() : END");
		}
		return null;
	}
	
	
	/**
	 * <p>
	 * getShipGroupsByNickName.
	 * </p>
	 * .
	 *
	 * @param pOrder the order
	 * @param pNickName the nick name
	 * @param pShippingGroup the shipping group
	 * @return List TRUHardgoodShippingGroup - repective SG's
	 */
	public List<TRUHardgoodShippingGroup> getShipGroupsByNickName(Order pOrder, String pNickName,TRUHardgoodShippingGroup pShippingGroup) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getShipGroupsByNickName() : START");
			vlogDebug(
					"@Class::TRUOrderManager::@method::getShipGroupsByNickName() INPUT PARAMS: getShipGroupsByNickName pOrder: {0}" + 
							" pNickName: {1} ", pOrder, pNickName);
		}
		if (pOrder == null || pNickName == null) {
			return null;
		}
		String classType = pShippingGroup.getShippingGroupClassType() ;
		String channelId = TRUConstants.EMPTY ;
		String channelType = TRUConstants.EMPTY ;
		if(pShippingGroup instanceof TRUChannelHardgoodShippingGroup){
			channelType = ((TRUChannelHardgoodShippingGroup)pShippingGroup).getChannelType() ;
			channelId = ((TRUChannelHardgoodShippingGroup)pShippingGroup).getChannelId() ;
		}
		List<TRUHardgoodShippingGroup> shipGroups = new ArrayList<TRUHardgoodShippingGroup>();
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		for (ShippingGroup sg : shippingGroups) {
			if (sg instanceof TRUChannelHardgoodShippingGroup) {
				TRUChannelHardgoodShippingGroup hardgoodShippingGroup = (TRUChannelHardgoodShippingGroup) sg;
				if (channelId.equalsIgnoreCase(hardgoodShippingGroup.getChannelId()) &&
					channelType.equalsIgnoreCase(hardgoodShippingGroup.getChannelType()) && 	
						pNickName.equalsIgnoreCase(hardgoodShippingGroup.getNickName())) {
					shipGroups.add(hardgoodShippingGroup);
				}
			}else if (sg instanceof TRUHardgoodShippingGroup) {
				TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) sg;
				if (pNickName.equalsIgnoreCase(hardgoodShippingGroup.getNickName()) && 
						classType.equalsIgnoreCase(hardgoodShippingGroup.getShippingGroupClassType())) {
					shipGroups.add(hardgoodShippingGroup);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::getShipGroupsByNickName() : END");
		}
		return shipGroups;
	}
	
	/**
	 * Promo financing reset.
	 *
	 * @param pOrder the order
	 */
	public void promoFinancingReset(Order pOrder){
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::promoFinancingReset() : END");
		}

		if(!((TRUOrderImpl)pOrder).getActiveAgreementsVO().isEmpty()) {
			((TRUOrderImpl)pOrder).setFinanceAgreed(Boolean.FALSE);
			for (String cardNumber : ((TRUOrderImpl)pOrder).getActiveAgreementsVO().keySet()) {
				((TRUOrderImpl)pOrder).getActiveAgreementsVO().get(cardNumber).setFinanceAgreed(TRUConstants.INTEGER_NUMBER_TWO);
			}
		}
		
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::promoFinancingReset() : END");
		}
		
	}
	
	/**
	 * This method is used to check, is User used max limit of coupon in Order?
	 * 
	 * @param pOrder - Order object
	 * @return - boolean
	 * @throws RepositoryException - Repository exception if any
	 */
	public boolean checkOrderMaxCouponLimit(Order pOrder) throws RepositoryException{
		if(isLoggingDebug()){
			logDebug("Enter into [Class: TRUOrderManager  method: checkOrderMaxCouponLimit]");
			vlogDebug("pOrder : {0}",pOrder);
		}
		boolean maxCouponLimit = Boolean.FALSE;
		TRUOrderImpl orderImpl = null;
		if(pOrder instanceof TRUOrderImpl){
			orderImpl = (TRUOrderImpl) pOrder;
		}
		if(orderImpl == null){
			return maxCouponLimit;
		}
		int numOfCouponUsed = 0;
		Map<String, String> notAppliedCouponCode = orderImpl.getNotAppliedCouponCode();
		Map<String, String> singleUseCoupon = orderImpl.getSingleUseCoupon();
		if(notAppliedCouponCode != null){
			numOfCouponUsed += notAppliedCouponCode.size();
		}
		if(singleUseCoupon != null){
			numOfCouponUsed += singleUseCoupon.size();
		}
		int couponMaxLimit = getCouponMaxLimit();
		if(isLoggingDebug()){
			vlogDebug("numOfCouponUsed:{0} couponMaxLimit : {1}"
					, numOfCouponUsed,couponMaxLimit);
		}
		if(couponMaxLimit > 0 && numOfCouponUsed >= couponMaxLimit){
			maxCouponLimit = Boolean.TRUE;
		}
		if(isLoggingDebug()){
			vlogDebug("maxCouponLimit : {0}",maxCouponLimit);
			logDebug("Exiting from [Class: TRUOrderManager  method: checkOrderMaxCouponLimit]");
		}
		return maxCouponLimit;
	}
	
	/**
	 * Ensure only apple pay pament group.
	 *
	 * @param pOrder the order
	 * @throws CommerceException the commerce exception
	 */
	public void ensureOnlyApplePayPamentGroup(TRUOrderImpl pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.isApplePayPamentGroup method.STARTS");
			vlogDebug("TRUOrderManager.isApplePayPamentGroup, pOrder:{0}", pOrder);
		}
		int nonApplePg = 0;
		boolean isApplepayPGExist = false;
		if (pOrder != null) {
			List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
			List<PaymentGroup> applePayPG = new ArrayList<PaymentGroup>();
			for (PaymentGroup pg : paymentGroups) {
				if (pg instanceof TRUApplePay) {
					applePayPG.add(pg);
					isApplepayPGExist = true;
				} else {
					if (isLoggingDebug()) {
						vlogDebug("TRUOrderManager.isApplePayPamentGroup, removeAllPaymentGroupsFromOrder:{0}", pg);
					}
					nonApplePg++;
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("TRUOrderManager.isApplePayPamentGroup, nonApplePg:{0}", nonApplePg);
			}
			if (nonApplePg >0) {
				getPaymentGroupManager().removeAllPaymentGroupsFromOrder(pOrder, applePayPG);
			}
			if (!isApplepayPGExist) {
				TRUApplePay applePay = (TRUApplePay) getPaymentGroupManager().createPaymentGroup(TRUCheckoutConstants.APPLE_PAY);
				getPaymentGroupManager().addPaymentGroupToOrder(pOrder, applePay);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.isApplePayPamentGroup method.ENDS");
		}
	}
	
	
	
	/**
	 * Ensure apple pay pament group removed.
	 *
	 * @param pOrder the order
	 * @throws CommerceException the commerce exception
	 */
	public void removeApplePayPamentGroup(TRUOrderImpl pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.removeApplePayPamentGroup method.STARTS");
			vlogDebug("TRUOrderManager.removeApplePayPamentGroup, pOrder:{0}", pOrder);
		}
		if (pOrder != null) {
			List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
			if (paymentGroups != null) {
				for (PaymentGroup pg : paymentGroups) {
					if (pg instanceof TRUApplePay) {
						String pgID = pg.getId();
						getPaymentGroupManager().removePaymentGroupFromOrder(pOrder, pgID);
						if (isLoggingDebug()) {
							vlogDebug("TRUOrderManager.removing ApplePay PamentGroup, pg:{0}", pgID);
						}
						break;
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderManager.removeApplePayPamentGroup method.ENDS");
		}
	}
	
		
	/**
	 * Adds the shipping address.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pProfile
	 *            the profile
	 * @param pShippingAddress
	 *            the shipping address
	 * @param pShippingGroupMapContainer
	 *            the shipping group map container
	 * @param pShipToName
	 *            the shipping address name
	 * @throws CommerceException
	 *             the commerce exception
	 */
	@SuppressWarnings("unchecked")
	public void addPayPalShippingAddressToOrder(Order pOrder, RepositoryItem pProfile, ContactInfo pShippingAddress,
			ShippingGroupMapContainer pShippingGroupMapContainer,String pShipToName) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::addShippingAddress() : START");
		}
		List<ShippingGroup> shippingGroupList = pOrder.getShippingGroups();
		if (shippingGroupList != null && !shippingGroupList.isEmpty()) {
			for (ShippingGroup shippingGroup : shippingGroupList) {
				if (shippingGroup instanceof TRUHardgoodShippingGroup && !(shippingGroup instanceof TRUChannelHardgoodShippingGroup)) {
					TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
					if(hardgoodShippingGroup.getShippingAddress() != null && 
							StringUtils.isNotBlank(hardgoodShippingGroup.getShippingAddress().getFirstName())){
						continue;
					}
					//Start CR:21 Changes
					String nickName = pShippingAddress.getFirstName() + TRUPaypalConstants.SPACE_STRING	+ pShippingAddress.getLastName();
					Map<String,String> firstAndLastNameMap = getFirstAndLastName(pShipToName, pShippingAddress.getFirstName(), pShippingAddress.getLastName() );
					//End CR:21 Changes
					hardgoodShippingGroup.setNickName(nickName);
					hardgoodShippingGroup.setShipToName(pShipToName);
					pShippingAddress.setFirstName(firstAndLastNameMap.get(FIRST_NAME));
					pShippingAddress.setLastName(firstAndLastNameMap.get(LAST_NAME));
					hardgoodShippingGroup.setShippingAddress(pShippingAddress);
					// set the shipping group in container
					if(StringUtils.isBlank(pShippingGroupMapContainer.getDefaultShippingGroupName())){
						pShippingGroupMapContainer.setDefaultShippingGroupName(nickName);
						((TRUShippingGroupContainerService) pShippingGroupMapContainer).setSelectedAddressNickName(nickName);
						((TRUShippingGroupContainerService)pShippingGroupMapContainer).setShippingAddress(pShippingAddress);
					}					
					// Put the new shipping group in the container.
					TRUHardgoodShippingGroup newHardgoodShippingGroup = (TRUHardgoodShippingGroup)getShippingGroupManager().createShippingGroup();
					OrderTools.copyAddress(hardgoodShippingGroup.getShippingAddress(), newHardgoodShippingGroup.getShippingAddress());
					newHardgoodShippingGroup.setBillingAddress(Boolean.FALSE);
					newHardgoodShippingGroup.setShippingMethod(hardgoodShippingGroup.getShippingMethod());
					newHardgoodShippingGroup.setShippingPrice(hardgoodShippingGroup.getShippingPrice());
					newHardgoodShippingGroup.setShipToName(pShipToName);
					newHardgoodShippingGroup.setNickName(nickName);
					/*newHardgoodShippingGroup.getShippingAddress().setFirstName(firstAndLastNameMap.get(FIRST_NAME));
					newHardgoodShippingGroup.getShippingAddress().setLastName(firstAndLastNameMap.get(LAST_NAME));*/
					pShippingGroupMapContainer.addShippingGroup(nickName, newHardgoodShippingGroup);
					//String billingAddressnickName = nickName+TRUCheckoutConstants.STRING_BA;
					//pShippingGroupMapContainer.addShippingGroup(billingAddressnickName, hardgoodShippingGroup);
					
					//Update the shipping method and its price
					getShippingHelper().updateShippingMethodAndPrice(pOrder, hardgoodShippingGroup);
					//Cross check whether the default ship method is eligible for all the cart items or not.If not do the order reconciliation.
					//((TRUShippingGroupManager)getShippingGroupManager()).validateShippingMethodAndPrice(pOrder);
					// setting shipping as active tab
					((TRUOrderImpl) pOrder).getActiveTabs().put(TRUCheckoutConstants.SHIPPING_TAB, Boolean.TRUE);										
				} else if (shippingGroup instanceof TRUInStorePickupShippingGroup) {
					TRUInStorePickupShippingGroup truInStorePickupShippingGroup = (TRUInStorePickupShippingGroup) shippingGroup;
					if(StringUtils.isNotBlank(truInStorePickupShippingGroup.getFirstName()) || 
							StringUtils.isNotBlank(truInStorePickupShippingGroup.getPhoneNumber())){
						continue;
					}
					Map<String,String> firstAndLastNameMap = getFirstAndLastName(pShipToName, pShippingAddress.getFirstName(), pShippingAddress.getLastName() );
					// PRE-Populating pick up step fields.
					truInStorePickupShippingGroup.setFirstName(firstAndLastNameMap.get(FIRST_NAME));
					truInStorePickupShippingGroup.setLastName(firstAndLastNameMap.get(LAST_NAME));
					truInStorePickupShippingGroup.setPhoneNumber(pShippingAddress.getPhoneNumber());
					truInStorePickupShippingGroup.setEmail(pShippingAddress.getEmail());
					truInStorePickupShippingGroup.setShipToName(pShipToName);
					// pick up step is also complete now
					((TRUOrderImpl) pOrder).getActiveTabs().put(TRUCheckoutConstants.PICKUP_TAB, Boolean.TRUE);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::addShippingAddress() : END");
		}
	}
	
	/** 
	 * Update order with fraud parameters for REST API's.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pOrder
	 *            the order 
	 */

	private void updateOrderFraudParametersForRest(DynamoHttpServletRequest pRequest, TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderManager::@method::updateOrderFraudParametersForRest() : START");
		}
		pOrder.setBrowserID(pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_ID));
		pOrder.setBrowserSessionId(pRequest.getCookieParameter(TRUCheckoutConstants.CO_BROWSER_SESSION_ID));
		pOrder.setBrowserConnection(pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_CONNECTION));
		pOrder.setBrowserAccept(pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_ACCEPT));
		if (isLoggingDebug()) {
			vlogDebug("pRequest.getHeader(Accept-Encoding) :{0}", pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_ACCEPT_ENCODING));
			//vlogDebug("pRequest.getHeader(Content-Encoding) :{0}", pRequest.getHeader(TRUCheckoutConstants.CONTENT_ENCODING));
		}
		pOrder.setBrowserAcceptEncoding(pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_ACCEPT_ENCODING));
		pOrder.setBrowserAcceptCharset(pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_ACCEPT_CHARSET));
		pOrder.setBrowserIdLanguageCode(pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_ID_LANGUAGE_CODE));
		String cookie = pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_COOKIE);
		if (StringUtils.isNotBlank(cookie)) {
			int cookieLenth = cookie.length();
			if (cookieLenth > TRUCheckoutConstants.INT_FOUR_ZERO_ZERO_ZERO) {
				pOrder.setBrowserCookie(cookie.substring(TRUCheckoutConstants.INT_ZERO, TRUCheckoutConstants.INT_FOUR_ZERO_ZERO_ZERO));
			} else {
				pOrder.setBrowserCookie(cookie);
			}
		}
		pOrder.setBrowserReferer(pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_REFERRER));
		pOrder.setHostName(pRequest.getServerName());
		String clientIp = pRequest.getHeader(TRUCheckoutConstants.CO_CUSTOMER_IP_ADDRESS);
		if (isLoggingDebug()) {
			vlogDebug("Request.getHeader(True-Client-IP) :{0}", clientIp);
		}
		if (StringUtils.isBlank(clientIp)) {
			clientIp = pRequest.getRemoteAddr();
			if (isLoggingDebug()) {
				vlogDebug("Request.getRemoteAddr():{0}", clientIp);
			}
		}
		if (StringUtils.isBlank(clientIp)) {
			clientIp = pRequest.getLocalAddr();
			if (isLoggingDebug()) {
				vlogDebug("Request.getLocalAddr():{0}", clientIp);
			}
		}
		if (StringUtils.isNotBlank(clientIp)) {
			pOrder.setCustomerIPAddress(clientIp);
		}
		if (pRequest.getHeader(TRUCheckoutConstants.CO_TIME_SPENT_ON_SITE) != null) {
			pOrder.setTimeSpentOnSite(pRequest.getHeader(TRUCheckoutConstants.CO_TIME_SPENT_ON_SITE));
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderManager::@method::updateOrderFraudParametersForRest() : END");
		}
	}
	
	/**
	 * Gets the request client ip address.
	 *
	 * @param pRequest the request
	 * @return the request client ip address
	 */
	public String getTrueClientIpAddress(DynamoHttpServletRequest pRequest) {
		
		if (isLoggingDebug()) {
			logDebug("START:: TRUOrderManager.getRequestClientIpAddress method..");
		}
		String clientIp = pRequest.getHeader(getConfiguration().getHeaderparamForIpAddress());
		if (isLoggingDebug()) {
			vlogDebug("Request.getHeader():: {0} ::{1}", getConfiguration().getHeaderparamForIpAddress(), clientIp);
		}
		if(StringUtils.isNotBlank(clientIp)) {
			StringTokenizer tokenizer = new StringTokenizer(clientIp, SPLIT_SYMBOL_COMA);
			while (tokenizer.hasMoreTokens()) {
				clientIp = tokenizer.nextToken().trim();
				break;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug(" True Client IP :{0}", clientIp);
		}
		if (StringUtils.isBlank(clientIp)) {
			if (isLoggingDebug()) {
				vlogDebug(
						"The Client ip :{0} Header is null so setting Remote Addr as client IP",clientIp);
			}
			clientIp = pRequest.getRemoteAddr();
			if (isLoggingDebug()) {
				vlogDebug("True Client IP after setting RemoteAddress:{0}",clientIp);
			}
		}

		if (StringUtils.isBlank(clientIp)) {
			clientIp = pRequest.getLocalAddr();
			if (isLoggingDebug()) {
				vlogDebug("Request.getLocalAddr():{0}", clientIp);
			}
		}
		
		if (StringUtils.isBlank(clientIp)) {
			clientIp = TRURadialConstants.DEFAULT_IP;
		}
		
		if (isLoggingDebug()) {
			vlogDebug(" True Client IP :{0}", clientIp);
			logDebug("END:: TRUOrderManager.getRequestClientIpAddress method..");
		}

		return clientIp;
	}

	/**
	 * Update order billing address.
	 *
	 * @param pShoppingCart the shopping cart
	 * @param pCurrentOrder the order
	 * @param pShippingGroupMapContainerService the shipping group map container service
	 */
	public void updateOrderBillingAddress(TRUOrderHolder pShoppingCart, TRUOrderImpl pCurrentOrder, ShippingGroupMapContainer pShippingGroupMapContainerService) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderManager::@method ::updateOrderBillingAddress() : START");
		}
		RepositoryItem profileCCard = null;
		Address orderBillingAddress = ((TRUOrderImpl)pCurrentOrder).getBillingAddress();
		RepositoryItem profile = pShoppingCart.getProfile();
		if(!pShoppingCart.getProfile().isTransient() && orderBillingAddress !=null){
			String billingAddressNickName1 = ((TRUOrderImpl)pCurrentOrder).getBillingAddressNickName();
			RepositoryItem billingAddressItem = getPurchaseProcessHelper().getProfileTools().getSecondaryAddressByNickName(profile, billingAddressNickName1);
			if(!pShippingGroupMapContainerService.getShippingGroupMap().keySet().contains(billingAddressNickName1) && billingAddressItem ==null){
				orderBillingAddress=null;
			}
		}
		if(!pShoppingCart.getProfile().isTransient() && orderBillingAddress == null && profile != null &&
				profile.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getAllCreditCardsPropName()) != null && null != profile.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getSelectedCCNickNamePropName())) {
			Map allCredictCards = (Map) profile.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getAllCreditCardsPropName());
			String selectedCCNickName = (String)profile.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getSelectedCCNickNamePropName());
			if(StringUtils.isNotBlank(selectedCCNickName) && allCredictCards!=null && !allCredictCards.isEmpty()){					
				profileCCard = (RepositoryItem) allCredictCards.get(selectedCCNickName);
			}
			if(profileCCard == null){
				Object defaultCreditCard = profile.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getDefaultCreditCardPropName());
				if(!pShoppingCart.getProfile().isTransient() && orderBillingAddress == null && profile != null &&
						defaultCreditCard != null) {
					profileCCard = (RepositoryItem)defaultCreditCard;
				}
			}
		} else if(!pShoppingCart.getProfile().isTransient() && orderBillingAddress == null && profile != null &&
				profile.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getDefaultCreditCardPropName()) != null) {
			profileCCard = (RepositoryItem)profile.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getDefaultCreditCardPropName());
		}
		try {
			if(orderBillingAddress == null && profileCCard == null){
				((TRUOrderImpl)pCurrentOrder).setBillingAddressNickName(null);
				((TRUOrderImpl)pCurrentOrder).setBillingAddress(null);
				((TRUOrderImpl)pCurrentOrder).setRepriceRequiredOnCart(Boolean.TRUE);
				return;
			}
			if(profileCCard != null && null != profileCCard.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getBillingAddressPropName())) {
				RepositoryItem ccBillingAddress = (RepositoryItem) profileCCard.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getBillingAddressPropName());
				if(ccBillingAddress!=null){
					ContactInfo billingAddress  = new ContactInfo();
					OrderTools.copyAddress(ccBillingAddress, billingAddress);
					//Adding the billing address to the Order. 
					((TRUOrderImpl)pCurrentOrder).setBillingAddress(billingAddress);
					if(StringUtils.isNotBlank(getPurchaseProcessHelper().getProfileTools().getProfileAddressName(profile, ccBillingAddress))) {
						String billingAddressNickName = getPurchaseProcessHelper().getProfileTools().getProfileAddressName(profile, ccBillingAddress);
						((TRUOrderImpl)pCurrentOrder).setBillingAddressNickName(billingAddressNickName);
						((TRUOrderImpl)pCurrentOrder).setRepriceRequiredOnCart(Boolean.TRUE);
						String creditCardNickname = getPurchaseProcessHelper().getProfileTools().getCreditCardNickname(profile, profileCCard);
						((MutableRepositoryItem)profile).setPropertyValue(getPurchaseProcessHelper().getPropertyManager().getSelectedCCNickNamePropertyName(), creditCardNickname);
					}
				}
			} 
		} catch (CommerceException e) {
			if(isLoggingError()){
				vlogError("In@Class::TRUOrderManager::@method ::updateOrderBillingAddress while copyAddress {0}", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderManager::@method ::updateOrderBillingAddress() : END");
		}
	}
	/** 
	 * Update order with fraud parameters for REST API's.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pOrder
	 *            the order 
	 */

	private void updateLayawayOrderFraudParametersForRest(DynamoHttpServletRequest pRequest, TRULayawayOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderManager::@method::updateOrderFraudParametersForRest() : START");
		}
		pOrder.setBrowserID(pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_ID));
		pOrder.setBrowserSessionId(pRequest.getCookieParameter(TRUCheckoutConstants.CO_BROWSER_SESSION_ID));
		pOrder.setBrowserConnection(pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_CONNECTION));
		pOrder.setBrowserAccept(pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_ACCEPT));
		if (isLoggingDebug()) {
			vlogDebug("pRequest.getHeader(Accept-Encoding) :{0}", pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_ACCEPT_ENCODING));
			//vlogDebug("pRequest.getHeader(Content-Encoding) :{0}", pRequest.getHeader(TRUCheckoutConstants.CONTENT_ENCODING));
		}
		pOrder.setBrowserAcceptEncoding(pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_ACCEPT_ENCODING));
		pOrder.setBrowserAcceptCharset(pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_ACCEPT_CHARSET));
		pOrder.setBrowserIdLanguageCode(pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_ID_LANGUAGE_CODE));
		String cookie = pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_COOKIE);
		if (StringUtils.isNotBlank(cookie)) {
			int cookieLenth = cookie.length();
			if (cookieLenth > TRUCheckoutConstants.INT_FOUR_ZERO_ZERO_ZERO) {
				pOrder.setBrowserCookie(cookie.substring(TRUCheckoutConstants.INT_ZERO, TRUCheckoutConstants.INT_FOUR_ZERO_ZERO_ZERO));
			} else {
				pOrder.setBrowserCookie(cookie);
			}
		}
		pOrder.setBrowserReferer(pRequest.getHeader(TRUCheckoutConstants.CO_BROWSER_REFERRER));
		pOrder.setHostName(pRequest.getServerName());
		String clientIp = pRequest.getHeader(TRUCheckoutConstants.CO_CUSTOMER_IP_ADDRESS);
		if (isLoggingDebug()) {
			vlogDebug("Request.getHeader(True-Client-IP) :{0}", clientIp);
		}
		if (StringUtils.isBlank(clientIp)) {
			clientIp = pRequest.getRemoteAddr();
			if (isLoggingDebug()) {
				vlogDebug("Request.getRemoteAddr():{0}", clientIp);
			}
		}
		if (StringUtils.isBlank(clientIp)) {
			clientIp = pRequest.getLocalAddr();
			if (isLoggingDebug()) {
				vlogDebug("Request.getLocalAddr():{0}", clientIp);
			}
		}
		if (StringUtils.isNotBlank(clientIp)) {
			pOrder.setCustomerIPAddress(clientIp);
		}
		if (pRequest.getHeader(TRUCheckoutConstants.CO_TIME_SPENT_ON_SITE) != null) {
			pOrder.setTimeSpentOnSite(pRequest.getHeader(TRUCheckoutConstants.CO_TIME_SPENT_ON_SITE));
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderManager::@method::updateOrderFraudParametersForRest() : END");
		}
	}
	
	/**
	 * Method to remove the old single used coupons codes from Order.
	 *
	 * @param pDestOrder the dest order
	 * @return Map
	 */
	public Map<String, String> removeOldSingleUsedCoupons(Order pDestOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOrderManager  method: removeOldSingleUsedCoupons]");
			vlogDebug("pDestOrder : {0}", pDestOrder);
		}
		Map<String, String> singleUseCoupon = null;
		if(pDestOrder == null){
			return singleUseCoupon;
		}
		singleUseCoupon = ((TRUOrderImpl)pDestOrder).getSingleUseCoupon();
		if(isLoggingDebug()){
			vlogDebug("singleUseCoupon : {0}", singleUseCoupon);
		}
		for(Iterator<Map.Entry<String, String>> it = singleUseCoupon.entrySet().iterator(); it.hasNext(); ) {
			Map.Entry<String, String> entry = it.next();
		    if(!StringUtils.isBlank(entry.getKey())&&entry.getKey().length()==TRUConstants.TWENTY){
		    	it.remove();
			}
		}
		if(isLoggingDebug()){
			vlogDebug("singleUseCoupon : {0}", singleUseCoupon);
			logDebug("END : TRUOrdermanager removeOldSingleUsedCoupons()");
		}
		return singleUseCoupon;
	}
	
	/**
	 * Method to remove the old not used coupons codes from Order.
	 *
	 * @param pDestOrder the dest order
	 * @return Map - Updated Map
	 */
	public Map<String, String> removeOldNotAppliedCoupons(Order pDestOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOrderManager  method: removeOldNotAppliedCoupons]");
			vlogDebug("pDestOrder : {0}", pDestOrder);
		}
		Map<String, String> notAppliedCoupon = null;
		if(pDestOrder == null){
			return notAppliedCoupon;
		}
		notAppliedCoupon = ((TRUOrderImpl)pDestOrder).getNotAppliedCouponCode();
		if (isLoggingDebug()) {
			vlogDebug("notAppliedCoupon : {0}", notAppliedCoupon);
		}
		for(Iterator<Map.Entry<String, String>> it = notAppliedCoupon.entrySet().iterator(); it.hasNext(); ) {
			Map.Entry<String, String> entry = it.next();
		    if(!StringUtils.isBlank(entry.getKey())&&entry.getKey().length()==TRUConstants.TWENTY){
		    	it.remove();
			}
		}
		if(isLoggingDebug()){
			vlogDebug("singleUseCoupon : {0}", notAppliedCoupon);
			logDebug("END : TRUOrdermanager removeOldNotAppliedCoupons()");
		}
		return notAppliedCoupon;
	}
	
	/**
	 * Method to remove the old single used/not used coupons codes from Order.
	 * 
	 * @param pDestOrder - Order Repository Item.
	 * @param pCouponCOdes - Map of single/not used coupon codes.
	 * @param pPropertyName - String property name.
	 * @throws RepositoryException - If any
	 */
	public void updateOldSingleUsedCoupons(RepositoryItem pDestOrder ,Map<String, String> pCouponCOdes,
			String pPropertyName) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOrderManager  method: updateOldSingleUsedCoupons]");
			vlogDebug("pDestOrder : {0} pCouponCOdes : {1} pPropertyName : {2}", 
					pDestOrder,pCouponCOdes,pPropertyName);
		}
		Map<String, String> singleUseCoupon = null;
		if(pDestOrder == null || pCouponCOdes == null || pCouponCOdes.isEmpty() || StringUtils.isBlank(pPropertyName)){
			return;
		}
		MutableRepositoryItem mutOrder = (MutableRepositoryItem) pDestOrder;
		MutableRepository orderRepository = (MutableRepository) getOrderTools().getOrderRepository();
		for(Iterator<Map.Entry<String, String>> it = pCouponCOdes.entrySet().iterator(); it.hasNext(); ) {
			Map.Entry<String, String> entry = it.next();
		    if(!StringUtils.isBlank(entry.getKey())&&entry.getKey().length()==TRUConstants.TWENTY){
		    	it.remove();
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("pCouponCOdes : {0}", pCouponCOdes);
		}
		mutOrder.setPropertyValue(pPropertyName,pCouponCOdes);
		orderRepository.updateItem(mutOrder);
		if(isLoggingDebug()){
			vlogDebug("singleUseCoupon : {0}", singleUseCoupon);
			logDebug("END : TRUOrdermanager updateOldSingleUsedCoupons()");
		}
	}

	/**
	 * Method to remove the Old SUCN data from the Profile.
	 * @param pOrder - Order Object.
	 * @param pProfile - Profile Repository Item
	 * @return boolean.
	 * @throws RepositoryException If any.
	 */
	public boolean removeOldDataFromProfile(TRUOrderImpl pOrder, Profile pProfile) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOrderManager  method: updateOldSingleUsedCoupons]");
			vlogDebug("pOrder : {0} pProfile : {1}", 
					pOrder,pProfile);
		}
		boolean isOrderUpdated = Boolean.FALSE;
		if(pOrder == null || pProfile == null){
			return isOrderUpdated;
		}
		Map<String, String> appliedPromoCodes = pOrder.getSingleUseCoupon();
		Map<String, String> notAppliedPromoCodes = pOrder.getNotAppliedCouponCode();
		int appliedPromoSize = appliedPromoCodes.size();
		int notAppliedPromoSize = notAppliedPromoCodes.size();
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOrderManager  method: updateOldSingleUsedCoupons]");
			vlogDebug("is Order merge : {0}", 
					pOrder.getTransientData().get(TRUCheckoutConstants.IS_MERGE_ORDER));
		}
		if(null == pOrder.getTransientData().get(TRUCheckoutConstants.IS_MERGE_ORDER)) {
			// Calling method to remove old single used coupon if order merge in not happened.
			Map<String, String> singleUseCoupon = removeOldSingleUsedCoupons(pOrder);
			if(singleUseCoupon != null && singleUseCoupon.size() < appliedPromoSize){
				isOrderUpdated = Boolean.TRUE;
				pOrder.setSingleUseCoupon(singleUseCoupon);
			}
			// Calling method to remove old not used coupon if order merge in not happened.
			Map<String, String> notApplied = removeOldNotAppliedCoupons(pOrder);
			if(notApplied != null && notApplied.size() < notAppliedPromoSize){
				isOrderUpdated = Boolean.TRUE;
				pOrder.setNotAppliedCouponCode(notApplied);
			}
			if(((TRUPromotionTools)getPromotionTools()).revokeSingleUseCoupons(pProfile)){
				isOrderUpdated = Boolean.TRUE;
			}
		}else{
			Map<String, String> couponData = new HashMap<String, String>();
			if(appliedPromoCodes != null && !appliedPromoCodes.isEmpty()){
				couponData.putAll(appliedPromoCodes);
			}
			if(notAppliedPromoCodes != null && !notAppliedPromoCodes.isEmpty()){
				couponData.putAll(notAppliedPromoCodes);
			}
			if(((TRUPromotionTools)getPromotionTools()).removeSUCNCouponFromProfile(pProfile, couponData)){
				isOrderUpdated = Boolean.TRUE;
			}
		}
		if(isLoggingDebug()){
			vlogDebug("isOrderUpdated : {0}", isOrderUpdated);
			logDebug("END : TRUOrdermanager removeOldDataFromProfile()");
		}
		return isOrderUpdated;
	}
	
	/**
	 * Method to pre-validate the SUCN coupons.
	 * 
	 * @param pActivePromotions - List of Promotions from Profile.
	 * @param pTenderdCouponCode - String - Tender Coupon Id.
	 * @return boolean.
	 */
	public boolean validateSingleUseCoupon(
			List<RepositoryItem> pActivePromotions, String pTenderdCouponCode) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOrderManager  method: validateSingleUseCoupon]");
			vlogDebug("pActivePromotions : {0} pTenderdCouponCode : {1}", 
					pActivePromotions,pTenderdCouponCode);
		}
		boolean isValid = Boolean.TRUE;
		if(pActivePromotions == null || pActivePromotions.isEmpty()){
			return isValid;
		}
		TRUPropertyManager propertyManager = getPropertyManager();
		for (RepositoryItem promotionStatus : pActivePromotions) {
			Object appliedCodeValue = promotionStatus.getPropertyValue(propertyManager.getAppliedCodeValuePropertyName());
			if(appliedCodeValue != null && ((String)appliedCodeValue).equals(pTenderdCouponCode)){
				return Boolean.FALSE;
			}
		}
		if(isLoggingDebug()){
			vlogDebug("isValid : {0}", isValid);
			logDebug("END : TRUOrdermanager validateSingleUseCoupon()");
		}
		return isValid;
	}
	
	
	/**
	 * This method returns the boolean flag whether order contain channel shipping group or not.
	 * 
	 * @param pOrder - Order
	 * @return - boolean.
	 */
	public boolean isOrderContainRegistryItems(TRUOrderImpl pOrder){
		if(isLoggingDebug()){
			logDebug("Start : TRUOrdermanager isOrderContainRegistryItems()");
		}
		boolean isOrderContainRegistryItem = false;
		List<ShippingGroup> listOfShipGrps = pOrder.getShippingGroups();
		for(ShippingGroup shipGrp : listOfShipGrps){
		   if(shipGrp instanceof TRUChannelHardgoodShippingGroup || shipGrp instanceof TRUChannelInStorePickupShippingGroup) {
			   isOrderContainRegistryItem = true;
			   break;
		   }
		}
		if(isLoggingDebug()){
			logDebug("END : TRUOrdermanager isOrderContainRegistryItems()");
		}
		return isOrderContainRegistryItem;
	}
	/**
	 * 
	 */
	public void customLoadOrder(){
		try {
			
			TRUOrderImpl order = (TRUOrderImpl) loadOrder(getOrderIdToLoad());
			
			Address address = order.getBillingAddress();
			
			RepositoryItem billingAddress = (RepositoryItem)order.getRepositoryItem().getPropertyValue(getPropertyManager().getBillingAddress());
			String state = null;
			if(billingAddress != null){
				state = (String) billingAddress.getPropertyValue(getPropertyManager().getAddressStatePropertyName());
			}
			
			vlogDebug(" Address LastName   {0} ",address.getLastName());
			vlogDebug(" Address City   {0} ",address.getCity());
			vlogDebug(" Address State  {0}  ",state);
			vlogDebug(" Address State2  {0} ",address.getState());
			
			List<PaymentGroup> listPaymentGroup = order.getPaymentGroups();
			
            for(PaymentGroup pg : listPaymentGroup){
            	List<PaymentStatus> listOfPaymentStatus = pg.getAuthorizationStatus();
            	for(PaymentStatus ps : listOfPaymentStatus){
            		if(ps instanceof TRUCreditCardStatus){
            			vlogDebug(" AVSResponseCode :: {0}",((TRUCreditCardStatus)ps).getAvsCode());
            			vlogDebug(" AuthResponseCode :: {0}",((TRUCreditCardStatus)ps).getAuthResponseCode());
            		}
            	}
            }

			
		} catch (CommerceException e) {
			if(isLoggingError()){
				logError("CommerceException in customLoadOrder in TRUOrderManager: ",e);
			}
		}
		
	}
	
	/**
	 * Method to Update the Show Me Gifting Options to Order
	 * 
	 * @param pOrder
	 *            the Order
	 */
	@SuppressWarnings("unchecked")
	public void updateShowMeGiftingOptions(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderManager::@method::updateShowMeGiftingOptions() : START");
		}
		if (pOrder != null) {
			TRUOrderImpl orderImpl = (TRUOrderImpl) pOrder;
			boolean isShowMeGiftingOptions = orderImpl.isShowMeGiftingOptions();
			if(isShowMeGiftingOptions) {
				return;
			}
			final List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
			if (shippingGroups != null && !shippingGroups.isEmpty()) {
				for (ShippingGroup shippingGroup : shippingGroups) {
					if (shippingGroup instanceof TRUHardgoodShippingGroup || shippingGroup instanceof TRUChannelHardgoodShippingGroup) {
						final List<CommerceItemRelationship> ciRels = shippingGroup.getCommerceItemRelationships();
						for (CommerceItemRelationship commerceItemRelationship : ciRels) {
							final TRUShippingGroupCommerceItemRelationship truSgCiR = (TRUShippingGroupCommerceItemRelationship)commerceItemRelationship;
							if (truSgCiR.getIsGiftItem()) {
								orderImpl.setShowMeGiftingOptions(true);
								return;
							}
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrdermanager::@method::updateShowMeGiftingOptions() : END");
		}
	}
	
	/**
	 * Gets the first and last name.
	 *
	 * @param pShipToName the ship to name
	 * @param pDefaultFirstName the default first name
	 * @param pDefaultLastName the default last name
	 * @return the first and last name
	 */
	private Map<String, String> getFirstAndLastName(String pShipToName,String pDefaultFirstName,String pDefaultLastName){
		if(isLoggingDebug()){
			logDebug("Start getFirstAndLastName()");
		}
		Map<String, String> map = new ConcurrentHashMap<>();		
		//Set Default Value
		String firstName=pDefaultFirstName;
		String lastName =pDefaultLastName;
		if(StringUtils.isNotBlank(pShipToName)){
			String array[] = pShipToName.split(TRUPaypalConstants.SPACE_STRING);
			if(isLoggingDebug()){
				System.out.println("ShipToName Value array after Splitting \t:"+Arrays.toString(array));
			}			
			if(array!=null && array.length == TRUConstants._1){
				firstName = array[0];
				lastName = array[0];
			}else if(array!=null && array.length ==TRUConstants._2){
				firstName = array[0];
				lastName = array[1];
			}else{
				StringBuffer sb = new StringBuffer();
				for(int i = TRUConstants._0 ;i <array.length-TRUConstants._1 ; i++){
					sb.append(array[i]).append(TRUPaypalConstants.SPACE_STRING);
				}
				firstName = sb.toString().trim();
				lastName = array[array.length-1];
			}
		}
		map.put(FIRST_NAME, firstName);
		map.put(LAST_NAME, lastName);
		if(isLoggingDebug()){
			vlogDebug("End getFirstAndLastName() and Calculated FirstName and Last Name :{0}",map);
		}
		return map;
	}
}
