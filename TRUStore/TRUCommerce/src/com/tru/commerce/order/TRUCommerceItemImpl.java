package com.tru.commerce.order;

import java.math.BigDecimal;

import atg.commerce.order.CommerceItemImpl;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.common.TRUConstants;

/**
 * The Class TRUCommerceItemImpl.
 * @author PA
 * @version 1.0
 */
public class TRUCommerceItemImpl extends CommerceItemImpl {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * @return the type
	 */
	public String getType() {
		return (String) getPropertyValue(TRUCommerceConstants.TYPE);
	}

	/**
	 * @param pType the type to set
	 */
	public void setType(String pType) {
		setPropertyValue(TRUCommerceConstants.TYPE, pType);
	}

	/** The gift item. */
	private boolean mGiftItem ;

	/**
	 * @return the giftItem
	 */
	public boolean isGiftItem() {
		return mGiftItem;
	}

	/**
	 * @param pGiftItem the giftItem to set
	 */
	public void setGiftItem(boolean pGiftItem) {
		mGiftItem = pGiftItem;
	}
	
	/** The Tax applied quantity. */
	private long mTaxAppliedQuantity;
	
	
	/** The Tax applied amount. */
	private BigDecimal mTaxAppliedAmount;

	/**
	 * @return the taxAppliedAmount
	 */
	public BigDecimal getTaxAppliedAmount() {
		if(mTaxAppliedAmount == null) {
			return new BigDecimal(TRUConstants._0);
		}
		return mTaxAppliedAmount;
	}

	/**
	 * @param pTaxAppliedAmount the taxAppliedAmount to set
	 */
	public void setTaxAppliedAmount(BigDecimal pTaxAppliedAmount) {
		mTaxAppliedAmount = pTaxAppliedAmount;
	}

	/**
	 * @return the taxAppliedQuantity
	 */
	public long getTaxAppliedQuantity() {
		return mTaxAppliedQuantity;
	}

	/**
	 * @param pTaxAppliedQuantity the taxAppliedQuantity to set
	 */
	public void setTaxAppliedQuantity(long pTaxAppliedQuantity) {
		mTaxAppliedQuantity = pTaxAppliedQuantity;
	}
	
	/**
	 * @return the reservationId
	 */
	public String getReservationId() {
		return (String) getPropertyValue(TRUCheckoutConstants.RESERVATION_ID);
	}

	/**
	 * @param pReservationId the reservationId to set
	 */
	public void setReservationId(String pReservationId) {
		setPropertyValue(TRUCheckoutConstants.RESERVATION_ID, pReservationId);
	}
	
	/**
	 * @return the softAmountAllocated
	 */
	public long getSoftAmountAllocated() {
		Long quantity = (Long)getPropertyValue(TRUCheckoutConstants.SOFT_AMOUNT_ALLOCATED);
	    return ((quantity == null) ? TRUCheckoutConstants._0L : quantity.longValue());
	}

	/**
	 * @param pSoftAmountAllocated the softAmountAllocated to set
	 */
	public void setSoftAmountAllocated(long pSoftAmountAllocated) {
		 setPropertyValue(TRUCheckoutConstants.SOFT_AMOUNT_ALLOCATED, Long.valueOf(pSoftAmountAllocated));
	}
}
