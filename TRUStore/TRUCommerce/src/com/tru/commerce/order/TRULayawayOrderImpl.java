package com.tru.commerce.order;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

import atg.commerce.order.ChangedProperties;
import atg.commerce.order.CommerceIdentifierImpl;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.CostCenter;
import atg.commerce.order.CostCenterNotFoundException;
import atg.commerce.order.CostCenterRelationship;
import atg.commerce.order.DuplicateCommerceItemException;
import atg.commerce.order.DuplicateCostCenterException;
import atg.commerce.order.DuplicatePaymentGroupException;
import atg.commerce.order.DuplicateRelationshipException;
import atg.commerce.order.DuplicateShippingGroupException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupContainerImpl;
import atg.commerce.order.PaymentGroupNotFoundException;
import atg.commerce.order.PaymentGroupRelationship;
import atg.commerce.order.PaymentGroupRelationshipContainerImpl;
import atg.commerce.order.PropertyNameConstants;
import atg.commerce.order.Relationship;
import atg.commerce.order.RelationshipContainerImpl;
import atg.commerce.order.RelationshipNotFoundException;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupNotFoundException;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.TaxPriceInfo;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryItem;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.common.TRUConstants;


/**
 * @author PA
 * @version 1.0
 *
 */
@SuppressWarnings("serial")
public class TRULayawayOrderImpl extends CommerceIdentifierImpl
implements Order, ChangedProperties {
	
	protected transient boolean mInitialized;

	protected transient PaymentGroupContainerImpl mPaymentGroupContainer;
	
	protected transient RelationshipContainerImpl mRelationshipContainer;
	
	protected transient PaymentGroupRelationshipContainerImpl mPaymentGroupRelationshipContainer;
	
	
	/** holds property mRetryCount. **/
	private int mRetryCount ;
	
	/** holds property mPreviousResponseCode. **/
	private String mPreviousResponseCode ;
	
	/** holds property mPreviousCreditCardNumber. **/
	private String mPreviousCreditCardNumber ;
	
	
	/** holds property mLayawayOrderid. **/
	private String mLayawayOrderid;
	
	/** holds property mCommerceId. **/
	private String mCommerceId;
	
	/** holds property mRemainingAmount. **/
	private double mRemainingAmount = TRUConstants.DOUBLE_ZERO;
	
	/** holds layaway repository item. **/
	private MutableRepositoryItem mLayawayRepositoryItem;
	
	/** holds property mChanged. **/
	private boolean mChanged;
	
	/**
	 * Holds the property value of mHostName.
	 */
	private String mHostName;
	
	/**
	 * Holds the property value of mDeviceID.
	 */
	private String mDeviceID;
	/**
	 * Holds the property value of mBrowserID.
	 */
	private String mBrowserID;
	/**
	 * Holds the property value of mBrowserSessionId.
	 */
	private String mBrowserSessionId;
	/**
	 * Holds the property value of mBrowserConnection.
	 */
	private String mBrowserConnection;
	/**
	 * Holds the property value of mBrowserAccept.
	 */
	private String mBrowserAccept;
	/**
	 * Holds the property value of mBrowserAcceptEncoding.
	 */
	private String mBrowserAcceptEncoding;
	/**
	 * Holds the property value of mBrowserAcceptCharset.
	 */
	private String mBrowserAcceptCharset;
	/**
	 * Holds the property value of mBrowserIdLanguageCode.
	 */
	private String mBrowserIdLanguageCode;
	/**
	 * Holds the property value of mBrowserCookie.
	 */
	private String mBrowserCookie;
	/**
	 * Holds the property value of mBrowserReferer.
	 */
	private String mBrowserReferer;
	/**
	 * Holds the property value of mCustomerIPAddress.
	 */
	private String mCustomerIPAddress;
	/**
	 * Holds the property value of mServerDateTime.
	 */
	private Timestamp mServerDateTime;
	/**
	 * Holds the property value of mTimeSpentOnSite.
	 */
	private String mTimeSpentOnSite;
	
	/**
	 * @return the deviceID
	 */
	public String getDeviceID() {
		mDeviceID = (String) getPropertyValue(TRUCheckoutConstants.DEVICE_ID);
		return mDeviceID;
	}

	/**
	 * @param pDeviceID the deviceID to set
	 */
	public void setDeviceID(String pDeviceID) {
		setPropertyValue(TRUCheckoutConstants.DEVICE_ID, pDeviceID);
	}

	/**
	 * @return the browserID
	 */
	public String getBrowserID() {
		mBrowserID = (String) getPropertyValue(TRUCheckoutConstants.BROWSER_ID);
		return mBrowserID;
	}

	/**
	 * @param pBrowserID the browserID to set
	 */
	public void setBrowserID(String pBrowserID) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_ID, pBrowserID);
	}

	/**
	 * @return the browserSessionId
	 */
	public String getBrowserSessionId() {
		mBrowserSessionId = (String) getPropertyValue(TRUCheckoutConstants.BROWSER_SESSION_ID);
		return mBrowserSessionId;
	}

	/**
	 * @param pBrowserSessionId the browserSessionId to set
	 */
	public void setBrowserSessionId(String pBrowserSessionId) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_SESSION_ID, pBrowserSessionId);
	}

	/**
	 * @return the browserConnection
	 */
	public String getBrowserConnection() {
		mBrowserConnection = (String) getPropertyValue(TRUCheckoutConstants.BROWSER_CONNECTION);
		return mBrowserConnection;
	}

	/**
	 * @param pBrowserConnection the browserConnection to set
	 */
	public void setBrowserConnection(String pBrowserConnection) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_CONNECTION, pBrowserConnection);
	}

	/**
	 * @return the browserAccept
	 */
	public String getBrowserAccept() {
		mBrowserAccept = (String) getPropertyValue(TRUCheckoutConstants.BROWSER_ACCEPT);
		return mBrowserAccept;
	}

	/**
	 * @param pBrowserAccept the browserAccept to set
	 */
	public void setBrowserAccept(String pBrowserAccept) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_ACCEPT, pBrowserAccept);
	}

	/**
	 * @return the browserAcceptEncoding
	 */
	public String getBrowserAcceptEncoding() {
		mBrowserAcceptEncoding = (String) getPropertyValue(TRUCheckoutConstants.BROWSER_ACCEPT_ENCODING);
		return mBrowserAcceptEncoding;
	}

	/**
	 * @param pBrowserAcceptEncoding the browserAcceptEncoding to set
	 */
	public void setBrowserAcceptEncoding(String pBrowserAcceptEncoding) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_ACCEPT_ENCODING, pBrowserAcceptEncoding);
	}

	/**
	 * @return the browserAcceptCharset
	 */
	public String getBrowserAcceptCharset() {
		mBrowserAcceptCharset = (String) getPropertyValue(TRUCheckoutConstants.BROWSER_ACCEPT_CHARSET);
		return mBrowserAcceptCharset;
	}

	/**
	 * @param pBrowserAcceptCharset the browserAcceptCharset to set
	 */
	public void setBrowserAcceptCharset(String pBrowserAcceptCharset) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_ACCEPT_CHARSET, pBrowserAcceptCharset);
	}

	/**
	 * @return the browserIdLanguageCode
	 */
	public String getBrowserIdLanguageCode() {
		mBrowserIdLanguageCode = (String) getPropertyValue(TRUCheckoutConstants.BROWSER_ID_LANGUAGE_CODE);
		return mBrowserIdLanguageCode;
	}

	/**
	 * @param pBrowserIdLanguageCode the browserIdLanguageCode to set
	 */
	public void setBrowserIdLanguageCode(String pBrowserIdLanguageCode) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_ID_LANGUAGE_CODE, pBrowserIdLanguageCode);
	}

	/**
	 * @return the browserCookie
	 */
	public String getBrowserCookie() {
		mBrowserCookie = (String) getPropertyValue(TRUCheckoutConstants.BROWSER_COOKIE);
		return mBrowserCookie;
	}

	/**
	 * @param pBrowserCookie the browserCookie to set
	 */
	public void setBrowserCookie(String pBrowserCookie) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_COOKIE, pBrowserCookie);
	}

	/**
	 * @return the browserReferer
	 */
	public String getBrowserReferer() {
		mBrowserReferer = (String) getPropertyValue(TRUCheckoutConstants.BROWSER_REFERER);
		return mBrowserReferer;
	}

	/**
	 * @param pBrowserReferer the browserReferer to set
	 */
	public void setBrowserReferer(String pBrowserReferer) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_REFERER, pBrowserReferer);
	}

	/**
	 * @return the customerIPAddress
	 */
	public String getCustomerIPAddress() {
		mCustomerIPAddress = (String) getPropertyValue(TRUCheckoutConstants.CUSTOMER_IP_ADDRESS);
		return mCustomerIPAddress;
	}

	/**
	 * @param pCustomerIPAddress the customerIPAddress to set
	 */
	public void setCustomerIPAddress(String pCustomerIPAddress) {
		setPropertyValue(TRUCheckoutConstants.CUSTOMER_IP_ADDRESS, pCustomerIPAddress);
	}

	/**
	 * This is to get the value for serverDateTime
	 *
	 * @return the serverDateTime value
	 */
	public Timestamp getServerDateTime() {
		mServerDateTime = (Timestamp) getPropertyValue(TRUCheckoutConstants.SERVER_DATE_TIME);
		return mServerDateTime;
	}

	/**
	 * This method to set the pServerDateTime with serverDateTime
	 * 
	 * @param pServerDateTime the serverDateTime to set
	 */
	public void setServerDateTime(Timestamp pServerDateTime) {
		setPropertyValue(TRUCheckoutConstants.SERVER_DATE_TIME, pServerDateTime);
	}
	
	/**
	 * @return the timeSpentOnSite
	 */
	public String getTimeSpentOnSite() {
		mTimeSpentOnSite = (String) getPropertyValue(TRUCheckoutConstants.TIME_SPENT_ONSITE);
		return mTimeSpentOnSite;
	}

	/**
	 * @param pTimeSpentOnSite the timeSpentOnSite to set
	 */
	public void setTimeSpentOnSite(String pTimeSpentOnSite) {
		setPropertyValue(TRUCheckoutConstants.TIME_SPENT_ONSITE, pTimeSpentOnSite);
	}

	/**
	 * This method invalidates current layaway order.
	 */
	public synchronized void invalidateLayawayOrder() {
	    this.mPaymentGroupContainer = null;
	    this.mRelationshipContainer = null;
	    this.mPaymentGroupRelationshipContainer = null;
	    this.mInitialized = false;
	}
	
	/**
	 * @return the mRemainingAmount
	 */
	public double getRemainingAmount() {
		return mRemainingAmount;
	}
	/**
	 * @param pRemainingAmount the mRemainingAmount to set
	 */
	public void setRemainingAmount(double pRemainingAmount) {
		this.mRemainingAmount = pRemainingAmount;
	}
	/**
	 * @return the mLayawayOrderid
	 */
	public String getLayawayOrderid() {
		return mLayawayOrderid;
	}

	/**
	 * @param pLayawayOrderid the mLayawayOrderid to set
	 */
	public void setLayawayOrderid(String pLayawayOrderid) {
		this.mLayawayOrderid = pLayawayOrderid;
	}

	/**
	 * @return the pCommerceId
	 */
	public String getId() {
		return mCommerceId;
	}

	/**
	 * @param pCommerceId the mCommerceId to set
	 */
	public void setId(String pCommerceId) {
		this.mCommerceId = pCommerceId;
	}
	
	/**
	 * @return the layawayRepositoryItem
	 */
	public MutableRepositoryItem getRepositoryItem() {
		return mLayawayRepositoryItem;
	}

	/**
	 * @param pLayawayRepositoryItem the layawayRepositoryItem to set
	 */
	public void setRepositoryItem(MutableRepositoryItem pLayawayRepositoryItem) {
		this.mLayawayRepositoryItem = pLayawayRepositoryItem;
	}
	
	/**
	 * get the OMSOrderId property of layaway order.
	 * @return OMSOrderId - String
	 */
	public String getLayawayOMSOrderId() {
		String layawayOMSOrderId = null;
		if(getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_OMS_ORDER_ID) != null){
			layawayOMSOrderId = (String) getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_OMS_ORDER_ID);
		}
	    return layawayOMSOrderId;
	}
	
	/**
	 * Set the OMSOrderId property in layaway order.
	 * @param pOMSOrderId OMSOrderId
	 */
	public void setLayawayOMSOrderId(String pOMSOrderId) {
	    setPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_OMS_ORDER_ID, pOMSOrderId);
	}
	
	/**
	 * get the layawayCustName property of layaway order.
	 * @return layawayCustName - String
	 */
	public String getLayawayCustName() {
		String layawayCustName = null;
		if(getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_CUST_NAME) != null){
			layawayCustName = (String) getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_CUST_NAME);
		}
	    return layawayCustName;
	}
	
	/**
	 * Set the layawayCustName property in layaway order.
	 * @param pLayawayCustName layawayCustName
	 */
	public void setLayawayCustName(String pLayawayCustName) {
	    setPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_CUST_NAME, pLayawayCustName);
	}
	
	/**
	 * get the paymentAmount property of layaway order.
	 * @return paymentAmount - double
	 */
	public double getPaymentAmount() {
		double paymentAmount = TRUConstants.DOUBLE_ZERO;
		if(getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_PAYMENT_AMOUNT) != null){
			paymentAmount = (double) getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_PAYMENT_AMOUNT);
		}
	    return paymentAmount;
	}
	
	/**
	 * Set the layawayDueAmount property in layaway order.
	 * @param pLayawayDueAmount layawayDueAmount
	 */
	public void setLayawayDueAmount(double pLayawayDueAmount) {
	    setPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_DUE_AMOUNT, pLayawayDueAmount);
	}
	
	/**
	 * get the layawayDueAmount property of layaway order.
	 * @return layawayDueAmount - double
	 */
	public double getLayawayDueAmount() {
		double layawayDueAmount = TRUConstants.DOUBLE_ZERO;
		if(getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_DUE_AMOUNT) != null){
			layawayDueAmount = (double) getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_DUE_AMOUNT);
		}
	    return layawayDueAmount;
	}
	/**
	 * Set the paymentAmount property in layaway order.
	 * @param pPaymentAmount paymentAmount
	 */
	public void setPaymentAmount(double pPaymentAmount) {
	    setPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_PAYMENT_AMOUNT, pPaymentAmount);
	}
	/**
	 * 
	 * @return String
	 */
	@Override
	public String getProfileId() {
		return (String) getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_PROFILE_ID);
	}
	/**
	 * @param pProfileId String
	 */
	@Override
	public void setProfileId(String pProfileId) {
		setPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_PROFILE_ID, pProfileId);
	}
	
	/**
	 * @return the orderClassType
	 */
	public String getOrderClassType() {
		return ((String)getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_ORDER_CLASS_TYPE));
	}

	/**
	 * @param pOrderClassType the orderClassType to set
	 */
	public void setOrderClassType(String pOrderClassType) {
		setPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_ORDER_CLASS_TYPE, pOrderClassType);
	}
	
	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return (String) getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_ORDER_SITE_ID);
	}

	/**
	 * @param pSiteId the siteId to set
	 */
	public void setSiteId(String pSiteId) {
		setPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_ORDER_SITE_ID, pSiteId);
	}
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return (String) getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_FIRST_NAME);
	}

	/**
	 * @param pFirstName the firstName to set
	 */
	public void setFirstName(String pFirstName) {
		setPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_FIRST_NAME, pFirstName);
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return (String) getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_LAST_NAME);
	}

	/**
	 * @param pLastName the lastName to set
	 */
	public void setLastName(String pLastName) {
		setPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_LAST_NAME, pLastName);
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return (String) getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_ADDRESS1);
	}

	/**
	 * @param pAddress1 the address1 to set
	 */
	public void setAddress1(String pAddress1) {
		setPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_ADDRESS1, pAddress1);
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return (String) getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_ADDRESS2);
	}

	/**
	 * @param pAddress2 the address2 to set
	 */
	public void setAddress2(String pAddress2) {
		setPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_ADDRESS2, pAddress2);
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return (String) getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_CITY);
	}

	/**
	 * @param pCity the city to set
	 */
	public void setCity(String pCity) {
		setPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_CITY, pCity);
	}

	/**
	 * @return the addrState
	 */
	public String getAddrState() {
		return (String) getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_STATE);
	}

	/**
	 * @param pAddrState the addrState to set
	 */
	public void setAddrState(String pAddrState) {
		setPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_STATE, pAddrState);
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return (String) getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_POSTAL_CODE);
	}

	/**
	 * @param pPostalCode the postalCode to set
	 */
	public void setPostalCode(String pPostalCode) {
		setPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_POSTAL_CODE, pPostalCode);
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return (String) getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_COUNTRY);
	}

	/**
	 * @param pCountry the country to set
	 */
	public void setCountry(String pCountry) {
		setPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_COUNTRY, pCountry);
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return (String) getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_PHONE_NUMBER);
	}

	/**
	 * @param pPhoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String pPhoneNumber) {
		setPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_PHONE_NUMBER, pPhoneNumber);
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return (String) getPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_EMAIL);
	}

	/**
	 * @param pEmail the email to set
	 */
	public void setEmail(String pEmail) {
		setPropertyValue(TRUCheckoutConstants.PROP_LAYAWAY_EMAIL, pEmail);
	}
	
	/**
	 * @return the mChanged
	 */
	public boolean isChanged() {
		return mChanged;
	}

	/**
	 * @param pChanged the mChanged to set
	 */
	public void setChanged(boolean pChanged) {
		this.mChanged = pChanged;
	}
	
	/**
	 * @return the Date
	 */
	@Override
	public Date getCreationDate() {
		return ((Date)getPropertyValue(PropertyNameConstants.CREATIONDATE));
	}
	
	/**
	 * @param pCreationTime the pCreationTime to set
	 */
	public void setCreationTime(long pCreationTime) {
		setPropertyValue(PropertyNameConstants.CREATIONDATE, new Timestamp(pCreationTime));
	}
	
	/**
	 * @return the Date
	 */
	@Override
	public Date getSubmittedDate() {
		return ((Date)getPropertyValue(PropertyNameConstants.SUBMITTEDDATE));
	}
	
	/**
	 * @param pSubmittedDate the pSubmittedDate to set
	 */
	public void setSubmittedDate(long pSubmittedDate) {
		setPropertyValue(PropertyNameConstants.SUBMITTEDDATE, new Timestamp(pSubmittedDate));
	}
	
	/**
	 * @param pPropertyName PropertyName
	 * @return the mutItem
	 */
	@Override
	public Object getPropertyValue(String pPropertyName) {
		MutableRepositoryItem mutItem = getRepositoryItem();
		return mutItem.getPropertyValue(pPropertyName);
	}
	
	/**
	 * @param pPropertyName the pPropertyName to set
	 * @param pPropertyValue the pPropertyValue to set
	 */
	@Override
	public void setPropertyValue(String pPropertyName, Object pPropertyValue) {
		MutableRepositoryItem mutItem = getRepositoryItem();
		mutItem.setPropertyValue(pPropertyName, pPropertyValue);
		setChanged(true);
	}

	/**
	 * Overriding OOTB ensureContainers().
	 */
	protected void ensureContainers() {
		if (this.mInitialized) {
			return;
		}
		synchronized (this) {
			if (this.mInitialized) {
				return;
			}
			this.mPaymentGroupContainer = new PaymentGroupContainerImpl(getRepositoryItem());
			this.mRelationshipContainer = new RelationshipContainerImpl(getRepositoryItem());
	        this.mPaymentGroupRelationshipContainer = new PaymentGroupRelationshipContainerImpl();

			this.mInitialized = true;
		}
	}
	/**
	 * @return List
	 */
	@Override
	public List getPaymentGroups() {
		ensureContainers();
	    return this.mPaymentGroupContainer.getPaymentGroups();
	}
	/**
	 *  Overriding OOTB  addPaymentGroup().
	 *  @param pPaymentGroup - PaymentGroup
	 *  @throws InvalidParameterException - InvalidParameterException
	 *  @throws DuplicatePaymentGroupException - DuplicatePaymentGroupException
	 */
	@Override
	public void addPaymentGroup(PaymentGroup pPaymentGroup)
			throws DuplicatePaymentGroupException, InvalidParameterException {
		ensureContainers();
		this.mPaymentGroupContainer.addPaymentGroup(pPaymentGroup);
		setChanged(true);
	}
	/**
	 *  Overriding OOTB  addPaymentGroup().
	 *   @param pPaymentGroup - PaymentGroup
	 *   @param pIndex - int
	 *   @throws InvalidParameterException - InvalidParameterException
	 *   @throws DuplicatePaymentGroupException - DuplicatePaymentGroupException
	 */
	@Override
	public void addPaymentGroup(PaymentGroup pPaymentGroup, int pIndex)
			throws DuplicatePaymentGroupException, InvalidParameterException {
		ensureContainers();
		this.mPaymentGroupContainer.addPaymentGroup(pPaymentGroup, pIndex);
		setChanged(true);
	}
	/**
	 * @param pPaymentGroupId - PaymentGroupId
	 * @return PaymentGroup
	 * @throws PaymentGroupNotFoundException - PaymentGroupNotFoundException
	 * @throws InvalidParameterException - InvalidParameterException
	 */
	@Override
	public PaymentGroup removePaymentGroup(String pPaymentGroupId)
			throws PaymentGroupNotFoundException, InvalidParameterException {
		ensureContainers();
	    PaymentGroup pg = this.mPaymentGroupContainer.removePaymentGroup(pPaymentGroupId);
	    setChanged(true);
		return pg;
	}
	/**
	 *  Overriding OOTB  removeAllPaymentGroups().
	 */
	@Override
	public void removeAllPaymentGroups() {
		ensureContainers();
	    this.mPaymentGroupContainer.removeAllPaymentGroups();
	    setChanged(true);
	}
	/**
	 *  Overriding OOTB  removeAllPaymentGroups().
	 *  @param pPaymentGroupId PaymentGroupId
	 *  @throws PaymentGroupNotFoundException - PaymentGroupNotFoundException
	 *  @throws InvalidParameterException - InvalidParameterException
	 *  @return PaymentGroup
	 */
	@Override
	public PaymentGroup getPaymentGroup(String pPaymentGroupId)
			throws PaymentGroupNotFoundException, InvalidParameterException {
		ensureContainers();
	    return this.mPaymentGroupContainer.getPaymentGroup(pPaymentGroupId);
	}
	/**
	 *  Overriding OOTB  removeAllPaymentGroups().
	 *  @return int
	 */
	@Override
	public int getPaymentGroupCount() {
		ensureContainers();
	    return this.mPaymentGroupContainer.getPaymentGroupCount();
	}
	/**
	 *  Overriding OOTB  removeAllPaymentGroups().
	 *  @return List
	 */
	@Override
	public List getRelationships() {
		ensureContainers();
		return this.mRelationshipContainer.getRelationships();
	}
	/**
	 *  Overriding OOTB  removeAllPaymentGroups().
	 *  @param pRelationship -  Relationship
	 *  @throws DuplicateRelationshipException - DuplicateRelationshipException
	 *  @throws InvalidParameterException - InvalidParameterException
	 */
	@Override
	public void addRelationship(Relationship pRelationship)
			throws DuplicateRelationshipException, InvalidParameterException {
		ensureContainers();
		this.mRelationshipContainer.addRelationship(pRelationship);
	}
	/**
	 *  Overriding OOTB  removeAllPaymentGroups().
	 *  @param pRelationship - Relationship
	 *  @param pIndex - Index
	 *  @throws DuplicateRelationshipException - DuplicateRelationshipException
	 *  @throws InvalidParameterException - InvalidParameterException
	 */
	@Override
	public void addRelationship(Relationship pRelationship, int pIndex)
			throws DuplicateRelationshipException, InvalidParameterException {
		ensureContainers();
		this.mRelationshipContainer.addRelationship(pRelationship, pIndex);
		setChanged(true);
	}
	/**
	 *  Overriding OOTB  removeAllPaymentGroups().
	 *  @param pRelationshipId - String
	 *  @throws RelationshipNotFoundException - RelationshipNotFoundException
	 *  @throws InvalidParameterException - InvalidParameterException
	 *  @return Relationship - Relationship
	 */
	@Override
	public Relationship removeRelationship(String pRelationshipId)
			throws RelationshipNotFoundException, InvalidParameterException {
		ensureContainers();
		Relationship r = this.mRelationshipContainer.removeRelationship(pRelationshipId);
		setChanged(true);
		return r;
	}
	/**
	 *  Overriding OOTB  removeAllPaymentGroups().
	 */
	@Override
	public void removeAllRelationships() {
		ensureContainers();
		this.mRelationshipContainer.removeAllRelationships();
		setChanged(true);
	}
	/**
	 *  Overriding OOTB  removeAllPaymentGroups().
	 *  @param pRelationshipId -String
	 *  @throws RelationshipNotFoundException - RelationshipNotFoundException
	 *  @throws InvalidParameterException - InvalidParameterException
	 *  @return Relationship - Relationship
	 */
	@Override
	public Relationship getRelationship(String pRelationshipId)
			throws RelationshipNotFoundException, InvalidParameterException {
		ensureContainers();
		return this.mRelationshipContainer.getRelationship(pRelationshipId);
	}
	/**
	 *  Overriding OOTB  removeAllPaymentGroups().
	 *  @return int
	 */
	@Override
	public int getRelationshipCount() {
		ensureContainers();
		return this.mRelationshipContainer.getRelationshipCount();
	}
	/**
	 *  Overriding OOTB  removeAllPaymentGroups().
	 *  @return List
	 */
	@Override
	public List getPaymentGroupRelationships() {
		ensureContainers();
		return this.mPaymentGroupRelationshipContainer.getPaymentGroupRelationships();
	}
	/**
	 *  Overriding OOTB  addPaymentGroupRelationship().
	 *  @param pPaymentGroupRelationship - PaymentGroupRelationship
	 *  @throws DuplicateRelationshipException - DuplicateRelationshipException
	 *  @throws InvalidParameterException - InvalidParameterException
	 */
	@Override
	public void addPaymentGroupRelationship(PaymentGroupRelationship pPaymentGroupRelationship)
			throws DuplicateRelationshipException, InvalidParameterException {
		ensureContainers();
		this.mPaymentGroupRelationshipContainer.addPaymentGroupRelationship(pPaymentGroupRelationship);
		setChanged(true);
	}
	/**
	 *  Overriding OOTB  addPaymentGroupRelationship().
	 *  @param pPaymentGroupRelationship - PaymentGroupRelationship
	 *  @param pIndex - int
	 *  @throws DuplicateRelationshipException - DuplicateRelationshipException
	 *  @throws InvalidParameterException - InvalidParameterException
	 */
	@Override
	public void addPaymentGroupRelationship(PaymentGroupRelationship pPaymentGroupRelationship, int pIndex)
			throws DuplicateRelationshipException, InvalidParameterException {
		ensureContainers();
		this.mPaymentGroupRelationshipContainer.addPaymentGroupRelationship(pPaymentGroupRelationship, pIndex);
		setChanged(true);
	}
	/**
	 *  Overriding OOTB  removePaymentGroupRelationship().
	 *  @param pPaymentGroupRelationshipId - String
	 *  @throws RelationshipNotFoundException - RelationshipNotFoundException
	 *  @throws InvalidParameterException - InvalidParameterException
	 *  @return PaymentGroupRelationship - PaymentGroupRelationship
	 */
	@Override
	public PaymentGroupRelationship removePaymentGroupRelationship(String pPaymentGroupRelationshipId)
			throws RelationshipNotFoundException, InvalidParameterException {
		ensureContainers();
		PaymentGroupRelationship pgr = this.mPaymentGroupRelationshipContainer.removePaymentGroupRelationship(pPaymentGroupRelationshipId);
		setChanged(true);
		return pgr;
	}
	/**
	 *  Overriding OOTB  removeAllPaymentGroupRelationships().
	 */
	@Override
	public void removeAllPaymentGroupRelationships() {
		ensureContainers();
		this.mPaymentGroupRelationshipContainer.removeAllPaymentGroupRelationships();
		setChanged(true);
	}
	/**
	 *  Overriding OOTB  getPaymentGroupRelationship().
	 *  @param pPaymentGroupRelationshipId - String
	 *  @throws RelationshipNotFoundException - RelationshipNotFoundException
	 *  @throws InvalidParameterException - InvalidParameterException
	 *  @return PaymentGroupRelationship - PaymentGroupRelationship
	 */
	@Override
	public PaymentGroupRelationship getPaymentGroupRelationship(String pPaymentGroupRelationshipId)
			throws RelationshipNotFoundException, InvalidParameterException {
		ensureContainers();
		PaymentGroupRelationship pgr = this.mPaymentGroupRelationshipContainer.getPaymentGroupRelationship(pPaymentGroupRelationshipId);
		setChanged(true);
		return pgr;
	}
	/**
	 *  Overriding OOTB  getPaymentGroupRelationshipCount().
	 *  @return int
	 */
	@Override
	public int getPaymentGroupRelationshipCount() {
		ensureContainers();
		return this.mPaymentGroupRelationshipContainer.getPaymentGroupRelationshipCount();
	}
	/**
	 *  Overriding OOTB  getCommerceItems().
	 *  @return List
	 */
	@Override
	public List getCommerceItems() {
		return null;
	}
	
	/**
	 * This method addCommerceItem.
	 * 
	 * @param pParamCommerceItem -  CommerceItem
	 * @throws DuplicateCommerceItemException -  DuplicateCommerceItemException
	 * @throws InvalidParameterException - InvalidParameterException
	 */
	@Override
	public void addCommerceItem(CommerceItem pParamCommerceItem)
			throws DuplicateCommerceItemException, InvalidParameterException {
		//Overridden method 
	}
	
	/**
	 * This method addCommerceItem.
	 * 
	 * @param pParamCommerceItem - CommerceItem
	 * @param pParamInt - paramint
	 * @throws DuplicateCommerceItemException -  DuplicateCommerceItemException
	 * @throws InvalidParameterException -  InvalidParameterException
	 */
	@Override
	public void addCommerceItem(CommerceItem pParamCommerceItem, int pParamInt)
			throws DuplicateCommerceItemException, InvalidParameterException {
		//Overridden method 
	}
	
	/**
	 * This method removesallcommerceItems.
	 * 
	 * @param pParamString - paramString
	 * @return CommerceItem
	 * @throws CommerceItemNotFoundException CommerceItemNotFoundException
	 * @throws InvalidParameterException InvalidParameterException
	 */
	@Override
	public CommerceItem removeCommerceItem(String pParamString)
			throws CommerceItemNotFoundException, InvalidParameterException {
		return null;
	}
	/**
	 * his method removeAllCommerceItems.
	 */
	@Override
	public void removeAllCommerceItems() {
		//Overridden method 
	}
	
	/**
	 * This method getCommerceItem.
	 * 
	 * @param pParamString -  paramString
	 * @return CommerceItem
	 * @throws CommerceItemNotFoundException -  CommerceItemNotFoundException
	 * @throws InvalidParameterException - InvalidParameterException
	 */
	@Override
	public CommerceItem getCommerceItem(String pParamString)
			throws CommerceItemNotFoundException, InvalidParameterException {
		return null;
	}
	
	/**
	 * @param pParamString - paramString
	 * @return CommerceItem
	 * @throws CommerceItemNotFoundException -  CommerceItemNotFoundException
	 * @throws InvalidParameterException - InvalidParameterException
	 */
	@Override
	public List getCommerceItemsByCatalogRefId(String pParamString)
			throws CommerceItemNotFoundException, InvalidParameterException {
		return null;
	}
	/**
	 * his method removeAllCommerceItems.
	 * @return int
	 */
	@Override
	public int getCommerceItemCount() {
		return 0;
	}
	/**
	 * his method getTotalCommerceItemCount.
	 * @return long
	 */
	@Override
	public long getTotalCommerceItemCount() {
		return 0;
	}
	/**
	 * his method getShippingGroups.
	 * @return List
	 */
	@Override
	public List getShippingGroups() {
		return null;
	}
	
	/**
	 * This method addCommerceItem.
	 * 
	 * @param pParamShippingGroup - ShippingGroup
	 * @throws DuplicateShippingGroupException - DuplicateShippingGroupException
	 * @throws InvalidParameterException  - InvalidParameterException
	 */
	@Override
	public void addShippingGroup(ShippingGroup pParamShippingGroup)
			throws DuplicateShippingGroupException, InvalidParameterException {
		//Overridden method 
	}
	
	/**
	 * @param pParamShippingGroup - ShippingGroup
	 * @param pParamInt - paramInt
	 * @throws DuplicateShippingGroupException - DuplicateShippingGroupException
	 * @throws InvalidParameterException - InvalidParameterException
	 */
	@Override
	public void addShippingGroup(ShippingGroup pParamShippingGroup, int pParamInt)
			throws DuplicateShippingGroupException, InvalidParameterException {
		//Overridden method 
	}
	
	/**
	 * @param pParamString ParamString
	 * @return ShippingGroup
	 * @throws ShippingGroupNotFoundException ShippingGroupNotFoundException
	 * @throws InvalidParameterException InvalidParameterException
	 */
	@Override
	public ShippingGroup removeShippingGroup(String pParamString)
			throws ShippingGroupNotFoundException, InvalidParameterException {
		return null;
	}
	/**
	 * this method removeAllShippingGroups.
	 */
	@Override
	public void removeAllShippingGroups() {
		//Overridden method
	}
	
	/**
	 * @param pParamString ParamString
	 * @return ShippingGroup
	 * @throws ShippingGroupNotFoundException ShippingGroupNotFoundException
	 * @throws InvalidParameterException InvalidParameterException
	 */
	@Override
	public ShippingGroup getShippingGroup(String pParamString)
			throws ShippingGroupNotFoundException, InvalidParameterException {
		return null;
	}
	/**
	 * this method getShippingGroupCount.
	 * @return int.
	 */
	@Override
	public int getShippingGroupCount() {
		return 0;
	}
	/**
	 * this method getCostCenters.
	 * @return List.
	 */
	@Override
	public List getCostCenters() {
		return null;
	}
	
	/**
	 * @param pParamCostCenter ParamCostCenter
	 * @throws DuplicateCostCenterException DuplicateCostCenterException
	 * @throws InvalidParameterException InvalidParameterException
	 */
	@Override
	public void addCostCenter(CostCenter pParamCostCenter)
			throws DuplicateCostCenterException, InvalidParameterException {
		//Overridden method 
	}
	
	/**
	 * @param pParamCostCenter ParamCostCenter
	 * @param pParamInt ParamInt
	 * @throws DuplicateCostCenterException DuplicateCostCenterException
	 * @throws InvalidParameterException InvalidParameterException
	 */
	@Override
	public void addCostCenter(CostCenter pParamCostCenter, int pParamInt)
			throws DuplicateCostCenterException, InvalidParameterException {
		//Overridden method 
	}
	
	/**
	 * @param pParamString ParamString
	 * @return CostCenter
	 * @throws DuplicateCostCenterException DuplicateCostCenterException
	 * @throws InvalidParameterException InvalidParameterException
	 */
	@Override
	public CostCenter removeCostCenter(String pParamString)
			throws CostCenterNotFoundException, InvalidParameterException {
		return null;
	}
	/**
	 * this method removeAllCostCenters.
	 */
	@Override
	public void removeAllCostCenters() {
		//Overridden method 
	}
	
	/**
	 * @param pParamString ParamString
	 * @return CostCenter
	 * @throws CostCenterNotFoundException CostCenterNotFoundException
	 * @throws InvalidParameterException InvalidParameterException
	 */
	@Override
	public CostCenter getCostCenter(String pParamString)
			throws CostCenterNotFoundException, InvalidParameterException {
		return null;
	}
	/**
	 * this method removeAllCostCenters.
	 * @return int
	 */
	@Override
	public int getCostCenterCount() {
		return 0;
	}
	/**
	 * this method getCostCenterRelationships.
	 * @return List
	 */
	@Override
	public List getCostCenterRelationships() {
		return null;
	}
	
	/**
	 * @param pParamCostCenterRelationship CostCenterRelationship
	 * @throws DuplicateRelationshipException DuplicateRelationshipException
	 * @throws InvalidParameterException InvalidParameterException
	 */
	@Override
	public void addCostCenterRelationship(
			CostCenterRelationship pParamCostCenterRelationship)
			throws DuplicateRelationshipException, InvalidParameterException {
		//Overridden method 
	}
	
	/**
	 * @param pParamCostCenterRelationship CostCenterRelationship
	 * @param pParamInt paramInt
	 * @throws DuplicateRelationshipException DuplicateRelationshipException
	 * @throws InvalidParameterException InvalidParameterException
	 */
	@Override
	public void addCostCenterRelationship(
			CostCenterRelationship pParamCostCenterRelationship, int pParamInt)
			throws DuplicateRelationshipException, InvalidParameterException {
		//Overridden method 
	}
	
	/**
	 * @param pParamString pParamString
	 * @throws RelationshipNotFoundException RelationshipNotFoundException
	 * @throws InvalidParameterException InvalidParameterException
	 * @return null;
	 */
	@Override
	public CostCenterRelationship removeCostCenterRelationship(
			String pParamString) throws RelationshipNotFoundException,
			InvalidParameterException {
		return null;
	}
	/**
	 * this method removeAllCostCenterRelationships.
	 */
	@Override
	public void removeAllCostCenterRelationships() {
		//Overridden method 
	}
	
	/**
	 * @param pParamString pParamString
	 * @throws RelationshipNotFoundException RelationshipNotFoundException
	 * @throws InvalidParameterException InvalidParameterException
	 * @return null;
	 */
	@Override
	public CostCenterRelationship getCostCenterRelationship(String pParamString)
			throws RelationshipNotFoundException, InvalidParameterException {
		return null;
	}

	/**
	 * this method getCostCenterRelationshipCount.
	 * @return int
	 */
	@Override
	public int getCostCenterRelationshipCount() {
		return 0;
	}
	
	/**
	 * @param pO Observable
	 * @param pArg Object
	 */
	@Override
	public void update(Observable pO, Object pArg) {
		//Overridden method 
	}
	/**
	 * this method getSaveAllProperties.
	 * @return boolean
	 */
	@Override
	public boolean getSaveAllProperties() {
		return false;
	}
	
	/**
	 * @param pParamBoolean boolean
	 */
	@Override
	public void setSaveAllProperties(boolean pParamBoolean) {
		//Overridden method 
	}

	/**
	 * this method getChangedProperties.
	 * @return Set
	 */
	@Override
	public Set getChangedProperties() {
		return null;
	}
	
	/**
	 * @param pParamString String
	 */
	@Override
	public void addChangedProperty(String pParamString) {
		//Overridden method 
	}
	/**
	 * this method clearChangedProperties.
	 */
	@Override
	public void clearChangedProperties() {
		//Overridden method 
	}
	/**
	 * this method getDescription.
	 * @return String
	 */
	@Override
	public String getDescription() {
		return null;
	}
	
	/**
	 * @param pParamString String
	 */
	@Override
	public void setDescription(String pParamString) {
		//Overridden method 
	}

	@Override
	public int getState() {
		return 0;
	}
	
	/**
	 * @param pParamInt int
	 */
	@Override
	public void setState(int pParamInt) {
		//Overridden method 
	}
	/**
	 * @return String
	 */
	@Override
	public String getStateDetail() {
		return null;
	}
	
	/**
	 * @param pParamString paramString
	 */
	@Override
	public void setStateDetail(String pParamString) {
		//Overridden method 
	}
	/**
	 * @return OrderPriceInfo
	 */
	@Override
	public OrderPriceInfo getPriceInfo() {
		return null;
	}
	/**
	 * @param pParamOrderPriceInfo OrderPriceInfo
	 */
	@Override
	public void setPriceInfo(OrderPriceInfo pParamOrderPriceInfo) {
		//Overridden method 
	}
	/**
	 * @return TaxPriceInfo
	 */
	@Override
	public TaxPriceInfo getTaxPriceInfo() {
		return null;
	}
	/**
	 * @param pParamTaxPriceInfo TaxPriceInfo
	 */
	@Override
	public void setTaxPriceInfo(TaxPriceInfo pParamTaxPriceInfo) {
		//Overridden method 
	}
	/**
	 * @return String
	 */
	@Override
	public String getCreatedByOrderId() {
		return null;
	}
	/**
	 * @param pParamString String
	 */
	@Override
	public void setCreatedByOrderId(String pParamString) {
		//Overridden method 
	}
	/**
	 * @return List
	 */
	@Override
	public List getRelatedOrders() {
		return null;
	}
	/**
	 * @param pParamString String
	 */
	@Override
	public void addOrderIdToRelatedOrders(String pParamString) {
		//Overridden method 
	}
	/**
	 * @param pParamLong long
	 */
	@Override
	public void setSubmittedTime(long pParamLong) {
		//Overridden method 
	}
	/**
	 * @return long
	 */
	@Override
	public long getSubmittedTime() {
		return 0;
	}
	/**
	 * @return long
	 */
	@Override
	public long getCreationTime() {
		return 0;
	}
	/**
	 * @return Date
	 */
	@Override
	public Date getLastModifiedDate() {
		return null;
	}
	/**
	 * @param pParamLong long
	 */
	@Override
	public void setLastModifiedTime(long pParamLong) {
		//Overridden method 
	}
	/**
	 * @return long
	 */
	@Override
	public long getLastModifiedTime() {
		return 0;
	}
	/**
	 * @return Date
	 */
	@Override
	public Date getCompletedDate() {
		return null;
	}
	/**
	 * @param pParamLong long
	 */
	@Override
	public void setCompletedTime(long pParamLong) {
		//Overridden method 
	}
	/**
	 * @return long
	 */
	@Override
	public long getCompletedTime() {
		return 0;
	}
	/**
	 * @return boolean
	 */
	@Override
	public boolean isTransient() {
		MutableRepositoryItem mutItem = getRepositoryItem();
	    if (mutItem != null) {
	    	  return mutItem.isTransient();
	    }
	  return false;
	}
	/**
	 * @param pParamBoolean boolean
	 */
	@Override
	public void setTransient(boolean pParamBoolean) {
		//Overridden method 
	}
	/**
	 * @return Map
	 */
	@Override
	public Map getSpecialInstructions() {
		return null;
	}
	/**
	 * @return Map
	 */
	@Override
	public Map getTransientData() {
		return null;
	}
	/**
	 * @param pParamString String
	 */
	@Override
	public void setOriginOfOrder(String pParamString) {
		//Overridden method 
	}
	/**
	 * @return String
	 */
	@Override
	public String getOriginOfOrder() {
		return null;
	}
	/**
	 * @param pParamBoolean boolean
	 */
	@Override
	public void setExplicitlySaved(boolean pParamBoolean) {
		//Overridden method 
	}
	/**
	 * @return boolean
	 */
	@Override
	public boolean isExplicitlySaved() {
		return false;
	}
	/**
	 * @return List
	 */
	@Override
	public List getManualAdjustments() {
		return null;
	}
	/**
	 * @param pParamList List
	 */
	@Override
	public void setManualAdjustments(List pParamList) {
		//Overridden method 
	}
	/**
	 * @return RepositoryItem
	 */
	@Override
	public RepositoryItem getQuoteInfo() {
		return null;
	}
	/**
	 * @param pParamRepositoryItem RepositoryItem
	 */
	@Override
	public void setQuoteInfo(RepositoryItem pParamRepositoryItem) {
		//Overridden method 
	}
	/**
	 * @return String
	 */
	@Override
	public String getActiveQuoteOrderId() {
		return null;
	}
	/**
	 * @param pParamString String
	 */
	@Override
	public void setActiveQuoteOrderId(String pParamString) {
		//Overridden method 
	}
	/**
	 * @return String
	 */
	@Override
	public String getAgentId() {
		return null;
	}
	/**
	 * @param pParamString String
	 */
	@Override
	public void setAgentId(String pParamString) {
		//Overridden method 
	}
	/**
	 * @return String
	 */
	@Override
	public String getSalesChannel() {
		return null;
	}
	/**
	 * @param pParamString String
	 */
	@Override
	public void setSalesChannel(String pParamString) {
		//Overridden method 
	}
	/**
	 * @return String
	 */
	@Override
	public String getCreationSiteId() {
		return null;
	}
	/**
	 * @param pParamString String
	 */
	@Override
	public void setCreationSiteId(String pParamString) {
		//Overridden method 
	}
	/**
	 * @return List
	 */
	@Override
	public List getApproverIds() {
		return null;
	}
	/**
	 * @param pParamList List
	 */
	@Override
	public void setApproverIds(List pParamList) {
		//Overridden method 
	}
	/**
	 * @return List
	 */
	@Override
	public List getAuthorizedApproverIds() {
		return null;
	}
	/**
	 * @param pParamList List
	 */
	@Override
	public void setAuthorizedApproverIds(List pParamList) {
		//Overridden method 
	}
	/**
	 * @return List
	 */
	@Override
	public List getApprovalSystemMessages() {
		return null;
	}
	/**
	 * @param pParamList List
	 */
	@Override
	public void setApprovalSystemMessages(List pParamList) {
		//Overridden method 
	}
	/**
	 * @return List
	 */
	@Override
	public List getApproverMessages() {
		return null;
	}
	/**
	 * @param pParamList List
	 */
	@Override
	public void setApproverMessages(List pParamList) {
		//Overridden method 
	}
	/**
	 * @return String
	 */
	@Override
	public String getOrganizationId() {
		return null;
	}
	/**
	 * @param pParamString String
	 */
	@Override
	public void setOrganizationId(String pParamString) {
		//Overridden method 
	}
	/**
	 * get the sourceOfOrder property of order.
	 * @return string
	 */
	public String getSourceOfOrder() {
	    return (String) getPropertyValue(TRUCheckoutConstants.SOURCE_OF_ORDER);
	}
	
	/**
	 * Set the sourceOfOrder property in order.
	 * @param pSourceOfOrder sourceOfOrder
	 */
	public void setSourceOfOrder(String pSourceOfOrder) {
	    setPropertyValue(TRUCheckoutConstants.SOURCE_OF_ORDER, pSourceOfOrder);
	}
	/**
 	 * get the enteredBy property of order.
 	 * @return string
 	 */
 	public String getEnteredBy() {
 	    return (String) getPropertyValue(TRUCheckoutConstants.ENTERED_BY);
 	}
 	
 	/**
 	 * Set the enteredBy property in order.
 	 * @param pEnteredBy enteredBy
 	 */
 	public void setEnteredBy(String pEnteredBy) {
 	    setPropertyValue(TRUCheckoutConstants.ENTERED_BY, pEnteredBy);
 	}
 	/**
 	 * get the enteredLocation property of order.
 	 * @return string
 	 */
 	public String getEnteredLocation() {
 	    return (String) getPropertyValue(TRUCheckoutConstants.ENTERED_LOCATION);
 	}
 	
 	/**
 	 * Set the enteredLocation property in order.
 	 * @param pEnteredLocation enteredLocation
 	 */
 	public void setEnteredLocation(String pEnteredLocation) {
 	    setPropertyValue(TRUCheckoutConstants.ENTERED_LOCATION, pEnteredLocation);
 	}

 	@Override
	public CommerceItem getCommerceItem(String pArg0, boolean pArg1)
			throws CommerceItemNotFoundException, InvalidParameterException {
		// TODO Auto-generated method stub
		return null;
	}

 	@Override
	public double getTotalCommerceItemCountWithFraction() {
		// TODO Auto-generated method stub
		return 0;
	}

 	@Override
	public String getConfiguratorId() {
		// TODO Auto-generated method stub
		return null;
	}

 	@Override
	public void setConfiguratorId(String pArg0) {
		// TODO Auto-generated method stub
		
	}

 	/**
	 * @return the hostName
	 */
	public String getHostName() {
		mHostName = (String) getPropertyValue(TRUCheckoutConstants.HOST_NAME);
		return mHostName ;
	}

	/**
	 * @param pHostName the hostName to set
	 */
	public void setHostName(String pHostName) {
		setPropertyValue(TRUCheckoutConstants.HOST_NAME, pHostName);
	}

	/**
	 * This method is used to get retryCount.
	 * @return retryCount String
	 */
	public int getRetryCount() {
		return mRetryCount;
	}

	/**
	 *This method is used to set retryCount.
	 *@param pRetryCount String
	 */
	public void setRetryCount(int pRetryCount) {
		mRetryCount = pRetryCount;
	}

	/**
	 * This method is used to get previousResponseCode.
	 * @return mPreviousResponseCode String
	 */
	public String getPreviousResponseCode() {
		return mPreviousResponseCode;
	}

	/**
	 *This method is used to set previousResponseCode.
	 *@param pPreviousResponseCode String
	 */
	public void setPreviousResponseCode(String pPreviousResponseCode) {
		mPreviousResponseCode = pPreviousResponseCode;
	}

	/**
	 * This method is used to get previousCreditCardNumber.
	 * @return mPreviousCreditCardNumber String
	 */
	public String getPreviousCreditCardNumber() {
		return mPreviousCreditCardNumber;
	}

	/**
	 *This method is used to set previousCreditCardNumber.
	 *@param pPreviousCreditCardNumber String
	 */
	public void setPreviousCreditCardNumber(String pPreviousCreditCardNumber) {
		mPreviousCreditCardNumber = pPreviousCreditCardNumber;
	}
	
	
}
