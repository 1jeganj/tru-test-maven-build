package com.tru.commerce.order.vo;

import java.io.Serializable;


/**
 * 
 * The class TRUGiftItemInfo implements Cloneable.
 * @author Professional Access.
 * @version 1.0.
 */
public class TRUGiftItemInfo implements Cloneable,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mShippingGroupId;
	private String mShippingGroupName;

	private String mCommItemRelationshipId;

	private String mParentCommerceId;
	private long mParentQuantity;
	private String mProductId;
	private String mProductDisplayName;
	private String mProductImage;
	private String mProductColor;
	private String mProductSize;
	private String mSkuId;
	private int mQuantity;

	private String mWrapProductId;
	private String mWrapSkuId;
	private String mWrapCommerceItemId;
	private boolean mGiftItem;
	private boolean mGiftWrapEligible;
	
	/** The m channel. */
	private String mChannel;
	
	/**
	 * @return the channel
	 */
	public String getChannel() {
		return mChannel;
	}
	/**
	 * @param pChannel the channel to set
	 */
	public void setChannel(String pChannel) {
		mChannel = pChannel;
	}
	
	/**
	 * @return the giftWrapEligible
	 */
	public boolean isGiftWrapEligible() {
		return mGiftWrapEligible;
	}
	/**
	 * @param pGiftWrapEligible the giftWrapEligible to set
	 */
	public void setGiftWrapEligible(boolean pGiftWrapEligible) {
		mGiftWrapEligible = pGiftWrapEligible;
	}
	/**
	 * @return the giftItem
	 */
	public boolean isGiftItem() {
		return mGiftItem;
	}
	/**
	 * @param pGiftItem the giftItem to set
	 */
	public void setGiftItem(boolean pGiftItem) {
		mGiftItem = pGiftItem;
	}
	/**
	 * @return the productColor
	 */
	public String getProductColor() {
		return mProductColor;
	}
	/**
	 * @param pProductColor the productColor to set
	 */
	public void setProductColor(String pProductColor) {
		mProductColor = pProductColor;
	}
	/**
	 * @return the productSize
	 */
	public String getProductSize() {
		return mProductSize;
	}
	/**
	 * @param pProductSize the productSize to set
	 */
	public void setProductSize(String pProductSize) {
		mProductSize = pProductSize;
	}
	/**
	 * @return the shippingGroupName
	 */
	public String getShippingGroupName() {
		return mShippingGroupName;
	}
	/**
	 * @param pShippingGroupName the shippingGroupName to set
	 */
	public void setShippingGroupName(String pShippingGroupName) {
		mShippingGroupName = pShippingGroupName;
	}
	/**
	 * @return the productDisplayName
	 */
	public String getProductDisplayName() {
		return mProductDisplayName;
	}
	/**
	 * @param pProductDisplayName the productDisplayName to set
	 */
	public void setProductDisplayName(String pProductDisplayName) {
		mProductDisplayName = pProductDisplayName;
	}
	/**
	 * @return the productImage
	 */
	public String getProductImage() {
		return mProductImage;
	}
	/**
	 * @param pProductImage the productImage to set
	 */
	public void setProductImage(String pProductImage) {
		mProductImage = pProductImage;
	}
	/**
	 * @return the parentQuantity
	 */
	public long getParentQuantity() {
		return mParentQuantity;
	}
	/**
	 * @param pParentQuantity the parentQuantity to set
	 */
	public void setParentQuantity(long pParentQuantity) {
		mParentQuantity = pParentQuantity;
	}
	/**
	 * @return the shippingGroupId
	 */
	public String getShippingGroupId() {
		return mShippingGroupId;
	}
	/**
	 * @param pShippingGroupId the shippingGroupId to set
	 */
	public void setShippingGroupId(String pShippingGroupId) {
		mShippingGroupId = pShippingGroupId;
	}
	/**
	 * @return the commItemRelationshipId
	 */
	public String getCommItemRelationshipId() {
		return mCommItemRelationshipId;
	}
	/**
	 * @param pCommItemRelationshipId the commItemRelationshipId to set
	 */
	public void setCommItemRelationshipId(String pCommItemRelationshipId) {
		mCommItemRelationshipId = pCommItemRelationshipId;
	}
	/**
	 * @return the parentCommerceId
	 */
	public String getParentCommerceId() {
		return mParentCommerceId;
	}
	/**
	 * @param pParentCommerceId the parentCommerceId to set
	 */
	public void setParentCommerceId(String pParentCommerceId) {
		mParentCommerceId = pParentCommerceId;
	}
	/**
	 * @return the productId
	 */
	public String getProductId() {
		return mProductId;
	}
	/**
	 * @param pProductId the productId to set
	 */
	public void setProductId(String pProductId) {
		mProductId = pProductId;
	}
	/**
	 * @return the skuId
	 */
	public String getSkuId() {
		return mSkuId;
	}
	/**
	 * @param pSkuId the skuId to set
	 */
	public void setSkuId(String pSkuId) {
		mSkuId = pSkuId;
	}
	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return mQuantity;
	}
	/**
	 * @param pQuantity the quantity to set
	 */
	public void setQuantity(int pQuantity) {
		mQuantity = pQuantity;
	}
	/**
	 * @return the wrapProductId
	 */
	public String getWrapProductId() {
		return mWrapProductId;
	}
	/**
	 * @param pWrapProductId the wrapProductId to set
	 */
	public void setWrapProductId(String pWrapProductId) {
		mWrapProductId = pWrapProductId;
	}
	/**
	 * @return the wrapSkuId
	 */
	public String getWrapSkuId() {
		return mWrapSkuId;
	}
	/**
	 * @param pWrapSkuId the wrapSkuId to set
	 */
	public void setWrapSkuId(String pWrapSkuId) {
		mWrapSkuId = pWrapSkuId;
	}
	/**
	 * @return the wrapCommerceItemId
	 */
	public String getWrapCommerceItemId() {
		return mWrapCommerceItemId;
	}
	/**
	 * @param pWrapCommerceItemId the wrapCommerceItemId to set
	 */
	public void setWrapCommerceItemId(String pWrapCommerceItemId) {
		mWrapCommerceItemId = pWrapCommerceItemId;
	}
	/**
	 * @return the clone object
	 * @throws CloneNotSupportedException - CloneNotSupportedException
	 */
	public Object clone()throws CloneNotSupportedException{  
		return super.clone();  
	}
}
