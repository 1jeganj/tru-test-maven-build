package com.tru.commerce.order.vo;

import java.io.Serializable;
import java.util.Map;

import atg.core.util.Address;

/**
 * The class TRUGiftOptionsInfo implements Serializable.
 * @author Professional Access.
 * @version 1.0.
 *
 *
 */
public class TRUGiftOptionsInfo implements Serializable{
	/**
	 * Generated serialVersionUID
	 */
	private static final long serialVersionUID = -5051226324903238538L;
	private String mGiftWrapMessage;
	private boolean mGiftReceipt;
	private String mShippingGroupId;
	private Address mShippingAddress;
	private boolean mRegistryAddress;
	private Map<String, TRUGiftItemInfo> mEligibleGiftItems;
	private Map<String, TRUGiftItemInfo> mNonEligibleGiftItems;
	private Map<String, TRUGiftItemInfo> mGiftWrappedItems;
	private String mShippingAdressNickName;
	private String mGiftOptionId;
	
	/**
	 * @return the giftOptionId
	 */
	public String getGiftOptionId() {
		return mGiftOptionId;
	}
	/**
	 * @param pGiftOptionId the giftOptionId to set
	 */
	public void setGiftOptionId(String pGiftOptionId) {
		mGiftOptionId = pGiftOptionId;
	}
	/**
	 * @return the shippingAdressNickName
	 */
	public String getShippingAdressNickName() {
		return mShippingAdressNickName;
	}
	/**
	 * @param pShippingAdressNickName the shippingAdressNickName to set
	 */
	public void setShippingAdressNickName(String pShippingAdressNickName) {
		mShippingAdressNickName = pShippingAdressNickName;
	}
	/**
	 * @return the giftWrappedItems
	 */
	public Map<String, TRUGiftItemInfo> getGiftWrappedItems() {
		return mGiftWrappedItems;
	}
	/**
	 * @param pGiftWrappedItems the giftWrappedItems to set
	 */
	public void setGiftWrappedItems(Map<String, TRUGiftItemInfo> pGiftWrappedItems) {
		mGiftWrappedItems = pGiftWrappedItems;
	}
	/**
	 * @return the shippingAddress
	 */
	public Address getShippingAddress() {
		return mShippingAddress;
	}
	/**
	 * @param pShippingAddress the shippingAddress to set
	 */
	public void setShippingAddress(Address pShippingAddress) {
		mShippingAddress = pShippingAddress;
	}
	/**
	 * @return the registryAddress
	 */
	public boolean isRegistryAddress() {
		return mRegistryAddress;
	}
	/**
	 * @param pRegistryAddress the registryAddress to set
	 */
	public void setRegistryAddress(boolean pRegistryAddress) {
		mRegistryAddress = pRegistryAddress;
	}
	/**
	 * @return the giftWrapMessage
	 */
	public String getGiftWrapMessage() {
		return mGiftWrapMessage;
	}
	/**
	 * @param pGiftWrapMessage the giftWrapMessage to set
	 */
	public void setGiftWrapMessage(String pGiftWrapMessage) {
		mGiftWrapMessage = pGiftWrapMessage;
	}
	/**
	 * @return the giftReceipt
	 */
	public boolean isGiftReceipt() {
		return mGiftReceipt;
	}
	/**
	 * @param pGiftReceipt the giftReceipt to set
	 */
	public void setGiftReceipt(boolean pGiftReceipt) {
		mGiftReceipt = pGiftReceipt;
	}
	/**
	 * @return the eligibleGiftItems
	 */
	public Map<String, TRUGiftItemInfo> getEligibleGiftItems() {
		return mEligibleGiftItems;
	}
	/**
	 * @param pEligibleGiftItems the eligibleGiftItems to set
	 */
	public void setEligibleGiftItems(Map<String, TRUGiftItemInfo> pEligibleGiftItems) {
		mEligibleGiftItems = pEligibleGiftItems;
	}
	/**
	 * @return the nonEligibleGiftItems
	 */
	public Map<String, TRUGiftItemInfo> getNonEligibleGiftItems() {
		return mNonEligibleGiftItems;
	}
	/**
	 * @param pNonEligibleGiftItems the nonEligibleGiftItems to set
	 */
	public void setNonEligibleGiftItems(
			Map<String, TRUGiftItemInfo> pNonEligibleGiftItems) {
		mNonEligibleGiftItems = pNonEligibleGiftItems;
	}
	/**
	 * @return the shippingGroupId
	 */
	public String getShippingGroupId() {
		return mShippingGroupId;
	}
	/**
	 * @param pShippingGroupId the shippingGroupId to set
	 */
	public void setShippingGroupId(String pShippingGroupId) {
		mShippingGroupId = pShippingGroupId;
	}
	
}
