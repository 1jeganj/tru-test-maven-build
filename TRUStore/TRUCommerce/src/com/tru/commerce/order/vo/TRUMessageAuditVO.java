package com.tru.commerce.order.vo;

import java.util.Date;

/**
 * This class holds the attributes used for auditing the oms messages.
 * @author PA
 * @version 1.0
 */
public class TRUMessageAuditVO {
	/**
	 * Holds the order id.
	 */
	private String mOrderId;
	/**
	 * holds the message type.
	 */
	private String mMessageType;
	/**
	 * Holds the message data.
	 */
	private String mMessageData;
	/**
	 * Holds the audit date.
	 */
	private Date mAuditDate;
	/**
	 * Holds the Order Grid Exception.
	 */
	private String mOrderGridException;
	/**
	 * Holds Order Sent To Grid(true/false).
	 */
	private String mOrderSentToGrid;
	
	/**
	 * @return the mOrderSentToGrid
	 */
	public String getOrderSentToGrid() {
		return mOrderSentToGrid;
	}
	/**
	 * @param pOrderSentToGrid the pOrderSentToGrid to set
	 */
	public void setOrderSentToGrid(String pOrderSentToGrid) {
		mOrderSentToGrid = pOrderSentToGrid;
	}
	/**
	 * @return the mOrderGridException
	 */
	public String getOrderGridException() {
		return mOrderGridException;
	}
	/**
	 * @param pOrderGridException the pOrderGridException to set
	 */
	public void setOrderGridException(String pOrderGridException) {
		mOrderGridException = pOrderGridException;
	}
	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return mOrderId;
	}
	/**
	 * @param pOrderId the orderId to set
	 */
	public void setOrderId(String pOrderId) {
		mOrderId = pOrderId;
	}
	/**
	 * @return the messageType
	 */
	public String getMessageType() {
		return mMessageType;
	}
	/**
	 * @param pMessageType the messageType to set
	 */
	public void setMessageType(String pMessageType) {
		mMessageType = pMessageType;
	}
	/**
	 * @return the messageData
	 */
	public String getMessageData() {
		return mMessageData;
	}
	/**
	 * @param pMessageData the messageData to set
	 */
	public void setMessageData(String pMessageData) {
		mMessageData = pMessageData;
	}
	/**
	 * @return the auditDate
	 */
	public Date getAuditDate() {
		return mAuditDate;
	}
	/**
	 * @param pAuditDate the auditDate to set
	 */
	public void setAuditDate(Date pAuditDate) {
		mAuditDate = pAuditDate;
	} 
}
