package com.tru.commerce.order.vo;

/**
 * The Class TRUActiveAgreementsVO holds the Active Agreements Details
 */
public class TRUActiveAgreementsVO {

	/** The Credit card number. */
	private String mCreditCardNumber;
	
	/** The Finance agreed. */
	private int mFinanceAgreed = 0;
	
	/** The Name on card. */
	private String mNameOnCard;
	
	/** The Credit card expiration month. */
	private String mCreditCardExpirationMonth;
	
	/** The Credit card expiration year. */
	private String mCreditCardExpirationYear;
	
	/** The Credit card verification number. */
	private String mCreditCardVerificationNumber;

	/** The Financing terms availed. */
	private int mFinancingTermsAvailed;
	
	/**
	 * @return the creditCardNumber
	 */
	public String getCreditCardNumber() {
		return mCreditCardNumber;
	}

	/**
	 * @param pCreditCardNumber the creditCardNumber to set
	 */
	public void setCreditCardNumber(String pCreditCardNumber) {
		mCreditCardNumber = pCreditCardNumber;
	}

	/**
	 * @return the financeAgreed
	 */
	public int getFinanceAgreed() {
		return mFinanceAgreed;
	}

	/**
	 * @param pFinanceAgreed the financeAgreed to set
	 */
	public void setFinanceAgreed(int pFinanceAgreed) {
		mFinanceAgreed = pFinanceAgreed;
	}

	/**
	 * @return the nameOnCard
	 */
	public String getNameOnCard() {
		return mNameOnCard;
	}

	/**
	 * @param pNameOnCard the nameOnCard to set
	 */
	public void setNameOnCard(String pNameOnCard) {
		mNameOnCard = pNameOnCard;
	}

	/**
	 * @return the creditCardExpirationMonth
	 */
	public String getCreditCardExpirationMonth() {
		return mCreditCardExpirationMonth;
	}

	/**
	 * @param pCreditCardExpirationMonth the creditCardExpirationMonth to set
	 */
	public void setCreditCardExpirationMonth(String pCreditCardExpirationMonth) {
		mCreditCardExpirationMonth = pCreditCardExpirationMonth;
	}

	/**
	 * @return the creditCardExpirationYear
	 */
	public String getCreditCardExpirationYear() {
		return mCreditCardExpirationYear;
	}

	/**
	 * @param pCreditCardExpirationYear the creditCardExpirationYear to set
	 */
	public void setCreditCardExpirationYear(String pCreditCardExpirationYear) {
		mCreditCardExpirationYear = pCreditCardExpirationYear;
	}

	/**
	 * @return the creditCardVerificationNumber
	 */
	public String getCreditCardVerificationNumber() {
		return mCreditCardVerificationNumber;
	}

	/**
	 * @param pCreditCardVerificationNumber the creditCardVerificationNumber to set
	 */
	public void setCreditCardVerificationNumber(String pCreditCardVerificationNumber) {
		mCreditCardVerificationNumber = pCreditCardVerificationNumber;
	}

	/**
	 * @return the financingTermsAvailed
	 */
	public int getFinancingTermsAvailed() {
		return mFinancingTermsAvailed;
	}

	/**
	 * @param pFinancingTermsAvailed the financingTermsAvailed to set
	 */
	public void setFinancingTermsAvailed(int pFinancingTermsAvailed) {
		mFinancingTermsAvailed = pFinancingTermsAvailed;
	}
}
