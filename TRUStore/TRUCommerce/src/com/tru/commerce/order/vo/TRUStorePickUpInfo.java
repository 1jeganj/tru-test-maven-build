package com.tru.commerce.order.vo;

import java.io.Serializable;

/**
 * 
 * @author PA
 *
 */
public class TRUStorePickUpInfo implements Serializable{
	
	/** holds shipping group Id **/
	private String mShippingGroupId;

	/** holds shipping group type **/
	private String mShippingGroupType;
	
	/** holds primary first name **/
	private String mFirstName;
	
	/** holds primary last name **/
	private String mLastName;
	
	/** holds primary phone number **/
	private String mPhoneNumber;
	
	/** holds primary email **/
	private String mEmail;
	
	/** holds alternate info required **/
	private boolean mAlternateInfoRequired;
	
	/** holds alternate first name **/
	private String mAlternateFirstName;
	
	/** holds alternate last name **/
	private String mAlternateLastName;
	
	/** holds alternate phone number **/
	private String mAlternatePhoneNumber;
	
	/** holds alternate email **/
	private String mAlternateEmail;

	/**
	 * @return the shippingGroupId
	 */
	public String getShippingGroupId() {
		return mShippingGroupId;
	}

	/**
	 * @param pShippingGroupId the shippingGroupId to set
	 */
	public void setShippingGroupId(String pShippingGroupId) {
		mShippingGroupId = pShippingGroupId;
	}

	/**
	 * @return the shippingGroupType
	 */
	public String getShippingGroupType() {
		return mShippingGroupType;
	}

	/**
	 * @param pShippingGroupType the shippingGroupType to set
	 */
	public void setShippingGroupType(String pShippingGroupType) {
		mShippingGroupType = pShippingGroupType;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return mFirstName;
	}

	/**
	 * @param pFirstName the firstName to set
	 */
	public void setFirstName(String pFirstName) {
		mFirstName = pFirstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return mLastName;
	}

	/**
	 * @param pLastName the lastName to set
	 */
	public void setLastName(String pLastName) {
		mLastName = pLastName;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return mPhoneNumber;
	}

	/**
	 * @param pPhoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String pPhoneNumber) {
		mPhoneNumber = pPhoneNumber;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return mEmail;
	}

	/**
	 * @param pEmail the email to set
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}

	/**
	 * @return the alternateInfoRequired
	 */
	public boolean isAlternateInfoRequired() {
		return mAlternateInfoRequired;
	}

	/**
	 * @param pAlternateInfoRequired the alternateInfoRequired to set
	 */
	public void setAlternateInfoRequired(boolean pAlternateInfoRequired) {
		mAlternateInfoRequired = pAlternateInfoRequired;
	}

	/**
	 * @return the alternateFirstName
	 */
	public String getAlternateFirstName() {
		return mAlternateFirstName;
	}

	/**
	 * @param pAlternateFirstName the alternateFirstName to set
	 */
	public void setAlternateFirstName(String pAlternateFirstName) {
		mAlternateFirstName = pAlternateFirstName;
	}

	/**
	 * @return the alternateLastName
	 */
	public String getAlternateLastName() {
		return mAlternateLastName;
	}

	/**
	 * @param pAlternateLastName the alternateLastName to set
	 */
	public void setAlternateLastName(String pAlternateLastName) {
		mAlternateLastName = pAlternateLastName;
	}

	/**
	 * @return the alternatePhoneNumber
	 */
	public String getAlternatePhoneNumber() {
		return mAlternatePhoneNumber;
	}

	/**
	 * @param pAlternatePhoneNumber the alternatePhoneNumber to set
	 */
	public void setAlternatePhoneNumber(String pAlternatePhoneNumber) {
		mAlternatePhoneNumber = pAlternatePhoneNumber;
	}

	/**
	 * @return the alternateEmail
	 */
	public String getAlternateEmail() {
		return mAlternateEmail;
	}

	/**
	 * @param pAlternateEmail the alternateEmail to set
	 */
	public void setAlternateEmail(String pAlternateEmail) {
		mAlternateEmail = pAlternateEmail;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (mAlternateInfoRequired) {
			return "TRUStorePickUpInfo [mFirstName=" + mFirstName + ", mLastName="
					+ mLastName + ", mPhoneNumber=" + mPhoneNumber + ", mEmail="
					+ mEmail + ", mAlternateInfoRequired=" + mAlternateInfoRequired
					+ ", mAlternateFirstName=" + mAlternateFirstName
					+ ", mAlternateLastName=" + mAlternateLastName
					+ ", mAlternatePhoneNumber=" + mAlternatePhoneNumber
					+ ", mAlternateEmail=" + mAlternateEmail + "]";
		} else {
			return "TRUStorePickUpInfo [mFirstName=" + mFirstName + ", mLastName="
					+ mLastName + ", mPhoneNumber=" + mPhoneNumber + ", mEmail="
					+ mEmail + ", mAlternateInfoRequired=" + mAlternateInfoRequired	+ "]";
		}
	}
	
	
}
