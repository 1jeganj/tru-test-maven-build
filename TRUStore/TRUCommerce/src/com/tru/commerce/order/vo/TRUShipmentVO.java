package com.tru.commerce.order.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import atg.commerce.order.ShippingGroup;

import com.tru.commerce.cart.vo.TRUCartItemDetailVO;
import com.tru.commerce.cart.vo.TRUStoreVO;
import com.tru.commerce.pricing.TRUShipMethodVO;

/**
 * The Class TRUShipmentVO.
 *
 * @author Professional Access.
 * @version 1.0
 */
public class TRUShipmentVO implements Serializable{

	/** property: holds CartItemDetailVOs. */
	private List<TRUCartItemDetailVO> mCartItemDetailVOs = new ArrayList<TRUCartItemDetailVO>();
	
	/** property: holds CartOOSItemDetailVOs. */
	private List<TRUCartItemDetailVO> mCartOOSItemDetailVOs = new ArrayList<TRUCartItemDetailVO>();
	
	/** property: holds ShipMethodVOs. */
	private List<TRUShipMethodVO> mShipMethodVOs;
	
	/** holds shipping group. */
	private ShippingGroup mShippingGroup;
	
	/**  holds estimateShipWindowMin *. */
	private int mEstimateShipWindowMin;
	
	/**  holds estimateShipWindowMax *. */
	private int mEstimateShipWindowMax;
	
	/**  holds estimateMinDate *. */
	private Date mEstimateMinDate;
	
	/**  holds estimateMaxDate *. */
	private Date mEstimateMaxDate;
	
	/**  holds StoreDetails *. */
	private TRUStoreVO mStoreDetails; 
	
	/**
	 * Gets the cart item detail v os.
	 *
	 * @return the cartItemDetailVOs
	 */
	public List<TRUCartItemDetailVO> getCartItemDetailVOs() {
		return mCartItemDetailVOs;
	}

	/**
	 * Sets the cart item detail v os.
	 *
	 * @param pCartItemDetailVOs the cartItemDetailVOs to set
	 */
	public void setCartItemDetailVOs(List<TRUCartItemDetailVO> pCartItemDetailVOs) {
		mCartItemDetailVOs = pCartItemDetailVOs;
	}

	/**
	 * Gets the cart oos item detail v os.
	 *
	 * @return the cartOOSItemDetailVOs
	 */
	public List<TRUCartItemDetailVO> getCartOOSItemDetailVOs() {
		return mCartOOSItemDetailVOs;
	}

	/**
	 * Sets the cart oos item detail v os.
	 *
	 * @param pCartOOSItemDetailVOs the cartOOSItemDetailVOs to set
	 */
	public void setCartOOSItemDetailVOs(
			List<TRUCartItemDetailVO> pCartOOSItemDetailVOs) {
		mCartOOSItemDetailVOs = pCartOOSItemDetailVOs;
	}

	/**
	 * Gets the ship method v os.
	 *
	 * @return the shipMethodVOs
	 */
	public List<TRUShipMethodVO> getShipMethodVOs() {
		return mShipMethodVOs;
	}

	/**
	 * Sets the ship method v os.
	 *
	 * @param pShipMethodVOs the shipMethodVOs to set
	 */
	public void setShipMethodVOs(List<TRUShipMethodVO> pShipMethodVOs) {
		mShipMethodVOs = pShipMethodVOs;
	}
	
	/** property: holds selectedShippingMethod. */
	private String mSelectedShippingMethod;
	
	/** property: holds selectedShippingPrice. */
	private double mSelectedShippingPrice;

	/**
	 * Gets the selected shipping method.
	 *
	 * @return the selectedShippingMethod
	 */
	public String getSelectedShippingMethod() {
		return mSelectedShippingMethod;
	}

	/**
	 * Sets the selected shipping method.
	 *
	 * @param pSelectedShippingMethod the selectedShippingMethod to set
	 */
	public void setSelectedShippingMethod(String pSelectedShippingMethod) {
		mSelectedShippingMethod = pSelectedShippingMethod;
	}

	/**
	 * Gets the selected shipping price.
	 *
	 * @return the selectedShippingPrice
	 */
	public double getSelectedShippingPrice() {
		return mSelectedShippingPrice;
	}

	/**
	 * Sets the selected shipping price.
	 *
	 * @param pSelectedShippingPrice the selectedShippingPrice to set
	 */
	public void setSelectedShippingPrice(double pSelectedShippingPrice) {
		mSelectedShippingPrice = pSelectedShippingPrice;
	}
	
	/** property: holds related ship id. */
	private String mRelationShipId;

	/**
	 * Gets the relation ship id.
	 *
	 * @return the relationShipId
	 */
	public String getRelationShipId() {
		return mRelationShipId;
	}

	/**
	 * Sets the relation ship id.
	 *
	 * @param pRelationShipId the relationShipId to set
	 */
	public void setRelationShipId(String pRelationShipId) {
		mRelationShipId = pRelationShipId;
	}

	/**
	 * Gets the shipping group.
	 *
	 * @return the shippingGroup
	 */
	public ShippingGroup getShippingGroup() {
		return mShippingGroup;
	}

	/**
	 * Sets the shipping group.
	 *
	 * @param pShippingGroup the shippingGroup to set
	 */
	public void setShippingGroup(ShippingGroup pShippingGroup) {
		mShippingGroup = pShippingGroup;
	}

	/**
	 * Gets the estimate ship window min.
	 *
	 * @return the estimateShipWindowMin
	 */
	public int getEstimateShipWindowMin() {
		return mEstimateShipWindowMin;
	}

	/**
	 * Sets the estimate ship window min.
	 *
	 * @param pEstimateShipWindowMin the estimateShipWindowMin to set
	 */
	public void setEstimateShipWindowMin(int pEstimateShipWindowMin) {
		mEstimateShipWindowMin = pEstimateShipWindowMin;
	}

	/**
	 * Gets the estimate ship window max.
	 *
	 * @return the estimateShipWindowMax
	 */
	public int getEstimateShipWindowMax() {
		return mEstimateShipWindowMax;
	}

	/**
	 * Sets the estimate ship window max.
	 *
	 * @param pEstimateShipWindowMax the estimateShipWindowMax to set
	 */
	public void setEstimateShipWindowMax(int pEstimateShipWindowMax) {
		mEstimateShipWindowMax = pEstimateShipWindowMax;
	}

	/**
	 * Gets the estimate min date.
	 *
	 * @return the estimateMinDate
	 */
	public Date getEstimateMinDate() {
		return mEstimateMinDate;
	}

	/**
	 * Sets the estimate min date.
	 *
	 * @param pEstimateMinDate the estimateMinDate to set
	 */
	public void setEstimateMinDate(Date pEstimateMinDate) {
		mEstimateMinDate = pEstimateMinDate;
	}

	/**
	 * Gets the estimate max date.
	 *
	 * @return the estimateMaxDate
	 */
	public Date getEstimateMaxDate() {
		return mEstimateMaxDate;
	}

	/**
	 * Sets the estimate max date.
	 *
	 * @param pEstimateMaxDate the estimateMaxDate to set
	 */
	public void setEstimateMaxDate(Date pEstimateMaxDate) {
		mEstimateMaxDate = pEstimateMaxDate;
	}

	/**
	 * Gets the store details.
	 *
	 * @return the mStoreDetails
	 */
	public TRUStoreVO getStoreDetails() {
		return mStoreDetails;
	}

	/**
	 * Sets the store details.
	 *
	 * @param pStoreDetails the mStoreDetails to set
	 */
	public void setStoreDetails(TRUStoreVO pStoreDetails) {
		this.mStoreDetails = pStoreDetails;
	}
	
	/**  holds store pick up information *. */
	private TRUStorePickUpInfo mStorePickUpInfo;

	/**
	 * Gets the store pick up info.
	 *
	 * @return the storePickUpInfo
	 */
	public TRUStorePickUpInfo getStorePickUpInfo() {
		return mStorePickUpInfo;
	}

	/**
	 * Sets the store pick up info.
	 *
	 * @param pStorePickUpInfo the storePickUpInfo to set
	 */
	public void setStorePickUpInfo(TRUStorePickUpInfo pStorePickUpInfo) {
		mStorePickUpInfo = pStorePickUpInfo;
	}
	

	/**
	 * Holds the future date for preSellItem
	 */
	private Date mStreetDate;

	/**
	 * 
	 * @return StreetDate
	 */
	public Date getStreetDate() {
		return mStreetDate;
	}

	/**
	 * 
	 * @param pStreetDate - StreetDate
	 */
	public void setStreetDate(Date pStreetDate) {
		this.mStreetDate = pStreetDate;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TRUShipmentVO [mShipMethodVOs=" + mShipMethodVOs
				+ ", mStoreDetails=" + mStoreDetails
				+ ", mSelectedShippingMethod=" + mSelectedShippingMethod
				+ ", mSelectedShippingPrice=" + mSelectedShippingPrice
				+ ", mRelationShipId=" + mRelationShipId
				+ ", mStorePickUpInfo=" + mStorePickUpInfo + "]";
	}

}