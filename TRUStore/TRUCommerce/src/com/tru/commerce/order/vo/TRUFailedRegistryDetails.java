package com.tru.commerce.order.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.tru.integrations.registry.beans.Product;


/**
 * The Class TRUFailedRegistryDetails.
 * 
 * @author Professional Access
 * 
 */
public class TRUFailedRegistryDetails implements Cloneable,Serializable{
	/**
	 * generated serialVersionUID
	 */
	private static final long serialVersionUID = -5505866574002912354L;
	
	/** The Registry id. */
	private String mRegistryId;
	
	/** The Registry type. */
	private String mRegistryType;
	
	/** The Order id. */
	private String mOrderId;
	
	/** The Date created. */
	private Date mDateCreated;
	
	/** The Date submitted. */
	private Date mDateSubmitted;
	
	/** The Status. */
	private String mStatus;
	
	/** The Products. */
	List<Product> mProducts;
	
	/**
	 * @return the registryId
	 */
	public String getRegistryId() {
		return mRegistryId;
	}


	/**
	 * @param pRegistryId the registryId to set
	 */
	public void setRegistryId(String pRegistryId) {
		mRegistryId = pRegistryId;
	}


	/**
	 * @return the registryType
	 */
	public String getRegistryType() {
		return mRegistryType;
	}


	/**
	 * @param pRegistryType the registryType to set
	 */
	public void setRegistryType(String pRegistryType) {
		mRegistryType = pRegistryType;
	}


	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return mOrderId;
	}


	/**
	 * @param pOrderId the orderId to set
	 */
	public void setOrderId(String pOrderId) {
		mOrderId = pOrderId;
	}

	
	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return mDateCreated;
	}


	/**
	 * @param pDateCreated the dateCreated to set
	 */
	public void setDateCreated(Date pDateCreated) {
		mDateCreated = pDateCreated;
	}


	/**
	 * @return the dateSubmitted
	 */
	public Date getDateSubmitted() {
		return mDateSubmitted;
	}


	/**
	 * @param pDateSubmitted the dateSubmitted to set
	 */
	public void setDateSubmitted(Date pDateSubmitted) {
		mDateSubmitted = pDateSubmitted;
	}


	/**
	 * @return the status
	 */
	public String getStatus() {
		return mStatus;
	}


	/**
	 * @param pStatus the status to set
	 */
	public void setStatus(String pStatus) {
		mStatus = pStatus;
	}

	/**
	 * @return the pProducts
	 */
	public List<Product> getpProducts() {
		return mProducts;
	}


	/**
	 * @param pPProducts the mProducts to set
	 */
	public void setpProducts(List<Product> pPProducts) {
		mProducts = pPProducts;
	}


	/**
	 * @return the clone object
	 * @throws CloneNotSupportedException - CloneNotSupportedException
	 */
	public Object clone()throws CloneNotSupportedException{  
		return super.clone();  
	}
}
