package com.tru.commerce.order.vo;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import atg.core.util.Address;

/**
 * The Class TRUShippingDestinationVO.
 *
 * @author Professional Access.
 * class TRUShippingDestinationVO 
 */
public class TRUShippingDestinationVO {
	
	/**  holds check if it is registry address *. */
	private boolean mRegistryAddress;
	
	/**
	 * Checks if is registry address.
	 *
	 * @return the registryAddress
	 */
	public boolean isRegistryAddress() {
		return mRegistryAddress;
	}

	/**
	 * Sets the registry address.
	 *
	 * @param pRegistryAddress the registryAddress to set
	 */
	public void setRegistryAddress(boolean pRegistryAddress) {
		mRegistryAddress = pRegistryAddress;
	}

	/** property: holds ItemsCount. */
	private long mItemsCount = 0;
	
	/**
	 * Gets the items count.
	 *
	 * @return the itemsCount
	 */
	public long getItemsCount() {
		return mItemsCount;
	}

	/**
	 * Sets the items count.
	 *
	 * @param pItemsCount the itemsCount to set
	 */
	public void setItemsCount(long pItemsCount) {
		mItemsCount = pItemsCount;
	}

	/** property: holds shipping group type. */
	private String mShippingGroupType;
	
	/** property: holds Address. */
	
	private Address mAddress;
	
	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public Address getAddress() {
		return mAddress;
	}

	/**
	 * Sets the address.
	 *
	 * @param pAddress the address to set
	 */
	public void setAddress(Address pAddress) {
		mAddress = pAddress;
	}

	/** property: holds shipments. */
	private Map<String, TRUShipmentVO> mShipmentVOMap = new LinkedHashMap<String, TRUShipmentVO>();

	/**
	 * Gets the shipment vo map.
	 *
	 * @return the shipmentVOMap
	 */
	public Map<String, TRUShipmentVO> getShipmentVOMap() {
		return mShipmentVOMap;
	}

	/**
	 * Sets the shipment vo map.
	 *
	 * @param pShipmentVOMap the shipmentVOMap to set
	 */
	public void setShipmentVOMap(Map<String, TRUShipmentVO> pShipmentVOMap) {
		mShipmentVOMap = pShipmentVOMap;
	}
	
	/**
	 * Gets the shipping group type.
	 *
	 * @return shipping group type
	 */
	public String getShippingGroupType() {
		return mShippingGroupType;
	}
	
	/**
	 * Sets the shipping group type.
	 *
	 * @param pShippingGroupType shipping group type
	 */
	public void setShippingGroupType(String pShippingGroupType) {
		this.mShippingGroupType = pShippingGroupType;
	}

	/** The m total ship amount. */
	private double mTotalShipAmount;

	/**
	 * Gets the total ship amount.
	 *
	 * @return the mTotalShipAmount
	 */
	public double getTotalShipAmount() {
		return mTotalShipAmount;
	}

	/**
	 * Sets the total ship amount.
	 *
	 * @param pTotalShipAmount the new total ship amount
	 */
	public void setTotalShipAmount(double pTotalShipAmount) {
		this.mTotalShipAmount = pTotalShipAmount;
	}
	
	/** The m shiip gruoup ids. */
	private List<String> mShiipGruoupIds;
	
	/**
	 * @return the mShiipGruoupIds
	 */
	public List<String> getShiipGruoupIds() {
		return mShiipGruoupIds;
	}

	/**
	 * @param pShiipGruoupIds the mShiipGruoupIds to set
	 */
	public void setShiipGruoupIds(List<String> pShiipGruoupIds) {
		mShiipGruoupIds = pShiipGruoupIds;
	}



	/** The m ship to name. */
	private String mShipToName;




	/**
	 * Gets the ship to name.
	 *
	 * @return the mShipToName
	 */
	public String getShipToName() {
		return mShipToName;
	}

	/**
	 * Sets the ship to name.
	 *
	 * @param pShipToName the new ship to name
	 */
	public void setShipToName(String pShipToName) {
		this.mShipToName = pShipToName;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TRUShippingDestinationVO [mRegistryAddress=" + mRegistryAddress
				+ ", mItemsCount=" + mItemsCount + ", mShippingGroupType="
				+ mShippingGroupType + ", mAddress=" + mAddress
				+ ", mShipmentVOMap=" + mShipmentVOMap + ", mTotalShipAmount="
				+ mTotalShipAmount + "]";
	}
	
	
}
