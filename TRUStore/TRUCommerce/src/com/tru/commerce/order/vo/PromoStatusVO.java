package com.tru.commerce.order.vo;

/**
 * This class is PromoStatusVO.
 * 
 * @author PA
 * @version 1.0
 */
public class PromoStatusVO extends PromotionStatusVO {

	/**
	 * Property to hold Promo Discount Amount.
	 */
	private Double mAmount;


	/**
	 * @return the amount
	 */
	public Double getAmount() {
		return mAmount;
	}


	/**
	 * @param pAmount
	 *            the amount to set
	 */
	public void setAmount(Double pAmount) {
		mAmount = pAmount;
	}

}