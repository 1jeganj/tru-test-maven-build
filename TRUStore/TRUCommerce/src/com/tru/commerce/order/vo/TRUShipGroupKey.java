/**
 * used to group shipments under one nickname based on certain properties
 */
package com.tru.commerce.order.vo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import atg.core.util.StringUtils;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.pricing.TRUShipMethodVO;
import com.tru.common.TRUConstants;

/**
 * @author PA
 * 
 */
public class TRUShipGroupKey {
	
	/** holds nick name **/
	private String mNickName;
	
	/**
	 * @return the nickName
	 */
	public String getNickName() {
		return mNickName;
	}

	/**
	 * @param pNickName the nickName to set
	 */
	public void setNickName(String pNickName) {
		mNickName = pNickName;
	}

	/** holds store location Id **/
	private String mLocationId;

	/**
	 * @return the locationId
	 */
	public String getLocationId() {
		return mLocationId;
	}

	/**
	 * @param pLocationId the locationId to set
	 */
	public void setLocationId(String pLocationId) {
		mLocationId = pLocationId;
	}

	/** holds estimate ship window min **/
	private int mEstimateShipWindowMin;
	
	/** holds estimate ship window max **/
	private int mEstimateShipWindowMax;

	/**
	 * @return the estimateShipWindowMin
	 */
	public int getEstimateShipWindowMin() {
		return mEstimateShipWindowMin;
	}

	/**
	 * @param pEstimateShipWindowMin the estimateShipWindowMin to set
	 */
	public void setEstimateShipWindowMin(int pEstimateShipWindowMin) {
		mEstimateShipWindowMin = pEstimateShipWindowMin;
	}

	/**
	 * @return the estimateShipWindowMax
	 */
	public int getEstimateShipWindowMax() {
		return mEstimateShipWindowMax;
	}

	/**
	 * @param pEstimateShipWindowMax the estimateShipWindowMax to set
	 */
	public void setEstimateShipWindowMax(int pEstimateShipWindowMax) {
		mEstimateShipWindowMax = pEstimateShipWindowMax;
	}

	/** holds selected shipping method **/
	private String mSelectedShippingMethod;
	
	/**
	 * @return the selectedShippingMethod
	 */
	public String getSelectedShippingMethod() {
		return mSelectedShippingMethod;
	}

	/**
	 * @param pSelectedShippingMethod the selectedShippingMethod to set
	 */
	public void setSelectedShippingMethod(String pSelectedShippingMethod) {
		mSelectedShippingMethod = pSelectedShippingMethod;
	}

	/** holds set of possible shipping method codes **/
	private Set<String> mPossibleShipMethodCodes = new HashSet<String>();

	/**
	 * @return the possibleShipMethodCodes
	 */
	public Set<String> getPossibleShipMethodCodes() {
		return mPossibleShipMethodCodes;
	}
	
	/**
	 * @param pPossibleShipMethodCodes the possibleShipMethodCodes to set
	 */
	public void setPossibleShipMethodCodes(Set<String> pPossibleShipMethodCodes) {
		mPossibleShipMethodCodes = pPossibleShipMethodCodes;
	}
	
	/**
	 * Empty constructor.
	 */
	public TRUShipGroupKey() {
		// Empty constructor
	}
	
	/**
	 * constructor - adds ship method names 
	 *  @param pShipMethodVOs - list shipmethodVO
	 */
	public TRUShipGroupKey(List<TRUShipMethodVO> pShipMethodVOs) {
		if (pShipMethodVOs != null && !pShipMethodVOs.isEmpty()) {
			for (TRUShipMethodVO shipMethodVO : pShipMethodVOs) {
				mPossibleShipMethodCodes.add(shipMethodVO.getShipMethodCode());
			}
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (StringUtils.isNotBlank(mNickName)) {
			sb.append(mNickName);
		}
		if (StringUtils.isNotBlank(mLocationId)) {
			sb.append(mLocationId);
		}
		sb.append(mEstimateShipWindowMin);
		sb.append(mEstimateShipWindowMax);
		if (StringUtils.isNotBlank(mSelectedShippingMethod)) {
			sb.append(mSelectedShippingMethod);
		}
		if (mPossibleShipMethodCodes != null && !mPossibleShipMethodCodes.isEmpty()) {
			List<String> possibleShipMethodCodes = new ArrayList<String>(mPossibleShipMethodCodes);
			Collections.sort(possibleShipMethodCodes);
			sb.append(possibleShipMethodCodes);
		}
		return sb.toString();
	}

	@Override
	public int hashCode() {
		int prime = TRUCommerceConstants.HASH_CODE_PRIME;
		int result = TRUCommerceConstants.ONE;
		result = prime * result + mEstimateShipWindowMax;
		result = prime * result + mEstimateShipWindowMin;
		result = prime * result
				+ ((mLocationId == null) ? 0 : mLocationId.hashCode());
		result = prime * result
				+ ((mNickName == null) ? 0 : mNickName.hashCode());
		result = prime
				* result
				+ ((mPossibleShipMethodCodes == null) ? 0
						: mPossibleShipMethodCodes.hashCode());
		result = prime
				* result
				+ ((mSelectedShippingMethod == null) ? 0
						: mSelectedShippingMethod.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object pObj) {
		
		if (this == pObj){
			return true; 
		}
		if (pObj == null){
			return false;
		}
		if (getClass() != pObj.getClass()){
			return false;
		}
		TRUShipGroupKey other = (TRUShipGroupKey) pObj;
		if(StringUtils.isBlank(other.getSelectedShippingMethod())){
			other.setSelectedShippingMethod(TRUConstants.EMPTY_STRING);
		}
		if(StringUtils.isBlank(this.getSelectedShippingMethod())){
			this.setSelectedShippingMethod(TRUConstants.EMPTY_STRING);
		}
		if (mEstimateShipWindowMax != other.mEstimateShipWindowMax){
			return false;
		}
		if (mEstimateShipWindowMin != other.mEstimateShipWindowMin){
			return false;
		}
		if (mLocationId == null) {
			if (other.mLocationId != null){
				return false;
			}
		} else if (!mLocationId.equals(other.mLocationId)){
			return false;
		}
		if (mNickName == null) {
			if (other.mNickName != null){
				return false;
			}
		} else if (!mNickName.equals(other.mNickName)){
			return false;
		}
		if (mPossibleShipMethodCodes == null) {
			if (other.mPossibleShipMethodCodes != null){
				return false;
			}
		} else if (!mPossibleShipMethodCodes
				.equals(other.mPossibleShipMethodCodes)){
			return false;
		}
		if (mSelectedShippingMethod == null) {
			if (other.mSelectedShippingMethod != null){
				return false;
			}
		} else if (!mSelectedShippingMethod
				.equals(other.mSelectedShippingMethod)){
			return false;
		}
		return true;
	}
}