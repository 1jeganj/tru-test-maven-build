package com.tru.commerce.order.vo;


/**
 * The Class ApplePayOrderSummaryVO used to hold apple pay order summary.
 * 
 * @author PA
 * @version 1.0
 */
public class ApplePayOrderSummaryVO {

	/**
	 * Holds total shipping surcharge.
	 */
	private double mSurchargeTotal;

	/**
	 *  mEwasteFees Fees.
	 */
	private double mEwasteFees;
	/**
	 *  holds estimatedShipping.
	 */
	private double mShippingTotal;
	/**
	 * Holds rawSubtotal.
	 */
	private double mRawSubtotal;

	/**
	 *  Holds orderTotal
	 */
	private double mOrderTotal;
	/**
	 * Holds taxTotal.
	 */
	private double mTaxTotal;

	/**
	 *  holds currencyCode.
	 */
	private String mCurrencyCode;

	

	/**
	 * @return the mSurchargeTotal
	 */
	public double getSurchargeTotal() {
		return mSurchargeTotal;
	}

	/**
	 * @param pSurchargeTotal the mSurchargeTotal to set
	 */
	public void setSurchargeTotal(double pSurchargeTotal) {
		this.mSurchargeTotal = pSurchargeTotal;
	}

	/**
	 * @return the ewasteFees
	 */
	public double getEwasteFees() {
		return mEwasteFees;
	}

	/**
	 * @param pEwasteFees the ewasteFees to set
	 */
	public void setEwasteFees(double pEwasteFees) {
		mEwasteFees = pEwasteFees;
	}

	/**
	 * @return the shippingTotal
	 */
	public double getShippingTotal() {
		return mShippingTotal;
	}

	/**
	 * @param pShippingTotal the shippingTotal to set
	 */
	public void setShippingTotal(double pShippingTotal) {
		mShippingTotal = pShippingTotal;
	}

	/**
	 * @return the rawSubtotal
	 */
	public double getRawSubtotal() {
		return mRawSubtotal;
	}

	/**
	 * @param pRawSubtotal the rawSubtotal to set
	 */
	public void setRawSubtotal(double pRawSubtotal) {
		mRawSubtotal = pRawSubtotal;
	}

	/**
	 * @return the orderTotal
	 */
	public double getOrderTotal() {
		return mOrderTotal;
	}

	/**
	 * @param pOrderTotal the orderTotal to set
	 */
	public void setOrderTotal(double pOrderTotal) {
		mOrderTotal = pOrderTotal;
	}

	/**
	 * @return the taxTotal
	 */
	public double getTaxTotal() {
		return mTaxTotal;
	}

	/**
	 * @param pTaxTotal the taxTotal to set
	 */
	public void setTaxTotal(double pTaxTotal) {
		mTaxTotal = pTaxTotal;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return mCurrencyCode;
	}

	/**
	 * @param pCurrencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String pCurrencyCode) {
		mCurrencyCode = pCurrencyCode;
	}


}
