package com.tru.commerce.order.vo;

/**
 * The Class ShippingAddressVO used to hold shipping address of order.
 * 
 * @author PA
 * @version 1.0
 */
public class ShippingAddressVO {

	/** holds firstName **/
	private String mFirstName;
	/** holds MiddleName **/
	private String mMiddleName;
	/** holds LastName **/
	private String mLastName;
	/** holds Address1 **/
	private String mAddress1;
	/** holds Address2 **/
	private String mAddress2;
	/** holds Address3 **/
	private String mAddress3;
	/** holds City **/
	private String mCity;
	/** holds State **/
	private String mState;
	/** holds PostalCode **/
	private String mPostalCode;
	/** holds CountryCode **/
	private String mCountryCode;
	/** holds Phone1 **/
	private String mPhone1;
	/** holds Phone2 **/
	private String mPhone2;

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return mFirstName;
	}

	/**
	 * @param pFirstName
	 *            the firstName to set
	 */
	public void setFirstName(String pFirstName) {
		mFirstName = pFirstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return mMiddleName;
	}

	/**
	 * @param pMiddleName
	 *            the middleName to set
	 */
	public void setMiddleName(String pMiddleName) {
		mMiddleName = pMiddleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return mLastName;
	}

	/**
	 * @param pLastName
	 *            the lastName to set
	 */
	public void setLastName(String pLastName) {
		mLastName = pLastName;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return mAddress1;
	}

	/**
	 * @param pAddress1
	 *            the address1 to set
	 */
	public void setAddress1(String pAddress1) {
		mAddress1 = pAddress1;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return mAddress2;
	}

	/**
	 * @param pAddress2
	 *            the address2 to set
	 */
	public void setAddress2(String pAddress2) {
		mAddress2 = pAddress2;
	}

	/**
	 * @return the address3
	 */
	public String getAddress3() {
		return mAddress3;
	}

	/**
	 * @param pAddress3
	 *            the address3 to set
	 */
	public void setAddress3(String pAddress3) {
		mAddress3 = pAddress3;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return mCity;
	}

	/**
	 * @param pCity
	 *            the city to set
	 */
	public void setCity(String pCity) {
		mCity = pCity;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return mState;
	}

	/**
	 * @param pState
	 *            the state to set
	 */
	public void setState(String pState) {
		mState = pState;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return mPostalCode;
	}

	/**
	 * @param pPostalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String pPostalCode) {
		mPostalCode = pPostalCode;
	}

	/**
	 * @return the mCountryCode
	 */
	public String getCountryCode() {
		return mCountryCode;
	}

	/**
	 * @param pCountryCode
	 *            the mCountryCode to set
	 */
	public void setCountryCode(String pCountryCode) {
		mCountryCode = pCountryCode;
	}

	/**
	 * @return the phone1
	 */
	public String getPhone1() {
		return mPhone1;
	}

	/**
	 * @param pPhone1
	 *            the phone1 to set
	 */
	public void setPhone1(String pPhone1) {
		mPhone1 = pPhone1;
	}

	/**
	 * @return the phone2
	 */
	public String getPhone2() {
		return mPhone2;
	}

	/**
	 * @param pPhone2
	 *            the phone2 to set
	 */
	public void setPhone2(String pPhone2) {
		mPhone2 = pPhone2;
	}

}
