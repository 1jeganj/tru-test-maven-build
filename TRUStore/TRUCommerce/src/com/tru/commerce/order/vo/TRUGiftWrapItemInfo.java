package com.tru.commerce.order.vo;


/**
 *
 * 
 * Class TRUGiftWrapItemInfo.
 *  @author Professional Access.
 *  @version 1.0.
 */
public class TRUGiftWrapItemInfo {
	private String mWrapProductId;
	private String mWrapSkuId;
	private String mWrapCommerceItemId;
	private int mWrapQuantity;
	
	/**
	 * @return the wrapCommerceItemId
	 */
	public String getWrapCommerceItemId() {
		return mWrapCommerceItemId;
	}
	/**
	 * @param pWrapCommerceItemId the wrapCommerceItemId to set
	 */
	public void setWrapCommerceItemId(String pWrapCommerceItemId) {
		mWrapCommerceItemId = pWrapCommerceItemId;
	}
	/**
	 * @return the wrapQuantity
	 */
	public int getWrapQuantity() {
		return mWrapQuantity;
	}
	/**
	 * @param pWrapQuantity the wrapQuantity to set
	 */
	public void setWrapQuantity(int pWrapQuantity) {
		mWrapQuantity = pWrapQuantity;
	}
	/**
	 * @return the wrapProductId
	 */
	public String getWrapProductId() {
		return mWrapProductId;
	}
	/**
	 * @param pWrapProductId the wrapProductId to set
	 */
	public void setWrapProductId(String pWrapProductId) {
		mWrapProductId = pWrapProductId;
	}
	/**
	 * @return the wrapSkuId
	 */
	public String getWrapSkuId() {
		return mWrapSkuId;
	}
	/**
	 * @param pWrapSkuId the wrapSkuId to set
	 */
	public void setWrapSkuId(String pWrapSkuId) {
		mWrapSkuId = pWrapSkuId;
	}
	
	
}
