package com.tru.commerce.order.vo;

/**
 * This class is PromotionStatus.
 * @author PA
 * @version 1.0
 */
public class PromotionStatusVO {
	
	/**
	 * Property to hold details.
	 */
	private String mDescription;
	/**
	 * Property to hold PromotionName.
	 */
	private String mDisplayName;
	/**
	 * @return the promotionName
	 */
	public String getDisplayName() {
		return mDisplayName;
	}

	/**
	 * @param pPromotionName the promotionName to set
	 */
	public void setDisplayName(String pPromotionName) {
		mDisplayName = pPromotionName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return mDescription;
	}

	/**
	 * @param pDescription the description to set
	 */
	public void setDescription(String pDescription) {
		mDescription = pDescription;
	}	
}