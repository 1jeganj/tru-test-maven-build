package com.tru.commerce.order;

import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.RelationshipNotFoundException;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.ShippingGroupManager;
import atg.commerce.order.purchase.ShippingGroupMapContainer;
import atg.core.util.Address;
import atg.core.util.StringUtils;

import com.tru.commerce.cart.vo.TRUChannelDetailsVo;
import com.tru.commerce.cart.vo.TRURegistrantDetailsVO;
import com.tru.commerce.order.purchase.TRUShippingProcessHelper;
import com.tru.commerce.order.vo.TRUGiftOptionsInfo;
import com.tru.commerce.pricing.TRUShipMethodVO;
import com.tru.common.TRUConstants;

/**
 * TRUShippingGroupManager - Extensions to ShippingGroupManager.
 *
 * @author Professional Access
 * @version 1.0.
 */
public class TRUShippingGroupManager extends ShippingGroupManager {

	/**
	 * This method is overridden for InStorePickupShippingGroup shipping groups are need to merge from source order to
	 * Destination order.
	 * 
	 * @param pSrcOrder
	 *            the src order
	 * @param pDstOrder
	 *            the dst order
	 * @param pGroup
	 *            the group
	 * @return the shipping group
	 * @throws CommerceException
	 *             the commerce exception
	 */

	public ShippingGroup mergeOrdersCopyShippingGroup(Order pSrcOrder, Order pDstOrder, ShippingGroup pGroup)
			throws CommerceException {
		ShippingGroup shippingGroup = null;
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyShippingGroup() : BEGIN");
			vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyShippingGroup() : pSrcOrder-->{0} pDstOrder-->{1} pGroup-->{2}",pSrcOrder, pDstOrder, pGroup);
		}
		if (pGroup instanceof TRUChannelInStorePickupShippingGroup) {
			shippingGroup = mergeOrdersCopyChannelInStrPickupShippingGroup(pSrcOrder, pDstOrder,
					(TRUChannelInStorePickupShippingGroup) pGroup);
		} else if (pGroup instanceof TRUInStorePickupShippingGroup) {
			shippingGroup = mergeOrdersCopyInStrPickupShippingGroup(pSrcOrder, pDstOrder,
					(TRUInStorePickupShippingGroup) pGroup);
		} else if (pGroup instanceof TRUChannelHardgoodShippingGroup) {
			shippingGroup = mergeOrdersCopyChannelHardgoodShippingGroup(pSrcOrder, pDstOrder,
					(TRUChannelHardgoodShippingGroup) pGroup);
		} else {
			shippingGroup = super.mergeOrdersCopyShippingGroup(pSrcOrder, pDstOrder, pGroup);
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyShippingGroup() : shippingGroup-->{0} ",shippingGroup);
			vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyShippingGroup() : END");
		}
		return shippingGroup;
	}
	
	/**
	 * Adds the channel hardgood shipping group to order.
	 *
	 * @param pOrder the order
	 * @param pChannelDetailsVo the channelDetailsVo
	 * @return the matchingSG
	 * @throws CommerceException the commerce exception
	 */
	public ShippingGroup addChannelHardgoodShippingGroupToOrder(Order pOrder, TRUChannelDetailsVo pChannelDetailsVo)
			throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShippingGroupManager::@method::addChannelHardgoodShippingGroupToOrder() : BEGIN");
			vlogDebug("@Class::TRUShippingGroupManager::@method::addChannelHardgoodShippingGroupToOrder() : Order -->{0} TRUChannelDetailsVo -->{1}",pOrder,pChannelDetailsVo);
		}
		ShippingGroup matchingSG = createShippingGroup(((TRUOrderTools) getOrderTools()).getChannelHardgoodShippingGroupType());
		if (matchingSG instanceof TRUChannelHardgoodShippingGroup) {
			if (StringUtils.isNotBlank(pChannelDetailsVo.getChannelType())) {
				((TRUChannelHardgoodShippingGroup) matchingSG).setChannelType(pChannelDetailsVo.getChannelType());
			}
			if (StringUtils.isNotBlank(pChannelDetailsVo.getChannelId())) {
				((TRUChannelHardgoodShippingGroup) matchingSG).setChannelId(pChannelDetailsVo.getChannelId());
			}
			if (StringUtils.isNotBlank(pChannelDetailsVo.getChannelUserName())) {
				((TRUChannelHardgoodShippingGroup) matchingSG).setChannelUserName(pChannelDetailsVo.getChannelUserName());
			}
			if (StringUtils.isNotBlank(pChannelDetailsVo.getChannelCoUserName())) {
				((TRUChannelHardgoodShippingGroup) matchingSG).setChannelCoUserName(pChannelDetailsVo.getChannelCoUserName());
			}
			if (StringUtils.isNotBlank(pChannelDetailsVo.getChannelName())) {
				((TRUChannelHardgoodShippingGroup) matchingSG).setChannelName(pChannelDetailsVo.getChannelName());
			}
			if (StringUtils.isNotBlank(pChannelDetailsVo.getSourceChannel())) {
				((TRUChannelHardgoodShippingGroup) matchingSG).setSourceChannel(pChannelDetailsVo.getSourceChannel());
			}
		}
		addShippingGroupToOrder(pOrder, matchingSG);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShippingGroupManager::@method::addChannelHardgoodShippingGroupToOrder() : matchingSG --> {0}",matchingSG);
			vlogDebug("@Class::TRUShippingGroupManager::@method::addChannelHardgoodShippingGroupToOrder() : END");
		}
		return matchingSG;
	}
	
	/**
	 * Adds the channel ispu shipping group to order.
	 *
	 * @param pOrder the order
	 * @param pChannelDetailsVo the channelDetailsVo
	 * @param pLocationId locationId
	 * @return the matchingSG
	 * @throws CommerceException the commerce exception
	 */
	public ShippingGroup addChannelISPUShippingGroupToOrder(Order pOrder, TRUChannelDetailsVo pChannelDetailsVo, String pLocationId)
			throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShippingGroupManager::@method::addChannelISPUShippingGroupToOrder() : BEGIN");
			vlogDebug("@Class::TRUShippingGroupManager::@method::addChannelISPUShippingGroupToOrder() : Order --> {0}, TRUChannelDetailsVo --> {1},  pLocationId --> {2}", pOrder,pChannelDetailsVo,pLocationId);
		}
		ShippingGroup matchingSG = createShippingGroup(((TRUOrderTools) getOrderTools()).getChannelInStorePickupShippingGroupType());
		if (matchingSG instanceof TRUChannelInStorePickupShippingGroup) {
			TRUChannelInStorePickupShippingGroup shippingGroup= ((TRUChannelInStorePickupShippingGroup) matchingSG);
			shippingGroup.setLocationId(pLocationId);
			if (StringUtils.isNotBlank(pChannelDetailsVo.getChannelType())) {
				shippingGroup.setChannelType(pChannelDetailsVo.getChannelType());
			}
			if (StringUtils.isNotBlank(pChannelDetailsVo.getChannelId())) {
				shippingGroup.setChannelId(pChannelDetailsVo.getChannelId());
			}
			if (StringUtils.isNotBlank(pChannelDetailsVo.getChannelUserName())) {
				//Start GEOFUS-1576 :: Commented as part of the defect. 
				/*// update registrant details thorugh service
				final boolean isRegistrantDetailsFound = getShippingProcessHelper().updateRegistrantDetails(pChannelDetailsVo);
				if(isRegistrantDetailsFound){
					List<TRURegistrantDetailsVO> detailsVOs= pChannelDetailsVo.getRegistrantDetailsVOs();
					if(detailsVOs!=null && detailsVOs.size()>TRUConstants.ZERO ){
						final TRURegistrantDetailsVO detailsVO= detailsVOs.get(0);
						if(detailsVO!=null && detailsVO.getAddress()!=null){
							shippingGroup.setFirstName(detailsVO.getAddress().getFirstName());
							shippingGroup.setLastName(detailsVO.getAddress().getLastName());
							shippingGroup.setPhoneNumber(detailsVO.getAddress().getPhoneNumber());
							shippingGroup.setEmail(detailsVO.getAddress().getEmail());
						}						
					}						
				}*/
				//End GEOFUS-1576 :: Commented as part of the defect.
				shippingGroup.setChannelUserName(pChannelDetailsVo.getChannelUserName());
				
			}
			if (StringUtils.isNotBlank(pChannelDetailsVo.getChannelCoUserName())) {
				shippingGroup.setChannelCoUserName(pChannelDetailsVo.getChannelCoUserName());
			}
			if (StringUtils.isNotBlank(pChannelDetailsVo.getChannelName())) {
				shippingGroup.setChannelName(pChannelDetailsVo.getChannelName());
			}
			if (StringUtils.isNotBlank(pChannelDetailsVo.getSourceChannel())) {
				shippingGroup.setSourceChannel(pChannelDetailsVo.getSourceChannel());
			}
		}
		addShippingGroupToOrder(pOrder, matchingSG);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShippingGroupManager::@method::addChannelISPUShippingGroupToOrder() matchingSG :-->{0}",matchingSG);
			vlogDebug("@Class::TRUShippingGroupManager::@method::addChannelISPUShippingGroupToOrder() : END");
		}
		return matchingSG;
	}

	/**
	 * This method is creating the InStorePickupShippingGroup if it is not there in Destination Order.
	 * 
	 * @param pSrcOrder
	 *            the src order
	 * @param pDstOrder
	 *            the dst order
	 * @param pGroup
	 *            the group
	 * @return the in store pickup shipping group
	 * @throws CommerceException
	 *             the commerce exception
	 */
	protected TRUInStorePickupShippingGroup mergeOrdersCopyInStrPickupShippingGroup(Order pSrcOrder, Order pDstOrder,
			TRUInStorePickupShippingGroup pGroup) throws CommerceException {
		if (isLoggingDebug()) {
		vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyInStrPickupShippingGroup() : BEGIN");
		vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyShippingGroup() : pSrcOrder-->{0} pDstOrder-->{1} TRUInStorePickupShippingGroup-->{2}",pSrcOrder, pDstOrder, pGroup);
		}
		TRUInStorePickupShippingGroup inStrShippingGroup = null;
		List<ShippingGroup> shippingGroups = null;
		if(null != pDstOrder) {
			shippingGroups = pDstOrder.getShippingGroups();
		}

		if (null != pGroup) {
			if (null != shippingGroups && !shippingGroups.isEmpty()) {
				for (ShippingGroup shippingGroup : shippingGroups) {
					if (shippingGroup instanceof TRUInStorePickupShippingGroup
							&& ((TRUInStorePickupShippingGroup) shippingGroup).getLocationId().equals(
									pGroup.getLocationId())) {

						inStrShippingGroup = (TRUInStorePickupShippingGroup) shippingGroup;
						if (isLoggingDebug()) {
						vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyInStrPickupShippingGroup() ShippingGroup already exists {0}",inStrShippingGroup);
						}
						break;
					}
				}
			}
			if (null == inStrShippingGroup) {
				inStrShippingGroup = (TRUInStorePickupShippingGroup) createShippingGroup(pGroup.getShippingGroupClassType());
				inStrShippingGroup.copyProperties(pGroup);
				if(inStrShippingGroup instanceof TRUInStorePickupShippingGroup 
						 && pGroup instanceof TRUInStorePickupShippingGroup){
					
					((TRUInStorePickupShippingGroup) inStrShippingGroup)
							.setEmail(((TRUInStorePickupShippingGroup) pGroup)
									.getEmail());
					((TRUInStorePickupShippingGroup) inStrShippingGroup)
							.setPhoneNumber(((TRUInStorePickupShippingGroup) pGroup)
									.getPhoneNumber());
					((TRUInStorePickupShippingGroup) inStrShippingGroup)
							.setAltFirstName(((TRUInStorePickupShippingGroup) pGroup)
									.getAltFirstName());
					((TRUInStorePickupShippingGroup) inStrShippingGroup)
							.setAltLastName(((TRUInStorePickupShippingGroup) pGroup)
									.getAltLastName());
					((TRUInStorePickupShippingGroup) inStrShippingGroup)
							.setAltEmail(((TRUInStorePickupShippingGroup) pGroup)
									.getAltEmail());
					((TRUInStorePickupShippingGroup) inStrShippingGroup)
							.setAltPhoneNumber(((TRUInStorePickupShippingGroup) pGroup)
									.getAltPhoneNumber());
					((TRUInStorePickupShippingGroup) inStrShippingGroup)
							.setAltAddrRequired(((TRUInStorePickupShippingGroup) pGroup)
									.isAltAddrRequired());
				}
				if (isLoggingDebug()) {
				vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyInStrPickupShippingGroup() New ShippingGroup created {0}",inStrShippingGroup);
				}
				return inStrShippingGroup;
			}
		}
		if (isLoggingDebug()) {
		vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyInStrPickupShippingGroup() : END");
		}
		return null;
	}

	/**
	 * Apply gift message.
	 *
	 * @param pMap map
	 * @param pShippingGroupMapContainer 
	 */
	public void applyGiftMessage(Map<ShippingGroup, TRUGiftOptionsInfo> pMap, ShippingGroupMapContainer pShippingGroupMapContainer){
		// iterate over the shipping groups map
		// add the gift message to the shipping group.
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShippingGroupManager::@method::applyGiftMessage() : BEGIN");
			vlogDebug("@Class::TRUShippingGroupManager::@method::applyGiftMessage() : pMap -->{0}",pMap);
		}
		for (ShippingGroup shippingGroup : pMap.keySet()) {
			TRUGiftOptionsInfo giftOptionsInfo = pMap.get(shippingGroup);
			((TRUHardgoodShippingGroup) shippingGroup).setGiftReceipt(giftOptionsInfo.isGiftReceipt());
			((TRUHardgoodShippingGroup) shippingGroup).setGiftWrapMessage(giftOptionsInfo.getGiftWrapMessage());
			if(pShippingGroupMapContainer != null) {
				ShippingGroup containerShippingGroup = pShippingGroupMapContainer.getShippingGroup(giftOptionsInfo.getShippingAdressNickName());
				if(containerShippingGroup != null) {
					((TRUHardgoodShippingGroup) containerShippingGroup).setGiftReceipt(giftOptionsInfo.isGiftReceipt());
					((TRUHardgoodShippingGroup) containerShippingGroup).setGiftWrapMessage(giftOptionsInfo.getGiftWrapMessage());
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShippingGroupManager::@method::applyGiftMessage() : END");
		}
	}
	
	/**
	 * updates SGCI relation ship with target shipping group.
	 *
	 * @param pOrder the order
	 * @param pSgCiRel the sg ci rel
	 * @param pTargetShippingGroup the target shipping group
	 * @throws CommerceException the commerce exception
	 */
	public void updateSGCIRelationshipWithShippingGroup(Order pOrder, ShippingGroupCommerceItemRelationship pSgCiRel, ShippingGroup pTargetShippingGroup) 
			throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShippingGroupManager::@method::updateSGCIRelationshipWithShippingGroup() : BEGIN");
			vlogDebug("@Class::TRUShippingGroupManager::@method::updateSGCIRelationshipWithShippingGroup() : Order --> {0}, ShippingGroupCommerceItemRelationship --> {1}, ShippingGroup --> {2} ", pOrder, pSgCiRel,  pTargetShippingGroup);
		}
		if (pSgCiRel instanceof TRUShippingGroupCommerceItemRelationship) {
			try {
				getShippingGroupCommerceItemRelationship(pOrder, pSgCiRel.getCommerceItem().getId(), pTargetShippingGroup.getId());
				if (isLoggingDebug()) {
					vlogDebug("Found relation ship: " + pSgCiRel.getId() + " old shipping goup: " + pTargetShippingGroup.getId());
				}
			} catch(RelationshipNotFoundException re) {
				if (isLoggingError()) {
				vlogError("Updating relation ship: {0}  new shipping goup: {1} @Class::TRUShippingGroupManager::@method::updateSGCIRelationshipWithShippingGroup()" , pSgCiRel.getId() , pTargetShippingGroup.getId());
				}
				TRUHardgoodShippingGroup originalShippingGroup = (TRUHardgoodShippingGroup) pSgCiRel.getShippingGroup();
				pSgCiRel.setShippingGroup(pTargetShippingGroup);
				originalShippingGroup.removeCommerceItemRelationship(pSgCiRel.getId());
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShippingGroupManager::@method::updateSGCIRelationshipWithShippingGroup() : END");
		}
	}
	
	/**
	 * Merge orders copy hardgood shipping group.
	 *
	 * @param pSrcOrder the src order
	 * @param pDstOrder the dst order
	 * @param pGroup the group
	 * @return the hardgood shipping group
	 * @throws CommerceException the commerce exception
	 * @see atg.commerce.order.ShippingGroupManager#mergeOrdersCopyHardgoodShippingGroup(atg.commerce.order.Order, atg.commerce.order.Order, atg.commerce.order.HardgoodShippingGroup)
	 */
	@Override
	protected HardgoodShippingGroup mergeOrdersCopyHardgoodShippingGroup(Order pSrcOrder, Order pDstOrder,
			HardgoodShippingGroup pGroup) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyHardgoodShippingGroup() : BEGIN");
			vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyHardgoodShippingGroup() :  pSrcOrder -->{0},  pDstOrder -->{1},	 HardgoodShippingGroup -->{2}",pSrcOrder,pDstOrder,pGroup);
		}
		HardgoodShippingGroup hardgoodShippingGroup=null;
		if(isOrderHavingHardGoodShippingGrp(pDstOrder)){
			hardgoodShippingGroup=super.mergeOrdersCopyHardgoodShippingGroup(pSrcOrder, pDstOrder, pGroup);
		} else {
			Address addr = pGroup.getShippingAddress();
			hardgoodShippingGroup = (HardgoodShippingGroup) createShippingGroup(pGroup.getShippingGroupClassType());
			if (!(isNullAddress(addr))) {

				Address newAddress = null;
				try {
					newAddress = (Address) addr.getClass().newInstance();
					if (DynamicBeans.getBeanInfo(newAddress).hasProperty(TRUConstants.REPOSITORY_ITEM)){
						DynamicBeans.setPropertyValue(newAddress, TRUConstants.REPOSITORY_ITEM,
								hardgoodShippingGroup.getRepositoryItem());
					}
				} catch (IntrospectionException ie) {
					if(isLoggingError()){
						logError("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyHardgoodShippingGroup(), IntrospectionException",ie);
					}
					throw new CommerceException(ie);
				} catch (PropertyNotFoundException pnfe) {
					if(isLoggingError()){
						logError("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyHardgoodShippingGroup(), PropertyNotFoundException",pnfe);
					}
					throw new CommerceException(pnfe);
				} catch (IllegalAccessException iae) {
					if(isLoggingError()){
						logError("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyHardgoodShippingGroup(), IllegalAccessException",iae);
					}
					throw new CommerceException(iae);
				} catch (InstantiationException ie) {
					if(isLoggingError()){
						logError("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyHardgoodShippingGroup(), InstantiationException",ie);
					}
					throw new CommerceException(ie);
				}
				hardgoodShippingGroup.setShippingAddress(newAddress);
				hardgoodShippingGroup.copyProperties(pGroup);
			}
		}
		if(hardgoodShippingGroup instanceof TRUHardgoodShippingGroup 
				 && pGroup instanceof TRUHardgoodShippingGroup){
			((TRUHardgoodShippingGroup)hardgoodShippingGroup).setNickName(((TRUHardgoodShippingGroup)pGroup).getNickName());
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyHardgoodShippingGroup() : END");
		}
		return hardgoodShippingGroup;
	}
	
	
	/**
	 * Checks if is destination order having hard good shipping grp.
	 *
	 * @param pOrder the order
	 * @return true, if is order having hard good shipping grp
	 * @throws CommerceException the commerce exception
	 */
	public boolean isOrderHavingHardGoodShippingGrp(Order pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupManager.isOrderHavingHardGoodShippingGrp method");
			vlogDebug("INPUT PARAMS OF isOrderHavingHardGoodShippingGrp() pOrder: {0}", pOrder);
		}
		boolean hasHardGoodShipGrp = false;
		if ((pOrder.getShippingGroups() != null) && (pOrder.getShippingGroups().size() > 0)) {
			List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
			for (ShippingGroup shippingGroup : shippingGroups) {
				if (shippingGroup instanceof TRUHardgoodShippingGroup && !(shippingGroup instanceof TRUChannelHardgoodShippingGroup)) {
					hasHardGoodShipGrp = true;
					break;
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyHardgoodShippingGroup() : END ");
		}
		return hasHardGoodShipGrp;
	}
	
	/**
	 * Merge orders copy channel hardgood shipping group.
	 *
	 * @param pSrcOrder the src order
	 * @param pDstOrder the dst order
	 * @param pGroup the group
	 * @return the TRU channel hardgood shipping group
	 * @throws CommerceException the commerce exception
	 */
	protected TRUChannelHardgoodShippingGroup mergeOrdersCopyChannelHardgoodShippingGroup(Order pSrcOrder, Order pDstOrder,
			TRUChannelHardgoodShippingGroup pGroup) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyChannelHardgoodShippingGroup() : BEGIN");
			vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyHardgoodShippingGroup() :  pSrcOrder -->{0},  pDstOrder -->{1},	 HardgoodShippingGroup -->{2}",pSrcOrder,pDstOrder,pGroup);
		}
		TRUChannelHardgoodShippingGroup channelHardgoodShippingGroup = null;
		List<ShippingGroup> shippingGroups = null;
		if(pDstOrder != null) {
			shippingGroups = pDstOrder.getShippingGroups();
		}
		if (null != pGroup) {
			if (null != shippingGroups && !shippingGroups.isEmpty()) {
				for (ShippingGroup shippingGroup : shippingGroups) {
					if (shippingGroup instanceof TRUChannelHardgoodShippingGroup
							&& ((TRUChannelHardgoodShippingGroup) shippingGroup).getChannelId() != null 
							&& ((TRUChannelHardgoodShippingGroup) shippingGroup).getChannelId().equals(
									pGroup.getChannelId())) {

						channelHardgoodShippingGroup = (TRUChannelHardgoodShippingGroup) shippingGroup;
						if (isLoggingDebug()) {
							vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyChannelHardgoodShippingGroup() ShippingGroup already exists {0}",channelHardgoodShippingGroup);
						}
						break;
					}
				}
			}
			if (null == channelHardgoodShippingGroup) {
				channelHardgoodShippingGroup = (TRUChannelHardgoodShippingGroup) createShippingGroup(pGroup.getShippingGroupClassType());
				channelHardgoodShippingGroup.copyProperties(pGroup);
				
				if(channelHardgoodShippingGroup instanceof TRUChannelHardgoodShippingGroup 
						 && pGroup instanceof TRUChannelHardgoodShippingGroup){
					((TRUChannelHardgoodShippingGroup) channelHardgoodShippingGroup)
							.setNickName(((TRUChannelHardgoodShippingGroup) pGroup)
									.getNickName());
					((TRUChannelHardgoodShippingGroup) channelHardgoodShippingGroup)
							.setChannelId(((TRUChannelHardgoodShippingGroup) pGroup)
									.getChannelId());
					((TRUChannelHardgoodShippingGroup) channelHardgoodShippingGroup)
							.setChannelName(((TRUChannelHardgoodShippingGroup) pGroup)
									.getChannelName());
					((TRUChannelHardgoodShippingGroup) channelHardgoodShippingGroup)
							.setChannelType(((TRUChannelHardgoodShippingGroup) pGroup)
									.getChannelType());
					((TRUChannelHardgoodShippingGroup) channelHardgoodShippingGroup)
							.setChannelUserName(((TRUChannelHardgoodShippingGroup) pGroup)
									.getChannelUserName());
					((TRUChannelHardgoodShippingGroup) channelHardgoodShippingGroup)
							.setChannelCoUserName(((TRUChannelHardgoodShippingGroup) pGroup)
									.getChannelCoUserName());
					((TRUChannelHardgoodShippingGroup) channelHardgoodShippingGroup)
							.setSourceChannel(((TRUChannelHardgoodShippingGroup) pGroup)
									.getSourceChannel());
				}
				if (isLoggingDebug()) {
					vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyChannelHardgoodShippingGroup() : {0}",channelHardgoodShippingGroup);
					vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyChannelHardgoodShippingGroup() : END");
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyChannelHardgoodShippingGroup() : END");
		}
		return channelHardgoodShippingGroup;
	}
	
	
	/**
	 * Merge orders copy channel in str pickup shipping group.
	 *
	 * @param pSrcOrder the src order
	 * @param pDstOrder the dst order
	 * @param pGroup the group
	 * @return the TRU channel in store pickup shipping group
	 * @throws CommerceException the commerce exception
	 */
	protected TRUChannelInStorePickupShippingGroup mergeOrdersCopyChannelInStrPickupShippingGroup(Order pSrcOrder, Order pDstOrder,
			TRUChannelInStorePickupShippingGroup pGroup) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyChannelInStrPickupShippingGroup() : BEGIN");
			vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyHardgoodShippingGroup() :  pSrcOrder -->{0},  pDstOrder -->{1},	 HardgoodShippingGroup -->{2}",pSrcOrder,pDstOrder,pGroup);
		}
		TRUChannelInStorePickupShippingGroup channelIspuShippingGroup = null;
		List<ShippingGroup> shippingGroups = null;
		if(null != pDstOrder){
			shippingGroups = pDstOrder.getShippingGroups();
		}
		if (null != pGroup) {
			if (null != shippingGroups && !shippingGroups.isEmpty()) {
				for (ShippingGroup shippingGroup : shippingGroups) {
					if (shippingGroup instanceof TRUChannelInStorePickupShippingGroup
							&& ((TRUChannelInStorePickupShippingGroup) shippingGroup).getLocationId().equals(
									pGroup.getLocationId())
							&& ((TRUChannelInStorePickupShippingGroup) shippingGroup).getChannelId().equals(
									pGroup.getChannelId())) {

						channelIspuShippingGroup = (TRUChannelInStorePickupShippingGroup) shippingGroup;
						if (isLoggingDebug()) {
							vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyChannelInStrPickupShippingGroup()  ShippingGroup already exists {0}",channelIspuShippingGroup);
						}
						break;
					}
				}
			}
			if (null == channelIspuShippingGroup) {
				channelIspuShippingGroup = (TRUChannelInStorePickupShippingGroup) createShippingGroup(
						pGroup.getShippingGroupClassType());
				channelIspuShippingGroup.copyProperties(pGroup);
				((TRUChannelInStorePickupShippingGroup) channelIspuShippingGroup)
						.setEmail(((TRUChannelInStorePickupShippingGroup) pGroup)
								.getEmail());
				((TRUChannelInStorePickupShippingGroup) channelIspuShippingGroup)
						.setPhoneNumber(((TRUChannelInStorePickupShippingGroup) pGroup)
								.getPhoneNumber());
				((TRUChannelInStorePickupShippingGroup) channelIspuShippingGroup)
						.setAltFirstName(((TRUChannelInStorePickupShippingGroup) pGroup)
								.getAltFirstName());
				((TRUChannelInStorePickupShippingGroup) channelIspuShippingGroup)
						.setAltLastName(((TRUChannelInStorePickupShippingGroup) pGroup)
								.getAltLastName());
				((TRUChannelInStorePickupShippingGroup) channelIspuShippingGroup)
						.setAltEmail(((TRUChannelInStorePickupShippingGroup) pGroup)
								.getAltEmail());
				((TRUChannelInStorePickupShippingGroup) channelIspuShippingGroup)
						.setAltPhoneNumber(((TRUChannelInStorePickupShippingGroup) pGroup)
								.getAltPhoneNumber());
				((TRUChannelInStorePickupShippingGroup) channelIspuShippingGroup)
						.setAltAddrRequired(((TRUChannelInStorePickupShippingGroup) pGroup)
								.isAltAddrRequired());
				((TRUChannelInStorePickupShippingGroup) channelIspuShippingGroup)
						.setChannelId(((TRUChannelInStorePickupShippingGroup) pGroup)
								.getChannelId());
				((TRUChannelInStorePickupShippingGroup) channelIspuShippingGroup)
						.setChannelName(((TRUChannelInStorePickupShippingGroup) pGroup)
								.getChannelName());
				((TRUChannelInStorePickupShippingGroup) channelIspuShippingGroup)
						.setChannelType(((TRUChannelInStorePickupShippingGroup) pGroup)
								.getChannelType());
				((TRUChannelInStorePickupShippingGroup) channelIspuShippingGroup)
						.setChannelUserName(((TRUChannelInStorePickupShippingGroup) pGroup)
								.getChannelUserName());
				((TRUChannelInStorePickupShippingGroup) channelIspuShippingGroup)
						.setChannelCoUserName(((TRUChannelInStorePickupShippingGroup) pGroup)
								.getChannelCoUserName());
				((TRUChannelInStorePickupShippingGroup) channelIspuShippingGroup)
						.setSourceChannel(((TRUChannelInStorePickupShippingGroup) pGroup)
								.getSourceChannel());
				if (isLoggingDebug()) {
					vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyChannelInStrPickupShippingGroup()  New ShippingGroup created {0}",channelIspuShippingGroup);
					vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyChannelInStrPickupShippingGroup() : END");
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUShippingGroupManager::@method::mergeOrdersCopyChannelInStrPickupShippingGroup() : END");
		}
		return channelIspuShippingGroup;
	}
		
	
	/**
	 * Checks if is not instance of tru commerce item.
	 *
	 * @param pCommerceItem the commerce item
	 * @return true, if is not instance of tru commerce item
	 */
	public boolean isNotInstanceOfTRUCommerceItem(CommerceItem pCommerceItem){
		if(pCommerceItem instanceof TRUGiftWrapCommerceItem || pCommerceItem instanceof TRUDonationCommerceItem){
			return true;
		}
		return false;
	}
	
	
	
	
	/**
	 * Gets the only ship to home item relation ships by filtering out the TRUGiftWrapCommerceItem and TRUDonationCommerceItem.
	 *
	 * @param pShipGrp the ship grp
	 * @return the only ship to home item relation ships
	 */
	@SuppressWarnings("unchecked")
	public List<CommerceItemRelationship> getOnlyShipToHomeItemRelationShips(ShippingGroup pShipGrp){
		List<CommerceItemRelationship> s2hItemsList=new ArrayList<CommerceItemRelationship>();
			if (pShipGrp instanceof TRUHardgoodShippingGroup ) {
				List<ShippingGroupCommerceItemRelationship> commerceItemRelationships = pShipGrp.getCommerceItemRelationships();
				for (ShippingGroupCommerceItemRelationship sgCIR : commerceItemRelationships) {
					if (commerceItemRelationships == null || commerceItemRelationships.isEmpty()){
						continue;
					}
					if (! (sgCIR.getCommerceItem() instanceof TRUGiftWrapCommerceItem || sgCIR.getCommerceItem() instanceof TRUDonationCommerceItem)){
						s2hItemsList.add(sgCIR);
					}
				}
			}
		return s2hItemsList;
	}
	
	
	
	/** The m shipping process helper. */
	private TRUShippingProcessHelper mShippingProcessHelper;

	/**
	 * @return the shippingProcessHelper
	 */
	public TRUShippingProcessHelper getShippingProcessHelper() {
		return mShippingProcessHelper;
	}

	/**
	 * @param pShippingProcessHelper the shippingProcessHelper to set
	 */
	public void setShippingProcessHelper(TRUShippingProcessHelper pShippingProcessHelper) {
		this.mShippingProcessHelper = pShippingProcessHelper;
	}
	
	
	
	/**
	 * Gets the only channel hard good shipping group relationships.
	 *
	 * @param pOrder the order
	 * @return the only channel hard good shipping group relationships
	 */
	@SuppressWarnings("unchecked")
	public List<TRUShippingGroupCommerceItemRelationship> getOnlyChannelHardGoodShippingGroupRelationships(final Order pOrder){
		final List<TRUShippingGroupCommerceItemRelationship> channelHgsgCiRels = new ArrayList<TRUShippingGroupCommerceItemRelationship>();
		try {						
			final List<ShippingGroupCommerceItemRelationship> sgCiRels =getAllShippingGroupRelationships(pOrder);
			if (sgCiRels != null && !sgCiRels.isEmpty()) {
				final int sgCiRelsCount = sgCiRels.size();
				for (int i = 0; i < sgCiRelsCount; ++i) {
					if (sgCiRels.get(i) instanceof TRUShippingGroupCommerceItemRelationship) {
						TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) sgCiRels.get(i);					
						if (sgCiRel.getShippingGroup() instanceof TRUChannelHardgoodShippingGroup) {
							channelHgsgCiRels.add(sgCiRel);
						}
					}
				}
			}
		} catch (CommerceException e) {
			if(isLoggingError()){
				vlogError("CommerceException while getOnlyChannelHardGoodShippingGroupRelationships() for the order {0}",pOrder.getId());
			}
		}
		return channelHgsgCiRels;
	}
	
	/**
	 * Gets the only ship to home item relation ships by filtering out the TRUGiftWrapCommerceItem and TRUDonationCommerceItem.
	 *
	 * @param pOrder the order
	 * @return the only ship to home item relation ships
	 */
	@SuppressWarnings("unchecked")
	public List<TRUShippingGroupCommerceItemRelationship> getOnlyShipToHomeItemRelationShips(final Order pOrder){
		final List<TRUShippingGroupCommerceItemRelationship> hgsgCiRels = new ArrayList<TRUShippingGroupCommerceItemRelationship>();
		try {				
			final List<ShippingGroupCommerceItemRelationship> sgCiRels =getAllShippingGroupRelationships(pOrder);
			if (sgCiRels != null && !sgCiRels.isEmpty()) {
				final int sgCiRelsCount = sgCiRels.size();
				for (int i = 0; i < sgCiRelsCount; ++i) {
					if (sgCiRels.get(i) instanceof TRUShippingGroupCommerceItemRelationship) {
						TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) sgCiRels.get(i);					
						if (sgCiRel.getShippingGroup() instanceof TRUHardgoodShippingGroup/* &&! (sgCiRel.getShippingGroup() instanceof TRUChannelHardgoodShippingGroup)*/) {
							hgsgCiRels.add(sgCiRel);
						}
					}
				}
			}
		} catch (CommerceException e) {
			if(isLoggingError()){
				vlogError("CommerceException while getOnlyChannelHardGoodShippingGroupRelationships() for the order {0}",pOrder.getId());
			}
		}
		return hgsgCiRels;
	}
	
	
	/**
	 * Copy channel shipping info.
	 *
	 * @param pOldShippingGroup the old shipping group
	 * @param pNewShippingGroup the new shipping group
	 */
	public void copyChannelShippingInfo(final TRUHardgoodShippingGroup pOldShippingGroup, TRUHardgoodShippingGroup pNewShippingGroup) {
		TRUChannelHardgoodShippingGroup channelNewShippingGroup = (TRUChannelHardgoodShippingGroup) pNewShippingGroup;
		TRUChannelHardgoodShippingGroup channelOldShippingGroup = (TRUChannelHardgoodShippingGroup) pOldShippingGroup;
		channelNewShippingGroup.setChannelId(channelOldShippingGroup.getChannelId());
		channelNewShippingGroup.setChannelType(channelOldShippingGroup.getChannelType());
		channelNewShippingGroup.setChannelName(channelOldShippingGroup.getChannelName());
		channelNewShippingGroup.setChannelUserName(channelOldShippingGroup.getChannelUserName());
		channelNewShippingGroup.setChannelCoUserName(channelOldShippingGroup.getChannelCoUserName());
		channelNewShippingGroup.setRegistrantId(channelOldShippingGroup.getRegistrantId());
		channelNewShippingGroup.setRegistryAddress(channelOldShippingGroup.isRegistryAddress());
		channelNewShippingGroup.setSourceChannel(channelOldShippingGroup.getSourceChannel());
	}
	
	
	
	
	/**
	 * <p>
	 * This method will validate the eligibility of the shipping method for the associated shipping address.
	 * If any item is not eligible for default ship method,Then it searches for matched shippingGroup based on
	 * <li>Shipping GroupType</li>
	 * <li>Channel Type</li>
	 * <li>Channel Id</li>
	 * <li>ShipToName</li>
	 * <li>Shipping Method</li>
	 * add add the items to the matched shipping group.
	 * If no  matched shipping group found ,New shipping group will be created and default ship method and price will be applied.
	 * </p>
	 *
	 * @param pOrder the order
	 * @return the list of shippingGroupId which are added newly
	 */
	public List<String> validateShippingMethodAndPrice(Order pOrder) {
		final boolean orderMandateForMultiShipping =  ((TRUOrderImpl)pOrder).isOrderMandateForMultiShipping();
		List<String> shipGrpIds = new ArrayList<String>();		
		if(orderMandateForMultiShipping || ((TRUOrderImpl) pOrder).isOrderIsInMultiShip() || ((TRUOrderImpl) pOrder).isExpressPayPal()){			
			try {
				final TRUOrderManager orderManager =  (TRUOrderManager)getShippingProcessHelper().getPurchaseProcessHelper().getOrderManager();
				final List<TRUShippingGroupCommerceItemRelationship> sgCiRels = this.getOnlyShipToHomeItemRelationShips(pOrder);
				final TRUShippingManager shippingManager =  getShippingProcessHelper().getPurchaseProcessHelper().getShippingManager();
				
				if (sgCiRels != null && sgCiRels.size() > TRUConstants._1 && !sgCiRels.isEmpty()) {
					final int sgCiRelsCount = sgCiRels.size();
					for (int i = TRUConstants._0; i < sgCiRelsCount; ++i) {
						final TRUCommerceItemImpl commerceItem = (TRUCommerceItemImpl) sgCiRels.get(i).getCommerceItem();
						if (isNotInstanceOfTRUCommerceItem(commerceItem)) {
							continue;
						}						
						TRUHardgoodShippingGroup  oldShippingGroup=((TRUHardgoodShippingGroup)sgCiRels.get(i).getShippingGroup());
						TRUShipMethodVO methodVO = shippingManager.isItemQualifyForShipMethod(commerceItem, oldShippingGroup,pOrder);
						if (null !=  methodVO) {
							String shipGrpId = TRUConstants.EMPTY; 
							final TRUOrderTools orderTools=(TRUOrderTools)orderManager.getOrderTools();
							final TRUHardgoodShippingGroup matchedShippingGroup = orderManager.getShipGroupByNickNameAndShipMethod(
									pOrder, oldShippingGroup, methodVO.getShipMethodCode());
							
						if(matchedShippingGroup!=null){
							matchedShippingGroup.setShippingPrice(methodVO.getShippingPrice());
							final long qnty=sgCiRels.get(i).getQuantity();
							getCommerceItemManager().removeItemQuantityFromShippingGroup(pOrder, commerceItem.getId(), oldShippingGroup.getId(),qnty);
							getCommerceItemManager().addItemQuantityToShippingGroup(pOrder, commerceItem.getId(),
									matchedShippingGroup.getId(),qnty);										
							final TRUShippingGroupCommerceItemRelationship newShpGrpCIRel = (TRUShippingGroupCommerceItemRelationship) this.
									getShippingGroupCommerceItemRelationship(pOrder, commerceItem.getId(), matchedShippingGroup.getId());						
							orderTools.copyShippingRelationShip(sgCiRels.get(i), newShpGrpCIRel);
							shipGrpId = matchedShippingGroup.getId();
						}else{
							final TRUHardgoodShippingGroup newShippingGroup = (TRUHardgoodShippingGroup) this.createShippingGroup(
									oldShippingGroup.getShippingGroupClassType());							
							if (oldShippingGroup instanceof TRUChannelHardgoodShippingGroup
									&& newShippingGroup instanceof TRUChannelHardgoodShippingGroup) {											
								copyChannelShippingInfo(oldShippingGroup, newShippingGroup);
							}
							this.addShippingGroupToOrder(pOrder, newShippingGroup);
							OrderTools.copyAddress(oldShippingGroup.getShippingAddress(), newShippingGroup.getShippingAddress());
							//Set the updated ship method and price
							newShippingGroup.setNickName(oldShippingGroup.getNickName());
							newShippingGroup.setShippingMethod(methodVO.getShipMethodCode());
							newShippingGroup.setShippingMethodName(methodVO.getShipMethodName());
							newShippingGroup.setShippingPrice(methodVO.getShippingPrice());
							final long qnty=sgCiRels.get(i).getQuantity();
							getCommerceItemManager().removeItemQuantityFromShippingGroup(pOrder, commerceItem.getId(), oldShippingGroup.getId(),qnty);
							getCommerceItemManager().addItemQuantityToShippingGroup(pOrder, commerceItem.getId(), newShippingGroup.getId(),qnty);
							shipGrpId = newShippingGroup.getId();
							final TRUShippingGroupCommerceItemRelationship newShpGrpCIRel = (TRUShippingGroupCommerceItemRelationship) this.
									getShippingGroupCommerceItemRelationship(pOrder, commerceItem.getId(), newShippingGroup.getId());							
							if(isLoggingInfo()){
								vlogInfo("{0}  is not applicable for commerceItem #{1} creating new shiping group #{2} with new shipmethod {3}", oldShippingGroup.getShippingMethod(),commerceItem.getId(),shipGrpId , newShippingGroup.getShippingMethod());
							}
							orderTools.copyShippingRelationShip(sgCiRels.get(i), newShpGrpCIRel);	
						}						
						shipGrpIds.add(shipGrpId);							
					}						
				}
				//this.removeEmptyShippingGroups(pOrder);
			}
		} catch (CommerceException e) {
				if(isLoggingError()){
					logError("CommerceException while updating ship method and price while moving from cart to shipping ", e);
				}
			}
		}
		return shipGrpIds;
	}

}
