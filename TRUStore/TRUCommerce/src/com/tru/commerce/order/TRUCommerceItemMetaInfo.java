package com.tru.commerce.order;

import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Observable;
import java.util.Set;

import atg.commerce.order.ChangedProperties;
import atg.commerce.order.CommerceIdentifierImpl;
import atg.commerce.order.Constants;
import atg.repository.MutableRepositoryItem;

import com.tru.commerce.TRUCommerceConstants;

/**
 * The Class TRUCommerceItemMetaInfo used for 
 * 	storing the commerce item metainfo objects.
 */
public class TRUCommerceItemMetaInfo extends CommerceIdentifierImpl implements
		ChangedProperties {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Save all properties. */
	private boolean mSaveAllProperties = false;

	/** The Changed. */
	private boolean mChanged = false;

	/** The Constant PROPERTY_COUNT. */
	private static final int PROPERTY_COUNT = 20;

	/** The Changed properties. */
	private Set<String> mChangedProperties = new HashSet<String>(PROPERTY_COUNT);

	/** The Repository item. */
	private MutableRepositoryItem mRepositoryItem = null;

	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable pObservable, Object pArgument) {
		if (pArgument instanceof String) {
			addChangedProperty((String) pArgument);
		} else {
			throw new RuntimeException(TRUCommerceConstants.OBSERVABLE_UPDATE_FOR + super.getClass().getName() + TRUCommerceConstants.WAS_RECEIVED_WITH_ARG_TYPE
					+ pArgument.getClass().getName() + TRUCommerceConstants.COLON_SYMBOL + pArgument);
		}

	}

	/* (non-Javadoc)
	 * @see atg.commerce.order.ChangedProperties#getSaveAllProperties()
	 */
	@Override
	public boolean getSaveAllProperties() {
		return this.mSaveAllProperties;
	}

	/* (non-Javadoc)
	 * @see atg.commerce.order.ChangedProperties#setSaveAllProperties(boolean)
	 */
	@Override
	public void setSaveAllProperties(boolean pSaveAllProperties) {
		this.mSaveAllProperties = pSaveAllProperties;
	}

	/* (non-Javadoc)
	 * @see atg.commerce.order.ChangedProperties#isChanged()
	 */
	@Override
	public boolean isChanged() {
		return ((this.mChanged) || ((this.mChangedProperties != null) && (!(getChangedProperties().isEmpty()))));
	}

	/* (non-Javadoc)
	 * @see atg.commerce.order.ChangedProperties#setChanged(boolean)
	 */
	@Override
	public void setChanged(boolean pChanged) {
		this.mChanged = pChanged;
	}

	/* (non-Javadoc)
	 * @see atg.commerce.order.ChangedProperties#getChangedProperties()
	 */
	@Override
	public Set getChangedProperties() {
		return mChangedProperties;
	}

	/* (non-Javadoc)
	 * @see atg.commerce.order.ChangedProperties#addChangedProperty(java.lang.String)
	 */
	@Override
	public void addChangedProperty(String pPropertyName) {
		mChangedProperties.add(pPropertyName);

	}

	/* (non-Javadoc)
	 * @see atg.commerce.order.ChangedProperties#clearChangedProperties()
	 */
	@Override
	public void clearChangedProperties() {
		mChangedProperties.clear();
	}

	/* (non-Javadoc)
	 * @see atg.commerce.order.ChangedProperties#getRepositoryItem()
	 */
	@Override
	public MutableRepositoryItem getRepositoryItem() {
		return mRepositoryItem;
	}

	/* (non-Javadoc)
	 * @see atg.commerce.order.ChangedProperties#setRepositoryItem(atg.repository.MutableRepositoryItem)
	 */
	@Override
	public void setRepositoryItem(MutableRepositoryItem pRepositoryItem) {
		mRepositoryItem = pRepositoryItem;
	}

	/* (non-Javadoc)
	 * @see atg.commerce.order.ChangedProperties#getPropertyValue(java.lang.String)
	 */
	@Override
	public Object getPropertyValue(String pPropertyName) {
		MutableRepositoryItem mutItem = getRepositoryItem();
		if (mutItem == null) {
			throw new RuntimeException(MessageFormat.format(Constants.NULL_REPITEM_IN_COMMERCEITEM, new Object[] { getId() }));
		}
		return mutItem.getPropertyValue(pPropertyName);
	}

	/* (non-Javadoc)
	 * @see atg.commerce.order.ChangedProperties#setPropertyValue(java.lang.String, java.lang.Object)
	 */
	@Override
	public void setPropertyValue(String pPropertyName, Object pPropertyValue) {
		MutableRepositoryItem mutItem = getRepositoryItem();

		if (mutItem == null) {
			throw new RuntimeException(MessageFormat.format(Constants.NULL_REPITEM_IN_COMMERCEITEM, new Object[] { getId() }));
		}
		mutItem.setPropertyValue(pPropertyName, pPropertyValue);
		setChanged(true);
	}
}
