/*
 * @(#)TRUPropertyNameConstants.java	1.0 Oct 14, 2015
 *
 * Copyright (C) 2015, Professional Access (A division of Zensar
 * Technologies Limited).  All Rights Reserved. No use, copying or
 * distribution of this work may be made except in accordance with a
 * valid license agreement from PA, India. This notice must be included
 * on all copies, modifications and derivatives of this work.
 */
package com.tru.commerce.order;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import atg.core.i18n.LayeredResourceBundle;
import atg.service.dynamo.LangLicense;

import com.tru.common.TRUConstants;

/**
 * This TRUPropertyNameConstants holds constants for property names. Property will retrieved from resource bundle.
 *
 * @author Professional Access
 *
 *
 */
public class TRUPropertyNameConstants {

	/** The singleton instance. *//*
	private static TRUPropertyNameConstants SINGLETON_INSTANCE=new TRUPropertyNameConstants();
	*/
	/**
	 * This is a constructor.
	 *//*
	private TRUPropertyNameConstants() {
	}
*/
	/**
	 * Gets the single instance of TRUPropertyNameConstants.
	 *
	 * @return TRUPropertyNameConstants
	 *//*
	public static TRUPropertyNameConstants getInstance(){
		return SINGLETON_INSTANCE;
	}*/
	// Reading ResourceBundle and getting the reference of it.
	/** The s resource bundle. */
	private static ResourceBundle sResourceBundle = LayeredResourceBundle.getBundle(
			TRUConstants.TRU_PROPERTY_RESOURCES, LangLicense.getLicensedDefault());
	
	/** constant to hold ITEM AMOUNT. */
	public static final String BPP_ITEM_INFO = getStringResource("bppItemInfo");
	
	/** constant to hold ITEM AMOUNT. */
	public static final String ITEM_AMOUNT = getStringResource("itemAmount");

	/** constant to hold SURCHARGE. */
	public static final String SURCHARGE = getStringResource("surcharge");

	/** constant to hold SHIPPING CHARGE. */
	public static final String SHIPPING_CHARGE = getStringResource("shippingCharge");
	
	/** constant to hold E911 FEES. */
	public static final String E_911_FEES = getStringResource("e911Fees");

	/** constant to hold BOTLLE RECYCLE FEES. */
	public static final String BOTTLE_RECYCLE_FEES = getStringResource("bottleRecycleFees");
	
	/** constant to hold nickName. */
	public static final String NICK_NAME = getStringResource("nickName");
	
	/** constant to hold billingAddressNickName. */
	public static final String BILLING_ADDRESS_NICK_NAME = getStringResource("billingAddressNickName");

	/** constant to hold META INFO. */
	public static final String META_INFO = getStringResource("metaInfo");
	
	/** constant to hold giftCardNumber. */
	public static final String GIFT_CARD_NUMBER = getStringResource("giftCardNumber");
	
	/** constant to hold giftCardPin. */
	public static final String GIFT_CARD_PIN = getStringResource("giftCardPin");
	
	/** constant to hold cardHolderName. */
	public static final String CARD_HOLDER_NAME = getStringResource("cardHolderName");
	
	/** constant to hold remainingBalance. */
	public static final String REMAINING_BALANCE = getStringResource("remainingBalance");
	
	/** constant to hold currentBalance. */
	public static final String CURRENT_BALANCE = getStringResource("currentBalance");
	
	/** Constant to store the token. */
	public static final String PAYPAL_TOKEN  = getStringResource("payPalToken");
	
	/** Constant to store the payerId. */
	public static final String PAYPAL_PAYER_ID = getStringResource("payerId");
	
	/** Constant for PP_PAYER_STATUS. */
	public static final String PP_PAYER_STATUS = getStringResource("payerStatus");
	
	/** Constant for PP_CHECKOUT_STATUS. */
	public static final String PP_CHECKOUT_STATUS = getStringResource("checkoutStatus");
	
	/** payPal first name. */
	public static final String PP_FIRST_NAME = getStringResource("payerFirstName");
	
	/** payPal middle name. */
	public static final String PP_MIDDLE_NAME = getStringResource("payerMiddleName");
	
	/** payPal last name. */
	public static final String PP_LAST_NAME = getStringResource("payerLastName");
	
	/** payPal email. */
	public static final String PP_EMAIL = getStringResource("payerEmail");
	
	/** payPal phoneNumber. */
	public static final String PP_PHONE_NUMBER = getStringResource("payerPhoneNumber");
	
	/** payPal CorRelationId. */
	public static final String PAYPAL_CORRELATIONID = getStringResource("payPalCorrelationId");
	
	/** payPal payPalAck. */
	public static final String PAYPAL_ACK = getStringResource("payPalAck");
	
	/** payPal TransactionId. */
	public static final String PAYPAL_TRANSACTIONID = getStringResource("payPalTransactionId");
	
	/** payPal payPalGetExpTransactionTimestamp. */
	public static final String PAYPAL_TRANSACTION_TIMESTAMP = getStringResource("transactionTimestamp");
	
	/** payPal payment type. */
	public static final String PAYPAL_PAYMENT = getStringResource("payPal");
	
	/** Constant to store the token. */
	public static final String PAYPAL_STATUS_CORRELATIONID = getStringResource("correlationId");
	
	/** Constant to store the token. */
	public static final String PAYPAL_STATUS_PAYMENTSTATUS = getStringResource("paymentStatus");
	
	/** Constant to store the token. */
	public static final String PAYPAL_STATUS_ERRORCODE = getStringResource("errorCode");
	
	/** Constant to store the token. */
	public static final String PAYPAL_STATUS_PAYMENTPENDINGREASON = getStringResource("paymentPendingReason");
	
	/** Constant to store the token. */
	public static final String PAYPAL_STATUS_REASONCODE = getStringResource("reasonCode");
	
	/** Constant to store the coupon code. */
     public static final String SINGLE_USE_COUPON = getStringResource("singleUseCoupon");
     
    /** Constant to store the not applied coupon code. */
    public static final String NOT_APPLIED_COUPON_CODE = getStringResource("notAppliedCouponCode");
  	/** Constant to store the applePayTransactionId. */
	public static final String APPLEPAY_TRANSACTION_ID = getStringResource("applePayTransactionId");
	/** Constant to store the applePayEphemeralPublicKey. */
	public static final String APPLEPAY_EPHEMERAL_PUBLICKEY = getStringResource("applePayEphemeralPublicKey");
	/** Constant to store the applePayPublicKeyHash. */
	public static final String APPLEPAY_PUBLICKEY_HASH = getStringResource("applePayPublicKeyHash");
	/** Constant to store the applePayVersion. */
	public static final String APPLEPAY_VERSION = getStringResource("applePayVersion");
	/** Constant to store the applePayData. */
	public static final String APPLEPAY_DATA = getStringResource("applePayData");
	/** Constant to store the applePaySignature. */
	public static final String APPLEPAY_SIGNATURE = getStringResource("applePaySignature");
	/** Constant to store the applePayOrderId. */
	public static final String APPLEPAY_ORDERID = getStringResource("applePayOrderId");
    
	/**
	 * /**
	 * <p>
	 * This method getStringResource is used to get the value of key from ResourceBundle.
	 * </p>
	 * 
	 * @param pResourceName
	 *            - String
	 * @return ret - String
	 * @exception MissingResourceException
	 *                if any error occurs while reading the value from ResourceBundle.
	 * 
	 */
	public static String getStringResource(String pResourceName)throws MissingResourceException
	{
		// getting the value of key from ResourceBundle.
		final String ret = sResourceBundle.getString(pResourceName);
		// if the value is null then throwing a MissingResourceException
		if (ret == null) {
			final String str = TRUConstants.MSG_ERROR_UNABLE_TO_LOADRESOURCE + pResourceName;
			throw new MissingResourceException(str, TRUConstants.TRU_PROPERTY_RESOURCES,
					pResourceName);
		}
		// returning the value of key.
		return ret;

	}

	 /** The Constant PROP_NAME_PAYER_COUNTRY. */
 	public static final String PROP_NAME_PAYER_COUNTRY=getStringResource("payerCountry");
 	

}