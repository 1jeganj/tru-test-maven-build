package com.tru.commerce.order.utils;

/**
 * The Class TRUShippingPropertyManager.
 *
 * @author Professional Access.
 * @version 1.0
 */
public class TRUShippingPropertyManager {

	//FreightClass item properties
	/** The Constant FREIGHT_CLASS_NAME_PROEPRTY1. */
	public static final String FREIGHT_CLASS_NAME_PROEPRTY1="freightClass";
	
	/** The Constant SHIP_METHODS_PROEPRTY1. */
	public static final String SHIP_METHODS_PROEPRTY1 ="applicableShippingMethods";
	
	//ShipMethod item's properties
	/** The Constant ID1. */
	public static final String ID1 ="id";
	
	/** The Constant SHIP_METHOD_CODE1. */
	public static final String SHIP_METHOD_CODE1 ="methodCode";
	
	/** The Constant SHIP_METHOD_NAME1. */
	public static final String SHIP_METHOD_NAME1 ="methodName";
	
	/** The Constant FULL_FILLMENT_MSG1. */
	public static final String FULL_FILLMENT_MSG1 ="fullfillmentMsg";
	
	/** The Constant SHIPPING_REGION_PROPERTY_NAME1. */
	public static final String SHIPPING_REGION_PROPERTY_NAME1 ="shipRegion";
	
	/** The Constant REGION_CODE_PROPERTY_NAME1. */
	public static final String REGION_CODE_PROPERTY_NAME1 ="regionCode";
	
}
