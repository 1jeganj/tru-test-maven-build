package com.tru.commerce.order.utils;

import com.tru.common.TRUConstants;

/**
 * This class is TRURegionConstants.
 * @author PA
 * @version 1.0
 */
public enum TRURegionConstants {

	LOWER48(TRUConstants.LOWER48),
	LOWER48_PO_BOX(TRUConstants.LOWER48_PO_BOX),
	
	AK_HI(TRUConstants.AK_HI),
	AK_HI_PO_BOX(TRUConstants.AK_HI_PO_BOX),	
	
	US_TERRITORY(TRUConstants.US_TERRITORY),
	US_TERRITORY_PO_BOX(TRUConstants.US_TERRITORY_PO_BOX),
	
	APO_FPO(TRUConstants.APO_FPO);
	//PO_BOX("PO_BOX"),	
	
	
	

	private String mRegionCode;
	/**
	 * @param pS the regionCode to set
	 */
	private TRURegionConstants(String pS) {
		mRegionCode = pS;
	}
	/**
	 * @return the regionCode
	 */
	public String getRegion() {
		return mRegionCode;
	}

}
