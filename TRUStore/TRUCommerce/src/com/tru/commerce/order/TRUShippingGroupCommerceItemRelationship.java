/*
 * @(#)TRUShippingGroupCommerceItemRelationship.java	1.0 Oct 14, 2015
 *
 * Copyright (C) 2015, Professional Access (A division of Zensar
 * Technologies Limited).  All Rights Reserved. No use, copying or
 * distribution of this work may be made except in accordance with a
 * valid license agreement from PA, India. This notice must be included
 * on all copies, modifications and derivatives of this work.
 */
package com.tru.commerce.order;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.pricing.ItemPriceInfo;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.common.TRUConstants;


/**
 * This TRUShippingGroupCommerceItemRelationship holds custom properties to be updated in.
 * ShippingGroupCommerceItemRelationship item.
 *
 * @author Professional Access
 * @version 1.0
 *
 */
public class TRUShippingGroupCommerceItemRelationship extends ShippingGroupCommerceItemRelationship {

	/** Holds the mWarehouseMessage. */
	private String mWarehouseMessage ;
	
	/** Holds FAILED_UPDATE_REGISTRY_ITEM. */
	private static final String FAILED_UPDATE_REGISTRY_ITEM = "failedUpdateRegistryItem";

	/** Holds default serial version id. */
	private static final long serialVersionUID = 1L;

	/** Holds the mTRUMetaInfoItemsList. */
	private List<TRUCommerceItemMetaInfo> mTRUMetaInfoItemsList = null;

	/**
	 * Constant to hold double zero.
	 */
	private static final double DOUBLE_ZERO = 0.0;

	/** Constant for INT_ONE. */
	public static final int INT_ONE = 1;

	/**
	 * property PROP_IS_GIFT_ITEM.
	 */
	public static final String PROP_IS_GIFT_ITEM = "isGiftItem";

	/**
	 * property PROP_GIFT_ITEM_INFO.
	 */
	public static final String PROP_GIFT_ITEM_INFO = "giftItemInfo";

	/**
	 * property PROP_SHIP_WINDOW_MIN.
	 */
	public static final String PROP_SHIP_WINDOW_MIN = "estimateShipWindowMin";

	/**
	 * property PROP_SHIP_WINDOW_MAX.
	 */
	public static final String PROP_SHIP_WINDOW_MAX = "estimateShipWindowMax";

	/** The Bpp item info. */
	private TRUBppItemInfo mBppItemInfo = null;

	/**
	 * This method is used to get warehouseMessage.
	 * @return warehouseMessage String
	 */
	public String getWarehouseMessage() {
		return mWarehouseMessage;
	}

	/**
	 *This method is used to set warehouseMessage.
	 *@param pWarehouseMessage String
	 */
	public void setWarehouseMessage(String pWarehouseMessage) {
		mWarehouseMessage = pWarehouseMessage;
	}

	/**
	 * Sets the warehouse pickup.
	 *
	 * @param pIsWarehousePickup the new warehouse pickup
	 */
	public void setWarehousePickup(boolean pIsWarehousePickup) {
		 setPropertyValue(TRUCommerceConstants.WAREHOUSE_PICKUP_FLAG,pIsWarehousePickup);
	}

	/**
	 * Checks if is warehouse pickup.
	 *
	 * @return the boolean
	 */
	public Boolean isWarehousePickup() {
		boolean isWarehousePickup = Boolean.FALSE;
		if(getPropertyValue(TRUCommerceConstants.WAREHOUSE_PICKUP_FLAG) != null){
			isWarehousePickup = (Boolean)getPropertyValue(TRUCommerceConstants.WAREHOUSE_PICKUP_FLAG);
		}
		return isWarehousePickup;
	}
	
	/**
	 * Gets the bpp item info.
	 *
	 * @return the bpp item info
	 */
	public TRUBppItemInfo getBppItemInfo() {
		if (mBppItemInfo == null) {
			mBppItemInfo = new TRUBppItemInfo();
			MutableRepositoryItem item = (MutableRepositoryItem)getPropertyValue(TRUPropertyNameConstants.BPP_ITEM_INFO) ;
			if(item != null) {
				mBppItemInfo.setRepositoryItem((MutableRepositoryItem) getPropertyValue(TRUPropertyNameConstants.BPP_ITEM_INFO));
			}
		}
		return mBppItemInfo;
	}

	/**
	 * Sets the bpp item info id.
	 *
	 * @param pBppItemInfo the new bpp item info id
	 */
	public void setBppItemInfo(TRUBppItemInfo pBppItemInfo) {
		mBppItemInfo = pBppItemInfo;
		setPropertyValue(TRUPropertyNameConstants.BPP_ITEM_INFO, pBppItemInfo.getRepositoryItem());
	}

	/**
	 * Gets the item amount.
	 *
	 * @return the itemAmount
	 */
	public double getItemAmount() {
		if(getPropertyValue(TRUPropertyNameConstants.ITEM_AMOUNT) == null){
			return TRUConstants.DOUBLE_ZERO;
		}
		return (double) getPropertyValue(TRUPropertyNameConstants.ITEM_AMOUNT);
	}

	/**
	 * Sets the item amount.
	 *
	 * @param pItemAmount            the itemAmount to set
	 */
	public void setItemAmount(double pItemAmount) {
		setPropertyValue(TRUPropertyNameConstants.ITEM_AMOUNT, pItemAmount);
	}

	/**
	 * Gets the surcharge.
	 *
	 * @return the surcharge
	 */
	public double getSurcharge() {
		if(getPropertyValue(TRUPropertyNameConstants.SURCHARGE) == null){
			return TRUConstants.DOUBLE_ZERO;
		}
		return (double) getPropertyValue(TRUPropertyNameConstants.SURCHARGE);
	}

	/**
	 * Sets the surcharge.
	 *
	 * @param pSurcharge            the surcharge to set
	 */
	public void setSurcharge(double pSurcharge) {
		setPropertyValue(TRUPropertyNameConstants.SURCHARGE, pSurcharge);
	}

	/**
	 * Gets the shipping charge.
	 *
	 * @return the shippingCharge
	 */
	public double getShippingCharge() {
		if(getPropertyValue(TRUPropertyNameConstants.SHIPPING_CHARGE) == null){
			return TRUConstants.DOUBLE_ZERO;
		}
		return (double) getPropertyValue(TRUPropertyNameConstants.SHIPPING_CHARGE);
	}

	/**
	 * Sets the shipping charge.
	 *
	 * @param pShippingCharge            the shippingCharge to set
	 */
	public void setShippingCharge(double pShippingCharge) {
		setPropertyValue(TRUPropertyNameConstants.SHIPPING_CHARGE, pShippingCharge);
	}
	
	/**
	 * Gets the e911 fees.
	 *
	 * @return the e911Fees
	 */
	public double getE911Fees() {
		if(getPropertyValue(TRUPropertyNameConstants.E_911_FEES) == null){
			return TRUConstants.DOUBLE_ZERO;
		}
		return (double) getPropertyValue(TRUPropertyNameConstants.E_911_FEES);
	}

	/**
	 * Sets the e911 fees.
	 *
	 * @param pE911Fees            the e911Fees to set
	 */
	public void setE911Fees(double pE911Fees) {
		setPropertyValue(TRUPropertyNameConstants.E_911_FEES, pE911Fees);
	}

	/**
	 * Gets the bottle recycle fees.
	 *
	 * @return the bottleRecycleFees
	 */
	public double getBottleRecycleFees() {
		if(getPropertyValue(TRUPropertyNameConstants.BOTTLE_RECYCLE_FEES) == null){
			return TRUConstants.DOUBLE_ZERO;
		}
		return (double) getPropertyValue(TRUPropertyNameConstants.BOTTLE_RECYCLE_FEES);
	}

	/**
	 * Sets the bottle recycle fees.
	 *
	 * @param pBottleRecycleFees            the bottleRecycleFees to set
	 */
	public void setBottleRecycleFees(double pBottleRecycleFees) {
		setPropertyValue(TRUPropertyNameConstants.BOTTLE_RECYCLE_FEES, pBottleRecycleFees);
	}
	/**
	 * Gets the meta info.
	 *
	 * @return the metaInfo
	 */
	@SuppressWarnings("unchecked")
	public List<TRUCommerceItemMetaInfo> getMetaInfo() {
        if (mTRUMetaInfoItemsList == null) {
        	mTRUMetaInfoItemsList = new ArrayList<TRUCommerceItemMetaInfo>();
     }
     return mTRUMetaInfoItemsList;

	}

	/**
	 * Sets the meta info.
	 *
	 * @param pMetaInfo            the metaInfo to set
	 */
	public void setMetaInfo(List<TRUCommerceItemMetaInfo> pMetaInfo) {
		mTRUMetaInfoItemsList = pMetaInfo;
	}

	/**
	 * Gets the checks if is gift item.
	 *
	 * @return the isGiftItem
	 */
	public boolean getIsGiftItem() {
		if(null == getPropertyValue(PROP_IS_GIFT_ITEM)){
			return false;
		}
		return (boolean) getPropertyValue(PROP_IS_GIFT_ITEM);
	}

	/**
	 * Sets the checks if is gift item.
	 *
	 * @param pIsGiftItem the new checks if is gift item
	 */
	public void setIsGiftItem(boolean pIsGiftItem) {
		setPropertyValue(PROP_IS_GIFT_ITEM, pIsGiftItem);
	}
	
	/**
	 * Gets the amount for relation ship.
	 *
	 * @return the amount for relation ship
	 */
	public double getAmountForRelationShip()
	{
		final ItemPriceInfo priceInfo = getCommerceItem().getPriceInfo();
		if (priceInfo == null){
			return DOUBLE_ZERO;
		}
		if(getCommerceItem().getShippingGroupRelationshipCount() > INT_ONE &&(null==getMetaInfo()|| getMetaInfo().isEmpty())){
			final double salePrice = priceInfo.getSalePrice();
			return (salePrice * getQuantity());
		}
		else{
			return getAmountByAverage();	
		}
	}
	
	/**
	 * Gets the gift item info.
	 *
	 * @return the giftItemInfo
	 */
	@SuppressWarnings("unchecked")
	public List<RepositoryItem> getGiftItemInfo() {
		return (List<RepositoryItem>) getPropertyValue(PROP_GIFT_ITEM_INFO);
	}

	/**
	 * Sets the gift item info.
	 *
	 * @param pGiftItemInfo the giftItemInfo to set
	 */
	public void setGiftItemInfo(List<RepositoryItem> pGiftItemInfo) {
		setPropertyValue(PROP_GIFT_ITEM_INFO, pGiftItemInfo);
	}
	
	/**
	 * Gets the estimate ship window min.
	 *
	 * @return the estimateShipWindowMin
	 */
	public int getEstimateShipWindowMin() {
		return (int) getPropertyValue(PROP_SHIP_WINDOW_MIN);
	}

	/**
	 * Sets the estimate ship window min.
	 *
	 * @param pEstimateShipWindowMin the estimateShipWindowMin to set
	 */
	public void setEstimateShipWindowMin(int pEstimateShipWindowMin) {
		setPropertyValue(PROP_SHIP_WINDOW_MIN, pEstimateShipWindowMin);
	}

	/**
	 * Gets the estimate ship window max.
	 *
	 * @return the estimateShipWindowMax
	 */
	public int getEstimateShipWindowMax() {
		return (int) getPropertyValue(PROP_SHIP_WINDOW_MAX);
	}

	/**
	 * Sets the estimate ship window max.
	 *
	 * @param pEstimateShipWindowMax the estimateShipWindowMax to set
	 */
	public void setEstimateShipWindowMax(int pEstimateShipWindowMax) {
		setPropertyValue(PROP_SHIP_WINDOW_MAX, pEstimateShipWindowMax);
	}

	/**
	 * Holds the future date for preSellItem
	 */
	private Date mStreetDate;

	/**
	 * 
	 * @return StreetDate
	 */
	public Date getStreetDate() {
		return mStreetDate;
	}

	/**
	 * 
	 * @param pStreetDate StreetDate
	 */
	public void setStreetDate(Date pStreetDate) {
		this.mStreetDate = pStreetDate;
	}

	/**
	 * @return the failedUpdateRegistryItem
	 */
	public boolean isFailedUpdateRegistryItem() {
		if(null == getPropertyValue(FAILED_UPDATE_REGISTRY_ITEM)){
			return false;
		}
		return (boolean) getPropertyValue(FAILED_UPDATE_REGISTRY_ITEM);
	}

	/**
	 * @param pFailedUpdateRegistryItem the failedUpdateRegistryItem to set
	 */
	public void setFailedUpdateRegistryItem(boolean pFailedUpdateRegistryItem) {
		setPropertyValue(FAILED_UPDATE_REGISTRY_ITEM, pFailedUpdateRegistryItem);
	}
	
}
