/*
 * 
 */
package com.tru.commerce.order;

import java.sql.Timestamp;

import atg.commerce.order.CreditCard;

/**
 * The Class TRUCreditCard.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUCreditCard extends CreditCard {
	
	 /**
     * property TRANSACTION_ID.
     */
	private static final String PROP_NAME_TRANSACTION_ID = "transactionId";
    
    /**
     * property PROP_NAME_ON_CARD.
     */
	private static final String PROP_NAME_ON_CARD = "nameOnCard";
	
    /**
     * property PROP_REVERSED_AMOUNT.
     */
	private static final String PROP_REVERSED_AMOUNT = "reversedAmount";
    
    /**
	 * Constant  to hold double zero.
	 */
	private static final double DOUBLE_ZERO = 0.0;
	
	/**
	 * Constant  to hold threeDSecure String.
	 */
	private static final String THREE_D_SECURE= "threeDSecure";
	
	/**
     * property CAVV.
     */
	private static final String CAVV = "cavv";
    
    /**
     * property ECI_FLAG.
     */
	private static final String ECI_FLAG = "eciFlag";
    
    /**
     * property PARES_STATUS.
     */
	private static final String PARES_STATUS = "paResStatus";
    
    /**
     * property SIGNATURE_VERIFICATION.
     */
	private static final String SIGNATURE_VERIFICATION = "signatureVerification";
    
    /**
     * property XID.
     */
	private static final String XID = "xid";
    
    /**
     * property ENROLLED.
     */
	private static final String ENROLLED = "enrolled";
    
    /**
     * property TOKEN.
     */
	private static final String TOKEN = "token";
    
    /**
     * property REASON_CODE.
     */
	private static final String REASON_CODE = "reasonCode";
    
    /**
     * property REASON_DESCRIPTION.
     */
	private static final String REASON_DESCRIPTION = "reasonDescription";
    
    /**
     * property ACTION_CODE.
     */
	private static final String ACTION_CODE = "actionCode";
    
    /**
     * property ERROR_NUMBER.
     */
	private static final String ERROR_NUMBER = "errorNumber";
    
    /**
     * property ERROR_DESCRIPTION.
     */
	private static final String ERROR_DESCRIPTION = "errorDescription";

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	/** The Constant PROP_LAST_ACTIVITY. */
	private Timestamp mLastActivity;
	
	/**
     * The constant to hold the retryCount.
     */
	private static final String RETRY_COUNT = "retryCount";
	
	/**
     * The constant to hold the PAYMENT_RETRY_ACCEPT.
     */
	private static final String PAYMENT_RETRY_ACCEPT = "paymentRetryAccept";
	
	/**
     * The constant to hold the RESPONSE_CODE.
     */
	private static final String RESPONSE_CODE = "responseCode";
	
	/**
	 * Gets the name on card.
	 *
	 * @return the nameOnCard.
	 */
	public String getNameOnCard() {
		return (String) getPropertyValue(PROP_NAME_ON_CARD);
	}

	
	/**
	 * Sets the name on card.
	 *
	 * @param pNameOnCard the new name on card
	 */
	public void setNameOnCard(String pNameOnCard) {
		setPropertyValue(PROP_NAME_ON_CARD, pNameOnCard);
	}
	
	 /**
 	 * Gets the reversal amount.
 	 *
 	 * @return the reversalAmount
 	 */
    public double getReversalAmount() {
        final Object loRevrsalAmount = (Object) getPropertyValue(PROP_REVERSED_AMOUNT);
        if (loRevrsalAmount != null) {
            return ((Double)loRevrsalAmount).doubleValue();
        } else {
            return DOUBLE_ZERO;
        }
    }    
    
    /**
     * Sets the reversal amount.
     *
     * @param pReversalAmount the reversalAmount to set
     */
    public void setReversalAmount(double pReversalAmount) {
        setPropertyValue(PROP_REVERSED_AMOUNT, pReversalAmount);
    }
    
    /**
     * Gets the transaction id.
     *
     * @return the Transaction Id.
     */
    public String getTransactionId() {
        return (String)getPropertyValue(PROP_NAME_TRANSACTION_ID);
    }

    /**
     * Sets the transaction id.
     *
     * @param pTransactionId the cardHolderName to set
     */
    public void setTransactionId(String pTransactionId) {
        setPropertyValue(PROP_NAME_TRANSACTION_ID, pTransactionId);
    }
    
    /**
     * Gets the billing address nick name.
     *
     * @return the billing address nick name.
     */
	public String getBillingAddressNickName() {
		return (String) getPropertyValue(TRUPropertyNameConstants.BILLING_ADDRESS_NICK_NAME);
	}

	/**
	 * Sets the billingAddressNickName.
	 * 
	 * @param pBillingAddressNickName
	 *            - the billing address nick name
	 */
	public void setBillingAddressNickName(String pBillingAddressNickName) {
		setPropertyValue(TRUPropertyNameConstants.BILLING_ADDRESS_NICK_NAME, pBillingAddressNickName);
	}
	
	/**
	 * Checks if is three d secure.
	 *
	 * @return the billing address nick name.
	 */
	public Boolean isThreeDSecure() {
		return (Boolean) getPropertyValue(THREE_D_SECURE);
	}

	/**
	 * Sets the billingAddressNickName.
	 * 
	 * @param pBillingAddressNickName
	 *            - the billing address nick name
	 */
	public void setThreeDSecure(String pBillingAddressNickName) {
		setPropertyValue(THREE_D_SECURE, pBillingAddressNickName);
	}
	
	/**
	 * Gets the cavv.
	 *
	 * @return the cavv.
	 */
	 public String getCavv() {
		return (String) getPropertyValue(CAVV);
	 }

	 /**
	 * Sets the cavv.
	 * 
	 * @param pCavv
	 *            - the cavv
	 */
	 public void setCavv(String pCavv) {
		setPropertyValue(CAVV, pCavv);
	 }
	 
	 /**
 	 * Gets the eci flag.
 	 *
 	 * @return the eciFlag.
 	 */
	 public String getEciFlag() {
		return (String) getPropertyValue(ECI_FLAG);
	 }

	 /**
	 * Sets the paResStatus.
	 * 
	 * @param pEciFlag
	 *            - the paResStatus
	 */
	 public void setEciFlag(String pEciFlag) {
		setPropertyValue(ECI_FLAG, pEciFlag);
	 }
	 
	 /**
 	 * Gets the pa res status.
 	 *
 	 * @return the paResStatus.
 	 */
	 public String getPaResStatus() {
		return (String) getPropertyValue(PARES_STATUS);
	 }

	 /**
	 * Sets the paResStatus.
	 * 
	 * @param pPaResStatus
	 *            - the paResStatus
	 */
	 public void setPaResStatus(String pPaResStatus) {
		setPropertyValue(PARES_STATUS, pPaResStatus);
	 }
	 
	 /**
 	 * Gets the signature verification.
 	 *
 	 * @return the signatureVerification.
 	 */
	 public String getSignatureVerification() {
		return (String) getPropertyValue(SIGNATURE_VERIFICATION);
	 }

	 /**
	 * Sets the signatureVerification.
	 * 
	 * @param pSignatureVerification
	 *            - the signatureVerification
	 */
	 public void setSignatureVerification(String pSignatureVerification) {
		setPropertyValue(SIGNATURE_VERIFICATION, pSignatureVerification);
	 }
	 
	 /**
 	 * Gets the xid.
 	 *
 	 * @return the xid.
 	 */
	 public String getXid() {
		return (String) getPropertyValue(XID);
	 }

	 /**
	 * Sets the xid.
	 * 
	 * @param pXid
	 *            - the xid
	 */
	 public void setXid(String pXid) {
		setPropertyValue(XID, pXid);
	 }
	 
	 /**
 	 * Gets the enrolled.
 	 *
 	 * @return the enrolled.
 	 */
	 public String getEnrolled() {
		return (String) getPropertyValue(ENROLLED);
	 }

	 /**
	 * Sets the enrolled.
	 * 
	 * @param pEnrolled
	 *            - the enrolled
	 */
	 public void setEnrolled(String pEnrolled) {
		setPropertyValue(ENROLLED, pEnrolled);
	 }
	 
	 
	 /**
 	 * Gets the token.
 	 *
 	 * @return the token.
 	 */
	 public String getToken() {
		return (String) getPropertyValue(TOKEN);
	 }

	 /**
	 * Sets the token.
	 * 
	 * @param pToken
	 *            - the token
	 */
	 public void setToken(String pToken) {
		setPropertyValue(TOKEN, pToken);
	 }

	 /**
 	 * Gets the reason code.
 	 *
 	 * @return the reasonCode.
 	 */
	 public String getReasonCode() {
		return (String) getPropertyValue(REASON_CODE);
	 }

	 /**
	 * Sets the reasonCode.
	 * 
	 * @param pReasonCode
	 *            - the reasonCode
	 */
	 public void setReasonCode(String pReasonCode) {
		setPropertyValue(REASON_CODE, pReasonCode);
	 }
	 
	 /**
 	 * Gets the reason description.
 	 *
 	 * @return the reasonDescription.
 	 */
	 public String getReasonDescription() {
		return (String) getPropertyValue(REASON_DESCRIPTION);
	 }

	 /**
	 * Sets the reasonDescription.
	 * 
	 * @param pReasonDescription
	 *            - the reasonDescription
	 */
	 public void setReasonDescription(String pReasonDescription) {
		setPropertyValue(REASON_DESCRIPTION, pReasonDescription);
	 }
	 
	 /**
 	 * Gets the action code.
 	 *
 	 * @return the actionCode.
 	 */
	 public String getActionCode() {
		return (String) getPropertyValue(ACTION_CODE);
	 }

	 /**
	 * Sets the actionCode.
	 * 
	 * @param pActionCode
	 *            - the actionCode
	 */
	 public void setActionCode(String pActionCode) {
		setPropertyValue(ACTION_CODE, pActionCode);
	 }
	
	 /**
 	 * Gets the error number.
 	 *
 	 * @return the errorNumber.
 	 */
	 public String getErrorNumber() {
		return (String) getPropertyValue(ERROR_NUMBER);
	 }

	 /**
	 * Sets the errorNumber.
	 * 
	 * @param pErrorNumber
	 *            - the errorNumber
	 */
	 public void setErrorNumber(String pErrorNumber) {
		setPropertyValue(ERROR_NUMBER, pErrorNumber);
	 }
	 
	 /**
 	 * Gets the error description.
 	 *
 	 * @return the errorDescription.
 	 */
	 public String getErrorDescription() {
		return (String) getPropertyValue(ERROR_DESCRIPTION);
	 }

	 /**
	 * Sets the errorDescription.
	 * 
	 * @param pErrorDescription
	 *            - the errorDescription
	 */
	 public void setErrorDescription(String pErrorDescription) {
		setPropertyValue(ERROR_DESCRIPTION, pErrorDescription);
	 }

	 /**
 	 * Gets the last activity.
 	 *
 	 * @return lastActivity
 	 */
	public Timestamp getLastActivity() {
		return mLastActivity;
	}

	/**
	 * Sets the lastActivity.
	 * 
	 * @param pLastActivity - the lastActivity
	 */
	public void setLastActivity(Timestamp pLastActivity) {
		this.mLastActivity = pLastActivity;
	}
	 

	 /**
 	 * Gets the error description.
 	 *
 	 * @return the errorDescription.
 	 */
	 public int getRetryCount() {
		return (int) getPropertyValue(RETRY_COUNT);
	 }

	 /**
 	 * Sets the errorDescription.
 	 *
 	 * @param pRetryCount the new retry count
 	 */
	 public void setRetryCount(int pRetryCount) {
		setPropertyValue(RETRY_COUNT, pRetryCount);
	 }
	 
	 /**
 	 * Gets the Retry accept payment.
 	 *
 	 * @return the Boolean.
 	 */
	 public Boolean getPaymentRetryAccept() {
		return (Boolean) getPropertyValue(PAYMENT_RETRY_ACCEPT);
	 }

	 /**
	 * Sets the Retry accept payment.
	 * 
	 * @param pRetryCount
	 *            - the RetryCount
	 */
	 public void setPaymentRetryAccept(Boolean pRetryCount) {
		setPropertyValue(PAYMENT_RETRY_ACCEPT, pRetryCount);
	 }
	 
	 /**
	 * Sets the ResponseCode.
	 * 
	 * @param pResponseCode
	 *            - the errorNumber
	 */
	 public void setResponseCode(String pResponseCode) {
		setPropertyValue(RESPONSE_CODE, pResponseCode);
	 }
	 
	 /**
 	 * Gets the Response code.
 	 *
 	 * @return the String.
 	 */
	 public String getResponseCode() {
		return (String) getPropertyValue(RESPONSE_CODE);
	 }
	 
		
}
