
package com.tru.commerce.order.processor;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.commerce.order.processor.BillingAddrValidator;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.nucleus.GenericService;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.common.TRUErrorKeys;
import com.tru.common.cml.SystemException;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;
import com.tru.radial.commerce.payment.paypal.TRUPaypalConstants;

/**
 * This  class validates PayPal payment group's billing address.
 * 
 * @author Professional Access 
 * @version 1.0
 */
public class ProcTRUValidateBillingAddress extends GenericService implements PipelineProcessor
{
	
	/**
	 * Resource bundle name.
	 */
	private static final String ORDER_RESOURCE_NAME = "atg.commerce.order.OrderResources";

	 /**
     * Constant to hold the PAYPAL_LOGGING_IDENTIFIER.
     */
    public static final String USER_MESSAGE_BUNDLE = "atg.commerce.order.UserMessages";
    
	/**
	 * InvalidOrderParameter.
	 */
	private  static final String MSG_INVALID_ORDER_PARAMETER = "InvalidOrderParameter";
	
	/** Property to Hold Error handler manager. 
	 * 
	 */
	private DefaultErrorHandlerManager mErrorHandlerManager;

	/**
	 * @return the errorHandlerManager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * @param pErrorHandlerManager the errorHandlerManager to set
	 */
	public void setErrorHandlerManager(
			DefaultErrorHandlerManager pErrorHandlerManager) {
		mErrorHandlerManager = pErrorHandlerManager;
	}

	/** 
	 * Resource Bundle. 
	 */
	private static java.util.ResourceBundle sOrderResourceBundle = 
			LayeredResourceBundle.getBundle(ORDER_RESOURCE_NAME, atg.service.dynamo.LangLicense.getLicensedDefault());
	
	
	/**
	 * This property holds sUserResourceBundle.
	 */
	private static ResourceBundle sUserResourceBundle = LayeredResourceBundle.getBundle(USER_MESSAGE_BUNDLE, Locale.getDefault());

	/**
	 * This property holds the success code.
	 */
	private static final  int SUCCESS_CODE = TRUPaypalConstants.ONE;
	
	/**
	 * This property holds the error code.
	 */
	private static final  int ERROR_CODE = -1;
	
	/**
	 * This property holds the return codes.
	 */
	private static final int[] RETURN_CODES = { SUCCESS_CODE ,ERROR_CODE };
	
	/**
	 * property to hold mLoggingIdentifier.
	 */
	  private  String mLoggingIdentifier;
	/**
	 * property to hold mBillingAddressValidator.
	 */
	private BillingAddrValidator mBillingAddressValidator;
	
	/**
	 * Default constructor.
	 */
	public ProcTRUValidateBillingAddress()
	{
		this.mLoggingIdentifier = TRUPaypalConstants.PAYPAL_LOGGING_IDENTIFIER;
	}

	/**
	 * @param pLoggingIdentifier the mLoggingIdentifier to set
	 */
	public void setLoggingIdentifier(String pLoggingIdentifier)
	{
		this.mLoggingIdentifier = pLoggingIdentifier;
	}

	/**
	 * Gets the loggingIdentifier.
	 * 
	 * @return the LoggingIdentifier
	 */
	public String getLoggingIdentifier()
	{
		return this.mLoggingIdentifier;
	}

	/**
	 * Sets the BillingAddressValidator.
	 * 
	 * @param pBillingAddressValidator the mBillingAddressValidator to set
	 */
	public void setBillingAddressValidator(BillingAddrValidator pBillingAddressValidator)
	{
		this.mBillingAddressValidator = pBillingAddressValidator;
	}

	/**
	 * Gets the BilingAddressValidatory.
	 * 
	 * @return the bBllingAddressValidator
	 */
	public BillingAddrValidator getBillingAddressValidator() {
		return this.mBillingAddressValidator;
	}

	/**
	 * This method will give return codes.
	 * 
	 * 
	 * @return retrun code
	 */
	@Override
	public int[] getRetCodes() {
		return RETURN_CODES;
	}

	/**
	 * This method will do  Order billing address validation.
	 * 
	 * @param pParamObject the paramObject
	 * @param pParamPipelineResult - the paramPipelineResult
	 * @return success  or failure
	 * @throws InvalidParameterException occurs while validating payment group
	 */
	@Override
	public int runProcess(Object pParamObject, PipelineResult pParamPipelineResult)
	throws  InvalidParameterException  {
		if (isLoggingDebug()) {
			vlogDebug("Start of ProcTRUValidateBillingAddress.runProcess() method");
		}
		
		Map map = (HashMap) pParamObject;
		Order order = (Order) map.get(PipelineConstants.ORDER);
		if (order == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(
					MSG_INVALID_ORDER_PARAMETER, ORDER_RESOURCE_NAME,
					sOrderResourceBundle));
		}
		ResourceBundle userResourceBundle = sUserResourceBundle;
		BillingAddrValidator bav = getBillingAddressValidator();
		if(null == ((TRUOrderImpl)order).getBillingAddress()){
			String errorMessage = null;
			try {
				errorMessage = getErrorHandlerManager().getErrorMessage(TRUErrorKeys.TRU_BILLING_ADDRESS_EMPTY);
			} catch (SystemException sysExp) {
				if(isLoggingError()){
					logError("SystemException occured at ProcTRUValidateBillingAddress/runProcess",sysExp);
				}
			}
			pParamPipelineResult.addError(TRUErrorKeys.TRU_BILLING_ADDRESS_EMPTY, errorMessage);
			return ERROR_CODE ;
		}else{
			bav.validateBillingAddress(((TRUOrderImpl)order).getBillingAddress(), TRUCheckoutConstants.ORDER_BILLING_ADDRESS, pParamPipelineResult, userResourceBundle);
		}		
		if (isLoggingDebug()) {
			vlogDebug("End of ProcTRUValidateBillingAddress.runProcess() method");
		}
		return SUCCESS_CODE;
	}

}