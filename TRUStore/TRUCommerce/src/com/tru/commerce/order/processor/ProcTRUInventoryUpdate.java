package com.tru.commerce.order.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.core.util.ResourceUtils;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.nucleus.naming.ComponentName;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;
import atg.servlet.ServletUtil;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.inventory.TRUInventoryConstants;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.common.TRUUserSession;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;

/**
 *  @author Professional access
 *  @version 1.0
 */
public class ProcTRUInventoryUpdate extends ApplicationLoggingImpl implements
		PipelineProcessor {
	/**
	 * Success constant.
	 */
	private static final int SUCCESS = TRUPipeLineConstants.NUMERIC_ONE;
	
	/**
	 * Failure constant.
	 */
	private static final int STOP_CHAIN_EXECUTION = TRUPipeLineConstants.NUMERIC_ZERO;

	/**
	 * Property to hold OrderResources.
	 */
	private static final String MY_RESOURCE_NAME = "atg.commerce.order.OrderResources";

	/**  Resource Bundle *. */
	private static java.util.ResourceBundle sResourceBundle = java.util.ResourceBundle
			.getBundle(MY_RESOURCE_NAME,
					atg.service.dynamo.LangLicense.getLicensedDefault());
	
	/** The m error handler manager. */
	private DefaultErrorHandlerManager mErrorHandlerManager;
	
	/**
	 * Gets the error handler manager.
	 *
	 * @return the error handler manager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * Sets the error handler manager.
	 *
	 * @param pErrorHandlerManager the new error handler manager
	 */
	public void setErrorHandlerManager(
			DefaultErrorHandlerManager pErrorHandlerManager) {
		this.mErrorHandlerManager = pErrorHandlerManager;
	}
	/**
	 * Returns the valid return codes:
	 * 1 - The processor completed.
	 * 
	 * @return an integer array of the valid return codes.
	 */
	@Override
	public int[] getRetCodes() {
		int[] ret = {SUCCESS, STOP_CHAIN_EXECUTION};
		return ret;
	}

	/**
	 * Updates the inventory for all items in the order.
	 *
	 * @param pParam a HashMap which must contain an Order and optionally a Locale object.
	 * @param pResult a PipelineResult object which stores any information which must
	 *                be returned from this method invocation.
	 *                
	 * @return an integer specifying the processor's return code.
	 * @throws Exception Exception
	 * @see atg.service.pipeline.PipelineProcessor#runProcess(Object, PipelineResult)
	 */
	@Override
	public int runProcess(Object pParam, PipelineResult pResult)
			throws Exception {
		if (isLoggingDebug()) {
			vlogDebug("Start ProcTRUInventoryUpdate.runProcess()");
		}
		Map map = (HashMap) pParam;
		String errorMessage = null;
		int status = TRUCommerceConstants.INT_ZERO;
		Order order = (Order) map.get(PipelineConstants.ORDER);
		TRUOrderManager orderManager = (TRUOrderManager) map
				.get(PipelineConstants.ORDERMANAGER);
		if (order == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(
					TRUPipeLineConstants.INVALID_ORDER_PARAMETER,
					MY_RESOURCE_NAME, sResourceBundle));
		}

		if (isLoggingDebug()) {
			vlogDebug("order id : {0}", order.getId());
		}
		
		Map<String, String> qtyAdjustedSkuList = new HashMap<String, String>();
		status = orderManager.updateInventory(order, qtyAdjustedSkuList);
		if (isLoggingDebug()) {
			vlogDebug("status : {0} and qtyAdjustedSkuList: {1}", status, qtyAdjustedSkuList);
		}

		if (qtyAdjustedSkuList.isEmpty() && status == TRUInventoryConstants.INVENTORY_STATUS_SUCCEED) {
			if (isLoggingDebug()) {
				vlogDebug("End ProcTRUInventoryUpdate.runProcess() return 1");
			}
			return SUCCESS;
		} else {
			if (isLoggingError()) {
				vlogError( "Failed Order ProcTRUInventoryUpdate.runProcess(): {0}", order.getId());
			}
			errorMessage = getErrorHandlerManager().getErrorMessage(TRUCheckoutConstants.INVENTORY_STATUS_INSUFFICIENT_SUPPLY);
			pResult.addError(TRUCheckoutConstants.INVENTORY, errorMessage);
			((TRUOrderImpl) order).setValidationFailed(Boolean.TRUE);
			
			TRUUserSession sessionComponent= (TRUUserSession)ServletUtil.getCurrentRequest().resolveName(TRU_SESSION_SCOPE_COMPONENT_NAME);
			List<String> qtyAdjustedList = getShoppingCartUtils().getQtyAdjustedInfoFromMerge(qtyAdjustedSkuList);
			sessionComponent.setQtyAdjustedSkuList(qtyAdjustedList);
						
			if (isLoggingDebug()) {
				vlogDebug("End ProcTRUInventoryUpdate.runProcess() return 0");
			}
			return STOP_CHAIN_EXECUTION;
		}
	}
	
	/** The Constant TRU_SESSION_SCOPE_COMPONENT_NAME. */
	public final static ComponentName TRU_SESSION_SCOPE_COMPONENT_NAME = ComponentName.getComponentName(TRUCommerceConstants.TRU_SESSION_SCOPE_COMPONENT_NAME);
	
	/** The Shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;

	/**
	 * @return the shoppingCartUtils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * @param pShoppingCartUtils the shoppingCartUtils to set
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		mShoppingCartUtils = pShoppingCartUtils;
	}
	
}
