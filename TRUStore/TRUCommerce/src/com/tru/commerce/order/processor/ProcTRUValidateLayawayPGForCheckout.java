package com.tru.commerce.order.processor;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PipelineConstants;
import atg.commerce.order.processor.ValidatePaymentGroupPipelineArgs;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;
import atg.service.pipeline.RunProcessException;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.common.TRUConstants;

/**
 * This processor validates PaymentGroups before checking an Order out. The two main
 * things which are checked for are that all CommerceItems, shipping costs, and tax in
 * the Order are assigned to PaymentGroups and that all the required fields in all the
 * PaymentGroups, regardless of type, are not null or empty String.
 *
 * The first check, that all CommerceItems, shipping costs, and tax are assigned to
 * PaymentGroups, first checks to see if there is only one PaymentGroup and it has no

 * Relationships. If so, then all CommerceItems, shipping costs, and tax are implicitly
 * assigned to that PaymentGroup. If there is more than one PaymentGroup or the PaymentGroup
 * has at least one Relationship, then all CommerceItems, ShippingGroups, and the Order must
 * be assigned to one or more PaymentGroups.
 *
 * The second check, that required fields are provided, iterates over all the PaymentGroups
 * in the order and invokes a separate pipeline chain for each payment group.  The first
 * processor in that chain is responsible for examining the type of the payment group and
 * dispatching to an appropriate validation processor or processor chain that knows which
 * fields are required for each payment method.
 *
 * @author Manny Parasirakis, Matt Landau
 * @version $Id: //product/DCS/version/11.1/Java/atg/commerce/order/processor/ProcValidatePaymentGroupsForCheckout.java#2 $$Change: 890057 $
 */
public class ProcTRUValidateLayawayPGForCheckout extends ApplicationLoggingImpl implements PipelineProcessor {  
  /** Class version string .
   */
  public static final String CLASS_VERSION = "$Id: //product/DCS/version/11.1/Java/atg/commerce/order/processor/ProcValidatePaymentGroupsForCheckout.java#2 $$Change: 890057 $";
  /** RESOURCE_NAME.
   */
  static final String RESOURCE_NAME = "atg.commerce.order.OrderResources";
  /** USER_MSGS_RES_NAME.
   */
  static final String USER_MSGS_RES_NAME = "atg.commerce.order.UserMessages";

  /** Resource Bundle.
   */
  private static ResourceBundle sResourceBundle = LayeredResourceBundle.getBundle(RESOURCE_NAME, atg.service.dynamo.LangLicense.getLicensedDefault());
  
  /**
   * Returns the valid return codes.  This processor always returns
   * a status of 1 indicating successful completion.
   **/
  private static final int SUCCESS = 1;
  /** ValidatePaymentGroupChain.
   */
  private String mValidatePaymentGroupChain = TRUCheckoutConstants.VALIDATE_PAYMENT_GROUP;
  
  /** mRetCodes.
   */
  private int[] mRetCodes = { SUCCESS };
  
  /** LoggingIdentifier.
   */
  private String mLoggingIdentifier = TRUCheckoutConstants.PROC_TRU_VALIDATE_LAYAWAY_ORDER_PG;
  /**
   * Set the name of the pipeline chain to run to validate each individual payment group.
   * @param pValidatePaymentGroupChain - String
   **/
  public void setValidatePaymentGroupChain(String pValidatePaymentGroupChain) {
    mValidatePaymentGroupChain = pValidatePaymentGroupChain;
  }

  /**
   * Get the name of the pipeline chain to run to validate each individual payment group. The default chain name is "validatePaymentGroup".
   * @return mValidatePaymentGroupChain - String
   **/
  public String getValidatePaymentGroupChain() {
    return mValidatePaymentGroupChain;
  }
  
  /**
   * This method returns retCodes.
   * @return mRetCodes - int[]
   */
  public int[] getRetCodes()
  {
    return mRetCodes;
  } 

  /** Sets property LoggingIdentifier. 
   * @param pLoggingIdentifier - String
   */
  public void setLoggingIdentifier(String pLoggingIdentifier) {
    mLoggingIdentifier = pLoggingIdentifier;
  }

  /** Returns property LoggingIdentifier. 
   * @return mLoggingIdentifier - String
   */
  public String getLoggingIdentifier() {
    return mLoggingIdentifier;
  }

  
  /**
   * This method performs two checks on all the PaymentGroups in the given Order.
   * The first check is to see if all the CommerceItems, shipping costs, and tax
   * in the Order are assigned to PaymentGroups. The second check is to determine
   * if all the required properties are not null or empty String. The properties
   * which are checked are as follows.
   *
   * This method requires that an Order, an OrderManager, and optionally a Locale
   * object be supplied in pParam in a HashMap. Use the PipelineConstants class
   * static members to key the objects in the HashMap.
   *
   * @param pParam a HashMap which must contain an Order, an OrderManager, and optionally a Locale object
   * @param pResult a PipelineResult object which stores any information which must be returned from this method invokation
   * @return an integer specifying the processor's return code
   * @throws InvalidParameterException  InvalidParameterException
   * @throws RunProcessException  RunProcessException
   * @see atg.service.pipeline.PipelineProcessor#runProcess(Object, PipelineResult)
   **/
  
  public int runProcess(Object pParam, PipelineResult pResult) throws InvalidParameterException, RunProcessException {
    Map map = (HashMap) pParam;
    Order order = (Order) map.get(PipelineConstants.ORDER);
    OrderManager om = (OrderManager)map.get(PipelineConstants.ORDERMANAGER);
    Locale locale = (Locale) map.get(PipelineConstants.LOCALE);

    if(order == null) {
    	throw new InvalidParameterException(ResourceUtils.getMsgResource(TRUCheckoutConstants.INVALID_ORDER_PARAMETER, RESOURCE_NAME, sResourceBundle));
    }

    PaymentGroup pg;
    // This section iterates over all payment groups and invokes a pipeline
    // chain to validate each payment group in turn.

    Iterator paymentGroups = order.getPaymentGroups().iterator();
    ValidatePaymentGroupPipelineArgs param = new ValidatePaymentGroupPipelineArgs();
    PipelineResult newResult;
    
    param.setOrder(order);
    param.setOrderManager(om);
    if(locale != null){
    	param.setLocale(locale);
    }

    while (paymentGroups.hasNext())
    {
      pg = (PaymentGroup) paymentGroups.next();

      // Skip empty payment groups

      if(!om.getPaymentGroupManager().isPaymentGroupUsed(order, pg)) {
    	  continue;
      }

      param.setPaymentGroup(pg);

      newResult = om.getPipelineManager().runProcess(getValidatePaymentGroupChain(), param);
      if (newResult.hasErrors()){
        pResult.copyInto(newResult);
        return STOP_CHAIN_EXECUTION_AND_ROLLBACK;
      }
    }
    return SUCCESS;
  }


  /**
   * This method adds an error to the PipelineResult object. This method, rather than
   * just storing a single error object in pResult, stores a Map of errors. This allows more
   * than one error to be stored using the same key in the pResult object. pKey is
   * used to reference a HashMap of errors in pResult. So, calling
   * pResult.getError(pKey) will return an object which should be cast to a Map.
   * Each entry within the map is keyed by pId and its value is pError.
   *
   * @param pResult the PipelineResult object supplied in runProcess()
   * @param pKey the key to use to store the HashMap in the PipelineResult object
   * @param pId the key to use to store the error message within the HashMap in the
   *            PipelineResult object
   * @param pError the error object to store in the HashMap
   * @see atg.service.pipeline.PipelineResult
   * @see #runProcess(Object, PipelineResult)
   */
  protected void addHashedError(PipelineResult pResult, String pKey, String pId, Object pError)
  {
    Object error = pResult.getError(pKey);
    if (error == null) {
      Map map = new HashMap(TRUConstants.FIVE);
      pResult.addError(pKey, map);
      map.put(pId, pError);
    }
    else if (error instanceof Map) {
      Map map = (Map) error;
      map.put(pId, pError);
    }
  }

}
