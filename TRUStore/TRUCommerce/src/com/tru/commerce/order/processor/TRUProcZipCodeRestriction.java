package com.tru.commerce.order.processor;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import atg.commerce.order.PipelineConstants;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUShippingManager;

/**
 * 
 * @author PA
 * @version 1.0
 *
 */
public class TRUProcZipCodeRestriction extends ApplicationLoggingImpl implements
		PipelineProcessor {

	
	/**
	 * mShippingManager
	 */
	private TRUShippingManager mShippingManager;
	
	private final int mSUCCESS = TRUCommerceConstants.INT_ONE;


	/**
	 * @return ret
	 */
	public int[] getRetCodes() {
		int[] ret = { mSUCCESS };
		return ret;
	}

	
	/**
	 * getter for mShippingManager
	 * @return shippingManager
	 */
	public TRUShippingManager getShippingManager() {
		return mShippingManager;
	}

	/**
	 * setter for mShippingManager
	 * @param pShippingManager shippingManager
	 */
	public void setShippingManager(TRUShippingManager pShippingManager) {
		this.mShippingManager = pShippingManager;
	}
	
	/** The m enable zip code restriction. */
	private boolean mEnableZipCodeRestriction;
	
	
	/**
	 * Checks if is enable zip code restriction.
	 *
	 * @return true, if is enable zip code restriction
	 */
	public boolean isEnableZipCodeRestriction() {
		return mEnableZipCodeRestriction;
	}


	/**
	 * Sets the enable zip code restriction.
	 *
	 * @param pEnableZipCodeRestriction the new enable zip code restriction
	 */
	public void setEnableZipCodeRestriction(boolean pEnableZipCodeRestriction) {
		this.mEnableZipCodeRestriction = pEnableZipCodeRestriction;
	}


	/**
	 * @param pParam object
	 * @param pResult PipelineResult
	 * @return int
	 * @throws Exception Exception
	 */	
	public int runProcess(Object pParam, PipelineResult pResult) {
		if(isLoggingDebug()){
			vlogDebug("Start TRUProcZipCodeRestriction.runProcess()");
		}
		Map map = (HashMap) pParam;
		TRUOrderImpl order = (TRUOrderImpl) map.get(PipelineConstants.ORDER);
		Map<String,String>  validationMap=new HashMap<>();
		if(isEnableZipCodeRestriction()){
			validationMap=getShippingManager().validateZipCodeRestriction(order);
		}
		
		order.setShipRestrictionsFound(Boolean.FALSE);
		
		if(validationMap!=null && !(validationMap.isEmpty())){
			Iterator<String> iterator = validationMap.keySet().iterator();			
			while(iterator.hasNext()){
			  String key   = iterator.next();
			  String value = validationMap.get(key);
			  pResult.addError(key, value);
			}
			order.setShipRestrictionsFound(Boolean.TRUE);
		}
		
		if(isLoggingDebug()){
			vlogDebug("End TRUProcZipCodeRestriction.runProcess()");
		}
		return mSUCCESS;
	}
	
}
