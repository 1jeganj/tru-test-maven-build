package com.tru.commerce.order.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PipelineConstants;
import atg.commerce.order.processor.ProcAuthorizePayment;
import atg.commerce.states.PaymentGroupStates;
import atg.commerce.states.StateDefinitions;
import atg.core.util.ResourceUtils;
import atg.core.util.StringUtils;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUGiftCard;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.commerce.payment.TRUPaymentConstants;
import com.tru.commerce.payment.TRUPaymentManager;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;
import com.tru.integrations.sucn.common.TRUSUCNConstants;
import com.tru.integrations.sucn.coupon.TRUSUCNCouponService;
import com.tru.integrations.sucn.coupon.response.CouponResponse;

/**
 * Processor invokes from the pipeline 'ProcTRUReverseAuthorizePayment'. 
 * 
 * @author ORACLE
 * @version 1.0
 *
 */
public class ProcTRUReverseAuthorizePayment extends ProcAuthorizePayment {
	
	/**
	 * Success constant.
	 */
	private static final int ROLLBACK = TRUPipeLineConstants.NUMERIC_MINUS_ONE;
	/**
	 * Property to hold OrderResources.
	 */
	private static final String MY_RESOURCE_NAME = "atg.commerce.order.OrderResources";
	
	/** Resource Bundle. 
	 * 
	 */
	private static java.util.ResourceBundle sResourceBundle = java.util.ResourceBundle.getBundle(MY_RESOURCE_NAME, atg.service.dynamo.LangLicense
					.getLicensedDefault());
	
	/**
	 * Holds the reference for TRUOrderManager.
	 */
	private TRUOrderManager mOrderManager;
	
	/** Property to Hold Error handler manager. */
	private DefaultErrorHandlerManager mErrorHandlerManager;
	/**
	 * Holds return codes.
	 */
	private int[] mRetCodes = { ROLLBACK };
	
	/** The Purchase process helper. */
	private TRUSUCNCouponService mUseCouponService;
	
	/**
	 * @return the useCouponService
	 */
	public TRUSUCNCouponService getUseCouponService() {
		return mUseCouponService;
	}

	/**
	 * @param pUseCouponService the useCouponService to set
	 */
	public void setUseCouponService(TRUSUCNCouponService pUseCouponService) {
		mUseCouponService = pUseCouponService;
	}
	/**
	 * Gets the error handler manager.
	 * 
	 * @return the error handler manager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * Sets the error handler manager.
	 * 
	 * @param pErrorHandlerManager
	 *            the new error handler manager
	 */
	public void setErrorHandlerManager(DefaultErrorHandlerManager pErrorHandlerManager) {
		mErrorHandlerManager = pErrorHandlerManager;
	}
//	private static final String CLASS_NAME = "ProcTRUReverseAuthorizePayment";
	
	/**
	 * Returns the valid return codes:
	 * 1 - The processor completed.
	 * 
	 * @return an integer array of the valid return codes.
	 */
	@Override
	public int[] getRetCodes() {
	    return mRetCodes;
	}

	/**
	 * Process method to validate the gift card payment group using AJB integration service.
	 * @param pParam - the pParam
	 * @param pResult - the pPipelineResult
	 * @return int - the success/failure
	 * 
	 * @throws InvalidParameterException	-	if any.
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public int runProcess(Object pParam, PipelineResult pResult) throws InvalidParameterException {
		if (isLoggingDebug()) {
			vlogDebug("ProcReverseAuthorizePayment.runProcess()method.STARTS");
			vlogDebug("Method body");
			vlogDebug("ProcReverseAuthorizePayment.runProcess() method,pParam:{0} pResult:{1}",pParam, pResult);

		}
		Map map = (HashMap) pParam;
		Order order = (Order) map.get(PipelineConstants.ORDER);
		if (order == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(TRUPipeLineConstants.INVALID_ORDER_PARAMETER,
					MY_RESOURCE_NAME, sResourceBundle));
		}
		if (isLoggingDebug()) {
			vlogDebug("ProcReverseAuthorizePayment.runProcess() method Authorizing , OrderPaymentGroups:{0} ",order.getPaymentGroups());
		}

		if (isLoggingDebug()) {
			vlogDebug("order id : {0}",order.getId());
		}
		//START: Calling Void Single Use Coupon code service
		Map<String, String> couponData = null;
		String singleUseCoupon = null;
		if (order instanceof TRUOrderImpl) {
			couponData = ((TRUOrderImpl)order).getSingleUseCoupon();
			for (Map.Entry<String, String> coupon : couponData.entrySet()) {
				singleUseCoupon = coupon.getKey();
				if (StringUtils.isNotBlank(singleUseCoupon) && StringUtils.isNotBlank(coupon.getValue()) && !singleUseCoupon.equals(coupon.getValue())) {
					Map<String,String> voidCouponInputMap = new HashMap<String, String>();
					voidCouponInputMap.put(TRUSUCNConstants.SUCN_COUPON_PARAMETER_SINGLE_USE_COUPON_NUMBER, singleUseCoupon);
					voidCouponInputMap.put(TRUSUCNConstants.SUCN_COUPON_PARAMETER_ORDER_NUMBER, order.getId());
					voidCouponInputMap.put(TRUSUCNConstants.SUCN_COUPON_PARAMETER_VOID_BUSINIESS_DATE_STRING, TRUCheckoutConstants.EMPTY_STRING);
					CouponResponse voidCouponDetails = getUseCouponService().voidCouponDetails(voidCouponInputMap);
					if (isLoggingDebug()) {
						vlogDebug("ProcReverseAuthorizePayment.runProcess()method for Void coupon with Order id: {0} couponCode: {1} and CouponResponse: {2}", 
								order.getId(), singleUseCoupon, voidCouponDetails.getReturnCode());
					}
				}
			}
		}
		
		
		//END: Calling Void Single Use Coupon code service
		List<PaymentGroup> authorizePG = new ArrayList<PaymentGroup>();
		List<PaymentGroup> orderPaymentGroups = order.getPaymentGroups();
		int state = StateDefinitions.PAYMENTGROUPSTATES.getStateValue(PaymentGroupStates.AUTHORIZED);

		for (PaymentGroup paymentGroup : orderPaymentGroups) {
			if(paymentGroup.getState() == state && paymentGroup.getAmountAuthorized() >  TRUPaymentConstants.DOUBLE_INITIAL_VALUE ) {
				authorizePG.add(paymentGroup);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("ProcReverseAuthorizePayment.runProcess() : authorizePG list : {0}", authorizePG);
		}
		
		if (!authorizePG.isEmpty()) {
			for (PaymentGroup authorizePay : authorizePG) {
				if(authorizePay instanceof TRUGiftCard){
					((TRUGiftCard)authorizePay).setReversalAmount(authorizePay.getAmountAuthorized());
				}else if(authorizePay instanceof TRUCreditCard){
					((TRUCreditCard)authorizePay).setReversalAmount(authorizePay.getAmountAuthorized());
				}else if(authorizePay instanceof TRUPayPal){
					((TRUPayPal)authorizePay).setReversalAmount(authorizePay.getAmountAuthorized());
				}
				((TRUPaymentManager)getPaymentManager()).reverseAuthorize(order, authorizePG);
				//TODO commented the below API call, since we are not auditing
				/*try {
	        		if (!failedReverseAuth.isEmpty()) {
	        			if (isLoggingDebug()) {
	            			logDebug("ProcReverseAuthorizePayment.runProcess() method, Passing false to createOrUpdateFailedReverseAuthDetails method of Order Manager");
	            		}
	        			orderManager.createOrUpdateFailedReverseAuthDetails(order.getId(), false, failedReverseAuth);
	        		} else {
	        			if (isLoggingDebug()) {
	            			logDebug("ProcReverseAuthorizePayment.runProcess() method,Passing true to createOrUpdateFailedReverseAuthDetails method of Order Manager");
	            		}
	        			orderManager.createOrUpdateFailedReverseAuthDetails(order.getId(), true, failedReverseAuth);
	        		}
	        		if (isLoggingDebug()) {
	        			vlogDebug("ProcReverseAuthorizePayment.runProcess() method,failedReverseAuth:{0} Order Id:{1}",failedReverseAuth, order.getId());
	        		}
	        	} catch (RepositoryException reExp) {
	        		if (isLoggingError()) {
	        			vlogError(" Repository Exception occured at ProcReverseAuthorizePayment.runProcess ,RepositoryException:{0} OrderId:{1}",reExp,order.getId());
	        		}
	        	}*/
			}
			if (isLoggingDebug()) {
				vlogDebug("ProcReverseAuthorizePayment.runProcess()method.ENDS");
			}
		}
		return ROLLBACK;
	}

	/**
	 * To add the corresponding payment group error to the pipeline.
	 * 
	 * @param pFailedPaymentGroup
	 *            the payment group that failed to authorize
	 * @param pStatusMessage
	 *            message indicating why the payment group failed to authorize
	 * @param pResult
	 *            the pipeline result object.
	 * @param pBundle
	 *            resource bundle specific to users locale.
	 */
	public void addErrorToPipelineResult(PaymentGroup pFailedPaymentGroup,String pStatusMessage, PipelineResult pResult,ResourceBundle pBundle) {
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUAuthorizePayment.addErrorToPipelineResult()method.STARTS");
			vlogDebug("ProcTRUAuthorizePayment.addErrorToPipelineResult()method ,pFailedPaymentGroup:{0} pStatusMessage:{1} pResult:{2} pBundle:{3}",
					pFailedPaymentGroup, pStatusMessage, pResult, pBundle);
		}
		if (pFailedPaymentGroup instanceof TRUGiftCard) {
			addGiftCardError(pFailedPaymentGroup, pStatusMessage, pResult,pBundle);
		} else if (pFailedPaymentGroup instanceof TRUCreditCard || pFailedPaymentGroup instanceof TRUPayPal) {
			addCreditCardError(pFailedPaymentGroup, pStatusMessage, pResult,pBundle);
		} else {
			addPaymentGroupError(pFailedPaymentGroup, pStatusMessage, pResult,pBundle);
		}
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUAuthorizePayment.addErrorToPipelineResult()method.END");
		}
	}

	/**
	 * Adds authorization error messages to pipeline result. Note: it adds error
	 * messages only for authorization failures.
	 * 
	 * @param pFailedPaymentGroup
	 *            the payment group that failed to authorize
	 * @param pStatusMessage
	 *            message indicating why the payment group failed to authorize
	 * @param pResult
	 *            the pipeline result object.
	 * @param pBundle
	 *            resource bundle specific to users locale.
	 */
	protected void addGiftCardError(PaymentGroup pFailedPaymentGroup, String pStatusMessage, PipelineResult pResult,ResourceBundle pBundle) {
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUAuthorizePayment.addGiftCardError() method.STARTS");
			vlogDebug("ProcTRUAuthorizePayment.addGiftCardError()method ,pFailedPaymentGroup:{0} pStatusMessage:{1} pResult:{2} pBundle:{3}",
					pFailedPaymentGroup, pStatusMessage, pResult, pBundle);
		}
		String errorKey = null;
		if (pFailedPaymentGroup instanceof TRUGiftCard) {
			errorKey = TRUCheckoutConstants.GIFT_CARD;
		}
		pResult.addError(errorKey, pStatusMessage);
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUAuthorizePayment.addGiftCardError method, errorKey:{0} pStatusMessage:{1}",errorKey, pStatusMessage);
			vlogDebug("ProcTRUAuthorizePayment.addGiftCardError() method.ENDS");
		}
	}

	/**
	 * Adds authorization error messages to pipeline result. Note: it adds error
	 * messages only for authorization failures and NOT reversal failures.
	 * 
	 * @param pFailedPaymentGroup
	 *            the payment group that failed to authorize
	 * @param pStatusMessage
	 *            message indicating why the payment group failed to authorize
	 * @param pResult
	 *            the pipeline result object.
	 * @param pBundle
	 *            resource bundle specific to users locale.
	 */
	protected void addCreditCardError(PaymentGroup pFailedPaymentGroup,
			String pStatusMessage, PipelineResult pResult,
			ResourceBundle pBundle) {
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUAuthorizePayment.addCreditCardError() method.STARTS");
			vlogDebug("ProcTRUAuthorizePayment.addCreditCardError()method ,pFailedPaymentGroup:{0} pStatusMessage:{1} pResult:{2} pBundle:{3}",
					pFailedPaymentGroup, pStatusMessage, pResult, pBundle);
		}
		String errorKey = null;
		if (pFailedPaymentGroup instanceof TRUCreditCard) {
			errorKey = TRUCheckoutConstants.CREDIT_CARD;
		}
		
		if (pFailedPaymentGroup instanceof TRUPayPal) {
			errorKey = TRUCheckoutConstants.PAY_PAL;
		}
		
		pResult.addError(errorKey, pStatusMessage);
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUAuthorizePayment.addCreditCardError method, errorKey:{0} pStatusMessage:{1}",errorKey, pStatusMessage);
			vlogDebug("ProcTRUAuthorizePaymen.addCreditCardError()methods.ENDS");
		}
	}
	
	/**
	 * @return the orderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * @param pOrderManager
	 *            the orderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		this.mOrderManager = pOrderManager;
	}	
}
