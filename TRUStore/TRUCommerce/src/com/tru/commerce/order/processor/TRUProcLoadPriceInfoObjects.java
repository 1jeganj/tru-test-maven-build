package com.tru.commerce.order.processor;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import atg.beans.DynamicBeans;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.OrderTools;
import atg.commerce.order.processor.ProcLoadPriceInfoObjects;
import atg.repository.ItemDescriptorImpl;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryItemDescriptor;

import com.tru.radial.integration.taxware.TRUTaxwareConstant;
/**
 * This method extends OOTB ProcLoadPriceInfoObjects.
 * @author PA
 * @version 1.0
 */
public class TRUProcLoadPriceInfoObjects extends ProcLoadPriceInfoObjects {
	/**
	 * hold to property shipItemRelationsTaxPriceInfosProperty.
	 */
	private String mShipItemRelationsTaxPriceInfosProperty;
	
	/**
	 * @return the shipItemRelationsTaxPriceInfosProperty
	 */
	public String getShipItemRelationsTaxPriceInfosProperty() {
		return mShipItemRelationsTaxPriceInfosProperty;
	}
	/**
	 * @param pShipItemRelationsTaxPriceInfosProperty the shipItemRelationsTaxPriceInfosProperty to set
	 */
	public void setShipItemRelationsTaxPriceInfosProperty(
			String pShipItemRelationsTaxPriceInfosProperty) {
		mShipItemRelationsTaxPriceInfosProperty = pShipItemRelationsTaxPriceInfosProperty;
	}
	/** atg.commerce.order.processor.ProcLoadPriceInfoObjects#loadShippingItemsTaxPriceInfos(atg.commerce.order.Order, java.lang.Object, atg.repository.MutableRepositoryItem, atg.commerce.order.OrderManager, java.lang.Boolean).
	 * 
	 * @param  pOrder - Order
	 * @param pObj - Obj
	 * @param  pPiRepItem - PiRepItem
	 * @param pOrderManager - OrderManager
	 * @param pInvalidateCache - InvalidateCache
	 * @throws Exception - Exception
	 */ 
	
	protected void loadShippingItemsTaxPriceInfos(Order pOrder, Object pObj, MutableRepositoryItem pPiRepItem, OrderManager pOrderManager, Boolean pInvalidateCache)
			throws Exception  {
		OrderTools orderTools = pOrderManager.getOrderTools();
		Map itemMap = (Map)pPiRepItem.getPropertyValue(getShipItemRelationsTaxPriceInfosProperty());
		Map objMap = new HashMap(TRUTaxwareConstant.NUMERIC_SEVEN);
		DynamicBeans.setPropertyValue(pObj, getShipItemRelationsTaxPriceInfosProperty(), objMap);
		Iterator iter = itemMap.entrySet().iterator();
		while(iter.hasNext()) {
			Map.Entry entry = (Map.Entry)iter.next();
			String key = (String)entry.getKey();
			MutableRepositoryItem mutItem = (MutableRepositoryItem)entry.getValue();
			RepositoryItemDescriptor desc = mutItem.getItemDescriptor();
			if(pInvalidateCache.booleanValue()) {
				invalidateCache((ItemDescriptorImpl)desc, mutItem);
			}
			String className = orderTools.getMappedBeanName(desc.getItemDescriptorName());
			Object opi = Class.forName(className).newInstance();
			readProperties(pOrder, opi, getLoadProperties(), mutItem, desc, pOrderManager);
			loadPricingAdjustments(pOrder, opi, mutItem, pOrderManager, pInvalidateCache);
			objMap.put(key, opi);
		}
	}
}