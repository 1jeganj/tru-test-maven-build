package com.tru.commerce.order.processor;

import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.order.ChangedProperties;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PipelineConstants;
import atg.commerce.order.processor.OrderRepositoryUtils;
import atg.commerce.order.processor.SavedProperties;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.payment.PaymentStatus;
import atg.payment.creditcard.CreditCardStatus;
import atg.payment.giftcertificate.GiftCertificateStatus;
import atg.payment.storecredit.StoreCreditStatus;
import atg.repository.ConcurrentUpdateException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.service.dynamo.LangLicense;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This processor is used to update payment status objects in layaway order.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class ProcTRUUpdatePaymentStatusObjectsForLayaway extends SavedProperties implements PipelineProcessor {

	/**
	 * Property to hold OrderResources.
	 */
	static final String MY_RESOURCE_NAME = "atg.commerce.order.OrderResources";

	/**
	 * Property to hold resource bundle.
	 */
	private static ResourceBundle sResourceBundle = LayeredResourceBundle.getBundle(MY_RESOURCE_NAME, LangLicense.getLicensedDefault());

	/**
	 * To hold SUCCESS_CODE.
	 */
	private static final int SUCCESS_CODE = 1;
	
	/**
	 * To hold return code.
	 */
	private int[] mRetCodes = {SUCCESS_CODE};
	/**
	 * @return the metaInfoItemDescName
	 */
	public String getMetaInfoItemDescName() {
		return mMetaInfoItemDescName;
	}

	/**
	 * @param pMetaInfoItemDescName the metaInfoItemDescName to set
	 */
	public void setMetaInfoItemDescName(String pMetaInfoItemDescName) {
		mMetaInfoItemDescName = pMetaInfoItemDescName;
	}

	/** property to hold mMetaInfoItemDescName. */
	private String mMetaInfoItemDescName;
	
	/**
	 * This method is used to update payment status objects in layaway order to repository.
	 * @param pParam - Object
	 * @param pPipelineResult - PipelineResult
	 * @throws Exception - If any
	 * @return SUCCESS_CODE - Integer
	 */
	@SuppressWarnings("rawtypes")
	public int runProcess(Object pParam, PipelineResult pPipelineResult) throws Exception {
		if(isLoggingDebug()){
			vlogDebug("Start: ProcTRUUpdatePaymentStatusObjectsForLayaway (runProcess())");
		}
		Map map = (HashMap) pParam;
		TRUOrderManager orderManager = (TRUOrderManager) map.get(PipelineConstants.ORDERMANAGER);
		TRULayawayOrderImpl order = (TRULayawayOrderImpl) map.get(PipelineConstants.ORDER);
		Repository repository = (Repository) map.get(PipelineConstants.ORDERREPOSITORY);
		if (order == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(TRUPipeLineConstants.INVALID_ORDER_PARAMETER, MY_RESOURCE_NAME, sResourceBundle));
		}
		if (repository == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(TRUPipeLineConstants.INVALID_REPOSITORY_PARAM, MY_RESOURCE_NAME, sResourceBundle));
		}
		if (orderManager == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(TRUPipeLineConstants.INVALID_ORDER_MANAGER_PARAM, MY_RESOURCE_NAME, sResourceBundle));
		}
		TRUOrderTools orderTools = (TRUOrderTools) orderManager.getOrderTools();
		TRUPropertyManager propertyManager = orderManager.getPropertyManager();
		MutableRepository mutRep;
		MutableRepositoryItem mutItem = null;
		try{
			mutRep = (MutableRepository) repository;
			String[] statusProperties = {propertyManager.getAuthorizationStatusPropertyName(), propertyManager.getDebitStatusPropertyName(), propertyManager.getCreditStatusPropertyName()};
			MutableRepositoryItem groupMutItem = null;
			boolean changed = false;
			Iterator iter = order.getPaymentGroups().iterator();
			for (PaymentGroup group = null; iter.hasNext();) {
				group = (PaymentGroup) iter.next();
				groupMutItem = null;
				if (group instanceof ChangedProperties) {
					groupMutItem = ((ChangedProperties) group).getRepositoryItem();
				}
				if (groupMutItem == null) {
					setMetaInfoItemDescName( orderTools.getMappedItemDescriptorName(group.getClass().getName()));
					groupMutItem = mutRep.getItemForUpdate(group.getId(),getMetaInfoItemDescName());
					if (group instanceof ChangedProperties) {
						((ChangedProperties) group).setRepositoryItem(groupMutItem);
					}
				}
				for (int i = 0; i < statusProperties.length; ++i) {
					List mutItemList = (List) groupMutItem.getPropertyValue(statusProperties[i]);
					List statList = (List) DynamicBeans.getPropertyValue(group, statusProperties[i]);
					if (statList.isEmpty()) {
						mutItemList.clear();
						changed = true;
					} else {
						if (ensureList(mutItemList, statList, mutRep, orderManager)) {
							changed = true;
						}
						int j = 0;
						for (Iterator iter2 = statList.iterator(); iter2.hasNext(); ++j) {
							Object paymentStatus = iter2.next();
							mutItem = (MutableRepositoryItem) mutItemList.get(j);
							if (saveStatusProperties(order, paymentStatus, mutItem, mutRep, orderTools)) {
								changed = true;
							}
							if ((!(order.isTransient())) && (mutItem.isTransient())) {
								if (isLoggingDebug()){
									logDebug("add to repository[" + mutItem.getItemDescriptor().getItemDescriptorName() + ":" + mutItem.getRepositoryId() + "]");
								}
								mutRep.addItem(mutItem);
								changed = true;
							}
							mutRep.updateItem(mutItem);
							if(changed) {
								setChangedToTrue(map);
							}
						}
					}
				}
			}
		} catch (ClassCastException ccExp) {
			if(isLoggingError()){
				vlogError("Exception occured in ProcTRUUpdatePaymentStatusObjectsForLayaway: runProcess(): {0}", ccExp);
			}
			String[] msgArgs = {order.getId(), mutItem.getItemDescriptor().getItemDescriptorName()};
			throw new InvalidParameterException(ResourceUtils.getMsgResource(TRUPipeLineConstants.INVALID_REPOSITORY_PARAM,
					MY_RESOURCE_NAME, sResourceBundle, msgArgs), ccExp);
		} catch (ConcurrentUpdateException cuExp) {
			if(isLoggingError()){
				vlogError("Exception occured in ProcTRUUpdatePaymentStatusObjectsForLayaway: runProcess(): {0}", cuExp);
			}
			String[] msgArgs = {order.getId(), mutItem.getItemDescriptor().getItemDescriptorName()};
			throw new CommerceException(ResourceUtils.getMsgResource(TRUCommerceConstants.SHOPPINGCART_CONCURRENT_UPDATE_ATTEMPT,
							MY_RESOURCE_NAME, sResourceBundle, msgArgs), cuExp);
		}
		return SUCCESS_CODE;
	}
	
	/**
	 * This method sets the changed property in the map to true.
	 * @param pMap Map
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void setChangedToTrue(Map pMap){
		pMap.put(PipelineConstants.CHANGED, Boolean.TRUE);
		if (isLoggingDebug()) {
			logDebug("Set changed flag to true in ProcTRUUpdatePaymentStatusObjectsForLayaway");
		}
	}

	/**
	 * This method saves payment status properties.
	 * @param pOrder Order
	 * @param pPaymentStatus Object
	 * @param pMutItem MutableRepositoryItem
	 * @param pMutRep MutableRepository
	 * @param pOrderTools OrderTools
	 * @return changed boolean
	 * @throws IntrospectionException - IntrospectionException
	 * @throws RepositoryException - RepositoryException
	 */
	protected boolean saveStatusProperties(Order pOrder, Object pPaymentStatus, MutableRepositoryItem pMutItem, MutableRepository pMutRep, OrderTools pOrderTools) throws IntrospectionException, RepositoryException
	   
	  {
	    Object[] savedProperties = getSavedProperties();
	    boolean changed = false;
	    for (int i = 0; i < savedProperties.length; ++i) {
	      String mappedPropName = getMappedPropertyName((String)savedProperties[i]);
	      if (!(OrderRepositoryUtils.hasProperty(pOrder, pPaymentStatus, mappedPropName))){
	        continue;
	      }
	      Object value;
	      try {
	        value = OrderRepositoryUtils.getPropertyValue(pOrder, pPaymentStatus, mappedPropName);
	      }
	      catch (PropertyNotFoundException exp) {
	    	  if(isLoggingError()){
	    		  vlogError("Property not found in repository: {0}", exp);
	    	  }
	    	  changed = true;
	    	  break;
	      }

	      if (isLoggingDebug()) {
	        logDebug("save property[" + ((String)savedProperties[i]) + ":" + value + ":" + pPaymentStatus.getClass().getName() + "]");
	      }
	      OrderRepositoryUtils.saveRepositoryItem(pMutRep, pMutItem, (String)savedProperties[i], value, pOrderTools);
	      changed = true;
	    }

	    return changed;
	  }

	/**
	 * This method ensures list.
	 * @param pMutItemList List
	 * @param pStatList List
	 * @param pMutRep MutableRepository
	 * @param pOrderManager TRUOrderManager
	 * @return boolean boolean
	 * @throws CommerceException CommerceException
	 * @throws RepositoryException RepositoryException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected boolean ensureList(List pMutItemList, List pStatList, MutableRepository pMutRep, TRUOrderManager pOrderManager)
			throws CommerceException, RepositoryException {
		if ((pStatList.isEmpty()) && (pMutItemList.isEmpty())) {
			return false;
		}
		TRUOrderTools orderTools = (TRUOrderTools) pOrderManager.getOrderTools();
		TRUPropertyManager propertyManager = pOrderManager.getPropertyManager();
		Map map = new HashMap(TRUCommerceConstants.DIGIT_EIGHT);
		Iterator iter = pMutItemList.iterator();
		while (iter.hasNext()) {
			MutableRepositoryItem mutItem = (MutableRepositoryItem) iter.next();
			String descName = mutItem.getItemDescriptor().getItemDescriptorName();
			List list = (List) map.get(descName);
			if (list == null) {
				list = new ArrayList(TRUCommerceConstants.DIGIT_THREE);
				map.put(descName, list);
			}
			list.add(mutItem);
		}
		pMutItemList.clear();
		int i = 0;
		for (int size = pStatList.size(); i < size; ++i) {
			PaymentStatus ps = (PaymentStatus) pStatList.get(i);
			String descName = getPaymentStatusDescriptorName(ps, orderTools, propertyManager);
			List list = (List) map.get(descName);
			MutableRepositoryItem mutItem;
			if ((list == null) || (list.isEmpty())) {
				mutItem = pMutRep.createItem(descName);
				if (isLoggingDebug()){
					logDebug("create in repository[" + mutItem.getItemDescriptor().getItemDescriptorName() + ":" + mutItem.getRepositoryId() + "]");
				}
			} else {
				mutItem = (MutableRepositoryItem) list.remove(0);
			}
			pMutItemList.add(i, mutItem);
		}
		return true;
	}

	/**
	 * This method return item descriptor name for the payment status instance.
	 * @param pPaymentStatus PaymentStatus
	 * @param pOrderTools TRUOrderTools
	 * @param pPropertyManager TRUPropertyManager
	 * @return statusDecsriptorName String
	 * @throws CommerceException CommerceException
	 */
	@SuppressWarnings("rawtypes")
	protected String getPaymentStatusDescriptorName(PaymentStatus pPaymentStatus, TRUOrderTools pOrderTools, TRUPropertyManager pPropertyManager)
			throws CommerceException {
		boolean hasClass = true;
		Class clazz = pPaymentStatus.getClass();
		String descName = pOrderTools.getBeanNameToItemDescriptorMap().getProperty(clazz.getName());
		while (descName == null) {
			clazz = clazz.getSuperclass();
			if (clazz == null) {
				hasClass = false;
				break;
			}
			descName = pOrderTools.getBeanNameToItemDescriptorMap().getProperty(clazz.getName());
		}
		if (!(hasClass)) {
			descName = getStatusDescriptorName(pPaymentStatus, pPropertyManager);
			if (descName == null) {
				throw new CommerceException(ResourceUtils.getMsgResource(TRUPipeLineConstants.INVALID_PAYMENT_STATUS_CLASS_TYPE,
						MY_RESOURCE_NAME, sResourceBundle));
			}
		}
		return descName;
	}

	/**
	 * This method return item descriptor name for the payment status instance.
	 * @param pStatusObject PaymentStatus
	 * @param pPropertyManager TRUPropertyManager
	 * @return statusDecsriptorName String
	 * @throws CommerceException CommerceException
	 */
	protected String getStatusDescriptorName(PaymentStatus pStatusObject, TRUPropertyManager pPropertyManager) throws CommerceException {
		if (pStatusObject instanceof CreditCardStatus) {
			return pPropertyManager.getCreditCardStatusPropertyName();
		}
		if (pStatusObject instanceof GiftCertificateStatus) {
			return pPropertyManager.getGiftCertificateStatusPropertyName();
		}
		if (pStatusObject instanceof StoreCreditStatus) {
			return pPropertyManager.getStoreCreditStatusPropertyName();
		}
		if (pStatusObject instanceof PaymentStatus) {
			return pPropertyManager.getPaymentStatusDescriptorName();
		}
		return null;
	}
	
	/**
	 * Return the list of possible return values from this processor.
	 * This processor always returns a single value indicating success.
	 * In case of errors, it adds messages to the pipeline result object.
	 *
	 * @return the list of possible return values
	 */
	public int[] getRetCodes() {
		if(isLoggingDebug()){
			vlogDebug("START : ProcTRUUpdatePaymentStatusObjectsForLayaway (getRetCodes()) : BEGIN and END");
		}
		return mRetCodes;
	}
}
