package com.tru.commerce.order.processor;

import java.util.HashMap;
import java.util.Map;

import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.commerce.order.purchase.TRUPurchaseProcessHelper;
import com.tru.common.TRUUserSession;

/**
 * Implemented for Intraday items check.
 * The Class ProcTRUCheckForDiscontinuedProducts.
 *  @author Professional access
 *  @version 1.0
 */
public class ProcTRUCheckForDiscontinuedProducts extends ApplicationLoggingImpl
implements PipelineProcessor {
	/**
	 * Resource bundle name.
	 */
	private static final String MY_RESOURCE_NAME = "atg.commerce.order.OrderResources";
	/** The Constant MSG_INVALID_ORDER_PARAMETER. 
	 */
	public static final String MSG_INVALID_ORDER_PARAMETER = "InvalidOrderParameter";
	/** The Constant SUCCESS. 
	 * 
	 */
	private static final int SUCCESS = 1;
	/** 
	 * Resource Bundle. 
	 */
	private static java.util.ResourceBundle sResourceBundle = 
			LayeredResourceBundle.getBundle(MY_RESOURCE_NAME, atg.service.dynamo.LangLicense.getLicensedDefault());
	/** The Purchase process helper. */
	private TRUPurchaseProcessHelper mPurchaseProcessHelper;

	
	/**
	 * Gets the purchase process helper.
	 *
	 * @return the purchaseProcessHelper
	 */
	public TRUPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}

	/**
	 * Sets the purchase process helper.
	 *
	 * @param pPurchaseProcessHelper the purchaseProcessHelper to set
	 */
	public void setPurchaseProcessHelper(
			TRUPurchaseProcessHelper pPurchaseProcessHelper) {
		mPurchaseProcessHelper = pPurchaseProcessHelper;
	}

	/**
	 * Returns the valid return codes:
	 * 1 - The processor completed.
	 * @return an integer array of the valid return codes.
	 */
	public int[] getRetCodes() {
		int[] ret = { SUCCESS };

		return ret;
	}

	/**
	 * Overriden Run process for Intraday items check.
	 *
	 * @param pParamObject a HashMap which must contain an Order and optionally a Locale object.
	 * @param pParamPipelineResult a PipelineResult object which stores any information which must
	 *                be returned from this method invocation.
	 *                
	 * @return an integer specifying the processor's return code.
	 * 
	 * @throws Exception - when it catch
	 * 
	 * @see atg.service.pipeline.PipelineProcessor#runProcess(Object, PipelineResult)
	 * @see atg.commerce.pricing.AmountInfo
	 * @see atg.commerce.pricing.OrderPriceInfo
	 * @see atg.commerce.pricing.TaxPriceInfo
	 * @see atg.commerce.pricing.ItemPriceInfo
	 * @see atg.commerce.pricing.ShippingPriceInfo
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public int runProcess(Object pParamObject,
			PipelineResult pParamPipelineResult) throws Exception {
		if(isLoggingDebug()){
			vlogDebug("ProTRUCheckForDiscontinuedProducts.runProcess()methods.STARTS");
		}
		Map map = (HashMap) pParamObject;
		Order order = (Order) map.get(PipelineConstants.ORDER);
		TRUUserSession session = (TRUUserSession) map.get(TRUPipeLineConstants.SESSION_COMPONENT);
		if (order == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(MSG_INVALID_ORDER_PARAMETER, 
					MY_RESOURCE_NAME, sResourceBundle));
		}
		//Custom validation
		getPurchaseProcessHelper().checkForDiscontinuedProducts(order, session);
		if (isLoggingDebug()) {
			vlogDebug("ProTRUCheckForDiscontinuedProducts.runProcess()methods.ENDS Orderid : {0}",order.getId());
		}
		return TRUCheckoutConstants.INT_ONE ;
	}
}
