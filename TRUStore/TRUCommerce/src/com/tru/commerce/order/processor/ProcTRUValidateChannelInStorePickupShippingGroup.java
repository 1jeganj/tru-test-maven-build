package com.tru.commerce.order.processor;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.processor.ProcValidateInStorePickupShippingGroup;
import atg.core.util.StringUtils;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUChannelInStorePickupShippingGroup;
import com.tru.common.cml.SystemException;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;

/**
 * This class validates an Channel in-store shipping group.
 * 
 * @author Professional access
 * @version 1.0
 */
public class ProcTRUValidateChannelInStorePickupShippingGroup extends ProcValidateInStorePickupShippingGroup {

	/** Property to Hold Error handler manager. 
	 */
	private DefaultErrorHandlerManager mErrorHandlerManager;

	/**
	 * Gets the error handler manager.
	 * 
	 * @return the error handler manager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * Sets the error handler manager.
	 * 
	 * @param pErrorHandlerManager
	 *            the new error handler manager
	 */
	public void setErrorHandlerManager(
			DefaultErrorHandlerManager pErrorHandlerManager) {
		mErrorHandlerManager = pErrorHandlerManager;
	}

	/**
	 * 
	 * @param pShippingGroup
	 *            InStorePickupShippingGroup.
	 * @param pResult
	 *            PipelineResult.
	 * @param pResourceBundle
	 *            returns if Servlet exception
	 * @throws Exception
	 *             returns if exception
	 * 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void validateOnlineOnly(InStorePickupShippingGroup pShippingGroup, PipelineResult pResult,ResourceBundle pResourceBundle) throws Exception {
		super.validateOnlineOnly(pShippingGroup, pResult, pResourceBundle);
		TRUChannelInStorePickupShippingGroup channelInStorePickupShippingGroup = (TRUChannelInStorePickupShippingGroup) pShippingGroup;
		String channelId = channelInStorePickupShippingGroup.getChannelId();
		String channelType = channelInStorePickupShippingGroup.getChannelType();
		String channelUserName = channelInStorePickupShippingGroup.getChannelUserName();
		Map errorsMap = (Map) pResult.getError(pShippingGroup.getId());
		String error = null;
		if (errorsMap != null) {
			errorsMap = new HashMap();
			errorsMap.put(pShippingGroup.getId(), errorsMap);
		}
		try {
			if (StringUtils.isBlank(channelId)) {
				error = getErrorHandlerManager().getErrorMessage(TRUCheckoutConstants.ERROR_CHANNELID_EMPTY);
				errorsMap.put(TRUCheckoutConstants.CHANNEL_ID, error);
			}
			if (StringUtils.isBlank(channelType)) {
				error = getErrorHandlerManager().getErrorMessage(TRUCheckoutConstants.ERROR_CHANNELTYPE_EMPTY);
				errorsMap.put(TRUCheckoutConstants.CHANNEL_TYPE, error);
			}
			if (StringUtils.isBlank(channelUserName)) {
				error = getErrorHandlerManager().getErrorMessage(TRUCheckoutConstants.ERROR_CHANNELUSERNAME_EMPTY);
				errorsMap.put(TRUCheckoutConstants.USER_NAME, error);
			}
		} catch (SystemException systemExp) {
			if (isLoggingError()) {
				vlogError("SystemException at TRUProcValidateHardgoodShippingGroup.validateHardgoodShippingGroupFields :{0}",systemExp);
			}
		}
	}
}
