package com.tru.commerce.order.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import atg.commerce.order.InStorePayment;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PipelineConstants;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.TRUCommerceConstants;

/**
 * This class.
 * 
 * @author PA
 * @version 1.0
 */
public class ProcTRUValidateOnlyInStoreForCheckout extends
		ApplicationLoggingImpl implements PipelineProcessor {
	/**
	 * This property holds RESOURCE_NAME.
	 */
	static final String RESOURCE_NAME = "atg.commerce.order.OrderResources";
	/**
	 * This property holds USER_MSGS_RES_NAME.
	 */
	static final String USER_MSGS_RES_NAME = "atg.commerce.order.UserMessages";

	/**
	 * This property holds ResourceBundle.
	 */
	private static ResourceBundle sResourceBundle = LayeredResourceBundle
			.getBundle(RESOURCE_NAME,
					atg.service.dynamo.LangLicense.getLicensedDefault());
	/**
	 * This property holds UserResourceBundle.
	 */
	private static ResourceBundle sUserResourceBundle = LayeredResourceBundle
			.getBundle(USER_MSGS_RES_NAME, java.util.Locale.getDefault());
	/**
	 * This property holds mEnabled.
	 */
	private boolean mEnabled;

	/**
	 * This property holds LoggingIdentifier.
	 */
	private String mLoggingIdentifier = TRUCommerceConstants.VALIDATE_INSTORE_CHECKOUT;
	/**
	 * Returns the set of valid return codes for this processor. This processor
	 * always returns a value of 1, indicating successful completion. If any
	 * errors occur during validation, they are added to the pipeline result
	 * object where the caller can examine them.
	 **/

	private final int mSUCCESS = TRUCommerceConstants.INT_ONE;
	/**
	 * This property holds Return Codes.
	 */
	private int[] mRetCodes = { mSUCCESS };

	/**
	 * Gets the enabled parameter.
	 * 
	 * @return the mEnabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets the enabled parameter.
	 * 
	 * @param pEnabled
	 *            the pEnabled to set
	 */
	public void setEnabled(boolean pEnabled) {
		this.mEnabled = pEnabled;
	}
	/**
	 * Set the LoggingIdentifier used in log messages.
	 * 
	 * @param pLoggingIdentifier
	 *            the pLoggingIdentifier to set
	 */
	public void setLoggingIdentifier(String pLoggingIdentifier) {
		mLoggingIdentifier = pLoggingIdentifier;
	}

	/**
	 * Return the LoggingIdentifier used in log messages.
	 * 
	 * @return the mLoggingIdentifier
	 */
	public String getLoggingIdentifier() {
		return mLoggingIdentifier;
	}

	/**
	 * Return the RetCodes.
	 * 
	 * @return the mRetCodes
	 */
	public int[] getRetCodes() {
		return mRetCodes;
	}

	/**
	 * Verify that all required shipping adddress properties are provided in a
	 * HardgoodShippingGroup.
	 * @see atg.commerce.order.processor.ShippingAddrValidator#validateShippingAddress
	 *      ShippingAddrValidator
	 * @return mSUCCESS
	 * 			succes
	 * @param pParam
	 *            the param
	 * @param pResult
	 *            the result
	 * @throws InvalidParameterException -  InvalidParameterException
	 * 
	 */
	public int runProcess(Object pParam, PipelineResult pResult) throws InvalidParameterException
			  {
		if (!(isEnabled())) {
			return TRUCommerceConstants.INT_ONE;
		}

		@SuppressWarnings("rawtypes")
		Map map = (HashMap) pParam;
		Order order = (Order) map.get(PipelineConstants.ORDER);
		// OrderManager om =
		// (OrderManager)map.get(PipelineConstants.ORDERMANAGER);
		Locale locale = (Locale) map.get(PipelineConstants.LOCALE);
		@SuppressWarnings("unused")
		ResourceBundle resourceBundle;
		if (locale == null) {
			resourceBundle = sUserResourceBundle;
		} else {
			resourceBundle = LayeredResourceBundle.getBundle(
					USER_MSGS_RES_NAME, locale);
		}
		if (order == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(
					TRUCommerceConstants.INVALID_ORDER_PARAMETER,
					RESOURCE_NAME, sResourceBundle));
		}

		boolean anyInStorePayment = false;
		boolean anyOtherPayment = false;

		List<PaymentGroup> pgs = order.getPaymentGroups();
		for (PaymentGroup pg : pgs) {
			if (pg instanceof InStorePayment) {
				anyInStorePayment = true;
				break;
			}
		}

		for (PaymentGroup pg : pgs) {
			if (!(pg instanceof InStorePayment)) {
				anyOtherPayment = true;
				break;
			}
		}

		if (anyInStorePayment && anyOtherPayment) {
			String msg = sResourceBundle.getString(TRUCommerceConstants.INVALID_INSTORE_PAYMENTUSE);
			pResult.addError(TRUCommerceConstants.INVALID_INSTORE_PAYMENTUSE, msg);
			return STOP_CHAIN_EXECUTION_AND_ROLLBACK;
		}

		return mSUCCESS;
	}

	/**
	 * This method adds an error to the PipelineResult object. This method,
	 * rather than just storing a single error object in pResult, stores a Map
	 * of errors. This allows more than one error to be stored using the same
	 * key in the pResult object. pKey is used to reference a HashMap of errors
	 * in pResult. So, calling pResult.getError(pKey) will return an object
	 * which should be cast to a Map. Each entry within the map is keyed by pId
	 * and its value is pError.
	 * 
	 * @param pResult
	 *            the PipelineResult object supplied in runProcess()
	 * @param pKey
	 *            the key to use to store the HashMap in the PipelineResult
	 *            object
	 * @param pId
	 *            the key to use to store the error message within the HashMap
	 *            in the PipelineResult object
	 * @param pError
	 *            the error object to store in the HashMap
	 * @see atg.service.pipeline.PipelineResult
	 * @see #runProcess(Object, PipelineResult)
	 */
	protected void addHashedError(PipelineResult pResult, String pKey,
			String pId, Object pError) {
		Object error = pResult.getError(pKey);
		if (error == null) {
			Map map = new HashMap(TRUCommerceConstants.INT_FIVE);
			pResult.addError(pKey, map);
			map.put(pId, pError);
		} else if (error instanceof Map) {
			Map map = (Map) error;
			map.put(pId, pError);
		}
	}
}
