/**
 * 
 */
package com.tru.commerce.order.processor;

import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.pipeline.PipelineManager;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.common.TRUConstants;

/**
 * The Class TRUExecuteShipValidations.
 * 
 * @author PA
 */
public class TRUExecuteShipValidations extends ApplicationLoggingImpl implements
		PipelineProcessor {

	/** The m pipeline manager. */
	private PipelineManager mPipelineManager = null;

	/**
	 * Gets the pipeline manager.
	 * 
	 * @return the pipeline manager
	 */
	public PipelineManager getPipelineManager() {
		return this.mPipelineManager;
	}

	/**
	 * Sets the pipeline manager.
	 * 
	 * @param pPipelineManager
	 *            the new pipeline manager
	 */
	public void setPipelineManager(PipelineManager pPipelineManager) {
		this.mPipelineManager = pPipelineManager;
	}

	/** The m chain to run. */
	private String mChainToRun = null;

	/**
	 * Gets the chain to run.
	 * 
	 * @return the chain to run
	 */
	public String getChainToRun() {
		return this.mChainToRun;
	}

	/**
	 * Sets the chain to run.
	 * 
	 * @param pChainToRun
	 *            the new chain to run
	 */
	public void setChainToRun(String pChainToRun) {
		this.mChainToRun = pChainToRun;
	}

	/** The sucess. */
	private static final int SUCESS = TRUConstants.ONE;
	/** The failure. */
	private static final int FAILURE = TRUConstants.INT_MINUS_ONE;

	/* (non-Javadoc)
	 * @see atg.service.pipeline.PipelineProcessor#getRetCodes()
	 */
	@Override
	public int[] getRetCodes() {
		int[] ret = { SUCESS, FAILURE };
		return ret;
	}

	/**
     *
     * @param pParam - Param
     * @param pResult - Result
     * @return SUCESS - Success
     * @exception Exception throws any exception back to the caller
     * @see atg.service.pipeline.PipelineProcessor#runProcess(Object, PipelineResult)
     */
	public int runProcess(Object pParam, PipelineResult pResult)
			throws Exception {
		if (isLoggingDebug()) {
			logDebug("Executing chain " + getChainToRun());
		}
		PipelineResult result = getPipelineManager().runProcess(
				getChainToRun(), pParam);

		if (result.hasErrors()) {
			if (isLoggingDebug()) {
				logDebug("Found error objects in pipeline result -- returning value: "
						+ FAILURE);
				logDebug("Errors: " + result.getErrors());
			}
			pResult.copyInto(result);
			return FAILURE;

		}

		if (isLoggingDebug()){
			logDebug("No error objects in pipeline result -- returning value: "
					+ SUCESS);
		}
		return SUCESS;
	}

}
