package com.tru.commerce.order.processor;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUShippingManager;
import com.tru.commerce.order.TRUShippingTools;

/**
 * 
 * @author PA
 * @version 1.0
 *
 */
public class TRUProcShipMethodRestriction extends ApplicationLoggingImpl implements
		PipelineProcessor {
	/**
	 * holds ShippingTools.
	 */
	private TRUShippingTools mShippingTools;
	/**
	 * holds ShippingManager.
	 */
	private TRUShippingManager mShippingManager;
	
	/**
	 * success code.
	 */
	private final int mSUCCESS = TRUCommerceConstants.INT_ONE;

	/**
	 * @return ret
	 */
	public int[] getRetCodes() {
		int[] ret = { TRUCheckoutConstants.INT_ONE };
		return ret;
	}

	/**
	 * ShippingTools.
	 * @return shippingTools
	 */
	public TRUShippingTools getShippingTools() {
		return mShippingTools;
	}

	/**
	 * 
	 * @param pShippingTools the shippingTools to set
	 */
	public void setShippingTools(TRUShippingTools pShippingTools) {
		this.mShippingTools = pShippingTools;
	}

	/**
	 * 
	 * @return shippingManager
	 */
	public TRUShippingManager getShippingManager() {
		return mShippingManager;
	}

	/**
	 * 
	 * @param pShippingManager the shippingManager to set
	 */
	public void setShippingManager(TRUShippingManager pShippingManager) {
		this.mShippingManager = pShippingManager;
	}

	/**
	 * Run process.
	 *
	 * @param pParam param
	 * @param pResult result
	 * @return int
	 */
	public int runProcess(Object pParam, PipelineResult pResult) {
		if(isLoggingDebug()){
			vlogDebug("Start TRUProcShipMethodRestriction.runProcess()");
		}
		Map map = (HashMap) pParam;
		Order order = (Order) map.get(PipelineConstants.ORDER);	
		Map<String,String>  validationMap=getShippingManager().validateShipMethodRestrction(order);			
		
		if(isLoggingDebug()){
			logDebug("PipelineResult\t"+validationMap);
		}
		if(validationMap!=null && (!validationMap.isEmpty())){
			Iterator<String> iterator = validationMap.keySet().iterator();			
			while(iterator.hasNext()){
				String key   = iterator.next();
				String value = validationMap.get(key);
			  pResult.addError(key, value);			 			
			}
		}
		if(isLoggingDebug()){
			vlogDebug("End TRUProcShipMethodRestriction.runProcess()");
		}
		return mSUCCESS;
	}

}
