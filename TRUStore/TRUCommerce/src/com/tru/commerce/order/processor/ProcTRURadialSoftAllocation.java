package com.tru.commerce.order.processor;
import java.util.HashMap;
import java.util.Map;

import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.PipelineConstants;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.common.TRUIntegrationConfiguration;
import com.tru.common.TRUSOSIntegrationConfig;
import com.tru.payment.radial.TRURadialPaymentProcessor;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialIntegrationException;
import com.tru.radial.integration.inventory.RadialInventoryService;
import com.tru.radial.integration.inventory.bean.RadialInventoryRequest;


/**
 * <p>
 *   Determine if the item is available by ensuring that at least 1 is available for pre-order, 
 *   back-order or immediate availability.
 * </p>
 * <p>
 *   This does not check the quantity being purchased with the levels available and the threshold 
 *   levels. The OMS will do that since it has more up to the minute inventory data. The order will 
 *   be accepted as long as the inventory data shows that it can be at least partially fulfilled.
 * </p>
 * 
 * @author Professional access
 * @version 1.0
 */
public class ProcTRURadialSoftAllocation extends ApplicationLoggingImpl implements 
PipelineProcessor {

	/**
	 * Resource bundle name.
	 */
	private static final String MY_RESOURCE_NAME = "atg.commerce.order.OrderResources";

	/**
	 * InvalidOrderParameter.
	 */
	private  static final String MSG_INVALID_ORDER_PARAMETER = "InvalidOrderParameter";
	
	/** The mSosIntegrationConfiguration. */
	private TRUSOSIntegrationConfig mSosIntegrationConfiguration;
	
	/** The m site context mSiteContextManager. */
	private SiteContextManager mSiteContextManager;

	/**
	 * @return the sosIntegrationConfiguration
	 */
	public TRUSOSIntegrationConfig getSosIntegrationConfiguration() {
		return mSosIntegrationConfiguration;
	}

	/**
	 * @param pSosIntegrationConfiguration the sosIntegrationConfiguration to set
	 */
	public void setSosIntegrationConfiguration(
			TRUSOSIntegrationConfig pSosIntegrationConfiguration) {
		mSosIntegrationConfiguration = pSosIntegrationConfiguration;
	}

	/**
	 * @return the siteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * @param pSiteContextManager the siteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}

	/** 
	 * Resource Bundle. 
	 */
	private static java.util.ResourceBundle sResourceBundle = 
			LayeredResourceBundle.getBundle(MY_RESOURCE_NAME, atg.service.dynamo.LangLicense.getLicensedDefault());

	/**
	 * Success constant.
	 */
	private static final int SUCCESS = 1;
	
	private RadialInventoryService mRadialInventoryService;
	
	/**
	 * @return the radialInventoryService
	 */
	public RadialInventoryService getRadialInventoryService() {
		return mRadialInventoryService;
	}

	/**
	 * @param pRadialInventoryService the radialInventoryService to set
	 */
	public void setRadialInventoryService(
			RadialInventoryService pRadialInventoryService) {
		mRadialInventoryService = pRadialInventoryService;
	}

	/** The Radial payment processor. */
	private TRURadialPaymentProcessor mRadialPaymentProcessor;
	/**
	 * Holds the property value of mOrderManager.
	 */
	private OrderManager mOrderManager;
	
	
	/**
	 * hold boolean property integrationConfig.
	 */
	private TRUIntegrationConfiguration mIntegrationConfig;

	/**
	 * @return the integrationConfig
	 */
	public TRUIntegrationConfiguration getIntegrationConfig() {
		return mIntegrationConfig;
	}

	/**
	 * @param pIntegrationConfig the integrationConfig to set
	 */
	public void setIntegrationConfig(TRUIntegrationConfiguration pIntegrationConfig) {
		mIntegrationConfig = pIntegrationConfig;
	}
	/**
	 * This method is used to get orderManager.
	 * @return orderManager OrderManager
	 */
	public OrderManager getOrderManager() {
		return mOrderManager;
	}
	
	/**
	 *This method is used to set orderManager.
	 *@param pOrderManager OrderManager
	 */
	
	public void setOrderManager(OrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}
	/**
	 * @return the radialPaymentProcessor
	 */
	public TRURadialPaymentProcessor getRadialPaymentProcessor() {
		return mRadialPaymentProcessor;
	}

	/**
	 * @param pRadialPaymentProcessor the radialPaymentProcessor to set
	 */
	public void setRadialPaymentProcessor(
			TRURadialPaymentProcessor pRadialPaymentProcessor) {
		mRadialPaymentProcessor = pRadialPaymentProcessor;
	}
	/**  Holds reference for TRUCommercePropertyManager. */
	private TRUCommercePropertyManager mCommercePropertyManager;
	/**
	 * Returns the commercePropertyManager.
	 *
	 * @return the commercePropertyManager
	 */
	public TRUCommercePropertyManager getCommercePropertyManager() {
		return mCommercePropertyManager;
	}

	/**
	 * Sets/updates the commercePropertyManager.
	 *
	 * @param pCommercePropertyManager            the commercePropertyManager to set
	 */
	public void setCommercePropertyManager(TRUCommercePropertyManager pCommercePropertyManager) {
		mCommercePropertyManager = pCommercePropertyManager;
	}
	/**
	 * Validates that there is inventory for all items in the order.
	 *
	 * @param pParam a HashMap which must contain an Order and optionally a Locale object.
	 * @param pResult a PipelineResult object which stores any information which must
	 *                be returned from this method invocation.
	 *                
	 * @return an integer specifying the processor's return code.
	 * @throws Exception Exception
	 * @see atg.service.pipeline.PipelineProcessor#runProcess(Object, PipelineResult)
	 * @see atg.commerce.pricing.AmountInfo
	 * @see atg.commerce.pricing.OrderPriceInfo
	 * @see atg.commerce.pricing.TaxPriceInfo
	 * @see atg.commerce.pricing.ItemPriceInfo
	 * @see atg.commerce.pricing.ShippingPriceInfo
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public int runProcess(Object pParam, PipelineResult pResult)
			throws Exception {
		Map map = (HashMap) pParam;
		Order order = (Order) map.get(PipelineConstants.ORDER);
		boolean lEnabled = Boolean.FALSE;
		if (order == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(
					MSG_INVALID_ORDER_PARAMETER, MY_RESOURCE_NAME,
					sResourceBundle));
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::ProcTRURadialSoftAllocation::@method::runProcess() : BEGIN");
		}
		if(SiteContextManager.getCurrentSite() != null){
			Site site=SiteContextManager.getCurrentSite();
			if(site != null && site.getId()==TRUCommerceConstants.SOS){
				lEnabled = getSosIntegrationConfiguration().isEnableRadialSoftAllocation();
				if (isLoggingDebug()) {
					vlogDebug("@Class::ProcTRURadialSoftAllocation : RadialSoftAllocation service is  enabled in SOS : {0} ",lEnabled);
				}
			}else{
				lEnabled = getIntegrationConfig().isEnableRadialSoftAllocation();
				if (isLoggingDebug()) {
					vlogDebug("@Class::ProcTRURadialSoftAllocation : Radial SoftAllocation service is  enabled in store : {0} ",lEnabled);
				}
			}
			TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
			TRUCreditCard orderCreditCard = (TRUCreditCard) orderManager.getCreditCard((TRUOrderImpl)order);
			if(orderCreditCard != null){
			//Boolean isRetryAccept = orderCreditCard.getPaymentRetryAccept();
			String responseCode = orderCreditCard.getResponseCode();
				if( responseCode != null && 
						getRadialPaymentProcessor().getPaymentTools().getSoftAllocationNotAllowedList().
						contains(responseCode.toLowerCase())){
					return SUCCESS;
				}
			}
			if(lEnabled){
				RadialInventoryRequest softAllocationsRequest = getRadialPaymentProcessor().constructRadialSoftAllocationsRequest(order);
				if(softAllocationsRequest != null && !softAllocationsRequest.getAllocationRequest().isEmpty()) {
					try{
					IRadialResponse radialSoftAllocations = getRadialInventoryService().radialSoftAllocations(softAllocationsRequest);
					if(radialSoftAllocations != null) {
						getRadialPaymentProcessor().updateRadialSoftAllocationsResponse(order, radialSoftAllocations);
					}
					}catch(TRURadialIntegrationException radIntExp){
						vlogError("TRURadialIntegrationException at ProcTRURadialSoftAllocation\runProcess",radIntExp);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::ProcTRURadialSoftAllocation::@method::runProcess() : END");
		}
		return SUCCESS;
	}

	/**
	 * Returns the valid return codes:
	 * 1 - The processor completed.
	 * 
	 * @return an integer array of the valid return codes.
	 */
	public int[] getRetCodes() {
		int[] ret = {SUCCESS};
		return ret;
	}
}