package com.tru.commerce.order.processor;
import java.util.HashMap;
import java.util.Map;

import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.commerce.order.purchase.TRUPurchaseProcessHelper;


/**
 * <p>
 *   Determine if the item is available by ensuring that at least 1 is available for pre-order, 
 *   back-order or immediate availability.
 * </p>
 * <p>
 *   This does not check the quantity being purchased with the levels available and the threshold 
 *   levels. The OMS will do that since it has more up to the minute inventory data. The order will 
 *   be accepted as long as the inventory data shows that it can be at least partially fulfilled.
 * </p>
 * 
 * @author Professional access
 * @version 1.0
 */
public class ProcTRUValidateInventoryForCheckout extends ApplicationLoggingImpl implements 
PipelineProcessor {

	/**
	 * Resource bundle name.
	 */
	private static final String MY_RESOURCE_NAME = "atg.commerce.order.OrderResources";


	/**
	 * InvalidOrderParameter.
	 */
	private  static final String MSG_INVALID_ORDER_PARAMETER = "InvalidOrderParameter";

	/** 
	 * Resource Bundle. 
	 */
	private static java.util.ResourceBundle sResourceBundle = 
			LayeredResourceBundle.getBundle(MY_RESOURCE_NAME, atg.service.dynamo.LangLicense.getLicensedDefault());

	/**
	 * Success constant.
	 */
	private static final int SUCCESS = 1;
	
	
	/** The Purchase process helper.
	 */
	private TRUPurchaseProcessHelper mPurchaseProcessHelper;

	/**
	 * @return the purchaseProcessHelper
	 */
	public TRUPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}
	/**
	 * @param pPurchaseProcessHelper the purchaseProcessHelper to set
	 */
	public void setPurchaseProcessHelper(
			TRUPurchaseProcessHelper pPurchaseProcessHelper) {
		mPurchaseProcessHelper = pPurchaseProcessHelper;
	}


	/**
	 * Validates that there is inventory for all items in the order.
	 *
	 * @param pParam a HashMap which must contain an Order and optionally a Locale object.
	 * @param pResult a PipelineResult object which stores any information which must
	 *                be returned from this method invocation.
	 *                
	 * @return an integer specifying the processor's return code.
	 * @throws Exception Exception
	 * @see atg.service.pipeline.PipelineProcessor#runProcess(Object, PipelineResult)
	 * @see atg.commerce.pricing.AmountInfo
	 * @see atg.commerce.pricing.OrderPriceInfo
	 * @see atg.commerce.pricing.TaxPriceInfo
	 * @see atg.commerce.pricing.ItemPriceInfo
	 * @see atg.commerce.pricing.ShippingPriceInfo
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public int runProcess(Object pParam, PipelineResult pResult)
			throws Exception {
		Map map = (HashMap) pParam;
		String chainId = null;
		if (null != map.get(TRUPipeLineConstants.CHAIN_ID)){
			chainId = (String) map.get(TRUPipeLineConstants.CHAIN_ID);
		}
		
		Order order = (Order) map.get(PipelineConstants.ORDER);
		if (order == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(
					MSG_INVALID_ORDER_PARAMETER, MY_RESOURCE_NAME,
					sResourceBundle));
		}

		// Custom validation
		getPurchaseProcessHelper().validateInventoryForCheckout(order, chainId,map);
		return STOP_CHAIN_EXECUTION;
	}

	/**
	 * Returns the valid return codes:
	 * 1 - The processor completed.
	 * 
	 * @return an integer array of the valid return codes.
	 */
	public int[] getRetCodes() {
		int[] ret = {SUCCESS};
		return ret;
	}
}
