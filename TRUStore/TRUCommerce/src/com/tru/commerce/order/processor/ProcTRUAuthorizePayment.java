package com.tru.commerce.order.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import atg.commerce.CommerceException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupNotFoundException;
import atg.commerce.order.PipelineConstants;
import atg.commerce.order.processor.ProcAuthorizePayment;
import atg.commerce.states.PaymentGroupStates;
import atg.commerce.states.StateDefinitions;
import atg.core.util.ResourceUtils;
import atg.core.util.StringUtils;
import atg.payment.PaymentStatus;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUGiftCard;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardStatus;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardStatus;
import com.tru.commerce.payment.TRUPaymentConstants;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.commerce.payment.paypal.TRUPayPalStatus;
import com.tru.common.cml.SystemException;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;

/**
 * This method extends OOTB ApplicationLoggingImpl.
 * @author ORACLE
 * @version 1.0
 */
public class ProcTRUAuthorizePayment extends ProcAuthorizePayment {

	/**
	 * Property to hold success value.
	 */
	private static final int SUCCESS = 1;

	/**
	 * Property to hold REVERSE_AUTHORIZE value.
	 */
	private static final int REVERSE_AUTHORIZE = 2;
	/**
	 * Property to hold OrderResources.
	 */
	private static final String MY_RESOURCE_NAME = "atg.commerce.order.OrderResources";
	/**
	 *  Resource Bundle.
	 */
	private static java.util.ResourceBundle sResourceBundle = java.util.ResourceBundle.getBundle(MY_RESOURCE_NAME, atg.service.dynamo.LangLicense
					.getLicensedDefault());
	/**
	 * This property holds mErrorMessage.
	 */
	private String mErrorMessage;
	/**
	 * Holds return codes.
	 */
	private int[] mRetCodes = { SUCCESS, REVERSE_AUTHORIZE };
	
	/**
	 * Holds the reference for TRUOrderManager.
	 */
	private TRUOrderManager mOrderManager;
	
	/** Property to Hold Error handler manager. 
	 * 
	 */
	
	private DefaultErrorHandlerManager mErrorHandlerManager;
	
	/**
	 * Gets the error handler manager.
	 * 
	 * @return the error handler manager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * Sets the error handler manager.
	 * 
	 * @param pErrorHandlerManager
	 *            the new error handler manager
	 */
	public void setErrorHandlerManager(DefaultErrorHandlerManager pErrorHandlerManager) {
		mErrorHandlerManager = pErrorHandlerManager;
	}
	

	/**
	 * Returns the set of valid return codes for this processor. This processor always returns a value of 1, indicating successful
	 * completion. If any errors occur during validation, they are added to the pipeline result object where the caller can examine them.
	 * 
	 * @return the list of possible return values
	 */
	public int[] getRetCodes() {
		return mRetCodes;
	}


	/**
	 * Gets the ErrorMessage.
	 * 
	 * @return the mErrorMessage
	 */
	public String getErrorMessage() {
		return mErrorMessage;
	}

	/**
	 * Sets the ErrorMessage.
	 * 
	 * @param pErrorMessage
	 *            the mErrorMessage to set
	 */
	public void setErrorMessage(String pErrorMessage) {
		this.mErrorMessage = pErrorMessage;
	}

	/**
	 * Defines interface method.
	 * 
	 * @param pResult
	 *            -- result object
	 * @param pParam
	 *            -- param object
	 * @return int, indicating success or failure.
	 * 
	 * @throws InvalidParameterException
	 *             - if any
	 */
	@SuppressWarnings( { "unchecked" })
	public int runProcess(Object pParam, PipelineResult pResult) throws InvalidParameterException {
			vlogDebug("ProcTRUAuthorizePayment.runProcess()method.STARTS");
			vlogDebug("Method body");
			vlogDebug("ProcTRUAuthorizePayment.runProcess() method,pParam:{0} pResult:{1}",pParam, pResult);
		Map map = (HashMap) pParam;
		List<String> failedList = null;
		Order order = (Order) map.get(PipelineConstants.ORDER);
		if (order == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(TRUPipeLineConstants.INVALID_ORDER_PARAMETER,
					MY_RESOURCE_NAME, sResourceBundle));
		}
	    if(map.containsKey(PipelineConstants.BYPASSPAYMENTAUTHORIZATIONS))
	    {  
	        vlogDebug("Bypassing PG authorization" );
	      return SUCCESS;
	    }
			vlogDebug("ProcTRUAuthorizePayment.runProcess() method Authorizing , OrderPaymentGroups:{0} OrderId:{1} ",order.getPaymentGroups(),order.getId());
		try {
			List paymentGroups = order.getPaymentGroups();
			if (paymentGroups != null && !paymentGroups.isEmpty()) {
					List<PaymentGroup> listOfNotAuthorizePaymentGroups = fetchNotAuthorizedPGs(order);
				if(!listOfNotAuthorizePaymentGroups.isEmpty()){
					//Calling the authorize method of payment manager.
					failedList = getPaymentManager().authorize(order, listOfNotAuthorizePaymentGroups);
				}
			}
		} catch (CommerceException commerceException) {
			if (isLoggingError()) {
				vlogError("CommerceException inside ProcTRUAuthorizePayment.runProcess() ,OrderId:{0} profileId:{1} commerceException:{2}  ",order.getId(),order.getProfileId(),commerceException);
			}
		}
		if (failedList != null && !failedList.isEmpty()) {
				vlogDebug("The following payment groups failed authorization, failedList:{0} ",failedList);
			// for each failed group, generate a message
			for (String failedPgId : failedList) {
				try {
					PaymentGroup pg = order.getPaymentGroup(failedPgId);
					List<PaymentStatus> listPS = (List<PaymentStatus>)pg.getAuthorizationStatus();
					if(listPS != null && !listPS.isEmpty()){
						for(PaymentStatus ps :listPS ){
								vlogDebug(" Error Message :{0}",ps.getErrorMessage());
							if(!ps.getTransactionSuccess()){
							 addErrorToPipelineResult(pg, getErrorHandlerManager().getErrorMessage(ps.getErrorMessage()),pResult, sResourceBundle,ps.getErrorMessage());
							 break;
							}
							 if(ps instanceof TRUPayPalStatus && ((TRUPayPalStatus)ps).getTransactionSuccess()){
									if(((TRUPayPalStatus)ps).getErrorMessage() != null){
										addErrorToPipelineResult(pg, getErrorHandlerManager().getErrorMessage(
												((TRUPayPalStatus)ps).getErrorMessage()),pResult, sResourceBundle);
									}
									break;
							}
							 if(ps instanceof TRUCreditCardStatus && ((TRUCreditCardStatus)ps).getTransactionSuccess()){
									if(((TRUCreditCardStatus)ps).getErrorMessage() != null) {
										addErrorToPipelineResult(pg, getErrorHandlerManager().getErrorMessage(
												((TRUCreditCardStatus)ps).getErrorMessage()),pResult, sResourceBundle);
									} 
									break;
							}
							 if(ps instanceof TRUGiftCardStatus && !((TRUGiftCardStatus)ps).getTransactionSuccess()){
									if(((TRUGiftCardStatus)ps).getErrorMessage() != null) {
										addErrorToPipelineResult(pg, getErrorHandlerManager().getErrorMessage(
												((TRUGiftCardStatus)ps).getErrorMessage()),pResult, sResourceBundle);
									} 
									break;
							} 
						}
					}
				} catch (PaymentGroupNotFoundException pgExp) {
						vlogError("PaymentGroupNotFoundException at ProcTRUAuthorizePayment.runProcess method,OrderId:{0} profileId:{1} PaymentGroupNotFoundException:{2}",order.getId(),order.getProfileId(),pgExp);
				} catch (SystemException sysExp) {
						vlogError("SystemException at ProcTRUAuthorizePayment.runProcess method OrderId:{0} profileId:{1} PaymentGroupNotFoundException:{2}",order.getId(),order.getProfileId(),sysExp);
				}
			}
		} 
		if (pResult.hasErrors()) {
			if (isLoggingError()) {
				vlogError("*******process order pipeline throwing errors of ProcTRUAuthorizePayment.runProcess method,ORDERID:{0} ProfileId:{1}",order.getId(),order.getProfileId());
			}
			// If any errors exists in the authorization process, invoke adjust InventoryReservation
			// by returning 2,so that adjust Inventory Reservation will start.
				return REVERSE_AUTHORIZE;
		}
		return SUCCESS;
	}

	

	/**
	 * To add the corresponding payment group error to the pipeline.
	 *
	 * @param pFailedPaymentGroup            the payment group that failed to authorize
	 * @param pStatusMessage            message indicating why the payment group failed to authorize
	 * @param pResult            the pipeline result object.
	 * @param pBundle            resource bundle specific to users locale.
	 * @param pErrorKey the error key
	 */
	public void addErrorToPipelineResult(PaymentGroup pFailedPaymentGroup,String pStatusMessage, PipelineResult pResult,ResourceBundle pBundle,String pErrorKey) {
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUAuthorizePayment.addErrorToPipelineResult()method.STARTS");
			vlogDebug("ProcTRUAuthorizePayment.addErrorToPipelineResult()method ,pFailedPaymentGroup:{0} pStatusMessage:{1} pResult:{2} pBundle:{3}",
					pFailedPaymentGroup, pStatusMessage, pResult, pBundle);
		}
		if (pFailedPaymentGroup instanceof TRUGiftCard) {
			addGiftCardError(pFailedPaymentGroup, pStatusMessage, pResult,pBundle,pErrorKey);
		} else if (pFailedPaymentGroup instanceof TRUCreditCard || pFailedPaymentGroup instanceof TRUPayPal) {
			addCreditCardError(pFailedPaymentGroup, pStatusMessage, pResult,pBundle,pErrorKey);
		} else {
			addPaymentGroupError(pFailedPaymentGroup, pStatusMessage, pResult,pBundle);
		}
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUAuthorizePayment.addErrorToPipelineResult()method.END");
		}
	}

	

	/**
	 * Adds authorization error messages to pipeline result. Note: it adds error
	 * messages only for authorization failures.
	 *
	 * @param pFailedPaymentGroup            the payment group that failed to authorize
	 * @param pStatusMessage            message indicating why the payment group failed to authorize
	 * @param pResult            the pipeline result object.
	 * @param pBundle            resource bundle specific to users locale.
	 * @param pErrorKey the error key
	 */
	protected void addGiftCardError(PaymentGroup pFailedPaymentGroup, String pStatusMessage, PipelineResult pResult,ResourceBundle pBundle,String pErrorKey) {
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUAuthorizePayment.addGiftCardError() method.STARTS");
			vlogDebug("ProcTRUAuthorizePayment.addGiftCardError()method ,pFailedPaymentGroup:{0} pStatusMessage:{1} pResult:{2} pBundle:{3}",
					pFailedPaymentGroup, pStatusMessage, pResult, pBundle);
		}
		String errorKey = null;
		if (pFailedPaymentGroup instanceof TRUGiftCard) {
			errorKey = pErrorKey;
		}
		pResult.addError(errorKey, pStatusMessage);
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUAuthorizePayment.addGiftCardError method, errorKey:{0} pStatusMessage:{1}",errorKey, pStatusMessage);
			vlogDebug("ProcTRUAuthorizePayment.addGiftCardError() method.ENDS");
		}
	}

	/**
	 * Adds authorization error messages to pipeline result. Note: it adds error
	 * messages only for authorization failures and NOT reversal failures.
	 *
	 * @param pFailedPaymentGroup            the payment group that failed to authorize
	 * @param pStatusMessage            message indicating why the payment group failed to authorize
	 * @param pResult            the pipeline result object.
	 * @param pBundle            resource bundle specific to users locale.
	 * @param pErrorKey the error key
	 */
	protected void addCreditCardError(PaymentGroup pFailedPaymentGroup,
			String pStatusMessage, PipelineResult pResult,
			ResourceBundle pBundle,String pErrorKey) {
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUAuthorizePayment.addCreditCardError() method.STARTS");
			vlogDebug("ProcTRUAuthorizePayment.addCreditCardError()method ,pFailedPaymentGroup:{0} pStatusMessage:{1} pResult:{2} pBundle:{3}",
					pFailedPaymentGroup, pStatusMessage, pResult, pBundle);
		}
		String errorKey = null;
		if (pFailedPaymentGroup instanceof TRUCreditCard) {
			errorKey = pErrorKey;
		}
		
		if (pFailedPaymentGroup instanceof TRUPayPal) {
			errorKey = pErrorKey;
		}
		
		pResult.addError(errorKey, pStatusMessage);
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUAuthorizePayment.addCreditCardError method, errorKey:{0} pStatusMessage:{1}",errorKey, pStatusMessage);
			vlogDebug("ProcTRUAuthorizePaymen.addCreditCardError()methods.ENDS");
		}
	}
	
	
	/**
	 * Fetch the list of non authorized payment groups.
	 * 
	 * @param pOrder - Order
	 * @return List
	 */
	private List<PaymentGroup> fetchNotAuthorizedPGs(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUAuthorizePayment.fetchNotAuthorizedPGs()method.Start");
		}
		List<PaymentGroup> listOfNonAuthorizedPgs = new ArrayList<PaymentGroup>();
		PaymentGroupStates paymentGroupStates = StateDefinitions.PAYMENTGROUPSTATES;
		List<PaymentGroup> listOfPaymentGroups = pOrder.getPaymentGroups();
		for (PaymentGroup pg : listOfPaymentGroups) {
			int state = pg.getState();
			if (isLoggingDebug()) {
				vlogDebug("ProcTRUAuthorizePayment.fetchNotAuthorizedPGs() method processed variables, creditCardPG:{0} state:{1} paymentGroupStates:{2}",
						pg, state, paymentGroupStates);
			}
			String stateString = paymentGroupStates.getStateString(state);
			if (isLoggingDebug()) {
				vlogDebug("ProcTRUAuthorizePayment.fetchNotAuthorizedPGs() method processed variables, stateString:{0}",stateString);
			}
			if (!StringUtils.isBlank(stateString) && !stateString.equalsIgnoreCase(TRUPaymentConstants.AUTHORIZE_STATE_VALUE)) {
				listOfNonAuthorizedPgs.add(pg);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUAuthorizePayment.fetchNotAuthorizedPGs()method.End");
		}
		return listOfNonAuthorizedPgs;
	}
	
	/**
	 * @return the orderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * @param pOrderManager
	 *            the orderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		this.mOrderManager = pOrderManager;
	}	
}