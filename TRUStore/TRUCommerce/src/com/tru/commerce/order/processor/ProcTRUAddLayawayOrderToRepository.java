package com.tru.commerce.order.processor;

import java.util.HashMap;
import java.util.Map;

import atg.commerce.CommerceException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.PipelineConstants;
import atg.core.util.ResourceUtils;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUPipeLineConstants;

/**
 * This processor is used to persist layaway order to repository.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class ProcTRUAddLayawayOrderToRepository extends ApplicationLoggingImpl
		implements PipelineProcessor {

	/**
	 * Property to hold OrderResources.
	 */
	static final String MY_RESOURCE_NAME = "atg.commerce.order.OrderResources";
	
	/** Resource Bundle. 
	 */
	private static java.util.ResourceBundle sResourceBundle = java.util.ResourceBundle.getBundle(MY_RESOURCE_NAME, atg.service.dynamo.LangLicense.getLicensedDefault());
	
	/**
	 * To hold SUCCESS_CODE.
	 */
	private static final int SUCCESS_CODE = 1;

	/**
	 * To hold return code.
	 */
	private int[] mRetCodes = {SUCCESS_CODE};
	
	/**
	 * This method is used to persist layaway order to repository.
	 * @param pParam - Object
	 * @param pPipelineResult - PipelineResult
	 * @throws InvalidParameterException - If any
	 * @return SUCCESS_CODE - Integer
	 */
	@SuppressWarnings("rawtypes")
	public int runProcess(Object pParam, PipelineResult pPipelineResult) throws InvalidParameterException {
		if(isLoggingDebug()){
			vlogDebug("Start: ProcTRUAddLayawayOrderToRepository (runProcess())");
		}
		
		Map map = (HashMap)pParam;
	    TRUOrderManager mgr = (TRUOrderManager)map.get(PipelineConstants.ORDERMANAGER);
	    TRULayawayOrderImpl order = (TRULayawayOrderImpl)map.get(PipelineConstants.ORDER);

	    if (order == null) {
	    	throw new InvalidParameterException(ResourceUtils.getMsgResource(TRUPipeLineConstants.INVALID_ORDER_PARAMETER,
					MY_RESOURCE_NAME, sResourceBundle));
	    }
	    if (mgr == null) {
	      throw new InvalidParameterException(ResourceUtils.getMsgResource(TRUPipeLineConstants.INVALID_ORDER_MANAGER_PARAM,
					MY_RESOURCE_NAME, sResourceBundle));
	    }

	    try {
			mgr.addLayawayOrder(order);
		} catch (CommerceException e) {
			if (isLoggingError()) {
				vlogError("Exception occured in ProcTRUAddLayawayOrderToRepository while adding layaway order to repository {0}", order.getId());
			}
		}
	    
		if(isLoggingDebug()){
			vlogDebug("END : ProcTRUAddLayawayOrderToRepository (runProcess())");
		}
		
		return SUCCESS_CODE;
	}
	
	/**
	 * Return the list of possible return values from this processor.
	 * This processor always returns a single value indicating success.
	 * In case of errors, it adds messages to the pipeline result object.
	 *
	 * @return the list of possible return values
	 */
	public int[] getRetCodes() {
		if(isLoggingDebug()){
			vlogDebug("START : ProcValidateGiftCardPG (getRetCodes()) : BEGIN and END");
		}
		return mRetCodes;
	}

}