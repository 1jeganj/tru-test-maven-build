package com.tru.commerce.order.processor;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUShippingManager;

/**
 * This class  will validate Freight restriction.
 * @author PA
 * @version 1.0
 *
 */
public class TRUProcFreightRestriction extends ApplicationLoggingImpl implements
		PipelineProcessor {
	
	/**
	 * holds TRUShippingManager.
	 */
	private TRUShippingManager mShippingManager;
	
	/*private static ResourceBundle sUserResourceBundle = LayeredResourceBundle
			.getBundle("atg.commerce.order.UserMessages", Locale.getDefault());*/
	
	/**
	 * success code.
	 */
	private final int mSUCCESS = TRUCommerceConstants.INT_ONE;

	/**
	 * @return ret
	 */
	public int[] getRetCodes() {
		int[] ret = { mSUCCESS };
		return ret;
	}

	
	/**
	 * 
	 * @return shippingManager
	 */
	public TRUShippingManager getShippingManager() {
		return mShippingManager;
	}

	/**
	 * 
	 * @param pShippingManager the shippingManager to set
	 */
	public void setShippingManager(TRUShippingManager pShippingManager) {
		this.mShippingManager = pShippingManager;
	}

	/**
	 * @param pParam param
	 * @param pResult result
	 * @return int
	 * @throws Exception Exception
	 */
	public int runProcess(Object pParam, PipelineResult pResult) throws Exception {
		if(isLoggingDebug()){
			logDebug("Start TRUProcFreightRestriction.runProcess()");
		}
		Map map = (HashMap) pParam;
		Order order = (Order) map.get(PipelineConstants.ORDER);		
		Map<String,String>  validationMap=getShippingManager().validateFreightRestrction(order);
		
		if(isLoggingDebug()){
			vlogDebug("PipelineResult {0}", validationMap);
		}
		if(validationMap!=null && (!validationMap.isEmpty())){
			Iterator<String> iterator = validationMap.keySet().iterator();			
			while(iterator.hasNext()){
				String key   = iterator.next();
				String value = validationMap.get(key);
			  pResult.addError(key, value);			 			
			}
		}
		if(isLoggingDebug()){
			logDebug("End TRUProcFreightRestriction.runProcess()");
		}
		return TRUCheckoutConstants.INT_ONE;
	}

}
