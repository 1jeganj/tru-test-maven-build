package com.tru.commerce.order.processor;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.processor.ValidatePaymentGroupPipelineArgs;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.core.util.StringUtils;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUGiftCard;
import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardInfo;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardStatus;
import com.tru.commerce.payment.TRUPaymentManager;
import com.tru.commerce.pricing.TRUOrderPriceInfo;
import com.tru.common.cml.SystemException;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;

/**
 * Validate a single instance of the TRUGiftCardPG payment group at checkout
 * time.  This processor verifies that all required fields in a Gift Card
 * are valid.
 * 
 * @author PA
 * @version 1.0
 */
public class ProcTRUValidateGiftCardPG extends ApplicationLoggingImpl implements PipelineProcessor {

	/**
	 * To hold SUCCESS_CODE.
	 */
	private static final int  SUCCESS_CODE = 1;

	/**
	 * To hold SUCCESS_CODE.
	 */
	private int[] mRetCodes = {SUCCESS_CODE};

	/**
	 * Holds the PaymentManager.
	 */
	private TRUPaymentManager mPaymentManager;
	
	private boolean mValidationEnabled;

	/**
	 * @return the validationEnabled
	 */
	public boolean isValidationEnabled() {
		return mValidationEnabled;
	}


	/**
	 * @param pValidationEnabled the validationEnabled to set
	 */
	public void setValidationEnabled(boolean pValidationEnabled) {
		mValidationEnabled = pValidationEnabled;
	}
	/**
	 * Getting the PaymentManager.
	 * @return the PaymentManager
	 */
	public TRUPaymentManager getPaymentManager() {
		return mPaymentManager;
	}

	/**
	 * Setting the value for the mPaymentManager.
	 * @param pPaymentManager
	 * 			- the PaymentManager
	 */
	public void setPaymentManager(TRUPaymentManager pPaymentManager) {
		this.mPaymentManager = pPaymentManager;
	}
	
	/** Property to Hold Error handler manager. 
	 * 
	 */
	
	private DefaultErrorHandlerManager mErrorHandlerManager;
	
	/**
	 * Gets the error handler manager.
	 * 
	 * @return the error handler manager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * Sets the error handler manager.
	 * 
	 * @param pErrorHandlerManager
	 *            the new error handler manager
	 */
	public void setErrorHandlerManager(DefaultErrorHandlerManager pErrorHandlerManager) {
		mErrorHandlerManager = pErrorHandlerManager;
	}

	
	/**
	 * Process method to validate the gift card payment group using AJB integration service.
	 * @param pParam - the pParam
	 * @param pPipelineResult - the pPipelineResult
	 * @return int - the success/failure
	 * 
	 * @throws InvalidParameterException	-	if any.
	 */
	public int runProcess(Object pParam, PipelineResult pPipelineResult) throws InvalidParameterException {
		if(isLoggingDebug()){
			vlogDebug("Start : ProcValidateGiftCardPG (runProcess())");
			vlogDebug("pParam :  {0} , pPipelineResult :  {1}" , pParam ,pPipelineResult);
		}
		ValidatePaymentGroupPipelineArgs args = (ValidatePaymentGroupPipelineArgs)pParam;
		Locale locale = args.getLocale();
		PaymentGroup paymentGroup = args.getPaymentGroup();
		if(isLoggingDebug()){
			vlogDebug("paymentGroup {0}", paymentGroup);
		}
		ResourceBundle resourceBundle;
		if (locale == null) {
			resourceBundle = null;
		} else {
			resourceBundle = LayeredResourceBundle.getBundle(TRUPipeLineConstants.USER_MESSAGES_BUNDLE, locale);
		}
		if (paymentGroup == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(TRUPipeLineConstants.INVALID_PG_PARAM, TRUPipeLineConstants.USER_MESSAGES_BUNDLE, resourceBundle));
		}
		if(isValidationEnabled()){
			String errorString = TRUCheckoutConstants.EMPTY_STRING;
			TRUGiftCardStatus giftCardStatus = null;
	        Order order = (Order)args.getOrder();
	        try {
	        	TRUGiftCard giftCard = (TRUGiftCard) paymentGroup;
	        	giftCardStatus = giftCardStatus(pPipelineResult, giftCard, order);
	        	if(!giftCardStatus.getTransactionSuccess()) {
	        		if(!StringUtils.isBlank(giftCardStatus.getErrorMessage())) 
	        		{
	        			addHashedError(pPipelineResult, TRUCheckoutConstants.GIFT_CARD_ERR, giftCard.getId(), giftCardStatus.getErrorMessage());
	        		}
	        	}
	        	else {
	        		double gcBalance = TRUCheckoutConstants.MINIMUM_PRICE;
	        		gcBalance = giftCardStatus.getBalanceAmount().doubleValue();
	        		if(isLoggingDebug()){
	        			vlogDebug("gcBalance {0}", gcBalance);
	        		}
	        		if (order instanceof TRUOrderImpl) {
	        			TRUOrderPriceInfo orderPriceInfo = (TRUOrderPriceInfo) order.getPriceInfo();
	        			double orderPriceInfoTotal = getPaymentManager().getPricingTools().round(orderPriceInfo.getTotal());
	        			if(isLoggingDebug()){
	        				vlogDebug("l_oOrderPriceInfo.getTotal() {0},orderPriceInfoTotal {1} , l_oOrder.getPaymentGroupCount() {2}", 
	        						orderPriceInfo.getTotal(),orderPriceInfoTotal, order.getPaymentGroupCount());
	        			}
	        			if (order.getPaymentGroupCount() == TRUCheckoutConstants.INT_ONE && gcBalance < orderPriceInfoTotal) {
	        				errorString = getErrorHandlerManager().getErrorMessage(TRUCheckoutConstants.GIFT_CARD_BALANCE);
	        				addHashedError(pPipelineResult, TRUCheckoutConstants.GIFT_CARD_ERR, giftCard.getId(), errorString);
	        				return STOP_CHAIN_EXECUTION_AND_COMMIT;
	        			} else if (gcBalance < giftCard.getAmount()) {
	        				errorString = getErrorHandlerManager().getErrorMessage(TRUCheckoutConstants.INSUFF_GIFT_CARD_BALANCE);
	        				addHashedError(pPipelineResult, TRUCheckoutConstants.GIFT_CARD_ERR, giftCard.getId(), errorString);
	        				return STOP_CHAIN_EXECUTION_AND_COMMIT;
	        			}
	        		}
	        		if (order instanceof TRULayawayOrderImpl) {
	        			return pipeLineStatus(pPipelineResult, order, giftCard, gcBalance);
	        		}
	        	}
	        } 
	        catch (ClassCastException | SystemException ss ) {
					vlogError("Expected a GiftCard payment group got this instead in ProcTRUValidateGiftCardPG.runProcess():" + paymentGroup.getClass().getName(), ss);
				
	        	pPipelineResult.addError(TRUCheckoutConstants.CLASS_NOT_RECOGNIZED, TRUCheckoutConstants.EXPECTED_GIFT_CARD +
	            		paymentGroup.getClass().getName() + TRUCheckoutConstants.INSTEAD);
	        } 
		}
		if(isLoggingDebug()){
			vlogDebug("END : ProcValidateGiftCardPG (runProcess())");
		}
		return SUCCESS_CODE;
	}

	/**
	 * @param pPipelineResult - pipe line result
	 * @param pOrder  - order
	 * @param pGiftCard -gift card
	 * @param pGcBalance - gc balance
	 * @return SUCCESS_CODE
	 * @throws SystemException - SystemException
	 * 
	 */
	private int pipeLineStatus(PipelineResult pPipelineResult, Order pOrder,
			TRUGiftCard pGiftCard, double pGcBalance) throws SystemException {
		if(isLoggingDebug()){
			vlogDebug("Start : pipeLineStatus method");
		}
		String errorString;
		if (pOrder.getPaymentGroupCount() == TRUCheckoutConstants.INT_ONE && pGcBalance < ((TRULayawayOrderImpl) pOrder).getPaymentAmount()) {
			errorString = getErrorHandlerManager().getErrorMessage(TRUCheckoutConstants.GIFT_CARD_BALANCE);
			addHashedError(pPipelineResult, TRUCheckoutConstants.GIFT_CARD_ERR, pGiftCard.getId(), errorString);
			return STOP_CHAIN_EXECUTION_AND_COMMIT;
		} else if (pGcBalance < pGiftCard.getAmount()) {
			errorString = getErrorHandlerManager().getErrorMessage(TRUCheckoutConstants.INSUFF_GIFT_CARD_BALANCE);
			addHashedError(pPipelineResult, TRUCheckoutConstants.GIFT_CARD_ERR, pGiftCard.getId(), errorString);
			return STOP_CHAIN_EXECUTION_AND_COMMIT;

		}
		if(isLoggingDebug()){
			vlogDebug("End : pipeLineStatus method");
		}
		return SUCCESS_CODE;
	}

	/**
	 * @param pPipelineResult - Pipeline Result
	 * @param pGiftCard - gift card
	 * @param pOrder - Order
	 * @return  int
	 * @throws SystemException - SystemException
	 */
	private TRUGiftCardStatus giftCardStatus(PipelineResult pPipelineResult,
			TRUGiftCard pGiftCard, Order pOrder) throws SystemException {
		if(isLoggingDebug()){
			vlogDebug("Start : giftCardStatus method");
		}
		String errorString;
		TRUGiftCardStatus giftCardStatus;
		TRUGiftCardInfo giftCardInfo;
		giftCardInfo = new TRUGiftCardInfo();
		giftCardInfo.setGiftCardNumber(pGiftCard.getGiftCardNumber());
		giftCardInfo.setPin(pGiftCard.getGiftCardPin());
		giftCardInfo.setCardHolderName(TRUCheckoutConstants.JOE_SMITH);
		giftCardInfo.setOrder(pOrder);
		if(isLoggingDebug()){
			vlogDebug("in method giftCardStatus -giftCardInfo.getGiftCardNumber() {0}", giftCardInfo.getGiftCardNumber());
		}
		if (StringUtils.isBlank(giftCardInfo.getGiftCardNumber())) {   
			errorString = getErrorHandlerManager().getErrorMessage(TRUCheckoutConstants.INVALID_GIFT_CARD_NO);
			addHashedError(pPipelineResult, TRUCheckoutConstants.GIFT_CARD_ERR, pGiftCard.getId(), errorString);
		}
		if (StringUtils.isBlank(pGiftCard.getGiftCardPin())) {
			errorString = getErrorHandlerManager().getErrorMessage(TRUCheckoutConstants.INVALID_GIFT_CARD_PIN);
			addHashedError(pPipelineResult, TRUCheckoutConstants.GIFT_CARD_ERR, pGiftCard.getId(), errorString);
		}
		giftCardStatus=getPaymentManager().getGiftCardBalance(giftCardInfo);
		if(isLoggingDebug()){
			vlogDebug("End : giftCardStatus method");
		}
		return giftCardStatus;
	}


	/**
	 * Return the list of possible return values from this processor. This
	 * processor always returns a single value indicating success. In case of
	 * errors, it adds messages to the pipeline result object.
	 *
	 * @return the list of possible return values
	 */
	public int[] getRetCodes() {
		if(isLoggingDebug()){
			vlogDebug("START : ProcValidateGiftCardPG (getRetCodes()) : BEGIN and END");
		}
		return mRetCodes;
	}

	
	/**
	 * This method is to add error message.
	 * @param pResult PipelineResult.
	 * @param pKey String
	 * @param pId PipelineResult.
	 * @param pError String
	 * 
	 */
	protected void addHashedError(PipelineResult pResult, String pKey, String pId, Object pError) {
		if(isLoggingDebug()){
			vlogDebug("START : ProcValidateGiftCardPG (addHashedError())");
		}
		Object errorObj = pResult.getError(pKey);
		if (errorObj == null) {
			Map errorMap = new HashMap();
			pResult.addError(pKey, errorMap);
			errorMap.put(pId, pError);
		} else if (errorObj instanceof Map) {
			Map errorMap = (Map) errorObj;
			errorMap.put(pId, pError);
		}
		if(isLoggingDebug()){
			vlogDebug("START : ProcValidateGiftCardPG (addHashedError())");
		}
	}

	


}
