package com.tru.commerce.order.processor;

import java.util.HashMap;
import java.util.Map;

import atg.commerce.order.Order;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.purchase.TRUShippingProcessHelper;
import com.tru.common.TRUConstants;

/**
 * This class Extends  OOTB PipelineProcessor and sets estimated delivery date.
 * @author PA
 * @version 1.0
 */
public class ProcTRUSetEstimateDeliveryDate extends ApplicationLoggingImpl
		implements PipelineProcessor {
	/**
	 * property: Reference to the SUCCESS constant.
	 */
	private static final int SUCCESS = TRUConstants.INTEGER_NUMBER_ONE;
	
	/**
	 * property: Reference to the ShippingProcessHelper component.
	 */
	private TRUShippingProcessHelper mShippingHelper;
	
	/**
	 * @return the shippingHelper
	 */
	public TRUShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * @param pShippingHelper
	 *            the shippingHelper to set
	 */
	public void setShippingHelper(TRUShippingProcessHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}
	/**
	 * Sets estimated delivery date in the order.
	 *
	 * @param pParamObject a HashMap which must contain an Order and optionally a Locale object.
	 * @param pParamPipelineResult a PipelineResult object which stores any information which must
	 *                be returned from this method invocation.
	 *                
	 * @return an integer specifying the processor's return code.
	 * @throws Exception Exception
	 * @see atg.service.pipeline.PipelineProcessor#runProcess(Object, PipelineResult)
	 */
	@Override
	public int runProcess(Object pParamObject,
			PipelineResult pParamPipelineResult) throws Exception {
		if (isLoggingDebug()) {
			vlogDebug("Start ProcTRUSetEstimateDeliveryDate.runProcess()");
		}

		Map map = (HashMap) pParamObject;
		Order order = (Order) map.get(TRUCheckoutConstants.PARAM_ORDER);

		if (order != null) {
			if (isLoggingDebug()) {
				vlogDebug("Order id is {0}", order.getId());
			}
			getShippingHelper().updateEstimateDeliveryDates(order);
		}

		if (isLoggingDebug()) {
			vlogDebug("End ProcTRUSetEstimateDeliveryDate.runProcess()");
		}
		return SUCCESS;
	}
	/**
	 * Returns the valid return codes:
	 * 1 - The processor completed.
	 * 
	 * @return an integer array of the valid return codes.
	 */
	@Override
	public int[] getRetCodes() {
		int[] ret = { TRUConstants.INTEGER_NUMBER_ONE };
		return ret;
	}

}
