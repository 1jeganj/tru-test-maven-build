package com.tru.commerce.order.processor;

import java.util.HashMap;
import java.util.Map;

import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.core.util.StringUtils;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.integrations.sucn.coupon.TRUSUCNCouponService;
import com.tru.integrations.sucn.coupon.response.CouponResponse;

/**
 * Implemented to call Use Coupon Service 
 * The Class ProcTRUUseCouponProcessor.
 *  @author Professional access
 *  @version 1.0
 */
public class ProcTRUUseCouponProcessor extends ApplicationLoggingImpl
implements PipelineProcessor {
	
	/**
	 * Resource bundle name.
	 */
	private static final String MY_RESOURCE_NAME = "atg.commerce.order.OrderResources";
	/**
	 * Resource message keys.
	 */
	private  static final String MSG_INVALID_ORDER_PARAMETER = "InvalidOrderParameter";

	/** 
	 * Resource Bundle. 
	 */
	private static java.util.ResourceBundle sResourceBundle = 
			LayeredResourceBundle.getBundle(MY_RESOURCE_NAME, atg.service.dynamo.LangLicense.getLicensedDefault());

	/** The Constant SUCCESS. 
	 */
	private static final int SUCCESS = 1;

	/** The Purchase process helper.
	 */
	private TRUSUCNCouponService mUseCouponService;
	/** Enable/disable flag .
	 */
	private boolean mEnable;
	
	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * Returns the valid return codes:
	 * 1 - The processor completed.
	 * @return an integer array of the valid return codes.
	 */
	public int[] getRetCodes() {
		int[] ret = { SUCCESS };

		return ret;
	}

	/**
	 * Override Run process for use coupon service.
	 *
	 * @param pResult a PipelineResult object which stores any information which must
	 *                be returned from this method invocation.
	 *                
	 * @return an integer specifying the processor's return code.
	 * @param pParamObject - paramObject
	 * 
	 * @throws Exception - when it catch
	 * 
	 * @see atg.service.pipeline.PipelineProcessor#runProcess(Object, PipelineResult)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public int runProcess(Object pParamObject, PipelineResult pResult) throws Exception {
		if(isLoggingDebug()){
			vlogDebug("ProcTRUUseCouponProcessor.runProcess()methods.STARTS");
		}
		if(isEnable()){
			if(isLoggingDebug()){
				vlogDebug("ProcTRUUseCouponProcessor.runProcess()methods.STARTS");
			}
			Map map = (HashMap) pParamObject;
			String errorMessage = null;
			Order order = (Order) map.get(PipelineConstants.ORDER);
			Map<String, String> couponData = null;
			String singleUseCoupon = null;
			if (order == null) {
				throw new InvalidParameterException(ResourceUtils.getMsgResource(MSG_INVALID_ORDER_PARAMETER, 
						MY_RESOURCE_NAME, sResourceBundle));
			}
			if (order instanceof TRUOrderImpl) {
				couponData = ((TRUOrderImpl)order).getSingleUseCoupon();
			}
			if (couponData == null) {
				return SUCCESS ;
			}
			for (Map.Entry<String, String> coupon : couponData.entrySet()) {
				singleUseCoupon = coupon.getKey();
				//Fix for TSJ-8822
				//if (StringUtils.isNotBlank(singleUseCoupon)) {
				if (StringUtils.isNotBlank(singleUseCoupon) && StringUtils.isNotBlank(coupon.getValue()) && !singleUseCoupon.equals(coupon.getValue())) {
					CouponResponse useCouponDetails = getUseCouponService().useCouponDetails(singleUseCoupon, order.getId());
					if (useCouponDetails != null && useCouponDetails.getReturnCode() > 0) {
						errorMessage = TRUCheckoutConstants.THE_COUPON_NO + useCouponDetails.getSUCcouponNumber() + TRUCheckoutConstants.ALREADY_USED;
						pResult.addError(TRUCheckoutConstants.FAILED_USE_COUPON, errorMessage);
						if (isLoggingError()) {
							vlogError("UseCouponFailed for the claiming coupon in ProcTRUUseCouponProcessor.runProcess(): {0}", coupon.getKey());
						}
						return STOP_CHAIN_EXECUTION;
					}
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("ProcTRUUseCouponProcessor.runProcess()methods.ENDS Orderid : {0}",order.getId());
			}
		}
		return SUCCESS ;
	}
	
	 

	/**
	 * @return mUseCouponService
	 */
	public TRUSUCNCouponService getUseCouponService() {
		return mUseCouponService;
	}

	/**
	 * @param pUseCouponService - useCouponService
	 */
	public void setUseCouponService(TRUSUCNCouponService pUseCouponService) {
		this.mUseCouponService = pUseCouponService;
	}
}
