package com.tru.commerce.order.processor;

import java.util.HashMap;
import java.util.Map;

import atg.commerce.CommerceException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.core.util.ResourceUtils;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUPaymentGroupManager;
import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.commerce.payment.TRUPaymentManager;
import com.tru.common.TRUConstants;

/**
 * This class will be responsible for adjustment of payment groups based on the order amount.
 *  
 * 
 * @author : PA
 * @version : 1.0
 */

public class ProcTRUAdjustPaymentGroups extends ApplicationLoggingImpl implements PipelineProcessor {
    /**
     * To Hold the Order Resources.
     */
    static final String MY_RESOURCE_NAME = "atg.commerce.order.OrderResources";

    /** 
     * Resource Bundle.
     */    
    private static java.util.ResourceBundle sResourceBundle = java.util.ResourceBundle.getBundle(MY_RESOURCE_NAME,
            											atg.service.dynamo.LangLicense.getLicensedDefault());
    /**
     *To hold Success constant.
     */
    private static final int SUCCESS = 1;
    
    /**
     *To hold FAILURE constant. 
     */
    private static final int FAILURE = -1;

    /**
     * String to hold Enable.
     */
    private boolean mEnable;       
    
	/**
	 * Holds the reference for TRUOrderManager.
	 */
	private TRUOrderManager mOrderManager;
	
	/**
	 * Property to hold TRUPaymentGroupManager.
	 */
	private TRUPaymentGroupManager mPaymentGroupManager;
	
	/**
	 * This property is to hold reference for TRUPaymentManager.
	 */
	private TRUPaymentManager mPaymentManager;
	
	/**
	 * Property to hold reference to TRUOrderTools.
	 */
	private TRUOrderTools mOrderTools;
	
    /**
     * @return the enable
     */
    public boolean isEnable() {
        return mEnable;
    }

    /**
     * @param pEnable the enable to set
     */
    public void setEnable(boolean pEnable) {
    	mEnable = pEnable;
    }

	/**
	 * @return the orderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * @param pOrderManager
	 *            the orderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		this.mOrderManager = pOrderManager;
	}
	
	/**
	 * @return the paymentGroupManager
	 */
	public TRUPaymentGroupManager getPaymentGroupManager() {
		return mPaymentGroupManager;
	}

	/**
	 * @param pPaymentGroupManager the paymentGroupManager to set
	 */
	public void setPaymentGroupManager(TRUPaymentGroupManager pPaymentGroupManager) {
		this.mPaymentGroupManager = pPaymentGroupManager;
	}
	
	/**
	 * @return the mPaymentManager
	 */
	public TRUPaymentManager getPaymentManager() {
		return mPaymentManager;
	}

	/**
	 * @param pPaymentManager the mPaymentManager to set
	 */
	public void setPaymentManager(TRUPaymentManager pPaymentManager) {
		this.mPaymentManager = pPaymentManager;
	}
	
	/**
	 * @return the orderTools
	 */
	public TRUOrderTools getOrderTools() {
		return mOrderTools;
	}

	/**
	 * @param pOrderTools the orderTools to set
	 */
	public void setOrderTools(TRUOrderTools pOrderTools) {
		this.mOrderTools = pOrderTools;
	}
	
   /**
     * Abstract method defined in PiplelineProcessor overridden.
     * @return Return Code Integer
     */
    public int[] getRetCodes() {
        int[] lRet = {SUCCESS};
        return lRet;
    }
    
    /**
     * This method invokes the process of adjustment of payment groups based on the order amount.
     *
     * @param pParam a HashMap which must contain an Order object passed as part of request.
     * @param pResult a PipelineResult object which stores any information which must be returned from this method invocation.
     * @return an integer specifying the processor's return code
     * @exception Exception throws any exception back to the caller
     * @see atg.service.pipeline.PipelineProcessor#runProcess(Object, PipelineResult)
     */
    public int runProcess(Object pParam, PipelineResult pResult) throws Exception {
        Map lParamMap = (HashMap) pParam;
        if (isEnable()) {
            if (isLoggingDebug()) {
                vlogDebug(" Inside Method runProcess of ProcTRUAdjustPaymentGroups  ");
            }
            Order truOrder = (Order) lParamMap.get(PipelineConstants.ORDER);
    		if (truOrder == null) {
    			throw new InvalidParameterException(ResourceUtils.getMsgResource(TRUPipeLineConstants.INVALID_ORDER_PARAMETER,
    					MY_RESOURCE_NAME, sResourceBundle));
    		}
            if (isLoggingDebug()) {
                vlogDebug("Order : " + truOrder.getId());
            }
            double giftCardAmtApplied = getOrderTools().getGiftCardAppliedAmount(truOrder);
            try {
                if (truOrder.getPaymentGroupCount()>TRUConstants.ZERO && giftCardAmtApplied>TRUConstants.DOUBLE_ZERO) {
                	getPaymentGroupManager().adjustGiftCardAmount(truOrder);
                }
            } catch (CommerceException pExec) {
                if (isLoggingError()) {
                    logError("RepositoryException during adjustment of payment groups for Order in ProcTRUAdjustPaymentGroups.runProcess(): " + 
                    																			truOrder.getId(), pExec);
                }
                return FAILURE;
            } 
        }
        return SUCCESS;
    }
   
}
