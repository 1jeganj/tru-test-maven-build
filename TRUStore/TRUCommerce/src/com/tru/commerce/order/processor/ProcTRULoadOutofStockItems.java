package com.tru.commerce.order.processor;

import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.order.ChangedProperties;
import atg.commerce.order.CommerceIdentifier;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.commerce.order.processor.SavedProperties;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItemDescriptor;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.order.TRUOrderTools;

/**
 * The Class ProcTRULoadOutofStockItems.
 */
public class ProcTRULoadOutofStockItems extends SavedProperties implements PipelineProcessor {

	/**
	 * String to hold Enable.
	 */
	private boolean mEnable;

	/** The Out of stock item info property. */
	private String mOutOfStockItemInfoProperty;

	/** The m order tools. */
	private TRUOrderTools mOrderTools;

	/**
	 * @param pParam - Param
	 * @param pResult - Result
	 * @throws  Exception - object of Exception
	 * @return status
	 */
	public int runProcess(Object pParam, PipelineResult pResult) throws Exception {
		vlogDebug("@Class::TRULoadOutofStockItems::@method::runProcess() : BEGIN ");
		if (isEnable()) {
			HashMap map = (HashMap) pParam;
			Order order = (Order) map.get(PipelineConstants.ORDER);
			// Check for null parameters
			if (order == null) {
				throw new InvalidParameterException();
			}
			MutableRepositoryItem orderItem = (MutableRepositoryItem) map.get(PipelineConstants.ORDERREPOSITORYITEM);
			loadOutofStockItems(order, orderItem);

		}
		vlogDebug("@Class::TRULoadOutofStockItems::@method::runProcess() : END ");
		return TRUCommerceConstants.ONE;
	}

	/**
	 * Load outof stock items.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pOrderItem
	 *            the order item
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws InstantiationException
	 *             the instantiation exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 * @throws PropertyNotFoundException
	 *             the property not found exception
	 * @throws IntrospectionException
	 *             the introspection exception
	 */
	public void loadOutofStockItems(Order pOrder, MutableRepositoryItem pOrderItem) throws RepositoryException,
			InstantiationException, IllegalAccessException, ClassNotFoundException, PropertyNotFoundException,
			IntrospectionException {
		vlogDebug("@Class::TRULoadOutofStockItems::@method::loadOutofStockItems() : BEGIN ");
		if (null != pOrderItem.getPropertyValue(getOutOfStockItemInfoProperty())) {
			List<MutableRepositoryItem> outOfStockItemInfoList = (List<MutableRepositoryItem>) pOrderItem
					.getPropertyValue(getOutOfStockItemInfoProperty());
			if (null != outOfStockItemInfoList && !outOfStockItemInfoList.isEmpty()) {
				List<CommerceIdentifier> ciList = new ArrayList<CommerceIdentifier>();
				for (MutableRepositoryItem mutItem : outOfStockItemInfoList) {
					if (null != mutItem) {
						RepositoryItemDescriptor desc = mutItem.getItemDescriptor();
						String className = getOrderTools().getMappedBeanName(desc.getItemDescriptorName());

						/*
						 * Next, an instance of OutofStockItems is constructed, its id property set to the repository item's id,
						 * and if the object has a repositoryItem property, the item descriptor is set to it.
						 */
						CommerceIdentifier ci = (CommerceIdentifier) Class.forName(className).newInstance();
						DynamicBeans.setPropertyValue(ci, TRUCommerceConstants.ID, mutItem.getRepositoryId());
						if (DynamicBeans.getBeanInfo(ci).hasProperty(TRUCommerceConstants.REPOSITORY_ITEM)) {
							DynamicBeans.setPropertyValue(ci, TRUCommerceConstants.REPOSITORY_ITEM, mutItem);
						}
						if (isLoggingDebug()) {
							vlogDebug("ci : {0}", ci);
						}
						/*
						 * If the loaded object implements ChangedProperties, then clear the changedProperties Set. Then set the
						 * OutofStockItems property in the Order object to ci. Finally, return SUCCESS.
						 */
						if (ci instanceof ChangedProperties) {
							((ChangedProperties) ci).clearChangedProperties();
						}
						ciList.add(ci);
					}
				}
				// Updating bean list into order object
				if (!ciList.isEmpty()) {
					DynamicBeans.setPropertyValue(pOrder, getOutOfStockItemInfoProperty(), ciList);
				}
			}
		}
		vlogDebug("@Class::TRULoadOutofStockItems::@method::loadOutofStockItems() : END ");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see atg.service.pipeline.PipelineProcessor#getRetCodes()
	 */
	@Override
	public int[] getRetCodes() {
		int[] ret = { TRUCommerceConstants.ONE };
		return ret;
	}
	
	/**
	 * Gets the order tools.
	 * 
	 * @return the order tools
	 */
	public TRUOrderTools getOrderTools() {
		return mOrderTools;
	}

	/**
	 * Sets the order tools.
	 * 
	 * @param pOrderTools
	 *            the new order tools
	 */
	public void setOrderTools(TRUOrderTools pOrderTools) {
		mOrderTools = pOrderTools;
	}

	/**
	 * Gets the out of stock item info property.
	 * 
	 * @return the out of stock item info property
	 */
	public String getOutOfStockItemInfoProperty() {
		return mOutOfStockItemInfoProperty;
	}

	/**
	 * Sets the out of stock item info property.
	 * 
	 * @param pOutOfStockItemInfoProperty
	 *            the new out of stock item info property
	 */
	public void setOutOfStockItemInfoProperty(String pOutOfStockItemInfoProperty) {
		mOutOfStockItemInfoProperty = pOutOfStockItemInfoProperty;
	}

	/**
	 * Checks if is enable.
	 * 
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * Sets the enable.
	 * 
	 * @param pEnable
	 *            the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

}
