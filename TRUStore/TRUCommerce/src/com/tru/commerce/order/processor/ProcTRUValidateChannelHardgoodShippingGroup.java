package com.tru.commerce.order.processor;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.processor.ProcValidateHardgoodShippingGroup;
import atg.core.util.StringUtils;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.common.cml.SystemException;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;

/**
 * This class validates all the properties of Channel HardgoodShippingGroup.
 * 
 * @author Professional access
 * @version 1.0
 */
public class ProcTRUValidateChannelHardgoodShippingGroup extends ProcValidateHardgoodShippingGroup {
	/** Property to Hold Error handler manager. */
	private DefaultErrorHandlerManager mErrorHandlerManager;

	/**
	 * Gets the error handler manager.
	 * 
	 * @return the error handler manager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * Sets the error handler manager.
	 * 
	 * @param pErrorHandlerManager
	 *            the new error handler manager
	 */
	public void setErrorHandlerManager(DefaultErrorHandlerManager pErrorHandlerManager) {
		mErrorHandlerManager = pErrorHandlerManager;
	}

	/**	This method adds errors to Error handler based on empty channelType, channelId, channelName, channelUserName,
	 * 			channelCoUserName and registrantId
	 * @param pHsg the HardgoodShippingGroup object 
	 * @param pResult the PipelineResult object 
	 * @param pResourceBundle the key to use to store the errors 
	 */

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void validateHardgoodShippingGroupFields(HardgoodShippingGroup pHsg, PipelineResult pResult,ResourceBundle pResourceBundle) {
		super.validateHardgoodShippingGroupFields(pHsg, pResult,pResourceBundle);
		TRUChannelHardgoodShippingGroup channelHardgoodShippingGroup = (TRUChannelHardgoodShippingGroup) pHsg;
		String channelType = channelHardgoodShippingGroup.getChannelType();
		String channelId = channelHardgoodShippingGroup.getChannelId();
		String channelUserName = channelHardgoodShippingGroup.getChannelUserName();
		Map errorsMap = (Map) pResult.getError(channelHardgoodShippingGroup.getId());
		String error = null;
		if (errorsMap== null) {
			errorsMap = new HashMap();
			errorsMap.put(channelHardgoodShippingGroup.getId(), errorsMap);
		}
		try {
			if (StringUtils.isBlank(channelType)) {
				error = getErrorHandlerManager().getErrorMessage(TRUCheckoutConstants.ERROR_CHANNELTYPE_EMPTY);
				errorsMap.put(TRUCheckoutConstants.CHANNEL_TYPE, error);
			}if (StringUtils.isBlank(channelId)) {
				error = getErrorHandlerManager().getErrorMessage(TRUCheckoutConstants.ERROR_CHANNELID_EMPTY);
				errorsMap.put(TRUCheckoutConstants.CHANNEL_ID, error);
			}if (StringUtils.isBlank(channelUserName)) {
				error = getErrorHandlerManager().getErrorMessage(TRUCheckoutConstants.ERROR_CHANNELUSERNAME_EMPTY);
				errorsMap.put(TRUCheckoutConstants.USER_NAME, error);
			}
		} catch (SystemException systemExp) {
			if (isLoggingError()) {
				vlogError("SystemException at TRUProcValidateHardgoodShippingGroup.validateHardgoodShippingGroupFields :{0}",systemExp);
			}
		}
	}
}
