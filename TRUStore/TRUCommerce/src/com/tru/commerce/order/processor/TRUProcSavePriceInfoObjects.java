package com.tru.commerce.order.processor;

import java.beans.IntrospectionException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.order.CommerceIdentifier;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.processor.ProcSavePriceInfoObjects;
import atg.commerce.pricing.AmountInfo;
import atg.commerce.pricing.TaxPriceInfo;
import atg.commerce.states.StateDefinitions;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.radial.integration.taxware.TRUTaxwareConstant;
/**
 * This method extends OOTB ProcSavePriceInfoObjects.
 * @author PA
 * @version 1.0
 */
public class TRUProcSavePriceInfoObjects extends ProcSavePriceInfoObjects {
	/**
	 * Holds the ShipItemRelationsTaxPriceInfosProperty.
	 */
	private String mShipItemRelationsTaxPriceInfosProperty;

	/**
	 * @return the shipItemRelationsTaxPriceInfosProperty
	 */
	public String getShipItemRelationsTaxPriceInfosProperty() {
		return mShipItemRelationsTaxPriceInfosProperty;
	}
	/**
	 * @param pShipItemRelationsTaxPriceInfosProperty the shipItemRelationsTaxPriceInfosProperty to set
	 */
	public void setShipItemRelationsTaxPriceInfosProperty(
			String pShipItemRelationsTaxPriceInfosProperty) {
		mShipItemRelationsTaxPriceInfosProperty = pShipItemRelationsTaxPriceInfosProperty;
	}

	/**
	 * This OTB method is overriden to set the custom multitype property into taxPriceInfo object.
	 * @param pOrder order
	 * @param pCi ci
	 * @param pPiRepItem piRepItem
	 * @param pRepItemPropName repItemPropName
	 * @param pPriceInfo priceInfo
	 * @param pMutRep mutRep
	 * @param pOrderManager orderManager
	 * @return MutableRepositoryItem Object
	 * @throws  RepositoryException - RepositoryException
	 * @throws IntrospectionException - IntrospectionException
	 * @throws PropertyNotFoundException - PropertyNotFoundException
	 * @throws CommerceException - CommerceException
	 * 
	 */
	protected MutableRepositoryItem savePriceInfo(Order pOrder, CommerceIdentifier pCi, MutableRepositoryItem pPiRepItem, String pRepItemPropName, AmountInfo pPriceInfo, MutableRepository pMutRep, OrderManager pOrderManager)
			throws RepositoryException, IntrospectionException, PropertyNotFoundException, CommerceException {
		MutableRepositoryItem lpiRepItem = pPiRepItem;
		lpiRepItem = super.savePriceInfo(pOrder, pCi, lpiRepItem, pRepItemPropName, pPriceInfo, pMutRep, pOrderManager);
		if(lpiRepItem.getItemDescriptor().hasProperty(getShipItemRelationsTaxPriceInfosProperty())) {
			saveShippingRelationItemsTaxPriceInfos(pOrder, pPriceInfo, lpiRepItem, pMutRep, pOrderManager);
		}
		return lpiRepItem;
	}

	/**
	 * @param pOrder - pOrder
	 * @param pPriceInfo - pPriceInfo
	 * @param pMutItem - pMutItem
	 * @param pMutRep - pMutRep
	 * @param pOrderManager - pOrderManager
	 * @throws RepositoryException - RepositoryException
	 * @throws IntrospectionException -IntrospectionException
	 * @throws PropertyNotFoundException -PropertyNotFoundException
	 * @throws CommerceException - CommerceException
	 */
	protected void saveShippingRelationItemsTaxPriceInfos(Order pOrder, AmountInfo pPriceInfo, MutableRepositoryItem pMutItem, MutableRepository pMutRep, OrderManager pOrderManager)
			throws RepositoryException, IntrospectionException, PropertyNotFoundException, CommerceException {
		Map shipTaxMap = (Map)DynamicBeans.getPropertyValue(pPriceInfo, getShipItemRelationsTaxPriceInfosProperty());
		Map shipTaxItemMap = (Map)pMutItem.getPropertyValue(getShipItemRelationsTaxPriceInfosProperty());
		if ((shipTaxMap == null) || (shipTaxMap.size() == TRUTaxwareConstant.NUMERIC_ZERO)) {
			shipTaxItemMap.clear();
			return;
		}
		Set objSet = shipTaxMap.keySet();
		Set itemSet = shipTaxItemMap.keySet();
		Iterator iter = objSet.iterator();
		while (iter.hasNext()) {
			String key = (String)iter.next();
			if (!(itemSet.contains(key))){
				MutableRepositoryItem tpiMutItem = pMutRep.createItem(getTaxPriceInfoDescName());
				if (isLoggingDebug()) {
					vlogDebug("create in repository[" + tpiMutItem.getItemDescriptor().getItemDescriptorName() + ":" + tpiMutItem.getRepositoryId() + "]");
				}
				shipTaxItemMap.put(key, tpiMutItem);
			}
		}
		iter = itemSet.iterator();
		while (iter.hasNext()) {
			String key = (String)iter.next();
			if (!(objSet.contains(key))) {
				RepositoryItem value = (RepositoryItem)shipTaxItemMap.get(key);
				iter.remove();
				pMutRep.removeItem(value.getRepositoryId(), value.getItemDescriptor().getItemDescriptorName());
			}

		}
		iter = shipTaxItemMap.keySet().iterator();
		while(iter.hasNext()) {
			String key = (String)iter.next();
			MutableRepositoryItem tpiMutItem = (MutableRepositoryItem)shipTaxItemMap.get(key);
			TaxPriceInfo tpi = (TaxPriceInfo)shipTaxMap.get(key);

			writeProperties(pOrder, getSavedProperties(), tpi, tpiMutItem, pMutRep, pOrderManager);

			String saveMode = getSaveMode(pOrder);
			if((saveMode.equals(TRUTaxwareConstant.ALL)) || (saveMode.equals(TRUTaxwareConstant.ORDER))) {
				savePricingAdjustments(pOrder, tpi, tpiMutItem, pMutRep, pOrderManager);

				if(tpiMutItem.getItemDescriptor().hasProperty(getCurrentPriceDetailsProperty())){
					saveDetailedItemPriceInfos(pOrder, tpi, tpiMutItem, pMutRep, pOrderManager);
				}
			}
		}
	}

	/**
	 * @param pOrder - Order
	 * @return SaveMode
	 */
	private String getSaveMode(Order pOrder) {
		return getSaveMode(StateDefinitions.ORDERSTATES.getStateString(pOrder.getState()));
	}

	/**
	 * 
	 * @param pOrderState -orderState
	 * @return saveMode
	 */
	private String getSaveMode(String pOrderState) {
		String saveMode = (String)getOrderStateSaveModes().get(pOrderState);
		if (isLoggingDebug()) {
			vlogDebug("Mapped save mode for order state: " + pOrderState + " is " + saveMode);
		}
		if(saveMode == null) {
			if(isLoggingDebug()) {
				vlogDebug("No mapped save mode for this order state.  Using default save mode: " + getDefaultSaveMode());
			}
			saveMode = getDefaultSaveMode();
		}
		return saveMode;
	}

}