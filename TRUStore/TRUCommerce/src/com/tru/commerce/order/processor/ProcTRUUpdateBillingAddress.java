package com.tru.commerce.order.processor;

import java.util.HashMap;
import java.util.Map;

import atg.commerce.order.PipelineConstants;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * The Class ProcTRUUpdateBillingAddress.
 */
public class ProcTRUUpdateBillingAddress extends ApplicationLoggingImpl implements PipelineProcessor {
	
	/** Holds the OrderManager. */
	private TRUOrderManager mOrderManager;

	/**
	 * Gets the order manager.
	 *
	 * @return the orderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * Sets the order manager.
	 *
	 * @param pOrderManager the orderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	/**
	 *  
	 *
	 * @param pParamObject - Object
	 * @param pParamPipelineResult - PipelineResult
	 * @return the int
	 * @throws Exception the exception
	 */
	public int runProcess(Object pParamObject, PipelineResult pParamPipelineResult) throws Exception {

			Map map = (HashMap) pParamObject;
			
			TRUPropertyManager propertyManager = getOrderManager().getPropertyManager();
	
			TRUOrderImpl order = (TRUOrderImpl) map.get(PipelineConstants.ORDER);
	
			MutableRepository mutOrderRepository = (MutableRepository) getOrderManager().getOrderTools().getOrderRepository();
	
			MutableRepositoryItem mutBillingAddressRepItem = mutOrderRepository.createItem(propertyManager.getBillingAddressItemPropName());
	
			MutableRepositoryItem mutOrderRepItem = order.getRepositoryItem();
	
			Address billingAddress = order.getBillingAddress();
	
			getOrderManager().getOrderTools().copyAddress(billingAddress,mutBillingAddressRepItem);
			Address contactInfo = (Address) order.getBillingAddress();
			mutBillingAddressRepItem.setPropertyValue(propertyManager.getPhoneNumberPropertyName(), ((ContactInfo)contactInfo).getPhoneNumber());
			mutBillingAddressRepItem.setPropertyValue(TRUCommerceConstants.STATE,contactInfo.getState());
			mutOrderRepository.addItem(mutBillingAddressRepItem);
			
			mutOrderRepItem.setPropertyValue(propertyManager.getBillingAddressItemPropName(),mutBillingAddressRepItem);
		
		return mSUCCESS;
	}

	/**
	 * Constant holds the Success value.
	 */
	private final int mSUCCESS = TRUCommerceConstants.INT_ONE;

	/**
	 * Gets the ret codes.
	 *
	 * @return ret - int[]
	 */
	public int[] getRetCodes() {
		int[] ret = { mSUCCESS };
		return ret;
	}
}
