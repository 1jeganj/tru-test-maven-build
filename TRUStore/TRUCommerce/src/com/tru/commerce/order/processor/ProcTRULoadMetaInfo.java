package com.tru.commerce.order.processor;

import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import atg.adapter.gsa.ChangeAwareList;
import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.order.ChangedProperties;
import atg.commerce.order.CommerceIdentifier;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.commerce.order.processor.SavedProperties;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.helper.TRUCartModifierHelper;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;

/**
 * The Class ProcTRULoadMetaInfo.
 */
public class ProcTRULoadMetaInfo extends SavedProperties implements PipelineProcessor {

	/** property to hold OrderTools. */
	private TRUOrderTools mOrderTools;

	/** The Bpp info property. */
	private String mCommerceItemmetaInfoProperty;

	/** The Enable. */
	private boolean mEnable;

	/** The TRU cart modifier helper. */
	private TRUCartModifierHelper mTRUCartModifierHelper;

	/* (non-Javadoc)
	 * @see atg.service.pipeline.PipelineProcessor#getRetCodes()
	 */
	@Override
	public int[] getRetCodes() {
		int[] l_iRet = { TRUCommerceConstants.ONE };
		return l_iRet;
	}

	/* (non-Javadoc)
	 * @see atg.service.pipeline.PipelineProcessor#runProcess(java.lang.Object, atg.service.pipeline.PipelineResult)
	 */
	@Override
	public int runProcess(Object pParam, PipelineResult pArg1) throws Exception {
		if (isLoggingDebug()) {
			logDebug("@Class::ProcTRULoadMetaInfo::@method::runProcess() : BEGIN");
		}
		if (isEnable()) {

			HashMap map = (HashMap) pParam;
			Order order = (Order) map.get(PipelineConstants.ORDER);
			// Check for null parameters
			if (order == null) {
				throw new InvalidParameterException();
			}

			List<TRUShippingGroupCommerceItemRelationship> sgCiRels = null;
			sgCiRels = getTRUCartModifierHelper().getShippingGroupCommerceItemRelationships(order);
			if ((sgCiRels != null && !sgCiRels.isEmpty())) {
				for (TRUShippingGroupCommerceItemRelationship sgciRel : sgCiRels) {
					loadMetaInfoItems(sgciRel);
				}
			}

		}
		if (isLoggingDebug()) {
			logDebug("@Class::ProcTRULoadMetaInfo::@method::runProcess() : END");
		}
		return TRUCommerceConstants.ONE;
	}

	/**
	 * loadMetaInfoItems
	 * @param pSGCIRel pSGCIRel
	 * @throws RepositoryException RepositoryException
	 * @throws PropertyNotFoundException PropertyNotFoundException
	 * @throws IntrospectionException IntrospectionException
	 * @throws CommerceException CommerceException
	 * @throws InstantiationException InstantiationException
	 * @throws IllegalAccessException IllegalAccessException
	 * @throws ClassNotFoundException ClassNotFoundException
	 * 
	 */
	private void loadMetaInfoItems(TRUShippingGroupCommerceItemRelationship pSGCIRel) throws RepositoryException,
			PropertyNotFoundException, IntrospectionException, CommerceException, InstantiationException, IllegalAccessException,
			ClassNotFoundException {
		if (isLoggingDebug()) {
			logDebug("@Class::ProcTRULoadMetaInfo::@method::loadBPPInfoItems() : BEGIN");
		}
		/*
		 * This section of code first gets the bppItemInfo item descriptor from the order repository item. Next it gets the
		 * bppItemInfo item descriptor. The third line of code does a lookup in the OrderTools object and returns the class mapped
		 * to the bppItemInfo item descriptor.
		 */
	     ChangeAwareList mutItem = (ChangeAwareList) pSGCIRel.getRepositoryItem().getPropertyValue(getCommerceItemmetaInfoProperty());
		if (mutItem == null) {
			return;
		}
	     List<CommerceIdentifier> ciList=new ArrayList<CommerceIdentifier>();
	     CommerceIdentifier ci = null;
		for(Object item:mutItem){
		RepositoryItemDescriptor desc = ((RepositoryItem) item).getItemDescriptor();
		String className = getOrderTools().getMappedBeanName(desc.getItemDescriptorName());

		/*
		 * Next, an instance of bppItemInfo is constructed, its id property set to the repository item's id, and if the object has
		 * a repositoryItem property, the item descriptor is set to it.
		 */
		 ci = (CommerceIdentifier) Class.forName(className).newInstance();
		DynamicBeans.setPropertyValue(ci, TRUCommerceConstants.ID, ((RepositoryItem) item).getRepositoryId());
		if (DynamicBeans.getBeanInfo(ci).hasProperty(TRUCommerceConstants.REPOSITORY_ITEM)) {
			DynamicBeans.setPropertyValue(ci, TRUCommerceConstants.REPOSITORY_ITEM, item);
		}
		if (isLoggingDebug()) {
			vlogDebug("ci : {0}", ci);
		}
		/*
		 * If the loaded object implements ChangedProperties, then clear the changedProperties Set. Then set the bppItemInfo
		 * property in the Order object to ci. Finally, return SUCCESS.
		 */
		if (ci instanceof ChangedProperties) {
			((ChangedProperties) ci).clearChangedProperties();
		}
		ciList.add(ci);
		}
		// Updating bean into order object
		DynamicBeans.setPropertyValue(pSGCIRel,getCommerceItemmetaInfoProperty(),ciList);
		if (isLoggingDebug()) {
			logDebug("@Class::ProcTRULoadMetaInfo::@method::loadBPPInfoItems() : END");
		}
	}
	/**
	 * Gets the order tools.
	 *
	 * @return the order tools
	 */
	public TRUOrderTools getOrderTools() {
		return mOrderTools;
	}

	/**
	 * Sets the order tools.
	 *
	 * @param pOrderTools the new order tools
	 */
	public void setOrderTools(TRUOrderTools pOrderTools) {
		mOrderTools = pOrderTools;
	}


	/**
	 * Checks if is enable.
	 *
	 * @return true, if is enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * Sets the enable.
	 *
	 * @param pEnable the new enable
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * Gets the TRU cart modifier helper.
	 * 
	 * @return the TRU cart modifier helper
	 */
	public TRUCartModifierHelper getTRUCartModifierHelper() {
		return mTRUCartModifierHelper;
	}

	/**
	 * Sets the TRU cart modifier helper.
	 * 
	 * @param pTRUCartModifierHelper
	 *            the new TRU cart modifier helper
	 */
	public void setTRUCartModifierHelper(TRUCartModifierHelper pTRUCartModifierHelper) {
		this.mTRUCartModifierHelper = pTRUCartModifierHelper;
	}

	/**
	 * This method is to get commerceItemmetaInfoProperty
	 *
	 * @return the commerceItemmetaInfoProperty
	 */
	public String getCommerceItemmetaInfoProperty() {
		return mCommerceItemmetaInfoProperty;
	}

	/**
	 * This method sets commerceItemmetaInfoProperty with pCommerceItemmetaInfoProperty
	 *
	 * @param pCommerceItemmetaInfoProperty the commerceItemmetaInfoProperty to set
	 */
	public void setCommerceItemmetaInfoProperty(String pCommerceItemmetaInfoProperty) {
		mCommerceItemmetaInfoProperty = pCommerceItemmetaInfoProperty;
	}

}
