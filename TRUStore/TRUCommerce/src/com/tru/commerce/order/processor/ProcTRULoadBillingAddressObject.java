package com.tru.commerce.order.processor;

import java.util.HashMap;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.PipelineConstants;
import atg.commerce.order.processor.LoadProperties;
import atg.commerce.states.OrderStates;
import atg.core.util.Address;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.payment.TRUPaymentConstants;
import com.tru.userprofiling.TRUPropertyManager;



/**
 * Process loads the Billing Address's of order into the container.
 * 
 * @author ORACLE
 * @version 1.0
 *
 */
public class ProcTRULoadBillingAddressObject extends LoadProperties implements
		PipelineProcessor {
	/**
	 * Constant to hold the int 1.
	 */
	private final static int SUCCESS = 1;
	/**
	 * Property to hold the PropertyManager.
	 */
	private TRUPropertyManager mPropertyManager;
	
	private boolean mEnable;
	
	private OrderStates mOrderStates;
	
	/**
	 * @return the orderStates
	 */
	public OrderStates getOrderStates() {
		return mOrderStates;
	}
	/**
	 * @param pOrderStates the orderStates to set
	 */
	public void setOrderStates(OrderStates pOrderStates) {
		mOrderStates = pOrderStates;
	}
	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}
	/**
	 * @param pEnable the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}
	/**
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}
	/**
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}
	/**
	 * @return return code
	 */
	@Override
	public int[] getRetCodes() {
		int[] ret = { SUCCESS };
		return ret;
	}
	/**
	 * This method loads the Billing address's order to the Order Object Container.
	 * 
	 * @param pParamObject - Object
	 * @param pParamPipelineResult - PipelineResult
	 * @return int
	 * @throws CommerceException - CommerceException
	 */
	public int runProcess(Object pParamObject,PipelineResult pParamPipelineResult) throws CommerceException {
		if(isLoggingDebug()){
			logDebug(" ProcTRULoadBillingAddressObject.runProcess() :: START");
		}
		
		HashMap map = (HashMap) pParamObject;
		// Fetching the Order.
		Order order = (Order) map.get(PipelineConstants.ORDER);
		
		if (isEnable() && OrderStates.SUBMITTED.equalsIgnoreCase(getOrderStates().getStateString(order.getState()))) {

			// Fetching the ordermanager.
			OrderManager orderManager = (OrderManager) map
					.get(PipelineConstants.ORDERMANAGER);
			// Fetching the OrderTools.
			TRUOrderTools orderTools = (TRUOrderTools) orderManager
					.getOrderTools();
			// Fetching the property manager.
			TRUPropertyManager propertyManager = (TRUPropertyManager) orderTools
					.getProfileTools().getPropertyManager();
			// MutableRepositoryItem of the order.
			MutableRepositoryItem orderItem = (MutableRepositoryItem) map
					.get(PipelineConstants.ORDERREPOSITORYITEM);
			// BillingAddress's repositoryItem.
			RepositoryItem billAddressRepItem = (RepositoryItem) orderItem
					.getPropertyValue(getPropertyManager().getBillingAddress());
			

			logDebug(" Address State :: "+billAddressRepItem.getPropertyValue(propertyManager.getAddressStatePropertyName()));
			
			if (isLoggingDebug()) {
				vlogDebug(
						" ProcTRULoadBillingAddressObject.runProcess,BillAddressRepItem:{0} OrderItem :{1}",
						billAddressRepItem, orderItem);
			}
			try {
				// Address object from the order.
				Address objAddress = (Address) Class.forName(
						TRUPaymentConstants.CONT_BILL_ADDRESS_CLASS_PAC)
						.newInstance();
				if (billAddressRepItem == null) {
					// Creating the new repository item for the Billing Address.
					billAddressRepItem = ((MutableRepository) orderTools
							.getOrderRepository()).createItem(propertyManager
							.getBillingAddress());
					billAddressRepItem.getPropertyValue(propertyManager.getAddressStatePropertyName());
				}
				// Setting the billingAddress item to the Address object.
				DynamicBeans.setPropertyValue(objAddress, getPropertyManager()
						.getRepositoryItemProperty(), billAddressRepItem);
				if (isLoggingDebug()) {
					vlogDebug(
							"Loading the Biling Address object into the order,Address Item:{0}",
							objAddress);
				}
				// Setting the Address object to the Order.
				DynamicBeans.setPropertyValue(order,
						propertyManager.getBillingAddress(), objAddress);
			} catch (InstantiationException insExp) {
				if (isLoggingError()) {
					vlogError(
							" InstantiationException at ProcTRULoadBillingAddressObject.runProcess,OrderId:{0} profileId:{1} InstantiationException:{2}  ",
							order.getId(), order.getProfileId(), insExp);
				}
				throw new CommerceException(insExp);
			} catch (IllegalAccessException illegalExp) {
				if (isLoggingError()) {
					vlogError(
							" IllegalAccessException at ProcTRULoadBillingAddressObject.runProcess,OrderId:{0} profileId:{1} IllegalAccessException:{2} ",
							order.getId(), order.getProfileId(), illegalExp);
				}
				throw new CommerceException(illegalExp);
			} catch (ClassNotFoundException classExp) {
				if (isLoggingError()) {
					vlogError(
							" ClassNotFoundException at ProcTRULoadBillingAddressObject.runProcess,OrderId:{0} profileId:{1} ClassNotFoundException:{2} ",
							order.getId(), order.getProfileId(), classExp);
				}
				throw new CommerceException(classExp);
			} catch (PropertyNotFoundException propExp) {
				if (isLoggingError()) {
					vlogError(
							" PropertyNotFoundException at ProcTRULoadBillingAddressObject.runProcess,OrderId:{0} profileId:{1} PropertyNotFoundException:{2}",
							order.getId(), order.getProfileId(), propExp);
				}
				throw new CommerceException(propExp);
			} catch (RepositoryException rep) {
				if (isLoggingError()) {
					vlogError(
							" RepositoryException at ProcTRULoadBillingAddressObject.runProcess,OrderId:{0} profileId:{1} RepositoryException:{2}",
							order.getId(), order.getProfileId(), rep);
				}
				throw new CommerceException(rep);
			}
		}
	    if(isLoggingDebug()){
			logDebug(" ProcTRULoadBillingAddressObject.runProcess() :: END");
		}
		return SUCCESS;
	}
}
