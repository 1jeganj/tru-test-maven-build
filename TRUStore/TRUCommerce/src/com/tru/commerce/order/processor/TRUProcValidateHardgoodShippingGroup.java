package com.tru.commerce.order.processor;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.processor.ProcValidateHardgoodShippingGroup;
import atg.commerce.order.processor.ValidateShippingGroupPipelineArgs;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.service.dynamo.LangLicense;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.common.TRUConstants;

/**
 * This class.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUProcValidateHardgoodShippingGroup extends ProcValidateHardgoodShippingGroup {

	private static ResourceBundle sResourceBundle = LayeredResourceBundle.getBundle(TRUConstants.ORDER_RESOURCES, LangLicense.getLicensedDefault());

	private static ResourceBundle sUserResourceBundle = LayeredResourceBundle.getBundle(TRUConstants.USER_MESSAGES, Locale.getDefault());

	/**
	 * Run process.
	 *
	 * @param pParam the param
	 * @param pResult the result
	 * @return the int
	 * @throws Exception the exception
	 */
	public int runProcess(Object pParam, PipelineResult pResult)
			throws Exception {
		ValidateShippingGroupPipelineArgs args = (ValidateShippingGroupPipelineArgs)pParam;
		Locale locale = args.getLocale();
		ShippingGroup shippingGroup = args.getShippingGroup();
		Order order = args.getOrder();
		ResourceBundle resourceBundle;
		if (locale == null) {
			resourceBundle = sUserResourceBundle;
		} else {
			resourceBundle = LayeredResourceBundle.getBundle(TRUConstants.USER_MESSAGES, locale);
		}
		if (shippingGroup == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(TRUConstants.INVALID_SHIP_GRP_PARAM, TRUConstants.ORDER_RESOURCES, sResourceBundle));
		}
		if (order == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(TRUConstants.INVALID_ORDER_PARAM, TRUConstants.ORDER_RESOURCES, sResourceBundle));
		}
		if (!(shippingGroup instanceof HardgoodShippingGroup )) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(TRUConstants.INVALID_SHIP_GRP_PARAM, TRUConstants.ORDER_RESOURCES, sResourceBundle));
		}
		if (isLoggingDebug()) {
			vlogDebug("Validating one HardgoodShippingGroup of type " + shippingGroup.getShippingGroupClassType());
		}
		boolean validationRequired = false;
		List commerceItemRelationships = shippingGroup.getCommerceItemRelationships();
		for (Object object : commerceItemRelationships) {
			ShippingGroupCommerceItemRelationship sgCIR = (ShippingGroupCommerceItemRelationship) object;
			if(!(sgCIR.getCommerceItem() instanceof  TRUDonationCommerceItem)) {
				validationRequired = true;
				break;
			}
		}
		if(validationRequired) {
			validateHardgoodShippingGroupFields((HardgoodShippingGroup)shippingGroup, pResult, resourceBundle);
			return TRUConstants.INTEGER_NUMBER_ONE;
		} else {
			return TRUConstants.INTEGER_NUMBER_ONE;
		}
	}
}