package com.tru.commerce.order.processor;

import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.order.ChangedProperties;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.commerce.order.processor.SavedProperties;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryUtils;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.cart.helper.TRUCartModifierHelper;
import com.tru.commerce.order.TRUCommerceItemMetaInfo;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;

/**
 * The Class ProcTRUSaveCommerceItemMetaInfo.
 */
public class ProcTRUSaveCommerceItemMetaInfo extends SavedProperties implements PipelineProcessor {

	/** property to hold OrderTools. */
	private TRUOrderTools mOrderTools;
	
	/** property to hold mMetaInfoItemDescName. */
	private String mMetaInfoItemDescName;
	
	/**
	 * @return the metaInfoID
	 */
	public String getMetaInfoItemDescName() {
		return mMetaInfoItemDescName;
	}

	/**
	 * @param pMetaInfoItemDescName the metaInfoID to set
	 */
	public void setMetaInfoItemDescName(String pMetaInfoItemDescName) {
		mMetaInfoItemDescName = pMetaInfoItemDescName;
	}

	/** The m commerce property manager. */
	private TRUCommercePropertyManager mCommercePropertyManager;

	/**
	 * Gets the commerce property manager.
	 *
	 * @return the commerce property manager
	 */
	public TRUCommercePropertyManager getCommercePropertyManager() {
		return mCommercePropertyManager;
	}

	/**
	 * Sets the commerce property manager.
	 *
	 * @param pCommercePropertyManager the new commerce property manager
	 */
	public void setCommercePropertyManager(TRUCommercePropertyManager pCommercePropertyManager) {
		this.mCommercePropertyManager = pCommercePropertyManager;
	}

	/** The m meta info. */
	private String mMetaInfo;

	/**
	 * Gets the meta info.
	 * 
	 * @return the meta info
	 */
	public String getMetaInfo() {
		return mMetaInfo;
	}

	/**
	 * Sets the meta info.
	 * 
	 * @param pMetaInfo
	 *            the new meta info
	 */
	public void setMetaInfo(String pMetaInfo) {
		this.mMetaInfo = pMetaInfo;
	}

	/**
	 * Gets the order tools.
	 * 
	 * @return the order tools
	 */
	public TRUOrderTools getOrderTools() {
		return mOrderTools;
	}

	/**
	 * Sets the order tools.
	 * 
	 * @param pOrderTools
	 *            the new order tools
	 */
	public void setOrderTools(TRUOrderTools pOrderTools) {
		this.mOrderTools = pOrderTools;
	}

	/** The Enable. */
	private boolean mEnable;

	/**
	 * Checks if is enable.
	 * 
	 * @return true, if is enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * Sets the enable.
	 * 
	 * @param pEnable
	 *            the new enable
	 */
	public void setEnable(boolean pEnable) {
		this.mEnable = pEnable;
	}

	/** The TRU cart modifier helper. */
	private TRUCartModifierHelper mCartModifierHelper;

	/**
	 * Gets the TRU cart modifier helper.
	 * 
	 * @return the TRU cart modifier helper
	 */
	public TRUCartModifierHelper getCartModifierHelper() {
		return mCartModifierHelper;
	}

	/**
	 * Sets the TRU cart modifier helper.
	 * 
	 * @param pCartModifierHelper
	 *            the new TRU cart modifier helper
	 */
	public void setCartModifierHelper(TRUCartModifierHelper pCartModifierHelper) {
		this.mCartModifierHelper = pCartModifierHelper;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see atg.service.pipeline.PipelineProcessor#getRetCodes()
	 */
	@Override
	public int[] getRetCodes() {
		int[] l_iRet = { TRUCommerceConstants.ONE };
		return l_iRet;
	}

	/**
	 * run process
	 * 
	 * @param pParam
	 * 			- Object 
	 * @param pArg1
	 * 			-Argument
	 * @throws Exception 
	 *@return 1
	 */
	@Override
	public int runProcess(Object pParam, PipelineResult pArg1) throws Exception {
		if (isLoggingDebug()) {
			logDebug("@Class::ProcTRUSaveCommerceItemMetaInfo::@method::runProcess() : BEGIN");
		}
		if (isEnable()) {
			HashMap map = (HashMap) pParam;
			Order order = (Order) map.get(PipelineConstants.ORDER);
			// Check for null parameters
			if (order == null) {
				throw new InvalidParameterException();
			}
			// Updating the bean information into order repository
			List<TRUShippingGroupCommerceItemRelationship> sgCiRels = null;
			sgCiRels = getCartModifierHelper().getShippingGroupCommerceItemRelationships(order);
			if ((sgCiRels != null && !sgCiRels.isEmpty())) {
				for (TRUShippingGroupCommerceItemRelationship sgciRel : sgCiRels) {
					saveCommerceItemMetaInfo(sgciRel);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::ProcTRUSaveCommerceItemMetaInfo::@method::runProcess() : END");
		}
		return TRUCommerceConstants.ONE;
	}

	/**
	 * Save commerce item meta info.
	 *
	 * @param pSgciRel            the sgci rel
	 * @throws RepositoryException the repository exception
	 * @throws PropertyNotFoundException the property not found exception
	 * @throws IntrospectionException the introspection exception
	 */
	private void saveCommerceItemMetaInfo(TRUShippingGroupCommerceItemRelationship pSgciRel) throws RepositoryException,
			PropertyNotFoundException, IntrospectionException {
		if (isLoggingDebug()) {
			logDebug("@Class::ProcTRUSaveCommerceItemMetaInfo::@method::saveCommerceItemMetaInfo() : BEGIN");
		}

		// Fetching the travel info from order object
		if (null != pSgciRel.getMetaInfo() && !pSgciRel.getMetaInfo().isEmpty()) {
			List<MutableRepositoryItem> mutItems = new ArrayList<MutableRepositoryItem>();
			for (TRUCommerceItemMetaInfo metaInfo : pSgciRel.getMetaInfo()) {
				MutableRepositoryItem metaInfoItem = null;
				MutableRepository orderRepository = (MutableRepository) getOrderTools().getOrderRepository();
				// Validating travel info object having the mutable travel info
				// repository item
				boolean hasProperty = false;
				if (DynamicBeans.getBeanInfo(metaInfo).hasProperty(TRUCommerceConstants.REPOSITORY_ITEM)) {
					hasProperty = true;
					// If the travelInfo mutable item available get it for update
					metaInfoItem = (MutableRepositoryItem) DynamicBeans.getPropertyValue(metaInfo,
							TRUCommerceConstants.REPOSITORY_ITEM);
				}

				// if the item is not available perform a lookup in order repository to
				// check item is available in repository
				if (metaInfoItem == null && metaInfo.getId() != null) {
					setMetaInfoItemDescName(getOrderTools()
							.getMappedItemDescriptorName(metaInfo.getClass().getName()));
					metaInfoItem = orderRepository.getItemForUpdate(metaInfo.getId(),getMetaInfoItemDescName() );
					if (hasProperty) {
						DynamicBeans.setPropertyValue(metaInfo, TRUCommerceConstants.REPOSITORY_ITEM, metaInfoItem);
					}
				}
				// Validating item need to be populated into repository
				if (metaInfoItem != null) {
					orderRepository.addItem(RepositoryUtils.getMutableRepositoryItem(metaInfoItem));
					mutItems.add(metaInfoItem);
				}

				// Finally, the ChangedProperties Set is cleared and the
				// saveAllProperties property is set to false. This resets
				// the object for more edits. Then the SUCCESS value is returned.
				if (metaInfo instanceof ChangedProperties) {
					ChangedProperties cp = (ChangedProperties) metaInfo;
					cp.clearChangedProperties();
					cp.setSaveAllProperties(false);
				}
			}
			pSgciRel.setPropertyValue(getCommercePropertyManager().getMetaInfo(), mutItems);
		} else {
			pSgciRel.setPropertyValue(getCommercePropertyManager().getMetaInfo(), null);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::ProcTRUSaveCommerceItemMetaInfo::@method::saveCommerceItemMetaInfo() : END");
		}
	}

}
