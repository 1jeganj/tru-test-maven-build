package com.tru.commerce.order.processor;

import java.beans.IntrospectionException;
import java.util.HashMap;
import java.util.List;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.order.ChangedProperties;
import atg.commerce.order.CommerceIdentifier;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.commerce.order.processor.SavedProperties;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItemDescriptor;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.helper.TRUCartModifierHelper;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;

/**
 * The Class ProcTRULoadBPPInfo.
 */
public class ProcTRULoadBPPInfo extends SavedProperties implements PipelineProcessor {

	/** property to hold OrderTools. */
	private TRUOrderTools mOrderTools;

	/** The Bpp info property. */
	private String mBppInfoProperty;

	/** The Enable. */
	private boolean mEnable;

	/** The TRU cart modifier helper. */
	private TRUCartModifierHelper mTRUCartModifierHelper;

	/* (non-Javadoc)
	 * @see atg.service.pipeline.PipelineProcessor#getRetCodes()
	 */
	@Override
	public int[] getRetCodes() {
		int[] l_iRet = { TRUCommerceConstants.ONE };
		return l_iRet;
	}

	/* (non-Javadoc)
	 * @see atg.service.pipeline.PipelineProcessor#runProcess(java.lang.Object, atg.service.pipeline.PipelineResult)
	 */
	@Override
	public int runProcess(Object pParam, PipelineResult pArg1) throws Exception {
		if (isLoggingDebug()) {
			logDebug("@Class::ProcTRULoadBPPInfo::@method::runProcess() : BEGIN");
		}
		if (isEnable()) {

			HashMap map = (HashMap) pParam;
			Order order = (Order) map.get(PipelineConstants.ORDER);
			// Check for null parameters
			if (order == null) {
				throw new InvalidParameterException();
			}

			List<TRUShippingGroupCommerceItemRelationship> sgCiRels = null;
			sgCiRels = getTRUCartModifierHelper().getShippingGroupCommerceItemRelationships(order);
			if ((sgCiRels != null && !sgCiRels.isEmpty())) {
				for (TRUShippingGroupCommerceItemRelationship sgciRel : sgCiRels) {
					loadBPPInfoItems(sgciRel);
				}
			}

		}
		if (isLoggingDebug()) {
			logDebug("@Class::ProcTRULoadBPPInfo::@method::runProcess() : END");
		}
		return TRUCommerceConstants.ONE;
	}

	/**
	 * Load bpp info items.
	 *
	 * @param pSGCIRel the SGCI rel
	 * @throws RepositoryException the repository exception
	 * @throws PropertyNotFoundException the property not found exception
	 * @throws IntrospectionException the introspection exception
	 * @throws CommerceException the commerce exception
	 * @throws InstantiationException the instantiation exception
	 * @throws IllegalAccessException the illegal access exception
	 * @throws ClassNotFoundException the class not found exception
	 */
	private void loadBPPInfoItems(TRUShippingGroupCommerceItemRelationship pSGCIRel) throws RepositoryException,
			PropertyNotFoundException, IntrospectionException, CommerceException, InstantiationException, IllegalAccessException,
			ClassNotFoundException {
		if (isLoggingDebug()) {
			logDebug("@Class::ProcTRULoadBPPInfo::@method::loadBPPInfoItems() : BEGIN");
		}
		/*
		 * This section of code first gets the bppItemInfo item descriptor from the order repository item. Next it gets the
		 * bppItemInfo item descriptor. The third line of code does a lookup in the OrderTools object and returns the class mapped
		 * to the bppItemInfo item descriptor.
		 */
		MutableRepositoryItem mutItem = (MutableRepositoryItem) pSGCIRel.getRepositoryItem().getPropertyValue(
				getBppInfoProperty());
		if (mutItem == null) {
			return;
		}
		RepositoryItemDescriptor desc = mutItem.getItemDescriptor();
		String className = getOrderTools().getMappedBeanName(desc.getItemDescriptorName());

		/*
		 * Next, an instance of bppItemInfo is constructed, its id property set to the repository item's id, and if the object has
		 * a repositoryItem property, the item descriptor is set to it.
		 */
		CommerceIdentifier ci = (CommerceIdentifier) Class.forName(className).newInstance();
		DynamicBeans.setPropertyValue(ci, TRUCommerceConstants.ID, mutItem.getRepositoryId());
		if (DynamicBeans.getBeanInfo(ci).hasProperty(TRUCommerceConstants.REPOSITORY_ITEM)) {
			DynamicBeans.setPropertyValue(ci, TRUCommerceConstants.REPOSITORY_ITEM, mutItem);
		}
		if (isLoggingDebug()) {
			vlogDebug("ci : {0}", ci);
		}
		/*
		 * If the loaded object implements ChangedProperties, then clear the changedProperties Set. Then set the bppItemInfo
		 * property in the Order object to ci. Finally, return SUCCESS.
		 */
		if (ci instanceof ChangedProperties) {
			((ChangedProperties) ci).clearChangedProperties();
		}
		// Updating bean into order object
		DynamicBeans.setPropertyValue(pSGCIRel, getBppInfoProperty(), ci);
		if (isLoggingDebug()) {
			logDebug("@Class::ProcTRULoadBPPInfo::@method::loadBPPInfoItems() : END");
		}
	}

	/**
	 * Gets the order tools.
	 *
	 * @return the order tools
	 */
	public TRUOrderTools getOrderTools() {
		return mOrderTools;
	}

	/**
	 * Sets the order tools.
	 *
	 * @param pOrderTools the new order tools
	 */
	public void setOrderTools(TRUOrderTools pOrderTools) {
		mOrderTools = pOrderTools;
	}

	/**
	 * Gets the bpp info property.
	 *
	 * @return the bpp info property
	 */
	public String getBppInfoProperty() {
		return mBppInfoProperty;
	}

	/**
	 * Sets the bpp info property.
	 *
	 * @param pBppInfoProperty the new bpp info property
	 */
	public void setBppInfoProperty(String pBppInfoProperty) {
		mBppInfoProperty = pBppInfoProperty;
	}

	/**
	 * Checks if is enable.
	 *
	 * @return true, if is enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * Sets the enable.
	 *
	 * @param pEnable the new enable
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * Gets the TRU cart modifier helper.
	 * 
	 * @return the TRU cart modifier helper
	 */
	public TRUCartModifierHelper getTRUCartModifierHelper() {
		return mTRUCartModifierHelper;
	}

	/**
	 * Sets the TRU cart modifier helper.
	 * 
	 * @param pTRUCartModifierHelper
	 *            the new TRU cart modifier helper
	 */
	public void setTRUCartModifierHelper(TRUCartModifierHelper pTRUCartModifierHelper) {
		this.mTRUCartModifierHelper = pTRUCartModifierHelper;
	}

}
