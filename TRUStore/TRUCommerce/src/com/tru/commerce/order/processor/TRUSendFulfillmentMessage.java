package com.tru.commerce.order.processor;

import atg.commerce.order.processor.ProcSendFulfillmentMessage;

import com.tru.messaging.oms.TRUCustomerOrderUpdateMessage;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This class is to send order update message to sink.
 * @author PA
 * @version 1.0
 *
 */
public class TRUSendFulfillmentMessage extends ProcSendFulfillmentMessage {
	
	/** mPropertyManager. */
	private TRUPropertyManager mPropertyManager;

	
	/**
	 * This method is to send the order update XML message.
	 *
	 * @param pOrdeId the orde id
	 * @param pProfileId the profile id
	 */
	public void sendRadialOrderUpdateMessage(String pOrdeId, String pProfileId) {
		if(isLoggingDebug()){
			logDebug(" Enter into TRUSendFulfillmentMessage.sendRadialOrderUpdateMessage() ");
		}
		if(isLoggingDebug()){
			logDebug(" Exit from TRUSendFulfillmentMessage.sendRadialOrderUpdateMessage() ");
		}
	}


	/**
	 * This method is to send the order update message to sink
	 * @param pCustomerOrderUpdateMessage - update message
	 */
	public void sendOrderUpdateMessage(TRUCustomerOrderUpdateMessage pCustomerOrderUpdateMessage) {
		if(isLoggingDebug()){
			logDebug(" Enter into TRUSendFulfillmentMessage.sendOrderUpdateMessage() ");
		}
		if(isLoggingDebug()){
			logDebug(" Exit from TRUSendFulfillmentMessage.sendOrderUpdateMessage() ");
		}
	}

	/**
	 * This method returns the propertyManager value
	 *
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * This method sets the propertyManager with parameter value pPropertyManager
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}
	
}
