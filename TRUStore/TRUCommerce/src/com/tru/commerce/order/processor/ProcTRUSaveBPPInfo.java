package com.tru.commerce.order.processor;

import java.beans.IntrospectionException;
import java.util.HashMap;
import java.util.List;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.order.ChangedProperties;
import atg.commerce.order.CommerceIdentifier;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.commerce.order.processor.SavedProperties;
import atg.repository.ConcurrentUpdateException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryUtils;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.helper.TRUCartModifierHelper;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;

/**
 * The Class ProcTRUSaveBPPInfo.
 */
public class ProcTRUSaveBPPInfo extends SavedProperties implements PipelineProcessor {

	/** property to hold OrderTools. */
	private TRUOrderTools mOrderTools;

	/** The Bpp info property. */
	private String mBppInfoProperty;

	/** The Enable. */
	private boolean mEnable;

	/** The TRU cart modifier helper. */
	private TRUCartModifierHelper mTRUCartModifierHelper;
	
	/** property to hold mMetaInfoItemDescName. */
	private String mMetaInfoItemDescName;

	/**
	 * @return the metaInfoItemDescName
	 */
	public String getMetaInfoItemDescName() {
		return mMetaInfoItemDescName;
	}

	/**
	 * @param pMetaInfoItemDescName the metaInfoItemDescName to set
	 */
	public void setMetaInfoItemDescName(String pMetaInfoItemDescName) {
		mMetaInfoItemDescName = pMetaInfoItemDescName;
	}

	/* (non-Javadoc)
	 * @see atg.service.pipeline.PipelineProcessor#getRetCodes()
	 */
	@Override
	public int[] getRetCodes() {
		int[] l_iRet = { TRUCommerceConstants.ONE };
		return l_iRet;
	}

	/* (non-Javadoc)
	 * @see atg.service.pipeline.PipelineProcessor#runProcess(java.lang.Object, atg.service.pipeline.PipelineResult)
	 */
	@Override
	public int runProcess(Object pParam, PipelineResult pArg1) throws Exception {
		if (isLoggingDebug()) {
			logDebug("@Class::ProcTRUSaveBPPInfo::@method::runProcess() : BEGIN");
		}
		if (isEnable()) {
			HashMap map = (HashMap) pParam;
			Order order = (Order) map.get(PipelineConstants.ORDER);
			// Check for null parameters
			if (order == null) {
				throw new InvalidParameterException();
			}
			// Updating the bean information into order repository
			List<TRUShippingGroupCommerceItemRelationship> sgCiRels = null;
			sgCiRels = getTRUCartModifierHelper().getShippingGroupCommerceItemRelationships(order);
			if ((sgCiRels != null && !sgCiRels.isEmpty())) {
				for (TRUShippingGroupCommerceItemRelationship sgciRel : sgCiRels) {
					addUpdateBPPInfo(order, sgciRel);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::ProcTRUSaveBPPInfo::@method::runProcess() : END");
		}
		return TRUCommerceConstants.ONE;
	}

	/**
	 * Adds the update bpp info.
	 *
	 * @param pOrder the order
	 * @param pSGCIRel the SGCI rel
	 * @throws RepositoryException the repository exception
	 * @throws PropertyNotFoundException the property not found exception
	 * @throws IntrospectionException the introspection exception
	 * @throws CommerceException the commerce exception
	 */
	private void addUpdateBPPInfo(Order pOrder, TRUShippingGroupCommerceItemRelationship pSGCIRel) throws RepositoryException,
			PropertyNotFoundException, IntrospectionException, CommerceException {
		if (isLoggingDebug()) {
			logDebug("@Class::ProcTRUSaveBPPInfo::@method::addUpdateBPPInfo() : BEGIN");
		}
		MutableRepositoryItem bppItem = null;
		MutableRepository orderRepository = (MutableRepository) getOrderTools().getOrderRepository();

		// Fetching the bpp info from order object
		CommerceIdentifier bppItemInfo = (CommerceIdentifier) DynamicBeans.getPropertyValue(pSGCIRel, getBppInfoProperty());

		// Validating bpp info object having the mutable travel info
		// repository item
		boolean hasProperty = false;
		if (DynamicBeans.getBeanInfo(bppItemInfo).hasProperty(TRUCommerceConstants.REPOSITORY_ITEM)) {
			hasProperty = true;
			// If the bppItemInfo mutable item available get it for update
			bppItem = (MutableRepositoryItem) DynamicBeans.getPropertyValue(bppItemInfo, TRUCommerceConstants.REPOSITORY_ITEM);
		}

		// if the item is not available perform a lookup in order repository to
		// check item is available in repository
		if (bppItem == null && bppItemInfo.getId() != null) {
			setMetaInfoItemDescName(getOrderTools().getMappedItemDescriptorName(bppItemInfo.getClass().getName()));
			bppItem = orderRepository.getItemForUpdate(bppItemInfo.getId(),getMetaInfoItemDescName());
			if (hasProperty) {
				DynamicBeans.setPropertyValue(bppItemInfo, TRUCommerceConstants.REPOSITORY_ITEM, bppItem);
			}
		}
		// Validating item need to be populated into repository
		if (bppItem != null && bppItem.isTransient() && shouldSaveBPPInfo(pOrder)) {

			orderRepository.addItem(RepositoryUtils.getMutableRepositoryItem(bppItem));
		}

		// Here the repository item is updated to the repository. This is done
		// here to catch any Concurrency exceptions.
		if (bppItem != null) {
			try {
				orderRepository.updateItem(bppItem);
			} catch (ConcurrentUpdateException concurrentUpdateException) {
				if (isLoggingError()) {
					logError("ConcurrentUpdateException in ProcTRUSaveBPPInfo.addUpdateBPPInfo method", concurrentUpdateException);
				}
				throw new CommerceException(TRUCommerceConstants.CONCURRENT_UPDATE_ATTEMPT, concurrentUpdateException);
			}
		}

		// Finally, the ChangedProperties Set is cleared and the
		// saveAllProperties property is set to false. This resets
		// the object for more edits. Then the SUCCESS value is returned.
		if (bppItemInfo instanceof ChangedProperties) {
			ChangedProperties cp = (ChangedProperties) bppItemInfo;
			cp.clearChangedProperties();
			cp.setSaveAllProperties(false);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::ProcTRUSaveBPPInfo::@method::addUpdateBPPInfo() : END");
		}
	}

	/**
	 * Should save bpp info.
	 *
	 * @param pOrder the order
	 * @return true, if successful
	 */
	private boolean shouldSaveBPPInfo(Order pOrder) {
		if (pOrder.isTransient()) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	/**
	 * Gets the order tools.
	 *
	 * @return the order tools
	 */
	public TRUOrderTools getOrderTools() {
		return mOrderTools;
	}

	/**
	 * Sets the order tools.
	 *
	 * @param pOrderTools the new order tools
	 */
	public void setOrderTools(TRUOrderTools pOrderTools) {
		mOrderTools = pOrderTools;
	}

	/**
	 * Gets the bpp info property.
	 *
	 * @return the bpp info property
	 */
	public String getBppInfoProperty() {
		return mBppInfoProperty;
	}

	/**
	 * Sets the bpp info property.
	 *
	 * @param pBppInfoProperty the new bpp info property
	 */
	public void setBppInfoProperty(String pBppInfoProperty) {
		mBppInfoProperty = pBppInfoProperty;
	}

	/**
	 * Checks if is enable.
	 *
	 * @return true, if is enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * Sets the enable.
	 *
	 * @param pEnable the new enable
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * Gets the TRU cart modifier helper.
	 * 
	 * @return the TRU cart modifier helper
	 */
	public TRUCartModifierHelper getTRUCartModifierHelper() {
		return mTRUCartModifierHelper;
	}

	/**
	 * Sets the TRU cart modifier helper.
	 * 
	 * @param pTRUCartModifierHelper
	 *            the new TRU cart modifier helper
	 */
	public void setTRUCartModifierHelper(TRUCartModifierHelper pTRUCartModifierHelper) {
		this.mTRUCartModifierHelper = pTRUCartModifierHelper;
	}

}
