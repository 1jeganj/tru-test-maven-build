package com.tru.commerce.order.processor;

import java.beans.IntrospectionException;
import java.util.HashMap;
import java.util.List;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.order.ChangedProperties;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.commerce.order.processor.SavedProperties;
import atg.repository.ConcurrentUpdateException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryUtils;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUOutOfStockItem;

/**
 * The Class ProcTRUSaveOutofStockItems.
 */
public class ProcTRUSaveOutofStockItems extends SavedProperties implements PipelineProcessor {

	/** The Enable. */
	private boolean mEnable;

	/** The Out of stock item info property. */
	private String mOutOfStockItemInfoProperty;
	
	/** The m order tools. */
	private TRUOrderTools mOrderTools;
	
	/** runProcess.
	 * 
	 * @see atg.service.pipeline.PipelineProcessor#runProcess(java.lang.Object, atg.service.pipeline.PipelineResult)
	 * @param pParam - Holds the Param.
	 * @param pResult - Holds the Result.
	 * @throws Exception - Exception if an error occurs.
	 * @return status
	 */
	public int runProcess(Object pParam, PipelineResult pResult) throws Exception {
		if (isEnable()) {
			HashMap map = (HashMap) pParam;
			Order order = (Order) map.get(PipelineConstants.ORDER);
			// Check for null parameters
			if (order == null) {
				throw new InvalidParameterException();
			}
			updateOutofStockItems(order);
		}
		return TRUCommerceConstants.ONE;
	}

	/**
	 * Update outof stock items.
	 * 
	 * @param pOrder
	 *            the order
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws CommerceException
	 *             the commerce exception
	 * @throws IntrospectionException
	 *             the introspection exception
	 * @throws PropertyNotFoundException
	 *             the property not found exception
	 */
	private void updateOutofStockItems(Order pOrder) throws RepositoryException, CommerceException, IntrospectionException,
			PropertyNotFoundException {
		vlogDebug("@Class::TRUSaveOutofStockItems::@method::updateOutofStockItems() : START ");
		MutableRepositoryItem outofStockItem = null;
		MutableRepository orderRepository = (MutableRepository) getOrderTools().getOrderRepository();
		List<TRUOutOfStockItem> outofStockInfos = (List<TRUOutOfStockItem>) DynamicBeans.getPropertyValue(pOrder,
				getOutOfStockItemInfoProperty());

		if (null != outofStockInfos && !outofStockInfos.isEmpty()) {
			for (TRUOutOfStockItem outOfStockInfo : outofStockInfos) {
				boolean hasProperty = false;
				if (DynamicBeans.getBeanInfo(outOfStockInfo).hasProperty(TRUCommerceConstants.REPOSITORY_ITEM)) {
					hasProperty = true;
					// If the outofStockItem mutable item available get it for update
					outofStockItem = (MutableRepositoryItem) DynamicBeans.getPropertyValue(outOfStockInfo,
							TRUCommerceConstants.REPOSITORY_ITEM);
				}

				if (outofStockItem == null && outOfStockInfo.getId() != null) {
					outofStockItem = orderRepository.getItemForUpdate(outOfStockInfo.getId(), getOrderTools()
							.getMappedItemDescriptorName(outOfStockInfo.getClass().getName()));
					if (hasProperty) {
						DynamicBeans.setPropertyValue(outOfStockInfo, TRUCommerceConstants.REPOSITORY_ITEM, outofStockItem);
					}
				}
				// Validating item need to be populated into repository
				if (outofStockItem != null && outofStockItem.isTransient() && shouldSaveOutOfStockItemInfo(pOrder)) {
					orderRepository.addItem(RepositoryUtils.getMutableRepositoryItem(outofStockItem));
				}
				// Here the repository item is updated to the repository. This is done
				// here to catch any Concurrency exceptions.
				if (outofStockItem != null) {
					try {
						orderRepository.updateItem(outofStockItem);
					} catch (ConcurrentUpdateException concurrentUpdateException) {
						if (isLoggingError()) {
							logError("ConcurrentUpdateException in ProcTRUSaveBPPInfo.addUpdateBPPInfo method",
									concurrentUpdateException);
						}
						throw new CommerceException(TRUCommerceConstants.CONCURRENT_UPDATE_ATTEMPT, concurrentUpdateException);
					}
				}
				// Finally, the ChangedProperties Set is cleared and the saveAllProperties property is set to false. This resets
				// the object for more edits. Then the SUCCESS value is returned.
				if (outOfStockInfo instanceof ChangedProperties) {
					ChangedProperties cp = (ChangedProperties) outOfStockInfo;
					cp.clearChangedProperties();
					cp.setSaveAllProperties(false);
				}
			}
		}

		vlogDebug("@Class::TRUSaveOutofStockItems::@method::updateOutofStockItems() : END ");
	}

	/**
	 * Should save out of stock item info.
	 *
	 * @param pOrder the order
	 * @return true, if successful
	 */
	protected boolean shouldSaveOutOfStockItemInfo(Order pOrder) {

		if (pOrder.isTransient()) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	/* (non-Javadoc)
	 * @see atg.service.pipeline.PipelineProcessor#getRetCodes()
	 */
	@Override
	public int[] getRetCodes() {
		int[] ret = { TRUCommerceConstants.ONE };
		return ret;
	}
	
	/**
	 * Gets the order tools.
	 *
	 * @return the order tools
	 */
	public TRUOrderTools getOrderTools() {
		return mOrderTools;
	}

	/**
	 * Sets the order tools.
	 *
	 * @param pOrderTools the new order tools
	 */
	public void setOrderTools(TRUOrderTools pOrderTools) {
		mOrderTools = pOrderTools;
	}

	/**
	 * Gets the out of stock item info property.
	 * 
	 * @return the out of stock item info property
	 */
	public String getOutOfStockItemInfoProperty() {
		return mOutOfStockItemInfoProperty;
	}

	/**
	 * Sets the out of stock item info property.
	 * 
	 * @param pOutOfStockItemInfoProperty
	 *            the new out of stock item info property
	 */
	public void setOutOfStockItemInfoProperty(String pOutOfStockItemInfoProperty) {
		mOutOfStockItemInfoProperty = pOutOfStockItemInfoProperty;
	}

	/**
	 * Checks if is enable.
	 *
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * Sets the enable.
	 *
	 * @param pEnable            the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

}
