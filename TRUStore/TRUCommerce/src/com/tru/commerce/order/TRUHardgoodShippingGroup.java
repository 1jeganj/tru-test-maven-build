package com.tru.commerce.order;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import atg.commerce.order.HardgoodShippingGroup;
import atg.core.util.StringUtils;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.common.TRUConstants;


/**
 * The class TRUHardgoodShippingGroup.
 * @author PA.
 * @version 1.0.
 *
 */
public class TRUHardgoodShippingGroup extends HardgoodShippingGroup {
	
	/**
     * property PROP_IS_GIFT_RECEIPT.
     */
    public static final String PROP_IS_GIFT_RECEIPT = "isGiftReceipt";
    
    /**
     * property PROP_GIFT_WRAP_MSG.
     */
    public static final String PROP_GIFT_WRAP_MSG = "giftWrapMessage";
    
    /**
     * property PROP_SHIPPING_PRICE.
     */
    public static final String PROP_SHIPPING_PRICE = "shippingPrice";
 
	 /**
     * property PROP_REGION_CODE.
     */
    public static final String PROP_REGION_CODE = "regionCode";	
    
    /**
     * property PROP_IS_REGISTRY_ADDRESS.
     */
    public static final String PROP_IS_REGISTRY_ADDRESS = "isRegistryAddress";
    
    /**
     * property PROP_IS_MARKER_SHIPPING_GROUP.
     */
    public static final String PROP_IS_MARKER_SHIPPING_GROUP = "isMarkerShippingGroup";
    
    /**
     * property PROP_ACTUAL_AMOUNT.
     */
    public static final String PROP_ACTUAL_AMOUNT = "actualAmount";
    
    /**
     * property PROP_DEPENDENCY_SHIPPING_GROUP_IDS.
     */
    public static final String PROP_DEPENDENCY_SHIPPING_GROUP_IDS = "dependencyShippingGroupIds";
    
    /**
     * property PROP_SHIPPING_ADDRR_OWNER_ID.
     */
    public static final String PROP_SHIPPING_ADDRR_OWNER_ID = "shippingAddress.ownerId";	
    
    /** The Constant CONSTANT_EMPTY. */
	public static final String CONSTANT_EMPTY = ""; 
	
	private static final long serialVersionUID = -347867323339384378L;
    /**
	 * Constant  to hold double zero.
	 */
	private static final double DOUBLE_ZERO = 0.0;
	
	
	/** The Constant PROP_SHIP_METHOD_NAME. */
	public static final String PROP_SHIP_METHOD_NAME = "shippingMethodName";
	    
	/** The Constant PROP_DELIVERY_TIME_FRAME. */
	public static final String PROP_DELIVERY_TIME_FRAME = "deliveryTimeFrame";
    
	/** The Constant PROP_LAST_ACTIVITY. */
	private Timestamp  mLastActivity;

	/**
	 * @return the nick name.
	 */
	public String getNickName() {
		return (String) getPropertyValue(TRUPropertyNameConstants.NICK_NAME);
	}

	/**
	 * Sets the SAP order id.
	 * 
	 * @param pNickName
	 *            - the nick name
	 */
	public void setNickName(String pNickName) {
		if(StringUtils.isNotBlank(getShipToName())){
			setShipToName(null);
		}		
		setPropertyValue(TRUPropertyNameConstants.NICK_NAME, pNickName);
	}

	/**
	 * @return isGiftReceipt
	 */
	public boolean isGiftReceipt() {
		 return ((boolean)getPropertyValue(PROP_IS_GIFT_RECEIPT));
	}

	/**
	 * @param pGiftReceipt giftReceipt
	 */
	public void setGiftReceipt(boolean pGiftReceipt) {
		setPropertyValue(PROP_IS_GIFT_RECEIPT, pGiftReceipt);
	}

	/**
	 * @return giftWrapMessage
	 */
	public String getGiftWrapMessage() {
		 return ((String)getPropertyValue(PROP_GIFT_WRAP_MSG));
	}

	/**
	 * @param pGiftWrapMessage giftWrapMessage
	 */
	public void setGiftWrapMessage(String pGiftWrapMessage) {
		 setPropertyValue(PROP_GIFT_WRAP_MSG, pGiftWrapMessage);
	}
	
	/**
	 * @return double
	 */
	public double getShippingPrice() {
		Object value=getPropertyValue(PROP_SHIPPING_PRICE);
		if(null!=value){
			return (Double)value;
		}
		return DOUBLE_ZERO;
	}
	/**
	 * @param pShippingPrice shippingPrice
	 */
	public void setShippingPrice(double pShippingPrice) {
		 setPropertyValue(PROP_SHIPPING_PRICE, pShippingPrice);
	}
	/**
	 * @return string
	 */
	public String getRegionCode() {
		Object regionCode=getPropertyValue(PROP_REGION_CODE);
		if(null!=regionCode){
			return (String)regionCode;
		}
		return CONSTANT_EMPTY;
	}
	/**
	 * @param pRegionCode regionCode
	 */
	public void setRegionCode(String pRegionCode) {
		setPropertyValue(PROP_REGION_CODE, pRegionCode);
	}
	/**
	 * 
	 * @return List<String>
	 */
	
	@Override
	public List<String> getPropertyContainerPropertyNames() {
		List<String> propertyContainerPropertyNames = super.getPropertyContainerPropertyNames();
		if (propertyContainerPropertyNames != null && !propertyContainerPropertyNames.isEmpty()) {
			propertyContainerPropertyNames.remove(PROP_SHIPPING_ADDRR_OWNER_ID);
		}
		return super.getPropertyContainerPropertyNames();
	}
	
	/**
	 * @return isRegistryAddress
	 */
	public boolean isRegistryAddress() {
		 return ((boolean)getPropertyValue(PROP_IS_REGISTRY_ADDRESS));
	}

	/**
	 * @param pRegistryAddress the isRegistryAddress to set
	 */
	public void setRegistryAddress(boolean pRegistryAddress) {
		setPropertyValue(PROP_IS_REGISTRY_ADDRESS, pRegistryAddress);
	}
	
	/**
	 * 
	 * @return lastActivity
	 */
	public Timestamp getLastActivity() {
		return mLastActivity;
	}
	
	/**
	 * 
	 * @param pLastActivity the mLastActivity to set
	 */
	public void setLastActivity(Timestamp pLastActivity) {
		mLastActivity=pLastActivity;
	}
	
	/** The Order shipping address. */
	private boolean mOrderShippingAddress;

	/**
	 * @return the orderShippingAddress
	 */
	public boolean isOrderShippingAddress() {
		return mOrderShippingAddress;
	}

	/**
	 * @param pOrderShippingAddress the orderShippingAddress to set
	 */
	public void setOrderShippingAddress(boolean pOrderShippingAddress) {
		mOrderShippingAddress = pOrderShippingAddress;
	}
	
	/** The Billing address is same. */
	private boolean mBillingAddressIsSame;

	/**
	 * @return the billingAddressIsSame
	 */
	public boolean isBillingAddressIsSame() {
		return mBillingAddressIsSame;
	}

	/**
	 * @param pBillingAddressIsSame the billingAddressIsSame to set
	 */
	public void setBillingAddressIsSame(boolean pBillingAddressIsSame) {
		mBillingAddressIsSame = pBillingAddressIsSame;
	}

	/** The Soft deleted. */
	private boolean mSoftDeleted;

	/**
	 * @return the softDeleted
	 */
	public boolean isSoftDeleted() {
		return mSoftDeleted;
	}

	/**
	 * @param pSoftDeleted the softDeleted to set
	 */
	public void setSoftDeleted(boolean pSoftDeleted) {
		mSoftDeleted = pSoftDeleted;
	}

	/**
	 * @return the shippingMethodName
	 */
	public String getShippingMethodName() {
		return (String) getPropertyValue(PROP_SHIP_METHOD_NAME);
	}

	/**
	 * @param pShippingMethodName the shippingMethodName to set
	 */
	public void setShippingMethodName(String pShippingMethodName) {		   
			setPropertyValue(PROP_SHIP_METHOD_NAME, pShippingMethodName);
	}

	/**
	 * @return the deliveryTimeFrame
	 */
	public String getDeliveryTimeFrame() {
		return (String) getPropertyValue(PROP_DELIVERY_TIME_FRAME);
	}

	/**
	 * @param pDeliveryTimeFrame the deliveryTimeFrame to set
	 */
	public void setDeliveryTimeFrame(String pDeliveryTimeFrame) {
		setPropertyValue(PROP_DELIVERY_TIME_FRAME, pDeliveryTimeFrame);
	}
	    
	
	/** The m transient state. */
	private String mTransientState;
	
	

	/**
	 * @return the transientState
	 */
	public String getTransientState() {
		return mTransientState;
	}

	/**
	 * @param pTransientState the mTransientState to set
	 */
	public void setTransientState(String pTransientState) {
		this.mTransientState = pTransientState;
	}
	
	/** The Tax applied quantity. */
	private long mTaxAppliedQuantity;
	
	
	/** The Tax applied amount. */
	private BigDecimal mTaxAppliedAmount;

	/**
	 * @return the taxAppliedAmount
	 */
	public BigDecimal getTaxAppliedAmount() {
		if(mTaxAppliedAmount == null) {
			return new BigDecimal(TRUConstants._0);
		}
		return mTaxAppliedAmount;
	}

	/**
	 * @param pTaxAppliedAmount the taxAppliedAmount to set
	 */
	public void setTaxAppliedAmount(BigDecimal pTaxAppliedAmount) {
		mTaxAppliedAmount = pTaxAppliedAmount;
	}

	/**
	 * @return the taxAppliedQuantity
	 */
	public long getTaxAppliedQuantity() {
		return mTaxAppliedQuantity;
	}

	/**
	 * @param pTaxAppliedQuantity the taxAppliedQuantity to set
	 */
	public void setTaxAppliedQuantity(long pTaxAppliedQuantity) {
		mTaxAppliedQuantity = pTaxAppliedQuantity;
	}
	
	/** The Billing address. */
	private boolean mBillingAddress;

	/**
	 * @return the billingAddress
	 */
	public boolean isBillingAddress() {
		return mBillingAddress;
	}

	/**
	 * @param pBillingAddress the billingAddress to set
	 */
	public void setBillingAddress(boolean pBillingAddress) {
		mBillingAddress = pBillingAddress;
	}
	
	/**
	 * @return the markerShippingGroup
	 */
	public boolean isMarkerShippingGroup() {
		return ((boolean)getPropertyValue(PROP_IS_MARKER_SHIPPING_GROUP));
	}

	/**
	 * @param pMarkerShippingGroup the markerShippingGroup to set
	 */
	public void setMarkerShippingGroup(boolean pMarkerShippingGroup) {
		setPropertyValue(PROP_IS_MARKER_SHIPPING_GROUP, pMarkerShippingGroup);
	}

	/**
	 * @return the dependencyShippingGruoupIds
	 */
	@SuppressWarnings("unchecked")
	public List<String> getDependencyShippingGruoupIds() {
		Object value=getPropertyValue(PROP_DEPENDENCY_SHIPPING_GROUP_IDS);
		if(null!=value){
			return (List<String>)value;
		}
		return new ArrayList<String>();
	}

	/**
	 * @param pDependencyShippingGruoupIds the dependencyShippingGruoupIds to set
	 */
	public void setDependencyShippingGruoupIds(
			List<String> pDependencyShippingGruoupIds) {
		setPropertyValue(PROP_DEPENDENCY_SHIPPING_GROUP_IDS, pDependencyShippingGruoupIds);
	}
	
	/**
	 * @return the actualAmount
	 */
	public Double getActualAmount() {
		Object value=getPropertyValue(PROP_ACTUAL_AMOUNT);
		if(null!=value){
			return (Double)value;
		}
		return DOUBLE_ZERO;
	}

	/**
	 * @param pActualAmount the actualAmount to set
	 */
	public void setActualAmount(Double pActualAmount) {
		setPropertyValue(PROP_ACTUAL_AMOUNT, pActualAmount);
	}
	
	/**
	 * Gets the ship to name.
	 *
	 * @return the mShipToName
	 */
	public String getShipToName() {
		return (String)getPropertyValue(TRUCommerceConstants.PROP_SHIP_TO_NAME);
	}

	/**
	 * Sets the ship to name.
	 *
	 * @param pShipToName the new ship to name
	 */
	public void setShipToName(String pShipToName) {
		setPropertyValue(TRUCommerceConstants.PROP_SHIP_TO_NAME, pShipToName);
	}
	
}
