package com.tru.commerce.order;

import com.tru.commerce.TRUCommerceConstants;



/**
 * The Class TRUDonationCommerceItem.
 * @author PA
 * @version 1.0
 */
public class TRUDonationCommerceItem extends TRUCommerceItemImpl {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Gets the donation amount.
	 *
	 * @return the donation amount
	 */
	public Double getDonationAmount() {
		return (Double) getPropertyValue(TRUCommerceConstants.DONATION_AMOUNT);
	}
	
	/**
	 * Sets the donation amount.
	 *
	 * @param pDonationAmount the new donation amount
	 */
	public void setDonationAmount(Double pDonationAmount) {
		setPropertyValue(TRUCommerceConstants.DONATION_AMOUNT, pDonationAmount);
	}
}
