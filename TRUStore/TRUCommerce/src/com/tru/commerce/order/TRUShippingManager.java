package com.tru.commerce.order;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.Relationship;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.Address;
import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.order.purchase.TRUPurchaseProcessException;
import com.tru.commerce.order.utils.TRURegionConstants;
import com.tru.commerce.pricing.TRUShipMethodComparator;
import com.tru.commerce.pricing.TRUShipMethodVO;
import com.tru.commerce.pricing.TRUShipRegionVO;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;
import com.tru.radial.commerce.payment.paypal.TRUPaypalConstants;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.userprofiling.TRUCookieManager;

/**
 * TRUShippingManager - Extensions to GenericService.
 *
 * @author Professional Access
 * @version 1.0.
 */
public class TRUShippingManager extends GenericService {
	
	
	/** The Constant PO_BOX. */
	private static final String PO_BOX="PO_BOX";
	
	/** The Pay pal shipping valid countries. */
	private List<String> mPayPalShippingValidCountries;

	/** Tools for ShippingRepository. */
	private TRUShippingTools mShippingTools;

	/** Property Manager. */
	private TRUCommercePropertyManager mPropertyManager;
	
	
	

	/**
	 * Holds the configurable value for mExcludeShipHolidays
	 */
	private boolean mExcludeShipHolidays;


	/** The m catalog tools. */
	private TRUCatalogTools mCatalogTools;
	
	/**
	 * Gets the catalog tools.
	 *
	 * @return the catalog tools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Sets the catalog tools.
	 *
	 * @param pCatalogTools the new catalog tools
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		this.mCatalogTools = pCatalogTools;
	}


	/**
	 * Gets the shipping tools.
	 *
	 * @return shippingTools
	 */
	public TRUShippingTools getShippingTools() {
		return mShippingTools;
	}


	/**
	 * Sets the shipping tools.
	 *
	 * @param pShippingTools the shippingTools to set
	 */
	public void setShippingTools(TRUShippingTools pShippingTools) {
		this.mShippingTools = pShippingTools;
	}

	
	
	/**
	 * getter for property manager.
	 * @return propertyManager
	 */
	public TRUCommercePropertyManager getPropertyManager() {
		return mPropertyManager;
	}


	/**
	 * Sets the property manager.
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUCommercePropertyManager pPropertyManager) {
		this.mPropertyManager = pPropertyManager;
	}
	
	/**
	 * Gets the pay pal shipping valid countries.
	 *
	 * @return the payPalShippingValidCountries
	 */
	public List<String> getPayPalShippingValidCountries() {
		return mPayPalShippingValidCountries;
	}

	/**
	 * Sets the pay pal shipping valid countries.
	 *
	 * @param pPayPalShippingValidCountries the payPalShippingValidCountries to set
	 */
	public void setPayPalShippingValidCountries(
			List<String> pPayPalShippingValidCountries) {
		mPayPalShippingValidCountries = pPayPalShippingValidCountries;
	}
	
	/** Holds the ak,hi idetifier list. */
	private List<String> mAKHIIdentifierList;
	
	/** Holds apo,fpo identifiers list. */
	private List<String> mAPOFPOIdentifierList;
	
	/** Holds po box identifiers list. */
	private List<String> mPOBOXIdentifierList;
		
	/** Holds us territory identifiers list. */
	private List<String> mUSTerritoryIdentifierList;
	
	/** Holds TRU_SHPA Restricted Identifiers. */
	private List<String> mTRUSHPARestrictedIdentifiers;
	
	
	
	/**  This flag holds the EDD flag value *. */
	private boolean mEddEnabled;
	
	/**  Hold the edd cutoff time. */
	private int mEddCutOffTime;
	
	/**  Property to hold mTimeInTransitShipMethodCodes *. */
	private List<String> mTimeInTransitShipMethodCodes;
	
	/**
	 * Getter for mAKHIIdentifierList.
	 *
	 * @return aKHIIdentifierList
	 */
	public List<String> getAKHIIdentifierList() {
		return mAKHIIdentifierList;
	}

	/**
	 * setter for mAKHIIdentifierList.
	 *
	 * @param pAKHIIdentifierList the aKHIIdentifierList to set
	 */
	public void setAKHIIdentifierList(List<String> pAKHIIdentifierList) {
		mAKHIIdentifierList = pAKHIIdentifierList;
	}

	/**
	 * Getter for mAPOFPOIdentifierList.
	 *
	 * @return aPOFPOIdentifierList
	 */
	public List<String> getAPOFPOIdentifierList() {
		return mAPOFPOIdentifierList;
	}

	/**
	 * Setter for mAPOFPOIdentifierList.
	 *
	 * @param pAPOFPOIdentifierList the aPOFPOIdentifierList to set
	 */
	public void setAPOFPOIdentifierList(List<String> pAPOFPOIdentifierList) {
		mAPOFPOIdentifierList = pAPOFPOIdentifierList;
	}

	/**
	 * Getter for mPOBOXIdentifierList.
	 *
	 * @return pOBOXIdentifierList
	 */
	public List<String> getPOBOXIdentifierList() {
		return mPOBOXIdentifierList;
	}

	/**
	 * Setter for mPOBOXIdentifierList.
	 *
	 * @param pOBOXIdentifierList the oBOXIdentifierList to set
	 */
	public void setPOBOXIdentifierList(List<String> pOBOXIdentifierList) {
		mPOBOXIdentifierList = pOBOXIdentifierList;
	}

	/**
	 * Getter for mUSTerritoryIdentifierList.
	 *
	 * @return uSTerritoryIdentifierList
	 */
	public List<String> getUSTerritoryIdentifierList() {
		return mUSTerritoryIdentifierList;
	}

	/**
	 * Sets the US territory identifier list.
	 *
	 * @param pUSTerritoryIdentifierList the uSTerritoryIdentifierList to set
	 */
	public void setUSTerritoryIdentifierList(List<String> pUSTerritoryIdentifierList) {
		mUSTerritoryIdentifierList = pUSTerritoryIdentifierList;
	}		
	
	/**
	 * Getter for mTRU_SHPARestrictedIdentifiers.
	 *
	 * @return TRU_SHPARestrictedIdentifiers
	 */
	public List<String> getTRUSHPARestrictedIdentifiers() {
		return mTRUSHPARestrictedIdentifiers;
	}


	/**
	 * Setter for mTRU_SHPARestrictedIdentifiers.
	 *
	 * @param pTRU_SHPARestrictedIdentifiers TRU_SHPARestrictedIdentifiers
	 */
	public void setTRUSHPARestrictedIdentifiers(List<String> pTRU_SHPARestrictedIdentifiers) {
		mTRUSHPARestrictedIdentifiers = pTRU_SHPARestrictedIdentifiers;
	}
	
	
	/**
	 * Setter for mExcludeShipHolidays.
	 *
	 * @return true, if is exclude ship holidays
	 */
	public boolean isExcludeShipHolidays() {
		return mExcludeShipHolidays;
	}

	/**
	 *  Getter for mExcludeShipHolidays.
	 *
	 * @param pExcludeShipHolidays the new exclude ship holidays
	 */
	public void setExcludeShipHolidays(boolean pExcludeShipHolidays) {
		this.mExcludeShipHolidays = pExcludeShipHolidays;
	}
	

	/**
	 * <p>
	 *	This method checks the item level shipping restrictions to which zipcode is not allowed.
	 *	Return a map where key is commerceItemId and value is the error message.
	 *  Return empty map if no zipcode restrictions found.
	 * </p>
	 * @param pOrder order
	 * @return restrctionsMap
	 */
	@SuppressWarnings("unchecked")
	public Map<String,String> validateZipCodeRestriction(Order pOrder ){
		if(isLoggingDebug()){
			vlogDebug("Start TRUShippingManager.validateZipCodeRestriction() for Order {0}",pOrder.getId());
		}		
		final Map<String, String> restrctionsMap=new HashMap<String, String>();
		HardgoodShippingGroup hardgoodShippingGroup=null;
		ShippingGroup shippingGroup=null;
		final List<CommerceItem> commerceItems=pOrder.getCommerceItems();
		if(commerceItems!=null && !commerceItems.isEmpty()){
			for(CommerceItem citem:commerceItems){	
				if(null!=citem){
					final List<Relationship> cmSgRelList = citem.getShippingGroupRelationships();
					for(Relationship cmSgRel:cmSgRelList){
						if(cmSgRel instanceof ShippingGroupCommerceItemRelationship){
							shippingGroup = ((ShippingGroupCommerceItemRelationship) cmSgRel).getShippingGroup();
							if(shippingGroup instanceof HardgoodShippingGroup){
								hardgoodShippingGroup=(HardgoodShippingGroup)shippingGroup;
								if(!(citem instanceof TRUDonationCommerceItem ||citem instanceof TRUGiftWrapCommerceItem)){
									getRestrctionError(restrctionsMap, hardgoodShippingGroup, citem);
								}													
							}
						}
						
					}
				}					
			}
		}
		if(!restrctionsMap.isEmpty()){
				vlogInfo("Zipcode Restrctions found and restrctionsMap is {0} for order {1}", restrctionsMap,pOrder.getId());	
		}
				
		if(isLoggingDebug()){
			vlogDebug("End TRUShippingManager.validateZipCodeRestriction() for Order {0}",pOrder.getId());
		}
		return restrctionsMap;
	}



	/**
	 * Gets the restrction error.
	 *
	 * @param pRestrctionsMap the restrctions map
	 * @param pHardgoodShippingGroup the hardgood shipping group
	 * @param pCitem the citem
	 */
	private void getRestrctionError(final Map<String, String> pRestrctionsMap, HardgoodShippingGroup pHardgoodShippingGroup, CommerceItem pCitem) {
		String shipToZipCode=pHardgoodShippingGroup.getShippingAddress().getPostalCode();
		if(StringUtils.isNotBlank(shipToZipCode) && StringUtils.isNotBlank(pCitem.getCatalogRefId())){
			shipToZipCode = shipToZipCode.trim();
			String skuId=pCitem.getCatalogRefId().trim();
			if(shipToZipCode.length()>TRUConstants.FIVE) {
				shipToZipCode = shipToZipCode.replaceAll(TRUConstants.IFAN, TRUConstants.EMPTY);
				shipToZipCode = shipToZipCode.substring(TRUConstants.ZERO, TRUConstants.FIVE);
			}
			String displayName = TRUConstants.EMPTY;
			String errorMsg = getShippingTools().checkSKUZipCodeRestriction(skuId, shipToZipCode);
			if (isLoggingDebug()) {
				vlogDebug("Is zipcode:{0} restriction found for catalogRefId:{1} ? {2}", shipToZipCode,
						pCitem.getCatalogRefId(), (StringUtils.isBlank(errorMsg) ? false : true));
			}		
			try {
				RepositoryItem skuItem= getCatalogTools().findSKU(skuId);			
				displayName = (String)skuItem.getPropertyValue(getPropertyManager().getDisplayNamePropertyName());
			} catch (RepositoryException e) {
				if(isLoggingError()){
					vlogError("RepositoryException while getting displayName for the catalogId {0}",skuId);
				}
			}
			
			if(StringUtils.isNotBlank(errorMsg)){
				pRestrctionsMap.put(pCitem.getCatalogRefId()+TRUConstants.IFAN+shipToZipCode, displayName+TRUConstants.WHITE_SPACE+errorMsg)	;
			}
		}
		
	}
	
	
	/**
	 * <p>
	 * This method will check whether any restriction found for ship method and
	 * non eligible shipping addresses in both combination .
	 * </p>
	 * 
	 * @param pOrder
	 *            order
	 * @return restrctionsMap
	 */
	@SuppressWarnings("unchecked")
	public Map<String, String> validateShipMethodRestrction(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("Start TRUShippingManager.validateShipMethodRestrction() for Order {0}",pOrder.getId());
		}
		final Map<String, String> restrctionsMap = new HashMap<String, String>(TRUConstants.ONE);
		boolean validationRequired = false;
		TRUHardgoodShippingGroup CIShippingGroup = null;
		final List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		if (shippingGroups != null && !shippingGroups.isEmpty()) {
			for (ShippingGroup shippingGroup : shippingGroups) {
				validationRequired = false;
				List commerceItemRelationships = shippingGroup.getCommerceItemRelationships();
				for (Object object : commerceItemRelationships) {
					ShippingGroupCommerceItemRelationship sgCIR = (ShippingGroupCommerceItemRelationship) object;
					if(!(sgCIR.getCommerceItem() instanceof  TRUDonationCommerceItem || sgCIR.getCommerceItem() instanceof TRUGiftWrapCommerceItem)) {
						validationRequired = true;
						break;
					}
				}
				if (validationRequired && shippingGroup instanceof TRUHardgoodShippingGroup) {
					CIShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
					if (CIShippingGroup != null && CIShippingGroup.getCommerceItemRelationshipCount() > 0) {
						final Address address = CIShippingGroup.getShippingAddress();
						if (isLoggingDebug()) {
							vlogDebug("Shipping state ={0} ,  address1={1}, and country={2}", address.getState(),
									address.getAddress1(), address.getCountry());
						}
						if(StringUtils.isEmpty(CIShippingGroup.getShippingMethod())){
							if(isLoggingInfo()){
								vlogInfo("No Shipmethod found for {0}",CIShippingGroup.getId());
							}
							String errorMessage=getErrorMessage(TRUPaypalConstants.INVALID_PAYPAL_SHIPPING_ADDRESS);
							restrctionsMap.put(TRUPaypalConstants.INVALID_PAYPAL_SHIPPING_ADDRESS,errorMessage);
						}else if (TRUConstants.ECONOMY_SHIP_METHOD_CODE.equals(CIShippingGroup.getShippingMethod()) &&
								 checkShipMethodRestrictionForState(address.getState(), CIShippingGroup.getShippingMethod())) {
							String errorMessage=getErrorMessage(TRUErrorKeys.TRU_ERROR_ECONOMY_SHIP_METHOD_RESTRICTION_TO_ADDRESS);	
							restrctionsMap.put(CIShippingGroup.getId(),errorMessage);
							errorMessage=null;
						} else if (TRUConstants.FREIGHT_SHIP_METHOD_CODE.equals(CIShippingGroup.getShippingMethod()) &&
								 checkShipMethodRestrictionForAddress1(address.getAddress1(),
										CIShippingGroup.getShippingMethod())) {
							String errorMessage=getErrorMessage(TRUErrorKeys.TRU_ERROR_SHPTRUCK_SHIP_METHOD_RESTRICTION_TO_ADDRESS);
							restrctionsMap.put(CIShippingGroup.getId(), errorMessage);
							errorMessage=null;
						}
						if (!getPayPalShippingValidCountries().contains(address.getCountry())) {
							restrctionsMap.put(CIShippingGroup.getId(), getPayPalRestrictionErrMsg());
						}
					}
				}
			}
		}
		if(!restrctionsMap.isEmpty()){
				vlogInfo("Shipmethod Restrctions Map is {0} for Order {1}", restrctionsMap,pOrder.getId());
		}		
		if (isLoggingDebug()) {
			vlogDebug("End TRUShippingManager.validateShipMethodRestrction() for Order {0}",pOrder.getId());
		}
		return restrctionsMap;
	}

	/**
	 * <p>
	 * 	This method will check whether any shipping restriction found for TRU_SHPA item.
	 * </p>
	 * @param pOrder order
	 * @return restrctionsMap
	 */
	@SuppressWarnings("unchecked")
	public Map<String, String> validateFreightRestrction(Order pOrder) {
		if(isLoggingDebug()){
			vlogDebug("Start TRUShippingManager.validateFreightRestrction() for Order {0}",pOrder.getId());
		}		
		final List<CommerceItem> commerceItems = ((TRUOrderImpl)pOrder).getS2HItems();
		final Map<String, String> restrctionsMap = new HashMap<String, String>();
		ShippingGroup shippingGroup = null;
		HardgoodShippingGroup hardgoodShippingGroup = null;
		if (commerceItems != null && !commerceItems.isEmpty()) {
			for (CommerceItem commerceItem : commerceItems) {
				final String freightClass = getFreightClassFromCI(commerceItem);
				if (StringUtils.isNotBlank(freightClass) &&
						 getFreightClassTRUSHPA().equalsIgnoreCase(freightClass)) {
					final List<Relationship> cmSgRelList = commerceItem.getShippingGroupRelationships();
					for (Relationship cmSgRel : cmSgRelList) {
						if (cmSgRel instanceof ShippingGroupCommerceItemRelationship) {
							shippingGroup = ((ShippingGroupCommerceItemRelationship) cmSgRel).getShippingGroup();
							if (shippingGroup instanceof HardgoodShippingGroup) {
								hardgoodShippingGroup = (HardgoodShippingGroup) shippingGroup;
								final String shipToZipCode = hardgoodShippingGroup.getShippingAddress().getPostalCode();
								final String address1 = hardgoodShippingGroup.getShippingAddress().getAddress1();
								final boolean isTRUSHPARestrictedAddress = isTRUSHPARestrictedAddress(address1);
								vlogDebug("Is freight:{0} restriction found for for the address1 :  address1={1} ? {2}",
										freightClass, address1, isTRUSHPARestrictedAddress);
								if (isTRUSHPARestrictedAddress) {
									String errorMessage=getErrorMessage(TRUErrorKeys.TRU_ERROR_FREIGHT_CLASS_RESTRICTION_TO_ADDRESS);									
									restrctionsMap.put(commerceItem.getCatalogRefId() + TRUConstants.IFAN + shipToZipCode,
											errorMessage);
									break;
								}
							}
						}

					}
				}
			}
		}
		
		if(!restrctionsMap.isEmpty()){
				vlogInfo("Found Freight's restriction , restrctionsMap is {0} for the order {1}", restrctionsMap,pOrder.getId());
		}
		if(isLoggingDebug()){
			vlogDebug("End TRUShippingManager.validateFreightRestrction() for Order {0}",pOrder.getId());
		}
		return restrctionsMap;
	}

	
	/**
	 * <p>
	 * 		This method will check whether the ship method is restricted for specific state or not? 
	 * </p>.
	 *
	 * @param pShipToState shipToState
	 * @param pShipMethod shipMethod
	 * @return boolean
	 */
	private boolean checkShipMethodRestrictionForState(String pShipToState, String pShipMethod) {
		if (isLoggingDebug()) {
			logDebug("Start checkShipMethodRestrictionForState()");
		}
		boolean isShipMethodRestrictionExists = false;
		final Set<String> nonShippableState = getShippingTools().getNonQualifiedRegionsByShipMethod(pShipMethod);
		if (nonShippableState != null && !nonShippableState.isEmpty() && !StringUtils.isEmpty(pShipToState)
				&& nonShippableState.contains(pShipToState)) {
			isShipMethodRestrictionExists = true;
		}
		if (isLoggingInfo()) {
			vlogInfo("{0} is eligible shipmethod for pShipToState {1}? {2}", pShipMethod,pShipToState,isShipMethodRestrictionExists);
		}
		if (isLoggingDebug()) {
			logDebug("End checkShipMethodRestrictionForState()");
		}
		return isShipMethodRestrictionExists;
	}
	
	
	/**
	 * <p>
	 * 		This method will check whether the ship method is restricted for specific address1 or not? 
	 * </p>.
	 *
	 * @param pAddress1 address1
	 * @param pShipMethod shipMethod
	 * @return boolean
	 */
	private boolean checkShipMethodRestrictionForAddress1(String pAddress1, String pShipMethod) {
		if(isLoggingDebug()){
			logDebug("Start checkShipMethodRestrictionForAddress1()");
		}
		boolean isShipMethodRestrictionExists=false;
		final Set<String> nonShippableState=getShippingTools().getNonQualifiedRegionsByShipMethod(pShipMethod) ;
		if(nonShippableState!=null && !nonShippableState.isEmpty() && !StringUtils.isEmpty(pAddress1)){						
			for(String identifier:nonShippableState){
				if(pAddress1.contains(identifier)){
					isShipMethodRestrictionExists= true;
					break;
				}
			}						
		}
		if(isLoggingInfo()){
			vlogInfo("{0} is eligible shipmethod for pAddress1 {1} ? {2}", pShipMethod,pAddress1,isShipMethodRestrictionExists);
		}
		if(isLoggingDebug()){
			logDebug("End checkShipMethodRestrictionForAddress1()");
		}
		return isShipMethodRestrictionExists;
	}
	
	
	
	/**
	 * <p>
	 * 	This method retrieves all the eligible shipping methods for the cart items .
	 *   
	 * </p>
	 * @param pFreightClassesSet freightClassesSet
	 *  @param pRegionCode regionCode
	 *   @param pOrder order
	 * @return methodVOs
	 */
	public List<TRUShipMethodVO> getShippingMethods(Set<String> pFreightClassesSet, String pRegionCode, Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("Start TRUShippingManager.getShippingMethods() for FreightClasses {0} , region {1} and Order {2} " , pFreightClassesSet ,pRegionCode,pOrder.getId());
		}
		if (pFreightClassesSet != null && pFreightClassesSet.size() > TRUConstants.SIZE_ONE &&
				 pFreightClassesSet.contains(getFreightClassShiptruck())) {
			pFreightClassesSet.remove(getFreightClassShiptruck());
		}
		final RepositoryItem[] items = getShippingTools().getShipMethodsByFreighs(pFreightClassesSet);
		Set<TRUShipMethodVO> methodVOs = new HashSet<TRUShipMethodVO>();

		if (null != items && items.length > 0) {
			methodVOs = createShipMethodVOSet(items, pRegionCode, pOrder);
			if (isLoggingDebug()) {
				logDebug("==================================Start : Final Eligible ShipMethods======================");
				for (TRUShipMethodVO methodVO : methodVOs) {
					logDebug(pFreightClassesSet + "|" + methodVO.getShipRegionVO().getRegionName() + "==>" +
							 methodVO.getShipMethodCode() + "(" + methodVO.getShipMethodName() + ")," +
							 methodVO.getShippingPrice() + "," + methodVO.getDelivertTimeLine() /*+ "," + methodVO.getDetails()*/);
				}
				logDebug("==================================End : Final Eligible ShipMethods======================");
			}
		}
		if (isLoggingDebug()) {
			logDebug("End TRUShippingManager.getShippingMethods()");
		}
		return sortShipMethods(methodVOs);
	}
		
	
	/**<p>
	 * This method sorts the shipping methods based on price value.
	 * </p>
	 * @param pApplicableShipMethods applicableShipMethods
	 * @return collection
	 */
	private List<TRUShipMethodVO> sortShipMethods(Set<TRUShipMethodVO> pApplicableShipMethods) {
		if(pApplicableShipMethods!=null && !pApplicableShipMethods.isEmpty()){
			final List<TRUShipMethodVO> list=new ArrayList<TRUShipMethodVO>(pApplicableShipMethods);
			Collections.sort(list, new TRUShipMethodComparator());
			return list;
		}
		return Collections.emptyList();
	}

	/**<p>
	 * This method will get freightClass value from the CI.
	 * </p>
	 * @param pCommerceItem commerceItem
	 * @return string
	 */
	public String getFreightClassFromCI(CommerceItem pCommerceItem) {
		final RepositoryItem skuItem= (RepositoryItem) pCommerceItem.getAuxiliaryData().getCatalogRef();
		final String type = (String) skuItem.getPropertyValue(getPropertyManager().getTypePropertyName());
		if(!type.equalsIgnoreCase(TRUConstants.NON_MERCH_SKU)){
			final String freightClassPropVal=(String)skuItem.getPropertyValue(getPropertyManager().getFreightClassPropertyName());
			return freightClassPropVal;
		}else{
			return null;
		}
	}
		
	
	
	
	
	/**
	 * <p>
	 * This method parse the reositoryitem to VO objects.
	 * </p>
	 * @param pShipMethodItems shipMethodItems
	 * @param pRegionName regionName
	 * @param pOrder pOrder
	 * @return shipMethodVOs
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	private Set<TRUShipMethodVO> createShipMethodVOSet(RepositoryItem[] pShipMethodItems, String pRegionName, Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("Start TRUShippingManager.createShipMethodVOSet() for pRegionName : {0}", pRegionName);
		}
		String shipMethodId = null;
		String shipMethodName = null;
		String shipMethodCode = null;
		String details = null;
		final Set<TRUShipMethodVO> shipMethodVOs = new HashSet<TRUShipMethodVO>();
		Set<RepositoryItem> regionItems = null;
		Set<String> restrictedStates = null;
		TRUShipMethodVO shipMethodVO = null;
		TRUShipRegionVO shipRegionVO = null;
		for (RepositoryItem item : pShipMethodItems) {
			regionItems = (Set<RepositoryItem>) item.getPropertyValue(getPropertyManager().getEligibleShipRegionsPropertyName());
			if (regionItems != null && !regionItems.isEmpty()) {
				shipRegionVO = getMatchedShipRegionAndPrice(regionItems, pRegionName);
				if (null != shipRegionVO) {
					shipMethodVO = createShipMethodVO(item);
					shipMethodVO.setShipRegionVO(shipRegionVO);
					shipMethodVO.setShippingPrice(getShippingPrice(pOrder, shipRegionVO));
					shipMethodVO.setDelivertTimeLine(shipRegionVO.getDeliveryMinDays() + TRUConstants.IFAN
							+ shipRegionVO.getDeliveryMaxDays() + TRUConstants.WHITE_SPACE + getBusinessDaysText());
					shipMethodVO.setRegionCode(shipRegionVO.getRegionName());
					shipMethodVOs.add(shipMethodVO);
				}

			}

		}
		if (isLoggingDebug()) {
			vlogDebug("Total Ship Methods {0}", shipMethodVOs.size());
			vlogDebug("End TRUShippingManager.createShipMethodVOSet() ");
		}
		return shipMethodVOs;
	}


	
	
	/**
	 * <p>
	 * 	This method will identify and return shipping prices for customer entered address.
	 * </p>
	 * @param pShipRegionItems shipRegionItems
	 * @param pRegionName regionName
	 * @return TRUShipRegionVO
	 */
	public TRUShipRegionVO getMatchedShipRegionAndPrice(Set<RepositoryItem> pShipRegionItems, String pRegionName) {
		if (pShipRegionItems != null && !pShipRegionItems.isEmpty()) {
			for (RepositoryItem shipRegionItem : pShipRegionItems) {
				final String regionName = (String) shipRegionItem.getPropertyValue(getPropertyManager()
						.getShipRegionNamePropertyName());
				if (!StringUtils.isEmpty(pRegionName) && !StringUtils.isEmpty(regionName)
						&& pRegionName.equalsIgnoreCase(regionName)) {
					return createShipRegionVO(shipRegionItem, regionName);
				}
			}
		}
		return null;
	}


	/**
	 * Creates the ship region vo.
	 *
	 * @param pShipRegionItem shipRegionItem
	 * @param pRegionName regionName
	 * @return TRUShipRegionVO
	 */
	private TRUShipRegionVO createShipRegionVO(RepositoryItem pShipRegionItem, String pRegionName) {
		if(isLoggingDebug()){
			logDebug("Start TRUShippingManager.createShipRegionVO() ");
		}
		final TRUShipRegionVO shipRegionVO=new TRUShipRegionVO();		
		shipRegionVO.setRegionName(pRegionName);
		final double orderCutOff=(Double)pShipRegionItem.getPropertyValue(getPropertyManager().getOrderCutOffPricePropertyName());
		
		// updating delivery min and max days
		int deliveryMinDays = TRUConstants.ZERO;
		if (pShipRegionItem.getPropertyValue(getPropertyManager().getDeliveryTimeMinDaysPropertyName()) != null) {
			deliveryMinDays = (Integer)pShipRegionItem.getPropertyValue(getPropertyManager().getDeliveryTimeMinDaysPropertyName());
		}
		int deliveryMaxDays = TRUConstants.ZERO;
		if (pShipRegionItem.getPropertyValue(getPropertyManager().getDeliveryTimeMaxDaysPropertyName()) != null) {
			deliveryMaxDays = (Integer)pShipRegionItem.getPropertyValue(getPropertyManager().getDeliveryTimeMaxDaysPropertyName());
		}
		
		// updating time in transit min and max days
		int timeInTransitMinDays = TRUConstants.ZERO;
		if (pShipRegionItem.getPropertyValue(getPropertyManager().getTimeInTransitMinDaysPropertyName()) != null) {
			timeInTransitMinDays = (Integer)pShipRegionItem.getPropertyValue(getPropertyManager().getTimeInTransitMinDaysPropertyName());
		}
		int timeInTransitMaxDays = TRUConstants.ZERO;
		if (pShipRegionItem.getPropertyValue(getPropertyManager().getTimeInTransitMaxDaysPropertyName()) != null) {
			timeInTransitMaxDays = (Integer)pShipRegionItem.getPropertyValue(getPropertyManager().getTimeInTransitMaxDaysPropertyName());
		}
		
		final double shipMinPrice=(Double)pShipRegionItem.getPropertyValue(getPropertyManager().getShipMinPricePropertyName());
		final double shipMaxPrice=(Double)pShipRegionItem.getPropertyValue(getPropertyManager().getShipMaxPricePropertyName());
		shipRegionVO.setShipMinPrice(shipMinPrice);
		shipRegionVO.setShipMaxPrice(shipMaxPrice);
		shipRegionVO.setDeliveryMinDays(deliveryMinDays);
		shipRegionVO.setDeliveryMaxDays(deliveryMaxDays);
		shipRegionVO.setTimeInTransitMinDays(timeInTransitMinDays);
		shipRegionVO.setTimeInTransitMaxDays(timeInTransitMaxDays);
		shipRegionVO.setOrderCutOff(orderCutOff);
		if(isLoggingDebug()){
			logDebug("End TRUShippingManager.createShipRegionVO() ");
		}
		return shipRegionVO;
	}
	
	
	
	/**
	 * <p>
	 * 
	 * </p>.
	 *
	 * @param pOrder order
	 * @param pRegionVO regionVO
	 * @return shippingPrice
	 */
	private double getShippingPrice(Order pOrder,TRUShipRegionVO pRegionVO ){
		double rawSubtotal = TRUConstants.DOUBLE_ZERO;
		double shippingPrice= TRUConstants.DOUBLE_ZERO;
		if (pOrder != null && pOrder.getPriceInfo() != null) {
			rawSubtotal = pOrder.getPriceInfo().getRawSubtotal();
		}		
		if(rawSubtotal<pRegionVO.getOrderCutOff()){
			shippingPrice= pRegionVO.getShipMaxPrice();
		}if(rawSubtotal>=pRegionVO.getOrderCutOff()){
			shippingPrice= pRegionVO.getShipMinPrice();
		}
		if(isLoggingDebug()){
			vlogDebug("Shipping price is {0} for order value {1}",shippingPrice,rawSubtotal);
		}		
		return shippingPrice;
	}
	
	/**
	 * <p>
	 * This method parses the RI to VO.
	 * </p>
	 * @param pShipMethodItem shipMethodItem
	 * @return methodVO
	 */
	@SuppressWarnings("unchecked")
	private TRUShipMethodVO createShipMethodVO(RepositoryItem pShipMethodItem) {
		TRUShipMethodVO methodVO = null;
		if (null != pShipMethodItem) {
			final String shipMethodId = pShipMethodItem.getRepositoryId();
			final String shipMethodCode = (String) pShipMethodItem.getPropertyValue(getPropertyManager()
					.getShipMethodCodePropertyName());
			final String shipMethodName = (String) pShipMethodItem.getPropertyValue(getPropertyManager()
					.getShipMethodNamePropertyName());
			final String details = (String) pShipMethodItem.getPropertyValue(getPropertyManager().getDetailsPropertyName());
			methodVO = new TRUShipMethodVO(shipMethodId, shipMethodCode, shipMethodName, details);
			final Set<String> nonQualifiedAreas = (Set<String>) pShipMethodItem.getPropertyValue(getPropertyManager()
					.getNonQualifiedStateCodesPropertyName());
			final Set<String> freightClasses = (Set<String>) pShipMethodItem.getPropertyValue(getPropertyManager()
					.getEligibleFreightClassesPropertyName());
			methodVO.setNonQualifiedAreas(nonQualifiedAreas);
			methodVO.setFreightClasses(freightClasses);

		}
		return methodVO;
	}
	
	


	
	/**
	 * <p>
	 * This method gets all the shipMethods for the given freightClass value.
	 * </p>
	 * @param pFreightName freightName
	 * @param pRegionCode regionCode
	 * @param pOrder order
	 * @param pRestrictionFlag restrictionFlag
	 * @return applicableShipMethodsSet
	 */
	public List<TRUShipMethodVO> getShipMethods(final String pFreightName, String pRegionCode, Order pOrder,
			boolean pRestrictionFlag) {
		if (isLoggingDebug()) {
			logDebug("Start TRUShippingManager.getShipMethods() for freightName :" + pFreightName);
		}
		final Set<String> freightNameSet = new HashSet<String>(TRUConstants.SIZE_ONE);
		freightNameSet.add(pFreightName);
		final List<TRUShipMethodVO> eligibleShipMethods = getShippingMethods(freightNameSet, pRegionCode, pOrder);
		if (pRestrictionFlag) {
			removeRetrictedShipMethods(pRegionCode, eligibleShipMethods);
		}
		if (isLoggingDebug()) {
			logDebug("Eligible ShipMethods List" + eligibleShipMethods + " For Freight " + pFreightName);
			logDebug("End TRUShippingManager.getShipMethods()");
		}
		return eligibleShipMethods;
	}

	
	/**
	 * <p>
	 * 	This method will remove  the restricted shipping methods from the eligible shipping methods based on the shipping region.
	 * </p>
	 * @param pRegionCode regionCode
	 * @param pApplicableShipMethodsSet applicableShipMethodsSet
	 */
	private void removeRetrictedShipMethods(String pRegionCode, List<TRUShipMethodVO> pApplicableShipMethodsSet) {
		if (!StringUtils.isEmpty(pRegionCode) && TRURegionConstants.AK_HI.getRegion().equalsIgnoreCase(pRegionCode)) {
			if (pApplicableShipMethodsSet != null && pApplicableShipMethodsSet.contains(TRUConstants.ECONOMY_SHIP_METHOD_CODE)) {
				pApplicableShipMethodsSet.remove(TRUConstants.ECONOMY_SHIP_METHOD_CODE);
			}
		} else if (!StringUtils.isEmpty(pRegionCode) && TRURegionConstants.APO_FPO.getRegion().equalsIgnoreCase(pRegionCode)) {
			if (pApplicableShipMethodsSet != null && pApplicableShipMethodsSet.contains(TRUConstants.FREIGHT_SHIP_METHOD_CODE)) {
				pApplicableShipMethodsSet.remove(TRUConstants.FREIGHT_SHIP_METHOD_CODE);
			}
		} else if (!StringUtils.isEmpty(pRegionCode) && pRegionCode.contains(PO_BOX) && pApplicableShipMethodsSet != null
				&& pApplicableShipMethodsSet.contains(TRUConstants.FREIGHT_SHIP_METHOD_CODE)) {
			pApplicableShipMethodsSet.remove(TRUConstants.FREIGHT_SHIP_METHOD_CODE);
		}
	}

	/**
	 * Checks if is edd enabled.
	 *
	 * @return the eddEnabled
	 */
	public boolean isEddEnabled() {
		return mEddEnabled;
	}

	/**
	 * Sets the edd enabled.
	 *
	 * @param pEddEnabled the eddEnabled to set
	 */
	public void setEddEnabled(boolean pEddEnabled) {
		mEddEnabled = pEddEnabled;
	}

	/**
	 * Gets the edd cut off time.
	 *
	 * @return the eddCutOffTime
	 */
	public int getEddCutOffTime() {
		return mEddCutOffTime;
	}

	/**
	 * Sets the edd cut off time.
	 *
	 * @param pEddCutOffTime the eddCutOffTime to set
	 */
	public void setEddCutOffTime(int pEddCutOffTime) {
		mEddCutOffTime = pEddCutOffTime;
	}

	/**
	 * Gets the time in transit ship method codes.
	 *
	 * @return the timeInTransitShipMethodCodes
	 */
	public List<String> getTimeInTransitShipMethodCodes() {
		return mTimeInTransitShipMethodCodes;
	}

	/**
	 * Sets the time in transit ship method codes.
	 *
	 * @param pTimeInTransitShipMethodCodes the timeInTransitShipMethodCodes to set
	 */
	public void setTimeInTransitShipMethodCodes(
			List<String> pTimeInTransitShipMethodCodes) {
		mTimeInTransitShipMethodCodes = pTimeInTransitShipMethodCodes;
	}

	/**
	 * <p>
	 * 
	 * </p>.
	 *
	 * @param pCommerceItem - CommerceItem
	 * @param pAddress - Address
	 * @param pOrder - Order
	 * @return shipMethodVOs - shipMethodVOs
	 */
	public List<TRUShipMethodVO> getEligibleShipMethodsByItem(
			CommerceItem pCommerceItem, Address pAddress,Order pOrder) {
		final String frieghtClass = getFreightClassFromCI(pCommerceItem);
		final String region = getShippingRegion(pAddress);	
		if(StringUtils.isNotBlank(frieghtClass)){
			return getShipMethods(frieghtClass, region, pOrder, true);
		}
		
		return Collections.emptyList();
	}
	
	/**
	 * Gets the default ship method by item.
	 *
	 * @param pShipMethodVOs - ShipMethodVOs
	 * @return null
	 */
	public TRUShipMethodVO getDefaultShipMethodByItem(List<TRUShipMethodVO> pShipMethodVOs) {
		if (!pShipMethodVOs.isEmpty() && pShipMethodVOs.get(0) != null) {
			return pShipMethodVOs.get(0);
		}
		return null;
	}
	

	
	
	
	/**<p>
	 * This method will determines the region for identifying the shipmethod's price based address object.This is written for Applepay where in P.O.box validations based on
	 * address1 are skipped.
	 * </p>
	 * 
	 * @param pAddress address
	 * @return string
	 */
	public String getShippingRegionforApplePay(Address pAddress) {
		if (isLoggingDebug()) {
			logDebug("Start getShippingRegion()");
		}
		String region = TRURegionConstants.LOWER48.getRegion();
		if (null == pAddress) {
			if (isLoggingDebug()) {
				vlogDebug("pAddress is null and Region is {0}", TRURegionConstants.LOWER48.getRegion());
			}
			return region;
		}
		final boolean isMilitaryArea = isMilitaryArea(pAddress);
		if (isMilitaryArea) {
			region = TRURegionConstants.APO_FPO.getRegion();
			if (isLoggingDebug()) {
				vlogDebug("Region is {0}", region);
			}
			return region;
		}
		final boolean isUSTerritory = isUSTerritory(pAddress.getState());
		final boolean isAKHIState = isAKHIState(pAddress.getState());
		  if (isUSTerritory) {
			region = TRURegionConstants.US_TERRITORY.getRegion();
		} else if (isAKHIState) {
			region = TRURegionConstants.AK_HI.getRegion();
		}
		if (isLoggingDebug()) {
			vlogDebug("Identifying the region for Address1 : {0}, City:{1} , State :{2} and region is {3}",
					pAddress.getAddress1(), pAddress.getCity(), pAddress.getState(), region);
			vlogDebug("End getShippingRegion()");
		}
		return region;
	}

	
	/**<p>
	 * This method will determines the region for identifying the shipmethod's price based address object.
	 * </p>
	 * 
	 * @param pAddress address
	 * @return string
	 */
	public String getShippingRegion(Address pAddress) {
		if (isLoggingDebug()) {
			logDebug("Start getShippingRegion()");
		}
		String region = TRURegionConstants.LOWER48.getRegion();
		if (null == pAddress) {
			if (isLoggingDebug()) {
				vlogDebug("pAddress is null and Region is {0}", TRURegionConstants.LOWER48.getRegion());
			}
			return region;
		}
		final boolean isMilitaryArea = isMilitaryArea(pAddress);
		if (isMilitaryArea) {
			region = TRURegionConstants.APO_FPO.getRegion();
			if (isLoggingDebug()) {
				vlogDebug("Region is {0}", region);
			}
			return region;
		}
		final boolean isUSTerritory = isUSTerritory(pAddress.getState());
		final boolean isPOBOXAddress = isPOBOXAddress(pAddress.getAddress1());
		final boolean isAKHIState = isAKHIState(pAddress.getState());
		if (isUSTerritory && isPOBOXAddress) {
			region = TRURegionConstants.US_TERRITORY_PO_BOX.getRegion();
		} else if (isAKHIState && isPOBOXAddress) {
			region = TRURegionConstants.AK_HI_PO_BOX.getRegion();
		} else if (isPOBOXAddress) {
			region = TRURegionConstants.LOWER48_PO_BOX.getRegion();
		} else if (isUSTerritory) {
			region = TRURegionConstants.US_TERRITORY.getRegion();
		} else if (isAKHIState) {
			region = TRURegionConstants.AK_HI.getRegion();
		}
		if (isLoggingDebug()) {
			vlogDebug("Identifying the region for Address1 : {0}, City:{1} , State :{2} and region is {3}",
					pAddress.getAddress1(), pAddress.getCity(), pAddress.getState(), region);
			vlogDebug("End getShippingRegion()");
		}
		return region;
	}

	

	/**
	 * This method checks whether previously selected shipping method found in available shipping methods or not after changing the address.
	 * 
	 * @param pAvailableShippingMethods availableShippingMethods
	 * @param pShippingMethod shippingMethod
	 * @return boolean
	 */
	public boolean isPreferredShippingExistis(List<TRUShipMethodVO> pAvailableShippingMethods, String pShippingMethod) {
		if (pAvailableShippingMethods != null && StringUtils.isNotBlank(pShippingMethod)) {
			for (TRUShipMethodVO shipMethodVO : pAvailableShippingMethods) {
				if (shipMethodVO != null && pShippingMethod.equalsIgnoreCase(shipMethodVO.getShipMethodCode())) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * <p>
	 * This method determines whether the state belongs to AK,HI state or not.
	 * </p>
	 * 
	 * @param pState
	 *            state
	 * @return boolean
	 */
	private boolean isAKHIState(String pState) {
		if (isLoggingDebug()) {
			logDebug("Start TRUShippingProcessHelper.isAKHIAddress() and State \t" + pState);
		}
		if (StringUtils.isBlank(pState)) {
			return false;
		}
		final List<String> AKHIIdentfierList = getAKHIIdentifierList();
		if (AKHIIdentfierList != null && !(AKHIIdentfierList.isEmpty())
				&& TRUTaxwareConstant.MINUS_ONE != AKHIIdentfierList.indexOf(pState.toUpperCase())) {
			return true;
		}
		if (isLoggingDebug()) {
			logDebug("End TRUShippingProcessHelper.isAKHIAddress() ");
		}
		return false;
	}

	/**
	 * <p>
	 * This method determines the state belongs to USTerritory region or not.
	 * </p>
	 * 
	 * @param pState
	 *            state
	 * @return boolean
	 */
	private boolean isUSTerritory(String pState) {
		if (isLoggingDebug()) {
			logDebug("Start TRUShippingProcessHelper.isUSTerritory() and Address \t" + pState);
		}

		if (StringUtils.isBlank(pState)) {
			return false;
		}
		final List<String> USTerritoryIdentifierList = getUSTerritoryIdentifierList();

		if (USTerritoryIdentifierList != null && !(USTerritoryIdentifierList.isEmpty())
				&& TRUTaxwareConstant.MINUS_ONE != USTerritoryIdentifierList.indexOf(pState.toUpperCase())) {
			return true;
		}
		if (isLoggingDebug()) {
			logDebug("End TRUShippingProcessHelper.isUSTerritory() ");
		}
		return false;
	}
	  
	
	/**
	 * <p>
	 * This method determines the pAddress1 based region.
	 * </p>
	 * 
	 * @param pAddress1
	 *            address1
	 * @return boolean
	 */
	public boolean isPOBOXAddress(String pAddress1) {
		if(isLoggingDebug()){
			logDebug("Start TRUShippingProcessHelper.isPOBOXAddress() and Address1 \t" + pAddress1);
		}
		final List<String> POBOXIdentifierList = getPOBOXIdentifierList();
		if (StringUtils.isNotBlank(pAddress1) && POBOXIdentifierList != null && !(POBOXIdentifierList.isEmpty())) {
			for (String identifier : POBOXIdentifierList) {
				if (pAddress1.trim().toUpperCase().contains(identifier)) {
					vlogDebug("It is a PO Box address");
					return true;
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("End TRUShippingProcessHelper.isPOBOXAddress() ");
		}
		return false;
	}
	
	/**
	 * <p>
	 * This method determines the pAddress1 based region.
	 * </p>
	 * 
	 * @param pAddress1
	 *            address1
	 * @return boolean
	 */
	private boolean isTRUSHPARestrictedAddress(String pAddress1) {
		if(isLoggingDebug()){
			logDebug("Start TRUShippingProcessHelper.isTRUSHPARestrictedAddress() and Address \t" + pAddress1);
		}
		final List<String> TRU_SHPARestrictedIdentifiers = getTRUSHPARestrictedIdentifiers();
		if (!StringUtils.isEmpty(pAddress1) && TRU_SHPARestrictedIdentifiers != null && !(TRU_SHPARestrictedIdentifiers.isEmpty())) {
			for (String identifier : TRU_SHPARestrictedIdentifiers) {
				if (pAddress1.toUpperCase().contains(identifier)) {
					return true;
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("End TRUShippingProcessHelper.isTRUSHPARestrictedAddress() ");
		}
		return false;
	}
	
	/** Holds SHPTRUCK freight value. */
	private String mFreightClassShiptruck;


	/** Holds TRU_SHPA freight value. */
	private String mFreightClassTRUSHPA;
	
	/** Holds mNoShipMethodsErrorInMultiShip error message. */
	private String mNoShipMethodsErrorInMultiShip;
	
	/**
	 * Gets the no ship methods error in multi ship.
	 *
	 * @return mNoShipMethodsErrorInMultiShip
	 */
	public String getNoShipMethodsErrorInMultiShip() {
		return mNoShipMethodsErrorInMultiShip;
	}

	/**
	 * Sets the no ship methods error in multi ship.
	 *
	 * @param pNoShipMethodsErrorInMultiShip the mNoShipMethodsErrorInMultiShip to set
	 */
	public void setNoShipMethodsErrorInMultiShip(String pNoShipMethodsErrorInMultiShip) {
		this.mNoShipMethodsErrorInMultiShip = pNoShipMethodsErrorInMultiShip;
	}


	/**
	 * Gets the freight class shiptruck.
	 *
	 * @return mFreightClassShiptruck
	 */
	public String getFreightClassShiptruck() {
		return mFreightClassShiptruck;
	}


	/**
	 * Sets the freight class shiptruck.
	 *
	 * @param pFreightClassShiptruck the mFreightClassShiptruck to set
	 */
	public void setFreightClassShiptruck(String pFreightClassShiptruck) {
		this.mFreightClassShiptruck = pFreightClassShiptruck;
	}

	/**
	 * Gets the freight class trushpa.
	 *
	 * @return mFreightClassTRUSHPA
	 */
	public String getFreightClassTRUSHPA() {
		return mFreightClassTRUSHPA;
	}


	/**
	 * Sets the freight class trushpa.
	 *
	 * @param pFreightClassTRUSHPA the mFreightClassTRUSHPA to set
	 */
	public void setFreightClassTRUSHPA(String pFreightClassTRUSHPA) {
		this.mFreightClassTRUSHPA = pFreightClassTRUSHPA;
	}
	
	/** Holds text business days on shipping page. */
	private String mBusinessDaysText;

	/**
	 * getter for pBusinessDaysText.
	 *
	 * @return String
	 */
	public String getBusinessDaysText() {
		return mBusinessDaysText;
	}

	/**
	 * setter for  pBusinessDaysText.
	 *
	 * @param pBusinessDaysText BusinessDaysText
	 */
	public void setBusinessDaysText(String pBusinessDaysText) {
		this.mBusinessDaysText = pBusinessDaysText;
	}
	
	/** Holds error message for payPal validation error message. */
	private String mPayPalRestrictionErrMsg;

	/**
	 * getter for mPayPalRestrictionErrMsg.
	 *
	 * @return String
	 */
	public String getPayPalRestrictionErrMsg() {
		return mPayPalRestrictionErrMsg;
	}

	/**
	 * setter for mPayPalRestrictionErrMsg.
	 *
	 * @param pPayPalRestrictionErrMsg PayPalRestrictionErrMsg
	 */
	public void setPayPalRestrictionErrMsg(String pPayPalRestrictionErrMsg) {
		this.mPayPalRestrictionErrMsg = pPayPalRestrictionErrMsg;
	}
	
	/** Holds US military states. */
	private List<String> mUSMilitaryStates;
	
	/** Holds US military states. */
	private List<String> mUSMilitaryCities;

	/**
	 * getter for mUSMilitaryStates.
	 *
	 * @return USMilitaryStates
	 */
	public List<String> getUSMilitaryStates() {
		return mUSMilitaryStates;
	}

	/**
	 * setter for mUSMilitaryStates.
	 *
	 * @param pUSMilitaryStates USMilitaryStates
	 */
	public void setUSMilitaryStates(List<String> pUSMilitaryStates) {
		mUSMilitaryStates = pUSMilitaryStates;
	}

	/**
	 * getter for mUSMilitaryCities.
	 *
	 * @return USMilitaryCities
	 */
	public List<String> getUSMilitaryCities() {
		return mUSMilitaryCities;
	}

	/**
	 * setter for mUSMilitaryCities.
	 *
	 * @param pUSMilitaryCities USMilitaryCities
	 */
	public void setUSMilitaryCities(List<String> pUSMilitaryCities) {
		mUSMilitaryCities = pUSMilitaryCities;
	}
	
	/**
	 * Checks if is military area.
	 *
	 * @param pAddress Address
	 * @return boolean
	 */
	public boolean isMilitaryArea(Address pAddress) {
		final String state = pAddress.getState();	
		if ((!getUSMilitaryStates().isEmpty()) && StringUtils.isNotBlank(state) && getUSMilitaryStates().contains(state.toUpperCase())) {			
			return true;
		}
		return false;
	}

	
	/**
	 * <p>
	 * This method will checks whether an individual item is eligible for selected ship method or not. 
	 * </p>
	 * @param pCommerceItem -  CommerceItem
	 * @param pShippingGroup - ShippingGroup
	 * @param pOrder -Order
	 * @return TRUShipMethodVO - cheapShipMethodVO
	 */
	public TRUShipMethodVO isItemQualifyForShipMethod(CommerceItem pCommerceItem, TRUHardgoodShippingGroup pShippingGroup,
			Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("Start isItemQualifyForShipMethod()");
		}
		TRUShipMethodVO cheapShipMethodVO = null;
		boolean isItemQualifyForShipMethod = false;
		if (pCommerceItem != null && pShippingGroup != null && pShippingGroup.getShippingAddress() != null) {
			if (isLoggingDebug()) {
				vlogDebug("Checking pCommerceItem {0} eligible for shipMethod {1}", pCommerceItem.getId(),
						pShippingGroup.getShippingMethod());
			}
			final Address shippingAddress = pShippingGroup.getShippingAddress();
			// Get eligible ship methods for specific item based on the address and item's freightClass property
			final List<TRUShipMethodVO> eligibleShipMethodVOs = getEligibleShipMethodsByItem(pCommerceItem, shippingAddress, pOrder);
			if (null == eligibleShipMethodVOs || eligibleShipMethodVOs.isEmpty()) {
				if (isLoggingDebug()) {
					vlogDebug("No ship methods found for pCommerceItem {0} ", pCommerceItem.getId());
				}
				cheapShipMethodVO = new TRUShipMethodVO();
				cheapShipMethodVO.setShipMethodCode(TRUConstants.EMPTY_STRING);
				cheapShipMethodVO.setShipMethodName(TRUConstants.EMPTY_STRING);
				cheapShipMethodVO.setShippingPrice(TRUConstants.DOUBLE_ZERO);
				return cheapShipMethodVO;
			}
			if (isPreferredShippingExistis(eligibleShipMethodVOs, pShippingGroup.getShippingMethod())) {
				isItemQualifyForShipMethod = true;
				if (isLoggingInfo()) {
					vlogInfo("Shipmethod :{0} is eligible for commerceItem {1}  in order {2}..",
							pShippingGroup.getShippingMethod(), pCommerceItem.getId(), pOrder.getId());
				}
			} else {
				cheapShipMethodVO = new TRUShipMethodVO();
				cheapShipMethodVO.setShipMethodCode(eligibleShipMethodVOs.get(0).getShipMethodCode());
				cheapShipMethodVO.setShipMethodName(eligibleShipMethodVOs.get(0).getShipMethodName());
				cheapShipMethodVO.setShippingPrice(eligibleShipMethodVOs.get(0).getShippingPrice());
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End isItemQualifyForShipMethod() and the eligibility is {0}", isItemQualifyForShipMethod);
		}
		return cheapShipMethodVO;
	}

	
	/**
	 * This method retrieves all the eligible shipping methods for the cart items .
	 *
	 * @param pOrder - order
	 * @param pRegionCode -regionCode
	 * @param pApBuyNowFlow the ap buy now flow
	 * @return methodVOs
	 */
	public List<TRUShipMethodVO> getShippingMethods(
			Order pOrder,String pRegionCode,Boolean pApBuyNowFlow) {		
		if(isLoggingDebug()){
			vlogDebug("Start TRUShippingManager.getShippingMethods() for pOrder {0} and pRegionCode {1} ",pOrder.getId(),pRegionCode);
		}	
		Set<String> freightClassesSet =getUniqueFreightClasses(pOrder);
		if(freightClassesSet==null || freightClassesSet.isEmpty()){
			if(isLoggingDebug()){
				vlogDebug("pFreightClassesSet in null/empty for the order id {0}",pOrder.getId());
			}
			return Collections.emptyList();
		}
		Set<TRUShipMethodVO> methodVOs=new HashSet<TRUShipMethodVO>();
		if(freightClassesSet.size()>TRUConstants.SIZE_ONE && freightClassesSet.contains(getFreightClassShiptruck())){
			freightClassesSet.remove(getFreightClassShiptruck());
		}	
		RepositoryItem[] items = null;
		if(pApBuyNowFlow){
			items = getShippingTools().getShipMethodsByFreights(freightClassesSet);
		}else{
			items=	getShippingTools().getShipMethodsByFreighs(freightClassesSet);
		}
		
		if(items== null || items.length==TRUConstants._0){
			if(isLoggingDebug()){
				vlogDebug("No eligible ship methods found for the pOrder {0} and pRegionCode {1} ",pOrder.getId(),pRegionCode);
			}
		}
		if(null!=items && items.length>0){			
			methodVOs=createShipMethodVOSet(items,pRegionCode,pOrder);
			if(isLoggingDebug()){
				logDebug("==================================Start : Final Eligible ShipMethods======================");
				for(TRUShipMethodVO methodVO: methodVOs){
					logDebug(freightClassesSet + "|" + methodVO.getShipRegionVO().getRegionName() + "==>"
							+ methodVO.getShipMethodCode() + "(" + methodVO.getShipMethodName() + "),"
							+ methodVO.getShippingPrice() + "," + methodVO.getDelivertTimeLine());
				}
				logDebug("==================================End : Final Eligible ShipMethods======================");				
			}
		}
		if(isLoggingDebug()){
			vlogDebug("End TRUShippingManager.getShippingMethods() for pOrder {0} and pRegionCode {1} ",pOrder.getId(),pRegionCode);
		}
		return sortShipMethods(methodVOs);
	}
	
	
	/**
	 * mShipHoliDays holds the 
	 */
	private Set<String> mShipHoliDays;

	/**
	 * 
	 * @return mShipHoliDays
	 */
	public Set<String> getShipHoliDays() {
		return mShipHoliDays;
	}

	/**
	 * 
	 * @param pShipHoliDays the shipHoliDays to set
	 */
	public void setShipHoliDays(Set<String> pShipHoliDays) {
		this.mShipHoliDays = pShipHoliDays;
	}
	
	
	/**
	 * <p>
	 * This method will return active holiday list from the repository.
	 * </p>
	 * @return holidaySet -holidayset
	 */
	public Set<Date> getActiveHolidayDates(){
		if(isLoggingDebug()){
			logDebug("Start : HolidaySet ");
		}
		Set<Date> holidaySet=Collections.emptySet();
		try {
			RepositoryItem[] holyDayItems=  getShippingTools().getHolidayList();
			if(holyDayItems!=null && holyDayItems.length>TRUConstants.ZERO){
				holidaySet=new LinkedHashSet<Date>( holyDayItems.length);
				for(RepositoryItem holidayItem:holyDayItems ){					
					Object value=holidayItem.getPropertyValue(getPropertyManager().getHolidayDatePropertyName());
					if(value instanceof Date){
						holidaySet.add((Date)value);
					}
				}				
			}else{
				if(isLoggingDebug()){
					logDebug("No TRU Ship holidays found..");
				}
			}
		} catch (RepositoryException e) {
			if(isLoggingError()){
				logError("Error : while retrieving from the repository getActiveHolidays()",e);
			}
			return holidaySet;
		}finally{
			if(isLoggingDebug()){
				vlogDebug("holidaySet {0}",holidaySet);
			}
		}
		if(isLoggingDebug()){
			logDebug("End : HolidaySet");
		}
		return holidaySet;
	}
	
	/**
	 * This method is used to retrieve shipWindow values from Repo
	 *
	 * @param pHardgoodShippingGroup the hardgood shipping group
	 * @return the region based ship window
	 */
	@SuppressWarnings("unchecked")
	public Map<String,Integer> getRegionBasedShipWindow(HardgoodShippingGroup pHardgoodShippingGroup){
		if(isLoggingDebug()){
			logDebug("Start : getRegionBasedShipWindow");
		}
		int estimateShipWindowMin=0;
		int estimateShipWindowMax=0;
		if (StringUtils.isNotBlank(pHardgoodShippingGroup.getShippingMethod())) {			
			final RepositoryItem shipMethodItem =getShippingTools().getShipMethodDetails(pHardgoodShippingGroup.getShippingMethod());
			if (shipMethodItem != null) {
				final String regionCode = getShippingRegion(pHardgoodShippingGroup.getShippingAddress());
				final Set<RepositoryItem> shipRegionItems = (Set<RepositoryItem>) shipMethodItem
						.getPropertyValue(getPropertyManager().getEligibleShipRegionsPropertyName());
				if (shipRegionItems != null && !shipRegionItems.isEmpty()) {
					final TRUShipRegionVO shipRegionVO = getMatchedShipRegionAndPrice(shipRegionItems,
							regionCode);
					if (shipRegionVO != null) {
						final boolean isConsiderTimeInTransit = isShipMethodCodeConsiderForTimeInTransit(pHardgoodShippingGroup
								.getShippingMethod());
						if (isConsiderTimeInTransit) {
							estimateShipWindowMin += shipRegionVO.getTimeInTransitMinDays();
							estimateShipWindowMax += shipRegionVO.getTimeInTransitMaxDays();
						} else {
							estimateShipWindowMin += shipRegionVO.getDeliveryMinDays();
							estimateShipWindowMax += shipRegionVO.getDeliveryMaxDays();
						}
					}	
					TRUHardgoodShippingGroup truHardgoodShippingGroup=(TRUHardgoodShippingGroup)pHardgoodShippingGroup;
					// updating delivery min and max days
					int deliveryMinDays = shipRegionVO.getDeliveryMinDays();					
					int deliveryMaxDays =  shipRegionVO.getDeliveryMaxDays();
					String shipMethodName = (String) shipMethodItem.getPropertyValue(getPropertyManager()
							.getShipMethodNamePropertyName());
					truHardgoodShippingGroup.setRegionCode(regionCode);
					truHardgoodShippingGroup.setDeliveryTimeFrame(deliveryMinDays+TRUConstants.IFAN+deliveryMaxDays+ TRUConstants.WHITE_SPACE + getBusinessDaysText());
					truHardgoodShippingGroup.setShippingMethodName(shipMethodName);
					shipMethodName=null;
				
				}
			}
		}
		Map<String,Integer> resultMap = new LinkedHashMap<>(TRUConstants.INTEGER_NUMBER_TWO);
		resultMap.put(TRUConstants.EDD_SHIP_MIN, estimateShipWindowMin);
		resultMap.put(TRUConstants.EDD_SHIP_MAX, estimateShipWindowMax);		
		if(isLoggingDebug()){
			vlogDebug("Result Map\t: {0}", resultMap);
			logDebug("End : getRegionBasedShipWindow");
		}
		return resultMap;
	}
	
	
	/**
	 * Identifies the given ship method code to be consider for time in transit.
	 * 
	 * @param pShipMethodCode shipMethodCode
	 * @return boolean
	 */
	private boolean isShipMethodCodeConsiderForTimeInTransit(String pShipMethodCode) {
		boolean isConsiderTimeInTransit = false;
		final List<String> timeInTransitShipMethodCodes = getTimeInTransitShipMethodCodes();
		if (timeInTransitShipMethodCodes != null && !timeInTransitShipMethodCodes.isEmpty()) {
			for (String shipMethodCode : timeInTransitShipMethodCodes) {
				if (StringUtils.isNotBlank(shipMethodCode) &&
						shipMethodCode.equalsIgnoreCase(pShipMethodCode)) {
					isConsiderTimeInTransit = true;
					break;
				}
			}
		}
		return isConsiderTimeInTransit;
	}


	/**
	 * Checks if is eligible for existing ship method.
	 *
	 * @param pCommerceItem the commerce item
	 * @param pShippingGroup the shipping group
	 * @param pOrder the order
	 * @return the TRU ship method vo
	 * @throws TRUPurchaseProcessException the TRU purchase process exception
	 */
	public TRUShipMethodVO isEligibleForExistingShipMethod(CommerceItem pCommerceItem, TRUHardgoodShippingGroup pShippingGroup,
			Order pOrder) throws TRUPurchaseProcessException{
		if (isLoggingDebug()) {
			vlogDebug("Start isEligibleForExistingShipMethod()");
		}
		TRUShipMethodVO cheapShipMethodVO = null;
		boolean isItemQualifyForShipMethod = true;
		if (pCommerceItem != null && pShippingGroup != null && pShippingGroup.getShippingAddress() != null) {
			if (isLoggingDebug()) {
				vlogDebug("Checking pCommerceItem {0} eligible for shipMethod {1} in order {2}", pCommerceItem.getId(),
						pShippingGroup.getShippingMethod() ,pOrder.getId());
			}
			final Address shippingAddress = pShippingGroup.getShippingAddress();
			// Get eligible ship methods for specific item based on the address and item's freightClass property
			final List<TRUShipMethodVO> eligibleShipMethodVOs = getEligibleShipMethodsByItem(pCommerceItem, shippingAddress, pOrder);
			if (isLoggingError()) {
				vlogError("eligibleShipMethodVOs {0} for pCommerceItem  {1} ", eligibleShipMethodVOs, pCommerceItem.getId());
			}
			if (!isPreferredShippingExistis(eligibleShipMethodVOs, pShippingGroup.getShippingMethod())) {
				cheapShipMethodVO = new TRUShipMethodVO();
				cheapShipMethodVO.setShipMethodCode(eligibleShipMethodVOs.get(0).getShipMethodCode());
				cheapShipMethodVO.setShipMethodName(eligibleShipMethodVOs.get(0).getShipMethodName());
				cheapShipMethodVO.setShippingPrice(eligibleShipMethodVOs.get(0).getShippingPrice());
				isItemQualifyForShipMethod = false;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End isEligibleForExistingShipMethod() and the eligibility is {0}", isItemQualifyForShipMethod);
		}
		return cheapShipMethodVO;
	}
	
	
	/** Property to Hold Error handler manager. */
	private DefaultErrorHandlerManager mErrorHandlerManager;
	
	/**
	 * getter for errorHandlerManager.
	 *
	 * @return mErrorHandlerManager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}
	
	/**
	 * Sets the errorHandlerManager.
	 *
	 * @param pErrorHandlerManager the mErrorHandlerManager to set
	 */
	public void setErrorHandlerManager(DefaultErrorHandlerManager pErrorHandlerManager) {
		this.mErrorHandlerManager = pErrorHandlerManager;
	}
	
	
	/**
 	 * <p>
 	 * This method will get the error message for given error key.
 	 * </p>
 	 * @param pErrorKey - pErrorKey
 	 * @return errorMessage - errorMessage
 	 */
 	private String getErrorMessage(String pErrorKey){
 		String errorMessage=pErrorKey;
 		try {
			 errorMessage = getErrorHandlerManager().getErrorMessage(pErrorKey);
		} catch (com.tru.common.cml.SystemException e) {
			if(isLoggingError()){
				logError("com.tru.common.cml.SystemException @Class:::TRUShippingGroupFormHandler::@method::getErrorMessage()\t:"+e,e);
			}
		}
 		return errorMessage;
 	}
	
 	
 	/**<p>
	 * 	This method will retrieve only S2H items from the order by ignoring the ISPU items.
	 * </p>
	 * @param pOrder Order
	 * @return freightClasseSet Set<String>
	 */
	public Set<String> getUniqueFreightClasses(final Order pOrder) {		
		if (isLoggingDebug()) {
			vlogDebug("Start TRUShippingManager.getUniqueFreightClasses() {0}",pOrder.getId());
		}
		Set<String> freightClasseSet=new LinkedHashSet<String>();
		if (pOrder != null) {
			TRUOrderImpl v_pOrder=(TRUOrderImpl)pOrder;
			List<CommerceItem> items= v_pOrder.getS2HItems();
			for(CommerceItem item:items){
				final String freightClassVal=getFreightClassFromCI(item);
				if(StringUtils.isNotBlank(freightClassVal)){
					freightClasseSet.add(freightClassVal);
				}		
			}						
		}
		if (isLoggingDebug()) {
			vlogDebug("Unique freightClasseSet :{0}", freightClasseSet);
			vlogDebug("End TRUShippingManager.getUniqueFreightClasses() {0}",pOrder.getId());
		}		
		return freightClasseSet;
	}	
	
	
	/**
	 * Gets the shipping method from promo.
	 *
	 * @return the shipping method from promo
	 */
	public List<String> getShippingMethodFromPromo(){
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShippingManager::@method::getShippingMethodFromPromo() : BEGIN");
		}
		Site site = SiteContextManager.getCurrentSite();
		Date now = getPromotionTools().getCurrentDate().getTimeAsDate();
		RepositoryItem promotionItem = null;
		if (site != null) {
			promotionItem = (RepositoryItem) site.getPropertyValue(getCookieManager().getPropertyManager()
					.getFreeShippingPromotionPropertyName());
		}
		if(promotionItem != null && !getPromotionTools().checkPromotionExpiration(promotionItem, now)){
			String shippingMethod=(String) promotionItem.getPropertyValue(getCookieManager().getPropertyManager().getShippingMethodPropertyName());			
			if(StringUtils.isNotBlank(shippingMethod)){
				String[] shippingMethArray=shippingMethod.split(TRUConstants.SPLIT_SYMBOL_COMA);
				if (isLoggingDebug()) {
					vlogDebug("shipping Method Array : {0}", String.valueOf(shippingMethArray));
					logDebug("@Class::TRUShippingManager::@method::getShippingMethodFromPromo() : END");
				}
				return Arrays.asList(shippingMethArray);
			}			
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUShippingManager::@method::getShippingMethodFromPromo() : END");
		}
		return Collections.emptyList();
	}

	/** The m promotion tools. */
	private TRUPromotionTools mPromotionTools;

	/**
	 * Gets the promotion tools.
	 *
	 * @return the mPromotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * Sets the promotion tools.
	 *
	 * @param pPromotionTools the new promotion tools
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		this.mPromotionTools = pPromotionTools;
	}
	
	/** The cookie manager. */
	private TRUCookieManager mCookieManager;

	/**
	 * Gets the cookie manager.
	 *
	 * @return the mCookieManager
	 */
	public TRUCookieManager getCookieManager() {
		return mCookieManager;
	}

	/**
	 * Sets the cookie manager.
	 *
	 * @param pCookieManager the mCookieManager to set
	 */
	public void setCookieManager(TRUCookieManager pCookieManager) {
		this.mCookieManager = pCookieManager;
	}
	
}