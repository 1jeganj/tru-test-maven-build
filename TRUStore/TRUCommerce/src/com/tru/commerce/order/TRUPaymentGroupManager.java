package com.tru.commerce.order;

import java.util.ArrayList;
import java.util.List;

import atg.commerce.CommerceException;
import atg.commerce.order.CreditCard;
import atg.commerce.order.InStorePayment;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupManager;
import atg.commerce.order.PaymentGroupOrderRelationship;
import atg.commerce.pricing.OrderPriceInfo;

import com.tru.commerce.payment.TRUPaymentConstants;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.TRUConstants;

	/**
	 * @author Professional Access
     * @version 1.0
	 */

public class TRUPaymentGroupManager extends PaymentGroupManager {
	/**
	 * Property to hold reference to OrderManager.
	 */
	private TRUOrderManager mOrderManager;

	/**
	 * @return mOrderManager OrderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * @param pOrderManager
	 *            OrderManager
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}


	/**
	 * This method will remove all payment groups from the current order.
	 * 
	 * @param pOrder current order
	 * 
	 * @throws CommerceException reference to CommerceException
	 * 
	 */
	public void removeOtherPaymentGroups(Order pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("Entering into TRUPaymentGroupManager.removeCreditCardPaymentGroupFromOrder() menthod");
		}
		if (pOrder == null) {
			return;
		}
		List<PaymentGroup> paymentGroupsList = pOrder.getPaymentGroups();
		
		if (paymentGroupsList == null || paymentGroupsList.isEmpty()) {
			return;
		}
		
	    for (int i = (paymentGroupsList.size() -1); i >= 0; i--) {
	    	PaymentGroup paymentGroup = (PaymentGroup)paymentGroupsList.get(i);
			if ((paymentGroup instanceof CreditCard) || (paymentGroup instanceof InStorePayment)) {
				removePaymentGroupFromOrder(pOrder, paymentGroup.getId());
			}
	    }
	    
		if (isLoggingDebug()) {
			vlogDebug("Exiting from TRUPaymentGroupManager.removeCreditCardPaymentGroupFromOrder() menthod");
		}
	}
	
	/**
	 * This method is used to get the remaining amount of order amount.
	 * 
	 * @param pOrder
	 *            - the order
	 * @return paymentAmount - the remaining amount for the creditCard
	 *         
	 */
	public double getOrderAmount(Order pOrder) {
		double paymentAmount = TRUConstants.DOUBLE_ZERO;
		if (pOrder != null) {
			OrderPriceInfo orderPriceInfo = pOrder.getPriceInfo();
		//	double otherPGAmount = TRUConstants.DOUBLE_ZERO;
			if (orderPriceInfo != null) {
		//		paymentAmount = orderPriceInfo.getTotal() - otherPGAmount;
				paymentAmount = orderPriceInfo.getTotal();
			}
		}
		return paymentAmount;
	}
	
	/**
	 * This method will remove all payment groups from the current order.
	 * 
	 * @param pOrder current order
	 * 
	 * @throws CommerceException reference to CommerceException
	 * 
	 */
	public void removePayPalPaymentGroupToOrder(Order pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("Entering into TRUPaymentGroupManager.removeCreditCardPaymentGroupFromOrder() menthod");
		}
		if (pOrder == null) {
			return;
		}
		List<PaymentGroup> paymentGroupsList = pOrder.getPaymentGroups();
		
		if (paymentGroupsList == null || paymentGroupsList.isEmpty()) {
			return;
		}
		
		for (int i = (paymentGroupsList.size() -1); i >= 0; i--) {
			PaymentGroup paymentGroup = (PaymentGroup)paymentGroupsList.get(i);
			if ((paymentGroup instanceof TRUPayPal) || (paymentGroup instanceof InStorePayment)) {
				removePaymentGroupFromOrder(pOrder, paymentGroup.getId());
			}
		}		
		
		if (isLoggingDebug()) {
			vlogDebug("Exiting from TRUPaymentGroupManager.removeCreditCardPaymentGroupFromOrder() menthod");
		}
	}
	
	/**
	 * This method is used to get the remaining amount of order amount.
	 * 
	 * @param pOrder
	 *            - the order
	 * @return paymentAmount - the remaining amount for the creditCard
	 *         
	 */
	public double getRemainingOrderAmount(Order pOrder) {
		double paymentGroupAmount = TRUConstants.DOUBLE_ZERO;
		double finalRemaniningAmount = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingtools = (TRUPricingTools)getOrderTools().getProfileTools().getPricingTools();
		if (pOrder.getPriceInfo() != null) {
			finalRemaniningAmount = pOrder.getPriceInfo().getTotal();
			finalRemaniningAmount = pricingtools.round(finalRemaniningAmount);
		}
		if (!pOrder.getPaymentGroups().isEmpty()) {
			List<PaymentGroup> paymentGroups=pOrder.getPaymentGroups();
			for(PaymentGroup paymentGroup :paymentGroups ){
				if(paymentGroup instanceof TRUGiftCard){
					paymentGroupAmount += paymentGroup.getAmount();
				}
			}
		}
		finalRemaniningAmount = finalRemaniningAmount - paymentGroupAmount;
		return pricingtools.round(finalRemaniningAmount);

	}
	
	/**
	 * @param pOrder - Order
	 * @param pPaymentGroup - PaymentGroup
	 * @throws CommerceException - CommerceException
	 */
	public void addPaymentGroupToOrder(Order pOrder, PaymentGroup pPaymentGroup)
			throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentGroupManager.(addPaymentGroupToOrder()) :: End");
			vlogDebug(" Order :{0} pPaymentGroup :{1}", pOrder, pPaymentGroup);
		}
		int index = 0;
		if (pPaymentGroup != null && pOrder != null) {
			PaymentGroup orderPG = null;
			if (!pOrder.getPaymentGroups().isEmpty()) {
				for (; index < pOrder.getPaymentGroups().size(); index++) {
					orderPG = (PaymentGroup) pOrder.getPaymentGroups().get(index);
					if (pPaymentGroup instanceof TRUGiftCard) {
						if (orderPG instanceof TRUGiftCard) {
							if (((TRUGiftCard) orderPG).getCurrentBalance() > ((TRUGiftCard) pPaymentGroup).getCurrentBalance()) {
								break;
							}
							continue;
						} else if (orderPG instanceof TRUPayPal) {
							break;
						} else if (orderPG instanceof TRUCreditCard) {
							break;
						}
					}  else if (pPaymentGroup instanceof TRUCreditCard) {
						if (orderPG instanceof TRUGiftCard) {
							continue;
						} else if (orderPG instanceof TRUPayPal) {
							continue;
						} else if (orderPG instanceof TRUCreditCard) {
							break;
						}
					}
				}
			}
			addPaymentGroupToOrder(pOrder, pPaymentGroup, index);
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentGroupManager.(addPaymentGroupToOrder()) :: End");
		}
	}
	/**
	 * @param pOrder - Order
	 * @param pPaymentGroupType - String
	 * @return List PaymentGroup
	 */
	public List<PaymentGroup> getAllPaymentGroupsWithType(Order pOrder,String pPaymentGroupType){
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentGroupManager.(getAllPaymentGroupsWithType()) :: Start");
			vlogDebug(" pOrder:{0}  pPaymentGroupType:{1} ",pOrder,pPaymentGroupType);
		}
		List<PaymentGroup> listOfSamePaymentGroups = null;
		if(pOrder != null && pPaymentGroupType != null) {
			listOfSamePaymentGroups = new ArrayList<PaymentGroup>();
			List<PaymentGroup> listOfPGs = pOrder.getPaymentGroups();
			if(listOfPGs != null){
				for(PaymentGroup pg :listOfPGs){
					if(pg.getPaymentMethod()!=null && pg.getPaymentMethod().equalsIgnoreCase(pPaymentGroupType)){
						listOfSamePaymentGroups.add(pg);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("listOfSamePaymentGroups :{0} ",listOfSamePaymentGroups);
			vlogDebug("TRUPaymentGroupManager.(getAllPaymentGroupsWithType()) :: End");
		}
		return listOfSamePaymentGroups;
 	}
	
	/**
	 * @param pOrder - Order
	 * @return ListTRUGiftCard - List TRUGiftCard
	 */
	public List<TRUGiftCard> getAllGiftCardPGs(Order pOrder){
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentGroupManager.(getAllGiftCardPGs()) :: Start");
			vlogDebug(" Order :{0}",pOrder);
		}
		List<TRUGiftCard> listOfGiftCardPGs = null; 
		if(pOrder != null){
			List<PaymentGroup> listOfPGs = pOrder.getPaymentGroups();
			listOfGiftCardPGs = new ArrayList<TRUGiftCard>();
			for(PaymentGroup pg :listOfPGs){
				if(pg instanceof TRUGiftCard){
					listOfGiftCardPGs.add((TRUGiftCard)pg);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug(" ListOfGiftCardPGs :{0}",listOfGiftCardPGs);
			vlogDebug("TRUPaymentGroupManager.(getAllGiftCardPGs()) :: End");
		}
		return listOfGiftCardPGs;
 	}
	
	
	/**
	 * @param pOrder  - Order
	 * @throws CommerceException 
	 */
	public void adjustGiftCardAmount(Order pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentGroupManager/adjustGiftCardAmount Method :Start");
		}		
		double orderTotal = pOrder.getPriceInfo().getTotal();
		double adjustAmt = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingtools = (TRUPricingTools)getOrderTools().getProfileTools().getPricingTools();
		List<PaymentGroup> listOfGiftCardPGs = (List<PaymentGroup>) getAllPaymentGroupsWithType(pOrder, TRUConstants.GIFT_CARD);
		if ((!listOfGiftCardPGs.isEmpty()) && listOfGiftCardPGs.size() > TRUConstants.INTEGER_NUMBER_ZERO) {
				double totalGiftCardAmt = getOrderManager().getPaymentGroupsTotalAppliedAmount(pOrder);
				if (orderTotal > totalGiftCardAmt ) {
					adjustAmt = pricingtools.round(orderTotal - totalGiftCardAmt);
					adjustOrderExcessAmtToPGs(pOrder, adjustAmt);
				} else if (totalGiftCardAmt > orderTotal ) {
					adjustAmt = pricingtools.round(totalGiftCardAmt - orderTotal);	
					adjustExcessGiftCardAmt(pOrder, pricingtools.round(adjustAmt));
				}
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentGroupManager/adjustGiftCardAmount Method :End");
		}
		}
			
	}
	
	
	/**
	 * @param pOrder - Order
	 * @param pExcessMerchantAmt - Double
	 * @throws CommerceException 
	 */
	public void adjustOrderExcessAmtToPGs(Order pOrder,double pExcessMerchantAmt) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentGroupManager/adjustOrderExcessAmtToPGs Method :Start");
		}
		TRUPricingTools pricingtools = (TRUPricingTools)getOrderTools().getProfileTools().getPricingTools();
		if (pOrder != null && pExcessMerchantAmt > TRUPaymentConstants.DOUBLE_ZERO_VALUE) {
			double excessMerchantAmt = pExcessMerchantAmt;
			TRUGiftCard truGiftCard = null;
			List<PaymentGroup> listOfGiftCardPGs = (List<PaymentGroup>) getAllPaymentGroupsWithType(pOrder, TRUConstants.GIFT_CARD);
			if (listOfGiftCardPGs != null && !listOfGiftCardPGs.isEmpty()) {
				while (excessMerchantAmt > TRUPaymentConstants.DOUBLE_ZERO_VALUE) {
						for (PaymentGroup pg : listOfGiftCardPGs) {
							if (pg instanceof TRUGiftCard && ((TRUGiftCard) pg).getRemainingBalance() > TRUPaymentConstants.DOUBLE_ZERO_VALUE) {
									truGiftCard = (TRUGiftCard) pg;
									break;
							}
						}
						if(truGiftCard!=null) {
						PaymentGroupOrderRelationship pgOrdRel=getPaymentGroupOrderRelationship(pOrder, truGiftCard.getId());
						if (truGiftCard.getRemainingBalance() > excessMerchantAmt) {
							double amountApplied = truGiftCard.getAmount();
							double amount = pricingtools.round(amountApplied + excessMerchantAmt);
							truGiftCard.setAmount(amount);
							if(isLoggingDebug()) {
								vlogDebug(" TRUPurchaseProcessManager.applyGiftcard ::: GC id :{0} ExcessMercAmt :{1} Old Remaing Amount :{2}" + 
										" new Remaining Amount :{3} ",truGiftCard.getId(),excessMerchantAmt,truGiftCard.
										getRemainingBalance(),truGiftCard.getRemainingBalance() - excessMerchantAmt);
							}
							truGiftCard.setRemainingBalance(pricingtools.round(truGiftCard.getRemainingBalance() - excessMerchantAmt));
							excessMerchantAmt = TRUPaymentConstants.DOUBLE_ZERO_VALUE;
					        pgOrdRel.setAmount(amount);
						} else {
							double amountApplied = truGiftCard.getAmount();
							excessMerchantAmt -= truGiftCard.getRemainingBalance();
							double amount = pricingtools.round(amountApplied + truGiftCard.getRemainingBalance());							
							truGiftCard.setAmount(amount);
							truGiftCard.setRemainingBalance(TRUPaymentConstants.DOUBLE_ZERO_VALUE);
					        pgOrdRel.setAmount(amount);
						}
				}
				else {
						break;
				}
						truGiftCard = null;
					} 
				}
			}
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentGroupManager/adjustOrderExcessAmtToPGs Method :End");
		}
	}
	
	/**
	 * @param pOrder - Order
	 * @param pExcessOrderTotalAmt - Double
	 * @throws CommerceException 
	 * 
	 */
	public void adjustExcessGiftCardAmt(Order pOrder,double pExcessOrderTotalAmt) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentGroupManager/adjustExcessGiftCardAmt Method :End");
			vlogDebug(" Order : {0} Excess Order Total Amount : {1}",pOrder,pExcessOrderTotalAmt);
		}
		TRUPricingTools pricingtools = (TRUPricingTools)getOrderTools().getProfileTools().getPricingTools();
		if (pOrder != null && pExcessOrderTotalAmt > TRUConstants.DOUBLE_ZERO) {
	        double excessOrderTotalAmt = pExcessOrderTotalAmt;
			TRUGiftCard truGiftCard = null;
			List<PaymentGroup> listOfGiftCardPGs = (List<PaymentGroup>) getAllPaymentGroupsWithType(pOrder,TRUConstants.GIFT_CARD);
			if (listOfGiftCardPGs != null && !listOfGiftCardPGs.isEmpty()) {
				while (excessOrderTotalAmt > TRUPaymentConstants.DOUBLE_ZERO_VALUE) {
					for (PaymentGroup pg : listOfGiftCardPGs) {
						if (pg instanceof TRUGiftCard) {
							truGiftCard = (TRUGiftCard) pg;
							if (truGiftCard.getRemainingBalance() > TRUPaymentConstants.DOUBLE_ZERO_VALUE) {
								break;
							}
						}
					}					
			        if (truGiftCard != null) 
			        {
			        PaymentGroupOrderRelationship pgOrdRel=getPaymentGroupOrderRelationship(pOrder, truGiftCard.getId());
					if (truGiftCard.getAmount() > excessOrderTotalAmt) {
						double amountApplied = truGiftCard.getAmount();
						double amount = pricingtools.round(amountApplied - excessOrderTotalAmt);
						truGiftCard.setAmount(amount);
						if(isLoggingDebug()) {
							vlogDebug(" TRUPurchaseProcessManager.applyGiftcard ::: GC id :{0} ExcessOrderTotalAmt :{1} Old Remaing Amount :{2} " +
									"new Remaining Amount :{3} ",truGiftCard.getId(),excessOrderTotalAmt,truGiftCard.
												getRemainingBalance(),truGiftCard.getRemainingBalance() - excessOrderTotalAmt);
						}
						truGiftCard.setRemainingBalance(pricingtools.round(truGiftCard.getRemainingBalance() + excessOrderTotalAmt));
						excessOrderTotalAmt = TRUPaymentConstants.DOUBLE_ZERO_VALUE;			
				        pgOrdRel.setAmount(amount);
						if (isLoggingDebug()) {
							vlogDebug(" Amount :{0} RemainingBalance :{1}",truGiftCard.getAmount(),truGiftCard.getRemainingBalance());
						}
					} else 
					{
						excessOrderTotalAmt = excessOrderTotalAmt - truGiftCard.getAmount();
						if (isLoggingDebug()) {
							vlogDebug(" Excess Order Total Amount :{0}",excessOrderTotalAmt);
						}
						try {
							removePaymentGroupFromOrder(pOrder, truGiftCard.getId());
							listOfGiftCardPGs.remove(truGiftCard);
						} catch (CommerceException comExp) {
							if (isLoggingError()) {
								vlogError("CommerceException occured at TRUPaymentGroupManager/service method,,orderId:{0} profileId:{1} comExp:{2}",
										pOrder.getId(),pOrder.getProfileId(),comExp);
							}
						}
					}
				}else {
						break;
					}
					truGiftCard = null;
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("TRUPaymentGroupManager/adjustExcessGiftCardAmt Method :End");
			}
		}
	}	
	
}
