/**
 * 
 */
package com.tru.commerce.order;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.core.util.Address;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.purchase.TRUShippingGroupContainerService;
import com.tru.commerce.order.purchase.TRUShippingProcessHelper;
import com.tru.common.TRUConstants;

/**
 * This droplet fetches all available shipping addresses for multi shipping.
 * @author PA
 * @version 1.0
 */
public class TRUAvailableMultipleShippingAddressesDroplet extends DynamoServlet {
	
	/**
	 * property: Reference to the ShippingProcessHelper component.
	 */
	private TRUShippingProcessHelper mShippingHelper;
	
	/**
	 * @return the shippingHelper
	 */
	public TRUShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * @param pShippingHelper
	 *            the shippingHelper to set
	 */
	public void setShippingHelper(TRUShippingProcessHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}
	
	/**
	 * The service method of the droplet.
	 * @param pRequest - DynamoHttpServletRequest
	 * @param pResponse - DynamoHttpServletResponse
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {		
		TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) 
				pRequest.getObjectParameter(TRUCheckoutConstants.SHIPPING_GROUP_MAP_CONTAINER);  
		//TRUCartItemDetailVO cartItemDetailVO = (TRUCartItemDetailVO) pRequest.getObjectParameter(TRUCheckoutConstants.CART_ITEM_DETAIL_VO);
		Order order = (Order) pRequest.getObjectParameter(TRUCheckoutConstants.ORDER);		
		Map<String, ShippingGroup> validShippingGroupsMapForDisplay = new LinkedHashMap<String, ShippingGroup>();
		// holds unique ship group key againist nicknames
		Map<String, String> shipGroupKeyMap = new LinkedHashMap<String, String>();
		
		if (shippingGroupMapContainer == null || order == null) {
			pRequest.setParameter(TRUCheckoutConstants.VALID_SHIPPING_GROUP_MAP, validShippingGroupsMapForDisplay);
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
			return;
		}		
		Map<String, ShippingGroup> shippingGroupMap = shippingGroupMapContainer.getShippingGroupMap();
		String nickName = TRUConstants.EMPTY;
		if (shippingGroupMap != null && !shippingGroupMap.isEmpty()) {
			Set<String> shippingGroupKeys = shippingGroupMap.keySet();
			for (String shippingGroupKey : shippingGroupKeys) {
					nickName = shippingGroupKey;
					ShippingGroup shippingGroup = (ShippingGroup) shippingGroupMap.get(shippingGroupKey);
					// add only cartItemDetailVo related shipping groups to map.
					if (shippingGroup instanceof TRUChannelHardgoodShippingGroup) {
						TRUChannelHardgoodShippingGroup hardgoodShippingGroup = (TRUChannelHardgoodShippingGroup) shippingGroup;
						Address address = hardgoodShippingGroup.getShippingAddress();
						nickName = address.getFirstName() + TRUConstants.WHITE_SPACE + address.getLastName();
					}
					TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;					
					if (!hardgoodShippingGroup.isBillingAddress() && TRUConstants.UNITED_STATES.equalsIgnoreCase(hardgoodShippingGroup.getShippingAddress().getCountry())) {
						validShippingGroupsMapForDisplay.put(shippingGroupKey, hardgoodShippingGroup);
						shipGroupKeyMap.put(shippingGroupKey, nickName);
					}
			}
		}		
		pRequest.setParameter(TRUCheckoutConstants.VALID_SHIPPING_GROUP_MAP, validShippingGroupsMapForDisplay);
		pRequest.setParameter(TRUCheckoutConstants.SHIP_GROUP_KEY_MAP, shipGroupKeyMap);
		pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
	}
}
