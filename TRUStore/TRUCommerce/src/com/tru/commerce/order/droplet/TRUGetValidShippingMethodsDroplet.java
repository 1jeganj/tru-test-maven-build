package com.tru.commerce.order.droplet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import atg.commerce.order.Order;
import atg.core.util.Address;
import atg.core.util.StringUtils;
import atg.json.JSONArray;
import atg.json.JSONObject;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.google.gson.Gson;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.ComponentFilter;
import com.google.maps.model.GeocodingResult;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUShippingManager;
import com.tru.commerce.order.purchase.TRUShippingProcessHelper;
import com.tru.commerce.order.utils.TRURegionConstants;
import com.tru.commerce.pricing.TRUShipMethodVO;
import com.tru.commerce.pricing.TRUShippingPricingEngine;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;

/**
 * This Droplet will fetch the Common Shipping Options for ApplePay Order.
 * @author PA
 * @version 1.0
 */
public class TRUGetValidShippingMethodsDroplet extends DynamoServlet {

	/**
	 * Property to hold shippingManager component
	 */
	private TRUShippingManager mShippingManager;
	/**
	 * property to hold number of mTRUConfiguration.
	 */
	private TRUConfiguration mTRUConfiguration;
	
	/**
	 * property to hold stateErrorValue.
	 */
	private String mStateErrorValue;
	/**
	 * property to hold mCountryErrorValue.
	 */
	private String mCountryErrorValue;
	/**
	 * @return the mCountryErrorValue
	 */
	public String getCountryErrorValue() {
		return mCountryErrorValue;
	}

	/**
	 * @param pCountryErrorValue the mCountryErrorValue to set
	 */
	public void setCountryErrorValue(String pCountryErrorValue) {
		mCountryErrorValue = pCountryErrorValue;
	}
	/**
	 * property to hold combinationErrorValue.
	 */
	private String mCombinationErrorValue;
	/**
	 * @return the stateErrorValue
	 */
	public String getStateErrorValue() {
		return mStateErrorValue;
	}

	/**
	 * @param pStateErrorValue the stateErrorValue to set
	 */
	public void setStateErrorValue(String pStateErrorValue) {
		mStateErrorValue = pStateErrorValue;
	}

	/**
	 * @return the combinationErrorValue
	 */
	public String getCombinationErrorValue() {
		return mCombinationErrorValue;
	}

	/**
	 * @param pCombinationErrorValue the combinationErrorValue to set
	 */
	public void setCombinationErrorValue(String pCombinationErrorValue) {
		mCombinationErrorValue = pCombinationErrorValue;
	}

	/**
	 * @return the mTRUConfiguration.
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * @param pTRUConfiguration the mTRUConfiguration to set.
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		this.mTRUConfiguration = pTRUConfiguration;
	}

	/**
	 * 
	 * @return the TRUShippingManager component
	 */
	public TRUShippingManager getShippingManager() {
		return mShippingManager;
	}

	/**
	 * 
	 * @param pShippingManager the pShippingManager to set
	 *
	 **/

	public void setShippingManager(TRUShippingManager pShippingManager) {
		mShippingManager = pShippingManager;
	}
	/**
	 * holds mShippingPricingEngine.
	 */
	private TRUShippingPricingEngine mShippingPricingEngine;

	/**
	 * @return the shippingPricingEngine.
	 */
	public TRUShippingPricingEngine getShippingPricingEngine() {
		return mShippingPricingEngine;
	}


	/**
	 * @param pShippingPricingEngine the shippingPricingEngine to set.
	 */
	public void setShippingPricingEngine(TRUShippingPricingEngine pShippingPricingEngine) {
		mShippingPricingEngine = pShippingPricingEngine;
	}

	/**
	 * Used to Render the JSON Response for mobile & registry.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			vlogDebug("TRUGetValidShippingMethodsDroplet.service :: START");
		}
		Order order = (Order) pRequest.getObjectParameter(TRUCheckoutConstants.ORDER);
		String state = (String)pRequest.getLocalParameter(TRUConstants.STATE);
		String city = (String)pRequest.getLocalParameter(TRUConstants.CITY);
		String postalCode = (String)pRequest.getLocalParameter(TRUConstants.POSTAL_CODE_FOR_ADD);
		String country = (String)pRequest.getLocalParameter(TRUConstants.COUNTRY_TO_ADD);
		boolean isvalidAddress=  validateGeoLocation(pRequest,state, postalCode,city,country);
		if(isvalidAddress){
			String region = TRURegionConstants.LOWER48.getRegion();
			String defaultShipMethod = TRUConstants.EMPTY;
			boolean apBuyNowFlow = Boolean.parseBoolean((String) pRequest.getLocalParameter(TRUConstants.APPLE_BUY_FLOW));
			TRUShippingPricingEngine shippingPricingEngine = getShippingPricingEngine();
			TRUShippingProcessHelper shippingHelper = shippingPricingEngine.getShippingHelper();
			if(order == null || order.getCommerceItems().isEmpty()){
				pRequest.serviceLocalParameter(TRUCheckoutConstants.OPARAM_EMPTY, pRequest, pResponse);
				return;
			}
			// setting to address object
			Address address = new Address();
			address.setState(state);
			address.setCity(city);
			region = getShippingManager().getShippingRegionforApplePay(address);
			List<TRUShipMethodVO> validShipMethods = getShippingManager().getShippingMethods(order,region,apBuyNowFlow);
			// setting defaultShipMethod
			defaultShipMethod = shippingHelper.getDefaultShipMethod(order, validShipMethods);

			pRequest.setParameter(TRUConstants.VALID_SHIP_METHODS, validShipMethods);
			pRequest.setParameter(TRUConstants.PREFERRED_SHIP_METHOD, defaultShipMethod);
			pRequest.serviceLocalParameter(TRUCheckoutConstants.OPARAM_OUTPUT, pRequest, pResponse);
		}else{
			pRequest.serviceLocalParameter(TRUConstants.ERROR, pRequest, pResponse);
			return;
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUGetValidShippingMethodsDroplet.service :: END");
		}
	}

	/**
	 * Validate geo location.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pState
	 *            the state
	 * @param pPostalCode
	 *            the postal code
	 * @param pCity
	 *            the city
	 * @param pCountry
	 *            the country
	 * @return true, if successful
	 */
	private boolean validateGeoLocation(DynamoHttpServletRequest pRequest, String pState, String pPostalCode, String pCity, String pCountry) {
		if (isLoggingDebug()) {
			vlogDebug("TRUGetValidShippingMethodsDroplet.validateGeoLocation :: START");
		}
		boolean response = false;
		boolean validAddress = true;
		try {
			if (StringUtils.isEmpty(pState) || StringUtils.isEmpty(pCountry) || StringUtils.isEmpty(pPostalCode) || StringUtils.isEmpty(pCity)) {
				pRequest.setParameter(TRUConstants.ERROR_VALUE, TRUConstants.REQUIRED_FIELD_MESSAGE);
				return response;
			}
			GeoApiContext context=null;
			if(getTRUConfiguration().isEnableGeoValidationfromClientId()){
				 context = new GeoApiContext().setEnterpriseCredentials(getTRUConfiguration().getClientID(), getTRUConfiguration().getClientSecret());
			}else{
				 context = new GeoApiContext().setApiKey(getTRUConfiguration().getGoogleMapApiKey());
			}
			if (context != null) {
				GeocodingResult[] results = GeocodingApi
						.geocode(context, pCity)
						.components(ComponentFilter.administrativeArea(pState), ComponentFilter.country(pCountry),
								ComponentFilter.postalCode(pPostalCode)).await();
				Gson gson = new Gson();
				if (isLoggingDebug()) {
					vlogDebug("TRUGetValidShippingMethodsDroplet.validateGeoLocation results:: ", gson.toJson(results));
				}
				JSONArray obj = new JSONArray(gson.toJson(results));
				if (obj.isEmpty()) {
					pRequest.setParameter(TRUConstants.ERROR_CODE, TRUConstants.INVALID_COMBINATION);
					pRequest.setParameter(TRUConstants.ERROR_VALUE, getCombinationErrorValue());
					response = false;
					return response;
				} else {
					JSONObject obj2 = obj.getJSONObject(0);
					if (obj2.getString(TRUConstants.PARTIAL_MATCH).equals(TRUConstants.TRUE) ) {
						JSONArray addressComponents = obj2.getJSONArray(TRUConstants.ADDRESS_COMPONENTS);
						int arrayLength = addressComponents.length();
						for (int i = 0; i < arrayLength; i++) {
							JSONObject objects = addressComponents.getJSONObject(i);
							JSONArray types = objects.getJSONArray(TRUConstants.TYPES);
							if (types.contains(TRUConstants.ADMINISTRATIVE_AREA_LEVEL_1)
									&& !(pState.equalsIgnoreCase(objects.getString(TRUConstants.SHORT_NAME))||pState.equalsIgnoreCase(objects.getString(TRUConstants.LONG_NAME)))) {
								pRequest.setParameter(TRUConstants.ERROR_CODE, TRUConstants.INVALID_COMBINATION);
								pRequest.setParameter(TRUConstants.ERROR_VALUE, getCombinationErrorValue());
								validAddress = false;
								return response;
							} else if (types.contains(TRUConstants.LOCALITY_ADDRESS_VAL)
									&& !pCity.equalsIgnoreCase(objects.getString(TRUConstants.SHORT_NAME))) {
								pRequest.setParameter(TRUConstants.ERROR_CODE, TRUConstants.INVALID_COMBINATION);
								pRequest.setParameter(TRUConstants.ERROR_VALUE, getCombinationErrorValue());
								validAddress = false;
								return response;
							} else if (types.contains(TRUConstants.POSTAL_CODE_ADDRESS_VAL)
									&& !pPostalCode.equalsIgnoreCase(objects.getString(TRUConstants.SHORT_NAME))) {
								pRequest.setParameter(TRUConstants.ERROR_CODE, TRUConstants.INVALID_COMBINATION);
								pRequest.setParameter(TRUConstants.ERROR_VALUE, getCombinationErrorValue());
								validAddress = false;
								return response;
							} else if (types.contains(TRUConstants.COUNTRY_ADDRESS_VAL)
									&& !(pCountry.equalsIgnoreCase(objects.getString(TRUConstants.SHORT_NAME))||pCountry.equalsIgnoreCase(objects.getString(TRUConstants.LONG_NAME)))) {
								pRequest.setParameter(TRUConstants.ERROR_CODE, TRUConstants.INVALID_COMBINATION);
								pRequest.setParameter(TRUConstants.ERROR_VALUE, getCombinationErrorValue());
								validAddress = false;
								return response;
						    }
						}
						if (validAddress) {
							response = true;
							return response;
						}
					} else {
						response = true;
						return response;
					}
				}
			}
		} catch (Exception exception) {
			if (isLoggingError()) {
				logError("TRUGetValidShippingMethodsDroplet.validateGeoLocation :", exception);
			}
			pRequest.setParameter(TRUConstants.ERROR_VALUE, TRUConstants.REQUIRED_FIELD_MESSAGE);
			return response;
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUGetValidShippingMethodsDroplet.validateGeoLocation :: END");
		}
		return response;
	}
}
