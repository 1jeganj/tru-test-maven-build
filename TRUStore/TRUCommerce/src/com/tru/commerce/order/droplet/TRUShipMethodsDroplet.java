package com.tru.commerce.order.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.order.TRUShippingManager;
import com.tru.commerce.order.TRUShippingTools;


/**
 * <p>
 * Order reconcilation droplet
 * </p>.
 *
 * @author PA
 */
public class TRUShipMethodsDroplet extends DynamoServlet {

	/** The Shipping tools. */
	private TRUShippingTools mShippingTools;
	
	/** The Shipping manager. */
	private TRUShippingManager mShippingManager;

	/**
	 * Gets the shipping tools.
	 *
	 * @return the shippingTools
	 */
	public TRUShippingTools getShippingTools() {
		return mShippingTools;
	}

	/**
	 * Sets the shipping tools.
	 *
	 * @param pShippingTools the shippingTools to set
	 */
	public void setShippingTools(TRUShippingTools pShippingTools) {
		mShippingTools = pShippingTools;
	}

	/**
	 * Gets the shipping manager.
	 *
	 * @return the shippingManager
	 */
	public TRUShippingManager getShippingManager() {
		return mShippingManager;
	}

	/**
	 * Sets the shipping manager.
	 *
	 * @param pShippingManager the shippingManager to set
	 */
	public void setShippingManager(TRUShippingManager pShippingManager) {
		mShippingManager = pShippingManager;
	}

	/**
	 * Service method.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if(isLoggingDebug()){
			vlogDebug("Start TRUShipMethodsDroplet.service()");
		}		
		if(isLoggingDebug()){
			vlogDebug("End TRUShipMethodsDroplet.service()");
		}
	}

	
}
