package com.tru.commerce.order.droplet;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.order.CreditCard;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.purchase.TRUPaymentGroupContainerService;
import com.tru.common.TRUConstants;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This credit card fetches all available credit card map from the profile and also the default credit card.
 * @author PA
 * @version 1.0
 */
public class TRUAvailableCreditCardsDroplet extends DynamoServlet {
	
	/**
	 * mPropertyManager holds TRUPropertyManager.
	 */
	private TRUPropertyManager mPropertyManager; 
	/**
	 * @return mPropertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager - TRUPropertyManager
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		this.mPropertyManager = pPropertyManager;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			vlogDebug("TRUAvailableCreditCardsDroplet.service :: START");
		}
		TRUPaymentGroupContainerService paymentGroupMapContainer = (TRUPaymentGroupContainerService) pRequest.getObjectParameter(TRUCheckoutConstants.PAYMENT_GROUP_MAP_CONTAINER);
		Order order = (Order) pRequest.getObjectParameter(TRUCheckoutConstants.ORDER);
		
		if (paymentGroupMapContainer == null || order == null) {
			pRequest.serviceLocalParameter(TRUCheckoutConstants.OPARAM_EMPTY, pRequest, pResponse);
			return;
		}
		
		Map<String, PaymentGroup> creditCardsMap = new LinkedHashMap<String, PaymentGroup>();
		String defaultCreditCardName = TRUCheckoutConstants.EMPTY_STRING;
		
			Map<String, PaymentGroup> paymentGroupMap = paymentGroupMapContainer.getPaymentGroupMap();
			if (paymentGroupMap != null && !paymentGroupMap.isEmpty()) {
				Set<String> paymentGroupKeys = paymentGroupMap.keySet();
				for (String paymentGroupKey : paymentGroupKeys) {
					if (StringUtils.isNotBlank(paymentGroupKey)) {
						PaymentGroup paymentGroup = paymentGroupMap.get(paymentGroupKey);
						if (paymentGroup instanceof CreditCard) {
							CreditCard creditCard = (CreditCard) paymentGroup;
							if (creditCard != null) {
								creditCardsMap.put(paymentGroupKey, creditCard);
							}
						}
					}
				}
			}
			//Start :: TUW-43619/46853 defect changes.
			Profile profile = (Profile)pRequest.getObjectParameter(TRUConstants.PROFILE);
			//Start :: TUW-56702
			defaultCreditCardName = (String)profile.getPropertyValue(getPropertyManager().getSelectedCCNickNamePropertyName());
			if(StringUtils.isBlank(defaultCreditCardName)){
				defaultCreditCardName = paymentGroupMapContainer.getDefaultPaymentGroupName();
			}
			//End :: TUW-56702
			if(StringUtils.isBlank(defaultCreditCardName)){
				String defaultPaymentGroupName = paymentGroupMapContainer.getDefaultPaymentGroupName();
				if (StringUtils.isNotBlank(defaultPaymentGroupName)) {
					PaymentGroup paymentGroup = paymentGroupMapContainer.getPaymentGroup(defaultPaymentGroupName);
					if (paymentGroup instanceof CreditCard) {
						CreditCard creditCard = (CreditCard) paymentGroup;
						if (creditCard != null) {
							defaultCreditCardName = defaultPaymentGroupName;
						}
					}
				}
			}
		
		pRequest.setParameter(TRUCheckoutConstants.DEFAULT_CREDIT_CARD_NAME, defaultCreditCardName);
		pRequest.setParameter(TRUCheckoutConstants.CREDIT_CARDS_MAP, creditCardsMap);
		if (creditCardsMap.isEmpty()) {
			pRequest.serviceLocalParameter(TRUCheckoutConstants.OPARAM_EMPTY, pRequest, pResponse);
		} else {
			pRequest.serviceLocalParameter(TRUCheckoutConstants.OPARAM_OUTPUT, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUAvailableCreditCardsDroplet.service :: END");
		}
	}
}
