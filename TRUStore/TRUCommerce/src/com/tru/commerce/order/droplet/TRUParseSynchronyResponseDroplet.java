package com.tru.commerce.order.droplet;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.integrations.synchrony.bean.SynchronySDPResponseBean;

/**
 * This Droplet Parses Synchrony Response. 
 * @author Professional Access
 * @version 1.0
 * 
 */
public class TRUParseSynchronyResponseDroplet extends DynamoServlet{
	
	/**
	 * Property to hold orderManager.
	 */
	private TRUOrderManager mOrderManager;
	
	private boolean mRestRequest;
	
	/**
	 * service method Parses Synchrony Response .
	 * 
	 * @see atg.servlet.DynamoServlet#service(atg.servlet.DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse).
	 * @param pRequest - reference to DynamoHttpServletRequest object.
	 * @param pResponse - reference to DynamoHttpServletResponse object.
	 * @throws ServletException - if any exception occurs in servicing the request.
	 * @throws IOException - if any exception occurs while writing the response to output stream.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Entering into method TRUParseSynchronyResponseDroplet.service :: START");
		}
		String synchronySDPParam = null;
		SynchronySDPResponseBean synchronySDPResponseObj = null;
		if(pRequest.getLocalParameter(TRUCommerceConstants.REST_REQUEST)!=null){
			mRestRequest = Boolean.parseBoolean((pRequest.getLocalParameter(TRUCommerceConstants.REST_REQUEST).toString()));
		}
		
		if (pRequest.getLocalParameter(TRUCheckoutConstants.SDP_PARAM) != null || mRestRequest) {
			if (isLoggingDebug()) {
				vlogDebug("SDP Before URL Encoding:: {0}",pRequest.getLocalParameter(TRUCheckoutConstants.SDP_PARAM));
			}
			
			if(mRestRequest){
				if (isLoggingDebug()) {
				vlogDebug("Request is From REST::");
				}
				synchronySDPParam = (String) pRequest.getLocalParameter(TRUCommerceConstants.SYNCHRONY_REQ_PARAM);
			} else {
				String decodedSDP = (String) pRequest.getLocalParameter(TRUCheckoutConstants.SDP_PARAM);
				synchronySDPParam = URLEncoder.encode(decodedSDP, TRUCheckoutConstants.UTF_EIGHT);
			}
			if (isLoggingDebug()) {
				vlogDebug("SDP After URL Encoding:: {0}",synchronySDPParam);
			}
			if(synchronySDPParam != null){
				synchronySDPResponseObj = getOrderManager().getSynchronyResponseBean(synchronySDPParam);
				if (isLoggingDebug()) {
					vlogDebug("synchronySDPResponseObj From OrderManager:: {0}",synchronySDPResponseObj);
				}
				if(synchronySDPResponseObj != null){
					pRequest.setParameter(TRUCommerceConstants.SYNCHRONY_SDRESPONSE_OBJECT, synchronySDPResponseObj);
					pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
				} else {
					pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
				}
			} else {
				pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
			}
		} else {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			vlogDebug("Exiting  method TRUParseSynchronyResponseDroplet.service :: END");
		}
	}

	/**
	 * @return the mOrderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * @param pOrderManager the orderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		this.mOrderManager = pOrderManager;
	}
}
