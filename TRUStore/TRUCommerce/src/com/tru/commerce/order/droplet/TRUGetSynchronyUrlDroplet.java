package com.tru.commerce.order.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.order.Order;
import atg.nucleus.naming.ComponentName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConstants;
import com.tru.common.TRUUserSession;
/**
 * This class TRUGetSynchronyUrlDroplet.
 * @author Professional access
 * @version 1.0
 */
public class TRUGetSynchronyUrlDroplet extends DynamoServlet{
	
	/**
	 * Property to hold orderManager.
	 */
	private TRUOrderManager mOrderManager;
	/**
	 * Property to hold paymentPageUrl.
	 */
	private String mPaymentPageUrl;
	
	/** The Constant TRU_SESSION_SCOPE_COMPONENT_NAME. */
	private static final ComponentName TRU_SESSION_SCOPE_COMPONENT_NAME = ComponentName.getComponentName(TRUCommerceConstants.TRU_SESSION_SCOPE_COMPONENT_NAME);
	
	/**
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the .response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Entering into method TRUGetSynchronyUrlDroplet.service :: START");
		}
		
		String synchronyUrl = null;
		final TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		final TRUUserSession sessionComponent = (TRUUserSession) pRequest.resolveName(TRU_SESSION_SCOPE_COMPONENT_NAME);
		final Order order = (Order) pRequest.getObjectParameter(TRUCommerceConstants.PARAM_ORDER);
		final String inputEmail = (String) pRequest.getLocalParameter(TRUCommerceConstants.INPUT_EMAIL);
		sessionComponent.setInputEmail(inputEmail);
		final String currentUrl = profileTools.generateURL(pRequest, getPaymentPageUrl() + TRUConstants.SYNCHRONY_DUMMY_PARAM);
		synchronyUrl = getOrderManager().getSynchronyUrl(order, currentUrl);
		pRequest.setParameter(TRUCommerceConstants.PARAM_REDIRECTION_URL, synchronyUrl);
		pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		if (isLoggingDebug()) {
			vlogDebug("Exiting  method TRUGetSynchronyUrlDroplet.service :: END");
		}
	}
	
	/**
	 * Gets the order manager.
	 *
	 * @return the mOrderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * Sets the order manager.
	 *
	 * @param pOrderManager the orderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		this.mOrderManager = pOrderManager;
	}

	/**
	 * Gets the payment page url.
	 *
	 * @return the mPaymentPageUrl
	 */
	public String getPaymentPageUrl() {
		return mPaymentPageUrl;
	}

	/**
	 * Sets the payment page url.
	 *
	 * @param pPaymentPageUrl the paymentPageUrl to set
	 */
	public void setPaymentPageUrl(String pPaymentPageUrl) {
		this.mPaymentPageUrl = pPaymentPageUrl;
	}
}
