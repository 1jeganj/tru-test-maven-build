package com.tru.commerce.order.droplet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import atg.nucleus.naming.ComponentName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConstants;
import com.tru.common.TRUUserSession;
import com.tru.integrations.synchrony.bean.SynchronySDPResponseBean;

/**
 * This Droplet helps to put TSP card data from Synchrony to user session.
 * @author Professional Access
 * @version 1.0
 */
public class TRUPopulateTSPInfoToUserSessionDroplet extends DynamoServlet{
	
	/** The Constant TRU_SESSION_SCOPE_COMPONENT_NAME. */
	private static final ComponentName TRU_SESSION_SCOPE_COMPONENT_NAME = ComponentName.getComponentName(TRUCommerceConstants.TRU_SESSION_SCOPE_COMPONENT_NAME);

	/** The Profile tools. */
	private TRUProfileTools mProfileTools;
	
	/**
	 * @return the profileTools
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * @param pProfileTools the profileTools to set
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	/**
	 * service method helps to put TSP card data to user Session.
	 * 
	 * @see atg.servlet.DynamoServlet#service(atg.servlet.DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse).
	 * @param pRequest - reference to DynamoHttpServletRequest object.
	 * @param pResponse - reference to DynamoHttpServletResponse object.
	 * @throws ServletException - if any exception occurs in servicing the request.
	 * @throws IOException - if any exception occurs while writing the response to output stream.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Entering into method TRUPopulateTSPInfoToUserSessionDroplet.service :: START");
		}
		
		final TRUUserSession sessionComponent = (TRUUserSession) pRequest.resolveName(TRU_SESSION_SCOPE_COMPONENT_NAME);
		SynchronySDPResponseBean synchronyResponseBean = (SynchronySDPResponseBean) pRequest.getLocalParameter(TRUCheckoutConstants.SYNCHRONY_SDP_RESPONSE_OBJ);
		if(synchronyResponseBean != null){
			if(sessionComponent.getTSPCardInfoMap() != null){
				sessionComponent.setTSPCardInfoMap(null);
			}
			Map<String, String> tspCardInfo = new HashMap<String, String>();
			tspCardInfo.put(TRUCommerceConstants.TSP_CARD_NUMBER, synchronyResponseBean.getTempAcctNumber());
			tspCardInfo.put(TRUCommerceConstants.TSP_CVV, synchronyResponseBean.getCVV());
			tspCardInfo.put(TRUCommerceConstants.TSP_EXP_MONTH, synchronyResponseBean.getPassMonth());
			tspCardInfo.put(TRUCommerceConstants.TSP_EXP_YEAR, synchronyResponseBean.getPassYear());
			tspCardInfo.put(TRUCommerceConstants.TSP_SINGLE_USE_COUPON_CODE, synchronyResponseBean.getSingleUseCouponCode());
			tspCardInfo.put(TRUCommerceConstants.TSP_OLSON_REWARD_NUMBER, synchronyResponseBean.getOlsonRewardNumber());
			tspCardInfo.put(TRUCommerceConstants.TSP_APPLICATION_DECISSION, synchronyResponseBean.getApplicationDecision());
			tspCardInfo.put(TRUCommerceConstants.ORDER_HAS_RUS_CARD, TRUCommerceConstants.TRUE_FLAG);
			String cardLength = String.valueOf(synchronyResponseBean.getTempAcctNumber().length());
			String creditCardType = getProfileTools().getCreditCardTypeFromRepository(synchronyResponseBean.getTempAcctNumber()
						,cardLength);
			tspCardInfo.put(TRUConstants.CREDIT_CARD_TYPE, creditCardType);
			sessionComponent.setTSPCardInfoMap(tspCardInfo);
		}
		
		if (isLoggingDebug()) {
			vlogDebug("Exiting method TRUPopulateTSPInfoToUserSessionDroplet.service :: END");
		}
	}
	
}
