package com.tru.commerce.order.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.order.Order;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.order.TRUPaymentGroupManager;
import com.tru.common.TRUConstants;
/**
 * This class is made to get the final order remaining amount after applying all the coupon codes.
 * Input parameter:- order : the Order Object
 * Output parameter:- finalRemaniningAmount : the amount left after applying all the coupons
 * Oparam Name :- output 
 * @author Professional Access
 * @version 1.0
 * 
 */
public class TRUOrderRemainingAmountDroplet extends DynamoServlet {
	
	private TRUPaymentGroupManager mPaymentGroupManager;
	
	/**
	 * @return the paymentGroupManager
	 */
	public TRUPaymentGroupManager getPaymentGroupManager() {
		return mPaymentGroupManager;
	}



	/**
	 * @param pPaymentGroupManager the paymentGroupManager to set
	 */
	public void setPaymentGroupManager(TRUPaymentGroupManager pPaymentGroupManager) {
		mPaymentGroupManager = pPaymentGroupManager;
	}



	/**
	 * This method is used to get the order remaining amount after applying all the promo code & gift cards.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Entering into method TRUOrderRemainingAmountDroplet.service :: START");
		}
		Order order = (Order) pRequest.getObjectParameter(TRUCommerceConstants.PARAM_ORDER);
		double finalRemaniningAmount = TRUConstants.DOUBLE_ZERO;
		if(order != null && order.getPriceInfo() != null){
			finalRemaniningAmount = getPaymentGroupManager().getRemainingOrderAmount(order);
			if (isLoggingDebug()) {
				vlogDebug("Entering into method TRUOrderRemainingAmountDroplet.service :: finalRemaniningAmount"+finalRemaniningAmount);
			}
		}
			pRequest.setParameter(TRUCommerceConstants.FINAL_REMAINING_AMOUNT, String.valueOf(finalRemaniningAmount));
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
			if (isLoggingDebug()) {
				vlogDebug("Entering into method TRUOrderRemainingAmountDroplet.service :: END");
			}
		
	}
	
}
