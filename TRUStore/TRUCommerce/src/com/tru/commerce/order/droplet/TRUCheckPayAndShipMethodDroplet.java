package com.tru.commerce.order.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.order.Order;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.order.TRUOrderManager;
/**
 * This class TRUCheckPayAndShipMethodDroplet.
 * @author PA
 * @version 1.0
 */
public class TRUCheckPayAndShipMethodDroplet extends DynamoServlet {
	
	/**
	 * hold the property orderManager.
	 */
	private TRUOrderManager mOrderManager;

	/**
	 * Gets the order manager.
	 *
	 * @return the orderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * Sets the order manager.
	 *
	 * @param pOrderManager the orderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	/**
	 * This method will check if order has payInStore Payment Group & storePickUp Shipping Group.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("TRUPayInStoreDroplet.service :: START");
		}
		boolean isInStorePayment = Boolean.FALSE;
		boolean isInStorePickupShippingGroup = Boolean.FALSE;
		final Order order = (Order) pRequest.getObjectParameter(TRUCommerceConstants.PARAM_ORDER);
		isInStorePayment = getOrderManager().isPayGroupInStorePayment(order);
		isInStorePickupShippingGroup = getOrderManager().isShipGroupInStorePickup(order);
		pRequest.setParameter(TRUCommerceConstants.PAY_IN_STORE_PAYMENT_GROUP, isInStorePayment);
		pRequest.setParameter(TRUCommerceConstants.STORE_PICKUP_SHIP_GROUP, isInStorePickupShippingGroup);
		pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		if (isLoggingDebug()) {
			vlogDebug("TRUCheckPayAndShipMethodDroplet.service :: END");
			vlogDebug("TRUCheckPayAndShipMethodDroplet, payInStorePaymentGroup:{0}", isInStorePayment);
			vlogDebug("TRUCheckPayAndShipMethodDroplet, storePickUpShipGroup:{0}", isInStorePickupShippingGroup);
		}
	}

}
