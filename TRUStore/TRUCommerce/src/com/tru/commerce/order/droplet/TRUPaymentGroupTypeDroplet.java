/**
 * TRUPayInStoreDroplet
 */
package com.tru.commerce.order.droplet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.PaymentGroup;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.order.TRUOrderManager;

/**
 * This droplet is used to check the order is eligible for pay in store or not. This droplet will the ordertools method isOrderEligibleForPayInStore.
 *  to validate the order for pay in store. It will return the boolean value.
 * @author Professional Access
 * @version 1.0
 */
public class TRUPaymentGroupTypeDroplet extends DynamoServlet {
	

	/**
	 * hold the property orderManager.
	 */
	private OrderManager mOrderManager;
	
	/**
	 * @return the orderManager
	 */
	public OrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * @param pOrderManager the orderManager to set
	 */
	public void setOrderManager(OrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	/**
	 * This method return will the boolean value either true and false. If it is true means.
	 *  cart is eligible for placing order with pay in store.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("TRUPayInStoreDroplet.service :: START");
		}
		Order order = (Order) pRequest.getObjectParameter(TRUCommerceConstants.PARAM_ORDER);
		
		TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
		
		// checking the payment group in order which is used to pre-select.
		String selectedPaymentGroupType = null;
		List<PaymentGroup> paymentGroups = order.getPaymentGroups();
		for (PaymentGroup paymentGroup : paymentGroups) {
			if (paymentGroup != null){
				selectedPaymentGroupType = paymentGroup.getPaymentGroupClassType();
			}
		}
		pRequest.setParameter(TRUCommerceConstants.SELECTED_PAYMENT_GRP_TYPE, selectedPaymentGroupType);

		// pay in store eligible check
		boolean payInStoreEligible = orderManager.isOrderEligibleForPayInStore(order);
		pRequest.setParameter(TRUCommerceConstants.PAY_IN_STORE_ELIGIBLE, payInStoreEligible);
		if (isLoggingDebug()) {
			vlogDebug("TRUPayInStoreDroplet, payInStoreEligible:{0}", payInStoreEligible);
		}
		
		pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		if (isLoggingDebug()) {
			vlogDebug("TRUPayInStoreDroplet.service :: END");
		}
	}
}
