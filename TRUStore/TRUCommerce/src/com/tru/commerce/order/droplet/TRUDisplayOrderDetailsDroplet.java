/**
 * 
 */
package com.tru.commerce.order.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ComponentName;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.cart.vo.TRUCartItemDetailVO;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.purchase.TRUShippingProcessHelper;
import com.tru.commerce.order.vo.TRUShippingDestinationVO;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConstants;
import com.tru.common.TRUUserSession;
import com.tru.userprofiling.TRUCookieManager;

/**
 * The Class TRUDisplayOrderDetailsDroplet.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUDisplayOrderDetailsDroplet extends DynamoServlet {

	/** The Constant TRU_SESSION_SCOPE_COMPONENT_NAME. */
	private static final ComponentName TRU_SESSION_SCOPE_COMPONENT_NAME = ComponentName
			.getComponentName(TRUCommerceConstants.TRU_SESSION_SCOPE_COMPONENT_NAME);

	/**
	 * property: Reference to the ShippingProcessHelper component.
	 */
	private TRUShippingProcessHelper mShippingHelper;
	/**
	 * property to Hold shopping cart utils.
	 */
	private TRUShoppingCartUtils mShoppingCartUtils;

	/**
	 * Gets the shipping helper.
	 *
	 * @return the shippingHelper
	 */
	public TRUShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * Sets the shipping helper.
	 *
	 * @param pShippingHelper the shippingHelper to set
	 */
	public void setShippingHelper(TRUShippingProcessHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}

	/**
	 * Gets the shopping cart utils.
	 *
	 * @return the shoppingCartUtils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * @param pShoppingCartUtils
	 *            the shoppingCartUtils to set
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		mShoppingCartUtils = pShoppingCartUtils;
	}

	/**
	 * Service.
	 *
	 * @param pRequest            the request
	 * @param pResponse            the response
	 * @throws ServletException             the servlet exception
	 * @throws IOException             Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START: TRUDisplayOrderDetailsDroplet.service");
		}
		boolean isAPIRequest = false;
		if (pRequest.getContextPath().equals(TRUCommerceConstants.REST_CONTEXT_PATH)) {
			isAPIRequest = true;
		}
		List<TRUCartItemDetailVO> outOfStockItemList = new ArrayList<TRUCartItemDetailVO>();
		final TRUOrderImpl order = (TRUOrderImpl) pRequest.getObjectParameter(TRUCommerceConstants.PARAM_ORDER);
		final RepositoryItem profile = (RepositoryItem) pRequest.getObjectParameter(TRUCommerceConstants.PROFILE);

		final TRUUserSession sessionComponent = (TRUUserSession) pRequest.resolveName(TRU_SESSION_SCOPE_COMPONENT_NAME);
		if (null != order && null != sessionComponent.getDeletedSkuList()) {
			final Set<String> deletedSkuList = (Set<String>) sessionComponent.getDeletedSkuList();
			if (!deletedSkuList.isEmpty()) {
				pRequest.setParameter(TRUCommerceConstants.ITEMS_REMOVED_INFO_MESSAGE, getShoppingCartUtils().getTRUCartModifierHelper()
						.getRemovedItemMessages(deletedSkuList));
				sessionComponent.setDeletedSkuList(null);
			}
		}
		if (null != order && null != sessionComponent.getQtyAdjustedSkuList() && !((List<String>) sessionComponent.getQtyAdjustedSkuList()).isEmpty()) {
			pRequest.setParameter(TRUCommerceConstants.ITEMS_QTY_ADJUSTED_INFO_MESSAGE,
					(List<String>) sessionComponent.getQtyAdjustedSkuList());
			sessionComponent.setQtyAdjustedSkuList(null);
		}

		String page = (String) pRequest.getObjectParameter(TRUCommerceConstants.PARAM_PAGE);
		if (isAPIRequest && StringUtils.isBlank(page)) {
			page = TRUCommerceConstants.SHIPPING;
		}
		final Map<String, TRUShippingDestinationVO> shippingDestinationVOs = new LinkedHashMap<String, TRUShippingDestinationVO>();
		final List<TRUCartItemDetailVO> donationsItemsList = new ArrayList<TRUCartItemDetailVO>();
		final Map<String, TRUCartItemDetailVO> cartItemDetailVOMap = new LinkedHashMap<String, TRUCartItemDetailVO>();
		try {
			final List sgCiRels = getShippingHelper().getPurchaseProcessHelper().getShippingGroupManager().getAllShippingGroupRelationships(order);
			if (sgCiRels != null && !sgCiRels.isEmpty()) {
				final int sgCiRelsCount = sgCiRels.size();
				for (int i = 0; i < sgCiRelsCount; ++i) {
					final TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) sgCiRels.get(i);
					final CommerceItem commerceItem = sgCiRel.getCommerceItem();
					if (commerceItem instanceof TRUGiftWrapCommerceItem) {
						continue;
					}
					if (commerceItem instanceof TRUDonationCommerceItem) {
						final TRUCartItemDetailVO cartItemDetailVO = getShoppingCartUtils().getDonationItemsVO(
								(TRUDonationCommerceItem) sgCiRel.getCommerceItem());
						donationsItemsList.add(cartItemDetailVO);
						continue;
					}

					getShoppingCartUtils().getShippingDestinationVOByRelationship(shippingDestinationVOs, sgCiRel, order, profile, page,
							isAPIRequest, cartItemDetailVOMap);
				}
			}
			if (!page.equalsIgnoreCase(TRUCommerceConstants.SHIPPING) && !((TRUOrderImpl) order).getOutOfStockItems().isEmpty()) {
				outOfStockItemList = getShoppingCartUtils().getTRUCartModifierHelper().createIndividualItemForOutOfStock(order);
			}
			if (order.isValidationFailed() && !TRUCommerceConstants.CONFIRM.equalsIgnoreCase(page)) {
				final TRUProfileTools profileTools = getShoppingCartUtils().getShippingHelper().getPurchaseProcessHelper().getProfileTools();
				if ((profileTools.isAnonymousUser(profileTools.getProfileForOrder(order)))) {
					final TRUCookieManager cookieManager = (TRUCookieManager) profileTools.getCookieManager();
					try {
						cookieManager.createCartCookie(order, pRequest, pResponse, TRUConstants.CART_COOKIE_NAME);
					} catch (CommerceException commerceException) {
						if (isLoggingError()) {
							vlogError("CommerceException @Class::TRUCartModifierFormHandler::@method::postAddItemToOrder() ",
									commerceException);
						}
					}
				} else {
					final TRUCookieManager cookieManager = (TRUCookieManager) profileTools.getCookieManager();
					try {
						cookieManager.createCartCookie(order, pRequest, pResponse, TRUConstants.LOGGEDEIN_CART_COOKIE_NAME);
					} catch (CommerceException commerceException) {
						if (isLoggingError()) {
							vlogError("CommerceException @Class::TRUCartModifierFormHandler::@method::postAddItemToOrder() ",
									commerceException);
						}
					}

				}
			}
			if (!isAPIRequest) {
				final Map<String, String> hookLogicDetails = getShoppingCartUtils().getHookLogicDetails(order);
				pRequest.setParameter(TRUCommerceConstants.HOOK_LOGIC_DETAILS, hookLogicDetails);	
			}
		} catch (CommerceException e) {
			if (isLoggingError()) {
				logError("CommerceException in TRUDisplayOrderDetailsDroplet.service()::", e);
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException in TRUDisplayOrderDetailsDroplet.service()::", e);
			}
		}
		pRequest.setParameter(TRUCommerceConstants.PARAM_OUT_OF_STOCK_ITEMS_LIST, outOfStockItemList);
		pRequest.setParameter(TRUCommerceConstants.PARAM_SHIPPINGVO, shippingDestinationVOs);
		if (null != order && TRUCommerceConstants.SHIPPING.equalsIgnoreCase(page)) {
			((TRUOrderImpl) order).setCartItemDetailVOMap(cartItemDetailVOMap);
		}
		pRequest.setParameter(TRUCommerceConstants.PARAM_DONATION_ITEM_LIST, donationsItemsList);
		if (shippingDestinationVOs.isEmpty() && donationsItemsList.isEmpty()) {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		} else {
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			vlogDebug("END: TRUDisplayOrderDetailsDroplet.service");
		}
	}
}
