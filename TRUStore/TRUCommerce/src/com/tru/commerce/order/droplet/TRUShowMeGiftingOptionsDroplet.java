/**
 * 
 */
package com.tru.commerce.order.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.order.Order;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;

/**
 * The Class TRUShowMeGiftingOptionsDroplet.
 *
 * @author PA
 * @version 1.0
 */
public class TRUShowMeGiftingOptionsDroplet extends DynamoServlet {
	
	/** property to Hold shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;

	/**
	 * This method is used to get the current order as input and iterates all the commerce items present in the current
	 * order and send the items in form of TRUGiftOptionsDetails  object.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Entering into method TRUShowMeGiftingOptionsDroplet.service :: START");
		}
		final Order order = (Order) pRequest.getObjectParameter(TRUCommerceConstants.PARAM_ORDER);
		
		if (order != null && !order.getCommerceItems().isEmpty()) {
			final boolean isOrderHasGiftWrappedItem = getShoppingCartUtils().isOrderHasGiftWrappedItem(order);
			pRequest.setParameter(TRUCommerceConstants.IS_ORDER_HAS_GW_ITEM, isOrderHasGiftWrappedItem);
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
			if (isLoggingDebug()) {
				vlogDebug("TRUShowMeGiftingOptionsDroplet.service :: ");
			}
		} else {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			vlogDebug("Exiting  method TRUShoppingCartDroplet.service :: END");
		}
	}

	/**
	 * Gets the shopping cart utils.
	 * 
	 * @return the shopping cart utils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * Sets the shopping cart utils.
	 * 
	 * @param pShoppingCartUtils
	 *            the new shopping cart utils
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		this.mShoppingCartUtils = pShoppingCartUtils;
	}

}
