package com.tru.commerce.order.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUUserSession;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This class TRUSynchronyCardHolderDroplet.
 * @author Professional access
 * @version 1.0
 */

public class TRUSynchronyCardHolderDroplet extends DynamoServlet{
	
	/**property to hold user session .*/
	private TRUUserSession mUserSession;
	
	/**property to hold tRUConfiguration .*/
 	private TRUConfiguration mTRUConfiguration;
 	
 	/** property to hold propertyManager. */
	private TRUPropertyManager mPropertyManager;

	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		
		String cardHolderName = null;
		Profile profile = (Profile) pRequest.getObjectParameter(TRUCommerceConstants.PROFILE);
		String firstName = (String) profile.getPropertyValue(getPropertyManager().getFirstNamePropertyName());
		String lastName =  (String) profile.getPropertyValue(getPropertyManager().getLastNamePropertyName());
		
		if(getUserSession().isBillingAddressSameAsShippingAddress() || getUserSession().isTSPEditBillingAddress()){
			cardHolderName = (getUserSession().getTSPBillingAddressMap().getFirstName() +
					TRUCheckoutConstants.SPACE + getUserSession().getTSPBillingAddressMap().getLastName());
		}else if (!profile.isTransient() && firstName != null && lastName != null){
			cardHolderName = (firstName + TRUCheckoutConstants.SPACE + lastName);
		}else{
			 cardHolderName = getTRUConfiguration().getDefaultSynchronyCardHolder();
		}
		
		pRequest.setParameter(TRUConstants.CARD_HOLDER_NAME, cardHolderName);
		pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
	}

	/**
	 * @return the mUserSession
	 */
	public TRUUserSession getUserSession() {
		return mUserSession;
	}

	/**
	 * @param pUserSession the mUserSession to set
	 */
	public void setUserSession(TRUUserSession pUserSession) {
		this.mUserSession = pUserSession;
	}

	/**
	 * @return the mTRUConfiguration
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * @param pTRUConfiguration the mTRUConfiguration to set
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		this.mTRUConfiguration = pTRUConfiguration;
	}
	
	/**
	 * @return the mPropertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager the mPropertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		this.mPropertyManager = pPropertyManager;
	}
}
