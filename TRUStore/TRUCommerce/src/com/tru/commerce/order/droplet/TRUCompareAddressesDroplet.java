package com.tru.commerce.order.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.profile.TRUProfileTools;

/**
 * This droplet is used to compare two addresses field by field.
 * @author PA
 * @version 1.0
 */
public class TRUCompareAddressesDroplet extends DynamoServlet {
	
	/**
	 * hold the property profile tools.
	 */
	private TRUProfileTools mProfileTools;
	
	/**
	 * Gets the profile tools.
	 *
	 * @return the profileTools
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * Sets the profile tools.
	 *
	 * @param pProfileTools the profileTools to set
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		this.mProfileTools = pProfileTools;
	}

	/**
	 * This methods compares two addresses.
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if(isLoggingDebug()) {
			vlogDebug("TRUCompareAddressesDroplet.service :: START");
		}
		
		boolean isAddressesMatching = false;
		
		final Object address1 = pRequest.getObjectParameter(TRUCommerceConstants.ADDRESS1_PARAM);
		final Object address2 = pRequest.getObjectParameter(TRUCommerceConstants.ADDRESS2_PARAM);
		
		isAddressesMatching = getProfileTools().compareAddresses(address1, address2);
		
		if(isAddressesMatching){
			pRequest.serviceLocalParameter(TRUCommerceConstants.TRUE, pRequest, pResponse);
		} else {
			pRequest.serviceLocalParameter(TRUCommerceConstants.FALSE, pRequest, pResponse);
		}
		
		if (isLoggingDebug()) {
			vlogDebug("TRUCompareAddressesDroplet.service :: END");
		}
	}
}
