package com.tru.commerce.order;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CreditCard;
import atg.commerce.order.OrderImpl;
import atg.commerce.order.OrderTools;
import atg.commerce.order.Relationship;
import atg.commerce.order.RepositoryAddress;
import atg.commerce.order.RepositoryContactInfo;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.core.util.Address;
import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.repository.MutableRepositoryItem;
import atg.repository.RemovedItemException;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.vo.TRUCartItemDetailVO;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.vo.TRUActiveAgreementsVO;
import com.tru.commerce.order.vo.TRUGiftItemInfo;
import com.tru.commerce.order.vo.TRUGiftOptionsInfo;
import com.tru.commerce.order.vo.TRUShipmentVO;
import com.tru.commerce.order.vo.TRUStorePickUpInfo;
import com.tru.common.TRUConstants;

/**
 * Extends the OrderImpl as part of TRU external values .
 * @author PA
 * @version 1.0
 */
public class TRUOrderImpl extends OrderImpl {

	/**
	 * 
	 */
	public Map<String,String> mCCAddressNickNameMap = new HashMap<String,String>();
	/**
	 * map to hold credit cards for annonymous users.
	 */
	public Map<String,CreditCard> mCreditCardMap = new HashMap<String,CreditCard>();
	
	/**
	 * map to hold secondary address for annoynymous user.
	 */
	public Map<String,Address> mSecondaryAddresses = new HashMap<String,Address>();
	/**
	 * generated serialVersionUID.
	 */
	private static final long serialVersionUID = -4631329661165131290L;

	/**
	 * Property to hold Credit Card Type.
	 */
	private String mCreditCardType;

	/**
	 * Property to hold the Previous Tab as part of check progress bar.
	 * 
	 */
	private String mPreviousTab;

	/**
	 * Property to hold the Current Tab as part of check progress bar.
	 * 
	 */
	private String mCurrentTab;
	
	/**
	 * Property to hold the next Tab as part of check progress bar.
	 * 
	 */
	private String mNextTab;

	/**
	 * Property to hold the Active Tabs as part of check progress bar.
	 * 
	 */
	private Map<String, Boolean> mActiveTabs;
	
	/**
	 * Property to hold the PAYPAL token.
	 * 
	 */
	private String mPaypalToken;
	
	/**
	 * Property to hold the mPayPalOrder.
	 * 
	 */
	private boolean mPayPalOrder;
	/**
	 * Property to hold the mGWItemInOrder.
	 * 
	 */
	private boolean mGWItemInOrder;
	
	/**
	 * Property to hold the PAYPAL PayerId.
	 * 
	 */
	private String mPaypalPayerId;
	/**
	 * Property to hold the ValidationFailed.
	 * 
	 */
	private boolean mValidationFailed;
	
	private List<TRUOutOfStockItem> mTRUOutOfStockItemsList;
	/**
	 * Property to hold Order is eligible for special financing or not.
	 */
	private boolean mOrderIsEligibleForFinancing;
	
	/** Property to hold available financing terms in order - transient property.*/
	private int mFinancingTermsAvailable;
	
	/**
	 * Property to hold Order Is In MultiShip.
	 */
	private boolean mOrderIsInMultiShip;
	
	
	/** The Cart item detail vo map. */
	private Map<String, TRUCartItemDetailVO> mCartItemDetailVOMap;
	
	/**
	 * property: holds shipments.
	 */
	private TRUShipmentVO mShipmentVO;

 	/**
 	 * Property to hold true once the user visited the shipping page and user is in payment page.
 	 * 
 	 */
 	private boolean mShippingVisitedPage;
    /**
 	 * Property to hold true once the user visited the shipping page and user is in payment page in CSS.
 	 * 
 	 */
 	private boolean mShippingVisitedPageInCSC;
	
		/**
	 * Property to hold the Current Tab as part of check progress bar.
	 * 
	 */
	private String mCurrentTabInCSC;
	/**
	 * Property to hold the mPgRemoved.
	 * 
	 */
	private boolean mPgRemoved;
	
	/** Property to hold if Rus radio is selscted. */
	private boolean mRusCardSelected;
	
	
	
	
	
	/** The m reprice required on cart. */
	private boolean mRepriceRequiredOnCart;
	
	
	/** The m ship restrictions found. */
	private boolean mShipRestrictionsFound;
	
	/** Property hold RetryCount */
	private int mRetryCount;
	
	
	/**
	 * Property to hold the Previous CreditCard Number.
	 * 
	 */
	private String mPreviousCreditCardNumber;
	
	
	/** property to hold mApplepay. */
	private boolean mApplepay;
	/**
	 * Checks if is Apple pay.
	 * 
	 * @return the mApplepay
	 */
	public boolean isApplepay() {
		return mApplepay;
	}

	/**
	 * Sets the Apple pay.
	 * 
	 * @param pApplepay
	 *            the pApplepay to set
	 */
	public void setApplepay(boolean pApplepay) {
		this.mApplepay = pApplepay;
	}
	/**
	 * @return the previousCreditCardNumber
	 */
	public String getPreviousCreditCardNumber() {
		return mPreviousCreditCardNumber;
	}

	/**
	 * @param pPreviousCreditCardNumber the previousCreditCardNumber to set
	 */
	public void setPreviousCreditCardNumber(String pPreviousCreditCardNumber) {
		mPreviousCreditCardNumber = pPreviousCreditCardNumber;
	}

	/**
	 * @return the retryCount
	 */
	public int getRetryCount() {
		return mRetryCount;
	}

	/**
	 * @param pRetryCount the retryCount to set
	 */
	public void setRetryCount(int pRetryCount) {
		mRetryCount = pRetryCount;
	}

	/**
	 * Checks if is ship restrictions found.
	 *
	 * @return true, if is ship restrictions found
	 */
	public boolean isShipRestrictionsFound() {
		return mShipRestrictionsFound;
	}

	/**
	 * Sets the ship restrictions found.
	 *
	 * @param pShipRestrictionsFound the new ship restrictions found
	 */
	public void setShipRestrictionsFound(boolean pShipRestrictionsFound) {
		this.mShipRestrictionsFound = pShipRestrictionsFound;
	}

	/**
	 * @return the validationFailed
	 */
	public boolean isValidationFailed() {
		return mValidationFailed;
	}

	/**
	 * @param pValidationFailed the validationFailed to set
	 */
	public void setValidationFailed(boolean pValidationFailed) {
		mValidationFailed = pValidationFailed;
	}

	/**
	 * @return the paypalToken
	 */
	public String getPaypalToken() {
		return mPaypalToken;
	}

	/**
	 * @param pPaypalToken the paypalToken to set
	 */
	public void setPaypalToken(String pPaypalToken) {
		this.mPaypalToken = pPaypalToken;
	}

	/**
	 * @return the paypalPayerId
	 */
	public String getPaypalPayerId() {
		return mPaypalPayerId;
	}

	/**
	 * @param pPaypalPayerId the paypalPayerId to set
	 */
	public void setPaypalPayerId(String pPaypalPayerId) {
		this.mPaypalPayerId = pPaypalPayerId;
	}

	/**
	 * @return the showMeGiftingOptions
	 */
	public boolean isShowMeGiftingOptions() {
		if(null == getPropertyValue(TRUCheckoutConstants.SHOW_ME_GIFTING_OPTIONS)){
			return false;
		}
		return (boolean) getPropertyValue(TRUCheckoutConstants.SHOW_ME_GIFTING_OPTIONS);
	}

	/**
	 * @param pShowMeGiftingOptions
	 *            the showMeGiftingOptions to set
	 */
	public void setShowMeGiftingOptions(boolean pShowMeGiftingOptions) {
		setPropertyValue(TRUCheckoutConstants.SHOW_ME_GIFTING_OPTIONS, pShowMeGiftingOptions);
	}
	
	/**
	 * @return the shipmentVO.
	 */
	public TRUShipmentVO getShipmentVO() {
		return mShipmentVO;
	}

	/**
	 * @param pShipmentVO the shipmentVO to set
	 */
	public void setShipmentVO(TRUShipmentVO pShipmentVO) {
		mShipmentVO = pShipmentVO;
	}

	/**
	 * @return the cartItemDetailVOMap
	 */
	public Map<String, TRUCartItemDetailVO> getCartItemDetailVOMap() {
		return mCartItemDetailVOMap;
	}

	/**
	 * @param pCartItemDetailVOMap the cartItemDetailVOMap to set
	 */
	public void setCartItemDetailVOMap(
			Map<String, TRUCartItemDetailVO> pCartItemDetailVOMap) {
		mCartItemDetailVOMap = pCartItemDetailVOMap;
	}

	/**
	 * @return the orderIsInMultiShip
	 */
	public boolean isOrderIsInMultiShip() {
		//Start:Code Added for defect-TUW-59065
		if((getCurrentTab() == TRUCheckoutConstants.SHIPPING_TAB) && (getShipToHomeItemsCount() == TRUConstants.ONE)){
			mOrderIsInMultiShip = Boolean.FALSE;
		}
		//End:Code Added for defect-TUW-59065
		return mOrderIsInMultiShip;
	}

	/**
	 * @param pOrderIsInMultiShip the orderIsInMultiShip to set
	 */
	public void setOrderIsInMultiShip(boolean pOrderIsInMultiShip) {
		mOrderIsInMultiShip = pOrderIsInMultiShip;
	}

	/**
	 * @return the creditCardType
	 */
	public String getCreditCardType() {
		return mCreditCardType;
	}

	/**
	 * @param pCreditCardType the creditCardType to set
	 * 
	 */
	public void setCreditCardType(String pCreditCardType) {
		mCreditCardType = pCreditCardType;
	}

	/** The m gift options details. */
	private Map<String, TRUGiftOptionsInfo> mGiftOptionsDetails;
	
	/** The m store pick up infos. */
	private Map<String, TRUStorePickUpInfo> mStorePickUpInfos=new LinkedHashMap<String, TRUStorePickUpInfo>();

	
	
	
	/**
	 * @return the storePickUpInfos
	 */
	public Map<String, TRUStorePickUpInfo> getStorePickUpInfos() {
		return mStorePickUpInfos;
	}

	/**
	 * @param pStorePickUpInfos the storePickUpInfos to set
	 */
	public void setStorePickUpInfos(Map<String, TRUStorePickUpInfo> pStorePickUpInfos) {
		this.mStorePickUpInfos = pStorePickUpInfos;
	}

	/**
	 * @return the giftOptionsDetails
	 */
	public Map<String, TRUGiftOptionsInfo> getGiftOptionsDetails() {
		return mGiftOptionsDetails;
	}

	/**
	 * @param pGiftOptionsDetails the giftOptionsDetails to set
	 */
	public void setGiftOptionsDetails(
			Map<String, TRUGiftOptionsInfo> pGiftOptionsDetails) {
		mGiftOptionsDetails = pGiftOptionsDetails;
	}

	/** The m split gift items. */
	Map<String, TRUGiftItemInfo> mSplitGiftItems;

	/**
	 * @return the splitGiftItems
	 */
	public Map<String, TRUGiftItemInfo> getSplitGiftItems() {
		return mSplitGiftItems;
	}

	/**
	 * @param pSplitGiftItems the splitGiftItems to set
	 */
	public void setSplitGiftItems(Map<String, TRUGiftItemInfo> pSplitGiftItems) {
		mSplitGiftItems = pSplitGiftItems;
	}

	/**
	 * @return the previousTab
	 */
	public String getPreviousTab() {
		return mPreviousTab;
	}

	/**
	 * @param pPreviousTab the previousTab to set
	 */
	public void setPreviousTab(String pPreviousTab) {
		mPreviousTab = pPreviousTab;
	}

	/**
	 * @return the currentTab
	 */
	public String getCurrentTab() {
		return mCurrentTab;
	}

	/**
	 * @param pCurrentTab the currentTab to set
	 */
	public void setCurrentTab(String pCurrentTab) {
		mCurrentTab = pCurrentTab;
	}
	
	/**
	 * @return the creditCardMap
	 */
	public Map<String, CreditCard> getCreditCardMap() {
		return mCreditCardMap;
	}

	/**
	 * @param pCreditCardMap the creditCardMap to set
	 */
	public void setCreditCardMap(Map<String, CreditCard> pCreditCardMap) {
		mCreditCardMap = pCreditCardMap;
	}

	/**
	 * @return the secondaryAddresses
	 */
	public Map<String, Address> getSecondaryAddresses() {
		return mSecondaryAddresses;
	}

	/**
	 * @param pSecondaryAddresses the secondaryAddresses to set
	 */
	public void setSecondaryAddresses(Map<String, Address> pSecondaryAddresses) {
		mSecondaryAddresses = pSecondaryAddresses;
	}

	/**
	 * @return the cCAddressNickNameMap
	 */
	public Map<String, String> getCCAddressNickNameMap() {
		return mCCAddressNickNameMap;
	}

	/**
	 * @param pCCAddressNickNameMap the cCAddressNickNameMap to set
	 */
	public void setCCAddressNickNameMap(Map<String, String> pCCAddressNickNameMap) {
		mCCAddressNickNameMap = pCCAddressNickNameMap;
	}
	
	/**
	 * @return the OrderIsInStorePickUp
	 */
	@SuppressWarnings("unchecked")
	public boolean isOrderIsInStorePickUp() {
		List<Relationship> listOfRel = getRelationships();
		ShippingGroup shipGrp = null;
		for (Relationship rel : listOfRel) {
			if (rel instanceof TRUShippingGroupCommerceItemRelationship) {
				shipGrp = ((TRUShippingGroupCommerceItemRelationship) rel).getShippingGroup();
				if (shipGrp instanceof TRUInStorePickupShippingGroup) {
					return Boolean.TRUE;
				}
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * get the email property of order.
	 * @return email - String
	 */
	public String getEmail() {
	    return (String) getPropertyValue(TRUCheckoutConstants.EMAIL);
	}
	
	/**
	 * Set the email property in order.
	 * @param pEmail email
	 */
	public void setEmail(String pEmail) {
	    setPropertyValue(TRUCheckoutConstants.EMAIL, pEmail.trim());
	}	
	
	/**
	 * get the reward number property of order.
	 * @return string
	 */
	public String getRewardNumber() {
	    return (String) getPropertyValue(TRUCheckoutConstants.REWARD_NUMBER);
	}
	
	/**
	 * Set the reward number property in order.
	 * @param pRewardNumber rewardNumber
	 */
	public void setRewardNumber(String pRewardNumber) {
	    setPropertyValue(TRUCheckoutConstants.REWARD_NUMBER, pRewardNumber.trim());
	}
	
	/**
	 * @return the activeTabs
	 */
	public Map<String, Boolean> getActiveTabs() {
		if(mActiveTabs == null){
			mActiveTabs = new LinkedHashMap<String, Boolean>();
		}
		return mActiveTabs;
	}

	/**
	 * @param pActiveTabs the activeTabs to set
	 */
	public void setActiveTabs(Map<String, Boolean> pActiveTabs) {
		if(pActiveTabs == null){
			mActiveTabs = new LinkedHashMap<String, Boolean>();
		}
		mActiveTabs = pActiveTabs;
	}
	
	/**
	 * Gets the out of stock items.
	 *
	 * @return the out of stock items
	 */
	public List<TRUOutOfStockItem> getOutOfStockItems() {
		if (mTRUOutOfStockItemsList == null) {
			mTRUOutOfStockItemsList = new ArrayList<TRUOutOfStockItem>();
		}
		return mTRUOutOfStockItemsList;
	}

	/**
	 * Sets the out of stock items.
	 *
	 * @param pOutOfStockItems the new out of stock items
	 */
	public void setOutOfStockItems(List<TRUOutOfStockItem> pOutOfStockItems) {
		mTRUOutOfStockItemsList = pOutOfStockItems;
		if (null != pOutOfStockItems && !pOutOfStockItems.isEmpty()) {
			List<MutableRepositoryItem> mutItems = new ArrayList<MutableRepositoryItem>();
			for (TRUOutOfStockItem outOfStockItem : pOutOfStockItems) {
				if(null != outOfStockItem.getRepositoryItem()) {
					mutItems.add(outOfStockItem.getRepositoryItem());
				}
			}
			setPropertyValue(TRUCheckoutConstants.OUT_OF_STOCK_ITEMS, mutItems);
		} else {
			setPropertyValue(TRUCheckoutConstants.OUT_OF_STOCK_ITEMS, null);
		}
	}
	
	
	
	/**
	 * Returns the IP address.
	 * 
	 * @return String - ipAddress
	 */
	public String getIpAddress() {

		return (String) getPropertyValue(TRUCheckoutConstants.IP_ADDRESS);
	}

	/**
	 * Sets the orders IP Address.
	 * 
	 * @param pIpAddress
	 *            String
	 */
	public void setIpAddress(String pIpAddress) {

		setPropertyValue(TRUCheckoutConstants.IP_ADDRESS, pIpAddress);

	}

	/**
	 * @return the orderIsEligibleForFinancing
	 */
	public boolean isOrderIsEligibleForFinancing() {
		return mOrderIsEligibleForFinancing;
	}

	/**
	 * @param pOrderIsEligibleForFinancing the orderIsEligibleForFinancing to set
	 */
	public void setOrderIsEligibleForFinancing(boolean pOrderIsEligibleForFinancing) {
		this.mOrderIsEligibleForFinancing = pOrderIsEligibleForFinancing;
	}

	/**
	 * get the financingTermsAvailed property of order.
	 * @return integer
	 */
	public int getFinancingTermsAvailed() {
		if(getPropertyValue(TRUCheckoutConstants.FINANCING_TERMS_AVAILED)==null){
			return 0;
		}
	    return (int) getPropertyValue(TRUCheckoutConstants.FINANCING_TERMS_AVAILED);
	}
	
	/**
	 * Set the financingTermsAvailed property in order.
	 * @param pFinancingTermsAvailed financingTermsAvailed
	 */
	public void setFinancingTermsAvailed(int pFinancingTermsAvailed) {
	    setPropertyValue(TRUCheckoutConstants.FINANCING_TERMS_AVAILED, pFinancingTermsAvailed);
	}
	
	/**
	 * get the financeAgreed property of order.
	 * @return string
	 */
	public boolean isFinanceAgreed() {
		if(getPropertyValue(TRUCheckoutConstants.FINANCE_AGREED)==null){
			return false;
		}
		   return (boolean) getPropertyValue(TRUCheckoutConstants.FINANCE_AGREED);
	}
	
	/**
	 * Set the financeAgreed property in order.
	 * @param pFinanceAgreed financeAgreed
	 */
	public void setFinanceAgreed(boolean pFinanceAgreed) {
	    setPropertyValue(TRUCheckoutConstants.FINANCE_AGREED, pFinanceAgreed);
	}
	
	/**
	 * @return the financingTermsAvailable
	 */
	public int getFinancingTermsAvailable() {
		return mFinancingTermsAvailable;
	}

	/**
	 * @param pFinancingTermsAvailable the financingTermsAvailable to set
	 */
	public void setFinancingTermsAvailable(int pFinancingTermsAvailable) {
		this.mFinancingTermsAvailable = pFinancingTermsAvailable;
	}

	/**
	 * is Order Has Ship To Home Items or not.
	 * @return true/false
	 */
	@SuppressWarnings("unchecked")
	public boolean isOrderHasNonRegistryItems() {
		final List<ShippingGroup> shippingGroups = getShippingGroups();
		for (ShippingGroup shipGrp : shippingGroups) {
			if (shipGrp instanceof TRUHardgoodShippingGroup && !(shipGrp instanceof TRUChannelHardgoodShippingGroup)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * is Order Has Ship To Home Items or not.
	 * @return true/false
	 */
	@SuppressWarnings("unchecked")
	public boolean isOrderHasWishListItems() {
		final List<ShippingGroup> shippingGroups = getShippingGroups();
		for (ShippingGroup shipGrp : shippingGroups) {
			if (shipGrp instanceof TRUChannelHardgoodShippingGroup ) {
				TRUChannelHardgoodShippingGroup channelShippingGroup = (TRUChannelHardgoodShippingGroup)shipGrp;
				if(channelShippingGroup.getChannelType() != null &&  
						channelShippingGroup.getCommerceItemRelationshipCount()>TRUConstants._0 &&
						channelShippingGroup.getChannelType().equalsIgnoreCase(TRUCommerceConstants.WISHLIST)){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * is Order Has Registry Items or not.
	 * @return true/false
	 */
	@SuppressWarnings("unchecked")
	public boolean isOrderHasRegistryItems(){
		List<ShippingGroup> shippingGroups = getShippingGroups();
		for (ShippingGroup shipGrp : shippingGroups) {
			if (shipGrp instanceof TRUChannelHardgoodShippingGroup ) {
				TRUChannelHardgoodShippingGroup channelShippingGroup = (TRUChannelHardgoodShippingGroup)shipGrp;
				if(channelShippingGroup.getChannelType() != null &&
						channelShippingGroup.getCommerceItemRelationshipCount()>TRUConstants._0 &&
						channelShippingGroup.getChannelType().equalsIgnoreCase(TRUCommerceConstants.REGISTRY)){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * is Order Eligible For Multi Shipping.
	 * @return true/false.
	 */
	public boolean isOrderEligibleForMultiShipping() {
		return (getShipToHomeItemsCount() > TRUConstants.ONE);
	}
	
	/**
	 * is Order Eligible For Single Shipping.
	 * When no registry/wish items in order, then every order is eligible for single shipping
	 * @return true/false
	 */
	public boolean isOrderEligibleForSingleShipping() {
		return !(isOrderHasWishListItems() || isOrderHasRegistryItems());
	}
	
	/**
	 * is Order Mandate For Multi Shipping.
	 * When registry/wish items or more than one hardgoodShippingGroup in order, then every order is mandate for multi shipping
	 * @return true/false
	 */
	public boolean isOrderMandateForMultiShipping() {
		return isOrderHasWishListItems() || isOrderHasRegistryItems() || (getHardgoodShippingGroupsCount() > TRUConstants.ONE);
	}
	
	/**
	 * @return Ship To Home Items Count
	 */
	@SuppressWarnings("unchecked")
	public long getShipToHomeItemsCount(){
		long quantity = 0;
		List<ShippingGroup> shippingGroups = getShippingGroups();
		for (ShippingGroup shipGrp : shippingGroups) {
			if (shipGrp instanceof TRUHardgoodShippingGroup) {
				List<ShippingGroupCommerceItemRelationship> commerceItemRelationships = shipGrp.getCommerceItemRelationships();
				for (ShippingGroupCommerceItemRelationship sgCIR : commerceItemRelationships) {
					if (commerceItemRelationships == null || commerceItemRelationships.isEmpty()){
						continue;
					}
					if (! (sgCIR.getCommerceItem() instanceof TRUGiftWrapCommerceItem || sgCIR.getCommerceItem() instanceof TRUDonationCommerceItem)){
						quantity +=sgCIR.getQuantity();
					}
				}
			}
		}
		return quantity;
	}
		
	
	/**
	 * Checks if is invalidated.
	 *
	 * @return true, if is invalidated
	 */
	public boolean isInvalidated(){
		return this.mInvalidated;
	}
	
	/**
	 * @return Ship To Home Items Count
	 */
	@SuppressWarnings("unchecked")
	public long getHardgoodShippingGroupsCount(){
		long count = 0;
		List<ShippingGroup> shippingGroups = getShippingGroups();
		for (ShippingGroup shipGrp : shippingGroups) {
			if (shipGrp instanceof TRUHardgoodShippingGroup) {
				List<ShippingGroupCommerceItemRelationship> commerceItemRelationships = shipGrp.getCommerceItemRelationships();
					if (commerceItemRelationships != null && !commerceItemRelationships.isEmpty()){
						count += TRUConstants.ONE;
					}
			}
		}
		return count;
	}
	
	
	/**
	 * Gets the total item count.
	 *
	 * @return the total item count
	 * @throws RemovedItemException the removed item exception
	 */
	@SuppressWarnings("unchecked")
	public long getTotalItemCount() throws RemovedItemException{
		long total = TRUCommerceConstants.LONG_ZERO;
		List<CommerceItem> commerceItems = getCommerceItems();
		for (CommerceItem commerceItem : commerceItems) {
			if (!(commerceItem instanceof TRUGiftWrapCommerceItem)) {
				total += commerceItem.getQuantity();
			}
		}
		return total;
	}
		
	
	
	/**
	 * Returns the boolean if payment done via PAYPAL.
	 * 
	 * @return the mPayPalOrder
	 */
	public boolean isPayPalOrder() {
		return mPayPalOrder;
	}
	
	/**
	 * Sets payPal to true or false based on if there is any PAYPAL payment done.
	 * 
	 * @param pPayPalOrder
	 *            the boolean value to set
	 */
	public void setPayPalOrder(boolean pPayPalOrder) {
		this.mPayPalOrder = pPayPalOrder;
	}
	
	/**
	 * Returns the boolean if gift wrap items are present in order or not.
	 * 
	 * @return the mGWItemInOrder
	 */
	public boolean isGWItemInOrder() {
		return mGWItemInOrder;
	}
	
	/**
	 * Sets GWItemInOrder to true or false based on if there are gift wrap items or not.
	 * 
	 * @param pGWItemInOrder
	 *            the boolean value to set
	 */
	public void setGWItemInOrder(boolean pGWItemInOrder) {
		this.mGWItemInOrder = pGWItemInOrder;
	}
	
	/**
	 * property: flag indicating whether billing address will be same as shipping address.
	 */
	private boolean mBillingAddressSameAsShippingAddress = Boolean.FALSE;
	
	/**
	 * @return the billingAddressSameAsShippingAddress
	 */
	public boolean isBillingAddressSameAsShippingAddress() {
		return mBillingAddressSameAsShippingAddress;
	}

	/**
	 * @param pBillingAddressSameAsShippingAddress the billingAddressSameAsShippingAddress to set
	 */
	public void setBillingAddressSameAsShippingAddress(
			boolean pBillingAddressSameAsShippingAddress) {
		mBillingAddressSameAsShippingAddress = pBillingAddressSameAsShippingAddress;
	}
	
	/**
     * @return the singleUseCoupon
     */
     @SuppressWarnings("unchecked")
     public Map<String, String> getSingleUseCoupon() {
                     return ((Map<String, String>)getPropertyValue(TRUPropertyNameConstants.SINGLE_USE_COUPON));
     }
     
     /**
     * @param pSingleUseCoupon the singleUseCoupon to set
     */
     public void setSingleUseCoupon(Map<String, String> pSingleUseCoupon) {
                     setPropertyValue(TRUPropertyNameConstants.SINGLE_USE_COUPON, pSingleUseCoupon);
     }

	/**
	 * @return the shippingVisitedPage
	 */
	public boolean isShippingVisitedPage() {
		return mShippingVisitedPage;
	}

	/**
	 * @param pShippingVisitedPage the shippingVisitedPage to set
	 */
	public void setShippingVisitedPage(boolean pShippingVisitedPage) {
		mShippingVisitedPage = pShippingVisitedPage;
	}
	/**
	 * @return the shippingVisitedPageInCSC
	 */
	public boolean isShippingVisitedPageInCSC() {
		return mShippingVisitedPageInCSC;
	}

	/**
	 * @param pShippingVisitedPageInCSC the shippingVisitedPageInCSC to set
	 */
	public void setShippingVisitedPageInCSC(boolean pShippingVisitedPageInCSC) {
		mShippingVisitedPageInCSC = pShippingVisitedPageInCSC;
	}

	/**
	 * @return the currentTabInCSC
	 */
	public String getCurrentTabInCSC() {
		return mCurrentTabInCSC;
	}

	/**
	 * @param pCurrentTabInCSC the currentTabInCSC to set
	 */
	public void setCurrentTabInCSC(String pCurrentTabInCSC) {
		mCurrentTabInCSC = pCurrentTabInCSC;
	}
	
	/**
	 * get the sourceOfOrder property of order.
	 * @return string
	 */
	public String getSourceOfOrder() {
	    return (String) getPropertyValue(TRUCheckoutConstants.SOURCE_OF_ORDER);
	}
	
	/**
	 * Set the sourceOfOrder property in order.
	 * @param pSourceOfOrder sourceOfOrder
	 */
	public void setSourceOfOrder(String pSourceOfOrder) {
	    setPropertyValue(TRUCheckoutConstants.SOURCE_OF_ORDER, pSourceOfOrder);
	}
	
	/**
	 * @return the nextTab
	 */
	public String getNextTab() {
		return mNextTab;
	}

	/**
	 * @param pNextTab the nextTab to set
	 */
	public void setNextTab(String pNextTab) {
		mNextTab = pNextTab;
	}
	/**
     * @return the notAppliedCouponCode
     */
     @SuppressWarnings("unchecked")
     public Map<String, String> getNotAppliedCouponCode() {
                     return ((Map<String, String>)getPropertyValue(TRUPropertyNameConstants.NOT_APPLIED_COUPON_CODE));
     }
     
     /**
     * @param pNotAppliedCouponCode the notAppliedCouponCode to set
     */
     public void setNotAppliedCouponCode(Map<String, String> pNotAppliedCouponCode) {
                     setPropertyValue(TRUPropertyNameConstants.NOT_APPLIED_COUPON_CODE, pNotAppliedCouponCode);
     }
     /**
 	 * get the enteredBy property of order.
 	 * @return string
 	 */
 	public String getEnteredBy() {
 	    return (String) getPropertyValue(TRUCheckoutConstants.ENTERED_BY);
 	}
 	
 	/**
 	 * Set the enteredBy property in order.
 	 * @param pEnteredBy enteredBy
 	 */
 	public void setEnteredBy(String pEnteredBy) {
 	    setPropertyValue(TRUCheckoutConstants.ENTERED_BY, pEnteredBy);
 	}
 	/**
 	 * get the enteredLocation property of order.
 	 * @return string
 	 */
 	public String getEnteredLocation() {
 	    return (String) getPropertyValue(TRUCheckoutConstants.ENTERED_LOCATION);
 	}
 	
 	/**
 	 * Set the enteredLocation property in order.
 	 * @param pEnteredLocation enteredLocation
 	 */
 	public void setEnteredLocation(String pEnteredLocation) {
 	    setPropertyValue(TRUCheckoutConstants.ENTERED_LOCATION, pEnteredLocation);
 	}

	/**
	 * @return the pgRemoved
	 */
	public boolean isPgRemoved() {
		return mPgRemoved;
	}

	/**
	 * @param pPgRemoved the pgRemoved to set
	 */
	public void setPgRemoved(boolean pPgRemoved) {
		mPgRemoved = pPgRemoved;
	}
	
	/**
	 * @return the applyExpressCheckout
	 */
	public boolean isApplyExpressCheckout() {
		if(getPropertyValue(TRUCheckoutConstants.APPLY_EXPRESS_CHECKOUT)==null){
			return false;
		}
		return (boolean) getPropertyValue(TRUCheckoutConstants.APPLY_EXPRESS_CHECKOUT);
	}

	/**
	 * @param pApplyExpressCheckout the applyExpressCheckout to set
	 */
	public void setApplyExpressCheckout(boolean pApplyExpressCheckout) {
		 setPropertyValue(TRUCheckoutConstants.APPLY_EXPRESS_CHECKOUT, pApplyExpressCheckout);
	}

	/**
	 * @return the mRusCardSelected
	 */
	public boolean isRusCardSelected() {
		return mRusCardSelected;
	}

	/**
	 * @param pRusCardSelected the mRusCardSelected to set
	 */
	public void setRusCardSelected(boolean pRusCardSelected) {
		this.mRusCardSelected = pRusCardSelected;
	}

	/** The Apply gifting options. */
	private boolean mApplyGiftingOptions;

	/**
	 * @return the applyGiftingOptions
	 */
	public boolean isApplyGiftingOptions() {
		return mApplyGiftingOptions;
	}

	/**
	 * @param pApplyGiftingOptions the applyGiftingOptions to set
	 */
	public void setApplyGiftingOptions(boolean pApplyGiftingOptions) {
		mApplyGiftingOptions = pApplyGiftingOptions;
	}

	
	/**
	 * @return Ship To Home Items Count
	 */
	@SuppressWarnings("unchecked")
	public List<CommerceItem> getS2HItems(){
		List<CommerceItem> s2hItems=new ArrayList<CommerceItem>();
		List<ShippingGroup> shippingGroups = getShippingGroups();
		for (ShippingGroup shipGrp : shippingGroups) {
			if (shipGrp instanceof TRUHardgoodShippingGroup) {
				List<ShippingGroupCommerceItemRelationship> commerceItemRelationships = shipGrp.getCommerceItemRelationships();
					if (commerceItemRelationships != null && !commerceItemRelationships.isEmpty()){
						for(ShippingGroupCommerceItemRelationship itemRel:commerceItemRelationships){
							if(itemRel.getCommerceItem() instanceof TRUDonationCommerceItem || itemRel.getCommerceItem() instanceof TRUGiftWrapCommerceItem ){
								continue;
							}else{
								s2hItems.add(itemRel.getCommerceItem());
							}							
						}
					}
			}
		}
		return s2hItems;
	}
	
	

	/**
	 * Gets the only hardgood shipping groups.
	 *
	 * @return the only hardgood shipping groups
	 */
	@SuppressWarnings("unchecked")
	public List<TRUHardgoodShippingGroup> getOnlyHardgoodShippingGroups(){
		List<TRUHardgoodShippingGroup> groups=new ArrayList<TRUHardgoodShippingGroup>();
		List<ShippingGroup> shippingGroups = getShippingGroups();
		for (ShippingGroup shipGrp : shippingGroups) {
			if (shipGrp.getCommerceItemRelationshipCount()>0 && shipGrp instanceof TRUHardgoodShippingGroup && !(shipGrp instanceof TRUChannelHardgoodShippingGroup)) {
				groups.add((TRUHardgoodShippingGroup)shipGrp);
			}
		}
		return groups;
	}

	/**
	 * @return the repriceRequiredOnCart
	 */
	public boolean isRepriceRequiredOnCart() {
		return mRepriceRequiredOnCart;
	}

	
	/**
	 * Sets the reprice required on cart.
	 *
	 * @param pRepriceRequiredOnCart the new reprice required on cart
	 */
	public void setRepriceRequiredOnCart(boolean pRepriceRequiredOnCart) {
		this.mRepriceRequiredOnCart = pRepriceRequiredOnCart;
	}

	
	/**
	 * Gets the all hardgood shipping groups.
	 *
	 * @return the hardgood shipping groups
	 */
	@SuppressWarnings("unchecked")
	public List<TRUHardgoodShippingGroup> getAllHardgoodShippingGroups(){
		List<TRUHardgoodShippingGroup> groups=new ArrayList<TRUHardgoodShippingGroup>();
		List<ShippingGroup> shippingGroups = getShippingGroups();
		for (ShippingGroup shipGrp : shippingGroups) {
			if (shipGrp instanceof TRUHardgoodShippingGroup) {
				groups.add((TRUHardgoodShippingGroup)shipGrp);
			}
		}
		return groups;
	}
	
	/**
	 * Gets the ship to home shipping methods count.
	 *
	 * @return Ship To Home Shipping Methods Count
	 */
	@SuppressWarnings("unchecked")
	public long getShipToHomeShippingMethodsCount(){
		long quantity = 0;
		List<ShippingGroup> shippingGroups = getShippingGroups();
		for (ShippingGroup shipGrp : shippingGroups) {
			if (shipGrp instanceof TRUHardgoodShippingGroup && StringUtils.isNotBlank(shipGrp.getShippingMethod())) {
				quantity +=TRUConstants._1;
			}
		}
		return quantity;
	}	
	
	
	/**
	 * Gets the only channel hardgood shipping groups.
	 *
	 * @return the only channel hardgood shipping groups
	 */
	@SuppressWarnings("unchecked")
	public List<TRUChannelHardgoodShippingGroup> getOnlyChannelHardgoodShippingGroups(){
		List<TRUChannelHardgoodShippingGroup> groups=new ArrayList<TRUChannelHardgoodShippingGroup>();
		List<ShippingGroup> shippingGroups = getShippingGroups();
		for (ShippingGroup shipGrp : shippingGroups) {
			if (shipGrp.getCommerceItemRelationshipCount()>0 && shipGrp instanceof TRUChannelHardgoodShippingGroup) {
				groups.add((TRUChannelHardgoodShippingGroup)shipGrp);
			}
		}
		return groups;
	}

	
	/**
	 * * This method will return non registry Items from the order.
	 *
	 * @return the non registry items
	 */
	@SuppressWarnings("unchecked")
	public List<CommerceItem> getRegistryItems(){
		List<CommerceItem> nonRegistryItems=new ArrayList<CommerceItem>();
		List<ShippingGroup> shippingGroups = getShippingGroups();
		for (ShippingGroup shipGrp : shippingGroups) {
			if (shipGrp instanceof TRUChannelHardgoodShippingGroup) {
				List<ShippingGroupCommerceItemRelationship> commerceItemRelationships = shipGrp.getCommerceItemRelationships();
					if (commerceItemRelationships != null && !commerceItemRelationships.isEmpty()){
						for(ShippingGroupCommerceItemRelationship itemRel:commerceItemRelationships){
							if(itemRel.getCommerceItem() instanceof TRUDonationCommerceItem || itemRel.getCommerceItem() instanceof TRUGiftWrapCommerceItem ){
								continue;
							}else{
								nonRegistryItems.add(itemRel.getCommerceItem());
							}							
						}
					}
			}
		}
		return nonRegistryItems;
	}
	
	
	
	/**
	 * This method will return registry Items from the order.
	 *
	 * @return the registry items
	 */
	@SuppressWarnings("unchecked")
	public List<CommerceItem> getNonRegistryItems(){
		List<CommerceItem> registryItems=new ArrayList<CommerceItem>();
		List<ShippingGroup> shippingGroups = getShippingGroups();
		for (ShippingGroup shipGrp : shippingGroups) {
			if (shipGrp instanceof TRUHardgoodShippingGroup && !(shipGrp instanceof TRUChannelHardgoodShippingGroup)) {
				List<ShippingGroupCommerceItemRelationship> commerceItemRelationships = shipGrp.getCommerceItemRelationships();
					if (commerceItemRelationships != null && !commerceItemRelationships.isEmpty()){
						for(ShippingGroupCommerceItemRelationship itemRel:commerceItemRelationships){
							if(itemRel.getCommerceItem() instanceof TRUDonationCommerceItem || itemRel.getCommerceItem() instanceof TRUGiftWrapCommerceItem ){
								continue;
							}else{
								registryItems.add(itemRel.getCommerceItem());
							}							
						}
					}
			}
		}
		return registryItems;
	}
	
	/**
	 * <p>
	 * This method will return if the order contains of both registry and non registry items.
	 * </p>
	 *
	 * @return true, if is order contains mixed items
	 */
	public boolean isOrderContainsMixedItems(){		
		return (isOrderHasWishListItems() || isOrderHasRegistryItems())&& isOrderHasNonRegistryItems();
	}

	/** The Express pay pal. */
	private boolean mExpressPayPal;
	
	/** The Standard pay pal. */
	private boolean mStanderdPayPal;

	/**
	 * @return the expressPayPal
	 */
	public boolean isExpressPayPal() {
		return mExpressPayPal;
	}

	/**
	 * @param pExpressPayPal the expressPayPal to set
	 */
	public void setExpressPayPal(boolean pExpressPayPal) {
		mExpressPayPal = pExpressPayPal;
	}

	/**
	 * @return the standerdPayPal
	 */
	public boolean isStanderdPayPal() {
		return mStanderdPayPal;
	}

	/**
	 * @param pStanderdPayPal the standerdPayPal to set
	 */
	public void setStanderdPayPal(boolean pStanderdPayPal) {
		mStanderdPayPal = pStanderdPayPal;
	}
	
	/** The Active agreements vo. */
	private Map<String, TRUActiveAgreementsVO> mActiveAgreementsVO;


	/**
	 * @return the activeAgreementsVO
	 */
	public Map<String, TRUActiveAgreementsVO> getActiveAgreementsVO() {
		if(mActiveAgreementsVO == null) {
			mActiveAgreementsVO = new HashMap<String, TRUActiveAgreementsVO>();
		}
		return mActiveAgreementsVO;
	}

	/**
	 * @param pActiveAgreementsVO the activeAgreementsVO to set
	 */
	public void setActiveAgreementsVO(
			Map<String, TRUActiveAgreementsVO> pActiveAgreementsVO) {
		mActiveAgreementsVO = pActiveAgreementsVO;
	}

	/**
	 * @return the deviceID
	 */
	public String getDeviceID() {
		return (String) getPropertyValue(TRUCheckoutConstants.DEVICE_ID);
	}

	/**
	 * @param pDeviceID the deviceID to set
	 */
	public void setDeviceID(String pDeviceID) {
		setPropertyValue(TRUCheckoutConstants.DEVICE_ID, pDeviceID);
	}

	/**
	 * @return the browserID
	 */
	public String getBrowserID() {
		return (String) getPropertyValue(TRUCheckoutConstants.BROWSER_ID);
	}

	/**
	 * @param pBrowserID the browserID to set
	 */
	public void setBrowserID(String pBrowserID) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_ID, pBrowserID);
	}

	/**
	 * @return the browserSessionId
	 */
	public String getBrowserSessionId() {
		return (String) getPropertyValue(TRUCheckoutConstants.BROWSER_SESSION_ID);
	}

	/**
	 * @param pBrowserSessionId the browserSessionId to set
	 */
	public void setBrowserSessionId(String pBrowserSessionId) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_SESSION_ID, pBrowserSessionId);
	}

	/**
	 * @return the browserConnection
	 */
	public String getBrowserConnection() {
		return (String) getPropertyValue(TRUCheckoutConstants.BROWSER_CONNECTION);
	}

	/**
	 * @param pBrowserConnection the browserConnection to set
	 */
	public void setBrowserConnection(String pBrowserConnection) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_CONNECTION, pBrowserConnection);
	}

	/**
	 * @return the browserAccept
	 */
	public String getBrowserAccept() {
		return (String) getPropertyValue(TRUCheckoutConstants.BROWSER_ACCEPT);
	}

	/**
	 * @param pBrowserAccept the browserAccept to set
	 */
	public void setBrowserAccept(String pBrowserAccept) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_ACCEPT, pBrowserAccept);
	}

	/**
	 * @return the browserAcceptEncoding
	 */
	public String getBrowserAcceptEncoding() {
		return (String) getPropertyValue(TRUCheckoutConstants.BROWSER_ACCEPT_ENCODING);
	}

	/**
	 * @param pBrowserAcceptEncoding the browserAcceptEncoding to set
	 */
	public void setBrowserAcceptEncoding(String pBrowserAcceptEncoding) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_ACCEPT_ENCODING, pBrowserAcceptEncoding);
	}

	/**
	 * @return the browserAcceptCharset
	 */
	public String getBrowserAcceptCharset() {
		return (String) getPropertyValue(TRUCheckoutConstants.BROWSER_ACCEPT_CHARSET);
	}

	/**
	 * @param pBrowserAcceptCharset the browserAcceptCharset to set
	 */
	public void setBrowserAcceptCharset(String pBrowserAcceptCharset) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_ACCEPT_CHARSET, pBrowserAcceptCharset);
	}

	/**
	 * @return the browserIdLanguageCode
	 */
	public String getBrowserIdLanguageCode() {
		return (String) getPropertyValue(TRUCheckoutConstants.BROWSER_ID_LANGUAGE_CODE);
	}

	/**
	 * @param pBrowserIdLanguageCode the browserIdLanguageCode to set
	 */
	public void setBrowserIdLanguageCode(String pBrowserIdLanguageCode) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_ID_LANGUAGE_CODE, pBrowserIdLanguageCode);
	}

	/**
	 * @return the browserCookie
	 */
	public String getBrowserCookie() {
		return (String) getPropertyValue(TRUCheckoutConstants.BROWSER_COOKIE);
	}

	/**
	 * @param pBrowserCookie the browserCookie to set
	 */
	public void setBrowserCookie(String pBrowserCookie) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_COOKIE, pBrowserCookie);
	}

	/**
	 * @return the browserReferer
	 */
	public String getBrowserReferer() {
		return (String) getPropertyValue(TRUCheckoutConstants.BROWSER_REFERER);
	}

	/**
	 * @param pBrowserReferer the browserReferer to set
	 */
	public void setBrowserReferer(String pBrowserReferer) {
		setPropertyValue(TRUCheckoutConstants.BROWSER_REFERER, pBrowserReferer);
	}

	/**
	 * @return the customerIPAddress
	 */
	public String getCustomerIPAddress() {
		return (String) getPropertyValue(TRUCheckoutConstants.CUSTOMER_IP_ADDRESS);
	}

	/**
	 * @param pCustomerIPAddress the customerIPAddress to set
	 */
	public void setCustomerIPAddress(String pCustomerIPAddress) {
		setPropertyValue(TRUCheckoutConstants.CUSTOMER_IP_ADDRESS, pCustomerIPAddress);
	}

	/**
	 * @return the timeSpentOnSite
	 */
	public String getTimeSpentOnSite() {
		return (String) getPropertyValue(TRUCheckoutConstants.TIME_SPENT_ONSITE);
	}

	/**
	 * @param pTimeSpentOnSite the timeSpentOnSite to set
	 */
	public void setTimeSpentOnSite(String pTimeSpentOnSite) {
		setPropertyValue(TRUCheckoutConstants.TIME_SPENT_ONSITE, pTimeSpentOnSite);
	}

	/**
	 * @return the hostName
	 */
	public String getHostName() {
		return (String) getPropertyValue(TRUCheckoutConstants.HOST_NAME);
	}

	/**
	 * @param pHostName the hostName to set
	 */
	public void setHostName(String pHostName) {
		setPropertyValue(TRUCheckoutConstants.HOST_NAME, pHostName);
	}

	/**
	 * @return the reservationId
	 */
	public String getReservationId() {
		return (String) getPropertyValue(TRUCheckoutConstants.RESERVATION_ID);
	}

	/**
	 * @param pReservationId the reservationId to set
	 */
	public void setReservationId(String pReservationId) {
		setPropertyValue(TRUCheckoutConstants.RESERVATION_ID, pReservationId);
	}
	
		/** The Existing single use coupons. */
	private Map<String,String> mExistingSingleUseCoupons;

	/**
	 * @return the existingSingleUseCoupons
	 */
	public Map<String, String> getExistingSingleUseCoupons() {
		return mExistingSingleUseCoupons;
	}

	/**
	 * @param pExistingSingleUseCoupons the existingSingleUseCoupons to set
	 */
	public void setExistingSingleUseCoupons(
			Map<String, String> pExistingSingleUseCoupons) {
		mExistingSingleUseCoupons = pExistingSingleUseCoupons;
	}
	/**
	 * map to hold Applied promotion names and associated coupon ids.
	 */
	private Map<String, String> mAppliedPromoNames;
	/**
	 * map to hold Not Applied promotion names and associated coupon ids.
	 */
	private Map<String, String> mNotAppliedPromoNames;
	
	/**
	 * @return the appliedPromoNames
	 */
	public Map<String, String> getAppliedPromoNames() {
		return mAppliedPromoNames;
	}

	/**
	 * @param pAppliedPromoNames the appliedPromoNames to set
	 */
	public void setAppliedPromoNames(Map<String, String> pAppliedPromoNames) {
		mAppliedPromoNames = pAppliedPromoNames;
	}

	/**
	 * @return the mNotAppliedPromoNames
	 */
	public Map<String, String> getNotAppliedPromoNames() {
		return mNotAppliedPromoNames;
	}

	/**
	 * Sets the not applied promo names.
	 *
	 * @param pNotAppliedPromoNames the not applied promo names
	 */
	public void setNotAppliedPromoNames(Map<String, String> pNotAppliedPromoNames) {
		mNotAppliedPromoNames = pNotAppliedPromoNames;
	}
	
	/**
	 * Property to hold BillingAddress.
	 */
	private Address mBillingAddress;
	
	/**
	 * @return Address
	 */
	public Address getBillingAddress() {

		return this.mBillingAddress;
	}

	/**
	 * Billing Address for order.
	 * 
	 * @param pShippingAddress
	 *            - Address
	 * @throws CommerceException
	 *             - CommerceException
	 */
	public void setBillingAddress(Address pShippingAddress) throws CommerceException {
		if ((pShippingAddress instanceof RepositoryContactInfo) || (pShippingAddress instanceof RepositoryAddress)) {
			if (this.mBillingAddress != null) {
				this.mBillingAddress.deleteObservers();
			}
			this.mBillingAddress = pShippingAddress;
			this.mBillingAddress.addObserver(this);
		} else if (pShippingAddress == null) {
			if (this.mBillingAddress != null) {
				this.mBillingAddress.deleteObservers();
			}
			this.mBillingAddress = pShippingAddress;
		} else {
			try {
				if (this.mBillingAddress == null) {
					this.mBillingAddress = (pShippingAddress.getClass().newInstance());
				}
				OrderTools.copyAddress(pShippingAddress, this.mBillingAddress);
			} catch (InstantiationException ie) {
				throw new CommerceException(ie);
			} catch (IllegalAccessException iae) {
				throw new CommerceException(iae);
			}
		}
		setSaveAllProperties(true);
	} 
	
	/**
	 * @return the billingAddressNickName
	 */
	public String getBillingAddressNickName() {
		return (String) getPropertyValue(TRUCheckoutConstants.BILLING_ADDRESS_NICK_NAME);
	}

	/**
	 * @param pBillingAddressNickName the billingAddressNickName to set
	 */
	public void setBillingAddressNickName(String pBillingAddressNickName) {
		setPropertyValue(TRUCheckoutConstants.BILLING_ADDRESS_NICK_NAME, pBillingAddressNickName);
	}

	/**
	 * get the ApplePayPDP property of order.
	 * @return ApplePayPDP - boolean
	 */
	public boolean isApplePayPDP() {
	    return (boolean) getPropertyValue(TRUCheckoutConstants.APPLE_PAY_PDP);
	}
	
	/**
	 * Set the ApplePayPDP property in order.
	 *
	 * @param pIsApplePayPDP the new apple pay PDP
	 */
	public void setApplePayPDP(boolean pIsApplePayPDP) {
	    setPropertyValue(TRUCheckoutConstants.APPLE_PAY_PDP, pIsApplePayPDP);
	}	
	
	
	/**
	 * <p>
	 *	 This method will return true if order has at leaset TRUChannelInStorePickupShippingGroup.
	 *</p>
	 * @return the OrderIsInStorePickUp
	 */
	@SuppressWarnings("unchecked")
	public boolean isOrderHasChannelISPUShippingGroups() {
		List<Relationship> listOfRel = getRelationships();
		ShippingGroup shipGrp = null;
		for (Relationship rel : listOfRel) {
			if (rel instanceof TRUShippingGroupCommerceItemRelationship) {
				shipGrp = ((TRUShippingGroupCommerceItemRelationship) rel).getShippingGroup();
				if (shipGrp instanceof TRUChannelInStorePickupShippingGroup && 
						((TRUChannelInStorePickupShippingGroup)shipGrp).getCommerceItemRelationshipCount()>TRUConstants.ZERO) {
					return Boolean.TRUE;
				}
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 *
	 * @return true, if is order has at least one channelhardgoodshippinggroup.
	 */
	@SuppressWarnings("unchecked")
	public boolean isOrderHasChannelHGShippingGroups() {
		List<Relationship> listOfRel = getRelationships();
		ShippingGroup shipGrp = null;
		for (Relationship rel : listOfRel) {
			if (rel instanceof TRUShippingGroupCommerceItemRelationship) {
				shipGrp = ((TRUShippingGroupCommerceItemRelationship) rel).getShippingGroup();
				if (shipGrp instanceof TRUChannelHardgoodShippingGroup && 
						((TRUChannelHardgoodShippingGroup)shipGrp).getCommerceItemRelationshipCount()>TRUConstants.ZERO) {
					return Boolean.TRUE;
				}
			}
		}
		return Boolean.FALSE;
	}
	
	/** The Previous response code. */
	private String mPreviousResponseCode;

	/**
	 * @return the previousResponseCode
	 */
	public String getPreviousResponseCode() {
		return mPreviousResponseCode;
	}

	/**
	 * @param pPreviousResponseCode the previousResponseCode to set
	 */
	public void setPreviousResponseCode(String pPreviousResponseCode) {
		mPreviousResponseCode = pPreviousResponseCode;
	}	 	
	
	
	
}
