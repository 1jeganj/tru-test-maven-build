package com.tru.commerce.order.scheduler;

import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.scheduler.SchedulableService;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;

import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;

/**
 * @author PA
 * 
 * The Class TRUUpdateRegistryItemsScheduler.
 *
 */
public class TRUUpdateRegistryItemsScheduler extends SchedulableService {

	/** The Order manager. */
	private TRUOrderManager mOrderManager;

	/**
	 * @return the orderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * @param pOrderManager the orderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	/**
	 * 
	 * This method always execute the specified time.
	 *
	 * Perform scheduled task1.
	 *
	 * @param pArg0 the arg0
	 * @param pArg1 the arg1
	 */
	@Override
	public void performScheduledTask(Scheduler pArg0, ScheduledJob pArg1) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUUpdateRegistryItemsScheduler::@method::performScheduledTask() :START");
		}
		TRUOrderTools orderTools = (TRUOrderTools) getOrderManager().getOrderTools();
		RepositoryItem[] failedUpdateRegistryItems = null;
		try {
			failedUpdateRegistryItems = orderTools.getFailedUpdateRegistryItems();
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError("RepositoryException occured in TRUUpdateRegistryItemsScheduler.performScheduledTask()",e);
			}
		}
		if (failedUpdateRegistryItems == null) {
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUUpdateRegistryItemsScheduler::@method::performScheduledTask() failedReverseAuths is null: END");
			}
			return;
		}
		for (int i = 0; i < failedUpdateRegistryItems.length; i++) {
			RepositoryItem item = failedUpdateRegistryItems[i];
			getOrderManager().getRegistryServiceProcessor().updateRegistryItemByFailedItem(item);
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUUpdateRegistryItemsScheduler::@method::performScheduledTask() : END");
		}
	}
}
