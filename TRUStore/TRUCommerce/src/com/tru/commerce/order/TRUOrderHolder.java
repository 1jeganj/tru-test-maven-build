package com.tru.commerce.order;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.transaction.TransactionManager;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.ShippingGroup;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.order.purchase.TRUAddCommerceItemInfo;
import com.tru.commerce.order.purchase.TRUPurchaseProcessHelper;
import com.tru.common.TRUConstants;
import com.tru.userprofiling.TRUCookieManager;

/**
 * The Class TRUOrderHolder.
 *  @author PA
 *  @version 1.0
 */
public class TRUOrderHolder extends OrderHolder {

	/** The Constant DEFAULT_STRING. */
	public static final String DEFAULT_STRING = "default";

	/** The Constant DEFAULT_STRING. */
	public static final String PIPE_STRING = "\\|";

	/** The Purchase process helper. */
	private TRUPurchaseProcessHelper mPurchaseProcessHelper;

	/** The Free shipping promotion. */
	private boolean mFreeShippingPromotion;

	/** The mShippingPromotionOrderLimit. */
	private double mShippingPromotionOrderLimit;

	/** The mAmountToQualifyPromotion. */
	private double mAmountToQualifyPromotion;
	
	/** The mLayawayOrder. */
	private TRULayawayOrderImpl mLayawayOrder;
	
	/** The mLayawayOrder. */
	private TRULayawayOrderImpl mLastLayawayOrder;
	
	/** The  shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;
	
	/** The commerce item type for cookie map. */
	private Map<String,String> mCommerceItemTypeForCookieMap;
	
	/** The site id for cookie map. */
	private Map<String,String> mSiteIdForCookieMap;
	
	/** The  excluded shipping promotion skus amount. */
	private double mExcludedShippingPromotionSkusAmount;
	/**Holds order from pdp for apple pay **/
	private TRUOrderImpl mBuyNowWithAP;
	/**
	 * Gets the site id for cookie map.
	 *
	 * @return the site id for cookie map
	 */
	public Map<String, String> getSiteIdForCookieMap() {
		return mSiteIdForCookieMap;
	}

	/**
	 * Sets the site id for cookie map.
	 *
	 * @param pSiteIdForCookieMap the site id for cookie map
	 */
	public void setSiteIdForCookieMap(Map<String, String> pSiteIdForCookieMap) {
		mSiteIdForCookieMap = pSiteIdForCookieMap;
	}
	
	/**
	 * Gets the commerce item type for cookie map.
	 *
	 * @return the commerce item type for cookie map
	 */
	public Map<String, String> getCommerceItemTypeForCookieMap() {
		return mCommerceItemTypeForCookieMap;
	}

	/**
	 * Sets the commerce item type for cookie map.
	 *
	 * @param pCommerceItemTypeForCookieMap the commerce item type for cookie map
	 */
	public void setCommerceItemTypeForCookieMap(Map<String, String> pCommerceItemTypeForCookieMap) {
		mCommerceItemTypeForCookieMap = pCommerceItemTypeForCookieMap;
	}

	/**
	 * Gets the shopping cart utils.
	 *
	 * @return the shopping cart utils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * Sets the shopping cart utils.
	 *
	 * @param pShoppingCartUtils the new shopping cart utils
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		this.mShoppingCartUtils = pShoppingCartUtils;
	}

	/**
	 * This method is used to create the order from cookie items for Anonymous users.
	 * 
	 * @param pProfile
	 *            the profile
	 * @return the order
	 * @throws CommerceException
	 *             the commerce exception
	 */
	@Override
	protected Order createInitialOrder(RepositoryItem pProfile) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderHolder::@method::createInitialOrder() : BEGIN");
			vlogDebug("@Class::TRUOrderHolder::@method::createInitialOrder() : pProfile -->{0}",pProfile);
		}
		Order order = null;
		if (pProfile.isTransient()) {
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUOrderHolder::@method::createInitialOrder() : pProfile.isTransient() {0}", pProfile.isTransient());
			}
			boolean rollbackFlag = Boolean.FALSE;
			order = super.createInitialOrder(pProfile);
			//TO Avoid StackOverflowError
			setCurrent(order);
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = null;

			td = new TransactionDemarcation();
			try {
				td.begin(tm);
				synchronized (order) {
					addCookieItemsToOrder(order, pProfile);
					getOrderManager().updateOrder(order);
				}
			} catch (TransactionDemarcationException tde) {
				rollbackFlag = Boolean.TRUE;
				if (isLoggingError()) {
					vlogError("TransactionDemarcationException in @Class::TRUOrderHolder::@method::createInitialOrder()  {0}", tde);
				}
			} finally {
				try {
					td.end(rollbackFlag);
				} catch (TransactionDemarcationException tde) {
					if (isLoggingError()) {
						vlogError("TransactionDemarcationException in @Class::TRUOrderHolder::@method::createInitialOrder()  {0}", tde);
					}
				}
			}
		} else {
			order = super.createInitialOrder(pProfile);
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderHolder::@method::createInitialOrder() : END");
		}
		return order;
	}

	/**
	 * Adds the cookie items to order.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pProfile
	 *            the profile
	 * @return the order
	 * @throws CommerceException
	 *             the commerce exception
	 */
	protected Order addCookieItemsToOrder(Order pOrder, RepositoryItem pProfile) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderHolder::@method::addCookieItemsToOrder() : BEGIN");
			vlogDebug("@Class::TRUOrderHolder::@method::addCookieItemsToOrder() : pOrder --> {0},  pProfile  --> {1}",pOrder,pProfile);
		}
		Object enableCartCookie = null;
		if (SiteContextManager.getCurrentSite() != null) {
			enableCartCookie = SiteContextManager.getCurrentSite().getPropertyValue(
					getPurchaseProcessHelper().getPropertyManager().getEnablePersistOrderPropertyName());
		}
		if (enableCartCookie != null && (boolean) enableCartCookie) {
			String cartCookieValue = getCartCookieValue();
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUOrderHolder::@method::addCookieItemsToOrder() : cart Cookie is -->{0}", cartCookieValue);
			}
			TRUAddCommerceItemInfo[] commerceItemInfo = generateItemInfos(cartCookieValue, pOrder);
			ShippingGroup matchingSG = null;
			if (null != pOrder && null != pProfile && null != commerceItemInfo && commerceItemInfo.length > 0) {
				for (TRUAddCommerceItemInfo truAddCommerceItemInfo : commerceItemInfo) {
					if (StringUtils.isNotBlank(truAddCommerceItemInfo.getLocationId())) {
						matchingSG = getPurchaseProcessHelper().getMatchedISPUShippingGroup(pOrder,
								truAddCommerceItemInfo);
						if (null == matchingSG) {
							getPurchaseProcessHelper().createMissedIspuSGsInOrder(pOrder, truAddCommerceItemInfo);
						}
					}
				}
				getPurchaseProcessHelper().addItemsToOrder(pOrder, (ShippingGroup) pOrder.getShippingGroups().get(0),
						pProfile, commerceItemInfo, null, null, null, null);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderHolder::@method::addCookieItemsToOrder() : END");
		}
		return pOrder;
	}

	/**
	 * Gets the cart cookie value.
	 *
	 * @return the cart cookie value
	 */
	private String getCartCookieValue() {
		StringBuffer cookieString = new StringBuffer();
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderHolder::@method::getCartCookieValue() : BEGIN");
		}
		int cookieCount = TRUCommerceConstants.INT_ONE;
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		Cookie cartCookie = getCookieManager().getCookieFromRequest(
				((TRUCookieManager) getCookieManager()).getOrderCookieName(), request);
		while (cartCookie != null) {
			cookieString.append(cartCookie.getValue());
			cartCookie = getCookieManager().getCookieFromRequest(
					((TRUCookieManager) getCookieManager()).getOrderCookieName() + cookieCount, request);
			cookieCount++;
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderHolder::@method::getCartCookieValue() : END");
		}
		return cookieString.toString();
	}
	
	/**
	 * This method is used to generate the item infos from cookie items.
	 * 
	 * @param pCookieItems
	 *            the cookie items
	 * @param pOrder
	 *            the order
	 * @return the TRU add commerce item info[]
	 */
	public TRUAddCommerceItemInfo[] generateItemInfos(String pCookieItems, Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderHolder::@method::generateItemInfos() : BEGIN");
			vlogDebug("@Class::TRUOrderHolder::@method::generateItemInfos() :pCookieItems  -->{0} pOrder -->{1}",pCookieItems,pOrder);
		}
		List<TRUAddCommerceItemInfo> itemInfoList = new ArrayList<TRUAddCommerceItemInfo>();
		if (!StringUtils.isEmpty(pCookieItems)) {
			String[] productInfos = pCookieItems.split(TRUConstants.TILDA_STRING);
			if (productInfos != null && productInfos.length > 0) {
				for (String productInfoItem : productInfos) {
					if (!StringUtils.isEmpty(productInfoItem)) {
						String[] productInfo = productInfoItem.split(PIPE_STRING);

						if (productInfo != null && productInfo.length >= TRUConstants.TWELVE) {
							itemInfoList.add(createItemInfo(productInfo));
						}

					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderHolder::@method::generateItemInfos() : END");
		}
		return (TRUAddCommerceItemInfo[]) itemInfoList.toArray(new TRUAddCommerceItemInfo[itemInfoList.size()]);
	}

	/**
	 * Creates the item info.
	 * 
	 * @param pProductInfo
	 *            the product info
	 * @return the tRU add commerce item info
	 */
	private TRUAddCommerceItemInfo createItemInfo(String[] pProductInfo) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderHolder::@method::createItemInfo() : BEGIN");
			
		}
		TRUAddCommerceItemInfo itemInfo = new TRUAddCommerceItemInfo();
		itemInfo.setCatalogRefId(pProductInfo[TRUConstants.ZERO]);
		itemInfo.setProductId(pProductInfo[TRUConstants.SIZE_ONE]);

		if (!StringUtils.isEmpty(pProductInfo[TRUConstants.TWO])) {
			itemInfo.setLocationId(pProductInfo[TRUConstants.TWO]);
		}
		if (!StringUtils.isEmpty(pProductInfo[TRUConstants.THREE])) {
			itemInfo.setChannelId(pProductInfo[TRUConstants.THREE]);
		}
		if (!StringUtils.isEmpty(pProductInfo[TRUConstants.FOUR])) {
			itemInfo.setChannelType(pProductInfo[TRUConstants.FOUR]);
		}
		if (!StringUtils.isEmpty(pProductInfo[TRUConstants.FIVE])) {
			itemInfo.setChannelUserName(pProductInfo[TRUConstants.FIVE]);
		}
		if (!StringUtils.isEmpty(pProductInfo[TRUConstants.SIX])) {
			itemInfo.setChannelCoUserName(pProductInfo[TRUConstants.SIX]);
		}
		if (!StringUtils.isEmpty(pProductInfo[TRUConstants.SEVEN])) {
			itemInfo.setChannelName(pProductInfo[TRUConstants.SEVEN]);
		}
		if (!StringUtils.isEmpty(pProductInfo[TRUConstants.EIGHT])) {
			itemInfo.setSourceChannel(pProductInfo[TRUConstants.EIGHT]);
		}
		if (!StringUtils.isBlank(pProductInfo[TRUConstants.NINE])) {
			itemInfo.setQuantity(Long.parseLong(pProductInfo[TRUConstants.NINE]));
		}
		if (!StringUtils.isBlank(pProductInfo[TRUConstants.TEN])) {
			itemInfo.setDonationAmount(Double.parseDouble(pProductInfo[TRUConstants.TEN]));
		}
		if (!StringUtils.isBlank(pProductInfo[TRUConstants.ELEVEN])) {
			itemInfo.setSiteId(getSiteIdForCookieMap().get(pProductInfo[TRUConstants.ELEVEN]));
		}
		if (!StringUtils.isBlank(pProductInfo[TRUConstants.TWELVE])) {
			itemInfo.setCommerceItemType(getCommerceItemTypeForCookieMap().get(pProductInfo[TRUConstants.TWELVE]));
		}
		if(itemInfo.getLocationId()!=null){
		Double wareHouseLocationId;
		try {
			wareHouseLocationId = getShoppingCartUtils().getTRUCartModifierHelper().getWareHouseLocationId(itemInfo.getLocationId());
		if (wareHouseLocationId != null) {
			long longValue = wareHouseLocationId.longValue();
			((TRUAddCommerceItemInfo) itemInfo).setWarehouseLocationCode(String.valueOf(longValue));
		}
		} catch (RepositoryException repositoryExc) {
			if (isLoggingError()) {
				vlogError("Repository Exception in @Class::TRUOrderHolder::@method::createInitialOrder()", repositoryExc);
				}
		}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderHolder::@method::generateItemInfos() itemInfo is: {0} " , itemInfo);
			vlogDebug("@Class::TRUOrderHolder::@method::createItemInfo() : END");
		}
		return itemInfo;
	}

	/**
	 * This method is used to get the persistOrders property from site repository.
	 * 
	 * @return true, if is persist orders
	 */
	public boolean isPersistOrders() {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderHolder::@method::isPersistOrders() : BEGIN");
		}
		boolean enablepersistOrder = Boolean.TRUE;
		Object persistOrder = null;
		if (SiteContextManager.getCurrentSite() != null) {
			persistOrder = SiteContextManager.getCurrentSite().getPropertyValue(
					getCookieManager().getPropertyManager().getEnablePersistOrderPropertyName());
		}
		if (null != persistOrder) {
			enablepersistOrder = (Boolean) persistOrder;
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUOrderHolder::@method::isPersistOrders() : enablepersistOrder", enablepersistOrder);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderHolder::@method::isPersistOrders() : END");
		}
		return enablepersistOrder;
	}

	/**
	 * Gets the cookie manager.
	 * 
	 * @return the cookie manager
	 */
	public TRUCookieManager getCookieManager() {

		if (getProfileTools() == null || getProfileTools().getCookieManager() == null) {
			return null;
		}
		return (TRUCookieManager) getProfileTools().getCookieManager();
	}

	/**
	 * Gets the purchase process helper.
	 * 
	 * @return the purchase process helper
	 */
	public TRUPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}

	/**
	 * Sets the purchase process helper.
	 * 
	 * @param pPurchaseProcessHelper
	 *            the new purchase process helper
	 */
	public void setPurchaseProcessHelper(TRUPurchaseProcessHelper pPurchaseProcessHelper) {
		mPurchaseProcessHelper = pPurchaseProcessHelper;
	}

	/**
	 * Gets the free shipping progress bar promotion.
	 *
	 * @return freeShippingPromo - Shipping Promotion
	 */
	public RepositoryItem getFreeShippingProgressBarPromotion() {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderHolder::@method::getFreeShippingProgressBarPromotion() : BEGIN");
		}
		Date now = getPurchaseProcessHelper().getPromotionTools().getCurrentDate().getTimeAsDate();
		RepositoryItem promotionItem = null;
		Site site = SiteContextManager.getCurrentSite();
		if (site != null) {
			promotionItem = (RepositoryItem) site.getPropertyValue(getCookieManager().getPropertyManager()
					.getFreeShippingPromotionPropertyName());
		}
		// checking promotion is expired or not
		if (promotionItem != null && !getPurchaseProcessHelper().getPromotionTools().checkPromotionExpiration(promotionItem, now)) {
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUOrderHolder::@method::getFreeShippingProgressBarPromotion() : END");
			}
			return (RepositoryItem) SiteContextManager.getCurrentSite().getPropertyValue(
					getCookieManager().getPropertyManager().getFreeShippingPromotionPropertyName());
		} else {
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUOrderHolder::@method::getFreeShippingProgressBarPromotion() : END");
			}
			return null;
		}
	}
	
	
	/**
	 * Checks if is free shipping promotion.
	 *
	 * @return the freeShippingPromotion
	 */
	public boolean isFreeShippingPromotion() {
		if (getFreeShippingProgressBarPromotion() != null) {
			mFreeShippingPromotion = Boolean.TRUE;
		} else {
			mFreeShippingPromotion = Boolean.FALSE;
		}
		return mFreeShippingPromotion;
	}

	/**
	 * Sets the free shipping promotion.
	 *
	 * @param pFreeShippingPromotion            the freeShippingPromotion to set
	 */
	public void setFreeShippingPromotion(boolean pFreeShippingPromotion) {
		mFreeShippingPromotion = pFreeShippingPromotion;
	}

	/**
	 * Gets the shipping promotion order limit.
	 *
	 * @return the shippingPromotionOrderLimit
	 */
	public double getShippingPromotionOrderLimit() {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderHolder::@method::getShippingPromotionOrderLimit() : BEGIN");
		}
		if (getFreeShippingProgressBarPromotion() != null &&
				getFreeShippingProgressBarPromotion().getPropertyValue(
						getCookieManager().getPropertyManager().getOrderLimitPropertyName()) != null) {
			mShippingPromotionOrderLimit = (double) getFreeShippingProgressBarPromotion().getPropertyValue(
					getCookieManager().getPropertyManager().getOrderLimitPropertyName());
		}else{
			mShippingPromotionOrderLimit = TRUConstants.DOUBLE_ZERO;
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderHolder::@method::getShippingPromotionOrderLimit() : END");
		}
		return mShippingPromotionOrderLimit;
	}

	/**
	 * Sets the shipping promotion order limit.
	 *
	 * @param pShippingPromotionOrderLimit            the shippingPromotionOrderLimit to set
	 */
	public void setShippingPromotionOrderLimit(double pShippingPromotionOrderLimit) {
		mShippingPromotionOrderLimit = pShippingPromotionOrderLimit;
	}

	/**
	 * Gets the amount to qualify promotion.
	 *
	 * @return the amountToQualifyPromotion
	 */
	public double getAmountToQualifyPromotion() {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderHolder::@method::getAmountToQualifyPromotion() : BEGIN");
		}
		RepositoryItem promotionItem=getFreeShippingProgressBarPromotion();
		double promoLimit=getShippingPromotionOrderLimit();
		if (promotionItem != null && getCurrent() != null && getCurrent().getPriceInfo() != null) {
			List<RepositoryItem> qualifiedSkus=new ArrayList<RepositoryItem>();
			List<RepositoryItem> excludedSKUforPromo=new ArrayList<RepositoryItem>();
			getPurchaseProcessHelper().getPromotionTools().getQualifiedSKUforPromoFromCatalog(getCurrent(), promotionItem, qualifiedSkus, excludedSKUforPromo);
			List<String> qualifyBulkSkuids=getPurchaseProcessHelper().getPromotionTools().getQualifyBulkSkuIds(promotionItem);
			List<String> excludedBulkSkuids=getPurchaseProcessHelper().getPromotionTools().getExcludedBulkSkuIds(promotionItem);
			mExcludedShippingPromotionSkusAmount=getShoppingCartUtils().calculateExcludedSkusAmount(excludedSKUforPromo,this.getCurrent(),
					qualifiedSkus,qualifyBulkSkuids,excludedBulkSkuids);
			double orderAmount = getCurrent().getPriceInfo().getAmount()-mExcludedShippingPromotionSkusAmount;
			if(orderAmount<=0){
				mAmountToQualifyPromotion = promoLimit;
			}else{
				mAmountToQualifyPromotion = promoLimit - orderAmount;
			}
		}else{
			mExcludedShippingPromotionSkusAmount=TRUConstants.DOUBLE_ZERO;
			mAmountToQualifyPromotion = promoLimit;
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderHolder::@method::getAmountToQualifyPromotion() : END");
		}
		return mAmountToQualifyPromotion;
	}

	/**
	 * Sets the amount to qualify promotion.
	 *
	 * @param pAmountToQualifyPromotion            the amountToQualifyPromotion to set
	 */
	public void setAmountToQualifyPromotion(double pAmountToQualifyPromotion) {
		mAmountToQualifyPromotion = pAmountToQualifyPromotion;
	}

	/**
	 * Gets the layaway order.
	 *
	 * @return the mLayawayOrder
	 */
	public TRULayawayOrderImpl getLayawayOrder() {
		return mLayawayOrder;
	}

	/**
	 * Sets the layaway order.
	 *
	 * @param pLayawayOrder the mLayawayOrder to set
	 */
	public void setLayawayOrder(TRULayawayOrderImpl pLayawayOrder) {
		this.mLayawayOrder = pLayawayOrder;
	}

	/**
	 * Gets the last layaway order.
	 *
	 * @return the mLastLayawayOrder
	 */
	public TRULayawayOrderImpl getLastLayawayOrder() {
		return mLastLayawayOrder;
	}

	/**
	 * Sets the last layaway order.
	 *
	 * @param pLastLayawayOrder the mLastLayawayOrder to set
	 */
	public void setLastLayawayOrder(TRULayawayOrderImpl pLastLayawayOrder) {
		this.mLastLayawayOrder = pLastLayawayOrder;
	}

	/**
	 * Gets the excluded shipping promotion skus amount.
	 *
	 * @return the excluded shipping promotion skus amount
	 */
	public double getExcludedShippingPromotionSkusAmount() {
		return mExcludedShippingPromotionSkusAmount;
	}

	/**
	 * Sets the excluded shipping promotion skus amount.
	 *
	 * @param pExcludedShippingPromotionSkusAmount the new excluded shipping promotion skus amount
	 */
	public void setExcludedShippingPromotionSkusAmount(double pExcludedShippingPromotionSkusAmount) {
		this.mExcludedShippingPromotionSkusAmount = pExcludedShippingPromotionSkusAmount;
	}

	/**
	 * Gets order for apple pay from pdp.
	 * @return the order which is created from pdp.
	 */
	public TRUOrderImpl getBuyNowWithAP() {
		return mBuyNowWithAP;
	}

	/**
	 *  * Sets the order for apple pay from pdp.
	 *
	 * @param pBuyNowWithAP the new buy now with AP
	 */
	public void setBuyNowWithAP(TRUOrderImpl pBuyNowWithAP) {
		this.mBuyNowWithAP = pBuyNowWithAP;
	}
}
