/**
 * 
 */
package com.tru.commerce.order;

/**
 * @author ORACLE
 * @version 1.0
 */
public class TRUPipeLineConstants {
	
	/**
	 * Constant  to hold numeric zero.
	 */
	public static final int NUMERIC_ZERO = 0;
	/**
	 * Constant  to hold numeric one.
	 */
	public static final int NUMERIC_ONE = 1;
	
	/**
	 * Constant  to hold String PROCESS_PG_FAILED.
	 */
	public static final Object PROCESS_PG_FAILED = "ProcProcessPaymentGroupFailed";
	
	/**
	 * Constant  to hold String REVERSE_ACTION.
	 */
	public static final String REVERSE_ACTION = "reverseAction";
	
	/**
	 * Constant  to hold  showSetPGAmountErr.
	 */
	public static final String SHOW_SET_PGAMOUNT_ERR = "showSetPGAmountErr"; 
	
	/**
	 * Constant to hold status.
	 */
	public static final String STATUS = "status";
	
	/**
	 * Constant for 0.00001.
	 */
	public static final double MARGIN_OF_ERROR = 0.00001;
	
	/**
	 * Constant for PROC_SET_PAYMENT_GROUP_AMOUNT.
	 */
	public static final String PROC_SET_PAYMENT_GROUP_AMOUNT = "ProcSetPaymentGroupAmount";
	
	/**
	 * Constant  to hold String USER_MESSAGES_BUNDLE.
	 */
	public static final String USER_MESSAGES_BUNDLE = "atg.commerce.order.UserMessages";
	
	/**
	 * Constant  to hold String INVALID PG.
	 */
	public static final String INVALID_PG_PARAM = "InvalidPaymentGroupParameter";
	
	/**
	 * Constant  to hold String INVALID_ORDER_PARAMETER.
	 */
	public static final String INVALID_ORDER_PARAMETER = "InvalidOrderParameter";
	
	/**
	 * Constant  to hold String INVALID_ORDER_MANAGER_PARAM.
	 */
	public static final String INVALID_ORDER_MANAGER_PARAM = "InvalidOrderManagerParameter";
	
	/**
	 * Constant  to hold String INVALID_REPOSITORY_PARAM.
	 */
	public static final String INVALID_REPOSITORY_PARAM = "InvalidRepositoryParameter";
	
	/**
	 * Constant  to hold String INVALID_PAYMENT_STATUS_CLASS_TYPE.
	 */
	public static final String INVALID_PAYMENT_STATUS_CLASS_TYPE = "InvalidPaymentStatusClassType";
	
	/**
	 * Constant  to hold String CREDITCARD_INFO_CLASS.
	 */
	public static final String CREDITCARD_INFO_CLASS = "com.tru.commerce.order.payment.creditcard.TRUCreditCardInfo";
	
	/**
	 * Constant  to hold String GIFTCARD_INFO_CLASS.
	 */
	public static final String GIFTCARD_INFO_CLASS = "com.tru.commerce.order.payment.giftcard.TRUGiftCardInfo";

	/**
	 * Constant  to hold String APPLE_PAY_INFO_CLASS.
	 */
	public static final String APPLE_PAY_INFO_CLASS = "com.tru.commerce.order.payment.applepay.TRUApplePayInfo";
	
	/**
	 * Constant  to hold String ERROR_GIFT_CARD_VALIDATION.
	 */
	public static final String ERROR_GIFT_CARD_VALIDATION = "tru_error_checkout_giftcard_validation";
	
	/** Constant for processOrder. */
	public static final String PROCESS_ORDER = "processOrder";
	
	/** Constant for chainId. */
	public static final String CHAIN_ID = "chainId";
		
	/** Constant for moveToPurchaseInfo. */
	public static final String MOVE_TO_PURCHASE_INFO = "moveToPurchaseInfo";
	
	/** Constant for truValidateForCheckoutChain. */
	public static final String TRU_VALIDATE_CHECKOUT_CHAIN = "truValidateForCheckoutChain";
	/** The Constant COOKIE_LOCATION_ID. */
	public static final String COOKIE_LOCATION_ID = "cookieLocationId";
	
	/** The Constant TEST. */
	public static final String TEST = "test";
	
	/**
	 * Constant  to hold numeric minus one.
	 */
	public static final int NUMERIC_MINUS_ONE = -1;
	
	/** The Constant SESSION_COMPONENT. */
	public static final String SESSION_COMPONENT = "sessionComponent";
}
