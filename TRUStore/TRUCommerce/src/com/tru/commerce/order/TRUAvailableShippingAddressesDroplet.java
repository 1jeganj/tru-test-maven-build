/**
 * 
 */
package com.tru.commerce.order;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.CommerceException;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.core.util.Address;
import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.purchase.TRUShippingGroupContainerService;
import com.tru.commerce.order.purchase.TRUShippingProcessHelper;
import com.tru.common.TRUConstants;

/**
 * This droplet fetches available shipping addresses.
 * @author PA.
 * @version 1.0.
 */
public class TRUAvailableShippingAddressesDroplet extends DynamoServlet {
	
	/**
	 * property: Reference to the ShippingProcessHelper component.
	 */
	private TRUShippingProcessHelper mShippingHelper;
	
	/**
	 * @return the shippingHelper
	 */
	public TRUShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * @param pShippingHelper
	 *            the shippingHelper to set
	 */
	public void setShippingHelper(TRUShippingProcessHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}
	/**
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		
		TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) 
				pRequest.getObjectParameter(TRUCheckoutConstants.SHIPPING_GROUP_MAP_CONTAINER);
		Order order = (Order) pRequest.getObjectParameter(TRUCheckoutConstants.ORDER);
		boolean restService = Boolean.parseBoolean((String)pRequest.getLocalParameter(TRUCheckoutConstants.REST_SERVICE)) ;
		// Getting default shipping address nick name and default shipping group
		String selectedAddressNickName = null;
		Address selectedShippingAddress = null;
		
		Map<String, ShippingGroup> validShippingGroupsMapForDisplay = new LinkedHashMap<String, ShippingGroup>();
		Map<String, Address> validShippingAddressMap = new LinkedHashMap<String, Address>();
		
		if (shippingGroupMapContainer == null || order == null) {
			pRequest.setParameter(TRUCheckoutConstants.VALID_SHIPPING_GROUP_MAP, validShippingGroupsMapForDisplay);
			pRequest.setParameter(TRUCheckoutConstants.SELECTED_ADDRESS_NICK_NAME, selectedAddressNickName);
			pRequest.setParameter(TRUCheckoutConstants.SELECTED_SHIPPING_ADDRESS, selectedShippingAddress);
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
			return;
		}
		
		try {
			// fetching default address nickName
			String defaultAddressNickName = getShippingHelper().findDefaultShippingAddress(shippingGroupMapContainer);
			
			TRUHardgoodShippingGroup orderHardgoodShippingGroup = getShippingHelper().getPurchaseProcessHelper().getFirstHardgoodShippingGroup(order);
			
			// fetching valid shipping groups
			Map<String, ShippingGroup> shippingGroupMapForDisplay = shippingGroupMapContainer.getShippingGroupMapForDisplay();
			// code review comments fix to reduce if condition complexity
			if (shippingGroupMapForDisplay == null || shippingGroupMapForDisplay.isEmpty()) {
				pRequest.setParameter(TRUCheckoutConstants.VALID_SHIPPING_GROUP_MAP, validShippingGroupsMapForDisplay);
				pRequest.setParameter(TRUCheckoutConstants.VALID_SHIPPING_ADDRESS_MAP, validShippingAddressMap);
				pRequest.setParameter(TRUCheckoutConstants.SELECTED_ADDRESS_NICK_NAME, selectedAddressNickName);
				pRequest.setParameter(TRUCheckoutConstants.SELECTED_SHIPPING_ADDRESS, selectedShippingAddress);
				pRequest.setParameter(TRUCheckoutConstants.DEFAULT_SHIPPING_ADDRESS, shippingGroupMapContainer.getDefaultShippingGroupName());
				pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
				return;				
			}	
			// get address nick name from order first hardgood shipping group
			//Fix for defect TUW-54080 : checking the nickname in shippingGroupMapForDisplay.
			if (orderHardgoodShippingGroup != null && shippingGroupMapForDisplay.get(orderHardgoodShippingGroup.getNickName()) != null) {
				selectedAddressNickName = orderHardgoodShippingGroup.getNickName();
			}
			
			// fetching default address nickName as selectedAddressNickName
			if (StringUtils.isBlank(selectedAddressNickName) && StringUtils.isNotBlank(defaultAddressNickName)) {
				selectedAddressNickName = defaultAddressNickName;
			}
			
			if (StringUtils.isNotBlank(selectedAddressNickName) && shippingGroupMapForDisplay.get(selectedAddressNickName) != null) {
				ShippingGroup shippingGroup = (ShippingGroup) shippingGroupMapForDisplay.get(selectedAddressNickName);
				if (shippingGroup instanceof HardgoodShippingGroup) {
					HardgoodShippingGroup hardgoodShippingGroup = (HardgoodShippingGroup) shippingGroup;
					selectedShippingAddress = hardgoodShippingGroup.getShippingAddress();
				}
			}
			Set<String> shippingGroupKeys = shippingGroupMapForDisplay.keySet();
			for (String shippingGroupKey : shippingGroupKeys) {
				if (StringUtils.isNotBlank(shippingGroupKey)&&
								(ShippingGroup) shippingGroupMapForDisplay
								.get(shippingGroupKey) instanceof HardgoodShippingGroup) {
					
					HardgoodShippingGroup hardgoodShippingGroup = (HardgoodShippingGroup) (ShippingGroup) shippingGroupMapForDisplay
							.get(shippingGroupKey);
					if (TRUConstants.UNITED_STATES.equalsIgnoreCase(hardgoodShippingGroup.getShippingAddress().getCountry())) {
						if (restService) {
							TRUHardgoodShippingGroup hardShipppingGroup = (TRUHardgoodShippingGroup) hardgoodShippingGroup;
							if (!hardShipppingGroup.isBillingAddress()) {
								validShippingAddressMap.put(shippingGroupKey, hardgoodShippingGroup.getShippingAddress());	
							}
						} else {
							validShippingAddressMap.put(shippingGroupKey, hardgoodShippingGroup.getShippingAddress());	
						}
						validShippingGroupsMapForDisplay.put(shippingGroupKey, hardgoodShippingGroup);
					}
				}
			}
		} catch (CommerceException comExc) {
			if (isLoggingError()) {
				logError("Commerce Exception in @Class:::TRUAvailableShippingAddressesDroplet:::@method:::service()", comExc);
			}
		}		
		pRequest.setParameter(TRUCheckoutConstants.VALID_SHIPPING_GROUP_MAP, validShippingGroupsMapForDisplay);
		pRequest.setParameter(TRUCheckoutConstants.VALID_SHIPPING_ADDRESS_MAP, validShippingAddressMap);
		pRequest.setParameter(TRUCheckoutConstants.SELECTED_ADDRESS_NICK_NAME, selectedAddressNickName);
		pRequest.setParameter(TRUCheckoutConstants.SELECTED_SHIPPING_ADDRESS, selectedShippingAddress);
		pRequest.setParameter(TRUCheckoutConstants.DEFAULT_SHIPPING_ADDRESS, shippingGroupMapContainer.getDefaultShippingGroupName());
		pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
	}
}
