package com.tru.commerce.order;

import com.tru.common.TRUConstants;


/**
 * The class TRUHardgoodShippingGroup.
 * @author PA
 * @version 1.0
 */
public class TRUChannelHardgoodShippingGroup extends TRUHardgoodShippingGroup {
	
	private static final long serialVersionUID = -347867323339384378L;
	
	/**
	 * Gets the channel.
	 * 
	 * @return the channel
	 */
	public String getChannelType() {
		return (String) getPropertyValue(TRUConstants.CHANNEL_TYPE);
	}

	/**
	 * Sets the channel.
	 * 
	 * @param pChannel
	 *            the new channel
	 */
	public void setChannelType(String pChannel) {
		setPropertyValue(TRUConstants.CHANNEL_TYPE, pChannel);
	}

	/**
	 * Gets the channel id.
	 * 
	 * @return the channel id
	 */
	public String getChannelId() {
		return (String) getPropertyValue(TRUConstants.CHANNEL_ID);
	}

	/**
	 * Sets the channel id.
	 * 
	 * @param pChannelId
	 *            the new channel id
	 */
	public void setChannelId(String pChannelId) {
		setPropertyValue(TRUConstants.CHANNEL_ID, pChannelId);
	}

	/**
	 * Gets the channel user name.
	 * 
	 * @return the channel user name
	 */
	public String getChannelUserName() {
		return (String) getPropertyValue(TRUConstants.CHANNEL_USER_NAME);
	}

	/**
	 * Sets the channel user name.
	 * 
	 * @param pChannelUserName
	 *            the new channel user name
	 */
	public void setChannelUserName(String pChannelUserName) {
		setPropertyValue(TRUConstants.CHANNEL_USER_NAME, pChannelUserName);
	}

	/**
	 * Gets the channel co user name.
	 * 
	 * @return the channel co user name
	 */
	public String getChannelCoUserName() {
		return (String) getPropertyValue(TRUConstants.CHANNEL_CO_USER_NAME);
	}

	/**
	 * Sets the channel co user name.
	 * 
	 * @param pChannelCoUserName
	 *            the new channel co user name
	 */
	public void setChannelCoUserName(String pChannelCoUserName) {
		setPropertyValue(TRUConstants.CHANNEL_CO_USER_NAME, pChannelCoUserName);
	}

	
	/**
	 * Gets the channel name.
	 *
	 * @return the channel name
	 */
	public String getChannelName() {
		return (String) getPropertyValue(TRUConstants.CHANNEL_NAME);
	}

	/**
	 * Sets the channel name.
	 *
	 * @param pChannelName the new channel name
	 */
	public void setChannelName(String pChannelName) {
		setPropertyValue(TRUConstants.CHANNEL_NAME, pChannelName);
	}
	
	/**
	 * Gets the registrant id.
	 *
	 * @return the registrant id
	 */
	public String getRegistrantId() {
		return (String) getPropertyValue(TRUConstants.REGISTRANT_ID);
	}

	/**
	 * Sets the registrant id.
	 *
	 * @param pRegistrantId the new registrant id
	 */
	public void setRegistrantId(String pRegistrantId) {
		setPropertyValue(TRUConstants.REGISTRANT_ID, pRegistrantId);
	}
	
	/**
	 * Gets the source channel.
	 *
	 * @return the source channel
	 */
	public String getSourceChannel() {
		return (String) getPropertyValue(TRUConstants.SOURCE_CHANNEL);
	}
	/**
	 * Sets the source channel.
	 *
	 * @param pSourceChannel the new source channel
	 */
	public void setSourceChannel(String pSourceChannel) {
		setPropertyValue(TRUConstants.SOURCE_CHANNEL, pSourceChannel);
	}
}
