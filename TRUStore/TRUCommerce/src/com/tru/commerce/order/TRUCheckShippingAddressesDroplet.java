package com.tru.commerce.order;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.order.ShippingGroup;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.order.purchase.TRUShippingGroupContainerService;
import com.tru.common.TRUConstants;

/**
 * This class is TRUCheckShippingAddressesDroplet.
 *
 * @author PA
 * @version 1.0
 */
public class TRUCheckShippingAddressesDroplet extends DynamoServlet {

	/**
	 * checks for available US addresses.
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		final TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) 
				pRequest.getObjectParameter(TRUCommerceConstants.SHIPPING_GROUP_MAP_CONTAINER);  
		boolean hasSavedAddress = false;
		
		if (shippingGroupMapContainer == null) {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
			return;
		}
		
		final Map shippingGroupMapForDisplay = shippingGroupMapContainer.getShippingGroupMapForDisplay();
		final Set<String> shippingGroupKeys = shippingGroupMapForDisplay.keySet();
		for (String shippingGroupKey : shippingGroupKeys) {
			final ShippingGroup shippingGroup = (ShippingGroup) shippingGroupMapForDisplay.get(shippingGroupKey);
			if (shippingGroup instanceof TRUHardgoodShippingGroup) {
				final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
				if (TRUConstants.UNITED_STATES.equalsIgnoreCase(hardgoodShippingGroup.getShippingAddress().getCountry()) && !hasSavedAddress && !hardgoodShippingGroup.isBillingAddress()) {
						hasSavedAddress = true;
						break;
				}
			}
		}
		
		if (hasSavedAddress) {
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		} else {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		}
	}
}
