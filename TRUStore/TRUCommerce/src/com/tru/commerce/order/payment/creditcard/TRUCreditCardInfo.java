package com.tru.commerce.order.payment.creditcard;

import atg.commerce.order.Order;
import atg.payment.creditcard.CreditCardInfo;
import atg.payment.creditcard.GenericCreditCardInfo;

import com.tru.commerce.payment.PaymentInfo;

/**
 * The Class TRUCreditCardInfo.
 *
 * @author PA
 * @version 1.0
 */
public class TRUCreditCardInfo extends GenericCreditCardInfo implements PaymentInfo,CreditCardInfo {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** Property to hold the  order id. */
	private String mOrderId;
	
	/** Property to hold the  cvv. */
	private String mCvv;
	
	/** Property to hold the  expiration date. */
	private String mExpirationDate;
	
	/** Property to hold the  card holder name. */
	private String mCardHolderName;
	
	/** Property to hold the  sd dba name. */
	private String mSDDbaName;
	
	/** Property to hold the  sd street. */
	private String mSDStreet;
	
	/** Property to hold the  sd city. */
	private String mSDCity;
	
	/** Property to hold the  sd region. */
	private String mSDRegion;
	
	/** Property to hold the  sd mid. */
	private String mSDMid;
	
	/** Property to hold the  sd mcc. */
	private String mSDMcc;
	
	/** Property to hold the  sd postal code. */
	private String mSDPostalCode;
	
	/** Property to hold the  sd country code. */
	private String mSDCountryCode;
	
	/** Property to hold the  sd merchant contact info. */
	private String mSDMerchantContactInfo;
	
	/** Property to hold the  tax number. */
	private String mTaxNumber;
	
	/** Property to hold the  tax amount. */
	private double mTaxAmount;
	
	/** Property to hold the  token type. */
	private String mTokenType;
	
	/** Property to hold the  merchant ref number. */
	private String mMerchantRefNumber;
	
	/** Property to hold the  email. */
	private String mEmail;
	
	/** Property to hold the  transaction id. */
	private String mTransactionId;
	
	/** Property to hold the  transaction tag. */
	private String mTransactionTag;
	
	/** Property to hold the  transaction type. */
	private String mTransactionType;
	
	/** Property to hold the  order. */
	private Order mOrder;
	
	/** Property to hold the  cavv. */
	private String mCavv;
	
	/** Property to hold the  eci flag. */
	private String mEciFlag;
	
	/** Property to hold the  pa res status. */
	private String mPaResStatus;
	
	/** Property to hold the  signature verification. */
	private String mSignatureVerification;
	
	/** Property to hold the  xid. */
	private String mXid;
	
	/** Property to hold the  enrolled. */
	private String mEnrolled;
	
	/** Property to hold the  action code. */
	private String mActionCode;
	
	/** Property to hold the no of retries	 */
	private int mRetryCount;
	
	/** Property to hold the siteId. */
	private String mSiteId;
	
	/**
	 * @return the retryCount
	 */
	public int getRetryCount() {
		return mRetryCount;
	}

	/**
	 * @param pRetryCount the retryCount to set
	 */
	public void setRetryCount(int pRetryCount) {
		mRetryCount = pRetryCount;
	}

	/**
	 * Gets the order.
	 *
	 * @return the order
	 */
	public Order getOrder() {
		return mOrder;
	}
	
	/**
	 * Sets the order.
	 *
	 * @param pOrder the order to set
	 */
	public void setOrder(Order pOrder) {
		mOrder = pOrder;
	}
	
	/**
	 * Gets the transaction type.
	 *
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return mTransactionType;
	}
	
	/**
	 * Sets the transaction type.
	 *
	 * @param pTransactionType the transactionType to set
	 */
	public void setTransactionType(String pTransactionType) {
		mTransactionType = pTransactionType;
	}
	
	/**
	 * Gets the transaction tag.
	 *
	 * @return the transactionTag
	 */
	public String getTransactionTag() {
		return mTransactionTag;
	}
	
	/**
	 * Sets the transaction tag.
	 *
	 * @param pTransactionTag the transactionTag to set
	 */
	public void setTransactionTag(String pTransactionTag) {
		mTransactionTag = pTransactionTag;
	}
	
	/**
	 * Gets the transaction id.
	 *
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return mTransactionId;
	}
	
	/**
	 * Sets the transaction id.
	 *
	 * @param pTransactionId the transactionId to set
	 */
	public void setTransactionId(String pTransactionId) {
		mTransactionId = pTransactionId;
	}
	
	/**
	 * Gets the tax amount.
	 *
	 * @return the taxAmount
	 */
	public double getTaxAmount() {
		return mTaxAmount;
	}
	
	/**
	 * Sets the tax amount.
	 *
	 * @param pTaxAmount the taxAmount to set
	 */
	public void setTaxAmount(double pTaxAmount) {
		mTaxAmount = pTaxAmount;
	}
	
	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return mEmail;
	}
	
	/**
	 * Sets the email.
	 *
	 * @param pEmail the email to set
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}
	
	/**
	 * Gets the merchant ref number.
	 *
	 * @return the merchantRefNumber
	 */
	public String getMerchantRefNumber() {
		return mMerchantRefNumber;
	}
	
	/**
	 * Sets the merchant ref number.
	 *
	 * @param pMerchantRefNumber the merchantRefNumber to set
	 */
	public void setMerchantRefNumber(String pMerchantRefNumber) {
		mMerchantRefNumber = pMerchantRefNumber;
	}
	
	/**
	 * Gets the token type.
	 *
	 * @return the tokenType
	 */
	public String getTokenType() {
		return mTokenType;
	}
	
	/**
	 * Sets the token type.
	 *
	 * @param pTokenType the tokenType to set
	 */
	public void setTokenType(String pTokenType) {
		mTokenType = pTokenType;
	}
	
	/**
	 * Gets the tax number.
	 *
	 * @return the taxNumber
	 */
	public String getTaxNumber() {
		return mTaxNumber;
	}
	
	/**
	 * Sets the tax number.
	 *
	 * @param pTaxNumber the taxNumber to set
	 */
	public void setTaxNumber(String pTaxNumber) {
		mTaxNumber = pTaxNumber;
	}
	
	/**
	 * Gets the SD street.
	 *
	 * @return the sDStreet
	 */
	public String getSDStreet() {
		return mSDStreet;
	}
	
	/**
	 * Sets the SD street.
	 *
	 * @param pSDStreet the sDStreet to set
	 */
	public void setSDStreet(String pSDStreet) {
		mSDStreet = pSDStreet;
	}
	
	/**
	 * Gets the SD dba name.
	 *
	 * @return the sDDbaName
	 */
	public String getSDDbaName() {
		return mSDDbaName;
	}
	
	/**
	 * Sets the SD dba name.
	 *
	 * @param pSDDbaName the sDDbaName to set
	 */
	public void setSDDbaName(String pSDDbaName) {
		mSDDbaName = pSDDbaName;
	}
	
	/**
	 * Gets the SD city.
	 *
	 * @return the sDCity
	 */
	public String getSDCity() {
		return mSDCity;
	}
	
	/**
	 * Sets the SD city.
	 *
	 * @param pSDCity the sDCity to set
	 */
	public void setSDCity(String pSDCity) {
		mSDCity = pSDCity;
	}
	
	/**
	 * Gets the SD region.
	 *
	 * @return the sDRegion
	 */
	public String getSDRegion() {
		return mSDRegion;
	}
	
	/**
	 * Sets the SD region.
	 *
	 * @param pSDRegion the sDRegion to set
	 */
	public void setSDRegion(String pSDRegion) {
		mSDRegion = pSDRegion;
	}
	
	/**
	 * Gets the SD mid.
	 *
	 * @return the sDMid
	 */
	public String getSDMid() {
		return mSDMid;
	}
	
	/**
	 * Sets the SD mid.
	 *
	 * @param pSDMid the sDMid to set
	 */
	public void setSDMid(String pSDMid) {
		mSDMid = pSDMid;
	}
	
	/**
	 * Gets the SD mcc.
	 *
	 * @return the sDMcc
	 */
	public String getSDMcc() {
		return mSDMcc;
	}
	
	/**
	 * Sets the SD mcc.
	 *
	 * @param pSDMcc the sDMcc to set
	 */
	public void setSDMcc(String pSDMcc) {
		mSDMcc = pSDMcc;
	}
	
	/**
	 * Gets the SD postal code.
	 *
	 * @return the sDPostalCode
	 */
	public String getSDPostalCode() {
		return mSDPostalCode;
	}
	
	/**
	 * Sets the SD postal code.
	 *
	 * @param pSDPostalCode the sDPostalCode to set
	 */
	public void setSDPostalCode(String pSDPostalCode) {
		mSDPostalCode = pSDPostalCode;
	}
	
	/**
	 * Gets the SD country code.
	 *
	 * @return the sDCountryCode
	 */
	public String getSDCountryCode() {
		return mSDCountryCode;
	}
	
	/**
	 * Sets the SD country code.
	 *
	 * @param pSDCountryCode the sDCountryCode to set
	 */
	public void setSDCountryCode(String pSDCountryCode) {
		mSDCountryCode = pSDCountryCode;
	}
	
	/**
	 * Gets the SD merchant contact info.
	 *
	 * @return the sDMerchantContactInfo
	 */
	public String getSDMerchantContactInfo() {
		return mSDMerchantContactInfo;
	}
	
	/**
	 * Sets the SD merchant contact info.
	 *
	 * @param pSDMerchantContactInfo the sDMerchantContactInfo to set
	 */
	public void setSDMerchantContactInfo(String pSDMerchantContactInfo) {
		mSDMerchantContactInfo = pSDMerchantContactInfo;
	}
	
	/**
	 * Gets the card holder name.
	 *
	 * @return the cardHolderName
	 */
	public String getCardHolderName() {
		return mCardHolderName;
	}
	
	/**
	 * Sets the card holder name.
	 *
	 * @param pCardHolderName the cardHolderName to set
	 */
	public void setCardHolderName(String pCardHolderName) {
		mCardHolderName = pCardHolderName;
	}
	
	/**
	 * Gets the expiration date.
	 *
	 * @return the expirationDate
	 */
	public String getExpirationDate() {
		return mExpirationDate;
	}
	
	/**
	 * Sets the expiration date.
	 *
	 * @param pExpirationDate the expirationDate to set
	 */
	public void setExpirationDate(String pExpirationDate) {
		mExpirationDate = pExpirationDate;
	}
	
	/**
	 * Gets the cvv.
	 *
	 * @return the cvv
	 */
	public String getCvv() {
		return mCvv;
	}
	
	/**
	 * Sets the cvv.
	 *
	 * @param pCvv the cvv to set
	 */
	public void setCvv(String pCvv) {
		mCvv = pCvv;
	}
	
		
	/**
	 * Sets the order id.
	 *
	 * @param pOrderId the mOrderId to set
	 */
	public void setOrderId(String pOrderId) {
		this.mOrderId = pOrderId;
	}
	
	/**
	 * Gets the order id.
	 *
	 * @return the mOrderId
	 */
	public String getOrderId() {
		return mOrderId;
	}
	
	/**
	 * Gets the cavv.
	 *
	 * @return the cavv
	 */
	public String getCavv() {
		return mCavv;
	}
	
	/**
	 * Sets the cavv.
	 *
	 * @param pCavv the cavv to set
	 */
	public void setCavv(String pCavv) {
		mCavv = pCavv;
	}
	
	/**
	 * Gets the eci flag.
	 *
	 * @return the eiFlag
	 */
	public String getEciFlag() {
		return mEciFlag;
	}
	
	/**
	 * Sets the eci flag.
	 *
	 * @param pEciFlag the eiFlag to set
	 */
	public void setEciFlag(String pEciFlag) {
		mEciFlag = pEciFlag;
	}
	
	/**
	 * Gets the pa res status.
	 *
	 * @return the paResStatus
	 */
	public String getPaResStatus() {
		return mPaResStatus;
	}
	
	/**
	 * Sets the pa res status.
	 *
	 * @param pPaResStatus the paResStatus to set
	 */
	public void setPaResStatus(String pPaResStatus) {
		mPaResStatus = pPaResStatus;
	}
	
	/**
	 * Gets the signature verification.
	 *
	 * @return the signatureVerification
	 */
	public String getSignatureVerification() {
		return mSignatureVerification;
	}
	
	/**
	 * Sets the signature verification.
	 *
	 * @param pSignatureVerification the signatureVerification to set
	 */
	public void setSignatureVerification(String pSignatureVerification) {
		mSignatureVerification = pSignatureVerification;
	}
	
	/**
	 * Gets the xid.
	 *
	 * @return the xid
	 */
	public String getXid() {
		return mXid;
	}
	
	/**
	 * Sets the xid.
	 *
	 * @param pXid the xid to set
	 */
	public void setXid(String pXid) {
		mXid = pXid;
	}
	
	/**
	 * Gets the enrolled.
	 *
	 * @return the enrolled
	 */
	public String getEnrolled() {
		return mEnrolled;
	}
	
	/**
	 * Sets the enrolled.
	 *
	 * @param pEnrolled the enrolled to set
	 */
	public void setEnrolled(String pEnrolled) {
		mEnrolled = pEnrolled;
	}
	
	/**
	 * Gets the action code.
	 *
	 * @return the actionCode
	 */
	public String getActionCode() {
		return mActionCode;
	}
	
	/**
	 * Sets the action code.
	 *
	 * @param pActionCode the actionCode to set
	 */
	public void setActionCode(String pActionCode) {
		mActionCode = pActionCode;
	}

	/**
	 * Gets the siteId.
	 *
	 * @return the mSiteId
	 */
	public String getSiteId() {
		return mSiteId;
	}

	/**
	 * Sets the siteId.
	 *
	 * @param pSiteId the mSiteId to set
	 */
	public void setSiteId(String pSiteId) {
		this.mSiteId = pSiteId;
	}
	
	/** The Previous response code. */
	private String mPreviousResponseCode;

	/**
	 * @return the previousResponseCode
	 */
	public String getPreviousResponseCode() {
		return mPreviousResponseCode;
	}

	/**
	 * @param pPreviousResponseCode the previousResponseCode to set
	 */
	public void setPreviousResponseCode(String pPreviousResponseCode) {
		mPreviousResponseCode = pPreviousResponseCode;
	}
	
}