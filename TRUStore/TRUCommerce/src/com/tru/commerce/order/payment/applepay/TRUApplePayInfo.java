package com.tru.commerce.order.payment.applepay;

import java.math.BigDecimal;

import com.tru.commerce.payment.PaymentInfo;

/**
 * The Class TRUApplePayInfo.
 */
public class TRUApplePayInfo  implements PaymentInfo {

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7475628501394974670L;

	/**
	 * Property to denote the Apple Amount.
	 */
	private BigDecimal mAmount;
	
	/**
	 *  Property to denote the Currency code.
	 */
	private String mCurrencyCode;
	/**
	 * Property to denote the Customer emai.
	 */
	private String mCustomerEmail;
	/**
	 * Property to denote the Customer ip address.
	 */
	private String mCustomerIPAddress;
	
	/**
	 * Property to denote the Currency code.
	 */
	private String mBillingFirstName;
    
    /** The m billing last name. */
	private String mBillingLastName;
    
    /** The m billing phone no. */
	private String mBillingPhoneNo;
    
    /** The m billing address line 1. */
	private String mBillingAddressLine1;
    
    /** The m billing address line 2. */
	private String mBillingAddressLine2;
    
    /** The m billing address line 3. */
	private String mBillingAddressLine3;
    
    /** The m billing address line 4. */
	private String mBillingAddressLine4;
    
    /** The m billing address city. */
	private String mBillingAddressCity;
    
    /** The m billing address state. */
	private String mBillingAddressState;
    
    /** The m billing address country code. */
	private String mBillingAddressCountryCode;
    
    /** The m billing address postal code. */
	private String mBillingAddressPostalCode;
	
	/** The m shipping first name. */
	private String mShippingFirstName;
    
    /** The m shipping last name. */
	private String mShippingLastName;
    
    /** The m shipping phone no. */
	private String mShippingPhoneNo;
    
    /** The m shipping address line 1. */
	private String mShippingAddressLine1;
    
    /** The m shipping address line 2. */
	private String mShippingAddressLine2;
    
    /** The m shipping address line 3. */
	private String mShippingAddressLine3;
    
    /** The m shipping address line 4. */
	private String mShippingAddressLine4;
    
    /** The m shipping city. */
    private String mShippingCity;
    
    /** The m shipping state. */
    private String mShippingState;
    
    /** The m shipping country code. */
    private String mShippingCountryCode;
    
    /** The m shipping postal code. */
    private String mShippingPostalCode;
    
	/** Holds apple pay tranaction Id. */
    private String mApplePayTransactionId;
	
	/** Holds apple pay Ephemerapublickey. */
    private String mEphemeralPublicKey;
	
	/** Holds apply pay publickeyhash. */
    private String mPublicKeyHash;
	
    /** Holds apple pay Data. */
	private String mData;
	
	/** Holds apple pay signature. */
	private String mSignature;
	
	/** Holds version. */
	private String mVersion;


	/**
	 * Holds order id.
	 */
	private String mOrderId;

	/**
	 * Sets the order id.
	 *
	 * @param pOrderId            the mOrderId to set
	 */
	public void setOrderId(String pOrderId) {
		this.mOrderId = pOrderId;
	}

	/**
	 * Gets the order id.
	 *
	 * @return the mOrderId
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * Gets the amount.
	 *
	 * @return the mAmount
	 */
	public BigDecimal getAmount() {
		return mAmount;
	}

	/**
	 * Sets the amount.
	 *
	 * @param pAmount the new amount
	 */
	public void setAmount(BigDecimal pAmount) {
		this.mAmount = pAmount;
	}

	/**
	 * Gets the currency code.
	 *
	 * @return the mCurrencyCode
	 */
	public String getCurrencyCode() {
		return mCurrencyCode;
	}

	/**
	 * Sets the currency code.
	 *
	 * @param pCurrencyCode the new currency code
	 */
	public void setCurrencyCode(String pCurrencyCode) {
		this.mCurrencyCode = pCurrencyCode;
	}

	/**
	 * Gets the customer email.
	 *
	 * @return the mCustomerEmail
	 */
	public String getCustomerEmail() {
		return mCustomerEmail;
	}

	/**
	 * Sets the customer email.
	 *
	 * @param pCustomerEmail the new customer email
	 */
	public void setCustomerEmail(String pCustomerEmail) {
		this.mCustomerEmail = pCustomerEmail;
	}

	/**
	 * Gets the customer IP address.
	 *
	 * @return the mCustomerIPAddress
	 */
	public String getCustomerIPAddress() {
		return mCustomerIPAddress;
	}

	/**
	 * Sets the customer IP address.
	 *
	 * @param pCustomerIPAddress the new customer IP address
	 */
	public void setCustomerIPAddress(String pCustomerIPAddress) {
		this.mCustomerIPAddress = pCustomerIPAddress;
	}

	/**
	 * Gets the billing first name.
	 *
	 * @return the mBillingFirstName
	 */
	public String getBillingFirstName() {
		return mBillingFirstName;
	}

	/**
	 * Sets the billing first name.
	 *
	 * @param pBillingFirstName the new billing first name
	 */
	public void setBillingFirstName(String pBillingFirstName) {
		this.mBillingFirstName = pBillingFirstName;
	}

	/**
	 * Gets the billing last name.
	 *
	 * @return the mBillingLastName
	 */
	public String getBillingLastName() {
		return mBillingLastName;
	}

	/**
	 * Sets the billing last name.
	 *
	 * @param pBillingLastName the new billing last name
	 */
	public void setBillingLastName(String pBillingLastName) {
		this.mBillingLastName = pBillingLastName;
	}

	/**
	 * Gets the billing phone no.
	 *
	 * @return the mBillingPhoneNo
	 */
	public String getBillingPhoneNo() {
		return mBillingPhoneNo;
	}

	/**
	 * Sets the billing phone no.
	 *
	 * @param pBillingPhoneNo the new billing phone no
	 */
	public void setBillingPhoneNo(String pBillingPhoneNo) {
		this.mBillingPhoneNo = pBillingPhoneNo;
	}

	/**
	 * Gets the billing address line 1.
	 *
	 * @return the mBillingAddressLine1
	 */
	public String getBillingAddressLine1() {
		return mBillingAddressLine1;
	}

	/**
	 * Sets the billing address line 1.
	 *
	 * @param pBillingAddressLine1 the new billing address line 1
	 */
	public void setBillingAddressLine1(String pBillingAddressLine1) {
		this.mBillingAddressLine1 = pBillingAddressLine1;
	}

	/**
	 * Gets the billing address line 2.
	 *
	 * @return the mBillingAddressLine2
	 */
	public String getBillingAddressLine2() {
		return mBillingAddressLine2;
	}

	/**
	 * Sets the billing address line 2.
	 *
	 * @param pBillingAddressLine2 the new billing address line 2
	 */
	public void setBillingAddressLine2(String pBillingAddressLine2) {
		this.mBillingAddressLine2 = pBillingAddressLine2;
	}

	/**
	 * Gets the billing address line 3.
	 *
	 * @return the mBillingAddressLine3
	 */
	public String getBillingAddressLine3() {
		return mBillingAddressLine3;
	}

	/**
	 * Sets the billing address line 3.
	 *
	 * @param pBillingAddressLine3 the new billing address line 3
	 */
	public void setBillingAddressLine3(String pBillingAddressLine3) {
		this.mBillingAddressLine3 = pBillingAddressLine3;
	}

	/**
	 * Gets the billing address line 4.
	 *
	 * @return the mBillingAddressLine4
	 */
	public String getBillingAddressLine4() {
		return mBillingAddressLine4;
	}

	/**
	 * Sets the billing address line 4.
	 *
	 * @param pBillingAddressLine4 the new billing address line 4
	 */
	public void setBillingAddressLine4(String pBillingAddressLine4) {
		this.mBillingAddressLine4 = pBillingAddressLine4;
	}

	/**
	 * Gets the billing address city.
	 *
	 * @return the mBillingAddressCity
	 */
	public String getBillingAddressCity() {
		return mBillingAddressCity;
	}

	/**
	 * Sets the billing address city.
	 *
	 * @param pBillingAddressCity the new billing address city
	 */
	public void setBillingAddressCity(String pBillingAddressCity) {
		this.mBillingAddressCity = pBillingAddressCity;
	}

	/**
	 * Gets the billing address state.
	 *
	 * @return the mBillingAddressState
	 */
	public String getBillingAddressState() {
		return mBillingAddressState;
	}

	/**
	 * Sets the billing address state.
	 *
	 * @param pBillingAddressState the new billing address state
	 */
	public void setBillingAddressState(String pBillingAddressState) {
		this.mBillingAddressState = pBillingAddressState;
	}

	/**
	 * Gets the billing address country code.
	 *
	 * @return the mBillingAddressCountryCode
	 */
	public String getBillingAddressCountryCode() {
		return mBillingAddressCountryCode;
	}

	/**
	 * Sets the billing address country code.
	 *
	 * @param pBillingAddressCountryCode the new billing address country code
	 */
	public void setBillingAddressCountryCode(String pBillingAddressCountryCode) {
		this.mBillingAddressCountryCode = pBillingAddressCountryCode;
	}

	/**
	 * Gets the billing address postal code.
	 *
	 * @return the mBillingAddressPostalCode
	 */
	public String getBillingAddressPostalCode() {
		return mBillingAddressPostalCode;
	}

	/**
	 * Sets the billing address postal code.
	 *
	 * @param pBillingAddressPostalCode the new billing address postal code
	 */
	public void setBillingAddressPostalCode(String pBillingAddressPostalCode) {
		this.mBillingAddressPostalCode = pBillingAddressPostalCode;
	}

	/**
	 * Gets the shipping first name.
	 *
	 * @return the mShippingFirstName
	 */
	public String getShippingFirstName() {
		return mShippingFirstName;
	}

	/**
	 * Sets the shipping first name.
	 *
	 * @param pShippingFirstName the new shipping first name
	 */
	public void setShippingFirstName(String pShippingFirstName) {
		this.mShippingFirstName = pShippingFirstName;
	}

	/**
	 * Gets the shipping last name.
	 *
	 * @return the mShippingLastName
	 */
	public String getShippingLastName() {
		return mShippingLastName;
	}

	/**
	 * Sets the shipping last name.
	 *
	 * @param pShippingLastName the new shipping last name
	 */
	public void setShippingLastName(String pShippingLastName) {
		this.mShippingLastName = pShippingLastName;
	}

	/**
	 * Gets the shipping phone no.
	 *
	 * @return the mShippingPhoneNo
	 */
	public String getShippingPhoneNo() {
		return mShippingPhoneNo;
	}

	/**
	 * Sets the shipping phone no.
	 *
	 * @param pShippingPhoneNo the new shipping phone no
	 */
	public void setShippingPhoneNo(String pShippingPhoneNo) {
		this.mShippingPhoneNo = pShippingPhoneNo;
	}

	/**
	 * Gets the shipping address line 1.
	 *
	 * @return the mShippingAddressLine1
	 */
	public String getShippingAddressLine1() {
		return mShippingAddressLine1;
	}

	/**
	 * Sets the shipping address line 1.
	 *
	 * @param pShippingAddressLine1 the new shipping address line 1
	 */
	public void setShippingAddressLine1(String pShippingAddressLine1) {
		this.mShippingAddressLine1 = pShippingAddressLine1;
	}

	/**
	 * Gets the shipping address line 2.
	 *
	 * @return the mShippingAddressLine2
	 */
	public String getShippingAddressLine2() {
		return mShippingAddressLine2;
	}

	/**
	 * Sets the shipping address line 2.
	 *
	 * @param pShippingAddressLine2 the new shipping address line 2
	 */
	public void setShippingAddressLine2(String pShippingAddressLine2) {
		this.mShippingAddressLine2 = pShippingAddressLine2;
	}

	/**
	 * Gets the shipping address line 3.
	 *
	 * @return the mShippingAddressLine3
	 */
	public String getShippingAddressLine3() {
		return mShippingAddressLine3;
	}

	/**
	 * Sets the shipping address line 3.
	 *
	 * @param pShippingAddressLine3 the new shipping address line 3
	 */
	public void setShippingAddressLine3(String pShippingAddressLine3) {
		this.mShippingAddressLine3 = pShippingAddressLine3;
	}

	/**
	 * Gets the shipping address line 4.
	 *
	 * @return the mShippingAddressLine4
	 */
	public String getShippingAddressLine4() {
		return mShippingAddressLine4;
	}

	/**
	 * Sets the shipping address line 4.
	 *
	 * @param pShippingAddressLine4 the new shipping address line 4
	 */
	public void setShippingAddressLine4(String pShippingAddressLine4) {
		this.mShippingAddressLine4 = pShippingAddressLine4;
	}

	/**
	 * Gets the shipping city.
	 *
	 * @return the mShippingCity
	 */
	public String getShippingCity() {
		return mShippingCity;
	}

	/**
	 * Sets the shipping city.
	 *
	 * @param pShippingCity the new shipping city
	 */
	public void setShippingCity(String pShippingCity) {
		this.mShippingCity = pShippingCity;
	}

	/**
	 * Gets the shipping state.
	 *
	 * @return the mShippingState
	 */
	public String getShippingState() {	
		return mShippingState;
	}

	/**
	 * Sets the shipping state.
	 *
	 * @param pShippingState the new shipping state
	 */
	public void setShippingState(String pShippingState) {
		this.mShippingState = pShippingState;
	}

	/**
	 * Gets the shipping country code.
	 *
	 * @return the mShippingCountryCode
	 */
	public String getShippingCountryCode() {
		return mShippingCountryCode;
	}

	/**
	 * Sets the shipping country code.
	 *
	 * @param pShippingCountryCode the new shipping country code
	 */
	public void setShippingCountryCode(String pShippingCountryCode) {
		this.mShippingCountryCode = pShippingCountryCode;
	}

	/**
	 * Gets the shipping postal code.
	 *
	 * @return the mShippingPostalCode
	 */
	public String getShippingPostalCode() {
		return mShippingPostalCode;
	}

	/**
	 * Sets the shipping postal code.
	 *
	 * @param pShippingPostalCode the new shipping postal code
	 */
	public void setShippingPostalCode(String pShippingPostalCode) {
		this.mShippingPostalCode = pShippingPostalCode;
	}

	/**
	 * Gets the apple pay transaction id.
	 *
	 * @return the mApplePayTransactionId
	 */
	public String getApplePayTransactionId() {
		return mApplePayTransactionId;
	}

	/**
	 * Sets the apple pay transaction id.
	 *
	 * @param pApplePayTransactionId the new apple pay transaction id
	 */
	public void setApplePayTransactionId(String pApplePayTransactionId) {
		this.mApplePayTransactionId = pApplePayTransactionId;
	}

	/**
	 * Gets the ephemeral public key.
	 *
	 * @return the mEphemeralPublicKey
	 */
	public String getEphemeralPublicKey() {
		return mEphemeralPublicKey;
	}

	/**
	 * Sets the ephemeral public key.
	 *
	 * @param pEphemeralPublicKey the new ephemeral public key
	 */
	public void setEphemeralPublicKey(String pEphemeralPublicKey) {
		this.mEphemeralPublicKey = pEphemeralPublicKey;
	}

	/**
	 * Gets the public key hash.
	 *
	 * @return the mPublicKeyHash
	 */
	public String getPublicKeyHash() {
		return mPublicKeyHash;
	}

	/**
	 * Sets the public key hash.
	 *
	 * @param pPublicKeyHash the new public key hash
	 */
	public void setPublicKeyHash(String pPublicKeyHash) {
		this.mPublicKeyHash = pPublicKeyHash;
	}

	/**
	 * Gets the data.
	 *
	 * @return the mData
	 */
	public String getData() {
		return mData;
	}

	/**
	 * Sets the data.
	 *
	 * @param pData the new data
	 */
	public void setData(String pData) {
		this.mData = pData;
	}

	/**
	 * Gets the signature.
	 *
	 * @return the mSignature
	 */
	public String getSignature() {
		return mSignature;
	}

	/**
	 * Sets the signature.
	 *
	 * @param pSignature the new signature
	 */
	public void setSignature(String pSignature) {
		this.mSignature = pSignature;
	}

	/**
	 * Gets the version.
	 *
	 * @return the mVersion
	 */
	public String getVersion() {
		return mVersion;
	}

	/**
	 * Sets the version.
	 *
	 * @param pVersion the new version
	 */
	public void setVersion(String pVersion) {
		this.mVersion = pVersion;
	}


}
