package com.tru.commerce.order.payment.creditcard;

import java.util.Date;

import atg.payment.creditcard.CreditCardStatus;

import com.tru.commerce.order.payment.TRUPaymentStatus;

// TODO: Auto-generated Javadoc
/**
 * This class maintain the credit card request and response statuses.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUCreditCardStatus extends TRUPaymentStatus implements CreditCardStatus {

	/**
	 * holds the serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * holds the mPromoFinanceRate.
	 */
	private String mPromoFinanceRate;
	
	/**
	 * holds the mPromoText.
	 */
	private String mPromoText;
	
	/**
	 * holds the mAvsDescriptiveResult.
	 */
	private String mAvsDescriptiveResult;

	/**
	 * holds the mAuthorizationExpiration.
	 */
	private Date mAuthorizationExpiration ;
	
	/**
	 * holds the mPromoDetails.
	 */
	private String mPromoDetails;
	
	/**
	 * holds the mAuthResponseCode
	 */
	private String mAuthResponseCode;
	/**
	 * holds the mBankAuthCode
	 */
	private String mBankAuthCode;
	/**
	 * holds the mCvv2Code
	 */
	private String mCvvCode;
	
	/**
	 * holds the mCreditCardToken
	 */
	private String mCreditCardToken;
	
	/**
	 * @return the String
	 */
	public String getCvvCode() {
		return mCvvCode;
	}

	/**
	 * @param pCvv2Code the cvv2Code to set
	 */
	public void setCvvCode(String pCvv2Code) {
		mCvvCode = pCvv2Code;
	}

	/**
	 * holds the mAvsCode
	 */
	private String mAvsCode;
	
	/**
	 * @return the avsCode
	 */
	public String getAvsCode() {
		return mAvsCode;
	}

	/**
	 * @param pAvsCode the avsCode to set
	 */
	public void setAvsCode(String pAvsCode) {
		mAvsCode = pAvsCode;
	}

	/**
	 * @return the authResponseCode
	 */
	public String getAuthResponseCode() {
		return mAuthResponseCode;
	}

	/**
	 * @param pAuthResponseCode the authResponseCode to set
	 */
	public void setAuthResponseCode(String pAuthResponseCode) {
		mAuthResponseCode = pAuthResponseCode;
	}

	/**
	 * @return the bankAuthCode
	 */
	public String getBankAuthCode() {
		return mBankAuthCode;
	}

	/**
	 * @param pBankAuthCode the bankAuthCode to set
	 */
	public void setBankAuthCode(String pBankAuthCode) {
		mBankAuthCode = pBankAuthCode;
	}

	/**
	 * Gets the authorization expiration.
	 *
	 * @return the mAuthorizationExpiration
	 */
	public Date getAuthorizationExpiration() {
		return this.mAuthorizationExpiration;
	}

	/**
	 * Sets the authorization expiration.
	 *
	 * @param pAuthorizationExpiration the mAuthorizationExpiration to set
	 */
	public void setAuthorizationExpiration(Date pAuthorizationExpiration) {
		this.mAuthorizationExpiration = pAuthorizationExpiration;
	}

	/**
	 * Gets the avs descriptive result.
	 *
	 * @return the mAvsDescriptiveResult
	 */
	public String getAvsDescriptiveResult() {
		return this.mAvsDescriptiveResult;
	}

	/**
	 * Sets the avs descriptive result.
	 *
	 * @param pAvsDescriptiveResult  the mAvsDescriptiveResult to set
	 */
	public void setAvsDescriptiveResult(String pAvsDescriptiveResult) {
		this.mAvsDescriptiveResult = pAvsDescriptiveResult;
	}

	/**
	 * Gets the promo details.
	 *
	 * @return the mPromoDetails
	 */
	public String getPromoDetails() {
		return mPromoDetails;
	}

	/**
	 * Sets the promo details.
	 *
	 * @param pPromoDetails the mPromoDetails to set
	 */
	public void setPromoDetails(String pPromoDetails) {
		this.mPromoDetails = pPromoDetails;
	}

	/**
	 * Gets the credit card token.
	 *
	 * @return the mCreditCardToken
	 */
	public String getCreditCardToken() {
		return mCreditCardToken;
	}

	/**
	 * Sets the credit card token.
	 *
	 * @param pCreditCardToken the mCreditCardToken to set
	 */
	public void setCreditCardToken(String pCreditCardToken) {
		this.mCreditCardToken = pCreditCardToken;
	}

	/**
	 * This method is used to get promoText.
	 * @return promoText String
	 */
	public String getPromoText() {
		return mPromoText;
	}

	/**
	 * This method is used to set promoText.
	 *
	 * @param pPromoText the new promo text
	 */
	public void setPromoText(String pPromoText) {
		mPromoText = pPromoText;
	}

	/**
	 * This method is used to get promoFinanceRate.
	 * @return promoFinanceRate String
	 */
	public String getPromoFinanceRate() {
		return mPromoFinanceRate;
	}

	/**
	 * This method is used to set promoFinanceRate.
	 *
	 * @param pPromoFinanceRate the new promo finance rate
	 */
	public void setPromoFinanceRate(String pPromoFinanceRate) {
		mPromoFinanceRate = pPromoFinanceRate;
	}
	
}