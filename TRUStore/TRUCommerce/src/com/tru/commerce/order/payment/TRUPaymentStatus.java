package com.tru.commerce.order.payment;

import java.util.Date;

import atg.payment.PaymentStatusImpl;

/**
 * This class defines payment status properties.
 *  @author Professional Access
 *  @version 1.0
 */
public class TRUPaymentStatus extends PaymentStatusImpl {
	
	/**
	 * This proprety hold serialVersionUid.
	 */
	private static final long serialVersionUID = 1L;
	
	//Started JDA Implementation
	
	/**
	 * This property to hold the ResponseCode
	 */
	private String mResponseCode;
	
	/**
	 * @holds the mTransactionDTTM
	 */
	private Date mTransactionDTTM;
	
	/**
	 * @holds the mTransactionDTTM
	 */
	private String mFraudTransactionId;
	
	/**
	 * @holds the mTransactionDTTM
	 */
	private String mFraudScore;
	
	/**
	 * @holds the mTransactionDTTM
	 */
	private boolean mFraud;
	
	/**
	 * @holds the mTransactionDTTM
	 */
	private String mCurrency;
	
	/**
	 * @holds the mTransactionDTTM
	 */
	private String mActionCode;
	
	/**
	 * @holds the mTransactionDTTM
	 */
	private String mErrorCode;
	
	/**
	 * @holds the mTransactionType
	 */
	private String mTransactionType;
	
	/**
	 * @holds the mMethod
	 */
	private String mMethod;
	
	private String mReversalId;
	
	/**
	 * @holds the mRetryAcceptOrder
	 */
	private boolean mRetryAcceptOrder;

	/**
	 * @return the retryAcceptOrder
	 */
	public boolean isRetryAcceptOrder() {
		return mRetryAcceptOrder;
	}

	/**
	 * @param pRetryAcceptOrder the retryAcceptOrder to set
	 */
	public void setRetryAcceptOrder(boolean pRetryAcceptOrder) {
		mRetryAcceptOrder = pRetryAcceptOrder;
	}

	/**
	 * @return the reversalId
	 */
	public String getReversalId() {
		return mReversalId;
	}

	/**
	 * @param pReversalId the reversalId to set
	 */
	public void setReversalId(String pReversalId) {
		mReversalId = pReversalId;
	}

	/**
	 * @return the method
	 */
	public String getMethod() {
		return mMethod;
	}

	/**
	 * @param pMethod the method to set
	 */
	public void setMethod(String pMethod) {
		mMethod = pMethod;
	}

	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return mTransactionType;
	}

	/**
	 * @param pTransactionType the transactionType to set
	 */
	public void setTransactionType(String pTransactionType) {
		mTransactionType = pTransactionType;
	}

	/**
	 * @return the transactionDTTM
	 */
	public Date getTransactionDTTM() {
		return mTransactionDTTM;
	}

	/**
	 * @param pTransactionDTTM the transactionDTTM to set
	 */
	public void setTransactionDTTM(Date pTransactionDTTM) {
		mTransactionDTTM = pTransactionDTTM;
	}

	/**
	 * @return the fraudTransactionId
	 */
	public String getFraudTransactionId() {
		return mFraudTransactionId;
	}

	/**
	 * @param pFraudTransactionId the fraudTransactionId to set
	 */
	public void setFraudTransactionId(String pFraudTransactionId) {
		mFraudTransactionId = pFraudTransactionId;
	}

	/**
	 * @return the fraudScore
	 */
	public String getFraudScore() {
		return mFraudScore;
	}

	/**
	 * @param pFraudScore the fraudScore to set
	 */
	public void setFraudScore(String pFraudScore) {
		mFraudScore = pFraudScore;
	}

	/**
	 * @return the fraud
	 */
	public boolean isFraud() {
		return mFraud;
	}

	/**
	 * @param pFraud the fraud to set
	 */
	public void setFraud(boolean pFraud) {
		mFraud = pFraud;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return mCurrency;
	}

	/**
	 * @param pCurrency the currency to set
	 */
	public void setCurrency(String pCurrency) {
		mCurrency = pCurrency;
	}

	/**
	 * @return the actionCode
	 */
	public String getActionCode() {
		return mActionCode;
	}

	/**
	 * @param pActionCode the actionCode to set
	 */
	public void setActionCode(String pActionCode) {
		mActionCode = pActionCode;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return mErrorCode;
	}

	/**
	 * @param pErrorCode the errorCode to set
	 */
	public void setErrorCode(String pErrorCode) {
		mErrorCode = pErrorCode;
	}

	/**
	 * @return the mResponseCode
	 */
	public String getResponseCode() {
		return mResponseCode;
	}

	/**
	 * @param pResponseCode the mResponseCode to set
	 */
	public void setResponseCode(String pResponseCode) {
		mResponseCode = pResponseCode;
	}
}