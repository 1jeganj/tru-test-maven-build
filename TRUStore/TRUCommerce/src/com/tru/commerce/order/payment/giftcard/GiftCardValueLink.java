package com.tru.commerce.order.payment.giftcard;

/**
 * This is a bean class used to contain Gift Card ValueLink related information.
 * 
 * @author PA
 * @version 1.0
 */

public class GiftCardValueLink {

	/**
	 * @holds the mCardHolderName
	 */
	private String mCardHolderName;
	/**
	 * @holds the mCardNumber
	 */
	private String mCardNumber;
	/**
	 * @holds the mCardType
	 */
	private String mCardType;
	/**
	 * @holds the mAmount
	 */
	private String mAmount;
	/**
	 * @holds the mCardCost
	 */
	private String mCardCost;
	/**
	 * @holds the mReference3
	 */
	private String mReference3;
	/**
	 * @holds the mEan
	 */
	private String mEan;
	/**
	 * @holds the mVirtualCard
	 */
	private String mVirtualCard;
	/**
	 * @holds the mPreviousBalance
	 */
	private String mPreviousBalance;
	/**
	 * @holds the mCurrentBalance
	 */
	private String mCurrentBalance;

	/**
	 * @return the cardHolderName
	 */
	public String getCardHolderName() {
		return mCardHolderName;
	}

	/**
	 * @param pCardHolderName
	 *            the cardHolderName to set
	 */
	public void setCardHolderName(String pCardHolderName) {
		mCardHolderName = pCardHolderName;
	}

	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return mCardNumber;
	}

	/**
	 * @param pCardNumber
	 *            the cardNumber to set
	 */
	public void setCardNumber(String pCardNumber) {
		mCardNumber = pCardNumber;
	}

	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return mCardType;
	}

	/**
	 * @param pCardType
	 *            the cardType to set
	 */
	public void setCardType(String pCardType) {
		mCardType = pCardType;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return mAmount;
	}

	/**
	 * @param pAmount
	 *            the amount to set
	 */
	public void setAmount(String pAmount) {
		mAmount = pAmount;
	}

	/**
	 * @return the cardCost
	 */
	public String getCardCost() {
		return mCardCost;
	}

	/**
	 * @param pCardCost
	 *            the cardCost to set
	 */
	public void setCardCost(String pCardCost) {
		mCardCost = pCardCost;
	}

	/**
	 * @return the reference_3
	 */
	public String getReference3() {
		return mReference3;
	}

	/**
	 * @param pReference3
	 *            the reference_3 to set
	 */
	public void setReference3(String pReference3) {
		mReference3 = pReference3;
	}

	/**
	 * @return the ean
	 */
	public String getEan() {
		return mEan;
	}

	/**
	 * @param pEan
	 *            the ean to set
	 */
	public void setEan(String pEan) {
		mEan = pEan;
	}

	/**
	 * @return the virtualCard
	 */
	public String getVirtualCard() {
		return mVirtualCard;
	}

	/**
	 * @param pVirtualCard
	 *            the virtualCard to set
	 */
	public void setVirtualCard(String pVirtualCard) {
		mVirtualCard = pVirtualCard;
	}

	/**
	 * @return the previousBalance
	 */
	public String getPreviousBalance() {
		return mPreviousBalance;
	}

	/**
	 * @param pPreviousBalance
	 *            the previousBalance to set
	 */
	public void setPreviousBalance(String pPreviousBalance) {
		mPreviousBalance = pPreviousBalance;
	}

	/**
	 * @return the currentBalance
	 */
	public String getCurrentBalance() {
		return mCurrentBalance;
	}

	/**
	 * @param pCurrentBalance
	 *            the currentBalance to set
	 */
	public void setCurrentBalance(String pCurrentBalance) {
		mCurrentBalance = pCurrentBalance;
	}

}
