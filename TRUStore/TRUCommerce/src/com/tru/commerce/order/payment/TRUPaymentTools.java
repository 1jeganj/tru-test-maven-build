package com.tru.commerce.order.payment;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.service.idgen.SQLIdGenerator;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardInfo;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardStatus;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardInfo;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardStatus;
import com.tru.commerce.payment.util.PaymentIntegrationException;
import com.tru.common.TRUConstants;
import com.tru.common.cml.Constants;
import com.tru.payment.radial.TRUCreditCardPaymentProcessor;
import com.tru.payment.radial.TRUGiftCardPaymentProcessor;
import com.tru.payment.radial.TRURadialPaymentProcessor;
import com.tru.radial.exception.TRURadialIntegrationException;
import com.tru.radial.integration.exception.TRUPaymentIntegrationException;
import com.tru.radial.integration.util.TRUUIDGenerator;

/**
 * Tools class invokes service.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUPaymentTools extends GenericService {

	/**
	 * Property to hold OrderManager.
	 */
	private TRUOrderManager mOrderManager;

	private Map<String, String> mShippingMethodCodesMap;

	private List<String> mTranStatusForRefundCall;

	private String mDummyPhoneNumber;
	/**
	 * Holds component for IDGenerator.
	 */
	private SQLIdGenerator mIdGenerator;

	/**
	 * Holds the Radial payment processor.
	 */
	private TRURadialPaymentProcessor mRadialProcessor;
	
	/**
	 * Holds the Credit Card mCreditCardRespRetrySuccessCodes
	 */
	private List<String> mCreditCardRespManualRetrySuccessCodes;
	
	/**
	 * Holds the Credit Card mCreditCardRespSuccessCodes
	 */
	private List<String> mCreditCardRespSuccessCodes;
	
	/**
	 * Holds the Credit Card mFailureResponseCodes
	 */
	private Map<String, String> mFailureResponseCodes = new HashMap<String,String>();
	
	
	/**
	 * Holds the Credit Card mCreditCardRespSuccessCodes
	 */
	private List<String> mCreditCardRespAutoRetrySuccessCodes;
	
	/**
	 * Holds the mGiftCardRespSuccessCodes
	 */
	private List<String> mGiftCardRespSuccessCodes;
	
	/**
	 * Holds the mFailureGiftCardResponseCodesMap.
	 */
	private Map<String, String> mFailureGiftCardRedeemResponseCodes = new HashMap<String,String>();

	
	/**
	 * Holds the mBalanceInquiryResponseCodes.
	 */
	private Map<String, String> mBalanceInquiryResponseCodes = new HashMap<String,String>();
	
	/**
	 * Holds the mRedeemVoidResponseCodes.
	 */
	private Map<String, String> mRedeemVoidResponseCodes = new HashMap<String,String>();
	
	/**
	 * Holds the mValidateCreditCardSuccessCodes.
	 */
	private List<String> mValidateCreditCardSuccessCodes;
	
	/**
	 * Holds the mValidateCreditCardRetryCodes.
	 */
	private List<String> mValidateCreditCardRetryCodes;
	
	/**
	 * Holds the mValidateCreditCardRetryCodes.
	 */
	private String mValidateCreditCardRetryErrorKey;
	
	/**
	 * Holds the mValidateCreditCardErrorCodes.
	 */
	private Map<String, String> mValidateCreditCardErrorCodes = new HashMap<String, String>();
	
	/**
	 * Holds the mSoftAllocationNotAllowedList.
	 */
	private String mSoftAllocationNotAllowedList;
	
	
	/**
	 * Holds the PaymentProcessorTimeout.
	 */
	private String mPaymentProcessorTimeout;
	
	
	/** The m credit card processor. */
	private TRUCreditCardPaymentProcessor mCreditCardProcessor;
	
	/**
	 * @return the paymentProcessorTimeout
	 */
	public String getPaymentProcessorTimeout() {
		return mPaymentProcessorTimeout;
	}

	/**
	 * @param pPaymentProcessorTimeout the paymentProcessorTimeout to set
	 */
	public void setPaymentProcessorTimeout(String pPaymentProcessorTimeout) {
		mPaymentProcessorTimeout = pPaymentProcessorTimeout;
	}

	/**
	 * @return the softAllocationNotAllowedList
	 */
	public String getSoftAllocationNotAllowedList() {
		return mSoftAllocationNotAllowedList;
	}

	/**
	 * @param pSoftAllocationNotAllowedList the softAllocationNotAllowedList to set
	 */
	public void setSoftAllocationNotAllowedList(String pSoftAllocationNotAllowedList) {
		mSoftAllocationNotAllowedList = pSoftAllocationNotAllowedList;
	}

	/**
	 * @return the balanceInquiryResponseCodes
	 */
	public Map<String, String> getBalanceInquiryResponseCodes() {
		return mBalanceInquiryResponseCodes;
	}

	/**
	 * @param pBalanceInquiryResponseCodes the balanceInquiryResponseCodes to set
	 */
	public void setBalanceInquiryResponseCodes(
			Map<String, String> pBalanceInquiryResponseCodes) {
		mBalanceInquiryResponseCodes = pBalanceInquiryResponseCodes;
	}

	/**
	 * @return the redeemVoidResponseCodes
	 */
	public Map<String, String> getRedeemVoidResponseCodes() {
		return mRedeemVoidResponseCodes;
	}

	/**
	 * @param pRedeemVoidResponseCodes the redeemVoidResponseCodes to set
	 */
	public void setRedeemVoidResponseCodes(
			Map<String, String> pRedeemVoidResponseCodes) {
		mRedeemVoidResponseCodes = pRedeemVoidResponseCodes;
	}

	/**
	 * @return the failureGiftCardRedeemResponseCodes
	 */
	public Map<String, String> getFailureGiftCardRedeemResponseCodes() {
		return mFailureGiftCardRedeemResponseCodes;
	}

	/**
	 * @param pFailureGiftCardRedeemResponseCodes the failureGiftCardRedeemResponseCodes to set
	 */
	public void setFailureGiftCardRedeemResponseCodes(
			Map<String, String> pFailureGiftCardRedeemResponseCodes) {
		mFailureGiftCardRedeemResponseCodes = pFailureGiftCardRedeemResponseCodes;
	}

	/**
	 * @return the giftCardRespSuccessCodes
	 */
	public List<String> getGiftCardRespSuccessCodes() {
		return mGiftCardRespSuccessCodes;
	}

	/**
	 * @param pGiftCardRespSuccessCodes the giftCardRespSuccessCodes to set
	 */
	public void setGiftCardRespSuccessCodes(List<String> pGiftCardRespSuccessCodes) {
		mGiftCardRespSuccessCodes = pGiftCardRespSuccessCodes;
	}

	/**
	 * @return the creditCardRespAutoRetrySuccessCodes
	 */
	public List<String> getCreditCardRespAutoRetrySuccessCodes() {
		return mCreditCardRespAutoRetrySuccessCodes;
	}

	/**
	 * @param pCreditCardRespAutoRetrySuccessCodes the creditCardRespAutoRetrySuccessCodes to set
	 */
	public void setCreditCardRespAutoRetrySuccessCodes(
			List<String> pCreditCardRespAutoRetrySuccessCodes) {
		mCreditCardRespAutoRetrySuccessCodes = pCreditCardRespAutoRetrySuccessCodes;
	}

	/**
	 * @return the creditCardRespManualRetrySuccessCodes
	 */
	public List<String> getCreditCardRespManualRetrySuccessCodes() {
		return mCreditCardRespManualRetrySuccessCodes;
	}

	/**
	 * @param pCreditCardRespManualRetrySuccessCodes the creditCardRespManualRetrySuccessCodes to set
	 */
	public void setCreditCardRespManualRetrySuccessCodes(
			List<String> pCreditCardRespManualRetrySuccessCodes) {
		mCreditCardRespManualRetrySuccessCodes = pCreditCardRespManualRetrySuccessCodes;
	}

	/**
	 * @return the failureResponseCodes
	 */
	public Map<String, String> getFailureResponseCodes() {
		return mFailureResponseCodes;
	}

	/**
	 * @param pFailureResponseCodes the failureResponseCodes to set
	 */
	public void setFailureResponseCodes(Map<String, String> pFailureResponseCodes) {
		mFailureResponseCodes = pFailureResponseCodes;
	}

	/**
	 * @return the creditCardRespSuccessCodes
	 */
	public List<String> getCreditCardRespSuccessCodes() {
		return mCreditCardRespSuccessCodes;
	}

	/**
	 * @param pCreditCardRespSuccessCodes the creditCardRespSuccessCodes to set
	 */
	public void setCreditCardRespSuccessCodes(
			List<String> pCreditCardRespSuccessCodes) {
		mCreditCardRespSuccessCodes = pCreditCardRespSuccessCodes;
	}

	/**
	 * Holds the UidGenerator.
	 */
	private TRUUIDGenerator   mUidGenerator;
	
	/** Property Holds mSiteContextManager. */
	private SiteContextManager mSiteContextManager;
	
	/**
	 * Returns Site Context Manager.
	 *
	 * @return the SiteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * sets Site Context Manager.
	 *
	 * @param pSiteContextManager the SiteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}
	
	/**
	 * @return the uidGenerator
	 */
	public TRUUIDGenerator getUidGenerator() {
		return mUidGenerator;
	}

	/**
	 * @param pUidGenerator the uidGenerator to set
	 */
	public void setUidGenerator(TRUUIDGenerator pUidGenerator) {
		mUidGenerator = pUidGenerator;
	}

	/**
	 * @return the radialProcessor
	 */
	public TRURadialPaymentProcessor getRadialProcessor() {
		return mRadialProcessor;
	}

	/**
	 * @param pRadialProcessor
	 *            the radialProcessor to set
	 */
	public void setRadialProcessor(TRURadialPaymentProcessor pRadialProcessor) {
		mRadialProcessor = pRadialProcessor;
	}

	/**
	 * @return the idGenerator
	 */
	public SQLIdGenerator getIdGenerator() {
		return mIdGenerator;
	}

	/**
	 * @param pIdGenerator
	 *            the idGenerator to set
	 */
	public void setIdGenerator(SQLIdGenerator pIdGenerator) {
		mIdGenerator = pIdGenerator;
	}

	/**
	 * @return the dummyPhoneNumber
	 */
	public String getDummyPhoneNumber() {
		return mDummyPhoneNumber;
	}

	/**
	 * @param pDummyPhoneNumber
	 *            the dummyPhoneNumber to set
	 */
	public void setDummyPhoneNumber(String pDummyPhoneNumber) {
		mDummyPhoneNumber = pDummyPhoneNumber;
	}

	/**
	 * @return the tranStatusForRefundCall
	 */
	public List<String> getTranStatusForRefundCall() {
		return mTranStatusForRefundCall;
	}

	/**
	 * @param pTranStatusForRefundCall
	 *            the tranStatusForRefundCall to set
	 */
	public void setTranStatusForRefundCall(List<String> pTranStatusForRefundCall) {
		mTranStatusForRefundCall = pTranStatusForRefundCall;
	}

	/**
	 * @return the orderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * @param pOrderManager
	 *            the orderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}
	
	/**
	 * @return the validateCreditCardSuccessCodes
	 */
	public List<String> getValidateCreditCardSuccessCodes() {
		return mValidateCreditCardSuccessCodes;
	}

	/**
	 * @param pValidateCreditCardSuccessCodes
	 *            the mValidateCreditCardSuccessCodes to set
	 */
	public void setValidateCreditCardSuccessCodes(
			List<String> pValidateCreditCardSuccessCodes) {
		this.mValidateCreditCardSuccessCodes = pValidateCreditCardSuccessCodes;
	}

	/**
	 * @return the validateCreditCardRetryCodes
	 */
	public List<String> getValidateCreditCardRetryCodes() {
		return mValidateCreditCardRetryCodes;
	}

	/**
	 * @param pValidateCreditCardRetryCodes
	 *            the mValidateCreditCardRetryCodes to set
	 */
	public void setValidateCreditCardRetryCodes(
			List<String> pValidateCreditCardRetryCodes) {
		this.mValidateCreditCardRetryCodes = pValidateCreditCardRetryCodes;
	}
	
	/**
	 * @return the validateCreditCardRetryErrorKey
	 */
	public String getValidateCreditCardRetryErrorKey() {
		return mValidateCreditCardRetryErrorKey;
	}

	/**
	 * @param pValidateCreditCardRetryErrorKey
	 *            the mValidateCreditCardRetryErrorKey to set
	 */
	public void setValidateCreditCardRetryErrorKey(
			String pValidateCreditCardRetryErrorKey) {
		this.mValidateCreditCardRetryErrorKey = pValidateCreditCardRetryErrorKey;
	}

	/**
	 * @return the validateCreditCardErrorCodes
	 */
	public Map<String, String> getValidateCreditCardErrorCodes() {
		return mValidateCreditCardErrorCodes;
	}

	/**
	 * @param pValidateCreditCardErrorCodes
	 *            the mValidateCreditCardErrorCodes to set
	 */
	public void setValidateCreditCardErrorCodes(
			Map<String, String> pValidateCreditCardErrorCodes) {
		this.mValidateCreditCardErrorCodes = pValidateCreditCardErrorCodes;
	}

	/**
	 * This method returns gift card balance from payeezy.
	 * 
	 * @param pGiftCardInfo
	 *            -- holds gift card information such as card number, pin etc
	 * @return TRUGiftCardStatus -- updated gift card info with balance
	 * @throws TRUPaymentIntegrationException 
	 * @throws TRURadialIntegrationException 
	 */
	public TRUGiftCardStatus getGiftCardbalance(TRUGiftCardInfo pGiftCardInfo) throws TRURadialIntegrationException {
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (getGiftCardBalanceInquire) : BEGIN");
			vlogDebug(" GiftCardInfo :{0} ", pGiftCardInfo);
		}
		TRUGiftCardStatus giftCardStatus = new TRUGiftCardStatus();
		if (pGiftCardInfo != null) {
			// Call balanceInquiry Service with giftcardInfo and giftcardStatus
			getGiftCardProcessor().giftCardBalanceInquiry(pGiftCardInfo, giftCardStatus);
			String responseCode = giftCardStatus.getResponseCode();
			if(responseCode != null){
				responseCode = responseCode.toLowerCase();
			}
			if (isLoggingDebug()) {
				logDebug("giftCardStatus.getResponseCode() " + giftCardStatus.getResponseCode());
			}
			if(responseCode != null){
				if (getGiftCardRespSuccessCodes().contains(responseCode)) {
					giftCardStatus.setTransactionSuccess(true);
					giftCardStatus.setTransactionTimestamp(new Date());
				} else {
					giftCardStatus.setErrorMessage(getBalanceInquiryResponseCodes().get(responseCode));
					giftCardStatus.setTransactionTimestamp(new java.util.Date());
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (getGiftCardBalanceInquiry) : End");
		}
		return giftCardStatus;
	}

	/**
	 * @param pCreditCardInfo
	 *            creditCardInfo
	 * @param pCreditCardStatus
	 *            creditCardStatus
	 * @throws PaymentIntegrationException
	 *             PaymentIntegrationException
	 */
	public void fullAuthorization(TRUCreditCardInfo pCreditCardInfo,TRUCreditCardStatus pCreditCardStatus){
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (fullAuthorization) : BEGIN");
			vlogDebug(" CreditCardInfo :{0} ", pCreditCardInfo);
		}
		try {
			getCreditCardProcessor().cardAuthorization(pCreditCardInfo, pCreditCardStatus);
		} catch (TRURadialIntegrationException exe) {
			if(isLoggingError()){
				vlogError("Exception while calling/Processing the fullAuthorization() Exp: {0} ",exe);
			}
		}
		//getRadialProcessor().creditCardAuthorization(pCreditCardInfo, pCreditCardStatus);
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (fullAuthorization) : End");
		}
	}

	/**
	 * @param pCreditCardInfo
	 *            creditCardInfo
	 * @param pCreditCardStatus
	 *            creditCardStatus
	 * @throws PaymentIntegrationException
	 *             PaymentIntegrationException
	 */
	public void fullAuth3DSecure(TRUCreditCardInfo pCreditCardInfo,
			TRUCreditCardStatus pCreditCardStatus)
			throws PaymentIntegrationException {
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (fullAuthorization) : BEGIN");
			vlogDebug(" CreditCardInfo :{0} ", pCreditCardInfo);
		}
		//getPaymentProcessor().fullAuth3DSecure(pCreditCardInfo,
		//		pCreditCardStatus);
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (fullAuthorization) : End");
		}
	}

	/**
	 * @param pCreditCardInfo
	 *            TRUCreditCardInfo
	 * @param pCreditCardStatus
	 *            TRUCreditCardStatus
	 * @throws PaymentIntegrationException
	 *             PaymentIntegrationException
	 */
	public void zeroAuthorization(TRUCreditCardInfo pCreditCardInfo,
			TRUCreditCardStatus pCreditCardStatus)
			throws TRUPaymentIntegrationException {
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (zeroAuthorization) : BEGIN");
			vlogDebug(" CreditCardInfo :{0} ", pCreditCardInfo);
		}

		//getRadialProcessor().validateCreditCard(pCreditCardInfo, pCreditCardStatus);
		try {
			getCreditCardProcessor().validateCreditCard(pCreditCardInfo, pCreditCardStatus);
		} catch (TRURadialIntegrationException exe) {
			throw new TRUPaymentIntegrationException(exe);
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (zeroAuthorization) : BEGIN");
		}
	}

	/**
	 * @param pCreditCardInfo
	 *            TRUCreditCardInfo
	 * @param pCreditCardStatus
	 *            TRUCreditCardStatus
	 * @throws PaymentIntegrationException
	 *             PaymentIntegrationException
	 */
	public void preAuthorization(TRUCreditCardInfo pCreditCardInfo,
			TRUCreditCardStatus pCreditCardStatus)
			throws PaymentIntegrationException {
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (preAuthorization) : BEGIN");
			vlogDebug(" CreditCardInfo :{0} ", pCreditCardInfo);
		}

		/*getPaymentProcessor().preAuthorization(pCreditCardInfo,
				pCreditCardStatus);*/

		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (zeroAuthorization) : BEGIN");
		}
	}

	/**
	 * @param pCreditCardInfo
	 *            TRUCreditCardInfo
	 * @param pCreditCardStatus
	 *            TRUCreditCardStatus
	 * @throws PaymentIntegrationException
	 *             PaymentIntegrationException
	 */
	public void voidFullAuthorization(TRUCreditCardInfo pCreditCardInfo,
			TRUCreditCardStatus pCreditCardStatus)
			throws PaymentIntegrationException {
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (zeroAuthorization) : BEGIN");
			vlogDebug(" CreditCardInfo :{0} ", pCreditCardInfo);
		}

/*		getPaymentProcessor().voidFullAuthorization(pCreditCardInfo,pCreditCardStatus);
*/
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (zeroAuthorization) : BEGIN");
		}
	}

	/**
	 * Method wil give current time in YYYY/MM/DD HH:MI:SS format.
	 * 
	 * @param pCurrentDate
	 *            Date
	 * @return currentDate of String
	 */
	public String getCurrentTimeStamp(Date pCurrentDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				TRUCommerceConstants.PAYPAL_DATE_FORMAT, Locale.US);
		String currentDate = dateFormat.format(pCurrentDate);
		return currentDate;
	}

	/**
	 * @return the shippingMethodCodesMap
	 */
	public Map<String, String> getShippingMethodCodesMap() {
		return mShippingMethodCodesMap;
	}

	/**
	 * @param pShippingMethodCodesMap
	 *            the shippingMethodCodesMap to set
	 */
	public void setShippingMethodCodesMap(
			Map<String, String> pShippingMethodCodesMap) {
		mShippingMethodCodesMap = pShippingMethodCodesMap;
	}

	/**
	 * Method generates the Random number for the Reversal id.
	 * 
	 * @param pOrderId
	 *            the order
	 * @return String
	 * @throws PaymentIntegrationException
	 *             - PaymentIntegrationException
	 */
	public String getReversalId(String pOrderId)
			throws PaymentIntegrationException {
		if (isLoggingDebug()) {
			logDebug("TRUPaymentManager  (getReversalId) : BEGIN");
		}
		StringBuffer reversalId = new StringBuffer();
		reversalId.append(pOrderId);
		reversalId.append(TRUConstants.UNDER_SCORE);
		/*
		 * try { reversalId.append(getIdGenerator().generateStringId(
		 * getOrderManager().getPropertyManager().getReversalIdPropertyName()));
		 * } catch (IdGeneratorException e) { if (isLoggingError()) {
		 * logError("Unable to get UniquePaymentId from IdGenerator.",e); } }
		 */
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				Constants.TIMESTAMP_FORMAT_FOR_IMPORT, Locale.getDefault());
		String dateString = simpleDateFormat.format(Calendar.getInstance()
				.getTime());
		reversalId.append(dateString).toString();
		if (isLoggingDebug()) {
			logDebug("TRUPaymentManager  (getReversalId) : END");
		}
		return reversalId.toString();
	}

	/**
	 * This method provides the marshaller object.
	 *
	 * @param pJAXBObject the jAXB object
	 * @param pPackage the package
	 * @return String
	 */
	public String marshalXML(Object pJAXBObject, String pPackage) {
		if (isLoggingDebug()) {
			logDebug("TRUPaymentTools  (marshalXML) : Start");
		}
		String xmlRequest = TRUCheckoutConstants.EMPTY_STRING;
		if (pJAXBObject == null) {
			return xmlRequest;
		}
		StringWriter sw = new StringWriter();
		try {
			JAXBContext context = JAXBContext.newInstance(pPackage);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(TRUCheckoutConstants.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.marshal(pJAXBObject, sw);
			xmlRequest = sw.toString();
			sw.close();
		} catch (JAXBException jaxbExp) {
			vlogError(
					"Exception while calling/Processing the transact() Exp: {0} ",
					jaxbExp);
		} catch (IOException ioExp) {
			vlogError(
					"Exception while calling/Processing the transact() Exp: {0} ",
					ioExp);
		}
		if (isLoggingDebug()) {
			logDebug("TRUPaymentTools  (marshalXML) : END");
		}
		return xmlRequest;
	}

	/**
	 * This method provides the unmarshaller object.
	 *
	 * @param pServiceResponse the service response
	 * @param pPackage the package
	 * @return Object
	 */
	public Object unMarshalXML(String pServiceResponse, String pPackage) {
		if (isLoggingDebug()) {
			logDebug("TRUPaymentTools  (unMarshalXML) : Start");
		}
		Object obj = null;
		InputStream inputStream = null;
		try {
			StringReader stringReader = new StringReader(pServiceResponse);
            JAXBContext jaxbContext = JAXBContext.newInstance(pPackage);
            XMLInputFactory xif = XMLInputFactory.newInstance();
            XMLStreamReader xsr = xif.createXMLStreamReader(stringReader);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            obj =  unmarshaller.unmarshal(xsr);
			return obj;
		} catch (JAXBException | XMLStreamException exp) {
			vlogError("Exception while calling/Processing the transact() Exp: {0} ",exp);
		} 
		finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException ioExp) {
					vlogError("Exception while calling/Processing the transact() Exp: {0} ",ioExp);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUPaymentTools  (unMarshalXML) : END");
		}
		return obj;
	}
	
	/**
	 * @param pGiftCardInfo
	 *            giftCardInfo
	 * @param pGiftCardStatus
	 *            giftCardStatus
	 * @throws PaymentIntegrationException
	 *             PaymentIntegrationException
	 * @throws TRUPaymentIntegrationException 
	 */
	public void giftCardRedeem(TRUGiftCardInfo pGiftCardInfo,TRUGiftCardStatus pGiftCardStatus)
			throws TRURadialIntegrationException {
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (giftCardRedeem) : BEGIN");
			vlogDebug(" TRUGiftCardInfo :{0} ", pGiftCardInfo);
		}
		long unid = getUidGenerator().getPaymentId(); 
		pGiftCardInfo.setTransactionId(String.valueOf(unid));
		//getRadialProcessor().giftCardRedeem(pGiftCardInfo,pGiftCardStatus);
		getGiftCardProcessor().giftCardRedeem(pGiftCardInfo,pGiftCardStatus);
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (giftCardRedeem) : End");
		}
	}
	
	/**
	 * Gift card void redeem.
	 *
	 * @param pGiftCardInfo the gift card info
	 * @param pGiftCardStatus the gift card status
	 * @throws TRUPaymentIntegrationException the tRU payment integration exception
	 */
	public void giftCardVoidRedeem(TRUGiftCardInfo pGiftCardInfo,TRUGiftCardStatus pGiftCardStatus) throws TRURadialIntegrationException{
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (zeroAuthorization) : BEGIN");
			vlogDebug(" CreditCardInfo :{0} ", pGiftCardInfo);
		}
		getGiftCardProcessor().giftCardVoidRedeem(pGiftCardInfo,pGiftCardStatus);
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (zeroAuthorization) : BEGIN");
		}
	}
	
	/**
	 * @param pCreditCardInfo
	 *            pCreditCardInfo
	 * @param pCreditCardStatus
	 *            pCreditCardStatus
	 * @throws PaymentIntegrationException
	 *             PaymentIntegrationException
	 */
	public void promoFinancingAuthorization(TRUCreditCardInfo pCreditCardInfo,TRUCreditCardStatus pCreditCardStatus) throws TRUPaymentIntegrationException {
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (fullAuthorization) : BEGIN");
			vlogDebug(" CreditCardInfo :{0} ");
		}
		getRadialProcessor().promoFinanceAuthorization(pCreditCardInfo, pCreditCardStatus);
		if (isLoggingDebug()) {
			vlogDebug("TRUPaymentTools  (fullAuthorization) : End");
		}
	}
	
	/** The m gift card processor. */
	private TRUGiftCardPaymentProcessor mGiftCardProcessor;

	/**
	 * Gets the gift card processor.
	 *
	 * @return the mGiftCardProcessor
	 */
	public TRUGiftCardPaymentProcessor getGiftCardProcessor() {
		return mGiftCardProcessor;
	}

	/**
	 * Sets the gift card processor.
	 *
	 * @param pGiftCardProcessor the mGiftCardProcessor to set
	 */
	public void setGiftCardProcessor(TRUGiftCardPaymentProcessor pGiftCardProcessor) {
		this.mGiftCardProcessor = pGiftCardProcessor;
	}

	/**
	 * Gets the credit card processor.
	 *
	 * @return the creditCardProcessor
	 */
	public TRUCreditCardPaymentProcessor getCreditCardProcessor() {
		return mCreditCardProcessor;
	}

	/**
	 * Sets the credit card processor.
	 *
	 * @param pCreditCardProcessor the creditCardProcessor to set
	 */
	public void setCreditCardProcessor(
			TRUCreditCardPaymentProcessor pCreditCardProcessor) {
		mCreditCardProcessor = pCreditCardProcessor;
	}
	
	
	/** The Credit card resp synchrony accept codes. */
	private List<String> mCreditCardRespSynchronyAcceptCodes;
	
	/** The Synchrony accept cvv. */
	private String mSynchronyAcceptCVV;

	/**
	 * @return the creditCardRespSynchronyAcceptCodes
	 */
	public List<String> getCreditCardRespSynchronyAcceptCodes() {
		return mCreditCardRespSynchronyAcceptCodes;
	}

	/**
	 * @param pCreditCardRespSynchronyAcceptCodes the creditCardRespSynchronyAcceptCodes to set
	 */
	public void setCreditCardRespSynchronyAcceptCodes(
			List<String> pCreditCardRespSynchronyAcceptCodes) {
		mCreditCardRespSynchronyAcceptCodes = pCreditCardRespSynchronyAcceptCodes;
	}
	
	/**
	 * @return the synchronyAcceptCVV
	 */
	public String getSynchronyAcceptCVV() {
		return mSynchronyAcceptCVV;
	}

	/**
	 * @param pSynchronyAcceptCVV the synchronyAcceptCVV to set
	 */
	public void setSynchronyAcceptCVV(String pSynchronyAcceptCVV) {
		mSynchronyAcceptCVV = pSynchronyAcceptCVV;
	}

	/** The Synchrony card types. */
	private List<String> mSynchronyCardTypes;

	/**
	 * @return the synchronyCardTypes
	 */
	public List<String> getSynchronyCardTypes() {
		return mSynchronyCardTypes;
	}

	/**
	 * @param pSynchronyCardTypes the synchronyCardTypes to set
	 */
	public void setSynchronyCardTypes(List<String> pSynchronyCardTypes) {
		mSynchronyCardTypes = pSynchronyCardTypes;
	}
}
