package com.tru.commerce.order.payment.giftcard;

import java.math.BigDecimal;

import com.tru.commerce.order.payment.TRUPaymentStatus;

/**
 * This class  defines gift card payment status.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUGiftCardStatus extends TRUPaymentStatus {
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/**
	 * holds the service returned balance amount.
	 */
	private BigDecimal mBalanceAmount;
	
	/**
	 * holds the previous balance amount.
	 */
	private BigDecimal mPreviousBalance;
	
	/**
	 * Sets the balance amount.
	 *
	 * @param pBalanceAmount  the mBalanceAmount to set
	 */
	public void setBalanceAmount(BigDecimal pBalanceAmount) {
		this.mBalanceAmount = pBalanceAmount;
	}

	/**
	 * Gets the balance amount.
	 *
	 * @return the mBalanceAmount
	 */
	public BigDecimal getBalanceAmount() {
		return mBalanceAmount;
	}
	
	/**
	 * Gets the previous balance.
	 *
	 * @return the mPreviousBalance
	 */
	public BigDecimal getPreviousBalance() {
		return mPreviousBalance;
	}

	/**
	 * Sets the previous balance.
	 *
	 * @param pPreviousBalance  the mPreviousBalance to set
	 */
	public void setPreviousBalance(BigDecimal pPreviousBalance) {
		this.mPreviousBalance = pPreviousBalance;
	}
}