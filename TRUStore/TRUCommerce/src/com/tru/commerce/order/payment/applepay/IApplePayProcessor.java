/**
 * 
 */
package com.tru.commerce.order.payment.applepay;

import com.tru.commerce.payment.applepay.TRUApplePayStatus;

/**
 * The Interface IApplePayProcessor.
 *
 * @author Siva, Gopishetti {eclipse.ini}
 */
public interface IApplePayProcessor {
	

	  /**
  	 * Authorize.
  	 *
  	 * @param pApplePayInfo the apple pay info
  	 * @return the TRU apple pay status
  	 */
  	TRUApplePayStatus authorize(TRUApplePayInfo pApplePayInfo);

	  /**
  	 * Reverse authorization.
  	 *
  	 * @param pApplePayInfo the apple pay info
  	 * @param pApplePayStatus the apple pay status
  	 * @return the TRU apple pay status
  	 */
  	TRUApplePayStatus reverseAuthorization(TRUApplePayInfo pApplePayInfo, TRUApplePayStatus pApplePayStatus );

}
