package com.tru.commerce.order.payment.giftcard;

import atg.commerce.order.Order;

import com.tru.commerce.payment.PaymentInfo;

/**
 * This class is a bean class whose job is to store the GiftCardInfo.
 * 
 * @author Oracle
 * @version 1.0
 */
public class TRUGiftCardInfo implements PaymentInfo {

	/**
	 * This Property holds the mCardHolderName.
	 */
	private String mCardHolderName;
	
	/**
	 * This Property holds the mOrder.
	 */
	private Order mOrder;

	/**
	 * This Property holds the mFraudGiftCardId.
	 */
	private String mFraudGiftCardId;

	/**
	 * This Property holds the currencyCode.
	 */
	private String mCurrencyCode;
	/**
	 * Property to hold CardType.
	 */
	// private String mCardType;

	/**
	 * Property to hold TranType.
	 */
	private String mTranType;

	/**
	 * Property to hold GiftCardNumber.
	 */
	private String mGiftCardNumber;

	/**
	 * Property to denote the pin number.
	 */
	private String mPin;
	/**
	 * Property to denote the GiftCard Amount.
	 */
	private double mAmount;

	/** The m order id. */
	private String mOrderId;

	/**
	 * Property to denote Profile Id.
	 */
	private String mProfileId;

	/**
	 * Property to hold mPaymentId.
	 */
	private String mPaymentId;

	/**
	 * Holds the balance amount.
	 */
	private double mBalanceAmt;

	/**
	 * Property to denote the TransactionId.
	 */
	private String mTransactionId;
	/**
	 * Property to denote the TransactionTag.
	 */
	private String mTransactionTag;

	/**
	 * Property to denote the merchantRefNumber.
	 */
	private String mMerchantRefNumber;

	/**
	 * Gets the order.
	 *
	 * @return the order
	 */
	public Order getOrder() {
		return this.mOrder;
	}

	/**
	 * Sets the order.
	 *
	 * @param pOrder   the order to set.
	 */
	public void setOrder(Order pOrder) {
		this.mOrder = pOrder;
	}

	/**
	 * Gets the fraud gift card id.
	 *
	 * @return the mFraudGiftCardId
	 */
	public String getFraudGiftCardId() {
		return mFraudGiftCardId;
	}

	/**
	 * Sets the fraud gift card id.
	 *
	 * @param pFraudGiftCardId   the mFraudGiftCardId to set
	 */
	public void setFraudGiftCardId(String pFraudGiftCardId) {
		mFraudGiftCardId = pFraudGiftCardId;
	}

	/**
	 * Gets the card holder name.
	 *
	 * @return the cardHolderName
	 */
	public String getCardHolderName() {
		return mCardHolderName;
	}

	/**
	 * Sets the card holder name.
	 *
	 * @param pCardHolderName   the cardHolderName to set
	 */
	public void setCardHolderName(String pCardHolderName) {
		this.mCardHolderName = pCardHolderName;
	}

	
	/**
	 * Gets the balance amt.
	 *
	 * @return the balanceAmt
	 */
	public double getBalanceAmt() {
		return mBalanceAmt;
	}

	/**
	 * Sets the balance amt.
	 *
	 * @param pBalanceAmt   the balanceAmt to set
	 */
	public void setBalanceAmt(double pBalanceAmt) {
		mBalanceAmt = pBalanceAmt;
	}

	/**
	 * Gets the GiftCardNumber.
	 * 
	 * @return the mGiftCardNumber
	 */
	public String getGiftCardNumber() {
		return mGiftCardNumber;
	}

	/**
	 * Sets the GiftCardNumber.
	 * 
	 * @param pGiftCardNumber the mGiftCardNumber to set
	 */
	public void setGiftCardNumber(String pGiftCardNumber) {
		this.mGiftCardNumber = pGiftCardNumber;
	}

	/**
	 * Sets the pin.
	 *
	 * @param pPin   the mPin to set
	 */
	public void setPin(String pPin) {
		this.mPin = pPin;
	}

	/**
	 * Gets the pin.
	 *
	 * @return the mPin
	 */
	public String getPin() {
		return mPin;
	}

	/**
	 * Sets the amount.
	 *
	 * @param pAmount the mAmount to set
	 */
	public void setAmount(double pAmount) {
		this.mAmount = pAmount;
	}

	/**
	 * Gets the amount.
	 *
	 * @return the mAmount
	 */
	public double getAmount() {
		return mAmount;
	}

	/**
	 * Sets the profile id.
	 *
	 * @param pProfileId   the mProfileId to set
	 */
	public void setProfileId(String pProfileId) {
		this.mProfileId = pProfileId;
	}

	/**
	 * Gets the profile id.
	 *
	 * @return the mProfileId
	 */
	public String getProfileId() {
		return mProfileId;
	}

	/**
	 * Sets the payment id.
	 *
	 * @param pPaymentId   the mPaymentId to set
	 */
	public void setPaymentId(String pPaymentId) {
		this.mPaymentId = pPaymentId;
	}

	/**
	 * Gets the payment id.
	 *
	 * @return the mPaymentId
	 */
	public String getPaymentId() {
		return mPaymentId;
	}

	/**
	 * Sets the order id.
	 *
	 * @param pOrderId   the mOrderId to set
	 */
	public void setOrderId(String pOrderId) {
		this.mOrderId = pOrderId;
	}

	/**
	 * Gets the order id.
	 *
	 * @return the mOrderId
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * Gets the tran type.
	 *
	 * @return the tranType
	 */

	public String getTranType() {
		return mTranType;
	}

	/**
	 * Sets value to mTranType property.
	 * 
	 * @param pTranType  the tranType to set
	 */

	public void setTranType(String pTranType) {
		this.mTranType = pTranType;
	}

	/**
	 * Gets the transaction id.
	 *
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return mTransactionId;
	}

	/**
	 * Sets the transaction id.
	 *
	 * @param pTransactionId   the transactionId to set
	 */
	public void setTransactionId(String pTransactionId) {
		mTransactionId = pTransactionId;
	}

	/**
	 * Gets the transaction tag.
	 *
	 * @return the transactionTag
	 */
	public String getTransactionTag() {
		return mTransactionTag;
	}

	/**
	 * Sets the transaction tag.
	 *
	 * @param pTransactionTag   the transactionTag to set
	 */
	public void setTransactionTag(String pTransactionTag) {
		mTransactionTag = pTransactionTag;
	}

	/**
	 * Gets the currency code.
	 *
	 * @return the mCurrencyCode
	 */
	public String getCurrencyCode() {
		return mCurrencyCode;
	}

	/**
	 * Sets the currency code.
	 *
	 * @param pCurrencyCode   the mCurrencyCode to set
	 */
	public void setCurrencyCode(String pCurrencyCode) {
		this.mCurrencyCode = pCurrencyCode;
	}

	/**
	 * Gets the merchant ref number.
	 *
	 * @return the merchantRefNumber
	 */
	public String getMerchantRefNumber() {
		return mMerchantRefNumber;
	}

	/**
	 * Sets the merchant ref number.
	 *
	 * @param pMerchantRefNumber   the merchantRefNumber to set
	 */
	public void setMerchantRefNumber(String pMerchantRefNumber) {
		mMerchantRefNumber = pMerchantRefNumber;
	}

}
