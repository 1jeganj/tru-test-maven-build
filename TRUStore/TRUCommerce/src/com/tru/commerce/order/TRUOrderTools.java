package com.tru.commerce.order;

import java.beans.IntrospectionException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import atg.beans.DynamicBeanInfo;
import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.order.ChangedProperties;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.CreditCard;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.ObjectCreationException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.Relationship;
import atg.commerce.order.RelationshipNotFoundException;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.pricing.DetailedItemPriceInfo;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.PricingAdjustment;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.payment.creditcard.GenericCreditCardInfo;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.vo.TRUStorePickUpInfo;
import com.tru.commerce.payment.TRUPaymentConstants;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.commerce.pricing.TRUItemPriceInfo;
import com.tru.commerce.pricing.TRUPricingModelProperties;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.utils.TRUValueComparator;

/**
 * The Class TRUOrderTools.
 * @author Professional Access
 * @version 1.0
 */
public class TRUOrderTools extends OrderTools {

	/** The Credit card info. */
	private GenericCreditCardInfo mCreditCardInfo;

	/**  Holds reference for TRUCommercePropertyManager. */
	private TRUCommercePropertyManager mCommercePropertyManager;

	/** property to hold meta info descriptor name. */
	private String mMetaInfoDescriptorName;
	
	/** property to hold mStoreConfigurationRepositroy. */
	private Repository mStoreConfigRepository;
	
	/**  tieredPriceBreakTemplate. */
	private String mTieredPriceBreakTemplate;
	
	/** The Bpp item info descriptor name. */
	private String mBppItemInfoDescriptorName;
	
	/** The Donation commerce item class type. */
	private String mDonationCommerceItemClassType;
	
	/**  mPricingModelProperties. */
	private TRUPricingModelProperties mPricingModelProperties;
	
	/** property to hold ChannelInStorePickupShippingGroupType. */
	private String mChannelInStorePickupShippingGroupType;
	
	/** property to hold ChannelHardgoodShippingGroupType. */
	private String mChannelHardgoodShippingGroupType;
	
	/** property to hold PricingTools. */
	private TRUPricingTools mPricingTools;
	
	/** property to hold InStorePickupShippingGroupType. */
	private String mInStorePickupShippingGroupType;
	
	/** The Ship item rel item descriptor name. */
	private String mShipItemRelItemDescriptorName;
	
	/** property to hold SiteContextManager. */
	private SiteContextManager mSiteContextManager;
	
	/** property to hold layawayOrderType. */
	private String mLayawayOrderType;
	
	/** The Configuration. */
	private TRUConfiguration mConfiguration;
	
	/** The Configuration.mPropertyManager. */
	private TRUPropertyManager mPropertyManager;
	
	/** The Configuration. mCatalogProperties. */
	private TRUCatalogProperties mCatalogProperties;
	/** The Configuration. groupDiscountTemplate. */
	private String mGroupDiscountTemplate;
	
	
	/** The Configuration bogo discount template. */
	private String mBOGODiscountTemplate;
	
	/**
	 * Gets the BOGO discount template.
	 *
	 * @return the BOGO discount template
	 */
	public String getBOGODiscountTemplate() {
		return mBOGODiscountTemplate;
	}
	
	/**
	 * Sets the BOGO discount template
	 *
	 * @param pBOGODiscountTemplate the shippingHelper to set
	 */
	public void setBOGODiscountTemplate(String pBOGODiscountTemplate) {
		this.mBOGODiscountTemplate = pBOGODiscountTemplate;
	}

	/**
	 * @return the configuration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * @param pConfiguration the configuration to set
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		mConfiguration = pConfiguration;
	}
	
	/**
	 * Saves the address to address book.
	 * 
	 * @param pProfile - profile
	 * @param pAddress - address
	 * @param pNickName - nickname
	 * @throws CommerceException- the commerce exception
	 */
	public void saveAddressToAddressBook(RepositoryItem pProfile, Address pAddress, String pNickName)
			throws CommerceException {
		copyAddressToAddressBook(pProfile, pAddress, pNickName);
	}

	/**
	 * Get the profile from the order, and update the address book with the
	 * shipping group address.
	 * 
	 * @param pProfile
	 *            - profile to copy address to.
	 * @param pAddress
	 *            -the address to copy.
	 * @param pNickname
	 *            -key for address book map.
	 * 
	 * @return true on success, otherwise false.
	 * 
	 * @throws CommerceException
	 *             if error with copy.
	 */
	public boolean copyAddressToAddressBook(RepositoryItem pProfile,
			Address pAddress, String pNickname) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::copyAddressToAddressBook() : BEGIN");
		}
		TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();

		if (profileTools.isDuplicateAddressNickName(pProfile, pNickname) && isLoggingDebug()) {
				vlogDebug("User tried to create address book entry with nickname that already exists");
		}

		try {
			profileTools.createProfileRepositorySecondaryAddress(pProfile, pNickname,
					pAddress);
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				vlogError("RepositorException occured in TRUOrderTools.copyAddressToAddressBook():{0}", re);
			}
			return false;
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::copyAddressToAddressBook() : END");
		}
		return true;
	}

	/**
	 * Validate credit card.
	 *
	 * @param pCreditCardInfoMap the credit card info map
	 * @return the int to indicate if credit card is valid or not
	 */
	@SuppressWarnings("rawtypes")
	public int validateCreditCard(Map pCreditCardInfoMap) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::validateCreditCard() : BEGIN");
		}
		GenericCreditCardInfo ccInfo = (GenericCreditCardInfo) getCreditCardInfo();
		TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();
		String ccType = TRUConstants.EMPTY;
		if(!StringUtils.isBlank((String) pCreditCardInfoMap.get(TRUConstants.CARD_TYPE)) &&
				!TRUConstants.CARD_TYPE_UNDEFINED.equalsIgnoreCase((String) pCreditCardInfoMap.get(TRUConstants.CARD_TYPE))){
						ccType = (String) pCreditCardInfoMap.get(TRUConstants.CARD_TYPE);
		} else {
		ccType = ((TRUProfileTools)getProfileTools()).getCreditCardTypeFromRepository
										((String) pCreditCardInfoMap.get(TRUConstants.CREDIT_CARD_NUMBER),(String) pCreditCardInfoMap
												.get(TRUConstants.CARD_LENGTH));
		}
		ccInfo.setCreditCardType(ccType);
		ccInfo.setCreditCardNumber((String) (pCreditCardInfoMap
				.get(TRUConstants.CREDIT_CARD_NUMBER)));
		ccInfo.setExpirationMonth((String) (pCreditCardInfoMap
				.get(TRUConstants.EXPIRATION_MONTH)));
		ccInfo.setExpirationDayOfMonth(TRUConstants.EMPTY_STRING);
		ccInfo.setExpirationYear((String) (pCreditCardInfoMap
				.get(TRUConstants.EXPIRATION_YEAR)));
		ccInfo.setSecurityCode((String) (pCreditCardInfoMap
				.get(TRUConstants.CREDIT_CARD_CVV)));

		int result = profileTools.getExtendableCreditCardTools()
				.verifyCreditCard(ccInfo);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::validateCreditCard() : END");
		}
		return result;
	}

	/**
	 * Gets the credit card type.
	 *
	 * @param pCreditCardNumber            the credit card number
	 * @return the credit card type
	 */
	@SuppressWarnings("rawtypes")
	public String getCreditCardType(String pCreditCardNumber) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::getCreditCardType() : BEGIN");
		}
		String creditCardType = null;
		TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();
		Enumeration e= profileTools.getExtendableCreditCardTools()
				.getCardPrefixesMap().propertyNames();
		while (e.hasMoreElements()) {
			String cardPrefix = (String) e.nextElement();
			if (pCreditCardNumber.startsWith(cardPrefix)) {
				creditCardType = profileTools.getExtendableCreditCardTools()
						.getCardPrefixesMap().getProperty(cardPrefix);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::getCreditCardType() : END");
		}
		return creditCardType;
	}
	
	

	/**
	 * Update credit card address.
	 *
	 * @param pAddressToBeCopied            - pAddressToBeCopied to copy from and add to order payment group 
	 * @param pCreditCard the credit card
	 * @throws CommerceException the commerce exception
	 */
	public void updateCreditCardAddress(ContactInfo pAddressToBeCopied,CreditCard pCreditCard) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::updateCreditCardAddress() : BEGIN");
		}
		//Setting the country code.
		if (StringUtils.isBlank(pAddressToBeCopied.getCountry())) {
			pAddressToBeCopied.setCountry(TRUConstants.UNITED_STATES);
		}
		ContactInfo billingAddress  = new ContactInfo();
		OrderTools.copyAddress(pAddressToBeCopied, billingAddress);
		//Adding the billing address to the credit card. 
		pCreditCard.setBillingAddress(billingAddress);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::updateCreditCardAddress() : END");
		}
	}
	
	/**
	 * This method will create/update the commerce item meta info.
	 *
	 * @param pShipItemRel            - {@link ShippingGroupCommerceItemRelationship}
	 * @param pSalePrice            - Sale price
	 * @param pQtyAdj            - Adjusted Quantity
	 * @param pShipItemRelQty            - ShipItemRelquantity
	 * @param pAdjustedAmount            - Adjusted amount
	 * @param pListPrice the list price
	 * @param pPriceQuote - Item price info
	 * @param pRelAndDiscountMap - Map
	 * @throws RepositoryException the repository exception
	 */
	@SuppressWarnings("unchecked")
	public void createOrUpdateCommItemMetaInfo(TRUShippingGroupCommerceItemRelationship pShipItemRel, double pSalePrice,
			long pQtyAdj, long pShipItemRelQty, double pAdjustedAmount , double pListPrice,
			TRUItemPriceInfo pPriceQuote, Map<String, String> pRelAndDiscountMap) throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("Enter into [Class: TRUPricingTools  method: createOrUpdateCommItemMetaInfo]");
			vlogDebug("ShippingGroupCommerceItemRelationship : {0} SalePrice : {1} QuantityAdjusted : {2}", pShipItemRel, pSalePrice, pQtyAdj);
		}
		MutableRepository orderRepository = (MutableRepository) getOrderRepository();
		double lAdjustedAmount = pAdjustedAmount;
		long lQtyAdj =pQtyAdj;
		TRUCommercePropertyManager commerceItemPropertyManager = getCommercePropertyManager();
		if (pShipItemRel == null) {
			return;
		}
		//First remove the existing metaInfo Items from SGCIR and order repository
		removeMetaInfo(pShipItemRel);
		Map<String, Long> gwItems = new HashMap<String, Long>();
		List<RepositoryItem> getGiftItemInfo = pShipItemRel.getGiftItemInfo();
		if (getGiftItemInfo != null && !getGiftItemInfo.isEmpty()) {
			for (RepositoryItem giftItem : getGiftItemInfo) {
				if (giftItem.getPropertyValue(getCommercePropertyManager().getGiftWrapCommItemId()) != null) {
					gwItems.put((String) giftItem.getPropertyValue(getCommercePropertyManager().getGiftWrapCommItemId()),
							(Long) giftItem.getPropertyValue(getCommercePropertyManager().getGiftWrapQuantity()));
				}
			}
		}
		Map<String, Long> final_Map = new HashMap<String, Long>();
		Map<String,Long> giftWrapItems = null;
		//Sorting the Gift wrap items by value
		if(gwItems!= null && !gwItems.isEmpty()){
			TRUValueComparator vc =  new TRUValueComparator(gwItems);
			giftWrapItems = new TreeMap<String,Long>(vc);
			giftWrapItems.putAll(gwItems);
		}
		List<TRUCommerceItemMetaInfo> metaInfoList = new ArrayList<TRUCommerceItemMetaInfo>();
		if(giftWrapItems != null && !giftWrapItems.isEmpty()){
			String giftWrapItemId = null;
			long giftWrapItemQty = TRUConstants.LONG_ZERO;
			long giftWrapItemQtyTotal = TRUConstants.LONG_ZERO;
			RepositoryItem giftWrapItem = null;
			for (Map.Entry<String,Long> entry : giftWrapItems.entrySet()) {
				//Sorted map
				final_Map.put(entry.getKey(), entry.getValue());
			}
			Set<String> giKeySet = final_Map.keySet();
			for (Iterator<String> iterator = giKeySet.iterator(); iterator.hasNext();) {
				giftWrapItemId = (String) iterator.next();
				giftWrapItemQtyTotal += final_Map.get(giftWrapItemId);
			}
			if(pPriceQuote.getDiscountedAmount() != TRUConstants.DOUBLE_ZERO && giftWrapItemQtyTotal != TRUConstants.LONG_ZERO){
				if(isLoggingDebug()){
					vlogDebug("metaInfoList : {0}", metaInfoList);
				}
				createMetaInfoforGiftwrap(metaInfoList, pPriceQuote,  pShipItemRel, final_Map, giftWrapItemQtyTotal);
				if(isLoggingDebug()){
					vlogDebug("metaInfoList : {0}", metaInfoList);
				}
			}else if(pShipItemRelQty >= giftWrapItemQtyTotal){ //if clause for ShipCIItem Relationship Qty >= giftWrapItems Total Qty
				if (isLoggingDebug()) {
					vlogDebug("Enters into Scenario 1 shipItemRelQty greater than GWrapItemQty pShipItemRel: {0} giftWrapItemQtyTotal : {1} lQtyAdj : {2}" ,pShipItemRel ,giftWrapItemQtyTotal,lQtyAdj);
				}
				if(lQtyAdj >= giftWrapItemQtyTotal){
					//T1 and B1 and 3QtyAdj 5TotalQty scenario
					//T2/B2 and 3QtyAdj 5TotalQty scenario
					if (isLoggingDebug()) {
						vlogDebug("Enters into Scenario 2 Qty adjusted >= GWropItemQty pShipItemRel: {0} giftWrapItemQtyTotal{1}" ,pShipItemRel,giftWrapItemQtyTotal);
					}
					long qty = TRUConstants.LONG_ZERO;
					for (Iterator<String> iterator = giKeySet.iterator(); iterator.hasNext();) {
						if (isLoggingDebug()) {
							vlogDebug("Enters into Scenario 3 for each GiftWrap item from the final_Map pShipItemRel: {0}  pSalePrice:{1}," ,pShipItemRel,pSalePrice);
						}
						giftWrapItemId = (String) iterator.next();
						giftWrapItemQty = final_Map.get(giftWrapItemId);
						giftWrapItem = orderRepository.getItem(giftWrapItemId, commerceItemPropertyManager.getCommerceItemPropertyName());
						metaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), giftWrapItemQty,pSalePrice * giftWrapItemQty + lAdjustedAmount*giftWrapItemQty,
								pListPrice*giftWrapItemQty, pSalePrice*giftWrapItemQty, pSalePrice, giftWrapItem, pShipItemRel.getId(), pPriceQuote, null));
					}
					//For Remaining promotional items
					qty = lQtyAdj - giftWrapItemQtyTotal;
					if(qty > TRUConstants.LONG_ZERO){
						if (isLoggingDebug()) {
							vlogDebug("Enters into Scenario 4  for Remaining promotional items pShipItemRel: {0} pSalePrice : {1} " ,pShipItemRel,pSalePrice);
						}
						metaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(),
								qty, pSalePrice * qty + lAdjustedAmount*qty,pListPrice * qty,pSalePrice*qty, pSalePrice, null, pShipItemRel.getId(), pPriceQuote, null));
					}
					//Remaining shipItemRel qty
					qty = pShipItemRelQty - lQtyAdj;
					if(qty > TRUConstants.LONG_ZERO){
						if (isLoggingDebug()) {
							vlogDebug("Enters into Scenario 5 for Remaining shipItemRel qty   pShipItemRel: {0}  pSalePrice : {1}" ,pShipItemRel,pSalePrice);
						}
						metaInfoList.add(createMetaInfoItem(Boolean.FALSE, pShipItemRel.getCommerceItem(),
								qty, pSalePrice * qty, pListPrice * qty,pSalePrice*qty, pSalePrice, null, pShipItemRel.getId(), pPriceQuote, null));
					}
				} else if(lQtyAdj < giftWrapItemQtyTotal){
					if (isLoggingDebug()) {
						vlogDebug("Enters into Scenario 6 where QtyAdjusted lessthan GWrapTotalQty pShipItemRel: {0} giftWrapItemQtyTotal :{1}" ,pShipItemRel,giftWrapItemQtyTotal);
					}
					//It has to be created with giftwrap
					long qty = giftWrapItemQtyTotal - lQtyAdj;
					long remGiftWrapItemQtyTotal = TRUConstants.LONG_ZERO;
					for (Iterator<String> iterator = giKeySet.iterator(); iterator.hasNext();) {
						giftWrapItemId = (String) iterator.next();
						giftWrapItemQty = final_Map.get(giftWrapItemId);
						giftWrapItem = orderRepository.getItem(giftWrapItemId, commerceItemPropertyManager.getCommerceItemPropertyName());
						if(giKeySet.size() > TRUTaxwareConstant.NUMBER_ONE && !iterator.hasNext()){
							qty = remGiftWrapItemQtyTotal;
							if(qty > TRUConstants.LONG_ZERO){
								if (isLoggingDebug()) {
									vlogDebug("Enters into Scenario 7 pShipItemRel: {0} pSalePrice:{1} " ,pShipItemRel , pSalePrice);
								}
								metaInfoList.add(createMetaInfoItem(Boolean.FALSE, pShipItemRel.getCommerceItem(), qty, pSalePrice * qty,pListPrice * qty,pSalePrice*qty, pSalePrice,
										giftWrapItem, pShipItemRel.getId(), pPriceQuote, null));
							}
						} else if(lQtyAdj == 0 && giftWrapItemQty > TRUConstants.LONG_ZERO){	
							if (isLoggingDebug()) {
								vlogDebug("Enters into Scenario 8 pShipItemRel: {0} pSalePrice : {1}" ,pShipItemRel,pSalePrice);
							}
							metaInfoList.add(createMetaInfoItem(Boolean.FALSE, pShipItemRel.getCommerceItem(), giftWrapItemQty, pSalePrice * giftWrapItemQty,pListPrice * giftWrapItemQty,
									pSalePrice*giftWrapItemQty, pSalePrice, giftWrapItem, pShipItemRel.getId(), pPriceQuote, null));
						} else if(giftWrapItemQty > TRUConstants.LONG_ZERO && lQtyAdj <= giftWrapItemQty){
							if (isLoggingDebug()) {
								vlogDebug("Enters into Scenario 9 pShipItemRel: {0} pSalePrice : {1}" ,pShipItemRel,pSalePrice);
							}
							metaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), lQtyAdj, pSalePrice * lQtyAdj + lAdjustedAmount*lQtyAdj,pListPrice * lQtyAdj,pSalePrice*lQtyAdj, pSalePrice, 
									giftWrapItem, pShipItemRel.getId(), pPriceQuote, null));
							long qty1 = giftWrapItemQty - lQtyAdj;
							if(qty1 > TRUConstants.LONG_ZERO){
								if (isLoggingDebug()) {
									vlogDebug("Enters into Scenario 10 pShipItemRel: {0} pSalePrice : {1}" ,pShipItemRel,pSalePrice);
								}
								metaInfoList.add(createMetaInfoItem(Boolean.FALSE, pShipItemRel.getCommerceItem(),
										qty1, pSalePrice * qty1,pListPrice * qty1,pSalePrice*qty1, pSalePrice, giftWrapItem, pShipItemRel.getId(), pPriceQuote, null));
							}
						}
						//remGiftWrapItemQtyTotal = giftWrapItemQtyTotal - giftWrapItemQty;
						if(remGiftWrapItemQtyTotal == TRUConstants.LONG_ZERO){
							remGiftWrapItemQtyTotal = giftWrapItemQtyTotal - giftWrapItemQty;
						} else{
							remGiftWrapItemQtyTotal = remGiftWrapItemQtyTotal - giftWrapItemQty;
						}
					}
					qty = pShipItemRelQty - giftWrapItemQtyTotal;
					if(qty > TRUConstants.LONG_ZERO){
						if (isLoggingDebug()) {
							vlogDebug("Enters into Scenario 11 pShipItemRel: {0} pSalePrice : {1}" ,pShipItemRel,pSalePrice);
						}
						metaInfoList.add(createMetaInfoItem(Boolean.FALSE, pShipItemRel.getCommerceItem(),
								qty, pSalePrice * qty,pListPrice * qty,pSalePrice*qty, pSalePrice, null, pShipItemRel.getId(), pPriceQuote, null));
					}
					//2T and 2B and 3qtyAdj 5TotalQty scenario will fail
				}
			} 
		} else if(pRelAndDiscountMap != null && !pRelAndDiscountMap.isEmpty()){//Single commerce item with multiple shipping groups logic
			if (isLoggingDebug()) {
				vlogDebug("Enters into Single commerce item with multiple shipping groups logic");
			}
			double salePrice = pPriceQuote.getSalePrice();
			String[] adjustedAmountArray = pRelAndDiscountMap.get(pShipItemRel.getId()).toString().split(TRUConstants.SPLIT_PIPE);
			double adjustedAmount=Double.parseDouble(adjustedAmountArray[TRUConstants._0]);
			String promotionId=adjustedAmountArray[TRUConstants._1];
			if(adjustedAmount == TRUConstants.DOUBLE_ZERO){
				metaInfoList.add(createMetaInfoItem(Boolean.FALSE, pShipItemRel.getCommerceItem(), pShipItemRelQty, salePrice * pShipItemRelQty,pListPrice * pShipItemRelQty,
						salePrice*pShipItemRelQty, salePrice, null, pShipItemRel.getId(), pPriceQuote, null));
				if (isLoggingDebug()) {
					vlogDebug("Enters into Scenario 14 when adjustedAmount is zero pShipItemRel: {0} salePrice : {1}" ,pShipItemRel,salePrice);
				}
			}else if(salePrice * pShipItemRelQty > adjustedAmount){
				int qtyTodisc = (int) (adjustedAmount/salePrice);
				if(qtyTodisc != TRUConstants._0){
					metaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), qtyTodisc, 0,pListPrice * qtyTodisc,salePrice*qtyTodisc,
							salePrice, null, pShipItemRel.getId(), pPriceQuote, promotionId));
					if (isLoggingDebug()) {
						vlogDebug("Enters into Scenario 15 pShipItemRel: {0} promotionId : {1} salePrice : {2}" ,pShipItemRel,promotionId,salePrice);
					}
				}
				long remQty = pShipItemRelQty-qtyTodisc;
				adjustedAmount = adjustedAmount - salePrice * qtyTodisc;
				metaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), remQty, ((remQty*salePrice)-adjustedAmount),pListPrice * remQty,salePrice*remQty,
						salePrice, null, pShipItemRel.getId(), pPriceQuote, promotionId));
				if (isLoggingDebug()) {
					vlogDebug("Enters into Scenario 16 pShipItemRel: {0} promotionId : {1} salePrice : {2}" ,pShipItemRel,promotionId,salePrice);
				}
			}else{
				metaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), pShipItemRelQty, salePrice * pShipItemRelQty - adjustedAmount,
						pListPrice * pShipItemRelQty,salePrice*pShipItemRelQty, salePrice, null, pShipItemRel.getId(), pPriceQuote, promotionId));
				if (isLoggingDebug()) {
					vlogDebug("Enters into Scenario 17 pShipItemRel: {0} promotionId : {1} salePrice : {2}" ,pShipItemRel,promotionId,salePrice);
				}
			}
		}else{	// Create new meta info items and with out Giftwarp items
			long totalAdjustedQty = TRUConstants.LONG_ZERO;
			double salePrice = TRUConstants.DOUBLE_ZERO;
			double adjustmentPositiveValue = TRUConstants.DOUBLE_ZERO;
			if (pPriceQuote != null && pPriceQuote.getAdjustments() != null && !pPriceQuote.getAdjustments().isEmpty()) {
				DetailedItemPriceInfo detaileditemPriceInfo = null;
				salePrice = pPriceQuote.getSalePrice();
				for (Iterator<PricingAdjustment> iterator = pPriceQuote.getAdjustments().iterator(); iterator.hasNext();) {
					PricingAdjustment pricingAdjustment = iterator.next();
					if (pricingAdjustment.getPricingModel() != null && 
							!(boolean)pricingAdjustment.getPricingModel().getPropertyValue(getPricingModelProperties().getTagPromotionPropertyName())) {
						String propertyValue = 
								(String) pricingAdjustment.getPricingModel().getPropertyValue(getPricingModelProperties().getTemplate());
						lQtyAdj = pricingAdjustment.getQuantityAdjusted();
						lAdjustedAmount = pricingAdjustment.getTotalAdjustment();
						adjustmentPositiveValue = -lAdjustedAmount;
						if(propertyValue.equals(getTieredPriceBreakTemplate())){//Tiered price break template- added this as price will be available in  detailed item price info
							detaileditemPriceInfo = (DetailedItemPriceInfo) pPriceQuote.getCurrentPriceDetails().get(0);
							double detailedSalePrice = detaileditemPriceInfo.getDetailedUnitPrice();
							metaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), lQtyAdj, detailedSalePrice * lQtyAdj,pListPrice * lQtyAdj,detailedSalePrice*lQtyAdj, salePrice, 
									null, pShipItemRel.getId(), pPriceQuote, pricingAdjustment.getPricingModel().getRepositoryId()));
							if (isLoggingDebug()) {
								vlogDebug("Enters into tiered price break condition Scenario 18 pShipItemRel: {0} promotionId : {1} detailedSalePrice : {2}" ,pShipItemRel,pricingAdjustment.getPricingModel().getRepositoryId(),detailedSalePrice);
							}
						}else if(propertyValue.equals(getGroupDiscountTemplate())){//Group discount template - added this as price will be available in  detailed item price info
							detaileditemPriceInfo = (DetailedItemPriceInfo) pPriceQuote.getCurrentPriceDetails().get(0);
							double grpDiscSalePrice = detaileditemPriceInfo.getDetailedUnitPrice();
							if(adjustmentPositiveValue > salePrice){
								if (isLoggingDebug()) {
									vlogDebug("Enters into Group discount  Scenario 19, when  sale price in promotion condition mentioned as less thenitem sale price.   ----  item sale Price : {0} sale price mentioned in promotion : {1}  pShipItemRel: {2} pShipItemRel : {3}" ,salePrice, grpDiscSalePrice, pShipItemRel );
								}
								int qtyToDispalyZeroPrice = (int) (adjustmentPositiveValue/salePrice);
								long qty = TRUConstants.LONG_ONE;
								if (isLoggingDebug()) {
									vlogDebug("Enters into Group discount  Scenario 20,  sale Price : {0} grpDiscSalePrice : {1} promotionId : {2}  pShipItemRel: {3}" ,salePrice, grpDiscSalePrice,pricingAdjustment.getPricingModel().getRepositoryId(), pShipItemRel );
								}
								metaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), qtyToDispalyZeroPrice, 0,pListPrice * qtyToDispalyZeroPrice,grpDiscSalePrice*qtyToDispalyZeroPrice, salePrice, null,
										pShipItemRel.getId(), pPriceQuote, pricingAdjustment.getPricingModel().getRepositoryId()));
								double diffValue = (adjustmentPositiveValue - (salePrice*qtyToDispalyZeroPrice));
								if(diffValue > salePrice){
									if (isLoggingDebug()) {
										vlogDebug("Enters into Group discount  Scenario 21,  sale Price : {0} grpDiscSalePrice : {1} promotionId : {2}  pShipItemRel: {3}" ,salePrice, grpDiscSalePrice,pricingAdjustment.getPricingModel().getRepositoryId(), pShipItemRel );
									}
									metaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), qty, diffValue - salePrice,pListPrice * qty,grpDiscSalePrice*qty, salePrice, null, pShipItemRel.getId(), 
											pPriceQuote, pricingAdjustment.getPricingModel().getRepositoryId()));
								} else if(diffValue == TRUConstants.DOUBLE_ZERO){
									//Do Nothing
								}else{
									if (isLoggingDebug()) {
										vlogDebug("Enters into Group discount  Scenario 22,  Sale Price : {0} grpDiscSalePrice : {1} promotionId : {2}  pShipItemRel: {3}" ,salePrice, grpDiscSalePrice,pricingAdjustment.getPricingModel().getRepositoryId(), pShipItemRel );
									}
									metaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), qty, salePrice - diffValue,pListPrice * qty,salePrice,grpDiscSalePrice*qty, null, pShipItemRel.getId(),
											pPriceQuote, pricingAdjustment.getPricingModel().getRepositoryId()));
								}
								qty += qtyToDispalyZeroPrice;
								if(lQtyAdj - qty > 0){
									if (isLoggingDebug()) {
										vlogDebug("Enters into Group discount  Scenario 23,  sale Price : {0} grpDiscSalePrice{1} promotionId : {2}  pShipItemRel: {3}" ,salePrice, grpDiscSalePrice,pricingAdjustment.getPricingModel().getRepositoryId(), pShipItemRel );
									}
									metaInfoList.add(createMetaInfoItem(Boolean.FALSE, pShipItemRel.getCommerceItem(),
											lQtyAdj - qty, salePrice * (lQtyAdj - qty) ,pListPrice * (lQtyAdj - qty),salePrice*(lQtyAdj - qty),
											salePrice, null, pShipItemRel.getId(), pPriceQuote, null));
								}
							} else{
								metaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), lQtyAdj, grpDiscSalePrice * lQtyAdj,pListPrice * lQtyAdj,grpDiscSalePrice*lQtyAdj, 
										salePrice, null, pShipItemRel.getId(), pPriceQuote, pricingAdjustment.getPricingModel().getRepositoryId()));
								if (isLoggingDebug()) {
									vlogDebug("Enters into Group discount scenario 24, when  sale price in promotion condition mentioned as greater thenitem sale price.   ----  item sale Price : {0} sale price mentioned in promotion : {1} grpDiscSalePrice : {2} pShipItemRel: {3}" ,salePrice,pricingAdjustment.getPricingModel().getRepositoryId(), grpDiscSalePrice, pShipItemRel );
								}
							}
						}else if(propertyValue.equals(getBOGODiscountTemplate())){
							//Creating meta info for  discounted items
							metaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), lQtyAdj, salePrice * lQtyAdj + lAdjustedAmount,pListPrice * lQtyAdj,salePrice*lQtyAdj,
									salePrice, null, pShipItemRel.getId(), pPriceQuote, pricingAdjustment.getPricingModel().getRepositoryId()));
							if (isLoggingDebug()) {
								vlogDebug("Enters into scenario 25 ,BOGO template where Creating meta info for  discounted items salePrice : {0} pListPrice : {1} pShipItemRel: {2} PromotionId {3}" ,salePrice,pListPrice, pShipItemRel, pricingAdjustment.getPricingModel().getRepositoryId());
							}
						}else if(getPricingTools().round(adjustmentPositiveValue) == getPricingTools().round(salePrice*lQtyAdj)){
							metaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), lQtyAdj, 0,pListPrice * lQtyAdj,salePrice*lQtyAdj, salePrice, null,
									pShipItemRel.getId(), pPriceQuote, pricingAdjustment.getPricingModel().getRepositoryId()));
							if (isLoggingDebug()) {
								vlogDebug("Enters into BOGO  discount scenario 26,when adjustmentpostiveValue equals saleprice*quantityadjusted" ,salePrice, pShipItemRel, pricingAdjustment.getPricingModel().getRepositoryId());
							}
						} else if(adjustmentPositiveValue > salePrice){
							if (isLoggingDebug()) {
								vlogDebug("Enters into Scenario 27 where adjustmentPositiveValue grt than salePrice pShipItemRel: {0} salePrice: {1} adjustmentPositiveValue : {2}" ,pShipItemRel ,salePrice,adjustmentPositiveValue);
							}
							int qtyToDispalyZeroPrice = (int) (adjustmentPositiveValue/salePrice);
							long qty = TRUConstants.LONG_ONE;
							if (isLoggingDebug()) {
								vlogDebug("Enters into Scenario 28  pShipItemRel : {0} salePrice : {1} adjustmentPositiveValue : {2} qtyToDispalyZeroPrice : {3}" ,pShipItemRel ,salePrice,adjustmentPositiveValue,qtyToDispalyZeroPrice);
							}
							metaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), qtyToDispalyZeroPrice, 0,pListPrice * qtyToDispalyZeroPrice,salePrice*qtyToDispalyZeroPrice, salePrice, null,
									pShipItemRel.getId(), pPriceQuote, pricingAdjustment.getPricingModel().getRepositoryId()));
							double diffValue = (adjustmentPositiveValue - (salePrice*qtyToDispalyZeroPrice));
							if(diffValue > salePrice){
								if (isLoggingDebug()) {
									vlogDebug("Enters into Scenario 29  pShipItemRel : {0} salePrice : {1} adjustmentPositiveValue : {2} diffValue : {3}" ,pShipItemRel ,salePrice,adjustmentPositiveValue,diffValue);
								}
								metaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), qty, diffValue - salePrice,pListPrice * qty,salePrice*qty, salePrice, null, pShipItemRel.getId(), 
										pPriceQuote, pricingAdjustment.getPricingModel().getRepositoryId()));
							} else if(diffValue == TRUConstants.DOUBLE_ZERO){
								//Do Nothing
								qty = TRUConstants.LONG_ZERO;
							}else{
								if (isLoggingDebug()) {
									vlogDebug("Enters into Scenario 30  pShipItemRel : {0} salePrice : {1} adjustmentPositiveValue : {2} diffValue : {3}" ,pShipItemRel ,salePrice,adjustmentPositiveValue,diffValue);
								}
								metaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), qty, salePrice - diffValue,pListPrice * qty,salePrice*qty, salePrice, null, pShipItemRel.getId(),
										pPriceQuote, pricingAdjustment.getPricingModel().getRepositoryId()));
							}
							qty += qtyToDispalyZeroPrice;
							if(lQtyAdj - qty > 0){
								if (isLoggingDebug()) {
									vlogDebug("Enters into Scenario 31  pShipItemRel : {0} salePrice : {1} adjustmentPositiveValue : {2}" ,pShipItemRel ,salePrice,adjustmentPositiveValue);
								}
								metaInfoList.add(createMetaInfoItem(Boolean.FALSE, pShipItemRel.getCommerceItem(),
										lQtyAdj - qty, salePrice * (lQtyAdj - qty) ,pListPrice * (lQtyAdj - qty),salePrice*(lQtyAdj - qty),
										salePrice, null, pShipItemRel.getId(), pPriceQuote, null));
							}
						} else{
							if (isLoggingDebug()) {
								vlogDebug("Enters into Scenario 12 pShipItemRel : {0} salePrice : {1}" ,pShipItemRel ,salePrice );
							}
							metaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), lQtyAdj, salePrice * lQtyAdj + lAdjustedAmount,pListPrice * lQtyAdj,salePrice*lQtyAdj,
									salePrice, null, pShipItemRel.getId(), pPriceQuote, pricingAdjustment.getPricingModel().getRepositoryId()));
						}
						if (isLoggingDebug()) {
							vlogDebug("Quantity Adjusted : {0} Pricing Model : {1}", lQtyAdj, pricingAdjustment.getPricingModel());
						}
						totalAdjustedQty += lQtyAdj;
					}
				}
			} 
			if(pShipItemRelQty - totalAdjustedQty > TRUConstants.LONG_ZERO){
				if (isLoggingDebug()) {
					vlogDebug("Enters into Scenario 13 pShipItemRel: {0} salePrice : {1} totalAdjustedQty : {2}" ,pShipItemRel,salePrice,totalAdjustedQty);
				}
				metaInfoList.add(createMetaInfoItem(Boolean.FALSE, pShipItemRel.getCommerceItem(),pShipItemRelQty - totalAdjustedQty, salePrice * (pShipItemRelQty - totalAdjustedQty),
						pListPrice* (pShipItemRelQty - totalAdjustedQty),salePrice*(pShipItemRelQty - totalAdjustedQty), salePrice, null, pShipItemRel.getId(), pPriceQuote, null));
			}
		}
		if (!metaInfoList.isEmpty()) {
			pShipItemRel.setMetaInfo(metaInfoList);
		}
		if (isLoggingDebug()) {
			vlogDebug("Exit from [Class: TRUPricingTools  method: createOrUpdateCommItemMetaInfo]");
		}
	}

	

	/**
	 * This method creates the new item with details.
	 *
	 * @param pIsAdjusted            - Whether item is adjusted
	 * @param pCommerceItem            - Commerce Item
	 * @param pQuantity            - Quantity
	 * @param pAmount            - Amount
	 * @param pListPrice the list price
	 * @param pSalePrice - Total sale price
	 * @param pUnitPrice - Total unit price
	 * @param pGiftWrapItem - Gift Wrap Item
	 * @param pShipIteRelId - ShipIteRel
	 * @param pPriceQuote - Item price info
	 * @param pPromotionId the promotion id
	 * @return repItem - the repository item
	 * @throws RepositoryException             - If any
	 */
	public TRUCommerceItemMetaInfo createMetaInfoItem(boolean pIsAdjusted, CommerceItem pCommerceItem, long pQuantity,
			double pAmount ,double pListPrice, double pSalePrice, double pUnitPrice, RepositoryItem pGiftWrapItem, String pShipIteRelId,
			TRUItemPriceInfo pPriceQuote, String pPromotionId ) throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("Enter into [Class: TRUPricingTools  method: createMetaInfoItem]");
			vlogDebug(
					"pIsAdjusted : {0} pCommerceItem : {1} pQuantity : {2} pAmount : {3} pListPrice : {4} pSalePrice : {5} pUnitPrice : {6} pShipIteRelId : {7} pPriceQuote : {8} pPromotionId : {9} ",
					pIsAdjusted,
					pCommerceItem,
					pQuantity,
					pAmount,
					pListPrice,
					pSalePrice,
					pUnitPrice,
					pShipIteRelId,
					pPriceQuote,
					pPromotionId);
		}
		double totalSavings=pListPrice- pAmount;
		double totalSavingsPercentage=(pListPrice - pAmount) / pListPrice * TRUCommerceConstants.INT_HUNDRED;
		boolean displayStrikeThroughPrice = false;
		TRUCommerceItemMetaInfo metaInfoItem = new TRUCommerceItemMetaInfo();
		if(pPriceQuote != null){
			displayStrikeThroughPrice = pPriceQuote.isDisplayStrikeThroughPrice();		
		}
		MutableRepository orderRepository = (MutableRepository) getOrderRepository();
		TRUCommercePropertyManager commerceItemPropertyManager = getCommercePropertyManager();
		MutableRepositoryItem metaInfoMutItem =
				orderRepository.createItem(commerceItemPropertyManager.getCommerceItemMetaInfoDescriptorName());
		metaInfoItem.setId(metaInfoMutItem.getRepositoryId());
		metaInfoItem.setRepositoryItem(metaInfoMutItem);
		metaInfoItem.setPropertyValue(
				commerceItemPropertyManager.getCatalogRefIdPropertyName(),
				pCommerceItem.getCatalogRefId());
		metaInfoItem.setPropertyValue(
				commerceItemPropertyManager.getProductIdPropertyName(),
				pCommerceItem.getAuxiliaryData().getProductId());
		metaInfoItem.setPropertyValue(commerceItemPropertyManager.getQuantityPropertyName(), pQuantity);
		metaInfoItem.setPropertyValue(commerceItemPropertyManager.getPricePropertyName(), pAmount);
		metaInfoItem.setPropertyValue(commerceItemPropertyManager.getIsAdjustedPropertyName(), pIsAdjusted);
		metaInfoItem.setPropertyValue(commerceItemPropertyManager.getUnitPricePropertyName(), pUnitPrice);
		metaInfoItem.setPropertyValue(commerceItemPropertyManager.getGiftWrapItemPropertyName(), pGiftWrapItem);
		metaInfoItem.setPropertyValue(commerceItemPropertyManager.getShipItemRelIdPropertyName(), pShipIteRelId);
		metaInfoItem.setPropertyValue(commerceItemPropertyManager.getPromotionIdPropertyName(), pPromotionId);
		if(displayStrikeThroughPrice){
			if (isLoggingDebug()) {
				vlogDebug("totalSavings : {0} totalSavingsPercentage {1}", totalSavings,totalSavingsPercentage);
			}
			metaInfoItem.setPropertyValue(commerceItemPropertyManager.getTotalSavedAmount(),totalSavings);
			metaInfoItem.setPropertyValue(commerceItemPropertyManager.getTotalSavedPercentage(),totalSavingsPercentage);
		}else if(pIsAdjusted){
			totalSavings = pSalePrice - pAmount;
            totalSavingsPercentage = totalSavings / pSalePrice * TRUCommerceConstants.INT_HUNDRED;
            if (isLoggingDebug()) {
				vlogDebug("totalSavings : {0} totalSavingsPercentage {1}", totalSavings,totalSavingsPercentage);
			}
            metaInfoItem.setPropertyValue(commerceItemPropertyManager.getTotalSavedAmount(),totalSavings);
            metaInfoItem.setPropertyValue(commerceItemPropertyManager.getTotalSavedPercentage(),totalSavingsPercentage);

		}else{
			metaInfoItem.setPropertyValue(commerceItemPropertyManager.getTotalSavedAmount(),TRUConstants.DOUBLE_ZERO);
			metaInfoItem.setPropertyValue(commerceItemPropertyManager.getTotalSavedPercentage(),TRUConstants.DOUBLE_ZERO);
		}
		if (isLoggingDebug()) {
			vlogDebug("New meta info item created : {0}", metaInfoItem);
			vlogDebug("Exit from [Class: TRUPricingTools  method: createMetaInfoItem]");
		}
		return metaInfoItem;
	}
/**
	 * Removes the meta info.
	 *
	 * @param pShippingGroupCommerceItemRelationship the shipping group commerce item relationship
	 * @throws RepositoryException the repository exception
	 */
	public void removeMetaInfo(
			TRUShippingGroupCommerceItemRelationship pShippingGroupCommerceItemRelationship) throws RepositoryException {
		if(isLoggingDebug()){
			logDebug("@Class::TRUOrderTools::@method::removeMetaInfo() : BEGIN");
		}
		if(pShippingGroupCommerceItemRelationship != null && null != pShippingGroupCommerceItemRelationship.getMetaInfo() && 
				!pShippingGroupCommerceItemRelationship.getMetaInfo().isEmpty()){
			pShippingGroupCommerceItemRelationship.getMetaInfo().clear();
			MutableRepository orderRepository = (MutableRepository) getOrderRepository();			
			for (TRUCommerceItemMetaInfo metaInfoItem : pShippingGroupCommerceItemRelationship.getMetaInfo()) {
				orderRepository.removeItem(metaInfoItem.getId(),getMetaInfoDescriptorName()); 
			}
		}
		if(isLoggingDebug()){
			vlogDebug("@Class::TRUOrderTools::@method::removeMetaInfo() : END");
		}
	}
	
	/**
	   * Returns the CreditCard payment group for the order. This is a utility method so form handlers can easily
	   * get the credit card payment group. This method returns the first payment group of type CreditCard. If multiple
	   * credit cards are supported, this method needs to be updated.
	   * @param pOrder - order to get credit card from
	   * @return credit card for this order
	   */
	@SuppressWarnings("unchecked")
	public CreditCard getCreditCard(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderTools.getCreditCard() method.STARTS");
			vlogDebug("TRUOrderTools.getCreditCard ,  pOrder:{0}", pOrder);
		}
		if (pOrder == null) {
			if (isLoggingDebug()) {
				vlogDebug("TRUOrderTools (getCreditCard) : pOrder:{0}", pOrder);
			}
			return null;
		}

		List<PaymentGroup> paymentGroups = (List<PaymentGroup>) pOrder.getPaymentGroups();
		if (paymentGroups == null) {
			if (isLoggingDebug()) {
				vlogDebug("TRUOrderTools (getCreditCard) : paymentGroups:{0}",paymentGroups);
			}
			return null;
		}
		int numPayGroups = paymentGroups.size();

		if (numPayGroups == TRUConstants.ZERO) {
			if (isLoggingDebug()) {
				vlogDebug("TRUOrderTools (getCreditCard) : numPayGroups:{0}",numPayGroups);
			}
			return null;
		}
		// We are only supporting a single credit card payment group.
		// Return the first one we get.
		for (PaymentGroup pg : paymentGroups) {
			if (isLoggingDebug()) {
				vlogDebug("TRUOrderTools (getCreditCard) : pg:{0}", pg);
			}
			if (pg instanceof CreditCard) {
				return (CreditCard) pg;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderTools.getCreditCard() method.END");
		}
		return null;
	}
	
	/**
	 * This method is used to retrive all the profanity words.
	 *
	 * @return the profanity words
	 * @throws RepositoryException the repository exception
	 */
	public Set<String> getProfanityWords() throws RepositoryException {
		if (isLoggingDebug()) {
		vlogDebug("@Class::TRUOrderTools::@method::getProfanityWords() : BEGIN");
		}
		RepositoryView view;
		  RepositoryItem [] profanityWords;
		  Set<String> profanityWordSet = new HashSet<String>();
		  view = getStoreConfigRepository().getView(TRUConstants.PROFANITY_WORDS);
		  QueryBuilder builder = view.getQueryBuilder();
		  Query profanityWordsQuery = builder.createUnconstrainedQuery();
		  profanityWords = view.executeQuery(profanityWordsQuery);
		  if(profanityWords != null){
			  for (int i=0; i<profanityWords.length; i++) {
		           RepositoryItem item = profanityWords[i];
		           String profanityWord = (String)item.getPropertyValue(getPropertyManager().getWord());
		           profanityWordSet.add(profanityWord.toLowerCase());
			  }
		  }
		  if (isLoggingDebug()) {
		  vlogDebug("@Class::TRUOrderTools::@method::getProfanityWords() : END");
		  }
		return profanityWordSet;
	}
	
	/**
	 * Generate out of stock items.
	 *
	 * @param pOrder the order
	 * @param pOutOfStockList - outOfStockList
	 */
	@SuppressWarnings("unchecked")
	public void generateOutOfStockItems(Set<String> pOutOfStockList, Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::generateOutOfStockItems() : BEGIN");
		}
		if (null == pOutOfStockList || pOutOfStockList.isEmpty()) {
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUOrderTools::@method::generateOutOfStockItems() : END with Empty check");
			}
			return;
		}
		List<TRUOutOfStockItem> outOfStockItems = new ArrayList<TRUOutOfStockItem>();
		if (null != ((TRUOrderImpl) pOrder).getOutOfStockItems() && !((TRUOrderImpl) pOrder).getOutOfStockItems().isEmpty()) {
			outOfStockItems.addAll(((TRUOrderImpl) pOrder).getOutOfStockItems());
		}
		TRUOutOfStockItem truOutOfStockItem = null;
		CommerceItem commerceItem = null;
		try {
			for (String cid : pOutOfStockList) {
				commerceItem = pOrder.getCommerceItem(cid);
				if (commerceItem != null) {
					List<ShippingGroupCommerceItemRelationship> sgcirs = commerceItem.getShippingGroupRelationships();
					if(sgcirs != null && !sgcirs.isEmpty()) {
						for (ShippingGroupCommerceItemRelationship sgcir : sgcirs) {
							//To avoid the duplicate code, calling the generateOutOfStockItem() method inside for loop
							truOutOfStockItem = generateOutOfStockItem(sgcir);
							outOfStockItems.add(truOutOfStockItem);
						}
					}
				}
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError(" RepositoryException in @Class::TRUOrderTools::@method::generateOutOfStockItems() :", e);
			}
		} catch (CommerceItemNotFoundException e) {
			if (isLoggingError()) {
				vlogError(" CommerceItemNotFoundException in @Class::TRUOrderTools::@method::generateOutOfStockItems() :", e);
			}
		} catch (InvalidParameterException e) {
			if (isLoggingError()) {
				vlogError(" InvalidParameterException in @Class::TRUOrderTools::@method::generateOutOfStockItems() :", e);
			}
		}
		if (!outOfStockItems.isEmpty()) {
			((TRUOrderImpl) pOrder).setOutOfStockItems(outOfStockItems);
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::generateOutOfStockItems() : END");
		}
	}

	/**
	 * Generate out of stock items by sg rel ids.
	 *
	 * @param pOutOfStockList the out of stock list
	 * @param pOrder the order
	 */
	public void generateOutOfStockItemsBySGRelIds(Set<String> pOutOfStockList, Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::generateOutOfStockItemsBySGRelIds() : BEGIN");
		}
		if (null == pOutOfStockList || pOutOfStockList.isEmpty()) {
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUOrderTools::@method::generateOutOfStockItemsBySGRelIds() : END with Empty check");
			}
			return;
		}
		List<TRUOutOfStockItem> outOfStockItems = new ArrayList<TRUOutOfStockItem>();
		if (null != ((TRUOrderImpl) pOrder).getOutOfStockItems() && !((TRUOrderImpl) pOrder).getOutOfStockItems().isEmpty()) {
			outOfStockItems.addAll(((TRUOrderImpl) pOrder).getOutOfStockItems());
		}
		TRUOutOfStockItem truOutOfStockItem = null;
		try {
			for(String relationshipId:pOutOfStockList){
				Relationship relationship = pOrder.getRelationship(relationshipId);
				if (relationship instanceof ShippingGroupCommerceItemRelationship){
					TRUShippingGroupCommerceItemRelationship sgcir = (TRUShippingGroupCommerceItemRelationship) relationship;
					truOutOfStockItem = generateOutOfStockItem(sgcir);
					outOfStockItems.add(truOutOfStockItem);
				} 
			}
		}
		catch (RepositoryException re) {
			if (isLoggingError()) {
				vlogError(" RepositoryException in @Class::TRUOrderTools::@method::generateOutOfStockItemsBySGRelIds() :", re);
			}
		}  catch (InvalidParameterException ie) {
			if (isLoggingError()) {
				vlogError(" InvalidParameterException in @Class::TRUOrderTools::@method::generateOutOfStockItemsBySGRelIds() :", ie);
			}
		} catch (RelationshipNotFoundException relE) {
			if (isLoggingError()) {
				vlogError(" InvalidParameterException in @Class::TRUOrderTools::@method::generateOutOfStockItemsBySGRelIds() :", relE);
			}
		}
		if(!outOfStockItems.isEmpty()){
			((TRUOrderImpl)pOrder).getOutOfStockItems().addAll(outOfStockItems);
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::generateOutOfStockItemsBySGRelIds() : END");
		}
	}
	
	/**
	 * Generate out of stock item.
	 *
	 * @param pSGCItemRel the SGC item rel
	 * @return the TRUOutOfStockItem
	 * @throws RepositoryException the repository exception
	 */
	private TRUOutOfStockItem generateOutOfStockItem(ShippingGroupCommerceItemRelationship pSGCItemRel) throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::generateOutOfStockItem() : BEGIN");
		}
		TRUCommercePropertyManager commerceItemPropertyManager = getCommercePropertyManager();
		MutableRepository orderRepository = (MutableRepository) getOrderRepository();
		MutableRepositoryItem outOfStockItem = orderRepository.createItem(commerceItemPropertyManager
				.getOutOfStockItemDescriptorName());
		CommerceItem commerceItem = pSGCItemRel.getCommerceItem();
		ShippingGroup sg = pSGCItemRel.getShippingGroup();
		TRUOutOfStockItem truOutOfStockItem = new TRUOutOfStockItem();
		truOutOfStockItem.setId(outOfStockItem.getRepositoryId());
		if (truOutOfStockItem instanceof ChangedProperties) {
			((ChangedProperties) truOutOfStockItem).setRepositoryItem(outOfStockItem);
			((ChangedProperties) truOutOfStockItem).setSaveAllProperties(Boolean.TRUE);
		}
		TRUItemPriceInfo priceQuote = (TRUItemPriceInfo) pSGCItemRel.getCommerceItem().getPriceInfo();
		truOutOfStockItem.setPropertyValue(commerceItemPropertyManager.getCatalogRefIdPropertyName(),
				commerceItem.getCatalogRefId());
		truOutOfStockItem.setPropertyValue(commerceItemPropertyManager.getProductIdPropertyName(), commerceItem
				.getAuxiliaryData().getProductId());
		truOutOfStockItem.setPropertyValue(commerceItemPropertyManager.getQuantityPropertyName(),
				pSGCItemRel.getQuantity());
		if (null != sg && sg instanceof TRUInStorePickupShippingGroup || sg instanceof TRUChannelInStorePickupShippingGroup) {
			String locationId = ((TRUInStorePickupShippingGroup) pSGCItemRel.getShippingGroup()).getLocationId();
			truOutOfStockItem.setPropertyValue(commerceItemPropertyManager.getLocationIdPropertyName(),
					locationId);
		}
		setChannelInfoToOutofStockItems(commerceItemPropertyManager, sg, truOutOfStockItem);
		if (null != priceQuote) {
			truOutOfStockItem.setPropertyValue(commerceItemPropertyManager.getSalePricePropertyName(),
					priceQuote.getSalePrice());
			truOutOfStockItem.setPropertyValue(commerceItemPropertyManager.getListPricePropertyName(),
					priceQuote.getListPrice());
			if (priceQuote.isDisplayStrikeThroughPrice()) {
				double totalSavings = (priceQuote.getListPrice() - priceQuote.getSalePrice()) * pSGCItemRel.getQuantity();
				double totalSavingsPercentage = totalSavings / (priceQuote.getListPrice() * pSGCItemRel.getQuantity()) * TRUConstants.HUNDERED;
				if (isLoggingDebug()) {
						vlogDebug("totalSavings : {0} totalSavingsPercentage {1}", totalSavings,totalSavingsPercentage);
				}
				truOutOfStockItem.setPropertyValue(commerceItemPropertyManager.getTotalSavedAmount(),
						totalSavings);
				truOutOfStockItem.setPropertyValue(commerceItemPropertyManager.getTotalSavedPercentage(),
						totalSavingsPercentage);
			} else {
				truOutOfStockItem.setPropertyValue(commerceItemPropertyManager.getTotalSavedAmount(),
						TRUConstants.DOUBLE_ZERO);
				truOutOfStockItem.setPropertyValue(commerceItemPropertyManager.getTotalSavedPercentage(),
						TRUConstants.DOUBLE_ZERO);
			}
		}
		truOutOfStockItem.setPropertyValue(commerceItemPropertyManager.getSiteIdPropertyName(),
				SiteContextManager.getCurrentSiteId());
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::generateOutOfStockItems() : END");
		}
		return truOutOfStockItem;
	}
	
	
	/**
	 * Sets the channel info to outof stock items.
	 *
	 * @param pCommerceItemPropertyManager the commerce item property manager
	 * @param pSg the sg
	 * @param pOutOfStockItem the out of stock item
	 */
	private void setChannelInfoToOutofStockItems(TRUCommercePropertyManager pCommerceItemPropertyManager, ShippingGroup pSg,
			TRUOutOfStockItem pOutOfStockItem) {
		if(pSg instanceof TRUChannelInStorePickupShippingGroup){
			if (!StringUtils.isBlank(((TRUChannelInStorePickupShippingGroup) pSg).getChannelId())) {
				pOutOfStockItem.setPropertyValue(pCommerceItemPropertyManager.getChannelId(),
						((TRUChannelInStorePickupShippingGroup) pSg).getChannelId());
			}
			if (!StringUtils.isBlank(((TRUChannelInStorePickupShippingGroup) pSg).getChannelType())) {
				pOutOfStockItem.setPropertyValue(pCommerceItemPropertyManager.getChannelType(),
						((TRUChannelInStorePickupShippingGroup) pSg).getChannelType());
			}
			if (!StringUtils.isBlank(((TRUChannelInStorePickupShippingGroup) pSg).getChannelUserName())) {
				pOutOfStockItem.setPropertyValue(pCommerceItemPropertyManager.getChannelUserName(),
						((TRUChannelInStorePickupShippingGroup) pSg).getChannelUserName());
			}
			if (!StringUtils.isBlank(((TRUChannelInStorePickupShippingGroup) pSg).getChannelCoUserName())) {
				pOutOfStockItem.setPropertyValue(pCommerceItemPropertyManager.getChannelCoUserName(),
						((TRUChannelInStorePickupShippingGroup) pSg).getChannelCoUserName());
			}
			if (!StringUtils.isBlank(((TRUChannelInStorePickupShippingGroup) pSg).getChannelName())) {
				pOutOfStockItem.setPropertyValue(pCommerceItemPropertyManager.getChannelName(),
						((TRUChannelInStorePickupShippingGroup) pSg).getChannelName());
			}
		}
		else if(pSg instanceof TRUChannelHardgoodShippingGroup){
			if (!StringUtils.isBlank(((TRUChannelHardgoodShippingGroup) pSg).getChannelId())) {
				pOutOfStockItem.setPropertyValue(pCommerceItemPropertyManager.getChannelId(),((TRUChannelHardgoodShippingGroup) pSg).getChannelId());
			}
			if (!StringUtils.isBlank(((TRUChannelHardgoodShippingGroup) pSg).getChannelType())) {
				pOutOfStockItem.setPropertyValue(pCommerceItemPropertyManager.getChannelType(),
						((TRUChannelHardgoodShippingGroup) pSg).getChannelType());
			}
			if (!StringUtils.isBlank(((TRUChannelHardgoodShippingGroup) pSg).getChannelUserName())) {
				pOutOfStockItem.setPropertyValue(pCommerceItemPropertyManager.getChannelUserName(),
						((TRUChannelHardgoodShippingGroup) pSg).getChannelUserName());
			}
			if (!StringUtils.isBlank(((TRUChannelHardgoodShippingGroup) pSg).getChannelCoUserName())) {
				pOutOfStockItem.setPropertyValue(pCommerceItemPropertyManager.getChannelCoUserName(),
						((TRUChannelHardgoodShippingGroup) pSg).getChannelCoUserName());
			}
			if (!StringUtils.isBlank(((TRUChannelHardgoodShippingGroup) pSg).getChannelName())) {
				pOutOfStockItem.setPropertyValue(pCommerceItemPropertyManager.getChannelName(),
						((TRUChannelHardgoodShippingGroup) pSg).getChannelName());
			}
		}
	}
	
	/**
	 * Removes the bpp item info.
	 *
	 * @param pOrder the order
	 * @param pRelationShipId the relation ship id
	 * @throws RepositoryException the repository exception
	 * @throws RelationshipNotFoundException the relationship not found exception
	 * @throws InvalidParameterException the invalid parameter exception
	 */
	public void removeBPPItemInfo(Order pOrder, String pRelationShipId) throws RepositoryException,
			RelationshipNotFoundException, InvalidParameterException {
		vlogDebug("@Class::TRUOrderTools::@method::removeBPPItemInfo() : BEGIN");
		TRUShippingGroupCommerceItemRelationship shipItemRel = (TRUShippingGroupCommerceItemRelationship) pOrder
				.getRelationship(pRelationShipId);
		if (shipItemRel != null && null != shipItemRel.getBppItemInfo() && StringUtils.isNotBlank(shipItemRel.getBppItemInfo().getId())) {
			TRUBppItemInfo bppItemInfo = shipItemRel.getBppItemInfo();
			bppItemInfo.setBppItemId(null);
			bppItemInfo.setBppPrice(TRUConstants.DOUBLE_ZERO);
			bppItemInfo.setBppSkuId(null);
			bppItemInfo.setId(null);
			bppItemInfo.setRepositoryItem(null);
			shipItemRel.setBppItemInfo(bppItemInfo);
		}
		vlogDebug("@Class::TRUOrderTools::@method::removeBPPItemInfo() : END");
	}
	
	/**
	 * This method updates the InStore pick up items data with user inputs.
	 *
	 * @param pOrder order
	 * @param pInStorePickupShippingGroup the in store pickup shipping group
	 * @param pStorePickUpInfo the store pick up info
	 * @throws CommerceException the commerce exception
	 */
	public void updateInStoreData(Order pOrder, InStorePickupShippingGroup pInStorePickupShippingGroup, 
			TRUStorePickUpInfo pStorePickUpInfo) throws CommerceException {
		if (isLoggingDebug()) {
		vlogDebug("@Class::TRUOrderTools::@method::updateInStoreData() : START");
		}
		if (pInStorePickupShippingGroup == null || pStorePickUpInfo == null) {
			throw new CommerceException();
		}
		TRUStorePickUpInfo storePickUpInfo = pStorePickUpInfo;
		if (pInStorePickupShippingGroup instanceof TRUInStorePickupShippingGroup) {
			TRUInStorePickupShippingGroup inStorePickupShippingGroup = (TRUInStorePickupShippingGroup) pInStorePickupShippingGroup;
			inStorePickupShippingGroup.setShipToName(null);
			inStorePickupShippingGroup.setFirstName(storePickUpInfo.getFirstName().trim());
			inStorePickupShippingGroup.setLastName(storePickUpInfo.getLastName().trim());
			inStorePickupShippingGroup.setPhoneNumber(storePickUpInfo.getPhoneNumber().trim());
			inStorePickupShippingGroup.setEmail(storePickUpInfo.getEmail().trim());
			if (storePickUpInfo.isAlternateInfoRequired()) {
				inStorePickupShippingGroup.setAltAddrRequired(storePickUpInfo.isAlternateInfoRequired());
				inStorePickupShippingGroup.setAltFirstName(storePickUpInfo.getAlternateFirstName().trim());
				inStorePickupShippingGroup.setAltLastName(storePickUpInfo.getAlternateLastName().trim());
				inStorePickupShippingGroup.setAltPhoneNumber(storePickUpInfo.getAlternatePhoneNumber().trim());
				inStorePickupShippingGroup.setAltEmail(storePickUpInfo.getAlternateEmail().trim());
			} else {
				inStorePickupShippingGroup.setAltAddrRequired(Boolean.FALSE);
				inStorePickupShippingGroup.setAltFirstName(null);
				inStorePickupShippingGroup.setAltLastName(null);
				inStorePickupShippingGroup.setAltPhoneNumber(null);
				inStorePickupShippingGroup.setAltEmail(null);
			}
		}
		if (isLoggingDebug()) {
		vlogDebug("@Class::TRUOrderTools::@method::updateInStoreData() : END");
		}
	}
	
	/**
	 * This method returns the all the instore pickup shipping groups.
	 * 
	 * @param pOrder order
	 * @return listOfInstorePickupSG
	 */
	@SuppressWarnings("unchecked")
	public List<TRUInStorePickupShippingGroup> getAllInStorePickupShippingGroups(Order pOrder){
		if (isLoggingDebug()) {
		vlogDebug("@Class::TRUOrderTools::@method::getAllInStorePickupShippingGroups() : BEGIN");
		vlogDebug("INPUT PARAMS OF getAllInStorePickupShippingGroups method pOrder: {0} ", pOrder);
		}
		List<TRUInStorePickupShippingGroup> listOfInstorePickupSG = new ArrayList<TRUInStorePickupShippingGroup>();
		List<ShippingGroup> listOfShipGroup = pOrder.getShippingGroups();
		for(ShippingGroup sg : listOfShipGroup){
			if(sg instanceof TRUInStorePickupShippingGroup){
				listOfInstorePickupSG.add((TRUInStorePickupShippingGroup)sg);
			}
		}
		if (isLoggingDebug()) {
		vlogDebug("End:: TRUPurchaseProcessHelper.getAllInStorePickupShippingGroups and returns listOfInstorePickupSG: {0}", listOfInstorePickupSG);
		vlogDebug("@Class::TRUOrderTools::@method::getAllInStorePickupShippingGroups() : END");
		}
		return listOfInstorePickupSG;
	}
	
	/**
	 * This method will populate the payerId in PayPal Payment Group.
	 *
	 * @param pOrder   current order
	 * @param pBillingAddress billingAddress
	 * @param pPayerId payerId which comes from PayPal site
	 * @throws CommerceException  commerceException
	 */
	public void updatePayPalDetails(Order pOrder,Address pBillingAddress,String pPayerId) throws CommerceException{
		
		if (isLoggingDebug()) {
			vlogDebug("Entering into TRUOrderManager.updatePayPalDetails() menthod");
			vlogDebug("The payer id is" ,pPayerId );
		}
		if (pOrder == null || StringUtils.isBlank(pPayerId)) {
			return;
		}
		PaymentGroup paymentGroup = fetchPayPalPaymentGroupForOrder(pOrder);
		if(paymentGroup instanceof TRUPayPal){
			((TRUPayPal)paymentGroup).setPayerId(pPayerId);
			((TRUPayPal)paymentGroup).setBillingAddress(pBillingAddress);
		}
		
		if (isLoggingDebug()) {
			vlogDebug("Exiting from TRUOrderManager.updatePayPalDetails() menthod");
		}		
	}
	
	/**
	 * This method will return the payment group of type Paypal from the list of
	 * payment groups for an order.
	 * 
	 * @param pOrder
	 *            current order
	 * @return TRUPayPal payPalPG
	 */
	@SuppressWarnings("unchecked")
	public TRUPayPal fetchPayPalPaymentGroupForOrder(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("Entering into TRUOrderManager.getPayPalPaymentGroupForOrder() method");
		}
		TRUPayPal payPalPG = null;
		if (pOrder == null) {
			return payPalPG;
		}
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		for (PaymentGroup paymentGroup : paymentGroups) {
			if (paymentGroup instanceof TRUPayPal) {
				if (isLoggingDebug()) {
					vlogDebug("payPal payment group already added to order : " + paymentGroup.toString());
					vlogDebug("Exiting from TRUOrderManager.getPayPalPaymentGroupForOrder() method");
				}
				return (TRUPayPal) paymentGroup;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("No payPal payment group already added to order");
			vlogDebug("Exiting from TRUOrderManager.getPayPalPaymentGroupForOrder() method");
		}
		return payPalPG;
	}
	
	/**
	 * This method sets the shipping address.
	 *
	 * @param pOrder   current order
	 * @param pShippingAddress   ShippingAddress
	 */
	@SuppressWarnings("unchecked")
	public void addShippingAddress(Order pOrder,Address pShippingAddress){
		if (isLoggingDebug()) {
		vlogDebug("@Class::TRUOrderTools::@method::addShippingAddress() : BEGIN");
		}
		List<ShippingGroup> shippingGroupList = pOrder.getShippingGroups();
		if (shippingGroupList != null && !shippingGroupList.isEmpty()) {
			for (ShippingGroup shippingGroup : shippingGroupList) {
				if(shippingGroup instanceof TRUHardgoodShippingGroup){
					((TRUHardgoodShippingGroup) shippingGroup).setShippingAddress(pShippingAddress);
				}
			}
		}
		if (isLoggingDebug()) {
		vlogDebug("@Class::TRUOrderTools::@method::addShippingAddress() : END");
		}
	}
	
	/**
	 * This method is used to check if order is eligible for special financing or not. 
	 * It sets the transient property orderIsEligibleForFinancing to true/false accordingly.
	 * It also sets the transient property financingTermsAvailable to 'six' or 'twelve' in order based on order price total if order is eligible for financing.
	 * @param pOrder Order
	 */
	@SuppressWarnings("static-access")
	public void updateOrderIfEligibleForFinancing(Order pOrder) {
		if (isLoggingDebug()) {
		vlogDebug("@Class::TRUOrderTools::@method::updateOrderIfEligibleForFinancing() : BEGIN");
		}
		TRUOrderImpl order = (TRUOrderImpl) pOrder;
		TRUOrderManager orderManager = (TRUOrderManager)getOrderManager();
		double orderTotal = orderManager.getOrderRemaningAmount(order);
		
		int financingTermValue = 0;
		boolean orderIsEligible = false;
		Site site  = getSiteContextManager().getCurrentSite();
		Double sixMonthFinancingLimit = (Double) site.getPropertyValue(getCatalogProperties().getThresholdSixMonthFinancing());
		Double twelveMonthFinancingLimit = (Double) site.getPropertyValue(getCatalogProperties().getThresholdTwelveMonthFinancing());
		((TRUOrderImpl)pOrder).setOrderIsEligibleForFinancing(Boolean.FALSE);
		if (sixMonthFinancingLimit != null && twelveMonthFinancingLimit != null && orderTotal >= sixMonthFinancingLimit) {
			orderIsEligible = true;
			if ((orderTotal >= sixMonthFinancingLimit) && (orderTotal < twelveMonthFinancingLimit)) {
				financingTermValue = TRUCheckoutConstants.CHECKOUT_FINANCING_SIX_MONTHS;
			} else if (orderTotal >= twelveMonthFinancingLimit) {
				financingTermValue = TRUCheckoutConstants.CHECKOUT_FINANCING_TWELVE_MONTHS;
			}
		}
		((TRUOrderImpl)pOrder).setOrderIsEligibleForFinancing(orderIsEligible);
		((TRUOrderImpl)pOrder).setFinancingTermsAvailable(financingTermValue);
		if (isLoggingDebug()) {
		vlogDebug("@Class::TRUOrderTools::@method::updateOrderIfEligibleForFinancing() : END");
		}
	}
	/**
	 * hold the property orderManager.
	 */
	private OrderManager mOrderManager;
	
	/**
	 * @return the orderManager
	 */
	public OrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * @param pOrderManager the orderManager to set
	 */
	public void setOrderManager(OrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}
	
	

	/**
	 * This method checks the order contains gift item or not.
	 * @param pOrder - OrderImpl
	 * @return isGiftItem - boolean 
	 */
	@SuppressWarnings("unchecked")
	public boolean isOrderHasGiftItem(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderTools.isOrderHasGiftItem method.STARTS");
			vlogDebug("TRUOrderTools.isOrderHasGiftItem ,pOrder:{0}",pOrder);
	    }
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		if (shippingGroups != null && !shippingGroups.isEmpty()) {
			for (ShippingGroup shippingGroup : shippingGroups) {
				if (shippingGroup instanceof HardgoodShippingGroup) {
					List<CommerceItemRelationship> ciRels = shippingGroup.getCommerceItemRelationships();
					for (CommerceItemRelationship commerceItemRelationship : ciRels) {
						TRUShippingGroupCommerceItemRelationship truSgCiR = (TRUShippingGroupCommerceItemRelationship) commerceItemRelationship;
						if (truSgCiR.getIsGiftItem()) {
							if (isLoggingDebug()) {
								vlogDebug("TRUOrderTools.isOrderHasGiftItem method , Order has Gift item");
							}
							return true ;
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUOrderTools.isOrderHasGiftItem method.ENDS");
		}
		return false;
	}
	
    /**
     * This method is used to return the remaining amount in the order
     * for which payment group can be set.
     * @param pOrder Order.
     * @return double
     */
    @SuppressWarnings("rawtypes")
	public double getRemainingOrderAmount(Order pOrder) {
    	if (isLoggingDebug()) {
    	vlogDebug("@Class::TRUOrderTools::@method::getRemainingOrderAmount() : BEGIN");
    	}
        double paymentGroupAmt = TRUConstants.DOUBLE_ZERO;
        double orderAmt = getPricingTools().round(pOrder.getPriceInfo().getTotal());
        List paymentGroupList = pOrder.getPaymentGroups();
        Iterator l_oIterator = paymentGroupList.iterator();
        PaymentGroup paymentGroup = null;
        while (l_oIterator.hasNext()) {
        	paymentGroup = (PaymentGroup) l_oIterator.next();
            if (paymentGroup instanceof TRUGiftCard) {
                TRUGiftCard l_oEcomGiftCard = (TRUGiftCard) paymentGroup;
                paymentGroupAmt += l_oEcomGiftCard.getAmount();
            }
        }
        if (isLoggingDebug()) {
        vlogDebug("@Class::TRUOrderTools::@method::getRemainingOrderAmount() : END");
        }
        return  orderAmt - paymentGroupAmt;
    }
    
    /**
     * This method is used to return the remaining amount in the Layaway order
     * for which payment group can be set.
     * @param pOrder TRULayawayOrderImpl.
     */
    @SuppressWarnings("rawtypes")
	public void updateRemainingAmountInLayawayOrder(TRULayawayOrderImpl pOrder) {
    	if (isLoggingDebug()) {
    	vlogDebug("@Class::TRUOrderTools::@method::getRemainingOrderAmount() : BEGIN");
    	}
        double paymentGroupAmt = TRUConstants.DOUBLE_ZERO;
        double orderAmt = getPricingTools().round(pOrder.getPaymentAmount());
        List paymentGroupList = pOrder.getPaymentGroups();
        Iterator l_oIterator = paymentGroupList.iterator();
        PaymentGroup paymentGroup = null;
        while (l_oIterator.hasNext()) {
        	paymentGroup = (PaymentGroup) l_oIterator.next();
            if (paymentGroup instanceof TRUGiftCard) {
                TRUGiftCard l_oEcomGiftCard = (TRUGiftCard) paymentGroup;
                paymentGroupAmt += l_oEcomGiftCard.getAmount();
            }
        }
        pOrder.setRemainingAmount(orderAmt-paymentGroupAmt);
        if (isLoggingDebug()) {
        vlogDebug("@Class::TRUOrderTools::@method::getRemainingOrderAmount() : END");
        }
    }
    
    /**
     * This method is used to return the gift card amount applied on the order.
     *
     * @param pOrder Order.
     * @return double
     */
    @SuppressWarnings("rawtypes")
	public double getGiftCardAppliedAmount(Order pOrder) {
    	if (isLoggingDebug()) {
    	vlogDebug("@Class::TRUOrderTools::@method::getGiftCardAppliedAmount() : BEGIN");
    	}
        double paymentGroupAmt = TRUConstants.DOUBLE_ZERO;
        List paymentGroupList = null;
        if(pOrder != null && pOrder.getPaymentGroups() != null){
        paymentGroupList = pOrder.getPaymentGroups();
        Iterator l_oIterator = paymentGroupList.iterator();
        PaymentGroup paymentGroup = null;
	        while (l_oIterator.hasNext()) {
	        	paymentGroup = (PaymentGroup) l_oIterator.next();
	            if (paymentGroup instanceof TRUGiftCard) {
	                TRUGiftCard giftCard = (TRUGiftCard) paymentGroup;
	                paymentGroupAmt += giftCard.getAmount();
	            }
	        }
        }
        if (isLoggingDebug()) {
        vlogDebug("@Class::TRUOrderTools::@method::getGiftCardAppliedAmount() : END");
        }
        return paymentGroupAmt;
    }
    
    /**
     * Adds the bpp item to order.
     *
     * @param pOrder the order
     * @param pRelationShipId the relation ship id
     * @param pBppSkuId the bpp sku id
     * @param pBppItemId the bpp item id
     * @throws RepositoryException the repository exception
     * @throws RelationshipNotFoundException the relationship not found exception
     * @throws InvalidParameterException the invalid parameter exception
     */
	public void addBppItemToOrder(Order pOrder, String pRelationShipId, String pBppSkuId, String pBppItemId)
			throws RepositoryException, RelationshipNotFoundException, InvalidParameterException {
		vlogDebug("@Class::TRUOrderTools::@method::addBppItemToOrder() : BEGIN");
		TRUBppItemInfo bppItemInfo = new TRUBppItemInfo(); 
		TRUShippingGroupCommerceItemRelationship shipItemRel = (TRUShippingGroupCommerceItemRelationship) pOrder
				.getRelationship(pRelationShipId);
		RepositoryItem bppItem = getCatalogTools().getCatalog().getItem(pBppItemId,
				((TRUCatalogTools) getCatalogTools()).getBppDescriptorName());
		double bppPrice = TRUCommerceConstants.DOUBLE_ZERO; 
		if (null != bppItem) {
			Object bppPriceObj = bppItem.getPropertyValue(getCommercePropertyManager().getBppRetailPropertyName());
			if(null != bppPriceObj) {
				bppPrice = (double) bppPriceObj;
			}
			MutableRepository orderRepository = (MutableRepository) getOrderRepository();
			MutableRepositoryItem bppItemInfoForAdd = orderRepository.createItem(getBppItemInfoDescriptorName());
			if(null != bppItemInfoForAdd) {
				bppItemInfo.setId(bppItemInfoForAdd.getRepositoryId());
				if (bppItemInfo instanceof ChangedProperties) {
					((ChangedProperties) bppItemInfo).setSaveAllProperties(Boolean.TRUE);
					((ChangedProperties) bppItemInfo).setRepositoryItem(bppItemInfoForAdd);
				}
			}
			bppItemInfo.setBppItemId(pBppItemId);
			bppItemInfo.setBppSkuId(pBppSkuId);
			bppItemInfo.setBppPrice(bppPrice);
			
			shipItemRel.setBppItemInfo(bppItemInfo);
		}
		vlogDebug("@Class::TRUOrderTools::@method::addBppItemToOrder() : END");
	}

	
	/**
	 * Update bpp item in order.
	 *
	 * @param pOrder the order
	 * @param pRelationShipId the relation ship id
	 * @param pBppSkuId the bpp sku id
	 * @param pBppItemId the bpp item id
	 * @throws RepositoryException the repository exception
	 * @throws RelationshipNotFoundException the relationship not found exception
	 * @throws InvalidParameterException the invalid parameter exception
	 */
	public void updateBppItemInOrder(Order pOrder, String pRelationShipId, String pBppSkuId,
			String pBppItemId) throws RepositoryException, RelationshipNotFoundException, InvalidParameterException {
		vlogDebug("@Class::TRUOrderTools::@method::updateBppItemInOrder() : BEGIN");
		TRUShippingGroupCommerceItemRelationship shipItemRel = (TRUShippingGroupCommerceItemRelationship) pOrder
				.getRelationship(pRelationShipId);
		RepositoryItem bppItem = getCatalogTools().getCatalog().getItem(pBppItemId,
				((TRUCatalogTools) getCatalogTools()).getBppDescriptorName());
		double bppPrice = TRUConstants.DOUBLE_ZERO;
		if (null != bppItem) {
			Object bppPriceObj = bppItem.getPropertyValue(getCommercePropertyManager().getBppRetailPropertyName());
			if(null != bppPriceObj) {
				bppPrice = (double) bppPriceObj;
			}
			TRUBppItemInfo bppItemInfo = shipItemRel.getBppItemInfo();
			bppItemInfo.setBppItemId(pBppItemId);
			bppItemInfo.setBppSkuId(pBppSkuId);
			bppItemInfo.setBppPrice(bppPrice);
	
			shipItemRel.setBppItemInfo(bppItemInfo);
		}
		vlogDebug("@Class::TRUOrderTools::@method::updateBppItemInOrder() : END");
	}
	
	/**
	 * This method is used to create a new order of type TRULayawayOrderImpl.
	 * @return TRULayawayOrderImpl - TRULayawayOrderImpl
	 * @throws CommerceException - CommerceException
	 * 
	 */
	public TRULayawayOrderImpl createLayawayOrder() throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::createLayawayOrder() : BEGIN");
		}
		String orderClassName = getOrderTypeClassMap().getProperty(getLayawayOrderType());
		Order order = null;
		try {
			order = (TRULayawayOrderImpl)Class.forName(orderClassName).newInstance();
			if (order instanceof ChangedProperties) {
		        ((ChangedProperties)order).setSaveAllProperties(true);
			}
			if (order != null) {
			  MutableRepository mutRep = (MutableRepository)getOrderRepository();
		      MutableRepositoryItem mutItem = mutRep.createItem(getPropertyManager().getLayawayItemDescriptorPropertyName());
		      DynamicBeans.setPropertyValue(order, getCatalogProperties().getLayawayOrderid(), mutItem.getRepositoryId());
		      DynamicBeans.setPropertyValue(order, getCatalogProperties().getSiteId(), mutItem.getRepositoryId());
		      if (order instanceof ChangedProperties) {
		        ((ChangedProperties)order).setRepositoryItem(mutItem);
		      }
		      order.setOrderClassType(getLayawayOrderType());
			}
		} catch (InstantiationException ine) {
			if (isLoggingError()) {
			vlogError("InstantiationException occured in TRUOrderTools.createLayawayOrder()",ine);
			}
			throw new CommerceException(ine);
		} catch (IllegalAccessException ile) {
			if (isLoggingError()) {
			vlogError("IllegalAccessException occured in TRUOrderTools.createLayawayOrder()",ile);
			}
			throw new CommerceException(ile);
		} catch (ClassNotFoundException cnfe) {
			if (isLoggingError()) {
			vlogError("ClassNotFoundException occured in TRUOrderTools.createLayawayOrder()",cnfe);
			}
			throw new CommerceException(cnfe);
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				vlogError("RepositoryException occured in TRUOrderTools.createLayawayOrder()",re);
			}
	      throw new ObjectCreationException(re);
	    } catch (PropertyNotFoundException pne) {
	    	if (isLoggingError()) {
	    		vlogError("RepositoryException occured in TRUOrderTools.createLayawayOrder()",pne);
	    	}
		  throw new ObjectCreationException(pne);
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::createLayawayOrder() : END");
			}
		return (TRULayawayOrderImpl) order;
	}
	
	/**
	 * Gets the bpp item info descriptor name.
	 *
	 * @return the bpp item info descriptor name
	 */
	public String getBppItemInfoDescriptorName() {
		return mBppItemInfoDescriptorName;
	}
	
	/**
	 * Sets the bpp item info descriptor name.
	 *
	 * @param pBppItemInfoDescriptorName the new bpp item info descriptor name
	 */
	public void setBppItemInfoDescriptorName(String pBppItemInfoDescriptorName) {
		mBppItemInfoDescriptorName = pBppItemInfoDescriptorName;
	}
	
	/**
	 * property to hold the CreditCardInfo.
	 *
	 * @return the credit card info
	 */
	public GenericCreditCardInfo getCreditCardInfo() {
		return mCreditCardInfo;
	}

	/**
	 * property to hold the CreditCardInfo.
	 *
	 * @param pCreditCardInfo the new credit card info
	 */
	public void setCreditCardInfo(GenericCreditCardInfo pCreditCardInfo) {
		mCreditCardInfo = pCreditCardInfo;
	}


	/**
	 * Returns the commercePropertyManager.
	 *
	 * @return the commercePropertyManager
	 */
	public TRUCommercePropertyManager getCommercePropertyManager() {
		return mCommercePropertyManager;
	}

	/**
	 * Sets/updates the commercePropertyManager.
	 *
	 * @param pCommercePropertyManager            the commercePropertyManager to set
	 */
	public void setCommercePropertyManager(TRUCommercePropertyManager pCommercePropertyManager) {
		mCommercePropertyManager = pCommercePropertyManager;
	}
	
	/**
	 * Gets the meta info descriptor name.
	 *
	 * @return the meta info descriptor name
	 */
	public String getMetaInfoDescriptorName() {
		return mMetaInfoDescriptorName;
	}

	/**
	 * Sets the meta info descriptor name.
	 *
	 * @param pMetaInfoDescriptorName the new meta info descriptor name
	 */
	public void setMetaInfoDescriptorName(String pMetaInfoDescriptorName) {
		mMetaInfoDescriptorName = pMetaInfoDescriptorName;
	}
	
	/**
	 * Gets the store config repository.
	 *
	 * @return the mStoreConfigRepository
	 */
	public Repository getStoreConfigRepository() {
		return mStoreConfigRepository;
	}

	/**
	 * Sets the store config repository.
	 *
	 * @param pStoreConfigRepository the new store config repository
	 */
	public void setStoreConfigRepository(Repository pStoreConfigRepository) {
		this.mStoreConfigRepository = pStoreConfigRepository;
	}

	/**
	 * Gets the tiered price break template.
	 *
	 * @return the tieredPriceBreakTemplate
	 */
	public String getTieredPriceBreakTemplate() {
		return mTieredPriceBreakTemplate;
	}

	/**
	 * Sets the tiered price break template.
	 *
	 * @param pTieredPriceBreakTemplate the tieredPriceBreakTemplate to set
	 */
	public void setTieredPriceBreakTemplate(String pTieredPriceBreakTemplate) {
		mTieredPriceBreakTemplate = pTieredPriceBreakTemplate;
	}

	/**
	 * Gets the pricing model properties.
	 *
	 * @return the pricingModelProperties
	 */
	public TRUPricingModelProperties getPricingModelProperties() {
		return mPricingModelProperties;
	}

	/**
	 * Sets the pricing model properties.
	 *
	 * @param pPricingModelProperties the pricingModelProperties to set
	 */
	public void setPricingModelProperties(
			TRUPricingModelProperties pPricingModelProperties) {
		mPricingModelProperties = pPricingModelProperties;
	}
	
	/**
	 * Gets the channel in store pickup shipping group type.
	 *
	 * @return the channel in store pickup shipping group type
	 */
	public String getChannelInStorePickupShippingGroupType() {
		return mChannelInStorePickupShippingGroupType;
	}

	/**
	 * Sets the channel in store pickup shipping group type.
	 *
	 * @param pChannelInStorePickupShippingGroupType the new channel in store pickup shipping group type
	 */
	public void setChannelInStorePickupShippingGroupType(String pChannelInStorePickupShippingGroupType) {
		mChannelInStorePickupShippingGroupType = pChannelInStorePickupShippingGroupType;
	}

	/**
	 * Gets the channel hardgood shipping group type.
	 *
	 * @return the channel hardgood shipping group type
	 */
	public String getChannelHardgoodShippingGroupType() {
		return mChannelHardgoodShippingGroupType;
	}

	/**
	 * Sets the channel hardgood shipping group type.
	 *
	 * @param pChannelHardgoodShippingGroupType the new channel hardgood shipping group type
	 */
	public void setChannelHardgoodShippingGroupType(String pChannelHardgoodShippingGroupType) {
		mChannelHardgoodShippingGroupType = pChannelHardgoodShippingGroupType;
	}
	
	/**
	 * Gets the donation commerce item class type.
	 *
	 * @return the donation commerce item class type
	 */
	public String getDonationCommerceItemClassType() {
		return mDonationCommerceItemClassType;
	}
	
	/**
	 * Sets the donation commerce item class type.
	 *
	 * @param pDonationCommerceItemClassType the new donation commerce item class type
	 */
	public void setDonationCommerceItemClassType(
			String pDonationCommerceItemClassType) {
		mDonationCommerceItemClassType = pDonationCommerceItemClassType;
	}
	
	/** property to hold mInStorePickUpProperties. */
	private Set<String> mInStorePickUpProperties = new HashSet<String>();
	
	/**
	 * Gets the in store pick up properties.
	 *
	 * @return the inStorePickUpProperties
	 */
	public Set<String> getInStorePickUpProperties() {
		return mInStorePickUpProperties;
	}
	
	/**
	 * Sets the in store pick up properties.
	 *
	 * @param pInStorePickUpProperties the inStorePickUpProperties to set
	 */
	public void setInStorePickUpProperties(Set<String> pInStorePickUpProperties) {
		mInStorePickUpProperties = pInStorePickUpProperties;
	}
	
    /**
     * Gets the pricing tools.
     *
     * @return the pricingTools
     */
    public TRUPricingTools getPricingTools() {
        return mPricingTools;
    }

    /**
     * Sets the pricing tools.
     *
     * @param pPricingTools the new pricing tools
     */
    public void setPricingTools(TRUPricingTools pPricingTools) {
        mPricingTools = pPricingTools;
    }

	/**
	 * Gets the in store pickup shipping group type.
	 *
	 * @return the in store pickup shipping group type
	 */
	public String getInStorePickupShippingGroupType() {
		return mInStorePickupShippingGroupType;
	}

	/**
	 * Sets the in store pickup shipping group type.
	 *
	 * @param pInStorePickupShippingGroupType the new in store pickup shipping group type
	 */
	public void setInStorePickupShippingGroupType(
			String pInStorePickupShippingGroupType) {
		mInStorePickupShippingGroupType = pInStorePickupShippingGroupType;
	}

	/**
	 * Gets the ship item rel item descriptor name.
	 *
	 * @return the ship item rel item descriptor name
	 */
	public String getShipItemRelItemDescriptorName() {
		return mShipItemRelItemDescriptorName;
	}

	/**
	 * Sets the ship item rel item descriptor name.
	 *
	 * @param pShipItemRelItemDescriptorName the new ship item rel item descriptor name
	 */
	public void setShipItemRelItemDescriptorName(
			String pShipItemRelItemDescriptorName) {
		mShipItemRelItemDescriptorName = pShipItemRelItemDescriptorName;
	}

	/**
	 * Gets the SiteContextManager.
	 *
	 * @return the SiteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * Sets the SiteContextManager.
	 *
	 * @param pSiteContextManager the SiteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		this.mSiteContextManager = pSiteContextManager;
	}
	
	/**
	 * @param pSrcSGCIRel - SrcSGCIRel
	 * @param pDestSGCIRel - DestSGCIRel
	 * @throws CommerceException - CommerceException
	 */
	public void copyShippingRelationShip(ShippingGroupCommerceItemRelationship pSrcSGCIRel, 
			ShippingGroupCommerceItemRelationship pDestSGCIRel) throws CommerceException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderTools::@method::copyShippingRelationShip() : BEGIN");
			}
		try {
			if (pSrcSGCIRel != null && pDestSGCIRel != null) {
				TRUBppItemInfo srcBppItemInfo = ((TRUShippingGroupCommerceItemRelationship) pSrcSGCIRel).getBppItemInfo();
				TRUBppItemInfo destBppItemInfo = ((TRUShippingGroupCommerceItemRelationship) pDestSGCIRel).getBppItemInfo();
				//If src null and dest is not null==>Copy the dest value into SRC
				if(srcBppItemInfo.getId()==null && destBppItemInfo.getId()!=null){
					MutableRepository orderRepository = (MutableRepository) getOrderRepository();
					MutableRepositoryItem bppItemInfoForAdd = orderRepository.createItem(getBppItemInfoDescriptorName());					
					destBppItemInfo = new TRUBppItemInfo();					
					destBppItemInfo.setId(bppItemInfoForAdd.getRepositoryId());														
					if (destBppItemInfo instanceof ChangedProperties) {
						((ChangedProperties) destBppItemInfo).setSaveAllProperties(Boolean.TRUE);
						((ChangedProperties) destBppItemInfo).setRepositoryItem(bppItemInfoForAdd);
					}					
					destBppItemInfo.setBppItemId(destBppItemInfo.getBppItemId());
					destBppItemInfo.setBppSkuId(destBppItemInfo.getBppSkuId());
					destBppItemInfo.setBppPrice(destBppItemInfo.getBppPrice());	
					((TRUShippingGroupCommerceItemRelationship) pSrcSGCIRel).setBppItemInfo(destBppItemInfo);
				}else if(srcBppItemInfo.getId()!=null && destBppItemInfo.getId()==null){					
					MutableRepository orderRepository = (MutableRepository) getOrderRepository();
					MutableRepositoryItem bppItemInfoForAdd = orderRepository.createItem(getBppItemInfoDescriptorName());					
					TRUBppItemInfo destBppItemInfo1 = new TRUBppItemInfo();					
					destBppItemInfo1.setId(bppItemInfoForAdd.getRepositoryId());														
					if (destBppItemInfo instanceof ChangedProperties) {
						((ChangedProperties) destBppItemInfo1).setSaveAllProperties(Boolean.TRUE);
						((ChangedProperties) destBppItemInfo1).setRepositoryItem(bppItemInfoForAdd);
					}					
					destBppItemInfo1.setBppItemId(srcBppItemInfo.getBppItemId());
					destBppItemInfo1.setBppSkuId(srcBppItemInfo.getBppSkuId());
					destBppItemInfo1.setBppPrice(srcBppItemInfo.getBppPrice());	
					((TRUShippingGroupCommerceItemRelationship) pDestSGCIRel).setBppItemInfo(destBppItemInfo1);									
				}else if(srcBppItemInfo.getId()!=null && destBppItemInfo.getId()!=null){
					((TRUShippingGroupCommerceItemRelationship) pSrcSGCIRel).setBppItemInfo(destBppItemInfo);
				}
			}		
			List<String> copyProperties = getConfiguration().getCopyShipItemRelProperties();		
			DynamicBeanInfo destBeanInfo = DynamicBeans.getBeanInfo(pDestSGCIRel);
			for (String copyProperty : copyProperties) {
				if ((!(destBeanInfo.hasProperty(copyProperty)))){
					continue;
				}
				Object value = DynamicBeans.getPropertyValue(pSrcSGCIRel, copyProperty);
				if (value != null) {
					DynamicBeans.setPropertyValue(pDestSGCIRel, copyProperty, value);
				}
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError("RepositoryException occured in TRUOrderTools.copyShippingRelationShip()", e);
			}
			throw new CommerceException(e);
		}catch (IntrospectionException e) {
			if (isLoggingError()) {
			vlogError("IntrospectionException occured in TRUOrderTools.copyShippingRelationShip()",e);
			}
			throw new CommerceException(e);
		}  catch (PropertyNotFoundException e) {
			if (isLoggingError()) {
			vlogError("PropertyNotFoundException occured in TRUOrderTools.copyShippingRelationShip()",e);
			}
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUOrderTools::@method::copyShippingRelationShip() : END");
				}
			throw new CommerceException(e);
		}
	}
	
	/**
	 * @return PayeezyEnabled
	 */
	public boolean isPayeezyEnabled(){
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::isPayeezyEnabled() : BEGIN");
			}
		boolean isPayeezyEnabled = Boolean.FALSE;
		if(SiteContextManager.getCurrentSite() != null){
			Site site=SiteContextManager.getCurrentSite();
			Object isPayeezyEnabledVal = site.getPropertyValue(getCommercePropertyManager().getEnablePayeezyPropertyName());
			if(isPayeezyEnabledVal != null && (boolean)isPayeezyEnabledVal){
				isPayeezyEnabled = Boolean.TRUE;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::isPayeezyEnabled() : END");
			}
		return isPayeezyEnabled;
	}
	
	/**
	 * @param pOrderId - pOrderId
	 * @param pItemDescriptorName - pItemDescriptorName
	 * @param pCreatePropertyNames - pCreatePropertyNames
	 * @param pCreatePropertyValues - pCreatePropertyValues
	 * @param pUpdatePropertyNames - pUpdatePropertyNames
	 * @param pUpdatePropertyValues - pUpdatePropertyValues
	 * @param pItemId - pItemId
	 * @return item - item
	 * @throws RepositoryException - RepositoryException
	 */
	public RepositoryItem createUpdateFailedRecord(String pOrderId, String pItemDescriptorName, String[] pCreatePropertyNames,
			Object[] pCreatePropertyValues,String[] pUpdatePropertyNames, Object[] pUpdatePropertyValues, String pItemId) throws RepositoryException{
		
		if(isLoggingDebug()){
			vlogDebug("TRUInventoryManager.createUpdateFailedRecord()methods.STARTS");
		}
		
		RepositoryItem item = null;
		RepositoryItem items[] = null;
		
		if ( getOrderRepository() != null && !StringUtils.isBlank(pItemDescriptorName)){
			if (!StringUtils.isBlank(pItemId)) {
				//fetches the failedReverseAdjustInventory item whose repository id matching with 'pItemId'
				item = getOrderRepository().getItem(pItemId, pItemDescriptorName);
			} else if(pOrderId != null){
				items = fetchItems(pItemDescriptorName,new String[]{TRUPaymentConstants.ORDER_ID},new Object[]{pOrderId}, Boolean.TRUE);
			}
			if(items != null && items.length > TRUPaymentConstants.NUMERIC_ZERO){					
				item = items[TRUPaymentConstants.NUMERIC_ZERO];
			}
			if(item != null && pUpdatePropertyNames  != null && pUpdatePropertyValues != null) {
				
				item = updateRecord(pItemDescriptorName,item.getRepositoryId(),pUpdatePropertyNames,pUpdatePropertyValues);
			}else 
				if(pCreatePropertyNames!= null && pCreatePropertyValues != null && item == null){
				
				item = createRecord(pItemDescriptorName,pCreatePropertyNames,pCreatePropertyValues);
			}
		}else {
			if ( isLoggingDebug() ) {
				vlogDebug(" Repository is not available");
			}
			throw new RepositoryException(TRUPaymentConstants.TRU_ERROR_INVALID_INFO);
		}
		if(isLoggingDebug()){
			vlogDebug("TRUOrderTools.createUpdateFailedRecord()methods.ENDS");
		}
		return item;
	}
	/**
	 * This method will create the item in the repository .
	 * @param pItemDescriptorName  -- item descriptor name of the item
	 * @param pPropertyNames -- array of properties names
	 * @param pPropertyValues -- array of properties values
	 * @throws RepositoryException --  this exception is thrown for any problems
	 * @return item -- repository item of the created one
	 */
	protected RepositoryItem createRecord(String pItemDescriptorName,String[] pPropertyNames,Object[] pPropertyValues)
					throws RepositoryException{
		if ( isLoggingDebug() ) {
			vlogDebug("starts of createRecord method in  TRUOrderTools");
		}
		RepositoryItem item = null;
		MutableRepositoryItem mutItem = null;
		if ( getOrderRepository() != null && !StringUtils.isBlank(pItemDescriptorName)  && pPropertyNames != null && pPropertyValues != null){
		  
			mutItem = ((MutableRepository)getOrderRepository()).createItem(pItemDescriptorName);
			setPropertyValues(mutItem, pPropertyNames, pPropertyValues);
			item = ((MutableRepository)getOrderRepository()).addItem(mutItem);
			
			if ( isLoggingDebug() ) {
				vlogDebug(" successfully created the item in the Repository "+pItemDescriptorName);
				vlogDebug(" Created Repository id is "+item.getRepositoryId());
			}
		}else {
			if ( isLoggingDebug() ) {
				vlogDebug(" Repository is not available");
			}
		}
		if ( isLoggingDebug() ) {
			vlogDebug("ends of createRecord method in  TRUOrderTools");
		}
		return item;
	}
	
	/**
	 * This method will update the item in the repository.
	 * @param pItemDescriptorName  -- item descriptor name of the item
	 * @param pId -- repository item id
	 * @param pPropertyNames -- array of properties names
	 * @param pPropertyValues -- array of properties values
	 * @throws RepositoryException --  this exception is thrown for any problems
	 * @return repository item of the created one
	 */
	protected RepositoryItem updateRecord(String pItemDescriptorName,String pId,String[] pPropertyNames,Object[] pPropertyValues)
		throws RepositoryException{
		if ( isLoggingDebug() ) {
			vlogDebug("starts of updateRecord method in  TRUOrderTools");
		}
		RepositoryItem item = null;
		MutableRepositoryItem mutItem = null;
		if ( getOrderRepository() != null && !StringUtils.isBlank(pItemDescriptorName) 
				&& pPropertyNames != null && pPropertyValues != null){
		
			   mutItem = ((MutableRepository)getOrderRepository()).getItemForUpdate(pId,pItemDescriptorName);
			   setPropertyValues(mutItem, pPropertyNames, pPropertyValues);
			   ((MutableRepository)getOrderRepository()).updateItem(mutItem);
			    item = (mutItem);
			    
			    if ( isLoggingDebug() ) {
					vlogDebug(" successfully updated the item in the Repository "+pItemDescriptorName);
					vlogDebug("  updated Repository id is  "+item.getRepositoryId());
				}
		}else {
			if ( isLoggingDebug() ) {
				vlogDebug(" Repository is not available");
			}
			throw new RepositoryException(TRUPaymentConstants.TRU_ERROR_INVALID_INFO);
		}
		if(isLoggingDebug()){
			vlogDebug("TRUInventoryManager.createRecord()methods.ENDS");
		}
		return item;
	}
	
	/**
	 * @param pItem - pItem
	 * @param pPropertyNames - pPropertyNames
	 * @param pPropertyValues - pPropertyValues
	 */
	private void setPropertyValues(MutableRepositoryItem pItem,String[] pPropertyNames,Object[] pPropertyValues){
		 if ( isLoggingDebug() ) {
				vlogDebug("TRUInventoryManager.setPropertyValues()method.STARTS");
			}
			if ( pItem != null &&  pPropertyNames != null && pPropertyValues != null  && pPropertyNames.length == pPropertyValues.length) {
				for ( int i = TRUPaymentConstants.NUMERIC_ZERO; i <pPropertyNames.length ; i++  ) {
					if (! pPropertyNames[i].equalsIgnoreCase(TRUPaymentConstants.ID)) {
						if ( isLoggingDebug() ) {
							vlogDebug("Property Name = {0}, Property Value = {1} ", pPropertyNames[i],pPropertyValues[i]);
						}
						pItem.setPropertyValue(pPropertyNames[i], pPropertyValues[i]);
					}
				}
			}
			if ( isLoggingDebug() ) {
				vlogDebug("TRUInventoryManager.setPropertyValues()method.ENDS");
			}
			
		}
	
	/**
	 * @param pItemDescriptorName - pItemDescriptorName
	 * @param pPropertyNames - pPropertyNames
	 * @param pPropertyValues - pPropertyValues
	 * @param pAnd - pAnd
	 * @return items - items
	 * @throws RepositoryException - RepositoryException
	 */
	protected RepositoryItem[] fetchItems(String pItemDescriptorName,String[] pPropertyNames,Object[] pPropertyValues,boolean pAnd)
			throws RepositoryException{
	if ( isLoggingDebug() ) {
		vlogDebug("TRUInventoryManager.fetchItems()method.STARTS");
	}
	RepositoryItem items [] = null;
	if ( getOrderRepository() != null  && !StringUtils.isBlank(pItemDescriptorName) &&  pPropertyNames != null && pPropertyValues != null && 
				pPropertyNames.length == pPropertyValues.length) {
		try {
			RepositoryItemDescriptor itemDesc = getOrderRepository().getItemDescriptor(pItemDescriptorName);
			QueryBuilder queryBuilder = itemDesc.getRepositoryView().getQueryBuilder();
			QueryExpression propertyExpression = null;
			QueryExpression constantExpression  = null;
			List<Query> queries = new ArrayList<Query>();
			Query orginalQuery = null;
			for ( int i = TRUPaymentConstants.NUMERIC_ZERO; i < pPropertyNames.length ; i++ ) {
				// create a QueryExpression that represents the property 
				propertyExpression = queryBuilder.createPropertyQueryExpression(pPropertyNames[i]);
				if ( isLoggingDebug() ) {
					vlogDebug("property name = {0}", pPropertyNames[i]);
				}
				if(pPropertyValues[i] != null){
					 // create a QueryExpression that represents the constant 
					  constantExpression = queryBuilder.createConstantQueryExpression(pPropertyValues[i]);					
					if ( isLoggingDebug() ) {
						vlogDebug("property value = {0}", pPropertyValues[i]);
					}
					queries.add(queryBuilder.createComparisonQuery(propertyExpression, constantExpression, QueryBuilder.EQUALS));
				}else{
					if(pAnd){
					 queries.add(queryBuilder.createIsNullQuery(propertyExpression));
					}
				}
			}
			if ( isLoggingDebug() ) {
				vlogDebug("Number of Queries build are "+ queries.size());
			}
			if ( queries .size() > TRUPaymentConstants.NUMERIC_ONE) {
				orginalQuery = pAnd ? queryBuilder.createAndQuery(
						queries.toArray(new Query[TRUPaymentConstants.NUMERIC_ZERO])) :queryBuilder.createOrQuery(queries.toArray(new Query[0])) ;
			   
			} else {
				orginalQuery = queries.get(0);
			}
			if ( isLoggingDebug() ) {
				vlogDebug("orginalQuery is {0}",orginalQuery);
			}
			if(orginalQuery!=null){
				items = itemDesc.getRepositoryView().executeQuery(orginalQuery);
			}
			if ( isLoggingDebug() ) {
				vlogDebug("Number of items fetched are {0}", (items != null ?items.length: null));
			}				
		} catch (RepositoryException repExc) {				
			if ( isLoggingError() ) {
				logError("RepositoryException while fetch the items TRUOrderTools.fetchItems() ",repExc);
			}
		}

	} else {
		if ( isLoggingDebug() ) {
			vlogDebug("Either Repository or required values are not available");
		}
	}
	if ( isLoggingDebug() ) {
		vlogDebug("TRUInventoryManager.fetchItems()method.ENDS");
	}
	return items;
	
}
	/**
	 * Gets the purge oos orders.
	 * 
	 * @throws RepositoryException the repository exception
	 */
	public void getPurgeOOSOrders() throws RepositoryException{
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderTools::@method::getPurgeOOSOrders() : BEGIN");
		}
		RepositoryView view = getOrderRepository().getView(TRUCommerceConstants.ORDER);
		QueryBuilder queryBuilder = view.getQueryBuilder();
		
		QueryExpression state = queryBuilder.createPropertyQueryExpression(TRUCommerceConstants.STATE);
		QueryExpression submitted = queryBuilder.createConstantQueryExpression(TRUCommerceConstants.SUBMITTED);
		
		QueryExpression submittedDate = queryBuilder.createPropertyQueryExpression(TRUCommerceConstants.SUBMITTED_DATE);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -TRUCommerceConstants.INT_ONE);
		Timestamp oneDayEarlier =new Timestamp(cal.getTimeInMillis()); 
		QueryExpression yesterdayDate = queryBuilder.createConstantQueryExpression(oneDayEarlier);
		
		QueryExpression outOfStockItemsExp = queryBuilder.createPropertyQueryExpression(TRUCommerceConstants.OUT_OF_STOCK_ITEMS);
		Query isEmptyOOSItems = queryBuilder.createIsNullQuery(outOfStockItemsExp);
		Query isNotEmptyOOSItems = queryBuilder.createNotQuery(isEmptyOOSItems);
		
		Query submittedStatusMatch = queryBuilder.createComparisonQuery(state, submitted, QueryBuilder.EQUALS);
		Query dateMatch = queryBuilder.createComparisonQuery(yesterdayDate, submittedDate, QueryBuilder.GREATER_THAN);
		
		Query[] pieces = { submittedStatusMatch, dateMatch, isNotEmptyOOSItems };
		Query andStatusQuery = queryBuilder.createAndQuery(pieces);
		RepositoryItem[] orders = view.executeQuery(andStatusQuery);
		
		for (RepositoryItem order : orders) {
			MutableRepositoryItem mutableOrder = (MutableRepositoryItem)order;
			mutableOrder.setPropertyValue(getCommercePropertyManager().getOutOfStockItemsPropertyName(), null);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderTools::@method::getPurgeOOSOrders() : END");
		}
	}
	
	/**
	 * Gets the layawayOrderType.
	 *
	 * @return the mLayawayOrderType
	 */
	public String getLayawayOrderType() {
		return mLayawayOrderType;
	}

	/**
	 * Sets the layawayOrderType.
	 *
	 * @param pLayawayOrderType the layawayOrderType to set
	 */
	public void setLayawayOrderType(String pLayawayOrderType) {
		this.mLayawayOrderType = pLayawayOrderType;
	}

	/**
	 * @return the mPropertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager the mPropertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		this.mPropertyManager = pPropertyManager;
	}

	/**
	 * @return the mCatalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * @param pCatalogProperties the mCatalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		this.mCatalogProperties = pCatalogProperties;
	}

	/**
	 * This method returns the groupDiscountTemplate value
	 *
	 * @return the groupDiscountTemplate
	 */
	public String getGroupDiscountTemplate() {
		return mGroupDiscountTemplate;
	}

	/**
	 * This method sets the groupDiscountTemplate with parameter value pGroupDiscountTemplate
	 *
	 * @param pGroupDiscountTemplate the groupDiscountTemplate to set
	 */
	public void setGroupDiscountTemplate(String pGroupDiscountTemplate) {
		mGroupDiscountTemplate = pGroupDiscountTemplate;
	}

	/**
	 * Update billing address is same.
	 *
	 * @param pAddress the address
	 * @param pBillingAddressIsSame billingaddress
	 */
	public void updateBillingAddressIsSame(RepositoryItem pAddress, boolean pBillingAddressIsSame) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUOrderTools::@method:: updateBillingAddressIsSame()");
		}
		((MutableRepositoryItem)pAddress).setPropertyValue(getCommercePropertyManager().getBillingAddressIsSame(), pBillingAddressIsSame);
	}
	
	/**
     * Gets the failed reverse authorization details.
     *
     * @return the failed reverse authorization details
     * @throws RepositoryException the repository exception
     */
	public RepositoryItem[] getFailedUpdateRegistryItems() throws RepositoryException{
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::getFailedUpdateRegistryItems() : BEGIN");
		}
		RepositoryView view = getOrderRepository().getView(getCommercePropertyManager().getRegistryFailedItemPropName());
		QueryBuilder queryBuilder = view.getQueryBuilder();

		QueryExpression exp1 = queryBuilder.createPropertyQueryExpression(getCommercePropertyManager().getStatusPropName());
		QueryExpression exp2 = queryBuilder.createConstantQueryExpression(TRUConstants._0);
		Query exp2Query = queryBuilder.createComparisonQuery(exp1, exp2, QueryBuilder.EQUALS);
		QueryExpression exp3 = queryBuilder.createConstantQueryExpression(TRUConstants._1);
		Query exp3Query = queryBuilder.createComparisonQuery(exp1, exp3, QueryBuilder.EQUALS);
		QueryExpression exp4 = queryBuilder.createConstantQueryExpression(TRUConstants.INTEGER_NUMBER_TWO);
		Query exp4Query = queryBuilder.createComparisonQuery(exp1, exp4, QueryBuilder.EQUALS);
		
		Query[] paramArrayOfQuery = {exp2Query, exp3Query, exp4Query};
		Query createOrQuery = queryBuilder.createOrQuery(paramArrayOfQuery);
		RepositoryItem[] results = view.executeQuery(createOrQuery);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUOrderTools::@method::getFailedUpdateRegistryItems() : END");
		}
		return results;
	}
	
	/**
	 * Method is used to get the eligible items for promotion.
	 * 
	 * @param pExcludedSkuItem - List of SKU repository Item.
	 * @param pIncludedSkuItem - List of SKU repository Item.
	 * @param pBulkSkuItems - - List of qualify SKU ids.
	 * @param pExcludedBulkSkuIds - List of excluded SKU ids.
	 * @param pOrder - Order Object
	 * @return List - List of eligible commerce items.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<CommerceItem> getEligibleitemForOrderDis(List<RepositoryItem> pExcludedSkuItem, 
			List<RepositoryItem> pIncludedSkuItem, List<String> pBulkSkuItems,
			List<String> pExcludedBulkSkuIds, Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOrderTools  method: getEligibleitemForOrderDis]");
			vlogDebug("pExcludedSkuItem : {0} pIncludedSkuItem : {1}", pExcludedSkuItem,pIncludedSkuItem);
			vlogDebug("pBulkSkuItems : {0} pOrder : {1} pExcludedBulkSkuIds : {2}", pBulkSkuItems,pOrder,pExcludedBulkSkuIds);
		}
		if(pOrder == null){
			return null;
		}
		List<CommerceItem> commerceItems = pOrder.getCommerceItems();
		if(commerceItems == null || commerceItems.isEmpty()){
			return null;
		}
		List<CommerceItem> listOfItemsEligibleForDis = new ArrayList<CommerceItem>();
		for(CommerceItem cItem : commerceItems){
			if(cItem instanceof TRUGiftWrapCommerceItem || cItem instanceof TRUDonationCommerceItem){
				continue;
			}
			listOfItemsEligibleForDis.add(cItem);
		}
		if((pIncludedSkuItem == null || pIncludedSkuItem.isEmpty()) && (pExcludedSkuItem == null || pExcludedSkuItem.isEmpty()) &&
				(pBulkSkuItems == null || pBulkSkuItems.isEmpty()) && (pExcludedBulkSkuIds == null || pExcludedBulkSkuIds.isEmpty())){
			if (isLoggingDebug()) {
				vlogDebug("listOfItemsEligibleForDis : {0}", listOfItemsEligibleForDis);
			}
			return listOfItemsEligibleForDis;
		}
		if((pIncludedSkuItem == null|| pIncludedSkuItem.isEmpty()) && 
	    		  (pBulkSkuItems == null || pBulkSkuItems.isEmpty())){
			List<CommerceItem> itemsListModified = new ArrayList<CommerceItem>(listOfItemsEligibleForDis);
			Iterator itemsListIterator = itemsListModified.iterator();
			while (itemsListIterator.hasNext()) {
				CommerceItem item = (CommerceItem)itemsListIterator.next();
				if(pExcludedSkuItem != null && pExcludedSkuItem.contains((RepositoryItem)item.getAuxiliaryData().getCatalogRef())){
					itemsListIterator.remove();
					continue;
				}
				if(pExcludedBulkSkuIds != null && pExcludedBulkSkuIds.contains(item.getCatalogRefId())){
					itemsListIterator.remove();
					continue;
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("itemsListModified : {0}", itemsListModified);
			}
			return itemsListModified;
	    }
		List<CommerceItem> finalCommerceItemsList = new ArrayList<CommerceItem>();
		for(CommerceItem lCommItem : commerceItems){
			if(lCommItem instanceof TRUGiftWrapCommerceItem || lCommItem instanceof TRUDonationCommerceItem){
				continue;
			}
			// Checking condition if commerce is qualified or excluded for Promotion.
			if(pIncludedSkuItem != null && pIncludedSkuItem.contains((RepositoryItem)lCommItem.getAuxiliaryData().getCatalogRef())){
				finalCommerceItemsList.add(lCommItem);
			}
			if(pBulkSkuItems != null && pBulkSkuItems.contains(lCommItem.getCatalogRefId()) &&
					!finalCommerceItemsList.contains(lCommItem)){
				finalCommerceItemsList.add(lCommItem);
			}
			if(pExcludedSkuItem != null && pExcludedSkuItem.contains((RepositoryItem)lCommItem.getAuxiliaryData().getCatalogRef()) &&
					finalCommerceItemsList.contains(lCommItem)){
				finalCommerceItemsList.remove(lCommItem);
			}
			if(pExcludedBulkSkuIds != null && pExcludedBulkSkuIds.contains(lCommItem.getCatalogRefId()) &&
					finalCommerceItemsList.contains(lCommItem)){
				finalCommerceItemsList.remove(lCommItem);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("finalCommerceItemsList : {0}", finalCommerceItemsList);
			logDebug("Exit from [Class: TRUOrderTools  method: calculateOrderDiscountShare]");
		}
		return finalCommerceItemsList;
	}
	
	/**
	 * Method is used to get the eligible items for promotion.
	 * 
	 * @param pCommerceItem - List of Commerce Items.
	 * @return List - List of eligible commerce items.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<CommerceItem> getItemEligibleForDiscount(
			List<CommerceItem> pCommerceItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOrderTools  method: getItemEligibleForDiscount]");
			vlogDebug("pCommerceItem : {0}", pCommerceItem);
		}
		if(pCommerceItem == null || pCommerceItem.isEmpty()){
			return null;
		}
		List<CommerceItem> itemsListModified = new ArrayList(pCommerceItem);
		Iterator itemsListIterator = itemsListModified.iterator();
		while (itemsListIterator.hasNext()) {
			CommerceItem item = (CommerceItem)itemsListIterator.next();
		    if(item instanceof TRUDonationCommerceItem || item instanceof TRUGiftWrapCommerceItem){
		    	itemsListIterator.remove();
		    }
		}
		if (isLoggingDebug()) {
			vlogDebug("itemsListModified : {0}", itemsListModified);
			logDebug("Exit from [Class: TRUOrderTools  method: getItemEligibleForDiscount]");
		}
		return itemsListModified;
	}
	
	/**
	 * Method is used to get the order amount for eligible items.
	 * 
	 * @param pCommerceItem - List of commerce items.
	 * @return double - Order Amount after removing donation items amount.
	 */
	@SuppressWarnings("rawtypes")
	public double getOrderToatal(List<CommerceItem> pCommerceItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOrderTools  method: getOrderToatal]");
			vlogDebug("pCommerceItem : {0}", pCommerceItem);
		}
		double orderAmount = TRUConstants.DOUBLE_ZERO;
		if(pCommerceItem == null || pCommerceItem.isEmpty()){
			return orderAmount;
		}
		// Iterating commerce items and removing the donation item amount.
		TRUItemPriceInfo itemPriceInfo = null;
		for(CommerceItem cItem : pCommerceItem){
			if(cItem instanceof TRUDonationCommerceItem || cItem instanceof TRUGiftWrapCommerceItem){
				continue;
			}
			double discountShare = TRUConstants.DOUBLE_ZERO;
			itemPriceInfo = (TRUItemPriceInfo) cItem.getPriceInfo();
			Map truOrderDiscountShare = itemPriceInfo.getTruOrderDiscountShare();
			if(truOrderDiscountShare != null && !truOrderDiscountShare.isEmpty()){
				Iterator it = truOrderDiscountShare.entrySet().iterator();
		    	while (it.hasNext()) {
		    		Map.Entry entry  = (Map.Entry)it.next();
					if(entry.getValue() instanceof Double){
						discountShare += ((Double)entry.getValue());
					}
				}
			}
			Map truItemDiscountShare = itemPriceInfo.getTruItemDiscountShare();
			if(truItemDiscountShare != null && !truItemDiscountShare.isEmpty()){
				String value = null;
				Iterator it = truItemDiscountShare.entrySet().iterator();
		    	while (it.hasNext()) {
		    		Map.Entry entry  = (Map.Entry)it.next();
					if(entry.getValue() instanceof String){
						value = (String) entry.getValue();
						if(StringUtils.isNotBlank(value)){
							discountShare += Double.valueOf((value.split(TRUConstants.PIPE_DELIMETER))[0]);
						}
					}
				}
			}
			orderAmount +=(itemPriceInfo.getSalePrice()*cItem.getQuantity())-discountShare;
		}
		// END
		if (isLoggingDebug()) {
			vlogDebug("Order Discount to share : {0}", orderAmount);
			logDebug("Exit from [Class: TRUOrderTools  method: getOrderToatal]");
		}
		return orderAmount;
	}
	
	/**
	 * @param pCItemRel - TRUShippingGroupCommerceItemRelationship
	 * @return bppSkuID - String 
	 */ 
	public String getBppSkuId(TRUShippingGroupCommerceItemRelationship pCItemRel)	{
		TRUCommercePropertyManager commercePropertyManager = getCommercePropertyManager();
		String bppSkuID = TRUConstants.EMPTY_STRING;
		RepositoryItem bppItem = (RepositoryItem) pCItemRel.getPropertyValue(commercePropertyManager.getBppItemInfoPropertyName());
		if(bppItem != null){
		bppSkuID = (String) bppItem.getPropertyValue(commercePropertyManager.getBppSkuIdPropertyName());
		}
		return bppSkuID;
	}
	
	/**
	 * Method to get the excluded items amount.
	 * @param pOrder - Order Object
	 * @param pShippingGroup - Object of ShippingGroup
	 * @param pQualifiedSKUforPromo - List of qualified sku repository items.
	 * @param pQualifyBulkSkuIds - List of qualified sku ids.
	 * @param pExcludedSKUforPromo - List of excluded skus repository items.
	 * @param pExcludedBulkSkuIds - List of excluded sku ids.
	 * @return Double - Excluded items amount if any.
	 */
	@SuppressWarnings("unchecked")
	public Double getNonEligibleItemsAmount(Order pOrder, ShippingGroup pShippingGroup,
			List<RepositoryItem> pQualifiedSKUforPromo,
			List<String> pQualifyBulkSkuIds,
			List<RepositoryItem> pExcludedSKUforPromo, List<String> pExcludedBulkSkuIds) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOrderTools  method: getNonEligibleItemsAmount]");
			vlogDebug("pShippingGroup : {0} pQualifiedSKUforPromo : {1} pOrder : {2}", pShippingGroup,pQualifiedSKUforPromo,pOrder);
			vlogDebug("pQualifyBulkSkuIds : {0} pExcludedSKUforPromo : {1} pExcludedBulkSkuIds {2}",
					pQualifyBulkSkuIds,pExcludedSKUforPromo,pExcludedBulkSkuIds);
		}
		Double nonDiscountableAmt = Double.valueOf(TRUConstants.DOUBLE_ZERO);
		if(pOrder == null){
			return nonDiscountableAmt;
		}
		List<CommerceItem> commerceItems = pOrder.getCommerceItems();
		if(commerceItems == null || commerceItems.isEmpty()){
			return nonDiscountableAmt;
		}
		String skuId = null;
		ItemPriceInfo itemPriceInfo = null;
		for(CommerceItem cItem : commerceItems){
			if(cItem == null){
				continue;
			}
			itemPriceInfo = cItem.getPriceInfo();
			if(itemPriceInfo == null){
				continue;
			}
			skuId = cItem.getCatalogRefId();
			if(cItem instanceof TRUDonationCommerceItem){
				nonDiscountableAmt = Double.valueOf(nonDiscountableAmt.doubleValue() + itemPriceInfo.getAmount());
		    	continue;
		    }
		    if(pExcludedSKUforPromo != null && pExcludedSKUforPromo.contains(cItem.getAuxiliaryData().getCatalogRef())){
		    	nonDiscountableAmt = Double.valueOf(nonDiscountableAmt.doubleValue() + itemPriceInfo.getAmount());
		    	continue;
		    }
			if (pExcludedBulkSkuIds != null && pExcludedBulkSkuIds.contains(skuId)) {
				nonDiscountableAmt = Double.valueOf(nonDiscountableAmt.doubleValue() + itemPriceInfo.getAmount());
				continue;
			}
			if ((pQualifiedSKUforPromo == null || pQualifiedSKUforPromo.isEmpty()) &&
					(pQualifyBulkSkuIds == null || pQualifyBulkSkuIds.isEmpty())) {
				continue;
			}
			if ((pQualifiedSKUforPromo == null || !pQualifiedSKUforPromo.contains(cItem.getAuxiliaryData().getCatalogRef()))
					&& (pQualifyBulkSkuIds == null || !pQualifyBulkSkuIds.contains(skuId))) {
				nonDiscountableAmt = Double.valueOf(nonDiscountableAmt.doubleValue() + itemPriceInfo.getAmount());
			}
		}
		TRUPricingTools pricingTools = getPricingTools();
		// Calling method to get the BPP Item amount.
		nonDiscountableAmt = nonDiscountableAmt+pricingTools.getBPPItemsAmount(pOrder);
		if (isLoggingDebug()) {
			vlogDebug("nonDiscountableAmt : {0}", nonDiscountableAmt);
			logDebug("Exit from [Class: TRUOrderTools  method: getNonEligibleItemsAmount]");
		}
		return nonDiscountableAmt;
	}

	/**
	 * Method to get the Incomplete Order from Profile.
	 * 
	 * @param pProfile - RepositoryItem
	 * @return RepositoryItem - Order Item.
	 * 
	 * @throws RepositoryException - If any
	 */
	public RepositoryItem getOrderForProfile(RepositoryItem pProfile) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOrderTools  method: getOrderForProfile]");
			vlogDebug("pDestOrder : {0}", pProfile);
		}
		RepositoryItem orderImlp = null;
		if(pProfile == null){
			return orderImlp;
		}
		Repository orderRepository = getOrderRepository();
		RepositoryView view = orderRepository.getView(getOrderItemDescriptorName());
		QueryBuilder queryBuilder = view.getQueryBuilder();
		QueryExpression constantExp = null;
		QueryExpression propertyExp = null;
		constantExp = queryBuilder.createConstantQueryExpression(pProfile.getRepositoryId());
		propertyExp = queryBuilder.createPropertyQueryExpression(getProfileTools().getProfileIdPropertyName());
		Query createComparisonQuery2 = queryBuilder.createComparisonQuery(propertyExp, constantExp, QueryBuilder.EQUALS);
		constantExp = queryBuilder.createConstantQueryExpression(TRUCommerceConstants.INCOMPLETE);
		propertyExp = queryBuilder.createPropertyQueryExpression(TRUCommerceConstants.STATE);
		Query createComparisonQuery = queryBuilder.createComparisonQuery(propertyExp, constantExp, QueryBuilder.EQUALS);
		Query[] pieces = { createComparisonQuery, createComparisonQuery2};
		Query andQueryResult = queryBuilder.createAndQuery(pieces);
		RepositoryItem[] executeQuery = view.executeQuery(andQueryResult);
		if(executeQuery != null && executeQuery.length > TRUConstants.INT_ZERO){
			orderImlp = executeQuery[TRUConstants.INT_ZERO];
		}
		if (isLoggingDebug()) {
			logDebug("Exit From [Class: TRUOrderTools  method: getOrderForProfile]");
			vlogDebug("orderImlp : {0}", orderImlp);
		}
		return orderImlp;
	}
	
	/**
	 * This method is to create the meta info when discounts applied and gift wrap selected on items.
	 *
	 * @param pMetaInfoList - Meta Infos List
	 * @param pPriceQuote - TRUItemPriceInfo
	 * @param pShipItemRel - TRUShippingGroupCommerceItemRelationship
	 * @param pFinalMap - Gift wrap details map
	 * @param pGiftWrapItemQtyTotal - Gift wrap item total quantity
	 * @throws RepositoryException - if any
	 */
	@SuppressWarnings("unchecked")
	private void createMetaInfoforGiftwrap(List<TRUCommerceItemMetaInfo> pMetaInfoList, TRUItemPriceInfo pPriceQuote, TRUShippingGroupCommerceItemRelationship pShipItemRel, Map<String, Long> pFinalMap, long pGiftWrapItemQtyTotal) throws RepositoryException{
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOrderTools  method: createMetaInfoforGiftwrap]");
			vlogDebug("pMetaInfoList : {0}  pPriceQuote : {1}  pShipItemRel : {2}  pFinalMap : {3}  pGiftWrapItemQtyTotal : {4}",pMetaInfoList, pPriceQuote, pShipItemRel, pFinalMap, pGiftWrapItemQtyTotal);
		}
		if(pFinalMap == null || pFinalMap.isEmpty()){
			return;
		}
		TRUCommercePropertyManager commerceItemPropertyManager = getCommercePropertyManager();
		String giftWrapItemId = null;
		long giftWrapItemQty = TRUConstants.LONG_ZERO;
		double diffValue = TRUConstants.DOUBLE_ZERO;
		RepositoryItem giftWrapItem = null;
		DetailedItemPriceInfo detaileditemPriceInfo = null;
		double detailedUnitPrice = TRUConstants.DOUBLE_ZERO;
		Repository orderRepository = getOrderRepository();
		long shipItemRelQty = pShipItemRel.getQuantity();
		double salePrice = pPriceQuote.getSalePrice();
		detaileditemPriceInfo = (DetailedItemPriceInfo) pPriceQuote.getCurrentPriceDetails().get(0);
		double listPrice = pPriceQuote.getListPrice();
		Set<String> giKeySet = pFinalMap.keySet();
		if(pPriceQuote.getDiscountedAmount() != TRUConstants.DOUBLE_ZERO && pFinalMap != null && !pFinalMap.isEmpty()) {
			List<PricingAdjustment> adjustments = pPriceQuote.getAdjustments();
			for (PricingAdjustment pricingAdjustment : adjustments) {
				long qty = TRUConstants._0;
				long quantity = TRUConstants.LONG_ZERO;
				int qtyToDispalyZeroPrice = TRUConstants._0;
				if (pricingAdjustment.getPricingModel() != null && !(boolean)pricingAdjustment.getPricingModel().getPropertyValue(getPricingModelProperties().getTagPromotionPropertyName())) {
					if(isLoggingDebug()){
						vlogDebug("Pricing Adjustment Promotion : {0}", pricingAdjustment.getPricingModel());
					}
					String propertyValue = (String) pricingAdjustment.getPricingModel().getPropertyValue(getPricingModelProperties().getTemplate());
					if(propertyValue.equals(getGroupDiscountTemplate())){
						detailedUnitPrice = detaileditemPriceInfo.getDetailedUnitPrice();
					}else if(propertyValue.equals(getTieredPriceBreakTemplate())){
						detailedUnitPrice = detaileditemPriceInfo.getDetailedUnitPrice();
					}else{
						detailedUnitPrice = salePrice;
					}
					long lQtyAdj = pricingAdjustment.getQuantityAdjusted();
					double lAdjustedAmount = pricingAdjustment.getTotalAdjustment();
					double adjustmentPositiveValue = -lAdjustedAmount;
					String promoId = pricingAdjustment.getPricingModel().getRepositoryId();
					if(shipItemRelQty >= pGiftWrapItemQtyTotal){
						if(isLoggingDebug()){
							vlogDebug("shipItemRelQty : {0}  pGiftWrapItemQtyTotal : {1}", shipItemRelQty, pGiftWrapItemQtyTotal);
						}
						if(adjustmentPositiveValue > salePrice){
							qtyToDispalyZeroPrice = (int) (adjustmentPositiveValue/salePrice);
							diffValue = (adjustmentPositiveValue - (salePrice*qtyToDispalyZeroPrice));
						}
						if (lQtyAdj >= pGiftWrapItemQtyTotal) {
							quantity =  TRUConstants.LONG_ZERO;
							for (Iterator<String> iterator = giKeySet.iterator(); iterator.hasNext();) {
								qty = TRUConstants.LONG_ZERO;
								giftWrapItemId = (String) iterator.next();
								giftWrapItemQty = pFinalMap.get(giftWrapItemId);
								giftWrapItem = orderRepository.getItem(giftWrapItemId, commerceItemPropertyManager.getCommerceItemPropertyName());
								if(adjustmentPositiveValue > salePrice){
									if(qtyToDispalyZeroPrice >= giftWrapItemQty){
										if (isLoggingDebug()) {
											vlogDebug("Scenario 1 with Gift wrap and discounts ::: [Adjusted quantity greater than or equals to  individual gift wrap item quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
											vlogDebug("Qunatity to display zero :: {0} Gift wrap quantity :: {1}", qtyToDispalyZeroPrice, giftWrapItemQty);
										}
										pMetaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), giftWrapItemQty, 0,listPrice * giftWrapItemQty,salePrice*giftWrapItemQty, salePrice, giftWrapItem,
												pShipItemRel.getId(), pPriceQuote, promoId));
										adjustmentPositiveValue -= salePrice*qtyToDispalyZeroPrice;
										qtyToDispalyZeroPrice -= giftWrapItemQty;
									}else{
										if(qtyToDispalyZeroPrice > TRUConstants._0){
											if (isLoggingDebug()) {
												vlogDebug("Scenario 2 with Gift wrap and discounts ::: [Adjusted quantity greater than or equals to  individual gift wrap item quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
												vlogDebug("Qunatity to display zero :: {0} Gift wrap quantity :: {1}", qtyToDispalyZeroPrice, giftWrapItemQty);
											}
											pMetaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), qtyToDispalyZeroPrice, 0,listPrice * qtyToDispalyZeroPrice,salePrice*qtyToDispalyZeroPrice, salePrice, giftWrapItem,
												pShipItemRel.getId(), pPriceQuote, promoId));
										}
										qty += TRUConstants.LONG_ONE;
										if(diffValue > salePrice){
											if (isLoggingDebug()) {
												vlogDebug("Scenario 3 with Gift wrap and discounts ::: [Adjusted quantity greater than or equals to  individual gift wrap item quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
												vlogDebug("Qunatity to display zero :: {0} Gift wrap quantity :: {1}", qtyToDispalyZeroPrice, giftWrapItemQty);
											}
											pMetaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), qty, diffValue,listPrice * qty,salePrice*qty, salePrice, giftWrapItem,
													pShipItemRel.getId(), pPriceQuote, promoId));
										}else if(diffValue == TRUConstants.DOUBLE_ZERO){
											//Do Nothing
										} else{
											if (isLoggingDebug()) {
												vlogDebug("Scenario 4 with Gift wrap and discounts ::: [Adjusted quantity greater than or equals to  individual gift wrap item quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
												vlogDebug("Qunatity to display zero :: {0} Gift wrap quantity :: {1}", qtyToDispalyZeroPrice, giftWrapItemQty);
											}
											pMetaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(),qty, salePrice-diffValue,
													listPrice*qty,salePrice*qty, salePrice, giftWrapItem, pShipItemRel.getId(), pPriceQuote, promoId));
										}
										qty +=qtyToDispalyZeroPrice;
									}
								}else{
									qty += TRUConstants.LONG_ONE;
									if(diffValue > salePrice){
										if (isLoggingDebug()) {
											vlogDebug("Scenario 5 with Gift wrap and discounts ::: [Adjusted quantity greater than or equals to  individual gift wrap item quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
											vlogDebug("Qunatity to display zero :: {0} Gift wrap quantity :: {1}", qtyToDispalyZeroPrice, giftWrapItemQty);
										}
										pMetaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), qty, diffValue,listPrice * qty,salePrice*qty, salePrice, giftWrapItem,
												pShipItemRel.getId(), pPriceQuote, promoId));
									}else if(diffValue != TRUConstants.DOUBLE_ZERO){
										if (isLoggingDebug()) {
											vlogDebug("Scenario 6 with Gift wrap and discounts ::: [Adjusted quantity greater than or equals to  individual gift wrap item quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
											vlogDebug("Qunatity to display zero :: {0} Gift wrap quantity :: {1}", qtyToDispalyZeroPrice, giftWrapItemQty);
										}
										pMetaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(),qty, salePrice-diffValue,
												listPrice*qty,salePrice*qty, salePrice, giftWrapItem, pShipItemRel.getId(), pPriceQuote, promoId));
									} else{
										if (isLoggingDebug()) {
											vlogDebug("Scenario 7 with Gift wrap and discounts ::: [Adjusted quantity greater than or equals to  individual gift wrap item quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
											vlogDebug("Qunatity to display zero :: {0} Gift wrap quantity :: {1}", qtyToDispalyZeroPrice, giftWrapItemQty);
										}
										pMetaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(),giftWrapItemQty, detailedUnitPrice*giftWrapItemQty+lAdjustedAmount,
												listPrice*giftWrapItemQty,salePrice*giftWrapItemQty, salePrice, giftWrapItem, pShipItemRel.getId(), pPriceQuote, promoId));
										lAdjustedAmount -= lAdjustedAmount;
									}
									qty +=qtyToDispalyZeroPrice;
								}
								quantity += giftWrapItemQty;
							}
							if(lQtyAdj - quantity > TRUConstants._0){
								qty += TRUConstants.LONG_ONE;
								if(diffValue > salePrice){
									if (isLoggingDebug()) {
										vlogDebug("Scenario 8 with Gift wrap and discounts ::: [Adjusted quantity greater than or equals to  individual gift wrap item quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
										vlogDebug("Qunatity to display zero :: {0} Gift wrap quantity :: {1}", qtyToDispalyZeroPrice, giftWrapItemQty);
									}
									pMetaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), qty, diffValue,listPrice * qty,salePrice*qty, salePrice, giftWrapItem,
											pShipItemRel.getId(), pPriceQuote, promoId));
									quantity += qty;
								}else if(diffValue == TRUConstants.DOUBLE_ZERO){
									//Do Nothing
								} else{
									if (isLoggingDebug()) {
										vlogDebug("Scenario 9 with Gift wrap and discounts ::: [Adjusted quantity greater than or equals to  individual gift wrap item quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
										vlogDebug("Qunatity to display zero :: {0} Gift wrap quantity :: {1}", qtyToDispalyZeroPrice, giftWrapItemQty);
									}
									pMetaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(),qty, salePrice-diffValue,
											listPrice*qty,salePrice*qty, salePrice, giftWrapItem, pShipItemRel.getId(), pPriceQuote, promoId));
									quantity += qty;
								}
							}
							if(shipItemRelQty - quantity != TRUConstants._0){
								if (isLoggingDebug()) {
									vlogDebug("Scenario 10 with Gift wrap and discounts ::: [Adjusted quantity greater than or equals to  individual gift wrap item quantity] :::  Ship Item Relationship Quantity: {0}  Sum of gift Wrap Items quantity :: {1}" ,shipItemRelQty, quantity );
								}
								pMetaInfoList.add(createMetaInfoItem(Boolean.FALSE, pShipItemRel.getCommerceItem(),shipItemRelQty - quantity, detailedUnitPrice * (shipItemRelQty - quantity),
										listPrice* (shipItemRelQty - quantity),salePrice*(shipItemRelQty - quantity), salePrice, null, pShipItemRel.getId(), pPriceQuote, null));
							}
						} else if(lQtyAdj < pGiftWrapItemQtyTotal){
							for (Iterator<String> iterator = giKeySet.iterator(); iterator.hasNext();) {
								qty = TRUConstants.LONG_ZERO;
								giftWrapItemId = (String) iterator.next();
								giftWrapItemQty = pFinalMap.get(giftWrapItemId);
								giftWrapItem = orderRepository.getItem(giftWrapItemId, commerceItemPropertyManager.getCommerceItemPropertyName());
								if(adjustmentPositiveValue > salePrice){
									if(qtyToDispalyZeroPrice >= giftWrapItemQty){
										if (isLoggingDebug()) {
											vlogDebug("Scenario 11 with Gift wrap and discounts ::: [Adjusted quantity less than  total  gift wrap items quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
											vlogDebug("Qunatity to display zero :: {0} Gift wrap quantity :: {1}", qtyToDispalyZeroPrice, giftWrapItemQty);
										}
										pMetaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), qtyToDispalyZeroPrice, 0,listPrice * qtyToDispalyZeroPrice,salePrice*qtyToDispalyZeroPrice, salePrice, giftWrapItem,
												pShipItemRel.getId(), pPriceQuote, promoId));
										qtyToDispalyZeroPrice -= giftWrapItemQty;
									}else	{
										if(qtyToDispalyZeroPrice > TRUConstants._0){
											if (isLoggingDebug()) {
												vlogDebug("Scenario 12 with Gift wrap and discounts ::: [Adjusted quantity less than  total  gift wrap items quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
												vlogDebug("Qunatity to display zero :: {0} Gift wrap quantity :: {1}", qtyToDispalyZeroPrice, giftWrapItemQty);
											}
										pMetaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), qtyToDispalyZeroPrice, 0,listPrice * qtyToDispalyZeroPrice,salePrice*qtyToDispalyZeroPrice, salePrice, giftWrapItem,
												pShipItemRel.getId(), pPriceQuote, promoId));
										quantity += qtyToDispalyZeroPrice;
										}
										if(diffValue > salePrice){
											qty += TRUConstants.LONG_ONE;
											if (isLoggingDebug()) {
												vlogDebug("Scenario 13 with Gift wrap and discounts ::: [Adjusted quantity less than  total  gift wrap items quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
												vlogDebug("Qunatity to display zero :: {0} Gift wrap quantity :: {1}", qtyToDispalyZeroPrice, giftWrapItemQty);
											}
											pMetaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), qty, diffValue,listPrice * qty,salePrice*qty, salePrice, giftWrapItem,
													pShipItemRel.getId(), pPriceQuote, promoId));
										}else if(diffValue == TRUConstants.DOUBLE_ZERO){
											//Do Nothing
										} else{
											qty += TRUConstants.LONG_ONE;
											if(quantity < lQtyAdj){
												if (isLoggingDebug()) {
													vlogDebug("Scenario 14 with Gift wrap and discounts ::: [Adjusted quantity less than  total  gift wrap items quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
													vlogDebug("Sum of gift wrap items quantity :: {0} Gift wrap quantity :: {1}", quantity, giftWrapItemQty);
												}
												pMetaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(),qty, salePrice-diffValue,
														listPrice*qty,salePrice*qty, salePrice, giftWrapItem, pShipItemRel.getId(), pPriceQuote, promoId));
											}else{
												if (isLoggingDebug()) {
													vlogDebug("Scenario 15 with Gift wrap and discounts ::: [Adjusted quantity less than  total  gift wrap items quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
													vlogDebug("Sum of gift wrap items quantity :: {0} Gift wrap quantity :: {1}", quantity, giftWrapItemQty);
												}
												pMetaInfoList.add(createMetaInfoItem(Boolean.FALSE, pShipItemRel.getCommerceItem(), giftWrapItemQty, salePrice*giftWrapItemQty,listPrice * giftWrapItemQty,salePrice*giftWrapItemQty, salePrice, giftWrapItem,
														pShipItemRel.getId(), pPriceQuote, null));
											}
											quantity += qty;
											if(giftWrapItemQty - quantity > TRUConstants._0){
												if (isLoggingDebug()) {
													vlogDebug("Scenario 16 with Gift wrap and discounts ::: [Adjusted quantity less than  total  gift wrap items quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
													vlogDebug("Sum of gift wrap items quantity :: {0} Gift wrap quantity :: {1}", quantity, giftWrapItemQty);
												}
												pMetaInfoList.add(createMetaInfoItem(Boolean.FALSE, pShipItemRel.getCommerceItem(), (giftWrapItemQty - quantity), salePrice*(giftWrapItemQty - quantity),listPrice * (giftWrapItemQty - quantity),
														salePrice*(giftWrapItemQty - quantity), salePrice, giftWrapItem, pShipItemRel.getId(), pPriceQuote, null));
												quantity += (giftWrapItemQty - quantity);
											}
										}
										//qty +=qtyToDispalyZeroPrice;
										qtyToDispalyZeroPrice -= giftWrapItemQty;
									}
								}else{
									if(lQtyAdj >=giftWrapItemQty){
										if (isLoggingDebug()) {
											vlogDebug("Scenario 17 with Gift wrap and discounts ::: [Adjusted quantity greater than individual gift wrap item quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
										}
										pMetaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), lQtyAdj, (detailedUnitPrice * lQtyAdj)+lAdjustedAmount,listPrice * lQtyAdj,salePrice*lQtyAdj, 
												salePrice, giftWrapItem, pShipItemRel.getId(), pPriceQuote, pricingAdjustment.getPricingModel().getRepositoryId()));
										lQtyAdj -= giftWrapItemQty;
									}else if(lQtyAdj != TRUConstants.LONG_ZERO && lQtyAdj < giftWrapItemQty){
										if (isLoggingDebug()) {
											vlogDebug("Scenario 18 with Gift wrap and discounts ::: [Adjusted quantity less than individual gift wrap item quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
										}
										pMetaInfoList.add(createMetaInfoItem(Boolean.TRUE, pShipItemRel.getCommerceItem(), lQtyAdj, (detailedUnitPrice * lQtyAdj)+lAdjustedAmount,listPrice * lQtyAdj,salePrice*lQtyAdj, 
												salePrice, giftWrapItem, pShipItemRel.getId(), pPriceQuote, pricingAdjustment.getPricingModel().getRepositoryId()));
										qty = giftWrapItemQty - lQtyAdj;
										if (isLoggingDebug()) {
											vlogDebug("Scenario 19 with Gift wrap and discounts ::: [Adjusted quantity less than individual gift wrap item quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
											vlogDebug("Creating meta info with Quantity ::: [giftWrapItemQty - lQtyAdj] :: : {0}", giftWrapItemQty);
										}
										pMetaInfoList.add(createMetaInfoItem(Boolean.FALSE, pShipItemRel.getCommerceItem(), qty, (detailedUnitPrice * qty),listPrice * qty,salePrice*qty, 
												salePrice, giftWrapItem, pShipItemRel.getId(), pPriceQuote, null));
										lQtyAdj = TRUConstants.LONG_ZERO;
									}else if(lQtyAdj == TRUConstants.LONG_ZERO && giftWrapItemQty > TRUConstants.LONG_ZERO){
										if (isLoggingDebug()) {
											vlogDebug("Scenario 20 with Gift wrap and discounts ::: [Adjusted quantity greater than individual gift wrap item quantity] :::  lQtyAdj: {0}  giftWrapItemQty :{1}" ,lQtyAdj, giftWrapItemQty );
										}
										pMetaInfoList.add(createMetaInfoItem(Boolean.FALSE, pShipItemRel.getCommerceItem(), giftWrapItemQty, (detailedUnitPrice * giftWrapItemQty),listPrice * giftWrapItemQty,salePrice*giftWrapItemQty, 
												salePrice, giftWrapItem, pShipItemRel.getId(), pPriceQuote, null));
									}
								}
								quantity += giftWrapItemQty;
							}
							if(shipItemRelQty - quantity > TRUConstants._0){
								if (isLoggingDebug()) {
									vlogDebug("Scenario 21 with Gift wrap and discounts ::: [Adjusted quantity greater than individual gift wrap item quantity] :::  Ship Item Relationship Quantity: {0}  Sum of gift Wrap Items quantity :: {1}" ,shipItemRelQty, quantity );
								}
								pMetaInfoList.add(createMetaInfoItem(Boolean.FALSE, pShipItemRel.getCommerceItem(),shipItemRelQty - quantity, detailedUnitPrice * (shipItemRelQty - quantity),
										listPrice* (shipItemRelQty - quantity),salePrice*(shipItemRelQty - quantity), salePrice, null, pShipItemRel.getId(), pPriceQuote, null));
							}
						}
					}else{
						vlogDebug("ShipItemRelQty is less than giftwrap total quantity. ShipItem Rel qty : {0} Giftwrap total qty : {1}", shipItemRelQty, pGiftWrapItemQtyTotal);
					}
					/*if(propertyValue.equals(getTieredPriceBreakTemplate())){//Tiered price break template- added this as price will be available in  detailed item price info
							detaileditemPriceInfo = (DetailedItemPriceInfo) itemPriceInfo.getCurrentPriceDetails().get(0);
							salePrice = detaileditemPriceInfo.getDetailedUnitPrice();
						}*/
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOrderTools  method: createMetaInfoforGiftwrap]");
		}
	}
}
