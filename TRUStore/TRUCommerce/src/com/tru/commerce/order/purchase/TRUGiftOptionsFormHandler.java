package com.tru.commerce.order.purchase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.transaction.Transaction;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.RelationshipNotFoundException;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupNotFoundException;
import atg.commerce.order.purchase.PurchaseProcessFormHandler;
import atg.commerce.pricing.PricingConstants;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.vo.TRUGiftItemInfo;
import com.tru.commerce.order.vo.TRUGiftOptionsInfo;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.cml.ValidationExceptions;
import com.tru.errorhandler.fhl.IErrorHandler;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.validator.fhl.IValidator;

/**
 * @author PA
 * @version 1.0
 */
public class TRUGiftOptionsFormHandler extends PurchaseProcessFormHandler {
	
	private ValidationExceptions mVe = new ValidationExceptions();
	private boolean mGiftWrapSelected;
	private String mGiftWrapSkuId;
	private String mGiftWrapProductId;
	private String mGiftNoteSelected;
	private String mGiftMessage;
	private boolean mGiftReceiptSelected;
	private String mShipGroupId;
	private String mGiftOptionsSuccessURL;
	private String mGiftOptionsErrorURL;
	private String mCommerceItemId;
	private String mCommerceItemRelationshipId;
	private boolean mGiftItem;
	private IErrorHandler mErrorHandler;
	private String mCurrentPage;
	private IValidator mValidator;
	private long mQuantity;
	private TRUGiftOptionsInfo[] mGiftOptionsInfo;
	private int mGiftOptionsInfoCount;
	private boolean mShowMeGiftingOptions;
	private String mSelectedId;
	private String mPreviousGiftWrapSkuId;
	private TRUGiftingPurchaseProcessHelper mGiftingProcessHelper;
	private boolean mRestService;
	
	/** Holds mErrorMessageShipGroupIds value. */
	private List<String> mErrorMessageShipGroupIds;
	
	/**
	 * @return the errorMessageShipGroupIds
	 */
	public List<String> getErrorMessageShipGroupIds() {
		return mErrorMessageShipGroupIds;
	}

	/**
	 * @param pErrorMessageShipGroupIds the errorMessageShipGroupIds to set
	 */
	public void setErrorMessageShipGroupIds(List<String> pErrorMessageShipGroupIds) {
		mErrorMessageShipGroupIds = pErrorMessageShipGroupIds;
	}

	/**
	 * @return the mRestService
	 */
	public boolean isRestService() {
		return mRestService;
	}

	/**
	 * @param pRestService the mRestService to set
	 */
	public void setRestService(boolean pRestService) {
		this.mRestService = pRestService;
	}

	/**
	 * @return the giftingProcessHelper
	 */
	public TRUGiftingPurchaseProcessHelper getGiftingProcessHelper() {
		return mGiftingProcessHelper;
	}

	/**
	 * @param pGiftingProcessHelper the giftingProcessHelper to set
	 */
	public void setGiftingProcessHelper(
			TRUGiftingPurchaseProcessHelper pGiftingProcessHelper) {
		mGiftingProcessHelper = pGiftingProcessHelper;
	}
	/**
	 * @return the IValidator
	 */

	public IValidator getValidator() {
		return mValidator;
	}
	/**
	 * @param pValidator the pValidator to set
	 */
	public void setValidator(IValidator pValidator) {
		this.mValidator = pValidator;
	}
	/**
	 * @return the IErrorHandler
	 */
	public IErrorHandler getErrorHandler() {
		return mErrorHandler;
	}

	/**
	 * @param pErrorHandler the pErrorHandler to set
	 */
	public void setErrorHandler(IErrorHandler pErrorHandler) {
		this.mErrorHandler = pErrorHandler;
	}

	/**
	 * @return the giftItem
	 */
	public boolean isGiftItem() {
		return mGiftItem;
	}

	/**
	 * @param pGiftItem pGiftItem
	 */
	public void setGiftItem(boolean pGiftItem) {
		mGiftItem = pGiftItem;
	}

	/**
	 * @return commerceItemRelationshipId
	 */
	public String getCommerceItemRelationshipId() {
		return mCommerceItemRelationshipId;
	}

	/**
	 * @param pCommerceItemRelationshipId the commerceItemRelationshipId to set
	 */
	public void setCommerceItemRelationshipId(String pCommerceItemRelationshipId) {
		mCommerceItemRelationshipId = pCommerceItemRelationshipId;
	}

	/**
	 * @return commerceItemId
	 */
	public String getCommerceItemId() {
		return mCommerceItemId;
	}

	/**
	 * @param pCommerceItemId the commerceItemId to set 
	 */
	public void setCommerceItemId(String pCommerceItemId) {
		mCommerceItemId = pCommerceItemId;
	}

	/**
	 * @return giftOptionsSuccessURL
	 */
	public String getGiftOptionsSuccessURL() {
		return mGiftOptionsSuccessURL;
	}

	/**
	 * @param pGiftOptionsSuccessURL the giftOptionsSuccessURL to set 
	 */
	public void setGiftOptionsSuccessURL(String pGiftOptionsSuccessURL) {
		mGiftOptionsSuccessURL = pGiftOptionsSuccessURL;
	}

	/**
	 * @return giftOptionsErrorURL
	 */
	public String getGiftOptionsErrorURL() {
		return mGiftOptionsErrorURL;
	}

	/**
	 * @param pGiftOptionsErrorURL the giftOptionsErrorURL to set
	 */
	public void setGiftOptionsErrorURL(String pGiftOptionsErrorURL) {
		mGiftOptionsErrorURL = pGiftOptionsErrorURL;
	}

	/**
	 * @return shipGroupId
	 */
	public String getShipGroupId() {
		return mShipGroupId;
	}

	/**
	 * @param pShipGroupId the shipGroupId to set
	 */
	public void setShipGroupId(String pShipGroupId) {
		mShipGroupId = pShipGroupId;
	}

	/**
	 * @return giftReceiptSelected
	 */
	public boolean isGiftReceiptSelected() {
		return mGiftReceiptSelected;
	}

	/**
	 * @param pGiftReceiptSelected the giftReceiptSelected to set
	 */
	public void setGiftReceiptSelected(boolean pGiftReceiptSelected) {
		mGiftReceiptSelected = pGiftReceiptSelected;
	}

	/**
	 * @return giftWrapSelected
	 */
	public boolean isGiftWrapSelected() {
		return mGiftWrapSelected;
	}

	/**
	 * @param pGiftWrapSelected the giftWrapSelected to set
	 */
	public void setGiftWrapSelected(boolean pGiftWrapSelected) {
		mGiftWrapSelected = pGiftWrapSelected;
	}

	/**
	 * @return giftWrapSkuId
	 */
	public String getGiftWrapSkuId() {
		return mGiftWrapSkuId;
	}

	/**
	 * @param pGiftWrapSkuId the giftWrapSkuId to set
	 */
	public void setGiftWrapSkuId(String pGiftWrapSkuId) {
		mGiftWrapSkuId = pGiftWrapSkuId;
	}

	/**
	 * @return giftWrapProductId
	 */
	public String getGiftWrapProductId() {
		return mGiftWrapProductId;
	}

	/**
	 * @param pGiftWrapProductId the giftWrapProductId to set
	 */
	public void setGiftWrapProductId(String pGiftWrapProductId) {
		mGiftWrapProductId = pGiftWrapProductId;
	}

	/**
	 * @return giftNoteSelected
	 */
	public String getGiftNoteSelected() {
		return mGiftNoteSelected;
	}

	/**
	 * @param pGiftNoteSelected the giftNoteSelected to set
	 */
	public void setGiftNoteSelected(String pGiftNoteSelected) {
		mGiftNoteSelected = pGiftNoteSelected;
	}

	/**
	 * @return giftMessage
	 */
	public String getGiftMessage() {
		return mGiftMessage;
	}

	/**
	 * @param pGiftMessage the giftMessage to set
	 */
	public void setGiftMessage(String pGiftMessage) {
		mGiftMessage = pGiftMessage;
	}

	/**
	 * @return the giftOptionsInfoCount
	 */
	public int getGiftOptionsInfoCount() {
		return mGiftOptionsInfoCount;
	}

	/**
	 * @return the showMeGiftingOptions
	 */
	public boolean isShowMeGiftingOptions() {
		return mShowMeGiftingOptions;
	}

	/**
	 * @param pShowMeGiftingOptions
	 *            the showMeGiftingOptions to set
	 */
	public void setShowMeGiftingOptions(boolean pShowMeGiftingOptions) {
		mShowMeGiftingOptions = pShowMeGiftingOptions;
	}

	/**
	 * @param pGiftOptionsInfoCount the giftOptionsInfoCount to set
	 */
	public void setGiftOptionsInfoCount(int pGiftOptionsInfoCount) {
		if (pGiftOptionsInfoCount <= 0) {
			this.mGiftOptionsInfoCount = 0;
			this.mGiftOptionsInfo = null;
		} else {
			this.mGiftOptionsInfoCount = pGiftOptionsInfoCount;
			this.mGiftOptionsInfo = new TRUGiftOptionsInfo[this.mGiftOptionsInfoCount];
				try {
					for (int index = 0; index < pGiftOptionsInfoCount; ++index){
						this.mGiftOptionsInfo[index] = ((TRUGiftOptionsInfo)Class.forName(TRUCommerceConstants.GIFT_OPTIONS_INFO_CLASS_NAME)
								.newInstance());
					}
				} catch (InstantiationException insExe) {
					this.mGiftOptionsInfo = null;
					if (isLoggingError()) {
						vlogError("InstantiationException occurred in setGiftOptionsInfoCount @Class::TRUGiftOptionsFormHandler::@method::" + 
								"setGiftOptionsInfoCount()", insExe);
					}
				} catch (IllegalAccessException illExe) {
					this.mGiftOptionsInfo = null;
					if (isLoggingError()) {
						vlogError("IllegalAccessException occurred in setGiftOptionsInfoCount @Class::TRUGiftOptionsFormHandler::@method::" + 
								"setGiftOptionsInfoCount()", illExe);
					}
				} catch (ClassNotFoundException clsExe) {
					this.mGiftOptionsInfo = null;
					if (isLoggingError()) {
						vlogError("ClassNotFoundException occurred in setGiftOptionsInfoCount @Class::TRUGiftOptionsFormHandler::@method::" + 
								"setGiftOptionsInfoCount()", clsExe);
					}
					
				}
			}
		
	}

	/**
	 * @return the giftOptionsInfo
	 */
	public TRUGiftOptionsInfo[] getGiftOptionsInfo() {
		return mGiftOptionsInfo;
	}

	/**
	 * @param pGiftOptionsInfo the giftOptionsInfo to set
	 */
	public void setGiftOptionsInfo(TRUGiftOptionsInfo[] pGiftOptionsInfo) {
		mGiftOptionsInfo = pGiftOptionsInfo;
	}

	/**
	 * @return the quantity
	 */
	public long getQuantity() {
		return mQuantity;
	}

	/**
	 * @param pQuantity the quantity to set
	 */
	public void setQuantity(long pQuantity) {
		mQuantity = pQuantity;
	}

	/**
	 * @return the selectedId
	 */
	public String getSelectedId() {
		return mSelectedId;
	}

	/**
	 * @param pSelectedId the selectedId to set
	 */
	public void setSelectedId(String pSelectedId) {
		mSelectedId = pSelectedId;
	}

	/**
	 * @return the previousGiftWrapSkuId
	 */
	public String getPreviousGiftWrapSkuId() {
		return mPreviousGiftWrapSkuId;
	}

	/**
	 * @param pPreviousGiftWrapSkuId the previousGiftWrapSkuId to set
	 */
	public void setPreviousGiftWrapSkuId(String pPreviousGiftWrapSkuId) {
		mPreviousGiftWrapSkuId = pPreviousGiftWrapSkuId;
	}

	/**
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	protected void preApplyGiftWrap(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if(isLoggingDebug()){
			vlogDebug("START: TRUGiftOptionsFormHandler.preApplyGiftWrap");
		}
		if(StringUtils.isBlank(mCommerceItemId) || StringUtils.isBlank(mCommerceItemRelationshipId) || StringUtils.isBlank(mShipGroupId)) {
			mVe.addValidationError(
					TRUErrorKeys.TRU_ERROR_GIFT_WRAP_MISSING_INFO, null);
			getErrorHandler().processException(mVe, this);
		}
		if(isLoggingDebug()){
			vlogDebug("END: TRUGiftOptionsFormHandler.preApplyGiftWrap");
		}
		
	}

	/**
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleApplyGiftWrap(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		// get the gift wrap options, update the gift options to the shipping
		// group, and update the gift wrap options to commerceItemShippingInfo
		// handling instructions
		String myHandleMethod = "TRUGiftOptionsFormHandler.handleApplyGiftWrap";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					preApplyGiftWrap(pRequest, pResponse);
					if(!getFormError()) {
					Map<String,Object> inputParams=new HashMap<String, Object>(TRUCheckoutConstants._10);
					inputParams.put(TRUConstants.USER_LOCALE, getUserLocale(pRequest, pResponse));
					inputParams.put(TRUConstants.ORDER, getOrder());
					inputParams.put(TRUConstants.SHIP_GRP_ID,getShipGroupId());
					inputParams.put(TRUConstants.COMMERCE_REL_ID,getCommerceItemRelationshipId());
					inputParams.put(TRUConstants.GIFT_WRAP_SKU_ID ,getGiftWrapSkuId());
					inputParams.put(TRUConstants.GIFT_WRAP_PROD_ID ,getGiftWrapProductId());
					inputParams.put(TRUConstants.PRE_GIFT_WRAP_SKU_ID ,getPreviousGiftWrapSkuId());
					inputParams.put(TRUConstants.IS_GIFT_WRAP_SELECTED ,isGiftWrapSelected());					
					getGiftingProcessHelper().createOrUpdateGiftWrapCommerceItems(inputParams);
					runProcessRepriceOrder(getOrder(),
							getUserPricingModels(), getUserLocale(pRequest, pResponse),
							getProfile(), createRepriceParameterMap());
					getOrderManager().updateOrder(getOrder());
				  }
				}
			} catch (CommerceException| RepositoryException | RunProcessException e) {
				if (isLoggingError()) {
					logError("Exception in TRUGiftOptionsFormHandler.handleApplyGiftWrap", e);
				}
			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		return checkFormRedirect(getGiftOptionsSuccessURL(), getGiftOptionsErrorURL(), pRequest, pResponse);
	}

	/**
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */ 
	public boolean handleApplyGiftMessage(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		// get the gift message
		// check for profanity words through the pre method
		// update the gift message to the shipping group

		String myHandleMethod = "TRUGiftOptionsFormHandler.handleApplyGiftMessage";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					//preApplyGiftMessage(pRequest, pResponse);
					if (getFormError()){
						return checkFormRedirect(getGiftOptionsSuccessURL(), getGiftOptionsErrorURL(), pRequest, pResponse);
					}
					applyGiftMessage(pRequest, pResponse);
					//postApplyGiftMessage(pRequest, pResponse);
					((TRUOrderImpl)getOrder()).getActiveTabs().put(TRUCheckoutConstants.GIFTING_TAB, Boolean.TRUE);
					if (!getFormError()) {
						if(!((TRUOrderTools) getOrderManager().getOrderTools()).getAllInStorePickupShippingGroups(getOrder()).isEmpty()){
							//Redirect URL: store pickup page
							((TRUOrderImpl)getOrder()).getActiveTabs().put(TRUCheckoutConstants.PICKUP_TAB, Boolean.TRUE);
						} else {
							((TRUOrderImpl) getOrder()).getActiveTabs().put(TRUCheckoutConstants.PAYMENT_TAB, Boolean.TRUE);
						}
					}
					//Start :: Commented out for no of taxware calls.
					/*runProcessRepriceOrder(getOrder(),
							getUserPricingModels(), getUserLocale(pRequest, pResponse),
							getProfile(), createRepriceParameterMap());*/
					//End :: Commented out for no of taxware calls.
					((TRUOrderManager) getOrderManager()).updateOrder(getOrder());
				}
			} catch (CommerceException | RepositoryException exc) {
				if (isLoggingError()) {
					logError("Exception in TRUGiftOptionsFormHandler.handleApplyGiftMessage", exc);
				}
			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		return checkFormRedirect(getGiftOptionsSuccessURL(), getGiftOptionsErrorURL(), pRequest, pResponse);
	}

	/**
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void postApplyGiftMessage (DynamoHttpServletRequest pRequest,  DynamoHttpServletResponse pResponse)  
			throws ServletException, IOException { 
		// will be responsible for post logic
	}

	/**
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 * @throws RepositoryException RepositoryException
	 */
	public void preApplyGiftMessage(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, RepositoryException {
		getGiftMessage();

		boolean profaneWordsExists =  getGiftingProcessHelper().checkProfanityWords(getGiftMessage());
		if(profaneWordsExists){
			mVe.addValidationError(
					TRUErrorKeys.TRU_ERROR_CHECKOUT_PROFANE_WORD, null);
			getErrorHandler().processException(mVe, this);
		}
	}

	/** 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 * @throws InvalidParameterException InvalidParameterException 
	 * @throws ShippingGroupNotFoundException ShippingGroupNotFoundException
	 * @throws RepositoryException 
	 */
	public void applyGiftMessage(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, ShippingGroupNotFoundException, InvalidParameterException, RepositoryException {
		// update the gift messages to order s shipping groups.
		Map<ShippingGroup, TRUGiftOptionsInfo> giftMessages = new HashMap<ShippingGroup, TRUGiftOptionsInfo>();
		List<String> errorMessageShipGroupIds=new ArrayList<String>();
		for (int index = TRUConstants.INTEGER_NUMBER_ZERO; index < getGiftOptionsInfo().length; ++index) {
			TRUGiftOptionsInfo input = getGiftOptionsInfo()[index];
			if(StringUtils.isBlank(input.getGiftOptionId()) && StringUtils.isNotBlank(input.getShippingGroupId())){
				TRUHardgoodShippingGroup shipGroup = (TRUHardgoodShippingGroup) getOrder().getShippingGroup(input.getShippingGroupId());
				input.setShippingAdressNickName(shipGroup.getNickName());
			}
			if(StringUtils.isNotBlank(input.getShippingAdressNickName())) {
				//TO update the message in Gift Options page
				boolean profaneWordsExists =  getGiftingProcessHelper().checkProfanityWords(input.getGiftWrapMessage());
				if(profaneWordsExists) {
					if(StringUtils.isBlank(input.getGiftOptionId()) && StringUtils.isNotBlank(input.getShippingGroupId())){
						Object[] finalMsgCntx = new Object[TRUConstants.INTEGER_NUMBER_ONE];
						finalMsgCntx[0] = input.getShippingGroupId();
						mVe.addValidationError(
								TRUErrorKeys.TRU_ERROR_CHECKOUT_PROFANE_WORD, finalMsgCntx);
						getErrorHandler().processException(mVe, this);
						input.setGiftWrapMessage(TRUConstants.EMPTY);
					} else {
						Object[] finalMsgCntx = new Object[TRUConstants.INTEGER_NUMBER_ONE];
						finalMsgCntx[0] = input.getGiftOptionId();
						mVe.addValidationError(
								TRUErrorKeys.TRU_ERROR_CHECKOUT_PROFANE_WORD, finalMsgCntx);
						getErrorHandler().processException(mVe, this);
						input.setGiftWrapMessage(TRUConstants.EMPTY);
					}
					if(isRestService()){
						errorMessageShipGroupIds.add(input.getGiftOptionId());
					}
				}
				List<TRUHardgoodShippingGroup> shipGroupsByNickName = 
						((TRUOrderManager)getOrderManager()).getShipGroupsByNickName(getOrder(), input.getShippingAdressNickName());
				for (TRUHardgoodShippingGroup truHardgoodShippingGroup : shipGroupsByNickName) {
					giftMessages.put(truHardgoodShippingGroup, input);
				}
			}
		}
		if(isRestService() && errorMessageShipGroupIds!=null && !errorMessageShipGroupIds.isEmpty()){
			setErrorMessageShipGroupIds(errorMessageShipGroupIds);
		}
		getGiftingProcessHelper().applyGiftMessage(giftMessages, getShippingGroupMapContainer());
	}
	
		
		
	

	/**
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleUpdateGiftItem(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		String myHandleMethod = "TRUGiftOptionsFormHandler.handleUpdateGiftItem";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					ShippingGroup shippingGroup = getOrder().getShippingGroup(getShipGroupId());
					TRUShippingGroupCommerceItemRelationship shpGrpCIRel = (TRUShippingGroupCommerceItemRelationship) getOrder()
							.getRelationship(getCommerceItemRelationshipId());
					if(isGiftItem()){
						//Update isGiftItem in the CommerceItem Relation ship
						updateIsGiftItem(shippingGroup, shpGrpCIRel);
						//((TRUOrderImpl)getOrder()).isShowMeGiftingOptions();
						 ((TRUOrderManager)getOrderManager()).updateShowMeGiftingOptions(getOrder());
					} else{
						boolean giftItemFlag = getGiftingProcessHelper().updateGiftItem(getOrder(), 
								shippingGroup, shpGrpCIRel, getGiftWrapSkuId(), getQuantity());
						// To update the Gift Item
						if(StringUtils.isNotBlank(getCurrentPage()) && !getCurrentPage().equalsIgnoreCase(TRUCommerceConstants.CART)){
							Map<String, TRUGiftOptionsInfo> giftOptionsDetails = ((TRUOrderImpl)getOrder()).getGiftOptionsDetails();
							if(giftOptionsDetails != null && !giftOptionsDetails.isEmpty()){
								TRUGiftOptionsInfo truGiftOptionsInfo = giftOptionsDetails.get(((TRUHardgoodShippingGroup)shippingGroup)
										.getNickName());
								Map<String, TRUGiftItemInfo> eligibleGiftItems = truGiftOptionsInfo.getEligibleGiftItems();
								TRUGiftItemInfo selectedGiftItem = eligibleGiftItems.get(getSelectedId());
								if (selectedGiftItem != null) {
									selectedGiftItem.setGiftItem(false);
									//START: TO update the isGiftItem to shpGrpCIRel
									long parentQuantity =  selectedGiftItem.getParentQuantity();
									StringBuffer selectedGiftId = new StringBuffer();
									giftItemFlag = false;
									for (int i = 1; i <= parentQuantity; i++) {
										selectedGiftId = new StringBuffer();
										selectedGiftId = selectedGiftId.append(shpGrpCIRel.getCommerceItem().getId() + 
												TRUCommerceConstants.UNDER_SCORE + shpGrpCIRel.getId() +
												TRUCommerceConstants.UNDER_SCORE + i);
										if(null != eligibleGiftItems.get(selectedGiftId.toString()) && eligibleGiftItems
												.get(selectedGiftId.toString()).isGiftItem()){
											giftItemFlag = true;
											break;
										}
									} 
									shpGrpCIRel.setIsGiftItem(giftItemFlag);
									//END: TO update the isGiftItem to shpGrpCIRel
								}
							}
							((TRUOrderImpl)getOrder()).isShowMeGiftingOptions();
						} else {
							shpGrpCIRel.setIsGiftItem(giftItemFlag);
							final List<ShippingGroup> shippingGroups = ((TRUOrderImpl)getOrder()).getShippingGroups();
							if (shippingGroups != null && !shippingGroups.isEmpty()) {
								boolean continueLoop = Boolean.TRUE; 
								boolean showMeGiftingOptions = Boolean.FALSE;
								for (ShippingGroup shipGroup : shippingGroups) {
									if (continueLoop && shipGroup instanceof TRUHardgoodShippingGroup) {
										final List<CommerceItemRelationship> ciRels = shipGroup.getCommerceItemRelationships();
										for (CommerceItemRelationship commerceItemRelationship : ciRels) {
											final TRUShippingGroupCommerceItemRelationship truSgCiR = (TRUShippingGroupCommerceItemRelationship)commerceItemRelationship;
											if (truSgCiR.getIsGiftItem()) {
												showMeGiftingOptions = Boolean.TRUE;
												continueLoop = Boolean.FALSE;
												break;
											}
										}
									}
								}
								((TRUOrderImpl)getOrder()).setShowMeGiftingOptions(showMeGiftingOptions);
							}
						}
					}
					runProcessRepriceOrder(getOrder(),
							getUserPricingModels(), getUserLocale(pRequest, pResponse),
							getProfile(), createRepriceParameterMap());
					getOrderManager().updateOrder(getOrder());
				}
			} catch (RelationshipNotFoundException relationshipNotFoundException) {
				if (isLoggingError()) {
					logError("RelationshipNotFoundException in TRUGiftOptionsFormHandler.handleUpdateGiftItem", relationshipNotFoundException);
				}
			} catch (InvalidParameterException invalidParameterException) {
				if (isLoggingError()) {
					logError("InvalidParameterException in TRUGiftOptionsFormHandler.handleUpdateGiftItem", invalidParameterException);
				}
			}catch (ShippingGroupNotFoundException sgNotFoundException) {
				if (isLoggingError()) {
					logError("ShippingGroupNotFoundException in TRUGiftOptionsFormHandler.handleUpdateGiftItem", sgNotFoundException);
				}
			} catch (CommerceException commerceException) {
				if (isLoggingError()) {
					logError("ShippingGroupNotFoundException in TRUGiftOptionsFormHandler.handleUpdateGiftItem", commerceException);
				}
			} catch (CloneNotSupportedException cloneNotSupportedException) {
				if (isLoggingError()) {
					logError("CloneNotSupportedException in TRUGiftOptionsFormHandler.handleUpdateGiftItem", cloneNotSupportedException);
				}
			} catch (RunProcessException runProcessException) {
				if (isLoggingError()) {
					logError("CloneNotSupportedException in TRUGiftOptionsFormHandler.handleUpdateGiftItem", runProcessException);
				}
			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		return checkFormRedirect(getGiftOptionsSuccessURL(), getGiftOptionsErrorURL(), pRequest, pResponse);
	}

	/**
	 * @param pShpGrpCIRel 
	 * @param pShippingGroup 
	 * @throws CloneNotSupportedException 
	 * @throws CloneNotSupportedException CloneNotSupportedException
	 */
	private void updateIsGiftItem(ShippingGroup pShippingGroup, TRUShippingGroupCommerceItemRelationship pShpGrpCIRel)
							throws CloneNotSupportedException  {
		//Update isGiftItem in the CommerceItem Relation ship
		CommerceItem commerceItem = pShpGrpCIRel.getCommerceItem();
		if (commerceItem != null &&  commerceItem.getId().equalsIgnoreCase(getCommerceItemId())) {
			pShpGrpCIRel.setIsGiftItem(isGiftItem());
			if(StringUtils.isNotBlank(getCurrentPage()) && !getCurrentPage().equalsIgnoreCase(TRUTaxwareConstant.CART)){
				Map<String, TRUGiftOptionsInfo> giftOptionsDetails = ((TRUOrderImpl)getOrder()).getGiftOptionsDetails();
				TRUGiftOptionsInfo truGiftOptionsInfo = giftOptionsDetails.get(((TRUHardgoodShippingGroup)pShippingGroup).getNickName());
				Map<String, TRUGiftItemInfo> eligibleGiftItems = truGiftOptionsInfo.getEligibleGiftItems();
				Map<String, TRUGiftItemInfo> splitGiftItems = new HashMap<String, TRUGiftItemInfo>();
				TRUGiftItemInfo giftItemInfo = eligibleGiftItems.get(getSelectedId());
				//Splitting the item for Presentation layer
				int itemQtyInCart = giftItemInfo.getQuantity();
				if (itemQtyInCart == TRUTaxwareConstant.NUMBER_ONE){
					giftItemInfo.setGiftItem(true);
					splitGiftItems.put(getSelectedId(), giftItemInfo);
					((TRUOrderImpl)getOrder()).setSplitGiftItems(splitGiftItems);
				}/*else{
					eligibleGiftItems.remove(getSelectedId());
					for(int i=1; i <= itemQtyInCart ; i++){
						TRUGiftItemInfo newGiftItemInfo = (TRUGiftItemInfo) giftItemInfo.clone();
						newGiftItemInfo.setGiftItem(true);
						newGiftItemInfo.setQuantity(TRUTaxwareConstant.NUMBER_ONE);

						eligibleGiftItems.put(commerceItem.getId() + TRUCommerceConstants.UNDER_SCORE +pShpGrpCIRel.getId()+ TRUCommerceConstants.UNDER_SCORE + i, newGiftItemInfo);
						splitGiftItems.put(commerceItem.getId() + TRUCommerceConstants.UNDER_SCORE +pShpGrpCIRel.getId()+ TRUCommerceConstants.UNDER_SCORE + i, newGiftItemInfo);
					}
				}*/
				truGiftOptionsInfo.setEligibleGiftItems(eligibleGiftItems);
				((TRUOrderImpl)getOrder()).setGiftOptionsDetails(giftOptionsDetails);
				((TRUOrderImpl)getOrder()).setSplitGiftItems(splitGiftItems); 	
			}
		}

	}

	/**
	 * Handle 'Update Gift Options' case.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * 
	 * @return redirection result.
	 * 
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	public boolean handleUpdateGiftOptions(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		String myHandleMethod = "TRUShippingGroupFormHandler.handleUpdateGiftOptions";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					// Update Show me gift Options value in Order
					((TRUOrderImpl) getOrder()).setShowMeGiftingOptions(isShowMeGiftingOptions());
					// Clear Gift Option details in Order and item
					if (!isShowMeGiftingOptions()) {
						((TRUOrderImpl) getOrder()).setApplyGiftingOptions(Boolean.FALSE);
						getGiftingProcessHelper().clearAnyGiftOptions(getOrder());
					} else{
						getGiftingProcessHelper().applyGiftOptions(getOrder());
						((TRUOrderImpl) getOrder()).setApplyGiftingOptions(Boolean.TRUE);
					}
					runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_SUBTOTAL_SHIPPING, getOrder(), getUserPricingModels(), 
							getUserLocale(pRequest, pResponse), getProfile(), createRepriceParameterMap()); 
					getOrderManager().updateOrder(getOrder());
				}
			} catch (CommerceException comm) {
				if (isLoggingError()) {
					logError("CommerceException in TRUGiftOptionsFormHandler.handleUpdateGiftOptions", comm);
				}
			} catch (RunProcessException RunProcess) {
				if (isLoggingError()) {
					logError("RunProcessException in TRUGiftOptionsFormHandler.handleUpdateGiftOptions", RunProcess);
				}
			}finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		return checkFormRedirect(getGiftOptionsSuccessURL(), getGiftOptionsErrorURL(), pRequest, pResponse);
	}

	

	/**
	 * @return the currentPage
	 */
	public String getCurrentPage() {
		return mCurrentPage;
	}

	/**
	 * @param pCurrentPage the currentPage to set
	 */
	public void setCurrentPage(String pCurrentPage) {
		mCurrentPage = pCurrentPage;
	}

	/**
	 * This method is used to add calculate tax parameter.
	 * 
	 * @return Map
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected Map createRepriceParameterMap() {
		if (isLoggingDebug()) {
			vlogDebug("TRUCommitOrderFormHandler (createRepriceParameterMap) : BEGIN");
		}
		Map parameters;
		parameters = super.createRepriceParameterMap();
		if (parameters == null) {
			parameters = new HashMap();
		}
		parameters.put(TRUTaxwareConstant.CALC_TAX, Boolean.TRUE);
		if (isLoggingDebug()) {
			vlogDebug("TRUCommitOrderFormHandler (createRepriceParameterMap) : END");
		}
		return parameters;
	}
}
