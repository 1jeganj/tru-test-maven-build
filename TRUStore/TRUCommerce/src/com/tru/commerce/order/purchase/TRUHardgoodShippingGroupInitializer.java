package com.tru.commerce.order.purchase;

import java.sql.Timestamp;
import java.util.List;

import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.OrderTools;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.purchase.HardgoodShippingGroupInitializer;
import atg.commerce.order.purchase.ShippingGroupInitializationException;
import atg.commerce.order.purchase.ShippingGroupMapContainer;
import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.Profile;

import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConstants;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * The Class TRUHardgoodShippingGroupInitializer.
 * @author PA.
 * @version 1.0.
 */
public class TRUHardgoodShippingGroupInitializer extends
		HardgoodShippingGroupInitializer {
	
	/**
	 * holds ShoppingCart instance.
	 */
	private OrderHolder mShoppingCart;

	/**
	 * @return the shoppingCart
	 */
	public OrderHolder getShoppingCart() {
		return mShoppingCart;
	}

	/**
	 * @param pShoppingCart the shoppingCart to set
	 */
	public void setShoppingCart(OrderHolder pShoppingCart) {
		mShoppingCart = pShoppingCart;
	}
	/**
	 * This method is used for copying the configueration.
	 */
	@Override
	protected void copyConfiguration() {
		if (getConfiguration() != null) {
			super.copyConfiguration();
			if (getShoppingCart() == null) {
				setShoppingCart(getConfiguration().getShoppingCart());
			}
		}
	}
	/**
	 * This method is used for initializing Hard good shipping.
	 * @param pProfile - Profile
	 * @param pShippingGroupMapContainer - ShippingGroupMapContainer
	 * @param pRequest - Request
	 * @throws ShippingGroupInitializationException - ShippingGroupInitializationException.
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	protected void initializeHardgood(Profile pProfile,
			ShippingGroupMapContainer pShippingGroupMapContainer,
			DynamoHttpServletRequest pRequest)
			throws ShippingGroupInitializationException {
		super.initializeHardgood(pProfile, pShippingGroupMapContainer, pRequest);
		
		// updates last order first non-channel hard good shipping group to shipping group map container.
		String initBasedOnLastOrder = pRequest.getParameter(TRUConstants.INIT_BASED_ON_LAST_ORDER);
		if (Boolean.valueOf(initBasedOnLastOrder)) {
			TRUProfileTools profileTools = (TRUProfileTools) getShippingGroupManager().getOrderTools().getProfileTools();
			boolean isAnonymousUser = profileTools.isAnonymousUser(getProfile());
			if (!isAnonymousUser) {
				return;
			}
			TRUOrderImpl lastOrder = (TRUOrderImpl) getShoppingCart().getLast();
			TRUOrderImpl currentOrder = (TRUOrderImpl) getShoppingCart().getCurrent();
			if (lastOrder != null && !lastOrder.getShippingGroups().isEmpty() && !lastOrder.isPayPalOrder()) {
				List<ShippingGroup> shippingGroups = lastOrder.getShippingGroups();
				for (ShippingGroup shippingGroup : shippingGroups) {
					if (shippingGroup instanceof TRUHardgoodShippingGroup && 
							!(shippingGroup instanceof TRUChannelHardgoodShippingGroup)) {
						TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
						if (hardgoodShippingGroup != null &&
								StringUtils.isNotBlank(hardgoodShippingGroup.getNickName())) {
							pShippingGroupMapContainer.addShippingGroup(hardgoodShippingGroup.getNickName(), hardgoodShippingGroup);
							break;
						}
					}
				}
				if(currentOrder != null && StringUtils.isNotBlank(currentOrder.getBillingAddressNickName()) && currentOrder.getBillingAddress() != null) {
					try {
						TRUHardgoodShippingGroup billingAddressSG = (TRUHardgoodShippingGroup)getShippingGroupManager().createShippingGroup(getHardgoodShippingGroupType());
						if(billingAddressSG != null) {
							OrderTools.copyAddress(lastOrder.getBillingAddress(), billingAddressSG.getShippingAddress());
							billingAddressSG.setNickName(lastOrder.getBillingAddressNickName());
							billingAddressSG.setBillingAddress(Boolean.TRUE);
							pShippingGroupMapContainer.addShippingGroup(lastOrder.getBillingAddressNickName(), billingAddressSG);
						}
					} catch (CommerceException ce) {
						vlogDebug("CommerceException: TRUHardgoodShippingGroupInitializer.initializeHardgood() orderId: {0} Exception: {1}", currentOrder.getId(), ce);
					}
				}
			}
		}
	}

	/**
	 * updating profile nickName to the hard good shipping group.
	 * @param pAddressItem addressItem
	 * @param pProfile profile
	 * @return hardgoodShippingGroup
	 * @throws CommerceException CommerceException
	 * @throws PropertyNotFoundException PropertyNotFoundException
	 * @throws RepositoryException RepositoryException
	 */
	@Override
	protected ShippingGroup createHardgoodShippingGroup(
			RepositoryItem pAddressItem, Profile pProfile)
			throws CommerceException, PropertyNotFoundException,
			RepositoryException {
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getShippingGroupManager().getOrderTools()).getCommercePropertyManager();
		TRUOrderTools orderTools = (TRUOrderTools) getShippingGroupManager().getOrderTools();
		TRUPropertyManager propertyManager = orderTools.getPropertyManager();		
		Boolean isBillingAddress = (Boolean)pAddressItem.getPropertyValue(propertyManager.getIsBillingAddressPropertyName());			
		TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup)super.createHardgoodShippingGroup(pAddressItem, pProfile);
		String nickName = getShippingGroupManager().getOrderTools().getProfileTools()
				.getProfileAddressName(pProfile, pAddressItem);
		
		//Start :Added logic for seq in displaying the addresses
		Timestamp lastActivityTimeStamp= (Timestamp)pAddressItem.getPropertyValue(propertyManager.getLastActivityPropertyName());
		hardgoodShippingGroup.setLastActivity(lastActivityTimeStamp);
		//End :Added logic for seq in displaying the addresses		
		
		if(null != pAddressItem.getPropertyValue(commercePropertyManager.getBillingAddressIsSame())) {
			boolean billingAddressIsSame = (boolean) pAddressItem.getPropertyValue(commercePropertyManager.getBillingAddressIsSame());
			hardgoodShippingGroup.setBillingAddressIsSame(billingAddressIsSame);
		}
		if (StringUtils.isNotBlank(nickName)) {
			hardgoodShippingGroup.setNickName(nickName);
		}	
		if(isBillingAddress!= null){						
			hardgoodShippingGroup.setBillingAddress(isBillingAddress);
		}
		return hardgoodShippingGroup;
	}
}
