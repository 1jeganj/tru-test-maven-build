package com.tru.commerce.order.purchase;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.jms.JMSException;
import javax.servlet.ServletException;
import javax.transaction.Transaction;

import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.CreditCard;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupManager;
import atg.commerce.order.purchase.CommitOrderFormHandler;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.PricingConstants;
import atg.commerce.promotion.TRUPromotionTools;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.droplet.DropletFormException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.multisite.SiteContextManager;
import atg.nucleus.naming.ComponentName;
import atg.payment.PaymentStatus;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.service.pipeline.PipelineResult;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.claimable.TRUClaimableTools;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUBppItemInfo;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUOrderHolder;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUPaymentGroupManager;
import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.commerce.order.TRUPropertyNameConstants;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.TRUShippingGroupManager;
import com.tru.commerce.order.TRUShippingManager;
import com.tru.commerce.payment.TRUPaymentManager;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.commerce.payment.paypal.TRUPayPalStatus;
import com.tru.commerce.pricing.TRUItemPriceInfo;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.commerce.pricing.TRUTaxPriceInfo;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.TRUUrlUtil;
import com.tru.common.TRUUserSession;
import com.tru.common.cml.SystemException;
import com.tru.common.cml.ValidationError;
import com.tru.common.cml.ValidationExceptions;
import com.tru.common.vo.TRUAddressDoctorResponseVO;
import com.tru.errorhandler.fhl.IErrorHandler;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;
import com.tru.integrations.common.TRUEpslonBppInfoVO;
import com.tru.logging.TRUSETALogger;
import com.tru.messaging.TRUEpslonOrderAcknowledgeSource;
import com.tru.payment.paypal.TRUPaypalUtilityManager;
import com.tru.payment.radial.TRURadialPaymentProcessor;
import com.tru.radial.commerce.payment.paypal.TRUPaypalConstants;
import com.tru.radial.commerce.payment.paypal.util.TRUPayPalConstants;
import com.tru.radial.commerce.payment.paypal.util.TRUPayPalException;
import com.tru.radial.common.TRURadialConfiguration;
import com.tru.radial.common.beans.TRURadialError;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.radial.payment.paypal.PaypalConstants;
import com.tru.radial.payment.paypal.bean.GetExpressCheckoutResponse;
import com.tru.radial.payment.paypal.bean.SetExpressCheckoutResponse;
import com.tru.regex.EmailPasswordValidator;
import com.tru.userprofiling.TRUCookieManager;
import com.tru.userprofiling.TRUProfileManager;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.utils.TRUIntegrationStatusUtil;
import com.tru.utils.TRUSchemeDetectionUtil;
import com.tru.validator.fhl.IValidator;

/**
 * This class extends the atg.commerce.order.purchase.CommitOrderFormHandler.
 * It is used for the following operations.
 * 
 * @author PA
 * @version 1.0
 * 
 */
public class TRUCommitOrderFormHandler extends CommitOrderFormHandler {
	
	
	private static final String PAYPAL = "PAYPAL";

	/** The Constant TRU_SESSION_SCOPE_COMPONENT_NAME. */
	public static final ComponentName TRU_SESSION_SCOPE_COMPONENT_NAME = ComponentName
			.getComponentName(TRUCommerceConstants.TRU_SESSION_SCOPE_COMPONENT_NAME);

	//private static final String TRUOrderManager = null;
	/** Holds the pipe delimiter. */
	public static final String PIPE_DELIMETRE = "\\|";
	
	/** Holds the pipe delimiter. */
	public static final String UNDERSCORE = "_";

	/** The m validate shipping groups chain id. */
	private String mValidateShippingGroupsChainId;

	/** The m shipping manager. */
	private TRUShippingManager mShippingManager;
	/** Holds reference for PromotionTools. */
	private TRUPromotionTools mPromotionTools;

	/** Holds reference for mEpslonOrderAcknowledgeSource. */
	private TRUEpslonOrderAcknowledgeSource mEpslonOrderAcknowledgeSource;

	/** Holds reference for mTRUConfiguration. */
	private TRUConfiguration mTRUConfiguration;

	/** property to hold ValidationException. */
	private ValidationExceptions mVe = new ValidationExceptions();
	
	/**
	 * property to hold TRUApplePayForm
	 */
	private TRUApplePayForm mApplePayForm = new TRUApplePayForm();

	/**
	 * property to hold TRUCookieManager.
	 */
	private TRUCookieManager mCookieManager;

	/** property to hold OrderSetWithDefault. */
	private boolean mOrderSetWithDefault;

	/** property to hold mCreditCardNickName. */
	private String mCreditCardNickName;

	/** property: Reference to the ShippingProcessHelper component. */
	private TRUShippingProcessHelper mShippingHelper;

	/** property: Reference to the BillingProcessHelper component. */
	private TRUBillingProcessHelper mBillingHelper;

	/** property to hold reward number of profile. */
	private String mRewardNumber;

	/** property to hold Reward Number Valid flag. */
	private String mRewardNumberValid;

	/** property to hold email address in order level. */
	private String mEmail;

	/** property to hold user session component. */
	private TRUUserSession mUserSession;

	/** Property to Hold Error handler manager. */
	private DefaultErrorHandlerManager mErrorHandlerManager;


	/** The m radial configuration. */
	private TRURadialConfiguration mRadialConfiguration;
	
	/** The Payment group type. */
	private String mPaymentGroupType;

	/** Property to hold if RUS radio is selected. */
	private boolean mRUSCard;

	/**
	 * property to hold mRestService.
	 */
	private boolean mRestService;
	/**
	 * property to hold mPaypalRedirectURL.
	 */
	private String mPaypalRedirectURL;

	/**
	 * property to hold PropertyManager.
	 */
	private TRUPropertyManager mPropertyManager;

	/**
	 * property to hold integrationStatusUtil.
	 */
	private TRUIntegrationStatusUtil mIntegrationStatusUtil;
	/**
	 * This property holds PayPalUtilityManager.
	 */
	private TRUPaypalUtilityManager mPayPalUtilityManager;

	/** Property to hold mMoveToConfirmationChainId. */
	private String mMoveToConfirmationChainId;

	/** Holds address values. */
	private ContactInfo mAddressInputFields = new ContactInfo();

	/** map to hold credit card info. */
	private Map<String, String> mCreditCardInfoMap = new HashMap<String, String>();
	/** property to hold the mErrorHandler. */
	private IErrorHandler mErrorHandler;

	/** property to hold the mValidator. */
	private IValidator mValidator;

	/** Property to holds mEmailPasswordValidator. */
	private EmailPasswordValidator mEmailPasswordValidator;

	/**
	 * Holds String page name.
	 */
	private String mPageName;

	/**
	 * Holds String PayerID.
	 */
	private String mPayerId;
	/** property to hold BillingAddressNickName. */
	private String mBillingAddressNickName;

	/** Property to hold Selected Billing Address . */
	private String mSelectedCardBillingAddress;
	
	/** member holds the mAddressDoctorProcessStatus. */
 	private String mAddressDoctorProcessStatus;
 	
 	/** member holds the mAddressDoctorResponseVO. */
 	private TRUAddressDoctorResponseVO mAddressDoctorResponseVO;
 	
 	/** boolean property to hold if address is validated by address doctor or not. */
 	private String mAddressValidated;
 	
 	/**
	 * This property holds SETALogger component ref.
	 */
 	private TRUSETALogger mSetaLogger;
 	
 	/** Property to hold the profile tools */
 	private TRUProfileTools mProfileTools;
 	
 	/** The Radial payment processor. */
	private TRURadialPaymentProcessor mRadialPaymentProcessor;
	
	/**
	 * Property to hold mEwasteSurchargeSkuPropertyName.
	 */
	private String mEwasteSurchargeSkuPropertyName;
	
	/**
	 * Gets the ewaste surcharge sku property name.
	 * 
	 * @return the ewasteSurchargeSkuPropertyName
	 */
	public String getEwasteSurchargeSkuPropertyName() {
		return mEwasteSurchargeSkuPropertyName;
	}

	/**
	 * Sets the ewaste surcharge sku property name.
	 * 
	 * @param pEwasteSurchargeSkuPropertyName
	 *            the ewasteSurchargeSkuPropertyName to set
	 */
	public void setEwasteSurchargeSkuPropertyName(
			String pEwasteSurchargeSkuPropertyName) {
		mEwasteSurchargeSkuPropertyName = pEwasteSurchargeSkuPropertyName;
	}

	
	/**
	
	 * @return the radialPaymentProcessor
	 */
	public TRURadialPaymentProcessor getRadialPaymentProcessor() {
		return mRadialPaymentProcessor;
	}

	/**
	 * @param pRadialPaymentProcessor the radialPaymentProcessor to set
	 */
	public void setRadialPaymentProcessor(
			TRURadialPaymentProcessor pRadialPaymentProcessor) {
		mRadialPaymentProcessor = pRadialPaymentProcessor;
	}

	/**
	 * @return the profileTools
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * @param pProfileTools the profileTools to set
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	/**
	 * @return the setaLogger
	 */
	public TRUSETALogger getSetaLogger() {
		return mSetaLogger;
	}

	/**
	 * @param pSetaLogger the setaLogger to set
	 */
	public void setSetaLogger(TRUSETALogger pSetaLogger) {
		mSetaLogger = pSetaLogger;
	}
	
	/** property to Hold financeAgreed. */
	private boolean mNoFinanceAgreed;
	
	/**
	 * @return the noFinanceAgreed
	 */
	public boolean isNoFinanceAgreed() {
		return mNoFinanceAgreed;
	}

	/**
	 * @param pNoFinanceAgreed the noFinanceAgreed to set
	 */
	public void setNoFinanceAgreed(boolean pNoFinanceAgreed) {
		mNoFinanceAgreed = pNoFinanceAgreed;
	}
	
	/** The Device id. */
	public String mDeviceID;
	
	/** The Browser Accept Encoding. */
	public String mBrowserAcceptEncoding;

	/**
	 * @return the browserAcceptEncoding
	 */
	public String getBrowserAcceptEncoding() {
		return mBrowserAcceptEncoding;
	}

	/**
	 * @param pBrowserAcceptEncoding the browserAcceptEncoding to set
	 */
	public void setBrowserAcceptEncoding(String pBrowserAcceptEncoding) {
		mBrowserAcceptEncoding = pBrowserAcceptEncoding;
	}

	/**
	 * @return the deviceID
	 */
	public String getDeviceID() {
		return mDeviceID;
	}

	/**
	 * @param pDeviceID the deviceID to set
	 */
	public void setDeviceID(String pDeviceID) {
		mDeviceID = pDeviceID;
	}

	/**
	 * add credit card.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 * @throws CommerceException
	 *             CommerceException
	 */
	@SuppressWarnings("unchecked")
	protected void addCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUBillingInfoFormHandler.addCreditCard method");
		}
		final TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
		final TRUPropertyManager propertyManager = (TRUPropertyManager) profileTools.getPropertyManager();
		String billingAddressNickName=null;
		String generatedUniqueNickName = null;
		boolean usedGeneratedNickName = Boolean.FALSE;
		
		if (StringUtils.isBlank(getBillingAddressNickName())) {
			Map<String, ShippingGroup> shippingGroups  = getShippingGroupMapContainer().getShippingGroupMap();
			for (String nickName : shippingGroups.keySet()) {
				ShippingGroup shippingGroup = shippingGroups.get(nickName);
				if(shippingGroup instanceof TRUHardgoodShippingGroup){
					boolean billingAddressIsSame = ((TRUHardgoodShippingGroup) shippingGroup).isBillingAddressIsSame();
					if(billingAddressIsSame){
						billingAddressNickName = nickName;
					}
				}
			}
			generatedUniqueNickName = orderManager.generateUniqueNickNameForAddress(getAddressInputFields(), getShippingGroupMapContainer().getShippingGroupMap());
		} else {
			billingAddressNickName = getBillingAddressNickName();
		}
		if (getShippingGroupMapContainer().getShippingGroup(billingAddressNickName) != null) {
			if(orderManager.getShipGroupByNickName(getOrder(), billingAddressNickName) != null){
				Address orderShipingAddress = null;
				if(orderManager.getShipGroupByNickName(getOrder(), billingAddressNickName) instanceof TRUHardgoodShippingGroup){
					orderShipingAddress = orderManager.getShipGroupByNickName(getOrder(), billingAddressNickName).getShippingAddress();
				}
				Address currentInputAddress = getAddressInputFields();
				boolean addressMatching = profileTools.compareAddresses(currentInputAddress, orderShipingAddress);
				if(!addressMatching){
					getShippingHelper().addShippingAddress(getProfile(), generatedUniqueNickName, 
							getAddressInputFields(), getShippingGroupMapContainer());
					usedGeneratedNickName = Boolean.TRUE;
				}
			} else {
				// updating address in shipping group map container and also in
				// profile for logged in user
				getShippingHelper().modifyShippingAddress(getProfile(), billingAddressNickName, getAddressInputFields(),
						getShippingGroupMapContainer(), getOrder());
				usedGeneratedNickName = Boolean.FALSE;
			}
		} else {
			// adding address in shipping group map container and also in
			// profile for logged in user
			getShippingHelper().addShippingAddress(getProfile(), generatedUniqueNickName, getAddressInputFields(),
					getShippingGroupMapContainer());
			usedGeneratedNickName = Boolean.TRUE;
		}

		// updating credit card type
		String creditCardType = TRUConstants.EMPTY;
		if (!StringUtils.isBlank((String) getCreditCardInfoMap().get(TRUConstants.CARD_TYPE))
				&& !TRUConstants.CARD_TYPE_UNDEFINED.equalsIgnoreCase((String) getCreditCardInfoMap().get(TRUConstants.CARD_TYPE))) {
			creditCardType = (String) getCreditCardInfoMap().get(TRUConstants.CARD_TYPE);
		} else {
			creditCardType = profileTools.getCreditCardTypeFromRepository(
					(String) getCreditCardInfoMap().get(propertyManager.getCreditCardNumberPropertyName()), (String) getCreditCardInfoMap()
							.get(TRUConstants.CARD_LENGTH));
		}
		if (isLoggingDebug()) {
			vlogDebug("value of credit card type calculated from getOrderTools().getCreditCardType() creditCardType: {0} ", creditCardType);
		}
		getCreditCardInfoMap().put(propertyManager.getCreditCardTypePropertyName(), creditCardType);
		if(usedGeneratedNickName){
			getBillingHelper().addCreditCard(getOrder(), getProfile(), getCreditCardInfoMap(), generatedUniqueNickName, getAddressInputFields(),
					getPaymentGroupMapContainer(), false);
		} else {
			getBillingHelper().addCreditCard(getOrder(), getProfile(), getCreditCardInfoMap(), billingAddressNickName, getAddressInputFields(),
					getPaymentGroupMapContainer(), false);
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUBillingInfoFormHandler.addCreditCard method");
		}
	}
	

	/**
	 * Commit orader.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void commitOrder(Order pOrder, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
	IOException {
		if (isLoggingDebug()) {
			vlogDebug("START TRUCommitOrderFormHandler:: commitOrder method");
		}
		if (getFormError()) {
			return;
		}
		final String myHandleMethod = TRUCheckoutConstants.COMMIT_ORDER;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(myHandleMethod);
		}
		TRUOrderImpl order = (TRUOrderImpl) getOrder();
		// START: To avoid synchronization exceptions
		order.setCartItemDetailVOMap(null);
		order.setSplitGiftItems(null);
		order.setGiftOptionsDetails(null);
		order.setShipmentVO(null);
		((TRUOrderManager)getOrderManager()).updateOrderWithFraudParameters(pRequest, (TRUOrderImpl)getOrder(), getProfile(), getDeviceID(), 
				getUserSession().getUserSessionStartTime(),isRestService(), getBrowserAcceptEncoding());
		// END: To avoid synchronization exceptions
		super.commitOrder(pOrder, pRequest, pResponse);
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(myHandleMethod);
		}
		if (isLoggingDebug()) {
			vlogDebug("END TRUCommitOrderFormHandler:: commitOrder method");
		}
	}

	/**
	 * Commit order from payment.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws CommerceException
	 *             the commerce exception
	 */
	public void commitOrderFromPayment(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START TRUCommitOrderFormHandler:: commitOrderFromPayment method");
		}
		TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
		//TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		
		String billingAddressNickName = getBillingAddressNickName();
		if(isNewBillingAddress()) {
			ContactInfo billingAddress  = new ContactInfo();
			OrderTools.copyAddress(getAddressInputFields(), billingAddress);
			//Adding the billing address to the credit card. 
			((TRUOrderImpl)getOrder()).setBillingAddress(billingAddress);
			String generatedUniqueNickName = orderManager.generateUniqueNickNameForAddress(getAddressInputFields(), getShippingGroupMapContainer().getShippingGroupMap());
			String nickName = null;
			if(generatedUniqueNickName.length() > TRUConstants.NICKNAME_MAX_LENGTH){
				nickName = generatedUniqueNickName.substring(TRUConstants.NICKNAME_MIN_LENGTH, TRUConstants.NICKNAME_MAX_LENGTH);
			} else {
				nickName = generatedUniqueNickName;
			}
			((TRUOrderImpl)getOrder()).setBillingAddressNickName(nickName);
			final ShippingGroupManager sgm = getPurchaseProcessHelper().getShippingGroupManager();
			final TRUHardgoodShippingGroup hgsg = (TRUHardgoodShippingGroup) sgm.createShippingGroup();	   
			final Address address = hgsg.getShippingAddress();	    
			OrderTools.copyAddress(getAddressInputFields(), address);
			hgsg.setBillingAddress(Boolean.TRUE);
			hgsg.setNickName(nickName);
			billingAddressNickName = nickName;
			hgsg.setLastActivity(new Timestamp((new Date()).getTime()));
			getShippingGroupMapContainer().addShippingGroup(nickName, hgsg);
			setBillingAddressNickName(nickName);
		} else if (isEditBillingAddress()){
			Address billingAddress  = ((TRUOrderImpl)getOrder()).getBillingAddress();
			OrderTools.copyAddress(getAddressInputFields(), billingAddress);
			//Adding the billing address to the credit card. 
			billingAddressNickName = ((TRUOrderImpl)getOrder()).getBillingAddressNickName();
			final TRUHardgoodShippingGroup containerShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupMapContainer().
					getShippingGroup(billingAddressNickName);  
			setBillingAddressNickName(billingAddressNickName);
			OrderTools.copyAddress(getAddressInputFields(), containerShippingGroup.getShippingAddress());
		}
		
		if(!TRUCheckoutConstants.CREDITCARD.equalsIgnoreCase(getPaymentGroupType())) {
			return;
		}
		// For Credit card
		double giftCardAmtApplied = ((TRUOrderTools) (orderManager.getOrderTools())).getGiftCardAppliedAmount(getOrder());
		double orderTotal = TRUCheckoutConstants.DOUBLE_ZERO;
		if(null != getOrder() && null != getOrder().getPriceInfo()){
			orderTotal = roundTwoPlaceDecimal(getOrder().getPriceInfo().getTotal(), TRUConstants.TWO);
		}
		//orderTotal = roundTwoPlaceDecimal(getOrder().getPriceInfo().getTotal(), TRUConstants.TWO);
		double balnceAmount = orderTotal - giftCardAmtApplied;
		if(balnceAmount == TRUCommerceConstants.DOUBLE_ZERO) {
				return;
		}
		// Getting Credit card
		TRUCreditCard creditCard = (TRUCreditCard) ((TRUOrderManager) getOrderManager()).getCreditCard(getOrder());
		if (creditCard == null) {
			if (isLoggingError()) {
				logError("Exception in @Class::TRUCommitOrderFormHandler::@method::commitOrderFromPayment(): Credit Card is NULL");
			}
			return;
		}
		
		if(isRUSCard()){
			/*Start - MVP for R Us card Billing address*/
			getUserSession().setTSPBillingAddressMap(getAddressInputFields());
			getUserSession().setTSPEditBillingAddress(true);
			getUserSession().setInputEmail(null);
			/*End - MVP for R Us card Billing address*/
			getBillingHelper().updateCreditCard(getOrder(), getCreditCardInfoMap(), getAddressInputFields());
			creditCard.setBillingAddressNickName(((TRUOrderImpl)getOrder()).getBillingAddressNickName());
		}
		
		if(isNewCreditCard()) {
			if(!isRUSCard()){
				// update credit card form data to container
				addCreditCard(pRequest, pResponse);
			}
			// updating order credit card
			getBillingHelper().updateCreditCard(getOrder(), getCreditCardInfoMap(), getAddressInputFields());
			creditCard.setBillingAddressNickName(((TRUOrderImpl)getOrder()).getBillingAddressNickName());
		} else if(isEditCreditCard()) {
			billingAddressNickName = ((TRUOrderImpl)getOrder()).getBillingAddressNickName();
			getBillingHelper().editCreditCard(getProfile(), getCreditCardInfoMap(), getEditCardNickName(), 
					billingAddressNickName, getAddressInputFields(), getPaymentGroupMapContainer());
			CreditCard containerCreditCard = (CreditCard) getPaymentGroupMapContainer().getPaymentGroup(getEditCardNickName());
			TRUCreditCard orderCreditCard = (TRUCreditCard) orderManager.getCreditCard(getOrder());
			getPurchaseProcessHelper().copyCreditCardInfo(containerCreditCard, orderCreditCard);
		}
		
		final CreditCard orderCreditCard = ((TRUOrderManager) getOrderManager()).getCreditCard(getOrder());
		if (orderCreditCard != null) {
			if (!getFormError() && StringUtils.isBlank(orderCreditCard.getBillingAddress().getAddress1())) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
				getErrorHandler().processException(mVe, this);
			}
		} else {
			if (isLoggingError()) {
				logError("Exception in @Class::TRUCommitOrderFormHandler::@method::validateCommitOrderFromPayment(): Credit Card is NULL");
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("END TRUCommitOrderFormHandler:: commitOrderFromPayment method");
		}
	}

	/**
	 * This method is used to add calculate tax parameter.
	 * 
	 * @return Map
	 */
	protected Map createRepriceParameterMaps() {
		if (isLoggingDebug()) {
			vlogDebug("TRUCommitOrderFormHandler (createRepriceParameterMapToSkipTaxCal) : BEGIN");
		}
		Map parameters;
		parameters = super.createRepriceParameterMap();
		if (parameters == null) {
			parameters = new HashMap();
		}
		parameters.put(TRUTaxwareConstant.CALC_TAX, Boolean.TRUE);
		if (isLoggingDebug()) {
			vlogDebug("TRUCommitOrderFormHandler (createRepriceParameterMapToSkipTaxCal) : END");
		}
		return parameters;
	}

	/**
	 * Ensure email address.
	 * 
	 * @throws ServletException
	 *             the servlet exception
	 */
	private void ensureEmailAddress() throws ServletException {
		if (isLoggingDebug()) {
			vlogDebug("START TRUCommitOrderFormHandler:: ensureEmailAddress method");
		}
		try {
			if (getProfile().isTransient()) {
				/*
				 * To validate email when user is not logged in along with email
				 * format
				 */
				if (StringUtils.isBlank(getEmail())) {
					if (isLoggingDebug()) {
						vlogDebug("TRUBillingInfoFormHandler.ensureEmailAddress is having errors");
					}
					getValidator().validate(TRUConstants.MY_ACCOUNT_UPDATE_EMAIL_FORM, this);
					
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
					getErrorHandler().processException(mVe, this);
				} else if (!getFormError() && StringUtils.isNotBlank(getEmail()) && !getEmailPasswordValidator().validateEmail(getEmail())) {
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_INVALID_EMAIL, null);
					getErrorHandler().processException(mVe, this);
				}
				if (getFormError()) {
					return;
				}
				((TRUOrderImpl) getOrder()).setEmail(getEmail().toLowerCase());
			} else {
				((TRUOrderImpl) getOrder()).setEmail((String) getProfile().getPropertyValue(
						getCommercePropertyManager().getEmailAddressPropertyName()));
			}
		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			if (isLoggingError()) {
				logError("Validation exception occured: @Class::TRUCommitOrderFormHandler::@method::setPayPalConfiguration()", ve);
			}
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			if (isLoggingError()) {
				logError("System exception occured: @Class::TRUCommitOrderFormHandler::@method::setPayPalConfiguration()", se);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END TRUCommitOrderFormHandler:: ensureEmailAddress method");
		}
	}

	/**
	 * @return This is a map that stores the value of address properties.
	 */
	public ContactInfo getAddressInputFields() {
		return mAddressInputFields;
	}

	/**
	 * Sets property AddressValue map.
	 * 
	 * @param pAddressValue
	 *            addressValue
	 **/
	public void setAddressInputFields(ContactInfo pAddressValue) {
		mAddressInputFields = pAddressValue;
	}

	/**
	 * @return mBillingAddressNickName
	 */
	public String getBillingAddressNickName() {
		return mBillingAddressNickName;
	}

	/**
	 * @return the billingHelper
	 */
	public TRUBillingProcessHelper getBillingHelper() {
		return mBillingHelper;
	}

	/**
	 * @return the cookieManager
	 */
	public TRUCookieManager getCookieManager() {
		return mCookieManager;
	}

	/**
	 * @return the creditCardInfoMap
	 */
	public Map<String, String> getCreditCardInfoMap() {
		return mCreditCardInfoMap;
	}

	/**
	 * @return mCreditCardNickName credit card nick name
	 */
	public String getCreditCardNickName() {
		return mCreditCardNickName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return mEmail;
	}

	/**
	 * @return the emailPasswordValidator
	 */
	public EmailPasswordValidator getEmailPasswordValidator() {
		return mEmailPasswordValidator;
	}

	/**
	 * This method is used to get epslonOrderAcknowledgeSource.
	 * 
	 * @return epslonOrderAcknowledgeSource TRUEpslonOrderAcknowledgeSource
	 */
	public TRUEpslonOrderAcknowledgeSource getEpslonOrderAcknowledgeSource() {
		return mEpslonOrderAcknowledgeSource;
	}

	/**
	 * @return the IErrorHandler
	 */
	public IErrorHandler getErrorHandler() {
		return mErrorHandler;
	}

	/**
	 * Gets the error handler manager.
	 * 
	 * @return the error handler manager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * @return the integrationStatusUtil
	 */
	public TRUIntegrationStatusUtil getIntegrationStatusUtil() {
		return mIntegrationStatusUtil;
	}

	/**
	 * This method gets moveToConfirmationChainId.
	 * 
	 * @return the moveToConfirmationChainId
	 */
	public String getMoveToConfirmationChainId() {
		return mMoveToConfirmationChainId;
	}

	/**
	 * @return the pageName
	 */
	public String getPageName() {
		return mPageName;
	}

	/**
	 * @return the mPayerId.
	 */
	public String getPayerId() {
		return mPayerId;
	}

	/**
	 * @return the paymentGroupType
	 */
	public String getPaymentGroupType() {
		return mPaymentGroupType;
	}

	/**
	 * Gets the radial configuration.
	 *
	 * @return the mRadialConfiguration
	 */
	public TRURadialConfiguration getRadialConfiguration() {
		return mRadialConfiguration;
	}

	/**
	 * Sets the radial configuration.
	 *
	 * @param pRadialConfiguration the new radial configuration
	 */
	public void setRadialConfiguration(TRURadialConfiguration pRadialConfiguration) {
		this.mRadialConfiguration = pRadialConfiguration;
	}
	/**
	 * This method return payPal Error key for error scenario from TRUPaypalStatus object.
	 * 
	 * @return payPalErrKey - the String
	 */
	public String getPayPalErrorKey() {
		if (isLoggingDebug()) {
			vlogDebug("START  of TRUCommitOrderFormHandler :: getPayPalErrorKey()");
		}
		List<PaymentStatus> paymentStatus = null;
		// fetching PAYPAL Payment Group
		TRUPayPal truPayPal = ((TRUOrderManager) getOrderManager()).fetchPayPalPaymentGroupForOrder(getOrder());
		if (truPayPal != null) {
			paymentStatus = truPayPal.getAuthorizationStatus();
		}
		TRURadialError radialError = null;
		if (paymentStatus != null && !paymentStatus.isEmpty()) {
			for (PaymentStatus status : paymentStatus) {
				if (status instanceof TRUPayPalStatus) {
					radialError = ((TRUPayPalStatus) status).getRadialError();
					break;
				}
			}
		}
		String payPalErrKey = TRUErrorKeys.TRU_PAYPAL_GENERIC_ERR_KEY;
		if(radialError !=null && StringUtils.isNotBlank(radialError.getErrorCode())){
			payPalErrKey = PaypalConstants.PAYPAL_PAYMENT_AUTH_ERROR;
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUCommitOrderFormHandler :: getPayPalErrorKey() : {0} " ,payPalErrKey);
		}
		return payPalErrKey;
	}

	/**
	 * @return the paypalRedirectURL
	 */
	public String getPaypalRedirectURL() {
		return mPaypalRedirectURL;
	}

	/**
	 * Gets the mPayPalUtilityManager.
	 * 
	 * @return the mPayPalUtilityManager
	 */

	public TRUPaypalUtilityManager getPayPalUtilityManager() {
		return mPayPalUtilityManager;
	}

	/**
	 * @return the promotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @return the mRewardNumber
	 */
	public String getRewardNumber() {
		return mRewardNumber;
	}

	/**
	 * @return the mRewardNumberValid
	 */
	public String getRewardNumberValid() {
		return mRewardNumberValid;
	}

	/**
	 * @return the selectedCardBillingAddress
	 */
	public String getSelectedCardBillingAddress() {
		return mSelectedCardBillingAddress;
	}

	/**
	 * @return the shippingHelper
	 */
	public TRUShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * @return the shippingManager.
	 */
	public TRUShippingManager getShippingManager() {
		return mShippingManager;
	}

	/**
	 * This method is used to get tRUConfiguration.
	 * 
	 * @return tRUConfiguration TRUConfiguration
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * Gets the user session.
	 * 
	 * @return the userSession
	 */
	public TRUUserSession getUserSession() {
		return mUserSession;
	}

	/**
	 * @return the validateShippingGroupsChainId.
	 */
	public String getValidateShippingGroupsChainId() {
		return mValidateShippingGroupsChainId;
	}

	/**
	 * @return the IValidator
	 */
	public IValidator getValidator() {
		return mValidator;
	}
	
	/**
	 * This method place the order if paid via PAYPAL from cartPage and
	 * internally calls doAuth/doVoid PAYPAL method.
	 * 
	 * @param pRequest
	 *            - type of DynamoHttpServletRequest
	 * @param pResponse
	 *            - type of DynamoHttpServletResponse
	 * @return form handler
	 * @throws ServletException
	 *             - type of ServletException
	 * @throws IOException
	 *             - type of IOException
	 */

	public boolean handleCartExpressCheckoutWithPayPal(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUCommitOrderFormHandler : handleCartExpressCheckoutWithPayPal() method ");
		}
		boolean rollbackFlag = false;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String myHandleMethod = TRUPayPalConstants.PAYPAL_COMMIT_HANDLER_CART_EXPRESS_CHECKOUT;
		TransactionDemarcation td = new TransactionDemarcation();
		String errorURL = TRUUrlUtil.getConstructedUrl(pRequest, getSchemeDetectionUtil().getScheme(),getTRUConfiguration().getCartCheckoutWithPayPalErrorURL(),null);
		String successURL = TRUUrlUtil.getConstructedUrl(pRequest, getSchemeDetectionUtil().getScheme(), getTRUConfiguration().getCheckoutWithPayPalSuccessURL(),null);
		
		if (isLoggingDebug()) {
			logDebug("PayPal Return Success URL :: "+successURL+ " PayPal Return Failure URL "+errorURL);
		}

		setCommitOrderErrorURL(errorURL);
		setCommitOrderSuccessURL(successURL);
		String payPalErrMessage = null;
		String payPalErrKey = TRUErrorKeys.TRU_PAYPAL_GENERIC_ERR_KEY;
		TRUPaymentGroupManager paymentGroupManager = (TRUPaymentGroupManager) getPaymentGroupManager();
		
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			if (PerformanceMonitor.isEnabled()) {
				PerformanceMonitor.startOperation(myHandleMethod);
			}
			try {
				if (!StringUtils.isBlank(payPalErrKey)) {
					payPalErrMessage = getErrorHandlerManager().getErrorMessage(payPalErrKey);
				}
				td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
				// run move to confirmation pipeline chain

				runProcessMoveToConfirmation(pRequest, pResponse);
				TRUPayPal payPalPG = ((TRUOrderManager) getOrderManager()).fetchPayPalPaymentGroupForOrder(getOrder());
				double amount = paymentGroupManager.getRemainingOrderAmount(getOrder());
				if (payPalPG != null) {
					synchronized (getOrder()) {
						// Validate order before placing same as OOTB
						validateCommitOrder(pRequest, pResponse);
						if (getFormError()) {
							return checkFormRedirect(getCommitOrderSuccessURL(), getCommitOrderErrorURL(), pRequest, pResponse);
						}
						getOrderManager().removeRemainingOrderAmountFromPaymentGroup(getOrder(), payPalPG.getId());
						payPalPG.setAmount(amount);
						getOrderManager().addRemainingOrderAmountToPaymentGroup(getOrder(), payPalPG.getId());
						// Custom TRU validation
						truValidateForCheckoutChain();												
						getOrderManager().updateOrder(getOrder());
						if (getFormError()) {
							return checkFormRedirect(getCommitOrderSuccessURL(), getCommitOrderErrorURL(), pRequest, pResponse);
						}
						// process commit order
						commitOrder(getOrder(), pRequest, pResponse);
						postCommitOrder(pRequest, pResponse);
						if (!getFormError()) {
							getUserSession().setPaypalPaymentDetailsMap(null);
							getUserSession().setTSPCardInfoMap(null);
						}
						if (getRadialConfiguration().isEnableDoVoid()) {
							addFormException(new DropletException(payPalErrMessage));
						}
					}
				}
			} catch (RunProcessException rpE) {
				rollbackFlag = true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("RunProcessException occured during PayPal  @Class::TRUCommitOrderFormHandler::@method::"
							+ "handleCartExpressCheckoutWithPayPal() payment for order Id : " + getOrder().getId(), rpE);
				}
			} catch (CommerceException cE) {
				rollbackFlag = true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("RunProcessException occured during PayPal payment @Class::TRUCommitOrderFormHandler::@method::"
							+ "handleCartExpressCheckoutWithPayPal() for order Id : " + getOrder().getId(), cE);
				}
			} catch (SystemException sE) {
				rollbackFlag = true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("SystemException occured during PayPal payment @Class::TRUCommitOrderFormHandler::@method::"
							+ "handleCartExpressCheckoutWithPayPal() for order Id : " + getOrder().getId(), sE);
				}
			} catch (TransactionDemarcationException tdE) {
				rollbackFlag = true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("TransactionDemarcationException occurred @Class::TRUCommitOrderFormHandler::@method::"
							+ "handleCartExpressCheckoutWithPayPal()", tdE);
				}
			} finally {
				try {
					td.end(rollbackFlag);
				} catch (TransactionDemarcationException e) {
					if(isLoggingError()){
						vlogError("TransactionDemarcationException for Order {0} While committing the transaction {1}", getOrder().getId(), e);	
					}
				}
				handleError(pRequest, pResponse, rollbackFlag, td);				
			}
		}
		if (PerformanceMonitor.isEnabled()){
			PerformanceMonitor.endOperation(myHandleMethod);
		}
		if (rrm != null) {
			rrm.removeRequestEntry(myHandleMethod);
		}
			vlogDebug("End of TRUCommitOrderFormHandler : handleCartExpressCheckoutWithPayPal() method ");
		return checkFormRedirect(getCommitOrderSuccessURL(), getCommitOrderErrorURL(), pRequest, pResponse);
	}

	/**
	 * @param pRequest - pRequest
	 * @param pResponse - pResponse
	 * @param pRollbackFlag - pRollbackFlag
	 * @param pTd - pTd
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	private void handleError(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse, boolean pRollbackFlag,
			TransactionDemarcation pTd) throws ServletException, IOException {		
		try {
			if (getFormError() || pRollbackFlag) {
				try {
					// calling PAYPAL doVoid Request in case of failure
					if (getRadialConfiguration().isEnableDoVoid()) {
						runDoVoidPayPalProcessorChain(pRequest, pResponse);
					}
					//resetFormExceptions();
					// display error message on page
					//String payPalErrKey = getPayPalErrorKey();
					// get the message corresponding to key and add to form exceptions
					//String payPalErrMessage = getErrorHandlerManager().getErrorMessage(payPalErrKey);
					//addFormException(new DropletException(payPalErrMessage));
					/*if (isLoggingError()) {
						logError("Paypal Error Message @Class::TRUCommitOrderFormHandler::@method::handleCartExpressCheckoutWithPayPal()"
								+ payPalErrMessage);
					}*/
					if (getFormError() || pRollbackFlag) {
						removePaypalPGFromOrder();
					}
				} catch (RunProcessException rpE) {
					if (isLoggingError()) {
						logError(
								"RunProcessException occured during payPal doVoid payment @Class::TRUCommitOrderFormHandler::@method::"
										+ "handleCartExpressCheckoutWithPayPal() for order Id : " + getOrder().getId(), rpE);
					}
				} catch (CommerceException cE) {
					if (isLoggingError()) {
						logError(
								"CommerceException occured during payPal doVoid payment @Class::TRUCommitOrderFormHandler::@method::"
										+ "handleCartExpressCheckoutWithPayPal() for order Id : " + getOrder().getId(), cE);
					}
				}/* catch (SystemException sE) {
					if (isLoggingError()) {
						logError("SystemException occured during payPal payment @Class::TRUCommitOrderFormHandler::@method::"
								+ "handleCartExpressCheckoutWithPayPal() for order Id : " + getOrder().getId(), sE);
					}
				}*/
				
			}
			/*if (pTd != null) {
				pTd.end(pRollbackFlag);
			}*/
		} /*catch (TransactionDemarcationException tdE) {
			if (isLoggingError()) {
				logError(
						"TransactionDemarcationException occured during payPal payment @Class::TRUCommitOrderFormHandler::@method::"
								+ "handleCartExpressCheckoutWithPayPal() for order Id : " + getOrder().getId(), tdE);
			}
		}*/
		finally{
			
		}
	}

	/**
	 * This method does a SetExpressCheckout API call to fetch token.
	 * 
	 * @param pRequest
	 *            - the pRequest
	 * @param pResponse
	 *            - the pResponse
	 * @return boolean -true or false
	 * @throws SystemException
	 *             -system exception
	 * @throws ServletException
	 *             -servlet exception
	 * @throws IOException
	 *             -io exception
	 */

	public boolean handleCheckoutWithPayPalFromOrderReview(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, SystemException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUCommitOrderFormHandler : handleCheckoutWithPayPalFromOrderReview() method ");
		}
		String payPalToken = null;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String myHandleMethod = TRUPayPalConstants.PAYPAL_CHECKOUT_ORDERREVIEW_HANDLER_METHOD_NAME;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(myHandleMethod);
		}
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			final TRUUserSession userSession = getUserSession();
			if (userSession != null && userSession.getPaypalPaymentDetailsMap() != null
					&& !userSession.getPaypalPaymentDetailsMap().isEmpty()) {
				payPalToken = getUserSession().getPaypalPaymentDetailsMap().get(TRUPayPalConstants.PAYPAL_TOKEN);
				if (isLoggingDebug()) {
					vlogDebug("payPalToken = " + payPalToken);
				}
				if (isRestService()) {
					setPaypalRedirectURL(getRadialConfiguration().getPaypalRedirectURL() + payPalToken
							+ getRadialConfiguration().getPayPalCommitParam());
				}
			}
		}
		if (rrm != null) {
			rrm.removeRequestEntry(myHandleMethod);
		}
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(myHandleMethod);
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUCommitOrderFormHandler : handleCheckoutWithPayPalFromOrderReview() method ");
		}
		if (isRestService()) {
			return checkFormRedirect(getCommitOrderSuccessURL(), getCommitOrderErrorURL(), pRequest, pResponse);
		} else {
			return false;
		}
	}

	/**
	 * This method place the order & internally calls doAuth PAYPAL method.
	 * 
	 * @param pRequest
	 *            - the DynamoHttpServletRequest
	 * @param pResponse
	 *            - the DynamoHttpServletResponse
	 * 
	 * @return form handler
	 * @throws SystemException
	 *             -SystemException
	 * @throws ServletException
	 *             -ServletException
	 * @throws IOException
	 *             -IOException
	 */

	public boolean handleExpressCheckoutWithPayPal(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
			vlogDebug("Start of TRUCommitOrderFormHandler : handleExpressCheckoutWithPayPal() method ");
		boolean rollbackFlag = false;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String myHandleMethod = TRUPayPalConstants.PAYPAL_HANDLER_EXPRESS_CHECKOUT;
		final TransactionDemarcation transactionDemarcation = new TransactionDemarcation();
		final String errorURL = TRUUrlUtil.getConstructedUrl(pRequest, getSchemeDetectionUtil().getScheme(), getTRUConfiguration().getCheckoutWithPayPalErrorURL(),null);
		final String successURL = TRUUrlUtil.getConstructedUrl(pRequest, getSchemeDetectionUtil().getScheme(), getTRUConfiguration().getCheckoutWithPayPalSuccessURL(),null);
		if (isLoggingDebug()) {
			logDebug("PayPal Return Success URL :: "+successURL+ " PayPal Return Failure URL "+errorURL);
		}
		if (!isRestService()) {
			setCommitOrderErrorURL(errorURL);
			setCommitOrderSuccessURL(successURL);
		}
		boolean ignoreFinallyBlock =false;
		ensureAllShipMethods();
		if (getFormError()) {
			ignoreFinallyBlock = true;
			return false;
		}	
		final TRUPaymentGroupManager paymentGroupManager = (TRUPaymentGroupManager) getPaymentGroupManager();
		final TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(myHandleMethod);
		}
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			try {
				if (StringUtils.isNotEmpty(getRewardNumber()) && StringUtils.isNotBlank(getRewardNumberValid())&& getRewardNumberValid().equalsIgnoreCase(TRUConstants.FALSE)) {
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_REWARD_NUMBER_INVALID, null);
					getErrorHandler().processException(mVe, this);
				}
				if (getFormError()) {
					if (isRestService()) {
						return checkFormRedirect(getCommitOrderSuccessURL(), getCommitOrderErrorURL(), pRequest, pResponse);
					}
					return false;
				}
				transactionDemarcation.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
				// run move to confirmation pipeline chain
				//runProcessMoveToConfirmation(pRequest, pResponse);
				final TRUPayPal payPalPG = ((TRUOrderManager) getOrderManager()).fetchPayPalPaymentGroupForOrder(getOrder());
				final double amount = paymentGroupManager.getRemainingOrderAmount(getOrder());
				if (payPalPG != null) {
					synchronized (getOrder()) {
						// Custom TRU validation
						truValidateForCheckoutChain();
						// End :Custom TRU validation
						boolean isOOSItemsFound=((TRUOrderImpl)getOrder()).isValidationFailed();
						if(isOOSItemsFound){
							if(getOrder().getCommerceItemCount()==TRUConstants.ZERO){
								final String redirectURL = TRUUrlUtil.getConstructedUrl(pRequest,TRUCommerceConstants.SHOPPING_CART_PAGE_NAME,
										getTRUConfiguration().isPortRequired(), null);
								setCommitOrderSuccessURL(redirectURL);
							}else{
								setCommitOrderSuccessURL(errorURL);
								((TRUOrderImpl)getOrder()).setCurrentTab(TRUCheckoutConstants.REVIEW_TAB);
							}	
							
							if (isRestService()) {
								return checkFormRedirect(getCommitOrderSuccessURL(), getCommitOrderErrorURL(), pRequest, pResponse);
							}
							return false;
						}								
						// Validate order before placing same as OOTB
						validateCommitOrder(pRequest, pResponse);
						if (getFormError()) {
							if (isRestService()) {
								return checkFormRedirect(getCommitOrderSuccessURL(), getCommitOrderErrorURL(), pRequest, pResponse);
							}
							return false;
						}
						purchaseProcessHelper(pRequest);
						getOrderManager().removeRemainingOrderAmountFromPaymentGroup(getOrder(), payPalPG.getId());
						payPalPG.setAmount(amount);
						getOrderManager().addRemainingOrderAmountToPaymentGroup(getOrder(), payPalPG.getId());
						((TRUOrderImpl) getOrder()).setRewardNumber(getRewardNumber());
						profileTools.updateProperty(((TRUPropertyManager) profileTools.getPropertyManager()).getRewardNumberPropertyName(),
							getRewardNumber(), getProfile());						
						if (!getFormError()) {
							commitOrder(getOrder(), pRequest, pResponse);
							if(!getFormError()){
								postCommitOrder(pRequest, pResponse);
								getUserSession().setPaypalPaymentDetailsMap(null);
								getUserSession().setTSPCardInfoMap(null);
								getOrderManager().updateOrder(getOrder());
								rollbackFlag = false ;
							}else{
								rollbackFlag = true;
							}
						}else{
							rollbackFlag = true;
						}
					}
				}
			} catch (TransactionDemarcationException | CommerceException | RepositoryException exp ) {
				rollbackFlag = true;
				if (isLoggingError()) {
					logError("Exception occurred @Class::TRUCommitOrderFormHandler::@method::"
							+ "handleExpressCheckoutWithPayPal()", exp);
				}
			} finally {								
				try {
					transactionDemarcation.end(rollbackFlag);
				} catch (TransactionDemarcationException e) {
					if(isLoggingError()){
						if(isLoggingError()){
							vlogError("TransactionDemarcationException In handleExpressCheckoutWithPayPal() for Order {0} While committing the transaction {1}", getOrder().getId(), e);	
						}
					}
				}
				if(!ignoreFinallyBlock){
					handleError(pRequest, pResponse, rollbackFlag, transactionDemarcation);					
				}
			}
		}
		if (rrm != null) {
			rrm.removeRequestEntry(myHandleMethod);
		}
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(myHandleMethod);
		}
			vlogDebug("End of TRUCommitOrderFormHandler : handleExpressCheckoutWithPayPal() method ");
		if (isRestService()) {
			return checkFormRedirect(getCommitOrderSuccessURL(), getCommitOrderErrorURL(), pRequest, pResponse);
		}
		return false;
	}
	
	
	/**
	 * This method place the APPLEPAY order & internally calls doAuth PAYPAL
	 * method.
	 * 
	 * @param pRequest
	 *            - the DynamoHttpServletRequest
	 * @param pResponse
	 *            - the DynamoHttpServletResponse
	 * 
	 * @return form handler
	 * @throws SystemException
	 *             -SystemException
	 * @throws ServletException
	 *             -ServletException
	 * @throws IOException
	 *             -IOException
	 * @throws CommerceException
	 *             - CommerceException
	 */

	public boolean handleCommitOrderWithApplePay (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, CommerceException {
		vlogDebug("Start of TRUCommitOrderFormHandler : handleCommitOrderWithApplePay() method ");
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "TRUCommitOrderFormHandler.handleCommitOrderWithApplePay";
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(myHandleMethod);
		}
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod)))
		{
			Transaction tr = null;
			TRUOrderImpl order = null;
			try {
				tr = ensureTransaction();
				if (getUserLocale() == null) {
					setUserLocale(getUserLocale(pRequest, pResponse));
				}
				if (getFormError()) {
					
					if(isRestService()){
						return checkFormRedirect(null, getCommitOrderErrorURL(), pRequest, pResponse);
					}else{
						return false;
					}
				}
				TRUOrderHolder shoppingCart = (TRUOrderHolder)getShoppingCart();
				if (StringUtils.isNotBlank(getPageName()) && getPageName().equalsIgnoreCase(TRUCommerceConstants.APPLE_PAY_PDP) ) {
					order = shoppingCart.getBuyNowWithAP();
					setOrder(order);
				} else if (StringUtils.isNotBlank(getPageName()) && getPageName().equalsIgnoreCase(TRUCommerceConstants.APPLE_PAY_CART) ) {
					shoppingCart.setBuyNowWithAP(null);
					order = 	(TRUOrderImpl)getOrder();
				}
				if (order == null) {
					String msg = formatUserMessage(TRUCommerceConstants.ORDER_EMPTY, pRequest, pResponse);
					String propertyPath = generatePropertyPath(TRUCommerceConstants.ALLOW_EMPTY_ORDERS);
					addFormException(new DropletFormException(msg, propertyPath, TRUCommerceConstants.ORDER_EMPTY));
					if (getFormError()) {
						if(isRestService()){
							return checkFormRedirect(null, getCommitOrderErrorURL(), pRequest, pResponse);
						}else{
							return false;
						}
					}
				}
				validateApplePayRequest(pRequest, pResponse);
				//validateAppleShipping information
				//set shipping address to shipping group
				//eg. order.getShippinggroup.get(0)-returnSg;and then copy request address to returnSg.getShippingAddress();
				//validateShippingRequest(pRequest, pResponse);
				setshippingAddress(pRequest, pResponse);
				if (getFormError()) {
					
					if(isRestService()){
						return checkFormRedirect(null, getCommitOrderErrorURL(), pRequest, pResponse);
					}else{
						return false;
					}
				}
				ensureAllShipMethods();
				if (getFormError()) {
					
					if(isRestService()){
						return checkFormRedirect(null, getCommitOrderErrorURL(), pRequest, pResponse);
					}else{
						return false;
					}
				}
				truValidateForCheckoutChain();
				if(order.isValidationFailed()){
					if(isRestService() && order.getTotalItemCount() > 0){
						mVe.addValidationError(TRUErrorKeys.INVALID_OUTOFSTOCK_MSG, null);
						getErrorHandler().processException(mVe, this);
					}else if(isRestService() && order.getTotalItemCount() == 0 && !order.getOutOfStockItems().isEmpty()){
						mVe.addValidationError(TRUErrorKeys.INVALID_OUTOFSTOCK_CART_MSG, null);
						getErrorHandler().processException(mVe, this);
					}
					else{
					addFormException(new DropletException(String.valueOf(Boolean.FALSE)));
					}
				}
				if (getFormError()) {
					if(isRestService()){
						return checkFormRedirect(null, getCommitOrderErrorURL(), pRequest, pResponse);
					}else{
						return false;
					}
				}
				preCommitOrderWithApplePay(order, pRequest, pResponse);
				
				synchronized (order) {
					Order lastOrder = getShoppingCart().getLast();
					String orderId = order.getId();
					if (orderId != null && lastOrder != null && orderId.equals(lastOrder.getId())) {
						String msg = formatUserMessage(MSG_ORDER_ALREADY_SUBMITTED, pRequest, pResponse);
						String propertyPath = generatePropertyPath(TRUCommerceConstants.ORDER_ID);
						addFormException(new DropletFormException(msg, propertyPath ,MSG_ORDER_ALREADY_SUBMITTED));
					}
					if (getFormError()) {
						
						if(isRestService()){
							return checkFormRedirect(null, getCommitOrderErrorURL(), pRequest, pResponse);
						}else{
							return false;
						}
					}
					commitOrder (order, pRequest, pResponse);
					if (getFormError()) {
						
						if(isRestService()){
							try {
								((TRUOrderManager) getOrderManager()).updateOrder(getOrder());
							} catch (CommerceException comExcep) {
								if (isLoggingError()) {
									vlogError("CommerceException occured while updating the order: @Class::TRUCommitOrderFormHandler:: @method::postCommitOrder()", comExcep);
								}
							}
							return checkFormRedirect(null, getCommitOrderErrorURL(), pRequest, pResponse);
						}else{
							return false;
						}
					}
					postCommitOrderWithApplePay(pRequest, pResponse);
				}
				vlogDebug("END of TRUCommitOrderFormHandler : handleCommitOrderWithApplePay() method ");
				return checkFormRedirect (getCommitOrderSuccessURL(), getCommitOrderErrorURL(), pRequest, pResponse);
			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.endOperation(myHandleMethod);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		} else {
			
			if (isRestService()) {
				return checkFormRedirect(getCommitOrderSuccessURL(), getCommitOrderErrorURL(), pRequest, pResponse);
			} else {
				return false;
			}
		
			
		}
	}

	/**
	 * Called before any processing is done by the handleCommitOrderWithApplePay
	 * method. The default implementation does nothing. This method is called
	 * from within a synchronized block that synchronizes on the current order
	 * object.
	 * 
	 * @param pRequest
	 *            the request object
	 * @param pResponse
	 *            the response object
	 * @exception ServletException
	 *                if an error occurs
	 * @exception IOException
	 *                if an error occurs
	 */
	public void validateApplePayRequest(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();

		if (getApplePayForm().getBillingAddress() != null) {
			setBillingAddressNickName(getApplePayForm().getBillingAddress().getFirstName() + TRUCommerceConstants.HYPHEN
					+ getApplePayForm().getBillingAddress().getLastName());
		}
		if (getApplePayForm() == null || StringUtils.isBlank(getApplePayForm().getPersonalEmail())
				|| StringUtils.isBlank(getApplePayForm().getPersonalPhone()) || StringUtils.isBlank(getApplePayForm().getApplePayTransactionId())
				|| StringUtils.isBlank(getApplePayForm().getEphemeralPublicKey()) || StringUtils.isBlank(getApplePayForm().getEphemeralPublicKey())
				|| StringUtils.isBlank(getApplePayForm().getPublicKeyHash()) || StringUtils.isBlank(getApplePayForm().getData())
				|| StringUtils.isBlank(getApplePayForm().getSignature())
				|| orderManager.isAnyAddressFieldEmpty(getApplePayForm().getBillingAddress())
				|| StringUtils.isBlank(getApplePayForm().getShippingAddress().getFirstName())
				|| StringUtils.isBlank(getApplePayForm().getShippingAddress().getLastName())
				|| StringUtils.isBlank(getApplePayForm().getShippingAddress().getAddress1())
				|| StringUtils.isBlank(getApplePayForm().getShippingAddress().getCity())
				|| StringUtils.isBlank(getApplePayForm().getShippingAddress().getPostalCode())
				|| StringUtils.isBlank(getApplePayForm().getShippingAddress().getState())
				|| StringUtils.isBlank(getApplePayForm().getShippingAddress().getPhoneNumber())) {
			if (isLoggingDebug()) {
				vlogDebug("TRUCommitOrderFormHandler.validateApplePayRequest::PersonalEmail is blank");
			}
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_APPLEPAY_FORM_MISS_INFO, null);
			getErrorHandler().processException(mVe, this);
		}

	}

/** 
 * This method will validate all the input params for adding new shipping address for applePay commit order API 	  
 * @param pRequest - Request
 * @param pResponse - Response
 * @throws ServletException if an error occurs
 */
	public void validateShippingRequest(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException {
		if (StringUtils.isBlank(getApplePayForm().getShippingAddress().getFirstName()) || StringUtils.isBlank(getApplePayForm().getShippingAddress().getLastName()) ||
				 StringUtils.isBlank(getApplePayForm().getShippingAddress().getAddress1()) || StringUtils.isBlank(getApplePayForm().getShippingAddress().getCity()) ||
				 StringUtils.isBlank(getApplePayForm().getShippingAddress().getPostalCode()) || StringUtils.isBlank(getApplePayForm().getShippingAddress().getState()) ||
				 StringUtils.isBlank(getApplePayForm().getShippingAddress().getPhoneNumber())){
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDRESS_MISSING_INFO, null);
			getErrorHandler().processException(mVe, this);
	}	
}
	
/**
 *  This method to add new shipping address when committing the applePay order
 * @param pRequest - Request
 * @param pResponse - Response
 * @throws ServletException if an error occurs 
 * @throws CommerceException if an error occurs
 */
public void setshippingAddress(DynamoHttpServletRequest pRequest,
		DynamoHttpServletResponse pResponse) throws ServletException, CommerceException {
	final TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService)getShippingGroupMapContainer(); 
	getShippingHelper().findOrAddShippingGroupByNickname(getProfile(), getBillingAddressNickName(), getApplePayForm().getShippingAddress(),
			shippingGroupMapContainer);
	// get shipping group based on nick name
	final TRUHardgoodShippingGroup containerShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupMapContainer().getShippingGroup(getBillingAddressNickName());
	// order first shipping group
	final TRUHardgoodShippingGroup hardgoodShippingGroup = getShippingHelper().getPurchaseProcessHelper().getFirstHardgoodShippingGroup(getOrder());
	if (hardgoodShippingGroup != null) {
		// updating address
		OrderTools.copyAddress(
				containerShippingGroup.getShippingAddress(), hardgoodShippingGroup.getShippingAddress());
		hardgoodShippingGroup.setNickName(getBillingAddressNickName());
		containerShippingGroup.setOrderShippingAddress(true);
		hardgoodShippingGroup.setOrderShippingAddress(true);
	}
}
	 /**
  	 * Called before any processing is done by the handleCommitOrderWithApplePay method.
  	 * The default implementation does nothing.  This method is called from
  	 * within a synchronized block that synchronizes on the current order object.
  	 *
  	 * @param pOrder the order
  	 * @param pRequest the request object
  	 * @param pResponse the response object
  	 * @exception ServletException if an error occurs
  	 * @exception IOException if an error occurs
  	 */
	  public void preCommitOrderWithApplePay(TRUOrderImpl pOrder, DynamoHttpServletRequest pRequest,
	                                   DynamoHttpServletResponse pResponse)
	    throws ServletException, IOException
	    {
		  final TransactionDemarcation transactionDemarcation = new TransactionDemarcation();
		  final TRUOrderManager orderManager = (TRUOrderManager)getOrderManager();
		  boolean doRollBack=false;
		  try {
			  transactionDemarcation.begin(getTransactionManager(), TransactionDemarcation.REQUIRES_NEW);
			  synchronized (pOrder) {
				  String trueClientIpAddress = ((TRUOrderManager)getOrderManager()).getTrueClientIpAddress(pRequest);
					if (StringUtils.isNotBlank(trueClientIpAddress)) {
						pOrder.setCustomerIPAddress(trueClientIpAddress);
					}
					
				  //ensure apple payment group`
				  getShippingGroupManager().removeEmptyShippingGroups(pOrder);
				  orderManager.ensureOnlyApplePayPamentGroup(pOrder);
				  orderManager.applyApplePayPG(pOrder, getApplePayForm());

				  pOrder.setBillingAddress(getApplePayForm().getBillingAddress());
				  pOrder.setSourceOfOrder(getTRUConfiguration().getMobileSourceOrderType());
				  pOrder.setEmail(getApplePayForm().getPersonalEmail());
				  pOrder.setDeviceID(getDeviceID());
				  orderManager.updateOrder(pOrder);
			  }

		  } catch (CommerceException | TransactionDemarcationException exp ) {
			  doRollBack=true;
			  if (isLoggingError()) {logError("CommerceException occured during preCommitOrderWithApplePay @Class::TRUCommitOrderFormHandler::@method::"+ "preCommitOrderWithApplePay() for order Id : " + pOrder.getId(), exp);
			  }
		  }finally {
			  
			  if(getFormError() && !pOrder.isValidationFailed()){
				  doRollBack=true;
			  }
			  if (transactionDemarcation != null) {
				  try {					
					  transactionDemarcation.end(doRollBack);
				  } catch (TransactionDemarcationException demarcationException) {
					  if (isLoggingError()) {
						  logError("TransactionDemarcationException occurs @Class::TRUCommitOrderFormHandler::@method::preCommitOrderWithApplePay() ",demarcationException);
					  }
				  }
			  }
		  }
	    }
	  
	  /**
	   * Called before any processing is done by the handleCommitOrderWithApplePay method.
	   * The default implementation does nothing.  This method is called from
	   * within a synchronized block that synchronizes on the current order object.
	   *
	   * @param pRequest the request object
	   * @param pResponse the response object
	   * @exception ServletException if an error occurs
	   * @exception IOException if an error occurs
	   */
	  public void postCommitOrderWithApplePay(DynamoHttpServletRequest pRequest,
	                                   DynamoHttpServletResponse pResponse)
	    throws ServletException, IOException
	    {
		  super.postCommitOrder(pRequest, pResponse);
		  if (getOrder() != null) {
			  final List<PricingAdjustment> tenderTypeAdjustmentPromotions = getPromotionTools().updateTenderTypeAdjustment(getOrder());
			  // update or create the tenderTypePromotion item
			  if (tenderTypeAdjustmentPromotions != null) {
				  getPromotionTools().createOrUpdateTenderTypePromo(tenderTypeAdjustmentPromotions);
			  }
			  // clearing cart cookie for anonymous user
			  final TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
			  if (profileTools.isAnonymousUser(getProfile())) {
				  ((TRUCookieManager) getCookieManager()).removeCartCookie(pRequest, pResponse);
			  }
			  // Added for epslon order acknowledgment mail.
			  boolean isEpslonEnabled = Boolean.FALSE;
			  isEpslonEnabled = getIntegrationStatusUtil().getEpslonIntStatus(pRequest);
			  if (isEpslonEnabled && getShoppingCart() != null) {
				  sendEpsilonOrderAcknowledgementEmail();

			  } else {
				  vlogDebug("Epslon Is Disabled or Order is Empty");
			  }
		  }
	    }

	/**
	 * This method handles PayPal commit from order review page once the PaypPal
	 * is initiated from cart page. This method will call the DoExpress checkout
	 * service.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest.
	 * @param pResponse
	 *            DynamoHttpServletResponse.
	 * @throws ServletException
	 *             returns if Servlet exception
	 * @throws IOException
	 *             returns if I/O exception
	 * @return true or false
	 * 
	 */
	public boolean handlePayPalCommit(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException,
			ServletException {
			vlogDebug("START : TRUCommitOrderFormHandler.handlePayPalCommit()");
		boolean isValidToken = false;
		
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String myHandleMethod = TRUPayPalConstants.PAYPAL_COMMIT_HANDLER_METHOD_NAME;
		String errorURL = null;
		String successURL = null;
		GetExpressCheckoutResponse expressCheckoutResponse = null;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(myHandleMethod);
		}
		if (!StringUtils.isBlank(getPageName()) && getPageName().equals(TRUPayPalConstants.CART_PAGE_NAME)) {
			errorURL = TRUUrlUtil.getConstructedUrl(pRequest, getSchemeDetectionUtil().getScheme(),getTRUConfiguration().getCartCheckoutWithPayPalErrorURL(),null);
			successURL = TRUUrlUtil.getConstructedUrl(pRequest, getSchemeDetectionUtil().getScheme(),getTRUConfiguration().getCartCheckoutWithPayPalSuccessURL(),null);
		} else if (!StringUtils.isBlank(getPageName()) && getPageName().equals(TRUPayPalConstants.REVIEW_PAGE_NAME)) {
			errorURL = TRUUrlUtil.getConstructedUrl(pRequest, getSchemeDetectionUtil().getScheme(),getTRUConfiguration().getPayPalReviewErrorUrl(), null);
			successURL = TRUUrlUtil.getConstructedUrl(pRequest, getSchemeDetectionUtil().getScheme() , getTRUConfiguration().getPayPalReviewSuccessUrl(),  null);
		} else {
			errorURL = TRUUrlUtil.getConstructedUrl(pRequest,getSchemeDetectionUtil().getScheme(), getTRUConfiguration().getCheckoutWithPayPalErrorURL(), null);
			successURL = TRUUrlUtil.getConstructedUrl(pRequest, getSchemeDetectionUtil().getScheme() , getTRUConfiguration().getCheckoutWithPayPalSuccessURL(), null);
		}		
		if (isLoggingDebug()) {
			logDebug("PayPal Return Success URL :: "+successURL+ " \nPayPal Return Failure URL "+errorURL);
		}
		if (!isRestService()) {
			setCommitOrderErrorURL(errorURL);
			setCommitOrderSuccessURL(successURL);
		}
		String payPalErrMessage = null;
		final String payPalErrKey = TRUErrorKeys.TRU_PAYPAL_GENERIC_ERR_KEY;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			boolean rollBack = false;
			Transaction tr = null;
			boolean ignoreRollBack = false;
			try {
				tr = ensureTransaction();
				if (tr == null) {
					tr = getCurrentTransaction();
				}
				if (!StringUtils.isBlank(payPalErrKey)) {
					payPalErrMessage = getErrorHandlerManager().getErrorMessage(payPalErrKey);
				}				
				synchronized (getOrder()) {
					// Validate order before placing same as OOTB
					validateCommitOrder(pRequest, pResponse);
					if (getFormError()) {
						return checkFormRedirect(getCommitOrderSuccessURL(), getCommitOrderErrorURL(), pRequest, pResponse);
					}
					if (!StringUtils.isBlank(getPageName()) && getPageName().equals(TRUPayPalConstants.CART_PAGE_NAME)) {
						// calling get express checkout of PAYPAL
						expressCheckoutResponse = getPayPalUtilityManager().payPalGetExpressCheckout(getOrder(), getProfile(),
								getPageName(), getShippingGroupMapContainer(), getPaymentGroupMapContainer());
						if (expressCheckoutResponse != null && expressCheckoutResponse.isSuccess()
								&& !StringUtils.isBlank(expressCheckoutResponse.getPayerId())) {
							if (!getFormError()) {
								setPayerId(expressCheckoutResponse.getPayerId());
								isValidToken = true;
							}							
							
						}
					} else {
						expressCheckoutResponse = getPayPalUtilityManager().payPalGetExpressCheckout(getOrder(), getProfile(),
								getPageName(), getShippingGroupMapContainer(), getPaymentGroupMapContainer());
						if (expressCheckoutResponse != null && expressCheckoutResponse.isSuccess()
								&& !StringUtils.isBlank(expressCheckoutResponse.getPayerId())) {
							setPayerId(expressCheckoutResponse.getPayerId());
							isValidToken = true;
						}
					}	
					
					
					if (!getFormError()) {
						getShippingGroupManager().removeEmptyShippingGroups(getOrder());
						final boolean shipAdressInValid = runProcessValidateShippingGroups(pRequest, pResponse);												
						if (shipAdressInValid && !isRestService()) {
							//if (!isRestService()) {
							ignoreRollBack = true;
							((TRUOrderImpl)getOrder()).setCurrentTab(TRUCheckoutConstants.SHIPPING_TAB);
							setCommitOrderErrorURL(successURL);
							//}
						}
					}
					
					// capturing PAYPAL details in user session component
					if (isValidToken && !StringUtils.isBlank(getPayerId()) && getUserSession() != null) {
						Map<String, String> paypalPaymentDetailsMap = getUserSession().getPaypalPaymentDetailsMap();
						if (paypalPaymentDetailsMap == null) {
							paypalPaymentDetailsMap = new HashMap<String, String>();
						}
						paypalPaymentDetailsMap.put(TRUPayPalConstants.PAYER_ID, getPayerId());
						getUserSession().setPaypalPaymentDetailsMap(paypalPaymentDetailsMap);
					} else {
						getPayPalUtilityManager().addPayPalErrorMessage(expressCheckoutResponse, this);
						return checkFormRedirect(null, getCommitOrderErrorURL(), pRequest, pResponse);	
					}	
					
					// Start MVP8:Custom TRU validation
					truValidateForCheckoutChain();
					// End :Custom TRU validation
					boolean isOOSItemsFound=((TRUOrderImpl)getOrder()).isValidationFailed();
					if(!getFormError() && isOOSItemsFound){
						if(((TRUOrderImpl)getOrder()).getCommerceItemCount()==TRUConstants.ZERO){
							final String redirectURL = TRUUrlUtil.getConstructedUrl(pRequest,TRUCommerceConstants.SHOPPING_CART_PAGE_NAME,
									getTRUConfiguration().isPortRequired(), null);
							setCommitOrderSuccessURL(redirectURL);
						}else{
							setCommitOrderSuccessURL(getCommitOrderErrorURL());
							((TRUOrderImpl)getOrder()).setCurrentTab(TRUCheckoutConstants.REVIEW_TAB);
						}		
						return checkFormRedirect(getCommitOrderSuccessURL(), getCommitOrderErrorURL(), pRequest, pResponse);
					}
					((TRUOrderImpl)getOrder()).setDeviceID(getDeviceID());
					// End MVP8 :Custom TRU validation
					getOrderManager().updateOrder(getOrder());
				}
			} catch (TRUPayPalException | SystemException | InstantiationException | IllegalAccessException | ClassNotFoundException | CommerceException | IntrospectionException | RunProcessException exp) {
				addFormException(new DropletException(payPalErrMessage));
				rollBack = true;
				if (isLoggingError()) {
					logError("TRUPayPalException Exception occured during payPal payment @Class::TRUCommitOrderFormHandler::@method"
							+ "::handlePayPalCommit() for order Id : " + getOrder().getId(), exp);
				}
			}  finally {
				if (tr != null) {
					if ((rollBack || getFormError())) {
						try {
							if(!ignoreRollBack){
								setTransactionToRollbackOnly();
							}
						} catch (javax.transaction.SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in @Class::TRUCommitOrderFormHandler::@method::handlePayPalCommit() ", e);
							}
						}
					}
					commitTransaction(tr);
				}
			}
			// calling payPal final commit
			if (!rollBack && !getFormError()) {
				rollBack = payPalCommit(isValidToken, pRequest, pResponse);
			}
			if (rollBack || getFormError()) {
			//	removePaypalPGFromOrder();
				if (isRestService()) {
					return checkFormRedirect(getCommitOrderSuccessURL(), getCommitOrderErrorURL(), pRequest, pResponse);
				}
				return checkFormRedirect(null, getCommitOrderErrorURL(), pRequest, pResponse);
			}
		}
		if (rrm != null) {
			rrm.removeRequestEntry(myHandleMethod);
		}
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(myHandleMethod);
		}
			vlogDebug("END : TRUCommitOrderFormHandler.handlePayPalCommit()");
		return checkFormRedirect(getCommitOrderSuccessURL(), getCommitOrderErrorURL(), pRequest, pResponse);
	}

	/**
	 * This method does a SetExpressCheckout API call to fetch token.
	 * 
	 * @param pRequest
	 *            - the pRequest
	 * @param pResponse
	 *            - the pResponse
	 * @return boolean -true or false
	 * @throws SystemException
	 *             -system exception
	 * @throws ServletException
	 *             -servlet exception
	 * @throws IOException
	 *             -io exception
	 */

	public boolean handlePayPalSetExpress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, SystemException {
			vlogDebug("Start of TRUCommitOrderFormHandler : handleCheckoutWithPayPalFromOrderReview() method ");
		boolean rollbackFlag = false;
		String payPalToken = null;
		SetExpressCheckoutResponse setExpressCheckoutResponse = null;
		String timeStamp = null;
		String errorURL = null;
		String successURL = null;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String myHandleMethod = TRUPayPalConstants.PAYPAL_CHECKOUT_ORDERREVIEW_HANDLER_METHOD_NAME;
		final TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
		final TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		errorURL = TRUUrlUtil.getConstructedUrl(pRequest,getSchemeDetectionUtil().getScheme(), getTRUConfiguration().getPayPalChkoutFromOrderReviewCancelUrl(),null);
		successURL = TRUUrlUtil.getConstructedUrl(pRequest, getSchemeDetectionUtil().getScheme(),getTRUConfiguration().getPayPalChkoutFromOrderReviewSuccessUrl(),null);
		//successURL = successURL.replace(TRUCommerceConstants.HTTP_SCHEME, TRUCommerceConstants.HTTPS_SCHEME);
		//errorURL   = errorURL.replace(TRUCommerceConstants.HTTP_SCHEME, TRUCommerceConstants.HTTPS_SCHEME);
		if (isLoggingDebug()) {
			logDebug("PayPal Return Success URL :: "+successURL+ " PayPal Return Failure URL "+errorURL);
		}
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(myHandleMethod);
		}
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					if (StringUtils.isNotEmpty(getRewardNumber()) && StringUtils.isNotBlank(getRewardNumberValid())&& getRewardNumberValid().equalsIgnoreCase(TRUConstants.FALSE)) {
						mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_REWARD_NUMBER_INVALID, null);
						getErrorHandler().processException(mVe, this);
						if (getFormError() && isRestService()) {
							return checkFormRedirect(getCommitOrderSuccessURL(), getCommitOrderErrorURL(), pRequest, pResponse);
						} else if(getFormError()) {
							return false;
						}
					}
					((TRUOrderImpl) getOrder()).setRewardNumber(getRewardNumber());
					profileTools.updateProperty(((TRUPropertyManager) profileTools.getPropertyManager()).getRewardNumberPropertyName(),
						getRewardNumber(), getProfile());
					((TRUOrderImpl) getOrder()).setStanderdPayPal(Boolean.TRUE);
					((TRUOrderImpl) getOrder()).setCurrentTab(TRUCheckoutConstants.REVIEW_TAB);
					setExpressCheckoutResponse = ((TRUPaypalUtilityManager) getPayPalUtilityManager()).payPalSetExpressCheckout(
							getPaymentGroupMapContainer(), pRequest.getLocale().toString(), getOrder(), errorURL, successURL, getProfile(),
							true);
					if(isLoggingDebug()){
						vlogDebug("SetExpressCheckoutResponse : " + setExpressCheckoutResponse);
					}						
					if (setExpressCheckoutResponse != null && setExpressCheckoutResponse.isSuccess()
							&& !StringUtils.isBlank(setExpressCheckoutResponse.getToken())) {
						payPalToken = setExpressCheckoutResponse.getToken();
						timeStamp = setExpressCheckoutResponse.getTimeStamp();
						if (isLoggingDebug()) {
							vlogDebug("payPalToken : {0} , payPalTokenTimeStamp : {1}" + payPalToken,timeStamp);
						}
						if (!StringUtils.isBlank(payPalToken) && getUserSession() != null) {
							Map<String, String> paypalPaymentDetailsMap = getUserSession().getPaypalPaymentDetailsMap();
							if (paypalPaymentDetailsMap == null) {
								paypalPaymentDetailsMap = new HashMap<String, String>();
							}
							paypalPaymentDetailsMap.put(TRUPayPalConstants.PAYPAL_TOKEN, payPalToken);
							paypalPaymentDetailsMap.put(TRUPayPalConstants.PAYPAL_TOKEN_TIMESTAMP, timeStamp);
							getUserSession().setPaypalPaymentDetailsMap(paypalPaymentDetailsMap);
							TRUPayPal payPalPG = orderManager.fetchPayPalPaymentGroupForOrder(getOrder());
							payPalPG.setToken(payPalToken);
							orderManager.updateOrder(getOrder());
						}
					} else {
						rollbackFlag = true;
						getPayPalUtilityManager().addPayPalErrorMessage(setExpressCheckoutResponse, this);
					}

				}
			} catch (TRUPayPalException | CommerceException | RepositoryException |SystemException exp) {
				rollbackFlag = true;
				String payPalErrMessage = getErrorHandlerManager().getErrorMessage(TRUErrorKeys.TRU_PAYPAL_GENERIC_ERR_KEY);
				addFormException(new DropletException(payPalErrMessage));
				if (isLoggingError()) {
					logError("TRUPayPalException occurs @Class::TRUCommitOrderFormHandler::@method::handlePayPalSetExpress() ",
							exp);
				}
			}finally {
				if (getFormError() && rollbackFlag) {
					removePaypalPGFromOrder();
				}		
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.endOperation(myHandleMethod);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUCommitOrderFormHandler : handleCheckoutWithPayPalFromOrderReview() method ");
		}
		if (isRestService()) {
			return checkFormRedirect(getCommitOrderSuccessURL(), getCommitOrderErrorURL(), pRequest, pResponse);
		}
		return checkFormRedirect(getCommitOrderSuccessURL(), getCommitOrderErrorURL(), pRequest, pResponse);
	}

	/**
	 * Checks for credit card expiration in order. Adds form exception if credit
	 * card expired.
	 * 
	 * @throws ServletException
	 *             returns if Servlet exception
	 * @throws IOException
	 *             returns if I/O exception
	 * @return true - if credit card is expired.
	 */
	public boolean isOrderCreditCardExpired() throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START :: TRUCommitOrderFormHandler.isOrderCreditCardExpired method");
		}
		final TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
		CreditCard creditCard = orderManager.getCreditCard(getOrder());
		if (creditCard != null && StringUtils.isNotBlank(creditCard.getCreditCardNumber())) {
			TRUProfileManager profileManager = getBillingHelper().getProfileManager();
			boolean isCardExpired = profileManager.validateCardExpiration(creditCard.getExpirationMonth(), creditCard.getExpirationYear());
			if (isCardExpired) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ORDER_CREDIT_CARD_EXPIRED, null);
				getErrorHandler().processException(mVe, this);
				if (isLoggingDebug()) {
					vlogDebug("END :: TRUCommitOrderFormHandler.isOrderCreditCardExpired method");
				}
				return Boolean.TRUE;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END :: TRUCommitOrderFormHandler.isOrderCreditCardExpired method");
		}
		return Boolean.FALSE;
	}

	/**
	 * @return mOrderSetWithDefault order set with default property
	 */
	public boolean isOrderSetWithDefault() {
		return mOrderSetWithDefault;
	}

	/**
	 * Checks if is rest service.
	 * 
	 * @return the mRestService
	 */
	public boolean isRestService() {
		return mRestService;
	}

	/**
	 * @return the mRUSCard
	 */
	public boolean isRUSCard() {
		return mRUSCard;
	}

	/**
	 * This method handles PayPal commit from order review page once the PaypPal
	 * is initiated from cart page. This method will call the DoExpress checkout
	 * service.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest.
	 * @param pValidToken
	 *            boolean
	 * @param pResponse
	 *            DynamoHttpServletResponse.
	 * @throws ServletException
	 *             returns if Servlet exception
	 * @throws IOException
	 *             returns if I/O exception
	 * @return true or false
	 * 
	 */
	public boolean payPalCommit(boolean pValidToken, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		vlogDebug("START :TRUCommitOrderFormHandler.payPalCommit()");
		boolean rollBack = false;
		String payPalErrMessage = null;
		String payPalErrKey = TRUErrorKeys.TRU_PAYPAL_GENERIC_ERR_KEY;
		//Transaction tr = null;
		final TransactionDemarcation transactionDemarcation = new TransactionDemarcation();
		try {
			transactionDemarcation.begin(getTransactionManager(), TransactionDemarcation.REQUIRES_NEW);
			synchronized (getOrder()) {
				if (isLoggingDebug()) {
					vlogDebug("payPal token validity" + pValidToken);
				}
				payPalErrMessage = getErrorHandlerManager().getErrorMessage(payPalErrKey);
				purchaseProcessHelper(pRequest);
				if (pValidToken && !getFormError()) {
					// calling move to confirm pipeline chain
					runProcessMoveToConfirmation(pRequest, pResponse);
				} else {
					rollBack = true;
					addFormException(new DropletException(TRUPaypalConstants.PAYPAL_TOKEN_NOT_VALID));
					if (isLoggingError()) {
						logError("Paypal token is not valid @Class::TRUCommitOrderFormHandler::@method::payPalCommit() ");
					}
				}
				if (!getFormError() && (!StringUtils.isBlank(getPageName()) && (!getPageName().equals(TRUPayPalConstants.CART_PAGE_NAME)))) {
					// Custom TRU validation
					commitOrder(getOrder(), pRequest, pResponse);
					if (!getFormError()) {
						postCommitOrder(pRequest, pResponse);
						if (getShippingGroupMapContainer() != null) {
							((TRUShippingGroupContainerService) getShippingGroupMapContainer()).setShippingAddress(null);
						}
						if (getPaymentGroupMapContainer() != null) {
							((TRUPaymentGroupContainerService) getPaymentGroupMapContainer()).setBillingAddress(null);
						}
						getUserSession().setPaypalPaymentDetailsMap(null);
						getUserSession().setTSPCardInfoMap(null);
					}
				}
				if (getFormError()
						&& ((getPageName().equals(TRUPayPalConstants.PAYMENT_PAGE_NAME)) || (getPageName()
								.equals(TRUPayPalConstants.REVIEW_PAGE_NAME)))) {
					// add PAYPAL related error messages\
					Vector exceptions = getFormExceptions();
					Enumeration vEnum = exceptions.elements();
					Object error = null;
					boolean isPayPalError = false;
					while(vEnum.hasMoreElements()){
						error =  (Object) vEnum.nextElement();
						if(error instanceof DropletException && ((DropletException)error).getErrorCode() != null && ((DropletException)error).getErrorCode().contains(PAYPAL)){
							isPayPalError = true;
							break;
						}
					}
					if(isPayPalError){
						resetFormExceptions();
						//addPayPalErrorMessage1();
						payPalErrKey = getPayPalErrorKey();
						// get the message corresponding to key
						payPalErrMessage = getErrorHandlerManager().getErrorMessage(payPalErrKey);
						addFormException(new DropletException(payPalErrMessage));
					}
					rollBack = true;
				}
				if ((getPageName().equals(TRUPayPalConstants.CART_PAGE_NAME))) {
					getOrderManager().updateOrder(getOrder());
				}
			}
		} catch (TransactionDemarcationException | RunProcessException | CommerceException | SystemException exp) {
			addFormException(new DropletException(payPalErrMessage));
			rollBack = true;
			if (isLoggingError()) {
				logError("TransactionDemarcationException occured during payPal payment @Class::TRUCommitOrderFormHandler::@method::"
						+ "payPalCommit() for order Id : " + getOrder().getId(), exp);
			}
		} finally {
			if (transactionDemarcation != null) {
				try {
					transactionDemarcation.end(rollBack);
				} catch (TransactionDemarcationException e) {
					if(isLoggingError()){
						if(isLoggingError()){
							vlogError("TransactionDemarcationException In payPalCommit() for Order {0} While committing the transaction {1}", getOrder().getId(), e);	
						}
					}
				}				
			}
			handleError(pRequest, pResponse, rollBack, transactionDemarcation);
		}
		vlogDebug("END : TRUCommitOrderFormHandler.payPalCommit()");
		return rollBack;
	}

	/**<p>
	 * This method will set the order's meta info.
	 * </p>
	 * @param pRequest - pRequest
	 */
	private void purchaseProcessHelper(DynamoHttpServletRequest pRequest) {
		String enteredBy = null;
		String enteredLocation = null;
		TRUPurchaseProcessHelper purchaseProcessHelper = (TRUPurchaseProcessHelper) getPurchaseProcessHelper();
		TRUOrderImpl order = (TRUOrderImpl) getOrder();
		if (isRestService()) {
			order.setSourceOfOrder(getTRUConfiguration().getMobileSourceOrderType());
		} else if (purchaseProcessHelper.isSOSOrder(pRequest)) {
			order.setSourceOfOrder(getTRUConfiguration().getSosSourceOrderType());
			enteredBy = purchaseProcessHelper.getEnteredByValue(pRequest);
			enteredLocation = purchaseProcessHelper.getEnteredLocation(pRequest);
			if(StringUtils.isNotBlank(enteredBy)){
				order.setEnteredBy(enteredBy);
			}
			if(StringUtils.isNotBlank(enteredLocation)){
				order.setEnteredLocation(enteredLocation);
			}
		} else {
			order.setSourceOfOrder(getTRUConfiguration().getWebSourceOrderType());
		}
	}

	/**
	 * This method overridden for tender type and budget limit changes.
	 * 
	 * @param pRequest
	 *            - References to DynamoHttpServletRequest
	 * @param pResponse
	 *            - References to DynamoHttpServletResponse
	 * @throws ServletException
	 *             - throws IOException
	 * @throws IOException
	 *             - throws ServletException
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void postCommitOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
	IOException {
		vlogDebug("Entering  TRUCommitOrderFormHandler.postCommitOrder() method");
		long starTime = Calendar.getInstance().getTimeInMillis();
		final String myHandleMethod = TRUCheckoutConstants.POST_COMMIT_ORDER;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(myHandleMethod);
		}
		if (getFormError()) {
			for (ValidationError paymentGroupKey : mVe.getValidationErrors()) {
				if(TRUErrorKeys.TRU_ERROR_ADDRESS_DOCTOR_PLACE_ORDER.equalsIgnoreCase(paymentGroupKey.getErrorId())){
					mVe.getValidationErrors().remove(paymentGroupKey);
					if(mVe.getValidationErrors().isEmpty()){
						resetFormExceptions();
					}
				}
			}
			if(isNewCreditCard()) {
				String selectedCCNickName = getPaymentGroupMapContainer().getDefaultPaymentGroupName();
				if(StringUtils.isBlank(selectedCCNickName)) {
					selectedCCNickName = (String) getProfile().getPropertyValue(getPropertyManager().getSelectedCCNickNamePropertyName());
				}
				getPaymentGroupMapContainer().removePaymentGroup(selectedCCNickName);
			}
			try {
				((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL,
						getOrder(), getUserPricingModels(), getUserLocale(), getProfile(), createRepriceParameterMaps(), this);
				((TRUOrderManager) getOrderManager()).updateOrder(getOrder());
			} catch (CommerceException comExcep) {
				if (isLoggingError()) {
					logError("CommerceException occured while updating the order: @Class::TRUCommitOrderFormHandler:: @method::postCommitOrder()", comExcep);
				}
			} catch (RunProcessException e) {
				if (isLoggingError()) {
					logError("RunProcessException occured while updating the order: @Class::TRUCommitOrderFormHandler:: @method::postCommitOrder()", e);
				}
			}
			if (PerformanceMonitor.isEnabled()) {
				PerformanceMonitor.endOperation(myHandleMethod);
			}
			return;
		}
		getUserSession().setTSPCardInfoMap(null);
		getUserSession().setTSPBillingAddressMap(null);
		getUserSession().setInputEmail(null);
		getUserSession().setTSPEditBillingAddress(false);
		getUserSession().setBillingAddressSameAsShippingAddress(false);
		getUserSession().setPaypalPaymentDetailsMap(null);
		super.postCommitOrder(pRequest, pResponse);
		if (getOrder() == null) {
			if (isLoggingError()) {
				vlogError("getOrder() is null");
			}
			if (PerformanceMonitor.isEnabled()) {
				PerformanceMonitor.endOperation(myHandleMethod);
			}
			return;
		}
		final TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		((TRUCookieManager) getCookieManager()).removeCartCookie(pRequest, pResponse);
		Boolean isPayPalPaymentGroup = Boolean.FALSE;
		try {
			vlogDebug("Current Order :{0} ",getShoppingCart().getCurrent());
			vlogDebug("Last Order :{0} ",getShoppingCart().getLast());
			//Start : code Added for defect-TSJ-9218
			final List<PaymentGroup> listOfAllPaymentGroup = (List<PaymentGroup>) getOrder().getPaymentGroups();
			for (PaymentGroup pg : listOfAllPaymentGroup) {
				if (pg instanceof TRUPayPal) {
					isPayPalPaymentGroup = Boolean.TRUE;
				}
			}
			//End : code Added for defect-TSJ-9218
			synchronized ((TRUOrderImpl) getShoppingCart().getCurrent()) {
				if (profileTools.isAnonymousUser(getProfile())) {
					((TRUOrderImpl) getShoppingCart().getCurrent()).setEmail(((TRUOrderImpl) getShoppingCart().getLast()).getEmail());
				}
				if(getShoppingCart().getLast() != null && getShoppingCart().getCurrent() != null){
					if(((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).isSOSOrder(pRequest)){
						((TRUOrderImpl) getShoppingCart().getCurrent()).setEmail(TRUCheckoutConstants.EMPTY_STRING);
						((TRUOrderImpl) getShoppingCart().getCurrent()).setBillingAddress(null);
						((TRUOrderImpl) getShoppingCart().getCurrent()).setBillingAddressNickName(null);
						getUserSession().setBillingAddressSameAsShippingAddress(false);
					}else{
						if(!isPayPalPaymentGroup){
						((TRUOrderImpl) getShoppingCart().getCurrent()).setBillingAddress(((TRUOrderImpl) getShoppingCart().getLast()).getBillingAddress());
						((TRUOrderImpl) getShoppingCart().getCurrent()).setBillingAddressNickName(((TRUOrderImpl) getShoppingCart()
								.getLast()).getBillingAddressNickName());
						}
					}
				}
				getPurchaseProcessHelper().getOrderManager().updateOrder(((TRUOrderImpl) getShoppingCart().getCurrent()));
			}
		} catch (CommerceException coe) {
			if (isLoggingError()) {
				vlogError("CommerceException occured while updating the order: @Class::TRUCommitOrderFormHandler:: @method::postCommitOrder()", coe);
			}
		}
		try {
			final List<PricingAdjustment> tenderTypeAdjustmentPromotions = getPromotionTools().updateTenderTypeAdjustment(getOrder());
			// update or create the tenderTypePromotion item
			if (tenderTypeAdjustmentPromotions != null) {
				getPromotionTools().createOrUpdateTenderTypePromo(tenderTypeAdjustmentPromotions);
			}
			if ( getShoppingCart().getLast() !=null && (((TRUOrderImpl) getShoppingCart().getLast()).getRewardNumber() != null)) {
				final String rewardNumber = ((TRUOrderImpl) getShoppingCart().getLast()).getRewardNumber();
				if(((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).isSOSOrder(pRequest)){
					profileTools.updateProperty(((TRUPropertyManager) profileTools.getPropertyManager()).getRewardNumberPropertyName(),
							TRUCheckoutConstants.EMPTY_STRING, getProfile());
				}else{
					profileTools.updateProperty(((TRUPropertyManager) profileTools.getPropertyManager()).getRewardNumberPropertyName(),
							rewardNumber, getProfile());
				}
			}

			//SETA logging START
			if(!getProfile().isTransient()) {
				String login = (String)getProfile().getPropertyValue(getPropertyManager().getLoginPropertyName());
				if(isNewBillingAddress()) {
					getSetaLogger().logAddBillingAddress(login, 
							((TRUOrderImpl) getShoppingCart().getLast()).getBillingAddressNickName() );
				} else if (isEditBillingAddress()) {
					getSetaLogger().logUpdateBillingAddress(login, 
							((TRUOrderImpl) getShoppingCart().getLast()).getBillingAddressNickName());
				}
				if(isNewCreditCard()) {
					getSetaLogger().logAddCreditCard(login);
				} else if(isEditCreditCard()) {
					getSetaLogger().logUpdateCreditCard(login);
				}
			}
			//SETA logging END

			CreditCard srcCreditCard = null;
			TRUCreditCard destCreditCard = (TRUCreditCard) ((TRUOrderManager) getOrderManager()).getCreditCard((TRUOrderImpl) getShoppingCart().getLast());
			String responseCode = null;
			if(destCreditCard != null){
				responseCode = destCreditCard.getResponseCode();
			}
			if (destCreditCard != null && !isRUSCard() && responseCode != null &&
					!getRadialPaymentProcessor().getPaymentTools().getSoftAllocationNotAllowedList().
					contains(responseCode.toLowerCase())) {
				if (getProfile().isTransient()) {
					if (getPaymentGroupMapContainer().getPaymentGroup(getCreditCardNickName()) != null) {
						srcCreditCard = (TRUCreditCard) getPaymentGroupMapContainer().getPaymentGroup(getCreditCardNickName());
						getOrderManager().getOrderTools().getProfileTools().copyShallowCreditCardProperties(srcCreditCard,destCreditCard);
					}
				} else {
					String selectedCCNickName = getPaymentGroupMapContainer().getDefaultPaymentGroupName();
					if(StringUtils.isBlank(selectedCCNickName)) {
						selectedCCNickName = (String) getProfile().getPropertyValue(getPropertyManager().getSelectedCCNickNamePropertyName());
					}
					RepositoryItem srcRepCreditCard = getOrderManager().getOrderTools().getProfileTools().getCreditCardByNickname(selectedCCNickName,
							getProfile());
					RepositoryItem billingAddress = getProfileTools().getSecondaryAddressByNickName(getProfile(), 
							((TRUOrderImpl) getShoppingCart().getLast()).getBillingAddressNickName());
					if(billingAddress == null) {
						((TRUProfileTools) getProfileTools()).createProfileRepositorySecondaryAddress(getProfile(), ((TRUOrderImpl) getShoppingCart()
								.getLast()).getBillingAddressNickName(), ((TRUOrderImpl) getShoppingCart().getLast()).getBillingAddress());
						billingAddress = getProfileTools().getSecondaryAddressByNickName(getProfile(), 
								((TRUOrderImpl) getShoppingCart().getLast()).getBillingAddressNickName());
						((MutableRepositoryItem)billingAddress).setPropertyValue(getPropertyManager().getIsBillingAddressPropertyName(), true); 
					}
					if(srcRepCreditCard != null){
						getOrderManager().getOrderTools().getProfileTools().copyShallowCreditCardProperties(destCreditCard,srcRepCreditCard);
						getProfileTools().setCreditCardBillingAddress(selectedCCNickName, billingAddress, (Profile)getProfile());
					} else {
						final MutableRepositoryItem newCreditCard = getProfileTools().createCreditCardItem(getProfile(), destCreditCard, billingAddress);
						getProfileTools().addCreditCardToUsersMap(getProfile(), newCreditCard, selectedCCNickName);
					}
				}
			}
			if (destCreditCard != null && !isRUSCard() && responseCode != null &&
					getRadialPaymentProcessor().getPaymentTools().getSoftAllocationNotAllowedList().
					contains(responseCode.toLowerCase())) {
				String selectedCCNickName = getPaymentGroupMapContainer().getDefaultPaymentGroupName();
				if(StringUtils.isBlank(selectedCCNickName)) {
					selectedCCNickName = (String) getProfile().getPropertyValue(getPropertyManager().getSelectedCCNickNamePropertyName());
				}
				getPaymentGroupMapContainer().removePaymentGroup(selectedCCNickName);
			}
			updateRegistryItem();
			//Duplicate code can be removed from Paypal & CC
			//Start : code commented  for defect-TSJ-9218
			/*if(!profileTools.isAnonymousUser(getProfile()) && getTRUConfiguration().isSaveBillingAddress()){
				RepositoryItem billingAddress = getProfileTools().getSecondaryAddressByNickName(getProfile(), 
						((TRUOrderImpl) getShoppingCart().getLast()).getBillingAddressNickName());
				if(billingAddress == null) {
					String nickName = ((TRUOrderImpl) getShoppingCart().getLast()).getBillingAddressNickName();
					Address billingAddress1 = ((TRUOrderImpl) getShoppingCart().getLast()).getBillingAddress();
					((TRUProfileTools) getProfileTools()).createProfileRepositorySecondaryAddress(getProfile(), nickName, billingAddress1);
					billingAddress = getProfileTools().getSecondaryAddressByNickName(getProfile(), nickName);
					((MutableRepositoryItem)billingAddress).setPropertyValue(getPropertyManager().getIsBillingAddressPropertyName(), Boolean.TRUE);
					((MutableRepositoryItem)getProfile()).setPropertyValue(getPropertyManager().getBillingAddressPropertyName(), billingAddress);
				}
			}			*/
			//End : code commented  for defect-TSJ-9218
			/** Fix for TUW-49684--START **/
			if (!profileTools.isAnonymousUser(getProfile())) {
				List shippingGroups = getOrder().getShippingGroups();
				Address shippingAddress = null;
				final List<PaymentGroup> listOfPaymentGroup = (List<PaymentGroup>) getOrder().getPaymentGroups();
				for (PaymentGroup pg : listOfPaymentGroup) {
					if (pg instanceof TRUPayPal) {
						int shippingGroupSize = shippingGroups.size();
						for (int i = 0; i < shippingGroupSize; i++) {
							ShippingGroup shippingGroup = (ShippingGroup) shippingGroups.get(i);
							if (shippingGroup instanceof TRUHardgoodShippingGroup) {
								shippingAddress = (ContactInfo) ((TRUHardgoodShippingGroup) shippingGroup).getShippingAddress();
								boolean checkDuplicateNickName = profileTools.isDuplicateAddressNickName(getProfile(),
										((TRUHardgoodShippingGroup) shippingGroup).getNickName());
								if (!checkDuplicateNickName) {
									((TRUProfileTools) profileTools).createProfileRepositorySecondaryAddress(getProfile(),
											((TRUHardgoodShippingGroup) shippingGroup).getNickName(),shippingAddress);
								}
							}
						}
					}
				}
			}
			// Updating PapPay PG Details in container after placing the order
			TRUPaymentGroupContainerService paymentGroupContainerService = (TRUPaymentGroupContainerService) getPaymentGroupMapContainer();
			if (paymentGroupContainerService.getPaymentGroupMap() != null && null != paymentGroupContainerService.getPaymentGroupMap().get(
					TRUPropertyNameConstants.PAYPAL_PAYMENT)) {
				paymentGroupContainerService.getPaymentGroupMap().remove(TRUPropertyNameConstants.PAYPAL_PAYMENT);
				if (paymentGroupContainerService != null) {
					paymentGroupContainerService.setBillingAddress(null);
				}
				if (getShippingGroupMapContainer() != null) {
					((TRUShippingGroupContainerService) getShippingGroupMapContainer()).removeShippingGroup(((TRUOrderImpl) getShoppingCart()
							.getLast()).getBillingAddressNickName());
				}
			}
			if(isPayPalPaymentGroup && getShippingGroupMapContainer() != null){
				((TRUShippingGroupContainerService) getShippingGroupMapContainer()).removeShippingGroup(((TRUOrderImpl) getShoppingCart()
						.getLast()).getBillingAddressNickName());
			}
			// update the single use coupon uses
			List<RepositoryItem> singleUseCoupons = ((TRUClaimableTools) getClaimableManager().getClaimableTools()).getSingleUsedCoupon(getOrder());
			if(singleUseCoupons != null && !singleUseCoupons.isEmpty()){
				getPromotionTools().revokeSingleUseCouponsfromUser(singleUseCoupons, getProfile());
			}
			// START: Added for epslon order acknowledgment mail.
			boolean isEpslonEnabled = Boolean.FALSE;
			isEpslonEnabled = getIntegrationStatusUtil().getEpslonIntStatus(pRequest);
			if (isEpslonEnabled && getShoppingCart() != null) {
				sendEpsilonOrderAcknowledgementEmail();
			} else {
				vlogDebug("Epslon Is Disabled or Order is Empty ");
			}
			// END: Added for epslon order acknowledgment mail.
			getUserPricingModels().initializePricingModels();
		} catch (CommerceException | RepositoryException exp) {
			vlogError("CommerceException occured while updateRegistryItem to service: @Class::TRUCommitOrderFormHandler::"+ "@method::postCommitOrder()", exp);
		} catch (PropertyNotFoundException propNotFoundExp) {
			if (isLoggingError()) {
				logError("Exception in @Class::TRUCommitOrderFormHandler::@method::postCommitOrder()", propNotFoundExp);
			}
		}
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(myHandleMethod);
		}
		long endTime = Calendar.getInstance().getTimeInMillis();
		if(isLoggingInfo()){
			vlogInfo("starTime : {0} ,endTime : {1} ==>> Total Time Consumed For postCommitOrder(): {2}",starTime,endTime, (endTime- starTime));
		}
		vlogDebug("Exiting from TRUCommitOrderFormHandler.postCommitOrder() method");
	}

	/**
	 * Pare commit order.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void preCommitOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		vlogDebug("START TRUCommitOrderFormHandler:: preCommitOrder method for Order :{0} and Profile :{1}",getOrder().getId(),getProfile().getRepositoryId());
		long starTime = Calendar.getInstance().getTimeInMillis();
		final String myHandleMethod = TRUCheckoutConstants.PRE_COMMIT_ORDER;
		final TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
		final TransactionDemarcation transactionDemarcation = new TransactionDemarcation();
		String enteredBy = null;
		String enteredLocation = null;
		boolean isEnableAddressDoctor = false;
		boolean doRollBack=false;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(myHandleMethod);
		}
		try {
			transactionDemarcation.begin(getTransactionManager(), TransactionDemarcation.REQUIRES_NEW);			
			synchronized (getOrder()) {				
				TRUOrderImpl order = (TRUOrderImpl) getOrder();
				ensureAllShipMethods();
				if (getFormError()) {
					return;
				}				
				TRUPurchaseProcessHelper purchaseProcessHelper = (TRUPurchaseProcessHelper) getPurchaseProcessHelper();
				if (isRestService()) {
					order.setSourceOfOrder(getTRUConfiguration().getMobileSourceOrderType());
				} else if (purchaseProcessHelper.isSOSOrder(pRequest)) {
					order.setSourceOfOrder(getTRUConfiguration().getSosSourceOrderType());
					enteredBy = purchaseProcessHelper.getEnteredByValue(pRequest);
					enteredLocation = purchaseProcessHelper.getEnteredLocation(pRequest);
					if(StringUtils.isNotBlank(enteredBy)){
						order.setEnteredBy(enteredBy);
					}
					if(StringUtils.isNotBlank(enteredLocation)){
						order.setEnteredLocation(enteredLocation);
					}
				} else {
					order.setSourceOfOrder(getTRUConfiguration().getWebSourceOrderType());
				}								
				if (TRUCheckoutConstants.REVIEW_PAGE.equalsIgnoreCase(getPageName())) {
					// Apply Reward Number
					preCommitOrderFromOrderReview(pRequest, pResponse);					
					// Validate order remaining amount
					validateOrderRemainingAmount(pRequest, pResponse);					
					//Start : MVP 8 Change for Invenotory Check				
					truValidateForCheckoutChain();				
					if (order.isValidationFailed()  || order.isPgRemoved()) {			
						if(order.isValidationFailed()){
							if(isRestService() && order.getTotalItemCount() > 0){
								mVe.addValidationError(TRUErrorKeys.INVALID_OUTOFSTOCK_MSG, null);
								getErrorHandler().processException(mVe, this);
							}else if(isRestService() && order.getTotalItemCount() == 0 && !order.getOutOfStockItems().isEmpty()){
								mVe.addValidationError(TRUErrorKeys.INVALID_OUTOFSTOCK_CART_MSG, null);
								getErrorHandler().processException(mVe, this);
							}
							else{
							addFormException(new DropletException(String.valueOf(Boolean.FALSE)));
							}
						}	
						if(order.isPgRemoved()){
							order.setPgRemoved(Boolean.FALSE);
						}										
					}
					if(((TRUOrderImpl)getOrder()).isFinanceAgreed() && isNoFinanceAgreed()) {
						order.setFinanceAgreed(Boolean.FALSE);
					}
					if(order.isFinanceAgreed()) {
						order.setFinancingTermsAvailed(((TRUOrderImpl)getOrder()).getFinancingTermsAvailable());
					}
					//End : MVP 8 Change for Invenotory Check	
					//orderManager.updateOrder(getOrder());
					return;
				} 
				// Validation for payment method selection
				ensureEmailAddress();
				if (getFormError()) {
					return;
				}								
				// For Credit card
				double giftCardAmtApplied = ((TRUOrderTools) (orderManager.getOrderTools())).getGiftCardAppliedAmount(getOrder());
				double orderTotal = TRUCheckoutConstants.DOUBLE_ZERO;
				if(null != getOrder() && null != getOrder().getPriceInfo()){
					orderTotal = roundTwoPlaceDecimal(getOrder().getPriceInfo().getTotal(), TRUConstants.TWO);
				}
				//orderTotal = roundTwoPlaceDecimal(getOrder().getPriceInfo().getTotal(), TRUConstants.TWO);
				double balnceAmount = orderTotal - giftCardAmtApplied;
				if (balnceAmount != TRUCommerceConstants.DOUBLE_ZERO
						&& TRUCheckoutConstants.CREDITCARD.equalsIgnoreCase(getPaymentGroupType())) {
					validateCommitOrderFromPayment();
					if (getFormError()) {						
						return;
					}
				}				
				ensureRewardNumber();
				if(getFormError()){
					return ;
				}
				// Validate order remaining amount
				validateOrderRemainingAmount(pRequest, pResponse);
				if(getFormError()){
					return ;
				}

				boolean isAddressValidated = Boolean.parseBoolean(getAddressValidated());
				if (!TRUConstants.UNITED_STATES.equalsIgnoreCase(getAddressInputFields().getCountry())){
					isAddressValidated = true;
				}
				if(SiteContextManager.getCurrentSite() != null){
					isEnableAddressDoctor = getIntegrationStatusUtil().getAddressDoctorIntStatus(pRequest);
				}
				isAddressValidated = addressDoctorValidation(isAddressValidated,isEnableAddressDoctor);		
				if (!getFormError() && (isAddressValidated || !TRUConstants.UNITED_STATES.equalsIgnoreCase(getAddressInputFields().getCountry()))) {
					commitOrderFromPayment(pRequest, pResponse);
					if(((TRUOrderImpl)getOrder()).isFinanceAgreed() && isNoFinanceAgreed()) {
						order.setFinanceAgreed(Boolean.FALSE);
					}
					if(order.isFinanceAgreed()) {
						order.setFinancingTermsAvailed(((TRUOrderImpl)getOrder()).getFinancingTermsAvailable());
					}
					if (getFormError()) {
						return;
					}
					
					super.preCommitOrder(pRequest, pResponse);
				}
				//Start : MVP 8 Change for Invenotory Check
				if(isAddressValidated && !getFormError()){
					truValidateForCheckoutChain();				
					if (order.isValidationFailed()  || order.isPgRemoved()) {			
						if(order.isValidationFailed()){
							if(isRestService() && order.getTotalItemCount() > 0){
								mVe.addValidationError(TRUErrorKeys.INVALID_OUTOFSTOCK_MSG, null);
								getErrorHandler().processException(mVe, this);
							}else if(isRestService() && order.getTotalItemCount() == 0 && !order.getOutOfStockItems().isEmpty()){
								mVe.addValidationError(TRUErrorKeys.INVALID_OUTOFSTOCK_CART_MSG, null);
								getErrorHandler().processException(mVe, this);
							}
							else{
							addFormException(new DropletException(String.valueOf(Boolean.FALSE)));
							}
						}	
						if(order.isPgRemoved()){
							order.setPgRemoved(Boolean.FALSE);
						}										
					}	
				}							
				//End : MVP 8 Change for Invenotory Check				
			}
		} catch (CommerceException | RunProcessException | TransactionDemarcationException | RepositoryException exp ) {
			doRollBack=true;
			if (PerformanceMonitor.isEnabled()) {
				PerformanceMonitor.cancelOperation(myHandleMethod);
			}
			if (isLoggingError()) {
				vlogError("CommerceException occured during @Class::TRUCommitOrderFormHandler.preCommitOrder() for order Id :{0} and details {1}" , getOrder().getId(), exp);
			}
		}finally {
			try {
				orderManager.updateOrder(getOrder());
			} catch (CommerceException e) {
				doRollBack=true;
				if (isLoggingError()) {
					vlogError("CommerceException : Unable to update the order {0}  and exception deatails {1}",getOrder().getId(),e);
				}				
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.cancelOperation(myHandleMethod);
				}
			}
			if(getFormError() && !((TRUOrderImpl)getOrder()).isValidationFailed()){
				doRollBack=true;
			}
			if (transactionDemarcation != null) {
				try {					
					transactionDemarcation.end(doRollBack);
				} catch (TransactionDemarcationException demarcationException) {
					if (isLoggingError()) {
						logError("TransactionDemarcationException occurs @Class::TRUCommitOrderFormHandler::@method::preCommitOrder() ",demarcationException);
					}
				}
			}
			if (PerformanceMonitor.isEnabled()) {
				PerformanceMonitor.endOperation(myHandleMethod);
			}
		long endTime = Calendar.getInstance().getTimeInMillis();
		if(isLoggingInfo()){
			vlogInfo("starTime : {0} ,endTime : {1} ==>> Total Time Consumed For PreCommit(): {2}",starTime,endTime, (endTime- starTime));
		}
		}
		vlogDebug("END TRUCommitOrderFormHandler:: preCommitOrder method for Order :{0} and Profile :{1}",getOrder().getId(),getProfile().getRepositoryId());
	}

	/**
	 * @param pAddressValidated - pAddressValidated
	 * @param pEnableAddressDoctor - pEnableAddressDoctor
	 * @return addressValidated 
	 * @throws ServletException - ServletException
	 */
	private boolean addressDoctorValidation(boolean pAddressValidated,boolean pEnableAddressDoctor)
			throws ServletException {
		boolean addressValidated = pAddressValidated;
		boolean enableAddressDoctor = pEnableAddressDoctor;
		// Validate address with AddressDoctor Service
		if (!addressValidated && enableAddressDoctor) {
			String addressStatus = ((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).validateAddress(getAddressInputFields(), getOrder(), getProfile(), 
					TRUConstants.SHIPPING,TRUConstants.ADD_ADDRESS);
			setAddressDoctorProcessStatus(addressStatus);
			TRUAddressDoctorResponseVO addresResponseVo = ((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).getProfileTools().getAddressDoctorResponseVO();
			setAddressDoctorResponseVO(addresResponseVo);
			if (TRUConstants.ADDRESS_DOCTOR_STATUS_VERIFIED.equalsIgnoreCase(addressStatus)) {
				addressValidated = Boolean.TRUE;
			}
			else if (TRUConstants.ADDRESS_DOCTOR_STATUS_CORRECTED.equalsIgnoreCase(addressStatus)) {
				String derivedStatus = addresResponseVo.getDerivedStatus();
				if(derivedStatus.equalsIgnoreCase(TRUConstants.NO)){
					addressValidated = Boolean.TRUE;
					ContactInfo contactInfo = getAddressInputFields();
					if(!StringUtils.isBlank(addresResponseVo.getAddressDoctorVO().get(0).getAddress1())){
					contactInfo.setAddress1(addresResponseVo.getAddressDoctorVO().get(0).getAddress1());}
					if(!StringUtils.isBlank(addresResponseVo.getAddressDoctorVO().get(0).getAddress2())){
					contactInfo.setAddress2(addresResponseVo.getAddressDoctorVO().get(0).getAddress2());}
					if(!StringUtils.isBlank(addresResponseVo.getAddressDoctorVO().get(0).getCity())){
					contactInfo.setCity(addresResponseVo.getAddressDoctorVO().get(0).getCity());}
					if(!StringUtils.isBlank(addresResponseVo.getAddressDoctorVO().get(0).getState())){
					contactInfo.setState(addresResponseVo.getAddressDoctorVO().get(0).getState());}
					if(!StringUtils.isBlank(addresResponseVo.getAddressDoctorVO().get(0).getPostalCode())){
					contactInfo.setPostalCode(addresResponseVo.getAddressDoctorVO().get(0).getPostalCode());
					}
					setAddressInputFields(contactInfo);
				}
				else{
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ADDRESS_DOCTOR_PLACE_ORDER, null);
					getErrorHandler().processException(mVe, this);
				}
				
			} else if (TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR.equalsIgnoreCase(addressStatus)) {
				setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR);
				TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
				addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
				addressDoctorResponseVO.setDerivedStatus(addresResponseVo.getDerivedStatus());
				setAddressDoctorResponseVO(addressDoctorResponseVO);
				//TUW-58890 :: Start
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ADDRESS_DOCTOR_PLACE_ORDER, null);
				getErrorHandler().processException(mVe, this);
				//TUW-58890 :: End
			}else if (addressStatus != null && TRUConstants.CONNECTION_ERROR.equalsIgnoreCase(addressStatus)) {
				setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_CONNECTION_ERROR);
				TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
				addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
				addressDoctorResponseVO.setDerivedStatus(TRUConstants.YES);
				setAddressDoctorResponseVO(addressDoctorResponseVO);
			}
		} else if (!enableAddressDoctor && !addressValidated) {
			setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR);
			TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
			addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
			addressDoctorResponseVO.setDerivedStatus(TRUConstants.YES);
			setAddressDoctorResponseVO(addressDoctorResponseVO);
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ADDRESS_DOCTOR_PLACE_ORDER, null);
			getErrorHandler().processException(mVe, this);
		}
		return addressValidated;
	}

	/**
	 * Pre commit order from order review.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws RunProcessException
	 *             the run process exception
	 * @throws ServletException
	 *             the servlet exception
	 * @throws CommerceException
	 *             the commerce exception
	 * @throws RepositoryException 
	 */
	public void preCommitOrderFromOrderReview(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws RunProcessException, ServletException, CommerceException, RepositoryException {
			vlogDebug("START TRUCommitOrderFormHandler:: preCommitOrderFromOrderReview method");
		TRUPurchaseProcessHelper purchaseProcessHelper = (TRUPurchaseProcessHelper) getPurchaseProcessHelper();
		final TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		Order order = getOrder();
		if (StringUtils.isNotEmpty(getRewardNumber()) && getRewardNumberValid().equalsIgnoreCase(TRUConstants.FALSE)) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_REWARD_NUMBER_INVALID, null);
			getErrorHandler().processException(mVe, this);
		}
		if (getFormError()) {
			return;
		}
		//Start :: TUW-56721
		if (!getFormError()) {
			profileTools.updateProperty(((TRUPropertyManager) profileTools.getPropertyManager()).getRewardNumberPropertyName(),
				((TRUOrderImpl) order).getRewardNumber(), getProfile());
			((TRUOrderImpl)order).setRewardNumber(getRewardNumber());
		}
		//End :: TUW-56721
		String enteredBy = null;
		String enteredLocation = null;
		if (order instanceof TRUOrderImpl) {
			if (isRestService()) {
				((TRUOrderImpl) order).setSourceOfOrder(getTRUConfiguration().getMobileSourceOrderType());
			} else if (purchaseProcessHelper.isSOSOrder(pRequest)) {
				((TRUOrderImpl) order).setSourceOfOrder(getTRUConfiguration().getSosSourceOrderType());
				enteredBy = purchaseProcessHelper.getEnteredByValue(pRequest);
				enteredLocation = purchaseProcessHelper.getEnteredLocation(pRequest);
				if(StringUtils.isNotBlank(enteredBy)){
					((TRUOrderImpl) order).setEnteredBy(enteredBy);
				}
				if(StringUtils.isNotBlank(enteredLocation)){
					((TRUOrderImpl) order).setEnteredLocation(enteredLocation);
				}
			} else {
				((TRUOrderImpl) order).setSourceOfOrder(getTRUConfiguration().getWebSourceOrderType());
			}
		}
		// Getting Credit card
		//TRUCreditCard creditCard = (TRUCreditCard) ((TRUOrderManager) getOrderManager()).getCreditCard(getOrder());
		// set cardinal properties
		/*if (null != creditCard) {
			//getBillingHelper().updateCardinalProperties(getCreditCardInfoMap().get(TRUConstants.CARDINAL_JWT), creditCard);
		}*/			
			vlogDebug("END TRUCommitOrderFormHandler:: preCommitOrderFromOrderReview method");
	}

	/**
	 * @param orderManager
	 * @throws RunProcessException RunProcessException
	 * @throws CommerceException CommerceException
	 *//*
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void runProcessorForInventoryCheck()
			throws RunProcessException, CommerceException {		
		TRUOrderManager orderManager=(TRUOrderManager)getOrderManager();
		TRUOrderImpl orderImpl=(TRUOrderImpl) getOrder();
		Map extraParams = new HashMap();
		TRUUserSession sessionComponent = getUserSession();
		extraParams.put(TRUPipeLineConstants.SESSION_COMPONENT, sessionComponent);
		extraParams.put(TRUPipeLineConstants.CHAIN_ID, TRUPipeLineConstants.PROCESS_ORDER);
		extraParams.put(TRUTaxwareConstant.CALCULATE_TAX, Boolean.TRUE);
		extraParams.put(TRUCommerceConstants.UPDATE_SHIP_METHOD_PRICE,Boolean.TRUE);
		((TRUOrderImpl) getOrder()).setValidationFailed(Boolean.FALSE);
		((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).runProcessTRUValidateForCheckoutChain(getOrder(), getUserPricingModels(),
				getUserLocale(), getProfile(), extraParams, null, this);
		// Calling the reprice order and adjusting the PGs when the order gets modified
		
		if (orderImpl.isValidationFailed()  || orderImpl.isPgRemoved()) {			
			orderImpl.setPgRemoved(Boolean.FALSE);
			if(orderImpl.isValidationFailed()){
				addFormException(new DropletException(String.valueOf(Boolean.FALSE)));
			}	
			if(orderImpl.isPgRemoved()){
				orderImpl.setPgRemoved(Boolean.FALSE);
			}
			runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL, getOrder(), getUserPricingModels(),
					getUserLocale(), getProfile(), extraParams);
			orderManager.adjustPaymentGroups(getOrder());	
			orderManager.updateOrder(getOrder());
		}
	}*/

	/**
	 * <Description> Remove the Pay pal PG from order.
	 */
	public void removePaypalPGFromOrder() {
		if (isLoggingDebug()) {
			vlogDebug("Start of  TRUCommitOrderFormHandler :: removePaypalPGFromOrder()");
		}
		String payPalErrMessage = null;
		String payPalErrKey = null;
		boolean isRollBack = false;
		TransactionDemarcation transactionDemarcation = new TransactionDemarcation();
		try {
			transactionDemarcation.begin(getTransactionManager(), TransactionDemarcation.REQUIRES_NEW);
			if (StringUtils.isNotBlank(payPalErrKey)) {
				payPalErrMessage = getErrorHandlerManager().getErrorMessage(payPalErrKey);
			}
			synchronized (getOrder()) {
				TRUPaymentGroupManager paymentGroupManager = (TRUPaymentGroupManager) getPaymentGroupManager();
				paymentGroupManager.removePayPalPaymentGroupToOrder(getOrder());
				((TRUOrderImpl)getOrder()).setPayPalOrder(Boolean.FALSE);
				((TRUOrderImpl)getOrder()).setExpressPayPal(Boolean.FALSE);
				((TRUOrderImpl)getOrder()).setStanderdPayPal(Boolean.FALSE);
				// ((TRUOrderManager)
				// getOrderManager()).createCreditCardPaymentGroupToOrder(getOrder(),getPaymentGroupMapContainer());
				if (!TRUPayPalConstants.CART_PAGE_NAME.equals(getPageName())) {
					((TRUOrderImpl) getOrder()).setCurrentTab(TRUCheckoutConstants.PAYMENT_TAB);
				}
				getUserSession().setPaypalPaymentDetailsMap(null);
				((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL,
						getOrder(), getUserPricingModels(), getUserLocale(), getProfile(), createRepriceParameterMaps(), this);
				((TRUOrderManager) getOrderManager()).updateOrder(getOrder());
			}
		} catch (TransactionDemarcationException demarcationException) {
			addFormException(new DropletException(payPalErrMessage));
			if (isLoggingError()) {
				logError("TransactionDemarcationException occurs @Class::TRUCommitOrderFormHandler::@method::removePaypalPGFromOrder()",
						demarcationException);
			}
		} catch (CommerceException commerceException) {
			isRollBack = true;
			addFormException(new DropletException(payPalErrMessage));
			if (isLoggingError()) {
				logError("CommerceException occurs @Class::TRUCommitOrderFormHandler::@method::removePaypalPGFromOrder() ",
						commerceException);
			}
		} catch (SystemException systemException) {
			isRollBack = true;
			addFormException(new DropletException(payPalErrMessage));
			if (isLoggingError()) {
				logError("SystemException occurs @Class::TRUCommitOrderFormHandler::@method::removePaypalPGFromOrder() ", systemException);
			}
		} catch (RunProcessException re) {
			isRollBack = true;
			if (isLoggingError()) {
				logError("RunProcessException occurs @Class::TRUCommitOrderFormHandler::@method::removePaypalPGFromOrder() ", re);
			}
		} finally {
			if (transactionDemarcation != null) {
				try {
					transactionDemarcation.end(isRollBack);
				} catch (TransactionDemarcationException demarcationException) {
					addFormException(new DropletException(payPalErrMessage));
					if (isLoggingError()) {
						logError(
								"TransactionDemarcationException occurs @Class::TRUCommitOrderFormHandler::@method::removePaypalPGFromOrder() ",
								demarcationException);
					}
				}
			}
		}
			vlogDebug("End of  TRUCommitOrderFormHandler :: removePaypalPGFromOrder()");
	}

	/**
	 * this method is used to round off the decimal value with two place.
	 * 
	 * @param pNumber
	 *            the number
	 * @param pRoundingDecimalPlaces
	 *            the roundingDecimalPlaces
	 * @return BigDecimal Object
	 */
	private double roundTwoPlaceDecimal(double pNumber, int pRoundingDecimalPlaces) {
		if (isLoggingDebug()) {
			vlogDebug("TRUCommitOrderFormHandler:: (roundTwoPlaceDecimal): pNumber:{0}", pNumber);
		}
		BigDecimal bd = new BigDecimal(Double.toString(pNumber));
		bd = bd.setScale(pRoundingDecimalPlaces, TRUTaxwareConstant.NUMERIC_FOUR);
		if (isLoggingDebug()) {
			vlogDebug("TRUCommitOrderFormHandler: (roundTwoPlaceDecimal): bid=g decimal:{0}", bd);
		}
		return bd.doubleValue();
	}

	/**
	 * This method invokes the pipeline chain "payPalProcessorChain" to process
	 * the order. This is for reverse Authorize PAYPAL. Internally it'll trigger
	 * doVoid invocation.
	 * 
	 * @param pRequest
	 *            - the request.
	 * @param pResponse
	 *            - the response
	 * @throws CommerceException
	 *             - the CommerceException
	 * @throws RunProcessException
	 *             - the RunProcessException
	 * @throws ServletException
	 *             - the ServletException
	 * @throws IOException
	 *             - the IOException
	 */
	public void runDoVoidPayPalProcessorChain(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws CommerceException, RunProcessException, ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN  TRUCommitOrderFormHandler :: runProcessPayPalProcessorChain()");
		}
		Map params = new HashMap(TRUPayPalConstants.DIGIT_ELEVEN);
		params.put(TRUPipeLineConstants.REVERSE_ACTION, Boolean.TRUE);
		final TRUPayPal payPalPG = ((TRUOrderManager) getOrderManager()).fetchPayPalPaymentGroupForOrder(getOrder());
		// Get the reverse authorization chain name.
		final String chainName = ((TRUPaymentManager) ((TRUOrderManager) getOrderManager()).getPaymentManager())
				.getReverseAuthorizationChainName(payPalPG);
		if (isLoggingDebug()) {
			vlogDebug("payPalProcessorChainId : " + chainName);
		}
		final PipelineResult result = runProcess(chainName, getOrder(), getUserPricingModels(), getUserLocale(pRequest, pResponse), getProfile(),
				params, null);
		if (processPipelineErrors(result)) {
			for (Object error : result.getErrorKeys()) {
				if (isLoggingError()) {
					logError("Exception in @Class::TRUCommitOrderFormHandler::@method::runDoVoidPayPalProcessorChain(): ProfileId="
							+ getProfile().getRepositoryId() + " Pipeline error :: " + result.getError(error));
				}
			}
			// addFormException(new
			// DropletFormException(TRUCheckoutConstants.EMPTY_STRING,
			// TRUCheckoutConstants.INTERNAL_ERROR,
			// CheckoutConstants.EXCEPTION));
		}
		if (isLoggingDebug()) {
			vlogDebug("END TRUCommitOrderFormHandler :: runProcessPayPalProcessorChain()");
		}
	}

	/**
	 * This method invokes the pipeline chain "moveToConfirmation" to process
	 * the order.
	 * 
	 * @param pRequest
	 *            - the request.
	 * @param pResponse
	 *            - the response
	 * @throws RunProcessException
	 *             - the RunProcessException
	 * @throws ServletException
	 *             - the ServletException
	 * @throws IOException
	 *             - the IOException
	 */
	public void runProcessMoveToConfirmation(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws RunProcessException, ServletException, IOException {
	if (isLoggingDebug()) {
			vlogDebug("BEGIN  TRUCommitOrderFormHandler:: runProcessMoveToConfirmation()");
			vlogDebug("moveToConfirmationChainId : " + getMoveToConfirmationChainId());
		}
		Map params = new HashMap(TRUPayPalConstants.DIGIT_ELEVEN);
		params.put(PricingConstants.PRICING_OPERATION_PARAM, PricingConstants.OP_REPRICE_ORDER_TOTAL);
		//params.put(TRUTaxwareConstant.CALC_TAX, Boolean.TRUE);		
		Map extraParameters = new HashMap();
		extraParameters.put(TRUTaxwareConstant.CALC_TAX, Boolean.TRUE);
		
	final	PipelineResult result = runProcess(getMoveToConfirmationChainId(), getOrder(), getUserPricingModels(),
				getUserLocale(pRequest, pResponse), getProfile(), params, extraParameters);
		if (processPipelineErrors(result)) {
			for (Object error : result.getErrorKeys()) {
				if (isLoggingError()) {
					logError("Exception in @Class::TRUCommitOrderFormHandler::@method::runProcessMoveToConfirmation():  ProfileId="
							+ getProfile().getRepositoryId() + " Pipeline error :: " + result.getError(error));
				}
			}
			// addFormException(new
			// DropletFormException(TRUCheckoutConstants.EMPTY_STRING,
			// TRUCheckoutConstants.INTERNAL_ERROR,
			// CheckoutConstants.EXCEPTION));
		}
		if (isLoggingDebug()) {
			vlogDebug("END TRUCommitOrderFormHandler :: runProcessMoveToConfirmation()");
		}
	}

	/**
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse.
	 * @throws RunProcessException
	 *             RunProcessException
	 * @throws IOException - IOException
	 * @throws ServletException - ServletException
	 * @return true or false
	 */
	protected boolean runProcessValidateShippingGroups(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws RunProcessException, ServletException, IOException {
		Map<String, Object> extraParams = new HashMap<String, Object>();
		extraParams.put(TRUConstants.TRU_SHIPPING_MANAGER, getShippingManager());
		extraParams.put(TRUConstants.TRU_SHIPPING_TOOLS, getShippingManager().getShippingTools());
		final PipelineResult result = runProcess(getValidateShippingGroupsChainId(), getOrder(), getUserPricingModels(),
				getUserLocale(pRequest, pResponse), getProfile(), extraParams);
		return processPipelineErrors(result);
	}

	/**
	 * @param pBillingAddressNickName
	 *            parameter
	 */
	public void setBillingAddressNickName(String pBillingAddressNickName) {
		mBillingAddressNickName = pBillingAddressNickName;
	}

	/**
	 * @param pBillingHelper
	 *            the billingHelper to set.
	 */
	public void setBillingHelper(TRUBillingProcessHelper pBillingHelper) {
		mBillingHelper = pBillingHelper;
	}

	/**
	 * @param pCookieManager
	 *            the cookieManager to set
	 */
	public void setCookieManager(TRUCookieManager pCookieManager) {
		mCookieManager = pCookieManager;
	}

	/**
	 * @param pCreditCardInfoMap
	 *            the creditCardInfoMap to set.
	 */
	public void setCreditCardInfoMap(Map<String, String> pCreditCardInfoMap) {
		mCreditCardInfoMap = pCreditCardInfoMap;
	}

	/**
	 * @param pCreditCardNickName
	 *            credit card nick name
	 */
	public void setCreditCardNickName(String pCreditCardNickName) {
		mCreditCardNickName = pCreditCardNickName;
	}

	/**
	 * @param pEmail
	 *            the email to set
	 */
	public void setEmail(String pEmail) {
		this.mEmail = pEmail;
	}

	/**
	 * @param pEmailPasswordValidator
	 *            the emailPasswordValidator to set.
	 */
	public void setEmailPasswordValidator(EmailPasswordValidator pEmailPasswordValidator) {
		this.mEmailPasswordValidator = pEmailPasswordValidator;
	}

	/**
	 * This method is used to set epslonOrderAcknowledgeSource.
	 * 
	 * @param pEpslonOrderAcknowledgeSource
	 *            TRUEpslonOrderAcknowledgeSource
	 */
	public void setEpslonOrderAcknowledgeSource(TRUEpslonOrderAcknowledgeSource pEpslonOrderAcknowledgeSource) {
		mEpslonOrderAcknowledgeSource = pEpslonOrderAcknowledgeSource;
	}

	/**
	 * @param pErrorHandler
	 *            the IErrorHandler to set
	 */
	public void setErrorHandler(IErrorHandler pErrorHandler) {
		this.mErrorHandler = pErrorHandler;
	}

	/**
	 * Sets the error handler manager.
	 * 
	 * @param pErrorHandlerManager
	 *            the new error handler manager
	 */
	public void setErrorHandlerManager(DefaultErrorHandlerManager pErrorHandlerManager) {
		mErrorHandlerManager = pErrorHandlerManager;
	}

	/**
	 * @param pIntegrationStatusUtil
	 *            the integrationStatusUtil to set
	 */
	public void setIntegrationStatusUtil(TRUIntegrationStatusUtil pIntegrationStatusUtil) {
		mIntegrationStatusUtil = pIntegrationStatusUtil;
	}

	/**
	 * This method sets moveToConfirmationChainId.
	 * 
	 * @param pMoveToConfirmationChainId
	 *            the moveToConfirmationChainId to set
	 */
	public void setMoveToConfirmationChainId(String pMoveToConfirmationChainId) {
		mMoveToConfirmationChainId = pMoveToConfirmationChainId;
	}

	/**
	 * @param pOrderSetWithDefault
	 *            parameter
	 */
	public void setOrderSetWithDefault(boolean pOrderSetWithDefault) {
		mOrderSetWithDefault = pOrderSetWithDefault;
	}

	/**
	 * @param pPageName
	 *            the pageName to set.
	 */
	public void setPageName(String pPageName) {
		mPageName = pPageName;
	}

	/**
	 * @param pPayerId
	 *            the pPayerId to set.
	 */
	public void setPayerId(String pPayerId) {
		this.mPayerId = pPayerId;
	}

	/**
	 * @param pPaymentGroupType
	 *            the paymentGroupType to set.
	 */
	public void setPaymentGroupType(String pPaymentGroupType) {
		mPaymentGroupType = pPaymentGroupType;
	}

	
	/**
	 * @param pPaypalRedirectURL
	 *            the paypalRedirectURL to set
	 */
	public void setPaypalRedirectURL(String pPaypalRedirectURL) {
		mPaypalRedirectURL = pPaypalRedirectURL;
	}

	/**
	 * Sets the mPayPalUtilityManager.
	 * 
	 * @param pPayPalUtilityManager
	 *            the mPayPalUtilityManager to set
	 */
	public void setPayPalUtilityManager(TRUPaypalUtilityManager pPayPalUtilityManager) {
		this.mPayPalUtilityManager = pPayPalUtilityManager;
	}

	/**
	 * @param pPromotionTools
	 *            the promotionTools to set
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		mPromotionTools = pPromotionTools;
	}

	/**
	 * @param pPropertyManager
	 *            the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * Sets the rest service.
	 * 
	 * @param pRestService
	 *            the pRestService to set
	 */
	public void setRestService(boolean pRestService) {
		this.mRestService = pRestService;
	}

	/**
	 * @param pRewardNumber
	 *            the mRewardNumber to set
	 */
	public void setRewardNumber(String pRewardNumber) {
		this.mRewardNumber = pRewardNumber;
	}

	/**
	 * @param pRewardNumberValid
	 *            the rewardNumberValid to set.
	 */
	public void setRewardNumberValid(String pRewardNumberValid) {
		this.mRewardNumberValid = pRewardNumberValid;
	}

	/**
	 * @param pRUSCard
	 *            the mRUSCard to set.
	 */
	public void setRUSCard(boolean pRUSCard) {
		this.mRUSCard = pRUSCard;
	}

	/**
	 * @param pSelectedCardBillingAddress
	 *            the selectedCardBillingAddress to set
	 */
	public void setSelectedCardBillingAddress(String pSelectedCardBillingAddress) {
		this.mSelectedCardBillingAddress = pSelectedCardBillingAddress;
	}

	/**
	 * @param pShippingHelper
	 *            the shippingHelper to set.
	 */
	public void setShippingHelper(TRUShippingProcessHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}

	/**
	 * @param pShippingManager
	 *            the shippingManager to set.
	 */
	public void setShippingManager(TRUShippingManager pShippingManager) {
		mShippingManager = pShippingManager;
	}

	/**
	 * This method is used to set tRUConfiguration.
	 * 
	 * @param pTRUConfiguration
	 *            TRUConfiguration
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		mTRUConfiguration = pTRUConfiguration;
	}

	/**
	 * Sets the user session.
	 * 
	 * @param pUserSession
	 *            the userSession to set
	 */
	public void setUserSession(TRUUserSession pUserSession) {
		mUserSession = pUserSession;
	}

	/**
	 * @param pValidateShippingGroupsChainId
	 *            the validateShippingGroupsChainId to set.
	 */
	public void setValidateShippingGroupsChainId(String pValidateShippingGroupsChainId) {
		mValidateShippingGroupsChainId = pValidateShippingGroupsChainId;
	}

	/**
	 * @param pValidator
	 *            the IValidator to set
	 */
	public void setValidator(IValidator pValidator) {
		this.mValidator = pValidator;
	}

	/**
	 * This method used to trim the address values in a contactInfo bean.
	 * 
	 * @param pAddress
	 *            The map to hold the address details entered by the user
	 */
	public void trimContactInfoValues(ContactInfo pAddress) {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUCommitOrderFormHandler.trimContactInfoValues method");
		}
		getAddressInputFields().setFirstName(pAddress.getFirstName().trim());
		getAddressInputFields().setLastName(pAddress.getLastName().trim());
		getAddressInputFields().setAddress1(pAddress.getAddress1().trim());
		if(StringUtils.isNotBlank(pAddress.getAddress2())){
		getAddressInputFields().setAddress2(pAddress.getAddress2().trim());
		}
		getAddressInputFields().setCity(pAddress.getCity().trim());
		getAddressInputFields().setState(pAddress.getState().trim());
		getAddressInputFields().setPostalCode(pAddress.getPostalCode().trim());
		getAddressInputFields().setPhoneNumber(pAddress.getPhoneNumber().trim());
		getAddressInputFields().setCountry(pAddress.getCountry().trim());
		if (isLoggingDebug()) {
			vlogDebug("End of TRUCommitOrderFormHandler.trimContactInfoValues method");
		}
	}

	/**
	 * Tru validate for checkout chain.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void truValidateForCheckoutChain() {
		final TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
		TRUOrderImpl order=(TRUOrderImpl) getOrder();
		// Custom TRU validation
		Map parameters = new HashMap();
		boolean rollBack = false;
		//TransactionDemarcation transactionDemarcation = new TransactionDemarcation();
		try {
			//transactionDemarcation.begin(getTransactionManager(), TransactionDemarcation.REQUIRES_NEW);
			synchronized (getOrder()) {
				TRUUserSession sessionComponent = getUserSession();
				parameters.put(TRUPipeLineConstants.SESSION_COMPONENT, sessionComponent);
				parameters.put(TRUPipeLineConstants.CHAIN_ID, TRUPipeLineConstants.PROCESS_ORDER);				
				parameters.put(TRUPipeLineConstants.SESSION_COMPONENT, sessionComponent);
				parameters.put(TRUTaxwareConstant.CALC_TAX, Boolean.TRUE);
				parameters.put(TRUCommerceConstants.UPDATE_SHIP_METHOD_PRICE,Boolean.TRUE);
				order.setValidationFailed(Boolean.FALSE);								
				((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).runProcessTRUValidateForCheckoutChain(getOrder(),
						getUserPricingModels(), getUserLocale(), getProfile(), parameters, null, this);
				// Calling the reprice order and adjusting the PGs when the
				// order gets modified
				if (order.isValidationFailed()) {
					((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL,
							getOrder(), getUserPricingModels(), getUserLocale(), getProfile(), createRepriceParameterMaps(), this);
					orderManager.adjustPaymentGroups(getOrder());
					orderManager.updateOrder(getOrder());
				}
			}
		} catch (RunProcessException rpE) {
			rollBack = true;
			if (isLoggingError()) {
				logError("RunProcessException occured during truValidateForCheckoutChain @Class::TRUCommitOrderFormHandler::@method::"
						+ "truValidateForCheckoutChain() for order Id : " + getOrder().getId(), rpE);
			}
		} catch (CommerceException commerceException) {
			rollBack = true;
			if (isLoggingError()) {
				logError("CommerceException occured during truValidateForCheckoutChain @Class::TRUCommitOrderFormHandler::@method::"
						+ "truValidateForCheckoutChain() for order Id : " + getOrder().getId(), commerceException);
			}
		} finally {
			//if (transactionDemarcation != null) {
				//try {
					if (rollBack) {
						addFormException(new DropletException(TRUConstants.PLEASE_TRY_AGAIN_ERROR));
					}
					//transactionDemarcation.end(rollBack);
				/*} catch (TransactionDemarcationException demarcationException) {
					if (isLoggingError()) {
						logError(
								"TransactionDemarcationException occurs @Class::TRUCommitOrderFormHandler::@method::truValidateForCheckoutChain()",
								demarcationException);
					}
				}*/
			}
		//}
	}

	/**
	 * updates purchased registry item to service.
	 * 
	 * @throws RepositoryException
	 *             RepositoryException
	 * @throws CommerceException
	 *             CommerceException
	 */
	protected void updateRegistryItem() throws CommerceException, RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("Start for TRUCommitOrderFormHandler.updateRegistryItem() method");
		}
		final TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();

		Order lastOrder = getShoppingCart().getLast();
		if (lastOrder != null) {
			orderManager.updateRegistryItem(getShoppingCart());
		}

		if (isLoggingDebug()) {
			vlogDebug("Exiting from TRUCommitOrderFormHandler.updateRegistryItem() method");
		}
	}

	/**
	 * Validate commit order same as OOTB.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void validateCommitOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			vlogDebug("START :: TRUCommitOrderFormHandler.validateCommitOrder method");
		}
		Order lastOrder = getShoppingCart().getLast();
		String orderId = getOrderId();
		if (orderId != null && lastOrder != null && orderId.equals(lastOrder.getId())) {
			// Trying to submit the previous order again
			String msg = formatUserMessage(MSG_ORDER_ALREADY_SUBMITTED, pRequest, pResponse);
			String propertyPath = generatePropertyPath(TRUCheckoutConstants.PROP_ORDER_ID);
			addFormException(new DropletFormException(msg, propertyPath, MSG_ORDER_ALREADY_SUBMITTED));
		} else if (orderId != null && !orderId.equals(getOrder().getId())) {
			// Trying to submit some order other than the current one, probably
			// because the user hit the browser's Back button
			String msg = formatUserMessage(MSG_ORDER_NOT_CURRENT, pRequest, pResponse);
			String propertyPath = generatePropertyPath(TRUCheckoutConstants.PROP_ORDER_ID);
			addFormException(new DropletFormException(msg, propertyPath, MSG_ORDER_NOT_CURRENT));
		} else if (!getAllowEmptyOrders() && getShoppingCart().isCurrentEmpty()) {
			// Trying to submit an empty order
			String msg = formatUserMessage(MSG_ORDER_EMPTY, pRequest, pResponse);
			String propertyPath = generatePropertyPath(TRUCheckoutConstants.ALLOW_EMPTY_ORDERS);
			addFormException(new DropletFormException(msg, propertyPath, MSG_ORDER_EMPTY));
		}
		if (isLoggingDebug()) {
			vlogDebug("END :: TRUCommitOrderFormHandler.validateCommitOrder method");
		}
	}

	/**
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 * @throws CommerceException - CommerceException
	 */
	private void validateCommitOrderFromPayment() throws ServletException, IOException, CommerceException {
		if (isLoggingDebug()) {
			//vlogDebug("Start of TRUBillingInfoFormHandler/validateCommitOrderFromPayment method");
			vlogDebug("Start of TRUCommitOrderFormHandler/validateCommitOrderFromPayment method");
		}
		try {
			if (isLoggingDebug()) {
				vlogDebug("TRUCommitOrderFormHandler.validateCommitOrderFromPayment :: addCreditCardWithoutAddress");
			}
			if (isNewCreditCard() || isEditCreditCard()) {
				final TRUPropertyManager propertyManager = getPropertyManager();
				if (getCreditCardInfoMap().get(propertyManager.getCreditCardNumberPropertyName()).startsWith(
								getTRUConfiguration().getCardNumberPLCC())) {
					getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationMonthPropertyName(),
							getTRUConfiguration().getExpMonthAdjustmentForPLCC());
					getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationYearPropertyName(),
							getTRUConfiguration().getExpYearAdjustmentForPLCC());
				}
				if (getProfile().isTransient()) {
					/*
					 * To validate all mandatory fields including email when
					 * user is not logged in along with email format
					 */
					if (((TRUOrderManager) getOrderManager()).isAnyCreditCardFieldEmpty(getCreditCardInfoMap())
							|| ((TRUOrderManager) getOrderManager()).isAnyAddressFieldEmpty(getAddressInputFields())) {
						if (isLoggingDebug()) {
							vlogDebug("TRUCommitOrderFormHandler.validateCommitOrderFromPayment is having errors");
						}
						getValidator().validate(TRUConstants.MY_ACCOUNT_ADD_CARD_WITHOUT_ADDR_FORM, this);
						mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
						getErrorHandler().processException(mVe, this);

					}
				} else {
					/* To validate all mandatory fields when user is logged in */
					if (((TRUOrderManager) getOrderManager()).isAnyCreditCardFieldEmpty(getCreditCardInfoMap())
							|| ((TRUOrderManager) getOrderManager()).isAnyAddressFieldEmpty(getAddressInputFields())) {
						if (isLoggingDebug()) {
							vlogDebug("TRUCommitOrderFormHandler.validateCommitOrderFromPayment is having errors");
						}
						getValidator().validate(TRUConstants.MY_ACCOUNT_ADD_CARD_WITHOUT_ADDR_FORM, this);
						mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
						getErrorHandler().processException(mVe, this);

					}
				}
			} else {
				// validates order credit card
				isOrderCreditCardExpired();
			}
			/*if (!getFormError() && StringUtils.isNotBlank(getRewardNumber()) && getRewardNumberValid().equalsIgnoreCase(TRUConstants.FALSE)) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_REWARD_NUMBER_INVALID, null);
				getErrorHandler().processException(mVe, this);
			}*/
		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			if (isLoggingError()) {
				logError("Validation exception occured: @Class::TRUCommitOrderFormHandler::@method::validateCommitOrderFromPayment()", ve);
			}
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			if (isLoggingError()) {
				logError("System exception occured: @Class::TRUCommitOrderFormHandler::@method::validateCommitOrderFromPayment()", se);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUBillingInfoFormHandler/validateCommitOrderFromPayment method");
		}

	}

	/**
	 * Validate order remaining amount.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("unchecked")
	public void validateOrderRemainingAmount(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		// validate the order for order amount.
		if (isLoggingDebug()) {
			vlogDebug("START TRUCommitOrderFormHandler:: validateOrderRemainingAmount method");
		}
		TRUOrderImpl order = (TRUOrderImpl) getOrder();
		List<PaymentGroup> listOfPaymentGroup = order.getPaymentGroups();
		if (listOfPaymentGroup == null || listOfPaymentGroup.isEmpty()) {
			double orderRemaningAmount = ((TRUOrderManager)getPurchaseProcessHelper().getOrderManager()).getOrderRemaningAmount(order);		
			if (orderRemaningAmount !=  TRUConstants.DOUBLE_ZERO) {
				mVe.addValidationError(TRUErrorKeys.ERR_OTHER_PAYMENT_OPTION_IS_REQUIRED, null);
				getErrorHandler().processException(mVe, this);
			}
		} else {
			boolean validateOrderRemainingAmount = getBillingHelper()
					.validateOrderRemainingAmount(order, TRUCheckoutConstants.COMMIT_ORDER);
			if (!validateOrderRemainingAmount) {
				mVe.addValidationError(TRUErrorKeys.GIFT_CARDS_BALANCE_IS_LESS_THAN_ORDER_AMOUNT, null);
				getErrorHandler().processException(mVe, this);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END TRUCommitOrderFormHandler:: validateOrderRemainingAmount method");
		}
	}
	
	/**
	 * Ensure reward number.
	 *
	 * @throws ServletException the servlet exception
	 * @throws RepositoryException 
	 */
	private void ensureRewardNumber() throws ServletException, RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("START TRUCommitOrderFormHandler:: ensureRewardNumber method");
		}
		final TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		if (!getFormError() && StringUtils.isNotEmpty(getRewardNumber()) && getRewardNumberValid().equalsIgnoreCase(TRUConstants.FALSE)) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_REWARD_NUMBER_INVALID, null);
			getErrorHandler().processException(mVe, this);
			return;
		}
		((TRUOrderImpl) getOrder()).setRewardNumber(getRewardNumber());
		profileTools.updateProperty(((TRUPropertyManager) profileTools.getPropertyManager()).getRewardNumberPropertyName(),
			getRewardNumber(), getProfile());
		if (isLoggingDebug()) {
			vlogDebug("END TRUCommitOrderFormHandler:: ensureRewardNumber method");
		
		}
	}
	
	/**
	 * <p>
	 * This method will check all the items are associated with shipping address and shipping method
	 * </p>
	 * @throws ServletException ServletException
	 */
	private void ensureAllShipMethods() throws ServletException{
		try {
			if (isLoggingDebug()) {
				vlogDebug("Start of TRUCommitOrderFormHandler.ensureAllShipMethods() method");
			}
			final List sgCiRels = getShippingGroupManager().getAllShippingGroupRelationships(getOrder());
			if (sgCiRels != null && !sgCiRels.isEmpty()) {
				final int sgCiRelsCount = sgCiRels.size();
				for (int i = 0; i < sgCiRelsCount; ++i) {
					final TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) sgCiRels
							.get(i);
					final CommerceItem commerceItem = sgCiRel.getCommerceItem();
					final TRUShippingGroupManager groupManager = (TRUShippingGroupManager) getShippingGroupManager();
					if (groupManager.isNotInstanceOfTRUCommerceItem(commerceItem)) {
						continue;
					}
					if (sgCiRel.getShippingGroup() instanceof HardgoodShippingGroup) {
						final TRUHardgoodShippingGroup sg = (TRUHardgoodShippingGroup) sgCiRel.getShippingGroup();
						final Address address = sg.getShippingAddress();
						if (isLoggingDebug()) {
							vlogDebug(
									"Shipping address.getFirstName() ={0} ,  address.getLastName()={1}, and ShippingMethod={2}",
									address.getFirstName(), address.getLastName(), sgCiRel.getShippingGroup().getShippingMethod());
						}
						if (address == null || StringUtils.isBlank(address.getFirstName()) ||
								 StringUtils.isBlank(address.getLastName()) || StringUtils.isBlank(address.getAddress1())) {
							mVe.addValidationError(TRUErrorKeys.TRU_ERROR_SHIPPING_ADDRESS_MISSING_INFO, null);
							getErrorHandler().processException(mVe, this);
							
						} else if (StringUtils.isBlank(sgCiRel.getShippingGroup().getShippingMethod())) {
							mVe.addValidationError(TRUErrorKeys.TRU_ERROR_SHIPPING_NO_SHIP_METHOD_SELECTED, null);
							getErrorHandler().processException(mVe, this);
						}

					}

				}
			}
		} catch (CommerceException e) {
			addFormException(new DropletException(TRUConstants.PLEASE_TRY_AGAIN_ERROR));
			if (isLoggingError()) {
				logError("Exception in TRUCommitOrderFormHandler.ensureAllShipMethods()", e);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUCommitOrderFormHandler.ensureAllShipMethods() method");
		}
	}

	/**
	 * @return the mAddressDoctorProcessStatus
	 */
	public String getAddressDoctorProcessStatus() {
		return mAddressDoctorProcessStatus;
	}

	/**
	 * @param pAddressDoctorProcessStatus the pAddressDoctorProcessStatus to set
	 */
	public void setAddressDoctorProcessStatus(String pAddressDoctorProcessStatus) {
		this.mAddressDoctorProcessStatus = pAddressDoctorProcessStatus;
	}

	/**
	 * @return the mAddressDoctorResponseVO
	 */
	public TRUAddressDoctorResponseVO getAddressDoctorResponseVO() {
		return mAddressDoctorResponseVO;
	}

	/**
	 * @param pAddressDoctorResponseVO the pAddressDoctorResponseVO to set
	 */
	public void setAddressDoctorResponseVO(
			TRUAddressDoctorResponseVO pAddressDoctorResponseVO) {
		this.mAddressDoctorResponseVO = pAddressDoctorResponseVO;
	}

	/**
	 * @return the mAddressValidated
	 */
	public String getAddressValidated() {
		return mAddressValidated;
	}

	/**
	 * @param pAddressValidated the mAddressValidated to set
	 */
	public void setAddressValidated(String pAddressValidated) {
		this.mAddressValidated = pAddressValidated;
	}	
	
	/**
	 * This method is used to send the order acknowledge email to the customer who has placed order successfully
	 * Mail will be sent via Epsilon
	 */
	public void sendEpsilonOrderAcknowledgementEmail(){
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUCommitOrderFormHandler.ensureAllShipMethods() method");
		}
		Map<String,TRUEpslonBppInfoVO> epslonBppItemMap = new HashMap<String, TRUEpslonBppInfoVO>();
		Map<String,String> shipDeliveryTimeMap = new HashMap<String, String>();
		Map<String,Double> itemEwasteFeeMap = new HashMap<String,Double>();
		double eWastefee = TRUConstants.DOUBLE_ZERO;
		ContactInfo shippingAddress=null;
		String state = TRUConstants.ABCD;
		Address billingAddress = null;
		
		if (getShoppingCart().getLast() != null) {
			final String orderID = getShoppingCart().getLast().getId();
			String email = ((TRUOrderImpl) getShoppingCart().getLast()).getEmail();
			Order order = null;
			try {
				if (StringUtils.isBlank(email)) {
					email = TRUConstants.EMPTY;
					if (isLoggingDebug()) {
						logDebug("Email in order is Empty ::: ");
					}
				}
				order =  getShoppingCart().getLast();
				billingAddress  = ((TRUOrderImpl)order).getBillingAddress();
				if(billingAddress != null){
					shipDeliveryTimeMap.put(TRUCommerceConstants.BILLINGFIRSTNAME,billingAddress.getFirstName());
					shipDeliveryTimeMap.put(TRUCommerceConstants.BILLINGLASTNAME, billingAddress.getLastName());
				}
				ShippingGroup shippingGroup = null;
				TRUBppItemInfo bppItemInfo = null;
				String relationShipId = TRUConstants.EMPTY;
				List<ShippingGroup> shippingGroups = order.getShippingGroups();
				CommerceItem commItem = null;
				if((shippingGroups !=null) && !(shippingGroups.isEmpty())){
				Iterator<ShippingGroup> it = shippingGroups.iterator();
				 while (it.hasNext()) {
		        	shippingGroup = it.next();
		        	String shippingGroupID = shippingGroup.getId();
		        	String deliveryTimeLine = TRUConstants.EMPTY;
		        	String bppSkuID  = TRUConstants.EMPTY;
		        	if(shippingGroup instanceof HardgoodShippingGroup ){
		        		TRUHardgoodShippingGroup hardGoodSG = (TRUHardgoodShippingGroup) shippingGroup;
		        		if(hardGoodSG.getDeliveryTimeFrame() != null){
		        			deliveryTimeLine = hardGoodSG.getDeliveryTimeFrame();
		        		}
		        			shippingAddress=(ContactInfo)hardGoodSG.getShippingAddress();
		        			state=shippingAddress.getState();
		        		}
		        	shipDeliveryTimeMap.put(shippingGroupID, deliveryTimeLine);
					List<CommerceItemRelationship> itemList = shippingGroup.getCommerceItemRelationships();
					Iterator<CommerceItemRelationship> itr = itemList.iterator();
					while (itr.hasNext()) {
						TRUShippingGroupCommerceItemRelationship cItemRel  = (TRUShippingGroupCommerceItemRelationship) itr.next();						
						if(cItemRel != null){
							relationShipId = cItemRel.getId();
							bppItemInfo = cItemRel.getBppItemInfo();
							bppSkuID  = ((TRUOrderTools) getOrderManager().getOrderTools()).getBppSkuId(cItemRel);
							commItem = cItemRel.getCommerceItem();
							if(commItem != null){
								TRUItemPriceInfo itemPriceEwasteInfo = ((TRUItemPriceInfo)commItem.getPriceInfo());
								if(null != itemPriceEwasteInfo){
									RepositoryItem productItem=(RepositoryItem)cItemRel.getCommerceItem().getAuxiliaryData().getProductRef();									
									if(productItem != null && productItem.getPropertyValue(getEwasteSurchargeSkuPropertyName())!=null && !((String)productItem.getPropertyValue(getEwasteSurchargeSkuPropertyName())).equalsIgnoreCase(state))
									{
										eWastefee = ((TRUPricingTools)((TRUPurchaseProcessHelper)getPurchaseProcessHelper()).getProfileTools().getPricingTools()).getEWastePrice(productItem);
										itemEwasteFeeMap.put(commItem.getCatalogRefId(), eWastefee);
									}

									if (isLoggingDebug()) {
										logDebug("Item eWasteFee value for Item::: " + commItem.getCatalogRefId());
										logDebug("Item eWasteFee value ::: " + eWastefee);
									}
									
								}
							}
							if(bppItemInfo != null && StringUtils.isNotBlank(bppItemInfo.getId()))
							{
								TRUEpslonBppInfoVO epslonBppInfoVO = new TRUEpslonBppInfoVO();
								if(StringUtils.isBlank(bppSkuID)){
									bppSkuID = bppItemInfo.getId();
								}
								String bppItemName = bppItemInfo.getRepositoryItem().getItemDisplayName();
								double bppItemPrice = bppItemInfo.getBppPrice();
								epslonBppInfoVO.setBppSkuID(bppSkuID);
								epslonBppInfoVO.setBppItemName(bppItemName);
								epslonBppInfoVO.setBppItemPrice(bppItemPrice);
								epslonBppItemMap.put(relationShipId, epslonBppInfoVO);
						   }
						}
					}
				 }
				
				 boolean isAnonymous = getProfile().isTransient();
				 TRUTaxPriceInfo tPriceInfo = (TRUTaxPriceInfo) order.getTaxPriceInfo();
				 itemEwasteFeeMap.put(TRUCommerceConstants.IS_LAND_TAX, tPriceInfo.getEstimatedIslandTax());
				 itemEwasteFeeMap.put(TRUCommerceConstants.LOCAL_TAX, tPriceInfo.getEstimatedLocalTax());
				getEpslonOrderAcknowledgeSource().sendOrderAcknowledgementEmail(orderID, email,epslonBppItemMap,isAnonymous,shipDeliveryTimeMap,itemEwasteFeeMap);
			}} catch (JMSException e) {
				if (isLoggingError()) {
					logError("JMSException in TRUCommitOrderFormHandler.sendEpsilonOrderAcknowledgementEmail() : ", e);
				}
			}
		} else {
			if (isLoggingDebug()) {
				vlogDebug("Order is Empty");
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUCommitOrderFormHandler.sendEpsilonOrderAcknowledgementEmail() method");
		}	
	}
	
	/**property to hold mEditCardNickName. */
	private String mEditCardNickName;
	
	/** The Saved address. */
	private boolean mNewBillingAddress;
	
	/** The Edit billing address. */
	private boolean mEditBillingAddress;
	
	/** The Edit credit card. */
	private boolean mEditCreditCard;
	
	/** The Saved address. */
	private boolean mNewCreditCard;

	/**
	 * @return the editCardNickName
	 */
	public String getEditCardNickName() {
		return mEditCardNickName;
	}

	/**
	 * @param pEditCardNickName the editCardNickName to set
	 */
	public void setEditCardNickName(String pEditCardNickName) {
		mEditCardNickName = pEditCardNickName;
	}

	/**
	 * @return the newBillingAddress
	 */
	public boolean isNewBillingAddress() {
		return mNewBillingAddress;
	}

	/**
	 * @param pNewBillingAddress the newBillingAddress to set
	 */
	public void setNewBillingAddress(boolean pNewBillingAddress) {
		mNewBillingAddress = pNewBillingAddress;
	}

	/**
	 * @return the editBillingAddress
	 */
	public boolean isEditBillingAddress() {
		return mEditBillingAddress;
	}

	/**
	 * @param pEditBillingAddress the editBillingAddress to set
	 */
	public void setEditBillingAddress(boolean pEditBillingAddress) {
		mEditBillingAddress = pEditBillingAddress;
	}

	/**
	 * @return the editCreditCard
	 */
	public boolean isEditCreditCard() {
		return mEditCreditCard;
	}

	/**
	 * @param pEditCreditCard the editCreditCard to set
	 */
	public void setEditCreditCard(boolean pEditCreditCard) {
		mEditCreditCard = pEditCreditCard;
	}

	/**
	 * @return the newCreditCard
	 */
	public boolean isNewCreditCard() {
		return mNewCreditCard;
	}

	/**
	 * @param pNewCreditCard the newCreditCard to set
	 */
	public void setNewCreditCard(boolean pNewCreditCard) {
		mNewCreditCard = pNewCreditCard;
	}

	/**
	 * @return the mApplePayForm
	 */
	public TRUApplePayForm getApplePayForm() {
		return mApplePayForm;
	}

	/**
	 * Sets the apple pay form.
	 *
	 * @param pApplePayForm the new apple pay form
	 */
	public void setApplePayForm(TRUApplePayForm pApplePayForm) {
		this.mApplePayForm = pApplePayForm;
	}
	
	/** The m scheme detection util. */
	private TRUSchemeDetectionUtil mSchemeDetectionUtil;

	/**
	 * Gets the scheme detection util.
	 *
	 * @return the scheme detection util
	 */
	public TRUSchemeDetectionUtil getSchemeDetectionUtil() {
		return mSchemeDetectionUtil;
	}

	/**
	 * Sets the scheme detection util.
	 *
	 * @param pSchemeDetectionUtil the new scheme detection util
	 */
	public void setSchemeDetectionUtil(TRUSchemeDetectionUtil pSchemeDetectionUtil) {
		this.mSchemeDetectionUtil = pSchemeDetectionUtil;
	}
	
	
}
