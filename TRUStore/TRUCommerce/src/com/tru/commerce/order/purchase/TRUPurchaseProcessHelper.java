package com.tru.commerce.order.purchase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.Relationship;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.purchase.AddCommerceItemInfo;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.util.PipelineErrorHandler;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.ResourceUtils;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.dynamo.LangLicense;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUChannelInStorePickupShippingGroup;
import com.tru.commerce.order.TRUCommerceItemImpl;
import com.tru.commerce.order.TRUCommerceItemMetaInfo;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardInfo;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardStatus;
import com.tru.commerce.payment.processor.TRUCreditCardProcessor;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConstants;
import com.tru.common.TRUUserSession;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * The Class TRUPurchaseProcessHelper.
 */
public class TRUPurchaseProcessHelper extends TRUBasePurchaseProcessHelper {
	
	private static ResourceBundle sResourceBundle = LayeredResourceBundle.getBundle(TRUConstants.PURCHASE_PROCESS_RESOURCES, LangLicense.getLicensedDefault());

	/**
	 * Checks if is order having only instore pickup items.
	 * 
	 * @param pOrder
	 *            the order
	 * @return true, if is order having only instore pickup items
	 */
	@SuppressWarnings("unchecked")
	public boolean isOrderHavingOnlyInstorePickupItems(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUPurchaseProcessHelper.isOrderHavingOnlyInstorePickupItems method");
			vlogDebug("INPUT PARAMS OF isOrderHavingOnlyInstorePickupItems method pOrder: {0} ", pOrder);
		}
		boolean isOrderHavingOnlyInstorePickup = true;
		if (pOrder != null) {
			final List<Relationship> listOfRel = pOrder.getRelationships();
			ShippingGroup shipGrp = null;
			for (Relationship rel : listOfRel) {
				if (rel instanceof TRUShippingGroupCommerceItemRelationship) {
					shipGrp = ((TRUShippingGroupCommerceItemRelationship) rel).getShippingGroup();
					if (shipGrp instanceof TRUHardgoodShippingGroup) {
						isOrderHavingOnlyInstorePickup = false;
						break;
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug(
					"End:: TRUPurchaseProcessHelper.isOrderHavingOnlyInstorePickupItems returns isOrderHavingOnlyInstorePickup: {0}",
					isOrderHavingOnlyInstorePickup);
		}
		return isOrderHavingOnlyInstorePickup;
	}
	
	/**
	 * Checks if is order having only gift pickup items.
	 * 
	 * @param pOrder
	 *            the order
	 * @return true, if is order having only instore pickup items
	 */
	@SuppressWarnings("unchecked")
	public boolean isOrderHavingOnlyGiftItem(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUPurchaseProcessHelper.isOrderHavingOnlyGiftItem method");
			vlogDebug("INPUT PARAMS OF isOrderHavingOnlyGiftItem method pOrder: {0} ", pOrder);
		}
		boolean isOrderHavingOnlyGiftItem = false;
		if (pOrder != null) {
			List<CommerceItem> commerceItems = pOrder.getCommerceItems();
			for (CommerceItem commerceItem : commerceItems) {
				if (commerceItem instanceof TRUCommerceItemImpl) {
					List<TRUShippingGroupCommerceItemRelationship> listOfRelationships = (List<TRUShippingGroupCommerceItemRelationship>) commerceItem
							.getShippingGroupRelationships();
					for (TRUShippingGroupCommerceItemRelationship shipGpComRel : listOfRelationships) {
						if (shipGpComRel.getIsGiftItem()) {
							isOrderHavingOnlyGiftItem = true;
							break;
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End:: TRUPurchaseProcessHelper.isOrderHavingOnlyGiftItem returns isOrderHavingOnlyGiftItem: {0}",isOrderHavingOnlyGiftItem);
		}
		return isOrderHavingOnlyGiftItem;
	}


	/**
	 * Checks if is order having only donation items.
	 * 
	 * @param pOrder
	 *            the order
	 * @return true, if is order having only donation items
	 */
	@SuppressWarnings("unchecked")
	public boolean isOrderHavingOnlyDonationItems(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUPurchaseProcessHelper.isOrderHavingOnlyDonationItems method");
			vlogDebug("INPUT PARAMS OF isOrderHavingOnlyDonationItems method pOrder: {0} ", pOrder);
		}
		boolean isOrderHavingOnlyDonationItems = true;
		if (pOrder != null) {
			List<CommerceItem> commerceItems = pOrder.getCommerceItems();
			for (CommerceItem commerceItem : commerceItems) {
				if (!(commerceItem instanceof TRUDonationCommerceItem)) {
					isOrderHavingOnlyDonationItems = false;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug(
					"End:: TRUPurchaseProcessHelper.isOrderHavingOnlyInstorePickupItems returns isOrderHavingOnlyDonationItems: {0}",
					isOrderHavingOnlyDonationItems);
		}
		return isOrderHavingOnlyDonationItems;
	}


	/**
	 * This method is used to Remove the items from the current order for which
	 * sku or product is soft deleted from the catalog.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pRemovalItemList
	 *            the removal item list
	 * @param pProfileId
	 *            the profileId
	 * @return the sets the
	 * @throws CommerceException
	 *             the commerce exception
	 * @throws RunProcessException
	 *             the run process exception
	 * @throws RepositoryException
	 *             - throws when
	 */
	@SuppressWarnings("unchecked")
	public Set<String> removeItemsFromOrder(Order pOrder, Set<String> pRemovalItemList, String pProfileId)
			throws CommerceException, RunProcessException, RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUPurchaseProcessHelper::@method::addItemsToOrderFromOutOfStockList() : START -->pOrder,pRemovalItemList,pProfile--> {0} {1} {2}",
					pOrder, pRemovalItemList, pProfileId);
		}
		final Set<String> deletedSkuList = new HashSet<String>();
		if ((pRemovalItemList != null) && !pRemovalItemList.isEmpty()) {
			final Iterator<String> items = pRemovalItemList.iterator();
			CommerceItem itemsToRemove = null;
			while (items.hasNext()) {
				itemsToRemove = pOrder.getCommerceItem(items.next());
				getGiftingProcessHelper().updateGiftWrap(pOrder, itemsToRemove);
				deletedSkuList.add(itemsToRemove.getCatalogRefId());
				List<ShippingGroupCommerceItemRelationship> rels = itemsToRemove.getShippingGroupRelationships();
				final TransactionDemarcation td = new TransactionDemarcation();
				try {
					td.begin(getTransactionManager());
					for (ShippingGroupCommerceItemRelationship rel : rels) {
						getOrderTools().removeBPPItemInfo(pOrder, rel.getId());
					}
				} catch (TransactionDemarcationException e) {
					if (isLoggingError()) {
						logError(
								"TransactionDemarcationException while removing coupon @Class::TRUPurchaseProcessHelper::@method::removeItemsFromOrder() ",
								e);
					}
					throw new CommerceException(e);
				} catch (RepositoryException e) {
					if (isLoggingError()) {
						logError(
								"RepositoryException while removing coupon @Class::TRUPurchaseProcessHelper::@method::removeItemsFromOrder() ",
								e);
					}
					throw new CommerceException(e);
				} finally {
					try {
						td.end();
					} catch (TransactionDemarcationException e) {
						if (isLoggingError()) {
							logError(
									"TransactionDemarcationException while removing coupon in finally block @Class::TRUPurchaseProcessHelper::@method::removeItemsFromOrder() ",
									e);
						}
						throw new CommerceException(e);
					}
				}
				deleteItem(itemsToRemove.getId(), pOrder);
				postDeleteItem(pOrder, itemsToRemove, itemsToRemove.getQuantity(), null);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUPurchaseProcessHelper::@method::addItemsToOrderFromOutOfStockList() : START -->deletedSkuList--> {0}",
					deletedSkuList);
		}
		return deletedSkuList;
	}

	/**
	 * This method is used to Remove the items from the current order for which sku or product is soft deleted from the catalog.
	 *
	 * @param pOrder the order
	 * @param pOutofStockSGItemList the outof stock sg item list
	 * @throws CommerceException the commerce exception
	 * @throws RunProcessException the run process exception
	 * @throws RepositoryException the repository exception
	 */
	public void removeSGRelItemsFromOrder(Order pOrder, Set<String> pOutofStockSGItemList)
			throws CommerceException, RunProcessException, RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::removeSGRelItemsFromOrder() : START -->pOrder, pOutofStockSGItemList--> {0} {1}",
					pOrder, pOutofStockSGItemList);
		}
		if (pOrder == null || pOutofStockSGItemList == null || pOutofStockSGItemList.isEmpty()) {
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUPurchaseProcessHelper::@method::removeSGRelItemsFromOrder() : END's with null check ");
			}
			return;
		}
		Relationship relationship = null;
		//CommerceItem giftWrapCommerceItem = null;
		TRUShippingGroupCommerceItemRelationship sgCiRel = null;
		//List<RepositoryItem> getGiftItemInfo = null;
		//long giftWrapQty = TRUConstants.INT_ZERO;
		CommerceItem ci = null;
		ShippingGroup sg = null; 
		for (String relationshipId : pOutofStockSGItemList) {
			relationship = pOrder.getRelationship(relationshipId);
			if (!(relationship instanceof ShippingGroupCommerceItemRelationship)) {
				continue;
			}
			sgCiRel = (TRUShippingGroupCommerceItemRelationship) relationship;
			// TO Remove the Gift Wrap Relationships
			if (sgCiRel.getIsGiftItem()) {
				final TRUCommercePropertyManager commercePropertyManager = getOrderTools().getCommercePropertyManager();
				final List<TRUCommerceItemMetaInfo> metaList = ((TRUShippingGroupCommerceItemRelationship) sgCiRel).getMetaInfo();
				for (TRUCommerceItemMetaInfo metaInfo : metaList) {
					if (metaInfo != null && metaInfo.getPropertyValue(commercePropertyManager.getGiftWrapItemPropertyName()) != null) {
						final RepositoryItem gwCommerceItem = (RepositoryItem) metaInfo.getPropertyValue(commercePropertyManager
								.getGiftWrapItemPropertyName());
						final TRUGiftWrapCommerceItem truGiftWrapCommerceItem = (TRUGiftWrapCommerceItem) pOrder.getCommerceItem(gwCommerceItem
								.getRepositoryId());
						final String giftWrapCommId = truGiftWrapCommerceItem.getId();
						if (!StringUtils.isBlank(giftWrapCommId)) {
							List<RepositoryItem> giftInfos = ((TRUShippingGroupCommerceItemRelationship) sgCiRel).getGiftItemInfo();
							if(giftInfos != null && !giftInfos.isEmpty()) {
								Map<String, Long> wrapInfo = new HashMap<String, Long>();
								for (RepositoryItem giftItem : giftInfos) {
									String wrapCommId = (String) giftItem.getPropertyValue(commercePropertyManager.getGiftWrapCommItemId());
									if (StringUtils.isNotBlank(giftWrapCommId) && wrapCommId.equals(giftWrapCommId)) {
										long wrapCommerceItemQty = (long) giftItem.getPropertyValue(commercePropertyManager.getGiftWrapQuantity());
										String getGiftWrapCatalogRefId = (String) giftItem.getPropertyValue(commercePropertyManager.getGiftWrapCatalogRefId());
										wrapInfo.put(getGiftWrapCatalogRefId, wrapCommerceItemQty);
									}
								}
								for (String getGiftWrapCatalogRefId : wrapInfo.keySet()) {
									getGiftingProcessHelper().updateGiftWrap(pOrder, sgCiRel.getId(), getGiftWrapCatalogRefId,
											wrapInfo.get(getGiftWrapCatalogRefId), false);
								}
							}
						}
					}
				}
			}
			// TO Remove the BPP Relationship
			final TransactionDemarcation td = new TransactionDemarcation();
			try {
				td.begin(getTransactionManager());
				getOrderTools().removeBPPItemInfo(pOrder, sgCiRel.getId());
			} catch (TransactionDemarcationException e) {
				if (isLoggingError()) {
					logError(
							"TransactionDemarcationException @Class::TRUPurchaseProcessHelper::@method::removeSGRelItemsFromOrder() ",
							e);
				}
				throw new CommerceException(e);
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError(
							"RepositoryException @Class::TRUPurchaseProcessHelper::@method::removeSGRelItemsFromOrder() ",
							e);
				}
				throw new CommerceException(e);
			} finally {
				try {
					td.end();
				} catch (TransactionDemarcationException e) {
					if (isLoggingError()) {
						logError(
								"TransactionDemarcationException @Class::TRUPurchaseProcessHelper::@method::removeItemsFromOrder() ",
								e);
					}
					throw new CommerceException(e);
				}
			}
			// TO Remove the Shipping Group Commerce Item Relationship
			ci = sgCiRel.getCommerceItem();
			if (sgCiRel.getQuantity() >= ci.getQuantity()) {
				getCommerceItemManager().removeAllRelationshipsFromCommerceItem(pOrder, ci.getId());
				getCommerceItemManager().removeItemFromOrder(pOrder, ci.getId());
				sg = sgCiRel.getShippingGroup();
				if ((sg instanceof InStorePickupShippingGroup) && (sg.getCommerceItemRelationshipCount() == 0)) {
					pOrder.removeShippingGroup(sg.getId());
				}
			} else {
				ci.setQuantity(ci.getQuantity() - sgCiRel.getQuantity());
				getCommerceItemManager().removeItemQuantityFromShippingGroup(pOrder, ci.getId(),
						sgCiRel.getShippingGroup().getId(), sgCiRel.getQuantity());
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::removeSGRelItemsFromOrder() : END ");
		}
	}

	/**
	 * Gets the shipping group.
	 *
	 * @param pOrder the order
	 * @return the shipping group
	 * @throws CommerceException the commerce exception
	 */
	public TRUHardgoodShippingGroup getShippingGroup(Order pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUPurchaseProcessHelper.getShippingGroup method");
		}
		if (pOrder.getShippingGroupCount() > 0) {
			getShippingGroupManager().removeAllShippingGroupsFromOrder(pOrder);
		}
		final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupManager()
				.createShippingGroup();
		if (isLoggingDebug()) {
			vlogDebug("End of getShippingGroup method and returns hardgoodShippingGroup: {0}", hardgoodShippingGroup);
		}
		return hardgoodShippingGroup;
	}

	/**
	 * checkForDiscontinuedProducts.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pSessionComponent
	 * 			  the session component
	 * @throws CommerceException
	 *             the commerce exception
	 * @throws RunProcessException
	 *             the run process exception
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void checkForDiscontinuedProducts(Order pOrder, TRUUserSession pSessionComponent) throws CommerceException, RunProcessException, RepositoryException,
			ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::checkForDiscontinuedProducts() : BEGIN ");
		}
		((TRUOrderImpl) pOrder).setValidationFailed(false);
		Set<String> removalItemList = null;
		Set<String> deletedSkuList = null;
		removalItemList = getShoppingCartUtils().getTRUCartModifierHelper().getRemovalItemList(pOrder);
		if (null != removalItemList && !removalItemList.isEmpty()) {
			String profileId = pOrder.getProfileId();
			deletedSkuList = removeItemsFromOrder(pOrder, removalItemList, profileId);
		}
		pSessionComponent.setRemovalItemList(removalItemList);
		pSessionComponent.setDeletedSkuList(deletedSkuList);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::checkForDiscontinuedProducts() : END ");
		}
	}

	/**
	 * validateInventoryFor Checkout.
	 *
	 * @param pOrder            the order
	 * @param pChainId            - chainId
	 * @param pParaemetrs            - paraemetrs
	 * @throws CommerceException             the commerce exception
	 * @throws RunProcessException             the run process exception
	 * @throws RepositoryException             the repository exception
	 * @throws ServletException             the servlet exception
	 * @throws IOException             Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("rawtypes")
	public void validateInventoryForCheckout(Order pOrder, String pChainId, Map pParaemetrs) throws CommerceException,
			RunProcessException, RepositoryException, ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::validateInventoryForCheckout() : BEGIN ");
		}
		
		Set<String> removalItemList = null;
		Set<String> outOfStockShipRelItemList = null;
		List<String> qtyAdjustedList = null;
		String profileId = pOrder.getProfileId();
		TRUUserSession sessionComponent = (TRUUserSession) pParaemetrs.get(TRUPipeLineConstants.SESSION_COMPONENT);
		if (null != sessionComponent.getRemovalItemList()) {
			removalItemList = (Set<String>) sessionComponent.getRemovalItemList();
		}					
		if ((TRUPipeLineConstants.PROCESS_ORDER.equalsIgnoreCase(pChainId))  || (TRUPipeLineConstants.MOVE_TO_PURCHASE_INFO.equalsIgnoreCase(pChainId))) {
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUPurchaseProcessHelper::@method::pipeline chain id is processOrder : BEGIN ");
			}
			outOfStockShipRelItemList = getShoppingCartUtils().getOutOfStockSGRelItemList(removalItemList, pOrder, null);
			if (null != outOfStockShipRelItemList && !outOfStockShipRelItemList.isEmpty()) {
				if (isLoggingDebug()) {
					vlogDebug("@Class::TRUPurchaseProcessHelper::@method::outOfStockList list is not empty block:BEGIN");
				}		
				removeSGRelItemsFromOrder(pOrder, outOfStockShipRelItemList);				
				if (isLoggingDebug()) {
					vlogDebug("@Class::TRUPurchaseProcessHelper::@method::outOfStockList list is not empty block::ENDS");
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUPurchaseProcessHelper::@method::pipeline chain id is processOrder : ENDS ");
			}
		} else {
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUPurchaseProcessHelper::@method::pipeline chain id is not  processOrder and it's a cookieLocationId : BEGIN ");
			}
			String cookieLocation = null;
			if (null != pParaemetrs.get(TRUPipeLineConstants.COOKIE_LOCATION_ID)) {
				cookieLocation = (String) pParaemetrs.get(TRUPipeLineConstants.COOKIE_LOCATION_ID);
			}
			outOfStockShipRelItemList = getShoppingCartUtils().getOutOfStockItemList(removalItemList, pOrder, cookieLocation);
			if (null != outOfStockShipRelItemList && !outOfStockShipRelItemList.isEmpty()) {
				removeItemsFromOrder(pOrder, outOfStockShipRelItemList, profileId);
			}
		}
		qtyAdjustedList = ((TRUOrderManager) getOrderManager()).autoCorrectQuantity(pOrder, false);
		sessionComponent.setRemovalItemList(null);
		sessionComponent.setQtyAdjustedSkuList(qtyAdjustedList);

		if ((removalItemList != null && !removalItemList.isEmpty()) || (outOfStockShipRelItemList != null && !outOfStockShipRelItemList.isEmpty())
				|| (qtyAdjustedList != null && !qtyAdjustedList.isEmpty())) {
			((TRUOrderImpl) pOrder).setValidationFailed(true);
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::validateInventoryForCheckout() : END ");
		}
	}

	/**
	 * Gets the matched ispu shipping group.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pTruAddCommerceItemInfo
	 *            the tru add commerce item info
	 * @return the matched ispu shipping group
	 */
	@SuppressWarnings("unchecked")
	public ShippingGroup getMatchedISPUShippingGroup(Order pOrder, TRUAddCommerceItemInfo pTruAddCommerceItemInfo) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::getMatchedISPUShippingGroup() : BEGIN");
		}
		ShippingGroup matchedShippingGroup = null;
		final List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		if (null != shippingGroups && !shippingGroups.isEmpty()) {
			for (ShippingGroup shippingGroup : shippingGroups) {
				if (StringUtils.isBlank(pTruAddCommerceItemInfo.getChannelId()) &&
						 TRUCommerceConstants.INSTORE_PICKUP_SG.equalsIgnoreCase(shippingGroup.getShippingGroupClassType()) &&
						 ((TRUInStorePickupShippingGroup) shippingGroup).getLocationId().equals(
								pTruAddCommerceItemInfo.getLocationId())) {
					if (isLoggingDebug()) {
						vlogDebug(
								"@Class::TRUPurchaseProcessHelper::@method::getMatchedISPUShippingGroup() :TRUInStorePickupShippingGroup is already exsist",
								shippingGroup.getId());
					}
					matchedShippingGroup = shippingGroup;
					break;
				}

				if (shippingGroup instanceof TRUChannelInStorePickupShippingGroup &&
						 ((TRUChannelInStorePickupShippingGroup) shippingGroup).getLocationId().equals(
								pTruAddCommerceItemInfo.getLocationId()) &&
						 StringUtils.isNotBlank(((TRUChannelInStorePickupShippingGroup) shippingGroup).getChannelId()) &&
						 ((TRUChannelInStorePickupShippingGroup) shippingGroup).getChannelId().equals(
								pTruAddCommerceItemInfo.getChannelId())) {
					if (isLoggingDebug()) {
						vlogDebug(
								"@Class::TRUPurchaseProcessHelper::@method::getMatchedISPUShippingGroup() :TRUChannelInStorePickupShippingGroup is already exsist",
								shippingGroup.getId());
					}
					matchedShippingGroup = shippingGroup;
					break;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::getMatchedISPUShippingGroup() : BEGIN");
		}
		return matchedShippingGroup;
	}


	/**
	 * This method invokes the Zero Authorization for the credit card
	 * verification.
	 * 
	 * @param pAmount
	 *            amount
	 * @param pSiteId
	 *            siteId
	 * @param pCreditCardMap
	 *            creditCardMap
	 * @param pAddressFields
	 *            addressFields
	 * @return TRUCreditCardStatus - TRUCreditCardStatus
	 * @param pMerchantRefNumber
	 *            - merchantRefNumber
	 */
	public TRUCreditCardStatus verifyZeroAuthorization(Double pAmount, String pSiteId, Map<String, String> pCreditCardMap,
			ContactInfo pAddressFields, String pMerchantRefNumber) {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUPurchaseProcessHelper/verifyZeroAuthorization method");
		}

		if (pSiteId != null && pCreditCardMap != null && pAddressFields != null) {

			TRUCreditCardInfo cardInfo = new TRUCreditCardInfo();

			cardInfo.setMerchantRefNumber(pMerchantRefNumber);

			//cardInfo.setCurrencyCode(getConfiguration().getCurrencyCodesString().get(pSiteId));

			TRUPropertyManager propertyManager = ((TRUOrderManager) getOrderManager()).getPropertyManager();

			cardInfo.setCardHolderName(pCreditCardMap.get(propertyManager.getNameOnCardPropertyName()));

			cardInfo.setCreditCardNumber(pCreditCardMap.get(propertyManager.getCreditCardTokenPropertyName()));

			cardInfo.setTokenType(getConfiguration().getTokenType().get(pSiteId));
			
			cardInfo.setSiteId(pSiteId);

			String creditCardType = null;

			if (!StringUtils.isBlank(pCreditCardMap.get(propertyManager.getCreditCardTypePropertyName()))) {
				creditCardType = pCreditCardMap.get(propertyManager.getCreditCardTypePropertyName());
			} else {
				creditCardType = ((TRUProfileTools) ((TRUOrderTools) ((TRUOrderManager) getOrderManager()).getOrderTools())
						.getProfileTools()).getCreditCardTypeFromRepository(
						pCreditCardMap.get(propertyManager.getCreditCardNumberPropertyName()),
						pCreditCardMap.get(TRUConstants.CARD_LENGTH));
			}

			cardInfo.setCreditCardType(getConfiguration().getCreditCardTypeNamesForIntegration().get(creditCardType));

			cardInfo.setExpirationYear(pCreditCardMap.get(propertyManager.getCreditCardExpirationYearPropertyName()));
			
			cardInfo.setExpirationMonth(pCreditCardMap.get(propertyManager.getCreditCardExpirationMonthPropertyName()));

			cardInfo.setCvv(pCreditCardMap.get(propertyManager.getCreditCardVerificationNumberPropertyName()));

			cardInfo.setBillingAddress(pAddressFields);

			/*if (getConfiguration().getCreditCardTypeNamesForIntegration().get(creditCardType)
					.equalsIgnoreCase(TRUCheckoutConstants.RUS_COB_MASTER_CARD)
					|| getConfiguration().getCreditCardTypeNamesForIntegration().get(creditCardType)
							.equalsIgnoreCase(TRUCheckoutConstants.RUS_PRIVATE_LABEL_CARD)) {
				cardInfo.setAmount(TRUConstants._1);
			} else {
				cardInfo.setAmount(TRUConstants._0);
			}*/

			return ((TRUCreditCardProcessor) ((TRUOrderManager) getOrderManager()).getPaymentManager().getCreditCardProcessor())
					.zeroAuthorization(cardInfo);
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUPurchaseProcessHelper/verifyZeroAuthorization method");
		}
		return null;
	}

	/**
	 * This method invokes the pre authorization for promo APR flow.
	 *
	 * @param pSiteId            - String
	 * @param pCreditCardMap            - creditCardMap
	 * @param pMerchantRefNumber            - String
	 * @param pIsSavedCard            - boolean
	 * @param pOrder the order
	 * @param pAdddress - Adddress
	 * @return TRUCreditCardStatus - TRUCreditCardStatus
	 */
	public TRUCreditCardStatus verifyPreAuthorization(String pSiteId, Map<String, String> pCreditCardMap,
			String pMerchantRefNumber, boolean pIsSavedCard, TRUOrderImpl pOrder, Address pAdddress) {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUPurchaseProcessHelper/verifyPreAuth method");
		}

		if (pSiteId != null && pCreditCardMap != null) {
			TRUCreditCardInfo cardInfo = new TRUCreditCardInfo();

			TRUPropertyManager propertyManager = ((TRUOrderManager) getOrderManager()).getPropertyManager();
			cardInfo.setMerchantRefNumber(pMerchantRefNumber);
			cardInfo.setCurrencyCode(getConfiguration().getCurrencyCodesString().get(pSiteId));

			//cardInfo.setAmount(TRUConstants._1);

			cardInfo.setTokenType(getConfiguration().getTokenType().get(pSiteId));

			cardInfo.setCreditCardType(pCreditCardMap.get(propertyManager.getCreditCardTypePropertyName()));
			//cardInfo.setCreditCardType(TRUCheckoutConstants.PL_TRU);

			cardInfo.setCardHolderName(pCreditCardMap.get(propertyManager.getNameOnCardPropertyName()));

			cardInfo.setCreditCardNumber(pCreditCardMap.get(propertyManager.getCreditCardTokenPropertyName()));
			cardInfo.setBillingAddress(pAdddress);

			StringBuilder expirationDate = new StringBuilder().append(
					pCreditCardMap.get(propertyManager.getCreditCardExpirationMonthPropertyName())).append(
					pCreditCardMap.get(propertyManager.getCreditCardExpirationYearPropertyName()).substring(
							TRUCheckoutConstants.INDEX_TWO, TRUCheckoutConstants.INDEX_FOUR));

			cardInfo.setExpirationDate(expirationDate.toString());
			cardInfo.setExpirationYear(pCreditCardMap.get(propertyManager.getCreditCardExpirationYearPropertyName()));
			cardInfo.setExpirationMonth(pCreditCardMap.get(propertyManager.getCreditCardExpirationMonthPropertyName()));

			if (pIsSavedCard) {
				PaymentGroup destCreditCard = ((TRUOrderManager) getOrderManager()).getCreditCard(pOrder);
				if(destCreditCard != null && StringUtils.isNotBlank(((TRUCreditCard)destCreditCard).getCardVerificationNumber())) {
					cardInfo.setCvv(((TRUCreditCard)destCreditCard).getCardVerificationNumber());
				} else {
					cardInfo.setCvv(TRUCheckoutConstants.EMPTY_STRING);
				}
			} else {
				cardInfo.setCvv(pCreditCardMap.get(propertyManager.getCreditCardVerificationNumberPropertyName()));
			}

			return ((TRUCreditCardProcessor) ((TRUOrderManager) getOrderManager()).getPaymentManager().getCreditCardProcessor())
					.preAuthorization(cardInfo);
		}

		if (isLoggingDebug()) {
			vlogDebug("End of TRUPurchaseProcessHelper/verifyPreAuth method");
		}
		return null;
	}

	/**
	 * generate Unique Nickname.
	 *
	 * @param pFirstName            pFirstName
	 * @param pLastName            pLastName
	 * @return nickName String
	 */
	public String generateUniqueNickname(String pFirstName, String pLastName) {
		String nickName = pFirstName.trim() + TRUCheckoutConstants.STRING_UNDERSCORE + pLastName.trim();
		
		if(nickName.length()>getConfiguration().getBillingNickNameLength()){
			nickName=nickName.substring(0, getConfiguration().getBillingNickNameLength());
		}
		if(isLoggingDebug()){
			vlogDebug("Nickname  : {0} and Length {1}", nickName,nickName.length());
		}
		return nickName;
	}

	/**
	 * Checks if is SOS order.
	 *
	 * @param pRequest            : DynamoHttpServletRequest Object
	 * @return boolean : True if Request contains isSOS cookie with value as "true"
	 */
	public boolean isSOSOrder(DynamoHttpServletRequest pRequest) {
		if (isLoggingDebug()) {
			vlogDebug("Enter intos [Class: TRUPurchaseProcessHelper  method: isSOSOrder]");
		}
		boolean isSOSOrder = Boolean.FALSE;
		if (pRequest == null) {
			return isSOSOrder;
		}
		String cookieParameter = pRequest.getCookieParameter(TRUConstants.IS_SOS);
		if (StringUtils.isNotBlank(cookieParameter) && cookieParameter.equalsIgnoreCase(TRUConstants.IS_SOS_VALUE)) {
			isSOSOrder = Boolean.TRUE;
		}
		if (isLoggingDebug()) {
			vlogDebug("Exit from [Class: TRUPurchaseProcessHelper  method: isSOSOrder]");
			vlogDebug("Order is SOS order : {0}", isSOSOrder);
		}
		return isSOSOrder;
	}

	/**
	 * This method is override to return first hardgood shipping group in the order.
	 *
	 * @param pOrder            the order
	 * @return the first shipping group
	 */

	public ShippingGroup getFirstShippingGroup(Order pOrder) {
		ShippingGroup sg = null;
		try {
			sg = getFirstHardgoodShippingGroup(pOrder);
		} catch (CommerceException ce) {
			if (isLoggingError()) {
				logError("CommerceException in getFirstShippingGroup() @@ class : TRUPurchaseProcessHelper :", ce);
			}
		}
		return sg;
	}

	/**
	 * Method to get the SOS emp name and sos emp number.
	 * 
	 * @param pRequest : DynamoHttpServletRequest Object
	 * @return String : Employee name with Employee Number for SOS orders.
	 */
	public String getEnteredByValue(DynamoHttpServletRequest pRequest) {
		if (isLoggingDebug()) {
			vlogDebug("Enter into [Class: TRUPurchaseProcessHelper  method: getEnteredByValue]");
		}
		String enteredBy = null;
		if (pRequest == null) {
			return enteredBy;
		}
		String sosEmpName = pRequest.getCookieParameter(TRUConstants.SOS_EMP_NAME);
		String sosEmpNumber = pRequest.getCookieParameter(TRUConstants.SOS_EMP_NUMBER);
		StringBuffer buffer = new StringBuffer();
		if(StringUtils.isNotBlank(sosEmpName)){
			buffer.append(sosEmpName);
			buffer.append(TRUConstants.HYPHEN);
		}
		if(StringUtils.isNotBlank(sosEmpNumber)){
			buffer.append(sosEmpNumber);
		}
		enteredBy = buffer.toString();
		if (isLoggingDebug()) {
			vlogDebug("Exiting from [Class: TRUPurchaseProcessHelper  method: getEnteredByValue]");
			vlogDebug("SOS Emp name and Number : {0}", enteredBy);
		}
		return enteredBy;
	}
	
	/**
	 * Method to get the SOS Enter location(Store Number).
	 * 
	 * @param pRequest : DynamoHttpServletRequest Object
	 * @return String : location id.
	 */
	public String getEnteredLocation(DynamoHttpServletRequest pRequest) {
		if (isLoggingDebug()) {
			vlogDebug("Enter into [Class: TRUPurchaseProcessHelper  method: getEnteredLocation]");
		}
		String enteredLocation = null;
		if (pRequest == null) {
			return enteredLocation;
		}
		String sosStoreNumber = pRequest.getCookieParameter(TRUConstants.SOS_STORE_NUMBER);
		if (isLoggingDebug()) {
			vlogDebug("Exiting from [Class: TRUPurchaseProcessHelper  method: getEnteredLocation]");
			vlogDebug("SOS Store Number : {0}", sosStoreNumber);
		}
		return sosStoreNumber;
	}
	
	/**
	 * *.
	 *
	 * @param pOrder the order
	 * @param pShippingGroup the shipping group
	 * @param pProfile the profile
	 * @param pItemInfos the item infos
	 * @param pUserLocale the user locale
	 * @param pCatalogKey the catalog key
	 * @param pUserPricingModels the user pricing models
	 * @param pErrorHandler the error handler
	 * @param pExtraParameters the extra parameters
	 * @return the list
	 * @throws CommerceException the commerce exception
	 */
	public List addItemToApplePayOrder(TRUOrderImpl pOrder, ShippingGroup pShippingGroup, 
			RepositoryItem pProfile, AddCommerceItemInfo[] pItemInfos, Locale pUserLocale, 
			String pCatalogKey, PricingModelHolder pUserPricingModels, PipelineErrorHandler pErrorHandler, 
			Map pExtraParameters) throws CommerceException {

		List itemsAdded = new ArrayList(pItemInfos.length);
		final TransactionDemarcation td = new TransactionDemarcation();
		try {
			td.begin(getTransactionManager());
			synchronized (pOrder) {
				if (isLoggingDebug()) {
					logDebug("Synchronized on order");
				}
				for (int index = 0; index < pItemInfos.length; ++index) {
					AddCommerceItemInfo itemInfo = pItemInfos[index];
					if (itemInfo.getQuantity() > TRUConstants.LONG_ZERO) {
						CommerceItem ci = super.addItemToOrder(itemInfo, pCatalogKey, pOrder, pShippingGroup, pProfile);
						itemsAdded.add(ci);
						if (isLoggingDebug()) {
							logDebug("Done adding one item to order.");
						}

					}
				}
				runProcessRepriceOrder(getAddItemToOrderPricingOp(), pOrder, pUserPricingModels, pUserLocale, pProfile, pExtraParameters, pErrorHandler);

				if (isTransactionMarkedAsRollBack()) {
					Object[] args = { pOrder.getId() };
					throw new CommerceException(ResourceUtils.getMsgResource(TRUConstants.ERROR_REPRICING_ORDER_AFTER_ADD_ITEM, TRUConstants.PURCHASE_PROCESS_RESOURCES, sResourceBundle, args));

				}
				getOrderManager().updateOrder(pOrder);
			}
		}catch (TransactionDemarcationException e) {
			if (isLoggingError()) {
				logError(
						"TransactionDemarcationException @Class::TRUPurchaseProcessHelper::@method::addItemToApplePayOrder() ",
						e);
			}
			throw new CommerceException(e);

		} catch (RunProcessException e) {
			if (isLoggingError()) {
				logError(
						"TransactionDemarcationException @Class::TRUPurchaseProcessHelper::@method::addItemToApplePayOrder() ",
						e);
			}
			throw new CommerceException(e);

		} finally {
			try {
				td.end();
			} catch (TransactionDemarcationException e) {
				if (isLoggingError()) {
					logError(
							"TransactionDemarcationException @Class::TRUPurchaseProcessHelper::@method::addItemToApplePayOrder() ",
							e);
				}
				throw new CommerceException(e);
			}
		}


		return itemsAdded;
	}

	/**
	 * Removes the zero price items.
	 *
	 * @param pOrder - Order
	 * @throws RepositoryException the repository exception
	 * @throws RunProcessException the run process exception
	 * @throws CommerceException the commerce exception
	 */
	@SuppressWarnings("unused")
	public void removeZeroPriceItems(Order pOrder) throws RepositoryException, RunProcessException, CommerceException{
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::checkForDiscontinuedProducts() : BEGIN ");
		}
		Set<String> removalItemList = null;
		Set<String> deletedSkuList = null;
		removalItemList = getShoppingCartUtils().getTRUCartModifierHelper().removeZeroPricelItems(pOrder);
		if (null != removalItemList && !removalItemList.isEmpty()) {
			String profileId = pOrder.getProfileId();
			deletedSkuList = removeItemsFromOrder(pOrder, removalItemList, profileId);
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::checkForDiscontinuedProducts() : END ");
		}
	}


}
