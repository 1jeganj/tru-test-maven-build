package com.tru.commerce.order.purchase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.purchase.ShippingGroupMapContainer;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.order.TRUCommerceItemMetaInfo;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.TRUShippingGroupManager;
import com.tru.commerce.order.vo.TRUGiftOptionsInfo;
import com.tru.common.TRUConstants;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;

/**
 * The Class TRUGiftingPurchaseProcessHelper.
 * @author PA
 * @version 1.0
 */
public class TRUGiftingPurchaseProcessHelper extends GenericService {
	
	/** hold mPurchaseProcessHelper. */
	private TRUPurchaseProcessHelper mPurchaseProcessHelper;
 	
 	/**
	 * @return the mPurchaseProcessHelper
	 */
	public TRUPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}

	/**
	 * @param pPurchaseProcessHelper the mPurchaseProcessHelper to set
	 */
	public void setPurchaseProcessHelper(TRUPurchaseProcessHelper pPurchaseProcessHelper) {
		this.mPurchaseProcessHelper = pPurchaseProcessHelper;
	}

	/**
	 * Apply gift message.
	 * 
	 * @param pMap
	 *            the map
	 * @param pShippingGroupMapContainer 
	 */
	public void applyGiftMessage(Map<ShippingGroup, TRUGiftOptionsInfo> pMap, ShippingGroupMapContainer pShippingGroupMapContainer) {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUGiftingPurchaseProcessHelper.applyGiftMessage method");
			vlogDebug("INPUT PARAMS OF applyGiftMessage() pMap: {0}", pMap);
		}
		((TRUShippingGroupManager) getPurchaseProcessHelper().getShippingGroupManager()).applyGiftMessage(pMap, pShippingGroupMapContainer);
	}

	/**
	 * This method is used to check if the message typed by the user contains
	 * any profane words or not and returns a boolean based on that.
	 * 
	 * @param pGiftMessage
	 *            the gift message
	 * @return true, if successful
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public boolean checkProfanityWords(String pGiftMessage) throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUGiftingPurchaseProcessHelper.checkProfanityWords method");
			vlogDebug("INPUT PARAMS OF checkProfanityWords method pGiftMessage: {0} ", pGiftMessage);
		}
		String[] userDataWords = pGiftMessage.split(TRUConstants.WHITE_SPACE);
		final Set<String> profanityWordSet = ((TRUOrderTools) getPurchaseProcessHelper().getOrderTools()).getProfanityWords();
		if(profanityWordSet.contains(pGiftMessage.toLowerCase())){
			if (isLoggingDebug()) {
				vlogDebug("END:: TRUGiftingPurchaseProcessHelper.checkProfanityWords return: true");
			}
			return true;
		}
		for (int i = 0; i < userDataWords.length; i++) {
			if(profanityWordSet.contains(userDataWords[i].toLowerCase())) {
				if (isLoggingDebug()) {
					vlogDebug("END:: TRUGiftingPurchaseProcessHelper.checkProfanityWords return: true");
				}
				return true;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUGiftingPurchaseProcessHelper.checkProfanityWords return: false");
		}
		return false;
	}

	/**
	 * Update gift wrap.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pItemsToRemove
	 *            the items to remove
	 * @throws CommerceException
	 *             -the commerce exception
	 * @throws RepositoryException
	 *             - throws when
	 */
	@SuppressWarnings("unchecked")
	public void updateGiftWrap(Order pOrder, CommerceItem pItemsToRemove) throws CommerceException, RepositoryException {
		if (null != pItemsToRemove) {
			final TRUCommercePropertyManager commercePropertyManager = getPurchaseProcessHelper().getOrderTools().getCommercePropertyManager();
			final List<ShippingGroupCommerceItemRelationship> sgcirs = pItemsToRemove.getShippingGroupRelationships();
			for (ShippingGroupCommerceItemRelationship sgcir : sgcirs) {
				final List<TRUCommerceItemMetaInfo> metaList = ((TRUShippingGroupCommerceItemRelationship) sgcir).getMetaInfo();
				for (TRUCommerceItemMetaInfo metaInfo : metaList) {
					if (metaInfo != null && metaInfo.getPropertyValue(commercePropertyManager.getGiftWrapItemPropertyName()) != null) {
						final RepositoryItem gwCommerceItem = (RepositoryItem) metaInfo.getPropertyValue(commercePropertyManager
								.getGiftWrapItemPropertyName());
						final TRUGiftWrapCommerceItem truGiftWrapCommerceItem = (TRUGiftWrapCommerceItem) pOrder.getCommerceItem(gwCommerceItem
								.getRepositoryId());
						final String giftWrapCommId = truGiftWrapCommerceItem.getId();
						if (!StringUtils.isBlank(giftWrapCommId)) {
							List<RepositoryItem> giftInfos = ((TRUShippingGroupCommerceItemRelationship) sgcir).getGiftItemInfo();
							if(giftInfos != null && !giftInfos.isEmpty()) {
								Map<String, Long> wrapInfo = new HashMap<String, Long>();
								for (RepositoryItem giftItem : giftInfos) {
									String wrapCommId = (String) giftItem.getPropertyValue(getPurchaseProcessHelper()
											.getCommercePropertyManager().getGiftWrapCommItemId());
									if (StringUtils.isNotBlank(giftWrapCommId) && wrapCommId.equals(giftWrapCommId)) {
										long wrapCommerceItemQty = (long) giftItem.getPropertyValue(getPurchaseProcessHelper()
												.getCommercePropertyManager().getGiftWrapQuantity());
										String getGiftWrapCatalogRefId = (String) giftItem.getPropertyValue(getPurchaseProcessHelper()
												.getCommercePropertyManager().getGiftWrapCatalogRefId());
										wrapInfo.put(getGiftWrapCatalogRefId, wrapCommerceItemQty);
										/*updateGiftWrap(pOrder, sgcir.getId(), getGiftWrapCatalogRefId,
												wrapCommerceItemQty, false);*/
									}
								}
								for (String getGiftWrapCatalogRefId : wrapInfo.keySet()) {
									updateGiftWrap(pOrder, sgcir.getId(), getGiftWrapCatalogRefId,
											wrapInfo.get(getGiftWrapCatalogRefId), false);
								}
							}
							//updateGiftWrap(pOrder, sgcir.getId(), skuId, sgcir.getQuantity(), false);
						}
					}
				}
			}
		}
	}

	
	/**
	 * Creates the or update gift wrap commerce items.
	 *
	 * @param pInputParams the input params
	 * @throws CommerceException the commerce exception
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws RepositoryException the repository exception
	 */
	public void createOrUpdateGiftWrapCommerceItems(Map<String, Object> pInputParams) throws CommerceException, ServletException,
			IOException, RepositoryException {

		/**
		 * Locale pUserLocale, Order pOrder, String pShipGroupId, String
		 * pCommerceItemRelationshipId, String pGiftWrapSkuId, String
		 * pGiftWrapProductId, String pPreviousGiftWrapSkuId, boolean
		 * pIsGiftWrapSelected
		 */
		final Locale userLocale = (Locale) pInputParams.get(TRUConstants.USER_LOCALE);
		final Order order = (Order) pInputParams.get(TRUConstants.ORDER);
		final String shipGroupId = (String) pInputParams.get(TRUConstants.SHIP_GRP_ID);
		final String commerceItemRelationshipId = (String) pInputParams.get(TRUConstants.COMMERCE_REL_ID);
		final String giftWrapSkuId = (String) pInputParams.get(TRUConstants.GIFT_WRAP_SKU_ID);
		final String giftWrapProductId = (String) pInputParams.get(TRUConstants.GIFT_WRAP_PROD_ID);
		final String previousGiftWrapSkuId = (String) pInputParams.get(TRUConstants.PRE_GIFT_WRAP_SKU_ID);
		final boolean isGiftWrapSelected = (Boolean) pInputParams.get(TRUConstants.IS_GIFT_WRAP_SELECTED);

		TRUGiftWrapCommerceItem currentgiftWrapCommerceItem = null;
		TRUGiftWrapCommerceItem prevGiftWrapCommerceItem = null;
		MutableRepositoryItem currentGiftItem = null;
		MutableRepositoryItem prevGiftItem = null;
		final TRUCommercePropertyManager commercePropertyManager = getPurchaseProcessHelper().getOrderTools().getCommercePropertyManager();
		final MutableRepository orderRepository = (MutableRepository) getPurchaseProcessHelper().getOrderTools().getOrderRepository();
		ShippingGroup shippingGroup = order.getShippingGroup(shipGroupId);
		final TRUShippingGroupCommerceItemRelationship shpGrpCIRel = (TRUShippingGroupCommerceItemRelationship) order
				.getRelationship(commerceItemRelationshipId);
		if (shippingGroup == null) {
			shippingGroup = shpGrpCIRel.getShippingGroup();
		}

		List<RepositoryItem> getGiftItemInfo = shpGrpCIRel.getGiftItemInfo();
		if (getGiftItemInfo == null) {
			getGiftItemInfo = new ArrayList<RepositoryItem>();
		}
		for (RepositoryItem giftItem : getGiftItemInfo) {
			if (giftItem.getPropertyValue(commercePropertyManager.getGiftWrapCommItemId()) != null) {
				final CommerceItem commerceItem = order.getCommerceItem((String) giftItem.getPropertyValue(commercePropertyManager
						.getGiftWrapCommItemId()));
				if (commerceItem instanceof TRUGiftWrapCommerceItem) {
					if (StringUtils.isNotBlank(giftWrapSkuId) && giftWrapSkuId.equalsIgnoreCase(commerceItem.getCatalogRefId())) {
						currentgiftWrapCommerceItem = (TRUGiftWrapCommerceItem) commerceItem;
						currentGiftItem = (MutableRepositoryItem) giftItem;
					}
					if (StringUtils.isNotBlank(previousGiftWrapSkuId)
							&& previousGiftWrapSkuId.equalsIgnoreCase(commerceItem.getCatalogRefId())) {
						prevGiftWrapCommerceItem = (TRUGiftWrapCommerceItem) commerceItem;
						prevGiftItem = (MutableRepositoryItem) giftItem;
					}
				}
			}
		}
		if (isGiftWrapSelected && currentgiftWrapCommerceItem != null) {
			currentgiftWrapCommerceItem.setQuantity(currentgiftWrapCommerceItem.getQuantity() + TRUTaxwareConstant.NUMBER_ONE);
			getPurchaseProcessHelper().getCommerceItemManager().adjustItemRelationshipsForQuantityChange(order, currentgiftWrapCommerceItem,
					currentgiftWrapCommerceItem.getQuantity());
			getPurchaseProcessHelper().getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(order, currentgiftWrapCommerceItem.getId(),
					shippingGroup.getId(), TRUTaxwareConstant.NUMBER_ONE);
			// Update the quantity in gift item
			final long selectedWrapQty = ((long) currentGiftItem.getPropertyValue(commercePropertyManager.getGiftWrapQuantity()))
					+ TRUTaxwareConstant.NUMBER_ONE;
			currentGiftItem.setPropertyValue(commercePropertyManager.getGiftWrapQuantity(), selectedWrapQty);
		} else if (isGiftWrapSelected && currentgiftWrapCommerceItem == null) {
			final TRUAddCommerceItemInfo commerceItemInfo = new TRUAddCommerceItemInfo();
			commerceItemInfo.setCommerceItemType(TRUCommerceConstants.GIFT_WRAP_COMMERCE_ITEM);
			commerceItemInfo.setCatalogRefId(giftWrapSkuId);
			commerceItemInfo.setProductId(giftWrapProductId);
			commerceItemInfo.setQuantity(TRUTaxwareConstant.NUMBER_ONE);
			commerceItemInfo.setSiteId(order.getSiteId());
			currentgiftWrapCommerceItem = (TRUGiftWrapCommerceItem) getPurchaseProcessHelper().createCommerceItem(commerceItemInfo, userLocale.toString(), order);
			getPurchaseProcessHelper().getOrderManager().getCommerceItemManager().addAsSeparateItemToOrder(order, currentgiftWrapCommerceItem);
			getPurchaseProcessHelper().getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(order, currentgiftWrapCommerceItem.getId(),
					shippingGroup.getId(), TRUTaxwareConstant.NUMBER_ONE);
			MutableRepositoryItem giftItem = orderRepository.createItem(commercePropertyManager.getGiftItem());
			giftItem.setPropertyValue(commercePropertyManager.getOrderId(), order.getId());
			giftItem.setPropertyValue(commercePropertyManager.getShippingGroupId(), shippingGroup.getId());
			giftItem.setPropertyValue(commercePropertyManager.getGiftWrapCommItemId(), currentgiftWrapCommerceItem.getId());
			giftItem.setPropertyValue(commercePropertyManager.getGiftWrapCatalogRefId(), giftWrapSkuId);
			giftItem.setPropertyValue(commercePropertyManager.getGiftWrapProductId(), giftWrapProductId);
			giftItem.setPropertyValue(commercePropertyManager.getGiftWrapQuantity(), Long.valueOf(TRUTaxwareConstant.NUMBER_ONE));
			getGiftItemInfo.add(giftItem);
		}
		if (prevGiftWrapCommerceItem != null && prevGiftWrapCommerceItem.getQuantity() == TRUTaxwareConstant.NUMBER_ONE) {
			getGiftItemInfo.remove(prevGiftItem);
			getPurchaseProcessHelper().getOrderManager().getCommerceItemManager().removeItemFromOrder(order, prevGiftWrapCommerceItem.getId());
		} else if (prevGiftWrapCommerceItem != null) {
			prevGiftWrapCommerceItem.setQuantity(prevGiftWrapCommerceItem.getQuantity() - TRUTaxwareConstant.NUMBER_ONE);
			getPurchaseProcessHelper().getCommerceItemManager().adjustItemRelationshipsForQuantityChange(order, prevGiftWrapCommerceItem,
					prevGiftWrapCommerceItem.getQuantity());
			getPurchaseProcessHelper().getOrderManager().getCommerceItemManager().removeItemQuantityFromShippingGroup(order, prevGiftWrapCommerceItem.getId(),
					shippingGroup.getId(), TRUTaxwareConstant.NUMBER_ONE);
			// Update the quantity in gift item
			final long selectedWrapQty = ((long) prevGiftItem.getPropertyValue(commercePropertyManager.getGiftWrapQuantity()))
					- TRUTaxwareConstant.NUMBER_ONE;
			prevGiftItem.setPropertyValue(commercePropertyManager.getGiftWrapQuantity(), selectedWrapQty);
		}
	}

	/**
	 * If Show me gift Options value is false, clear all Gift Options details.
	 * 
	 * @param pOrder
	 *            the order
	 * @throws CommerceException
	 *             - the commerce exception
	 */
	@SuppressWarnings("unchecked")
	public void clearAnyGiftOptions(Order pOrder) throws CommerceException {
		final List<String> wrapItemIds = new ArrayList<String>();
		// Clear all Gift Options details
		final TRUCommercePropertyManager commercePropertyManager = getPurchaseProcessHelper().getOrderTools().getCommercePropertyManager();
		final List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		if (shippingGroups != null && !shippingGroups.isEmpty()) {
			for (ShippingGroup shippingGroup : shippingGroups) {
				synchronized (shippingGroup) {
					if (shippingGroup instanceof HardgoodShippingGroup) {
						((TRUHardgoodShippingGroup) shippingGroup).setGiftReceipt(false);
						((TRUHardgoodShippingGroup) shippingGroup).setGiftWrapMessage(null);
						final List<CommerceItemRelationship> ciRels = shippingGroup.getCommerceItemRelationships();
						for (CommerceItemRelationship commerceItemRelationship : ciRels) {
							TRUShippingGroupCommerceItemRelationship truSgCiR = (TRUShippingGroupCommerceItemRelationship) commerceItemRelationship;
							if (truSgCiR.getIsGiftItem()) {
								final List<RepositoryItem> getGiftItemInfo = truSgCiR.getGiftItemInfo();
								if (getGiftItemInfo != null && !getGiftItemInfo.isEmpty()) {
									for (RepositoryItem giftItem : getGiftItemInfo) {
										if (giftItem.getPropertyValue(commercePropertyManager.getGiftWrapCommItemId()) != null) {
											wrapItemIds.add((String) giftItem.getPropertyValue(commercePropertyManager
													.getGiftWrapCommItemId()));
										}
									}
								}
								// Remove the Gift Wrap commerceItem and assign
								// null
								truSgCiR.setIsGiftItem(false);
								truSgCiR.setGiftItemInfo(null);
							}
						}
					}
				}
			}
		}
		for (String giftWrapItemId : wrapItemIds) {
			getPurchaseProcessHelper().getOrderManager().getCommerceItemManager().removeItemFromOrder(pOrder, giftWrapItemId);
		}
	}

	/**
	 * To update the Gift wrap item details.
	 * 
	 * @param pOrder
	 *            - Order obj
	 * @param pCommerceItemRelationshipId
	 *            - CIRel Id
	 * @param pGiftWrapSkuId
	 *            - wrap sku id
	 * @param pGiftWrapQty
	 *            - wrap qty
	 * @param pIsIncreaseQty
	 *            - Is increasing the GW qty or not
	 * @throws CommerceException
	 *             - while getting
	 * @throws RepositoryException
	 *             - while getting
	 */
	public void updateGiftWrap(Order pOrder, String pCommerceItemRelationshipId, String pGiftWrapSkuId, long pGiftWrapQty,
			boolean pIsIncreaseQty) throws CommerceException, RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN :: TRUGiftingPurchaseProcessHelper.updateGiftWrap method..");
		}
		TRUGiftWrapCommerceItem giftWrapCommerceItem = null;
		MutableRepositoryItem currentGiftItem = null;
		TRUCommercePropertyManager commercePropertyManager = getPurchaseProcessHelper().getOrderTools().getCommercePropertyManager();
		TRUShippingGroupCommerceItemRelationship shpGrpCIRel = (TRUShippingGroupCommerceItemRelationship) pOrder
				.getRelationship(pCommerceItemRelationshipId);
		ShippingGroup shippingGroup = shpGrpCIRel.getShippingGroup();
		List<RepositoryItem> getGiftItemInfo = shpGrpCIRel.getGiftItemInfo();
		if (getGiftItemInfo == null) {
			getGiftItemInfo = new ArrayList<RepositoryItem>();
		}
		// Taking all Gift wrap items and adding it to wrapItemDetails to assign
		// to each Main Comm Item
		if (getGiftItemInfo != null && !getGiftItemInfo.isEmpty()) {
			for (RepositoryItem giftItem : getGiftItemInfo) {
				if (giftItem.getPropertyValue(commercePropertyManager.getGiftWrapCommItemId()) != null) {
					CommerceItem commerceItem = pOrder.getCommerceItem((String) giftItem.getPropertyValue(commercePropertyManager
							.getGiftWrapCommItemId()));
					if (commerceItem instanceof TRUGiftWrapCommerceItem && StringUtils.isNotBlank(pGiftWrapSkuId) &&
							pGiftWrapSkuId.equalsIgnoreCase(commerceItem.getCatalogRefId())) {
						giftWrapCommerceItem = (TRUGiftWrapCommerceItem) commerceItem;
						currentGiftItem = (MutableRepositoryItem) giftItem;
						break;
					}
				}
			}
		}

		// While increase qty
		if (pIsIncreaseQty & giftWrapCommerceItem != null) {
			giftWrapCommerceItem.setQuantity(giftWrapCommerceItem.getQuantity() + pGiftWrapQty/*
																							 * TRUCheckoutConstants
																							 * .
																							 * INT_ONE
																							 */);
			getPurchaseProcessHelper().getCommerceItemManager().adjustItemRelationshipsForQuantityChange(pOrder, giftWrapCommerceItem,
					giftWrapCommerceItem.getQuantity());
			getPurchaseProcessHelper().getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(pOrder, giftWrapCommerceItem.getId(),
					shippingGroup.getId(), pGiftWrapQty/*
														 * TRUCheckoutConstants.
														 * INT_ONE
														 */);

			// Update the quantity in gift item
			long selectedWrapQty = ((long) currentGiftItem.getPropertyValue(commercePropertyManager.getGiftWrapQuantity())) + pGiftWrapQty;
			currentGiftItem.setPropertyValue(commercePropertyManager.getGiftWrapQuantity(), selectedWrapQty);
		}
		// While Remove or decrease qty
		else if (giftWrapCommerceItem != null && giftWrapCommerceItem.getQuantity() == pGiftWrapQty) {
			getGiftItemInfo.remove(currentGiftItem);
			getPurchaseProcessHelper().getOrderManager().getCommerceItemManager().removeItemFromOrder(pOrder, giftWrapCommerceItem.getId());
		} else if (giftWrapCommerceItem != null) {
			giftWrapCommerceItem.setQuantity(giftWrapCommerceItem.getQuantity() - pGiftWrapQty);
			getPurchaseProcessHelper().getCommerceItemManager().adjustItemRelationshipsForQuantityChange(pOrder, giftWrapCommerceItem,
					giftWrapCommerceItem.getQuantity());
			getPurchaseProcessHelper().getOrderManager().getCommerceItemManager().removeItemQuantityFromShippingGroup(pOrder, giftWrapCommerceItem.getId(),
					shippingGroup.getId(), pGiftWrapQty);

			// Update the quantity in gift item
			long selectedWrapQty = ((long) currentGiftItem.getPropertyValue(commercePropertyManager.getGiftWrapQuantity())) - pGiftWrapQty;
			currentGiftItem.setPropertyValue(commercePropertyManager.getGiftWrapQuantity(), selectedWrapQty);
		}
		if (isLoggingDebug()) {
			vlogDebug("END :: TRUGiftingPurchaseProcessHelper.updateCardDataInOrder method..");
		}
	}

	/**
	 * updateGiftItem.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pShippingGroup
	 *            the shipping group
	 * @param pShpGrpCIRel
	 *            the shp grp ci rel
	 * @param pGiftWrapSkuId
	 *            the gift wrap sku id
	 * @param pQuantity
	 *            the quantity
	 * @return giftItemFlag
	 * @throws CommerceException
	 *             - the commerce exception
	 */
	public boolean updateGiftItem(Order pOrder, ShippingGroup pShippingGroup, TRUShippingGroupCommerceItemRelationship pShpGrpCIRel,
			String pGiftWrapSkuId, long pQuantity) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN :: TRUGiftingPurchaseProcessHelper.updateGiftItem method..");
		}
		boolean giftItemFlag = false;
		TRUGiftWrapCommerceItem giftWrapCommerceItem = null;
		MutableRepositoryItem currentGiftItem = null;
		TRUCommercePropertyManager commercePropertyManager = getPurchaseProcessHelper().getOrderTools().getCommercePropertyManager();
		List<RepositoryItem> getGiftItemInfo = pShpGrpCIRel.getGiftItemInfo();
		if (getGiftItemInfo == null) {
			getGiftItemInfo = new ArrayList<RepositoryItem>();
		}
		if (getGiftItemInfo != null && !getGiftItemInfo.isEmpty()) {
			for (RepositoryItem giftItem : getGiftItemInfo) {
				if (giftItem.getPropertyValue(commercePropertyManager.getGiftWrapCommItemId()) != null) {

					if (pGiftWrapSkuId == null) {
						throw new CommerceException(TRUCommerceConstants.NO_GIFT_WRAP);
					}

					CommerceItem truGiftWrapCommerceItem = pOrder.getCommerceItem((String) giftItem
							.getPropertyValue(commercePropertyManager.getGiftWrapCommItemId()));
					if (pGiftWrapSkuId.equalsIgnoreCase(truGiftWrapCommerceItem.getCatalogRefId())) {
						giftWrapCommerceItem = (TRUGiftWrapCommerceItem) truGiftWrapCommerceItem;
						currentGiftItem = (MutableRepositoryItem) giftItem;
						break;
					}
				}
			}
			if (giftWrapCommerceItem != null && giftWrapCommerceItem.getQuantity() == pQuantity) {
				giftItemFlag = false;
				// Remove Wrap Commerce Item
				getGiftItemInfo.remove(currentGiftItem);
				getPurchaseProcessHelper().getOrderManager().getCommerceItemManager().removeItemFromOrder(pOrder, giftWrapCommerceItem.getId());
			} else if (giftWrapCommerceItem != null) {
				giftWrapCommerceItem.setQuantity(giftWrapCommerceItem.getQuantity() - pQuantity);
				getPurchaseProcessHelper().getCommerceItemManager().adjustItemRelationshipsForQuantityChange(pOrder, giftWrapCommerceItem,
						giftWrapCommerceItem.getQuantity());
				getPurchaseProcessHelper().getOrderManager().getCommerceItemManager().removeItemQuantityFromShippingGroup(pOrder,
						giftWrapCommerceItem.getRepositoryItem().getRepositoryId(), pShippingGroup.getId(), pQuantity);
				// Update the quantity in gift item
				long selectedWrapQty = ((long) currentGiftItem.getPropertyValue(commercePropertyManager.getGiftWrapQuantity())) - pQuantity;
				currentGiftItem.setPropertyValue(commercePropertyManager.getGiftWrapQuantity(), selectedWrapQty);
				giftItemFlag = true;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END :: TRUGiftingPurchaseProcessHelper.updateGiftItem method..");
		}
		return giftItemFlag;
	}

	/**
	 * Removes the gift item quantity.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pSgCiRel
	 *            the sg ci rel
	 * @param pGiftWrapCommerceItem
	 *            the gift wrap commerce item
	 * @param pQuantity
	 *            the quantity
	 * @throws CommerceItemNotFoundException
	 *             the commerce item not found exception
	 * @throws InvalidParameterException
	 *             the invalid parameter exception
	 */
	public void removeGiftItemQuantity(Order pOrder, ShippingGroupCommerceItemRelationship pSgCiRel, CommerceItem pGiftWrapCommerceItem,
			long pQuantity) throws CommerceItemNotFoundException, InvalidParameterException {
		if (isLoggingDebug()) {
			vlogDebug("START :: TRUGiftingPurchaseProcessHelper.removeGiftItemQuantity method..");
		}
		TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) pSgCiRel;
		TRUCommercePropertyManager commercePropertyManager = getPurchaseProcessHelper().getOrderTools().getCommercePropertyManager();

		List<RepositoryItem> giftItemInfos = sgCiRel.getGiftItemInfo();
		MutableRepositoryItem giftItem = null;

		if (giftItemInfos != null && !giftItemInfos.isEmpty()) {
			for (RepositoryItem giftItemInfo : giftItemInfos) {
				if (giftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapCommItemId()) != null &&
						 giftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapCatalogRefId()) != null &&
						 StringUtils.isNotBlank(pGiftWrapCommerceItem.getId()) &&
						 pGiftWrapCommerceItem.getId().equalsIgnoreCase(
								(String) giftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapCommItemId()))) {
					giftItem = (MutableRepositoryItem) giftItemInfo;
					break;
				}
			}

			if (giftItem == null) {
				return;
			}

			if (null != giftItem.getPropertyValue(commercePropertyManager.getGiftWrapQuantity())) {
				Long giftWrapQty = (Long) giftItem.getPropertyValue(commercePropertyManager.getGiftWrapQuantity());
				if (giftWrapQty <= pQuantity) {
					// Remove Wrap Commerce Item
					giftItemInfos.remove(giftItem);
				} else {
					giftItem.setPropertyValue(commercePropertyManager.getGiftWrapQuantity(), giftWrapQty - pQuantity);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END :: TRUGiftingPurchaseProcessHelper.removeGiftItemQuantity method..");
		}
	}

	/**
	 * Adds the gift item quantity.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pSgCiRel
	 *            the sg ci rel
	 * @param pGiftWrapCommerceItem
	 *            the gift wrap commerce item
	 * @param pQuantity
	 *            the quantity
	 * @throws CommerceException
	 *             the commerce exception
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public void addGiftItemQuantity(Order pOrder, ShippingGroupCommerceItemRelationship pSgCiRel, CommerceItem pGiftWrapCommerceItem,
			long pQuantity) throws CommerceException, RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("START :: TRUGiftingPurchaseProcessHelper.addGiftItemQuantity method..");
		}
		if(null == pGiftWrapCommerceItem) {
			return;
		}
		CommerceItem giftWrapCommerceItem = pGiftWrapCommerceItem;
		TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) pSgCiRel;
		TRUCommercePropertyManager commercePropertyManager = getPurchaseProcessHelper().getOrderTools().getCommercePropertyManager();
		MutableRepository orderRepository = (MutableRepository) getPurchaseProcessHelper().getOrderTools().getOrderRepository();
		List<RepositoryItem> giftItemInfos = sgCiRel.getGiftItemInfo();
		ShippingGroup shippingGroup = sgCiRel.getShippingGroup();
		MutableRepositoryItem giftItem = null;

		if (giftItemInfos != null && !giftItemInfos.isEmpty()) {
			for (RepositoryItem giftItemInfo : giftItemInfos) {
				if (giftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapCommItemId()) != null &&
						giftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapCatalogRefId()) != null &&
						StringUtils.isNotBlank(pGiftWrapCommerceItem.getId()) &&
						pGiftWrapCommerceItem.getId().equalsIgnoreCase(
								(String) giftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapCommItemId()))) {
					giftItem = (MutableRepositoryItem) giftItemInfo;
					giftWrapCommerceItem = pOrder.getCommerceItem((String) giftItemInfo.getPropertyValue(commercePropertyManager
							.getGiftWrapCommItemId()));
					break;
				}
			}
		}

		if (giftItem == null && giftWrapCommerceItem != null) {
			giftItem = orderRepository.createItem(commercePropertyManager.getGiftItem());
			giftItem.setPropertyValue(commercePropertyManager.getOrderId(), pOrder.getId());
			giftItem.setPropertyValue(commercePropertyManager.getShippingGroupId(), shippingGroup.getId());
			giftItem.setPropertyValue(commercePropertyManager.getGiftWrapCommItemId(), giftWrapCommerceItem.getId());
			giftItem.setPropertyValue(commercePropertyManager.getGiftWrapCatalogRefId(), giftWrapCommerceItem.getCatalogRefId());
			giftItem.setPropertyValue(commercePropertyManager.getGiftWrapProductId(), giftWrapCommerceItem.getAuxiliaryData()
					.getProductId());
			giftItem.setPropertyValue(commercePropertyManager.getGiftWrapQuantity(), Long.valueOf(pQuantity));
			giftItemInfos.add(giftItem);
		} else {
			Long giftWrapQty = (Long) giftItem.getPropertyValue(commercePropertyManager.getGiftWrapQuantity());
			giftItem.setPropertyValue(commercePropertyManager.getGiftWrapQuantity(), giftWrapQty + pQuantity);
		}
		if (isLoggingDebug()) {
			vlogDebug("END :: TRUGiftingPurchaseProcessHelper.addGiftItemQuantity method..");
		}
	}

	/**
	 * Merge gift item info.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pOldGiftItemInfo
	 *            the old gift item info
	 * @param pSGCIR
	 *            the sgcir
	 * @throws CommerceItemNotFoundException
	 *             the commerce item not found exception
	 * @throws InvalidParameterException
	 *             the invalid parameter exception
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public void mergeGiftItemInfo(Order pOrder, List<RepositoryItem> pOldGiftItemInfo, TRUShippingGroupCommerceItemRelationship pSGCIR)
			throws CommerceItemNotFoundException, InvalidParameterException, RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("START :: TRUGiftingPurchaseProcessHelper.mergeGiftItemInfo method..");
		}
		TRUCommercePropertyManager commercePropertyManager = getPurchaseProcessHelper().getOrderTools().getCommercePropertyManager();
		Map<String, Long> giftWrapInfo = new HashMap<String, Long>();
		String giftWrapCommItemId = null;
		CommerceItem lGiftWrapCommerceItem = null;
		ShippingGroup shippingGroup = pSGCIR.getShippingGroup();
		MutableRepository orderRepository = (MutableRepository) getPurchaseProcessHelper().getOrderTools().getOrderRepository();
		MutableRepositoryItem newGiftItem = null;

		Long giftWrapQty = null;
		if (pOldGiftItemInfo != null && !pOldGiftItemInfo.isEmpty()) {
			for (RepositoryItem giftItem : pOldGiftItemInfo) {
				giftWrapCommItemId = (String) giftItem.getPropertyValue(commercePropertyManager.getGiftWrapCommItemId());
				giftWrapQty = (Long) giftItem.getPropertyValue(commercePropertyManager.getGiftWrapQuantity());
				giftWrapInfo.put(giftWrapCommItemId, giftWrapQty);
			}
		}

		List<RepositoryItem> giftItemInfos = pSGCIR.getGiftItemInfo();
		if (giftItemInfos != null && !giftItemInfos.isEmpty()) {
			for (RepositoryItem giftItemInfo : giftItemInfos) {
				giftWrapCommItemId = (String) giftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapCommItemId());
				if (StringUtils.isNotBlank(giftWrapCommItemId) && giftWrapInfo.containsKey(giftWrapCommItemId)) {
					giftWrapQty = (Long) giftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapQuantity());
					((MutableRepositoryItem) giftItemInfo).setPropertyValue(commercePropertyManager.getShippingGroupId(),
							shippingGroup.getId());
					((MutableRepositoryItem) giftItemInfo).setPropertyValue(commercePropertyManager.getGiftWrapQuantity(), giftWrapQty
							+ giftWrapInfo.get(giftWrapCommItemId));
					giftWrapInfo.remove(giftWrapCommItemId);
				}
			}
		}
		if (giftWrapInfo != null && !giftWrapInfo.isEmpty()) {
			for (String gwCommItemId : giftWrapInfo.keySet()) {
				lGiftWrapCommerceItem = pOrder.getCommerceItem(gwCommItemId);
				newGiftItem = orderRepository.createItem(commercePropertyManager.getGiftItem());
				newGiftItem.setPropertyValue(commercePropertyManager.getOrderId(), pOrder.getId());
				newGiftItem.setPropertyValue(commercePropertyManager.getShippingGroupId(), shippingGroup.getId());
				newGiftItem.setPropertyValue(commercePropertyManager.getGiftWrapCommItemId(), lGiftWrapCommerceItem.getId());
				newGiftItem.setPropertyValue(commercePropertyManager.getGiftWrapCatalogRefId(), lGiftWrapCommerceItem.getCatalogRefId());
				newGiftItem.setPropertyValue(commercePropertyManager.getGiftWrapProductId(), lGiftWrapCommerceItem.getAuxiliaryData()
						.getProductId());
				newGiftItem.setPropertyValue(commercePropertyManager.getGiftWrapQuantity(), Long.valueOf(giftWrapInfo.get(gwCommItemId)));
				giftItemInfos.add(newGiftItem);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END :: TRUGiftingPurchaseProcessHelper.mergeGiftItemInfo method..");
		}
	}
	
	/**
	 * Apply gift options.
	 *
	 * @param pOrder the order
	 * @throws CommerceException the commerce exception
	 */
	@SuppressWarnings("unchecked")
	public void applyGiftOptions(Order pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUGiftingPurchaseProcessHelper.applyGiftOptions method");
		}
		if(pOrder == null) {
			return;
		}
		final TRUCommercePropertyManager commercePropertyManager = getPurchaseProcessHelper().getOrderTools().getCommercePropertyManager();
		final List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		TRUShippingGroupCommerceItemRelationship truSgCiR = null;
		CommerceItem commerceItem = null;
		RepositoryItem skuItem = null;
		String canBeGiftWrapped = null;
		if (shippingGroups != null && !shippingGroups.isEmpty()) {
			for (ShippingGroup shippingGroup : shippingGroups) {
				synchronized (shippingGroup) {
					if (shippingGroup instanceof HardgoodShippingGroup) {
						final List<CommerceItemRelationship> ciRels = shippingGroup.getCommerceItemRelationships();
						for (CommerceItemRelationship commerceItemRelationship : ciRels) {
							truSgCiR = (TRUShippingGroupCommerceItemRelationship) commerceItemRelationship;
							commerceItem = truSgCiR.getCommerceItem();
							skuItem = (RepositoryItem) commerceItem.getAuxiliaryData().getCatalogRef();
							if (skuItem != null) {
								canBeGiftWrapped = (String) skuItem.getPropertyValue(commercePropertyManager.getCanBeGiftWrapped());
								if (!StringUtils.isBlank(canBeGiftWrapped) && canBeGiftWrapped.equals(TRUCommerceConstants.YES)) {
									truSgCiR.setIsGiftItem(true);
								} 
							}
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUGiftingPurchaseProcessHelper.applyGiftOptions method");
		}
	}
	
	
	/**
	 * Update sgci rel based on gw and promo split.
	 * 
	 * @param pSelectedShippingGroupRel
	 *            the selected shipping group rel
	 * @param pQtyToMerge
	 *            the qty to merge
	 *  @param pOrder
	 *   			the prder
	 *    @param pMetaInfoId
	 *    			the method info
	 * @return the long
	 * @throws CommerceException
	 *             the commerce exception
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public long updateSGCIRelBasedOnGWAndPromoSplit(ShippingGroupCommerceItemRelationship pSelectedShippingGroupRel,
			long pQtyToMerge, Order pOrder, String pMetaInfoId) throws CommerceException, RepositoryException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::updateSGCIRelBasedOnGWAndPromoSplit() : BEGIN");
		}
		long qtyToMerge = pQtyToMerge;
		if (pSelectedShippingGroupRel instanceof TRUShippingGroupCommerceItemRelationship
				&& StringUtils.isNotBlank(pMetaInfoId)) {
			TRUShippingGroupCommerceItemRelationship sgci = (TRUShippingGroupCommerceItemRelationship) pSelectedShippingGroupRel;
			String giftWrapCommId = null;
			List<TRUCommerceItemMetaInfo> metaInfos = sgci.getMetaInfo();
			if (!metaInfos.isEmpty() && metaInfos.size() >= TRUCommerceConstants.INT_ONE) {
				for (TRUCommerceItemMetaInfo metaInfo : metaInfos) {
					if (pMetaInfoId.equals(metaInfo.getId())) {
						qtyToMerge = (long) metaInfo.getPropertyValue(getPurchaseProcessHelper()
								.getCommercePropertyManager().getQuantityPropertyName());
						if (null != metaInfo.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager()
								.getGiftWrapItemPropertyName())) {
							RepositoryItem item = (RepositoryItem) metaInfo.getPropertyValue(getPurchaseProcessHelper()
									.getCommercePropertyManager().getGiftWrapItemPropertyName());
							if (null != item) {
								giftWrapCommId = item.getRepositoryId();
							}
						}
						break;
					}
				}
			}
			if (sgci.getGiftItemInfo() != null && !sgci.getGiftItemInfo().isEmpty()) {
				List<RepositoryItem> giftInfos = sgci.getGiftItemInfo();
				for (RepositoryItem giftItem : giftInfos) {
					String wrapCommId = (String) giftItem.getPropertyValue(getPurchaseProcessHelper()
							.getCommercePropertyManager().getGiftWrapCommItemId());
					if (StringUtils.isNotBlank(giftWrapCommId) && wrapCommId.equals(giftWrapCommId)) {
						long wrapCommerceItemQty = (long) giftItem.getPropertyValue(getPurchaseProcessHelper()
								.getCommercePropertyManager().getGiftWrapQuantity());
						String getGiftWrapCatalogRefId = (String) giftItem.getPropertyValue(getPurchaseProcessHelper()
								.getCommercePropertyManager().getGiftWrapCatalogRefId());
						updateGiftWrap(pOrder, pSelectedShippingGroupRel.getId(), getGiftWrapCatalogRefId,
								wrapCommerceItemQty, false);
					}
				}
			}
			sgci.setIsGiftItem(Boolean.FALSE);
			if (isLoggingDebug()) {
				vlogDebug("qtyToMerge :: {0}", qtyToMerge);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::updateSGCIRelBasedOnGWAndPromoSplit() : END");
		}
		return qtyToMerge;
	}
	
}
