package com.tru.commerce.order.purchase;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.transaction.Transaction;

import atg.commerce.order.purchase.PurchaseProcessFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

/**
 * The Class TRUApplePayCheckoutFormHandler.
 */
public class TRUApplePayCheckoutFormHandler extends PurchaseProcessFormHandler {

	/** The m buy now with apple pay error URL. */
	private String mBuyNowWithApplePayErrorURL;
	
	/** The m buy now with apple pay success URL. */
	private String mBuyNowWithApplePaySuccessURL;


	/**
	 * This form handler validates all the form required business rules for Apple pay.
	 * 
	 * @param pRequest -- request
	 * @param pResponse -- response
	 * @return -- a boolean value
	 * @throws ServletException -- servlet exception
	 * @throws IOException -- IO exception
	 */
	public boolean handleBuyNowWithApplePay(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse)throws ServletException, IOException {

		String methodname = "handleBuyNowWithApplePay";
		if(isLoggingDebug()) {
			logDebug("TRUApplePayCheckoutFormHandler.handleBuyNowWithApplePay()method:STARTS");
		}
		RepeatingRequestMonitor repeatingRequestMonitor = getRepeatingRequestMonitor();     
		if ((repeatingRequestMonitor == null) || (repeatingRequestMonitor.isUniqueRequestEntry(methodname))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				if (!checkFormRedirect(null, getBuyNowWithApplePayErrorURL(), pRequest, pResponse)) {
					if (isLoggingDebug()) {
						logDebug("Form error at beginning of TRUApplePayCheckoutFormHandler.handleBuyNowWithApplePay, redirecting.");
					}
					return false;
				}
				preBuyNowWithApplePay(pRequest, pResponse);

				if (getFormError()) {
					return checkFormRedirect(null, getBuyNowWithApplePayErrorURL(),pRequest, pResponse);
				}

			}finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (repeatingRequestMonitor != null) {
					repeatingRequestMonitor.removeRequestEntry(methodname); 
				}                 
			}
		}
		if(isLoggingDebug()) {
			logDebug("TRUApplePayCheckoutFormHandler.handleBuyNowWithApplePay()method:ENDS");	
		}

		return checkFormRedirect(getBuyNowWithApplePaySuccessURL(),getBuyNowWithApplePayErrorURL(), pRequest, pResponse);
	}
	
	/**
	 * pre method for handleBuyNowWithApplePay
	 * sets success and error URLs.
	 *
	 * @param pRequest -- request
	 * @param pResponse - response
	 */
	public void preBuyNowWithApplePay(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse){
			if(isLoggingDebug()) {
				logDebug("KLSPurchaseVGCFormHandler.preAddVGCItemToOrder()method.STARTS");
			}
		
		}

	/**
	 * Gets the buy now with apple pay error URL.
	 *
	 * @return the buy now with apple pay error URL
	 */
	public String getBuyNowWithApplePayErrorURL() {
		return mBuyNowWithApplePayErrorURL;
	}

	/**
	 * Sets the buy now with apple pay error URL.
	 *
	 * @param pBuyNowWithApplePayErrorURL the new buy now with apple pay error URL
	 */
	public void setBuyNowWithApplePayErrorURL(
			String pBuyNowWithApplePayErrorURL) {
		this.mBuyNowWithApplePayErrorURL = pBuyNowWithApplePayErrorURL;
	}

	/**
	 * Gets the buy now with apple pay success URL.
	 *
	 * @return the buy now with apple pay success URL
	 */
	public String getBuyNowWithApplePaySuccessURL() {
		return mBuyNowWithApplePaySuccessURL;
	}

	/**
	 * Sets the buy now with apple pay success URL.
	 *
	 * @param pBuyNowWithApplePaySuccessURL the new buy now with apple pay success URL
	 */
	public void setBuyNowWithApplePaySuccessURL(
			String pBuyNowWithApplePaySuccessURL) {
		this.mBuyNowWithApplePaySuccessURL = pBuyNowWithApplePaySuccessURL;
	}
	

}
