package com.tru.commerce.order.purchase;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import atg.commerce.order.ShippingGroup;
import atg.commerce.order.purchase.CommerceItemShippingInfoContainer;
import atg.commerce.order.purchase.CommerceItemShippingInfoTools;
import atg.commerce.order.purchase.ShippingGroupMapContainer;

import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;

/**
 * Extending the OOTB class.
 * 
 * @author RRATHIRAJU
 *
 */
public class TRUCommerceItemShippingInfoTools extends CommerceItemShippingInfoTools {
	/**
	 * This method removes all the channel shipping groups.
	 * 
	 * @param pCommerceItemShippingInfoContainer
	 *            - CommerceItemShippingInfoContainer
	 * @param pShippingGroupMapContainer
	 *            - ShippingGroupMapContainer
	 */
	public void clearAllChannelShippingGroups(CommerceItemShippingInfoContainer pCommerceItemShippingInfoContainer,
			ShippingGroupMapContainer pShippingGroupMapContainer) {
		if (isLoggingDebug()) {
			logDebug("TRUCommerceItemShippingInfoTools/clearChannelShippingGroups method Start");
		}
		//Start : TUW-55621 code changes.
		Map<String, ShippingGroup> shippingGroups = (Map<String, ShippingGroup>)pShippingGroupMapContainer.getShippingGroupMap();
		Set<String> listOfChannelShippingGroups = new HashSet<String>();
		for (Map.Entry<String, ShippingGroup> entry : shippingGroups.entrySet()) {
			if (entry.getValue() instanceof TRUChannelHardgoodShippingGroup) {
					vlogDebug("Removing the Channel payment groups :{0}",entry.getValue());
				listOfChannelShippingGroups.add((String) entry.getKey());
			}
		}
		for (String shippingName : listOfChannelShippingGroups) {
			pShippingGroupMapContainer.removeShippingGroup(shippingName);
		}
		//#End : TUW-55621 code changes.
		if (isLoggingDebug()) {
			logDebug("TRUCommerceItemShippingInfoTools/clearChannelShippingGroups method End");
		}
	}
}
