package com.tru.commerce.order.purchase;

/**
 * This class extends Exception and used for handling the custom exceptions.
 * 
 * @author ORACLE
 * @version 1.0
 */
public class TRUPurchaseProcessException extends Exception {
	
	/**
	 * Variable to hold the serial version
	 */
	private static final long serialVersionUID = 1L;
	
	protected Object[] mParams;
	
	/**
	   * Set the Params property.
	   * @param pParams a <code>Params to be used while formatting message</code> value
	   */
	  public void setParams(Object[] pParams) {
	    mParams = pParams;
	  }

	  /**
	   * Return the Params property.
	   * @return a <code>Params to be used while formatting message</code> value
	   */
	  public Object[] getParams() {
	    return mParams;
	  }
	/**
	 * Empty constructor
	 */
	public TRUPurchaseProcessException() {
		super();
	}
	/**
	 * This Constructor used to throw only error message
	 * @param pMessage -- message
	 */
	public TRUPurchaseProcessException(String pMessage) {
		super(pMessage);
	}
	/**
	 * This Constructor used to throw only root cause
	 * @param pRootCause -- root cause
	 */
	public TRUPurchaseProcessException(Throwable pRootCause) {
		super(pRootCause);
	}
	/**
	 * This Constructor used to throw, both custom message and root cause.
	 * @param pMessage -- message
	 * @param pRootCause -- root cause
	 */
	public TRUPurchaseProcessException(String pMessage, Throwable pRootCause) {
		super(pMessage, pRootCause);
	}
	
	  /**
	   * Constructs a new StorePurchaseProcessException with the given
	   * explanation.
	   *
	   * @param pMessage the explanation of the exception
	   * @param pParams list of message paramenters to correctly format the message
	   **/
	  public TRUPurchaseProcessException(String pMessage, Object[] pParams) {
	    super(pMessage);
	    mParams = pParams;
	  }

	  /**
	   * Constructs a new StorePurchaseProcessException with the given
	   * explanation.
	   * 
	   * @param pMessage the explanation of the exception
	   * @param pException the initial exception which was the root
	   * cause of the problem
	   **/
	  public TRUPurchaseProcessException(String pMessage, Exception pException) {
	    super(pMessage, pException);
	  }

	  /**
	   * Constructs a new StorePurchaseProcessException with the given
	   * explanation.
	   * 
	   * @param pMessage the explanation of the exception
	   * @param pParams list of message paramenters to correctly format the message
	   * @param pException the initial exception which was the root
	   * cause of the problem
	   **/
	  public TRUPurchaseProcessException(String pMessage, Exception pException, Object[] pParams) {
	    super(pMessage, pException);
	    mParams = pParams;
	  }

}
