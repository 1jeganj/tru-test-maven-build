/*
 * 
 */
package com.tru.commerce.order.purchase;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.transaction.SystemException;
import javax.transaction.Transaction;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.Relationship;
import atg.commerce.order.RelationshipNotFoundException;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.ShippingGroupManager;
import atg.commerce.order.purchase.ShippingGroupFormHandler;
import atg.commerce.order.purchase.ShippingGroupInitializationException;
import atg.commerce.order.purchase.ShippingGroupInitializer;
import atg.commerce.pricing.PricingConstants;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.multisite.SiteContextManager;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.service.pipeline.PipelineResult;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.helper.TRUCartModifierHelper;
import com.tru.commerce.cart.vo.TRUCartItemDetailVO;
import com.tru.commerce.cart.vo.TRUChannelDetailsVo;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUBppItemInfo;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUCommerceItemImpl;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUOrderHolder;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.TRUShippingGroupManager;
import com.tru.commerce.order.TRUShippingManager;
import com.tru.commerce.order.vo.ApplePayOrderSummaryVO;
import com.tru.commerce.order.vo.PromoStatusVO;
import com.tru.commerce.order.vo.TRUShipGroupKey;
import com.tru.commerce.order.vo.TRUShipmentVO;
import com.tru.commerce.pricing.TRUItemPriceInfo;
import com.tru.commerce.pricing.TRUShipMethodVO;
import com.tru.commerce.pricing.TRUShippingPriceInfo;
import com.tru.commerce.pricing.TRUShippingPricingEngine;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.TRUUserSession;
import com.tru.common.cml.ValidationExceptions;
import com.tru.common.vo.TRUAddressDoctorResponseVO;
import com.tru.errorhandler.fhl.IErrorHandler;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.utils.TRUIntegrationStatusUtil;
import com.tru.utils.TRUStoreUtils;
import com.tru.validator.fhl.IValidator;

/**
 * Form Handler for handling shipping related checkout processes.
 * 
 * @author PA.
 * @version 1.0.
 */
public class TRUShippingGroupFormHandler extends ShippingGroupFormHandler {

	/** The Constant _1. */
	private static final int _1 = 1;

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CLOSE_ADDRESS_MODEL. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CLOSE_ADDRESS_MODEL = "TRUShippingGroupFormHandler.handleCloseAddressModel";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CHANGE_SHIP_METHOD_ZONE. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CHANGE_SHIP_METHOD_ZONE = "TRUShippingGroupFormHandler.handleChangeShipMethodZone";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CHANGE_SHIPPING_ADDRESS. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CHANGE_SHIPPING_ADDRESS = "TRUShippingGroupFormHandler.handleChangeShippingAddress";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SHIP_TO_MULTI_ADDRESS. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SHIP_TO_MULTI_ADDRESS = "TRUShippingGroupFormHandler.handleShipToMultiAddress";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SELECT_EXISTING_ADDRESS. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SELECT_EXISTING_ADDRESS = "TRUShippingGroupFormHandler.handleSelectExistingAddress";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_APPLY_SINGLE_SHIPPING. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_APPLY_SINGLE_SHIPPING = "TRUShippingGroupFormHandler.handleApplySingleShipping";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SELECT_SHIP_METHOD. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SELECT_SHIP_METHOD = "TRUShippingGroupFormHandler.handleSelectShipMethod";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CHANGE_SHIP_METHOD_IN_REVIEW. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CHANGE_SHIP_METHOD_IN_REVIEW = "TRUShippingGroupFormHandler.handleChangeShipMethodInReview";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CHANGE_SHIP_METHOD. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CHANGE_SHIP_METHOD = "TRUShippingGroupFormHandler.handleChangeShipMethod";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SET_AS_DEFAULT_SHIPPING_ADDRESS. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SET_AS_DEFAULT_SHIPPING_ADDRESS = "TRUShippingGroupFormHandler.handleSetAsDefaultShippingAddress";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS = "TRUShippingGroupFormHandler.handleEditShippingAddress";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_REMOVE_SHIPPING_ADDRESS. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_REMOVE_SHIPPING_ADDRESS = "TRUShippingGroupFormHandler.handleRemoveShippingAddress";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS = "TRUShippingGroupFormHandler.handleAddShippingAddress";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SHIP_TO_EXISTING_ADDRESS. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SHIP_TO_EXISTING_ADDRESS = "TRUShippingGroupFormHandler.handleShipToExistingAddress";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SHIP_TO_NEW_ADDRESS. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SHIP_TO_NEW_ADDRESS = "TRUShippingGroupFormHandler.handleShipToNewAddress";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_UPDATE_RELATIONSHIPS. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_UPDATE_RELATIONSHIPS = "TRUShippingGroupFormHandler.handleUpdateRelationShips";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_INIT_REGISTRY_SHIPPING_GROUPS. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_INIT_REGISTRY_SHIPPING_GROUPS = "TRUShippingGroupFormHandler.handleInitRegistryShippingGroups";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_INIT_SINGLE_SHIPPING_GROUP. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_INIT_SINGLE_SHIPPING_GROUP = "TRUShippingGroupFormHandler.handleInitSingleShippingGroup";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_INIT_REGISTRY_SHIPPING_GROUPS. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_APPLAY_SHIPPING_GROUP_RULES = "TRUShippingGroupFormHandler.handleApplyShippingGroupRules";
	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_INIT_REGISTRY_SHIPPING_GROUPS. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_APPLEPAY_CALCULATE_ORDER = "TRUShippingGroupFormHandler.handleCalculateOrderWithApplePay";


	/**
	 * Instance variable to hold default shipping address nick name.
	 */
	private String mDefaultShippingAddressNickName;

	/**
	 * Instance variable to hold remove shipping address nick name.
	 */
	private String mRemoveShippingAddressNickName;


	/**
	 * property: shipping address.
	 */
	private ContactInfo mAddress = new ContactInfo();

	/**
	 * List to hold ErrorKeys.
	 */
	private List<String> mErrorKeysList;

	/**
	 * List to hold PromoStatusVO.
	 */
	private List<PromoStatusVO> mPromoStatusVO;

	/**
	 * property to hold PropertyManager.
	 */
	private TRUPropertyManager mPropertyManager;
	
	/**
	 * property to hold TRUCartItemDetailVO.
	 */
	private TRUCartItemDetailVO mCartItemDetailObj;
	
	/**
	 * @return the promoStatusVO
	 */
	public List<PromoStatusVO> getPromoStatusVO() {
		return mPromoStatusVO;
	}

	/**
	 * @param pPromoStatusVO
	 *            the promoStatusVO to set
	 */
	public void setPromoStatusVO(List<PromoStatusVO> pPromoStatusVO) {
		mPromoStatusVO = pPromoStatusVO;
	}

	/** The Constant PLEASE_TRY_AGAIN. */
	private static final String PLEASE_TRY_AGAIN = "Please Try again";

	/** Common log-1. */
	private static final String COMMON_LOG_1 ="Redirecting due to form error in runProcessValidateShippingGroups";

	/**
	 * property: Reference to the ShippingProcessHelper component.
	 */
	private TRUShippingProcessHelper mShippingHelper;

	/** mShippingManager. */
	private TRUShippingManager mShippingManager;

	/**
	 * property: flag indicating whether billing address will be same as
	 * shipping address.
	 */
	private boolean mBillingAddressSameAsShippingAddress;

	/**
	 * mShippingPricingEngine.
	 */
	private TRUShippingPricingEngine mShippingPricingEngine;

	/**
	 * mShipMethodVO.
	 */
	private TRUShipMethodVO mShipMethodVO=new TRUShipMethodVO();
	/**
	 *  property to hold integrationStatusUtil.
	 */
	private TRUIntegrationStatusUtil mIntegrationStatusUtil;

	/** The m ship to new address error url. */
	private String mShipToNewAddressErrorURL;

	/**
	 * holds relation ship id.
	 */
	private String mRelationShipId;


	/**
	 * property: Nickname for new shipping address.
	 */
	private String mNewShipToAddressName;

	/**
	 * mShipToNewAddressSuccessURL.
	 */
	private String mShipToNewAddressSuccessURL;

	/** mErrorHandler. */
	private IErrorHandler mErrorHandler;


	/** The m ve. */
	private ValidationExceptions mVe = new ValidationExceptions();

	/**
	 * mValidator.
	 */
	private IValidator mValidator;

	/**
	 * property: flag indicating whether gift options page will be displayed.
	 */
	private boolean mShowMeGiftingOptions;

	/**
	 * Instance variable to hold mRestService.
	 */
	private boolean mRestService;

	/**
	 * property to hold user session component.
	 */
	private TRUUserSession mUserSession;

	/**
	 * property: shipping method to use for current order.
	 */
	private String mShippingMethod;

	/**
	 * Instance variable to hold shipping price.
	 */
	private double mShippingPrice;

	/**
	 * property: flag indicating whether shipping will be performed to the new
	 * address or not.
	 */
	private boolean mShipToNewAddress;

	/**
	 * property: flag indicating to check for ship to address name.
	 */
	private String mShipToAddressName;

	/**
	 * property to hold tRUConfiguration.
	 */
	private TRUConfiguration mTRUConfiguration;

	/**
	 * Property to hold Address doctor response VO.
	 */
	private TRUAddressDoctorResponseVO mAddressDoctorResponseVO;

	/**
	 * boolean property to hold if address is validated by address doctor ot not.
	 */
	private String mAddressValidated;

	/**
	 * Property to hold address doctor response status.
	 */
	private String mAddressDoctorProcessStatus;

	/**
	 * property to hold mCommonSuccessURL.
	 */
	private String mCommonSuccessURL;
	/**
	 * property to hold mCommonErrorURL.
	 */
	private String mCommonErrorURL;

	/** property to hold Navigate To Multi Ship.  */
	private boolean mNavigateToMultiShip;

	/** property to hold Is Empty Address.  */
	private boolean mEmptyShippingAddress;

	/** The Unique id. */
	private String mUniqueId;

	/** The Gift wrap sku id. */
	private String mGiftWrapSkuId;
	/**
	 * Instance variable to hold mapplePayBuyNow.
	 */
	private boolean mApplePayBuyNow;

	/**
	 * @return the applePayBuyNow
	 */
	public boolean isApplePayBuyNow() {
		return mApplePayBuyNow;
	}

	/**
	 * @param pApplePayBuyNow the applePayBuyNow to set
	 */
	public void setApplePayBuyNow(boolean pApplePayBuyNow) {
		mApplePayBuyNow = pApplePayBuyNow;
	}

	/** The m gifting process helper. */
	private TRUGiftingPurchaseProcessHelper mGiftingProcessHelper;


	/** Property To Hold Wrap Commerce Item Id. */
	private String mWrapCommerceItemId;


	/**
	 * Gets the shipping manager.
	 *
	 * @return the shippingManager.
	 */
	public TRUShippingManager getShippingManager() {
		return mShippingManager;
	}

	/**
	 * Sets the shipping manager.
	 *
	 * @param pShippingManager the shippingManager to set.
	 */
	public void setShippingManager(TRUShippingManager pShippingManager) {
		mShippingManager = pShippingManager;
	}

	/**
	 * Gets the shipping helper.
	 *
	 * @return the shippingHelper.
	 */
	public TRUShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * Sets the shipping helper.
	 *
	 * @param pShippingHelper the shippingHelper to set.
	 */
	public void setShippingHelper(TRUShippingProcessHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}



	/**
	 * Gets the relation ship id.
	 *
	 * @return the relationShipId
	 */
	public String getRelationShipId() {
		return mRelationShipId;
	}

	/**
	 * Sets the relation ship id.
	 *
	 * @param pRelationShipId the relationShipId to set.
	 */
	public void setRelationShipId(String pRelationShipId) {
		mRelationShipId = pRelationShipId;
	}

	/**
	 * Gets the ship to new address error url.
	 *
	 * @return the ship to new address error URL.
	 */
	public String getShipToNewAddressErrorURL() {
		return mShipToNewAddressErrorURL;
	}

	/**
	 * Sets the ship to new address error url.
	 *
	 * @param pShipToNewAddressErrorURL - the ship to new address error URL.
	 */
	public void setShipToNewAddressErrorURL(String pShipToNewAddressErrorURL) {
		mShipToNewAddressErrorURL = pShipToNewAddressErrorURL;
	}

	/**
	 * Gets the ship method vo.
	 *
	 * @return the shipMethodVO
	 */
	public TRUShipMethodVO getShipMethodVO() {
		return mShipMethodVO;
	}

	/**
	 * Sets the ship method vo.
	 *
	 * @param pShipMethodVO - the shipMethodVO to set.
	 */
	public void setShipMethodVO(TRUShipMethodVO pShipMethodVO) {
		mShipMethodVO = pShipMethodVO;
	}

	/**
	 * Gets the shipping price.
	 *
	 * @return the shippingPrice
	 */
	public double getShippingPrice() {
		return mShippingPrice;
	}

	/**
	 * Sets the shipping price.
	 *
	 * @param pShippingPrice the shippingPrice to set.
	 */
	public void setShippingPrice(double pShippingPrice) {
		mShippingPrice = pShippingPrice;
	}




	/**
	 * Checks if is rest service.
	 *
	 * @return the mRestService.
	 */
	public boolean isRestService() {
		return mRestService;
	}

	/**
	 * Sets the rest service.
	 *
	 * @param pRestService the mRestService to set.
	 */
	public void setRestService(boolean pRestService) {
		mRestService = pRestService;
	}

	/**
	 * getValidator().
	 *
	 * @return the validator.
	 */
	public IValidator getValidator() {
		return mValidator;
	}

	/**
	 * Sets the validator.
	 *
	 * @param pValidator - the validator to set.
	 */
	public void setValidator(IValidator pValidator) {
		mValidator = pValidator;
	}

	/**
	 * Gets the ship to new address success url.
	 *
	 * @return the ship to new address success URL.
	 */
	public String getShipToNewAddressSuccessURL() {
		return mShipToNewAddressSuccessURL;
	}

	/**
	 * Sets the ship to new address success url.
	 *
	 * @param pShipToNewAddressSuccessURL - the ship to new address success URL.
	 */
	public void setShipToNewAddressSuccessURL(String pShipToNewAddressSuccessURL) {
		mShipToNewAddressSuccessURL = pShipToNewAddressSuccessURL;
	}

	/**
	 * Checks if is ship to new address.
	 *
	 * @return true if shipping will be performed to the new address, false -
	 * otherwise.
	 */
	public boolean isShipToNewAddress() {
		return mShipToNewAddress;
	}

	/**
	 * Sets the ship to new address.
	 *
	 * @param pShipToNewAddress - true if shipping will be performed to the new address,
	 * otherwise false.
	 */
	public void setShipToNewAddress(boolean pShipToNewAddress) {
		mShipToNewAddress = pShipToNewAddress;
	}



	/**
	 * Checks if is billing address same as shipping address.
	 *
	 * @return the billingAddressSameAsShippingAddress.
	 */
	public boolean isBillingAddressSameAsShippingAddress() {
		return mBillingAddressSameAsShippingAddress;
	}

	/**
	 * Sets the billing address same as shipping address.
	 *
	 * @param pBillingAddressSameAsShippingAddress the billingAddressSameAsShippingAddress to set.
	 */
	public void setBillingAddressSameAsShippingAddress(
			boolean pBillingAddressSameAsShippingAddress) {
		mBillingAddressSameAsShippingAddress = pBillingAddressSameAsShippingAddress;
	}



	/**
	 * Checks if is show me gifting options.
	 *
	 * @return the showMeGiftingOptions.
	 */
	public boolean isShowMeGiftingOptions() {
		return mShowMeGiftingOptions;
	}

	/**
	 * Sets the show me gifting options.
	 *
	 * @param pShowMeGiftingOptions            the showMeGiftingOptions to set.
	 */
	public void setShowMeGiftingOptions(boolean pShowMeGiftingOptions) {
		mShowMeGiftingOptions = pShowMeGiftingOptions;
	}



	/**
	 * Gets the ship to address name.
	 *
	 * @return the shipToAddressName.
	 */
	public String getShipToAddressName() {
		return mShipToAddressName;
	}

	/**
	 * Sets the ship to address name.
	 *
	 * @param pShipToAddressName            the shipToAddressName to set.
	 */
	public void setShipToAddressName(String pShipToAddressName) {
		mShipToAddressName = pShipToAddressName;
	}


	/**
	 * Gets the address.
	 *
	 * @return the address.
	 */
	public ContactInfo getAddress() {
		return mAddress;
	}

	/**
	 * Sets the address.
	 *
	 * @param pAddress            - the address to set.
	 */
	public void setAddress(ContactInfo pAddress) {
		mAddress = pAddress;
	}



	/**
	 * Gets the new ship to address name.
	 *
	 * @return new shipping address.
	 */
	public String getNewShipToAddressName() {
		return mNewShipToAddressName;
	}

	/**
	 * Sets the new ship to address name.
	 *
	 * @param pNewShipToAddressName            - new shipping address.
	 */
	public void setNewShipToAddressName(String pNewShipToAddressName) {
		mNewShipToAddressName = pNewShipToAddressName;

		if (mNewShipToAddressName != null) {
			mNewShipToAddressName = mNewShipToAddressName.trim();
		}
	}



	/**
	 * Gets the shipping method.
	 *
	 * @return the shipping method.
	 */
	public String getShippingMethod() {
		return mShippingMethod;
	}

	/**
	 * Sets the shipping method.
	 *
	 * @param pShippingMethod            - the shipping method to set.
	 */
	public void setShippingMethod(String pShippingMethod) {
		mShippingMethod = pShippingMethod;
	}



	/**
	 * Gets the user session.
	 *
	 * @return the userSession
	 */
	public TRUUserSession getUserSession() {
		return mUserSession;
	}

	/**
	 * Sets the user session.
	 *
	 * @param pUserSession            the userSession to set.
	 */
	public void setUserSession(TRUUserSession pUserSession) {
		mUserSession = pUserSession;
	}



	/**
	 * Gets the wrap commerce item id.
	 *
	 * @return the wrapCommerceItemId.
	 */
	public String getWrapCommerceItemId() {
		return mWrapCommerceItemId;
	}

	/**
	 * Sets the wrap commerce item id.
	 *
	 * @param pWrapCommerceItemId the wrapCommerceItemId to set.
	 */
	public void setWrapCommerceItemId(String pWrapCommerceItemId) {
		mWrapCommerceItemId = pWrapCommerceItemId;
	}

	/**
	 * Gets the gift wrap sku id.
	 *
	 * @return the giftWrapSkuId.
	 */
	public String getGiftWrapSkuId() {
		return mGiftWrapSkuId;
	}

	/**
	 * Sets the gift wrap sku id.
	 *
	 * @param pGiftWrapSkuId the giftWrapSkuId to set.
	 */
	public void setGiftWrapSkuId(String pGiftWrapSkuId) {
		mGiftWrapSkuId = pGiftWrapSkuId;
	}


	/**
	 * Gets the gifting process helper.
	 *
	 * @return the giftingProcessHelper.
	 */
	public TRUGiftingPurchaseProcessHelper getGiftingProcessHelper() {
		return mGiftingProcessHelper;
	}

	/**
	 * Sets the gifting process helper.
	 *
	 * @param pGiftingProcessHelper the giftingProcessHelper to set.
	 */
	public void setGiftingProcessHelper(
			TRUGiftingPurchaseProcessHelper pGiftingProcessHelper) {
		mGiftingProcessHelper = pGiftingProcessHelper;
	}

	/**
	 * Gets the unique id.
	 *
	 * @return the uniqueId.
	 */
	public String getUniqueId() {
		return mUniqueId;
	}

	/**
	 * Sets the unique id.
	 *
	 * @param pUniqueId the uniqueId to set.
	 */
	public void setUniqueId(String pUniqueId) {
		mUniqueId = pUniqueId;
	}

	/**
	 * Checks if is empty shipping address.
	 *
	 * @return the emptyShippingAddress.
	 */
	public boolean isEmptyShippingAddress() {
		return mEmptyShippingAddress;
	}

	/**
	 * Sets the empty shipping address.
	 *
	 * @param pEmptyShippingAddress the emptyShippingAddress to set.
	 */
	public void setEmptyShippingAddress(boolean pEmptyShippingAddress) {
		mEmptyShippingAddress = pEmptyShippingAddress;
	}

	/**
	 * Checks if is navigate to multi ship.
	 *
	 * @return the mNavigateToMultiShip.
	 */
	public boolean isNavigateToMultiShip() {
		return mNavigateToMultiShip;
	}

	/**
	 * Sets the navigate to multi ship.
	 *
	 * @param pNavigateToMultiShip the mNavigateToMultiShip to set.
	 */
	public void setNavigateToMultiShip(boolean pNavigateToMultiShip) {
		mNavigateToMultiShip = pNavigateToMultiShip;
	}

	/**
	 * Gets the TRU configuration.
	 *
	 * @return the tRUConfiguration.
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * Sets the TRU configuration.
	 *
	 * @param pTRUConfiguration            the tRUConfiguration to set.
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		mTRUConfiguration = pTRUConfiguration;
	}

	/**
	 * Gets the address doctor response vo.
	 *
	 * @return the TRUAddressDoctorResponseVO.
	 */
	public TRUAddressDoctorResponseVO getAddressDoctorResponseVO() {
		return mAddressDoctorResponseVO;
	}

	/**
	 * Sets the address doctor response vo.
	 *
	 * @param pAddressDoctorResponseVO the addressDoctorResponseVO to set.
	 */
	public void setAddressDoctorResponseVO(
			TRUAddressDoctorResponseVO pAddressDoctorResponseVO) {
		mAddressDoctorResponseVO = pAddressDoctorResponseVO;
	}

	/**
	 * Gets the address doctor process status.
	 *
	 * @return addressDoctorProcessStatus.
	 */
	public String getAddressDoctorProcessStatus() {
		return mAddressDoctorProcessStatus;
	}

	/**
	 * Sets the address doctor process status.
	 *
	 * @param pAddressDoctorProcessStatus the addressDoctorProcessStatus to set.
	 */
	public void setAddressDoctorProcessStatus(String pAddressDoctorProcessStatus) {
		mAddressDoctorProcessStatus = pAddressDoctorProcessStatus;
	}

	/**
	 * mAddressValidated.
	 * @return addressValidated.
	 */
	public String getAddressValidated() {
		return mAddressValidated;
	}

	/**
	 * mAddressValidated.
	 * @param pAddressValidated the addressValidated to set
	 */
	public void setAddressValidated(String pAddressValidated) {
		mAddressValidated = pAddressValidated;
	}

	/**
	 * Gets the common success url.
	 *
	 * @return the commonSuccessURL
	 */
	public String getCommonSuccessURL() {
		return mCommonSuccessURL;
	}

	/**
	 * Sets the common success url.
	 *
	 * @param pCommonSuccessURL the commonSuccessURL to set
	 */
	public void setCommonSuccessURL(String pCommonSuccessURL) {
		mCommonSuccessURL = pCommonSuccessURL;
	}

	/**
	 * Gets the common error url.
	 *
	 * @return the commonErrorURL
	 */
	public String getCommonErrorURL() {
		return mCommonErrorURL;
	}

	/**
	 * Sets the common error url.
	 *
	 * @param pCommonErrorURL the commonErrorURL to set
	 */
	public void setCommonErrorURL(String pCommonErrorURL) {
		mCommonErrorURL = pCommonErrorURL;
	}



	/**
	 * Gets the default shipping address nick name.
	 *
	 * @return the defaultShippingAddressNickName
	 */
	public String getDefaultShippingAddressNickName() {
		return mDefaultShippingAddressNickName;
	}

	/**
	 * Sets the default shipping address nick name.
	 *
	 * @param pDefaultShippingAddressNickName the defaultShippingAddressNickName to set
	 */
	public void setDefaultShippingAddressNickName(
			String pDefaultShippingAddressNickName) {
		mDefaultShippingAddressNickName = pDefaultShippingAddressNickName;
	}



	/**
	 * Gets the removes the shipping address nick name.
	 *
	 * @return the removeShippingAddressNickName
	 */
	public String getRemoveShippingAddressNickName() {
		return mRemoveShippingAddressNickName;
	}

	/**
	 * Sets the removes the shipping address nick name.
	 *
	 * @param pRemoveShippingAddressNickName            the removeShippingAddressNickName to set.
	 */
	public void setRemoveShippingAddressNickName(
			String pRemoveShippingAddressNickName) {
		mRemoveShippingAddressNickName = pRemoveShippingAddressNickName;
	}

	/**
	 * Gets the property manager.
	 *
	 * @return the mPropertyManager.
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 *
	 * @param pPropertyManager the mPropertyManager to set.
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * property to hold TRUStoreUtils.
	 */
	private TRUStoreUtils mStoreUtils;


	/**
	 * Gets the TRU Store Util.
	 * 
	 * @return the TRUStoreUtils
	 */
	public TRUStoreUtils getStoreUtils() {
		return mStoreUtils;
	}

	/**
	 * Sets the TRU Store Util.
	 * 
	 * @param pStoreUtils
	 *            the TRUStoreUtils to set
	 */
	public void setStoreUtils(TRUStoreUtils pStoreUtils) {
		mStoreUtils = pStoreUtils;
	}
	/**
	 * property to hold ApplePayOrderSummaryVO.
	 */
	private ApplePayOrderSummaryVO mApplePayOrderSummary;

	/**
	 * @return the applePayOrderSummary
	 */
	public ApplePayOrderSummaryVO getApplePayOrderSummary() {
		return mApplePayOrderSummary;
	}

	/**
	 * @param pApplePayOrderSummary the applePayOrderSummary to set
	 */
	public void setApplePayOrderSummary(ApplePayOrderSummaryVO pApplePayOrderSummary) {
		mApplePayOrderSummary = pApplePayOrderSummary;
	}
	/**
	 * Property to hold validShipMethods.
	 */
	private List<TRUShipMethodVO> mValidShipMethods;

	/**
	 * @return the validShipMethods
	 */
	public List<TRUShipMethodVO> getValidShipMethods() {
		return mValidShipMethods;
	}
	/**
	 * Property to hold selectedShippingMethod.
	 */
	private String mSelectedShippingMethod;

	/**
	 * @return the selectedShippingMethod
	 */
	public String getSelectedShippingMethod() {
		return mSelectedShippingMethod;
	}

	/**
	 * @param pSelectedShippingMethod the selectedShippingMethod to set
	 */
	public void setSelectedShippingMethod(String pSelectedShippingMethod) {
		mSelectedShippingMethod = pSelectedShippingMethod;
	}

	/**
	 * @param pValidShipMethods the validShipMethods to set
	 */
	public void setValidShipMethods(List<TRUShipMethodVO> pValidShipMethods) {
		mValidShipMethods = pValidShipMethods;
	}

	/**
	 * initialize with default shipping address.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleInitSingleShippingGroup(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_INIT_SINGLE_SHIPPING_GROUP)) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					final TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) getShippingGroupMapContainer();
					if (TRUConstants.SINGLE_SHIPPING.equalsIgnoreCase(getUserSession().getShippingMode())) {

						// set to null if nickname is not in container
						String selectedAddressNickName = shippingGroupMapContainer.getSelectedAddressNickName();
						if (!isNickNameInContainer(selectedAddressNickName)) {
							selectedAddressNickName = null;
						}

						if (StringUtils.isBlank(selectedAddressNickName)) {
							final TRUHardgoodShippingGroup hardgoodShippingGroup = getShippingHelper().getPurchaseProcessHelper().getFirstHardgoodShippingGroup(
									getOrder());
							if (hardgoodShippingGroup == null ||
									StringUtils.isBlank(hardgoodShippingGroup.getNickName())) {

								final String nickName = getShippingHelper().findDefaultShippingAddress(shippingGroupMapContainer);
								if (StringUtils.isNotBlank(nickName)) {
									shippingGroupMapContainer.setSelectedAddressNickName(nickName);
								}
							} else {
								shippingGroupMapContainer.setSelectedAddressNickName(hardgoodShippingGroup.getNickName());
							}
						}
					}
					getOrderManager().updateOrder(getOrder());
				}
			} catch (CommerceException e) {
				if (isLoggingError()) {
					logError(
							"CommerceException in TRUShippingGroupFormHandler.handleInitSingleShippingGroup",
							e);
				}
			}   finally {
				if (tr != null)
				{
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_INIT_SINGLE_SHIPPING_GROUP);
				}
			}
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * Finds for the empty address channel hard good shipping groups.
	 * Invokes the service for getting registrant order.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleInitRegistryShippingGroups(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean hasException = false;
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_INIT_REGISTRY_SHIPPING_GROUPS)) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					preInitRegistryShippingGroups(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in handleInitRegistryShippingGroups() method");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					initRegistryShippingGroups(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in handleInitRegistryShippingGroups() method");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					//Start :: Condition added for no of taxware calls.
					if(((TRUOrderManager)getOrderManager()).isOrderContainRegistryItems((TRUOrderImpl)getOrder())){
						runProcessRepriceOrder(
							getOrder(), getUserPricingModels(), getLocale(), getProfile(), createRepriceParameterMap());
					}
					//End :: Commented added for no of taxware calls.
					postInitRegistryShippingGroups(pRequest, pResponse);
					getOrderManager().updateOrder(getOrder());
				}
			} catch (CommerceException e) {
				hasException = true;
				if (isLoggingError()) {
					logError(
							"CommerceException in TRUShippingGroupFormHandler.handleInitRegistryShippingGroups",
							e);
				}
			} catch (RunProcessException e) {
				hasException = true;
				if (isLoggingError()) {
					logError(
							"CommerceException in TRUShippingGroupFormHandler.handleInitRegistryShippingGroups",
							e);
				}
			} finally {
				if (hasException) {
					try {
						setTransactionToRollbackOnly();
					} catch (SystemException e) {
						if (isLoggingError()) {
							logError("SystemException in TRUShippingGroupFormHandler.handleChangeShippingAddress", e);
						}
					}
				}
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_INIT_REGISTRY_SHIPPING_GROUPS);
				}
			}
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * Pre init registry shipping groups.
	 *
	 * @param pRequest            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException                if an error occurs.
	 * @exception IOException                if an error occurs.
	 */
	protected void preInitRegistryShippingGroups(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if(isLoggingDebug()){
			vlogDebug("START of TRUShippingGroupFormHandler.preInitRegistryShippingGroups method");
		}

		if(isLoggingDebug()){
			vlogDebug("END of TRUShippingGroupFormHandler.preInitRegistryShippingGroups method");
		}

	}

	/**
	 * Finds for the empty address channel hard good shipping groups.
	 * Invokes the service for getting registrant order.
	 *
	 * @param pRequest            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException                if an error occurs.
	 * @exception IOException                if an error occurs.
	 * @throws CommerceException the commerce exception
	 */
	protected void initRegistryShippingGroups(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUShippingGroupFormHandler.initRegistryShippingGroups method");
		}
		final TRUOrderImpl order = (TRUOrderImpl) getOrder();
		final TRUShippingGroupManager shippingGroupManager = (TRUShippingGroupManager) getShippingGroupManager();
		final TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) getShippingGroupMapContainer();
		final TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
		// holds all order relationship having shipping group has TRUChannelHardgoodShippingGroup
		final List<TRUShippingGroupCommerceItemRelationship> channelHgsgCiRels=
				((TRUShippingGroupManager)getShippingGroupManager()).getOnlyChannelHardGoodShippingGroupRelationships(order);

		for (TRUShippingGroupCommerceItemRelationship channelHgsgCiRel : channelHgsgCiRels) {
			final CommerceItem commerceItem = channelHgsgCiRel.getCommerceItem();
			final TRUChannelHardgoodShippingGroup orderShippingGroup = (TRUChannelHardgoodShippingGroup) channelHgsgCiRel.getShippingGroup();
			String nickName1=orderShippingGroup.getNickName();
			// add registry shipping group to order if not set
			if (orderShippingGroup != null &&
					!shippingGroupMapContainer.getShippingGroupMap().keySet().contains(nickName1)) {
				// construct channel details vo to find in container.
				final TRUChannelDetailsVo channelDetailsVo = getShippingHelper().getPurchaseProcessHelper().createChannelDetailVO(orderShippingGroup);
				// find shipping groups with channel details vo.
				List<TRUChannelHardgoodShippingGroup> containerShippingGroups = getShippingHelper().
						findChannelHardgoodShippingGroups(shippingGroupMapContainer, channelDetailsVo);
				if (containerShippingGroups != null && !containerShippingGroups.isEmpty()) {
					for (TRUChannelHardgoodShippingGroup containerShippingGroup : containerShippingGroups) {
						if (containerShippingGroup != null &&
								StringUtils.isNotBlank(containerShippingGroup.getShippingAddress().getAddress1())) {

							final String nickName = containerShippingGroup.getNickName();
							final String shippingMethod = orderShippingGroup.getShippingMethod();
							// find order has same shipping group by nick name and shipping method.
							final TRUHardgoodShippingGroup matchedShippingGroup = orderManager.getShipGroupByNickNameAndShipMethod(
									getOrder(), nickName, shippingMethod);
							if (matchedShippingGroup != null) {
								// If relationship found, remove relationship from current shipping group
								getOrderManager().getCommerceItemManager().removeItemQuantityFromShippingGroup(getOrder(),
										commerceItem.getId(), orderShippingGroup.getId(), channelHgsgCiRel.getQuantity());
								// adding item quantity to matched shipping group
								getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(),
										commerceItem.getId(), matchedShippingGroup.getId(), channelHgsgCiRel.getQuantity());
							} else {
								// new shipping group and update required details.
								orderShippingGroup.setNickName(containerShippingGroup.getNickName());
								OrderTools.copyAddress(
										containerShippingGroup.getShippingAddress(), orderShippingGroup.getShippingAddress());
								getShippingHelper().setShipMethodAndPrice(getOrder(), commerceItem, orderShippingGroup,orderShippingGroup.getShippingMethod());
								orderShippingGroup.setRegistryAddress(Boolean.TRUE);
							}
							break;
						}
					}
				}
			} else if(StringUtils.isNotBlank(orderShippingGroup.getNickName())) {
				TRUHardgoodShippingGroup matchedContainerSG = (TRUHardgoodShippingGroup) shippingGroupMapContainer.getShippingGroupMap().
						get(orderShippingGroup.getNickName());
				if(matchedContainerSG != null) {
					OrderTools.copyAddress(
							matchedContainerSG.getShippingAddress(), orderShippingGroup.getShippingAddress());
				}
			}
		}
		shippingGroupManager.removeEmptyShippingGroups(order);

		if (isLoggingDebug()) {
			vlogDebug("End of TRUShippingGroupFormHandler.initRegistryShippingGroups method");
		}

	}



	/**
	 * Post init registry shipping groups.
	 *
	 * @param pRequest            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException                if an error occurs.
	 * @exception IOException                if an error occurs.
	 */
	protected void postInitRegistryShippingGroups(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if(isLoggingDebug()){
			vlogDebug("START of TRUShippingGroupFormHandler.postInitRegistryShippingGroups method");
		}
		if(isLoggingDebug()){
			vlogDebug("END of TRUShippingGroupFormHandler.postInitRegistryShippingGroups method");
		}
	}

	/**
	 * checks for the given nick name in container.
	 *
	 * @param pNickName nickname
	 * @return returns true if nickname is in container
	 */
	@SuppressWarnings("unchecked")
	private boolean isNickNameInContainer(String pNickName) {
		TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) getShippingGroupMapContainer();

		if (StringUtils.isNotBlank(pNickName)) {
			final Map<String, ShippingGroup> shippingGroups = shippingGroupMapContainer.getShippingGroupMapForDisplay();
			if (shippingGroups!= null && !shippingGroups.isEmpty()) {
				final Set<String> nickNames = shippingGroups.keySet();
				return nickNames.contains(pNickName);
			}
		}
		return false;
	}

	/**
	 * Handle 'Ship to new address' case.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * 
	 * @return redirection result.
	 * 
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	public boolean handleShipToNewAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.handleShipToNewAddress method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean hasException = false;
		TRUOrderImpl orderImpl= (TRUOrderImpl)getOrder();
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SHIP_TO_NEW_ADDRESS)) {
			Transaction tr = null;
			boolean isEnableAddressDoctor = false;
			try {
				 if (PerformanceMonitor.isEnabled()){
						PerformanceMonitor.startOperation(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SHIP_TO_NEW_ADDRESS);
					}
				tr = ensureTransaction();
				synchronized (getOrder()) {
					preShipToNewAddress(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in field validation() method");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					// Navigating to Multiple Shipping with empty shipping address
					if (isNavigateToMultiShip() && isEmptyShippingAddress()) {
						orderImpl.setOrderIsInMultiShip(Boolean.TRUE);
						setOrderReconciliationNeeded(isOrderReconciliationRequired());
						applyMultiShipping();						
						orderImpl.getActiveTabs().put(TRUCheckoutConstants.SHIPPING_TAB, Boolean.TRUE);
						((TRUPaymentGroupContainerService) getPaymentGroupMapContainer())
						.setBillingAddressSameAsShippingAddress(isBillingAddressSameAsShippingAddress());
						runProcessRepriceOrder(getOrder(), getUserPricingModels(), getLocale(), getProfile(),
								createRepriceParameterMap());
						getOrderManager().updateOrder(getOrder());
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					} else {
						orderImpl.setApplyGiftingOptions(Boolean.FALSE);
					}
				}
				// flag to check is address already validated
				boolean isAddressValidated = Boolean.parseBoolean(getAddressValidated());
				// Validate address with AddressDoctor Service
				if (SiteContextManager.getCurrentSite() != null) {
					/*
					 * Object value=SiteContextManager.getCurrentSite().getPropertyValue(getPropertyManager().
					 * getEnableAddressDoctorPropertyName());
					 */
					final Object value = getIntegrationStatusUtil().getAddressDoctorIntStatus(pRequest);
					if (null != value) {
						isEnableAddressDoctor = (boolean) value;
					}
				}
				if (!isAddressValidated && isEnableAddressDoctor) {
					final String addressStatus = ((TRUPurchaseProcessHelper) getPurchaseProcessHelper())
							.validateAddress(getAddress(), getOrder(), getProfile(), TRUConstants.SHIPPING,TRUConstants.ADD_ADDRESS);
					TRUAddressDoctorResponseVO addressResponseVo = ((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).getProfileTools().getAddressDoctorResponseVO();
					setAddressDoctorProcessStatus(addressStatus);
					setAddressDoctorResponseVO(addressResponseVo);
					if (TRUConstants.ADDRESS_DOCTOR_STATUS_VERIFIED.equalsIgnoreCase(addressStatus)) {
						isAddressValidated = Boolean.TRUE;
					}
					else if(TRUConstants.ADDRESS_DOCTOR_STATUS_CORRECTED.equalsIgnoreCase(addressStatus)){
						//addressResponseVo.setDerivedStatus("NO");
						String derivedStatus = addressResponseVo.getDerivedStatus();
						if(derivedStatus.equalsIgnoreCase(TRUConstants.NO)){
							isAddressValidated = Boolean.TRUE;
							ContactInfo contactInfo = getAddress();
							if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getAddress1())){
								contactInfo.setAddress1(addressResponseVo.getAddressDoctorVO().get(0).getAddress1());}
							if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getAddress2())){
								contactInfo.setAddress2(addressResponseVo.getAddressDoctorVO().get(0).getAddress2());}
							if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getCity())){
								contactInfo.setCity(addressResponseVo.getAddressDoctorVO().get(0).getCity());}
							if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getState())){
								contactInfo.setState(addressResponseVo.getAddressDoctorVO().get(0).getState());}
							if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getCountry())){
								contactInfo.setCountry(TRUConstants.UNITED_STATES);
							}
							if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getPostalCode())){
								contactInfo.setPostalCode(addressResponseVo.getAddressDoctorVO().get(0).getPostalCode());
							}
							setAddress(contactInfo);
						}
					}
					else if (TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR.equalsIgnoreCase(addressStatus)) {
						setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR);
						TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
						addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
						addressDoctorResponseVO.setDerivedStatus(addressResponseVo.getDerivedStatus());
						setAddressDoctorResponseVO(addressDoctorResponseVO);

						// isAddressValidated = Boolean.TRUE;
					} else if (addressStatus != null && TRUConstants.CONNECTION_ERROR.equalsIgnoreCase(addressStatus)) {
						setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_CONNECTION_ERROR);
						TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
						addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
						addressDoctorResponseVO.setDerivedStatus(TRUConstants.YES);
						setAddressDoctorResponseVO(addressDoctorResponseVO);
					}
				} else if (!isEnableAddressDoctor && !isAddressValidated) {
					setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR);
					TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
					addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
					addressDoctorResponseVO.setDerivedStatus(TRUConstants.YES);
					setAddressDoctorResponseVO(addressDoctorResponseVO);
				}
				if (isAddressValidated) {
					if (StringUtils.isBlank(getNewShipToAddressName())) {
						String nickName = ((TRUOrderManager)getOrderManager()).
								generateUniqueNickNameForAddress(getAddress(), getShippingGroupMapContainer().getShippingGroupMap());
						if (nickName.length() > TRUConstants.NICKNAME_MAX_LENGTH) {
							nickName = nickName.substring(TRUConstants.NICKNAME_MIN_LENGTH, TRUConstants.NICKNAME_MAX_LENGTH);
						}
						setNewShipToAddressName(nickName);
					}
					shipToNewAddress(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in shipToNewAddress() method");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					if(!isSkipShippingRestrctions()){
						runProcessValidationShippingInfos(pRequest, pResponse);
					}
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug(COMMON_LOG_1);
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}

					// Check for order reconciliation

					setOrderReconciliationNeeded(isOrderReconciliationRequired());
					if (isLoggingDebug()) {
						vlogDebug("isOrderReconciliationNeeded() ? {0}", isOrderReconciliationNeeded());
					}
					((TRUPaymentGroupContainerService) getPaymentGroupMapContainer())
					.setBillingAddressSameAsShippingAddress(isBillingAddressSameAsShippingAddress());

					getUserSession().setBillingAddressSameAsShippingAddress(isBillingAddressSameAsShippingAddress());
					/*Start - MVP for R Us card Billing address*/
					if(isBillingAddressSameAsShippingAddress()){
						getUserSession().setTSPBillingAddressMap(getAddress());
					}else if(!getUserSession().isTSPEditBillingAddress()){
						getUserSession().setTSPBillingAddressMap(null);
					}

					/*End - MVP for R Us card Billing address*/
					postShipToNewAddress(pRequest, pResponse);
					if (isNavigateToMultiShip() && !getFormError() || isOrderReconciliationNeeded()) {
						orderImpl.setOrderIsInMultiShip(Boolean.TRUE);
						applyMultiShipping();
						orderImpl.getActiveTabs().put(TRUCheckoutConstants.SHIPPING_TAB, Boolean.TRUE);
					} else{
						getUserSession().setSignInHidden(Boolean.TRUE);
					}
					if (orderImpl.isShowMeGiftingOptions()) {
						orderImpl.getActiveTabs().put(TRUCheckoutConstants.SHIPPING_TAB, Boolean.TRUE);
						orderImpl.getActiveTabs().put(TRUCheckoutConstants.GIFTING_TAB, Boolean.TRUE);
					} else if (!((TRUOrderTools) getOrderManager().getOrderTools()).getAllInStorePickupShippingGroups(getOrder())
							.isEmpty()) {
						orderImpl.getActiveTabs().put(TRUCheckoutConstants.SHIPPING_TAB, Boolean.TRUE);
						orderImpl.getActiveTabs().put(TRUCheckoutConstants.PICKUP_TAB, Boolean.TRUE);
					} else {
						orderImpl.getActiveTabs().put(TRUCheckoutConstants.SHIPPING_TAB, Boolean.TRUE);
						orderImpl.getActiveTabs().put(TRUCheckoutConstants.PAYMENT_TAB, Boolean.TRUE);
					}
					runProcessRepriceOrder(getOrder(), getUserPricingModels(), getLocale(), getProfile(),
							createRepriceParameterMap());
					if(getFormError()){
						hasException = true;
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(),pRequest, pResponse);
					}
					// Added for updating special financing related flags in
					// order if order is eligible
					//((TRUOrderTools) getOrderManager().getOrderTools()).updateOrderIfEligibleForFinancing(getOrder());
					getOrderManager().updateOrder(getOrder());
					// getShippingHelper().getProfileTools().setProfileDefaultAddress(getProfile());
					if(getFormError()){
						hasException = true;
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(),pRequest, pResponse);
					}
				}

			} catch (RunProcessException e) {
				hasException = true;
				addFormException(new DropletException(PLEASE_TRY_AGAIN));
				PerformanceMonitor.cancelOperation(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SHIP_TO_NEW_ADDRESS);
				if (isLoggingError()) {
					logError("RunProcessException in TRUShippingGroupFormHandler.handleShipToNewAddress", e);
				}
			} catch (CommerceException e) {
				hasException = true;
				addFormException(new DropletException(PLEASE_TRY_AGAIN));
				PerformanceMonitor.cancelOperation(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SHIP_TO_NEW_ADDRESS);
				if (isLoggingError()) {
					logError("CommerceException in TRUShippingGroupFormHandler.handleShipToNewAddress", e);
				}
			} finally {
				if (tr != null) {					
					if (hasException && !isOrderReconciliationNeeded()) {
						try {
							setTransactionToRollbackOnly();
						} catch (SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in TRUInStorePickUpFormHandler.handleInitRegistryShippingGroups", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.endOperation(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SHIP_TO_NEW_ADDRESS);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SHIP_TO_NEW_ADDRESS);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.handleShipToNewAddress method");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}






	/**
	 * Run process validation shipping infos.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws RunProcessException RunProcessException
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	private void runProcessValidationShippingInfos(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws RunProcessException, ServletException, IOException {
		final Map<String, Object> extraParams = new HashMap<String, Object>();
		extraParams.put(TRUConstants.TRU_SHIPPING_MANAGER, getShippingManager());
		extraParams.put(TRUConstants.ORDER, getOrder());
		extraParams.put(TRUConstants.TRU_SHIPPING_TOOLS, getShippingManager().getShippingTools());
		runProcessValidateShippingGroups(getOrder(), getUserPricingModels(), getUserLocale(pRequest, pResponse), getProfile(),
				extraParams);
	}

	/**
	 * Run process validate shipping groups.
	 *
	 * @param pOrder order
	 * @param pPricingModels pricingModels
	 * @param pLocale locale
	 * @param pProfile profile
	 * @param pExtraParameters extraParameters
	 * @throws RunProcessException RunProcessException
	 */
	@Override
	@SuppressWarnings("rawtypes")
	protected void runProcessValidateShippingGroups(Order pOrder, PricingModelHolder pPricingModels, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters) throws RunProcessException {
		final PipelineResult result = runProcess(getValidateShippingGroupsChainId(), pOrder, pPricingModels, pLocale, pProfile,
				pExtraParameters);
		processPipelineErrors(result);
	}


	/**
	 * Performs input data validations for new shipping address specified by
	 * shopper.
	 *
	 * @param pRequest a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse a <code>DynamoHttpServletResponse</code> value.
	 * @throws ServletException if an error occurs.
	 * @throws IOException if an error occurs.
	 */
	protected void preShipToNewAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		if (isLoggingDebug()) {
			vlogDebug("Start: TRUShippingGroupFormHandler.preShipToNewAddress()");
		}

		// CR-76 Trim all the input fields.
		trimContactInfoValues(getAddress());

		if (isNavigateToMultiShip() && StringUtils.isBlank(getAddress().getFirstName()) &&
				StringUtils.isBlank(getAddress().getLastName()) && StringUtils.isBlank(getAddress().getAddress1()) &&
				StringUtils.isBlank(getAddress().getCity()) && StringUtils.isBlank(getAddress().getPostalCode()) &&
				StringUtils.isBlank(getAddress().getState()) && StringUtils.isBlank(getAddress().getPhoneNumber())) {
			setEmptyShippingAddress(Boolean.TRUE);
			return;
		}

		if (StringUtils.isBlank(getAddress().getFirstName()) || StringUtils.isBlank(getAddress().getLastName()) ||
				StringUtils.isBlank(getAddress().getAddress1()) || StringUtils.isBlank(getAddress().getCity()) ||
				StringUtils.isBlank(getAddress().getPostalCode()) || StringUtils.isBlank(getAddress().getState()) ||
				StringUtils.isBlank(getAddress().getPhoneNumber()) || StringUtils.isEmpty(getShippingMethod())) {

			setEmptyShippingAddress(Boolean.TRUE);

			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDRESS_MISSING_INFO, null);
			getErrorHandler().processException(mVe, this);
			return;
		}
		if (StringUtils.isEmpty(getShippingMethod())) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_SHIPPING_NO_SHIP_METHOD_SELECTED, null);
			getErrorHandler().processException(mVe, this);
			return;
		}
		if (isRestService() && (StringUtils.isBlank(getNewShipToAddressName()) || StringUtils.isBlank(getShippingMethod()))) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDRESS_MISSING_INFO, null);
			getErrorHandler().processException(mVe, this);
			return;
		}
		if (isRestService() && StringUtils.isNotBlank(getNewShipToAddressName())) {
			boolean invalidNkName =  getStoreUtils().containsSpecialCharacter(getNewShipToAddressName());
			if(invalidNkName) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDR_INVALID_NICKNAME, null);
				getErrorHandler().processException(mVe, this);
				return;
			}
		}
		if (isRestService() && getShippingGroupMapContainer().getShippingGroup(getNewShipToAddressName()) != null) {
			if (getErrorKeysList().contains(TRUErrorKeys.TRU_ERROR_ACCOUNT_NICKNAME_EXISTS)) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_NICKNAME_EXISTS, null);
				getErrorHandler().processException(mVe, this);
			} else {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_DUPLICATE_ADDR_NICKNAME, null);
				getErrorHandler().processException(mVe, this);
			}
			return;
		}
		if (isLoggingDebug()) {
			vlogDebug("End: TRUProfileFormHandler.preAddShippingAddress()");
		}
	}

	/**
	 * Gets the error handler.
	 *
	 * @return the errorHandler
	 */
	public IErrorHandler getErrorHandler() {
		return mErrorHandler;
	}

	/**
	 * Sets the error handler.
	 *
	 * @param pErrorHandler the errorHandler to set
	 */
	public void setErrorHandler(IErrorHandler pErrorHandler) {
		mErrorHandler = pErrorHandler;
	}

	/**
	 * Setup single shipping details for shipping to a new address.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 * @throws CommerceException CommerceException
	 */
	@SuppressWarnings("unchecked")
	protected void shipToNewAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		if (isLoggingDebug()) {
			logDebug("Start of TRUShippingGroupFormHandler.shipToNewAddress method");
		}
		if (!checkFormRedirect(null, getShipToNewAddressErrorURL(), pRequest,
				pResponse)) {
			return;
		}
		final TRUShippingGroupManager shippingGroupManager = (TRUShippingGroupManager) getShippingGroupManager();
		final TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService)
				getShippingGroupMapContainer();
		if (StringUtils.isNotEmpty(getNewShipToAddressName())) {
			// Create a new shipping group with the new address and put the new shipping group in the container.
			getShippingHelper().findOrAddShippingGroupByNickname(getProfile(), getNewShipToAddressName(), getAddress(),
					shippingGroupMapContainer);
			// get shipping group based on nick name
			final TRUHardgoodShippingGroup containerShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupMapContainer().
					getShippingGroup(getNewShipToAddressName());
			// order first shipping group
			final TRUHardgoodShippingGroup hardgoodShippingGroup = getShippingHelper().getPurchaseProcessHelper().getFirstHardgoodShippingGroup(getOrder());
			if (hardgoodShippingGroup != null) {
				// updating address
				OrderTools.copyAddress(
						containerShippingGroup.getShippingAddress(), hardgoodShippingGroup.getShippingAddress());
				// updating shipping price
				hardgoodShippingGroup.setNickName(getNewShipToAddressName());
				hardgoodShippingGroup.setShippingPrice(getShippingPrice());
				hardgoodShippingGroup.setShippingMethod(getShippingMethod());
				if(isBillingAddressSameAsShippingAddress()) {
					containerShippingGroup.setBillingAddressIsSame(true);
					hardgoodShippingGroup.setBillingAddressIsSame(true);
				}
				containerShippingGroup.setOrderShippingAddress(true);
				hardgoodShippingGroup.setOrderShippingAddress(true);
				if(!getProfile().isTransient()) {
					Map<String, RepositoryItem> profileAddress = (Map) getProfile().getPropertyValue(getCommercePropertyManager().getSecondaryAddressPropertyName());
					if(profileAddress != null) {
						RepositoryItem addrRepositoryItem = profileAddress.get(hardgoodShippingGroup.getNickName());
						if(null!=addrRepositoryItem){
							getShippingHelper().getPurchaseProcessHelper().getOrderTools().updateBillingAddressIsSame(addrRepositoryItem, containerShippingGroup.isBillingAddressIsSame());
						}
					}
				}
				if(isBillingAddressSameAsShippingAddress()) {
					ContactInfo billingAddress  = new ContactInfo();
					OrderTools.copyAddress(
							containerShippingGroup.getShippingAddress(), billingAddress);
					//Adding the billing address to the credit card.
					((TRUOrderImpl)getOrder()).setBillingAddress(billingAddress);

					String nickName = getNewShipToAddressName();
					if (nickName.length() > TRUCheckoutConstants.NICKNAME_MAX_LENGTH) {
					nickName = nickName.substring(TRUConstants.NICKNAME_MIN_LENGTH, TRUCheckoutConstants.NICKNAME_MAX_LENGTH);
					}
					String generatedUniqueNickName = nickName+TRUCheckoutConstants.STRING_BA;
					((TRUOrderImpl)getOrder()).setBillingAddressNickName(generatedUniqueNickName);
					final ShippingGroupManager sgm = getPurchaseProcessHelper().getShippingGroupManager();
					final TRUHardgoodShippingGroup hgsg = (TRUHardgoodShippingGroup) sgm.createShippingGroup();
					hgsg.setBillingAddress(Boolean.TRUE);
					final Address address = hgsg.getShippingAddress();
					OrderTools.copyAddress(containerShippingGroup.getShippingAddress(), address);
					hgsg.setNickName(generatedUniqueNickName);
					hgsg.setLastActivity(new Timestamp(new Date().getTime()));
					getShippingGroupMapContainer().addShippingGroup(generatedUniqueNickName, hgsg);
					//Adding billling address to the profile :TUW-65647
					if(!getProfile().isTransient()) {
						try {

							TRUProfileTools profileTools = getShippingHelper().getPurchaseProcessHelper().getProfileTools();
							profileTools.createProfileRepositorySecondaryAddress(getProfile(),generatedUniqueNickName,billingAddress);
							RepositoryItem billingAddressItem = profileTools.getSecondaryAddressByNickName(getProfile(), generatedUniqueNickName);
							((MutableRepositoryItem)billingAddressItem).setPropertyValue(getPropertyManager().getIsBillingAddressPropertyName(), Boolean.TRUE);
						} catch (RepositoryException e) {
							if(isLoggingError()){
								logError("RepositoryException while adding isBillingAddressSameAsShippingAddress",e);
							}
							throw new CommerceException(PLEASE_TRY_AGAIN, e);
						}
					}
				}
			}
			// setting address as selected address nick name
			shippingGroupMapContainer.setSelectedAddressNickName(getNewShipToAddressName());
		}
		shippingGroupManager.removeEmptyShippingGroups(getOrder());
		if (isLoggingDebug()) {
			vlogDebug("End of TRUShippingGroupFormHandler.shipToNewAddress method");
		}
	}


	/**
	 * Setup single shipping details for shipping to a existing address.
	 *
	 * @param pRequest            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException                if an error occurs.
	 * @exception IOException                if an error occurs.
	 * @throws CommerceException the commerce exception
	 */
	@SuppressWarnings("unchecked")
	protected void shipToExistingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUShippingGroupFormHandler.shipToExistingAddress method for order : {0} and {1} ",getOrder().getId(),getProfile().getRepositoryId())
			;
		}
		if (!checkFormRedirect(null, getShipToNewAddressErrorURL(), pRequest,
				pResponse)) {
			return;
		}
		final TRUShippingGroupManager shippingGroupManager = (TRUShippingGroupManager) getShippingGroupManager();
		final TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService)
				getShippingGroupMapContainer();
		if (StringUtils.isNotEmpty(getShipToAddressName())) {
			// getting container shipping group
			final TRUHardgoodShippingGroup containerShippingGroup = (TRUHardgoodShippingGroup) shippingGroupMapContainer.getShippingGroup(
					getShipToAddressName());
			// Get the shippingGroup if already available
			final TRUHardgoodShippingGroup hardgoodShippingGroup = getShippingHelper().getPurchaseProcessHelper().getFirstHardgoodShippingGroup(getOrder());
			if (hardgoodShippingGroup != null && containerShippingGroup != null) {
				//Copying the address from containerShippingGroup to the  hardgoodShippingGroup's address

				OrderTools.copyAddress(containerShippingGroup.getShippingAddress(), hardgoodShippingGroup.getShippingAddress());
				if (containerShippingGroup.getShippingAddress() instanceof ContactInfo) {
					setAddress((ContactInfo) containerShippingGroup.getShippingAddress());
				}
				// If different nick name Then update the shipping group with selectedNickname
				if (!getShipToAddressName().equalsIgnoreCase(hardgoodShippingGroup.getNickName())) {
					hardgoodShippingGroup.setNickName(getShipToAddressName());
					if(containerShippingGroup.isGiftReceipt()) {
						hardgoodShippingGroup.setGiftReceipt(Boolean.TRUE);
						hardgoodShippingGroup.setGiftWrapMessage(containerShippingGroup.getGiftWrapMessage());
					} else {
						hardgoodShippingGroup.setGiftReceipt(Boolean.FALSE);
						hardgoodShippingGroup.setGiftWrapMessage(TRUCheckoutConstants.EMPTY_STRING);
					}
				}
				//EDIT BA Changes
				Map<String, ShippingGroup> shippingGroupMap = shippingGroupMapContainer.getShippingGroupMap();
				if (shippingGroupMap != null && !shippingGroupMap.isEmpty()) {
					final Set<String> shipGroupNames = shippingGroupMap.keySet();
					for (String shipGroupName : shipGroupNames) {
						ShippingGroup shippingGroup = shippingGroupMapContainer.getShippingGroup(shipGroupName);
						if(shippingGroup instanceof TRUHardgoodShippingGroup) {
							((TRUHardgoodShippingGroup)shippingGroup).setOrderShippingAddress(false);
						}
					}
				}

				hardgoodShippingGroup.setOrderShippingAddress(true);
				containerShippingGroup.setOrderShippingAddress(true);
			}
			if (isLoggingDebug()) {
				vlogDebug("Nick Name: " + hardgoodShippingGroup.getNickName());
			}
			getShippingHelper().updateShippingMethodAndPrice(getOrder(), hardgoodShippingGroup,shippingGroupMapContainer);
			// setting address as selected address nick name
			//shippingGroupMapContainer.setDefaultShippingGroupName(getShipToAddressName());
			shippingGroupMapContainer.setSelectedAddressNickName(getShipToAddressName());
		}
		shippingGroupManager.removeEmptyShippingGroups(getOrder());
		if (isLoggingDebug()) {
			vlogDebug("End of TRUShippingGroupFormHandler.shipToExistingAddress method for order : {0} and {1} ",getOrder().getId(),getProfile().getRepositoryId());
		}
	}

	/**
	 * This method initializes the billing address from the shipping address if
	 * the user selected that option. Saves addresses in the profile, if the
	 * user selected that option.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 * @throws CommerceException
	 * 					if an error occurs.
	 */
	protected void postShipToNewAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUShippingGroupFormHandler.postShipToNewAddress method");
		}

		final TRUOrderImpl order = (TRUOrderImpl) getOrder();
		order.setBillingAddressSameAsShippingAddress(isBillingAddressSameAsShippingAddress());
		//Start : CR161
		if(order.getStorePickUpInfos()!=null && !order.getStorePickUpInfos().isEmpty()){
			order.getStorePickUpInfos().clear();
		}
		//End  : CR161
		if (isLoggingDebug()) {
			vlogDebug("End of TRUShippingGroupFormHandler.postShipToNewAddress method");
		}
	}

	/**
	 * Handle 'Ship to existing address' case.
	 *
	 * @param pRequest            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse            a <code>DynamoHttpServletResponse</code> value.
	 * @return redirection result.
	 * @exception ServletException                if an error occurs.
	 * @exception IOException                if an error occurs.
	 * @throws com.tru.common.cml.SystemException  the system exception
	 */
	public boolean handleShipToExistingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException, com.tru.common.cml.SystemException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.handleShipToExistingAddress method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean hasException = false;		
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SHIP_TO_EXISTING_ADDRESS)) {
			Transaction tr = null;
			try {				
				tr = ensureTransaction();				
				synchronized (getOrder()) {
					TRUOrderImpl orderImpl=(TRUOrderImpl) getOrder();
					preShipToExistingAddress(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							logDebug("Redirecting due to form error in field validation() method");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					shipToExistingAddress(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in handleShipToExistingAddress() method");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					if(!isSkipShippingRestrctions()){
						runProcessValidationShippingInfos(pRequest, pResponse);
					}
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug(COMMON_LOG_1);
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					postShipToExisitngAddress(pRequest, pResponse);
					// Construct Redirect URL
					orderImpl.getActiveTabs().put(TRUCheckoutConstants.SHIPPING_TAB, Boolean.TRUE);
					//orderImpl.setShowMeGiftingOptions(isShowMeGiftingOptions());
					((TRUOrderManager)getOrderManager()).updateShowMeGiftingOptions(getOrder());
					if (orderImpl.isShowMeGiftingOptions()) {
						setApplyShippingGroupsSuccessURL(getTRUConfiguration().getGiftOptionsPageURL());
						orderImpl.getActiveTabs().put(TRUCheckoutConstants.GIFTING_TAB, Boolean.TRUE);
					} else if (!((TRUOrderTools) getOrderManager().getOrderTools()).getAllInStorePickupShippingGroups(
							getOrder()).isEmpty()) {
						setApplyShippingGroupsSuccessURL(getTRUConfiguration().getPickupPageURL());
						orderImpl.getActiveTabs().put(TRUCheckoutConstants.PICKUP_TAB, Boolean.TRUE);
					} else {
						setApplyShippingGroupsSuccessURL(getTRUConfiguration().getPaymentPageURL());
						if (orderImpl.getActiveTabs().get(TRUCheckoutConstants.GIFTING_TAB) != null) {
							orderImpl.getActiveTabs().remove(TRUCheckoutConstants.GIFTING_TAB);
						}
						orderImpl.getActiveTabs().put(TRUCheckoutConstants.PAYMENT_TAB, Boolean.TRUE);
					}
					// Check for Order Reconciliation
					setOrderReconciliationNeeded(isOrderReconciliationRequired());
					if (isLoggingDebug()) {
						vlogDebug("isOrderReconciliationNeeded() ? {0}", isOrderReconciliationNeeded());
					}
					if (isNavigateToMultiShip() || isOrderReconciliationNeeded()) {
						orderImpl.setOrderIsInMultiShip(Boolean.TRUE);
						applyMultiShipping();
						((TRUPaymentGroupContainerService)getPaymentGroupMapContainer()).setBillingAddressSameAsShippingAddress(
								isBillingAddressSameAsShippingAddress());
					} else {
						getUserSession().setSignInHidden(Boolean.TRUE);
						orderImpl.setApplyGiftingOptions(Boolean.FALSE);
					}
					runProcessRepriceOrder(
							getOrder(), getUserPricingModels(), getLocale(), getProfile(), createRepriceParameterMap());
					// Added for updating special financing related flags in  order if order is eligible
					//((TRUOrderTools) getOrderManager().getOrderTools()).updateOrderIfEligibleForFinancing(getOrder());
					getOrderManager().updateOrder(getOrder());
				}				
			} catch (RunProcessException e) {
				hasException = true;
				addFormException(new DropletException(PLEASE_TRY_AGAIN));
				if (isLoggingError()) {
					logError("RunProcessException in TRUShippingGroupFormHandler.handleShipToExistingAddress", e);
				}
			}  catch (CommerceException e) {
				hasException = true;
				addFormException(new DropletException(PLEASE_TRY_AGAIN));
				if (isLoggingError()) {
					logError("CommerceException in TRUShippingGroupFormHandler.handleShipToExistingAddress", e);
				}
			} finally {
				if (tr != null) {					
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in TRUInStorePickUpFormHandler.handleInitRegistryShippingGroups", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SHIP_TO_EXISTING_ADDRESS);
				}
			}
		}if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.handleShipToExistingAddress method");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * Performs input data validations for existing shipping address specified
	 * by shopper.
	 *
	 * @param pRequest a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse a <code>DynamoHttpServletResponse</code> value.
	 * @throws ServletException if an error occurs.
	 * @throws IOException if an error occurs.
	 */
	protected void preShipToExistingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {

		if (isLoggingDebug()) {
			vlogDebug("Start: TRUShippingGroupFormHandler.preShipToExistingAddress()");
		}

		if (!isNavigateToMultiShip() && StringUtils.isEmpty(getShippingMethod())) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_SHIPPING_NO_SHIP_METHOD_SELECTED, null);
			getErrorHandler().processException(mVe, this);
			//addFormException(new DropletException(getShippingManager().getShipMethodRequired()));
			return;
		}
		if (isLoggingDebug()) {
			vlogDebug("End: TRUProfileFormHandler.preShipToExistingAddress()");
		}
	}

	/**
	 * <p>
	 * 
	 * </p>.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	/*protected void postShipToExistingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.postShipToExistingAddress method");
		}
		final TRUOrderImpl order = (TRUOrderImpl) getOrder();
		final String previousTab = order.getPreviousTab();
		if (TRUCheckoutConstants.REVIEW_TAB.equals(previousTab)) {
			getShippingHelper().updateEstimateDeliveryDates(getOrder());
			if (isLoggingDebug()) {
				logDebug("EDD Updated");
			}
		}

		//Start : CR161
		if(order.getStorePickUpInfos()!=null && !order.getStorePickUpInfos().isEmpty()){
			order.getStorePickUpInfos().clear();
		}
		//End : CR161
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.postShipToExistingAddress method");
		}
	}*/


	/**
	 * Post ship to exisitng address.
	 *
	 * @param pRequest            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException                if an error occurs.
	 * @exception IOException                if an error occurs.
	 * @throws CommerceException the commerce exception
	 */
	protected void postShipToExisitngAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.postShipToExisitngAddress method");
		}
		final TRUOrderImpl order = (TRUOrderImpl) getOrder();
		final String previousTab = order.getPreviousTab();
		if (TRUCheckoutConstants.REVIEW_TAB.equals(previousTab)) {
			getShippingHelper().updateEstimateDeliveryDates(getOrder());
			if (isLoggingDebug()) {
				logDebug("EDD Updated");
			}
		}

		//Start : CR161
		if(order.getStorePickUpInfos()!=null && !order.getStorePickUpInfos().isEmpty()){
			order.getStorePickUpInfos().clear();
		}
		//End : CR161
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.postShipToExisitngAddress method");
		}
	}


	/**
	 * Handle 'Add Address in Profile' case.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * 
	 * @return redirection result.
	 * 
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	public boolean handleAddShippingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.handleAddShippingAddress method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS)) {
			Transaction tr = null;
			boolean isEnableAddressDoctor = false;
			try {
				 if (PerformanceMonitor.isEnabled()){
						PerformanceMonitor.startOperation(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
					}
				
				tr = ensureTransaction();
				synchronized (getOrder()) {
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in addShippingAddress");
						}
						return checkFormRedirect(getCommonSuccessURL(),
								getCommonErrorURL(), pRequest, pResponse);
					}
					preAddShippingAddress(pRequest, pResponse);

					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in preAddShippingAddress.");
						}
						return checkFormRedirect(getCommonSuccessURL(),
								getCommonErrorURL(), pRequest, pResponse);
					}

					boolean isAddressValidated = Boolean
							.parseBoolean(getAddressValidated());
					// Validate address with AddressDoctor Service
					if(SiteContextManager.getCurrentSite() != null){
						/*Object value=SiteContextManager.getCurrentSite().getPropertyValue(getPropertyManager().getEnableAddressDoctorPropertyName());*/
						Object value = getIntegrationStatusUtil().getAddressDoctorIntStatus(pRequest);
						if(value!=null){
							isEnableAddressDoctor = (boolean)value;
						}
					}

					if (!isAddressValidated && isEnableAddressDoctor) {
						final String addressStatus = ((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).validateAddress(
								getAddress(), getOrder(), getProfile(), TRUConstants.SHIPPING,TRUConstants.ADD_ADDRESS);

						setAddressDoctorProcessStatus(addressStatus);
						TRUAddressDoctorResponseVO addressResponseVo = ((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).getProfileTools().getAddressDoctorResponseVO();
						setAddressDoctorResponseVO(addressResponseVo);
						if (TRUConstants.ADDRESS_DOCTOR_STATUS_VERIFIED.equalsIgnoreCase(addressStatus)) {
							isAddressValidated = Boolean.TRUE;
						}

						else if(TRUConstants.ADDRESS_DOCTOR_STATUS_CORRECTED.equalsIgnoreCase(addressStatus)){
							String derivedStatus = addressResponseVo.getDerivedStatus();
							if(derivedStatus.equalsIgnoreCase(TRUConstants.NO)){
								isAddressValidated = Boolean.TRUE;
								ContactInfo contactInfo = getAddress();
								if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getAddress1())){
									contactInfo.setAddress1(addressResponseVo.getAddressDoctorVO().get(0).getAddress1());}
								if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getAddress2())){
									contactInfo.setAddress2(addressResponseVo.getAddressDoctorVO().get(0).getAddress2());}
								if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getCity())){
									contactInfo.setCity(addressResponseVo.getAddressDoctorVO().get(0).getCity());}
								if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getState())){
									contactInfo.setState(addressResponseVo.getAddressDoctorVO().get(0).getState());}
								if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getPostalCode())){
									contactInfo.setPostalCode(addressResponseVo.getAddressDoctorVO().get(0).getPostalCode());
								}if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getCountry())){
									contactInfo.setCountry(TRUConstants.UNITED_STATES);
								}
								setAddress(contactInfo);
							}
						}
						else if (TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR.equalsIgnoreCase(addressStatus)) {
							setAddressDoctorProcessStatus(addressStatus);
							final TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
							addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
							addressDoctorResponseVO.setDerivedStatus(addressResponseVo.getDerivedStatus());
							setAddressDoctorResponseVO(addressDoctorResponseVO);
							//isAddressValidated = Boolean.TRUE;
						}
					} else if (!isEnableAddressDoctor && !isAddressValidated) {
						setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR);
						final TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
						addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
						addressDoctorResponseVO.setDerivedStatus(TRUConstants.YES);
						setAddressDoctorResponseVO(addressDoctorResponseVO);
					}
					if (isAddressValidated) {
						addShippingAddress();
						if (getFormError()) {
							if (isLoggingDebug()) {
								vlogDebug("Redirecting due to form error in addShippingAddress");
							}
							return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
						}
						postAddShippingAddress(pRequest, pResponse);
						getOrderManager().updateOrder(getOrder());
					}
				}
			} catch (CommerceException e) {
				PerformanceMonitor.cancelOperation(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
				if (isLoggingError()) {
					vlogError("CommerceException in TRUShippingGroupFormHandler.handleAddShippingAddress", e);
				}
			} finally {
				if (tr != null)
				{
					commitTransaction(tr);
				}
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.endOperation(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.handleAddShippingAddress method");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(),
				pRequest, pResponse);
	}

	/**
	 * preAddShippingAddress.
	 *
	 * @param pRequest a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse a <code>DynamoHttpServletResponse</code> value.
	 * @throws ServletException if an error occurs.
	 * @throws IOException if an error occurs.
	 */
	protected void preAddShippingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {

		//CR-76 Trim all the input fields.
		trimContactInfoValues(getAddress());

		final ContactInfo address = getAddress();
		validateShippingAddress(address, pRequest, pResponse);

		if (!getFormError() && StringUtils.isBlank(getNewShipToAddressName())) {
			mVe.addValidationError(
					TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDRESS_MISSING_INFO, null);
			getErrorHandler().processException(mVe, this);
		}

		if (!getFormError()) {
			validateDuplicateAddressNickname(getNewShipToAddressName(), pRequest, pResponse);
		}
	}

	/**
	 * postAddShippingAddress.
	 *
	 * @param pRequest a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse a <code>DynamoHttpServletResponse</code> value.
	 * @throws ServletException if an error occurs.
	 * @throws IOException if an error occurs.
	 */
	protected void postAddShippingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if(isLoggingDebug()){
			vlogDebug("START of TRUShippingGroupFormHandler.postAddShippingAddress method");
		}

		/*TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
    	boolean isAnonymousUser = profileTools.isAnonymousUser(getProfile());
		if(!isAnonymousUser){
			try {
				((TRUHardgoodShippingGroupInitializer) getShippingGroupInitializer()).initializeHardgood(
						(Profile) getProfile(), getShippingGroupMapContainer(), pRequest);
			} catch (ShippingGroupInitializationException e) {
				if (isLoggingError()) {
					logError("ShippingGroupInitializationException occured @TRUShippingGroupFormHandler.postAddShippingAddress", e);
				}
			}
		}	*/
		if(isLoggingDebug()){
			vlogDebug("END of TRUShippingGroupFormHandler.postAddShippingAddress method");
		}
	}

	/**
	 * This method will be invoked from the overlay.
	 *
	 * @throws ServletException if an error occurs.
	 * @throws IOException if an error occurs.
	 */
	@SuppressWarnings("unchecked")
	private void addShippingAddress() throws ServletException, IOException {
		if(isLoggingDebug()){
			vlogDebug("START of TRUShippingGroupFormHandler.addShippingAddress method");
		}
		try {
			final TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) getShippingGroupMapContainer();
			TRUHardgoodShippingGroup hgsg = getShippingHelper().addShippingAddress(getProfile(), getNewShipToAddressName(), getAddress(),
					shippingGroupMapContainer);
			if(hgsg != null && isBillingAddressSameAsShippingAddress()) {
				Map<String, ShippingGroup> shippingGroupMap = shippingGroupMapContainer.getShippingGroupMap();
				if (shippingGroupMap != null && !shippingGroupMap.isEmpty()) {
					final Set<String> shipGroupNames = shippingGroupMap.keySet();
					for (String shipGroupName : shipGroupNames) {
						ShippingGroup shippingGroup = shippingGroupMapContainer.getShippingGroup(shipGroupName);
						if(shippingGroup instanceof TRUHardgoodShippingGroup) {
							((TRUHardgoodShippingGroup)shippingGroup).setBillingAddressIsSame(false);
						}
					}
				}
				hgsg.setBillingAddressIsSame(true);
			}
			// Set Default Address if the user is logged in
			if(!getShippingHelper().getPurchaseProcessHelper().getProfileTools().isAnonymousUser(getProfile())){
				getShippingHelper().getPurchaseProcessHelper().getProfileTools().setProfileDefaultAddress(getProfile());
			}
			// update nick name to shipping group
			final ShippingGroup shippingGroup = shippingGroupMapContainer.getShippingGroup(getNewShipToAddressName());
			if (shippingGroup instanceof TRUHardgoodShippingGroup) {
				final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
				hardgoodShippingGroup.setNickName(getNewShipToAddressName());
			}
		} catch (CommerceException e) {
			if (isLoggingError()) {
				logError("CommerceException in TRUShippingGroupFormHandler.addShippingAddress", e);
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError(
						"RepositoryException in TRUShippingGroupFormHandler.addShippingAddress while calling setProfileDefaultAddress()",
						e);
			}
		}
		if(isLoggingDebug()){
			vlogDebug("END of TRUShippingGroupFormHandler.addShippingAddress method");
		}
	}

	/**
	 * validateShippingAddress.
	 *
	 * @param pNickName nickname
	 * @param pRequest a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse a <code>DynamoHttpServletResponse</code> value.
	 * @throws IOException if an error occurs.
	 * @throws ServletException if an error occurs.
	 */
	protected void validateDuplicateAddressNickname(String pNickName, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws IOException, ServletException {
		if (isLoggingDebug()) {
			vlogDebug("Start: TRUShippingGroupFormHandler.validateDuplicateAddressNickname()");
		}
		if(isRestService() && StringUtils.isNotBlank(pNickName) ) {
			boolean invalidNkName =  getStoreUtils().containsSpecialCharacter(pNickName);
			if(invalidNkName) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDR_INVALID_NICKNAME, null);
				getErrorHandler().processException(mVe, this);
				return;
			}
		}
		if (getShippingGroupMapContainer().getShippingGroup(pNickName) != null) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_DUPLICATE_ADDR_NICKNAME, null);
			getErrorHandler().processException(mVe, this);
		}

		if (isLoggingDebug()) {
			vlogDebug("End: TRUShippingGroupFormHandler.validateDuplicateAddressNickname()");
		}
	}

	/**
	 * This method is used to validate shipping address.
	 * @param pAddress address
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws IOException io exception
	 * @throws ServletException servlet exception
	 */
	protected void validateShippingAddress(Address pAddress, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws IOException, ServletException {
		if (isLoggingDebug()) {
			vlogDebug("Start: TRUShippingGroupFormHandler.validateShippingAddress()");
		}

		if (StringUtils.isBlank(pAddress.getFirstName()) || StringUtils.isBlank(pAddress.getLastName()) ||
				StringUtils.isBlank(pAddress.getAddress1()) || StringUtils.isBlank(pAddress.getCity()) ||
				StringUtils.isBlank(pAddress.getPostalCode()) || StringUtils.isBlank(pAddress.getState()) ||
				StringUtils.isBlank(((ContactInfo) pAddress).getPhoneNumber())) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDRESS_MISSING_INFO, null);
			getErrorHandler().processException(mVe, this);
		}

		/*
		 * if (pattern.matcher(getAddress().getPostalCode()).matches()) {
		 * ve.addValidationError("ZIP/Postal Code must be a digit.", null);
		 * getErrorHandler().processException(ve, this); } if
		 * (pattern.matcher(((ContactInfo) getAddress()).getPhoneNumber())
		 * .matches()) { ve.addValidationError("Phone Number must be a digit.",
		 * null); getErrorHandler().processException(ve, this); }
		 */

		if (isLoggingDebug()) {
			vlogDebug("End: TRUProfileFormHandler.validateShippingAddress()");
		}
	}

	/**
	 * Handler for removing a shipping address.
	 *
	 * @param pRequest            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse            a <code>DynamoHttpServletResponse</code> value.
	 * @return true if success, false - otherwise.
	 * @throws ServletException             if servlet error occurs.
	 * @throws IOException             if IO error occurs.
	 */

	public boolean handleRemoveShippingAddress(
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.handleRemoveShippingAddress method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_REMOVE_SHIPPING_ADDRESS)) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in handleRemoveShippingAddress.");
						}
						return checkFormRedirect(getCommonSuccessURL(),
								getCommonErrorURL(), pRequest, pResponse);
					}
					preRemoveShippingAddress(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in preRemoveShippingAddress.");
						}
						return checkFormRedirect(getCommonSuccessURL(),
								getCommonErrorURL(), pRequest, pResponse);
					}
					removeShippingAddress();
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in removeShippingAddress");
						}
						return checkFormRedirect(getCommonSuccessURL(),
								getCommonErrorURL(), pRequest, pResponse);
					}
					postRemoveShippingAddress(pRequest, pResponse);
					runProcessRepriceOrder(getOrder(),
							getUserPricingModels(), getLocale(),
							getProfile(), createRepriceParameterMap());
					getPurchaseProcessHelper().getShippingGroupManager().removeEmptyShippingGroups(getOrder());
					getOrderManager().updateOrder(getOrder());
				}
				return checkFormRedirect(getCommonSuccessURL(),
						getCommonErrorURL(), pRequest, pResponse);
			} catch (RunProcessException e) {
				if (isLoggingError()) {
					logError(
							"RunProcessException in TRUShippingGroupFormHandler.handleRemoveShippingAddress",
							e);
				}
			} catch (CommerceException e) {
				if (isLoggingError()) {
					logError(
							"CommerceException in TRUShippingGroupFormHandler.handleRemoveShippingAddress",
							e);
				}
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError(
							"RepositoryException in TRUShippingGroupFormHandler.handleRemoveShippingAddress",
							e);
				}
			} finally {
				if (tr != null)
				{
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_REMOVE_SHIPPING_ADDRESS);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.handleRemoveShippingAddress method");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(),
				pRequest, pResponse);
	}

	/**
	 * removeShippingAddress.
	 *
	 * @throws ServletException if an error occurs.
	 * @throws IOException if an error occurs.
	 * @throws RepositoryException if an error occurs.
	 * @throws CommerceException if an error occurs.
	 */
	private void removeShippingAddress() throws ServletException,
	IOException, RepositoryException, CommerceException {
		getShippingHelper().removeShippingAddress(getOrder(),
				getProfile(), getRemoveShippingAddressNickName(), getShippingGroupMapContainer());
	}

	/**
	 * Pre remove shipping address processing.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * 
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	protected void preRemoveShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (getRemoveShippingAddressNickName() == null || getRemoveShippingAddressNickName().isEmpty() ||
				getShippingGroupMapContainer().getShippingGroup(getRemoveShippingAddressNickName()) == null) {
			mVe.addValidationError(TRUErrorKeys.REMOVE_SHIPPING_NICK_NAME, null);
			getErrorHandler().processException(mVe, this);
		}

	}

	/**
	 * Post remove shipping address processing. If the address nickname is in
	 * the profile's map, the address is removed from the profile too.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * 
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	protected void postRemoveShippingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if(isLoggingDebug()){
			vlogDebug("START of TRUShippingGroupFormHandler.postRemoveShippingAddress method");
		}
		if(isLoggingDebug()){
			vlogDebug("END of TRUShippingGroupFormHandler.postRemoveShippingAddress method");
		}
	}

	/**
	 * This method is for editing the shipping address pertaining to the user
	 * account.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean success/fail
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleEditShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.handleEditShippingAddress method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS)) {
			Transaction tr = null;
			boolean isEnableAddressDoctor = false;
			try {
			 if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.startOperation(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
				}
				tr = ensureTransaction();
				synchronized (getOrder()) {
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in handleEditShippingAddress");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					preEditShippingAddress(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in preEditShippingAddress.");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					boolean isAddressValidated = Boolean.parseBoolean(getAddressValidated());
					if (TRUConstants.UNITED_STATES.equalsIgnoreCase(getAddress().getCountry())) {
						if (SiteContextManager.getCurrentSite() != null) {
							/*
							 * Object value=SiteContextManager.getCurrentSite().getPropertyValue(getPropertyManager().
							 * getEnableAddressDoctorPropertyName());
							 */
							final Object value = getIntegrationStatusUtil().getAddressDoctorIntStatus(pRequest);
							if (value != null) {
								isEnableAddressDoctor = (boolean) value;
							}
						}

						// Validate address with AddressDoctor Service
						if (!isAddressValidated && isEnableAddressDoctor) {
							final String addressStatus = ((TRUPurchaseProcessHelper) getPurchaseProcessHelper())
									.validateAddress(getAddress(), getOrder(), getProfile(), TRUConstants.SHIPPING,TRUConstants.UPDATE_ADDRESS);
							setAddressDoctorProcessStatus(addressStatus);
							TRUAddressDoctorResponseVO addressResponseVo = ((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).getProfileTools().getAddressDoctorResponseVO();
							setAddressDoctorResponseVO(addressResponseVo);
							if (TRUConstants.ADDRESS_DOCTOR_STATUS_VERIFIED.equalsIgnoreCase(addressStatus)) {
								isAddressValidated = Boolean.TRUE;
							}
							else if(TRUConstants.ADDRESS_DOCTOR_STATUS_CORRECTED.equalsIgnoreCase(addressStatus)){
								String derivedStatus = addressResponseVo.getDerivedStatus();
								if(derivedStatus.equalsIgnoreCase(TRUConstants.NO)){
									isAddressValidated = Boolean.TRUE;
									ContactInfo contactInfo = getAddress();
									if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getAddress1())){
										contactInfo.setAddress1(addressResponseVo.getAddressDoctorVO().get(0).getAddress1());}
									if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getAddress2())){
										contactInfo.setAddress2(addressResponseVo.getAddressDoctorVO().get(0).getAddress2());}
									if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getCity())){
										contactInfo.setCity(addressResponseVo.getAddressDoctorVO().get(0).getCity());}
									if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getState())){
										contactInfo.setState(addressResponseVo.getAddressDoctorVO().get(0).getState());}
									if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getCountry())){
										//contactInfo.setCountry(addressResponseVo.getAddressDoctorVO().get(0).getCountry());
										contactInfo.setCountry(TRUConstants.UNITED_STATES);
									}
									if(!StringUtils.isBlank(addressResponseVo.getAddressDoctorVO().get(0).getPostalCode())){
										contactInfo.setPostalCode(addressResponseVo.getAddressDoctorVO().get(0).getPostalCode());
									}
									setAddress(contactInfo);
								}
							}

							else if (TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR.equalsIgnoreCase(addressStatus)) {
								setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR);
								final TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
								addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
								addressDoctorResponseVO.setDerivedStatus(addressResponseVo.getDerivedStatus());
								setAddressDoctorResponseVO(addressDoctorResponseVO);
								// isAddressValidated = Boolean.TRUE;
							}
						} else if (!isEnableAddressDoctor && !isAddressValidated) {
							setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR);
							final TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
							addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
							addressDoctorResponseVO.setDerivedStatus(TRUConstants.YES);
							setAddressDoctorResponseVO(addressDoctorResponseVO);
						}
					}
					if (isAddressValidated || !TRUConstants.UNITED_STATES.equalsIgnoreCase(getAddress().getCountry())) {
						editShippingAddress(pRequest, pResponse);
						if (getFormError()) {
							if (isLoggingDebug()) {
								vlogDebug("Redirecting due to form error in editShippingAddress");
							}
							return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
						}
						postEditShippingAddress(pRequest, pResponse);
						runProcessRepriceOrder(getOrder(), getUserPricingModels(), getLocale(), getProfile(),
								createRepriceParameterMap());
						getOrderManager().updateOrder(getOrder());
					}
				}

			} catch (RunProcessException e) {
				PerformanceMonitor.cancelOperation(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
				if (isLoggingError()) {
					logError("RunProcessException in TRUShippingGroupFormHandler.handleEditShippingAddress", e);
				}
			} catch (CommerceException e) {
				PerformanceMonitor.cancelOperation(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
				if (isLoggingError()) {
					logError("CommerceException in TRUShippingGroupFormHandler.handleEditShippingAddress", e);
				}
			}finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.endOperation(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.handleEditShippingAddress method");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * Edits the shipping address.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 * @throws CommerceException CommerceException
	 */
	public void editShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, CommerceException {
		if (getNewShipToAddressName() != null) {
			if (isRestService()) {
				final HardgoodShippingGroup hgsg = (HardgoodShippingGroup) getShippingGroupMapContainer().getShippingGroup(
						getNewShipToAddressName());
				if (hgsg == null) {
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDR_INVALID_NICKNAME, null);
					getErrorHandler().processException(mVe, this);
					return;
				}
			}
			getShippingHelper().modifyShippingAddress(getProfile(), getNewShipToAddressName(), getAddress(),
					getShippingGroupMapContainer(), getOrder());
		}
	}

	/**
	 * Pre edit shipping address.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	protected void preEditShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		// CR-76 Trim all the input fields.
		trimContactInfoValues(getAddress());

		final ContactInfo address = getAddress();
		validateShippingAddress(address, pRequest, pResponse);
		if (isRestService() && StringUtils.isBlank(getNewShipToAddressName())) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDR_EMPTY_NICKNAME, null);
			getErrorHandler().processException(mVe, this);
		}
	}

	/**
	 * Post edit shipping address.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 * @throws CommerceException CommerceException
	 */
	protected void postEditShippingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		if(isLoggingDebug()){
			vlogDebug("START of TRUShippingGroupFormHandler.postEditShippingAddress method");
		}
		/*TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
    	boolean isAnonymousUser = profileTools.isAnonymousUser(getProfile());
		if(!isAnonymousUser){
			try {
				((TRUHardgoodShippingGroupInitializer) getShippingGroupInitializer()).initializeHardgood(
						(Profile) getProfile(), getShippingGroupMapContainer(), pRequest);
			} catch (ShippingGroupInitializationException e) {
				if (isLoggingError()) {
					logError("ShippingGroupInitializationException occured @TRUShippingGroupFormHandler.postAddShippingAddress", e);
				}
			}
		}	*/
		if(isLoggingDebug()){
			vlogDebug("END of TRUShippingGroupFormHandler.postEditShippingAddress method");
		}
	}

	/**
	 * This method is for setting the default shipping address pertaining to the
	 * user account.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean success/fail
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleSetAsDefaultShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.handleSetAsDefaultShippingAddress method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SET_AS_DEFAULT_SHIPPING_ADDRESS)) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in handleSetAsDefaultShippingAddress.");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					preSetAsDefaultShippingAddress(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in preSetAsDefaultShippingAddress.");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					setAsDefaultShippingAddress();
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in setAsDefaultShippingAddress");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					postSetAsDefaultShippingAddress(pRequest, pResponse);
					// getOrderManager().updateOrder(getOrder());
				}
				return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					vlogError(
							"Repository Exception occured in @Class::TRUShippingGroupFormHandler:::@method:::handleSetAsDefaultShippingAddress ",
							e);
				}
			} /*
			 * catch (CommerceException comEx) { if (isLoggingError()) { vlogError(
			 * "CommerceException occured in @Class::TRUShippingGroupFormHandler:::@method:::handleSetAsDefaultShippingAddress ",
			 * comEx); } }
			 */finally {
				 if (tr != null) {
					 commitTransaction(tr);
				 }
				 if (rrm != null) {
					 rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SET_AS_DEFAULT_SHIPPING_ADDRESS);
				 }
			 }
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.handleSetAsDefaultShippingAddress method");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * Pre set as default shipping address processing.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * 
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	protected void preSetAsDefaultShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.preSetAsDefaultShippingAddress method");
		}
		if (StringUtils.isBlank(getDefaultShippingAddressNickName())) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDR_EMPTY_NICKNAME, null);
			getErrorHandler().processException(mVe, this);
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.preSetAsDefaultShippingAddress method");
		}

	}

	/**
	 * Post setting as default shipping address processing.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * 
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	protected void postSetAsDefaultShippingAddress(
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if(isLoggingDebug()){
			vlogDebug("START of TRUShippingGroupFormHandler.postSetAsDefaultShippingAddress method");
		}
		if(isLoggingDebug()){
			vlogDebug("END of TRUShippingGroupFormHandler.postSetAsDefaultShippingAddress method");
		}

	}

	/**
	 * This method is used to set the address as default address.
	 *
	 * @throws ServletException servlet exception
	 * @throws IOException io exception
	 * @throws RepositoryException repository exception
	 */
	private void setAsDefaultShippingAddress() throws ServletException, IOException, RepositoryException {
		if (getDefaultShippingAddressNickName() != null) {
			if (isRestService()) {
				final HardgoodShippingGroup hgsg = (HardgoodShippingGroup) getShippingGroupMapContainer().getShippingGroup(
						getDefaultShippingAddressNickName());
				if (hgsg == null) {
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDR_INVALID_NICKNAME, null);
					getErrorHandler().processException(mVe, this);
					return;
				}
			}
			getShippingHelper().setAsDefaultShippingAddress(getProfile(), getDefaultShippingAddressNickName(),
					getShippingGroupMapContainer());
		}
	}

	/**
	 * to validate all the required field to change ship method.
	 *
	 * @param pRequest - request
	 * @param pResponse -response
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	protected void preChangeShipMethod(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.preChangeShipMethod method");
		}
		final Double shipPrice = getShipMethodVO().getShippingPrice();

		if (getShipMethodVO() != null
				&& (StringUtils.isBlank(getShipMethodVO().getShipMethodCode())
						|| StringUtils.isBlank(getShipMethodVO().getRegionCode()) || shipPrice == null)) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDRESS_MISSING_INFO, null);
			getErrorHandler().processException(mVe, this);
		}

		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.preChangeShipMethod method");
		}
	}

	/**
	 * to validate all the required field to change ship method.
	 *
	 * @param pRequest - request
	 * @param pResponse -response
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	protected void postChangeShipMethod(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.postChangeShipMethod method");
		}

		final String previousTab = ((TRUOrderImpl) getOrder()).getPreviousTab();
		if (TRUCheckoutConstants.REVIEW_TAB.equals(previousTab)) {
			getShippingHelper().updateEstimateDeliveryDates(getOrder());
			if (isLoggingDebug()) {
				logDebug("EDD Updated");
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.postChangeShipMethod method");
		}

	}


	/**
	 * <p>
	 * 	This method will be invoked on change of ship method in single ship page.
	 * </p>
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleChangeShipMethod(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.handleChangeShipMethod method");
		}
		boolean hasException=false;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CHANGE_SHIP_METHOD)) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				final String selectedShipMethodCode = getShipMethodVO().getShipMethodCode();
				final String selectedregionCode = getShipMethodVO().getRegionCode();
				final double selectedShippingPrice = getShipMethodVO().getShippingPrice();
				synchronized (getOrder()) {
					if (isRestService()) {
						preChangeShipMethod(pRequest, pResponse);
						if (getFormError()) {
							if (isLoggingDebug()) {
								vlogDebug("Redirecting due to form error in ChangeShipMethod");
							}
							return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
						}
					}
					final TRUHardgoodShippingGroup hardgoodShippingGroup = getShippingHelper().getPurchaseProcessHelper()
							.getFirstHardgoodShippingGroup(getOrder());
					if (hardgoodShippingGroup != null) {
						hardgoodShippingGroup.setShippingPrice(selectedShippingPrice);
						hardgoodShippingGroup.setShippingMethod(selectedShipMethodCode);
						hardgoodShippingGroup.setRegionCode(selectedregionCode);
						runProcessRepriceOrder(getOrder(), getUserPricingModels(), getLocale(), getProfile(),
								createRepriceParameterMap());
						postChangeShipMethod(pRequest, pResponse);
						getOrderManager().updateOrder(getOrder());
					}
				}
			} catch (RunProcessException e) {
				hasException=true;
				if (isLoggingError()) {
					logError("RunProcessException in TRUShippingGroupFormHandler.handleChangeShipMethod", e);
				}
			} catch (CommerceException e) {
				hasException=true;
				if (isLoggingError()) {
					logError("CommerceException in TRUShippingGroupFormHandler.handleChangeShipMethod", e);
				}
			}finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in TRUShippingGroupFormHandler.handleChangeShipMethod", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CHANGE_SHIP_METHOD);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.handleChangeShipMethod method");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/** holds estimateShipWindowMin. **/
	private int mEstimateShipWindowMin;

	/** holds estimateShipWindowMax. **/
	private int mEstimateShipWindowMax;

	/**
	 * Gets the estimate ship window min.
	 *
	 * @return the estimateShipWindowMin.
	 */
	public int getEstimateShipWindowMin() {
		return mEstimateShipWindowMin;
	}

	/**
	 * Sets the estimate ship window min.
	 *
	 * @param pEstimateShipWindowMin the estimateShipWindowMin to set.
	 */
	public void setEstimateShipWindowMin(int pEstimateShipWindowMin) {
		mEstimateShipWindowMin = pEstimateShipWindowMin;
	}

	/**
	 * Gets the estimate ship window max.
	 *
	 * @return the estimateShipWindowMax.
	 */
	public int getEstimateShipWindowMax() {
		return mEstimateShipWindowMax;
	}

	/**
	 * Sets the estimate ship window max.
	 *
	 * @param pEstimateShipWindowMax the estimateShipWindowMax to set.
	 */
	public void setEstimateShipWindowMax(int pEstimateShipWindowMax) {
		mEstimateShipWindowMax = pEstimateShipWindowMax;
	}

	/** property: holds ShipMethodVOs. **/
	private List<TRUShipMethodVO> mShipMethodVOs;

	/**
	 * Gets the ship method v os.
	 *
	 * @return the shipMethodVOs.
	 */
	public List<TRUShipMethodVO> getShipMethodVOs() {
		return mShipMethodVOs;
	}

	/**
	 * Sets the ship method v os.
	 *
	 * @param pShipMethodVOs the shipMethodVOs to set.
	 */
	public void setShipMethodVOs(List<TRUShipMethodVO> pShipMethodVOs) {
		mShipMethodVOs = pShipMethodVOs;
	}

	/** property to hold selected index. **/
	private int mSelectedIndex;

	/**
	 * Gets the selected index.
	 *
	 * @return the selectedIndex.
	 */
	public int getSelectedIndex() {
		return mSelectedIndex;
	}

	/**
	 * Sets the selected index.
	 *
	 * @param pSelectedIndex the selectedIndex to set.
	 */
	public void setSelectedIndex(int pSelectedIndex) {
		mSelectedIndex = pSelectedIndex;
	}

	/** property to hold eligible shipping methods count. **/
	private int mEligibleShipMethodsCount;

	/**
	 * Gets the eligible ship methods count.
	 *
	 * @return the eligibleShipMethodsCount.
	 */
	public int getEligibleShipMethodsCount() {
		return mEligibleShipMethodsCount;
	}

	/**
	 * Sets the eligible ship methods count.
	 *
	 * @param pEligibleShipMethodsCount the eligibleShipMethodsCount to set.
	 */
	public void setEligibleShipMethodsCount(int pEligibleShipMethodsCount) {
		mEligibleShipMethodsCount = pEligibleShipMethodsCount;
		mShipMethodVOs = new ArrayList<TRUShipMethodVO>(mEligibleShipMethodsCount);

		final DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();

		for (int index = 0; index < mEligibleShipMethodsCount; index++) {
			TRUShipMethodVO shipMethodVO = new TRUShipMethodVO();
			final String shipMethodName = request.getParameter(TRUCommerceConstants.SHIP_METHOD_NAME + index);
			final String shipMethodCode = request.getParameter(TRUCommerceConstants.SHIP_METHOD + index);

			double shippingPrice = TRUConstants.DOUBLE_ZERO;
			try {
				shippingPrice = Double.parseDouble(request.getParameter(TRUCommerceConstants.SHIP_PRICE + index));
			} catch(NumberFormatException nfe) {
				shippingPrice = TRUConstants.DOUBLE_ZERO;
				if (isLoggingError()) {
					logError("Exception in @Class::TRUShippingGroupFormHandler:::@method:::setEligibleShipMethodsCount",nfe);
				}
			}
			final String regionCode = request.getParameter(TRUCommerceConstants.REGION_CODE + index);

			shipMethodVO.setShipMethodName(shipMethodName);
			shipMethodVO.setShipMethodCode(shipMethodCode);
			shipMethodVO.setShippingPrice(shippingPrice);
			shipMethodVO.setRegionCode(regionCode);
			mShipMethodVOs.add(index, shipMethodVO);
		}
	}

	/**
	 * <p>
	 * 	Will be called when user updates the shipping method in review page.
	 * </p>
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleChangeShipMethodInReview(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.handleChangeShipMethodInReview method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CHANGE_SHIP_METHOD_IN_REVIEW)) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				if(isLoggingDebug()){
					vlogDebug("handleChangeShipMethodInReview()");
				}
				PipelineResult result;
				synchronized (getOrder()) {
					preChangeShipMethodInReview(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in preChangeShipMethodInReview method");
						}
						return checkFormRedirect(getCommonSuccessURL(),
								getCommonErrorURL(), pRequest, pResponse);
					}
					changeShipMethodInReview(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in changeShipMethodInReview() method");
						}
						return checkFormRedirect(getCommonSuccessURL(),
								getCommonErrorURL(), pRequest, pResponse);
					}
					postChangeShipMethodInReview(pRequest, pResponse);

					runProcessRepriceOrder(getOrder(),
							getUserPricingModels(), getLocale(),
							getProfile(), createRepriceParameterMap());
					result = ((TRUOrderManager)getOrderManager()).adjustPaymentGroups(getOrder());
					if(result.hasErrors()){
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to Pipeline error in changeShipMethodInReview() method");
						}
						return checkFormRedirect(getCommonSuccessURL(),
								getCommonErrorURL(), pRequest, pResponse);
					}
					getOrderManager().updateOrder(getOrder());
				}
			} catch (RunProcessException e) {
				if (isLoggingError()) {
					logError(
							"RunProcessException in TRUShippingGroupFormHandler.handleChangeShipMethodInReview",
							e);
				}
			} catch (CommerceException e) {
				if (isLoggingError()) {
					logError(
							"CommerceException in TRUShippingGroupFormHandler.handleChangeShipMethodInReview",
							e);
				}
			} finally {
				if (tr != null)
				{
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CHANGE_SHIP_METHOD_IN_REVIEW);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.handleChangeShipMethodInReview method");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * Pre change ship method in review.
	 *
	 * @param pRequest            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException                if an error occurs.
	 * @exception IOException                if an error occurs.
	 */
	protected void preChangeShipMethodInReview(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		vlogDebug("Start: TRUShippingGroupFormHandler.preChangeShipMethodInReview()");
		if(isRestService()){
			preRestChangeShipMethodInReview(pRequest, pResponse);
		}
		vlogDebug("End: TRUShippingGroupFormHandler.preChangeShipMethodInReview()");
	}

	/**
	 * <p>
	 * Gets the input details and creates the shipGroupKey having the old shipping method.
	 * Find the shipGroupKey through all order relationships and compare to the above shipGroupKey.
	 * Update the new shipping method to the matched shipGroupKey's ShippingGroups.
	 * </p>
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 * @throws CommerceException CommerceException
	 */
	@SuppressWarnings("unchecked")
	protected void changeShipMethodInReview(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, CommerceException {
		vlogDebug("Start of TRUShippingGroupFormHandler.changeShipMethodInReview method");
		if (!checkFormRedirect(null, getCommonErrorURL(), pRequest, pResponse)) {
			return;
		}
		final TRUShippingGroupManager shippingGroupManager = (TRUShippingGroupManager) getShippingHelper().getPurchaseProcessHelper()
				.getShippingGroupManager();
		final TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
		TRUShipGroupKey oldShipGroupKey=null;
		String newShipMethodCode =null;
		double shippingPrice = TRUCheckoutConstants.MINIMUM_PRICE;
		String selectedRegionCode = null;
		//REST API changes
		if(isRestService()){
			oldShipGroupKey = setShippingDestinationInfo();
			if(oldShipGroupKey!=null){
				setShipToAddressName(oldShipGroupKey.getNickName());
			}
			newShipMethodCode = getShipMethodVO().getShipMethodCode();
			shippingPrice = getShipMethodVO().getShippingPrice();
			selectedRegionCode = getShipMethodVO().getRegionCode();
		}else{
			// From request parameters - Start
			newShipMethodCode = getShipMethodVOs().get(getSelectedIndex()).getShipMethodCode();
			shippingPrice = getShipMethodVOs().get(getSelectedIndex()).getShippingPrice();

			oldShipGroupKey = new TRUShipGroupKey(getShipMethodVOs());
			oldShipGroupKey.setEstimateShipWindowMin(getEstimateShipWindowMin());
			oldShipGroupKey.setEstimateShipWindowMax(getEstimateShipWindowMax());
			oldShipGroupKey.setSelectedShippingMethod(getShippingMethod());
			oldShipGroupKey.setNickName(getShipToAddressName());
			// From request parameters - End
		}

		// update relationship with target shipping group
		final List<Relationship> sgCiRels = shippingGroupManager.getAllShippingGroupRelationships(getOrder());
		if (sgCiRels != null && !sgCiRels.isEmpty()) {
			final int sgCiRelsCount = sgCiRels.size();
			for (int i = 0; i < sgCiRelsCount; ++i) {
				final TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) sgCiRels
						.get(i);
				final CommerceItem commerceItem = sgCiRel.getCommerceItem();
				final TRUShippingGroupManager groupManager = (TRUShippingGroupManager) getShippingGroupManager();
				if (groupManager.isNotInstanceOfTRUCommerceItem(commerceItem)) {
					continue;
				}
				List<RepositoryItem> getGiftItemInfos = sgCiRel.getGiftItemInfo();
				final ShippingGroup shippingGroup = sgCiRel.getShippingGroup();
				if (shippingGroup instanceof TRUHardgoodShippingGroup) {
					final TRUHardgoodShippingGroup oldShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
					final TRUShipGroupKey shipGroupKey = getShippingHelper().findShipGroupKey(getOrder(), sgCiRel);
					if (oldShipGroupKey != null && shipGroupKey != null && oldShipGroupKey.equals(shipGroupKey)) {
						final TRUHardgoodShippingGroup matchedShippingGroup = orderManager
								.getShipGroupByNickNameAndShipMethod(getOrder(), getShipToAddressName(), newShipMethodCode,oldShippingGroup.getShippingGroupClassType());
						if (matchedShippingGroup != null) {
							try {
								final ShippingGroupCommerceItemRelationship relationship = getShippingGroupManager()
										.getShippingGroupCommerceItemRelationship(getOrder(), commerceItem.getId(),
												matchedShippingGroup.getId());
								final long relQty = relationship.getQuantity();
								// If relationship found, remove relationship from matched shipping group
								getOrderManager().getCommerceItemManager().removeItemQuantityFromShippingGroup(getOrder(),
										commerceItem.getId(), matchedShippingGroup.getId(), relQty);
								// update matched shipping group to current relationship
								shippingGroupManager.updateSGCIRelationshipWithShippingGroup(getOrder(), sgCiRel,
										matchedShippingGroup);
								getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(),
										commerceItem.getId(), matchedShippingGroup.getId(), relQty);
								if (oldShippingGroup.isGiftReceipt()) {
									matchedShippingGroup.setGiftReceipt(true);
									matchedShippingGroup.setGiftWrapMessage(oldShippingGroup.getGiftWrapMessage());
									}
							} catch (RelationshipNotFoundException rNFExe) {
								shippingGroupManager.updateSGCIRelationshipWithShippingGroup(getOrder(), sgCiRel,
										matchedShippingGroup);
								if (isLoggingError()) {
									logError(
											"RelationshipNotFoundException occured while TRUShippingGroupFormHandler.changeShipMethodInReview()",
											rNFExe);
								}
							}
						} else {
							final TRUHardgoodShippingGroup newShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupManager()
									.createShippingGroup(oldShippingGroup.getShippingGroupClassType());
							getShippingGroupManager().addShippingGroupToOrder(getOrder(), newShippingGroup);

							if (oldShippingGroup instanceof TRUChannelHardgoodShippingGroup
									&& newShippingGroup instanceof TRUChannelHardgoodShippingGroup) {
								((TRUShippingGroupManager)getShippingGroupManager()).copyChannelShippingInfo(oldShippingGroup, newShippingGroup);
							}
							OrderTools.copyAddress(oldShippingGroup.getShippingAddress(),newShippingGroup.getShippingAddress());
							newShippingGroup.setNickName(getShipToAddressName());
							newShippingGroup.setShippingMethod(newShipMethodCode);
							newShippingGroup.setShippingPrice(shippingPrice);
							if(isRestService()){
								newShippingGroup.setRegionCode(selectedRegionCode);
							}

							if (oldShippingGroup.isGiftReceipt()) {
								newShippingGroup.setGiftReceipt(true);
								newShippingGroup.setGiftWrapMessage(oldShippingGroup.getGiftWrapMessage());
							}

							if (null != getGiftItemInfos && !getGiftItemInfos.isEmpty()) {
								for (RepositoryItem giftRepoItem : getGiftItemInfos) {
									final String giftWrapCommItemId = (String) giftRepoItem.getPropertyValue(getPropertyManager()
											.getGiftWrapCommItemId());
									if (StringUtils.isNotBlank(giftWrapCommItemId)) {
										final CommerceItem giftWrapCommerceItem = getOrder().getCommerceItem(giftWrapCommItemId);
										final int giftQuantiy = (int) giftWrapCommerceItem.getQuantity();
										if (giftWrapCommerceItem != null) {
											getOrderManager().getCommerceItemManager().removeItemQuantityFromShippingGroup(
													getOrder(), giftWrapCommerceItem.getId(), oldShippingGroup.getId(),
													giftWrapCommerceItem.getQuantity());
											getGiftingProcessHelper().removeGiftItemQuantity(getOrder(), sgCiRel,
													giftWrapCommerceItem, giftQuantiy);
										}

										// assign the gw item to new SG
										if (giftWrapCommerceItem != null) {
											getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(),
													giftWrapCommerceItem.getId(), newShippingGroup.getId(),
													giftWrapCommerceItem.getQuantity());
											try {
												getGiftingProcessHelper().addGiftItemQuantity(getOrder(), sgCiRel,
														giftWrapCommerceItem, giftQuantiy);
											} catch (RepositoryException e) {
												if (isLoggingError()) {
													logError("Repository Error:::", e);
												}
											}
										}

									}
								}
							}
							shippingGroupManager.updateSGCIRelationshipWithShippingGroup(getOrder(), sgCiRel, newShippingGroup);
						}
						getShippingHelper().updateEstimateDeliveryDate(sgCiRel);
					}
				}
			}
			shippingGroupManager.removeEmptyShippingGroups(getOrder());
		}
		vlogDebug("End of TRUShippingGroupFormHandler.changeShipMethodInReview method");
	}

	/**
	 * set shipping destination details.
	 *
	 * @return pOldShipGroupKey - OldShipGroupKey
	 */

	private TRUShipGroupKey setShippingDestinationInfo() {

		if (isLoggingDebug()) {
			vlogDebug("Start: TRUShippingGroupFormHandler.setShippingDestinationInfo()");
		}
		TRUShipGroupKey shipGroupKey=null;
		if (StringUtils.isNotBlank(getRelationShipId())) {
			Relationship relationship=null;

			try {
				relationship = getOrder().getRelationship(getRelationShipId());
			} catch (CommerceException e) {
				if (isLoggingError()) {
					logError("CommerceException in TRUShippingGroupFormHandler.setShippingDestinationInfo", e);
				}
			}
			if (relationship instanceof TRUShippingGroupCommerceItemRelationship) {
				final TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) relationship;

				shipGroupKey = getShippingHelper().findShipGroupKey(getOrder(), sgCiRel);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End: TRUShippingGroupFormHandler.setShippingDestinationInfo()");
		}
		return shipGroupKey;


	}

	/**
	 * Post change ship method in review.
	 *
	 * @param pRequest            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException                if an error occurs.
	 * @exception IOException                if an error occurs.
	 */
	protected void postChangeShipMethodInReview(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start: TRUShippingGroupFormHandler.preChangeShipMethodInReview()");
		}


		if (isLoggingDebug()) {
			vlogDebug("End: TRUShippingGroupFormHandler.preChangeShipMethodInReview()");
		}
	}

	/**
	 * <p>
	 * 	Will be called when user updates the shipping method in review page using rest services.
	 * </p>
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleRestChangeShipMethodInReview(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.handleUpdateShipMethod method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SELECT_SHIP_METHOD)) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				preRestChangeShipMethodInReview(pRequest, pResponse);
				if (!getFormError()) {
					final String selectedShipMethodCode = getShipMethodVO().getShipMethodCode();
					final String selectedregionCode = getShipMethodVO().getRegionCode();
					final double selectedShippingPrice = getShipMethodVO().getShippingPrice();
					synchronized (getOrder()) {
						if (StringUtils.isNotBlank(getRelationShipId())) {
							final Relationship relationship = getOrder().getRelationship(getRelationShipId());
							if (relationship instanceof TRUShippingGroupCommerceItemRelationship) {
								final TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) relationship;
								final TRUHardgoodShippingGroup oldShippingGroup = (TRUHardgoodShippingGroup) getOrder()
										.getShippingGroup(getShippingGroupId());
								if (oldShippingGroup != null) {
									if (sgCiRel.getQuantity() >= TRUTaxwareConstant.NUMBER_ONE) {
										getOrderManager().getCommerceItemManager().removeItemQuantityFromShippingGroup(
												getOrder(), sgCiRel.getCommerceItem().getId(), oldShippingGroup.getId(),
												sgCiRel.getQuantity());
									}
									final TRUHardgoodShippingGroup existingShippingGroup = ((TRUOrderManager) getOrderManager())
											.getShipGroupByNickNameAndShipMethod(getOrder(), oldShippingGroup.getNickName(),
													selectedShipMethodCode);
									if (existingShippingGroup != null) {
										getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(),
												sgCiRel.getCommerceItem().getId(), existingShippingGroup.getId(),
												sgCiRel.getQuantity());
									} else {
										// Creating new shipping group and increase qty
										final TRUHardgoodShippingGroup newShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupManager()
												.createShippingGroup(oldShippingGroup.getShippingGroupClassType());
										if (newShippingGroup != null && oldShippingGroup != null) {
											OrderTools.copyAddress(oldShippingGroup.getShippingAddress(),
													newShippingGroup.getShippingAddress());
										}
										if (newShippingGroup != null) {
											if (oldShippingGroup instanceof TRUChannelHardgoodShippingGroup &&
													newShippingGroup instanceof TRUChannelHardgoodShippingGroup) {
												((TRUShippingGroupManager)getShippingGroupManager()).copyChannelShippingInfo(oldShippingGroup, newShippingGroup);
											}
											getShippingGroupManager().addShippingGroupToOrder(getOrder(), newShippingGroup);
											newShippingGroup.setNickName(oldShippingGroup.getNickName());
											newShippingGroup.setShippingMethod(selectedShipMethodCode);
											newShippingGroup.setShippingPrice(selectedShippingPrice);
											newShippingGroup.setRegionCode(selectedregionCode);
											// assign the item to new SG
											getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(),
													sgCiRel.getCommerceItem().getId(), newShippingGroup.getId(),
													sgCiRel.getQuantity());
										}
									}
								}
							}
						}
						getShippingGroupManager().removeEmptyShippingGroups(getOrder());
						runProcessRepriceOrder(getOrder(), getUserPricingModels(), getLocale(), getProfile(),
								createRepriceParameterMap());
						getOrderManager().updateOrder(getOrder());
					}
				}
			} catch (RunProcessException e) {
				if (isLoggingError()) {
					logError("RunProcessException in TRUShippingGroupFormHandler.handleUpdateShipMethod", e);
				}
			} catch (CommerceException e) {
				if (isLoggingError()) {
					logError("CommerceException in TRUShippingGroupFormHandler.handleUpdateShipMethod", e);
				}
			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SELECT_SHIP_METHOD);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.handleUpdateShipMethod method");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * Pre rest change ship method in review page. This method is used to validate the required parameters from the rest service.
	 *
	 * @param pRequest            the request
	 * @param pResponse            the response
	 * @throws ServletException the servlet exception
	 */
	protected void preRestChangeShipMethodInReview(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException {
		if (isLoggingDebug()) {
			vlogDebug("Start: TRUShippingGroupFormHandler.preRestChangeShipMethodInReview()");
		}
		if (StringUtils.isBlank(getRelationShipId()) || StringUtils.isBlank(getShipMethodVO().getRegionCode())
				|| StringUtils.isBlank(getShipMethodVO().getShipMethodCode())) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDRESS_MISSING_INFO, null);
			getErrorHandler().processException(mVe, this);
		}
		if (isLoggingDebug()) {
			vlogDebug("End: TRUShippingGroupFormHandler.preRestChangeShipMethodInReview()");
		}
	}



	/**
	 * Gets the shipping pricing engine.
	 *
	 * @return the shippingPricingEngine
	 */

	public TRUShippingPricingEngine getShippingPricingEngine() {
		return mShippingPricingEngine;
	}

	/**
	 * Sets the shipping pricing engine.
	 *
	 * @param pShippingPricingEngine the shippingPricingEngine to set
	 */

	public void setShippingPricingEngine(
			TRUShippingPricingEngine pShippingPricingEngine) {
		mShippingPricingEngine = pShippingPricingEngine;
	}


	/**
	 * <p>This method will be invoked when clicks 'ship to single address' on multi ship.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * 
	 * @return redirection result.
	 * 
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	@SuppressWarnings("unchecked")
	public boolean handleApplySingleShipping(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.handleApplySingleShipping method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean hasException = false;
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_APPLY_SINGLE_SHIPPING)) {
			Transaction tr = null;
			try {
				final TRUOrderImpl order = (TRUOrderImpl) getOrder();
				final TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) getShippingGroupMapContainer();
				final TRUShippingProcessHelper shippingHelper = getShippingHelper();
				final TRUShippingGroupManager shippingGroupManager = (TRUShippingGroupManager) shippingHelper.getPurchaseProcessHelper().getShippingGroupManager();

				tr = ensureTransaction();
				synchronized (order) {
					order.setOrderIsInMultiShip(Boolean.FALSE);
					final List<Relationship> updateSGCIRelList = new ArrayList<Relationship>();
					final String defaultNickName = getShippingHelper().findDefaultShippingAddress(shippingGroupMapContainer);
					final TRUHardgoodShippingGroup containerShippingGroup = (TRUHardgoodShippingGroup) shippingGroupMapContainer.getShippingGroup(defaultNickName);
					// creating default shipping group
					final TRUHardgoodShippingGroup defaultShippingGroup = (TRUHardgoodShippingGroup) shippingGroupManager
							.createShippingGroup();
					shippingGroupManager.addShippingGroupToOrder(order, defaultShippingGroup);
					if(containerShippingGroup != null && containerShippingGroup.isGiftReceipt()) {
						defaultShippingGroup.setGiftReceipt(Boolean.TRUE);
						defaultShippingGroup.setGiftWrapMessage(containerShippingGroup.getGiftWrapMessage());
					}
					if(StringUtils.isNotEmpty(defaultNickName) && containerShippingGroup != null) {
						defaultShippingGroup.setNickName(defaultNickName);
						OrderTools.copyAddress(containerShippingGroup.getShippingAddress(),
								defaultShippingGroup.getShippingAddress());
					}
					// Get all the commerceItems Relationships by each SG.
					final List<ShippingGroup> shippingGroups = order.getShippingGroups();
					for (ShippingGroup sg : shippingGroups) {
						if (sg instanceof TRUHardgoodShippingGroup) {
							final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) sg;
							updateSGCIRelList.addAll(hardgoodShippingGroup.getCommerceItemRelationships());
						}
					}
					if (!updateSGCIRelList.isEmpty()) {
						for (Object sgCIRel : updateSGCIRelList) {
							final TRUShippingGroupCommerceItemRelationship truSGCIRel = (TRUShippingGroupCommerceItemRelationship) sgCIRel;
							final CommerceItem commerceItem = truSGCIRel.getCommerceItem();
							final long quantity = truSGCIRel.getQuantity();
							final boolean isGiftItem = truSGCIRel.getIsGiftItem();
							final List<RepositoryItem> oldGiftItemInfo = truSGCIRel.getGiftItemInfo();

							getOrderManager().getCommerceItemManager().removeItemQuantityFromShippingGroup(order,
									commerceItem.getId(), truSGCIRel.getShippingGroup().getId(), quantity);
							getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(order,
									commerceItem.getId(), defaultShippingGroup.getId(), quantity);

							TRUShippingGroupCommerceItemRelationship sGCIR = (TRUShippingGroupCommerceItemRelationship) getShippingGroupManager()
									.getShippingGroupCommerceItemRelationship(order, commerceItem.getId(),
											defaultShippingGroup.getId());

							final List<RepositoryItem> newGiftItemInfo = sGCIR.getGiftItemInfo();

							if (oldGiftItemInfo != null && !oldGiftItemInfo.isEmpty()
									&& (newGiftItemInfo == null || newGiftItemInfo.isEmpty())) {
								getGiftingProcessHelper().mergeGiftItemInfo(order, oldGiftItemInfo, sGCIR);
//								sGCIR.setGiftItemInfo(oldGiftItemInfo);
							} else if (oldGiftItemInfo != null && !oldGiftItemInfo.isEmpty() && newGiftItemInfo != null
									&& !newGiftItemInfo.isEmpty()) {
								// Merge giftwarp old GiftItemInfo into new GiftItemInfo
								// Associate the updated GiftItemInfo to current SGRel
								getGiftingProcessHelper().mergeGiftItemInfo(order, oldGiftItemInfo, sGCIR);
							}

							if (isGiftItem) {
								sGCIR.setIsGiftItem(true);
							}
						}
					}
					
					shippingHelper.updateShippingMethodAndPrice(order, defaultShippingGroup,shippingGroupMapContainer);
					getShippingGroupManager().removeEmptyShippingGroups(order);
					runProcessRepriceOrder(order, getUserPricingModels(), getLocale(), getProfile(), createRepriceParameterMap());
					getOrderManager().updateOrder(order);
				}
			} catch (CommerceException e) {
				hasException = true;
				if (isLoggingError()) {
					logError("CommerceException in TRUShippingGroupFormHandler.handleApplySingleShipping", e);
				}
			} catch (RunProcessException e) {
				hasException = true;
				if (isLoggingError()) {
					logError("RunProcessException in TRUShippingGroupFormHandler.handleApplySingleShipping", e);
				}
			} catch (RepositoryException e) {
				hasException = true;
				if (isLoggingError()) {
					logError("RepositoryException", e);
				}
			} finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in TRUInStorePickUpFormHandler.handleInitRegistryShippingGroups", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_APPLY_SINGLE_SHIPPING);
				}
			}
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * <p>Handle 'saved address' case.</p>
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * 
	 * @return redirection result.
	 * 
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	public boolean handleSelectExistingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.handleSelectExistingAddress method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean hasException=false;
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SELECT_EXISTING_ADDRESS)) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					final String shipToAddressName =getShipToAddressName();
					preSelectExistingAddress(pRequest, pResponse);
					if(!getFormError()){
						final TRUShippingGroupCommerceItemRelationship newShpGrpCIRel = selectExistingAddress(pRequest, pResponse);
						runProcessRepriceOrder(getOrder(),
								getUserPricingModels(), getLocale(),
								getProfile(), createRepriceParameterMap());
						//updating CartItemDetailVO for the presentation layer
						if (newShpGrpCIRel != null) {
							updateCartItemDetailVO(newShpGrpCIRel);
						}
						getOrderManager().updateOrder(getOrder());
						/*Start : The below snippet added for handling the changing default address scenarios*/
						final TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
						final String currentDefaultShippingNickname =getShippingGroupMapContainer().getDefaultShippingGroupName();
						final boolean isDefaultAddressChanged=profileTools.isDefaultShippingAddressChangedOnProfile(
								getProfile(),currentDefaultShippingNickname);
						final boolean  isPrevShippingAddress= getShippingHelper().isOrderHavingPrevShippingAddress(
								shipToAddressName, getProfile());
						if(isDefaultAddressChanged && isPrevShippingAddress && StringUtils.isNotBlank(currentDefaultShippingNickname)){
							// Updating the previous address on profile
							profileTools.updatePreviousShippingAddress((MutableRepositoryItem)getProfile(),currentDefaultShippingNickname);
						}
						/*End : The below snippet added for handling the changing default address scenarios*/
						postSelectExistingAddress(pRequest,pResponse);
					}
				}
			}  catch (CommerceException e) {
				hasException=false;
				addFormException(new DropletException(PLEASE_TRY_AGAIN));
				if (isLoggingError()) {
					logError("CommerceException in TRUShippingGroupFormHandler.handleSelectExistingAddress", e);
				}
			}  catch (RepositoryException e) {
				hasException=false;
				addFormException(new DropletException(PLEASE_TRY_AGAIN));
				if (isLoggingError()) {
					logError("RepositoryException in TRUShippingGroupFormHandler.handleSelectExistingAddress", e);
				}
			} catch (RunProcessException e) {
				hasException=false;
				addFormException(new DropletException(PLEASE_TRY_AGAIN));
				if (isLoggingError()) {
					logError("RunProcessException in TRUShippingGroupFormHandler.handleSelectExistingAddress", e);
				}

			} finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in TRUShippingGroupFormHandler.handleSelectExistingAddress", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SELECT_EXISTING_ADDRESS);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.handleSelectExistingAddress method");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(),
				pRequest, pResponse);
	}


	/**
	 * Setup single shipping details for shipping to a existing address.
	 *
	 * @param pRequest            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse            a <code>DynamoHttpServletResponse</code> value.
	 * @return the TRU shipping group commerce item relationship
	 * @throws CommerceException the commerce exception
	 * @throws RepositoryException the repository exception
	 */
	protected TRUShippingGroupCommerceItemRelationship selectExistingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws CommerceException, RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUShippingGroupFormHandler.selectExistingAddress method");
		}
		// current shipping group
		final TRUShippingGroupManager shippingGroupManager = (TRUShippingGroupManager) getShippingHelper().getPurchaseProcessHelper()
				.getShippingGroupManager();
		final TRUOrderTools orderTools = (TRUOrderTools) getOrderManager().getOrderTools();
		TRUShippingGroupCommerceItemRelationship newShpGrpCIRel = null;
		if (StringUtils.isNotEmpty(getShipToAddressName())) {
			// Getting CIRItem
			final TRUShippingGroupCommerceItemRelationship oldShpGrpCIRel = (TRUShippingGroupCommerceItemRelationship) getOrder()
					.getRelationship(getRelationShipId());
			final CommerceItem currentItem = oldShpGrpCIRel.getCommerceItem();
			CommerceItem giftWrapCommerceItem = null;
			try {
				if (StringUtils.isNotBlank(getWrapCommerceItemId())) {
					giftWrapCommerceItem = getOrder().getCommerceItem(getWrapCommerceItemId());
				}
			} catch (CommerceItemNotFoundException ceE) {
				if (isLoggingError()) {
					logError("CommerceItemNotFoundException in TRUShippingGroupFormHandler.selectExistingAddress", ceE);
				}
			}
			// getting container shipping group
			final TRUHardgoodShippingGroup containerShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupMapContainer()
					.getShippingGroup(getShipToAddressName());
			containerShippingGroup.setOrderShippingAddress(true);
			// Getting prev shipping group and decrease qty
			final TRUHardgoodShippingGroup oldShippingGroup = (TRUHardgoodShippingGroup) getOrder().getShippingGroup(
					getShippingGroupId());
			// Create new shipping group and increase qty
			TRUHardgoodShippingGroup existingShippingGroup = ((TRUOrderManager) getOrderManager())
					.getShipGroupByNickNameAndShipMethod(getOrder(),
							getShipToAddressName(),
							oldShippingGroup);
			if(existingShippingGroup == null){
				List<TRUHardgoodShippingGroup> matchedShipGrps= ((TRUOrderManager) getOrderManager()).getShipGroupsByNickName(getOrder(), getShipToAddressName() , oldShippingGroup);
				if(matchedShipGrps!=null && !matchedShipGrps.isEmpty()){
					String leastShipMethod = TRUConstants.EMPTY;
					double leastShippingPrice = TRUConstants.DOUBLE_ZERO;
					/*if(StringUtils.isBlank(oldShipMethod)){*/
					CommerceItem  commerceItem = oldShpGrpCIRel.getCommerceItem();
					final String region = getShippingManager().getShippingRegion(matchedShipGrps.get(TRUConstants._0).getShippingAddress());
					List<TRUShipMethodVO> shipMethodVOs = new ArrayList<TRUShipMethodVO>();
					//Logic for retrieving the ship methods for a particular item
					final String frieghtClass = getShippingManager().getFreightClassFromCI(commerceItem);
					if(StringUtils.isNotBlank(frieghtClass)){
						shipMethodVOs = getShippingManager().getShipMethods(frieghtClass, region, getOrder(), true);
					}
					if (null != shipMethodVOs && !shipMethodVOs.isEmpty()) {
						leastShipMethod = shipMethodVOs.get(TRUConstants._0).getShipMethodCode();
						leastShippingPrice = shipMethodVOs.get(TRUConstants._0).getShippingPrice();
					}
					/*}	*/
					for(TRUHardgoodShippingGroup shpg:matchedShipGrps){
						if(shpg.getShippingMethod().equalsIgnoreCase(leastShipMethod) && leastShippingPrice == shpg.getShippingPrice()){
							existingShippingGroup = shpg;
							break;
						}
					}
				}
			}

			if (oldShippingGroup != null) {
				if (oldShpGrpCIRel.getQuantity() == TRUTaxwareConstant.NUMBER_ONE && null != oldShpGrpCIRel.getBppItemInfo()
						&& existingShippingGroup != null) {
					final TRUBppItemInfo bppItemInfo = new TRUBppItemInfo();
					oldShpGrpCIRel.setBppItemInfo(bppItemInfo);
				}
				if (oldShpGrpCIRel.getQuantity() >= TRUTaxwareConstant.NUMBER_ONE) {
					getOrderManager().getCommerceItemManager().removeItemQuantityFromShippingGroup(getOrder(),
							currentItem.getId(), oldShippingGroup.getId(), TRUTaxwareConstant.NUMBER_ONE);
					if (giftWrapCommerceItem != null) {
						getOrderManager().getCommerceItemManager().removeItemQuantityFromShippingGroup(getOrder(),
								giftWrapCommerceItem.getId(), oldShippingGroup.getId(), TRUTaxwareConstant.NUMBER_ONE);
						getGiftingProcessHelper().removeGiftItemQuantity(getOrder(), oldShpGrpCIRel, giftWrapCommerceItem,
								TRUTaxwareConstant.NUMBER_ONE);
					}
				}

				if (existingShippingGroup != null) {
					getShippingHelper().setShipMethodAndPrice(getOrder(), currentItem, existingShippingGroup,
							oldShippingGroup.getShippingMethod());
					getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(), currentItem.getId(),
							existingShippingGroup.getId(), TRUTaxwareConstant.NUMBER_ONE);
					newShpGrpCIRel = (TRUShippingGroupCommerceItemRelationship) getShippingGroupManager()
							.getShippingGroupCommerceItemRelationship(getOrder(), currentItem.getId(),
									existingShippingGroup.getId());
					// assign the gw item to existing SG
					if (giftWrapCommerceItem != null) {
						getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(),
								giftWrapCommerceItem.getId(), existingShippingGroup.getId(), TRUTaxwareConstant.NUMBER_ONE);
						getGiftingProcessHelper().addGiftItemQuantity(getOrder(), newShpGrpCIRel, giftWrapCommerceItem,
								TRUTaxwareConstant.NUMBER_ONE);
					}
					if (!existingShippingGroup.isGiftReceipt() && oldShippingGroup.isGiftReceipt()) {
						existingShippingGroup.setGiftReceipt(true);
						existingShippingGroup.setGiftWrapMessage(oldShippingGroup.getGiftWrapMessage());
					}
				} else {
					final TRUHardgoodShippingGroup newShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupManager()
							.createShippingGroup(oldShippingGroup.getShippingGroupClassType());
					newShippingGroup.setShippingMethod(oldShippingGroup.getShippingMethod());
					getShippingGroupManager().addShippingGroupToOrder(getOrder(), newShippingGroup);

					if (newShippingGroup != null) {
						if (oldShippingGroup instanceof TRUChannelHardgoodShippingGroup
								&& newShippingGroup instanceof TRUChannelHardgoodShippingGroup) {
							((TRUShippingGroupManager)getShippingGroupManager()).copyChannelShippingInfo(oldShippingGroup, newShippingGroup);
						}
						if (containerShippingGroup != null) {
							newShippingGroup.setRegistryAddress(containerShippingGroup.isRegistryAddress());
							OrderTools.copyAddress(containerShippingGroup.getShippingAddress(),
									newShippingGroup.getShippingAddress());
						}
						newShippingGroup.setNickName(getShipToAddressName());
						getShippingHelper().setShipMethodAndPrice(getOrder(), currentItem, newShippingGroup,
								oldShippingGroup.getShippingMethod());
						// assign the item to new SG
						getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(), currentItem.getId(),
								newShippingGroup.getId(), TRUTaxwareConstant.NUMBER_ONE);
						newShpGrpCIRel = (TRUShippingGroupCommerceItemRelationship) getShippingGroupManager()
								.getShippingGroupCommerceItemRelationship(getOrder(), currentItem.getId(),
										newShippingGroup.getId());

						// assign the gw item to new SG
						if (giftWrapCommerceItem != null) {
							getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(),
									giftWrapCommerceItem.getId(), newShippingGroup.getId(), TRUTaxwareConstant.NUMBER_ONE);
							getGiftingProcessHelper().addGiftItemQuantity(getOrder(), newShpGrpCIRel, giftWrapCommerceItem,
									TRUTaxwareConstant.NUMBER_ONE);
						}
						newShippingGroup.setOrderShippingAddress(true);
					}
					// Copy relationship properties from old SG to new SG
					orderTools.copyShippingRelationShip(oldShpGrpCIRel, newShpGrpCIRel);
					
					if (containerShippingGroup.isGiftReceipt()) {
						newShippingGroup.setGiftReceipt(true);
						newShippingGroup.setGiftWrapMessage(containerShippingGroup.getGiftWrapMessage());
					}
				}

				if(oldShippingGroup.getCommerceItemRelationshipCount() == 0 ) {
					final TRUHardgoodShippingGroup containerOldShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupMapContainer()
							.getShippingGroup(oldShippingGroup.getNickName());
					if(containerOldShippingGroup!=null){
						containerOldShippingGroup.setOrderShippingAddress(false);
					}
				}
			}
		}
		shippingGroupManager.removeEmptyShippingGroups(getOrder());
		if (isLoggingDebug()) {
			vlogDebug("End of TRUShippingGroupFormHandler.shipToExistingAddress method");
		}
		return newShpGrpCIRel;
	}

	/**
	 * returns date in fomat DD/MM/YY *.
	 *
	 * @param pDays the days
	 * @return the relative date
	 */
	private Date getRelativeDate(int pDays) {
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DATE, pDays);
		//DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
		return now.getTime();
	}

	/**
	 * Pre rest change ship method in review page. This method is used to validate the required parameters from the rest service.
	 *
	 * @param pRequest            the request
	 * @param pResponse            the response
	 * @throws ServletException the servlet exception
	 */
	protected void preSelectExistingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException {
		if (isLoggingDebug()) {
			vlogDebug("Start: TRUShippingGroupFormHandler.preRestChangeShipMethodInReview()");
		}
		if (StringUtils.isBlank(getRelationShipId()) || StringUtils.isBlank(getShippingGroupId()) || StringUtils.isBlank(getShipToAddressName())) {
			mVe.addValidationError(
					TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDRESS_MISSING_INFO, null);
			getErrorHandler().processException(mVe, this);
		}
		if (isLoggingDebug()) {
			vlogDebug("End: TRUProfileFormHandler.preRestChangeShipMethodInReview()");
		}
	}

	/**
	 * <p>
	 * 	This method will be invoked on change of ship method in multi ship page.
	 * </p>
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleUpdateShipMethod(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.handleUpdateShipMethod method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SELECT_SHIP_METHOD)) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				final TRUOrderTools orderTools = (TRUOrderTools) getOrderManager().getOrderTools();
				final String selectedShipMethodCode = getShipMethodVO().getShipMethodCode();
				final String selectedregionCode = getShipMethodVO().getRegionCode();
				final double selectedShippingPrice = getShipMethodVO().getShippingPrice();
				TRUShippingGroupCommerceItemRelationship newShpGrpCIRel = null;
				synchronized (getOrder()) {
					CommerceItem giftWrapCommerceItem = null;
					try {
						if (StringUtils.isNotBlank(getWrapCommerceItemId())) {
							giftWrapCommerceItem = getOrder().getCommerceItem(getWrapCommerceItemId());
						}
					} catch (CommerceItemNotFoundException ceE) {
						if (isLoggingError()) {
							logError("CommerceItemNotFoundException in TRUShippingGroupFormHandler.selectExistingAddress", ceE);
						}
					}
					if (StringUtils.isNotBlank(getRelationShipId())) {
						final Relationship relationship = getOrder().getRelationship(getRelationShipId());
						if (relationship instanceof TRUShippingGroupCommerceItemRelationship) {
							final TRUShippingGroupCommerceItemRelationship oldShpGrpCIRel = (TRUShippingGroupCommerceItemRelationship) relationship;
							final TRUHardgoodShippingGroup oldShippingGroup = (TRUHardgoodShippingGroup) getOrder().getShippingGroup(
									getShippingGroupId());
							if (oldShippingGroup != null) {
								if (oldShpGrpCIRel.getQuantity() >= TRUTaxwareConstant.NUMBER_ONE) {
									getOrderManager().getCommerceItemManager().removeItemQuantityFromShippingGroup(getOrder(),
											oldShpGrpCIRel.getCommerceItem().getId(), oldShippingGroup.getId(),
											TRUTaxwareConstant.NUMBER_ONE);
									if (giftWrapCommerceItem != null) {
										getOrderManager().getCommerceItemManager().removeItemQuantityFromShippingGroup(
												getOrder(), giftWrapCommerceItem.getId(), oldShippingGroup.getId(),
												TRUTaxwareConstant.NUMBER_ONE);
										getGiftingProcessHelper().removeGiftItemQuantity(getOrder(), oldShpGrpCIRel,
												giftWrapCommerceItem, TRUTaxwareConstant.NUMBER_ONE);
									}
								}

								/*final TRUHardgoodShippingGroup existingShippingGroup = ((TRUOrderManager) getOrderManager())
										.getShipGroupByNickNameAndShipMethod(getOrder(), oldShippingGroup.getNickName(),
												selectedShipMethodCode);*/
								final TRUHardgoodShippingGroup existingShippingGroup = ((TRUOrderManager) getOrderManager())
										.getShipGroupByNickNameAndShipMethod(getOrder(), oldShippingGroup,selectedShipMethodCode);
								if (existingShippingGroup != null) {
									getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(),
											oldShpGrpCIRel.getCommerceItem().getId(), existingShippingGroup.getId(),
											TRUTaxwareConstant.NUMBER_ONE);
									newShpGrpCIRel = (TRUShippingGroupCommerceItemRelationship) getShippingGroupManager()
											.getShippingGroupCommerceItemRelationship(getOrder(),
													oldShpGrpCIRel.getCommerceItem().getId(), existingShippingGroup.getId());
									// assign the gw item to existing SG
									if (giftWrapCommerceItem != null) {
										getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(),
												giftWrapCommerceItem.getId(), existingShippingGroup.getId(),
												TRUTaxwareConstant.NUMBER_ONE);
										getGiftingProcessHelper().addGiftItemQuantity(getOrder(), newShpGrpCIRel,
												giftWrapCommerceItem, TRUTaxwareConstant.NUMBER_ONE);
									}
									if (!existingShippingGroup.isGiftReceipt() && oldShippingGroup.isGiftReceipt()) {
										existingShippingGroup.setGiftReceipt(true);
										existingShippingGroup.setGiftWrapMessage(oldShippingGroup.getGiftWrapMessage());
									}
								} else {
									// Creating new shipping group and increase qty
									final TRUHardgoodShippingGroup newShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupManager()
											.createShippingGroup(oldShippingGroup.getShippingGroupClassType());
									if (newShippingGroup != null && oldShippingGroup != null) {
										OrderTools.copyAddress(oldShippingGroup.getShippingAddress(),
												newShippingGroup.getShippingAddress());
									}
									if (newShippingGroup != null) {
										if (oldShippingGroup instanceof TRUChannelHardgoodShippingGroup
												&& newShippingGroup instanceof TRUChannelHardgoodShippingGroup) {
											((TRUShippingGroupManager)getShippingGroupManager()).copyChannelShippingInfo(oldShippingGroup, newShippingGroup);
										}
										getShippingGroupManager().addShippingGroupToOrder(getOrder(), newShippingGroup);
										newShippingGroup.setNickName(oldShippingGroup.getNickName());
										newShippingGroup.setShippingMethod(selectedShipMethodCode);
										newShippingGroup.setShippingPrice(selectedShippingPrice);
										newShippingGroup.setRegionCode(selectedregionCode);
										
										if (oldShippingGroup.isGiftReceipt()) {
											newShippingGroup.setGiftReceipt(true);
											newShippingGroup.setGiftWrapMessage(oldShippingGroup.getGiftWrapMessage());
										}
										
										// assign the item to new SG
										getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(),
												oldShpGrpCIRel.getCommerceItem().getId(), newShippingGroup.getId(),
												TRUTaxwareConstant.NUMBER_ONE);

										newShpGrpCIRel = (TRUShippingGroupCommerceItemRelationship) getShippingGroupManager()
												.getShippingGroupCommerceItemRelationship(getOrder(),
														oldShpGrpCIRel.getCommerceItem().getId(), newShippingGroup.getId());
										// assign the gw item to new SG
										if (giftWrapCommerceItem != null) {
											getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(),
													giftWrapCommerceItem.getId(), newShippingGroup.getId(),
													TRUTaxwareConstant.NUMBER_ONE);
											getGiftingProcessHelper().addGiftItemQuantity(getOrder(), newShpGrpCIRel,
													giftWrapCommerceItem, TRUTaxwareConstant.NUMBER_ONE);
										}
									}
								}
								orderTools.copyShippingRelationShip(oldShpGrpCIRel, newShpGrpCIRel);
							}
						}
					}
					getShippingGroupManager().removeEmptyShippingGroups(getOrder());
					runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL, getOrder(), getUserPricingModels(),
							getUserLocale(pRequest, pResponse), getProfile(), createRepriceParameterMap());
					// updating CartItemDetailVO for the presentation layer
					if (newShpGrpCIRel != null) {
						updateCartItemDetailVO(newShpGrpCIRel);
					}

					postUpdateShipMethod(newShpGrpCIRel);
					getOrderManager().updateOrder(getOrder());
				}
			} catch (RunProcessException e) {
				if (isLoggingError()) {
					logError("RunProcessException in TRUShippingGroupFormHandler.handleUpdateShipMethod", e);
				}
			} catch (CommerceException e) {
				if (isLoggingError()) {
					logError("CommerceException in TRUShippingGroupFormHandler.handleUpdateShipMethod", e);
				}
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError("RepositoryException in TRUShippingGroupFormHandler.handleUpdateShipMethod", e);
				}
			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SELECT_SHIP_METHOD);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.handleUpdateShipMethod method");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}


	/**
	 * to validate all the required field to change ship method.
	 *
	 * @param pNewShpGrpCIRel - TRUShippingGroupCommerceItemRelationship
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	protected void postUpdateShipMethod(
			TRUShippingGroupCommerceItemRelationship pNewShpGrpCIRel) throws ServletException,
			IOException {
		if(isLoggingDebug()){
			vlogDebug("START of TRUShippingGroupFormHandler.postUpdateShipMethod method ");
		}

		final String previousTab=((TRUOrderImpl)getOrder()).getPreviousTab();
		if(TRUCheckoutConstants.REVIEW_TAB.equals(previousTab)){
			getShippingHelper().updateEstimateDeliveryDate(pNewShpGrpCIRel);
			if(isLoggingDebug()){
				logDebug("EDD Updated");
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.postUpdateShipMethod method");
		}

	}
	
	/**
	 * Handle 'Ship to existing address' case.
	 *
	 * @param pRequest            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse            a <code>DynamoHttpServletResponse</code> value.
	 * @return redirection result.
	 * @exception ServletException                if an error occurs.
	 * @exception IOException                if an error occurs.
	 * @throws com.tru.common.cml.SystemException the system exception
	 */
	public boolean handleShipToMultiAddress(
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, com.tru.common.cml.SystemException{
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.handleShipToMultiAddress method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean hasException=false;
		TRUOrderImpl orderImpl= (TRUOrderImpl)getOrder();
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SHIP_TO_MULTI_ADDRESS)) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (orderImpl) {
					preShipToMultiAddress(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug(COMMON_LOG_1);
						}
						return checkFormRedirect(getCommonSuccessURL(),
								getCommonErrorURL(), pRequest, pResponse);
					}
					// Construct Redirect URL
					orderImpl.getActiveTabs().put(TRUCheckoutConstants.SHIPPING_TAB, Boolean.TRUE);
					if (orderImpl.isShowMeGiftingOptions()) {
						// Redirect URL: Gift Options page
						setApplyShippingGroupsSuccessURL(getTRUConfiguration().getGiftOptionsPageURL());
						orderImpl.getActiveTabs().put(TRUCheckoutConstants.GIFTING_TAB, Boolean.TRUE);
					} else {
						// Redirect URL: Payment page
						setApplyShippingGroupsSuccessURL(getTRUConfiguration().getPaymentPageURL());
						if(orderImpl.getActiveTabs().get(TRUCheckoutConstants.GIFTING_TAB) != null ){
							orderImpl.getActiveTabs().remove(TRUCheckoutConstants.GIFTING_TAB);
						}
						orderImpl.getActiveTabs().put(TRUCheckoutConstants.PAYMENT_TAB, Boolean.TRUE);
					}
					orderImpl.setApplyGiftingOptions(Boolean.FALSE);
					getUserSession().setSignInHidden(Boolean.TRUE);
					//Making Tax Reprice Call
					runProcessRepriceOrder(getOrder(), getUserPricingModels(), getLocale(), getProfile(),
							createRepriceParameterMap());
					postShipToMultiAddress(pRequest, pResponse);
					getOrderManager().updateOrder(getOrder());
				}
			} catch (RunProcessException e) {
				hasException=true;
				addFormException(new DropletException(PLEASE_TRY_AGAIN));
				if (isLoggingError()) {
					logError(
							"RunProcessException in TRUShippingGroupFormHandler.handleShipToMultiAddress",
							e);
				}
			} catch (CommerceException e) {
				hasException=true;
				addFormException(new DropletException(PLEASE_TRY_AGAIN));
				if (isLoggingError()) {
					logError(
							"CommerceException in TRUShippingGroupFormHandler.handleShipToMultiAddress",
							e);
				}
			} finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in TRUInStorePickUpFormHandler.handleInitRegistryShippingGroups", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SHIP_TO_MULTI_ADDRESS);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.handleShipToExistingAddress method");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(),
				pRequest, pResponse);
	}


	/**
	 * <p>
	 * 
	 * </p>.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	@SuppressWarnings("rawtypes")
	protected void preShipToMultiAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUShippingGroupFormHandler.preShipToMultiAddress method");
		}
		try {
			final List sgCiRels = getShippingGroupManager().getAllShippingGroupRelationships(getOrder());
			if (sgCiRels != null && !sgCiRels.isEmpty()) {
				final int sgCiRelsCount = sgCiRels.size();
				for (int i = 0; i < sgCiRelsCount; ++i) {
					final TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) sgCiRels
							.get(i);
					final CommerceItem commerceItem = sgCiRel.getCommerceItem();
					final TRUShippingGroupManager groupManager = (TRUShippingGroupManager) getShippingGroupManager();
					if (groupManager.isNotInstanceOfTRUCommerceItem(commerceItem)) {
						continue;
					}
					if (sgCiRel.getShippingGroup() instanceof HardgoodShippingGroup) {
						final TRUHardgoodShippingGroup sg = (TRUHardgoodShippingGroup) sgCiRel.getShippingGroup();
						final Address address = sg.getShippingAddress();
						if (isLoggingDebug()) {
							vlogDebug(
									"Shipping address.getFirstName() ={0} ,  address.getLastName()={1}, and ShippingMethod={2}",
									address.getFirstName(), address.getLastName(), sgCiRel.getShippingGroup().getShippingMethod());
						}
						if (address == null || StringUtils.isBlank(address.getFirstName()) ||
								StringUtils.isBlank(address.getLastName()) || StringUtils.isBlank(address.getAddress1())) {
							String errorMessage = getErrorMessage(TRUErrorKeys.TRU_ERROR_SHIPPING_ADDRESS_MISSING_INFO);
							addFormException(new DropletException(errorMessage, commerceItem.getCatalogRefId() + TRUConstants.IFAN));
							errorMessage = null;
							return;
						} else if (StringUtils.isBlank(sgCiRel.getShippingGroup().getShippingMethod())) {
							String errorMessage = getErrorMessage(TRUErrorKeys.TRU_ERROR_SHIPPING_NO_SHIP_METHODS_MULTISHIP);
							addFormException(new DropletException(errorMessage, sg.getId()));
							errorMessage = null;
							return;
						}

					}

				}
			}
			runProcessValidationShippingInfos(pRequest, pResponse);
		} catch (RunProcessException e) {
			addFormException(new DropletException(PLEASE_TRY_AGAIN));
			if (isLoggingError()) {
				logError("Exception in TRUShippingGroupFormHandler.preShipToMultiAddress", e);
			}
		} catch (CommerceException e) {
			addFormException(new DropletException(PLEASE_TRY_AGAIN));
			if (isLoggingError()) {
				logError("CommerceException in TRUShippingGroupFormHandler.preShipToMultiAddress", e);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUShippingGroupFormHandler.preShipToMultiAddress method");
		}
	}

	/**
	 * post method for ship to multiple addresses.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	protected void postShipToMultiAddress(
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if(isLoggingDebug()){
			vlogDebug("START of TRUShippingGroupFormHandler.postShipToMultiAddress method");
		}
		//Start : CR161
		final TRUOrderImpl order=(TRUOrderImpl) getOrder();
		if(order.getStorePickUpInfos()!=null && !order.getStorePickUpInfos().isEmpty()){
			order.getStorePickUpInfos().clear();
		}
		//End : CR161
		if(isLoggingDebug()){
			vlogDebug("END of TRUShippingGroupFormHandler.postShipToMultiAddress method");
		}
	}


	/**
	 * <p>
	 * 	This method will be invoked when "ship to multiple address" CTA clicked on single shipping page.
	 * </p>
	 * @throws CommerceException commerce exception
	 */
	private void applyMultiShipping() throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUShippingGroupFormHandler.applyMultiShipping()  for order id {0}",getOrder().getId());
		}
		((TRUShippingGroupManager)getShippingGroupManager()).validateShippingMethodAndPrice(getOrder());		
		if (isLoggingDebug()) {
			vlogDebug("End of TRUShippingGroupFormHandler.applyMultiShipping()  for order id {0}",getOrder().getId());
		}
	}

	/** booean to hold orderReconciliationNeeded. */
	private boolean mOrderReconciliationNeeded;


	/**
	 * Checks if is order reconciliation needed.
	 *
	 * @return orderReconciliationNeeded
	 */
	public boolean isOrderReconciliationNeeded() {
		return mOrderReconciliationNeeded;
	}

	/**
	 * Sets the order reconciliation needed.
	 *
	 * @param pOrderReconciliationNeeded OrderReconciliationNeeded
	 */
	public void setOrderReconciliationNeeded(boolean pOrderReconciliationNeeded) {
		mOrderReconciliationNeeded = pOrderReconciliationNeeded;
	}

	/**
	 * <p>
	 * This method will check whether all the cart items or eligible for selected shipping method or not.
	 * </p>.
	 *
	 * @return boolean
	 */
	private boolean isOrderReconciliationRequired() {
		boolean isOrderReconciliationRequired = false;
		final List<TRUShippingGroupCommerceItemRelationship> sgCiRels = ((TRUShippingGroupManager)getShippingGroupManager()).getOnlyShipToHomeItemRelationShips(getOrder());
		if (sgCiRels != null && sgCiRels.size() > TRUConstants._1 && !sgCiRels.isEmpty()) {
			final int sgCiRelsCount = sgCiRels.size();
			for (int i = 0; i < sgCiRelsCount; ++i) {
				final TRUCommerceItemImpl commerceItem = (TRUCommerceItemImpl) sgCiRels.get(i).getCommerceItem();
				final TRUShippingGroupManager groupManager = (TRUShippingGroupManager) getShippingGroupManager();
				if (groupManager.isNotInstanceOfTRUCommerceItem(commerceItem)) {
					continue;
				}
				TRUHardgoodShippingGroup  oldShippingGroup=(TRUHardgoodShippingGroup)sgCiRels.get(i).getShippingGroup();
				if (null !=  getShippingManager().isItemQualifyForShipMethod(commerceItem, oldShippingGroup, getOrder())) {
					String errorMessage = getErrorMessage(TRUErrorKeys.TRU_ERROR_SHIPPING_NO_PREFERRED_SHIP_METHOD);
					addFormException(new DropletException(errorMessage, commerceItem.getId()));
					if (isLoggingInfo()) {
						vlogInfo("{0} is not eligible for commerceItem {1} ,So order reconciliation required .",
								oldShippingGroup.getShippingMethod(), commerceItem.getId());
					}
					isOrderReconciliationRequired = true;
					errorMessage = null;
					break;
				}
			}
		}
		return isOrderReconciliationRequired;
	}


	/**
	 * Handle 'Ship to existing address' for single shipping.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * 
	 * @return redirection result.
	 * 
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	public boolean handleChangeShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.handleChangeShippingAddress method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean hasException = false;
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CHANGE_SHIPPING_ADDRESS)) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					preChangeShippingAddress(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in field validation() method");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					/* Start : The below snippet added for handling the changing default address scenarios */
					final TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
					final String currentAddressNickName = getShippingGroupMapContainer().getDefaultShippingGroupName();
					final boolean isDefaultAddressChanged = profileTools.isDefaultShippingAddressChangedOnProfile(getProfile(),
							currentAddressNickName);
					if (isDefaultAddressChanged) {
						final boolean isOrderHavingPrevShippingAddress = getShippingHelper().isOrderHavingPrevShippingAddress(
								getShipToAddressName(), getProfile());
						if (isOrderHavingPrevShippingAddress && StringUtils.isNotBlank(currentAddressNickName)) {
							setShipToAddressName(currentAddressNickName);
							shipToExistingAddress(pRequest, pResponse);
							// Updating the previous address on profile
							profileTools.updatePreviousShippingAddress((MutableRepositoryItem) getProfile(),
									getShippingGroupMapContainer().getDefaultShippingGroupName());
						} else {
							shipToExistingAddress(pRequest, pResponse);
						}
					} else {
						shipToExistingAddress(pRequest, pResponse);
					}
					/* End : The below snippet added for handling the changing default address scenarios */
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in handleChangeShippingAddress() method");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					runProcessValidationShippingInfos(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug(COMMON_LOG_1);
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					// Reprice
					runProcessRepriceOrder(getOrder(), getUserPricingModels(), getLocale(), getProfile(),
							createRepriceParameterMap());
					getOrderManager().updateOrder(getOrder());
				}
			} catch (RunProcessException e) {
				hasException = true;
				addFormException(new DropletException(PLEASE_TRY_AGAIN));
				if (isLoggingError()) {
					logError("RunProcessException in TRUShippingGroupFormHandler.handleChangeShippingAddress", e);
				}
			} catch (CommerceException e) {
				hasException = true;
				addFormException(new DropletException(PLEASE_TRY_AGAIN));
				if (isLoggingError()) {
					logError("CommerceException in TRUShippingGroupFormHandler.handleChangeShippingAddress", e);
				}
			}finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in TRUShippingGroupFormHandler.handleChangeShippingAddress", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CHANGE_SHIPPING_ADDRESS);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.handleChangeShippingAddress method");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * Performs input data validations for existing shipping address specified
	 * by shopper.
	 *
	 * @param pRequest a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse a <code>DynamoHttpServletResponse</code> value.
	 * @throws ServletException if an error occurs.
	 * @throws IOException if an error occurs.
	 */
	protected void preChangeShippingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {

		vlogDebug("Start: TRUShippingGroupFormHandler.preChangeShippingAddress()");

		vlogDebug("End: TRUProfileFormHandler.preChangeShippingAddress()");
	}

	/**
	 * getter for errorHandlerManager.
	 *
	 * @return mErrorHandlerManager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * Sets the errorHandlerManager.
	 *
	 * @param pErrorHandlerManager the mErrorHandlerManager to set
	 */
	public void setErrorHandlerManager(DefaultErrorHandlerManager pErrorHandlerManager) {
		mErrorHandlerManager = pErrorHandlerManager;
	}

	/**
	 * getter for addUpdateItem.
	 *
	 * @return mAddUpdateItem  the errorHandlerManager to set
	 */
	public boolean isAddUpdateItem() {
		return mAddUpdateItem;
	}

	/**
	 * Sets the shipping manager.
	 * 
	 * @param pAddUpdateItem
	 * 			the  mAddUpdateItem to set
	 */
	public void setAddUpdateItem(boolean pAddUpdateItem) {
		mAddUpdateItem = pAddUpdateItem;
	}

	/** Property to Hold Error handler manager. */
	private DefaultErrorHandlerManager mErrorHandlerManager;

	/** The m add update item. */
	private boolean mAddUpdateItem;

	/**
	 * Holds the shipping city value.
	 */
	private String mCity;




	/**
	 * Holds the mState field value.
	 */
	private String mState;

	/**
	 * Holds the mAdress1 field value.
	 */
	private String mAdress1;


	/**getter for state.
	 * 
	 * @return mState
	 */
	public String getState() {
		return mState;
	}

	/**
	 * setter for state.
	 * @param pState the mState to set
	 */
	public void setState(String pState) {
		mState = pState;
	}

	/**
	 * getter for mAdress1.
	 * @return mAdress1
	 */
	public String getAddress1() {
		return mAdress1;
	}

	/**
	 * setter for address1.
	 * @param pAddress1 the mAdress1 to set
	 */
	public void setAddress1(String pAddress1) {
		mAdress1 = pAddress1;
	}

	/**
	 * getter for mCity.
	 * @return city
	 */
	public String getCity() {
		return mCity;
	}

	/**
	 * setter for mCity.
	 * @param pCity - city
	 */
	public void setCity(String pCity) {
		mCity = pCity;
	}
	/**
	 * <p>
	 * 	This method will be invoked when address/state values are getting changed on shipping page.
	 * </p>
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleChangeShipMethodZone(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("START of TRUShippingGroupFormHandler.handleChangeShipMethodZone()");
		vlogDebug("State : {0} and Address1 :{1}", getState(), getAddress1());
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean hasException = false;
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CHANGE_SHIP_METHOD_ZONE)) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					final TRUHardgoodShippingGroup hardgoodShippingGroup = getShippingHelper().getPurchaseProcessHelper()
							.getFirstHardgoodShippingGroup(getOrder());
					if (hardgoodShippingGroup != null) {
						if(((TRUOrderManager)getOrderManager()).isEnableTransientShippingInfo()){
							final Address address =new Address();
							address.setAddress1(getAddress1());
							address.setState(getState());
							address.setCity(getCity());
							hardgoodShippingGroup.setTransientState(getState());
							getShippingHelper().setDefaultShipMethodAndPrice(getOrder(), hardgoodShippingGroup,address,hardgoodShippingGroup.getShippingMethod()) ;
						}else{
							final Address address =hardgoodShippingGroup.getShippingAddress();
							address.setAddress1(getAddress1());
							address.setState(getState());
							address.setCity(getCity());
							hardgoodShippingGroup.setShippingAddress(address);
							getShippingHelper().updateShippingMethodAndPrice(getOrder(), hardgoodShippingGroup,(TRUShippingGroupContainerService)getShippingGroupMapContainer());
						}						
						runProcessRepriceOrder(getOrder(), getUserPricingModels(), getLocale(), getProfile(),
								createRepriceParameterMap());
						getOrderManager().updateOrder(getOrder());
						if(isRestService()) {
							postChangeShipMethodZone(getOrder(),hardgoodShippingGroup.getShippingAddress());
						}
					}
				}
			} catch (RunProcessException e) {
				hasException = true;
				if (isLoggingError()) {
					logError("RunProcessException in TRUShippingGroupFormHandler.handleChangeShipMethodZone", e);
				}
			} catch (CommerceException e) {
				hasException = true;
				if (isLoggingError()) {
					logError("CommerceException in TRUShippingGroupFormHandler.handleChangeShipMethodZone", e);
				}
			} finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in TRUShippingGroupFormHandler.handleChangeShipMethodZone", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CHANGE_SHIP_METHOD_ZONE);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.handleChangeShipMethodZone()");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}
	/**
	 *	This method fetches available Ship Methods for REST Service.
	 * @param pOrder Order
	 * @param pShippingAddress Address
	 */
	private void postChangeShipMethodZone(Order pOrder, Address pShippingAddress) {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.postChangeShipMethodZone()");
		}
		TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService)getShippingGroupMapContainer();
		Map<String, Object> map=new HashMap<String, Object>();
		String defaultShipMethod = TRUConstants.EMPTY;
		map.put(TRUConstants.ORDER_PARAM,pOrder);
		map.put(TRUConstants.SHIPPING_GROUP_MAP_CONTAINER, shippingGroupMapContainer);
		map.put(TRUConstants.STATE_PARAM,getState());
		map.put(TRUConstants.CITY,getCity());
		map.put(TRUConstants.ADDRESS1_PARAM,getAddress1());
		map.put(TRUConstants.IS_NEW_ADDRESS_FORM,TRUConstants.TRUE);
		map.put(TRUConstants.ADDRESS_PARAM, pShippingAddress);
		List<TRUShipMethodVO> availableShippingMethods = getShippingHelper().getAvailableShippingMethods(map);
		setValidShipMethods(availableShippingMethods);
		if(availableShippingMethods != null && !availableShippingMethods.isEmpty()) {
			defaultShipMethod =	getShippingHelper().getDefaultShipMethod(pOrder, availableShippingMethods);
			if(StringUtils.isNotBlank(defaultShipMethod)) {
				setSelectedShippingMethod(defaultShipMethod);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.postChangeShipMethodZone()");
		}
	}

	/**
	 * Update cart item detail vo.
	 *
	 * @param pNewShpGrpCIRel the new shp group ci rel
	 */
	private void updateCartItemDetailVO(TRUShippingGroupCommerceItemRelationship pNewShpGrpCIRel){
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.updateCartItemDetailVO()");
		}
		//TRUHardgoodShippingGroup newShippGroup = null;
		final TRUShipmentVO shipmentVO = new TRUShipmentVO();
		TRUHardgoodShippingGroup newShippGroup = (TRUHardgoodShippingGroup) pNewShpGrpCIRel.getShippingGroup();
		//setUniqueId("r23280001_224600005_0");
		final TRUCartItemDetailVO truCartItemDetailVO = ((TRUOrderImpl) getOrder()).getCartItemDetailVOMap().get(getUniqueId());
		if(truCartItemDetailVO!=null){
			truCartItemDetailVO.setId(pNewShpGrpCIRel.getId());
			truCartItemDetailVO.setShipGroupId(newShippGroup.getId());
			truCartItemDetailVO.setShippingGroupName(newShippGroup.getNickName());
			truCartItemDetailVO.setSelectedShippingMethod(newShippGroup.getShippingMethod());
			truCartItemDetailVO.setParentQuantity(pNewShpGrpCIRel.getQuantity());
			final TRUShippingPriceInfo priceInfo = (TRUShippingPriceInfo) newShippGroup.getPriceInfo();
			if (priceInfo != null) {
				if(!newShippGroup.isMarkerShippingGroup()) {
					truCartItemDetailVO.setSelectedShippingMethodPrice(newShippGroup.getActualAmount());
				} else {
					truCartItemDetailVO.setSelectedShippingMethodPrice(priceInfo.getAmount());
				}
			} else {
				truCartItemDetailVO.setSelectedShippingMethodPrice(newShippGroup.getShippingPrice());
			}
			getCartModifierHelper().setBPPItemDetails(truCartItemDetailVO,
					(RepositoryItem)pNewShpGrpCIRel.getCommerceItem().getAuxiliaryData().getCatalogRef(),
					pNewShpGrpCIRel, (TRUItemPriceInfo) pNewShpGrpCIRel.getCommerceItem().getPriceInfo());
		}
		final String freightClass = getShippingManager().getFreightClassFromCI(pNewShpGrpCIRel.getCommerceItem());
		final String region = getShippingManager().getShippingRegion(newShippGroup.getShippingAddress());
		final List<TRUShipMethodVO> shipMethodVOs = getShippingManager().getShipMethods (
				freightClass, region, getOrder(), true);

		shipmentVO.setShipMethodVOs(shipMethodVOs);
		shipmentVO.setShippingGroup(newShippGroup);
		shipmentVO.setSelectedShippingMethod(newShippGroup.getShippingMethod());
		shipmentVO.setEstimateShipWindowMin(pNewShpGrpCIRel.getEstimateShipWindowMin());
		shipmentVO.setEstimateShipWindowMax(pNewShpGrpCIRel.getEstimateShipWindowMax());
		shipmentVO.setEstimateMinDate(getRelativeDate(pNewShpGrpCIRel.getEstimateShipWindowMin()));
		shipmentVO.setEstimateMaxDate(getRelativeDate(pNewShpGrpCIRel.getEstimateShipWindowMax()));
		double shippingPrice = TRUCommerceConstants.DOUBLE_ZERO;
		if (newShippGroup.getPriceInfo() != null) {
			if(!newShippGroup.isMarkerShippingGroup()) {
				shippingPrice = newShippGroup.getActualAmount();
			} else {
				shippingPrice = newShippGroup.getPriceInfo().getAmount();
			}
		}
		shipmentVO.setSelectedShippingPrice(shippingPrice);
		setCartItemDetailObj(truCartItemDetailVO);
		((TRUOrderImpl) getOrder()).setShipmentVO(shipmentVO);
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.updateCartItemDetailVO()");
		}
	}



	/**
	 * <p>
	 * This method will get the error message for givem error key.
	 * </p>
	 * @param pErrorKey - pErrorKey
	 * @return errorMessage - errorMessage
	 */
	private String getErrorMessage(String pErrorKey){
		String errorMessage=pErrorKey;
		try {
			errorMessage = getErrorHandlerManager().getErrorMessage(pErrorKey);
		} catch (com.tru.common.cml.SystemException e) {
			if(isLoggingError()){
				logError("com.tru.common.cml.SystemException @Class:::TRUShippingGroupFormHandler::@method::getErrorMessage()\t:"+e,e);
			}
		}
		return errorMessage;
	}

	/**
	 * Gets the integration status util.
	 *
	 * @return the integrationStatusUtil.
	 */
	public TRUIntegrationStatusUtil getIntegrationStatusUtil() {
		return mIntegrationStatusUtil;
	}

	/**
	 * Sets the integration status util.
	 *
	 * @param pIntegrationStatusUtil the integrationStatusUtil to set.
	 */
	public void setIntegrationStatusUtil(TRUIntegrationStatusUtil pIntegrationStatusUtil) {
		mIntegrationStatusUtil = pIntegrationStatusUtil;
	}


	/**
	 * <p>
	 * 		This method will be invoked when closing the address model from single and multi shipping.
	 * </p>
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return true/false the success/failure
	 * @throws ServletException the servlet Exception
	 * @throws IOException the IO Exception
	 */
	public boolean handleCloseAddressModel(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.handleCloseAddressModel method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean hasException = false;
		final TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		final RepositoryItem profile = getProfile();
		final boolean isAnonymousUser = profileTools.isAnonymousUser(profile);

		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CLOSE_ADDRESS_MODEL)) {
			Transaction tr = null;
			try {
				if (getShippingGroupMapContainer().getShippingGroupMap().isEmpty()) {
					if (isLoggingDebug()) {
						logDebug("ShippingGroupMapContainer is empty");
					}
					getShippingGroupMapContainer().setDefaultShippingGroupName(TRUConstants.EMPTY);
					((TRUShippingGroupContainerService)getShippingGroupMapContainer()).setSelectedAddressNickName(TRUConstants.EMPTY);
					return true;
				}
				preCloseAddressModel(pRequest, pResponse);
				if (!getFormError()) {
					return true;
				} else {
					getFormExceptions().clear();
				}
				// Reinitialize the shipping groups for logged in users
				if (!isAnonymousUser) {
					((TRUHardgoodShippingGroupInitializer) getShippingGroupInitializer()).initializeHardgood(
							(Profile) getProfile(), getShippingGroupMapContainer(), pRequest);
				}
				tr = ensureTransaction();
				synchronized (getOrder()) {
					final TRUOrderImpl order = (TRUOrderImpl) getOrder();
					getShippingHelper().updateDefaultShippingAddress(profile, order, getShippingGroupMapContainer());
					getShippingGroupManager().removeEmptyShippingGroups(getOrder());
					runProcessValidationShippingInfos(pRequest, pResponse);
					runProcessRepriceOrder(order, getUserPricingModels(), getLocale(), getProfile(), createRepriceParameterMap());
					getOrderManager().updateOrder(getOrder());
					// Updating the previous address on profile
					profileTools.updatePreviousShippingAddress((MutableRepositoryItem) getProfile(),
							getShippingGroupMapContainer().getDefaultShippingGroupName());
				}
			} catch (ShippingGroupInitializationException e1) {
				hasException = true;
				if (isLoggingError()) {
					logError("ShippingGroupInitializationException occured @TRUShippingGroupFormHandler.handleCloseAddressModel",
							e1);
				}
			} catch (RunProcessException rpe) {
				if (isLoggingError()) {
					logError("RunProcessException occured @TRUShippingGroupFormHandler.handleCloseAddressModel",
							rpe);
				}
			} catch (CommerceException ce) {
				if (isLoggingError()) {
					logError("CommerceException occured @TRUShippingGroupFormHandler.handleCloseAddressModel",
							ce);
				}
			} finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in TRUInStorePickUpFormHandler.handleCloseAddressModel", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_CLOSE_ADDRESS_MODEL);
				}

			}
		}

		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.handleCloseAddressModel method");
		}
		return true;
	}


	/**
	 * <p>
	 * This method will check whether the default address changed or not.If changed one error message will be constructed to track this.
	 * </p>
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException the servlet Exception
	 * @throws IOException the IO Exception
	 */
	public void preCloseAddressModel(
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if(isLoggingDebug()){
			logDebug("Start:TRUShippingGroupFormHandler.preCloseAddressModel");
		}
		final TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		final TRUShippingGroupContainerService lShippingGroupMapContainer=(TRUShippingGroupContainerService) getShippingGroupMapContainer();
		final String currentDefaultAddressNickname=lShippingGroupMapContainer.getDefaultShippingGroupName();
		final boolean isDefaultShippingAddressChanged=profileTools.isDefaultShippingAddressChangedOnProfile(
				getProfile(),currentDefaultAddressNickname);
		if(isDefaultShippingAddressChanged){
			if(isRestService()){
				mVe.addValidationError(TRUConstants.DEFAULT_SHIPPING_ADDRESS_CHANGED, null);
				getErrorHandler().processException(mVe, this);
			}else{
				addFormException(new DropletException(TRUConstants.DEFAULT_SHIPPING_ADDRESS_CHANGED));
			}
		}
		if(isLoggingDebug()){
			logDebug("End:TRUShippingGroupFormHandler.preCloseAddressModel() isDefaultShippingAddressChanged? "+isDefaultShippingAddressChanged);
		}
	}

	/** The Shipping group initializer. */
	private ShippingGroupInitializer mShippingGroupInitializer;

	/**
	 * Gets shippingGroupInitialiser.
	 *
	 * @return ShippingGroupInitializer the shippingGroupInitialiser
	 */
	public ShippingGroupInitializer getShippingGroupInitializer() {
		return mShippingGroupInitializer;
	}

	/**
	 * Sets the shippingGroupInitialiser.
	 *
	 * @param pShippingGroupInitializer the shippingGroupInitialiser
	 */
	public void setShippingGroupInitializer(
			ShippingGroupInitializer pShippingGroupInitializer) {
		mShippingGroupInitializer = pShippingGroupInitializer;
	}


	/**
	 * to validate all the required field to change ship method.
	 *
	 * @param pRequest - request
	 * @param pResponse -response
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	protected void postSelectExistingAddress(
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if(isLoggingDebug()){
			vlogDebug("START of TRUShippingGroupFormHandler.postChangeShipMethod method");
		}

		final String previousTab=((TRUOrderImpl)getOrder()).getPreviousTab();
		if(TRUCheckoutConstants.REVIEW_TAB.equals(previousTab)){
			getShippingHelper().updateEstimateDeliveryDates(getOrder());
			if(isLoggingDebug()){
				logDebug("EDD Updated");
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.postChangeShipMethod method");
		}

	}

	/**
	 * This method used to trim the address values in a contactInfo bean.
	 * 
	 * @param pAddress
	 *            The map to hold the address details entered by the user
	 */
	public void trimContactInfoValues(ContactInfo pAddress) {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.trimContactInfoValues method");
		}
		if (StringUtils.isNotBlank(pAddress.getFirstName())) {
			getAddress().setFirstName(pAddress.getFirstName().trim());
		}
		if (StringUtils.isNotBlank(pAddress.getLastName())) {
			getAddress().setLastName(pAddress.getLastName().trim());
		}
		if (StringUtils.isNotBlank(pAddress.getAddress1())) {
			getAddress().setAddress1(pAddress.getAddress1().trim());
		}
		if (StringUtils.isNotBlank(pAddress.getAddress2())) {
			getAddress().setAddress2(pAddress.getAddress2().trim());
		}
		if (StringUtils.isNotBlank(pAddress.getCity())) {
			getAddress().setCity(pAddress.getCity().trim());
		}
		if (StringUtils.isNotBlank(pAddress.getState())) {
			getAddress().setState(pAddress.getState().trim());
		}
		if (StringUtils.isNotBlank(pAddress.getPostalCode())) {
			getAddress().setPostalCode(pAddress.getPostalCode().trim());
		}
		if (StringUtils.isNotBlank(pAddress.getPhoneNumber())) {
			getAddress().setPhoneNumber(pAddress.getPhoneNumber().trim());
		}
		if (StringUtils.isNotBlank(pAddress.getCountry())) {
			getAddress().setCountry(pAddress.getCountry().trim());
		}
	}

	/**
	 * Gets the error keys list.
	 *
	 * @return the errorKeysList
	 */
	public List<String> getErrorKeysList() {
		return mErrorKeysList;
	}

	/**
	 * Sets the error keys list.
	 *
	 * @param pErrorKeysList the errorKeysList to set
	 */
	public void setErrorKeysList(List<String> pErrorKeysList) {
		mErrorKeysList = pErrorKeysList;
	}

	/**
	 * Handle 'Ship to new address' case.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * 
	 * @return redirection result.
	 * 
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	public boolean handleUpdateRelationShips(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.handleUpdateRelationShips method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean hasException = false;
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_UPDATE_RELATIONSHIPS)) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					if(((TRUOrderImpl)getOrder()).isRepriceRequiredOnCart()) {
						runProcessRepriceOrder(getOrder(), getUserPricingModels(), getLocale(), getProfile(),
								createRepriceParameterMap());
						((TRUOrderImpl)getOrder()).setRepriceRequiredOnCart(Boolean.FALSE);
						getOrderManager().updateOrder(getOrder());
					} else if(updateRelationShips(pRequest, pResponse)) {
						getShippingGroupManager().removeEmptyShippingGroups(getOrder());
						runProcessRepriceOrder(getOrder(), getUserPricingModels(), getLocale(), getProfile(),
								createRepriceParameterMap());
						getOrderManager().updateOrder(getOrder());
					}
				}
			} catch (RunProcessException re) {
				hasException = true;
				if (isLoggingError()) {
					logError("RunProcessException in TRUShippingGroupFormHandler.handleUpdateRelationShips", re);
				}
			} catch (CommerceException ce) {
				hasException = true;
				if (isLoggingError()) {
					logError("CommerceException in TRUShippingGroupFormHandler.handleUpdateRelationShips", ce);
				}
			} catch (TRUPurchaseProcessException te) {
				hasException = true;
				if (isLoggingError()) {
					logError("TRUPurchaseProcessException in TRUShippingGroupFormHandler.handleUpdateRelationShips", te);
				}
			} finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in TRUShippingGroupFormHandler.handleUpdateRelationShips", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_UPDATE_RELATIONSHIPS);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.handleUpdateRelationShips method");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * Update relation ships.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @return true, if successful
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CommerceException the commerce exception
	 * @throws TRUPurchaseProcessException the TRU purchase process exception
	 */
	@SuppressWarnings("unchecked")
	public boolean updateRelationShips(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, CommerceException, TRUPurchaseProcessException {
		if(isLoggingDebug()){
			vlogDebug("START of TRUShippingGroupFormHandler.updateRelationShips method");
		}
		final TRUShippingGroupManager groupManager = (TRUShippingGroupManager) getShippingGroupManager();
		final TRUShippingProcessHelper shippingHelper = getShippingHelper();
		final List<Relationship> sgCiRels = groupManager.getAllShippingGroupRelationships(getOrder());
		Order lOrder = getOrder();
		if (sgCiRels != null && !sgCiRels.isEmpty()) {
			final int sgCiRelsCount = sgCiRels.size();
			for (int i = 0; i < sgCiRelsCount; ++i) {
				final TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) sgCiRels.get(i);
				final TRUCommerceItemImpl commerceItem = (TRUCommerceItemImpl)sgCiRel.getCommerceItem();
				if (groupManager.isNotInstanceOfTRUCommerceItem(commerceItem) || !commerceItem.isGiftItem()) {
					continue;
				}
				ShippingGroup oldShippingGroup= sgCiRel.getShippingGroup();
				if(oldShippingGroup instanceof TRUHardgoodShippingGroup){
					TRUHardgoodShippingGroup oldShippingGroup1 =(TRUHardgoodShippingGroup)oldShippingGroup;
					TRUShipMethodVO methodVO = getShippingManager().isEligibleForExistingShipMethod(
							commerceItem,
							oldShippingGroup1,
							lOrder);
					if (methodVO != null) {
						final TRUHardgoodShippingGroup newShippingGroup = (TRUHardgoodShippingGroup) groupManager.createShippingGroup(
								oldShippingGroup.getShippingGroupClassType());
						groupManager.addShippingGroupToOrder(lOrder, newShippingGroup);
						OrderTools.copyAddress(oldShippingGroup1.getShippingAddress(), newShippingGroup.getShippingAddress());
						shippingHelper.setShipMethodAndPrice(lOrder, commerceItem, newShippingGroup, oldShippingGroup.getShippingMethod());
						final long qnty = sgCiRel.getQuantity();
						getCommerceItemManager().removeItemQuantityFromShippingGroup(lOrder, commerceItem.getId(), oldShippingGroup.getId(),
								qnty);
						getCommerceItemManager().addItemQuantityToShippingGroup(lOrder, commerceItem.getId(), newShippingGroup.getId(),
								qnty);
					}
					getShippingHelper().updateEstimateDeliveryDates(getOrder());
					commerceItem.setGiftItem(Boolean.FALSE);
					return true;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUShippingGroupFormHandler.updateRelationShips method");
		}
		return false;
	}

	/** The m skip shipping restrctions. */
	private boolean mSkipShippingRestrctions;

	/** The cart modifier helper. */
	private TRUCartModifierHelper mCartModifierHelper;

	/**
	 * @return the skipShippingRestrctions
	 */
	public boolean isSkipShippingRestrctions() {
		return mSkipShippingRestrctions;
	}

	/**
	 * @param pSkipShippingRestrctions the skipShippingRestrctions to set
	 */
	public void setSkipShippingRestrctions(boolean pSkipShippingRestrctions) {
		mSkipShippingRestrctions = pSkipShippingRestrctions;
	}

	/**
	 * Gets the cart modifier helper.
	 *
	 * @return the cart modifier helper
	 */
	public TRUCartModifierHelper getCartModifierHelper() {
		return mCartModifierHelper;
	}

	/**
	 * Sets the cart modifier helper.
	 *
	 * @param pCartModifierHelper the new cart modifier helper
	 */
	public void setCartModifierHelper(TRUCartModifierHelper pCartModifierHelper) {
		mCartModifierHelper = pCartModifierHelper;
	}


	/**
	 * This method will be called when shipping.jsp page loaed and apply all the shipping rules.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @return true, if successful
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public boolean handleApplyShippingGroupRules(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean hasException = false;
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_APPLAY_SHIPPING_GROUP_RULES)) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					if(applyShippingGroupRules(pRequest,pResponse)){
						runProcessRepriceOrder(
								getOrder(), getUserPricingModels(), getLocale(), getProfile(), createRepriceParameterMap());
						getOrderManager().updateOrder(getOrder());
					}					
				}
			} catch (CommerceException e) {
				hasException = true;
				if (isLoggingError()) {
					logError(
							"CommerceException in TRUShippingGroupFormHandler.handleApplyShippingGroupRules",
							e);
				}
			} catch (RunProcessException e) {
				hasException = true;
				if (isLoggingError()) {
					logError(
							"CommerceException in TRUShippingGroupFormHandler.handleApplyShippingGroupRules",
							e);
				}
			} finally {
				if (hasException) {
					try {
						setTransactionToRollbackOnly();
					} catch (SystemException e) {
						if (isLoggingError()) {
							logError("SystemException in TRUShippingGroupFormHandler.handleApplyShippingGroupRules", e);
						}
					}
				}
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_APPLAY_SHIPPING_GROUP_RULES);
				}
			}
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * AS part of MVP280 :Removes the shipping address from the order if the removed registry's address being used by other items.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @return true, if successful
	 */
	public boolean applyShippingGroupRules(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {
		final TRUOrderImpl order =(TRUOrderImpl) getOrder();
		if (isLoggingDebug()) {
			vlogDebug("applyShippingGroupRules() for pOrder {0}", order.getId());
		}
		boolean isRepriceRequired = false;
		final TRUShippingGroupContainerService containerService = (TRUShippingGroupContainerService)getShippingGroupMapContainer();
		try {
			if(!order.isOrderMandateForMultiShipping() && order.getShipToHomeItemsCount()==_1){
				order.setOrderIsInMultiShip(Boolean.FALSE);
			}
			List<TRUHardgoodShippingGroup> allhgsGroups = order.getAllHardgoodShippingGroups();
			String nickName =null;
			List<TRUHardgoodShippingGroup> removableShippingAddress = new ArrayList<TRUHardgoodShippingGroup>();
			for(TRUHardgoodShippingGroup shipGrp:allhgsGroups) {
				nickName = shipGrp.getNickName();
				if(StringUtils.isNotBlank(nickName) &&
						containerService.getShippingGroupMap()!=null &&
						!containerService.getShippingGroupMap().keySet().contains(nickName)){
					removableShippingAddress.add(shipGrp);
				}
			}
			if(!removableShippingAddress.isEmpty()){
				for(TRUHardgoodShippingGroup shipGrp : removableShippingAddress){
					nickName = shipGrp.getNickName();
					getShippingHelper().removeShippingAddress(getOrder(),nickName,getShippingGroupMapContainer());
					isRepriceRequired = true ;
				}
			}
		} catch (CommerceException e) {
			if (isLoggingError()) {
				vlogError("RepositoryException while applyShippingGroupRules for the pOrder {0}", order.getId());
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError("RepositoryException while applyShippingGroupRules for the pOrder {0}", order.getId());
			}
		}
		return isRepriceRequired;

	}

	/**
	 * This method is used to add calculate tax parameter.
	 * 
	 * @return Map
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected Map createRepriceParameterMap() {
		if (isLoggingDebug()) {
			vlogDebug("TRUCommitOrderFormHandler (createRepriceParameterMapToSkipTaxCal) : BEGIN");
		}
		Map parameters;
		parameters = super.createRepriceParameterMap();
		if (parameters == null) {
			parameters = new HashMap();
		}
		parameters.put(TRUTaxwareConstant.CALC_TAX, Boolean.TRUE);
		if (isLoggingDebug()) {
			vlogDebug("TRUCommitOrderFormHandler (createRepriceParameterMapToSkipTaxCal) : END");
		}
		return parameters;
	}

	/**
	 * Adding empty form error to highlight the div box if the preferred ship method is not eligible for any item.
	 *
	 * @param pShipGrpIds the ship grp ids
	 */
	private void addFormErrors(List<String> pShipGrpIds){
		if(pShipGrpIds != null && !pShipGrpIds.isEmpty()){

			for(String shipGrpId : pShipGrpIds){
				addFormException(new DropletException(TRUConstants.EMPTY,shipGrpId ));
			}
		}
	}
	/**
	 * Performs input data validations for new shipping address specified by
	 * shopper.
	 *
	 * @param pRequest a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse a <code>DynamoHttpServletResponse</code> value.
	 * @throws ServletException if an error occurs.
	 * @throws IOException if an error occurs.
	 */
	protected void preCalculateOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		if (isLoggingDebug()) {
			vlogDebug("Start: TRUShippingGroupFormHandler.preShipToNewAddress()");
		}

		if (StringUtils.isBlank(getAddress().getCity()) || StringUtils.isBlank(getAddress().getPostalCode()) ||
				StringUtils.isBlank(getAddress().getState()) || StringUtils.isBlank(getAddress().getCountry()) ||
				StringUtils.isEmpty(getShippingMethod())) {
			setEmptyShippingAddress(Boolean.TRUE);
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDRESS_MISSING_INFO, null);
			getErrorHandler().processException(mVe, this);
			return;
		}

		if (StringUtils.isEmpty(getShippingMethod())) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_SHIPPING_NO_SHIP_METHOD_SELECTED, null);
			getErrorHandler().processException(mVe, this);
			return;
		}
		if (isLoggingDebug()) {
			vlogDebug("End: TRUShippingGroupFormHandler.preCalculateOrder()");
		}
	}
	/**
	 * Handle 'Calculate Order' case.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * 
	 * @return redirection result.
	 * 
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	public boolean handleCalculateOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.handleShipToNewAddress method");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = TRU_SHIPPING_GROUP_FORM_HANDLER_APPLEPAY_CALCULATE_ORDER;
		if (rrm == null || rrm.isUniqueRequestEntry(myHandleMethod))
		{
			Transaction tr = null;
			setApplePayBuyNow(isApplePayBuyNow());
			TRUOrderImpl order = null;
			TRUOrderHolder shoppingCart = (TRUOrderHolder)getShoppingCart();

			if (isApplePayBuyNow()) {
				order = shoppingCart.getBuyNowWithAP();
				setOrder(order);
			} else {
				//	shoppingCart.setBuyNowWithAP(null);
				order = (TRUOrderImpl)getOrder();
				setOrder(order);
			}
			if (order == null || order.getCommerceItems().isEmpty()) {
				addFormException(new DropletException(TRUCommerceConstants.CART_EMPTY));
				if (getFormError()) {
					if (isLoggingDebug()) {
						vlogDebug("Redirecting due to form error in field validation() method");
					}
					return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
				}
			}
			try {
				tr = ensureTransaction();

				synchronized (getOrder()) {
					preCalculateOrder(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in field validation() method");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					applePayShipToNewAddress(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in field validation() method");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					runProcessRepriceOrder(
							getOrder(), getUserPricingModels(), getLocale(), getProfile(), createRepriceParameterMap());
				}
			} catch (RunProcessException e) {
				addFormException(new DropletException(PLEASE_TRY_AGAIN));
				if (isLoggingError()) {
					logError("RunProcessException in TRUShippingGroupFormHandler.handleCalculateOrder", e);
				}
			}  catch (CommerceException e) {
				addFormException(new DropletException(PLEASE_TRY_AGAIN));
				if (isLoggingError()) {
					logError("CommerceException in TRUShippingGroupFormHandler.handleCalculateOrder", e);
				}
			}finally {
				if (tr != null){
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_APPLEPAY_CALCULATE_ORDER);
				}
			}
			if (getFormError()) {
				if (isLoggingDebug()) {
					vlogDebug("Redirecting due to form error in field validation() method");
				}
				return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
			}
			postCalculateOrder(pRequest, pResponse);

		} else {
			return false;
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}
	/**
	 * Setup single shipping details for shipping to a new address.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 * @throws CommerceException CommerceException
	 */

	protected void applePayShipToNewAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		if (isLoggingDebug()) {
			logDebug("Start of TRUShippingGroupFormHandler.applePayShipToNewAddress method");
		}
		if (!checkFormRedirect(null, getShipToNewAddressErrorURL(), pRequest,pResponse)) {
			return;
		}
		final TRUShippingGroupManager shippingGroupManager = (TRUShippingGroupManager) getShippingGroupManager();
		// order first shipping group
		final TRUHardgoodShippingGroup hardgoodShippingGroup = getShippingHelper().getPurchaseProcessHelper().getFirstHardgoodShippingGroup(getOrder());
		if (hardgoodShippingGroup != null) {
			// updating address
			if(getTRUConfiguration().isEnableApplePayAddresses()){
				getAddress().setAddress1(getTRUConfiguration().getApplePayAddress1());
				getAddress().setFirstName(getTRUConfiguration().getApplePayFirstName());
				getAddress().setLastName(getTRUConfiguration().getApplePayLastName());
			}
			OrderTools.copyAddress(getAddress(), hardgoodShippingGroup.getShippingAddress());
			// updating shipping price
			hardgoodShippingGroup.setShippingPrice(getShippingPrice());
			hardgoodShippingGroup.setShippingMethod(getShippingMethod());
			hardgoodShippingGroup.setOrderShippingAddress(true);
		}
		shippingGroupManager.removeEmptyShippingGroups(getOrder());
		if (isLoggingDebug()) {
			vlogDebug("End of TRUShippingGroupFormHandler.applePayShipToNewAddress method");
		}
	}

	/**
	 * This method populates the orderSummary Details VO
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 * @throws CommerceException
	 * 					if an error occurs.
	 */
	protected void postCalculateOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUShippingGroupFormHandler.postCalculateOrder method");
		}
		ApplePayOrderSummaryVO applePayOrderSummary = getShippingHelper().populateApplePayOrderSummary(getOrder());
		setApplePayOrderSummary(applePayOrderSummary);
		List<PromoStatusVO> promoList = getShippingHelper().fetchApplePayPromotions(getOrder());
		setPromoStatusVO(promoList);
		if (isLoggingDebug()) {
			vlogDebug("End of TRUShippingGroupFormHandler.postCalculateOrder method");
		}
	}

	/**
	 * @return the mCartItemDetailObj
	 */
	public TRUCartItemDetailVO getCartItemDetailObj() {
		return mCartItemDetailObj;
	}

	/**
	 * Sets the cart item detail obj.
	 *
	 * @param pCartItemDetailObj the new cart item detail obj
	 */
	public void setCartItemDetailObj(TRUCartItemDetailVO pCartItemDetailObj) {
		mCartItemDetailObj = pCartItemDetailObj;
	}
	
	
}