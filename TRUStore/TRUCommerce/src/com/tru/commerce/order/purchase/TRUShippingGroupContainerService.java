package com.tru.commerce.order.purchase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.commerce.order.ShippingGroup;
import atg.commerce.order.purchase.ShippingGroupContainerService;
import atg.core.util.ContactInfo;

import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.common.TRUConstants;

/**
 * The Class TRUShippingGroupContainerService.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUShippingGroupContainerService extends
		ShippingGroupContainerService {

	/** Initially set the mShippingAddress. */
	private ContactInfo mShippingAddress;

	/** property: nonDisplayableShippingGroups. */
	private List<String> mNonDisplayableShippingGroups;

	/** property: shippingGroupMapForDisplay. */
	private Map mShippingGroupMapForDisplay;

	/** Holds order single shipping group nick name. */
	private String mSelectedAddressNickName;

	/**
	 * Gets the non displayable shipping groups.
	 * 
	 * @return the nonDisplayableShippingGroups
	 */
	public List<String> getNonDisplayableShippingGroups() {
		return mNonDisplayableShippingGroups;
	}

	/**
	 * Sets the non displayable shipping groups.
	 * 
	 * @param pNonDisplayableShippingGroups
	 *            the nonDisplayableShippingGroups to set
	 */
	public void setNonDisplayableShippingGroups(
			List<String> pNonDisplayableShippingGroups) {
		mNonDisplayableShippingGroups = pNonDisplayableShippingGroups;
	}

	/**
	 * @return A shipping group map with non-displayable addresses removed.
	 */
	public Map getShippingGroupMapForDisplay() {
		initalizeShippingGroupMapForDisplay();
		return mShippingGroupMapForDisplay;
	}

	/**
	 * @param pShippingGroupMapForDisplay
	 *            - set a new shippingGroupMapForDisplay
	 */
	public void setShippingGroupMapForDisplay(Map pShippingGroupMapForDisplay) {
		mShippingGroupMapForDisplay = pShippingGroupMapForDisplay;
	}

	/**
	 * Initalizes the shippingGroupMapForDisplay property from contents of the
	 * shippingGroupMap.
	 */
	protected void initalizeShippingGroupMapForDisplay() {

		if (mShippingGroupMapForDisplay != null) {
			mShippingGroupMapForDisplay.clear();
		}

		final Map sgMap = getShippingGroupMap();
		final List<String> nonDisplayableShippingGroups = getNonDisplayableShippingGroups();

		mShippingGroupMapForDisplay = new HashMap(sgMap);

		if (nonDisplayableShippingGroups != null
				&& !nonDisplayableShippingGroups.isEmpty()) {

			// Remove the entries we don't wish to display from the return map.
			for (String nonDisplayableSg : nonDisplayableShippingGroups) {

				if (mShippingGroupMapForDisplay.containsKey(nonDisplayableSg)) {
					mShippingGroupMapForDisplay.remove(nonDisplayableSg);
				}
			}
		}

	}

	/**
	 * Gets the selected address nick name.
	 * 
	 * @return the selectedAddressNickName
	 */
	public String getSelectedAddressNickName() {
		return mSelectedAddressNickName;
	}

	/**
	 * Sets the selected address nick name.
	 * 
	 * @param pSelectedAddressNickName
	 *            the selectedAddressNickName to set
	 */
	public void setSelectedAddressNickName(String pSelectedAddressNickName) {
		mSelectedAddressNickName = pSelectedAddressNickName;
	}

	/**
	 * Gets the shipping address.
	 * 
	 * @return the shippingAddress
	 */
	public ContactInfo getShippingAddress() {
		return mShippingAddress;
	}

	/**
	 * Sets the shipping address.
	 * 
	 * @param pShippingAddress
	 *            the shippingAddress to set
	 */
	public void setShippingAddress(ContactInfo pShippingAddress) {
		mShippingAddress = pShippingAddress;
	}

	
	
	/**
	 * Gets the hard good ship group count from the container.
	 *
	 * @return the hard good ship group count
	 */
	@SuppressWarnings("unchecked")
	public int getHardGoodShipGroupCount(){
		if(isLoggingDebug()){
			logDebug("Start : getHardGoodShipGroupCount()");
		}
		int count=TRUConstants._0;
		Map<String, ShippingGroup> shippingGroupMap = getShippingGroupMap();
		if(shippingGroupMap!=null && !shippingGroupMap.isEmpty()){
			if(isLoggingDebug()){
				vlogDebug("getHardGoodShipGroupCount() and Total Ship groups Count {0}",shippingGroupMap.size());
			}
			Set<String> keySet= shippingGroupMap.keySet();
			TRUHardgoodShippingGroup shippingGroup=null;
			for(String key :keySet){
				if( shippingGroupMap.get(key) instanceof TRUHardgoodShippingGroup){
					shippingGroup=(TRUHardgoodShippingGroup)shippingGroupMap.get(key);
					if(!shippingGroup.isBillingAddress() && !shippingGroup.isRegistryAddress()){
						count+=TRUConstants.ONE;
					}
				}
			}
			
		}
		if(isLoggingDebug()){
			vlogDebug("End getHardGoodShipGroupCount() and count is {0}",count);
		}
		return count;
	}
}
