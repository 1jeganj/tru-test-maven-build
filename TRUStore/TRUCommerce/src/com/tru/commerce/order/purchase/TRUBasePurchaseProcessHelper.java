package com.tru.commerce.order.purchase;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import atg.adapter.gsa.ChangeAwareSet;
import atg.commerce.CommerceException;
import atg.commerce.claimable.ClaimableManager;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemManager;
import atg.commerce.order.DuplicateShippingGroupException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.purchase.AddCommerceItemInfo;
import atg.commerce.order.purchase.PurchaseProcessHelper;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.promotion.TRUPromotionTools;
import atg.commerce.util.PipelineErrorHandler;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.naming.ComponentName;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.pipeline.PipelineResult;
import atg.service.pipeline.RunProcessException;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.cart.vo.TRUChannelDetailsVo;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUChannelInStorePickupShippingGroup;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUShippingGroupManager;
import com.tru.commerce.order.TRUShippingManager;
import com.tru.commerce.pricing.TRUShipMethodVO;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * The Class TRUPurchaseProcessHelper.
 * @author PA
 * @version 1.0
 */
public class TRUBasePurchaseProcessHelper extends PurchaseProcessHelper {

	/** The Constant TRU_SESSION_SCOPE_COMPONENT_NAME. */
	public static final ComponentName TRU_SESSION_SCOPE_COMPONENT_NAME = ComponentName
			.getComponentName(TRUCommerceConstants.TRU_SESSION_SCOPE_COMPONENT_NAME);

	/** Propery to hold the CardinalToken number *. */
	private String mCardinalTokenNumber;

	/** The claimable item. */
	RepositoryItem mClaimableItem = null;

	/** property to hold mClaimableManager. */
	private ClaimableManager mClaimableManager;

	/** Property To property manager. */
	private TRUCommercePropertyManager mCommercePropertyManager;

	/** Property to hold the TRUConfiguration. */	
	private TRUConfiguration mConfiguration;

	/** property to Hold TRUGiftingPurchaseProcessHelper. */
	private TRUGiftingPurchaseProcessHelper mGiftingProcessHelper;

	/** property: orderTools. */
	private TRUOrderTools mOrderTools;

	/** Property to hold ProfileTools. */
	private TRUProfileTools mProfileTools;

	/**
	 * Property to hold PromotionTools.
	 */
	private TRUPromotionTools mPromotionTools;

	/** Property to hold commerce property manager. */
	private TRUPropertyManager mPropertyManager;

	/** Holds shippingManager instance. */
	private TRUShippingManager mShippingManager;

	
	/** property to Hold shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;
	
	/** property to hold mUserLocale. */
	private Locale mUserLocale;

	/** property to hold mUserPricingModels. */
	private PricingModelHolder mUserPricingModels;

	
	
	/**
	 * Adds the item to order.
	 * 
	 * @param pItemInfo
	 *            the item info
	 * @param pCatalogKey
	 *            the catalog key
	 * @param pOrder
	 *            the order
	 * @param pShippingGroup
	 *            ShippingGroup
	 * @param pProfile
	 *            profile
	 * @return the commerce item
	 * @throws CommerceException
	 *             the commerce exception
	 */
	@Override
	@SuppressWarnings("unchecked")
	public CommerceItem addItemToOrder(AddCommerceItemInfo pItemInfo, String pCatalogKey, Order pOrder,
			ShippingGroup pShippingGroup, RepositoryItem pProfile) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::addItemToOrder() : BEGIN ");
		}
		ShippingGroup shippingGroup = pShippingGroup;
		CommerceItem ci = null;
		TRUAddCommerceItemInfo addCommerceItemInfo = null;
		if (pItemInfo instanceof TRUAddCommerceItemInfo) {
			addCommerceItemInfo = (TRUAddCommerceItemInfo) pItemInfo;
			List<ShippingGroup> sgs = pOrder.getShippingGroups();
			ShippingGroup matchingSG = null;
			if (StringUtils.isBlank(addCommerceItemInfo.getLocationId()) && StringUtils.isNotBlank(addCommerceItemInfo.getChannelId())) {
				for (ShippingGroup sg : sgs) {
					if (sg instanceof TRUChannelHardgoodShippingGroup) {
						// return empty shipping group having nick name as null - check to have lower48 region while add an
						// item to cart.
						TRUChannelHardgoodShippingGroup channelHardgoodShippingGroup = (TRUChannelHardgoodShippingGroup) sg;
						if (addCommerceItemInfo.getChannelId().equalsIgnoreCase(
								((TRUChannelHardgoodShippingGroup) sg).getChannelId()) && 
								channelHardgoodShippingGroup != null && 
								StringUtils.isBlank(channelHardgoodShippingGroup.getNickName())) {
							matchingSG = channelHardgoodShippingGroup;
							if (addCommerceItemInfo.getChannelUserName() != null
									&& !addCommerceItemInfo.getChannelUserName().equalsIgnoreCase(
											((TRUChannelHardgoodShippingGroup) sg).getChannelUserName())) {
								((TRUChannelHardgoodShippingGroup) matchingSG).setChannelUserName(addCommerceItemInfo
										.getChannelUserName());
							}
							if (addCommerceItemInfo.getChannelCoUserName() != null
									&& !addCommerceItemInfo.getChannelCoUserName().equalsIgnoreCase(
											((TRUChannelHardgoodShippingGroup) sg).getChannelCoUserName())) {
								((TRUChannelHardgoodShippingGroup) matchingSG).setChannelCoUserName(addCommerceItemInfo
										.getChannelCoUserName());
							}
							break;
						}
					}
				}
				if (null == matchingSG) {
					TRUChannelDetailsVo channelDetailsVo = createChannelDetailVO(addCommerceItemInfo);

					matchingSG = ((TRUShippingGroupManager) getShippingGroupManager())
							.addChannelHardgoodShippingGroupToOrder(pOrder, channelDetailsVo);
				}
			} else {
				matchingSG = getMatchedISPUShippingGroup(pOrder, addCommerceItemInfo);
			}
			if (null != matchingSG) {
				shippingGroup = matchingSG;
				pItemInfo.setShippingGroupType(matchingSG.getShippingGroupClassType());
			}
		}
		ci = super.addItemToOrder(pItemInfo, pCatalogKey, pOrder, shippingGroup, pProfile);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::addItemToOrder() : END ");
		}
		return ci;
	}

	/**
	 * updates least shipping method for item having shipping group of type hardgoodShippingGroup or channelHardgoodShippingGroup if not set.
	 * 
	 * @param pItem
	 *            the CommerceItem
	 * @param pItemInfo
	 *            the add commerce item info
	 * @param pOrder
	 *            the order
	 * @param pShippingGroup
	 *            the ShippingGroup
	 * @throws CommerceException
	 *             commerce exception
	 */
	@Override
	protected void addItemToShippingGroup(CommerceItem pItem, AddCommerceItemInfo pItemInfo, Order pOrder,
			ShippingGroup pShippingGroup) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::addItemToShippingGroup() : BEGIN ");
		}
		TRUOrderImpl order = (TRUOrderImpl) pOrder;
		TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
		// multi ship check
		if ((order.isOrderIsInMultiShip() || pShippingGroup instanceof TRUChannelHardgoodShippingGroup) && (pShippingGroup instanceof TRUHardgoodShippingGroup)) {
				TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) pShippingGroup;
				if (hardgoodShippingGroup != null) {
					updateShippingGroup(pItem, pOrder, orderManager, pShippingGroup,
							hardgoodShippingGroup);
				}
		}
		super.addItemToShippingGroup(pItem, pItemInfo, pOrder, pShippingGroup);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::addItemToShippingGroup() : END ");
		}
	}

	/**
	 * Creates the channel detail vo.
	 * 
	 * @param pAddCommerceItemInfo
	 *            the add commerce item info
	 * @return the tRU channel details vo
	 */
	private TRUChannelDetailsVo createChannelDetailVO(TRUAddCommerceItemInfo pAddCommerceItemInfo) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createChannelDetailVO() : BEGIN ");
		}
		TRUChannelDetailsVo channelDetailsVo = new TRUChannelDetailsVo();
		channelDetailsVo.setChannelId(pAddCommerceItemInfo.getChannelId());
		channelDetailsVo.setChannelType(pAddCommerceItemInfo.getChannelType());
		channelDetailsVo.setChannelName(pAddCommerceItemInfo.getChannelName());
		channelDetailsVo.setChannelCoUserName(pAddCommerceItemInfo.getChannelCoUserName());
		channelDetailsVo.setChannelUserName(pAddCommerceItemInfo.getChannelUserName());
		channelDetailsVo.setSourceChannel(pAddCommerceItemInfo.getSourceChannel());
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createChannelDetailVO() : END ");
		}
		return channelDetailsVo;
	}
	
	/**
	 * Creates the channel detail vo based on given channelHardgoodShippingGroup.
	 * 
	 * @param pChannelHardgoodShippingGroup
	 *            the channel hardgood shipping group
	 * @return the TRU channel details vo
	 */
	protected TRUChannelDetailsVo createChannelDetailVO(TRUChannelHardgoodShippingGroup pChannelHardgoodShippingGroup) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createChannelDetailVO() : BEGIN ");
		}
		TRUChannelDetailsVo channelDetailsVo = new TRUChannelDetailsVo();
		channelDetailsVo.setChannelId(pChannelHardgoodShippingGroup.getChannelId());
		channelDetailsVo.setChannelType(pChannelHardgoodShippingGroup.getChannelType());
		channelDetailsVo.setChannelName(pChannelHardgoodShippingGroup.getChannelName());
		channelDetailsVo.setChannelCoUserName(pChannelHardgoodShippingGroup.getChannelCoUserName());
		channelDetailsVo.setChannelUserName(pChannelHardgoodShippingGroup.getChannelUserName());
		channelDetailsVo.setSourceChannel(pChannelHardgoodShippingGroup.getSourceChannel());
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createChannelDetailVO() : END ");
		}
		return channelDetailsVo;
	}

	/**
	 * createCommerceItem.
	 * 
	 * @param pItemInfo
	 *            the item info
	 * @param pCatalogKey
	 *            the catalog key
	 * @param pOrder
	 *            the order
	 * @return the commerce item
	 * @throws CommerceException
	 *             the commerce exception
	 */
	@Override
	protected CommerceItem createCommerceItem(AddCommerceItemInfo pItemInfo, String pCatalogKey, Order pOrder)
			throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createCommerceItem() : BEGIN ");
		}
		CommerceItemManager cimgr = getOrderManager().getCommerceItemManager();
		String siteId = pItemInfo.getSiteId();
		if (StringUtils.isBlank(siteId)) {
			siteId = null;
		}
		CommerceItem ci = cimgr.createCommerceItem(pItemInfo.getCommerceItemType(), pItemInfo.getCatalogRefId(), null,
				pItemInfo.getProductId(), null, pItemInfo.getQuantity(), pCatalogKey, null, siteId, null);
		TRUAddCommerceItemInfo addCommerceItemInfo = null;
		if (pItemInfo instanceof TRUAddCommerceItemInfo) {
			addCommerceItemInfo = (TRUAddCommerceItemInfo) pItemInfo;
			if (ci instanceof TRUGiftWrapCommerceItem) {
				return ci;
			} else if (ci instanceof TRUDonationCommerceItem && addCommerceItemInfo.getDonationAmount() != null) {
				((TRUDonationCommerceItem) ci).setDonationAmount(addCommerceItemInfo.getDonationAmount());
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("Item created with ID ", ci.getId());
		}
		CommerceItem ciFinal = cimgr.addItemToOrder(pOrder, ci);
		if (isLoggingDebug()) {
			vlogDebug("New item :: {0} :: merged into existing item :: {1}", ci.getId(), ciFinal.getId());
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createCommerceItem() : END ");
		}
		return ciFinal;
	}

	/**
	 * Creates the channel in store pickup shipping groups.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pItems
	 *            the items
	 * @throws CommerceException
	 *             the commerce exception
	 */
	public void createISPUShippingGroups(Order pOrder,
			AddCommerceItemInfo[] pItems) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createISPUShippingGroups() : BEGIN ");
		}
		if (null != pOrder && pItems != null) {
			if (isLoggingDebug()) {
				vlogDebug(
						"@Class::TRUPurchaseProcessHelper::@method::createISPUShippingGroups() : Items size : {0}",
						pItems.length);
			}
			TRUAddCommerceItemInfo truAddCommerceItemInfo = null;
			for (AddCommerceItemInfo itemInfo : pItems) {
				if (itemInfo instanceof TRUAddCommerceItemInfo) {
					truAddCommerceItemInfo = (TRUAddCommerceItemInfo) itemInfo;
					if (isLoggingDebug()) {
						vlogDebug(
								"ItemInfo quantity : {0} : ItemInfo location id : {1} :",
								truAddCommerceItemInfo.getQuantity(),
								truAddCommerceItemInfo.getLocationId());
					}
					if ((truAddCommerceItemInfo.getQuantity() > TRUCheckoutConstants._0L) && 
							(truAddCommerceItemInfo.getLocationId() != null)) {
						ShippingGroup matchingSG = getMatchedISPUShippingGroup(
								pOrder, truAddCommerceItemInfo);
						if (matchingSG == null && truAddCommerceItemInfo instanceof TRUAddCommerceItemInfo) {
							matchingSG = createMissedIspuSGsInOrder(pOrder,
									truAddCommerceItemInfo);
						}
						if (truAddCommerceItemInfo.getShippingGroupType() == null) {
							truAddCommerceItemInfo
									.setShippingGroupType(matchingSG
											.getShippingGroupClassType());
						}
						if (matchingSG instanceof TRUInStorePickupShippingGroup) {
							((TRUInStorePickupShippingGroup) matchingSG)
									.setWarhouseLocationCode(truAddCommerceItemInfo
											.getWarehouseLocationCode());
						} else if (matchingSG instanceof TRUChannelInStorePickupShippingGroup) {
							((TRUChannelInStorePickupShippingGroup) matchingSG)
									.setWarhouseLocationCode(truAddCommerceItemInfo
											.getWarehouseLocationCode());
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createISPUShippingGroups() : END ");
		}
	}

	/**
	 * Creates the missed ispu s gs in order.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pItemInfo
	 *            the item info
	 * @return the shipping group
	 * @throws CommerceException
	 *             the commerce exception
	 */
	public ShippingGroup createMissedIspuSGsInOrder(Order pOrder,
			TRUAddCommerceItemInfo pItemInfo) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createMissedIspuSGsInOrder() : BEGIN");
		}
		ShippingGroup matchingSG = null;
		if (null != pItemInfo && (pItemInfo.getQuantity() > TRUConstants.LONG_ZERO) && 
				(pItemInfo.getLocationId() != null)) {
			if (StringUtils.isBlank(pItemInfo.getChannelId())) {
				matchingSG = getShippingGroupManager().createShippingGroup(
						((TRUOrderTools) getOrderManager().getOrderTools())
								.getInStorePickupShippingGroupType());
				if (matchingSG instanceof TRUInStorePickupShippingGroup) {
					TRUInStorePickupShippingGroup ispsg = (TRUInStorePickupShippingGroup) matchingSG;
					ispsg.setLocationId(pItemInfo.getLocationId());
					ispsg.setWarhouseLocationCode(pItemInfo
							.getWarehouseLocationCode());
					getShippingGroupManager().addShippingGroupToOrder(pOrder,
							ispsg);
					if (isLoggingDebug()) {
						vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createMissedIspuSGsInOrder() : Adding TRUInStorePickupShippingGroup to order");
					}
				}
				return matchingSG;
			} else if (StringUtils.isNotBlank(pItemInfo.getChannelId())) {
				TRUChannelDetailsVo channelDetailsVo = createChannelDetailVO(pItemInfo);
				matchingSG = ((TRUShippingGroupManager) getShippingGroupManager())
						.addChannelISPUShippingGroupToOrder(pOrder,
								channelDetailsVo, pItemInfo.getLocationId());
				((TRUChannelInStorePickupShippingGroup) matchingSG)
						.setWarhouseLocationCode(pItemInfo
								.getWarehouseLocationCode());
				if (isLoggingDebug()) {
					vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createMissedIspuSGsInOrder() : Adding TRUChannelInStorePickupShippingGroup to order");
				}
				return matchingSG;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createMissedIspuSGsInOrder() : END");
		}
		return matchingSG;
	}

	/**
	 * Gets the cardinal token number.
	 * 
	 * @return the cardinalTokenNumber
	 */
	public String getCardinalTokenNumber() {
		return mCardinalTokenNumber;
	}

	

	/**
	 * Gets the claimable manager.
	 * 
	 * @return the mClaimableManager
	 */
	public ClaimableManager getClaimableManager() {
		return mClaimableManager;
	}

	/**
	 * Gets the commerce property manager.
	 * 
	 * @return the commerce property manager
	 */
	public TRUCommercePropertyManager getCommercePropertyManager() {
		return mCommercePropertyManager;
	}

	


	/**
	 * @return the configuration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * returns first hard good shipping group.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pShippingMethod
	 *            shipping method
	 * @param pShippingPrice shipping price 
	 * @param pShippingGroupType
	 *            the shipping group type
	 * @return the first hardgood shipping group
	 * @throws CommerceException
	 *             the commerce exception
	 */
	@SuppressWarnings("unchecked")
	public TRUHardgoodShippingGroup getEmptyHardgoodShippingGroupByMethod(Order pOrder, String pShippingMethod, double pShippingPrice,
			String pShippingGroupType) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUPurchaseProcessHelper.getFirstHardgoodShippingGroup method");
			vlogDebug("INPUT PARAMS OF updateOrderWithPayment() pOrder: {0}", pOrder);
		}
		if (StringUtils.isBlank(pShippingMethod)) {
			return null;
		}
		String shippingGroupType = pShippingGroupType;
		if (StringUtils.isBlank(shippingGroupType)) {
			shippingGroupType = getOrderTools().getDefaultShippingGroupType();
		}
		if ((pOrder.getShippingGroups() != null) && (pOrder.getShippingGroups().size() > 0)) {
			List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
			for (ShippingGroup shippingGroup : shippingGroups) {
				if (shippingGroup != null && shippingGroupType.equals(shippingGroup.getShippingGroupClassType())) {
					TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
					if (StringUtils.isBlank(hardgoodShippingGroup.getNickName()) && 
							pShippingMethod.equalsIgnoreCase(hardgoodShippingGroup.getShippingMethod()) && 
							pShippingPrice == hardgoodShippingGroup.getShippingPrice()) {
						return (TRUHardgoodShippingGroup) shippingGroup;
					}
				}
			}
		}
		TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupManager()
				.createShippingGroup(shippingGroupType);
		hardgoodShippingGroup.setShippingMethod(pShippingMethod);
		hardgoodShippingGroup.setShippingPrice(pShippingPrice);
		pOrder.addShippingGroup(hardgoodShippingGroup);
		if (isLoggingDebug()) {
			vlogDebug("End of getFirstHardgoodShippingGroup method and returns hardgoodShippingGroup: {0}", hardgoodShippingGroup);
		}
		return hardgoodShippingGroup;
	}

	/**
	 * returns first hard good shipping group.
	 * 
	 * @param pOrder
	 *            the order
	 * @return the first hardgood shipping group
	 * @throws CommerceException
	 *             the commerce exception
	 */
	@SuppressWarnings("unchecked")
	public TRUHardgoodShippingGroup getFirstHardgoodShippingGroup(Order pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUPurchaseProcessHelper.getFirstHardgoodShippingGroup method");
			vlogDebug("INPUT PARAMS OF updateOrderWithPayment() pOrder: {0}", pOrder);
		}
		if ((pOrder.getShippingGroups() != null) && (pOrder.getShippingGroups().size() > 0)) {
			List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
			for (ShippingGroup shippingGroup : shippingGroups) {
				if (shippingGroup instanceof TRUHardgoodShippingGroup &&
						!((TRUHardgoodShippingGroup) shippingGroup).isBillingAddress() &&
						!(shippingGroup instanceof TRUChannelHardgoodShippingGroup)) {
					return (TRUHardgoodShippingGroup) shippingGroup;
				}
			}
		}
		TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupManager()
				.createShippingGroup();
		hardgoodShippingGroup.setRegistryAddress(Boolean.FALSE);
		getShippingGroupManager().addShippingGroupToOrder(pOrder, hardgoodShippingGroup);
		if (isLoggingDebug()) {
			vlogDebug("End of getFirstHardgoodShippingGroup method and returns hardgoodShippingGroup: {0}", hardgoodShippingGroup);
		}
		return hardgoodShippingGroup;
	}

	/**
	 * Gets the gifting process helper.
	 * 
	 * @return the giftingProcessHelper
	 */
	public TRUGiftingPurchaseProcessHelper getGiftingProcessHelper() {
		return mGiftingProcessHelper;
	}

	/**
	 * Gets the matched ispu shipping group.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pTruAddCommerceItemInfo
	 *            the tru add commerce item info
	 * @return the matched ispu shipping group
	 */
	@SuppressWarnings("unchecked")
	private ShippingGroup getMatchedISPUShippingGroup(Order pOrder, TRUAddCommerceItemInfo pTruAddCommerceItemInfo) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::getMatchedISPUShippingGroup() : BEGIN");
		}
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		if (null != shippingGroups && !shippingGroups.isEmpty()) {
			for (ShippingGroup shippingGroup : shippingGroups) {
				if (StringUtils.isBlank(pTruAddCommerceItemInfo.getChannelId()) && 
						TRUCommerceConstants.INSTORE_PICKUP_SG.equalsIgnoreCase(shippingGroup.getShippingGroupClassType()) && 
						((TRUInStorePickupShippingGroup) shippingGroup).getLocationId().equals(
								pTruAddCommerceItemInfo.getLocationId())) {
					if (isLoggingDebug()) {
						vlogDebug(
								"@Class::TRUPurchaseProcessHelper::@method::getMatchedISPUShippingGroup() :TRUInStorePickupShippingGroup is already exsist",
								shippingGroup.getId());
					}
					return shippingGroup;
				}

				if (shippingGroup instanceof TRUChannelInStorePickupShippingGroup && 
						((TRUChannelInStorePickupShippingGroup) shippingGroup).getLocationId().equals(
								pTruAddCommerceItemInfo.getLocationId()) && 
								StringUtils.isNotBlank(((TRUChannelInStorePickupShippingGroup) shippingGroup).getChannelId()) && 
								((TRUChannelInStorePickupShippingGroup) shippingGroup).getChannelId().equals(
								pTruAddCommerceItemInfo.getChannelId())) {
					if (isLoggingDebug()) {
						vlogDebug(
								"@Class::TRUPurchaseProcessHelper::@method::getMatchedISPUShippingGroup() :TRUChannelInStorePickupShippingGroup is already exsist",
								shippingGroup.getId());
					}
					return shippingGroup;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::getMatchedISPUShippingGroup() : BEGIN");
		}
		return null;
	}

	/**
	 * Gets the order tools.
	 * 
	 * @return the order tools property.
	 */
	public TRUOrderTools getOrderTools() {
		return mOrderTools;
	}

	/**
	 * Gets the profile tools.
	 * 
	 * @return the mProfileTools
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * This property contains PromotionTools object.
	 * 
	 * @return PromotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return this.mPromotionTools;
	}

	/**
	 * Gets the property manager.
	 * 
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Gets the shipping manager.
	 * 
	 * @return the shippingManager
	 */
	public TRUShippingManager getShippingManager() {
		return mShippingManager;
	}

	/**
	 * Gets the shopping cart utils.
	 * 
	 * @return the shoppingCartUtils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * Gets the user locale.
	 * 
	 * @return the mLocale
	 */
	public Locale getUserLocale() {
		return mUserLocale;
	}

	/**
	 * Gets the user pricing models.
	 * 
	 * @return the mUserPricingModels
	 */
	public PricingModelHolder getUserPricingModels() {
		return mUserPricingModels;
	}

	/**
	 * Method to remove the coupons.
	 * 
	 * @param pProfile profile
	 * @param pMutProfile MutProfile
	 * @param pPromotionTools promotionTools
	 * @param pProfileRepId profileRepId
	 * @param pPromotions promotions
	 * @param pCurrentCoupon - Coupon Repository Item
	 */
	public void processRemoveCoupon(RepositoryItem pProfile,
			MutableRepositoryItem pMutProfile, TRUPromotionTools pPromotionTools,
			String pProfileRepId, ChangeAwareSet pPromotions, RepositoryItem pCurrentCoupon) {
		if (pPromotions != null && !pPromotions.isEmpty()) {
			if (isLoggingDebug()) {
				vlogDebug("Before removing promotions size :{0}, Profile : {1} ", pPromotions.size(), pProfileRepId);
			}
			for (Object promotion : pPromotions) {
				if (pMutProfile != null) {
					pPromotionTools.removeCoupon(pMutProfile, (RepositoryItem) promotion, false,pCurrentCoupon);
				} else {
					pPromotionTools.removeCoupon((MutableRepositoryItem) pProfile, (RepositoryItem) promotion, false,pCurrentCoupon);
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("After removing promotions size :{0} ", pPromotions.size());
			}
		}
	}

	/**
	 * Removes the coupon item.
	 * 
	 * @param pCurrentCouponCode
	 *            the current coupon code
	 * @param pProfile
	 *            the profile
	 * @param pUserLocale
	 *            the user locale
	 * @throws CommerceException
	 *             the commerce exception
	 */
	public void removeCouponItem(String pCurrentCouponCode, RepositoryItem pProfile, Locale pUserLocale) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::removeCouponItem() : BEGIN ");
		}
		if (pProfile != null) {
			MutableRepositoryItem profile = null;
			TRUPromotionTools promotionTools = getPromotionTools();
			MutableRepository profileMutableRepItem = (MutableRepository) pProfile.getRepository();
			String profileRepId = pProfile.getRepositoryId();
			ClaimableManager claimableManager = getClaimableManager();
			TransactionDemarcation td = new TransactionDemarcation();
			boolean shouldRollback = true;
			try {
				td.begin(getTransactionManager());

				RepositoryItem currentCoupon = null;
				// Get and remove all coupon's promotions from order
				if (!StringUtils.isEmpty(pCurrentCouponCode)) {
					currentCoupon = claimableManager.claimItem(pCurrentCouponCode);
				}
				ChangeAwareSet promotions = null;
				// get all promotions
				if (currentCoupon != null) {
					promotions = (ChangeAwareSet) currentCoupon.getPropertyValue(claimableManager.getClaimableTools()
							.getPromotionsPropertyName());
				}

				// Ensure profile to be a MutableRepositoryItem
				if (pProfile != null && !(pProfile instanceof MutableRepositoryItem)) {
					profile = profileMutableRepItem.getItemForUpdate(profileRepId, pProfile.getItemDescriptor()
							.getItemDescriptorName());
				}
				processRemoveCoupon(pProfile, profile, promotionTools,
						profileRepId, promotions,currentCoupon);
				//((TRUClaimableTools)getClaimableManager().getClaimableTools()).updateCouponUses(currentCoupon);
				shouldRollback = false;
			} catch (TransactionDemarcationException e) {
				if (isLoggingError()) {
					logError(
							"TransactionDemarcationException while removing coupon @Class::TRUPurchaseProcessHelper::@method::removeCouponItem() ",
							e);
				}
				throw new CommerceException(e);
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError(
							"RepositoryException while removing coupon @Class::TRUPurchaseProcessHelper::@method::removeCouponItem() ",
							e);
				}
				throw new CommerceException(e);
			} finally {
				// Commit or rollback transaction
				try {
					td.end(shouldRollback);
				} catch (TransactionDemarcationException e) {
					if (isLoggingError()) {
						logError(
								"TransactionDemarcationException while removing coupon in finally block @Class::TRUPurchaseProcessHelper::@method::removeCouponItem() ",
								e);
					}
					throw new CommerceException(e);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::removeCouponItem() : END ");
		}
	}

	


	/**
	 * Run process reprice.
	 * @param pPricingOperation - String
	 * @param pOrder - Order
	 * @param pPricingModels - PricingModelHolder
	 * @param pLocale - Locale
	 * @param pProfile - RepositoryItem
	 * @param pExtraParameters - Map
	 * @param pErrorHandler - PipelineErrorHandler
	 * @throws RunProcessException - RunProcessException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void runProcessRepriceOrder(String pPricingOperation, Order pOrder,
			PricingModelHolder pPricingModels, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters,
			PipelineErrorHandler pErrorHandler) throws RunProcessException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::runProcessRepriceOrder() : BEGIN ");
		}
		// This flag is used in Shipping calculator to update the shipping
		// method price
		// This flag will be set in case of add item to order or delete item
		// from order
		if (pExtraParameters == null) {
			Map extraParameters = new HashMap();
			extraParameters.put(TRUCommerceConstants.UPDATE_SHIP_METHOD_PRICE,
					Boolean.TRUE);
			extraParameters.put(TRUTaxwareConstant.CALCULATE_TAX, Boolean.TRUE);
			super.runProcessRepriceOrder(pPricingOperation, pOrder,
					pPricingModels, pLocale, pProfile, extraParameters,
					pErrorHandler);
		} else {
			pExtraParameters.put(TRUCommerceConstants.UPDATE_SHIP_METHOD_PRICE,
					Boolean.TRUE);
			pExtraParameters
					.put(TRUTaxwareConstant.CALCULATE_TAX, Boolean.TRUE);
			super.runProcessRepriceOrder(pPricingOperation, pOrder,
					pPricingModels, pLocale, pProfile, pExtraParameters,
					pErrorHandler);
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::runProcessRepriceOrder() : END ");
		}
	}

	/**
	 * Run process tru validate for checkout chain.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pPricingModels
	 *            the pricing models
	 * @param pLocale
	 *            the locale
	 * @param pProfile
	 *            the profile
	 * @param pExtraParameters
	 *            the extra parameters
	 * @throws RunProcessException
	 *             the run process exception
	 * @param pParameters
	 *            - parameters
	 * @param pErrorHandler
	 *            - errorHandler
	 */
	@SuppressWarnings({ "rawtypes" })
	protected void runProcessTRUValidateForCheckoutChain(Order pOrder,
			PricingModelHolder pPricingModels, Locale pLocale,
			RepositoryItem pProfile, Map pParameters, Map pExtraParameters,
			PipelineErrorHandler pErrorHandler) throws RunProcessException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::runProcessTRUValidateForCheckoutChain() : BEGIN");
		}
		PipelineResult result = runProcess(
				TRUCheckoutConstants.TRU_VALIDATE_FOR_CHECKOUT_CHAIN, pOrder,
				pPricingModels, pLocale, pProfile, pParameters,
				pExtraParameters);
		processPipelineErrors(result, pErrorHandler);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::runProcessTRUValidateForCheckoutChain() : END");
		}
	}

	/**
	 * Method to create an Extra Parameters for Coupon Code Update in Order.
	 * 
	 * @param pChainId
	 *            chain id
	 * @param pPricingOperation
	 *            the pricing operation
	 * @param pOrder
	 *            the order
	 * @param pPricingModels
	 *            the pricing models
	 * @param pLocale
	 *            the locale
	 * @param pProfile
	 *            the profile
	 * @param pExtraParameters
	 *            the extra parameters
	 * @return PipelineResult
	 * @throws RunProcessException
	 *             the run process exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override	
	protected PipelineResult runRepricingProcess(String pChainId,
			String pPricingOperation, Order pOrder,
			PricingModelHolder pPricingModels, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters)
			throws RunProcessException {
		if (isLoggingDebug()) {
			vlogDebug("Enter intos [Class: TRUPurchaseProcessHelper  method: runRepricingProcess]");
		}
		String chainId = pChainId;
		Map extraParameters = pExtraParameters;
		if (extraParameters == null) {
			extraParameters = new HashMap();
		}
		extraParameters.put(TRUConstants.COUPON_CODE_UPDATE, Boolean.TRUE);
		if (isLoggingDebug()) {
			vlogDebug("Exit from [Class: TRUPurchaseProcessHelper  method: runRepricingProcess]");
		}
		if(StringUtils.isBlank(chainId)) {
			chainId = TRUConstants.REPRICE_ORDER; 
		}
		return super.runRepricingProcess(chainId, pPricingOperation, pOrder,
				pPricingModels, pLocale, pProfile, extraParameters);
	}

	

	/**
	 * Sets the cardinal token number.
	 * 
	 * @param pCardinalTokenNumber
	 *            the cardinalTokenNumber to set
	 */
	public void setCardinalTokenNumber(String pCardinalTokenNumber) {
		mCardinalTokenNumber = pCardinalTokenNumber;
	}
	
	/**
	 * Sets the claimable manager.
	 * 
	 * @param pClaimableManager
	 *            the new claimable manager
	 */
	public void setClaimableManager(ClaimableManager pClaimableManager) {
		this.mClaimableManager = pClaimableManager;
	}

	/**
	 * Sets the commerce property manager.
	 * 
	 * @param pCommercePropertyManager
	 *            the new commerce property manager
	 */
	public void setCommercePropertyManager(TRUCommercePropertyManager pCommercePropertyManager) {
		mCommercePropertyManager = pCommercePropertyManager;
	}

	
	/**
	 * @param pConfiguration the configuration to set
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		mConfiguration = pConfiguration;
	}

	
	/**
	 * Sets the gifting process helper.
	 * 
	 * @param pGiftingProcessHelper
	 *            the giftingProcessHelper to set
	 */
	public void setGiftingProcessHelper(TRUGiftingPurchaseProcessHelper pGiftingProcessHelper) {
		mGiftingProcessHelper = pGiftingProcessHelper;
	}
	
	
	/**
	 * Sets the order tools.
	 * 
	 * @param pOrderTools
	 *            the new order tools
	 */
	public void setOrderTools(TRUOrderTools pOrderTools) {
		mOrderTools = pOrderTools;
	}

	
	/**
	 * Sets the profile tools.
	 * 
	 * @param pProfileTools
	 *            the mProfileTools to set
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		this.mProfileTools = pProfileTools;
	}

	
	/**
	 * This method will set PromotionTools with pPromotionTools.
	 * 
	 * @param pPromotionTools
	 *            -Promotion Tools.
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		this.mPromotionTools = pPromotionTools;
	}
	
	
	/**
	 * Sets the property manager.
	 * 
	 * @param pPropertyManager
	 *            the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}
	
	/**
	 * Sets the shipping manager.
	 * 
	 * @param pShippingManager
	 *            the shippingManager to set
	 */
	public void setShippingManager(TRUShippingManager pShippingManager) {
		mShippingManager = pShippingManager;
	}


	/**
	 * Sets the shopping cart utils.
	 * 
	 * @param pShoppingCartUtils
	 *            the shoppingCartUtils to set
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		mShoppingCartUtils = pShoppingCartUtils;
	}

	
	/**
	 * Sets the user locale.
	 * 
	 * @param pUserLocale
	 *            the new user locale
	 */
	public void setUserLocale(Locale pUserLocale) {
		this.mUserLocale = pUserLocale;
	}
	
	
	/**
	 * Sets the user pricing models.
	 * 
	 * @param pUserPricingModels
	 *            the new user pricing models
	 */
	public void setUserPricingModels(PricingModelHolder pUserPricingModels) {
		this.mUserPricingModels = pUserPricingModels;
	}
	
	/**
	 * This method updates the shipping group.
	 * @param pItem
	 *            the CommerceItem
	 * @param pOrderManager
	 *            TRUOrderManager
	 * @param pOrder
	 *            the order
	 * @param pShippingGroup
	 *            the ShippingGroup
	 * @param pHardgoodShippingGroup
	 * 			TRUHardgoodShippingGroup
	 * @throws CommerceException
	 *             commerce exception
	 * @throws DuplicateShippingGroupException
	 *             DuplicateShippingGroupException exception	            
	 * @throws InvalidParameterException
	 *             InvalidParameterException exception
	 */
	private void updateShippingGroup(CommerceItem pItem, Order pOrder,
			TRUOrderManager pOrderManager, ShippingGroup pShippingGroup,
			TRUHardgoodShippingGroup pHardgoodShippingGroup)
			throws CommerceException, DuplicateShippingGroupException,
			InvalidParameterException {
		String nickName = pHardgoodShippingGroup.getNickName();

		// getting least shipping method
		Address address = pHardgoodShippingGroup.getShippingAddress();
		List<TRUShipMethodVO> shipMethodVOs = getShippingManager().getEligibleShipMethodsByItem(pItem, address,
				pOrder);
		TRUShipMethodVO leastShipMethodVO = getShippingManager().getDefaultShipMethodByItem(shipMethodVOs);
		ShippingGroup lShippingGroup = pShippingGroup;
		String leastShipMethod = lShippingGroup.getShippingGroupClassType();
		double leastShippingPrice = TRUCommerceConstants.DOUBLE_ZERO;
		if (leastShipMethodVO != null) {
			leastShipMethod = leastShipMethodVO.getShipMethodCode();
			leastShippingPrice = leastShipMethodVO.getShippingPrice();
		}

		// find shipping group by nickname and shipping method in order
		if (StringUtils.isNotBlank(nickName) && StringUtils.isNotBlank(leastShipMethod)) {
			TRUHardgoodShippingGroup matchedShippingGroup = pOrderManager.getShipGroupByNickNameAndShipMethod(pOrder,
					nickName, leastShipMethod);
			if (matchedShippingGroup != null) {
				lShippingGroup = matchedShippingGroup;
			}
		} else if (StringUtils.isBlank(nickName) && StringUtils.isNotBlank(leastShipMethod)) {
			// if empty nickname find shipping group by least shipping method in order
			TRUHardgoodShippingGroup matchedShippingGroup = getEmptyHardgoodShippingGroupByMethod(pOrder,
					leastShipMethod, leastShippingPrice, lShippingGroup.getShippingGroupClassType());
			if (matchedShippingGroup != null) {
				lShippingGroup = matchedShippingGroup;
			}
		} else {
			// create shipping group and update the properties
			TRUHardgoodShippingGroup newShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupManager()
					.createShippingGroup(lShippingGroup.getShippingGroupClassType());
			pOrder.addShippingGroup(newShippingGroup);

			if (StringUtils.isNotBlank(nickName)) {
				newShippingGroup.setNickName(nickName);
				newShippingGroup.setShippingAddress(address);
			}
			if (StringUtils.isNotBlank(leastShipMethod)) {
				newShippingGroup.setShippingMethod(leastShipMethod);
				newShippingGroup.setShippingPrice(leastShippingPrice);
			}
			lShippingGroup = newShippingGroup;
		}
	}

	
	/**
	 * validates the address with address doctor.
	 *
	 * @param pAddress            address object in which the user entered address is populated
	 * @param pOrder order
	 * @param pProfile the profile
	 * @param pPageName the page name
	 * @param pTaskName the task name
	 * @return the status address doctor returned
	 */
	public String validateAddress(ContactInfo pAddress, Order pOrder, RepositoryItem pProfile, String pPageName, String pTaskName) {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUPurchaseProcessHelper.validateAddress method");
			vlogDebug("INPUT PARAMS OF validateAddress method pAddress: {0} ", pAddress);
		}
		String addressStatus = null;
		if (pAddress != null) {
			Map<String, String> addressValue = new HashMap<String, String>();
			addressValue.put(TRUConstants.ADDRESS_1, pAddress.getAddress1());
			addressValue.put(TRUConstants.ADDRESS_2, pAddress.getAddress2());
			addressValue.put(TRUConstants.CITY, pAddress.getCity());
			addressValue.put(TRUConstants.STATE, pAddress.getState());
			addressValue.put(TRUConstants.POSTAL_CODE_FOR_ADD, pAddress.getPostalCode());
			addressValue.put(TRUConstants.ORDER_ID, pOrder.getId());
			addressValue.put(TRUConstants.USER_ID, pProfile.getRepositoryId());
			addressValue.put(TRUConstants.PAGE, pPageName);
			addressValue.put(TRUConstants.TASK_NAME, pTaskName);
			addressStatus = getProfileTools().validateAddress(addressValue);
		} 
		if (addressStatus == null) {
			addressStatus = TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR;
		}
		if (isLoggingDebug()) {
			vlogDebug("End of validateAddress method and returns addressStatus: {0} ", addressStatus);
		}
		return addressStatus;
	}
	
	/**
	 *  This method used to trim the string values in a map.
	 * 
	 * @param pMap -The map to hold the address details entered by the user.
	 */
	public void trimMapValues(Map<String, String> pMap) {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUBasePurchaseProcessHelper.trimMapValues method");
		}
		String trimmedValue = null;
		for (Entry<String, String> entry : pMap.entrySet()) {
			if(StringUtils.isNotBlank(entry.getValue())) {
				trimmedValue = entry.getValue().trim();
				pMap.remove(entry.getValue());
				pMap.put(entry.getKey(), trimmedValue);
			}
		}
		if(isLoggingDebug()) {
			vlogDebug("End of TRUBasePurchaseProcessHelper.trimMapValues method");
		}
	}
}
