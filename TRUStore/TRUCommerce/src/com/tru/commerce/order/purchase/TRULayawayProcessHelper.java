package com.tru.commerce.order.purchase;

import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;

import atg.commerce.CommerceException;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * @author PA
 * @version 1.0
 *
 */
public class TRULayawayProcessHelper extends GenericService {
	
	/**property to hold mTRUConfiguration. */
 	private TRUConfiguration mTRUConfiguration;
 	
 	/** hold mPurchaseProcessHelper. */
	private TRUPurchaseProcessHelper mPurchaseProcessHelper;
 	
 	/**
	 * @return the mPurchaseProcessHelper
	 */
	public TRUPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}

	/**
	 * @param pPurchaseProcessHelper the mPurchaseProcessHelper to set
	 */
	public void setPurchaseProcessHelper(TRUPurchaseProcessHelper pPurchaseProcessHelper) {
		this.mPurchaseProcessHelper = pPurchaseProcessHelper;
	}
	
	/**
	 * @return the mTRUConfiguration
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * @param pTRUConfiguration the mTRUConfiguration to set
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		this.mTRUConfiguration = pTRUConfiguration;
	}
	
	
	/**
	 * check if all the fields are empty in a credit card.
	 * @param pCreditCardMap creditcardMap 
	 * @return boolean true/false
	 * 
	 */
	public boolean isAllCreditCardFieldEmpty(Map<String, String> pCreditCardMap) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRULayawayProcessHelper::@method::isAllCreditCardFieldEmpty() : START");
		}
		TRUPropertyManager cpmgr = ((TRUOrderManager) getPurchaseProcessHelper().getOrderManager()).getPropertyManager();
		String ccNum = null;
		String ccExpMonth = null;
		String ccExpYear = null;
		String ccNameOnCard = null;
		String ccCVV = null;
		if (pCreditCardMap != null) {
			ccNum = (String) pCreditCardMap.get(cpmgr.getCreditCardNumberPropertyName());
			ccExpMonth = (String) pCreditCardMap.get(cpmgr.getCreditCardExpirationMonthPropertyName());
			ccExpYear = (String) pCreditCardMap.get(cpmgr.getCreditCardExpirationYearPropertyName());
			ccNameOnCard = (String) pCreditCardMap.get(cpmgr.getNameOnCardPropertyName());
			ccCVV = (String) pCreditCardMap.get(cpmgr.getCreditCardVerificationNumberPropertyName());
		}
		if(ccNum == null && ccExpMonth == null && ccExpYear == null && ccNameOnCard == null && ccCVV == null){
			return true;
		}
		return false;
	}
	
	
	/**
	 * populate billing address in layaway order object.
	 * @param pLayawayOrder layawayorder
	 * @param pContactInfo contactinfo
	 * @param pEmail email 
	 *
	 * @throws ServletException the servlet exception
	 */
	public void populateBillingAddress(TRULayawayOrderImpl pLayawayOrder, ContactInfo pContactInfo, String pEmail) throws ServletException{
		if (isLoggingDebug()) {
			logDebug("START TRULayawayPaymentInfoFormHandler:: populateBillingAddress method");
		}
		pLayawayOrder.setFirstName(pContactInfo.getFirstName());
		pLayawayOrder.setLastName(pContactInfo.getLastName());
		pLayawayOrder.setAddress1(pContactInfo.getAddress1());
		pLayawayOrder.setAddress2(pContactInfo.getAddress2());
		pLayawayOrder.setCity(pContactInfo.getCity());
		pLayawayOrder.setAddrState(pContactInfo.getState());
		pLayawayOrder.setPostalCode(pContactInfo.getPostalCode());
		pLayawayOrder.setCountry(pContactInfo.getCountry());
		pLayawayOrder.setPhoneNumber(pContactInfo.getPhoneNumber());
		pLayawayOrder.setEmail(pEmail.trim());
		
		if (isLoggingDebug()) {
			logDebug("END TRULayawayPaymentInfoFormHandler:: populateBillingAddress method");
		}

	}

	/**
     * To check if the pin length is valid or not.
     * @param pValue
     *        card value
     * @return boolean
     *        boolean value
     */
    public boolean isValidGCPinLength(String pValue) {
        boolean l_bIsValid = true;
        if (pValue.length() < getTRUConfiguration().getGiftCardPinLength()) {
            l_bIsValid = false;
        }
        return l_bIsValid;
    }
    
    /**
	 * This method used to trim the string values in a map.
	 * 
	 * @param pMap
	 *            The map to hold the address details entered by the user
	 */
	public void trimMapValues(Map<String, String> pMap) {
		if (isLoggingDebug()) {
			vlogDebug("START of TRULayawayProcessHelper.trimMapValues method");
		}
		String trimmedValue = null;
		for (Entry<String, String> entry : pMap.entrySet()) {
			if(StringUtils.isNotBlank(entry.getValue())){
				trimmedValue = entry.getValue().trim();
				pMap.remove(entry.getValue());
				pMap.put(entry.getKey(), trimmedValue);
			}
		}
		if(isLoggingDebug()) {
			vlogDebug("End of TRULayawayProcessHelper.trimMapValues method");
		}
	}
	
	
    /**
     * To check if the gift card number lenght is valid or not.
     * @param pValue
     *        card value
     * @return boolean
     *        boolean value
     */
    public boolean isValidGCNumberLength(String pValue) {
        boolean l_bIsValid = true;
        if (pValue.length() <  getTRUConfiguration().getGiftCardNumberLength()) {
            l_bIsValid = false;
        }
        return l_bIsValid;
    }
	
	/**
	 * Updates the order credit card with the input map values and billing addess ContactInfo object.
	 * @param pLayawayOrder 
	 * 
	 * @param pCreditCard
	 *            the credit card
	 * @param pCreditCardInputMap
	 *            the credit card input map
	 * @param pBillingAddress
	 *            the billing address
	 * @throws CommerceException
	 *             the commerce exception
	 */
	public void updateCreditCard(TRULayawayOrderImpl pLayawayOrder, TRUCreditCard pCreditCard, Map<String, String> pCreditCardInputMap, ContactInfo pBillingAddress)
			throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRULayawayProcessHelper.updateCreditCard method");
		}
		TRUOrderManager orderManager = (TRUOrderManager) getPurchaseProcessHelper().getOrderManager();

		if (pCreditCard == null) {
			if(isLoggingError()) {
				logError("Exception in @Class::TRULayawayProcessHelper::@method::updateCreditCard(): Credit Card is NULL");
			}
		} else {
			if (isLoggingDebug()) {
				vlogDebug("Updating the creditCard details for order");
			}
			TRUPropertyManager pm = getPurchaseProcessHelper().getPropertyManager();
			if (StringUtils.isBlank(pCreditCardInputMap.get(pm.getCreditCardTokenPropertyName()))) {
					pCreditCard.setCreditCardNumber((String) pCreditCardInputMap.get(pm.getCreditCardNumberPropertyName()));
			} else {
				pCreditCard.setCreditCardNumber(pCreditCardInputMap.get(pm.getCreditCardTokenPropertyName()));
			}
			pCreditCard.setCreditCardType(pCreditCardInputMap.get(pm.getCreditCardTypePropertyName()));
			pCreditCard.setExpirationMonth(pCreditCardInputMap.get(pm.getCreditCardExpirationMonthPropertyName()));
			pCreditCard.setExpirationYear(pCreditCardInputMap.get(pm.getCreditCardExpirationYearPropertyName()));
			pCreditCard.setCardVerificationNumber(pCreditCardInputMap.get(pm.getCreditCardVerificationNumberPropertyName()));
			pCreditCard.setNameOnCard(pCreditCardInputMap.get(pm.getNameOnCardPropertyName()));
			if(pLayawayOrder instanceof TRULayawayOrderImpl){
				if(!pCreditCard.getCreditCardNumber().equals(((TRULayawayOrderImpl)pLayawayOrder).getPreviousCreditCardNumber())){
					((TRULayawayOrderImpl)pLayawayOrder).setRetryCount(TRUConstants.INT_ZERO);
					((TRULayawayOrderImpl)pLayawayOrder).setPreviousResponseCode(null);
				}
			}
			// Updating the credit card addresses.
			((TRUOrderTools) orderManager.getOrderTools()).updateCreditCardAddress(pBillingAddress, pCreditCard);
			
			vlogDebug("TRULayawayProcessHelper.updateCreditCard :: "
					+ "credit card from the order updated with the input map and billing address provided.");
		}

		if (isLoggingDebug()) {
			logDebug("End of updateCreditCard method");
		}
	}

	/**
	 * Checks if is any credit card field empty.
	 * 
	 * @param pCreditCardMap
	 *            - creditCardMap
	 * @return boolean true/false
	 */
	public boolean isAnyCreditCardFieldEmpty(Map<String, String> pCreditCardMap) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRULayawayProcessHelper::@method::isAnyCreditCardFieldEmpty() : START");
		}
		TRUPropertyManager cpmgr = ((TRUOrderManager) getPurchaseProcessHelper().getOrderManager()).getPropertyManager();
		String ccNum = null;
		String ccExpMonth = null;
		String ccExpYear = null;
		String ccCVV = null;
		if (pCreditCardMap != null) {
			ccNum = (String) pCreditCardMap.get(cpmgr.getCreditCardNumberPropertyName());
			ccExpMonth = (String) pCreditCardMap.get(cpmgr.getCreditCardExpirationMonthPropertyName());
			ccExpYear = (String) pCreditCardMap.get(cpmgr.getCreditCardExpirationYearPropertyName());
			ccCVV = (String) pCreditCardMap.get(cpmgr.getCreditCardVerificationNumberPropertyName());
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRULayawayProcessHelper::@method::isAnyCreditCardFieldEmpty() : END");
		}
		return StringUtils.isBlank(ccNum) || StringUtils.isBlank(ccCVV)|| StringUtils.isBlank(ccExpMonth)|| StringUtils.isBlank(ccExpYear);
	}
	
}
