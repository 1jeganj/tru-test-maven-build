package com.tru.commerce.order.purchase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.transaction.SystemException;
import javax.transaction.Transaction;

import atg.commerce.CommerceException;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupNotFoundException;
import atg.commerce.order.purchase.PurchaseProcessFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.vo.TRUChannelDetailsVo;
import com.tru.commerce.cart.vo.TRURegistrantDetailsVO;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUChannelInStorePickupShippingGroup;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.vo.TRUStorePickUpInfo;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.TRUUserSession;
import com.tru.common.cml.ValidationExceptions;
import com.tru.errorhandler.fhl.IErrorHandler;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.regex.EmailPasswordValidator;
import com.tru.validator.fhl.IValidator;

/**
 * @author PA
 * @version 1.0.
 */
public class TRUInStorePickUpFormHandler extends PurchaseProcessFormHandler{
	
	/**
	 * property to hold mCommonSuccessURL.
	 */
	private String mCommonSuccessURL;
	/**
	 * property to hold mCommonErrorURL.
	 */
	private String mCommonErrorURL;
	
	/** property to hold the mValidator.
	 * */
	private IValidator mValidator;
	
	/** property to hold the mErrorHandler.
	  */
	private IErrorHandler mErrorHandler;
	
	/** property to hold ValidationException.
	 *  */
	private ValidationExceptions mVe = new ValidationExceptions();
	
	/** property: Reference to the ShippingProcessHelper component.
	 **/
	private TRUShippingProcessHelper mShippingHelper;
	
	/** property to hold EmailPasswordValidator. */
	private EmailPasswordValidator mEmailPasswordValidator;
	/**
	 * property to hold mRestService.
	 */
	private boolean mRestService;
	
	/** holds mStorePickUpCollection string.  **/
	private String mStorePickUpCollection;
	
	/** holds Array of store pick up info vo .**/
	private TRUStorePickUpInfo[] mStorePickUpInfos;
	
	/** holds shipping group count. **/
	private int mShippingGroupsCount;
	
	/**
	 * @return the emailPasswordValidator.
	 */
	public EmailPasswordValidator getEmailPasswordValidator() {
		return mEmailPasswordValidator;
	}

	/**
	 * @param pEmailPasswordValidator the emailPasswordValidator to set.
	 */
	public void setEmailPasswordValidator(
			EmailPasswordValidator pEmailPasswordValidator) {
		mEmailPasswordValidator = pEmailPasswordValidator;
	}

	/**
	 * @return the shippingHelper.
	 */
	public TRUShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * @param pShippingHelper  the shippingHelper to set
	 */
	public void setShippingHelper(TRUShippingProcessHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}
		
	/**
	 * @return the restService.
	 */
	public boolean isRestService() {
		return mRestService;
	}

	/**
	 * @param pRestService the restService to set.
	 */
	public void setRestService(boolean pRestService) {
		mRestService = pRestService;
	}
		
	/**
	 * @return the storePickUpCollection
	 */
	public String getStorePickUpCollection() {
		return mStorePickUpCollection;
	}

	/**
	 * @param pStorePickUpCollection the storePickUpCollection to set
	 */
	public void setStorePickUpCollection(String pStorePickUpCollection) {
		mStorePickUpCollection = pStorePickUpCollection;
	}
	
	/**
	 * @return the storePickUpInfos.
	 */
	public TRUStorePickUpInfo[] getStorePickUpInfos() {
		return mStorePickUpInfos;
	}

	/**
	 * @param pStorePickUpInfos the storePickUpInfos to set.
	 */
	public void setStorePickUpInfos(TRUStorePickUpInfo[] pStorePickUpInfos) {
		mStorePickUpInfos = pStorePickUpInfos;
	}
	
	/**
	 * @return the shippingGroupsCount.
	 */
	public int getShippingGroupsCount() {
		return mShippingGroupsCount;
	}
	
	/**
	 * property to hold user session component.
	 */
	private TRUUserSession mUserSession;
	
	/**
	 * @return the userSession
	 */
	public TRUUserSession getUserSession() {
		return mUserSession;
	}

	/**
	 * @param pUserSession the userSession to set
	 */
	public void setUserSession(TRUUserSession pUserSession) {
		mUserSession = pUserSession;
	}

	/**
	 * @param pShippingGroupsCount the shippingGroupsCount to set.
	 */
	public void setShippingGroupsCount(int pShippingGroupsCount) {
		mShippingGroupsCount = pShippingGroupsCount;
		if (mShippingGroupsCount <= 0) {
			this.mShippingGroupsCount = 0;
			this.mStorePickUpInfos = null;
		} else {
			this.mShippingGroupsCount = pShippingGroupsCount;
			this.mStorePickUpInfos = new TRUStorePickUpInfo[this.mShippingGroupsCount];
			try {
				for (int index = 0; index < mShippingGroupsCount; ++index) {
					this.mStorePickUpInfos[index] = ((TRUStorePickUpInfo) Class
							.forName(TRUCommerceConstants.STORE_PICKUP_INFO_CLASS_NAME).newInstance());
				}
			} catch (InstantiationException insExe) {
				this.mStorePickUpInfos = null;
				if (isLoggingError()) {
				vlogError("InstantiationException occurred in setShippingGroupsCount @Class::TRUInStorePickUpFormHandler::" + 
						"@method::setShippingGroupsCount()", insExe);
				}
			} catch (IllegalAccessException illExe) {
				this.mStorePickUpInfos = null;
				if (isLoggingError()) {
				vlogError("IllegalAccessException occurred in setShippingGroupsCount @Class::TRUInStorePickUpFormHandler::@method::" + 
						"setShippingGroupsCount()", illExe);
				}
			} catch (ClassNotFoundException clsExe) {
				this.mStorePickUpInfos = null;
				if (isLoggingError()) {
				vlogError("ClassNotFoundException occurred in setShippingGroupsCount @Class::TRUInStorePickUpFormHandler::@method::" + 
						"setShippingGroupsCount()", clsExe);
				}
			}
		}
	}

	/**
	 * Finds for the empty first name in-store pickup shipping groups.
	 * updates the first name and last names to in-store pickup shipping groups.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 * @throws CommerceException CommerceException
	 */
	@SuppressWarnings("unchecked")
	public boolean handleInitRegistryShippingGroups(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		String myHandleMethod = "TRUInStorePickUpFormHandler.handleInitRegistryShippingGroups";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			boolean hasException = false;
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				TRUOrderImpl order = (TRUOrderImpl) getOrder();
				synchronized (order) {
					/*boolean hasRegistryInStoreShippingGroups1 = false;
					List<ShippingGroup> shippingGroups = order.getShippingGroups();
					for (ShippingGroup shippingGroup : shippingGroups) {
						if (shippingGroup instanceof TRUChannelInStorePickupShippingGroup) {
							hasRegistryInStoreShippingGroups = true;
						}
					}
					// no registry items for pick-up, hence returning
					if (!hasRegistryInStoreShippingGroups) {
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(),pRequest, pResponse);
					}*/
					preInitRegistryShippingGroups(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in handleInitRegistryShippingGroups() method");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(),pRequest, pResponse);
					}
					initRegistryShippingGroups(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in handleInitRegistryShippingGroups() method");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(),pRequest, pResponse);
					}
					postInitRegistryShippingGroups(pRequest, pResponse);
					runProcessRepriceOrder(order, getUserPricingModels(), getUserLocale(),	getProfile(), createRepriceParameterMap());
					getOrderManager().updateOrder(order);
				}
			} catch (CommerceException e) {
				hasException = true;
				if (isLoggingError()) {
					logError(
							"CommerceException in TRUInStorePickUpFormHandler.handleInitRegistryShippingGroups",
							e);
				}
			} catch (RunProcessException e) {
				hasException = true;
				if (isLoggingError()) {
					logError(
							"RunProcessException in TRUInStorePickUpFormHandler.handleInitRegistryShippingGroups",
							e);
				}
			} finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in TRUInStorePickUpFormHandler.handleInitRegistryShippingGroups", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(),pRequest, pResponse);
	}
	
	/**
	 * Pre initRegistryShippingGroups.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * 
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	protected void preInitRegistryShippingGroups(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if(isLoggingDebug()){
			vlogDebug("START of TRUInStorePickUpFormHandler.preInitRegistryShippingGroups method");
		}
		
		if(isLoggingDebug()){
			vlogDebug("END of TRUInStorePickUpFormHandler.preInitRegistryShippingGroups method");
		}
		
	}
	
	/**
	 * Setup single shipping details for shipping to a existing address.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 * @throws CommerceException 
	 */
	@SuppressWarnings("unchecked")
	protected void initRegistryShippingGroups(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUInStorePickUpFormHandler.initRegistryShippingGroups method");
		}
		
		TRUOrderImpl order = (TRUOrderImpl) getOrder();
		List<TRUChannelDetailsVo> channelDetailsVos = new ArrayList<TRUChannelDetailsVo>();
		// get unique channel detail vo having channel id and channel type
		List<ShippingGroup> shippingGroups = order.getShippingGroups();
		for (ShippingGroup shippingGroup : shippingGroups) {
			if (shippingGroup instanceof TRUChannelInStorePickupShippingGroup) {
				TRUChannelInStorePickupShippingGroup orderShippingGroup = (TRUChannelInStorePickupShippingGroup) shippingGroup;
				String channelId = orderShippingGroup.getChannelId();
				String channelType = orderShippingGroup.getChannelType();
				
				boolean isChannelFound = false;
				for (TRUChannelDetailsVo channelDetailsVo : channelDetailsVos) {
					if (StringUtils.isNotBlank(channelId) &&
							StringUtils.isNotBlank(channelType)	&&
							channelId.equals(channelDetailsVo.getChannelId()) && 
							channelType.equals(channelDetailsVo.getChannelType())) {
						isChannelFound = true;
					}
				}
				if (!isChannelFound) {
					TRUChannelDetailsVo channelDetailsVo = new TRUChannelDetailsVo();
					channelDetailsVo.setChannelId(channelId);
					channelDetailsVo.setChannelType(channelType);
					channelDetailsVo.setChannelName(orderShippingGroup.getChannelName());
					channelDetailsVo.setChannelUserName(orderShippingGroup.getChannelUserName());
					channelDetailsVo.setChannelCoUserName(orderShippingGroup.getChannelCoUserName());
					channelDetailsVo.setSourceChannel(orderShippingGroup.getSourceChannel());
					channelDetailsVos.add(channelDetailsVo);
				}
			}
		}
		//Start GEOFUS-1576 :: Commented as part of the defect. 
		/*// get addresses from populateStorePickupInfo and update to container shipping groups
		for (TRUChannelDetailsVo channelDetailsVo : channelDetailsVos) {
			// update registrant details thorugh populateStorePickupInfo
			boolean isRegistrantDetailsFound = getShippingHelper().updateRegistrantDetails(channelDetailsVo);
			if (isRegistrantDetailsFound) {
				List<TRURegistrantDetailsVO> registrantDetailsVOs = channelDetailsVo.getRegistrantDetailsVOs();
				if (registrantDetailsVOs != null && !registrantDetailsVOs.isEmpty() && registrantDetailsVOs.get(0) != null) {
						ContactInfo contactInfo = registrantDetailsVOs.get(0).getAddress();
						// find shipping groups with channel details vo.
						List<TRUChannelInStorePickupShippingGroup> orderShippingGroups = getShippingHelper().
								findChannelInStorePickUpShippingGroups(order, channelDetailsVo);
						for (TRUChannelInStorePickupShippingGroup sg : orderShippingGroups) {
							if(StringUtils.isBlank(sg.getFirstName())) {
								sg.setFirstName(contactInfo.getFirstName());
								sg.setLastName(contactInfo.getLastName());
								sg.setPhoneNumber(contactInfo.getPhoneNumber());
								sg.setEmail(contactInfo.getEmail());
							}
						}
				}
			}
		}*/
		//Start GEOFUS-1576 :: Commented as part of the defect. 
		
		if (isLoggingDebug()) {
			vlogDebug("End of TRUInStorePickUpFormHandler.initRegistryShippingGroups method");
		}				
		
	}
	
	/**
	 * Post setting as default shipping address processing.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * 
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	protected void postInitRegistryShippingGroups(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if(isLoggingDebug()){
			vlogDebug("START of TRUInStorePickUpFormHandler.postInitRegistryShippingGroups method");
		}
		
		if(isLoggingDebug()){
			vlogDebug("END of TRUInStorePickUpFormHandler.postInitRegistryShippingGroups method");
		}
	}
	
	/**
	 * @param pRequest DynamoHttpServletRequest.
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleMoveToPaymentInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException,IOException {
		if(isLoggingDebug()){
			vlogDebug("START of TRUInStorePickUpFormHandler.handleMoveToPaymentInfo method");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "TRUInStorePickUpFormHandler.handleMoveToPaymentInfo";

		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			resetFormExceptions();
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					preMoveToPaymentInfo(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in field validation() method");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					moveToPaymentInfo(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					postMoveToPaymentInfo(pRequest, pResponse);
					getUserSession().setSignInHidden(Boolean.TRUE);
					((TRUOrderImpl) getOrder()).getActiveTabs().put(TRUCheckoutConstants.PICKUP_TAB, Boolean.TRUE);
					getShippingGroupManager().removeEmptyShippingGroups(getOrder());
					getOrderManager().updateOrder(getOrder());
				}
			} catch (CommerceException exp) {
				if(isLoggingError()){
					vlogError("Exception occurred while moving to payment info @Class::TRUInStorePickUpFormHandler::" + 
							"@method::handleMoveToPaymentInfo()", exp);
				}
			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		if(isLoggingDebug()){
			vlogDebug("END of TRUInStorePickUpFormHandler.handleMoveToPaymentInfo method");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(),pRequest, pResponse);
	}
	
	/**
	 * @param pRequest DynamoHttpServletRequest.
	 * @param pResponse DynamoHttpServletResponse
	 * @return
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	protected void preMoveToPaymentInfo(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUInStorePickUpFormHandler.preMoveToPaymentInfo method");
		}
		if(isRestService()){
			boolean validInStorePickupInfoParams=validateInStorePickupInfoDetails();
			if(!validInStorePickupInfoParams){
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_MISS_INFO_INSTORE, null);
				getErrorHandler().processException(mVe, this);
			}
			
		}
		TRUStorePickUpInfo[] storePickUpInfos = getStorePickUpInfos();
		if (storePickUpInfos != null) {
			boolean addFormError = false;
			boolean emailValidationError = false;
			for (TRUStorePickUpInfo storePickUpInfo : storePickUpInfos) {
				if (storePickUpInfo != null &&
						StringUtils.isNotBlank(storePickUpInfo.getShippingGroupId())) {
					if (!addFormError) {
						addFormError = StringUtils.isBlank(storePickUpInfo.getFirstName()) ||
								StringUtils.isBlank(storePickUpInfo.getLastName()) ||
								StringUtils.isBlank(storePickUpInfo.getPhoneNumber()) ||
								StringUtils.isBlank(storePickUpInfo.getEmail());
					}
					if (!emailValidationError) {
						emailValidationError = StringUtils.isNotBlank(storePickUpInfo.getEmail()) &&
								!getEmailPasswordValidator().validateEmail(storePickUpInfo.getEmail());
					}
					if (!addFormError && storePickUpInfo.isAlternateInfoRequired()) {
						addFormError = StringUtils.isBlank(storePickUpInfo.getAlternateFirstName())	||
								StringUtils.isBlank(storePickUpInfo.getAlternateLastName())	||
								StringUtils.isBlank(storePickUpInfo.getAlternatePhoneNumber()) ||
								StringUtils.isBlank(storePickUpInfo.getAlternateEmail());
					}
					if (!emailValidationError && storePickUpInfo.isAlternateInfoRequired()) {
						emailValidationError = StringUtils.isNotBlank(storePickUpInfo.getAlternateEmail()) && 
								!getEmailPasswordValidator().validateEmail(storePickUpInfo.getAlternateEmail());
					}
				}
			}
			
			if (addFormError) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_MISS_INFO_INSTORE, null);
				getErrorHandler().processException(mVe, this);
			}else if (emailValidationError) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_INVALID_EMAIL, null);
				getErrorHandler().processException(mVe, this);
			}
			
		}
		
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUInStorePickUpFormHandler.preMoveToPaymentInfo method");
		}
	}

	/**
	 * This method updates the InStore pick up items data with user inputs.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 * @throws CommerceException CommerceException
	 */
	protected void moveToPaymentInfo(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUInStorePickUpFormHandler.moveToPaymentInfo method");
		}
		TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
		
		TRUStorePickUpInfo[] storePickUpInfos = getStorePickUpInfos();
		if (storePickUpInfos != null) {
			for (TRUStorePickUpInfo storePickUpInfo : storePickUpInfos) {
				if (storePickUpInfo != null &&
						StringUtils.isNotBlank(storePickUpInfo.getShippingGroupId())) {
					try {
						ShippingGroup shippingGroup = getOrder().getShippingGroup(storePickUpInfo.getShippingGroupId());
						if (shippingGroup instanceof TRUInStorePickupShippingGroup) {
							TRUInStorePickupShippingGroup inStorePickupShippingGroup = (TRUInStorePickupShippingGroup) shippingGroup;
							if (inStorePickupShippingGroup != null) {
								orderManager.updateInStoreData(getOrder(), inStorePickupShippingGroup, storePickUpInfo);
							}
						}
					} catch (ShippingGroupNotFoundException e) {
						if (isLoggingError()) {
							logError("ShippingGroupNotFoundException in TRUInStorePickUpFormHandler.setShippingGroupIdForDisplay", e);
						}
					}
				}
			}
		}
		
		if (isLoggingDebug()) {
			vlogDebug("End of TRUInStorePickUpFormHandler.moveToPaymentInfo method");
		}
	}
	
	/**
	 * @param pRequest DynamoHttpServletRequest.
	 * @param pResponse DynamoHttpServletResponse
	 * @return
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	protected void postMoveToPaymentInfo(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUInStorePickUpFormHandler.postMoveToPaymentInfo method");
		}
		clearPickUpDetails();
		((TRUOrderImpl) getOrder()).getActiveTabs().put(TRUCheckoutConstants.PAYMENT_TAB, Boolean.TRUE);
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUInStorePickUpFormHandler.postMoveToPaymentInfo method");
		}
		
	}
	
	/**
	 * @return the commonSuccessURL.
	 */
	public String getCommonSuccessURL() {
		return mCommonSuccessURL;
	}

	/**
	 * @param pCommonSuccessURL   the commonSuccessURL to set.
	 */
	public void setCommonSuccessURL(String pCommonSuccessURL) {
		this.mCommonSuccessURL = pCommonSuccessURL;
	}

	/**
	 * @return the commonErrorURL
	 */
	public String getCommonErrorURL() {
		return mCommonErrorURL;
	}

	/**
	 * @param pCommonErrorURL   the commonErrorURL to set.
	 */
	public void setCommonErrorURL(String pCommonErrorURL) {
		this.mCommonErrorURL = pCommonErrorURL;
	}
	/**
	 * @return the IValidator.
	 */
	public IValidator getValidator() {
		return mValidator;
	}

	/**
	 * @param pValidator the IValidator to set.
	 */
	public void setValidator(IValidator pValidator) {
		this.mValidator = pValidator;
	}

	/**
	 * @return the IErrorHandler.
	 */
	public IErrorHandler getErrorHandler() {
		return mErrorHandler;
	}

	/**
	 * @param pErrorHandler  the IErrorHandler to set.
	 */
	public void setErrorHandler(IErrorHandler pErrorHandler) {
		this.mErrorHandler = pErrorHandler;
	}
	/**
	 *This method validates StorePickupInfo input .
	 * @return true/false 
	 */
	private boolean validateInStorePickupInfoDetails() {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUInStorePickUpFormHandler.validateInstoreDetails method");
		}
		if (StringUtils.isNotBlank(getStorePickUpCollection())) {
			TRUStorePickUpInfo[] inStorePickupInfo=populateStorePickupInfo(getStorePickUpCollection().trim());
			if(inStorePickupInfo==null){
				if (isLoggingDebug()) {
					vlogDebug("InstorePickup info validation failed ");
				}
				return false;
			}else{
				if (isLoggingDebug()) {
					vlogDebug("InstorePickup info validation is success  ");
				}
				setStorePickUpInfos(inStorePickupInfo);
			}
		}
		else{
			if (isLoggingDebug()) {
				vlogDebug("InstorePick up info validation failed ");
			}
			return false;
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUInStorePickUpFormHandler.validateInstoreDetails method");
		}
		return true;
		
	}
	/**
	 * @param pStorePickUpInfos StorePickUpInfos.
	 * @return TRUStorePickUpInfo[]
	 */
	private TRUStorePickUpInfo[] populateStorePickupInfo(String pStorePickUpInfos){
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUInStorePickUpFormHandler.populateStorePickupInfo method");
		}
		List<TRUStorePickUpInfo> inSorePickUpInfoList= new ArrayList<TRUStorePickUpInfo>();
		TRUStorePickUpInfo[] inStorePickupInfo =null;
		if(pStorePickUpInfos.contains(TRUConstants.SPLIT_SYMBOL_COMA)){
			if (isLoggingDebug()) {
				vlogDebug("Multiple  instore items to process");
			}
			String[] inStorePickUpInfos = pStorePickUpInfos.split(TRUConstants.SPLIT_SYMBOL_COMA);
			if (inStorePickUpInfos != null && inStorePickUpInfos.length > TRUConstants.INT_ZERO) {
				for (String infoItem : inStorePickUpInfos) {
					if (StringUtils.isBlank(infoItem)) {
						return null;
					}
						String[] inStorePickupProperties = infoItem.split(TRUConstants.SPLIT_PIPE);
						if (inStorePickupProperties != null && inStorePickupProperties.length >= TRUConstants.THREE) {
							TRUStorePickUpInfo inStorePickupVO =populateinStorePickupVO(inStorePickupProperties);
							if(inStorePickupVO==null){
								return null;
							}
								inSorePickUpInfoList.add(inStorePickupVO);
						}
				}
			}
		}
		else{
			if (isLoggingDebug()) {
				vlogDebug("Only one instore item to process");
			}
			String[] inStorePickupProperties = pStorePickUpInfos.split(TRUConstants.SPLIT_PIPE);
			if (inStorePickupProperties != null && inStorePickupProperties.length >= TRUConstants.THREE) {
				TRUStorePickUpInfo inStorePickupVO =populateinStorePickupVO(inStorePickupProperties);
				if(inStorePickupVO==null){
					return null;
				}
					inSorePickUpInfoList.add(inStorePickupVO);
			}
		}
		if(inSorePickUpInfoList.isEmpty()){
			return null;	
		}
		 inStorePickupInfo= inSorePickUpInfoList.toArray(new TRUStorePickUpInfo[inSorePickUpInfoList.size()]);
		if (isLoggingDebug()) {
			vlogDebug("End of TRUInStorePickUpFormHandler.populateStorePickupInfo method");
		}
		return inStorePickupInfo;
		
	}
	/**
	 * @param pInStorePickupProperties InStorePickupProperties.
	 * @return inStorePickupVO inStorePickupVO
	 */
	private TRUStorePickUpInfo populateinStorePickupVO(String[] pInStorePickupProperties) {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:TRUInStorePickUpFormHandlert.populateinStorePickupVO method");
		}
		TRUStorePickUpInfo inStorePickupVO = new TRUStorePickUpInfo();
		if(pInStorePickupProperties.length>TRUConstants.FOUR){
			if(StringUtils.isBlank(pInStorePickupProperties[TRUConstants.ZERO]) || StringUtils.isBlank(pInStorePickupProperties[TRUConstants.ONE]) ||
					StringUtils.isBlank(pInStorePickupProperties[TRUConstants.TWO]) || 
					StringUtils.isBlank(pInStorePickupProperties[TRUConstants.THREE]) ||
					StringUtils.isBlank(pInStorePickupProperties[TRUConstants.FOUR])){
				return null;
			}
			inStorePickupVO.setShippingGroupId((pInStorePickupProperties[TRUConstants.ZERO]));
			//inStorePickupVO.setShippingGroupType((pInStorePickupProperties[TRUConstants.ONE]));
			inStorePickupVO.setFirstName((pInStorePickupProperties[TRUConstants.ONE]));
			inStorePickupVO.setLastName((pInStorePickupProperties[TRUConstants.TWO]));
			inStorePickupVO.setEmail((pInStorePickupProperties[TRUConstants.THREE]));
			inStorePickupVO.setPhoneNumber(pInStorePickupProperties[TRUConstants.FOUR]);
		}
		if(pInStorePickupProperties.length>TRUConstants.NINE){
			if(StringUtils.isBlank(pInStorePickupProperties[TRUConstants.FIVE])|| StringUtils.isBlank(pInStorePickupProperties[TRUConstants.SIX]) || 
					StringUtils.isBlank(pInStorePickupProperties[TRUConstants.SEVEN]) ||
					StringUtils.isBlank(pInStorePickupProperties[TRUConstants.EIGHT]) || StringUtils.isBlank(pInStorePickupProperties[TRUConstants.NINE])){
				return null;
			}
			inStorePickupVO.setAlternateInfoRequired(Boolean.parseBoolean((pInStorePickupProperties[TRUConstants.FIVE])));
			inStorePickupVO.setAlternateFirstName((pInStorePickupProperties[TRUConstants.SIX]));
			inStorePickupVO.setAlternateLastName((pInStorePickupProperties[TRUConstants.SEVEN]));
			inStorePickupVO.setAlternateEmail((pInStorePickupProperties[TRUConstants.EIGHT]));
			inStorePickupVO.setAlternatePhoneNumber((pInStorePickupProperties[TRUConstants.NINE]));
		}
		if(isLoggingDebug()){
			vlogDebug("TRUInStorePickUpFormHandler::@method::populateStorePickupInfo inStorePickupVO is: " + inStorePickupVO);
		}
		if (isLoggingDebug()) {
			vlogDebug("END:TRUInStorePickUpFormHandler.populateinStorePickupVO method");
		}
		return inStorePickupVO;
	}
	
	
	/**
	 * Handle retain pick up details.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @return true, if successful
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	
	public boolean handleRetainPickUpDetails(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException,IOException {
		if(isLoggingDebug()){
			vlogDebug("START of TRUInStorePickUpFormHandler.handleRetainPickUpDetails method for {0}",getOrder().getId());
		}
		
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String myHandleMethod = TRUConstants.HANDLE_RETAIN_PICKUP_DETAILS;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
		TRUStorePickUpInfo[] infos= getStorePickUpInfos();
		@SuppressWarnings({ "rawtypes", "unchecked" })
		Map<String, TRUStorePickUpInfo> storePickUpInfos = new LinkedHashMap();
		if(null!=infos && infos.length>0){
			for(TRUStorePickUpInfo pickUpInfo:infos){
				if(null!=pickUpInfo){
					storePickUpInfos.put(pickUpInfo.getShippingGroupId(), pickUpInfo);
				}
			}
			TRUOrderImpl truOrderImpl=(TRUOrderImpl)getOrder();
			truOrderImpl.setStorePickUpInfos(storePickUpInfos);
		}
		}
		if (rrm != null) {
			rrm.removeRequestEntry(myHandleMethod);
		}
		if(isLoggingDebug()){
			vlogDebug("END of TRUInStorePickUpFormHandler.handleRetainPickUpDetails method");
		}
		return true;
	}
	
	/**
	 * Clear pick up details from the session.
	 */
	private void clearPickUpDetails() {
		if(isLoggingDebug()){
			vlogDebug("START of TRUInStorePickUpFormHandler.clearPickUpDetails method");
		}
		
		TRUOrderImpl truOrderImpl=(TRUOrderImpl)getOrder();
		if(truOrderImpl.getStorePickUpInfos()!=null){
			truOrderImpl.getStorePickUpInfos().clear();
		}		
		if(isLoggingDebug()){
			vlogDebug("END of TRUInStorePickUpFormHandler.clearPickUpDetails method");
		}
	}
	
	/**
	 * This method is used to add calculate tax parameter.
	 * 
	 * @return Map
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected Map createRepriceParameterMap() {
		if (isLoggingDebug()) {
			vlogDebug("TRUCommitOrderFormHandler (createRepriceParameterMapToSkipTaxCal) : BEGIN");
		}
		Map parameters;
		parameters = super.createRepriceParameterMap();
		if (parameters == null) {
			parameters = new HashMap();
		}
		parameters.put(TRUTaxwareConstant.CALC_TAX, Boolean.TRUE);
		if (isLoggingDebug()) {
			vlogDebug("TRUCommitOrderFormHandler (createRepriceParameterMapToSkipTaxCal) : END");
		}
		return parameters;
	}
}
