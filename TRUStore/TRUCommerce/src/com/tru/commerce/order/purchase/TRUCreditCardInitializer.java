package com.tru.commerce.order.purchase;

import java.sql.Timestamp;
import java.util.List;

import atg.commerce.order.CreditCard;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.purchase.CreditCardInitializer;
import atg.commerce.order.purchase.PaymentGroupInitializationException;
import atg.commerce.order.purchase.PaymentGroupMapContainer;
import atg.commerce.profile.CommercePropertyManager;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.Profile;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConstants;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This class is for initializing credit card.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUCreditCardInitializer extends CreditCardInitializer {

	/**
	 * holds ShoppingCart instance.
	 */
	private OrderHolder mShoppingCart;
	/**
	 * holds Profile instance.
	 */
	private RepositoryItem mProfile;

	/** property: Reference to the BillingProcessHelper component. */
	private TRUBillingProcessHelper mBillingHelper;

	/**
	 * Gets the shopping cart.
	 * 
	 * @return the shoppingCart
	 */
	public OrderHolder getShoppingCart() {
		return mShoppingCart;
	}

	/**
	 * Sets the shopping cart.
	 * 
	 * @param pShoppingCart
	 *            the shoppingCart to set
	 */
	public void setShoppingCart(OrderHolder pShoppingCart) {
		mShoppingCart = pShoppingCart;
	}

	/**
	 * Gets the profile.
	 * 
	 * @return the profile
	 */
	public RepositoryItem getProfile() {
		return mProfile;
	}

	/**
	 * Sets the profile.
	 * 
	 * @param pProfile
	 *            the profile to set
	 */
	public void setProfile(RepositoryItem pProfile) {
		mProfile = pProfile;
	}

	/**
	 * Gets the billing helper.
	 * 
	 * @return the billingHelper
	 */
	public TRUBillingProcessHelper getBillingHelper() {
		return mBillingHelper;
	}

	/**
	 * Sets the billing helper.
	 * 
	 * @param pBillingHelper
	 *            the billingHelper to set
	 */
	public void setBillingHelper(TRUBillingProcessHelper pBillingHelper) {
		mBillingHelper = pBillingHelper;
	}

	/**
	 * Method copyConfiguration.
	 */
	@Override
	protected void copyConfiguration() {
		if (getConfiguration() != null) {
			super.copyConfiguration();
			if (getShoppingCart() == null) {
				setShoppingCart(getConfiguration().getShoppingCart());
			}
			if (getProfile() == null) {
				setProfile(getConfiguration().getProfile());
			}
		}
	}

	/**
	 * Initialize credit cards.
	 * 
	 * @param pProfile
	 *            Profile
	 * @param pPaymentGroupMapContainer
	 *            PaymentGroupMapContainer
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @throws PaymentGroupInitializationException
	 *             PaymentGroupInitializationException
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void initializeCreditCards(Profile pProfile, PaymentGroupMapContainer pPaymentGroupMapContainer,
			DynamoHttpServletRequest pRequest) throws PaymentGroupInitializationException {

		super.initializeCreditCards(pProfile, pPaymentGroupMapContainer, pRequest);
		// updates last order first credit card to payment group map container.
		final String initBasedOnLastOrder = pRequest.getParameter(TRUConstants.INIT_BASED_ON_LAST_ORDER);
		if (Boolean.valueOf(initBasedOnLastOrder)) {
			final TRUProfileTools profileTools = (TRUProfileTools) getPaymentGroupManager().getOrderTools().getProfileTools();

			final boolean isAnonymousUser = profileTools.isAnonymousUser(getProfile());
			if (!isAnonymousUser) {
				return;
			}
			final TRUOrderImpl lastOrder = (TRUOrderImpl) getShoppingCart().getLast();
			if (lastOrder != null && !lastOrder.getPaymentGroups().isEmpty()) {
				final List<PaymentGroup> paymentGroups = lastOrder.getPaymentGroups();
				for (PaymentGroup paymentGroup : paymentGroups) {
					if (paymentGroup instanceof CreditCard) {
						final TRUCreditCard orderCreditCard = (TRUCreditCard) paymentGroup;
						/*
						 * String creditCardType=((CreditCard)
						 * orderCreditCard).getCreditCardType(); String
						 * creditCardNumber=((CreditCard)
						 * orderCreditCard).getCreditCardNumber(); String
						 * paymentGroupName =
						 * creditCardType+" - "+creditCardNumber
						 * .substring(creditCardNumber.length()-4);
						 */
						if (!pPaymentGroupMapContainer.getPaymentGroupMap().isEmpty() && orderCreditCard != null) {
							pPaymentGroupMapContainer.setDefaultPaymentGroupName(getNickNameFromOrderCC(orderCreditCard));
							break;
						}

					}
				}
			}
		}
	}

	/**
	 * updates profile address nickname to credit card.
	 * 
	 * @param pPaymentMethod
	 *            paymentMethod
	 * @param pProfile
	 *            profile
	 * @return creditCard
	 * @throws PaymentGroupInitializationException
	 *             PaymentGroupInitializationException
	 */
	@Override
	protected CreditCard copyCreditCard(String pPaymentMethod, Profile pProfile) throws PaymentGroupInitializationException {

		final TRUProfileTools profileTools = (TRUProfileTools) getCommerceProfileTools();
		final CommercePropertyManager cpmgr = (CommercePropertyManager) profileTools.getPropertyManager();

		final TRUCreditCard creditCard = (TRUCreditCard) super.copyCreditCard(pPaymentMethod, pProfile);

		// get profile credit card
		final RepositoryItem profileCreditCard = profileTools.getCreditCardByNickname(pPaymentMethod, pProfile);
		if (profileCreditCard != null) {
			final RepositoryItem billingAddress = (RepositoryItem) profileCreditCard.getPropertyValue(cpmgr
					.getCreditCardBillingAddressPropertyName());
			if (billingAddress != null) {
				final RepositoryItem secondaryAddress = profileTools.getProfileAddressById(billingAddress.getRepositoryId());
				if (secondaryAddress != null) {
					final String addressNickName = profileTools.getProfileAddressName(pProfile, secondaryAddress);
					creditCard.setBillingAddressNickName(addressNickName);
				}
			}
			// Start :Added logic for sequence in displaying the creditcards
			Timestamp lastActivityTimeStamp = (Timestamp) profileCreditCard.getPropertyValue(((TRUPropertyManager)cpmgr).getLastActivityPropertyName());
			creditCard.setLastActivity(lastActivityTimeStamp);
			// End :Added logic for sequence in displaying the creditcards
		}
		return creditCard;
	}

	/**
	 * Gets the nick name from order cc.
	 * 
	 * @param pCreditCard
	 *            the credit card
	 * @return the nick name from order cc
	 */
	private String getNickNameFromOrderCC(CreditCard pCreditCard) {
		String nickname = TRUConstants.EMPTY_STRING;
		if (null != pCreditCard) {
			StringBuffer sb = new StringBuffer(pCreditCard.getCreditCardType());
			final String ccNumber = pCreditCard.getCreditCardNumber();
			final int cardNumberLength = pCreditCard.getCreditCardNumber().length();
			String lastFour = ccNumber.substring(cardNumberLength - TRUConstants.FOUR, cardNumberLength);
			sb.append(TRUCommerceConstants.HYPHEN_SPACE + lastFour);
			if (isLoggingDebug()) {
				logDebug("Nickname from creditCard: " + sb.toString());
			}
			nickname = sb.toString();
		}
		return nickname;
	}
}
