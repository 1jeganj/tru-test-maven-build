/**
 * 
 */
package com.tru.commerce.order.purchase;

import atg.nucleus.Nucleus;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;
import atg.repository.RepositoryView;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.locations.TRUStoreLocatorTools;

/**
 * The Class TRUEwasteStatePropertyDescriptor.
 */
public class TRUEwasteStatePropertyDescriptor extends RepositoryPropertyDescriptor {

	/**
	 * property to hold mLogger
	 */
	private static ApplicationLogging mLogger = ClassLoggingFactory.getFactory().getLoggerForClass(
			TRUEwasteStatePropertyDescriptor.class);

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant STORE. */
	private static final String STORE = "store";

	/** The Constant PARAL_STORE_LOCATOR_TOOLS. */
	private static final String PARAL_STORE_LOCATOR_TOOLS = "/com/tru/storelocator/tol/PARALStoreLocatorTools";

	/**
	 * This method returns the state of the locationId which user has selected for in store pickup shipping group.
	 * 
	 * @param pItem
	 *            repository item.
	 * @param pValue
	 *            cached value.
	 * @return returns object : state.
	 */
	@Override
	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {

		if (mLogger != null && mLogger.isLoggingDebug()) {
			mLogger.logDebug("TRUEwasteStatePropertyDescriptor.getPropertyValue(RepositoryItemImpl pItem, Object pValue) START");
		}
		String state = null;
		RepositoryItem[] storeItem = null;
		if(pValue == null){
			
			Nucleus nucleus = (Nucleus) Nucleus.getGlobalNucleus();
			TRUStoreLocatorTools storeLocatorTools = (TRUStoreLocatorTools) nucleus
					.resolveName(PARAL_STORE_LOCATOR_TOOLS);
			String locationId = (String) pItem.getPropertyValue(storeLocatorTools.getLocationPropertyManager()
					.getLocationIdPropertyName());
			try {
				RepositoryItemDescriptor store = storeLocatorTools.getStoreRepository().getItemDescriptor(STORE);
				if (store != null) {
					RepositoryView repositoryView = store.getRepositoryView();
					if (repositoryView != null) {
						QueryBuilder queryBuilder = repositoryView.getQueryBuilder();
						QueryExpression paramProperty = queryBuilder.createPropertyQueryExpression(storeLocatorTools
								.getLocationPropertyManager().getLocationIdPropertyName());
						QueryExpression paramValue = queryBuilder.createConstantQueryExpression(locationId);
						Query query = queryBuilder.createComparisonQuery(paramProperty, paramValue, QueryBuilder.EQUALS);
						storeItem = repositoryView.executeQuery(query);
						if (storeItem != null && storeItem.length > TRUCommerceConstants.INT_ZERO) {
							state = (String) storeItem[0].getPropertyValue(storeLocatorTools.getLocationPropertyManager()
									.getStateAddressPropertyName());
							pItem.setPropertyValue(storeLocatorTools.getCommercePropertyManager().getEwasteStatePropertyName(), state);
						}
					}
				}
				
			} catch (RepositoryException re) {
				if (mLogger != null && mLogger.isLoggingError()) {
					mLogger.logError("Repository Exception :", re);
				}
			}
		}else{
			state = (String) pValue;
		}
		if (mLogger != null && mLogger.isLoggingDebug()) {
			mLogger.logDebug("TRUEwasteStatePropertyDescriptor.getPropertyValue(RepositoryItemImpl pItem, Object pValue) END");
		}
		return state;
	}
}
