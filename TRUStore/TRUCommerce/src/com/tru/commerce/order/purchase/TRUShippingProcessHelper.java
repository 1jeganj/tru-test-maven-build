package com.tru.commerce.order.purchase;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.Days;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.Relationship;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.ShippingGroupManager;
import atg.commerce.order.purchase.CommerceItemShippingInfo;
import atg.commerce.order.purchase.ShippingGroupContainerService;
import atg.commerce.order.purchase.ShippingGroupInitializationException;
import atg.commerce.order.purchase.ShippingGroupMapContainer;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.PricingException;
import atg.commerce.profile.CommercePropertyManager;
import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.nucleus.naming.ComponentName;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryUtils;
import atg.service.pipeline.RunProcessException;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.vo.TRUChannelDetailsVo;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUChannelInStorePickupShippingGroup;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.TRUShippingGroupManager;
import com.tru.commerce.order.vo.ApplePayOrderSummaryVO;
import com.tru.commerce.order.vo.PromoStatusVO;
import com.tru.commerce.order.vo.TRUShipGroupKey;
import com.tru.commerce.pricing.TRUItemPriceInfo;
import com.tru.commerce.pricing.TRUOrderPriceInfo;
import com.tru.commerce.pricing.TRUPricingModelProperties;
import com.tru.commerce.pricing.TRUShipMethodVO;
import com.tru.commerce.pricing.TRUShippingPricingEngine;
import com.tru.commerce.pricing.TRUTaxPriceInfo;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConstants;
import com.tru.shipping.registry.TRURegistryServiceProcessor;

/**
 * Helper methods for handling shipping information.
 *
 * @author PA
 * @version 1.0
 */
public class TRUShippingProcessHelper extends GenericService {

	private static final String DEFAULT_OOTB__SHIPPING_METHOD = "hardgoodShippingGroup";

	/** hold CatalogProperties. */
	private TRUCatalogProperties  mCatalogProperties;

	/** hold mPurchaseProcessHelper. */
	private TRUPurchaseProcessHelper mPurchaseProcessHelper;

	/**
	 * Gets the catalog properties.
	 *
	 * @return the mCatalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the catalog properties.
	 *
	 * @param pCatalogProperties pCatalogProperties
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}
	/**property to hold promotionTools */
	private TRUPromotionTools mPromotionTools;
	/**
	 * @return the promotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * @param pPromotionTools the promotionTools to set
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		mPromotionTools = pPromotionTools;
	}
	/** property to Hold SHOPPING_CART_COMPONENT_NAME. */
	public final static ComponentName SHIPPING_GROUP_CONTAINER_SERVICE =
			ComponentName.getComponentName("/atg/commerce/order/purchase/ShippingGroupContainerService");

	/**
	 * 
	 */
	private final static String EDD_START_COUNTER="EDD_START_COUNTER";

	/**
	 * 
	 */
	private final static String  EDD_END_COUNTER="EDD_END_COUNTER";


	/**
	 * 
	 */
	private final static String EDD_WORKING_DATES_SET="EDD_WORKING_DATES_SET";


	/** constant for int 30. */
	public static final int THIRTY = 30;

	/**
	 * Tries to find an appropriate hardgood shipping group within the shipping
	 * group map container specified.
	 * 
	 * Shipping group is good enough if it has
	 * the same shipping address as the address specified or if it has empty
	 * address and the same name as shipping group that should be added. If such
	 * shipping group can't be found, this method will create a new one.
	 * 
	 * @param pProfile - Current Profile
	 * @param pNewShipToAddressName - Nick name
	 * @param pAddress - Address Obj
	 * @param pShippingGroupMapContainer - session scoped ShippingGroupMapContainer obj
	 * @throws CommerceException if something goes wrong.
	 */
	@SuppressWarnings("unchecked")
	public void findOrAddShippingGroupByNickname(RepositoryItem pProfile, String pNewShipToAddressName, Address pAddress,
			ShippingGroupMapContainer pShippingGroupMapContainer) throws CommerceException {
		// Check if shipping group with such name already exists.
		final ShippingGroup shippingGroup = ((Map<String, ShippingGroup>) pShippingGroupMapContainer.getShippingGroupMap())
				.get(pNewShipToAddressName);

		if (shippingGroup instanceof TRUHardgoodShippingGroup) {
			final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
			final Address shippingGroupAddress = hardgoodShippingGroup.getShippingAddress();
			if (shippingGroupAddress != null && shippingGroupAddress.getCountry() == null) {
				final Address address = hardgoodShippingGroup.getShippingAddress();
				OrderTools.copyAddress(pAddress, address);
				hardgoodShippingGroup.setNickName(pNewShipToAddressName);
				return;
			}
		}
		addShippingAddress(pProfile, pNewShipToAddressName, pAddress, pShippingGroupMapContainer);
		// Set Default Address if the user is logged in
		if (!getPurchaseProcessHelper().getProfileTools().isAnonymousUser(pProfile)) {
			try {
				getPurchaseProcessHelper().getProfileTools().setProfileDefaultAddress(pProfile);
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError(
							"RepositoryException in TRUShippingProcessHelper.findOrAddShippingGroupByNickname while calling setProfileDefaultAddress()",
							e);
				}
			}
		}
	}

	/**
	 * <p>
	 * Creates a new shipping group and adds it to the shipping group map
	 * container.
	 * </p>
	 * <p>
	 * Optionally saves the shipping group address to the profile based on the
	 * saveShippingAddress option.
	 * </p>
	 *
	 * @param pProfile - shopper profile.
	 * @param pNewShipToAddressName - Address nickname to use for the address being added.
	 * @param pAddress - Address to add.
	 * @param pShippingGroupMapContainer - map of all shipping groups for the profile.
	 * @return the TRUHardgoodShippingGroup - hgsg
	 * @throws CommerceException indicates that a severe error occurred while performing a commerce operation.
	 */
	public TRUHardgoodShippingGroup addShippingAddress(RepositoryItem pProfile,
			String pNewShipToAddressName, Address pAddress,
			ShippingGroupMapContainer pShippingGroupMapContainer) throws CommerceException {
		final ShippingGroupManager sgm = getPurchaseProcessHelper().getShippingGroupManager();
		final TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) pShippingGroupMapContainer;

		// Create a new shipping group with the new address.
		final TRUHardgoodShippingGroup hgsg = (TRUHardgoodShippingGroup) sgm.createShippingGroup();
		final Address address = hgsg.getShippingAddress();
		OrderTools.copyAddress(pAddress, address);

		hgsg.setNickName(pNewShipToAddressName);
		hgsg.setLastActivity(new Timestamp(new Date().getTime()));
		// Put the new shipping group in the container.
		pShippingGroupMapContainer.addShippingGroup(pNewShipToAddressName, hgsg);		
		//Set Default when first address added
		if (shippingGroupMapContainer.getHardGoodShipGroupCount()==TRUConstants.ONE) {
			shippingGroupMapContainer.setDefaultShippingGroupName(pNewShipToAddressName);
		}

		final TRUProfileTools profileTools = (TRUProfileTools) getPurchaseProcessHelper().getOrderTools().getProfileTools();
		final boolean isAnonymousUser = profileTools.isAnonymousUser(pProfile);
		if (!isAnonymousUser) {
			saveAddressToAddressBook(pAddress, pNewShipToAddressName, pProfile);
		}
		return hgsg;
	}

	/**
	 * Removing address from profile and container
	 * Remove address from order if it contains.
	 *
	 * @param pOrder order
	 * @param pProfile profile
	 * @param pRemoveShippingAddressNickName removeShippingAddressNickName
	 * @param pShippingGroupMapContainer shippingGroupMapContainer
	 * @throws CommerceException CommerceException
	 * @throws RepositoryException RepositoryException
	 */
	@SuppressWarnings("unchecked")
	public void removeShippingAddress(Order pOrder, RepositoryItem pProfile, String pRemoveShippingAddressNickName,
			ShippingGroupMapContainer pShippingGroupMapContainer) throws CommerceException, RepositoryException {

		if(pOrder==null || pProfile==null ||pShippingGroupMapContainer==null ||StringUtils.isBlank(pRemoveShippingAddressNickName)){
			throw new InvalidParameterException(TRUCheckoutConstants.ORDER_IS_NULL);
		}
		final TRUOrderImpl order = (TRUOrderImpl) pOrder;
		final TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) pShippingGroupMapContainer;
		// remove from container
		final TRUHardgoodShippingGroup hgsg = (TRUHardgoodShippingGroup) shippingGroupMapContainer
				.getShippingGroup(pRemoveShippingAddressNickName);
		if (hgsg != null && TRUCheckoutConstants.PAYMENT_TAB.equals(order.getCurrentTab()) && hgsg.isOrderShippingAddress() && pProfile.isTransient()) {
			hgsg.setSoftDeleted(true);
			hgsg.setBillingAddressIsSame(false);
			return;
		} else  if(hgsg != null) {
			if (pRemoveShippingAddressNickName.equalsIgnoreCase(shippingGroupMapContainer.getDefaultShippingGroupName())) {
				shippingGroupMapContainer.setDefaultShippingGroupName(null);
			}
			shippingGroupMapContainer.removeShippingGroup(pRemoveShippingAddressNickName);
		}


		// remove from profile
		final TRUProfileTools profileTools = (TRUProfileTools) getPurchaseProcessHelper().getOrderTools().getProfileTools();
		if (!profileTools.isAnonymousUser(pProfile)) {
			processRemoveProfile(pProfile, pRemoveShippingAddressNickName, profileTools, profileTools.isAnonymousUser(pProfile));
		}

		// remove address from order
		if (order.isOrderIsInMultiShip()) {
			final List<TRUHardgoodShippingGroup> matchedShippingGroups = getMatchedHardGoodShippingGroups(pOrder,
					pRemoveShippingAddressNickName);
			for (TRUHardgoodShippingGroup matchedShipGroup : matchedShippingGroups) {
				if (matchedShipGroup != null) {
					final String oldShippingMethod = matchedShipGroup.getShippingMethod();
					final double oldShippingPrice = matchedShipGroup.getShippingPrice();
					final List<CommerceItemRelationship> ciRels = new ArrayList<CommerceItemRelationship>(
							matchedShipGroup.getCommerceItemRelationships());
					if (ciRels != null && !ciRels.isEmpty()) {
						for (CommerceItemRelationship ciRel : ciRels) {
							updateQtyToSG(pOrder, order, matchedShipGroup, oldShippingMethod, oldShippingPrice, ciRel);
						}
					}
				}
			}
		} else {
			final TRUHardgoodShippingGroup hardgoodShippingGroup = getPurchaseProcessHelper().getFirstHardgoodShippingGroup(order);
			final String defaultNickName = findDefaultShippingAddress(shippingGroupMapContainer);
			// setting default shipping address to order
			if (StringUtils.isNotBlank(defaultNickName) && shippingGroupMapContainer.getShippingGroup(defaultNickName) != null) {
				final TRUHardgoodShippingGroup containerShippingGroup = (TRUHardgoodShippingGroup) shippingGroupMapContainer
						.getShippingGroup(defaultNickName);
				hardgoodShippingGroup.setNickName(defaultNickName);
				OrderTools.copyAddress(containerShippingGroup.getShippingAddress(), hardgoodShippingGroup.getShippingAddress());
			} else {
				// setting empty address to order
				hardgoodShippingGroup.setNickName(null);
				hardgoodShippingGroup.setShippingMethod(TRUConstants.EMPTY);
				hardgoodShippingGroup.setShippingPrice(TRUConstants.DOUBLE_ZERO);
				OrderTools.copyAddress(new ContactInfo(), hardgoodShippingGroup.getShippingAddress());
			}
			updateShippingMethodAndPrice(pOrder, hardgoodShippingGroup,(TRUShippingGroupContainerService)pShippingGroupMapContainer);
		}
		getPurchaseProcessHelper().getShippingGroupManager().removeEmptyShippingGroups(order);
	}


	/**
	 * Update qty to sg.
	 *
	 * @param pOrder the order
	 * @param pTRUOrder the tRU order
	 * @param pHardgoodShippingGroup the hardgood shipping group
	 * @param pOldShippingMethod the old shipping method
	 * @param pOldShippingPrice the old shipping price
	 * @param pCiRel the ci rel
	 * @throws CommerceException the commerce exception
	 */
	private void updateQtyToSG(Order pOrder, TRUOrderImpl pTRUOrder, TRUHardgoodShippingGroup pHardgoodShippingGroup,
			String pOldShippingMethod, double pOldShippingPrice, CommerceItemRelationship pCiRel) throws CommerceException {
		if (pCiRel != null && pCiRel.getCommerceItem() != null) {
			final CommerceItem commerceItem = pCiRel.getCommerceItem();
			final TRUHardgoodShippingGroup matchedShippingGroup = getPurchaseProcessHelper().getEmptyHardgoodShippingGroupByMethod(pOrder, pOldShippingMethod,
					pOldShippingPrice, pHardgoodShippingGroup.getShippingGroupClassType());
			if(matchedShippingGroup == null){
				return;
			}
			//Start :Added logic for resetting the registry data
			if(matchedShippingGroup instanceof TRUChannelHardgoodShippingGroup && pHardgoodShippingGroup instanceof TRUChannelHardgoodShippingGroup){
				TRUChannelHardgoodShippingGroup matchedChannelSG= (TRUChannelHardgoodShippingGroup)matchedShippingGroup;
				TRUChannelHardgoodShippingGroup channelOldShippingGroup = (TRUChannelHardgoodShippingGroup) pHardgoodShippingGroup;
				matchedChannelSG.setChannelId(channelOldShippingGroup.getChannelId());
				matchedChannelSG.setChannelType(channelOldShippingGroup.getChannelType());
				matchedChannelSG.setChannelName(channelOldShippingGroup.getChannelName());
				matchedChannelSG.setChannelUserName(channelOldShippingGroup.getChannelUserName());
				matchedChannelSG.setChannelCoUserName(channelOldShippingGroup.getChannelCoUserName());
				matchedChannelSG.setRegistrantId(channelOldShippingGroup.getRegistrantId());
				matchedChannelSG.setRegistryAddress(channelOldShippingGroup.isRegistryAddress());
				matchedChannelSG.setSourceChannel(channelOldShippingGroup.getSourceChannel());
			}
			//End :Added logic for resetting the registry data
			Address address=matchedShippingGroup.getShippingAddress();
			if(null!=address && StringUtils.isNotBlank(address.getFirstName())&&StringUtils.isNotBlank(address.getLastName()) ){
				setDefaultShippingAddress(pOrder, matchedShippingGroup);
			}
			//updateShippingMethodAndPrice(pOrder, matchedShippingGroup);
			setShipMethodAndPrice(pOrder, pCiRel.getCommerceItem(), matchedShippingGroup, pOldShippingMethod);
			if (matchedShippingGroup != null) {
				getPurchaseProcessHelper().getOrderManager().getCommerceItemManager().removeItemQuantityFromShippingGroup(pTRUOrder, commerceItem.getId(),
						pHardgoodShippingGroup.getId(), pCiRel.getQuantity());
				getPurchaseProcessHelper().getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(pTRUOrder, commerceItem.getId(),
						matchedShippingGroup.getId(), pCiRel.getQuantity());
			}
		}
	}


	/**
	 * Process remove profile.
	 *
	 * @param pProfile the profile
	 * @param pRemoveShippingAddressNickName the remove shipping address nick name
	 * @param pProfileTools the profile tools
	 * @param pIsAnonymousUser the is anonymous user
	 * @throws RepositoryException the repository exception
	 */
	@SuppressWarnings("unchecked")
	private void processRemoveProfile(RepositoryItem pProfile, String pRemoveShippingAddressNickName,
			TRUProfileTools pProfileTools, boolean pIsAnonymousUser) throws RepositoryException {
		if (!pIsAnonymousUser) {
			final CommercePropertyManager commercePropertyManager = (CommercePropertyManager) pProfileTools.getPropertyManager();
			final Map<String, RepositoryItem> addresses = (Map<String, RepositoryItem>) pProfile
					.getPropertyValue(commercePropertyManager.getSecondaryAddressPropertyName());

			if (!addresses.containsKey(pRemoveShippingAddressNickName)) {
				return;
			}
			pProfileTools.removeProfileRepositoryAddress(pProfile, pRemoveShippingAddressNickName, true);
			pProfileTools.setProfileDefaultAddress(pProfile);
		}
	}


	/**
	 * Sets the as default shipping address.
	 *
	 * @param pProfile the profile
	 * @param pDefaultShippingAddressNickName the default shipping address nick name
	 * @param pShippingGroupMapContainer the shipping group map container
	 * @throws RepositoryException the repository exception
	 */
	public void setAsDefaultShippingAddress(RepositoryItem pProfile, String pDefaultShippingAddressNickName,
			ShippingGroupMapContainer pShippingGroupMapContainer) throws RepositoryException {
		pShippingGroupMapContainer.setDefaultShippingGroupName(pDefaultShippingAddressNickName);
		String prevShippingAddressNickNamePropertyName = ((TRUShippingGroupContainerService)pShippingGroupMapContainer).getSelectedAddressNickName();
		((TRUShippingGroupContainerService)pShippingGroupMapContainer).setSelectedAddressNickName(pDefaultShippingAddressNickName);
		if (getPurchaseProcessHelper().getOrderTools().getProfileTools() instanceof TRUProfileTools) {
			final TRUProfileTools profileTools = (TRUProfileTools) getPurchaseProcessHelper().getOrderTools().getProfileTools();
			final boolean isAnonymousUser = profileTools.isAnonymousUser(pProfile);
			if (!isAnonymousUser) {
				final RepositoryItem newDefaultShippingAddress = profileTools.getProfileAddress(pProfile,
						pDefaultShippingAddressNickName);
				profileTools.updateProperty(getPurchaseProcessHelper().getPropertyManager().getShippingAddressPropertyName(), newDefaultShippingAddress,
						pProfile);
				profileTools.updateProperty(getPurchaseProcessHelper().getPropertyManager().getPrevShippingAddressNickNamePropertyName(),
						prevShippingAddressNickNamePropertyName, pProfile);
			}
		}
	}

	/**
	 * Saves address to address book.
	 * 
	 * @param pAddress
	 *            - address Address to save.
	 * @param pNickName
	 *            - nickname - Nickname for the address being saved.
	 * @param pProfile
	 *            - the profile.
	 * 
	 * @throws CommerceException
	 *             indicates that a severe error occurred while performing a
	 *             commerce operation.
	 */
	public void saveAddressToAddressBook(Address pAddress, String pNickName, RepositoryItem pProfile) throws CommerceException {
		getPurchaseProcessHelper().getOrderTools().saveAddressToAddressBook(pProfile, pAddress, pNickName);
	}

	/**
	 * Modify shipping address.
	 *
	 * @param pProfile profile
	 * @param pEditShipToAddressName editShipToAddressName
	 * @param pEditAddress editAddress
	 * @param pShippingGroupMapContainer shippingGroupMapContainer
	 * @param pOrder order
	 */
	public void modifyShippingAddress(RepositoryItem pProfile, String pEditShipToAddressName, Address pEditAddress,
			ShippingGroupMapContainer pShippingGroupMapContainer, Order pOrder) {
		if (StringUtils.isNotBlank(pEditShipToAddressName)) {
			final TRUHardgoodShippingGroup hgsg = (TRUHardgoodShippingGroup) pShippingGroupMapContainer
					.getShippingGroup(pEditShipToAddressName);
			if (hgsg != null) {
				hgsg.setLastActivity(new Timestamp(new Date().getTime()));
				hgsg.setShippingAddress(pEditAddress);
				if(hgsg.isSoftDeleted()) {
					hgsg.setSoftDeleted(false);
				}
			}
			for (Object sg : pOrder.getShippingGroups()) {
				final ShippingGroup shippingGroup = (ShippingGroup) sg;
				if (shippingGroup instanceof TRUHardgoodShippingGroup) {
					final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
					if (hardgoodShippingGroup != null
							&& pEditShipToAddressName.equalsIgnoreCase(hardgoodShippingGroup.getNickName())) {
						hardgoodShippingGroup.setShippingAddress(pEditAddress);
						updateShippingMethodAndPrice(pOrder, hardgoodShippingGroup,(TRUShippingGroupContainerService)pShippingGroupMapContainer);
					}
				}
			}
			final TRUProfileTools profileTools = (TRUProfileTools) getPurchaseProcessHelper().getOrderManager().getOrderTools().getProfileTools();
			final boolean isAnonymousUser = profileTools.isAnonymousUser(pProfile);
			if (!isAnonymousUser) {
				saveModifiedShippingAddressToProfile(pProfile, pEditShipToAddressName, pEditAddress);
			}
		}
	}

	/**
	 * Save modified shipping address to profile.
	 *
	 * @param pProfile profile
	 * @param pEditShippingAddressNickName editShippingAddressNickName
	 * @param pEditAddress editAddress
	 */
	public void saveModifiedShippingAddressToProfile(RepositoryItem pProfile, String pEditShippingAddressNickName,
			Address pEditAddress) {
		final TRUProfileTools profileTools = (TRUProfileTools) getPurchaseProcessHelper().getOrderManager().getOrderTools().getProfileTools();
		/* boolean isAnonymousUser = profileTools.isAnonymousUser(pProfile); */
		final CommercePropertyManager commercePropertyManager = (CommercePropertyManager) profileTools.getPropertyManager();
		final Map addresses = (Map) pProfile.getPropertyValue(commercePropertyManager.getSecondaryAddressPropertyName());

		if (addresses.containsKey(pEditShippingAddressNickName)) {
			final RepositoryItem address = profileTools.getProfileAddress(pProfile, pEditShippingAddressNickName);

			if (address == null) {
				return;
			}
			try {
				final MutableRepositoryItem mutAddress = RepositoryUtils.getMutableRepositoryItem(address);
				final MutableRepository mutrep = (MutableRepository) mutAddress.getRepository();
				OrderTools.copyAddress(pEditAddress, mutAddress);
				mutrep.updateItem(mutAddress);
			} catch (CommerceException e) {
				if (isLoggingError()) {
					vlogError(
							"Exception occured while saving modified shipping address in profile @Class:::TRUShippingProcessHelper:::@method::saveModifiedShippingAddressToProfile()",
							e);
				}
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					vlogError(
							"Exception occured while saving modified shipping address in profile @Class:::TRUShippingProcessHelper:::@method::saveModifiedShippingAddressToProfile()",
							e);
				}
			}
		}
	}

	/**
	 * Update shipping methods and its prices for shipping group on adding a item.
	 *
	 * @param pOrder order
	 * @param pShippingGroup shipping group
	 */
	public void updateShippingMethodAndPrice(Order pOrder, ShippingGroup pShippingGroup) {
		if (isLoggingDebug()) {
			vlogDebug("Start TRUPurchaseProcessHelper.updateShippingMethodAndPrice for pShippingGroup : {0} in Order : {1}", pShippingGroup ,pOrder.getId());
			//	vlogDebug("pOrder : {0} pShippingGroup : {1}", pOrder.getId(), pShippingGroup);
		}
		if (pShippingGroup instanceof TRUHardgoodShippingGroup && !(pShippingGroup instanceof TRUChannelHardgoodShippingGroup)) {
			final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) pShippingGroup;
			if (hardgoodShippingGroup.getCommerceItemRelationshipCount() == 0) {
				if (isLoggingDebug()) {
					logDebug("Commerce item count is zero");
				}
				hardgoodShippingGroup.setShippingMethod(TRUConstants.EMPTY);
				hardgoodShippingGroup.setShippingPrice(TRUCheckoutConstants.MINIMUM_PRICE);
				hardgoodShippingGroup.setNickName(TRUConstants.EMPTY);
				return;
			}
			setShipMethodAndPrice(pOrder,null, hardgoodShippingGroup,hardgoodShippingGroup.getShippingMethod());
		}
		if (isLoggingDebug()) {
			vlogDebug("End TRUPurchaseProcessHelper.updateShippingMethodAndPrice");
		}
	}


	/**
	 * <p>
	 * This method will set the default ship method and price based on shipping address.
	 * </p>
	 *
	 * @param pOrder - Order
	 * @param pCommerceItem - CommerceItem
	 * @param pHardgoodShippingGroup - hardgoodShippingGroup
	 * @param pOldShipMethod OldShipMethod
	 */
	public void setShipMethodAndPrice(Order pOrder, CommerceItem pCommerceItem, TRUHardgoodShippingGroup pHardgoodShippingGroup,
			String pOldShipMethod) {
		final String region = getPurchaseProcessHelper().getShippingManager().getShippingRegion(pHardgoodShippingGroup.getShippingAddress());
		pHardgoodShippingGroup.setRegionCode(region);
		List<TRUShipMethodVO> shipMethodVOs = new ArrayList<TRUShipMethodVO>();
		if (pCommerceItem == null) {
			//Logic for retrieving the ship methods based on order items
			shipMethodVOs = getPurchaseProcessHelper().getShippingManager().getShippingMethods(pOrder,region,false);
		} else {
			//Logic for retrieving the ship methods for a particular item
			final String frieghtClass = getPurchaseProcessHelper().getShippingManager().getFreightClassFromCI(pCommerceItem);
			if(StringUtils.isNotBlank(frieghtClass)){
				shipMethodVOs = getPurchaseProcessHelper().getShippingManager().getShipMethods(frieghtClass, region, pOrder, true);
			}
		}
		if (null != shipMethodVOs && shipMethodVOs.isEmpty()) {
			pHardgoodShippingGroup.setShippingMethod(TRUConstants.EMPTY);
			pHardgoodShippingGroup.setShippingPrice(TRUConstants.DOUBLE_ZERO);
			if (isLoggingInfo()) {
				vlogInfo("No ship method found for the one of the item in order #{0}", pOrder.getId());
			}
			return;
		}
		if (!getPurchaseProcessHelper().getShippingManager().isPreferredShippingExistis(shipMethodVOs, pOldShipMethod)) {
			TRUShipMethodVO defaultShipMethod = shipMethodVOs.get(TRUConstants._0);
			//if(DEFAULT_OOTB__SHIPPING_METHOD.equals(pOldShipMethod)){
				List<String> shippingMethodsFromPromo = getPurchaseProcessHelper().getShippingManager().getShippingMethodFromPromo();
					OUTER:
					for(int i = TRUConstants._0 ; i <shipMethodVOs.size() ; i++){
						TRUShipMethodVO lshipMethodVO = shipMethodVOs.get(i);
						INNER1:
						for(int j =TRUConstants._0 ;j <shippingMethodsFromPromo.size() ; j++){
							 if(shippingMethodsFromPromo.get(j).equals(lshipMethodVO.getShipMethodCode())){
								defaultShipMethod = lshipMethodVO;
								break OUTER;
							 }
					}					
				}
			//}						
			// If the preferred ship method is not found ,apply cheap shipping method and its corresponding price
			if (isLoggingDebug()) {
				vlogDebug("applying least shipping method : {0} and its price is {1} for the order {2}",
						defaultShipMethod.getShipMethodCode(), defaultShipMethod.getShippingPrice(), pOrder.getId());
			}
			pHardgoodShippingGroup.setShippingMethod(defaultShipMethod.getShipMethodCode());
			pHardgoodShippingGroup.setShippingPrice(defaultShipMethod.getShippingPrice());
		} else {
			// If the shipping method is same ,Update the shipping price with respect to region.
			for (TRUShipMethodVO methodVO : shipMethodVOs) {
				if (methodVO.getShipMethodCode().equalsIgnoreCase(pHardgoodShippingGroup.getShippingMethod())) {
					pHardgoodShippingGroup.setShippingPrice(methodVO.getShippingPrice());
					break;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End setShipMethodAndPrice() for Order Id {0}", pOrder.getId());
		}
	}


	/**
	 * update Estimate delivery date to shipping group commerce item relationships.
	 *
	 * @param pOrder order
	 */
	@SuppressWarnings("unchecked")
	public void updateEstimateDeliveryDates(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("Start TRUPurchaseProcessHelper.updateEstimateDeliveryDates for order {0}",pOrder.getId());
		}
		final List<Relationship> relationships = pOrder.getRelationships();
		if (relationships != null && !relationships.isEmpty()) {
			for (Relationship relationship : relationships) {
				if (relationship instanceof TRUShippingGroupCommerceItemRelationship) {
					final TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) relationship;
					updateEstimateDeliveryDate(sgCiRel);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End TRUPurchaseProcessHelper.updateEstimateDeliveryDates  for order {0}",pOrder.getId());
		}
	}

	/**
	 * update Estimate delivery date to shipping group commerce item relationships.
	 *
	 * @param pSgCiRel shipping group commerce item rel
	 */
	public void updateEstimateDeliveryDate(ShippingGroupCommerceItemRelationship pSgCiRel) {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUShippingProcessHelper.updateEstimateDeliveryDate method");
		}
		final TRUCatalogTools catalogTools = (TRUCatalogTools) getPurchaseProcessHelper().getOrderManager().getOrderTools().getCatalogTools();
		if (pSgCiRel instanceof TRUShippingGroupCommerceItemRelationship) {
			final TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) pSgCiRel;
			if (sgCiRel != null) {
				final CommerceItem commerceItem = sgCiRel.getCommerceItem();
				final ShippingGroup shippingGroup = sgCiRel.getShippingGroup();
				if (shippingGroup instanceof TRUHardgoodShippingGroup) {
					final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
					if (commerceItem == null || hardgoodShippingGroup == null || commerceItem instanceof TRUGiftWrapCommerceItem ||
							commerceItem instanceof TRUDonationCommerceItem) {
						return;
					}
					int estimateShipWindowMax = 0;
					int estimateShipWindowMin = 0;
					try {
						final RepositoryItem skuItem = catalogTools.findSKU(commerceItem.getCatalogRefId());
						if (skuItem != null) {
							//final String presellQuantityUnits = (String) skuItem.getPropertyValue(getCatalogProperties().getPresellQuantityUnits());
							final Date streetDate = (Date) skuItem.getPropertyValue(getCatalogProperties().getStreetDate());
							final boolean isPresellItem = isStreetDateIsFutureDate(streetDate);
							//Start : This logic has been implemented as part of #TUW-52221
							if (isPresellItem && getPurchaseProcessHelper().getConfiguration().isEnablePreSellEDD()) {
								calculateEDDForPreSellItem(sgCiRel, streetDate);
								return;
							}
							//End : This logic has been implemented as part of #TUW-52221
							final String type = (String) skuItem.getPropertyValue(getPurchaseProcessHelper().getOrderTools().getPricingTools().getCatalogProperties().getTypePropertyName());
							if (!TRUConstants.NON_MERCH_SKU.equalsIgnoreCase(type)) {
								final String shipWindowMin = (String) skuItem.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getShipWindowMin());
								final String shipWindowMax = (String) skuItem.getPropertyValue(getPurchaseProcessHelper().getCommercePropertyManager().getShipWindowMax());
								try {
									if (StringUtils.isNotBlank(shipWindowMin)) {
										estimateShipWindowMin = Integer.parseInt(shipWindowMin.trim());
									}
									if (StringUtils.isNotBlank(shipWindowMax)) {
										estimateShipWindowMax = Integer.parseInt(shipWindowMax.trim());
									}
								} catch (NumberFormatException e) {
									vlogError("NumberFormatException for shipWindowMin : {0} or shipWindowMax :{1} with exception {2} @Class:::TRUShippingProcessHelper:::@method::updateEstimateDeliveryDate()",shipWindowMin, shipWindowMax, e);
								}
								vlogDebug("1 . shipWindowMin/Max from sku item :{0} and {1}" , estimateShipWindowMin ,estimateShipWindowMax);
							}
						}
					} catch (RepositoryException e) {
						vlogError("RepositoryException for findSKU(){0} with exception {1} @Class:::TRUShippingProcessHelper:::@method::updateEstimateDeliveryDate()",commerceItem.getCatalogRefId(), e);
					}
					//Start:Region based ship window
					Map<String,Integer> resultMap=getPurchaseProcessHelper().getShippingManager().getRegionBasedShipWindow(hardgoodShippingGroup);
					estimateShipWindowMin+=resultMap.get(TRUConstants.EDD_SHIP_MIN);
					estimateShipWindowMax+=resultMap.get(TRUConstants.EDD_SHIP_MAX);
					vlogDebug("2 - After RegionBasedShipWindow Min/Max :{0} and {1}" , estimateShipWindowMin ,estimateShipWindowMax);
					//End:Region based ship window
					//Start:cutOffTime
					int cutOffTime = getPurchaseProcessHelper().getShippingManager().getEddCutOffTime();
					GregorianCalendar greCal = new GregorianCalendar();
					int hourOfDay=greCal.get(Calendar.HOUR_OF_DAY);
					vlogDebug("HOUR_OF_DAY is {0} and CUT_OFF_TIME is {1} ",hourOfDay,cutOffTime);
					if (hourOfDay <= cutOffTime) {
						if(estimateShipWindowMin>=TRUCheckoutConstants.INT_ONE){
							estimateShipWindowMin -= TRUCheckoutConstants.INT_ONE;
						}
						if(estimateShipWindowMax>=TRUCheckoutConstants.INT_ONE){
							estimateShipWindowMax -= TRUCheckoutConstants.INT_ONE;
						}
						vlogDebug("CUT OFF Time considered");
					}
					vlogDebug("3 - After CUT OFF Time Min/Max :{0} and {1}" , estimateShipWindowMin ,estimateShipWindowMax);
					//End:cutOffTime
					/**
					 * Start : Implementation for TUW-53260
					 */
					if(getPurchaseProcessHelper().getShippingManager().isExcludeShipHolidays()){
						Map<String,Object> map= calculateEDDWorkingDates(estimateShipWindowMin,estimateShipWindowMax);
						int start=(Integer)map.get(EDD_START_COUNTER);
						int end=(Integer)map.get(EDD_END_COUNTER);
						//map.get("EDD_WORKING_DATES_SET");
						estimateShipWindowMin=estimateShipWindowMin+start;
						estimateShipWindowMax=estimateShipWindowMax+end;
						vlogDebug("4 - After excluding ship holidays Min/Max  ::{0} and {1}" , estimateShipWindowMin,estimateShipWindowMax);
					}
					/**
					 * End :  Start : Implementation for TUW-53260
					 */
					sgCiRel.setEstimateShipWindowMin(estimateShipWindowMin);
					sgCiRel.setEstimateShipWindowMax(estimateShipWindowMax);
				}
			}
		}if (isLoggingDebug()) {
			vlogDebug("End of TRUShippingProcessHelper.updateEstimateDeliveryDate method");
		}
	}


	/**
	 * This method is used to calculate EDDForPreSellItem.
	 * 
	 * @param pSgCiRel - Shipping Group CommerceItem Relationship
	 * @param pStreetDate -street date
	 */
	private void calculateEDDForPreSellItem(final TRUShippingGroupCommerceItemRelationship pSgCiRel, final Date pStreetDate) {
		if(isLoggingDebug()){
			vlogDebug("Start of TRUShippingProcessHelper.calculateEDDForPreSellItem()");
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(getPurchaseProcessHelper().getConfiguration().getPreSellDateFormat());
		try {
			String dateStart=simpleDateFormat.format(Calendar.getInstance().getTime());
			String dateEnd=simpleDateFormat.format(pStreetDate);
			long diff=simpleDateFormat.parse(dateEnd).getTime()-simpleDateFormat.parse(dateStart).getTime();
			int noOfDays = (int)(diff / (TRUConstants.INT_TWENTY_FOUR *TRUConstants.INT_SIXTY * TRUConstants.INT_SIXTY * TRUConstants.INT_THOUSAND));
			int cutOffTime = getPurchaseProcessHelper().getShippingManager().getEddCutOffTime();
			GregorianCalendar greCal = new GregorianCalendar();
			int estimateShipWindowMin=TRUConstants.ZERO;
			int estimateShipWindowMax=TRUConstants.ZERO;

			if (greCal.get(Calendar.HOUR_OF_DAY) >= cutOffTime) {
				estimateShipWindowMin = TRUCheckoutConstants.INT_ONE;
				estimateShipWindowMax = TRUCheckoutConstants.INT_ONE;
			}

			pSgCiRel.setEstimateShipWindowMin(estimateShipWindowMin);
			pSgCiRel.setEstimateShipWindowMax(estimateShipWindowMax+noOfDays);
			pSgCiRel.setStreetDate(pStreetDate);
			if(isLoggingDebug()){
				vlogDebug("dateStart : {0} ,dateEnd :{1} ,noOfDays :{2} ", dateStart,dateEnd,noOfDays);
			}
		} catch (ParseException e) {
			if(isLoggingError()){
				logError("calculateEDDForPreSellItem: ParseException while parsing the streetDate",e);
			}
		}
		if(isLoggingDebug()){
			vlogDebug("End of TRUShippingProcessHelper.calculateEDDForPreSellItem()");
		}
	}

	/**
	 * returns unique shipping group key for the given relationship.
	 *
	 * @param pOrder order
	 * @param pSgCiRel shipping group commecre item rel
	 * @return TRUShipGroupKey
	 */
	public TRUShipGroupKey findShipGroupKey(Order pOrder, ShippingGroupCommerceItemRelationship pSgCiRel) {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUShippingProcessHelper.findShipGroupKey method");
		}
		if (pSgCiRel instanceof TRUShippingGroupCommerceItemRelationship) {
			final TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) pSgCiRel;
			final ShippingGroup shippingGroup = pSgCiRel.getShippingGroup();
			if (shippingGroup instanceof TRUHardgoodShippingGroup) {
				final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) sgCiRel.getShippingGroup();

				final String freightClass = getPurchaseProcessHelper().getShippingManager().getFreightClassFromCI(sgCiRel.getCommerceItem());
				final String region = getPurchaseProcessHelper().getShippingManager().getShippingRegion(hardgoodShippingGroup.getShippingAddress());

				final List<TRUShipMethodVO> shipMethodVOs = getPurchaseProcessHelper().getShippingManager().getShipMethods (
						freightClass, region, pOrder, true);
				final TRUShipGroupKey shipGroupKey = new TRUShipGroupKey(shipMethodVOs);
				shipGroupKey.setEstimateShipWindowMin(sgCiRel.getEstimateShipWindowMin());
				shipGroupKey.setEstimateShipWindowMax(sgCiRel.getEstimateShipWindowMax());
				shipGroupKey.setSelectedShippingMethod(sgCiRel.getShippingGroup().getShippingMethod());
				shipGroupKey.setNickName(hardgoodShippingGroup.getNickName());
				if (isLoggingDebug()) {
					vlogDebug("ship group key for the relatioship : {0} is {1}", pSgCiRel.getId(), shipGroupKey);
				}
				return shipGroupKey;
			} else if (shippingGroup instanceof TRUInStorePickupShippingGroup) {
				final TRUInStorePickupShippingGroup inStorePickupShippingGroup = (TRUInStorePickupShippingGroup) shippingGroup;
				final TRUShipGroupKey shipGroupKey = new TRUShipGroupKey();
				shipGroupKey.setLocationId(inStorePickupShippingGroup.getLocationId());
				if (isLoggingDebug()) {
					vlogDebug("ship group key for the relatioship : {0} is {1}", pSgCiRel.getId(), shipGroupKey);
				}
				return shipGroupKey;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUShippingProcessHelper.findShipGroupKey method");
		}
		return null;
	}


	/**
	 * returns matched channel hardgood shipping groups in order.
	 *
	 * @param pShippingGroupMapContainer shipping group container
	 * @param pChannelDetailsVo channel detail vo
	 * @return list of shipping groups
	 */
	@SuppressWarnings("unchecked")
	public List<TRUChannelHardgoodShippingGroup> findChannelHardgoodShippingGroups(ShippingGroupMapContainer pShippingGroupMapContainer,
			TRUChannelDetailsVo pChannelDetailsVo) {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUShippingProcessHelper.findChannelHardgoodShippingGroups method");
		}
		if (pShippingGroupMapContainer == null || pChannelDetailsVo == null) {
			return null;
		}
		final TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) pShippingGroupMapContainer;

		final List<TRUChannelHardgoodShippingGroup> channelHardgoodShippingGroups = new ArrayList<TRUChannelHardgoodShippingGroup>();
		final String channelId = pChannelDetailsVo.getChannelId();
		final String channelType = pChannelDetailsVo.getChannelType();

		Map<String, ShippingGroup> shippingGroupMap = shippingGroupMapContainer.getShippingGroupMap();
		if (shippingGroupMap != null && !shippingGroupMap.isEmpty()) {
			final Set<String> shipGroupNames = shippingGroupMap.keySet();
			for (String shipGroupName : shipGroupNames) {
				final ShippingGroup shippingGroup = shippingGroupMapContainer.getShippingGroup(shipGroupName);
				if (shippingGroup instanceof TRUChannelHardgoodShippingGroup) {
					final TRUChannelHardgoodShippingGroup chsg = (TRUChannelHardgoodShippingGroup) shippingGroup;
					if (StringUtils.isNotBlank(channelId) && StringUtils.isNotBlank(channelType) &&
							channelId.equalsIgnoreCase(chsg.getChannelId()) &&
							channelType.equalsIgnoreCase(chsg.getChannelType())) {
						channelHardgoodShippingGroups.add(chsg);
					}
				}
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("End of TRUShippingProcessHelper.findChannelHardgoodShippingGroups method");
		}
		return channelHardgoodShippingGroups;
	}

	/**
	 * returns matched channel in store pickup shipping groups in order.
	 *
	 * @param pOrder order
	 * @param pChannelDetailsVo channel detail vo
	 * @return list of shipping groups
	 */
	@SuppressWarnings("unchecked")
	public List<TRUChannelInStorePickupShippingGroup> findChannelInStorePickUpShippingGroups(Order pOrder,
			TRUChannelDetailsVo pChannelDetailsVo) {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUShippingProcessHelper.findChannelInStorePickUpShippingGroups method");
		}
		if (pOrder == null || pChannelDetailsVo == null) {
			return null;
		}

		final TRUOrderImpl order = (TRUOrderImpl) pOrder;

		final List<TRUChannelInStorePickupShippingGroup> inStorePickupShippingGroups= new ArrayList<TRUChannelInStorePickupShippingGroup>();
		final String channelId = pChannelDetailsVo.getChannelId();
		final String channelType = pChannelDetailsVo.getChannelType();

		final List<ShippingGroup> shippingGroups = order.getShippingGroups();
		if (shippingGroups != null && !shippingGroups.isEmpty()) {
			for (ShippingGroup shippingGroup : shippingGroups) {
				if (shippingGroup instanceof TRUChannelInStorePickupShippingGroup) {
					final TRUChannelInStorePickupShippingGroup chISPUsg = (TRUChannelInStorePickupShippingGroup) shippingGroup;
					if (StringUtils.isNotBlank(channelId) && StringUtils.isNotBlank(channelType)
							&& channelId.equalsIgnoreCase(chISPUsg.getChannelId())
							&& channelType.equalsIgnoreCase(chISPUsg.getChannelType())) {
						inStorePickupShippingGroups.add(chISPUsg);
					}
				}
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("End of TRUShippingProcessHelper.findChannelInStorePickUpShippingGroups method");
		}
		return inStorePickupShippingGroups;
	}

	/**
	 * looks for shipping method found in available shipping methods.
	 *
	 * @param pAvailableShippingMethods availableShippingMethods
	 * @param pShippingMethod shippingMethod
	 * @return boolean
	 */
	public boolean isShippingMethodFound(List<TRUShipMethodVO> pAvailableShippingMethods, String pShippingMethod) {
		if (pAvailableShippingMethods != null && StringUtils.isNotBlank(pShippingMethod)) {
			for (TRUShipMethodVO shipMethodVO : pAvailableShippingMethods) {
				if (shipMethodVO != null && pShippingMethod.equalsIgnoreCase(shipMethodVO.getShipMethodCode())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Update registrant details.
	 *
	 * @param pChannelDetailsVo channel details vo
	 * @return true if updated
	 */
	public boolean updateRegistrantDetails(TRUChannelDetailsVo pChannelDetailsVo) {
		final TRUOrderManager orderManager = (TRUOrderManager) getPurchaseProcessHelper().getOrderManager();
		final TRURegistryServiceProcessor registryServiceProcessor = orderManager.getRegistryServiceProcessor();
		if (pChannelDetailsVo == null) {
			return false;
		}
		return registryServiceProcessor.getRegistryDetails(pChannelDetailsVo);
	}


	/**
	 * returns default address nickname if country is united states.
	 * 
	 * @param pShippingGroupMapContainer
	 *            - ShippingGroupMapContainer obj
	 * @return nickName - String
	 */
	@SuppressWarnings("unchecked")
	public String findDefaultShippingAddress(ShippingGroupMapContainer pShippingGroupMapContainer) {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUPurchaseProcessHelper.findDefaultShippingAddress method");
			vlogDebug("INPUT PARAMS OF findDefaultShippingAddress() pShippingGroupMapContainer: {0}", pShippingGroupMapContainer);
		}
		final TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) pShippingGroupMapContainer;
		final Map<String, ShippingGroup> shippingGroupMapForDisplay = shippingGroupMapContainer.getShippingGroupMapForDisplay();

		final String defaultShippingAddressNickName = shippingGroupMapContainer.getDefaultShippingGroupName();
		if (StringUtils.isNotBlank(defaultShippingAddressNickName)) {
			final ShippingGroup shippingGroup = shippingGroupMapForDisplay.get(defaultShippingAddressNickName);
			if (shippingGroup instanceof TRUHardgoodShippingGroup) {
				final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
				if (hardgoodShippingGroup.getShippingAddress() != null &&
						!hardgoodShippingGroup.isBillingAddress() &&
						TRUConstants.UNITED_STATES.equalsIgnoreCase(hardgoodShippingGroup.getShippingAddress().getCountry())) {
					if (isLoggingDebug()) {
						vlogDebug("End of findDefaultShippingAddress method and returns defaultShippingAddressNickName: {0}",
								defaultShippingAddressNickName);
					}
					return defaultShippingAddressNickName;
				}
			}
		}

		final Set<String> nickNames = shippingGroupMapForDisplay.keySet();
		final Iterator<String> iterator = nickNames.iterator();
		while (iterator.hasNext()) {
			final String nickName = iterator.next();
			if (StringUtils.isNotBlank(nickName)) {
				final ShippingGroup shippingGroup = shippingGroupMapForDisplay.get(nickName);
				if (shippingGroup instanceof TRUHardgoodShippingGroup) {
					final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
					if (hardgoodShippingGroup.getShippingAddress() != null && 
							!hardgoodShippingGroup.isBillingAddress() &&
							TRUConstants.UNITED_STATES.equalsIgnoreCase(hardgoodShippingGroup.getShippingAddress()
									.getCountry()) ) {
						if (isLoggingDebug()) {
							vlogDebug("End of findDefaultShippingAddress method and returns nickName: {0}", nickName);
						}
						return nickName;
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of findDefaultShippingAddress method and returns nickName returns null");
		}
		return null;
	}




	/**
	 * <p>
	 * 	This method will check whether order is having previous default shipping address or not.
	 * </p>
	 * @param pShippingGroupNickname shippingGroupNickname
	 * @param pProfile the profile
	 * @return true/false, the success/failure
	 */
	public boolean isOrderHavingPrevShippingAddress(String pShippingGroupNickname,RepositoryItem pProfile) {
		if(isLoggingDebug()){
			logDebug("Start isOrderHavingPrevShippingAddress()");
		}
		boolean isOrderHavingPrevShippingAddress=false;
		final String prevAddressNickName=getPurchaseProcessHelper().getProfileTools().getPrevShippingAddressNickName(pProfile);
		if(isLoggingDebug()){
			vlogDebug("shippingGroupNickname {0} and prevAddressNickName {1} ",pShippingGroupNickname,prevAddressNickName);
		}
		if(StringUtils.isNotBlank(pShippingGroupNickname) &&
				StringUtils.isNotBlank(prevAddressNickName) &&
				pShippingGroupNickname.equalsIgnoreCase(prevAddressNickName)){
			isOrderHavingPrevShippingAddress= true;
		}
		if(isLoggingDebug()){
			vlogDebug("End isOrderHavingPrevShippingAddress() and isOrderHavingPrevShippingAddress {0}",isOrderHavingPrevShippingAddress);
		}
		return isOrderHavingPrevShippingAddress;
	}






	/**
	 * <p>
	 * 	This method will check if profile's defaultAddress being used by the order and modified from my account  or some where else,
	 * 	Then new defaultAddress will be updated with the order in multi ship flow .
	 * </p>
	 *
	 * @param pProfile the profile
	 * @param pOrder the order
	 * @param pShippingGroupMapContainer the shippingGroupMapContainer
	 * @throws CommerceException the commerceException
	 * @throws ShippingGroupInitializationException the shippingGroupInitializationException
	 * @throws RunProcessException the runProcessException
	 */
	@SuppressWarnings("unchecked")
	public void updateDefaultShippingAddress(RepositoryItem pProfile, Order pOrder,
			ShippingGroupMapContainer pShippingGroupMapContainer) throws CommerceException, ShippingGroupInitializationException,
			RunProcessException {
		if (isLoggingDebug()) {
			vlogDebug("End TRUShippingProcessHelper.updateDefaultShippingAddress() for profile {0} ,order {1} ",pProfile.getRepositoryId(),pOrder.getId());
		}
		final TRUShippingGroupContainerService shippingGrpContainer = (TRUShippingGroupContainerService) pShippingGroupMapContainer;
		final boolean isAnonymousUser = getPurchaseProcessHelper().getProfileTools().isAnonymousUser(pProfile);

		final String prevDefaultAddressNickname = getPurchaseProcessHelper().getProfileTools().getPrevShippingAddressNickName(pProfile);
		final String currentDefaultAddressNickname = shippingGrpContainer.getDefaultShippingGroupName();

		if (isLoggingDebug()) {
			vlogDebug("isAnonymousUser {0},pProfile {1},currentDefaultAddressNickName {2},prevAddressNickName{3}",
					isAnonymousUser, pProfile, currentDefaultAddressNickname, prevDefaultAddressNickname);
		}

		final TRUOrderImpl order = (TRUOrderImpl) pOrder;
		// Get the shippingGroups matched with prevDefaultAddress.
		if (order.isOrderIsInMultiShip()) {
			final List<TRUHardgoodShippingGroup> matchedShippingGroups = getMatchedHardGoodShippingGroups(pOrder,
					prevDefaultAddressNickname, shippingGrpContainer);
			List<CommerceItemRelationship> ciRels = null;
			final TRUHardgoodShippingGroup containerShippingGroup = (TRUHardgoodShippingGroup) pShippingGroupMapContainer
					.getShippingGroup(currentDefaultAddressNickname);

			for (TRUHardgoodShippingGroup hardgoodShippingGroup : matchedShippingGroups) {
				if (hardgoodShippingGroup != null) {
					// String oldShippingMethod = hardgoodShippingGroup.getShippingMethod();
					ciRels = hardgoodShippingGroup.getCommerceItemRelationships();
					if (ciRels != null && !ciRels.isEmpty()) {
						if(containerShippingGroup==null || containerShippingGroup.getShippingAddress() ==null || StringUtils.isBlank(currentDefaultAddressNickname)){
							hardgoodShippingGroup.setNickName(null);
							hardgoodShippingGroup.setShippingAddress(null);
						}else{
							hardgoodShippingGroup.setNickName(currentDefaultAddressNickname);
							OrderTools.copyAddress(containerShippingGroup.getShippingAddress(),
									hardgoodShippingGroup.getShippingAddress());
						}
						for (CommerceItemRelationship ciRel : ciRels) {
							if (ciRel != null && ciRel.getCommerceItem() != null) {
								CommerceItem commerceItem = ciRel.getCommerceItem();
								setShipMethodAndPrice(order, commerceItem, hardgoodShippingGroup,
										hardgoodShippingGroup.getShippingMethod());
							}
						}
						hardgoodShippingGroup.setOrderShippingAddress(true);
						if(containerShippingGroup.isGiftReceipt()) {
							hardgoodShippingGroup.setGiftReceipt(Boolean.TRUE);
							hardgoodShippingGroup.setGiftWrapMessage(containerShippingGroup.getGiftWrapMessage());
						}
					}
				}
			}
		} else {
			try {
				final TRUHardgoodShippingGroup hardgoodShippingGroup = getPurchaseProcessHelper().getFirstHardgoodShippingGroup(order);
				updateDefaultAddress(pShippingGroupMapContainer, currentDefaultAddressNickname, hardgoodShippingGroup);
				setShipMethodAndPrice(pOrder, null, hardgoodShippingGroup, hardgoodShippingGroup.getShippingMethod());
				hardgoodShippingGroup.setOrderShippingAddress(true);
				hardgoodShippingGroup.setNickName(currentDefaultAddressNickname);
			} catch (CommerceException e) {
				if (isLoggingError()) {
					logError(
							"CommerceException occured while updating the default address@ TRUShippingProcessHelper.updateShippingAddressIfChanged()",
							e);
				}
			}
		}

		if (isLoggingDebug()) {
			logDebug("End TRUShippingProcessHelper.updateDefaultShippingAddress()");
		}
	}

	/**
	 * Gets the matched hard good shipping groups.
	 *
	 * @param pOrder the order
	 * @param pNickName the nick name
	 * @return matchedShippingGroups
	 */
	private List<TRUHardgoodShippingGroup> getMatchedHardGoodShippingGroups(Order pOrder, String pNickName) {
		List<TRUHardgoodShippingGroup> matchedShippingGroups = new ArrayList<TRUHardgoodShippingGroup>();
		for (Object sg : pOrder.getShippingGroups()) {
			final ShippingGroup shippingGroup = (ShippingGroup) sg;
			if (shippingGroup instanceof TRUHardgoodShippingGroup) {
				final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
				if (StringUtils.isNotBlank(pNickName) &&
						pNickName.equalsIgnoreCase(hardgoodShippingGroup.getNickName())) {
					matchedShippingGroups.add(hardgoodShippingGroup);
				}
			}
		}
		return matchedShippingGroups;
	}


	/**
	 * <p>
	 * This method iterates all the shipping groups and returns all the HSGs if the sgContainerSize size is one otherwise returns all the  matchedShippingGroups for given
	 * shippingGroupName.
	 * </p>
	 * @param pOrder - Order
	 * @param pPrevDefaultAddressNickname - PrevDefaultAddressNickname
	 * @param pShippingGroupMapContainer - ShippingGroupMapContainer
	 * @return - matchedShippingGroups
	 */
	public List<TRUHardgoodShippingGroup> getMatchedHardGoodShippingGroups(Order pOrder, String pPrevDefaultAddressNickname,
			TRUShippingGroupContainerService pShippingGroupMapContainer) {
		final List<TRUHardgoodShippingGroup> matchedShippingGroups = new ArrayList<TRUHardgoodShippingGroup>();
		final int sgContainerSize = pShippingGroupMapContainer.getShippingGroupMap().size();
		for (Object sg : pOrder.getShippingGroups()) {
			final ShippingGroup shippingGroup = (ShippingGroup) sg;
			if (shippingGroup instanceof TRUHardgoodShippingGroup && !(shippingGroup instanceof TRUChannelHardgoodShippingGroup)) {
				final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
				if (sgContainerSize == TRUCheckoutConstants.INT_ONE || StringUtils.isEmpty(hardgoodShippingGroup.getNickName())) {
					matchedShippingGroups.add(hardgoodShippingGroup);
				} else if (StringUtils.isNotBlank(pPrevDefaultAddressNickname) &&
						pPrevDefaultAddressNickname.equalsIgnoreCase(hardgoodShippingGroup.getNickName())) {
					matchedShippingGroups.add(hardgoodShippingGroup);
				}
			}
		}
		return matchedShippingGroups;
	}

	/**
	 * <p>
	 * This method updated the default address on change shipping address on overlay.
	 * </p>
	 *
	 * @param pShippingGroupMapContainer the shippingGroupMapContainer
	 * @param pCurrentAddressNickName the currentAddressNickName
	 * @param pHardgoodShippingGroup the hardgoodShippingGroup
	 * @throws CommerceException the commerce Exception
	 */
	private void updateDefaultAddress(ShippingGroupMapContainer pShippingGroupMapContainer, String pCurrentAddressNickName,
			TRUHardgoodShippingGroup pHardgoodShippingGroup) throws CommerceException {
		final TRUShippingGroupContainerService containerService = (TRUShippingGroupContainerService) pShippingGroupMapContainer;
		final TRUHardgoodShippingGroup containerShippingGroup = (TRUHardgoodShippingGroup) containerService
				.getShippingGroup(pCurrentAddressNickName);
		if (null == containerShippingGroup) {
			if (isLoggingWarning()) {
				logWarning("No TRUHardgoodShippingGroup found for " + pCurrentAddressNickName);
			}
			return;
		}
			pHardgoodShippingGroup.setNickName(pCurrentAddressNickName);
		containerService.setDefaultShippingGroupName(pCurrentAddressNickName);
		containerService.setSelectedAddressNickName(pCurrentAddressNickName);
		OrderTools.copyAddress(containerShippingGroup.getShippingAddress(), pHardgoodShippingGroup.getShippingAddress());
		containerShippingGroup.setOrderShippingAddress(true);
	}


	/**
	 * <p>
	 * 
	 * </p>.
	 *
	 * @param pOrder the order
	 * @param pSelectedShippingGroupRel the selected shipping group rel
	 * @return the string
	 * @throws CommerceItemNotFoundException the commerce item not found exception
	 * @throws InvalidParameterException the invalid parameter exception
	 *//*
	private void updateShipMethodAndPrice1(TRUOrderImpl pTRUOrder,
			TRUHardgoodShippingGroup pHardgoodShippingGroup,
			String pOldShippingMethod,
			CommerceItemRelationship pCiRel) throws CommerceException {
		if (pCiRel != null && pCiRel.getCommerceItem() != null) {
			//Address address= pHardgoodShippingGroup.getShippingAddress();
			CommerceItem commerceItem = pCiRel.getCommerceItem();
			//String frieghtClass = getShippingManager().getFreightClassFromCI(commerceItem);
			//List<TRUShipMethodVO> shipMethodVOs=getShippingManager().getEligibleShipMethodsByItem(commerceItem,address, pTRUOrder);
			setShipMethodAndPrice((Order)pTRUOrder,commerceItem,pHardgoodShippingGroup);
		}
	}*/

	/**
	 * This method will determines, if the commerce item is eligible for ISPU or not.
	 * @param pOrder - Order
	 * @param pSelectedShippingGroupRel - pSelectedShippingGroupRel
	 * @return eligibleForISPU - eligibleForISPU
	 * @throws CommerceItemNotFoundException - CommerceItemNotFoundException
	 * @throws InvalidParameterException - InvalidParameterException
	 */
	public boolean isProductEligibleForISPU(Order pOrder, TRUShippingGroupCommerceItemRelationship pSelectedShippingGroupRel)
			throws CommerceItemNotFoundException, InvalidParameterException {
		if (isLoggingDebug()) {
			vlogDebug("Entering - TRUShippingProcessHelper :: isProductEligibleForISPU method to determine if the commerce Item is eligible for ISPU or not.");
		}
		String s2s = null;
		if (pSelectedShippingGroupRel != null) {
			final RepositoryItem skuItem = (RepositoryItem) pOrder
					.getCommerceItem(pSelectedShippingGroupRel.getCommerceItem().getId()).getAuxiliaryData().getCatalogRef();
			Boolean s2sValue=(Boolean) skuItem.getPropertyValue(getPurchaseProcessHelper().getPropertyManager().getShipToStoreEligible());

			//check for ShipToStoreEligible flag from SKU
			if(s2sValue!=null && s2sValue.booleanValue())
			{
				s2s=TRUCommerceConstants.YES;
			}else{
				s2s=TRUCommerceConstants.NO;
			}

			//check for inStorePickUp flag from SKU
			String ispu=(String) skuItem
					.getPropertyValue(getPurchaseProcessHelper().getPropertyManager().getItemInStorePickUp());
			if(TRUCommerceConstants.YES.equalsIgnoreCase(ispu) || TRUCommerceConstants.YES_STRING.equalsIgnoreCase(ispu) || TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(ispu)){
				ispu=TRUCommerceConstants.YES;
			}else{
				ispu=TRUCommerceConstants.NO;
			}

			if(s2s.equals(TRUCommerceConstants.YES) ||ispu.equals(TRUCommerceConstants.YES)){
				return true;
			}
			if (isLoggingDebug()) {
				vlogDebug("Exiting - TRUShippingProcessHelper :: isProductEligibleForISPU method to determine if the commerce Item is eligible for ISPU or not."
						+ pSelectedShippingGroupRel.getCommerceItem().getId());
			}
		}
		return false;
	}

	/**
	 * This method will determines, if the commerce item is eligible for ISPU or not by commerce item shipping info object.
	 * @param pCisi
	 * 		commeceItemShippingInfo
	 * @return eligibleForISPU
	 */
	public boolean isProductEligibleForISPUByCISI(CommerceItemShippingInfo pCisi){
		if (isLoggingDebug()) {
			vlogDebug("Entering - TRUShippingProcessHelper :: isProductEligibleForISPUByCISI method to determine if the commerce Item is eligible for ISPU or not.");
		}

		Boolean s2sValue = null;
		String ispu = null;

		final RepositoryItem skuItem = (RepositoryItem) pCisi.getCommerceItem().getAuxiliaryData().getCatalogRef();
		if(skuItem != null){
			ispu = (String) skuItem
					.getPropertyValue(getPurchaseProcessHelper().getPropertyManager().getItemInStorePickUp());
			s2sValue = (Boolean) skuItem.getPropertyValue(getPurchaseProcessHelper().getPropertyManager().getShipToStoreEligible());
		}
		String s2s = null;
		//check for ShipToStoreEligible flag from SKU
		if(s2sValue!=null && s2sValue.booleanValue())
		{
			s2s=TRUCommerceConstants.YES;
		}else{
			s2s=TRUCommerceConstants.NO;
		}
		if(TRUCommerceConstants.YES.equalsIgnoreCase(ispu) || TRUCommerceConstants.YES_STRING.equalsIgnoreCase(ispu) || TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(ispu)){
			ispu=TRUCommerceConstants.YES;
		}else{
			ispu=TRUCommerceConstants.NO;
		}
		if(s2s.equals(TRUCommerceConstants.NO) && ispu.equals(TRUCommerceConstants.NO) ){
			if (isLoggingDebug()) {
				vlogDebug("ISPU Eligible - false - Exiting - TRUShippingProcessHelper :: isProductEligibleForISPUByCISI method to determine if the commerce Item is eligible for ISPU or not.");
			}
			return false;
		}else{
			if (isLoggingDebug()) {
				vlogDebug("ISPU Eligible - TRUE - Exiting - TRUShippingProcessHelper :: isProductEligibleForISPUByCISI method to determine if the commerce Item is eligible for ISPU or not.");
			}
			return true;

		}

	}

	/**
	 * <p>
	 * This method will check whether the SKU id preSellItem or not based on preSellQuantity and streetDate.
	 * </p>
	 *
	 * @param pStreetDate - StreetDate
	 * @return isStreetDateIsFutureDate
	 */
	private boolean isStreetDateIsFutureDate(Date pStreetDate){
		if(isLoggingDebug()){
			vlogDebug("Starting isStreetDateIsFutureDate() for streetDate : {0}", pStreetDate);
		}
		boolean isStreetDateIsFutureDate=true;
		if(null==pStreetDate){
			isStreetDateIsFutureDate= false;
		}else {
			final Date today=new Date();
			if(pStreetDate.compareTo(today)==TRUConstants.INT_MINUS_ONE){
				isStreetDateIsFutureDate= false;
			}
		}
		if(isLoggingDebug()){
			vlogDebug("Ending isStreetDateIsFutureDate() isStreetDateIsFutureDate {0}", isStreetDateIsFutureDate);
		}
		return isStreetDateIsFutureDate;
	}


	/**
	 * <p>
	 * This method is used to calculate EDD working dates.
	 * </p>
	 * @param pMin -min value
	 * @param pMax -max value
	 * @return map -return the map object
	 */
	private Map<String,Object> calculateEDDWorkingDates(int pMin,int pMax){
		if(isLoggingDebug()){
			vlogDebug("Start  getWorkingDatesInfo() Min ={0} \t Max ={1}", pMin,pMax);
		}
		int requiredWorkingDays=pMax-pMin+TRUConstants.ONE;
		int eddStartCntr=TRUConstants.ZERO;
		int eddEndCntr=TRUConstants.ZERO;
		String eddMinWorkingDate=TRUConstants.EMPTY;
		String eddMaxWorkingDate=TRUConstants.EMPTY;
		String date=TRUConstants.EMPTY;

		Map<String,Object> resultMap=new LinkedHashMap<String, Object>();

		List<String> eddWorkingDateList=new ArrayList<String>(requiredWorkingDays);
		Set<Date> shipHoliDayDates= getPurchaseProcessHelper().getShippingManager().getActiveHolidayDates();
		Set<String> weekendDays =getPurchaseProcessHelper().getShippingManager().getShipHoliDays();
		if(isLoggingDebug()){
			vlogDebug("No Of WorkingDays Required?: {0}",requiredWorkingDays);
			vlogDebug("Holiday Dates List : {0}", shipHoliDayDates);
			vlogDebug("Configured Weekends: {0}", weekendDays);
			vlogDebug("Min : {0} and  Max {1}",pMin,pMax);
		}
		weekendDays=null;
		int i=TRUConstants.ZERO;
		do{
			date=getNextWorkingDate(pMin+i ,shipHoliDayDates);
			if(StringUtils.isNotBlank(date)){
				eddWorkingDateList.add(date);
			}
			i++;
		}while(eddWorkingDateList.size()!=requiredWorkingDays);
		if(isLoggingDebug()){
			vlogDebug("Final Working Dates List after exclusing holidays and weekends :{0}",eddWorkingDateList);
		}
		if(eddWorkingDateList!=null && eddWorkingDateList.size() == requiredWorkingDays){
			eddMinWorkingDate=eddWorkingDateList.get(TRUConstants.ZERO);
			eddMaxWorkingDate=eddWorkingDateList.get(eddWorkingDateList.size()-TRUConstants.ONE);
		}
		SimpleDateFormat format = new SimpleDateFormat(getPurchaseProcessHelper().getConfiguration().getPreSellDateFormat());
		try {
			String s_eddMinDate=getDateAfterXDays(pMin);
			if(StringUtils.isNotBlank(eddMinWorkingDate) && StringUtils.isNotBlank(s_eddMinDate)){
				DateTime eddMinWorkingDateTime = new DateTime(format.parse(eddMinWorkingDate));
				DateTime eddMinDate=new DateTime(format.parse(s_eddMinDate));
				//Difference btn actual EDD and working day
				eddStartCntr=  Days.daysBetween(eddMinDate,eddMinWorkingDateTime).getDays();
			}

			String s_eddMaxDate=getDateAfterXDays(pMax);
			if(StringUtils.isNotBlank(eddMaxWorkingDate) && StringUtils.isNotBlank(s_eddMaxDate)){
				DateTime eddMaxWorkingDateTime = new DateTime(format.parse(eddMaxWorkingDate));
				DateTime eddMaxDate=new DateTime(format.parse(s_eddMaxDate));
				//Difference btn actual EDD and working day
				eddEndCntr=  Days.daysBetween(eddMaxDate, eddMaxWorkingDateTime).getDays();
			}
		}catch(ParseException e){
			if(isLoggingError()){
				logError("ParseException while calulating while exclusind holidays and weekends",e);
			}
		}
		resultMap.put(EDD_START_COUNTER, eddStartCntr);
		resultMap.put(EDD_END_COUNTER, eddEndCntr);
		resultMap.put(EDD_WORKING_DATES_SET,eddWorkingDateList);
		if(isLoggingDebug()){
			logDebug("Result Map \t:"+resultMap);
		}
		return resultMap;
	}

	/**
	 * This method will construct a new date relative to the current date and check whether it is eligible for shipping or not.
	 *
	 * @param pDays -days
	 * @param pHoliDaysSet the holi days set
	 * @return getRelativeDate- relative days
	 */
	private String getNextWorkingDate(int pDays,Set<Date> pHoliDaysSet){
		if(!isWeekendDate(pDays) && !isShipHoliDayDate(pDays,pHoliDaysSet)){
			return getDateAfterXDays(pDays);
		}
		return TRUConstants.EMPTY;
	}

	/**
	 * <p>
	 * This method will check given date is weekend or not.
	 * </p>
	 * @param pDays -days
	 * @return isWeekendDate -the isWeekendDate
	 */
	private boolean isWeekendDate(int pDays){
		Calendar now=Calendar.getInstance();
		now.add(Calendar.DATE, pDays);
		boolean isWeekendDate=false;
		Set<String> weekendDays =getPurchaseProcessHelper().getShippingManager().getShipHoliDays();
		if(weekendDays!=null && weekendDays.contains(String.valueOf(now.get(Calendar.DAY_OF_WEEK)))){
			isWeekendDate= true;
		}
		if(isLoggingDebug()){
			vlogDebug("Is {0} WeekendDate ? {1}",now.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US),isWeekendDate);
		}
		return isWeekendDate;
	}


	/**
	 * Checks if is ship holi day.
	 *
	 * @param pDays the days
	 * @param pHoliDatesSet the holi dates set
	 * @return true, if is ship holi day
	 */
	private boolean isShipHoliDayDate(int pDays,Set<Date> pHoliDatesSet){
		String relativeDate=getDateAfterXDays(pDays);
		String formattedHolidayDate=TRUConstants.EMPTY;
		boolean isShipHoliDayFound=false;
		SimpleDateFormat dateFormat=new SimpleDateFormat(getPurchaseProcessHelper().getConfiguration().getPreSellDateFormat());
		for(Date holyDayDate:pHoliDatesSet){
			formattedHolidayDate=dateFormat.format(holyDayDate);
			if(null!=relativeDate && relativeDate.equals(formattedHolidayDate)){
				isShipHoliDayFound= true;
				if(isLoggingDebug()){
					vlogDebug("Checking the date {0}  with {1} and isShipHoliDayFound {2}",relativeDate,formattedHolidayDate,isShipHoliDayFound);
				}
				break;
			}

		}
		return isShipHoliDayFound;
	}


	/**
	 * returns date in fomat DD/MM/YY *.
	 * 
	 * @param pDays
	 *            the days
	 * @return the relative date
	 */
	private String getDateAfterXDays(int pDays) {
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DATE, pDays);
		SimpleDateFormat dateFormat=new SimpleDateFormat(getPurchaseProcessHelper().getConfiguration().getPreSellDateFormat());
		String s_Date=dateFormat.format(now.getTime());
		return s_Date;
	}



	/**
	 * @return the mPurchaseProcessHelper
	 */
	public TRUPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}

	/**
	 * @param pPurchaseProcessHelper the mPurchaseProcessHelper to set
	 */
	public void setPurchaseProcessHelper(TRUPurchaseProcessHelper pPurchaseProcessHelper) {
		mPurchaseProcessHelper = pPurchaseProcessHelper;
	}





	/**
	 * Update shipping method and price.
	 *
	 * @param pOrder the order
	 * @param pShippingGroup the shipping group
	 * @param pContainerService1  ShippingGroupContainerService
	 */
	public void updateShippingMethodAndPrice(Order pOrder, ShippingGroup pShippingGroup,ShippingGroupContainerService pContainerService1) {
		if (isLoggingDebug()) {
			vlogDebug("Updating updateShippingMethodAndPrice() for pOrder : {0} pShippingGroup : {1}", pOrder.getId(), pShippingGroup.getId());
		}
		final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) pShippingGroup;
		TRUOrderImpl orderImpl= (TRUOrderImpl)pOrder;
		if(orderImpl.isOrderIsInMultiShip()){
			final TRUShippingGroupManager sgm = (TRUShippingGroupManager)getPurchaseProcessHelper().getShippingGroupManager();
			final List<CommerceItemRelationship> shipToHomeRelationShips = sgm.getOnlyShipToHomeItemRelationShips(pShippingGroup);
			if(shipToHomeRelationShips!=null && !shipToHomeRelationShips.isEmpty()){
				for (CommerceItemRelationship ciRel : shipToHomeRelationShips) {
					if (ciRel != null && ciRel.getCommerceItem() != null) {
						CommerceItem commerceItem = ciRel.getCommerceItem();
						setShipMethodAndPrice(orderImpl, commerceItem, hardgoodShippingGroup,
								hardgoodShippingGroup.getShippingMethod());
					}
				}
			}
		}else{
			setShipMethodAndPrice(orderImpl,null, hardgoodShippingGroup,hardgoodShippingGroup.getShippingMethod());
		}
		if (isLoggingDebug()) {
			vlogDebug("Updating updateShippingMethodAndPrice() for pOrder : {0} pShippingGroup : {1}", pOrder.getId(), pShippingGroup.getId());
		}
	}




	/**
	 * Sets the default shipping address for logged In user.
	 *
	 * @param pOrder the order
	 * @param pContainerService the container service
	 * @param pHardgoodShippingGroup the hardgood shipping group
	 */
	@SuppressWarnings("unchecked")
	public void setDefaultShippingAddress(final Order pOrder, TRUShippingGroupContainerService pContainerService,
			TRUHardgoodShippingGroup pHardgoodShippingGroup) {
		if (isLoggingDebug()) {
			vlogDebug("setDefaultShippingAddress() for pOrder {0}", pOrder.getId());
		}
		try {
			TRUHardgoodShippingGroup lHardgoodShippingGroup = pHardgoodShippingGroup;
			RepositoryItem profile = getPurchaseProcessHelper().getProfileTools().getProfileItem(pOrder.getProfileId());
			final TRUProfileTools profileTools = getPurchaseProcessHelper().getProfileTools();
			final boolean isAnonymousUser = profileTools.isAnonymousUser(profile);
			final String defaultAddressNickName = profileTools.getDefaultShippingAddressNickname(profile);
			if (!isAnonymousUser && StringUtils.isNotBlank(defaultAddressNickName)) {
				/*
				 * The below code will be useful updating the defaultShippingAddress in container when logged in user navigating from cart to checkout.
				 */
				if (pHardgoodShippingGroup == null) {
					List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
					for (ShippingGroup shippingGroup : shippingGroups) {
						if (shippingGroup instanceof TRUHardgoodShippingGroup && !(shippingGroup instanceof TRUChannelHardgoodShippingGroup)) {
							lHardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
							if (StringUtils.isNotBlank(lHardgoodShippingGroup.getNickName())
									&& defaultAddressNickName.equals(lHardgoodShippingGroup.getNickName())) {
								pContainerService.setDefaultShippingGroupName(defaultAddressNickName);
								pContainerService.addShippingGroup(defaultAddressNickName, lHardgoodShippingGroup);
								return;
							}else{
								lHardgoodShippingGroup=null;
							}
						}
					}
				}
				if(lHardgoodShippingGroup==null){
					lHardgoodShippingGroup = getPurchaseProcessHelper().getFirstHardgoodShippingGroup(pOrder);
				}
				if(!((TRUOrderImpl)pOrder).isOrderContainsMixedItems()){
					final RepositoryItem defaultShippingAddress = profileTools.getDefaultShippingAddress(profile);
					if (null != defaultShippingAddress) {
						// Copy the default into hardgood shipping
						lHardgoodShippingGroup.setNickName(defaultAddressNickName);
						OrderTools.copyAddress(defaultShippingAddress, lHardgoodShippingGroup.getShippingAddress());
						pContainerService.setDefaultShippingGroupName(defaultAddressNickName);
						pContainerService.addShippingGroup(defaultAddressNickName, lHardgoodShippingGroup);
					}
				}

			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError("RepositoryException while setDefaultShippingGroupOnContainer for the pOrder {0}", pOrder);
			}
		} catch (CommerceException e) {
			if (isLoggingError()) {
				vlogError("CommerceException while setDefaultShippingGroupOnContainer for the pOrder {0}", pOrder);
			}
		}

	}


	/**
	 * Sets the default shipping address from the profile and will be invoked if any shipping address removed.
	 *
	 * @param pOrder the order
	 * @param pHardgoodShippingGroup the hardgood shipping group
	 */
	private void setDefaultShippingAddress(final Order pOrder, TRUHardgoodShippingGroup pHardgoodShippingGroup) {
		if (isLoggingDebug()) {
			vlogDebug("setDefaultShippingAddress() for pOrder {0}", pOrder.getId());
		}
		final TRUProfileTools profileTools = getPurchaseProcessHelper().getProfileTools();
		try {
			RepositoryItem profile = getPurchaseProcessHelper().getProfileTools().getProfileItem(pOrder.getProfileId());
			final boolean isAnonymousUser = profileTools.isAnonymousUser(profile);
			if (!isAnonymousUser) {
				final String defaultAddressNickName = profileTools.getDefaultShippingAddressNickname(profile);
				final RepositoryItem defaultShippingAddress = profileTools.getDefaultShippingAddress(profile);
				if (null != defaultShippingAddress && !(pHardgoodShippingGroup instanceof TRUChannelHardgoodShippingGroup)) {
					// Copy the default into hardgood shipping
					pHardgoodShippingGroup.setNickName(defaultAddressNickName);
					OrderTools.copyAddress(defaultShippingAddress, pHardgoodShippingGroup.getShippingAddress());
				}
			}
		} catch (RepositoryException  | CommerceException exp) {
			if (isLoggingError()) {
				vlogError("RepositoryException while setDefaultShippingAddress for the pOrder {0}", pOrder);
			}
		}

	}


	/**
	 * <p>
	 * This method will set the default ship method and price based on shipping address.
	 * </p>
	 *
	 * @param pOrder - Order
	 * @param pHardgoodShippingGroup - hardgoodShippingGroup
	 * @param pAddress - Address
	 * @param pOldShipMethod OldShipMethod
	 */
	public void setDefaultShipMethodAndPrice(Order pOrder, TRUHardgoodShippingGroup pHardgoodShippingGroup, Address pAddress,
			String pOldShipMethod) {
		final String region = getPurchaseProcessHelper().getShippingManager().getShippingRegion(pAddress);
		List<TRUShipMethodVO> shipMethodVOs = new ArrayList<TRUShipMethodVO>();
		//Logic for retrieving the ship methods based on order items
		shipMethodVOs = getPurchaseProcessHelper().getShippingManager().getShippingMethods(pOrder,region,false);
		if (null != shipMethodVOs && shipMethodVOs.isEmpty()) {
			pHardgoodShippingGroup.setShippingMethod(TRUConstants.EMPTY);
			pHardgoodShippingGroup.setShippingPrice(TRUConstants.DOUBLE_ZERO);
			if (isLoggingInfo()) {
				vlogInfo("No ship method found for the one of the item in order #{0}", pOrder.getId());
			}
			return;
		}
		if (!getPurchaseProcessHelper().getShippingManager().isPreferredShippingExistis(shipMethodVOs, pOldShipMethod)) {
			final TRUShipMethodVO shipMethodVO = shipMethodVOs.get(0);
			// If the preferred ship method is not found ,apply cheap shipping method and its corresponding price
			if (isLoggingDebug()) {
				vlogDebug("applying least shipping method : {0} and its price is {1} for the order {2}",
						shipMethodVO.getShipMethodCode(), shipMethodVO.getShippingPrice(), pOrder.getId());
			}
			pHardgoodShippingGroup.setShippingMethod(shipMethodVO.getShipMethodCode());
			pHardgoodShippingGroup.setShippingPrice(shipMethodVO.getShippingPrice());
		} else {
			// If the shipping method is same ,Update the shipping price with respect to region.
			for (TRUShipMethodVO methodVO : shipMethodVOs) {
				if (methodVO.getShipMethodCode().equalsIgnoreCase(pHardgoodShippingGroup.getShippingMethod())) {
					pHardgoodShippingGroup.setShippingPrice(methodVO.getShippingPrice());
					break;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End setShipMethodAndPrice() for Order Id {0}", pOrder.getId());
		}
	}


	/**
	 * This method will update all the shipping method and it's price with respective destination address after merge happens.
	 *
	 * @param pOrder the order
	 * @param pContainerService the container service
	 */
	public void updateShippingMethodAndPrice(Order pOrder,ShippingGroupMapContainer pContainerService) {
		if (isLoggingDebug()) {
			vlogDebug("Start TRUPurchaseProcessHelper.updateShippingMethodAndPrice for pOrder : {0} ", pOrder.getId());
		}
		TRUOrderImpl orderImpl = (TRUOrderImpl) pOrder;
		List<TRUHardgoodShippingGroup> shippingGroups = orderImpl.getAllHardgoodShippingGroups();
		if (orderImpl.isOrderIsInMultiShip() || orderImpl.isOrderMandateForMultiShipping()) {
			for (TRUHardgoodShippingGroup hardgoodShippingGroup : shippingGroups) {
				final List<CommerceItemRelationship> ciRels = new ArrayList<CommerceItemRelationship>(
						hardgoodShippingGroup.getCommerceItemRelationships());
				if (ciRels != null && !ciRels.isEmpty()) {
					for (CommerceItemRelationship ciRel : ciRels) {
						if (ciRel.getCommerceItem() instanceof TRUGiftWrapCommerceItem
								|| ciRel.getCommerceItem() instanceof TRUDonationCommerceItem) {
							continue;
						}
						if(null==orderImpl.getRegistryItems() || orderImpl.getRegistryItems().isEmpty()){
							addDefaultAddressToShippingGroup(pOrder, hardgoodShippingGroup,pContainerService);
						}
						setShipMethodAndPrice(pOrder, ciRel.getCommerceItem(), hardgoodShippingGroup,
								hardgoodShippingGroup.getShippingMethod());
					}
				}

			}
		} else {
			for (TRUHardgoodShippingGroup hardgoodShippingGroup : shippingGroups) {
				if(StringUtils.isBlank(hardgoodShippingGroup.getNickName())){
					addDefaultAddressToShippingGroup(pOrder, hardgoodShippingGroup,pContainerService);
				}
				setShipMethodAndPrice(pOrder, null, hardgoodShippingGroup, hardgoodShippingGroup.getShippingMethod());
			}

		}

		if (isLoggingDebug()) {
			vlogDebug("End TRUPurchaseProcessHelper.updateShippingMethodAndPrice for pOrder : {0} ", pOrder.getId());
		}
	}


	/**
	 * Adds the default shipping address from the profile after cart merge done.
	 *
	 * @param pOrder the order
	 * @param pHardgoodShippingGroup the hardgood shipping group
	 * @param pContainerService the container service
	 */
	private void addDefaultAddressToShippingGroup(Order pOrder, TRUHardgoodShippingGroup pHardgoodShippingGroup,ShippingGroupMapContainer pContainerService) {
		if(isLoggingDebug()){
			vlogDebug("Start TRUPurchaseProcessHelper.addDefaultAddressToShippingGroup for pOrder : {0} pShippingGroup {1}", pOrder.getId(),pHardgoodShippingGroup.getId());
		}
		Address address=pHardgoodShippingGroup.getShippingAddress();
		String nickName=pHardgoodShippingGroup.getNickName();
		String contanierDefaultnickName = null;
		if(pContainerService!=null  && (null != pContainerService.getShippingGroupMap()) && !(pContainerService.getShippingGroupMap().isEmpty()) ){
			contanierDefaultnickName = pContainerService.getDefaultShippingGroupName();
		}
		
		//Start : This logic will handle old nickName format scenario changes [ "-" to " " cases].
		if(StringUtils.isNotBlank(nickName) && nickName.contains(TRUConstants.IFAN)){
			nickName=nickName.replace(TRUConstants.IFAN, TRUConstants.WHITE_SPACE);
			pHardgoodShippingGroup.setNickName(nickName);
			return;

		}

		//End : This logic will handle old nickName format scenario changes [ "-" to " " cases].
		if(StringUtils.isBlank(nickName) ||StringUtils.isBlank(address.getFirstName()) && StringUtils.isBlank(address.getLastName())){
			TRUProfileTools profileTools=getPurchaseProcessHelper().getProfileTools();
			try {
				RepositoryItem profileItem=profileTools.getProfileForOrder(pOrder);
				//For guest & logged in users :If container having default nickName ,Get the address and nickName from container and copy to order's shippingGroup
				if(pContainerService !=null && StringUtils.isNotBlank(contanierDefaultnickName)){
					TRUHardgoodShippingGroup containerShiGrp = (TRUHardgoodShippingGroup) pContainerService.getShippingGroup(contanierDefaultnickName);
					if(null!=containerShiGrp && !containerShiGrp.isBillingAddress()){
						OrderTools.copyAddress(containerShiGrp.getShippingAddress(), pHardgoodShippingGroup.getShippingAddress());
						pHardgoodShippingGroup.setShippingMethod(TRUConstants.EMPTY);
						pHardgoodShippingGroup.setShippingPrice(TRUConstants.ZERO);
						pHardgoodShippingGroup.setNickName(contanierDefaultnickName);
					}
				//If container's default nickName is null ,Get the default address and nickName from profile and copy to order's shippingGroup
				}else if(!profileTools.isAnonymousUser(profileItem)){	
					String defaultNickName=profileTools.getDefaultShippingAddressNickname(profileTools.getProfileForOrder(pOrder));
					if(StringUtils.isNotBlank(defaultNickName)){
						RepositoryItem defaultShipAddress=profileTools.getDefaultShippingAddress(profileItem);
						boolean isBillingAddress = false;
						if(null != defaultShipAddress.getPropertyValue(getPurchaseProcessHelper().getPropertyManager().getIsBillingAddressPropertyName())){
							isBillingAddress = (boolean) defaultShipAddress.getPropertyValue(getPurchaseProcessHelper().getPropertyManager().getIsBillingAddressPropertyName());
						}
						if(!isBillingAddress){
							OrderTools.copyAddress(defaultShipAddress, pHardgoodShippingGroup.getShippingAddress());
							pHardgoodShippingGroup.setShippingMethod(TRUConstants.EMPTY);
							pHardgoodShippingGroup.setShippingPrice(TRUConstants.ZERO);
							pHardgoodShippingGroup.setNickName(defaultNickName);
							if(isLoggingInfo()){
								vlogInfo("Updating defaultShipping address for the shippingGroup :{0} in pOrder :{1} and profile {2}", pHardgoodShippingGroup.getId(),pOrder.getId(),profileItem.getRepositoryId());
							}
						}
					}
				}							
			} catch (RepositoryException e) {
				if(isLoggingError()){
					vlogError("RepositoryException : Unable to load profile from the order after cart merge for the order {0}", pOrder.getId());
				}
			}catch (CommerceException e) {
				if(isLoggingError()){
					vlogError("CommerceException : Unable copy default shipping address to the empty shipping group after cart merge for the order {0}", pOrder.getId());
				}
			}finally{
				if(isLoggingDebug()){
					vlogDebug("End TRUPurchaseProcessHelper.addDefaultAddressToShippingGroup for pOrder : {0} pShippingGroup {1}", pOrder.getId(),pHardgoodShippingGroup.getId());
				}
			}
		}
	}


	/**
	 * Removes the shipping address from the order if the removed registry's address being used by other items.
	 *
	 * @param pOrder the order
	 * @param pRemoveShippingAddressNickName the remove shipping address nick name
	 * @param pShippingGroupMapContainer the shipping group map container
	 * @throws CommerceException the commerce exception
	 * @throws RepositoryException the repository exception
	 */
	@SuppressWarnings("unchecked")
	public void removeShippingAddress(Order pOrder,String pRemoveShippingAddressNickName,
			ShippingGroupMapContainer pShippingGroupMapContainer) throws CommerceException, RepositoryException {
		if(isLoggingDebug()){
			vlogDebug("Start removeShippingAddress()  with nickName {0} in the order {1}",pRemoveShippingAddressNickName, pOrder.getId());
		}

		final TRUOrderImpl order = (TRUOrderImpl) pOrder;
		final TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) pShippingGroupMapContainer;
		final TRUHardgoodShippingGroup hgsg = (TRUHardgoodShippingGroup) shippingGroupMapContainer
				.getShippingGroup(pRemoveShippingAddressNickName);
		List<String> machedShipGrpnickNames=new ArrayList<String>();
		machedShipGrpnickNames.add(pRemoveShippingAddressNickName);
		if(hgsg != null) {
			if (pRemoveShippingAddressNickName.equalsIgnoreCase(shippingGroupMapContainer.getDefaultShippingGroupName())) {
				shippingGroupMapContainer.setDefaultShippingGroupName(null);
			}
			//Start : Demo fix for removing all the registry addresses from the container for the deleted registry/wishlist item.
			if(pRemoveShippingAddressNickName.contains(TRUConstants.UNDER_SCORE)){
				String []items = pRemoveShippingAddressNickName.split(TRUConstants.UNDER_SCORE);
				if(items != null && items.length>=TRUCheckoutConstants.INT_ONE){
					String channelId = items[0].trim();
					Set<String> nickNamesSet=pShippingGroupMapContainer.getShippingGroupNames();
					for(String nickName : nickNamesSet){
						if(StringUtils.isNotBlank(nickName) && nickName.startsWith(channelId)){
							machedShipGrpnickNames.add(nickName);
						}
					}
				}
			}
			//End : Demo fix for removing all the registry addresses from the container for the deleted registry/wishlist item.
		}
		final String defaultNickName = findDefaultShippingAddress(shippingGroupMapContainer);
		final TRUHardgoodShippingGroup containerShippingGroup = (TRUHardgoodShippingGroup) shippingGroupMapContainer
				.getShippingGroup(defaultNickName);
		// Remove address from order
		boolean isOrderHasMultiItems=order.isOrderContainsMixedItems();
		if (order.isOrderEligibleForMultiShipping() ) {
			List<TRUHardgoodShippingGroup> totalMatchedShippingGroups = new ArrayList<TRUHardgoodShippingGroup>();
			List<TRUHardgoodShippingGroup> matchedShippingGroups = new ArrayList<TRUHardgoodShippingGroup>();
			for(String nickName : machedShipGrpnickNames){
				matchedShippingGroups = getMatchedHardGoodShippingGroups(pOrder,
						nickName);
				if(!matchedShippingGroups.isEmpty()){
					totalMatchedShippingGroups.addAll(matchedShippingGroups);
				}
				//Remove from the container
				shippingGroupMapContainer.removeShippingGroup(nickName);
			}
			for (TRUHardgoodShippingGroup matchedShipGroup : totalMatchedShippingGroups) {
				if (matchedShipGroup != null) {
					final String oldShippingMethod = matchedShipGroup.getShippingMethod();
					final double oldShippingPrice = matchedShipGroup.getShippingPrice();
					final List<CommerceItemRelationship> ciRels = new ArrayList<CommerceItemRelationship>(
							matchedShipGroup.getCommerceItemRelationships());
					if (ciRels != null && !ciRels.isEmpty()) {
						for (CommerceItemRelationship ciRel : ciRels) {
							if(isOrderHasMultiItems){
								matchedShipGroup.setShippingMethod(null);
								matchedShipGroup.setShippingPrice(TRUConstants.ZERO);
								OrderTools.copyAddress(new ContactInfo(), matchedShipGroup.getShippingAddress());
							}else{
								if(containerShippingGroup==null){
									OrderTools.copyAddress(new ContactInfo(), matchedShipGroup.getShippingAddress());
								}else{
									OrderTools.copyAddress(containerShippingGroup.getShippingAddress(), matchedShipGroup.getShippingAddress());
								}

							}
							updateQtyToSG(pOrder, order, matchedShipGroup, oldShippingMethod, oldShippingPrice, ciRel);
						}
					}
				}
			}
		} else {
			for(String nickName : machedShipGrpnickNames){
				shippingGroupMapContainer.removeShippingGroup(nickName);
			}

			final TRUHardgoodShippingGroup hardgoodShippingGroup = getPurchaseProcessHelper().getFirstHardgoodShippingGroup(order);
			String shipNickName=hardgoodShippingGroup.getNickName();
			if(StringUtils.isNotBlank(shipNickName) && shipNickName.equals(pRemoveShippingAddressNickName) ){
				//Get defaultNickName
				if(StringUtils.isNotBlank(defaultNickName)){
					hardgoodShippingGroup.setNickName(defaultNickName);
					OrderTools.copyAddress(containerShippingGroup.getShippingAddress(), hardgoodShippingGroup.getShippingAddress());
				}else{
					hardgoodShippingGroup.setNickName(null);
					hardgoodShippingGroup.setShippingMethod(null);
					hardgoodShippingGroup.setShippingPrice(TRUConstants.ZERO);
					OrderTools.copyAddress(new ContactInfo(), hardgoodShippingGroup.getShippingAddress());
				}
			}
			setShipMethodAndPrice(order,null, hardgoodShippingGroup,hardgoodShippingGroup.getShippingMethod());
		}
		getPurchaseProcessHelper().getShippingGroupManager().removeEmptyShippingGroups(order);
		if(isLoggingDebug()){
			vlogDebug("End removeShippingAddress()  with nickName {0} in the order {1}",pRemoveShippingAddressNickName, pOrder.getId());
		}
	}
	/**
	 * fetch the default Ship Method
	 * @param pOrder the order
	 * @param pValidShipMethods ValidShipMethods
	 * @return defaultShipMethod
	 */
	public String getDefaultShipMethod(Order pOrder,List<TRUShipMethodVO> pValidShipMethods) {
		String defaultShipMethod = TRUConstants.EMPTY;
		TRUHardgoodShippingGroup existingShippingGroup = null;
		try {
			existingShippingGroup = getPurchaseProcessHelper().getFirstHardgoodShippingGroup(pOrder);
		} catch (CommerceException commerceException) {
			if (isLoggingError()) {
				logError("commerceException occured in TRUShippingProcessHelper.getDefaultShipMethod", commerceException);
			}
		}
		if(!pValidShipMethods.isEmpty() && existingShippingGroup!= null) {
			for (TRUShipMethodVO truShipMethodVO : pValidShipMethods) {
				if(truShipMethodVO.getShipMethodCode().equals(existingShippingGroup.getShippingMethod())) {
					defaultShipMethod = existingShippingGroup.getShippingMethod();
					break;
				}
			}
		}
		return defaultShipMethod;
	}
	/**
	 *	This method populates Order Summary VO.
	 * @param pOrder order
	 * @return ApplePayOrderSummaryVO orderSummary.
	 */
	public ApplePayOrderSummaryVO populateApplePayOrderSummary(Order pOrder){
		ApplePayOrderSummaryVO orderSummary = new ApplePayOrderSummaryVO();
		if(pOrder != null) {
			TRUOrderPriceInfo  orderPriceInfo = (TRUOrderPriceInfo) pOrder.getPriceInfo();
			TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo)pOrder.getTaxPriceInfo();
			orderSummary.setEwasteFees(orderPriceInfo.getEwasteFees());
			orderSummary.setCurrencyCode(orderPriceInfo.getCurrencyCode());
			orderSummary.setShippingTotal(orderPriceInfo.getShipping());
			orderSummary.setRawSubtotal(orderPriceInfo.getRawSubtotal());
			orderSummary.setOrderTotal(orderPriceInfo.getTotal());
			orderSummary.setSurchargeTotal(orderPriceInfo.getTotalShippingSurcharge());
			double totalTax = taxPriceInfo.getEstimatedIslandTax() + taxPriceInfo.getEstimatedLocalTax() + taxPriceInfo.getEstimatedSalesTax();
			orderSummary.setTaxTotal(roundTwoPlaceDecimal(totalTax,TRUConstants.TWO));
		}
		return orderSummary;
	}

	/**
     *     This method fetches item level promotions for applePay.
     * @param pOrder order
     * @return List of Promotions.
     */
     @SuppressWarnings("unchecked")
     public List<PromoStatusVO> fetchApplePayPromotions(Order pOrder) {
            TRUOrderPriceInfo orderPriceInfo = (TRUOrderPriceInfo)pOrder.getPriceInfo();
            List<PricingAdjustment> orderAdjustments = orderPriceInfo.getAdjustments();
            List<PromoStatusVO> filteredActivePromotions = new ArrayList<PromoStatusVO>();
            if(orderAdjustments != null && !orderAdjustments.isEmpty()){
                   RepositoryItem orderPromotion = null;
                   for(PricingAdjustment orderAdjustment : orderAdjustments){
                         orderPromotion = orderAdjustment.getPricingModel();
                         if(orderPromotion == null){
                                continue;
                         }
                         PromoStatusVO filteredOrderPromotionStatus = new PromoStatusVO();
                         TRUPricingModelProperties pricingModelProperties= (TRUPricingModelProperties)getPromotionTools().getPricingModelProperties();
                         String description = null;
                         String displayName = null;
                         if(orderPromotion != null){
                         description=(String)orderPromotion.getPropertyValue(pricingModelProperties.getPromotionDetailsPropertyName());
                             displayName=(String)orderPromotion.getPropertyValue(pricingModelProperties.getDisplayName());
                         }
                         if(StringUtils.isNotBlank(description)){
                                filteredOrderPromotionStatus.setDescription(description);
                         }
                         if(StringUtils.isNotBlank(displayName)){
                                filteredOrderPromotionStatus.setDisplayName(displayName);
                         }
                         double setAmount = -orderAdjustment.getTotalAdjustment();
                          filteredOrderPromotionStatus.setAmount(setAmount);
                         filteredActivePromotions.add(filteredOrderPromotionStatus);
                   }
            }       
            final List<CommerceItem> commerceItems = pOrder.getCommerceItems();
            TRUItemPriceInfo itemPriceInfo = null;       
            for(CommerceItem cItem : commerceItems){
                   itemPriceInfo = (TRUItemPriceInfo) cItem.getPriceInfo();
                   List<PricingAdjustment> adjustments = itemPriceInfo.getAdjustments();
                   if(adjustments == null || adjustments.isEmpty()){
                         continue;
                   }
                   RepositoryItem promotion = null;
                   for(PricingAdjustment priceAdj : adjustments){
                         promotion = priceAdj.getPricingModel();
                         if(promotion == null){
                                continue;
                         }
                         PromoStatusVO filteredPromotionStatus = new PromoStatusVO();
                         TRUPricingModelProperties pricingModelProperties= (TRUPricingModelProperties)getPromotionTools().getPricingModelProperties();
                         String description = null;
                         String displayName = null;
                         if(promotion!=null){
                         description=(String)promotion.getPropertyValue(pricingModelProperties.getPromotionDetailsPropertyName());
                                displayName=(String)promotion.getPropertyValue(pricingModelProperties.getDisplayName());

                         }
                         if(StringUtils.isNotBlank(description)){
                                filteredPromotionStatus.setDescription(description);
                         }
                         if(StringUtils.isNotBlank(displayName)){
                                filteredPromotionStatus.setDisplayName(displayName);
                         }
                         double setAmount = -priceAdj.getTotalAdjustment();
                         filteredPromotionStatus.setAmount(setAmount);

                         filteredActivePromotions.add(filteredPromotionStatus);
                   }
            }
            return filteredActivePromotions;
     }	/**
	 *	This method fetches available Ship Methods for REST Service.
	 * @param pInputParamsMap InputParamsMap
	 * @return List of ShipMethods.
	 */
	@SuppressWarnings("unchecked")
	public List<TRUShipMethodVO> getAvailableShippingMethods(Map pInputParamsMap) {
		TRUShippingPricingEngine shippingPricingEngine = (TRUShippingPricingEngine) getPromotionTools()
				.getPricingTools().getShippingPricingEngine();
		List<TRUShipMethodVO> availableShippingMethods = new ArrayList<TRUShipMethodVO>();
		if (shippingPricingEngine != null) {
			try {
				availableShippingMethods = shippingPricingEngine.getAvailableMethods(null, null, null, null, pInputParamsMap);
			} catch (PricingException pE) {
				if (isLoggingError()) {
					logError("PricingException in pricing shipping method @Class::TRUOrderManager:::@method::addShippingAddress()", pE);
				}
			}
		}
		return availableShippingMethods;
	}
	/**
	 * this method is used to round off the decimal value with two place
	 * @param pNumber the number
	 * @param pRoundingDecimalPlaces the roundingDecimalPlaces
	 * @return BigDecimal Object
	 */
	private double roundTwoPlaceDecimal(double pNumber, int pRoundingDecimalPlaces) {
		if(isLoggingDebug()) {
			vlogDebug("(roundTwoPlaceDecimal): pNumber:{0}",pNumber);
		}
		BigDecimal bd = new BigDecimal(Double.toString(pNumber));
		bd = bd.setScale(pRoundingDecimalPlaces, TRUConstants.NUMERIC_FOUR);
		if(isLoggingDebug()) {
			vlogDebug("(roundTwoPlaceDecimal): bid=g decimal:{0}",bd);
		}
		return bd.doubleValue();
	}
	
	
	/**
	 * This method will update all the shipping method and it's price with respective destination address after merge happens.
	 *
	 * @param pOrder the order
	 */
	public void updateShippingMethodAndPrice(Order pOrder) {
		if (isLoggingDebug()) {
			vlogDebug("Start TRUPurchaseProcessHelper.updateShippingMethodAndPrice for pOrder : {0} ", pOrder.getId());
		}
		TRUOrderImpl orderImpl = (TRUOrderImpl) pOrder;
		List<TRUHardgoodShippingGroup> shippingGroups = orderImpl.getAllHardgoodShippingGroups();
		if (orderImpl.isOrderIsInMultiShip() || orderImpl.isOrderMandateForMultiShipping() || shippingGroups.size()>TRUConstants.ONE) {
			for (TRUHardgoodShippingGroup hardgoodShippingGroup : shippingGroups) {
				final List<CommerceItemRelationship> ciRels = new ArrayList<CommerceItemRelationship>(
						hardgoodShippingGroup.getCommerceItemRelationships());
				if (ciRels != null && !ciRels.isEmpty()) {
					for (CommerceItemRelationship ciRel : ciRels) {
						if (ciRel.getCommerceItem() instanceof TRUGiftWrapCommerceItem
								|| ciRel.getCommerceItem() instanceof TRUDonationCommerceItem) {
							continue;
						}
						if(null==orderImpl.getRegistryItems() || orderImpl.getRegistryItems().isEmpty()){
							addDefaultAddressToShippingGroup(pOrder, hardgoodShippingGroup,null);
						}
						setShipMethodAndPrice(pOrder, ciRel.getCommerceItem(), hardgoodShippingGroup,
								hardgoodShippingGroup.getShippingMethod());
					}
				}

			}
		} else {
			for (TRUHardgoodShippingGroup hardgoodShippingGroup : shippingGroups) {
				if(StringUtils.isBlank(hardgoodShippingGroup.getNickName())){
					addDefaultAddressToShippingGroup(pOrder, hardgoodShippingGroup,null);
				}
				setShipMethodAndPrice(pOrder, null, hardgoodShippingGroup, hardgoodShippingGroup.getShippingMethod());
			}

		}

		if (isLoggingDebug()) {
			vlogDebug("End TRUPurchaseProcessHelper.updateShippingMethodAndPrice for pOrder : {0} ", pOrder.getId());
		}
	}

}
