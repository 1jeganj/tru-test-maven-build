/**
 * 
 */
package com.tru.commerce.order.purchase;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.transaction.Transaction;

import atg.commerce.CommerceException;
import atg.commerce.order.CreditCard;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroupManager;
import atg.commerce.order.purchase.CommerceItemShippingInfoTools;
import atg.commerce.order.purchase.PurchaseProcessFormHandler;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.json.JSONException;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.payment.creditcard.CreditCardTools;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.service.pipeline.PipelineResult;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUPaymentGroupManager;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardStatus;
import com.tru.commerce.order.vo.TRUActiveAgreementsVO;
import com.tru.commerce.payment.TRUPaymentManager;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.TRUIntegrationConfiguration;
import com.tru.common.TRUSOSIntegrationConfig;
import com.tru.common.TRUUrlUtil;
import com.tru.common.TRUUserSession;
import com.tru.common.cml.SystemException;
import com.tru.common.cml.ValidationExceptions;
import com.tru.common.vo.TRUAddressDoctorResponseVO;
import com.tru.errorhandler.fhl.IErrorHandler;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;
import com.tru.logging.TRUSETALogger;
import com.tru.payment.paypal.TRUPaypalUtilityManager;
import com.tru.payment.radial.TRURadialPaymentProcessor;
import com.tru.radial.commerce.payment.paypal.util.TRUPayPalConstants;
import com.tru.radial.commerce.payment.paypal.util.TRUPayPalException;
import com.tru.radial.common.TRURadialConfiguration;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.radial.payment.creditcard.bean.TRURadialGetNonceResponse;
import com.tru.radial.payment.paypal.bean.SetExpressCheckoutResponse;
import com.tru.regex.EmailPasswordValidator;
import com.tru.userprofiling.TRUProfileManager;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.utils.TRUIntegrationStatusUtil;
import com.tru.utils.TRUSchemeDetectionUtil;
import com.tru.utils.TRUStoreUtils;
import com.tru.validator.fhl.IValidator;

/**
 * This class is used to perform all the operations on payment page in checkout flow.
 * @author PA
 * @version 1.0
 */
public class TRUBillingInfoFormHandler extends PurchaseProcessFormHandler {
	
	/** The mSosIntegrationConfiguration. */
	private TRUSOSIntegrationConfig mSosIntegrationConfiguration;

    /**
	 * hold boolean property integrationConfig.
	 */
	private TRUIntegrationConfiguration mIntegrationConfig;	
	
	/**
 	* Property to hold mPromoFinanceRate.
 	 */
 	private String mPromoFinanceRate;
	
	/**
 	* Property to hold mPromoResponseCode.
 	 */
 	private String mPromoResponseCode;
	/**
 	* Property to hold mPromoText.
 	 */
 	private String mPromoText;
	
	/**
	 * property to hold PropertyManager.
	 */
	private TRUPropertyManager mPropertyManager;
	/**
 	 *  Property to hold Address doctor response VO.
 	 */
 	private TRUAddressDoctorResponseVO mAddressDoctorResponseVO;
	/**
 	* Property to hold address doctor response status.
 	 */
 	private String mAddressDoctorProcessStatus;
 	/**
 	* Property to hold address doctor response status.
 	 */
 	private String mPercentageValuation;
	/** 
	 * Property to hold billing MoveToOrder Success URL. */
	private String mMoveToOrderReviewSuccessURL;

	/** 
	 * Property to hold billing MoveToOrder Error URL. */
	private String mMoveToOrderReviewErrorURL;

	/** Property to hold Selected Billing Address. */
	private String mSelectedCardBillingAddress;

	/** property to hold the mValidator.*/
	private IValidator mValidator;
	
	/** property to hold the mErrorHandler.*/
	private IErrorHandler mErrorHandler;
	
	/** property to hold ValidationException . */
	private ValidationExceptions mVe = new ValidationExceptions();
	/** property to hold OrderTools. */
	private TRUOrderTools mOrderTools;
	
	/** map to hold credit card info. */
	private Map<String, String> mCreditCardInfoMap = new HashMap<String, String>();
	
	/** String holds the chain id. */
	private String mMoveToConfirmChainId;
	
	/** Property holds the credit card tools. */
	private CreditCardTools mCreditCardTools;
	
	/** boolean property to hold if address is validated by address doctor or not. */
 	private String mAddressValidated;
 	
 	/** property to hold mCommonSuccessURL. */
	private String mCommonSuccessURL;
	
	/** property to hold mCommonErrorURL. */
	private String mCommonErrorURL;
	
	/** Property to holds mEmailPasswordValidator. */
	private EmailPasswordValidator mEmailPasswordValidator;
	
	/** Holds address values. */
	private ContactInfo mAddressInputFields = new ContactInfo();
	
	/** Holds key for the card to be deleted.*/
	private String mRemoveCardKey;
	
	/** Property to hold paymentInfo Success URL. */
	private String mPaymentInfoSuccessURL;
	
	/** Property to hold paymentInfo Error URL. */
	private String mPaymentInfoErrorURL;
	
	/**property to hold tRUConfiguration .*/
 	private TRUConfiguration mTRUConfiguration;
 	
 	/**property to hold mEditCardNickName. */
	private String mEditCardNickName;
	
	/**property to hold mCreditCardNickName.*/
	private String mCreditCardNickName;
	
	/** property to hold user session component. */
	private TRUUserSession mUserSession;
	
	/** property to hold BillingAddressNickName.*/
	private String mBillingAddressNickName;
	
	/** property to hold OrderSetWithDefault. */
	private boolean mOrderSetWithDefault;
	
	/** property to hold EditOrderBillingAddress. */
	private boolean mEditOrderBillingAddress;
	
	/**property: holds boolean value to consolidate shipping info's before apply. */
	private boolean mConsolidateShippingInfosBeforeApply;

	/** property: holds boolean value to apply default shipping group before apply. */
	private boolean mApplyDefaultShippingGroup;
	
	/** property: Reference to the CommerceItemShippingInfoTools component. */
	private CommerceItemShippingInfoTools mCommerceItemShippingInfoTools;
	
	/** property: Reference to the ShippingProcessHelper component .*/
	private TRUShippingProcessHelper mShippingHelper;
	
	/** property: Reference to the BillingProcessHelper component .*/
	private TRUBillingProcessHelper mBillingHelper;
	
	/** property to hold reward number of profile.*/	
	private String mRewardNumber;
	
	/** property to hold Reward Number Valid flag.*/	
	private String mRewardNumberValid;
	
	/** property to hold email address in order level. */
	private String mEmail;
	
	/** property to Hold payPal utility manager. */
	private TRUPaypalUtilityManager mPayPalUtilityManager;
	
	/** property to Hold financeAgreed. */
	private boolean mFinanceAgreed;
	
	/** Property to hold financingTermsAvailed. */
	private int mFinancingTermsAvailed;

	/** Property to Hold Error handler manager. */
	private DefaultErrorHandlerManager mErrorHandlerManager;
	
	
	/** The m radial configuration. */
	private TRURadialConfiguration mRadialConfiguration;
	
	/** Property to hold if Rus radio is selscted. */
	private boolean mRUSCard;
	/** Property to hold the CreditCardProcessor .*/
	private TRUPaymentManager mPaymentManager;
	
	/** property to hold email address in order level. */
	private String mPayPalToken;
	/**
	 * property to hold mRestService.
	 */
	private boolean mRestService;
	/**
	 * property to hold mPaypalRedirectURL.
	 */
	private String mPaypalRedirectURL;
	
	/** The Gift cards Exist. */
	private boolean mGiftCardsExist;
	
	/**
	 * property to hold mNonceSuccessURL.
	 */
	private String mNonceSuccessURL;
	
	/**
	 * property to hold mNonceErrorURL.
	 */
	private String mNonceErrorURL;
	/**
	 *  property to hold integrationStatusUtil.
	 */
	private TRUIntegrationStatusUtil mIntegrationStatusUtil;
	
	/**
	 *  property to hold mSavedCard.
	 */
	private boolean mSavedCard;
	
	/**
	 * property to hold mRadialPaymentErrorKey.
	 */
	private String mRadialPaymentErrorKey;
	
	
	/**
	 * @return the radialPaymentErrorKey
	 */
	public String getRadialPaymentErrorKey() {
		return mRadialPaymentErrorKey;
	}

	/**
	 * @param pRadialPaymentErrorKey the radialPaymentErrorKey to set
	 */
	public void setRadialPaymentErrorKey(String pRadialPaymentErrorKey) {
		mRadialPaymentErrorKey = pRadialPaymentErrorKey;
	}

	/** property to Hold mStoreUtils. */
	private TRUStoreUtils mStoreUtils;
	
	/**
	 * @return the mStoreUtils
	 */
	public TRUStoreUtils getStoreUtils() {
		return mStoreUtils;
	}

	/**
	 * @param pStoreUtils the mStoreUtils to set
	 */
	public void setStoreUtils(TRUStoreUtils pStoreUtils) {
		this.mStoreUtils = pStoreUtils;
	}
	
	/** The Saved address. */
	private boolean mNewBillingAddress;
	
	/** The Edit billing address. */
	private boolean mEditBillingAddress;
	
	/** The Edit credit card. */
	private boolean mEditCreditCard;
	
	/** The Saved address. */
	private boolean mNewCreditCard;
	
	/**
	 * @return the editCreditCard
	 */
	public boolean isEditCreditCard() {
		return mEditCreditCard;
	}

	/**
	 * @param pEditCreditCard the editCreditCard to set
	 */
	public void setEditCreditCard(boolean pEditCreditCard) {
		mEditCreditCard = pEditCreditCard;
	}

	/**
	 * @return the newCreditCard
	 */
	public boolean isNewCreditCard() {
		return mNewCreditCard;
	}

	/**
	 * @param pNewCreditCard the newCreditCard to set
	 */
	public void setNewCreditCard(boolean pNewCreditCard) {
		mNewCreditCard = pNewCreditCard;
	}

	/**
	 * @return the newBillingAddress
	 */
	public boolean isNewBillingAddress() {
		return mNewBillingAddress;
	}

	/**
	 * @param pNewBillingAddress the newBillingAddress to set
	 */
	public void setNewBillingAddress(boolean pNewBillingAddress) {
		mNewBillingAddress = pNewBillingAddress;
	}

	/**
	 * @return the editBillingAddress
	 */
	public boolean isEditBillingAddress() {
		return mEditBillingAddress;
	}

	/**
	 * @param pEditBillingAddress the editBillingAddress to set
	 */
	public void setEditBillingAddress(boolean pEditBillingAddress) {
		mEditBillingAddress = pEditBillingAddress;
	}

	/** property to Hold financeAgreed. */
	private boolean mNoFinanceAgreed;
	
	/**
	 * @return the noFinanceAgreed
	 */
	public boolean isNoFinanceAgreed() {
		return mNoFinanceAgreed;
	}

	/**
	 * @param pNoFinanceAgreed the noFinanceAgreed to set
	 */
	public void setNoFinanceAgreed(boolean pNoFinanceAgreed) {
		mNoFinanceAgreed = pNoFinanceAgreed;
	}

	/**
	 * Checks if is saved card.
	 *
	 * @return the mSavedCard
	 */
	public boolean isSavedCard() {
		return mSavedCard;
	}

	/**
	 * Sets the saved card.
	 *
	 * @param pSavedCard the savedCard to set
	 */
	public void setSavedCard(boolean pSavedCard) {
		this.mSavedCard = pSavedCard;
	}

	/**
	 * Checks if is gift cards exist.
	 *
	 * @return the giftCardsExist
	 */
	public boolean isGiftCardsExist() {
		return mGiftCardsExist;
	}

	/**
	 * Sets the gift cards exist.
	 *
	 * @param pGiftCardsExist the giftCardsExist to set
	 */
	public void setGiftCardsExist(boolean pGiftCardsExist) {
		mGiftCardsExist = pGiftCardsExist;
	}

	/**
	 * Gets the paypal redirect url.
	 *
	 * @return the paypalRedirectURL
	 */
	public String getPaypalRedirectURL() {
		return mPaypalRedirectURL;
	}

	/**
	 * Sets the paypal redirect url.
	 *
	 * @param pPaypalRedirectURL the paypalRedirectURL to set
	 */
	public void setPaypalRedirectURL(String pPaypalRedirectURL) {
		mPaypalRedirectURL = pPaypalRedirectURL;
	}

	/**
	 * Checks if is rest service.
	 *
	 * @return the mRestService
	 */
	public boolean isRestService() {
		return mRestService;
	}

	/**
	 * Sets the rest service.
	 *
	 * @param pRestService the pRestService to set
	 */
	public void setRestService(boolean pRestService) {
		this.mRestService = pRestService;
	}
	
	/**
	 * Gets the payment manager.
	 *
	 * @return the paymentManager
	 */
	public TRUPaymentManager getPaymentManager() {
		return mPaymentManager;
	}

	/**
	 * Sets the payment manager.
	 *
	 * @param pPaymentManager the paymentManager to set
	 */
	public void setPaymentManager(TRUPaymentManager pPaymentManager) {
		mPaymentManager = pPaymentManager;
	}

	/**
	 * Gets the error handler manager.
	 * 
	 * @return the error handler manager.
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * Sets the error handler manager.
	 * 
	 * @param pErrorHandlerManager
	 *            the new error handler manager
	 */
	public void setErrorHandlerManager(DefaultErrorHandlerManager pErrorHandlerManager) {
		mErrorHandlerManager = pErrorHandlerManager;
	}
	
	/**
	 * Gets the PayPal Utility Manager.
	 * 
	 * @return the PayPal Utility Manager.
	 */
	public TRUPaypalUtilityManager getPayPalUtilityManager() {
		return mPayPalUtilityManager;
	}

	/**
	 * Sets the PayPal Utility Manager.
	 * 
	 * @param pPayPalUtilityManager
	 *            the new PayPal Utility Manager.
	 */
	public void setPayPalUtilityManager(TRUPaypalUtilityManager pPayPalUtilityManager) {
		this.mPayPalUtilityManager = pPayPalUtilityManager;
	}
	
	/**
	 * Gets the reward number valid.
	 *
	 * @return the mRewardNumberValid
	 */
	public String getRewardNumberValid() {
		return mRewardNumberValid;
	}
	
	/**
	 * Sets the reward number valid.
	 *
	 * @param pRewardNumberValid the rewardNumberValid to set
	 */
	public void setRewardNumberValid(String pRewardNumberValid) {
		this.mRewardNumberValid = pRewardNumberValid;
	}
	
	/**
	 * Gets the reward number.
	 *
	 * @return the mRewardNumber
	 */
	public String getRewardNumber() {
		return mRewardNumber;
	}
	
	/**
	 * Sets the reward number.
	 *
	 * @param pRewardNumber the mRewardNumber to set
	 */
	public void setRewardNumber(String pRewardNumber) {
		this.mRewardNumber = pRewardNumber;
	}
	
	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return mEmail;
	}

	/**
	 * Sets the email.
	 *
	 * @param pEmail the email to set
	 */
	public void setEmail(String pEmail) {
		this.mEmail = pEmail;
	}

	/**
	 * Gets the address validated.
	 *
	 * @return mAddressValidated address validated
	 */
	public String getAddressValidated() {
		return mAddressValidated;
	}

	/**
	 * Sets the address validated.
	 *
	 * @param pAddressValidated parameter
	 */
	public void setAddressValidated(String pAddressValidated) {
		this.mAddressValidated = pAddressValidated;
	}

	/**
	 * Gets the common success url.
	 *
	 * @return the commonSuccessURL
	 */
	public String getCommonSuccessURL() {
		return mCommonSuccessURL;
	}

	/**
	 * Sets the common success url.
	 *
	 * @param pCommonSuccessURL the commonSuccessURL to set
	 */
	public void setCommonSuccessURL(String pCommonSuccessURL) {
		this.mCommonSuccessURL = pCommonSuccessURL;
	}

	/**
	 * Gets the common error url.
	 *
	 * @return the commonErrorURL
	 */
	public String getCommonErrorURL() {
		return mCommonErrorURL;
	}

	/** 
	 * property to hold Credit Card Type.
	 */
	private String mCreditCardType;
	
	/**
	 * This property holds SETALogger component ref.
	 */
	private TRUSETALogger mSetaLogger;
	
	/**
	 * @return the setaLogger
	 */
	public TRUSETALogger getSetaLogger() {
		return mSetaLogger;
	}

	/**
	 * @param pSetaLogger the setaLogger to set
	 */
	public void setSetaLogger(TRUSETALogger pSetaLogger) {
		mSetaLogger = pSetaLogger;
	}

	/**
	 * Gets the credit card type.
	 *
	 * @return the creditCardType
	 */
	public String getCreditCardType() {
		return mCreditCardType;
	}

	/**
	 * Sets the credit card type.
	 *
	 * @param pCreditCardType the creditCardType to set
	 * Need to setCreditCardType from Payment jsp
	 */
	public void setCreditCardType(String pCreditCardType) {
		mCreditCardType = pCreditCardType;
	}

	/**
	 * Sets the common error url.
	 *
	 * @param pCommonErrorURL common error url
	 */
	public void setCommonErrorURL(String pCommonErrorURL) {
		this.mCommonErrorURL = pCommonErrorURL;
	}

	/**
	 * Gets the shipping helper.
	 *
	 * @return the shippingHelper
	 */
	public TRUShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * Sets the shipping helper.
	 *
	 * @param pShippingHelper the shippingHelper to set
	 */
	public void setShippingHelper(TRUShippingProcessHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}

	/**
	 * Gets the billing helper.
	 *
	 * @return the billingHelper
	 */
	public TRUBillingProcessHelper getBillingHelper() {
		return mBillingHelper;
	}

	/**
	 * Sets the billing helper.
	 *
	 * @param pBillingHelper the billingHelper to set
	 */
	public void setBillingHelper(TRUBillingProcessHelper pBillingHelper) {
		mBillingHelper = pBillingHelper;
	}

	/**
	 * Gets the commerce item shipping info tools.
	 *
	 * @return the commerceItemShippingInfoTools
	 */
	public CommerceItemShippingInfoTools getCommerceItemShippingInfoTools() {
		return mCommerceItemShippingInfoTools;
	}

	/**
	 * Sets the commerce item shipping info tools.
	 *
	 * @param pCommerceItemShippingInfoTools the commerceItemShippingInfoTools to set
	 */
	public void setCommerceItemShippingInfoTools(
			CommerceItemShippingInfoTools pCommerceItemShippingInfoTools) {
		mCommerceItemShippingInfoTools = pCommerceItemShippingInfoTools;
	}

	/**
	 * Checks if is consolidate shipping infos before apply.
	 *
	 * @return the consolidateShippingInfosBeforeApply
	 */
	public boolean isConsolidateShippingInfosBeforeApply() {
		return mConsolidateShippingInfosBeforeApply;
	}

	/**
	 * Sets the consolidate shipping infos before apply.
	 *
	 * @param pConsolidateShippingInfosBeforeApply the consolidateShippingInfosBeforeApply to set
	 */
	public void setConsolidateShippingInfosBeforeApply(
			boolean pConsolidateShippingInfosBeforeApply) {
		mConsolidateShippingInfosBeforeApply = pConsolidateShippingInfosBeforeApply;
	}

	/**
	 * Checks if is apply default shipping group.
	 *
	 * @return the applyDefaultShippingGroup
	 */
	public boolean isApplyDefaultShippingGroup() {
		return mApplyDefaultShippingGroup;
	}

	/**
	 * Sets the apply default shipping group.
	 *
	 * @param pApplyDefaultShippingGroup the applyDefaultShippingGroup to set
	 */
	public void setApplyDefaultShippingGroup(boolean pApplyDefaultShippingGroup) {
		mApplyDefaultShippingGroup = pApplyDefaultShippingGroup;
	}
	
	/**
	 * Gets the email password validator.
	 *
	 * @return the emailPasswordValidator
	 */
	public EmailPasswordValidator getEmailPasswordValidator() {
		return mEmailPasswordValidator;
	}
	
	/**
	 * Sets the email password validator.
	 *
	 * @param pEmailPasswordValidator the emailPasswordValidator to set
	 */
	public void setEmailPasswordValidator(EmailPasswordValidator pEmailPasswordValidator) {
		this.mEmailPasswordValidator = pEmailPasswordValidator;
	}
	
	/**
	 * Gets the credit card tools.
	 *
	 * @return the creditCardTools
	 */
	public CreditCardTools getCreditCardTools() {
		return mCreditCardTools;
	}

	/**
	 * Sets the credit card tools.
	 *
	 * @param pCreditCardTools the creditCardTools to set
	 */
	public void setCreditCardTools(CreditCardTools pCreditCardTools) {
		mCreditCardTools = pCreditCardTools;
	}

	/**
	 * Gets the move to confirm chain id.
	 *
	 * @return the moveToConfirmChainId
	 */
	public String getMoveToConfirmChainId() {
		return mMoveToConfirmChainId;
	}

	/**
	 * Sets the move to confirm chain id.
	 *
	 * @param pMoveToConfirmChainId the moveToConfirmChainId to set
	 */
	public void setMoveToConfirmChainId(String pMoveToConfirmChainId) {
		mMoveToConfirmChainId = pMoveToConfirmChainId;
	}

	/**
	 * Gets the credit card nick name.
	 *
	 * @return mCreditCardNickName credit card nick name
	 */
	public String getCreditCardNickName() {
		return mCreditCardNickName;
	}

	/**
	 * Sets the credit card nick name.
	 *
	 * @param pCreditCardNickName credit card nick name
	 */
	public void setCreditCardNickName(String pCreditCardNickName) {
		mCreditCardNickName = pCreditCardNickName;
	}
	
	/**
	 * Gets the user session.
	 *
	 * @return the userSession
	 */
	public TRUUserSession getUserSession() {
		return mUserSession;
	}

	/**
	 * Sets the user session.
	 *
	 * @param pUserSession the userSession to set
	 */
	public void setUserSession(TRUUserSession pUserSession) {
		mUserSession = pUserSession;
	}

	/**
	 * Checks if is order set with default.
	 *
	 * @return mOrderSetWithDefault order set with default property
	 */
	public boolean isOrderSetWithDefault() {
		return mOrderSetWithDefault;
	}
	
	/**
	 * Sets the order set with default.
	 *
	 * @param pOrderSetWithDefault parameter
	 */
	public void setOrderSetWithDefault(boolean pOrderSetWithDefault) {
		mOrderSetWithDefault = pOrderSetWithDefault;
	}
	
	/**
	 * Gets the billing address nick name.
	 *
	 * @return mBillingAddressNickName.
	 */
	public String getBillingAddressNickName() {
		return mBillingAddressNickName;
	}
	
	/**
	 * Sets the billing address nick name.
	 *
	 * @param pBillingAddressNickName parameter.
	 */
	public void setBillingAddressNickName(String pBillingAddressNickName) {
		mBillingAddressNickName = pBillingAddressNickName;
	}
	
	/**
	 * Gets the tRU configuration.
	 *
	 * @return the tRUConfiguration.
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * Sets the tRU configuration.
	 *
	 * @param pTRUConfiguration the tRUConfiguration to set.
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		mTRUConfiguration = pTRUConfiguration;
	}
	
	/**
	 * Gets the edits the card nick name.
	 *
	 * @return the editCardNickName.
	 */
	public String getEditCardNickName() {
		return mEditCardNickName;
	}

	/**
	 * Sets the edits the card nick name.
	 *
	 * @param pEditCardNickName the editCardNickName to set.
	 */
	public void setEditCardNickName(String pEditCardNickName) {
		mEditCardNickName = pEditCardNickName;
	}
	
	/**
	 * Gets the move to order review success url.
	 *
	 * @return the moveToOrderReviewSuccessURL.
	 */
	public String getMoveToOrderReviewSuccessURL() {
		return mMoveToOrderReviewSuccessURL;
	}

	/**
	 * Sets property moveToOrderReviewSuccessURL.
	 * 
	 * @param pMoveToOrderReviewSuccessURL - the moveToOrderReviewSuccessURL to set
	 **/
	public void setMoveToOrderReviewSuccessURL(
			String pMoveToOrderReviewSuccessURL) {
		mMoveToOrderReviewSuccessURL = pMoveToOrderReviewSuccessURL;
	}

	/**
	 * Gets the move to order review error url.
	 *
	 * @return the moveToOrderReviewErrorURL
	 */
	public String getMoveToOrderReviewErrorURL() {
		return mMoveToOrderReviewErrorURL;
	}

	/**
	 * Sets property moveToOrderReviewErrorURL.
	 * 
	 * @param pMoveToOrderReviewErrorURL -the moveToOrderReviewErrorURL to set
	 **/
	public void setMoveToOrderReviewErrorURL(String pMoveToOrderReviewErrorURL) {
		mMoveToOrderReviewErrorURL = pMoveToOrderReviewErrorURL;
	}

	/**
	 * Gets the order tools.
	 *
	 * @return the orderTools
	 */
	public TRUOrderTools getOrderTools() {
		return mOrderTools;
	}

	/**
	 * Sets property OrderTools.
	 * 
	 * @param pOrderTools - the orderTools to set
	 **/
	public void setOrderTools(TRUOrderTools pOrderTools) {
		mOrderTools = pOrderTools;
	}


	/**
	 * Gets the address input fields.
	 *
	 * @return the mAddressInputFields
	 */
	public ContactInfo getAddressInputFields() {
		return mAddressInputFields;
	}

	/**
	 * Sets the address input fields.
	 *
	 * @param pAddressInputFields the addressInputFields to set
	 */
	public void setAddressInputFields(ContactInfo pAddressInputFields) {
		
		this.mAddressInputFields = pAddressInputFields;
	}

	/**
	 * Gets the credit card info map.
	 *
	 * @return the creditCardInfoMap
	 */
	public Map<String, String> getCreditCardInfoMap() {
		return mCreditCardInfoMap;
	}

	/**
	 * Sets the credit card info map.
	 *
	 * @param pCreditCardInfoMap the creditCardInfoMap to set
	 */
	public void setCreditCardInfoMap(Map<String, String> pCreditCardInfoMap) {
		mCreditCardInfoMap = pCreditCardInfoMap;
	}

	/**
	 * Gets the selected card billing address.
	 *
	 * @return the selectedCardBillingAddress
	 */
	public String getSelectedCardBillingAddress() {
		return mSelectedCardBillingAddress;
	}
	
	/**
	 * Sets the selected card billing address.
	 *
	 * @param pSelectedCardBillingAddress the selectedCardBillingAddress to set
	 */
	public void setSelectedCardBillingAddress(String pSelectedCardBillingAddress) {
		this.mSelectedCardBillingAddress = pSelectedCardBillingAddress;
	}

	/**
	 * Gets the validator.
	 *
	 * @return the IValidator
	 */
	public IValidator getValidator() {
		return mValidator;
	}

	/**
	 * Sets the validator.
	 *
	 * @param pValidator the IValidator to set
	 */
	public void setValidator(IValidator pValidator) {
		this.mValidator = pValidator;
	}

	/**
	 * Gets the error handler.
	 *
	 * @return the IErrorHandler
	 */
	public IErrorHandler getErrorHandler() {
		return mErrorHandler;
	}

	/**
	 * Sets the error handler.
	 *
	 * @param pErrorHandler the IErrorHandler to set
	 */
	public void setErrorHandler(IErrorHandler pErrorHandler) {
		this.mErrorHandler = pErrorHandler;
	}
	
	/**
	 * Gets the removes the card key.
	 *
	 * @return the RemoveCardKey
	 */
	public String getRemoveCardKey() {
		return mRemoveCardKey;
	}

	/**
	 * Sets the removes the card key.
	 *
	 * @param pRemoveCardKey the pRemoveCardKey to set
	 */
	public void setRemoveCardKey(String pRemoveCardKey) {
		mRemoveCardKey = pRemoveCardKey;
	}
	
	/**
	 * Gets the payment info success url.
	 *
	 * @return the paymentInfoSuccessURL
	 */
	public String getPaymentInfoSuccessURL() {
		return mPaymentInfoSuccessURL;
	}
	
	/**
	 * Sets property paymentInfoSuccessURL.
	 * 
	 * @param pPaymentInfoSuccessURL
	 *         the paymentInfoSuccessURL to set
	 **/
	public void setPaymentInfoSuccessURL(String pPaymentInfoSuccessURL) {
		mPaymentInfoSuccessURL = pPaymentInfoSuccessURL;
	}
    
	/**
	 * Gets the payment info error url.
	 *
	 * @return the paymentInfoErrorURL
	 */
	public String getPaymentInfoErrorURL() {
		return mPaymentInfoErrorURL;
	}
    
	/**
	 * Sets property paymentInfoErrorURL.
	 *     
	 * @param pPaymentInfoErrorURL
	 *     the paymentInfoErrorURL to set   
	 **/
	public void setPaymentInfoErrorURL(String pPaymentInfoErrorURL) {
		mPaymentInfoErrorURL = pPaymentInfoErrorURL;
	}
	
	/**
	 * Gets the address doctor process status.
	 *
	 * @return the addressDoctorProcessStatus
	 */
 	public String getAddressDoctorProcessStatus() {
 		return mAddressDoctorProcessStatus;
 	}
 	
	 /**
	  * Sets the address doctor process status.
	  *
	  * @param pAddressDoctorProcessStatus the addressDoctorProcessStatus to set
	  */
	public void setAddressDoctorProcessStatus(String pAddressDoctorProcessStatus) {
		mAddressDoctorProcessStatus = pAddressDoctorProcessStatus;
	}
	
	/**
	 * Gets the address doctor response vo.
	 *
	 * @return the addressDoctorResponseVO
	 */
	public TRUAddressDoctorResponseVO getAddressDoctorResponseVO() {
		return mAddressDoctorResponseVO;
	}
	
	/**
	 * Sets the address doctor response vo.
	 *
	 * @param pAddressDoctorResponseVO the addressDoctorResponseVO to set
	 */
	public void setAddressDoctorResponseVO(
			TRUAddressDoctorResponseVO pAddressDoctorResponseVO) {
		mAddressDoctorResponseVO = pAddressDoctorResponseVO;
	}

	/**
	 * Checks if is finance agreed.
	 *
	 * @return the financeAgreed
	 */
	public boolean isFinanceAgreed() {
		return mFinanceAgreed;
	}

	/**
	 * Sets the finance agreed.
	 *
	 * @param pFinanceAgreed the financeAgreed to set
	 */
	public void setFinanceAgreed(boolean pFinanceAgreed) {
		this.mFinanceAgreed = pFinanceAgreed;
	}

	/**
	 * Gets the financing terms availed.
	 *
	 * @return the financingTermsAvailed
	 */
	public int getFinancingTermsAvailed() {
		return mFinancingTermsAvailed;
	}

	/**
	 * Sets the financing terms availed.
	 *
	 * @param pFinancingTermsAvailed the financingTermsAvailed to set
	 */
	public void setFinancingTermsAvailed(int pFinancingTermsAvailed) {
		this.mFinancingTermsAvailed = pFinancingTermsAvailed;
	}
	
	/**
	 * Checks if is rUS card.
	 *
	 * @return the mRUSCard
	 */
	public boolean isRUSCard() {
		return mRUSCard;
	}

	/**
	 * Sets the rUS card.
	 *
	 * @param pRUSCard the mRUSCard to set
	 */
	public void setRUSCard(boolean pRUSCard) {
		this.mRUSCard = pRUSCard;
	}
	
	/** The Payment group type. */
	private String mPaymentGroupType;

	/**
	 * Gets the payment group type.
	 *
	 * @return the paymentGroupType
	 */
	public String getPaymentGroupType() {
		return mPaymentGroupType;
	}

	/**
	 * Sets the payment group type.
	 *
	 * @param pPaymentGroupType the paymentGroupType to set
	 */
	public void setPaymentGroupType(String pPaymentGroupType) {
		mPaymentGroupType = pPaymentGroupType;
	}
	
	/**
	 * Gets the property manager.
	 *
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}
		
	/**
	 * This method is used to get promoFinanceRate.
	 * @return promoFinanceRate String
	 */
	public String getPromoFinanceRate() {
		return mPromoFinanceRate;
	}

	/**
	 * This method is used to set promoFinanceRate.
	 *
	 * @param pPromoFinanceRate the new promo finance rate
	 */
	public void setPromoFinanceRate(String pPromoFinanceRate) {
		mPromoFinanceRate = pPromoFinanceRate;
	}

	/**
	 * This method is used to get promoResponseCode.
	 * @return promoResponseCode String
	 */
	public String getPromoResponseCode() {
		return mPromoResponseCode;
	}

	/**
	 * This method is used to set promoResponseCode.
	 *
	 * @param pPromoResponseCode the new promo response code
	 */
	public void setPromoResponseCode(String pPromoResponseCode) {
		mPromoResponseCode = pPromoResponseCode;
	}

	/**
	 * This method is used to get promoText.
	 * @return promoText String
	 */
	public String getPromoText() {
		return mPromoText;
	}

	/**
	 * This method is used to set promoText.
	 *
	 * @param pPromoText the new promo text
	 */
	public void setPromoText(String pPromoText) {
		mPromoText = pPromoText;
	}
	
	

	/**
	 * Gets the radial configuration.
	 *
	 * @return the mRadialConfiguration
	 */
	public TRURadialConfiguration getRadialConfiguration() {
		return mRadialConfiguration;
	}

	/**
	 * Sets the radial configuration.
	 *
	 * @param pRadialConfiguration the new radial configuration
	 */
	public void setRadialConfiguration(TRURadialConfiguration pRadialConfiguration) {
		this.mRadialConfiguration = pRadialConfiguration;
	}

	/**
	 * This method is used to handle the initialization of payment group card based on the given payment group type.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return true
	 * @throws ServletException
	 * 			servletException
	 * @throws IOException
	 * 			iOException
	 * @throws SystemException 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean handleEnsurePaymentGroup(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, SystemException {
		if (isLoggingDebug()) {
			vlogDebug("Start of handleEnsurePaymentGroup method");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = TRUConstants.HANDLE_ENSURE_PAYMENT_GROUP;
		//String taxwareErrMessage = getErrorHandlerManager().getErrorMessage(TRUTaxwareConstant.TRU_TAXWARE_GENERAL_DATA_ERROR);		
		boolean isRepriceRequired = true;
		boolean hasException = false;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			resetFormExceptions();
			if (StringUtils.isBlank(getPaymentGroupType())) {
				return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
			}
			try {
				 if (PerformanceMonitor.isEnabled()){
						PerformanceMonitor.startOperation(myHandleMethod);
					}
				tr = ensureTransaction();
				synchronized (getOrder()) {
					TRUOrderImpl truOrderImpl = (TRUOrderImpl) getOrder();
					TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
					TRUCreditCard containerCreditCard = (TRUCreditCard) orderManager.getDefaultCreditCard(
							truOrderImpl, getPaymentGroupMapContainer());
					boolean isApplyExpressCheckout = truOrderImpl.isApplyExpressCheckout();
					if ((isApplyExpressCheckout && containerCreditCard != null) ||
							!isApplyExpressCheckout) {
						boolean isPayPalOrder = truOrderImpl.isPayPalOrder();
						if (isApplyExpressCheckout && isPayPalOrder) {
							truOrderImpl.setApplyExpressCheckout(Boolean.FALSE);
						} else {
							getBillingHelper().ensurePaymentGroup(truOrderImpl, getPaymentGroupType());
						}
					}
					if (isApplyExpressCheckout) {
						truOrderImpl.setApplyExpressCheckout(Boolean.FALSE);
					}
					getUserSession().setRusCardSelected(isRUSCard());
					
					// ensures default credit card to the order
					if (TRUCheckoutConstants.CREDITCARD.equalsIgnoreCase(getPaymentGroupType())) {
						TRUCreditCard orderCreditCard = (TRUCreditCard) orderManager.getCreditCard(truOrderImpl);
						// If credit card is empty update the default credit card
						if(!isRUSCard() && truOrderImpl.isRusCardSelected()){
							orderCreditCard = null;
						}
						if (((orderCreditCard == null)
								|| (orderCreditCard != null && StringUtils.isBlank(orderCreditCard
										.getCreditCardNumber()))) && containerCreditCard != null) {
							TRUProfileManager profileManager = getBillingHelper().getProfileManager();
							if(getProfile().isTransient()){
								//Start changes TUW-58766.
								boolean isCardExpired = profileManager.validateCardExpiration(containerCreditCard.getExpirationMonth(),containerCreditCard.getExpirationYear());
								if(!isCardExpired){
									getBillingHelper().updateOrderWithCreditCard(truOrderImpl, containerCreditCard);
								}
								if(truOrderImpl.getBillingAddressNickName() == null){
									ContactInfo billingAddress  = new ContactInfo();
									OrderTools.copyAddress(containerCreditCard.getBillingAddress(), billingAddress);
									((TRUOrderImpl)getOrder()).setBillingAddress(billingAddress);
									((TRUOrderImpl)getOrder()).setBillingAddressNickName(containerCreditCard.getBillingAddressNickName());
								}
							}else{
								//Start : Changes done for TUW-56872.
								String creditCardNickName = (String) getProfile().getPropertyValue(getPropertyManager().getSelectedCCNickNamePropertyName());
								if(StringUtils.isNotBlank(creditCardNickName)) {
									CreditCard creditCard = (CreditCard)getPaymentGroupMapContainer().getPaymentGroup(creditCardNickName);
									if(creditCard != null) {
										boolean isCardExpired = profileManager.validateCardExpiration(creditCard.getExpirationMonth(),creditCard.getExpirationYear());
										if(!isCardExpired){
											getBillingHelper().updateOrderWithCreditCard(truOrderImpl, (CreditCard)getPaymentGroupMapContainer().getPaymentGroup(creditCardNickName));
										}
									}
								} else {
									boolean isCardExpired = profileManager.validateCardExpiration(containerCreditCard.getExpirationMonth(),containerCreditCard.getExpirationYear());
									if(!isCardExpired){
										getBillingHelper().updateOrderWithCreditCard(truOrderImpl, containerCreditCard);
										creditCardNickName = getPaymentGroupMapContainer().getDefaultPaymentGroupName();
										getBillingHelper().getPurchaseProcessHelper().getProfileTools().updateProperty(getPropertyManager().getSelectedCCNickNamePropertyName(), creditCardNickName, getProfile());
									}
								}
								//Start changes TUW-58766.
								//End   : Changes done for TUW-56872.
							}
						}
					}
					TRUCreditCard orderCreditCard = (TRUCreditCard) orderManager.getCreditCard(truOrderImpl);
					if(orderCreditCard != null) {
						if(getUserSession().isRusCardSelected() && null != getUserSession().getTSPCardInfoMap().get(TRUCommerceConstants.TSP_CARD_NUMBER)) {
							truOrderImpl.setCreditCardType(getUserSession().getTSPCardInfoMap().get(TRUConstants.CREDIT_CARD_TYPE));
						} else {
							truOrderImpl.setCreditCardType(orderCreditCard.getCreditCardType());
						}
						orderManager.setCCTypeNullIfPGAsGC(truOrderImpl);
						if(getUserSession().isRusCardSelected() &&
								null != ((TRUOrderImpl)getOrder()).getActiveAgreementsVO().get(TRUConstants.TSP_CARD_NUMBER) && 
								TRUConstants.INTEGER_NUMBER_ONE == ((TRUOrderImpl)getOrder()).getActiveAgreementsVO().
								get(TRUConstants.TSP_CARD_NUMBER).getFinanceAgreed()) { 
							((TRUOrderImpl)getOrder()).setFinanceAgreed(Boolean.TRUE);
						} if(orderCreditCard.getCreditCardNumber() != null &&
								null != ((TRUOrderImpl)getOrder()).getActiveAgreementsVO().get(orderCreditCard.getCreditCardNumber()) && 
								TRUConstants.INTEGER_NUMBER_ONE == ((TRUOrderImpl)getOrder()).getActiveAgreementsVO().get(orderCreditCard.getCreditCardNumber()).
								getFinanceAgreed()) { 
							((TRUOrderImpl)getOrder()).setFinanceAgreed(Boolean.TRUE);
						} else {
							((TRUOrderImpl)getOrder()).setFinanceAgreed(Boolean.FALSE);
						}
						if(TRUCheckoutConstants.CREDITCARD.equalsIgnoreCase(getPaymentGroupType()) && StringUtils.isBlank(orderCreditCard
								.getCreditCardNumber())){
							isRepriceRequired = false;
						}
						
					} else {
						truOrderImpl.setCreditCardType(null);
					}
					if (getFormError()) {
						hasException = true;
					}
					Map extraParams=createRepriceParameterMap();
					if(extraParams==null){
						extraParams=new HashMap<>();
					}
					if(isRepriceRequired){
						extraParams.put(TRUTaxwareConstant.CALC_TAX, Boolean.FALSE);
						runProcessRepriceOrder(truOrderImpl, getUserPricingModels(), getUserLocale(), getProfile(), createRepriceParameterMap());
					}
					orderManager.updateOrder(truOrderImpl);
				}
			} catch (CommerceException comExc) {
				hasException = true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handleEnsurePaymentGroup()", comExc);
				}
			} catch (RunProcessException runProcExp) {
				hasException = true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handleEnsurePaymentGroup()", runProcExp);
				}
			} catch (RepositoryException re) {
				hasException = true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("RepositoryException in @Class::TRUBillingInfoFormHandler::@method::handleEnsurePaymentGroup()", re);
				}
			} finally {
				if (tr != null) {					
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (javax.transaction.SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in TRUBillingInfoFormHandler.handleEnsurePaymentGroup", e);
							}
						}
					}
					if (tr != null) {
						commitTransaction(tr);
					}
				}
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.endOperation(myHandleMethod);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of handleEnsurePaymentGroup method");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}	
	
	/**
	 * Handle move to order review.
	 *
	 * @param pRequest  DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return true
	 * @throws ServletException servletException
	 * @throws IOException iOException
	 * @throws SystemException SystemException
	 */
	public boolean handleMoveToOrderReview(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, SystemException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUBillingInfoFormHandler.handleMoveToOrderReview method");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "TRUBillingInfoFormHandler.handleMoveToOrderReview";
		boolean hasException=false;
		boolean isEnableAddressDoctor = false;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			resetFormExceptions();
			try {
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.startOperation(myHandleMethod);
				}
				tr = ensureTransaction();
 				synchronized (getOrder()) {
					double giftCardAmtApplied = ((TRUOrderTools)(getOrderManager().getOrderTools())).getGiftCardAppliedAmount(getOrder());
					double orderTotal = ((TRUOrderTools) (getOrderManager().getOrderTools())).getPricingTools().round(
							getOrder().getPriceInfo().getTotal());
					double balnceAmount = orderTotal-giftCardAmtApplied;
					if(balnceAmount != TRUCommerceConstants.DOUBLE_ZERO) {
						// Calling the pre handle method
						preMoveToOrderReview(pRequest, pResponse);
					} else{
						if(!getFormError() && StringUtils.isNotEmpty(getRewardNumber()) && getRewardNumberValid().equalsIgnoreCase(TRUConstants.FALSE)){
							mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_REWARD_NUMBER_INVALID, null);   
							getErrorHandler().processException(mVe, this);
						}
					}
					if(getFormError()){
						return false;
					}
					TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
					TRUProfileTools profileTools = (TRUProfileTools) getOrderTools().getProfileTools();
					
					boolean isAddressValidated = Boolean.parseBoolean(getAddressValidated());
					if (!TRUConstants.UNITED_STATES.equalsIgnoreCase(getAddressInputFields().getCountry())){
						isAddressValidated = true;
					}
					
					//Validate order remaining amount.
					validateOrderRemainingAmount(pRequest, pResponse);
					// Validate address with AddressDoctor Service
					
					if(SiteContextManager.getCurrentSite() != null){
						isEnableAddressDoctor = getIntegrationStatusUtil().getAddressDoctorIntStatus(pRequest);
					}
					
					isAddressValidated = setAddressFromAddressDoctorResponse(isAddressValidated,isEnableAddressDoctor);
					
					if(getFormError() || !isAddressValidated) {
						if (isLoggingDebug()) {
							vlogDebug("End of TRUBillingInfoFormHandler.handleMoveToOrderReview method");
						}
						return checkFormRedirect(getCommonSuccessURL(),	getCommonErrorURL(), pRequest, pResponse);
					}
					
					String billingAddressNickName = getBillingAddressNickName();
					if(isNewBillingAddress()) {
						ContactInfo billingAddress  = new ContactInfo();
						OrderTools.copyAddress(getAddressInputFields(), billingAddress);
						//Adding the billing address to the credit card. 
						((TRUOrderImpl)getOrder()).setBillingAddress(billingAddress);
						String generatedUniqueNickName = orderManager.generateUniqueNickNameForAddress(getAddressInputFields(), getShippingGroupMapContainer().getShippingGroupMap());
						String nickName = null;
						if(generatedUniqueNickName.length() > TRUConstants.NICKNAME_MAX_LENGTH){
							nickName = generatedUniqueNickName.substring(TRUConstants.NICKNAME_MIN_LENGTH, TRUConstants.NICKNAME_MAX_LENGTH);
						} else {
							nickName = generatedUniqueNickName;
						}
						((TRUOrderImpl)getOrder()).setBillingAddressNickName(nickName);
						final ShippingGroupManager sgm = getPurchaseProcessHelper().getShippingGroupManager();
						final TRUHardgoodShippingGroup hgsg = (TRUHardgoodShippingGroup) sgm.createShippingGroup();	   
						final Address address = hgsg.getShippingAddress();	    
						OrderTools.copyAddress(getAddressInputFields(), address);
						hgsg.setBillingAddress(Boolean.TRUE);
						hgsg.setNickName(nickName);
						billingAddressNickName = nickName;
						hgsg.setLastActivity(new Timestamp((new Date()).getTime()));
						getShippingGroupMapContainer().addShippingGroup(nickName, hgsg);
						setBillingAddressNickName(nickName);
						if(!getProfile().isTransient()){
							getSetaLogger().logAddBillingAddress((String)getProfile().getPropertyValue(getPropertyManager().getLoginPropertyName()), billingAddressNickName );
						}
					} else if (isEditBillingAddress()){
						Address billingAddress  = ((TRUOrderImpl)getOrder()).getBillingAddress();
						OrderTools.copyAddress(getAddressInputFields(), billingAddress);
						//Adding the billing address to the credit card. 
						billingAddressNickName = ((TRUOrderImpl)getOrder()).getBillingAddressNickName();
						final TRUHardgoodShippingGroup containerShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupMapContainer().
								getShippingGroup(billingAddressNickName);  
						setBillingAddressNickName(billingAddressNickName);
						OrderTools.copyAddress(getAddressInputFields(), containerShippingGroup.getShippingAddress());
						if(!getProfile().isTransient()){
							getSetaLogger().logUpdateBillingAddress((String)getProfile().getPropertyValue(getPropertyManager().getLoginPropertyName()), billingAddressNickName);
						}
					}
					
					if(isRUSCard()){
						TRUCreditCard creditCard = (TRUCreditCard) orderManager.getCreditCard(getOrder());
						/*Start - MVP for R Us card Billing address*/
						getUserSession().setTSPBillingAddressMap(getAddressInputFields());
						getUserSession().setTSPEditBillingAddress(true);
						getUserSession().setInputEmail(null);
						/*End - MVP for R Us card Billing address*/
						getBillingHelper().updateCreditCard(getOrder(), getCreditCardInfoMap(), getAddressInputFields());
						creditCard.setBillingAddressNickName(((TRUOrderImpl)getOrder()).getBillingAddressNickName());
					}
					
					if(isNewCreditCard() && balnceAmount != TRUCommerceConstants.DOUBLE_ZERO) {
						TRUCreditCard creditCard = (TRUCreditCard) orderManager.getCreditCard(getOrder());
						if (creditCard == null && isLoggingError()) {
							if (isLoggingError()) {
								logError("handleMoveToOrderReview() Credit Card is NULL");
							}
						}
						if(!isRUSCard()){
							// update credit card form data to container
							addCreditCard(pRequest, pResponse);
						} 
						// updating order credit card
						getBillingHelper().updateCreditCard(getOrder(), getCreditCardInfoMap(), getAddressInputFields());
						creditCard.setBillingAddressNickName(((TRUOrderImpl)getOrder()).getBillingAddressNickName());
					} else if(isEditCreditCard()) {
						Address billingAddress  = ((TRUOrderImpl)getOrder()).getBillingAddress();
						//OrderTools.copyAddress(billingAddress, getAddressInputFields());
						final TRUPropertyManager propertyManager = getPropertyManager();
						if (getCreditCardInfoMap().get(propertyManager.getCreditCardNumberPropertyName()).startsWith(
										getTRUConfiguration().getCardNumberPLCC())) {
							getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationMonthPropertyName(),
									getTRUConfiguration().getExpMonthAdjustmentForPLCC());
							getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationYearPropertyName(),
									getTRUConfiguration().getExpYearAdjustmentForPLCC());
						}
						billingAddressNickName = ((TRUOrderImpl)getOrder()).getBillingAddressNickName();
						getBillingHelper().editCreditCard(getProfile(), getCreditCardInfoMap(), getEditCardNickName(), 
								billingAddressNickName, billingAddress, getPaymentGroupMapContainer());
						CreditCard containerCreditCard = (CreditCard) getPaymentGroupMapContainer().getPaymentGroup(getEditCardNickName());
						TRUCreditCard orderCreditCard = (TRUCreditCard) orderManager.getCreditCard(getOrder());
						getPurchaseProcessHelper().copyCreditCardInfo(containerCreditCard, orderCreditCard);
						if(!getProfile().isTransient()){
							getSetaLogger().logUpdateCreditCard((String)getProfile().getPropertyValue(getPropertyManager().getLoginPropertyName()));
						}
					}

					// START: Promo Financing
					if(!getFormError() && ((TRUOrderImpl)getOrder()).isFinanceAgreed() && isNoFinanceAgreed()) {
						((TRUOrderImpl)getOrder()).setFinanceAgreed(Boolean.FALSE);
						if(getUserSession().isRusCardSelected()) {
							((TRUOrderImpl)getOrder()).getActiveAgreementsVO().get(TRUConstants.TSP_CARD_NUMBER).setFinanceAgreed(TRUConstants.INTEGER_NUMBER_TWO);
						} else {
							TRUCreditCard orderCreditCard = (TRUCreditCard) orderManager.getCreditCard((TRUOrderImpl)getOrder());
							if(orderCreditCard != null) {
								if(orderCreditCard.getCreditCardNumber() != null &&
										null != ((TRUOrderImpl)getOrder()).getActiveAgreementsVO().get(orderCreditCard.getCreditCardNumber()) && 
										TRUConstants.INTEGER_NUMBER_ONE == ((TRUOrderImpl)getOrder()).getActiveAgreementsVO().get(orderCreditCard.getCreditCardNumber()).getFinanceAgreed()) { 
									((TRUOrderImpl)getOrder()).getActiveAgreementsVO().get(orderCreditCard.getCreditCardNumber()).setFinanceAgreed(TRUConstants.INTEGER_NUMBER_TWO);
								}
								if(orderCreditCard.getCreditCardNumber() != null &&
										null == ((TRUOrderImpl)getOrder()).getActiveAgreementsVO().get(orderCreditCard.getCreditCardNumber())) {
									TRUActiveAgreementsVO activeAgreementsVO = null;
									String creditCardNumber = orderCreditCard.getCreditCardNumber();
									if(null != ((TRUOrderImpl)getOrder()).getActiveAgreementsVO().get(creditCardNumber)){
										activeAgreementsVO = ((TRUOrderImpl)getOrder()).getActiveAgreementsVO().get(creditCardNumber);
									} else {
										activeAgreementsVO = new TRUActiveAgreementsVO();
									}
									activeAgreementsVO.setCreditCardNumber(creditCardNumber);
									activeAgreementsVO.setFinanceAgreed(TRUConstants.INTEGER_NUMBER_TWO);
									activeAgreementsVO.setFinancingTermsAvailed(((TRUOrderImpl)getOrder()).getFinancingTermsAvailable());
									((TRUOrderImpl)getOrder()).getActiveAgreementsVO().put(creditCardNumber, activeAgreementsVO);
								}
							}
						}
					} else if(!getFormError() && ((TRUOrderImpl)getOrder()).isFinanceAgreed()) {
						TRUCreditCard orderCreditCard = (TRUCreditCard) orderManager.getCreditCard((TRUOrderImpl)getOrder());
						if(orderCreditCard != null && orderCreditCard.getCreditCardNumber() != null &&
								null == ((TRUOrderImpl)getOrder()).getActiveAgreementsVO().get(orderCreditCard.getCreditCardNumber())) {
							TRUActiveAgreementsVO activeAgreementsVO = new TRUActiveAgreementsVO();
							String creditCardNumber = orderCreditCard.getCreditCardNumber();
							activeAgreementsVO.setCreditCardNumber(creditCardNumber);
							activeAgreementsVO.setFinanceAgreed(TRUConstants.INTEGER_NUMBER_ONE);
							activeAgreementsVO.setFinancingTermsAvailed(((TRUOrderImpl)getOrder()).getFinancingTermsAvailable());
							((TRUOrderImpl)getOrder()).getActiveAgreementsVO().put(creditCardNumber, activeAgreementsVO);
						}
					} else if(!getFormError() && isNoFinanceAgreed()){
						TRUCreditCard orderCreditCard = (TRUCreditCard) orderManager.getCreditCard((TRUOrderImpl)getOrder());
						if(orderCreditCard != null && orderCreditCard.getCreditCardNumber() != null &&
								null != ((TRUOrderImpl)getOrder()).getActiveAgreementsVO().get(orderCreditCard.getCreditCardNumber())) {
							TRUActiveAgreementsVO activeAgreementsVO = new TRUActiveAgreementsVO();
							String creditCardNumber = orderCreditCard.getCreditCardNumber();
							activeAgreementsVO.setCreditCardNumber(creditCardNumber);
							activeAgreementsVO.setFinanceAgreed(TRUConstants.INTEGER_NUMBER_TWO);
							activeAgreementsVO.setFinancingTermsAvailed(((TRUOrderImpl)getOrder()).getFinancingTermsAvailable());
							((TRUOrderImpl)getOrder()).getActiveAgreementsVO().put(creditCardNumber, activeAgreementsVO);
						}
					}
					// END: Promo Financing
					if(getProfile().isTransient()){
						((TRUOrderImpl)getOrder()).setEmail(getEmail());
					} else {
						((TRUOrderImpl) getOrder()).setEmail((String) getProfile().getPropertyValue(
								profileTools.getPropertyManager().getEmailAddressPropertyName()));
					}
					((TRUOrderImpl)getOrder()).setRewardNumber(getRewardNumber());
					profileTools.updateProperty(((TRUPropertyManager) profileTools.getPropertyManager()).getRewardNumberPropertyName(),
							getRewardNumber(), getProfile());
					//Validate order remaining amount.
					validateOrderRemainingAmount(pRequest, pResponse);
					//Calling the moveToConfirmation chain.
					if (!getFormError()) {
						processMoveToConfirmation(pRequest, pResponse);
					}
					if (!getFormError()) {
						((TRUOrderImpl) getOrder()).setRusCardSelected(isRUSCard());
						((TRUOrderImpl) getOrder()).getActiveTabs().put(TRUCheckoutConstants.PAYMENT_TAB, Boolean.TRUE);
						((TRUOrderImpl) getOrder()).getActiveTabs().put(TRUCheckoutConstants.REVIEW_TAB, Boolean.TRUE);
					}
					postMoveToOrderReview(pRequest, pResponse);
					runProcessRepriceOrder(getOrder(), getUserPricingModels(), getUserLocale(), getProfile(), createRepriceParameterMap());
					orderManager.updateOrder(getOrder());
				}
			} catch (CommerceException comExp) {
				hasException=true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handleMoveToOrderReview()", comExp);
				}
			} catch (RunProcessException runProcExp) {
				hasException=true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handleMoveToOrderReview()", runProcExp);
				}
			} catch (RepositoryException e) {
				hasException=true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handleMoveToOrderReview()", e);
				}
			} finally {				
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (javax.transaction.SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in @Class::TRUBillingInfoFormHandler::@method::handleEnsurePaymentGroup()", e);
							}
						}
				}
					commitTransaction(tr);
				}
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.endOperation(myHandleMethod);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUBillingInfoFormHandler.handleMoveToOrderReview method");
		}
		return checkFormRedirect(getCommonSuccessURL(),	getCommonErrorURL(), pRequest, pResponse);
	}
	
	/**
	 * This method is to set selected credit card nickname in profile 
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	protected void postMoveToOrderReview(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUBillingInfoFormHandler/postMoveToOrderReview method");
		}
		if(isRestService()&&!isOrderSetWithDefault()){
			Profile profile = (Profile)getProfile();
			String selectedCCNickName = (String)profile.getPropertyValue(getPropertyManager().getSelectedCCNickNamePropertyName());
			
			if(profile.isTransient()){
				if(StringUtils.isBlank(selectedCCNickName)){
					String defaultCreditCardName = getPaymentGroupMapContainer().getDefaultPaymentGroupName();
					profile.setPropertyValue(getPropertyManager().getSelectedCCNickNamePropertyName(),defaultCreditCardName);
				}
			}else{
				RepositoryItem defaultCreditCard = getOrderTools().getProfileTools().getDefaultCreditCard(getProfile());
				if (StringUtils.isBlank(selectedCCNickName) && defaultCreditCard != null) {
					String profileCCNickName = getOrderTools().getProfileTools().getCreditCardNickname(profile, defaultCreditCard);
					if (StringUtils.isNotBlank(profileCCNickName)) {
						profile.setPropertyValue(getPropertyManager().getSelectedCCNickNamePropertyName(),profileCCNickName);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUBillingInfoFormHandler/postMoveToOrderReview method");
		}
	}

	/**
	 * Pre move to review order validation for Gift Card.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	protected void preMoveToReviewOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUBillingInfoFormHandler/preMoveToOrderReview method");
		}
		try {
			if(getProfile().isTransient()){
				/*To validate all mandatory fields including email when user is not logged in along with email format*/
				if (StringUtils.isBlank(getEmail())) {
					if (isLoggingDebug()) {
						vlogDebug("TRUBillingInfoFormHandler.preMoveToOrderReview is having errors");
					}
					getValidator().validate(TRUConstants.MY_ACCOUNT_UPDATE_EMAIL_FORM,this);
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
					getErrorHandler().processException(mVe, this);
				}
				if (!getFormError() && StringUtils.isNotBlank(getEmail()) && !getEmailPasswordValidator().validateEmail(getEmail())) {
					// to validate email format
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_INVALID_EMAIL, null);
					getErrorHandler().processException(mVe, this);
				}
			} 
			if(!getFormError() && StringUtils.isNotEmpty(getRewardNumber()) && getRewardNumberValid().equalsIgnoreCase(TRUConstants.FALSE)){
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_REWARD_NUMBER_INVALID, null);   
				getErrorHandler().processException(mVe, this);
			}
			//Validate order remaining amount.
			if(!getFormError()){
				validateOrderRemainingAmount(pRequest, pResponse);
			}
			
		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			if (isLoggingError()) {
				logError("Validation exception occured in TRUBillingInfoFormHandler.preMoveToReviewOrder()", ve);
			}
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			if (isLoggingError()) {
				logError("System exception occured TRUBillingInfoFormHandler.preMoveToReviewOrder()", se);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUBillingInfoFormHandler/preMoveToOrderReview method");
		}
	}
	
	/**
	 * Handle in store payment to review order.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 * @throws SystemException SystemException
	 *
	 */
	public boolean handleInStorePaymentToReviewOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, SystemException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUBillingInfoFormHandler.handleInStorePaymentToReviewOrder method");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "TRUBillingInfoFormHandler.handleInStorePaymentToReviewOrder";
		boolean hasException=false;
		boolean isEnableAddressDoctor = false;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			resetFormExceptions();
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					preMoveToReviewOrder(pRequest, pResponse);
					
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in handleInStorePaymentToReviewOrder() method");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					
					boolean isAddressValidated = Boolean.parseBoolean(getAddressValidated());
					if (!TRUConstants.UNITED_STATES.equalsIgnoreCase(getAddressInputFields().getCountry())){
						isAddressValidated = true;
					}
					if(SiteContextManager.getCurrentSite() != null){
						isEnableAddressDoctor = getIntegrationStatusUtil().getAddressDoctorIntStatus(pRequest);
					}
					
					// Validate address with AddressDoctor Service
					isAddressValidated = setAddressFromAddressDoctorResponse(isAddressValidated,isEnableAddressDoctor);
					
					if(getFormError() || !isAddressValidated) {
						if (isLoggingDebug()) {
							vlogDebug("End of TRUBillingInfoFormHandler.handleInStorePaymentToReviewOrder method");
						}
						return checkFormRedirect(getCommonSuccessURL(),	getCommonErrorURL(), pRequest, pResponse);
					}
					
					if(!getFormError()){
						TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
						TRUProfileTools profileTools = (TRUProfileTools) orderManager.getOrderTools().getProfileTools();
						TRUPropertyManager propertyManager = (TRUPropertyManager) profileTools.getPropertyManager();
						if(getProfile().isTransient()){
							((TRUOrderImpl)getOrder()).setEmail(getEmail());
						} else {
							((TRUOrderImpl) getOrder()).setEmail((String) getProfile().getPropertyValue(
									propertyManager.getEmailAddressPropertyName()));
						}
						((TRUOrderImpl)getOrder()).setRewardNumber(getRewardNumber());
						
						String billingAddressNickName = getBillingAddressNickName();
						if(isNewBillingAddress()) {
							ContactInfo billingAddress  = new ContactInfo();
							OrderTools.copyAddress(getAddressInputFields(), billingAddress);
							//Adding the billing address to the credit card. 
							((TRUOrderImpl)getOrder()).setBillingAddress(billingAddress);
							String generatedUniqueNickName = orderManager.generateUniqueNickNameForAddress(getAddressInputFields(), getShippingGroupMapContainer().getShippingGroupMap());
							String nickName = null;
							if(generatedUniqueNickName.length() > TRUConstants.NICKNAME_MAX_LENGTH){
								nickName = generatedUniqueNickName.substring(TRUConstants.NICKNAME_MIN_LENGTH, TRUConstants.NICKNAME_MAX_LENGTH);
							} else {
								nickName = generatedUniqueNickName;
							}
							((TRUOrderImpl)getOrder()).setBillingAddressNickName(nickName);
							final ShippingGroupManager sgm = getPurchaseProcessHelper().getShippingGroupManager();
							final TRUHardgoodShippingGroup hgsg = (TRUHardgoodShippingGroup) sgm.createShippingGroup();	   
							final Address address = hgsg.getShippingAddress();	    
							OrderTools.copyAddress(getAddressInputFields(), address);
							hgsg.setBillingAddress(Boolean.TRUE);
							hgsg.setNickName(nickName);
							billingAddressNickName = nickName;
							hgsg.setLastActivity(new Timestamp((new Date()).getTime()));
							getShippingGroupMapContainer().addShippingGroup(nickName, hgsg);
							setBillingAddressNickName(nickName);
							if(!getProfile().isTransient()){
								getSetaLogger().logAddBillingAddress((String)getProfile().getPropertyValue(getPropertyManager().getLoginPropertyName()), billingAddressNickName );
							}
						} else if (isEditBillingAddress()){
							Address billingAddress  = ((TRUOrderImpl)getOrder()).getBillingAddress();
							OrderTools.copyAddress(getAddressInputFields(), billingAddress);
							//Adding the billing address to the credit card. 
							billingAddressNickName = ((TRUOrderImpl)getOrder()).getBillingAddressNickName();
							final TRUHardgoodShippingGroup containerShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupMapContainer().
									getShippingGroup(billingAddressNickName);  
							setBillingAddressNickName(billingAddressNickName);
							OrderTools.copyAddress(getAddressInputFields(), containerShippingGroup.getShippingAddress());
							if(!getProfile().isTransient()){
								getSetaLogger().logUpdateBillingAddress((String)getProfile().getPropertyValue(getPropertyManager().getLoginPropertyName()), billingAddressNickName);
							}
						}
					    
						profileTools.updateProperty(((TRUPropertyManager) profileTools.getPropertyManager()).getRewardNumberPropertyName(),
								getRewardNumber(), getProfile());
						if (!getFormError()) {
							((TRUOrderImpl) getOrder()).getActiveTabs().put(TRUCheckoutConstants.PAYMENT_TAB, Boolean.TRUE);
							((TRUOrderImpl) getOrder()).getActiveTabs().put(TRUCheckoutConstants.REVIEW_TAB, Boolean.TRUE);
						}
						processMoveToConfirmation(pRequest, pResponse);
						runProcessRepriceOrder(getOrder(),getUserPricingModels(), getUserLocale(),getProfile(), createRepriceParameterMap());
						orderManager.updateOrder(getOrder());
					}
					if (getFormError()) {
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
				}
			} catch (CommerceException comExp) {
				hasException=true;
				if (isLoggingError()) {
					logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handleInStorePaymentToReviewOrder()", comExp);
				}
			} catch (RunProcessException runProcExp) {
				hasException=true;
				if (isLoggingError()) {
					logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handleInStorePaymentToReviewOrder()", runProcExp);
				}
			} catch (RepositoryException e) {
				hasException=true;
				if (isLoggingError()) {
					logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handleInStorePaymentToReviewOrder()", e);
				}
			}  finally {
				/*String taxwareErrorMsg=((TRUOrderImpl)getOrder()).getTaxwareErroMessage();
				if(StringUtils.isNotBlank(taxwareErrorMsg)){
					taxwareException(taxwareErrMessage);
					((TRUOrderImpl)getOrder()).setTaxwareErroMessage(null);
					if(isLoggingError()) {
						logError("Taxware Error Message "
								+ "@Class::TRUBillingInfoFormHandler::@method::handleInStorePaymentToReviewOrder() :"+ taxwareErrorMsg);
					}	
				}*/
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (javax.transaction.SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in @Class::TRUBillingInfoFormHandler::@method::handleInStorePaymentToReviewOrder()", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUBillingInfoFormHandler.handleInStorePaymentToReviewOrder method");
		}
		return checkFormRedirect(getCommonSuccessURL(),	getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * Handle gift card payment to review order.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleGiftCardPaymentToReviewOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUBillingInfoFormHandler.handleGiftCardPaymentToReviewOrder method");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "TRUBillingInfoFormHandler.handleGiftCardPaymentToReviewOrder";
		//String login = (String)getProfile().getPropertyValue(getPropertyManager().getLoginPropertyName());
		boolean hasException=false;
		boolean isEnableAddressDoctor = false;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			resetFormExceptions();
			try {
				 if (PerformanceMonitor.isEnabled()){
						PerformanceMonitor.startOperation(myHandleMethod);
					}
				tr = ensureTransaction();
				TRUOrderImpl order = (TRUOrderImpl) getOrder();
				TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
				TRUProfileTools profileTools = (TRUProfileTools) orderManager.getOrderTools().getProfileTools();
				TRUPropertyManager propertyManager = (TRUPropertyManager) profileTools.getPropertyManager();
				synchronized (order) {
					preMoveToReviewOrder(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in handleGiftCardPaymentToReviewOrder() method");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					
					boolean isAddressValidated = Boolean.parseBoolean(getAddressValidated());
					if (!TRUConstants.UNITED_STATES.equalsIgnoreCase(getAddressInputFields().getCountry())){
						isAddressValidated = true;
					}
					if(SiteContextManager.getCurrentSite() != null){
						isEnableAddressDoctor = getIntegrationStatusUtil().getAddressDoctorIntStatus(pRequest);
					}
					
					// Validate address with AddressDoctor Service
					isAddressValidated = setAddressFromAddressDoctorResponse(isAddressValidated,isEnableAddressDoctor);
					
					if(getFormError() || !isAddressValidated) {
						if (isLoggingDebug()) {
							vlogDebug("End of TRUBillingInfoFormHandler.handleInStorePaymentToReviewOrder method");
						}
						return checkFormRedirect(getCommonSuccessURL(),	getCommonErrorURL(), pRequest, pResponse);
					}
					
					if(isNewBillingAddress()) {
						String billingAddressNickName = getBillingAddressNickName();
						ContactInfo billingAddress  = new ContactInfo();
						OrderTools.copyAddress(getAddressInputFields(), billingAddress);
						//Adding the billing address to the credit card. 
						((TRUOrderImpl)getOrder()).setBillingAddress(billingAddress);
						String generatedUniqueNickName = orderManager.generateUniqueNickNameForAddress(getAddressInputFields(), getShippingGroupMapContainer().getShippingGroupMap());
						String nickName = null;
						if(generatedUniqueNickName.length() > TRUConstants.NICKNAME_MAX_LENGTH){
							nickName = generatedUniqueNickName.substring(TRUConstants.NICKNAME_MIN_LENGTH, TRUConstants.NICKNAME_MAX_LENGTH);
						} else {
							nickName = generatedUniqueNickName;
						}
						((TRUOrderImpl)getOrder()).setBillingAddressNickName(nickName);
						final ShippingGroupManager sgm = getPurchaseProcessHelper().getShippingGroupManager();
						final TRUHardgoodShippingGroup hgsg = (TRUHardgoodShippingGroup) sgm.createShippingGroup();	   
						final Address address = hgsg.getShippingAddress();	    
						OrderTools.copyAddress(getAddressInputFields(), address);
						hgsg.setBillingAddress(Boolean.TRUE);
						hgsg.setNickName(nickName);
						hgsg.setLastActivity(new Timestamp((new Date()).getTime()));
						getShippingGroupMapContainer().addShippingGroup(nickName, hgsg);
						if(!getProfile().isTransient()){
							getSetaLogger().logAddBillingAddress((String)getProfile().getPropertyValue(getPropertyManager().getLoginPropertyName()), billingAddressNickName );
						}
					} else if (isEditBillingAddress()){
						Address billingAddress  = ((TRUOrderImpl)getOrder()).getBillingAddress();
						OrderTools.copyAddress(getAddressInputFields(), billingAddress);
						//Adding the billing address to the credit card. 

						String billingAddressNickName = ((TRUOrderImpl)getOrder()).getBillingAddressNickName();
						final TRUHardgoodShippingGroup containerShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupMapContainer().
								getShippingGroup(billingAddressNickName);   
						OrderTools.copyAddress(getAddressInputFields(), containerShippingGroup.getShippingAddress());
						if(!getProfile().isTransient()){
							getSetaLogger().logUpdateBillingAddress((String)getProfile().getPropertyValue(getPropertyManager().getLoginPropertyName()), billingAddressNickName);
						}
					}
					
					processMoveToConfirmation(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Redirecting due to form error in handleGiftCardPaymentToReviewOrder");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					order.setRewardNumber(getRewardNumber());
					profileTools.updateProperty(((TRUPropertyManager) profileTools.getPropertyManager()).getRewardNumberPropertyName(),
							getRewardNumber(), getProfile());
					if(getProfile().isTransient()){
						order.setEmail(getEmail());
					} else {
						order.setEmail((String) getProfile().getPropertyValue(propertyManager.getEmailAddressPropertyName()));
					}
					order.getActiveTabs().put(TRUCheckoutConstants.PAYMENT_TAB, Boolean.TRUE);
					order.getActiveTabs().put(TRUCheckoutConstants.REVIEW_TAB, Boolean.TRUE);
					runProcessRepriceOrder(order,getUserPricingModels(), getUserLocale(),getProfile(), createRepriceParameterMap());
					orderManager.updateOrder(order);
				}
			} catch (CommerceException comExp) {
				hasException=true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handleGiftCardPaymentToReviewOrder()", comExp);
				}
			} catch (RunProcessException runProcExp) {
				hasException=true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handleGiftCardPaymentToReviewOrder()", runProcExp);
				}
			} catch (RepositoryException e) {
				hasException=true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handleGiftCardPaymentToReviewOrder()", e);
				}
			}  finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (javax.transaction.SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in @Class::TRUBillingInfoFormHandler::@method::handleInStorePaymentToReviewOrder()", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.endOperation(myHandleMethod);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUBillingInfoFormHandler.handleGiftCardPaymentToReviewOrder method");
		}
		return checkFormRedirect(getCommonSuccessURL(),	getCommonErrorURL(), pRequest, pResponse);
	}
	
	   /**
   	 * Process move to confirmation.
   	 *
   	 * @param pRequest DynamoHttpServletRequest
   	 * @param pResponse DynamoHttpServletResponse
   	 * @throws RunProcessException RunProcessException
   	 * @throws ServletException ServletException
   	 * @throws IOException IOException
   	 */
		@SuppressWarnings({ "rawtypes", "unchecked" })
		private void processMoveToConfirmation(DynamoHttpServletRequest pRequest,
				DynamoHttpServletResponse pResponse) throws RunProcessException,
				ServletException, IOException {
			if (isLoggingDebug()) {
				vlogDebug("Start of TRUBillingInfoFormHandler.processMoveToConfirmation method");
			}		
			Map params = new HashMap();			
			params.put(TRUCheckoutConstants.PAYMENTG_GROUP_MANAGER,getPaymentGroupManager());
		PipelineResult result = runProcess(getMoveToConfirmChainId(), getOrder(), getUserPricingModels(),
				getUserLocale(pRequest, pResponse), getProfile(), params, null);
		    processPipelineErrors(result);
			if (isLoggingDebug()) {
				vlogDebug("End of TRUBillingInfoFormHandler.processMoveToConfirmation method");
			}
		}

	/**
	 * This method is to check the validity of the credit card and billing
	 * information.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	protected void preMoveToOrderReview(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUBillingInfoFormHandler/preMoveToOrderReview method");
		}
		try {
			double giftCardAmtApplied = ((TRUOrderTools)(getOrderManager().getOrderTools())).getGiftCardAppliedAmount(getOrder());
			double orderTotal = ((TRUOrderTools)(getOrderManager().getOrderTools())).getPricingTools().round(getOrder().getPriceInfo().getTotal());
			double balnceAmount = orderTotal-giftCardAmtApplied;
			if(TRUCheckoutConstants.CREDITCARD.equalsIgnoreCase(getPaymentGroupType()) && balnceAmount != TRUCommerceConstants.DOUBLE_ZERO) {
				TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
				if(!isOrderSetWithDefault()){
					TRUPropertyManager propertyManager = getPropertyManager();
					if (isRestService()
							&& getCreditCardInfoMap().get(propertyManager.getCreditCardNumberPropertyName()).equals(
									getTRUConfiguration().getCardNumberPLCC())) {
						getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationMonthPropertyName(),
								getTRUConfiguration().getExpMonthAdjustmentForPLCC());
						getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationYearPropertyName(),
								getTRUConfiguration().getExpYearAdjustmentForPLCC());
					}
					
					
					if(getProfile().isTransient()){
						/*To validate all mandatory fields including email when user is not logged in along with email format*/
						if (orderManager
								.isAnyCreditCardFieldEmpty(getCreditCardInfoMap()) || orderManager.
								isAnyAddressFieldEmpty(getAddressInputFields()) || StringUtils.isBlank(getEmail())) {
							if (isLoggingDebug()) {
								vlogDebug("TRUBillingInfoFormHandler.preMoveToOrderReview is having errors");
							}
							getValidator().validate(TRUConstants.MY_ACCOUNT_ADD_CARD_WITHOUT_ADDR_FORM,this);
							mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
							getErrorHandler().processException(mVe, this);
						}
						if (!getFormError() && StringUtils.isNotBlank(getEmail())
								&& !getEmailPasswordValidator().validateEmail(getEmail())) {
							// to validate email format
							mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_INVALID_EMAIL, null);
							getErrorHandler().processException(mVe, this);
						}
					} else {
						/*To validate all mandatory fields when user is logged in*/
						if (orderManager.isAnyCreditCardFieldEmpty(getCreditCardInfoMap())
								|| orderManager.isAnyAddressFieldEmpty(getAddressInputFields())) {
							if (isLoggingDebug()) {
								vlogDebug("TRUBillingInfoFormHandler.preMoveToOrderReview is having errors");
							}
							getValidator().validate(TRUConstants.MY_ACCOUNT_ADD_CARD_WITHOUT_ADDR_FORM,this);
							mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
							getErrorHandler().processException(mVe, this);
						}
					}
					if (!getFormError()) {
						//Trim all the input values from the map
						trimContactInfoValues(getAddressInputFields());
						((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).trimMapValues(getCreditCardInfoMap());
						validateCreditCard();
					}	
				} else {
					// validates order credit card
					isOrderCreditCardExpired();
				}
			}
			if(!getFormError() && StringUtils.isNotEmpty(getRewardNumber()) && getRewardNumberValid().equalsIgnoreCase(TRUConstants.FALSE)){
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_REWARD_NUMBER_INVALID, null);   
				getErrorHandler().processException(mVe, this);
			}
			if(getFormError()){
				return;
			}
		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			if (isLoggingError()) {
				logError("Validation exception occured @Class:::TRUBillingInfoFormHandler:::#method::preMoveToOrderReview()", ve);
			}
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			if (isLoggingError()) {
				logError("System exception occured@Class:::TRUBillingInfoFormHandler:::#method::preMoveToOrderReview()", se);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUBillingInfoFormHandler/preMoveToOrderReview method");
		}
	}

	/**
	 * This custom method is used to set the credit card item into edit value map, when user clicks on edit link from the payment info page.
	 * @param pRequest -- request
	 * @param pResponse -- response
	 * @return true/ false.
	 * @throws ServletException -- servlet exception
	 * @throws IOException -- IO exception
	 *//*
	public boolean handleEditCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) 
			throws ServletException, IOException {
		if(isLoggingDebug()){
			vlogDebug("TRUBilingInfoFormHandler.handleEditCreditCard() method.STARTS");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "TRUBillingInfoFormHandler.handleEditCreditCard";
		boolean hasException=false;
		Transaction tr = null;
		boolean isEnableAddressDoctor = false;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			try {
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.startOperation(myHandleMethod);
				}
				preEditCreditCard(pRequest, pResponse);
				if (getFormError()) {
					if (isLoggingDebug()) {
						vlogDebug("Redirecting due to form error in preEditCreditCard.");
					}
					return checkFormRedirect(getPaymentInfoSuccessURL(), getPaymentInfoErrorURL(), pRequest, pResponse);
				}
				boolean isAddressValidated = Boolean.parseBoolean(getAddressValidated());
				if (TRUConstants.UNITED_STATES.equalsIgnoreCase(getAddressInputFields()
						.getCountry())) {
					if(SiteContextManager.getCurrentSite() != null){
						isEnableAddressDoctor = (boolean) SiteContextManager.getCurrentSite().getPropertyValue(getPropertyManager().getEnableAddressDoctorPropertyName());
						isEnableAddressDoctor = getIntegrationStatusUtil().getAddressDoctorIntStatus(pRequest);
						}
					// Validate address with AddressDoctor Service
						if(isEnableAddressDoctor){
							isAddressValidated = Boolean.parseBoolean(getAddressValidated());
						}
					
					
					// Validate address with AddressDoctor Service
					//Changed for excessive method length PMD fix
						isAddressValidated = setAddressFromAddressDoctorResponse(isAddressValidated,isEnableAddressDoctor);
					
				}
				if (isAddressValidated	|| !TRUConstants.UNITED_STATES.equalsIgnoreCase(getAddressInputFields().getCountry())) {
					tr = ensureTransaction();
					synchronized (getOrder()) {
						editCreditCard(pRequest, pResponse);
						((TRUProfileTools) getOrderTools().getProfileTools()).setProfileDefaultAddress(getProfile());
						postEditCreditCard(pRequest, pResponse);
						getOrderManager().updateOrder(getOrder());
					}
				}
			 } catch (RepositoryException repoEx) {
				 hasException=true;
				 if (isLoggingError()) {
					 vlogError("RepositoryException occured while updating credit card with exception "
					 		+ "@Class::TRUBillingInfoFormHandler::@method::handleEditCreditCard()", repoEx);
				 }
				PerformanceMonitor.cancelOperation(myHandleMethod);
			 } catch (CommerceException cExe) {
				 hasException=true;
				 if (isLoggingError()) {
					 vlogError("CommerceException occured while updating credit card with exception "
					 		+ "@Class::TRUBillingInfoFormHandler::@method::handleEditCreditCard()", cExe);
				 }
				PerformanceMonitor.cancelOperation(myHandleMethod);
			} catch (IntrospectionException iExe) {
				hasException=true;
				 if (isLoggingError()) {
					 vlogError("IntrospectionException occured while updating credit card with exception "
					 		+ "@Class::TRUBillingInfoFormHandler::@method::handleEditCreditCard()", iExe);
				 }
				PerformanceMonitor.cancelOperation(myHandleMethod);
			}finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (javax.transaction.SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in @Class::TRUBillingInfoFormHandler::@method::handleEditCreditCard()", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.endOperation(myHandleMethod);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		return checkFormRedirect(getPaymentInfoSuccessURL(), getPaymentInfoErrorURL(), pRequest, pResponse);
	}*/
	

	/**
	 * Handle update order with existing card.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleUpdateOrderWithExistingCard(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start of handleMoveToOrderReview method");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "TRUBillingInfoFormHandler.handleUpdateOrderWithExistingCard";
		boolean hasException=false;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			resetFormExceptions();
			try {
				 if (PerformanceMonitor.isEnabled()){
						PerformanceMonitor.startOperation(myHandleMethod);
					}
				tr = ensureTransaction();
				synchronized (getOrder()) {
					if (!getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Start of handleUpdateOrderWithExistingCard method with cardNickname ",getCreditCardNickName());
						}
						String creditCardName = getCreditCardNickName();
						if (StringUtils.isNotBlank(creditCardName)) {
							TRUCreditCard creditCard = (TRUCreditCard) getPaymentGroupMapContainer().getPaymentGroup(creditCardName);
							if (creditCard != null) {
								TRUProfileManager profileManager = getBillingHelper().getProfileManager();
								getBillingHelper().changeCreditCardInOrder(getOrder(), creditCard, getPaymentGroupMapContainer());
								
								Profile profile = (Profile)getProfile();
								//Start :: TUW-43619/46853 defect changes.
								profile.setPropertyValue(getPropertyManager().getSelectedCCNickNamePropertyName(), creditCardName);
								getPaymentGroupMapContainer().setDefaultPaymentGroupName(creditCardName);
								//End :: TUW-43619/46853 defect changes.
								boolean isCardExpired = profileManager.validateCardExpiration(
										creditCard.getExpirationMonth(), creditCard.getExpirationYear());
								
								if(isCardExpired) {
									mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ORDER_CREDIT_CARD_EXPIRED, null);
									getErrorHandler().processException(mVe, this);
								}
							}
							PricingModelHolder userPricingModels = getUserPricingModels();
							userPricingModels.initializePricingModels();
							runProcessRepriceOrder(getOrder(), userPricingModels, getUserLocale(pRequest, pResponse),
									getProfile(), createRepriceParameterMap());
							if(isGiftCardsExist()) {
								((TRUOrderManager)getOrderManager()).adjustPaymentGroups(getOrder());
							}
							
							
							
							getOrderManager().updateOrder(getOrder());
						}
					}
				}
			} catch (CommerceException comExp) {
				hasException=true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handleUpdateOrderWithExistingCard()", comExp);
				}
			} catch (RunProcessException rpcExp) {
				hasException=true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handleUpdateOrderWithExistingCard()", rpcExp);
				}
			}  finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (javax.transaction.SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in @Class::TRUBillingInfoFormHandler::@method::handleUpdateOrderWithExistingCard()", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.endOperation(myHandleMethod);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of handleUpdateOrderWithExistingCard method");
		}
		return checkFormRedirect(getCommonSuccessURL(),	getCommonErrorURL(), pRequest, pResponse);
	}
	/**
	 * add credit card.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 * @throws CommerceException  CommerceException
	 */
	protected void addCreditCard(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUBillingInfoFormHandler.addCreditCard method");
		}
		String billingAddressNickName=getBillingAddressNickName();
		String login = (String)getProfile().getPropertyValue(getPropertyManager().getLoginPropertyName());
		TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		TRUPropertyManager propertyManager = (TRUPropertyManager) profileTools.getPropertyManager();

		// updating credit card type
		String creditCardType = TRUConstants.EMPTY;
		if (!StringUtils.isBlank((String) getCreditCardInfoMap().get(TRUConstants.CARD_TYPE))
				&& !TRUConstants.CARD_TYPE_UNDEFINED.equalsIgnoreCase((String) getCreditCardInfoMap().get(
						TRUConstants.CARD_TYPE))) {
			creditCardType = (String) getCreditCardInfoMap().get(TRUConstants.CARD_TYPE);
		} else {
			creditCardType = profileTools.getCreditCardTypeFromRepository(
					(String) getCreditCardInfoMap().get(propertyManager.getCreditCardNumberPropertyName()),
					(String) getCreditCardInfoMap().get(TRUConstants.CARD_LENGTH));
		}
		if (isLoggingDebug()) {
			vlogDebug("value of credit card type calculated from getOrderTools().getCreditCardType() creditCardType: {0} ", creditCardType);
		}
		getCreditCardInfoMap().put(propertyManager.getCreditCardTypePropertyName(), creditCardType);
		getBillingHelper().addCreditCard(getOrder(), getProfile(), getCreditCardInfoMap(), billingAddressNickName, 
				((TRUOrderImpl)getOrder()).getBillingAddress(), getPaymentGroupMapContainer(), false);
		if (!getProfile().isTransient()) {
			getSetaLogger().logAddCreditCard(login);
		}
		if (isLoggingDebug()) {
			vlogDebug("END of TRUBillingInfoFormHandler.addCreditCard method");
		}
	}
	
	/**
	 * This method is for the actions to be performed after adding credit card.
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 */
	protected void postAddCreditCard(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {
		//Added this method for future implementations.
	}
	
	
	/**
	 * pre method for handleEditCreditCard.
	 * @param pRequest -- request
	 * @param pResponse - response
	 * @throws ServletException	-	if any.
	 * @throws IOException	-	if any.
	 */
	public void preEditCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
	throws ServletException, IOException{
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUBillingInfoFormHandler:preUpdateCreditCard method");
		}
		try {
			TRUPropertyManager propertyManager = ((TRUOrderManager) getOrderManager()).getPropertyManager();
			String creditCardType = null;
			// moved fetching creditcardtype
			if (getProfile().isTransient()) {
				CreditCard creditCard = (CreditCard) getPaymentGroupMapContainer().getPaymentGroup(
						getEditCardNickName());
				creditCardType = getBillingHelper().getCCTypeFromPaymentGroup(creditCard);
			} else {
				RepositoryItem creditCard = ((TRUProfileTools) getOrderTools().getProfileTools())
						.getCreditCardByNickname(getEditCardNickName(), getProfile());
				creditCardType = getBillingHelper().getCreditCardType(creditCard);
			}
			
			getCreditCardInfoMap().put(propertyManager.getCreditCardTypePropertyName(), creditCardType);
			if (StringUtils.isNotBlank(creditCardType)
					&& creditCardType.equals(TRUCheckoutConstants.RUS_PRIVATE_LABEL_CARD)) {
				getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationMonthPropertyName(),
						getTRUConfiguration().getExpMonthAdjustmentForPLCC());
				getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationYearPropertyName(),
						getTRUConfiguration().getExpYearAdjustmentForPLCC());
			}
			
			if(getAddressInputFields().getState().isEmpty() && getAddressInputFields().getCountry() != TRUConstants.UNITED_STATES){
				getAddressInputFields().setState(TRUConstants.NA);
			}
			
			getValidator().validate(TRUConstants.MY_ACCOUNT_ADD_CARD_WITH_ADDR_FORM, this);
			if (StringUtils.isBlank(getExpirationMonth()) || StringUtils.isBlank(getExpirationYear())
					|| StringUtils.isBlank(getBillingAddressNickName())
					|| StringUtils.isBlank(getAddressInputFields().getAddress1())
					|| StringUtils.isBlank(getAddressInputFields().getCity())
					|| StringUtils.isBlank(getAddressInputFields().getState())
					|| StringUtils.isBlank(getAddressInputFields().getPostalCode())
					|| StringUtils.isBlank(getAddressInputFields().getCountry())
					|| StringUtils.isBlank(getBillingHelper().getAddressPhoneNumber(getAddressInputFields()))
					|| StringUtils.isBlank((String) (getCreditCardInfoMap()).get(TRUConstants.CREDIT_CARD_CVV))) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
				getErrorHandler().processException(mVe, this);
			} else {
				String nickName = getBillingAddressNickName();
				if(isRestService() && StringUtils.isNotBlank(getBillingAddressNickName()) ) {
					boolean invalidNkName =  getStoreUtils().containsSpecialCharacter(getBillingAddressNickName());
					if(invalidNkName) {
						mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDR_INVALID_NICKNAME, null);
						getErrorHandler().processException(mVe, this);
						return;
					}
				}
				TRUProfileManager profileManager = getBillingHelper().getProfileManager();
				boolean isCardExpired = profileManager.validateCardExpiration(
						getCreditCardInfoMap().get(TRUConstants.EXPIRATION_MONTH),
						getCreditCardInfoMap().get(TRUConstants.EXPIRATION_YEAR));
				boolean isDuplicateNickName = profileManager.validateDuplicateAddressNickname(getProfile(), nickName);
				if (StringUtils.isBlank(getSelectedCardBillingAddress()) && isDuplicateNickName) {
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CARD_DUPLICATE_ADDR_NICKNAME, null);
					getErrorHandler().processException(mVe, this);
				} else if (isCardExpired) {
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CARD_EXPIRED, null);
					getErrorHandler().processException(mVe, this);
				}
			}
			// Calling the Zero authorization for card verification.
			if (!getFormError() && getOrderTools().isPayeezyEnabled()) {
				//method to Trim the input field values.
				trimContactInfoValues(getAddressInputFields());
				((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).trimMapValues(getCreditCardInfoMap());
				
				if (StringUtils.isBlank(getCreditCardInfoMap().get(propertyManager.getCreditCardTokenPropertyName()))) {
					getCreditCardInfoMap().put(propertyManager.getCreditCardTokenPropertyName(),
							getCreditCardInfoMap().get(propertyManager.getCreditCardNumberPropertyName()));
				}
				if (!StringUtils.isBlank(getEmail())) {
					getAddressInputFields().setEmail(getEmail());
				} else {
					getAddressInputFields().setEmail(((TRUOrderImpl) getOrder()).getEmail());
				}
			}
		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			if (isLoggingError()) {
				vlogError("Validation exception occured: @Class::TRUBillingInfoFormHandler::@method::preEditCreditCard()", ve);
			}
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			if (isLoggingError()) {
				vlogError("System exception occured: @Class::TRUBillingInfoFormHandler::@method::preEditCreditCard()", se);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUBillingInfoFormHandler:preUpdateCreditCard method");
		}
		
	
	}
	/**
	 * post method for handleEditCreditCard.
	 * @param pRequest -- request
	 * @param pResponse - response
	 * @throws ServletException	-	if any.
	 * @throws IOException	-	if any.
	 */
	public void postEditCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
	throws ServletException, IOException{
		//Added this method for future implementations.
	}
	
	
	/**
	 * This pre method used to set the cart type property in profile with the selected card type.
	 *
	 * @param pRequest - The servlet's request
	 * @param pResponse - The servlet's response
	 * @throws IOException - If an error occurred while reading or writing the DynamoServlet request
	 * @throws ServletException - If an error occurred while processing the DynamoServlet request
	 */
	public void preUpdateCardDataInOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN :: TRUBillingInforFormHandler.preUpdateCardDataInOrder method..");
		}
		// Updating profile payByCoBrandedFlag to apply the promotions based on the flag
		 ((TRUOrderManager) getOrderManager()).updateCardDataInOrder(getOrder(), getCreditCardType());
		if (isLoggingDebug()) {
			vlogDebug("END :: TRUBillingInforFormHandler.preUpdateCardDataInOrder method..");
		}
	}

	
	
	/**
	 * This handle method used to select if the raffle order used co branded card for the payment.
	 * If user select true then need to update the profile property to and repeice the order to get the configured promotion for the user.
	 *
	 * @param pRequest - The servlet's request
	 * @param pResponse - The servlet's response
	 * @return boolean - Boolean flag
	 * @throws IOException - If an error occurred while reading or writing the DynamoServlet request
	 * @throws ServletException - If an error occurred while processing the DynamoServlet request
	 */
	public boolean handleUpdateCardDataInOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		if (isLoggingDebug()) {
			logDebug("BEGIN :: TRUBillingInforFormHandler.handleUpdateCardDataInOrder method..");
		}
		boolean hasException=false;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRUCommerceConstants.HANDLE_UPDATE_CARD_DATA_IN_ORDER))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				try {
					synchronized (getOrder()) {
						preUpdateCardDataInOrder(pRequest, pResponse);
						if (!getFormError()) {
							PricingModelHolder userPricingModels = getUserPricingModels();
							userPricingModels.initializePricingModels();
							runProcessRepriceOrder(getOrder(), userPricingModels, getUserLocale(),
									getProfile(), createRepriceParameterMap());
							if(isGiftCardsExist()) {
								((TRUOrderManager)getOrderManager()).adjustPaymentGroups(getOrder());
							}
							getOrderManager().updateOrder(getOrder());
						}
					}
				} catch (RunProcessException e) {
					hasException=true;
					processException(e, TRUCommerceConstants.ERROR_UPDATING_ORDER_MSG, pRequest,
							pResponse);
					if (isLoggingError()) {
						vlogError("CommerceException occurs in @Class::TRUBillingInfoFormHandler::@method::handleUpdateCardDataInOrder() method.. :{0}",
								e);
					}
				} 
			} 
			catch (CommerceException e) {
				hasException=true;
				if (isLoggingError()) {
					vlogError("CommerceException occurs in @Class::TRUBillingInfoFormHandler::@method::handleUpdateCardDataInOrder() method.. {0}",
							e);
				}
			}finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (javax.transaction.SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in @Class::TRUBillingInfoFormHandler::@method::handleAddCreditCard()", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRUCommerceConstants.HANDLE_UPDATE_CARD_DATA_IN_ORDER);
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("END :: TRUBillingInforFormHandler.handleUpdateCardDataInOrder method");
			}
		} else {
			if (isLoggingDebug()) {
				vlogDebug("END :: TRUBillingInforFormHandler.handleUpdateCardDataInOrder method, repeat request");
			}
		}
		if(isRestService()){
			return checkFormRedirect(getCommonSuccessURL(),	getCommonErrorURL(), pRequest, pResponse);
		}else{
			return Boolean.FALSE;
		}
	}

	
	
	/**
	 * This method validates the credit card input details entered by the user.
	 * 
	 * @throws ServletException ServletException
	 */
	private void validateCreditCard() throws ServletException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUBillingInfoFormHandler:validateCreditCard method");
		}
		int validationResult = getOrderTools().validateCreditCard(getCreditCardInfoMap());
		if (validationResult != 0) {
			switch (validationResult) {
			case TRUCommerceConstants.CARD_EXPIRED:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CARD_EXPIRED, null);
				break;
			case TRUCommerceConstants.CARD_NUMBER_HAS_INVALID_CHARS:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_NUMBER_HAS_INVALID_CHARS,null);
				break;
			case TRUCommerceConstants.CARD_NUMBER_DOESNT_MATCH_TYPE:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_NUMBER_DOESNT_MATCH_TYPE,null);
				break;
			case TRUCommerceConstants.CARD_LENGTH_NOT_VALID:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_LENGTH_NOT_VALID, null);
				break;
			case TRUCommerceConstants.CARD_NUMBER_NOT_VALID:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_NUMBER_NOT_VALID,null);
				break;
			case TRUCommerceConstants.CARD_INFO_NOT_VALID:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_INFO_NOT_VALID, null);
				break;
			case TRUCommerceConstants.CARD_EXP_DATE_NOT_VALID:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_EXP_DATE_NOT_VALID,null);
				break;
			case TRUCommerceConstants.CARD_TYPE_NOT_VALID:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_TYPE_NOT_VALID, null);
				break;
			}
			getErrorHandler().processException(mVe, this);
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUBillingInfoFormHandler:validateCreditCard method");
		}
	}

	/**
	 * Gets the credit card number.
	 *
	 * @return string the creditCardNumber
	 */
	public String getCreditCardNumber() {
		return (String) (getCreditCardInfoMap())
				.get(TRUConstants.CREDIT_CARD_NUMBER);
	}

	/**
	 * Gets the expiration month.
	 *
	 * @return the expirationMonth
	 */
	public String getExpirationMonth() {
		return (String) (getCreditCardInfoMap())
				.get(TRUConstants.EXPIRATION_MONTH);
	}

	/**
	 * Gets the expiration year.
	 *
	 * @return the expirationYear
	 */
	public String getExpirationYear() {
		return (String) (getCreditCardInfoMap())
				.get(TRUConstants.EXPIRATION_YEAR);
	}
	
	//Moved to BillingProcessHelper as part of fixing PMD NcssTypeCount
	/**
	 * Apply shipping groups.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @return the addressPhoneNumber
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 *//*
	public String getAddressPhoneNumber() {
		String addressPhoneNumber = (getAddressInputFields().getPhoneNumber());
		if (!StringUtils.isBlank(addressPhoneNumber)) {
			addressPhoneNumber = addressPhoneNumber.replaceAll(
					TRUConstants.HYPHEN, TRUConstants.EMPTY);
			addressPhoneNumber = addressPhoneNumber.trim();
			getAddressInputFields().setPhoneNumber(addressPhoneNumber);
		}
		return addressPhoneNumber;
	}*/

	/**
	 * This method applies shipping group.
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void applyShippingGroups(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			    throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUBillingInfoFormHandler:applyShippingGroups method");
		}
		try {
			getCommerceItemShippingInfoTools().applyCommerceItemShippingInfos(getCommerceItemShippingInfoContainer(),
					getShippingGroupMapContainer(), getShoppingCart().getCurrent(),
					isConsolidateShippingInfosBeforeApply(), isApplyDefaultShippingGroup());
		} catch (CommerceException exc) {
			if (isLoggingError()) {
				logError("Exception in @Class::TRUBillingInfoFormHandler::@method::applyShippingGroups()",exc);
			}
			processException(exc, TRUCommerceConstants.ERROR_UPDATE_SG, pRequest,
					pResponse);
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUBillingInfoFormHandler:applyShippingGroups method");
		}
	}
	
	/**
	 * This method is used to initialize the rewards number in order from profile.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleInitRewardNumberInOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUBillingInfoFormHandler.handleInitRewardNumberInOrder method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String myHandleMethod = TRUCommerceConstants.HANDLE_INIT_REWARD_NUMBER_IN_ORDER;
		boolean hasException=false;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			resetFormExceptions();
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					if (!getFormError()) {
						// get the rewards number from profile if any and update to order 
						String rewardNumber = (String) getProfile().getPropertyValue(TRUConstants.REWARD_NO);
						if(StringUtils.isNotBlank(rewardNumber) && StringUtils.isBlank(((TRUOrderImpl)getOrder()).getRewardNumber())){
							((TRUOrderImpl)getOrder()).setRewardNumber(rewardNumber);
						}
						//call the post method
						getOrderManager().updateOrder(getOrder());
					}
				}
		  } catch (CommerceException comExc) {
			  hasException=true;
			  if(isLoggingError())
			  {
			  logError(
						"Exception in @Class::TRUBillingInfoFormHandler::@method::handleInitRewardNumberInOrder()",
						comExc);
			  }
		} finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (javax.transaction.SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in @Class::TRUBillingInfoFormHandler::@method::handleInitRewardNumberInOrder()", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		  }
		if (isLoggingDebug()) {
			vlogDebug("End of TRUBillingInfoFormHandler.handleInitRewardNumberInOrder method");
		}
		return checkFormRedirect(getPaymentInfoSuccessURL(), getPaymentInfoErrorURL(), pRequest, pResponse);
	}
	
	/**
	 * This method does a SetExpressCheckout API call to fetch token.
	 * 
	 * @param pRequest - DynamoHttpServletRequest
	 * @param pResponse - DynamoHttpServletResponse
	 * @return the PayPal token after calling the SetExpressCheckout API call
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleCheckoutWithPayPal(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUBillingInfoFormHandler : handleCheckoutWithPayPal() method ");
		}			
		String payPalToken = null;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String myHandleMethod = TRUCommerceConstants.HANDLE_CHECKOUT_WITH_PAYPAL;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			TRUUserSession userSession = getUserSession(); 
			if (isLoggingDebug()) {
				vlogDebug(" UserSession :{0} PaypalPaymentDetailsMap() :{1} ",userSession, userSession.getPaypalPaymentDetailsMap());
			}
			if (userSession != null && userSession.getPaypalPaymentDetailsMap() != null
					&& !userSession.getPaypalPaymentDetailsMap().isEmpty()) {
				payPalToken = getUserSession().getPaypalPaymentDetailsMap().get(TRUPayPalConstants.PAYPAL_TOKEN);
				if (isLoggingDebug()) {
					vlogDebug("payPalToken = {0}", payPalToken);
				}
				if (isRestService()) {
					setPaypalRedirectURL(getRadialConfiguration().getPaypalRedirectURL() + payPalToken
							+ getRadialConfiguration().getPayPalCommitParam());
				}
			}
			if (rrm != null) {
				rrm.removeRequestEntry(myHandleMethod);
			}			
		}
		
		if (isLoggingDebug()) {
			vlogDebug("End of TRUBillingInfoFormHandler : handleCheckoutWithPayPal() method ");
		}
		if(isRestService()){
			return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
		}else{
			return false;
		}
		
	}
	
	/**
	 * This method is used for validation before updating finance agreed flag in order.
	 * Basically this method invokes pre-auth API and handles exceptions if any comes from the service response.
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void preUpdateFinanceAgreedToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException{
		if(isLoggingDebug()){
			vlogDebug("TRUBillingInfoFormHandler.preUpdateFinanceAgreedToOrder() method.STARTS");
		}
		
		/*try{
			TRUPropertyManager propertyManager = getPropertyManager();
			if (getCreditCardInfoMap().get(propertyManager.getCreditCardNumberPropertyName()).equals(
					getTRUConfiguration().getCardNumberPLCC()) && !isRUSCard()) {
				getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationMonthPropertyName(),
						getTRUConfiguration().getExpMonthAdjustmentForPLCC());
				getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationYearPropertyName(),
						getTRUConfiguration().getExpYearAdjustmentForPLCC());
			}
			
			getValidator().validate(TRUConstants.MY_ACCOUNT_ADD_CARD_WITHOUT_ADDR_FORM,	this);
			if(((TRUOrderManager)getOrderManager()).isAnyCreditCardFieldEmpty(getCreditCardInfoMap()) && !isSavedCard()){
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
				getErrorHandler().processException(mVe, this);
			} else {
				TRUProfileManager profileManager = getBillingHelper().getProfileManager();
				boolean isCardExpired = profileManager.validateCardExpiration(
						getCreditCardInfoMap().get(TRUConstants.EXPIRATION_MONTH),
						getCreditCardInfoMap().get(TRUConstants.EXPIRATION_YEAR));
				if(isCardExpired) {
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CARD_EXPIRED, null);
					getErrorHandler().processException(mVe, this);
				}
			}
			
			if (!getFormError() && getOrderTools().isPayeezyEnabled()) {
				TRUCreditCardStatus creditCardStatus = getShippingHelper().getPurchaseProcessHelper()
						.verifyPreAuthorization(SiteContextManager.getCurrentSite().getId(), getCreditCardInfoMap(),
								getProfile().getRepositoryId(), isSavedCard());

				if (creditCardStatus != null && !creditCardStatus.getTransactionSuccess()) {
					mVe.addValidationError(creditCardStatus.getErrorMessage(), null);
					getErrorHandler().processException(mVe, this);
				}
				
				if(!getFormError() && creditCardStatus.getPromoDetails() != null){
					setPercentageValuation(creditCardStatus.getPromoDetails());
				}
			}
		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			vlogError("Validation exception occured: {0}", ve);
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			vlogError("System exception occured: {0}", se);
		}*/
		
		if(isLoggingDebug()){
			vlogDebug("TRUBillingInfoFormHandler.preUpdateFinanceAgreedToOrder() method.STARTS");
		}
	}
	
	/**
	 * This method is used call BAMS pre-auth API and if the response is success then to update the user's promo financing disclosure agreement in order level.
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException servlet exception
	 * @throws IOException io exception
	 */
	public boolean handleUpdateFinanceAgreedToOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if(isLoggingDebug()){
			vlogDebug("TRUBillingInfoFormHandler.handleUpdateFinanceAgreedToOrder() method.STARTS");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "TRUBillingInfoFormHandler.handleUpdateFinanceAgreedToOrder";
		boolean hasException=false;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			if (PerformanceMonitor.isEnabled()){
				PerformanceMonitor.startOperation(myHandleMethod);
			}
			Transaction tr = null;
			resetFormExceptions();
			preUpdateFinanceAgreedToOrder(pRequest, pResponse);
			if(!getFormError()){
				try {
					tr = ensureTransaction();
					//String cardinalToken = getPreAuthCardinalToken();
					if(!getFormError()){
						synchronized (getOrder()) {
							TRUPropertyManager propertyManager = getPropertyManager();
							((TRUOrderImpl)getOrder()).setFinanceAgreed(isFinanceAgreed());
							((TRUOrderImpl)getOrder()).setFinancingTermsAvailable(getFinancingTermsAvailed());
							
							TRUActiveAgreementsVO activeAgreementsVO = null;
							String creditCardNumber = getCreditCardInfoMap().get(propertyManager.getCreditCardTokenPropertyName());
							if(null != ((TRUOrderImpl)getOrder()).getActiveAgreementsVO().get(creditCardNumber)){
								activeAgreementsVO = ((TRUOrderImpl)getOrder()).getActiveAgreementsVO().get(creditCardNumber);
							} else {
								activeAgreementsVO = new TRUActiveAgreementsVO();
							}
							activeAgreementsVO.setCreditCardNumber(creditCardNumber);
							activeAgreementsVO.setFinanceAgreed(TRUConstants.INTEGER_NUMBER_ONE);
							activeAgreementsVO.setFinancingTermsAvailed(((TRUOrderImpl)getOrder()).getFinancingTermsAvailable());
							if(getUserSession().isRusCardSelected()) {
								((TRUOrderImpl)getOrder()).getActiveAgreementsVO().put(TRUConstants.TSP_CARD_NUMBER, activeAgreementsVO);
							} else {
								((TRUOrderImpl)getOrder()).getActiveAgreementsVO().put(creditCardNumber, activeAgreementsVO);
							}
							getOrderManager().updateOrder(getOrder());
						}
					}
				} catch (CommerceException comExp) {
					hasException=true;
					if(isLoggingError()) {
						logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handleUpdateFinanceAgreedToOrder()", comExp);
					}
					PerformanceMonitor.cancelOperation(myHandleMethod);
				}  finally {
						if (tr != null) {
							if (hasException) {
								try {
									setTransactionToRollbackOnly();
								} catch (javax.transaction.SystemException e) {
									if (isLoggingError()) {
										logError("SystemException in "
												+ "@Class::TRUBillingInfoFormHandler::@method::handleUpdateFinanceAgreedToOrder()", e);
									}
								}
							}
							commitTransaction(tr);
						}
						if (rrm != null) {
							rrm.removeRequestEntry(myHandleMethod);
						}
						if (PerformanceMonitor.isEnabled()){
							PerformanceMonitor.endOperation(myHandleMethod);
						}
					}
				}
			}
			if(isLoggingDebug()){
				vlogDebug("TRUBillingInfoFormHandler.handleUpdateFinanceAgreedToOrder() method.STARTS");
			}
			return checkFormRedirect(getCommonSuccessURL(),	getCommonErrorURL(), pRequest, pResponse);
	}
	
	/**
	 * Checks if is edits the order billing address.
	 *
	 * @return the mEditOrderBillingAddress
	 */
	public boolean isEditOrderBillingAddress() {
		return mEditOrderBillingAddress;
	}

	/**
	 * Sets the edits the order billing address.
	 *
	 * @param pEditOrderBillingAddress the mEditOrderBillingAddress to set
	 */
	public void setEditOrderBillingAddress(boolean pEditOrderBillingAddress) {
		this.mEditOrderBillingAddress = pEditOrderBillingAddress;
	}
	
	/**
	 * Handle pay pal payment to review order.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 * @throws SystemException SystemException
	 */
	public boolean handlePayPalPaymentToReviewOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, SystemException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUBillingInfoFormHandler.handlePayPalPaymentToReviewOrder method");
		}
		boolean rollbackFlag = false;
		boolean isEmailValid = true;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "TRUBillingInfoFormHandler.handlePayPalPaymentToReviewOrder";
		String payPalErrKey = TRUErrorKeys.TRU_PAYPAL_GENERIC_ERR_KEY;
		String payPalErrMessage = getErrorHandlerManager().getErrorMessage(payPalErrKey);
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					prePayPalPaymentToReviewOrder(pRequest, pResponse);
					if (!getFormError()) {
						TRUOrderManager orderManager = ((TRUOrderManager)getOrderManager());
						TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
						TRUPropertyManager propertyManager = (TRUPropertyManager) profileTools.getPropertyManager();
						if(getProfile().isTransient()){
							((TRUOrderImpl)getOrder()).setEmail(getEmail());
						} else {
							((TRUOrderImpl) getOrder()).setEmail((String) getProfile().getPropertyValue(
									propertyManager.getEmailAddressPropertyName()));
						}
						((TRUOrderImpl)getOrder()).setRewardNumber(getRewardNumber());
						profileTools.updateProperty(((TRUPropertyManager) profileTools.getPropertyManager()).getRewardNumberPropertyName(),
								getRewardNumber(), getProfile());
						TRUPaymentGroupManager paymentGroupManager = (TRUPaymentGroupManager)getPaymentGroupManager();
						paymentGroupManager.removeOtherPaymentGroups(getOrder());
						orderManager.addUpdatePayPalPaymentGroupToOrder(getOrder(), getPaymentGroupMapContainer(), getProfile());
						PricingModelHolder userPricingModels = getUserPricingModels();
						userPricingModels.initializePricingModels();
						runProcessRepriceOrder(getOrder(), userPricingModels, getUserLocale(),getProfile(), createRepriceParameterMap());
						if (!getFormError()) {
							((TRUOrderImpl) getOrder()).setCurrentTab(TRUCheckoutConstants.REVIEW_TAB);
							((TRUOrderImpl) getOrder()).getActiveTabs().put(TRUCheckoutConstants.PAYMENT_TAB, Boolean.TRUE);
							((TRUOrderImpl) getOrder()).getActiveTabs().put(TRUCheckoutConstants.REVIEW_TAB, Boolean.TRUE);
						}
						postPayPalPaymentToReviewOrder(pRequest, pResponse);
						orderManager.updateOrder(getOrder());
					} else {
						isEmailValid = false;
					}					
				}
			} catch (CommerceException comExp) {
				rollbackFlag = true;
				if(isLoggingError()) {
					logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handlePayPalPaymentToReviewOrder()", comExp);
				}
			} catch (RunProcessException repriceException) {
				rollbackFlag = true;
				if(isLoggingError()) {
					logError(" Reprice Exception in "
							+ "@Class::TRUBillingInfoFormHandler::@method::handlePayPalPaymentToReviewOrder()", repriceException);
				}
			} catch (RepositoryException e) {
				rollbackFlag = true;
				if(isLoggingError()) {
					logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handlePayPalPaymentToReviewOrder()", e);
				}
			}  finally {				
				if (isEmailValid && (rollbackFlag || getFormError())) {
						 addFormException(new DropletException(payPalErrMessage));
						if(isLoggingError()) {
							logError("Paypal Error Message "
									+ "@Class::TRUBillingInfoFormHandler::@method::handlePayPalPaymentToReviewOrder()"+ payPalErrMessage);
						}	
				}
				if (tr != null) {
					commitTransaction(tr);
				}
				if (isEmailValid && (getFormError() || rollbackFlag)) {
					removePaypalPGFromOrder();
					//return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
				}				
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUBillingInfoFormHandler.handlePayPalPaymentToReviewOrder method");
		}
		return checkFormRedirect(getCommonSuccessURL(),	getCommonErrorURL(), pRequest, pResponse);
	}
	
	/**
	 * This method is to validate the payment form in case of Pay In Store.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	protected void prePayPalPaymentToReviewOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUBillingInfoFormHandler/prePayPalPaymentToReviewOrder method");
		}
		try {
			if(getProfile().isTransient()){
				/*To validate all mandatory fields including email when user is not logged in along with email format*/
				if (StringUtils.isBlank(getEmail())) {
					if (isLoggingDebug()) {
						vlogDebug("TRUBillingInfoFormHandler.prePayPalPaymentToReviewOrder is having errors");
					}
					getValidator().validate(TRUConstants.MY_ACCOUNT_UPDATE_EMAIL_FORM,this);
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
					getErrorHandler().processException(mVe, this);
				}
				if (!getFormError() && StringUtils.isNotBlank(getEmail()) && !getEmailPasswordValidator().validateEmail(getEmail())) {
					// to validate email format
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_INVALID_EMAIL, null);
					getErrorHandler().processException(mVe, this);
				}
				if(!getFormError() && StringUtils.isNotEmpty(getRewardNumber()) && getRewardNumberValid().equalsIgnoreCase(TRUConstants.FALSE)){
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_REWARD_NUMBER_INVALID, null);   
					getErrorHandler().processException(mVe, this);
				}
			} 
			/*if(!getFormError()){			
				TRUOrderImpl order=(TRUOrderImpl)getOrder();
				Map<String,Object> parameters = new HashMap<String,Object>();
				parameters.put(PipelineConstants.ORDER, order);
				((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).runProcessTRUValidateForCheckoutChain(order,
						getUserPricingModels(), getUserLocale(), getProfile(), parameters, null, this);
				// Calling the reprice order and adjusting the PGs when the order gets modified					
				if (order.isValidationFailed()) {
					addFormException(new DropletException(String.valueOf(Boolean.FALSE)));
					((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL,
							order, getUserPricingModels(), getUserLocale(), getProfile(), parameters, this);
					((TRUOrderManager) getOrderManager()).adjustPaymentGroups(order);
					getOrderManager().updateOrder(order);
				}				
			}*/
		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			if (isLoggingError()) {
				logError("Validation exception occured@Class::TRUBillingInfoFormHandler::@method::prePayPalPaymentToReviewOrder()", ve);
			}
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			if (isLoggingError()) {
				logError("System exception occured@Class::TRUBillingInfoFormHandler::@method::prePayPalPaymentToReviewOrder()", se);
			}
		}/*catch (RunProcessException se) {
			if (isLoggingError()) {
				logError("RunProcessException occured@Class::TRUBillingInfoFormHandler::@method::prePayPalPaymentToReviewOrder()", se);
			}
		}catch (CommerceException se) {
			if (isLoggingError()) {
				logError("CommerceException occured@Class::TRUBillingInfoFormHandler::@method::prePayPalPaymentToReviewOrder()", se);
			}
		}*/
		if (isLoggingDebug()) {
			vlogDebug("End of TRUBillingInfoFormHandler/preMoveToOrderReview method");
		}
	}
	
	/**
	 * Handle pay pal email validate.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handlePayPalEmailValidate(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUBillingInfoFormHandler.handlePayPalEmailValidate method");
		}
		String payPalToken= null; 
		SetExpressCheckoutResponse setExpressCheckoutResponse = null; 
		String timeStamp = null;
		boolean rollbackFlag = false;
		boolean isEmailValid = true;
		String errorURL = TRUUrlUtil.getConstructedUrl(pRequest, getSchemeDetectionUtil().getScheme(), getTRUConfiguration().getPayPalChkoutCancelUrl(), null);
		String successURL = TRUUrlUtil.getConstructedUrl(pRequest, getSchemeDetectionUtil().getScheme(),getTRUConfiguration().getPayPalChkoutSuccessUrl(),null);
		if (isLoggingDebug()) {
			logDebug("PayPal Return Success URL :: "+successURL+ " PayPal Return Failure URL "+errorURL);
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "TRUBillingInfoFormHandler.handlePayPalEmailValidate";
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			try {
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.startOperation(myHandleMethod);
				}
				tr = ensureTransaction();
				prePayPalPaymentToReviewOrder(pRequest, pResponse);
				if (getFormError()) {
					isEmailValid = false;
					return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
				} else {
					if (!StringUtils.isBlank(getPayPalToken()) && TRUPayPalConstants.TOKEN_NOT_EXIST.equals(getPayPalToken())) {
						((TRUOrderImpl)getOrder()).setStanderdPayPal(Boolean.TRUE);
						((TRUOrderImpl)getOrder()).setCurrentTab(TRUCheckoutConstants.PAYMENT_TAB);
						setExpressCheckoutResponse = ((TRUPaypalUtilityManager)getPayPalUtilityManager()).
								payPalSetExpressCheckout(getPaymentGroupMapContainer() , pRequest.getLocale().toString(), getOrder(),
										errorURL, successURL,getProfile(),true);
						if (isLoggingDebug()) {
							vlogDebug("SetExpressCheckoutResponse : {0}  Is Success :{1} Token :{2}" , setExpressCheckoutResponse,
									setExpressCheckoutResponse.isSuccess(),setExpressCheckoutResponse.getToken());
						}
						if (setExpressCheckoutResponse != null && setExpressCheckoutResponse.isSuccess()
								&& !StringUtils.isBlank(setExpressCheckoutResponse.getToken())) {
							if (isLoggingDebug()) {
								vlogDebug("SetExpressCheckoutResponse : {0} " , setExpressCheckoutResponse);
							}

							payPalToken = setExpressCheckoutResponse.getToken();
							timeStamp = setExpressCheckoutResponse.getTimeStamp();
							if (isLoggingDebug()) {
								vlogDebug("payPalToken  : {0}, payPalTokenTimeStamp : {1}",payPalToken,timeStamp);
							}
							if(!StringUtils.isBlank(payPalToken) && getUserSession() != null) {
								Map<String, String> paypalPaymentDetailsMap = getUserSession().getPaypalPaymentDetailsMap();
								if(paypalPaymentDetailsMap == null) {
									paypalPaymentDetailsMap = new HashMap<String,String>();
								}
								paypalPaymentDetailsMap.put(TRUPayPalConstants.PAYPAL_TOKEN, payPalToken);
								paypalPaymentDetailsMap.put(TRUPayPalConstants.PAYPAL_TOKEN_TIMESTAMP, timeStamp);
								getUserSession().setPaypalPaymentDetailsMap(paypalPaymentDetailsMap);
							}
							synchronized (getOrder()) {
								if (isLoggingDebug()) {
									vlogDebug("Updating the card details");
								}
								TRUPaymentGroupManager paymentGroupManager = (TRUPaymentGroupManager)getPaymentGroupManager();
								paymentGroupManager.removeOtherPaymentGroups(getOrder());
								((TRUOrderManager) getOrderManager()).addUpdatePayPalPaymentGroupToOrder(getOrder(),
										getPaymentGroupMapContainer(), payPalToken, getProfile());
								PricingModelHolder userPricingModels = getUserPricingModels();
								userPricingModels.initializePricingModels();
								runProcessRepriceOrder(getOrder(), userPricingModels, getUserLocale(),getProfile(), createRepriceParameterMap());
								if (isLoggingDebug()) {
									vlogDebug("End of updating card details");
								}
							}						
						} else {
							if (isLoggingDebug()) {
								vlogDebug(" Token or Is Success is empty ");
							}
							removePaypalPGFromOrder();
							rollbackFlag = true;
						}						
					}
					synchronized (getOrder()) {
						if (isLoggingDebug()) {
							vlogDebug(" Updating the email id to the order ");
						}
						TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
						TRUPropertyManager propertyManager = (TRUPropertyManager) profileTools.getPropertyManager();
						if(getProfile().isTransient()){
							((TRUOrderImpl)getOrder()).setEmail(getEmail());
						} else {
							((TRUOrderImpl) getOrder()).setEmail((String) getProfile().getPropertyValue(
									propertyManager.getEmailAddressPropertyName()));
						}
						((TRUOrderImpl)getOrder()).setRewardNumber(getRewardNumber());
						profileTools.updateProperty(((TRUPropertyManager) profileTools.getPropertyManager()).getRewardNumberPropertyName(),
								getRewardNumber(), getProfile());
						getOrderManager().updateOrder(getOrder());
					}
				}
			} catch (CommerceException commerceException) {
				rollbackFlag = true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("TRUBillingInfoFormHandler.handlePayPalEmailValidate", commerceException);
				}
			} catch (SystemException systemException) {
				rollbackFlag = true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("TRUBillingInfoFormHandler.handlePayPalEmailValidate", systemException);
				}
			} catch (TRUPayPalException payPalException) {
				rollbackFlag = true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("TRUBillingInfoFormHandler.handlePayPalEmailValidate", payPalException);
				}
			} catch (RunProcessException pricingExcep) {
				rollbackFlag = true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("Pricing Exception TRUBillingInfoFormHandler.handlePayPalEmailValidate", pricingExcep);
				}
			} catch (RepositoryException e) {
				rollbackFlag = true;
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("Pricing Exception TRUBillingInfoFormHandler.handlePayPalEmailValidate", e);
				}
			} finally {
				if (isEmailValid && (getFormError() || rollbackFlag)) {
					// get the message corresponding to key
					getPayPalUtilityManager().addPayPalErrorMessage(setExpressCheckoutResponse, this);
					//	setCommonErrorURL(errorURL);
				}
				if (tr != null) {
					commitTransaction(tr);
				}
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.endOperation(myHandleMethod);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("CommonSuccessUrl :{0} CommonErrorUrl :{1}",getCommonSuccessURL(),getCommonErrorURL());
			vlogDebug("End of TRUBillingInfoFormHandler.handlePayPalEmailValidate method");
		}
		return checkFormRedirect(getCommonSuccessURL(),	getCommonErrorURL(), pRequest, pResponse);
	}

	
	
	/**
	 * Remove the Pay pal PG from order.
	 */
	public void removePaypalPGFromOrder() {
		String payPalErrMessage = null;
		String payPalErrKey = null;//getPayPalConfiguration().getPayPalGenericErrorKey();
		final TransactionDemarcation transactionDemarcation = new TransactionDemarcation();
		try {
			transactionDemarcation.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
			if (!StringUtils.isBlank(payPalErrKey)) {
				payPalErrMessage = getErrorHandlerManager().getErrorMessage(payPalErrKey);
			}
			synchronized (getOrder()) {									
				final TRUPaymentGroupManager paymentGroupManager = (TRUPaymentGroupManager)getPaymentGroupManager();
				paymentGroupManager.removePayPalPaymentGroupToOrder(getOrder());
				//((TRUOrderManager) getOrderManager()).createCreditCardPaymentGroupToOrder(getOrder(),getPaymentGroupMapContainer());
				((TRUOrderImpl) getOrder()).setCurrentTab(TRUCheckoutConstants.PAYMENT_TAB);
				((TRUOrderManager) getOrderManager()).updateOrder(getOrder());
			}			
		} catch (TransactionDemarcationException demarcationException) {
			addFormException(new DropletException(payPalErrMessage));
			if(isLoggingError()){
				logError(
						"TransactionDemarcationException occurs @Class:::TRUBillingInfoFormHandler:::@method:::removePaypalPGFromOrder() ",
						demarcationException);
			}
		} catch (CommerceException commerceException) {
			addFormException(new DropletException(payPalErrMessage));
			if(isLoggingError()){
				logError("CommerceException occurs @Class:::TRUBillingInfoFormHandler:::@method:::removePaypalPGFromOrder()", commerceException);
			}
		} catch (SystemException systemException) {
			addFormException(new DropletException(payPalErrMessage));
			if(isLoggingError()){
				logError("SystemException occurs @Class:::TRUBillingInfoFormHandler:::@method:::removePaypalPGFromOrder()", systemException);
			}
		} finally {
			if (transactionDemarcation != null){
				try {
					transactionDemarcation.end();
				} catch (TransactionDemarcationException demarcationException) {
					addFormException(new DropletException(payPalErrMessage));
					if(isLoggingError()){
						logError(
								"TransactionDemarcationException occurs @Class:::TRUBillingInfoFormHandler:::@method:::removePaypalPGFromOrder()",
								demarcationException);
					}
				}
			}
		}
	}
	
	/**
	 * This handle method used to select if the raffle order used co branded card for the payment. If user select true
	 * then need to update the profile property to and repeice the order to get the configured promotion for the user
	 *
	 * @param pRequest - The servlet's request
	 * @param pResponse - The servlet's response
	 * @return boolean - Boolean flag
	 * @throws IOException - If an error occurred while reading or writing the DynamoServlet request
	 * @throws ServletException - If an error occurred while processing the DynamoServlet request
	 */
	public boolean handleupdateDataInOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN :: TRUBillingInforFormHandler.handleupdateDataInOrder method..");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean hasException=false;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRUCommerceConstants.HANDLE_UPDATE_CARD_DATA_IN_ORDER))) {
			Transaction tr = null;
			try {
				// Creating the transaction
				tr = ensureTransaction();
				try {
					synchronized (getOrder()) {
						if (!getFormError()) {
							((TRUOrderImpl)getOrder()).setCreditCardType(null);
							((TRUOrderManager)getOrderManager()).ensurePayPal(getOrder());
							PricingModelHolder userPricingModels = getUserPricingModels();
							userPricingModels.initializePricingModels();
							runProcessRepriceOrder(getOrder(), userPricingModels, getUserLocale(),getProfile(), createRepriceParameterMap());
							getOrderManager().updateOrder(getOrder());
						}
					}
				} catch (RunProcessException e) {
					hasException=true;
					processException(e, TRUCommerceConstants.ERROR_UPDATING_ORDER_MSG, pRequest,
							pResponse);
					if (isLoggingError()) {
						vlogError("CommerceException occurs in  @Class:::TRUBillingInfoFormHandler:::@method:::handleupdateDataInOrder()..",
								e);
					}
				} 
			} 
			catch (CommerceException e) {
				hasException=true;
				if (isLoggingError()) {
					vlogError("CommerceException occurs in @Class:::TRUBillingInfoFormHandler:::@method:::handleupdateDataInOrder().",
							e);
				}
			}finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (javax.transaction.SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in @Class::TRUBillingInfoFormHandler::@method::handleupdateDataInOrder()", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRUCommerceConstants.HANDLE_UPDATE_CARD_DATA_IN_ORDER);
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("END :: TRUBillingInforFormHandler.handleUpdateCardDataInOrder method");
			}
		} else {
			if (isLoggingDebug()) {
				vlogDebug("END :: TRUBillingInforFormHandler.handleUpdateCardDataInOrder method, repeat request");
			}
		}
		// This method always calls through AJAX. Hence return false 
		// for rest using checkFormRedirect
		if(isRestService()){
			return checkFormRedirect(getCommonSuccessURL(),	getCommonErrorURL(), pRequest, pResponse);
		}else{
			return Boolean.FALSE;
		}
		
	}

	/**
	 * Gets the pay pal token.
	 *
	 * @return the mPayPalToken
	 */
	public String getPayPalToken() {
		return mPayPalToken;
	}

	/**
	 * Sets the pay pal token.
	 *
	 * @param pPayPalToken the mPayPalToken to set
	 */
	public void setPayPalToken(String pPayPalToken) {
		this.mPayPalToken = pPayPalToken;
	}
	
	/**
	 * Checks for credit card expiration in order.
	 * Adds form exception if credit card expired.
	 * 
	 * @return true - if credit card is expired.
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	public boolean isOrderCreditCardExpired() throws ServletException, IOException  {
		if (isLoggingDebug()) {
			vlogDebug("START :: TRUBillingInforFormHandler.isOrderCreditCardExpired method");
		}
		String defaultPaymentGroup = null;
		//Start :: TUW-56705
		if(!getProfile().isTransient()){
			defaultPaymentGroup = (String)getProfile().getPropertyValue(getPropertyManager().getSelectedCCNickNamePropertyName());
		}else{
			defaultPaymentGroup = getPaymentGroupMapContainer().getDefaultPaymentGroupName();
		}
		TRUCreditCard orderCreditCard = (TRUCreditCard) ((TRUOrderManager) getOrderManager()).getCreditCard(getOrder());
		
		PaymentGroup paymentGroup = (PaymentGroup)getPaymentGroupMapContainer().getPaymentGroup(defaultPaymentGroup);
		if (paymentGroup instanceof CreditCard && orderCreditCard != null) {
			TRUProfileManager profileManager = getBillingHelper().getProfileManager();
			CreditCard creditCard = (CreditCard)paymentGroup;
		//End :: TUW-56705
			boolean isCardExpired = profileManager.validateCardExpiration(creditCard.getExpirationMonth(),creditCard.getExpirationYear());
			if(isCardExpired) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ORDER_CREDIT_CARD_EXPIRED, null);
				getErrorHandler().processException(mVe, this);
				if (isLoggingDebug()) {
					vlogDebug("END :: TRUBillingInforFormHandler.isOrderCreditCardExpired method");
				}
				return Boolean.TRUE;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END :: TRUBillingInforFormHandler.isOrderCreditCardExpired method");
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Validate order remaining amount.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("unchecked")
	public void validateOrderRemainingAmount(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		// validate the order for order amount.
		if (isLoggingDebug()) {
			vlogDebug("START TRUBillingInfoFormHandler:: validateOrderRemainingAmount method");
		}
		TRUOrderImpl order = (TRUOrderImpl) getOrder();
		List<PaymentGroup> listOfPaymentGroup = order.getPaymentGroups();
		if (listOfPaymentGroup.isEmpty()) {
			boolean validateOrderRemainingAmount = getBillingHelper().validateOrderRemainingAmount(order,
					TRUCheckoutConstants.MOVE_TO_REVIEW_ORDER);
			if (!validateOrderRemainingAmount) {
				mVe.addValidationError(TRUErrorKeys.ERR_OTHER_PAYMENT_OPTION_IS_REQUIRED, null);
				getErrorHandler().processException(mVe, this);
			}
		} else {
			boolean validateOrderRemainingAmount = getBillingHelper().validateOrderRemainingAmount(order,
					TRUCheckoutConstants.MOVE_TO_REVIEW_ORDER);
			if (!validateOrderRemainingAmount) {
				mVe.addValidationError(TRUErrorKeys.GIFT_CARDS_BALANCE_IS_LESS_THAN_ORDER_AMOUNT, null);
				getErrorHandler().processException(mVe, this);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END TRUBillingInfoFormHandler:: validateOrderRemainingAmount method");
		}
	} 
	
	/**
	 * This method used to trim the address values in a contactInfo bean.
	 * 
	 * @param pAddress
	 *            The map to hold the address details entered by the user
	 */
	public void trimContactInfoValues(ContactInfo pAddress) {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUBillingInfoFormHandler.trimContactInfoValues method");
		}
		getAddressInputFields().setFirstName(pAddress.getFirstName().trim());
		getAddressInputFields().setLastName(pAddress.getLastName().trim());
		getAddressInputFields().setAddress1(pAddress.getAddress1().trim());
		if (StringUtils.isNotBlank(pAddress.getAddress2())) {
			getAddressInputFields().setAddress2(pAddress.getAddress2().trim());
		}
		getAddressInputFields().setCity(pAddress.getCity().trim());
		getAddressInputFields().setState(pAddress.getState().trim());
		getAddressInputFields().setPostalCode(pAddress.getPostalCode().trim());
		getAddressInputFields().setPhoneNumber(pAddress.getPhoneNumber().trim());
		getAddressInputFields().setCountry(pAddress.getCountry().trim());
		if (isLoggingDebug()) {
			vlogDebug("End of TRUBillingInfoFormHandler.trimContactInfoValues method");
		}
	}
    
	
	/**
	 * Gets the integration status util.
	 *
	 * @return the integrationStatusUtil
	 */
	public TRUIntegrationStatusUtil getIntegrationStatusUtil() {
		return mIntegrationStatusUtil;
	}

	/**
	 * Sets the integration status util.
	 *
	 * @param pIntegrationStatusUtil the integrationStatusUtil to set
	 */
	public void setIntegrationStatusUtil(TRUIntegrationStatusUtil pIntegrationStatusUtil) {
		mIntegrationStatusUtil = pIntegrationStatusUtil;
	}

	/**
	 * @return the mPercentageValuation
	 */
	public String getPercentageValuation() {
		return mPercentageValuation;
	}

	/**
	 * @param pPercentageValuation the mPercentageValuation to set
	 */
	public void setPercentageValuation(String pPercentageValuation) {
		this.mPercentageValuation = pPercentageValuation;
	} 

	/**
	 * post method for handleSetCreditCardAsDefault.
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 */
	protected void postPayPalPaymentToReviewOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {
		if(isLoggingDebug()){
			vlogDebug("Start : Updating the EDD for Order {0}", getOrder().getId());
		}
		getShippingHelper().updateEstimateDeliveryDates(getOrder());
		if(isLoggingDebug()){
			vlogDebug("End : Updating the EDD for Order {0}", getOrder().getId());
		}
	}
	
	/**
	 * Handle update synchrony data in order.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @return true, if successful
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	public boolean handleUpdateSynchronyDataInOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN :: TRUBillingInforFormHandler.handleUpdateSynchronyDataInOrder method..");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean hasException=false;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRUCommerceConstants.HANDLE_UPDATE_SYNCHRONY_DATA_INORDER))) {
			Transaction tr = null;
			try {
				// Creating the transaction
				tr = ensureTransaction();
				synchronized (getOrder()) {
					boolean prev_orderIsEligible = ((TRUOrderImpl)getOrder()).isOrderIsEligibleForFinancing();
					int prev_financingTermValue = ((TRUOrderImpl)getOrder()).getFinancingTermsAvailable();
					boolean prev_financeAgreed = ((TRUOrderImpl)getOrder()).isFinanceAgreed();
					((TRUOrderTools) getOrderManager().getOrderTools()).updateOrderIfEligibleForFinancing(getOrder());
					boolean current_orderIsEligible = ((TRUOrderImpl)getOrder()).isOrderIsEligibleForFinancing();
					if(prev_orderIsEligible) {
						int current_financingTermValue = ((TRUOrderImpl)getOrder()).getFinancingTermsAvailable();
						//Clearing the prev finance Agreement and showing the error msg
						//Use case 1: prev_financeAgreed is true, but current_orderIsEligible is false
						if(prev_financeAgreed && !current_orderIsEligible){
							mVe.addValidationError(TRUErrorKeys.TRU_AGREED_FINANCE_BUT_ORDER_IS_NOT_ELLIGIBLE, null);
							getErrorHandler().processException(mVe, this);
						}

						//Use case 2: prev_financeAgreed is true, but prev_financingTermValue is not same as current_financingTermValue
						if(prev_financeAgreed && current_orderIsEligible && prev_financingTermValue != current_financingTermValue){
							mVe.addValidationError(TRUErrorKeys.TRU_AGREED_FINANCE_BUT_CURRENT_ORDER_HAS_DIFF_FINANCING_TERM_VALUE, null);
							getErrorHandler().processException(mVe, this);
						}
						if(getFormError()) {
							((TRUOrderImpl)getOrder()).setFinanceAgreed(Boolean.FALSE);
							//Fix for TUW-63845
							if(!((TRUOrderImpl)getOrder()).getActiveAgreementsVO().isEmpty()) {
								for (String cardNumber : ((TRUOrderImpl)getOrder()).getActiveAgreementsVO().keySet()) {
									((TRUOrderImpl)getOrder()).getActiveAgreementsVO().get(cardNumber).setFinanceAgreed(TRUConstants.INTEGER_NUMBER_ZERO);
								}
							}
						}
					} 
					//Use case 3: prev_financeAgreed is false and current_orderIsEligible is true and only in Review tab
					else if(StringUtils.isNotBlank(((TRUOrderImpl)getOrder()).getCurrentTab()) && 
						((TRUOrderImpl)getOrder()).getCurrentTab().equalsIgnoreCase(TRUCheckoutConstants.REVIEW_TAB) && current_orderIsEligible) {
						String creditCardType = ((TRUOrderImpl)getOrder()).getCreditCardType();
						if(StringUtils.isNotBlank(creditCardType) && 
								(creditCardType.equalsIgnoreCase(TRUCheckoutConstants.RUS_COB_MASTER_CARD) || 
										creditCardType.equalsIgnoreCase(TRUCheckoutConstants.RUS_PRIVATE_LABEL_CARD))) {
							mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CURRENT_ORDER_MAY_QUALIFY_FOR_SPECIAL_FINANCING, null);
							getErrorHandler().processException(mVe, this);
						}
					}
					getOrderManager().updateOrder(getOrder());
				} 
			} catch (CommerceException e) {
				hasException=true;
				if (isLoggingError()) {
					logError("CommerceException occurs in @Class:::TRUBillingInfoFormHandler:::@method:::handleUpdateSynchronyDataInOrder().",
							e);
				}
			} finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (javax.transaction.SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in @Class::TRUBillingInfoFormHandler::@method::handleUpdateSynchronyDataInOrder()", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRUCommerceConstants.HANDLE_UPDATE_SYNCHRONY_DATA_INORDER);
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("END :: TRUBillingInforFormHandler.handleUpdateSynchronyDataInOrder method");
			}
		} else {
			if (isLoggingDebug()) {
				vlogDebug("END :: TRUBillingInforFormHandler.handleUpdateCardDataInOrder method, repeat request");
			}
		}
		if(isRestService()){
			return checkFormRedirect(getCommonSuccessURL(),	getCommonErrorURL(), pRequest, pResponse);
		}else{
			return Boolean.FALSE;
		}
	}
	
	/**
	 * this method sets the addressDoctor response VO.
	 * @param pAddressValidated boolean
	 * @param pEnableAddressDoctor boolean
	 * @return isAddressValidated boolean
	 * @throws ServletException servlet Exception
	 */
	private boolean setAddressFromAddressDoctorResponse(boolean pAddressValidated,boolean pEnableAddressDoctor) throws ServletException{
		boolean isAddressValidated = pAddressValidated;
		boolean isEnableAddressDoctor =	pEnableAddressDoctor;	

		if (!isAddressValidated && isEnableAddressDoctor && !getFormError()) {
			String addressStatus = ((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).validateAddress(getAddressInputFields(), getOrder(), getProfile(), TRUConstants.PAYMENT,TRUConstants.ADD_ADDRESS);
			setAddressDoctorProcessStatus(addressStatus);
			TRUAddressDoctorResponseVO addresResponseVo = ((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).getProfileTools().getAddressDoctorResponseVO();
			if(addresResponseVo != null){
				setAddressDoctorResponseVO(addresResponseVo);
				if (TRUConstants.ADDRESS_DOCTOR_STATUS_VERIFIED.equalsIgnoreCase(addressStatus)) {
					isAddressValidated = Boolean.TRUE;
				}  else if(TRUConstants.ADDRESS_DOCTOR_STATUS_CORRECTED.equalsIgnoreCase(addressStatus)){
					String derivedStatus = addresResponseVo.getDerivedStatus();
					if(derivedStatus.equalsIgnoreCase(TRUConstants.NO)){
						isAddressValidated = Boolean.TRUE;
						ContactInfo contactInfo = getAddressInputFields();
						if(!StringUtils.isBlank(addresResponseVo.getAddressDoctorVO().get(0).getAddress1())){
							contactInfo.setAddress1(addresResponseVo.getAddressDoctorVO().get(0).getAddress1());}
						if(!StringUtils.isBlank(addresResponseVo.getAddressDoctorVO().get(0).getAddress2())){
							contactInfo.setAddress2(addresResponseVo.getAddressDoctorVO().get(0).getAddress2());}
						if(!StringUtils.isBlank(addresResponseVo.getAddressDoctorVO().get(0).getCity())){
							contactInfo.setCity(addresResponseVo.getAddressDoctorVO().get(0).getCity());}
						if(!StringUtils.isBlank(addresResponseVo.getAddressDoctorVO().get(0).getState())){
							contactInfo.setState(addresResponseVo.getAddressDoctorVO().get(0).getState());}
						if(!StringUtils.isBlank(addresResponseVo.getAddressDoctorVO().get(0).getPostalCode())){
							contactInfo.setPostalCode(addresResponseVo.getAddressDoctorVO().get(0).getPostalCode());
						}
						setAddressInputFields(contactInfo);
					}
				} else if (TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR.equalsIgnoreCase(addressStatus)) {
					setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR);
					TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
					addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
					addressDoctorResponseVO.setDerivedStatus(addresResponseVo.getDerivedStatus());
					setAddressDoctorResponseVO(addressDoctorResponseVO);
				} else if (addressStatus != null && TRUConstants.CONNECTION_ERROR.equalsIgnoreCase(addressStatus)) {
					setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_CONNECTION_ERROR);
					TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
					addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
					addressDoctorResponseVO.setDerivedStatus(TRUConstants.YES);
					setAddressDoctorResponseVO(addressDoctorResponseVO);
				}
			}
		} else if (!isEnableAddressDoctor && !isAddressValidated) {
			setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR);
			TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
			addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
			addressDoctorResponseVO.setDerivedStatus(TRUConstants.YES);
			setAddressDoctorResponseVO(addressDoctorResponseVO);
		}

		return isAddressValidated;
	}	
	
	/**
	 * This method is used to add calculate tax parameter.
	 * 
	 * @return Map
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected Map createRepriceParameterMap() {
		if (isLoggingDebug()) {
			vlogDebug("TRUCommitOrderFormHandler (createRepriceParameterMapToSkipTaxCal) : BEGIN");
		}
		Map parameters;
		parameters = super.createRepriceParameterMap();
		if (parameters == null) {
			parameters = new HashMap();
		}
		parameters.put(TRUTaxwareConstant.CALC_TAX, Boolean.TRUE);
		if (isLoggingDebug()) {
			vlogDebug("TRUCommitOrderFormHandler (createRepriceParameterMapToSkipTaxCal) : END");
		}
		return parameters;
	}
	
	/**
	 * THis method is used to get the financing details by calling promo service.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @return true, if successful
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	public boolean handlePopulateFinancingDetails(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN :: TRUBillingInforFormHandler.handlePopulateFinancingDetails method..");
		}
		
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "TRUBillingInfoFormHandler.handlePopulateFinancingDetails";
		boolean hasException=false;
		
		TRUPropertyManager propertyManager = getPropertyManager();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			resetFormExceptions();
	   boolean lEnabledRadialPayment = enableRadialCreditCard();
	   if(lEnabledRadialPayment){
		try {
			 if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.startOperation(myHandleMethod);
				}
			tr = ensureTransaction();
			synchronized (getOrder()) {
				if (getCreditCardInfoMap().get(propertyManager.getCreditCardNumberPropertyName()).equals(
						getTRUConfiguration().getCardNumberPLCC()) && !isRUSCard()) {
					getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationMonthPropertyName(),
							getTRUConfiguration().getExpMonthAdjustmentForPLCC());
					getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationYearPropertyName(),
							getTRUConfiguration().getExpYearAdjustmentForPLCC());
				}

				//getValidator().validate(TRUConstants.MY_ACCOUNT_ADD_CARD_WITHOUT_ADDR_FORM,	this);
				TRUProfileManager profileManager = getBillingHelper().getProfileManager();
				boolean isCardExpired = profileManager.validateCardExpiration(
						getCreditCardInfoMap().get(TRUConstants.EXPIRATION_MONTH),
						getCreditCardInfoMap().get(TRUConstants.EXPIRATION_YEAR));
				if(isCardExpired) {
					hasException=true;
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CARD_EXPIRED, null);
					getErrorHandler().processException(mVe, this);
				}
				
				if (!getFormError()) {
					Address billingAddress = null;
					if(isNewBillingAddress() || isEditBillingAddress()) {
						billingAddress = getAddressInputFields();
					} else {
						billingAddress  = ((TRUOrderImpl)getOrder()).getBillingAddress();
					}
					
					TRUCreditCardStatus creditCardStatus = getShippingHelper().getPurchaseProcessHelper()
							.verifyPreAuthorization(SiteContextManager.getCurrentSite().getId(), getCreditCardInfoMap(),
									getProfile().getRepositoryId(), isSavedCard(),(TRUOrderImpl)getOrder(), billingAddress);
					setPromoResponseCode(creditCardStatus.getResponseCode());
					setPromoText(creditCardStatus.getPromoText());
					setPromoFinanceRate(creditCardStatus.getPromoFinanceRate());
					
					if (creditCardStatus != null && !creditCardStatus.getTransactionSuccess() && creditCardStatus.getErrorMessage()!= null) {
						mVe.addValidationError(creditCardStatus.getErrorMessage(), null);
						getErrorHandler().processException(mVe, this);
					}
					
				  }else{
					  hasException=true;
				  }
				 }
			     }
			  finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (javax.transaction.SystemException e) {
							
							if (isLoggingError()) {
								logError("SystemException in @Class::TRUBillingInfoFormHandler::@method::handlePopulateFinancingDetails()", e);
							}
						}
				}
					commitTransaction(tr);
				}
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.endOperation(myHandleMethod);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
	     }else{
	    	 
	    	 mVe.addValidationError(TRUCheckoutConstants.TRU_ERROR_RADIAL_CREDIT_CARD_DISABLED, null);
				getErrorHandler().processException(mVe, this);
	     }
		}
		if (isLoggingDebug()) {
			vlogDebug("END :: TRUBillingInforFormHandler.handlePopulateFinancingDetails method..");
		}
		if (isRestService()) {
			return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
		} else {
			return false;
		}
	}
	
	/**
	 * Handle update order with existing card.
	 *
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleChangeBillingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start of handleChangeBillingAddress method");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "TRUBillingInfoFormHandler.handleChangeBillingAddress";
		boolean hasException=false;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			resetFormExceptions();
			if(isRestService()) {
				preChangeBillingAddress(pRequest, pResponse);
				if (getFormError()) {
					if (isLoggingDebug()) {
						vlogDebug("Redirecting due to form error in TRUBillingInfoFormHandler.preChangeBillingAddress method");
					}
					return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
				}
			}
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					if (!getFormError()) {
						if (isLoggingDebug()) {
							vlogDebug("Start of handleChangeBillingAddress method with BillingAddressNickName ",getBillingAddressNickName());
						}
						String billingAddressNickName = getBillingAddressNickName();
						if (StringUtils.isNotBlank(billingAddressNickName)) {
							ContactInfo orderBillingAddress = null;
							orderBillingAddress = (ContactInfo) ((TRUHardgoodShippingGroup) getShippingGroupMapContainer().getShippingGroupMap().get(billingAddressNickName)).getShippingAddress();
							if (orderBillingAddress != null) {
								ContactInfo billingAddress  = new ContactInfo();
								OrderTools.copyAddress(orderBillingAddress, billingAddress);
								//Adding the billing address to the credit card. 
								((TRUOrderImpl)getOrder()).setBillingAddress(billingAddress);
								((TRUOrderImpl)getOrder()).setBillingAddressNickName(billingAddressNickName);
								String selectedPaymentGroupType = null;
								List<PaymentGroup> paymentGroups = getOrder().getPaymentGroups();
								for (PaymentGroup paymentGroup : paymentGroups) {
									if (paymentGroup != null) {
										selectedPaymentGroupType = paymentGroup.getPaymentGroupClassType();
										if(selectedPaymentGroupType.equalsIgnoreCase(TRUCheckoutConstants.CREDITCARD)){
											((CreditCard)paymentGroup).setBillingAddress(billingAddress);
										}
									}
								}
							}
							/*runProcessRepriceOrder(getOrder(), getUserPricingModels(), getUserLocale(pRequest, pResponse),
									getProfile(), createRepriceParameterMap());*/
							getOrderManager().updateOrder(getOrder());
						}
					}
				}
			} catch (CommerceException comExp) {
				hasException=true;
				if (isLoggingError()) {
					logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handleUpdateOrderWithExistingCard()", comExp);
				}
			}/* catch (RunProcessException rpcExp) {
				hasException=true;
				if (isLoggingError()) {
					logError("Exception in @Class::TRUBillingInfoFormHandler::@method::handleUpdateOrderWithExistingCard()", rpcExp);
				}
			} */ finally {
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (javax.transaction.SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in @Class::TRUBillingInfoFormHandler::@method::handleUpdateOrderWithExistingCard()", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of handleUpdateOrderWithExistingCard method");
		}
		return checkFormRedirect(getCommonSuccessURL(),	getCommonErrorURL(), pRequest, pResponse);
	}
	
	/**
	 * Handle get NONCE.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @return true, if successful
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SystemException the system exception
	 * @throws JSONException 
	 */
	public boolean handleGetNONCE(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, SystemException, JSONException {
		if (isLoggingDebug()) {
			vlogDebug("Start of handleGetNONCE method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String myHandleMethod = TRUConstants.HANDLE_GET_NONCE;
		
		
		TRURadialGetNonceResponse radialGetNonceResponse = new TRURadialGetNonceResponse();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			
		 if (PerformanceMonitor.isEnabled()){
				PerformanceMonitor.startOperation(myHandleMethod);
			}
		 
		getPaymentManager().getPaymentTools().getCreditCardProcessor().getRadialNonceDetails(radialGetNonceResponse, getOrder(), getProfile());
		setRadialPaymentErrorKey(radialGetNonceResponse.getRadilalPaymentErrorKey());
		setNonceValue(radialGetNonceResponse.getNonce());
		setExpiresInSecondsValue( radialGetNonceResponse.getExpiresInSeconds());
		setJwtValue( radialGetNonceResponse.getJwt());
		if (PerformanceMonitor.isEnabled()){
			PerformanceMonitor.endOperation(myHandleMethod);
		}
		
		}
		if (rrm != null) {
			rrm.removeRequestEntry(myHandleMethod);
		}
		if (isLoggingDebug()) {
			vlogDebug("End of handleGetNONCE method");
		}
		if (isRestService()) {
			return checkFormRedirect(getNonceSuccessURL(),	null, pRequest, pResponse);	
		}
		return false;
	}
	
	/**
	 * Holds the Radial payment processor.
	 */
	private TRURadialPaymentProcessor mRadialProcessor;

	/**
	 * @return the radialProcessor
	 */
	public TRURadialPaymentProcessor getRadialProcessor() {
		return mRadialProcessor;
	}

	/**
	 * @param pRadialProcessor the radialProcessor to set
	 */
	public void setRadialProcessor(TRURadialPaymentProcessor pRadialProcessor) {
		mRadialProcessor = pRadialProcessor;
	}

	private String mNonceValue;
	private String mExpiresInSecondsValue;
	private String mJwtValue;
	/**
	 * @return the nonceValue
	 */
	public String getNonceValue() {
		return mNonceValue;
	}

	/**
	 * @param pNonceValue the nonceValue to set
	 */
	public void setNonceValue(String pNonceValue) {
		mNonceValue = pNonceValue;
	}

	/**
	 * @return the expiresInSecondsValue
	 */
	public String getExpiresInSecondsValue() {
		return mExpiresInSecondsValue;
	}

	/**
	 * @param pExpiresInSecondsValue the expiresInSecondsValue to set
	 */
	public void setExpiresInSecondsValue(String pExpiresInSecondsValue) {
		mExpiresInSecondsValue = pExpiresInSecondsValue;
	}

	/**
	 * @return the jwtValue
	 */
	public String getJwtValue() {
		return mJwtValue;
	}

	/**
	 * @param pJwtValue the jwtValue to set
	 */
	public void setJwtValue(String pJwtValue) {
		mJwtValue = pJwtValue;
	}

	/**
	 * @return the mNonceSuccessURL
	 */
	public String getNonceSuccessURL() {
		return mNonceSuccessURL;
	}

	/**
	 * @param pNonceSuccessURL the mNonceSuccessURL to set
	 */
	public void setNonceSuccessURL(String pNonceSuccessURL) {
		this.mNonceSuccessURL = pNonceSuccessURL;
	}

	/**
	 * @return the mNonceErrorURL
	 */
	public String getNonceErrorURL() {
		return mNonceErrorURL;
	}

	/**
	 * @param pNonceErrorURL the mNonceErrorURL to set
	 */
	public void setNonceErrorURL(String pNonceErrorURL) {
		this.mNonceErrorURL = pNonceErrorURL;
	}
	/**
	 * Pre ChangeBillingAddress.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	protected void preChangeBillingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start: TRUBillingInfoFormHandler.preChangeBillingAddress()");
		}
		if(StringUtils.isBlank(getBillingAddressNickName()) || getShippingGroupMapContainer().getShippingGroupMap().get(getBillingAddressNickName()) == null) {
			mVe.addValidationError(TRUErrorKeys.TRU_BILLING_ADDRESS_NICKNAME_INVALID, null);
			getErrorHandler().processException(mVe, this);
			return;
		}
		if (isLoggingDebug()) {
			vlogDebug("End: TRUBillingInfoFormHandler.preChangeBillingAddress()");
		}
	}
	
	/**
	 * Handle validate credit card bin ranges.
	 *
	 * @param pRequest - DynamoHttpServletRequest
	 * @param pResponse - DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SystemException the system exception
	 * @throws JSONException the JSON exception
	 */
	public boolean handleValidateCreditCardBinRanges(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, SystemException, JSONException {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUBillingInfoFormHandler/handleValidateCreditCardBinRanges method");
		}
		String sixDigitCardNumber = pRequest.getParameter(TRUCommerceConstants.CARD_NUMBER);
		boolean isBinRangeExist = ((TRUProfileTools) getOrderManager().getOrderTools().getProfileTools()).isBingeRangeAvailable(sixDigitCardNumber);
		if (isLoggingDebug()) {
			vlogDebug("BinRangesExist :: "+isBinRangeExist);
			vlogDebug("Start of TRUBillingInfoFormHandler/handleValidateCreditCardBinRanges method");
		}
		if(isBinRangeExist){
			return true;
		}else{
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_COMMON_CC_BIN_RANGES_NOT_AVAILABLE, null);
			getErrorHandler().processException(mVe, this);
			return false;
		}
	}
	
	/**
	 * Enable RadialCreditCard
	 *
	 * @return true, if successful
	 */
	private boolean enableRadialCreditCard() {
		if (isLoggingDebug()) {
			vlogDebug("@class:TRUBillingInfoFormHandler/enableRadialCreditCard method start");
		}
		boolean lEnabled = Boolean.FALSE;
		Site site=null;
		site=SiteContextManager.getCurrentSite();
		if(site != null && site.getId().equalsIgnoreCase(TRUCommerceConstants.SOS)) {
			lEnabled = getSosIntegrationConfiguration().isEnableRadialCreditCard();
			if (isLoggingDebug()) {
				vlogDebug("@class:TRUBillingInfoFormHandler/enableRadialCreditCard is  radial payment  service enabled in SOS : {0} ",lEnabled);
			}
		} else{
			lEnabled = getIntegrationConfig().isEnableRadialCreditCard();
			if (isLoggingDebug()) {
				vlogDebug("@class:TRUBillingInfoFormHandler/enableRadialCreditCard is  radial payment  service enabled in store : {0} ",lEnabled);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@class:TRUBillingInfoFormHandler/enableRadialCreditCard method end");
		}
		return lEnabled;
	}

	/**
	 * This method is used to get sosIntegrationConfiguration.
	 * @return sosIntegrationConfiguration String
	 */
	public TRUSOSIntegrationConfig getSosIntegrationConfiguration() {
		return mSosIntegrationConfiguration;
	}

	/**
	 *This method is used to set sosIntegrationConfiguration.
	 *@param pSosIntegrationConfiguration String
	 */
	public void setSosIntegrationConfiguration(
			TRUSOSIntegrationConfig pSosIntegrationConfiguration) {
		mSosIntegrationConfiguration = pSosIntegrationConfiguration;
	}

	/**
	 * This method is used to get integrationConfig.
	 * @return integrationConfig String
	 */
	public TRUIntegrationConfiguration getIntegrationConfig() {
		return mIntegrationConfig;
	}

	/**
	 *This method is used to set integrationConfig.
	 *@param pIntegrationConfig String
	 */
	public void setIntegrationConfig(TRUIntegrationConfiguration pIntegrationConfig) {
		mIntegrationConfig = pIntegrationConfig;
	}
	
	/** The m scheme detection util. */
	private TRUSchemeDetectionUtil mSchemeDetectionUtil;

	
	/**
	 * Gets the scheme detection util.
	 *
	 * @return the scheme detection util
	 */
	public TRUSchemeDetectionUtil getSchemeDetectionUtil() {
		return mSchemeDetectionUtil;
	}

	/**
	 * Sets the scheme detection util.
	 *
	 * @param pSchemeDetectionUtil the new scheme detection util
	 */
	public void setSchemeDetectionUtil(TRUSchemeDetectionUtil pSchemeDetectionUtil) {
		this.mSchemeDetectionUtil = pSchemeDetectionUtil;
	}
	
}
