/**
 * 
 */
package com.tru.commerce.order.purchase;

import atg.commerce.order.purchase.AddCommerceItemInfo;

/**
 * This class having extra attributes related to Commerce Item.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUAddCommerceItemInfo extends AddCommerceItemInfo {

	/** Property to hold channel. */
	private String mChannelType;

	/** Property to hold channel id. */
	private String mChannelId;

	/** Property to hold channel user name. */
	private String mChannelUserName;

	/** Property to Channel co user name. */
	private String mChannelCoUserName;

	
	/** The Channel name. */
	private String mChannelName;
	
	/** The Source channel. */
	private String mSourceChannel;

	/** The Donation amount. */
	private Double mDonationAmount;
	
	/** The m warehouse location code. */
	private String mWarehouseLocationCode;
	
	/**
	 * Gets the source channel.
	 *
	 * @return the source channel
	 */
	public String getSourceChannel() {
		return mSourceChannel;
	}

	/**
	 * Sets the source channel.
	 *
	 * @param pSourceChannel the new source channel
	 */
	public void setSourceChannel(String pSourceChannel) {
		mSourceChannel = pSourceChannel;
	}

	/**
	 * Gets the donation amount.
	 *
	 * @return the donation amount
	 */
	public Double getDonationAmount() {
		return mDonationAmount;
	}

	/**
	 * Sets the donation amount.
	 *
	 * @param pDonationAmount the new donation amount
	 */
	public void setDonationAmount(Double pDonationAmount) {
		mDonationAmount = pDonationAmount;
	}

	/**
	 * Gets the channel.
	 * 
	 * @return the channel
	 */
	public String getChannelType() {
		return mChannelType;
	}

	/**
	 * Sets the channel.
	 * 
	 * @param pChannelType
	 *            the new channel
	 */
	public void setChannelType(String pChannelType) {
		this.mChannelType = pChannelType;
	}

	/**
	 * Gets the channel id.
	 * 
	 * @return the channel id
	 */
	public String getChannelId() {
		return mChannelId;
	}

	/**
	 * Sets the channel id.
	 * 
	 * @param pChannelId
	 *            the new channel id
	 */
	public void setChannelId(String pChannelId) {
		this.mChannelId = pChannelId;
	}

	/**
	 * Gets the channel user name.
	 * 
	 * @return the channel user name
	 */
	public String getChannelUserName() {
		return mChannelUserName;
	}

	/**
	 * Sets the channel user name.
	 * 
	 * @param pChannelUserName
	 *            the new channel user name
	 */
	public void setChannelUserName(String pChannelUserName) {
		this.mChannelUserName = pChannelUserName;
	}



	/**
	 * Gets the channel co user name.
	 * 
	 * @return the channel co user name
	 */
	public String getChannelCoUserName() {
		return mChannelCoUserName;
	}

	/**
	 * Sets the channel co user name.
	 * 
	 * @param pChannelCoUserName
	 *            the new channel co user name
	 */
	public void setChannelCoUserName(String pChannelCoUserName) {
		mChannelCoUserName = pChannelCoUserName;
	}


	/**
	 * Gets the channel name.
	 *
	 * @return the channel name
	 */
	public String getChannelName() {
		return mChannelName;
	}

	/**
	 * Sets the channel name.
	 *
	 * @param pChannelName the new channel name
	 */
	public void setChannelName(String pChannelName) {
		mChannelName = pChannelName;
	}

	/**
	 * Gets the warehouse location code.
	 *
	 * @return the warehouse location code
	 */
	public String getWarehouseLocationCode() {
		return mWarehouseLocationCode;
	}

	/**
	 * Sets the warehouse location code.
	 *
	 * @param pWarehouseLocationCode the new warehouse location code
	 */
	public void setWarehouseLocationCode(String pWarehouseLocationCode) {
		mWarehouseLocationCode = pWarehouseLocationCode;
	}

	
}
