package com.tru.commerce.order.purchase;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.order.ShippingGroup;
import atg.commerce.order.purchase.ShippingGroupDroplet;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.core.util.StringUtils;
import atg.service.dynamo.LangLicense;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.common.TRUConstants;

/**
 * @author RRATHIRAJU
 *
 */
public class TRUShippingGroupDroplet extends ShippingGroupDroplet{
	
	/** Constant for Channel shipping group. */
	public static final String  STR_CHANNEL_SHIPGRP_NAME = "channelHardgoodShippingGroup";
	/** Constant for Purchase resources. */
	public static final String STR_PURCHASE_RESOURCES = "atg.commerce.order.purchase.PurchaseProcessResources";
	/** Constant for defaultShipping. */
	public static final String DEFAULT_SHIPPING = "defaultShipping";
	/** Constant for NULL ORDER. */
	public static final String STR_NULL_ORDER = "nullOrder";
	/** Constant for NULL PROFILE. */
	public static final String STR_NULL_PROFILE = "nullProfile";
	/** Constant for Shipping groups */
	public static final String SHIPPING_GROUPS = "shippingGroups";
	/** Constant for Shipping Infos */
	public static final String SHIPPING_INFOS = "shippingInfos";
	
	private static ResourceBundle sResourceBundle = LayeredResourceBundle.getBundle(STR_PURCHASE_RESOURCES, LangLicense.getLicensedDefault());
	/* (non-Javadoc)
	 * @see atg.commerce.order.purchase.ShippingGroupDroplet#service(atg.servlet.DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,IOException {
	    initializeRequestParameters(pRequest);
	    if (getOrder() == null) {
	      if (isLoggingError()) {
	        logError(ResourceUtils.getMsgResource(STR_NULL_ORDER, STR_PURCHASE_RESOURCES, sResourceBundle));
	      }
	      return;
	    }
	    if (getProfile() == null) {
	      if (isLoggingError()) {
	        logError(ResourceUtils.getMsgResource(STR_NULL_PROFILE, STR_PURCHASE_RESOURCES, sResourceBundle));
	      }
	      return;
	    }
		if ((isClearAll()) || (isClearShippingGroups())) {
			//Start : TUW-55621 code changes.
			if (STR_CHANNEL_SHIPGRP_NAME.equalsIgnoreCase(getShippingGroupTypes())) {
				//In case of anonymous flow channel address removing after placing an order. 
				((TRUCommerceItemShippingInfoTools)getCommerceItemShippingInfoTools()).
				clearAllChannelShippingGroups(getCommerceItemShippingInfoContainer(), getShippingGroupMapContainer());
				//End : TUW-55621 code changes.
			}else {
				getCommerceItemShippingInfoTools().clearShippingGroups(getCommerceItemShippingInfoContainer(),getShippingGroupMapContainer());
			}
			if (isLoggingDebug()) {
				logDebug("ShippingGroupDroplet removing all ShippingGroups");
			}
		}
	    if ((isClearAll()) || (isClearShippingInfos())) {
	      getCommerceItemShippingInfoTools().clearCommerceItemShippingInfos(getCommerceItemShippingInfoContainer(), getShippingGroupMapContainer());
	      if (isLoggingDebug()) {
	        logDebug("ShippingGroupDroplet removing all ShippingInfos");
	      }
	    }
	    if (isInitShippingGroups()) {
	      initializeUserShippingMethods(pRequest, getProfile());
	    }
	    removeDeletedShippingGroups();
	    if (isInitBasedOnOrder())
	    {
	      initializeBasedOnOrder(getProfile(), getOrder());
	      if (!(isAnyShippingInfoExistsInContainter())) {
	        if (isLoggingDebug()){
	          logDebug("CommerceItemShippingInfoContainer does not have any cisi.");
	        }
	        initializeCommerceItemShippingInfos(getProfile(), getOrder());
	      }
	    }
	    else if (isInitShippingInfos()) {
	      initializeCommerceItemShippingInfos(getProfile(), getOrder());
	    }
	    String channelType = (String) pRequest.getHeader(TRUConstants.API_CHANNEL);
	    if(StringUtils.isNotBlank(channelType)) {
	    	Map shippingGroupMapForAPIDisplay = new HashMap<>();
		    final Map shippingGroupMapForDisplay = getShippingGroupMapContainer().getShippingGroupMap();
			if(shippingGroupMapForDisplay != null && !shippingGroupMapForDisplay.isEmpty()) {
			    final Set<String> shippingGroupKeys = shippingGroupMapForDisplay.keySet();
				for(String shippingGroupKey : shippingGroupKeys) {
					final ShippingGroup shippingGroup = (ShippingGroup) shippingGroupMapForDisplay.get(shippingGroupKey);
					if(shippingGroup instanceof TRUHardgoodShippingGroup) {
						final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
						if(!hardgoodShippingGroup.isBillingAddress()) {
							shippingGroupMapForAPIDisplay.put(shippingGroupKey, shippingGroup);
						}
					}
				}
			 }
			 pRequest.setParameter(SHIPPING_GROUPS, shippingGroupMapForAPIDisplay);
	    }
	    else {
	    	pRequest.setParameter(SHIPPING_GROUPS, getShippingGroupMapContainer().getShippingGroupMap());
	    }
	    //Start : Added for rest service
	    pRequest.setParameter(DEFAULT_SHIPPING, getShippingGroupMapContainer().getDefaultShippingGroupName());
	    //End : Added for rest service
	    pRequest.setParameter(SHIPPING_INFOS, getCommerceItemShippingInfoContainer().getCommerceItemShippingInfoMap());
	    pRequest.setParameter(TRUCheckoutConstants.ORDER, getOrder());
	    pRequest.serviceParameter(TRUCheckoutConstants.OPARAM_OUTPUT, pRequest, pResponse);
	}

}
