package com.tru.commerce.order.purchase;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.transaction.Transaction;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.purchase.PurchaseProcessFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.repository.RepositoryException;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.service.pipeline.PipelineResult;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.endeca.infront.shaded.org.apache.commons.lang.math.NumberUtils;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUGiftCard;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardInfo;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardStatus;
import com.tru.commerce.payment.TRUPaymentConstants;
import com.tru.commerce.payment.TRUPaymentManager;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.cml.SystemException;
import com.tru.common.cml.ValidationExceptions;
import com.tru.errorhandler.fhl.IErrorHandler;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;
import com.tru.logging.TRUSETALogger;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.validator.fhl.IValidator;

/**
 * The Class TRUGiftCardFormHandler.
 *
 * @author PA
 * @version 1.0
 */
public class TRUGiftCardFormHandler extends PurchaseProcessFormHandler {
	
	/** The m gift card email. */
	private String mGiftCardEmail;
	
	/** The m csc request. */
	private boolean mCSCRequest;
	

	/**
	 * property to hold page name.
	 */
	private String mPageName;
	/**
	 * List to hold Error Keys.
	 */
	private List<String> mErrorKeyList;

	/** member holds the gift card number. */
	private String mGiftCardNumber;

	/** member holds the gift card pin. */
	private String mGiftCardPin;

	/** holds the gift card info. */
	private TRUGiftCardInfo mGiftCardInfo;

	/** property to hold tRUConfiguration. */
	private TRUConfiguration mTRUConfiguration;

	/**
	 * Constant to hold mGiftCardBalance.
	 */
	private double mGiftCardBalance;

	/**
	 * Constant to hold mCardId.
	 */
	private String mCardId;

	/**
	 * Holds the PaymentManager.
	 */
	private TRUPaymentManager mPaymentManager;

	/** property to hold mCommonSuccessURL. */
	private String mCommonSuccessURL;

	/** property to hold mCommonErrorURL. */
	private String mCommonErrorURL;

	/** property to hold ValidationException. */
	private ValidationExceptions mVe = new ValidationExceptions();

	/**
	 * mValidator.
	 */
	private IValidator mValidator;

	/** property to hold the mErrorHandler. */
	private IErrorHandler mErrorHandler;

	/** mEnableAdjustPaymentGroups. */
	private boolean mEnableAdjustPaymentGroups;

	/** property to hold mCommonSuccessURL. */
	private String mGiftCardBalanceSuccessURL;

	/** property to hold mCommonErrorURL. */
	private String mGiftCardBalanceErrorURL;
	
	/** The m SETA logger. */
	private TRUSETALogger mSETALogger;
	/**
	 * property to hold mRestService.
	 */
	private boolean mRestService;
	/**
	 * Checks if is rest service.
	 * 
	 * @return the mRestService
	 */
	public boolean isRestService() {
		return mRestService;
	}

	/**
	 * Sets the rest service.
	 * 
	 * @param pRestService
	 *            the mRestService to set
	 */
	public void setRestService(boolean pRestService) {
		this.mRestService = pRestService;
	}
	/**
	 * Gets the gift card email.
	 *
	 * @return the gift card email
	 */
	public String getGiftCardEmail() {
		return mGiftCardEmail;
	}

	/**
	 * Sets the gift card email.
	 *
	 * @param pGiftCardEmail the new gift card email
	 */
	public void setGiftCardEmail(String pGiftCardEmail) {
		this.mGiftCardEmail = pGiftCardEmail;
	}

	
	/**
	 * Checks if is cSC request.
	 *
	 * @return true, if is cSC request
	 */
	public boolean isCSCRequest() {
		return mCSCRequest;
	}

	/**
	 * Sets the cSC request.
	 *
	 * @param pCSCRequest the new cSC request
	 */
	public void setCSCRequest(boolean pCSCRequest) {
		this.mCSCRequest = pCSCRequest;
	}

	/**
	 * Gets the gift card balance error url.
	 * 
	 * @return the GiftCardBalanceErrorURL
	 */
	public String getGiftCardBalanceErrorURL() {
		return mGiftCardBalanceErrorURL;
	}

	/**
	 * Sets the gift card balance error url.
	 * 
	 * @param pGiftCardBalanceErrorURL
	 *            giftCardBalance error url
	 */
	public void setGiftCardBalanceErrorURL(String pGiftCardBalanceErrorURL) {
		this.mGiftCardBalanceErrorURL = pGiftCardBalanceErrorURL;
	}

	/**
	 * Gets the gift card balance success url.
	 * 
	 * @return the GiftCardBalanceSuccessURL
	 */
	public String getGiftCardBalanceSuccessURL() {
		return mGiftCardBalanceSuccessURL;
	}

	/**
	 * Sets the gift card balance success url.
	 * 
	 * @param pGiftCardBalanceSuccessURL
	 *            the giftCardBalanceSuccessURL to set
	 */
	public void setGiftCardBalanceSuccessURL(String pGiftCardBalanceSuccessURL) {
		this.mGiftCardBalanceSuccessURL = pGiftCardBalanceSuccessURL;
	}

	/**
	 * Checks if is enable adjust payment groups.
	 * 
	 * @return the mEnableAdjustPaymentGroups
	 */
	public boolean isEnableAdjustPaymentGroups() {
		return mEnableAdjustPaymentGroups;
	}

	/**
	 * Sets the enable adjust payment groups.
	 * 
	 * @param pEnableAdjustPaymentGroups
	 *            the mEnableAdjustPaymentGroups to set
	 */
	public void setEnableAdjustPaymentGroups(boolean pEnableAdjustPaymentGroups) {
		mEnableAdjustPaymentGroups = pEnableAdjustPaymentGroups;
	}

	/**
	 * Gets the error handler.
	 * 
	 * @return the IErrorHandler
	 */
	public IErrorHandler getErrorHandler() {
		return mErrorHandler;
	}

	/**
	 * Sets the error handler.
	 * 
	 * @param pErrorHandler
	 *            the IErrorHandler to set
	 */
	public void setErrorHandler(IErrorHandler pErrorHandler) {
		this.mErrorHandler = pErrorHandler;
	}

	/**
	 * getValidator().
	 * 
	 * @return the validator
	 */
	public IValidator getValidator() {
		return mValidator;
	}

	/**
	 * Sets the validator.
	 * 
	 * @param pValidator
	 *            - the validator to set
	 */
	public void setValidator(IValidator pValidator) {
		this.mValidator = pValidator;
	}

	/**
	 * Gets the TRU configuration.
	 * 
	 * @return the tRUConfiguration
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * Sets the TRU configuration.
	 * 
	 * @param pTRUConfiguration
	 *            the tRUConfiguration to set
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		mTRUConfiguration = pTRUConfiguration;
	}

	/**
	 * Gets the gift card info.
	 * 
	 * @return the giftCardInfo
	 */
	public TRUGiftCardInfo getGiftCardInfo() {
		return mGiftCardInfo;
	}

	/**
	 * Sets the gift card info.
	 * 
	 * @param pGiftCardInfo
	 *            the giftCardInfo to set
	 */
	public void setGiftCardInfo(TRUGiftCardInfo pGiftCardInfo) {
		this.mGiftCardInfo = pGiftCardInfo;
	}

	/**
	 * Gets the gift card number.
	 * 
	 * @return the giftCardNumber
	 */
	public String getGiftCardNumber() {
		return mGiftCardNumber;
	}

	/**
	 * Sets the gift card number.
	 * 
	 * @param pGiftCardNumber
	 *            the giftCardNumber to set
	 */
	public void setGiftCardNumber(String pGiftCardNumber) {
		mGiftCardNumber = pGiftCardNumber;
	}

	/**
	 * Gets the card id.
	 * 
	 * @return the cardId
	 */
	public String getCardId() {
		return mCardId;
	}

	/**
	 * Sets the card id.
	 * 
	 * @param pCardId
	 *            the cardId to set
	 */
	public void setCardId(String pCardId) {
		mCardId = pCardId;
	}

	/**
	 * Sets the gift card balance.
	 * 
	 * @param pGiftCardBalance
	 *            the giftCardBalance to set
	 */
	public void setGiftCardBalance(double pGiftCardBalance) {
		mGiftCardBalance = pGiftCardBalance;
	}

	/**
	 * Gets the gift card balance.
	 * 
	 * @return the giftCardBalance
	 */
	public double getGiftCardBalance() {
		return mGiftCardBalance;
	}

	/**
	 * Getting the PaymentManager.
	 * 
	 * @return the PaymentManager
	 */
	public TRUPaymentManager getPaymentManager() {
		return mPaymentManager;
	}

	/**
	 * Setting the value for the mPaymentManager.
	 * 
	 * @param pPaymentManager
	 *            - the PaymentManager
	 */
	public void setPaymentManager(TRUPaymentManager pPaymentManager) {
		this.mPaymentManager = pPaymentManager;
	}

	/**
	 * Gets the common success url.
	 * 
	 * @return the commonSuccessURL
	 */
	public String getCommonSuccessURL() {
		return mCommonSuccessURL;
	}

	/**
	 * Sets the common success url.
	 * 
	 * @param pCommonSuccessURL
	 *            the commonSuccessURL to set.
	 */
	public void setCommonSuccessURL(String pCommonSuccessURL) {
		this.mCommonSuccessURL = pCommonSuccessURL;
	}

	/**
	 * Gets the common error url.
	 * 
	 * @return the commonErrorURL
	 */
	public String getCommonErrorURL() {
		return mCommonErrorURL;
	}

	/**
	 * Sets the common error url.
	 * 
	 * @param pCommonErrorURL
	 *            common error url
	 */
	public void setCommonErrorURL(String pCommonErrorURL) {
		this.mCommonErrorURL = pCommonErrorURL;
	}

	/**
	 * Gets the gift card pin.
	 * 
	 * @return the giftCardPin
	 */
	public String getGiftCardPin() {
		return mGiftCardPin;
	}

	/**
	 * Sets the gift card pin.
	 * 
	 * @param pGiftCardPin
	 *            the giftCardPin to set
	 */
	public void setGiftCardPin(String pGiftCardPin) {
		mGiftCardPin = pGiftCardPin;
	}

	/**
	 * Handle fetch gift card balance.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean true/false
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleFetchGiftCardBalance(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start of TRUGiftCardFormHandler/handleFetchGiftCardBalance method");
		}
		final String myHandleMethod = TRUCommerceConstants.HANDLE_FETCH_GIFT_CARD_BALANCE;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			 if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.startOperation(myHandleMethod);
				}
			resetFormExceptions();
			if (isLoggingDebug()) {
				logDebug("getGiftCardNumber() = " + getGiftCardNumber());
			}
			try {
				validateGiftCard(getGiftCardNumber(), getGiftCardPin());
				if (!getFormError()) {
					String oldCurrentTab = null;
					if (getOrder() instanceof TRUOrderImpl) { 
						oldCurrentTab = ((TRUOrderImpl)getOrder()).getCurrentTab();
						((TRUOrderImpl)getOrder()).setCurrentTab(TRUCommerceConstants.MYACCOUNT);
					}
					processGetGiftCardBalance();
					//Resetting to the old value
					if (getOrder() instanceof TRUOrderImpl) { 
						((TRUOrderImpl)getOrder()).setCurrentTab(oldCurrentTab);
					}
					if (isLoggingDebug()) {
						logDebug("Gift Card Balance is " + getGiftCardBalance());
					}
				}
			} catch (ServletException ex) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_WHILE_PROCESSING_REQUEST, null);
				getErrorHandler().processException(mVe, this);
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("Exception in @Class::TRUGiftCardFormHandler::@method::handleFetchGiftCardBalance()", ex);
				}
			} finally {
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.endOperation(myHandleMethod);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End of TRUGiftCardFormHandler/handleFetchGiftCardBalance method");
		}
		return checkFormRedirect(getGiftCardBalanceSuccessURL(), getGiftCardBalanceErrorURL(), pRequest, pResponse);
	}

	/**
	 * Process get gift card balance.
	 * 
	 * @throws ServletException
	 *             ServletException
	 */
	private void processGetGiftCardBalance() throws ServletException {
		final TRUGiftCardInfo giftCardInfo = new TRUGiftCardInfo();
		giftCardInfo.setOrder(getOrder());
		giftCardInfo.setGiftCardNumber(getGiftCardNumber());
		giftCardInfo.setPin(getGiftCardPin());
		giftCardInfo.setCardHolderName(TRUCheckoutConstants.JOE_SMITH);
		TRUGiftCardStatus giftCardStatus = getPaymentManager().getGiftCardBalance(giftCardInfo);
		if (isLoggingDebug()) {
			logDebug("giftCardStatus = " + giftCardStatus);
		}
		if(!giftCardStatus.getTransactionSuccess()) {
			if (!StringUtils.isBlank(giftCardStatus.getErrorMessage())) {
				mVe.addValidationError(giftCardStatus.getErrorMessage(), null);
				getErrorHandler().processException(mVe, this);
			}
		} else {
			setGiftCardBalance(giftCardStatus.getBalanceAmount().doubleValue());
			if (getGiftCardBalance() == TRUConstants.DOUBLE_ZERO) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_GIFT_CARD_BALANCE_ZERO, null);
				getErrorHandler().processException(mVe, this);
			}
		}
	}

	/**
	 * This method validates the input (gift card number, pin etc) Adds form
	 * errors in case of failed validation.
	 * 
	 * @param pGiftCardNumber
	 *            giftCardNumber
	 * @param pGiftCardPin
	 *            giftCardPin
	 * @throws ServletException
	 *             ServletException
	 */
	private void validateGiftCard(String pGiftCardNumber, String pGiftCardPin) throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Inside into GiftCardFormHandler.validateGiftCard");
		}
	
		if (!getFormError()) {
			if (StringUtils.isBlank(pGiftCardNumber) && StringUtils.isBlank(pGiftCardPin)) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ENTER_GIFT_CARD_NUMBER_AND_PIN, null);
				getErrorHandler().processException(mVe, this);
			} else if (StringUtils.isBlank(pGiftCardNumber)) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ENTER_GIFT_CARD_NUMBER, null);
				getErrorHandler().processException(mVe, this);
			} else 	
				if(!isValidGCNumberLength(pGiftCardNumber)) {
					if (StringUtils.isNotBlank(getPageName()) && getPageName().equals(TRUConstants.CHECKOUT) && 
						getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_CHECKOUT_GCNUMBER_INVALID_CARDNUMBER)) {
						setChannelError(TRUErrorKeys.TRU_ERROR_CHECKOUT_GCNUMBER_INVALID_CARDNUMBER);
					} else {
						mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_GCNUMBER_INVALID_CARDNUMBER, null);
						getErrorHandler().processException(mVe, this);
					}	
					}
				 else if (!NumberUtils.isNumber(pGiftCardNumber)) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ENTER_GIFT_CARD_NUMBER_NUMERIC, null);
				getErrorHandler().processException(mVe, this);
			} else if (StringUtils.isBlank(pGiftCardPin)) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ENTER_GIFT_CARD_PIN, null);
				getErrorHandler().processException(mVe, this);
			} else if (!isValidGCPinLength(pGiftCardPin)) {
				if (StringUtils.isNotBlank(getPageName()) && getPageName().equals(TRUConstants.CHECKOUT) && 
						getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_ACCOUNT_PIN_INVALID)) {
						setChannelError(TRUErrorKeys.TRU_ERROR_ACCOUNT_PIN_INVALID);
					} else {
						mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ENTER_VALID_GIFT_CARD_PIN, null);
						getErrorHandler().processException(mVe, this);
					}	
			} else if (!NumberUtils.isNumber(pGiftCardPin)) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ENTER_GIFT_CARD_PIN_NUMERIC, null);
				getErrorHandler().processException(mVe, this);
			}
		}

	}

	/**
	 * To check the length of the gift card.
	 * 
	 * @param pValue
	 *            card value
	 * @return boolean boolean value
	 */
	public boolean isValidGCNumberLength(String pValue) {
		boolean lbIsValid = true;
		if (pValue.length() < getTRUConfiguration().getGiftCardNumberLength()) {
			lbIsValid = false;
		}
		return lbIsValid;
	}

	/**
	 * To check the length of the pin.
	 * 
	 * @param pValue
	 *            card value
	 * @return boolean boolean value
	 */
	public boolean isValidGCPinLength(String pValue) {
		boolean lbIsValid = true;
		if (pValue.length() < getTRUConfiguration().getGiftCardPinLength()) {
			lbIsValid = false;
		}
		return lbIsValid;
	}

	/**
	 * Handle apply gift card.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean true/false
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleApplyGiftCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Start of TRUGiftCardFormHandler/handleApplyGiftCard method");
		}
		final String myHandleMethod = TRUCommerceConstants.HANDLE_APPLY_GIFT_CARD;
		Transaction tr = null;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			tr = ensureTransaction();
			resetFormExceptions();
			 if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.startOperation(myHandleMethod);
				}
			try {
				synchronized (getOrder()) {
					if(isCSCRequest() && StringUtils.isNotBlank(getGiftCardEmail()))
					{
						((TRUOrderImpl) getOrder()).setEmail(getGiftCardEmail());
					}
					if (isLoggingDebug()) {
						logDebug("getGiftCardNumber() = " + getGiftCardNumber());
					}
					validateGiftCard(getGiftCardNumber(), getGiftCardPin());
					if (!getFormError()) {
						final TRUOrderTools orderTools = (TRUOrderTools) getPaymentGroupManager().getOrderTools();
						final double remainingOrderAmount = orderTools.getRemainingOrderAmount(getOrder());
						if (remainingOrderAmount <= TRUConstants.DOUBLE_ZERO) {
							mVe.addValidationError(TRUErrorKeys.TRU_ERROR_INSUFFICIENT_ORDER_AMOUNT, null);
							getErrorHandler().processException(mVe, this);
							return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
						}
						checkOrderPaymentGroups(getOrder());
						if (getFormError()) {
							return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
						}
						processGetGiftCardBalance();
						if (getFormError()) {
							return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
						}
						if (!getFormError()) {
							if (getGiftCardBalance() == TRUConstants.DOUBLE_ZERO) {
								mVe.addValidationError(TRUErrorKeys.TRU_ERROR_GIFT_CARD_BALANCE_ZERO, null);
								getErrorHandler().processException(mVe, this);
								return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
							}
							applyGiftCard(getGiftCardBalance(), remainingOrderAmount);
							runProcessRepriceOrder(getOrder(),
									getUserPricingModels(), getUserLocale(pRequest, pResponse),
									getProfile(), createRepriceParameterMap());
							adjustOrderPaymentGroups();
							getOrderManager().updateOrder(getOrder());
							//SETA log for adding gift card
							String login = (String)getProfile().getPropertyValue(getOrderManager().getOrderTools().getProfileTools().getPropertyManager().getLoginPropertyName());
							if(!getProfile().isTransient() && getSETALogger() != null){
								getSETALogger().logAddGiftCard(login);
							}
						}
					}
				}
			} catch (CommerceException comExp) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_WHILE_PROCESSING_REQUEST, null);
				getErrorHandler().processException(mVe, this);
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("Exception in TRUGiftCardFormHandler/handleApplyGiftCard method ", comExp);
				}
			} catch (RepositoryException repExp) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_WHILE_PROCESSING_REQUEST, null);
				getErrorHandler().processException(mVe, this);
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("Exception in TRUGiftCardFormHandler/handleApplyGiftCard method ", repExp);
				}
			} catch (RunProcessException runExp) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_WHILE_PROCESSING_REQUEST, null);
				getErrorHandler().processException(mVe, this);
				PerformanceMonitor.cancelOperation(myHandleMethod);
				if (isLoggingError()) {
					logError("RunProcessException in TRUGiftCardFormHandler/handleApplyGiftCard method ", runExp);
				}
			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.endOperation(myHandleMethod);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
			if (isLoggingDebug()) {
				logDebug("End of TRUGiftCardFormHandler/handleApplyGiftCard method");
			}
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * This method is used to create payment group of type gift card.
	 * 
	 * @param pGiftCardBalAmt
	 *            gift card balance amount
	 * @param pRemainingOrderAmount
	 *            remaining order amount
	 * @throws CommerceException
	 *             throws CommerceException
	 * 
	 */
	public void applyGiftCard(double pGiftCardBalAmt, double pRemainingOrderAmount) throws CommerceException {
		if (isLoggingDebug()) {
			logDebug("Entered into TRUGiftCardFormHandler.applyGiftCard");
		}
		final TRUGiftCard giftCard = (TRUGiftCard) getPaymentGroupManager().createPaymentGroup(TRUPaymentConstants.GIFT_CARD);
		final TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getOrderTools().getProfileTools().getPricingTools();
		final double giftCardBalAmt = pricingTools.round(pGiftCardBalAmt);
		final double remainingOrderAmount = pricingTools.round(pRemainingOrderAmount);
		if (giftCardBalAmt >= remainingOrderAmount) {
			giftCard.setAmount(remainingOrderAmount);
		} else {
			giftCard.setAmount(giftCardBalAmt);
		}
		giftCard.setRemainingBalance(giftCardBalAmt - giftCard.getAmount());
		giftCard.setGiftCardNumber(getGiftCardNumber());
		giftCard.setGiftCardPin(getGiftCardPin());
		giftCard.setCurrencyCode(getTRUConfiguration().getCurrencyCodesString().get(getOrder().getSiteId()));
		getPaymentGroupManager().addPaymentGroupToOrder(getOrder(), giftCard);
		getOrderManager().addOrderAmountToPaymentGroup(getOrder(), giftCard.getId(), giftCard.getAmount());
		if (isLoggingDebug()) {
			logDebug("Exit TRUGiftCardFormHandler.applyGiftCard");
		}
	}

	/**
	 * Handle remove gift card.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean boolean value
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleRemoveGiftCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
	IOException {
		if (isLoggingDebug()) {
			logDebug("Start of TRUGiftCardFormHandler/handleRemoveGiftCard method");
		}
		final String myHandleMethod = TRUCommerceConstants.HANDLE_REMOVE_GIFT_CARD;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			resetFormExceptions();
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					if(isCSCRequest() && StringUtils.isNotBlank(getGiftCardEmail()))
					{
						((TRUOrderImpl) getOrder()).setEmail(getGiftCardEmail());
					}
					if (isLoggingDebug()) {
						logDebug("CardId is " + getCardId());
					}
					getOrderManager().getPaymentGroupManager().removePaymentGroupFromOrder(getOrder(), getCardId());
					if(isCSCRequest()) {
						getPaymentGroupMapContainer().removePaymentGroup(getCardId());
					}
					adjustOrderPaymentGroups();
					runProcessRepriceOrder(getOrder(),
							getUserPricingModels(), getUserLocale(pRequest, pResponse),
							getProfile(), createRepriceParameterMap());
					getOrderManager().updateOrder(getOrder());
					//SETA log for adding gift card
					String login = (String)getProfile().getPropertyValue(getOrderManager().getOrderTools().getProfileTools().getPropertyManager().getLoginPropertyName());
					if(!getProfile().isTransient() && getSETALogger() != null){
						getSETALogger().logRemoveGiftCard(login);
					}
				}
			} catch (CommerceException comExp) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_WHILE_PROCESSING_REQUEST, null);
				getErrorHandler().processException(mVe, this);
				if (isLoggingError()) {
					logError("Exception in TRUGiftCardFormHandler/handleRemoveGiftCard method ", comExp);
				}
			} catch (RunProcessException e) {
				if (isLoggingError()) {
					logError("RunProcessException in TRUGiftCardFormHandler.handleRemoveGiftCard", e);
				}
			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
			if (isLoggingDebug()) {
				logDebug("End of TRUGiftCardFormHandler/handleRemoveGiftCard method");
			}
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * This method is to validate gift card max limit and if same card was added
	 * already.
	 * 
	 * @param pOrder
	 *            Order.
	 * @throws CommerceException
	 *             the commerce exception
	 * @throws ServletException
	 *             the servlet exception
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public void checkOrderPaymentGroups(Order pOrder) throws CommerceException, ServletException, RepositoryException {

		if (isLoggingDebug()) {
			logDebug(" invoked TRUGiftCardFormHandler.checkOrderPaymentGroups() method");
		}
		final List<PaymentGroup> paymentGroupList = pOrder.getPaymentGroups();
		//int paymentGroupSize = paymentGroupList.size();
		String giftCardNumber = getGiftCardNumber();
		PaymentGroup paymentGroup = null;
		TRUGiftCard giftCard = null;
		int giftCardSize = 0;
		String giftCardNo = null;
		boolean giftCardApplied = false;
		String infoMessage = null;
		int paymentGroup_Size = paymentGroupList.size();
		// checks whether added gift card is already set as payment method.
		for (int count = 0; count < paymentGroup_Size; count++) {
			paymentGroup = paymentGroupList.get(count);
			if (paymentGroup instanceof TRUGiftCard) {
				giftCard = (TRUGiftCard) paymentGroup;
				giftCardNo = giftCard.getGiftCardNumber();
				if (giftCardNo.equals(giftCardNumber)) {
					giftCardApplied = true;
				}
				giftCardSize++;
			}
		}
		final int maxGiftCardSize = ((TRUOrderManager) getOrderManager()).getGiftCardMaxLimit();
		// checks maximum Limit of Gift cards.
		if (giftCardSize >= maxGiftCardSize) {
			try {
				infoMessage = getErrorHandlerManager().getErrorMessage(TRUErrorKeys.TRU_ERROR_ADD_MAXIMUM_FIVE_GIFT_CARDS);
			} catch (SystemException sysExce) {
				if (isLoggingError()) {
					logError(" SystemException in @Class::TRUGiftCardFormHandler::@method::checkOrderPaymentGroups() :", sysExce);
				}
			}
			infoMessage = MessageFormat.format(infoMessage, maxGiftCardSize);
			addFormException(new DropletException(infoMessage, TRUErrorKeys.TRU_ERROR_ADD_MAXIMUM_FIVE_GIFT_CARDS));
		}
		if (giftCardApplied) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_GIFT_CARD_ALREADY_APPLIED, null);
			getErrorHandler().processException(mVe, this);
		}
		if (isLoggingDebug()) {
			logDebug(" end TRUGiftCardFormHandler.checkOrderPaymentGroups() method");
		}
	}

	/**
	 * Handle adjust payment groups.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public void handleAdjustPaymentGroups(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Start of TRUGiftCardFormHandler/handleAdjustPaymentGroups method");
		}
		final String myHandleMethod = TRUCommerceConstants.HANDLE_ADJUST_PAYMENT_GROUPS;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			resetFormExceptions();
			if (!isEnableAdjustPaymentGroups()) {
				return;
			}
			Transaction tr = null;
			try {
				if (!getFormError()) {
					tr = ensureTransaction();
					synchronized (getOrder()) {
						// Ensure InStorePaymentGroup
						((TRUOrderManager) getOrderManager()).ensureInStorePaymentGroup(getOrder());
						adjustOrderPaymentGroups();
						getOrderManager().updateOrder(getOrder());
					}
				}
			} catch (CommerceException comExp) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_WHILE_PROCESSING_REQUEST, null);
				getErrorHandler().processException(mVe, this);
				if (isLoggingError()) {
					logError("CommerceException in TRUGiftCardFormHandler/handleAdjustPaymentGroups method ", comExp);
				}
				try {
					setTransactionToRollbackOnly();
				} catch (javax.transaction.SystemException e1) {
					if (isLoggingError()) {
						logError("SystemException in TRUGiftCardFormHandler/handleAdjustPaymentGroups method ", e1);
					}
				}
			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
			if (isLoggingDebug()) {
				logDebug("End of TRUGiftCardFormHandler/handleAdjustPaymentGroups method");
			}
		}
	}

	/**
	 * Adjust order payment groups.
	 * 
	 * @throws CommerceException
	 *             CommerceException
	 */
	private void adjustOrderPaymentGroups() throws CommerceException {
		final PipelineResult 	result = ((TRUOrderManager) getOrderManager()).adjustPaymentGroups(getOrder());
		if (isLoggingDebug()) {
			logDebug("TRUGiftCardFormHandler/adjustPaymentGroups method " + result);
		}
		if (processPipelineErrors(result)) {
			for (Object error : result.getErrorKeys()) {
				if (isLoggingError()) {
					logError(" Pipeline error :: @Class::TRUGiftCardFormHandler::@method::adjustOrderPaymentGroups()"
							+ result.getError(error));
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End of TRUGiftCardFormHandler/adjustPaymentGroups method");
		}
	}

	/**
	 * Gets the error key list.
	 * 
	 * @return the errorKeyList
	 */
	public List<String> getErrorKeyList() {
		return mErrorKeyList;
	}

	/**
	 * Sets the error key list.
	 * 
	 * @param pErrorKeyList
	 *            the errorKeyList to set
	 */
	public void setErrorKeyList(List<String> pErrorKeyList) {
		mErrorKeyList = pErrorKeyList;
	}

	/**
	 * @return the pageName
	 */
	public String getPageName() {
		return mPageName;
	}

	/**
	 * @param pPageName the pageName to set
	 */
	public void setPageName(String pPageName) {
		mPageName = pPageName;
	}
	/** Property to Hold Error handler manager. */
	private DefaultErrorHandlerManager mErrorHandlerManager;

	/**
	 * @return the errorHandlerManager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * @param pErrorHandlerManager the errorHandlerManager to set
	 */
	public void setErrorHandlerManager(
			DefaultErrorHandlerManager pErrorHandlerManager) {
		mErrorHandlerManager = pErrorHandlerManager;
	}
	
	/**
	 * This method is used to add calculate tax parameter.
	 * 
	 * @return Map
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected Map createRepriceParameterMap() {
		if (isLoggingDebug()) {
			vlogDebug("TRUCommitOrderFormHandler (createRepriceParameterMapToSkipTaxCal) : BEGIN");
		}
		Map parameters;
		parameters = super.createRepriceParameterMap();
		if (parameters == null) {
			parameters = new HashMap();
		}
		parameters.put(TRUTaxwareConstant.CALC_TAX, Boolean.TRUE);
		if (isLoggingDebug()) {
			vlogDebug("TRUCommitOrderFormHandler (createRepriceParameterMapToSkipTaxCal) : END");
		}
		return parameters;
	}
	/**
	 * This method will set error 
	 * 
	 * @param pErrorMessage - ErrorMessage
	 * @throws ServletException - If any ServletException.
	 */
	private void setChannelError(String pErrorMessage) throws ServletException {
		if(isRestService()) {
			mVe.addValidationError(pErrorMessage, null);
			getErrorHandler().processException(mVe, this);
		} else {
			addFormException(new DropletException(pErrorMessage));
		}
	}

	/**
	 * @return the mSETALogger
	 */
	public TRUSETALogger getSETALogger() {
		return mSETALogger;
	}

	/**
	 * Sets the SETA logger.
	 *
	 * @param pSETALogger the new SETA logger
	 */
	public void setSETALogger(TRUSETALogger pSETALogger) {
		mSETALogger = pSETALogger;
	}	
	
}
