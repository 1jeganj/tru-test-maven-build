package com.tru.commerce.order.purchase;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.transaction.Transaction;

import atg.commerce.CommerceException;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.RelationshipNotFoundException;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.purchase.AddCommerceItemInfo;
import atg.commerce.order.purchase.CartModifierFormHandler;
import atg.commerce.pricing.PricingModelHolder;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.pipeline.PipelineResult;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUChannelInStorePickupShippingGroup;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUUserSession;
import com.tru.common.cml.SystemException;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;
import com.tru.integrations.registry.beans.Registry;
import com.tru.integrations.registry.beans.RegistryAccountDetailList;
import com.tru.integrations.registry.beans.RegistryAccountHeader;
import com.tru.integrations.registry.beans.RegistryResponse;
import com.tru.integrations.registry.exception.RegistryIntegrationException;
import com.tru.integrations.registry.service.RegistryService;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.userprofiling.TRUCookieManager;

/**
 * The Class TRUBaseCartModifierFormHandler.
 */
public class TRUBaseCartModifierFormHandler extends CartModifierFormHandler {


	/** Property to hold channel. */
	private String mChannelType;

	/** Property to hold channel id. */
	private String mChannelId;

	/** Property to hold channel name. */
	private String mChannelName;

	/** Property to hold channel user name. */
	private String mChannelUserName;

	/** Property to Channel co user name. */
	private String mChannelCoUserName;
	
	/** The Source channel. */
	private String mSourceChannel;

	/** The Cart info message. */
	private boolean mAddUpdateItem;

	/** The Is donation item. */
	private boolean mDonationItem;

	/** Property to hold mCollectionRequest. */
	private String mCollectionRequest;

	/** Property to hold mShopLocalRequest. */
	private String mShopLocalRequest;

	/** Property to Hold Error handler manager. */
	private DefaultErrorHandlerManager mErrorHandlerManager;

	/** property to Hold shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;

	/** The Multiple items info messages. */
	private List<String> mMultipleItemsInfoMessages;
	
	/** The Item inline messages. */
	private Map<String, String> mItemInlineMessages;

	/** property to hold mRestService. */
	private boolean mRestService;
	
	/** The Donation amount. */
	private double mDonationAmount;
	
	/** property to hold mRestSuccessURL. */
	private String mRestSuccessURL;
	
	/** property to hold mRestErrorURL. */
	private String mRestErrorURL;
	
	/** Property to hold Profile tools. */
	private TRUProfileTools mProfileTools;
	
	/** Property to Hold is remove item. */
	private boolean mSplitItemRemove;
	
	/** Holds editCartFromPage. */
	private String mEditCartFromPage;
	
	/** The Relation ship id. */
	private String mRelationShipId;
	
	/** property to hold Gift Wrap Sku Id. */
	private String mGiftWrapSkuId;
	
	/** The Is TRUGiftingPurchaseProcessHelper. */
	private TRUGiftingPurchaseProcessHelper mGiftingProcessHelper;
	
	/** property to hold Wrap Item Quantity. */
	private String mWrapItemQuantity;
	
	/** The Invalidate order pricing op. */
	private String mInvalidateOrderPricingOp;

	
	/** The temp nick name. */
	private String mTempNickName;
	

	/**
	 * Property to hold mCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;
	
	/** Property to hold mCatalogProperties. */
	private TRUCatalogProperties mCatalogProperties;
	
	/** The User session. */
	private TRUUserSession mUserSession;
	
	/** The Is InventoryLimit For Review Page. */
	private boolean mInventoryLimitForReviewPage;
	/**
	 * @return the inventoryLimitForReviewPage
	 */
	public boolean isInventoryLimitForReviewPage() {
		return mInventoryLimitForReviewPage;
	}

	/**
	 * @param pInventoryLimitForReviewPage the inventoryLimitForReviewPage to set
	 */
	public void setInventoryLimitForReviewPage(boolean pInventoryLimitForReviewPage) {
		mInventoryLimitForReviewPage = pInventoryLimitForReviewPage;
	}

	/**
	 * @return the userSession
	 */
	public TRUUserSession getUserSession() {
		return mUserSession;
	}

	/**
	 * @param pUserSession the userSession to set
	 */
	public void setUserSession(TRUUserSession pUserSession) {
		mUserSession = pUserSession;
	}

	/**
	 * @return the catalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * @param pCatalogProperties the catalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}

	/**
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * Gets the temp nick name.
	 *
	 * @return the temp nick name
	 */
	public String getTempNickName() {
		return mTempNickName;
	}

	/**
	 * Sets the temp nick name.
	 *
	 * @param pTempNickName the new temp nick name
	 */
	public void setTempNickName(String pTempNickName) {
		this.mTempNickName = pTempNickName;
	}
	
	/**
	 * Pre add item to order.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void preAddItemToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::preAddItemToOrder() : BEGIN");
		}
		final String[] skus = this.getCatalogRefIds();
		if (isRestService()
				&& !StringUtils.isBlank(getChannelType())
				&& (getChannelType().equalsIgnoreCase(TRUCommerceConstants.WISHLIST)
						|| getChannelType().equalsIgnoreCase(TRUCommerceConstants.REGISTRY) || getChannelType()
						.equalsIgnoreCase(TRUCommerceConstants.REGISTRY_STORE))) {
			String ParentProductId = getCatalogTools().getParentProductfromSKUs(skus);
			if(StringUtils.isNotBlank(ParentProductId)){
				setProductId(ParentProductId);
			}
		}
		if(pRequest.getHeader(TRUCommerceConstants.API_CHANNEL) != null){
			setSourceChannel(pRequest.getHeader(TRUCommerceConstants.API_CHANNEL));
		}
		final AddCommerceItemInfo[] items = this.getItems();
		if (null != items) {
			vlogDebug("@Class::TRUBaseCartModifierFormHandler::@method::preAddItemToOrder() : Items size : {0}", items.length);
		}
		setAddUpdateItem(Boolean.TRUE);
		if (isDonationItem()) {
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUBaseCartModifierFormHandler::@method::preAddItemToOrder() : isDonationItem : {0}",
						isDonationItem());
			}
			return;
		}
		long quantity = this.getQuantity();
		String locationId = null;
		// Setting the UserName and CoUserName details of Channel Items. 
		if(isChannelItemFromPDP()){
			updateChannelDetails();
		} else if(!isRestService() && !isChannelItemFromPDP()) {
			nullifyChannelDetails();
		}
		validateChannelDetails();
		if ((StringUtils.isNotBlank(getCollectionRequest()) && getCollectionRequest().equalsIgnoreCase(
				TRUCommerceConstants.COLLECTION))
				|| (StringUtils.isNotBlank(getShopLocalRequest()) && getShopLocalRequest().equalsIgnoreCase(
						TRUCommerceConstants.SHOP_LOCAL))) {
			if (isLoggingDebug()) {
				vlogDebug(
						"@Class::TRUBaseCartModifierFormHandler::@method::preAddItemToOrder() : Adding multiple items to cart either from collection "
								+ ": {0} : Or from shoplocal : {1}", getCollectionRequest(), getShopLocalRequest());
			}
			final List<AddCommerceItemInfo> itemList = new ArrayList<AddCommerceItemInfo>();
			for (AddCommerceItemInfo item : items) {
				if (StringUtils.isNotBlank(item.getLocationId())) {
					locationId = item.getLocationId();
				} else {
					locationId = getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome();
				}
				if (performQtyValidationsForAddingMultipleItems(item, item.getCatalogRefId(), item.getQuantity(), locationId)) {
					itemList.add(item);
				}
			}
			if(getUserSession() != null){
				getUserSession().getMultipleItemsInfoMessages().clear();
				getUserSession().getItemInlineMessages().clear();
				getUserSession().getMultipleItemsInfoMessages().addAll(getMultipleItemsInfoMessages());
				getUserSession().getItemInlineMessages().putAll(getItemInlineMessages());
			}
			if (!itemList.isEmpty()) {
				setAddItemCount(itemList.size());
				final AddCommerceItemInfo[] copyItems = itemList.toArray(new AddCommerceItemInfo[itemList.size()]);
				for (int index = 0; index < copyItems.length; index++) {
					if (copyItems[index].getCatalogRefId() != null) {
						getItems()[index] = copyItems[index];
					}
				}
			} else {
				setAddUpdateItem(Boolean.FALSE);
			}
		} else {
			if (!StringUtils.isBlank(getLocationId())) {
				locationId = getLocationId();
			} else {
				locationId = getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome();
			}
			for (String skuId : skus) {
				if (isLoggingDebug()) {
					vlogDebug(
							"@Class::TRUBaseCartModifierFormHandler::@method::preAddItemToOrder() : skuId : {0} : quantity : {1} : location id : {2}",
							skuId, quantity, locationId);
				}
				boolean removeItemFlag = performRemovalFlagValidation(skuId);
				if(removeItemFlag){
					setAddUpdateItem(Boolean.FALSE);
				} else if (isRestService() && ! StringUtils.isBlank(getSourceChannel()) && getSourceChannel().equalsIgnoreCase(TRUCommerceConstants.REGISTRY_STORE)) {
					boolean performQtyvalidations = getShoppingCartUtils().checkItemInStock(skuId, locationId);
					if (performQtyvalidations) {
						performQtyValidations(skuId, quantity, locationId);
					}
				} else {
					performQtyValidations(skuId, quantity, locationId);
				}
				
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::preAddItemToOrder() : END");
		}
	}

	/**
	 * Perform removal flag validation.
	 *
	 * @param skuId the sku id
	 * @return removeItemFlag boolean
	 */
	private boolean performRemovalFlagValidation(String skuId) {
		if(isLoggingDebug()){
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::performRemovalFlagValidation() : BEGIN");
		}
		boolean removeItemFlag = Boolean.FALSE;
		try {
			String name = null;
			String errorMessage = null;
			RepositoryItem skuItem = getCatalogTools().findSKU(skuId);
			if(skuItem != null){
				name = (String) skuItem.getPropertyValue(getShoppingCartUtils().getPropertyManager().getDisplayName());
			}
			String infoMessage = getErrorHandlerManager().getErrorMessage(
					TRUCommerceConstants.TRU_SHOPPINGCART_REMOVED_IETM_FROM_BCC);
			if (null != skuItem && ((null != skuItem.getPropertyValue(getShoppingCartUtils().getPropertyManager().getIsDeleted()) && Boolean.TRUE
							.equals(skuItem.getPropertyValue(getShoppingCartUtils().getPropertyManager().getIsDeleted()))) || (null != skuItem
							.getPropertyValue(getShoppingCartUtils().getPropertyManager().getWebDisplayFlag())&& Boolean.FALSE.equals(skuItem.getPropertyValue(getShoppingCartUtils().getPropertyManager().getWebDisplayFlag()))))
							|| (null != skuItem && Boolean.FALSE.equals(skuItem.getPropertyValue(getShoppingCartUtils().getPropertyManager().getSuperDisplayFlag()))) || (null != skuItem
									.getPropertyValue(getShoppingCartUtils().getPropertyManager().getUnCartable()) && Boolean.TRUE.equals(skuItem
											.getPropertyValue(getShoppingCartUtils().getPropertyManager().getUnCartable())))) {
				errorMessage = MessageFormat.format(infoMessage, name, name);
				addFormException(new DropletException(errorMessage, TRUCommerceConstants.CART_TOP_MSG));
				removeItemFlag = Boolean.TRUE;
			}
			
		} catch (RepositoryException e) {
			if(isLoggingError()){
				logError("RepositoryException in performRemovalFlagValidation", e);
			}
		} catch (SystemException e) {
			if(isLoggingError()){
				logError("SystemException in performRemovalFlagValidation", e);
			}
		}
		return removeItemFlag;
	}

	/**
	 * This method is used to get the ChannelDetails from the rest service call
	 * and setting it to the respective setter methods.
	 */
	private void updateChannelDetails() {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::updateChannelDetails() : BEGIN");
			vlogDebug("ChannelId : {0} : ChannelType : {1} : mDeletedFlag : {2}", getChannelId(), getChannelType(), mDeletedFlag);
		}
		try {
			RegistryResponse registryResponse = getRegistryService().getWishlistItems(getChannelId(), mDeletedFlag);
			if(isLoggingDebug()) {
				vlogDebug("RegistryResponse : {0} ", registryResponse);
			}
			if(registryResponse != null) {
				Registry registry = registryResponse.getRegistryDetail().getRegistry();
				RegistryAccountHeader accountHeader = registry.getRegistryAccountHeader();
				if(null != accountHeader) {
					StringBuilder channelUserNameValue = new StringBuilder();
					channelUserNameValue.append(accountHeader.getFirstName()).append(TRUCommerceConstants.SPACE);
					channelUserNameValue.append(accountHeader.getLastName());
					setChannelUserName(channelUserNameValue.toString());
				}
				List<RegistryAccountDetailList> accountDetailLists = registry.getRegistryAccountDetailList();
				if(null != accountDetailLists) { 
					for(RegistryAccountDetailList registryDetailsList : accountDetailLists) {
						if(registryDetailsList != null && registryDetailsList.getRegistryNumber() != null && registryDetailsList.getRegistryNumber().equals(getChannelId())) {
							StringBuilder channelCoUserNameValue = new StringBuilder();
							channelCoUserNameValue.append(registryDetailsList.getCoregistrantFirstName()).append(TRUCommerceConstants.SPACE);
							channelCoUserNameValue.append(registryDetailsList.getCoregistrantLastName());
							setChannelCoUserName(channelCoUserNameValue.toString());
							break;
						}
					}
				}
			}else {
				nullifyChannelDetails();
			} 
		} catch (RegistryIntegrationException exception) {
			nullifyChannelDetails();
			if (isLoggingError()) {
				logError("RunProcessException in TRURegistryServiceProcessor.getDetails() method", exception);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("ChannelUserName : {0} : ChannelCoUserName : {1}", getChannelUserName(), getChannelCoUserName());
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::updateChannelDetails() : END");
		}
	}
	
	/**
	 * Adds the item to order.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	protected void addItemToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::addItemToOrder() : BEGIN");
			vlogDebug("isAddUpdateItem() : {0} : locationId : {1}", isAddUpdateItem(), getLocationId());
		}
		if (isAddUpdateItem()) {
			if (StringUtils.isBlank(getLocationId())) {
				setLocationId(null);
			}
			final boolean valid = mergeItemInputForAdd(pRequest, pResponse, getCommerceItemType(), false);
			if (isLoggingDebug()) {
				vlogDebug("valid : {0} : channelId : {1} : isDonationItem : {2}", isAddUpdateItem(), getLocationId());
			}
			if (null != getChannelId() || isDonationItem() || getLocationId() != null) {
				mergeItemCustomInputForAdd();
			}
			if (valid) {
				doAddItemsToOrder(pRequest, pResponse);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::addItemToOrder() : END");
		}
	}
	
	/**
	 * This method creates in store pick up shipping groups.
	 *
	 * @param pOrder            to set order
	 * @throws CommerceException             - commerce exception
	 */
	protected void createInStorePickupShippingGroups(Order pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUBaseCartModifierFormHandler::@method::createInStorePickupShippingGroups() : BEGIN : channel Id : {0}",
					getChannelId());
		}
		((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).createISPUShippingGroups(pOrder, getItems());
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUBaseCartModifierFormHandler::@method::createInStorePickupShippingGroups() : END");
		}
	}
	
	/**
	 * This method is required for the guest customers in order to create cart cookie.
	 * And persist the order in DB, provided if the guest user add any items to the cart.
	 * 
	 * @param pRequest
	 *            to set pRequest
	 * @param pResponse
	 *            to set pResponse
	 * @throws ServletException
	 *             -
	 * @throws IOException
	 *             -
	 */
	@Override
	public void postAddItemToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::postAddItemToOrder() : BEGIN ");
		}
		super.postAddItemToOrder(pRequest, pResponse);
		createCartCookies(getOrder(), pRequest, pResponse);
		if(isRestService()){
			setAddItemToOrderSuccessURL(getRestSuccessURL());
			setAddItemToOrderErrorURL(getRestErrorURL());
		}
		nullifyChannelDetails();
		try {
			getShippingGroupManager().removeEmptyShippingGroups(getOrder());
		} catch (CommerceException commerceException) {
			if (isLoggingError()) {
				logError("CommerceException @Class::TRUBaseCartModifierFormHandler::@method::postAddItemToOrder() ",
						commerceException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::postAddItemToOrder() : END ");
		}
	}
	
	/**
	 * This method Creates the cart cookies based on the user logged-in status.
	 *
	 * @param pOrder            the order
	 * @param pRequest            the request
	 * @param pResponse            the response
	 * @throws IOException             Signals that an I/O exception has occurred.
	 * @throws ServletException             the servlet exception
	 */
	protected void createCartCookies(Order pOrder, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::createCartCookies() : START");
		}
		final TRUProfileTools profileTools = getProfileTools();
		final TRUCookieManager cookieManager = (TRUCookieManager) profileTools.getCookieManager();
		if (profileTools != null && profileTools.isAnonymousUser(getProfile())) {
			try {
				cookieManager.createCartCookie(pOrder, pRequest, pResponse, TRUConstants.CART_COOKIE_NAME);
			} catch (CommerceException commerceException) {
				if (isLoggingError()) {
					logError("CommerceException @Class::TRUBaseCartModifierFormHandler::@method::createCartCookies() ",
							commerceException);
				}
			}
		} else {
			try {
				cookieManager.createCartCookie(pOrder, pRequest, pResponse, TRUConstants.LOGGEDEIN_CART_COOKIE_NAME);
			} catch (CommerceException commerceException) {
				if (isLoggingError()) {
					logError("CommerceException @Class::TRUBaseCartModifierFormHandler::@method::createCartCookies() ",
							commerceException);
				}
			}
		}
		// cookieManager.createCookieForOrderItemCount(pOrder, pRequest, pResponse);
		// cookieManager.createCookieForFreeShippingProgressBar(pOrder, pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::createCartCookies() : END");
		}

	}
	
	/**
	 * Merge item custom input for add.
	 */
	private void mergeItemCustomInputForAdd() {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::mergeItemCustomInputForAdd() : BEGIN ");
			vlogDebug(
					"channel Id : {0} : channelType : {1} : channelUserName : {2} : channelCoUserName : {3} : channelName : {4} :" + 
							" CatalogRefIds : {5} : isDonationItem : {6}",
					getChannelId(), getChannelType(), getChannelUserName(), getChannelCoUserName(), getChannelName(),
					getCatalogRefIds(), isDonationItem());
		}
		for (int index = 0; index < getCatalogRefIds().length; ++index) {
			final AddCommerceItemInfo addCommerceItemInfo = getItems()[index];
			if (!StringUtils.isBlank(getChannelId())) {
				final String classType = ((TRUOrderTools) getOrderManager().getOrderTools()).getDefaultCommerceItemType();
				if (StringUtils.isNotBlank(classType)) {
					((TRUAddCommerceItemInfo) addCommerceItemInfo).setCommerceItemType(classType);
				}
				if (StringUtils.isNotBlank(getChannelType())) {
					((TRUAddCommerceItemInfo) addCommerceItemInfo).setChannelType(getChannelType());
				}
				if (StringUtils.isNotBlank(getChannelId())) {
					((TRUAddCommerceItemInfo) addCommerceItemInfo).setChannelId(getChannelId());
				}
				if (StringUtils.isNotBlank(getChannelUserName())) {
					((TRUAddCommerceItemInfo) addCommerceItemInfo).setChannelUserName(getChannelUserName());
				}
				if (StringUtils.isNotBlank(getChannelCoUserName())) {
					((TRUAddCommerceItemInfo) addCommerceItemInfo).setChannelCoUserName(getChannelCoUserName());
				}
				if (StringUtils.isNotBlank(getChannelName())) {
					((TRUAddCommerceItemInfo) addCommerceItemInfo).setChannelName(getChannelName());
				}
				if (StringUtils.isNotBlank(getSourceChannel())) {
					((TRUAddCommerceItemInfo) addCommerceItemInfo).setSourceChannel(getSourceChannel());
				}
				
			}
			if (isDonationItem()) {
				final String classType = ((TRUOrderTools) getOrderManager().getOrderTools()).getDonationCommerceItemClassType();
				if (isLoggingDebug()) {
					vlogDebug("classType : {0} : DonationAmount : {1}", classType, getDonationAmount());
				}
				if (!StringUtils.isBlank(classType)) {
					((TRUAddCommerceItemInfo) addCommerceItemInfo).setCommerceItemType(classType);
				}
				if (getDonationAmount() != 0) {
					((TRUAddCommerceItemInfo) addCommerceItemInfo).setDonationAmount(getDonationAmount());
				}
			}
			try {
				Double wareHouseLocationId = getShoppingCartUtils().getTRUCartModifierHelper().getWareHouseLocationId(getLocationId());
				if (wareHouseLocationId != null) {
					long longValue = wareHouseLocationId.longValue();
					((TRUAddCommerceItemInfo) addCommerceItemInfo).setWarehouseLocationCode(String.valueOf(longValue));
				}
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					logError("@Class::TRUBaseCartModifierFormHandler::@method::mergeItemCustomInputForAdd()", repositoryException);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::mergeItemCustomInputForAdd() : END ");
		}
	}
	
	/**
	 * Nullify channel details.
	 */
	protected void nullifyChannelDetails() {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::nullifyChannelDetails() : BEGIN");
		}
		setChannelId(null);
		setChannelName(null);
		setChannelType(null);
		setChannelUserName(null);
		setChannelCoUserName(null);
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::nullifyChannelDetails() : END");
		}
	}

	/**
	 * Check the sku price and inventory.
	 * 
	 * @param pSkuId
	 *            the sku id
	 * @param pQuantity
	 *            the quantity
	 * @param pLocationId
	 *            the location id
	 */
	protected void performQtyValidations(String pSkuId, long pQuantity, String pLocationId) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::performQtyValidations() : BEGIN");
			vlogDebug("pSkuId : {0} : pQuantity : {1} : pLocationId : {2}", pSkuId, pQuantity, pLocationId);
		}
		try {
			String inlineInfoMessage = null;
			final String displayName = getShoppingCartUtils().getDisplayNameForSku(pSkuId);
			int invenotryStatus = TRUCommerceConstants.INT_IN_STOCK;
			RepositoryItem skuItem = getCatalogTools().findSKU(pSkuId);
			Date currentDate = new Date();
			if (skuItem != null) {
				Date streetDate = (Date) skuItem.getPropertyValue(getShoppingCartUtils().getPropertyManager()
						.getStreetDate());
				String backOrderStatus = (String) skuItem.getPropertyValue(getShoppingCartUtils().getPropertyManager()
						.getBackOrderStatus());
				if (streetDate != null && currentDate.before(streetDate)) {
					if (backOrderStatus != null && backOrderStatus.equalsIgnoreCase(TRUCommerceConstants.R)) {
						invenotryStatus = TRUCommerceConstants.INT_PRE_ORDER;
					} else if (backOrderStatus != null && backOrderStatus.equalsIgnoreCase(TRUCommerceConstants.N)) {
						invenotryStatus = TRUCommerceConstants.INT_OUT_OF_STOCK;
					} else {
						invenotryStatus = getShoppingCartUtils().getInventoryStatus(pSkuId, pLocationId);
					}
				} else {
					invenotryStatus = getShoppingCartUtils().getInventoryStatus(pSkuId, pLocationId);
				}
			}
			inlineInfoMessage = getInlineInfoMessage(pSkuId, pQuantity, pLocationId,
					  displayName, invenotryStatus);
			if(StringUtils.isNotBlank(inlineInfoMessage)) {
				getItemInlineMessages().put(pSkuId + TRUCommerceConstants.UNDER_SCORE + pLocationId, inlineInfoMessage);
			}
		} catch (RepositoryException | SystemException e) {
			if (isLoggingError()) {
				logError(" RepositoryException in @Class::TRUBaseCartModifierFormHandler::@method::preAddItemToOrder() :", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::performQtyValidations() : END");
		}
	}
/**
 * This method will  get inline message.
 * @param pSkuId - sku id
 * @param pQuantity - quantity
 * @param pLocationId - location id
 * @param pDisplayName - display name
 * @param pInvenotryStatus - inventory status
 * @return String - string
 * @throws RepositoryException - Repository Exception
 * @throws SystemException - System Exception
 */
	private String getInlineInfoMessage(String pSkuId, long pQuantity, String pLocationId,
			final String pDisplayName,
			final int pInvenotryStatus) throws RepositoryException,
			SystemException {
		String infoMessage;
		String inlineInfoMessage = null;
		long inventoryQty = 0;
		long itemQtyInCart = 0;
		long itemRelQtyInCart = 0;
		long customerLimit = 0;
		if (pInvenotryStatus == TRUCommerceConstants.INT_IN_STOCK || pInvenotryStatus == TRUCommerceConstants.INT_LIMITED_STOCK
				|| pInvenotryStatus == TRUCommerceConstants.INT_PRE_ORDER) {
			RepositoryItem skuItem = getCatalogTools().findSKU(pSkuId);
			Date currentDate = new Date();
			if (skuItem != null) {
				Date streetDate = (Date) skuItem.getPropertyValue(getShoppingCartUtils().getPropertyManager()
						.getStreetDate());
				String backOrderStatus = (String) skuItem.getPropertyValue(getShoppingCartUtils().getPropertyManager()
						.getBackOrderStatus());
				if (streetDate != null && currentDate.before(streetDate)) {
					if (backOrderStatus != null && backOrderStatus.equalsIgnoreCase(TRUCommerceConstants.R)) {
						inventoryQty = TRUCommerceConstants.TEST_MODE_STOCK_LEVEL;
					} else {
						inventoryQty = getShoppingCartUtils().getItemInventory(pSkuId, pLocationId, pInvenotryStatus);
					}
				} else {
					inventoryQty = getShoppingCartUtils().getItemInventory(pSkuId, pLocationId, pInvenotryStatus);
				}
				itemRelQtyInCart = getShoppingCartUtils().getItemQtyInCartByRelShip(pSkuId, pLocationId,
						getOrder());
			} else if (isRestService() && skuItem == null) {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_SKU_NOT_AVAILABLE_CATALOG_MESSAGE_KEY);
				infoMessage = MessageFormat.format(infoMessage, pDisplayName, inventoryQty);
				setAddUpdateItem(Boolean.FALSE);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_SKU_NOT_AVAILABLE_CATALOG_MESSAGE_KEY));
				inlineInfoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_SKU_NOT_AVAILABLE_CATALOG_MESSAGE_KEY);
				return inlineInfoMessage;
			}
			customerLimit = getShoppingCartUtils().getCustomerPurchaseLimit(pSkuId);
			itemQtyInCart = getShoppingCartUtils().getItemQtyInCart(pSkuId, getOrder());

			final long purchaseLimit = customerLimit - itemQtyInCart;
			final long inventoryLimit = inventoryQty - itemRelQtyInCart;
			if (isLoggingDebug()) {
				vlogDebug(
						"invenotryStatus : {0} : inventoryQty : {1} : itemRelQtyInCart : {2} : customerLimit : {3} : itemQtyInCart : {4}",
						pInvenotryStatus, inventoryQty, itemRelQtyInCart, customerLimit, itemQtyInCart);
			}
			if (purchaseLimit > inventoryLimit) {
				if (inventoryLimit == TRUCommerceConstants.INT_ZERO) {
					infoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_INVENTORY_REACHED_INFO_MESSAGE_KEY);
					infoMessage = MessageFormat.format(infoMessage, pDisplayName, inventoryQty);
					setAddUpdateItem(Boolean.FALSE);
					addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
					inlineInfoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.SHOPPINGCART_QTY_LIMIT_REACHED_INLINE_INFO_MESSAGE_KEY);
				} else if (inventoryLimit < pQuantity) {
					if (getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome()
							.equalsIgnoreCase(pLocationId)) {
						infoMessage = getErrorHandlerManager().getErrorMessage(
								TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_INVENTORY_ENFORCEMENT_MESSAGE_KEY);
					} else {
						infoMessage = getErrorHandlerManager().getErrorMessage(
								TRUCommerceConstants.TRU_CART_ITEM_INVENTORY_AT_SELECTED_STORE_ENFORCEMENT_INFO_MSG_KEY);
					}
					infoMessage = MessageFormat.format(infoMessage, pDisplayName);
					addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
					setQuantity(inventoryLimit);
					inlineInfoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.SHOPPINGCART_QTY_ADJUSTMENT_INFO_INLINE_MESSAGE_KEY);
				}
			} else {
				if (purchaseLimit == TRUCommerceConstants.INT_ZERO) {
					infoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.SHOPPINGCART_QTY_LIMIT_REACHED_INFO_MESSAGE_KEY);
					infoMessage = MessageFormat.format(infoMessage, pDisplayName);
					setAddUpdateItem(Boolean.FALSE);
					addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
					inlineInfoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.SHOPPINGCART_QTY_LIMIT_REACHED_INLINE_INFO_MESSAGE_KEY);
				} else if (purchaseLimit < pQuantity) {
					infoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_LIMIT_ENFORCEMENT_MESSAGE_KEY);
					infoMessage = MessageFormat.format(infoMessage, pDisplayName);
					addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
					setQuantity(purchaseLimit);
					inlineInfoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_LIMIT_ENFORCEMENT_INLINE_MESSAGE_KEY);
				}
			}
		} else if (pInvenotryStatus == TRUCommerceConstants.INT_OUT_OF_STOCK
				|| pInvenotryStatus == TRUCommerceConstants.NUM_ZERO) {
			if (isLoggingDebug()) {
				vlogDebug("invenotryStatus : {0} : inventoryQty", pInvenotryStatus);
			}
			if (getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome().equalsIgnoreCase(pLocationId)) {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.TRU_SHOPPINGCART_NO_ONLINE_INVENTORY_INFO_MESSAGE_KEY);
			} else {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.TRU_SHOPPINGCART_NO_INVENTORY_AT_SELECTED_STORE_INFO_MESSAGE_KEY);
			}
			infoMessage = MessageFormat.format(infoMessage, pDisplayName);
			setAddUpdateItem(Boolean.FALSE);
			addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
		}
		return inlineInfoMessage;
	}

	/**
	 * Check the sku price and inventory.
	 * 
	 * @param pItem
	 *            the item
	 * @param pSkuId
	 *            the sku id
	 * @param pQuantity
	 *            the quantity
	 * @param pLocationId
	 *            the location id
	 * @return true, if successful
	 */
	protected boolean performQtyValidationsForAddingMultipleItems(AddCommerceItemInfo pItem, String pSkuId, long pQuantity,
			String pLocationId) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::performQtyValidationsForAddingMultipleItems() : BEGIN");
			vlogDebug("AddCommerceItemInfo pItem : {0} : pSkuId : {1} : pQuantity : {2} : pLocationId : {3}", pItem, pSkuId,
					pQuantity, pLocationId);
		}
		boolean isValid = Boolean.TRUE;
		try {
			isValid = quantityValidationsForAddingMultiItems(pItem, pSkuId, pQuantity, pLocationId);
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				logError(" RepositoryException in @Class::TRUBaseCartModifierFormHandler::@method::preAddItemToOrder() :",
						repositoryException);
			}
		} catch (SystemException systemException) {
			if (isLoggingError()) {
				logError(" SystemException in @Class::TRUBaseCartModifierFormHandler::@method::preAddItemToOrder() :",
						systemException);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUBaseCartModifierFormHandler::@method::performQtyValidationsForAddingMultipleItems() : END : isValid : {0}",
					isValid);
		}

		return isValid;
	}

	/**
	 * Quantity validations for adding multi items.
	 * 
	 * @param pItem
	 *            the item
	 * @param pSkuId
	 *            the sku id
	 * @param pQuantity
	 *            the quantity
	 * @param pLocationId
	 *            the location id
	 * @return true, if successful
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws SystemException
	 *             the system exception
	 */
	private boolean quantityValidationsForAddingMultiItems(AddCommerceItemInfo pItem, String pSkuId, long pQuantity,
			String pLocationId) throws RepositoryException, SystemException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::quantityValidationsForAddingMultiItems() : BEGIN");
		}
		boolean isValid = Boolean.TRUE;
		String infoMessage = null;
		String inlineInfoMessage = null;
		long inventoryQty = 0;
		long itemQtyInCart = 0;
		long itemRelQtyInCart = 0;
		long customerLimit = 0;
		final String displayName = getShoppingCartUtils().getDisplayNameForSku(pSkuId);
		int invenotryStatus = getShoppingCartUtils().getInventoryStatus(pSkuId, pLocationId);
		if(StringUtils.isNotBlank(displayName)){

		if (invenotryStatus == TRUCommerceConstants.INT_IN_STOCK || invenotryStatus == TRUCommerceConstants.INT_LIMITED_STOCK
				|| invenotryStatus == TRUCommerceConstants.INT_PRE_ORDER) {
			inventoryQty = getShoppingCartUtils().getItemInventory(pSkuId, pLocationId, invenotryStatus);
			itemRelQtyInCart = getShoppingCartUtils().getItemQtyInCartByRelShip(pSkuId, pLocationId, getOrder());

			customerLimit = getShoppingCartUtils().getCustomerPurchaseLimit(pSkuId);
			itemQtyInCart = getShoppingCartUtils().getItemQtyInCart(pSkuId, getOrder());

			long purchaseLimit = customerLimit - itemQtyInCart;
			long inventoryLimit = inventoryQty - itemRelQtyInCart;
			if (isLoggingDebug()) {
				vlogDebug(
						"invenotryStatus : {0} : inventoryQty : {1} : itemRelQtyInCart : {2} : customerLimit : {3} : itemQtyInCart : {4}",
						invenotryStatus, inventoryQty, itemRelQtyInCart, customerLimit, itemQtyInCart);
			}
			if (purchaseLimit > inventoryLimit) {
				if (inventoryLimit == TRUCommerceConstants.INT_ZERO) {
					infoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_INVENTORY_REACHED_INFO_MESSAGE_KEY);
					infoMessage = MessageFormat.format(infoMessage, displayName, inventoryQty);
					inlineInfoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.SHOPPINGCART_QTY_LIMIT_REACHED_INLINE_INFO_MESSAGE_KEY);
					isValid = Boolean.FALSE;
				} else if (inventoryLimit < pQuantity) {
					if (getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome().equalsIgnoreCase(pLocationId)) {
						infoMessage = getErrorHandlerManager().getErrorMessage(
								TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_INVENTORY_ENFORCEMENT_MESSAGE_KEY);
					} else {
						infoMessage = getErrorHandlerManager().getErrorMessage(
								TRUCommerceConstants.TRU_CART_ITEM_INVENTORY_AT_SELECTED_STORE_ENFORCEMENT_INFO_MSG_KEY);
					}
					infoMessage = MessageFormat.format(infoMessage, displayName);
					inlineInfoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.SHOPPINGCART_QTY_ADJUSTMENT_INFO_INLINE_MESSAGE_KEY);
					if (pItem != null) {
						pItem.setQuantity(inventoryLimit);
					} else {
						setQuantity(inventoryLimit);
					}
				}
			} else {
				if (purchaseLimit == TRUCommerceConstants.INT_ZERO) {
					infoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.SHOPPINGCART_QTY_LIMIT_REACHED_INFO_MESSAGE_KEY);
					infoMessage = MessageFormat.format(infoMessage, displayName);
					inlineInfoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.SHOPPINGCART_QTY_LIMIT_REACHED_INLINE_INFO_MESSAGE_KEY);
					isValid = Boolean.FALSE;
				} else if (purchaseLimit < pQuantity) {
					infoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_LIMIT_ENFORCEMENT_MESSAGE_KEY);
					infoMessage = MessageFormat.format(infoMessage, displayName);
					inlineInfoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_LIMIT_ENFORCEMENT_INLINE_MESSAGE_KEY);
					if (pItem != null) {
						pItem.setQuantity(purchaseLimit);
					} else {
						setQuantity(purchaseLimit);
					}
				}
			}
		} else if (invenotryStatus == TRUCommerceConstants.INT_OUT_OF_STOCK || invenotryStatus == TRUCommerceConstants.NUM_ZERO) {
			if (isLoggingDebug()) {
				vlogDebug("invenotryStatus : {0} : inventoryQty", invenotryStatus);
			}
			if (getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome().equalsIgnoreCase(pLocationId)) {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.TRU_SHOPPINGCART_NO_ONLINE_INVENTORY_INFO_MESSAGE_KEY);
			} else {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.TRU_SHOPPINGCART_NO_INVENTORY_AT_SELECTED_STORE_INFO_MESSAGE_KEY);
			}
			infoMessage = MessageFormat.format(infoMessage, displayName);
			isValid = Boolean.FALSE;
		}
		}
		else{
			infoMessage =  getErrorHandlerManager().getErrorMessage(
					TRUCommerceConstants.TRU_SHOPPINGCART_NO_SKU_AVAILABLE);
		}
		if (StringUtils.isNotBlank(infoMessage)) {
			getMultipleItemsInfoMessages().add(infoMessage);
		}
		if(StringUtils.isNotBlank(inlineInfoMessage)) {
			getItemInlineMessages().put(pSkuId + TRUCommerceConstants.UNDER_SCORE + pLocationId, inlineInfoMessage);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::quantityValidationsForAddingMultiItems() : END");
		}
		return isValid;
	}

	/**
	 * Validate channel details.
	 */
	public void validateChannelDetails() {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::validateChannelDetails() : BEGIN");
		}
		try {
			String infoMessage = null;
			if (!StringUtils.isBlank(getChannelId())) {
				if (StringUtils.isBlank(getChannelType())) {
					infoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.TRU_SHOPPINGCART_CHANNEL_TYPE_MESSAGE_KEY);
					addFormException(new DropletException(infoMessage, TRUCommerceConstants.INVALID_CHANNEL_PARAMETER));
				}
				if (StringUtils.isBlank(getChannelUserName())) {
					infoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.TRU_SHOPPINGCART_CHANNEL_USER_NAME_MESSAGE_KEY);
					addFormException(new DropletException(infoMessage, TRUCommerceConstants.INVALID_CHANNEL_PARAMETER));
				}
			}
		} catch (SystemException systemException) {
			if (isLoggingError()) {
				logError(" SystemException in @Class::TRUBaseCartModifierFormHandler::@method::validateChannelDetails() :",
						systemException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::validateChannelDetails() : END");
		}

	}
	
	/**
	 * Pre set order by relationship id.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void preSetOrderByRelationshipId(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipId() : BEGIN : isSplitItemRemove : {0}",
					isSplitItemRemove());
		}
		try {
			if (isSplitItemRemove()) {
				preSetOrderByRelationshipIdForSplitItemRemove(pRequest, pResponse);
			} else {
				preSetOrderByRelationshipIdForQuantityUpdate(pRequest, pResponse);
			}
		} catch (CommerceException ce) {
			if (isLoggingError()) {
				logError("CommerceException in @Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipId()", ce);
			}
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError("RepositoryException in @Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipId()", re);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipId() : END");
		}
	}
	
	/**
	 * This method is used to check item inventory.
	 * If inventory is available it will do auto correct the quantity based on the
	 * inventory/customerPurchaseLimit which ever is less and will add corresponding error messages.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws CommerceException
	 *             the commerce exception
	 */
	protected void preSetOrderByRelationshipIdForQuantityUpdate(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException, CommerceException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipIdForQuantityUpdate() : BEGIN");
		}
		setAddUpdateItem(Boolean.TRUE);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipIdForQuantityUpdate() : EditCartFromPage():{0} ",
					getEditCartFromPage());
		}
		try {
			setOrderByRelationshipIdForQuantityUpdate(pRequest);
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError(" RepositoryException in @Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipIdForQuantityUpdate() :",
						e);
			}
		} catch (RelationshipNotFoundException e) {
			if (isLoggingError()) {
				logError(" RelationshipNotFoundException in @Class::TRUBaseCartModifierFormHandler::@method::" + 
				"preSetOrderByRelationshipIdForQuantityUpdate() :", e);
			}
		} catch (InvalidParameterException e) {
			if (isLoggingError()) {
				logError(" InvalidParameterException in @Class::TRUBaseCartModifierFormHandler::" + 
						"@method::preSetOrderByRelationshipIdForQuantityUpdate() :", e);
			}
		} catch (SystemException e) {
			if (isLoggingError()) {
				logError(" SystemException in @Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipIdForQuantityUpdate() :", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipIdForQuantityUpdate() : END");
		}
	}
	
	/**
	 * Sets the order by relationship id for quantity update.
	 *
	 * @param pRequest the new order by relationship id for quantity update
	 * @throws SystemException the system exception
	 * @throws RelationshipNotFoundException the relationship not found exception
	 * @throws InvalidParameterException the invalid parameter exception
	 * @throws RepositoryException the repository exception
	 * @throws CommerceException the commerce exception
	 */
	protected void setOrderByRelationshipIdForQuantityUpdate(DynamoHttpServletRequest pRequest) throws SystemException,
			RelationshipNotFoundException, InvalidParameterException, RepositoryException, CommerceException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::setOrderByRelationshipIdForQuantityUpdate() : BEGIN");
		}
		String skuId = null;
		long desiredQty = 0;
		ShippingGroupCommerceItemRelationship selectedShippingGroupRel = null;
		long relQuantity = 0;
		String locationId = null;
		String displayName = null;
		String infoMessage = null;
		if (null != getOrder()) {
			final Order order = getOrder();
			if (!StringUtils.isBlank(pRequest.getParameter(getRelationShipId()))) {
				desiredQty = Long.parseLong(pRequest.getParameter(getRelationShipId()));
			}
			// added for mobile service if quantity is empty
			if (StringUtils.isBlank(pRequest.getParameter(getRelationShipId()))) {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_EMPTY_INFO_MESSAGE_KEY);
				setAddUpdateItem(Boolean.FALSE);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
			}
			selectedShippingGroupRel = (TRUShippingGroupCommerceItemRelationship) order.getRelationship(getRelationShipId());
			relQuantity = selectedShippingGroupRel.getQuantity();
			if (TRUCommerceConstants.SHIPPING.equalsIgnoreCase(getEditCartFromPage())) {
				desiredQty = desiredQty + relQuantity - TRUConstants.ONE;
				pRequest.setParameter(getRelationShipId(), desiredQty);
			}
			if (null != selectedShippingGroupRel) {
				if (selectedShippingGroupRel.getShippingGroup() instanceof TRUInStorePickupShippingGroup) {
					locationId = ((TRUInStorePickupShippingGroup) selectedShippingGroupRel.getShippingGroup())
							.getLocationId();
				} else if (selectedShippingGroupRel.getShippingGroup() instanceof TRUChannelInStorePickupShippingGroup) {
					locationId = ((TRUChannelInStorePickupShippingGroup) selectedShippingGroupRel.getShippingGroup())
							.getLocationId();
				} else if (selectedShippingGroupRel.getShippingGroup() instanceof TRUHardgoodShippingGroup) {
					locationId = getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome();
				}
				skuId = selectedShippingGroupRel.getCommerceItem().getCatalogRefId();
				if (isLoggingDebug()) {
					vlogDebug("@Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipIdForQuantityUpdate():skuId:{0}, " + 
							"locationId:{1}, desiredQty:{2}", skuId, locationId, desiredQty);
				}
				if (!StringUtils.isBlank(skuId)) {
					displayName = getShoppingCartUtils().getDisplayNameForSku(skuId);
					final int invenotryStatus = getShoppingCartUtils().getInventoryStatus(skuId, locationId);
					if (isLoggingDebug()) {
						vlogDebug(
								"@Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipIdForQuantityUpdate() : invenotryStatus" + 
										" : {0}", invenotryStatus);
					}
					if (invenotryStatus == TRUCommerceConstants.INT_IN_STOCK || invenotryStatus == TRUCommerceConstants.INT_LIMITED_STOCK || 
							invenotryStatus == TRUCommerceConstants.INT_PRE_ORDER) {
						setOrderByRelationshipIdForUpdate(pRequest, desiredQty, locationId, invenotryStatus,
								selectedShippingGroupRel);
						// Gift Wrap Changes
						if (isAddUpdateItem() && StringUtils.isNotBlank(getGiftWrapSkuId())) {
							if (isLoggingDebug()) {
								vlogDebug("desiredQty: {0} relQuantity: {1}", desiredQty, relQuantity);
							}
							String qty = pRequest.getParameter(getRelationShipId());
							if(StringUtils.isNotBlank(qty)){//Fix for
								long updatedQty = Long.parseLong(qty);
								if(desiredQty > updatedQty){
									desiredQty = updatedQty;
								}
							}
							if (desiredQty - relQuantity > 0) {
								getGiftingProcessHelper().updateGiftWrap(getOrder(), getRelationShipId(), getGiftWrapSkuId(),
										desiredQty - relQuantity, true);
							} else {
								getGiftingProcessHelper().updateGiftWrap(getOrder(), getRelationShipId(), getGiftWrapSkuId(),
										-(desiredQty - relQuantity), false);
							}
						}
					} else if (invenotryStatus == TRUCommerceConstants.INT_OUT_OF_STOCK || invenotryStatus == TRUCommerceConstants.NUM_ZERO) {
						if (getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome()
								.equalsIgnoreCase(locationId)) {
							infoMessage = getErrorHandlerManager().getErrorMessage(
									TRUCommerceConstants.TRU_SHOPPINGCART_NO_ONLINE_INVENTORY_INFO_MESSAGE_KEY);
						} else {
							infoMessage = getErrorHandlerManager().getErrorMessage(
									TRUCommerceConstants.TRU_SHOPPINGCART_NO_INVENTORY_AT_SELECTED_STORE_INFO_MESSAGE_KEY);
						}
						infoMessage = MessageFormat.format(infoMessage, displayName);
						setAddUpdateItem(Boolean.FALSE);
						addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::setOrderByRelationshipIdForQuantityUpdate() : END");
		}
	}
	
	/**
	 * Pre set order by relationship id for update.
	 *
	 * @param pRequest            the request
	 * @param pDesiredQty            the desired qty
	 * @param pLocationId            the location id
	 * @param pInventoryStatus            the inventory status
	 * @param pSelectedShippingGroupRel the selected shipping group rel
	 * @throws RepositoryException             the repository exception
	 * @throws SystemException             the system exception
	 */

	protected void setOrderByRelationshipIdForUpdate(DynamoHttpServletRequest pRequest, long pDesiredQty, String pLocationId,
			int pInventoryStatus, ShippingGroupCommerceItemRelationship pSelectedShippingGroupRel) throws RepositoryException,
			SystemException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::setOrderByRelationshipIdForUpdate() : BEGIN");
		}
		boolean isAPIRequest = false;
		if (pRequest.getContextPath().equals(TRUCommerceConstants.REST_CONTEXT_PATH)) {
			isAPIRequest = true;
		}
		long inventoryQty = 0;
		long customerLimit = 0;
		String infoMessage = null;
		long relQuantityPerItemInCart = 0;

		String skuId = pSelectedShippingGroupRel.getCommerceItem().getCatalogRefId();
		long relQuantity = pSelectedShippingGroupRel.getQuantity();
		long commerceItemQtyInCart = pSelectedShippingGroupRel.getCommerceItem().getQuantity();
		String displayName = getShoppingCartUtils().getDisplayNameForSku(skuId);

		customerLimit = getShoppingCartUtils().getCustomerPurchaseLimit(skuId);
		inventoryQty = getShoppingCartUtils().getItemInventory(skuId, pLocationId, pInventoryStatus);
		relQuantityPerItemInCart = (getShoppingCartUtils().getItemQtyInCartByRelShip(skuId, pLocationId, getOrder())) - relQuantity;
		final long purchaseLimit = customerLimit - (commerceItemQtyInCart - relQuantity);
		final long inventoryLimit = inventoryQty - relQuantityPerItemInCart;

		if (isLoggingDebug()) {
			vlogDebug("desiredQty : {0} relQuantity : {1} skuId : {2} : locationId : {3} displayName : {4} inventoryQty : {5} : customerLimit : {6}",
					pDesiredQty, relQuantity, skuId, pLocationId, displayName, inventoryQty, customerLimit);
			vlogDebug("purchaseLimit : {0} inventoryLimit : {1}", purchaseLimit, inventoryLimit);
		}

		if (purchaseLimit > inventoryLimit) {
			setInventoryLimitForReviewPage(Boolean.TRUE);
			if (inventoryLimit == TRUCommerceConstants.INT_ZERO) {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_INVENTORY_REACHED_INFO_MESSAGE_KEY);
				infoMessage = MessageFormat.format(infoMessage, displayName);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_ADJUSTMENT_INFO_INLINE_MESSAGE_KEY);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_INLINE_MSG));
				setAddUpdateItem(Boolean.FALSE);
			} else if (inventoryLimit < pDesiredQty) {
				if (getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome().equalsIgnoreCase(pLocationId)) {
					infoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_INVENTORY_ENFORCEMENT_MESSAGE_KEY);
				} else {
					infoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.TRU_CART_ITEM_INVENTORY_AT_SELECTED_STORE_ENFORCEMENT_INFO_MSG_KEY);
				}
				infoMessage = MessageFormat.format(infoMessage, displayName);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_ADJUSTMENT_INFO_INLINE_MESSAGE_KEY);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_INLINE_MSG));
				pRequest.setParameter(getRelationShipId(), inventoryLimit);
			} else if (inventoryLimit == pDesiredQty) {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_INVENTORY_REACHED_INFO_MESSAGE_KEY);
				infoMessage = MessageFormat.format(infoMessage, displayName, inventoryQty);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_ADJUSTMENT_INFO_INLINE_MESSAGE_KEY);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_INLINE_MSG));
			}
		} else {
			setInventoryLimitForReviewPage(Boolean.TRUE);
			if (purchaseLimit == TRUCommerceConstants.INT_ZERO) {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_LIMIT_REACHED_INFO_MESSAGE_KEY);
				infoMessage = MessageFormat.format(infoMessage, displayName);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_LIMIT_REACHED_INLINE_INFO_MESSAGE_KEY);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_INLINE_MSG));
				setAddUpdateItem(Boolean.FALSE);
			} else if (purchaseLimit < pDesiredQty) {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_LIMIT_ENFORCEMENT_MESSAGE_KEY);
				infoMessage = MessageFormat.format(infoMessage, displayName);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_LIMIT_ENFORCEMENT_INLINE_MESSAGE_KEY);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_INLINE_MSG));
				pRequest.setParameter(getRelationShipId(), purchaseLimit);
			} else if (purchaseLimit == pDesiredQty && !isAPIRequest) {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_LIMIT_REACHED_INFO_MESSAGE_KEY);
				infoMessage = MessageFormat.format(infoMessage, displayName);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_LIMIT_REACHED_INLINE_INFO_MESSAGE_KEY);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_INLINE_MSG));
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::setOrderByRelationshipIdForUpdate() : END");
		}
	}
	
	/**
	 * Pre set order by relationship id for remove.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws CommerceException
	 *             - thows this
	 * @throws RepositoryException
	 *             - if this catch
	 */
	protected void preSetOrderByRelationshipIdForSplitItemRemove(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException, CommerceException, RepositoryException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipIdForSplitItemRemove() : BEGIN");
		}
		long desiredQty = 0;
		ShippingGroupCommerceItemRelationship selectedShippingGroupRel = null;
		long relQtyInCart = 0;
		setAddUpdateItem(Boolean.TRUE);
		if (isLoggingDebug()) {
			vlogDebug("Order : {0} : isSplitItemRemove() : {1} : getGiftWrapSkuId() : {2}", getOrder(), isSplitItemRemove(),
					getGiftWrapSkuId());
		}
		try {
			if (null != getOrder()) {
				if (!StringUtils.isBlank(pRequest.getParameter(getRelationShipId()))) {
					desiredQty = Long.parseLong(pRequest.getParameter(getRelationShipId()));
				}
				selectedShippingGroupRel = (TRUShippingGroupCommerceItemRelationship) getOrder().getRelationship(
						getRelationShipId());
				relQtyInCart = selectedShippingGroupRel.getQuantity();
				if (isLoggingDebug()) {
					vlogDebug("desiredQty : {0} : relQtyInCart : {1}", desiredQty, relQtyInCart);
				}
				if (isSplitItemRemove() && !StringUtils.isBlank(getRelationShipId())) {
					// Gift Wrap changes
					if (StringUtils.isNotBlank(getGiftWrapSkuId())) {
						final long giftWrapQty = Long.parseLong(getWrapItemQuantity());
						getGiftingProcessHelper().updateGiftWrap(getOrder(), getRelationShipId(), getGiftWrapSkuId(),
								giftWrapQty, false);
						if (desiredQty == 0) {
							desiredQty = giftWrapQty;
						}
					}
					if (desiredQty == relQtyInCart) {
						final String[] removalId = new String[TRUConstants.SIZE_ONE];
						removalId[TRUConstants.ZERO] = getRelationShipId();
						setRemovalRelationshipIds(removalId);
						return;
					} else {
						pRequest.setParameter(getRelationShipId(), relQtyInCart - desiredQty);
						return;
					}
				}
			}
		} catch (RelationshipNotFoundException e) {
			if (isLoggingError()) {
				logError(" RelationshipNotFoundException in @Class::TRUBaseCartModifierFormHandler::@method::" + 
						"preSetOrderByRelationshipIdForSplitItemRemove() :", e);
			}
		} catch (InvalidParameterException e) {
			if (isLoggingError()) {
				logError(" InvalidParameterException in @Class::TRUBaseCartModifierFormHandler::@method::" + 
								"preSetOrderByRelationshipIdForSplitItemRemove() :", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipIdForSplitItemRemove() : END");
		}
	}
	
	/**
	 * this method is overridden to call super modifyOrderByRelationshipId method when no form errors are there.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws CommerceException
	 *             the commerce exception
	 * @throws RunProcessException
	 *             the run process exception
	 */
	protected void modifyOrderByRelationshipId(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, CommerceException, RunProcessException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUBaseCartModifierFormHandler::@method::modifyOrderByRelationshipId() : BEGIN : isAddUpdateItem : {0}",
					isAddUpdateItem());
		}
		if (isAddUpdateItem()) {
			super.modifyOrderByRelationshipId(pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::modifyOrderByRelationshipId() : END");
		}
	}
	
	/**
	 * Post set order by relationship id.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void postSetOrderByRelationshipId(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::postSetOrderByRelationshipId() : BEGIN");
		}
		super.postSetOrderByCommerceId(pRequest, pResponse);
		try {
			processEditCartFromReview();
		} catch (CommerceException ce) {
			if (isLoggingError()) {
				logError("CommerceException @Class::TRUBaseCartModifierFormHandler::@method::postSetOrderByRelationshipId()", ce);
			}
		}
		createCartCookies(getOrder(), pRequest, pResponse);
		if(isRestService()){
			setSetOrderByRelationshipIdSuccessURL(getRestSuccessURL());
			setSetOrderByRelationshipIdErrorURL(getRestErrorURL());
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::postSetOrderByRelationshipId() : END");
		}
	}
	
	/**
	 * Process edit cart from review.
	 *
	 * @throws CommerceException             CommerceException
	 */
	protected void processEditCartFromReview() throws CommerceException {
		if (StringUtils.isNotBlank(getEditCartFromPage()) && (getEditCartFromPage().equals(TRUCommerceConstants.SHIPPING))) {
			PipelineResult result = ((TRUOrderManager) getOrderManager()).adjustPaymentGroups(getOrder());
			if (isLoggingDebug()) {
				logDebug(" pResult*********************** " + result);
			}
		}
	}
	
	/**
	 * This method is used to update the cart cookie when ever guest user updates any item in the cart.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void postSetOrderByCommerceId(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::postSetOrderByCommerceId() : BEGIN");
		}
		super.postSetOrderByCommerceId(pRequest, pResponse);
		createCartCookies(getOrder(), pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::postSetOrderByCommerceId() : END");
		}
	}
	
	

	/**
	 * This method will be called after all the processing is done in removeItemFromOrder method, updateRemoveCartCookies() method
	 * will called to update/remove cart cookie accordingly.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 */
	@Override
	public void preRemoveItemFromOrderByRelationshipId(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		// Updating Giftwrap Item
		String infoMessage = null;
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUBaseCartModifierFormHandler::@method::preRemoveItemFromOrderByRelationshipId() : BEGIN : GiftWrapSkuId : {0}",
					getGiftWrapSkuId());
		}
		try {
			if (StringUtils.isNotBlank(getGiftWrapSkuId())) {
				final long giftWrapQty = Long.parseLong(getWrapItemQuantity());
				getGiftingProcessHelper().updateGiftWrap(getOrder(), getRelationShipId(), getGiftWrapSkuId(), giftWrapQty, false);
			}
			if (StringUtils.isBlank(getRelationShipId())) {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_EMPTY_INFO_MESSAGE_KEY);
				setAddUpdateItem(Boolean.FALSE);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
			}
			//Start : MVP 280 changes
			final ShippingGroupCommerceItemRelationship sgCiRel = (ShippingGroupCommerceItemRelationship)getOrder().getRelationship(getRelationShipId());
			TRUHardgoodShippingGroup hardgoodShippingGroup = null;
			if(sgCiRel.getShippingGroup() instanceof HardgoodShippingGroup){
				hardgoodShippingGroup= (TRUHardgoodShippingGroup) sgCiRel.getShippingGroup();			
			}
			if(hardgoodShippingGroup instanceof TRUChannelHardgoodShippingGroup && hardgoodShippingGroup.isRegistryAddress()){
				setTempNickName(hardgoodShippingGroup.getNickName());
			}	
			
			//End : MVP 280 changes
		} catch (CommerceException ce) {
			if (isLoggingError()) {
				logError(
						"CommerceException @Class::TRUBaseCartModifierFormHandler::@method::preRemoveItemFromOrderByRelationshipId()",
						ce);
			}
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError(
						"RepositoryException @Class::TRUBaseCartModifierFormHandler::@method::preRemoveItemFromOrderByRelationshipId()",
						re);
			}
		} catch (SystemException se) {
			if (isLoggingError()) {
				logError(
						"SystemException in @Class::TRUBaseCartModifierFormHandler::@method::preRemoveItemFromOrderByRelationshipId()",
						se);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::preRemoveItemFromOrderByRelationshipId() : END");
		}
	}

	/**
	 * This method will be called after all the processing is done in removeItemFromOrder method.
	 *  updateRemoveCartCookies() method will called to update/remove cart cookie accordingly.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void postRemoveItemFromOrderByRelationshipId(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::postRemoveItemFromOrderByRelationshipId() : BEGIN");
		}
		try {
			processEditCartFromReview();
			getShippingGroupManager().removeEmptyShippingGroups(getOrder());
			if (((TRUOrderTools) getOrderManager().getOrderTools()).getAllInStorePickupShippingGroups(getOrder()) != null) {
				final boolean isOrderOnlyInStorePickupItems = ((TRUPurchaseProcessHelper) getPurchaseProcessHelper())
						.isOrderHavingOnlyInstorePickupItems(getOrder());
				((TRUOrderImpl) getOrder()).setShowMeGiftingOptions(false);
				if (isOrderOnlyInStorePickupItems && 
						((TRUOrderImpl) getOrder()).getActiveTabs().get(TRUCheckoutConstants.SHIPPING_TAB) != null) {
					((TRUOrderImpl) getOrder()).getActiveTabs().remove(TRUCheckoutConstants.SHIPPING_TAB);
					if (((TRUOrderImpl) getOrder()).getActiveTabs().get(TRUCheckoutConstants.GIFTING_TAB) != null) {
						((TRUOrderImpl) getOrder()).getActiveTabs().remove(TRUCheckoutConstants.GIFTING_TAB);
					}
				}
			} else {
				((TRUOrderImpl) getOrder()).getActiveTabs().remove(TRUCheckoutConstants.PICKUP_TAB);
			}
			//Start : MVP 280 changes
			//Start : If registry item removed	,Then remove all the registry addresses from the container for the deleted registry/wishlist item.			
			if(TRUCheckoutConstants.REVIEW_TAB.equalsIgnoreCase(((TRUOrderImpl) getOrder()).getCurrentTab()) && StringUtils.isNotBlank(getTempNickName())){
				if(getShippingGroupMapContainer()!=null){					
					if(StringUtils.isNotBlank(getTempNickName()) &&  getTempNickName().contains(TRUConstants.UNDER_SCORE)){
						String []items = getTempNickName().split(TRUConstants.UNDER_SCORE);
						if(items != null && items.length>=TRUConstants._1){
							String channelId = items[TRUConstants._0].trim();													
							Set<String> nickNamesSet= getShippingGroupMapContainer().getShippingGroupNames();
							for(Iterator<String> nickNameIterator=nickNamesSet.iterator() ; nickNameIterator.hasNext();){
								String removeNickName = nickNameIterator.next();
								if(StringUtils.isNotBlank(removeNickName) && removeNickName.startsWith(channelId)){									
									nickNameIterator.remove();
								}	
							}
						}
					}
				}
			}else if(StringUtils.isNotBlank(getTempNickName())){
				TRUShippingProcessHelper shippingProcessHelper=	getShoppingCartUtils().getShippingHelper();
				shippingProcessHelper.removeShippingAddress(getOrder(),getTempNickName(),getShippingGroupMapContainer());
			}			
			//End : MVP 280 changes
		} catch (CommerceException commerceException) {
			if (isLoggingError()) {
				logError("Commerce Exception in TRUShoppingCartUtils.getItemDetailVOsByRelationShips", commerceException);
			}
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				logError("Commerce Exception in TRUShoppingCartUtils.getItemDetailVOsByRelationShips", repositoryException);
			}
		}

		try {
			updateRemoveCartCookies(pRequest, pResponse);
		} catch (CommerceException commerceException) {
			if (isLoggingError()) {
				logError(
						"CommerceException @Class::TRUBaseCartModifierFormHandler::@method::postRemoveItemFromOrderByRelationshipId()",
						commerceException);
			}

		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::postRemoveItemFromOrderByRelationshipId() : END");
		}
	}
	
	/**
	 * This method is used to update the cart cookies, while removing the commerce items from order.
	 * If user removes the last item from the order this method will remove the cart cookies.
	 * 
	 * @param pRequest
	 *            the servlet's request
	 * @param pResponse
	 *            the servlet's response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws CommerceException
	 *             the commerce exception
	 */
	protected void updateRemoveCartCookies(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, CommerceException {
		final Order order = getOrder();
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::updateRemoveCartCookies() : BEGIN");
			vlogDebug("@Class::TRUBaseCartModifierFormHandler::@method::updateRemoveCartCookies() :: order : {0}", order);
		}

		if (order == null) {
			return;
		}
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUBaseCartModifierFormHandler::@method::updateRemoveCartCookies() :: commerce item count in order : {0}",
					order.getTotalCommerceItemCount());
		}
		final TRUCookieManager cookieManager = (TRUCookieManager) getProfileTools().getCookieManager();
		if (cookieManager != null) {
			if (order.getTotalCommerceItemCount() > 0) {
				createCartCookies(getOrder(), pRequest, pResponse);
			} else {
				cookieManager.removeCartCookie(pRequest, pResponse);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::updateRemoveCartCookies() : END");
		}
	}
	
	/**
	 * This method is overrided to reprice the order when order is invalidated.
	 * 
	 * @param pTransaction
	 *            the transaction
	 */
	@Override
	protected void commitTransaction(Transaction pTransaction) {
		super.commitTransaction(pTransaction);
		if (((TRUOrderImpl) getOrder()).isInvalidated()) {
			repriceOrder(getOrder());

		}

	}
	
	/**
	 * This method is used to reprice the order.
	 * 
	 * @param pOrder
	 *            the order
	 */
	protected void repriceOrder(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::repriceOrder() : BEGIN");
		}
		boolean rollbackFlag = Boolean.FALSE;
		final TransactionDemarcation td = new TransactionDemarcation();
		if (pOrder != null) {
			synchronized (pOrder) {
				try {

					td.begin(getTransactionManager(), TransactionDemarcation.REQUIRES_NEW);
					getPurchaseProcessHelper().runProcessRepriceOrder(getInvalidateOrderPricingOp(), getOrder(),
							getUserPricingModels(), getUserLocale(), getProfile(), createRepriceParameterMap(), this);
				} catch (RunProcessException rpe) {
					rollbackFlag = Boolean.TRUE;
					if (isLoggingError()) {
						logError("RunProcessException in @Class::TRUBaseCartModifierFormHandler::@method::repriceOrder()", rpe);
					}
				} catch (TransactionDemarcationException e) {
					rollbackFlag = Boolean.TRUE;
					if (isLoggingError()) {
						logError(
								"TransactionDemarcationException in @Class::TRUBaseCartModifierFormHandler::@method::repriceOrder()",
								e);
					}
				} finally {
					try {
						if (td != null) {
							td.end(rollbackFlag);
						}
					} catch (TransactionDemarcationException tde) {
						if (isLoggingError()) {
							logError(
									"TransactionDemarcationException in @Class::TRUBaseCartModifierFormHandler::@method::repriceOrder()",
									tde);
						}

					}
				}

			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::repriceOrder() : END");
		}
	}
	
	/**
	 * Method is overrided to add shipping price and calc tax into extra parameters.
	 * 
	 * @param pPricingOperation
	 *            the pricing operation
	 * @param pOrder
	 *            the order
	 * @param pPricingModels
	 *            the pricing models
	 * @param pLocale
	 *            the locale
	 * @param pProfile
	 *            the profile
	 * @param pExtraParameters
	 *            the extra parameters
	 * @throws RunProcessException
	 *             the run process exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected void runProcessRepriceOrder(String pPricingOperation, Order pOrder, PricingModelHolder pPricingModels,
			Locale pLocale, RepositoryItem pProfile, Map pExtraParameters) throws RunProcessException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::runProcessRepriceOrder() : BEGIN");
		}
		Map extraParameters = pExtraParameters;
		if (extraParameters == null) {
			extraParameters = new HashMap();
		}
		// This flag is used in Shipping calculator to update the shipping method price
		// This flag will be set in case of add item to order or delete item from order
		extraParameters.put(TRUCommerceConstants.UPDATE_SHIPPING_PRICE, Boolean.TRUE);
		extraParameters.put(TRUTaxwareConstant.CALC_TAX, Boolean.TRUE);

		super.runProcessRepriceOrder(pPricingOperation, pOrder, pPricingModels, pLocale, pProfile, extraParameters);
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::runProcessRepriceOrder() : END");
		}
	}

	/**
	 * Gets the channel.
	 * 
	 * @return the channel
	 */
	public String getChannelType() {
		return mChannelType;
	}

	/**
	 * Sets the channel.
	 * 
	 * @param pChannelType
	 *            the new channel
	 */
	public void setChannelType(String pChannelType) {
		this.mChannelType = pChannelType;
	}

	/**
	 * Gets the channel id.
	 * 
	 * @return the channel id
	 */
	public String getChannelId() {
		return mChannelId;
	}

	/**
	 * Sets the channel id.
	 * 
	 * @param pChannelId
	 *            the new channel id
	 */
	public void setChannelId(String pChannelId) {
		this.mChannelId = pChannelId;
	}

	/**
	 * Gets the channel user name.
	 * 
	 * @return the channel user name
	 */
	public String getChannelUserName() {
		return mChannelUserName;
	}

	/**
	 * Sets the channel user name.
	 * 
	 * @param pChannelUserName
	 *            the new channel user name
	 */
	public void setChannelUserName(String pChannelUserName) {
		this.mChannelUserName = pChannelUserName;
	}

	/**
	 * Gets the channel co user name.
	 * 
	 * @return the channel co user name
	 */
	public String getChannelCoUserName() {
		return mChannelCoUserName;
	}

	/**
	 * Sets the channel co user name.
	 * 
	 * @param pChannelCoUserName
	 *            the new channel co user name
	 */
	public void setChannelCoUserName(String pChannelCoUserName) {
		mChannelCoUserName = pChannelCoUserName;
	}

	/**
	 * Gets the channel name.
	 * 
	 * @return the channel name
	 */
	public String getChannelName() {
		return mChannelName;
	}

	/**
	 * Sets the channel name.
	 * 
	 * @param pChannelName
	 *            the new channel name
	 */
	public void setChannelName(String pChannelName) {
		mChannelName = pChannelName;
	}
	
	/**
	 * Gets the source channel.
	 *
	 * @return the source channel
	 */
	public String getSourceChannel() {
		return mSourceChannel;
	}

	/**
	 * Sets the source channel.
	 *
	 * @param pSourceChannel the new source channel
	 */
	public void setSourceChannel(String pSourceChannel) {
		mSourceChannel = pSourceChannel;
	}

	/**
	 * Checks if is adds the update item.
	 * 
	 * @return true, if is adds the update item
	 */
	public boolean isAddUpdateItem() {
		return mAddUpdateItem;
	}

	/**
	 * Sets the adds the update item.
	 * 
	 * @param pAddUpdateItem
	 *            the new adds the update item
	 */
	public void setAddUpdateItem(boolean pAddUpdateItem) {
		mAddUpdateItem = pAddUpdateItem;
	}

	/**
	 * Checks if is donation item.
	 * 
	 * @return true, if is donation item
	 */
	public boolean isDonationItem() {
		return mDonationItem;
	}

	/**
	 * Sets the donation item.
	 * 
	 * @param pDonationItem
	 *            the new donation item
	 */
	public void setDonationItem(boolean pDonationItem) {
		mDonationItem = pDonationItem;
	}

	/**
	 * Gets the collection request.
	 * 
	 * @return the mCollectionRequest
	 */
	public String getCollectionRequest() {
		return mCollectionRequest;
	}

	/**
	 * Sets the collection request.
	 * 
	 * @param pCollectionRequest
	 *            the mCollectionRequest to set
	 */
	public void setCollectionRequest(String pCollectionRequest) {
		this.mCollectionRequest = pCollectionRequest;
	}

	/**
	 * Gets the shop local request.
	 * 
	 * @return the mShopLocalRequest
	 */
	public String getShopLocalRequest() {
		return mShopLocalRequest;
	}

	/**
	 * Sets the shop local request.
	 * 
	 * @param pShopLocalRequest
	 *            the mShopLocalRequest to set
	 */
	public void setShopLocalRequest(String pShopLocalRequest) {
		this.mShopLocalRequest = pShopLocalRequest;
	}

	/**
	 * Gets the error handler manager.
	 * 
	 * @return the error handler manager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * Sets the error handler manager.
	 * 
	 * @param pErrorHandlerManager
	 *            the new error handler manager
	 */
	public void setErrorHandlerManager(DefaultErrorHandlerManager pErrorHandlerManager) {
		mErrorHandlerManager = pErrorHandlerManager;
	}

	/**
	 * Gets the shopping cart utils.
	 * 
	 * @return the shopping cart utils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * Sets the shopping cart utils.
	 * 
	 * @param pShoppingCartUtils
	 *            the new shopping cart utils
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		this.mShoppingCartUtils = pShoppingCartUtils;
	}

	/**
	 * Checks if is rest service.
	 * 
	 * @return the mRestService
	 */
	public boolean isRestService() {
		return mRestService;
	}

	/**
	 * Sets the rest service.
	 * 
	 * @param pRestService
	 *            the pRestService to set
	 */
	public void setRestService(boolean pRestService) {
		this.mRestService = pRestService;
	}
	
	/**
	 * Gets the donation amount.
	 * 
	 * @return the donation amount
	 */
	public double getDonationAmount() {
		return mDonationAmount;
	}

	/**
	 * Sets the donation amount.
	 * 
	 * @param pDonationAmount
	 *            the new donation amount
	 */
	public void setDonationAmount(double pDonationAmount) {
		mDonationAmount = pDonationAmount;
	}
	
	/**
	 * Gets the rest success url.
	 *
	 * @return the restSuccessURL
	 */
	public String getRestSuccessURL() {
		return mRestSuccessURL;
	}

	/**
	 * Sets the rest success url.
	 *
	 * @param pRestSuccessURL the restSuccessURL to set
	 */
	public void setRestSuccessURL(String pRestSuccessURL) {
		mRestSuccessURL = pRestSuccessURL;
	}

	/**
	 * Gets the rest error url.
	 *
	 * @return the restErrorURL
	 */
	public String getRestErrorURL() {
		return mRestErrorURL;
	}

	/**
	 * Sets the rest error url.
	 *
	 * @param pRestErrorURL the restErrorURL to set
	 */
	public void setRestErrorURL(String pRestErrorURL) {
		mRestErrorURL = pRestErrorURL;
	}
	
	/**
	 * Gets the profile tools.
	 * 
	 * @return the profile tools
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * Sets the profile tools.
	 * 
	 * @param pProfileTools
	 *            the new profile tools
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}
	
	/**
	 * Checks if is split item remove.
	 * 
	 * @return true, if is split item remove
	 */
	public boolean isSplitItemRemove() {
		return mSplitItemRemove;
	}

	/**
	 * Sets the split item remove.
	 * 
	 * @param pSplitItemRemove
	 *            the new split item remove
	 */
	public void setSplitItemRemove(boolean pSplitItemRemove) {
		mSplitItemRemove = pSplitItemRemove;
	}
	
	/**
	 * Gets the editCartFromPage.
	 * 
	 * @return the editCartFromPage
	 */
	public String getEditCartFromPage() {
		return mEditCartFromPage;
	}

	/**
	 * Sets the editCartFromPage.
	 * 
	 * @param pEditCartFromPage
	 *            the editCartFromPage
	 */
	public void setEditCartFromPage(String pEditCartFromPage) {
		mEditCartFromPage = pEditCartFromPage;
	}
	
	/**
	 * Gets the relation ship id.
	 * 
	 * @return the relation ship id
	 */
	public String getRelationShipId() {
		return mRelationShipId;
	}

	/**
	 * Sets the relation ship id.
	 * 
	 * @param pRelationShipId
	 *            the new relation ship id
	 */
	public void setRelationShipId(String pRelationShipId) {
		mRelationShipId = pRelationShipId;
	}
	
	/**
	 * Gets the gift wrap sku id.
	 * 
	 * @return the giftWrapSkuId
	 */
	public String getGiftWrapSkuId() {
		return mGiftWrapSkuId;
	}

	/**
	 * Sets the gift wrap sku id.
	 * 
	 * @param pGiftWrapSkuId
	 *            the giftWrapSkuId to set
	 */
	public void setGiftWrapSkuId(String pGiftWrapSkuId) {
		mGiftWrapSkuId = pGiftWrapSkuId;
	}
	
	/**
	 * Gets the gifting process helper.
	 * 
	 * @return the giftingProcessHelper
	 */
	public TRUGiftingPurchaseProcessHelper getGiftingProcessHelper() {
		return mGiftingProcessHelper;
	}

	/**
	 * Sets the gifting process helper.
	 * 
	 * @param pGiftingProcessHelper
	 *            the giftingProcessHelper to set
	 */
	public void setGiftingProcessHelper(TRUGiftingPurchaseProcessHelper pGiftingProcessHelper) {
		mGiftingProcessHelper = pGiftingProcessHelper;
	}
	
	/**
	 * Gets the wrap item quantity.
	 * 
	 * @return the wrapItemQuantity
	 */
	public String getWrapItemQuantity() {
		return mWrapItemQuantity;
	}

	/**
	 * Sets the wrap item quantity.
	 * 
	 * @param pWrapItemQuantity
	 *            the wrapItemQuantity to set
	 */
	public void setWrapItemQuantity(String pWrapItemQuantity) {
		mWrapItemQuantity = pWrapItemQuantity;
	}
	
	/**
	 * Gets the invalidate order pricing op.
	 * 
	 * @return the invalidate order pricing op
	 */
	public String getInvalidateOrderPricingOp() {
		return mInvalidateOrderPricingOp;
	}

	/**
	 * Sets the invalidate order pricing op.
	 * 
	 * @param pInvalidateOrderPricingOp
	 *            the new invalidate order pricing op
	 */
	public void setInvalidateOrderPricingOp(String pInvalidateOrderPricingOp) {
		mInvalidateOrderPricingOp = pInvalidateOrderPricingOp;
	}

	/**
	 * Gets the multiple items info messages.
	 *
	 * @return the multiple items info messages
	 */
	public List<String> getMultipleItemsInfoMessages() {
		if(null == mMultipleItemsInfoMessages) {
			mMultipleItemsInfoMessages = new ArrayList<String>();
		}
		return mMultipleItemsInfoMessages;
	}

	/**
	 * Sets the multiple items info messages.
	 *
	 * @param pMultipleItemsInfoMessages the new multiple items info messages
	 */
	public void setMultipleItemsInfoMessages(List<String> pMultipleItemsInfoMessages) {
		mMultipleItemsInfoMessages = pMultipleItemsInfoMessages;
	}

	/**
	 * Gets the item inline messages.
	 *
	 * @return the item inline messages
	 */
	public Map<String, String> getItemInlineMessages() {
		if(null == mItemInlineMessages) {
			mItemInlineMessages = new HashMap<String, String>();
		}
		return mItemInlineMessages;
	}

	/**
	 * Sets the item inline messages.
	 *
	 * @param pItemInlineMessages the item inline messages
	 */
	public void setItemInlineMessages(Map<String, String> pItemInlineMessages) {
		mItemInlineMessages = pItemInlineMessages;
	}
	
	/**
	 * holds RegistryService instance.
	 */
	private RegistryService mRegistryService;
	

	/**
	 * @return the mRegistryService.
	 */
	public RegistryService getRegistryService() {
		return mRegistryService;
	}

	/**
	 * @param pRegistryService the mRegistryService to set.
	 */
	public void setRegistryService(RegistryService pRegistryService) {
		this.mRegistryService = pRegistryService;
	}
	
	
	/** holds TRUConfiguration instance. **/
	private TRUConfiguration mTruConfiguration;

	/**
	 * @return the configuration
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * @param pTruConfiguration the configuration to set
	 */
	public void setTruConfiguration(TRUConfiguration pTruConfiguration) {
		mTruConfiguration = pTruConfiguration;
	}
	
	/**
	 * The variable for channelItemFromPDP check.
	 */
	private boolean mChannelItemFromPDP;

	/**
	 * @return true, If it's a channelItemFromPDP.
	 */
	public boolean isChannelItemFromPDP() {
		return mChannelItemFromPDP;
	}

	/**
	 * @param pChannelItemFromPDP the boolean value to set
	 */
	public void setChannelItemFromPDP(boolean pChannelItemFromPDP) {
		this.mChannelItemFromPDP = pChannelItemFromPDP;
	}
	
	/**
	 * Property to hold deletedFlag.
	 */
	
	private String mDeletedFlag;

	
	/**
	 * @return the deletedFlag.
	 */
	public String getDeletedFlag() {
		return mDeletedFlag;
	}

	/**
	 * @param pDeletedFlag the deletedFlag to set.
	 */
	public void setDeletedFlag(String pDeletedFlag) {
		mDeletedFlag = pDeletedFlag;
	}
}
