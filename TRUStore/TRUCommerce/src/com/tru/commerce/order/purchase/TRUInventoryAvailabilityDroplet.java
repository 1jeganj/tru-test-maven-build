package com.tru.commerce.order.purchase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.inventory.TRUCoherenceInventoryManager;
import com.tru.commerce.inventory.TRUInventoryInfo;
import com.tru.common.TRUConstants;

/**
 * This droplet is to get the item inventory status and availability.
 * @version 1.0
 * @author Professional Access
 */
public class TRUInventoryAvailabilityDroplet extends DynamoServlet {

	/** The Constant EMPTY_JSON. */
	private static final String EMPTY_JSON = "{}";

	/** The Constant UNDER_SCORE. */
	private static final String UNDER_SCORE = "_";

	/** The Constant INT_ZERO. */
	private static final int INT_ZERO = 0;


	/** The Constant INT_OUT_OF_STOCK. */
	private static final int INT_OUT_OF_STOCK = 16770;


	/** The Constant INVENTORY_RESPONSE. */
	private static final String INVENTORY_RESPONSE = "inventoryResponse";


	/** The Constant QUANTITY. */
	private static final String QUANTITY = "quantity";


	/** The Constant OUT_OF_STOCK. */
	private static final String OUT_OF_STOCK = "OutOfStock";


	/** The Constant IN_STOCK. */
	private static final String IN_STOCK = "InStock";


	/** The Constant STATUS. */
	private static final String STATUS = "status";


	/** The Constant ONLINE_STRING. */
	private static final String ONLINE_STRING = "643";


	/** The Constant LOCATION_ID. */
	private static final String LOCATION_ID = "locationId";


	/** The Constant SKU_ID. */
	private static final String SKU_ID = "skuId";


	/** The Constant INVENTORY_REQUEST_POST. */
	private static final String INVENTORY_REQUEST_POST = "inventoryRequestPost";


	/** The Constant INVENTORY_REQUEST. */
	private static final String INVENTORY_REQUEST = "inventoryRequest";


	/** The Coherence inventory manager. */
	private TRUCoherenceInventoryManager mCoherenceInventoryManager;
	
	
	/**
	 * @return the coherenceInventoryManager
	 */
	public TRUCoherenceInventoryManager getCoherenceInventoryManager() {
		return mCoherenceInventoryManager;
	}

	/**
	 * @param pCoherenceInventoryManager the coherenceInventoryManager to set
	 */
	public void setCoherenceInventoryManager(TRUCoherenceInventoryManager pCoherenceInventoryManager) {
		mCoherenceInventoryManager = pCoherenceInventoryManager;
	}

	/**
	 * This service method is to get the item inventory status and availability.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest
	 * @param pResponse
	 *            - DynamoHttpServletResponse
	 * @throws ServletException
	 *             - if any
	 * @throws IOException
	 *             - if any
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Enter into Class : TRUInventoryAvailabilityDroplet method : service");
		}
		JSONObject json = null;
		List<TRUInventoryInfo> invRequestList = new ArrayList<TRUInventoryInfo>();
		String inventoryRequest = (String) pRequest.getLocalParameter(INVENTORY_REQUEST);
		JSONArray finalJsonArray = new JSONArray();
		Map<String,TRUInventoryInfo> responseMap = new HashMap<String, TRUInventoryInfo>();
		if(StringUtils.isNotEmpty(inventoryRequest) && !EMPTY_JSON.equals(inventoryRequest)){
			try {
				json = new JSONObject(inventoryRequest);
				if(json != null){
					String skuId = null;
					String locationId = null;
					TRUInventoryInfo respInventoryInfo = null;
					JSONArray inventoryRequestJson = (JSONArray)json.get(INVENTORY_REQUEST_POST);
					for (int i = 0; i < inventoryRequestJson.length(); i++) {
		                skuId = (String) inventoryRequestJson.getJSONObject(i).get(SKU_ID);
		                locationId = (String) inventoryRequestJson.getJSONObject(i).get(LOCATION_ID);
		                TRUInventoryInfo invInfo = new TRUInventoryInfo(skuId, INT_ZERO, locationId);
		                invRequestList.add(invInfo);
					}
					List<TRUInventoryInfo> responseInfoList = getCoherenceInventoryManager().validateBulkStockLevel(invRequestList);
					for (TRUInventoryInfo truInventoryInfo : responseInfoList) {
						String truInventoryInfoLocId = truInventoryInfo.getLocationId();
						if(truInventoryInfoLocId == null){
							truInventoryInfoLocId = ONLINE_STRING;
						}
						responseMap.put(truInventoryInfo.getCatalogRefId()+UNDER_SCORE+truInventoryInfoLocId, truInventoryInfo);
					}
					if(!invRequestList.isEmpty() && !responseInfoList.isEmpty()){
						for (TRUInventoryInfo reqInventoryInfo : invRequestList) {
							JSONObject formDetailsJson = new JSONObject();
							if(responseMap.containsKey(reqInventoryInfo.getCatalogRefId()+UNDER_SCORE+reqInventoryInfo.getLocationId())){
								respInventoryInfo=responseMap.get(reqInventoryInfo.getCatalogRefId()+UNDER_SCORE+reqInventoryInfo.getLocationId());
								formDetailsJson.put(SKU_ID, respInventoryInfo.getCatalogRefId());
								if(respInventoryInfo.getLocationId() == null){
									formDetailsJson.put(LOCATION_ID, ONLINE_STRING);
								}else{
									formDetailsJson.put(LOCATION_ID, respInventoryInfo.getLocationId());
								}
								if(respInventoryInfo.getAtcStatus() == INT_OUT_OF_STOCK){
									formDetailsJson.put(STATUS, OUT_OF_STOCK);
								}else{
									formDetailsJson.put(STATUS, IN_STOCK);
								}
								formDetailsJson.put(QUANTITY, respInventoryInfo.getQuantity());
								finalJsonArray.add(formDetailsJson);
							}else {
								formDetailsJson.put(SKU_ID, reqInventoryInfo.getCatalogRefId());
								if(reqInventoryInfo.getLocationId() == null){
									formDetailsJson.put(LOCATION_ID, ONLINE_STRING);
								}else{
									formDetailsJson.put(LOCATION_ID, reqInventoryInfo.getLocationId());
								}
								formDetailsJson.put(STATUS, OUT_OF_STOCK);
								formDetailsJson.put(QUANTITY, reqInventoryInfo.getQuantity());
								finalJsonArray.add(formDetailsJson);
							}
							
						}
					}
				}
			} catch (JSONException e) {
				if(isLoggingError()){
					logError("JSONException @TRUInventoryAvailabilityDroplet --",e);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("final JSON Array is -- " +finalJsonArray);
		}
		pRequest.setParameter(INVENTORY_RESPONSE, finalJsonArray);
		pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("Exit from Class : TRUInvnetoryAvailabilityDroplet method : service");
		}
	}

}
